#******************************************************************************
# Copyright (c) 2010-2011 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes that represent SystemC configuration data.
# This is a layer on top of Carbon.Cfg.Config that organizes the configuration
# into a form that is more useful for generating the code.

import os
import re

import Carbon.Cfg
import Carbon.DB
import Carbon.SystemC.transactors

###############################################################################
#
# Utils
#
###############################################################################
def escapeCString(s):
  result = ''
  for c in s:
    if c == '\\' or c == '"':
      result += '\\'
    result += c
  return result

class Variable:
  def __init__(self, typeName, name):
    self.__typeName = typeName
    self.__name = name

  def declaration(self):
    return self.__typeName + ' ' + self.__name

  def name(self):
    return self.__name


###############################################################################
#
# Wrappers around the RTL ports
#
###############################################################################
class CarbonPortWrapper:
  def __init__(self, portName, fullName, width, isTristate):
    self.__portName = portName
    self.__fullName = fullName
    self.__isTristate = isTristate
    self.__numWords = (width + 31) / 32
    self.__name = 'm_carbon_' + portName

  def name(self):
    return self.__name

  def fullName(self):
    return self.__fullName

  def portName(self):
    return self.__portName

  def bufType(self):
    bufType = 'Carbon2StateBuffer'
    if self.__isTristate:
      bufType = 'Carbon3StateBuffer'
    return '%s<%d>' % (bufType, self.__numWords)

  def declaration(self):
    code = "CarbonVhmNet<%s >\t%s;" % (self.bufType(), self.name())
    return code

  def constructorParm(self):
    return ''

  def vhmNet(self):
    return self.name()

  def initialize(self, delayed):
    return '%s.initialize(carbonModelHandle, "%s");' % (self.name(), escapeCString(self.fullName()))

class CarbonOutputPortWrapper(CarbonPortWrapper):
  def initialize(self, delayed):
    data = 'mCarbonWriteData'
    func = 'mCarbonWriteFuncs'
    if delayed:
      data = 'mCarbonDelayedWriteData'
      func = 'mCarbonDelayedWriteFuncs'
    return '%s.initialize(carbonModelHandle, "%s", &%s, &%s);' % (self.name(), escapeCString(self.fullName()), data, func)

  def vhmNet(self):
    return self.name() + '.net()'

  def constructorParm(self):
    return '%s(%s)' % (self.name(), self.portName())

class CarbonInoutPortWrapper(CarbonPortWrapper):
  def vhmNet(self):
    return self.name() + '.net()'

  def constructorParm(self):
    return '%s(%s)' % (self.name(), self.portName())

###############################################################################
# SystemC Data Types.
#
# There is a subclass of ScDataType for every SystemC type that we
# allow to represent a port value.
###############################################################################
class ScDataType:
  def __init__(self, bitSize, scType):
    self.__bitSize = bitSize
    self.__scType = scType

  def isValidSize(self, size):
    max = self.maxSize()
    if max == 0:
      return True
    else:
      return size <= max

  def maxSize(self):
    return 0

  def supportsBidirects(self):
    return True

  def bitSize(self):
    return self.__bitSize

  def numUInt32(self):
    return (self.__bitSize + 31) / 32

  def typeName(self):
    return "%s<%d>" % (self.__scType, self.__bitSize)


#------------------------------------------------------------------------------
# sc_logic
#------------------------------------------------------------------------------
class ScLogicType(ScDataType):
  def __init__(self, bitSize):
    ScDataType.__init__(self, bitSize, "sc_logic")

  def typeName(self):
    return "sc_logic"

  def maxSize(self):
    return 1

#------------------------------------------------------------------------------
# sc_lv
#------------------------------------------------------------------------------
class ScLvType(ScDataType):
  def __init__(self, bitSize):
    ScDataType.__init__(self, bitSize, "sc_lv")

#------------------------------------------------------------------------------
# sc_bool
#------------------------------------------------------------------------------
class ScBoolType(ScDataType):
  def __init__(self, bitSize):
    ScDataType.__init__(self, bitSize, "bool")

  def typeName(self):
    return "bool"

  def maxSize(self):
    return 1

  def supportsBidirects(self):
    return False

#------------------------------------------------------------------------------
# sc_uint, sc_int
#------------------------------------------------------------------------------
class ScUintType(ScDataType):
  def __init__(self, bitSize, name='sc_uint'):
    ScDataType.__init__(self, bitSize, name)

  def maxSize(self):
    return 64
  
  def supportsBidirects(self):
    return False

class ScIntType(ScUintType):
  def __init__(self, bitSize):
    ScUintType.__init__(self, bitSize, 'sc_int')

#------------------------------------------------------------------------------
# sc_biguint, sc_bigint
#------------------------------------------------------------------------------
class ScBigUintType(ScDataType):
  def __init__(self, bitSize, name='sc_biguint'):
    ScDataType.__init__(self, bitSize, name)

  def supportsBidirects(self):
    return False

class ScBigIntType(ScBigUintType):
  def __init__(self, bitSize):
    ScBigUintType.__init__(self, bitSize, 'sc_bigint')

#------------------------------------------------------------------------------
# sc_bv
#------------------------------------------------------------------------------
class ScBvType(ScDataType):
  def __init__(self, bitSize):
    ScDataType.__init__(self, bitSize, "sc_bv")

  def supportsBidirects(self):
    return False

#------------------------------------------------------------------------------
# unsigned char, char
#------------------------------------------------------------------------------
class ScUnsignedCharType(ScDataType):
  def __init__(self, bitSize, name='unsigned char'):
    ScDataType.__init__(self, bitSize, name)

  def typeName(self):
    return 'unsigned char'

  def maxSize(self):
    return 8

  def supportsBidirects(self):
    return False

class ScCharType(ScUnsignedCharType):
  def __init__(self, bitSize):
    ScUnsignedCharType.__init__(self, bitSize, 'char')

  def typeName(self):
    return 'char'

#------------------------------------------------------------------------------
# unsigned int, int
#------------------------------------------------------------------------------
class ScUnsignedIntegerType(ScDataType):
  def __init__(self, bitSize, name='unsigned int'):
    ScDataType.__init__(self, bitSize, name)

  def typeName(self):
    return "unsigned int"

  def maxSize(self):
    return 32

  def supportsBidirects(self):
    return False

class ScIntegerType(ScUnsignedIntegerType):
  def __init__(self, bitSize):
    ScUnsignedIntegerType.__init__(self, bitSize, 'int')

  def typeName(self):
    return 'int'

#------------------------------------------------------------------------------
# unsigned long, long
#------------------------------------------------------------------------------
class ScUnsignedLongType(ScDataType):
  def __init__(self, bitSize, name='unsigned long'):
    ScDataType.__init__(self, bitSize, name)

  def typeName(self):
    return 'unsigned long'

  def maxSize(self):
    return 32

  def supportsBidirects(self):
    return False

class ScLongType(ScUnsignedLongType):
  def __init__(self, bitSize):
    ScUnsignedLongType.__init__(self, bitSize, 'long int')

  def typeName(self):
    return 'long int'

###############################################################################
# Port Types
###############################################################################
class ScPort:
  INPUT = 1; OUTPUT = 2; INOUT = 3;

  def __init__(self, name, cfgPort, dbNode, scDataType, isTristate, scPortDir, export):
    self.__name = name
    self.__scDataType = scDataType
    self.__cfgPort = cfgPort
    self._scPortDir = scPortDir
    self.__dbNode = dbNode
    self.__export = export

    # TODO - get rid of this
    self._carbonPortWrapper = CarbonPortWrapper(name, dbNode.fullName(), dbNode.getWidth(), isTristate)

    if not scDataType.isValidSize(scDataType.bitSize()):
      message = 'Port %(name)s is %(bits)s bits wide. Type %(type)s can only hold values up to %(max)s bits wide.' % \
                { 'name': name,
                  'bits': scDataType.bitSize(),
                  'max':  scDataType.maxSize(),
                  'type': scDataType.typeName() }
      raise Exception(message)

  def dbNode(self):
    return self.__dbNode
  
  def exported(self):
    return self.__export

  def scPortDirection(self):
    return self._scPortDir

  def scPortExpr(self):
    return self.__cfgPort.expr()

  def buildDepositString(self):
    code=''
    if  len(self.scPortExpr()) > 0:
      if (self.numUInt32() == 1 or self.numUInt32() == 2):
        code+=  "CarbonUInt%(thisWidth)s value = %(netName)s.getBuffer%(thisWidth)s();\n"%{'netName': self.net().vhmNet(), 'thisWidth':self.thisPortWidth()}
        code += 'value=%s;\n'%(self.translateExpr(self.scName(), self.scPortExpr(), 'value'))
        code+=  "%(netName)s.setBuffer%(thisWidth)s(value);\n"%{'netName': self.net().vhmNet(),'thisWidth':self.thisPortWidth()}

    code+=  "%(netName)s.deposit();\n"%{'netName': self.net().vhmNet()}
    return code;

  def fullName(self):
    return self.__dbNode.fullName()
  
  def translateExpr(self, name, expr, repl):
    # Create a regular expression to match the identifier. It has to be
    # <non-ident-chars><name><non-ident-chars>
    regexpr = "([^a-zA-Z0-9_]|^)" + name + "([^a-zA-Z0-9_]|$)"
    prog = re.compile(regexpr)
    return prog.sub("\\1" + repl + "\\2", expr)

  def thisPortWidth(self):
    if (self.numUInt32() == 1):
      return "32"
    elif (self.numUInt32() == 2):
      return "64"
    else:
        raise "invalid port width"

  def scName(self):
    # for now, the SystemC name for the port is the same as the given name
    return self.__name

  def portName(self):
    return self.__cfgPort.name()

  def scDataType(self):
    return self.__scDataType

  def scPortType(self):
    raise "not implemented"

  def carbonNetVar(self):
    return 'm_' + self.__name + '_netId'

  def net(self):
    return self._carbonPortWrapper

  def numUInt32(self):
    return self.__scDataType.numUInt32()

  def generateDeclaration(self):
    return self.scPortType() + " " + self.scName() + ";"

  def generateDataMembers(self):
    return self._carbonPortWrapper.declaration()
  

  def generateInputChangeMethod(self, userCode):
    return ""

  def registerInputChangeMethod(self):
    return ""

  def generateOutputResolvedMethod(self):
    return ""

  def registerOutputResolvedMethod(self):
    return ""

  def generateConstructorParms(self):
    return '%s("%s")' % (self.scName(), self.scName())

class ScInPort(ScPort):
  def __init__(self, name, cfgPort, dbNode, scDataType, isTristate, trigger, export):
    ScPort.__init__(self, name, cfgPort, dbNode, scDataType, isTristate, ScPort.INPUT, export)
    self.__trigger = trigger
    if export:
      self.__portType = "sc_in<" + self.scDataType().typeName() +" >"
    else:
      self.__portType = "sc_signal<" + self.scDataType().typeName() +" >"

  def trigger(self):
    return self.__trigger

  def scPortType(self):
    return self.__portType

  def generateInputChangeMethod(self, userCode):
    code = ''
    
    # if 1-bit clock, also generate an edge-sensitive version of the input method
    if self.scDataType().bitSize() == 1 and self.trigger().isEdgeSensitive():
      code += self.generateEdgeSensitiveInputChangeMethod(userCode)
        
    runSchedule = ''
    if self.trigger().runSchedule():
      runSchedule = """
    // request a call to carbonSchedule to update the Carbon Model state
    carbon_schedule_channel.requestSchedule();
       """
    code += """
  void carbon_input_change_%(port)s()\n  {
    %(userCodeBegin)s
#ifdef CARBON_SC_TRACE
    cout << "CARBON_SC_TRACE: [" << sc_time_stamp() << "] %(port)s input change: value = " << %(port)s.read() << endl;
#endif

    // read the new port value into the buffer for the Carbon net
    carbon_sc_to_uint32<%(bufType)s, %(numWords)s, %(numBits)s>(%(port)s.read(), %(net)s.buffer());

    // transfer the new value of the signal to the Carbon Model
    %(buildDepositString)s;
    %(runSchedule)s
    %(userCodeEnd)s
  }
  """ % { 'port': self.scName(),
          'scType': self.scDataType().typeName(),
          'scPortType': self.scPortType(),
          'numWords': self.numUInt32(),
          'buildDepositString': self.buildDepositString(),
          'numBits': self.scDataType().bitSize(),
          'bufType': self.net().bufType(),
          'net': self.net().vhmNet(),
          'runSchedule': runSchedule,
          'scPortExpr':self.scPortExpr(),            
          'userCodeBegin': userCode.preSection(self.trigger().userCodeTag(self.scName())),
          'userCodeEnd': userCode.postSection(self.trigger().userCodeTag(self.scName())) }
    return code

  def generateEdgeSensitiveInputChangeMethod(self, userCode):
    newValue = self.trigger().value()
    oldValue = 1 - self.trigger().value()
    if len(self.scPortExpr()) > 0:
      print "Port Expressions not supported on clocks for port %(portItem)s\n"%{'portItem':self.scName}
    return """\
  void carbon_input_change_%(edge)s_%(port)s()\n  {
    %(userCodeBegin)s
#ifdef CARBON_SC_TRACE
    cout << "CARBON_SC_TRACE: %(port)s input change: value = " << %(port)s.read() << endl;
#endif

    // toggle the signal value so the Carbon Model knows it has changed
    %(net)s.buffer().value()[0] = %(oldValue)s;
    %(net)s.deposit();
    %(net)s.buffer().value()[0] = %(newValue)s;
    %(net)s.deposit();

    // request a call to carbonSchedule to update the Carbon Model state
    carbon_schedule_channel.requestSchedule();
    %(userCodeEnd)s
  }
  """ % { 'port': self.scName(),
          'edge': self.trigger().edge(),
          'net': self.net().vhmNet(),
          'oldValue': oldValue,
          'newValue': newValue,
          'scPortExpr':self.scPortExpr(),  
          'userCodeBegin': userCode.preSection(self.trigger().userCodeTag(self.scName())),
          'userCodeEnd': userCode.postSection(self.trigger().userCodeTag(self.scName())) }
    

  def registerInputChangeMethod(self):
    registerBothEdgeMethod = """\
      SC_METHOD(carbon_input_change_%(port)s);
      sensitive << %(port)s;
      dont_initialize();""" % { "port" : self.scName() }

    # if 1-bit clock, also register an edge-sensitive version of the input method
    if self.scDataType().bitSize() == 1 and self.trigger().isEdgeSensitive():
      code = """
    if (scheduleAllClockEdges) {
%(registerBothEdgeMethod)s    
    }
    else {
      SC_METHOD(carbon_input_change_%(edge)s_%(port)s);
      %(sensitive)s;
      dont_initialize();
    }
    """ % { "port" : self.scName(),
            'edge': self.trigger().edge(),
            'registerBothEdgeMethod': registerBothEdgeMethod,
            'sensitive': self.trigger().sensitive(self.scName()) }
    else:
      code = registerBothEdgeMethod
    return code


class ScDepositable(ScInPort):
  def __init__(self, name, cfgPort, dbNode, scDataType, isTristate, trigger):
    ScInPort.__init__(self, name, cfgPort, dbNode, scDataType, isTristate, trigger, True)

  def scPortType(self):
      return "sc_buffer<" + self.scDataType().typeName() +" >"

class ScOutPort(ScPort):
  def __init__(self, name, cfgPort, dbNode, scDataType, isTristate, export):
    ScPort.__init__(self, name, cfgPort, dbNode, scDataType, isTristate, ScPort.OUTPUT, export)
    self._carbonPortWrapper = CarbonOutputPortWrapper(name, dbNode.fullName(), dbNode.getWidth(), isTristate)
    if export:
      self.__portType = "sc_out<" + self.scDataType().typeName() +" >"
    else:
      self.__portType = "sc_signal<" + self.scDataType().typeName() +" >"

  def generateDataMembers(self):
    return """\
CarbonScOutputPort<%(scType)s, %(scPortType)s, %(bufType)s, %(numWords)s, %(numBits)s>  %(portWrapper)s;\
""" % { 'port': self.scName(),
        'scType': self.scDataType().typeName(),
        'scPortType': self.scPortType(),
        'numWords': self.numUInt32(),
        'numBits': self.scDataType().bitSize(),
        'portWrapper': self.net().name(),
        'bufType': self.net().bufType() }
    
  def generateWriteOutput(self, userCode):
    return """
%(userCodeBegin)s
%(userCodeEnd)s
""" % { 'userCodeBegin': userCode.preSection('WRITE PORT ' + self.scName()),
        'userCodeEnd': userCode.postSection('WRITE PORT ' + self.scName()) }

  def buildExamineString(self):
    code=''
    code+=  "%(netName)s.examine();\n"%{'netName': self.net().vhmNet()}
    if  len(self.scPortExpr()) > 0:
      if (self.numUInt32() == 1 or self.numUInt32() == 2):
        code+=  "      CarbonUInt%(thisWidth)s value = %(netName)s.getBuffer%(thisWidth)s();\n"%{'netName': self.net().vhmNet(), 'thisWidth':self.thisPortWidth()}
        code += '      value=%s;\n'%(self.translateExpr(self.scName(), self.scPortExpr(), 'value'))
        code+=  "      %(netName)s.setBuffer%(thisWidth)s (value);\n"%{'netName': self.net().vhmNet(),'thisWidth':self.thisPortWidth()}
      else:
        print "Ignoring non 32 or 64 bit port expression for port %(portItem)s\n"%{'portItem':self.scName}

    return code;

  def generateInitializeOutput(self, userCode, scMethod='initialize'):
    code = """\
    { // port %(port)s, Carbon net %(path)s
%(userCodeBegin)s
      %(buildExamineString)s;
       %(portWrapper)s.enqueueWrite();
%(userCodeEnd)s
    }
""" % { 'port': self.scName(),
        'path': self.net().fullName(),
        'scPortExpr':self.scPortExpr(),
        'vhmNet': self.net().vhmNet(),
        'portWrapper': self.net().name(),
        'buildExamineString': self.buildExamineString(),
        'userCodeBegin': userCode.preSection('INIT OUT PORT ' + self.scName()),
        'userCodeEnd': userCode.postSection('INIT OUT PORT ' + self.scName()) }
    return code
  
  def scPortType(self):
    return self.__portType

class ScInoutPort(ScInPort,ScOutPort):
  def __init__(self, name, cfgPort, dbNode, scDataType, isTristate, trigger, export, allow2StateTypes=False):
    ScInPort.__init__(self, name, cfgPort, dbNode, scDataType, isTristate, trigger, export)
    ScOutPort.__init__(self, name, cfgPort, dbNode, scDataType, isTristate, export)
    self._scPortDir = ScPort.INOUT
    self.__scValueBuffer = Variable(scDataType.typeName(), 'm_' + self.scName() + '_sc_value_buf')
    self.__scheduleFlag = Variable('bool', 'm_' + self.scName() + '_schedule_change')
    self.__resolvedEvent = Variable('sc_event', 'm_' + self.scName() + '_resolved_event')
    
    self._carbonPortWrapper = CarbonInoutPortWrapper(name, dbNode.fullName(), dbNode.getWidth(), isTristate)

    if export:
      self.__portType = "sc_inout<" + self.scDataType().typeName() +" >"
    else:
      self.__portType = "sc_signal<" + self.scDataType().typeName() +" >"

    if not (allow2StateTypes or scDataType.supportsBidirects()):
      raise Exception('Port %s is bidirectional, and cannot be represented by type %s' % (name, scDataType.typeName()))

  def scPortType(self):
    return self.__portType

  def generateDataMembers(self):
    return """\
CarbonScInoutPort<%(scType)s, %(scPortType)s, %(bufType)s, %(numWords)s, %(numBits)s>  %(portWrapper)s;\
""" % { 'port': self.scName(),
        'scType': self.scDataType().typeName(),
        'scPortType': self.scPortType(),
        'numWords': self.numUInt32(),
        'numBits': self.scDataType().bitSize(),
        'portWrapper': self.net().name(),
        'bufType': self.net().bufType() }
    
  def generateInitializeOutput(self, userCode, scMethod='initialize'):
    code = """\
    { // port %(port)s, Carbon net %(path)s
%(userCodeBegin)s
      %(portWrapper)s.initializeSystemCValue();
%(userCodeEnd)s
    }
""" % { 'port': self.scName(),
        'path': self.net().fullName(),
        'portWrapper': self.net().name(),
        'userCodeBegin': userCode.preSection('INIT OUT PORT ' + self.scName()),
        'userCodeEnd': userCode.postSection('INIT OUT PORT ' + self.scName()) }
    return code

  def generateInputChangeMethod(self, userCode):
    runSchedule = ''
    if self.trigger().runSchedule():
      runSchedule = """
    // request a call to carbonSchedule to update the Carbon Model state
    carbon_schedule_channel.requestSchedule();
      """
    code = """
  void carbon_input_change_%(port)s()\n  {
%(userCodeBegin)s

#ifdef CARBON_SC_TRACE
    cout << "CARBON_SC_TRACE: [" << sc_time_stamp() << "] %(port)s input change: value = " << %(port)s.read() << endl;
#endif

    // read the new port value into the buffer for the Carbon net
    %(portWrapper)s.scValueBuffer() = %(port)s.read();
    carbon_sc_to_uint32<%(bufType)s, %(numWords)s, %(numBits)s>(%(portWrapper)s.scValueBuffer(), %(net)s.buffer());

    // transfer the new value of the signal to the Carbon Model
    %(buildDepositString)s;
    %(runSchedule)s
%(userCodeEnd)s
  }
  """ % { 'port': self.scName(),
          'scType': self.scDataType().typeName(),
          'scPortType': self.scPortType(),
          'scPortExpr':self.scPortExpr(),            
          'numWords': self.numUInt32(),
          'numBits': self.scDataType().bitSize(),
          'bufType': self.net().bufType(),
          'buildDepositString': self.buildDepositString(),
          'net': self.net().vhmNet(),
          'portWrapper': self.net().name(),
          'runSchedule': runSchedule,
          'userCodeBegin' :userCode.preSection('INPUT ' + self.scName()),
          'userCodeEnd' : userCode.postSection('INPUT ' + self.scName()) }
    return code

  def generateWriteOutput(self, userCode):
    return """\
%(userCodeBegin)s
  %(portWrapper)s.updateSystemCValue();
%(userCodeEnd)s
""" % { 'portWrapper': self.net().name(),
        'userCodeBegin': userCode.preSection('WRITE PORT ' + self.scName()),
        'userCodeEnd': userCode.postSection('WRITE PORT ' + self.scName()) }

class ScObservable(ScOutPort):
  def __init__(self, name, cfgPort, dbNode, scDataType, isTristate):
    ScOutPort.__init__(self, name, cfgPort, dbNode, scDataType, isTristate, True)
    
  def setPortExpr(self,expr):
    self.__scPortExp=expr
    
  def scPortType(self):
    return "sc_signal<" + self.scDataType().typeName() +" >"
  
  def generateInitializeOutput(self,userCode):
    # call base class, but tell it to use 'write' method,
    # because sc_signal has no initialize()
    return ScOutPort.generateInitializeOutput(self, userCode, 'write')

class ScDepositableAndObservable(ScInoutPort):
  def __init__(self, name, cfgPort, dbNode, scDataType, isTristate, trigger):
    ScInoutPort.__init__(self, name, cfgPort, dbNode, scDataType, isTristate, trigger, True, allow2StateTypes = True)
    
  def scPortType(self):
    return "carbon_sc_multiwrite_signal<" + self.scDataType().typeName() +" >"

  def generateInitializeOutput(self, userCode):
    # call base class, but tell it to use 'write' method,
    # because sc_signal has no initialize()
    return ScInoutPort.generateInitializeOutput(self, userCode, 'write')

class ScClock(ScInPort):
  def __init__(self, name, cfgPort, dbNode, trigger):
    ScInPort.__init__(self, name, cfgPort, dbNode, ScBoolType(1), False, trigger, False)
    self.__cfgPort = cfgPort

  def generateConstructorParms(self):
    return '''\
%(name)s("%(name)s", %(period)f, %(periodUnits)s, %(dutyCycle)f, %(startTime)f, %(startTimeUnits)s, %(posedge)s)\
''' % { 'name' : self.scName(),
        'period' : self.__cfgPort.period(),
        'periodUnits' : self.__cfgPort.periodUnits(),
        'dutyCycle' : self.__cfgPort.dutyCycle(),
        'startTime' : self.__cfgPort.startTime(),
        'startTimeUnits' : self.__cfgPort.startTimeUnits(),
        'posedge' : self.__posedge() }

  def scPortType(self):
    return "sc_clock"

  def __posedge(self):
    if self.__cfgPort.initialValue() == 0:
      return "true"
    else:
      return "false"

class TriggerNoEdges:
  def runSchedule(self): return False
  def isEdgeSensitive(self): return False
  def sensitive(self, signal): return 'sensitive << %s' % signal
  def userCodeTag(self, signal): return 'INPUT ' + signal

class TriggerBothEdges:
  def runSchedule(self): return True
  def isEdgeSensitive(self): return False
  def sensitive(self, signal): return 'sensitive << %s' % signal
  def userCodeTag(self, signal): return 'INPUT ' + signal

class TriggerPosedge:
  def runSchedule(self): return True
  def isEdgeSensitive(self): return True
  def sensitive(self, signal): return 'sensitive << %s.pos()' % signal
  def userCodeTag(self, signal): return 'INPUT POSEDGE ' + signal
  def edge(self): return 'posedge'
  def value(self): return 1

class TriggerNegedge:
  def runSchedule(self): return True
  def isEdgeSensitive(self): return True
  def sensitive(self, signal): return 'sensitive << %s.neg()' % signal
  def userCodeTag(self, signal): return 'INPUT NEGEDGE ' + signal
  def edge(self): return 'negedge'
  def value(self): return 0

class TriggerPosedgeClk:
  def runSchedule(self): return True
  def isEdgeSensitive(self): return True
  def sensitive(self, signal): return 'sensitive << %s.posedge_event()' % signal
  def userCodeTag(self, signal): return 'INPUT POSEDGE ' + signal
  def edge(self): return 'posedge'
  def value(self): return 1

class TriggerNegedgeClk:
  def runSchedule(self): return True
  def isEdgeSensitive(self): return True
  def sensitive(self, signal): return 'sensitive << %s.negedge_event()' % signal
  def userCodeTag(self, signal): return 'INPUT NEGEDGE ' + signal
  def edge(self): return 'negedge'
  def value(self): return 0


# This class is a data container for all the components of a SystemC model.
# So it stores things like all the ports, registers, and memories in the
# model. The ports can be signal or transactor ports
#
# This is the base configuration. Dependending on the target a
# derived class can change some of the configuration elements.
class ScConfig:
  def __init__(self, module, ccfgFile, mode):
    # Store constructor inputs
    self.__module = module
    self.__cfg = Carbon.Cfg.Config(ccfgFile, '', mode)
    self.__db = Carbon.DB.DBContext(self.__cfg.ioDbFile())
    self.__dbFile = self.__cfg.ioDbFile()
    self.__scModuleName = self.__cfg.compNameUnaltered()

    # The following members store the SystemC component information
    # They are populated below
    self.__portsCreated = {}
    self.__scMemories = []  # list of memories
    self.__scRegisters = [] # list of registers
    self.__scPorts = []     # ordered list of ScPort for module port list
    self.__scSignals = []   # list of ScPort for observe/deposit signals
    self.__scOutputs = []   # list of outputs
    self.__scClockOutputs = []    # list of output clocks
    self.__regGroupSize = {}
    self.__regGroups = {}   # register groups hashtable
    self.__regInitNets = {} # the carbon objects accessed by registers
    self.__scTies = []      # tie offs
    self.__transactors = [] # Transactor instances

    # Gather the tie nets
    for tie in self.__cfg.ties():
      self.addTie(tie)

    # Gather the set of sc_clocks
    for scClock in self.__cfg.scClocks():
      self.addScClock(scClock)

    # and ESL ports
    for cfgPort in self.__cfg.ports():
      self.addSignalPort(cfgPort)

    # Gather the set of xtor ports.
    error = False
    for cfgXtor in self.__cfg.transactors():
      xtor = self.addTransactor(cfgXtor)
      if xtor:
        for cfgPort in cfgXtor.inputs():
          self.addTransactorScPort(xtor, cfgPort)
        for cfgPort in cfgXtor.outputs():
          self.addTransactorScPort(xtor, cfgPort)
        for cfgPort in cfgXtor.unconnected():
          self.addTransactorUnconnected(xtor, cfgPort)

      # Check for errors, if there are any, raise and exception
      if xtor.hasErrors():
        error = True

    # Gather the set of debug elements (registers, memories, etc) from the ccfg file
    for reg in self.__cfg.debugRegs():
      self.addRegister(reg)
    for mem in self.__cfg.debugMemories():
      self.addMemory(mem)

    # Run any post initialization steps
    self.postInit()

    # Cause an exception at the end so we get all config errors
    if error:
      raise Exception

  # Function to add a tie (overload if needed)
  def addTie(self, tie):
    if self.tieDefined(tie.carbonName()) == False:
      self.__scTies.append(tie)

  # Function to add a signal port (overload if needed)
  def addSignalPort(self, cfgPort):
    self.addScPort(cfgPort, cfgPort.name(), "signal")

  # Function to add a transactor SC port (overload if needed)
  def addTransactorScPort(self, xtor, cfgPort):
    return

  # Function to add a transactor unconnected port (overload if needed)
  def addTransactorUnconnected(self, xtor, cfgPort):
    return

  # Function to add a register (overload if needed)
  def addRegister(self, reg):
    return

  # Function to add a memory (overload if needed)
  def addMemory(self, mem):
    return

  # Function to add a transaction port if needed. Returns None if the
  # transaction port should not be added. It returns a valid python
  # value to be passed to addTransactorScPort and
  # addTransactorUnconnected if it should be added.
  def addTransactor(self, xtor):
    return None

  # Function to run any post initialization steps
  def postInit(self):
    # Organize the registers by group name
    for reg in self.__cfg.debugRegs():
      groupName = "Registers" + str(reg.width())
      reg.putGroup(self.__cfg, groupName)
      self.__regGroups[groupName] = reg.width()
      currCount = 1
      if self.__regGroupSize.has_key(groupName):
        currCount = self.__regGroupSize[groupName] + 1
      self.__regGroupSize[groupName] = currCount

  # Helper function to add a register to the list
  def appendRegister(self, reg):
    self.__scRegisters.append(reg)

  # Helper function to add a memory to the list
  def appendMemory(self, mem):
    self.__scMemories.append(mem)

  # Helper function to add a transaction port
  def appendTransactor(self, xtor):
    self.__transactors.append(xtor)

  # Helper function to Create the port from a ccfg port
  def addScPort(self, cfgPort, portName, portType, export=True):
    # if we have already created this port, then return
    if self.__portsCreated.has_key(portName):
      return self.__portsCreated[portName]

    # Find the net for this config port so we can gather more data
    net = self.__db.findNode(cfgPort.carbonName())
    if net == None:
      raise Exception("Could not find net '%s' in db" % cfgPort.carbonName())

    # lookup the class that represents the given type. Note that the bit
    # size is stored in different places for RTL nets connected to
    # pure signal ports vs transactor signal ports
    scTypeName = cfgPort.typedef()
    if portType == "signal":
      bitSize = cfgPort.width()
    elif portType == "transactor":
      # Some transactor ports have no size, they rely on the RTL. Others are fixed. We can
      # tell because the cfgPort width is 0 for RTL sized ports
      bitSize = net.getBitSize()
      if (cfgPort.width() != 0) and (cfgPort.width() != net.getBitSize()):
        raise Exception("Mismatched port width for transactor port: '%s' (%d), RTL port: '%s' (%d)" % \
                        (cfgPort.name(), cfgPort.width(), cfgPort.carbonName(), bitSize))
    else:
      raise Exception("Invalid port type '%s' for net '%s'" % (portType, cfgPort.carbonName()))
    scDataType = self.__getScType(scTypeName, bitSize)
    if scDataType == None:
      raise Exception("Unsupported SystemC type: %s for port %s" % (scTypeName, portName))

    # Determine the port name and whether we should treat this as
    # tri-state. For some reason constants can sometimes be tristates
    # so ignore them.
    isTristate = (net.isTristate() and not net.isConstant()) or net.isPrimaryBidi()
    portCppName = self.__module.getCppName(portName)
    trigger = self.__trigger(net, False)
      
    # create port object
    if net.isPrimaryInput():
      port = Carbon.SystemC.config.ScInPort(portCppName, cfgPort, net, scDataType, isTristate, trigger, export)
    elif net.isPrimaryOutput():
      port = Carbon.SystemC.config.ScOutPort(portCppName, cfgPort, net, scDataType, isTristate, export)
      self.__sSortOutputs(net, port.scName(), port)
    elif net.isPrimaryBidi():
      port = Carbon.SystemC.config.ScInoutPort(portCppName, cfgPort, net, scDataType, isTristate, trigger,
                                               export)
      self.__sSortOutputs(net, port.scName(), port)
    elif net.isScObservable() and net.isScDepositable():
      port = Carbon.SystemC.config.ScDepositableAndObservable(portCppName, cfgPort, net, scDataType, isTristate,
                                                              TriggerBothEdges())
      self.__sSortOutputs(net, port.scName(), port)
    elif net.isScDepositable():
      port = Carbon.SystemC.config.ScDepositable(portCppName, cfgPort, net, scDataType, isTristate,
                                                 TriggerBothEdges())
    elif net.isScObservable():
      port = Carbon.SystemC.config.ScObservable(portCppName, cfgPort, net, scDataType, isTristate)
      self.__sSortOutputs(net, port.scName(), port)
    else:
      raise Exception("net %s is not a known port" % net.leafName())

    # Remember this port
    self.__portsCreated[portName] = port
    self.__scPorts.append(port)
    return port

  def __trigger(self, net, isClock):
    # The trigger determines if we will call carbonSchedule when this
    # signal changes.Due to bug 7041, we always use TriggerBothEdges
    # for all deposit signals.
    isReset = net.isAsyncPosReset() or net.isAsyncNegReset()
    if net.isBothEdgeTrigger() or net.isTriggerUsedAsData() or isReset:
      trigger = TriggerBothEdges()
    elif net.isPosedgeTrigger():
      if isClock:
        trigger = TriggerPosedgeClk()
      else:
        trigger = TriggerPosedge()
    elif net.isNegedgeTrigger():
      if isClock:
        trigger = TriggerNegedgeClk()
      else:
        trigger = TriggerNegedge()
    else:
      trigger = TriggerNoEdges()
    return trigger

  def __getScType(self, typeName, bitSize):
    """
    Create an object representing the named SystemC type.
    """
    scType = None
    if typeName == 'bool':
      scType = Carbon.SystemC.config.ScBoolType(bitSize)
    elif typeName == 'sc_uint':
      scType = Carbon.SystemC.config.ScUintType(bitSize)
    elif typeName == 'sc_int':
      scType = Carbon.SystemC.config.ScIntType(bitSize)
    elif typeName == 'sc_biguint':
      scType = Carbon.SystemC.config.ScBigUintType(bitSize)
    elif typeName == 'sc_bigint':
      scType = Carbon.SystemC.config.ScBigIntType(bitSize)
    elif typeName == 'sc_logic':
      scType = Carbon.SystemC.config.ScLogicType(bitSize)
    elif typeName == 'sc_lv':
      scType = Carbon.SystemC.config.ScLvType(bitSize)
    elif typeName == 'sc_bv':
      scType = Carbon.SystemC.config.ScBvType(bitSize)
    elif typeName in ('unsigned char', 'uchar'):
      scType = Carbon.SystemC.config.ScUnsignedCharType(bitSize)
    elif typeName == 'char':
      scType = Carbon.SystemC.config.ScCharType(bitSize)
    elif typeName in ('unsigned int', 'uint', 'unsigned'):
      scType = Carbon.SystemC.config.ScUnsignedIntegerType(bitSize)
    elif typeName  == 'int':
      scType = Carbon.SystemC.config.ScIntegerType(bitSize)
    elif typeName in ('unsigned long', 'unsigned long int', 'ulong'):
      scType = Carbon.SystemC.config.ScUnsignedLongType(bitSize)
    elif typeName in ('long int', 'long'):
      scType = Carbon.SystemC.config.ScLongType(bitSize)
    return scType

  # Helper function to create and add sc_clock object
  def addScClock(self, scClock):
    # Find the RTL node for it
    dbNode = self.__db.findNode(scClock.carbonName())
    if dbNode == None:
      raise Exception("Could not find net '%s' in db" % scClock.carbonName())

    # Add it to the clocks
    portName = self.__module.getCppName(scClock.leafName())
    trigger = self.__trigger(dbNode, True)
    self.__scPorts.append(ScClock(portName, scClock, dbNode, trigger))

  # helper function to determine if an rtl name is tied
  def tieDefined(self, tieName):
    for tie in self.__scTies:
      if tieName == tie.carbonName():
        return True
    return False
    
  def ccfg(self):
    return self.__cfg

  def module(self):
    return self.__module

  def scModuleName(self):
    return self.__scModuleName
  
  def dbFile(self):
    return self.__dbFile

  def getDB(self):
    return self.__db

  def scPorts(self):
    return self.__scPorts
  
  def scMemories(self):
    return self.__scMemories
  
  def scRegisters(self):
    return self.__scRegisters

  def transactors(self):
    return self.__transactors
  
  def scOutputs(self):
    return self.__scOutputs

  def scClockOutputs(self):
    return self.__scClockOutputs

  def scTies(self):
    return self.__scTies

  def __sSortOutputs(self, net, netName, port):
    if net.isClk():
      addIt = not net.isPrimaryInput()
      for p in self.__scClockOutputs:
        if netName == p.scName():
          addIt = False
          break
      if (addIt):
        self.__scClockOutputs.append(port)
    else:
      addIt = True
      for p in self.__scOutputs:
        if netName == p.scName():
          addIt = False
          break
      if (addIt):
        self.__scOutputs.append(port)

# The base SystemC configuration
class ScConfigSystemC(ScConfig):
  def __init__(self, module, ccfgFile):
    ScConfig.__init__(self, module, ccfgFile, Carbon.Cfg.eCarbonXtorsSystemC)

  # Overload the addTransactorScPort function
  def addTransactorScPort(self, xtor, cfgPort):
    scPort = self.addScPort(cfgPort, cfgPort.carbonName().split('.')[-1], "transactor", export=False)
    xtor.addScPort(scPort)

  # Overload the addTransactorUnconnected function
  def addTransactorUnconnected(self, xtor, cfgPort):
    xtor.addUnconnected(cfgPort)

  # Overload the addTransactor function. Add it if it is an AMBA-TLM2 transactor
  def addTransactor(self, xtor):
    if xtor.isAmba():
      if xtor.layerId() == 'pv':
        xtorPort = Carbon.SystemC.transactors.TLM2PVTransactor(xtor.name(), self.ccfg(), xtor)
      else:
        xtorPort = Carbon.SystemC.transactors.TLM2Transactor(xtor.name(), self.ccfg(), xtor)
      self.appendTransactor(xtorPort)
      return xtorPort
    else:
      return None

# CoWare version of a configuration
class ScConfigCoWare(ScConfig):
  def __init__(self, module, ccfgFile):
    ScConfig.__init__(self, module, ccfgFile, Carbon.Cfg.eCarbonXtorsCoware)

  # Overload the addTransactor function because we want them added to the ScConfig
  def addTransactor(self, xtor):
    # Create a CoWareTransactor wrapper so that we emit the right commands in the import script
    result = Carbon.SystemC.transactors.CoWareTransactor(xtor.name(), self.ccfg(), xtor)
    self.appendTransactor(result)
    return result

  # Overload the addTransactorScPort function
  def addTransactorScPort(self, xtor, cfgPort):
    self.addScPort(cfgPort, cfgPort.carbonName().split('.')[-1], "transactor")
    return

  # Overload the addRegister function
  def addRegister(self, reg):
    # Create the scml wrapper around the register
    scmlReg = None
    if reg.width() == 32:
      scmlReg = Carbon.SystemC.ScmlRegisters.ScmlRegister32(self.module(), reg)
    elif reg.width() == 64:
      scmlReg = Carbon.SystemC.ScmlRegisters.ScmlRegister64(self.module(), reg)
    elif reg.width() == 16:
      scmlReg = Carbon.SystemC.ScmlRegisters.ScmlRegister16(self.module(), reg)
    elif reg.width() == 8:
      scmlReg = Carbon.SystemC.ScmlRegisters.ScmlRegister8(self.module(), reg)

    # Use the base class to add the register
    if scmlReg != None:
      ScConfig.appendRegister(self, scmlReg)
    else:
      raise ScmlError('Unsupported Register Width ' + str(reg.width()) + ' bit(s) for Register ' + reg.name() + ' Registers must be 32 or 64 bits only')

  # Overload the addMemory function
  def addMemory(self, mem):
    # Create the scml wrapper around the memory
    scmlMem = None
    if mem.width() <= 32:
      scmlMem = Carbon.SystemC.ScmlMemories.ScmlMemory32(self.module(), mem)
    elif mem.width() <= 64:
      scmlMem = Carbon.SystemC.ScmlMemories.ScmlMemory64(self.module(), mem)

    # Use the base class to add the memory
    if scmlMem != None:
      ScConfig.appendMemory(self, scmlMem)
    else:
      raise ScmlError('Unsupported Memory Width=' + str(mem.width()) + " bit(s) for Memory " + mem.name() + ' Memories must be 1 to 512 bits')

  # Overload the post initialization step
  def postInit(self):
    # Print status messages for the final size of the registers and memories
    for memWidth in [32, 64]:
      print "%d bit Memories: %d" % (memWidth, self.numScmlMemories(memWidth))
    for regWidth in [8, 16, 32, 64]:
      print "%d bit Registers: %d" % (regWidth, self.numScmlRegisters(regWidth))

    # Run the base class post init - this organizes the registers
    ScConfig.postInit(self)

  def numScmlMemories(self, size):
    count = 0
    for mem in self.scMemories():
      if mem.size() == size:
        count=count+1
    return count

  def numScmlRegisters(self, size):
    count = 0
    for reg in self.scRegisters():
      if reg.size() == size:
        count=count+1
    return count

