import Carbon
import Carbon.DB
import Carbon.Cfg
import Carbon.SystemC.ScmlMemories
import Carbon.SystemC.ScmlRegisters
import Carbon.userCode
import Carbon.outputStream
import os
import os.path
import sys
import re
import Carbon.SystemC.config
import Carbon.SystemC.generator
from Carbon.carboncfg import *

class ScmlError(Exception):
  pass

###############################################################################
class ScModule:
  def __init__(self, dbFile, ccfgFile, portTypeMap, defineMap, moduleName='', useStandAlone = False, saName = '', scUseComponentName = False, genCowareBuild = False):

    # Use to make unique and valid C++ names
    self.__cppNames = {}

    # Map to help in the dbFile to ccfg conversion process
    self.__alreadyWarned = {}

    # The systemCWrapper.py file makes sure that we have valid values
    # for dbFile and ccfgFile. Here we unify the flows by always
    # creating a ccfg file from a db file. After that we always work
    # off a ccfg.
    #
    # So if this is a db file build, first create a ccfg file from the
    # db file.
    if (ccfgFile == '') and (dbFile != None):
      ccfgFile = self.createCcfgFromDB(dbFile, portTypeMap, moduleName)

    # Configuration information. This builds up all the components that make up the
    # final set of pieces that we are going to emit.
    if genCowareBuild:
      self.__config = Carbon.SystemC.config.ScConfigCoWare(self, ccfgFile)
    else:
      self.__config = Carbon.SystemC.config.ScConfigSystemC(self, ccfgFile)

    # The following are names that we use to uniquify the emitted code
    if scUseComponentName:
      ifaceName = saName
    else:
      ifaceName = self.config().getDB().getInterfaceName()

    # Args to put in header file
    args = ''
    if len(dbFile) > 0:
      args += dbFile
    else:
      args += ' -ccfg ' + ccfgFile

    # Generation class for the three types of generation (SystemC, StandAlone, and CoWare)
    if useStandAlone:
      self.__generator = Carbon.SystemC.generator.ScGeneratorSA(self.__config, args, ifaceName, saName)
    elif genCowareBuild:
      self.__generator = Carbon.SystemC.generator.ScGeneratorCoWare(self.__config, args, ifaceName,
                                                                    defineMap)
    else:
      self.__generator = Carbon.SystemC.generator.ScGeneratorSystemC(self.__config, args, ifaceName)

  def config(self):
    return self.__config

  def generate(self):
    # Initialize the objects and then generate the code. It must be done in this order so we
    # don't get circular depenenices due to objects not being constructed while emitting code
    self.__generator.init()
    return self.__generator.generate()

  def hUserCode(self):
    return self.__generator.hUserCode()

  def cppUserCode(self):
    return self.__generator.cppUserCode()

  # Create a section name for h/cpp pre/post
  def userCodeSection(self, file, position, section):
    return self.__generator.userCodeSection(file, position, section)

  def scModuleName(self):
    return self.__config.scModuleName()

  # convert things like Verilog escaped identifiers to valid C++ names
  def getCppName(self, hdlName, uniquify=True):
    name = ''

    # replace all questionable chars with _
    for c in hdlName:
      code = ord(c)
      if code >= ord('a') and code <= ord('z'):
        name += c
      elif code >= ord('A') and code <= ord('Z'):
        name += c
      elif code >= ord('0') and code <= ord('9'):
        name += c
      else:
        name += '_'
    
    if uniquify:
      # if name is not unique, keep appending a number to it until it is
      if self.__cppNames.has_key(name):
        i = 0
        while True:
          newName = name + '_' + str(i)
          if not self.__cppNames.has_key(newName):
            name = newName
            break
      self.__cppNames[name] = True
    return name
  
  # Helper function to filter out any nets that we cannot map to SystemC
  def validScNets(self, netIter):
    for net in netIter:
      name = net.fullName()
      if net.isStruct():
        if not self.__alreadyWarned.get(name, False):
          self.__alreadyWarned[name] = True
          print 'Warning: Net %s is a composite. Composite ports are not supported.' % name
      elif net.isContainedByComposite():
        if not self.__alreadyWarned.get(name, False):
          self.__alreadyWarned[name] = True
          if net.isPrimaryInput() or net.isPrimaryOutput() or net.isPrimaryBidi():
            print 'Warning: Primary port %s is contained in a composite. Composite ports are not supported.' % name
          elif net.canBeCarbonNet():
            yield net
      elif not net.canBeCarbonNet():
        if not self.__alreadyWarned.get(name, False):
          self.__alreadyWarned[name] = True
          print 'Warning: Net %s is not a valid Net.' % name
      else:
        yield net
          
  # Create a ccfg from a db file for backwards compatabilty
  def createCcfgFromDB(self, dbFile, portTypeMap, moduleName):
    # Create a ccfg and associate the db with it
    ccfg = carbonCfgCreate()
    carbonCfgPutIODBFile(ccfg, dbFile)
    carbonCfgPutCcfgMode(ccfg, eCarbonCfgSYSTEMC);

    # Open the db and transfer some of the pertinent information:
    # component name, library name, top module name
    #
    # Note that the component name comes from one of three places in
    # the following priority order:
    #
    # 1. The module name passed in using the -moduleName switch.
    # 2. The cbuild -systemCModuleName switch.
    # 3. The top module name
    #
    # If the user did not use the -systemCModuleName switch, the top
    # module name is put into that I/O DB field. So using
    # db.getSystemCModuleName() takes care of the 2nd and 3rd case.
    db = Carbon.DB.DBContext(dbFile)
    xfaceName = db.getInterfaceName()
    carbonCfgPutLibName(ccfg, "lib%s.a" % xfaceName)
    topModuleName = db.getTopLevelModuleName()
    carbonCfgPutTopModuleName(ccfg, topModuleName)
    if moduleName == '':
      moduleName = self.getCppName(db.getSystemCModuleName(), uniquify=False)
    carbonCfgPutCompName(ccfg, moduleName)
    
    # Walk the set of nets that need to be SystemC ports (order matters for some reason)
    for netIter in (db.iterPrimaryPorts(), db.iterScDepositable(), db.iterScObservable()):
      for net in self.validScNets(netIter):
        self.addPort(ccfg, net, portTypeMap)
                    
    # Write out the ccfg for reading back into the ScConfig class
    ccfgFile = "lib%s.gen.ccfg" % xfaceName
    carbonCfgWrite(ccfg, ccfgFile)
    print "Wrote: %s" % ccfgFile
    return ccfgFile

  def isTopLevelPort(self, net):
    return net.isPrimaryInput() or net.isPrimaryOutput() or net.isPrimaryBidi()

  def addPort(self, ccfg, net, portTypeMap):
    # Figure out the esl name
    # For deposit/observe signals, use the full path to make them unique
    eslName = net.leafName()
    if not self.isTopLevelPort(net):
      eslName = '_'.join(net.fullName().split('.')[1:])

    # Check if it was already created
    if carbonCfgFindESLPort(ccfg, eslName):
      return

    # Figure out the RTL type
    if net.isPrimaryBidi():
      rtlType = eCarbonCfgRTLInout
    elif net.isPrimaryOutput():
      rtlType = eCarbonCfgRTLOutput
    elif net.isPrimaryInput():
      rtlType = eCarbonCfgRTLInput
    elif net.isScObservable():
      rtlType = eCarbonCfgRTLOutput
    elif net.isScDepositable():
      rtlType = eCarbonCfgRTLInput
      
    # And the ESL type
    if net.isPrimaryBidi() or (net.isScObservable() and net.isScDepositable()):
      eslType = eCarbonCfgESLInout
    elif net.isPrimaryInput() or net.isScDepositable():
      eslType = eCarbonCfgESLInput
    elif net.isPrimaryOutput() or net.isScObservable():
      eslType = eCarbonCfgESLOutput
    else:
      raise Exception("net %s is not a known port" % net.leafName())

    # Create it
    rtlPort = carbonCfgAddRTLPort(ccfg, net.fullName(), net.getWidth(), rtlType)
    eslPort = carbonCfgAddESLPort(ccfg, rtlPort, eslName, eslType)

    # Set the sc type. This occurs in one of two cases:
    #
    # 1. Set it from the user port map. If we don't set it then the
    #    default is used by the ccfg processing. This used to be
    #    duplicated between Python and C++
    # 2. We need to treat this net as a tri-state. The
    #    current default computation does not take this
    #    into account because it doesn't have access to
    #    the db node. This should be fixed in one
    #    place at some point
    isTristate = (net.isTristate() and not net.isConstant()) or net.isPrimaryBidi()
    if portTypeMap.has_key(eslName):
      scTypeName = portTypeMap[eslName]
      carbonCfgESLPortPutTypeDef(eslPort, scTypeName)
    elif isTristate:
      if net.getWidth() == 1:
        carbonCfgESLPortPutTypeDef(eslPort, "sc_logic")
      else:
        carbonCfgESLPortPutTypeDef(eslPort, "sc_lv")
