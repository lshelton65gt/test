# Class to represent a transaction port in a SystemC design
class Transactor:
  def __init__(self, name, config, xtor):
    self.__name = name
    self.__config = config
    self.__xtor = xtor
    self.__scPorts = []
    self.__unconnected = {}

    # Assume there are no errors. If there are any we print the message
    # immediately and then raise the exception at the end
    #
    # Note, all errors must occur during configuration since that
    # is when they are detected
    self.__error = False

  # Add the SystemC port associated with a pin on this Transactor
  def addScPort(self, scPort):
    self.__scPorts.append(scPort)

  # Add the unconnected xtor ports
  def appendUnconnected(self, name, declType):
    self.__unconnected[name] = declType

  # Iterate over the list of SC ports
  def scPorts(self):
    for scPort in self.__scPorts:
      yield scPort

  # Iterate over the list of unconnected ports
  # This returns value pairs
  def unconnected(self):
    for (portName, scDataType) in self.__unconnected.items():
      yield (portName, scDataType)

  # Iterate over the list of RTL ports
  def rtlPorts(self):
    for port in self.xtor().inputs():
      yield port
    for port in self.xtor().outputs():
      yield port
    for port in self.xtor().unconnected():
      yield port
    
  # Iterate over the connected RTL ports
  def connectedRTLPorts(self):
    for port in self.xtor().inputs():
      yield port
    for port in self.xtor().outputs():
      yield port

  # Find an RTL port by name
  def findRTLPort(self, portName):
    for port in self.rtlPorts():
      if port.name() == portName:
        return port
    return None

  # Return the xtor
  def xtor(self):
    return self.__xtor

  # Use RTL width as the port width for the transactor port
  def getPortWidth(self, port):
    return port.rtlWidth()

  # Helper function to access config
  def config(self):
    return self.__config

  # Helper function to return the port name
  def name(self):
    return self.__name

  # Return whether there are any errors for this transactor
  def hasErrors(self):
    return self.__error

  # Print an error message
  def errorMsg(self, msg):
    print msg
    self.__error = True

# Class for a TLM2 SystemC Transactor
class TLM2Transactor(Transactor):
  """TLM2Transactor represents ports used for TLM2 and more specifically the AMBA TLM2"""
  def __init__(self, name, config, xtor):
    Transactor.__init__(self, name, config, xtor)

    # Walk through the port width template parameters and create
    # a map from ports to their width. This is so we can handle
    # unconnected ports correctly. The issue is that if some of the
    # ports that are associated with a template parameter are
    # unconnected, they have to have a consistent size with the
    # connected ports. And since that size is determined by the RTL,
    # we have to match. This map is also used for the template
    # code below
    self.__dataWidth = 0
    self.__templatePortWidths = {}
    for templateArg in xtor.templateArgs():
      if templateArg.isPortWidth():
        # Figure out the port width for this class of ports
        if templateArg.default() != "":
          portWidth = int(templateArg.default())
        else:
          portWidth = 0
        for portName in templateArg.value().split():
          rtlPort = self.findRTLPort(portName)
          if rtlPort != None:
            width = self.getPortWidth(rtlPort)
            if width > 0:
              portWidth = width
              break

        # Populate the map
        for portName in templateArg.value().split():
          self.__templatePortWidths[portName] = portWidth

        # If this is the data width template argument, also set the width
        if templateArg.busDataWidth():
          self.__dataWidth = portWidth

    # We have to have a data width or things won't work right
    if self.__dataWidth == 0:
      self.errorMsg("Error: unable to determine the bus port width; at least one data port must be connected.")

    # create name for this adaptor instance
    self.__className = "%s<" % xtor.className()
    for arg in xtor.templateArgs():
      # add template arguments onto class name
      if not self.__className.endswith("<"):
        # add a comma if this is not the first template argument
        self.__className += ","
      if arg.isClass():
        self.__className += "%s" % self.config().compName()
      elif arg.isPortWidth():
        # Use the first port name populated in our map above
        portName = arg.value().split()[0]
        self.__className += "%d" % self.__templatePortWidths[portName]
      elif arg.isText():
        self.__className += arg.value()
    self.__className += ">"
    
    # determine socket type
    self.__xtorType = "master"
    if self.xtor().isSlave():
      self.__xtorType = "slave"

    # Validate that we have a clock and reset for this
    # transactor. These are required currently. We might be able to
    # make reset optional by connecting it to constant 1.
    if self.xtor().eslClockMaster() == "":
      self.errorMsg("Error: clock must be specified to run the TLM2/RTL transactor for '%s'" % self.name())
    if self.xtor().eslResetMaster() == "":
      self.errorMsg("Error: reset must be specified to run the TLM2/RTL transactor for '%s'" % self.name())

  def xtorType(self):
    return self.__xtorType

  def dataWidth(self):
    return self.__dataWidth

  # Returns the type for the transactor TLM2 socket (opposite side of the RTL)
  def socketType(self):
    return "amba::amba_%(type)s_socket<%(dataWidth)s>" % { 'type' : self.xtorType(),
                                                           'dataWidth' : self.dataWidth() }

  # Returns the name of the transactor TLM2 socket
  def socketName(self):
    return "%(name)s_socket" % { 'name' : self.name() }

  def isInputPort(self, name):
    port = self.findRTLPort(name)
    if port != None:
      if port in self.xtor().inputs():
        return True
    return False

  def className(self):
    return self.__className

  def instanceName(self):
    return self.name()

  def addUnconnected(self, cfgPort):
    # Figure out the width
    if cfgPort.width() > 0:
      portWidth = cfgPort.width()
    elif cfgPort.widthExpr() != "":
      portWidthExpr = eval(cfgPort.widthExpr() % { 'busDataWidth' : self.dataWidth() })
      portWidth = int(portWidthExpr)
    elif self.__templatePortWidths.has_key(cfgPort.name()):
      portWidth = self.__templatePortWidths[cfgPort.name()]
    else:
      self.errorMsg("Error: unconnected port '%s' for transactor '%s' has an unknown width; It can't be left unconnected." % \
                    (cfgPort.name(), self.name()))

    # Figure out the data type
    if portWidth == 1:
      dataType = "bool"
    elif (portWidth > 1) and (portWidth <= 64):
      dataType = "sc_uint<%d> " % portWidth
    elif (portWidth > 64):
      dataType = "sc_biguint<%d> " % portWidth

    # Add it to the set of unconnected ports
    self.appendUnconnected(cfgPort.name(), "sc_signal<%s>" % dataType)

  def instanceDeclaration(self):
    # Return the code to instantiate the transactor
    code = """%(className)s* %(instanceName)s;   // transactor %(name)s\
""" % { 'name': self.name(), 'instanceName': self.instanceName(), 'className': self.className() }
    return code

  def socketDecl(self):
    """name of amba socket instance"""
    return """%(socketType)s %(socketName)s;\
""" % { 'socketType': self.socketType(), 
        'socketName': self.socketName()
        }

  def deleteAdapter(self):
    """code to delete the adapter (goes in the owning components destructor)"""
    code = """delete %(instanceName)s""" % { 'instanceName': self.instanceName() }
    return code

  def initAdapter(self):
    """code to initialize the adapter when initialized through an initialization list"""

    # Add any constructor args
    extraAdapterArgs = ''
    if self.xtor().constructorArgs() != '':
      extraAdapterArgs = ', %s' % self.xtor().constructorArgs()
    
    # Create code to init the transactor
    code = '''\
%(instanceName)s(new %(className)s("amba_adapter_%(instanceName)s"%(extraAdapterArgs)s))\
''' % { 'instanceName': self.instanceName(),
         'className': self.className(),
         'extraAdapterArgs': extraAdapterArgs
         }
    return code

  def initSocket(self):
    """code to initialize the amba socket when initialized through an initialization list"""
    code = '''%(socketName)s("%(socketName)s"%(initSocketArgs)s)\
''' % { 'socketName': self.socketName(),
        'initSocketArgs': self.initSocketArgs()
        }
    return code

  # The base version init socket args. Overload to change them
  def initSocketArgs(self):
    return '''\
, amba::amba_%(protocol)s, amba::amba_%(layerId)s, false\
''' % { 'protocol': self.xtor().protocolName(),
        'layerId': self.xtor().layerId()
        }

  def connectAdapterSocket(self):
    """connect the adapter's internal socket to the exposed external socket"""
    if self.xtorType() == "master":
      return """\
%(name)s->master_sock.bind(%(socketName)s);""" % { 'name' : self.name(), 'socketName': self.socketName() }
    else:
      return """\
%(socketName)s.bind(%(name)s->slave_sock);""" % { 'name' : self.name(), 'socketName': self.socketName() }

  def connectSignal(self, sigPort):
    return self.connectSignalPort(sigPort.name(), sigPort.leafName())

  def connectSignalPort(self, portName, sigName):
    return """%(name)s->m_%(portName)s(%(sigName)s);
""" % { 'name': self.name(),
        'portName' : portName,
        'sigName': sigName }

  def connectSignals(self):
    # Connect the clock and rest masters if they exist
    statements = []
    statements.append(self.connectSignalPort("clk", self.xtor().eslClockMaster()))
    statements.append(self.connectSignalPort("Reset", self.xtor().eslResetMaster()))

    # Connect the ports
    for rtlPort in self.connectedRTLPorts():
      statements.append(self.connectSignal(rtlPort))
    return statements

  def registerTimingListeners(self, compName, otherXtor):
    """register timing listener methods"""
    return """%(name)s->register_%(otherXtorType)s_timing_listener(this, &%(compName)s::%(xtorType)s_timing_listener_%(name)s);\
    """ % { 'name': self.name(),
            'otherXtorType': otherXtor.xtorType(),
            'xtorType': self.xtorType(),
            'compName': compName
          }

  def defineTimingListener(self, timingMap):
    """timing listener method definitions"""
    return """
  void %(xtorType)s_timing_listener_%(name)s(gs::socket::timing_info info)
  {
    %(timingAlgo)s
  }
    """ % { 'xtorType': self.xtorType()
          , 'name': self.name()
          , 'timingAlgo': self.timingAlgo(timingMap)
          }

  def timingFunc(self):
    """timing listener function name"""
    return "set_%(xtorType)s_timing" % { 'xtorType': self.xtorType() }   

  def timingAlgo(self, timingMap):
    code = ''
    listener = {}
    for scPort in self.scPorts():
      if timingMap.has_key(scPort):
        for (fanoutXtor, fanoutScPort) in timingMap[scPort]:
          startShort = scPort.portName()
          endShort = fanoutScPort.portName()
          bphases = self.getPhases(startShort)
          ephases = fanoutXtor.getPhases(endShort)
          for bphase in bphases:
            for ephase in ephases:
              if not bphase == "*" and not ephase == "*":
                tmp = """%(xtorName)s->%(timingFunc)s(%(ephase)s, info.get_start_time(%(bphase)s));""" % { 'xtorName': fanoutXtor.name()
      , 'timingFunc': fanoutXtor.timingFunc()
      , 'bphase': bphase
      , 'ephase': ephase
      }
                if not tmp in listener:
                  listener[tmp] = []
                listener[tmp].append(scPort.scName() + "->" + fanoutScPort.scName())

    for key in listener:
      for bar in listener[key]:
        code += "\n    // " + bar
      code += "\n    " + key + "\n"
         
    return code

  # Helper function to get the phases for this transactor and a given SystemC port
  def getPhases(self, shortName):
    for rtlPort in self.rtlPorts():
      if rtlPort.name() == shortName:
        return rtlPort.phaseNames()
    return ["*"]

  # Emit the code for debug access for this transactor
  def debugAccessClassDecl(self, userCodeNumHitBytesPre, userCodeNumHitBytesPost):
    if self.xtor().isSlave():
      # Create a class to pass the slave debug access to the component debug access
      code = '''\
class %(debugAccessClass)s : public CarbonDebugAccessIF
  {
  public:
    //! Constructor
    %(debugAccessClass)s(%(compName)s* parent) : mParent(parent) {}

    //! Read Debug Access
    CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
    {
       uint32_t numBytesProcessed = 0;
       return mParent->debugAccess("%(name)s", eCarbonDebugAccessRead, addr, numBytes, buf, &numBytesProcessed, ctrl);
    }

    //! Write Debug Access
    CarbonDebugAccessStatus debugMemWrite(uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
    {
       uint8_t* data = const_cast<uint8_t*>(buf);
       uint32_t numBytesProcessed = 0;
       return mParent->debugAccess("%(name)s", eCarbonDebugAccessWrite, addr, numBytes, data, &numBytesProcessed, ctrl);
    }

  private:
    %(compName)s* mParent;
  };
''' % { 'debugAccessClass': self.debugAccessClass(),
        'name': self.name(),
        'compName': self.config().compName() }
    else:
      code = '''\
class %(debugAccessClass)s : public CarbonDebugAccessIF
  {
  public:
    //! Constructor
    %(debugAccessClass)s(%(className)s* parent) : mParent(parent) {}

    //! Read Debug Access
    CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
    {
       return mParent->debugMemRead(addr, buf, numBytes, ctrl);
    }

    //! Write Debug Access
    CarbonDebugAccessStatus debugMemWrite(uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
    {
       return mParent->debugMemWrite(addr, buf, numBytes, ctrl);
    }

    //! Fill in code for this function to handle address mapping
    uint32_t numHitBytes(uint64_t addr, uint32_t numBytes)
    {
%(userCodeNumHitBytesPre)s
%(userCodeNumHitBytesPost)s
      return 0;
    }
    
  private:
    %(className)s* mParent;
  };
''' % { 'debugAccessClass': self.debugAccessClass(),
        'className': self.className(),
        'userCodeNumHitBytesPre': userCodeNumHitBytesPre,
        'userCodeNumHitBytesPost': userCodeNumHitBytesPost }
    return code

  def debugAccessMemberDecl(self):
    code = '%(debugAccessClass)s %(debugAccessMember)s;' % { 'debugAccessClass': self.debugAccessClass(),
                                                             'debugAccessMember': self.debugAccessMember() }
    return code

  def debugAccessClass(self):
    return '%(name)sDebugAccess' % { 'name': self.name() }

  def debugAccessMember(self):
    return 'm_%(name)sDebugAccess' % { 'name': self.name() }
  
  def debugAccessConstructorInit(self):
    code = ''
    if self.xtor().isSlave():
      code = '%(debugAccessMember)s(this)' % { 'debugAccessMember': self.debugAccessMember() }
    else:
      code = '%(debugAccessMember)s(%(name)s)' % { 'debugAccessMember': self.debugAccessMember(),
                                                   'name': self.name() }
    return code

  def debugAccessRegisterCB(self):
    code = ''
    if self.xtor().isSlave():
      code = '''\
%(name)s->registerDebugAccessCB(&%(debugAccessMember)s);\
''' % { 'name': self.name(),
        'debugAccessMember': self.debugAccessMember() }
    return code

  # Return code for a get monitor function if the transactor supports it
  def monitorFunction(self):
    code = ''
    if self.xtor().hasFlag("monitor"):
      code = '''\
gs::socket::monitor<%(dataWidth)d>* get_%(name)s_monitor(void) { return %(name)s->getMonitor(); }\
''' % { 'name' : self.name(),
        'dataWidth' : self.dataWidth() }
    return code

# The PV version of the transactor sockets behave slightly differently
# than the AMBA CT/LT/AT transactors. Overload the functions that
# are different
class TLM2PVTransactor(TLM2Transactor):
  """PV TLM2Transactor represents ports used for TLM2 and more specifically the AMBA TLM2"""
  def __init__(self, name, config, xtor):
    TLM2Transactor.__init__(self, name, config, xtor)

    # convert master/slave to target/initiator for pv
    self.__socketSubType = "invalid"
    if self.xtorType() == "master":
      self.__socketSubType = "initiator"
    elif self.xtorType() == "slave":
      self.__socketSubType =  "target"
    else:
      self.errorMsg("Error: invalid socket type '%s' for transactor '%s'." % (self.xtorType(), self.name()))

  # Function to return converted master/slave to target/initiator for pv
  def socketSubType(self):
    return self.__socketSubType

  # Overload the socket type function since we need to use the base tlm socket type
  def socketType(self):
    return "tlm::tlm_%(type)s_socket<%(dataWidth)s, amba_pv::amba_pv_protocol_types>" % \
           { 'type' : self.socketSubType(),
             'dataWidth' : self.dataWidth() }

  # Overload the init socket args, there aren't any for the base socket types
  def initSocketArgs(self):
    return ""

# Class for a CoWare transactor
class CoWareTransactor(Transactor):
  """CoWareTransactor represents ports used for CoWare Protocols"""
  def __init__(self, name, config, xtor):
    Transactor.__init__(self, name, config, xtor)

  # delegate to the cfg xtor for any methods we don't define
  def __getattr__(self):
    return getattr(self.xtor())

  # delete to the cfg xtor for any name methods we don't define
  def __getattr__(self, name):
    return getattr(self.xtor(), name)
