
import carbondbapi
import DBNode
import DBIter

##
# \file
#  This file contains the python wrapper over the carbon database C
#  api. It is intended to be an object-oriented and simple to
#  use.

##
#    \defgroup CarbonDBContextDoc Database Context
#    @{
#    @}

##
#    \addtogroup CarbonDBContextDoc
#    \brief The following class or classes describe general database functionality 
#    @{



## The context for the Carbon database. 
# This manages the database
# file. It contains methods to access design information including,
# but not limited to, version, signals and direction, and design name.
#

class DBContext:

    ##  The constructor for the database context.
    #
    #  \param fileName The name of the carbon database to open.
    #
    #   This will open the database and return 0 if there were any
    #   problems.
    #
    #  \retval 0 If any problems were encountered
    #  \retval DBContext object if no problems were encountered.
    def __init__(self, fileName):
        self.mObj = carbondbapi.carbonDBCreate(fileName, 1)
        
    def __del__(self):
        carbondbapi.carbonDBFree(self.mObj)


    ## Is this a valid DBContext?
    #
    # Use this in a truth statement such as
    # \verbatim
    #  db = Carbon.DB.DBContext("libdesign.io.db")
    #  if db:
    #     ...
    # \endverbatim
    # \retval 1 If this is a valid DBContext
    # \retval 0 If this is not a valid DBContext

    def __nonzero__(self):
        if self.mObj == None:
            return 0
        return 1
        
    
    ## Find a DB node by path, return NULL if on failure
    def findNode(self, hierPath):
        node = carbondbapi.carbonDBFindNode(self.mObj, hierPath)
        if node:
            return DBNode.DBNode(self.mObj, node)
        else:
            return None

    ##   Iterate all the primary ports of the design
    #    
    #    This iterates the primary ports in the design in the order
    #    they were declared
    #        
    #    \returns a DBIter.DBIter object
    def iterPrimaryPorts(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopPrimaryPorts)


    ##   Iterate all the primary inputs of the design.
    #    
    #    This iterates fully unidirectional primary inputs in the
    #    design. If a primary input vector is found to be bidirectional
    #    in part or in whole then that will appear in the iterBidis
    #    method.
    #        
    #    \returns a DBIter.DBIter object
    def iterInputs(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopPrimaryInputs)


    ##   Iterate all the primary outputs of the design.
    #
    #    This iterates fully unidirectional primary outputs in the
    #    design. If a primary output vector is found to be bidirectional
    #    in part or in whole then that will appear in the iterBidis
    #    method.
    # 
    #    \returns a DBIter.DBIter object
    def iterOutputs(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopPrimaryOutputs)

    ##   Iterate all the primary bidirects of the design.
    #
    #    \returns a DBIter.DBIter object
    def iterBidis(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopPrimaryBidis)

    ##   Iterate all the primary clocks in the design
    #    
    #    Primary clocks are top-level signals that directly drive a
    #    sequential schedule (ie., flop). If the signal passes through
    #    combinational logic prior to driving the flop then that will
    #    appear in iterClkTree.
    #    
    #    \returns a DBIter.DBIter object
    def iterPrimaryClks(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopPrimaryClks)

    ##   Iterate all the clock tree signals in the design
    #
    #    Clock tree signals are top-level signals that indirectly drive a
    #    sequential schedule (ie., flop) through combinational
    #    logic. If the signal does not pass through combinational logic
    #    prior to driving the flop then that will appear in
    #    iterPrimaryClks.
    #
    #    \returns a DBIter.DBIter object
    def iterClkTree(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopClkTree)


    ## Iterate all the depositable signals in the design
    #
    #  Depositable signals are signals that were marked with the
    #  'depositSignal' directive.
    #
    #    \returns a DBIter.DBIter object
    def iterDepositable(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopDepositable)

    ##   Iterate all the observable signals in the design
    #
    #    Observable signals are signals that were marked with the
    #    'observeSignal' directive.
    #
    #    \returns a DBIter.DBIter object
    def iterObservable(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopObservable)

    ##  Iterate all the systemc depositable signals in the design
    #   
    #
    #    SystemC depositable signals are signals that were marked with
    #    the 'scDespositSignal' directive.
    #
    #    \returns a DBIter.DBIter object
    def iterScDepositable(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopScDepositable)

    ##  Iterate all the systemc observable signals in the design
    #
    #
    #    SystemC observable signals are signals that were marked with
    #    the 'scObserveSignal' directive.
    #
    #    \returns a DBIter.DBIter object
    def iterScObservable(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopScObservable)

    ## Iterate over all the primary asynchronous signals in the design.
    #
    #
    #    A primary asynchronous signal is a signal that directly feeds
    #    a top-level output through combinational logic.
    #
    #    Note that this is the same as iterAsyncInputs(). This function
    #    is kept for backwards compatability.
    #    
    #    \returns a DBIter.DBIter object
    def iterAsyncs(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopAsyncs)

    ## Iterate over all the primary asynchronous input signals in the design.
    #
    #
    #    A primary asynchronous signal is a signal that directly feeds
    #    a top-level output through combinational logic.
    #    
    #    \returns a DBIter.DBIter object
    def iterAsyncInputs(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopAsyncs)

    ## Iterate over all the primary asynchronous outputs signals in the design.
    #
    #
    #    A primary asynchronous output signal is a signal that is directly
    #    fed by a top-level input through combinational logic.
    #    
    #    \returns a DBIter.DBIter object
    def iterAsyncOutputs(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopAsyncOutputs)

    ## Iterate over all the fanin for a given asynchronous output.
    #
    #
    #    A primary asynchronous output signal is a signal that is directly
    #    fed by a top-level input through combinational logic. This
    #    function returns the inputs that flow into the output.
    #    
    #    \returns a DBIter.DBIter object
    def iterAsyncFanin(self, node):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopAsyncFanin, node.mNode)

    ## Iterate over all the asynchronous deposits signals in the design.
    #
    #
    #    A asynchronous deposit signal is a depositable signal that directly
    #    feeds a primary output or observe through combinational logic.
    #    
    #    \returns a DBIter.DBIter object
    def iterAsyncDeposits(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopAsyncDeposits)

    ##  Iterate over all the primary asynchronous posedge resets in
    ##  the design.
    #
    #    
    #
    #    A primary posedge reset is a signal that appears in both the
    #    sensitivity list and in a conditional if statement in the body
    #    of an always block. In the sensitivity list, the signal has
    #    posedge sensitivity.
    #    
    #    \returns a DBIter.DBIter object
    def iterAsyncPosResets(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopAsyncPosResets)


    ## Iterate over all the primary asynchronous negedge resets in the
    ## design.
    #
    #    A primary posedge reset is a signal that appears in both the
    #    sensitivity list and in a conditional if statement in the body
    #    of an always block. In the sensitivity list, the signal has
    #    negedge sensitivity.
    #    
    #    \returns a DBIter.DBIter object
    def iterAsyncNegResets(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopAsyncNegResets)

    ## Iterate over all posedge schedule triggers
    #
    #  A schedule trigger that fires on both edges as well as triggers
    #  that fire only on their positive edge will appear in
    #  this loop. 
    #
    #
    #    \returns a DBIter.DBIter object
    def iterPosedgeTriggers(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopPosedgeTriggers)
    
    ## Iterate over all negedge schedule triggers
    #
    #  A schedule trigger that fires on both edges as well as triggers
    #  that fire only on their negative edge will appear in
    #  this loop. 
    #
    #
    #    \returns a DBIter.DBIter object
    def iterNegedgeTriggers(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopNegedgeTriggers)

    ## Iterate over all dual-edge schedule triggers
    #
    #  Only schedule triggers that fire on both edges
    #  will appear in this loop. 
    #
    #    \returns a DBIter.DBIter object
    def iterBothEdgeTriggers(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopBothEdgeTriggers)
    
    ##  Iterate over posedge-only schedule triggers
    #
    #   A schedule trigger that fires on both edges will not appear in
    #   this loop. Only triggers that run schedules on their positive
    #   edges will be present in the loop.
    #
    #    \returns a DBIter.DBIter object
    def iterPosedgeOnlyTriggers(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopPosedgeOnlyTriggers)

    ##  Iterate over negedge-only schedule triggers
    #
    #   A schedule trigger that fires on both edges will not appear in
    #   this loop. Only triggers that run schedules on their negative
    #   edges will be present in the loop.
    #
    #    \returns a DBIter.DBIter object
    def iterNegedgeOnlyTriggers(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopNegedgeOnlyTriggers)

    ## Iterate over all Triggers that also appear in the data path.
    #
    #  The data path does not trigger schedules, but triggers that also
    #  feed the data path may require special handling within a
    #  simulation system so that the data path is always updated on a
    #  trigger change.
    #
    #    \returns a DBIter.DBIter object
    def iterTriggersUsedAsData(self):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopTriggersUsedAsData)

    ## Iterate over all nodes matching a string, including wildcards
    #
    #    \returns a DBIter.DBIter object
    def iterMatching(self, matchString):
        return DBIter.DBIter(self.mObj, carbondbapi.carbonDBLoopMatching, matchString)

    ##  Return the name of the design
    #
    #   The name of the design is specified on the Carbon compiler
    #   command line with -o. For example, if the command line has -o
    #   libmyflop.a then the name of the design is 'myflop'
    def getInterfaceName(self):
        return carbondbapi.carbonDBGetInterfaceName(self.mObj)

    ## Return the top-level module name of the design
    #
    #  The top-level (or root) module name is the topmost module in a
    #  design. If there are several top-level modules then the
    #  top-level module was specified with either -vlogTop or
    #  -vhdlTop on the Carbon compiler command line.
    def getTopLevelModuleName(self):
        return carbondbapi.carbonDBGetTopLevelModuleName(self.mObj)

    ## Return the name of the systemC wrapper module name
    #
    #    This is specified on the Carbon compiler command line with
    #    -systemCModuleName 
    def getSystemCModuleName(self):
        return carbondbapi.carbonDBGetSystemCModuleName(self.mObj)

    ##  Return the version of the Carbon Database API.
    #
    #    The string consists of a title and a revision string. The
    #    revision string is the same as the carbon C API and Carbon
    #    compiler version strings in the distribution.
    def version(self):
        return carbondbapi.carbonDBGetVersion()

    ##  Return the version of the software that created the database
    #   
    #   The string is the same as what cbuild -version would return.
    #
    def softwareVersion(self):
        return carbondbapi.carbonDBGetSoftwareVersion(self.mObj)

    ##  Return the unique identifier of the database
    #   
    #    Each Carbon database has a unique identification string,
    #       reflecting its creation timestamp and the software version used to
    #      create it.
    #
    def idString(self):
        return carbondbapi.carbonDBGetIdString(self.mObj)

    ## Return the database type.
    #
    #    The string is either "FullDB" or "IODB". The full database
    #    contains all the symbols in the design and the I/O database
    #    only contains the primary I/Os as well as all depositable
    #    and observables
    def getDBType(self):
        return carbondbapi.carbonDBGetType(self.mObj)

    ## Return whether or not the Carbon Model is replayable
    #
    #    A Carbon Model is replayable if the design was compiled with
    #    -enableReplay.
    def isReplayable(self):
        return carbondbapi.carbonDBIsReplayable(self.mObj)

    ## Return whether or not the Carbon Model supports OnDemand
    #
    #    A Carbon Model supports OnDemand if the design was compiled with
    #    -onDemand.
    def supportsOnDemand(self):
        return carbondbapi.carbonDBSupportsOnDemand(self.mObj)

    ## Returns the value of a string attribute from the database
    #  
    # None is returned if the attribute does not exist
    def getStringAttribute(self, attributeName):
        return carbondbapi.carbonDBGetStringAttribute(self.mObj, attributeName)
    
    ## Returns the value of an integer attribute from the database
    #  
    # None is returned if the attribute does not exist
    def getIntAttribute(self, attributeName):
        # The C function takes a pointer to the value, and returns
        # success.  Translate that here.
        ret = None
        val = 0
        # eCarbon_OK is 0
        if (carbondbapi.carbonDBGetIntAttribute(self.mObj, val) == 0):
            ret = val
        return ret

##
#
# @}
