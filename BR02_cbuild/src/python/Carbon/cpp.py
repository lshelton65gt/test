#******************************************************************************
# Copyright (c) 2003-2011 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************

##
# \file
# Classes for generation of C++ code.
import re

###############################################################################
#
# first some Util functions
#
###############################################################################
def escapeCString(s):
  result = ''
  for c in s:
    if c == '\\' or c == '"':
      result += '\\'
    result += c
  return result



class NamedObject:
  def __init__(self, name):
    self.__name = name
    self.__desc = ''
    self.__userCodeName = name
    self.__customCodeName = name

  def name(self):
    return self.__name

  def setUserCodeName(self, name):
    self.__userCodeName = name
    
  def userCodeName(self):
    return self.__userCodeName

  def setCustomCodeName(self, name):
    self.__customCodeName = name
    
  def customCodeName(self):
    return self.__customCodeName

  def setDescription(self, desc):
    self.__desc = desc

  def description(self):
    return self.__desc

class TypedObject:
  def __init__(self, typeName):
    self.__typeName = typeName

  def typeName(self):
    return self.__typeName

  def emitDeclaration(self):
    raise "must implement emitDeclaration"

class AccessedObject:
  def __init__(self, accessSpecifier):
    assert(accessSpecifier in ('private', 'protected', 'public'))  # //this_assert_OK
    self.__access = accessSpecifier

  def access(self):
    return self.__access

  def isPublic(self): return self.__access == 'public'
  def isProtected(self): return self.__access == 'protected'
  def isPrivate(self): return self.__access == 'private'

class Scope:
  def __init__(self):
    self.__identifiers = {}
    self.__code = ''

  def addIdentifier(self, namedObject):
    name = namedObject.name()
    if name in self.__identifiers:
      raise 'identifier "%s" already exists in this scope' % name
    self.__identifiers[name] = namedObject

  def getIdentifier(self, name):
    return self.__identifiers[name]

  def hasIdentifier(self, name):
    return name in self.__identifiers
  
  def addCode(self, text):
    self.__code += text

  def code(self):
    return self.__code

class ScopedObject:
  def __init__(self, scope):
    self.__scope = scope

  def scope(self):
    return self.__scope

class LegalNamedObject(NamedObject):
  def __init__(self, origName, scope = None):
    name = ''
    # replace all questionable chars with _
    for c in origName:
      code = ord(c)
      if code >= ord('a') and code <= ord('z'):
        name += c
      elif code >= ord('A') and code <= ord('Z'):
        name += c
      elif code >= ord('0') and code <= ord('9'):
        name += c
      else:
        name += '_'

    # If this is a scoped object, create a unique name
    if scope != None:
      # if name is not unique, keep appending a number to it until it is
      if scope.hasIdentifier(name):
        i = 1
        while True:
          newName = name + '_' + str(i)
          if not scope.hasIdentifier(newName):
            name = newName
            break
          i = i +1

    NamedObject.__init__(self, name) 

    # Add new identifier to the current scope, if any
    # Add Identifyer needs to be called after NamedObject is initialized.
    if scope != None and name != origName: scope.addIdentifier(self)

class Variable(NamedObject, TypedObject):
  def __init__(self, typeName, name, arrayElem=0, static=False):
    NamedObject.__init__(self, name)
    TypedObject.__init__(self, typeName)
    self.__static = ''
    if static: self.__static = 'static '
    self.__array = ''
    if arrayElem > 0: self.__array = '[%d]' % arrayElem
    
  def emitDeclaration(self, userCode, customCode, indent):
    return indent + self.__static + self.typeName() + ' \t' + self.name() + self.__array

  def emitImplementation(self, userCode, customCode, indent):
    return ''

class Parameter(Variable):
  def emitDeclaration(self, userCode, customCode, indent):
    return self.typeName() + ' ' + self.name()

# type is the type of the return value
class Function(Scope, ScopedObject, NamedObject, TypedObject):
  def __init__(self, typeName, name, parameters, parentScope = None):
    Scope.__init__(self)
    ScopedObject.__init__(self, parentScope)
    NamedObject.__init__(self, name)
    TypedObject.__init__(self, typeName)

    self.__parameters = parameters
    self.__userCodeScopeName = None
    self.__preCode = ''
    self.__postCode = ''
    self.__preMainCode = ''
    self.__postMainCode = ''
    
  def addPreCode(self, text):
    self.__preCode += text

  def addPostCode(self, text):
    self.__postCode += text

  def addPreMainCode(self, text):
    self.__preMainCode += text

  def addPostMainCode(self, text):
    self.__postMainCode += text

  def setUserCodeScopeName(self, name):
    self.__userCodeScopeName = name
    
  def _declaration(self, name, indent, userCode, filterOutDefaultVal = False):
    code = ''
    typeName = self.typeName()
    if typeName != '':
      code += typeName + ' '

    # If the parameters has defult values, we need to filter them out when emitting
    # the implementation.
    params = self.__parameters
    if filterOutDefaultVal:
      params = '(' + ', '.join(re.sub(r"=.*", '', p).strip(' ()') for p in params.split(',')) + ')'
    
#    return code + name + '(' + ', '.join([p.emitDeclaration(userCode, indent) for p in self.__parameters]) + ')'
    return code + name + params

  def emitDeclaration(self, userCode, customCode, indent):
    return indent + self._declaration(self.name(), indent, userCode)

  def _scopedName(self):
    name = self.name()
    if self.scope() != None:
      name = self.scope().name() + '::' + name
    return name

  def _scopedUserCodeName(self):
    name = self.userCodeName()
    if self.__userCodeScopeName != None:
      name = self.__userCodeScopeName + '::' + name
    elif self.scope() != None:
      name = self.scope().userCodeName() + '::' + name
    return name
  
  def emitImplementation(self, userCode, customCode, indent):
    code = indent + self._declaration(self._scopedName(), indent, userCode, True)
    code += '\n' + indent + '{\n' + self.emitCode(userCode, customCode, indent + '  ') + indent + '}'
    return code

  def __emitCodeHelper(self, code, indent):
    sep = '\n' + indent
    code = indent + sep.join(code.split('\n')) + '\n'
    return code
    

  def emitCode(self, userCode, customCode, indent):
    # Add the code before the user/custom code sections
    code = self.__emitCodeHelper(self.__preCode, indent)

    # Add the user/custom code sections
    if userCode != None:
      code += userCode.preSection(self._scopedUserCodeName(), indent) + '\n'
    if customCode != None:
      code += customCode.getCustomCodePre(self.customCodeName())

    # Add the main body of code
    code += self.__emitCodeHelper(self.__preMainCode, indent)
    code += self.__emitCodeHelper(self.code(), indent)
    code += self.__emitCodeHelper(self.__postMainCode, indent)

    # Add the post user/custom code
    if customCode != None:
      code += customCode.getCustomCodePost(self.customCodeName())
    if userCode != None:
      code += userCode.postSection(self._scopedUserCodeName(), indent) + '\n'

    # Add the post code
    code += self.__emitCodeHelper(self.__postCode, indent)
    return code

class MemberVariable(Variable, AccessedObject):
  def __init__(self, typeName, name, access = 'public', arrayElem=0, static=False):
    Variable.__init__(self, typeName, name, arrayElem, static)
    AccessedObject.__init__(self, access)
    self.__constructorInit = ''

  def hasConstructorInit(self):
    return self.__constructorInit != ''
  
  def setConstructorInit(self, code):
    self.__constructorInit = code

  def emitConstructorInit(self):
    if self.hasConstructorInit():
      return self.name() + '(' + self.__constructorInit + ')'
    return ''

class MemberFunction(Function, AccessedObject):
  def __init__(self, parentScope, typeName, name, parameters, access, inline, virtual, pureVirtual):
    Function.__init__(self, typeName, name, parameters, parentScope)
    AccessedObject.__init__(self, access)
    self.__inline = inline
    self.__virtual = virtual or pureVirtual
    self.__pureVirtual = pureVirtual

  def typeName(self):
    # constructors and destructors don't have types, so the type is None.
    # catch the special case here to prevent codegen problems.
    typeName = Function.typeName(self)
    if typeName is None:
      typeName = ''
    return typeName

  def emitDeclaration(self, userCode, customCode, indent):
    code = ''
    if self.__virtual:
      code += 'virtual '
    code += Function.emitDeclaration(self, userCode, customCode, indent)
    if self.__pureVirtual:
      code += ' = 0'
    elif self.__inline:
      code += self.initializeMembers(indent)
      code += '{\n' + self.emitCode(userCode, customCode, indent + '  ') + indent + '  '+ '}'
    return code

  def emitImplementation(self, userCode, customCode, indent):
    code = ''
    if not (self.__inline or self.__pureVirtual):
      code = indent + self._declaration(self._scopedName(), indent, userCode, True)
      code += self.initializeMembers(indent)
      code += '\n' + indent + '{\n' + self.emitCode(userCode, customCode, indent + '  ') + indent + '}'
    return code

  def initializeMembers(self, indent):
    return ' '

class Constructor(MemberFunction):
  def __init__(self, parentScope, typeName, name, parameters, access, inline, baseClassInit):
    MemberFunction.__init__(self, parentScope, typeName, name, parameters, access, inline, virtual=False, pureVirtual=False)
    self.__baseClassInit = baseClassInit
    self.setCustomCodeName("constructor")

  def initializeMembers(self, indent):
    code = ''
    inits = []

    if self.__baseClassInit != '':
      inits.append(self.__baseClassInit)
      
    for var in self.scope().memberVariables():
      if var.hasConstructorInit():
        inits.append(var.emitConstructorInit())

    if len(inits) > 0:
      code = '\n' + indent + ': ' + ', '.join(inits) + '\n'
    return code

class Enum(ScopedObject, NamedObject, AccessedObject):
  def __init__(self, parentScope, access, name, valuePrefix):
    ScopedObject.__init__(self, parentScope)
    NamedObject.__init__(self, name)
    AccessedObject.__init__(self, access)
    self.__valuesDict = {}
    self.__values = []
    self.__valuePrefix = valuePrefix

  def addValue(self, name):
    nameObj = LegalNamedObject(name, self.scope())
    newName = nameObj.name()
    if nameObj.name() in self.__valuesDict:
      raise 'value "%s" already exists in this enum' % newName
    self.__valuesDict[newName] = nameObj
    self.__values.append(nameObj)

  def emitDeclaration(self, userCode, customCode, indent):
    code = indent + 'enum %(name)s {\n' % { 'name': self.name() }
    for nameObj in self.__values:
      code += indent + '  e%(valuePrefix)sNum%(name)s,\n' % \
              { 'name': nameObj.name(),
                'valuePrefix': self.__valuePrefix }
    code += indent + '  eNum%(valuePrefix)ss_%(name)s\n' % \
            { 'name': self.name(),
              'valuePrefix': self.__valuePrefix }
    code += indent + '}'
    return code

class Class(Scope, LegalNamedObject):
  def __init__(self, name, baseList):
    Scope.__init__(self)
    LegalNamedObject.__init__(self, name)
    self.__baseClasses = baseList
    self.__memberFunctions = []
    self.__memberVariables = []
    self.__enums = []
    self.__friends = []
    self.setCustomCodeName("class")

  def __addMemberVariable(self, member):
    self.addIdentifier(member)
    self.__memberVariables.append(member)

  def __addMemberFunction(self, member):
    self.addIdentifier(member)
    self.__memberFunctions.append(member)

  def __addEnum(self, enum):
    self.addIdentifier(enum)
    self.__enums.append(enum)

  def memberVariables(self):
    return iter(self.__memberVariables)

  def getMember(self, name):
    return self.getIdentifier(name)

  def addConstructor(self, parameters = '()', access = 'public', inline = False, baseClassInit = ''):
    constructor = Constructor(self, None, self.name(), parameters, access, inline, baseClassInit)
    self.__addMemberFunction(constructor)
    return constructor

  def addDestructor(self, access = 'public', inline = False, virtual = False):
    destructor = MemberFunction(self, None, '~' + self.name(), '()', access, inline, virtual, pureVirtual = False)
    self.__addMemberFunction(destructor)
    destructor.setCustomCodeName('destructor')
    return destructor

  def addMemberFunction(self, typeName, name, parameters = '()', access = 'public', inline = False, virtual = False, pureVirtual = False):
    func = MemberFunction(self, typeName, name, parameters, access, inline, virtual, pureVirtual)
    self.__addMemberFunction(func)
    return func

  def addMemberVariable(self, typeName, name, access = 'public', arrayElem=0, static=False):
    var = MemberVariable(typeName, name, access, arrayElem, static)
    self.__addMemberVariable(var)
    return var

  def addEnum(self, name, valuePrefix, access = 'public'):
    enum = Enum(self, access, name, valuePrefix)
    self.__addEnum(enum)
    return enum

  def addFriend(self, friend):
    self.__friends.append(friend)
    
  def emitMembers(self, access, indent, userCode, customCode):
    code = ''
    for memberList in (self.__enums, self.__memberFunctions, self.__memberVariables):
      for member in memberList:
        if member.access() == access:
          desc = member.description()
          if desc != '':
            code += indent + '// ' + desc + '\n'
          code += member.emitDeclaration(userCode, customCode, indent) + ';\n\n'
      if code != '':
        code = access + ':\n' + code
    return code

  def emitDefinition(self, userCode, customCode, indent = ''):
    derivation = ', '.join(self.__baseClasses)
    if derivation != '':
      derivation = ' : ' + derivation

    members = '\n'.join([self.emitMembers(access, indent, userCode, customCode) for access in ('public', 'protected', 'private')])

    # Setup 
    preSection = ''
    postSection = ''
    if userCode != None:
      preSection  = '\n' + userCode.preSection(self.name() + ' CLASS DEFINITION', indent) 
      postSection = '\n' + userCode.postSection(self.name() + ' CLASS DEFINITION', indent)
    if customCode != None:
      preCustomSection = customCode.getCustomCodePre('class')
      postCustomSection = customCode.getCustomCodePost('class')
    else:
      preCustomSection = ''
      postCustomSection = ''

    friendDecl = ''
    if self.__friends != []:
      friendDecl  = '// Friends'
      friendDecl += '\n%s'%(indent).join(['friend %s;'% friend for friend in self.__friends])
      friendDecl += '\n'
      
    code = '\n' + '/' * 79
    code += """
%(indent)sclass %(className)s%(derivation)s
%(indent)s{%(preUserCode)s%(preCustomCode)s
%(indent)s%(friends)s
%(indent)s%(members)s%(postCustomCode)s%(postUserCode)s
%(indent)s};
""" % { 'indent': indent,
        'className': self.name(),
        'derivation': derivation,
        'members': members,
        'friends': friendDecl,
        'preUserCode': preSection,
        'postUserCode': postSection,
        'preCustomCode': preCustomSection,
        'postCustomCode': postCustomSection
        }
    return code
  
  def emitImplementation(self, userCode, customCode, indent = ''):
    code = ''
    for memberList in (self.__memberFunctions, self.__memberVariables):
      for member in memberList:
        memberCode = member.emitImplementation(userCode, customCode, indent)
        if memberCode != '':
          code += memberCode + '\n\n'
    return code
