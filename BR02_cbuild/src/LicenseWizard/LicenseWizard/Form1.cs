/***************************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace LicenseWizard
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            // Initialize appropriate textBox with current value of User environment variable.
            string currentValue = Environment.GetEnvironmentVariable("CARBOND_LICENSE_FILE", EnvironmentVariableTarget.User);
            if (currentValue != null && currentValue.Contains("@"))
                textBoxServer.Text = currentValue;
            else
                textBoxFile.Text = currentValue;

            // Should set Form's AcceptButton and CancelButton, but the buttons are private in Wizard.
        }

        private void textBoxFile_TextChanged(object sender, EventArgs e)
        {
            radioButtonFile.Checked = true;
        }

        private void textBoxServer_TextChanged(object sender, EventArgs e)
        {
            radioButtonServer.Checked = true;
        }

        private void buttonFileOpen_Click(object sender, EventArgs e)
        {
            radioButtonFile.Checked = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                textBoxFile.Text = openFileDialog1.FileName;
        }

        private void wizard1_BeforeSwitchPages(object sender, Wizard.BeforeSwitchPagesEventArgs e)
        {
            WizardPage oldPage = wizard1.Pages[e.OldIndex];
            if (oldPage == wizardPageServerOrFile && e.NewIndex > e.OldIndex)
            {
                // Get the value from textBox corresponding to radio button chosen by the user.
                string result;
                if (radioButtonServer.Checked)
                    // We could insist that this contain a "@".
                    result = textBoxServer.Text;
                else
                    result = textBoxFile.Text;

                // If the environment variable doesn't exist it's possible that neither
                // radio button is checked.
                if (!radioButtonServer.Checked && !radioButtonFile.Checked)
                {
                    MessageBox.Show("Please select either a license server or a license file.",
                        Text, MessageBoxButtons.OK, MessageBoxIcon.Question);
                    e.Cancel = true;
                }
                else if (radioButtonServer.Checked && !textBoxServer.Text.Contains("@"))
                {
                    MessageBox.Show("The license server specification must be in the form port@host, " +
                        "where port and host are the TCP/IP port number and host name from the " +
                        "SERVER line of the license file, for example 40000@myhost.",
                        Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                }
                else if (radioButtonFile.Checked && textBoxFile.Text == "")
                {
                    MessageBox.Show("Please enter or browse to a file.",
                        Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                }

                else if (radioButtonFile.Checked && !File.Exists(textBoxFile.Text))
                {
                    MessageBox.Show("File " + textBoxFile.Text + " doesn't exist.",
                        Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                }

                else // OK, store the result
                {
                    // Since this is slow, use wait cursor.
                    Cursor prevCursor = Cursor;
                    Cursor = Cursors.WaitCursor;

                    // Set User environment variable.  Don't bother setting Process, since we won't use it.
                    Environment.SetEnvironmentVariable("CARBOND_LICENSE_FILE", result, EnvironmentVariableTarget.User);

                    Cursor = prevCursor;
                }
            }
        }
    }
}