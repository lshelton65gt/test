/***************************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
namespace LicenseWizard
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.wizard1 = new LicenseWizard.Wizard();
            this.wizardPageWelcome = new LicenseWizard.WizardPage();
            this.label1 = new System.Windows.Forms.Label();
            this.wizardPageServerOrFile = new LicenseWizard.WizardPage();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.radioButtonServer = new System.Windows.Forms.RadioButton();
            this.buttonFileOpen = new System.Windows.Forms.Button();
            this.textBoxFile = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButtonFile = new System.Windows.Forms.RadioButton();
            this.wizardPageFinish = new LicenseWizard.WizardPage();
            this.label3 = new System.Windows.Forms.Label();
            this.wizard1.SuspendLayout();
            this.wizardPageWelcome.SuspendLayout();
            this.wizardPageServerOrFile.SuspendLayout();
            this.wizardPageFinish.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "carbon.lic";
            this.openFileDialog1.Filter = "\"License files (*.lic)|*.lic|All files (*.*)|*.*";
            // 
            // wizard1
            // 
            this.wizard1.Controls.Add(this.wizardPageWelcome);
            this.wizard1.Controls.Add(this.wizardPageFinish);
            this.wizard1.Controls.Add(this.wizardPageServerOrFile);
            this.wizard1.HeaderImage = global::LicenseWizard.Properties.Resources.logo122;
            this.wizard1.Location = new System.Drawing.Point(0, 0);
            this.wizard1.Name = "wizard1";
            this.wizard1.Pages.AddRange(new LicenseWizard.WizardPage[] {
            this.wizardPageWelcome,
            this.wizardPageServerOrFile,
            this.wizardPageFinish});
            this.wizard1.Size = new System.Drawing.Size(493, 360);
            this.wizard1.TabIndex = 0;
            this.wizard1.WelcomeImage = global::LicenseWizard.Properties.Resources.WelcomeImage;
            this.wizard1.BeforeSwitchPages += new LicenseWizard.Wizard.BeforeSwitchPagesEventHandler(this.wizard1_BeforeSwitchPages);
            // 
            // wizardPageWelcome
            // 
            this.wizardPageWelcome.Controls.Add(this.label1);
            this.wizardPageWelcome.Description = "This wizard helps you install your Carbon license.";
            this.wizardPageWelcome.Location = new System.Drawing.Point(0, 0);
            this.wizardPageWelcome.Name = "wizardPageWelcome";
            this.wizardPageWelcome.Size = new System.Drawing.Size(493, 312);
            this.wizardPageWelcome.Style = LicenseWizard.WizardPageStyle.Welcome;
            this.wizardPageWelcome.TabIndex = 10;
            this.wizardPageWelcome.Title = "Welcome to the Carbon License Wizard";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(175, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(318, 188);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // wizardPageServerOrFile
            // 
            this.wizardPageServerOrFile.Controls.Add(this.textBoxServer);
            this.wizardPageServerOrFile.Controls.Add(this.radioButtonServer);
            this.wizardPageServerOrFile.Controls.Add(this.buttonFileOpen);
            this.wizardPageServerOrFile.Controls.Add(this.textBoxFile);
            this.wizardPageServerOrFile.Controls.Add(this.label2);
            this.wizardPageServerOrFile.Controls.Add(this.radioButtonFile);
            this.wizardPageServerOrFile.Description = "The wizard needs to know if you\'re using a license server or a license file.";
            this.wizardPageServerOrFile.Location = new System.Drawing.Point(0, 0);
            this.wizardPageServerOrFile.Name = "wizardPageServerOrFile";
            this.wizardPageServerOrFile.Size = new System.Drawing.Size(428, 208);
            this.wizardPageServerOrFile.TabIndex = 0;
            this.wizardPageServerOrFile.Title = "Server or File";
            // 
            // textBoxServer
            // 
            this.textBoxServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxServer.Location = new System.Drawing.Point(224, 102);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(192, 20);
            this.textBoxServer.TabIndex = 2;
            this.textBoxServer.TextChanged += new System.EventHandler(this.textBoxServer_TextChanged);
            // 
            // radioButtonServer
            // 
            this.radioButtonServer.AutoSize = true;
            this.radioButtonServer.Location = new System.Drawing.Point(30, 103);
            this.radioButtonServer.Name = "radioButtonServer";
            this.radioButtonServer.Size = new System.Drawing.Size(188, 17);
            this.radioButtonServer.TabIndex = 1;
            this.radioButtonServer.TabStop = true;
            this.radioButtonServer.Text = "Enter a license server (port@host):";
            this.radioButtonServer.UseVisualStyleBackColor = true;
            // 
            // buttonFileOpen
            // 
            this.buttonFileOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFileOpen.Location = new System.Drawing.Point(392, 126);
            this.buttonFileOpen.Name = "buttonFileOpen";
            this.buttonFileOpen.Size = new System.Drawing.Size(25, 23);
            this.buttonFileOpen.TabIndex = 5;
            this.buttonFileOpen.Text = "...";
            this.buttonFileOpen.UseVisualStyleBackColor = true;
            this.buttonFileOpen.Click += new System.EventHandler(this.buttonFileOpen_Click);
            // 
            // textBoxFile
            // 
            this.textBoxFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFile.Location = new System.Drawing.Point(150, 128);
            this.textBoxFile.Name = "textBoxFile";
            this.textBoxFile.Size = new System.Drawing.Size(236, 20);
            this.textBoxFile.TabIndex = 4;
            this.textBoxFile.TextChanged += new System.EventHandler(this.textBoxFile_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Select the option that you want to use.";
            // 
            // radioButtonFile
            // 
            this.radioButtonFile.AutoSize = true;
            this.radioButtonFile.Location = new System.Drawing.Point(30, 128);
            this.radioButtonFile.Name = "radioButtonFile";
            this.radioButtonFile.Size = new System.Drawing.Size(114, 17);
            this.radioButtonFile.TabIndex = 3;
            this.radioButtonFile.TabStop = true;
            this.radioButtonFile.Text = "Enter a license file:";
            this.radioButtonFile.UseVisualStyleBackColor = true;
            // 
            // wizardPageFinish
            // 
            this.wizardPageFinish.BackColor = System.Drawing.SystemColors.Control;
            this.wizardPageFinish.Controls.Add(this.label3);
            this.wizardPageFinish.Description = "The wizard has added a user environment variable named CARBOND_LICENSE_FILE with " +
                "the value you specified.  Programs started after this point will be able to use " +
                "the Carbon license.";
            this.wizardPageFinish.Location = new System.Drawing.Point(0, 0);
            this.wizardPageFinish.Name = "wizardPageFinish";
            this.wizardPageFinish.Size = new System.Drawing.Size(493, 312);
            this.wizardPageFinish.Style = LicenseWizard.WizardPageStyle.Finish;
            this.wizardPageFinish.TabIndex = 12;
            this.wizardPageFinish.Title = "Completing the Carbon License Wizard";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(173, 287);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "To close this wizard, click OK.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 360);
            this.Controls.Add(this.wizard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Carbon License Wizard";
            this.wizard1.ResumeLayout(false);
            this.wizardPageWelcome.ResumeLayout(false);
            this.wizardPageServerOrFile.ResumeLayout(false);
            this.wizardPageServerOrFile.PerformLayout();
            this.wizardPageFinish.ResumeLayout(false);
            this.wizardPageFinish.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Wizard wizard1;
        private WizardPage wizardPageWelcome;
        private System.Windows.Forms.Label label1;
        private WizardPage wizardPageServerOrFile;
        private System.Windows.Forms.RadioButton radioButtonFile;
        private System.Windows.Forms.TextBox textBoxServer;
        private System.Windows.Forms.RadioButton radioButtonServer;
        private System.Windows.Forms.Button buttonFileOpen;
        private System.Windows.Forms.TextBox textBoxFile;
        private System.Windows.Forms.Label label2;
        private WizardPage wizardPageFinish;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;

    }
}

