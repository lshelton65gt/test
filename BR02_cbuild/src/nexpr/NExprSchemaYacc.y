/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file nucleus.y
 *
 *  Parser for reading the nucleus schema
 */

%{

#include <stdarg.h>
#include "util/SourceLocator.h"
#include "util/AtomicCache.h"
#include "util/StringAtom.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include "NExprSchema.h"

#define YYDEBUG 1

#define YYLLOC_DEFAULT(Current, Rhs, N) Current= Rhs [1]

// The arg is passed in as a void *, it is immediately assigned to gSchema
#define YYPARSE_PARAM schema
NExprSchema *gSchema;

static NExprSchema::ClassType *gClass = NULL;

// Context for base type attributes
static NExprSchema::BaseType *gBaseType = NULL;

// Context for enumeration type attributes
static NExprSchema::EnumType *gEnumType = NULL;

// Context for vector type attributes
static NExprSchema::VectorType *gVectorType = NULL;

// Context for API attributes
static NExprSchema::API *gAPI = NULL;

#define YYLTYPE SourceLocator

extern int NExprSchema_lex ();

#ifdef __GNUC__
#define PRINTF(_m_, _n_) __attribute__ ((__format__ (printf, _m_, _n_)))
#else
#define PRINTF(_m_, _n_)
#endif

static void NExprSchema_error (const char *fmt, ...) PRINTF (1, 2);

static void YYError (const SourceLocator loc, const char *fmt, ...) PRINTF (2, 3);
static void YYWarn (const SourceLocator loc, const char *fmt, ...) PRINTF (2, 3);
static void YYInfo (const SourceLocator loc, const char *fmt, ...) PRINTF (2, 3);

#undef PRINTF

%}

%union {
  StringAtom *mSymbol;
  NExprSchema::ClassType *mClass;
  NExprSchema::ClassType::List *mClassList;
  NExprSchema::Field *mField;
  NExprSchema::Field::List *mFieldList;
  NExprSchema::Type *mType;
  NExprSchema::Code *mCode;
  NExprSchema::Code::OrdinalMap *mCodeMap;
  NExprSchema::NonTerminal *mNT;
  NExprSchema::Rewrite::Pattern *mPattern;
  NExprSchema::IRTransformation::RuleTree *mRuleTree;
  NExprSchema::IRTransformation::IRTree *mIRTree;
  NExprSchema::IROp::Parameters *mFormals;
  NExprSchema::IRTransformation::IRTree::Parameter *mActual;
  NExprSchema::IRTransformation::IRTree::Parameter::List *mActuals;
  UInt32 mFlags;
  UInt32 mInteger;
}

%token _ABSTRACT_
%token _ALIAS_
%token _API_
%token _BUILD_
%token _CASE_
%token _CHOICE_
%token _CLASS_
%token _COLON_COLON_
%token _DEBUG_
%token _ENUM_
%token _GOAL_
%token _INCLUDE_
%token _LEFT_
%token _LET_
%token _MASK_
%token _NESTED_
%token _NO_
%token _OTHERS_
%token _POST_
%token _PRE_
%token _PRINT_
%token _RIGHT_
%token _SWITCH_
%token _TOKEN_
%token _TOP_
%token _TYPE_
%token _SET_
%token _VECTOR_
%token _WHEN_
%token _WRITE_

%token <mSymbol> _IDENT_
%token <mSymbol> _STRING_
%token <mInteger> _INTEGER_

%type <mClass> superclass
%type <mClassList> superclass_list
%type <mClassList> superclasses
%type <mField> field
%type <mFieldList> field_list
%type <mFlags> class_directive
%type <mFlags> class_directives
%type <mFlags> class_properties
%type <mFlags> class_property
%type <mFlags> field_properties
%type <mFlags> field_property
%type <mFlags> maybe_pointer
%type <mCode> code
%type <mSymbol> type_ident
%type <mSymbol> field_name
%type <mType> type
%type <mNT> non_terminal
%type <mPattern> rewrite_pattern
%type <mInteger> cost;
%type <mIRTree> irtree;
%type <mRuleTree> transformation;
%type <mRuleTree> switch_transformation;
%type <mRuleTree> transformation_case_list;
%type <mRuleTree> transformation_case;
%type <mRuleTree> guarded_transformation;
%type <mRuleTree> when_transformation_list;
%type <mRuleTree> irtreerule;
%type <mRuleTree> coderule;
%type <mCode> opt_code;
%type <mCodeMap> let_map
%type <mInteger> code_options;
%type <mInteger> include_properties;
%type <mInteger> include_property_list;
%type <mInteger> include_property;
%type <mInteger> api_properties;
%type <mInteger> api_property_list;
%type <mInteger> api_property;
%type <mInteger> enum_mode;
%type <mFormals> ir_parameters
%type <mFormals> ir_parameter_list
%type <mActual> actual_parameter
%type <mActuals> actual_parameters
%type <mActuals> actual_parameter_list

%locations

%%

schema: 
  /* grab the handle on the schema from the yyparse argument */
  { gSchema = static_cast <NExprSchema *> (schema); }
  decl_list
  { gSchema = NULL; }
;

decl_list:
  decl_list decl
| /*empty*/
;

decl:
  class_decl
| class_defn
| type_decl
| include_decl
| token_decl
| vector_decl
| codegen_decl
| api_defn
| enum_decl
;

include_decl: _INCLUDE_ _STRING_ include_properties';'
{
  NExprSchema::IncludeFile *includes;
  if (gSchema->isDeclared ($2, &includes)) {
    // ignore multiple include file definitions
  } else {
    includes = new NExprSchema::IncludeFile (gSchema, @2, $2);
    gSchema->declare (includes);
    includes->setProperties ($3);
  }
}
;

include_properties:
  /*empty*/                      { $$ = 0; }
| '{' include_property_list '}'  { $$ = $2; }
;

include_property_list:
  include_property                       { $$ = $1; }
| include_property_list include_property { $$ = $1 | $2; }
;

include_property:
  _SET_ _IDENT_ ';' 
  { 
    if (!strcasecmp ("base", $2->str ())) {
      $$ = NExprSchema::IncludeFile::eBASE; 
    } else {
      YYError (@2, "%s: unrecognised include directive", $2->str ());
      $$ = 0;
    }
  }
;

token_decl: _TOKEN_ _IDENT_ ';'
{
  gSchema->declareToken (@1, $2, 0);
}
;

vector_decl: 
  _VECTOR_ _TYPE_ _IDENT_ '[' type ']' 
  { gVectorType = gSchema->declareVectorType (@1, $3, $5, 0); }
  vector_properties ';'
  { gVectorType = NULL; }
| _VECTOR_ _TYPE_ _IDENT_ '[' '*' type ']' 
  { gVectorType = gSchema->declareVectorType (@1, $3, $6, NExprSchema::VectorType::ePOINTER_ELEMENT); }
  vector_properties ';'
  { gVectorType = NULL; }
;

vector_properties:
  /*empty*/
| '{' vector_property_list '}'
;

  /* no vector properties yet... but there probably will be */
vector_property_list:
  /*empty*/
;

type: type_ident
  {
    NExprSchema::Type *type;
    if (gSchema->isDeclared ($1, &type)) {
      $$ = type;
    } else {
      YYError (@1, "Undeclared type '%s'", $1->str ());
      NExprSchema::ErrorType *error_type = new NExprSchema::ErrorType (gSchema, @1, $1);
      gSchema->declare (error_type);
      $$ = error_type;
    }
  }
;

class_decl: class_properties _CLASS_ type_ident ';'
{
  NExprSchema::ClassType *classtype;
  NExprSchema::Type *type = NULL;
  if (gSchema->isDeclared ($3, &classtype)) {
    // already declared as a class
  } else if (gSchema->isDeclared ($3, &type)) {
    YYError (@3, "'%s' has already been declared as a type.", $3->str ());
    YYInfo (type->getDecl (), "This is the declaration of type '%s'.", $3->str ());
  }
  if (classtype != NULL && classtype->isUndeclared ()) {
    // the class was implicitly declared so set the attributes here
    classtype->setDecl (@2);
    classtype->setProperties ($1);
    YYError (@3, "Class '%s' is already declared.", $3->str ());
    YYInfo (type->getDecl (), "This is the previous declaration of '%s'", $3->str ());
  } else if (classtype == NULL) {
    // declare a new class
    classtype = new NExprSchema::ClassType (gSchema, @2, $3);
    gSchema->declare (classtype);
    classtype->setProperties ($1);
  }
}
;

class_defn: class_properties _CLASS_ type_ident superclasses 
  {
    NExprSchema::ClassType *type;
    NExprSchema::BaseType *basetype;
    if (gSchema->isDeclared ($3, &basetype)) {
      YYError (@3, "'%s' has already been declared as a base type.", $3->str ());
      YYInfo (basetype->getDecl (), "This is the declaration of type '%s'.", $3->str ());
    }
    if (!gSchema->isDeclared ($3, &type)) {
      // declare the type
      type = new NExprSchema::ClassType (gSchema, @3, $3);
      gSchema->declare (type);
      type->setProperties ($1);
    } else if (type->isUndeclared ()) {
      type->setDecl (@3);
      type->setProperties ($1);
    } else if (type->getProperties () != $1) {
      YYError (@3, "Properties for class '%s' are inconsistent with the class declaration.", $3->str ());
      YYInfo (type->getDecl (), "This is the declaration of class '%s'.", $3->str ());
    }
    if (type->isDefined ()) {
      YYError (@3, "Class '%s' is already defined.", $3->str ());
      YYInfo (type->getDefn (), "This is the previous definition of '%s'", $3->str ());
    } else {
      type->Define (@3);
      type->setSuperclasses ($4);
    }
    gClass = type;
  }
  '{' class_directives field_list '}' ';'
  {
    NExprSchema::ClassType *type = gClass;
    type->setProperties ($7);
    NExprSchema::Field::List::iterator fields;
    for (fields = $8->begin (); fields != $8->end (); ++fields) {
      NExprSchema::Field *field = *fields;
      NExprSchema::Field *other;
      StringAtom *name = field->getName ();
      if (type->isDeclared (field->getName (), &other)) {
	YYError (field->getDecl (), "Field '%s.%s' is already declared.", $3->str (), name->str ());
	YYInfo (other->getDecl (), "This is the previous declaration of '%s.%s",
	  $3->str (), name->str ());
      } else if (field->getType () == type && !field->isPointer ()) {
	YYError (field->getDecl (), 
	  "Cannot declare field '%s' with type '%s' inside the declaration of '%s'.",
	  name->str (), $3->str (), $3->str ());
      } else {
	type->declare (field);
      }
      // It's probably bad to have fields with the same name in
      // superclasses... but I'm not sure if this happens in nucleus...
      NExprSchema::ClassType::List::iterator superclasses;
      for (superclasses = type->superbegin (); superclasses != type->superend (); ++superclasses) {
	NExprSchema::ClassType *superclass = *superclasses;
	NExprSchema::Field *other;
	if (superclass->isDeclared (name, &other)) {
	  YYWarn (field->getDecl (), "Field '%s.%s' is also declared in the superclass '%s'.",
	    $3->str (), name->str (), superclass->getName ()->str ());
	  YYInfo (other->getDecl (), "This is the declaration of '%s.%s'.",
	    superclass->getName ()->str (), name->str ());
	  YYError (field->getDecl (), "Sorry, you lose, the schema compiler must be extended to handle this situation.");
	}
      }
    }
    gClass = NULL;
    delete $4;
    delete $8;
  }
;

superclasses:
  /*empty*/
  { $$ = new NExprSchema::ClassType::List; }
| ':' superclass_list
  { $$ = $2; }
;

superclass: type_ident
  { 
    NExprSchema::ClassType *type;
    if (gSchema->isDeclared ($1, &type)) {
      // class is declared
    } else {
      NExprSchema::BaseType *basetype;
      if (gSchema->isDeclared ($1, &basetype)) {
	YYError (@1, "'%s' has already been declared as a base type.", $1->str ());
	YYInfo (basetype->getDecl (), "This is the declaration of type '%s'.", $1->str ());
      } 
      // implicitly define the class type
      type = new NExprSchema::ClassType (gSchema, @1, $1);
      gSchema->declare (type);
      type->setUndeclared ();
    }
    $$ = type;
  }
;

superclass_list:
  superclass
  {
    $$ = new NExprSchema::ClassType::List;
    $$->push_back ($1);
  }
| superclass_list ',' superclass
  { 
    $$ = $1;
    $$->push_back ($3);
  }
;

class_properties:
  class_properties class_property
  { $$ = $1 | $2; }
| /*empty*/
  { $$ = 0; }
;

class_property:
  _ABSTRACT_
  { $$ = NExprSchema::ClassType::eABSTRACT; }
| _NESTED_
  { $$ = NExprSchema::ClassType::eNESTED; }
;

class_directives:
  class_directives class_directive
  { $$ = $1 | $2; }
| { $$ = 0; }
;

class_directive:
  _SET_ _IDENT_ ';'
  {
    if (!strcasecmp ($2->str (), "unbuildable")) {
      $$ = NExprSchema::ClassType::eUNBUILDABLE;
    } else if (!strcasecmp ($2->str (), "unnested")) {
      $$ = NExprSchema::ClassType::eUNNESTED;
    } else if (!strcasecmp ($2->str (), "unwalkable")) {
      $$ = NExprSchema::ClassType::eUNWALKABLE;
    } else {
      YYError (@2, "%s: unrecognised class directive", $2->str ());
      $$ = 0;
    }
  }
| _SET_ _NO_ _API_ ';'
  { $$ = NExprSchema::ClassType::eNOAPI; }
| _SET_ _NO_ _IDENT_ ';'
  {
    YYError (@3, "%s: unrecognised class directive", $3->str ());
    $$ = 0;
  }
| _PRE_ _WRITE_ code ';'
  {
    $$ = 0; 
    $3->set (NExprSchema::Code::ePREWRITE);
    NExprSchema::Code *other;
    if (!gClass->install (NExprSchema::ClassType::ePRE_WRITE_CODE, $3, &other)) {
      YYError (@1, "pre-write code already defined for '%s'", gClass->getName ()->str ());
      YYInfo (other->getDefn (), "this is the previous pre-write code for '%s'", 
        gClass->getName ()->str ());
      delete $3;
    }
  }
| _POST_ _WRITE_ code ';'
  {
    $$ = 0; 
    $3->set (NExprSchema::Code::ePOSTWRITE);
    NExprSchema::Code *other;
    if (!gClass->install (NExprSchema::ClassType::ePOST_WRITE_CODE, $3, &other)) {
      YYError (@1, "post-write code already defined for '%s'", gClass->getName ()->str ());
      YYInfo (other->getDefn (), "this is the previous post-write code for '%s'", 
        gClass->getName ()->str ());
      delete $3;
    }
  }
| _POST_ _BUILD_ code ';'
  {
    $$ = 0; 
    $3->set (NExprSchema::Code::ePOSTBUILD);
    NExprSchema::Code *other;
    if (!gClass->install (NExprSchema::ClassType::ePOST_BUILD_CODE, $3, &other)) {
      YYError (@1, "build code already defined for '%s'", gClass->getName ()->str ());
      YYInfo (other->getDefn (), "this is the previous build code for '%s'", gClass->getName ()->str ());
      delete $3;
    }
  }
| _POST_ _TOP_ _BUILD_ code ';'
  {
    $$ = 0; 
    $4->set (NExprSchema::Code::ePOSTBUILD);
    NExprSchema::Code *other;
    if (!gClass->install (NExprSchema::ClassType::ePOST_TOP_BUILD_CODE, $4, &other)) {
      YYError (@1, "top build code already defined for '%s'", gClass->getName ()->str ());
      YYInfo (other->getDefn (), "this is the previous top build code for '%s'", gClass->getName ()->str ());
      delete $4;
    }
  }
| _BUILD_ code ';'
  { 
    $$ = 0; 
    $2->set (NExprSchema::Code::eBUILD);
    NExprSchema::Code *other;
    if (!gClass->install (NExprSchema::ClassType::eBUILD_CODE, $2, &other)) {
      YYError (@1, "build code already defined for '%s'", gClass->getName ()->str ());
      YYInfo (other->getDefn (), "this is the previous build code for '%s'", gClass->getName ()->str ());
      delete $2;
    }
  }
;

code_options:
  /*empty*/         { $$ = 0; }
| code_options '!' { $$ = $1 | NExprSchema::CodeFragment::eISBANG; }
;

code:
  code_options _STRING_
  { 
    $$ = new NExprSchema::Code (gSchema, @2); 
    UtString errmsg;
    if (!$$->build (@2, $2->str (), $1, &errmsg)) {
      YYError (@2, "%s: %s", errmsg.c_str (), $2->str ());
    }
  }
| code code_options _STRING_
  { 
    $$ = $1; 
    UtString errmsg;
    if (!$$->build (@3, $3->str (), $2, &errmsg)) {
      YYError (@3, "%s: %s", errmsg.c_str (), $3->str ());
    }
  }
;

field_list:
  /*empty*/
  { $$ = new NExprSchema::Field::List; }
| field_list field
  {
    $$ = $1;
    if ($2 != NULL) {
      $$->push_back ($2);
    }
  }
;

field_name:
  _IDENT_
  { $$ = $1; }
| field_name '.' _IDENT_
  {
    UtString ident;
    ident << $1->str () << "." << $3->str ();
    $$ = gSchema->intern (ident.c_str ());
  }
;

field: 
  field_properties type field_name ';'
  { $$ = gSchema->createField (@3, $2, $3, $1); }
| field_properties type '*' field_name ';'
  { $$ = gSchema->createField (@4, $2, $4, $1|NExprSchema::Field::ePOINTER); }
| field_properties type '[' field_name ']' ';'
  { $$ = gSchema->createField (@4, $2, $4, $1|NExprSchema::Field::eVECTOR); }
| field_properties type '[' '*' field_name ']' ';'
  { $$ = gSchema->createField (@5, $2, $5, $1|NExprSchema::Field::eVECTOR|NExprSchema::Field::ePOINTER); }
;

field_properties:
  field_properties field_property
  { $$ = $1 | $2; }
| /*empty*/
  { $$ = 0; }
;

field_property:
  _DEBUG_ _PRINT_
  { $$ = NExprSchema::Field::eDEBUGPRINT; }
;

type_ident: 
  _IDENT_
  { $$ = $1; }
| type_ident _COLON_COLON_ _IDENT_
  {
    UtString ident;
    ident << $1->str () << "::" << $3->str ();
    $$ = gSchema->intern (ident.c_str ());
  }
;

type_decl: 
  _TYPE_ maybe_pointer type_ident
  { gBaseType = gSchema->declareBaseType (@1, $3, $2); }
  base_type_attributes
  ';'
  { gBaseType = NULL; }
;

maybe_pointer:
  /*empty*/ { $$ = 0; }
| '*'       { $$ = NExprSchema::BaseType::ePOINTER; }
;

base_type_attributes:
  /*empty*/
| '{' base_type_attribute_list '}'
;

base_type_attribute_list:
  /*empty*/
| base_type_attribute_list base_type_attribute
;

base_type_attribute:
  _TOKEN_ _IDENT_ ';'
 { 
   NExprSchema::Token *token;
   token = gSchema->declareToken (@1, $2, NExprSchema::Token::eBOUND_TO_VALUE);
   if (gBaseType->getToken () != NULL) {
     YYError (@1, "Type '%s' already has a scanner token.", gBaseType->getName ()->str ());
     YYInfo (gBaseType->getToken ()->getDecl (), "This is the token for type '%s'.", 
       gBaseType->getName ()->str ());
   } else {
     gBaseType->setToken (token);
   }
 }
| _ALIAS_ type_ident ';'
  {
    NExprSchema::BaseType *alias;
    if (gBaseType->getAlias () != NULL) {
      YYError (@1, "'%s' already has an alias.", gBaseType->getName ()->str ());
    } else if (!gSchema->isDeclared ($2, &alias)) {
      YYError (@1, "'%s' is not declared as a basetype.", $2->str ());
    } else {
      gBaseType->setAlias (alias);
    }
  }
| _INCLUDE_ _STRING_ ';'
  {
    NExprSchema::IncludeFile *includes;
    if (gSchema->isDeclared ($2, &includes)) {
      // Do not consider this an error in this context
    } else {
      includes = new NExprSchema::IncludeFile (gSchema, @2, $2);
      gSchema->declare (includes);
    }
    includes->setProperties (NExprSchema::IncludeFile::eBASE);
  }
| _TYPE_ _STRING_ ';'
  {
    if (gBaseType->getCType () != NULL) {
      YYError (@1, "The C++ type for '%s' is already defined.", gBaseType->getName ()->str ());
    } else {
      gBaseType->setCType ($2);
    }
  }
;

codegen_decl:
  codegen_transform_rule
| codegen_ir_decl
| codegen_rewrite_decl
| codegen_goal_decl
;

codegen_transform_rule: 
  type_ident
  {
    if (!gSchema->isDeclared ($1, &gClass)) {
      YYError (@1, "Class '%s' is not declared.", $1->str ());
      gClass = new NExprSchema::ClassType (gSchema, @1, $1);
      gSchema->declare (gClass);
    }
  }
  '{' let_map transformation '}' ';'
  {
    NExprSchema::IRTransformation *transform;
    if (gSchema->isDeclared (gClass, &transform)) {
      YYError (@1, "IR transformation already defined for class '%s'.", $1->str ());
      YYInfo (transform->getDecl (), "This is the other transformation for '%s'.", $1->str ());
    } else {
      transform = new NExprSchema::IRTransformation (gSchema, @1, gClass, $4, $5);
      gSchema->declare (transform);
    }
    gClass = NULL;     
  }

;

let_map:
  /*empty*/ 
  { $$ = new NExprSchema::Code::OrdinalMap; }
| let_map _LET_ '$' _INTEGER_ '=' code ';'
  {
    $$ = $1;
    $6->set (NExprSchema::Code::eIRLET);
    $6->bind (gClass);
    NExprSchema::Code::OrdinalMap::iterator found;
    if ((found = $1->find ($4)) != $1->end ()) {
      YYError (@2, "$%d is multiply bound.", $4);
      YYInfo (found->second->getDefn (), "This is the earlier binding for $%d.", $4);
    } else {
      $1->insert (NExprSchema::Code::OrdinalMap::value_type ($4, $6));
    }
  }
;
transformation:
  irtreerule ';'         { $$ = $1; }
| switch_transformation  { $$ = $1; }
| guarded_transformation { $$ = $1; }
;

switch_transformation:
  _SWITCH_ '(' coderule ')' '{' transformation_case_list '}'
  { $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eSWITCH, $3, $6); }
;

transformation_case_list:
  transformation_case
  { $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eCASEPAIR, $1, NULL); }
| transformation_case transformation_case_list
  { $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eCASEPAIR, $1, $2); }
;

transformation_case: _CASE_ coderule ':' irtreerule ';'
  { $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eCASE, $2, $4);
  }
;

guarded_transformation:
when_transformation_list { $$ = $1; }
;

when_transformation_list:
  /*empty*/
  { $$ = NULL; }
| _OTHERS_ _RIGHT_ irtreerule ';'
  { $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eWHENPAIR, 
      new NExprSchema::IRTransformation::RuleTree (@1, gClass,
        NExprSchema::IRTransformation::RuleTree::eWHEN, NULL, $3), NULL);
  }
| _WHEN_ coderule _RIGHT_ irtreerule ';' when_transformation_list
  { $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eWHENPAIR, 
      new NExprSchema::IRTransformation::RuleTree (@1, gClass,
        NExprSchema::IRTransformation::RuleTree::eWHEN, $2, $4),
      $6);
  }
;

coderule: code
  { 
    $1->set (NExprSchema::Code::eTRANSFORMRULE);
    $1->bind (gClass);
    $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eCODE, $1); 
  }
;

irtreerule:
  irtree
  { 
    $$ = new NExprSchema::IRTransformation::RuleTree (@1, gClass,
      NExprSchema::IRTransformation::RuleTree::eIRTREE, $1); 
  }
;

irtree:
  code
  { 
    $1->bind (gClass);
    $1->set (NExprSchema::Code::eIRTREE);
    $$ = new NExprSchema::IRTransformation::IRTree (gSchema, @1, $1); 
  }
| '$' _INTEGER_
  { 
     UtString name;
     name << "$ref" << $2;
     NExprSchema::Code *code = new NExprSchema::Code (gSchema, @1);
     code->set (NExprSchema::Code::eALLOWNUMBERED);
     code->bind (gClass);
     NExprSchema::CodeFragment *fragment = new NExprSchema::NumberedCodeFragment (gSchema, @1, 
       gSchema->intern (name.c_str ()), $2);
     code->push_back (fragment);
	
     $$ = new NExprSchema::IRTransformation::IRTree (gSchema, @1, code); 
  }
| '$' _IDENT_
  { 
     NExprSchema::Code *code = new NExprSchema::Code (gSchema, @1);
     code->set (NExprSchema::Code::eALLOWFIELD);
     code->bind (gClass);
     NExprSchema::CodeFragment *fragment = new NExprSchema::FieldCodeFragment (gSchema, @1, $2);
     code->push_back (fragment);
     $$ = new NExprSchema::IRTransformation::IRTree (gSchema, @1, code); 
  }
| _IDENT_ actual_parameters
  { $$ = new NExprSchema::IRTransformation::IRTree (gSchema, @1, $1, $2); }
| _IDENT_ actual_parameters '(' irtree ')'
  { $$ = new NExprSchema::IRTransformation::IRTree (gSchema, @1, $1, $2, $4); }
| _IDENT_ actual_parameters '(' irtree ',' irtree ')'
  { $$ = new NExprSchema::IRTransformation::IRTree (gSchema, @1, $1, $2, $4, $6); }
;

actual_parameters:
  /*empty*/                     { $$ = NULL; }
| '[' actual_parameter_list ']' { $$ = $2; }
;

actual_parameter_list:
  actual_parameter
  {
    $$ = new NExprSchema::IRTransformation::IRTree::Parameter::List;
    $$->push_back ($1);
  }
| actual_parameter_list ',' actual_parameter
  {
    $$ = $1;
    $$->push_back ($3);
  }
;

actual_parameter:
  _STRING_   { $$ = new NExprSchema::IRTransformation::IRTree::Parameter (@1, $1); }
| _INTEGER_  { $$ = new NExprSchema::IRTransformation::IRTree::Parameter (@1, $1); }
;

codegen_ir_decl:
  _IDENT_ ir_parameters _INTEGER_ ';'
  {
    NExprSchema::IROp *other;
    if (gSchema->isDeclared ($1, &other)) {
      YYError (@1, "\"%s\" is already declared as an IR operator.", $1->str ());
      YYInfo (other->getDecl (), "This is the previous declaration of \"%s\".", $1->str ());
    } else {
      NExprSchema::IROp *irop = new NExprSchema::IROp (gSchema, @1, $1, $3, $2);
      gSchema->declare (irop);
    }
  }
;

ir_parameters:
  /*empty*/ 
  { $$ = new NExprSchema::IROp::Parameters; }
| '[' ir_parameter_list ']'
  { $$ = $2; }
;

ir_parameter_list:
  _IDENT_ _IDENT_
  {
    $$ = new NExprSchema::IROp::Parameters;
    $$->push_back (NExprSchema::IROp::Parameter ($1, $2));
  }
| ir_parameter_list ',' _IDENT_ _IDENT_ 
  {
    $$ = $1;
    $$->push_back (NExprSchema::IROp::Parameter ($3, $4));
  }
;

non_terminal: _IDENT_
  {
    if (!gSchema->isDeclared ($1, &$$)) {
      $$ = new NExprSchema::NonTerminal (gSchema, @1, $1);
      gSchema->declare ($$);
    }
  }
;

codegen_goal_decl: 
  _GOAL_ non_terminal ';'
  { 
    NExprSchema::NonTerminal *other;
    SourceLocator loc;
    if (gSchema->hasGoal (&other, &loc)) {
      gSchema->Error (@1, "Multiple goal symbol declarations.");
      gSchema->Info (loc, "This is the declaration of '%s' as the goal symbol.", other->getName ()->str ());
    } else {
      gSchema->bindGoal ($2, @1);
    }
  }
;

codegen_rewrite_decl: non_terminal _LEFT_ rewrite_pattern cost opt_code ';'
  { 
    if ($5 != NULL) {
      $5->set (NExprSchema::Code::eCODE);
    }
    gSchema->declare (new NExprSchema::Rewrite (gSchema, @1, $1, $3, $4, $5)); 
  }
;

opt_code:
  /*empty*/
  { $$ = NULL; }
| code
  { $$ = $1; }
;

cost:
  /*empty*/
  { $$ = 0; }
| '[' _INTEGER_ ']'
  { $$ = $2; }
;

rewrite_pattern:
  _IDENT_
  { $$ = new NExprSchema::Rewrite::Pattern (@1, $1); }
| _IDENT_ '(' rewrite_pattern ')'
  { $$ = new NExprSchema::Rewrite::Pattern (@1, $1, $3); }
| _IDENT_ '(' rewrite_pattern ',' rewrite_pattern ')'
  { $$ = new NExprSchema::Rewrite::Pattern (@1, $1, $3, $5); }
;

api_defn: _API_ _IDENT_ 
  {
    NExprSchema::API *api;
    if (gSchema->isDeclared ($2, &api)) {
      YYError (@2, "\"%s\" is already declared as an API or alias for an API.", $2->str ());
      YYInfo (api->getDecl (), "This is the previoius declaration for the API \"%s\".", $2->str ());
    } else {
      api = new NExprSchema::API (gSchema, @2, $2);
    }
    gAPI = api;                         // context for api_properties productions
  }
  api_properties ';'
  {
    gAPI->setProperties ($4);
    gAPI = NULL;
  }
;

api_properties:
  /*empty*/                 { $$ = 0; }
| '{' api_property_list '}' { $$ = $2; }
;

api_property_list:
   /*empty*/                     { $$ = 0; }
| api_property_list api_property { $$ = $1 | $2; }
;

api_property:
  _IDENT_ _API_ ';'
  {
    if (!strcasecmp ($1->str (), "distillery")) {
      $$ = NExprSchema::API::eIS_DISTILLERY;
    } else {
      YYError (@1, "Unrecognised API attribute \"%s\".", $1->str ());
      $$ = 0;
    }
  }
| _ALIAS_ _IDENT_ ';'
  {
    NExprSchema::API *api;
    if (gSchema->isDeclared ($2, &api)) {
      YYError (@2, "\"%s\" is already declared as an API or alias for an API.", $2->str ());
      YYInfo (api->getDecl (), "This is the previoius declaration for the API \"%s\".", $2->str ());
    } else {
      gSchema->alias ($2, gAPI);
    }
  }
;

enum_decl: 
  enum_mode _ENUM_ type_ident 
  { gEnumType = gSchema->declareEnumType (@1, $3, $1); }
  '{' enum_ident_list '}' ';'
;

enum_mode:
  /*empty*/ { $$ = 0; }
| _MASK_    { $$ = NExprSchema::EnumType::eMASK; }
| _CHOICE_  { $$ = NExprSchema::EnumType::eCHOICE; }
;

enum_ident_list:
  enum_ident
| enum_ident_list ',' enum_ident
;

enum_ident: 
  _IDENT_ '=' _INTEGER_
  { gEnumType->declareEnumValue (gSchema, @1, $1, $3); }
;

%%

extern int NExprSchema_lineno;

static void NExprSchema_error (const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  gSchema->YYError (NExprSchema_lineno, fmt, ap);
  va_end (ap);  
}

static void YYError (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  gSchema->YYError (loc, fmt, ap);
  va_end (ap);  
}

static void YYWarn (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  gSchema->YYWarn (loc, fmt, ap);
  va_end (ap);  
}

static void YYInfo (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  gSchema->YYInfo (loc, fmt, ap);
  va_end (ap);  
}

