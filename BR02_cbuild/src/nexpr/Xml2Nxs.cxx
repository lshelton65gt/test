// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/UtIOStream.h"
#include "util/ArgProc.h"
#include "util/NExprMsgContext.h"
#include "util/SourceLocator.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/CodeStream.h"
#include "util/UtList.h"
#include "util/UtSet.h"
#include "util/UtMap.h"
#include "util/UtMultiMap.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

//! \class Context
/*! Context handles the command line processing for the XML to
 *  N-expression schema translator.
 *
 * \note This is subclass from FileCollector so that it may be passed to
 * ArgProc::parseCommandLine to receive the file name arguments.
 */

static const char *scVerbose = "-v";
static const char *scQuiet   = "-q";

class Context : public FileCollector {
public:

  //! ctor for class Context
  Context (MsgContext &msg_context, SourceLocatorFactory &sourcelocators);

  virtual ~Context () {}

  //! Process the command line arguments
  bool processArgs (int *argc, char **argv);

  //! \return the ArgProc
  const ArgProc &getArgs () const { return mArgs; }

  //! \return iff -quiet was specified
  bool isQuiet () const { return mArgs.getBoolValue (scQuiet); }

  //! \return iff -v was specified
  bool isVerbose () const { return mArgs.getBoolValue (scVerbose); }

  //! \return the input filename
  const char *getInputFilename () const { return mInputFilename.c_str (); }
  
  //! \return the output filename
  const char *getOuputFilename () const { return mOutputFilename.c_str (); }

  //! \return the diagnostic dump file
  UtOStream &getDiag () { INFO_ASSERT (mDiag != NULL, "no diagnostic file"); return *mDiag; }

  //! Construct a SourceLocator for a node in the document
  SourceLocator getXmlLoc (const xmlNodePtr node) const
  { return mSources.create (mInputFilename.c_str (), xmlGetLineNo (node)); }

private:

  MsgContext &mMsg;                     //!< whining stream
  ArgProc mArgs;                        //!< argument vector handler
  SourceLocatorFactory &mSources;       //!< our fabulously complex location stuff
  UtList <UtString> mFilenames;         //!< filenames from the command line
  UtString mInputFilename;              //!< name of the input file
  UtString mOutputFilename;             //!< name of the output file
  UtString mDiagFilename;               //!< name of the diagnostic file
  UtOStream *mDiag;                     //!< the diagnostic file

  //! Configure the argument processor
  void configureArgs ();

  //! Print annotation store reader usage instructions
  void printUsage ();

  //! Implement FileCollector::scanArgument method
  virtual bool scanArgument (const char *, const char *) { return false; }

  //! Implement FileCollector::addArgument method
  virtual void addArgument (const char *filename) { mFilenames.push_back (filename); }

};

//
// XML stuff
//

// Optionally tag error messages with the Xml2Nxs source location that induces
// the error.

#if 1
#define XMLERRORTAG(_tag_) UtIO::cout () << __FILE__ << ": " << __LINE__ << ": " << #_tag_ << "\n"
#else
#define XMLERRORTAG(_tag_)
#endif

//! Convenience macro for emitting a source related error message
#define XMLERROR(_node_, _source_, _msg_, _error_, ...) {        \
    XMLERRORTAG (_error_); \
    SourceLocator srcloc; \
    SourceLocator xmlloc = _source_.getXmlLoc (_node_); \
    if (_source_.getSrcLoc (_node_, &srcloc)) { \
      _msg_.Xml2Nxs##_error_ (&srcloc, ## __VA_ARGS__); \
    } else { \
      _msg_.Xml2Nxs##_error_ (&xmlloc, ## __VA_ARGS__); \
      _msg_.Xml2NxsNoLocation (&xmlloc); \
    } \
  };

//! \struct xmlStringCompare
/*! Implements lexical comparison of xmlChar * strings
 */

struct xmlStringCompare {
  bool operator () (const xmlChar *a, const xmlChar *b) const
  { return xmlStrcmp (a, b) < 0; }
};

//! Stream output operator for XML strings
UtOStream &operator << (UtOStream &out, const xmlChar *str)
{
  out << (const char *) str;
  return out;
}

//! Stream output operator for XML strings
UtString &operator << (UtString &s, const xmlChar *str)
{
  s << (const char *) str;
  return s;
}

// maps and lists of XML strings
typedef UtMap <xmlChar *, xmlChar *, xmlStringCompare> xmlStringMap;
typedef UtMap <xmlChar *, xmlNodePtr, xmlStringCompare> xmlNodeMap;
typedef UtList <xmlChar *> xmlStringList;

// These are the tags used by GCCXML
#define sABSTRACT        (xmlChar *) "abstract"
#define sARRAYTYPE       (xmlChar *) "arraytype"
#define sATTRIBUTES      (xmlChar *) "attributes"
#define sBASE            (xmlChar *) "base"
#define sCLASS           (xmlChar *) "class"
#define sCONTEXT         (xmlChar *) "context"
#define sCVQUALIFIEDTYPE (xmlChar *) "cvqualifiedtype"
#define sENUMERATION     (xmlChar *) "enumeration"
#define sENUMVALUE       (xmlChar *) "enumvalue"
#define sFIELD           (xmlChar *) "field"
#define sFILE            (xmlChar *) "file"
#define sFUNDAMENTALTYPE (xmlChar *) "fundamentaltype"
#define sID              (xmlChar *) "id"
#define sINCOMPLETE      (xmlChar *) "incomplete"
#define sINIT            (xmlChar *) "init"
#define sLOCATION        (xmlChar *) "location"
#define sMEMBERS         (xmlChar *) "members"
#define sNAME            (xmlChar *) "name"
#define sNAMESPACE       (xmlChar *) "namespace"
#define sPOINTERTYPE     (xmlChar *) "pointertype"
#define sSTRUCT          (xmlChar *) "struct"
#define sTYPE            (xmlChar *) "type"
#define sTYPEDEF         (xmlChar *) "typedef"
#define sUNION           (xmlChar *) "union"

class Field;                            // forward declaration
class Schema;                           // forward declaration
class NxsType;                          // forward declaration
class TypeFactory;                      // forward declaration

//! \class SourceMapper
/*! GCCXML leaves source location information in the form of location="id:line"
 *  attributes. SourceMapper constructs a table of id->filename mappings and
 *  then provides translation from xmlNodePtr to SourceLocator.
 */

class SourceMapper {
public:

  SourceMapper (Context &context, MsgContext &msg, SourceLocatorFactory &sources, xmlDocPtr doc);

  //! Visibility for debugging
  /*! Dumps the node (using xmlElemDump) and the source location information to
   *  a file.
   */
  void dump (FILE *f, xmlNodePtr node, const char *fmt = NULL, ...) const;

  //! Dump the node to standard output
  void dump (xmlNodePtr node) const;

  //! \return the XML source location for an XML node
  SourceLocator getXmlLoc (xmlNodePtr node) const;

  //! Find the XML source location for an XML node
  bool getXmlLoc (xmlNodePtr node, SourceLocator *loc) const;

  //! \return the C++ source location for an XML node
  /*! This method looks for a "location" attribute in the node, plucks apart
   *  the string and constructs a SourceLocation instance.
   */
  bool getSrcLoc (xmlNodePtr node, SourceLocator *sourceloc) const;

  //! \return the C++ source location that induced an XML node
  /*! This is really a thin wrapper around the other getSrcLoc */
  SourceLocator getSrcLoc (xmlNodePtr node) const;

private:

  Context &mContext;                    //!< configuration
  SourceLocatorFactory &mSources;       //!< source locator generation
  MsgContext &mMsg;                     //!< message stream
  xmlDocPtr mDoc;                       //!< the XML document
  xmlStringMap mFiles;                  //!< maps tags to filenames

};

//! \class Attributes
/*! This class plucks apart the string from the "attributes" attribute of an
 *  XML node.
 */
class Attributes {
public:

  enum {
    eDISTILL = 1<<0,                    //!< set for distilled classes
    eABSTRACT = 1<<1,                   //!< set for abstract classes
    eNESTED = 1<<2,                     //!< set for nested classes
    eBASE_TYPE = 1<<3,                  //!< set for base types
    eOMITTED = 1<<4,                    //!< set for omitted fields and types
    eCHOICE = 1<<5,                     //!< set for enums used as choices
    eMASK = 1<<6,                       //!< set for enums used as masks
    eBUILD = 1<<7,                      //!< the build string attribute
    eBUILTIN = 1<<8,                    //!< set for builtin types
    eSTRUCT = 1<<9,                     //!< set when this is really a struct
    eCONTAINER = 1<<10,                 //!< set for container classes
    eDECLARED = 1<<11,                  //!< set when the declaration is emitted
    eCLOSED = 1<<12,                    //!< set when a type is closed
    eSELECT = 1<<13,                    //!< select a member from a union
    eCLASS_ATTRIBUTES = eDISTILL|eNESTED|eBASE_TYPE|eBUILD|eOMITTED,
    eFIELD_ATTRIBUTES = eOMITTED|eSELECT,
    eTYPE_ATTRIBUTES = eBASE_TYPE|eCHOICE|eMASK
  };

  Attributes (MsgContext &msg, SourceMapper &sourcemap);

  virtual ~Attributes ();
  
  //! Parse the attributes from a node
  UInt32 operator () (xmlNodePtr node, const UInt32 legal);

  //! Lookup a string valued attribute
  bool get (UInt32 key, UtString *str) const;

private:

  MsgContext &mMsg;                     //!< message stream
  SourceMapper &mSource;                //!< maps nodes to C++ source

  // Some attributes are associated with a string value. Keep these in ma
  typedef UtMap <UInt32, UtString *> Values;
  Values mStrValue;

  enum Token {
    eIDENT,                             //!< an identifier
    eLPAREN,                            //!< left parenthesis
    eRPAREN,                            //!< right parenthesis
    eCOMMA,                             //!< a comma
    eERROR,                             //!< error in the string 
    eEND                                //!< end of the string
  };
  
  Token mToken;                         //!< current token
  char mIdent [64];                     //!< identifier value when mToken == eIDENT

  //! \class Source
  /*! A simple scanner for the attribute value string */
  class Source {
  public:

    Source (SourceLocator xmlloc, SourceMapper &sourcemapper, xmlNodePtr node, const char *str) : 
      mXmlLoc (xmlloc), mSource (sourcemapper), mStr (str), mPosition (0), mNode (node) {}

    //! \return the current character
    char operator * () const { return mStr [mPosition]; };
    
    //! Advance to the next character
    void operator ++ (int) { mPosition++; }

    //! Report an error at the current location
    /*! Two messages are emitted. The first message includes the test passed into
     *  the method and is attached to the XML source. The second message notes
     *  the location in the C++ source that induced the node in the XML
     *  document.
     */
    void error (MsgContext &msg, const char *fmt, ...);
    void verror (MsgContext &msg, const char *fmt, va_list ap);

    //! Write this to a stream
    void print (UtOStream &out) const;

    //! Write this to standard output
    void pr () const;

  private:

    SourceLocator mXmlLoc;              //!< location in the XML source document
    SourceMapper &mSource;              //!< for mapping the node to a C++ source location
    const char *mStr;                   //!< the attribute string scanned by this
    UInt32 mPosition;                   //!< offset of the current character in the string
    const xmlNodePtr mNode;             //!< the node that contains the string

  };

  //! Write this to a stream
  void print (UtOStream &out) const;

  //! Write this to standard output
  void pr () const;

  //! Entry point for a simple recursive descent parser
  /*! attributes <- { gccxml-attribute | unknown-attribute } */
  UInt32 parse (Source &source, const UInt32 legal);

  //! Parse a gccxml attribute
  /*! gccxml-attribute <- gccxml ( gccxml-attribute-name {, gccxml-attribute-name } )
   *  gccxml-attribute-name <- "DISTILL" | "NESTED" | _IDENT_ gccxml-arg-list
   */
  UInt32 parseGccXml (Source &source, const UInt32 legal);

  //! Parse an argument list
  /*! gcc-arg-list = gcc-parenthesised-value
   */
  void parseGccXmlArgs (Source &source, UInt32 key);

  //! Skip a parenthesised term
  /*! term <- IDENT {, IDENT }  |  ( term ) */
  void skipTerm (Source &source);

  //! Skip an unknown attribute
  /*! unknown-attribute <- _IDENT_  |  _IDENT ( term ) */
  void skipUnknown (Source &source);

  //! Scan the string for a parenthesised value
  /*! parenthesised-value-string = <a string with balanced parenthesis>
   *  This is not done as a recursive descent parser. We are really only
   *  interested in grabbing the string with balanced parenthsis, so just scan
   *  the characters and count the parentheses.
   */
  void getParenthesedValue (Source &source, UtString *str);

  //! Scan the next token in the source string
  /*! The token value is written to mToken. If an identifier is found then the
   *  lexical value is stored in mIdent and nul terminated
   */
  void nextToken (Source &source);

};

//! \class Node
/*! Wraps an XML node with additional information. This class just adds a
 *  property bitmask to the node. Subclasses of Node extend as
 *  appropriate.
 */
class Node {
public:

  Node (xmlChar *id, xmlNodePtr node, UInt32 flags = 0) : 
    mNode (node), mId (id), mFlags (flags), mMarked (false)
  {
    INFO_ASSERT (mId != NULL, "node does not have an id attribute");
  }

  virtual ~Node ()
  { xmlFree (mId); }

  //! \return the flags set for the class
  UInt32 getFlags () const { return mFlags; }

  //! coerce the wrapper to the encapsulated node
  xmlNodePtr getNode () const { return mNode; }
  
  //! \return the key string for the node
  xmlChar *getId () const { return mId; }

  //! \return iff attributes are set
  inline bool isSet (const UInt32 flags) const { return mFlags & flags; }

  //! \return iff the node is marked as omitted
  inline bool isOmitted () const { return isSet (Attributes::eOMITTED); }

  //! \return iff a node is marked as distilled
  inline bool isDistilled () const { return isSet (Attributes::eDISTILL); } 

  //! \return iff the class is abstract
  inline bool isAbstract () const { return isSet (Attributes::eABSTRACT); }

  //! \return iff the class is marked as nested
  inline bool isNested () const { return isSet (Attributes::eNESTED); }

  //! \return iff the class is marked as a base type
  inline bool isBaseType () const { return isSet (Attributes::eBASE_TYPE); }

  //! \return iff a node is marked with the ephemeral marker
  inline bool isMarked () const { return mMarked; }

  //! Set attribute bits on the node
  void set (const UInt32 bits) { mFlags |= bits; }

  //! Set the ephemeral marker
  inline void setMark () { mMarked = true; }

  //! Clear the ephemeral marker
  inline void clearMark () { mMarked = false; }

  //! \return the fully qualified name
  UtString qualifiedName (const Schema *schema, const char *flavour) const;

  //! Simple wrapper around maps from id to Node
  class Map : public UtMap <xmlChar *, Node *, xmlStringCompare> {
  public:
    Map () : UtMap <xmlChar *, Node *, xmlStringCompare> () {}
    virtual ~Map () {}
    void insert (Node *x)
    { UtMap <xmlChar *, Node *, xmlStringCompare>::insert (value_type (x->getId (), x)); }
  };

  //! Debugging
  void dump (xmlDocPtr) const;

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const;

protected:

  xmlNodePtr mNode;                     //!< the encapsulated node
  xmlChar *mId;                         //!< the "id" of the class node
  UInt32 mFlags;                        //!< mask over Attributes

private:

  bool mMarked;

};

//! \class XmlType
/*! In its initial scan of the document, the TypeFactory identifies nodes that
 *  define types. Subsequently, when processing the members of distilled
 *  classes, the XmlType instances will induce an NxsType instance when a type
 *  is identified for distillation.
 */

class XmlClass;
class XmlClassMap;

class XmlType : public Node {
public:

  XmlType (xmlChar *id, xmlNodePtr node, xmlDocPtr doc, UInt32 flags = 0);

  virtual ~XmlType () {}

  //! Construct an NxsType from an XmlType
  virtual bool distill (TypeFactory &, NxsType **type) = 0;

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const;

  //! Write this to a stream
  void print (UtOStream &out) const;

  //! Write this to standard output
  void pr () const;

  void dump () const;

  //! \return the name of the type
  virtual const char *getName () const { return NULL; }

  class Map : public UtMap <xmlChar *, XmlType *, xmlStringCompare> {
  public:

    Map () : UtMap <xmlChar *, XmlType *, xmlStringCompare> () {}
    virtual ~Map () {}

    bool collect (xmlChar *typeId, xmlNodePtr node, xmlDocPtr doc, 
      MsgContext &msg, SourceMapper &source, TypeFactory &factory, XmlClassMap *);

    bool find (xmlChar *typeId, XmlType **type) const
    {
      const_iterator found;
      if ((found = UtMap <xmlChar *, XmlType *, xmlStringCompare>::find (typeId)) != end ()) {
        *type = found->second;
        return true;
      } else {
        *type = NULL;
        return false;
      }
    }

    bool find (const char *name, xmlChar **typeId) const
    {
      NameMap::const_iterator found;
      if ((found = mNameMap.find (name)) != mNameMap.end ()) {
        *typeId = found->second;
        return true;
      } else {
        *typeId = NULL;
        return false;
      }
    }

    void insert (XmlType *type)
    {
      UtMap <xmlChar *, XmlType *, xmlStringCompare>::insert (value_type (type->getId (), type));
      const char *name = type->getName ();
      if (name != NULL) {
        //UtIO::cout () << __LINE__ << ": type name: " << name << " = " << type->getId () << "\n";
        mNameMap.insert (NameMap::value_type (name, type->getId ()));
      }
    }

  private:

    struct StringLessThan {
      bool operator () (const char *a, const char *b) const
      {
        return strcmp (a, b) < 0;
      }
    };

    typedef UtMap <const char *, xmlChar *, StringLessThan> NameMap;

    NameMap mNameMap;                     //!< maps class name to XmlClass *

  };

  //! Bind this XmlType to a distilled type
  void bind (NxsType *type) 
  { 
    INFO_ASSERT (mNxsType == NULL, "multiple types bound to XML type");
    mNxsType = type; 
  }

protected:

  xmlDocPtr mDoc;
  NxsType *mNxsType;

};

//! \class Field
/*! Wraps Field-type XML nodes
 */

class Field : public Node {
public:

  typedef UtList <Field *> List;

  Field (xmlChar *id, xmlNodePtr node, UInt32 flags) : 
    Node (id, node, flags), mType (NULL)
  {
    xmlChar *xmlname = xmlGetProp (node, sNAME);
    if (xmlname == NULL) {
      INFO_ASSERT (xmlname != NULL, "field with no name");
    } else {
      mName << (const char *) xmlname;
      xmlFree (xmlname);
    }
  }

  virtual ~Field () {}

  //! \return the name of the field
  const char *getName () const { return mName.c_str (); }

  //! \return iff the eSELECT attribute is set
  bool isSelected () const { return isSet (Attributes::eSELECT); }

  //! Set the type of the field
  void setType (NxsType *type) { mType = type; }

  //! \return the type of the field
  NxsType *getType () const { return mType; }

  //! emit the declaration of the field
  void emitDecl (CodeStream &out);

  //! Construct a human readable representation of this
  const char *compose (UtString *s) const;

  //! Write this to a stream
  void print (UtOStream &out) const;

  //! Write this to standard output
  void pr () const;

private:

  UtString mName;                       //!< name of the field
  NxsType *mType;                       //!< type of the field

};

class FieldMapper;

class ClassType;                        // forward declaration
class UnionType;

//! \class XmlClass
/*! Wraps Class-type XML nodes
 */

class XmlClass : public XmlType {
public:

  //! ctor
  XmlClass (xmlChar *id, xmlNodePtr node, xmlDocPtr doc, UtString name, const UInt32 flags);

  virtual ~XmlClass () {}

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &, NxsType **);

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const;

  //! \return the name of the class
  const char *getName () const { return mName.c_str (); }

  typedef UtSet <XmlClass *> Set;
  typedef UtList <XmlClass *> List;

  //! push the name of the class in a UtString
  //! Note the hierarchical relationship between classes
  void setSuper (XmlClass *super)
  {
    mSuper.insert (super);
    super->mSub.insert (this);
  }

  // Iterate over subclasses
  Set::iterator beginSub () { return mSub.begin (); }
  Set::iterator endSub () { return mSub.end (); }

  // Iterate over superclasses
  Set::iterator beginSuper () { return mSuper.begin (); }
  Set::iterator endSuper () { return mSuper.end (); }

  //! Clear the degree
  void clearInDegree () { mInDegree = 0; }

  //! Increment the degree
  void incInDegree () { mInDegree++; }

  //! Decrement the degree
  void decInDegree () { mInDegree--; }

  //! \return the degree
  UInt32 getInDegree () const { return mInDegree; }

  //! Propagate attributes from the class to all its subclasses
  bool propagateSynthesisedAttributes (MsgContext &msg, SourceMapper sourcemap, 
    const UInt32 mask, const UInt32 syn);

  enum Properties {
    eBUILD,                             //!< the build string
    eNProperties
  };

  //! Set properties
  void bind (const Properties key, UtString value)
  {
    INFO_ASSERT (mProperties.find (key) == mProperties.end (), "duplicate property");
    mProperties.insert (PropertyMap::value_type (key, value));
  }

  bool isSet (const Properties key, UtString *value) const
  {
    PropertyMap::const_iterator found;
    if ((found = mProperties.find (key)) != mProperties.end ()) {
      *value = found->second;
      return true;
    } else {
      return false;
    }
  }

  typedef UtMap <xmlChar *, XmlClass *, xmlStringCompare> Map;

  ClassType *getNxsClass ();

private:

  typedef UtMap <Properties, UtString> PropertyMap;

  Set mSuper;                           //!< superclasses
  Set mSub;                             //!< subclasses
  UtString mName;                       //!< the name of the class
  UInt32 mInDegree;                     //!< degree of the class in a graph
  PropertyMap mProperties;              //!< the string valued properties

};

class XmlClassMap : public XmlClass::Map {
public:

  XmlClassMap () : XmlClass::Map () {}
  virtual ~XmlClassMap () {}

  void insert (XmlClass *theclass)
  {
    XmlClass::Map::insert (value_type (theclass->getId (), theclass));
  }

  bool find (xmlChar *typeId, XmlClass **type) const
  {
    const_iterator found;
    if ((found = UtMap <xmlChar *, XmlClass *, xmlStringCompare>::find (typeId)) != end ()) {
      *type = found->second;
      return true;
    } else {
      *type = NULL;
      return false;
    }
  }

};

//
// Type descriptors
//

//! \class NxsType
/*! This class hierarchy represents the types of class fields.
 */
class NxsType {
public:

  NxsType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &, const UInt32 flags);

  virtual ~NxsType () {}

  //! \return the XmlType that induced this instance
  /*! \note There are circumstances where this will be NULL. For example, the
   *  pointer type implicit in "UtList<UInt32 *>"
   */
  XmlType *getXmlType () const { return mXmlType; }

  //! Set attribute bits on the type
  void set (const UInt33 flags) { mFlags |= flags; }

  //! \return the id string
  xmlChar *getId () const { return mId; }

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &) const = 0;

  //! Write a declaration for a field of this type
  virtual void emitFieldDecl (CodeStream &, const char *fieldName) const = 0;

  //! Close the type
  virtual void close (TypeFactory &) { }

  //! \return iff this type can be the victim of a PointerType
  virtual bool canPoint () const = 0;

  //! \return iff the class is marked as declared
  bool isDeclared () const { return (mFlags & Attributes::eDECLARED); }

  //! \return iff the type has been closed
  bool isClosed () const { return (mFlags & Attributes::eCLOSED); }

  //! \return iff this is the void type
  virtual bool isVoid () const { return false; }

  //! \return iff the type is a class type
  virtual bool isClassType () const { return false; } // no I'm not...

  //! \return iff the node is marked as a base type
  inline bool isBaseType () const { return mFlags & Attributes::eBASE_TYPE; }

  //! \return iff the node is marked as a base type
  virtual bool isOmitted () const { return mFlags & Attributes::eOMITTED; }

  //! \return the name of the type
  virtual const char *getName () const { return "<anon>"; }

  //! Emit a forward declaration for the type
  virtual void emitForwardDeclaration (CodeStream &) {}

  //! Emit any required schema declaration for the type
  virtual void emitSchema (CodeStream &, Schema *) {}

  //! Write this to a stream
  void print (UtOStream &out) const;

  //! Write this to standard output
  void pr () const;

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *) const = 0;

  //! Lists of NxsType instances
  typedef UtList <NxsType *> List;

  typedef UtMap <xmlChar *, NxsType *, xmlStringCompare> SimpleMap;

  /*! A thin wrapper around UtMap <...> */
  class Map : public SimpleMap {
  public:

    Map () : UtMap <xmlChar *, NxsType *, xmlStringCompare> () {}

    virtual ~Map () {}

    void insert (NxsType *type)
    { 
      const_iterator found;
      if ((found = SimpleMap::find (type->getId ())) != end ()) {
        UtIO::cout () << "typeId = " << type->getId () << "\n";
        INFO_ASSERT (found->second == type, "duplicate distilled type in map");
      } else {
        SimpleMap::insert (value_type (type->getId (), type)); 
      }
    }

    bool find (xmlChar *id, NxsType **type) const
    {
      const_iterator found;
      if ((found = SimpleMap::find (id)) != end ()) {
        *type = found->second;
        return true;
      } else {
        *type = NULL;
        return false;
      }
    }

  };

  //! \return iff this is a container instance
  bool isContainer () const { return mFlags & Attributes::eCONTAINER; }

protected:

  xmlNodePtr mNode;                     //!< the XML node that defines the type
  xmlChar *mId;                         //!< the "id" tag for the type
  XmlType *mXmlType;                    //!< the xmltype that induced this NxsType
  UInt32 mFlags;                        //!< attributes on the type
  bool mUsed;                           //!< set when the type is used by a distilled class

};

class ClassGraph;
class NxsContainerType;

//! \class TypeFactory
/*! This class constructs Type instances from "id" tags/
 */
class TypeFactory {
public:

  TypeFactory (MsgContext &msg, SourceMapper &source, xmlDocPtr doc, Schema &schema,
    FieldMapper &allfields) :
    mMsg (msg), mSource (source), mDoc (doc), mSchema (schema), mAllFields (allfields),
    mContext (NULL)
  {}

  //! \return the source mapper
  SourceMapper &getSource () const { return mSource; }

  //! \return the message stream
  MsgContext &getMsg () const { return mMsg; }

  //! \return the class map
  XmlClassMap &getClassMap () { return mClasses; }

  /*! \return iff a type could be construed */
  bool infer (Field *field, NxsType **);

  //! Infer a type from an XML "id" value
  bool infer (xmlNodePtr, xmlChar *typeId, NxsType **);

  //! Infer a type from the name
  bool infer (xmlNodePtr, const char *name, NxsType **);

  //! Collect all the nodes that may define a type into a map
  void collectTypeNodes (xmlNodePtr node)
  {
    xmlChar *typeId = xmlGetProp (node, sID);
    if (typeId == NULL) {
      // ignore this node
    } else if (mXmlTypes.collect (typeId, node, mDoc, mMsg, mSource, *this, &mClasses)) {
      // The node was recognised as a type inducing node and added to the
      // table. The map takes ownership of the typeId storage
    } else {
      xmlFree (typeId);
    }
    for (xmlNodePtr child = node->xmlChildrenNode; child != NULL; child = child->next) {
      collectTypeNodes (child);
    }
  }

  //! Distill an XmlType
  bool distill (XmlType *xmltype, NxsType **nxstype);

  //! Close the type system w.r.t. the closure worklist
  void close ();

  //! Lookup the class name map
  bool find (xmlChar *typeId, XmlClass **theclass) const
  { return mClasses.find (typeId, theclass); }

  //! \return the fully qualified name
  UtString qualifiedName (xmlNodePtr node, const char *flavour) const;

  //! Output any declarations required for inferred types
  void emitSchema (CodeStream &, Schema *, ClassGraph &graph);

  bool NoTypeId (xmlNodePtr, const char *flavour);
  bool NoName (xmlNodePtr, const char *flavour);

  //! Locate a field
  bool find (xmlChar *id, Field **field) const;

  //! Install a type into the map
  void bind (NxsType *type)
  { 
    mNxsTypes.insert (type); 
    mClosureWorklist.push_back (type);
  }

  //! Pluck apart a template instance
  bool inferContainer (xmlNodePtr srcnode, xmlChar *typeId, XmlType *xmltype, const char *, NxsType **);

  //! Output a trace back
  void traceBack () const { mContext->traceBack (); }

private:

  MsgContext &mMsg;                     //!< message stream
  SourceMapper &mSource;                //!< maps nodes to locations
  xmlDocPtr mDoc;                       //!< the XML tree
  XmlType::Map mXmlTypes;               //!< maps "id" values for type nodes to node
  Schema &mSchema;                      //!< context
  XmlClassMap mClasses;                 //!< maps "id" values to classes
  NxsType::Map mNxsTypes;               //!< cached types
  NxsType::List mClosureWorklist;       //!< worklist for type closure
  FieldMapper &mAllFields;              //!< map of all the fields in the document

public:

  /*! Locating the source of errors can be a little problematics, so maintain a
   *  stack of records indicating the type inference context.
   */
  friend class Context;

  //! If a class is omitted, then mark all classes with that class as context as omitted
  void propagateOmittedClasses ();

  class Context {
  public:
    enum Flavour {
      eFIND_FIELD,
      eFIND_NAME,
      eFIND_TYPEID,
      eFIND_CONTAINER,
      eDISTILLING,
      eCLOSING_SUPER,
      eCLOSING_FIELDS,
      eCLOSING
    };

    Context (const xmlNodePtr node, const Flavour flavour, const char *name, TypeFactory &factory) :
      mFactory (factory), mNode (node), mFlavour (flavour), mName (name), 
      mTypeId (NULL), mXmlType (NULL), mPrev (push ()) {}

    Context (const Flavour flavour, const XmlType *type, TypeFactory &factory) :
      mFactory (factory), mNode (type->getNode ()), mFlavour (flavour), mName (NULL),
      mTypeId (NULL), mXmlType (type), mPrev (push ()) {}

    Context (const xmlNodePtr node, const Flavour flavour, const xmlChar *typeId, TypeFactory &factory) :
      mFactory (factory), mNode (node), mFlavour (flavour), mName (NULL), 
      mTypeId (typeId), mXmlType (NULL), mPrev (push ()) {}

    ~Context () { pop (); }

    void traceBack () const
    {
#define NOTE(_error_, ...) {        \
        SourceLocator srcloc; \
        SourceLocator xmlloc = mFactory.mSource.getXmlLoc (mNode); \
        if (mFactory.mSource.getSrcLoc (mNode, &srcloc)) { \
          UtString buffer; \
          srcloc.compose (&buffer); \
          mFactory.mMsg.Xml2Nxs##_error_ (&srcloc, ## __VA_ARGS__); \
          mFactory.mMsg.Xml2NxsNoteXmlSource (&xmlloc, buffer.c_str ()); \
        } else { \
          mFactory.mMsg.Xml2Nxs##_error_ (&xmlloc, ## __VA_ARGS__); \
          mFactory.mMsg.Xml2NxsNoLocation (&xmlloc); \
        } \
      };
      UtString b0;
      switch (mFlavour) {
      case eFIND_FIELD:
        NOTE (LookingForField, mName);
        break;
      case eFIND_NAME:
        NOTE (LookingForName, mName);
        break;
      case eFIND_TYPEID:
        NOTE (LookingForTypeId, (const char *) mTypeId);
        break;
      case eFIND_CONTAINER:
        NOTE (LookingForContainer, mName);
        break;
      case eDISTILLING:
        NOTE (LookingForContainer, mXmlType->compose (&b0));
        break;
      case eCLOSING_SUPER:
        NOTE (CloseSuper, mXmlType->compose (&b0));
        break;
      case eCLOSING_FIELDS:
        NOTE (CloseFields, mXmlType->compose (&b0));
        break;
      case eCLOSING:
        NOTE (Closing, mXmlType->compose (&b0));
        break;
      }
#undef NOTE
      if (mPrev != NULL) {
        mPrev->traceBack ();
      }
    }

  private:

    TypeFactory &mFactory;
    const xmlNodePtr mNode;
    const Flavour mFlavour;
    const char *mName;
    const xmlChar *mTypeId;
    const XmlType *mXmlType;
    Context *mPrev;

    Context *push ()
    {
      Context *prev = mFactory.mContext;
      mFactory.mContext = this;
      return prev;
    }

    void pop ()
    {
      INFO_ASSERT (mFactory.mContext == this, "oops");
      mFactory.mContext = mPrev;
    }

  };

  class Reason {
  public:
    enum Flavour {
      eIS_FIELD_TYPE,
      eIS_SUPERCLASS,
      eIS_DISTILLED
    };

    Reason (XmlType *subject, XmlType *object) :
      mSubject (subject), mFlavour (eIS_SUPERCLASS), mObject (object), mField (NULL) {}

    Reason (XmlType *subject, XmlType *object, Field *field) :
      mSubject (subject), mFlavour (eIS_FIELD_TYPE), mObject (object), mField (field) {}

    Reason (XmlType *subject) :
      mSubject (subject), mFlavour (eIS_DISTILLED), mObject (NULL), mField (NULL) {}

    const XmlType *getSubject () const { return mSubject; }
    
    typedef UtMultiMap <const XmlType *, Reason *> Map;
    typedef UtList <Reason *> List;

    void explain (UtOStream &) const;

  private:

    const XmlType *mSubject;
    const Flavour mFlavour;
    const XmlType *mObject;
    const Field *mField;
  };

  void note (Reason *reason);
  void note (ClassType *parent, Field *field);
  void note (UnionType *parent, Field *field);
  void explain (UtOStream &s);
  void explain (UtOStream &s, XmlType *type) const;

private:

  Context *mContext;                    //!< the type inference context
  Reason::Map mReasons;                 //!< tracks why types are 
  Reason::List mReasonList;             //!< reasons in the order of occurrence

  //! Infer a pointer type from an XML "id" value
  /*! \return iff the type could be construed */
  bool inferPointerType (Field *, xmlNodePtr node, xmlChar *typeId, NxsType **);

  bool getId (xmlNodePtr node, xmlChar **id)
  {
    if ((*id = xmlGetProp (node, sID)) != NULL) {
      return true;
    } else {
      XMLERROR (node, mSource, mMsg, NoId, (char *) node->name);
      return false;
    }
  }

};

//! \class NodeMapper
/*! Maintains a mapping from the "id" value of a class of XML nodes to the node
 */
class NodeMapper {
public:

  //! ctor
  /*! \note While it would be nice to call buildMap from this constructor this
   *  just cannot be done. buildMap calls the abstract virtual wrapNode method
   *  and so if called from this constructor a "pure virtual method" runtime
   *  error results. Thus, the onus is on the subclass to call buildMap.
   */
  NodeMapper (MsgContext &msg, SourceMapper &sourcemap) : 
    mMsg (msg), mSource (sourcemap)
  {}

  virtual ~NodeMapper () {}

  //! Dump the map to an output stream - for debugging
  void dump (UtOStream &out, UInt32 max = 0) const
  {
    UInt32 n = 0;
    Node::Map::const_iterator it;
    for (it = mMap.begin (); it != mMap.end (); it++) {
      const xmlChar *key = it->first;
      const xmlNodePtr node = it->second->getNode ();
      xmlChar *name = xmlGetProp (node, sNAME);
      SourceLocator loc = mSource.getXmlLoc (node);
      UtString b0;
      loc.compose (&b0);
      if (name != NULL) {
        out << b0 << ": " << key << " = " << name << "\n";
      } else {
        out << b0 << ": " << key << "\n";
      }
      xmlFree (name);
      if (max > 0 && n++ >= max) {
        return;
      }
    }
  }

  // Map lookup
  Node::Map::const_iterator find (xmlChar *key) const { return mMap.find (key); }
  Node::Map::iterator find (xmlChar *key) { return mMap.find (key); }

  // Map iteration
  Node::Map::const_iterator begin () const { return mMap.begin (); }
  Node::Map::const_iterator end () const { return mMap.end (); }
  Node::Map::iterator begin () { return mMap.begin (); }
  Node::Map::iterator end () { return mMap.end (); }

protected:
  
  //! Construct a mapping from the "id" value to the node for all matching nodes
  void buildMap (xmlDocPtr doc, xmlChar *type)
  {
    // Walk the document looking for File objects.
    xmlNodePtr root = xmlDocGetRootElement (doc);
    for (xmlNodePtr node = root->xmlChildrenNode; node != NULL; node = node->next) {
      xmlChar *key;
      if (xmlStrcasecmp (node->name, type) != 0) {
        // wrong type of node
      } else if ((key = xmlGetProp (node, sID)) == NULL) {
        XMLERROR (node, mSource, mMsg, NoId, (const char *) type);
      } else {
        mMap.insert (wrapNode (key, node));
      }
    }
  }

  MsgContext &mMsg;                     //!< error stream
  SourceMapper &mSource;                //!< configuration
  Node::Map mMap;                //!< maps "id" to the wrapped node

  //! \return an encapsulation of the XML node
  virtual Node *wrapNode (xmlChar *, xmlNodePtr) = 0;

};

//! \class IdList
/*! IdList explodes the list valued attributes of the XML node */
class IdList {
public:

  IdList (xmlChar *list)
  {
    UInt32 n = 0;
    UInt32 m = 0;
    int state = 0;
    while (state >= 0) {
      switch (state) {
      case 0:                           // initial state
        if (list [n] == '\0') {
          state = -1;                   // done
        } else if (isspace (list [n])) {
          n++;                          // consume whitespace
        } else {
          m = n;                        // note the start of the tag
          n++;
          state = 10;                   // start accumulating a tag
        }
        break;
      case 10:
        if (isspace (list [n]) || list [n] == '\0') {
          state++;                      // end of the tag
        } else {
          n++;                          // advance to next character
        }
        break;
      case 11:
        {
          xmlChar *tag = xmlCharStrndup ((const char *) list + m, n - m);
          mIds.push_back (tag);
          state = 0;
        }
        break;
      }
    }
  }

  virtual ~IdList ()
  {
    xmlStringList::iterator it;
    for (it = mIds.begin (); it != mIds.end (); it++) {
      xmlFree (*it);
    }
  }

  xmlStringList::iterator begin () { return mIds.begin (); }
  xmlStringList::iterator end () { return mIds.end (); }

  //! Write this to a stream
  void print (UtOStream &) const;

  //! Write this to standard output
  void pr () const;

private:

  xmlStringList mIds;                   //!< exploded list of the identifiers

};

//! Write this to a stream
void IdList::print (UtOStream &s) const
{
  UInt32 n = 0;
  for (xmlStringList::const_iterator it = mIds.begin (); it != mIds.end (); it++) {
    if (n++ > 0) {
      s << ", " << *it;
    } else {
      s << *it;
    }
  }
  s << "\n";
}

//! Write this to standard output
void IdList::pr () const
{ print (UtIO::cout ()); }

//! \class Namespace
/*! Wraps Namespace-type XML nodes
 */

class Namespace : public Node {
public:

  enum Properties {
    eBUILD,                             //!< the build string
    eNProperties
  };

  //! ctor
  Namespace (xmlChar *id, xmlNodePtr node, UInt32 flags) : 
    Node (id, node, flags)
  {
    xmlChar *xmlname = xmlGetProp (node, sNAME);
    INFO_ASSERT (xmlname != NULL, "anonymous namespace");
    mName << (const char *) xmlname;
    xmlFree (xmlname);
    mIsStd = (strcmp (mName.c_str (), "::") == 0);
  }

  virtual ~Namespace () {}

  //! \return the name of the namespace
  const char *getName () const { return mName.c_str (); }

  //! \return iff this is the std namespace
  bool isStd () const { return mIsStd; }

private:

  UtString mName;                       //!< the name of the class
  bool mIsStd;                          //!< iff this is std::
};

//! \class NamespaceMapper
/*! A mapping from node "id" to the XML object for namespaces
 */
class NamespaceMapper : public NodeMapper {
public:

  NamespaceMapper (MsgContext &msg, SourceMapper &sourcemapper, xmlDocPtr doc) : 
    NodeMapper (msg, sourcemapper)
  { 
    buildMap (doc, sNAMESPACE); 
  }

  virtual ~NamespaceMapper () {}
  
  bool find (xmlChar *key, Namespace **theclass) const
  {
    Node::Map::const_iterator found = NodeMapper::find (key);
    if (found == end ()) {
      *theclass = NULL;
      return false;
    } else {
      *theclass = static_cast <Namespace *> (found->second);
      return true;
    }
  }

protected:
  
  //! \return the node encapsulated in a Namespace instance
  virtual Node *wrapNode (xmlChar *id, xmlNodePtr node) 
  { 
    Attributes attributes (mMsg, mSource);
    UInt32 flags = attributes (node, Attributes::eCLASS_ATTRIBUTES);
    if (xmlHasProp (node, sABSTRACT)) {
      flags |= Attributes::eABSTRACT;
    }
    return new Namespace (id, node, flags); 
  }

};

//
// Type descriptor hierarchy
//

// Currently, unions are supported iff a member of the union is identified as
// the selected member. Union fields are then treated as if they are just that
// single element.

class UnionType : public NxsType {
public:

  UnionType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory,
    Field *selected) : 
    NxsType (node, typeId, xmltype, factory, 0), mSelected (selected)
  {
  }

  virtual ~UnionType () {}

  //! Close the type
  virtual void close (TypeFactory &factory) 
  { 
    NxsType *type;
    if (!factory.infer (mSelected, &type)) {
      factory.note (this, mSelected);
      XMLERROR (mSelected->getNode (), factory.getSource (), factory.getMsg (), NoSchemaType, 
        mSelected->getName ());
    } else if (type->isOmitted ()) {
      // do nothing
    } else {
      factory.note (this, mSelected);
      mSelected->setType (type);
    }
  }

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &out) const
  {
    mSelected->getType ()->emitName (out);
  }

  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    emitName (out);
    out << " " << fieldName << "." << mSelected->getName () << ";"
        << " // selected union element " << __LINE__ << "\n";
  }

  //! Emit a forward declaration for the type
  virtual void emitForwardDeclaration (CodeStream &out)
  {
    if (!mSelected->getType ()->isDeclared ()) {
      mSelected->getType ()->emitForwardDeclaration (out);
    }
    set (Attributes::eDECLARED);
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    UtString b0;
    mSelected->compose (&b0);
    *s << b0 << " union item";
    return s->c_str ();
  }

  virtual bool canPoint () const { return true; }

  virtual bool isOmitted () const
  { 
    return NxsType::isOmitted () 
      || mSelected->isOmitted () 
      || (mSelected->getType () != NULL && mSelected->getType ()->isOmitted ());
  }

private:

  Field *mSelected;                   //!< the designated field

};

class PointerType : public NxsType {
public:

  PointerType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory, 
    NxsType *basetype) : 
    NxsType (node, typeId, xmltype, factory, 0), mBaseType (basetype) 
  {
    if (basetype->isOmitted ()) {
      set (Attributes::eOMITTED);
    }
  }

  virtual ~PointerType () {}

  //! \return iff this denotes void *
  bool isVoidPointer () const { return mBaseType->isVoid (); }

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &out) const
  {
    mBaseType->emitName (out);
    out << " *";
  }

  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    emitName (out);
    out << fieldName << "; // " << __LINE__ << "\n";
  }

  //! Emit a forward declaration for the type
  virtual void emitForwardDeclaration (CodeStream &out)
  {
    if (!mBaseType->isDeclared ()) {
      mBaseType->emitForwardDeclaration (out);
    }
    set (Attributes::eDECLARED);
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    UtString b0;
    mBaseType->compose (&b0);
    *s << b0 << "*";
    return s->c_str ();
  }

  //! No pointers-to-pointers in NExpression schema
  virtual bool canPoint () const { return false; }

  virtual bool isOmitted () const
  { return NxsType::isOmitted () || mBaseType->isOmitted (); }

private:

  NxsType *mBaseType;                   //!< the pointed type

};

class EnumType : public NxsType {
public:

  EnumType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory, 
    const UInt32 flags, UtString name) : 
    NxsType (node, typeId, xmltype, factory, flags), mName (name)
  {
    INFO_ASSERT (!xmlStrcasecmp (node->name, sENUMERATION), "node is not an enumeration");
  }

  virtual ~EnumType () {}

  virtual const char *getName () const { return mName.c_str (); }

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &out) const
  { out << getName (); }

  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    out << mName << " " << fieldName << "; // type=" << mId  << " (" << __LINE__ << ")\n";
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    *s << "enum";
    return s->c_str ();
  }

  //! Emit any required schema declaration for the type
  virtual void emitSchema (CodeStream &out, Schema *)
  {
    INFO_ASSERT (!isOmitted (), "outputing omitted type");
    bool is_choice = (mFlags & Attributes::eCHOICE) != 0;
    bool is_mask = (mFlags & Attributes::eMASK) != 0;
    INFO_ASSERT (!is_mask | !is_choice, "overqualified");
    if (is_mask) {
      out << "mask enum ";
    } else if (is_choice) {
      out << "choice enum ";
    } else {
      out << "enum ";
    }
    out << mName << " {\n";
    out++;
    bool is_first = true;
    for (xmlNodePtr child = mNode->xmlChildrenNode; child != NULL; child = child->next) {
      if (!xmlStrcasecmp (child->name, sENUMVALUE)) {
        if (!is_first) {
          out << ",\n";
        }
        xmlChar *name = xmlGetProp (child, sNAME);
        xmlChar *value = xmlGetProp (child, sINIT);
        // write the value in hex
        UInt32 n = 0;
        for (int i = 0; value [i] != '\0'; i++) {
          n = 10 * n + (int) value [i] - (int) '0';
        }
        char buffer [64];
        snprintf (buffer, sizeof (buffer), "0x%08x", n);
        out << name << " = " << buffer << " /*" << value << "*/";
        xmlFree (name);
        xmlFree (value);
        is_first = false;
      }
    }
    out << "\n";
    out--;
    out << "};\n";
    out << "\n";
    set (Attributes::eDECLARED);
  }

  //! I don't think that we have any of these...
  /*! \note: if we find any of these in distilled classes it is not hard to do
   */
  virtual bool canPoint () const { return false; }

private:

  UtString mName;

};

class ArrayType : public NxsType {
public:

  ArrayType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory, 
    NxsType *element) : 
    NxsType (node, typeId, xmltype, factory, 0), mElement (element)
  {
    INFO_ASSERT (!xmlStrcasecmp (node->name, sARRAYTYPE), "node is not an array");
  }

  virtual ~ArrayType () {}

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &) const
  { INFO_ASSERT (false, "unimplemented"); }

  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    UtString b0;
    out << "// " << mElement->compose (&b0) << " " << fieldName << " [];" << " (" << __LINE__ << ")\n";
  }

  virtual bool isOmitted () const
  { return true; }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    UtString b0;
    *s << "array of " << mElement->compose (&b0);
    return s->c_str ();
  }

  //! Emit any required schema declaration for the type
  virtual void emitSchema (CodeStream &out, Schema *)
  {
    INFO_ASSERT (!isOmitted (), "outputing omitted type");
    UtString b0;
    out << "// schema for arrary type " << compose (&b0) << "\n";
    out << "\n";
    set (Attributes::eDECLARED);
  }

  //! Cannot be pointed to
  virtual bool canPoint () const { return false; }

private:

  NxsType *mElement;

};

class XmlClass;                         // forward declaration

class ClassType : public NxsType {
public:

  ClassType (XmlClass *theclass, TypeFactory &factory);
  virtual ~ClassType () {}

  //! \return iff the type is a class type
  virtual bool isClassType () const { return true; } // yes I am...

  virtual const char *getName () const;

  //! Emit the schema for a class
  virtual void emitForwardDeclaration (CodeStream &out);

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &out) const
  { out << getName (); }

  //! Emit the schema for a class
  virtual void emitSchema (CodeStream &out, Schema *schema);

  //! Close the type
  virtual void close (TypeFactory &);

  //! Populate the field list
  void populateFieldList (TypeFactory &);

  //! Output the declaration of a field of this type
  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    out << getName () << " " << fieldName << "; // distilled class instance "
        << mId  << " (" << __LINE__ << ")\n";
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    *s << "class " << getName ();
    return s->c_str ();
  }

  //! Of course we can...
  virtual bool canPoint () const { return true; }

private:

  //! Just emit the declaration part, without the semi-colon
  void emitClassDecl (CodeStream &);

  XmlClass *mClass;
  Field::List mFields;                  //!< the fields of class

};

class BuiltinType : public NxsType {
public:

  enum Flavour {
    eBool,
    eCarbonUInt32,
    eChar,
    eUnsignedChar,
    eDouble,
    eInt,
    eSInt16,
    eSInt32,
    eShortUnsignedInt,
    eUInt16,
    eUInt32,
    eUnsignedInt,
    eVoid,
    eShortInt,
    eNBuiltin
  };

  BuiltinType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory,
    Flavour flavour, const char *name) :
    NxsType (node, typeId, xmltype, factory, Attributes::eBUILTIN), mFlavour (flavour), mName (name) 
  {
    // assume that these are automatically declared
    set (Attributes::eDECLARED);
  }

  virtual ~BuiltinType () {}

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &out) const
  { out << getName (); }

  //! \return the name of the type
  virtual const char *getName () const { return mName; }

  //! \return iff this is the void type
  virtual bool isVoid () const { return mFlavour == eVoid; }

  static bool build (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory,  NxsType **type)
  {
    xmlChar *name = xmlGetProp (node, sNAME);
#define PROBE(_type_, _name_, _flavour_) if (!xmlStrcmp (name, (xmlChar *) #_type_)) { \
      *type = new BuiltinType (node, typeId, xmltype, factory, _flavour_, #_name_); \
      xmlFree (name); \
      return true; \
    }
    PROBE (bool, bool, eBool);
    PROBE (char, char, eChar);
    PROBE (unsigned char, unsigned_char, eUnsignedChar);
    PROBE (int, int, eInt);
    PROBE (short unsigned int, unsigned_short, eShortUnsignedInt);
    PROBE (short int, short_int, eShortInt);
    PROBE (unsigned int, unsigned_it, eUnsignedInt);
    PROBE (double, double, eDouble);
    PROBE (void, void, eVoid);
    PROBE (CarbonUInt32, CarbonUInt32, eCarbonUInt32);
    PROBE (SInt16, SInt16, eSInt16);
    PROBE (SInt32, SInt32, eSInt32);
    PROBE (UInt16, UInt16, eUInt16);
    PROBE (UInt32, UInt32, eUInt32);
    xmlFree (name);
    return false;
  }

  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    out << getName () << " " << fieldName << "; // builtin (" << __LINE__ << ")\n";
  }


  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    *s << "builtin " << getName ();
    return s->c_str ();
  }

  //! I don't think that we have any of these...
  /*! \note: if we find any of these in distilled classes it is not hard to do
   */
  virtual bool canPoint () const { return false; }

private:

  const Flavour mFlavour;               //!< which builtin
  const char *mName;                    //!< the name of the type
};

class NxsContainerType : public NxsType {
public:

  enum Flavour {
    eStdPair,
    eUtArray,                           // UtArray <type>
    eUtDeque,                           // UtDeque <type>
    eUtHashMap,                         // UtHashMap <type, type, ...>
    eUtHashSet,                         // UtHashSet <type ...>
    eUtList,                            // UtList <type>
    eUtMap,                             // UtMap <type, type, ...>
    eUtSet,                             // UtSet <type>
    eUtVector,                          // UtVector <type>
    eNUNetRefMultiMap,                  // NUNetRefMultiMap <type, type>
    eNFlavours
  };

  enum {
    MAX_ARGS = 2
  };

  NxsContainerType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory,
    NxsType *arg0, NxsType *arg1 = NULL) :
    NxsType (node, typeId, xmltype, factory, Attributes::eCONTAINER)
  {
    mArg [0] = arg0;
    mArg [1] = arg1;
    if (arg0 != NULL && arg0->isOmitted ()) {
      set (Attributes::eOMITTED);
    }
    if (arg1 != NULL && arg1->isOmitted ()) {
      set (Attributes::eOMITTED);
    }
  }

  virtual ~NxsContainerType () {}

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &) const
  { INFO_ASSERT (false, "unimplemented"); }

  virtual void emitFieldDecl (CodeStream &out, const char *name) const
  {
    UtString b0;
    out << "// " << compose (&b0) << " " << name << " - this is a container (" << __LINE__ << ")\n";
  }

  //! Close the type
  virtual void close (TypeFactory &) {}

  static bool isTag (const char *s, Flavour *flavour, UInt32 *nargs)
  {
    if (!strcmp (s, "UtHashSet")) {
      *flavour = eUtHashSet;
      *nargs = 1;
      return true;
    } else if (!strcmp (s, "UtVector")) {
      *flavour = eUtVector;
      *nargs = 1;
      return true;
    } else if (!strcmp (s, "UtArray")) {
      *flavour = eUtArray;
      *nargs = 1;
      return true;
    } else if (!strcmp (s, "UtList")) {
      *flavour = eUtList;
      *nargs = 1;
      return true;
    } else if (!strcmp (s, "UtDeque")) {
      *flavour = eUtDeque;
      *nargs = 1;
      return true;
    } else if (!strcmp (s, "UtSet")) {
      *flavour = eUtSet;
      *nargs = 1;
      return true;
    } else if (!strcmp (s, "std::pair")) {
      *flavour = eStdPair;
      *nargs = 2;
      return true;
    } else if (!strcmp (s, "UtHashMap")) {
      *flavour = eUtHashMap;
      *nargs = 2;
      return true;
    } else if (!strcmp (s, "UtMap")) {
      *flavour = eUtMap;
      *nargs = 2;
      return true;
    } else if (!strcmp (s, "NUNetRefMultiMap")) {
      *flavour = eNUNetRefMultiMap;
      *nargs = 1;
      return true;
    } else {
      return false;
    }
  }

  static bool build (xmlNodePtr srcnode, xmlChar *typeId, XmlType *xmltype, 
    TypeFactory &factory, const Flavour flavour, NxsType *args [], NxsType **type);

  virtual void emitForwardDeclaration (CodeStream &out)
  {
    UtString b0;
    out << "// forward declaration for container " << compose (&b0) << "\n\n";
    set (Attributes::eDECLARED);
  }

  virtual void emitSchema (CodeStream &out, Schema *)
  {
    INFO_ASSERT (!isOmitted (), "outputing omitted type");
    UtString b0;
    out << "// declaration for container " << compose (&b0) << "\n\n";
    set (Attributes::eDECLARED);
  }

  //! Of course we can...
  virtual bool canPoint () const { return true; }

  virtual bool isOmitted () const
  { return true; }

protected:

  NxsType *mArg [MAX_ARGS];

};

#define UNARY_CONTAINER(_tag_) \
class _tag_##Type : public NxsContainerType { \
public: \
  _tag_##Type (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, \
    TypeFactory &factory, NxsType *a) : \
    NxsContainerType (node, typeId, xmltype, factory, a) \
  {} \
  virtual ~_tag_##Type () {} \
  virtual const char *compose (UtString *s) const \
  { \
    UtString b0; \
    *s << #_tag_ " <" << mArg [0]->compose (&b0) << ">"; \
    return s->c_str (); \
  } \
}

#define BINARY_CONTAINER(_tag_) \
class _tag_##Type : public NxsContainerType { \
public: \
  _tag_##Type (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, \
    TypeFactory &factory, NxsType *a, NxsType *b) : \
    NxsContainerType (node, typeId, xmltype, factory, a, b) \
  {} \
  virtual ~_tag_##Type () {} \
  virtual const char *compose (UtString *s) const \
  { \
    UtString b0, b1; \
    *s << #_tag_ " <" << mArg [0]->compose (&b0) << ", " << mArg [1]->compose (&b1) << ">"; \
    return s->c_str (); \
  } \
}

UNARY_CONTAINER (UtVector);
UNARY_CONTAINER (UtHashSet);
UNARY_CONTAINER (UtArray);
UNARY_CONTAINER (UtList);
UNARY_CONTAINER (UtDeque);
UNARY_CONTAINER (UtSet);
UNARY_CONTAINER (NUNetRefMultiMap);
BINARY_CONTAINER (UtHashMap);
BINARY_CONTAINER (UtMap);
BINARY_CONTAINER (StdPair);

#undef UNARY_CONTAINER
#undef BINARY_CONTAINER

/*static*/ bool NxsContainerType::build (xmlNodePtr srcnode, xmlChar *typeId, 
  XmlType *xmltype, TypeFactory &factory, const Flavour flavour, NxsType *arg [], 
  NxsType **type)
{
  switch (flavour) {
  case eUtVector:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    *type = new UtVectorType (srcnode, typeId, xmltype, factory, arg [0]);
    return true;
    break;
  case eNUNetRefMultiMap:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    *type = new NUNetRefMultiMapType (srcnode, typeId, xmltype, factory, arg [0]);
    return true;
    break;
  case eUtHashSet:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    *type = new UtHashSetType (srcnode, typeId, xmltype, factory, arg [0]);
    return true;
    break;
  case eUtArray:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    *type = new UtArrayType (srcnode, typeId, xmltype, factory, arg [0]);
    return true;
    break;
  case eUtMap:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    INFO_ASSERT (arg [1] != NULL, "missing template argument");
    *type = new UtMapType (srcnode, typeId, xmltype, factory, arg [0], arg [1]);
    return true;
    break;
  case eUtHashMap:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    INFO_ASSERT (arg [1] != NULL, "missing template argument");
    *type = new UtHashMapType (srcnode, typeId, xmltype, factory, arg [0], arg [1]);
    return true;
    break;
  case eUtList:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    *type = new UtListType (srcnode, typeId, xmltype, factory, arg [0]);
    return true;
    break;
  case eUtSet:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    *type = new UtSetType (srcnode, typeId, xmltype, factory, arg [0]);
    return true;
    break;
  case eStdPair:
    INFO_ASSERT (arg [0] != NULL, "missing template argument");
    INFO_ASSERT (arg [1] != NULL, "missing template argument");
    *type = new StdPairType (srcnode, typeId, xmltype, factory, arg [0], arg [1]);
    return true;
    break;
  default:
    INFO_ASSERT (false, "unimplemented");
    return false;
    break;
  }
}

class TypedefType : public NxsType {
public:

  TypedefType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory,
    UtString name, NxsType *defn) :
    NxsType (node, typeId, xmltype, factory, 0), mDefn (defn), mName (name)
  {
    if (defn->isOmitted ()) {
      set (Attributes::eOMITTED);
    }
    if (defn->isContainer ()) {
      set (Attributes::eCONTAINER);
    }
  }

  virtual ~TypedefType () {}

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &out) const
  { out << getName (); }

  virtual const char *getName () const { return mName.c_str (); }

  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    UtString b0;
    compose (&b0);
    out << getName () << " " << fieldName << "; // " << b0  << " (" << __LINE__ << ")\n";
  }

  virtual bool isOmitted () const
  { return NxsType::isOmitted () || mDefn->isOmitted (); }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    UtString b0;
    mDefn->compose (&b0);
    *s << "typedef " << getName () << " = " << b0;
    return s->c_str ();
  }

  virtual void emitForwardDeclaration (CodeStream &out)
  {
    UtString b0;
    out << "// forward declaration for typedef " << compose (&b0) << "\n\n";
    set (Attributes::eDECLARED);
  }

  virtual void emitSchema (CodeStream &out, Schema *)
  {
    INFO_ASSERT (!isOmitted (), "outputing omitted type");
    UtString b0;
    out << "// declaration for typedef " << compose (&b0) << "\n\n";
    set (Attributes::eDECLARED);
  }

  virtual bool canPoint () const { return mDefn->canPoint (); }

private:

  NxsType *mDefn;                       //!< the definition of the typedef
  UtString mName;                       //!< name of the type
};

class BaseType : public NxsType {
public:

  BaseType (XmlClass *theclass, TypeFactory &factory);

  virtual ~BaseType () {}

  //! Emit the full NXS name for the type
  virtual void emitName (CodeStream &out) const
  { out << getName (); }

  virtual const char *getName () const { return mName.c_str (); }

  virtual void emitSchema (CodeStream &out, Schema *)
  {
    INFO_ASSERT (!isOmitted (), "outputing omitted type");
    out << "// " << getName () << " is a base type\n\n";
  }

  virtual void emitFieldDecl (CodeStream &out, const char *fieldName) const
  {
    out << getName () << " " << fieldName << "; // base type " << mId  << " (" << __LINE__ << ")\n";
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    *s << "base " << getName ();
    return s->c_str ();
  }

  //! Of course we can...
  virtual bool canPoint () const { return true; }

private:

  UtString mName;

};

//! \class FieldMapper
/*! A mapping from node "id" to the XML object for fields
 */
class FieldMapper : public NodeMapper {
public:

  FieldMapper (MsgContext &msg, SourceMapper &sourcemap, XmlClassMap &classes, xmlDocPtr doc) : 
    NodeMapper (msg, sourcemap), mClasses (classes), mAttributes (msg, sourcemap)
  { 
    buildMap (doc, sFIELD); 
  }

  virtual ~FieldMapper () {}

  bool find (xmlChar *id, Field **field) const
  {
    Node::Map::const_iterator found;
    if ((found = NodeMapper::find (id)) == end ()) {
      *field = NULL;
      return false;
    } else {
      *field = static_cast <Field *> (found->second);
      return true;
    }
  }

protected:
  
  XmlClassMap &mClasses;

  bool findClass (xmlNodePtr node, Field *field, XmlClass **theclass)
  {
    xmlChar *id = xmlGetProp (node, sCONTEXT);
    if (id == NULL) {
      XMLERROR (node, mSource, mMsg, OrphanField, field->getName ());
      return false;
    } else if (!mClasses.find (id, theclass)) {
      XMLERROR (node, mSource, mMsg, BadContext, (const char *) id);
      xmlFree (id);
      return false;
    } else {
      xmlFree (id);
      return true;
    }
  }

  //! \return the node encapsulated in a Field instance
  virtual Node *wrapNode (xmlChar *id, xmlNodePtr node) 
  {
    UInt32 flags = mAttributes (node, Attributes::eFIELD_ATTRIBUTES);
    return new Field (id, node, flags);
  }

private:
  
  Attributes mAttributes;               //!< scanner for field "attributes" attribute

};

//! Populate the field vector for the class
void ClassType::populateFieldList (TypeFactory &types)
{
  TypeFactory::Context context (TypeFactory::Context::eCLOSING_FIELDS, mClass, types);
  if (mClass->isMarked ()) {
    // The class contains a member with a type relying on the class. For
    // example, if class C1 constains a field of type C1*. Just return so that
    // we do not dive into a recursive well and never see the light of day
    // again.
    return;
  }
  mClass->setMark ();
  //UtIO::cout () << __LINE__ << ": " << __FUNCTION__ << ": " << getName () << "\n";
  xmlChar *members = xmlGetProp (mNode, sMEMBERS);
  if (members == NULL) {
    // If a class has no members, then GCCXML just omits the members attribute
    // instead if writing an empty attribute.
  } else {
    IdList list (members);
    xmlStringList::iterator it;
    for (it = list.begin (); it != list.end (); it++) {
      Field *field;
      NxsType *type;
      if (!types.find (*it, &field)) {
        // the item does not denote a field member
      } else if (field->isOmitted ()) {
        // ignoring this field for now
      } else if (!types.infer (field, &type)) {
        types.note (this, field);
        XMLERROR (field->getNode (), types.getSource (), types.getMsg (), NoSchemaType, field->getName ());
      } else if (type->isOmitted ()) {
        // do nothing
      } else {
        types.note (this, field);
        field->setType (type);
        mFields.push_back (field);
      }
    }
    xmlFree (members);
  }
  mClass->clearMark ();
}

//! \class ClassGraph
class ClassGraph {
public:

  ClassGraph (MsgContext &msg, SourceMapper &sourcemap, XmlClassMap &classes) :
    mMsg (msg), mSource (sourcemap), mClasses (classes)
  {}

  //! \return iff the class graph is empty
  bool empty () const { return mEdges.empty (); }

  //! Build the class graph
  void build ()
  {
    // construct the set of edges
    XmlClassMap::iterator i;
    for (i = mClasses.begin (); i != mClasses.end (); i++) {
      XmlClass *theclass = static_cast <XmlClass *> (i->second);
      XmlClass::Set::iterator j;
      for (j = theclass->beginSub (); j != theclass->endSub (); j++) {
        XmlClass *subclass = *j;
        subclass->incInDegree ();
        mEdges.insert (Edges::value_type (theclass, subclass));
      }
    }
    // identify the classes with in-degree zero - these are the starting points
    // for any depth first traversal
    for (i = mClasses.begin (); i != mClasses.end (); i++) {
      XmlClass *theclass = static_cast <XmlClass *> (i->second);
      if (theclass->getInDegree () == 0) {
        // this is a root node
        mRoot.insert (theclass);
      }
    }
    INFO_ASSERT (!mRoot.empty (), "no root nodes - this is too weird to continue");
    // propagate class attributes around the DAG
    XmlClass::Set::iterator roots;
    for (roots = mRoot.begin (); roots != mRoot.end (); roots++) {
      (*roots)->propagateSynthesisedAttributes (mMsg, mSource, Attributes::eCLASS_ATTRIBUTES, 0);
    }
    if (mMsg.getExitStatus () != 0) {
      // If propagation runs into a cycle an Xml2NxsClassLoop error is
      // reported. Cycles in the inheritance graph are not legal, and the code
      // should not even compile.
      return;                           // bail out...
    }
  }

  void prune ()
  {
    XmlClassMap::iterator i;
    // prune the graph to remove any classes that are not distilled
    for (i = mClasses.begin (); i != mClasses.end (); i++) {
      XmlClass *theclass = static_cast <XmlClass *> (i->second);
      if (!theclass->isDistilled ()) {
        // find all the edges out of this node
        std::pair <Edges::iterator, Edges::iterator> edges = mEdges.equal_range (theclass);
        // adjust the in degree of the subclasses
        for (Edges::iterator j = edges.first; j != edges.second; j++) {
          INFO_ASSERT (j->first == theclass, "weird...");
          XmlClass *subclass = j->second;
          INFO_ASSERT (subclass->getInDegree () > 0, "subclass has in-degree zero");
          subclass->decInDegree ();
          if (subclass->getInDegree () == 0 && subclass->isDistilled ()) {
            // this is a root of the distilled class hierarchy
            mRoot.insert (subclass);
          }
        }
        // remove the edges from the graph
        mEdges.erase (edges.first, edges.second);
        XmlClass::Set::iterator found;
        if (theclass->getInDegree () == 0 && ((found = mRoot.find (theclass)) != mRoot.end ())) {
          // remove the class from the set of root nodes
          mRoot.erase (found);
        }
      }
    }
  }

  //! Flatten the graph into a topological ordering
  /*! \note This topological sort trashes the graph
   */
  void flattenGraph (XmlClass::List *sorted)
  {
    XmlClass::List worklist;
    XmlClass::Set::iterator roots;
    // initialise the worklist with the root nodes
    for (roots = mRoot.begin (); roots != mRoot.end (); roots++) {
      worklist.push_back (*roots);
    }
    // the bog-standard topological sort algorithm
    while (!worklist.empty ()) {
      // the first item on the worklist goes to the back of the sorted list
      XmlClass *theclass = worklist.front ();
      worklist.pop_front ();
      INFO_ASSERT (theclass->getInDegree () == 0, "topological sort crashed and burned");
      sorted->push_back (theclass);
      // now follow the edges out of this class
      std::pair <Edges::iterator, Edges::iterator> edges = mEdges.equal_range (theclass);
      // adjust the in degree of the subclasses
      for (Edges::iterator j = edges.first; j != edges.second; j++) {
        INFO_ASSERT (j->first == theclass, "weird...");
        XmlClass *subclass = j->second;
        INFO_ASSERT (subclass->getInDegree () > 0, "subclass has in-degree zero");
        subclass->decInDegree ();
        if (subclass->getInDegree () == 0) {
          // the subclass has no incoming edges left in the graph so push it
          // onto the end of the worklist
          worklist.push_back (subclass);
        }
      }
      // remove the edges from the graph
      mEdges.erase (edges.first, edges.second);
    }
  }

private:

  MsgContext &mMsg;                     //!< error stream
  SourceMapper &mSource;                //!< maps XMl nodes to locations
  XmlClassMap &mClasses;                //!< all the class and struct nodes in the XML document

  /*! The edge maps map classes to subclasses. */
  typedef UtMultiMap <XmlClass *, XmlClass *> Edges;
  Edges mEdges;                         //!< map a class to immediate subclasses
  XmlClass::Set mRoot;                  //!< "root" nodes of the DAG

};

class Schema {
public:

  Schema (MsgContext &msg, SourceMapper &sourcemap, xmlDocPtr doc) :
    mMsg (msg), mSource (sourcemap), mDoc (doc),
    mNamespaces (msg, mSource, doc),
    mTypes (mMsg, mSource, doc, *this, mFields),
    mFields (msg, mSource, mTypes.getClassMap (), doc),
    mGraph (mMsg, mSource, mTypes.getClassMap ())
  {
  }

  virtual ~Schema () {}

  bool build (Context &context)
  {
    // configure the type factory
    mTypes.collectTypeNodes (xmlDocGetRootElement (mDoc));
    mTypes.propagateOmittedClasses ();
    // work out the type hierarchy
    inferClassHierarchy ();
    // the inferred class hierarchy is usefull to work out why the tool is
    // trying to distill a class
    dumpClassHierarchy (context.getDiag ());
    mTypes.explain (context.getDiag ());
    return (mMsg.getExitStatus () == 0);
  }

  void emitNxs (CodeStream &out)
  {
    mTypes.emitSchema (out, this, mGraph);
  }

  //! \return a fully qualified name
  UtString qualifiedName (xmlNodePtr node, const char *flavour) const;

  //! Find the source location for a type
  bool getSrcLoc (const NxsType *type, SourceLocator *loc) const
  {
    XmlType *xmltype = type->getXmlType ();
    if (xmltype == NULL) {
      return false;
    } else {
      return mSource.getSrcLoc (xmltype->getNode (), loc);
    }
  }

private:

  MsgContext &mMsg;                     //!< message stream
  SourceMapper &mSource;                //!< maps XML nodes to locations
  xmlDocPtr mDoc;

  NamespaceMapper mNamespaces;
  TypeFactory mTypes;                   //!< type construction object
  FieldMapper mFields;
  ClassGraph mGraph;                    //!< graph of class hierarchy

  void listSuperClasses (UtOStream &s, XmlClass *theclass) const
  {
    UInt32 n = 0;
    XmlClass::Set::const_iterator it;
    s << theclass->getName ();
    for (it = theclass->beginSuper (); it != theclass->endSuper (); it++) {
      if (n++ == 0) {
        s << "(";
      } else {
        s << ", ";
      }
      listSuperClasses (s, *it);
    }
    if (n > 0) {
      s << ")";
    }
  }

  void dumpClassHierarchy (UtOStream &s)
  {
    XmlClassMap classes = mTypes.getClassMap ();
    for (XmlClassMap::iterator it = classes.begin (); it != classes.end (); it++) {
      if (it->second->isDistilled ()) {
        listSuperClasses (s, it->second);
        s << "\n";
      }
    }
  }

  void inferClassHierarchy ()
  {
    // Configure the super and subclass relationships
    XmlClassMap classes = mTypes.getClassMap ();
    for (XmlClassMap::iterator it = classes.begin (); it != classes.end (); it++) {
      XmlClass *theclass = it->second;
      xmlNodePtr child;
      xmlChar *baseid;
      XmlClass *baseclass;
      for (child = theclass->getNode ()->xmlChildrenNode; child != NULL; child = child->next) {
        if (xmlStrcasecmp (child->name, sBASE) != 0) {
          // this is not a Base XML node
        } else if ((baseid = xmlGetProp (child, sTYPE)) == NULL) {
          XMLERROR (child, mSource, mMsg, NoId, "type");
        } else if (!classes.find (baseid, &baseclass)) {
          XMLERROR (child, mSource, mMsg, NoId, "class");
          xmlFree (baseid);
        } else {
          theclass->setSuper (baseclass);
          xmlFree (baseid);
        }
      }
    }
    // build the class graph
    mGraph.build ();
    // Force the type factory to distill all the classes marked as distilled
    // in the graph. This will prime the closure worklist with all the classes.
    for (XmlClassMap::iterator it = classes.begin (); it != classes.end (); it++) {
      NxsType *type;
      if (it->second->isDistilled ()) {
        mTypes.distill (it->second, &type);
      }
    }
    // form the closure of the types
    mTypes.close ();
#if 0
    if (mGraph.empty ()) {
      mMsg.Xml2NxsNoDistilledClasses ();
      return;
    }
#endif
    // locate all the fields of the distilled classes and tie them to their
    // class
    
  }

};

//! \return the fully qualified name
UtString Node::qualifiedName (const Schema *schema, const char *flavour) const
{ return schema->qualifiedName (mNode, flavour); }

//! \return the fully qualified name
UtString TypeFactory::qualifiedName (xmlNodePtr node, const char *flavour) const
{ return mSchema.qualifiedName (node, flavour); }

//! Infer a type from an XML "id" value
bool TypeFactory::infer (Field *field, NxsType **type)
{
  Context context (field->getNode (), Context::eFIND_FIELD, field->getName (), *this);
  xmlChar *typeId = xmlGetProp (field->getNode (), sTYPE);
  if (typeId == NULL) {
    XMLERROR (field->getNode (), mSource, mMsg, NoTypeId, "field");
    traceBack ();
    return false;
  } else if (!infer (field->getNode (), typeId, type)) {
    return false;
  } else {
    return true;
  }
}

bool TypeFactory::infer (xmlNodePtr srcnode, xmlChar *typeId, NxsType **type)
{
  Context context (srcnode, Context::eFIND_TYPEID, typeId, *this);
  INFO_ASSERT (typeId != NULL, "no type id");
  XmlType *xmltype;
  if (mNxsTypes.find (typeId, type)) {
    // found the type in the map of already distilled types
    return true;
  } else if (!mXmlTypes.find (typeId, &xmltype)) {
    // The type id is not in the map of XML types. This means that the initial
    // scan of the XML document did not correctly recognise the XML node as a
    // type.
    XMLERROR (srcnode, mSource, mMsg, UngatheredType, (char *) typeId);
    traceBack ();
    xmlFree (typeId);
    mMsg.Xml2NxsUnimplemented ();
    return false;
  } else if (!distill (xmltype, type)) {
    // Distillation of the XML type into an NXS type failed. This means that we
    // have a type definition that we have not yet worked out how to realise as
    // an NXS type.
#if 0
    XMLERROR (srcnode, mSource, mMsg, UndistillableType, (char *) typeId);
    traceBack ();
    xmlFree (typeId);
    mMsg.Xml2NxsUnimplemented ();
#endif
    return false;
  } else {
    return true;                        // found an NXS type
  }

  return false;
}

bool TypeFactory::NoName (xmlNodePtr node, const char *flavour)
{
  XMLERROR (node, mSource, mMsg, NoName, flavour);
  return false;
}

bool TypeFactory::NoTypeId (xmlNodePtr node, const char *flavour)
{
  XMLERROR (node, mSource, mMsg, NoTypeId, flavour);
  return false;
}

bool TypeFactory::inferContainer (xmlNodePtr srcnode, xmlChar *typeId, XmlType *xmltype, 
  const char *s, NxsType **type)
{
  *type = NULL;
  Context context (srcnode, Context::eFIND_CONTAINER, s, *this);
  // a predetermined set of template classes are explicitly recognised and handled.
  NxsContainerType::Flavour flavour = NxsContainerType::eNFlavours;
  // The first part of the string is the container name
  char container [256];
  // The template instances will have a number of actual type parameters. The
  // number of parameters is determined by container. 
  UInt32 expecting = 0;                 // number of expected type arguments
  char name [256];                      // string with the type arguments
  UInt32 ntypes = 0;                    // number of type arguments found
  NxsType *arg [NxsContainerType::MAX_ARGS]; // the infered type arguments
  // state machine variables
  int state = 0;                        // state of the machine
  UInt32 n = 0;                         // index into the string
  UInt32 m = 0;                         // start of the template actual parameter
  UInt32 nesting = 0;                   // level of <...> nesting
  bool is_pointer = false;              // set if the type argument is a pointer
  bool is_templated = false;            // set if the type argument is templated
  // and now, the state machine that plucks apart the UtFoo<a,b,c,d...> string...
  while (state >= 0) {
    switch (state) {
    case 0:
      if (s [n] == '\0') {
        return false;                   // hit the end of the string
      } else if (isspace (s [n])) {
        n++;                            // consume whitespace
      } else if (isalnum (s [n])) {
        // hit the start of the container name
        m = 0;                          // length of the container name so far
        state++;
      } else {
        return false;                   // not a template instantiation
      }
      break;
    case 1:
      // Acrete the characters of the container name
      if (s [n] == '\0') {
        return false;                   // end of the string and it's not a container
      } else if (!isalnum (s [n]) && s [n] != '_' && s [n] != ':') {
        // hit the end of the container name
        container [m] = '\0';           // nul terminate the name
        state++;
      } else if (m >= sizeof (container) - 1) {
        // Hit the end of the container name. Just assume that this is not one
        // of the containers that we can handle here.
        return false;
      } else {
        container [m++] = s [n++];
      }
      break;
    case 2:
      // Hit the end of the container name.
      if (s [n] == '\0') {
        return false;                   // not a template instance
      } else if (isspace (s [n])) {
        n++;                            // ignore whitespace
      } else if (s [n] == '<') {
        // OK, this is a template instance
        n++;                            // consume the '<'
        nesting = 1;                    // seen one '<' instance
        state++;
        // Initialise the argument vector to NULLs so that we can test that all
        // the necessary arguments have been constructed when we come to
        // instantiate the wrapper for the container.
        for (UInt32 i = 0; i < NxsContainerType::MAX_ARGS; i++) {
          arg [i] = NULL;
        }
      } else {
        return false;                   // not a template instance
      }
      break;
    case 3:
      if (!NxsContainerType::isTag (container, &flavour, &expecting)) {
        // An unhandled container type
        XMLERROR (srcnode, mSource, mMsg, UnhandledTemplateInstance, container);
        traceBack ();
        return true;                    // weird
      } else {
        state++;
      }
      break;
    case 4:
      // Looking for the start of the next type argument or the end
      INFO_ASSERT (nesting == 1, "scanner jammed");
      if (s [n] == '\0') {
        // We are inside the template actual parameters, so we expect at least
        // the closing '>' before the end of the string
        XMLERROR (srcnode, mSource, mMsg, MalformedTemplateInstance, s);
        traceBack ();
        return true;                    // weird
      } else if (isspace (s [n])) {
        n++;                            // consume whitespace
      } else if (isalnum (s [n])) {
        is_templated = false;
        is_pointer = false;
        m = 0;
        state++;                        // start of the type name argument
      } else if (s [n] == '>' && nesting == 1) {
        state = -1;                     // final state
      } else {
        // Should have found either the closing > or the start of the next
        // argument to the template instantiation
        XMLERROR (srcnode, mSource, mMsg, MalformedTemplateInstance, s);
        traceBack ();
        return true;                    // weird
      }
      break;
    case 5:
      // Acreting a typename argument
      if (s [n] == '\0') {
        XMLERROR (srcnode, mSource, mMsg, MalformedTemplateInstance, s);
        traceBack ();
        return true;;                   // weird
      } else if (nesting == 1 && (s [n] == ',' || s [n] == '>')) {
        // hit the end of the argument
        name [m] = '\0';
        state++;
      } else if (m >= sizeof (name) - 1) {
        XMLERROR (srcnode, mSource, mMsg, MalformedTemplateInstance, s);
        traceBack ();
        return true;                    // weird
      } else if (s [n] == '<') {
        is_templated = true;
        name [m++] = s [n++];
        nesting++;
      } else if (s [n] == '>') {
        name [m++] = s [n++];
        nesting--;
      } else if (isspace (s [n]) && m == 5 && !strncmp (name, "const", 5)) {
        // We ignore the leading const on a type name
        m = 0;                          // reset the index into the output string
      } else if (isspace (s [n]) && m == 0) {
        n++;                            // strip out leading spaces in the type name
      } else if (s [n] == '*' && nesting == 1) {
        state++;
      } else {
        name [m++] = s [n++];
      }
      break;
    case 6:
      // Check for the presence of a '*'
      name [m] = '\0';
      if (isspace (s [n])) {
        n++;
      } else if (s [n] == '*') {
        n++;
        is_pointer = true;
        state++;
      } else {
        state++;
      }
      break;
    case 7:
      // We have hit the end of a typename argument.
      INFO_ASSERT (nesting == 1, "scanner jammed");
      if (ntypes >= expecting) {
        // This is not a type argument so just skip it
        state++;
      } else if (is_templated && !inferContainer (srcnode, NULL, NULL, name, &arg [ntypes])) {
        *type = NULL;
        return true;                    // this is a container
      } else if (is_templated && arg [ntypes] == NULL) {
        *type = NULL;
        return true;
      } else if (is_templated) {
        // Recursively constructed the argument... cool...
        ntypes++;
        state++;
      } else if (!infer (srcnode, name, &arg [ntypes])) {
        XMLERROR (srcnode, mSource, mMsg, CannotInferType, name);
        traceBack ();
        return true;                    // could not identify the type argument
      } else if (is_pointer) {
        arg [ntypes] = new PointerType (srcnode, NULL, NULL, *this, arg [ntypes]);
        ntypes++;
        state++;
      } else {
        ntypes++;
        state++;
      }
      break;
    case 8:
      if (isspace (s [n])) {
        n++;                            // consume the space
      } else if (s [n] == ',') {
        n++;
        state = 4;
      } else {
        state = 4;
      }
      break;
      // Looking at the start of a type name argument
    default:
      INFO_ASSERT (false, "container inference scanner jammed");
      break;
    }
  }
  if (ntypes < expecting) {
    XMLERROR (srcnode, mSource, mMsg, NotEnoughTemplateArgs, s, expecting, ntypes);
    traceBack ();
    return false;
  } else {
    return NxsContainerType::build (srcnode, typeId, xmltype, *this, flavour, arg, type);
  }
}
  
//! Infer a type from the name
bool TypeFactory::infer (xmlNodePtr srcnode, const char *name, NxsType **type)
{
  Context context (srcnode, Context::eFIND_NAME, name, *this);
  xmlChar *typeId;
  if (!mXmlTypes.find (name, &typeId)) {
    // Cannot identify this named type. Named types are really only needed when
    // burrowing into the type actual parameter in templated type constructs,
    // such as "UtVector<NUNet*>". In (maybe?) all other instances, the type is
    // inferred from the type field of the XML node.
    return false;
  } else if (!infer (srcnode, typeId, type)) {
    return false;
  } else {
    return true;
  }
}

//! If a class is omitted, then mark all classes with that class as context as omitted
void TypeFactory::propagateOmittedClasses ()
{
  // WARNING - this is n-squared
  // If performance turns out to be a problem then a graph of the context
  // relation should be construct and omitted propagated around the graph.
  bool changed = true;
  while (changed) {
    changed = false;
    XmlClassMap::const_iterator it;
    for (it = mClasses.begin (); it != mClasses.end (); it++) {
      XmlClass *theclass = it->second;
      xmlChar *contextId;
      XmlType *context;
      if (theclass->isOmitted ()) {
        // this class is already marked omitted so no more is needed here
      } else if ((contextId = xmlGetProp (theclass->getNode (), sCONTEXT)) == NULL) {
        // class is not embedded
      } else if (!mXmlTypes.find (contextId, &context)) {
        xmlFree (contextId);
      } else if (context->isOmitted ()) {
        // The context is marked as omitted so also mark this class
        theclass->set (Attributes::eOMITTED);
        changed = true;
        xmlFree (contextId);
      } else {
        xmlFree (contextId);
      }
    }
  }
}

void TypeFactory::note (Reason *reason)
{
  mReasons.insert (Reason::Map::value_type (reason->getSubject (), reason));
  mReasonList.push_back (reason);
}

void TypeFactory::note (ClassType *parent, Field *field)
{
  xmlChar *typeId = xmlGetProp (field->getNode (), sTYPE);
  XmlType *xmltype;
  if (typeId == NULL) {
    // no type id
  } else if (!mXmlTypes.find (typeId, &xmltype)) {
    // no type
    xmlFree (typeId);
  } else {
    note (new Reason (xmltype, parent->getXmlType (), field));
    xmlFree (typeId);
  }
}

void TypeFactory::note (UnionType *parent, Field *field)
{
  xmlChar *typeId = xmlGetProp (field->getNode (), sTYPE);
  XmlType *xmltype;
  if (typeId == NULL) {
    // no type id
  } else if (!mXmlTypes.find (typeId, &xmltype)) {
    // no type
    xmlFree (typeId);
  } else {
    note (new Reason (xmltype, parent->getXmlType (), field));
    xmlFree (typeId);
  }
}

void TypeFactory::explain (UtOStream &s)
{
  s << "#### REASONS ####\n";
  for (Reason::List::const_iterator i = mReasonList.begin (); i != mReasonList.end (); i++) {
    (*i)->explain (s);
  }
}

void TypeFactory::explain (UtOStream &s, XmlType *type) const
{
  std::pair <Reason::Map::const_iterator, Reason::Map::const_iterator> reasons;
  reasons = mReasons.equal_range (type);
  for (Reason::Map::const_iterator i = reasons.first; i != reasons.second; i++) {
    i->second->explain (s);
  }
}

void TypeFactory::Reason::explain (UtOStream &s) const
{
  UtString b0, b1;
  switch (mFlavour) {
  case eIS_FIELD_TYPE:
    s << (mSubject->getName () ? mSubject->getName () : mSubject->compose (&b0))
      << " is the type of " 
      << (mObject->getName () ? mObject->getName () : mObject->compose (&b1)) 
      << "." << mField->getName () << "\n";
    break;
  case eIS_SUPERCLASS:
    s << (mSubject->getName () ? mSubject->getName () : mSubject->compose (&b0))
      << " is a superclass of " << mObject->getName () << "\n";
    break;
  case eIS_DISTILLED:
    s << (mSubject->getName () ? mSubject->getName () : mSubject->compose (&b0))
      << " is distilled\n";
    break;
  }
}

//! Form the qualified name of a type
UtString Schema::qualifiedName (xmlNodePtr node, const char *flavour) const
{
  xmlChar *nodename;
  if ((nodename = xmlGetProp (node, sNAME)) == NULL) {
    return "";
  }
  UtString name;
  xmlChar *context = xmlGetProp (node, sCONTEXT);
  XmlClass *theclass;
  Namespace *thenamespace;
  if (context == NULL) {
    // the enum is not embedded in a class
  } else if (mTypes.find (context, &theclass)) {
    // embedded in a class so prefix the name
    name << qualifiedName (theclass->getNode (), flavour) << "::";
    xmlFree (context);
  } else if (mNamespaces.find (context, &thenamespace) && !thenamespace->isStd ()) {
    // embedded in a namespace so prefix the name
    name << qualifiedName (thenamespace->getNode (), "namespace") << "::";
    xmlFree (context);
  } else {
    // embedded in something odd
    xmlFree (context);
  }
  name << (const char *) nodename;
  xmlFree (nodename);
  return name;
}

//! Just emit the declaration part, without the semi-colon
void ClassType::emitClassDecl (CodeStream &out)
{
  if (mClass->isAbstract ()) {
    out << "abstract ";
  }
  if (mClass->isNested ()) {
    out << "nested ";
  }
  out << "class " << getName ();
  set (Attributes::eDECLARED);
}

void ClassType::emitForwardDeclaration (CodeStream &out)
{
  emitClassDecl (out);
  out << ";\n\n";
  set (Attributes::eDECLARED);
}

void ClassType::emitSchema (CodeStream &out, Schema *schema)
{
  // emit declaration stubs for undeclared field types
  for (Field::List::const_iterator fields = mFields.begin (); fields != mFields.end (); fields++) {
    NxsType *type = (*fields)->getType ();
    if (!type->isDeclared () && !type->isOmitted ()) {
      type->emitForwardDeclaration (out);
    }
  }
  SourceLocator srcloc;
  if (schema->getSrcLoc (this, &srcloc)) {
    UtString b0;
    srcloc.compose (&b0);
    out << "// " << b0 << "\n";
  }
  emitClassDecl (out);
  UInt32 n_parents = 0;
  XmlClass::Set::const_iterator it;
  for (it = mClass->beginSuper (); it != mClass->endSuper (); it++) { 
    XmlClass *xmlclass = (*it);
    ClassType *thesuperclass = xmlclass->getNxsClass ();
    if (thesuperclass == NULL) {
      continue;
    }
    if (n_parents++ == 0) {
      out << " : ";
    } else {
      out << ", ";
    }
    out << thesuperclass->getName ();
  }
  out << " {\n";
  out++;
  // output properties of the class
  UtString value;
  if (mClass->isSet (XmlClass::eBUILD, &value)) {
    out << "build \"" << value << "\";\n";
  }
  // output the fields of the class
  for (Field::List::const_iterator fields = mFields.begin (); fields != mFields.end (); fields++) {
    Field *field = *fields;
    if (field->getType ()->isContainer ()) {
      out << "// container ";
    }
    field->emitDecl (out);
  }
  out--;
  out << "};\n\n";
}

//! Distill an XmlType
bool TypeFactory::distill (XmlType *xmltype, NxsType **nxstype)
{
  Context context (Context::eDISTILLING, xmltype, *this);
  if (!xmltype->distill (*this, nxstype)) {
    return false;
  }
  // verify that the distill method install the type into the map
  NxsType *other;
  if ((*nxstype)->getId () == NULL) {
    // This is a type without an id. This can happen with recursive template
    // instantiations.
    // If you enable recursion in inferContainer by deleting the
    // INFO_ASSERT(!is_templated) then you will hit this one next. It should
    // just work to delete the assertion, but the code is untested
    INFO_ASSERT (false, "untested");
  } else if (!mNxsTypes.find ((*nxstype)->getId (), &other)) {
    // The type was not installed by the distill method. Find the appropriate
    // distill method and add a call to TypeFactory::bind.
    (*nxstype)->pr ();
    INFO_ASSERT (false, "distillation failed to bind the type in the type factory");
  } else if (other != *nxstype) {
    // The type bind in the factory is not consistent with the type returned by
    // distill. This is bad...
    (*nxstype)->pr ();
    other->pr ();
    INFO_ASSERT (other == *nxstype, "inconsistent type binding");
  }
  return true;
}

//! Close the type system w.r.t. the closure worklist
void TypeFactory::close ()
{
  while (!mClosureWorklist.empty ()) {
    NxsType *type = mClosureWorklist.back ();
    mClosureWorklist.pop_back ();
    if (type->isClosed ()) {
      // this type has already been closed
    } else {
      type->close (*this);
      type->set (Attributes::eCLOSED);
    }
  }
}

//! Locate a field
bool TypeFactory::find (xmlChar *id, Field **field) const
{ return mAllFields.find (id, field); }

//! Output any declarations required for inferred types
void TypeFactory::emitSchema (CodeStream &out, Schema *schema, ClassGraph &graph)
{
  // first emit all the non-class types
  for (NxsType::Map::const_iterator it = mNxsTypes.begin (); it != mNxsTypes.end (); ++it) {
    NxsType *type = it->second;
    UtString b0;
    if (type->isOmitted ()) {
      // this type has been explicitly omitted from the distilled types
    } else if (type->isBaseType () || !type->isClassType ()) {
      type->emitSchema (out, schema);
    }
  }
  // now emit the distilled types in topological order
  XmlClass::List sorted;
  graph.flattenGraph (&sorted);
  for (XmlClass::List::iterator it = sorted.begin (); it != sorted.end (); it++) {
    XmlClass *xmlclass = *it;
    ClassType *nxsclass;
    if ((nxsclass = xmlclass->getNxsClass ()) == NULL) {
      // this class is not distilled
    } else if (nxsclass->isOmitted ()) {
      // this class has been explicitly omitted
    } else {
      nxsclass->emitSchema (out, schema);
    }
  }
}

//
// Program entry point
//

int main (int argc, char **argv)
{
  // infrastructure for error messages and source locations
  SourceLocatorFactory sourcelocations;
  MsgStreamIO msg_stream (stdout, true);
  MsgContext msg;
  msg.addReportStream (&msg_stream);
  // configuration
  Context context (msg, sourcelocations);
  if (!context.processArgs (&argc, argv)) {
    return 1;
  }
  // grab the source document
  xmlDocPtr doc;
  xmlLineNumbersDefault (1);
  doc = xmlParseFile (context.getInputFilename ());
  if (doc == NULL ) {
    msg.Xml2NxsParseFailed (context.getInputFilename ());
  }
  if (msg.getExitStatus () != 0) {
    return msg.getExitStatus ();       // errors were found
  }
  // construct the mapping from XML nodes to locations
  SourceMapper sourcemap (context, msg, sourcelocations, doc);
  // Construct an internal representation of the schema
  Schema schema (msg, sourcemap, doc);
  if (!schema.build (context)) {
    return 1;
  }
  // 
  // construct the output file
  SimpleCodeStream nxs (context.getOuputFilename ());
  nxs << "// Nucleus schema derived from " << context.getInputFilename () << "\n";
  schema.emitNxs (nxs);
  return msg.getExitStatus ();
}

//
// Context implementation
//

//! ctor for class Context
Context::Context (MsgContext &msg_context, SourceLocatorFactory &sourcelocators) : 
  mMsg (msg_context), mSources (sourcelocators), mFilenames (), mDiag (NULL)
{ 
  configureArgs (); 
}

//! Process the command line arguments
bool Context::processArgs (int *argc, char **argv)
{
  mArgs.accessHiddenOptions ();
  mArgs.allowUsageAll ();
  // run the command-line parser
  UtString errMsg;
  int numOptions;
  if (mArgs.parseCommandLine (argc, argv, &numOptions, &errMsg, this) != ArgProc::eParsed) {
    mMsg.Xml2NxsCmdLineError (errMsg.c_str ());
    return false;
  }
  // be minimally helpful
  if (mArgs.getBoolValue ("-h")) {
    printUsage ();
    exit(0);
  }
  // check that at least one input file was specified
  if (mFilenames.empty ()) {
    mMsg.Xml2NxsCmdLineError ("No files specified on the command line.");
    printUsage ();
    return false;
  } else if (mFilenames.size () == 1) {
    mMsg.Xml2NxsCmdLineError ("No output file specified on the command line.");
    printUsage ();
    return false;
  } else if (mFilenames.size () > 2) {
    mMsg.Xml2NxsCmdLineError ("Too many files specified on the command line.");
    printUsage ();
    return false;
  }
  // grab the output filename
  INFO_ASSERT (mFilenames.size () == 2, "wrong number of filenames");
  UtList <UtString>::iterator it = mFilenames.begin ();
  mInputFilename = *(it++);
  mOutputFilename = *it;
  mDiagFilename << mInputFilename << ".log";
  mDiag = new UtOFStream (mDiagFilename.c_str ());
  return true;
}

//! Configure the argument processor
void Context::configureArgs ()
{
  mArgs.setDescription ("GCCXML to N-Expression schema compiler.", "xml2nxs",
    "Translates the output of GCCXML to a N-Expression schema.");
  mArgs.addSynopsis ("xml2nxs input output");
  mArgs.addBool (scQuiet, "Supress warnings and information messages", false, 1);
  mArgs.addBool (scVerbose, "Verbose output", false, 1);
  mArgs.addBool ("-h", "Print the documentation for annotation store reader options and exit.", 
    false, 1);
  mArgs.addSynonym ("-h", "-help", false);
}

//! Print annotation store reader usage instructions
void Context::printUsage ()
{
  UtString usage;
  mArgs.getUsageVerbose(&usage, false);
  UtIO::cout() << usage << UtIO::endl;
}

//
// SourceMapper implementation
//

SourceMapper::SourceMapper (Context &context, MsgContext &msg, 
  SourceLocatorFactory &sources, xmlDocPtr doc) :
  mContext (context), mSources (sources), mMsg (msg), mDoc (doc)
{
  // Walk the document looking for File objects.
  xmlNodePtr root = xmlDocGetRootElement (doc);
  for (xmlNodePtr node = root->xmlChildrenNode; node != NULL; node = node->next) {
    if (!xmlStrcasecmp (node->name, sFILE)) {
      // Found a file object.
      xmlChar *key = xmlGetProp (node, sID);
      xmlChar *filename = xmlGetProp (node, sNAME);
      if (mFiles.find (key) == mFiles.end ()) {
        // insert the file into the map
        mFiles.insert (xmlStringMap::value_type (key, filename));
      } else {
        // This is a file object that we have already seem. GCCXML should not
        // emit two file objects with the same id attribute
        SourceLocator loc = mContext.getXmlLoc (node);
        mMsg.Xml2NxsDuplicateObject (&loc, "File");
        xmlFree (key);
        xmlFree (filename);
      }
    }
  }
}

//! Visibility for debugging
void SourceMapper::dump (FILE *f, xmlNodePtr node, const char *fmt, ...) const
{
  char buffer [256];
  if (fmt == NULL) {
    snprintf (buffer, sizeof (buffer), "%s", (char *) node->name);
  } else {
    va_list ap;
    va_start (ap, fmt);
    vsnprintf (buffer, sizeof (buffer), fmt, ap);
    va_end (ap);
  }
  SourceLocator xmlloc = getXmlLoc (node);
  UtString b0;
  xmlloc.compose (&b0);
  fprintf (f, "%s: %s\n", b0.c_str (), buffer);
  SourceLocator srcloc;
  if (getSrcLoc (node, &srcloc)) {
    UtString b1;
    srcloc.compose (&b1);
    fprintf (f, "%s: source location\n", b1.c_str ());
  }
  xmlElemDump (f, mDoc, node);
  fprintf (f, "\n");
}

//! Dump the node to standard output
void SourceMapper::dump (xmlNodePtr node) const
{
  dump (stdout, node);
}

//! \return the XML source location for an XML node
SourceLocator SourceMapper::getXmlLoc (xmlNodePtr node) const
{
  return mContext.getXmlLoc (node);
}

//! \return the C++ source location for an XML node
/*! This method looks for a "location" attribute in the node, plucks apart
 *  the string and constructs a SourceLocation instance.
 */
bool SourceMapper::getSrcLoc (xmlNodePtr node, SourceLocator *sourceloc) const
{
  // Get the "file:line" attribute from the node
  xmlChar *location = xmlGetProp (node, sLOCATION);
  if (location == NULL) {
    return false;
  }
  // pull out the key
  xmlChar *key = location;
  while (*location != ':' && *location != '\0') {
    location++;
  }
  if (*location != ':') {
    SourceLocator loc = mContext.getXmlLoc (node);
    mMsg.Xml2NxsBadLocation (&loc, (const char *) location);
    xmlFree (key);
    return false;
  }
  *location = '\0';		// null terminate the key
  location++;			// consume the colon
  // map the file key into a file name
  xmlStringMap::const_iterator found;
  if ((found = mFiles.find (key)) == mFiles.end ()) {
    SourceLocator loc = mContext.getXmlLoc (node);
    mMsg.Xml2NxsBadLocation (&loc, (const char *) location);
    xmlFree (key);
    return false;
  }
  xmlChar *filename = found->second;
  // pull out the line number
  unsigned int line = 0;
  while (isdigit (*location)) {
    line = line * 10 + (int) (*location) - (int) '0';
    location++;
  }
  if (*location != '\0') {
    SourceLocator loc = mContext.getXmlLoc (node);
    mMsg.Xml2NxsBadLocation (&loc, (const char *) location);
    xmlFree (key);
    return false;
  }
  xmlFree (key);
  *sourceloc = mSources.create ((const char *) filename, line);
  return true;
}

//! \return the C++ source location that induced an XML node
SourceLocator SourceMapper::getSrcLoc (xmlNodePtr node) const
{
  SourceLocator loc;
  if (!getSrcLoc (node, &loc)) {
    SourceLocator loc = mContext.getXmlLoc (node);
    mMsg.Xml2NxsNoLocation (&loc);
    return SourceLocator ();
  } else {
    return loc;
  }
}

//
// Implementation of class Node
//

//! Construct a human readable representation of this
/*virtual*/ const char *Node::compose (UtString *s) const
{
  *s << "node " << mId;
  return s->c_str ();
}

void Node::dump (xmlDocPtr doc) const
{
  UtString buffer;
  compose (&buffer);
  UtIO::cout () << buffer << ": ";
  xmlElemDump (stdout, doc, mNode);
  UtIO::cout () << "\n";
}

//
// Type implementation
//

NxsType::NxsType (xmlNodePtr node, xmlChar *typeId, XmlType *xmltype, TypeFactory &factory,
  const UInt32 flags) : 
  mNode (node), mId (typeId), mXmlType (xmltype), 
  mFlags (flags|(xmltype != NULL ? xmltype->getFlags () : 0)), mUsed (false) 
{
  if (typeId == NULL) {
    // Types without a type id can be induced in template instantiations or
    // typedefs
  } else {
    factory.bind (this);
  }
  if (xmltype != NULL) {
    xmltype->bind (this);
  }
}

//! Write this to a stream
void NxsType::print (UtOStream &out) const
{
  UtString buffer;
  compose (&buffer);
  if (mId) {
    out << "type " << mId << " = " << buffer << "\n";
  } else {
    out << "type (no type id) = " << buffer << "\n";
  }
}

//! Write this to standard output
void NxsType::pr () const { print (UtIO::cout ()); }

//
// Attributes implementation
//

Attributes::Attributes (MsgContext &msg, SourceMapper &sourcemap) : 
  mMsg (msg), mSource (sourcemap)
{
  memset (mIdent, 0, sizeof (mIdent));
}

/*virtual*/ Attributes::~Attributes ()
{
  Values::iterator it;
  for (it = mStrValue.begin (); it != mStrValue.end (); it++) {
    delete it->second;
  }
}

//! Parse the attributes from a node
UInt32 Attributes::operator () (xmlNodePtr node, const UInt32 legal)
{
  xmlChar *attributes = xmlGetProp (node, sATTRIBUTES);
  if (attributes == NULL) {
    return 0;
  } else {
    Source source (mSource.getXmlLoc (node), mSource, node, (const char *) attributes);
    nextToken (source);
    UInt32 value = parse (source, legal);
    xmlFree (attributes);
    return value;
  }
}

//! Lookup a string valued attribute
bool Attributes::get (UInt32 key, UtString *str) const
{
  Values::const_iterator found;
  if ((found = mStrValue.find (key)) == mStrValue.end ()) {
    return false;
  } else {
    str->clear ();
    *str << *found->second;
    return true;
  }
}

//! Write this to a stream
void Attributes::print (UtOStream &out) const
{
  out << "token=" << mToken << ", ident=\"" << mIdent << "\"\n";
  if (mStrValue.empty ()) {
    out << "No string values.\n";
  } else {
    for (Values::const_iterator it = mStrValue.begin (); it != mStrValue.end (); ++it) {
      out << it->first << ": " << it->second->c_str () << "\n";
    }
  }
}

//! Write this to standard output
void Attributes::pr () const { print (UtIO::cout ()); }

// The attributes string from an XML node gets parsed by a simple recursive
// descent parser.

/*! attributes <- { gccxml-attribute | unknown-attribute } */
UInt32 Attributes::parse (Source &source, const UInt32 legal)
{
  switch (mToken) {
  case eLPAREN:
    source.error (mMsg, "expected identifier, found left parenthesis");
    return 0;
    break;
  case eRPAREN:
    source.error (mMsg, "expected identifier, found right parenthesis");
    return 0;
    break;
  case eCOMMA:
    source.error (mMsg, "expected identifier, found comma");
    return 0;
    break;
  case eEND:
  case eERROR:
    return 0;
    break;
  case eIDENT:
    if (!strcasecmp (mIdent, "gccxml")) {
      // this is a gccxml attribute
      UInt32 value = parseGccXml (source, legal);
      return value | parse (source, legal); 
    } else {
      // not a gccxml attribute
      skipUnknown (source);
      return parse (source, legal);
    }
    break;
  default:
    INFO_ASSERT (false, "attribute scanner jammed");
    break;
  }
  return 0;
}

//! Parse a gccxml attribute
/*! gccxml-attribute <- gccxml ( gccxml-attribute-name {, gccxml-attribute-name } )
 *  gccxml-attribute-name <- "DISTILL" | "NESTED"
 */
UInt32 Attributes::parseGccXml (Source &source, const UInt32 legal)
{
  INFO_ASSERT (mToken == eIDENT && !strcasecmp (mIdent, "gccxml"), "attribute scanner jammed");
  nextToken (source);
  if (mToken != eLPAREN) {
    nextToken (source);
    source.error (mMsg, "expected a left parenthesis");
    return 0;
  }
  nextToken (source);
  if (mToken != eIDENT) {
    nextToken (source);
    source.error (mMsg, "expected an identifier");
    return 0;
  }
  UInt32 value = 0;
  if (!strcasecmp (mIdent, "distill")) {
    value = eDISTILL;
  } else if (!strcasecmp (mIdent, "nested")) {
    value = eNESTED;
  } else if (!strcasecmp (mIdent, "omitted")) {
    value = eOMITTED;
  } else if (!strcasecmp (mIdent, "basetype")) {
    value = eBASE_TYPE;
  } else if (!strcasecmp (mIdent, "choice")) {
    value = eCHOICE;
  } else if (!strcasecmp (mIdent, "mask")) {
    value = eMASK;
  } else if (!strcasecmp (mIdent, "build")) {
    value = eBUILD;
  } else if (!strcasecmp (mIdent, "select")) {
    value = eSELECT;
  } else {
    source.error (mMsg, "unrecognised gccxml attribute: %s", mIdent);
  }
  if (value & ~legal) {
    // this attribute cannot be applied to this type of object
    source.error (mMsg, "\"%s\" cannot be applied to this entity.", mIdent);
  }
  nextToken (source);
  if (mToken == eLPAREN) {
    parseGccXmlArgs (source, value);
  }
  if (mToken != eRPAREN) {
    source.error (mMsg, "expecting a right parenthesis");
    return value;
  }
  nextToken (source);
  return value;
}

//! Parse an argument list
/*! gcc-arg-list = gcc-parenthesised-value
 */
void Attributes::parseGccXmlArgs (Source &source, UInt32 key)
{
  INFO_ASSERT (mToken == eLPAREN, "attribute scanner jammed");
  UtString value;
  getParenthesedValue (source, &value);
  Values::iterator found;
  if ((found = mStrValue.find (key)) != mStrValue.end ()) {
    source.error (mMsg, "multiple values for key %d", key);
  } else {
    mStrValue.insert (Values::value_type (key, new UtString (value)));
  }
  nextToken (source);
  if (mToken == eRPAREN) {
    nextToken (source);
  } else {
    source.error (mMsg, "expecting a right parenthesis");
  }
}

//! Skip a parenthesised term
/*! term <- IDENT {, IDENT }  |  ( term ) */
void Attributes::skipTerm (Source &source)
{
  if (mToken == eLPAREN) {
    nextToken (source);
    skipTerm (source);
    if (mToken != eRPAREN) {
      nextToken (source);
      source.error (mMsg, "expected a right parenthesis");
    } else {
      nextToken (source);
    }
  } else if (mToken == eIDENT) {
    nextToken (source);
    if (mToken == eCOMMA) {
      nextToken (source);
      skipTerm (source);
    }
  } else {
    nextToken (source);
    source.error (mMsg, "expecting a idenrtifier or left parenthesis");
  }
}

//! Skip an unknown attribute
/*! unknown-attribute <- _IDENT_  |  _IDENT ( term ) */
void Attributes::skipUnknown (Source &source) 
{
  INFO_ASSERT (mToken == eIDENT, "attribute scanner jammed");
  nextToken (source);
  if (mToken == eLPAREN) {
    skipTerm (source);
  }
}

//! Scan the string for a parenthesised value
/*! parenthesised-value-string = <a string with balanced parenthesis>
 *  This is not done as a recursive descent parser. We are really only
 *  interested in grabbing the string with balanced parenthsis, so just scan
 *  the characters and count the parentheses.
 */
void Attributes::getParenthesedValue (Source &source, UtString *str)
{
  char buffer [1024];
  UInt32 length = 0;                       // number of characters in buffer
  int nesting = 1;
  INFO_ASSERT (mToken == eLPAREN, "attribute scanner jammed");
  while (nesting > 0) {
    if (length >= sizeof (buffer) - 1) {
      source.error (mMsg, "gccxml attribute string is too long");
    } else if (*source == '\0') {
      source.error (mMsg, "unbalanced parenthesis in attribute");
      nesting = 0;                    // escape from the loop
    } else if (nesting == 1 && *source == ')') {
      // do not advance the source index... we want nextToken to return the
      // right parenthesis on the next call
      nesting = 0;                    // hit the end of the string
    } else if (*source == ')') {
      buffer [length++] = *source;
      source++;
      nesting--;
    } else if (*source == '(') {
      buffer [length++] = *source;
      nesting++;
      source++;
    } else {
      buffer [length++] = *source;
      source++;
    }
  }
  str->clear ();
  buffer [length++] = '\0';
  *str << buffer;
}

//! Scan the next token in the source string
/*! The token value is written to mToken. If an identifier is found then the
 *  lexical value is stored in mIdent and nul terminated
 */
void Attributes::nextToken (Source &source)
{
  int state = 0;
  UInt32 len = 0;
  while (state >= 0) {
    switch (state) {
    case 0:                           // initial
      if (*source == '\0') {
        mToken = eEND;                // end of input
        state = -1;                   // final state
      } else if (isspace (*source)) {
        source++;                     // consume whitespace
      } else if (isalpha (*source)) {
        state = 10;                   // scan an identifier
      } else {
        switch (*source) {
        case '(':
          source++;                   // consume the left parenthesis
          mToken = eLPAREN;
          state = -1;                 // final state
          break;
        case ')':
          source++;                   // consume the right parenthesis
          mToken = eRPAREN;
          state = -1;                 // final state
          break;
        case ',':
          source++;                   // consume the comma
          mToken = eCOMMA;
          state = -1;                 // final state
          break;
        default:
          // bogus character in the attributes string
          source.error (mMsg, "unrecognised character in string: '%c'", *source);
          mToken = eERROR;
          state = -1;
        }
      }
      break;
    case 10:                          // start constructing a string
      if (!isalnum (*source)) {
        // end of the identifier
        mIdent [len] = '\0';          // null terminate the identifier
        mToken = eIDENT;
        state = -1;                   // final state
      } else if (len >= sizeof (mIdent)) {
        // identifier is too long
        source.error (mMsg, "identifer is too long");
        mToken = eERROR;
        state = -1;                   // final state
      } else {
        mIdent [len++] = *source;
        source++;
      }
      break;
    default:
      INFO_ASSERT (false, "attribute scanner jammed");
      break;          
    }
  }
}

//! Report an error at the current location
/*! Two messages are emitted. The first message includes the test passed into
 *  the method and is attached to the XML source. The second message notes
 *  the location in the C++ source that induced the node in the XML
 *  document.
 */
void Attributes::Source::error (MsgContext &msg, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  verror (msg, fmt, ap);
  va_end (ap);
}

void Attributes::Source::verror (MsgContext &msg, const char *fmt, va_list ap)
{
  char reason [256];
  vsnprintf (reason, sizeof (reason), fmt, ap);
  XMLERROR (mNode, mSource, msg, BadAttributes, mStr, mPosition, reason);
}

//! Write this to a stream
void Attributes::Source::print (UtOStream &out) const
{
  UtString xmlloc;
  mXmlLoc.compose (&xmlloc);
  out << "Source " << xmlloc << ": \"" << mStr << "\", at position " << mPosition << "\n";
}

//! Write this to standard output
void Attributes::Source::pr () const { print (UtIO::cout ()); }

//
// Implementation of XmlType
//

XmlType::XmlType (xmlChar *id, xmlNodePtr node, xmlDocPtr doc, const UInt32 flags) : 
  Node (id, node, flags), mDoc (doc), mNxsType (NULL) {}

void XmlType::dump () const
{
  Node::dump (mDoc);
  UtString buffer;
  compose (&buffer);
  UtIO::cout () << buffer << "\n";
}

//! Write this to a stream
void XmlType::print (UtOStream &out) const
{
  UtString buffer;
  compose (&buffer);
  out << buffer << "\n";
}

//! Write this to standard output
void XmlType::pr () const
{
  print (UtIO::cout ());
}

//! Construct a human readable representation of this
/*virtual*/ const char *XmlType::compose (UtString *s) const
{
  *s << "Type " << mId;
  if (isOmitted ()) {
    *s << " omit";
  }
  if (isAbstract ()) {
    *s << " abstract";
  }
  if (isBaseType ()) {
    *s << " base";
  }
  return s->c_str ();
}

class XmlCvQualified : public XmlType {
public:

  XmlCvQualified (xmlChar *id, xmlNodePtr node, xmlDocPtr doc) : XmlType (id, node, doc) {}
  virtual ~XmlCvQualified () {}

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &factory, NxsType **type)
  { 
    TypeFactory::Context context (TypeFactory::Context::eDISTILLING, this, factory);
    // This does not induce a new type, instead it works as an alias
    xmlChar *typeId = xmlGetProp (mNode, sTYPE);
    if (typeId == NULL) {
      xmlFree (typeId);
      return factory.NoTypeId (mNode, "CvQualifiedType");
    } else if (!factory.infer (mNode, typeId, type)) {
      XMLERROR (mNode, factory.getSource (), factory.getMsg (), BadCvQualified, (char *) typeId);
      *type = NULL;
      xmlFree (typeId);
      return false;
    } else {
      xmlFree (typeId);
      return true;
    }
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    XmlType::compose (s);
    *s << " cvqualified";
    return s->c_str ();
  }

};

class XmlUnion : public XmlType {
public:

  XmlUnion (xmlChar *id, xmlNodePtr node, xmlDocPtr doc) : XmlType (id, node, doc) {}
  virtual ~XmlUnion () {}

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &factory, NxsType **type)
  { 
    // Look for a member marked with the eSELECT attribute
    xmlChar *members = xmlGetProp (mNode, sMEMBERS);
    if (members == NULL) {
      XMLERROR (mNode, factory.getSource (), factory.getMsg (), DegenerateUnion);
      return false;
    } else {
      IdList list (members);
      xmlStringList::iterator it;
      Field *selected = NULL;
      for (it = list.begin (); it != list.end (); it++) {
        Field *field;
        if (!factory.find (*it, &field)) {
          // the item does not denote a field
        } else if (field->isOmitted ()) {
          // this field is explicitly marked as omitted
        } else if (!field->isSelected ()) {
          // this is not the selected field
        } else if (selected != NULL) {
          XMLERROR (field->getNode (), factory.getSource (), factory.getMsg (), MultipleSelectedFields);
        } else {
          selected = field;
        }
      }
      if (selected == NULL) {
        XMLERROR (getNode (), factory.getSource (), factory.getMsg (), NoSelectedField);
        xmlFree (members);
        return false;
      } else {
        *type = new UnionType (getNode (), getId (), this, factory, selected);
        xmlFree (members);
        return true;
      }
    }
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    XmlType::compose (s);
    *s << " union";
    return s->c_str ();
  }

};

class XmlTypedef : public XmlType {
public:

  XmlTypedef (xmlChar *id, xmlNodePtr node, xmlDocPtr doc) : 
    XmlType (id, node, doc) 
  {
    xmlChar *name = xmlGetProp (node, sNAME);
    if (name != NULL) {
      mName << name;
      xmlFree (name);
    }
  }

  virtual ~XmlTypedef () {}

  //! \return the name of the type
  virtual const char *getName () const { return mName.size () > 0 ? mName.c_str () : NULL; }

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &factory, NxsType **type)
  { 
    TypeFactory::Context context (TypeFactory::Context::eDISTILLING, this, factory);
    NxsType *basetype;
    xmlChar *baseTypeId;
    if (BuiltinType::build (mNode, mId, this, factory, type)) {
      return true;
    } else if ((baseTypeId = xmlGetProp (mNode, sTYPE)) == NULL) {
      return factory.NoTypeId (mNode, "typedef");
    } else if (!factory.infer (mNode, baseTypeId, &basetype)) {
      xmlFree (baseTypeId);
      return false;
    } else {
      *type = new TypedefType (mNode, mId, this, factory, 
        factory.qualifiedName (mNode, "typedef"), basetype);
      return true;
    }
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    XmlType::compose (s);
    *s << " typedef";
    return s->c_str ();
  }

private:

  UtString mName;

};

//! ctor
XmlClass::XmlClass (xmlChar *id, xmlNodePtr node, xmlDocPtr doc, UtString name, const UInt32 flags) : 
  XmlType (id, node, doc, flags), mName (name), mInDegree (0)
{
  if (!xmlStrcasecmp (node->name, sSTRUCT)) {
    set (Attributes::eSTRUCT);
  }
}

ClassType *XmlClass::getNxsClass ()
{
  if (mNxsType == NULL) {
    return NULL;
  } else {
    return dynamic_cast <ClassType *> (mNxsType);
  }
}

//! Construct a distilled type descriptor for this type
/*virtual*/ bool XmlClass::distill (TypeFactory &factory, NxsType **type)
{ 
  TypeFactory::Context context (TypeFactory::Context::eDISTILLING, this, factory);
  bool is_container = factory.inferContainer (getNode (), getId (), this, getName (), type);
  if (is_container && *type == NULL) {
    return false;
  } else if (is_container) {
    // This is a templated class that was specially dealt with
    return true;
  } else if (isBaseType ()) {
    *type = new BaseType (this, factory);
    return true;
  } else {
    ClassType *theclass = new ClassType (this, factory);
    *type = theclass;
    return true;
  }
}

//! Construct a human readable representation of this
/*virtual*/ const char *XmlClass::compose (UtString *s) const
{
  XmlType::compose (s);
  if (mFlags & Attributes::eSTRUCT) {
    *s << " struct " << getName ();
  } else {
    *s << " class " << getName ();
  }
  return s->c_str ();
}

//! Propagate attributes from the class to all its subclasses
bool XmlClass::propagateSynthesisedAttributes (MsgContext &msg, SourceMapper sourcemap, 
  const UInt32 mask, const UInt32 syn)
{
  if (isMarked ()) {
    SourceLocator loc = sourcemap.getSrcLoc (mNode);
    msg.Xml2NxsClassLoop (&loc);
    return false;
  } else {
    bool acyclic = true;
    setMark ();
    mFlags |= (mask & syn);
    for (Set::iterator it = mSub.begin (); it != mSub.end (); it++) {
      if (!(*it)->propagateSynthesisedAttributes (msg, sourcemap, mask, mFlags)) {
        acyclic = false;
      }
    }
    if (!acyclic) {
      SourceLocator loc = sourcemap.getSrcLoc (mNode);
      msg.Xml2NxsInCycle (&loc, mName.c_str ());
    }
    clearMark ();
    return acyclic;
  }
}

class XmlArray : public XmlType {
public:

  XmlArray (xmlChar *id, xmlNodePtr node, xmlDocPtr doc) : XmlType (id, node, doc) {}
  virtual ~XmlArray () {}

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &factory, NxsType **type)
  { 
    TypeFactory::Context context (TypeFactory::Context::eDISTILLING, this, factory);
    xmlChar *elementId;
    NxsType *element;
    if ((elementId = xmlGetProp (getNode (), sTYPE)) == NULL) {
      return factory.NoTypeId (getNode (), "array");
    } else if (!factory.infer (getNode (), elementId, &element)) {
      return false;
    } else {
      *type = new ArrayType (getNode (), getId (), this, factory, element);
      return true;
    }
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    XmlType::compose (s);
    *s << " array";
    return s->c_str ();
  }

};

class XmlEnum : public XmlType {
public:

  XmlEnum (xmlChar *id, xmlNodePtr node, xmlDocPtr doc) : XmlType (id, node, doc), mName () 
  {
    xmlChar *name = xmlGetProp (node, sNAME);
    if (name != NULL) {
      mName << name;
      xmlFree (name);
    }
  }

  virtual ~XmlEnum () {}

  //! \return the name of the type
  virtual const char *getName () const { return mName.size () > 0 ? mName.c_str () : NULL; }

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &factory, NxsType **type)
  { 
    TypeFactory::Context context (TypeFactory::Context::eDISTILLING, this, factory);
    *type = new EnumType (getNode (), getId (), this, factory, mFlags, 
      factory.qualifiedName (getNode (), "enmeration"));
    return true;
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    XmlType::compose (s);
    *s << " enum";
    return s->c_str ();
  }

private:

  UtString mName;

};

class XmlFundamental : public XmlType {
public:

  XmlFundamental (xmlChar *id, xmlNodePtr node, xmlDocPtr doc) : 
    XmlType (id, node, doc) 
  {
    xmlChar *name = xmlGetProp (node, sNAME);
    INFO_ASSERT (name != NULL, "expected a name for fundamental type");
    mName << name;
    xmlFree (name);
  }
  
  virtual ~XmlFundamental () {}

  //! \return the name of the type
  virtual const char *getName () const { return mName.c_str (); }

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &factory, NxsType **type)
  { 
    TypeFactory::Context context (TypeFactory::Context::eDISTILLING, this, factory);
    if (!BuiltinType::build (mNode, mId, this, factory, type)) {
      dump ();
      INFO_ASSERT (false, "unable to map fundamental type into builtin");
      return false;
    } else {
      return true;
    }
  }

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    XmlType::compose (s);
    *s << " fundamental";
    return s->c_str ();
  }

private:

  UtString mName;

};

class XmlPointer : public XmlType {
public:

  XmlPointer (xmlChar *id, xmlNodePtr node, xmlDocPtr doc) : XmlType (id, node, doc) {}
  virtual ~XmlPointer () {}

  //! Construct a distilled type descriptor for this type
  virtual bool distill (TypeFactory &factory, NxsType **type);

  //! Construct a human readable representation of this
  virtual const char *compose (UtString *s) const
  {
    XmlType::compose (s);
    *s << " pointer";
    return s->c_str ();
  }

};

//! Construct a distilled type descriptor for this type
/*virtual*/ bool XmlPointer::distill (TypeFactory &factory, NxsType **type)
{ 
  TypeFactory::Context context (TypeFactory::Context::eDISTILLING, this, factory);
  NxsType *baseType;
  xmlChar *baseTypeId;
  if ((baseTypeId = xmlGetProp (mNode, sTYPE)) == NULL) {
    return factory.NoTypeId (mNode, "pointer type");
  } else if (!factory.infer (mNode, baseTypeId, &baseType)) {
    xmlFree (baseTypeId);
    return false;
  } else if (!baseType->canPoint ()) {
    XMLERROR (mNode, factory.getSource (), factory.getMsg (), CannotPoint);
    xmlFree (baseTypeId);
    return false;
  } else {
    *type = new PointerType (mNode, mId, this, factory, baseType);
    xmlFree (baseTypeId);
    return true;
  }
}

bool XmlType::Map::collect (xmlChar *typeId, xmlNodePtr node, xmlDocPtr doc,
  MsgContext &msg, SourceMapper &source, TypeFactory &factory, XmlClassMap *classes)
{
  if (!xmlStrcasecmp (node->name, sTYPEDEF)) {
    insert (new XmlTypedef (typeId, node, doc));
    return true;
  } else if (!xmlStrcasecmp (node->name, sENUMERATION)) {
    insert (new XmlEnum (typeId, node, doc));
    return true;
  } else if (!xmlStrcasecmp (node->name, sARRAYTYPE)) {
    insert (new XmlArray (typeId, node, doc));
    return true;
  } else if (!xmlStrcasecmp (node->name, sCVQUALIFIEDTYPE)) {
    insert (new XmlCvQualified (typeId, node, doc));
    return true;
  } else if (!xmlStrcasecmp (node->name, sCLASS) || !xmlStrcasecmp (node->name, sSTRUCT)) {
    Attributes attributes (msg, source);
    UInt32 flags = attributes (node, Attributes::eCLASS_ATTRIBUTES);
    if (xmlHasProp (node, sABSTRACT)) {
      flags |= Attributes::eABSTRACT;
    }
    XmlClass *theclass = new XmlClass (typeId, node, doc, 
      factory.qualifiedName (node, "class or struct"), flags);
    UtString build;
    if (!(flags & Attributes::eBUILD)) {
      // no build attribute
    } else if (!attributes.get (Attributes::eBUILD, &build)) {
      XMLERROR (node, source, msg, ExpectedAttributeValue, "build");
    } else {
      theclass->bind (XmlClass::eBUILD, build);
    }
    insert (theclass);
    // classes also go into the class map
    classes->insert (theclass);
    return true;
  } else if (!xmlStrcasecmp (node->name, sFUNDAMENTALTYPE)) {
    insert (new XmlFundamental (typeId, node, doc));
    return true;
  } else if (!xmlStrcasecmp (node->name, sPOINTERTYPE)) {
    insert (new XmlPointer (typeId, node, doc));
    return true;
  } else if (!xmlStrcasecmp (node->name, sUNION)) {
    insert (new XmlUnion (typeId, node, doc));
    return true;
  } else {
    return false;
  }
}

ClassType::ClassType (XmlClass *theclass, TypeFactory &factory) :
  NxsType (theclass->getNode (), theclass->getId (), theclass, factory, theclass->getFlags ()), 
  mClass (theclass)
{
}

/*virtual*/ const char *ClassType::getName () const 
{ return mClass->getName (); }

//! Close the type
/*virtual*/ void ClassType::close (TypeFactory &factory)
{
  TypeFactory::Context context (TypeFactory::Context::eCLOSING, mClass, factory);
  populateFieldList (factory);
  // make sure all the superclasses are distilled
  XmlClass::Set::const_iterator it;
  XmlClass *xmlclass = dynamic_cast <XmlClass *> (mXmlType);
  INFO_ASSERT (xmlclass != NULL, "oops");
  if (isContainer ()) {
    // Do not add the superclasses of containers to the list of types that must
    // be closed. That way lies madness... you end up with all sorts of crap
    // from the STL in the distilled class list.
  } else {
    for (it = xmlclass->beginSuper (); it != xmlclass->endSuper (); it++) {
      XmlClass *superclass = *it;
      NxsType *nxstype;
      TypeFactory::Context context (TypeFactory::Context::eCLOSING_SUPER, mClass, factory);
      factory.infer (superclass->getNode (), superclass->getId (), &nxstype);
      factory.note (new TypeFactory::Reason (superclass, xmlclass));
    }
  }
}

BaseType::BaseType (XmlClass *theclass, TypeFactory &factory) :
  NxsType (theclass->getNode (), theclass->getId (), theclass, factory, 
    theclass->getFlags ()|Attributes::eBASE_TYPE|Attributes::eDECLARED),
   mName (theclass->getName ())
{
  INFO_ASSERT (isBaseType (), "eBASE_TYPE is not set on a base type");
}

void Field::emitDecl (CodeStream &out)
{
  INFO_ASSERT (mType != NULL, "field has no type");
  mType->emitFieldDecl (out, getName ());
}

//! Construct a human readable representation of this
const char *Field::compose (UtString *s) const
{
  if (mType != NULL) {
    UtString b;
    mType->compose (&b);
    *s << b << " " << mName;
  } else {
    *s << "<notype> " << mName;
  }
  return s->c_str ();
}

//! Write this to a stream
void Field::print (UtOStream &out) const
{
  UtString buffer;
  compose (&buffer);
  out << buffer << "\n";
}

//! Write this to standard out
void Field::pr () const
{ print (UtIO::cout ()); }
