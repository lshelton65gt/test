// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/UtIOStream.h"
#include "util/ArgProc.h"
#include "util/NExprMsgContext.h"
#include "util/SourceLocator.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/CodeStream.h"
#include "util/IndentedStream.h"
#include "util/UtSet.h"
#include "NExprSchema.h"
#include "Burs.h"

//
// BURS
//

BURS::BURS (NExprSchema &schema, const UInt32 flags) :
  mSchema (schema), mFlags (flags), 
  mIR (mSchema.getIROps ()), 
  mNT (mSchema.getNonTerminals ()),
  mRewrites (mSchema.getRewrite ()), 
  mTransforms (mSchema.getTransforms ()),
  mNonTerminalMap (NULL), 
  mNTerminals (0), mNNonTerminals (0), mNRules (0), mNChainRules (0),
  mRules (NULL), mTerminal (NULL), 
  mTransitions (NULL), mStates (NULL)
{
}

/*virtual*/ BURS::~BURS ()
{
  if (mRules != NULL) {
    delete mRules;
  }
  if (mNonTerminalMap != NULL) {
    delete [] mNonTerminalMap;
  }
  if (mTransitions != NULL) {
    delete mTransitions;
  }
  if (mStates != NULL) {
    delete mStates;
  }
}

//! Add a new state to the worklist
void BURS::addToWorklist (State *newstate)
{
  if (isTraced ()) {
    UtIO::cout () << "add state " << newstate->getIndex () << " to worklist\n";
  }
  mWorklist.push_back (newstate);
}

//! Write out the terminals, non terminals and rules of the system
void BURS::dumpSystem (UtOStream &s) const
{
  UInt32 n;
  // dump the terminals
  for (n = 0; n < mNTerminals; n++) {
    Terminal &op = mTerminal [n];
    switch (op.getArity ()) {
    case 0:
      s << "leaf ";
      break;
    case 1:
      s << "unary ";
      break;
    case 2:
      s << "binary ";
      break;
    default:
      INFO_ASSERT (false, "blah");
      break;
    }
    s << "terminal " << op.getName () << "\n";
  }
  // dump the non terminals
  for (n = 0; n < mNNonTerminals; n++) {
    s << "non terminal " << nonTerminalName (n) << "\n";
  }
  // dump the rules
  for (n = 0; n < mNRules; n++) {
    mRules->get (n).print (s);
  }
  // dump the chain cost matrix
  IndentedStream <UtOStream> s1 (s);
  UInt32 i, j;
  s1 << "chain costs:";
  for (i = 0; i < mNNonTerminals; i++) {
    for (j = 0; j < mNNonTerminals; j++) {
      if (mRules->chain (i, j) != eINFINITY) {
        char buffer [128];
        snprintf (buffer, sizeof (buffer), "(%d,%d)=%d", i, j, mRules->chain (i, j));
        s1 << "\t" << buffer;
      }
    }
  }
  s1.nl ();
}

//! Write out the results of BURS analysis to BURS.txt in the output directory
void BURS::dumpAnalysis (const char *dirname)
{
  UtString filename;
  filename << dirname << "/BURS.txt";
  UtOFStream s (filename.c_str ());
  // dump the list of terminals, non terminals and rules
  dumpSystem (s);
  mStates->print (s);
  // enumerate the proto state mappings at each dimension of each terminal
  for (UInt32 n = 0; n < mNTerminals; n++) {
    Terminal &terminal = mTerminal [n];
    terminal.print (s);
    IndentedStream <UtOStream> s1 (s, 2);
    for (UInt32 dim = 0; dim < terminal.getArity (); dim++) {
      StateMap::const_iterator it;
      for (it = terminal.firstState (dim); it != terminal.lastState (dim); ++it) {
        char buffer [65];
        snprintf (buffer, sizeof (buffer), "(%d,%d,%d)", dim, it->first, it->second);
        s1 << "\t" << buffer;
      }
    }
    s1.nl ();
  }
  // enumerate the transitions
  mTransitions->print (s);
}

//
// Terminal
//

/*virtual*/ BURS::Terminal::~Terminal ()
{
  // walks the parameter index and trash the parameters
  Parameter::Index::const_iterator formals;
  for (formals = mParamIndex.begin (); formals != mParamIndex.end (); formals++) {
    delete formals->second;
  }
  // trash the actual parameters
  ValueMap::const_iterator actuals;
  for (actuals = mActuals.begin (); actuals != mActuals.end (); actuals++) {
    delete actuals->second;
  }
}

//! Add a parameter in the next parameter position
bool BURS::Terminal::addParameter (StringAtom *type, StringAtom *name, UtString *errmsg)
{
  // identify the type of the parameter
  Parameter::Flavour flavour;
  if (!strcmp (type->str (), "integer")) {
    flavour = Parameter::eINTEGER;
  } else if (!strcmp (type->str (), "string")) {
    flavour = Parameter::eSTRING;
  } else {
    *errmsg << "bad type for parameter '" << name->str () << "'; expected string or integer, found "
            << type->str ();
    return false;
  }
  // check that the name is not a duplicate of an existing parameter name
  if (mParamMap.find (name) != mParamMap.end ()) {
    INFO_ASSERT (false, "duplicate parameter name");
  }
  // construct a new Parameter object and add to the index and map
  Parameter *parameter = new Parameter (flavour, name);
  UInt32 position = mParamIndex.size ();
  mParamIndex.insert (Parameter::Index::value_type (position, parameter));
  mParamMap.insert (Parameter::Map::value_type (name, parameter));
  return true;
}

//! Look up a terminal symbol and then validate the actual parameters
bool BURS::analyseActuals (SourceLocator loc, NExprSchema::IROp *irop,
  NExprSchema::IRTransformation::IRTree::Parameter::List *actuals, bool *bound, UInt32 *index)
{
  INFO_ASSERT (irop->getIndex () < mNTerminals, "blah");
  return mTerminal [irop->getIndex ()].analyseActuals (*this, loc, actuals, bound, index);
}

//! Check an ordered list of actual parameters against the formal parameters for this
/*! If the list is legitimate then the index into the bindings for this
 *  terminal is returned in index.
 */
bool BURS::Terminal::analyseActuals (BURS &burs, SourceLocator loc,
  NExprSchema::IRTransformation::IRTree::Parameter::List *list, bool *bound, UInt32 *index)
{
  bool status = true;
  // walk the formal parameter list and the actual parameter list in tandem,
  // checking types of formal against types of actuals
  Parameter::Index::iterator formals = mParamIndex.begin ();
  NExprSchema::IRTransformation::IRTree::Parameter::List::iterator actuals = list->begin ();
  while (formals != mParamIndex.end () && actuals != list->end ()) {
    Parameter *formal = formals->second;
    NExprSchema::IRTransformation::IRTree::Parameter *actual = *actuals;
    if (formal->isString () && !actual->isString ()) {
      burs.getSchema ().Error (loc, "Expected a string value for '%s' parameter.", 
        formal->getName ()->str ());
      burs.getSchema ().Info (mIROp->getDecl (), "This is the declaration of '%s'.",
        mIROp->getName ()->str ());
      status = false;
    } else if (formal->isInteger () && !actual->isInteger ()) {
      burs.getSchema ().Error (loc, "Expected an integer value for '%s' parameter.", 
        formal->getName ()->str ());
      burs.getSchema ().Info (mIROp->getDecl (), "This is the declaration of '%s'.",
        mIROp->getName ()->str ());
      status = false;
    } else {
      // The paramaters checked out
    }
    ValueMap::iterator found;
    if (!status) {
      // don't bother adding this value to the map
    } else if ((found = mActuals.find (formal->getName ())) == mActuals.end ()) {
      // create a new table for this formal containing this actual
      ValueList *map = new ValueList;
      mActuals.insert (ValueMap::value_type (formal->getName (), map));
      map->push_back (actual);
      *index = mNextActual++;
      *bound = true;
    } else {
      // add this actual to the binding list
      INFO_ASSERT (found->second->size () == mNextActual, "blah");
      found->second->push_back (actual);
      *index = mNextActual++;
      *bound = true;
    }
    ++formals;
    ++actuals;
  }
  // output error messages for any unbound formals
  while (formals != mParamIndex.end ()) {
    burs.mSchema.Error (loc, "No binding for terminal parameter %d: '%s'.", formals->first,
      formals->second->getName ()->str ());
    burs.mSchema.Info (mIROp->getDecl (), "This is the declaration of '%s'.",
      mIROp->getName ()->str ());
    ++formals;
    status = false;
  }
  // output error messages for any bogus actuals
  if (actuals != list->end ()) {
    burs.mSchema.Error (loc, "Bogus actual parameter for '%s'.", mIROp->getName ()->str ());
    burs.mSchema.Info (mIROp->getDecl (), "This is the declaration of '%s'.",
      mIROp->getName ()->str ());
    status = false;
  }
  return status;
}

//
// IRTransformation
//

//! Perform consistency checks on this
bool NExprSchema::IRTransformation::check (NExprSchema *schema)
{
  bool status = true;
  INFO_ASSERT (mClass != NULL, "transformation has no class");
  ContextWithOrdinalMap context (*mOrdmap);
  status &= mRule->check (schema, context);
  return status;
}

//! Perform consistency checks on this
bool NExprSchema::IRTransformation::IRTree::check (NExprSchema *schema, CodeContext &context)
{
  bool status = true;
  if (mKey != NULL && mCode != NULL) {
    schema->Error (mLoc, "Corrupted IRTree: contains key and macro.");
  } else if (mKey == NULL && mCode == NULL) {
    schema->Error (mLoc, "Corrupted IRTree: contains neither key nor macro.");
  } else if (mCode != NULL) {
    status &= mCode->check (schema, context);
  } else if (!schema->isDeclared (mKey, &mIROp)) {
    schema->Error (mLoc, "Undeclared LIR operator '%s'.", mKey->str ());
    status = false;
  } else if ((mIROp->getArity () == 0 && getArity () == 1 
              && mLeft->getArity () == 0) || mLeft->getCode () != NULL) {
    // special case - a leaf operator with a constructor argument
    status &= mLeft->check (schema, context);
  } else if (mIROp->getArity () != getArity ()) {
    schema->Error (mLoc, "wrong number of operands for '%s'; expected %d, found %d.",
      mKey->str (), mIROp->getArity (), getArity ());
    status = false;
  }
  if (mLeft != NULL) {
    status &= mLeft->check (schema, context);
  }
  if (mRight != NULL) {
    status &= mRight->check (schema, context);
  }
  return status;
}

/*virtual*/ NExprSchema::IRTransformation::IRTree::~IRTree ()
{
  if (mActuals != NULL) {
    delete mActuals;
  }
}

//! Analyse actual parameters in the IRTree
bool NExprSchema::IRTransformation::IRTree::analyseActuals (BURS &burs)
{
  bool status = true;
  if (mIROp == NULL) {
    INFO_ASSERT (mActuals == NULL, "blah");
  } else if (mActuals != NULL) {
    status &= burs.analyseActuals (mLoc, mIROp, mActuals, &mHasParameters, &mActualBinding);
  } else if (burs.hasParameters (mIROp)) {
    burs.getSchema ().Error (mLoc, "IR operator '%s' requires parameters.", mIROp->getName ()->str ());
    burs.getSchema ().Info (mIROp->getDecl (), "This is the declaration of '%s'.",
      mIROp->getName ()->str ());
    status = false;
  }
  return status;
}

//! Analyse actual parameters in the IRTree
bool NExprSchema::IRTransformation::RuleTree::analyseActuals (BURS &burs)
{
  bool status = true;
  if (mIRTree != NULL) {
    // there is an IRTree tied to this node, so check it...
    status &= mIRTree->analyseActuals (burs);
  } else {
  }
  // recursively check any children
  if (mLeft != NULL) {
    status &= mLeft->analyseActuals (burs);
  }
  if (mRight != NULL) {
    status &= mRight->analyseActuals (burs);
  }
  return status;
}

//! Perform consistency checks on this
bool NExprSchema::IRTransformation::RuleTree::check (NExprSchema *schema, CodeContext &context)
{
  bool status = true;
  switch (mFlavour) {
  case eIRTREE:
    if (mIRTree == NULL || mCode != NULL || mLeft != NULL || mRight != NULL) {
      schema->Error (mLoc, "badly formed rule tree for eIRTREE");
      status = false;
    } else {
      status &= mIRTree->check (schema, context);
    }
    break;
  case eCODE:
    if (mCode == NULL || mIRTree != NULL || mLeft != NULL || mRight != NULL) {
      schema->Error (mLoc, "badly formed rule tree for eCODE");
      status = false;
    } else {
      mCode->bind (mClass);
      status &= mCode->check (schema, context);
    }
    break;
  case eSWITCH:
    if (mCode != NULL || mIRTree != NULL) {
      schema->Error (mLoc, "badly formed rule tree for eSWITCH");
      status = false;
    } else if (mLeft == NULL || mLeft->mFlavour != eCODE) {
      schema->Error (mLoc, "badly formed left child for eSWITCH rule tree");
      status = false;
    } else if (mRight == NULL || mRight->mFlavour != eCASEPAIR) {
      schema->Error (mLoc, "badly formed right child for eSWITCH rule tree");
      status = false;
    } else {
      status &= mLeft->check (schema, context);
      status &= mRight->check (schema, context);
    }
    break;
  case eCASE:
    if (mCode != NULL || mIRTree != NULL) {
      schema->Error (mLoc, "badly formed rule tree for eCASE");
      status = false;
    } else if (mLeft == NULL || mLeft->mFlavour != eCODE) {
      schema->Error (mLoc, "badly formed left child for eCASE rule tree");
      status = false;
    } else if (mRight == NULL || mRight->mFlavour != eIRTREE) {
      schema->Error (mLoc, "badly formed right child for eCASE rule tree");
      status = false;
    } else {
      status &= mLeft->check (schema, context);
      status &= mRight->check (schema, context);
    }
    break;
  case eCASEPAIR:
    if (mCode != NULL || mIRTree != NULL) {
      schema->Error (mLoc, "badly formed rule tree for eCASEPAIR");
      status = false;
    } else if (mLeft == NULL || mLeft->mFlavour != eCASE) {
      schema->Error (mLoc, "badly formed left child for eCASEPAIR rule tree");
      status = false;
    } else if (mRight != NULL && mRight->mFlavour != eCASEPAIR) {
      schema->Error (mLoc, "badly formed right child for eCASEPAIR rule tree");
      status = false;
    } else {
      status &= mLeft->check (schema, context);
      status &= mRight == NULL || mRight->check (schema, context);
    }
    if (mRight != NULL) {
      mRight->setProperties (eINNER);
    }
    break;
  case eWHEN:
    if (mCode != NULL || mIRTree != NULL) {
      schema->Error (mLoc, "badly formed rule tree for eWHEN");
      status = false;
    } else if (mLeft != NULL && mLeft->mFlavour != eCODE) {
      schema->Error (mLoc, "badly formed left child for eWHEN rule tree");
      status = false;
    } else if (mRight == NULL || mRight->mFlavour != eIRTREE) {
      schema->Error (mLoc, "badly formed right child for eWHEN rule tree");
      status = false;
    } else {
      status &= mLeft == NULL || mLeft->check (schema, context);
      status &= mRight->check (schema, context);
    }
    break;
  case eWHENPAIR:
    if (mCode != NULL || mIRTree != NULL) {
      schema->Error (mLoc, "badly formed rule tree for eWHENPAIR");
      status = false;
    } else if (mLeft == NULL || mLeft->mFlavour != eWHEN) {
      schema->Error (mLoc, "badly formed left child for eWHENPAIR rule tree");
      status = false;
    } else if (mRight != NULL && mRight->mFlavour != eWHENPAIR) {
      schema->Error (mLoc, "badly formed right child for eWHENPAIR rule tree");
      status = false;
    } else {
      status &= mLeft->check (schema, context);
      status &= mRight == NULL || mRight->check (schema, context);
    }
    if (mRight != NULL) {
      mRight->setProperties (eINNER);
    }
    break;
  default:
    schema->Error (mLoc, "totally corrupt rule tree");
    status = false;
    break;
  }
  return status;
}

//
// BURS::Rules
//

BURS::Rules::Rules (BURS &burs, const UInt32 n_rules) : 
  mBurs (burs), mNRules (n_rules), mNext (0), mNChainRules (0), mChain (NULL)
{
  // allocate a suitable length vector of Rule object
  mRule = new Rule [mNRules];
}

BURS::Rules::~Rules ()
{
  delete [] mRule;                      // the rule vector
  if (mChain != NULL) {
    delete mChain;                      // the chain costs
  }
}

//! Add a rules to the aggregates
bool BURS::Rules::append (NExprSchema::Rewrite *rewrite)
{
  INFO_ASSERT (mNext < mNRules, "rule map overflow");
  NExprSchema::NonTerminal *lhs = rewrite->getNonTerminal ();
  NExprSchema::Rewrite::Pattern *pattern = rewrite->getPattern ();
  StringAtom *op = pattern->getSymbol ();
  NExprSchema::Rewrite::Pattern *l = pattern->getLeft ();
  NExprSchema::Rewrite::Pattern *r = pattern->getRight ();
  // Identify the operator of the pattern
  NExprSchema::NonTerminal *nt = NULL;
  NExprSchema::IROp *t = NULL;
  if (!mBurs.mSchema.isDeclared (op, &nt)) {
    // this is not a non-terminal
  } else if (l != NULL || r != NULL) {
    // non-terminal used as the label of a tree node
    mBurs.mSchema.Error (pattern->getLoc (), "Non-terminal '%s' used a tree label.",
      nt->getName ()->str ());
    return false;
  } else {
    // this is a chain rule
    mRule [mNext].bind (rewrite, lhs, nt);
    mNext++;
  }
  NExprSchema::NonTerminal *child [2] = {NULL, NULL};
  if (!mBurs.mSchema.isDeclared (op, &t)) {
    // this is not a terminal
  } else {
    switch (t->getArity ()) {
    case 0:
      if (l != NULL || r != NULL) {
        mBurs.mSchema.Error (pattern->getLoc (), "Too many operands for nullary operator '%s'.",
          t->getName ()->str ());
        return false;
      } else {
        // this is a leaf rule
        mRule [mNext].bind (rewrite, lhs, t, child);
        mNext++;
      }
      break;
    case 1:
      if (l == NULL) {
        mBurs.mSchema.Error (pattern->getLoc (), "Not enough operands for unary operator '%s'.",
          t->getName ()->str ());
        return false;
      } else if (r != NULL) {
        mBurs.mSchema.Error (pattern->getLoc (), "Too many operands for unary operator '%s'.",
          t->getName ()->str ());
        return false;
      } else if (!mBurs.mSchema.isDeclared (l->getSymbol (), &child [0])) {
        mBurs.mSchema.Error (l->getLoc (), "Left operand of '%s' is not a non-terminal: '%s'.",
          op->str (), l->getSymbol ()->str ());
        return false;
      } else {
        // this is a valid unary rule
        mRule [mNext].bind (rewrite, lhs, t, child);
        mNext++;
      }
      break;
    case 2:
      if (l == NULL || r == NULL) {
        mBurs.mSchema.Error (pattern->getLoc (), "Not enough operands for unary operator '%s'.",
          t->getName ()->str ());
        return false;
      } else if (!mBurs.mSchema.isDeclared (l->getSymbol (), &child [0])) {
        mBurs.mSchema.Error (l->getLoc (), "Left operand of '%s' is not a non-terminal: '%s'.",
          op->str (), l->getSymbol ()->str ());
        return false;
      } else if (!mBurs.mSchema.isDeclared (r->getSymbol (), &child [1])) {
        mBurs.mSchema.Error (r->getLoc (), "Right operand of '%s' is not a non-terminal: '%s'.",
          op->str (), r->getSymbol ()->str ());
        return false;
      } else {
        // this is a valid binary rule
        mRule [mNext].bind (rewrite, lhs, t, child);
        mNext++;
      }
      break;
    default:
      mBurs.mSchema.Error (pattern->getLoc (), "Terminal '%s' requires too many arguments.",
        op->str ());
      return false;
      break;
    }
  }
  if (nt == NULL && t == NULL) {
    mBurs.mSchema.Error (pattern->getLoc (), "'%s': undeclared symbol.", op->str ());
    return false;
  } else if (nt != NULL && t != NULL) {
    mBurs.mSchema.Error (pattern->getLoc (), "'%s' is ambiguous.", op->str ());
    mBurs.mSchema.Info (nt->getDecl (), "This is the use of '%s' as a non-terminal.", op->str ());
    mBurs.mSchema.Info (t->getDecl (), "This is the declaration of '%s' as a terminal.", op->str ());
    return false;
  }
  return true;
}

//! Sort the rules
/*! The ordering is such that chain rules come before non-chain rules,
 *  nullary rules before unary, unary before binary and all the non-chain
 *  rules with the same operator are contiguous.
 */
void BURS::Rules::sortRules ()
{
  INFO_ASSERT (mNext == mNRules, "wrong number of rules");
  BURS::sort <Rule> (mRule, 0, mNRules - 1, Rule::lessThan);
}

//
// BURS::Rule
//

//! Initialise a chain rule from a schema rewrite rule.
void BURS::Rule::bind (NExprSchema::Rewrite *rewrite, 
  NExprSchema::NonTerminal *lhs, NExprSchema::NonTerminal *rhs)
{
  INFO_ASSERT (mRewrite == NULL, "multiply bound rule");
  mRewrite = rewrite;
  mIsChain = true;
  mCost = rewrite->getCost ();
  mRhs = rhs->getIndex ();
  mArity = 0;
  mLhs = lhs->getIndex ();
  mChild [0] = 0;
  mChild [1] = 0;
}

//! Initialise a non-chain rule from a schema rewrite rules and a child tuple
void BURS::Rule::bind (NExprSchema::Rewrite *rewrite, NExprSchema::NonTerminal *lhs,
  NExprSchema::IROp *rhs, NExprSchema::NonTerminal *child [2])
{
  INFO_ASSERT (mRewrite == NULL, "multiply bound rule");
  mRewrite = rewrite;
  mIsChain = false;
  mCost = rewrite->getCost ();
  mLhs = lhs->getIndex ();
  mRhs = rhs->getIndex ();
  mArity = rhs->getArity ();
  mLhs = lhs->getIndex ();
  if (mArity == 0) {
    INFO_ASSERT (child [0] == NULL && child [1] == NULL, "bogus operands");
    mChild [0] = 0;
    mChild [1] = 0;
  } else if (mArity == 1) {
    INFO_ASSERT (child [0] != NULL && child [1] == NULL, "bogus operands");
    mChild [0] = child [0]->getIndex ();
    mChild [1] = 0;
  } else if (mArity == 2) {
    INFO_ASSERT (child [0] != NULL && child [1] != NULL, "bogus operands");
    mChild [0] = child [0]->getIndex ();
    mChild [1] = child [1]->getIndex ();
  } else {
    INFO_ASSERT (mArity < 3, "bogus arity");
  }
}

//! Comparison function for sorting rules
/*static*/ int BURS::Rule::lessThan (const Rule &a, const Rule &b)
{
  if (a.mIsChain && !b.mIsChain) {
    // a is a chain rule, b is not a chain rule
    return -1;
  } else if (!a.mIsChain && b.mIsChain) {
    // a is not a chain rule, b is a chain rule
    return 1;
  } else if (a.mIsChain && b.mIsChain) {
    // sort on the operator
    return (a.mRhs - b.mRhs);
  } else if (a.mArity != b.mArity) {
    // sort on the arity
    return (a.mArity - b.mArity);
  } else if (a.mRhs != b.mRhs) {
    // sort on the operator
    return (a.mRhs - b.mRhs);
  } else if (a.mArity == 0) {
    // sort on the cost
    return (a.mCost - b.mCost);
  } else if (a.mChild [0] != b.mChild [0]) {
    // sort on the left child symbol
    return (a.mChild [0] - b.mChild [0]);
  } else if (a.mArity == 1) {
    // sort on the cost
    return (a.mCost - b.mCost);
  } else if (a.mChild [1] != b.mChild [1]) {
    // sort on the right child symbol
    return (a.mChild [1] - b.mChild [1]);
  } else {
    // sort on the cost
    return (a.mCost - b.mCost);
  }
}

//
// BURS::Transitions
//

BURS::Transitions::Transitions (BURS &burs) : 
  mBurs (burs), mNTransitions (0), mTransition (NULL)
{
  // allocate a mapping from state x state to a transition for each non terminal
  mTransition = new Transition::Map  [mBurs.mNTerminals];
}

/*virtual*/ BURS::Transitions::~Transitions ()
{
  if (mTransition != NULL) {
    for (UInt32 n = 0; n < mBurs.mNTerminals; n++) {
      // trash each and every transition object reachable from the mappings
      Transition::Map::iterator it;
      for (it = mTransition [n].begin (); it != mTransition [n].end (); ++it) {
        delete it->second;
      }
    }
    delete [] mTransition;              // trash the vector
  }
}

//! Add a transition to the set of transitions
void BURS::Transitions::add (UInt32 op, UInt16 child[2], UInt16 result)
{
  Tuple tuple (child);
  Transition::Map::iterator found;
  if ((found = mTransition [op].find (tuple)) != mTransition [op].end ()) {
    // this transition is already registered
    INFO_ASSERT (found->second->getOutput () == result, "inconsistent transitions");
  } else {
    mNTransitions++;
    Transition *transition = new Transition (op, tuple, result);
    mTransition [op].insert (Transition::Map::value_type (tuple, transition));
    if (mBurs.isTraced ()) {
      UtIO::cout () << "transition ";
      transition->print (UtIO::cout (), mBurs.mTerminal [op]);
    }
  }
}

//
// BURS::States
//

/*virtual*/ BURS::States::~States ()
{
  // trash each state and proto state
  for (State::Obarray::iterator it = mStateObarray.begin (); it != mStateObarray.end (); ++it) {
    delete it->first;
  }
  for (ProtoState::Obarray::iterator it = mProtoObarray.begin (); it != mProtoObarray.end (); ++it) {
    delete it->first;
  }
  
}

//! \return the state with the given unique index
BURS::State *BURS::States::getState (const UInt16 n) const
{
  UtMap <UInt16, State *>::const_iterator found;
  if ((found = mStateTable.find (n)) == mStateTable.end ()) {
    return NULL;
  } else {
    return found->second;
  }
}

//! \return the proto state with the given unique proto state index
BURS::ProtoState *BURS::States::getProto (const UInt16 n) const
{
  UtMap <UInt16, ProtoState *>::const_iterator found;
  if ((found = mProtoTable.find (n)) == mProtoTable.end ()) {
    return NULL;
  } else {
    return found->second;
  }
}

//
// BURS::State
//

BURS::State::State (BURS &burs, const UInt16 index, const UInt32 op) :
  mBurs (burs), mIndex (index), mTerminal (op), mSize (burs.mNNonTerminals), mLive (false), 
  mItem (new Item [mSize])
{}

BURS::State::~State ()
{
  delete mItem;
}

//! Sorting comparison for states
bool BURS::State::lessThan::operator () (State *a, State *b)
{
  INFO_ASSERT (a->mSize == b->mSize, "itemset sizing error");
  if (a->mTerminal != b->mTerminal) {
    return (a->mTerminal < b->mTerminal);
  }
  for (UInt32 n = 0; n < a->mSize; n++) {
    if (a->mItem [n].mCost != b->mItem [n].mCost) {
      return (a->mItem [n].mCost < b->mItem [n].mCost);
    } else if (a->mItem [n].mRule != b->mItem [n].mRule) {
      return (a->mItem [n].mRule < b->mItem [n].mRule);
    }
  }
  return false;
}

//! Equality comparison for states
bool BURS::State::isEqual::operator () (State *a, State *b)
{
  INFO_ASSERT (a->mSize == b->mSize, "itemset sizing error");
  if (a->mTerminal != b->mTerminal) {
    return false;
  }
  for (UInt32 n = 0; n < a->mSize; n++) {
    if (a->mItem [n].mRule != b->mItem [n].mRule) {
      return false;
    } else if (a->mItem [n].mCost != b->mItem [n].mCost) {
      return false;
    }
  }
  return true;
}

//
// BURS::ProtoStates
//

BURS::ProtoState::ProtoState (BURS &burs, const UInt16 index) :
  mBurs (burs), mIndex (index), mSize (mBurs.mNNonTerminals), 
  mCost (new UInt16 [mSize])
{
  for (UInt32 n = 0; n < mSize; n++) {
    mCost [n] = eINFINITY;
  }
}

/*virtual*/ BURS::ProtoState::~ProtoState ()
{
  delete mCost;
}

//! Sorting comparison for proto states
bool BURS::ProtoState::lessThan::operator () (ProtoState *a, ProtoState *b)
{
  INFO_ASSERT (a->mSize == b->mSize, "protostate set size error");
  for (UInt32 n = 0; n < a->mSize; n ++) {
    if (a->mCost [n] != b->mCost [n]) {
      return (a->mCost [n] < b->mCost [n]);
    }
  }
  return false;
}

//! Equality comparison for proto states
bool BURS::ProtoState::isEqual::operator () (ProtoState *a, ProtoState *b)
{
  INFO_ASSERT (a->mSize == b->mSize, "protostate set size error");
  for (UInt32 n = 0; n < a->mSize; n ++) {
    if (a->mCost [n] < b->mCost [n]) {
      return false;
    }
  }
  return true;
}

//
// Sorting
//

//! Recursive quicksort
template <typename _T_>
/*static*/ void BURS::sort (_T_ *vector, SInt32 lo, SInt32 hi, 
  int (compare) (const _T_ &, const _T_ &))
{
  while (hi > lo) {
    int i = lo;
    int j = hi;
    _T_ tmp = vector [lo];
    // partition the array
    while (i < j) {
      while (compare (tmp, vector [j]) < 0) {
        j--;
      }
      vector [i] = vector [j];
      while (i < j && compare (vector [i], tmp) <= 0) {
        i++;
      }
      vector [j] = vector [i];
    }
    vector [i] = tmp;
    // sort recursively - the smaller partition first
    if (i - lo < hi - i) {
      sort (vector, lo, i - 1, compare);
      lo = i + 1;
    } else {
      sort (vector, i + 1, hi, compare);
      hi = i - 1;
    }
  }
}

//
// BURS::CostMatrix
//

//! ctor for an x by y matrix with a well-defined initial value
BURS::CostMatrix::CostMatrix (const UInt32 x, const UInt32 y, UInt16 initial) :
  mX (x), mY (y)
{
  mMatrix = new UInt16 [mX * mY];
  for (UInt32 n = 0; n < mX * mY; n++) {
    mMatrix [n] = initial;
  }
}

/*virtual*/ BURS::CostMatrix::~CostMatrix ()
{
  delete [] mMatrix;
}

//! \return cell (x, y) of the matrix
UInt16 &BURS::CostMatrix::get (const UInt32 x, const UInt32 y)
{
  INFO_ASSERT (x < mX && y < mY, "index out of range");
  return mMatrix [y * mX + x];
}

//
// BURS::BitMap
//

//! \class BitMap
/*! This class implements dynamically growing bitmaps. This is used during
 *  analysis to implement sets of proto states indices. Given that the number
 *  of proto states cannot be known a priori the bit map implementation must
 *  grow as needed.
 */

BURS::BitMap::BitMap () : mSize (32), mCell (new UInt32 [mSize]) 
{
  memset (mCell, 0, sizeof (UInt32) * mSize);
}

//! \return iff n is set.
bool BURS::BitMap::isSet (const UInt16 n) const
{
  UInt16 word = n >> 5;                 //!< word in the mCell vector
  UInt16 bit = n & 0x1F;                //!< bit within the word
  if (word > mSize) {
    return false;
  } else {
    return (mCell [word] & (1<<bit)) != 0;
  }
}

//! Set an item in the bitmap
void BURS::BitMap::set (const UInt32 n)
{
  UInt16 word = n >> 5;                 //!< word in the mCell vector
  UInt16 bit = n & 0x1F;                //!< bit within the word
  if (word >= mSize) {
    // The vector of integers is not long enough to store the new entry so it
    // must be grown.
    UInt32 newsize = (word * 10)/4;     //!< new size of the mCell vector
    UInt32 *newcell = new UInt32 [newsize];
    // copy the data from the previous vector
    memcpy (newcell, mCell, sizeof (UInt32) * mSize);
    // initialise the new entries to zero
    memset (newcell + sizeof (UInt32) * mSize, 0, sizeof (UInt32) * (newsize - mSize));
    // trash the old vector and glue the new one into its place
    delete [] mCell;
    mCell = newcell;
    mSize = newsize;
  }
  mCell [word] |= (1<<bit);             //!< set the appropriate bit
}

//! Advance the iterator over a bitmap
void BURS::BitMap::loop::operator ++ ()
{
  while (mWord < mMap.mSize) {
    if (mBit >= 1<<5) {
      // advance to the next word
      mBit = 0;
      mWord++;
    } else if (mMap.mCell [mWord] & (1<<mBit)) {
      // found a set bit
      mNext = (mWord * 1<<5) + mBit;
      mBit++;
      return;
    } else {
      // this bit is reset so advance to the next bit in the current word
      mBit++;
    }
  }
  // hit the end of the vector
  mAtEnd = true;
}
//
// Visualisation
//

//! Print a rule to a stream
void BURS::Rule::print (UtOStream &s) const
{
  if (mIsChain) {
    s << "chain ";
  } else if (mArity == 0) {
    s << "leaf ";
  } else if (mArity == 1) {
    s << "unary ";
  } else if (mArity == 2) {
    s << "binary ";
  }
  s << "rule " << mLhs << " <- " << mRhs;
  if (mArity == 1) {
    s << " (" << mChild [0] << ")";
  } else if (mArity == 2) {
    s << " (" << mChild [0] << ", " << mChild[1] << ")";
  }
  s << " from ";
  mRewrite->print (s);
}

//! Print a rule to stdout
void BURS::Rule::pr () const
{
  print (UtIO::cout ());
}

//! Print a formal parameter to an indenting stream
void BURS::Terminal::Parameter::print (IndentedStream <UtOStream> &s) const
{
  if (isString ()) {
    s << "string";
  } else if (isInteger ()) {
    s << "integer";
  } else {
    s << "### bogus ###";
  }
  s << "\t" << mName->str ();
}

//! Print a terminal to a stream
void BURS::Terminal::print (UtOStream &s0) const
{
  IndentedStream <UtOStream> s (s0, (70<<8));
  s << "operator " << mIndex << " is " << getName ();
  if (mArity == 0) {
    // no representatives
  } else if (mArity == 1) {
    s << " ([";
    mProto [0].print (s);
    s << "])";
  } else if (mArity == 2) {
    s << "([";
    mProto [0].print (s);
    s << "], [";
    mProto [1].print (s);
    s << "])";
  }
  s.nl ();
  s++;
  Parameter::Index::const_iterator it;
  for (it = mParamIndex.begin (); it != mParamIndex.end (); ++it) {
    s << "parameter " << it->first << "\t";
    it->second->print (s);
    s << "\n";
    s++;
    ValueMap::const_iterator found;
    if ((found = mActuals.find (it->second->getName ())) == mActuals.end ()) {
      s << "### no actual binding list ###\n";
    } else {
      for (ValueList::const_iterator it = found->second->begin (); it != found->second->end (); ++it) {
        if (it != found->second->begin ()) {
          s << ",\t";
        }
        (*it)->print (s);
      }
      s << "\n";
    }
    s--;
  }
  s--;
}

//! Print a terminal to stdout
void BURS::Terminal::pr () const
{
  print (UtIO::cout ());
}

//! Print a Nucleus to LIR transformation to a stream
void NExprSchema::IRTransformation::print (UtOStream &s) const
{
  UtString buffer;
  getDecl ().compose (&buffer);
  s << buffer << ":\n";
  mRule->print (s, 0);
  s << "\n";
}

//! Print a Nucleus to LIR transformation to stdout
void NExprSchema::IRTransformation::pr () const
{
  print (UtIO::cout ());
}

//! Print a Nucleus to LIR transformation rule to a stream
void NExprSchema::IRTransformation::RuleTree::print (UtOStream &s, const UInt32 indent) const
{
  UtString leaders (indent, ' ');
  UtString b0, b1;
  mLoc.compose (&b0);
  switch (mFlavour) {
  case eIRTREE:
    s << leaders;
    mIRTree->print (s);
    break;
  case eCODE:
    mCode->compose (&b1);
    s << "(" << b1 << ")";
    break;
  case eSWITCH:
    s << leaders << "switch ";
    if (mLeft == NULL) {
      s << "###NULL###";
    } else {
      mLeft->print (s, indent);
    }
    s << "{\n";
    if (mRight == NULL) {
      s << leaders << "  ###NULL###\n";
    } else {
      mRight->print (s, indent + 2);
    }
    s << leaders << "}\n";
    break;
  case eCASE:
    s << leaders << "case ";
    if (mLeft == NULL) {
      s << leaders << "  ###NULL###\n";
    } else {
      mLeft->print (s, indent);
    }
    s << ": ";
    if (mRight == NULL) {
      s << "###NULL$$$\n";
    } else {
      mRight->print (s, indent);
      s << "\n";
    }
    break;
  case eCASEPAIR:
    if (mLeft == NULL) {
      s << leaders << "###NULL###\n";
    } else {
      mLeft->print (s, indent);
    }
    if (mRight != NULL) {
      mRight->print (s, indent);
    }
    break;
  case eWHEN:
    if (mLeft == NULL) {
      s << leaders << "others => ";
    } else {
      s << leaders << "when ";
      mLeft->print (s, indent);
      s << " => ";
    }
    if (mRight == NULL) {
      s << "###NULL$$$\n";
    } else {
      mRight->print (s, indent);
      s << "\n";
    }
    break;
  case eWHENPAIR:
    if (mLeft == NULL) {
      s << leaders << "###NULL###\n";
    } else {
      mLeft->print (s, indent);
    }
    if (mRight != NULL) {
      mRight->print (s, indent);
    }
    break;
  default:
    s << b0 << ": *** corrupted RuleTree ***\n";
    break;
  }
}

//! Print a Nucleus to LIR transformation rule to stdout
void NExprSchema::IRTransformation::RuleTree::pr () const
{
  print (UtIO::cout (), 0);
}

//! Print the actual parameter to a stream
void NExprSchema::IRTransformation::IRTree::Parameter::print (UtOStream &s) const
{
  if (isString ()) {
    s << "string " << mString->str ();
  } else if (isInteger ()) {
    s << "integer " << mInteger;
  } else {
    s << "### bogus ###";
  }
}

//! Print the actual parameter to an indenting stream
void NExprSchema::IRTransformation::IRTree::Parameter::print (IndentedStream <UtOStream> &s) const
{
  if (isString ()) {
    s << "string " << mString->str ();
  } else if (isInteger ()) {
    s << "integer " << mInteger;
  } else {
    s << "### bogus ###";
  }
}

//! Print the actual parameter to standard output
void NExprSchema::IRTransformation::IRTree::Parameter::pr () const
{
  print (UtIO::cout ());
}

//! Print a transformation LIR tree to a stream
void NExprSchema::IRTransformation::IRTree::print (UtOStream &s) const
{
  if (mCode != NULL) {
    UtString b0;
    mCode->compose (&b0);
    s << b0;
  }
  if (mKey != NULL) {
    s << mKey->str ();
  }
  if (mActuals != NULL) {
    s << "[";
    Parameter::List::const_iterator it;
    for (it = mActuals->begin (); it != mActuals->end (); it++) {
      if (it != mActuals->begin ()) {
        s << ", ";
      }
      (*it)->print (s);
    }
    s << "]";
  }
  if (mLeft != NULL && mRight == NULL) {
    s << "(";
    mLeft->print (s);
    s << ")";
  } else if (mLeft != NULL && mRight != NULL) {
    s << "(";
    mLeft->print (s);
    s << ", ";
    mRight->print (s);
    s << ")";
  }
}

//! Print a transformation LIR tree to stdout
void NExprSchema::IRTransformation::IRTree::pr () const
{
  print (UtIO::cout ());
  UtIO::cout () << "\n";
}

//! Print the rule set to a stream
void BURS::Rules::print (UtOStream &s) const
{
  for (UInt32 n = 0; n < mNRules; n++) {
    Rule &rule = mRule [n];
    rule.print (s);
  }
}

//! Print the rule set to standard output
void BURS::Rules::pr () const
{
  print (UtIO::cout ());
}

//! Print a transition to a stream
void BURS::Transitions::Transition::print (UtOStream &s, const Terminal &op) const
{
  INFO_ASSERT (op.getIndex () == mTerminal, "blah");
  s << op.getName () << " " << op.getIndex ();
  if (mChild [0] == eNOT_A_STATE) {
    // leaf
  } else if (mChild [1] == eNOT_A_STATE) {
    s << " (" << mChild [0] << ")";
  } else {
    s << " (" << mChild [0] << ", " << mChild [1] << ")";
  }
  s << " to " << mOutput << "\n";
}

//! Print a set of transitions to a stream
void BURS::Transitions::print (UtOStream &s) const
{
  for (UInt32 m = 0; m < mBurs.mNTerminals; m++) {
    Terminal &op = mBurs.mTerminal [m];
    Transition::Map::const_iterator it;
    for (it = mTransition [m].begin (); it != mTransition [m].end (); it++) {
      it->second->print (s, op);
    }
    
  }
}

//! Print a set of transitions to stdout
void BURS::Transitions::pr () const
{
  print (UtIO::cout ());
}

//! Print a proto state to a stream
void BURS::ProtoState::print (UtOStream &s0, const UInt32 indent) const
{
  IndentedStream <UtOStream> s (s0, indent);
  s << "proto state " << mIndex << ":";
  UInt32 live = 0;
  for (UInt32 n = 0; n < mSize; n++) {
    if (mCost [n] != eINFINITY) {
      s << "\t" << mBurs.nonTerminalName (n) << "=" << mCost [n];
      live++;
    }
  }
  if (live == 0) {
    s << "\tdead state";
  }
  s.nl ();
}

//! Print a proto state to stdout
void BURS::ProtoState::pr () const
{
  print (UtIO::cout ());
}

//! Print a state to a stream
void BURS::State::print (UtOStream &s0, const UInt32 indent) const
{
  IndentedStream <UtOStream> s (s0, indent);
  s << "state " << mIndex << " for operator " << mBurs.terminalName (mTerminal) << ":";
  for (UInt32 n = 0; n < mSize; n++) {
    if (mItem [n].mCost != eINFINITY) {
      s << "\t" << mBurs.nonTerminalName (n) << "=[" 
        << mItem [n].mRule << "," << mItem [n].mCost << "]";
    }
  }
  s.nl ();
}

//! Print a state to stdout
void BURS::State::pr () const
{
  print (UtIO::cout ());
}

//! Print a set of states to a stream
void BURS::States::print (UtOStream &s) const
{
  s << mStateTable.size () << " states.\n";
  UtMap <UInt16, State *>::const_iterator states;
  for (states = mStateTable.begin (); states != mStateTable.end (); ++states) {
    states->second->print (s);
  }
  s << mProtoTable.size () << " protostates.\n";
  UtMap <UInt16, ProtoState *>::const_iterator protostates;
  for (protostates = mProtoTable.begin (); protostates != mProtoTable.end (); ++protostates) {
    protostates->second->print (s);
  }
}

//! Print a set of states to stdout
void BURS::States::pr () const
{
  print (UtIO::cout ());
}

//! Print a bitmap to an indented stream
void BURS::BitMap::print (IndentedStream <UtOStream> &s) const
{
  loop loop (*this);
  bool is_first = true;
  while (!loop.atEnd ()) {
    if (is_first) {
      is_first = false;
    } else {
      s << "\t";
    }
    s << *loop;
    ++loop;
  }
}

//! Print a bitmap to a stream
void BURS::BitMap::print (UtOStream &s0, const UInt32 indent) const
{
  IndentedStream <UtOStream> s (s0, indent);
  print (s);
  s.nl (true);
}

//! Print a bitmap to stdout
void BURS::BitMap::pr () const
{
  print (UtIO::cout ());
}


