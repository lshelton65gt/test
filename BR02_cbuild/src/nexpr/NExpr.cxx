// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/AtomicCache.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtMap.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/CbuildMsgContext.h"
#include "nexpr/NExpr.h"

//! \struct CmpChar
/*! Comparison method for field map class that looks at the string contents.
 */
struct CmpChar {
  bool operator () (const char *a, const char *b) const
  {
    return strcmp (a, b) < 0;
  }
};

//! \class NExpr::FieldMap
/*! This class implemented the (:name, value) pairs for N-expressions.
 *
 *  \note Values come in the StringAtom names from the N-expression
 *  parser. However, they are looked up with char * names coming from literal
 *  strings in the various NExpr::read methods.
 *
 *  \note Declaring it here, rather than the header, removes the need to
 *  include UtMap.h in NExpr.h.
 */

class NExpr::FieldMap : public UtMap <const char *, Field *, CmpChar> {
public:

  FieldMap () : UtMap <const char *, Field *, CmpChar> () {}

  virtual ~FieldMap ()
  {
    // clean up the fields
    for (iterator it = begin (); it != end (); ++it) {
      delete it->second;
    }
  }

  //! Insert a new field into the map.
  /*! \param loc the location in the N-expression file of the (name, value) tuple
   *  \param name the name of the field
   *  \param value the value of the field
   *  \param errmsg location to return an error message is adding the field fails
   *  \return iff the field was added to the map
   */
  bool addField (SourceLocator loc, StringAtom *name, BaseValue *value, UtString *errmsg)
  {
    iterator found;
    if ((found = find (name->str ())) == end ()) {
      insert (value_type (name->str (), new Field (loc, name, value)));
      return true;
    } else {
      NExpr::Error (errmsg, "duplicate field: %s", name->str ());
      return false;
    }
  }

};

//! ctor for NExpr
NExpr::NExpr (const SourceLocator loc, const StringAtom *type) : 
  mLoc (loc), mType (type), mLabel (0)
{
  mFields = new NExpr::FieldMap;
  if (!lookupTypeIndex (type->str (), &mTypeIndex)) {
    mTypeIndex = eNOTYPE;
  }
}

NExpr::NExpr (const SourceLocator loc, const StringAtom *type, const UInt32 type_index) : 
  mLoc (loc), mType (type), mTypeIndex (type_index), mLabel (0)
{
  mFields = new NExpr::FieldMap;
}

/*virtual*/ NExpr::~NExpr ()
{
  delete mFields;
}

//! \return iff this is a particular type or a subclass of that type
bool NExpr::isType (const UInt32 type) const
{
  UInt32 actualtype;
  if (!getType (&actualtype)) {
    return false;
  } else if (type == actualtype) {
    return true;
  } else {
    return isSubclass (type, actualtype);
  }
}

//! Construct an object from this NExpr instance
bool NExpr::build (const NExpr::Reader &reader, void **object, UtString *errmsg) const
{
  UInt32 type;
  if (!getType (&type)) {
    *errmsg << getType ()->str () << " is not a recognised type.";
    return false;
  } else {
    return reader.build (*this, object, type, errmsg);
  }
}

//! Add a field to the N-expression.
bool NExpr::addField (SourceLocator loc, StringAtom *name, BaseValue *value, UtString *errmsg)
{
  return mFields->addField (loc, name, value, errmsg);
}

//! Lookup a named field in the field map.
bool NExpr::get (const char *name, Field **field, UtString *errmsg) const
{
  FieldMap::const_iterator found;
  if ((found = mFields->find (name)) == mFields->end ()) {
    return NExpr::Error (errmsg, "field '%s' not found", name);
  } else {
    *field = found->second;
    return true;
  }
}

//! Prepare an error message string for more text.
/*! Multiple error messages may be tacked together in a single message
 *  string. This method prepares the string by adding a continuation
 *  backslash and a new line if the message string is non empty.
 */
/*static*/ void NExpr::prepare (UtString *errmsg)
{
  if (!errmsg->empty ()) {
    // there is already text in the error message so add the backslash, a new
    // line and two spaces of indentation.
    *errmsg << " \\\n  ";
  }
}

//! Append a message to an error message.
/*static*/ bool NExpr::Error (UtString *errmsg, const char *fmt, ...)
{
  char buffer [256];
  va_list ap;
  va_start (ap, fmt);
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  va_end (ap);
  prepare (errmsg);
  *errmsg << buffer;
  return false;
}

//! Lookup the sTypeNames table for the index of a name
/*! \note sTypeNames is generated by the schema compiler.
 */
/*static*/ bool NExpr::lookupTypeIndex (const char *s, UInt32 *index)
{
  UInt32 lo = 0;
  UInt32 hi = sNClasses;
  while (hi > lo) {
    UInt32 mid = (hi + lo) / 2;
    int cmp = strcmp (s, sTypeNames [mid]);
    if (cmp < 0) {
      hi = mid;
    } else if (cmp > 0) {
      lo = mid + 1;
    } else {
      *index = mid;
      return true;
    }
  }
  return false;
}

//! Lookup an enum identifier
/*static*/ bool NExpr::lookupEnum (const UInt32 type, const char *ident, UInt32 *value)
{
  UInt32 lo = 0;
  UInt32 hi = sNEnumValues;
  while (hi > 0) {
    UInt32 mid = (hi + lo) / 2;
    int cmp;
    if (type < sEnumValues [mid].mType) {
      hi = mid;
    } else if (type > sEnumValues [mid].mType) {
      lo = mid;
    } else if ((cmp = strcmp (ident, sEnumValues [mid].mName)) < 0) {
      hi = mid;
    } else if (cmp > 0) {
      lo = mid;
    } else {
      // found it
      *value = sEnumValues [mid].mValue;
      return true;
    }
  }
  return false;
}

//! Get the name of a type.
/*static*/ const char *NExpr::typeName (const UInt32 n)
{
  INFO_ASSERT (n < sNClasses + sNBaseTypes, "type index out of bounds");
  return sTypeNames [n];
}

//! Lookup the sFieldNames table for the index of a field
/*static*/ bool NExpr::lookupFieldIndex (const char *s, UInt32 *index)
{
  UInt32 lo = 0;
  UInt32 hi = sNFieldNames;
  while (hi > lo) {
    UInt32 mid = (hi + lo) / 2;
    int cmp = strcmp (s, sFieldNames [mid]);
    if (cmp < 0) {
      hi = mid;
    } else if (cmp > 0) {
      lo = mid + 1;
    } else {
      *index = mid;
      return true;
    }
  }
  return false;
}

//! Get the name of a field
/*static*/ const char *NExpr::fieldName (const UInt32 n)
{
  INFO_ASSERT (n < sNFieldNames, "field index out of bounds");
  return sFieldNames [n];
}

//! Check that the actual type is consistent with the expected type
/*static*/ bool NExpr::typeCheck (const UInt32 expected, const UInt32 actual, 
  UtString *errmsg)
{
  if (expected == actual) {
    return true;                        // actual type is the expected type
  } else if (isSubclass (expected, actual)) {
    return true;                       // actual type is a subclass of the expected type
  } else {
    prepare (errmsg);
    *errmsg << "expected type " << typeName (expected) 
            << ", found type " << typeName (actual);
    return false;
  }
}

//! Test a type against the field
bool NExpr::checkType (const UInt32 field_index, const UInt32 actual_type, UtString *errmsg) const
{
  UInt32 expected;
  if (mTypeIndex == eNOTYPE) {
    *errmsg << "N-expression has an invalid type; found eNOTYPE, expected " << mType->str ();
    return false;
  } else if (!getType (mTypeIndex, field_index, &expected)) {
    *errmsg << "invalid field for " << mType->str ();
    return false;
  } else if (!typeCheck (expected, actual_type, errmsg)) {
    return false;
  } else {
    return true;
  }
}

//! Test the N-expression type lookup method
/*! This is run from the tripWire method and check for internal consistency in
 * with type lookup.
 */
/*static*/ void NExpr::checkTypeLookup ()
{
  // check all the valid names
  UInt32 m;
  for (UInt32 n = 0; n < sNClasses; n++) {
    INFO_ASSERT (lookupTypeIndex (typeName (n), &m) && m == n, sTypeNames [n]);
  }
  // now some invalid names
#define BADNAME(_x_) INFO_ASSERT (!lookupTypeIndex (_x_, &m), "found bogus type: " _x_);
  BADNAME ("AAAAAA");
  BADNAME ("ZZZZZZ");
  BADNAME ("NUContAzign");
  BADNAME ("ThisIsNotAType");
  BADNAME ("NeitherIsThis");
#undef BADNAME
}

//! Test the N-expression field lookup method
/*static*/ void NExpr::checkFieldLookup ()
{
  // check all the valid names
  UInt32 m;
  for (UInt32 n = 0; n < sNFieldNames; n++) {
    INFO_ASSERT (lookupFieldIndex (fieldName (n), &m) && m == n, sFieldNames [n]);
  }
  // now some invalid names
#define BADNAME(_x_) INFO_ASSERT (!lookupFieldIndex (_x_, &m), "found bogus field: " _x_);
  BADNAME ("AAAAAA");
  BADNAME ("ZZZZZZ");
  BADNAME ("mAAAA");
  BADNAME ("mZZZZ");
  BADNAME ("");
  BADNAME ("ThisIsNotAField");
  BADNAME ("NeitherIsThis");
#undef BADNAME
}

#if !NEXPR_ENABLE

// When N-expressions are turned off in the sandbox, the generated NExprIO.cxx
// is not compiled and linked into the executable. So, we need to stub out a
// minimal set of methods and objects.

/*static*/ const char *NExpr::sFieldNames [] = {NULL};
/*static*/ const UInt32 NExpr::sNFieldNames = 0;

/*static*/ const UInt32 NExpr::sNClasses = 0;
/*static*/ const UInt32 NExpr::sNBaseTypes = 0;
/*static*/ const char *NExpr::sTypeNames [] = {NULL};

/*static*/ UInt32 NExpr::sNEnumTypes = 0;
/*static*/ UInt32 NExpr::sNEnumValues = 0;
/*static*/ NExpr::EnumValue NExpr::sEnumValues [] = {EnumValue (0, NULL, 0)};

/*static*/ void NExpr::writeTripwire (const char *filename)
{
  UtOFStream out (filename);
  out << "// -*- C++ -*-\n";
  out << "/******************************************************************************\n";
  out << CarbonMem::CarbonGetCopyrightNotice ();
  out << " ******************************************************************************/\n";
  out << "\n";
  out << "// DO NOT EDIT THIS FILE\n";
  out << "// IT SHOULD BE WRITTEN BY cbuild -writeNExprTripwire filename\n";
  out << "// IF AN ASSERTION FAILS THEN CHECK THE N-EXPRESSION SCHEMA IN src/codegen/*.xxx\n";
  out << "\n";
  out << "#include \"util/CarbonAssert.h\"\n";
  out << "#include \"nexpr/NExpr.h\"\n";
  out << "\n";
  out << "/*static*/ void NExpr::tripWire ()\n";
  out << "{\n";
  out << "#ifndef NEXPR_ENABLE\n";
  out << "  UtIO::cout () << \"N-expressions are enabled\\n\";\n";
  out << "#else\n";
  out << "  UtIO::cout () << \"N-expressions are disabled\\n\";\n";
  out << "#endif\n";
  out << "}\n";
}

// Stub out the build method
bool NExpr::Reader::build (const NExpr &, void **, const UInt32, UtString *errmsg) const
{
  *errmsg << "N-expressions are disabled.";
  return false;
}

// Stub out the scanner
int NExpr::Reader::lex (int, Lexeme *)
{
  INFO_ASSERT (false, "N-expressions are disabled.");
  return 0;
}

//! Test whether a class is a subclass of another
/*static*/ bool NExpr::isSubclass (const UInt32, const UInt32)
{
  return false;
}

//! \return the type expected
/*static*/ bool NExpr::getType (const UInt32, const UInt32, UInt32 *)
{
  return false;
}

#endif

//! Dump an NExpr to a stream
void NExpr::print (UtOStream &s, const UInt32 indent) const
{
  UtString leaders (indent, ' ');
  s << leaders << "(" << getType ()->str ();
  for (FieldMap::const_iterator it = mFields->begin (); it != mFields->end (); it++) {
    s << "\n";
    s << leaders << "  :" << it->first << " ";
    it->second->getValue ()->print (s, indent+2);
  }
  s << leaders << ")\n";
}

void NExpr::pr () const
{
  print (UtIO::cout (), 0);
}

//! Print to an output stream
/*virtual*/ void NExpr::BaseValue::print (UtOStream &s, const UInt32) const
{
  UtString buffer;
  compose (&buffer);
  s << buffer << "\n";
}

//! Print a value to a standard out
void NExpr::BaseValue::pr () const
{
  print (UtIO::cout (), 0);
}

//! Print a field to a stream
void NExpr::Field::print (UtOStream &s) const
{
  UtString buffer;
  mLoc.compose (&buffer);
  s << buffer << ": " << mName->str () << " = ";
  mValue->print (s, 2);
}

void NExpr::Field::pr () const
{
  print (UtIO::cout ());
}
