// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file 
 *
 * This file implements the algorithm for generating a BURS-based pattern
 * matchers from a Bottom-Up Rewrite System (BURS) specification.
 *
 * The goal of this analysis is to produce a set of states and a transition
 * function. The states are represented at runtime by a single integer. The
 * transition function maps the operator of the tree node and the labels of its
 * subtrees to the state of this node. Of course, because leaves have no
 * subtrees the label of a leaf node is uniquely determined its operator. Thus
 * the transition function can be used to label a tree in a single bottom up
 * pass.
 *
 * Analysis is driven by a worklist of states. States are a mapping from non
 * terminals to costs. The state models a set of possible matches to those non
 * terminals and the costs of the non terminals. If a particular non terminal
 * is not matched in that state then it maps to infinite cost. The worklist is
 * initialised with a unique state for each leaf operator. On each iteration a
 * state is taken from the worklist. Then for each unary rule in the rewrite
 * system, a set of new states is constructed representing possible matches
 * with the state on the rhs of the unary rule. Then for each binary rules new
 * states are construct for the state matching in either operand. If this new
 * state contruction yields new unique states, then they are added to the
 * worklist. If the new states are identical to extant states then they are not
 * added. The algorithm terminates when the work list is empty.
 *
 * Interestingly enough, this algorithm used for analysis of a BURS is semi
 * decidable. That is, it is guarenteed to terminate if a fixed point solution
 * exists. However, if there is not solution then there is no way to infer that
 * and the algorithm does not terminate. Fortunately, any sane rewrite system
 * for code generation will terminate.
 *
 * This implementation is basically a translation from a BURS implementation
 * written in Ada95 that I wrote in 1994. That implementation was based on the
 * algorithms published in T.A. Proebsting, "Code generation techniques", 1992,
 * Ph.D. dissertation, University of Wisconsin.
 *
 * -michael
 */

#include <stdarg.h>
#include "util/UtIOStream.h"
#include "util/ArgProc.h"
#include "util/NExprMsgContext.h"
#include "util/SourceLocator.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/CodeStream.h"
#include "util/IndentedStream.h"
#include "util/UtSet.h"
#include "NExprSchema.h"
#include "Burs.h"

//! Rewrite system analysis
bool BURS::analyse ()
{
  if (mRewrites.empty ()) {
    INFO_ASSERT (false, "what rewrite system");
    return false;
  } else if (!mSchema.hasGoal (&mGoal)) {
    mSchema.Error ((*mRewrites.begin ())->getDecl (), "No goal symbol declared.");
    return false;
  }
  // assign indices to all the burs things
  if (!allocateIndices ()) {
    // Do this even when eDOBURS is not set because the same indices are also
    // for the iburg matcher.
    return false;
  }
  if (!(mFlags & eDOBURS)) {
    // We are not generating a BURS matcher so don't bother with analysis. 
    return true;
  }
  // Initialise the transitions store
  mTransitions = new Transitions (*this);
  // Initialise the state vector store
  mStates = new States (*this);
  // Initialise with states for all the leaf operators
  computeLeafStates ();
  if (isTraced ()) {
    UtIO::cout () << "Leaf states:\n";
    mStates->pr ();
  }
  // Now keep taking states from the worklist and compute transitions out of
  // that state until the worklist is empty.
  while (!mWorklist.empty ()) {
    // take a state from the worklist
    State *state = mWorklist.front ();
    mWorklist.pop_front ();
    // compute the transitions from the state
    for (UInt32 terminal = 0; terminal < mNTerminals; terminal++) {
      if (mTerminal [terminal].getArity () > 0) {
        // Compute the transitions out of nodes labeled by terminal for the
        // state. This may add new states to the worklist.
        computeTransitions (terminal, state);
      }
    }
  }
  if (isVerbose ()) {
    UtIO::cout () << "Rewrite system contains:\n";
    UtIO::cout () << "  " << mNTerminals << " terminals\n";
    UtIO::cout () << "  " << mNNonTerminals << " non terminals\n";
    UtIO::cout () << "  " << mNRules << " rules\n";
    UtIO::cout () << "  " << mStates->getNumStates () << " states\n";
    UtIO::cout () << "  " << mStates->getNumProtoStates () << " proto states\n";
    UtIO::cout () << "  " << mTransitions->getNumTransitions () << " transitions\n";
  }
  return true;
}

//! Allocate indices to terminals, non terminals and rules
bool BURS::allocateIndices ()
{
  bool status = true;
  UInt32 n;
  // Allocate indices to the codegen objects
  n = 0;
  // Number the non-terminals from zero.
  mNNonTerminals = mNT.size ();
  mNonTerminalMap = new NExprSchema::NonTerminal * [mNNonTerminals];
  NExprSchema::NonTerminal::Map::iterator nt;
  for (nt = mNT.begin (); nt != mNT.end (); ++nt) {
    nt->second->setIndex (n);
    mNonTerminalMap [n] = nt->second;
    n++;
  }
  // Number the terminals, continuing from the end of the non terminal range.
  n = 0;
  mNTerminals = mIR.size ();
  mTerminal = new Terminal [mNTerminals];
  NExprSchema::IROp::Map::iterator irops;
  for (irops = mIR.begin (); irops != mIR.end (); ++irops) {
    NExprSchema::IROp *irop = irops->second;
    irop->setIndex (n);
    mTerminal [n].bind (irop);
    NExprSchema::IROp::Parameters::const_iterator params;
    for (params = irop->beginformals (); params != irop->endformals (); ++params) {
      StringAtom *type = params->first;
      StringAtom *name = params->second;
      UtString errmsg;
      if (!mTerminal [n].addParameter (type, name, &errmsg)) {
        mSchema.Error (irop->getDecl (), errmsg.c_str ());
        status = false;
      }
    }
    n++;
  }
  // Walk the IRTrees in the IR transforms and validate the actual parameters
  // in the IRTree nodes with the formal parameters of the terminal.
  NExprSchema::IRTransformation::Map::const_iterator it;
  for (it = mTransforms.begin (); it != mTransforms.end (); it++) {
    status &= it->second->analyseActuals (*this);
  }
  // Construct the rule map. Map construction uses the indices allocated to
  // terminals and non-terminals thus this must occur after these indices have
  // been allocated.
  n = 0;
  mNRules = mRewrites.size ();
  mRules = new Rules (*this, mNRules);
  NExprSchema::Rewrite::List::iterator rule;
  for (rule = mRewrites.begin (); rule != mRewrites.end (); ++rule) {
    status &= mRules->append (*rule);
    n++;
  }
  mRules->sortRules ();
  // Now iterate across the rule array noting the number of chain rules and the
  // first rule and number of rules for each terminal operator. Sorting has
  // ordered the array so that all the chain rules are at the start of the
  // array and the rules matching the same terminal are contiguous.
  UInt32 terminal = 0;                  // terminal seen on previous iteration
  UInt32 first_rule = 0;                // first rule that matched that terminal
  UInt32 n_rules = 0;                   // number of rules seen matching that terminal
  for (n = 0; n < mNRules; n++) {
    Rule &rule = mRules->get (n);
    rule.getRewrite ()->setIndex (n);
    rule.setIndex (n);
    if (rule.isChain ()) {
      // it is a chain rule
      INFO_ASSERT (mNChainRules == n, "all chain rules should be at the head of this vector");
      mNChainRules++;
    } else if (n_rules == 0) {
      // this is the first terminal rule we have encountered
      terminal = rule.getRhs ();
      first_rule = n;
      n_rules = 1;
    } else if (terminal != rule.getRhs ()) {
      // this rule has a different terminal to the last one we saw so bind the
      // range of that terminal
      mTerminal [terminal].setRules (first_rule, n_rules);
      terminal = rule.getRhs ();
      first_rule = n;
      n_rules = 1;
    } else {
      // this rule has the same terminal as the last one
      n_rules++;
    }
  }
  if (n_rules > 0) {
    // set the range for the last terminal we saw
    mTerminal [terminal].setRules (first_rule, n_rules);
  }
  mRules->computeChainCosts ();
  if (isTraced ()) {
    // write the terminals, non-terminals and rules to standard output
    dumpSystem (UtIO::cout ());
  }
  return status;
}

//! Compute the chain cost matrix
/*! This is a matrix that is intialised so that M[a,b] = C if there is chain
 *  rule a <- b with cost C. The matrix is then closed under the rewrite
 *  system. This is later used to trim entries from states.
 */
void BURS::Rules::computeChainCosts ()
{
  mNChainRules = mBurs.mNChainRules;
  mNNonTerminals = mBurs.mNNonTerminals;
  mChain = new CostMatrix (mNNonTerminals, mNNonTerminals, eINFINITY);
  // for each chain rule a <- b [C] set the chain cost to C
  UInt32 n = 0;
  for (n = 0; n < mNChainRules; n++) {
    Rule &rule = mRule [n];
    INFO_ASSERT (rule.isChain (), "not a chain rule");
    // we can map lhs <- rhs with cost C
    chain (rule.getRhs (), rule.getLhs ()) = rule.getCost ();
  }
  // Now compute the closure of the matrix under the chain rules. There are
  // certainly more efficient ways to close this matrix, but this naive way
  // works, if our rule set grows enough that this takes too long, then that
  // would be the time to come up with a better rule.
  bool changed = false;
  do {
    for (n = 0; n < mNChainRules; n++) {
      Rule &rule = mRule [n];
      UInt32 lhs = rule.getLhs ();
      UInt32 rhs = rule.getRhs ();
      UInt32 cost = rule.getCost ();
      for (UInt32 i = 0; i < mNNonTerminals; i++) {
        UInt32 chain_cost = chain (i, rhs);
        if (chain_cost != eINFINITY) {
          // the rule maps lhs <- rhs with cost C and chain [i, rhs] is finite so consider
          // if cost + chain cost is better than chain [i, lhs]
          if (chain_cost + cost < chain (i, lhs)) {
            changed = true;
            chain (i, lhs) = chain_cost + cost;
          }
        }
      }
    }
  } while (changed);
}

//! Create the state for each leaf terminal
/*! Leaf state are induced by nullary terminal symbols. Because there are no
 *  inputs (that is tree nodes labeled with these symbols must be leaves) there
 *  will be a single unique state for each nullary symbol. This method
 *  constructs such a state for each leaf symbol. For each rule Nx <- t with
 *  cost Cx in the rewrite system for the nullary symbol Nx is added to the
 *  leaf state with cost Cx.
 */
void BURS::computeLeafStates ()
{
  for (UInt32 op = 0; op < mNTerminals; op++) {
    Terminal &terminal = mTerminal [op];
    if (terminal.getArity () == 0) {
      State *state = mStates->create (op);
      const UInt32 first_rule = terminal.getFirstRule ();
      const UInt32 n_rules = terminal.getNRules ();
      for (UInt32 n = first_rule; n < first_rule + n_rules; n++) {
        Rule &rule = getRule (n);
        if (rule.getCost () < state->getCost (rule.getLhs ())) {
          state->set (rule.getLhs (), n, rule.getCost ());
        }
      }
      state->normalise ();
      state->close ();
      state = mStates->intern (state);
      UInt16 child [2] = {eNOT_A_STATE, eNOT_A_STATE};
      if (isTraced ()) {
        UtIO::cout () << "leaf state "; state->pr ();
      }
      mTransitions->add (op, child, state->getIndex ());
      addToWorklist (state);
    }
  }
}

//! Construct a proto state from a state and a non terminal
/*! The proto state consists of a mapping from non terminals to costs. The
 *  protostate constructed by projecting a state through a dimension of a non
 *  terminal constains a entry for each non terminal nt such that nt is in the
 *  state and there is a rule for the terminal with nt in position dim. The
 *  cost of nt in the proto state is the cost of nt in the state plus the cost
 *  of the rule.
 */
BURS::ProtoState *BURS::States::project (State *state, const Terminal *op, const UInt32 dim)
{
  ProtoState *proto = new ProtoState (mBurs, mNextProto);
  for (UInt32 nt = 0; nt < mBurs.mNNonTerminals; nt++) {
    if (state->getCost (nt) == eINFINITY) {
      // this state will not match as nt
    } else {
      // look for a rule with nt in position dim
      bool found = false;
      const UInt32 r0 = op->getFirstRule ();
      const UInt32 r1 = r0 + op->getNRules ();
      for (UInt32 r = r0; !found && r < r1; r++) {
        Rule &rule = mBurs.getRule (r);
        if (rule.getChild (dim) == nt) {
          // OK, there is rule with nt in position dim so note the cost of the
          // partial match in the protostate. We can stop at the first such
          // rule because the protostate just projects the non terminal costs
          // from the state through possibly matching rules. We are not
          // interested in the actual cost of the rule at this stage. 
          proto->setCost (nt, state->getCost (nt));
          found = true;
        }
      }
    }
  }
  proto->normalise ();
  proto = intern (proto);
  return proto;
}

//! Compute the transitions out of a terminal for a set of input proto states
/*! This method looks for transitions out of terminal for a set of inputs. A
 *  new state is created to model transitions out of the terminal.
 * 
 *  If the terminal, t, is unary, then if there is a rule X <- t(A) with cost
 *  C1 and A is in the proto state with cost C2, then X is added to the new
 *  output state with cost C1+C2, and the rule used is recorded in the state.
 *
 *  If the terminal, t, is binary and if there is a rule X <- t(A,B) with cost
 *  C1 and A is in the inputs[0] proto state with cost C2 and B is in the
 *  inputs [1] proto state with cost C3 then X is added to the new output state
 *  with cost C1+C2+C3 and the rule used is recorded in the state.
 *
 *  Once all rules have been considered, the state is normalised and checked to
 *  see if it is equal to an existing state. If not, it is interned and added
 *  to the worklist.
 */
void BURS::computeTransition (Terminal *terminal, ProtoState *inputs [2])
{
  if (isTraced ()) {
    UtIO::cout () << "  compute transition for " << terminal->getName () << " for ";
    if (inputs [0] != NULL) {
      UtIO::cout () << " (" << inputs [0]->getIndex ();
    }
    if (inputs [1] != NULL) {
      UtIO::cout () << ", " << inputs [1]->getIndex ();
    }
    UtIO::cout () << ")\n";
  }
  const UInt32 first_rule = terminal->getFirstRule ();
  const UInt32 n_rules = terminal->getNRules ();
  State *newstate = mStates->create (terminal->getIndex ());
  for (UInt32 i = first_rule; i < first_rule + n_rules; i++) {
    Rule &rule = mRules->get (i);
    UInt16 cost = eINFINITY;
    switch (rule.getArity ()) {
    case 1:
      INFO_ASSERT (inputs [0] != NULL && inputs [1] == NULL, "blah");
      if (inputs [0]->getCost (rule.getChild (0)) != eINFINITY) {
        // Found a matching rule
        cost = inputs [0]->getCost (rule.getChild (0)) + rule.getCost ();
      }
      break;
    case 2:
      INFO_ASSERT (inputs [0] != NULL && inputs [1] != NULL, "blah");
      if (inputs [0]->getCost (rule.getChild (0)) != eINFINITY
        && inputs [1]->getCost (rule.getChild (1)) != eINFINITY) {
        // Found a matching rule
        cost = inputs [0]->getCost (rule.getChild (0)) + inputs [1]->getCost (rule.getChild (1)) 
          + rule.getCost ();
      }
      break;
    default:
      // There can only be new transitions for unary and binary terminals
      INFO_ASSERT (false, "blah");
      break;
    }
    if (cost < newstate->getCost (rule.getLhs ())) {
      // This rule matched and the cost is less than the cost of the LHS non
      // terminal in the new state, so this match is pushed into the state.
      if (isTraced ()) {
        UtIO::cout () << "  apply rule " << i << ": ";
        rule.pr ();
      }
      newstate->set (rule.getLhs (), i, cost);
    }
  }
  // Trim the state to removed non terminals that will never be chosed
  trimState (newstate);
  // Normalise the state to the costs start at zero
  newstate->normalise ();
  // Close the state under the chain rules.
  newstate->close ();
  // Checking that the state is unique after trimming, normalisation and
  // closure reduces the total number of unique states. 
  if (mStates->isState (newstate)) {
    // identical to an existing state
    newstate = mStates->intern (newstate);
    if (isTraced ()) {
      UtIO::cout () << "  "; newstate->pr();
    }
  } else {
    // this is a really a new state so intern and add to the worklist
    newstate = mStates->intern (newstate);
    if (isTraced ()) {
      UtIO::cout () << "  "; newstate->pr();
    }
    addToWorklist (newstate);
  }
  // Record the transitions from proto states at terminal to
  // newstate. Interning may have replaced newstate with an existing state to
  // the transition is not recorded until after the possible intern.
  UInt16 protostates [2];
  protostates [0] = inputs [0] != NULL ? inputs [0]->getIndex () : (UInt16) eNOT_A_STATE;
  protostates [1] = inputs [1] != NULL ? inputs [1]->getIndex () : (UInt16) eNOT_A_STATE;
  mTransitions->add (terminal->getIndex (), protostates, newstate->getIndex ());
}

//! Compute the transitions out of a state for an operator
void BURS::computeTransitions (const UInt32 op, State *state)
{
  Terminal *opdescr = &mTerminal [op];
  for (UInt32 dim = 0; dim < opdescr->getArity (); dim++) {
    // For each dimension of the terminal, project matches at the dimension
    // into a proto state and then consider all position transitions from that
    // proto state.
    INFO_ASSERT (dim < 2, "arity blowout");
    ProtoState *proto = mStates->project (state, opdescr, dim);
    opdescr->bindState (dim, state->getIndex (), proto->getIndex ());
    if (isTraced ()) {
      UtIO::cout () << "compute transitions for " << terminalName (op) << " from "
                    << state->getIndex () << " at " << dim << ": ";
      state->pr ();
      proto->print (UtIO::cout (), 2);
      UtIO::cout () << "  "; opdescr->pr ();
    }
    switch (opdescr->getArity ()) {
    case 0:
      INFO_ASSERT (opdescr->getArity () == 0, "blah");
      break;
    case 1:
      // look for states reached from op (state);
      ProtoState *inputs [2];
      inputs [0] = proto;
      inputs [1] = NULL;
      computeTransition (opdescr, inputs);
      break;
    case 2:
      if (dim == 0) {
        // compute transitions for op (state, otherstate) for all possible otherstates
        ProtoState *inputs [2];
        inputs [0] = proto;
        BitMap::loop loop = opdescr->loopProtoStates (1);
        while (!loop.atEnd ()) {
          inputs [1] = mStates->getProto (*loop);
          computeTransition (opdescr, inputs);
          ++loop;
        }
      } else if (dim == 1) {
        // compute transitions for op (otherstate, state) for all possible otherstates
        ProtoState *inputs [2];
        inputs [1] = proto;
        BitMap::loop loop = opdescr->loopProtoStates (0);
        while (!loop.atEnd ()) {
          inputs [0] = mStates->getProto (*loop);
          computeTransition (opdescr, inputs);
          ++loop;
        }
      } else {
        // this should be exhaustive
        INFO_ASSERT (dim < 2, "blah");
      }
      break;
    default:
      INFO_ASSERT (opdescr->getArity () < 2, "blah");
      break;
    }
  }
}

//! Trim a state
void BURS::trimState (State *state)
{
  // Currently, only chain trimming is implemented...
  chainTrimState (state);
}

//! Chain trim a state
/*! Chain trimming looks at the non terminals in a state and the chain *
 *  rules. If there is a chain rule L <- R with cost C0 and the finite cost of
 *  L in * the state is C1 and the finite cost of R is C2. Then if C0 + C2 is
 *  less * than C1 then L can be removed because matching to R and then
 *  rewriting with * the chain will be cheaper than matching to L.
 */
void BURS::chainTrimState (State *state)
{
  UInt32 m, n;
  for (m = 0; m < mNNonTerminals; m++) {
    if (state->getCost (m) != eINFINITY) {
      // The state will match to non terminal m so see if there is a less cost
      // way to m via the chain rules.
      for (n = 0; n < mNNonTerminals; n++) {
        if (m != n && state->getCost (n) != eINFINITY) {
          UInt16 chain_cost = mRules->chain (m, n);
          if (state->getCost (n) >= state->getCost (m) + chain_cost) {
            // It is cheaper to reduce to m and then apply chain rules to
            // rewrite to n than it is to have the child rewrite to n. So,
            // remove n from the state. When the state is closed the chain rule
            // transitions will restore n to the state with a smaller cost.
            state->removeNonTerminal (n);
            if (isTraced ()) {
              UtIO::cout () << "chain trimmed " << nonTerminalName (n) << " from state\n";
            }
          }
        }
      }
    }
  }
}

//! Intern a new state into the state obarray
/*! If the new state is identical with an existing state then the new state is
 *  deleted and the existing state is returned. If not, then the new state is
 *  added to the obarray and the new state itself is returned.
 */
BURS::State *BURS::States::intern (State *state)
{
  State::Obarray::iterator found;
  if ((found = mStateObarray.find (state)) == mStateObarray.end ()) {
    mStateObarray.insert (State::Obarray::value_type (state, state));
    mStateTable.insert (UtMap <UInt16, State *>::value_type (state->getIndex (), state));
    INFO_ASSERT (state->getIndex () == mNextState, "intern sequence error");
    INFO_ASSERT (mNextState < (1<<16), "out of states");
    mNextState++;
    return state;
  } else {
    delete state;
    return found->second;
  }
}

//! Intern a new proto state into the proto state obarray
BURS::ProtoState *BURS::States::intern (ProtoState *proto)
{
  ProtoState::Obarray::iterator found;
  if ((found = mProtoObarray.find (proto)) == mProtoObarray.end ()) {
    mProtoObarray.insert (ProtoState::Obarray::value_type (proto, proto));
    mProtoTable.insert (UtMap <UInt16, ProtoState *>::value_type (proto->getIndex (), proto));
    INFO_ASSERT (proto->getIndex () == mNextProto, "intern sequence error");
    INFO_ASSERT (mNextProto < (1<<16), "out of states");
    mNextProto++;
    return proto;
  } else {
    delete proto;
    return found->second;
  }
}

//! \return iff a state is identical with an existing state
/*! This method looks for the state in the obarray but does not install new
 *  state.
 */

bool BURS::States::isState (State *state)
{
  State::Obarray::iterator found;
  if ((found = mStateObarray.find (state)) == mStateObarray.end ()) {
    return false;
  } else {
    return true;
  }
}

//! Normalise the costs of the state
void BURS::State::normalise ()
{
  UInt16 min = eINFINITY;
  UInt32 n;
  for (n = 0; n < mSize; n++) {
    if (mItem [n].mCost < min) {
      min = mItem [n].mCost;
    }
  }
  if (min == eINFINITY) {
    return;
  }
  for (n = 0; n < mSize; n++) {
    if (mItem [n].mCost != eINFINITY) {
      mItem [n].mCost -= min;
      if (mItem [n].mCost > eTHRESHOLD) {
        pr ();
        mBurs.mSchema.Error ("State %d is apparently divergent.", mIndex);
        exit (1);
      }
    }
  }
}

//! Normalise the costs of a proto state
void BURS::ProtoState::normalise ()
{
  UInt16 min = eINFINITY;
  UInt32 n;
  for (n = 0; n < mSize; n++) {
    if (mCost [n] < min) {
      min = mCost [n];
    }
  }
  if (min == eINFINITY) {
    return;
  }
  for (n = 0; n < mSize; n++) {
    if (mCost [n] != eINFINITY) {
      mCost [n] -= min;
      if (mCost [n] > eTHRESHOLD) {
        pr ();
        mBurs.mSchema.Error ("ProtoState is apparently divergent.");
        exit (1);
      }
    }
  }
}

//! Close a state under the chain rules.
/*! A state is closed by successively applying chain rules until no new non
 *  terminals are added to the state. For example, if N is a member of the
 *  state with cost C1 and there is a chain rule M <- N with cost C2 then M may
 *  be added to the state with cost C1+C2, provided M is either absent from the
 *  state or the cost of M is greater than C1+C2.
 */
void BURS::State::close ()
{
  bool changed = true;
  while (changed) {
    changed = false;
    for (UInt32 n = 0; n < mBurs.mNChainRules; n++) {
      Rule &rule = mBurs.getRule (n);
      INFO_ASSERT (rule.isChain (), "closing with non-chain rule");
      UInt32 lhs = rule.getLhs ();
      UInt32 rhs = rule.getRhs ();
      if (mItem [rhs].mCost == eINFINITY) {
        // the rhs cannot be reached
      } else if (rule.getCost () + mItem [rhs].mCost < mItem [lhs].mCost ) {
        // rhs is in the state and there is a chain rule lhs<-rhs so add lhs to
        // the state.
        changed = true;
        mItem [rule.getLhs ()].mCost = rule.getCost () + mItem [rhs].mCost;
        mItem [rule.getLhs ()].mRule = n;
      }
    }
  }
}
