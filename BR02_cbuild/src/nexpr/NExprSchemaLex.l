%{
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include "util/AtomicCache.h"
#include "util/SourceLocator.h"
#include "util/UtIOStream.h"
#include "NExprSchema.h"
#include "NExprSchemaYacc.h"

static void NExprSchema_error (const char *fmt, ...);

extern NExprSchema *gSchema;
extern const char *gNExprLexFilename;

#if __GNUC__
#define UNUSED __attribute__ ((unused));
#else
#define UNUSED
#endif

static void NExprSchema_error (const char *fmt, ...) UNUSED;
static void NExprSchema_error (const SourceLocator, const char *fmt, ...) UNUSED;
static void NExprSchema_error (const int, const char *fmt, ...) UNUSED;

extern SourceLocator yyloc;

#define ENTER(_state_) { \
  if (NExprSchema__flex_debug) { \
    UtIO::cerr () << "--enter " << #_state_ << "\n"; \
  } \
  BEGIN (_state_); \
}

#define RETURN(_x_) { \
  gSchema->create (NExprSchema_lineno, &NExprSchema_lloc); \
  if (!NExprSchema__flex_debug) { \
  } else if (_x_ == _IDENT_) { \
    UtIO::cerr () << "--returning " << #_x_ << ": " << NExprSchema_lval.mSymbol->str () << "\n"; \
  } else if (_x_ == _STRING_) { \
    UtIO::cerr () << "--returning " << #_x_ << ": " << NExprSchema_lval.mSymbol->str () << "\n"; \
  } else if (_x_ == _INTEGER_) { \
    UtIO::cerr () << "--returning " << #_x_ << ": " << NExprSchema_lval.mInteger << "\n"; \
  } else { \
    UtIO::cerr () << "--returning " << #_x_ << "\n"; \
  } \
  return _x_; \
}

%}

/* To avoid isatty problems on Windows. */
%option never-interactive
%option yylineno
%option noyywrap
%option nounput
%option nodefault
%option debug

%x COMMENT0 COMMENT1
%x STRING0

%%

  /* C++ style comments */
"//"               { ENTER (COMMENT0); }
<COMMENT0>.        { /* consume the comment */ }
<COMMENT0>\n       { ENTER (INITIAL); }

  /* C style comments */
"/*"               { ENTER (COMMENT1); }
<COMMENT1>"*/"     { ENTER (INITIAL);  }
<COMMENT1>.        { /* consume the comment */ }
<COMMENT1>\n       { /* consume the comment */ }

  /* keywords */
"abstract"     { RETURN (_ABSTRACT_); }
"alias"        { RETURN (_ALIAS_); }
"api"          { RETURN (_API_); }
"build"        { RETURN (_BUILD_); }
"case"         { RETURN (_CASE_); }
"choice"       { RETURN (_CHOICE_); }
"class"        { RETURN (_CLASS_); }
"debug"        { RETURN (_DEBUG_); }
"enum"         { RETURN (_ENUM_); }
"goal"         { RETURN (_GOAL_); }
"include"      { RETURN (_INCLUDE_); }
"let"          { RETURN (_LET_); }
"mask"         { RETURN (_MASK_); }
"nested"       { RETURN (_NESTED_); }
"no"           { RETURN (_NO_); }
"others"       { RETURN (_OTHERS_); }
"post"         { RETURN (_POST_); }
"pre"          { RETURN (_PRE_); }
"print"        { RETURN (_PRINT_); }
"set"          { RETURN (_SET_); }
"switch"       { RETURN (_SWITCH_); }
"token"        { RETURN (_TOKEN_); }
"top"          { RETURN (_TOP_); }
"type"         { RETURN (_TYPE_); }
"vector"       { RETURN (_VECTOR_); }
"when"         { RETURN (_WHEN_); }
"write"        { RETURN (_WRITE_); }

  /* integers */
[0-9][0-9]*    { NExprSchema_lval.mInteger = atoi (yytext); RETURN (_INTEGER_); }

  /* hexadecimal integers */
0x[0-9a-fA-F][0-9a-fA-F]* {
                 NExprSchema_lval.mInteger = strtol (yytext, NULL, 0); RETURN (_INTEGER_); }

  /* identifiers */
[a-zA-Z][a-zA-Z0-9_]* { NExprSchema_lval.mSymbol = gSchema->intern (yytext); RETURN (_IDENT_); }

  /* Strings */
\"               { ENTER (STRING0); }
<STRING0>([^\n"]|\\\")* { NExprSchema_lval.mSymbol = gSchema->intern (yytext); RETURN (_STRING_); }
<STRING0>\"      { ENTER (INITIAL); }
<STRING0>\n      { NExprSchema_error ("String extends beyond end of line."); ENTER (INITIAL); }

  /* Whitespace */
[ \t]      { /* consume whitespace */ }
\n         { /* consume newlines */ }

  /* operators */
"::"       { RETURN (_COLON_COLON_); }
":"        { RETURN (':'); }
"{"        { RETURN ('{'); }
"}"        { RETURN ('}'); }
";"        { RETURN (';'); }
"*"        { RETURN ('*'); }
"["        { RETURN ('['); }
"]"        { RETURN (']'); }
"("        { RETURN ('('); }
")"        { RETURN (')'); }
","        { RETURN (','); }
"="        { RETURN ('='); }
"<-"       { RETURN (_LEFT_); }
"=>"       { RETURN (_RIGHT_); }
"!"        { RETURN ('!'); }
"$"        { RETURN ('$'); }
"."        { RETURN ('.'); }

  /* anything else is bogus */
. { 
    char c = *yytext;
    if (isprint (c)) {
       NExprSchema_error ("Unrecognised character: '%c'", c);
    } else {
       NExprSchema_error ("Unrecognised character: \\x%02x", (int) c);
    }
  }

%%

static void NExprSchema_error (const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  gSchema->YYError (NExprSchema_lineno, fmt, ap);
  va_end (ap);  
}

static void NExprSchema_error (const int lineno, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  gSchema->YYError (lineno, fmt, ap);
  va_end (ap);  
}

static void NExprSchema_error (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  gSchema->YYError (loc, fmt, ap);
  va_end (ap);  
}

