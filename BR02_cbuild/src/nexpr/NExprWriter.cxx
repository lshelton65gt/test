// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/AtomicCache.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtMap.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/CbuildMsgContext.h"
#include "nexpr/NExpr.h"

//! \class NExpr::LabelMap
/*! Label maps map pointers to integer labels. It is used when writing
 *  references in N-expressions.
 */
class NExpr::LabelMap : public UtHashMap <void *, UInt32> {
public:

  LabelMap () : UtHashMap <void *, UInt32> (), mNextLabel (1) {}
  virtual ~LabelMap () {}

  //! \return the label allocated to an object
  /*! If no label has been allocated the allocate the next available object and
   *  put it into the talble.
   */
  UInt32 intern (const void *object)
  {
    const_iterator found;
    if (object == NULL) {
      return 0;                         // label 0 is always NULL
    } else if ((found = find ((void *) object)) != end ()) {
      return found->second;             // object is already in the table
    } else {
      // assign a label for the object and insert it into the table
      UInt32 label = mNextLabel++;
      insert (value_type ((void *) object, label));
      return label;
    }
  }

private:

  UInt32 mNextLabel;                    //!< the next label to allocate
};

//! \class NExpr::Writer
/*! ctor that creates a UtOFStream and writer the N-expressions to that stream.
 */
NExpr::Writer::Writer (const char *filename, const UInt32 flags) :
  mFlags (cOWNS_STREAM|flags), mDepth (0),
  mIsStartOfLine (true), mNeedsNewline (false), mNeedsSpace (false), mWidth (0),
  mOut (new UtOFStream (filename)), mFilename (filename)
{
  mLabels = new LabelMap;               // create the label map
  const char *value;
  if ((value = getenv ("CARBON_DEBUG_NEXPR")) && *value == '1') {
    mFlags |= cDEBUG;
  }
}

/*! ctor that writes N-expressions to an existing UtOStream.
 */
NExpr::Writer::Writer (UtOStream &out, const UInt32 flags) :
  mFlags (flags), mDepth (0),
  mIsStartOfLine (true), mNeedsNewline (false), mNeedsSpace (false), mWidth (0),
  mOut (&out), mFilename ("<anon>")
{
  mLabels = new LabelMap;               // create the label map
}

/*virtual*/ NExpr::Writer::~Writer ()
{
  if (mNeedsNewline && mOut != NULL) {
    mOut->write ("\n", 1);
  }
  if (mFlags & cOWNS_STREAM) {
    delete mOut;
  }
  delete mLabels;
}

//! Add an object to the writer object table and return the label.
UInt32 NExpr::Writer::intern (const void *object)
{
  return mLabels->intern (object);
}

//! Intern an object and write it as %label
bool NExpr::Writer::writeLabel (const void *object)
{
  (*this) << "@" << mLabels->intern (object);
  return true;
}

//! Output horizontal or vertical glue
/*! Glue is white space or newlines. This method outputs a newline if vertical
 *  glue is needed or a space if horizontal glue is needed. However, glue is
 *  never output before a closing parenthesis.
 */
void NExpr::Writer::doGlue (const char next_char)
{
  if (next_char == ')') {
    // do not put spaces or newlines before a closing parenthesis
    mNeedsSpace = false;
  } else if (mNeedsNewline) {
    mOut->write ("\n", 1);
    static char spaces [33] = "                                ";
    mWidth = 0;
    while (mWidth < mDepth) {
      UInt32 residue = mDepth - mWidth;
      if (residue <= 32) {
        mOut->write (spaces, residue);
        mWidth += residue;
      } else {
        mOut->write (spaces, 32);
        mWidth += 32;
      }
    }
    mNeedsNewline = false;
    mNeedsSpace = false;
  } else if (mNeedsSpace) {
    mOut->write (" ", 1);
    mNeedsSpace = false;
    mWidth++;
  }
}

//! Primitive outut method
void NExpr::Writer::output (const char *s)
{
  int n = 0;
  while (s [n] != '\0') {
    switch (s [n]) {
    case '\0':
      INFO_ASSERT (s [n] != '\0', "run off the end");
      break;
    case '\t':
      mNeedsSpace = true;
      n++;
      break;
    case '\n':
      mNeedsNewline = true;
      n++;
      break;
    default:
      {
        bool end_of_text = false;
        int m = n;
        do {
          switch (s [n]) {
          case '\0':
            end_of_text = true;
            break;
          case '\\':
            INFO_ASSERT (s [n+1] != '\0', "trailing \\");
            n++;
            break;
          case '\t':
            end_of_text = true;
            break;
          case '\n':
            end_of_text = true;
            break;
          default:
            n++;
            break;
          }
        } while (!end_of_text);
        INFO_ASSERT (n > m, "bogus code fragment");
        if (mWidth + n - m > cWIDTH) {
          mNeedsNewline = true;
        }
        doGlue (s [m]);
        mOut->write (s + m, n - m);
        mWidth += n - m;
      }
    }
  }
}

//! Write a string to output.
NExpr::Writer &NExpr::Writer::operator << (const char *s)
{
  output (s);
  return *this;
}

//! Write a single character to output
NExpr::Writer &NExpr::Writer::operator << (const char c)
{
  char s [2];
  s [0] = c;
  s [1] = '\0';
  output (s);
  return *this;
}

//! Write a Carbon string to output.
NExpr::Writer &NExpr::Writer::operator << (const UtString &s)
{
  output (s.c_str ());
  return *this;
}

//! Write an integer to output.
NExpr::Writer &NExpr::Writer::operator << (const UInt32 n)
{
  // The integer needs to be converted into a string and pushed through the
  // output method so that indentation and column counting occurs.
  char buffer [64];
  snprintf (buffer, sizeof (buffer), "%d", n);
  output (buffer);
  return *this;
}

//! Write all the nets in a design
bool NExpr::Writer::writeNets (NUDesign *design, UtString *errmsg)
{
  NUDesign::ModuleLoop loop = design->loopAllModules ();
  while (!loop.atEnd ()) {
    if (!writeNets (*loop, errmsg)) {
      return false;
    }
    ++loop;
  }
  return true;
}

#if NEXPR_ENABLE

//! Write all the nets in a module
bool NExpr::Writer::writeNets (NUModule *module, UtString *errmsg)
{
  NUModule::NetLoop loop = module->loopNets ();
  while (!loop.atEnd ()) {
    NUNet *net = *loop;
    if (!net->write (*this, true, errmsg)) {
      return false;
    }
    ++loop;
  }
  NUNamedDeclarationScopeLoop scope_loop = module->loopDeclarationScopes();
  while (!scope_loop.atEnd ()) {
    NUNetLoop net_loop = (*scope_loop)->loopLocals();
    while (!net_loop.atEnd ()) {
      NUNet *net = *net_loop;
      if (!net->write (*this, true, errmsg)) {
        return false;
      }
      ++net_loop;
    }
    ++scope_loop;
  }
  NUBlockLoop block_loop = module->loopBlocks();
  while (!block_loop.atEnd ()) {
    NUNetLoop net_loop = (*block_loop)->loopLocals();
    while(!net_loop.atEnd ()) {
      NUNet *net = *net_loop;
      if (!net->write (*this, true, errmsg)) {
        return false;
      }
      ++net_loop;
    }
    ++block_loop;
  }
  return true;
}

#else

//! Do not write any nets from any modules
bool NExpr::Writer::writeNets (NUModule *, UtString *errmsg)
{
  *errmsg << "N-expressions are disabled.";
  return false;
}

#endif

//! \class NExpr::Writer::Object
/*! An instance of Object is instantiated on entry to each of the
 *  schema-compiler generated write methods. The constructor output opening
 *  parens and the type name, if required. The destructor closes any open
 *  parens.
 */
NExpr::Writer::Object::Object (Writer &dump, const char *tag, const void *object, 
  bool isHierarchical, bool isTop) :
  mDump (dump), mParenthesised (false)
{
  if (!isTop) {
    // writing from a superclass write method
  } else if (isHierarchical) {
    // this is a hierarchical object so wrap it in parenthesis
    mDump << "(" << tag << "\t";
    dump.mDepth++;
    mParenthesised = true;
  } else {
    // this is a non-hierarchical object so a label must be allocated then the
    // object written in parenthesis with the label.
    mDump << "(";
    mDump.writeLabel (object);
    mDump << " " << tag << " ";
    dump.mDepth++;
    mParenthesised = true;
  }
}

NExpr::Writer::Object::~Object () 
{
  if (mParenthesised) {
    mDump << ")";
    mDump.mDepth--;
    if (mDump.mDepth == 0) {
      // force
      mDump << "\n"; mDump.doGlue ('\0');
      mDump << "\n"; mDump.doGlue ('\0');
    }
  }
}

