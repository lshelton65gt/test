// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/UtIOStream.h"
#include "util/ArgProc.h"
#include "util/NExprMsgContext.h"
#include "util/SourceLocator.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/CodeStream.h"
#include "util/UtSet.h"
#include "NExprSchema.h"
#include "Burs.h"

//! \class NExprSchemaContext
/*! NExprSchemaContext handles the command line processing for the N-expression
 *  schema compiler.
 *
 *  \note this is subclasses from FileCollector so that the ArgProc command
 *  line parser calls back with the filename arguments.
 */

static const char *scYYDebug = "-yydebug";
static const char *scLLDebug = "-lldebug";
static const char *scVerbose = "-v";
static const char *scQuiet   = "-q";
static const char *scOutputDir = "-o";
static const char *scBurs = "-burs";
static const char *scCGDir = "-cg";
static const char *scIburg = "-iburg";
static const char *scTraceBurs = "-traceBurs";
static const char *scNoLine = "-noline";

class NExprSchemaContext : private FileCollector {
public:

  //! ctor for class NExprSchemaContext
  NExprSchemaContext (MsgContext *msg_context, AtomicCache *symbols, 
    SourceLocatorFactory *sourcelocators) : 
    mMsgContext (msg_context), mSymbols (symbols), mSourceLocators (sourcelocators),
    mInputFilenames ()
  { configureArgs (); }

  //! Process the command line arguments
  bool processArgs (int *argc, char **argv)
  {
    mArgs.accessHiddenOptions ();
    mArgs.allowUsageAll ();
    // run the command-line parser
    UtString errMsg;
    int numOptions;
    if (mArgs.parseCommandLine (argc, argv, &numOptions, &errMsg, this) != ArgProc::eParsed) {
      mMsgContext->NXSchemaCmdLineError (errMsg.c_str ());
      return false;
    }
    // check that at least one input file was specified
    if (mInputFilenames.empty ()) {
      mMsgContext->NXSchemaCmdLineError ("No input files specified.");
      return false;
    }
    // grab the output filename
    const char *value;
    if (mArgs.getStrValue (scOutputDir, &value) != ArgProc::eKnown) {
      mMsgContext->NXSchemaCmdLineError ("No output file specified.");
      return false;
    } else {
      mOutputDirname = value;
    }
    if (mArgs.getStrValue (scCGDir, &value) == ArgProc::eKnown) {
      mCGDirname = value;
    }
    NExprSchema_debug = mArgs.getBoolValue (scYYDebug) ? 1 : 0;
    NExprSchema__flex_debug = mArgs.getBoolValue (scLLDebug) ? 1 : 0;
    UtString errmsg;
    // grab arguments
    if (mArgs.getBoolValue ("-h")) {
      printUsage ();
      exit(0);
    }
    return true;
  }

  //! \return the ArgProc
  const ArgProc &getArgs () const { return mArgs; }

  //! \return an iterator over the list of input files
  UtList <UtString>::iterator inputsbegin () { return mInputFilenames.begin (); }
  UtList <UtString>::iterator inputsend () { return mInputFilenames.end (); }

  //! \return the directory for the NExpr output
  const char *getOutputDirname () const { return mOutputDirname.c_str (); }

  //! \return the directory for the codegen output
  const char *getCGDirname () const { return mCGDirname.empty () ? NULL : mCGDirname.c_str (); }

  //! \return iff -quiet was specified
  bool isQuiet () const { return mArgs.getBoolValue (scQuiet); }

  //! \return iff -v was specified
  bool isVerbose () const { return mArgs.getBoolValue (scVerbose); }

  //! \return iff -noline
  bool noLine () const { return mArgs.getBoolValue (scNoLine); }

private:

  MsgContext *mMsgContext;              //!< whining stream
  ArgProc mArgs;                        //!< argument vector handler
  AtomicCache *mSymbols;                //!< obarrary of symbols
  SourceLocatorFactory *mSourceLocators; //!< our fabulously complex location stuff
  UtList <UtString> mInputFilenames;
  UtString mOutputDirname;              //!< directory for writing NExpr files
  UtString mCGDirname;                  //!< directory for writing CG files

  //! Implement the FileCollector::scanArgument method
  /*! It does nothing here...
   */
  virtual bool scanArgument (const char *, const char *) { return false; }

  //! Implement the FileCollector::addArgument method
  /*! It gathers up the list of input files here...
   */
  virtual void addArgument (const char *arg)
  {
    mInputFilenames.push_back (arg);
  }

  //! Configure the argument processor
  void configureArgs ()
  {
    mArgs.setDescription ("NExpr writer/reader writer", "nexpr",
      "Translates a schema into Nucleus writer/reader methods.");
    mArgs.addBool (scYYDebug, "Turn on debugging of the bison parser", false, 1);
    mArgs.addBool (scLLDebug, "Turn on debugging of the flex scanner", false, 1);
    mArgs.addBool (scQuiet, "Supress warnings and information messages", false, 1);
    mArgs.addOutputFile (scOutputDir, "Directory for the NExpr output files", NULL, true,
      ArgProc::eDirExist, false, 1);
    mArgs.addOutputFile (scCGDir, "Directory for the code generator output files", NULL, true,
      ArgProc::eDirExist, false, 1);
    mArgs.addBool (scBurs, "Emit an optimised BURS code generator", false, 1);
    mArgs.addBool (scIburg, "Emit an Iburg-based code generator", false, 1);
    mArgs.addBool (scTraceBurs, "Trace BURS analysis", false, 1);
    mArgs.addBool (scNoLine, "Do not emit #line directives in generated code", false, 1);
    mArgs.addBool (scVerbose, "Verbose output", false, 1);
    mArgs.addBool ("-h", "Print the documentation for annotation store reader options and exit.", 
      false, 1);
    mArgs.addSynonym ("-h", "-help", false);
  }

  //! Print annotation store reader usage instructions
  void printUsage ()
  {
    UtString usage;
    mArgs.getUsageVerbose(&usage, false);
    UtIO::cout() << usage << UtIO::endl;
  }

};

//! Entry point
int main (int argc, char **argv)
{
  SourceLocatorFactory sourcelocations;
  AtomicCache symbols;
  MsgStreamIO msg_stream (stdout, true);
  MsgContext msg_context;
  msg_context.addReportStream (&msg_stream);
  NExprSchemaContext context (&msg_context, &symbols, &sourcelocations);
  if (!context.processArgs (&argc, argv)) {
    return 1;
  }
  NExprSchema schema (&msg_context, &symbols, &sourcelocations, NExprSchema::options (context.getArgs ()));
  // read the schema
  UtList <UtString>::iterator it;
  for (it = context.inputsbegin (); it != context.inputsend (); it++) {
    if (!schema.parse ((*it).c_str ())) {
      return 1;
    }
  }
  // sanity check the schema
  if (!schema.check ()) {
    return 1;
  }
  // assign indices to all the schema objects
  schema.allocateIndices ();
  // write the NExr writer/reader
  if (!schema.write (context.getOutputDirname ())) {
    return 1;                           // failed
  }
  BURS burs (schema, BURS::options (context.getArgs ()));
  if (burs.isEmpty ()) {
    // no rewrite system specified so not much point generating cg.
  } else if (!burs.analyse ()) {
    return 1;                           // BURS analysis failed
  } else if (context.getCGDirname () == NULL) {
    // do not write any code generator output
  } else if (burs.doIburg () && !burs.writeIburgCG (context.getCGDirname ())) {
    return false;
  } else if (!burs.writeLIR (context.getCGDirname ())) {
    return false;
  } else if (burs.doBurs () && !burs.writeBursCG (context.getCGDirname ())) {
    return 1;                           // failed
  }
  return 0;
}

//! Construct an options vector from command line arguments
/*static*/ UInt32 BURS::options (const ArgProc &args)
{
  // Do the code generator
  return (args.getBoolValue (scBurs) ? eDOBURS : 0)
    | (args.getBoolValue (scIburg) ? eDOIBURG : 0)
    | (args.getBoolValue (scVerbose) ? eVERBOSE : 0);
}

//! Construct an options bit mask from the command line
/*static*/ UInt32 NExprSchema::options (const ArgProc &args)
{
  return (args.getBoolValue (scQuiet) ? eQUIET : 0)
    | (args.getBoolValue (scNoLine) ? eNOHASHLINE : 0)
    | (args.getBoolValue (scVerbose) ? eVERBOSE : 0);
}

//
// NExprSchema
//

NExprSchema::NExprSchema (MsgContext *msg_context, AtomicCache *symbols, 
  SourceLocatorFactory *sourcelocators, const UInt32 options) : 
  mMsgContext (msg_context), mSymbols (symbols), mSourceLocators (sourcelocators),
  mOptions (options), mNErrors (0), mNIterators (0), mNUBaseClass (NULL), mGoal (NULL)
{
}

//! The input file for the Flex-generated scanner
extern FILE *NExprSchema_in;
extern int NExprSchema_lineno;

//! Parse a schema specification file.
bool NExprSchema::parse (const char *filename)
{
  // open the input file
  UtString errmsg;
  if ((NExprSchema_in = OSFOpen (filename, "r", &errmsg)) == NULL) {
    // something went wrong...
    mMsgContext->NXSchemaCannotOpenInput (errmsg.c_str ());
    return false;                       // run away...
  }
  // call the Bison parser
  NExprSchema_lineno = 1;
  mCurrentFile = filename;              // used in error messages and locations
  int status = NExprSchema_parse (this);
  fclose (NExprSchema_in);
  // return true if there were no errors
  return (status == 0) && (nErrors () == 0);
}

//! Output an error message tied to a source location.
void NExprSchema::Error (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  YYError (loc, fmt, ap);
  va_end (ap);
}

//! Output an error message with no source line
void NExprSchema::Error (const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  char buffer [256];
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  mMsgContext->NXSchemaError (buffer);
  va_end (ap);
  mNErrors++;
}

//! Output a warning tied to a source location.
void NExprSchema::Warn (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  YYWarn (loc, fmt, ap);
  va_end (ap);
}

//! Output an information message tied to a source location.
void NExprSchema::Info (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  YYInfo (loc, fmt, ap);
  va_end (ap);
}

//! Output a warning tied to a source location.
void NExprSchema::YYWarn (const SourceLocator loc, const char *fmt, va_list ap)
{
  if (!isQuiet ()) {
    char buffer [1024];
    vsnprintf (buffer, sizeof (buffer), fmt, ap);
    mMsgContext->NXSchemaYYWarn (&loc, buffer);
  }
}

//! Output an information message tied to a source line.
void NExprSchema::YYInfo (const SourceLocator loc, const char *fmt, va_list ap)
{
  if (!isQuiet ()) {
    char buffer [1024];
    vsnprintf (buffer, sizeof (buffer), fmt, ap);
    mMsgContext->NXSchemaYYInfo (&loc, buffer);
  }
}

//! Output an error message at a source location and increment the error counter
void NExprSchema::YYError (const SourceLocator loc, const char *fmt, va_list ap)
{
  char buffer [1024];
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  mMsgContext->NXSchemaYYError (&loc, buffer);
  mNErrors++;
}

//! Output an error message at a source location and increment the error counter
void NExprSchema::YYError (const int lineno, const char *fmt, va_list ap)
{
  YYError (mSourceLocators->create (mCurrentFile, lineno), fmt, ap);
}

//! Allocate indices for the schema objects
/*! The indices are used at N-expression runtime to represent types and tokens.
 */
void NExprSchema::allocateIndices ()
{
  ClassType::Map::iterator classes;
  BaseType::Map::iterator basetypes;
  IncludeFile::Map::iterator includes;
  Token::Map::iterator tokens;
  // allocate an index to class types and base types
  UInt32 type_index = 0;
  for (classes = mClassTypes.begin (); classes != mClassTypes.end (); ++classes) {
    ClassType *classtype = classes->second;
    classtype->setIndex (type_index++);
  }
  INFO_ASSERT (type_index == mClassTypes.size (), "lost count");
  for (basetypes = mBaseTypes.begin (); basetypes != mBaseTypes.end (); ++basetypes) {
    BaseType *basetype = basetypes->second;
    basetype->setIndex (type_index++);
  }
  INFO_ASSERT (type_index == mClassTypes.size () + mBaseTypes.size (), "lost count");
  // Allocate indices to each scanner token.
  // Use the flex/bison convention that ascii characters used as token just use
  // the ascii code and more complex tokens are assigned indices starting at
  // 128
  UInt32 token_index = 128;
  for (tokens = mTokens.begin (); tokens != mTokens.end (); ++tokens) {
    tokens->second->setIndex (token_index++);
  }
  // Allocate indices to field names.
  // Each distinct field name gets allocated an index. This allows a mapping
  // from valid field names to an index. This index is used to lookup an
  // expected type table at runtime
  UInt32 field_index = 0;
  Field::MultiMap::iterator fields;
  for (fields = mFields.begin (); fields != mFields.end (); ++fields) {
    Field *field = fields->second;
    IndexMap::iterator found;
    if ((found = mFieldIndices.find (field->getName ())) != mFieldIndices.end ()) {
      // already allocated an index to this field name
      field->setIndex (found->second);
    } else {
      // allocate a new index
      field->setIndex (field_index);
      mFieldIndices.insert (IndexMap::value_type (field->getName (), field_index));
      field_index++;
    }
  }
  // Construct a map containing the field names for the API
  for (fields = mFields.begin (); fields != mFields.end (); ++fields) {
    Field *field = fields->second;
    if (!field->getClass ()->isInApi ()) {
      // the class is not part of the exported api so do not add it to the list
    } else if (mFieldNames.find (field->getName ()) != mFieldNames.end ()) {
      // the name of this field is already in the map
    } else if (*field->getName ()->str () == 'm') {
      // strip the leading 'm' of the field in the map... by convention all the
      // class members a prefixed with 'm'
      StringAtom *stripped = intern (field->getName ()->str () + 1);
      mFieldNames.insert (StringMap::value_type (field->getName (), stripped));
    } else {
      mFieldNames.insert (StringMap::value_type (field->getName (), field->getName ()));
    }
  }
}

//! Check the schema for internal consistency.
bool NExprSchema::check ()
{
  bool status = true;
  ClassType::Map::iterator classes;
  BaseType::Map::iterator basetypes;
  IncludeFile::Map::iterator includes;
  Token::Map::iterator tokens;
  // check the sanity of base types
  for (basetypes = mBaseTypes.begin (); basetypes != mBaseTypes.end (); ++basetypes) {
    // sanity check the type
    status &= basetypes->second->check (this);
  }
  // check the sanity of include files
  for (includes = mIncludeFiles.begin (); includes != mIncludeFiles.end (); ++includes) {
    status &= includes->second->check (this);
  }
  // check the sanity of each class type
  mNUBaseClass = NULL;
  bool found_cycle = false;
  for (classes = mClassTypes.begin (); classes != mClassTypes.end (); ++classes) {
    ClassType *classtype = classes->second;
    // sanity check on the class
    status &= classtype->check (this);
    // look for NUBase
    if (strcmp (classtype->getName ()->str (), "NUBase") != 0) {
      // not this one
    } else if (!classtype->isBaseClass ()) {
      SourceLocator loc = classtype->getDefn ();
      mMsgContext->NXSchemaNUBaseNotBase (&loc, classtype->getName ()->str ());
    } else {
      INFO_ASSERT (mNUBaseClass == NULL, "multiple NUBase objects found");
      mNUBaseClass = classtype;
      classtype->setProperties (NExprSchema::ClassType::eIS_NUCLEUS);
    }
    // if this is a base class then push out the inherited attributes
    if (classtype->isBaseClass ()) {
      classtype->propagateAttributes (ClassType::eINHERITED, 0);
    }
    // look for cycles in the hierarchy
    ClassType::List empty;
    if (classtype->checkedForCycles ()) {
      // already checked this class
    } else if (found_cycle) {
      // already found a cycle
    } else {
      classtype->checkCyclicHierarchy (this, empty, &found_cycle);
      status &= !found_cycle;
    }
  }
  // Check that there was a class called "NUBase" declared.
  if (mNUBaseClass == NULL) {
    mMsgContext->NXSchemaNoClass ("NUBase");
  }
  // Check the sanity of the IR operators
  IROp::Map::iterator irops;
  for (irops = mIR.begin (); irops != mIR.end (); ++irops) {
    status &= irops->second->check (this);
  }
  // Check the sanity of the Non-terminals
  NonTerminal::Map::iterator nts;
  for (nts = mNT.begin (); nts != mNT.end (); ++nts) {
    status &= nts->second->check (this);
  }
  // The code generator rewrite system
  Rewrite::List::iterator rewrites;
  for (rewrites = mRewrite.begin (); rewrites != mRewrite.end (); ++rewrites) {
    status &= (*rewrites)->check (this);
  }
  if (!mRewrite.empty () && mGoal == NULL) {
    Rewrite *first = *mRewrite.begin ();
    Error (first->getDecl (), "No goal non-terminal declared for the rewrite systems.");
    status = false;
  }
  // The low-level IR transformations
  IRTransformation::Map::iterator transforms;
  for (transforms = mTransforms.begin (); transforms != mTransforms.end (); ++transforms) {
    status &= transforms->second->check (this);
  }
  return status;
}

//! Declare a base type
NExprSchema::BaseType *NExprSchema::declareBaseType (const SourceLocator loc, StringAtom *name, 
  const UInt32 flags)
{
  NExprSchema::Type *type;
  NExprSchema::BaseType *basetype;
  if (isDeclared (name, &type)) {
    Error (loc, "Type '%s' is already declared.", name->str ());
    Info (type->getDecl (), "This is the previous declaration of '%s'", name->str ());
    return NULL;
  } else {
    basetype = new NExprSchema::BaseType (this, loc, name, flags);
    declare (basetype); 
    return basetype;
  }
}

//! Construct an enum type and declare it
NExprSchema::EnumType *NExprSchema::declareEnumType (const SourceLocator loc, StringAtom *name, 
  const UInt32 flags)
{
  NExprSchema::Type *type;
  NExprSchema::EnumType *enumtype;
  if (isDeclared (name, &type)) {
    Error (loc, "Type '%s' is already declared.", name->str ());
    Info (type->getDecl (), "This is the previous declaration of '%s'", name->str ());
    return NULL;
  } else {
    INFO_ASSERT (mEnumTypes.find (name) == mEnumTypes.end (), "bogus enumeration type entry");
    enumtype = new NExprSchema::EnumType (this, loc, name, flags);
    mEnumTypes.insert (EnumType::Map::value_type (name, enumtype));
    mBaseTypes.insert (BaseType::Map::value_type (name, enumtype));
    declare (enumtype); 
    return enumtype;
  }
}

//! Declare an enum value inside this enum
void NExprSchema::EnumType::declareEnumValue (NExprSchema *schema, const SourceLocator loc, StringAtom *name, UInt32 value)
{
  EnumLocationMap::iterator found;
  if ((found  = mLocations.find (name)) != mLocations.end ()) {
    schema->Error (loc, "Enum value '%s' is already declared.", name->str ());
    schema->Info (found->second, "This is the previous declaration of '%s'", name->str ());
  } else {
    INFO_ASSERT (mValues.find (name) == mValues.end (), "bogus value in enum value map");
    mValues.insert (EnumValueMap::value_type (name, value));
    mLocations.insert (EnumLocationMap::value_type (name, loc));
  }
}

//! Construct a vector type and declare it
NExprSchema::VectorType *NExprSchema::declareVectorType (const SourceLocator loc, StringAtom *name, 
  Type *element_type, const UInt32 flags)
{
  NExprSchema::Type *type;
  NExprSchema::VectorType *vectortype;
  if (isDeclared (name, &type)) {
    Error (loc, "Type '%s' is already declared.", name->str ());
    Info (type->getDecl (), "This is the previous declaration of '%s'", name->str ());
    return NULL;
  } else {
    vectortype = new NExprSchema::VectorType (this, loc, name, element_type, flags);
    declare (vectortype); 
    return vectortype;
  }
}

//! Construct a token and then declare it.
NExprSchema::Token *NExprSchema::declareToken (const SourceLocator loc, StringAtom *name, 
  const UInt32 flags)
{
  NExprSchema::Token *token;
  if (isDeclared (name, &token)) {
    // The same token may be used for several base type
#if 0
    Error (loc, "Token '%s' is already used.", name->str ());
    Info (token->getDecl (), "This is the previous use of token '%s'.", name->str ());
#endif
  } else {
    token = new NExprSchema::Token (this, loc, name, flags);
    declare (token);
  }
  return token;
}

//! Create a field object
NExprSchema::Field *NExprSchema::createField (const SourceLocator loc, Type *type,
  StringAtom *fieldident, const UInt32 properties)
{
  Field *field;
  field = new Field (this, loc, fieldident, type, properties);
  // check that the pointer modifier is only used with base type or vector fields
  bool is_pointer = properties & NExprSchema::Field::ePOINTER;
  if (is_pointer && !type->canPoint ()) {
    Error (loc, "The '*' type qualifier cannot be used on this type.");
    Info (type->getDecl (), "This is the declaration of type '%s'.", type->getName ()->str ());
  }
  return field;
}

//! Checking fields.
/*virtual*/ bool NExprSchema::Field::check (NExprSchema *schema)
{
  INFO_ASSERT (mClass != NULL, "field is not bound to a class");
  bool status = true;
  // add the field to the aggregate list of fields
  schema->addField (this);
  // check the field type
  if (isVector ()) {
    schema->Error (getDecl (), "Vector fields are not yet implemented.");
    status = false;
  }
  return status;
}

//! Check base types for consistency
bool NExprSchema::BaseType::check (NExprSchema *schema)
{
  bool status = true;
  // without a token scanner support is difficult
  if (mToken && (mFlags & eNONTERMINAL) != 0) {
    schema->Error (mToken->getDefn (), "Non-terminal type '%s' cannot have a scanner token.",
      getName ()->str ());
    status = false;
  } else if (mFlags & eNONTERMINAL) {
    // no token for the non-terminal
  } else if (mToken) {
    status &= mToken->check (schema);
  } else {
    schema->Warn (getDefn (), "No scanner token declared for type '%s'.", getName ()->str ());
  }
  return status;
}

//! Check vector types for correctness
/*virtual*/ bool NExprSchema::VectorType::check (NExprSchema *schema)
{
  bool status = true;
  if (mElementType->isErrorType ()) {
    // already whined about it
    status = false;
  } else if (!isPointerElement ()) {
    schema->Error (getDecl (), "Only vectors of pointers are currently implemented.");
    status = false;
  } else if (mElementType->isVectorType ()) {
    schema->Error (getDecl (), "Vectors of vectors are not yet supported.");
    schema->Info (mElementType->getDecl (), "This is the vector element type of '%s'.", getName ()->str ());
    status = false;
  }
  return status;
}

//! Set the list of superclasses for this class
void NExprSchema::ClassType::setSuperclasses (List *superclasses) 
{ 
  mSuperclasses = *superclasses; 
  for (List::iterator it = superclasses->begin (); it != superclasses->end (); ++it) {
    (*it)->mSubclasses.push_back (this);
  }
}

//! Propagtae attributes to children
void NExprSchema::ClassType::propagateAttributes (UInt32 mask, UInt32 inherited)
{
  mFlags |= (mask & inherited);
  char b [64];
  sprintf (b, "%04x", mFlags);
  for (List::iterator it = mSubclasses.begin (); it != mSubclasses.end (); it++) {
    (*it)->propagateAttributes (mask, mFlags);
  }
  if (mFlags & eUNNESTED) {
    mFlags &= ~eNESTED;
  }
}

//! \return a list of all the fields for the closure of this class
void NExprSchema::ClassType::getAllFields (Field::List *fields) const
{
  // acrete the fields of the superclasses
  ClassType::List::const_iterator supers;
  for (supers = mSuperclasses.begin (); supers != mSuperclasses.end (); ++supers) {
    (*supers)->getAllFields (fields);
  }
  // now append the fields of this class in declaration order
  Field::List::const_iterator it;
  for (it = mFieldList.begin (); it != mFieldList.end (); ++it) {
    fields->push_back (*it);
  }
}

//! Install code into the class
bool NExprSchema::ClassType::install (const Codes n, Code *code, Code **other)
{
  if (mCode [n] != NULL) {
    *other = mCode [n];
    return false;
  } else {
    mCode [n] = code;
    return true;
  }
}

//! Check the class type for internal consistency.
bool NExprSchema::ClassType::check (NExprSchema *schema)
{
  bool status = true;
  // check whether the type was implicitly declared
  if (isUndeclared ()) {
    schema->Error (getDecl (), "class '%s' was implictly declared but never defined.",
      getName ()->str ());
    status = false;
  }
  // check that abstract classes have at least one subclass
  if (isAbstract () && mSubclasses.empty ()) {
    // If you make this an error, then make sure to reset status
    schema->Warn (getDefn (), "abstract class '%s' does not have any subclasses.", getName ()->str ());
    // status = false;
  }
  // check all the fields
  Field::Map::iterator fields;
  for (fields = mFields.begin (); fields != mFields.end (); ++fields) {
    Field *field = fields->second;
    field->setClass (this);
    status &= field->check (schema);
  }
  // makes no sense to have a build code and mark the class as unbuildable
  if ((getBuildCode () != NULL || getPostBuildCode () != NULL || getPostTopBuildCode () != NULL) 
    && isUnbuildable ()) {
    schema->Error (getBuildCode ()->getDefn (), "class '%s' is marked as unbuildable.", 
      getName ()->str ());
    status = false;
  }
  // check the build code
  ContextWithDst context ("(*this)");
  if (getBuildCode () != NULL) {
    getBuildCode ()->bind (this);
    status &= getBuildCode ()->check (schema, context);
  }
  if (getPostBuildCode () != NULL) {
    getPostBuildCode ()->bind (this);
    status &= getPostBuildCode ()->check (schema, context);
  }
  if (getPostTopBuildCode () != NULL) {
    getPostTopBuildCode ()->bind (this);
    status &= getPostTopBuildCode ()->check (schema, context);
  }
  return status;
}

//! \return iff a named field is a superclass field
/*! \note if the field is declared then a handle is returned in field.
 */
bool NExprSchema::ClassType::isSuperclassField (StringAtom *name, Field **field) const
{
  List::const_iterator it;
  for (it = mSuperclasses.begin (); it != mSuperclasses.end (); it++) {
    ClassType *super = *it;
    if (super->isDeclared (name, field)) {
      return true;
    } else if (super->isSuperclassField (name, field)) {
      return true;
    }
  }
  return false;
}

#if 0
//! Check build code over the class for consistency
bool NExprSchema::ClassType::check (NExprSchema *schema, Code *code, bool is_build)
{
  bool status = true;
  Code::iterator it;
  for (it = code->begin (); it != code->end (); ++it) {
    CodeFragment *fragment = *it;
    Field *field;
    if (!fragment->hasField ()) {
      // no field associated with this fragment
    } else if (isSuperclassField (fragment->getName (), &field)) {
      // this is a field from a superclass
      if (field->getType ()->isClassType () && is_build) {
        schema->Error (fragment->getDecl (), "class-valued superclass fields cannot be used in build actions");
        schema->Info (field->getDecl (), "this is the declaration of '%s'.", field->getName ()->str ());
        status = false;
      } else {
        INFO_ASSERT (fragment->getField () == NULL, "multiple fields on code fragment");
        fragment->setField (field);
      }
    } else if (!isDeclared (fragment->getName (), &field)) {
      schema->Error (fragment->getDecl (), "class '%s' does not have a field called '%s'.",
        getName ()->str (), fragment->getName ()->str ());
      status = false;
    } else {
      INFO_ASSERT (fragment->getField () == NULL, "multiple fields on code fragment");
      fragment->setField (field);
      field->setProperties (Field::eNOTREAD);
    }
    status &= fragment->check (schema);
  }
  return status;
}
#endif

//! Check for cycles in the class hierarchy
void NExprSchema::ClassType::checkCyclicHierarchy (NExprSchema *schema, List parents, bool *found_cycle)
{
  setProperties (eCYCLE_CHECKED);
  // if the mark is set on entry, then this is a cycle
  if (!isMarked ()) {
    // set the mark and then descend into the subclasses
    setMark ();
    parents.push_back (this);
    for (List::iterator it = mSubclasses.begin (); it != mSubclasses.end () && !*found_cycle; ++it) {
      (*it)->checkCyclicHierarchy (schema, parents, found_cycle);
    }
  } else if (*found_cycle) {
    // only complain at the first cycle
  } else {
    // construct a string showing the cycle
    *found_cycle = true;
    UtString cycle;
    for (List::iterator it = parents.begin (); it != parents.end (); ++it) {
      if (cycle.length () == 0) {
        cycle << (*it)->getName ()->str ();
      } else {
        cycle << "->" << (*it)->getName ()->str ();
      }
    }
    cycle << "->" << getName ()->str ();
    schema->Error (getDecl (), "cyclic class hierarchy detected: %s", cycle.c_str ());
  }
  clearMark ();
}

//! Find the walkable subclasses of this class.
/*! The walkable subclasses are those subclass not tagged with the
 *  "unwalkable" attribute. This is used when generating a design walker
 *  for dumping nucleus as N-expressions.
 */
void NExprSchema::ClassType::findWalkableSubclasses (List *walkable)
{
  if (mFlags & eUNWALKABLE) {
    // the class cannot be walked
  } else if (mSubclasses.empty ()) {
    // this is a leaf class
    walkable->push_back (this);
  } else {
    // do not push this one onto the list, but traverse the subclasses
    for (List::iterator it = mSubclasses.begin (); it != mSubclasses.end (); it++) {
      (*it)->findWalkableSubclasses (walkable);
    }
  }
}

//! Add fragments to a code
/*! Parse a string to construct code fragments and push them onto this code
 *  \param loc the location in the schema containing the string
 *  \param s a specification string of the form "abc %foo def %foobar"
 *  \param errmsg point to a string for any error messages
 *  \return iff s was a valid code specification
 */
bool NExprSchema::Code::build (const SourceLocator loc, const char *s, const UInt32 flags,
  UtString *errmsg)
{
  UInt32 n = 0;                         // index into s
  while (s [n] != '\0') {
    // Each iteration of the loop constructs a code fragment.
    if (s [n] == '\\') {
      // special character
      push_back (new SpecialCodeFragment (mSchema, loc, s [n+1], mSchema->intern (s + n, 2)));
      n += 2;
    } else if (s [n] != '$') {
      // literal fragment
      // This time it is a literal fragment.
      UInt32 m = n;                     // start of the literal text
      while (s [n] != '\\' && s [n] != '$' && s [n] != '\0') {
        n++;
      }
      CodeFragment *fragment = new LiteralCodeFragment (mSchema, loc, mSchema->intern (s + m, n - m));
      fragment->setProperties (flags);
      push_back (fragment);
    } else if (s [n] == '$' && isdigit (s [n+1])) {
      // numbered reference
      UInt32 m = n;
      n++;
      UInt32 value = 0;
      while (isdigit (s [n])) {
        value = (10 * value) + static_cast <int> (s [n]) - static_cast <int> ('0');
        n++;
      }
      CodeFragment *fragment = new NumberedCodeFragment (mSchema, loc, mSchema->intern (s + m, n - m), value);
      fragment->setProperties (flags);
      push_back (fragment);
    } else if (s [n] == '$' && isalnum (s [n+1])) {
      // field reference
      n++;
      UInt32 m = n;                     // start of the identifier
      while (s [n] != '\\' && (isalnum (s [n]) || s [n] == '_')) {
        n++;
      }
      if (m == n) {
        *errmsg << "degenerate field identifier after '%s'";
        return false;
      } else {
        set (eHASTMPS);
        CodeFragment *fragment = new FieldCodeFragment (mSchema, loc, mSchema->intern (s + m, n - m));
        fragment->setProperties (flags);
        push_back (fragment);
      }
    } else if (s [n] == '$' && s [n+1] == '$') {
      // $$ references the destination
      set (eHASDST);
      CodeFragment *fragment = new DstCodeFragment (mSchema, loc, mSchema->intern ("$$"));
      fragment->setProperties (flags);
      push_back (fragment);
      n += 2;
    }
  }
  if (n == 0) {
    *errmsg << "degenerate code specification; expected something, found empty string";
    return false;
  }
  return true;
}

//! \return the name of a temporary variable for this fragment
/*virtual*/ const char *NExprSchema::FieldCodeFragment::getTmpName () const
{
  if (mTmpName == NULL) {
    UtString tmpname;
    tmpname << "tmp_" << getName ()->str ();
    mTmpName = getSchema ()->intern (tmpname.c_str ());
  }
  return mTmpName->str ();
}

void NExprSchema::CodeFragment::pr () const
{
  print (UtIO::cout ());
}

void NExprSchema::Code::pr () const
{
  print (UtIO::cout ());
}

/*virtual*/ void NExprSchema::LiteralCodeFragment::print (UtOStream &s) const
{
  s << "literal fragment \"" << getName ()->str () << "\"\n";
}

/*virtual*/ void NExprSchema::FieldCodeFragment::print (UtOStream &s) const
{
  s << "field fragment \"" << getName ()->str () << "\"\n";
}

/*virtual*/ void NExprSchema::DstCodeFragment::print (UtOStream &s) const
{
  s << "dst fragment\n";
}

/*virtual*/ void NExprSchema::NumberedCodeFragment::print (UtOStream &s) const
{
  s << "numbered fragment " << mIndex << "\n";
}

/*virtual*/ void NExprSchema::SpecialCodeFragment::print (UtOStream &s) const
{
  s << "special fragment: '\\" << mSpecial << "'\n";
}

const char *NExprSchema::Code::compose (UtString *s) const
{
  for (const_iterator it = begin (); it != end (); it++) {
    if (it != begin ()) {
      *s << " ";
    }
    *s << "\"" << (*it)->getName ()->str () << "\"";
  }
  return s->c_str ();
}

void NExprSchema::Code::print (UtOStream &s) const
{
  UtString buffer;
  getDefn ().compose (&buffer);
  s << buffer << ": code:\n";
  for (const_iterator it = begin (); it != end (); it++) {
    UtString b0;
    (*it)->getDecl ().compose (&b0);
    s << b0 << ": ";
    (*it)->print (s);
  }
}

//! analysis of codes
bool NExprSchema::Code::check (NExprSchema *schema, CodeContext &context)
{
  bool status = true;
  INFO_ASSERT (!useClassField () || mClass != NULL, "code has no class");
  for (iterator it = begin (); it != end (); ++it) {
    CodeFragment *fragment = *it;
    fragment->bind (this);
    status &= fragment->check (schema, context);
  }
  return status;
}

//! sanity checking for destination codes
/*virtual*/ bool NExprSchema::DstCodeFragment::check (NExprSchema *schema, CodeContext &)
{
  bool status = true;
  if (!getCode ()->allowDst ()) {
    schema->Error (getDecl (), "$$ cannot be used in this context.");
    status = false;
  }
  return status;
}

//! sanity checking for numbered codes
/*virtual*/ bool NExprSchema::NumberedCodeFragment::check (NExprSchema *schema, CodeContext &context)
{
  bool status = true;
  if (!getCode ()->allowNumbered ()) {
    schema->Error (getDecl (), "$%d cannot be used in this context.", mIndex);
    status = false;
  } else if (!context.hasOrdinal (mIndex)) {
    schema->Error (getDecl (), "$%d is out of range.", mIndex);
    status = false;
  }
  if (mIndex <= 0) {
    schema->Error (getDecl (), "$0 is an illegal expansion.");
    schema->Info (getDecl (), "Use $1 for the first child and $$ for the destination.");
  }
  getCode ()->set (NExprSchema::Code::eHASNUMBERED);
  return status;
}

//! sanity checking for field codes
/*virtual*/ bool NExprSchema::FieldCodeFragment::check (NExprSchema *schema, CodeContext &)
{
  bool status = true;
  if (!getCode ()->allowField ()) {
    // No particular class type is associated with code fragments.
    schema->Error (getDecl (), "Fields cannot be used in this context: $%s.", getName ()->str ());
    return false;
  }
  ClassType *classtype = getCode ()->getClass ();
  IROp *irop = getCode ()->getIROp ();
  INFO_ASSERT (getCode ()->useClassField () || getCode ()->useTerminalParameter (), "blah");
  // check against the class
  if (!getCode ()->useClassField ()) {
    // not checking this one against the class
  } else if (classtype == NULL) {
    // the should have been bound to a class
    INFO_ASSERT (classtype != NULL, "code has no class");
  } else if (classtype->isDeclared (getName (), &mField)) {
    mField->setProperties (getCode ()->inBuild () ? Field::eNOTREAD : 0);
  } else if (!classtype->isSuperclassField (getName (), &mField)) {
    schema->Error (getDecl (), "class '%s' does not have a field called '%s'.",
      classtype->getName ()->str (), getName ()->str ());
    status = false;
  } else if (mField->getType ()->isClassType () && getCode ()->inBuild ()) {
    schema->Error (getDecl (), "class-valued superclass fields cannot be used in build actions");
    schema->Info (mField->getDecl (), "this is the declaration of '%s'.", getName ()->str ());
    status = false;
  }
  // check against the terminal
  if (!getCode ()->useTerminalParameter ()) {
    // not checking this one against the terminal
  } else if (irop == NULL) {
    // This can happen if a chain rule code tries to use a field
    schema->Error (getDecl (), "Field $%s cannot be used in this context.", getName ()->str ());
    status = false;
  } else if (!irop->hasParameter (getName ())) {
    schema->Error (getDecl (), "Operator '%s' does not have a field called '%s'.",
      irop->getName ()->str (), getName ()->str ());
    schema->Info (irop->getDecl (), "This is the declaration of '%s'.", irop->getName ()->str ());
    status = false;
  }
  return status;
}

//! Expand a field code fragment
/*virtual*/ bool NExprSchema::FieldCodeFragment::expand (CodeStream &out, const CodeContext *) const
{ 
  if (mTmpName) {
    // the writeTmpInit method for this fragment has been called so output the
    // tmp name rather than the member name
    out << mTmpName->str ();
  } else {
    out << getName ()->str ();
  }
  return true;
}

bool NExprSchema::NumberedCodeFragment::expand (CodeStream &out, const CodeContext *expand) const
{
  Code *ordinal;
  if (expand->getOrdinal (mIndex, &ordinal)) {
    NestedCodeContext nested (*expand);
    nested (out, ordinal);
  } else {
    INFO_ASSERT (false, "cannot expand this numbered code in this context");
  }
  return true;
}

//! sanity checking for special codes
/*virtual*/ bool NExprSchema::SpecialCodeFragment::check (NExprSchema *schema, CodeContext &)
{
  switch (mSpecial) {
  case 'n':                             // new line
  case '\\':                            // backslash
  case '"':                             // double quotes
  case '>':                             // increase indentation
  case '<':                             // decrease indentation
    return true;
    break;
  default:
    schema->Error (getDecl (), "unrecognised special character '\\%c'.", mSpecial);
    return false;
  }
}

bool NExprSchema::SpecialCodeFragment::expand (CodeStream &out, const CodeContext *) const
{
  switch (mSpecial) {
  case 'n':                             // new line
    out << "\n";
    break;
  case '"':                             // double quotes
    out << '"';
    break;
  case '\\':                            // backslash
    out << '\\';
    break;
  case '>':                             // increase indent level
    out++;
    break;
  case '<':                             // decrease indent level
    out--;
    break;
  default:
    // if this assert triggers then the check method is deficient
    INFO_ASSERT (false, "bad special character");
    break;
  }
  return true;
}

//! Check a codegen IR operator for correctness
bool NExprSchema::IROp::check (NExprSchema *schema)
{
  bool status = true;
  if (mArity > 2) {
    schema->Error (getDecl (), "invalid arity for IR operator \"%s\"; expected 0, 1 or 2; found %d.",
      getName ()->str (), mArity);
    status = false;
  }
  // construct a map of the parameter names
  if (mParameters != NULL) {
    Parameters::const_iterator it;
    for (it = mParameters->begin (); it != mParameters->end (); ++it) {
      StringAtom *name = it->second;
      if (mParameterNames.find (name) != mParameterNames.end ()) {
        schema->Error (getDecl (), "duplicate parameter name: '%s'.", name->str ());
        status = false;
      } else {
        mParameterNames.insert (UtMap <const StringAtom *, bool>::value_type (name, true));
      }
    }
  }
  return status;
}

//! Check a BURS non-terminal for correctness
bool NExprSchema::NonTerminal::check (NExprSchema *schema)
{
  bool status = true;
  IROp *irop;
  if (schema->isDeclared (getName (), &irop)) {
    schema->Error (getDecl (), "\"%s\" is declared as both a terminal and a non-terminal.", 
      getName ()->str());
    schema->Info (irop->getDecl (), "This is the declaration of \"%s\" as a terminal.", getName ()->str ());
    status = false;
  }
  return status;
}

//! Check a BURS rewrite rules
bool NExprSchema::Rewrite::check (NExprSchema *schema)
{
  bool status = true;
  status &= mPattern->check (schema);
  if (mPattern->getIROp () != NULL && mCode != NULL) {
    mCode->bind (mPattern->getIROp ());
  }
  if (mCode == NULL) {
    schema->Error (getDecl (), "No code fragments for this rule.");
  } else {
    RewriteContext context (schema, getDecl (), mPattern->getArity ());
    status &= mCode->check (schema, context);
  }
  return status;
}

//! Check a BURS rewrite rule pattern
bool NExprSchema::Rewrite::Pattern::check (NExprSchema *schema)
{
  bool status = true;
  const UInt32 arity = (mLeft == NULL ? 0 : (mRight == NULL ? 1 : 2));
  if (schema->isDeclared (mSymbol, &mNT) && arity > 0) {
    // non-terminal used as a tree-operator
    schema->Error (mLoc, "Non-terminal \"%s\" used as a tree operator.", mSymbol->str ());
    status = false;
  } else if (mNT != NULL) {
    // non-terminal used in a chain rule
  } else if (!schema->isDeclared (mSymbol, &mIROp)) {
    schema->Error (mLoc, "Undeclared operator \"%s\".", mSymbol->str ());
    NExprSchema::IROp::Parameters *empty = new NExprSchema::IROp::Parameters;
    schema->declare (new NExprSchema::IROp (schema, mLoc, mSymbol, arity, empty));
    status = false;
  } else if (arity != mIROp->getArity ()) {
    schema->Error (mLoc, "Wrong number of operands for \"%s\"; expected %d, found %d.",
      mSymbol->str (), mIROp->getArity (), arity);
    status = false;
  }
  if (mLeft != NULL) {
    status &= mLeft->check (schema);
  }
  if (mRight != NULL) {
    status &= mRight->check (schema);
  }
  return status;
}

//! \return iff a goal non-terminal is declared
bool NExprSchema::hasGoal (NonTerminal **nt, SourceLocator *decl)
{
  if (mGoal == NULL) {
    *nt = NULL;
    return false;
  } else if (decl != NULL) {
    *nt = mGoal;
    *decl = mGoalDecl;
    return true;
  } else {
    *nt = mGoal;
    return true;
  }
}

  //! Set the goal non-terminal
void NExprSchema::bindGoal (NonTerminal *nt, SourceLocator loc)
{
  INFO_ASSERT (mGoal == NULL, "confused about goals");
  mGoal = nt;
  mGoalDecl = loc;
}

//! Output the rewrite to a stream
void NExprSchema::Rewrite::print (UtOStream &s) const
{
  IndentedStream <UtOStream> sink (s);
  sink << mNT->getName ()->str () << " <- ";
  mPattern->print (sink);
  sink << "\t[" << mCost << "]\n";
}

//! Output the rewrite to stdout
void NExprSchema::Rewrite::pr () const
{
  print (UtIO::cout ());
}

//! Output the pattern to a stream
void NExprSchema::Rewrite::Pattern::print (UtOStream &s0) const
{
  IndentedStream <UtOStream> s (s0);
  print (s);
}

//! Output the rewrite to stdout
void NExprSchema::Rewrite::Pattern::pr () const
{
  print (UtIO::cout ());
}

//! Output the pattern to an indenting stream
void NExprSchema::Rewrite::Pattern::print (IndentedStream <UtOStream> &s) const
{
  s << mSymbol->str ();
  if (mLeft != NULL && mRight == NULL) {
    s << "\t(";
    mLeft->print (s);
    s << ")";
  } else if (mLeft != NULL && mRight != NULL) {
    s << "\t(";
    mLeft->print (s);
    s << ",\t";
    mRight->print (s);
    s << ")";
  }
}

//
// NExprSchema::API
//

NExprSchema::API::API (NExprSchema *schema, const SourceLocator loc, StringAtom *name) :
  Declarable (schema, loc, name)
{}

