// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"
#include "util/CarbonAssert.h"
#include "nexpr/NExpr.h"
#include "nexpr/NExprTest.h"

#if NEXPR_ENABLE
#include "nexpr/NExprLex.h"
#endif

/*! \file NExprParse.cxx
 *
 *  This is the recursive descent parser that reads N-expressions from an input
 *  file and constructs NExpr instances.
 */

#if NEXPR_ENABLE

/* N-expressions have a simple syntax:
 *
 * object -> '(' label typename attributes ')'
 * typename -> _IDENT_ | _IDENT_ "::" typename
 * label  -> e | '@' _INTEGER_
 * attributes -> e | attribute attributes
 * attribute -> ':' _IDENT_ value
 * value -> basetype | object | label
 *
 * Methods of the recursive descent parser correspond to productions of the
 * grammar. Value parameters are used to pass inherited attributes down the
 * parse tree. Address parameters are used to pass synthesised attributes up
 * the parse tree.
 */

//!  object -> '(' typename label attributes ')'
/*! \param token the current token
 *  \param nexpr synthesised attribute - the constructed NExpr instance
 */
bool NExpr::Reader::parse_object (Lexeme *token, NExpr **nexpr)
{
  if (!token->is ('(')) {
    UtString errmsg;
    YYError (token->getLoc (), "%s", token->expected ('(', &errmsg));
    skip ('(', token);
    return false;
  }
  SourceLocator nexprstart = token->getLoc ();
  lex (0, token);                       // consume the open paren
  UInt32 label = 0;
  if (!token->is ('@')) {
    // not labelled
  } else if (!parse_label (token, &label)) {
    delete *nexpr;
    *nexpr = NULL;
    return false;
  } else if (label == 0) {
    // Objects cannot be bound to a zero label. Zero always denotes NULL
    YYError (token->getLoc (), "object cannot use zero as a label");
    label = 0;
  }
  StringAtom *classname;
  if (!parse_typename (token, &classname)) {
    return false;
  }
  // resolve the identifier into a type name
  UInt32 classtype;
  if (!lookupTypeIndex (classname->str (), &classtype)) {
    // the identifier leading the N-expression is not a known type
    YYError (token->getLoc (), "%s: unknown type.", classname->str ());
    return false;
  }
  // Create an NExpr instance. (name, value) tuples are glued into this
  // instance as they are read.
  *nexpr = new NExpr (nexprstart, classname);
  if (label > 0) {
    (*nexpr)->setLabel (label);
  }
  // parse the list of attributes
  while (token->is (':')) {
    if (!parse_attribute (token, classtype, *nexpr)) {
      delete *nexpr;
      *nexpr = NULL;
      return false;
    }
  }
  if (!token->is (')')) {
    UtString errmsg;
    YYError (token->getLoc (), "%s", token->expected (')', &errmsg));
    skip (')', token);
    delete *nexpr;
    *nexpr = NULL;
    return false;
  }
  lex (0, token);                       // valid parse of an N-expression
  return true;
}

//! typename -> _IDENT_ | _IDENT_ "::" typename 
/*! \param token the current token
 *  \param name synthesised attribute - the constructed type name
 */
bool NExpr::Reader::parse_typename (Lexeme *token, StringAtom **name)
{
  UtString errmsg;
  if (!token->is (eIDENT, name)) {
    YYError (token->getLoc (), token->expected (eIDENT, &errmsg));
    return false;
  }
  lex (0, token);                       // consume the identifier
  if (!token->is (eCOLONCOLON)) {
    // just a simple identifier
    return true;
  }
  // The type name is of the form A::B::C so we need to concat the various
  // components together and then form a new symbol.
  UtString symbol;
  symbol << (*name)->str ();
  while (token->is (eCOLONCOLON)) {
    StringAtom *next;
    lex (0, token);                     // consume the "::"
    if (!token->is (eIDENT, &next)) {
      YYError (token->getLoc (), token->expected (eIDENT, &errmsg));
      return false;
    }
    symbol << "::" << next->str ();
    lex (0, token);                     // consume the identifier
  }
  // Intern the name and return the symbol
  *name = intern (symbol.c_str ());
  return true;
}

//! label -> '@' _INTEGER_
/*! \param token the current token
 *  \param label synthesised attribute - the integer label
 */
bool NExpr::Reader::parse_label (Lexeme *token, UInt32 *label)
{
  if (!token->is ('@')) {
    UtString errmsg;
    YYError (token->getLoc (), token->expected ('@', &errmsg));
    return false;
  }
  lex (0, token);                       // consume the '@'
  if (!token->is (eINTEGER, label)) {
    UtString errmsg;
    YYError (token->getLoc (), token->expected (eINTEGER, &errmsg));
    return false;
  }
  lex (0, token);                       // consume the integer
  return true;
}

//! attribute -> ':' _IDENT_ value
/*! \param token the current token
 *  \param classtype inherited attribute - type of the current object
 *  \param nexpr inherited attribute - current NExpr instance
 */
bool NExpr::Reader::parse_attribute (Lexeme *token, const UInt32 classtype, NExpr *nexpr)
{
  UtString errmsg;
  // look for the attribute name
  INFO_ASSERT (token->is (':'), "parser jammed");
  lex (0, token);                       // consume the ':'
  StringAtom *fieldname;                // cache the name of the attribute
  if (!token->is (eIDENT, &fieldname)) {
    lex (0, token);
    UtString errmsg;
    YYError (token->getLoc (), "%s", token->expected (eIDENT, &errmsg));
    return false;
  }
  // look up the attribute name and work out the expected value
  UInt32 field, fieldtype;
  if (!lookupFieldIndex (fieldname->str (), &field)) {
    // the name is not known attribute name
    lex (0, token);                     // consume the bogus name
    YYError (token->getLoc (), "%s: unknown attribute name.", fieldname->str ());
    return false;
  } else if (!getType (classtype, field, &fieldtype)) {
    // does not name a field of this type
    lex (0, token);                     // consume the bogus name
    YYError (token->getLoc (), "%s: not a field of class '%s'.", 
      fieldname->str (), typeName (classtype));
    return false;
  }
  // read the value of the attribute
  BaseValue *value = NULL;
  SourceLocator loc = token->getLoc (); // cache the start location
  if (!parse_value (token, classtype, field, fieldtype, &value)) {
    // did not find a suitable value for this attribute
    return false;
  } else if (!nexpr->addField (loc, fieldname, value, &errmsg)) {
    // The field could not be installed. 
    YYError (loc, "%s", errmsg.c_str ());
    delete value;
    lex (0, token);                     // grab the next token
    return false;
  } else {
    // the field was installed 
  }
  return true;
}

//! value -> basetype | object | label
/*! \param token the previous token
 *  \param classtype inherited attribute - the class of the current N-expression
 *  \param field inherited attribute - the field ident of the current attribute
 *  \param fieldtype inherited attribute - the expected type of the current field
 *  \param value synthesised attribute - the constructed value
 *  \note This method is a little different from the other recursive descent
 *  methods in that token still hold the previous token, in this case the name
 *  of the attribute. This is because we need need to infer an expected token
 *  type before calling the scanner to yield the first token of the value.
 */
bool NExpr::Reader::parse_value (Lexeme *token, const UInt32 classtype, 
  const UInt32 field, const UInt32 fieldtype, BaseValue **value)
{
  // Work out the expected token type. If fieldtype is a terminal, then
  // expected will be set to the token type expected from the scanner. If the
  // expected argument to the scanner is non-zero, then the scanner enters a
  // state that will recognise a specific base type occurrence. This trick
  // allows us to use lexemes for base type values that are ambiguous. For
  // example, "pull" will scan as eStrPull when expecting a Strength basetype
  // but as an identifier otherwise.
  UInt32 expected;
  if (!expectedToken (fieldtype, &expected)) {
    expected = 0;
  }
  lex (expected, token);                // get the first lexeme of the value
  SourceLocator loc = token->getLoc (); // cache the location of the value
  // how we parse the value depends on the expected type
  UtString errmsg;
  UInt32 elementtype;                   // element type when doing a vector value
  if (isTerminal (fieldtype)) {
    // expecting an occurrence of a base type
    if (token->is (expected, value)) {
      // Expected as base type, and that base type we found.
      INFO_ASSERT (*value != NULL, "lexeme value is missing");
      lex (0, token);
      return true;
    } else {
      // the scanner did not find an occurrence of the expected field type
      YYError (loc, "%s", token->expected (expected, &errmsg));
      lex (0, token);                     // consume the bogus value
      return false;
    }
  } else if (isVector (fieldtype, &elementtype)) {
    // We are parsing a vector... but this is not implemented yet
    INFO_ASSERT (false, "unimplemented");
  } else if (token->is ('@')) {
    // expecting a non-terminal, and found a label reference
    UInt32 label;
    if (!parse_label (token, &label)) {
      return false;
    }
    *value = new Value <UInt32> (loc, label);
  } else if (token->is ('(')) {
    // expecting a non-terminal, and found a nested N-expression
    NExpr *nested = NULL;
    if (!parse_nested (token, classtype, field, fieldtype, &nested)) {
      return false;
    } else {
      // wrap a NestedValue container around the nested NExpr instance
      *value = new NestedValue (loc, nested);
    }
  } else {
    YYError (loc, "%s", token->expected ('(', &errmsg));
    skip ('(', token);
    return false;
  }
  return true;
}

//! value -> object
/*! \param token the current token
 *  \param classtype inherited attribute - the class of the current N-expression
 *  \param field inherited attribute - the field ident of the current attribute
 *  \param fieldtype inherited attribute - the expected type of the current field
 *  \param nested synthesised attribute - the constructed NExpr instance
 */
bool NExpr::Reader::parse_nested (Lexeme *token, const UInt32 classtype, 
  const UInt32 field, const UInt32 fieldtype, NExpr **nested)
{
  UInt32 actualtype;
  UtString errmsg;
  SourceLocator loc = token->getLoc ();
  if (!parse_object (token, nested)) {
    return false;
  } else if ((*nested) == NULL) {
    // If parse_nexpr returns true then nexpr should never be NULL, this
    // just checks...
    INFO_ASSERT ((*nested) != NULL, "nested N-expression is NULL");
    return false;
  } else if (!lookupTypeIndex ((*nested)->getType ()->str (), &actualtype)) {
    // the nested N-expression has a bogus type..
    delete (*nested);
    YYError (loc, "%s: unknown type in nested N-expression.", (*nested)->getType ()->str ());
    return false;
  } else if (actualtype != fieldtype && !isSubclass (fieldtype, actualtype)) {
    delete (*nested);
    YYError (loc, "%s is not a valid type for %s.%s.",
      typeName (actualtype), typeName (classtype), fieldName (field));
    return false;
  } else {
    return true;
  }
}

//! Consume tokens until we hit some distinguished token type
void NExpr::Reader::skip (const UInt32 expected, Lexeme *token)
{
  do {
    lex (0, token);
  } while (!token->is (0) && !token->is (expected) && !token->is (')'));
}

//! Construct a human readable representation of a token.
/*static*/ const char *NExpr::Reader::Lexeme::compose (const UInt32 token, UtString *s)
{
  if (isprint (token)) {
    // it is a single character token
    *s << "'" << static_cast <char> (token) << "'";
  } else {
    switch (token) {
    case 0:
      *s << "EOF";
      break;
    case eBOOL:
      *s << "boolean value";
      break;
    case eIDENT:
      *s << "identifier";
      break;
    case eUINT32:
    case eINTEGER:
      *s << "integer";
      break;
    case eSOURCE_LOCATOR:
      *s << "source locator";
      break;
#if 0
    case eSTLEAF:
      *s << "leaf (temporary)";
      break;
#endif
    case eSTRING_ATOM:
      *s << "string";
      break;
    default:
      *s << "unknown token: " << token;
      break;
    }
  }
  return s->c_str ();
}

//! Clear all the payload fields of a lexeme
void NExpr::Reader::Lexeme::clear ()
{
  mToken = 0;
  mSymbol = NULL;
  mInteger = 0;
  mValue = NULL;
}

//! Dump the lexeme to standard out
void NExpr::Reader::Lexeme::pr () const
{
  UtString b0, b1;
  mLoc.compose (&b0);
  UtIO::cout () << b0 << ": token " << mToken << ": " << compose (mToken, &b1) << "\n";
  if (mToken == eINTEGER || mToken == eUINT32) {
    UtIO::cout () << "  integer value: " << mInteger << "\n";
  }
  if (mSymbol != NULL) {
    UtIO::cout () << "  symbol is: " << mSymbol->str () << "\n";
  }
  if (mValue != NULL) {
    UtString buffer;
    mValue->compose (&buffer);
    UtIO::cout () << "  base type occurrence: " << buffer << "\n";
  }
}

//! Parse a file of N-expressions
bool NExpr::Reader::parse (const char *filename, UtString *errmsg)
{
  // open the input file for the scanner
  mFilename = filename;
  UtString syserrmsg;
  if ((NExpr_in = OSFOpen (mFilename, "r", &syserrmsg)) == NULL) {
    mMsgContext->NXCannotOpenInput (syserrmsg.c_str ());
    *errmsg << "fatal error";
    return false;
  }
  // This is where the method to reduce the start symbol of the grammar would
  // be called. For now, just repetitively call the lexer and output whatever
  // it returned. This demonstrates how to call the scanner, how to access
  // returned values and how to prime the scanner to look for specific tokens.
  Lexeme token;                         // return values from the scanner
  lex (0, &token);
  UInt32 testbase;
  if (!lookupTypeIndex ("NExprTest::Base", &testbase)) {
    INFO_ASSERT (false, "cannot find NExprTest::Base in the type table");
  }
  while (!token.is (0)) { /* 0 is end-of-file */
    NExpr *nexpr = NULL;
    if (!parse_object (&token, &nexpr)) {
      // failed to construct a NExpr
      INFO_ASSERT (nexpr == NULL, "leaked an NExpr instance");
    }
    if (nexpr != NULL && (mFlags & cECHO)) {
      nexpr->print (UtIO::cout ());
    }
    // construct an object
    UtString errmsg;
    void *object;
    if (nexpr == NULL) {
      // parse did not yield an NExpr
    } else if (!nexpr->build (*this, &object, &errmsg)) {
      YYError (nexpr->getLoc (), "object construction failed: %s", errmsg.c_str ());
      mNErrors++;
    }
    if (nexpr != NULL) {
      delete nexpr;
    }
  }
  patchLabels ();
  if (mNErrors == 1) {
    *errmsg << "1 error found by N-expression reader";
  } else if (mNErrors > 0) {
    *errmsg << mNErrors << " errors found by N-expression reader";
  }
  // clean up the chain of test objects
  NExprTest::Base::postmortem ();
  return (mNErrors == 0);
}

#else

//! Don't parse a file of N-expressions
bool NExpr::Reader::parse (const char *, UtString *errmsg)
{
  *errmsg << "N-expressions are disabled.";
  return false;
}

#endif
