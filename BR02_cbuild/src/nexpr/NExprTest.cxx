// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/AtomicCache.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtMap.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/CbuildMsgContext.h"
#include "nexpr/NExpr.h"
#include "nexpr/NExprTest.h"

#if NEXPR_ENABLE

namespace NExprTest {

//! The list of test objects
/*static*/ Base *Base::sHead = NULL;
/*static*/ Base *Base::sTail = NULL;

//! Add this object to the deletion chain
void Base::addToList ()
{
  if (sHead == NULL) {
    // first test object onto the list
    sHead = this;
    sTail = this;
  } else {
    // append to the end of the list
    sTail->mNext = this;
    sTail = this;
  }
}

//! Run through the chain of objects
/*static*/ void Base::postmortem ()
{
  // first walk the chain dumping all the top-level objects to stdout
  Base *object;
  for (object = sHead; object != NULL; object = object->mNext) {
    if (object->mDepth == 1) {
      object->pr ();
    }
  }
  // now walk the chain and free the objects
  Base *doomed;
  object = sHead;
  while (object != NULL) {
    doomed = object;
    object = object->mNext;
    delete doomed;
  }
  sHead = NULL;
  sTail = NULL;
}

/* visualisation of the NExprTest classes */

/* These are used for simple test cases that do not require any other
   infrastructure from cbuild. */

void Base::pr () const
{
  print (UtIO::cout ());
}

void Base::print (UtOStream &s) const
{
  PrintContext context (s, this);
  print (context);
}

/*virtual*/ void Base::print (PrintContext &print) const
{
  print ("mBaseX", mBaseX);
}

/*virtual*/ void NExprTest::A::print (PrintContext &print) const
{
  print ("mAX", mAX);
  Base::print (print);
}

/*virtual*/ void NExprTest::B::print (PrintContext &print) const
{
  print ("mBX", mBX);
  Base::print (print);
}

/*virtual*/ void NExprTest::C::print (PrintContext &print) const
{
  print ("mCX", mCX);
  print ("mCY", mCY);
  B::print (print);
  print ("mBase", mBase);
}

/*virtual*/ void NExprTest::D::print (PrintContext &print) const
{
  print ("mDX", mDX);
  B::print (print);
  print ("mBase", mBase);
}

Base::PrintContext::PrintContext (UtOStream &out, const Base *object,
  PrintContext *parent) :
  mOut (out), mObject (object), mParent (parent), mNFields (0)
{
  mOut << "[" << mObject->getType ();
}

/*virtual*/ Base::PrintContext::~PrintContext ()
{
  mOut << "]";
  if (mParent == NULL) {
    mOut << "\n";
  }
}

void Base::PrintContext::separate ()
{
  if (mNFields > 0) {
    mOut << ", ";
  } else {
    mOut << ": ";
  }
  mNFields++;
}

void Base::PrintContext::operator () (const char *name, const UInt32 value)
{
  separate ();
  mOut << name << "=" << value;
}

void Base::PrintContext::operator () (const char *name, const Base *nested)
{
  separate ();
  mOut << name << "=";
  if (nested == NULL) {
    mOut << "NULL";
  } else {
    PrintContext inner (mOut, nested, this);
    nested->print (inner);
  }
}

} // namespace NExprTest

#endif
