// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _BURS_H_
#define _BURS_H_

#include "util/UtMultiMap.h"
#include "NExprSchema.h"

//! \class BURS
/*! This class encapsulates the code for generating codegen modules based on
 *  bottom-up rewrite systems. During initialisation BURS instance imports
 *  terminals, non terminals and rewrite rules from the NExprSchema.
 *
 *  Methods that construct low-level IR (LIR) from Nucleus are emitted by the
 *  writeLIR method. This transformation is specified by the transformation
 *  rules in the schema.
 *
 *  Two cg variants may be emitted:
 *
 *  - writeIburgCG emits the iburg CG implementatin
 *  - writeBursCG emits the BURS CG implementation
 *
 *  For each of these, code generation is specified by a rewrite system that
 *  aims to rewrite LIR trees to a goal symbol. Code fragments associated with
 *  the rewrite rules in the schema determine the C/C++ that is emitted for
 *  matched nodes of the LIR tree.
 */
class BURS {
public:

  //
  enum Flags {
    eDOBURS = 1<<0,                     //!< output a BURS cg
    eDOIBURG = 1<<1,                    //!< output an Iburg cg
    eVERBOSE = 1<<2,                    //!< output progress information
    eTRACE = 1<<3                       //!< copious tracing of the BURS analysis
  };

  //! Constant values used in BURS analysis
  enum {
    /* Note that costs are stored in UInt16 so this value for eINFINITY is such
     * that any other UInt16 is less than infinity */
    eINFINITY = 0xFFFF,                 //!< effectively infinite cost
    eTHRESHOLD = 512,                   //!< threshold for detecting divergence
    eNOT_A_STATE = 0xFFFF               //!< used to indicate no state in state variables
  };

  //! ctor
  BURS (NExprSchema &schema, const UInt32 flags);
  virtual ~BURS ();

  //! Construct an options vector from command line arguments
  static UInt32 options (const ArgProc &);

  //! \return the handle to the schema
  NExprSchema &getSchema () const { return mSchema; }

  //! \return iff there are no rules in the system
  bool isEmpty () const { return mRewrites.empty (); }

  //! Rewrite system analysis
  /*! This method anaylses the rewrite system. If it is a valid BURS then it
   *  returns true and the various writeXXXX method emit the code generator.
   */
  bool analyse ();

  //! Write the low-level IR implementation.
  /*! This method writes LIR.def and LIRGen.cxx.
   *
   *  LIR.def contains macro invocations used by inc/src/LIR.h and src/LIR.cxx
   *  to declare the LIR operators. LIRGen.cx implements the various
   *  FOO::generateLIR methods that construct LIR trees from Nucleus.
   */
  bool writeLIR (const char *dirname);

  // The code generator is written in three parts:
  // 1. A ".def" file containing the operator definitions
  // 2. An iburg specification for the pattern matcher
  // 3. The implementation of the method to emit code from matched trees

  //! Output the Iburg based code generator
  /*! This method writes LIRMatch.brg and LIREmit.cxx.
   *
   *  LIRMarch.brg is an iburg specification for the rewrite system. This is
   *  pumped through iburg to get a BURS matcher. LIREmit.cxx contains methods
   *  that emit C++ from matched LIR trees. The C++ is specified by macros
   *  associated with the rewrite rules in the codegen specification.
   */
  bool writeIburgCG (const char *dirname);

  //! Output the BURS base code generator
  /*! This method writes BURS.cxx.
   *
   *  BURS.cxx contains the implementation LIR::label and
   *  LIR::emitCode. LIR::label the LIR tree with an integer pattern matcher
   *  state in a single bottom-up pass of the LIR treee. LIR::emitCode generates
   *  C/C++ from the LIR tree in a single top-down pass of the labelled tree.
   */
  bool writeBursCG (const char *dirame);

  //! Look up a terminal symbol and then validate the actual parameters
  bool analyseActuals (SourceLocator, NExprSchema::IROp *irop,
    NExprSchema::IRTransformation::IRTree::Parameter::List *, bool *, UInt32 *);

  //! \return iff a terminal operator requires parameters
  bool hasParameters (NExprSchema::IROp *irop) const
  { return mTerminal [irop->getIndex ()].hasParameters (); }

  //! \return iff we are generating an iburg definition
  bool doIburg () const { return mFlags & eDOIBURG; }

  //! \return iff we are generating an BURS definition
  bool doBurs () const { return mFlags & eDOBURS; }

private:

  //! \return iff this is a verbose compilation
  bool isVerbose () const { return mFlags & eVERBOSE; }

  //! \return iff this a traced compilatin
  bool isTraced () const { return mFlags & eTRACE; }

  //! Allocate indices to terminals, non terminals and rules
  bool allocateIndices ();

  //
  // Generic data types used during BURS analysis
  //

  //! Mapping between 16-bit state indices
  typedef UtMultiMap <UInt16, UInt16> StateMap;

  //! \class CostMatrix
  /*! This class implements 2d rectangular matrices of cost values.
   */
  class CostMatrix {
  public:
    //! ctor for an x by y matrix with a well-defined initial value
    CostMatrix (const UInt32 x, const UInt32 y, UInt16 initial);
    virtual ~CostMatrix ();

    //! \return cell (x, y) of the matrix
    UInt16 &get (const UInt32 x, const UInt32 y);

  private:
    const UInt32 mX, mY;                //!< matrix dimensions
    UInt16 *mMatrix;                    //!< storage for cells
  };

  //! \class BitMap
  /*! This class implements dynamically growing bitmaps. This is used during
   *  analysis to implement sets of proto states indices. Given that the number
   *  of proto states cannot be known a priori the bit map implementation must
   *  grow as needed.
   */
  class BitMap {
  public:

    BitMap ();
    virtual ~BitMap () { delete [] mCell; }

    //! \return iff n is set.
    bool isSet (const UInt16 n) const;

    //! Set an item in the bitmap
    void set (const UInt32 n);

    friend class loop;

    //! \class loop
    /*! Iteration over the members of a bitmap. */
    class loop {
    public:

      //! Intialise an iterator over map
      loop (const BitMap &map) : mMap (map), mWord (0), mBit (0), mAtEnd (false), mNext (0)
      { ++(*this); }

      //! \return iff the end of the iteration has been reached
      bool atEnd () { return mAtEnd; }

      //! \return the current member of the bitmap
      UInt32 operator * () const { return mNext; }

      //! Advance the iterator
      void operator ++ ();

    private:
      const BitMap &mMap;               //!< the bitmap under examination
      UInt32 mWord;                     //!< index of the current word in the map
      UInt32 mBit;                      //!< index of the current bit in the map
      bool mAtEnd;                      //!< set when the end is reached
      UInt32 mNext;                     //!< next item to yield from the iterator
    };

    //! Write the bitmap to a stream
    void print (UtOStream &, const UInt32 indent = 0) const;

    //! Write the bitmap to stdout
    void pr () const;

    //! Write the bitmap to an indented stream
    void print (IndentedStream <UtOStream> &) const;

  private:

    // The bitmap is implemented as a vector of UInt32 instances. A bit in the
    // vector is set when an item is added to the map. The lower five bits of
    // the item index the bit in a word, the upper bits select the word.
    UInt32 mSize;                       //!< current number of UInt32 instances used
    UInt32 *mCell;                      //!< vector of mSize UInt32s 
  };

  //
  // Data structures used during BURS analyis.
  //

  //! Representation of a rewrite rules.
  /*! The NExprSchema::Rewrite objects are converted into Rule instances before
   *  BURS analysis. The BURS algorithm distinguishes four different types of
   *  rule:
   *
   *  \item chain rules of the form a <- b where a and b are both non
   *   terminals.
   *
   *  \item leaf rules of the form a <- t0 where a is a non terminal and t0 is
   *  a nullary terminal.
   *
   *  \item unary rules of the form a <- t0(child0) where a is non terminal, t0
   *  is a unary terminal and child0 is a non terminal.
   *
   *  \item binary rules of the form a <- t0(child0, child1) where a is non
   *  terminal, t0 is a binary terminal and child0 and child1 are a non
   *  terminals.
   *
   *  The Rule object stores the integer indices of the terminals and non
   *  terminals of the rewrite rule.
   */
  class Rule {
  public:

    //! These are instantiated in an array so the ctor is parameter less.
    Rule () : 
      mRewrite (NULL), mIsChain (false), mIndex (0), mCost (eINFINITY), mLhs (0),
      mRhs (0), mArity (0)
    {}

    //! \return the specification source location for the rule
    SourceLocator getLoc () const { return mRewrite->getDecl (); }

    //! Initialise a chain rule from a schema rewrite rule.
    void bind (NExprSchema::Rewrite *, NExprSchema::NonTerminal *lhs,
      NExprSchema::NonTerminal *rhs);

    //! Initialise a non-chain rule from a schema rewrite rules and a child tuple
    void bind (NExprSchema::Rewrite *, NExprSchema::NonTerminal *lhs,
      NExprSchema::IROp *rhs, NExprSchema::NonTerminal *child [2]);

    //! Set the index of the rule
    void setIndex (const UInt32 n) { mIndex = n; }

    //! Return the Rewrite object that induced this rule object.
    NExprSchema::Rewrite *getRewrite () const { return mRewrite; }

    //! \return iff this is a chain rule
    /*! \note a chain rule is a rule of the form a <- b where a and b are both
     * non terminals.
     */
    bool isChain () const { return mIsChain; }

    //! \return the index of the rule
    UInt32 getIndex () const { return mIndex; }

    //! \return the lhs
    UInt32 getLhs () const { return mLhs; }

    //! \return the rhs
    UInt32 getRhs () const { return mRhs; }

    //! \return the cost
    UInt16 getCost () const { return mCost; }

    //! \return the arity
    UInt32 getArity () const { return mArity; }

    //! \return the n'th child
    UInt32 getChild (const UInt32 n) const { return mChild [n]; }
    
    //! Comparison function for sorting rules
    static int lessThan (const Rule &, const Rule &);

    //! Print the rule to a stream
    void print (UtOStream &) const;

    //! Print the rule to stdout
    void pr () const;

    //! Write the emitCode method for this rule.
    bool writeEmit (CodeStream &, BURS &);

  private:

    NExprSchema::Rewrite *mRewrite;     //!< handle on the schema object
    bool mIsChain;                      //!< iff this is a chain rule
    UInt32 mIndex;                      //!< index of the rule
    UInt16 mCost;                       //!< cost of this rule
    UInt32 mLhs;                        //!< left-side symbol
    UInt32 mRhs;                        //!< non-terminal if a chain rule, terminal otherwise
    UInt32 mArity;                      //!< number of children
    UInt32 mChild [2];                  //!< left and right children
  };                                    // class Rule
  
  //! \class Rules
  /*! This class encapsulates a vector of rule objects that represents all the
   *  rules of the rewrite system.
   */
  class Rules {
  public:

    Rules (BURS &mBurs, const UInt32 n_rules);
    virtual ~Rules ();

    UInt32 getNRules () const { return mNRules; }

    //! \return rule n
    Rule &get (const UInt32 n) const { return mRule [n]; }

    //! Add a rules to the aggregates
    /*! If the rewrite rule is not a valid rule, then an error message is
     *  written and this method returns false.
     */
    bool append (NExprSchema::Rewrite *);

    //! Sort the rules
    /*! The ordering is such that chain rules come before non-chain rules,
     *  nullary rules before unary, unary before binary and all the non-chain
     *  rules with the same terminal are contiguous. This allows allows
     *  iterating over all chain rules, or iterating over all the rules for a
     *  particular terminal with a simple for loop.
     */
    void sortRules ();

    UInt16 &chain (const UInt32 a, const UInt32 b)
    { return mChain->get (a, b); }

    //! Compute the chain cost matrix
    /*! This is a matrix that is intialised so that M[a,b] = C if there is
     *  chain rule a <- b with cost C. The matrix is then closed under the
     *  rewrite system. This is later used to trim entries from states.
     */
    void computeChainCosts ();

    //! \return the number of chain rules
    UInt32 numChainRules () const { return mNChainRules; }

    //! Print the rule set to a stream
    void print (UtOStream &) const;

    //! Print the rule set to standard output
    void pr () const;

  private:

    BURS &mBurs;                        //!< handle on the enclosing BURS object
    const UInt32 mNRules;               //!< number of rules
    UInt32 mNext;                       //!< location to add the next rule in append
    Rule *mRule;                        //!< array of objects
    UInt32 mNChainRules;                //!< number of chain rules
    UInt32 mNNonTerminals;              //!< number of non terminals
    CostMatrix *mChain;                 //!< transitive closure over chain rule costs

  };                                    // class Rules

  //! \class Terminal
  /*! This class encapsulates the terminal symbols of the rewrite system. Each
   *  IR operator declaration induces a terminal of the rewrite system. The
   *  declaration binds the arity of the terminal. This implementation requires
   *  that terminals are nullary, unary or binary operators. Implementation of
   *  the BURS analysis just gets too hard if arbitrary arity is
   *  allowed.
   */
  class Terminal {
  public:

    class Parameter {
    public:

      typedef UtMap <StringAtom *, Parameter *> Map;
      typedef UtMap <UInt32, Parameter *> Index;

      enum Flavour {
        eSTRING,                        //!< a string parameter
        eINTEGER                        //!< an integer parameter
      };

      Parameter (const Flavour flavour, StringAtom *name) :
        mFlavour (flavour), mName (name)
      {}

      virtual ~Parameter () {}

      //! \return the type of the parameter
      Flavour getFlavour () const { return mFlavour; }

      //! \return the name of the parameter
      StringAtom *getName () const { return mName; }

      //! \return iff this is a string parameter
      bool isString () const { return mFlavour == eSTRING; }

      //! \return iff this is an integer parameter
      bool isInteger () const { return mFlavour == eINTEGER; }

      //! Print the parameter to an indenting stream
      void print (IndentedStream <UtOStream> &) const;

    private:

      const Flavour mFlavour;           //!< the type of the parameter
      StringAtom *mName;                //!< the name of the parameter
    };

    Terminal () : mFirstRule (0xFFFF), mNRules (0), mNextActual (0) {}

    //! dtor frees storage allocated for parameters
    virtual ~Terminal ();
    
    //! \return iff this terminal is parameterised
    bool hasParameters () const { return !mParamMap.empty (); }

    //! Set the index of the first rule and the number of rules matching this terminal
    void setRules (const UInt32 first, const UInt32 count) 
    { 
      mFirstRule = first;
      mNRules = count;
    }

    //! Initialise this from a schema object
    void bind (NExprSchema::IROp *irop)
    {
      mIROp = irop;
      mArity = irop->getArity ();
      mIndex = irop->getIndex ();
    }

    //! \return the index of this rule
    UInt32 getIndex () const { return mIndex; }

    //! \return the schema object that induced this
    NExprSchema::IROp *getIROp () const { return mIROp; }

    //! \return the name of the terminal as a C string
    const char *getName () const { return mIROp->getName ()->str (); }

    //! \return the arity of the terminal
    UInt32 getArity () const { return mArity; }

    //! \return the first rule matching this terminal
    UInt32 getFirstRule () const { return mFirstRule; }

    //! \return the number of rules matching this terminal
    UInt32 getNRules () const { return mNRules; }

    //! Print the terminal to a stream
    void print (UtOStream &) const;

    //! Print the terminal to standard output
    void pr () const;

    //! Iterate across the state map for a given dimension
    StateMap::const_iterator firstState (const UInt32 dim) const { return mMap [dim].begin (); }
    StateMap::const_iterator lastState (const UInt32 dim) const { return mMap [dim].end (); }

    //! Iterate across the protostate for a given dimension
    BitMap::loop loopProtoStates (const UInt32 dim) { return BitMap::loop (mProto [dim]); }

    //! Bind a state to a proto state for a dimension
    void bindState (UInt32 dim, UInt32 state, UInt32 protostate)
    { 
      // add the mapping from state to protostate for the dimension
      mMap [dim].insert (StateMap::value_type (state, protostate));
      // add the protostate to the set of protostates applicable to the dimension
      mProto [dim].set (protostate);
    }

    //! Add a parameter in the next parameter position
    /*! \return true if successfull
     *  \note if the method returns false an error message is returned in
     *  errmsg
     */
    bool addParameter (StringAtom *type, StringAtom *name, UtString *errmsg);

    //! Check an ordered list of actual parameters against the formal parameters for this
    /*! If the list is legitimate then the index into the bindings for this
     *  terminal is returned in index.
     */
    bool analyseActuals (BURS &burs, SourceLocator loc, 
      NExprSchema::IRTransformation::IRTree::Parameter::List *, bool *, UInt32 *index);

    //! Write definitions for parameter bindings
    bool writeParameterBindingDefs (CodeStream &) const;

    //! Write the parameter bindings
    bool writeParameterBindings (CodeStream &s) const;

  private:

    NExprSchema::IROp *mIROp;           //!< handle on the inducing schema object
    UInt32 mIndex;                      //!< unique index of this terminal
    UInt32 mArity;                      //!< arity of the terminal
    UInt32 mFirstRule;                  //!< first rule that matches this terminal
    UInt32 mNRules;                     //!< number of rules that match this terminal
    Parameter::Map mParamMap;           //!< maps name to a parameter
    Parameter::Index mParamIndex;       //!< maps ordinal to a parameter
    UInt32 mNextActual;                 //!< index for next actual parameter binding

    /*! mMap maps the state index to a proto state index for each
     *  dimension. That is, if mMap[0] contains the entry (s,p) then p is the
     *  proto state corresponding to the left child of a node labeled with this
     *  operator matching to state s.
     */
    StateMap mMap [2];

    /*! mProto enumerates the proto states applicable to the children of nodes
     *  labeled with this operator.
     */
    BitMap mProto [2];

    typedef UtList <NExprSchema::IRTransformation::IRTree::Parameter *> ValueList;
    typedef UtMap <StringAtom *, ValueList *> ValueMap;
    ValueMap mActuals;                  //!< maps formal names to lists of values

  };

  friend class Transitions;

  class ProtoState;                     // forward declaration
  class State;                          // forward declaration

  //! \class Transitions
  /*! A transition is a 3-tuple (terminal, (s0, s1), result) where terminal is
   *  a terminal symbol children, (s0, s1) a 2-tuple of proto states and result
   *  a non-terminal symbol. During matching, if a tree node is labeled with
   *  terminal and its children are labeled according to s0 and s1 then the
   *  label of the node is set to result.
   *
   *  This class encapsulates all the states discovered during BURS
   *  analysis.
   */
  class Transitions {
  public:

    Transitions (BURS &);
    virtual ~Transitions ();

    //! Add a transition to the set of transitions
    /*! \param op the terminal symbol
     *  \param child the 2-tuple of proto state indices
     *  \param result the result state
     */
    void add (UInt32 op, UInt16 child [2], UInt16 result);

    //! \return the total number of transitions
    UInt32 getNumTransitions () const { return mNTransitions; }

    //! Dump transitions to a stream
    void print (UtOStream &) const;

    //! Dump transitions to stdout
    void pr () const;

    //! \class Tuple
    /*! This is a 2-tuple of states. Tuples are used to index the map of state
     *  transitions for each operator. For nullary operators, each member of
     *  the tuple should be set to eNOT_A_STATE. For unary operators the second
     *  member of the tuple should be set to eNOT_A_STATE. Note that the
     *  members are the indices of proto states rather than states.
     */
    class Tuple {
    public:

      //! Initialise a tuple from a pair of integers
      Tuple (UInt16 items [2]) { mItem [0] = items [0]; mItem [1] = items [1]; }

      //! \return the items of the tuple
      UInt16 operator [] (const UInt32 n) const
      { return mItem [n]; }

      //! Sorting comparison for tuples.
      struct lessThan {
        bool operator () (const Tuple a, const Tuple b)
        { 
          if (a.mItem [0] != b.mItem [0]) {
            return (a.mItem [0] < b.mItem [0]);
          } else if (a.mItem [1] != b.mItem [1]) {
            return (a.mItem [1] < b.mItem [1]);
          } else {
            return false;
          }
        }
      };

    private:
      UInt16 mItem [2];               //!< the pair of values in the 2-tuple
    };

  private:
    
    //! Representation of a transition.
    /*! When a transition is identified from proto states to a state, a record
     *  is added to the map for the appropriate terminal.
     */
    class Transition {
    public:

      Transition (UInt16 op, Tuple child, UInt16 output) :
        mTerminal (op), mOutput (output), mChild (child)
      {}

      //! Map tuples to transitions
      typedef UtMap <Tuple, Transition *, Tuple::lessThan> Map;

      //! \return the terminal operator
      UInt16 getTerminal () const { return mTerminal; }

      //! \return the result state index
      UInt15 getOutput () const { return mOutput; }

      //! \return the input state tuple
      Tuple getInputs () const { return mChild; }

      //! Dump the transition to a stream
      void print (UtOStream &, const Terminal &op) const;

    private:

      UInt16 mTerminal;                 //!< the terminal
      UInt16 mOutput;                   //!< the output state index
      Tuple mChild;                     //!< the child proto-state indices
    };


  public:

    // Iteration across the transitions
    friend class loop;

    //! \class loop
    /*! This iterator will iterate either across all transitions or across all the
     *  transitions associated with a given operator.
     */
    class loop {
    public:

      //! ctor to iterate over all the transitions
      loop (Transitions &transitions) : 
        mTransitions (transitions), mTerminal (0), mLastTerminal (mTransitions.mBurs.mNTerminals - 1)
      { seek (); }

      //! ctor to iterate over the transitions for an operator
      loop (Transitions &transitions, UInt32 op) : 
        mTransitions (transitions), mTerminal (op), mLastTerminal (op)
      { seek (); }

      //! \return iff we are at the end of the iterations
      bool atEnd () { return mTerminal > mLastTerminal; }

      //! Advance the iterator
      void operator ++ ()
      {
        mIt++;
        if (mIt == mTransitions.mTransition [mTerminal].end ()) {
          mTerminal++;
          seek ();
        }
      }

      //! \return the input proto states of the current transition
      Tuple getInputs () const { return mIt->first; }

      //! \return the result state of the current transition
      UInt16 getOutput () const { return mIt->second->getOutput (); }
      
    private:

      Transitions &mTransitions;        //!< handle on the transitions object
      UInt32 mTerminal;                 //!< current terminal
      UInt32 mLastTerminal;             //!< last terminal to iterate across
      Transition::Map::iterator mIt;    //!< iterator over the map

      //! seek the next non-empty transition map
      void seek ()
      {
        while (mTransitions.mTransition [mTerminal].empty () && mTerminal <= mLastTerminal) {
          mTerminal++;
        }
        if (mTerminal <= mLastTerminal) {
          mIt = mTransitions.mTransition [mTerminal].begin ();
        }
      }
    };

  private:

    BURS &mBurs;                        //!< handle on the BURS context
    UInt32 mNTransitions;               //!< total number of transitions
    Transition::Map *mTransition;       //!< vector of mNTerminals transition maps
  };

  //! \class ProtoState
  /*! ProtoState instances represent partial rewrite applications at a node. 
   *  Suppose that a node is labelled with op and we are considering which
   *  rules are applicable when child dim is assigned state S.
   * 
   * The protostate consists of an terminal and a mapping cost from non
   * terminals to costs. Define the cost function of the protostate by: cost
   * (nt) is C if there is a rewrite for op with nt in position dim and the
   * cost of nt in state S is C. If is infinity if there is no such rewrite or
   * if the cost of nt in S is infinite.
   */
  class ProtoState {
  public:

    ProtoState (BURS &burs, const UInt16 index);
    virtual ~ProtoState ();

    //! Normalise the costs of the state
    /*! Normalisation looks for the smallest cost value in the protostate and
     *  subtracts that from all the costs. Thus, after normalisation, while
     *  the relative costs are the same, the costs ranges from
     *  0...max. Normalising prevents blowout in the number of distinct proto
     *  states.
     */
    void normalise ();

    //! \return the cost of a non terminal
    UInt16 getCost (const UInt32 nt) { return mCost [nt]; }

    //! Set the cost of a non terminal
    void setCost (const UInt32 nt, const UInt16 cost) { mCost [nt] = cost; }

    //! \return the unique index assigned to this proto state
    UInt16 getIndex () const { return mIndex; }

    //! Equality comparison for proto states
    /*! Two protostates are equivalent iff the terminal matches and the cost
     *  associated with non-terminal is the same.
     */
    struct isEqual {
      bool operator () (ProtoState *, ProtoState *);
    };

    //! Sorting comparison for proto states
    struct lessThan {
      bool operator () (ProtoState *, ProtoState *);
    };

    //! Obarray for identification of equivalent proto states
    typedef UtMap <ProtoState *, ProtoState *, lessThan> Obarray;
 
    //! Dump the proto state to a stream.
    void print (UtOStream &, const UInt32 indent = 0) const;
    
    //! Dump the proto state to standard output
    void pr () const;

  private:
    BURS &mBurs;                        //!< context
    UInt16 mIndex;                      //!< unique index assigned to protostate
    UInt32 mSize;                       //!< number of cost items
    UInt16 *mCost;                      //!< vector of costs indexed by non-terminal
  };
  
  //! \class State
  /*! A state represents consists of a terminal symbol and a mapping from each
   *  non terminal to a cost and a rule. If the non terminal is "not in" the
   *  state, then the cost is infinity. If the non terminal is "in" the state
   *  then it is associated with a cost and a rule. The cost represents the
   *  cost of treating the terminal as that non terminal when matching a parent
   *  and the rule that is applied for that match.
   */
  class State {
  public:

    //! Initialise a state with an index and a terminal.
    /*! \note After initialisation the state is empty. That is the cost of
     *  every non terminal is infinity.
     */
    State (BURS &, const UInt16 index, const UInt32 terminal);
    virtual ~State ();

    //! \return iff this state contains any live matches.
    bool isLive () const { return mLive; }

    //! \return the terminal symbol of the state
    UInt32 getTerminal () const { return mTerminal; }

    //! \return the cost for a non terminal
    UInt16 getCost (UInt32 non_terminal) const 
    { return mItem [non_terminal].mCost; }

    //! Remove an non terminal from the state
    void removeNonTerminal (UInt32 nt)
    { mItem [nt].mCost = eINFINITY; }

    //! Add a non terminal to the state with a given rule and cost
    void set (UInt32 symbol, UInt32 rule, UInt32 cost)
    {   
      mItem [symbol].mRule = rule;
      mItem [symbol].mCost = cost;
      mLive = true;
    }

    //! Equality comparison for states
    struct isEqual {
      bool operator () (State *, State *);
    };
    
    //! Sorting comparison of states
    struct lessThan {
      bool operator () (State *, State *);
    };

    //! Obarray for states
    /*! The obarray maps states to states with the state comparison
     *  function. It allows a newstate to be tested to see whether it is
     *  identical to an existing state. This matters because analysis
     *  terminates when no new unique states can be identifier for the rewrite
     *  system.
     */
    typedef UtMap <State *, State *, lessThan> Obarray;

    //! Normalise a state.
    /*! Normalisation examines all the costs and adjusts them so that relative
     *  costs are the same but the smallest cost in the state is zero. For
     *  example, a state consisting of {stmt=5, lvalue=2, rvalue=3} is
     *  normalised to {stmt=3, lvalue=0, rvalue=1}. Normalising newstates
     *  before looking for identical existing states reduces the number of
     *  unique states. This is OK because the relative costs within a state
     *  matter but the absolute values do not.
     *
     */
    void normalise ();

    //! Close a state under the chain rules.
    /*! A state is closed by successively applying chain rules until no new non
     *  terminals are added to the state. For example, if N is a member of the
     *  state with cost C1 and there is a chain rule M <- N with cost C2 then M
     *  may be added to the state with cost C1+C2, provided M is either absent
     *  from the state or the cost of M is greater than C1+C2.
     */
    void close ();

    //! Print a state to a stream.
    void print (UtOStream &, const UInt32 indent = 0) const;

    //! Print a state to standard output
    void pr () const;

    //! Return the unique index of a state
    UInt16 getIndex () const { return mIndex; }

    // Iteration over a state
    friend class loop;

    //! \class loop
    /*! This iterator yields successive non terminals from the state.
    */
    class loop {
    public:
      
      //! Initialise the iterator for a state
      loop (State &state) : mState (state), mNext (0)
      { seek (); }

      //! \return iff we have reached the end of the iteration
      bool atEnd () const { return mNext >= mState.mSize; }

      //! Advance the iterator
      void operator ++ () { mNext++; seek (); }

      //! \return the current non terminal in the iteration
      UInt32 getNT () const { return mNext; }

      //! \return the rule associated with the current non terminal
      UInt16 getRule () const { return mState.mItem [mNext].mRule; }

      //! \return the cost of the current non terminal
      UInt16 getCost () const { return mState.mItem [mNext].mCost; }

    private:

      State &mState;                    //!< the object of the iteration
      UInt32 mNext;                     //!< next non terminal to yield

      //! Look for the next non terminal in the state
      void seek ()
      { 
        while (mNext < mState.mSize && mState.mItem [mNext].mCost == eINFINITY) {
          mNext++;
        }
      }

   };

  private:

    //! The state is implemented as a vector of Item instances.
    struct Item {
      Item () : mRule (0), mCost (eINFINITY) {}
      UInt16 mRule;                     //!< the rule for this item
      UInt16 mCost;                     //!< the cost of this item
    };

    BURS &mBurs;                        //!< BURS context
    const UInt16 mIndex;                //!< unique index of this state
    const UInt32 mTerminal;             //!< the terminal symbol for this state
    const UInt32 mSize;                 //!< number of items in the state
    bool mLive;                         //!< set when the first non terminal is added to the state
    Item *mItem;                        //!< vector of mSize items
  };

  //! \class States
  /*! This class encapsulates all the state instances of the rewrite
   * system. New states are added during analysis.
   */
  class States {
  public:

    States (BURS &burs) : mBurs (burs), mNextState (0), mNextProto (0) {}
    virtual ~States ();

    //! \return iff a state is identical with an existing state
    bool isState (State *);

    //! Create a new state
    State *create (UInt32 terminal) { return new State (mBurs, mNextState, terminal); }

    //! Intern a new state into the state obarray
    /*! If the new state is identical with an existing state then the new state
     *  is deleted and the existing state is returned. If not, then the new
     *  state is added to the obarray and the new state itself is returned.
     */
    State *intern (State *);

    //! Intern a new proto state into the proto state obarray
    ProtoState *intern (ProtoState *);

    //! Construct a proto state from a state and a non terminal
    /*! The proto state consists of a mapping from non terminals to costs. The
     *  protostate constructed by projecting a state through a dimension of a
     *  non terminal constains a entry for each non terminal nt such that nt is
     *  in the state and there is a rule for the terminal with nt in position
     *  dim. The cost of nt in the proto state is the cost of nt in the state
     *  plus the cost of the rule.
     */
    ProtoState *project (State *, const Terminal *terminal, const UInt32 dim);

    //! \return the state with the given unique index
    State *getState (const UInt16 n) const;

    //! \return the proto state with the given unique proto state index
    ProtoState *getProto (const UInt16 n) const;

    //! \reutrn the total number of states
    UInt32 getNumStates () const { return mStateObarray.size (); }

    //! \return the total number of proto states
    UInt32 getNumProtoStates () const { return mProtoObarray.size (); }

    //! Dump to a stream
    void print (UtOStream &) const;

    //! Dump to standard output
    void pr () const;

    // Iteration over states
    friend class loop;

    //! \class loop
    /*! This is an iterator that yields all states of the BURS.
     */
    class loop {
    public:
      
      //! Initialise the iterator
      loop (States &states) : mStates (states), mIt (mStates.mStateTable.begin ()) {}

      //! \return iff we have reached the end of the iteration
      bool atEnd () { return mIt == mStates.mStateTable.end (); }

      //! \return the next state in the iteration
      State *operator * () { return mIt->second; }

      //! Advance the iteration
      void operator ++ () { mIt++; }
    private:
      States &mStates;                  //!< object over which we iterate
      UtMap <UInt16, State *>::iterator mIt;
    };

  private:

    BURS &mBurs;                        //!< BURS context
    UInt32 mNextState;                  //!< unique index to give to the next unique state
    UInt32 mNextProto;                  //!< unique index to give to the next unique proto state
    State::Obarray mStateObarray;       //!< obarray of states
    ProtoState::Obarray mProtoObarray;  //!< obarray of proto states
    UtMap <UInt16, State *> mStateTable; //!< map state indices to states
    UtMap <UInt16, ProtoState *> mProtoTable; //!< map proto state indices to proto states

  };

  //
  // Instance variables of class BURS
  //

  NExprSchema &mSchema;                 //!< the schema that specifies this BURS
  UInt32 mFlags;                        //!< options passed into the instance

  // These members encapsulate the schema objects.
  NExprSchema::IROp::Map &mIR;          //!< table of IR operators
  NExprSchema::NonTerminal::Map &mNT;   //!< table of non terminals
  NExprSchema::Rewrite::List &mRewrites; //!< list of rewrite rules
  NExprSchema::IRTransformation::Map &mTransforms; //!< list of LIR transformations
  NExprSchema::NonTerminal *mGoal;      //! the rewrite goal non terminal
  NExprSchema::NonTerminal **mNonTerminalMap; //!< maps index to non terminal

  // Each rule, terminal and non terminal may be represented by a single
  // integer during BURS analysis. Iteration across these symbols and rules may
  // then be realised with a simple for loop. These members count the number of
  // each of these entities.
  UInt32 mNTerminals;                   //!< number of terminals
  UInt32 mNNonTerminals;                //!< number of last non terminals
  UInt32 mNRules;                       //!< number of rules
  UInt32 mNChainRules;                  //!< number of chain rules

  // Containers for rules, terminals, transitions and states
  Rules *mRules;                        //!< container for rules
  Terminal *mTerminal;                  //!< vector of terminal symbol descriptors
  Transitions *mTransitions;            //!< container of transitions
  States *mStates;                      //!< container of states

  // When a new state is created during analysis it is added to the state
  // worklist. When removed from the worklist, matches with that state in all
  // positions of all non terminals are considered. Any new unique states
  // resulting from that consideration are added to the worklist. Analysis
  // terminates when the work list is empty. 
  UtList <State *> mWorklist;           //!< the state worklist
  
  //! Add a new state to the worklist
  void addToWorklist (State *newstate);

  //
  // Queries for rewrite system entities
  //

  //! \return the rule with index equal to n
  Rule &getRule (const UInt32 n) 
  { return mRules->get (n); }

  //! \return the non terminal object of the schema with index equal to n
  NExprSchema::NonTerminal *getNonTerminal (const UInt32 n) const
  { return mNonTerminalMap [n]; }

  //! \return the name of the non terminal with index n as a C string
  const char *terminalName (const UInt32 n) const { return mTerminal [n].getName (); }

  //! \return the name of the terminal with index n as a C string
  const char *nonTerminalName (const UInt32 n) const { return getNonTerminal (n)->getName ()->str (); }

  //
  // Helper methods for analysis
  //

  //! Create the state for each leaf terminal
  /*! Leaf state are induced by nullary terminal symbols. Because there are no
   *  inputs (that is tree nodes labeled with these symbols must be leaves)
   *  there will be a single unique state for each nullary symbol. This method
   *  constructs such a state for each leaf symbol. For each rule Nx <- t with
   *  cost Cx in the rewrite system for the nullary symbol Nx is added to the
   *  leaf state with cost Cx.
   */
  void computeLeafStates ();

  //! Compute the transitions out of a state for an terminal.
  /*! All possible transitions for an operator with a state at one of its
   *  operands are constructed.
   */
  void computeTransitions (const UInt32 op, State *state);

  //! Compute the transitions out of a terminal for a set of input proto states
  void computeTransition (Terminal *, ProtoState *input [2]);

  //! Trim a state
  /*! Trimming removes non terminals from states if it can be shown that the
   *  rule rewriting to a given non terminal will never be chosed. Currently,
   *  only chain trimming is implemented.
   */
  void trimState (State *);
  
  //! Chain trim a state
  /*! Chain trimming looks at the non terminals in a state and the chain *
   *  rules. If there is a chain rule L <- R with cost C0 and the finite cost
   *  of L in * the state is C1 and the finite cost of R is C2. Then if C0 + C2
   *  is less * than C1 then L can be removed because matching to R and then
   *  rewriting with * the chain will be cheaper than matching to L.
   */
  void chainTrimState (State *);

  //! Sort a vector of objects.
  /*! This is a templated quicksort implementation for vectors. compare(a, b)
   *  should return <0 if a<b, 1, >0 if b>a and 0 if a=b.
   */
  template <typename _T_>
  static void sort (_T_ *vector, SInt32 lo, SInt32 hi, int (compare) (const _T_ &, const _T_ &));

  //
  // Post-portem dumps
  //

  //! Write out the terminals, non terminals and rules of the system
  /* \note when tracing is enable, the system is dumped to stdout immediately
   *  prior to analysis.
   */
  void dumpSystem (UtOStream &) const;

  //! Write out the results of BURS analysis to BURS.txt in the output directory
  /*! The analysis contains a dump of the rewrites (dumpSystem) and an
   *  enumeration of states, proto states and transitions. It is always written
   *  immediately prior to BURS code generation.
   */
  void dumpAnalysis (const char *dirname);

  //
  // Methods for emitting the two CG variants.
  //

  //! Output the LIR definition file
  /*! The definition file contains macro instances that define terminals and
   *  nonterminals. This is needed for both the iburg and the BURS cg variants.
   */
  bool writeLIRDef (const char *dirname);

  //! Output the IRTree generation
  /*! This method writes LIRGen.cxx. This is needed for both the iburg and the
   *  BURS cg variants. */
  bool writeLIRGen (const char *dirname);

  //! Output the iburg machine description
  /*! This method writes LIRMatch.brg. This is needed for both the iburg and
   *  the BURS cg variants. */
  bool writeIburg (const char *dirname);

  //! Output the code generation methods for the iburg matcher
  /*! This method writes LIREmit */
  bool writeLIREmit (const char *dirname);

  //! Output the BURS cg
  /*! This method writes BURScg.cxx */
  bool writeBURS (const char *dirname);

  //! Output the LIR::getProtoState method.
  /*! getProtoState is one of the two methods that implement LIR tree labelling */
  void writeGetProtoState (CodeStream &out);

  //! Output the LIR::label method
  /*! label is the other method that implements LIR tree labelling */
  void writeLabel (CodeStream &out);

  //! Output the LIR::emitCode method
  /*! This is the method that emits C/C++ from a labeled LIR tree */
  void writeEmitCode (CodeStream &out);

};

#endif
