// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "nexpr/NExpr.h"

#include "util/CarbonTypes.h"
#include "util/StringAtom.h"
#include "util/SourceLocator.h"
#include "nucleus/NUExpr.h"

/*! \file NExprBaseTypes.cxx
 *
 *  Attributes are glues to NExpr instances in (name, value) pairs where the
 *  value is some subclass of BaseValue. The PointerValue and Value subclasses
 *  are templated with a payload type. For each specific payload type used
 *  explicit specialisations of a number of methods are required. The scanner
 *  constructs instances of the appropriate value class when it encounters a
 *  base type instance.
 *
 *  When writing base type fields, NExpr::Writer uses a templated write
 *  method. For each base type, an explicit specialisation is required for the
 *  type. These methods should __NOT__ directly acces mOut. Instead, they
 *  should go through the primitive output methods of NExpr::Writer. Otherwise,
 *  the column counting and indentation will be screwed up.
 *
 *  This file gathers together all the explicit specialisations required for
 *  each base type.
 */

//! Specialisation of NExpr::Writer::write for "bool"
template <>
bool NExpr::Writer::write <bool> (const bool value)
{
  if (value) {
    output ("true");
  } else {
    output ("false");
  }
  return true;
}

//! Specialisation of Value::compose for "bool"
template <>
void NExpr::Value <bool>::compose (UtString *b) const
{
  if (mX) {
    *b << "true";
  } else {
    *b << "false";
  }
}

//! Specialisation of NExpr::Writer::write for "UInt32"
template <>
bool NExpr::Writer::write <UInt32> (const UInt32 value)
{
  *this << value;
  return true;
}

//! Specialisation of Value::compose for "UInt32"
template <>
void NExpr::Value <UInt32>::compose (UtString *b) const
{
  *b << mX;
}

// SourceLocators

//! Specialisation of NExpr::Writer::write for "SourceLocator"
template <>
bool NExpr::Writer::write <SourceLocator> (const SourceLocator loc)
{
  UtString b;
  loc.compose (&b);
  *this << '"' << b << '"';
  return true;
}

//! Specialisation of Value::compose for "SourceLocator"
template <>
void NExpr::Value <SourceLocator>::compose (UtString *b) const
{
  mX.compose (b);
}

//! Specialisation of NExpr::Writer::write for "StringAtom *"
template <>
bool NExpr::Writer::write <StringAtom> (const StringAtom *x)
{
  *this << '"' << x->str () << '"';
  return true;
}

//! Specialisation of PointerValue::compose for "StringAtom"
template <>
void NExpr::PointerValue <StringAtom>::compose (UtString *b) const
{
  *b << mX->str ();
}

//! Specialisation of NExpr::Writer::write for STAliasedLeafNodes *
/*! \note This is an interim hack. STAliasedLeafNodes really must be realised
 *  as a class. During initial development, just treat the symbol table node as
 *  a name to be written. This allows us to write simple Nucleus N-expressions
 *  before bringing symbol tables into the N-expression universe.
 */
template <>
bool NExpr::Writer::write <STAliasedLeafNode> (const STAliasedLeafNode *x)
{
  *this << '"' << x->strObject ()->str () << '"';
  return true;
}

//! Specialisation of PointerValue::compose for "STAliasedLeafNode"
template <>
void NExpr::PointerValue <STAliasedLeafNode>::compose (UtString *b) const
{
  *b << mX->strObject ()->str ();
}

//! Specialisation of NExpr::Writer::write for "NetFlags"
template <>
bool NExpr::Writer::write <NetFlags> (const NetFlags flags)
{
  char buffer [32];
  snprintf (buffer, sizeof (buffer), "0x%08x", flags);
  *this << buffer;
  return true;
}

//! Specialisation of Value::compose for "NetFlags"
template <>
void NExpr::Value <NetFlags>::compose (UtString *b) const
{
  char buffer [32];
  snprintf (buffer, sizeof (buffer), "0x%08x", mX);
  *b << buffer;
}

//! Specialisation of NExpr::Writer::write for "Strength"
template <>
bool NExpr::Writer::write <Strength> (const Strength strength)
{
  switch (strength) {
  case eStrPull:
    output ("pull");
    return true;
    break;
  case eStrDrive:
    output ("drive");
    return true;
    break;
  case eStrTie:
    output ("tie");
    return true;
    break;
  }
  output ("???");
  return false;
}

//! Specialisation of Value::compose for "Strength"
template <>
void NExpr::Value <Strength>::compose (UtString *b) const
{
  switch (mX) {
  case eStrPull:
    *b << "pull";
    break;
  case eStrDrive:
    *b << "drive";
    break;
  case eStrTie:
    *b << "tie";
    break;
  default:
    *b << "???";
    break;
  }
}

//! Specialisation of NExpr::Writer::write for "NUOp::OpT"
template <>
bool NExpr::Writer::write <NUOp::OpT> (const NUOp::OpT op)
{
  *this << '"' << NUOp::convertOpToChar (op) << '"';
  return true;
}

//! Specialisation of Value::compose for "NUOp::OpT"
template <>
void NExpr::Value <NUOp::OpT>::compose (UtString *b) const
{
  *b << NUOp::convertOpToChar (mX);
}


