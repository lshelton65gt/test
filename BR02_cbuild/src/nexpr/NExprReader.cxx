// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdarg.h>
#include "util/AtomicCache.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtMap.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/CbuildMsgContext.h"
#include "nexpr/NExpr.h"
#include "compiler_driver/CarbonContext.h"

#if NEXPR_ENABLE
#include "nexpr/NExprLex.h"
#endif

// NExpr::Reader
// *************

//! \class NExpr::ObjectMap
/*! Maps integer labels to pointers. When a labelled N-expression is read and
 *  NExpr with a label is construted. When object is built from the labelled
 *  NExpr a mapping from the label to that object gets added to the object map.
 */
class NExpr::ObjectMap : public UtHashMap <UInt32, void *> {
public:
  ObjectMap () : UtHashMap <UInt32, void *> () {}
  virtual ~ObjectMap () {}
};

//! \class NExpr::ForwardReferenceList
/*! When constructing objects from N-expressions, forward references occur when
 *  a field of an N-expression is bound to a label that is not yet
 *  defined. When a forward reference occurs, the numeric label is written into
 *  the field and an entry is added to the forward reference list. The location
 *  is also added to the list for use in error messages for undefined forward
 *  references.
 */
class NExpr::ForwardReferenceList : public UtList <std::pair <const SourceLocator, void *> > {
public:

  ForwardReferenceList () : UtList <std::pair <const SourceLocator, void *> > () {}
  virtual ~ForwardReferenceList () {}

  //! Add a undefined label reference to the worklist.
  void add (const SourceLocator loc, void *object)
  {
    push_back (std::pair <const SourceLocator, void *> (loc, object));
  }

  //! Patch all the unresolved references
  /*! This method walks the forward reference list. For forward references that
   *  have been defined, the label value in the field is overwritten with the
   *  address of the object that the label denotes. For undefined forward
   *  referecnes, and error message is output and the field gets deadbeef'd.
   */
  bool patch (NExpr::Reader &reader, const NExpr::ObjectMap &map) const
  {
    bool status = true;
    for (const_iterator it = begin (); it != end (); it++) {
      ObjectMap::const_iterator found;
      void *object = it->second;
      const UInt32 label = *(reinterpret_cast <UInt32 *> (object));
      if ((found = map.find (label)) == map.end ()) {
        reader.YYError (it->first, "no object defined for label @%d.", label);
        *(reinterpret_cast <void **> (object)) = NULL;
        status = false;
      } else {
        // replace the label of the forward reference with the address of the object
        *(reinterpret_cast <void **> (object)) = found->second;
      }
    }
    return status;
  }

};

//! ctor for the reader
NExpr::Reader::Reader (CarbonContext *ctx, const UInt32 flags) :
  mFlags (flags), mFilename (NULL), mNErrors (0), 
  mMsgContext (ctx->getMsgContext ()), 
  mSourceLocators (ctx->getSourceLocatorFactory ()), 
  mStrings (ctx->getAtomicCache ()), 
  mNetRefFactory (ctx->getNetRefFactory ()),
  mBuildDepth (0)
{
  mLabels = new ObjectMap;
  mForward = new ForwardReferenceList;
#if NEXPR_ENABLE
  NExpr__flex_debug = (mFlags & cLLDEBUG);
#endif
}

/*virtual*/ NExpr::Reader::~Reader ()
{
  delete mLabels;
  delete mForward;
}

//! Patch all the labels
bool NExpr::Reader::patchLabels ()
{
  return mForward->patch (*this, *mLabels);
}

//! Lookup a label in the map
bool NExpr::Reader::lookupLabel (const UInt32 label, void **object) const
{
  ObjectMap::const_iterator found;
  if (label == 0) {
    *object = NULL;                     // 0 always denotes NULL
    return true;
  } else if ((found = mLabels->find (label)) != mLabels->end ()) {
    *object = found->second;
    return true;
  } else {
    return false;
  }
}

//! Enter a label into the map
bool NExpr::Reader::addLabel (const UInt32 label, void *object, UtString *errmsg) const
{
  ObjectMap::iterator found = mLabels->find (label);
  if (found != mLabels->end ()) {
    prepare (errmsg);
    *errmsg << "@" << label << " is a duplicate label.";
    return false;
  } else {
    mLabels->insert (ObjectMap::value_type (label, object));
    return true;
  }
}

//! Add a pointer to the patch worklist
void NExpr::Reader::needsPatch (const SourceLocator loc, void *object) const
{
  mForward->add (loc, object);          // mForward is mutable
}

//! Read a nested object from an N-expression
/*! \param nx the NExpr instance that denotes the N-expression
 *  \param name the name of the field containing the nested object
 *  \param type_index the required type of the nested object
 *  \param label_ok iff a forward reference is permitted
 *  \param field the addres of the field in the object
 *  \param errmsg returned error message
 *  \return iff an object or allowed forward reference was found
 *
 *  \note This method looked for nested objects in the NExpr instance and
 *  writes either the address of the constructed object, or the label
 *  number of a forward reference into the field. If an allowed forward reference is
 *  found, then it is added to the readers worklist of forward references
 *  to patch.
 */
#if !NEXPR_ENABLE
bool NExpr::Reader::readObject (const NExpr &, const char *, const UInt32, bool, void **, 
  UtString *errmsg) const
{
  // The NEXPR_ENABLE=1 implementation induces a reference to Value<UInt32>
  *errmsg << "N-expressions are disabled.";
  return false;
}
#else
bool NExpr::Reader::readObject (const NExpr &nx, const char *name, 
  const UInt32 type_index, bool forward_is_ok, void **field, UtString *errmsg) const
{
  Field *attribute;
  const NestedValue *nested;
  const Value <UInt32> *label;
  if (!nx.get (name, &attribute, errmsg)) {
    // the field was not in the N-expression
    return false;
  } else if ((label = dynamic_cast <const Value <UInt32> *> (attribute->getValue ())) != NULL
    && lookupLabel (label->getValue (), reinterpret_cast <void **> (field))) {
    // it was a label and the reader already knowns about this object
    return true;
  } else if (label != NULL && !forward_is_ok) {
    // it was a label but we are not permitted to use a label here
    *errmsg << "a label is not permitted for the " << name << " field in this context";
    return false;
  } else if (label != NULL && forward_is_ok) {
    // smash the integer label into the pointer field
    *(reinterpret_cast <UInt32 *> (field)) = label->getValue ();
    needsPatch (label->getLoc (), static_cast <void *> (field));
    return true;
  } else if ((nested = dynamic_cast <const NestedValue *> (attribute->getValue ())) == NULL) {
    // the field was neither a label nor a nested N-expression
    *errmsg << "expected a label or a nested N-expression for " << name << "; found "
            << attribute->getValue ()->getType ();
    return false;
  } else if (!build (*nested->getNExpr (), reinterpret_cast <void **> (field), type_index, errmsg)) {
    return false;
  } else {
    return true;
  }
}
#endif

#if NEXPR_ENABLE
//! Dump a list of the field type for all classes.
/*! \note This is called from tripWire. The output goes into a test gold file
 *  for regression.
 */
/*static*/ void NExpr::Reader::dumpFieldTypes (UtOStream &out)
{
  for (UInt32 type = 0; type < sNClasses; type++) {
    for (UInt32 field = 0; field < sNFieldNames; field++) {
      UInt32 fieldtype;
      if (getType (type, field, &fieldtype)) {
        out << typeName (type) << "." << fieldName (field) << " is a " << typeName (fieldtype);
        UInt32 token;
        if (expectedToken (fieldtype, &token)) {
          out << " (token " << tokenName (token) << ")";
        }
        out << "\n";
      }
    }
  }
}
#else
/*static*/ void NExpr::Reader::dumpFieldTypes (UtOStream &)
{
  // The enabled code, while it is sort-of correct when N-expressions are
  // disabled induces a GCC warning because sNClasses is zero so the
  // loop comparison always returns false.
}
#endif

//! Output an N-expression error message.
void NExpr::Reader::YYError (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  vYYError (loc, fmt, ap);
  va_end (ap);
}

void NExpr::Reader::vYYError (const SourceLocator loc, const char *fmt, va_list ap)
{
  char buffer [512];
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  mMsgContext->NXYYError (&loc, buffer);
  mNErrors++;
}

//! Output an N-expression warning message
void NExpr::Reader::YYWarn (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  vYYWarn (loc, fmt, ap);
  va_end (ap);
}

void NExpr::Reader::vYYWarn (const SourceLocator loc, const char *fmt, va_list ap)
{
  char buffer [512];
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  mMsgContext->NXYYWarn (&loc, buffer);
}

//! Output an N-expresion information message
void NExpr::Reader::YYInfo (const SourceLocator loc, const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  vYYInfo (loc, fmt, ap);
  va_end (ap);
}

void NExpr::Reader::vYYInfo (const SourceLocator loc, const char *fmt, va_list ap)
{
  char buffer [512];
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  mMsgContext->NXYYInfo (&loc, buffer);
}

//! Intern a symbol
StringAtom *NExpr::Reader::intern (const char *s)
{
  return mStrings->intern (s);
}


