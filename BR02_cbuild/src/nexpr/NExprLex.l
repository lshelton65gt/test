%{
/******************************************************************************
 Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include "util/AtomicCache.h"
#include "util/StringAtom.h"
#include "util/SourceLocator.h"
#include "util/UtIOStream.h"
#include "nexpr/NExpr.h"
#include "NExprLex.h"

#if __GNUC__
#define UNUSED __attribute__ ((unused));
#else
#define UNUSED
#endif

//! This scanner will take an expected terminal as an input parameter
#define YY_DECL int NExpr::Reader::lex (int expecting, Lexeme *value)

//! Use YY_USER_ACTION to initialise the location in the Lexeme
#define YY_USER_ACTION { \
  SourceLocator loc; \
  create (NExpr_lineno, &loc); \
  value->clear (); \
  value->set (loc); \
}

//! Return an enumeration value
#define RETURNENUM(_value_, _token_) { \
  BaseValue *basevalue = new NExpr::Value <UInt32> (value->getLoc (), _value_); \
  value->Return (_token_, basevalue); \
  if (!NExpr__flex_debug) { \
  } else { \
    UtString b; \
    basevalue->compose (&b); \
    UtIO::cout () << "--returning enum " << NExpr::typeName (_token_) \
      << ": " << yytext << " = " << b.c_str () << "\n"; \
  } \
  return _token_; \
}
  
//! Return a value
#define RETURNVALUE(_type_, _value_, _token_) { \
  BaseValue *basevalue = new NExpr::_type_ (value->getLoc (), _value_); \
  value->Return (_token_, basevalue); \
  if (!NExpr__flex_debug) { \
  } else { \
    UtString b; \
    basevalue->compose (&b); \
    UtIO::cout () << "--returning " << #_token_ << ": " << b.c_str () << "\n"; \
  } \
  return _token_; \
}
  
//! Return a token
#define RETURN(_token_) { \
  value->Return (_token_); \
  if (!NExpr__flex_debug) { \
  } else { \
    UtIO::cout () << "--returning " << #_token_ << "\n"; \
  } \
  return _token_; \
}

#define RETURN2(_token_, _value_) { \
  value->Return (_token_, _value_); \
  if (!NExpr__flex_debug) { \
  } else if (_token_ == eIDENT) { \
    UtIO::cout () << "--returning " << #_token_ << ": " << value->getSymbol ()->str () << "\n"; \
  } else if (_token_ == eINTEGER) { \
    UtIO::cout () << "--returning " << #_token_ << ": " << value->getInteger () << "\n"; \
  } else { \
    UtIO::cout () << "--returning " << #_token_ << "\n"; \
  } \
  return _token_; \
}

//! Enter a state - like BEGIN but tracable
#define ENTER(_state_) { \
  if (NExpr__flex_debug) { \
    UtIO::cout () << "--entering " << #_state_ << "\n"; \
  } \
  BEGIN (_state_); \
}

static int saved_state = 0;

// convert an number in a unsigned hexadecimal to an int
static UInt32 htoi (const char *yytext);

%}

/* To avoid isatty problems on Windows. */
%option never-interactive
%option yylineno
%option noyywrap
%option nodefault
%option debug

%x ERROR
%x COMMENT0 COMMENT1
%x STRING0

%s ENUM0
%x BOOL0
%x UINT32

%%
  /* Code at the beginning of the flex rules section is used to initialise the scanner
     to search for an expected token. If a specific terminal is expected then its code
     is passed as the expecting argument to yylex. If no specific terminal is
     provided then the scanner is forced into the INITIAL state. */
  if (NExpr::isEnum (expecting)) {
    ENTER (ENUM0);
  } else {
    switch (expecting) {
    case eBOOL:
      ENTER (BOOL0);
      break;
    case eUINT32:
      ENTER (UINT32);
      break;
    default:
      ENTER (INITIAL);
      break;
    }
  }

  /* enumeration values */
<ENUM0>[a-zA-Z_][a-zA-Z_0-9]* {
    UInt32 enumvalue;
    INFO_ASSERT (NExpr::isEnum (expecting), "scanner jammed");
    if (NExpr::lookupEnum (expecting, yytext, &enumvalue)) {
      RETURNENUM (enumvalue, expecting);
    } else {
      NExpr_error ("\"%s\" is not a legitimate value for enum %s.",
	yytext, NExpr::typeName (expecting));
    }
  }

<ENUM0>[0-9][0-9]* { 
    UInt32 enumvalue = atoi (yytext);
    RETURNENUM (enumvalue, expecting); 
  }

<ENUM0>0[xX][0-9a-fA-F][0-9a-fA-F]* { 
    UInt32 enumvalue = htoi (yytext);
    RETURNENUM (enumvalue, expecting); 
  }

  /* field and type identifiers */
<INITIAL>[a-zA-Z_][a-zA-Z0-9_]* {
    RETURN2 (eIDENT, intern (yytext));
  } 

  /* integers that are UInt32 base types */
<UINT32>[0-9][0-9]* { RETURNVALUE (Value <UInt32>, atoi (yytext), eUINT32); }

  /* integers */
[0-9][0-9]*        { RETURN2 (eINTEGER, atoi (yytext)); }
0[xX][0-9a-fA-F][0-9a-fA-F]* { RETURNENUM (htoi (yytext), expecting); }


  /* strings */
\"               { ENTER (STRING0); }
<STRING0>[^\n"]* { value->set (intern (yytext)); }
<STRING0>\"      { ENTER (INITIAL); RETURN (eSTRING_ATOM); }
eq<STRING0>\n      { NExpr_error ("String extends beyond end of line."); ENTER (INITIAL); }


  /* C++ style comments */
"//"               { saved_state = YY_START; ENTER (COMMENT0); }
<COMMENT0>.        { /* consume the comment */ }
<COMMENT0>\n       { ENTER (saved_state); }

  /* C style comments */
"/*"               { saved_state = YY_START; ENTER (COMMENT1); }
<COMMENT1>"*/"     { ENTER (saved_state);  }
<COMMENT1>.        { /* consume the comment */ }
<COMMENT1>\n       { /* consume the comment */ }

  /* Whitespace */
[ \t]      { /* consume whitespace */ }
\n         { /* consume newlines */ }

  /* to add another literal type to the scanner:
     1. Define start conditions for the "subscanner" that recognised
        the literal. Use %s states so that the comment and whitespace
        rules will be used in that state (see "man flex" and search
        for inclusive start conditions).
     2. Add a branch to the switch at the start of the rules section
        to intialise the scanner state to the subscanner that
        recognises the new literal.
     3. Write the rules to construct the value. Use the RETURNVALUE
        macro, if possible, to return the new value. If RETURNVALUE
        does not work, then please tweak it or construct another
        similar macro that supports tracing.
     4. The expecting switch will put the scanner into the appropriate
        state on the next call to the scanner so there is no need to
        return the scanner state before returning.
   */

  /* boolean values */
<BOOL0>"true"      { RETURNVALUE (Value <bool>, true, eBOOL); }
<BOOL0>"false"     { RETURNVALUE (Value <bool>, false, eBOOL); }
<BOOL0>"TRUE"      { RETURNVALUE (Value <bool>, true, eBOOL); }
<BOOL0>"FALSE"     { RETURNVALUE (Value <bool>, false, eBOOL); }

  /* operators */
"("        { RETURN ('('); }
")"        { RETURN (')'); }
"@"        { RETURN ('@'); }
"%"        { RETURN ('%'); }
":"        { RETURN (':'); }
"::"       { RETURN (eCOLONCOLON); }

  /* end of file */
<<EOF>>    { RETURN (0); }

  /* State entered after an "unexpected character" error. Just consume
     characters until end of line, a parenthesis or a colon. */
<ERROR>\n   { ENTER (INITIAL); }
<ERROR>"/*" { saved_state = INITIAL; ENTER (COMMENT1); }
<ERROR>"("  { unput (*yytext); ENTER (INITIAL); }
<ERROR>")"  { unput (*yytext); ENTER (INITIAL); }
<ERROR>":"  { unput (*yytext); ENTER (INITIAL); }
<ERROR>.    { /*consume the character*/ }

  /* anything else is bogus */
. { 
    char c = *yytext;
    if (isprint (c) && expecting != 0) {
      // unmatched character, and we were expecting something specific
      NExpr_error ("unrecognised character '%c', expecting a %s", c, NExpr::typeName (expecting));
    } else if (isprint (c) && expecting == 0) {
      // unmatched character, and no expected type
      NExpr_error ("unrecognised character '%c'", c);
    } else if (expecting != 0) {
      // unmatched character, and we were expecting something specific
      NExpr_error ("unrecognised character \\x%02x, expecting a %s", c, NExpr::typeName (expecting));
    } else {
      // unmatched character, and no expected type
      NExpr_error ("unrecognised character: \\x%02x", (int) c);
    }
    // If we were expecting any specific token, then push the current character
    // back onto the input stream. I'm guessing this would give better error
    // recovery than just throwing it out...
    if (expecting != 0) {
      unput (*yytext);
    }
    ENTER (ERROR);
  }

%%

void NExpr::Reader::NExpr_error (const char *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  SourceLocator loc;
  create (NExpr_lineno, &loc);
  vYYError (loc, fmt, ap);
  va_end (ap);  
}

const char *NExpr::Reader::Lexeme::expected (const UInt32 expected, UtString *errmsg) const
{
  UtString b0, b1;
  *errmsg << "expected " << compose (expected, &b0) << ", found " << compose (mToken, &b1);
  return errmsg->c_str ();
}

static UInt32 htoi (const char *yytext)
{
  INFO_ASSERT (yytext [0] == '0', "scanner jammed");
  INFO_ASSERT (yytext [1] == 'x' || yytext [1] == 'X', "scanner jammed");
  UInt32 value = 0;
  UInt32 n = 2;
  while (yytext [n] != '\0') {
    switch (yytext [n]) {
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      value = 16 * value + (int) yytext [n] - (int) '0';
      break;
    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
      value = 16 * value + (int) yytext [n] - (int) 'a' + 10;
      break;
    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
      value = 16 * value + (int) yytext [n] - (int) 'A' + 10;
      break;
    default:
      INFO_ASSERT (isxdigit (yytext [n]), "scanner jammed");
      break;
    }
    n++;
  }
  return value;
}
