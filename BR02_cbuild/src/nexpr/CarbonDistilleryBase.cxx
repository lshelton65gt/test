// -*- C++ -*-
/******************************************************************************
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************
*/

#include "util/AtomicCache.h"
#include "nexpr/NExpr.h"
#include "nexpr/CarbonDistilleryBase.h"

CarbonDistilleryBase::CarbonDistilleryBase (AtomicCache *atomic_cache, MsgContext *msg_context) :
  mAtomicCache (atomic_cache), mMsgContext (msg_context)
{
}

