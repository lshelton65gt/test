// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _NEXPR_SCHEMA_H_
#define _NEXPR_SCHEMA_H_

#include "util/StringAtom.h"
#include "util/UtMap.h"
#include "util/UtMultiMap.h"
#include "util/UtList.h"
#include "util/CodeStream.h"
#include "util/IndentedStream.h"

class MsgContext;
class BURS;

#define YYLTYPE SourceLocator

class NExprSchema {
public:

  //! Bitmasks for options passed in the options ctor argument
  enum {
    eQUIET = 1<<0,                      //!< suppress warning messages
    eVERBOSE = 1<<1,                    //!< trace progress
    eNOHASHLINE = 1<<2                  //!< suppress #line in generated files
  };

  NExprSchema (MsgContext *msg_context, AtomicCache *symbols, 
    SourceLocatorFactory *sourcelocators, UInt32 options);

  //! Output an error message at a source location and increment the error counter
  /*! \note This uses vprintf-type arguments
   */
  void YYError (const SourceLocator, const char *fmt, va_list ap);

  //! Output an error message at a source location and increment the error counter
  /*! \note This is passed a line number and uses the name of the current input
   *  file to complete the source location.
   */
  void YYError (const int, const char *fmt, va_list ap);

  //! Output a warning at a source location.
  void YYWarn (const SourceLocator, const char *fmt, va_list ap);

  //! Output an information message tied to a source line.
  void YYInfo (const SourceLocator, const char *fmt, va_list ap);

#ifdef __GNUC__
#define PRINTF(_m_, _n_) __attribute__ ((__format__ (printf, _m_, _n_)))
#else
#define PRINTF(_m_, _n_)
#endif

  //! Output an error message tied to a source line.
  /*! \note This is the varargs form that uses printf-like arguments.
   */
  void Error (const SourceLocator, const char *fmt, ...) PRINTF (3,4);

  //! Output an error message with no source locator
  /*! \note This is the varargs form that uses printf-like arguments.
   */
  void Error (const char *fmt, ...) PRINTF (2,3);

  //! Output a warning tied to a source line.
  /*! \note This is the varargs form that uses printf-like arguments.
   */
  void Warn (const SourceLocator, const char *fmt, ...) PRINTF (3,4);

  //! Output an information message tied to a source line.
  /*! \note This is the varargs form that uses printf-like arguments.
   */
  void Info (const SourceLocator, const char *fmt, ...) PRINTF (3,4);

#undef PRINTF

  //! \return the number of errors
  UInt32 nErrors () const { return mNErrors; }

  //! Intern a string in the AtomicCache
  /*! Used by the scanner for identifiers */
  StringAtom* intern (const char *s) const
  { return mSymbols->intern (s); }

  StringAtom *intern (const char *s, size_t len)
  { return mSymbols->intern (s, len); }

  //! Create a source location in the current file
  void create (const int lineno, SourceLocator *loc)
  { *loc = mSourceLocators->create (mCurrentFile, lineno); }

  //! Parse a schema specification
  bool parse (const char *filename);

  //! Check the schema for internal consistency.
  bool check ();

  //! Allocate indices for the schema objects
  /*! The indices are used at N-expression runtime to represent types and tokens.
   */
  void allocateIndices ();

  //! Output everything.
  bool write (const char *dirname);

  //! \class Declarable
  /*! Objects in the schema are realised as instances of subclasses of class
   *  Declarable.
   */
  class Declarable {
  public:

    template <typename _T>
    class Map : public UtMap <StringAtom *, _T *, StringAtomCmp> {
    public:
      Map () : UtMap <StringAtom *, _T *, StringAtomCmp> () {}
      virtual ~Map () {}

      bool isDeclared (StringAtom *name, _T **object) const
      {
        if (UtMap <StringAtom *, _T*, StringAtomCmp>::find (name) 
          == UtMap <StringAtom *, _T *, StringAtomCmp>::end ()) {
          *object = NULL;
          return false;
        } else {
          *object = UtMap <StringAtom *, _T *, StringAtomCmp>::find (name)->second;
          return true;
        }
      }

      void declare (_T *object)
      {
        (*this) [object->getName ()] = object;
      }

    };

    enum {
      eDEFINED = 1<<0,
      eUNDECLARED = 1<<1,
      // bits 24-31 are used 
      eMARK = 1<<31,                    //!< an ephemeral mark
    };

    Declarable (NExprSchema *schema, const SourceLocator loc, StringAtom *name) : 
      mSchema (schema), mState (0), mIndex (0), mDeclared (loc), mDefined (), mName (name)
    {}

    virtual ~Declarable () {}

    //! \return the enclosing schema
    NExprSchema *getSchema () const { return mSchema; }

    bool isUndeclared () const { return mState & eUNDECLARED; }
    bool isDefined () const { return mState & eDEFINED; }

    void setUndeclared () { mState |= eUNDECLARED; }

    void setDecl (const SourceLocator loc)
    {
      mDeclared = loc;
      mState &= ~eUNDECLARED;
    }

    void Define (const SourceLocator loc)
    {
      mDefined = loc;
      mState |= eDEFINED;
    }

    StringAtom *getName () const { return mName; }
    const SourceLocator getDecl () const { return mDeclared; }
    const SourceLocator getDefn () const { return mDefined; }

    //! Set the index of the class
    void setIndex (const UInt32 index) { mIndex = index; }

    //! \return the index of the class
    UInt32 getIndex () const { return mIndex; }

    //! Set the ephemeral mark
    void setMark () { mState |= eMARK; }
    
    //! Clear the ephemeral mark
    void clearMark () { mState &= ~eMARK; }

    //! Test the mark
    bool isMarked () const { return mState & eMARK; }

    //! Checking for the object
    virtual bool check (NExprSchema *) { return false; }

  private:

    mutable NExprSchema *mSchema;       //!< the enclosing schema
    UInt32 mState;                      //!< status bits
    UInt32 mIndex;                      //!< integer index allocated to the object
    SourceLocator mDeclared;            //!< declaration point
    SourceLocator mDefined;             //!< definition point
    StringAtom *mName;                  //!< declared name
    
  };

  class ClassType;                      // forward decl
  class BaseType;
  class VectorType;
  class Field;
  class Code;

  //! \class API
  /*! Classes and base types of the schema may be tagged for membership in an
   *  API. In the schema, the API is represented by an identifier. In the
   *  internal representation a bitmask is associated with declarables.
   */
  class API : public Declarable {
  public:

    // Options for APIs
    enum {
      eIS_DISTILLERY = 1<<0             //!< this is the distillery interface
    };

    typedef Declarable::Map <API> Map;

    // Allow reference to APIs via aliases. This allows an alias to be
    // declared as "Population", but used as "pop"
    typedef UtMap <StringAtom *, API *> AliasMap;

    //! ctor for API descriptors
    API (NExprSchema *schema, const SourceLocator loc, StringAtom *name);

    //! Set properties of the API.
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return iff this has the eIS_DISTILLERY property
    bool isDistillery () const { return mFlags & eIS_DISTILLERY; }

  private:

    enum {
      cMAX_APIS = 32                    //!< use a bit mask in Declarable to track APIs
    };

    UInt32 mFlags;                      //!< option bits
    UInt32 mMaskBit;                    //!< position in the api mask in declarables
  };

  //! \return the number of APIs defined
  UInt32 getNumAPIs () const { return mAPIs.size (); }

  bool isDeclared (StringAtom *name, API **api) const
  { 
    API::AliasMap::const_iterator found;
    if ((found = mAPIAliases.find (name)) != mAPIAliases.end ()) {
      *api = found->second;
      return true;
    } else {
      return false;
    }
  }

  void declare (API *api);

  void alias (StringAtom *name, API *api)
  { mAPIAliases.insert (API::AliasMap::value_type (name, api)); }

  //! \class IncludeFile
  /*! Representation of a file that must be #included in the generated code
   */
  class IncludeFile : public Declarable {
  public:

    // Options for include files
    enum {
      eBASE = 1<<0,                     //!< the include file is for a base type
    };

    typedef Declarable::Map <IncludeFile> Map;
    typedef UtList <IncludeFile *> List;

    IncludeFile (NExprSchema *schema, const SourceLocator loc, StringAtom *path) :
      Declarable (schema, loc, path)
    {}

    //! Checking for the object
    /*! No specific checking for include files */
    virtual bool check (NExprSchema *) { return true; }

    //! Set properties of the field.
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return iff this is for a base type
    bool isBase () const { return mFlags & eBASE; }

  private:
    UInt32 mFlags;                      //!< mask of option bits
  };

  //! \class Token
  /*! Represntation of a scanner token name for a base type
   */
  class Token : public Declarable {
  public:

    enum {
      eBOUND_TO_VALUE = 1<<0            //!< token is bound to a base type
    };

    typedef Declarable::Map <Token> Map;
    typedef UtList <Token *> List;

    Token (NExprSchema *schema, const SourceLocator loc, StringAtom *token, const UInt32 flags) :
      Declarable (schema, loc, token), mFlags (flags)
    {}

    virtual ~Token () {}

    //! Checking scanner tokens
    virtual bool check (NExprSchema *) { return true; }

  private:

    UInt32 mFlags;

  };

  //! \class Type
  /*! This is the parent class for the Schema object that model types.
   */

  class Type : public Declarable {
  public:

    typedef Declarable::Map <Type> Map;

    Type (NExprSchema *schema, SourceLocator loc, StringAtom *name) : 
      Declarable (schema, loc, name) {}
    virtual ~Type () {}

    virtual bool canPoint () const { return true; }

    virtual bool isBaseType () const { return false; }
    virtual bool isClassType () const { return false; }
    virtual bool isVectorType () const { return false; }
    virtual bool isErrorType () const { return false; }
    virtual bool isPointer () const { return false; }

    virtual const ClassType *castClassType () const { return NULL; }
    virtual const BaseType *castBaseType () const { return NULL; }
    virtual const VectorType *castVectorType () const { return NULL; }

    //! Write a write method for fields of this type.
    virtual bool writeWrite (CodeStream &, const Field *) = 0;

    //! Write a read method for fields of this type.

    enum {
      eUSE_STATUS = 1<<0,               //!< set "status" rather than returning status
      eIS_READER = 1<<1,                //!< in a Reader method
      eLABEL_IS_OK = 1<<2               //!< a label is acceptable
    };

    virtual bool writeRead (CodeStream &, const Field *, const char *, const char *, 
      const UInt32 ctx = 0) = 0;

    virtual bool writeMask (CodeStream &) const;

  };

  //! \class ErrorType
  /*! An error type instance is create by the compiler when an undeclared type
   *  error is emitted. It just makes things a little easier to implement when
   *  the type reduction in the parser always returns non-NULL.
   */

  class ErrorType : public Type {
  public:
    
    typedef Declarable::Map <ErrorType> Map;
    
    ErrorType (NExprSchema *schema, SourceLocator loc, StringAtom *name) : 
      Type (schema, loc, name) {}
    virtual ~ErrorType () {}

    virtual bool isErrorType () const { return true; }

    // Check method always fails for error types
    virtual bool check (NExprSchema *) { return false; }

    //! Nothing to write for error fields
    virtual bool writeWrite (CodeStream &, const Field *) { return false; }

    //! Nothing to write for error fields
    virtual bool writeRead (CodeStream &, const Field *, const char *, const char *, const UInt32 = 0) 
    { return false; }

  };

  //! \class BaseType
  /*! Representation of a base type appearing in the schema as
   *  <verbatim>
   *    type Foo;
   *  </verbaim>
   */

  class BaseType : public Type {
  public:

    enum {
      eNONTERMINAL = 1<<0,              //!< a special type that cannot have a token
      ePOINTER = 1<<1,                  //!< fields of this type should be pointers
      eCHOICE = 1<<2,                   //!< the type is a choice enumeration
      eMASK = 1<<3,                     //!< the type is an enum used as a mask
      eENUM = 1<<4                      //!< the type is an enumeration
    };

    typedef Declarable::Map <BaseType> Map;

    BaseType (NExprSchema *schema, const SourceLocator loc, StringAtom *name, UInt32 flags) :
      Type (schema, loc, name), mFlags (flags), mToken (NULL), mAlias (NULL), mCType (NULL)
    {}

    virtual ~BaseType () {}

    virtual bool isBaseType () const { return true; }
    virtual const BaseType *castBaseType () const { return this; }

    //! \return the C++ type
    StringAtom *getCType () const { return mCType; }

    //! Set the C++ type for this base type
    void setCType (StringAtom *ctype) { mCType = ctype; }

    //! \return the ctype if defined, or the name
    StringAtom *getTypeName () const { return mCType != NULL ? mCType : getName (); }

    //! Set an alias for this base type
    void setAlias (BaseType *type) { mAlias = type; }

    //! \return the alias for this base type
    BaseType *getAlias () const { return mAlias; }

    //! \return iff this type is aliased
    bool isAliased () const { return (mAlias != NULL); }

    //! Set properties of the base type
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return the property mask for this
    UInt32 getProperties () const { return mFlags; }

    //! \return iff this field is tagged as a pointer field
    virtual bool isPointer () const { return mFlags & ePOINTER; }

    //! \return iff this field is a choice enum
    virtual bool isChoice () const { return mFlags & eCHOICE; }

    //! \return iff this field is a masking enum
    virtual bool isMask () const { return mFlags & eMASK; }

    //! \return iff this field is an enum
    virtual bool isEnum () const { return mFlags & eENUM; }

    //! \return iff this field is tagged as a non terminal
    virtual bool isNonterminal () const { return mFlags & eNONTERMINAL; }

    //! Set the scanner token name for this base type
    /*! If the scanner token is set to FOO, then:
     *  - #define _FOO_ _index_ is added to the the NExprLex.h file, then
     *    scanner is expected to construct an appropriate Value<_t> object and
     *    return _FOO_ when it recognises a token for this base type.
     */
    void setToken (Token *token) { mToken = token; }

    //! \return the scanner token for this type
    Token *getToken () const { return mToken; }

    //! Write the get method for the base type.
    /*! The get method extracts a basetype value from an N-expression field.
     *  For example, the bool get method looks like:
     *  <verbatim>
     *  template <>
     *  bool NExpr::get <bool> (const char *name, bool *value, UtString *errmsg) const
     *  </verbatim>
     */
    bool writeGet (CodeStream &out);

    //! Write a write method for fields of this type.
    virtual bool writeWrite (CodeStream &, const Field *);

    //! Write a read method for BaseType values
    virtual bool writeRead (CodeStream &, const Field *, const char *, const char *, const UInt32 = 0);

    //! Output the type mask for an enum type
    virtual bool writeMask (CodeStream &) const;

    //! Checking base types.
    virtual bool check (NExprSchema *);

  protected:
    
    UInt32 mFlags;                      //!< properties of the base type
    Token *mToken;                      //!< the scanner token for this base type
    BaseType *mAlias;                   //!< the alias type
    StringAtom *mCType;                 //!< the name of the C++ type

  };

  //! \class EnumType
  /*! Representation of an enumeration declared in the schema. */
  class EnumType : public BaseType {
  public:
    
    typedef Declarable::Map <EnumType> Map;

    EnumType (NExprSchema *schema, const SourceLocator loc, StringAtom *name, UInt32 flags) :
      BaseType (schema, loc, name, flags)
    {
    }

    virtual ~EnumType () {}

    //! Declare an enum value inside this enum
    void declareEnumValue (NExprSchema *schema, const SourceLocator loc, StringAtom *name, UInt32 value);

    //! Checking enum types.
    virtual bool check (NExprSchema *) { return true; }

    //! Output the value table for the enumeration type
    UInt32 writeTable (CodeStream &, const UInt32) const;

    //! Convert the name to something that may be used in a C++ identifier
    void foldName (UtString *name);

    //! Write the mask for an enumeration type
    virtual bool writeMask (CodeStream &) const;

  private:

    typedef UtMap <StringAtom *, UInt32, StringAtomCmp> EnumValueMap;
    typedef UtMap <StringAtom *, SourceLocator> EnumLocationMap;

    EnumValueMap mValues;
    EnumLocationMap mLocations;

  };

  //! \class VectorType
  /*! Representation of a vector type. A common idiom in cbuild is to use a
   *  typedef to declare a vector type and then class members are either of
   *  that vector type, or a pointer to an instance of that type. This idiom is
   *  modelled by VectorType.
   */

  class VectorType : public Type {
  public:

    typedef Declarable::Map <VectorType> Map;

    enum {
      ePOINTER_ELEMENT = 1<<0,          //!< element is a pointer
    };

    VectorType (NExprSchema *schema, SourceLocator loc, StringAtom *name, Type *element_type, 
      const UInt32 flags) :
      Type (schema, loc, name), mFlags (flags), mElementType (element_type)
    {}

    virtual ~VectorType () {}

    virtual bool isVectorType () const { return true; }
    virtual const VectorType *castVectorType () const { return this; }

    //! \return iff the element is a pointer
    bool isPointerElement () const { return mFlags & ePOINTER_ELEMENT; }

    //! \return the element type
    Type *getElementType () const { return mElementType; }

    //! Checking for vector types
    virtual bool check (NExprSchema *);

    //! Write a write method for Vector values
    virtual bool writeWrite (CodeStream &, const Field *);

    //! Write a read method for Vector values
    virtual bool writeRead (CodeStream &, const Field *, const char *, const char *, const UInt32 = 0);

  private:
    
    UInt32 mFlags;
    Type *mElementType;                 //!< the element type
  };

  //! \class Field
  /*! Representation of a field in a class
   */
  class Field : public Declarable {
  public:

    enum {
      eDEBUGPRINT = 1<<0,               //!< call pr on the field after writing
      ePOINTER    = 1<<1,               //!< the field is declared as type *ident;
      eVECTOR     = 1<<2,               //!< the field is declared as a vector
      eELEMENT    = 1<<3,               //!< the field is a vector element
      eNOTREAD    = 1<<4                //!< do not emit code in the read method
    };

    typedef UtList <Field *> List;
    typedef Declarable::Map <Field> Map;
    typedef UtMultiMap <StringAtom *, Field *, StringAtomCmp> MultiMap;

    Field (NExprSchema *schema, SourceLocator loc, StringAtom *name, Type *type, 
      const UInt32 flags, StringAtom *pname = NULL) :
      Declarable (schema, loc, name), mFlags (flags), mType (type), mClass (NULL),
      mPName (pname == NULL ? name : pname)
    {}

    virtual ~Field () {}

    Type *getType () const { return mType; }

    //! Set properties of the field.
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return the property mask for this
    UInt32 getProperties () const { return mFlags; }

    //! \return iff this field is tagged for debugging
    bool isDebugPrint () const { return mFlags & eDEBUGPRINT; }

    //! \return iff this field is tagged as a pointer field
    bool isPointer () const { return mFlags & ePOINTER; }

    //! \return iff this field is a vector field
    bool isVector () const { return mFlags & eVECTOR; }

    //! \return iff this field is a vector element
    bool isElement () const { return mFlags & eELEMENT; }

    //! \return iff the eNOTREAD property is set
    bool isNotRead () const { return mFlags & eNOTREAD; }

    //! Checking fields.
    virtual bool check (NExprSchema *);

    //! Set the class to which the field belongs
    void setClass (ClassType *classtype) { mClass = classtype; }

    //! \return the class to which the field belongs
    ClassType *getClass () const { return mClass; }
    
    //! \return the name used in generated error messages about the field
    StringAtom *getPName () const { return mPName; }
    //! Output the write method for the field
    bool writeWrite (CodeStream &);

    //! Output the read method for the field
    bool writeRead (CodeStream &, const char *nexpr, const char *dst, const UInt32 = 0);

  private:

    UInt32 mFlags;
    Type *mType;
    ClassType *mClass;
    StringAtom *mPName;

  };

  
  //! \class ClassType
  /*! Representation of a class type.
   */
  class ClassType : public Type {
  public:

    //! \enum properties of the class type
    enum {
      // declared properties
      eABSTRACT = 1<<0,                 //!< the class is marked abstract
      eNESTED = 1<<1,                   //!< the class is marked nested
      eUNNESTED = 1<<2,                 //!< make this class unnested indpt of superclasses
      eUNWALKABLE = 1<<3,               //!< the class is marked unwalkable
      eUNBUILDABLE = 1<<4,              //!< the class is marked unbuildable
      eNOAPI = 1<<5,                    //!< the class should not appear in the API

      // property bits used during compilation
      eIS_NUCLEUS = 1<<29,
      eCYCLE_CHECKED = 1<<30,           //!< set when this class is checked for inheritence cycles

      // mask of the attributes that are inherited by subclasses
      eINHERITED = eNESTED|eUNWALKABLE|eIS_NUCLEUS|eUNBUILDABLE|eNOAPI

    };

    //! \enum directives for the class
    enum {
      eNONE = 0                         //!< no directives defined
    };

    typedef UtList <ClassType *> List;
    typedef UtList <const ClassType *> ConstList;
    typedef Declarable::Map <ClassType> Map;

    ClassType (NExprSchema *schema, const SourceLocator loc, StringAtom *name) :
      Type (schema, loc, name), mFlags (0), mSuperclasses ()
    {
      for (int i = 0; i < eN_CODES; i++) {
        mCode [i] = NULL;
      }
    }

    virtual ~ClassType () {}

    //! This really is a class type
    virtual bool isClassType () const { return true; }
    virtual bool isPointer () const { return true; }

    virtual const ClassType *castClassType () const { return this; }

    //! \return iff this is a base class (i.e. one with no superclasses)
    bool isBaseClass () const { return mSuperclasses.empty (); }

    //! \return iff this is a direct subclass of parent
    bool isDirectSubclass (const ClassType *type) const;

    //! Set the list of superclasses for this class
    void setSuperclasses (List *superclasses);

    //! Iteration over the list of superclasses
    List::iterator superbegin () { return mSuperclasses.begin (); }
    List::iterator superend ()   { return mSuperclasses.end (); }

    //! Iteration over the fields
    Field::Map::const_iterator fieldbegin () const { return mFields.begin (); }
    Field::Map::const_iterator fieldend ()   const { return mFields.end (); }

    //! \return iff the class has any fields
    bool hasFields () const { return !mFields.empty (); }

    //! Test the eNESTED property bit
    bool isNested () const { return mFlags & eNESTED; }

    //! Test whether this is a nucleus class
    bool isNucleus () const { return mFlags & eIS_NUCLEUS; }

    //! Test whether this class cannot be built
    bool isUnbuildable () const { return mFlags & eUNBUILDABLE; }

    //! Test whether this class is in the API
    bool isInApi () const { return (mFlags & eNOAPI) == 0; }

    //! Test the eACYCLIC property bit
    bool checkedForCycles () const { return mFlags & eCYCLE_CHECKED; }

    //! Set the properties of the class
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! Get the properties of the class
    UInt32 getProperties () const { return mFlags; }

    //! Test the eABSTRACT property bit
    bool isAbstract () const { return mFlags & eABSTRACT; }

    //! \return iff a named field is declared
    /*! \note if the field is declared a handle is returned in field
     */
    bool isDeclared (StringAtom *name, Field **field) const
    { return mFields.isDeclared (name, field); }

    //! \return iff a named field is a superclass field
    /*! \note if the field is declared then a handle is returned in field.
     */
    bool isSuperclassField (StringAtom *name, Field **field) const;

    //! Declare a field of the class
    void declare (Field *field)
    { 
      mFields.declare (field);
      mFieldList.push_back (field);
      field->Define (field->getDecl ());
    }

    //! \return a list of all the fields for the closure of this class
    void getAllFields (Field::List *) const;

    enum Codes {
      eBUILD_CODE,
      ePOST_BUILD_CODE,
      ePOST_TOP_BUILD_CODE,
      ePRE_WRITE_CODE,
      ePOST_WRITE_CODE,
      eN_CODES
    };

    //! Install code into the class
    bool install (const Codes, Code *, Code **other);

    //! \return the post-build code
    Code *getPostBuildCode () const { return mCode [ePOST_BUILD_CODE]; }

    //! \return the post-top-build code
    Code *getPostTopBuildCode () const { return mCode [ePOST_TOP_BUILD_CODE]; }

    //! \return the build code
    Code *getBuildCode () const { return mCode [eBUILD_CODE]; }

    //! \return the pre-write code
    Code *getPreWriteCode () const { return mCode [ePRE_WRITE_CODE]; }

    //! \return the post-write code
    Code *getPostWriteCode () const { return mCode [ePOST_WRITE_CODE]; }

    //! Form the transitive list of subclasses.
    /*! The list is ordered such that is A is a transitive superclass of B then
     *  A appears in the list before B.
     */
    void getAllSubclasses (ConstList *subclasses) const;

    //! Check the class type for internal consistency
    virtual bool check (NExprSchema *);

    //! Check for cycles in the class hierarchy
    void checkCyclicHierarchy (NExprSchema *schema, List parents, bool *found_cycle);

    //! Construct the list of walkable subclasses
    /*! The walkable subclasses are those subclass not tagged with the
     *  "unwalkable" attribute. This is used when generating a design walker
     *  for dumping nucleus as N-expressions.
     */
    void findWalkableSubclasses (List *walkable);

    //! Propagate attributes to children
    void propagateAttributes (UInt32 mask, UInt32 inherited);

    //! Output code to instantiate a object of this class
    bool writeInstantiation (CodeStream &out, const char *nexpr, const char *dst) const;

    //! Output the specialisation of the build method for the class
    bool writeBuild (CodeStream &out);

    //! Output the write method for the class
    /*! The write (NExpr::Writer &...) method writes an object as an
     *  N-expression to an output stream.
     */
    bool writeWrite (CodeStream &out);

    //! Write a write method for fields of this type.
    virtual bool writeWrite (CodeStream &, const Field *);

    //! Write a read method for fields of this type.
    virtual bool writeRead (CodeStream &, const Field *, const char *, const char *, const UInt32 = 0);

    //! Output the read method for the class
    /*! The read (const NExpr &...) reconstructs an object from an
     *  N-expression.
     */
    bool writeRead (CodeStream &out);

    //! Output the post-build code for the class
    bool writePostBuild (CodeStream &out, const char *dst) const;

    //! Output the post-top-build code for the class
    bool writePostTopBuild (CodeStream &out, const char *dst, UInt32 *n_actions, 
      bool top = true) const;

    //! Output the API construction method header
    bool writeApiDecl (CodeStream &out, bool is_defn = false) const;

    //! Output the API construction method body
    bool writeApiDefn (CodeStream &out) const;

  private:

    UInt32 mFlags;                      //!< mask of option bits
    List mSuperclasses;                 //!< list of superclasses
    List mSubclasses;                   //!< list of subclasses
    Field::Map mFields;                 //!< map of the class fields
    Field::List mFieldList;             //!< the fields in declaration order
    Code *mCode [eN_CODES];             //!< all codes applicable to the class

  };

  class CodeContext;

  //! \class CodeFragment
  class CodeFragment : public Declarable {
  public:

    enum {
      eISBANG = 1<<0,                   //!< fragment is marked with a bang!
    };

    CodeFragment (NExprSchema *schema, const SourceLocator loc, StringAtom *value) :
      Declarable (schema, loc, value), mFlags (0)
    {}
    virtual ~CodeFragment () {}

    //! Bind the fragment to a code.
    void bind (Code *code) { mCode = code; }

    //! Expand the code fragment
    virtual bool expand (CodeStream &, const CodeContext *) const
    { INFO_ASSERT (false, "cannot expand fragment"); return false; }

    //! Expand the code in an emit method
    virtual bool writeEmit (CodeStream &) const;
    virtual bool writeEmit (CodeStream &, bool is_chain, UInt32 subgoal [2], const char *name [2]) const;

    //! \return iff this is a field code
    virtual bool hasField () const { return false; }

    virtual const Field *getField () const = 0;
    virtual const char *getTmpName () const = 0;

    //! Set properties of the field.
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return iff this is a bang code
    bool isBang () const { return mFlags & eISBANG; }

    //! sanity checks
    virtual bool check (NExprSchema *, CodeContext &) { return true; }

    // visualisation
    virtual void print (UtOStream &) const = 0;
    virtual void pr () const;

    //! Output temporary variable initialisation
    virtual bool writeTmpInit (CodeStream &, const char *) { return true; }

    //! \return the enclosing code
    Code *getCode () { return mCode; }
    const Code *getCode () const { return mCode; }

  private:

    UInt32 mFlags;
    Code *mCode;                      //!< the enclosing code

    //! Prevent the use of check without a CodeContext object
    /*! Checking for code segments is dependent on the context of the code
     *  segment. So, override the check method from Declarable to just assert
     *  out!
     */
    virtual bool check (NExprSchema *) { INFO_ASSERT (false, "blah"); return false; }

  };

  class LiteralCodeFragment : public CodeFragment {
  public:
    LiteralCodeFragment (NExprSchema *schema, const SourceLocator loc, StringAtom *value) :
      CodeFragment (schema, loc, value)
    {}

    //! sanity checks
    virtual bool check (NExprSchema *, CodeContext &) { return true; }

    //! Expand the code fragment
    virtual bool expand (CodeStream &out, const CodeContext *) const
    { out << getName ()->str (); return true; }

    //! Expand the code in an emit method
    virtual bool writeEmit (CodeStream &) const;
    virtual bool writeEmit (CodeStream &, bool is_chain, UInt32 subgoal [2], const char *name [2]) const;

    virtual const Field *getField () const 
    { INFO_ASSERT (false, "this is a literal fragment"); return NULL; }

    virtual const char *getTmpName () const
    { INFO_ASSERT (false, "this is a literal fragment"); return NULL; }

    // visualisation
    virtual void print (UtOStream &) const;

  };

  class SpecialCodeFragment : public LiteralCodeFragment {
  public:
    SpecialCodeFragment (NExprSchema *schema, const SourceLocator loc, const char special,
      StringAtom *value) :
      LiteralCodeFragment (schema, loc, value), mSpecial (special)
    {}

    //! sanity checks
    virtual bool check (NExprSchema *, CodeContext &);

    //! Expand the code fragment
    virtual bool expand (CodeStream &out, const CodeContext *) const;

    //! Expand the code in an emit method
    virtual bool writeEmit (CodeStream &) const;
    virtual bool writeEmit (CodeStream &, bool is_chain, UInt32 subgoal [2], const char *name [2]) const;

    // visualisation
    virtual void print (UtOStream &) const;

  private:
    const char mSpecial;
  };

  class NumberedCodeFragment : public CodeFragment {
  public:
    NumberedCodeFragment (NExprSchema *schema, const SourceLocator loc, StringAtom *value,
      const UInt32 index) :
      CodeFragment (schema, loc, value), mIndex (index)
    {}

    //! sanity checks
    virtual bool check (NExprSchema *, CodeContext &);

    //! Expand the code fragment
    virtual bool expand (CodeStream &out, const CodeContext *context) const;

    //! Expand the code in an emit method
    virtual bool writeEmit (CodeStream &) const;
    virtual bool writeEmit (CodeStream &, bool is_chain, UInt32 subgoal [2], const char *name [2]) const;

    virtual const Field *getField () const 
    { INFO_ASSERT (false, "this is a numbered fragment"); return NULL; }

    virtual const char *getTmpName () const
    { INFO_ASSERT (false, "this is a numbered fragment"); return NULL; }

    // visualisation
    virtual void print (UtOStream &) const;

  private:
    
    const UInt32 mIndex;

  };

  class DstCodeFragment : public CodeFragment {
  public:
    DstCodeFragment (NExprSchema *schema, const SourceLocator loc, StringAtom *value) :
      CodeFragment (schema, loc, value)
    {}

    //! sanity checks
    virtual bool check (NExprSchema *, CodeContext &);

    //! Expand the code fragment
    virtual bool expand (CodeStream &out, const CodeContext *context) const
    { out << context->getDst (); return true; }

    virtual const Field *getField () const 
    { INFO_ASSERT (false, "this is a dst fragment"); return NULL; }

    virtual const char *getTmpName () const
    { INFO_ASSERT (false, "this is a dst fragment"); return NULL; }

    // visualisation
    virtual void print (UtOStream &) const;

  };

  class FieldCodeFragment : public CodeFragment {
  public:

    FieldCodeFragment (NExprSchema *schema, const SourceLocator loc, StringAtom *value) :
      CodeFragment (schema, loc, value), mField (NULL), mTmpName (NULL)
    {}

    //! sanity checks
    virtual bool check (NExprSchema *, CodeContext &);

    //! Expand the code fragment
    virtual bool expand (CodeStream &out, const CodeContext *expand) const;

    //! \return iff this is a field code
    virtual bool hasField () const { return true; }

    //! \return the associate field
    virtual const Field *getField () const { return mField; }

    //! \return the name of a temporary variable for this fragment
    virtual const char *getTmpName () const;

    //! Output temporary variable initialisation
    virtual bool writeTmpInit (CodeStream &out, const char *nexpr);

    //! Expand the code in an emit method
    virtual bool writeEmit (CodeStream &) const;
    virtual bool writeEmit (CodeStream &, bool is_chain, UInt32 subgoal [2], const char *name [2]) const;

    // visualisation
    virtual void print (UtOStream &) const;

  private:
    Field *mField;
    mutable StringAtom *mTmpName;

  };

  //! \class CodeContext
  /*! Wraps the mechanism for expaning a code into specific text
   */
  class CodeContext {
  public:

    CodeContext  () {}
    virtual ~CodeContext() {}

    virtual const char *getDst () const { return NULL; }
    virtual const char *getNExpr () const { return NULL; }
    virtual bool hasOrdinal (const UInt32) const { return false; }
    virtual bool getOrdinal (const UInt32, Code **) const { return false; }

    //! Output the code expansion
    virtual bool operator () (CodeStream &, Code *);

  };

  class IROp;

  //! \class Code
  class Code : public UtList <CodeFragment *> {
  public:

    enum {
      // bits set to indicate permitted use and allowed fragments in a code string
      eALLOWNUMBERED = 1<<0,            //!< allow $n in the code
      eALLOWDST = 1<<1,                 //!< allow $$ in the code
      eALLOWFIELD = 1<<2,               //!< allow $name in the code
      eINBUILD = 1<<3,                  //!< this is in a build code
      eUSETMP = 1<<4,                   //!< use a temporary for fields
      eCLASSFIELD = 1<<5,               //!< resolve fields from the class
      eTERMINALFIELD = 1<<6,            //!< resolve fields from a terminal
      // bitmask for pre and post write code
      ePREWRITE = eALLOWFIELD|eALLOWDST|eCLASSFIELD,
      ePOSTWRITE = ePREWRITE,
      // bitmask for class build code
      eBUILD     = eALLOWFIELD|eALLOWDST|eCLASSFIELD|eINBUILD|eUSETMP,
      // bitmask for code executed after building
      ePOSTBUILD = eALLOWFIELD|eALLOWDST|eCLASSFIELD,
      // bitmask for code used in a LIR "let" construct
      eIRLET = eALLOWFIELD|eCLASSFIELD,
      // bitmask for code used in a LIR transformation rule
      eTRANSFORMRULE = eALLOWFIELD|eALLOWNUMBERED|eCLASSFIELD,
      // bitmask for code used in a LIR tree exppresion
      eIRTREE = eALLOWFIELD|eALLOWNUMBERED|eCLASSFIELD,
      // bitmask for code used in a rewrite code fragment
      eCODE = eALLOWNUMBERED|eALLOWFIELD|eTERMINALFIELD,
      // these are set during analysis
      eHASNUMBERED = 1<<29,             //!< the code includes $n fragments
      eHASDST = 1<<30,                  //!< the code includes a Dst fragment ($$)
      eHASTMPS = 1<<31,                 //!< the expansion uses temporaries
    };

    Code (NExprSchema *schema, const SourceLocator loc) : 
      UtList <CodeFragment *> (), mFlags (0), mSchema (schema), mLoc (loc), 
      mClass (NULL), mIROp (NULL)
    {}

    virtual ~Code () {}

    //! Mapping from integers to codes
    /*! This is used to implement explicit bindings from positions to
     *  codes. For example, the LIR transformation use "let $n = code" to
     *  define constructs used with the LIR tree rules.
     */
    typedef UtMap <UInt32, Code *> OrdinalMap;

    SourceLocator getDefn () const { return mLoc; }

    //! Bind the code to a class
    void bind (ClassType *classtype) { mClass = classtype; }

    //! Bind the code to an IR operator
    void bind (IROp *irop) { mIROp = irop; }

    //! \return the class associated with this code
    ClassType *getClass () const { return mClass; }

    //! \return the IR operator associated with this code
    IROp *getIROp () const { return mIROp; }

    //! Set properties of the field.
    void set (const UInt32 mask) { mFlags |= mask; }

    //! \return the property mask for this
    //UInt32 getProperties () const { return mFlags; }

    //! \return iff a temporary is used for fields
    bool useFieldTmp () const { return mFlags & eUSETMP; }

    //! \return iff field bindings use the class field
    bool useClassField () const { return mFlags & eCLASSFIELD; }

    //! \return iff field bindings use terminal parameters
    bool useTerminalParameter () const { return mFlags & eTERMINALFIELD; }

    //! \return iff a destination ($$) rule is allowed
    bool allowDst () const { return mFlags & eALLOWDST; }

    //! \return iff a numbered ($n) rule is allowed
    bool allowNumbered () const { return mFlags & eALLOWNUMBERED; }

    //! \return iff a named field rule ($ident) is allowd
    bool allowField () const { return mFlags & eALLOWFIELD; }

    //! \return iff a named field rule ($ident) is allowd
    bool inBuild () const { return mFlags & eINBUILD; }

    //! \return iff eHASDST is set
    bool hasDst () const { return mFlags & eHASDST; }

    //! \return iff eHASTMPS is set
    bool hasTmps () const { return mFlags & eHASTMPS; }

    //! \return iff eHASNUMBERED is set
    bool hasNumbered () const { return mFlags & eHASNUMBERED; }

    //! Add fragments to a code
    /*! Parse a string to construct code fragments and push them onto this code
     *  \param loc the location in the schema containing the string
     *  \param s a specification string of the form "abc %foo def %foobar"
     *  \param errmsg point to a string for any error messages
     *  \return iff s was a valid code specification
     */
    bool build (const SourceLocator loc, const char *, const UInt32 flags, UtString *errmsg);

    // analysis
    bool check (NExprSchema *, CodeContext &);

    // visualisation
    void print (UtOStream &) const;
    void pr () const;
    const char *compose (UtString *) const;

  private:

    UInt32 mFlags;
    NExprSchema *mSchema;
    const SourceLocator mLoc;
    ClassType *mClass;                  //!< the class type associated with the field
    IROp *mIROp;
  };

  //! \class ContextWithDst
  /*! Context for code fragments that admit $$
   */
  class ContextWithDst : public NExprSchema::CodeContext {
  public:

    ContextWithDst (const char *dst) : mDst (dst)
    {}
    virtual ~ContextWithDst() {}

    virtual const char *getDst () const { return mDst; }

  private:
    const char *mDst;
  };

  //! \class ContextWithBoundedOrdinal
  class RewriteContext : public CodeContext {
  public:
    RewriteContext (NExprSchema *schema, SourceLocator loc, const UInt32 arity) : 
      CodeContext (), mArity (arity), mLeft (schema, loc), mRight (schema, loc)
    {
      INFO_ASSERT (mArity < 3, "blah");
      mLeft.push_back (new LiteralCodeFragment (schema, loc, schema->intern ("mLeft")));
      mRight.push_back (new LiteralCodeFragment (schema, loc, schema->intern ("mRight")));
    }

    virtual ~RewriteContext () {}

    virtual bool hasOrdinal (const UInt32 n) const
    {
      return 0 < n && n <= mArity;
    }

    virtual bool getOrdinal (const UInt32 ordinal, NExprSchema::Code **macro) const
    {
      if (ordinal == 1) {
        *macro = const_cast <NExprSchema::Code *> (&mLeft);
        return true;
      } else if (ordinal == 2) {
        *macro = const_cast <NExprSchema::Code *> (&mRight);
        return true;
      } else {
        INFO_ASSERT (false, "blah");
        return false;
      }
    }

  private:
    
    const UInt32 mArity;
    Code mLeft;
    Code mRight;
  };

  //! \class ContextWithOrdinalMap
  /*! Context for code fragments that admit $n fragments
   */
  class ContextWithOrdinalMap : public CodeContext {
  public:

    ContextWithOrdinalMap (Code::OrdinalMap &map) : mMap (map)
    {}
    virtual ~ContextWithOrdinalMap() {}

    virtual bool hasOrdinal (const UInt32 n) const
    {
      return mMap.find (n) != mMap.end ();
    }

    virtual bool getOrdinal (const UInt32 ordinal, NExprSchema::Code **macro) const
    {
      NExprSchema::Code::OrdinalMap::const_iterator found;
      if ((found = mMap.find (ordinal)) == mMap.end ()) {
        *macro = NULL;
        return false;
      } else {
        *macro = found->second;
        return true;
      }
    }

  private:
    const Code::OrdinalMap &mMap;
  };

  class NestedCodeContext : public CodeContext {
  public:
    NestedCodeContext (const CodeContext &parent) : mParent (parent) {}
    virtual ~NestedCodeContext() {}

    virtual const char *getDst () const { return mParent.getDst (); }
    virtual const char *getNExpr () const { return mParent.getDst (); }
    virtual bool getOrdinal (const UInt32 n, Code **ordinal) const 
    { return mParent.getOrdinal (n, ordinal); }
  private:
    const CodeContext &mParent;
  };

  //! \return iff an include file is already noted
  /*! \note if the include file is declared a handle is returned in includes
   */
  bool isDeclared (StringAtom *name, IncludeFile **includes) const
  { return mIncludeFiles.isDeclared (name, includes); }

  //! declare an include file
  void declare (IncludeFile *includes)
  {
    mIncludeFiles.declare (includes);
    mIncludeFileList.push_back (includes);
    includes->Define (includes->getDecl ());
  }

  //! \return iff an include file is already noted
  /*! \note if the include file is declared a handle is returned in includes
   */
  bool isDeclared (StringAtom *name, Token **includes) const
  { return mTokens.isDeclared (name, includes); }

  //! Declare an include file
  void declare (Token *token)
  {
    mTokens.declare (token);
    token->Define (token->getDecl ());
  }

  //! Output the include statements.
  bool writeIncludeIncludes (CodeStream &) const;

  //! Construct a base type and declare it.
  BaseType *declareBaseType (const SourceLocator loc, StringAtom *name, const UInt32 flags);

  //! Construct an enum type and declare it
  EnumType *declareEnumType (const SourceLocator loc, StringAtom *name, const UInt32 flags);

  //! Construct a token and then declare it.
  Token *declareToken (const SourceLocator loc, StringAtom *name, const UInt32 flags);

  //! Construct a vector type and declare it
  VectorType *declareVectorType (const SourceLocator loc, StringAtom *name, 
    Type *element_type, const UInt32 flags);

  //! Create a field object
  /*! \note This method differs from the declareFoo methods in that it does not
   *  declare the field in the schema or any classes. It just checks the types
   *  and creates the field object.
   */
  Field *createField (const SourceLocator, Type *type,
    StringAtom *fieldident, const UInt32 properties);

  //! Declare an error type
  void declare (ErrorType *type)
  { mTypes.declare (type); }

  //! \return iff a name is declared as a base type
  /*! \note if the type is declared a handle on the type is returned in type
   */
  bool isDeclared (StringAtom *name, BaseType **type) const
  { return mBaseTypes.isDeclared (name, type); }

  //! Declare a base type
  void declare (BaseType *type)
  { 
    mBaseTypes.declare (type); 
    mTypes.declare (type);
    type->Define (type->getDecl ()); 
  }

  //! \return iff a vector type is declared
  /*! \note if the vector is declared then a handle is returned in type
   */
  bool isDeclared (StringAtom *name, VectorType **type) const
  { return mVectorTypes.isDeclared (name, type); }

  //! Declare a named vector
  void declare (VectorType *type)
  { 
    mVectorTypes.declare (type); 
    mTypes.declare (type);
  }

  //! Find a named type object
  bool isDeclared (StringAtom *name, Type **type) const
  { return mTypes.isDeclared (name, type); }

  //! \return iff a class is declared
  /*! \note if the class is declared then a handle is returned in type
   */
  bool isDeclared (StringAtom *name, ClassType **type) const
  { return mClassTypes.isDeclared (name, type); }

  //! Declare a named class
  void declare (ClassType *type)
  { 
    mClassTypes.declare (type); 
    mTypes.declare (type);
  }

  //! Add a field to the schemas maps of all fields
  void addField (Field *field)
  { mFields.insert (Field::MultiMap::value_type (field->getName (), field)); }

  //! \class IROp
  class IROp : public NExprSchema::Declarable {
  public:

    typedef NExprSchema::Declarable::Map <IROp> Map;

    typedef std::pair <StringAtom *, StringAtom *> Parameter;
    typedef UtList <Parameter> Parameters;

    IROp (NExprSchema *schema, const SourceLocator loc, StringAtom *name, const UInt32 arity, 
      Parameters *parameters, UInt32 flags = 0) :
      Declarable (schema, loc, name), mFlags (flags), mArity (arity), mParameters (parameters)
    {}

    virtual ~IROp () 
    { delete mParameters; }

    //! \return the arity of the operator
    UInt32 getArity () const { return mArity; }

    //! Set properties of the base type
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return the property mask for this
    UInt32 getProperties () const { return mFlags; }

    //! Checking base types.
    virtual bool check (NExprSchema *);

    //! \return iff the operator has a given named field
    bool hasParameter (const StringAtom *name) const
    { return mParameterNames.find (name) != mParameterNames.end (); }

    //! Iterate over the parameters
    Parameters::const_iterator beginformals () const { return mParameters->begin (); }
    Parameters::const_iterator endformals () const { return mParameters->end (); }

  private:

    UInt32 mFlags;                      //!< properties of the base type
    const UInt32 mArity;                //!< the arity of the IR operator
    Parameters *mParameters;            //!< terminal parameters
    UtMap <const StringAtom *, bool> mParameterNames;
  };

  //! \return iff an ir operator is already noted
  /*! \note if declared a handle is returned in **irop
   */
  bool isDeclared (StringAtom *name, IROp **irop) const
  { return mIR.isDeclared (name, irop); }

  //! declare an IR operator
  void declare (IROp *irop)
  {
    mIR.declare (irop);
    irop->Define (irop->getDecl ());
  }

  //! \class NonTerminal
  class NonTerminal : public NExprSchema::Declarable {
  public:

    typedef NExprSchema::Declarable::Map <NonTerminal> Map;

    NonTerminal (NExprSchema *schema, const SourceLocator loc, StringAtom *name, 
      UInt32 flags = 0) :
      Declarable (schema, loc, name), mFlags (flags)
    {}

    virtual ~NonTerminal () {}

    //! Set properties of the base type
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return the property mask for this
    UInt32 getProperties () const { return mFlags; }

    //! Checking base types.
    virtual bool check (NExprSchema *);

  private:

    UInt32 mFlags;                      //!< properties of the base type

  };

  //! \return iff non-terminal is already declared
  /*! \note if declared a handle is returned in **nt
   */
  bool isDeclared (StringAtom *name, NonTerminal **nt) const
  { return mNT.isDeclared (name, nt); }

  //! declare a non-terminal
  void declare (NonTerminal *nt)
  {
    mNT.declare (nt);
  }

  //! \class Rewrite
  class Rewrite : public NExprSchema::Declarable {
  public:

    typedef UtList <Rewrite *> List;

    class Pattern;

    Rewrite (NExprSchema *schema, const SourceLocator loc,
      NonTerminal *nt, Pattern *pattern, const UInt32 cost, Code *code, const UInt32 flags = 0) :
      Declarable (schema, loc, nt->getName ()), mFlags (flags), 
      mNT (nt), mPattern (pattern), mCost (cost), mCode (code)
    {}

    virtual ~Rewrite () {}

    //! Set properties of the base type
    void setProperties (const UInt32 mask) { mFlags |= mask; }

    //! \return the property mask for this
    UInt32 getProperties () const { return mFlags; }

    //! \return the left-side non-terminal
    NonTerminal *getNonTerminal () { return mNT; }

    //! \return the right-side pattern
    Pattern *getPattern () { return mPattern; }

    //! \return the cost of the rule
    UInt32 getCost () { return mCost; }

    //! \return the code
    const Code *getCode () const { return mCode; }

    //! Checking rewrites.
    virtual bool check (NExprSchema *);

    //! Output the emit method
    bool writeEmit (CodeStream &out);

    //! Output the rewrite to a stream
    void print (UtOStream &) const;

    //! Output the rewrite to stdout
    void pr () const;

    class Pattern {
    public:
      Pattern (const SourceLocator loc, StringAtom *symbol, Pattern *left = NULL, Pattern *right = NULL) :
        mLoc (loc), mSymbol (symbol), mIROp (NULL), mNT (NULL), mLeft (left), mRight (right)
      {}
      virtual ~Pattern () {}

      virtual bool check (NExprSchema *);

      //! \return the location of the pattern
      const SourceLocator getLoc () const { return mLoc; }

      //! Write the pattern in IBURG syntax
      bool write (CodeStream &) const;

      //! \return the operator
      StringAtom *getSymbol () const { return mSymbol; }
      
      //! \return the ir operator
      IROp *getIROp () const { return mIROp; }

      //! \return the left child
      Pattern *getLeft () const { return mLeft; }

      //! \return the right child
      Pattern *getRight () const { return mRight; }

      //! \return the arity of the pattern
      UInt32 getArity () const
      { return (mNT != NULL ? 1 : (mLeft == NULL ? 0 : (mRight == NULL ? 1 : 2))); }

      //! Output the pattern to a stream
      void print (UtOStream &) const;

      //! Output the pattern to stdout
      void pr () const;

      // Output the pattern to an indenting stream
      void print (IndentedStream <UtOStream> &s) const;

    private:
      const SourceLocator mLoc;         //!< location in the schema
      StringAtom *mSymbol;              //!< rhs symbol
      IROp *mIROp;                      //!< IR operator if not a chain rule
      NonTerminal *mNT;                 //!< Non terminal symbol if a chain rule
      Pattern *mLeft, *mRight;

    };

  private:

    UInt32 mFlags;
    NonTerminal *mNT;                   //!< left side of the rewrite rule
    Pattern *mPattern;                  //!< right side of the rewrite rule
    const UInt32 mCost;                 //!< cost of the rule
    Code *mCode;                      //!< the code to emit

  };

  //! Declare a rewrite rule
  void declare (Rewrite *rw)
  { mRewrite.push_back (rw); }

  //! \class IRTransformation
  /*! This class models tranformations from Nucleus to the low-level IR. A
   *  transformation maps a class type to a RuleTree. The RuleTree is
   *  effectively a small language for models mappings from one tree to
   *  another. This is by no means a general Nucleus to something else
   *  specification language. It is intended for the statements and expressions
   *  of Nucleus which are effectively trees.
   */
  class IRTransformation : public Declarable {
  public:

    class RuleTree;                     // forward declaration
    class IRTree;                       // forward declaration

    IRTransformation (NExprSchema *schema, const SourceLocator loc, 
      const ClassType *classtype, Code::OrdinalMap *ordmap, RuleTree *rule) :
      Declarable (schema, loc, classtype->getName ()), mClass (classtype), mOrdmap (ordmap), mRule (rule)
    {}

    virtual ~IRTransformation () {}

    //! \return the classtype that is the object of the tranformation
    const ClassType *getClass () const { return mClass; }

    //! Perform consistency checks on this
    virtual bool check (NExprSchema *);

    //! Analyse actual parameters in the IRTree
    bool analyseActuals (BURS &burs) { return mRule->analyseActuals (burs); }

    //! \return the $n -> code map for this transformation
    Code::OrdinalMap *getOrdinalMap () const { return mOrdmap; }

    //! Output the class::generateLIR method.
    bool writeGenerateLIR (CodeStream &) const;

    //! visualisation
    void print (UtOStream &) const;
    void pr () const;

    //! \class IRTransformation::Map
    /*! Mappings from class types to transformations.
     */
    class Map : public UtMap <const ClassType *, IRTransformation *> {
    public:

      Map () : UtMap <const ClassType *, IRTransformation *> () {}
      virtual ~Map () {}

      //! \return iff there is already a transformation for classtype in the map
      /*! If such a transform exists, then it is returned in the out parameter
       */
      bool isDeclared (const ClassType *classtype, IRTransformation **transform) const
      {
        const_iterator found;
        if ((found = find (classtype)) != end ()) {
          *transform = found->second;
          return true;
        } else {
          *transform = NULL;
          return false;
        }
      }

      //! Add a transformation for a classtype to the map.
      void declare (IRTransformation *transform)
      { insert (value_type (transform->getClass (), transform)); }

    };

    //! \class IRTransformation::RuleTree
    class RuleTree {
    public:

      enum Flavour {
        eIRTREE,                        //!< rule is just an IR tree
        eCODE,                         //!< rule is a code application
        eSWITCH,                        //!< a switch rule
        eCASE,                          //!< a case branch
        eCASEPAIR,                      //!< a chain of case branches
        eWHEN,                          //!< a when clause
        eWHENPAIR                       //!< a chain of when clauses
      };

      enum {
        eINNER = 1<<0,                  //!< set when a pair is not the first list item
      };

      RuleTree (const SourceLocator loc, ClassType *classtype, Flavour flavour) :
        mLoc (loc), mFlags (0), mClass (classtype), mFlavour (flavour), mCode (NULL), mIRTree (NULL),
        mLeft (NULL), mRight (NULL)
      {}
      RuleTree (const SourceLocator loc, ClassType *classtype, Flavour flavour, Code *code) :
        mLoc (loc), mFlags (0), mClass (classtype), mFlavour (flavour), mCode (code), mIRTree (NULL),
        mLeft (NULL), mRight (NULL)
      {}
      RuleTree (const SourceLocator loc, ClassType *classtype, Flavour flavour, IRTree *irtree) :
        mLoc (loc), mFlags (0), mClass (classtype), mFlavour (flavour), mCode (NULL), mIRTree (irtree),
        mLeft (NULL), mRight (NULL)
      {}
      RuleTree (const SourceLocator loc, ClassType *classtype, Flavour flavour, 
        RuleTree *left, RuleTree *right) :
        mLoc (loc), mFlags (0), mClass (classtype), mFlavour (flavour), mCode (NULL), mIRTree (NULL),
        mLeft (left), mRight (right)
      {}

      virtual ~RuleTree () {}

      //! Set properties of the rule tree type
      void setProperties (const UInt32 mask) { mFlags |= mask; }

      //! \return the property mask for this
      UInt32 getProperties () const { return mFlags; }

      //! \return iff this is an inner list pair node
      bool isInner () const { return mFlags & eINNER; }

      //! Analyse actual parameters in the IRTree
      bool analyseActuals (BURS &burs);

      //! Sanity checking
      virtual bool check (NExprSchema *, CodeContext &);

      //! \return the source location for the rule
      const SourceLocator getLoc () const { return mLoc; }

      //! Output code to implement the rule
      bool write (CodeStream &, const IRTransformation &) const;

      //! visualisation
      void print (UtOStream &, const UInt32 indent) const;
      void pr () const;

    private:

      const SourceLocator mLoc;
      UInt32 mFlags;
      ClassType *mClass;
      const Flavour mFlavour;
      Code *mCode;
      IRTree *mIRTree;
      RuleTree *mLeft;
      RuleTree *mRight;
    };

    class IRTree {
    public:

      //! \class Pattern
      /*! Actual parameters for tree patterns
       */
      class Parameter {
      public:

        class List : public UtList <Parameter *> {
        public:
          List () : UtList <Parameter *> () {}
          virtual ~List ()
          {
            for (iterator it = begin (); it != end (); it++) {
              delete *it;
            }
          }
        };

        //! ctor for string valued actual parameters
        Parameter (SourceLocator loc, StringAtom *value) :
          mLoc (loc), mFlavour (eSTRING), mString (value), mInteger (0xdeadbeef)
        {}

        //! \return the source location of the actual
        SourceLocator getLoc () const { return mLoc; }

        //! ctor for integer valued actual parameters
        Parameter (SourceLocator loc, UInt32 value) :
          mLoc (loc), mFlavour (eINTEGER), mString (NULL), mInteger (value)
        {}

        //! \return iff this is a string valued parameter
        bool isString () const { return mFlavour == eSTRING; }

        //! \return iff this is an integer valued parameter
        bool isInteger () const { return mFlavour == eINTEGER; }

        //! \return the string value
        StringAtom *getString () const { return mString; }
        
        //! \return the integer value
        UInt32 getInteger () const { return mInteger; }

        //! Print the actual parameter to a stream
        void print (UtOStream &) const;

        //! Print the actual parameter to an indenting stream
        void print (IndentedStream <UtOStream> &) const;

        //! Print the actual parameter to standard output
        void pr () const;

      private:

        enum Flavour {
          eSTRING,                      //!< string actual parameter
          eINTEGER                      //!< integer actual parameter
        };

        SourceLocator mLoc;             //!< location in the schema specification
        Flavour mFlavour;               //!< type of the parameter
        StringAtom *mString;            //!< string value when mFlavour == eSTRING
        UInt32 mInteger;                //!< integer value when mFlavour == eINTEGER
      };

      IRTree (NExprSchema *schema, const SourceLocator loc, StringAtom *key,
        Parameter::List *actuals, IRTree *left = NULL, IRTree *right = NULL) :
        mSchema (schema), mLoc (loc), mCode (NULL), 
        mKey (key), mActuals (actuals), mLeft (left), mRight (right), mIROp (NULL),
        mHasParameters (false)
      {}

      IRTree (NExprSchema *schema, const SourceLocator loc, Code *code) :
        mSchema (schema), mLoc (loc), mCode (code),
        mKey (NULL), mActuals (NULL), mLeft (NULL), mRight (NULL), mIROp (NULL),
        mHasParameters (false)
      {}

      virtual ~IRTree ();

      //! Output code to instantiate LIR
      bool write (CodeStream &, const IRTransformation &) const;

      //! \return iff this IRTree is a code application
      bool isCode () const { return mCode != NULL; }
      
      //! \return the code associated with this IRTree node
      const Code *getCode () const { return mCode; }

      //! \return the key symbol for this IR node
      const StringAtom *getKey () const { return mKey; }

      //! \return the arity of this node
      UInt32 getArity () const { return (mLeft == NULL) ? 0 : (mRight == NULL) ? 1 : 2; }

      //! Analyse actual parameters in the IRTree
      bool analyseActuals (BURS &burs);

      //! \return iff this tree has actual parameter bindings
      bool hasParameters () const { return mHasParameters; }

      //! \return the actual parameter binding index
      UInt32 getActualBinding () const { return mActualBinding; }

      //! Consistency checking
      bool check (NExprSchema *, CodeContext &);

      //! visualisation
      void print (UtOStream &) const;
      void pr () const;

    private:

      NExprSchema *mSchema;
      const SourceLocator mLoc;
      Code *mCode;                      //! a code fragment
      StringAtom *mKey;
      Parameter::List *mActuals;        //!< ordered list of actual parameters
      IRTree *mLeft, *mRight;
      IROp *mIROp;
      // When the actual parameters are analysed, the binding for this node is
      // allocated an index that is unique w.r.t. the IRTree node and the
      // terminal. This is used as an index into a static array of actual
      // parameter values for the IR operator.
      UInt32 mActualBinding;            //!< index allocated to the actual parameter binding
      bool mHasParameters;              //!< set when actual parameters are bound to this
    };

  private:

    const ClassType *mClass;            //!< the class bound to this set of rules
    Code::OrdinalMap *mOrdmap;         //!< mapping for resolving $1, $2, ...                    
    RuleTree *mRule;                    //!< the rule for mapping this class to a low-level IR tree
  };                                    // class IRTransformation

  //! \return iff an IR transform for the class is declared
  bool isDeclared (const ClassType *classtype, IRTransformation **transform)
  { return mTransforms.isDeclared (classtype, transform); }

  //! declare a non-terminal
  void declare (IRTransformation *transform)
  {
    mTransforms.declare (transform);
  }

  //! \return the table of IR operators
  IROp::Map &getIROps () { return mIR; }

  //! \return the table of non-terminals
  NonTerminal::Map &getNonTerminals () { return mNT; };
  
  //! \return the list of rewrite rules
  Rewrite::List &getRewrite () { return mRewrite; }

  //! \return the list of LIR transforms
  IRTransformation::Map &getTransforms () { return mTransforms; }

  //! \return the message context for error messages
  MsgContext *getMsgContext () { return mMsgContext; }

  //! \return iff a goal non-terminal is declared
  bool hasGoal (NonTerminal **nt, SourceLocator *decl = NULL);

  //! Set the goal non-terminal
  void bindGoal (NonTerminal *nt, SourceLocator loc);

  //! Construct an options bit mask from the command line
  static UInt32 options (const ArgProc &);

  //! \return a CodeStream option vector set from the compiler options
  UInt32 outputOptions () const
  { return (mOptions & eNOHASHLINE) ? CodeStream::cNO_USER_HASH_LINES : 0; }

private:
  //! \return a unique identifier for an iterator.
  /*! The write and read patch methods for vectors use an iterator in the
   *  generated code. This method returns an identifier for an iterator in the
   *  generated code that will be unique across the schema.
   */
  StringAtom *nextIteratorName ();

  typedef UtMap <StringAtom *, StringAtom *> StringMap;
  typedef UtMap <StringAtom *, UInt32> IndexMap;

  MsgContext *mMsgContext;
  mutable AtomicCache *mSymbols;
  SourceLocatorFactory *mSourceLocators;
  UInt32 mOptions;                      //!< configuration options for the compiler
  UInt32 mNErrors;
  const char *mCurrentFile;             //!< current file being parsed
  UInt32 mNIterators;                   //!< number of names generated by nextIteratorName

  IncludeFile::Map mIncludeFiles;
  IncludeFile::Map mBaseTypeIncludeFiles;
  IncludeFile::List mIncludeFileList;
  Type::Map mTypes;
  BaseType::Map mBaseTypes;
  EnumType::Map mEnumTypes;
  ClassType::Map mClassTypes;
  UInt32 mNApiClasses;
  Token::Map mTokens;
  Field::MultiMap mFields;
  StringMap mFieldNames;
  IndexMap mFieldIndices;
  VectorType::Map mVectorTypes;
  IROp::Map mIR;                        //!< the operators of the low-level IR
  NonTerminal::Map mNT;                 //!< the non-terminals of the codegen BURS
  Rewrite::List mRewrite;               //!< list of BURS rewrite rules
  IRTransformation::Map mTransforms;    //!< map class to IR generation rules
  API::Map mAPIs;                       //!< map names to API descriptors
  API::AliasMap mAPIAliases;            //!< maps alias names to APIs

  //! Output the NExprIO.cxx file
  bool writeNExprIO (const char *filename);

  //! Output the NExprLex.h file
  bool writeNExprLexHdr (const char *filename);

  //! Output the CarbonDistillery.h file
  bool writeDistilleryDecl (const char *filename);

  //! Output the CarbonDistillery.cxx file
  bool writeDistilleryDefn (const char *filename);

  //! Output the field type information
  bool writeFieldTyping (CodeStream &out);

  //! Output the NExpr build method
  bool writeBuild (CodeStream &out);
  
  //! Output the NExpr tripwire writer
  bool writeWriteTripwire (CodeStream &out);

  //! Write the Nucleus design walker for dumping a design as N-expressions.
  /*! This emits a NUDesignCallback subclass for dumping nucleus and the
   * implementation of the NExpr::Writer::writeNucelus method.
   */
  bool writeWriteWalker (CodeStream &out);

  //! Write the isSubclass method
  /*! The isSubclass method checks whether an N-expression type is a subclass
   *  of another N-expression type.
    */
  bool writeIsSubclass (CodeStream &out);

  //! Write the type definitions
  bool writeTypeDefns (CodeStream &out) const;

  //! Write the definitions for dealing with enum types as symbols
  bool writeEnumDefns (CodeStream &out);

  //! Write the NExpr::Reader::getType method
  bool writeGetType (CodeStream &out);

  //! Write the NExpr::Reader::expectedToken method
  bool writeExpectedToken (CodeStream &out);

  //! Write the NExpr::tokenName method
  bool writeTokenName (CodeStream &out);

  // Special objects
  ClassType *mNUBaseClass;              //!< the NUBase representative
  NonTerminal *mGoal;                   //!< the BURS goal symbol
  SourceLocator mGoalDecl;

  //! \return iff the quiet option is active
  bool isQuiet () const { return mOptions & eQUIET; }

  //! \return iff the verbose option is active
  bool isVerbose () const { return mOptions & eVERBOSE; }

};

//! The Bison-generated parser
extern int NExprSchema_parse (void *);

//! Debugging flags in the scanner and parser
extern int NExprSchema__flex_debug;     //!< for the flex generated scanner
extern int NExprSchema_debug;           //!< for the bison parseer

#endif
