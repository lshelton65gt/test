// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

//
// LIR terminals
//

// The low-level IR (LIR) consists of trees where each tree node is labeled with an
// terminal symbol and has zero, one or two subtrees. These are the LIR
// terminals and their arity.

// Statements
assign 2;

// Nets
bitnet 0;                               // a scalar net
vectornet8 0;                           // a vector net with size 1..8
vectornet16 0;                          // a vector net with size 9..16
vectornet32 0;                          // a vector net with size 17..32
vectornet64 0;                          // a vector net with size 33..64
bigvectornet 0;                         // a vector net with size greater than 64

// Constants
one 0;
zero 0;
constant8 0;
constant16 0;
constant32 0;
constant64 0;
bigconstant 0;

// expressions
binaryop [string op] 2;
unaryop [string op] 1;

//
// LIR generation rules
//

// The first phase of statement-level code generation generates LIR trees from
// Nucleus. These rules maps Nucleus classes to the rules for building LIR.

//
// Nets
//

NUIdentLvalue {
  when "mNet->isBitNet ()" => bitnet ("mNet");
  when "mNet->getBitSize () <= 8" => vectornet8 ("mNet");
  when "mNet->getBitSize () <= 16" => vectornet16 ("mNet");
  when "mNet->getBitSize () <= 32" => vectornet32 ("mNet");
  when "mNet->getBitSize () <= 64" => vectornet64 ("mNet");
  others => bigvectornet ("mNet");
};


NUBlockingAssign {
  assign ("getLvalue ()", "getRvalue ()");
};

NUAssign {
  assign ("getLvalue ()", "getRvalue ()");
};

NUIdentRvalue {
  bitnet ("getIdent ()");
};

NUUnaryOp {
  let $1 = "getArg (0)";
  switch ("$mOp") {
  case "NUOp::eBiBitAnd": "NULL";
  // assigning to NULL induces an assertion value at runtime
  case "NUOp::eStart": "NULL";
  case "NUOp::eUnBuf": "NULL";
  case "NUOp::eUnPlus": "NULL";
  case "NUOp::eUnMinus": "NULL";
  case "NUOp::eUnLogNot": unaryop ["!"] ($1);
  case "NUOp::eUnBitNeg": unaryop ["~"] ("$1");
  case "NUOp::eUnVhdlNot": "NULL";
  case "NUOp::eUnRedAnd": "NULL";
  case "NUOp::eUnRedOr": "NULL";
  case "NUOp::eUnRedXor": "NULL";
  case "NUOp::eUnCount": "NULL";
  case "NUOp::eUnAbs": "NULL";
  case "NUOp::eUnFFZ": "NULL";
  case "NUOp::eUnFFO": "NULL";
  case "NUOp::eUnFLZ": "NULL";
  case "NUOp::eUnFLO": "NULL";
  case "NUOp::eUnRound": "NULL";
  case "NUOp::eUnItoR": "NULL";
  case "NUOp::eUnRtoI": "NULL";
  case "NUOp::eUnRealtoBits": "NULL";
  case "NUOp::eUnBitstoReal": "NULL";
  case "NUOp::eUnChange": "NULL";
  case "NUOp::eBiPlus": "NULL";
  case "NUOp::eBiMinus": "NULL";
  case "NUOp::eBiSMult": "NULL";
  case "NUOp::eBiUMult": "NULL";
  case "NUOp::eBiSDiv": "NULL";
  case "NUOp::eBiUDiv": "NULL";
  case "NUOp::eBiSMod": "NULL";
  case "NUOp::eBiUMod": "NULL";
  case "NUOp::eBiVhdlMod": "NULL";
  case "NUOp::eBiEq": "NULL";
  case "NUOp::eBiNeq": "NULL";
  case "NUOp::eBiTrieq": "NULL";
  case "NUOp::eBiTrineq": "NULL";
  case "NUOp::eBiLogAnd": "NULL";
  case "NUOp::eBiLogOr": "NULL";
  case "NUOp::eBiSLt": "NULL";
  case "NUOp::eBiSLte": "NULL";
  case "NUOp::eBiSGtr": "NULL";
  case "NUOp::eBiSGtre": "NULL";
  case "NUOp::eBiULt": "NULL";
  case "NUOp::eBiULte": "NULL";
  case "NUOp::eBiUGtr": "NULL";
  case "NUOp::eBiUGtre": "NULL";
  //case "NUOp::eBiBitAnd": "NULL";
  case "NUOp::eBiBitOr": "NULL";
  case "NUOp::eBiBitXor": "NULL";
  case "NUOp::eBiRoR": "NULL";
  case "NUOp::eBiRoL": "NULL";
  case "NUOp::eBiExp": "NULL";
  case "NUOp::eBiDExp": "NULL";
  case "NUOp::eBiVhExt": "NULL";
  case "NUOp::eBiVhZxt": "NULL";
  case "NUOp::eBiVhLshift": "NULL";
  case "NUOp::eBiVhRshift": "NULL";
  case "NUOp::eBiVhLshiftArith": "NULL";
  case "NUOp::eBiVhRshiftArith": "NULL";
  case "NUOp::eBiRshiftArith": "NULL";
  case "NUOp::eBiLshiftArith": "NULL";
  case "NUOp::eBiRshift": "NULL";
  case "NUOp::eBiLshift": "NULL";
  case "NUOp::eBiDownTo": "NULL";
  case "NUOp::eTeCond": "NULL";
  case "NUOp::eNaConcat": "NULL";
  case "NUOp::eNaFopen": "NULL";
  case "NUOp::eNaLut": "NULL";
  case "NUOp::eZEndFile": "NULL";
  case "NUOp::eZSysTime": "NULL";
  case "NUOp::eZSysStime": "NULL";
  case "NUOp::eZSysRealTime": "NULL";
  case "NUOp::eInvalid": "NULL";
  }
};

NUBinaryOp {
  let $1 = "getArg (0)";
  let $2 = "getArg (1)";
  switch ("getOp ()") {
  case "NUOp::eBiBitAnd": binaryop ["&"] ("getArg (0)", "getArg (1)");
  // assigning to NULL induces an assertion value at runtime
  case "NUOp::eStart": "NULL";
  case "NUOp::eUnBuf": "NULL";
  case "NUOp::eUnPlus": "NULL";
  case "NUOp::eUnMinus": "NULL";
  case "NUOp::eUnLogNot": "NULL";
  case "NUOp::eUnBitNeg": "NULL";
  case "NUOp::eUnVhdlNot": "NULL";
  case "NUOp::eUnRedAnd": "NULL";
  case "NUOp::eUnRedOr": "NULL";
  case "NUOp::eUnRedXor": "NULL";
  case "NUOp::eUnCount": "NULL";
  case "NUOp::eUnAbs": "NULL";
  case "NUOp::eUnFFZ": "NULL";
  case "NUOp::eUnFFO": "NULL";
  case "NUOp::eUnFLZ": "NULL";
  case "NUOp::eUnFLO": "NULL";
  case "NUOp::eUnRound": "NULL";
  case "NUOp::eUnItoR": "NULL";
  case "NUOp::eUnRtoI": "NULL";
  case "NUOp::eUnRealtoBits": "NULL";
  case "NUOp::eUnBitstoReal": "NULL";
  case "NUOp::eUnChange": "NULL";
  case "NUOp::eBiPlus": "NULL";
  case "NUOp::eBiMinus": "NULL";
  case "NUOp::eBiSMult": "NULL";
  case "NUOp::eBiUMult": "NULL";
  case "NUOp::eBiSDiv": "NULL";
  case "NUOp::eBiUDiv": "NULL";
  case "NUOp::eBiSMod": "NULL";
  case "NUOp::eBiUMod": "NULL";
  case "NUOp::eBiVhdlMod": "NULL";
  case "NUOp::eBiEq": "NULL";
  case "NUOp::eBiNeq": "NULL";
  case "NUOp::eBiTrieq": "NULL";
  case "NUOp::eBiTrineq": "NULL";
  case "NUOp::eBiLogAnd": "NULL";
  case "NUOp::eBiLogOr": "NULL";
  case "NUOp::eBiSLt": "NULL";
  case "NUOp::eBiSLte": "NULL";
  case "NUOp::eBiSGtr": "NULL";
  case "NUOp::eBiSGtre": "NULL";
  case "NUOp::eBiULt": "NULL";
  case "NUOp::eBiULte": "NULL";
  case "NUOp::eBiUGtr": "NULL";
  case "NUOp::eBiUGtre": "NULL";
  //case "NUOp::eBiBitAnd": "NULL";
  case "NUOp::eBiBitOr": binaryop ["|"] ($1, $2);
  case "NUOp::eBiBitXor": binaryop ["^"] ($1, $2);
  case "NUOp::eBiRoR": "NULL";
  case "NUOp::eBiRoL": "NULL";
  case "NUOp::eBiExp": "NULL";
  case "NUOp::eBiDExp": "NULL";
  case "NUOp::eBiVhExt": "NULL";
  case "NUOp::eBiVhZxt": "NULL";
  case "NUOp::eBiVhLshift": "NULL";
  case "NUOp::eBiVhRshift": "NULL";
  case "NUOp::eBiVhLshiftArith": "NULL";
  case "NUOp::eBiVhRshiftArith": "NULL";
  case "NUOp::eBiRshiftArith": "NULL";
  case "NUOp::eBiLshiftArith": "NULL";
  case "NUOp::eBiRshift": "NULL";
  case "NUOp::eBiLshift": "NULL";
  case "NUOp::eBiDownTo": "NULL";
  case "NUOp::eTeCond": "NULL";
  case "NUOp::eNaConcat": "NULL";
  case "NUOp::eNaFopen": "NULL";
  case "NUOp::eNaLut": "NULL";
  case "NUOp::eZEndFile": "NULL";
  case "NUOp::eZSysTime": "NULL";
  case "NUOp::eZSysStime": "NULL";
  case "NUOp::eZSysRealTime": "NULL";
  case "NUOp::eInvalid": "NULL";
  }
};

// Declaration of the rewrite rules
goal e;                                 // the goal symbol

// Lists of statements
//e <- stmtlist "$1";
e <- stmt "$1";
//stmtlist <- stmtpair (stmt, stmtlist) "$1 $2";
//stmtlist <- null "$1";

//
// Assignments
//

lvalue1  <- bitnet      [1] !"mBitNet->getName ()->str ()";
lvalue8  <- vectornet8  [1] !"mBitNet->getName ()->str ()";
lvalue16 <- vectornet16 [1] !"mBitNet->getName ()->str ()";
lvalue32 <- vectornet32 [1] !"mBitNet->getName ()->str ()";
lvalue64 <- vectornet64 [1] !"mBitNet->getName ()->str ()";

stmt <- assign (lvalue1,  rvalue1) [1] "$1 = $2;\n";
stmt <- assign (lvalue8,  rvalue8) [1] "$1 = $2;\n";
stmt <- assign (lvalue16, rvalue16) [1] "$1 = $2;\n";
stmt <- assign (lvalue32, rvalue32) [1] "$1 = $2;\n";
stmt <- assign (lvalue64, rvalue64) [1] "$1 = $2;\n";

//
// Expressions
//

// One-bit wide values
rvalue1 <- bitnet [1] !"mBitNet->getName ()->str ()";
rvalue1 <- one [1]     "0x1";
rvalue1 <- zero [1]    "0x0";

// Eight-bit wide values
rvalue8 <- constant8    [1] "*mValue";    // mValue is a DynBitVector member of LIR
rvalue8 <- vectornet8   [1] !"mBitNet->getName ()->str ()";

// Sixteen-bit wide values
rvalue16 <- constant16    [1] "*mValue";    // mValue is a DynBitVector member of LIR
rvalue16 <- vectornet16   [1] !"mBitNet->getName ()->str ()";

// Thirytwo-bit wide values
rvalue32 <- constant32    [1] "*mValue";    // mValue is a DynBitVector member of LIR
rvalue32 <- vectornet32   [1] !"mBitNet->getName ()->str ()";

// Sixtyfour-bit wide values
rvalue64 <- constant64    [1] "*mValue";    // mValue is a DynBitVector member of LIR
rvalue64 <- vectornet64   [1] !"mBitNet->getName ()->str ()";

// Widening size coercions
rvalue8  <- rvalue1 [1] "static_cast <UInt8> ($1)";
rvalue16 <- rvalue1 [1] "static_cast <UInt16> ($1)";
rvalue32 <- rvalue1 [1] "static_cast <UInt32> ($1)";
rvalue64 <- rvalue1 [1] "static_cast <UInt64> ($1)";
rvalue16 <- rvalue8 [1] "static_cast <UInt16> ($1)";
rvalue32 <- rvalue8 [1] "static_cast <UInt32> ($1)";
rvalue64 <- rvalue8 [1] "static_cast <UInt64> ($1)";
rvalue32 <- rvalue16 [1] "static_cast <UInt32> ($1)";
rvalue64 <- rvalue16 [1] "static_cast <UInt64> ($1)";
rvalue64 <- rvalue32 [1] "static_cast <UInt64> ($1)";

// Binary expression
rvalue1  <- binaryop (rvalue1, rvalue1)   [1] "($1 $op $2)";
rvalue8  <- binaryop (rvalue8, rvalue8)   [1] "($1 $op $2)";
rvalue16 <- binaryop (rvalue16, rvalue16) [1] "($1 $op $2)";
rvalue32 <- binaryop (rvalue32, rvalue32) [1] "($1 $op $2)";
rvalue64 <- binaryop (rvalue64, rvalue64) [1] "($1 $op $2)";

// Unary expression
rvalue1  <- unaryop (rvalue1)  [1] "$op($1)";
rvalue8  <- unaryop (rvalue8)  [1] "$op($1)";
rvalue16 <- unaryop (rvalue16) [1] "$op($1)";
rvalue32 <- unaryop (rvalue32) [1] "$op($1)";
rvalue64 <- unaryop (rvalue64) [1] "$op($1)";
