//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2008-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CfgXmlParserMem.h"
#include "cfg/CarbonCfg.h"
#include "util/UtIStream.h"

CfgXmlParserMem::CfgXmlParserMem()
{
}

CfgXmlParserMem::~CfgXmlParserMem()
{
}


bool CfgXmlParserMem::parseStream(UtIStream &s, CarbonCfg *cfg)
{
  parseSetup();

  // Fill a buffer with the XML text, then create a
  // document from it.
  xmlBuffer *buf = xmlBufferCreate();
  bool done = false;
  UtString line;
  while (!done) {
    s.getline(&line);
    xmlBufferWriteChar(buf, line.c_str());
    // check for end of XML or EOF
    if (line.empty() || (line.find("</meminfo>") != UtString::npos))
      done = true;
  }

  xmlDoc *doc = xmlParseDoc(xmlBufferContent(buf));

  if (doc == 0)
    reportError("Failed to parse memory XML");
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "meminfo"))
      reportError("Root element of memory XML must be 'meminfo'");
    else
      parseMemInfo(rootNode, cfg);
  }

  xmlFreeDoc(doc);
  xmlBufferFree(buf);
  parseCleanup();

  return !getError();
}

void CfgXmlParserMem::parseMemInfo(xmlNode *node, CarbonCfg *cfg)
{
  // walk all children and parse them
  for (xmlNode *child = node->children; child != NULL; child = child->next)
    if (isElement(child, "memSpace"))
      parseMemSpace(child, cfg);
    else if (isElement(child, "elfloader"))
      parseELFLoader(child, cfg);
    else if (isElement(child, "procInfo"))
      parseProcInfo(child, cfg);
}

void CfgXmlParserMem::parseMemSpace(xmlNode *node, CarbonCfg *cfg)
{
  // get memory name
  CfgXmlAttr nameAttr(node, "name");
  // todo: here we should check that the name is a valid C-identifier, as it will be used within one
  if (nameAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<memSpace> element with no name attribute is not allowed.");
  }

  // get memory width
  UInt32 width = 0;
  CfgXmlAttr widthAttr(node, "width");
  if (widthAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<memSpace> element with no width attribute is not allowed.");
  }
  else {
    UtString widthStr(widthAttr.value());
    widthStr >> width;
  }

  // get memory max address
  UInt64 maxAddr = 0;
  CfgXmlAttr maxAddrAttr(node, "maxaddr");
  if (maxAddrAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<memSpace> element with no maxaddr attribute is not allowed.");
  }
  else {
    UtString maxAddrStr(maxAddrAttr.value());
    maxAddrStr >> maxAddr;
  }

  // Add a new memory to configuration
  CarbonCfgMemory* memSpace = cfg->addMemory(nameAttr.value(), width, maxAddr);

  // Check if memSpace was added successfully
  if(memSpace == NULL) {
    UtString err;
    err << "Adding memory space failed while attempting to add memory " << nameAttr.value() << "\n";
    reportError(err.c_str());
    return;
  }

  // get minimum addressable unit (Not yet supported.)
  CfgXmlAttr mauAttr(node, "mau");
  if (mauAttr.value() != NULL) {
    UtString mauStr(mauAttr.value());
    UInt32 mau;
    mauStr >> mau;
    memSpace->putMAU(mau);
  }

  // get endianess (Not yet supported.)
  CfgXmlAttr endianAttr(node, "endian");
  if (endianAttr.value() != NULL) {
    if(strcmp("big", endianAttr.value()) == 0)
      memSpace->putBigEndian(true);
    else if(strcmp("little", endianAttr.value()) == 0)
      memSpace->putBigEndian(false);
  }

  // Get if this memory is a program memory (required for RVDS to connect)
  CfgXmlAttr programMemoryAttr(node, "programMemory");
  if (programMemoryAttr.value() != NULL) {
    if (strcmp("true", programMemoryAttr.value()) == 0) {
      memSpace->putProgramMemory(true);
    } else if (strcmp("false", programMemoryAttr.value()) == 0) {
      memSpace->putProgramMemory(false);
    } else {
      reportError("<memSpace> programMemory attribute with an invalid value. Must be true of false");
    }
  }

  // walk all children and parse them
  for (xmlNode *child = node->children; child != NULL; child = child->next)
    if (isElement(child, "comment")) {
      UtString comment;
      getContent(child, &comment);
      memSpace->putComment(comment.c_str());
    }

    else if (isElement(child, "readMemInit")) {
      memSpace->putMemInitType(eCarbonCfgMemInitReadmem);
      CfgXmlAttr initTypeAttr(child, "type");
      if(initTypeAttr.value() != NULL) {
        CarbonCfgReadmemTypeIO rt;
        rt.parseString(initTypeAttr.value());
        memSpace->putReadmemType(rt);
      }
      for (xmlNode *rmiChild = child->children; rmiChild != NULL; rmiChild = rmiChild->next)
        if(isElement(rmiChild, "initFile")) {
          UtString initFile;
          getContent(rmiChild, &initFile);
          memSpace->putInitFile(initFile.c_str());
        }
    }

    else if (isElement(child, "debugAccess")) {
      memSpace->putMemInitType(eCarbonCfgMemInitProgPreload);
      qint64 baseAddress = 0;
      CfgXmlAttr portAttr(child, "baseAddress");
      if (portAttr.value() != NULL)
      {
        QString v = portAttr.value();
        baseAddress = v.toLongLong();
      }

      for (xmlNode *dbaChild = child->children; dbaChild != NULL; dbaChild = dbaChild->next)
        if(isElement(dbaChild, "debugPort")) {
          CfgXmlAttr nameAttr(dbaChild, "name");
          if(nameAttr.value() != NULL) {
            memSpace->addSystemAddressESLPort(nameAttr.value(), baseAddress);
          }
          else {
            reportError("<debugPort> element with no name attribute is not allowed.");
          }
        }
    }
      
    else if (isElement(child, "disassembly")) {
      CfgXmlAttr nameAttr(child, "name");
      if(nameAttr.value() != NULL)
        memSpace->putMemoryDisassemblyName(nameAttr.value());
      else
        reportError("<dissasembly> element with no name attribute is not allowed.");

      CfgXmlAttr classAttr(child, "class");
      if(classAttr.value() != NULL) {
        memSpace->putMemoryDisassemblerName(classAttr.value());

        // Check for duplicate class name
        CarbonCfgCadi* cadi = cfg->getCadi();
        if(strcmp("", cadi->getDisassemblerName()) != 0 && strcmp(classAttr.value(), cadi->getDisassemblerName()) != 0)
          reportError("Multiple memories with different disassemblers are not allowed.");
        else
          cadi->putDisassemblerName(classAttr.value());
      }
      else
        reportError("<dissasembly> element with no class attribute is not allowed.");
    }

    else if (isElement(child, "memBlock")) {
      parseMemBlock(child, memSpace);
    }
    else if (isElement(child, "displayAtZero")) {
      UtString val;
      getContent(child, &val);
      if (val == "true")       memSpace->putDisplayAtZero(true); 
      else if (val == "false") memSpace->putDisplayAtZero(false);
      else
        reportError("<displayAtZero> Element should be either 'true' or 'false'.");
    } 

    else if (isElement(child, "customcode")) {
      CfgXmlParserCustomCode parseCustom(child, memSpace->getLegalSections());
      bool success = parseCustom.parse(memSpace);
      if (!success) {
        reportError(errorText());
      }
    }

}

void CfgXmlParserMem::parseMemBlock(xmlNode *node, CarbonCfgMemory* memSpace)
{
  // Add Memory block to the Memory Space
  CarbonCfgMemoryBlock* memBlock = memSpace->addMemoryBlock();

  CfgXmlAttr nameAttr(node, "name");
  if(nameAttr.value() != NULL)
    memBlock->putName(nameAttr.value());

  CfgXmlAttr baseAddrAttr(node, "addr");
  if(baseAddrAttr.value() != NULL) {
    UInt64 baseAddr;
    UtString baseAddrStr(baseAddrAttr.value());
    baseAddrStr >> baseAddr;
    memBlock->putBase(baseAddr);
  }

  CfgXmlAttr sizeAttr(node, "size");
  if(sizeAttr.value() != NULL) {
    UInt64 size;
    UtString sizeStr(sizeAttr.value());
    sizeStr >> size;
    memBlock->putSize(size);
  }
  
  // Parse Locators
  for (xmlNode *child = node->children; child != NULL; child = child->next)
    if(isElement(child, "memLoc")) {
      CfgXmlAttr typeAttr(child, "type");
      if(typeAttr.value() != NULL) {
        UtString val(typeAttr.value());
        if(val == "rtl")
          parseRTLLoc(child, memBlock);
        else if(val == "port")
          parsePortLoc(child, memBlock);
        else if(val == "user")
          parseUserLoc(child, memBlock);
        else {
          UtString err;
          err << "A '<memLoc' element contained an unknown type definition: type=\"" << val <<  "\". (Note: all type definition names must be lowercase.)\n";
          reportError(err.c_str());
        }
      }
    }
    else if (isElement(child, "customcode")) {
      CfgXmlParserCustomCode parseCustom(child, memBlock->getLegalSections());
      bool success = parseCustom.parse(memBlock);
      if (!success) {
        reportError(errorText());
      }
    }
}

void CfgXmlParserMem::parseRTLLoc(xmlNode *node, CarbonCfgMemoryBlock* memBlock)
{
  // Add Locator object to Memory Block
  CarbonCfgMemoryLocRTL* loc = memBlock->addLocRTL();
  unsigned int count_path = 0;
  unsigned int count_display = 0;
  unsigned int count_rtl = 0;

  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if(isElement(child, "path")) {
      UtString path;
      getContent(child, &path);
      loc->putPath(path.c_str());
      ++count_path;
    }
    if(isElement(child, "display")) {
      CarbonCfgMemoryLocAttr attr = loc->getDisplayAttr();
      parseLocAttr(child, &attr);
      loc->putDisplayAttr(attr);
      ++count_display;
    }
    if(isElement(child, "rtl")) {
      CarbonCfgMemoryLocAttr attr = loc->getRTLAttr();
      parseLocAttr(child, &attr);
      loc->putRTLAttr(attr);
      ++count_rtl;
    }
  }

  // TODO: the following check would not be needed if validate using DTD/XSD
  if ( ( count_path != 1 ) || (count_display != 1) || (count_rtl != 1) ) {
    UtString err;
    err << "<memLoc type=\"rtl\"> requires a single entry for each of the elements: (<path>, <display>, <rtl>).  Number of entries found ("<< count_path << ", " << count_display << ", " << count_rtl << ")\n";
    reportError(err.c_str());
  }
}

void CfgXmlParserMem::parsePortLoc(xmlNode *node, CarbonCfgMemoryBlock* memBlock)
{
  UtString portName;
  bool     fixed = 0;
  
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if(isElement(child, "portname")) {
      getContent(child, &portName);
    }
    if(isElement(child, "fixedaddr")) {
      getContent(child, &fixed);
    }
  }

  // Check Port Name
  if(portName == "")
    reportError("<memLoc type=\"Port\"> Port name required for Port type memory locators.");
  
  // Add Locator object to Memory Block
  CarbonCfgMemoryLocPort* loc = memBlock->addLocPort(portName.c_str());
  loc->putFixedAddress(fixed);
}

void CfgXmlParserMem::parseUserLoc(xmlNode *node, CarbonCfgMemoryBlock* memBlock)
{
  UtString name;
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if(isElement(child, "name")) {
      getContent(child, &name);
    }
  }

  // Add Locator object to Memory Block
  memBlock->addLocUser(name.c_str());
}

void CfgXmlParserMem::parseLocAttr(xmlNode *node, CarbonCfgMemoryLocAttr* attr)
{
  CfgXmlAttr startAttr(node, "start");
  if (startAttr.value() != NULL) {
    UtString str = startAttr.value();
    str >> attr->mStartWordOffset;
  }
  CfgXmlAttr endAttr(node, "end");
  if (endAttr.value() != NULL) {
    UtString str = endAttr.value();
    str >> attr->mEndWordOffset;
  }
  CfgXmlAttr msbAttr(node, "msb");
  if (msbAttr.value() != NULL) {
    UtString str = msbAttr.value();
    str >> attr->mMsb;
  }
  CfgXmlAttr lsbAttr(node, "lsb");
  if (lsbAttr.value() != NULL) {
    UtString str = lsbAttr.value();
    str >> attr->mLsb;
  }
}

void CfgXmlParserMem::parseELFLoader(xmlNode *node, CarbonCfg *cfg)
{
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if(isElement(child, "section")) {

      UtString name;
      CfgXmlAttr nameAttr(child, "name");
      if(nameAttr.value() != NULL) {
        name = nameAttr.value();
      }
      else {
        reportError("<section> ELF Loader section must have a name attribute.");
        return;
      }

      unsigned int space = 0;
      CfgXmlAttr spaceAttr(child, "space");
      if(spaceAttr.value() != NULL) {
        UtString spaceStr;
        spaceStr = spaceAttr.value();
        spaceStr >> space;
      }
      else {
        reportError("<section> ELF Loader section must have a space attribute.");
        return;
      }

      UtString access;
      CfgXmlAttr accessAttr(child, "access");
      if(accessAttr.value() != NULL) {
        access = accessAttr.value();
      }
      else {
        reportError("<section> ELF Loader section must have a access attribute.");
        return;
      }

      cfg->getELFLoader()->addSection(name.c_str(), space, access.c_str());
    }
  }
}

void CfgXmlParserMem::parseProcInfo(xmlNode *node, CarbonCfg *cfg)
{
  CarbonCfgProcInfo* pi = cfg->getProcInfo();

  // Indicate that this is a processor model
  pi->putIsProcessor(true);

  UtString targetName;
  if(getAttribute(node, "targetName", &targetName)) {
    pi->putTargetName(targetName.c_str());
  }

  UtString debuggerName;
  if(getAttribute(node, "debuggerName", &debuggerName))
    pi->putDebuggerName(debuggerName.c_str());

  UInt32 pipeStages;
  if(getAttribute(node, "pipes", &pipeStages))
    pi->putPipeStages(pipeStages);

  UInt32 hwThreads;
  if(getAttribute(node, "threads", &hwThreads))
    pi->putHwThreads(hwThreads);

  // Process any child elements
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "options")) {
      parseProcessorOptions(child, pi);
    } else if (isElement(child, "pcReg")) {
      parsePcReg(child, pi);
    } else if (isElement(child, "extendedFeaturesReg")) {
      parseExtendedFeaturesReg(child, pi);
    } else if (isElement(child, "debuggable-point")) {
      pi->putDebuggablePoint(true);
    }
  }
}

void CfgXmlParserMem::parseProcessorOptions(xmlNode* node, CarbonCfgProcInfo* pi)
{
  // Add the option elements to the procInfo class
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "option")) {
      UtString option;
      getContent(child, &option);
      pi->addProcessorOption(option.c_str());
    }
  }
}

void CfgXmlParserMem::parsePcReg(xmlNode* node, CarbonCfgProcInfo* pi)
{
  // Get the group and register names
  bool valid = true;
  UtString groupName, regName;
  if (!getAttribute(node, "groupName", &groupName)) {
    reportError("<pcReg> element with no groupName attribute is not allowed.");
    valid = false;
  }
  if (!getAttribute(node, "regName", &regName)) {
    reportError("<pcReg> element with no regName attribute is not allowed.");
    valid = false;
  }

  // Add them to the proc info if we got valid data
  if (valid) {
    pi->putPCRegName(groupName.c_str(), regName.c_str());
  }
}

void CfgXmlParserMem::parseExtendedFeaturesReg(xmlNode* node, CarbonCfgProcInfo* pi)
{
  // Get the group and register names
  bool valid = true;
  UtString groupName, regName;
  if (!getAttribute(node, "groupName", &groupName)) {
    reportError("<extendedFeaturesReg> element with no groupName attribute is not allowed.");
    valid = false;
  }
  if (!getAttribute(node, "regName", &regName)) {
    reportError("<extendedFeaturesReg> element with no regName attribute is not allowed.");
    valid = false;
  }

  // Add them to the proc info if we got valid data
  if (valid) {
    pi->putExtendedFeaturesRegName(groupName.c_str(), regName.c_str());
  }
}
