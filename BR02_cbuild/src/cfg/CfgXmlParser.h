//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// \file Base XML parser with utilities

#ifndef _CfgXmlParser_h_
#define _CfgXmlParser_h_

#include "util/c_memmanager.h"
#include "util/UtString.h"

#include "libxml/parser.h"
#include "libxml/tree.h"
#include "libxml/xpath.h"

//! CfgXmlParser class
class CfgXmlParser
{
 public:
  CARBONMEM_OVERRIDES

  //! constructor
  CfgXmlParser();
  CfgXmlParser(const char* xmlFile);

  //! destructor
  ~CfgXmlParser();

  //! fetch the error message string
  /*!
   *  Fetch any error text that was accumulated by the parser
   */
  const char *errorText();

public:
  bool isElement(xmlNode *node, const char *name);
  bool isCdataNode(xmlNode* node);
  bool hasAttr(xmlNode *node, const char *name);
  void getContent(xmlNode *node, UtString *content);
  void getContent(xmlNode *node, UInt8 *content);
  void getContent(xmlNode *node, UInt32 *content);
  void getContent(xmlNode *node, UInt64 *content);
  void getContent(xmlNode *node, bool *content);
  bool getAttribute(xmlNode *node, const char *name, UtString *attr);
  bool getAttribute(xmlNode *node, const char *name, UInt8 *attr);
  bool getAttribute(xmlNode *node, const char *name, UInt32 *attr);
  bool getAttribute(xmlNode *node, const char *name, UInt64 *attr);
  bool getAttribute(xmlNode *node, const char *name, bool *attr);
  void reportError(const char *message);
  bool getError() const {return mErrorReported;}
  xmlNodePtr getRootNode() { return mRootNode; }

  xmlNodePtr findChildElement(xmlNodePtr root, const char* elementName);

  //! pre-parsing XML package setup
  void parseSetup();
  //! post-parsing XML package cleanup
  void parseCleanup();

 private:
  // error handling
  static void sXmlStructuredErrorHandler(void *arg, xmlErrorPtr error);
  bool mErrorReported;
  UtString mErrorText;
  xmlDocPtr mDocument;
  xmlNodePtr mRootNode;
};

/*!
 * Simple class to get attributes from libxml2 nodes.
 * Hides the casting and takes care of calling xmlFree().
 */
class CfgXmlAttr
{
public:
  CARBONMEM_OVERRIDES

  CfgXmlAttr(xmlNode *node, const char *name) {
    mValue = xmlGetProp(node, BAD_CAST name);
  }

  ~CfgXmlAttr() {
    if (mValue != NULL) {
      xmlFree(mValue);
    }
  }

  const char *value() {
    return (const char *) mValue;
  }

  template<class T>
  bool getValue(T *val) {
    if(mValue != NULL) {
      UtString strVal((const char *) mValue);
      strVal >> *val;
    }
    return mValue != NULL;
  }

private:
  xmlChar *mValue;
};


#endif  // _CfgXmlParser_h_
