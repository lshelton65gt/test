//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// \file XML parser for MxDI registers

#ifndef _CfgXmlParserReg_h_
#define _CfgXmlParserReg_h_

#include "CfgXmlParser.h"
#include "CfgXmlParserCustomCode.h"

class CarbonCfg;
class CarbonCfgGroup;
class CarbonCfgRegister;
class UtIStream;

//! CfgXmlParserReg class
class CfgXmlParserReg : public CfgXmlParser
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  CfgXmlParserReg();

  //! destructor
  ~CfgXmlParserReg();

  //! parse XML register format from a stream
  bool parseStream(UtIStream &s, CarbonCfg *cfg);
  
  //! Parse top level tag
  void parseRegInfo(xmlNode *node, CarbonCfg *cfg);

private:
  void parseGroup(xmlNode *node, CarbonCfg *cfg);
  void parseReg(xmlNode *node, CarbonCfgGroup *group, CarbonCfg *cfg);
  void parseField(xmlNode *node, CarbonCfgRegister *reg);
  void parseCustomCode(xmlNode *node, CarbonCfgRegister* reg);
}; // class CfgXmlParserReg : public CfgXmlParser

#endif
