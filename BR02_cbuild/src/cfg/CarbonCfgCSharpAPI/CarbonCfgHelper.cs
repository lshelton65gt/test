﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Carbon.Ccfg
{
  public partial class CarbonCfg
  {
    #region Properties
   
    public uint NumSubComponents
    {
      get
      {
        return numSubComponents();
      }
    }
    public uint NumXtorInstances
    {
      get
      {
        return numXtorInstances();
      }
    }
    public uint NumESLPorts
    {
      get
      {
        return numESLPorts();
      }
    }
    public uint NumRTLPorts
    {
      get
      {
        return numRTLPorts();
      }
    }
    public uint NumParams
    {
      get
      {
        return numParams();
      }
    }
    public uint NumRegisters
    {
      get
      {
        return numRegisters();
      }
    }
    public uint NumGroups
    {
      get
      {
        return numGroups();
      }
    }
    public uint NumMemories
    {
      get
      {
        return numMemories();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
   
    public string Description
    {
      set
      {
        putDescription(value);
      }
      get
      {
        return getDescription();
      }
    }
    public string CxxFlags
    {
      set
      {
        putCxxFlags(value);
      }
      get
      {
        return getCxxFlags();
      }
    }
    public string LinkFlags
    {
      set
      {
        putLinkFlags(value);
      }
      get
      {
        return getLinkFlags();
      }
    }
    public string SourceFiles
    {
      set
      {
        putSourceFiles(value);
      }
      get
      {
        return getSourceFiles();
      }
    }
    public string IncludeFiles
    {
      set
      {
        putIncludeFiles(value);
      }
      get
      {
        return getIncludeFiles();
      }
    }
    public string LibName
    {
      set
      {
        putLibName(value);
      }
      get
      {
        return getLibName();
      }
    }
    public string CompName
    {
      set
      {
        putCompName(value);
      }
      get
      {
        return getCompName();
      }
    }
    public string CompDisplayName
    {
      set
      {
        putCompDisplayName(value);
      }
      get
      {
        return getCompDisplayName();
      }
    }
    public string TopModuleName
    {
      set
      {
        putTopModuleName(value);
      }
      get
      {
        return getTopModuleName();
      }
    }
    public string WaveFile
    {
      set
      {
        putWaveFile(value);
      }
      get
      {
        return getWaveFile();
      }
    }
    public CarbonCfgWaveType WaveType
    {
      set
      {
        putWaveType(value);
      }
      get
      {
        return getWaveType();
      }
    }
    public CarbonCfgELFLoader ELFLoader
    {
      get
      {
        return getELFLoader();
      }
    }
    public CarbonCfgProcInfo ProcInfo
    {
      get
      {
        return getProcInfo();
      }
    }
    public string Type
    {
      set
      {
        putCompType(value);
      }
      get
      {
        return getCompType();
      }
    }
    public string Version
    {
      set
      {
        putCompVersion(value);
      }
      get
      {
        return getCompVersion();
      }
    }
    public CarbonCfgCadi Cadi
    {
      get
      {
        return getCadi();
      }
    }
    public bool RequiresMkLibrary
    {
      set
      {
        putRequiresMkLibrary(value);
      }
      get
      {
        return getRequiresMkLibrary();
      }
    }
    public bool UseVersionedMkLibrary
    {
      set
      {
        putUseVersionedMkLibrary(value);
      }
      get
      {
        return getUseVersionedMkLibrary();
      }
    }
    // The accessor expression for the SoCDPCTraceReporter instance, if it exists
    public string PCTraceAccessor
    {
      set
      {
        putPCTraceAccessor(value);
      }
      get
      {
        return getPCTraceAccessor();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgCadi
  {
    #region Properties
    public string DisassemblerName
    {
      set
      {
        putDisassemblerName(value);
      }
      get
      {
        return getDisassemblerName();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgCadiCustomCode
  {
    #region Properties
    public CarbonCfgCadiCustomCodeSection Section
    {
      set
      {
        putSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgCompCustomCode
  {
    #region Properties
    public CarbonCfgCompCustomCodeSection Section
    {
      set
      {
        putSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgCustomCode
  {
    #region Properties
    public CarbonCfgCustomCodePosition Position
    {
      set
      {
        putPosition(value);
      }
      get
      {
        return getPosition();
      }
    }
    public string Code
    {
      set
      {
        putCode(value);
      }
      get
      {
        return getCode();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgELFLoader
  {
    #region Properties
    public uint NumSections
    {
      get
      {
        return numSections();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgESLPort
  {
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public string Expr
    {
      set
      {
        putExpr(value);
      }
      get
      {
        return getExpr();
      }
    }
    public string TypeDef
    {
      get
      {
        return getTypeDef();
      }
    }
    public CarbonCfgRTLPort RtlPort
    {
      get
      {
        return getRTLPort();
      }
    }
    public CarbonCfgESLPortType PortType
    {
      get
      {
        return getPortType();
      }
    }
    public CarbonCfgXtorParamInst ParameterInstance
    {
      set
      {        
        putParamInstance(value);
      }
      get
      {
        return getParamInstance();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgGroup
  {  
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemory
  {
    #region Properties
    public uint NumMemoryBlocks
    {
      get
      {
        return numMemoryBlocks();
      }
    }
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public string InitFile
    {
      set
      {
        putInitFile(value);
      }
      get
      {
        return getInitFile();
      }
    }
    public string Comment
    {
      set
      {
        putComment(value);
      }
      get
      {
        return getComment();
      }
    }
    public uint Width
    {
      get
      {
        return getWidth();
      }
    }
    public CarbonCfgReadmemType ReadmemType
    {
      set
      {
        putReadmemType(value);
      }
      get
      {
        return getReadmemType();
      }
    }
    public CarbonCfgMemInitType InitType
    {
      set
      {
        putMemInitType(value);
      }
      get
      {
        return getMemInitType();
      }
    }
    public string ProgPreloadEslPort
    {
      set
      {
        putSystemAddressESLPortName(value);       
      }
      get
      {
        return getSystemAddressESLPortName();
      }
    }
    public string SystemAddressESLPortName
    {
      get
      {
        return getSystemAddressESLPortName();
      }
      set
      {
         putSystemAddressESLPortName(value);       
      }
    }
    public bool DisplayAtZero
    {
      set
      {
        putDisplayAtZero(value);
      }
      get
      {
        return getDisplayAtZero();
      }
    }
    public uint MAU
    {
      set
      {
        putMAU(value);
      }
      get
      {
        return getMAU();
      }
    }
    public bool BigEndian
    {
      set
      {
        putBigEndian(value);
      }
      get
      {
        return getBigEndian();
      }
    }
    public bool ProgramMemory
    {
      set
      {
        putProgramMemory(value);
      }
      get
      {
        return getProgramMemory();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemoryBlock
  {
    #region Properties
    public string Name
    {
      set
      {
        putName(value);
      }
      get
      {
        return getName();
      }
    }
    public ulong Base
    {
      set
      {
        putBase(value);
      }
      get
      {
        return getBase();
      }
    }
    public ulong Size
    {
      set
      {
        putSize(value);
      }
      get
      {
        return getSize();
      }
    }
    public uint Width
    {
      get
      {
        return getWidth();
      }
    }
    public uint NumLocs
    {
      get
      {
        return numLocs();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemoryBlockCustomCode
  {
    #region Properties
    public CarbonCfgMemoryBlockCustomCodeSection Section
    {
      set
      {
        putSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemoryCustomCode
  {
    #region Properties
    public CarbonCfgMemoryCustomCodeSection Section
    {
      set
      {
        putSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemoryLoc
  {
    #region Properties
    public CarbonCfgMemLocType Type
    {
      get
      {
        return getType();
      }
    }
    public ulong DisplayStartWordOffset
    {
      set
      {
        putDisplayStartWordOffset(value);
      }
      get
      {
        return getDisplayStartWordOffset();
      }
    }
    public ulong DisplayEndWordOffset
    {
      set
      {
        putDisplayEndWordOffset(value);
      }
      get
      {
        return getDisplayEndWordOffset();
      }
    }
    public CarbonCfgMemoryLocRTL CastRTL
    {
      get
      {
        return castRTL();
      }
    }
    public CarbonCfgMemoryLocPort CastPort
    {
      get
      {
        return castPort();
      }
    }
    public CarbonCfgMemoryLocUser CastUser
    {
      get
      {
        return castUser();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemoryLocPort
  {
    #region Properties
    public bool FixedAddress
    {
      get
      {
        return hasFixedAddress();
      }
      set
      {
        putFixedAddress(value);
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemoryLocRTL
  {
    #region Properties
    public string Path
    {
      set
      {
        putPath(value);
      }
      get
      {
        return getPath();
      }
    }
    public ulong StartWordOffset
    {
      set
      {        
        putRTLStartWordOffset(value);
      }
      get
      {
        return getRTLStartWordOffset();
      }
    }
    public ulong EndWordOffset
    {
      set
      {
       putRTLEndWordOffset(value);
      }
      get
      {
        return getRTLEndWordOffset();
      }
    }
    public uint BitWidth
    {
      get
      {
        return getBitWidth();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgMemoryLocUser
  {
    #region Properties
    public string Name
    {
      set
      {
        putName(value);
      }
      get
      {
        return getName();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgProcInfo
  {
    #region Properties
    public string DebuggerName
    {
      set
      {
        putDebuggerName(value);
      }
      get
      {
        return getDebuggerName();
      }
    }
    public uint PipeStages
    {
      set
      {
        putPipeStages(value);
      }
      get
      {
        return getPipeStages();
      }
    }
    public uint HwThreads
    {
      set
      {
        putHwThreads(value);
      }
      get
      {
        return getHwThreads();
      }
    }
    public uint NumProcessorOptions
    {
      get
      {
        return getNumProcessorOptions();
      }
    }
    public string ExtendedFeaturesRegGroupName
    {
      get
      {
        return getExtendedFeaturesRegGroupName();
      }
    }
    public string ExtendedFeaturesRegName
    {
      get
      {
        return getExtendedFeaturesRegName();
      }
    }
    public string TargetName
    {
      set
      {
        putTargetName(value);
      }
      get
      {
        return getTargetName();
      }
    }
    public bool DebuggablePoint
    {
      set
      {
        putDebuggablePoint(value);
      }
      get
      {
        return getDebuggablePoint();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRegCustomCode
  {
    #region Properties
    public CarbonCfgRegCustomCodeSection Section
    {
      set
      {
        putSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRegister
  {
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint Width
    {
      set
      {
        putWidth(value);
      }
      get
      {
        return getWidth();
      }
    }
    public string Comment
    {
      set
      {
        putComment(value);
      }
      get
      {
        return getComment();
      }
    }
    public uint Offset
    {
      set
      {
        putOffset(value);
      }
      get
      {
        return getOffset();
      }
    }
    public bool BigEndian
    {
      set
      {
        putBigEndian(value);
      }
      get
      {
        return getBigEndian();
      }
    }
    public string Port
    {
      set
      {
        putPort(value);
      }
      get
      {
        return getPort();
      }
    }
    public uint NumFields
    {
      get
      {
        return numFields();
      }
    }
    public string GroupName
    {
      get
      {
        return getGroupName();
      }
    }
    public CarbonCfgRadix Radix
    {
      set
      {
        putRadix(value);
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRegisterField
  {
    #region Properties
    public string Name
    {
      set
      {
        putName(value);
      }
      get
      {
        return getName();
      }
    }
    public uint Width
    {
      get
      {
        return getWidth();
      }
    }
    public uint High
    {
      set
      {
        putHigh(value);
      }
      get
      {
        return getHigh();
      }
    }
    public uint Low
    {
      set
      {
        putLow(value);
      }
      get
      {
        return getLow();
      }
    }
    public CarbonCfgRegAccessType Access
    {
      set
      {
        putAccess(value);
      }
      get
      {
        return getAccess();
      }
    }
    public CarbonCfgRegisterLocReg RegLoc
    {
      get
      {
        return getLoc().castReg();
      }
    }
    public CarbonCfgRegisterLocConstant ConstantLoc
    {
      get
      {
        return getLoc().castConstant();
      }
    }
    public CarbonCfgRegisterLocArray ArrayLoc
    {
      get
      {
        return getLoc().castArray();
      }
    }
    public CarbonCfgRegisterLocUser UserLoc
    {
      get
      {
        return getLoc().castUser();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRegisterLoc
  {
    #region Properties
    public CarbonCfgRegisterLocConstant CastConstant
    {
      get
      {
        return castConstant();
      }
    }
    public CarbonCfgRegisterLocRTL CastRTL
    {
      get
      {
        return castRTL();
      }
    }
    public CarbonCfgRegisterLocReg CastReg
    {
      get
      {
        return castReg();
      }
    }
    public CarbonCfgRegisterLocArray CastArray
    {
      get
      {
        return castArray();
      }
    }
    public CarbonCfgRegisterLocUser CastUser
    {
      get
      {
        return castUser();
      }
    }
    public CarbonCfgRegLocType Type
    {
      get
      {
        return getType();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRegisterLocArray
  {
    #region Properties
    public uint Index
    {
      set
      {
        putIndex(value);
      }
      get
      {
        return getIndex();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRegisterLocConstant
  {
    #region Properties
    public ulong Value
    {
      set
      {
        putValue(value);
      }
      get
      {
        return getValue();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRegisterLocRTL
  {
    #region Properties
    public bool HasPath
    {
      get
      {
        return hasPath();
      }
    }
    public bool HasRange
    {
      get
      {
        return getHasRange();
      }
    }
    public string Path
    {
      set
      {
        putPath(value);
      }
      get
      {
        return getPath();
      }
    }
    public uint Left
    {
      set
      {
        putLeft(value);
      }
      get
      {
        return getLeft();
      }
    }
    public uint Right
    {
      set
      {
        putRight(value);
      }
      get
      {
        return getRight();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRTLConnection
  {
    #region Properties
    public CarbonCfgRTLConnectionType Type
    {
      get
      {
        return getType();
      }
    }
    public CarbonCfgClockGen ClockGen
    {
      get
      {
        return castClockGen();
      }
    }
    public CarbonCfgResetGen ResetGen
    {
      get
      {
        return castResetGen();
      }
    }
    public CarbonCfgTie Tie
    {
      get
      {
        return castTie();
      }
    }
    public CarbonCfgTieParam TieParam
    {
      get
      {
        return castTieParam();
      }
    }
    public CarbonCfgXtorConn XtorConn
    {
      get
      {
        return castXtorConn();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgRTLPort
  {
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint Width
    {
      get
      {
        return getWidth();
      }
    }
    public CarbonCfgRTLPortType PortType
    {
      get
      {
        return getType();
      }
    }
    public uint NumConnections
    {
      get
      {
        return numConnections();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgTemplate
  {
    #region Properties
    public uint NumVariables
    {
      get
      {
        return getNumVariables();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgXtor
  {
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint NumPorts
    {
      get
      {
        return numPorts();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgXtorInstance
  {
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint NumConnections
    {
      get
      {
        return getType().numPorts();
      }
    }
    public uint NumParams
    {
      get
      {
        return numParams();
      }
    }
    public CarbonCfgXtorInstance ClockMaster
    {
      set
      {
        putClockMaster(value);
      }
      get
      {
        return getClockMaster();
      }
    }
    public CarbonCfgXtor Xtor
    {
      get
      {
        return getType();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgXtorLib
  {
    #region Properties
    public uint NumXtors
    {
      get
      {
        return numXtors();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgXtorParam
  {
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint Size
    {
      get
      {
        return getSize();
      }
    }
    public CarbonCfgParamFlag Flag
    {
      get
      {
        return getFlag();
      }
    }
    public CarbonCfgParamDataType Type
    {
      get
      {
        return getType();
      }
    }
    public string Description
    {
      get
      {
        return getDescription();
      }
    }
    public string EnumChoices
    {
      get
      {
        return getEnumChoices();
      }
    }
    public string DefaultValue
    {
      get
      {
        return getDefaultValue();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgXtorParamInst
  {
    #region Properties
    public string Value
    {
      set
      {
        putValue(value);
      }
      get
      {
        return getValue();
      }
    }
    public string DefaultValue
    {
      get
      {
        return getDefaultValue();
      }
    }
    public uint NumRTLPorts
    {
      get
      {
        return numRTLPorts();
      }
    }
    public uint NumEnumChoices
    {
      get
      {
        return numEnumChoices();
      }
    }
    public CarbonCfgXtorInstance Instance
    {
      get
      {
        return getInstance();
      }
    }
    public CarbonCfgXtorParam Param
    {
      get
      {
        return getParam();
      }
    }
    public bool Hidden
    {
      set
      {
        putHidden(value);
      }
      get
      {
        return hidden();
      }
    }
    #endregion Properties
  }

  public partial class CarbonCfgXtorPort
  {
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint Size
    {
      get
      {
        return getSize();
      }
    }
    public CarbonCfgRTLPortType Type
    {
      get
      {
        return getType();
      }
    }
    #endregion Properties
  }

}
