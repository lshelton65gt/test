// -*-C++-*-
/*****************************************************************************
Copyright (c) 2008-2012 by Carbon Design Systems, Inc., All Rights Reserved. 

THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
\file
Carbon configuration file data model for all components

Note that more code should be moved here as possible. It was not a good
time to move large amounts of code at the time that I added this file
due to model kits.
*/

#include "cfg/CarbonCfg.h"

#include "shell/carbon_capi.h"
#include "shell/carbon_dbapi.h"
#include "shell/CarbonDatabasePriv.h"
#include "iodb/IODBRuntime.h"
#include "util/DynBitVector.h"

#include "CfgXmlParser.h"

// Required for the QtScripting engine
QCoreApplication* qCoreApplication = NULL;


CarbonCfg::CarbonCfg() : QObject(0),
mCustomCodes(CarbonCfgCompCustomCodeSectionIO().getStrings()), 
mTemplateVariables(&mMsgHandler), mCompType("Other"), mCompVersion("0.1"), mCompDocFile(""),
mIsStandAlone(false), mIsSubComponentFullName(false)
{
  init(eRvsocd);
}

CarbonCfg::CarbonCfg(Mode mode) : QObject(0),
mCustomCodes(CarbonCfgCompCustomCodeSectionIO().getStrings()),
mTemplateVariables(&mMsgHandler), mCompType("Other"), mCompVersion("0.1"), mCompDocFile(""),
mIsStandAlone(false), mIsSubComponentFullName(false)
{
  init(mode);
}

// Associate the CarbonDB (.gui.db or .io.db or .symtab.db) with this ccfg
void CarbonCfg::createDefaultComponent(const char* dbFileName, const char* designHierarchyFile, CarbonXtorsType xtorsType)
{
  QFileInfo fi(dbFileName);
  if (fi.exists())
  {
    CarbonDB* db = carbonDBCreate(dbFileName, 0);
    if (db != NULL)
      createComponent(db, dbFileName, designHierarchyFile, xtorsType);
  }
}

const char* CarbonCfg::cbuildVersion()
{
  return carbonDBGetVersion();
}

void CarbonCfg::createComponent(CarbonDB* db, const char* dbFileName, const char* designHierarchyFile, CarbonXtorsType xtorsType)
{
  putLegacyMemories(0);
  readXtorLib(xtorsType);
  putIODBFileName(dbFileName);

  const char* topModuleName = carbonDBGetTopLevelModuleName(db);

  putTopModuleName(topModuleName);
  putCompName(topModuleName);
 
  bool populateFromDB = true;
  if (designHierarchyFile)
  {
    QString dh(designHierarchyFile);
    if (!dh.isEmpty())
    {
      QFileInfo fi(designHierarchyFile);
      if (fi.exists())
      {
        //
        // I realize this function is long, it's only because libxml is not exposed through the CarbonCfg header
        // file, and I didn't want to incur that change because public interfaces are created from it, its
        // a litte long but not too bad.
        //
        CfgXmlParser xp(designHierarchyFile);
        xmlNodePtr root = xp.getRootNode();
        if (xp.isElement(root, "DesignHierarchy"))
        {
          populateFromDB = false;
          xmlNodePtr interfacesNode = xp.findChildElement(root, "Interfaces");
          if (interfacesNode)
          {
            for (xmlNodePtr node = interfacesNode->children; node != NULL; node = node->next)
            {
              if (xp.isElement(node, "Interface"))
              {
                UtString ifaceName;
                if (xp.getAttribute(node, "name", &ifaceName))
                {
                  if (ifaceName == topModuleName)
                  {
                    xmlNodePtr portsNode = xp.findChildElement(node, "Ports");
                    for (xmlNodePtr portNode = portsNode->children; portNode != NULL; portNode = portNode->next)
                    {
                      if (xp.isElement(portNode, "Port"))
                      {
                        UtString portName; 
                        UtString portDir;
                        UInt32 portWidth = 0;

                        xp.getAttribute(portNode, "name", &portName);
                        xp.getAttribute(portNode, "direction", &portDir);
                        xp.getAttribute(portNode, "width", &portWidth);

                        UtString fullName;
                        fullName << topModuleName << "." << portName;
                        
                        CarbonCfgRTLPortType portType;
                        if (portDir == "IN")
                          portType = eCarbonCfgRTLInput;
                        else if (portDir == "BIDI")
                          portType = eCarbonCfgRTLInout;
                        else
                          portType = eCarbonCfgRTLOutput;

                        CarbonCfgRTLPort* port = addRTLPort(fullName.c_str(), portWidth, portType);
                        if (port != NULL)
                        {
                          (void) addConnection(db, port);
                        }
                      }
                    }
                    break;
                  }
                }
              }                
            }
          }
        }
      }
      else
      {
        UtString errMsg;
        errMsg << "Invalid Design Hierarchy File: " << designHierarchyFile;
        reportMessage(eCarbonMsgError, errMsg.c_str());
        return;
      }
    }
  }

  if (populateFromDB)
  {
    populateLoop(db, eCarbonCfgRTLInput,  carbonDBLoopPrimaryInputs(db));
    populateLoop(db, eCarbonCfgRTLInout,  carbonDBLoopPrimaryBidis(db));
    populateLoop(db, eCarbonCfgRTLOutput, carbonDBLoopPrimaryOutputs(db));
    populateLoop(db, eCarbonCfgRTLInput,  carbonDBLoopScDepositable(db));
    populateLoop(db, eCarbonCfgRTLOutput, carbonDBLoopScObservable(db));

   // Turn Tie'd ports into Ties
    for(UInt32 i=0; i<numESLPorts(); i++)
    {
      CarbonCfgESLPort* eslPort = getESLPort(i);
      CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
      if (rtlPort)
      {
        const CarbonDBNode* node = carbonDBFindNode(db, rtlPort->getName());
        if (carbonDBIsTied(db, node))
        {
          const IODBRuntime* iodb = CarbonDatabasePriv::getIODB(db);
          const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
          const DynBitVector* bv = iodb->getTieValue(stNode);
          UtString valStr;
          bv->format(&valStr, eCarbonHex);
          disconnect(eslPort);
          UInt32 tieValue = bv->value();
          carbonCfgTie(rtlPort, &tieValue, 1); 
        }
      }
    }
  }
}

void CarbonCfg::populateLoop(CarbonDB* carbonDB, CarbonCfgRTLPortType type, CarbonDBNodeIter* iter)
{
  const CarbonDBNode* node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL)
  {
    int width = carbonDBGetWidth(carbonDB, node);
    const char* name = carbonDBNodeGetFullName(carbonDB, node);

    CarbonCfgRTLPort* port = addRTLPort(name, (UInt32)width, type);
    if (port != NULL)
    {
      (void) addConnection(carbonDB, port);
    }
  }
  carbonDBFreeNodeIter(iter);
}

CarbonCfgESLPort* CarbonCfg::addConnection(CarbonDB* carbonDB, CarbonCfgRTLPort* rtlPort)
{
  CarbonCfgESLPortType portType = CarbonCfg::convertRTLPortType(rtlPort);
  return addESLConnection(carbonDB, rtlPort, portType);
}

CarbonCfgESLPort* CarbonCfg::addESLConnection(CarbonDB* carbonDB, CarbonCfgRTLPort* rtlPort,
                                                    CarbonCfgESLPortType type)
{
  // Get a leaf name for the RTL port, use that as a starting point for
  // the ESL name
  const CarbonDBNode* node = carbonDBFindNode(carbonDB, rtlPort->getName());
  UtString buf;
  if (node == NULL) {
    getUniqueESLName(&buf, rtlPort->getName());
  }
  else {
    getUniqueESLName(&buf, carbonDBNodeGetLeafName(carbonDB, node));
  }

  CarbonCfgESLPortID eslPort = carbonCfgAddESLPort(this, rtlPort, buf.c_str(), type);

  INFO_ASSERT(eslPort, "Failed to create ESL port");
  return eslPort;
}

CarbonCfg::~CarbonCfg()
{
  clear();
  for (LoopMap<XtorLibs> l(mXtorLibs); !l.atEnd(); ++l) {
    CarbonCfgXtorLib* xtorLib = l.getValue();
    delete xtorLib;
  }
  mXtorLibs.clear();
  if (mMsgCallback != NULL) {
    carbonRemoveMsgCB(NULL, &mMsgCallback);
  }
}

void CarbonCfg::init(Mode mode)
{
  initHelper(mode);

  mMsgCallback = carbonAddMsgCB(NULL, sMsgCallback, this);
}

#ifdef CARBON_PV      
CarbonCfg::CarbonCfg(CarbonContext*) {
  initHelper(eRvsocd);
}
#endif
void CarbonCfg::initHelper(Mode mode)
{
  // Default to user-created components (as opposed to script created ones)
  mElementGenerationType = eCarbonCfgElementUser;
  mEditFlags = CcfgFlags::EditComponentAll;
  mCcfgDebugOutput = getenv("CARBON_CCFG_DEBUG") ? true : false;
  mScriptingDB = NULL;
  mReadXtorLibrary = false;
  mMsgCallback = NULL;
  mMode = mode;
#ifdef CARBON_PV
  mPVContext = NULL;
#endif
  clear();
  mRequiresMkLibrary = false;
  mUseVersionedMkLibrary = false;
  mIsModified = false;

  // Default is false so that old ccfg files use the new flow through code
  mUseStaticScheduling = 0;

  mSystemCTypes.push_back("bool");
  mSystemCTypes.push_back("sc_uint");
  mSystemCTypes.push_back("sc_biguint");
  mSystemCTypes.push_back("sc_logic");
  mSystemCTypes.push_back("sc_lv");
  mSystemCTypes.push_back("sc_bv");
  mSystemCTypes.push_back("unsigned int");
  mSystemCTypes.push_back("unsigned char");
  addXtorLib(new CarbonCfgXtorLib(CARBON_DEFAULT_XTOR_LIB));

  // Init the QCoreApplication if necessary
  if (qCoreApplication == NULL && !QCoreApplication::instance())
  {     
    char** argv = NULL;
    int args = 0;
    qCoreApplication = new QCoreApplication(args, argv);
  }
  // buildup the scriptable module map
  findEmbeddedModuleScripts();
}


void CarbonCfg::findEmbeddedModuleScripts()
{
  mScriptableModuleMap.clear();

  QStringList pathList;
  if (getenv("CARBON_EM_PATH") != NULL)
  {
    QString emPath = getenv("CARBON_EM_PATH");
#if pfWINDOWS
    QStringList pathLets = emPath.split(";");
#else
    QStringList pathLets = emPath.split(":");
#endif
    foreach(QString scriptDir, pathLets)
      pathList.append(scriptDir);
  }

  QString scriptsHomePath = QString("%1/lib/carbonRTL").arg(getenv("CARBON_HOME"));
  pathList.append(scriptsHomePath);

  // walk the search paths
  foreach(QString scriptsPath, pathList)
  {
    QDir qdir(scriptsPath); 
    if (qdir.isReadable())
    {
      qdir.setFilter(QDir::Dirs);

      QStringList moduleList = qdir.entryList();
      foreach (QString module, moduleList)
      {
        QDir moduleDir(scriptsPath + "/" + module);
        moduleDir.setFilter(QDir::Dirs);

        foreach(QString subDir, moduleDir.entryList())
        {
          if (subDir.toLower() == "scripts")
          {
            QString scriptFileName = QString("%1/%2/%3.js").arg(moduleDir.absolutePath()).arg(subDir).arg(module);
            QFileInfo fi(scriptFileName);
            if (fi.exists())
            {
              if(mCcfgDebugOutput)
                qDebug() << "found module script" << scriptFileName;

              // first one wins
              if (!mScriptableModuleMap.contains(module))
                mScriptableModuleMap[module] = scriptFileName;
            }
          }
        }   
      }
    }
  }
}
void CarbonCfg::removeSubCompRecursive(CarbonCfg* cfg)
{
  // Remove all custom code sections
  cfg->removeCustomCodes();

  // Remove all sub registers recursively
  cfg->removeRegistersRecursive();

  // Remove all groups. It doesn't have to be recursive because we
  // removed all register. We should rethink the data model!
  while (cfg->numGroups() > 0) {
    CarbonCfgGroup* group = cfg->getGroup(0);
    cfg->removeGroup(group);
  }

  // Remove all sub memories recursively
  cfg->removeMemoriesRecursive();

  // Remove all sub components recursively
  cfg->removeSubCompsRecursive();

  // Now we can remove this sub component. This deletes the memory and
  // removes it from this CarbonCfg.
  removeSubComp(cfg);
}

void CarbonCfg::removeXtorInstanceRecursive(CarbonCfgXtorInstance* xtorInst)
{
  // Remove all the connections first
  for (UInt32 i = 0; i < xtorInst->getType()->numPorts(); i++) {
    CarbonCfgXtorConn* conn = xtorInst->getConnection(i);
    CarbonCfgRTLPort* rtlPort = conn->getRTLPort();
    if (rtlPort != NULL) {
      rtlPort->disconnect(conn);
    }
  }

  // Now remove the xtor instance. This deletes the memory and removes
  // it from this CarbonCfg.
  removeXtorInstance(xtorInst);
}

void CarbonCfg::removeMemoryRecursive(CarbonCfgMemory* mem)
{
  // Walk the memory blocks and remove them first
  while (mem->numMemoryBlocks() > 0) {
    CarbonCfgMemoryBlock* block = mem->getMemoryBlock(0);
    mem->removeMemoryBlock(block);
  }

  // Now we can remove the memory. This deletes the memory and removes
  // it from this CarbonCfg.
  removeMemory(mem);
}

void CarbonCfg::removeRegisterRecursive(CarbonCfgRegister* reg)
{
  // Remove all custom code sections
  while (reg->numCustomCodes() > 0) {
    CarbonCfgCustomCode* cc = reg->getCustomCode(0);
    reg->removeCustomCode(cc);
  }

  // Remove all the sub fields
  while (reg->numFields() > 0) {
    CarbonCfgRegisterField* field = reg->getField(0);
    reg->removeField(field);
  }

  // Now we can remove the register. This deletes the memory and
  // removes it from this CarbonCfg.
  removeRegister(reg);
}

void CarbonCfg::removeGroupRecursive(CarbonCfgGroup* group)
{
  // Remove all the registers that match this group
  // first. Unfortunately we don't store the registers in the
  // group. The registers reference the group. This seems wrong but...
  typedef UtArray<CarbonCfgRegister*> Remove;
  Remove toRemove;
  for (UInt32 i = 0; i < numRegisters(); ++i) {
    CarbonCfgRegister* reg = getRegister(i);
    if (reg->getGroup() == group) {
      toRemove.push_back(reg);
    }
  }

  // We can now walk the remove list and remove them. We couldn't do
  // it above cause we can't remove while iterating.
  for (Loop<Remove> l(toRemove); !l.atEnd(); ++l) {
    CarbonCfgRegister* reg = *l;
    removeRegisterRecursive(reg);
  }

  // Now we can remove this group. This deletes the memory and removes
  // it from this CarbonCfg.
  removeGroup(group);
}

void CarbonCfg::removeSubCompsRecursive(void)
{
  // Use a while iterator since we can't delete while we iterate
  while (numSubComponents() > 0) {
    CarbonCfg* subComp = getSubComponent(0);
    removeSubCompRecursive(subComp);
  }
}

void CarbonCfg::removeXtorInstancesRecursive(void)
{
  // Use a while iterator since we can't delete while we iterate
  while (numXtorInstances() > 0) {
    CarbonCfgXtorInstance* xtorInst = getXtorInstance(0);
    removeXtorInstanceRecursive(xtorInst);
  }
}

void CarbonCfg::removeRegistersRecursive(void)
{
  // Use a while iterator since we can't delete while we iterate
  while (numRegisters() > 0) {
    CarbonCfgRegister* reg = getRegister(0);
    removeRegisterRecursive(reg);
  }
}

void CarbonCfg::removeMemoriesRecursive(void)
{
  // Use a while iterator since we can't delete while we iterate
  while (numMemories() > 0) {
    CarbonCfgMemory* mem = getMemory(0);
    removeMemoryRecursive(mem);
  }
}

void CarbonCfg::removeCustomCodes(void)
{
  // Remove the component custom codes.
  mCustomCodes.removeCustomCodes();

  // Remove the cadi custom codes
  mCadi.removeCustomCodes();
}

void CarbonCfg::removeParameters(void)
{
  // Use a while iterator since we can't delete while we iterate
  while (numParams() > 0) {
    CarbonCfgXtorParamInst* param = getParam(0);
    removeParam(param);
  }
}

void CarbonCfg::removeGroupsRecursive(void)
{
  // Use a while iterator since we can't delete while we iterate
  while (numGroups() > 0) {
    CarbonCfgGroup* group = getGroup(0);
    removeGroupRecursive(group);
  }
}

void CarbonCfg::putCompType(const char* name)
{
  mCompType = name;
}

const char* CarbonCfg::getCompType(void) const
{
  return mCompType.c_str();
}

void CarbonCfg::putCompVersion(const char* name)
{
  mCompVersion = name;
}

const char* CarbonCfg::getCompVersion(void) const
{
  return mCompVersion.c_str();
}

void CarbonCfg::putCompDocFile(const char* docFile)
{
  mCompDocFile = docFile;
}

const char* CarbonCfg::getCompDocFile(void) const
{
  return mCompDocFile.c_str();
}

UInt32 CarbonCfg::numCustomCodes() const
{
  return mCustomCodes.numCustomCodes();
}

CarbonCfgCompCustomCode* CarbonCfg::getCustomCode(CarbonUInt32 i) const
{
  CarbonCfgCustomCode* cc = mCustomCodes.getCustomCode(i);
  return cc->castComp();
}

// Builtin functions
static QScriptValue debugFunction(QScriptContext *ctx, QScriptEngine *eng);
static QScriptValue includeFunction(QScriptContext *ctx, QScriptEngine *eng);


// ExpandFormatString function
// Arguments:
//   map, formatString
//
// var x = "foo %(x) can be %(y)";
// var map["x"] = "junk";
//     
QScriptValue ExpandFormatStringFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() > 1)
  {
    QString inputString = ctx->argument(0).toString();

    QScriptValue argMap = ctx->argument(1);
    QScriptValueIterator it(argMap);

    QString result = inputString;

    // Walk all the values
    while (it.hasNext())
    {
      it.next();

      QString key = it.name();
      QString value = it.value().toString();

      QString argumentSpec = "%(" + key + ")";

      result = result.replace(argumentSpec, value);
    }

    return eng->toScriptValue(result);
  }

  return ctx->throwError(QScriptContext::UnknownError, "Invalid or missing arguments to ExpandFormatString");
}

void CfgScriptingEngine::registerProperty(const QString& propertyName, QObject* obj)
{
  QScriptValue global = globalObject();
  global.setProperty(propertyName, newQObject(obj));
}

void CfgScriptingEngine::addBuiltinFunctions()
{
  QScriptValue global = globalObject();

  CarbonCfg::registerTypes(this);

  QScriptValue debugFunc = newFunction(debugFunction);
  if (!global.property(QLatin1String("debug")).isValid())
    global.setProperty(QLatin1String("debug"), debugFunc);

  QScriptValue includeFunc = newFunction(includeFunction);
  if (!global.property(QLatin1String("include")).isValid())
    global.setProperty(QLatin1String("include"), includeFunc);

  QScriptValue expandFunc = newFunction(ExpandFormatStringFunction);
  if (!global.property(QLatin1String("ExpandFormatString")).isValid())
    global.setProperty(QLatin1String("ExpandFormatString"), expandFunc);
}


QScriptValue CfgScriptingEngine::run(const QString& scriptFileName)
{
  QFileInfo fi(scriptFileName);

  if (fi.exists())
  {
    return runScript(scriptFileName);
  }
  else
    qDebug() << "Error: No such script file" << scriptFileName;

  return QScriptValue();
}

QScriptValue CfgScriptingEngine::runScript(const QString& scriptName)
{
  QString program;

  QString scriptFileName = scriptName;
  QFileInfo fi(scriptName);
  QString suffix = fi.suffix().toLower();

  QFile fileScript(scriptFileName);
  fileScript.open(QIODevice::ReadOnly);
  program = fileScript.readAll();


  QScriptValue resultCommon = evaluate(program, scriptFileName);

  if (hasUncaughtException())
  {
    qDebug() << "ScriptError:" << scriptFileName << QString("Exception %1 on line %2").arg(uncaughtException().toString()).arg(uncaughtExceptionLineNumber());

    qDebug() << QString("Script Error: %1").arg(scriptFileName) << QString("Exception %1 on line %2").arg(uncaughtException().toString()).arg(uncaughtExceptionLineNumber());
  }

  return resultCommon;
}

static QScriptValue includeFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QScriptValue value;

  if (ctx->argumentCount() > 0)
  {
    for (int i=0; i<ctx->argumentCount(); i++)
    {
      QString includeFile = ctx->argument(i).toString();
      UtString expandedFileName, err;
      UtString uIncludeFile; uIncludeFile << includeFile;
      if (!OSExpandFilename(&expandedFileName, uIncludeFile.c_str(), &err)) {
        QString msg = err.c_str();
        QString errMsg = QString("Unable to expand include file: %1, err: %2").arg(includeFile, msg);
        ctx->throwError(QScriptContext::URIError, errMsg);
      }

      QString expandedName = expandedFileName.c_str();
      QFileInfo fi(expandedName);
      QFile file( expandedName );
      if ( file.open( QIODevice::ReadOnly | QIODevice::Text))
      {
        QString fileContents = file.readAll();

        ctx->setActivationObject(ctx->parentContext()->activationObject());
        ctx->setThisObject(ctx->parentContext()->thisObject());
        value = eng->evaluate( fileContents, expandedName );
      }
      else
      {
        QString errMsg = QString("Unable to read include file: %1").arg(expandedName);
        ctx->throwError(QScriptContext::URIError, errMsg);
      }
    }
  }

  return value;
}

static QScriptValue debugFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QString outputLine;
  for (int i=0; i<ctx->argumentCount(); i++)
  {
    QScriptValue arg = ctx->argument(i);
    if (!outputLine.isEmpty())
      outputLine += " ";
    outputLine += arg.toString();
  }

  qDebug() << qPrintable(outputLine);

  return eng->undefinedValue();
}

void CfgScriptingEngine::scriptFinished()
{
  // qDebug() << "Script Finished";
  //if (mDebugger)
  //  mEmbeddedDebugger.detach();
}

void CfgScriptingEngine::selected(const QModelIndex&)
{
  //qDebug() << "index" << index;
}

void CfgScriptingEngine::executionHalted()
{
  //qDebug() << "Halted";
}

QScriptValue CfgScriptingEngine::callOptionalScriptFunction(const QString& methodName, const QScriptValueList args)
{
  QScriptValue retValue(this, true);

  QScriptValue func = globalObject().property(methodName);
  if (func.isFunction())
  {
    retValue = func.call(globalObject(), args);

    if (hasUncaughtException())
    {
      qDebug() << "Error" << QString("Exception %1 on line %2").arg(uncaughtException().toString()).arg(uncaughtExceptionLineNumber());
    }
  }

  return retValue;
}

QScriptValue CfgScriptingEngine::callScriptFunction(const QString& methodName, const QScriptValueList args)
{
  QScriptValue retValue(this, false);

  QScriptValue func = globalObject().property(methodName);
  if (func.isFunction())
  {
    retValue = func.call(globalObject(), args);

    if (hasUncaughtException())
    {
      qDebug() << "Error" << QString("Exception %1 on line %2").arg(uncaughtException().toString()).arg(uncaughtExceptionLineNumber());
    }
  }
  else
  {
    qDebug() << "Error: No such Scripting method" << methodName;
  }
  return retValue;
}
void CarbonCfgESLPort::putExpr(const QString& v)
{
  UtString value; value << v;
  putExpr(value.c_str());
}

void CarbonCfgRTLPort::ConnectClockGen(CarbonCfgClockGen* conn) 
{
  this->connect(conn); 
}
void CarbonCfgRTLPort::ConnectResetGen(CarbonCfgResetGen* conn)
{ 
  this->connect(conn); 
}
void CarbonCfgRTLPort::ConnectTie(CarbonCfgTie* conn)
{
  this->connect(conn);
}
void CarbonCfgRTLPort::ConnectTieParam(CarbonCfgTieParam* conn)
{ 
  this->connect(conn);
}
void CarbonCfgRTLPort::ConnectESLPort(CarbonCfgESLPort* conn)
{ 
  this->connect(conn); 
}
void CarbonCfgRTLPort::ConnectXtorConn(CarbonCfgXtorConn* conn)
{ 
  this->connect(conn); 
}

QScriptValue CarbonCfg::Clone(QVariant obj)
{
  CarbonCfgMemory* mem = qvariant_cast<CarbonCfgMemory*>(obj);
  if (mem)
    return engine()->toScriptValue(CloneMemory(mem));

  CarbonCfgCompCustomCode* ccode = qvariant_cast<CarbonCfgCompCustomCode*>(obj);
  if (ccode)
    return engine()->toScriptValue(CloneCustomCode(ccode));

  CarbonCfgCadi* cadi = qvariant_cast<CarbonCfgCadi*>(obj);
  if (cadi)
    CloneCadi(cadi);

  CarbonCfgProcInfo* proci = qvariant_cast<CarbonCfgProcInfo*>(obj);
  if (proci)
    CloneProcInfo(proci);

  return QScriptValue();
}
