//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CfgXmlParserReg.h"

#include "cfg/CarbonCfg.h"

#include "util/UtIStream.h"

CfgXmlParserReg::CfgXmlParserReg()
{
}

CfgXmlParserReg::~CfgXmlParserReg()
{
}


bool CfgXmlParserReg::parseStream(UtIStream &s, CarbonCfg *cfg)
{
  parseSetup();

  // Fill a buffer with the XML text, then create a
  // document from it.
  xmlBuffer *buf = xmlBufferCreate();
  bool done = false;
  UtString line;
  while (!done) {
    s.getline(&line);
    xmlBufferWriteChar(buf, line.c_str());
    // check for end of XML or EOF
    if (line.empty() || (line.find("</reginfo>") != UtString::npos))
      done = true;
  }

  xmlDoc *doc = xmlParseDoc(xmlBufferContent(buf));

  if (doc == 0)
    reportError("Failed to parse register XML");
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "reginfo"))
      reportError("Root element of register XML must be 'reginfo'");
    else
      parseRegInfo(rootNode, cfg);
  }

  xmlFreeDoc(doc);
  xmlBufferFree(buf);
  parseCleanup();

  return !getError();
}

void CfgXmlParserReg::parseRegInfo(xmlNode *node, CarbonCfg *cfg)
{
  CfgXmlAttr regBaseAddrAttr(node,"baseaddr");
  if (regBaseAddrAttr.value() != NULL) {
    cfg->putRegisterBaseAddress(regBaseAddrAttr.value());
  }

  // walk all children and parse them
  for (xmlNode *child = node->children; child != NULL; child = child->next)
    if (isElement(child, "reggroup"))
      parseGroup(child, cfg);
}

void CfgXmlParserReg::parseGroup(xmlNode *node, CarbonCfg *cfg)
{
  // get group name
  CfgXmlAttr nameAttr(node, "name");
  if (nameAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<reggroup> element with no name attribute is not allowed.");
  }

  CarbonCfgGroup *group = cfg->addGroup(nameAttr.value());

  // walk all children and parse them
  for (xmlNode *child = node->children; child != NULL; child = child->next)
    if (isElement(child, "register"))
      parseReg(child, group, cfg);
}

void CfgXmlParserReg::parseReg(xmlNode *node, CarbonCfgGroup *group, CarbonCfg *cfg)
{
  CarbonUInt32 width = 0;
  bool big_endian = false;
  bool pc_reg = false;
  CarbonCfgRadixIO radix;
  UtString comment;
  CarbonUInt32 offset = 0;
  UtString port;

  CfgXmlAttr nameAttr(node, "name");
  if (nameAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<register> element with no name attribute is not allowed.");
  }

  // walk all children looking for width, radix, etc.
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "width")) {
      getContent(child, &width);
    } else if (isElement(child, "bigendian")) {
      UtString temp;
      getContent(child, &temp);
      big_endian = (temp == "true");
    } else if (isElement(child, "pcreg")) {
      UtString temp;
      getContent(child, &temp);
      pc_reg = (temp == "true");
    } else if (isElement(child, "radix")) {
      UtString temp;
      getContent(child, &temp);
      radix.parseString(temp.c_str());
    } else if (isElement(child,"offset")) {
      getContent(child, &offset);
    } else if (isElement(child, "esl-port")) {
      getContent(child, &port);
    } else if (isElement(child, "comment")) {
      getContent(child, &comment);
    }
  }

  // Error checking
  bool localerror = false;
  if (width == 0) {
    reportError("<register> element must have a non-zero width.");
    localerror = true;
  }
  // TODO - do we need to validate the radix?
  if (localerror)
    return;

  CarbonCfgRegister *reg = cfg->addRegister(nameAttr.value(), group, width, big_endian, radix, comment.c_str(), pc_reg);

  // Set the Offset
  reg->putOffset(offset);

  // Set the port
  reg->putPort(port.c_str());

  // walk again, parsing the fields and custom code
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "field")) {
      parseField(child, reg);
    } else if (isElement(child, "customcode")) {
      parseCustomCode(child, reg);
    }
  }

  // validate fields
  if (!reg->checkFields()) {
    UtString errtext;
    errtext << "Register " << nameAttr.value() << " has illegal/overlapping fields.";
    reportError(errtext.c_str());
  }

}

void CfgXmlParserReg::parseField(xmlNode *node, CarbonCfgRegister *reg)
{
  // get range
  CfgXmlAttr highAttr(node, "high");
  CfgXmlAttr lowAttr(node, "low");
  if ((highAttr.value() == 0) || (lowAttr.value() == 0)) {
    // TODO: use DTD/XSD
    reportError("<field> element must have high and low attributes.");
    return;
  }

  CarbonUInt32 high = 0;
  CarbonUInt32 low = 0;
  UtString temp;
  temp = highAttr.value();
  temp >> high;
  temp = lowAttr.value();
  temp >> low;

  CarbonCfgRegAccessTypeIO access;
  UtString comment;
  CarbonCfgRegisterLoc *loc = 0;
  bool has_range = false;
  CarbonUInt32 left = 0;
  CarbonUInt32 right = 0;
  UtString fieldName;

  // walk all children looking for range, access, comment, and location
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "name")) {
      getContent(child, &fieldName);
    } else if (isElement(child, "access")) {
      UtString temp;
      getContent(child, &temp);
      access.parseString(temp.c_str());
    } else if (isElement(child, "comment"))
      getContent(child, &comment);
    else if (isElement(child, "range")) {
      CfgXmlAttr leftAttr(child, "left");
      CfgXmlAttr rightAttr(child, "right");
      if ((leftAttr.value() == 0) || (rightAttr.value() == 0)) {
        // TODO: use DTD/XSD
        reportError("<range> element must have left and right attributes.");
      }

      UtString temp;
      temp = leftAttr.value();
      temp >> left;
      temp = rightAttr.value();
      temp >> right;
      has_range = true;
    } else if (isElement(child, "location")) {
      CfgXmlAttr typeAttr(child, "type");
      UtString type = typeAttr.value();
      if (type == "constant") {
        CarbonUInt64 val;
        getContent(child, &val);
        loc = new CarbonCfgRegisterLocConstant(val);
      } else if (type == "reg") {
        UtString temp;
        getContent(child, &temp);
        loc = new CarbonCfgRegisterLocReg(temp.c_str());
      } else if (type == "array") {
        CfgXmlAttr indexAttr(child, "index");
        if (indexAttr.value() == 0)
          reportError("Array location needs an index.");
        else {
          CarbonUInt32 index = 0;
          UtString temp = indexAttr.value();
          temp >> index;
          temp.clear();
          getContent(child, &temp);
          loc = new CarbonCfgRegisterLocArray(temp.c_str(), index);
        }
      } else if (type == "user") {
        loc = new CarbonCfgRegisterLocUser();

      } else {
        // TODO: use DTD/XSD
        UtString errtext;
        errtext << "<register><field> element has an invalid or missing 'type' attribute: <location type=\"" << type << "\">.";
        reportError(errtext.c_str());
      }
    }
  }

  // Add RTL ranges, if necessary
  if (loc && has_range) {
    // Only registers/arrays can have ranges
    CarbonCfgRegisterLocRTL *rtlreg = loc->castRTL();
    if (rtlreg) {
      rtlreg->putLeft(left);
      rtlreg->putRight(right);
      rtlreg->putHasRange(true);
    } else {
      reportError("Constant and user locations cannot have ranges.");
    }
  }

  // Create the field, add the location, and add the whole thing
  // to the register
  CarbonCfgRegisterField *field = new CarbonCfgRegisterField(high, low, access);
  field->putLoc(loc);
  field->putName(fieldName.c_str());
  reg->addField(field);
}

void CfgXmlParserReg::parseCustomCode(xmlNode *node, CarbonCfgRegister* reg)
{
  // Parse the custom code
  CfgXmlParserCustomCode parseCustom(node, reg->getLegalSections());
  bool success = parseCustom.parse(reg);
  if (!success) {
    reportError(parseCustom.errorText());
    return;
  }
}
