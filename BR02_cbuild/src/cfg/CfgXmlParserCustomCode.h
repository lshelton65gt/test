// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Contains a class to parse a custom code element and return the code
*/

#ifndef _CFGXMLPARSERCUSTOMCODE_H_
#define _CFGXMLPARSERCUSTOMCODE_H_

#include "CfgXmlParser.h"
#include "cfg/CarbonCfg.h"

#include "util/UtHashSet.h"

class CfgXmlParserCustomCode : public CfgXmlParser
{
public:
  CARBONMEM_OVERRIDES

  //! Abstraction for a set of sections and their enums
  /*! Can't do derived class enums, so we translate from ints
   */
  typedef UtHashMap<UtString, int> SectionNames;

  //! Constructor
  CfgXmlParserCustomCode(xmlNode* node, const SectionNames& sections);

  //! Constructor
  CfgXmlParserCustomCode(xmlNode* node, const char** legalNames);

  //! Destructor
  ~CfgXmlParserCustomCode();

  //! Get the custom code data (returns false if there was a problem)
  /*! The caller is responsible for passing in a set of legal sections.
   */
  bool getCode(int* section, UtString* sectStr,
               CarbonCfgCustomCodePosition* position, UtString* code);

  template <class C>
  bool parse(C* container)
  {
    // Parse the custom code
    int                         sectInt;
    CarbonCfgCustomCodePosition position;
    UtString                    sectStr;
    UtString                    code;

    bool success = getCode(&sectInt, &sectStr, &position, &code);
    
    if(success) {
      // Add it to the configuration
      CarbonCfgCustomCode* customCode = container->addCustomCode();
      customCode->putSectionString(sectStr.c_str());
      customCode->putPosition(position);
      customCode->putCode(code.c_str());
    }

    return success;
  }
    
private:
  //! Node to process
  xmlNode* mNode;

  //! Legal Section Names
  SectionNames mLegalSections;
};

#endif // _CFGXMLPARSERCUSTOMCODE_H_
