//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// \file Generic XML writer

#ifndef _CfgXmlWriter_h_
#define _CfgXmlWriter_h_

#include "libxml/tree.h"
#include "util/UtStackPOD.h"

class UtOStream;

//! Generic XML writer class
class CfgXmlWriter
{
public:
  CARBONMEM_OVERRIDES;

  CfgXmlWriter(const char *root);
  ~CfgXmlWriter();

  //! Write DOM represenation to a new file
  void write(const char *filename);
  //! Write DOM represenation to an existing UtOStream
  void write(UtOStream &s);
  //! Add element to the DOM, with or without text content
  void addElement(const char *name, const char *text = 0);
  //! Add element to the DOM, with unsigned content
  void addElement(const char *name, CarbonUInt64 value);
   //! Add element to the DOM, with unsigned content
  void addCompletedElement(const char *name, CarbonUInt64 value);
  //! Add a completed element, with or without text content
  void addCompletedElement(const char *name, const char *text = 0);
  void addCompletedElement(const char *name, const UtString& value);
  //! Add a cdata element to the DOM
  void addCdata(const char* text);
  //! Add a string attribute to the most recently added element
  void addAttribute(const char *name, const UtString& value);
  //! Add a string attribute to the most recently added element
  void addAttribute(const char *name, const char *value);
  //! Add an unsigned attribute to the most recently added element
  void addAttribute(const char *name, CarbonUInt64 value);
  //! Closes the current element, returning to its parent
  void closeElement();

private:
  UtStackPOD<xmlNodePtr> mNodeStack;
  xmlDocPtr mDoc;
};


#endif
