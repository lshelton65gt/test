//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// \file XML Parser for xactors.xml 

#ifndef _CfgXmlParserXtor_h_
#define _CfgXmlParserXtor_h_

#include "CfgXmlParser.h"

//! CfgXmlParserXtor class
class CfgXmlParserXtor : public CfgXmlParser
{
 public:
  CARBONMEM_OVERRIDES

  //! constructor
  CfgXmlParserXtor();

  //! destructor
  ~CfgXmlParserXtor();

  //! parse the given input file
  /*!
   *  Parse the given XML file, adding all transactor components
   *  defined therin to the given CarbonCfgXtorLib.
   *
   *  \param fileName The path to the XML file to be parsed.
   *
   *  \param xtorLib Cfg Transactor library to which components will
   *  be added.
   *
   *  \returns true if the file is successfully parsed, false if
   *  errors were found.
   */
  bool parseFile(const char *fileName, CarbonCfgXtorLib *xtorLib);
  
  //! parse the xmlparameters section of a .ccfg file. Adds the parameters to the data model.
  bool parseStream(UtIStream &s, CarbonCfg *cfg);
    //! parse XML -component format from another XML Document
  bool parseParameters(const char* parametersXML, CarbonCfg *cfg);

 private:
  // parsing
  void parseComponent(xmlNode *node, CarbonCfgXtorLib *xtorLib);
  void parsePort(xmlNode *node, CarbonCfgXtor *xtor);
  void parseSystemCPort(xmlNode *node, CarbonCfgXtor *xtor);
  void parseSynonym(xmlNode *node, CarbonCfgXtorLib *xtorLib, CarbonCfgXtor *xtor);
  CarbonCfgXtorParam* parseParameter(xmlNode *node);
  void parseDescription(xmlNode *node, UtString *desc);
  void parseEnumChoices(xmlNode *node, UtString *enumChoices);
  void parseClassName(xmlNode *node, CarbonCfgXtor *xtor);
  void parseIncludeFile(xmlNode *node, CarbonCfgXtor *xtor);
  void parseProtocolName(xmlNode *node, CarbonCfgXtor *xtor);
  void parseLibFile(xmlNode *node, CarbonCfgXtor *xtor);
  void parseIncPath(xmlNode *node, CarbonCfgXtor *xtor);
  void parseLibPath(xmlNode *node, CarbonCfgXtor *xtor);
  void parseConstructorArgs(xmlNode *node, CarbonCfgXtor *xtor);
  void parseTimingTable(xmlNode* node);
  void parsePhasePorts(xmlNode* node, const UtString& phaseName);
  void parseTemplateArg(xmlNode* node, CarbonCfgXtor* xtor);
  void parseAbstraction(xmlNode* node, CarbonCfgXtor* xtor);
  void parseFlag(xmlNode* node, CarbonCfgXtor* xtor);
  void parseAbstractionClassName(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction);
  void parseAbstractionIncludeFile(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction);
  void parseAbstractionLibFile(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction);
  void parseAbstractionIncPath(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction);
  void parseAbstractionLibPath(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction);
  void parseAbstractionFlag(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction);
  void parseAbstractionLayerId(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction);

  //! A map from pin to the phases it is related to. This is for GreenSoCs timing listeners.
  typedef UtHashMap<UtString, UtStringArray> PinPhases;

  //! Storage for the timing map
  PinPhases mPinPhases;
};

//! CfgXmlParserXtor class
class CfgCowareXmlParserXtor : public CfgXmlParser
{
 public:
  CARBONMEM_OVERRIDES

  //! constructor
  CfgCowareXmlParserXtor();

  //! destructor
  ~CfgCowareXmlParserXtor();

  //! parse the given input file
  /*!
   *  Parse the given XML file, adding all transactor components
   *  defined therin to the given CarbonCfgXtorLib.
   *
   *  \param fileName The path to the XML file to be parsed.
   *
   *  \param xtorLib Cfg Transactor library to which components will
   *  be added.
   *
   *  \returns true if the file is successfully parsed, false if
   *  errors were found.
   */
  bool parseFile(const char *fileName, CarbonCfg* cfg);

 private:
  // Enum to set an xtor as master/slave or unknown
  enum XtorType {
    eMaster,
    eSlave,
    eUnknown
  };

  // parsing
  void parseData(xmlNode* node, CarbonCfg* cfg);
  void parseProtocols(xmlNode* node, const char* library, CarbonCfg* cfg, UInt32 major, UInt32 minor);
  void parseProtocol(xmlNode* node, const char* library, CarbonCfg* cfg, UInt32 major, UInt32 minor);
  CarbonCfgXtor* addXtor(xmlNode* node, const char* name, const char* library, const char* variant,
                         CarbonCfgXtorLib* xtorLib, XtorType xtorType);
  void parseBca(xmlNode* parent, CarbonCfgXtor* xtor, const char* library);
  void parsePins(xmlNode* parent, CarbonCfgXtor* xtor);
  void parsePin(xmlNode* parent, CarbonCfgXtor* xtor);
  void parseDataType(xmlNode* parent, CarbonCfgXtorPort* port);
  void parseDirect(xmlNode* parent, CarbonCfgXtorPort* port);
  void parseLibrary(xmlNode* parent, CarbonCfg* cfg, UInt32 major, UInt32 minor);
};

#endif  // _CfgXmlParserXtor_h_
