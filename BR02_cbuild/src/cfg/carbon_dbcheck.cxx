//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg_misc.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "util/UtParamFile.h"
#include "util/UtConv.h"
#include "util/RandomValGen.h"
#include "shell/carbon_misc.h"
#include "shell/carbon_capi.h"


// Predicate to check port types
bool CarbonCfg::portTypesEqual(CarbonCfgRTLPortType cfgPortType, const CarbonDBNode* node)
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  bool retValue = false;
  switch (cfgPortType) {
  case eCarbonCfgRTLInput:
    retValue = carbonDBIsPrimaryInput(db, node) || carbonDBIsScDepositable(db, node);
    break;
  case eCarbonCfgRTLOutput:
    retValue = carbonDBIsPrimaryOutput(db, node) || carbonDBIsScObservable(db, node);
    break;
  case eCarbonCfgRTLInout:
    retValue = carbonDBIsPrimaryBidi(db, node);
    break;
  }
  return retValue;
}

CarbonCfgRTLPortType CarbonCfg::computePortType(const CarbonDBNode* node)
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  if (carbonDBIsPrimaryInput(db, node) || carbonDBIsScDepositable(db, node))
    return eCarbonCfgRTLInput;

  if (carbonDBIsPrimaryOutput(db, node) || carbonDBIsScObservable(db, node))
    return eCarbonCfgRTLOutput;

  return eCarbonCfgRTLInout;
}


const CarbonDBNode* CarbonCfg::findCarbonPort(CarbonCfgRTLPort* port)
{
  const char* cfgPortName = port->getName();
  return carbonDBFindNode(getDB(), cfgPortName);;
}


void CarbonCfg::checkCcfgRegisters()
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  for (UInt32 i=0,num=numRegisters(); i<num; ++i) {
    CarbonCfgRegister* reg = getRegister(i);
    for (CarbonCfgRegister::RTLLoop l(reg); !l.atEnd(); ++l) {
      const CarbonDBNode* node = carbonDBFindNode(db, (*l)->getPath());

      // No matching node, report the error
      if (node == NULL) {
        UtString errStr;
        errStr << "Unable to locate Carbon Signal referenced as " << reg->getGroupName() << "\\" << reg->getName();
        sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOCARBONSIGNAL, errStr.c_str(), 0);
      } else {
        // update the data model with the true LSB/MSB
        // For scalars, just check the width and set LSB to 0 and MSB to width-1
        if(carbonDBIsScalar(db, node)) {
          (*l)->putLSB(0);
          (*l)->putMSB(carbonDBGetWidth(db, node)-1);
        }
        // Otherwise use the MSB and LSB from the DB
        else {
          (*l)->putLSB(carbonDBGetLSB(db, node));
          (*l)->putMSB(carbonDBGetMSB(db, node));
        }
        // Check that "RTL Array" register are really a memory
        if( (*l)->getType() == eCfgRegLocArray && !(carbonDBIs2DArray(db, node) && !carbonDBIsContainedByComposite(db, node)) ) {
          UtString errStr;
          errStr << "Memory register referenced as " << reg->getGroupName() << "\\" << reg->getName() << " is not a Carbon Memory";
          sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOTMEMORY, errStr.c_str(), 0);
        }
        
        // Check that "RTL Register" register can be a CarbonNetID
        else if((*l)->getType() == eCfgRegLocRegister && !carbonDBCanBeCarbonNet(db, node) ) {
          UtString errStr;
          errStr << "Register referenced as " << reg->getGroupName() << "\\" << reg->getName() << " is not a Carbon Net";
          sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOTCARBONNET, errStr.c_str(), 0);
        }
      }
      // TODO - additional checking, e.g. is the node a register/memory as expected,
      // bounds checking, etc.
    }
  }
}

void CarbonCfg::checkCcfgMemories()
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  bool foundDisassembler = false;
  for (UInt32 i=0,num=numMemories(); i<num; ++i) {
    CarbonCfgMemory* mem = getMemory(i);
    mem->check(this, db);

    // Check for duplicate disassemblers
    if (strcmp("", mem->getMemoryDisassemblerName()) != 0) {
      if(foundDisassembler) {
        UtString errStr;
        errStr << "Memory " << mem->getName() << ": Only one memory can have a disassembler for a given component/subcomponent.";
        sMsgCallback(this, eCarbonMsgError, carbonCfgERR_MULTDISASS, errStr.c_str(), 0);
      }
      else foundDisassembler = true;
    }
  }
}

void CarbonCfg::removeAllConnections(CarbonCfgRTLPort* port)
{
  for (UInt32 rc=0; rc<port->numConnections(); rc++)
  {
    CarbonCfgRTLConnection* conn = port->getConnection(rc);
    if (conn->castTieParam())
    {
      CarbonCfgTieParam* tiep = conn->castTieParam();

      CarbonCfgXtorParamInst* xtorParamInst = NULL;
      CarbonCfgXtorInstance* xtorInst = findXtorInstance(tiep->mXtorInstanceName.c_str());
      if (xtorInst)
        xtorParamInst = xtorInst->findParameterInst(tiep->mParam);
      else
      {
        for (UInt32 i=0; i<numParams(); i++)
        {
          CarbonCfgXtorParamInst* paramInst = getParam(i);
          if (tiep->mParam == paramInst->getParam()->getName())
          {
            xtorParamInst = paramInst;
            break;
          }
        }
      }
      if (xtorParamInst)
        xtorParamInst->removeRTLPort(port);      
    }
    disconnect(conn);
  }
}

void CarbonCfg::checkCcfgPorts()
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  for (SInt32 i=mRTLPortVec.size()-1; i>=0; --i) {
    CarbonCfgRTLPort* port = mRTLPortVec[i];
    const CarbonDBNode* node = findCarbonPort(port);

    CarbonCfgRTLPortType portType = port->getType();
    const char* portTypeStr = "Undefined";
    switch (portType) {
    case eCarbonCfgRTLInput:
      portTypeStr = "Input";
      break;
    case eCarbonCfgRTLOutput:
      portTypeStr = "Output";
      break;
    case eCarbonCfgRTLInout:
      portTypeStr = "Bidirectional";
      break;
    }

    // If we didn't find it, report it as missing
    if (node == NULL) {
      UtString errStr;
      errStr << "Unable to locate associated Carbon Port for " << portTypeStr << " port: " << port->getName() << " of width " << port->getWidth();
      removeAllConnections(port);
      sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOCARBONPORT, errStr.c_str(), 0);
      mRTLPortVec.remove(port);
      mRTLPortMap.erase(port->getName());
      delete port;
    } else {
      // Check if it is a valid port
      if(!carbonDBCanBeCarbonNet(db, node)) {
        UtString errStr;
        errStr << "Associated Carbon Port for " << portTypeStr << " port: " << port->getName() << " is not a carbon net";
        sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOTCARBONNET, errStr.c_str(), 0);
        removeAllConnections(port);
        mRTLPortVec.remove(port);
        mRTLPortMap.erase(port->getName());
        delete port;
      }
      else if(carbonDBIsContainedByComposite(db, node)) {
        // For SystemC projects we allow lower level carbon nets that
        // are part of a composite as long as they are not primary
        // ports
        if ((getCcfgMode() != eCarbonCfgSYSTEMC) ||
            carbonDBIsPrimaryInput(db, node) || 
            carbonDBIsPrimaryOutput(db, node)  ||
            carbonDBIsPrimaryBidi(db, node)) {
          UtString errStr;
          errStr << "Associated Carbon Port for " << portTypeStr << " port: " << port->getName() << " is part of a composite net. Composite ports are not currently supported";
          sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_COMPOSITEPORT, errStr.c_str(), 0);
          removeAllConnections(port);
          mRTLPortVec.remove(port);
          mRTLPortMap.erase(port->getName());
          delete port;
        }
      }
    }
  }
}

void CarbonCfg::checkCcfgXtorConnections()
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  for (UInt32 i=0, num = numXtorInstances(); i < num; ++i) {
    CarbonCfgXtorInstance* xtorInst = getXtorInstance(i);
    for (UInt32 pi=0, nports = xtorInst->getType()->numPorts(); pi<nports; ++pi) {
      CarbonCfgXtorConn* conn = xtorInst->getConnection(pi);
      CarbonCfgRTLPort* rtlPort = conn->getRTLPort();
      if (rtlPort != NULL) {
        const CarbonDBNode* node = findCarbonPort(rtlPort);
        if (node == NULL) {
          UtString errStr;
          errStr << "Unable to locate associated Carbon Port for Transactor " << xtorInst->getName() <<  " port: " << rtlPort->getName();
          sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOTRANSPORT, errStr.c_str(), 0);
        }
        else {
          // Check if it is a valid port
          if(!carbonDBCanBeCarbonNet(db, node)) {
            UtString errStr;
            errStr << "Associated Carbon Port for Transactor" << xtorInst->getName() << " port: " << rtlPort->getName() << " is not a carbon net";
            sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOTCARBONNET, errStr.c_str(), 0);
          }
          else if(carbonDBIsContainedByComposite(db, node)) {
            UtString errStr;
            errStr << "Associated Carbon Port for Transactor" << xtorInst->getName() << " port: " << rtlPort->getName() << " is part of a composite net. Composite ports are not currently supported";
            sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_COMPOSITEPORT, errStr.c_str(), 0);
          }
        }
      }
      // Only warn for connections that are not marked as optional
      else if ( !xtorInst->getType()->getPort(pi)->getOptional() ) {
        UtString errStr;
        errStr << "No RTL signal connected for " << xtorInst->getType()->getName() 
               << " instance " << xtorInst->getName() 
               << ", port " << xtorInst->getType()->getPort(pi)->getName();
        sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOXTORCONN, errStr.c_str(), 0);
      }
    }
  }
}

void CarbonCfg::checkCcfgPStreams()
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  // Loop through all nets in all Profile Streams and check that they are valid Carbon Nets
  for (UInt32 i=0,num=numPStreams(); i<num; ++i) {
    CarbonCfgPStream* ps = getPStream(i);
    for(UInt32 nid=0,nnum=ps->numNets(); nid<nnum; ++nid) {
      CarbonCfgPNet* net = ps->getNet(nid);
      const CarbonDBNode* node = carbonDBFindNode(db, net->getPath());

      // No matching node, report the error
      if (node == NULL) {
        UtString errStr;
        errStr << "Unable to locate Carbon Signal referenced by Profile Stream: " << ps->getName() << " Net: " << net->getName();
        sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOCARBONSIGNAL, errStr.c_str(), 0);
      } else {
        // Check that the net can be a Carbon Net
        if( !carbonDBCanBeCarbonNet(db, node) ) {
          UtString errStr;
          errStr << "Carbon Signal referenced by Profile Stream: " << ps->getName() << " Net: " << net->getName() << " is not a Carbon Net";
          sMsgCallback(this, eCarbonMsgWarning, carbonCfgERR_NOTCARBONNET, errStr.c_str(), 0);
        }
      }
    }
  }
}

// Temporary Message Callback function used to intercept "Error 5028: Error opening file lidesign.io.db"
eCarbonMsgCBStatus CarbonCfg::sOpenDBMessageCB(CarbonClientData clientData, 
                                              CarbonMsgSeverity, 
                                              int number,
                                              const char* text,
                                              unsigned int len)
{
  // Intercept Error 5028 and turn it into a warning
  if (number == 5028 && getenv("CARBON_DB_IS_OPTIONAL") != NULL) {
    sMsgCallback(clientData, eCarbonMsgWarning, number, text, len);
    return eCarbonMsgStop;
  }

  // For other errors, let it be handled by the other callbacks
  return eCarbonMsgContinue;
}

// Returns true upon an error
bool CarbonCfg::loadAndCheckDatabase()
{
  // Register temporary message callback to demote "Error 5028: Error opening file lidesign.io.db"
  // to a warning if CARBON_DB_IS_OPTIONAL is set.
  CarbonMsgCBDataID* msgID = carbonAddMsgCB(NULL, sOpenDBMessageCB, this);

  CarbonDB* db = NULL;
  switch(mMode)
  {
  case eRvsocd:
    {
      SInt32 key;
      RANDOM_SEEDGEN_1(key, "VSP", 3);
      db = carbonDBSocVspInit(key, getIODBFileName(), false);
    }
    break;
  case ePlatarch:
    {
      SInt32 key;
      RANDOM_SEEDGEN_1(key, "PLAT", 4);
      db = carbonDBPlatArchInit(key, getIODBFileName(), false);
    }
    break;
  case eSystemC:
    {
      db = carbonDBOpenFile(getIODBFileName());
    }
    break;
  case eUnknown:
    {
      db = carbonDBOpenFile(getIODBFileName());
    }
    break;
  default:
    INFO_ASSERT(0, "invalid mode");
    break;
  }
  
  if (db != NULL) {
    putDB(db);
    // First, validate everything in the ccfg exists int the Carbon Database
    checkCcfgIntegrity();

    // Now, check the Carbon ports
    checkCarbonRTLPorts();
  }
  else {
    // due primarily to the massive numbers of regression tests that lack ccfg
    // files, we have to have the option to allow reading CCFG files when there is no
    // DB present. Setting CARBON_DB_IS_OPTIONAL will continue to read the ccfg when the DB
    // is not found.
    if (getenv("CARBON_DB_IS_OPTIONAL") == NULL) {
      UtString errStr;
      errStr << "Database not loaded";
      sMsgCallback(this, eCarbonMsgError, carbonCfgERR_NODB, errStr.c_str(), 0);
    }
  }

  // Unregister message callback
  carbonRemoveMsgCB(NULL, &msgID);

  return mMsgHandler.getFailure();
} // bool CarbonCfg::loadAndCheckDatabase

void CarbonCfg::verifyCarbonPortLoop(CarbonDB* carbonDB, CarbonDBNodeIter* iter)
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  const CarbonDBNode* node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    // We will ignore ports that are contained in composites
    if(carbonDBIsContainedByComposite(carbonDB, node)) break;

    // Ignore ports that can't be a carbon net
    if (!carbonDBCanBeCarbonNet(carbonDB, node)) {
      break;
    }

    const char* name = carbonDBNodeGetFullName(carbonDB, node);
    CarbonCfgRTLPort* cfgRtlPort = findRTLPort(name);
    int cWidth = carbonDBGetWidth(carbonDB, node);
    CarbonCfgRTLPortType rtlPortType = computePortType(node);

    if (cfgRtlPort == NULL) {
      UtString errStr;
      errStr << "New Carbon Port found : " << name;
      sMsgCallback(this, eCarbonMsgNote, carbonCfgERR_NEWCARBONPORT, errStr.c_str(), 0);
      
      CarbonCfgRTLPort* rtlPort = addRTLPort(name, cWidth, rtlPortType);
      CarbonCfgESLPortType eslPortType = CarbonCfg::convertRTLPortType(rtlPort);

      // Use the RTL leaf name for the ESL port name, then make sure it's unique
      UtString eslName;
      getUniqueESLName(&eslName, carbonDBNodeGetLeafName(db, node));
      addESLPort(rtlPort, eslName.c_str(), eslPortType, NULL, eCarbonCfgESLPortUndefined);
    } else {
      int pWidth = cfgRtlPort->getWidth();
      if ( (cWidth != pWidth) || !portTypesEqual(cfgRtlPort->getType(), node)) {
        UtString errStr;
        if (cWidth != pWidth)
          errStr << "Port width characteristics have changed for port: " << name << " old width: " << pWidth << " new width: " << cWidth;
        else {
          const char* cfgPortType = gCarbonRTLPortTypes[cfgRtlPort->getType()];
          const char* crbPortType = gCarbonRTLPortTypes[rtlPortType];
          errStr << "Port direction characteristics have changed for port: " << name << " from: " << cfgPortType << " to: " << crbPortType;
        }

        sMsgCallback(this, eCarbonMsgError, carbonCfgERR_PORTCHANGED, errStr.c_str(), 0);
        cfgRtlPort->putWidth(cWidth);
        cfgRtlPort->putType(rtlPortType);
      }
    }
  }
  carbonDBFreeNodeIter(iter);
}

void CarbonCfg::checkCarbonRTLPorts()
{
  // Use getDB() since it works for both top components and sub
  // components even though the DB is only stored in the top
  // component.
  CarbonDB* db = getDB();

  // First verify the ports are in the Ccfg 
  verifyCarbonPortLoop(db, carbonDBLoopPrimaryInputs(db));

  verifyCarbonPortLoop(db, carbonDBLoopPrimaryBidis(db));

  verifyCarbonPortLoop(db, carbonDBLoopPrimaryOutputs(db));

  verifyCarbonPortLoop(db, carbonDBLoopScDepositable(db));

  verifyCarbonPortLoop(db, carbonDBLoopScObservable(db));
}

// Validate CCFG integrity against the Carbon Database
void CarbonCfg::checkCcfgIntegrity()
{
  // First, check the Ports from the Config File
  checkCcfgPorts();

  // Now verify the Registers
  checkCcfgRegisters();

  // Now verify the Memories
  checkCcfgMemories();
  
  // Now verify Profile registers
  checkCcfgPStreams();

  // Now check the Xtor connections
  checkCcfgXtorConnections();

  // Check the sub components
  checkCcfgSubComponents();
}

void CarbonCfg::checkCcfgSubComponents()
{
  for (UInt32 i = 0; i < numSubComponents(); ++i) {
    CarbonCfg* subComp = getSubComponent(i);
    subComp->checkCcfgIntegrity();
  }
}

CarbonCfgStatus CarbonCfg::read(const char* filename) {
  CarbonCfgStatus status = readNoCheck(filename);

  // Check database integrity
  if ((status == eCarbonCfgSuccess) && loadAndCheckDatabase()) {
    return eCarbonCfgInconsistent;
  }

  return status;
} // CarbonCfgStatus CarbonCfg::read

/*! create an empty cfg */
CarbonCfgID carbonCfgCreate() {
  return new CarbonCfgTop;
}

CarbonCfgID carbonCfgModeCreate(int mode)
{
  CarbonCfg::Mode cfgMode = static_cast<CarbonCfg::Mode>(mode);
  switch(cfgMode)
  {
  case CarbonCfg::eRvsocd:
  case CarbonCfg::ePlatarch:
  case CarbonCfg::eSystemC:
    break;
  default:
    {
      UtString str;
      str << "Unknown mode" << mode;
      INFO_ASSERT(0, str.c_str());
    }
    break;
  }

  return new CarbonCfgTop(cfgMode);
}

/*! load a config from a file, return 0 on success, 1 on failure */
/*! If an error occurs, get it with carbonCfgGetErrmsg(); */
CarbonCfgStatus carbonCfgRead(CarbonCfgID cfg,
                                         const char* filename)
{
  return cfg->read(filename);
}
