//! -*-C++-*-

#ifndef _CfgXmlParserComp_h_
#define _CfgXmlParserComp_h_

#include "CfgXmlParser.h"
#include "CfgXmlParserCustomCode.h"

class CarbonCfg;
class UtIStream;

//! CfgXmlParserComp class
class CfgXmlParserComp : public CfgXmlParser
{
public:
  CARBONMEM_OVERRIDES

  //! Constructor
  CfgXmlParserComp();

  //! parse XML -component format from a stream
  bool parseStream(UtIStream &s, CarbonCfg *cfg);
  //! parse XML -component format from another XML Document
  bool parseComponent(const char* componentXML, CarbonCfg *cfg);

private:
  void parseSubComp(xmlNode *node, CarbonCfg *cfg, const xmlChar* contents);
  void parseComp(xmlNode *node, CarbonCfg *cfg, bool top, const xmlChar* contents);
  void parseCustomCode(xmlNode *node, CarbonCfg *cfg);
  void parseCadi(xmlNode* node, CarbonCfg* cfg);
  void parseCadiCustomCode(xmlNode* node, CarbonCfg* cfg);
};

#endif
