%module CarbonCfgCSharp

%{
#include "cfg/CarbonCfg.h"
%}

#define Q_OBJECT
#define Q_CLASSINFO(x,y) 
#define Q_FLAGS(x)
#define Q_DECLARE_FLAGS(x,y)
#define Q_ENUMS(x)
#define Q_PROPERTY(x)
#define CARBONMEM_OVERRIDES
#define slots
#define signals
#define quint32 unsigned int
#define CarbonUInt32 unsigned int
#define CarbonUInt64 unsigned long long
#define UInt32 unsigned int
#define UInt64 unsigned long long
#define qint32 int
#define qint64 long long
#define quint64 unsigned long long

// Comments goe here need dependencie here   
%typemap(csclassmodifiers) SWIGTYPE "public partial class"

//%typemap(cstype) quint32 "uintxxx"

%typemap(imtype) quint32 "uint"
%typemap(imtype) quint64 "ulong"


// Memory is owned by CarbonCfg, do not delete the proxy

%typemap(csdestruct, methodname="Dispose", methodmodifiers="public") SWIGTYPE {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

%typemap(csdestruct_derived, methodname="Dispose", methodmodifiers="public") SWIGTYPE {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

 
%include "cfg/carbon_cfg_enum.h"
%include "cfg/CarbonCfg.h"

