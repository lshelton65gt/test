//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2008-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// \file XML parser for CADI memories

#ifndef _CfgXmlParserMem_h_
#define _CfgXmlParserMem_h_

#include "CfgXmlParser.h"
#include "CfgXmlParserCustomCode.h"

class CarbonCfg;
class CarbonCfgMemory;
class CarbonCfgMemoryBlock;
struct CarbonCfgMemoryLocAttr;
class UtIStream;

//! CfgXmlParserMem class
class CfgXmlParserMem : public CfgXmlParser
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  CfgXmlParserMem();

  //! destructor
  ~CfgXmlParserMem();

  //! parse XML register format from a stream
  bool parseStream(UtIStream &s, CarbonCfg *cfg);

  //! Parse top level tag
  void parseMemInfo(xmlNode *node, CarbonCfg *cfg);

private:
  void parseMemSpace(xmlNode *node, CarbonCfg *cfg);
  void parseMemBlock(xmlNode *node, CarbonCfgMemory* memSpace);
  void parseRTLLoc(xmlNode *node, CarbonCfgMemoryBlock* memBlock);
  void parsePortLoc(xmlNode *node, CarbonCfgMemoryBlock* memBlock);
  void parseUserLoc(xmlNode *node, CarbonCfgMemoryBlock* memBlock);
  void parseLocAttr(xmlNode *node, CarbonCfgMemoryLocAttr* attr);
  void parseELFLoader(xmlNode *node, CarbonCfg *cfg);
  void parseProcInfo(xmlNode *node, CarbonCfg *cfg);
  void parseProcessorOptions(xmlNode* node, CarbonCfgProcInfo* pi);
  void parsePcReg(xmlNode* node, CarbonCfgProcInfo* pi);
  void parseExtendedFeaturesReg(xmlNode* node, CarbonCfgProcInfo* pi);
};

#endif
