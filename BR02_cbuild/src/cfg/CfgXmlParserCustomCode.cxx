// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Contains a class to parse a custom code element and return the code
*/
#include "util/UtHashMap.h"
#include "util/LoopMap.h"
#include "util/UtIOStream.h"

#include "cfg/carbon_cfg.h"

#include "CfgXmlParserCustomCode.h"

CfgXmlParserCustomCode::CfgXmlParserCustomCode(xmlNode* node, const SectionNames& sections) :
  mNode(node),
  mLegalSections(sections)
{
  parseSetup();
}

CfgXmlParserCustomCode::CfgXmlParserCustomCode(xmlNode* node, const char** legalNames) :
  mNode(node)
{
  parseSetup();
  
  // Initialize legal sections
  for (UInt32 i = 0; legalNames[i] != NULL; ++i)
    mLegalSections[legalNames[i]] = i; // We don't care about the specific index anymore.
}

CfgXmlParserCustomCode::~CfgXmlParserCustomCode()
{
  parseCleanup();
}

// Example:
//      <customcode section="class" position="POST">
//         <![CDATA[
//                  //! Class to map to virtual registers
//                  CarbonVirtualRegisters* mVirtRegs;
//         ]]>
//       </customcode>
bool CfgXmlParserCustomCode::getCode(int* section,
                                     UtString *sectStr,
                                     CarbonCfgCustomCodePosition* position,
                                     UtString* code)
{
  // Get the section name attribute
  CfgXmlAttr sectionAttr(mNode, "section");
  if (sectionAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<customcode> element with no section attribute is not allowed.");
  }

  // Validate the section name
  UtString sectionStr(sectionAttr.value());
  SectionNames::const_iterator pos = mLegalSections.find(sectionStr);
  if (pos == mLegalSections.end()) {
    // TODO: use DTD/XSD??
    UtString error;
    error << "<customcode> element with invalid section '" << sectionStr << "'.\n"
          << "\tLegal sections include:";
    for (CLoopMap<SectionNames> l(mLegalSections); !l.atEnd(); ++l) {
      const UtString& validSection = l.getKey();
      error << " " << validSection;
    }
    error << ".";
    reportError(error.c_str());
  } else {
    *sectStr = sectionStr;
    *section = pos->second;
  }

  // Get the position attribute
  CfgXmlAttr positionAttr(mNode, "position");
  if (positionAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<customcode> element with no position attribute is not allowed.");
  }

  // Validate the legal positions (PRE and POST)
  UtString positionStr(positionAttr.value());
  if (positionStr == "PRE") {
    *position = eCarbonCfgPre;
  } else if (positionStr == "POST") {
    *position = eCarbonCfgPost;
  } else {
    // TODO: use DTD/XSD
    reportError("<customcode> element with invalid position attribute; legal values are PRE and POST");
  }

  // We must have a cdata node
  bool foundCdata = false;
  for (xmlNode *child = mNode->children; child != NULL; child = child->next) {
    if (isCdataNode(child)) {
      // Get the content and add it to the cfg
      getContent(child, code);
      foundCdata = true;
    }
  }

  // If we didn't find a cdata node, use the text. Hopefully the
  // writer knew what they were doing.
  if (!foundCdata) {
    getContent(mNode, code);
  }

  return !getError();
}
