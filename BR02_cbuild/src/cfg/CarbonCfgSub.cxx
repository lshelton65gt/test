// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2008-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  oCarbon configuration file data model for the sub components
*/
#include "cfg/CarbonCfg.h"

#include "util/CarbonAssert.h"


CarbonCfgSub::CarbonCfgSub(CarbonCfg* parent) :
  CarbonCfg(), mParent(parent)
{
  // Clear the component type.  This will cause the parent to be
  // queried for the type unless set explicitly later.
  putCompType("");
}

CarbonCfgSub::~CarbonCfgSub()
{
}

void CarbonCfgSub::putDB(CarbonDB*)
{
  INFO_ASSERT(false, "putDB() is not allowed on sub components.");
}

CarbonDB* CarbonCfgSub::getDB() const
{
   return mParent->getDB();
}

const char* CarbonCfgSub::getCompType() const
{
  // Query the component if the locally-stored type is empty.
  const char* thisCompType = CarbonCfg::getCompType();
  // Yes, strcmp is evil, but there's no point using UtString here,
  // since we need to return a const char*, and we can't call c_str()
  // on a temporary object.
  if (strcmp(thisCompType, "") == 0) {
    return mParent->getCompType();
  }
  return thisCompType;
}
