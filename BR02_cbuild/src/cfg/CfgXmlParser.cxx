//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CfgXmlParser.h"

#include "libxml/parser.h"
#include "libxml/xmlerror.h"


CfgXmlParser::CfgXmlParser()
  :mErrorReported(false)
{
  mDocument = NULL;
  mRootNode = NULL;
}

CfgXmlParser::CfgXmlParser(const char* xmlFile) 
  :mErrorReported(false)
{
  mDocument = xmlParseFile(xmlFile);
  mRootNode = xmlDocGetRootElement(mDocument);
}

CfgXmlParser::~CfgXmlParser()
{    
}


xmlNodePtr CfgXmlParser::findChildElement(xmlNodePtr root, const char* elementName)
{
  if (root)
  {
    for (xmlNodePtr node = root->children; node != NULL; node = node->next)
    {
      if (isElement(node, elementName))
        return node;
    }
  }
  return NULL;
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::sXmlStructuredErrorHandler(void *arg, xmlErrorPtr error)
{
  CfgXmlParser *cfgParser = (CfgXmlParser *) arg;
  const char *fileName = error->file;
  const char *msg = error->message;

  UtString message;
  message << ((fileName == NULL) ? "<unknown>" : fileName);
  message << ":" << error->line << ": " << msg;

  cfgParser->reportError(message.c_str());
}


///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::reportError(const char *message)
{
  mErrorText << message << "\n";
  mErrorReported = true;
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::parseSetup()
{
  // nothing has gone wrong yet
  mErrorReported = false;
  mErrorText.clear();

  // Check potential ABI mismatches between the version it was
  // compiled for and the actual shared library used.
  LIBXML_TEST_VERSION

  // replace default error handler
  xmlSetStructuredErrorFunc(this, sXmlStructuredErrorHandler);
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::parseCleanup()
{
  xmlCleanupParser();
}

///////////////////////////////////////////////////////////////////////////////
const char * CfgXmlParser::errorText()
{
  return mErrorText.c_str();
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::isElement(xmlNode *node, const char *name)
{
  if (node == NULL || node->type != XML_ELEMENT_NODE) {
    return false;
  }

  return strcmp((const char *) node->name, name) == 0;
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::isCdataNode(xmlNode *node)
{
  if ((node == NULL) || (node->type != XML_CDATA_SECTION_NODE)) {
    return false;
  } else {
    return true;
  }
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::hasAttr(xmlNode *node, const char *name)
{
  return xmlHasProp(node, (xmlChar *) name) != NULL;
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::getContent(xmlNode *node, UtString *content)
{
  *content = "";
  xmlChar *text = xmlNodeGetContent(node);
  if (text != 0) {
    *content = reinterpret_cast<const char *>(text);
    xmlFree(text);
  }
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::getContent(xmlNode *node, UInt8 *content)
{
  UtString strContent;
  getContent(node, &strContent);
  strContent >> *content;
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::getContent(xmlNode *node, UInt32 *content)
{
  UtString strContent;
  getContent(node, &strContent);
  strContent >> *content;
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::getContent(xmlNode *node, UInt64 *content)
{
  UtString strContent;
  getContent(node, &strContent);
  strContent >> *content;
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParser::getContent(xmlNode *node, bool *content)
{
  UtString strContent;
  getContent(node, &strContent);
  *content = (strContent == "true") ? true : false;
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::getAttribute(xmlNode *node, const char *name, UtString *value)
{
  bool result = hasAttr(node, name);
  if(result) {
    CfgXmlAttr attr(node, name);
    *value = attr.value();
  }
  return result;
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::getAttribute(xmlNode *node, const char *name, UInt8 *value)
{
  CfgXmlAttr attr(node, name);
  return attr.getValue(value);
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::getAttribute(xmlNode *node, const char *name, UInt32 *value)
{
  CfgXmlAttr attr(node, name);
  return attr.getValue(value);
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::getAttribute(xmlNode *node, const char *name, UInt64 *value)
{
  CfgXmlAttr attr(node, name);
  return attr.getValue(value);
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParser::getAttribute(xmlNode *node, const char *name, bool *value)
{
  UtString strVal;
  bool result = getAttribute(node, name, &strVal);
  if(result) {
    if(strVal == "true")
      *value = true;
    else
      *value = false;
  }
  return result;
}
