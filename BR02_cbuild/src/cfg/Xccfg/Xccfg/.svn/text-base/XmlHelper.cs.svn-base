﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// XML Helper Classes
  /// </summary>
  public class XmlHelper
  {
    /// <summary>
    /// Test for attribute
    /// </summary>
    /// <param name="node">Node</param>
    /// <param name="name">Name of attribute</param>
    /// <returns>true if found, false otherwise</returns>
    public static bool hasAttr(XmlNode node, string name)
    {
      return node.Attributes[name] != null;
    }

    /// <summary>
    /// The XML inner text for Element <paramref name="elementName"/> or <paramref name="defaultValue"/>
    /// if empty
    /// </summary>
    /// <param name="parent">Node</param>
    /// <param name="elementName">The Element name to locate</param>
    /// <param name="defaultValue">The default value if not located</param>
    /// <returns></returns>
    public static string text(XmlNode parent, string elementName, string defaultValue)
    {
      XmlNode childNode = parent.SelectSingleNode(elementName);
      if (childNode != null)
        return childNode.InnerText;
      else
        return defaultValue;
    }
  }
}
