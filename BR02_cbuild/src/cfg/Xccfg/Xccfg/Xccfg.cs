using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;  

namespace Carbon.Xccfg
{
  /// <summary>
  /// Class representing a Carbon Component Configuration File (XML)
  /// </summary>
  [Serializable]
  public class Xccfg
  {
    /// <summary>
    /// Platform
    /// </summary>
    public enum Platforms {
      /// <summary>
      /// Undefined
      /// </summary>
      Undefined, 
      /// <summary>
      /// ARM
      /// </summary>
      ARM, 
      /// <summary>
      /// CoWare
      /// </summary>
      CoWare, 
      /// <summary>
      /// System C
      /// </summary>
      SystemC };

    /// <summary>
    /// Access to the underlying source XmlDocument
    /// </summary>
    public XmlDocument Document { get; set; }
    /// <summary>
    /// The list of Transactor Instances
    /// </summary>
    public List<TransactorInstance> TransactorInstances { get; set; }

    /// <summary>
    /// The list of Tied-off Ports
    /// </summary>
    public List<TiedPort> TiedPorts { get; private set; }

    /// <summary>
    /// The list of SignalPorts
    /// </summary>
    public List<SignalPort> SignalPorts { get; private set; }

    /// <summary>
    /// The List of Disconnected Ports
    /// </summary>
    public List<DisconnectedPort> DisconnectedPorts { get; private set; }

    /// <summary>
    /// List of Parameters
    /// </summary>
    public List<Parameter> Parameters { get; private set; }

    /// <summary>
    /// TopModuleName specified by the user
    /// </summary>
    public string TopModuleName { get; set; }

    /// <summary>
    /// Library name, i.e. libdesign.a
    /// </summary>
    public string Library { get; set; }
    /// <summary>
    /// The IODB referenced by this ccfg file
    /// </summary>
    public string IODB { get; set; }
    /// <summary>
    /// User-provided description
    /// </summary>
    public string Description { get; set; }
    /// <summary>
    /// Cxx compilation flags
    /// </summary>
    public string CxxFlags { get; set; }
    /// <summary>
    /// Linker flags
    /// </summary>
    public string LinkFlags { get; set; }
    /// <summary>
    /// Source Files
    /// </summary>
    public string SouceFiles { get; set; }
    /// <summary>
    /// Load file extension
    /// </summary>
    public string LoadFileExtension { get; set; }
    /// <summary>
    /// Use static scheduling
    /// </summary>
    public bool UseStaticScheduling { get; set; }
    /// <summary>
    /// Platform type
    /// </summary>
    public Platforms Platform { get; set; }

    private static List<TransactorLibrary> _libs = null;

    /// <summary>
    /// List of Transactor Libraries
    /// </summary>
    public List<TransactorLibrary> Libraries
    {
      get
      {
        if (_libs == null)
          _libs = TransactorLibrary.ReadLibrary();
        return _libs;
      }
    }

    /// <summary>
    /// Find a top-level Parameter
    /// </summary>
    /// <param name="paramName">The name of the parameter to locate</param>
    /// <returns>Parameter object or null if not found</returns>
    public Parameter FindParameter(string paramName)
    {
      return Parameters.Find(p => p.Name == paramName);
    }
    
    /// <summary>
    /// Find a transactor from the library
    /// </summary>
    /// <param name="xtorName">The name of the transactor</param>
    /// <returns>Transactor or null if not found</returns>
    public Transactor FindTransactor(string xtorName)
    {
      foreach (TransactorLibrary lib in Libraries)
      {
        Transactor trans = lib.Transactors.Find(x => x.Name == xtorName);
        if (trans != null)
          return trans;
      }
      return null;
    }

    // Locate a transactor instance
    /// <summary>
    /// Find a transactor instance by instance name
    /// </summary>
    /// <param name="xtorInstanceName">The instance name of the transactor</param>
    /// <returns>TransactorInstance or null if not found</returns>
    public TransactorInstance FindTransactorInstance(string xtorInstanceName)
    {
      TransactorInstance inst = TransactorInstances.Find(x => x.Name == xtorInstanceName);
      return inst;
    }

    /// <summary>
    /// The XML ccfg file loaded in the constructor
    /// </summary>
    public string FileName { get; private set; }

    /// <summary>
    /// Construct a Xccfg file from an .XML ccfg source file
    /// </summary>
    /// <example>di
    /// var x = new Xccfg("foo.ccfg");
    /// </example>
    /// <param name="xmlCcfgFilePath">The path to the XML .ccfg file</param>
    /// <exception cref="System.Exception">Exception thrown upon error</exception>
    public Xccfg(string xmlCcfgFilePath)
    {
      FileName = xmlCcfgFilePath;

      Console.WriteLine(string.Format("Processing: {0}", xmlCcfgFilePath));

      TransactorInstances = new List<TransactorInstance>();
      TiedPorts = new List<TiedPort>();
      SignalPorts = new List<SignalPort>();
      DisconnectedPorts = new List<DisconnectedPort>();
      Parameters = new List<Parameter>();

      Document = new XmlDocument();
      Document.Load(xmlCcfgFilePath);

      XmlNode root = Document.DocumentElement;

      TopModuleName = XmlHelper.text(root, "TopModule", "");
      Library = XmlHelper.text(root, "Library", "");
      IODB = XmlHelper.text(root, "IODB", "");
      Description = XmlHelper.text(root, "Description", "");
      LinkFlags = XmlHelper.text(root, "LinkFlags", "");
      LoadFileExtension = XmlHelper.text(root, "LoadFileExtension", "");

      Platform = (Platforms)Enum.Parse(typeof(Platforms), XmlHelper.text(root, "Platform", "ARM"), true);

      UseStaticScheduling = XmlHelper.text(root, "UseStaticScheduling", "0") == "0" ? false : true;

      foreach(XmlNode xtorInstance in root.SelectNodes("Xtor"))
      {
        TransactorInstance xinst = new TransactorInstance(this, xtorInstance);
        TransactorInstances.Add(xinst);
      }

      // Read the parameters now, because the ESLPorts could make references to them.
      foreach (XmlNode paramNode in root.SelectNodes("ParamDef/parameter"))
      {
        Parameters.Add(new Parameter(paramNode));
      }

      // Walk all the RtlPorts and turn them into SignalPorts or connect them to a transactor
      foreach (XmlNode rtl in root.SelectNodes("RtlPort"))
      {   
        bool isTied = isTie(rtl);
        bool isTiedParam = isTieParameter(rtl);
        bool isDisconnected = isDisconnect(rtl);       
        bool isAXtorConn = isXtorConnection(rtl);        
        bool isASignalPort = isSignalPort(rtl);
        bool isAClockGen = isClockGenerator(rtl);
        bool isAResetGen = isResetGenerator(rtl);
        bool isAScClock = isScClock(rtl);
        bool isTiedXtorParam = isTiedXtorParameter(rtl);

        // SignalPort (Tied, TiedParam, Disconnect, ESLPort
        if (isTied)
          TiedPorts.Add(new TiedPort(rtl));

        if (isASignalPort)
          SignalPorts.Add(new SignalPort(this, rtl));

        // RTL Pin connected to parameter
        if (isTiedParam)
        {
          XmlNode tp = rtl.SelectSingleNode("TieParameter");
          string paramName = XmlHelper.text(tp, "Parameter", null);
          Parameter param = FindParameter(paramName);
          param.AddRTLPort(rtl);
        }

        // RTL Pin connected to Xtor Instance param
        if (isTiedXtorParam)
        {
          XmlNode tp = rtl.SelectSingleNode("TieParameter");
          string xtorInstName = XmlHelper.text(tp, "XtorInstance", null);
          string xtorParamName = XmlHelper.text(tp, "Parameter", null);

          TransactorInstance xinst = FindTransactorInstance(xtorInstName);
          TransactorInstanceParameter xinstParam = xinst.FindParameter(xtorParamName);
          xinstParam.AddRTLPort(rtl);
        }

        if (isDisconnected)
          DisconnectedPorts.Add(new DisconnectedPort(rtl));

        if (isAXtorConn)
        {
          string xtorInstName = rtl.SelectSingleNode("XtorConnection").Attributes["name"].Value;
          TransactorInstance xinst = FindTransactorInstance(xtorInstName);
          if (xinst == null)
            throw new Exception(string.Format("Error: Expecting to find XtorInstance: '{0}'", xtorInstName));
          xinst.AddConnection(rtl);         
        }
      }
      
      // Post-processing here (if any)
    }

    private bool isDisconnect(XmlNode rtl)
    {
      return rtl.ChildNodes.Count == 0;
    }

    private bool isTie(XmlNode rtl)
    {
      return rtl.SelectSingleNode("Tie") != null;
    }

    // A port connected to a Parameter
    private bool isTieParameter(XmlNode rtl)
    {
      XmlNode tp = rtl.SelectSingleNode("TieParameter");
      if (tp != null)
      {
        string xtorInst = XmlHelper.text(tp, "XtorInstance", null);
        return string.IsNullOrEmpty(xtorInst);
      }
      return false;
    }

    // A port connected to a Xtor instance Parameter
    private bool isTiedXtorParameter(XmlNode rtl)
    {
      XmlNode tp = rtl.SelectSingleNode("TieParameter");
      if (tp != null)
      {
        string xtorInst = XmlHelper.text(tp, "XtorInstance", null);
        return !string.IsNullOrEmpty(xtorInst);
      }
      return false;
    }


    private bool isClockGenerator(XmlNode rtl)
    {
      return rtl.SelectSingleNode("ClockGenerator") != null;
    }

    private bool isResetGenerator(XmlNode rtl)
    {
      return rtl.SelectSingleNode("ResetGenerator") != null;
    }

    private bool isScClock(XmlNode rtl)
    {
      return rtl.SelectSingleNode("ScClock") != null;
    }

    private bool isXtorConnection(XmlNode rtl)
    {
      return rtl.SelectSingleNode("XtorConnection") != null;
    }

    private bool isSignalPort(XmlNode rtl)
    {
      return rtl.SelectSingleNode("ESLPort") != null;
    }

  }
}
