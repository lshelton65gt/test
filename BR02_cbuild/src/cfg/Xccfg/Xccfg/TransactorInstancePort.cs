﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// Transactor Instance Port 
  /// </summary>
  public class TransactorInstancePort
  {
    /// <summary>
    /// Port Direction types
    /// </summary>
    public enum PortDirections 
    {
      /// <summary>
      /// Input
      /// </summary>
      Input, 
      /// <summary>
      /// Output
      /// </summary>
      Output, 
      /// <summary>
      /// Bi-directional
      /// </summary>
      InOut };

    /// <summary>
    /// The Logical port name
    /// </summary>
    public string LogicalPort { get; set; }
    /// <summary>
    /// The Physical (RTL) port name
    /// </summary>
    public string PhysicalPort { get; set; }
    /// <summary>
    /// Width (in bits)
    /// </summary>
    public uint Width { get; set; }
    /// <summary>
    /// Port Direction
    /// </summary>
    public PortDirections Direction { get; set; }

    /// <summary>
    /// Expression
    /// </summary>
    public string Expression { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rtl"></param>
    public TransactorInstancePort(XmlNode rtl)
    {
      PhysicalPort = rtl.Attributes["name"].Value;
      Width = uint.Parse(rtl.Attributes["width"].Value);
      Direction = (PortDirections)Enum.Parse(typeof(PortDirections), rtl.Attributes["type"].Value, true);

      XmlNode xtorConn = rtl.SelectSingleNode("XtorConnection");

      Expression = xtorConn.Attributes["expression"].Value;
      LogicalPort = xtorConn.Attributes["port"].Value;
    }
  }
}
