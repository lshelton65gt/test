﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// A Signal port mapping between the Logical and Physical
  /// </summary>
  public class SignalPort
  {
   /// <summary>
   /// Direction
   /// </summary>
    public enum PortDirections 
    { 
      /// <summary>
      /// Input
      /// </summary>
      Input, 
      /// <summary>
      /// Output
      /// </summary>
      Output, 
      /// <summary>
      /// Bi-Directional
      /// </summary>
      InOut };

    /// <summary>
    /// The Logical port
    /// </summary>
    public string LogicalPort { get; set; }
    /// <summary>
    /// The Physical (RTL) port
    /// </summary>
    public string PhysicalPort { get; set; }
    /// <summary>
    /// Width (in bits)
    /// </summary>
    public uint Width { get; set; }
    /// <summary>
    /// Direction
    /// </summary>
    public PortDirections Direction { get; set; }

    /// <summary>
    /// Parameter this port is connected to or null if not connected
    /// </summary>
    public Parameter Parameter { get; private set; }

    /// <summary>
    /// Port is tied to a Parameter
    /// </summary>
    public bool IsTiedParam
    {
      get
      {
        return Parameter != null;
      }
    }

    /// <summary>
    /// SystemC TypeDef
    /// </summary>
    public string TypeDef { get; set; }

    /// <summary>
    /// Expression
    /// </summary>
    public string Expression { get; set; }

    // unused?
    private string _xtorInstanceName;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="ccfg"></param>
    /// <param name="rtl"></param>
    public SignalPort(Xccfg ccfg, XmlNode rtl)
    {
      PhysicalPort = rtl.Attributes["name"].Value;
      Width = uint.Parse(rtl.Attributes["width"].Value);
      Direction = (PortDirections)Enum.Parse(typeof(PortDirections), rtl.Attributes["type"].Value, true);

      XmlNode eslPort = rtl.SelectSingleNode("ESLPort");
      if (eslPort == null)
        throw new Exception(string.Format("Expecing ESLPort for RtlPort '{0}'", PhysicalPort));

      LogicalPort = eslPort.Attributes["name"].Value;

      if (XmlHelper.hasAttr(eslPort, "typeDef"))
        TypeDef = eslPort.Attributes["typeDef"].Value;


      _xtorInstanceName = XmlHelper.text(eslPort, "ParamXtorInstanceName", null); // Seems to be an unused field??

      string paramName = XmlHelper.text(eslPort, "ParameterName", null);

      Parameter = ccfg.FindParameter(paramName);

      Expression = XmlHelper.text(eslPort, "Expression", null);
    }
  }
}
