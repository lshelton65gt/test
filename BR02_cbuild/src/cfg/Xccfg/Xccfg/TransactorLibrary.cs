﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;

namespace Carbon.Xccfg
{
  /// <summary>
  /// A collection of Transactors read from xactors.xml
  /// </summary>
  public class TransactorLibrary
  {
    /// <summary>
    /// Name of the Library
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Major Version
    /// </summary>
    public int MajorVersion { get; set; }
    /// <summary>
    /// Minor Version
    /// </summary>
    public int MinorVersion { get; set; }

    /// <summary>
    /// List of Transactors
    /// </summary>
    public List<Transactor> Transactors { get; private set; }

    private static Dictionary<string, TransactorLibrary> _xtorLibs;
    private const string CARBON_DEFAULT_XTOR_LIB = "CarbonDefault";

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="name"></param>
    public TransactorLibrary(string name)
    {
      Transactors = new List<Transactor>();
      MajorVersion = 1;
      MinorVersion = 0;
      Name = name;
    }

    /// <summary>
    /// Read the definitions
    /// </summary>
    /// <param name="libfilename"></param>
    /// <returns></returns>
    public bool ReadXtorDefinitions(string libfilename)
    {
      XmlDocument xd = new XmlDocument();
      xd.Load(libfilename);

      Debug.WriteLine(libfilename);

      if (xd.DocumentElement.Name == "transactors")
      {        
        string v = xd.DocumentElement.Attributes["version"].Value;
        string[] versionParts = v.Split('.');
        if (versionParts.Length != 2)
          throw new Exception(string.Format("Invalid version format: '{0}'; expecting X.Y", v));

        MajorVersion = int.Parse(versionParts[0]);
        MinorVersion = int.Parse(versionParts[1]);

        foreach (XmlNode compNode in xd.DocumentElement.SelectNodes("//transactors//component"))
        {
          Transactor xtor = Transactor.Parse(compNode);
          if (Transactors.Find(x => x.Name == xtor.Name) == null)
            Transactors.Add(xtor);
        }
      }
      else
        throw new Exception(string.Format("Error: Failed to parse: {0}", libfilename));

      return true;
    }

    /// <summary>
    /// Read all the Transactors
    /// </summary>
    /// <returns></returns>
    public static List<TransactorLibrary> ReadLibrary()
    {
      _xtorLibs = new Dictionary<string, TransactorLibrary>();
      List<TransactorLibrary> libs = new List<TransactorLibrary>();

      TransactorLibrary xl = new TransactorLibrary(CARBON_DEFAULT_XTOR_LIB);
      bool newLib = addXtorLib(xl);
      if (newLib)
        libs.Add(xl);

      List<string> xmlFiles = new List<string>();

      string carbon_home = System.Environment.GetEnvironmentVariable("CARBON_HOME");
      if (string.IsNullOrEmpty(carbon_home))
        throw new Exception("$CARBON_HOME is not defined, cannot read transactor library");

      string xmlFile = Path.Combine(carbon_home, "lib");
      xmlFile = Path.Combine(xmlFile, "xactors");
      xmlFile = Path.Combine(xmlFile, "xactors.xml");
      xmlFiles.Add(xmlFile);

      string maxsim_prot = System.Environment.GetEnvironmentVariable("MAXSIM_PROTOCOLS");
      if (!string.IsNullOrEmpty(maxsim_prot))
        findXtorDefs(maxsim_prot, xmlFiles);
      else
        throw new Exception("Could not find transactor definition file (MAXSIM_PROTOCOLS appear to be unset)");

      bool ret = false;
      // Now, loop through the files
      foreach (string xactorXmlFile in xmlFiles)
      {
        bool success = xl.ReadXtorDefinitions(xactorXmlFile);
        ret |= success;
      }

      return libs;
    }

    private static bool addXtorLib(TransactorLibrary lib)
    {
      string name = lib.Name;
      if (_xtorLibs.ContainsKey(name))
        return false;
   
      _xtorLibs[lib.Name] = lib;
      return true;
    }
    private static TransactorLibrary findXtorLib(string libraryName)
    {
      if (_xtorLibs.ContainsKey(libraryName))
        return _xtorLibs[libraryName];
      else
        return null;
    }

    private static void findXtorDefs(string dir, List<string> xmlFiles)
    {
      if (Directory.Exists(dir))
      {
        // First recurse down
        foreach (string dirName in Directory.GetDirectories(dir))
          findXtorDefs(dirName, xmlFiles);

        foreach (string fileName in Directory.GetFiles(dir))
        {
          FileInfo fi = new FileInfo(fileName);
          if (fi.Name == "xactors.xml")
            xmlFiles.Add(fileName);
        }
      }
    }
  }
}
