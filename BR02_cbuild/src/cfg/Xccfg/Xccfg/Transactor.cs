﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// A Transactor as represented within xactors.xml file
  /// </summary>
  public class Transactor
  {
    /// <summary>
    /// List of Parameters
    /// </summary>
    public List<TransactorParameter> Parameters { get; private set; }
    
    /// <summary>
    /// List of Ports
    /// </summary>
    public List<TransactorPort> Ports { get; private set; }

    /// <summary>
    /// Name of Transactor
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Has write debug capability
    /// </summary>
    public bool HasWriteDebug { get; set; }
    /// <summary>
    /// Is a clock master
    /// </summary>
    public bool IsXtorClockMaster { get; set; }
    /// <summary>
    /// Is flow through
    /// </summary>
    public bool IsFlowThrough { get; set; }
    /// <summary>
    /// Supports save and restore
    /// </summary>
    public bool IsSaveRestore { get; set; }
    /// <summary>
    /// Is a Master
    /// </summary>
    public bool IsMaster { get; set; }
    /// <summary>
    /// Is a Slave
    /// </summary>
    public bool IsSlave { get; set; }
    /// <summary>
    /// Is Amba based
    /// </summary>
    public bool IsAmba { get; set; }
    /// <summary>
    /// Has debug transaction
    /// </summary>
    public bool HasDebugTransaction { get; set; }
    /// <summary>
    /// Is SoC Designer Transactor
    /// </summary>
    public bool IsSDXtor { get; set; }
    /// <summary>
    /// Use event queue
    /// </summary>
    public bool UseEventQueue { get; set; }
    /// <summary>
    /// Use communicate
    /// </summary>
    public bool UseCommunicate { get; set; }
    /// <summary>
    /// Use ESL Clock Master
    /// </summary>
    public bool UseESLClockMaster { get; set; }
    /// <summary>
    /// Use ESL Reset Master
    /// </summary>
    public bool UseESLResetMaster { get; set; }
    /// <summary>
    /// Synonym
    /// </summary>
    public string Synonym { get; set; }
    /// <summary>
    /// Class Name
    /// </summary>
    public string ClassName { get; set; }
    /// <summary>
    /// Include File
    /// </summary>
    public string IncludeFile { get; set; }
    /// <summary>
    /// Library File
    /// </summary>
    public string LibFile { get; set; }
    /// <summary>
    /// Include Path
    /// </summary>
    public string IncludePath { get; set; }
    /// <summary>
    /// Library Path
    /// </summary>
    public string LibPath { get; set; }
    /// <summary>
    /// Protocol Name
    /// </summary>
    public string ProtocolName { get; set; }
    /// <summary>
    /// Constructor arguments
    /// </summary>
    public string ConstructorArgs { get; set; }

    /// <summary>
    /// Find Transactor parameter by Name
    /// </summary>
    /// <param name="paramName">Name of parameter</param>
    /// <returns></returns>
    public TransactorParameter FindParameter(string paramName)
    {
      return Parameters.Find(p => p.Name == paramName);
    }

    /// <summary>
    /// Construct a Transactor
    /// </summary>
    /// <param name="root"></param>
    protected Transactor(XmlNode root)
    {
      Parameters = new List<TransactorParameter>();
      Ports = new List<TransactorPort>();

      Name = root.Attributes["name"].Value;

      HasWriteDebug = XmlHelper.hasAttr(root, "write_debug");
      IsXtorClockMaster = XmlHelper.hasAttr(root, "xtor_clock_master");
      IsFlowThrough = XmlHelper.hasAttr(root, "flow_thru");
      IsSaveRestore = XmlHelper.hasAttr(root, "save_restore");
      IsMaster = XmlHelper.hasAttr(root, "master");
      IsSlave = XmlHelper.hasAttr(root, "slave");
      IsAmba = XmlHelper.hasAttr(root, "amba_xtor");
      HasDebugTransaction = XmlHelper.hasAttr(root, "debug_transaction");
      IsSDXtor = XmlHelper.hasAttr(root, "sd_xactor");
      UseEventQueue = XmlHelper.hasAttr(root, "use_event_queue");
      UseCommunicate = XmlHelper.hasAttr(root, "use_communicate");
      UseESLClockMaster = XmlHelper.hasAttr(root, "use_esl_clock_master");
      UseESLResetMaster = XmlHelper.hasAttr(root, "use_esl_reset_master");

      // parameters
      foreach (XmlNode paramNode in root.SelectNodes("//parameter"))
        Parameters.Add(new TransactorParameter(paramNode));
   
      foreach (XmlNode portNode in root.SelectNodes("//port"))
        Ports.Add(new TransactorPort(portNode));

      Synonym = XmlHelper.text(root, "synonym", "");

      ClassName = XmlHelper.text(root, "className", "");
      IncludeFile = XmlHelper.text(root, "includeFile", "");
      LibFile = XmlHelper.text(root, "libFile", "");
      IncludePath = XmlHelper.text(root, "incPath", "");
      LibPath= XmlHelper.text(root, "libPath", "");
      ProtocolName = XmlHelper.text(root, "protocolName", "");
      ConstructorArgs = XmlHelper.text(root, "constructorArgs", "");
   
    }

    /// <summary>
    /// Parse the Transactor
    /// </summary>
    /// <param name="root">Parent node</param>
    /// <returns>Transactor</returns>
    public static Transactor Parse(XmlNode root)
    {
      return new Transactor(root);
    }
  }

  /// <summary>
  /// Transactor Port
  /// </summary>
  public class TransactorPort
  {
    /// <summary>
    /// Port Types
    /// </summary>
    public enum PortTypes 
    { 
      /// <summary>
      /// Input
      /// </summary>
      Input, 
      /// <summary>
      /// Output
      /// </summary>
      Output, 
      /// <summary>
      /// Bi-Directional
      /// </summary>
      InOut };

    /// <summary>
    /// Port Type
    /// </summary>
    public PortTypes PortType { get; set; }
    /// <summary>
    /// Port Name
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Width of port
    /// </summary>
    public int Width { get; set; }
    /// <summary>
    /// Description of port
    /// </summary>
    public string Description { get; set; }
    /// <summary>
    /// Port is optional
    /// </summary>
    public bool Optional { get; set; }
    /// <summary>
    /// Is clock sensitive
    /// </summary>
    public bool IsClockSensitive { get; set; }
    /// <summary>
    /// Event name
    /// </summary>
    public string EventName { get; set; }

    /// <summary>
    /// Construct a TransactorPort
    /// </summary>
    /// <param name="root">Parent node</param>
    public TransactorPort(XmlNode root)
    {
      Name = root.Attributes["name"].Value;
      Width = int.Parse(root.Attributes["width"].Value);
      PortType = (PortTypes)Enum.Parse(typeof(PortTypes), root.Attributes["direction"].Value, true);
      Description = XmlHelper.text(root, "description", "");

      if (XmlHelper.hasAttr(root, "optional"))
        Optional = bool.Parse(root.Attributes["optional"].Value);

      IsClockSensitive = XmlHelper.hasAttr(root, "is_clock_sensitive");

      if (XmlHelper.hasAttr(root, "event_name"))
        EventName = root.Attributes["event_name"].Value;
    }

  }
  
  /// <summary>
  /// Transactor Parameter
  /// </summary>
  public class TransactorParameter
  {
    /// <summary>
    /// Data Type
    /// </summary>
    public enum DataTypes
    {  
      /// <summary>
      /// Boolean
      /// </summary>
      Bool, 
      /// <summary>
      /// Integer
      /// </summary>
      Integer, 
      /// <summary>
      /// Double precision
      /// </summary>
      Double, 
      /// <summary>
      /// String
      /// </summary>
      String, 
      /// <summary>
      /// 64-Bit Integer
      /// </summary>
      Integer64 };
    
    /// <summary>
    /// Scope
    /// </summary>
    public enum Scopes
    { 
      /// <summary>
      /// Run-time
      /// </summary>
      Runtime, 
      /// <summary>
      /// Compile-time
      /// </summary>
      CompileTime, 
      /// <summary>
      /// Both Run-Time and Compile-time
      /// </summary>
      RunTimeAndCompileTime, 
      /// <summary>
      /// Init time
      /// </summary>
      InitTime };

    /// <summary>
    /// Size 
    /// </summary>
    public int Size { get; set; }
    /// <summary>
    /// Mininum or null if not specified
    /// </summary>
    public string Min { get; set; }
    /// <summary>
    /// Maximum or null if not specified
    /// </summary>
    public string Max { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Data type
    /// </summary>
    public DataTypes DataType { get; set; }
    /// <summary>
    /// Scope
    /// </summary>
    public Scopes Scope { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }
    /// <summary>
    /// Enumerated choices
    /// </summary>
    public string EnumChoices { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="root"></param>
    public TransactorParameter(XmlNode root)
    {
      Name = root.Attributes["name"].Value;
      DataType = (DataTypes)Enum.Parse(typeof(DataTypes), root.Attributes["type"].Value, true);
      Scope = (Scopes)Enum.Parse(typeof(Scopes), root.Attributes["scope"].Value.Replace("-", ""), true);

      if (XmlHelper.hasAttr(root, "value"))
      {
        int size = 0;
        if (int.TryParse(root.Attributes["value"].Value, out size))
          Size = size;
      }

      if (XmlHelper.hasAttr(root, "min"))
        Min = root.Attributes["min"].Value;

      if (XmlHelper.hasAttr(root, "max"))
        Max = root.Attributes["max"].Value;

      Description = XmlHelper.text(root, "description", "");
      EnumChoices = XmlHelper.text(root, "enumChoices", "");
    }
  }

}
