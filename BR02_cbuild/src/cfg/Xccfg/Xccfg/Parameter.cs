﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

// <parameter>
namespace Carbon.Xccfg
{
  /// <summary>
  /// A User-Defined Parameter
  /// </summary>
  public class Parameter
  {
    /// <summary>
    /// Name of parameter
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Data Type 
    /// </summary>
    public TransactorParameter.DataTypes DataType { get; set; }
    /// <summary>
    /// Scope
    /// </summary>
    public TransactorParameter.Scopes Scope { get; set; }
    /// <summary>
    /// Value
    /// </summary>
    public string Value { get; set; }
    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }
    /// <summary>
    /// List of RTL Ports this is connected to
    /// </summary>
    public List<RTLPort> RTLPorts { get; private set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="r"></param>
    public Parameter(XmlNode r)
    {
      RTLPorts = new List<RTLPort>();
      Name = r.Attributes["name"].Value;
      DataType = (TransactorParameter.DataTypes)Enum.Parse(typeof(TransactorParameter.DataTypes), r.Attributes["type"].Value, true);
      Scope = (TransactorParameter.Scopes)Enum.Parse(typeof(TransactorParameter.Scopes), r.Attributes["scope"].Value.Replace("-", ""), true);
      Value = r.Attributes["value"].Value;
      Description = XmlHelper.text(r, "description", "");
    }

    /// <summary>
    /// Connect RTL Port to this Parameter
    /// </summary>
    /// <param name="rtl"></param>
    public void AddRTLPort(XmlNode rtl)
    {
      RTLPorts.Add(new RTLPort(rtl));
    }
  }
}
