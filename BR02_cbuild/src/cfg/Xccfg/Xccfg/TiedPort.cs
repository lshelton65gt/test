﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// Port is Tied-off
  /// </summary>
  public class TiedPort
  {
    /// <summary>
    /// Width
    /// </summary>
    public uint Width { get; set; }
    /// <summary>
    /// Tied to Value
    /// </summary>
    public string TiedValue { get; set; }
    /// <summary>
    /// Physical (RTL) port name
    /// </summary>
    public string PhysicalPort { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rtl"></param>
    public TiedPort(XmlNode rtl)
    {
      PhysicalPort = rtl.Attributes["name"].Value;
      Width = uint.Parse(rtl.Attributes["width"].Value);
      TiedValue = XmlHelper.text(rtl, "Tie", "");
    }
  }
}
