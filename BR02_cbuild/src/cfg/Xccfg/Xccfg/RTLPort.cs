﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// An RTL Port
  /// </summary>
  public class RTLPort
  {
    /// <summary>
    /// Direction
    /// </summary>
    public enum PortDirections
    { 
      /// <summary>
      /// Input
      /// </summary>
      Input, 
      /// <summary>
      /// Output
      /// </summary>
      Output, 
      /// <summary>
      /// Bi-directional
      /// </summary>
      InOut };

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Width (in bits)
    /// </summary>
    public UInt32 Width { get; set; }
    /// <summary>
    /// Direction of Port
    /// </summary>
    public PortDirections Direction { get; set; }

    /// <summary>
    /// Parse port
    /// </summary>
    /// <param name="root"></param>
    /// <returns></returns>
    public static RTLPort Parse(XmlNode root)
    {
      RTLPort port = new RTLPort(root);
      return port;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="root"></param>
    public RTLPort(XmlNode root)
    {
      Direction = (PortDirections)Enum.Parse(typeof(PortDirections), root.Attributes["type"].Value, true);
      Width = UInt32.Parse(root.Attributes["width"].Value);
      Name = root.Attributes["name"].Value;
    }
  }
}
