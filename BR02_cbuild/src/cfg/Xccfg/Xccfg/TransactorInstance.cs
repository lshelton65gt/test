﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// Transactor Instance
  /// </summary>
  public class TransactorInstance
  {
    /// <summary>
    /// Instance name
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Library this transactor belongs to
    /// </summary>
    public string Library { get; set; }

    /// <summary>
    /// Ports of this instance
    /// </summary>
    public List<TransactorInstancePort> Ports { get; private set; }
    /// <summary>
    /// Parameters of this instance
    /// </summary>
    public List<TransactorInstanceParameter> Parameters { get; private set; }

    /// <summary>
    /// Underlying Transactor this is an instance of
    /// </summary>
    public Transactor Transactor { get; private set; }

    /// <summary>
    /// Use Debug Access
    /// </summary>
    public bool UseDebugAccess { get; set; }
    /// <summary>
    /// Use Clock Master
    /// </summary>
    public string ClockMaster { get; set; }
    /// <summary>
    /// Use Reset Master
    /// </summary>
    public string ResetMaster { get; set; }
    /// <summary>
    /// Variant
    /// </summary>
    public string Variant { get; set; }
    /// <summary>
    /// Library Name
    /// </summary>
    public string LibraryName { get; set; }

    /// <summary>
    /// Abstraction
    /// </summary>
    public string Abstraction { get; set; }

    /// <summary>
    /// Construct a TransactorInstance
    /// </summary>
    /// <param name="ccfg">Ccfg file</param>
    /// <param name="root">Parent node</param>
    public TransactorInstance(Xccfg ccfg, XmlNode root)
    {
      Ports = new List<TransactorInstancePort>();
      Parameters = new List<TransactorInstanceParameter>();

      Name = root.Attributes["name"].Value;

      LibraryName = XmlHelper.text(root, "LibraryName", null);
      Variant = XmlHelper.text(root, "Variant", null);
      Abstraction = XmlHelper.text(root, "Abstraction", null);

      string type = root.Attributes["type"].Value;
      Transactor = ccfg.FindTransactor(type);

      UseDebugAccess = XmlHelper.text(root, "DebugAccess", "0") == "1" ? true : false;
      ClockMaster = XmlHelper.text(root, "ClockMaster", null);
      ResetMaster = XmlHelper.text(root, "ResetMaster", null);

      foreach (XmlNode parm in root.SelectNodes("Parameter"))
      {
        Parameters.Add(new TransactorInstanceParameter(parm));
      }
    }

    /// <summary>
    /// Find a <see cref="TransactorInstanceParameter"/> by name
    /// </summary>
    /// <param name="paramName">Name of the Parameter</param>
    /// <returns><see cref="TransactorInstanceParameter"/></returns>
    public TransactorInstanceParameter FindParameter(string paramName)
    {
      return Parameters.Find(p => p.Name == paramName);
    }

   /// <summary>
   /// Add a Connection to this Transactor Instance
   /// </summary>
   /// <param name="rtl"></param>
    public void AddConnection(XmlNode rtl)
    {
      TransactorInstancePort tpi = new TransactorInstancePort(rtl);
      Ports.Add(tpi);
    }
  }

  /// <summary>
  /// A Parameter on a <see cref="TransactorInstance"/>
  /// </summary>
  public class TransactorInstanceParameter
  {
    /// <summary>
    /// Name of the Parameter
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Value for the Parameter
    /// </summary>
    public string Value { get; set; }

    /// <summary>
    /// List of <see cref="RTLPort"/> attached to this parameter
    /// </summary>
    public List<RTLPort> RTLPorts { get; private set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="p"></param>
    public TransactorInstanceParameter(XmlNode p)
    {
      RTLPorts = new List<RTLPort>();
      Name = XmlHelper.text(p, "Pname", null);
      Value = XmlHelper.text(p, "Value", null);
    }
    /// <summary>
    /// Add an RTL Port to this parameter
    /// </summary>
    /// <param name="rtl"></param>
    public void AddRTLPort(XmlNode rtl)
    {
      RTLPorts.Add(new RTLPort(rtl));
    }
  }
}
