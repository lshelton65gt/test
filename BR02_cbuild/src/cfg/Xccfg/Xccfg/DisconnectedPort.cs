﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Carbon.Xccfg
{
  /// <summary>
  /// A Disconnected port
  /// </summary>
  public class DisconnectedPort
  {
    /// <summary>
    /// Direction Types
    /// </summary>
    public enum PortDirections
    { 
      /// <summary>
      /// Input
      /// </summary>
      Input, 
      /// <summary>
      /// Output
      /// </summary>
      Output, 
      /// <summary>
      /// Bi-Directional
      /// </summary>
      InOut };

    /// <summary>
    /// Width (in bits)
    /// </summary>
    public uint Width { get; set; }
    /// <summary>
    /// Direction
    /// </summary>
    public PortDirections Direction { get; set; }
    /// <summary>
    /// Physical (RTL) port name
    /// </summary>
    public string PhysicalPort { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rtl"></param>
    public DisconnectedPort(XmlNode rtl)
    {
      PhysicalPort = rtl.Attributes["name"].Value;
      Width = uint.Parse(rtl.Attributes["width"].Value);
      Direction = (PortDirections)Enum.Parse(typeof(PortDirections), rtl.Attributes["type"].Value, true);
    }
  }
}
