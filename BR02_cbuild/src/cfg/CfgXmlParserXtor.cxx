//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtShellTok.h"

#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"

#include "CfgXmlParserXtor.h"

#include "libxml/parser.h"

CfgXmlParserXtor::CfgXmlParserXtor()
{
}

CfgXmlParserXtor::~CfgXmlParserXtor()
{
}

///////////////////////////////////////////////////////////////////////////////
bool CfgXmlParserXtor::parseFile(const char *fileName, CarbonCfgXtorLib *xtorLib)
{
  parseSetup();


  // parse the transactor defs
  UtString message;
  if (OSStatFile(fileName, "r", &message) != 1) {
    reportError(message.c_str());
    return false;
  }
  xmlDocPtr doc = xmlReadFile(fileName, NULL, 0);

  if (doc == NULL) {
    message << "Failed to parse file " <<  fileName;
    reportError(message.c_str());
  }
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "transactors")) {
      UtString message;
      message << fileName << ": root element must be 'transactors'";
      reportError(message.c_str());
    }
    else {
      // Parse the version in case we need it to determine some features
      UInt32 major = 1;
      UInt32 minor = 0;
      UtString versionStr;
      if (getAttribute(rootNode, "version", &versionStr)) {
        if (sscanf(versionStr.c_str(), "%d.%d", &major, &minor) != 2) {
          UtString message;
          message << "Invalid version format for transactors element '" << versionStr << "'; expecing X.Y";
          reportError(message.c_str());
        }
      }
      xtorLib->putVersion(major, minor);

      // Walk all the children and read the timing table if it exists
      for (xmlNode *node = rootNode->children; node != NULL; node = node->next) {
        if (isElement(node, "timing_table")) {
          parseTimingTable(node);
        }
      }

      // walk all children and parse them - the timing table has been read
      for (xmlNode *node = rootNode->children; node != NULL; node = node->next) {
        if (isElement(node, "component")) {
          parseComponent(node, xtorLib);
        }
        // TODO: else error (or validate via DTD/XSD)
      }
    }
    // free the document
    xmlFreeDoc(doc);
  }
  
  // and cleanup the XML library
  parseCleanup();

  return !getError();
}

bool CfgXmlParserXtor::parseParameters(const char* documentXML, CarbonCfg *cfg)
{
  parseSetup();

  xmlBuffer *buf = xmlBufferCreate();
  xmlBufferWriteChar(buf, documentXML);

  xmlDoc *doc = xmlParseDoc(xmlBufferContent(buf));

  if (doc == 0)
    reportError("Failed to parse component XML");
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "ParamDef")) {
      reportError("Root element of register XML must be 'paramDef'");
    } else {
      // The only thing allowed at the top level is a single component
      // walk all children (supposed of type "component" only) and parse them
      for (xmlNode *node = rootNode->children; node != NULL; node = node->next) {
        if (isElement(node, "parameter")) {
          CarbonCfgXtorParam* param = parseParameter(node);
          cfg->addParam(new CarbonCfgXtorParamInst(NULL, param));
        }
      }
    }
  }

  xmlFreeDoc(doc);
  xmlBufferFree(buf);
  parseCleanup();

  return !getError();

}

///////////////////////////////////////////////////////////////////////////////
// Parses the xmlparameters section of the .ccfg file
bool CfgXmlParserXtor::parseStream(UtIStream &s, CarbonCfg *cfg)
{
  parseSetup();

  // Fill a buffer with the XML text, then create a
  // document from it.
  xmlBuffer *buf = xmlBufferCreate();
  bool done = false;
  UtString line;
  while (!done) {
    s.getline(&line);
    xmlBufferWriteChar(buf, line.c_str());
    // check for end of XML or EOF
    if (line.empty() || (line.find("</paramdef>") != UtString::npos))
      done = true;
  }

  xmlDoc *doc = xmlParseDoc(xmlBufferContent(buf));

  if (doc == 0)
    reportError("Failed to parse register XML");
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "paramdef"))
      reportError("Root element of register XML must be 'paramdef'");
    else
      // walk all children and parse them
      for (xmlNode *node = rootNode->children; node != NULL; node = node->next)
        if (isElement(node, "parameter")) {
          CarbonCfgXtorParam* param = parseParameter(node);
          cfg->addParam(new CarbonCfgXtorParamInst(NULL, param));
        }
  }

  xmlFreeDoc(doc);
  xmlBufferFree(buf);
  parseCleanup();

  return !getError();
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseComponent(xmlNode *node, CarbonCfgXtorLib *xtorLib)
{
  // get component name
  CfgXmlAttr nameAttr(node, "name");
  bool ignoreXtor = false;
  if (nameAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<component> element with no name attribute is not allowed.");
  }
  else if(xtorLib->findXtor(nameAttr.value())) {
    // This can be caused by re-reading a user transactor definition file, so don't complain, just don't add it.
    ignoreXtor = true;
  }

  // add new component to the configuration (unless it's a duplicate
  CarbonCfgXtor *xtor = new CarbonCfgXtor(nameAttr.value(), xtorLib->libName());
  if (!ignoreXtor) xtorLib->addXtor(xtor);
  
  // check for write_debug attribute
  xtor->putHasWriteDebug(hasAttr(node, "write_debug"));

  // check for xtor_clock_master attribute
  xtor->putIsXtorClockMaster(hasAttr(node, "xtor_clock_master"));

  // check for xtor_clock_master attribute
  xtor->putIsFlowThru(hasAttr(node, "flow_thru"));

  // check for save_restore attribute
  xtor->putIsSaveRestore(hasAttr(node, "save_restore"));

  // check for xtor_clock_master attribute
  xtor->putIsMaster(hasAttr(node, "master"));

  // check for xtor_clock_slave attribute
  xtor->putIsSlave(hasAttr(node, "slave"));

  // Check for amba transactor
  xtor->putIsAmba(hasAttr(node, "amba_xactor"));

  // check for debug_transaction attribute
  xtor->putHasDebugTransaction(hasAttr(node, "debug_transaction"));

  // check for official SD xactor attribute
  xtor->putIsSDXactor(hasAttr(node, "sd_xactor"));

  // check for event_queue attribute
  xtor->putUseEventQueue(hasAttr(node, "use_event_queue"));

  // check for preserve_port_name_case attribute
  xtor->putPreservePortNameCase(hasAttr(node, "preserve_port_name_case"));

  // Should communicate be called for the transactor
  xtor->putUseCommunicate(hasAttr(node, "use_communicate"));
  
  // Should use an ESL port as the clock master
  xtor->putUseESLClockMaster(hasAttr(node, "use_esl_clock_master"));
  
  // Should use an ESL port as the reset master
  xtor->putUseESLResetMaster(hasAttr(node, "use_esl_reset_master"));
  
  // look for ports, synonyms, description, etc.
  for (xmlNode *childNode = node->children; childNode != NULL; childNode = childNode->next) {
    if (isElement(childNode, "parameter")) {
      CarbonCfgXtorParam* p = parseParameter(childNode);
      xtor->addParam(p);
    }
    else if (isElement(childNode, "port")) {
      parsePort(childNode, xtor);
    }
    else if (isElement(childNode, "systemCPort")) {
      parseSystemCPort(childNode, xtor);
    }
    else if (isElement(childNode, "synonym")) {
      parseSynonym(childNode, xtorLib, xtor);
    }
    else if (isElement(childNode, "className")) {
      parseClassName(childNode, xtor);
    }
    else if (isElement(childNode, "includeFile")) {
      parseIncludeFile(childNode, xtor);
    }
    else if (isElement(childNode, "libFile")) {
      parseLibFile(childNode, xtor);
    }
    else if (isElement(childNode, "incPath")) {
      parseIncPath(childNode, xtor);
    }
    else if (isElement(childNode, "libPath")) {
      parseLibPath(childNode, xtor);
    }
    else if (isElement(childNode, "protocolName")) {
      parseProtocolName(childNode, xtor);
    }
    else if (isElement(childNode, "constructorArgs")) {
      parseConstructorArgs(childNode, xtor);
    }
    else if (isElement(childNode, "templateArg")) {
      parseTemplateArg(childNode, xtor);
    }
    else if (isElement(childNode, "abstraction")) {
      parseAbstraction(childNode, xtor);
    }
    else if (isElement(childNode, "flag")) {
      parseFlag(childNode, xtor);
    }
  }
  
  // If the transactor wasn't added, delete it right away
  if(ignoreXtor) delete xtor;
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parsePort(xmlNode *node, CarbonCfgXtor *xtor)
{
  // TODO: error check attributes, else use DTD/XSD

  // read name
  CfgXmlAttr nameAttr(node, "name");

  // read width
  UtString widthStr;
  int width = 0;
  if (getAttribute(node, "width", &widthStr)) {
    width = atoi(widthStr.c_str());
  }

  // read direction
  CarbonCfgRTLPortTypeIO dir;
  CfgXmlAttr dirAttr(node, "direction");
  dir.parseString(dirAttr.value());

  // description
  UtString desc;
  parseDescription(node, &desc);
  
  // create the port
  CarbonCfgXtorPort *port = new CarbonCfgXtorPort(width, dir, nameAttr.value(), desc.c_str());

  // Is this port optional? (don't warn for unconnected optional ports.)
  bool optional;
  if (getAttribute(node, "optional", &optional)) {
    port->putOptional(optional);
  }

  // clock sensitive event
  port->putIsClockSensitive(hasAttr(node, "is_clock_sensitive"));

  // event functor instance name
  if (hasAttr(node, "event_name"))
  {
     CfgXmlAttr eventNameAttr(node, "event_name");
     port->putEventName(eventNameAttr.value());
  }

  // If there is timing information for this port, add it now. These
  // can come from one of two places. Either a timing table at the top
  // (deprecated?) or in the phases attribute.
  //
  // Start with the timing table data
  PinPhases::iterator pos = mPinPhases.find(nameAttr.value());
  if (pos != mPinPhases.end()) {
    const UtStringArray& phases = pos->second;
    for (UtStringArray::UnsortedCLoop l = phases.loopCUnsorted(); !l.atEnd(); ++l) {
      const char* phaseName = *l;
      port->addPhaseName(phaseName);
    }
  }

  // Look for the phases attribute
  if (hasAttr(node, "phases")) {
    CfgXmlAttr phasesAttr(node, "phases");
    UtString phases(phasesAttr.value());
    for (UtShellTok tok(phases.c_str(), false, " "); !tok.atEnd(); ++tok) {
      const char* phaseName = *tok;
      port->addPhaseName(phaseName);
    }
  }

  // Look for optional width expression
  if (hasAttr(node, "widthExpr")) {
    CfgXmlAttr widthExprAttr(node, "widthExpr");
    port->putWidthExpr(widthExprAttr.value());
  }

  // add port to transactor
  xtor->addPort(port);
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseSystemCPort(xmlNode *node, CarbonCfgXtor *xtor)
{
  // read name
  UtString name;
  getAttribute(node, "name", &name);
  
  // read type
  UtString typeString;
  CarbonCfgSystemCPortType type;
  if (!getAttribute(node, "type", &typeString)) {
    reportError("<systemCPort> element with no type attribute is not allowed.");
    return;
  }
  else {
    if (typeString == "sc_port")
      type = eCarbonCfgScPort;
    else if (typeString == "sc_export")
      type = eCarbonCfgScExport;
    else if (typeString == "tlm2_socket")
      type = eCarbonCfgTlm2Socket;
    else {
      UtString err;
      err << "Unknown type " << typeString << " in element <systemCPort>";
      reportError(err.c_str());
      return;
    }
  }

  // read interface
  UtString interface;
  if (!getAttribute(node, "interface", &interface)) {
    reportError("<systemCPort> element with no interface attribute is not allowed.");
    return;
  }
  CfgXmlAttr nameAttr(node, "name");

  // description
  UtString desc;
  parseDescription(node, &desc);

  // add port to transactor
  CarbonCfgXtorSystemCPort *port = new CarbonCfgXtorSystemCPort(name.c_str(), type, interface.c_str(), desc.c_str());  
  xtor->addSystemCPort(port);

}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseSynonym(xmlNode *node, CarbonCfgXtorLib *xtorLib, CarbonCfgXtor *xtor)
{
  xmlChar *synonym = xmlNodeGetContent(node);
  if (synonym != NULL) {
    // TODO: error message if invalid name
    xtorLib->addSynonym(xtor, (const char *) synonym);
    xmlFree(synonym);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseClassName(xmlNode *node, CarbonCfgXtor *xtor)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtor->putClassName((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseIncludeFile(xmlNode *node, CarbonCfgXtor *xtor)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtor->addIncludeFile((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseProtocolName(xmlNode *node, CarbonCfgXtor *xtor)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtor->putProtocolName((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseLibFile(xmlNode *node, CarbonCfgXtor *xtor)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtor->addLibFile((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseIncPath(xmlNode *node, CarbonCfgXtor *xtor)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtor->addIncPath((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseLibPath(xmlNode *node, CarbonCfgXtor *xtor)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtor->addLibPath((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstractionClassName(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtorAbstraction->putClassName((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstractionIncludeFile(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtorAbstraction->addIncludeFile((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstractionLibFile(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtorAbstraction->addLibFile((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstractionFlag(xmlNode* node, CarbonCfgXtorAbstraction* xtorAbstraction)
{
  xmlChar* flag = xmlNodeGetContent(node);
  if (flag != NULL) {
    xtorAbstraction->addFlag((const char*)flag);
  }
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstractionIncPath(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtorAbstraction->addIncPath((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstractionLibPath(xmlNode *node, CarbonCfgXtorAbstraction *xtorAbstraction)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtorAbstraction->addLibPath((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstractionLayerId(xmlNode* node, CarbonCfgXtorAbstraction* xtorAbstraction)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtorAbstraction->putLayerId((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseConstructorArgs(xmlNode *node, CarbonCfgXtor *xtor)
{
  xmlChar *name = xmlNodeGetContent(node);
  if (name != NULL) {
    // TODO: error message if invalid name
    xtor->putConstructorArgs((const char *) name);
    xmlFree(name);
  }
  // TODO: error message for NULL name (or use DTD/XSD)
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseTemplateArg(xmlNode* node, CarbonCfgXtor* xtor)
{
  // get the type and value attributes
  UtString type, value, defaultValue, flags;
  if (!getAttribute(node, "type", &type)) {
    reportError("Xtor Template Arg without a type attribute is not allowed.");
    return;
  }
  if (!getAttribute(node, "value", &value)) {
    reportError("Xtor Template Arg without a value attribute is not allowed.");
    return;
  }
  getAttribute(node, "default", &defaultValue);
  getAttribute(node, "flags", &flags);

  // Parse the type and flags to create the right template argument type
  CarbonCfgXtorTemplateArg* arg = NULL;
  if (type == "class") {
    // Class has no flags
    arg = new CarbonCfgXtorClassTemplateArg(value.c_str(), defaultValue.c_str());
    if (flags[0] != '\0') {
      UtString msg;
      msg << "class template argument does not support flags, '" << flags << "'.";
      reportError(msg.c_str());
    }

  } else if (type == "port-width") {
    // Parse the flags
    bool busDataWidth = false;
    for (UtShellTok tok(flags.c_str(), false, " "); !tok.atEnd(); ++tok) {
      const char* flag = *tok;
      if (strcmp(flag, "busDataWidth") == 0) {
        busDataWidth = true;
      } else {
        UtString msg;
        msg << "Unknown port-width flag '" << flag << "'.";
        reportError(msg.c_str());
      }
    }

    // Create the template arg
    arg = new CarbonCfgXtorPortWidthTemplateArg(value.c_str(), defaultValue.c_str(), busDataWidth);

  } else if (type == "text") {
    // Class has no flags
    arg = new CarbonCfgXtorTextTemplateArg(value.c_str(), defaultValue.c_str());
    if (flags[0] != '\0') {
      UtString msg;
      msg << "text template argument does not support flags, '" << flags << "'.";
      reportError(msg.c_str());
    }

  } else {
    UtString msg;
    msg << "Unknown template argument type '" << type << "'.";
    reportError(msg.c_str());
  }
    
  // Add it to the xtor
  xtor->addTemplateArg(arg);
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseFlag(xmlNode* node, CarbonCfgXtor* xtor)
{
  xmlChar* flag = xmlNodeGetContent(node);
  if (flag != NULL) {
    xtor->addFlag((const char*)flag);
  }
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseAbstraction(xmlNode* node, CarbonCfgXtor* xtor)
{
  // Read the abstraction name and add the abstraction variant
  CfgXmlAttr nameAttr(node, "name");
  CarbonCfgXtorAbstraction* xtorAbstraction = new CarbonCfgXtorAbstraction(nameAttr.value());
  xtor->addAbstraction(xtorAbstraction);

  // Parse any child fields and add them
  for (xmlNode *childNode = node->children; childNode != NULL; childNode = childNode->next) {
    if (isElement(childNode, "className")) {
      parseAbstractionClassName(childNode, xtorAbstraction);
    }
    else if (isElement(childNode, "includeFile")) {
      parseAbstractionIncludeFile(childNode, xtorAbstraction);
    }
    else if (isElement(childNode, "libFile")) {
      parseAbstractionLibFile(childNode, xtorAbstraction);
    }
    else if (isElement(childNode, "incPath")) {
      parseAbstractionIncPath(childNode, xtorAbstraction);
    }
    else if (isElement(childNode, "libPath")) {
      parseAbstractionLibPath(childNode, xtorAbstraction);
    }
    else if (isElement(childNode, "layerId")) {
      parseAbstractionLayerId(childNode, xtorAbstraction);
    }
    else if (isElement(childNode, "flag")) {
      parseAbstractionFlag(childNode, xtorAbstraction);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
CarbonCfgXtorParam* CfgXmlParserXtor::parseParameter(xmlNode *node)
{
  // read name
  CfgXmlAttr nameAttr(node, "name");

  // read type
  CarbonCfgParamDataTypeIO paramType;
  CfgXmlAttr typeAttr(node, "type");
  paramType.parseString(typeAttr.value());

  // at present, there are only integer parameters, and for some reason the
  // value is the 'size' of the parameter object
  CfgXmlAttr sizeAttr(node, "value");
  int size = atoi(sizeAttr.value());

  // read parameter scope
  CarbonCfgParamFlagIO paramScope;
  CfgXmlAttr scopeAttr(node, "scope");
  paramScope.parseString(scopeAttr.value());

  // Optional min and max
  UtString min, max;
  if (hasAttr(node, "min")) {
    CfgXmlAttr minAttr(node, "min");
    min = minAttr.value();
  }
  if (hasAttr(node, "max")) {
    CfgXmlAttr maxAttr(node, "max");
    max = maxAttr.value();
  }

  // description
  UtString desc;
  parseDescription(node, &desc);

  // description
  UtString enumChoices;
  parseEnumChoices(node, &enumChoices);

  // return the newly constructed parameter
  return new CarbonCfgXtorParam(size, paramType, paramScope, nameAttr.value(), desc.c_str(), sizeAttr.value(), 
                                enumChoices.c_str(), min.c_str(), max.c_str());
}

///////////////////////////////////////////////////////////////////////////////
void CfgXmlParserXtor::parseDescription(xmlNode *node, UtString *desc)
{
  *desc = "";
  for (xmlNode *childNode = node->children; childNode != NULL; childNode = childNode->next) {
    if (isElement(childNode, "description")) {
      xmlChar *text = xmlNodeGetContent(childNode);
      if (text != NULL) {
        *desc = (const char *) text;
        xmlFree(text);
      }
      break;
    }
  }  
}

void CfgXmlParserXtor::parseEnumChoices(xmlNode *node, UtString *enumChoices)
{
  *enumChoices = "";
  for (xmlNode *childNode = node->children; childNode != NULL; childNode = childNode->next) {
    if (isElement(childNode, "enumChoices")) {
      xmlChar *text = xmlNodeGetContent(childNode);
      if (text != NULL) {
        *enumChoices = (const char *) text;
        xmlFree(text);
      }
      break;
    }
  }  
}

void CfgXmlParserXtor::parseTimingTable(xmlNode* node)
{
  // Look for phase name elements
  for (xmlNode* childNode = node->children; childNode != NULL; childNode = childNode->next) {
    if (isElement(childNode, "phase")) {
      // It must have a name attribute
      UtString name;
      if (!getAttribute(childNode, "name", &name)) {
        reportError("timing table phase with no name is not allowed.");
        return;
      }

      // Walk the port names
      parsePhasePorts(childNode, name);
    }
  }
}

void CfgXmlParserXtor::parsePhasePorts(xmlNode* node, const UtString& phaseName)
{
  // Look for port name elements
  for (xmlNode* childNode = node->children; childNode != NULL; childNode = childNode->next) {
    if (isElement(childNode, "port")) {
      // It must have a name attribute
      UtString name;
      if (!getAttribute(childNode, "name", &name)) {
        reportError("timing table port with no name is not allowed.");
        return;
      }

      // Create the association
      mPinPhases[name].push_back(phaseName.c_str());
    }
  }
}

// CoWare XML Parser

CfgCowareXmlParserXtor::CfgCowareXmlParserXtor()
{
}

CfgCowareXmlParserXtor::~CfgCowareXmlParserXtor()
{
}

void CfgCowareXmlParserXtor::parsePin(xmlNode* parent, CarbonCfgXtor* xtor)
{
  CarbonCfgXtorPort* port = NULL;

  for (xmlNode *node = parent->children; node != NULL; node = node->next)
  {
    if (port != NULL && isElement(node, "orientation"))
    {
      UtString orient;
      getContent(node, &orient);
      if (xtor->isMaster()) {
        if (orient == "master2slave") {
          port->putType(eCarbonCfgRTLInput);
        } else if (orient == "slave2master") {
          port->putType(eCarbonCfgRTLOutput);
        }
      } else if (xtor->isSlave()) {
        if (orient == "master2slave") {
          port->putType(eCarbonCfgRTLOutput);
        } else if (orient == "slave2master") {
          port->putType(eCarbonCfgRTLInput);
        }
      } else {
        // Old style protocol in ccfg file, just do what we did before
        if (orient == "master2slave") {
          port->putType(eCarbonCfgRTLInput);
        } else if (orient == "slave2master") {
          port->putType(eCarbonCfgRTLOutput);
        }
      }
    }

    if (isElement(node, "name"))
    {
     // description
      
      UtString name;
      getContent(node, &name);
      
      // create the port
      port = new CarbonCfgXtorPort(0, eCarbonCfgRTLInput, name.c_str(), name.c_str());

      // add port to transactor
      xtor->addPort(port);
    }

    if ((port != NULL) && isElement(node, "datatype")) {
      parseDataType(node, port);
    }
  }
}

void CfgCowareXmlParserXtor::parseDataType(xmlNode* parent, CarbonCfgXtorPort* port)
{
  // Look for the value child, hopefully there is only one
  for (xmlNode* node = parent->children; node != NULL; node = node->next) {
    // 2009 has a value sub element
    if (isElement(node, "value")) {
      // Get the value and remove any sizes
      UtString typeDef;
      getContent(node, &typeDef);
      size_t loc = typeDef.find('<');
      if (loc != UtString::npos) {
        typeDef = typeDef.substr(0, loc);
      }
      port->putTypeDef(typeDef.c_str());
    }

    // 2006 has a <direct> sub element which has a <class> sub element
    if (isElement(node, "direct")) {
      parseDirect(node, port);
    }
  }
}

void CfgCowareXmlParserXtor::parseDirect(xmlNode* parent, CarbonCfgXtorPort* port)
{
  // Look for the value child, hopefully there is only one
  for (xmlNode* node = parent->children; node != NULL; node = node->next) {
    // 2006 has a class sub element
    if (isElement(node, "class")) {
      UtString typeDef;
      getContent(node, &typeDef);
      port->putTypeDef(typeDef.c_str());
    }
  }
}

void CfgCowareXmlParserXtor::parsePins(xmlNode* parent, CarbonCfgXtor* xtor)
{
  for (xmlNode *node = parent->children; node != NULL; node = node->next)
  {
    if (isElement(node, "pin"))
      parsePin(node, xtor);
  }

}
void CfgCowareXmlParserXtor::parseBca(xmlNode* parent, CarbonCfgXtor* xtor, const char* /*library*/)
{
  for (xmlNode *node = parent->children; node != NULL; node = node->next)
  {
    if (isElement(node, "pins"))
      parsePins(node, xtor);
  }
}

void CfgCowareXmlParserXtor::parseProtocol(xmlNode* parent, const char* library, CarbonCfg* cfg,
                                           UInt32 major, UInt32 minor)
{
  // Check if this is a reference first. If it has a library_reference
  // tag, we should ignore it.  It is just a reference to the real
  // protocol.
  for (xmlNode *node = parent->children; node != NULL; node = node->next) {
    if (isElement(node, "library_reference")) {
      return;
    }
  }

  // Look for the name and BCA implementation (Bus Cycle Accurate)
  UtString name;
  for (xmlNode *node = parent->children; node != NULL; node = node->next)
  {
    if (isElement(node, "name"))
    {
      getContent(node, &name);
    }

    if (isElement(node,"bca_implementation"))
    {
      // Only create the transactor and library if there is a pin
      // implementation. So we create it now it it doesn't already
      // exist.
      CarbonCfgXtorLib* xtorLib = cfg->findXtorLib(library);
      if (xtorLib == NULL) {
        xtorLib = new CarbonCfgXtorLib(library);
        if (!cfg->addXtorLib(xtorLib)) {
          delete xtorLib;
          UtString err;
          err << "Duplicate library '" << library << "'; ignored.";
          reportError(err.c_str());
          return;
        }
        xtorLib->putVersion(major, minor);
      }

      // Now we can create the transactors (protocols really) - Master and slave
      addXtor(node, name.c_str(), library, "Master", xtorLib, eMaster);
      addXtor(node, name.c_str(), library, "Slave", xtorLib, eSlave);

      // Backwards compatability, add a transactor without a variant
      if (!cfg->hasXtorFullName()) {
        addXtor(node, name.c_str(), library, "", xtorLib, eUnknown);
      }
    }
  } // for
} // void CfgCowareXmlParserXtor::parseProtocol

CarbonCfgXtor*
CfgCowareXmlParserXtor::addXtor(xmlNode* node, const char* name, const char* library, const char* variant,
                                CarbonCfgXtorLib* xtorLib, XtorType xtorType)
{
  CarbonCfgXtor* xtor = new CarbonCfgXtor(name, library, variant);
  switch(xtorType) {
    case eMaster:
      xtor->putIsMaster(true);
      break;

    case eSlave:
      xtor->putIsSlave(true);
      break;

    case eUnknown:
      break;
  }
  xtorLib->addXtor(xtor);
  parseBca(node, xtor, library);
  return xtor;
}

void
CfgCowareXmlParserXtor::parseProtocols(xmlNode* parent, const char* library, CarbonCfg* cfg,
                                       UInt32 major, UInt32 minor)
{
  for (xmlNode *node = parent->children; node != NULL; node = node->next)
  {
    if (isElement(node, "protocol"))
      parseProtocol(node, library, cfg, major, minor);
  }
}

void CfgCowareXmlParserXtor::parseLibrary(xmlNode* parent, CarbonCfg* cfg, UInt32 major, UInt32 minor)
{
  // Find the name
  UtString libname;
  for (xmlNode *node = parent->children; node != NULL; node = node->next) {
    if (isElement(node, "name")) {
      getContent(node,&libname);
    }
  }

  // Read in the xtors
  for (xmlNode *node = parent->children; node != NULL; node = node->next) {
    if (isElement(node, "protocols")) {
      parseProtocols(node, libname.c_str(), cfg, major, minor);
    }
  }
}

void CfgCowareXmlParserXtor::parseData(xmlNode* parent, CarbonCfg* cfg)
{
  // Get the version first
  UInt32 major = 1;
  UInt32 minor = 0;
  for (xmlNode *node = parent->children; node != NULL; node = node->next) {
    if (isElement(node, "version")) {
      UtString versionStr;
      getContent(node, &versionStr);
      sscanf(versionStr.c_str(), "%d.%d", &major, &minor);
    }
  }

  for (xmlNode *node = parent->children; node != NULL; node = node->next)
  {
    if (isElement(node, "library"))
    {
      parseLibrary(node, cfg, major, minor);
    }
  }
}

bool CfgCowareXmlParserXtor::parseFile(const char* fileName, CarbonCfg* cfg)
{
  parseSetup();

  // parse the transactor defs
  xmlDocPtr doc = xmlReadFile(fileName, NULL, 0);

  if (doc == NULL)
  {
    UtString message;
    message << "Failed to parse file " <<  fileName;
    reportError(message.c_str());
  }
  else
  {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (isElement(rootNode, "pct_data"))
      parseData(rootNode, cfg);
  }
  // and cleanup the XML library
  parseCleanup();

  return !getError();
}

