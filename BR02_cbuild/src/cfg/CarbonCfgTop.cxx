// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Carbon configuration file data model for the top component
*/

#include "cfg/CarbonCfg.h"

CarbonCfgTop::CarbonCfgTop() :
  CarbonCfg(), mDB(NULL)
{}

CarbonCfgTop::CarbonCfgTop(Mode mode) :
  CarbonCfg(mode), mDB(NULL)
{}

CarbonCfgTop::~CarbonCfgTop()
{
  if (mDB != NULL) {
    carbonDBFree(mDB);
  }
}

// This is when we need to scan the DB for special "Embedded Models"
// The Modelstudio GUI is associating a DB with the CCFG at this point.
//
void CarbonCfgTop::putDB(CarbonDB* db)
{
  mDB = db;
}

CarbonDB* CarbonCfgTop::getDB() const
{
  return mDB;
}


