//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2013 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "CfgXmlParserComp.h"
#include "cfg/CarbonCfg.h"
#include "CfgXmlParserReg.h"
#include "CfgXmlParserMem.h"

#include "cfg/CarbonCfg.h"

#include "util/UtIStream.h"

CfgXmlParserComp::CfgXmlParserComp()
{
}


bool CfgXmlParserComp::parseComponent(const char* documentXML, CarbonCfg *cfg)
{
  parseSetup();

  xmlBuffer *buf = xmlBufferCreate();
  xmlBufferWriteChar(buf, documentXML);

  xmlDoc *doc = xmlParseDoc(xmlBufferContent(buf));

  if (doc == 0)
    reportError("Failed to parse component XML");
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "componentinfo")) {
      reportError("Root element of register XML must be 'component'");
    } else {
      // The only thing allowed at the top level is a single component
      // walk all children (supposed of type "component" only) and parse them
      for (xmlNode *node = rootNode->children; node != NULL; node = node->next) {
        if (isElement(node, "component")) {
          parseComp(node, cfg, true /* top component */, xmlBufferContent(buf));
        }
      }
    }
  }

  xmlFreeDoc(doc);
  xmlBufferFree(buf);
  parseCleanup();

  return !getError();

}

bool CfgXmlParserComp::parseStream(UtIStream &s, CarbonCfg *cfg)
{
  parseSetup();

  // Fill a buffer with the XML text, then create a document from it.
  xmlBuffer *buf = xmlBufferCreate();
  bool done = false;
  UtString line;
  while (!done) {
    s.getline(&line);
    xmlBufferWriteChar(buf, line.c_str());
    // check for end of XML or EOF
    if (line.empty() || (line.find("</componentinfo>") != UtString::npos))
      done = true;
  }

  xmlDoc *doc = xmlParseDoc(xmlBufferContent(buf));

  if (doc == 0)
    reportError("Failed to parse component XML");
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "componentinfo")) {
      reportError("Root element of register XML must be 'component'");
    } else {
      // The only thing allowed at the top level is a single component
      // walk all children (supposed of type "component" only) and parse them
      for (xmlNode *node = rootNode->children; node != NULL; node = node->next) {
        if (isElement(node, "component")) {
          parseComp(node, cfg, true /* top component */, xmlBufferContent(buf));
        }
      }
    }
  }

  xmlFreeDoc(doc);
  xmlBufferFree(buf);
  parseCleanup();

  return !getError();
}

void CfgXmlParserComp::parseSubComp(xmlNode *node, CarbonCfg *cfg, const xmlChar* contents)
{
  // The sub components tag has a number of components
  // underneath. These are the subc omponents of the top level. Note
  // that today we only support two levels of hierarchy. As far as we
  // know, no ARM IP needs more than that.
  for (xmlNode* child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "component")) {
      parseComp(child, cfg, false /* top component */, contents);
    }
    else if (isElement(child, "componentJournal"))
    {
      cfg->putComponentJournalXML(reinterpret_cast<const char *>(contents));
    }
  }
}

void CfgXmlParserComp::parseComp(xmlNode *node, CarbonCfg *cfg, bool top, const xmlChar* contents)
{
  // get component name
  CfgXmlAttr nameAttr(node, "name");
  if (nameAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<component> element with no name attribute is not allowed.");
  }

  // If this is the top, add the info to the to cfg, otherwise, create
  // a new one
  CarbonCfg* thisCfg;
  if (top) {
    thisCfg = cfg;
  } else {
    thisCfg = cfg->addSubComp(nameAttr.value());
  }

  // Parse the component sub elements
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "customcode")) {
      parseCustomCode(child, thisCfg);

    } else if (isElement(child, "reginfo")) {
      // we reuse the code from CfgXmlParserReg class
      CfgXmlParserReg parser;
      parser.parseRegInfo(child, thisCfg);
    }
    else if (isElement(child, "meminfo")) {
      // we reuse the code from CfgXmlParserMem class
      CfgXmlParserMem parser;
      parser.parseMemInfo(child, thisCfg);
      const char * errorText = parser.errorText();
      if ( strlen(errorText) != 0 ) {
        reportError(errorText);
      }
    } else if (isElement(child, "displayname")) {
      UtString name;
      getContent(child, &name);
      thisCfg->putCompDisplayName(name.c_str());
    } else if (isElement(child, "loadfileext")) {
      UtString ext;
      getContent(child, &ext);
      thisCfg->putLoadfileExtension(ext.c_str());
    } else if (isElement(child, "type")) {
      UtString name;
      getContent(child, &name);
      thisCfg->putCompType(name.c_str());
    } else if (isElement(child, "version")) {
      UtString name;
      getContent(child, &name);
      thisCfg->putCompVersion(name.c_str());
    } else if (isElement(child, "docfile")) {
      UtString name;
      getContent(child, &name);
      thisCfg->putCompDocFile(name.c_str());
    } else if (top && isElement(child, "subcomponents")) {
      // We allow one level of sub components
      parseSubComp(child, thisCfg, contents);
    } else if (isElement(child, "cadi")) {
      parseCadi(child, thisCfg);
    } else if (isElement(child, "requires-mk-library")) {
      thisCfg->putRequiresMkLibrary(true);
    } else if (isElement(child, "use-versioned-mk-library")) {
      thisCfg->putUseVersionedMkLibrary(true);
    } else if (isElement(child, "standalone")) {
      thisCfg->putIsStandAloneComp(true);
    } else if (isElement(child, "sub-component-full-name")) {
      thisCfg->putSubComponentFullName(true);
    } else if (isElement(child, "tag")) {
      UtString name;
      getContent(child, &name);
      thisCfg->SetComponentTag(name.c_str());
    } else if (isElement(child, "pctraceaccessor")) {
      UtString name;
      getContent(child, &name);
      thisCfg->putPCTraceAccessor(name.c_str());
    }
  }
}

void CfgXmlParserComp::parseCustomCode(xmlNode *node, CarbonCfg *cfg)
{
  // Parse the custom code
  CfgXmlParserCustomCode parseCustom(node, cfg->getLegalSections());
  bool success = parseCustom.parse(cfg);
  if (!success) {
    reportError(parseCustom.errorText());
    return;
  }
}

void CfgXmlParserComp::parseCadi(xmlNode* node, CarbonCfg* cfg)
{
  // Currently the only sub elements are custom code sections and disassembler name
  for (xmlNode* child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "customcode")) {
      parseCadiCustomCode(child, cfg);
    }
    
    if (isElement(child, "disassembler")) {
      UtString className;
      if (getAttribute(child, "class", &className)) {
        CarbonCfgCadi* cadi = cfg->getCadi();
        cadi->putDisassemblerName(className.c_str());
      }
      else reportError("<disassembler> element with no class attribute is not allowed.");
    }
  }
}

void CfgXmlParserComp::parseCadiCustomCode(xmlNode* node, CarbonCfg* cfg)
{
 // Parse the custom code
  CarbonCfgCadi* cadi = cfg->getCadi();
  CfgXmlParserCustomCode parseCustom(node, cadi->getLegalSections());
  bool success = parseCustom.parse(cadi);
  if (!success) {
    reportError(parseCustom.errorText());
    return;
  }
}
