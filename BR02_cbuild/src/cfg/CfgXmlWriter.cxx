//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CfgXmlWriter.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"

CfgXmlWriter::CfgXmlWriter(const char *root)
{
  mDoc = xmlNewDoc(reinterpret_cast<const xmlChar*>("1.0"));
  xmlNodePtr p = xmlNewNode(0, reinterpret_cast<const xmlChar*>(root));
  xmlDocSetRootElement(mDoc, p);
  mNodeStack.push(p);
}

CfgXmlWriter::~CfgXmlWriter()
{
  xmlFreeDoc(mDoc);
  xmlCleanupParser();
}

void CfgXmlWriter::write(const char *filename)
{
  xmlSaveFormatFile(filename, mDoc, 1 /* indent */);
}

void CfgXmlWriter::write(UtOStream &s)
{
  xmlChar *buf;
  int size;

  xmlDocDumpFormatMemory(mDoc, &buf, &size, 1 /* indent */);
  //  s << reinterpret_cast<char *>(buf);
  s.write(reinterpret_cast<char *>(buf), size);
}


void CfgXmlWriter::addCompletedElement(const char *name, const UtString& value)
{
  addElement(name, value.c_str());
  closeElement();
}

void CfgXmlWriter::addCompletedElement(const char *name, const char *text)
{
  addElement(name, text == NULL ? "" : text);
  closeElement();
}

void CfgXmlWriter::addCompletedElement(const char *name, CarbonUInt64 value)
{
  UtString buf;
  buf << value;
  addElement(name, buf.c_str());
  closeElement();
}


void CfgXmlWriter::addElement(const char *name, const char *text)
{
  xmlNodePtr p = xmlNewChild(mNodeStack.top(), 0,
                             reinterpret_cast<const xmlChar*>(name),
                             reinterpret_cast<const xmlChar*>(text));

  mNodeStack.push(p);
}


void CfgXmlWriter::addElement(const char *name, CarbonUInt64 value)
{
  UtString buf;
  buf << value;
  addElement(name, buf.c_str());
}

void CfgXmlWriter::addCdata(const char* text)
{
  xmlNodePtr p = xmlNewCDataBlock(mDoc,
                                  reinterpret_cast<const xmlChar*>(text),
                                  strlen(text));
  xmlAddChild(mNodeStack.top(), p);
}

void CfgXmlWriter::addAttribute(const char *name, const char *value)
{
  xmlNewProp(mNodeStack.top(),
             reinterpret_cast<const xmlChar*>(name),
             reinterpret_cast<const xmlChar*>(value));
}

void CfgXmlWriter::addAttribute(const char *name, const UtString& value)
{
  addAttribute(name, value.c_str());
}

void CfgXmlWriter::addAttribute(const char *name, CarbonUInt64 value)
{
  UtString buf;
  buf << value;
  addAttribute(name, buf.c_str());
}

void CfgXmlWriter::closeElement()
{
  mNodeStack.pop();
}
