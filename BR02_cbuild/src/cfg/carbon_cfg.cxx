//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"
#include "CfgXmlWriter.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "util/UtParamFile.h"
#include "util/UtConv.h"
#include "util/RandomValGen.h"
#include "shell/carbon_misc.h"
#include "util/OSWrapper.h"
#include "util/UtShellTok.h"
#include <QString>
#include <QObject>
#include <QCoreApplication>
#include <QtXml>


#define SCRIPT_ASSERT(expr, msg) \
  if (engine() && expr == 0) { \
    context()->throwError(QScriptContext::UnknownError, msg); \
    return; \
  } else { \
    INFO_ASSERT(expr,msg); \
  }

#define SCRIPT_ASSERT_RETURN(expr, msg, retVal) \
  if (engine() && expr == 0) { \
    context()->throwError(QScriptContext::UnknownError, msg); \
    return retVal; \
  } else { \
    INFO_ASSERT(expr,msg); \
  }


UtOStream& operator<<(UtOStream& stream, const QString& str)
{
  QByteArray bytes = str.toAscii(); // this_toascii_ok
  stream << bytes.constData();
  return stream;
}

UtString& operator<<(UtString& str, const QString& qstr)
{
  QByteArray bytes = qstr.toAscii(); // this_toascii_ok
  str << bytes.constData();
  return str;
}

UtString& operator<<(UtString& str, const QStringRef& qstr)
{
  QByteArray bytes = qstr.toString().toAscii(); // this_toascii_ok
  str << bytes.constData();
  return str;
}

#define QUSTR(qstr) qstr.toAscii().constData() /* this_toascii_ok */

// #define CFG_VERSION 8   // adds adaptor exprs
// #define CFG_VERSION 9   // adds cxxflags and linkflags
// #define CFG_VERSION 10  // adds xtor instance parameters
// #define CFG_VERSION 11  // xtor clocking controlled by slave ports, not event queue
// #define CFG_VERSION 12  // esl port modes
// #define CFG_VERSION 13  // ccfg identifier (CoWare, ARM)
// #define CFG_VERSION 14  // ccfg disassembly view (mDisassemblyName, disassemblerName)
// #define CFG_VERSION 15  // adds arbitrary active and inactive values for reset generator
// #define CFG_VERSION 16  // xml memories
// #define CFG_VERSION 17  // adds additional colors for profiling
// #define CFG_VERSION 18  // instance parameters
// #define CFG_VERSION 19  // use xtor debugAccess method
// #define CFG_VERSION 20  // TLM2 support
//#define CFG_VERSION 21     // Embedded Script Models
#define CFG_VERSION 22    // First XML Release
const char* gCarbonCCFGModes[] = {"undefined", "ARM", "CoWare", "SystemC", NULL};
const char* gCarbonESLPortModeTypes[] = {"undefined", "control", "clock", "reset", NULL};
const char* gCarbonRTLPortTypes[] = {"input", "output", "inout", NULL};
const char* gCarbonSystemCPortTypes[] = {"sc_port", "sc_export", "tlm2_socket", NULL};
const char* gCarbonESLPortTypes[] = {"undefined", "input", "output", "inout", NULL};
const char* gCarbonXtorDataTypes[] = {"bool", "integer", "double",
                                      "string", "integer64", NULL};
const char* gCarbonXtorParamFlag[] = {"run-time", "compile-time",
                                      "run-time-and-compile-time",
                                      "init-time",
                                      NULL};
const char* gCarbonMaxsimRadix[] = {
  "BooleanActiveHigh",
  "BooleanActiveLow",
  "Hex",
  "SignedInt",
  "UnsignedInt",
  "Float",
  "String",
  NULL
};
const char* gCarbonRTLConnectionTypes[] = {
  "ClockGen",
  "ResetGen",
  "Tie",
  "Param",
  "ESLPort",
  "XtorConn",
  NULL
};
const char* gCarbonCfgColors[] = {
  "Red",
  "Blue",
  "Green",
  "Yellow",
  "Orange",
  "Brown",
  "Purple",
  "LightGray",
  "LigthBlue",
  "LigthCyan",
  "LightSeaGreen",
  "LightCoral",
  "LightYellow1",
  "LightSalmon1",
  "LightGreen",
  NULL
};
const char* gCarbonCfgReadmemTypes[] = {
  "readmemh",
  "readmemb",
  NULL
};

const char* gCarbonCfgMemInitTypes[] = {
  "NoInit",
  "ProgPreload",
  "ReadMemInit",
  NULL
};

const char* gCarbonCfgFeature[] = {
  "PortExpr",
  "MemInitEnum",
  "XtorConnExpr",
  "XtorClockMasters",
  "NoTicks",
  "AbstractRegs",
  "EslPortModes",
  "Disassembly",
  "ResetValues",
  "ESLPortParameter",
  "XtorDebugAccess",
  "XtorFullName",
  "XtorTLM2Support",
  "EmbeddedModels",
  NULL
};

const char* gCarbonCfgRegAccessTypes[] = {
  "Read-Write",
  "Read-Only",
  "Write-Only",
  NULL
};

const char* gCarbonCfgRegAccessShortTypes[] = {
  "rw",
  "ro",
  "wo",
  NULL
};

const char* gCarbonCfgRegLocTypes[] = {
  "Constant",
  "Register",
  "Array",
  NULL
};

const char* gCarbonCfgMemLocTypes[] = {
  "RTL",
  "Port",
  "User",
  NULL
};
const char* gCarbonCfgEndianess[] = {"Big", "Little", NULL};

// if you make a change here you must also change the definition of enum CarbonCfgCompCustomCodeSection (in src/inc/cfg/carbon_cfg.h)
// and the definition of enum CarbonCfgCompCustomCodeSection (in src/cmm/SrcCcfg.h)
const char* gCarbonCfgCompCustomCodeSection[] = {
  "cpp-include",
  "h-include",
  "class",
  "constructor",
  "destructor",
  "communicate",
  "interconnect",
  "update",
  "init",
  "reset",
  "terminate",
  "setParameter",
  "getParameter",
  "getProperty",
  "debugTransaction",
  "debugAccess",
  "procCanStop",
  "procStopAtDebuggablePoint",
  "saveData",
  "restoreData",
  "getCB",
  "saveStreamType",
  "saveDataArch",
  "restoreDataArch",
  "saveDataBinary",
  "restoreDataBinary",

  // These sections are for stand-alone components
  "loadFile",
  "stopAtDebuggablePoint",
  "canStop",
  "registerDebugAccessCB",
  "retrieveDebugAccessIF",
  "debugMemRead",
  "debugMemWrite"
};

// This must match enum in carbon_cfg.h
const char* gCarbonCfgCadiCustomCodeSection[] = {
  "cpp-include",
  "h-include",
  "class",
  "constructor",
  "destructor",
  "CADIRegGetGroups",
  "CADIRegGetMap",
  "CADIRegRead",
  "CADIRegWrite",
  "CADIGetPC",
  "CADIGetDisassembler",
  "CADIXfaceBypass",
  "CADIMemGetSpaces",
  "CADIMemGetBlocks",
  "CADIMemWrite",
  "CADIMemRead",
  "CADIGetCommitedPCs"
};

const char* gCarbonCfgRegCustomCodeSection[] = {
  "class",
  "constructor",
  "destructor",
  "read",
  "write",
  "reset"
};

const char* gCarbonCfgMemoryBlockCustomCodeSection[] = {
  "class",
  "constructor",
  "destructor",
  "memRead",
  "memWrite"
};

const char* gCarbonCfgMemoryCustomCodeSection[] = {
  "class",
  "constructor",
  "destructor",
  "memRead",
  "memWrite"
};

const char* gCarbonCfgSystemCTimeUnits[] = {
  "SC_FS",
  "SC_PS",
  "SC_NS",
  "SC_US",
  "SC_MS",
  "SC_SEC",
  NULL
};

const char* gCarbonCfgElementGenerationType[] = {
  "User",
  "Generated",
  NULL
};

#define NO_EXTERN_STRINGS

#include "cfg/CarbonCfg.h"
#include "CfgXmlParserXtor.h"
#include "CfgXmlParserReg.h"
#include "CfgXmlParserMem.h"
#include "CfgXmlParserComp.h"
#ifdef CARBON_PV
#include "CfgXmlParserPSD.h"
#endif

//! compute the greatest common divisor 
static UInt32 greatestCommonDivisor(UInt32 a, UInt32 b) {
  UInt32 c;
  while (b != 0) {
    c = b;
    b = a % b;
    a = c;
  }
  return a;
}


//! Helper class to annotate errors with a filename & line number
class CarbonCfgErrorHandler {
public:
  CarbonCfgErrorHandler(UtParamFile* f, CarbonCfgMsgHandler* msgHandler):
    mFile(f),
    mMsgHandler(msgHandler),
    mHasError(false)
  {}
  void reportError(const char* msg) {
    mMsgHandler->reportMessage(eCarbonMsgError, msg, mFile->getFilename(),
                               mFile->getLineNumber());
    mHasError = true;
  }
  bool hasError() { return mHasError; }
private:
  UtParamFile* mFile;
  CarbonCfgMsgHandler* mMsgHandler;
  bool mHasError;
};

void CarbonCfgClockFrequency::read(UtIStream &f)
{
  f >> mClockCycles >> mCompCycles;
}


void CarbonCfgClockFrequency::read(QXmlStreamReader &xr)
{
  while (xr.readNextStartElement())
  {
    if ("ClockCycles" == xr.name())
      mClockCycles = xr.readElementText().toUInt();
    else if ("CompCycles" == xr.name())
      mCompCycles = xr.readElementText().toUInt();
  }
}
void CarbonCfgClockFrequency::write(CfgXmlWriter &f)
{
  f.addElement("Frequency");
  f.addCompletedElement("ClockCycles", mClockCycles);
  f.addCompletedElement("CompCycles", mCompCycles);
  f.closeElement();
  //f << mClockCycles << mCompCycles;
}


// Predicates
bool CarbonCfgRTLPort::isESLPort() const
{
  for (UInt32 c=0; c<numConnections(); c++)
  {
    CarbonCfgRTLConnection* conn = getConnection(c);
    if (conn->castESLPort())
      return true;
  }
  return false;
}

bool CarbonCfgRTLPort::isTied() const
{
  for (UInt32 c=0; c<numConnections(); c++)
  {
    CarbonCfgRTLConnection* conn = getConnection(c);
    if (conn->castTie())
      return true;
  }
  return false;
}

bool CarbonCfgRTLPort::isResetGen() const
{
  for (UInt32 c=0; c<numConnections(); c++)
  {
    CarbonCfgRTLConnection* conn = getConnection(c);
    if (conn->castResetGen())
      return true;
  }
  return false;
}
bool CarbonCfgRTLPort::isClockGen() const
{
  for (UInt32 c=0; c<numConnections(); c++)
  {
    CarbonCfgRTLConnection* conn = getConnection(c);
    if (conn->castClockGen())
      return true;
  }
  return false;
}

bool CarbonCfgRTLPort::isTiedToParam() const
{
  for (UInt32 c=0; c<numConnections(); c++)
  {
    CarbonCfgRTLConnection* conn = getConnection(c);
    if (conn->castTieParam())
      return true;
  }
  return false;
}

bool CarbonCfgRTLPort::isConnectedToXtor() const
{
  for (UInt32 c=0; c<numConnections(); c++)
  {
    CarbonCfgRTLConnection* conn = getConnection(c);
    if (conn->castXtorConn())
      return true;
  }
  return false;
}

CarbonCfgRTLPort::CarbonCfgRTLPort(const char* name, UInt32 width,
                                   CarbonCfgRTLPortType type):
  mName(name),
  mWidth(width),
  mType(type)
{
}

CarbonCfgRTLPort::~CarbonCfgRTLPort() {
  clear();
}

void CarbonCfgRTLPort::clear() {
  for (UInt32 i = 0, n = mConnections.size(); i < n; ++i) {
    CarbonCfgRTLConnection* con = mConnections[i];
    if (con->castXtorConn() == NULL)
      delete con;
    else
      con->disconnect();
  }
  mConnections.clear();
}

CarbonCfgRTLPort::CarbonCfgRTLPort(UtIStream& line, CarbonCfg* cfg,
                                   UtParamFile& f,
                                   CarbonCfgErrorHandler* err,
                                   UInt32 ticksPerBeat)
{
  UInt32 numCons = 0;
  line >> mName >> mWidth >> mType >> numCons;

  UtIStringStream connLine;
  for (UInt32 i = 0; (i < numCons) && !f.bad() && !connLine.bad() &&
         f.getline(&connLine); ++i)
  {
    CarbonCfgRTLConnection* conn
      = CarbonCfgRTLConnection::read(this, connLine, cfg, err, ticksPerBeat);
    if (conn != NULL) {
      connect(conn);
    }
    else {
      delete conn;
    }
  }
}


CarbonCfgRTLPort::CarbonCfgRTLPort(QXmlStreamReader& xr, CarbonCfg* cfg,
                                   UtParamFile& /*f*/,
                                   CarbonCfgErrorHandler* err,
                                   UInt32 ticksPerBeat)
{
  mName << xr.attributes().value("name");
  mWidth = xr.attributes().value("width").toString().toUInt();

  CarbonCfgRTLPortTypeIO portType;
  UtString str; str << xr.attributes().value("type");
  portType.parseString(str.c_str());
  mType = portType.getEnum();

  //line >> mName >> mWidth >> mType >> numCons;

  while (xr.readNextStartElement())
  {
    CarbonCfgRTLConnection* conn = CarbonCfgRTLConnection::read(this, xr, cfg, err, ticksPerBeat);
    if (conn != NULL) {
      connect(conn);
    }
    else {
      delete conn;
      xr.skipCurrentElement();   
    }
  }

#if 0
  UtIStringStream connLine;
  for (UInt32 i = 0; (i < numCons) && !f.bad() && !connLine.bad() && f.getline(&connLine); ++i)
  {
    CarbonCfgRTLConnection* conn = CarbonCfgRTLConnection::read(this, connLine, cfg, err, ticksPerBeat);
    if (conn != NULL) {
      connect(conn);
    }
    else {
      delete conn;
    }
  }
#endif

}


void CarbonCfgRTLPort::write(CfgXmlWriter& xmlf) {
 // f << "rtlPort" << mName << mWidth << mType.getString()
 //   << mConnections.size() << '\n';
  xmlf.addElement("RtlPort");
  xmlf.addAttribute("name", mName);
  xmlf.addAttribute("width", mWidth);
  xmlf.addAttribute("type", mType.getString());

  //xmlf.addCompletedElement("Connections", mConnections.size());

  for (UInt32 i = 0, n = mConnections.size(); i < n; ++i) {
    mConnections[i]->write(xmlf);
  }

  xmlf.closeElement();
}

CarbonCfgRTLConnection::CarbonCfgRTLConnection(CarbonCfgRTLPort* port) :
  mRTLPort(port)
{
}

CarbonCfgRTLConnection::~CarbonCfgRTLConnection() {
// Put back name as non-unique to be recycled
}

bool CarbonCfgRTLConnection::operator<(const CarbonCfgRTLConnection&) const {
  UtString buf1, buf2;
  getText(&buf1);
  getText(&buf2);
  return buf1 < buf2;
}


CarbonCfgRTLConnection* CarbonCfgRTLConnection::read(CarbonCfgRTLPort* port,
                                                     QXmlStreamReader& xr,
                                                     CarbonCfg* cfg,
                                                     CarbonCfgErrorHandler* err,
                                                     UInt32 ticksPerBeat)
{
  CarbonCfgRTLConnection* conn = NULL;
  UtString token; token << xr.name();

 
  if (token == "ClockGenerator")      {conn = new CarbonCfgClockGen(port, xr, cfg, ticksPerBeat);}
  else if (token == "ResetGenerator") {conn = new CarbonCfgResetGen(port, xr, cfg, ticksPerBeat);}
  else if (token == "ScClock")  {conn = new CarbonCfgSystemCClock(port, xr);}
  else if (token == "Tie")      {conn = new CarbonCfgTie(port, xr);}
  else if (token == "TieParameter") {conn = new CarbonCfgTieParam(port, xr);}
  else if (token == "XtorConnection") {
    CarbonCfgXtorConn* xtorConn = new CarbonCfgXtorConn(port, xr, cfg);
    const char* typeDef = xtorConn->getTypeDef();
    if ((typeDef == NULL) || (strlen(typeDef) == 0)) {
      xtorConn->putTypeDef(cfg->computeSystemCType(port));
    }
    conn = xtorConn;
  }
  else if (token == "ESLPort") {
    // Read it in, if there was no default type, add it
    CarbonCfgESLPort* eslPort = new CarbonCfgESLPort(port, xr, cfg);
    const char* typeDef = eslPort->getTypeDef();
    if ((typeDef == NULL) || (strlen(typeDef) == 0)) {
      eslPort->putTypeDef(cfg->computeSystemCType(port));
    }
    conn = eslPort;

  } else {
    UtString buf;
    buf << "Unexpected token: " << token;
    err->reportError(buf.c_str());
  }

  if ((conn != NULL) && xr.hasError())
  {
    // Error parsing the connection.  Clean up.
    delete conn;
    conn = NULL;
    UtString errStr; errStr << xr.errorString();
    err->reportError(errStr.c_str());
  }

  return conn;
} // Carbo

CarbonCfgRTLConnection* CarbonCfgRTLConnection::read(CarbonCfgRTLPort* port,
                                                     UtIStream& is,
                                                     CarbonCfg* cfg,
                                                     CarbonCfgErrorHandler* err,
                                                     UInt32 ticksPerBeat)
{
  CarbonCfgRTLConnection* conn = NULL;
  UtString token;
  if (is >> token) {
    if (token == "clockGen")      {conn = new CarbonCfgClockGen(port, is, cfg, ticksPerBeat);}
    else if (token == "resetGen") {conn = new CarbonCfgResetGen(port, is, cfg, ticksPerBeat);}
    else if (token == "scClock")  {conn = new CarbonCfgSystemCClock(port, is);}
    else if (token == "tie")      {conn = new CarbonCfgTie(port, is);}
    else if (token == "tieParam") {conn = new CarbonCfgTieParam(port, is);}
    else if (token == "xtorConn") {
      CarbonCfgXtorConn* xtorConn = new CarbonCfgXtorConn(port, is, cfg);
      const char* typeDef = xtorConn->getTypeDef();
      if ((typeDef == NULL) || (strlen(typeDef) == 0)) {
        xtorConn->putTypeDef(cfg->computeSystemCType(port));
      }
      conn = xtorConn;

    } else if (token == "eslPort") {
      // Read it in, if there was no default type, add it
      CarbonCfgESLPort* eslPort = new CarbonCfgESLPort(port, is, cfg);
      const char* typeDef = eslPort->getTypeDef();
      if ((typeDef == NULL) || (strlen(typeDef) == 0)) {
        eslPort->putTypeDef(cfg->computeSystemCType(port));
      }
      conn = eslPort;
      
    } else {
      UtString buf;
      buf << "Unexpected token: " << token;
      is.reportError(buf.c_str());
    }
    if ((conn != NULL) && is.bad()) {
      // Error parsing the connection.  Clean up.
      delete conn;
      conn = NULL;
      err->reportError(is.getErrmsg());
    }
  }
  return conn;
} // CarbonCfgRTLConnection* CarbonCfgRTLConnection::read

void CarbonCfgRTLConnection::connect() {
}

CarbonCfgClockGen::CarbonCfgClockGen(CarbonCfgRTLPort* port, UInt32 init, UInt32 delay,
                                     UInt32 clockCycles, UInt32 compCycles, UInt32 duty):
  CarbonCfgRTLConnection(port),
  mInitialValue(init), mDelay(delay), mFrequency(clockCycles, compCycles),
  mDutyCycle(duty)
{
}

  CarbonCfgClockGen::CarbonCfgClockGen(CarbonCfgRTLPort* port, QXmlStreamReader& xr, CarbonCfg*, UInt32):
  CarbonCfgRTLConnection(port), mFrequency(1,1)
{
  while(xr.readNextStartElement())
  {
    if ("InitialValue" == xr.name())
       mInitialValue = xr.readElementText().toUInt();
    else if ("Delay" == xr.name())
      mDelay = xr.readElementText().toUInt();
    else if ("DutyCycle" == xr.name())
      mDutyCycle = xr.readElementText().toUInt();
    else if ("Frequency" == xr.name())
      mFrequency.read(xr);
  }
}


CarbonCfgClockGen::CarbonCfgClockGen(CarbonCfgRTLPort* port, UtIStream& f, CarbonCfg *cfg, UInt32 ticksPerBeat):
  CarbonCfgRTLConnection(port), mFrequency(1,1)
{
  f >> mInitialValue;

  if (cfg->hasNoTicks()) {
    f >> mDelay;
    mFrequency.read(f);
    f >> mDutyCycle;
  }
  else {
    // old version of configuration, where delay and clock waveform
    // are specified in terms of ticks.
    UInt32 delay, highTicks, lowTicks;
    f >> delay >> highTicks >> lowTicks;

    // convert duty cycle to a percentage
    mDutyCycle = CarbonCfg::divideAndRound(highTicks * 100, highTicks + lowTicks);

    // get the length of perios and a component cycle, in ticks
    UInt32 clockTicks = highTicks + lowTicks;
    UInt32 compTicks = ticksPerBeat;

    // convert delay from ticks per beat into a percentage
    mDelay = CarbonCfg::divideAndRound(delay * 100, clockTicks); 

    // Compute gcd and reduce the ratio to something more palatable
    // E.g. reduce 300:100 to 3:1
    UInt32 gcd = greatestCommonDivisor(clockTicks, compTicks);
    clockTicks = clockTicks / gcd;
    compTicks = compTicks / gcd;

    // The ratio of clock:component cycles is the revers of the ratio
    // of clockTicks:compTicks
    //
    // For example, if the clock period is 3 times longer than the
    // component cycle, we currently have:
    //   clockTicks = 3, compTicks = 1
    // The ratio we want in this case is:
    //   1:3 (clock cycles:component cycles)
    mFrequency.putClockCycles(compTicks);
    mFrequency.putCompCycles(clockTicks);
  }
}

CarbonCfgClockGen::~CarbonCfgClockGen() {
}

void CarbonCfgClockGen::write(CfgXmlWriter& f) {
  f.addElement("ClockGenerator");
  //f << "clockGen" << mInitialValue << mDelay;
  f.addCompletedElement("InitialValue", mInitialValue);
  f.addCompletedElement("Delay", mDelay);
 
  mFrequency.write(f);

  //f << mDutyCycle << '\n';
  f.addCompletedElement("DutyCycle", mDutyCycle);
 
  f.closeElement();
}

CarbonCfgClockGen* CarbonCfgClockGen::castClockGen() {
  return this;
}

CarbonCfgClockGen* CarbonCfgRTLConnection::castClockGen() {
  return NULL;
}

CarbonCfgRTLConnectionType CarbonCfgClockGen::getType() const {
  return eCarbonCfgClockGen;
}

void CarbonCfgClockGen::getText(UtString* text) const {

  *text << "initial=" << mInitialValue
        << " delay=" << mDelay << "%"
        << " frequency=" << mFrequency.getClockCycles()
        << ":" << mFrequency.getCompCycles()
        << " dutyCycle=" << mDutyCycle << "%";
}

CarbonCfgSystemCClock::CarbonCfgSystemCClock(CarbonCfgRTLPort* port) :
  CarbonCfgRTLConnection(port),
  mPeriod(1),
  mPeriodUnits(eCarbonCfgScNS),
  mDutyCycle(0.5),
  mStartTime(0),
  mStartTimeUnits(eCarbonCfgScNS),
  mInitialValue(0)
{}

CarbonCfgSystemCClock::CarbonCfgSystemCClock(CarbonCfgRTLPort* port, UtIStream& f) :
  CarbonCfgRTLConnection(port)  
{
  // The order in the file is:
  //  period, period units, duty cycle, start time, start time units, initial value
  f >> mPeriod;
  CarbonCfgSystemCTimeUnitsIO timeUnits;
  timeUnits.read(f);
  mPeriodUnits = timeUnits.getEnum();
  f >> mDutyCycle >> mStartTime;
  timeUnits.read(f);
  mStartTimeUnits = timeUnits.getEnum();
  f >> mInitialValue;
}

  CarbonCfgSystemCClock::CarbonCfgSystemCClock(CarbonCfgRTLPort* port, QXmlStreamReader& xr) :
  CarbonCfgRTLConnection(port)  
{
  // The order in the file is:
  //  period, period units, duty cycle, start time, start time units, initial value
  while (xr.readNextStartElement())
  {
    if ("Period" == xr.name())
    {
      mPeriod = xr.readElementText().toDouble();
      CarbonCfgSystemCTimeUnitsIO units;
      UtString line; line << xr.attributes().value("units");
      units.parseString(line.c_str());
      mPeriodUnits = units.getEnum();
    }
    else if ("StartTime" == xr.name())
    {
      mStartTime = xr.readElementText().toDouble();
      CarbonCfgSystemCTimeUnitsIO units;
      UtString line; line << xr.attributes().value("units");
      units.parseString(line.c_str());
      mStartTimeUnits = units.getEnum();
    }
    else if ("DutyCycle" == xr.name())
      mDutyCycle = xr.readElementText().toDouble();
    else if ("InitialValue" == xr.name())
      mInitialValue = xr.readElementText().toUInt();
    else
      xr.skipCurrentElement();
  }
}

void CarbonCfgSystemCClock::getText(UtString* buf) const
{
  CarbonCfgSystemCTimeUnitsIO periodUnits = mPeriodUnits; 
  CarbonCfgSystemCTimeUnitsIO startTimeUnits = mStartTimeUnits; 
  *buf << "period=" << mPeriod
       << "periodUnits=" << periodUnits.getString()
       << "dutyCycle=" << mDutyCycle
       << "startTime=" << mStartTime
       << "startTimeUnits=" << startTimeUnits.getString()
       << "initial=" << mInitialValue;
}

void CarbonCfgSystemCClock::write(CfgXmlWriter& f)
{
  CarbonCfgSystemCTimeUnitsIO periodUnits = mPeriodUnits; 
  CarbonCfgSystemCTimeUnitsIO startTimeUnits = mStartTimeUnits; 

  f.addElement("ScClock");

  /*f << "scClock" << mPeriod << periodUnits.getString()
    << mDutyCycle << mStartTime << startTimeUnits.getString()
    << mInitialValue << '\n';*/

  UtString period; period << mPeriod;
  f.addElement("Period", period.c_str());
  f.addAttribute("units", periodUnits.getString());
  f.closeElement();

  UtString dc; dc << mDutyCycle;
  f.addCompletedElement("DutyCycle", dc.c_str());
  
  UtString st; st << mStartTime;
  f.addElement("StartTime", st.c_str());
  f.addAttribute("units", startTimeUnits.getString());
  f.closeElement();

  f.addCompletedElement("InitialValue", mInitialValue);

  f.closeElement();
}

CarbonCfgSystemCClock* CarbonCfgRTLConnection::castSystemCClock() {
  return NULL;
}

CarbonCfgSystemCClock* CarbonCfgSystemCClock::castSystemCClock() {
  return this;
}


CarbonCfgTie::CarbonCfgTie(CarbonCfgRTLPort* port, const DynBitVector& bv)
  : CarbonCfgRTLConnection(port),
    mValue(bv)
{
}

CarbonCfgTie::CarbonCfgTie(CarbonCfgRTLPort* port, UtIStream& f) :
  CarbonCfgRTLConnection(port)
{
  mValue.resize(port->getWidth());
  f >> UtIO::hex >> mValue >> UtIO::dec;
}

CarbonCfgTie::CarbonCfgTie(CarbonCfgRTLPort* port, QXmlStreamReader& xr) :
  CarbonCfgRTLConnection(port)
{
  mValue.resize(port->getWidth());

  UtString buf; buf << xr.readElementText();

  UtIStringStream is(buf);

  is >> UtIO::hex >> mValue >> UtIO::dec;
}

CarbonCfgTie::CarbonCfgTie(CarbonCfgRTLPort* port, UInt32 v1) : CarbonCfgRTLConnection(port)
{
  UInt32 width = port->getWidth();
  INFO_ASSERT(( (1 <= width) || (width <= 32) ), "Initial value not properly sized for port width");
  mValue.resize(width);
  UInt32* vals = mValue.getUIntArray();
  vals[0] = v1;
}
CarbonCfgTie::CarbonCfgTie(CarbonCfgRTLPort* port, UInt32 v1, UInt32 v2) : CarbonCfgRTLConnection(port)
{
  UInt32 width = port->getWidth();
  INFO_ASSERT(( (1 <= width) || (width <= 64) ), "Initial value not properly sized for port width");

  mValue.resize(width);
  UInt32* vals = mValue.getUIntArray();
  vals[0] = v1;
  vals[1] = v2;
}
CarbonCfgTie::CarbonCfgTie(CarbonCfgRTLPort* port, UInt32 v1, UInt32 v2, UInt32 v3, UInt32 v4) : CarbonCfgRTLConnection(port)
{
  UInt32 width = port->getWidth();
  INFO_ASSERT(( (1 <= width) || (width <= 128) ), "Initial value not properly sized for port width");
  mValue.resize(width);
  UInt32* vals = mValue.getUIntArray();
  vals[0] = v1;
  vals[1] = v2;
  vals[2] = v3;
  vals[3] = v4;
}

CarbonCfgTie::~CarbonCfgTie() {
}

void CarbonCfgTie::write(CfgXmlWriter& f) {
  UtString text;
  UtOStringStream ss(&text);
  ss << UtIO::hex << mValue;

  f.addCompletedElement("Tie", text);
 
  //f << "tie" << UtIO::hex << mValue << UtIO::dec << '\n';
}

CarbonCfgTie* CarbonCfgTie::castTie() {
  return this;
}

CarbonCfgTie* CarbonCfgRTLConnection::castTie() {
  return NULL;
}

CarbonCfgRTLConnectionType CarbonCfgTie::getType() const {
  return eCarbonCfgTie;
}

void CarbonCfgTie::getText(UtString* text) const {
  UtOStringStream ss(text);
  ss << "value=0x" << UtIO::hex << mValue;
}


CarbonCfgTieParam::CarbonCfgTieParam(CarbonCfgRTLPort* port, const char* xtorInstName, const char* param) 
  : CarbonCfgRTLConnection(port),
    mParam(param),
    mXtorInstanceName(xtorInstName)
{
  if(mXtorInstanceName.empty())
    mFullParamName = mParam; 
  else
    mFullParamName = mXtorInstanceName + UtString(" ") + mParam; 
}

CarbonCfgTieParam::CarbonCfgTieParam(CarbonCfgRTLPort* port, const UtString& xtorInstName, const UtString& param)
  : CarbonCfgRTLConnection(port),
    mParam(param),
    mXtorInstanceName(xtorInstName)
{
  if(mXtorInstanceName.empty())
    mFullParamName = mParam; 
  else
    mFullParamName = mXtorInstanceName + UtString(" ") + mParam; 
}

CarbonCfgTieParam::CarbonCfgTieParam(CarbonCfgRTLPort* port, UtIStream& f) :
  CarbonCfgRTLConnection(port)
{
  f >> mXtorInstanceName >> mParam;
  if(mXtorInstanceName.empty())
    mFullParamName = mParam; 
  else
    mFullParamName = mXtorInstanceName + UtString(" ") + mParam; 
}

 CarbonCfgTieParam::CarbonCfgTieParam(CarbonCfgRTLPort* port, QXmlStreamReader& xr) :
  CarbonCfgRTLConnection(port)
{
  while (xr.readNextStartElement())
  {
    if ("XtorInstance" == xr.name())
      mXtorInstanceName << xr.readElementText();
    else if ("Parameter" == xr.name())
      mParam << xr.readElementText(); 
  }

  if(mXtorInstanceName.empty())
    mFullParamName = mParam; 
  else
    mFullParamName = mXtorInstanceName + UtString(" ") + mParam; 

}

CarbonCfgTieParam::~CarbonCfgTieParam() {
}

void CarbonCfgTieParam::write(CfgXmlWriter& f) {
  //f << "tieParam" << mXtorInstanceName << mParam << '\n';

  f.addElement("TieParameter");
  f.addCompletedElement("XtorInstance", mXtorInstanceName);
  f.addCompletedElement("Parameter", mParam);
  f.closeElement();
 
}

CarbonCfgTieParam* CarbonCfgTieParam::castTieParam() {
  return this;
}

CarbonCfgTieParam* CarbonCfgRTLConnection::castTieParam() {
  return NULL;
}

CarbonCfgRTLConnectionType CarbonCfgTieParam::getType() const {
  return eCarbonCfgTieParam;
}

void CarbonCfgTieParam::getText(UtString* text) const {
  UtOStringStream ss(text);
  ss << "parameter=" << mParam;
}

CarbonCfgResetGen::CarbonCfgResetGen(CarbonCfgRTLPort* port, UInt64 activeValue, UInt64 inactiveValue,
                                     UInt32 clockCycles, UInt32 compCycles,
                                     UInt32 cyclesBefore, UInt32 cyclesAsserted, UInt32 cyclesAfter):
  CarbonCfgRTLConnection(port), mActiveValue(activeValue), mInactiveValue(inactiveValue),
  mFrequency(clockCycles, compCycles),
  mCyclesBefore(cyclesBefore), mCyclesAsserted(cyclesAsserted), mCyclesAfter(cyclesAfter)
{
}


CarbonCfgResetGen::CarbonCfgResetGen(CarbonCfgRTLPort* port, QXmlStreamReader& xr, CarbonCfg*, UInt32):
  CarbonCfgRTLConnection(port), mFrequency(1,1)
{
  while (xr.readNextStartElement())
  {
    if ("ActiveValue" == xr.name())
      mActiveValue = xr.readElementText().toULongLong();
    else if ("InactiveValue" == xr.name())
      mInactiveValue = xr.readElementText().toULongLong();
    else if ("CyclesBefore" == xr.name())
      mCyclesBefore = xr.readElementText().toUInt();
    else if ("CyclesAsserted" == xr.name())
      mCyclesAsserted = xr.readElementText().toUInt();
    else if ("CyclesAfter" == xr.name())
      mCyclesAfter = xr.readElementText().toUInt();
    else if ("Frequency" == xr.name())
      mFrequency.read(xr);
  }
}

CarbonCfgResetGen::CarbonCfgResetGen(CarbonCfgRTLPort* port, UtIStream& f, CarbonCfg *cfg, UInt32 ticksPerBeat):
  CarbonCfgRTLConnection(port), mFrequency(1,1)
{
  // New format defines arbitrary active and inactive values
  if(cfg->hasResetValues()) {
    f >> mActiveValue >> mInactiveValue;
  }

  // Old format only specifies if active high or low, so set values appropriately
  else {
    UInt32 activeHigh;
    f >> activeHigh; 
    mActiveValue   = (activeHigh != 0);
    mInactiveValue = (activeHigh == 0);
  }

  if (cfg->hasNoTicks()) {
    mFrequency.read(f);
    f >> mCyclesBefore >> mCyclesAsserted >> mCyclesAfter;
  }
  else {
    UInt32 activeTicks, inactiveTicks;
    f >> activeTicks >> inactiveTicks;
    
    // Convert ticks into frequency, cycle counts.
    // We make some (potentially bad) assumptions here because all known .ccfg
    // files use only very simple reset that is multiple cycles long.
    mCyclesBefore = 0;
    mCyclesAsserted = activeTicks / ticksPerBeat;
    mCyclesAfter = inactiveTicks / ticksPerBeat;
    if ((activeTicks % ticksPerBeat) || (inactiveTicks % ticksPerBeat)) {
      UtString buf;
      buf << "Reset generator with activeTicks = " << activeTicks
          << ", inactiveTicks = " << inactiveTicks
          << " has not been accurately converted to the cycle-based representation.";
      f.reportError(buf.c_str());
    }
  }
}

CarbonCfgResetGen::~CarbonCfgResetGen() {
}

void CarbonCfgResetGen::write(CfgXmlWriter& f) {
  f.addElement("ResetGenerator");

  f.addCompletedElement("ActiveValue", mActiveValue);
  f.addCompletedElement("InactiveValue", mInactiveValue);

  //f << "resetGen" << mActiveValue << mInactiveValue;
  mFrequency.write(f);

  f.addCompletedElement("CyclesBefore", mCyclesBefore);
  f.addCompletedElement("CyclesAsserted", mCyclesAsserted);
  f.addCompletedElement("CyclesAfter", mCyclesAfter);

  //f << mCyclesBefore << mCyclesAsserted << mCyclesAfter << '\n';

  f.closeElement();
}

CarbonCfgResetGen* CarbonCfgResetGen::castResetGen() {
  return this;
}

void CarbonCfgResetGen::getText(UtString* text) const {
  *text << "activeValue= 0x" << UtIO::hex << mActiveValue
        << " inactiveValue= 0x" << UtIO::hex << mInactiveValue
        << " frequency=" << UtIO::dec << mFrequency.getClockCycles()
        << ":" << mFrequency.getCompCycles()
        << " before=" << mCyclesBefore
        << " asserted=" << mCyclesAsserted
        << " after=" << mCyclesAfter;
}

CarbonCfgResetGen* CarbonCfgRTLConnection::castResetGen() {
  return NULL;
}

CarbonCfgRTLConnectionType CarbonCfgResetGen::getType() const {
  return eCarbonCfgResetGen;
}

void CarbonCfgXtorConn::connect() {
  CarbonCfgXtorConn* oldCon = mInstance->getConnection(mPortIndex);
  if (oldCon != NULL) {
    if (oldCon->mRTLPort != NULL) {
      oldCon->mRTLPort->disconnect(oldCon);
    }
  }
  mInstance->putConnection(mPortIndex, this);
}

void CarbonCfgRTLConnection::disconnect() {
}


void CarbonCfgXtorConn::disconnect() {
  // When an RTL port is disconnected from an xtor instance, we
  // need to delete the xtor port but replace it with an empty one.

  SCRIPT_ASSERT(this == mInstance->getConnection(mPortIndex), "connect sanity");
  CarbonCfgXtorConn* c = new CarbonCfgXtorConn(NULL, mInstance, mPortIndex);
  mInstance->putConnection(mPortIndex, c);
}

// The transactor connection must reference an instantiated transactor.
CarbonCfgXtorConn::CarbonCfgXtorConn(CarbonCfgRTLPort* rport,
                                     CarbonCfgXtorInstance* inst,
                                     UInt32 portIndex)
  : CarbonCfgRTLConnection(rport),
    mInstance(inst),
    mPortIndex(portIndex)
{
}

CarbonCfgXtorConn::CarbonCfgXtorConn(CarbonCfgRTLPort* rport, QXmlStreamReader& xr,
                                     CarbonCfg* cfg)
  : CarbonCfgRTLConnection(rport),
    mValid(true)
{
  UtString inst, port, buf, expr;

  inst = QUSTR(xr.attributes().value("name").toString());
  port = QUSTR(xr.attributes().value("port").toString());
  expr = QUSTR(xr.attributes().value("expression").toString());
  
  mExpr = expr;

  xr.skipCurrentElement();

  if (inst.length() > 0)
  {
    mInstance = cfg->findXtorInstance(inst.c_str());
    if (mInstance == NULL) {
      mPortIndex = 0;
      buf << carbonCfgERR_UNKNOWNXTOR << ": Cannot find transactor instance " << inst;
      xr.raiseError(buf.c_str());
      mValid = false;
    }
    else {
      if (mInstance->getType() && !mInstance->getType()->findPort(port.c_str(), &mPortIndex)) {
        buf << carbonCfgERR_NOXTORCONN << ": Cannot find transactor " << inst << " port " << port;
        xr.raiseError(buf.c_str());
        mValid = false;
      }
    }
  }
}

CarbonCfgXtorConn::CarbonCfgXtorConn(CarbonCfgRTLPort* rport, UtIStream& is,
                                     CarbonCfg* cfg)
  : CarbonCfgRTLConnection(rport),
    mValid(true)
{
  UtString inst, port, buf, expr;
  if ((is >> inst) && (is >> port) &&
      (!cfg->hasXtorConnExprs() || (is >> mExpr)))
  {
    mInstance = cfg->findXtorInstance(inst.c_str());
    if (mInstance == NULL) {
      mPortIndex = 0;
      buf << carbonCfgERR_UNKNOWNXTOR << ": Cannot find transactor instance " << inst;
      is.reportError(buf.c_str());
      mValid = false;
    }
    else {
      if (mInstance->getType() && !mInstance->getType()->findPort(port.c_str(), &mPortIndex)) {
        buf << carbonCfgERR_NOXTORCONN << ": Cannot find transactor " << inst << " port " << port;
        is.reportError(buf.c_str());
        mValid = false;
      }
    }
  }
}

CarbonCfgXtorConn::~CarbonCfgXtorConn() {
}

void CarbonCfgXtorConn::write(CfgXmlWriter& x) {
  x.addElement("XtorConnection");
//   f << "xtorConn" << mInstance->getName() << getPort()->getName()
//    << mExpr << '\n';

  x.addAttribute("name", mInstance->getName());
  x.addAttribute("port", getPort()->getName());
  x.addAttribute("expression", mExpr);
 
  x.closeElement();
}


CarbonCfgXtorConn* CarbonCfgXtorConn::castXtorConn() {
  return this;
}

CarbonCfgXtorPort* CarbonCfgXtorConn::getPort() const {
  return mInstance->getType()->getPort(mPortIndex);
}



CarbonCfgXtorConn* CarbonCfgRTLConnection::castXtorConn() {
  return NULL;
}

CarbonCfgRTLConnectionType CarbonCfgXtorConn::getType() const {
  return eCarbonCfgXtorConn;
}

void CarbonCfgXtorConn::getText(UtString* text) const {
  *text << mInstance->getName() << "." << getPort()->getName();
}

CarbonCfgESLPort::CarbonCfgESLPort(CarbonCfgESLPort* eslPort, CarbonCfgRTLPort* rtlPort) : CarbonCfgRTLConnection(rtlPort)
{
  mType = eslPort->mType;
  mName = eslPort->mName;
  mTypeDef = eslPort->mTypeDef;
  mPortMode = eslPort->mPortMode;
  mParamInstance = eslPort->mParamInstance;
  mParamName = eslPort->mParamName;
  mParamXtorInstName = eslPort->mParamXtorInstName;
}



CarbonCfgESLPort::CarbonCfgESLPort(CarbonCfgRTLPort* port, const char* name,
                                   CarbonCfgESLPortType type, const char* typeDef, 
                                   CarbonCfgESLPortMode portMode):
  CarbonCfgRTLConnection(port),
  mType(type),
  mName(name),
  mTypeDef(typeDef),
  mPortMode(portMode),
  mParamInstance(NULL)
{
}

CarbonCfgESLPort::CarbonCfgESLPort(CarbonCfgRTLPort* port,
                                   UtIStream& is,
                                   CarbonCfgID cfg):
  CarbonCfgRTLConnection(port)
{
  mName="";
  mTypeDef="";
  mPortMode=eCarbonCfgESLPortUndefined;
  mParamInstance = NULL;
  mParamName.clear();
  mParamXtorInstName.clear();

  UtString typeName, buf;
  if ((is >> mName) && (is >> typeName) &&
      (!cfg->hasPortExprs() || (is >> mExpr)) &&
      (!cfg->hasAbstractRegs() || (is >> mTypeDef)) && 
      (!cfg->hasESLPortModes() || (is >> mPortMode)) &&
      (!cfg->hasESLPortParameter() || (is >> mParamXtorInstName)) &&
      (!cfg->hasESLPortParameter() || (is >> mParamName))
      )
  {
    bool insert = true;
    if (typeName == "input") {
      mType = eCarbonCfgESLInput;
    }
    else if (typeName == "output") {
      mType = eCarbonCfgESLOutput;
    }
    else if (typeName == "inout") {
      mType = eCarbonCfgESLInout;
    }
    else {
      UtString buf;
      buf << "Invalid ESL port type: " << typeName;
      is.reportError(buf.c_str());
      insert = false;
    }
    if (insert && (! cfg->addESLPort(this))) {
      buf << "Duplicate port name: " << mName;
      is.reportError(buf.c_str());
    }
  }
}

CarbonCfgESLPort::CarbonCfgESLPort(CarbonCfgRTLPort* port,
                                   QXmlStreamReader& xr,
                                   CarbonCfgID cfg):
  CarbonCfgRTLConnection(port)
{
  mName="";
  mTypeDef="";
  mPortMode=eCarbonCfgESLPortUndefined;
  mParamInstance = NULL;
  mParamName.clear();
  mParamXtorInstName.clear();

  UtString typeName, buf;
 
  mName << xr.attributes().value("name");
  typeName << xr.attributes().value("type");
  mTypeDef << xr.attributes().value("typeDef");
  mExpr << xr.attributes().value("expression");

  // parse the other fields
  while (xr.readNextStartElement())
  {
    if ("PortMode" == xr.name())
    {
      UtString line; line << xr.readElementText();
      CarbonCfgESLPortModeIO mode;
      mode.parseString(line.c_str());
      mPortMode = mode.getEnum();
    }
    else if ("ParamXtorInstanceName" == xr.name())
      mParamXtorInstName << xr.readElementText();
    else if ("ParameterName" == xr.name())
      mParamName << xr.readElementText();
    else
      xr.skipCurrentElement();
  }

 
  bool insert = true;
  if (typeName == "input") {
    mType = eCarbonCfgESLInput;
  }
  else if (typeName == "output") {
    mType = eCarbonCfgESLOutput;
  }
  else if (typeName == "inout") {
    mType = eCarbonCfgESLInout;
  }
  else {
    UtString buf;
    buf << "Invalid ESL port type: " << typeName;
    xr.raiseError(buf.c_str());
    insert = false;
  }
  if (insert && (! cfg->addESLPort(this))) {
    buf << "Duplicate port name: " << mName;
    xr.raiseError(buf.c_str());
  }

}


CarbonCfgESLPort::~CarbonCfgESLPort() {
}

void CarbonCfgESLPort::getText(UtString* buf) const {
  *buf << mName;
}
void CarbonCfgESLPort::write(CfgXmlWriter& f) {
  f.addElement("ESLPort");

  f.addAttribute("name", mName);
  f.addAttribute("type", mType.getString());
  f.addAttribute("typeDef", mTypeDef);
  f.addAttribute("expression", mExpr);

  f.addCompletedElement("PortMode", mPortMode.getString());
  f.addCompletedElement("ParamXtorInstanceName", mParamXtorInstName);
  f.addCompletedElement("ParameterName", mParamName);
  
  //f << "eslPort" << mName << mType.getString() << mExpr << mTypeDef << mPortMode.getString(); 
  //f << mParamXtorInstName << mParamName << '\n';

  f.closeElement();
}
    
CarbonCfgESLPort* CarbonCfgESLPort::castESLPort() {
  return this;
}

CarbonCfgESLPort* CarbonCfgRTLConnection::castESLPort() {
  return NULL;
}

CarbonCfgRTLConnectionType CarbonCfgESLPort::getType() const {
  return eCarbonCfgESLPort;
}

CarbonCfgXtorInstance::CarbonCfgXtorInstance(const char* name,
                                             CarbonCfgXtor* xtype):
  mName(name),
  mXtorType(xtype),
  mClockMaster(NULL),
  mUseDebugAccess(1), // For new projects default to the debugAccess() methods
  mESLClockMaster(""),
  mESLResetMaster("")
{
  init();
}


CarbonCfgXtorInstance::CarbonCfgXtorInstance(QXmlStreamReader& xr, CarbonCfgID cfg) {
  mClockMaster = NULL;
  mESLClockMaster = "";
  mESLResetMaster = "";
  mXtorType = NULL;
  mConnections = NULL;
  UtString xtorType;
  UtString abstraction;

  mName = QUSTR(xr.attributes().value("name").toString());
  xtorType = QUSTR(xr.attributes().value("type").toString());

  if (mName.length() > 0 && xtorType.length() > 0)
  {
    unsigned int ticksPerCycle, initialCycleTicks;
    ticksPerCycle = 0;
    initialCycleTicks = 0;

    UtString clockMasterName;
    UtString xtorLibName;
    UtString variant;
    while (xr.readNextStartElement())
    {
      if ("ClockMaster" == xr.name())
        clockMasterName = QUSTR(xr.readElementText());
      else if ("DebugAccess" == xr.name())
        mUseDebugAccess = xr.readElementText().toUInt();
      else if ("LibraryName" == xr.name())
        xtorLibName = QUSTR(xr.readElementText());
      else if ("Variant" == xr.name())
      {
        variant = QUSTR(xr.readElementText());
        mXtorType = cfg->findXtor(xtorType.c_str(), xtorLibName.c_str(), variant.c_str());
        init();
      }
      else if ("ResetMaster" == xr.name())
        mESLResetMaster = QUSTR(xr.readElementText());
      else if ("Abstraction" == xr.name())
       abstraction = QUSTR(xr.readElementText());
      else if ("Parameter" == xr.name())
        addParameter(this, xr);
      else
        xr.skipCurrentElement();
    }

   
    if (mXtorType == NULL) {
      UtString buf;
      buf << carbonCfgERR_UNKNOWNXTOR << ": Cannot find transactor type " << xtorType << " in library " << xtorLibName << ". Check the \"Transactor Definition\" file.";
      xr.raiseError(buf.c_str());

      // Return because init() uses mXtorType
      return;
    }


    // Find the right clock master based on what the xtor supports if we have a clock master name
    if (!clockMasterName.empty()) {
      if (mXtorType->useESLClockMaster()) {
        // Set the ESL clock master as a string. This is easier to
        // manage right now. We do error checking to make sure the
        // port exists as part of CarbonCfg::loadAndCheckDatabase()
        putESLClockMaster(clockMasterName.c_str());
        
      } else {
        mClockMaster = cfg->findXtorInstance(clockMasterName.c_str());
        if (mClockMaster == NULL) {
          UtString message;
          message << "Clock master \"" << clockMasterName << "\" cannot be found for transactor " << mName;
          xr.raiseError(message.c_str());
        }
      }
    }
  

    // Override the abstraction name with the non-default version if
    // there is one. init() sets the default one.
    if (!abstraction.empty()) {
      mAbstraction = abstraction;
    }
  }
}

CarbonCfgXtorInstance::CarbonCfgXtorInstance(UtIStream& f, CarbonCfgID cfg) {
  mClockMaster = NULL;
  mESLClockMaster = "";
  mESLResetMaster = "";
  mXtorType = NULL;
  mConnections = NULL;
  UtString xtorType;
  UtString abstraction;
  if ((f >> mName) && (f >> xtorType)) {
    unsigned int ticksPerCycle, initialCycleTicks;
    ticksPerCycle = 0;
    initialCycleTicks = 0;

    // Get the clock master name. It is applied below when we know the
    // right transactor.
    UtString clockMasterName;
    if (cfg->hasXtorClockMaster()) {
      if (!(f >> clockMasterName)) {
        UtString message;
        message << "Error reading name of clock master for transactor " << mName;
        f.reportError(message.c_str());
      }
    }
    else if (cfg->hasClockedXtors()) {
      // the ticksPerCycle and initialCycleTicks do not exist in 
      // .ccfg files prior to version 5
      UtString message;
      if (!(f >> ticksPerCycle)) {
        message << "Error reading number of ticks per cycle for transactor " << mName;
        f.reportError(message.c_str());
      }
      if (!(f >> initialCycleTicks)) {
        message << "Error reading number of ticks before first cycle for transactor " << mName;
        f.reportError(message.c_str());
      }
      
      // Now that we use clock slave ports instead of the event queue for
      // clocking the transactor ports, it is no longer valid to specify the
      // number oif 'ticks' for the transactor cycle.  Issue an error.
      if (ticksPerCycle != 0 || initialCycleTicks != 0) {
        message << "Transactor port " << mName << " uses initial ticks and ticksPerCycle to "
                << "schedule transactor updates at a different rate than the component.\n"
                << "Simulation tick based transactor clocking is no longer supported.\n"
                << "Please specify a \"transactor\" parameter value that names a Clock_Input "
                << "that drives updates of the transactor.";
      }
    }

    // If the .ccfg version supports the debug access flag read it
    // from the file.  Note that this may be overridden later.
    int ccfgUseDebugAccess = 0;
    if (cfg->hasXtorDebugAccess()) {
      f >> ccfgUseDebugAccess;
    }
    // If the .ccfg file is an older file, assume that the user wants to use the old
    // debug transaction methods.
    else
      ccfgUseDebugAccess = 0;

    // If the .ccfg version supports the library name for the transactor get it
    UtString xtorLibName;
    UtString variant;
    if (cfg->hasXtorFullName()) {
      f >> xtorLibName >> variant;
    } else {
      // Search the libraries for the first occurance. This is no
      // better than what happened before we added multiple
      // library. Hopefully that will make existing configs work.
      bool found = false;
      for (CarbonCfg::XtorLibs::SortedLoop l = cfg->loopXtorLibs(); !l.atEnd() && !found; ++l) {
        const UtString& libName = l.getKey();
        CarbonCfgXtorLib* xtorLib = l.getValue();
        if (xtorLib->findXtor(xtorType.c_str(), variant.c_str()) != NULL) {
          xtorLibName = libName;
          found = true;
        }
      }
    }

    // If the ccfg has reset masters then read that
    if (cfg->hasXtorTLM2Support()) {
      // Note that we dont' read the value into mAbstraction because
      // we want the default value to get set by init() below so that
      // it is common to all constructors. We then override it if one
      // was set.
      f >> mESLResetMaster >> abstraction;
    }

    mXtorType = cfg->findXtor(xtorType.c_str(), xtorLibName.c_str(), variant.c_str());
    if (mXtorType == NULL) {
      UtString buf;
      buf << carbonCfgERR_UNKNOWNXTOR << ": Cannot find transactor type " << xtorType << " in library " << xtorLibName << ". Check the \"Transactor Definition\" file.";
      f.reportError(buf.c_str());

      // Return because init() uses mXtorType
      return;
    }

    // Now that we know the transactor type, we can properly set
    // mUseDebugAccess.  If this is a SoCD-provided transactor, use
    // debug access because it's always implemented and is the
    // preferred method.  Otherwise, use what was in the ccfg file.
    //
    // We can't always use debug access because it causes some
    // v1-based transactor tests to fail.  Those aren't worth
    // debugging.
    if (mXtorType->isSDXactor()) {
      mUseDebugAccess = 1;
    } else {
      mUseDebugAccess = ccfgUseDebugAccess;
    }


    // Find the right clock master based on what the xtor supports if we have a clock master name
    if (!clockMasterName.empty()) {
      if (mXtorType->useESLClockMaster()) {
        // Set the ESL clock master as a string. This is easier to
        // manage right now. We do error checking to make sure the
        // port exists as part of CarbonCfg::loadAndCheckDatabase()
        putESLClockMaster(clockMasterName.c_str());
        
      } else {
        mClockMaster = cfg->findXtorInstance(clockMasterName.c_str());
        if (mClockMaster == NULL) {
          UtString message;
          message << "Clock master \"" << clockMasterName << "\" cannot be found for transactor " << mName;
          f.reportError(message.c_str());
        }
      }
    }
    init();

    // Override the abstraction name with the non-default version if
    // there is one. init() sets the default one.
    if (!abstraction.empty()) {
      mAbstraction = abstraction;
    }
  }
} // CarbonCfgXtorInstance::CarbonCfgXtorInstance

void CarbonCfgXtorInstance::init() {
  if (mXtorType) {
    UInt32 np = mXtorType->numPorts();
    mConnections = CARBON_ALLOC_VEC(CarbonCfgXtorConn*, np);

    // By default, the instances creates XtorConns with null RTL ports.
    // As the RTL ports get connected, the empty XtorConns get erased.
    // The empty ones do not get written to disk, because the writing
    // to disk happens as part of the RTLPort.
    for (UInt32 i = 0; i < np; ++i) {
      mConnections[i] = new CarbonCfgXtorConn(NULL, this, i);
    }

    mParamInstMap = new ParamInstMap;

    CarbonCfgXtor::ParamLoop params = mXtorType->loopParams();
    for (; !params.atEnd(); ++params) {
      UtString paramName = params.getKey();
      CarbonCfgXtorParam* param = params.getValue();
      UtString value(param->getDefaultValue());
      addParameter(paramName, value);
    }

    // Pick a default for the abstraction variant if the transactor supports it
    if (mXtorType->numAbstractions() > 0) {
      mAbstraction = mXtorType->getAbstraction(0)->getName();
    }
  }
}

CarbonCfgXtorInstance::~CarbonCfgXtorInstance() {
  if (mConnections != NULL) {
    UInt32 np = mXtorType->numPorts();
    for (UInt32 i = 0; i < np; ++i) {
      CarbonCfgXtorConn* conn = mConnections[i];
      if (conn->getRTLPort() == NULL) {
        delete conn;
      }
    }
  }

  for (LoopMap<ParamInstMap> l(*mParamInstMap); !l.atEnd(); ++l) {
    CarbonCfgXtorParamInst* inst = l.getValue();
    if (inst != NULL)
      delete inst;
  }

  delete mParamInstMap;
}

void CarbonCfgXtorInstance::write(CfgXmlWriter& xmlw) {
  //f << "xtor" << mName << mXtorType->getName();
  xmlw.addElement("Xtor");
  xmlw.addAttribute("name", mName);
  xmlw.addAttribute("type", mXtorType->getName());

 
  if (mXtorType->useESLClockMaster()) {
    // f << mESLClockMaster;
    xmlw.addCompletedElement("ClockMaster", mESLClockMaster);
  
  } else {
    // f << ((mClockMaster == NULL) ? "" : mClockMaster->getName());
    xmlw.addCompletedElement("ClockMaster", ((mClockMaster == NULL) ? "" : mClockMaster->getName()));
  }

  //  f << mUseDebugAccess << mXtorType->getLibraryName() << mXtorType->getVariant() << mESLResetMaster
  //  << mAbstraction << '\n';

  xmlw.addCompletedElement("DebugAccess", mUseDebugAccess);
  xmlw.addCompletedElement("LibraryName", mXtorType->getLibraryName());
  xmlw.addCompletedElement("Variant", mXtorType->getVariant());
  xmlw.addCompletedElement("ResetMaster", mESLResetMaster);
  xmlw.addCompletedElement("Abstraction", mAbstraction);

 
  // Write all the xtor-connections that are not owned by an
  // RTL connection
  UInt32 np = mXtorType->numPorts();
  for (UInt32 i = 0; (i < np); ++i) {
    CarbonCfgXtorConn* conn = mConnections[i];
    if (conn->getRTLPort() == NULL) {
      conn->write(xmlw);
    }
  }

  bool envDebugEmbedded = getenv("CARBON_DEBUG_EMBEDDED_MODELS") ? true : false;

  // Write all the parameters
  for (LoopMap<ParamInstMap> l(*mParamInstMap); !l.atEnd(); ++l) {
    const UtString& name = l.getKey();
    CarbonCfgXtorParamInst* inst = l.getValue();
    bool hidden = inst->hidden();

    bool debugEmbeddedModels = false;
    if (inst->getElementGenerationType() == eCarbonCfgElementGenerated && envDebugEmbedded)
      debugEmbeddedModels = true;

    const UtString& value = inst->getValue();
    if (value != NULL)
    {
      bool added = false;
      if (inst->getElementGenerationType() == eCarbonCfgElementUser)
      {
        // f << "parameter" << mName << name << value << '\n';
        xmlw.addElement("Parameter");
        added = true;
      }
      else if (debugEmbeddedModels)
      {
        //f << "debugParameterInst" << mName << name << value << '\n';
        xmlw.addElement("DebugParameter");
        added = true;
      }

      // Close the element and add any extra sub elements that are common
      if (added) {
        // Parameter elements
        xmlw.addCompletedElement("Name", mName);
        xmlw.addCompletedElement("Hidden", (hidden? "true" : "false"));
        xmlw.addCompletedElement("Pname", name);
        xmlw.addCompletedElement("Value", value);

        // Enum choices if there are some
        for (UInt32 i = 0; i < inst->numEnumChoices(); ++i) {
          const UtString& choice = inst->getEnumChoice(i);
          xmlw.addCompletedElement("EnumChoice", choice);
        }
        
        // Close the parameter element opened in the if then else if
        xmlw.closeElement();
      }
    } // if
  } // for

  xmlw.closeElement(); // Xtor
}

void CarbonCfgXtorInstance::putAbstraction(const char* abstraction)
{
  // Validate the name
  SCRIPT_ASSERT(mXtorType != NULL, "Xtor instance not correctly configured");
  bool validName = false;
  for (UInt32 i = 0; i < mXtorType->numAbstractions() && !validName; ++i) {
    CarbonCfgXtorAbstraction* xtorAbstraction = mXtorType->getAbstraction(i);
    validName = (strcmp(xtorAbstraction->getName(), abstraction) == 0);
  }

  // Only set it if it is valid
  if (validName) {
    mAbstraction = abstraction;
  }
}

CarbonCfgXtorAbstraction* CarbonCfgXtorInstance::getAbstraction(void) const
{
  // Get the variant based on our name or NULL if there isn't one
  for (UInt32 i = 0; i < mXtorType->numAbstractions(); ++i) {
    CarbonCfgXtorAbstraction* xtorAbstraction = mXtorType->getAbstraction(i);
    if (strcmp(xtorAbstraction->getName(), mAbstraction.c_str()) == 0) {
      return xtorAbstraction;
    }
  }
  return NULL;
}
  

CarbonCfgRegisterLoc::CarbonCfgRegisterLoc()
{
}

CarbonCfgRegisterLoc::~CarbonCfgRegisterLoc()
{
}

CarbonCfgRegisterLocConstant *CarbonCfgRegisterLoc::castConstant()
{
  return 0;
}

CarbonCfgRegisterLocRTL *CarbonCfgRegisterLoc::castRTL()
{
  return 0;
}

CarbonCfgRegisterLocReg *CarbonCfgRegisterLoc::castReg()
{
  return 0;
}

CarbonCfgRegisterLocArray *CarbonCfgRegisterLoc::castArray()
{
  return 0;
}


CarbonCfgRegisterLocUser *CarbonCfgRegisterLoc::castUser()
{
  return 0;
}


CarbonCfgRegisterLocConstant::CarbonCfgRegisterLocConstant(CarbonUInt64 constant_value)
  : mValue(constant_value)
{
}

CarbonCfgRegisterLocConstant::~CarbonCfgRegisterLocConstant()
{
}

void CarbonCfgRegisterLocConstant::write(CfgXmlWriter &xml)
{
  xml.addElement("location", getValue());
  xml.addAttribute("type", "constant");
  xml.closeElement();
}

CarbonCfgRegisterLocConstant *CarbonCfgRegisterLocConstant::castConstant()
{
  return this;
}

CarbonCfgRegLocType CarbonCfgRegisterLocConstant::getType() const
{
  return eCfgRegLocConstant;
}

CarbonCfgRegisterLoc*
CarbonCfgRegisterLocConstant::clone(const CarbonCfgTemplate&) const
{
  CarbonCfgRegisterLoc* loc = new CarbonCfgRegisterLocConstant(mValue);
  return loc;
}


CarbonCfgRegisterLocRTL::CarbonCfgRegisterLocRTL(const char *rtl_path)
  : mPath(rtl_path),
    mHasRange(false)
{
  // These will be set when the config is loaded,
  // after checking the DB
  mLSB = 0;
  mMSB = 0;
  mLeft = 0;
  mRight = 0;
}

CarbonCfgRegisterLocRTL::CarbonCfgRegisterLocRTL(const char *rtl_path,
                                                 CarbonUInt32 left, CarbonUInt32 right)
  : mPath(rtl_path),
    mLeft(left),
    mRight(right),
    mHasRange(true)
{
  // These will be set when the config is loaded,
  // after checking the DB
  mLSB = 0;
  mMSB = 0;
}

CarbonCfgRegisterLocRTL::~CarbonCfgRegisterLocRTL()
{
}

CarbonCfgRegisterLocRTL *CarbonCfgRegisterLocRTL::castRTL()
{
  return this;
}

void CarbonCfgRegisterLocRTL::cloneHelper(CarbonCfgRegisterLocRTL* clone) const
{
  // the path is created by the constructor, so we just have to copy
  // the other fields.
  clone->mHasRange = mHasRange;
  clone->mLeft = mLeft;
  clone->mRight = mRight;

  // Not sure if LSB/MSB is significant since this is set by the
  // Python code, but lets copy it anyway.
  clone->mLSB = mLSB;
  clone->mMSB = mMSB;
}

CarbonCfgRegisterLocReg::CarbonCfgRegisterLocReg(const char *rtl_path)
  : CarbonCfgRegisterLocRTL(rtl_path)
{
}

CarbonCfgRegisterLocReg::CarbonCfgRegisterLocReg(const char *rtl_path,
                                                 CarbonUInt32 left, CarbonUInt32 right)
  : CarbonCfgRegisterLocRTL(rtl_path, left, right)
{
}

CarbonCfgRegisterLocReg::~CarbonCfgRegisterLocReg()
{
}

void CarbonCfgRegisterLocReg::write(CfgXmlWriter &xml)
{
  xml.addElement("location", getPath());
  xml.addAttribute("type", "reg");
  xml.closeElement();
  if (getHasRange()) {
    xml.addElement("range");
    xml.addAttribute("left", getLeft());
    xml.addAttribute("right", getRight());
    xml.closeElement();
  }
}

CarbonCfgRegisterLocReg *CarbonCfgRegisterLocReg::castReg()
{
  return this;
}

CarbonCfgRegLocType CarbonCfgRegisterLocReg::getType() const
{
  return eCfgRegLocRegister;
}

CarbonCfgRegisterLoc*
CarbonCfgRegisterLocReg::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the path and convert any variables
  UtString path;
  templ.resolveString(getPath(), &path);

  // Create a new register location
  CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(path.c_str());

  // Use the RTL clone helper to do the rest
  cloneHelper(loc);
  return loc;
}

CarbonCfgRegisterLocArray::CarbonCfgRegisterLocArray(const char *rtl_path, CarbonUInt32 index)
  : CarbonCfgRegisterLocRTL(rtl_path),
    mIndex(index)
{
}

CarbonCfgRegisterLocArray::CarbonCfgRegisterLocArray(const char *rtl_path, CarbonUInt32 index,
                                                     CarbonUInt32 left, CarbonUInt32 right)
  : CarbonCfgRegisterLocRTL(rtl_path, left, right),
    mIndex(index)
{
}

CarbonCfgRegisterLocArray::~CarbonCfgRegisterLocArray()
{
}

void CarbonCfgRegisterLocArray::write(CfgXmlWriter &xml)
{
  xml.addElement("location", getPath());
  xml.addAttribute("type", "array");
  xml.addAttribute("index", getIndex());
  xml.closeElement();
  if (getHasRange()) {
    xml.addElement("range");
    xml.addAttribute("left", getLeft());
    xml.addAttribute("right", getRight());
    xml.closeElement();
  }
}

CarbonCfgRegisterLocArray *CarbonCfgRegisterLocArray::castArray()
{
  return this;
}

CarbonCfgRegLocType CarbonCfgRegisterLocArray::getType() const
{
  return eCfgRegLocArray;
}

CarbonCfgRegisterLoc*
CarbonCfgRegisterLocArray::clone(const CarbonCfgTemplate& templ) const
{
  // Copy the path but first replace any variables
  UtString path;
  templ.resolveString(getPath(), &path);

  // Construct a new object using just the path and index
  CarbonCfgRegisterLocArray* loc;
  loc = new CarbonCfgRegisterLocArray(path.c_str(), getIndex());

  // Use the clone helper for the remaining fields
  cloneHelper(loc);
  return loc;
}

CarbonCfgRegisterLocUser::CarbonCfgRegisterLocUser()
{
}

CarbonCfgRegisterLocUser::~CarbonCfgRegisterLocUser()
{
}

void CarbonCfgRegisterLocUser::write(CfgXmlWriter& xml)
{
  xml.addElement("location");
  xml.addAttribute("type", "user");
  xml.closeElement();
}

CarbonCfgRegisterLocUser* CarbonCfgRegisterLocUser::castUser()
{
  return this;
}

CarbonCfgRegLocType CarbonCfgRegisterLocUser::getType() const
{
  return eCfgRegLocUser;
}

CarbonCfgRegisterLoc*
CarbonCfgRegisterLocUser::clone(const CarbonCfgTemplate&) const
{
  // No fields, simple construction
  return new CarbonCfgRegisterLocUser();
}

CarbonCfgRegisterField::CarbonCfgRegisterField(CarbonUInt32 high, CarbonUInt32 low,
                                               CarbonCfgRegAccessType access)
  : mHigh(high),
    mLow(low),
    mAccess(access),
    mLoc(0)
{
}

CarbonCfgRegisterField::~CarbonCfgRegisterField()
{
  if (mLoc)
    delete mLoc;
}

void CarbonCfgRegisterField::write(CfgXmlWriter &xml)
{
  xml.addElement("field");
  xml.addAttribute("high", getHigh());
  xml.addAttribute("low", getLow());

  xml.addElement("access", gCarbonCfgRegAccessTypes[getAccess()]);
  xml.closeElement();

  xml.addElement("name", getName());
  xml.closeElement();

  mLoc->write(xml);
  xml.closeElement();
}

CarbonCfgRegisterField*
CarbonCfgRegisterField::clone(const CarbonCfgTemplate& templ) const
{
  // Create a new object
  CarbonCfgRegisterField* field = new CarbonCfgRegisterField(mHigh, mLow, mAccess);

  // Copy the name but first replace any variables
  UtString name;
  templ.resolveString(getName(), &name);
  field->putName(name.c_str());

  if ( mLoc == NULL ){
    QString errMsg = QString("Undefined <location> for register field with name: %1. \nThere is probably a syntax error in the ccfg file, hint: the location type must be lower case.").arg(name.c_str());
    UtString msg; msg << errMsg;
    SCRIPT_ASSERT_RETURN(0, msg.c_str(),field);
  }
  // Clone the locator
  CarbonCfgRegisterLoc* loc = mLoc->clone(templ);
  field->putLoc(loc);

  return field;
}


CarbonCfgRegister::CarbonCfgRegister(const char* name,
                                     CarbonCfgGroup* group,
                                     UInt32 width,
                                     bool big_endian,
                                     CarbonCfgRadix radix,
                                     const char* comment,
                                     bool pc_reg)
  : CarbonCfgCustomCodeContainer(CarbonCfgRegCustomCodeSectionIO().getStrings()),
    mName(name),
    mGroup(group),
    mWidth(width),
    mBigEndian(big_endian),
    mPcReg(pc_reg),
    mRadix(radix),
    mComment(comment),
    mOffset(0)
{
}

CarbonCfgRegister::~CarbonCfgRegister() {
  mFields.clearPointers();
}

CarbonCfgRegister::CarbonCfgRegister(UtIStream& f, UtString* groupName):
  CarbonCfgCustomCodeContainer(CarbonCfgRegCustomCodeSectionIO().getStrings()),
  mGroup(NULL),
  mWidth(0),
  mBigEndian(false),
  mPcReg(false),
  mOffset(0)
{
  // Convert from old .ccfg file format
  UtString sig;
  f >> mWidth >> mRadix >> sig >> mName >> *groupName >> mComment;
  CarbonCfgRegisterField *field = new CarbonCfgRegisterField(mWidth - 1, 0,
                                                             eCfgRegAccessRW);
  field->putLoc(new CarbonCfgRegisterLocReg(sig.c_str()));
  mFields.push_back(field);
}

void CarbonCfgRegister::write(CfgXmlWriter &xml) {
  xml.addElement("register");
  xml.addAttribute("name", getName());
  xml.addElement("width", getWidth());
  xml.closeElement();
  xml.addElement("offset", getOffset());
  xml.closeElement();
  xml.addElement("bigendian", getBigEndian() ? "true" : "false");
  xml.closeElement();
  xml.addElement("pcreg", getPcReg() ? "true" : "false");
  xml.closeElement();
  xml.addElement("radix", getRadix().getString());
  xml.closeElement();
  xml.addElement("esl-port", getPort());
  xml.closeElement();
  xml.addElement("comment", getComment());
  xml.closeElement();

  for (Loop<CarbonCfgRegister::FieldList> l(mFields); !l.atEnd(); ++l)
    (*l)->write(xml);

  // Write any custom code sections
  for (UInt32 i = 0; i < mCustomCodeVec.size(); ++i) {
    mCustomCodeVec[i]->write(xml);
  }

  xml.closeElement();
}

CarbonCfgRegCustomCode*
CarbonCfgRegister::addCustomCode()
{
  CarbonCfgRegCustomCode* customCode = new CarbonCfgRegCustomCode();
  mCustomCodeVec.push_back(customCode);
  return customCode;
}

void CarbonCfgRegister::addCustomCode(CarbonCfgRegCustomCode* cc)
{
  mCustomCodeVec.push_back(cc);
}

bool CarbonCfgRegister::checkFields()
{
  // Fill a bitmask with the ranges covered by fields,
  // making sure they don't overlap or go out of range

  // TODO - better error reporting
  
  CarbonUInt32 num_words = (mWidth + 31) / 32;
  CarbonUInt32 *mask = CARBON_ALLOC_VEC(CarbonUInt32, num_words);
  memset(mask, 0, num_words * sizeof(CarbonUInt32));

  bool error = false;
  for (Loop<FieldList> l(mFields); !l.atEnd(); ++l) {
    // Ensure high index >= low index.
    // Check for out of range, too.

    if (((*l)->getHigh() < (*l)->getLow()) ||
        ((*l)->getHigh() >= mWidth)) {
      error = true;
      break;
    }

    // Set the bits covered by this range, making sure they're
    // not already set
    for (CarbonUInt32 i = (*l)->getLow();
         i <= (*l)->getHigh();
         ++i) {
      CarbonUInt32 word_index = i / 32;
      CarbonUInt32 bit_index = i % 32;
      if ((mask[word_index] >> bit_index) & 0x1) {
        // already set!
        error = true;
        break;
      }

      mask[word_index] |= (0x1 << bit_index);
    }

    if (error)
      break;
  }

  CARBON_FREE_VEC(mask, CarbonUInt32, num_words);

  return (!error);
}

CarbonCfgRegister*
CarbonCfgRegister::clone(CarbonCfgGroup* group, const CarbonCfgTemplate& templ)
{
  // Convert any variables in strings
  UtString name, comment;
  templ.resolveString(getName(), &name);
  templ.resolveString(getComment(), &comment);

  // Create the register from the source, but use the new group instead
  CarbonCfgRegister* reg = new CarbonCfgRegister(name.c_str(),
                                                 group,
                                                 mWidth,
                                                 mBigEndian,
                                                 mRadix,
                                                 comment.c_str(),
                                                 mPcReg);

  // Copy the simple fields that are not created by the constructor
  reg->mOffset = mOffset;
  reg->mPort = mPort;

  // Clone the custom code sections
  for (CarbonUInt32 i = 0; i < numCustomCodes(); ++i){
    CarbonCfgCustomCode* cc = getCustomCode(i);
    CarbonCfgRegCustomCode* regCc = cc->castReg();
    CarbonCfgRegCustomCode* newCc = regCc->clone(templ);
    reg->addCustomCode(newCc);
  }

  // Clone the fields
  for (Loop<const FieldList> l(mFields); !l.atEnd(); ++l) {
    CarbonCfgRegisterField* field = *l;
    CarbonCfgRegisterField* newField = field->clone(templ);
    reg->addField(newField);
  }

  return reg;
}

CarbonCfgRegister::RTLLoop::RTLLoop(CarbonCfgRegister *reg)
{
  // Build a list of pointers to all the RTL register/memory
  // locations used by fields of this registers
  for (Loop<CarbonCfgRegister::FieldList> l(reg->mFields); !l.atEnd(); ++l) {
    CarbonCfgRegisterLocRTL *loc = (*l)->getLoc()->castRTL();
    if (loc)
      mRTLList.push_back(loc);
  }

  mIter = mRTLList.begin();
}

CarbonCfgRegister::RTLLoop::~RTLLoop()
{
}

void CarbonCfgRegister::RTLLoop::operator++()
{
  ++mIter;
}

bool CarbonCfgRegister::RTLLoop::atEnd()
{
  return (mIter == mRTLList.end());
}

CarbonCfgRegisterLocRTL *CarbonCfgRegister::RTLLoop::operator*()
{
  return (*mIter);
}

void CarbonCfgMemoryLocAttr::cloneHelper(CarbonCfgMemoryLocAttr* clone) const
{
  clone->mStartWordOffset = mStartWordOffset;
  clone->mEndWordOffset = mEndWordOffset;
  clone->mMsb = mMsb;
  clone->mLsb = mLsb;
}

UInt32 CarbonCfgMemoryLocAttr::getBitSize(void) const
{
  // This assumes that msb > lsb and end word offset > start word offset
  // These are checked in the locator
  UInt32 bitWidth = mMsb - mLsb + 1;
  return bitWidth * (mEndWordOffset - mStartWordOffset + 1);
}

void CarbonCfgMemoryMessages::clearMessages(void)
{
  mMessages.clear();
}

void CarbonCfgMemoryMessages::reportWarning(CarbonCfg* cfg, const UtString& msg)
{
  // Add the message to the local context
  mMessages << msg << "\n";

  // If a cfg context was passed in, report it there was well (caller
  // can avoid duplicates by passing in NULL for the second locator on
  // a multi-locator error)
  if (cfg != NULL) {
    cfg->reportMessage(eCarbonMsgWarning, msg.c_str());
  }
}

CarbonCfgMemoryLoc::CarbonCfgMemoryLoc()
{
}

CarbonCfgMemoryLoc::~CarbonCfgMemoryLoc()
{
}

void CarbonCfgMemoryLoc::writeAttr(const char* tag, const CarbonCfgMemoryLocAttr& attr, CfgXmlWriter &xml)
{
  xml.addElement(tag);
  xml.addAttribute("start", attr.mStartWordOffset);
  xml.addAttribute("end", attr.mEndWordOffset);
  xml.addAttribute("msb", attr.mMsb);
  xml.addAttribute("lsb", attr.mLsb);
  xml.closeElement();
}

CarbonCfgMemoryLocRTL* CarbonCfgMemoryLoc::castRTL()
{
  return 0;
}

CarbonCfgMemoryLocPort* CarbonCfgMemoryLoc::castPort()
{
  return 0;
}

CarbonCfgMemoryLocUser* CarbonCfgMemoryLoc::castUser()
{
  return 0;
}

void CarbonCfgMemoryLoc::cloneHelper(CarbonCfgMemoryLoc* clone) const
{
  // Only one field, the display attribute
  clone->mDisplayAttr = mDisplayAttr;
}

bool CarbonCfgMemoryLoc::overlaps( const CarbonCfgMemoryLoc& other) const
{
  // Check if the words don't overlap. This occurs when the other
  // locator's start is beyond this locator's end or the others
  // locator's end is before this locator's start.
  UInt64 thisStart = getDisplayStartWordOffset();
  UInt64 thisEnd = getDisplayEndWordOffset();
  UInt64 otherStart = other.getDisplayStartWordOffset();
  UInt64 otherEnd = other.getDisplayEndWordOffset();
  if ((otherStart > thisEnd) || (otherEnd < thisStart)) {
    return false;
  }

  // Words overlap, make sure lsb and msb don't overlap. We assume that
  // the msb > lsb. That should be checked
  if ((other.getDisplayLsb() > getDisplayMsb()) ||
      (other.getDisplayMsb() < getDisplayLsb())) {
    return false;
  }

  // They must overlap
  return true;
}

void CarbonCfgMemoryLoc::check(CarbonCfg* cfg, const char* memInfo, UInt32 bitWidth, CarbonDB*) const
{
  // Clear the error messages from any previous check
  mMemoryMessages.clearMessages();

  // Msb is always > Lsb for the display (we use UInt32's)
  if (getDisplayMsb() < getDisplayLsb()) {
    UtString msg;
    msg << "Display MSB '" << getDisplayMsb() << "' should be greater than or equal to Display LSB '"
        << getDisplayLsb() << "'.";
    reportWarning(cfg, memInfo, msg);
  }

  // Display bits can't go beyond the width of the memory
  if (getDisplayLsb() >= bitWidth) {
    UtString msg;
    msg << "Display LSB '" << getDisplayLsb() << "' exceeds the bit width for the memory '" << bitWidth << "'.";
    reportWarning(cfg, memInfo, msg);
  }
  if (getDisplayMsb() >= bitWidth) {
    UtString msg;
    msg << "Display MSB '" << getDisplayMsb() << "' exceeds the bit width for the memory '" << bitWidth << "'.";
    reportWarning(cfg, memInfo, msg);
  }

  // end word offsets > start word offset
  if (getDisplayStartWordOffset() > getDisplayEndWordOffset()) {
    UtString msg;
    msg << "Display End word offset '" << getDisplayEndWordOffset()
        << "' should be greater than or equal to the Display Start word offset '"
        << getDisplayStartWordOffset() << "'.";
    reportWarning(cfg, memInfo, msg);
  }
}

const char* CarbonCfgMemoryLoc::rangeString(UtString* str)
{
  (*str) << "[" << getDisplayStartWordOffset() << ":" << getDisplayEndWordOffset() << "]["
         << getDisplayMsb() << ":" << getDisplayLsb() << "]";
  return str->c_str();
}

CarbonCfgMemoryLocRTL::CarbonCfgMemoryLocRTL(const char *rtl_path, UInt32 bitWidth, UInt64 endWord)
  : mPath(rtl_path)
{
  putDisplayEndWordOffset(endWord);
  putDisplayMsb(bitWidth-1);
  putRTLEndWordOffset(endWord);
  putRTLMsb(bitWidth-1);
}

CarbonCfgMemoryLocRTL::~CarbonCfgMemoryLocRTL()
{
}

CarbonCfgMemoryLocRTL *CarbonCfgMemoryLocRTL::castRTL()
{
  return this;
}

void CarbonCfgMemoryLocRTL::write(CfgXmlWriter &xml)
{
  xml.addElement("memLoc");
  xml.addAttribute("type", "rtl");

  xml.addElement("path", mPath.c_str());
  xml.closeElement(); // path

  writeAttr("display", mDisplayAttr, xml);
  writeAttr("rtl", mRTLAttr, xml);

  xml.closeElement(); // memLoc
}

CarbonCfgMemoryLoc*
CarbonCfgMemoryLocRTL::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the path and convert any variables
  UtString path;
  templ.resolveString(getPath(), &path);

  // Make a new object and clone the parent class
  CarbonCfgMemoryLocRTL* loc = new CarbonCfgMemoryLocRTL(path.c_str(),
                                                         getBitWidth(),
                                                         getRTLEndWordOffset());
  CarbonCfgMemoryLoc::cloneHelper(loc);

  // Now copy over the local fields not coverd by the
  // constructor. There is some overlap in the mRTLAttr field.
  loc->mRTLAttr = mRTLAttr;
  return loc;
}

void CarbonCfgMemoryLocRTL::check(CarbonCfg* cfg, const char* memInfo, UInt32 bitWidth, CarbonDB* db) const
{
  // Do the general check first. This clears any previous checks state so it must be first
  CarbonCfgMemoryLoc::check(cfg, memInfo, bitWidth, db);

  // Now do the local checks. The RTL sizes should match the display sizes
  SInt32 width = (SInt32)getDisplayMsb() - (SInt32)getDisplayLsb() + 1;
  if (width != (SInt32)getBitWidth()) {
    UtString msg;
    msg << "BitWidth mismatch: display(" << width << ") vs rtl(" << getBitWidth() << "). Calculated from: <display msb=\"" << getDisplayMsb() <<"\" lsb=\"" << getDisplayLsb() << "\" ...> vs <rtl msb=\"" << getRTLMsb() << "\" lsb=\"" << getRTLLsb() << "\" ...>";
    reportWarning(cfg, memInfo, msg);
  }

  // compare the depth for 'display' and 'rtl' elements
  SInt64 depth = getDisplayEndWordOffset() - getDisplayStartWordOffset() + 1;
  if (depth != getDepth()) {
    UtString msg;
    msg << "Depth mismatch: display(" << depth << ") vs rtl(" << getDepth() << ").  Calculated from: <display start=\"" << getDisplayStartWordOffset() <<"\" end=\"" << getDisplayEndWordOffset() << "\" ...> vs <rtl start=\"" << getRTLStartWordOffset() << "\" end=\"" << getRTLEndWordOffset() << "\" ...>";
    reportWarning(cfg, memInfo, msg);
  }

  // If a DB is provided, do additional checks
  if (db != NULL) {
    // Check if it exists - give up on other checks if it doesn't
    const CarbonDBNode* node = carbonDBFindNode(db, getPath());
    if (node == NULL) {
      UtString msg;
      msg << "RTL Path '" << getPath() << "' not found.";
      reportWarning(cfg, memInfo, msg);
      return;
    }

    // Path exists, it must be a 2d array. give up on other checks if it isn't
    if (!(carbonDBIs2DArray(db, node) && !carbonDBIsContainedByComposite(db, node))) {
      UtString msg;
      msg << "RTL Path '" << getPath() << "' is not a valid Carbon Memory.";
      reportWarning(cfg, memInfo, msg);
      return;
    }

    // Check for negative indices. They are not supported
    bool neg = false;
    if ((carbonDBGetMSB(db, node) < 0) || (carbonDBGetLSB(db, node) < 0)) {
      UtString msg;
      msg << "Bit range contains negative numbers [" << carbonDBGetMSB(db, node)
          << ":" << carbonDBGetLSB(db, node) << "]; not supported.";
      reportWarning(cfg, memInfo, msg);
      neg = true;
    }

    // Check for negative memory locations, not yet supported
    if ((carbonDBGet2DArrayRightAddr(db, node) < 0) || (carbonDBGet2DArrayLeftAddr(db, node) < 0)) {
      UtString msg;
      msg << "Memory range contains negative numbers [" << carbonDBGet2DArrayLeftAddr(db, node)
          << ":" << carbonDBGet2DArrayRightAddr(db, node) << "]; not supported.";
      reportWarning(cfg, memInfo, msg);
      neg = true;
    }

    // If we have negative numbers the code below does not work right. It deals with unsigned values.
    if (neg) {
      return;
    }

    // do range checks. msb/lsb must have the same polarity and be
    // within the right range. Otherwise other checks don't work so
    // stop here
    UInt32 msb = carbonDBGetMSB(db, node);
    UInt32 lsb = carbonDBGetLSB(db, node);
    if (((getRTLMsb() > getRTLLsb()) && (msb < lsb)) ||
        ((getRTLMsb() < getRTLLsb()) && (msb > lsb))) {
      // Ranges inverted
      UtString msg;
      msg << "RTL memory's range [" << msb << ":" << lsb
          << "] inverted from locator RTL range [" << getRTLMsb() << ":" << getRTLLsb() << "]";
      reportWarning(cfg, memInfo, msg);
      return;
    }

    // Check that they are in range
    bool msbInRange = true;
    bool lsbInRange = true;
    if (msb < lsb) {
      if ((getRTLMsb() < msb) || (getRTLMsb() > lsb)) {
        msbInRange = false;
      }
      if ((getRTLLsb() < msb) || (getRTLLsb() > lsb)) {
        lsbInRange = false;
      }

    } else {
      // msb >= lsb
      if ((getRTLMsb() > msb) || (getRTLMsb() < lsb)) {
        msbInRange = false;
      }
      if ((getRTLLsb() > msb) || (getRTLLsb() < lsb)) {
        lsbInRange = false;
      }
    }
    
    // Report any possible errors
    if (!msbInRange) {
      UtString msg;
      msg << "RTL MSB '" << getRTLMsb() << "' out of range, RTL memory range is ["
          << msb << ":" << lsb << "]";
      reportWarning(cfg, memInfo, msg);
    }
    if (!lsbInRange) {
      UtString msg;
      msg << "RTL LSB '" << getRTLLsb() << "' out of range, RTL memory range is ["
          << msb << ":" << lsb << "]";
      reportWarning(cfg, memInfo, msg);
    }

    // Check that the memory addresses are in range. They can be inverted unlike bit ranges
    UInt32 left = carbonDBGet2DArrayLeftAddr(db, node);
    UInt32 right = carbonDBGet2DArrayRightAddr(db, node);
    bool startInRange = true;
    bool endInRange = true;
    if (left < right) {
      if ((getRTLStartWordOffset() < left) || (getRTLStartWordOffset() > right)) {
        startInRange = false;
      }
      if ((getRTLEndWordOffset() < left) || (getRTLEndWordOffset() > right)) {
        endInRange = false;
      }
    } else {
      if ((getRTLStartWordOffset() > left) || (getRTLStartWordOffset() < right)) {
        startInRange = false;
      }
      if ((getRTLEndWordOffset() > left) || (getRTLEndWordOffset() < right)) {
        endInRange = false;
      }
    }

    // Report any possible errors
    if (!startInRange) {
      UtString msg;
      msg << "<rtl ... start=\"" << getRTLStartWordOffset() << "\" is not within declared range of HDL memory: [" << left << ":" << right << "]";
      reportWarning(cfg, memInfo, msg);
    }
    if (!endInRange) {
      UtString msg;
      msg << "<rtl ... end=\"" << getRTLEndWordOffset() << "\" is not within declared range of HDL memory: [" << left << ":" << right << "]";
      reportWarning(cfg, memInfo, msg);
    }
  } // if
} // void CarbonCfgMemoryLocRTL::check

CarbonCfgMemoryLocPort::CarbonCfgMemoryLocPort(const char *port_name)
  : mPortName(port_name),
    mFixedAddress(true)
{
}

CarbonCfgMemoryLocPort::~CarbonCfgMemoryLocPort()
{
}

CarbonCfgMemoryLocPort *CarbonCfgMemoryLocPort::castPort()
{
  return this;
}

void CarbonCfgMemoryLocPort::write(CfgXmlWriter &xml)
{
  xml.addElement("memLoc");
  xml.addAttribute("type", "port");

  xml.addElement("portname", mPortName.c_str());
  xml.closeElement(); // portname

  xml.addElement("fixedaddr", mFixedAddress ? "true" : "false");
  xml.closeElement(); // portname

  xml.closeElement(); // memLoc
}

CarbonCfgMemoryLoc*
CarbonCfgMemoryLocPort::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the name and convert any variables
  UtString name;
  templ.resolveString(getPortName(), &name);

  // Make a new object and clone the parent class
  CarbonCfgMemoryLocPort* loc = new CarbonCfgMemoryLocPort(name.c_str());
  CarbonCfgMemoryLoc::cloneHelper(loc);

  // Now copy over the local fields not covered by the constructor
  loc->mFixedAddress =  mFixedAddress;
  return loc;
}

void CarbonCfgMemoryLocUser::write(CfgXmlWriter &xml)
{
  xml.addElement("memLoc");
  xml.addAttribute("type", "user");
  xml.closeElement(); // memLoc
}

CarbonCfgMemoryLoc*
CarbonCfgMemoryLocUser::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the name and convert any variables
  UtString name;
  templ.resolveString(getName(), &name);

  // No fields, simple construction
  return new CarbonCfgMemoryLocUser(name.c_str());
}

CarbonCfgMemoryBlock::~CarbonCfgMemoryBlock()
{
  for(UInt32 i = 0; i < mLocators.size(); ++i)
    delete mLocators[i];
}

void CarbonCfgMemoryBlock::write(CfgXmlWriter &xml)
{
  xml.addElement("memBlock");
  xml.addAttribute("name", mName.c_str());
  xml.addAttribute("addr", mBaseAddr);
  xml.addAttribute("size", mSize);
  
  // Write all locators in this block
  for (UInt32 i = 0; i < mLocators.size(); ++i)
    mLocators[i]->write(xml);

  // Custom codes
  writeCustomCodes(xml);

  xml.closeElement(); // memBlock
  
}

CarbonCfgMemoryLocRTL* CarbonCfgMemoryBlock::addLocRTL(const char* path, UInt32 width)
{
  // Default to width of block
  if (width == 0) width = mBitWidth;

  if ( width == 0 ) {
    QString errMsg = QString("Invalid memory width: %1 specified for memory: %2").arg(width).arg(path);
    UtString msg; msg << errMsg;
    SCRIPT_ASSERT_RETURN(0, msg.c_str(), NULL);
  }

  // Default End Word to size of block
  UInt32 endWord = mSize/((width+7)/8) -1;
  CarbonCfgMemoryLocRTL* loc = new CarbonCfgMemoryLocRTL(path, width, endWord);
  mLocators.push_back(loc);

  return loc;
}

CarbonCfgMemoryLocPort* CarbonCfgMemoryBlock::addLocPort(const char* port_name)
{
  CarbonCfgMemoryLocPort* loc = new CarbonCfgMemoryLocPort(port_name);
  mLocators.push_back(loc);

  return loc;
}

CarbonCfgMemoryLocUser* CarbonCfgMemoryBlock::addLocUser(const char* name)
{
  CarbonCfgMemoryLocUser* loc = new CarbonCfgMemoryLocUser(name);
  mLocators.push_back(loc);

  return loc;
}

CarbonCfgMemoryLoc* CarbonCfgMemoryBlock::getLoc(UInt32 index) const
{
  if (index < mLocators.size())
    return mLocators[index];
  else
    return NULL;
}

CarbonCfgMemoryBlock*
CarbonCfgMemoryBlock::clone(const CarbonCfgTemplate& templ) const
{
  // Create a new one from this one
  CarbonCfgMemoryBlock* block = new CarbonCfgMemoryBlock(getWidth());

  // Initialize the fields that aren't covered by the constructor
  // Covert any variables in the name
  templ.resolveString(getName(), &block->mName);
  block->mBaseAddr = mBaseAddr;
  block->mSize = mSize;

  // Clone the locators
  for (UInt32 i = 0; i < numLocs(); ++i) {
    CarbonCfgMemoryLoc* loc = getLoc(i);
    CarbonCfgMemoryLoc* newLoc = loc->clone(templ);
    block->mLocators.push_back(newLoc);
  }

  // Clone the custom code sections
  for (CarbonUInt32 i = 0; i < numCustomCodes(); ++i) {
    CarbonCfgCustomCode* cc = getCustomCode(i);
    CarbonCfgMemoryBlockCustomCode* memCc = cc->castMemoryBlock();
    CarbonCfgMemoryBlockCustomCode* newCc = memCc->clone(templ);
    block->CarbonCfgCustomCodeContainer::addCustomCode(newCc);
  }

  return block;
}

void CarbonCfgMemoryBlock::check(CarbonCfg* cfg, const char* memInfo, UInt64 maxAddr, CarbonDB* db) const
{
  // Clear an error state from a previous check
  mMemoryMessages.clearMessages();

  // Create a string for this blocks mem info
  UtString blockInfo;
  blockInfo << memInfo << ":" << getName();

  // Check that the block is in range
  UInt64 maxByteAddr = ((maxAddr + 1) * ((getWidth() + 7) / 8)) - 1;
  if (getBase() > maxByteAddr) {
    UtString msg;
    msg << blockInfo << ": block base address '" << getBase()
        << "' out of range; must be within memory's maximum address '" << maxAddr
        << "(" << maxByteAddr << ")' words(bytes).";
    mMemoryMessages.reportWarning(cfg, msg);
  }
  UInt64 endAddr = getBase() + getSize() - 1;
  if (endAddr > maxByteAddr) {
    UtString msg;
    msg << blockInfo << ": block size '" << getSize() << "' is '" << endAddr - maxByteAddr
        << "' bytes beyond the memory's maximum address '" << maxAddr
        << "(" << maxByteAddr << ")' words(bytes).";
    mMemoryMessages.reportWarning(cfg, msg);
  }

  // Walk the locators and check them individually. Also compute the bit size of each locator
  UInt32 bitSize = 0;
  for (UInt32 i = 0; i < numLocs(); ++i) {
    CarbonCfgMemoryLoc* loc = getLoc(i);
    loc->check(cfg, blockInfo.c_str(), getWidth(), db);
    bitSize += loc->getBitSize();
  }

  // Check that the locators bit size doesn't exceed the block's bit size
  if ((getSize() * 8) < bitSize) {
    UtString msg;
    msg << blockInfo << ": sum of locators bit size '" << bitSize << "' exceeds the block bit size '"
        << (getSize() *8) << "' computed from block's byte size '" << getSize() << "'.";
    mMemoryMessages.reportWarning(cfg, msg);
  }

  // Check that the locators don't overlap. This is N*N
  for (UInt32 i = 0; i < numLocs(); ++i) {
    CarbonCfgMemoryLoc* loc1 = getLoc(i);
    for (UInt32 j = i + 1; j < numLocs(); ++j) {
      CarbonCfgMemoryLoc* loc2 = getLoc(j);
      if (loc1->overlaps(*loc2)) {
        UtString msg, loc1Range, loc2Range;
        msg << "locators overlap; loc[" << i << "]: " << loc1->rangeString(&loc1Range)
            << ", loc[" << j << "]: " << loc2->rangeString(&loc2Range) << ".";
        loc1->reportWarning(cfg, blockInfo.c_str(), msg);
        loc2->reportWarning(NULL, blockInfo.c_str(), msg);
      }
    }
  }
}

// legacy ctor
CarbonCfgMemory::CarbonCfgMemory(CarbonCfg* parent, const char* signal, const char* name,
                                 const char* initFile, UInt64 maxAddrs,
                                 UInt32 width, CarbonCfgReadmemType rt,
                                 const char* comment, const char* disassemblyName,
                 const char* disassemblerName)
  : CarbonCfgCustomCodeContainer(CarbonCfgMemoryCustomCodeSectionIO().getStrings()),
    mParent(parent),
    mName(name),
    mInitFile(initFile),
    mMaxAddrs(maxAddrs),
    mWidth(width),
    mComment(comment),
    mReadmemType(rt),
    mMemInitType(eCarbonCfgMemInitNone),
    mDisassemblyName(disassemblyName),
    mDisassemblerName(disassemblerName),
    mDisplayAtZero(true),
    mMAU((mWidth+7)/8),
    mBigEndian(false),
    mProgramMemory(false),
    mElementGenerationType(eCarbonCfgElementUser),
    mEditFlags(CcfgFlags::EditMemoryAll)
{
  // This constructor is a legacy constructor so setup a structure that are
  // compatible with legacy methods
  CarbonCfgMemoryBlock* block = addMemoryBlock();
  block->putName(mName.c_str());
  block->putSize((maxAddrs+1)*((mWidth+7)/8)); // Size in bytes
  block->addLocRTL(signal);
}

// this is the non-legacyMemory ctor
CarbonCfgMemory::CarbonCfgMemory(CarbonCfg* parent, const char* name, UInt32 width, UInt64 maxAddr)
  : CarbonCfgCustomCodeContainer(CarbonCfgMemoryCustomCodeSectionIO().getStrings()),
    mParent(parent),
    mName(name),
    mInitFile(""),
    mMaxAddrs(maxAddr),
    mWidth(width),
    mComment(""),
    mReadmemType(eCarbonCfgReadmemh),
    mMemInitType(eCarbonCfgMemInitNone),
    mDisassemblyName(""),
    mDisassemblerName(""),
    mDisplayAtZero(false), // nonLegacy memories normally do not dispalyAtZero
    mMAU((mWidth+7)/8),
    mBigEndian(false),
    mProgramMemory(false),
    mElementGenerationType(eCarbonCfgElementUser),
    mEditFlags(CcfgFlags::EditMemoryAll)
{
}

CarbonCfgMemory::~CarbonCfgMemory()
{
  for(UInt32 i = 0; i < mBlocks.size(); ++i)
    delete mBlocks[i];

  removeAllSystemAddressESLPorts();
}

void CarbonCfgMemory::putSystemAddressESLPortName(quint32 index, const char* eslPortName)
{
  UtString name; name << eslPortName;
  if (index < (quint32)mSystemAddressESLPortNames.count())
    mSystemAddressESLPortNames[index]->assign(eslPortName);
  else
  {
    QString errMsg = QString("Invalid index (%1) passed into putSystemAddressESLPortName for port (%2)").arg(index).arg(eslPortName);
    UtString msg; msg << errMsg;
    SCRIPT_ASSERT(0, msg.c_str());
  }
}

void CarbonCfgMemory::removeAllSystemAddressESLPorts()
{
  mSystemAddressESLPortBaseAddresses.clear();
 
  while(mSystemAddressESLPortNames.count() > 0)
  {
    UtString* value = mSystemAddressESLPortNames.takeFirst();
    delete value;
  }
}
void CarbonCfgMemory::removeSystemAddressESLPort(quint32 index)
{
  mSystemAddressESLPortBaseAddresses.removeAt(index);
  UtString* value = mSystemAddressESLPortNames.takeAt(index);
  delete value;
}

// These are always added as pairs
void CarbonCfgMemory::addSystemAddressESLPort(const char* eslPortName, qint64 baseAddress)
{
  UtString* value = new UtString(eslPortName);
  mSystemAddressESLPortNames.push_back(value);
  mSystemAddressESLPortBaseAddresses.push_back(baseAddress);

}

void CarbonCfgMemory::putSystemAddressESLPortName(const char* eslPortName)
{
  // If we weren't using a format that can be hand-edited I would
  // rather have the CarbonEslPort structure passed in. But, I don't
  // see a good way to do that and keep the ccfg file easily editable.

  if (mSystemAddressESLPortNames.empty())
    addSystemAddressESLPort(eslPortName, 0);
  else
    mSystemAddressESLPortNames[0]->assign(eslPortName);
}

void CarbonCfgMemory::putMemoryDisassemblyName(const char* disassemblyName)
{
  mDisassemblyName.assign(disassemblyName);
}

void CarbonCfgMemory::putMemoryDisassemblerName(const char* disassemblerName)
{
  mDisassemblerName.assign(disassemblerName);
}

// This constructor is only used when reading an old
// ccfg file from before we switched to xml format for the memories.
// This implies that the memory must have one memBlock with one memLoc
// of type=RTL, (since that was all we supported).
CarbonCfgMemory::CarbonCfgMemory(UtIStream& f, CarbonCfg* cfg):
  CarbonCfgCustomCodeContainer(CarbonCfgMemoryCustomCodeSectionIO().getStrings()),
  mParent(cfg),
  mMaxAddrs(0),
  mReadmemType(eCarbonCfgReadmemh),
  mMemInitType(eCarbonCfgMemInitNone),
  mDisassemblyName(""),
  mDisassemblerName(""),
  mDisplayAtZero(true),
  mBigEndian(false),
  mProgramMemory(false)
{
  UInt64   baseAddr  = 0;
  UtString signal;

  if (f >> signal >> mName >> mInitFile >> mMaxAddrs >> mWidth >> mComment) {
    if (! (f >> mReadmemType)) {
      // optional readmem type field added 3/8/06, so do not error out if it's 
      // missing
      (void) f.getErrmsg();
    }
  }
  mMemInitType = eCarbonCfgMemInitNone;
  baseAddr = 0;
  if (cfg->hasMemoryInitEnum())
  {
    UtString portName;  
    f >> mMemInitType >> baseAddr >> portName;
    putSystemAddressESLPortName(portName.c_str());
  }
  else
  {
    // initialize the meminittype on whether or not the initfile is
    // empty
    if (! mInitFile.empty())
      mMemInitType = eCarbonCfgMemInitReadmem;
  }

  // add data members for disassembly view
  if (cfg->hasDisassembly()) {
    f >> mDisassemblyName;
    f >> mDisassemblerName;
  } else {
    mDisassemblyName = "";
    mDisassemblerName = "";
  }

  // If there is a disassembler also mark the memory as a Program Memory
  if (mDisassemblerName != "")
    putProgramMemory(true);

  // Default to width of memory
  mMAU = ((mWidth+7)/8);

  // This constructor is only used when reading an old
  // ccfg file from before we switched to xml format for the memories
  // This implies that the memory must have one block with one RTL
  // locator, since that was all we supported.
  CarbonCfgMemoryBlock* block = addMemoryBlock();
  block->putName(mName.c_str());
  block->putBase(baseAddr);
  block->putSize((mMaxAddrs+1)*((mWidth+7)/8)); // calculate the size in bytes
  block->addLocRTL(signal.c_str());
}

  void CarbonCfgMemory::write(CfgXmlWriter &xml) {

    bool debugEmbeddedModels = false;
    if (getElementGenerationType() == eCarbonCfgElementGenerated && getenv("CARBON_DEBUG_EMBEDDED_MODELS"))
      debugEmbeddedModels = true;

    // cleanse the mutual exclusive init types
    //    if (getParent()->useLegacyMemories())
    {
      switch (mMemInitType)
      {
      case eCarbonCfgMemInitNone:
        mInitFile.clear();
        putSystemAddressESLPortName("");
        putBaseAddr(0);
        break;
      case eCarbonCfgMemInitProgPreload:
        {
          mInitFile.clear();
          QString progESLPort = getSystemAddressESLPortName();
          if (progESLPort.isEmpty())
          {
            // not a preload mem
            mMemInitType = eCarbonCfgMemInitNone;
            putBaseAddr(0);
          }
        }
        break;
      case eCarbonCfgMemInitReadmem:
        putBaseAddr(0);
        putSystemAddressESLPortName("");
        if (mInitFile.empty())
          // Not a readmem but an InitNone
          mMemInitType = eCarbonCfgMemInitNone;
        break;
      }
      putElementGenerationType(eCarbonCfgElementUser);
    }

    if (getElementGenerationType() == eCarbonCfgElementGenerated)
    {
      xml.addElement("memoryJournal");
      xml.addAttribute("name", mName.c_str());
      
      if(mMemInitType == eCarbonCfgMemInitReadmem) {
        xml.addElement("readMemInit");
        xml.addAttribute("type", mReadmemType.getString());
        xml.addElement("initFile", mInitFile.c_str());
        xml.closeElement(); // initFile
        xml.closeElement(); // readMemInit
      }

      if(mMemInitType == eCarbonCfgMemInitProgPreload) 
      {
        for (quint32 i=0; i<NumSystemAddressESLPorts(); i++)
        {
          xml.addElement("debugAccess");
          QString value = QString("%1").arg(getSystemAddressESLPortBaseAddress(i));
          UtString hv; hv << value;
          xml.addAttribute("baseAddress", hv.c_str());
          xml.addElement("debugPort");
          xml.addAttribute("name", getSystemAddressESLPortName(i));
          xml.closeElement(); // debugPort
          xml.closeElement(); // debugAccess
        }
      }

       xml.closeElement(); // memJournal
    }

    if (debugEmbeddedModels || getElementGenerationType() == eCarbonCfgElementUser)
    {
      // Write with a different tag, so the read doesn't cause
      // duplicate components to be created.
      if (debugEmbeddedModels)
        xml.addElement("debugEmbeddedMemSpace");
      else
        xml.addElement("memSpace");
      xml.addAttribute("name", mName.c_str());
      xml.addAttribute("id", "0");
      xml.addAttribute("width", mWidth);
      xml.addAttribute("maxaddr", mMaxAddrs);
      xml.addAttribute("mau", mMAU);
      if(mBigEndian)
        xml.addAttribute("endian", "big");
      else
        xml.addAttribute("endian", "little");
      if (mProgramMemory) {
        xml.addAttribute("programMemory", "true");
      } else {
        xml.addAttribute("programMemory", "false");
      }

      xml.addElement("comment", mComment.c_str());
      xml.closeElement(); // comment

      if(mMemInitType == eCarbonCfgMemInitReadmem) {
        xml.addElement("readMemInit");
        xml.addAttribute("type", mReadmemType.getString());
        xml.addElement("initFile", mInitFile.c_str());
        xml.closeElement(); // initFile
        xml.closeElement(); // readMemInit
      }

      if(mMemInitType == eCarbonCfgMemInitProgPreload) 
      {
        for (quint32 i=0; i<NumSystemAddressESLPorts(); i++)
        {
          xml.addElement("debugAccess");
          QString value = QString("%1").arg(getSystemAddressESLPortBaseAddress(i));
          UtString hv; hv << value;
          xml.addAttribute("baseAddress", hv.c_str());
          xml.addElement("debugPort");
          xml.addAttribute("name", getSystemAddressESLPortName(i));
          xml.closeElement(); // debugPort
          xml.closeElement(); // debugAccess
        }
      }

      UtString displayAtZero;
      displayAtZero = mDisplayAtZero ? "true" : "false";
      xml.addElement("displayAtZero", displayAtZero.c_str());
      xml.closeElement();

      // Write Memory Blocks
      for(UInt32 i = 0; i < mBlocks.size(); ++i)
        mBlocks[i]->write(xml);

      // Custom codes
      writeCustomCodes(xml);

      xml.closeElement(); // memSpace
    }
  }

CarbonCfgMemoryBlock* CarbonCfgMemory::addMemoryBlock()
{
  CarbonCfgMemoryBlock* block = new CarbonCfgMemoryBlock(mWidth);
  mBlocks.push_back(block);

  return block;
}

void CarbonCfgMemory::removeMemoryBlock(CarbonCfgMemoryBlock* block)
{
  mBlocks.remove(block);
  delete block;
}

void CarbonCfgMemory::insertMemoryBlockUndo(int pos, CarbonCfgMemoryBlock* block)
{
  UtArray<CarbonCfgMemoryBlock*> temp;
  temp.push_back(block);
  mBlocks.insert(pos, temp.begin(), temp.end());
}
int CarbonCfgMemory::removeMemoryBlockUndo(CarbonCfgMemoryBlock* block) {
  for(UInt32 i=0; i<mBlocks.size(); i++)
  {
    CarbonCfgMemoryBlock* b = mBlocks[i];
    if (b == block)
    {
      mBlocks.remove(b);       // order N
      return i;
    }
  }
  return -1;
}


CarbonCfgMemoryBlock* CarbonCfgMemory::getMemoryBlock(UInt32 index) const
{
  if(index < mBlocks.size())
    return mBlocks[index];
  else
    return NULL;
}

UInt64 CarbonCfgMemory::getMaxAddrs() const
{
  return mMaxAddrs;
}

void CarbonCfgMemory::putMaxAddrs(UInt64 maxAddrs)
{
  mMaxAddrs = maxAddrs;
}

CarbonCfgMemory* CarbonCfgMemory::clone(CarbonCfg* parent, const CarbonCfgTemplate& templ) const
{
  // Convert the name and convert the variables
  UtString name;
  templ.resolveString(getName(), &name);

  // Construct a new object
  CarbonCfgMemory* mem = new CarbonCfgMemory(parent, name.c_str(), getWidth(),
                                             getMaxAddrs());

  // Initialize the fields not handled by the constructor. Convert any strings
  templ.resolveString(getInitFile(), &mem->mInitFile);
  templ.resolveString(getComment(), &mem->mComment);

  for (quint32 pi=0; pi<(quint32)mSystemAddressESLPortNames.count(); pi++)
  {
    UtString* port = mSystemAddressESLPortNames[pi];
    qint64 baseAddress = mSystemAddressESLPortBaseAddresses[pi];
    mem->addSystemAddressESLPort((*port).c_str(), baseAddress);
  }

  mem->mReadmemType = mReadmemType;
  mem->mMemInitType = mMemInitType;
  templ.resolveString(getMemoryDisassemblyName(), &mem->mDisassemblyName);
  templ.resolveString(getMemoryDisassemblerName(), &mem->mDisassemblerName);
  mem->mDisplayAtZero = mDisplayAtZero;
  mem->mMAU = mMAU;
  mem->mBigEndian = mBigEndian;
  mem->mProgramMemory = mProgramMemory;

  // Clone the blocks
  for (UInt32 i = 0; i < numMemoryBlocks(); ++i) {
    CarbonCfgMemoryBlock* block = getMemoryBlock(i);
    CarbonCfgMemoryBlock* newBlock = block->clone(templ);
    mem->mBlocks.push_back(newBlock);
  }

  // Clone the custom code sections
  for (CarbonUInt32 i = 0; i < numCustomCodes(); ++i) {
    CarbonCfgCustomCode* cc = getCustomCode(i);
    CarbonCfgMemoryCustomCode* memCc = cc->castMemory();
    CarbonCfgMemoryCustomCode* newCc = memCc->clone(templ);
    mem->CarbonCfgCustomCodeContainer::addCustomCode(newCc);
  }

  return mem;
}

void CarbonCfgMemory::putSystemAddressESLPortBaseAddress(quint32 index, qint64 baseAddress)
{
  mSystemAddressESLPortBaseAddresses[index] = baseAddress;
}

qint64 CarbonCfgMemory::getSystemAddressESLPortBaseAddress(quint32 index) const
{
  return mSystemAddressESLPortBaseAddresses[index];
}

// Deprecated (left for compatability)
const char* CarbonCfgMemory::getSystemAddressESLPortName() const
{
  if (mSystemAddressESLPortNames.count() > 0)
  {
    UtString* value = mSystemAddressESLPortNames[0];
    return (*value).c_str();
  }

  return NULL;
}

// Replaced getSystemAddressESLPortName()
const char* CarbonCfgMemory::getSystemAddressESLPortName(quint32 index) const
{
  SCRIPT_ASSERT_RETURN(index < (quint32)mSystemAddressESLPortNames.count(), "getSystemAddressESLPortName index out of range", NULL);

  if (index < (quint32)mSystemAddressESLPortNames.count())
  {
    UtString* value = mSystemAddressESLPortNames[index];
    return (*value).c_str();
  }

  return NULL;
}

void CarbonCfgMemory::check(CarbonCfg* cfg, CarbonDB* db) const
{
  // Clear any memory errors
  mMemoryMessages.clearMessages();

  // We don't support big endian mode yet
  if (getBigEndian()) {
    UtString msg;
    msg << "Big Endian mode not supported.";
    reportWarning(cfg, msg);
  }

  // First walk all blocks and check them individually. This will
  // clear any state from a previous run
  for (UInt32 i = 0; i < numMemoryBlocks(); ++i) {
    CarbonCfgMemoryBlock* block = getMemoryBlock(i);
    block->check(cfg, getName(), getMaxAddrs(), db);
  }

  // Now do block overlap tests. This is N*N
  for (UInt32 i = 0; i < numMemoryBlocks(); ++i) {
    CarbonCfgMemoryBlock* block1 = getMemoryBlock(i);
    UInt64 base1 = block1->getBase();
    UInt64 end1 = block1->getBase() + block1->getSize() - 1;
    for (UInt32 j = i + 1; j < numMemoryBlocks(); ++j) {
      CarbonCfgMemoryBlock* block2 = getMemoryBlock(j);
      UInt32 base2 = block2->getBase();
      UInt64 end2 = block2->getBase() + block2->getSize() - 1;
      if (((base2 >= base1) && (base2 <= end1)) ||
          ((end2 >= base1) && (end2 <= end1))) {
        UtString msg;
        msg << "Block: " << block1->getName() << " [" << end1 << ":" << base1 << "] overlaps Block2: "
            << block2->getName() << " [" << end2 << ":" << base2 << "].";
        block1->reportWarning(cfg, getName(), msg);
        block2->reportWarning(NULL, getName(), msg);
      }
    }
  }
} // void CarbonCfgMemory::check

void CarbonCfgMemory::putWidth(UInt32 width)
{
  // Store it here and any underlying blocks. The blocks keep the width for convenience
  mWidth = width;
  for (UInt32 i = 0; i < numMemoryBlocks(); ++i) {
    CarbonCfgMemoryBlock* block = getMemoryBlock(i);
    block->putWidth(width);
  }
}

// --------------------------------------------------------------------------
//   Legacy Methods
//
const char* CarbonCfgMemory::getPath() const 
{
  // Get the path from the locator in the first block
  if(!mBlocks.empty()) {
    if(mBlocks[0]->numLocs() > 0) {
      CarbonCfgMemoryLocRTL* rtlLoc = mBlocks[0]->getLoc(0)->castRTL();
      if(rtlLoc) {
        return rtlLoc->getPath();
      }
    }
  }
  // There are no RTL memories
  return "";
}

UInt64 CarbonCfgMemory::getBaseAddr() const
{
  // Since this method is only used for legacy cfg's, base address is the base of
  // the first and only block
  if(!mBlocks.empty())
    return mBlocks[0]->getBase();
  
  // There are no Memory Blocks
  return 0;
}

UInt32 CarbonCfgMemory::getBaseAddrLo() const
{
  UInt32 word;
  UInt64 baseAddr = getBaseAddr();
  CarbonValRW::cpSrcToDestWord(&word, &baseAddr, 0);
  return word;
}

UInt32 CarbonCfgMemory::getBaseAddrHi() const
{
  UInt32 word;
  UInt64 baseAddr = getBaseAddr();
  CarbonValRW::cpSrcToDestWord(&word, &baseAddr, 1);
  return word;
}

void CarbonCfgMemory::putBaseAddr(UInt64 value)
{
  // Since this method is only used for legacy cfg's, base address is the base of
  // the first and only block
  if(!mBlocks.empty())
    mBlocks[0]->putBase(value);
}

// --------------------------------------------------------------------------

CarbonCfgCustomCode::CarbonCfgCustomCode() : mPosition(eCarbonCfgPre),mElementGenerationType(eCarbonCfgElementUser)
{
}

CarbonCfgCustomCode::~CarbonCfgCustomCode()
{
}

void CarbonCfgCustomCode::putCode(const char* code)
{
  mCode = code;
}

void CarbonCfgCustomCode::write(CfgXmlWriter& xml)
{
  bool debugEmbeddedModels = false;
  if (getElementGenerationType() == eCarbonCfgElementGenerated && getenv("CARBON_DEBUG_EMBEDDED_MODELS"))
    debugEmbeddedModels = true;

  if (debugEmbeddedModels || getElementGenerationType() == eCarbonCfgElementUser)
  {
    if (debugEmbeddedModels)
      xml.addElement("debugCustomcode");
    else
      xml.addElement("customcode");

    xml.addAttribute("section", getSectionString());

    // Convert the position to a string
    const char* positionStr = NULL;
    switch(mPosition) {
      case eCarbonCfgPre:
        positionStr = "PRE";
        break;
      case eCarbonCfgPost:
        positionStr = "POST";
        break;
    }

    // Write the XML
    xml.addAttribute("position", positionStr);
    xml.addCdata(mCode.c_str());
    
    xml.closeElement(); // customcode
  }
}

void CarbonCfgCustomCodeContainer::writeCustomCodes(CfgXmlWriter& xml)
{
  // Write custom code sections
  for (UInt32 i = 0; i < mCustomCodeVec.size(); ++i) {
    mCustomCodeVec[i]->write(xml);
  }
}

void CarbonCfgCustomCodeContainer::removeCustomCodes(void)
{
  mCustomCodeVec.clearPointers();
}

void CarbonCfgCustomCodeContainer::clearCustomCodes(void)
{
  mCustomCodeVec.clearPointers();
}

CarbonCfgCompCustomCode::CarbonCfgCompCustomCode() :
  CarbonCfgCustomCode()
{
  putSection(eCarbonCfgCompClass);
}

CarbonCfgCompCustomCode::~CarbonCfgCompCustomCode()
{
}

void
CarbonCfgCompCustomCode::putSection(CarbonCfgCompCustomCodeSection section)
{
  CarbonCfgCompCustomCodeSectionIO sectIO = section;
  putSectionString(sectIO.getString()); 
}

//! Get the section as an enum
CarbonCfgCompCustomCodeSection CarbonCfgCompCustomCode::getSection(void) const
{ 
  CarbonCfgCompCustomCodeSectionIO section;
  section.parseString(getSectionString());
  return section.getEnum(); 
}

CarbonCfgCompCustomCode* 
CarbonCfgCompCustomCode::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the custom code and copy the section and postion
  CarbonCfgCompCustomCode* cc = new CarbonCfgCompCustomCode();
  cc->cloneInPlace(*this, templ);
  return cc;
}

CarbonCfgCadiCustomCode::CarbonCfgCadiCustomCode() :
  CarbonCfgCustomCode()
{
  putSection(eCarbonCfgCadiConstructor);
}

CarbonCfgCadiCustomCode::~CarbonCfgCadiCustomCode()
{
}

void
CarbonCfgCadiCustomCode::putSection(CarbonCfgCadiCustomCodeSection section)
{
  CarbonCfgCadiCustomCodeSectionIO sectIO = section;
  putSectionString(sectIO.getString()); 
}

//! Get the section as an enum
CarbonCfgCadiCustomCodeSection CarbonCfgCadiCustomCode::getSection(void) const
{ 
  CarbonCfgCadiCustomCodeSectionIO section;
  section.parseString(getSectionString());
  return section.getEnum(); 
}

CarbonCfgCadiCustomCode* 
CarbonCfgCadiCustomCode::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the custom code and copy the section and postion
  CarbonCfgCadiCustomCode* cc = new CarbonCfgCadiCustomCode();
  cc->cloneInPlace(*this, templ);
  return cc;
}

// --------------------------------------------------------------------------
CarbonCfgRegCustomCode::CarbonCfgRegCustomCode() :
  CarbonCfgCustomCode()
{
  putSection(eCarbonCfgRegClass);
}

CarbonCfgRegCustomCode::~CarbonCfgRegCustomCode()
{
}

void
CarbonCfgRegCustomCode::putSection(CarbonCfgRegCustomCodeSection section)
{
  CarbonCfgRegCustomCodeSectionIO sectIO = section;
  putSectionString(sectIO.getString()); 
}

//! Get the section as an enum
CarbonCfgRegCustomCodeSection CarbonCfgRegCustomCode::getSection(void) const
{ 
  CarbonCfgRegCustomCodeSectionIO section;
  section.parseString(getSectionString());
  return section.getEnum(); 
}

CarbonCfgRegCustomCode*
CarbonCfgRegCustomCode::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the custom code and copy the section and postion
  CarbonCfgRegCustomCode* cc = new CarbonCfgRegCustomCode();
  cc->cloneInPlace(*this, templ);
  return cc;
}

// --------------------------------------------------------------------------

CarbonCfgMemoryBlockCustomCode::CarbonCfgMemoryBlockCustomCode() :
  CarbonCfgCustomCode()
{
  putSection(eCarbonCfgMemoryBlockClass);
}

CarbonCfgMemoryBlockCustomCode::~CarbonCfgMemoryBlockCustomCode()
{
}

//! Put the section
void CarbonCfgMemoryBlockCustomCode::putSection(CarbonCfgMemoryBlockCustomCodeSection section)
{
  CarbonCfgMemoryBlockCustomCodeSectionIO sectIO = section;
  putSectionString(sectIO.getString()); 
}

//! Get the section as an enum
CarbonCfgMemoryBlockCustomCodeSection CarbonCfgMemoryBlockCustomCode::getSection(void) const
{ 
  CarbonCfgMemoryBlockCustomCodeSectionIO section;
  section.parseString(getSectionString());
  return section.getEnum(); 
}

CarbonCfgMemoryBlockCustomCode*
CarbonCfgMemoryBlockCustomCode::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the custom code and copy the section and postion
  CarbonCfgMemoryBlockCustomCode* cc = new CarbonCfgMemoryBlockCustomCode();
  cc->cloneInPlace(*this, templ);
  return cc;
}

static bool absolutePath(const char* fn)
{
  int l = strlen(fn);
#if pfWINDOWS
  if (l >=2 && fn[1] == ':' || fn[0] == '/' || fn[0] == '\\')
    return true;
#else
  if (l > 0 && fn[0] == '/')
    return true;
#endif
  return false;
}

static bool fileExists(const char* fn)
{
  OSStatEntry statEntry;
  UtString statErrMsg;
  OSStatFileEntry(fn, &statEntry, &statErrMsg);
  return statEntry.exists();
}

// --------------------------------------------------------------------------

CarbonCfgMemoryCustomCode::CarbonCfgMemoryCustomCode() :
  CarbonCfgCustomCode()
{
  putSection(eCarbonCfgMemoryClass);
}

CarbonCfgMemoryCustomCode::~CarbonCfgMemoryCustomCode()
{
}

//! Put the section
void CarbonCfgMemoryCustomCode::putSection(CarbonCfgMemoryCustomCodeSection section)
{
  CarbonCfgMemoryCustomCodeSectionIO sectIO = section;
  putSectionString(sectIO.getString()); 
}

//! Get the section as an enum
CarbonCfgMemoryCustomCodeSection CarbonCfgMemoryCustomCode::getSection(void) const
{ 
  CarbonCfgMemoryCustomCodeSectionIO section;
  section.parseString(getSectionString());
  return section.getEnum(); 
}

CarbonCfgMemoryCustomCode*
CarbonCfgMemoryCustomCode::clone(const CarbonCfgTemplate& templ) const
{
  // Clone the custom code and copy the section and postion
  CarbonCfgMemoryCustomCode* cc = new CarbonCfgMemoryCustomCode();
  cc->cloneInPlace(*this, templ);
  return cc;
}

void CarbonCfg::parseFeatures(QXmlStreamReader& xr, UtString& /*lineErr*/, CarbonCfgErrorHandler* /*err*/)
{
  while (xr.readNextStartElement())
  {
    UtString line;
    line << xr.name();
    CarbonCfgFeatureIO feature;
    feature.parseString(line.c_str());
    mFeatureMask |= 1 << feature.getEnum();
    xr.skipCurrentElement();
  }
}

CarbonCfgStatus CarbonCfg::parseXMLCcfg(QXmlStreamReader& xr, UtString& lineErr, CarbonCfgErrorHandler* err, UtParamFile& f)
{
  UInt32 ticksPerBeat = 0; 

  while (xr.readNextStartElement())
  {
    if ("Features" == xr.name())
      parseFeatures(xr, lineErr, err);
    else if ("Platform" == xr.name())
    {
      CarbonCfgModeIO cfgMode;
      UtString line; line << xr.readElementText();
      cfgMode.parseString(line.c_str());
      mMode = static_cast<Mode>(cfgMode.getEnum());
    }
    else if ("Component" == xr.name())
      mCompName = QUSTR( xr.readElementText() );
    else if ("TopModule" == xr.name())
      mTopModuleName = QUSTR( xr.readElementText() );
    else if ("Library" == xr.name()) 
      mLibName = QUSTR( xr.readElementText() );
    else if ("LegacyMemories" == xr.name()) 
      mLegacyMemories = xr.readElementText().toInt();
    else if ("Description" == xr.name()) 
      mDescription = QUSTR( xr.readElementText() );
    else if ("CxxFlags" == xr.name()) 
      mCxxFlags = QUSTR( xr.readElementText() );
    else if ("LinkFlags" == xr.name()) 
      mLinkFlags = QUSTR( xr.readElementText() );
    else if ("SourceFiles" == xr.name())
      mSourceFiles = QUSTR( xr.readElementText() );
    else if ("IncludeFiles" == xr.name())
      mIncludeFiles = QUSTR( xr.readElementText() );
    else if ("LoadFileExtension" == xr.name())
      mLoadfileExtension = QUSTR( xr.readElementText() );
    else if ("UseStaticScheduling" == xr.name())
      mUseStaticScheduling = xr.readElementText().toInt();
    else if ("TicksPerBeat" == xr.name())
      ticksPerBeat = xr.readElementText().toUInt();
    else if ("RtlPort" == xr.name())
    {
      CarbonCfgRTLPort* port = new CarbonCfgRTLPort(xr, this, f, err, ticksPerBeat);
      if (mRTLPortMap.find(port->getName()) == mRTLPortMap.end()) {
        mRTLPortVec.push_back(port);
        mRTLPortMap[port->getName()] = port;
      }
      else {
        lineErr << "Duplicate rtlPort name: " << port->getName();
        delete port;
      }
    }
    else if ("Xtor" == xr.name())
    {
      if (!mReadXtorLibrary)
      {
        switch (mMode)
        {
        case eRvsocd:
          readXtorLib(eCarbonXtorsMaxsim);
          break;
        case ePlatarch:
          readXtorLib(eCarbonXtorsCoware);
          break;
        case eSystemC:
          readXtorLib(eCarbonXtorsSystemC);
          break;
        case eUnknown:
          lineErr << "Error: Xtor library not read because configuration has an unknown type";
          break;
        }
      }

      // Create the xtor instance but becareful because if it fails it
      // still returns a valid pointer. Test this by making sure we
      // got a valid xtor type.
      CarbonCfgXtorInstance* xi = new CarbonCfgXtorInstance(xr, this);
      if (xi->getType() == NULL) {
        lineErr = QUSTR(xr.errorString());
      }

      else if (mESLPortNames.insertWithCheck(xi->getName())) {
        mXtorVec.push_back(xi);
        mXtorMap[xi->getName()] = xi;
      }
      else {
        lineErr << "Duplicate ESL port name " << xi->getName();
        delete xi;
      }
    }
    else if ("ParamDef" == xr.name())
    {
      // OK, we've used the QXmlStreamReader to this point, unfortunately
      // there is a ton of code which uses libxml2 to parse the rest.
      // mCcfgFilePath, &mCcfgFileName
      UtString xfilepath;
      if (mCcfgFilePath.length() > 0)
        xfilepath << mCcfgFilePath << "/" << mCcfgFileName;
      else
        xfilepath << mCcfgFileName;

      QFile xdfile(xfilepath.c_str());
      if (xdfile.open(QFile::ReadOnly | QFile::Text))
      {
        QDomDocument doc;
        int errorLine;
        int errorCol;
        QString errStr;
        if (doc.setContent(&xdfile, true, &errStr, &errorLine, &errorCol))
        { 
          QDomElement rootElem = doc.documentElement();
          QDomElement paramdefNode = rootElem.firstChildElement("ParamDef");
          if (paramdefNode.isElement())
          {
            QString contents;
            QTextStream ts(&contents);
            ts << paramdefNode;
            UtString xmlcontent = QUSTR(contents);
            CfgXmlParserXtor parser;
            if (!parser.parseParameters(xmlcontent.c_str(), this))
              lineErr = parser.errorText();      

            xr.skipCurrentElement();
          }
        }
      }
    }
    else if ("componentinfo" == xr.name())
    {
      // OK, we've used the QXmlStreamReader to this point, unfortunately
      // there is a ton of code which uses libxml2 to parse the rest.
      // mCcfgFilePath, &mCcfgFileName
      UtString xfilepath;
      if (mCcfgFilePath.length() > 0)
        xfilepath << mCcfgFilePath << "/" << mCcfgFileName;
      else
        xfilepath << mCcfgFileName;

      QFile xdfile(xfilepath.c_str());
      if (xdfile.open(QFile::ReadOnly | QFile::Text))
      {
        QDomDocument doc;
        int errorLine;
        int errorCol;
        QString errStr;
        if (doc.setContent(&xdfile, true, &errStr, &errorLine, &errorCol))
        { 
          QDomElement rootElem = doc.documentElement();
          QDomElement componentNode = rootElem.firstChildElement("componentinfo");
          if (componentNode.isElement())
          {
            QString contents;
            QTextStream ts(&contents);
            ts << componentNode;
            UtString xmlcontent = QUSTR(contents);
            CfgXmlParserComp parser;
            if (!parser.parseComponent(xmlcontent.c_str(), this))
              lineErr = parser.errorText();      

            xr.skipCurrentElement();
          }
        }
      }
      else
      {
        UtString errMessage;
        errMessage <<  "ERROR: Unable to open: " << xfilepath;
        qDebug() << errMessage.c_str();
        INFO_ASSERT(true,  errMessage.c_str());
      }
    }
    else if ("ProfileStream" == xr.name())
    {
      CarbonCfgPStream* pstream = new CarbonCfgPStream(xr, f, err);
      if (mPStreamNames.insertWithCheck(pstream->getName())) {
        mPStreamVec.push_back(pstream);
      }
      else
      {
        lineErr << "Duplicate PStream name: " << pstream->getName();
        delete pstream;
      }
    }
    else if ("FSDB" == xr.name())
    {
      mWaveType = eCarbonCfgFSDB;
      mWaveFile = QUSTR(xr.readElementText());
    }
    else if ("VCD" == xr.name())
    {
      mWaveType = eCarbonCfgVCD;
      mWaveFile = QUSTR(xr.readElementText());
    }
    else if ("IODB" == xr.name())
    {     
      mIODBFile = QUSTR(xr.readElementText());

      // Test to see if this file exists
      if (!fileExists(mIODBFile.c_str()))
      {
        UtString filePath;
        UtString fileName;
        OSParseFileName(mIODBFile.c_str(), &filePath, &fileName);
        UtString baseName = fileName;
        size_t dot = baseName.find_first_of('.');
        if (dot != UtString::npos)
          baseName.erase(dot);

        UtString guiDbFile; guiDbFile << filePath << "/" << baseName << ".gui.db";
        UtString ioDbFile; ioDbFile << filePath << "/" << baseName << ".io.db";
        UtString symtabDbFile; symtabDbFile << filePath << "/" << baseName << ".symtab.db";

        if (fileExists(ioDbFile.c_str()))
          mIODBFile = ioDbFile;
        else if (fileExists(symtabDbFile.c_str()))
          mIODBFile = symtabDbFile;
        else if (fileExists(guiDbFile.c_str()))
          mIODBFile = guiDbFile;
      }
    }
    else
    {
      UtString errMessage;
      errMessage <<  "ERROR: Unhandled Keyword:" << xr.name();
      qDebug() << errMessage.c_str();
      INFO_ASSERT(true,  errMessage.c_str());
      xr.skipCurrentElement();
    }
  }

  return eCarbonCfgSuccess;
}

// --------------------------------------------------------------------------
CarbonCfgStatus CarbonCfg::readNoCheck(const char* filename) {
  UInt32 ticksPerBeat = 0; 

  UtParamFile f(filename);
  if (!f.is_open()) {
    mMsgHandler.putMsg(f.getErrmsg());
    return eCarbonCfgFailure;
  }

  OSParseFileName(filename, &mCcfgFilePath, &mCcfgFileName);

  UtHashMap<UtString,CarbonCfgGroup*> groupMap;
  UtString buf;
  mMsgHandler.clear();
  CarbonCfgErrorHandler err(&f, &mMsgHandler);
  UtString lineErr;

  // Format version of file being read.  Versions prior to 5 did not get
  // a keyword in the file, so anything older than 5 will leave this 
  // value at 0.
  mVersion = 0;
  mFeatureMask = 0;

  // Is this a new XML file?
  bool isXMLFormattedFile = false;
  QFile file(filename);
  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QXmlStreamReader xr;
    xr.setDevice(&file);

    if (xr.readNextStartElement())
    {
      if (xr.name() == "CarbonComponentConfiguration")
      {
        isXMLFormattedFile = true;
        mVersion = xr.attributes().value("version").toString().toUInt();
        if (mVersion > CFG_VERSION)
        {
          lineErr << "The XML format version number (" << mVersion
                  << ") of file " << filename
                  << " is too new; only files of version " << CFG_VERSION
                  << " or older can be read.";
        }
        else
          parseXMLCcfg(xr, lineErr, &err, f);
      }
    }
    file.close();
  }

  if (!isXMLFormattedFile)
  {
    bool traceCcfgReader = getenv("CARBON_TRACE_CCFG_READER");
    UtIStringStream line;
    while (f.getline(&line)) {
      UtString kwd, portName;
      line >> kwd;

      if (traceCcfgReader) {
        UtIO::cout() << "parsing line with kwd '" << kwd << "'\n";
      }

      if (kwd == "version") {
        if (line >> mVersion) {
          if (mVersion > CFG_VERSION) {
            lineErr << "The format version number (" << mVersion
                    << ") of file " << filename
                    << " is too new; only files of version " << CFG_VERSION
                    << " or older can be read.";
          }
        }
      }
      else if (kwd == "platform") {
        CarbonCfgModeIO cfgMode;
        line >> cfgMode;
        mMode = static_cast<Mode>(cfgMode.getEnum());
      }
      else if (kwd == "features") {
        CarbonCfgFeatureIO feature;
        while (line >> feature) {
          mFeatureMask |= 1 << feature.getEnum();
        }
        if (line.eof()) {
          // Ignore inevitable "no more tokens" error at end of line
          (void) line.getErrmsg();
        }
      }
      else if (kwd == "rtlPort") {
        CarbonCfgRTLPort* port = new CarbonCfgRTLPort(line, this, f, &err, ticksPerBeat);
        if (mRTLPortMap.find(port->getName()) == mRTLPortMap.end()) {
          mRTLPortVec.push_back(port);
          mRTLPortMap[port->getName()] = port;
        }
        else {
          lineErr << "Duplicate rtlPort name: " << port->getName();
          delete port;
        }
      }
      else if (kwd == "connect") {
        // what port are we looking for?
        UtString portName;
        if (line >> portName) {
          RTLPortMap::iterator p = mRTLPortMap.find(portName);
          if (p == mRTLPortMap.end()) {
            lineErr << "Cannot find port " << portName;
          }
          else {
            CarbonCfgRTLPort* port = p->second;
            CarbonCfgRTLConnection* conn =
              CarbonCfgRTLConnection::read(port, line, this, &err, ticksPerBeat);
            if (conn != NULL) {
              port->connect(conn);
            }
          }
        }
        else {
          lineErr = line.getErrmsg();
        }
      }
      else if (kwd == "xtor") {
        if (!mReadXtorLibrary)
        {
          switch (mMode)
          {
          case eRvsocd:
            readXtorLib(eCarbonXtorsMaxsim);
            break;
          case ePlatarch:
            readXtorLib(eCarbonXtorsCoware);
            break;
          case eSystemC:
            readXtorLib(eCarbonXtorsSystemC);
            break;
          case eUnknown:
            lineErr << "Error: Xtor library not read because configuration has an unknown type";
            break;
          }
        }

        // Create the xtor instance but becareful because if it fails it
        // still returns a valid pointer. Test this by making sure we
        // got a valid xtor type.
        CarbonCfgXtorInstance* xi = new CarbonCfgXtorInstance(line, this);
        if (xi->getType() == NULL) {
          lineErr = line.getErrmsg();
        }

        else if (mESLPortNames.insertWithCheck(xi->getName())) {
          mXtorVec.push_back(xi);
          mXtorMap[xi->getName()] = xi;
        }
        else {
          lineErr << "Duplicate ESL port name " << xi->getName();
          delete xi;
        }
      }
      else if (kwd == "xtorConn") {
        // an xtor connection will show up outside the context of an
        // RTL port only if it is not yet connected to an RTL port.
        // Starting in version 8, we write out these ports to capture
        // expressions, which may tie them to a constant
        CarbonCfgXtorConn* conn = new CarbonCfgXtorConn(NULL, line, this);
        if(conn->valid()) conn->connect();        // attach it to its xtor instance
        else delete conn;

      } else if (kwd == "ticksPerBeat") {
        line >> ticksPerBeat;
      }
      else if (kwd == "component") {
        line >> mCompName;
      }
      else if (kwd == "topModule") {
        line >> mTopModuleName;
      }
      else if (kwd == "iodb") {
        line >> mIODBFile;

        // Test to see if this file exists
        if (!fileExists(mIODBFile.c_str()))
        {
          UtString filePath;
          UtString fileName;
          OSParseFileName(mIODBFile.c_str(), &filePath, &fileName);
          UtString baseName = fileName;
          size_t dot = baseName.find_first_of('.');
          if (dot != UtString::npos)
            baseName.erase(dot);

          UtString guiDbFile; guiDbFile << filePath << "/" << baseName << ".gui.db";
          UtString ioDbFile; ioDbFile << filePath << "/" << baseName << ".io.db";
          UtString symtabDbFile; symtabDbFile << filePath << "/" << baseName << ".symtab.db";

          if (fileExists(ioDbFile.c_str()))
            mIODBFile = ioDbFile;
          else if (fileExists(symtabDbFile.c_str()))
            mIODBFile = symtabDbFile;
          else if (fileExists(guiDbFile.c_str()))
            mIODBFile = guiDbFile;
        }
      }
      else if (kwd == "library") {
        line >> mLibName;
      }
      else if (kwd == "legacymems") {
        line >> mLegacyMemories;
      }
      else if (kwd == "description") {
        line >> mDescription;
      }
      else if (kwd == "cxxflags") {
        line >> mCxxFlags;
      }
      else if (kwd == "linkflags") {
        line >> mLinkFlags;
      }
      else if (kwd == "sourcefiles") {
        line >> mSourceFiles;
      }
      else if (kwd == "includefiles") {
        line >> mIncludeFiles;
      }
      else if (kwd == "loadfileext") {
        line >> mLoadfileExtension;
      }
      else if (kwd == "usestaticscheduling") {
        line >> mUseStaticScheduling;
      }
      else if (kwd == "register") {
        // Old register format, versions 11 and earlier

        UtString groupName;
        CarbonCfgRegister* reg = new CarbonCfgRegister(line, &groupName);
        CarbonCfgGroup* group = groupMap[groupName];
        if (group == NULL) {
          group = addGroup(groupName.c_str());
          groupMap[groupName] = group;
        }
        if (mRegNames.insertWithCheck(reg->getName())) {
          reg->putGroup(group);
          mRegisterVec.push_back(reg);
        }
        else {
          lineErr << "Duplicate Register name: " << reg->getName();
          delete reg;
        }
      }
      else if (kwd == "xmlregisters") {
        // New XML register format, versions 12 and later

        CfgXmlParserReg parser;
        if (!(parser.parseStream(f, this)))
          lineErr << parser.errorText();

      }
      else if (kwd == "xmlmemories") {
        // New XML memory format, versions 16 and later

        CfgXmlParserMem parser;
        if (!(parser.parseStream(f, this)))
          lineErr << parser.errorText();

      }
      else if (kwd == "xmlparameters") {
        // New XML parameters format, versions 14 and later

        CfgXmlParserXtor parser;
        if (!(parser.parseStream(f, this)))
          lineErr << parser.errorText();

      }
      else if (kwd == "xmlcomponents") {
        CfgXmlParserComp parser;

        if (!(parser.parseStream(f, this))) {
          lineErr << parser.errorText();
        }
      }
  #ifdef CARBON_PV
      else if (kwd == "xmltransactions") {
        // New XML Transaction ID Specification
        SCRIPT_ASSERT(mPVContext, "Cannot parse transactions before calling CarbonCfg::putPVContext");

        CfgXmlParserPSD parser(mPVContext);
        if (!(parser.parseStream(f)))
          lineErr << parser.errorText();
      }
  #endif

      else if (kwd == "memory") {
        // Old Format, version 15 or older
        CarbonCfgMemory* mem = new CarbonCfgMemory(line, this);

        // If it has disassembly, let the addMemory method figure out how to add it
        if(strcmp("", mem->getMemoryDisassemblyName()) != 0) {
          addMemory(mem->getPath(), mem->getName(), mem->getInitFile(), mem->getMaxAddrs(), 
                    mem->getWidth(), mem->getReadmemType(), mem->getComment(),
                    mem->getMemoryDisassemblyName(), mem->getMemoryDisassemblerName());
          delete mem;
        }
        else {
          if (mMemNames.insertWithCheck(mem->getName())) {
            mMemoryVec.push_back(mem);
          }
          else {
            lineErr << "Duplicate Memory name: " << mem->getName();
            delete mem;
          }
        }
      }
      else if (kwd == "pstream") {
        CarbonCfgPStream* pstream = new CarbonCfgPStream(line, f, &err);
        if (mPStreamNames.insertWithCheck(pstream->getName())) {
          mPStreamVec.push_back(pstream);
        }
        else {
          lineErr << "Duplicate PStream name: " << pstream->getName();
          delete pstream;
        }
      }
      else if (kwd == "vcd") {
        mWaveType = eCarbonCfgVCD;
        line >> mWaveFile;
      }
      else if (kwd == "fsdb") {
        mWaveType = eCarbonCfgFSDB;
        line >> mWaveFile;
      }
      else if (kwd == "parameter") {
        addParameter(line);
      }
      else {
        lineErr << "Unexpected keyword: " << kwd;
      }
      if (line.bad()) {
        err.reportError(line.getErrmsg()) ;
      }

      if (! lineErr.empty()) {
        err.reportError(lineErr.c_str());
        lineErr.clear();
      }
    } // while
  } // parsing old .ccfg files (non-xml)

  // for backward compatibility, set top module to component name if no 
  // topModule keyword was found.
  if (mTopModuleName == "") {
    mTopModuleName = mCompName;
  }

  // resolution after read.
  for (UInt32 i=0; i<numXtorInstances(); i++)
  {
    CarbonCfgXtorInstance* xtorInst = getXtorInstance(i);
    for (UInt32 p=0; p<xtorInst->numParams(); p++)
    {
      CarbonCfgXtorParamInst* paramInst = xtorInst->getParamInstance(p, true);  // handle both hidden and non-hidden parameters
      for (UInt32 j=0; j<numRTLPorts(); j++)
      {
        CarbonCfgRTLPort* port = getRTLPort(j);
        for (UInt32 c=0; c<port->numConnections(); c++)
        {
          CarbonCfgRTLConnection* conn = port->getConnection(c);

          CarbonCfgTieParam* tieParam = conn->castTieParam();
          if (tieParam && 
              tieParam->mParam == paramInst->getParam()->getName() &&
              tieParam->mXtorInstanceName == xtorInst->getName())
            paramInst->addRTLPort(port);
          
          CarbonCfgESLPort* eslPort = conn->castESLPort();
          if (eslPort && (strlen(eslPort->getParamName()) > 0))
          {
            bool paramNamesMatch = 0 == strcmp(eslPort->getParamName(),
                                          paramInst->getParam()->getName());
            bool xtorNamesMatch = 0 == strcmp(eslPort->getParamXtorInstName(),
                                              xtorInst->getName());
            if (paramNamesMatch && xtorNamesMatch)
            {
              eslPort->putParamInstance(paramInst);
            }
          }
        }
      }
    }
  }
  // Fixup connections from the Parameter instance back to the RTL port
  // For Global Parameters
  // also fixup parameters hanging off of ESLPorts
  for (UInt32 i=0; i<numParams(); i++)
  {
    CarbonCfgXtorParamInst* paramInst = getParam(i);
    // Locate the RTL port attached to this
    for (UInt32 j=0; j<numRTLPorts(); j++)
    {
      CarbonCfgRTLPort* port = getRTLPort(j);
      for (UInt32 c=0; c<port->numConnections(); c++)
      {
        CarbonCfgRTLConnection* conn = port->getConnection(c);
        CarbonCfgTieParam* tieParam = conn->castTieParam();
        if (tieParam && tieParam->mParam == paramInst->getParam()->getName())
          paramInst->addRTLPort(port);

        CarbonCfgESLPort* eslPort = conn->castESLPort();
        if (eslPort && (strlen(eslPort->getParamName()) > 0))
        {
          if (0 == strcmp(eslPort->getParamName(),
                          paramInst->getParam()->getName()))
          {
            eslPort->putParamInstance(paramInst);
          }
        }
      }
    }
  }


  // For CoWare, remove ESL ports that are also connected to
  // transactor ports. We only allow one connection with the new
  // GUI.
  if (mMode == ePlatarch) {
    for (UInt32 r = 0; r < numRTLPorts(); ++r) {
      // Look for both an ESL and xtor connection or multiple ESL
      // connections.  We do this in two passes because we can't disconnect while
      // walking.
      CarbonCfgRTLPort* rtlPort = getRTLPort(r);
      CarbonCfgESLPort* eslPort = NULL;
      CarbonCfgXtorConn* xtorConn = NULL;
      UtArray<CarbonCfgESLPort*> disconnects;
      for (UInt32 c = 0; c < rtlPort->numConnections(); ++c) {
        // See if this is an esl or xtor connection
        CarbonCfgRTLConnection* conn = rtlPort->getConnection(c);
        CarbonCfgESLPort* thisEslPort = conn->castESLPort();
        CarbonCfgXtorConn* thisXtorConn = conn->castXtorConn();

        // If it is an esl connection, check if we already have one or a
        // xtor connection. If so, we dump it.
        if (thisEslPort != NULL) {
          if ((eslPort != NULL) || (xtorConn != NULL)) {
            disconnects.push_back(thisEslPort);
          } else {
            eslPort = thisEslPort;
          }
        }

        // Check if we have a duplicate esl and xtor connection
        if (thisXtorConn != NULL) {
          if (eslPort != NULL) {
            disconnects.push_back(eslPort);
            eslPort = NULL;
          }
          xtorConn = thisXtorConn;
        }
      } // for

      // Now can disconnect them
      for (Loop<UtArray<CarbonCfgESLPort*> > l(disconnects); !l.atEnd(); ++l) {
        UtString msg;
        CarbonCfgESLPort* eslPort = *l;
        msg << "RTL port '" << eslPort->getRTLPort()->getName() << "' has duplicate connections which are not support. ESL port '" << eslPort->getName() << "' has been disconnected.";
        reportMessage(eCarbonMsgWarning, msg.c_str());
        disconnect(eslPort);
        putIsModified(true);
      }
    } // for
  } // if

  if (f.bad()) {
    err.reportError(f.getErrmsg());
  }
  
  if (err.hasError() || mMsgHandler.getFailure()) {
    return eCarbonCfgFailure;
  }

  // At this point we have de-serialized a ccfg file, now process it for
  // EmbeddedModules
  if (getenv("CARBON_BYPASS_EMBEDDED_MODELS") == NULL)
    return processEmbeddedModules();
  else
  {
    if (mCcfgDebugOutput)
      qDebug() << "**BYPASS EMBEDDED MODULES";
    return eCarbonCfgSuccess;
  }
} // CarbonCfgStatus CarbonCfg::readNoCheck


eCarbonMsgCBStatus CarbonCfg::sMsgCallback(CarbonClientData clientData,
                                            CarbonMsgSeverity severity,
                                            int number, const char* text,
                                            unsigned int)
{
  CarbonCfg* ccfg = (CarbonCfg*) clientData;
  UtString buf;
  buf << number << ": " << text;
  ccfg->reportMessage(severity, buf.c_str());
  return eCarbonMsgContinue;
} 

bool
CarbonCfg::readCowareLibs(const char* path, bool readXml)
{
  bool retValue = true;

  for (OSDirLoop loop(path, "*"); !loop.atEnd(); ++loop)
  {
    const char* entry = *loop;
    UtString e(entry);
    OSStatEntry se;
    UtString err;
    const char* fullPath = loop.getFullPath(&e);

    OSStatFileEntry(fullPath, &se, &err);

    if (se.isDirectory())
    {
      if (0 == strcmp(entry, "ConvergenSC"))
        retValue = readCowareLibs(fullPath, true);
      else
        retValue = readCowareLibs(fullPath, false);
    }
    else // Its a file, should we read it?
    {
      UtString path;
      UtString filename;
      OSParseFileName(fullPath, &path, &filename);
      
      if (readXml && filename.rfind(".xml") != UtString::npos)
      {
        UtString errMsg;
        if (!readCowareXtorDefinitions(fullPath, &errMsg)) {
          reportMessage(eCarbonMsgError, errMsg.c_str());
          return false;
        }
      }
    }
  }
  return retValue;
}


bool
CarbonCfg::readCowareLibraryFile(FileMap& fileMap, const char* path, bool)
{
  bool retValue = true;

  UtParamFile libFile(path);
  if (!libFile.is_open())
  {
    return false;
  }

  UtIStringStream line;
  while (!libFile.bad() && libFile.getline(&line)) 
  {
    UtString kwd;

    line >> kwd;
    kwd.lowercase();

    // File contains keywords:
    // DEFINE/define INCLUDE/include
    if (kwd == "include" || kwd == "softinclude")
    {
      UtString errMsg;
      UtString fname;
      line >> fname;
      UtString expandedName;
      OSExpandFilename(&expandedName, fname.c_str(), &errMsg);
      bool exists = fileExists(expandedName.c_str());
      if (exists)
      {
        if (fileMap.count(expandedName) == 0)
        {
          fileMap.insert(expandedName);
          readCowareLibraryFile(fileMap, expandedName.c_str(), true);
        }
        else
        {
          UtString msg;
          msg << "In File: " << path << " INCLUDE circular reference to: "  << expandedName.c_str();
          reportMessage(eCarbonMsgError, msg.c_str());
          return false;
        }
      }
      else if (kwd == "include")
      {
        UtString msg;
        msg << "In File: " << path << " INCLUDE reference to: "  << expandedName.c_str() << " does not exist";
        reportMessage(eCarbonMsgError, msg.c_str());
        return false;
      }
    }
    else if (kwd == "define")
    {
      UtString errMsg;
      UtString filePath;
      UtString fileName;
      OSParseFileName(path, &filePath, &fileName);

      UtString defName;
      line >> defName;

      UtString entry;
      line >> entry;
      

      UtString expandedEntry;
      OSExpandFilename(&expandedEntry, entry.c_str(), &errMsg);

      UtString entryPath;
      UtString entryName;

      OSParseFileName(expandedEntry.c_str(), &entryPath, &entryName);

      UtString fullPath;

      if (absolutePath(expandedEntry.c_str()))
        fullPath = expandedEntry;
      else
         OSConstructFilePath(&fullPath, filePath.c_str(), expandedEntry.c_str());

      readCowareLibs(fullPath.c_str(), true);
    }
  }


  return retValue;
}


int CarbonCfg::findXtorDefs(const char* dir, UtStringArray* list)
{
  for (OSDirLoop loop(dir, "*"); !loop.atEnd(); ++loop)
  {
    // Add the file/directory name to the parent directory to constructy a full path
    UtString filePath;
    OSConstructFilePath(&filePath, dir, *loop);

    // If it is a directory, traverse down
    OSStatEntry stat;
    UtString errMsg;
    OSStatFileEntry(filePath.c_str(), &stat, &errMsg);
    if (stat.isDirectory()) {
      findXtorDefs(filePath.c_str(), list);
    }

    // Check if its a xactors.xml
    else if (strcmp(*loop, "xactors.xml") == 0) {
      list->push_back(filePath);
    }
  }
  return 0;
}

CarbonCfgStatus CarbonCfg::readXtorLib(CarbonXtorsType xtorTypes)
{
  bool ret = false;

  if (xtorTypes == eCarbonXtorsMaxsim)
  {
    CarbonCfgXtorLib* xl = NULL;

    const char* carbon_home = getenv("CARBON_HOME");
    if (carbon_home == NULL) {
      reportMessage(eCarbonMsgWarning, 
                    "$CARBON_HOME not defined, cannot read transactor library");
    }
    else {
      // List of xactors.xml files to read.
      UtStringArray list;

      // Check MAXSIM_PROTOCOLS first for transactors.  We want to
      // give those priority, so the new AXIv2 shared library
      // implementation from SoCD is used (if present) instead of the
      // old static library implementation in CMS.
      const char* maxsim_prot = getenv("MAXSIM_PROTOCOLS");
      if (maxsim_prot != NULL) {
        findXtorDefs(maxsim_prot, &list);
        UtString msg;
        msg << "Added transactor definition file from: " << maxsim_prot;
        reportMessage(eCarbonMsgNote, msg.c_str());
      } else {
        UtString msg;
        msg << "Could not find transactor definition file (MAXSIM_PROTOCOLS appears to be unset)";
        reportMessage(eCarbonMsgNote, msg.c_str());
      }

      // Add xactors.xml file from CARBON_HOME
      UtString xmlFile;
      OSConstructFilePath(&xmlFile, carbon_home, "lib");
      OSConstructFilePath(&xmlFile, xmlFile.c_str(), "xactors");
      OSConstructFilePath(&xmlFile, xmlFile.c_str(), "xactors.xml");
      list.push_back(xmlFile);

      xl = findXtorLib(CARBON_DEFAULT_XTOR_LIB);
      SCRIPT_ASSERT_RETURN(xl != NULL, "Could not find default transactor library", eCarbonCfgFailure);

      for (UtStringArray::UnsortedLoop l(list.loopUnsorted()); !l.atEnd(); ++l) {
        UtString errmsg;
        bool success = xl->readXtorDefinitions(*l, &errmsg);
        if (success) {
          UtString msg;
          msg << "Read transactor definition file: " << *l;
          reportMessage(eCarbonMsgNote, msg.c_str());
        } else {
          reportMessage(eCarbonMsgError, errmsg.c_str());
        }
        // If we read any transactor definitions consider it a success.
        ret |= success;
      }
    }
  }

  if (xtorTypes == eCarbonXtorsCoware)
  {
    // Look for the CoWare transactors. We can find these using one of
    // three methods in priority order. Look in sub directories of:
    //
    //  1. $COWAREIPLIB
    //  2. $COWAREIPDIR
    //  3. $COWAREHOME/IP and $COWAREHOME/PAMD/linux/IP_common
    //
    // Note that the first option points to the directory with the
    // protocols while the next two are search roots.
    UtStringArray searchPaths;
    const char* cowareIPDIR = getenv("COWAREIPDIR");
    const char* cowareIPLIB = getenv("COWAREIPLIB");
    const char* cowareHOME = getenv("COWAREHOME");
    if ((cowareIPLIB != NULL) && fileExists(cowareIPLIB)) {
      // They are pointing to the exact directory, get it from there
      FileMap fileMap;
      ret = readCowareLibraryFile(fileMap, cowareIPLIB, false);

    } else if ((cowareIPDIR != NULL) && fileExists(cowareIPDIR)) {
      // Add it to the search path for the for loop below
      searchPaths.push_back(cowareIPDIR);

    } else if ((cowareHOME != NULL) && fileExists(cowareHOME)) {
      // Add them to the search path for the for loop below
      UtString ipDir1, ipDir2;
      OSConstructFilePath(&ipDir1, cowareHOME, "IP");
      OSConstructFilePath(&ipDir2, cowareHOME, "IP_common");
      searchPaths.push_back(ipDir1);
      searchPaths.push_back(ipDir2);

    } else {
      reportMessage(eCarbonMsgWarning, "$COWAREHOME, $COWAREIPDIR or $COWAREIPLIB not defined, cannot read transactor library");
    }

    // If we have search paths, look for the ConvergenSC directories
    // under these root paths.
    ret = true;
    for (UtStringArray::UnsortedLoop l = searchPaths.loopUnsorted(); !l.atEnd(); ++l) {
      const UtString& path = *l;
      ret &= readCowareLibs(path.c_str(), false);
    }
  }

  // Read in the SystemC TLM2 transactor library
  if (xtorTypes == eCarbonXtorsSystemC) {
    const char* carbon_home = getenv("CARBON_HOME");
    if (carbon_home == NULL) {
      reportMessage(eCarbonMsgWarning, 
                    "$CARBON_HOME not defined, cannot read transactor library");
    } else {
      // Gather the xactor XML files - Currently there is only one,
      // but we may need to add others.
      typedef UtHashMap<UtString, UtString> XtorLibMap;
      XtorLibMap xtorLibMap;
      UtString xmlFile;
      OSConstructFilePath(&xmlFile, carbon_home, "lib");
      OSConstructFilePath(&xmlFile, xmlFile.c_str(), "xactors");
      OSConstructFilePath(&xmlFile, xmlFile.c_str(), "tlm2");
      OSConstructFilePath(&xmlFile, xmlFile.c_str(), "xactors.xml");
      xtorLibMap["AMBA-TLM2"] = xmlFile;

      // Only read this if it exists. We removed it temporarily for the current release.
      ret = true;
      if (fileExists(xmlFile.c_str())) {
        // Walk the map and add the various System C libraries
        for (LoopMap<XtorLibMap> l(xtorLibMap); !l.atEnd(); ++l) {
          // Create the xtor lib if it wasn't already
          const UtString& libName = l.getKey();
          CarbonCfgXtorLib* xtorLib = findXtorLib(libName.c_str());
          if (xtorLib == NULL) {
            xtorLib = new CarbonCfgXtorLib(libName.c_str());
            addXtorLib(xtorLib);
          }

          // Read these set of definitions
          UtString errMsg;
          const UtString& fileName = l.getValue();
          bool success = xtorLib->readXtorDefinitions(fileName.c_str(), &errMsg);
          if (success) {
            UtString msg;
            msg << "Read transactor definition file: " << fileName;
            reportMessage(eCarbonMsgNote, msg.c_str());
          } else {
            reportMessage(eCarbonMsgError, errMsg.c_str());
            ret = false;
          }
        } // for
      } // if
    } // } else
  } // if

  if (ret)
    mReadXtorLibrary = true;

  return ret ? eCarbonCfgSuccess : eCarbonCfgFailure;
} // CarbonCfgStatus CarbonCfg::readXtorLib

CarbonCfgStatus carbonCfgReadXtorLib(CarbonCfgID cfg, CarbonXtorsType xtorTypes) {
  return cfg->readXtorLib(xtorTypes);
}

CarbonCfgStatus carbonCfgReadXtorDefinitions(CarbonCfgID cfg, const char *fileName) {
  UtString errMessage;
  CarbonCfgXtorLib* xtorLib = cfg->findXtorLib(CARBON_DEFAULT_XTOR_LIB);

  if(xtorLib == NULL)
    return eCarbonCfgFailure;

  bool ok = xtorLib->readXtorDefinitions(fileName, &errMessage);
  if (!ok) {
    cfg->reportMessage(eCarbonMsgError, errMessage.c_str());
  }
  return ok ? eCarbonCfgSuccess : eCarbonCfgFailure;
}

void CarbonCfg::clear() {
  mRTLPortVec.clearPointers();
  mXtorVec.clearPointers();
  
  mLibName.clear();

  mLegacyMemories = 1;

  mRTLPortMap.clear();
  mXtorMap.clear();
  mESLPortMap.clear();

  mESLPortNames.clear();
  mMemNames.clear();
  mGroupNames.clear();
  mRegNames.clear();
  mGroupNames.clear();
  mPStreamNames.clear();

  mGroupVec.clearPointers();
  for (UInt32 i = 0; i < mPStreamVec.size(); ++i) {
    delete mPStreamVec[i];  // can't use clearPointers cuase dtor is private
  }
  mPStreamVec.clear();

  mCompName.clear();
  mTopModuleName.clear();
  mDescription.clear();
  clearRegister();
  clearMemory();
  clearCustomCode();
  mWaveType = eCarbonCfgNone;
  mVersion = CFG_VERSION;
  mFeatureMask = (1 << eCfgFeaturePortExpr) | (1 << eCfgFeatureMemInitEnum)
    | (1 << eCfgFeatureXtorConnExpr);

  mScriptExtensions.clear();
}

CarbonCfg* CarbonCfg::findSubComponent(const char* name)
{
  CarbonCfg* subComp = NULL;

  // If one exists, find it
  if (mSubCompNames.find(name) != mSubCompNames.end()) {
    for(unsigned int i = 0; i < numSubComponents(); ++i) {
      if(strcmp(name, mSubCompVec[i]->getCompName()) == 0) {
        subComp = mSubCompVec[i];
        break;
      }
    }
  }
  return subComp;
}

void CarbonCfg::clearRegister() {
  mRegisterVec.clearPointers();
}

void CarbonCfg::clearMemory() {
  mMemoryVec.clearPointers();
}

void CarbonCfg::clearCustomCode()
{
  mCustomCodes.clearCustomCodes();
}
CcfgEnum::CarbonCfgStatus CarbonCfg::Write(const QString& filename)
{
  UtString name;
  name << filename;
  CarbonCfgStatus status = write(name.c_str());
  return static_cast<CcfgEnum::CarbonCfgStatus>(status);
}

CcfgEnum::CarbonCfgStatus CarbonCfg::Write()
{
  if (!mFilePath.isEmpty())
  {
    QFileInfo fi(mFilePath);
    QString filename = mFilePath;
    UtString name;
    name << filename;

    CarbonCfgStatus status = write(name.c_str());
    return static_cast<CcfgEnum::CarbonCfgStatus>(status);
  }
  else
  {
    QScriptContext* ctx = context();
    if (ctx)
      ctx->throwError("No file was previously read, use write(filename) instead");
    return CcfgEnum::Failure;
  }
}

void CarbonCfgXtorInstance::addParameter(CarbonCfgXtorInstance* xinst, QXmlStreamReader& xr)
{
  UtString paramName;
  UtString paramValue;
  bool paramHidden = false;
  UtString tempValue;
  UtString xtorInst;
  UtStringArray enumChoices;
  while (xr.readNextStartElement())
  {
    if        ("Pname" == xr.name()) {
      paramName = QUSTR(xr.readElementText());
    } else if ("Name" == xr.name()) {
      xtorInst = QUSTR(xr.readElementText());
    } else if ("Value" == xr.name()) {
      paramValue = QUSTR(xr.readElementText());
    } else if ("Hidden" == xr.name()) {
      tempValue = QUSTR(xr.readElementText());
      paramHidden = (tempValue == "true");
    } else if ("EnumChoice" == xr.name()) {
      UtString enumChoice = QUSTR(xr.readElementText());
      enumChoices.push_back(enumChoice);
    } else {
      xr.skipCurrentElement();
    }
  }

  if (paramName.length() > 0) {
    CarbonCfgXtorParamInst* parmInst = xinst->findParameterInst(paramName, true); // include hidden parameters in search
    if (NULL == parmInst) {
      xinst->addParameter(paramName, paramValue, enumChoices);
    } else  {
      parmInst->putValue(paramValue.c_str());
      if (!enumChoices.empty()) {
        parmInst->putEnumChoices(enumChoices);
      }
    }
    if ( paramHidden ) {
      xinst->hideParameterInst(paramName);
    }
  }
}


void CarbonCfg::addParameter(UtIStream& f)
{
  UtString xtorInst;
  UtString paramName;
  UtString paramValue;

  // The parameter line is <xtor-instance> <parameter-name> <parameter-value>
  //
  // The <xtor-instance> should match the instance name in the xtor
  // line, the parameter name and value are strings (name can contain
  // spaces)
  f >> xtorInst >> paramName >> paramValue;

  // Find the instance (make sure it exists)
  CarbonCfgXtorInstance* inst = findXtorInstance(xtorInst.c_str());
  if (inst == NULL) {
    // Can't find the instance, error out
    UtString buf;
    buf << "Cannot find transactor instance " << xtorInst
        << " for parameter " << paramName;
    f.reportError(buf.c_str());
    return;
  }

  // Attach the parameter to the instance
  CarbonCfgXtorParamInst* paramInst = inst->findParameterInst(paramName.c_str());
  if (paramInst != NULL) {
    CarbonCfgXtorParam* param = paramInst->getParam();
    if (param->inRange(paramValue.c_str())) {
      paramInst->putValue(paramValue.c_str());
    } else {
      // Invalid parameter value
      UtString buf;
      buf << carbonCfgERR_INVPARAMVAL << ": Invalid Parameter `" << paramName << "' value for xtor instance `"
          << xtorInst << "' with value `" << paramValue << "', min: `" << param->min()
          << "', max: `" << param->max() << "'.";
      reportMessage(eCarbonMsgError, buf.c_str());
    }
  } else { 
    // Duplicate parameter
    UtString buf;
    buf << carbonCfgERR_ILLPARAM << ": Unknown Parameter `" << paramName << "' for xtor instance `"
        << xtorInst << "' with value `" << paramValue << "' ignored.";
    reportMessage(eCarbonMsgWarning, buf.c_str());
  }
} // void CarbonCfg::addParameter


const char* CarbonCfg::computeSystemCType(CarbonCfgRTLPort* rtlPort)
{
  int width = rtlPort->getWidth();

  if (rtlPort->getType() == eCarbonCfgRTLInout) {
    if (width == 1) {
      return "sc_logic";
    } else {
      return "sc_lv";
    }
  } else {
     // 2 State
    if (width == 1) {
      return "bool";
    } else if (width < 65) {
      return "sc_uint";
    } else {
      return "sc_biguint";
    }
  }
}


UInt32 CarbonCfg::numSystemCTypes(void)
{
  return mSystemCTypes.size();
}

const char* CarbonCfg::getSystemCType(UInt32 i)
{
  if (i < mSystemCTypes.size()) {
    return mSystemCTypes[i];
  } else {
    return "";
  }
}

CarbonCfgESLPort* CarbonCfg::addESLPort(CarbonCfgRTLPort* rtlPort,
                                        const char* name,
                                        CarbonCfgESLPortType type,
                                        const char* typeDef,
                                        CarbonCfgESLPortMode portMode)
{
  if (mESLPortNames.insertWithCheck(name)) {
    // Create the port and connect it
    CarbonCfgESLPort* eslPort = new CarbonCfgESLPort(rtlPort, name, type, typeDef, portMode);
    mESLPortVec.push_back(eslPort);
    mESLPortMap[name] = eslPort;
    rtlPort->connect(eslPort);

    // Set the default SystemC type in case this is a system c
    // model. Currently this type is ignore by SoC Designer models. In
    // the end SoC Designer models will go to SystemC anyway.
    if ((typeDef == NULL) || (strcmp(typeDef, "") == 0)) {
      // Not defined by caller, compute it
      eslPort->putTypeDef(computeSystemCType(rtlPort));
    }

    return eslPort;
  }
  return NULL;
}

bool CarbonCfg::addESLPort(CarbonCfgESLPort* eslPort) {
  if (mESLPortNames.insertWithCheck(eslPort->getName())) {
    mESLPortVec.push_back(eslPort);
    mESLPortMap[eslPort->getName()] = eslPort;
    // already connected 
    return true;
  }
  return false;
}

int CarbonCfg::useLegacyMemories() const
{
  // For testing purposes, we can overide this setting
  // If CARBON_USE_LEGACY_DEBUG_MEMORIES is defined,
  // use the value of it.
  const char* value = getenv("CARBON_USE_LEGACY_DEBUG_MEMORIES");
  if(value != NULL)
    return atoi(value);
  
  // Othewise use the value from the configuration file
  return mLegacyMemories;
}

CarbonCfgStatus CarbonCfg::write(const char* filename) 
{
  CfgXmlWriter xmlw("CarbonComponentConfiguration");

  UtOBStream f(filename);
  if (!f.is_open()) {
    mMsgHandler.putMsg(f.getErrmsg());
    return eCarbonCfgFailure;
  }

  xmlw.addAttribute("version", CFG_VERSION);

 /* f << "# Carbon CFG file version " << CFG_VERSION << "\n";
  f << "version " << CFG_VERSION << "\n\n";
  f << "features";*/

  xmlw.addElement("Features");
  for (const char** p = gCarbonCfgFeature; *p != NULL; ++p) 
  {
    xmlw.addCompletedElement(*p);
    //f << " " << *p;
  }
  //f << '\n';
  xmlw.closeElement();

  //f << "platform " << gCarbonCCFGModes[mMode] << '\n';
  xmlw.addCompletedElement("Platform", gCarbonCCFGModes[mMode]);

  //f << "component" << mCompName << '\n';
  xmlw.addCompletedElement("Component", mCompName);

  
  //f << "topModule" << mTopModuleName << '\n';
  xmlw.addCompletedElement("TopModule", mTopModuleName);

 
  //f << "library" << mLibName << '\n';
  xmlw.addCompletedElement("Library", mLibName);

 
  //f << "legacymems" << mLegacyMemories << '\n';
  xmlw.addCompletedElement("LegacyMemories", mLegacyMemories);

  if (mWaveType == eCarbonCfgVCD) {
    // f << "vcd" << mWaveFile << '\n';
    xmlw.addCompletedElement("VCD", mWaveFile);
  }
  else if (mWaveType == eCarbonCfgFSDB) {
    // f << "fsdb" << mWaveFile << '\n';
    xmlw.addCompletedElement("FSDB", mWaveFile);
  }

  //f << "iodb" << mIODBFile << '\n' << '\n';
  xmlw.addCompletedElement("IODB", mIODBFile);

  //f << "description" << mDescription << '\n';
  xmlw.addCompletedElement("Description", mDescription);

  //f << "cxxflags" << mCxxFlags << '\n';
  xmlw.addCompletedElement("CxxFlags", mCxxFlags);

  //f << "linkflags" << mLinkFlags << '\n';
  xmlw.addCompletedElement("LinkFlags", mLinkFlags);

  //f << "includefiles" << mIncludeFiles << '\n';
  xmlw.addCompletedElement("IncludeFiles", mIncludeFiles);

  //f << "sourcefiles" << mSourceFiles << '\n';
  xmlw.addCompletedElement("SourceFiles", mSourceFiles);

  //f << "usestaticscheduling" << mUseStaticScheduling << '\n';
  xmlw.addCompletedElement("UseStaticScheduling", mUseStaticScheduling);

   //f << "loadfileext" << mLoadfileExtension << '\n';
  xmlw.addCompletedElement("LoadFileExtension", mLoadfileExtension);

  mMsgHandler.clear();

  // write all transactors that clock other transactors first, so
  // that they will be available for lookup when reading the file back in.
  for (UInt32 i = 0; (i < mXtorVec.size()) && !f.bad(); ++i) {
    if (mXtorVec[i]->getType()->isXtorClockMaster()) {
      mXtorVec[i]->write(xmlw);
    }
  }
  for (UInt32 i = 0; (i < mXtorVec.size()) && !f.bad(); ++i) {
    if (!mXtorVec[i]->getType()->isXtorClockMaster()) {
      mXtorVec[i]->write(xmlw);
    }
  }

 
  for (UInt32 i = 0; (i < mRTLPortVec.size()) && !f.bad(); ++i) {
    mRTLPortVec[i]->write(xmlw);
  }
 
  writeXmlComps(xmlw);
 

  for (UInt32 i = 0; i < mPStreamVec.size(); ++i) {
    mPStreamVec[i]->write(xmlw);
  }
 
  writeXmlParamDef(xmlw);
 

#ifdef CARBON_PV
  writeXmlPSD(f);
  f << '\n';
#endif

  xmlw.write(f);

  if (f.bad())
  {
    reportMessage(eCarbonMsgError, f.getErrmsg());
    return eCarbonCfgFailure;
  }

  f.close();

  putIsModified(false);
  return eCarbonCfgSuccess;
} // CarbonCfgStatus write

CarbonCfgMode carbonCfgGetCcfgMode(CarbonCfgID cfg)
{
  return cfg->getCcfgMode();
}

void carbonCfgPutCcfgMode(CarbonCfgID cfg, CarbonCfgMode mode) {
  cfg->putCcfgMode(mode);
}

#ifdef CARBON_PV
void CarbonCfg::writeXmlPSD(UtOStream & /*s*/)
{
// I have not re-implemented XML write following the conversion from
// PSD data structures to PV
#define XML_WRITE_NYI 0
#if XML_WRITE_NYI
  if (mPSDTransactionVec.empty())
    return;

  s << "xmltransactions" << '\n' ;

  CfgXmlWriter xmlwriter("PSDinfo");

  for (UInt32 i = 0; i < mPSDTransactionVec.size(); i++) {
    mPSDTransactionVec[i]->write(xmlwriter);
  }

  xmlwriter.write(s);
#endif
}
#endif

void CarbonCfg::writeXmlRegisters(CfgXmlWriter& xmlwriter)
{
  // In the XML file, registers are located in their respective
  // groups, but CarbonCfgGroup doesn't have a list of registers
  // it contains.  It's easier just to build that relationship
  // when I need it than to fix the classes.  I think fixing
  // it would be useful in the long run, since the wizard presents
  // the registers in groups as well.
  
  xmlwriter.addAttribute("baseaddr",mRegisterBaseAddress);

  // If there are no registers, don't bother
  if (mRegisterVec.empty())
    return;

  for (UInt32 i = 0; i < mGroupVec.size(); ++i) {
    RegisterVec groupregs;
    UtString groupname;

    // sort registers that belong to the i'th group
    for (UInt32 j = 0; j < mRegisterVec.size(); ++j) {
      groupname = mRegisterVec[j]->getGroupName();

      if (groupname == mGroupVec[i]->getName())
    groupregs.push_back(mRegisterVec[j]);
    }

    // write i'th register group to ccfg
    if (groupregs.size() != 0) {
      xmlwriter.addElement("reggroup");
      xmlwriter.addAttribute("name", mGroupVec[i]->getName());

      for (UInt32 j = 0; j < groupregs.size(); ++j)
    groupregs[j]->write(xmlwriter);

      xmlwriter.closeElement();
    }
  }
}

void CarbonCfg::writeXmlMemories(CfgXmlWriter& xmlwriter)
{  
  // If there are no parameters, don't bother
  if (mMemoryVec.empty())
    return;

  // Build and write the XML for the memories
  for (UInt32 i = 0; i < mMemoryVec.size(); ++i)
    mMemoryVec[i]->write(xmlwriter);
  
  // Write ELF Loader configuration
  mELFLoader.write(xmlwriter);

  // Write Processor Info
  mProcInfo.write(xmlwriter);
}

void CarbonCfg::writeXmlComps(CfgXmlWriter &xmlwriter)
{
  // Build and write the XML for the subcomponents
  //s << "xmlcomponents" << '\n';
  //CfgXmlWriter xmlwriter("componentinfo");

  xmlwriter.addElement("componentinfo");

  // Write the top component
  write(xmlwriter);

  xmlwriter.closeElement();
  // Complete the write
  //xmlwriter.write(s);
}

void CarbonCfg::write(CfgXmlWriter& xmlwriter)
{
  bool debugEmbeddedModels = false;
  if (getElementGenerationType() == eCarbonCfgElementGenerated && getenv("CARBON_DEBUG_EMBEDDED_MODELS"))
    debugEmbeddedModels = true;

  if (getElementGenerationType() == eCarbonCfgElementGenerated)
  {
    xmlwriter.addElement("componentJournal");
    xmlwriter.addAttribute("origName", mPersistentCompName.c_str());
    xmlwriter.addAttribute("name", getCompName());

    // if we have any memory, we write a meminfo section
    xmlwriter.addElement("memoryJournal");
    writeXmlMemories(xmlwriter);
    xmlwriter.closeElement();

    xmlwriter.closeElement(); // componentJournal
  }
  
  if (debugEmbeddedModels || getElementGenerationType() == eCarbonCfgElementUser)
  {
    // Open the component element
    if (debugEmbeddedModels)
      xmlwriter.addElement("debugEmbeddedComponent");
    else
      xmlwriter.addElement("component");

    xmlwriter.addAttribute("name", getCompName());

    // If this has a display name, write it (this is optional since
    // usually the display name is the component name.
    if (!mCompDisplayName.empty()) {
      xmlwriter.addElement("displayname", getCompDisplayName());
      xmlwriter.closeElement();
    }

    // Write the component simple elements
    xmlwriter.addElement("type", getCompType());
    xmlwriter.closeElement();

    // Write the component simple elements
    xmlwriter.addElement("version", getCompVersion());
    xmlwriter.closeElement();

    xmlwriter.addElement("docfile", getCompDocFile());
    xmlwriter.closeElement();

    xmlwriter.addElement("loadfileext", getLoadfileExtension());
    xmlwriter.closeElement();

    if (mComponentTag.length() > 0)
    {
      xmlwriter.addElement("tag", getComponentTag());
      xmlwriter.closeElement();
    }

    if (getRequiresMkLibrary()) {
      xmlwriter.addElement("requires-mk-library");
      xmlwriter.closeElement();
    }

    if (getUseVersionedMkLibrary()) {
      xmlwriter.addElement("use-versioned-mk-library");
      xmlwriter.closeElement();
    }

    if (isStandAloneComp()) {
      xmlwriter.addElement("standalone", "true");
      xmlwriter.closeElement();
    }

    if (isSubComponentFullName()) {
      xmlwriter.addElement("sub-component-full-name", "true");
      xmlwriter.closeElement();
    }

    // Write the SoCDPCTraceReporter accessor expression, if it
    // exists.
    if (!mPCTraceAccessor.empty()) {
      xmlwriter.addElement("pctraceaccessor", mPCTraceAccessor.c_str());
      xmlwriter.closeElement();
    }

    // Write any custom code sections
    mCustomCodes.writeCustomCodes(xmlwriter);

    // if we have registers, we write a reginfo section
    xmlwriter.addElement("reginfo");
    writeXmlRegisters(xmlwriter);
    xmlwriter.closeElement();

    // if we have any memory, we write a meminfo section
    xmlwriter.addElement("meminfo");
    writeXmlMemories(xmlwriter);
    xmlwriter.closeElement();

    // Write the cadi sections
    mCadi.write(xmlwriter);

    // Write the subcomponents if it exists
    if (!mSubCompVec.empty()) {
      xmlwriter.addElement("subcomponents");

      // Traverse the subcomponents and
      for (UInt32 i = 0; i < mSubCompVec.size(); ++i) {
        mSubCompVec[i]->write(xmlwriter);
      }

      // Close the sub components
      xmlwriter.closeElement();
    }

    // Close component element
    xmlwriter.closeElement();
  }
}

void CarbonCfg::writeXmlParamDef(CfgXmlWriter &xmlwriter)
{  
  // If there are no parameters, don't bother
  if (mParamVec.empty())
    return;

  // Build and write the XML for the groups and
  // registers
  //s << "xmlparameters" << '\n';
  //CfgXmlWriter xmlwriter("paramdef");
  xmlwriter.addElement("ParamDef");
  for(UInt32 i = 0; i < numParams(); ++i)
    getParam(i)->getParam()->write(xmlwriter);

  xmlwriter.closeElement();
  //xmlwriter.write(s);


}

bool CarbonCfgClockGen::isExclusive() const {
  return true;
}

bool CarbonCfgResetGen::isExclusive() const {
  return true;
}

bool CarbonCfgTie::isExclusive() const {
  return true;
}

bool CarbonCfgTieParam::isExclusive() const {
  return true;
}

bool CarbonCfgXtorConn::isExclusive() const {
  return false;
}

void CarbonCfgXtorConn::putExpr(const char* expr) {
  mExpr = expr;
}

bool CarbonCfgESLPort::isExclusive() const {
  return false;
}


void CarbonCfgRTLPort::connect(CarbonCfgRTLConnection* c) {
  // Connecting Clocks/Resets/Ties eliminates all other connections.
  // Also, if there is one existing exclusive connection then it
  // has to disappear as well
  if (c->isExclusive() ||
      ((mConnections.size() == 1) &&
       (mConnections[0]->isExclusive())))
  {
    clear();
  }
  mConnections.push_back(c);
  c->connect();
}

void CarbonCfg::disconnect(CarbonCfgRTLConnection* c) {
  CarbonCfgESLPort* eslPort = c->castESLPort();
  if (eslPort != NULL) {
    mESLPortMap.erase(eslPort->getName());
    mESLPortNames.erase(eslPort->getName());
    mESLPortVec.remove(eslPort);
  }

  // empty xtor connections are not associated with an RTL port,
  // and they are owned by the xtor instance, so don't delete them
  CarbonCfgRTLPort* rtlPort = c->getRTLPort();
  if (rtlPort != NULL) {
    rtlPort->disconnect(c);
  }
}

void CarbonCfgRTLPort::disconnect(CarbonCfgRTLConnection* c) {
  UInt32 removed = mConnections.remove(c);
  if (removed == 1)
  {
    c->disconnect();
    delete c;
  }
}

// Disconnect and remove the RTL Port
void CarbonCfg::removeRTLPort(CarbonCfgRTLPort* rtlPort)
{
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
    disconnect(rtlPort->getConnection(i));

  mRTLPortMap.erase(rtlPort->getName());
  mRTLPortVec.remove(rtlPort);
}

void CarbonCfg::changeESLName(CarbonCfgESLPort* eslPort, const char* name)
{
  // Remove the current name from the port set
  mESLPortNames.erase(eslPort->getName());
  mESLPortMap.erase(eslPort->getName());

  UtString buf;
  getUniqueESLName(&buf, name);
  mESLPortNames.insert(buf);
  mESLPortMap[buf] = eslPort;
  eslPort->putName(buf.c_str());
}

void CarbonCfg::changeXtorInstanceName(CarbonCfgXtorInstance* xinst,
                                       const char* name)
{
  // Remove the current name from the port set
  mESLPortNames.erase(xinst->getName());
  mXtorMap.erase(xinst->getName());

  UtString buf;
  getUniqueESLName(&buf, name);
  mESLPortNames.insert(buf);
  mXtorMap[buf] = xinst;
  xinst->putName(buf.c_str());
}

void CarbonCfg::changeMemoryName(CarbonCfgMemory* mem, const char* name) {
  // Remove the current name from the port set
  mMemNames.erase(mem->getName());

  UtString buf;
  getUniqueMemName(&buf, name);
  mMemNames.insert(buf);
  mem->putName(buf.c_str());
}

void CarbonCfg::changePStreamName(CarbonCfgPStream* pstream, const char* name)
{
  // Remove the current name from the port set
  mPStreamNames.erase(pstream->getName());

  UtString buf;
  getUniquePStreamName(&buf, name);
  mPStreamNames.insert(buf);
  pstream->putName(buf.c_str());
}


void CarbonCfg::changeRegName(CarbonCfgRegister* reg, const char* name) {
  // Remove the current name from the port set
  mRegNames.erase(reg->getName());

  UtString buf;
  getUniqueRegName(&buf, name);
  mRegNames.insert(buf);
  reg->putName(buf.c_str());
}

void CarbonCfg::changeGroupName(CarbonCfgGroup* group, const char* name) {
  // Remove the current name from the group set
  mGroupNames.erase(group->getName());

  UtString buf;
  getUniqueGroupName(&buf, name);
  mGroupNames.insert(buf);
  group->putName(buf.c_str());
}

void CarbonCfg::restoreSubCompRedo(CarbonCfg* comp) {
  bool inserted = mSubCompNames.insertWithCheck(comp->getCompName());
  SCRIPT_ASSERT(inserted, "duplicate subcomponent"); 
  mSubCompVec.push_back(comp);
}


void CarbonCfg::removeSubComp(CarbonCfg* comp) {
  // Remove the current name from the sub component vector
  mSubCompNames.erase(comp->getCompName());
  mSubCompVec.remove(comp);       // order N
  delete comp;
}

void CarbonCfg::removeSubCompUndo(CarbonCfg* comp) {
  // Remove the current name from the sub component vector
  // But don't delete it yet.
  mSubCompNames.erase(comp->getCompName());
  mSubCompVec.remove(comp);       // order N
}


void CarbonCfg::removeMemory(CarbonCfgMemory* mem) {
  // Remove the current name from the memory vector
  mMemNames.erase(mem->getName());
  mMemoryVec.remove(mem);       // order N
  delete mem;
}

void CarbonCfg::insertMemoryUndo(int pos, CarbonCfgMemory* mem)
{
  MemoryVec temp;
  temp.push_back(mem);
  mMemNames.insert(mem->getName());
  mMemoryVec.insert(pos, temp.begin(), temp.end());
}
int CarbonCfg::removeMemoryUndo(CarbonCfgMemory* mem) {
  // Remove the current name from the memory vector (used by undo)
  mMemNames.erase(mem->getName());
  for(UInt32 i=0; i<mMemoryVec.size(); i++)
  {
    CarbonCfgMemory* m = mMemoryVec[i];
    if (m == mem)
    {
      mMemoryVec.remove(mem);       // order N
      return i;
    }
  }
  return -1;
}


void CarbonCfg::removeCustomCode(CarbonCfgCompCustomCode* customCode)
{
  mCustomCodes.removeCustomCode(customCode);
}

void CarbonCfg::removeRegister(CarbonCfgRegister* reg) {
  mRegNames.erase(reg->getName());
  mRegisterVec.remove(reg);       // order N
  delete reg;
}

void CarbonCfg::removeGroup(CarbonCfgGroup* group) {
  // Should check that no registers reference this group...

  // Remove the current name from the group name-set & map
  mGroupNames.erase(group->getName());
  mGroupVec.remove(group);      // order N
  delete group;
}

void CarbonCfg::changeRegGroup(CarbonCfgRegister *reg, const char *group_name)
{
  // See if this group already exists
  CarbonCfgGroup *group = NULL;

  UtStringSet::const_iterator iter = mGroupNames.find(group_name);
  if (iter == mGroupNames.end()) {
    group = addGroup(group_name);
  } else {
    // Lookup the entry in the group vector
    bool found = false;
    GroupVec::const_iterator i = mGroupVec.begin();
    while (!found && (i != mGroupVec.end())) {
      if (strcmp((*i)->getName(), group_name) == 0) {
        found = true;
      } else {
        ++i;
      }
    }
    if (found)
      group = *i;
  }
  reg->putGroup(group);
  // TODO - erase empty groups?
}

void CarbonCfg::removePStream(CarbonCfgPStream* pstream) {
  mPStreamNames.erase(pstream->getName());
  mPStreamVec.remove(pstream);       // order N
  delete pstream;
}

void CarbonCfg::removeXtorInstance(CarbonCfgXtorInstance* xinst) {
  // If the transactor is a type that can clock other transactors, 
  // find any other instances that depend on it and disconnect them.
  if (xinst->getType()->isXtorClockMaster()) {
    for (UInt32 i = 0; i < mXtorVec.size(); ++i) {
      if (mXtorVec[i]->getClockMaster() == xinst) {
        mXtorVec[i]->putClockMaster(NULL);
      }
    }
  }

  // Remove the current name from the port set
  mESLPortNames.erase(xinst->getName());
  mXtorMap.erase(xinst->getName());
  mXtorVec.remove(xinst);
  delete xinst;
}

void CarbonCfgESLPort::putName(const char* name) {
  mName = name;
}

void CarbonCfgESLPort::putExpr(const char* expr) {
  mExpr = expr;
}

void CarbonCfgXtorInstance::putName(const char* name) {
  mName = name;
}

void CarbonCfg::putDescription(const char* str) {
  mDescription = str;
}

const char* CarbonCfg::getDescription() const {
  return mDescription.c_str();
}

void CarbonCfg::putCxxFlags(const char* str) {
  mCxxFlags = str;
}

const char* CarbonCfg::getCxxFlags() const {
  return mCxxFlags.c_str();
}

void CarbonCfg::putLinkFlags(const char* str) {
  mLinkFlags = str;
}

const char* CarbonCfg::getLinkFlags() const {
  return mLinkFlags.c_str();
}

void CarbonCfg::putSourceFiles(const char* str) {
  mSourceFiles = str;
}

const char* CarbonCfg::getSourceFiles() const {
  return mSourceFiles.c_str();
}

void CarbonCfg::putIncludeFiles(const char* str) {
  mIncludeFiles = str;
}

const char* CarbonCfg::getIncludeFiles() const {
  return mIncludeFiles.c_str();
}

void CarbonCfg::putIsStandAloneComp(bool sa) {
  mIsStandAlone = sa;
}

bool CarbonCfg::isStandAloneComp() const {
  return mIsStandAlone;
}

void CarbonCfg::putSubComponentFullName(bool sa) {
  mIsSubComponentFullName = sa;
}

bool CarbonCfg::isSubComponentFullName() const {
  return mIsSubComponentFullName;
}

CarbonCfg* CarbonCfg::addSubComp(const char* subCompName) {
  SCRIPT_ASSERT_RETURN(isTopLevel(), "Sub components can only be added at the top level.", NULL);

  bool inserted = mSubCompNames.insertWithCheck(subCompName);
  SCRIPT_ASSERT_RETURN(inserted, "duplicate subcomponent", NULL);
  CarbonCfg* subcomp = new CarbonCfgSub(this);
  subcomp->putLegacyMemories(useLegacyMemories());   // propogate the style of memory down from the parent to the current
  subcomp->putCompName(subCompName);
  mSubCompVec.push_back(subcomp);
  return subcomp;
}

unsigned int carbonCfgNumSubComponents(CarbonCfg* cfg)
{
  return cfg->numSubComponents();
}

CarbonCfgID carbonCfgGetSubComponent(CarbonCfgID cfg, unsigned int index)
{
  return cfg->getSubComponent(index);
}

CarbonCfgRegister* CarbonCfg::addRegister(const char* debugName,
                                          CarbonCfgGroup* group,
                                          unsigned int width,
                                          bool big_endian,
                                          CarbonCfgRadix radix,
                                          const char* comment,
                                          bool pc_reg)
{
  bool inserted = mRegNames.insertWithCheck(debugName);
  if (inserted)
  {
    CarbonCfgRegister* reg = new CarbonCfgRegister(debugName, group, width,
                                                   big_endian, radix, comment,
                                                   pc_reg);
    mRegisterVec.push_back(reg);
    return reg;
  }
  return NULL;
}

CarbonCfgGroup* CarbonCfg::addGroup(const char* groupName) {
  bool inserted = mGroupNames.insertWithCheck(groupName);
  if (inserted)
  {
    CarbonCfgGroup* group = new CarbonCfgGroup(groupName);
    mGroupVec.push_back(group);
    return group;
  }
  return NULL;
}

// This is a legacy constructor compatible with the old format, when we didn't
// have explicit sub components
CarbonCfgMemory* CarbonCfg::addMemory(const char* signal, const char* name,
                                      const char* initFile, UInt64 maxAddrs,
                                      UInt32 width, CarbonCfgReadmemType rt,
                                      const char* comment, const char* disassemblyName,
                      const char* disassemblerName)
{
  CarbonCfgMemory* mem = NULL;

  // For legacy reasons we need to create a sub component if this method is used to instantiate
  // a memory that has a disassembler view. The disassembly name is actually the name of the
  // sub component, so that is what we check for. A memory could actually be setup to specify
  // disassembly without specifying disassembler meening that a memory without disassembler
  // should be placed in a sub component.

  // Check if we need to create a sub component
  if (strcmp("", disassemblyName) != 0) {
    CarbonCfg* comp = findSubComponent(disassemblyName);
    
    // If there is no sub component with this name create a new one
    if(comp == NULL)  comp = addSubComp(disassemblyName);

    SCRIPT_ASSERT_RETURN(comp, "adding memory failed.", NULL);
    if(comp != NULL) {
      // Add memory to sub component, omitt the disassemblyName since it is already in a sub component
      mem = comp->addMemory(signal, name, initFile, maxAddrs, width, rt, comment, "", disassemblerName);
      mem->putMemoryDisassemblyName(disassemblyName);

      // Set Disassemler Name to sub components CADI class, if any
      if(strcmp("", disassemblerName) != 0) {
         comp->getCadi()->putDisassemblerName(disassemblerName);
      }
    }
  }

  else {
    bool inserted = mMemNames.insertWithCheck(name);
    SCRIPT_ASSERT_RETURN(inserted, "duplicate memory", NULL);
    mem = new CarbonCfgMemory(this, signal, name, initFile,
                              maxAddrs, width, rt, comment, 
                              disassemblyName, disassemblerName);
    mMemoryVec.push_back(mem);
  }

  return mem;
}

CarbonCfgMemory* CarbonCfg::addMemory(const char* name, UInt32 width, UInt64 maxAddr)
{
  bool inserted = mMemNames.insertWithCheck(name);
  UtString msg;
  msg << "Duplicate memory: " << name;
  SCRIPT_ASSERT_RETURN(inserted, msg.c_str(), NULL);
  if (inserted)
  {
    CarbonCfgMemory* mem = new CarbonCfgMemory(this, name, width, maxAddr);
    mMemoryVec.push_back(mem);
    return mem;
  }
  return NULL;
}

void CarbonCfg::moveMemory(CarbonCfgMemory* srcMem)
{
  SCRIPT_ASSERT(srcMem, "moveMemory called with NULL memory.");
  CarbonCfg* srcComp = srcMem->getParent();

  // First find the memory in the source component and remove it from there
  for (MemoryVec::iterator iter = srcComp->mMemoryVec.begin(); iter != srcComp->mMemoryVec.end(); ++iter) {
    if(*iter == srcMem) {
      srcComp->mMemoryVec.erase(iter);
      break;
    }
  }

  // Adopt the memory and tell it who its new daddy is.
  mMemoryVec.push_back(srcMem);
  srcMem->putParent(this);
}

CarbonCfgCompCustomCode* CarbonCfg::addCustomCode()
{
  CarbonCfgCompCustomCode* customCode = new CarbonCfgCompCustomCode();
  mCustomCodes.addCustomCode(customCode);
  return customCode;
}

CarbonCfgPStream* CarbonCfg::addPStream(const char* name) {
  bool inserted = mPStreamNames.insertWithCheck(name);
  if (inserted)
  {
    CarbonCfgPStream* pstream = new CarbonCfgPStream(name, "");
    mPStreamVec.push_back(pstream);
    return pstream;
  }
  return NULL;
}

CarbonCfgXtor* CarbonCfg::findXtor(const char* name, const char* libraryName, const char* variant)
{
  // Look for the library
  CarbonCfgXtorLib* xtorLib = findXtorLib(libraryName);
  if (xtorLib != NULL) {
    return xtorLib->findXtor(name, variant);
  }
  return NULL;
}

bool CarbonCfg::addXtorLib(CarbonCfgXtorLib* xtorLib)
{
  const char* name = xtorLib->libName();
  XtorLibs::iterator pos = mXtorLibs.find(name);
  if (pos == mXtorLibs.end()) {
    mXtorLibs[name] = xtorLib;
    return true;
  } else {
    return false;
  }
}

CarbonCfgXtorLib* CarbonCfg::findXtorLib(const char* libraryName)
{
  XtorLibs::iterator pos = mXtorLibs.find(libraryName);
  if (pos != mXtorLibs.end()) {
    return pos->second;
  } else {
    return NULL;
  }
}

CarbonCfgXtorID carbonCfgFindXtor(CarbonCfgID cfg, const char * name) {
  return cfg->findXtor(name, CARBON_DEFAULT_XTOR_LIB);
}

void carbonCfgDestroy(CarbonCfgID cfg) {
  delete cfg;
}
  

/*! write a config to a file, return 0 on success, 1 on failure */
/*! If an error occurs, get it with carbonCfgGetErrmsg(CarbonCfgID); */
CarbonCfgStatus carbonCfgWrite(CarbonCfgID cfg,
                                          const char* filename)
{
  return cfg->write(filename);
}

/*! get the last error message, or NULL if there was never an error */
const char* carbonCfgGetErrmsg(CarbonCfgID cfg) {
  return cfg->getErrmsg();
}

/* get number of warnings in last check */
CarbonUInt32 carbonCfgGetNumWarnings(CarbonCfgID cfg) {
  return cfg->getNumWarnings();
}

/* get number of errors in last check */
CarbonUInt32 carbonCfgGetNumErrors(CarbonCfgID cfg) {
  return cfg->getNumErrors();
}

CarbonCfgRTLConnectionID carbonCfgClockGen(CarbonCfgRTLPort* rtlPort,
                                           unsigned int initialVal, unsigned int delay,
                                           unsigned int clockCycles, unsigned int compCycles,
                                           unsigned int duty)
{
  CarbonCfgClockGen* cg = new CarbonCfgClockGen(rtlPort, initialVal, delay, clockCycles, compCycles, duty);
  rtlPort->connect(cg);
  return cg;
}

CarbonCfgRTLConnectionID carbonCfgTie(CarbonCfgRTLPort* rtlPort,
                                      const unsigned int* src,
                                      unsigned int numVals)
{
  DynBitVector tieVal(rtlPort->getWidth());
  UInt32* vals = tieVal.getUIntArray();
  numVals = std::min(numVals, tieVal.getUIntArraySize());
  for (UInt32 i = 0; i < numVals; ++i) {
    vals[i] = src[i];
  }
  CarbonCfgRTLConnectionID tie = new CarbonCfgTie(rtlPort, tieVal);
  rtlPort->connect(tie);
  return tie;
}

CarbonCfgRTLConnectionID carbonCfgTieParam(CarbonCfgRTLPort* rtlPort,
                                           const char* xtorInstanceName,
                                           const char* defparam)
{
  CarbonCfgRTLConnectionID tie = new CarbonCfgTieParam(rtlPort, xtorInstanceName, defparam);
  rtlPort->connect(tie);
  return tie;
}

unsigned int carbonCfgNumRTLPorts(CarbonCfgID cfg) {
  return cfg->numRTLPorts();
}

CarbonCfgRTLPortID carbonCfgGetRTLPort(CarbonCfgID cfg, unsigned int i) {
  return cfg->getRTLPort(i);
}

CarbonCfgRTLPort* CarbonCfg::findRTLPort(const char* name) {
  RTLPortMap::iterator p = mRTLPortMap.find(name);
  if (p != mRTLPortMap.end()) {
    return p->second;
  }
  return NULL;
}

CarbonCfgRTLPortID carbonCfgFindRTLPort(CarbonCfgID cfg, const char* name) {
  return cfg->findRTLPort(name);
}
  

CarbonCfgRTLPort* CarbonCfg::addRTLPort(const char* name, UInt32 width,
                                        CarbonCfgRTLPortType type)
{
  if (mRTLPortMap.find(name) == mRTLPortMap.end()) {
    CarbonCfgRTLPort* rtlPort = new CarbonCfgRTLPort(name, width, type);
    mRTLPortVec.push_back(rtlPort);
    mRTLPortMap[name] = rtlPort;
    return rtlPort;
  }
  return NULL;
}

CarbonCfgRTLPortID carbonCfgAddRTLPort(CarbonCfgID cfg, const char* name,
                                       unsigned int width, CarbonCfgRTLPortType type)
{
  return cfg->addRTLPort(name, width, type);
}

const char* carbonCfgRTLPortGetName(CarbonCfgRTLPortID rtlPort) {
  return rtlPort->getName();
}

unsigned int carbonCfgRTLPortGetWidth(CarbonCfgRTLPortID rtlPort) {
  return rtlPort->getWidth();
}

CarbonCfgRTLPortType carbonCfgRTLPortGetType(CarbonCfgRTLPortID rtlPort) {
  return rtlPort->getType();
}

unsigned int carbonCfgRTLPortNumConnections(CarbonCfgRTLPortID rtlPort) {
  return rtlPort->numConnections();
}

CarbonCfgRTLConnectionID carbonCfgRTLPortGetConnection(CarbonCfgRTLPortID rtlPort,
                                                       unsigned int i)
{
  return rtlPort->getConnection(i);
}

CarbonCfgRTLConnectionType carbonCfgRTLConnectionGetType(CarbonCfgRTLConnectionID conn)
{
  return conn->getType();
}

CarbonCfgClockGenID carbonCfgCastClockGen(CarbonCfgRTLConnectionID conn) {
  return conn->castClockGen();
}
CarbonCfgResetGenID carbonCfgCastResetGen(CarbonCfgRTLConnectionID conn) {
  return conn->castResetGen();
}
CarbonCfgTieID carbonCfgCastTie(CarbonCfgRTLConnectionID conn) {
  return conn->castTie();
}
CarbonCfgTieParamID carbonCfgCastTieParam(CarbonCfgRTLConnectionID conn) {
  return conn->castTieParam();
}
CarbonCfgESLPortID carbonCfgCastESLPort(CarbonCfgRTLConnectionID conn) {
  return conn->castESLPort();
}
CarbonCfgXtorConnID carbonCfgCastXtorConn(CarbonCfgRTLConnectionID conn) {
  return conn->castXtorConn();
}
CarbonCfgSystemCClockID carbonCfgCastSystemCClock(CarbonCfgRTLConnectionID conn) {
  return conn->castSystemCClock();
}

CarbonCfgESLPortID carbonCfgAddESLPort(CarbonCfgID cfg,
                                       CarbonCfgRTLPortID rtlPort,
                                       const char* name,
                                       CarbonCfgESLPortType type)
{
  return cfg->addESLPort(rtlPort, name, type, NULL, eCarbonCfgESLPortUndefined);
}

unsigned int carbonCfgNumESLPorts(CarbonCfgID cfg) {
  return cfg->numESLPorts();
}

CarbonCfgESLPortID carbonCfgGetESLPort(CarbonCfgID cfg, unsigned int i) {
  return cfg->getESLPort(i);
}

CarbonCfgESLPort* CarbonCfg::findESLPort(const char* name) {
  ESLPortMap::iterator p = mESLPortMap.find(name);
  if (p != mESLPortMap.end()) {
    return p->second;
  }
  return NULL;
}

CarbonCfgESLPortID carbonCfgFindESLPort(CarbonCfgID cfg, const char* name) {
  return cfg->findESLPort(name);
}
  

unsigned int carbonCfgNumXtorInstances(CarbonCfgID cfg) {
  return cfg->numXtorInstances();
}

CarbonCfgXtorInstanceID carbonCfgGetXtorInstance(CarbonCfgID cfg,
                                                 unsigned int i)
{
  return cfg->getXtorInstance(i);
}

unsigned int carbonCfgXtorInstanceNumConnections(CarbonCfgXtorInstanceID xi) {
  return xi->getType()->numPorts();
}

CarbonCfgXtorConnID carbonCfgXtorInstanceGetConnection(CarbonCfgXtorInstanceID xi, unsigned int i) {
  return xi->getConnection(i);
}


CarbonCfgXtorID carbonCfgXtorInstanceGetXtor(CarbonCfgXtorInstanceID xi) {
  return xi->getType();
}

const char* carbonCfgXtorInstanceGetName(CarbonCfgXtorInstanceID xi) {
  return xi->getName();
}

const char* carbonCfgXtorInstanceFindParameter(CarbonCfgXtorInstanceID xi,
                                               const char* paramName)
{
  return xi->findParameter(paramName);
}

unsigned int carbonCfgXtorInstanceNumParams(CarbonCfgXtorInstanceID xi)
{
  return xi->numParams();
}

CarbonCfgXtorParamInstID carbonCfgXtorInstanceGetParam(CarbonCfgXtorInstanceID xi, unsigned int i)
{
  return xi->getParamInstance(i, false); // hidden parameters are returned as null
}

unsigned int carbonCfgXtorInstanceUseDebugAccess(CarbonCfgXtorInstanceID xi)
{
  return xi->useDebugAccess();
}

unsigned int carbonCfgNumParams(CarbonCfgID cfg)
{
  return cfg->numParams();
}

CarbonCfgXtorParamInstID carbonCfgGetParam(CarbonCfgID cfg, unsigned int i)
{
  return cfg->getParam(i);
}

const char* carbonCfgParamGetName(CarbonCfgXtorParamInstID parm)
{
  return parm->getParam()->getName();
}

unsigned int carbonCfgParamGetSize(CarbonCfgXtorParamInstID parm)
{
  return parm->getParam()->getSize();
}

const char* carbonCfgParamGetValue(CarbonCfgXtorParamInstID parm)
{
  return parm->getValue();
}

const char* carbonCfgParamGetDefaultValue(CarbonCfgXtorParamInstID parm)
{
  return parm->getDefaultValue();
}

const char* carbonCfgParamGetType(CarbonCfgXtorParamInstID parm)
{
  CarbonCfgParamDataTypeIO param_type(parm->getParam()->getType());
  return param_type.getString();
}

const char* carbonCfgParamGetFlag(CarbonCfgXtorParamInstID parm)
{
  CarbonCfgParamFlagIO param_flag(parm->getParam()->getFlag());
  return param_flag.getString();
}

const char* carbonCfgParamGetDescription(CarbonCfgXtorParamInstID parm)
{
  return parm->getParam()->getDescription();
}

const char* carbonCfgParamGetEnumChoices(CarbonCfgXtorParamInstID parm)
{
  // Note this gets all the enum choices
  return parm->getParam()->getEnumChoices();
}

unsigned int carbonCfgParamNumEnumChoices(CarbonCfgXtorParamInstID parm)
{
  // This only gets the enum choice subset size for this parameter instance
  return parm->numEnumChoices();
}

const char* carbonCfgParamGetEnumChoice(CarbonCfgXtorParamInstID param, unsigned int i)
{
  // This only gets the enum choice subset for this parameter instance
  return param->getEnumChoice(i);
}

CarbonCfgXtorInstanceID carbonCfgXtorInstanceGetClockMaster(CarbonCfgXtorInstanceID xi)
{
  return xi->getClockMaster();
}
  
const char* carbonCfgXtorInstanceGetESLClockMaster(CarbonCfgXtorInstanceID xi)
{
  return xi->getESLClockMaster();
}
  
const char* carbonCfgXtorInstanceGetESLResetMaster(CarbonCfgXtorInstanceID xi)
{
  return xi->getESLResetMaster();
}

CarbonCfgXtorAbstractionID carbonCfgXtorInstanceGetAbstraction(CarbonCfgXtorInstance* xi)
{
  return xi->getAbstraction();
}
  
const char* carbonCfgXtorGetLibraryName(CarbonCfgXtorID xtor) {
  return xtor->getLibraryName();
}

const char* carbonCfgXtorGetVariant(CarbonCfgXtorID xtor) {
  return xtor->getVariant();
}

unsigned int carbonCfgXtorUseESLClockMaster(CarbonCfgXtorID xtor)
{
  return xtor->useESLClockMaster();
}

unsigned int carbonCfgXtorUseESLResetMaster(CarbonCfgXtorID xtor)
{
  return xtor->useESLResetMaster();
}

const char* carbonCfgXtorGetName(CarbonCfgXtorID xtor) {
  return xtor->getName();
}

unsigned int carbonCfgXtorHasWriteDebug(CarbonCfgXtorID xtor) {
  return xtor->hasWriteDebug();
}

const char* carbonCfgXtorGetClassName(CarbonCfgXtorID xtor) {
  return xtor->getClassName();
}

unsigned int carbonCfgXtorNumIncludeFiles(CarbonCfgXtorID xtor) {
  return xtor->numIncludeFiles();
}

const char* carbonCfgXtorGetIncludeFile(CarbonCfgXtorID xtor, unsigned int index) {
  return xtor->getIncludeFile(index);
}

unsigned int carbonCfgXtorIsXtorClockMaster(CarbonCfgXtorID xtor) {
  return xtor->isXtorClockMaster();
}

unsigned int carbonCfgXtorIsFlowThru(CarbonCfgXtorID xtor) {
  return xtor->isFlowThru();
}

unsigned int carbonCfgXtorIsSaveRestore(CarbonCfgXtorID xtor) {
  return xtor->isSaveRestore();
}

unsigned int carbonCfgXtorIsMaster(CarbonCfgXtorID xtor) {
  return xtor->isMaster();
}

unsigned int carbonCfgXtorIsSlave(CarbonCfgXtorID xtor) {
  return xtor->isSlave();
}

unsigned int carbonCfgXtorIsAmba(CarbonCfgXtorID xtor) {
  return xtor->isAmba();
}

unsigned int carbonCfgXtorNumSystemCPorts(CarbonCfgXtorID xtor) {
  return xtor->numSystemCPorts();
}
CarbonCfgXtorSystemCPortID carbonCfgXtorGetSystemCPort(CarbonCfgXtorID xtor, unsigned int i) {
  return xtor->getSystemCPort(i);
}

unsigned int carbonCfgXtorHasDebugTransaction(CarbonCfgXtorID xtor) {
  return xtor->hasDebugTransaction();
}

const char* carbonCfgXtorGetProtocolName(CarbonCfgXtorID xtor) {
  return xtor->getProtocolName();
}
unsigned int carbonCfgXtorNumLibFiles(CarbonCfgXtorID xtor) {
  return xtor->numLibFiles();
}
const char* carbonCfgXtorGetLibFile(CarbonCfgXtorID xtor, unsigned int index) {
  return xtor->getLibFile(index);
}
unsigned int carbonCfgXtorNumIncPaths(CarbonCfgXtorID xtor) {
  return xtor->numIncPaths();
}
const char* carbonCfgXtorGetIncPath(CarbonCfgXtorID xtor, unsigned int index) {
  return xtor->getIncPath(index);
}
unsigned int carbonCfgXtorNumLibPaths(CarbonCfgXtorID xtor) {
  return xtor->numLibPaths();
}
const char* carbonCfgXtorGetLibPath(CarbonCfgXtorID xtor, unsigned int index) {
  return xtor->getLibPath(index);
}
unsigned int carbonCfgXtorNumFlags(CarbonCfgXtorID xtor) {
  return xtor->numFlags();
}
const char* carbonCfgXtorGetFlag(CarbonCfgXtorID xtor, unsigned int index) {
  return xtor->getFlag(index);
}
const char* carbonCfgXtorGetConstructorArgs(CarbonCfgXtorID xtor) {
  return xtor->getConstructorArgs();
}
unsigned int carbonCfgXtorIsSDXactor(CarbonCfgXtorID xtor) {
  return xtor->isSDXactor();
}
unsigned int carbonCfgXtorUseEventQueue(CarbonCfgXtorID xtor) {
  return xtor->useEventQueue();
}
unsigned int carbonCfgXtorPreservePortNameCase(CarbonCfgXtorID xtor) {
  return xtor->preservePortNameCase();
}
unsigned int carbonCfgXtorUseCommunicate(CarbonCfgXtorID xtor) {
  return xtor->useCommunicate();
}

unsigned int carbonCfgXtorNumTemplateArgs(CarbonCfgXtorID xtor)
{
  return xtor->numTemplateArgs();
}

CarbonCfgXtorTemplateArgID carbonCfgXtorGetTemplateArg(CarbonCfgXtorID xtor, unsigned int index)
{
  return xtor->getTemplateArg(index);
}

unsigned int carbonCfgXtorNumAbstractions(CarbonCfgXtorID xtor)
{
  return xtor->numAbstractions();
}

CarbonCfgXtorAbstractionID carbonCfgXtorGetAbstraction(CarbonCfgXtorID xtor, unsigned int index)
{
  return xtor->getAbstraction(index);
}

const char* carbonCfgXtorTemplateArgGetType(CarbonCfgXtorTemplateArgID arg)
{
  return arg->getType();
}

const char* carbonCfgXtorTemplateArgGetValue(CarbonCfgXtorTemplateArgID arg)
{
  return arg->getValue();
}

const char* carbonCfgXtorTemplateArgGetDefault(CarbonCfgXtorTemplateArgID arg)
{
  return arg->getDefault();
}

CarbonCfgXtorClassTemplateArgID carbonCfgXtorCastClassTemplateArg(CarbonCfgXtorTemplateArgID arg)
{
  return arg->castClassTemplateArg();
}

CarbonCfgXtorPortWidthTemplateArgID carbonCfgXtorCastPortWidthTemplateArg(CarbonCfgXtorTemplateArgID arg)
{
  return arg->castPortWidthTemplateArg();
}

CarbonCfgXtorTextTemplateArgID carbonCfgXtorCastTextTemplateArg(CarbonCfgXtorTemplateArgID arg)
{
  return arg->castTextTemplateArg();
}

unsigned int carbonCfgXtorPortWidthTemplateArgGetBusDataWidth(CarbonCfgXtorPortWidthTemplateArgID arg)
{
  return arg->isBusDataWidth();
}

const char* carbonCfgXtorAbstractionGetName(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->getName();
}

const char* carbonCfgXtorAbstractionGetClassName(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->getClassName();
}

unsigned int carbonCfgXtorAbstractionNumIncludeFiles(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->numIncludeFiles();
}

const char* carbonCfgXtorAbstractionGetIncludeFile(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index)
{
  return xtorAbstraction->getIncludeFile(index);
}

unsigned int carbonCfgXtorAbstractionNumLibFiles(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->numLibFiles();
}

const char* carbonCfgXtorAbstractionGetLibFile(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index)
{
  return xtorAbstraction->getLibFile(index);
}

unsigned int carbonCfgXtorAbstractionNumIncPaths(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->numIncPaths();
}

const char* carbonCfgXtorAbstractionGetIncPath(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index)
{
  return xtorAbstraction->getIncPath(index);
}

unsigned int carbonCfgXtorAbstractionNumLibPaths(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->numLibPaths();
}

const char* carbonCfgXtorAbstractionGetLibPath(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index)
{
  return xtorAbstraction->getLibPath(index);
}

unsigned int carbonCfgXtorAbstractionNumFlags(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->numFlags();
}

const char* carbonCfgXtorAbstractionGetFlag(CarbonCfgXtorAbstractionID xtorAbstraction, unsigned int index)
{
  return xtorAbstraction->getFlag(index);
}

const char* carbonCfgXtorAbstractionGetLayerId(CarbonCfgXtorAbstractionID xtorAbstraction)
{
  return xtorAbstraction->getLayerId();
}

CarbonCfgXtorInstance* CarbonCfg::findXtorInstance(const char* name) {
  XtorMap::iterator p = mXtorMap.find(name);
  if (p != mXtorMap.end()) {
    return p->second;
  }
  return NULL;
}

CarbonCfgXtorInstanceID carbonCfgFindXtorInstance(CarbonCfgID cfg,
                                                  const char* name) {
  return cfg->findXtorInstance(name);
}
  


/*
void carbonCfgAddRegister(CarbonCfgID cfg,
                          const char* signal, const char* debugName,
                          const char* groupName, unsigned int width,
                          CarbonCfgRadix radix, const char* comment)
{
  cfg->addRegister(signal, debugName, groupName, width, radix, comment);
}
*/

void carbonCfgRemoveAllRegisterInfo(CarbonCfgID cfg) {
  cfg->clearRegister();
}

void carbonCfgAddMemory(CarbonCfgID cfg,
                        const char* signal, const char* name,
                        const char* initFile, unsigned long long maxAddrs,
                        unsigned int width, CarbonCfgReadmemType rt,
                        const char* comment, const char* disassemblyName,
            const char* disassemblerName)
{
  cfg->addMemory(signal, name, initFile, maxAddrs, width, rt, comment, disassemblyName, disassemblerName);
}

void carbonCfgRemoveAllMemoryInfo(CarbonCfgID cfg) {
  cfg->clearMemory();
}

void carbonCfgRemoveAllCustomCode(CarbonCfgID cfg)
{
  cfg->clearCustomCode();
}


/*! add a reset */
/*!
 *! add a reset signal, specifying initial value (asserted), plus minimum
 *! number of carbon beats that the signal must remain asserted
 */
CarbonCfgRTLConnectionID carbonCfgResetGen(CarbonCfgRTLPort* rtlPort,
                                           UInt64 activeValue, UInt64 inactiveValue, 
                                           UInt32 clockCycles, UInt32 compCycles,
                                           UInt32 cyclesBefore, UInt32 cyclesAsserted, UInt32 cyclesAfter)
{
  CarbonCfgRTLConnectionID rg =
    new CarbonCfgResetGen(rtlPort, activeValue, inactiveValue, clockCycles, compCycles, cyclesBefore, cyclesAsserted, cyclesAfter);
  rtlPort->connect(rg);
  return rg;
}

CarbonCfgXtorInstance* CarbonCfg::addXtor(const char* inst_name,
                                          CarbonCfgXtor* xtor)
{
  // Find the library
  XtorLibs::iterator pos = mXtorLibs.find(xtor->getLibraryName());
  if (pos != mXtorLibs.end()) {
    if (mESLPortNames.find(inst_name) == mESLPortNames.end()) {
      CarbonCfgXtorInstance* i = new CarbonCfgXtorInstance(inst_name, xtor);
      if (i == NULL) {
        return NULL;
      }
      mXtorMap[inst_name] = i;
      mXtorVec.push_back(i);
      mESLPortNames.insert(inst_name);
      return i;
    }
  }
  return NULL;
}

void CarbonCfg::addParam(CarbonCfgXtorParamInst* param)
{
  mParamVec.push_back(param);
}

void CarbonCfg::removeParam(CarbonCfgXtorParamInst* param)
{
  for(CarbonCfg::ParamVec::iterator i = mParamVec.begin(); i != mParamVec.end(); ++i)
    if((*i) == param) {
      delete (*i)->getParam();
      mParamVec.erase(i);
      break;
    }
}

/*! add a transactor */
/*!
 *! This identifies a single port to represent an entire transactor
 *! interface to a Carbon model.  The individual Carbon model ports
 *! will have to be connected with carbonCfgConnectXtor
 */
CarbonCfgXtorInstance* carbonCfgAddXtor(CarbonCfgID cfg,
                                        const char* xtorInstance,
                                        CarbonCfgXtorID xtorType)
{
  return cfg->addXtor(xtorInstance, xtorType);
}

unsigned int carbonCfgClockGetDelay(CarbonCfgClockGenID clock) {
  return clock->mDelay;
}

unsigned int carbonCfgClockGetInitialVal(CarbonCfgClockGenID clock) {
  return clock->mInitialValue;
}

unsigned int carbonCfgClockGetClockCycles(CarbonCfgClockGenID clock) {
  return clock->mFrequency.getClockCycles();
}

unsigned int carbonCfgClockGetCompCycles(CarbonCfgClockGenID clock) {
  return clock->mFrequency.getCompCycles();
}

unsigned int carbonCfgClockGetDutyCycle(CarbonCfgClockGenID clock) {
  return clock->mDutyCycle;
}

double carbonCfgSystemCClockPeriod(CarbonCfgSystemCClockID scClock)
{
  return scClock->getPeriod();
}

const char* carbonCfgSystemCClockPeriodUnits(CarbonCfgSystemCClockID scClock)
{
  CarbonCfgSystemCTimeUnitsIO timeUnits = scClock->getPeriodUnits();
  return timeUnits.getString();
}

double carbonCfgSystemCClockDutyCycle(CarbonCfgSystemCClockID scClock)
{
  return scClock->getDutyCycle();
}

double carbonCfgSystemCClockStartTime(CarbonCfgSystemCClockID scClock)
{
  return scClock->getStartTime();
}

const char* carbonCfgSystemCClockStartTimeUnits(CarbonCfgSystemCClockID scClock)
{
  CarbonCfgSystemCTimeUnitsIO timeUnits = scClock->getStartTimeUnits();
  return timeUnits.getString();
}

unsigned int carbonCfgSystemCClockInitialValue(CarbonCfgSystemCClockID scClock)
{
  return scClock->getInitialValue();
}


UInt64 carbonCfgResetGetActiveValue(CarbonCfgResetGenID reset) {
  return reset->mActiveValue;
}

unsigned int carbonCfgResetGetActiveValueHigh(CarbonCfgResetGenID reset) {
  UInt32 word;
  CarbonValRW::cpSrcToDestWord(&word, &reset->mActiveValue, 1);
  return word;
}

unsigned int carbonCfgResetGetActiveValueLow(CarbonCfgResetGenID reset) {
  UInt32 word;
  CarbonValRW::cpSrcToDestWord(&word, &reset->mActiveValue, 0);
  return word;
}

UInt64 carbonCfgResetGetInactiveValue(CarbonCfgResetGenID reset) {
  return reset->mInactiveValue;
}

unsigned int carbonCfgResetGetInactiveValueHigh(CarbonCfgResetGenID reset) {
  UInt32 word;
  CarbonValRW::cpSrcToDestWord(&word, &reset->mInactiveValue, 1);
  return word;
}

unsigned int carbonCfgResetGetInactiveValueLow(CarbonCfgResetGenID reset) {
  UInt32 word;
  CarbonValRW::cpSrcToDestWord(&word, &reset->mInactiveValue, 0);
  return word;
}

unsigned int carbonCfgResetGetClockCycles(CarbonCfgResetGenID reset) {
  return reset->mFrequency.getClockCycles();
}

unsigned int carbonCfgResetGetCompCycles(CarbonCfgResetGenID reset) {
  return reset->mFrequency.getCompCycles();
}

unsigned int carbonCfgResetGetCyclesBefore(CarbonCfgResetGenID reset) {
  return reset->mCyclesBefore;
}

unsigned int carbonCfgResetGetCyclesAsserted(CarbonCfgResetGenID reset) {
  return reset->mCyclesAsserted;
}

unsigned int carbonCfgResetGetCyclesAfter(CarbonCfgResetGenID reset) {
  return reset->mCyclesAfter;
}

void carbonCfgClear(CarbonCfgID cfg) {
  cfg->clear();
}

void carbonCfgPutDescription(CarbonCfgID cfg, const char* str) {
  cfg->putDescription(str);
}

const char* carbonCfgGetDescription(CarbonCfgID cfg) {
  return cfg->getDescription();
}
  
void carbonCfgPutCxxFlags(CarbonCfgID cfg, const char* str) {
  cfg->putCxxFlags(str);
}

const char* carbonCfgGetCxxFlags(CarbonCfgID cfg) {
  return cfg->getCxxFlags();
}
  
void carbonCfgPutLinkFlags(CarbonCfgID cfg, const char* str) {
  cfg->putLinkFlags(str);
}

const char* carbonCfgGetLinkFlags(CarbonCfgID cfg) {
  return cfg->getLinkFlags();
}

void carbonCfgPutSourceFiles(CarbonCfgID cfg, const char* str) {
  cfg->putSourceFiles(str);
}

const char* carbonCfgGetSourceFiles(CarbonCfgID cfg) {
  return cfg->getSourceFiles();
}

void carbonCfgPutIncludeFiles(CarbonCfgID cfg, const char* str) {
  cfg->putIncludeFiles(str);
}

const char* carbonCfgGetIncludeFiles(CarbonCfgID cfg) {
  return cfg->getIncludeFiles();
}

unsigned int carbonCfgIsStandAloneComp(CarbonCfgID cfg) {
  return cfg->isStandAloneComp();
}

unsigned int carbonCfgIsSubComponentFullName(CarbonCfgID cfg) {
  return cfg->isSubComponentFullName();
}

void carbonCfgPutLoadfileExtension(CarbonCfgID cfg, const char* str) {
  cfg->putLoadfileExtension(str);
}

const char* carbonCfgGetLoadfileExtension(CarbonCfgID cfg) {
  return cfg->getLoadfileExtension();
}

void carbonCfgPutUseStaticScheduling(CarbonCfgID cfg, unsigned int val) {
  cfg->putUseStaticScheduling(val);
}

unsigned int carbonCfgGetUseStaticScheduling(CarbonCfgID cfg) {
  return cfg->getUseStaticScheduling();
}

void carbonCfgPutLibName(CarbonCfgID cfg, const char* str) {
  cfg->putLibName(str);
}

const char* carbonCfgGetLibName(CarbonCfgID cfg) {
  return cfg->getLibName();
}

void carbonCfgPutLegacyMemories(CarbonCfgID cfg, int val) {
  cfg->putLegacyMemories(val);
}

int carbonCfgUseLegacyMemories(CarbonCfgID cfg) {
  return cfg->useLegacyMemories();
}

void carbonCfgPutCompName(CarbonCfgID cfg, const char* str) {
  cfg->putCompName(str);
}

const char* carbonCfgGetCompName(CarbonCfgID cfg) {
  return cfg->getCompName();
}
  
void carbonCfgPutCompDisplayName(CarbonCfgID cfg, const char* str) {
  cfg->putCompDisplayName(str);
}

const char* carbonCfgGetCompDisplayName(CarbonCfgID cfg) {
  return cfg->getCompDisplayName();
}
  
void carbonCfgPutTopModuleName(CarbonCfgID cfg, const char* str) {
  cfg->putTopModuleName(str);
}

const char* carbonCfgGetTopModuleName(CarbonCfgID cfg) {
  return cfg->getTopModuleName();
}
  
void carbonCfgPutIODBFile(CarbonCfgID cfg, const char* str) {
  cfg->putIODBFileName(str);
}

const char* carbonCfgGetIODBFile(CarbonCfgID cfg) {
  return cfg->getIODBFileName();
}
  
unsigned int carbonCfgNumRegisters(CarbonCfgID cfg) {
  return cfg->numRegisters();
}

CarbonCfgRegisterID carbonCfgGetRegister(CarbonCfgID cfg, unsigned int index)
{
  return cfg->getRegister(index);
}

const char* carbonCfgRegisterGetName(CarbonCfgRegisterID reg) {
  return reg->getName();
}

unsigned int carbonCfgRegisterGetOffset(CarbonCfgRegisterID reg) {
  return reg->getOffset();
}

void carbonCfgRegisterPutGroup(CarbonCfgID cfg, CarbonCfgRegisterID reg, const char* groupName) {
  cfg->changeRegGroup(reg, groupName);
}

const char* carbonCfgRegisterGetGroup(CarbonCfgRegisterID reg) {
  return reg->getGroupName();
}

unsigned int carbonCfgRegisterGetWidth(CarbonCfgRegisterID reg) {
  return reg->getWidth();
}

CarbonCfgRadix carbonCfgRegisterGetRadix(CarbonCfgRegisterID reg) {
  return reg->getRadix();
}

const char* carbonCfgRegisterGetPort(CarbonCfgRegisterID reg) {
  return reg->getPort();
}

const char* carbonCfgRegisterGetComment(CarbonCfgRegisterID reg) {
  return reg->getComment();
}

unsigned int carbonCfgRegisterIsBigEndian(CarbonCfgRegisterID reg)
{
  return reg->getBigEndian();
}

unsigned int carbonCfgRegisterIsPcReg(CarbonCfgRegisterID reg)
{
  return reg->getPcReg();
}

unsigned int carbonCfgRegisterNumFields(CarbonCfgRegisterID reg)
{
  return reg->numFields();
}

CarbonCfgRegisterFieldID carbonCfgRegisterGetField(CarbonCfgRegisterID reg, unsigned int index)
{
  return reg->getField(index);
}

unsigned int carbonCfgRegisterNumCustomCodes(CarbonCfgRegisterID reg)
{
  return reg->numCustomCodes();
}

CarbonCfgCustomCode* carbonCfgRegisterGetCustomCode(CarbonCfgRegisterID reg, unsigned int index)
{
  return reg->getCustomCode(index);
}

// Register field functions
unsigned int carbonCfgRegisterFieldGetHigh(CarbonCfgRegisterFieldID field)
{
  return field->getHigh();
}

unsigned int carbonCfgRegisterFieldGetLow(CarbonCfgRegisterFieldID field)
{
  return field->getLow();
}

const char* carbonCfgRegisterFieldGetName(CarbonCfgRegisterFieldID field)
{
  return field->getName();
}

CarbonCfgRegAccessType carbonCfgRegisterFieldGetAccess(CarbonCfgRegisterFieldID field)
{
  return field->getAccess();
}

CarbonCfgRegisterLocID carbonCfgRegisterFieldGetLoc(CarbonCfgRegisterFieldID field)
{
  return field->getLoc();
}

// Register location functions
CarbonCfgRegLocType carbonCfgRegisterLocGetType(CarbonCfgRegisterLocID loc)
{
  return loc->getType();
}

CarbonCfgRegisterLocConstantID carbonCfgRegisterLocCastConstant(CarbonCfgRegisterLocID loc)
{
  return loc->castConstant();
}

CarbonCfgRegisterLocRegID carbonCfgRegisterLocCastReg(CarbonCfgRegisterLocID loc)
{
  return loc->castReg();
}

CarbonCfgRegisterLocArrayID carbonCfgRegisterLocCastArray(CarbonCfgRegisterLocID loc)
{
  return loc->castArray();
}

CarbonCfgRegisterLocUserID carbonCfgRegisterLocCastUser(CarbonCfgRegisterLocID loc)
{
  return loc->castUser();
}

unsigned long long carbonCfgRegisterLocConstantGetValue(CarbonCfgRegisterLocConstantID loc)
{
  return loc->getValue();
}

const char *carbonCfgRegisterLocRegGetPath(CarbonCfgRegisterLocRegID loc)
{
  return loc->getPath();
}

unsigned int carbonCfgRegisterLocRegGetHasRange(CarbonCfgRegisterLocRegID loc)
{
  return loc->getHasRange();
}

unsigned int carbonCfgRegisterLocRegGetLeft(CarbonCfgRegisterLocRegID loc)
{
  return loc->getLeft();
}

unsigned int carbonCfgRegisterLocRegGetRight(CarbonCfgRegisterLocRegID loc)
{
  return loc->getRight();
}

unsigned int carbonCfgRegisterLocRegGetLSB(CarbonCfgRegisterLocRegID loc)
{
  return loc->getLSB();
}

unsigned int carbonCfgRegisterLocRegGetMSB(CarbonCfgRegisterLocRegID loc)
{
  return loc->getMSB();
}

const char *carbonCfgRegisterLocArrayGetPath(CarbonCfgRegisterLocArrayID loc)
{
  return loc->getPath();
}

unsigned int carbonCfgRegisterLocArrayGetHasRange(CarbonCfgRegisterLocArrayID loc)
{
  return loc->getHasRange();
}

unsigned int carbonCfgRegisterLocArrayGetLeft(CarbonCfgRegisterLocArrayID loc)
{
  return loc->getLeft();
}

unsigned int carbonCfgRegisterLocArrayGetRight(CarbonCfgRegisterLocArrayID loc)
{
  return loc->getRight();
}

unsigned int carbonCfgRegisterLocArrayGetLSB(CarbonCfgRegisterLocArrayID loc)
{
  return loc->getLSB();
}

unsigned int carbonCfgRegisterLocArrayGetMSB(CarbonCfgRegisterLocArrayID loc)
{
  return loc->getMSB();
}

unsigned int carbonCfgRegisterLocArrayGetIndex(CarbonCfgRegisterLocArrayID loc)
{
  return loc->getIndex();
}

unsigned int carbonCfgTieGetNumWords(CarbonCfgTieID tie) {
  return tie->mValue.getUIntArraySize();
}

unsigned int carbonCfgTieGetWord(CarbonCfgTieID tie, unsigned int index) {
  return tie->mValue.getUIntArray()[index];
}

const char* carbonCfgTieParamGetFullParam(CarbonCfgTieParamID tie)
{
  return tie->mFullParamName.c_str();
}

const char* carbonCfgTieParamGetParam(CarbonCfgTieParamID tie)
{
  return tie->mParam.c_str();
}

const char* carbonCfgTieParamGetXtorInstanceName(CarbonCfgTieParamID tie)
{
  return tie->mXtorInstanceName.c_str();
}

CarbonCfgMemoryID carbonCfgAddMemory(CarbonCfgID cfg, const char* name, unsigned int width, unsigned long long max_addr) {
  return cfg->addMemory(name, width, max_addr);
}

CarbonCfgCustomCode* carbonCfgAddCustomCode(CarbonCfgID cfg)
{
  return cfg->addCustomCode();
}

unsigned int carbonCfgNumMemories(CarbonCfgID cfg) {
  return cfg->numMemories();
}

CarbonCfgMemoryID carbonCfgGetMemory(CarbonCfgID cfg, unsigned int i) {
  return cfg->getMemory(i);
}

unsigned int carbonCfgNumCustomCodes(CarbonCfgID cfg) {
  return cfg->numCustomCodes();
}

CarbonCfgCustomCode* carbonCfgGetCustomCode(CarbonCfgID cfg, unsigned int i) {
  return cfg->getCustomCode(i);
}

CarbonCfgCadi* carbonCfgGetCadi(CarbonCfgID cfg)
{
  return cfg->getCadi();
}

unsigned int carbonCfgCadiNumCustomCodes(CarbonCfgCadiID cadi)
{
  return cadi->numCustomCodes();
}

CarbonCfgCustomCode* carbonCfgCadiGetCustomCode(CarbonCfgCadiID cadi,
                                                unsigned int i)
{
  return cadi->getCustomCode(i);
}

const char* carbonCfgCadiGetDisassemblerName(CarbonCfgCadiID cadi){
  return cadi->getDisassemblerName();
}

const char* carbonCfgMemoryGetSignal(CarbonCfgMemoryID mem) {
  return mem->getPath();
}

const char* carbonCfgMemoryGetName(CarbonCfgMemoryID mem) {
  return mem->getName();
}

const char* carbonCfgMemoryGetInitFile(CarbonCfgMemoryID mem) {
  return mem->getInitFile();
}

CarbonCfgReadmemType carbonCfgMemoryGetReadmemType(CarbonCfgMemoryID mem) {
  return mem->getReadmemType();
}

unsigned long long carbonCfgMemoryGetMaxAddrs(CarbonCfgMemoryID mem) {
  return mem->getMaxAddrs();
}

unsigned int carbonCfgMemoryGetWidth(CarbonCfgMemoryID mem) {
  return mem->getWidth();
}

unsigned int carbonCfgMemoryGetProgramMemory(CarbonCfgMemoryID mem) {
  return mem->getProgramMemory();
}

const char* carbonCfgMemoryGetComment(CarbonCfgMemoryID mem) {
  return mem->getComment();
}

CarbonCfgMemInitType carbonCfgMemoryGetInitType(CarbonCfgMemoryID mem)
{
  return mem->getMemInitType();
}

UInt64 carbonCfgMemoryGetBaseAddr(CarbonCfgMemoryID mem)
{
  return mem->getBaseAddr();
}

UInt32 carbonCfgMemoryGetBaseAddrHi(CarbonCfgMemoryID mem)
{
  return mem->getBaseAddrHi();
}

UInt32 carbonCfgMemoryGetBaseAddrLo(CarbonCfgMemoryID mem)
{
  return mem->getBaseAddrLo();
}

const char* carbonCfgMemoryGetProgPreloadEslPort(CarbonCfgMemoryID mem)
{
  return mem->getSystemAddressESLPortName();
}

int carbonCfgMemoryGetDisplayAtZero(CarbonCfgMemoryID mem) {
  return mem->getDisplayAtZero();
}

void carbonCfgMemoryPutDisplayAtZero(CarbonCfgMemoryID mem, int val) {
  mem->putDisplayAtZero(val);
}

int carbonCfgMemoryGetBigEndian(CarbonCfgMemoryID mem) {
  return mem->getBigEndian();
}

void carbonCfgMemoryPutBigEndian(CarbonCfgMemoryID mem, int val) {
  mem->putBigEndian(val);
}

CarbonCfgMemoryBlockID carbonCfgMemoryAddMemoryBlock(CarbonCfgMemoryID mem)
{
  return mem->addMemoryBlock();
}

UInt32 carbonCfgMemoryNumBlocks(CarbonCfgMemoryID mem) {
  return mem->numMemoryBlocks();
}

CarbonCfgMemoryBlockID carbonCfgMemoryGetBlock(CarbonCfgMemoryID mem, unsigned int index)
{
  return mem->getMemoryBlock(index);
}

unsigned int carbonCfgMemoryNumCustomCodes(CarbonCfgMemoryID mem)
{
  return mem->numCustomCodes();
}

CarbonCfgCustomCodeID carbonCfgMemoryGetCustomCode(CarbonCfgMemoryID mem, unsigned int index)
{
  return mem->getCustomCode(index);
}


const char* carbonCfgMemoryBlockGetName(CarbonCfgMemoryBlockID block) {
  return block->getName();
}

UInt64 carbonCfgMemoryBlockGetBase(CarbonCfgMemoryBlockID block) {
  UInt64 base = block->getBase();
  return base;
}

UInt32 carbonCfgMemoryBlockGetBaseLo(CarbonCfgMemoryBlockID block) {
  UInt32 word;
  UInt64 base = block->getBase();
  CarbonValRW::cpSrcToDestWord(&word, &base, 0);
  return word;
}

UInt32 carbonCfgMemoryBlockGetBaseHi(CarbonCfgMemoryBlockID block) {
  UInt32 word;
  UInt64 base = block->getBase();
  CarbonValRW::cpSrcToDestWord(&word, &base, 1);
  return word;
}

UInt64 carbonCfgMemoryBlockGetSize(CarbonCfgMemoryBlockID block) {
  UInt64 size = block->getSize();
  return size;
}

UInt32 carbonCfgMemoryBlockGetSizeLo(CarbonCfgMemoryBlockID block) {
  UInt32 word;
  UInt64 size = block->getSize();
  CarbonValRW::cpSrcToDestWord(&word, &size, 0);
  return word;
}

UInt32 carbonCfgMemoryBlockGetSizeHi(CarbonCfgMemoryBlockID block) {
  UInt32 word;
  UInt64 size = block->getSize();
  CarbonValRW::cpSrcToDestWord(&word, &size, 1);
  return word;
}

UInt32 carbonCfgMemoryBlockGetWidth(CarbonCfgMemoryBlockID block) {
  return block->getWidth();
}

UInt32 carbonCfgMemoryBlockNumLocs(CarbonCfgMemoryBlockID block) {
  return block->numLocs();
}

unsigned int carbonCfgMemoryBlockNumCustomCodes(CarbonCfgMemoryBlockID block)
{
  return block->numCustomCodes();
}

CarbonCfgCustomCodeID carbonCfgMemoryBlockGetCustomCode(CarbonCfgMemoryBlockID block, unsigned int index)
{
  return block->getCustomCode(index);
}

CarbonCfgMemoryLocRTL* carbonCfgMemoryBlockAddLocRTL(CarbonCfgMemoryBlockID block, const char* path)
{
  return block->addLocRTL(path);
}

CarbonCfgMemoryLocPort* carbonCfgMemoryBlockAddLocPort(CarbonCfgMemoryBlockID block, const char* port)
{
  return block->addLocPort(port);
}

CarbonCfgMemoryLocID carbonCfgMemoryBlockGetLoc(CarbonCfgMemoryBlockID block, unsigned int index)
{
  return block->getLoc(index);
}

CarbonCfgMemLocType carbonCfgMemoryLocGetType(CarbonCfgMemoryLocID loc)
{
  return loc->getType();
}

CarbonCfgMemoryLocRTL* carbonCfgMemoryLocCastRTL(CarbonCfgMemoryLocID loc)
{
  return loc->castRTL();
}

CarbonCfgMemoryLocPort* carbonCfgMemoryLocCastPort(CarbonCfgMemoryLocID loc)
{
  return loc->castPort();
}

CarbonCfgMemoryLocUser* carbonCfgMemoryLocCastUser(CarbonCfgMemoryLocID loc)
{
  return loc->castUser();
}


const char* carbonCfgMemoryLocRTLGetPath(CarbonCfgMemoryLocRTLID loc) {
  return loc->getPath();
}

UInt32 carbonCfgMemoryLocRTLGetBitWidth(CarbonCfgMemoryLocRTLID loc) {
  return loc->getBitWidth();
}

UInt64 carbonCfgMemoryLocGetDisplayStartWordOffset(CarbonCfgMemoryLocID loc) {
  return loc->getDisplayStartWordOffset();
}

void carbonCfgMemoryLocPutDisplayStartWordOffset(CarbonCfgMemoryLocID loc, UInt64 val) {
  loc->putDisplayStartWordOffset(val);
}

UInt64 carbonCfgMemoryLocGetDisplayEndWordOffset(CarbonCfgMemoryLocID loc) {
  return loc->getDisplayEndWordOffset();
}

void carbonCfgMemoryLocPutDisplayEndWordOffset(CarbonCfgMemoryLocID loc, UInt64 val) {
  loc->putDisplayEndWordOffset(val);
}

void carbonCfgMemoryLocPutDisplayMsb(CarbonCfgMemoryLocID loc, UInt32 val) {
  loc->putDisplayMsb(val);
}

void carbonCfgMemoryLocPutDisplayLsb(CarbonCfgMemoryLocID loc, UInt32 val) {
  loc->putDisplayLsb(val);
}

UInt32 carbonCfgMemoryLocGetDisplayLsb(CarbonCfgMemoryLocID loc) {
  return loc->getDisplayLsb();
}

UInt32 carbonCfgMemoryLocGetDisplayMsb(CarbonCfgMemoryLocID loc) {
  return loc->getDisplayMsb();
}

UInt64 carbonCfgMemoryLocRTLGetStartWordOffset(CarbonCfgMemoryLocRTLID loc) {
  return loc->getRTLStartWordOffset();
}

void carbonCfgMemoryLocRTLPutStartWordOffset(CarbonCfgMemoryLocRTLID loc, UInt64 val) {
  loc->putRTLStartWordOffset(val);
}

UInt64 carbonCfgMemoryLocRTLGetEndWordOffset(CarbonCfgMemoryLocRTLID loc) {
  return loc->getRTLEndWordOffset();
}

void carbonCfgMemoryLocRTLPutEndWordOffset(CarbonCfgMemoryLocRTLID loc, UInt64 val) {
  loc->putRTLEndWordOffset(val);
}

void carbonCfgMemoryLocRTLPutMsb(CarbonCfgMemoryLocRTLID loc, UInt32 val) {
  loc->putRTLMsb(val);
}

void carbonCfgMemoryLocRTLPutLsb(CarbonCfgMemoryLocRTLID loc, UInt32 val) {
  loc->putRTLLsb(val);
}

UInt32 carbonCfgMemoryLocRTLGetLsb(CarbonCfgMemoryLocRTLID loc) {
  return loc->getRTLLsb();
}

UInt32 carbonCfgMemoryLocRTLGetMsb(CarbonCfgMemoryLocRTLID loc) {
  return loc->getRTLMsb();
}

const char* carbonCfgMemoryLocPortGetPortName(CarbonCfgMemoryLocPortID loc) {
  return loc->getPortName();
}

UInt32 carbonCfgMemoryLocPortHasFixedAddr(CarbonCfgMemoryLocPortID loc) {
  return loc->hasFixedAddress();
}

const char* carbonCfgMemoryLocUserGetName(CarbonCfgMemoryLocUserID loc) {
  return loc->getName();
}

const char* carbonCfgGetWaveFilename(CarbonCfgID cfg) {
  return cfg->getWaveFile();
}

CarbonCfgWaveType carbonCfgGetWaveType(CarbonCfgID cfg) {
  return cfg->getWaveType();
}

void carbonCfgPutWaveFilename(CarbonCfgID cfg, const char* file) {
  cfg->putWaveFile(file);
}

void carbonCfgPutWaveType(CarbonCfgID cfg, CarbonCfgWaveType type) {
  cfg->putWaveType(type);
}



// Xtor file format
//
// xtor xtor1
// port port_name width direction comment
// port ....
// parameter param_name type flags default_val
// parameter ...
//
// xtor xtor2
// ...
//
// This file is read-only, for now.  No need to write it

//! ctor
/*! Create a transactor library. This holds a number of transactors
 *
 *  The major/minor version default to 1.0 for backwards compatability to user xml files.
 */
CarbonCfgXtorLib::CarbonCfgXtorLib(const char* libName) :
  mLibName(libName),
  mMajorVersion(1),
  mMinorVersion(0)
{
}

void CarbonCfgXtorLib::putVersion(UInt32 major, UInt32 minor)
{
  mMajorVersion = major;
  mMinorVersion = minor;
}

//! dtor
CarbonCfgXtorLib::~CarbonCfgXtorLib() {
  mXtorVec.clearPointers();
}

//! read the transactor library
bool CarbonCfgXtorLib::readXtorDefinitions(const char* libfilename, UtString* errmsg)
{
  // nothing has gone wrong yet
  errmsg->clear();
  
  CfgXmlParserXtor parser;
  bool ok = parser.parseFile(libfilename, this);
  if (!ok) {
    *errmsg << "Error " << carbonCfgERR_PARSEERR << ": While parsing Transactor Definition. " << parser.errorText();
  }
  return ok;
}

//! read the transactor library
bool CarbonCfg::readCowareXtorDefinitions(const char* libfilename, UtString* errmsg)
{
  // nothing has gone wrong yet
  errmsg->clear();
  
  CfgCowareXmlParserXtor parser;
  bool ok = parser.parseFile(libfilename, this);
  if (!ok) {
    *errmsg << "Error: '" <<  parser.errorText() << "' while reading: " << libfilename;
  }
  return ok;
}

/*
  NOTE: This method has been replaced by readXtorDefinitions(), but it is still
        here for reference until all of the error checking has been 
        added to the XML parser flow.
 */
bool CarbonCfgXtorLib::read(const char* libfilename, UtString* errmsg) {
  UtParamFile libfile(libfilename);
  if (!libfile.is_open()) {
    *errmsg = libfile.getErrmsg();
    return false;
  }

  UtIStringStream line;
  CarbonCfgXtor* xtor = NULL;
  while (!libfile.bad() && libfile.getline(&line)) {
    UtString kwd, xtor_name, port_name, param_name, comment;
    CarbonCfgRTLPortTypeIO direction;
    CarbonCfgParamDataTypeIO param_type;
    CarbonCfgParamFlagIO param_flags;
    UInt32 width = 0;
    line >> kwd;

    if (kwd == "xtor") {
      if (line >> xtor_name) {
        xtor = new CarbonCfgXtor(xtor_name.c_str(), CARBON_DEFAULT_XTOR_LIB);
        addXtor(xtor);
      }

    } else if (kwd == "xtorSynonym") {
      // Get the xtor name and synonym
      UtString xtorName, xtorSynonym;
      line >> xtorName;
      line >> xtorSynonym;

      // Validate that xtorName is correct. The two possible problems
      // are an invalid xtorName and a duplicate xtorSynonym line
      CarbonCfgXtor* xtor1 = findXtor(xtorName.c_str());
      CarbonCfgXtor* xtor2 = findXtor(xtorSynonym.c_str());
      if (xtor1 == NULL) {
        UtString buf;
        buf << "Invalid Xtor name `" << xtorName << "' in xtorSynonym line.";
        libfile.reportError(buf.c_str());
      } else if (xtor2 != NULL) {
        UtString buf;
        buf << "Duplicate Xtor Synonym `" << xtorSynonym << "' found.";
        libfile.reportError(buf.c_str());
      } else {
        // Add the synonym to the map
        addSynonym(xtor1, xtorSynonym.c_str());
      }

    } else if (kwd == "port") {
      if ((line >> port_name) && (line >> width) && (line >> direction)
          && (line >> comment))
      {
        UInt32 portIndex;
        if (xtor == NULL) {
          libfile.reportError("Need xtor before port\n");
        }
        else if (xtor->findPort(port_name.c_str(), &portIndex)) {
          UtString buf;
          buf << "Duplicate xtor port: " << xtor_name << port_name << "\n";
          libfile.reportError(buf.c_str());
        }
        else {
          xtor->addPort(new CarbonCfgXtorPort(width, direction,
                                              port_name.c_str(),
                                              comment.c_str()));
        }
      }
    }
    else if (kwd == "parameter") {
      if ((line >> param_name)
          && (line >> width) &&  (line >> param_type)
          && (line >> param_flags) && (line >> comment))
      {
        if (xtor == NULL) {
          libfile.reportError("Need xtor before param\n");
        }
        else {
          xtor->addParam(new CarbonCfgXtorParam(width, param_type, param_flags,
                                                param_name.c_str(),
                                                comment.c_str(), NULL, NULL, NULL, NULL));
        }
      }
    }
    else if (kwd == "attributes") {
      UtString attrib;
      while (line >> attrib)
      {
        if (attrib == "write_debug")
          xtor->putHasWriteDebug(true);
        else
        {
          UtString buf;
          buf << "Unrecognized attribute '" << attrib << "'";
          libfile.reportError(buf.c_str());
        }
      }
      if (line.eof())
        // Ignore inevitable "no more tokens" error at end of line
        (void) line.getErrmsg();
    }
    else {
      UtString buf;
      buf << "Unexpected keyword: " << kwd;
      libfile.reportError(buf.c_str());
    }
    if (line.bad()) {
      libfile.reportError(line.getErrmsg());
    }
  } // while
  if (libfile.bad()) {
    *errmsg << libfilename << ":" << libfile.getLineNumber()
            << ": " << libfile.getErrmsg();
    return false;
  }
  return true;
} // bool CarbonCfgXtorLib::read


void CarbonCfgXtorLib::addXtor(CarbonCfgXtor *xtor)
{
  NameVariantPair key(xtor->getName(), xtor->getVariant());
  mXtorMap[key] = xtor;
  mXtorVec.push_back(xtor);
}

void CarbonCfgXtorLib::addSynonym(CarbonCfgXtor *xtor, const char *synonym, const char* synonymVariant)
{
  NameVariantPair key(synonym, synonymVariant);
  mXtorMap[key] = xtor;
}


//! find a transactor given a name
CarbonCfgXtor* CarbonCfgXtorLib::findXtor(const char* name, const char* variant) {
  NameVariantPair key(name, variant);
  XtorMap::iterator p = mXtorMap.find(key);
  if (p == mXtorMap.end()) {
    return NULL;
  }
  return p->second;
}

CarbonCfgXtorLib::XtorLoop CarbonCfgXtorLib::loopXtors() {
  return mXtorMap.loopSorted();
}

CarbonCfgXtorPort::CarbonCfgXtorPort(int size,
                                     CarbonCfgRTLPortType type,
                                     const char* name,
                                     const char* description) :
  mSize(size),
  mType(type),
  mName(name),
  mTypeDef(""),
  mDescription(description),
  mOptional(false),
  mIsClockSensitive(false)
{
}

void CarbonCfgXtorPort::addPhaseName(const char* phaseName)
{
  mPhaseNames.push_back(phaseName);
}

const char* CarbonCfgXtorPort::getPhaseName(UInt32 index) const
{
  SCRIPT_ASSERT_RETURN(index < mPhaseNames.size(), "Request for an out of range transactor port phase name", "");
  return mPhaseNames[index];
}

CarbonCfgXtorSystemCPort::CarbonCfgXtorSystemCPort(const char* name, 
                                                   CarbonCfgSystemCPortType type, 
                                                   const char* interface,
                                                   const char* description) :
  mName(name),
  mType(type),
  mInterface(interface),
  mDescription(description)
{
}

CarbonCfgXtorParamInst::CarbonCfgXtorParamInst(CarbonCfgXtorInstance* inst,
                                               CarbonCfgXtorParam* param) :
  mXtorInstance(inst),
  mXtorParam(param),
  mValue(0),
  mElementGenerationType(eCarbonCfgElementUser),
  mHidden(false)
{
  if(param) {
    mValue = param->getDefaultValue();
    for (UInt32 i = 0; i < param->numEnumChoices(); ++i) {
      const char* choice = param->getEnumChoice(i);
      mEnumChoices.push_back(choice);
    }
  }
}

void CarbonCfgXtorParamInst::removeRTLPort(CarbonCfgRTLPort* rtlPort)
{
  mRTLPortVec.remove(rtlPort);
}

CarbonCfgRTLPort* CarbonCfgXtorParamInst::findRTLPort(const char* name)
{
  for (UInt32 i=0; i<numRTLPorts(); i++)
  {
    CarbonCfgRTLPort* port = getRTLPort(i);
    if (0 == strcmp(port->getName(), name))
      return port;
  }
  return NULL;
}

CarbonCfgXtorParamInst::~CarbonCfgXtorParamInst()
{
}

bool CarbonCfgXtorParamInst::putValue(const char* value)
{
  // Validate the parameter
  if (mXtorParam->inRange(value)) {
    // OK to set
    mValue = value;
    return true;
  }
  return false;
}

bool CarbonCfgXtorParamInst::removeEnumChoice(const char* choice)
{
  // make sure this is an enum
  if (numEnumChoices() == 0) {
    return false;
  }

  // If the choice matches the current value don't do it
  UtString enumChoice(choice);
  if (enumChoice == mValue) {
    return false;
  }

  // Find this enum choice
  UtStringArray::iterator pos = mEnumChoices.begin();
  bool found = false;
  while (!found && (pos != mEnumChoices.end())) {
    const UtString& thisChoice = *pos;
    if (enumChoice == thisChoice) {
      found = true;
    } else {
      ++pos;
    }
  }

  // If we found it, remove it.
  if (found) {
    mEnumChoices.erase(pos);
    return true;
  } else {
    return false;
  }
}

const char* CarbonCfgXtorParamInst::getEnumChoice(UInt32 index) const
{
  SCRIPT_ASSERT_RETURN(index < mEnumChoices.size(), "Request for an out of range parameter enum choice.", "");
  return mEnumChoices[index];
}

void CarbonCfgXtorParamInst::putEnumChoices(const UtStringArray& enumChoices)
{
  // Gather the enum choices from the param definition so we verify these choices below.
  UtHashSet<UtString> validEnumChoices;
  for (UInt32 i = 0; i < getParam()->numEnumChoices(); ++i) {
    UtString enumChoice(getParam()->getEnumChoice(i));
    validEnumChoices.insert(enumChoice);
  }

  // Now walk the enum choices and add them to our array for this parameter instance.
  mEnumChoices.clear();
  for (UtStringArray::UnsortedCLoop l = enumChoices.loopCUnsorted(); !l.atEnd(); ++l) {
    const UtString& enumChoice = *l;
    if (validEnumChoices.count(enumChoice) > 0) {
      mEnumChoices.push_back(enumChoice);
    } else {
      QString msg = QString("Parameter instance '%1' enum choice '%2' not valid.").arg(getParam()->getName()).arg(enumChoice.c_str());
      UtString strMsg; strMsg << msg;
      SCRIPT_ASSERT(validEnumChoices.count(enumChoice) > 0, strMsg.c_str());
    }
  }
}

CarbonCfgXtorParam::CarbonCfgXtorParam(int size,
                                       CarbonCfgParamDataType type,
                                       CarbonCfgParamFlag flag,
                                       const char* name,
                                       const char* description,
                                       const char* defaultValue,
                                       const char* enumChoices,
                                       const char* min,
                                       const char* max) :
  mSize(size),
  mType(type),
  mFlag(flag),
  mName(name),
  mDescription(description),
  mDefaultValue(defaultValue),
  mEnumChoices(enumChoices),
  mElementGenerationType(eCarbonCfgElementUser),
  mMin(min),
  mMax(max)
{
  // Initialize the array of enum choices
  if (!mEnumChoices.empty()) {
    UtString delims(1, enumChoices[0]);
    for (UtShellTok tok(mEnumChoices.c_str(), false, delims.c_str(), false); !tok.atEnd(); ++tok) {
      const char* choice = *tok;
      mEnumChoiceArray.push_back(choice);
    }
  }
}

const char* CarbonCfgXtorParam::getEnumChoice(UInt32 index) const
{
  SCRIPT_ASSERT_RETURN(index < mEnumChoiceArray.size(),
                       "Request for an out of range parameter enum choice", "");
  return mEnumChoiceArray[index];
}

void CarbonCfgXtorParam::write(CfgXmlWriter &xml) {
  //    <parameter name="Enable Debug Messages" type="bool" value="false" scope="run-time">
  //      <description>Enable debug messages.</description>
  //      <enumChoices>choice1||choice 3| </enumChoices> <!-- second choice is empty, last choice is a space character, delimiter '|' -->
  //    </parameter>
  bool debugEmbeddedModels = false;
  if (getElementGenerationType() == eCarbonCfgElementGenerated && getenv("CARBON_DEBUG_EMBEDDED_MODELS"))
    debugEmbeddedModels = true;

  if (debugEmbeddedModels || getElementGenerationType() == eCarbonCfgElementUser)
  {
    // Write with a different tag, so the read doesn't cause
    // duplicate components to be created.
    if (debugEmbeddedModels)
      xml.addElement("debugParameter");
    else
      xml.addElement("parameter");

    xml.addAttribute("name", getName());

    CarbonCfgParamDataTypeIO type(getType());
    xml.addAttribute("type", type.getString());
                     
    xml.addAttribute("value", getDefaultValue());

    CarbonCfgParamFlagIO scope(getFlag());
    xml.addAttribute("scope", scope.getString());

    xml.addElement("description", getDescription());
    xml.closeElement(); // description

    if ( (0 != strlen(getEnumChoices()))) {
      xml.addElement("enumChoices", getEnumChoices());
      xml.closeElement(); // enumChoices
    }

    xml.closeElement(); // parameter
  }
}

bool CarbonCfgXtorParam::inRange(const char* value) const
{
  // Check for empty min and max, always return true
  if (mMin.empty() && mMax.empty()) {
    return true;
  }

  // Handle everything but strings and bool
  bool ok = true;
  UtString valueStr(value);
  UtString minStr(mMin);
  UtString maxStr(mMax);
  switch(mType) {
    case eCarbonCfgXtorBool:
    case eCarbonCfgXtorString:
      ok = true;
      break;
      
    case eCarbonCfgXtorUInt32:
    {
      UInt32 min, max, val;
      valueStr >> val;
      if (!minStr.empty()) {
        minStr >> min;
        ok &= (val >= min);
      }
      if (!maxStr.empty()) {
        maxStr >> max;
        ok &= (val <= max);
      }
      break;
    }

    case eCarbonCfgXtorDouble:
    {
      double min, max, val;
      valueStr >> val;
      if (!minStr.empty()) {
        minStr >> min;
        ok &= (val >= min);
      }
      if (!maxStr.empty()) {
        maxStr >> max;
        ok &= (val <= max);
      }
      break;
    }

    case eCarbonCfgXtorUInt64:
    {
      UInt64 min, max, val;
      valueStr >> val;
      if (!minStr.empty()) {
        minStr >> min;
        ok &= (val >= min);
      }
      if (!maxStr.empty()) {
        maxStr >> max;
        ok &= (val <= max);
      }
      break;
    }
  }
  return ok;
} // bool CarbonCfgXtorParam::inRange

CarbonCfgXtor::~CarbonCfgXtor() {
  mPortVec.clearPointers();
}

bool CarbonCfgXtor::findPort(const char* portName, UInt32* portIndex) {
  PortMap::iterator p = mPorts.find(portName);
  if (p != mPorts.end()) {
    *portIndex = p->second;
    return true;
  }
  return false;
}

CarbonCfgXtorPort* CarbonCfgXtor::findPort(const char* portName) {
  CarbonCfgXtorPort* port = NULL;
  UInt32 portIndex;
  if (findPort(portName, &portIndex)) {
    port = getPort(portIndex);
  }
  return port;
}

void CarbonCfgXtor::addPort(CarbonCfgXtorPort* port) {
  mPorts[port->getName()] = mPortVec.size();
  mPortVec.push_back(port);
}

void CarbonCfgXtor::addSystemCPort(CarbonCfgXtorSystemCPort* port) {
  // If there are more that one SystemC port in a transactor
  // They all need to have unique names
  for (UInt32 i = 0; i < mSystemCPortVec.size(); ++i) {
    UtString emptyErr;
    emptyErr << "Xtor " << mName 
             << ": SystemC ports needs to have a name if there are multiple ones in a transactor.";
    SCRIPT_ASSERT(strcmp(mSystemCPortVec[i]->getName(), "") != 0, emptyErr.c_str());

    UtString notUniqueErr;
    notUniqueErr << "Xtor " << mName
                 << ": A SystemC port with name " << mSystemCPortVec[i]->getName() 
                 << " already exists in the transactor.";
    SCRIPT_ASSERT(strcmp(mSystemCPortVec[i]->getName(), port->getName()) != 0, notUniqueErr.c_str());
  }
    
  mSystemCPortVec.push_back(port);
}


CarbonCfgXtorPort* carbonCfgXtorFindPort(CarbonCfgXtorID x, const char* pname)
{
  return x->findPort(pname);
}

CarbonCfgXtorParam* CarbonCfgXtor::findParam(const char* paramName) {
  ParamMap::iterator p = mParams.find(paramName);
  if (p != mParams.end()) {
    return p->second;
  }
  return NULL;
}

const char* CarbonCfgXtor::getLibraryName(void) const
{
  return mLibraryName.c_str();
}

const char* CarbonCfgXtor::getVariant(void) const
{
  return mVariant.c_str();
}

CarbonCfgXtorTemplateArg* CarbonCfgXtor::getTemplateArg(UInt32 index) const
{
  SCRIPT_ASSERT_RETURN(index < mTemplateArgs.size(), "Request for out of range xtor template arg", NULL);
  return mTemplateArgs[index];
}

CarbonCfgXtorAbstraction* CarbonCfgXtor::getAbstraction(UInt32 index) const
{
  return mAbstractions[index];
}

void CarbonCfgXtor::addAbstraction(CarbonCfgXtorAbstraction* xtorAbstraction)
{
  mAbstractions.push_back(xtorAbstraction);
}

const char* carbonCfgESLPortGetName(CarbonCfgESLPortID eslPort) {
  return eslPort->getName();
}

const char* carbonCfgESLPortGetTypeDef(CarbonCfgESLPortID eslPort) {
  return eslPort->getTypeDef();
}

void carbonCfgESLPortPutTypeDef(CarbonCfgESLPortID eslPort, const char* typeDef) {
  return eslPort->putTypeDef(typeDef);
}

const char* carbonCfgESLPortGetParamName(CarbonCfgESLPortID eslPort) {
  return eslPort->getParamName();
}
const char* carbonCfgESLPortGetParamXtorInstName(CarbonCfgESLPortID eslPort) {
  return eslPort->getParamXtorInstName();
}

CarbonCfgESLPortMode carbonCfgESLPortGetMode(CarbonCfgESLPortID eslPort) {
  return eslPort->getMode();
}

const char* carbonCfgESLPortGetExpr(CarbonCfgESLPortID eslPort) {
  return eslPort->getExpr();
}

CarbonCfgESLPortType carbonCfgESLPortGetType(CarbonCfgESLPortID eslPort) {
  return eslPort->mType;
}

CarbonCfgXtorParamInst *carbonCfgESLPortGetParamInst(CarbonCfgESLPortID eslPort) {
  return eslPort->getParamInstance();
}

CarbonCfgXtorConnID carbonCfgConnectXtor(CarbonCfgRTLPortID rtlPort,
                                         CarbonCfgXtorInstanceID xtor,
                                         unsigned int portIndex)
{
  CarbonCfgXtorConnID conn = new CarbonCfgXtorConn(rtlPort, xtor, portIndex);
  rtlPort->connect(conn);
  return conn;
}

CarbonCfgXtorInstanceID carbonCfgXtorConnGetXtorInstance(CarbonCfgXtorConnID conn) {
  return conn->getInstance();
}

CarbonCfgRTLPortID carbonCfgXtorConnGetRTLPort(CarbonCfgXtorConnID conn) {
  return conn->getRTLPort();
}

CarbonCfgXtorPortID carbonCfgXtorConnGetPort(CarbonCfgXtorConnID conn) {
  return conn->getPort();
}

unsigned int carbonCfgXtorConnGetPortIndex(CarbonCfgXtorConnID conn) {
  return conn->getPortIndex();
}

const char* carbonCfgXtorConnGetExpr(CarbonCfgXtorConnID conn) {
  return conn->getExpr();
}

const char* carbonCfgXtorConnGetTypeDef(CarbonCfgXtorConnID conn)
{
  return conn->getTypeDef();
}

const char* carbonCfgXtorPortGetName(CarbonCfgXtorPortID port) {
  return port->getName();
}

const char* carbonCfgXtorPortGetTypeDef(CarbonCfgXtorPortID port) {
  return port->getTypeDef();
}

const char* carbonCfgXtorPortGetWidthExpr(CarbonCfgXtorPortID port) {
  return port->getWidthExpr();
}

const char* carbonCfgXtorPortGetDescription(CarbonCfgXtorPortID port) {
  return port->getDescription();
}

CarbonCfgRTLPortType carbonCfgXtorPortGetType(CarbonCfgXtorPortID port) {
  return port->getType();
}

unsigned int carbonCfgXtorPortGetSize(CarbonCfgXtorPortID port) {
  return port->getSize();
}

unsigned int carbonCfgXtorPortIsClockSensitive(CarbonCfgXtorPortID port) {
  return port->isClockSensitive();
}

const char* carbonCfgXtorPortGetEventName(CarbonCfgXtorPortID port) {
  return port->getEventName();
}

unsigned int carbonCfgXtorPortNumPhaseNames(CarbonCfgXtorPortID port)
{
  return port->numPhaseNames();
}

const char* carbonCfgXtorPortGetPhaseName(CarbonCfgXtorPortID port, unsigned int index)
{
  return port->getPhaseName(index);
}

const char* carbonCfgXtorSystemCPortGetName(CarbonCfgXtorSystemCPortID port) {
  return port->getName();
}
CarbonCfgSystemCPortType carbonCfgXtorSystemCPortGetType(CarbonCfgXtorSystemCPortID port) {
  return port->getType();
}
const char* carbonCfgXtorSystemCPortGetInterface(CarbonCfgXtorSystemCPortID port) {
  return port->getInterface();
}
const char* carbonCfgXtorSystemCPortGetDescription(CarbonCfgXtorSystemCPortID port) {
  return port->getDescription();
}

void CarbonCfg::getUniqueESLName(UtString* outbuf, const char* name) {
  getUniqueName(outbuf, &mESLPortNames, name);
}

void CarbonCfg::getUniqueMemName(UtString* outbuf, const char* name) {
  getUniqueName(outbuf, &mMemNames, name);
}

void CarbonCfg::getUniqueRegName(UtString* outbuf, const char* name) {
  getUniqueName(outbuf, &mRegNames, name);
}

void CarbonCfg::getUniquePStreamName(UtString* outbuf, const char* name) {
  getUniqueName(outbuf, &mPStreamNames, name);
}

void CarbonCfg::getUniqueGroupName(UtString* outbuf, const char* name) {
  getUniqueName(outbuf, &mGroupNames, name, "regs_0");
}

void CarbonCfg::getUniqueName(UtString* outbuf, UtStringSet* names,
                              const char* name, const char* emptyName)
{
  *outbuf = name;
  for (UInt32 i = 0; i < outbuf->size(); ++i) {
    char c = (*outbuf)[i];
    if (!isalnum(c) && (c != '_')) {
      (*outbuf)[i] = '_';
    }
  }

  if (outbuf->empty()) {
    *outbuf = emptyName;
  }

  if (names->find(*outbuf) == names->end()) {
    return;
  }

  // If the existing name was already uniquified by appending _N, then
  // strip that off for the base name
  UInt32 base_size = outbuf->size();
  const char* last = outbuf->c_str() + base_size - 1;
  const char* p = last;
  for (; isdigit(*p) && (p > outbuf->c_str()); --p)
    ;

  // If, after walking backward over all the digits, we landed on
  // an "_", then generate unique names having stripped off that _N
  // suffix.
  if ((p != last) && (*p == '_')) {
    base_size = p - outbuf->c_str();
  }

  for (UInt32 i = 1; ; ++i) {
    outbuf->resize(base_size);
    *outbuf << "_" << i;
    if (names->find(*outbuf) == names->end()) {
      return;
    }
  }
} // void CarbonCfg::getUniqueName

CarbonCfgCompCustomCode* CarbonCfg::clone(CarbonCfgCompCustomCode* cc)
{
  CarbonCfgCompCustomCode* newCc = cc->clone(mTemplateVariables);
  mCustomCodes.addCustomCode(newCc);
  return newCc;
}

CarbonCfgRegister* CarbonCfg::clone(CarbonCfgGroup* group, CarbonCfgRegister* reg)
{
  // Make sure it doesn't already exist. That is an error. We do it
  // first so we don't allocate/deallocate the data.
  UtString regName;
  mTemplateVariables.resolveString(reg->getName(), &regName);
  if (mRegNames.count(regName) != 0) {
    return NULL;
  }

  // OK to clone
  CarbonCfgRegister* newReg = reg->clone(group, mTemplateVariables);
  bool inserted = mRegNames.insertWithCheck(newReg->getName());
  SCRIPT_ASSERT_RETURN(inserted, "Failed to insert a register in the name set", NULL);
  mRegisterVec.push_back(newReg);
  return newReg;
}

CarbonCfgMemory* CarbonCfg::clone(CarbonCfgMemory* mem)
{
  // Make sure it doesn't already exist. That is an error. We do it
  // first so we don't allocate/deallocate the data.
  UtString memName;
  mTemplateVariables.resolveString(mem->getName(), &memName);
  if (mMemNames.count(memName) != 0) {
    return NULL;
  }

  // OK to clone
  CarbonCfgMemory* newMem = mem->clone(this, mTemplateVariables);
  bool inserted = mMemNames.insertWithCheck(newMem->getName());
  SCRIPT_ASSERT_RETURN(inserted, "Failed to insert a register in the name set", NULL);
  mMemoryVec.push_back(newMem);
  return newMem;
}

void CarbonCfg::clone(const CarbonCfgCadi* cadi)
{
  mCadi.cloneInPlace(*cadi, mTemplateVariables);
}

void CarbonCfg::clone(const CarbonCfgProcInfo* procInfo)
{
  mProcInfo.cloneInPlace(*procInfo, mTemplateVariables);
}

CarbonCfgESLPortType CarbonCfg::convertRTLPortType(CarbonCfgRTLPort* rtlPort)
{
  // Convert the port type. We don't have a default so that -Wall
  // -Werror finds mising case items. But we do want to assert if
  // there is a corruption, so set a boolean saying we found a valid
  // type.
  CarbonCfgESLPortType portType = eCarbonCfgESLUndefined;
  switch(rtlPort->getType()) {
    case eCarbonCfgRTLInput:
      portType = eCarbonCfgESLInput;
      break;

    case eCarbonCfgRTLOutput:
      portType = eCarbonCfgESLOutput;
      break;

    case eCarbonCfgRTLInout:
      portType = eCarbonCfgESLInout;
      break;
  }

  // Make sure we have a valid rtl port type
  INFO_ASSERT(portType != eCarbonCfgESLUndefined,
              "Found an invalid RTL port type for the current component configuration file.");
  return portType;
} // CarbonCfgESLPortType CarbonCfg::convertRTLPortType
  

CarbonCfgCadi::CarbonCfgCadi() :
  CarbonCfgCustomCodeContainer(CarbonCfgCadiCustomCodeSectionIO().getStrings())
{
}

CarbonCfgCadi::~CarbonCfgCadi()
{
}

void CarbonCfgCadi::write(CfgXmlWriter& xml)
{
  // Only write it if we have something in there
  if (numCustomCodes() == 0 && mDisassemblerName == "" ) {
    return;
  }

  xml.addElement("cadi");

  // Write the custom code sections
  writeCustomCodes(xml);

  // Write dissasembler if any
  if ( mDisassemblerName != "" ) {
    xml.addElement("disassembler");
    xml.addAttribute("class", mDisassemblerName);
    xml.closeElement();
  }

  xml.closeElement();
}

void
CarbonCfgCadi::cloneInPlace(const CarbonCfgCadi& src, 
                            const CarbonCfgTemplate& templ) 
{
  // Clone disassembler
  templ.resolveString(src.getDisassemblerName(), &mDisassemblerName);

  // Clone the sub elements
  for (CarbonUInt32 i = 0; i < src.numCustomCodes(); ++i) {
    // Clone it in place
    CarbonCfgCustomCode* cc = src.getCustomCode(i);
    CarbonCfgCadiCustomCode* newCc = addCustomCode();
    newCc->cloneInPlace(*cc, templ);
  }
}

CarbonCfgCadiCustomCode* CarbonCfgCadi::addCustomCode()
{
  CarbonCfgCadiCustomCode* customCode = new CarbonCfgCadiCustomCode();
  CarbonCfgCustomCodeContainer::addCustomCode(customCode);
  return customCode;
}


// ELF Loader
void CarbonCfgELFLoader::addSection(const char* name, unsigned int space, const char* access)
{
  Section* sec = new Section;

  sec->space  = space;
  sec->name   = name;
  sec->access = access;

  // Add section
  mSection.push_back(sec);
}

void CarbonCfgELFLoader::removeSections(void)
{
  for (Loop<UtArray<Section*> > l(mSection); !l.atEnd(); ++l) {
    Section* section = *l;
    delete section;
  }
  mSection.clear();
}

void CarbonCfgELFLoader::write(CfgXmlWriter &xml)
{
  if(!mSection.empty()) {
    xml.addElement("elfloader");
    for(unsigned int i = 0; i < mSection.size(); ++i) {
      xml.addElement("section");
      xml.addAttribute("name", mSection[i]->name.c_str());
      xml.addAttribute("space", mSection[i]->space);
      xml.addAttribute("access", mSection[i]->access.c_str());
      xml.closeElement();
    }
    xml.closeElement();
  }
}

CarbonCfgELFLoader* carbonCfgGetELFLoader(CarbonCfgID cfg)
{
  return cfg->getELFLoader();
}

CarbonCfgProcInfo* carbonCfgGetProcInfo(CarbonCfgID cfg)
{
  return cfg->getProcInfo();
}

void carbonCfgELFLoaderAddSection(CarbonCfgELFLoader* elf, const char* name, unsigned int space, const char* access)
{
  elf->addSection(name, space, access);
}
unsigned int carbonCfgELFLoaderNumSections(CarbonCfgELFLoader* elf)
{
  return elf->numSections();
}

const char* carbonCfgELFLoaderGetName(CarbonCfgELFLoader* elf, unsigned int index)
{
  return elf->getSectionName(index);
}

unsigned int carbonCfgELFLoaderGetSpace(CarbonCfgELFLoader* elf, unsigned int index)
{
  return elf->getSectionSpace(index);
}

const char* carbonCfgELFLoaderGetAccess(CarbonCfgELFLoaderID elf, unsigned int index)
{
  return elf->getSectionAccess(index);
}

CarbonCfgProcInfo::~CarbonCfgProcInfo()
{
  mProcessorOptions.clear();
}

UInt32 CarbonCfgProcInfo::getNumProcessorOptions(void) const
{
  return mProcessorOptions.size();
}

const char* CarbonCfgProcInfo::getProcessorOption(UInt32 index) const
{
  if (index < mProcessorOptions.size()) {
    return mProcessorOptions[index];
  } else {
    return "";
  }
}

void CarbonCfgProcInfo::addProcessorOption(const char* option)
{
  mProcessorOptions.push_back(option);
}

void CarbonCfgProcInfo::cloneInPlace(const CarbonCfgProcInfo& other, CarbonCfgTemplate& templ)
{
  mIsProcessor = other.mIsProcessor;
  templ.resolveString(other.mName.c_str(), &mName);
  templ.resolveString(other.getDebuggerName(), &mDebuggerName);
  mPipeStages = other.mPipeStages;
  mHwThreads = other.mHwThreads;
  for (UInt32 i = 0; i < other.getNumProcessorOptions(); ++i) {
    const char* option = other.getProcessorOption(i);
    UtString newOption;
    templ.resolveString(option, &newOption);
    addProcessorOption(newOption.c_str());
  }
  templ.resolveString(other.getPCRegGroupName(), &mPCGroupName);
  templ.resolveString(other.getPCRegName(), &mPCName);
  templ.resolveString(other.getExtendedFeaturesRegGroupName(), &mExtendedFeaturesRegGroupName);
  templ.resolveString(other.getExtendedFeaturesRegName(), &mExtendedFeaturesRegName);
  mDebuggablePoint = other.mDebuggablePoint;
  templ.resolveString(other.getTargetName(), &mTargetName);
}

void CarbonCfgProcInfo::write(CfgXmlWriter &xml)
{
  if(mIsProcessor) {
    xml.addElement("procInfo");
    xml.addAttribute("debuggerName", mDebuggerName);
    xml.addAttribute("pipes",        mPipeStages);
    xml.addAttribute("threads",      mHwThreads);
    xml.addAttribute("targetName",   mTargetName);

    // Add the optional pc group/name element
    if (!mPCName.empty()) {
      xml.addElement("pcReg");
      xml.addAttribute("groupName", mPCGroupName);
      xml.addAttribute("regName", mPCName);
      xml.closeElement();
    }

    // Add the optional extended features element
    if (!mExtendedFeaturesRegName.empty()) {
      xml.addElement("extendedFeaturesReg");
      xml.addAttribute("groupName", mExtendedFeaturesRegGroupName);
      xml.addAttribute("regName", mExtendedFeaturesRegName);
      xml.closeElement();
    }

    // Add the optional debuggable point tag
    if (mDebuggablePoint) {
      xml.addElement("debuggable-point");
      xml.closeElement();
    }

    // Add the optional processor options to the XML file
    if (getNumProcessorOptions() > 0) {
      xml.addElement("options");
      for (UInt32 i = 0; i < getNumProcessorOptions(); ++i) {
        const char* option = getProcessorOption(i);
        xml.addElement("option", option);
        xml.closeElement();
      }
      xml.closeElement();
    }

    // Close the processor options element
    xml.closeElement();
  }
}

int carbonCfgIsProcessor(CarbonCfg* cfg)
{
  return cfg->getProcInfo()->isProcessor();
}

const char* carbonCfgProcInfoGetDebuggerName(CarbonCfgProcInfo* proc)
{
  return proc->getDebuggerName();
}
unsigned int carbonCfgProcInfoGetPipeStages(CarbonCfgProcInfo* proc)
{
  return proc->getPipeStages();
}
unsigned int carbonCfgProcInfoGetHwThreads(CarbonCfgProcInfo* proc)
{
  return proc->getHwThreads();
}

unsigned int carbonCfgProcInfoGetNumProcessorOptions(CarbonCfgProcInfo* proc)
{
  return proc->getNumProcessorOptions();
}

const char*
carbonCfgProcInfoGetProcessorOption(CarbonCfgProcInfo* proc, unsigned int i)
{
  return proc->getProcessorOption(i);
}

const char*
carbonCfgProcInfoGetPCRegGroupName(CarbonCfgProcInfo* proc)
{
  return proc->getPCRegGroupName();
}

const char*
carbonCfgProcInfoGetPCRegName(CarbonCfgProcInfo* proc)
{
  return proc->getPCRegName();
}

const char*
carbonCfgProcInfoGetExtendedFeaturesRegGroupName(CarbonCfgProcInfo* proc)
{
  return proc->getExtendedFeaturesRegGroupName();
}

const char*
carbonCfgProcInfoGetExtendedFeaturesRegName(CarbonCfgProcInfo* proc)
{
  return proc->getExtendedFeaturesRegName();
}

unsigned int carbonCfgProcInfoGetDebuggablePoint(CarbonCfgProcInfo* proc)
{
  if (proc->getDebuggablePoint()) {
    return 1;
  } else {
    return 0;
  }
}

const char*
carbonCfgProcInfoGetTargetName(CarbonCfgProcInfo* proc)
{
  return proc->getTargetName();
}


// profiling implementation

CarbonCfgPBucket::CarbonCfgPBucket(UtIStream& f):
  mIndex(0)
{
  f >> "pbucket" >> mName >> mColor >> mExpr;
}

  
CarbonCfgPBucket::CarbonCfgPBucket(QXmlStreamReader& xr):
  mIndex(0)
{
  CarbonCfgColorIO colorio; 
  colorio.parseString(QUSTR(xr.attributes().value("color").toString()));

  mColor = colorio;
  mExpr = QUSTR(xr.attributes().value("expression").toString());
  mName = QUSTR(xr.attributes().value("name").toString());
}

void CarbonCfgPBucket::write(CfgXmlWriter& f) {
  //f << "pbucket" << mName << mColor.getString() << mExpr << '\n';
  f.addElement("ProfileBucket");
  f.addAttribute("expression", mExpr);
  f.addAttribute("color", mColor.getString());
  f.addAttribute("name", mName);
 
  f.closeElement();
}

CarbonCfgPNet::CarbonCfgPNet(UtIStream& f) {
  f >> "pnet" >> mName >> mPath >> mWidth;
}

CarbonCfgPNet::CarbonCfgPNet(QXmlStreamReader& xr) {
  while (xr.readNextStartElement())
  {
    if ("Name" == xr.name())
      mName = QUSTR(xr.readElementText());
    else if ("Path" == xr.name())
      mPath = QUSTR(xr.readElementText());
    else if ("Width" == xr.name())
      mWidth = xr.readElementText().toUInt();
  }
}

void CarbonCfgPNet::write(CfgXmlWriter& f) {
  //f << "pnet" << mName << mPath << mWidth << '\n';
  f.addElement("ProfileNet");
  f.addCompletedElement("Name", mName);
  f.addCompletedElement("Path", mPath);
  f.addCompletedElement("Width", mWidth);
  f.closeElement();
}

CarbonCfgPChannel::CarbonCfgPChannel(QXmlStreamReader& xr, CarbonCfgErrorHandler* err)
{
  mExpr = QUSTR(xr.attributes().value("expression").toString());
  mName = QUSTR(xr.attributes().value("name").toString());   

  while(xr.readNextStartElement())
  {
    if ("ProfileBucket" == xr.name())
    {
      CarbonCfgPBucket* pb = new CarbonCfgPBucket(xr);
      if (mBucketNames.insertWithCheck(pb->getName()))
      {
        mBuckets.push_back(pb);
      }
      xr.skipCurrentElement();
    }
    else
    {
      UtString msg;
      msg << "Error: Unknown tag: " << xr.name();
      err->reportError(msg.c_str());
    }
  }
}

CarbonCfgPChannel::CarbonCfgPChannel(UtIStream& line, UtParamFile& f,
                                     CarbonCfgErrorHandler* err)
  : mNumTotalBuckets(0)
{
  UInt32 numBuckets = 0;
  if (line >> "pchannel" >> numBuckets >> mName >> mExpr) {
    UtIStringStream bline;
    for (UInt32 i = 0; (i < numBuckets) && !f.bad(); ++i) {
      if (!f.getline(&bline)) {
        err->reportError("End of file reading pchannel's buckets");
        return;
      }
      else {
        CarbonCfgPBucket* pb = new CarbonCfgPBucket(bline);
        if (mBucketNames.insertWithCheck(pb->getName())) {
          mBuckets.push_back(pb);
        }
        else {
          UtString buf;
          buf << "Duplicate pbucket name: " << pb->getName();
          err->reportError(buf.c_str());
          delete pb;
        }
        if (bline.bad()) {
          err->reportError(bline.getErrmsg());
        }
      }
    }
  }
} // CarbonCfgPChannel::CarbonCfgPChannel

CarbonCfgPChannel::~CarbonCfgPChannel() {
  mBuckets.clearPointers();
}
void CarbonCfgPChannel::write(CfgXmlWriter& f) {
 
  f.addElement("ProfileChannel");
  f.addAttribute("expression", mExpr);
  f.addAttribute("name", mName);
 
  // f << "pchannel" << mBuckets.size() << mName << mExpr << '\n';
  for (UInt32 i = 0; i < mBuckets.size(); ++i) {
    mBuckets[i]->write(f);
  }

  f.closeElement();
}

CarbonCfgPBucket* CarbonCfgPChannel::addBucket(const char* name) {
  CarbonCfgPBucket* pb = new CarbonCfgPBucket(name);
  mBucketNames.insert(name);
  mBuckets.push_back(pb);
  return pb;
}

void CarbonCfgPChannel::changeBucketName(CarbonCfgPBucket* pbucket,
                                         const char* name)
{
  // Remove the current name from the port set
  mBucketNames.erase(pbucket->getName());

  UtString buf;
  getUniqueBucketName(&buf, name);
  mBucketNames.insert(buf);
  pbucket->putName(buf.c_str());
}


void CarbonCfgPChannel::getUniqueBucketName(UtString* outbuf, const char* name)
{
  CarbonCfg::getUniqueName(outbuf, &mBucketNames, name);
}

void CarbonCfgPChannel::removeBucket(CarbonCfgPBucket* pbucket) {
  mBucketNames.erase(pbucket->getName());
  mBuckets.remove(pbucket);
  delete pbucket;
}

CarbonCfgPStream::CarbonCfgPStream(const char* name, const char* expr)
  : mName(name),
    mTriggerExpr(expr)
{
}

CarbonCfgPStream::CarbonCfgPStream(UtIStream& line, UtParamFile& f,
                                   CarbonCfgErrorHandler* err)
{
  UInt32 numNets = 0, numChannels = 0, numTriggers = 0;
  UtString oldTriggerExpr;      // non-blank if reading an old .ccfg file
  if (line >> mName >> oldTriggerExpr >> numNets >> numChannels) {
    UtIStringStream ln;
    for (UInt32 i = 0; (i < numNets) && !f.bad() && f.getline(&ln); ++i) {
      CarbonCfgPNet* pn = new CarbonCfgPNet(ln);
      mNets.push_back(pn);
      mNetNames.insert(pn->getName());
      if (ln.bad()) {
        line.reportError(ln.getErrmsg());
        return;
      }
    }

    // In the original profiling world, there would be exactly one trigger,
    // but in the new one, we need to parse a number of triggers
    if (oldTriggerExpr.empty()) {
      if (! (line >> numTriggers)) {
        return;
      }
      for (UInt32 i = 0; (i < numTriggers) && !f.bad(); ++i)
      {
        CarbonCfgPTrigger* pt = new CarbonCfgPTrigger(f, NULL,
                                                      numChannels, err);
        mTriggers.push_back(pt);
      }
    }
    else {
      CarbonCfgPTrigger* pt = new CarbonCfgPTrigger(f, oldTriggerExpr.c_str(),
                                                    numChannels, err);
      mTriggers.push_back(pt);
    }

    if (!mTriggers.empty() && !f.bad()) {
      // Add the channel names from the first trigger
      CarbonCfgPTrigger* trigger0 = mTriggers[0];
      for (UInt32 c = 0; c < numChannels; ++c) {
        const char* cname = trigger0->getChannel(c)->getName();
        if (!mChannelNames.insertWithCheck(cname)) {
          UtString buf;
          buf << "Duplicate channel name: " << cname;
          line.reportError(buf.c_str());
          return;
        }
      }

      // Ensure that every trigger has the same channel names
      for (UInt32 t = 1; t < mTriggers.size(); ++t) {
        CarbonCfgPTrigger* ptrigger = mTriggers[t];
        for (UInt32 c = 0; c < numChannels; ++c) {
          const char* name_0 = trigger0->getChannel(c)->getName();
          const char* name_t = ptrigger->getChannel(c)->getName();
          if (strcmp(name_0, name_t) != 0) {
            UtString buf;
            buf << "Mismatched channel name: " << name_0 << " vs " << name_t;
            line.reportError(buf.c_str());
            return;
          }
        }
      }
    }
  }
} // CarbonCfgPStream::CarbonCfgPStream


CarbonCfgPStream::CarbonCfgPStream(QXmlStreamReader& xr, UtParamFile& /*f*/,
                                   CarbonCfgErrorHandler* err)
{
  mName = QUSTR(xr.attributes().value("name").toString());

  while (xr.readNextStartElement())
  {
    if ("ProfileNet" == xr.name())
    {
      CarbonCfgPNet* pn = new CarbonCfgPNet(xr);
      mNets.push_back(pn);
      mNetNames.insert(pn->getName());
    }
    else if ("ProfileTrigger" == xr.name())
    {
      CarbonCfgPTrigger* pt = new CarbonCfgPTrigger(xr, NULL, 0, err);
      mTriggers.push_back(pt);
    }
    else 
    {
      UtString errMessage;
      errMessage << "Error:" << xr.name();
      INFO_ASSERT(true, errMessage.c_str());
    }
  }

  if (!mTriggers.empty()) {
    UInt32 numChannels = 0;
 
    CarbonCfgPTrigger* trigger = mTriggers[0];
    numChannels = trigger->numChannels();        
    
    // Add the channel names from the first trigger
    CarbonCfgPTrigger* trigger0 = mTriggers[0];
    for (UInt32 c = 0; c < numChannels; ++c) {
      const char* cname = trigger0->getChannel(c)->getName();
      if (!mChannelNames.insertWithCheck(cname)) {
        UtString buf;
        buf << "Duplicate channel name: " << cname;

        err->reportError(buf.c_str());
        return;
      }
    }

    // Ensure that every trigger has the same channel names
    for (UInt32 t = 1; t < mTriggers.size(); ++t) {
      CarbonCfgPTrigger* ptrigger = mTriggers[t];
      for (UInt32 c = 0; c < numChannels; ++c) {
        const char* name_0 = trigger0->getChannel(c)->getName();
        const char* name_t = ptrigger->getChannel(c)->getName();
        if (strcmp(name_0, name_t) != 0) {
          UtString buf;
          buf << "Mismatched channel name: " << name_0 << " vs " << name_t;
          err->reportError(buf.c_str());
          return;
        }
      }
    }
  }
} // CarbonCfg

CarbonCfgPTrigger::CarbonCfgPTrigger(UtParamFile& f, const char* expr,
                                     UInt32 numChannels,
                                     CarbonCfgErrorHandler* err)
{
  UtIStringStream ln;

  // new profiling streams contain the expression on their own line,
  // along with the keyword "ptrigger" (which is only needed for humans).
  // old profiling streams got the expression from the pstream line,
  // and don't have the "ptrigger" keyword
  if (expr != NULL) {
    mTriggerExpr = expr;
  }
  else if (! f.getline(&ln) || ! (ln >> "ptrigger" >> mTriggerExpr)) {
    return;
  }

  // Read the channels -- this is identical, regardless of whether we
  // are reading old pstreams or new pstreams
  for (UInt32 i = 0; (i < numChannels) && !f.bad(); ++i) {
    if (!f.getline(&ln)) {
      err->reportError("End of file reading channels from trigger");
    }
    else {
      CarbonCfgPChannel* pc = new CarbonCfgPChannel(ln, f, err);
      mChannels.push_back(pc);
    }
  }
} // CarbonCfgPTrigger::CarbonCfgPTrigger


CarbonCfgPTrigger::CarbonCfgPTrigger(QXmlStreamReader& xr, const char* expr,
                                     UInt32 /*numChannels*/,
                                     CarbonCfgErrorHandler* err)
{
  if (expr != NULL)
    mTriggerExpr = expr;

  mTriggerExpr = QUSTR(xr.attributes().value("expression").toString());

  while(xr.readNextStartElement())
  {
    if ("ProfileChannel" == xr.name())
    {
      CarbonCfgPChannel* pc = new CarbonCfgPChannel(xr, err);
      mChannels.push_back(pc);
    }
    else
    {
      xr.skipCurrentElement();
    }
  }
} // CarbonCfgPTrigger::CarbonCfgPTrigger


CarbonCfgPTrigger::CarbonCfgPTrigger(const char* expr)
  : mTriggerExpr(expr)
{
}

CarbonCfgPTrigger::~CarbonCfgPTrigger() {
  mChannels.clearPointers();
}

void CarbonCfgPTrigger::write(CfgXmlWriter& f) {
  //f << "ptrigger" << mTriggerExpr << '\n';
  f.addElement("ProfileTrigger");
  f.addAttribute("expression", mTriggerExpr);

  for (UInt32 i = 0; i < mChannels.size(); ++i) {
    mChannels[i]->write(f);
  }

  f.closeElement();
}

CarbonCfgPStream::~CarbonCfgPStream() {
  mNets.clearPointers();
  for (UInt32 i = 0; i < mTriggers.size(); ++i) {
    delete mTriggers[i];      // can't use clearPointers cause dtor is private
  }
  clearChannelDescriptors();
}
void CarbonCfgPStream::write(CfgXmlWriter& f) {
  f.addElement("ProfileStream");
  f.addAttribute("name", mName);

  //f << "pstream" << mName << mTriggerExpr << mNets.size()
  //  << mChannelNames.size() << mTriggers.size() << '\n';

  for (UInt32 i = 0; i < mNets.size(); ++i) {
    mNets[i]->write(f);
  }
  for (UInt32 i = 0; i < mTriggers.size(); ++i) {
    mTriggers[i]->write(f);
  }

  f.closeElement();
}

void CarbonCfgPStream::addChannel(const char* channelName) {
  mChannelNames.insert(channelName);
}

CarbonCfgPChannel* CarbonCfgPTrigger::addChannel(const char* channelName) {
  CarbonCfgPChannel* pc = new CarbonCfgPChannel(channelName);
  mChannels.push_back(pc);
  return pc;
}

void CarbonCfgPStream::getUniqueChannelName(UtString* outbuf, const char* name)
{
  CarbonCfg::getUniqueName(outbuf, &mChannelNames, name);
}

bool CarbonCfgPStream::isBucketChannel(UInt32 channelIndex) const {
  // Check to make sure none of the images of this channel have
  // buckets
  for (UInt32 t = 0, nt = numTriggers(); t < nt; ++t) {
    CarbonCfgPTrigger* ptrigger = getTrigger(t);
    CarbonCfgPChannel* pchannel = ptrigger->getChannel(channelIndex);
    if (pchannel->numBuckets() != 0) {
      return true;
    }
  }
  return false;
}

CarbonCfgPTrigger* CarbonCfgPStream::addTrigger() {
  CarbonCfgPTrigger* pt = new CarbonCfgPTrigger("1");
  for (UInt32 i = 0, n = numChannels(); i < n; ++i) {
    (void) pt->addChannel(getChannelName(i));
  }
  mTriggers.push_back(pt);
  return pt;
}

void CarbonCfgPStream::changeChannelName(UInt32 index, const char* name) {
  INFO_ASSERT(! mTriggers.empty(),
              "Cannot have channels if there are no triggers");
  CarbonCfgPTrigger* ptrigger = mTriggers[0];
  CarbonCfgPChannel* pchannel = ptrigger->getChannel(index);

  // Remove the current name from the port set
  mChannelNames.erase(pchannel->getName());

  UtString buf;
  getUniqueChannelName(&buf, name);
  mChannelNames.insert(buf);

  // Make all the channel names consistent across every trigger
  for (UInt32 i = 0; i < mTriggers.size(); ++i) {
    CarbonCfgPTrigger* ptrigger = mTriggers[i];
    CarbonCfgPChannel* pchannel = ptrigger->getChannel(index);
    pchannel->putName(buf.c_str());
  }
}

void CarbonCfgPStream::removeChannel(UInt32 index) {
  INFO_ASSERT(! mTriggers.empty(),
              "Cannot have channels if there are no triggers");
  CarbonCfgPTrigger* ptrigger = mTriggers[0];
  CarbonCfgPChannel* pchannel = ptrigger->getChannel(index);

  // Remove the current name from the port set
  mChannelNames.erase(pchannel->getName());

  // Remove the channel names in every trigger
  for (UInt32 i = 0; i < mTriggers.size(); ++i) {
    CarbonCfgPTrigger* ptrigger = mTriggers[i];
    ptrigger->removeChannel(index);
  }
}

void CarbonCfgPTrigger::removeChannel(UInt32 index) {
  CarbonCfgPChannel* pchannel = mChannels[index];
  mChannels.erase(mChannels.begin() + index);
  delete pchannel;
}

CarbonCfgPNet* CarbonCfgPStream::addNet(const char* netName, const char* name,
                                        UInt32 width)
{
  // get a unique name from the leaf...
  CarbonCfgPNet* pn = new CarbonCfgPNet(netName, name, width);
  mNets.push_back(pn);
  mNetNames.insert(netName);
  return pn;
}

// source: http://msdn.microsoft.com/library/default.asp?url=/library/en-us/
//         vclang/html/_pluslang_c.2b2b_.keywords.asp
static const char* sReservedWords[] = {
  "bool", "break", "case", "catch", "char", "class", "const", "const_cast",
  "continue", "default", "delete", "deprecated", "dllexport", "dllimport",
  "do", "double", "dynamic_cast", "else", "enum", "explicit", "extern",
  "false", "float", "for", "friend", "goto", "if", "inline", "int", "long",
  "mutable", "naked", "namespace", "new", "noinline", "noreturn", "nothrow",
  "novtable", "operator", "private", "property", "protected", "public",
  "register", "reinterpret_cast", "return", "selectany", "short", "signed",
  "sizeof", "static", "static_cast", "struct", "switch", "template", "this",
  "thread", "throw", "true", "try", "typedef", "typeid", "typename", "union",
  "unsigned", "using", "uuid", "virtual", "void", "volatile", "wchar_t",
  "while", NULL
};

static UtStringSet* sReservedWordSet = NULL;

void CarbonCfgPStream::getUniqueNetName(UtString* outbuf, const char* name)
{
  // Cache a lookup table with all active streams.  This will show up as
  // a memory leak for the moment.  I should figure out a good approach
  // to avoid reporting this as a false leak
  if (sReservedWordSet == NULL) {
    sReservedWordSet = new UtStringSet;
    for (const char** p = sReservedWords; *p != NULL; ++p) {
      sReservedWordSet->insert(*p);
    }
    sReservedWordSet->insert("posedge");
    sReservedWordSet->insert("negedge");
    sReservedWordSet->insert("changed");
  }

  UtString fixReserveBuf;
  if ((*name == '_') ||    // leading underscore is C/C++ reserved
      sReservedWordSet->find(name) != sReservedWordSet->end()) {
    fixReserveBuf << "x" << name;
    name = fixReserveBuf.c_str();
  }

  // Net names, for the moment, are emitted as unprefixed method names
  // in the generated C++.  So we need to avoid hitting any C or C++
  // keywords.  We also reserve names posedge,negedge,changed.

  CarbonCfg::getUniqueName(outbuf, &mNetNames, name);
}

void CarbonCfgPStream::changeNetName(CarbonCfgPNet* pnet, const char* name) {
  // Remove the current name from the port set
  mNetNames.erase(pnet->getName());

  UtString buf;
  getUniqueNetName(&buf, name);
  mNetNames.insert(buf);
  pnet->putName(buf.c_str());
}

void CarbonCfgPStream::removeNet(CarbonCfgPNet* pnet) {
  mNetNames.erase(pnet->getName());
  mNets.remove(pnet);
  delete pnet;
}

bool CarbonCfgPStream::isNetDeclared(const char* netName) const {
  return mNetNames.find(netName) != mNetNames.end();
}

SInt32 CarbonCfgPStream::findChannelIndex(const char* name) {
  for (UInt32 i = 0, n = numChannels(); i < n; ++i) {
    if (strcmp(name, getChannelName(i)) == 0) {
      return i;
    }
  }
  return -1;
}

const char* CarbonCfgPStream::getChannelName(UInt32 i) const {
  INFO_ASSERT(!mTriggers.empty(), "Cannot ask for channel name if there are no triggers");
  return mTriggers[0]->getChannel(i)->getName();
}

void CarbonCfgPStream::removeTrigger(CarbonCfgPTrigger* ptrigger) {
  mTriggers.remove(ptrigger);
  delete ptrigger;
  if (mTriggers.empty()) {
    mChannelNames.clear();
  }
}

void CarbonCfgPStream::clearChannelDescriptors() {
  mChannelDescriptors.clearPointers();
}

typedef UtHashMap<UtString,UInt32> StringIntMap;
typedef UtArray<StringIntMap*> StringIntMapVec;

void CarbonCfgPStream::computeChannelDescriptors() {
  UInt32 nc = numChannels();
  clearChannelDescriptors();
  StringIntMapVec bucketNameIndexMapVec;

  for (UInt32 t = 0, nt = mTriggers.size(); t < nt; ++t) {
    CarbonCfgPTrigger* ptrigger = mTriggers[t];
    INFO_ASSERT(ptrigger->mChannels.size() == nc, "trigger channel insanity");
    for (UInt32 c = 0; c < nc; ++c) {
      CarbonCfgPChannel* pchannel = ptrigger->getChannel(c);
      const char* cname = pchannel->getName();
      CarbonCfgPChannelDescriptor* cdesc = NULL;

      if (t == 0) {
        bucketNameIndexMapVec.push_back(new StringIntMap);
        cdesc = new CarbonCfgPChannelDescriptor(cname);
        mChannelDescriptors.push_back(cdesc);
      }
      else {
        cdesc = mChannelDescriptors[c];
        INFO_ASSERT(strcmp(cname, cdesc->getName()) == 0,
                    "channel name insanity");
      }
      StringIntMap* bucketNameIndexMap = bucketNameIndexMapVec[c];

      // Buckets with the same names will be merged together, so use a
      // map to avoid repeats, keeping the bucket index

      for (UInt32 b = 0, nb = pchannel->numBuckets(); b < nb; ++b) {
        CarbonCfgPBucket* pbucket = pchannel->getBucket(b);
        const char* name = pbucket->getName();
        if (bucketNameIndexMap->find(name) == bucketNameIndexMap->end()) {
          UInt32 bucketIndex = bucketNameIndexMap->size();
          pbucket->putIndex(bucketIndex);
          (*bucketNameIndexMap)[name] = bucketIndex;
          cdesc->addBucket(pbucket);
        }
        else {
          pbucket->putIndex((*bucketNameIndexMap)[name]);
        }
      }
    }
  }

  // We don't know the total number of unique buckets for each
  // channel across all the triggers until now.  So annotate all
  // Channels with that info
  for (UInt32 c = 0; c < nc; ++c) {
    StringIntMap* bucketNameIndexMap = bucketNameIndexMapVec[c];
    UInt32 numTotalBuckets = bucketNameIndexMap->size();
    delete bucketNameIndexMap;

    for (UInt32 t = 0, nt = mTriggers.size(); t < nt; ++t) {
      CarbonCfgPTrigger* ptrigger = mTriggers[t];
      CarbonCfgPChannel* pchannel = ptrigger->getChannel(c);
      pchannel->putNumTotalBuckets(numTotalBuckets);
    }
  }
}

void carbonCfgPStreamComputeChannelDescriptors(CarbonCfgPStream* pstream) {
  pstream->computeChannelDescriptors();
}

CarbonCfgPChannelDescriptorID
carbonCfgPStreamGetChannelDescriptor(CarbonCfgPStream* pstream,
                                     unsigned int index)
{
  return pstream->getChannelDescriptor(index);
}

const char* carbonCfgPChannelDescriptorGetName(CarbonCfgPChannelDescriptor* pcd) {
  return pcd->getName();
}

unsigned int carbonCfgPChannelDescriptorNumBuckets(CarbonCfgPChannelDescriptor* pcd) {
  return pcd->numBuckets();
}

CarbonCfgPBucketID
carbonCfgPChannelDescriptorGetBucket(CarbonCfgPChannelDescriptor* pcd,
                                     unsigned int index)
{
  return pcd->getBucket(index);
}

const char* carbonCfgPBucketGetName(const CarbonCfgPBucketID pbucket) {
  return pbucket->getName();
}

unsigned int carbonCfgPBucketGetIndex(const CarbonCfgPBucketID pbucket) {
  return pbucket->getIndex();
}

const char* carbonCfgPBucketGetExpr(const CarbonCfgPBucketID pbucket) {
  return pbucket->getExpr();
}

CarbonCfgColor carbonCfgPBucketGetColor(CarbonCfgPBucketID pbucket) {
  return pbucket->getColor();
}

const char* carbonCfgPBucketGetColorString(CarbonCfgPBucketID pbucket) {
  return pbucket->getColorString();
}

void carbonCfgPBucketPutExpr(CarbonCfgPBucketID pbucket, const char* expr) {
  return pbucket->putExpr(expr);
}

void carbonCfgPBucketPutColor(CarbonCfgPBucketID pbucket, CarbonCfgColor color)
{
  pbucket->putColor(color);
}

const char* carbonCfgPNetGetName(const CarbonCfgPNetID pnet) {
  return pnet->getName();
}

const char* carbonCfgPNetGetPath(const CarbonCfgPNetID pnet) {
  return pnet->getPath();
}

unsigned int carbonCfgPNetGetWidth(const CarbonCfgPNetID pnet) {
  return pnet->getWidth();
}

const char* carbonCfgPChannelGetName(const CarbonCfgPChannelID pchannel) {
  return pchannel->getName();
}

const char* carbonCfgPChannelGetExpr(const CarbonCfgPChannelID pchannel) {
  return pchannel->getExpr();
}

unsigned int carbonCfgPChannelNumBuckets(const CarbonCfgPChannelID pchannel) {
  return pchannel->numBuckets();
}

unsigned int carbonCfgPChannelNumTotalBuckets(const CarbonCfgPChannelID pchannel) {
  return pchannel->getNumTotalBuckets();
}

CarbonCfgPBucketID getBucket(const CarbonCfgPChannelID pchannel,
                             unsigned int i)
{
  return pchannel->getBucket(i);
}

void carbonCfgPChannelPutExpr(CarbonCfgPChannelID pchannel,
                              const char* expr)
{
  pchannel->putExpr(expr);
}

CarbonCfgPBucketID carbonCfgPChannelAddBucket(CarbonCfgPChannelID pchannel,
                                              const char* name)
{
  return pchannel->addBucket(name);
}

void carbonCfgPChannelChangeBucketName(CarbonCfgPChannelID pchannel,
                                       CarbonCfgPBucketID pbucket,
                                       const char* name)
{
  pchannel->changeBucketName(pbucket, name);
}

void carbonCfgPChannelRemoveBucket(CarbonCfgPChannelID pchannel,
                                   CarbonCfgPBucketID pbucket)
{
  pchannel->removeBucket(pbucket);
}


const char* carbonCfgPStreamGetName(const CarbonCfgPStreamID pstream) {
  return pstream->getName();
}

unsigned int carbonCfgPStreamNumTriggers(const CarbonCfgPStreamID pstream) {
  return pstream->numTriggers();
}

CarbonCfgPTriggerID carbonCfgPStreamGetTrigger(const CarbonCfgPStreamID pstream,
                                               unsigned int idx)
{
  return pstream->getTrigger(idx);
}

const char* carbonCfgPTriggerGetExpr(const CarbonCfgPTriggerID ptrigger) {
  return ptrigger->getTriggerExpr();
}

CarbonCfgPChannelID carbonCfgPTriggerGetChannel(const CarbonCfgPTriggerID ptrigger, unsigned int i) {
  return ptrigger->getChannel(i);
}


unsigned int carbonCfgPStreamNumNets(const CarbonCfgPStreamID pstream) {
  return pstream->numNets();
}

CarbonCfgPNetID carbonCfgPStreamGetNet(const CarbonCfgPStreamID pstream,
                                      unsigned int i)
{
  return pstream->getNet(i);
}

unsigned int carbonCfgPStreamNumChannels(const CarbonCfgPStreamID pstream) {
  return pstream->numChannels();
}

void carbonCfgPTriggerPutExpr(CarbonCfgPTriggerID ptrigger,
                              const char* expr)
{
  ptrigger->putTriggerExpr(expr);
}

void carbonCfgPStreamAddChannel(CarbonCfgPStreamID pstream,
                                const char* name)
{
  pstream->addChannel(name);
}
void carbonCfgPStreamRemoveChannel(CarbonCfgPStreamID pstream,
                                   unsigned int i)
{
  pstream->removeChannel(i);
}
void carbonCfgPStreamChangeChannelName(CarbonCfgPStreamID pstream,
                                       unsigned int idx,
                                       const char* name)
{
  pstream->changeChannelName(idx, name);
}

CarbonCfgPNetID carbonCfgPStreamAddNet(CarbonCfgPStreamID pstream,
                                      const char* path,
                                      const char* name,
                                      unsigned int width)
{
  return pstream->addNet(path, name, width);
}

void carbonCfgPStreamRemoveNet(CarbonCfgPStreamID pstream,
                               CarbonCfgPNetID pnet)
{
  pstream->removeNet(pnet);
}
void carbonCfgPStreamChangeNetName(CarbonCfgPStreamID pstream,
                                   CarbonCfgPNetID pnet,
                                   const char* name)
{
  pstream->changeNetName(pnet, name);
}


CarbonCfgPStreamID carbonCfgAddPStream(CarbonCfgID cfg, const char* name) {
  return cfg->addPStream(name);
}

unsigned int carbonCfgNumPStreams(const CarbonCfgID cfg) {
  return cfg->numPStreams();
}

CarbonCfgPStreamID carbonCfgGetPStream(const CarbonCfgID cfg, unsigned int i) {
  return cfg->getPStream(i);
}

void carbonCfgChangePStreamName(CarbonCfgID cfg, CarbonCfgPStreamID pstream,
                                const char* name)
{
  cfg->changePStreamName(pstream, name);
}

void carbonCfgRemovePStream(CarbonCfgID cfg, CarbonCfgPStreamID pstream) {
  cfg->removePStream(pstream);
}

void
CarbonCfgMsgHandler::reportMessage(CarbonMsgSeverity sev, const char* msg, 
                                   const char* file, int lineno)
{
  // Create a severity string and update counts/flags
  const char* severityStr = "";
  switch (sev) {
    case eCarbonMsgStatus:
      severityStr = "Status";
      break;

    case eCarbonMsgNote:
      severityStr = "Note";
      break;

    case eCarbonMsgWarning:
      severityStr = "Warning";
      ++mWarnings;
      break;

    case eCarbonMsgError:
      severityStr = "Error";
      ++mErrors;
      break;

    case eCarbonMsgFatal:
      severityStr = "Fatal";
      ++mErrors;
      break;

    case eCarbonMsgSuppress:
      severityStr = "Suppress";
      break;

    case eCarbonMsgAlert:
      severityStr = "Alert";
      ++mErrors;
      break;
  }

  // Emit the optional file and line number
  if (file != NULL) {
    mErrMsg << file << ":";
    if (lineno != 0) {
      mErrMsg << lineno << ": ";
    } else {
      mErrMsg << " ";
    }
  }

  // Emit the message
  mErrMsg << severityStr << ": " << msg << "\n";
}

void CarbonCfgMsgHandler::clear()
{
  mErrMsg.clear();
  mWarnings = mErrors = 0;
}

void CarbonCfgMsgHandler::putMsg(const char* msg)
{
  mErrMsg = msg;
}

bool CarbonCfgTemplate::addVariable(const char* variable, const char* value)
{
  // Validate that this variable is of the right form
  int len = strlen(variable);
  if ((variable[0] != '$') || (variable[1] != '(') || (variable[len-1] != ')')) {
    UtString msg;
    msg << "invalid framing '" << variable
        << "' in template variable; must be of the form $(var)";
    mMsgHandler->reportMessage(eCarbonMsgError, msg.c_str());
    return false;
  }
  for (int i = 2; i < len - 1; ++i) {
    bool passed = (i == 2) ? isalpha(variable[i]) : isalnum(variable[i]);
    if (!passed) {
      UtString msg;
      msg << "invalid character '" << variable[i]
          << "' in template variable''" << variable
          << "; Variable names must start with alpha char, then only alpha-numeric characters allowed in remainder of name.";
      mMsgHandler->reportMessage(eCarbonMsgError, msg.c_str());
      return false;
    }
  }

  // Make sure this doesn't exist in the map
  UtString key(variable);
  VariableMap::const_iterator pos = mVariableMap.find(key);
  if (pos != mVariableMap.end()) {
    UtString msg;
    msg << "duplicate variable '" << variable
        << "' in template ignore; previous version = '" << pos->second
        << "'; this version = '" << value << "'";
    mMsgHandler->reportMessage(eCarbonMsgWarning, msg.c_str());
    return false;
  }

  // All good, add it
  mVariableMap[key] = value;
  mVariables.push_back(new UtString(key));
  return true;
}

void CarbonCfgTemplate::clear(void)
{
  mVariableMap.clear();
  for (Loop<Variables> l(mVariables); !l.atEnd(); ++l) {
    UtString* var = *l;
    delete var;
  }
  mVariables.clear();
}

bool CarbonCfgTemplate::resolveString(const char* src, UtString* dst) const
{
  // We assume they want the destination cleared.
  dst->clear();

  // Walk the string copying normal characters. When we see $( it must
  // be a variable start. Translate that variable and then continue.
  bool success = true;
  UInt32 i = 0;
  while(src[i] != '\0') {
    if ((src[i] == '$') && (src[i+1] == '(')) {
      // Found a variable, construct it
      UtString key("$(");
      i += 2;
      for (; (src[i] != '\0') && (src[i] != ')'); ++i) {
        key << src[i];
      }
      if (src[i] == ')') {
        ++i;
        key << ')';
      } else {
        // Invalid variable, give up
        UtString msg;
        msg << "invalid variable format in string '"
            << src << "'.";
        mMsgHandler->reportMessage(eCarbonMsgError, msg.c_str());
        return false;
      }

      // Look up the variable
      VariableMap::const_iterator pos = mVariableMap.find(key);
      if (pos == mVariableMap.end()) {
        // Doesn't exist in the map, pring a warning an keep going by
        // keeping the variable in the output
        UtString msg;
        msg << "variable '" << key << "' does not exist in template map";
        mMsgHandler->reportMessage(eCarbonMsgWarning, msg.c_str());
        (*dst) << key;
        success = false;

      } else {
        // Successful translation: add the value to the end of the
        // destination string
        (*dst) << pos->second;
      }

    } else {
      // Normal processing, copy the character
      (*dst) << src[i];
      ++i;
    }
  } // while

  return success;
}

const char* CarbonCfgTemplate::getVariable(UInt32 index) const
{
  SCRIPT_ASSERT_RETURN(index < mVariables.size(), "Template variable out of range", "");
  return mVariables[index]->c_str();
}

const char* CarbonCfgTemplate::getValue(const char* variable) const
{
  UtString key(variable);
  VariableMap::const_iterator pos = mVariableMap.find(key);
  if (pos != mVariableMap.end()) {
    return pos->second.c_str();
  } else {
    return NULL;
  }
}

const char* carbonCfgCustomCodeGetSectionString(CarbonCfgCustomCodeID cc)
{
  return cc->getSectionString();
}

CarbonCfgCustomCodePosition carbonCfgCustomCodeGetPosition(CarbonCfgCustomCodeID cc)
{
  return cc->getPosition();
}

const char* carbonCfgCustomCodeGetCode(CarbonCfgCustomCodeID cc)
{
  return cc->getCode();
}

void carbonCfgPutType(CarbonCfgID cfg, const char* name)
{
  cfg->putCompType(name);
}

const char* carbonCfgGetType(CarbonCfgID cfg)
{
  return cfg->getCompType();
}

void carbonCfgPutVersion(CarbonCfgID cfg, const char* name)
{
  cfg->putCompVersion(name);
}

const char* carbonCfgGetVersion(CarbonCfgID cfg)
{
  return cfg->getCompVersion();
}

void carbonCfgPutDocFile(CarbonCfgID cfg, const char* name)
{
  cfg->putCompDocFile(name);
}

const char* carbonCfgGetDocFile(CarbonCfgID cfg)
{
  return cfg->getCompDocFile();
}

const char* carbonCfgGetComponentTag(CarbonCfgID cfg)
{
  UtString value;
  value << cfg->GetComponentTag();
  return value.c_str();
}

void carbonCfgPutRequiresMkLibrary(CarbonCfgID cfg, unsigned int req)
{
  cfg->putRequiresMkLibrary(req != 0);
}

unsigned int carbonCfgGetRequiresMkLibrary(CarbonCfgID cfg)
{
  return cfg->getRequiresMkLibrary();
}

void carbonCfgPutUseVersionedMkLibrary(CarbonCfgID cfg, unsigned req)
{
  cfg->putUseVersionedMkLibrary(req != 0);
}

unsigned int carbonCfgGetUseVersionedMkLibrary(CarbonCfgID cfg)
{
  return cfg->getUseVersionedMkLibrary();
}

void carbonCfgPutPCTraceAccessor(CarbonCfgID cfg, const char* accessor)
{
  cfg->putPCTraceAccessor(accessor);
}

const char* carbonCfgGetPCTraceAccessor(CarbonCfgID cfg)
{
  return cfg->getPCTraceAccessor();
}

unsigned int carbonCfgGetNumSystemCTypes(CarbonCfgID cfg)
{
  return cfg->numSystemCTypes();
}

const char* carbonCfgGetSystemCType(CarbonCfgID cfg, unsigned int i)
{
  return cfg->getSystemCType(i);
}



void CarbonCfg::findEmbeddedModules(CarbonDB* db, QMap<QString, CarbonEmbeddedModuleItems*>& moduleMap, const CarbonDBNode* parent)
{
  const char* compName = ::carbonDBComponentName(db, parent);
  // If we have a module, see if we have an associated script.
  if (compName)
  {
    if (mScriptableModuleMap.contains(compName))
    {
      CarbonEmbeddedModuleItems* moduleItems = NULL;
      // Build the list if this the first one.
      if (!moduleMap.contains(compName))
      {
        moduleItems = new CarbonEmbeddedModuleItems();
        moduleItems->mScriptFileName = mScriptableModuleMap[compName];
        moduleItems->mInstances.clear();
        moduleMap[compName] = moduleItems;
      }
      else
        moduleItems = moduleMap[compName];
      
      // carbonDb owns the node, it will free it.
      moduleItems->mInstances.append((CarbonDBNode*)parent);
    }
  }

  CarbonDBNodeIter* iter = ::carbonDBLoopChildren(db, parent);
  const CarbonDBNode* node = NULL; 
  while ((node = ::carbonDBNodeIterNext(iter)) != NULL) 
  {
    if (! (::carbonDBIsArray(db, node) || ::carbonDBIsStruct(db, node)))
      findEmbeddedModules(db, moduleMap, node);
  }

  ::carbonDBFreeNodeIter(iter);
}

// Scan the Database for special modules and delegate
// construction to script functions
CarbonCfgStatus CarbonCfg::processEmbeddedModules()
{
  CarbonDB* db = getDB();

  // Only for non-empty IODB File records
  if (!mIODBFile.empty())
  {
    UtString dbFilePath(mIODBFile.c_str());

    // Absolute path?
    bool iodbExists = fileExists(mIODBFile.c_str());
    if (!iodbExists)
    {
      dbFilePath.clear();
      OSConstructFilePath(&dbFilePath, mCcfgFilePath.c_str(), mIODBFile.c_str());  
    }
    bool freeDatabase = false;
    // We haven't loaded the DB which we want here to process the Embedded Models
    if (db == NULL && fileExists(dbFilePath.c_str()))
    {
      freeDatabase = true;
      db = carbonDBOpenFile(dbFilePath.c_str());
    }

    if (db)
    {    
      mModuleMap.clear();

      CarbonDBNodeIter* iter = carbonDBLoopDesignRoots(db);

      const CarbonDBNode* node = NULL;
      while ((node = ::carbonDBNodeIterNext(iter)) != NULL) 
      {
        findEmbeddedModules(db, mModuleMap, node);
      }
      ::carbonDBFreeNodeIter(iter);

      // We need to keep the Database
      if (!mModuleMap.empty())
      {        
        CfgScriptingCarbonDB* scriptdb = new CfgScriptingCarbonDB(0, this, db);

        mScriptingDB = scriptdb;

        foreach(QString module, mModuleMap.keys())
        {
          CarbonEmbeddedModuleItems* scriptItem = mModuleMap[module];
                    
          registerTypes(&scriptItem->mScriptEngine);
          // Register enumerators
          scriptItem->mScriptEngine.createEnumerationWrappers(scriptItem->mScriptEngine.globalObject());
          scriptdb->registerTypes(&scriptItem->mScriptEngine);

          QString scriptName = scriptItem->mScriptFileName;

          QFileInfo fi(scriptName);
          if (fi.exists())
          {
            if(mCcfgDebugOutput)
              qDebug() << "Looking for Script" << scriptName;

            foreach(CarbonCfgScriptExtension* ext, mScriptExtensions)
            {
              ext->registerTypes(&scriptItem->mScriptEngine);
            }

            scriptItem->mScriptEngine.runScript(scriptName);

            QScriptValueList args;
            // 1st Argument
            args.append(scriptItem->mScriptEngine.newQObject(this));

            // 2nd Argument
            args.append(scriptItem->mScriptEngine.newQObject(scriptdb));

            // Build an array of CarbonDBNode objects
            int numItems = scriptItem->mInstances.count();
            
            scriptItem->mItems = scriptItem->mScriptEngine.newArray(numItems);

            for (int i=0; i<numItems; i++)
            {
              CarbonDBNode* node = scriptItem->mInstances[i];
              QScriptValue so = scriptItem->mScriptEngine.newVariant(qVariantFromValue(node));
              scriptItem->mItems.setProperty(i, so);
            }
            
            // 3rd argument
            args.append(scriptItem->mItems);

            // Save return value and it will be 4th argument to restore
            scriptItem->mReturnValue = scriptItem->mScriptEngine.callOptionalScriptFunction("Construct", args);
          }
        }
      }
      else if (freeDatabase) // Only free it if we loaded it.
        carbonDBFree(db);
    }
  }

  QString xmlBuffer = mComponentJournalXML.c_str();
  QDomDocument doc;

  if (doc.setContent(xmlBuffer))
  { 
    QDomElement docElem = doc.documentElement();
    
    // first component
    QDomElement compElem = docElem.firstChildElement("component");  
    
    restoreSubComponentsJournal(compElem);
  }

  return eCarbonCfgSuccess;
}

// parent - component
void CarbonCfg::restoreSubComponentsJournal(const QDomElement& parent)
{
  for(QDomElement e = parent.firstChildElement("subcomponents"); e.isElement(); e = e.nextSiblingElement("subcomponents"))
  {
    restoreComponentJournal(e);
  }

  // Now, call optional script function
  foreach(QString module, mModuleMap.keys())
  {
    CarbonEmbeddedModuleItems* scriptItem = mModuleMap[module];
    QScriptValueList args;
    
    // 1st Argument
    args.append(scriptItem->mScriptEngine.newQObject(this));

    // 2nd Argument
    args.append(scriptItem->mScriptEngine.newQObject(mScriptingDB));

    // 3rd agument
    args.append(scriptItem->mItems);

    // 4th Argument
    args.append(scriptItem->mReturnValue);

    scriptItem->mScriptEngine.callOptionalScriptFunction("Restore", args);
  }

  // Now, free up all the engines
  foreach(QString module, mModuleMap.keys())
  {
    CarbonEmbeddedModuleItems* scriptItem = mModuleMap[module];
    delete scriptItem;
  }

  mModuleMap.clear();
}

// parent - componentJournal
void CarbonCfg::restoreComponentJournal(const QDomElement& parent)
{
  for(QDomElement e = parent.firstChildElement("componentJournal"); e.isElement(); e = e.nextSiblingElement("componentJournal"))
  {
    QString origName = e.attribute("origName");
    QString name = e.attribute("name");

    UtString on; on << origName;
    CarbonCfg* comp = findSubComponent(on.c_str());
    if (comp)
    {
      comp->SetCompName(name);
      for(QDomElement mje = e.firstChildElement("memoryJournal"); mje.isElement(); mje = mje.nextSiblingElement("memoryJournal"))
      {
        restoreMemoryJournal(comp, mje);
      }
    }
    else
    {
      UtString msg;
      msg << "Unable to locate Subcomponent '" << name << "'";
      reportMessage(eCarbonMsgWarning, msg.c_str());
    }
  }
}
// parent - memoryJournal
void CarbonCfg::restoreMemoryJournal(CarbonCfg* comp, const QDomElement& parent)
{
  for(QDomElement e = parent.firstChildElement("memSpace"); e.isElement(); e = e.nextSiblingElement("memSpace"))
  {
    QString memName = e.attribute("name");
    bool foundMemory = false;
    for (UInt32 mi=0; mi<comp->numMemories(); mi++)
    {
      CarbonCfgMemory* mem = comp->getMemory(mi);
      if (memName == mem->getName())
      {
        foundMemory = true;
        mem->restoreMemoryJournal(comp, e);
        break;
      }
    }

    if (!foundMemory)
    {
      UtString msg;
      msg << "Unable to locate Subcomponent '" << comp->getCompName() << "' Memory: '" << memName << "'";
      reportMessage(eCarbonMsgWarning, msg.c_str());
    }
  }
}

// parent - memoryJournal
void CarbonCfgMemory::restoreMemoryJournal(CarbonCfg*, const QDomElement& parent)
{
  // <debugAccesss> ?
  QDomElement eDebugAccess = parent.firstChildElement("debugAccess");
  while (eDebugAccess.isElement())
  {
    QDomElement eDebugPort = eDebugAccess.firstChildElement("debugPort");
    QString eslPortName = eDebugPort.attribute("name");

    SetInitType(CcfgEnum::MemInitProgPreload);

    UtString pn; pn << eslPortName;
    
    QString sBaseAddress = eDebugAccess.attribute("baseAddress");
    qint64 baseAddress = sBaseAddress.toLongLong();
    
    // Check for existing match, if so, change value
    bool foundExisting = false;
    for(quint32 i=0; i<numSystemAddressESLPorts(); i++)
    {
      QString portName = getSystemAddressESLPortName(i);
      if (portName == eslPortName)
      {
        putSystemAddressESLPortName(i, pn.c_str());
        putSystemAddressESLPortBaseAddress(i, baseAddress);
        foundExisting = true;
        break;
      }
    }

    // othwerwise, add a new one
    if (!foundExisting)
      addSystemAddressESLPort(pn.c_str(), baseAddress);

    eDebugAccess = eDebugAccess.nextSiblingElement("debugAccess");
  }
}


CfgScriptingEngine::CfgScriptingEngine()
{
  addBuiltinFunctions();
}


CfgScriptingEngine::~CfgScriptingEngine()
{
}


// Scripting Constructors
static QScriptValue constructCarbonCfgMemoryLocRTL(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 3)
  {
    QString rtlPath = qscriptvalue_cast<QString>(ctx->argument(0));
    quint32 bitWidth =  qscriptvalue_cast<quint32>(ctx->argument(1));
    quint32 endWord =  qscriptvalue_cast<quint32>(ctx->argument(2));

    UtString rtlpath;
    rtlpath << rtlPath;

    CarbonCfgMemoryLocRTL* memLocRTL = new CarbonCfgMemoryLocRTL(rtlpath.c_str(), bitWidth, endWord);
    return eng->toScriptValue(memLocRTL);
  }
  else
    ctx->throwError("CarbonCfgMemoryLocRTL() constructor requires 3 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgMemoryLocPort(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    QString portName = qscriptvalue_cast<QString>(ctx->argument(0));
    
    UtString portname;
    portname << portName;

    CarbonCfgMemoryLocPort* memLocPort = new CarbonCfgMemoryLocPort(portname.c_str());
    return eng->toScriptValue(memLocPort);
  }
  else
    ctx->throwError("CarbonCfgMemoryLocPort() constructor requires 1 arguments");

  return QScriptValue();
}


static QScriptValue constructCarbonCfgXtorConn(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 3)
  {
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));
    CarbonCfgXtorInstance* xtorInst = qscriptvalue_cast<CarbonCfgXtorInstance*>(ctx->argument(1));
    quint32 index = qscriptvalue_cast<quint32>(ctx->argument(2));

    CarbonCfgXtorConn* xtorConn = new CarbonCfgXtorConn(rtlPort, xtorInst, index);
    rtlPort->connect(xtorConn);
    return eng->toScriptValue(xtorConn);
  }
  else
    ctx->throwError("CarbonCfgXtorConn() constructor requires 3 arguments");

  return QScriptValue();
}
 
static QScriptValue constructCarbonCfgResetGen(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 8)
  {
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));
    quint64 activeValue = qscriptvalue_cast<quint64>(ctx->argument(1));
    quint64 inactiveValue = qscriptvalue_cast<quint64>(ctx->argument(2));
    quint32 clockCycles = qscriptvalue_cast<quint32>(ctx->argument(3));
    quint32 compCycles = qscriptvalue_cast<quint32>(ctx->argument(4));
    quint32 cyclesBefore = qscriptvalue_cast<quint32>(ctx->argument(5));
    quint32 cyclesAsserted = qscriptvalue_cast<quint32>(ctx->argument(6));
    quint32 cyclesAfter = qscriptvalue_cast<quint32>(ctx->argument(7));

    CarbonCfgResetGen* resetGen = new CarbonCfgResetGen(rtlPort, activeValue,
      inactiveValue, clockCycles, compCycles, cyclesBefore, cyclesAsserted, cyclesAfter);

    return eng->toScriptValue(resetGen);
 
  }
  else
    ctx->throwError("CarbonCfgResetGen() constructor requires 8 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgClockGen(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 6)
  {
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));
    quint32 init = qscriptvalue_cast<quint32>(ctx->argument(1));
    quint32 delay = qscriptvalue_cast<quint32>(ctx->argument(2));
    quint32 clockCycles = qscriptvalue_cast<quint32>(ctx->argument(3));
    quint32 compCycles = qscriptvalue_cast<quint32>(ctx->argument(4));
    quint32 duty = qscriptvalue_cast<quint32>(ctx->argument(5));

    CarbonCfgClockGen* clkGen = new CarbonCfgClockGen(rtlPort, init, delay, clockCycles, compCycles, duty);
    return eng->toScriptValue(clkGen);
  }
  else
    ctx->throwError("CarbonCfgClockGen() constructor requires 6 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgTie(QScriptContext* ctx, QScriptEngine* eng)
{
  if (ctx->argumentCount() >= 2) {
    // The rtl port is the first argument
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));    

    // The tie value is an array of uint32's passed as separate arguments
    DynBitVector tieVal(ctx->argumentCount()-1);
    UInt32* vals = tieVal.getUIntArray();
    for (int i = 1; i < ctx->argumentCount(); ++i) {
      vals[i-1] = qscriptvalue_cast<quint32>(ctx->argument(i));
    }

    // Construct the new object
    CarbonCfgTie* tie = new CarbonCfgTie(rtlPort, tieVal);
    return eng->toScriptValue(tie);

  } else {
    ctx->throwError("CarbonCfgTie() constructor takes at least 2 arguments");
    return QScriptValue();
  }
}


static QScriptValue
constructCarbonCfgTieParam(QScriptContext* ctx, QScriptEngine* eng)
{
  if (ctx->argumentCount() == 3) {
    // The rtl port is the first argument
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));    

    // The second argument is the xtor inst name
    QString xtorName = qscriptvalue_cast<QString>(ctx->argument(1));
    UtString xName;
    xName << xtorName;

    // The third argument is the transactor param name
    QString paramName = qscriptvalue_cast<QString>(ctx->argument(2));
    UtString pName;
    pName << paramName;

    // Construct the new object
    CarbonCfgTieParam* tieParam = new CarbonCfgTieParam(rtlPort, xName.c_str(),
                                                        pName.c_str());
    return eng->toScriptValue(tieParam);

  } else {
    ctx->throwError("CarbonCfgTie() constructor takes at least 2 arguments");
    return QScriptValue();
  }
}

static QScriptValue constructCarbonCfgRegisterField(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 3)
  {
    quint32 high = qscriptvalue_cast<quint32>(ctx->argument(0));
    quint32 low = qscriptvalue_cast<quint32>(ctx->argument(1));
    int intAccesss = qscriptvalue_cast<int>(ctx->argument(2));
    CarbonCfgRegAccessType access = static_cast<CarbonCfgRegAccessType>(intAccesss);

    CarbonCfgRegisterField* field = new CarbonCfgRegisterField(high, low, access);
    return eng->toScriptValue(field);
  }
  else
    ctx->throwError("CarbonCfgRegisterField() constructor requires 3 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgRegisterLocConstant(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    quint64 value = qscriptvalue_cast<quint64>(ctx->argument(0));
    CarbonCfgRegisterLocConstant* loc = new CarbonCfgRegisterLocConstant(value);
    return eng->toScriptValue(loc);
  }
  else
    ctx->throwError("CarbonCfgRegisterLocConstant() constructor requires 1 argument");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgRegisterLocUser(QScriptContext*, QScriptEngine *eng)
{
  CarbonCfgRegisterLocUser* loc = new CarbonCfgRegisterLocUser();
  return eng->toScriptValue(loc);
}

static QScriptValue constructCarbonCfgRegisterLocReg(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;
    CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(rtlPath.c_str());
    return eng->toScriptValue(loc);
  }
  else if (ctx->argumentCount() == 3)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;

    quint32 left = qscriptvalue_cast<quint32>(ctx->argument(1));
    quint32 right = qscriptvalue_cast<quint32>(ctx->argument(2));
    CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(rtlPath.c_str(), left, right);
    return eng->toScriptValue(loc);
 }
  else
    ctx->throwError("CarbonCfgRegisterLocReg() constructor requires 1 or 3 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgRegisterLocArray(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 2)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;
    quint32 index = qscriptvalue_cast<quint32>(ctx->argument(1));

    CarbonCfgRegisterLocArray* loc = new CarbonCfgRegisterLocArray(rtlPath.c_str(), index);
    return eng->toScriptValue(loc);
  }
  else if (ctx->argumentCount() == 4)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;
    quint32 index = qscriptvalue_cast<quint32>(ctx->argument(1));

    quint32 left = qscriptvalue_cast<quint32>(ctx->argument(2));
    quint32 right = qscriptvalue_cast<quint32>(ctx->argument(3));
    CarbonCfgRegisterLocArray* loc = new CarbonCfgRegisterLocArray(rtlPath.c_str(), index, left, right);
    return eng->toScriptValue(loc);
 }
  else
    ctx->throwError("CarbonCfgRegisterLocReg() constructor requires 2 or 4 arguments");

  return QScriptValue(); 
}


static QScriptValue constructCarbonCfg(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    QString ccfgFileName = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString ccfgPath;
    ccfgPath << ccfgFileName;

    QFileInfo fi(ccfgFileName);
    if (fi.exists())
    {
      CarbonCfg* cfg = carbonCfgCreate();
      UtString fn; fn << ccfgFileName;

      cfg->SetFilePath(ccfgFileName);
      cfg->readNoCheck(fn.c_str());

      UtString iodbFile;
      iodbFile << cfg->getIODBFileName();

      UtString dbFilePath(iodbFile.c_str());

      // Absolute path?
      bool iodbExists = fileExists(iodbFile.c_str());
      if (!iodbExists)
      {
        dbFilePath.clear();
        UtString ccfgBasePath;
        ccfgBasePath << fi.absolutePath();
        OSConstructFilePath(&dbFilePath, ccfgBasePath.c_str(), iodbFile.c_str());  
      }
 
      if (fileExists(dbFilePath.c_str()))
      {
        CarbonDB* db = carbonDBOpenFile(dbFilePath.c_str());
        if (db)
        { 
          CfgScriptingCarbonDB* sdb = new CfgScriptingCarbonDB(0, cfg, db);
          cfg->SetDatabase(sdb);
        }
      }
      else
      {
        QString errMsg = QString("Ccfg() Error: Unable to find or load Carbon Database file: %1").arg(iodbFile.c_str());
        return ctx->throwError(errMsg);
      }

      return eng->toScriptValue(cfg);
    }
    else
    {
      QString errMsg = QString("carbon_cfg() constructor Error: no such file: %1").arg(ccfgFileName);
      ctx->throwError(errMsg);
    }
  }
  else if (ctx->argumentCount() == 0)
  {
    CarbonCfg* cfg = carbonCfgCreate();
    return eng->toScriptValue(cfg);
  }
  
  ctx->throwError("carbon_cfg() constructor requires 1 arguments");
  return QScriptValue();
}

void CarbonCfg::registerTypes(QScriptEngine* engine)
{
  if (engine)
  {
    QScriptValue global = engine->globalObject();    
    // Allows an object to return a property of type ScrCcfg
    qScriptRegisterQObjectMetaType<CarbonCfg*>(engine);

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgElementGenerationType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgElementGenerationType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegisterLoc);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRegisterLocKind>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegAccessType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRegAccessType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRadix);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRadix>(engine); 
 
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRTLPortType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRTLPortType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRTLConnectionType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRTLConnectionType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgStatus);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgStatus>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMode);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMode>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgParamDataType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgParamDataType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgParamFlag);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgParamFlag>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgESLPortType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgESLPortType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgESLPortMode);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgESLPortMode>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemInitType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemInitType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgReadmemType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgReadmemType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemLocType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemLocType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgWaveType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgWaveType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCustomCodePosition);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgCustomCodePosition>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCompCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgCompCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCadiCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgCadiCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRegCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemoryCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemoryCustomCodeSection>(engine); 

    qScriptRegisterEnumMetaType<CcfgFlags::CarbonCfgMemoryEditFlags>(engine);
    qScriptRegisterEnumMetaType<CcfgFlags::CarbonCfgComponentEditFlags>(engine);

    // Static register for classes with Q_ENUMS
    QScriptValue ccfgEnum = engine->newQMetaObject(&CcfgEnum::staticMetaObject);
    engine->globalObject().setProperty("CcfgEnum", ccfgEnum);

    // Static register for classes with Q_FLAGS
    QScriptValue ccfgFlags = engine->newQMetaObject(&CcfgFlags::staticMetaObject);
    engine->globalObject().setProperty("CcfgFlags", ccfgFlags);


    // If you register these in addition to the ScrCcfg ones, you will
    // get an error when trying to resolve getType() on a CarbonCfgRTLConnection*
    // 
    qScriptRegisterQObjectMetaType<CarbonCfgRTLConnection*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgESLPort*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgXtorInstance*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRTLPort*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRTLConnection*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgTieParam*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgTie*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgResetGen*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgClockGen*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgXtorConn*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgXtor*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgXtorLib*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgXtorPort*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgXtorParam*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgXtorParamInst*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegister*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgGroup*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegisterField*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegisterLoc*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegisterLocConstant*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegisterLocUser*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegisterLocRTL*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegisterLocReg*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegisterLocArray*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemory*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemoryBlock*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemoryLoc*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemoryLocRTL*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemoryLocPort*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemoryLocUser*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgELFLoader*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgProcInfo*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgCustomCode*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgCompCustomCode*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgCadiCustomCode*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgRegCustomCode*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemoryBlockCustomCode*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgMemoryCustomCode*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgTemplate*>(engine);
    qScriptRegisterQObjectMetaType<CarbonCfgCadi*>(engine);

    // Constructors

    QScriptValue resetGenCtor = engine->newFunction(constructCarbonCfgResetGen);
    engine->globalObject().setProperty("CcfgCarbonCfgResetGen", resetGenCtor);
 
    QScriptValue clkGenCtor = engine->newFunction(constructCarbonCfgClockGen);
    engine->globalObject().setProperty("CcfgCarbonCfgClockGen", clkGenCtor);

    QScriptValue tieCtor = engine->newFunction(constructCarbonCfgTie);
    engine->globalObject().setProperty("CcfgCarbonCfgTie", tieCtor);

    QScriptValue tieParamCtor = engine->newFunction(constructCarbonCfgTieParam);
    engine->globalObject().setProperty("CcfgCarbonCfgTieParam", tieParamCtor);

    QScriptValue memLocPortctor = engine->newFunction(constructCarbonCfgMemoryLocPort);
    engine->globalObject().setProperty("CcfgCarbonCfgMemoryLocPort", memLocPortctor);

    QScriptValue memLocRTLctor = engine->newFunction(constructCarbonCfgMemoryLocRTL);
    engine->globalObject().setProperty("CcfgCarbonCfgMemoryLocRTL", memLocRTLctor);

    QScriptValue xtorConnCtor = engine->newFunction(constructCarbonCfgXtorConn);
    engine->globalObject().setProperty("CcfgCarbonCfgXtorConn", xtorConnCtor);

    QScriptValue regFieldCtor = engine->newFunction(constructCarbonCfgRegisterField);
    engine->globalObject().setProperty("CcfgCarbonCfgRegisterField", regFieldCtor);

    QScriptValue regLocConstantCtor = engine->newFunction(constructCarbonCfgRegisterLocConstant);
    engine->globalObject().setProperty("CarbonCfgRegisterLocConstant", regLocConstantCtor);

    QScriptValue regLocUserCtor = engine->newFunction(constructCarbonCfgRegisterLocUser);
    engine->globalObject().setProperty("CcfgCarbonCfgRegisterLocUser", regLocUserCtor);

    QScriptValue regLocRegCtor = engine->newFunction(constructCarbonCfgRegisterLocReg);
    engine->globalObject().setProperty("CcfgCarbonCfgRegisterLocReg", regLocRegCtor);

    QScriptValue regLocArrayCtor = engine->newFunction(constructCarbonCfgRegisterLocArray);
    engine->globalObject().setProperty("CCcfgCarbonCfgRegisterLocArray", regLocArrayCtor);

    QScriptValue ccfgCtor = engine->newFunction(constructCarbonCfg);
    engine->globalObject().setProperty("Ccfg", ccfgCtor);
 }
}

class EnumWrapper : public QScriptClass
{
public:
  EnumWrapper(QScriptEngine* engine, const QMetaObject* mo, QMetaEnum me) : QScriptClass(engine)
  {
    mMetaObject = mo;
    mMetaEnum = me;

    //qDebug() << "Creating enum wrapper for" << me.name();

    QString enumName = me.name();
    engine->globalObject().setProperty(enumName, engine->newObject(this));
  }

  virtual ~EnumWrapper()
  {
  }

  QueryFlags queryProperty(const QScriptValue &,
                                     const QScriptString &name,
                                     QueryFlags , uint *)
  {
    QString ename = name.toString();
    UtString v; v << ename;
    int value = mMetaEnum.keyToValue(v.c_str());

    if (value >= 0)
      return QScriptClass::HandlesReadAccess;
    else
    {
      QString errMsg = QString("Unknown enumeration %1 %2 %3")
        .arg(mMetaEnum.scope())
        .arg(mMetaEnum.name())
        .arg(ename);
      engine()->currentContext()->throwError(QScriptContext::TypeError, errMsg);
      return 0;
    }
  }


  QScriptValue property(const QScriptValue &,
                                  const QScriptString &name, uint)
  {
    QString ename = name.toString();
    UtString v; v << ename;   
    int value = mMetaEnum.keyToValue(v.c_str());
    return QScriptValue(engine(), value);
  }

private:
  QMetaEnum mMetaEnum;
  const QMetaObject* mMetaObject;
};


void CfgScriptingEngine::createEnumerationWrappers(QScriptValue value)
{
  if (value.isValid())
  {
    QScriptValueIterator it(value);
    while (it.hasNext()) 
    {
      it.next();
      QScriptValue sv = it.value();
      if (sv.isValid() && sv.isQMetaObject())
      {
        const QMetaObject* mo = sv.toQMetaObject();
        for (int i=0; i<mo->enumeratorCount(); i++)
        {
          const QMetaEnum eo = mo->enumerator(i);       
          new EnumWrapper(this, mo, eo);
        }
      }
    }
  }
}

// Carbon DB

CfgScriptingCarbonDB::CfgScriptingCarbonDB(QObject* parent, CarbonCfg* ccfg, CarbonDB* db) : QObject(parent)
{
  mDB = db;
  mCcfg = ccfg;
}

CfgScriptingCarbonDB* CarbonCfg::GetDatabase()
{
 return mScriptingDB;
}


static QScriptValue constructCarbonDB(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 2)
  {
    CarbonCfg* ccfg = qscriptvalue_cast<CarbonCfg*>(ctx->argument(0));    
    QString databasePath = qscriptvalue_cast<QString>(ctx->argument(1));


    UtString dbFilePath, err;
    UtString dbPath; dbPath << databasePath;

    if (!OSExpandFilename(&dbFilePath, dbPath.c_str(), &err))
    {
      QString msg = err.c_str();
      QString errMsg = QString("Unable to expand databaseFilePath file: %1, err: %2").arg(databasePath).arg(msg);
      return ctx->throwError(QScriptContext::URIError, errMsg);
    }
    else
    {
      QFileInfo fi(dbFilePath.c_str());
      if (fi.exists())
      {
        CarbonDB* db = carbonDBOpenFile(dbFilePath.c_str());

        CfgScriptingCarbonDB* scriptingDB = new CfgScriptingCarbonDB(0, ccfg, db);

        return eng->toScriptValue(scriptingDB);
      }
      else
      {
        QString msg = QString("Error: No such file: %1").arg(dbFilePath.c_str());
        return ctx->throwError(QScriptContext::URIError, msg);
      }
    }
  }

  return ctx->throwError(QScriptContext::ReferenceError, "Constructor requires CarbonCfg and database path arguments");
}

void CfgScriptingCarbonDB::registerTypes(QScriptEngine* engine)
{
  if (engine)
  {
    QScriptValue global = engine->globalObject();    

    // Constructors
    QScriptValue dbGenCtor = engine->newFunction(constructCarbonDB);
    global.setProperty("CarbonDatabase", dbGenCtor);

    qScriptRegisterQObjectMetaType<CfgScriptingCarbonDB*>(engine);

    ProtoCarbonDBNodeIter* dbNodeIterProto = new ProtoCarbonDBNodeIter();
    engine->setDefaultPrototype(qMetaTypeId<CarbonDBNodeIter*>(),
      engine->newQObject(dbNodeIterProto));

    ProtoCarbonDBNode* dbNodeProto = new ProtoCarbonDBNode();
    engine->setDefaultPrototype(qMetaTypeId<CarbonDBNode*>(),
      engine->newQObject(dbNodeProto));

  }
}

CarbonCfgRegisterField* CarbonCfgRegister::AddRegisterField(qint32 high, quint32 low, CcfgEnum::CarbonCfgRegAccessType accessType)
{
  CarbonCfgRegAccessType access = static_cast<CarbonCfgRegAccessType>(accessType);
  CarbonCfgRegisterField* field = new CarbonCfgRegisterField(high, low, access);
  addField(field);
  return field;
}

CarbonCfgRegisterLocReg* CarbonCfgRegisterField::AddLocReg(const QString& path)
{
  UtString rtlPath;
  rtlPath << path;
  CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(rtlPath.c_str());
  putLoc(loc);
  return loc;
}

CarbonCfgRegisterLocReg* CarbonCfgRegisterField::AddLocReg(const QString& path, quint32 left, quint32 right)
{
  UtString rtlPath;
  rtlPath << path;

  CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(rtlPath.c_str(), left, right);  
  putLoc(loc);

  return loc;
}

CarbonCfgRegisterLocArray* CarbonCfgRegisterField::AddLocArray(const QString& path, quint32 index)
{
  UtString rtlPath;
  rtlPath << path;
  CarbonCfgRegisterLocArray* loc = new CarbonCfgRegisterLocArray(rtlPath.c_str(), index);
  putLoc(loc);
  return loc;
}

CarbonCfgRegisterLocArray* CarbonCfgRegisterField::AddLocArray(const QString& path, quint32 index, quint32 left, quint32 right)
{
  UtString rtlPath;
  rtlPath << path;

  CarbonCfgRegisterLocArray* loc = new CarbonCfgRegisterLocArray(rtlPath.c_str(), index, left, right);  
  putLoc(loc);

  return loc;
}

const char* carbonCfgMemoryGetSystemAddressESLPortName(CarbonCfgMemoryID mem,unsigned int index)
{
  return mem->getSystemAddressESLPortName(index);
}

long long carbonCfgMemoryGetSystemAddressESLPortBaseAddress(CarbonCfgMemoryID mem,unsigned int index)
{
  return mem->getSystemAddressESLPortBaseAddress(index);
}

void carbonCfgMemorySetSystemAddressESLPortName(CarbonCfgMemoryID mem, unsigned int index, const char* eslPortName)
{
  mem->putSystemAddressESLPortName(index, eslPortName);
}
void  carbonCfgMemoryAddSystemAddressESLPort(CarbonCfgMemoryID mem, const char* eslPortName, long long baseAddress)
{
  mem->addSystemAddressESLPort(eslPortName, baseAddress);
}
void carbonCfgMemoryRemoveAllSystemAddressESLPorts(CarbonCfgMemoryID mem)
{
  mem->removeAllSystemAddressESLPorts();
}
void carbonCfgMemoryRemoveSystemAddressESLPort(CarbonCfgMemoryID mem, unsigned int index)
{
  mem->removeSystemAddressESLPort(index);
}
void carbonCfgMemorySetSystemAddressESLPortBaseAddress(CarbonCfgMemoryID mem, unsigned int index, long long baseAddress)
{
  mem->putSystemAddressESLPortBaseAddress(index, baseAddress);
}
unsigned int carbonCfgMemoryNumSystemAddressESLPorts(CarbonCfgMemoryID mem)
{
  return mem->numSystemAddressESLPorts();
}

