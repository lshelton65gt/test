//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "CfgXmlParserPSD.h"
#include "pv/PV.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"
#include "CfgXmlWriter.h"
//#include "compiler_driver/CarbonContext.h"


CfgXmlParserPSD::CfgXmlParserPSD(PVContext* pvc)
  : mPVContext(pvc)
{
}

CfgXmlParserPSD::~CfgXmlParserPSD()
{
}

bool CfgXmlParserPSD::parseStream(UtIStream &s)
{
  parseSetup();

  // Fill a buffer with the XML text, then create a
  // document from it.
  xmlBuffer *buf = xmlBufferCreate();
  bool done = false;
  UtString line;
  while (!done) {
    s.getline(&line);
    xmlBufferWriteChar(buf, line.c_str());
    // check for end of XML or EOF
    if (line.empty() || (line.find("</PSDinfo>") != UtString::npos))
      done = true;
  }
  
  xmlDoc *doc = xmlParseDoc(xmlBufferContent(buf));

  if (doc == 0)
    reportError("Failed to parse PSD XML");
  else {
    // check that the root element is what we expect
    // TODO: won't need to do this if validate using DTD/XSD
    xmlNode *rootNode = xmlDocGetRootElement(doc);
    if (!isElement(rootNode, "PSDinfo"))
      reportError("Root element of PSD XML must be 'PSDinfo'");
    else
      // walk all children and parse them
      for (xmlNode *node = rootNode->children; node != NULL; node = node->next)
        if (isElement(node, "Transaction")) // 
          parsePSDTransaction(node);
  }

  xmlFreeDoc(doc);
  xmlBufferFree(buf);
  parseCleanup();

  return !getError();
}

void CfgXmlParserPSD::parsePSDTransaction(xmlNode *node) {
  // get group name
  CfgXmlAttr nameAttr(node, "name");
  if (nameAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<Transaction> element with no name attribute is not allowed.");
    return;
  }

  PVTransactionType type = ePVRead;
  CfgXmlAttr typeAttr(node, "type");
  if (typeAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<Transaction> element with no type attribute is not allowed.");
  }
  else if (strcmp(typeAttr.value(), "write") == 0) {
    type = ePVWrite;
  }
  else if (strcmp(typeAttr.value(), "read") != 0) {
    // TODO: use DTD/XSD
    reportError("<Transaction> element with invalid type attribute: legal values are 'read' and 'write'.");
    return;
  }

  CfgXmlAttr addrAttr(node, "address");
  unsigned int a1 = 0, a2 = 0;
  if (addrAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<Transaction> element with no address attribute is not allowed.");
    return;
  }
  else if (sscanf(addrAttr.value(), "%u:%u", &a1, &a2) != 2) {
    reportError("<Transaction> element with invalid address attribute: expecting start_addr:end_addr.");
    return;
  }

  CfgXmlAttr clkAttr(node, "clock");
  if (clkAttr.value() == NULL) {
    // TODO: use DTD/XSD
    reportError("<Transaction> element with no clock attribute is not allowed.");
    return;
  }
    
  PVTransaction *trans = mPVContext->addTransaction(nameAttr.value(),
                                                    clkAttr.value(),
                                                    type, a1, a2);

  // walk all children and parse them
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "Sequence")) {
      parsePSDSequence(child, trans);
    }
    else if (isElement(child, "data")) {
      // This PV event represents a direct data transfer with the model,
      // without the need to run carbonSchedule.  This would typically
      // occur on a read
      SInt32 offset;
      PVNet* net = parseDataField(child, &offset);
      if (net != NULL) {
        if (trans->getType() == ePVRead) {
          trans->bindOutputData(net, offset);
        }
        else {
          trans->bindInputData(net, offset);
        }
      }
    }
  }
} // void CfgXmlParserPSD::parsePSDTransaction

void CfgXmlParserPSD::parsePSDSequence(xmlNode *node, PVTransaction *transaction) {
  // it's ok for the name to be NULL.  we can invent a name for a sequence.
  CfgXmlAttr nameAttr(node, "name");
  PVSequence *seq = transaction->addSequence(nameAttr.value());

  // walk Sequence children looking for phases
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "Phase")) {
      parsePSDPhase(child, seq);
    }
  }
}



void CfgXmlParserPSD::parsePSDPhase(xmlNode *node, PVSequence *sequence) {
  // it's OK for the name to be defaulted to NULL
  CfgXmlAttr nameAttr(node, "name");
  PVPhase *phase =  sequence->addPhase(nameAttr.value());

  // walk all children looking for signals and values
  for (xmlNode *child = node->children; child != NULL; child = child->next) {
    if (isElement(child, "sigval")) {
      CfgXmlAttr signalAttr(child, "signal");
      CfgXmlAttr valueAttr(child, "value");
      if ((signalAttr.value() == NULL) || (valueAttr.value() == NULL)) {
        // TODO: use DTD/XSD
        reportError("sigval element with either no signal or value attribute is not allowed.");
      } else {
        PVNet* net = mPVContext->getPVNet(signalAttr.value());
        if (net == NULL) {
          reportError("Cannot find net for phase tie.");
        }
        else {
          phase->addTie(net, valueAttr.value());
        }
      }
    }
    else if (isElement(child, "data")) {
      SInt32 offset;
      PVNet* net = parseDataField(child, &offset);
      if (net != NULL) {
        if (sequence->getTransaction()->getType() == ePVRead) {
          phase->bindOutputData(net, offset);
        }
        else {
          phase->bindInputData(net, offset);
        }
      }
    }
  }
  // Error checking
} // void CfgXmlParserPSD::parsePSDPhase

PVNet* CfgXmlParserPSD::parseDataField(xmlNode* node, SInt32* offset) {
  CfgXmlAttr signalAttr(node, "signal");
  CfgXmlAttr offsetAttr(node, "offset");
  PVNet* net = NULL;
  if ((signalAttr.value() == NULL) || (offsetAttr.value() == NULL)) {
    // TODO: use DTD/XSD
    reportError("data element with either no signal or offset attribute is not allowed.");
  } else {
    net = mPVContext->getPVNet(signalAttr.value());
    if (net == NULL) {
      reportError("Cannot find net for phase data.");
    }
    else if (!StringUtil::parseNumber(offsetAttr.value(), offset)) {
      reportError("Cannot parse numeric offset value.");
      net = NULL;
    }
  }
  return net;
}


// I have not re-implemented XML write following the conversion from
// PSD data structures to PV
#define XML_WRITE_NYI 0

#if XML_WRITE_NYI
void CarbonCfgPSDPhase::write(CfgXmlWriter &xml) {
  xml.addElement("Phase");
  xml.addAttribute("name", getName());

  for (UInt32 j = 0; j < mSigValVec.size(); j++) {
    xml.addElement("sigval");
    pairSigVal* sv = mSigValVec[j];
    xml.addAttribute("signal", (sv->first).c_str());
    xml.addAttribute("signal", (sv->second).c_str());
    xml.closeElement();
  }
  xml.closeElement();
}


void CarbonCfgPSDSequence::write(CfgXmlWriter &xml) {
  xml.addElement("Sequence");
  xml.addAttribute("name", getName());
  for (UInt32 j = 0; j < numPhases(); j++) {
    getPSDPhase(j)->write(xml);
  }
  xml.closeElement();
}

void CarbonCfgPSDTransaction::write(CfgXmlWriter &xml) {
  xml.addElement("Transaction");
  xml.addAttribute("name", getName());
  
  xml.addElement("Clock");
  xml.addAttribute("name", getClock());

  xml.closeElement();
  for (UInt32 j = 0; j < numSequences(); j++) {
    getPSDSequence(j)->write(xml);
  }
  xml.closeElement();
}
#endif
