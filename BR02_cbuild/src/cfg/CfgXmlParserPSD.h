//! -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// \file XML parser for PSD (Transaction ID Specification)

#ifndef _CfgXmlParserPSD_h_
#define _CfgXmlParserPSD_h_

#include "CfgXmlParser.h"

class PVContext;
class UtIStream;
class PVTransaction;
class PVSequence;
class PVNet;

//! CfgXmlParserPSD class
class CfgXmlParserPSD : public CfgXmlParser
{
public:
  CARBONMEM_OVERRIDES

  //! constructor
  CfgXmlParserPSD(PVContext*);

  //! destructor
  ~CfgXmlParserPSD();


  //! parse XML register format from a stream
  bool parseStream(UtIStream &s);

private:
  void parsePSDTransaction(xmlNode *node);
  void parsePSDSequence(xmlNode *node, PVTransaction *transaction);
  void parsePSDPhase(xmlNode *node, PVSequence *sequence);
  PVNet* parseDataField(xmlNode* node, SInt32* offset);

  PVContext* mPVContext;
};

#endif
