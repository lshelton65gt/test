// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file 
  Classes related to file operations used within VHDL/verilog simulations.
  Such as for FILE_OPEN, WRITE. Includes support for multi channel descriptors.
*/
#include "util/ShellMsgContext.h"
#include "util/UtIOStream.h"
#include "util/Loop.h"
#include "util/UtStreamSupport.h"
#include "hdl/HdlOStream.h"

static const int MAXBYTES = 65536 * 2;

HdlOStream::HdlOStream(HDLFileSystem* fileSystem, MsgContext* msgContext) :
  mHDLFileSystem(fileSystem),
  mMsgContext(msgContext)
{

  // reserve stdout, stderr, stdin file descriptors  in this verilog file system.
  mHDLFileSystem->reserveFileDescriptor(eMCDstdin,  &(UtIO::cin()), NULL           );
  mHDLFileSystem->reserveFileDescriptor(eMCDstdout, NULL,           &(UtIO::cout()));
  mHDLFileSystem->reserveFileDescriptor(eFDstdin,   &(UtIO::cin()), NULL);
  mHDLFileSystem->reserveFileDescriptor(eFDstdout,  NULL,           &(UtIO::cout()));
  mHDLFileSystem->reserveFileDescriptor(eFDstderr,  NULL,           &(UtIO::cerr()));
  mTargetFileDescriptor = 0;    // a value of 0 is eMCDstdin which
                                // cannot be used for output so
                                // mTargetFileDescriptor and
                                // mTargetFileDescriptors are now in
                                // sync (empty)
}

HdlOStream::~HdlOStream()
{
  close( );                      // close all open files
  // unreserve stdout, stderr, stdin file descriptors  in this verilog file system.
  mHDLFileSystem->unReserveFileDescriptor(eMCDstdin);
  mHDLFileSystem->unReserveFileDescriptor(eMCDstdout);
  mHDLFileSystem->unReserveFileDescriptor(eFDstdin);
  mHDLFileSystem->unReserveFileDescriptor(eFDstdout);
  mHDLFileSystem->unReserveFileDescriptor(eFDstderr);
}
 
bool HdlOStream::is_open() const
{
  return ( mHDLFileSystem->is_open() );
}

// This closes only the files specified by fdToClose 
// It will not close the reserved files
bool HdlOStream::close(UInt32 fdToClose)
{

  bool retStatus = true;        // assume all is well
  bool aFileWasClosed = false;

  if ( not mHDLFileSystem->isReservedFD( fdToClose ) ) {
      retStatus &= mHDLFileSystem->close(fdToClose);
      aFileWasClosed = true;
  }
  if ( aFileWasClosed ) {
    // force next reference to setup the mTargetFileDescriptors
    mTargetFileDescriptor = 0;    
    mTargetFileDescriptors.clear();
  }
  return retStatus;
}



// This flushes only the files specified by fdToFlush 
bool HdlOStream::flush(UInt32 fdToFlush)
{
  bool retStatus = true;        // assume all is well

  // only one file involved, flush it
  if ( not mHDLFileSystem->isReservedFD( fdToFlush ) ) {
    retStatus &= mHDLFileSystem->flush(fdToFlush);
  }
  return retStatus;
}

bool HdlOStream::write(const char* buf, UInt32 len)
{
  typedef CLoop<TargetFileDescriptors> TargetFileDescriptorsCLoop;

  bool returnForAll = true;
  // writes buf to all files that are currently marked for writing in
  // the HdlOStream
  for (TargetFileDescriptorsCLoop i(mTargetFileDescriptors); not i.atEnd(); ++i)
  {
    UtOStream * stream = *i;

    bool ret = stream->write(buf, len);
    if (! ret)
    {
      const char * filename = stream->getFilename();
      if (filename == NULL) filename = "stdout||stderr";
      getMsgContext()->SHLFailedToWrite(filename, stream->getErrmsg());
    }
    returnForAll &= ret;
  }

  return returnForAll;
}

//! Write a LINE variable \a str_stream contents to Files.
void HdlOStream::writeLine( UtIOStringStream* str_stream)
{
  str_stream->insert("\n",1);
  char buf[MAXBYTES];
  UInt32 len = str_stream->read(buf, MAXBYTES);

  write((const char*)buf, len);

  str_stream->reset();
  // After writing The LINE variable is to emptied.
}

void HdlOStream::putTargetFileDescriptor ( UInt32 descriptor )
{

  if ( descriptor == mTargetFileDescriptor )
  {
    // nothing to do, mTargetFileDescriptors is already setup to use
    // this descriptor.
    return;
  }

  mTargetFileDescriptors.clear();

  // only one file involved add the file associated with this file
  // descriptor to mTargetFileDescriptors
  UtOStream * out_stream = NULL;

  bool is_defined = mHDLFileSystem->getHdlFileStream(&out_stream, descriptor);
  if ( is_defined && out_stream ){
    mTargetFileDescriptors.push_back(out_stream);
  }

  mTargetFileDescriptor = descriptor; 
}

UInt32 HdlOStream::HdlOFileOpen (UInt2* status, 
                                 const char* filename,
                                 const char* mode)
{
  // rjc-todo really should check that mode is any valid form of
  // write, but for now we only support write and append mode
  if ( not ((0 == strcmp(mode, "w")) || (0 == strcmp(mode, "a" ))) )
  {
    *status = 0x02; // MODE_ERROR status in vhdl
    getMsgContext()->SHLUnableToOpenFileWithMode(filename, mode);
    return eMCDstdin;          // an invalid value for writing
  }   

  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(status, filename, mode,
                                                      true/*write_mode*/,
                                                      false);

  return (this_descriptor);

}

UInt32 HdlOStream::HdlOFileOpen (const char* filename, const char* mode)
{
  // rjc-todo really should check that mode is any valid form of write
  if ( not ((0 == strcmp(mode, "w")) || (0 == strcmp(mode, "a" ))) )
  {
    getMsgContext()->SHLUnableToOpenFileWithMode(filename, mode);
    return eMCDstdin;          // an invalid value for writing
  }   

  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(filename, mode,
                                                      true/*write_mode*/,
                                                      false);

  return (this_descriptor);

}

UInt32 HdlOStream::HdlOFileOpen (const char* filename)
{
  char mode [4] = "w";

  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(filename, mode,
                                                      true/*write_mode*/,
                                                      true);

  return (this_descriptor);
}


