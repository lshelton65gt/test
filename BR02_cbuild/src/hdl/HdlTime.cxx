// -*-C++-*-
/******************************************************************************
 Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file 
  Methods (namespace) for HdlTime, releated to handling timescale, timeunit, timeprecision specification

*/
#include <ctype.h>
#include <cstring>
#include "hdl/HdlTime.h"
#include "util/CarbonAssert.h"
#include "util/UtString.h"
#include "util/SourceLocator.h"
#include "util/ShellMsgContext.h"

// SV 1800-2012 standard allows float point numbers as timeString.

// Carbon limits support - number should contain one and only one '1' digit symbol in all parts of the number(e.g. 0.1, 1, 100 etc.), otherwise
// 1) If number doesn't contain any '1' digit symbol inside - than that is unsupported by carbon case and error will be reported (e.g. 20ns). TODO add a testcase to show this (0.2ns)
// 2) If number has more than one '1' digit symbol or if contains any other than '0' or '1' digit symbols ('2'-'9') alongside single '1' digit and any number of '0' digit symbols
//    than warning will be issued and the number converted to as if only most '1' exist and all other numbers ('2'-'9') are '0' (warning 0.112ns rounded to 100ps)
// 3) In case of float point number (e.g. 0.1ns) after rounding (per need), just convert it to the correct index into the VerilogTimeUnits array. 0.1ns will get just same index as 100ps).


const char * HdlTime::VerilogTimeUnits[18] =
    {
      "1fs", "10fs", "100fs",
      "1ps", "10ps", "100ps",
      "1ns", "10ns", "100ns",
      "1us", "10us", "100us",
      "1ms", "10ms", "100ms",
      "1s",  "10s",  "100s"
    };

bool hasSingleChar(const UtString& str, char ch)
{
    size_t f = str.find_first_of(ch);
    size_t l = str.find_last_of(ch);
    return (f != UtString::npos) && (f == l);
}

UtString normalizeTimeString(const UtString& timeStr, int* offset)
{
    size_t u = timeStr.find_first_not_of("0123456789.");
    UtString unit(timeStr, u, timeStr.size() - u);
    size_t d = timeStr.find_first_of('.');
    size_t o = timeStr.find_first_of('1');
    UtString normStr;
    if (o < d) {
        *offset = 0; // no offset
        normStr = UtString(timeStr, 0, d - 1); 
    } else {
        *offset = o - d;
        normStr = "1";
    }
    normStr << unit.c_str();
    return normStr;
}

UtString roundTimeString(const UtString& timeStr)
{
    size_t f = timeStr.find_first_of('1');
    UtString roundedStr(timeStr, 0, f); // write as it is 
    for (size_t i = f + 1; i < timeStr.size(); ++i) {
        if (('0' < timeStr[i]) && ('9' > timeStr[i])) {
            // ignore all numbers
            roundedStr << '0';
        } else {
            // just write whatever seen
            roundedStr << timeStr[i];
        }
    }
    return roundedStr;
}

SInt8 HdlTime::parseTimeString(const char *timeString, unsigned* status)
{
    // We are unable to pass SourceLocator and MsgContext (it seems there is compilation order problem)
    // I don't wanted big changes in the flows order, so added message encodings
    // 0 - NOMESSAGE = no report
    // 1 - WARNING = only warning will be reported
    // 2 - ERROR =  only error will be reported
    // 3 - WARNING_AND_ERROR = both warning and error will be reported (constructing by sum = ERROR + WARNING)
    // This isn't quite nice we should be careful in future support and messaging
    *status = NOMESSAGE;
    if ( timeString ) {
        UtString timeStr(timeString);
        bool hasSingleOne = hasSingleChar(timeStr, '1');
        bool hasOne = (timeStr.find_first_of('1') != UtString::npos);
        bool hasNoOtherThanOneAndZeroNumber = (timeStr.find_first_of("23456789") == UtString::npos);
        bool needRounding = ! (hasSingleOne && hasNoOtherThanOneAndZeroNumber);
        bool needNormalization = hasSingleChar(timeStr, '.'); 
        int offset = 0;
        if (!hasOne) { 
            // unsupported case don't continue
            *status += HdlTime::ERROR ;
            return 0;
        }
        if (needRounding) {
            // timeStr contain other than '0' and '1' digits or has more than one '1' digit - need rounding 
            timeStr = roundTimeString(timeStr); 
            *status += WARNING ;
        }
        if (needNormalization) {
            // need convert to table index units
            timeStr = normalizeTimeString(timeStr, &offset);
        }
        for ( int index = 0; index < 18; ++index )
        {
            if ( !strcmp( timeStr.c_str(), VerilogTimeUnits[index] ))
            {
                // Valid values are -15 to 2, which corresponds to the power of
                // 10 of the time unit.  This adjusts index appropriately.
                return (index - 15 - offset);
            }
        }
        // Getting here means that timeString was invalid, This should never happen if the parser does the correct job.
        // Or there is some unsupported TimeString by Carbon (see comment in top for more details)
        *status += ERROR;
    }
    // if timeString is NULL then then we assume there is no timescale, which is maped to a "1s" timescale as the default;
    return 0;
}
