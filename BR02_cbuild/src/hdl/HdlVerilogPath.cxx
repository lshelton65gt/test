// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/UtIOStream.h"
#include "hdl/HdlVerilogPath.h"
#include "util/AtomicCache.h"
#include "util/HierName.h"
#include "util/CarbonAssert.h"
#include "util/OSWrapper.h"
#include "hdl/HdlId.h"
#include <cctype>
#include "util/StringAtom.h"
#include "util/UtConv.h"
#include "util/UtStringUtil.h"

/*!
  \file
  Implementation of HdlVerilogPath class object
*/

static const char scSimpleSpace = ' ';
static const char scPathDelim = '.';
static const char scEscapeChar = '\\';
static const char scDollarChar = '$';
static const char scUnderscore = '_';
static const char scLeftBracket = '[';
static const char scRightBracket = ']';
static const char scColon = ':';
static const char scNewline = '\n';
static const char scMinus = '-';

// wildcards
static const char scAsterick = '*';
static const char scQuestion = '?';

static bool sLegalFirst[256];
static bool sLegalIdToks[256];
static bool sLegalVectorToks[256];
static bool sLegalWildcards[256];
static bool sIsLegalInit = false;

HdlVerilogPath::HdlVerilogPath() :
  mAllowWildcard(false)
{
  if (! sIsLegalInit)
  {
    unsigned char ic;
    for (unsigned int i = 0; i < 256; ++i)
    {
      ic = static_cast<unsigned char>(i);
      sLegalIdToks[i] = (isalnum(ic) || (ic == scDollarChar) || (ic == scUnderscore));
      sLegalFirst[i] = (isalpha(ic) || (ic == scUnderscore) || (ic == scEscapeChar));
      sLegalVectorToks[i] = (isdigit(ic) || (ic == scColon) || (ic == scLeftBracket) || (ic == scRightBracket) || (ic == scMinus));
      sLegalWildcards[i] = ((ic == scAsterick) || (ic == scQuestion));
    }
    sIsLegalInit = true;
  }
}

HdlVerilogPath::~HdlVerilogPath()
{
}

void HdlVerilogPath::putAllowWildcard(bool allow) {
  mAllowWildcard = allow;
}

HdlHierPath::Status 
HdlVerilogPath::makeLegalId(UtString* id, HdlId* info, bool keepBrackets) const
{
  Status stat = eIllegal;
  const char* pathBegin = id->c_str();
  const char* pathEnd = pathBegin + id->size();
  UtString buffer;
  stat = makeLegalId(&pathBegin, pathEnd, &buffer, info, keepBrackets);
  if (stat != eIllegal)
    *id = buffer;
  
  return stat;
}

HdlHierPath::Status 
HdlVerilogPath::makeLegalId(const char** pathBegin, const char* pathEnd, UtString* buffer, HdlId* info, bool keepBrackets) const
{
  Status stat = eIllegal;
  INFO_ASSERT(info, "Must pass an HdlId into makeLegalId");
  
  // skip spaces
  *pathBegin = StringUtil::skip(*pathBegin);
  const char* s = *pathBegin; 

  if (*pathBegin != pathEnd)
  {
    stat = parseToken(&s, pathEnd, buffer, info, keepBrackets);
    if ((stat != eLegal) || (! isspace(*s) && (*s != '\0') && (s != pathEnd)))
    {
      int subtractFactor = 0;
      const char* p = "";
      const char* pe = p;
      
      if ((info->getType() == HdlId::eInvalid) ||
          (! isspace(*s) && (s != pathEnd)) || 
          (stat == eIllegal))
      {
        UtString escapedTokenBuf;
        if (**pathBegin != scEscapeChar)
        {
          escapedTokenBuf << scEscapeChar;
          ++subtractFactor;
        }
        escapedTokenBuf.append(*pathBegin,  pathEnd - *pathBegin);
        if (! isspace(*pathEnd))
        {
          ++subtractFactor;
          escapedTokenBuf << scSimpleSpace;
        }
        
        // try again. fail if not legal
        p = escapedTokenBuf.c_str();
        pe = p + escapedTokenBuf.size();
        stat = parseToken(&p, pe, buffer, info, keepBrackets);
      }
      
      if ((stat == eLegal) || (stat == eEndPath))
      {
        stat = eLegal;
        int numCharsMoved = pe - p;
        *pathBegin += (numCharsMoved - subtractFactor);
      }
    }
    else
    {
      *pathBegin = s;
      stat = eLegal;
    }
  }
  
  return stat;
}

HdlHierPath::Status 
HdlVerilogPath::decompPath(const char* path, UtStringArray* ids, HdlId* info) const
{
  ListStrType listStr(ids);
  return decompPathToList(listStr, path, info);
}

HdlHierPath::Status 
HdlVerilogPath::decompPathAtom(const char* path, StrAtomVec* ids, 
                            const AtomicCache* strCache,
                            HdlId* info) const
{
  ListAtomType listAtom(ids, strCache);
  return decompPathToList(listAtom, path, info);
}

HdlHierPath::Status HdlVerilogPath::parseToken(const char** path, UtString* buffer, HdlId* info, bool keepBrackets) const
{
  Status status = eIllegal;
  if (VERIFY(path) && 
      VERIFY(*path))
  {
    // find end char
    const char* endPtr = *path + strlen(*path);

    status = parseToken(path, endPtr, buffer, info, keepBrackets);
  }
  return status;
}

HdlHierPath::Status 
HdlVerilogPath::parseToken(UtString* pathStr, UtString* buffer, HdlId* info, 
                           bool keepBrackets) const
{
  Status status = eIllegal;
  if (VERIFY(pathStr))
  {
    const char* p = pathStr->c_str();
    status = parseToken(&p, buffer, info, keepBrackets);
    if (status != eIllegal)
      pathStr->erase(0, p - pathStr->c_str());
  }
  return status;
}

HdlHierPath::Status 
HdlVerilogPath::parseToken(const char** pathBegin, const char* pathEnd, UtString* buffer, HdlId* info, bool keepBrackets) const
{
  Status status = eIllegal;
  if (VERIFY(pathBegin) && 
      VERIFY(*pathBegin) && 
      VERIFY(pathEnd) &&
      VERIFY(buffer))
  {
    
    *pathBegin = StringUtil::skip(*pathBegin);
    
    buffer->erase();
    
    if (substrToken(pathBegin, pathEnd, info, &status, buffer))
    {
      if (keepBrackets && info->hasVectorSemantics())
        addInfo(info, buffer);
    }
  }
  return status;
}

HdlHierPath::Status 
HdlVerilogPath::parseName(const char** path, UtStringArray* ids, HdlId* info) const
{
  Status stat = eLegal;
  UtString buf;
  info->setType(HdlId::eScalar);
  while((stat = parseToken(path, &buf, info, false)) == eLegal)
  {
    /*
      This is to deal with stupid verilog construct of
      foo[1].bar
    */
    /*
      We have to be able to break out of this if we have a trailing
      delimiter, so freakin' tests can work. Some tests have makefiles
      where an argument to cwavetestbench is $(PATH).$(INSTANCE),  and
      instance could be empty.
    */
    if ((*path == NULL) || (**path == '\0'))
    {
      stat = eEndPath;
      break;
    }

    if (info->hasVectorSemantics())
      addInfo(info, &buf);
    
    ids->push_back(buf);
  }
  
  // Get the last scope/net
  if ((stat == eEndPath) && ! buf.empty()) {
    // Add the bitselect/array index to the token if requested.  Note
    // that this is not done for partselects.
    if (((info->getType() == HdlId::eVectBit) ||
         (info->getType() == HdlId::eArrayIndex)) &&
        info->getAllowIndices()) {
      addInfo(info, &buf);
    }
    ids->push_back(buf);
  }
  
  return stat;
}

const char* HdlVerilogPath::compPath(const UtStringArray& ids, UtString* buffer,
                                  const HdlId* info) const
{
  ListStrType listStr(ids);
  return compPathFromList(listStr, buffer, info);
}

const char* HdlVerilogPath::compPathAtom(const StrAtomVec& ids, UtString* buffer,
                                      const HdlId* info) const
{
  ListAtomType listAtom(ids);
  return compPathFromList(listAtom, buffer, info);
}

const char* HdlVerilogPath::compPathFromList(ListType& theList, UtString* buffer, 
                                          const HdlId* info) const
{
  INFO_ASSERT(buffer, "NULL buffer passed to compPathFromList.");
  buffer->erase();
  const char* p = theList.beginIter();
  while (p)
  {
    composeScalar(buffer, p);
    p = theList.next();
    if (p)
      *buffer += scPathDelim;
  }

  if (info)
    addInfo(info, buffer);

  return buffer->c_str();
}

const char* HdlVerilogPath::compPathHier(const HierName* name, UtString* buffer, 
                                      const HdlId* info) const
{
  if (compPathFromHierName(name, buffer))
  {
    if (info != NULL)
      addInfo(info, buffer);
  }
  
  return buffer->c_str();
}

const char* HdlVerilogPath::compPathHierAppend(const HierName* name, StringAtom* tail, UtString* buffer, 
                                            const HdlId* info) const
{
  const char* ret = compPathFromHierName(name, buffer);
  // Do we have a path and something to append? If so, delimit
  if (ret && tail)
    *buffer += scPathDelim;
  
  // If we have a tail add it to the buffer
  if (tail)
    composeScalar(buffer, tail->str());

  // If the name had a null stringatom (impossible?), but not a null
  // tail then reset ret
  if (!ret && (! buffer->empty()))
    ret = buffer->c_str();

  // If an info was passed down and a UtString was formed, add the
  // remaining part of the name.
  if (ret && info)
    addInfo(info, buffer);
  
  return buffer->c_str();
}

const char* HdlVerilogPath::compPathAppend(UtString* buffer, const char* tail,
                                        const HdlId* info) const
{
  INFO_ASSERT(buffer, "NULL buffer passed to compPathAppend.");
  
  // Do we have a path and something to append? If so, delimit
  if (tail)
  {
    if (! buffer->empty())
      *buffer += scPathDelim;
    composeScalar(buffer, tail);    
  }
  
  // If an info was passed down and a UtString was formed, add the
  // remaining part of the name.
  if (! buffer->empty() && info)
    addInfo(info, buffer);
  
  return buffer->c_str();
}

const char* HdlVerilogPath::compPathFromHierName(const HierName* name, UtString* buffer) const
{
  INFO_ASSERT(buffer, "NULL buffer passed to compPathFromHierName.");
  const char* atomStr = NULL;
  if (name)
    atomStr = name->str();
  if (atomStr)
  {
    const HierName* parent = name->getParentName();
    // safe guard against circular relationships
    if (parent && (parent != name)) 
    {
      if (compPathFromHierName(parent, buffer))
        *buffer += scPathDelim;
    }
    else 
      // at top, clear the buffer
      buffer->erase();
    
    composeScalar(buffer, atomStr);
    return buffer->c_str();
  }
  // This is not needed, but safe
  buffer->erase();
  return NULL;
}

void HdlVerilogPath::addInfo(const HdlId* info, UtString* buffer) const
{
  // Use a switch to warn against possible enum additions
  switch(info->getType())
  {
  case HdlId::eVectBitRange:
    (*buffer) << scLeftBracket << info->getVectMSB() << scColon
              << info->getVectLSB() << scRightBracket;
    break;
  case HdlId::eVectBit:
    (*buffer) << scLeftBracket << info->getVectIndex() << scRightBracket;
    break;
  case HdlId::eArrayIndex:
    for (UInt32 i = 0; i < info->getNumDims(); ++i) {
      (*buffer) << scLeftBracket << info->getDim(i) << scRightBracket;
    }
    break;
  case HdlId::eInvalid:
    // Your funeral
    INFO_ASSERT(info->getType() != HdlId::eInvalid, "Invalid HdlId type.");
    break;
  case HdlId::eScalar:
    // nothing to do here
    break;
  }
}

void HdlVerilogPath::composeScalar(UtString* out, const char* in) const
{
  *out += in;
  // in *could* be zero sized
  unsigned int inSize = strlen(in);
  if (inSize > 0)
  {
    if (in[0] == scEscapeChar) {
      if (! isspace(in[inSize-1])) {
        // we may have wildcards after the escaped identifier
        if (mAllowWildcard) 
        {
          // this is a stupid, but easy way to do check
          UtString tmp(in);
          if (tmp.find_first_of("*?") == UtString::npos)
            *out += scSimpleSpace;     
        }
        else
          *out += scSimpleSpace;
      }
    }
  }
}

HdlHierPath::Status 
HdlVerilogPath::decompPathToList(ListType& theList, const char* path, HdlId* info) const
{
  if (! path)
    return eIllegal;
  
  Status stat = eLegal;
  // let's make this easy. use strings
  UtString pathStr = path;
  const char* pathCStr = pathStr.c_str();
  UtStringArray vecIds;
  stat = parseName(&pathCStr, &vecIds, info);
  if ((stat != eIllegal) && (info->getType() == HdlId::eInvalid))
    stat = eIllegal;
  
  // interface says it will return eLegal if parsed correctly
  if (stat == eEndPath)
    stat = eLegal;

  if (stat == eLegal)
  {
    for (UtStringArray::UnsortedCLoop p = vecIds.loopCUnsorted();
         ! p.atEnd(); ++p)
    {
      const char* token = *p;
      if (! theList.append(token))
        // Append failed (for StringAtom ListType)
        stat = eNoExist;
    }
  }

  return stat;
}

bool HdlVerilogPath::substrToken(const char** s,
                                 const char* endPtr,
                                 HdlId* info,
                                 Status* stat,
                                 UtString* token) const
{
  // The first character of a verilog identifier cannot be a digit,
  // a $, or at this point a space

  *stat = eLegal;
  
  // assume scalar first
  info->setType(HdlId::eScalar);
  
  const char* beginStr = *s;
  {
    bool atEnd = (*s == endPtr) || (**s == '\0');
    bool islegal = !atEnd &&
      (sLegalFirst[int(**s)] || (mAllowWildcard && sLegalWildcards[int(**s)]));
    if (atEnd || ! islegal)
    {
      info->setType(HdlId::eInvalid);
      *stat = eIllegal;
      if (atEnd)
        *stat = eEndPath;
      return false;
    }
  }

  // Are we at an escaped identifier?
  // Verilog escaped identifies must have a closing space, VHDL extended identifiers terminate with another backslash
  if (**s == scEscapeChar) {
    const char* start_s = *s;
    // Seen start of escaped name, find end of escaped/extended identifier,
    // Be careful here since escaped names in Verilog end in a space, while extended identifiers in VHDL end in another backslash, it is
    // known that spaces in a vhdl extended name are not supported (and have never been supported), so we first look for a space, if one is
    // found then it is assumed we are looking at a Verilog escaped identifier, if none is found then try again to see if this is a vhdl
    // extendend identifier
    // this code will support the following verilog escaped ID:  "\first_\strange "  where \strange is within the identifier.
    // this code does not support the following vhdl extended identifier: "\first strange\" because it looks too much like a verilog escaped name.
    while ((**s != '\0') && ! isspace(**s) && (*s != endPtr)) {
      ++(*s);
    }
    if (*s == endPtr) {
      // no space found, try again to see if this might be a vhdl extended identifier
      *s = start_s;
      ++(*s);                   // step over the first scEscapeChar
      while ((**s != '\0') && (**s != scEscapeChar) && (*s != endPtr)) {
        ++(*s);
      }
    }      
    if (*s == endPtr) {
      // invalid escaped identifier
      info->setType(HdlId::eInvalid);
      *stat = eIllegal;
      // we fall out at this point and ret is NULL
      return false;
    }
    else {
      ++(*s); // pass space or second backslash
      
      // check for wildcard
      if (mAllowWildcard) {
        while (sLegalWildcards[int(**s)])
          ++(*s);
      }
    }
  } else {
    // Not escaped, we have a 'normal' scope name
    while ((*s != endPtr) && 
           (sLegalIdToks[int(**s)] || (mAllowWildcard && sLegalWildcards[int(**s)])))
      ++(*s);
  }
  
  // do not add delimiter to string
  if ((*s == endPtr) || (**s != scPathDelim))
  {
    *stat = eEndPath;

    const char* idEndPos = *s;

    bool allIsGood = (parseVectorInfo(s, endPtr, info) == eLegal);
    if (allIsGood)
    {
      token->clear();
      token->append(beginStr, idEndPos - beginStr);
    }
    else
      *stat = eIllegal;
    
  }
  else if (**s == scPathDelim)
  {
    // fix up the token
    token->clear();
    token->append(beginStr, *s - beginStr); 
  }
  
  // We may have hit a vectorized scope, so we could be at a
  // delimiter.
  if ((*stat == eLegal) || (*stat == eEndPath))
  {
    if (*s != endPtr)
    {
      if (**s == scPathDelim)
      {
        // We cannot be at the end if we are at a delimiter
        *stat = eLegal;
        ++(*s);  // pass delimiter
      }
    }
  }
  
  return (*stat != eIllegal);
} // bool HdlVerilogPath::substrToken

HdlHierPath::Status
HdlVerilogPath::parseVectorInfo(const char** s, const char* endPtr, HdlId* info) const
{
  info->setType(HdlId::eScalar);
  HdlHierPath::Status stat = eLegal;
  // This may be a multi-dimensional array index, e.g. arr[1][2], so
  // we need to parse each one.
  while (stat == eLegal) {
    stat = parseBrackets(s, endPtr, info);
  }
  // End is good.  It means no brackets remain and there were no
  // errors.
  if (stat == eEndPath) {
    stat = eLegal;
  }
  return stat;
}

HdlHierPath::Status
HdlVerilogPath::parseBrackets(const char** s, const char* endPtr, HdlId* info) const
{
  // move past any whitespace
  while ((*s != endPtr) && isspace(**s))
    ++(*s);
  
  const char* vecBegin = *s;
  
  bool foundRbrack = false;
  while ((*s != endPtr) && ! foundRbrack && 
         (sLegalVectorToks[int(**s)] || isspace(**s)))
  {
    ++(*s);
    if ((foundRbrack = (**s == scRightBracket)))
      ++(*s);
  }
  
  bool allIsGood = true;
  const char* vecEnd = *s;
  
  if (*s != vecBegin)
  {
    // oh boy, we got ourselves a vector. Light the fire!
    
    // rewind *s;
    *s = vecBegin;
    if (**s != scLeftBracket)
      allIsGood = false;
    else
      ++(*s); // pass the [
    
    SInt32 msb, lsb;
    
    // now for msb or index
    if (allIsGood && ! UtConv::strToLongModify(s, &msb, eCarbonDec, NULL))
      allIsGood = false;
    
    // now for : or ]
    if (allIsGood)
    {
      // pass any whitespace
      *s = StringUtil::skip(*s);
      if (**s == scColon)
      {
        ++(*s);
        // We found a partselect.  This is only allowed if we haven't
        // seen anything else yet.  The HdlInfo needs be a scalar -
        // i.e. we haven't changed it yet.
        if (info->getType() == HdlId::eScalar) {
          info->setType(HdlId::eVectBitRange);
          info->setVectMSB(msb);
          if (UtConv::strToLongModify(s, &lsb, eCarbonDec, NULL)) {
            info->setVectLSB(lsb);
            // skip any whitespace to ]
            *s = StringUtil::skip(*s);
            ++(*s); // pass ]
          }
          else
            allIsGood = false;
        } else {
          // The HdlId already has some non-scalar state.  Something
          // like foo[1][7:0] or foo[3:2][7:0] is not allowed.
          allIsGood = false;
        }
      }
      else if (**s == scRightBracket)
      {
        ++(*s);
        // We found a bitselect or an array index.
        if ((info->getType() == HdlId::eVectBit) ||
            (info->getType() == HdlId::eArrayIndex)) {
          // We've already seen a bitselect (foo[1]) or array indices
          // (foo[1][2]), so just add a dimension.  The bitselect will
          // automatically be converted to an array index.
          info->addDimension(msb);
        } else if (info->getType() == HdlId::eScalar) {
          // If's currently a scalar, make it a vector bit.  We don't
          // know if this is a bitselect or an array index, but it
          // doesn't really matter.
          info->setType(HdlId::eVectBit);
          info->setVectIndex(msb);
        } else {
          // Can't add an index to anything else.  Something like
          // foo[3:0][1] is not allowed.
          allIsGood = false;
        }
      }
      else
        allIsGood = false;
    } // if allIsGood
    
    // We should be at the end of the vector now
    if (*s != vecEnd)
      allIsGood = false;
    
    if (! allIsGood)
      info->setType(HdlId::eInvalid);
  }

  Status stat = eLegal;
  if (! allIsGood)
    stat = eIllegal;
  else if ((*s == endPtr) || (**s == scPathDelim) || (vecBegin == vecEnd)) {
    // If we're at the end of the string or the end of the token, or
    // if we didn't find a vector, indicate that there's nothing left
    // to parse.
    stat = eEndPath;
  }

  return stat;
}

HdlVerilogPath::ListType::~ListType()
{
}

HdlVerilogPath::ListStrType::ListStrType(UtStringArray* hierList)
{
  mList = hierList;
}

HdlVerilogPath::ListStrType::ListStrType(const UtStringArray& hierList)
{
  mList = const_cast<UtStringArray*>(&hierList);
}

HdlVerilogPath::ListStrType::~ListStrType()
{
}

bool HdlVerilogPath::ListStrType::append(const char* id)
{
  mList->push_back(id);
  return true;
}

const char* HdlVerilogPath::ListStrType::beginIter()
{
  mIter = mList->loopCUnsorted();
  return getIterStr();
}

const char* HdlVerilogPath::ListStrType::next()
{
  ++mIter;
  return getIterStr();
}

const char* HdlVerilogPath::ListStrType::getIterStr() const
{
  const char* ret = NULL;
  if (! mIter.atEnd())
    ret = *mIter;
  return ret;
}

HdlVerilogPath::ListAtomType::ListAtomType(HdlHierPath::StrAtomVec* hierList, 
                                        const AtomicCache* cache)
{
  mList = hierList;
  mStrCache = cache;
}

HdlVerilogPath::ListAtomType::ListAtomType(const HdlHierPath::StrAtomVec& hierList)
{
  mStrCache = NULL;
  mList = const_cast<HdlHierPath::StrAtomVec*>(&hierList);
}

HdlVerilogPath::ListAtomType::~ListAtomType()
{
}

bool HdlVerilogPath::ListAtomType::append(const char* id)
{
  StringAtom* atom = mStrCache->getIntern(id);
  if (atom)
    mList->push_back(atom);

  return (atom != NULL);
}

const char* HdlVerilogPath::ListAtomType::beginIter()
{
  mIter = mList->begin();
  return getIterStr();
}

const char* HdlVerilogPath::ListAtomType::next()
{
  ++mIter;
  return getIterStr();
}

const char* HdlVerilogPath::ListAtomType::getIterStr() const
{
  const char* ret = NULL;
  if (mIter != mList->end())
  {
    const StringAtom* tmpAtom = *mIter;
    ret = tmpAtom->str();
  }
  return ret;
}


void HdlVerilogPath::appendVectorInfo(UtString* buf, const HdlId* info) const
{
  addInfo(info, buf);
}

