// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file 
  Classes releated to the basic file operations used within HDL
  simulations of any language, includes the tracking of file descriptors.
*/
#include "util/UtString.h"
#include <cassert>
#include "util/ShellMsgContext.h"
#include "util/UtIOStream.h"
#include "util/Loop.h"
#include "util/UtStreamSupport.h"
#include "hdl/HdlFileSystem.h"
#include "util/UtIOStringStream.h"
#include "util/UtCheckpointStream.h"


HDLFileSystem::HDLFileSystem(MsgContext* msgContext) :
  mMsgContext(msgContext)
{
  mCachedFileSystem = new UtCachedFileSystem;
}

HDLFileSystem::~HDLFileSystem()
{
  close();                   // close all open files
  delete mCachedFileSystem;  // release the cached file system
}
 
bool HDLFileSystem::is_open() const
{
  return ( ( mFDtoStreamMap.size() - mFDReservedSet.size() ) > 0 );
}

/*! \brief this closes only the file associated with the descriptor fdToClose,
 * (except for reserved FDs).
 *
 * \warning This method usually deletes an item from mFDtoStreamMap,
 * so if you have an iterator that points to the entry for \a
 * fdToClose it will be invalid after a call to this method.
 */
bool HDLFileSystem::close(UInt32 fdToClose)
{
  bool retStatus = true;        // assume all is well

  if ( not isReservedFD( fdToClose ) )
  {
    // not a reserved file so close it

    FDtoStreamMap::iterator fdtoStreamMap_Itr = mFDtoStreamMap.find(fdToClose);
    if (fdtoStreamMap_Itr == mFDtoStreamMap.end()) {
      // no file associated with this file descriptor
      // do we report problem here? trying to close a file that is not open
    }
    else {
      HDLStreamInfo &stream_info = fdtoStreamMap_Itr->second;

      UtOStream* out_stream = stream_info.mOutStream;
      if ( out_stream ) {
        retStatus &= closeAStream(out_stream);
        delete out_stream;        // delete before we lose our last ref to this stream
      }
      UtIStream* in_stream = stream_info.mInStream;
      if ( in_stream ){
        retStatus &= closeAStream(in_stream);
        delete in_stream;        // delete before we lose our last ref to this stream
      }
      mFDtoStreamMap.erase(fdtoStreamMap_Itr);
    }
  }
  return retStatus;
}

//! closes a stream, returns true if ok
bool HDLFileSystem::closeAStream( UtOStream* stream)
{
  bool status = stream->close(); // false means there was a problem

  if ( not status ) {
    const char * filename = stream->getFilename();
    if (filename == NULL) filename = "stdout||stderr";
    getMsgContext()->SHLFailedToClose(filename, stream->getErrmsg());
  }
  return status;
}

//! closes a stream, returns true if ok
bool HDLFileSystem::closeAStream( UtIStream* stream)
{
  bool status = stream->close(); // false means there was a problem

  if ( not status ) {
    const char * filename = stream->getFilename();
    if (filename == NULL) filename = "stdin";
    getMsgContext()->SHLFailedToClose(filename, stream->getErrmsg());
  }
  return status;
}

/*!
 * \brief this closes all files that were opened with HdlFileOpen, and
 * removes them from mFDtoStreamMap
 * 
 * Files that were reserved with reserveFileDescriptor are not closed and remain
 * reserved upon exit.
 */
bool HDLFileSystem::close()
{
  bool retStatus = true;

  for (FDtoStreamMap::UnsortedLoop fdtoStreamMap_Itr = mFDtoStreamMap.loopUnsorted();
       ! fdtoStreamMap_Itr.atEnd();
       /*done within loop*/)
  {
    UInt32 fd = fdtoStreamMap_Itr.getKey();

    ++fdtoStreamMap_Itr;   // move to the next iterator before calling
                           // close() as it will invalidate the
                           // current iterator.
    this->close(fd);       // I use 'this->' to avoid problem with test/code_review
  }

  return retStatus;
}


// flushes only the output file associated with fdToFlush 
bool HDLFileSystem::flush(UInt32 fdToFlush)
{
  bool retStatus = true;        // assume all is well

  FDtoStreamMap::iterator fdtoStreamMap_Itr = mFDtoStreamMap.find(fdToFlush);
  if (fdtoStreamMap_Itr != mFDtoStreamMap.end())
  {
    HDLStreamInfo& stream_info = fdtoStreamMap_Itr->second;
    UtOStream* out_stream = stream_info.mOutStream;

    if ( out_stream ) {
      retStatus = flushAStream(out_stream);
    }
  }
  return retStatus;
}



// do a flush on all files that are currently open
bool HDLFileSystem::flush()
{
  bool retStatus = true;

  for (FDtoStreamMap::UnsortedLoop fdtoStreamMap_Itr = mFDtoStreamMap.loopUnsorted();
       ! fdtoStreamMap_Itr.atEnd();
       ++fdtoStreamMap_Itr)
  {
    UInt32 fd = fdtoStreamMap_Itr.getKey();
    retStatus &= flush(fd);
  }
  return retStatus;
}

//! flush a stream, returns true if ok
bool HDLFileSystem::flushAStream( UtOStream* stream)
{
  bool status = stream->flush(); // false means there was a problem

  if ( not status )
  {
    const char * filename = stream->getFilename();
    if (filename == NULL) filename = "stdout||stderr";
    getMsgContext()->SHLFailedToFlush(filename, stream->getErrmsg());
  }
  return status;
}

/*!
   Opens a File.

  \param filename Name of the File to be opened.
  \param mode Mode in which the file is to be opened. 
  \param isWritable True if the file is to be opened for writing.
  \param oneHotDescriptor Selects the style of the returned descriptor
 */
HDLFD HDLFileSystem::HdlFileOpen (const char* filename,
                                  const char* mode,
                                  bool isWritable,
                                  bool oneHotDescriptor)
{
  UInt2 status; // Ignore this return status. Used by vhdl FILE_OPEN() only.
  return HdlFileOpen(&status, filename, mode, isWritable, oneHotDescriptor);
}

/*!
   Opens a File.
  \param status Returns the four different vhdl FILE_OPEN_STATUS.
  \param filename Name of the File to be opened.
  \param mode Mode in which the file is to be opened. 
  \param isWritable True if the file is to be opened for writing.
  \param oneHotDescriptor Selects the style of the returned descriptor
 */
HDLFD HDLFileSystem::HdlFileOpen (UInt2* status,
                                  const char* filename,
                                  const char* mode,
                                  bool isWritable,
                                  bool oneHotDescriptor)
{
  // status is for VHDL status variable used in FILE_OPEN procedure.
  *status = 0x00; // OPEN_OK status in vhdl

  // there are two possible types of file descriptors that can be
  // returned, one-hot or encoded.  The one-hot descriptors are
  // compatible with the verilog mutli-channel-descriptors, the
  // encoded are compatible with verilog file descriptors and all vhdl
  // file descriptors.

  const HDLFD out_of_range_one_hot_file_descriptor = (1U<<31);

  // identify the first available descriptor
  HDLFD descriptor = 1;
  if ( not oneHotDescriptor ) {
    descriptor = descriptor | out_of_range_one_hot_file_descriptor;
    // setting the MSB to 1 separates the one-hot descriptors from the
    // non-one-hot.  As a concession to Verilog MCDs we never return a
    // descriptor with the value: out_of_range_one_hot_file_descriptor.
    // (it is a one-hot value but verilog uses that bit to distinguish
    // between MCDs and normal descriptors)
  }

  bool found_available = false;

  for ( ;
        descriptor != 0; // if false then we have considered all posibilities
        descriptor = (oneHotDescriptor ? (descriptor << 1) : descriptor + 1))
  {
    if ( isReservedFD( descriptor ) ){
      continue;
    }
    if ( oneHotDescriptor and ( descriptor == out_of_range_one_hot_file_descriptor )) {
        continue;      // this descriptor value is unavailable for use
    }
    if (mFDtoStreamMap.find(descriptor) == mFDtoStreamMap.end()) {
      found_available = true;
      break;
    }
  }
  if ( not found_available ){
    getMsgContext()->SHLFailedToFindUnusedDescriptor((oneHotDescriptor? "MCD" : "file" ), filename);
    *status = 0x01;  // STATUS_ERROR in vhdl
    INFO_ASSERT(0, "unable to open file");
  }

  bool open_success = HDLFileOpenHelper(filename, descriptor, isWritable, mode);
  if ( not open_success ){
    *status = 0x01;  // STATUS_ERROR in vhdl
    return 0;
  } else {
    return (descriptor);
  }
}

//! here is where we associate the descriptor with the stream (and filename).
bool HDLFileSystem::HDLFileOpenHelper(const char* filename,
                                      const HDLFD descriptor,
                                      bool isWritable,
                                      const char* mode)
{

  if (isWritable)
  {
    //make a new cached file stream, it will be deleted when close() is called
    UtOCStream* f = new UtOCStream(filename, mCachedFileSystem, mode);
    if (!f->is_open())
    {
      getMsgContext()->SHLFailedToOpenForOutput(filename, f->getErrmsg());
      return false;
    }
    bool descriptor_in_use = (mFDtoStreamMap.find(descriptor) != mFDtoStreamMap.end());

    if ( descriptor_in_use ){
      // this is an internal problem
      getMsgContext()->SHLDescriptorAlreadyInUse(descriptor, filename);
      INFO_ASSERT(0, "unable to open file");
    }  

    HDLStreamInfo str_info(f,NULL);
    mFDtoStreamMap.insert(FDtoStreamMap::value_type(descriptor,str_info));
  }
  else
  {
    //make a new cached file stream, it will be deleted when close() is called
    UtICStream* f = new UtICStream(filename, mCachedFileSystem);
    if (!f->is_open())
    {
      getMsgContext()->SHLFailedToOpenForInput(filename, f->getErrmsg());
      return false;
    }
    bool descriptor_in_use = (mFDtoStreamMap.find(descriptor) != mFDtoStreamMap.end());
    if ( descriptor_in_use ){
      // this is an internal problem
      getMsgContext()->SHLDescriptorAlreadyInUse(descriptor, filename);
      INFO_ASSERT(0, "unable to open file");
    }

    HDLStreamInfo str_info(NULL,f);
    mFDtoStreamMap.insert(FDtoStreamMap::value_type(descriptor,str_info));
  }

  return true;
}

bool HDLFileSystem::reserveFileDescriptor(HDLFD descriptor, UtIStream* in_stream, UtOStream* out_stream){

  bool descriptor_reserved = isReservedFD(descriptor);
  FDtoStreamMap::iterator fdtoStreamMap_Itr = mFDtoStreamMap.find(descriptor);
  bool descriptor_in_use = (fdtoStreamMap_Itr != mFDtoStreamMap.end());

  if ( descriptor_reserved ){
    // a consistency check,
    // if in_stream and/or out_stream were previously specified, then 
    // must match the current reservation
    if ( descriptor_in_use ) {
      
      HDLStreamInfo &stream_info(fdtoStreamMap_Itr->second);

      // if current association is empty then just insert the requested value
      if ( stream_info.mOutStream == NULL ){
        stream_info.mOutStream = out_stream;
      }
      if ( stream_info.mInStream == NULL ){
        stream_info.mInStream = in_stream;
      }
      
      if ( ( out_stream and ( out_stream != stream_info.mOutStream ) ) or
           ( in_stream  and ( in_stream  != stream_info.mInStream ))     ) {
        getMsgContext()->SHLFailedToReserveFileDescriptor(descriptor, "file descriptor already reserved to a different stream.");
        return false;
      }
      return true;   // the existing definition is compatible this request
    }
    else {
      getMsgContext()->SHLFailedToReserveFileDescriptor(descriptor, "file descriptor already reserved but not used.");
      return false;
    }
  } else if ( descriptor_in_use ) {
    getMsgContext()->SHLFailedToReserveFileDescriptor(descriptor, "file descriptor already in use, but was not reserved.");
    return false;
  }

  // create the reservation
  HDLStreamInfo str_info(out_stream,in_stream);
  mFDtoStreamMap.insert(FDtoStreamMap::value_type(descriptor,str_info));

  mFDReservedSet.insert(descriptor);
  return true;
}

bool HDLFileSystem::unReserveFileDescriptor(HDLFD descriptor){
  bool descriptor_reserved = isReservedFD(descriptor);
  FDtoStreamMap::iterator fdtoStreamMap_Itr = mFDtoStreamMap.find(descriptor);
  bool descriptor_in_use = (fdtoStreamMap_Itr != mFDtoStreamMap.end());
  
  if ( descriptor_reserved and descriptor_in_use ) {
    // HDLStreamInfo &stream_info(fdtoStreamMap_Itr->second);

    // we are not the owner of the reserved streams, so we do not
    // close them here, but we do remove this item so it is not
    // reserved anymore 
    mFDtoStreamMap.erase(fdtoStreamMap_Itr);
    mFDReservedSet.erase(descriptor);
  }
  return ( descriptor_reserved and descriptor_in_use );
}

bool HDLFileSystem::getHdlFileStream(UtOStream ** oStream, HDLFD fd)
{
  FDtoStreamMap::iterator fdtoStreamMap_Itr = mFDtoStreamMap.find(fd);

  bool descriptor_in_use = (fdtoStreamMap_Itr != mFDtoStreamMap.end());
  
  if ( descriptor_in_use ){
    HDLStreamInfo &stream_info(fdtoStreamMap_Itr->second);

    (*oStream) = stream_info.mOutStream;
  }
  return descriptor_in_use;
}

bool HDLFileSystem::getHdlFileStream(UtIStream ** iStream, HDLFD fd)
{
  FDtoStreamMap::iterator fdtoStreamMap_Itr = mFDtoStreamMap.find(fd);
  bool descriptor_in_use = (fdtoStreamMap_Itr != mFDtoStreamMap.end());
  
  if ( descriptor_in_use ){
    HDLStreamInfo &stream_info(fdtoStreamMap_Itr->second);

    (*iStream) = stream_info.mInStream;
  }
  return descriptor_in_use;
}

bool HDLFileSystem::save(UtOCheckpointStream &out)
{
  UInt2 count;

  out.writeToken("hdl file system");

  // Count all of the non-reserved file descriptors.
  // The reserved ones (stderr, stdin, etc.) will be re-created and 
  // need not be saved.
  count = 0;
  for (FDtoStreamMap::iterator i = mFDtoStreamMap.begin(); i != mFDtoStreamMap.end(); ++i) {
    HDLFD fd = i->first;
    if (!isReservedFD(fd)) {
      ++count;
    }
  }

  // save the streams to the checkpoint file
  out << count;
  for (FDtoStreamMap::iterator i = mFDtoStreamMap.begin(); i != mFDtoStreamMap.end(); ++i) {
    HDLFD fd = i->first;

    if (!isReservedFD(fd)) {
      out << fd;

      bool isInput = (i->second.mInStream != NULL);
      out << isInput;
      if (isInput) {
        i->second.mInStream->save(out);
      }

      bool isOutput = (i->second.mOutStream != NULL);
      out << isOutput;
      if (isOutput) {
        i->second.mOutStream->save(out);
      }
    }
  }

  return !out.fail();
}

bool HDLFileSystem::restore(UtICheckpointStream &in)
{
  bool ok = true;
  UInt2 count;

  if (!in.checkToken("hdl file system")) return false;

  // restore fd -> stream map
  in >> count;
  for (UInt2 i = 0; i < count; i++) {

    HDLFD fd;
    bool isInput, isOutput;
    UtIStream *iStream = NULL;
    UtOStream *oStream = NULL;

    in >> fd;

    in >> isInput;
    if (isInput) {
      iStream = new UtICStream(in, mCachedFileSystem);
      if (iStream->bad()) {
        mMsgContext->SHLCheckpointRestoreError(iStream->getErrmsg());
        ok = false;
      }
    }

    in >> isOutput;
    if (isOutput) {
      oStream = new UtOCStream(in, mCachedFileSystem);
      if (oStream->bad()) {
        mMsgContext->SHLCheckpointRestoreError(oStream->getErrmsg());
        ok = false;
      }
    }

    HDLStreamInfo str_info(oStream, iStream);
    mFDtoStreamMap.insert(FDtoStreamMap::value_type(fd, str_info));
  }

  if (ok) {
    ok = !in.fail();
  }
  return ok;
}
