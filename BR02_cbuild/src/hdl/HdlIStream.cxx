// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file 
  Classes related to file input operations used within VHDL/verilog simulation.
  Such as for $fgets, $fread, FILE_OPEN, READ etc.
*/
#include "util/ShellMsgContext.h"
#include "util/UtIStream.h"
#include "util/Loop.h"
#include "util/UtStreamSupport.h"
#include "hdl/HdlIStream.h"

HdlIStream::HdlIStream(HDLFileSystem* fileSystem, MsgContext* msgContext) :
  mHDLFileSystem(fileSystem),
  mMsgContext(msgContext)
{

  // reserve stdout, stderr, stdin file descriptors  in this file system.
  mHDLFileSystem->reserveFileDescriptor(eMCDstdin,  &(UtIO::cin()), NULL           );
  mHDLFileSystem->reserveFileDescriptor(eMCDstdout, NULL,           &(UtIO::cout()));
  mHDLFileSystem->reserveFileDescriptor(eFDstdin,   &(UtIO::cin()), NULL);
  mHDLFileSystem->reserveFileDescriptor(eFDstdout,  NULL,           &(UtIO::cout()));
  mHDLFileSystem->reserveFileDescriptor(eFDstderr,  NULL,           &(UtIO::cerr()));
  mSourceFileDescriptor = 1;    // a value of 1 is eMCDstdout which
                                // cannot be used for input so
                                // mSourceFileDescriptor and
                                // mSourceFileDescriptors are now in
                                // sync (empty)
}

HdlIStream::~HdlIStream()
{
  close( );                      // close all open files
  // unreserve stdout, stderr, stdin file descriptors  in this  file system.
  mHDLFileSystem->unReserveFileDescriptor(eMCDstdin);
  mHDLFileSystem->unReserveFileDescriptor(eMCDstdout);
  mHDLFileSystem->unReserveFileDescriptor(eFDstdin);
  mHDLFileSystem->unReserveFileDescriptor(eFDstdout);
  mHDLFileSystem->unReserveFileDescriptor(eFDstderr);
}
 
bool HdlIStream::is_open() const
{
  return ( mHDLFileSystem->is_open() );
}

// this closes only the files specified by fdToClose 
// It will not close the reserved files
bool HdlIStream::close(UInt32 fdToClose)
{

  bool retStatus = true;        // assume all is well
  bool aFileWasClosed = false;

  if ( not mHDLFileSystem->isReservedFD( fdToClose ) ) {
      retStatus &= mHDLFileSystem->close(fdToClose);
      aFileWasClosed = true;
  }
  if ( aFileWasClosed ) {
    // force next reference to setup the mSourceFileDescriptors
    mSourceFileDescriptor = 0;    
    mSourceFileDescriptors.clear();
  }
  return retStatus;
}



bool HdlIStream::read(char* buf, UInt32 len)
{
  // TBD ?? . This method may not be required.
  // Only One file should be read.
  typedef CLoop<SourceFileDescriptors> SourceFileDescriptorsCLoop;

  bool returnForAll = true;
  // reads into buf  the file that is currently marked for reading in
  // the HdlIStream
  for (SourceFileDescriptorsCLoop i(mSourceFileDescriptors); not i.atEnd(); ++i)
  {
    UtIStream * stream = *i;

    bool ret = stream->read(buf, len) != 0;
    if (! ret)
    {
      const char * filename = stream->getFilename();
      if (filename == NULL) filename = "stdin";
      getMsgContext()->SHLFailedToRead(filename, stream->getErrmsg());
    }
    returnForAll &= ret;
  }

  return returnForAll;
}

UInt32 HdlIStream::readFromFile(char* buf, UInt32 len)
{
  // TBD // Is this method reqd ?
  // It is not being used currently. The generated code directly calls 
  // UtIStream::operator>> 
  UInt32 ret = 1;
  typedef CLoop<SourceFileDescriptors> SourceFileDescriptorsCLoop;
  bool returnForAll = true;
  // reads into buf  the file that is currently marked for reading in
  // the HdlIStream
  SourceFileDescriptorsCLoop i(mSourceFileDescriptors);
  if (not i.atEnd())
  {
    UtIStream * stream = *i;

    ret += stream->read(buf, len);  // Calls UtIBStream::readFromFile eventually
    if (fail())
    {
      const char * filename = stream->getFilename();
      if (filename == NULL) filename = "stdin";  
      getMsgContext()->SHLFailedToRead(filename, stream->getErrmsg());
    }
    returnForAll &= !fail();
  }

  return ret;
}

//! Reads from File into a LINE variable, represented by str_stream
void HdlIStream::readLine ( UtIOStringStream* str_stream )
{
  // This routine represents the vhdl READLINE() procedure.

  // mSourceFileDescriptor is already setup to the File from which to read.
  // Get the UtIStream associated with the fileDescriptor.
  bool is_defined = false;

  UtString buf;
  if (mSourceFileDescriptor == 0 ) {
    UtIStream* in_stream = NULL;
    is_defined = mHDLFileSystem->getHdlFileStream(&in_stream,
                                                   mSourceFileDescriptor);
    
    in_stream->getline(&buf);  // Get a line from stdin.
  } 
  else
  {
    UtIStream* in_stream = NULL;
    is_defined = mHDLFileSystem->getHdlFileStream(&in_stream,
                                                  mSourceFileDescriptor);
  
    if (is_defined && in_stream)
      in_stream->getline(&buf);  // Get a line from File.
    else
      is_defined = false;
  }

  if (is_defined) {   
    str_stream->reset();       // Clean up existing Buffer
    UInt32 len;
    if (buf[buf.size() - 1] == '\n')
      len = buf.size() - 1;    // remove newline char
    else
      len = buf.size();

    str_stream->insert(buf.c_str(), len);// insert File-line into LINE variable
  }    
}

UtIStream* HdlIStream::putSourceFileDescriptor ( UInt32 descriptor )
{
  // Only one file involved. Add the file associated with this file
  // descriptor to mSourceFileDescriptors
  UtIStream* in_stream = NULL;

  mSourceFileDescriptors.clear();

  bool  is_defined = mHDLFileSystem->getHdlFileStream(&in_stream, descriptor);
  if ( is_defined && in_stream ){
    mSourceFileDescriptors.push_back(in_stream);
  }
  
  mSourceFileDescriptor = descriptor;
  return in_stream;

}

UInt32 HdlIStream::HdlIFileOpen(UInt2* status, const char* filename, const char* mode)
{
  if (not ( 0 == strcmp(mode, "r")) )
  {
    *status = 0x02; // MODE_ERROR status in vhdl
    return 1;
    // Returning the value 1 because it represent eMCDStdout and cannot be used 
    // for input.
  }

  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(status, filename, mode, 
                                                      false/*write_mode*/,
                                                      false);


  return (this_descriptor);

}

UInt32 HdlIStream::HdlIFileOpen (const char* filename, const char* mode)
{
  // rjc-todo really should check that mode is any of the valid forms of read
  if ( not (0 == strcmp(mode, "r")) )
  {
    getMsgContext()->SHLUnableToOpenFileWithMode(filename, mode);
    return eMCDstdout;          // an invalid value for read
  }  

  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(filename, mode, 
                                                      false/*write_mode*/,
                                                      false);

  if ( this_descriptor == 0 )
  {
    getMsgContext()->SHLFatalFileProblem ("Failed to open file.");
    // Execution should have terminated on the fatal error,
    // but leave the folowing assert just to be sure.
    INFO_ASSERT(0, "failed to open file");
  }  
  return (this_descriptor);

}

bool HdlIStream::endOfFile( UInt32 descriptor )
{
  UtIStream * in_stream = NULL;
  bool is_defined = mHDLFileSystem->getHdlFileStream(&in_stream, descriptor);
  if ( is_defined && in_stream ) {
    return in_stream->eof();
  }
  return true;
}


UInt32 HdlIStream::HdlIFileOpen (const char* filename)
{
  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(filename, "r",
                                                      false/*write_mode*/,
                                                      true);

  return (this_descriptor);
}

SInt64 HdlIStream::seek(SInt64 pos, UtIO::SeekMode flag) {
  UtIStream * in_stream = NULL;
  if ((mSourceFileDescriptor != 0) &&
      mHDLFileSystem->getHdlFileStream(&in_stream, mSourceFileDescriptor))
  {
    return in_stream->seek(pos, flag);
  }
  return -1;
}
