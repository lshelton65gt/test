// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/UtConv.h"
#include "util/UtArray.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/ShellMsgContext.h"
#include "util/OSWrapper.h"
#include "shell/ShellGlobal.h"
#include "hdl/ReadMemX.h"
#include "util/UtIStream.h"
#include "util/CarbonAssert.h"

/*!
  \file
  Implementation for class that reads a $readmemX file.
*/

enum LexStat { eLexEnd, eLexAddress, eLexHexNum, eLexInvalid };

class HDLReadMemX::AuxLex
{
public:
  CARBONMEM_OVERRIDES


  AuxLex(const char* filename) :
    mFile(filename),
    mLineNumber(0)
  {}
  
  UtIBStream mFile;
  UtArray<bool> mWriteMap;

  LexStat lex()
  {
    if (mFile.bad())
      return eLexEnd;
    
    LexStat result = eLexHexNum;
    
    bool resolved = false;
    bool inComment = false;
    while (! resolved)
    {
      prepareBuffer();
      if (mLineBuf.empty())
      {
        result = eLexEnd;
        resolved = true;
      }
      else if (inComment)
      {
        inComment = lookForEndComment();
      }
      else if (mLineBuf[0] == '@')
      {
        mLineBuf.erase(0, 1);
        if (mLineBuf.empty() || isspace(mLineBuf[0]))
        {
          mText.clear();
          mText << '@';
          result = eLexInvalid;
        }
        else
        {
          result = readHexNum();
          if (result == eLexHexNum)
            result = eLexAddress;
          else 
            result = eLexInvalid;
        }
        resolved = true;
      } // if address
      else if (mLineBuf.size() > 1)
      {
        if ((mLineBuf[0] == '/') &&
            (mLineBuf[1] == '/'))
          // comment
          mLineBuf.clear();
        else if ((mLineBuf[0] == '/') &&
                 (mLineBuf[1] == '*'))
        {
          mLineBuf.erase(0, 2);
          inComment = lookForEndComment();
        }
        else
        {
          result = readHexNum();
          resolved = true;
        }
      } // line buf size > 1
      else 
      {
        result = readHexNum();
        resolved = true;
      } // else linebuf size == 1
    } //while
    
    // Unterminated comment?
    if (inComment)
      // No text left to give to user, but return invalid because of
      // unterminated comment. Maybe this should be a separate status?
      result = eLexInvalid;
    return result;
  }
  
  UInt32 getLineNumber() const { return mLineNumber; }
  const char* getText() const { return mText.c_str(); }
  
private:
  UInt32 mLineNumber;
  UtString mText;
  UtString mLineBuf;

  void prepareBuffer()
  {
    // for efficiency, avoid stripping the front part of the buffer
    // more than once
    bool newLine = false;
    while (mLineBuf.empty() && mFile.getline(&mLineBuf))
    {
      ++mLineNumber;
      newLine = true;
      // Strip removes ws from both the front and back
      StringUtil::strip(&mLineBuf);
    }
    if (! newLine)
    {
      // Not a new line. Remove leading ws.
      const char* begin = mLineBuf.c_str();
      const char* s = StringUtil::skip(begin);
      if (s > begin)
        mLineBuf.erase(0, (s - begin));
    }
  }

  // returns true if still in comment, false otherwise
  bool lookForEndComment()
  {
    bool inComment = true;
    size_t pos = mLineBuf.find("*/");
    if (pos != UtString::npos)
    {
      inComment = false;
      // erase up to current position + the */
      pos += 2;
      // pass by trailing spaces, if any
      while (isspace(mLineBuf[pos]))
        ++pos;
      mLineBuf.erase(0, pos); 
    }
    else
      mLineBuf.clear();
    
    return inComment;
  }

  // parse a hex number - until non hex character.
  LexStat readHexNum()
  {
    LexStat result = eLexHexNum;
    mText.clear();

    const char* s = mLineBuf.c_str();
    const char* begin = s;

    while((*s == '_') || // separator digit
          ((*s >= '0') && (*s <= '9')) ||
          ((tolower(*s) >= 'a') && (tolower(*s) <= 'f')) ||
          (tolower(*s) == 'x') || (tolower(*s) == 'z') // x/z
          )
      ++s;
    if (s > begin)
    {
      size_t numChars = (s - begin);
      mText.append(mLineBuf, 0, numChars);
      mLineBuf.erase(0, numChars);
    }
    else
    {
      if (*s != '\0')
        mText.append(s, 1);
      result = eLexInvalid;
    }
    
    return result;
  }

  // forbid
  AuxLex();
  AuxLex(const AuxLex&);
  AuxLex& operator=(const AuxLex&);
};

static bool inRange(SInt64 address, SInt64 startAddress, SInt64 endAddress)
{
  if (startAddress < endAddress)
    return ((address >= startAddress) && (address <= endAddress));
  else
    return ((address <= startAddress) && (address >= endAddress));
}

static UInt32 fixIndex(SInt64 address, SInt64 startAddress, SInt64 endAddress,
		       UInt32 depth)
{
  if (startAddress >= endAddress)
    return (UInt32)(address - endAddress);
  else
    return (UInt32)(depth - 1 - (address - startAddress));
}

static HDLReadMemXResult
convertTextNumber(const char* str, UInt32* data,
                  bool hexFormat, int bitWidth, int* truncStat)
{
  bool result = false;

  if (hexFormat)
  {
    result = UtConv::HexStrToUInt32Fit(str, data, NULL, bitWidth, truncStat);
  }
  else
  {
    result = UtConv::BinStrToUInt32Fit(str, data, NULL, bitWidth, truncStat);
  }

  return result ? eRMValidData : eRMInvalidData;
}

HDLReadMemX::HDLReadMemX(const char* fileName, bool hexFormat, UInt32 bitWidth,
                         SInt64 startAddress, SInt64 endAddress, 
                         bool checkComplete,
                         CarbonOpaque instance)
{
  init(fileName, hexFormat, bitWidth, startAddress, endAddress, checkComplete, instance);
}

HDLReadMemX::HDLReadMemX(const char* fileName, bool hexFormat, UInt32 bitWidth,
                         bool isDecreasing,
                         CarbonOpaque instance)
{
  init(fileName, hexFormat, bitWidth, 0, 0, false, instance);
  mDirection = (! isDecreasing) ? 1 : -1;
}

HDLReadMemX::~HDLReadMemX()
{
  delete mAux;
}

void HDLReadMemX::init(const char* fileName, 
                       bool hexFormat, UInt32 bitWidth,
                       SInt64 startAddress, SInt64 endAddress, 
                       bool checkComplete,
                       CarbonOpaque instance)
{

  mAux = new AuxLex(fileName);
  
  mHexFormat = hexFormat;
  mBitWidth = bitWidth;
  mStartAddress = mAddress = startAddress;
  mEndAddress = endAddress;
  mDirection = (mEndAddress >= mAddress) ? 1 : -1;
  mCheckComplete = checkComplete;
  mInstance = instance;
  mNextReadOverflows = false;

}

bool HDLReadMemX::openFile()
{
  // Make sure the file is already open
  if (mAux->mFile.bad())
  {
    ShellGlobal::gCarbonGetMessageContext(mInstance)->SHLReadmemOpenFailed(mAux->mFile.getErrmsg());
    
    return false;
  }
  
  // Set up the vector of booleans so that we can keep track of all
  // the addresses written.
  mRemainingCount = std::abs((SInt32)(mStartAddress - mEndAddress)) + 1;
  mDepth = mRemainingCount;
  mAux->mWriteMap.clear();
  mAux->mWriteMap.resize(mRemainingCount);
  
  return true;
}

void HDLReadMemX::closeFile()
{
  // Check if we have written all words
  if ((mRemainingCount != 0) && mCheckComplete)
    ShellGlobal::gCarbonGetMessageContext(mInstance)->
      SHLInsufficientReadmemData(mAux->mFile.getFilename(),
                                 mRemainingCount,
                                 mStartAddress,
                                 mEndAddress);

  // Close the file
  if (mAux->mFile.is_open())
    mAux->mFile.close();
  else {
    UtString error_msg("Attempt to close the file: ");
    error_msg << mAux->mFile.getFilename() << " after it was used by $readmemx, but the file was not open.";
    INFO_ASSERT(0,error_msg.c_str());
  }
}

HDLReadMemXResult HDLReadMemX::getNextWord(SInt64* address, UInt32* data)
{
  MsgContext* msgContext = NULL;

  // If we're reading data into a specific range, it's not necessarily
  // an error if data is specified in the file for an address outside
  // that range.  If the alert for out-of-range addresses is demoted,
  // keep reading as long at the data is valid.  This is to fix bug
  // 13015.
  const char* filename = mAux->mFile.getFilename();
  UInt32 RMLineNum;
  bool addressInRange = false;
  HDLReadMemXResult result = eRMEnd;
  while (!addressInRange && ((result = readNextRow(data)) == eRMValidData)) {
    RMLineNum = mAux->getLineNumber();
    // Check if the address we read is in range.
    if (inRange(mAddress, mStartAddress, mEndAddress)){
      addressInRange = true;
      // Mark this address as written. If it wasn't written before, we
      // can decrement the remaining count
      UInt32 index = fixIndex(mAddress, mStartAddress, mEndAddress, mDepth);
      if (! mAux->mWriteMap[index] && mCheckComplete)
      {
        --mRemainingCount;
        mAux->mWriteMap[index] = true;
      }
    } else {
      msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
      MsgContext::Severity sev = msgContext->SHLReadmemAddressOutOfRange(filename,
                                                                         RMLineNum, mAddress,
                                                                         mStartAddress,
                                                                         mEndAddress);
      // Only abort and report an out of range error if the message
      // was an error.
      if (MsgContext::isErrorSeverity(sev)) {
        result = eRMAddressOutOfRange;
        break;
      }
    }
    // Regardless of whether the address was in range, update the
    // saved current address.  This is required when an address in the
    // file is out of range, but later addresses in the file are
    // within the range being read.
    *address = mAddress;
    if (mAddress == mEndAddress)
      mNextReadOverflows = true;
    mAddress += mDirection;
  }

  // Print error messages for invalid files
  const char* ReadMemX_text = mAux->getText();
  RMLineNum = mAux->getLineNumber();
  switch (result)
  {
  case eRMValidData:
  case eRMAddressOutOfRange:
    // Both of these were handled above.
    break;
    
  case eRMSyntaxError:
    msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
    msgContext->SHLReadmemSyntaxError(filename,
                                      RMLineNum, ReadMemX_text);
    break;

  case eRMInvalidData:
    msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
    msgContext->SHLReadmemInvalidData(filename, RMLineNum,
                                      ReadMemX_text);
    break;

  case eRMEnd:
    break;
  } // switch
  return result;
} // HDLReadMemXResult HDLReadMemX::getNextWord

HDLReadMemXResult HDLReadMemX::readNextRow(UInt32* data)
{
  MsgContext* msgContext = NULL;
  LexStat token;
  const char* filename = mAux->mFile.getFilename();

  HDLReadMemXResult result = eRMEnd;
  token = eLexInvalid;
  while ((result == eRMEnd) && (token != eLexEnd))
  {
    // Parse the next token
    token = mAux->lex();
    // get the current line number
    UInt32 RMLineNum = mAux->getLineNumber();

    if (token == eLexAddress)
    {
      // We have an address, store it an iterate again
      const char* number = mAux->getText();
      UInt32 tmpAddress[2];
      tmpAddress[0] = tmpAddress[1] = 0;
      int truncStat = 0;
      result = convertTextNumber(number, tmpAddress, true, strlen(number) * 4, &truncStat);
      if (result == eRMValidData)
      {
        if (truncStat == -1)
        {
          msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
          msgContext->SHLReadMemRowTruncated(filename, RMLineNum);
        }
        else if (truncStat == 1)
        {
          msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
          msgContext->SHLReadMemRowExtended(filename, RMLineNum);
        }
        
	// Get the address and reset the result so that we look for data
	mAddress = ((SInt64)tmpAddress[1]) << 32 | tmpAddress[0];
	result = eRMEnd;
        
	// Addresses, cause us to no longer check if the complete
	// range was read in.
	mCheckComplete = false;
        // Also, we could have already been at the last address.
        // Getting a new address just resets the position.
        mNextReadOverflows = false;
      }
    }
    else if (token == eLexHexNum)
    {
      // We have data. This is the end of the line
      // The file may be bigger than we want. We keep track of this
      // during valid data processing
      if (mNextReadOverflows)
      {
        token = eLexEnd; // ends parsing with eRMEnd
        ShellGlobal::gCarbonGetMessageContext(mInstance)->
          SHLTooMuchReadmemData(filename, mStartAddress, mEndAddress);
      }
      else
      {
        const char* number = mAux->getText();
        int truncStat = 0;
        result = convertTextNumber(number, data, mHexFormat, mBitWidth, &truncStat);
        if (truncStat == -1)
        {
          msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
          msgContext->SHLReadMemRowTruncated(filename, RMLineNum);
        }
        else if (truncStat == 1)
        {
          msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
          msgContext->SHLReadMemRowExtended(filename, RMLineNum);
        }
      }
    }
    else if (token != eLexEnd)
      result = eRMSyntaxError;
  } // while
  
  return result;
}

HDLReadMemXResult HDLReadMemX::getNextRowNoDepth(SInt64* address, UInt32* data)
{
  MsgContext* msgContext = NULL;
  HDLReadMemXResult result = readNextRow(data);
  const char* ReadMemX_text = mAux->getText();
  const char* filename = mAux->mFile.getFilename();

  UInt32 RMLineNum = mAux->getLineNumber();

  switch (result)
  {
  case eRMValidData:
    *address = mAddress;
    mAddress += mDirection; 
    break;
    
  case eRMSyntaxError:
    msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
    msgContext->SHLReadmemSyntaxError(filename,
                                      RMLineNum, ReadMemX_text);
    break;
    
  case eRMInvalidData:
    msgContext = ShellGlobal::gCarbonGetMessageContext(mInstance);
    msgContext->SHLReadmemInvalidData(filename, RMLineNum,
                                      ReadMemX_text);
    break;
    
  case eRMAddressOutOfRange:
    // this case is only here to make the switch complete, it should
    // be impossible to get here.
    INFO_ASSERT(0, "Address out of range returned by readNextRow");
    break;

  case eRMEnd:
    break;
  } // switch
  return result;
}

const char* HDLReadMemX::getFileName() const
{
  return mAux->mFile.getFilename();
}
