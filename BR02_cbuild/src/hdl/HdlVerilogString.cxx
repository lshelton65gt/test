// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file 
  Classe(s) specific to string handling in Verilog.
*/

#include "util/CarbonPlatform.h"
#include <stddef.h>
#include <vector>
#include "hdl/HdlVerilogString.h"
#include "util/CarbonAssert.h"
#include "util/CarbonTypes.h"
#include "util/UtConv.h"
#include "util/UtStreamSupport.h"
#include "util/UtString.h"

// The following has the args in the particular order to make code generation easy.
char * HdlVerilogString::convertToStrRep (char * buffer, UInt32 bufferSize, UInt32 numChars, const UInt32* source)
{
  if ( numChars >= bufferSize ){
    // get up to the first 100 chars of the string to give the user a
    // clue about what is causing the problem
    char temp_buf[101];
    UInt32 temp_size(100);
    if ( temp_size > numChars ) {
      temp_size = numChars;
    }
    convertToStrRep(temp_buf, 101, temp_size, source);

    UtString error_message("Error: Buffer size too small.\nA call to convertToStrRep was made with a string of ");
    error_message << numChars << " and a bufferSize of only: " << bufferSize << ".\n  The string starts with: \"" << temp_buf << "\"";;
    INFO_ASSERT(0, error_message.c_str());
    return NULL;
  }


  // put chars into buffer
  UInt32 nullGoesHere = (std::min(numChars, bufferSize-1));
  if (nullGoesHere > 0)
    memcpy(buffer, reinterpret_cast<const char*>(source), nullGoesHere);
  buffer[nullGoesHere] = 0;

  convertNonTrailingNullToSpace(buffer, std::min(numChars, bufferSize-1));

  // find first zero, it might be before numChars
  UInt32 realLen = strlen(buffer);
  // chars in buffer are reversed, straighten them out
  if ( realLen )
  {
    reverse(buffer, (std::min(realLen, bufferSize - 1)));
  }
  return buffer;
}



char * HdlVerilogString::convertToStrRep (char * buffer, UInt32 bufferSize, UInt64 source)
{
  UInt64 cpy = source;

  memcpy(buffer, reinterpret_cast<const char*>(&cpy), bufferSize);
  buffer[bufferSize-1] = 0;

  convertNonTrailingNullToSpace(buffer, bufferSize-1);
  UInt32 realLen = strlen(buffer);

  if (realLen)
    reverse(buffer, (std::min(realLen, bufferSize - 1)));
  return buffer;
}

char * HdlVerilogString::convertToStrRep (char * buffer, UInt32 bufferSize, UInt32 source)
{
  UInt32 cpy = source;

  memcpy(buffer, reinterpret_cast<const char*>(&cpy), bufferSize);
  buffer[bufferSize-1] = 0;

  convertNonTrailingNullToSpace(buffer, bufferSize-1);
  UInt32 realLen = strlen(buffer);

  if (realLen)
    reverse(buffer, (std::min(realLen, bufferSize - 1)));
  return buffer;
}
char * HdlVerilogString::convertToStrRep (char * buffer, UInt32 bufferSize, UInt16 source)
{
  UInt16 cpy = source;

  memcpy(buffer, reinterpret_cast<const char*>(&cpy), bufferSize);
  buffer[bufferSize-1] = 0;

  convertNonTrailingNullToSpace(buffer, bufferSize-1);
  UInt32 realLen = strlen(buffer);

  if (realLen)
    reverse(buffer, (std::min(realLen, bufferSize - 1)));
  return buffer;
}
char * HdlVerilogString::convertToStrRep (char * buffer, UInt32 bufferSize, UInt8 source)
{
  if ( bufferSize <= 1 ){
    UtString error_message("Error: Buffer size too small.\nA call to convertToStrRep was made with a single character but a bufferSize of <= 1 characters.\nThe character is: '");
    if ( source == 0 ) {
      error_message << "\\0'.";
    } else {
      error_message << (char)source << "'.";
    }
    INFO_ASSERT(0, error_message.c_str());
    return NULL;
  }
  buffer[0] = source;           // can only be one char
  buffer[1] = 0;
  return buffer;
}

void HdlVerilogString::reverse(char* str, UInt32 size)
{
  SInt32 lhp = 0;
  SInt32 rhp = size-1;          // zero index
  while ( lhp < rhp )
  {
    char tmp = str[lhp];
    str[lhp] = str[rhp];
    str[rhp] = tmp;
    lhp++;
    rhp--;
  }
}

void HdlVerilogString::convertNonTrailingNullToSpace(char* buffer, SInt32 size)
{
  bool found_nonnull = false;
  for (SInt32 pos = size-1; pos >= 0; pos-- ){
    if ( found_nonnull ){
      // conversion phase
      if ( buffer[pos] == 0 ){
        buffer[pos]= ' ';
      }
    } else {
      // looking for first non-null char
      if ( buffer[pos] != 0 ){
        found_nonnull = true;
      }
    }
  }
}

// converts a string that may contain Verilog escaped chars into a
// UtString of ones/zeros. Verilog escaped chars are converted to their
// un-escaped equivalents   \n \t \\ \" \d (d is octal digit) before
// conversion to binary.
void HdlVerilogString::convertVerilogStringToBinaryString (const UtString *orig_str, UtString *binary_str)
{
  const char* buf = orig_str->c_str();
  UInt32 from_pos = 0;
  UInt32 beyond_end_pos = orig_str->length();
  char next_char;
  UtString new_buf;

  // work our way through orig_str, looking at each char in turn
  
  while ( from_pos < beyond_end_pos ){
    if ( buf[from_pos] != '\\' ) {
      new_buf << buf[from_pos];
      from_pos++;
    } else {
      from_pos++;      // first char of this bit is a backslash
      INFO_ASSERT((from_pos < beyond_end_pos), "Error while converting string to internal form, backslash at end of string?");
      next_char = buf[from_pos];
      switch ( next_char ){
      case '\\': {
        new_buf << '\\';
        from_pos++;
        break;
      }
      case 'n':{
        new_buf << '\n';
        from_pos++;
        break;
      }
      case 't':{
        new_buf << '\t';
        from_pos++;
        break;
      }
      case '"':{
        new_buf << '"';
        from_pos++;
        break;
      }
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':{
        // assumes ascii char encoding
        UInt32 num_chars = 0;
        UInt32 value = 0;
        while ( (num_chars < 3) && ( ( '0' <= next_char ) && (next_char <= '7') ) ) {
          num_chars++;          // counts chars combined into value
          value = (value*8) + (next_char - '0');
          from_pos++;
          if ( from_pos < beyond_end_pos ) {
            next_char = buf[from_pos];
          } else {
            break;
          }
        }
        new_buf <<(char) value;
        break;
      }
      default:{
        // unrecognized escape sequence, remove the backslash
        new_buf << next_char;
        from_pos++;
        break;
      }
      }
    }
  }

  reverse(new_buf.getBuffer(), new_buf.length());
  UInt32 binary_length = (8 * new_buf.length());
  binary_str->reserve(binary_length+1); // reserve space in binary_str (with one extra char for trailing null)
  binary_str->resize(binary_length);  // but we do not need the trailing null
  int convToStrOK = CarbonValRW::writeBinValToStr(binary_str->getBuffer(), binary_length+1, new_buf.getBuffer(), binary_length);
  INFO_ASSERT(convToStrOK != -1, "Failed to convert string to internal form.");
}

