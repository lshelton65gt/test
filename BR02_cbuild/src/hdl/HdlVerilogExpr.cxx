// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/
#include <ctype.h>
#include "util/UtConv.h"
#include "util/DynBitVector.h"
#include "hdl/HdlVerilogExpr.h"


HdlVerilogExpr::HdlVerilogExpr() {}

HdlVerilogExpr::~HdlVerilogExpr() {}

HdlVerilogExpr::Status
HdlVerilogExpr::parseConst(const char* constStr, DynBitVector& value,
                           UtString* errMsg)
{
  // Parse the size of the constant
  UInt32 size = 0;
  const char* str = constStr;
  while ((*str != '\0') && isdigit(*str))
  {
    size = (size * 10) + ((UInt32)*str - (UInt32)'0');
    ++str;
  }
  if (size == 0)
  {
    *errMsg << "size component is missing or invalid";
    return eError;
  }

  // Parse the '
  if (*str != '\'')
  {
    *errMsg << "radix component is missing or invalid";
    return eError;
  }
  ++str;

  // Resize the dynamic bit vector to the right size
  value.resize(size);

  // Convert the string to a constant
  Status status = eSuccess;
  const char radix = *str++;
  switch (radix) {
    case 'h':
    case 'H':
    {
      UInt32* data = value.getUIntArray();
      DynBitVector xzMask(value.size());
      UInt32* drive = xzMask.getUIntArray();
      int truncExtStat;
      if (!UtConv::HexStrToUInt32Fit(str, data, drive, size, &truncExtStat)) {
        *errMsg << "value component is missing or invalid";
        status = eError;

      } else if (xzMask.flip().any()) {
        // Error, we have x's or z'z
        *errMsg << "value component contains x's or z's";
        status = eError;

      } else if (truncExtStat == -1) {
        // Truncation could be a potential bug
        *errMsg << "value truncated to match size";
        status = eWarning;
      }
      break;
    }

    case 'b':
    case 'B':
    {
      UInt32* data = value.getUIntArray();
      DynBitVector xzMask(value.size());
      UInt32* drive = xzMask.getUIntArray();
      int truncExtStat;
      if (!UtConv::BinStrToUInt32Fit(str, data, drive, size, &truncExtStat)) {
        *errMsg << "value component is missing or invalid";
        status = eError;

      } else if (xzMask.flip().any()) {
        // Error, we have x's or z'z
        *errMsg << "value component contains x's or z's";
        status = eError;

      } else if (truncExtStat == -1) {
        // Truncation could be a potential bug
        *errMsg << "value truncated to match size";
        status = eWarning;
      }
      break;
    }

    case 'd':
    case 'D':
    case 'o':
    case 'O':
      *errMsg << "unsupported radix `" << radix << "'";
      status = eError;
      break;

    default:
      *errMsg << "radix component is missing or invalid";
      status = eError;
      break;
  } // switch
  return status;
} // HdlVerilogExpr::parseConst
