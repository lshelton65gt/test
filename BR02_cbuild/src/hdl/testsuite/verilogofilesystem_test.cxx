// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtIOStream.h"
#include <cstdio>
#include "hdl/HdlFileSystem.h"
#include "hdl/HdlVerilogOFileSystem.h"
#include "hdl/HdlVerilogString.h"
#include "util/BitVectorStream.h" 
#include "util/MsgContext.h"
#include "util/ShellMsgContext.h"
#include "util/OSWrapper.h"

#include "util/CarbonTypes.h"


//! run test 1 with output to the specified filesystem
/*!
 * test 1 is looking for an error message (either due to flush or
 * close on a read-only file)
 * one of:
 *   open->write->make read only->flush->check error message for fail to flush
 *   open->write->make read only->close->check error message for fail to close
 * done for both mcd and fd file specifiers
 */
bool vofsTest1(VerilogOutFileSystem* fs, const char * nlIndent, bool doFlush)
{
  bool retStatus = true;
  bool bstat;

  (*fs) << nlIndent << "vofsTest1 START";

  UInt32 mcd2 = fs->VerilogOFileOpen("vofsTest1_mcd2.dat");

  // first test with MCD
  fs->putTargetFileDescriptor(mcd2); // output only goes to mcd2
  (*fs) << nlIndent << "vofsTest1 run ckpt1";

  // change the file to readonly
  UtString errorString;
  int status = OSSystem("chmod 444 vofsTest1_mcd2.dat", &errorString);
  if ( status != 0 )
  {
    printf("Error, unable to chmod on vofsTest1_mcd2.dat. %s\n", errorString.c_str());
  }

  if ( doFlush ) {
    bstat = fs->flush();
    assert (bstat);
    // here is where we should see the error but for some reason we are
    // able to flush the file even after it has been changed to
    // read-only.
  }

  bstat = fs->close(mcd2);
  assert (bstat);
  // here is where we should see an error message but for some reason we are
  // able to close the file even after it has been changed to read-only.

  // change protection on file back to writable, so it can be used again
  status = OSSystem("chmod 644 vofsTest1_mcd2.dat", &errorString);
  if ( status != 0 )
  {
    printf("Error, unable to re-chmod on vofsTest1_mcd2.dat. %s\n", errorString.c_str());
  }



  // now test with non-MCD

  UInt32 fd3 = fs->VerilogOFileOpen("vofsTest1_fd3.dat");
  
  fs->putTargetFileDescriptor(fd3); // output only goes to fd3
  (*fs) << nlIndent << "vofsTest1 run ckpt1" ;

  status = OSSystem("chmod 444 vofsTest1_fd3.dat", &errorString);
  if ( status != 0 )
  {
    printf("Error, unable to chmod on vofsTest1_fd3.dat. %s\n", errorString.c_str());
  }

  if ( doFlush ) {
    bstat = fs->flush();
    assert(bstat);
    // here is where we should see the error but for some reason we are
    // able to flush the file even after it has been changed to
    // read-only.
  }


  bstat = fs->close(fd3);
  assert (bstat);
  // here is where we should see an error message but for some reason we are
  // able to close the file even after it has been changed to read-only.


  // change protection on file back to writable, so it can be used again
  status = OSSystem("chmod 644 vofsTest1_fd3.dat", &errorString);
  if ( status != 0 )
  {
    printf("Error, unable to re-chmod on vofsTest1_fd3.dat. %s\n", errorString.c_str());
  }


  fs->putTargetFileDescriptor(0x80000002U); // send output to stderr
  (*fs) << nlIndent << "vofsTest1 END" ;

  return retStatus;
}


//! run test 2 with output to the specified filesystem
/*!
 * Test2 is used to see if we can open a read-only file.
 */
bool vofsTest2(VerilogOutFileSystem* fs, const char * nlIndent)
{
  bool retStatus = true;
  bool bstat;
  
  (*fs) << nlIndent << "vofsTest2 START";

  UtString errorString;

  int status = OSSystem("touch vofsTest2_read-only.dat", &errorString);
  if ( status != 0 )
  {
    printf("Error, unable to create file  vofsTest2_read-only.dat. %s\n", errorString.c_str());
  }
  status = OSSystem("chmod 444 vofsTest2_read-only.dat", &errorString);
  if ( status != 0 )
  {
    printf("Error, unable to chmod on vofsTest2_read-only.dat. %s\n", errorString.c_str());
  }

  // try to open this read-only file, we expect an error message
  (*fs) << nlIndent << "The following line should be a message about being unable to open the file vofsTest2_read-only.dat\n" ;
  UInt32 fd3 = fs->VerilogOFileOpen("vofsTest2_read-only.dat");
  bstat = fs->flush();                  // do a flush to get output in sync
  assert (bstat);

  fs->putTargetFileDescriptor(fd3); // output only goes to fd3
  (*fs) << nlIndent << "vofsTest2 run ckpt1" ;

  bstat = fs->close(fd3);
  assert(bstat);
  // if properly working the file never opened, but we do this just in
  // case, it will probably generate an error if the file could not be
  // closed

  status = OSUnlink("vofsTest2_read-only.dat", &errorString);
  if ( status != 0 )
  {
    printf("Error, unable to unlink vofsTest2_read-only.dat. %s\n", errorString.c_str());
  }


  fs->putTargetFileDescriptor(0x80000002U); // send output to stderr
  (*fs)  << nlIndent << "vofsTest2 END";
  return retStatus;
}


//! run test 3 with output to the specified filesystem
/*!
 * Test3 is making sure that an attempted close on stdout/stderr does
 * not cause a close, and does not cause any error
 * test: open stdout->write->close->check for no message
 * do tests both on mcd and fd version of stdout, and fd verison of stderr
 */
bool vofsTest3(VerilogOutFileSystem* fs, const char * nlIndent)
{
  bool retStatus = true;
  bool bstat;
  
  fs->putTargetFileDescriptor(0x80000002U); // send output to stderr
  (*fs) << nlIndent << "vofsTest3 START";

  
  (*fs) << nlIndent << "now try to close stdout(mcd), no error expected";
  // try to close stdout
  bstat = fs->close(1U);
  assert(bstat);  
  // no messages should be seen here (aldec)


  (*fs) << nlIndent << "now try to close stdout(fd), no error expected";
  // try to close stdout
  UInt32 fd = (0x80000000 | 1U); // set to stdout
  bstat = fs->close(fd);
  assert(bstat);
  // no messages should be seen here (aldec)


  (*fs) << nlIndent << "now try to close stderr(fd), no error expected";
  // try to close 
  fd = (0x80000000 | 2U); // stderr
  bstat = fs->close(fd);
  assert(bstat);
  // no messages should be seen here (aldec)




  fs->putTargetFileDescriptor(0x80000002U); // send output to stderr
  (*fs) << nlIndent << "vofsTest3 END";
  return retStatus;
}



int main()
{
  bool status = true;
  
  MsgStreamIO* errStream = new MsgStreamIO(stderr, true);
  MsgContext* msgContext = new MsgContext;
  msgContext->addReportStream(errStream);
  HDLFileSystem* hdl_file_system = new HDLFileSystem(msgContext);
  VerilogOutFileSystem* vofs = new VerilogOutFileSystem(hdl_file_system, msgContext);

  // all output is sent to stderr since we want to watch for some
  // error messages and this helps keep them in order
  vofs->putTargetFileDescriptor(0x80000002U); // send output to stderr
  (*vofs) << "verilogofilesystem_test START\n";

  status |= vofsTest1(vofs, "\nT1:  ", true);
  status |= vofsTest1(vofs, "\nT1:  ", false);

  status |= vofsTest2(vofs, "\nT2:  ");

  status |= vofsTest3(vofs, "\nT3:  ");

  vofs->putTargetFileDescriptor(0x80000002U); // send output to stderr
  (*vofs) << "\n\n" << "test result: " << (status ? "PASSED" : "FAILED") << "\n";
  (*vofs) << "verilogofilesystem_test END\n";
}

