// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtIOStream.h"
#include <iomanip>
#include "util/CarbonTypes.h"
#include "hdl/HdlVerilogDist.h"


#define ITERATIONS 32

int main()
{
  SInt32 seed;
  SInt32 value;

  char* p;                      // pull in strtoll from libutil.a, otherwise
  (void) strtoll("10", &p, 10); // on windows it will be unresolved in libc++.

  /*
  ** $random
  */
  seed = 0;
  UtIO::cout() << "$random, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Random(&seed);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$random, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Random(&seed);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }

  /*
  ** $dist_chi_square
  */
  seed = 0;
  UtIO::cout() << "$dist_chi_square, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::ChiSquare(&seed, 2);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$dist_chi_square, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::ChiSquare(&seed, 2);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }

  /*
  ** $dist_uniform
  */
  seed = 0;
  UtIO::cout() << "$dist_uniform, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Uniform(&seed, 2, 12500);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$dist_uniform, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Uniform(&seed, 2, 12500);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 2660000;
  UtIO::cout() << "$dist_uniform, seed = 2660000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Uniform(&seed, -35493, 12500);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }

  /*
  ** $dist_normal
  */
  seed = 0;
  UtIO::cout() << "$dist_normal, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Normal(&seed, 200, 1);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$dist_normal, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Normal(&seed, 200, 1);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }

  /*
  ** $dist_exponential
  */
  seed = 0;
  UtIO::cout() << "$dist_exponential, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Exponential(&seed, 32000);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$dist_exponential, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Exponential(&seed, 32000);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }

  /*
  ** $dist_poisson
  */

  seed = 0;
  UtIO::cout() << "$dist_poisson, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Poisson(&seed, 1);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$dist_poisson, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Poisson(&seed, 1);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }


  /*
  ** $dist_t
  */
  seed = 0;
  UtIO::cout() << "$dist_t, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::T(&seed, 2);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$dist_t, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::T(&seed, 2);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }

  /*
  ** $dist_erlang
  */
  seed = 0;
  UtIO::cout() << "$dist_erlang, seed = 0" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Erlang(&seed, 15, 25);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
  seed = 20000;
  UtIO::cout() << "$dist_erlang, seed = 20000" << UtIO::endl;
  for (UInt32 i = 0; i < ITERATIONS; i++)
  {
    value = HdlVerilogDist::Erlang(&seed, 15, 25);
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << seed << " ";
    UtIO::cout() << UtIO::hex << UtIO::setw(8) << UtIO::setfill('0') << value << UtIO::endl;
  }
}

