// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtStream.h"
#include "util/UtIOStream.h"
#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/UtList.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"
#include "util/UtVector.h"

class EmptyBOM : public virtual STFieldBOM
{
public:
  EmptyBOM() {}
  virtual ~EmptyBOM() {}
    
  virtual Data allocLeafData() {
    return NULL;
  }

  virtual Data allocBranchData() {
    return NULL;
  }
  
  virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata) {
    *bomdata = NULL;
  }

  virtual void freeBranchData(const STBranchNode*, Data* bomdata) {
    *bomdata = NULL;
  }

  virtual void preFieldWrite(ZostreamDB&) 
  {}

  virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const {
  }
  
  virtual void writeBranchData(const STBranchNode*, ZostreamDB&,
                               AtomicCache*) const {
  }

  virtual ReadStatus preFieldRead(ZistreamDB&) 
  {
    return eReadOK;
  }
  
  virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, 
                                  MsgContext*) {
    return eReadOK;
  }
  
  virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, 
                                    MsgContext*) {
    return eReadOK;
  }
  
  virtual void printLeaf(const STAliasedLeafNode*) const {
  }

  virtual void printBranch(const STBranchNode*) const {
  }

  virtual void writeBOMSignature(ZostreamDB&) const {
  }

  virtual ReadStatus readBOMSignature(ZistreamDB&, UtString*) {
    return eReadIncompatible;
  }


 //! Return BOM class name
  virtual const char* getClassName() const
  {
    return "EmptyBOM";
  }

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
  {
  }

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
  {
  }



};

int main()
{
  AtomicCache strCache; // used later
  UtVector<const char*> names;
  names.push_back("a.b.\\tnet\\net2 ");
  StringAtom* atom1 = strCache.intern("a");
  StringAtom* atom2 = strCache.intern("b");
  StringAtom* atom3 = strCache.intern("\\tnet\\net2 ");

  names.push_back("top.\\next .bottom");
  StringAtom* atom4 = strCache.intern("top");
  StringAtom* atom5 = strCache.intern("\\next ");
  StringAtom* atom6 = strCache.intern("bottom");

  names.push_back("\\busa+index\\-clock\\***error-condition***\\net1/\\net2\\{a,b}\\a*(b+c) ");
  names.push_back("shiftreg_abusa_index");
  names.push_back("c.d?.\\foo *");
  names.push_back("?a.*");

  HdlVerilogPath vpath;
  HdlId info;
  UtString tmp;
  int stat = 0;
  unsigned int i = 0;
  UtIO::cout() << "PARSENAME TEST\n";
  {
    UtStringArray parseNames;
    UtStringArray elemList;
    const char* path = "stupid ooga.booga my.vector[3]";
    bool bad = false;
    while ((vpath.parseName(&path, &elemList, &info) == HdlHierPath::eEndPath) &&
           (*path != '\0'))
    {
      UtString buf;
      vpath.compPath(elemList, &buf, &info);
      parseNames.push_back(buf);

      if ((i == 0) && buf.compare("stupid") != 0)
        bad = true;
      else if ((i == 1) && buf.compare("ooga.booga") != 0)
        bad = true;
      else if ((i == 3) && buf.compare("my.vector[3]") != 0)
        bad = true;
      else if (i > 3)
        bad = true;
      ++i;
      elemList.clear();
      while (isspace(*path))
        ++path;
    }
    if (bad)
    {
      UtIO::cerr() << "ParseName failed" << UtIO::endl;
      stat = 3;
    }
  }
  i = 0;
  UtIO::cout() << "STRINGS\n";
  UtVector<const char*>::iterator nl = names.begin();
  {
    while (nl != names.end())
    {
      UtStringArray theList;
      if (vpath.decompPath(*nl, &theList, &info) == HdlHierPath::eLegal)
      {

        if (i == names.size() - 2)
        {
          UtString last(theList.back());
          if (last.find_first_of("*?") != UtString::npos)
          {
            UtIO::cerr() << "Wildcards allowed when they are not supposed to be.\n";
            stat = 3;
          }
        }

        if (i == names.size() - 1)
        {
          UtIO::cerr() << "Path beginning with wildcard parsed legally!" << UtIO::endl;
          stat = 3;
        }

        UtStringArray::iterator p = theList.begin();
        while (p != theList.end())
        {
          UtIO::cout() << *p << '\n';
          p++;
        }

        // now compose
        vpath.compPath(theList, &tmp);
        UtIO::cout() << "compose: " << tmp << '\n';
        if ((i <names.size() - 2) && tmp.compare(*nl) != 0)
        {
          UtIO::cout() << "Composition failed\n";
          stat = 3;
        }
      }
      else if (i != names.size() - 1)
      {
        UtIO::cerr() << "Parsing not done on a legal name?" << UtIO::endl;
        stat = 3;
      }
      
      nl++;
      i++;
    }
  }

  UtIO::cout() << "\nSTRINGATOMS\n";
  nl = names.begin();
  {
    i = 0;
    while (nl != names.end())
    {
      HdlHierPath::StrAtomVec theAtomList;
      if (vpath.decompPathAtom(*nl, &theAtomList, &strCache, &info) == HdlHierPath::eLegal)
      {
        if (i >= 2)
        {
          UtIO::cerr() << "Decomposition passed with un-cached strings.\n";
          stat = 3;
        }
        HdlHierPath::StrAtomVec::iterator p = theAtomList.begin();
        StringAtom* atom;
        while (p != theAtomList.end())
        {
          atom = *p;
          UtIO::cout() << atom->str() << '\n';
          p++;
        }

        vpath.compPathAtom(theAtomList, &tmp);
        UtIO::cout() << "compose: " << tmp << '\n';
        if (tmp.compare(*nl) != 0)
        {
          UtIO::cout() << "Composition failed\n";
          stat = 3;
        }
      }
      else if (i < 2)
      {
        UtIO::cerr() << "Did not parse name\n";
        stat = 3;
      }
      nl++;
      i++;
    }
  }

  UtIO::cout() << "\nTOKEN PARSE\n";
  {
    const char* tokName = "   man.oh.\\wow .this\t actually.works[-3]\n";
    const char* token;
    int j;
    {
      j = 0;

      UtString buf;
      HdlHierPath::Status hdlStat;
      bool done = false;
      while ((((hdlStat = vpath.parseToken(&tokName, &buf, &info, false)) == HdlHierPath::eLegal) || 
              (hdlStat == HdlHierPath::eEndPath)) && ! done)
      {
        token = buf.c_str();
        if (! buf.empty())
        {
          UtIO::cout() << token << '\n';
          int parseStat = 0;
          switch (j)
          {
          case 0:
            if ((strcmp(token, "man") != 0) ||
                info.getType() != HdlId::eScalar)
              parseStat = 3;
            break;
          case 1:
            if ((strcmp(token, "oh") != 0) ||
                info.getType() != HdlId::eScalar)
              parseStat = 3;          
            break;
          case 2:
            if ((strcmp(token, "\\wow ") != 0) ||
                info.getType() != HdlId::eScalar)
              parseStat = 3;          
            break;
          case 3:
            if ((strcmp(token, "this") != 0) ||
                info.getType() != HdlId::eScalar)
              parseStat = 3;          
            break;
          case 4:
            if ((strcmp(token, "actually") != 0) ||
                info.getType() != HdlId::eScalar)
              parseStat = 3;          
            break;
          case 5:
            if ((strcmp(token, "works") != 0) ||
                (info.getType() != HdlId::eVectBit) ||
                (info.getVectIndex() != -3) || 
                (hdlStat != HdlHierPath::eEndPath))
              parseStat = 3;
            else
              done = true;
            break;
          default:
            parseStat = 3;
            break;
          }
          
          if (parseStat)
          {
            UtIO::cerr() << "parseToken failed " << j << '\n';
            stat = 3;
          }
        }
        j++;

        if (j > 6)
        {
          done = true;
          hdlStat = HdlHierPath::eIllegal;
          UtIO::cerr() << "Parse token is overrunning\n";
          stat = 3;
        }
      }
      
      if ((j < 6) || (hdlStat != HdlHierPath::eEndPath))
      {
        UtIO::cerr() << "Did not parse token properly\n";
        stat = 3;
      }
    }
  }

  UtIO::cout() << "\nTOKEN PARSE WITH ESCAPED ID VECTOR\n";
  {
    const char* tokName = "a.\\f!@#$ [3:0]\n";
    const char* token;
    int j;
    {
      j = 0;

      UtString buf;
      HdlHierPath::Status hdlStat;
      bool done = false;
      while ((((hdlStat = vpath.parseToken(&tokName, &buf, &info, false)) == HdlHierPath::eLegal) || 
              (hdlStat == HdlHierPath::eEndPath)) && ! done)
      {
        token = buf.c_str();
        if (! buf.empty())
        {
          UtIO::cout() << token << '\n';
          int parseStat = 0;
          switch (j)
          {
          case 0:
            if ((strcmp(token, "a") != 0) ||
                info.getType() != HdlId::eScalar)
              parseStat = 3;
            break;
          case 1:
            if ((strcmp(token, "\\f!@#$ ") != 0) ||
                (info.getType() != HdlId::eVectBitRange) ||
                (info.getVectLSB() != 0) || 
                (info.getVectMSB() != 3) || 
                (hdlStat != HdlHierPath::eEndPath))
              parseStat = 3;
            else
              done = true;
            break;
          default:
            parseStat = 3;
            break;
          }
          
          if (parseStat)
          {
            UtIO::cerr() << "parseToken failed " << j << '\n';
            stat = 3;
          }
        }
        j++;

        if (j > 2)
        {
          done = true;
          hdlStat = HdlHierPath::eIllegal;
          UtIO::cerr() << "Parse token is overrunning\n";
          stat = 3;
        }
      }
      
      if ((j < 2) || (hdlStat != HdlHierPath::eEndPath))
      {
        UtIO::cerr() << "Did not parse token properly\n";
        stat = 3;
      }
    }
  }
  
  UtIO::cout() << "\nComposition - HierName\n";
  EmptyBOM bom;
  STSymbolTable symTable(&bom, &strCache);
  symTable.setHdlHier(&vpath);
  STSymbolTableNode* node = symTable.createNode(atom1, NULL, false);
  node = symTable.createNode(atom2, node->castBranch(), false);
  STSymbolTableNode* first = symTable.createNode(atom3, node->castBranch(), true);
  
  node = symTable.createNode(atom4, NULL, false);
  node = symTable.createNode(atom5, node->castBranch(), false);
  STSymbolTableNode* second = symTable.createNode(atom6, node->castBranch(), true);
  
  vpath.compPathHier(first, &tmp);
  UtIO::cout() << "HierCompose: " << tmp << '\n';
  nl = names.begin();
  if (tmp.compare(*nl) != 0)
  {
    UtIO::cerr() << "HierName compose failed\n";
    stat = 3;
  }
  vpath.compPathHier(second, &tmp);
  nl++;
  if (tmp.compare(*nl) != 0)
  {
    UtIO::cerr() << "HierName compose failed\n";
    stat = 3;
  }
  
  UtIO::cout() << "HierCompose: " << tmp << '\n';
  {
    UtIO::cout() << "\nAppend Hiername with tail\n";
    HdlId info2;
    info2.setType(HdlId::eVectBitRange);
    info2.setVectLSB(12);
    info2.setVectMSB(36);  
    vpath.compPathHierAppend(second, atom1, &tmp, &info2);
    UtString cmpStr;
    UtOStringStream cmpStream(&cmpStr);
    cmpStream << *nl << '.' << atom1->str() << '[' << UtIO::dec
              << 36 << ':' << UtIO::dec << 12 << ']';
    
    if (tmp.compare(cmpStr) != 0)
    {
      UtIO::cerr() << "HierName compose failed" << UtIO::endl;
      stat = 3;
    }
    
    UtIO::cout() << "HierAppend: " << tmp << UtIO::endl;
  }

  {
    UtIO::cout() << "\nAppend with str and tail\n";
    HdlId info2;
    info2.setType(HdlId::eVectBit);
    info2.setVectIndex(5);
    UtString cmpStr, tmp2 = "a.\\b ";
    UtOStringStream cmpStream(&cmpStr);
    cmpStream << tmp2;
    vpath.compPathAppend(&tmp2, "nada", &info2);

    cmpStream << '.' << "nada" << '[' << UtIO::dec << 5 << ']';
    if (tmp2.compare(cmpStr) != 0)
    {
      UtIO::cerr() << "String append compose failed" << UtIO::endl;
      stat = 3;
    }
  
    UtIO::cout() << "StringAppend: " << tmp2 << UtIO::endl;
  }

  UtIO::cout() << "Stupid Scope[0] construct" << UtIO::endl;
  {
    const char* vectorScope1 = "scope[3].blah[40:2]";
    // Just in case, let's be able to parse scope ranges as well
    const char* vectorScope2 = "scope[1:4].inst[0].mama[2]";
    
    UtString vecScope1(vectorScope1);
    UtString vecScope2(vectorScope2);
    
    UtString tokBuf;
    HdlId tokInfo;
    if (vpath.parseToken(&vecScope1, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "scope[3] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "scope[3]") != 0)
    {
      UtIO::cerr() << "scope[3] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }

    if (tokInfo.getType() != HdlId::eVectBit)
    {
      UtIO::cerr() << "scope[3] token parse created incorrect info" << UtIO::endl;
      stat = 3;

    }

    if (vpath.parseToken(&vecScope1, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "blah[40:2] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "blah[40:2]") != 0)
    {
      UtIO::cerr() << "blah[40:2] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }

    if (tokInfo.getType() != HdlId::eVectBitRange)
    {
      UtIO::cerr() << "blah[40:2] token parse created incorrect info" << UtIO::endl;
      stat = 3;
      
    }


    if (vpath.parseToken(&vecScope2, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "scope[1:4] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "scope[1:4]") != 0)
    {
      UtIO::cerr() << "scope[1:4] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }
    
    if (tokInfo.getType() != HdlId::eVectBitRange)
    {
      UtIO::cerr() << "scope[1:4] token parse created incorrect info" << UtIO::endl;
      stat = 3;
      
    }

    if (vpath.parseToken(&vecScope2, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "inst[0] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "inst[0]") != 0)
    {
      UtIO::cerr() << "inst[0] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }

    if (tokInfo.getType() != HdlId::eVectBit)
    {
      UtIO::cerr() << "inst[0] token parse created incorrect info" << UtIO::endl;
      stat = 3;

    }


    if (vpath.parseToken(&vecScope2, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "mama[2] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "mama[2]") != 0)
    {
      UtIO::cerr() << "mama[2] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }

    if (tokInfo.getType() != HdlId::eVectBit)
    {
      UtIO::cerr() << "mama[2] token parse created incorrect info" << UtIO::endl;
      stat = 3;

    }

    UtStringArray vecScopes;
    if (vpath.decompPath(vectorScope1, &vecScopes, &tokInfo) != HdlHierPath::eLegal)
    {
      UtIO::cerr() << "decompPath for vectorScope1 failed" << UtIO::endl;
      stat = 3;
    }

    if (vecScopes.size() != 2)
    {
      UtIO::cerr() << "decompPath failed for vectorScope1 - wrong number of scopes" << UtIO::endl;
      stat = 3;
    }
    else if (tokInfo.getType() != HdlId::eVectBitRange)
    {
      UtIO::cerr() << "decompPath failed for vectorScope1 - leaf dimension incorrect" << UtIO::endl;
      stat = 3;
    }
    else
    {
      vpath.compPath(vecScopes, &tokBuf, &tokInfo);
      if (strcmp(tokBuf.c_str(), "scope[3].blah[40:2]") != 0)
      {
        UtIO::cerr() << "decompPath for compPath for vectorScope1 failed - resultant name incorrect" << UtIO::endl;
        stat = 3;
      }
    }

    vecScopes.clear();
    if (vpath.decompPath(vectorScope2, &vecScopes, &tokInfo) != HdlHierPath::eLegal)
    {
      UtIO::cerr() << "decompPath for vectorScope2 failed" << UtIO::endl;
      stat = 3;
    }

    if (vecScopes.size() != 3)
    {
      UtIO::cerr() << "decompPath failed for vectorScope2 - wrong number of scopes" << UtIO::endl;
      stat = 3;
    }
    else if (tokInfo.getType() != HdlId::eVectBit)
    {
      UtIO::cerr() << "decompPath failed for vectorScope2 - leaf dimension incorrect" << UtIO::endl;
      stat = 3;
    }
    else
    {
      vpath.compPath(vecScopes, &tokBuf, &tokInfo);
      if (strcmp(tokBuf.c_str(), "scope[1:4].inst[0].mama[2]") != 0)
      {
        UtIO::cerr() << "decompPath for compPath for vectorScope2 failed - resultant name incorrect" << UtIO::endl;
        stat = 3;
      }
    }

    HdlHierPath::StrAtomVec vecAtomScopes;
    strCache.intern("scope[1:4]");
    strCache.intern("inst[0]");
    strCache.intern("mama");
    if (vpath.decompPathAtom(vectorScope2, &vecAtomScopes, &strCache, &tokInfo) != HdlHierPath::eLegal)
    {
      UtIO::cerr() << "decompPathAtom for vectorScope2 failed" << UtIO::endl;
      stat = 3;
    }

    if (vecAtomScopes.size() != 3)
    {
      UtIO::cerr() << "decompPathAtom failed for vectorScope2 - wrong number of scopes" << UtIO::endl;
      stat = 3;
    }
    else if (tokInfo.getType() != HdlId::eVectBit)
    {
      UtIO::cerr() << "decompPathAtom failed for vectorScope2 - leaf dimension incorrect" << UtIO::endl;
      stat = 3;
    }
    else
    {
      vpath.compPathAtom(vecAtomScopes, &tokBuf, &tokInfo);
      if (strcmp(tokBuf.c_str(), "scope[1:4].inst[0].mama[2]") != 0)
      {
        UtIO::cerr() << "decompPathAtom for compPath for vectorScope1 failed - resultant name incorrect" << UtIO::endl;
        stat = 3;
      }
    }
  
    // parse name
    UtStringArray parseNameVec;
    if (vpath.parseName(&vectorScope1, &parseNameVec, &tokInfo) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "parseName for vectorScope1 failed" << UtIO::endl;
      stat = 3;
    }
    else
    {
      vpath.compPath(parseNameVec, &tokBuf, &tokInfo);
      if (strcmp(tokBuf.c_str(), "scope[3].blah[40:2]") != 0)
      {
        UtIO::cerr() << "parseName for vectorScope1 incorrect" << UtIO::endl;
        stat = 3;
      }
    }
    
    // parseToken
    if (vpath.parseToken(&vectorScope2, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "scope[1:4] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "scope[1:4]") != 0)
    {
      UtIO::cerr() << "scope[1:4] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }
    
    if (tokInfo.getType() != HdlId::eVectBitRange)
    {
      UtIO::cerr() << "scope[1:4] token parse created incorrect info" << UtIO::endl;
      stat = 3;
      
    }

    if (vpath.parseToken(&vectorScope2, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "inst[0] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "inst[0]") != 0)
    {
      UtIO::cerr() << "inst[0] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }

    if (tokInfo.getType() != HdlId::eVectBit)
    {
      UtIO::cerr() << "inst[0] token parse created incorrect info" << UtIO::endl;
      stat = 3;

    }


    if (vpath.parseToken(&vectorScope2, &tokBuf, &tokInfo, true) == HdlHierPath::eIllegal)
    {
      UtIO::cerr() << "mama[2] token parse failed" << UtIO::endl;
      stat = 3;
    }

    if (strcmp(tokBuf.c_str(), "mama[2]") != 0)
    {
      UtIO::cerr() << "mama[2] token parse created incorrect token" << UtIO::endl;
      stat = 3;
    }

    if (tokInfo.getType() != HdlId::eVectBit)
    {
      UtIO::cerr() << "mama[2] token parse created incorrect info" << UtIO::endl;
      stat = 3;

    }
    
  }
  
  // decomppath with trailing .
  {
    HdlId tokInfo;
    const char* basePath = "top.down.";
    UtStringArray moreIds;
    if (vpath.decompPath(basePath, &moreIds, &tokInfo) != HdlHierPath::eLegal)
    {
      UtIO::cerr() << "DecompPath failed to legally parse top.down." << UtIO::endl;
      stat = 3;
      
    }

    if (moreIds.size() != 2)
    {
      UtIO::cerr() << "DecompPath failed to parse top.down. - wrong size vector" << UtIO::endl;
      stat = 3;
    }
    else
    {
      if ((strcmp(moreIds[0], "top") != 0) ||
          (strcmp(moreIds[1], "down") != 0))
      {
        UtIO::cerr() << "DecompPath failed to parse top.down. - Incorrect name" << UtIO::endl;
        stat = 3;
      }
    }
  }

  // check for wildcards
  
  UtIO::cout() << "WILDCARDS" << UtIO::endl;
  vpath.putAllowWildcard(true);
  nl = names.begin();
  {
    while (nl != names.end())
    {
      UtStringArray theList;
      if (vpath.decompPath(*nl, &theList, &info) == HdlHierPath::eLegal)
      {
        UtStringArray::iterator p = theList.begin();
        while (p != theList.end())
        {
          UtIO::cout() << *p << '\n';
          p++;
        }

        // now compose
        vpath.compPath(theList, &tmp);
        UtIO::cout() << "compose: " << tmp << '\n';
        if (tmp.compare(*nl) != 0)
        {
          UtIO::cout() << "Composition failed\n";
          stat = 3;
        }
      }
      else
      {
        UtIO::cerr() << "Parsing not done on a legal or wildcarded name?" << UtIO::endl;
        stat = 3;
      }
      
      nl++;
      i++;
    }
  }

  
  return stat;
}
