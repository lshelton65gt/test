// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtIOStream.h"
#include <cstdio>
#include "hdl/HdlFileSystem.h"
#include "hdl/HdlVerilogOFileSystem.h"
#include "hdl/HdlVerilogString.h"
#include "util/BitVectorStream.h" 
#include "util/MsgContext.h"
#include "util/ShellMsgContext.h"
#include "util/OSWrapper.h"

#include "util/CarbonTypes.h"

int main()
{
  
  MsgStreamIO* errStream = new MsgStreamIO(stderr, true);
  MsgContext* msgContext = new MsgContext;
  msgContext->addReportStream(errStream);

  UtIO::cerr() << "START: hdl file system.\n";
  HDLFileSystem* hdl_file_system = new HDLFileSystem(msgContext);
  // attempt to reserve a single descriptor,
  
  if ( hdl_file_system->reserveFileDescriptor(0x80000001U,  NULL,           &(UtIO::cerr())) ){
    UtIO::cerr() << "TEST1: PASS - able to reserve a descriptor\n";
  } else {
    UtIO::cerr() << "TEST1: FAIL - unable to reserve a descriptor\n";
  }
  // now get stream back from hdlFileSystem
  UtOStream * myout = NULL;
  if ( hdl_file_system->getHdlFileStream(&(myout), 0x80000001U) ){
    (*myout) << "TEST2: PASS - Able to retrieve a descriptor and use it\n";
  } else {
    UtIO::cerr() << "TEST2: FAIL - unable to retrieve descriptor and use it.\n";
  }

  // now try to reserve the same descriptor with a different stream,
  // this should generate an error
  UtIO::cerr() << "note:   The next line should show an error message\n";
  if (hdl_file_system->reserveFileDescriptor(0x80000001U,  NULL,           &(UtIO::cout()))) {
    UtIO::cerr() << "TEST3: FAIL - able to reserve a used descriptor with a different stream(this should have generated an error)\n";
  } else {
    UtIO::cerr() << "TEST3: PASS - unable to reserve a used descriptor with a different stream.\n";
  }
 
  // now try to reserve the same descriptor with the same stream, no error expected
  if ( hdl_file_system->reserveFileDescriptor(0x80000001U,  NULL,           &(UtIO::cerr()))) {
    UtIO::cerr() << "TEST4: PASS - able to reserve a used descriptor with its original stream\n";
  } else {
    UtIO::cerr() << "TEST4: FAIL - unable to reserve a used descriptor with its original stream\n";
  }

  // now try to reserve a different descriptor with a new stream, no error expected
  if ( hdl_file_system->reserveFileDescriptor(0x3U,  NULL,           &(UtIO::cout()))) {
    UtIO::cerr() << "TEST5: PASS - able to reserve a new descriptor with new stream\n";
  } else {
    UtIO::cerr() << "TEST5: FAIL - unable to reserve a new descriptor with new stream\n";
  }

  // now unreserve
  if ( hdl_file_system->unReserveFileDescriptor(0x80000001U)) {
    UtIO::cerr() << "TEST6: PASS - able to unreserve a descriptor\n";
  } else {
    UtIO::cerr() << "TEST6: FAIL - unable to unreserve a descriptor\n";
  }

  // check that it is removed
  myout = NULL;
  if ( hdl_file_system->getHdlFileStream(&(myout), 0x80000001U) ){
    UtIO::cerr() << "TEST7: FAIL - Able to retrieve a descriptor that was unreserved\n";
    (*myout) << "if you see this line we were able to retrive and use a descriptor that was unreserved\n";
  } else {
    UtIO::cerr() << "TEST7: PASS - unable to retrieve a descriptor that was unreserved.\n";
  }

  // reserve again
  if ( hdl_file_system->reserveFileDescriptor(0x80000001U,  NULL,           &(UtIO::cerr())) ){
    UtIO::cerr() << "TEST8: PASS - able to reserve a descriptor that was unreserved\n";
  } else {
    UtIO::cerr() << "TEST8: FAIL - unable to reserve a descriptor that was unreserved\n";
  }

  // reserve a different descriptor with same stream
  if ( hdl_file_system->reserveFileDescriptor(0x80000003U,  NULL,           &(UtIO::cerr())) ){
    UtIO::cerr() << "TEST9: PASS - able to reserve a new descriptor with used stream\n";
  } else {
    UtIO::cerr() << "TEST9: FAIL - unable to reserve a new descriptor with used stream\n";
  }

  // reserve an input stream
  if ( hdl_file_system->reserveFileDescriptor(0x0U,  &(UtIO::cin()), NULL)){
    UtIO::cerr() << "TEST10: PASS - able to reserve an input stream\n";
  } else {
    UtIO::cerr() << "TEST10: FAIL - unable to reserve an input stream\n";
  }

  // reserve an input and output stream
  if ( hdl_file_system->reserveFileDescriptor(0x0U,  &(UtIO::cin()), &(UtIO::cerr()))){
    UtIO::cerr() << "TEST11: PASS - able to re reserve an input/output stream combination\n";
  } else {
    UtIO::cerr() << "TEST11: FAIL - unable to re reserve an input/output stream combination\n";
  }
  
  
  UtIO::cerr() << "END:   hdl file system.\n";
}

