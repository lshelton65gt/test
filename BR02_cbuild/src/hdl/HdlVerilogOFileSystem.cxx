// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file 
  Classes releated to file operations used within verilog simulations.
  Such as for $fopen and $fdisplay.  Includes support for multi channel descriptors.
*/
#include <cassert>
#include "util/ShellMsgContext.h"
#include "util/UtIOStream.h"
#include "util/Loop.h"
#include "util/UtStreamSupport.h"
#include "hdl/HdlVerilogOFileSystem.h"


VerilogOutFileSystem::VerilogOutFileSystem(HDLFileSystem* fileSystem, MsgContext* msgContext) :
  mHDLFileSystem(fileSystem),
  mMsgContext(msgContext)
{

  // reserve stdout, stderr, stdin file descriptors  in this verilog file system.

  mHDLFileSystem->reserveFileDescriptor(eMCDstdin,  &(UtIO::cin()), NULL           );
  mHDLFileSystem->reserveFileDescriptor(eMCDstdout, NULL,           &(UtIO::cout()));
  mHDLFileSystem->reserveFileDescriptor(eFDstdin,   &(UtIO::cin()), NULL);
  mHDLFileSystem->reserveFileDescriptor(eFDstdout,  NULL,           &(UtIO::cout()));
  mHDLFileSystem->reserveFileDescriptor(eFDstderr,  NULL,           &(UtIO::cerr()));
  mTargetFileDescriptor = 0;    // a value of 0 is eMCDstdin which
                                // cannot be used for output so
                                // mTargetFileDescriptor and
                                // mTargetFileDescriptors are now in
                                // sync (empty)
}

VerilogOutFileSystem::~VerilogOutFileSystem()
{
  close( );                      // close all open files
  // unreserve stdout, stderr, stdin file descriptors  in this verilog file system.
  mHDLFileSystem->unReserveFileDescriptor(eMCDstdin);
  mHDLFileSystem->unReserveFileDescriptor(eMCDstdout);
  mHDLFileSystem->unReserveFileDescriptor(eFDstdin);
  mHDLFileSystem->unReserveFileDescriptor(eFDstdout);
  mHDLFileSystem->unReserveFileDescriptor(eFDstderr);
}
 
bool VerilogOutFileSystem::is_open() const
{
  return ( mHDLFileSystem->is_open() );
}

// this closes only the files specified by fdToClose (can be either a
// MCD or normal FD), it will not close the reserved files
bool VerilogOutFileSystem::close(UInt32 fdToClose)
{
  bool isMCD = not (fdToClose & sNonMCDFlagBit);
  bool retStatus = true;        // assume all is well
  bool aFileWasClosed = false;

  if ( isMCD )
  {
    // this is a MCD (msb is zero)
    UInt32 descriptors_remaining = fdToClose;
    for (UInt32 individualFD = 1;
         (descriptors_remaining != 0) and (individualFD != sNonMCDFlagBit);
         individualFD = individualFD << 1)
    {
      if ( individualFD & descriptors_remaining ) {
        descriptors_remaining = descriptors_remaining & ~individualFD; // mask out this bit
        
        if ( mHDLFileSystem->isReservedFD( individualFD ) ) {
          continue;               // do not attempt to close a reserved files
        }
        retStatus &= mHDLFileSystem->close(individualFD);
        aFileWasClosed = true;
      }
    }
  }
  else
  {
    // this is a file descriptor, (because MSB is 1)
    if ( not mHDLFileSystem->isReservedFD( fdToClose ) ) {
      retStatus &= mHDLFileSystem->close(fdToClose);
      aFileWasClosed = true;
    }
  }
  if ( aFileWasClosed ) {
    // force next reference to setup the mTargetFileDescriptors
    mTargetFileDescriptor = 0;    
    mTargetFileDescriptors.clear();
  }
  return retStatus;
}



// this flushes only the files specified by fdToFlush (can be either a MCD or normal FD)
bool VerilogOutFileSystem::flush(UInt32 fdToFlush)
{
  bool isMCD = not (fdToFlush & sNonMCDFlagBit);
  bool retStatus = true;        // assume all is well

  if ( isMCD )
  {
    UInt32 descriptors_remaining = fdToFlush;
    // this is a MCD (msb is zero)
    for (UInt32 individualFD = 1;
         (descriptors_remaining != 0) and (individualFD != sNonMCDFlagBit);
         individualFD = individualFD << 1)
    {
      if ( individualFD & fdToFlush ) {
        descriptors_remaining = descriptors_remaining & ~individualFD; // mask out this bit

        retStatus &= mHDLFileSystem->flush(individualFD);
      }
    }
  } else {
    // this is a file descriptor, (because MSB is 1)
    // only one file involved, flush it
    if ( not mHDLFileSystem->isReservedFD( fdToFlush ) ) {
      retStatus &= mHDLFileSystem->flush(fdToFlush);
    }
  }
  return retStatus;
}






bool VerilogOutFileSystem::write(const char* buf, UInt32 len)
{
  typedef CLoop<TargetFileDescriptors> TargetFileDescriptorsCLoop;

  bool returnForAll = true;
  // writes buf to all files that are currently marked for writing in
  // the VerilogOutFileSystem
  for (TargetFileDescriptorsCLoop i(mTargetFileDescriptors); not i.atEnd(); ++i)
  {
    UtOStream * stream = *i;

    bool ret = stream->write(buf, len);
    if (! ret)
    {
      const char * filename = stream->getFilename();
      if (filename == NULL) filename = "stdout||stderr";
      getMsgContext()->SHLFailedToWrite(filename, stream->getErrmsg());
    }
    returnForAll &= ret;
  }

  return returnForAll;
}

void VerilogOutFileSystem::putTargetFileDescriptor ( UInt32 descriptor )
{
  if ( descriptor == mTargetFileDescriptor )
  {
    // nothing to do, mTargetFileDescriptors is already setup to use
    // this descriptor.
    return;
  }
  mTargetFileDescriptors.clear();

  bool isMCD = not (descriptor & sNonMCDFlagBit);
  if ( isMCD )
  {
    UInt32 descriptors_remaining = descriptor;
    // this is a MCD (msb is zero)
    for (UInt32 individualFD = 1;
         (descriptors_remaining != 0) and (individualFD != sNonMCDFlagBit);
         individualFD = individualFD << 1)
    {
      if ( individualFD & descriptor )
      {
        descriptors_remaining = descriptors_remaining & ~individualFD; // mask out this bit
        // add the stream associated with this bit of MCD to
        // mTargetFileDescriptors
        UtOStream * out_stream = NULL;
        bool is_defined = mHDLFileSystem->getHdlFileStream(&out_stream, individualFD);
        if ( is_defined && out_stream){
          mTargetFileDescriptors.push_back(out_stream);
        }
      }
    }
  }
  else
  {
    // this is a file descriptor, (because MSB is 1)
    // only one file involved add the file associated with this file
    // descriptor to mTargetFileDescriptors
    UtOStream * out_stream = NULL;

    bool is_defined = mHDLFileSystem->getHdlFileStream(&out_stream, descriptor);
    if ( is_defined && out_stream ){
      mTargetFileDescriptors.push_back(out_stream);
    }
  }
  mTargetFileDescriptor = descriptor;
}

UInt32 VerilogOutFileSystem::VerilogOFileOpen (const char* filename, const char* mode)
{
  // rjc-todo really should check that mode is any valid form of write
  if ( not (0 == strcmp(mode, "w")) )
  {
    getMsgContext()->SHLUnableToOpenFileWithMode(filename, mode);
    return eMCDstdin;          // an invalid value for writing
  }   

  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(filename, mode, true, false);

  return (this_descriptor);

}

UInt32 VerilogOutFileSystem::VerilogOFileOpen (const char* filename)
{
  char mode [4] = "w";

  HDLFD this_descriptor = mHDLFileSystem->HdlFileOpen(filename, mode, true, true);

  return (this_descriptor);
}


