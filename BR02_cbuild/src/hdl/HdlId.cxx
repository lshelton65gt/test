// -*- C++ -*-                
/*****************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "hdl/HdlId.h"
#include "util/ConstantRange.h"
#include "util/UtHashSet.h"

class HdlIdFactory::Helper
{
  typedef UtHashSet<HdlId> HdlIdSet;
  
public:
  CARBONMEM_OVERRIDES
  
  Helper() {}
  ~Helper() {}
  
  const HdlId* getId(const HdlId& id)
  {
    std::pair<HdlIdSet::iterator,bool> insertStat = mIds.insert(id);
    HdlIdSet::iterator p = insertStat.first;
    const HdlId& val = *p;
    return &val;
  }

private:
  HdlIdSet mIds;
};

void HdlId::setVectorParams(const ConstantRange& range)
{
  setType(eVectBitRange);
  setVectMSB(range.getMsb());
  setVectLSB(range.getLsb());
}

void HdlId::addDimension(SInt32 index)
{
  if (mType == eArrayIndex) {
    // This is already an array index type, so just add to it.
    INFO_ASSERT(mIndices != NULL, "Index array not allocated");
    // Reallocate array space if necessary.
    if (mVect.uNumDims == mIndicesAllocated) {
      UInt32 newSize = mIndicesAllocated * 2;
      SInt32 *newArray = CARBON_ALLOC_VEC(SInt32, newSize);
      // Copy the old array
      memcpy(newArray, mIndices, mIndicesAllocated * sizeof(SInt32));
      CARBON_FREE_VEC(mIndices, SInt32, mIndicesAllocated);
      mIndices = newArray;
      mIndicesAllocated = newSize;
    }
    // Save the index and update the dimensions
    mIndices[mVect.uNumDims] = index;
    ++mVect.uNumDims;
  } else if (mType == eVectBit) {
    // This is currently a bitselect, but we're now going to interpret
    // that as the first of two (or more) array indices.  To store
    // this, we need to ensure an array is allocated.  It might
    // already be there if this HdlId is being reused.
    if (mIndices == NULL) {
      // Allocate just enough to start.  We'll grow the array as
      // needed.
      mIndicesAllocated = 2;
      mIndices = CARBON_ALLOC_VEC(SInt32, mIndicesAllocated);
    }
    // Copy old bitselect into an index, as well as the new one
    mIndices[0] = mVect.uIndex;
    mIndices[1] = index;
    // Set the number of dimensions.  Note that we must do this last
    // because it shares storage in the union with the previous index.
    mVect.uNumDims = 2;
    mType = eArrayIndex;
  } else {
    INFO_ASSERT(0, "Can't add dimension to this type");
  }
}

int HdlId::compare(const HdlId* a, const HdlId* b)
{
  int diff = a->mType - b->mType;
  if (diff == 0)
  {
    // types are the same, if scalar return 0;
    switch (a->mType)
    {
    case eScalar: diff = 0; break;
    case eInvalid: INFO_ASSERT(0,"a->mType is invalid"); break;
    case eVectBit:
      // compare bit indices
      diff = a->getVectIndex() - b->getVectIndex();
      break;
    case eVectBitRange:
      // Compare the size of the range first.
      diff = (int)a->getWidth() - (int)b->getWidth();
      if (diff == 0)
      {
        // compare the msb
        diff = a->getVectMSB() - b->getVectMSB();
        // it's possible that the msb's are the same and the direction
        // is different
        if (diff == 0)
          diff = a->getVectLSB() - b->getVectLSB();
      }
      break;
    case eArrayIndex:
      // Compare the number of dimensions first.
      diff = (int)a->getNumDims() - (int)b->getNumDims();
      for (UInt32 i = 0; (diff == 0) && (i < a->getNumDims()); ++i) {
        diff = (int)a->getDim(i) - (int)b->getDim(i);
      }
      break;
    }
  }
  
  // shrink the diff value
  if (diff > 0)
    diff = 1;
  else if (diff < 0)
    diff = -1;
  return diff;
}


HdlIdFactory::HdlIdFactory()
{
  mHelper = new Helper;
}

HdlIdFactory::~HdlIdFactory()
{
  delete mHelper;
}

const HdlId* HdlIdFactory::getHdlId(const HdlId& id)
{
  return mHelper->getId(id);
}

