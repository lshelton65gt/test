// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "hdl/HdlVerilogDist.h"
#include "util/CarbonAssert.h"
#include <climits> 
#include <cmath>


SInt32 HdlVerilogDist::ChiSquare(SInt32 *seed, SInt32 df)
{ 
  double r; 
  SInt32 i; 

  INFO_ASSERT((df > 0), "The degree of freedom for the ChiSquare function must be greater than 0");

  r = InternalChiSquare(seed,df); 
  if (r >= 0) 
  { 
    i = (SInt32)(r + 0.5); 
  } 
  else 
  { 
    r = -r; 
    i = (SInt32)(r + 0.5); 
    i = -i; 
  } 

  return i; 
} 


SInt32 HdlVerilogDist::Erlang(SInt32 *seed, SInt32 k, SInt32 mean)
{ 
  double r; 
  SInt32 i; 

  INFO_ASSERT((k > 0), "The mean for the Erlang function must be greater than 0");

  r = InternalErlangian(seed, k, mean); 
  if (r >= 0) 
  { 
    i = (SInt32)(r + 0.5); 
  } 
  else 
  { 
    r = -r; 
    i = (SInt32)(r + 0.5); 
    i = -i; 
  } 

  return i; 
} 


SInt32 
HdlVerilogDist::Exponential(SInt32 *seed, SInt32 mean)
{ 
  double r; 
  SInt32 i; 

  INFO_ASSERT((mean > 0), "The mean for the Exponential function must be greater than 0");

  r = InternalExponential(seed, mean);
  if (r >= 0) 
  { 
    i = (SInt32)(r + 0.5); 
  } 
  else 
  { 
    r = -r; 
    i = (SInt32)(r + 0.5); 
    i = -i; 
  } 

  return i; 
} 


SInt32 
HdlVerilogDist::Normal(SInt32 *seed, SInt32 mean, SInt32 sd)
{ 
  double r; 
  SInt32 i; 

  r = InternalNormal(seed, mean, sd); 
  if (r >= 0) 
  { 
    i = (SInt32)(r + 0.5); 
  } 
  else 
  { 
    r = -r; 
    i = (SInt32)(r + 0.5); 
    i = -i; 
  } 

  return i; 
} 


SInt32 
HdlVerilogDist::Poisson(SInt32 *seed, SInt32 mean)
{ 
  SInt32 i; 

  INFO_ASSERT((mean > 0), "The mean for the Poisson function must be greater than 0");
  i = InternalPoisson(seed, mean); 

  return i; 
} 


SInt32 
HdlVerilogDist::T(SInt32 *seed, SInt32 df)
{ 
  double r; 
  SInt32 i; 

  INFO_ASSERT((df > 0), "The degree of freedom for the T function must be greater than 0");

  r = InternalT(seed, df); 
  if (r >= 0) 
  { 
    i = (SInt32)(r + 0.5); 
  } 
  else 
  { 
    r = -r; 
    i = (SInt32)(r + 0.5); 
    i = -i; 
  } 

  return i; 
} 


SInt32 HdlVerilogDist::Uniform(SInt32 *seed, SInt32 start, SInt32 end)
{ 
  double r; 
  SInt32 i; 
         
  if (start >= end)
  {
    return start; 
  }

  if (end != UtSINT32_MAX) 
  { 
    end++; 
    r = InternalUniform(seed, start, end); 
    if (r >= 0) 
    { 
      i = (SInt32) r; 
    } 
    else 
    { 
      i = (SInt32) (r - 1); 
    } 
    if (i < start)
    {
      i = start;
    }
    if (i >= end)
    {
      i = end - 1; 
    }
  } 
  else if (start != UtSINT32_MIN)
  { 
    start--; 
    r = InternalUniform(seed, start, end) + 1.0; 
    if (r >= 0) 
    { 
      i = (SInt32) r; 
    } 
    else 
    { 
      i = (SInt32) (r - 1); 
    } 
    if (i <= start)
    {
      i = start + 1;
    }
    if (i > end)
    {
      i = end; 
    }
  } 
  else 
  { 
    r = (InternalUniform(seed, start, end) + 2147483648.0) / 4294967295.0; 
    r = r * 4294967296.0 - 2147483648.0; 
    if (r >= 0) 
    { 
      i = (SInt32) r; 
    } 
    else 
    { 
      i = (SInt32) (r - 1); 
    } 
  } 

  return i; 
} 


SInt32 HdlVerilogDist::Random(SInt32 *seed)
{ 
  return Uniform(seed, UtSINT32_MIN, UtSINT32_MAX);
}


double 
HdlVerilogDist::InternalUniform(SInt32 *seed, SInt32 start, SInt32 end)
{ 
  union u_s 
  { 
    float s; 
    unsigned stemp;  // !!!
  } u; 


  double d = 0.00000011920928955078125; 
  double a; 
  double b; 
  double c; 

  if ((*seed) == 0) 
  {
    *seed = 259341593; 
  }    

  if (start >= end) 
  { 
    a = 0.0; 
    b = 2147483647.0; 
  } 
  else 
  { 
    a = (double) start; 
    b = (double) end; 
  } 

  *seed = 69069 * (*seed) + 1; 
  u.stemp = *seed; 

  /* 
   * This relies on IEEE floating point format 
   */ 
  u.stemp = (u.stemp >> 9) | 0x3f800000; 
  
  c = (double) u.s; 

  c = c + (c * d); 
  c = ((b - a) * (c - 1.0)) + a; 

  return c; 
} 


double 
HdlVerilogDist::InternalNormal(SInt32 *seed, SInt32 mean, SInt32 deviation) 
{ 
  double v1=0.0;
  double v2;
  double s; 

  s = 1.0; 
  while((s >= 1.0) || (s == 0.0)) 
  {
    v1 = InternalUniform(seed, -1, 1); 
    v2 = InternalUniform(seed, -1, 1); 
    s = v1 * v1 + v2 * v2; 
  } 
  s = v1 * std::sqrt(-2.0 * std::log(s) / s); 
  v1 = (double) deviation; 
  v2 = (double) mean; 

  return (s * v1 + v2); 
} 


double 
HdlVerilogDist::InternalExponential(SInt32 *seed, SInt32 mean) 
{ 
  double n; 

  n = InternalUniform(seed, 0, 1); 
  if(n != 0) 
  { 
    n = -std::log(n) * mean; 
  } 

  return n; 
} 


SInt32 
HdlVerilogDist::InternalPoisson(SInt32 *seed, SInt32 mean) 
{ 
  SInt32 n; 
  double p;
  double q; 

  n = 0; 
  q = -(double)mean; 
  p = std::exp(q); 
  q = InternalUniform(seed, 0, 1); 
  while(p < q) 
  { 
    n++; 
    q = InternalUniform(seed, 0, 1) * q; 
  } 

  return n; 
} 


double 
HdlVerilogDist::InternalChiSquare(SInt32 *seed, SInt32 deg_of_free) 
{ 
  double x;

  if (deg_of_free % 2)
  {
    x = InternalNormal(seed, 0, 1);
    x = x * x;
  }
  else
  {
    x = 0.0;
  }

#if 0
  /*
  ** This is what the spec says.  It is almost certainly
  ** not correct.
  */
  double n;
  n = InternalUniform(seed, 0, 1);
  if (n != 0)
  {
    n = -std::log(n) * mean;
  }

  return n;
#else
  /*
  ** I found this from another source on the internet.
  */
  SInt32 k;
  for (k = 2; k <= deg_of_free; k = k + 2)
  {
    x = x + 2 * InternalExponential(seed, 1);
  }

  return x;
#endif
} 


double 
HdlVerilogDist::InternalT(SInt32 *seed, SInt32 deg_of_free) 
{ 
  double x; 
  double chi2 = InternalChiSquare(seed, deg_of_free); 
  double div = chi2 / (double)deg_of_free; 
  double root = std::sqrt(div); 
  
  x = InternalNormal(seed, 0, 1) / root; 

  return x; 
} 


double 
HdlVerilogDist::InternalErlangian(SInt32 *seed, SInt32 k, SInt32 mean) 
{ 
  double x;
  double a;
  double b;
  SInt32 i; 

  x = 1.0; 
  for(i = 1; i <= k; i++) 
  { 
    x = x * InternalUniform(seed, 0, 1); 
  } 
  a = (double)mean; 
  b = (double)k; 
  x = -a * std::log(x) / b; 

  return x; 
} 
