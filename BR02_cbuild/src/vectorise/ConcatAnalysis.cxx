// -*-C++-*-    $Revision: 1.5 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "reduce/Inference.h"
#include "reduce/REUtil.h"
#include "util/ArgProc.h"
#include "util/UtIOStream.h"
#include "util/UtIndent.h"
#include "util/GraphSCC.h"
#include "util/GraphWCC.h"
#include "vectorise/Collapser.h"
#include "vectorise/ConcatAnalysis.h"

/*! \file ConcatAnalysis.cxx
 *  Implementation of class ConcatAnalysis.
 */  

// Discovery
// *********

/*! \note Discovery uses the the Collapser pattern matcher to seek potentially
 *  concat adjacent nets. A walk of the module looks for statement sequences
 *  and attempts to group and match statements that will vectorise
 *  together. For each such group, the collapser extract method uses
 *  maybeAddEdge to add edges to the netmap.
 *
 *  \note If a collapser matcher cannot be constructed for a statement, or if
 *  the matcher does not match any other statements, then the statement is
 *  directly walked looking for concats, either as lvalues or as
 *  expressions. Whole identifiers occurring in consecutive positions in these
 *  concats induce edges in the graph.
 */

/*! \class StmtListCallback
 *  \brief Walk the module looking statement list that are then analysed for
 *  concat adjacency
 */

class StmtListCallback : public NUDesignCallback {
public:
  StmtListCallback (ConcatAnalysis &ca, NUNetRefFactory *netref_factory, 
    NUModule *module, ConcatAnalysis::NetMap *netmap) : 
    NUDesignCallback (), mConcatAnalysis (ca), mNetRefFactory (netref_factory),
    mModule (module), mNetMap (netmap), mChanged (false)
  { pushScope (module); }

  virtual ~StmtListCallback () {}

  bool isChanged () const { return mChanged; }

private:

  ConcatAnalysis &mConcatAnalysis;      //!< the port vectoriser context
  NUNetRefFactory *mNetRefFactory;      //!< the net reference factory
  NUModule *mModule;                    //!< module under discovery
  ConcatAnalysis::NetMap *mNetMap;      //!< graph construct during the design walk

  // StmtListCallback looks for statement lists and passes them to ConcatAnalysis
  // for scanning

  virtual Status operator()(Phase, NUDesign *) { return eNormal; }
  virtual Status operator()(Phase, NUBase *) { return eSkip; }
  virtual Status operator()(Phase, NUNamedDeclarationScope *) { return eNormal; }
  virtual Status operator()(Phase, NUStructuredProc *) { return eNormal; }
  virtual Status operator()(Phase, NUStmt *) { return eSkip; }
  virtual Status operator()(Phase, NUCase *) { return eNormal; }

  //! NUModule handler
  virtual Status operator () (Phase phase, NUModule *module)
  {
    if (phase == NUDesignCallback::ePre) {
      // walk the continuous statements
      NUContAssignList cont_assigns;
      NUStmtList stmts;
      module->getContAssigns (&cont_assigns);

      if (cont_assigns.empty ()) {
        return eNormal;                   // uninteresting...
      }

      // copy cont. assigns into a vector so we can use a custom sort method.
      NUContAssignVector cont_vector;
      for (NUContAssignList::iterator it = cont_assigns.begin (); it != cont_assigns.end (); it++) {
        cont_vector.push_back (*it);
      }

      LessThanAssign lt (mNetRefFactory);
      std::sort (cont_vector.begin (), cont_vector.end (), lt);

      // copy stmts into a stmtlist so it looks like all other stmtlists.
      NUStmtList cont_stmts;
      for (NUContAssignVector::iterator it = cont_vector.begin(); it != cont_vector.end (); it++) {
        cont_stmts.push_back (*it);
      }

      mChanged |= mConcatAnalysis.discovery (getScope (), cont_stmts, mNetMap);
    }
    return eNormal;
  }

  //! NUTF handler
  virtual Status operator () (Phase, NUTF *)
  {
    return eSkip;
  }

  //! NUBlock handler
  virtual Status operator () 
    (Phase phase, NUBlock *block)
  {
    if (phase == NUDesignCallback::ePre) {
      pushScope (block);
      NUStmtList *stmts = block->getStmts ();
      mChanged |= mConcatAnalysis.discovery (getScope (), *stmts, mNetMap);
      delete stmts;
    } else if (phase == NUDesignCallback::ePost) {
      popScope ();
    } else {
      NU_ASSERT (false, block);           // weird...
    }
    return eNormal;
  }

  //! NUCaseItem handler
  virtual Status operator () 
    (Phase phase, NUCaseItem *case_item)
  {
    if (phase == ePre) {
      NUStmtList *stmts = case_item->getStmts ();
      mChanged |= mConcatAnalysis.discovery (getScope (), *stmts, mNetMap);
      delete stmts;
    }
    return eNormal;
  }

  //! NUIf handler
  virtual Status operator () 
    (Phase phase, NUIf *if_stmt)
  {
    if (phase == ePre) {
      NUStmtList *true_branch = if_stmt->getThen ();
      NUStmtList *false_branch = if_stmt->getElse ();
      mChanged |= mConcatAnalysis.discovery (getScope (), *true_branch, mNetMap);
      mChanged |= mConcatAnalysis.discovery (getScope (), *false_branch, mNetMap);
      delete true_branch;
      delete false_branch;
    }
    return eNormal;
  }

  //! NUFor handler
  virtual  Status operator () 
    (Phase phase, NUFor *for_stmt)
  {
    if (phase == ePre) {
      NUStmtList *stmts = for_stmt->getBody ();
      mChanged |= mConcatAnalysis.discovery (getScope (), *stmts, mNetMap);
      delete stmts;
    }
    return eNormal;
  }

  //! Push a scope onto the stack.
  void pushScope(NUScope * scope) { mScopes.push(scope); }

  //! Pop a scope off the stack.
  void popScope() { mScopes.pop(); }

  //! Get the top of the stack.
  NUScope *getScope() const { return mScopes.top(); }

  UtStack <NUScope *> mScopes;
  bool mChanged;
};

/*! \class ConcatCallback
 *  \brief Walk the module looking statement list that are then analysed for
 *  concat adjacency
 */

class ConcatCallback : public NUDesignCallback {
public:
  ConcatCallback (ConcatAnalysis &ca, NUModule *module, ConcatAnalysis::NetMap *netmap) : 
    NUDesignCallback (), mConcatAnalysis (ca), mModule (module), mNetMap (netmap), mNewEdges (false)
  {}

  virtual ~ConcatCallback () {}

  bool newEdges () const { return mNewEdges; }

private:

  ConcatAnalysis &mConcatAnalysis;      //!< the concat discovery instance
  NUModule *mModule;                    //!< module under discovery
  ConcatAnalysis::NetMap *mNetMap;      //!< graph constructed during the design walk
  bool mNewEdges;                       //!< set when new edges are created

  virtual Status operator () (Phase, NUBase *) { return eNormal; }

  virtual Status operator () (Phase phase, NUConcatLvalue *concat)
  {
    if (phase == ePre) {
      NUNet *previous = NULL;
      for (NULvalueLoop loop = concat->loopLvalues (); !loop.atEnd (); ++loop) {
        NULvalue *lvalue = *loop;
        NUNet *net;
        if (!lvalue->isWholeIdentifier ()) {
          // We are looking for scalars, which are whole identifiers.
          previous = NULL;
        } else if ((net = lvalue->getWholeIdentifier ()) == NULL) {
          // Spooky... getWholeIdentifier should always give me a net for whole identifiers
          NU_ASSERT (!lvalue->isWholeIdentifier () || lvalue->getWholeIdentifier () != NULL, lvalue);
        } else if (!mConcatAnalysis.isInteresting (net)) {
          // It is a scalar, but not an interesting scalar.
          previous = NULL;
        } else if (previous == NULL) {
          previous = net;
        } else {
          ConcatAnalysis::NetMap::Edge *new_edge;
          mNewEdges |= mNetMap->maybeAddEdge (previous, net, &new_edge);
          previous = net;
        }
      }
    }
    return eSkip;                         // do not descend any lower
  }

  virtual Status operator()(Phase phase, NUConcatOp *concat)
  {
    if (phase == ePre) {
      NUNet *previous = NULL;
      for (NUExprLoop loop = concat->loopExprs (); !loop.atEnd (); ++loop) {
        NUExpr *expr = *loop;
        NUNet *net;
        if (!expr->isWholeIdentifier ()) {
          // We are looking for scalar, which will always be whole identifiers
          previous = NULL;
        } else if ((net = expr->getWholeIdentifier ()) == NULL) {
          // should not be NULL is isWholeIdentifier is true
          NU_ASSERT (!expr->isWholeIdentifier () || expr->getWholeIdentifier () != NULL, expr);
        } else if (!mConcatAnalysis.isInteresting (net)) {
          // An uninteresting scalar
          previous = NULL;
        } else if (previous == NULL) {
          previous = net;
        } else {
          ConcatAnalysis::NetMap::Edge *new_edge;
          mNewEdges |= mNetMap->maybeAddEdge (previous, net, &new_edge);
          previous = net;
        }
      }
    }
    return eSkip;                         // do not descend any lower
  }

};

//! construct the net adjacency graph for the module
bool ConcatAnalysis::discovery (NetMap *netmap)
{
  // construct the interesting-net adjacency graph by walking the module,
  // looking for vectorisable concats connecting the interesing nets.
  StmtListCallback callback (*this, mNetRefFactory, mModule, netmap);
  NUDesignWalker walk (callback, false);
  walk.module (mModule);
  return callback.isChanged ();
}

//! construct the net adjacency graph for a list of statements
bool ConcatAnalysis::discovery (NUScope *scope, NUStmtList &stmts, NetMap *netmap)
{
  // Replicate the vectorisation loop from Inference, looking for adjacent
  // statements in the list that can be vectorised. However, rather than
  // constructing the vectorised statement, instead extract the net adjacency
  // relation from the collapser plan and add it to the net map.
  NUStmtList::iterator outer = stmts.begin ();
  Inference::Params params (Inference::eConcatCollapse, mMsgContext);
  bool changed = false;
  while (outer != stmts.end ()) {
    NUStmt *stmt = *outer;
    NUStmtList::iterator first = outer;
    NUAssign *first_assign = dynamic_cast <NUAssign *> (stmt);
    NUAssign *last_assign = first_assign;
    UInt32 length = 1;
    outer++;                            // advance past this statement
    if (first_assign == NULL) {
      continue;                         // not an assignment statement
    }
    // Construct a collapser for this assignment statement
    Collapser collapser (params, scope, mIODB, mMsgContext, first_assign, NULL);
    bool extracted = false;
    if (!collapser.isFailed ()) {
      // now keep walking the list, matching successive statements against the
      // matcher.
      NUStmtList::iterator inner = outer; // next statement in the list
      while (inner != stmts.end ()) {
        NUAssign *next_assign = dynamic_cast <NUAssign *> (*inner);
        if (next_assign == NULL) {
          // not an assignment statement so break out the collapser loop
          break;
        } else if (!collapser.acrete (next_assign)) {
          // this statement does not collapse against the current pattern
          break;
        } else {
          // this statement collapses with all the others since first_assign
          last_assign = next_assign;
          length++;
          inner++;
        }
      }
      if (length > 1) {
        // only extract from the collapser if at least one statement was matched
        note_vectorisation (first, length);
        extracted |= collapser.extract (*this, netmap);
      }
      outer = inner;
    }
    if (collapser.isFailed () || length == 1) {
      // The collapser was unable to construct a matcher plan for this
      // statement, or the second statement did not match, so instead just scan
      // the extant assignment for concats.
      ConcatCallback callback (*this, mModule, netmap);
      NUDesignWalker walk (callback, false);
      walk.assign (first_assign);
      extracted |= callback.newEdges ();
    }
    if (extracted) {
      note_extract (first_assign, last_assign, length, *netmap);
    }
    changed |= extracted;
  }
  return changed;
}

// Implementation of ConcatAnalysis::NetMap
// ****************************************

/*virtual*/ ConcatAnalysis::NetMap::~NetMap ()
{
  NodeMap::iterator it;
  for (it = mNodes.begin (); it != mNodes.end (); it++) {
    delete it->second;
  }
}

//! Locate a node in the graph.
ConcatAnalysis::NetMap::Node *ConcatAnalysis::NetMap::findNode (NUNet *net) const
{
  NodeMap::const_iterator it;
  if ((it = mNodes.find (net)) == mNodes.end ()) {
    return NULL;
  } else {
    return it->second;
  }
}

//! test for an edge between two nets
bool ConcatAnalysis::NetMap::hasEdge (NUNet *a, NUNet *b) const
{
  const Node *anode = findNode (a);
  if (anode == NULL) {
    return false;
  } else {
    return anode->hasEdge (b);
  }
}

//! extract a subgraph of the given graph containing only certain nodes
/*virtual*/ Graph *ConcatAnalysis::NetMap::subgraph (GraphNodeSet &keep_set, bool copy_scratch)
{
  ConcatAnalysis::NetMap *g = new ConcatAnalysis::NetMap (mConcatAnalysis);
  GraphNodeSet::const_iterator old_nodes;
  // create a node in the new graph for each node in the keep set
  for (old_nodes = keep_set.begin (); old_nodes != keep_set.end (); old_nodes++) {
    Node *old_node = static_cast <Node *> (*old_nodes);
    Node *new_node = new Node (*old_node);
    if (copy_scratch) {
      new_node->mScratch = old_node->mScratch;
    }
    g->insert (new_node);
  }
  // create edges for all the edges between nodes in the keep set
  for (old_nodes = keep_set.begin (); old_nodes != keep_set.end (); old_nodes++) {
    Node *old_from = static_cast <Node *> (*old_nodes);
    for (Iter <GraphEdge *> old_edges = edges (old_from); !old_edges.atEnd (); ++old_edges) {
      Edge *old_edge = static_cast <Edge *> (*old_edges);
      Node *old_to = old_edge->mTo;
      if (keep_set.find (old_to) != keep_set.end ()) {
        // an edge between two nodes that we are keeping
        NUNet *from_net = old_from->getNet ();
        NUNet *to_net = old_to->getNet ();
        Node *new_from = g->findNode (from_net);
        Node *new_to = g->findNode (to_net);
        NU_ASSERT (new_from != NULL && new_to != NULL, from_net);
        Edge *new_edge = new Edge (new_from, new_to);
        new_from->insert (new_edge);
        if (copy_scratch) {
          new_edge->mScratch = old_edge->mScratch;
        }
      }
    }
  }
  return g;
}

//! Remove the set of nodes and all edges incident on them from the graph
/*virtual*/ void ConcatAnalysis::NetMap::removeNodes (const GraphNodeSet &doomed)
{
  GraphNodeSet::const_iterator it0;
  for (it0 = doomed.begin (); it0 != doomed.end (); it0++) {
    Node *node = static_cast <Node *> (*it0);
    node->snip ();
    mNodeSet.erase (node);
    mNodes.erase (node->getNet ());
    delete node;
  }
}

//! Scratch the set of nodes and remove all edges incident on them from the graph
/*virtual*/ void ConcatAnalysis::NetMap::scratchNodes (const GraphNodeSet &doomed)
{
  GraphNodeSet::const_iterator it0;
  for (it0 = doomed.begin (); it0 != doomed.end (); it0++) {
    Node *node = static_cast <Node *> (*it0);
    node->snip ();
    node->setClassification (cSCRATCHED);
  }
}

//! Remove all trace of an edge from the graph
void ConcatAnalysis::NetMap::Edge::snip ()
{
  GraphEdgeSet::iterator ref = mFrom->mEdgeSet.find (this);
  Edge::Map::iterator forward_ref = mFrom->mEdges.find (mTo->mNet);
  Edge::Map::iterator back_ref = mTo->mBackEdges.find (mFrom->mNet);
  NU_ASSERT (ref != mFrom->mEdgeSet.end (), mFrom->mNet);
  NU_ASSERT (forward_ref != mFrom->mEdges.end (), mFrom->mNet);
  NU_ASSERT (back_ref != mTo->mBackEdges.end (), mFrom->mNet);
  mFrom->mEdgeSet.erase (ref);
  mFrom->mEdges.erase (forward_ref);
  mTo->mBackEdges.erase (back_ref);
}

//! Remove a set of edges
void ConcatAnalysis::NetMap::removeEdges (const Edge::Set &doomed)
{
  // The edge must be removed from mEdges and mEdgeSet of the source node and
  // mBackEdges of the sink node.
  Edge::Set::const_iterator it;
  for (it = doomed.begin (); it != doomed.end (); it++) {
    Edge *edge = *it;
    edge->snip ();
    delete edge;
  }
}

//! Locate the node corresponding to a net
ConcatAnalysis::NetMap::Node *ConcatAnalysis::NetMap::getNode (NUNet *net)
{
  NodeMap::iterator it;
  if ((it = mNodes.find (net)) == mNodes.end ()) {
    Node *node = new Node (net, mConcatAnalysis.classifyNet (net));
    insert (node);
    return node;
  } else {
    return it->second;
  }
}

//! Insert a graph into an existing map
void ConcatAnalysis::NetMap::insert (Node *node)
{
  mNodes.insert (NodeMap::value_type (node->mNet, node));
  mNodeSet.insert (node);
}

//! Construct a string describing this node.
void ConcatAnalysis::NetMap::compose (UtString *s, const UInt32 options) const
{
  const UInt32 indent = options & cINDENT_MASK;
  s->clear ();
  UInt32 start = s->size ();
  if (empty ()) {
    s->append (indent, ' ');
    *s << "<empty graph>";
  } else {
    // The const_cast is ugly, but the Graph base class and the Iter gymnastics
    // make is more than a little difficult to tangle up the consts...
    s->append (indent, ' ');
    Iter <GraphNode *> outer;
    UInt32 n = 0;
    for (outer = const_cast <ConcatAnalysis::NetMap *> (this)->nodes (); !outer.atEnd (); ++outer) {
      Node *node = static_cast <Node *> (*outer);
      Iter <GraphEdge *> inner = node->edges ();
      for (inner = node->edges (); !inner.atEnd (); ++inner) {
        Edge *edge = static_cast <Edge *> (*inner);
        const char *a = edge->getFrom ()->getNet ()->getName ()->str ();
        const char *b = edge->getTo ()->getNet ()->getName ()->str ();
        UtString chunk;
        chunk << "(" << a << "," << b << ")";
        if ((options & cWEIGHT) && edge->getWeight () > 0) {
          chunk << "=" << edge->getWeight ();
        }
        if (n > 0 && !(options & cLINE)) {
          *s << ",";
        }
        if (chunk.size () + s->size () - start > 79 || ((options & cLINE) && n > 0)) {
          *s << (char) UtIO::endl;
          start = s->size ();
          s->append (indent, ' ');
        } else if (n > 0) {
          *s << " ";
        }
        *s << chunk;
        n++;
      }
    }
    if (s->size () > start) {
      *s << (char) UtIO::endl;
    }
    // enumerate the scratched nodes
    start = s->size ();
    for (outer = const_cast <ConcatAnalysis::NetMap *> (this)->nodes (); !outer.atEnd (); ++outer) {
      Node *node = static_cast <Node *> (*outer);
      if (node->getClassification () != cSCRATCHED) {
        // not scratched
      } else if (node->inDegree () + node->outDegree () > 0) {
        if (s->size () > start) {
          *s << UtIO::endl;
        }
        s->append (indent, ' ');
        *s << node->image () << "**** scratched node with degree " 
          << (node->inDegree () + node->outDegree ()) << UtIO::endl;
        start = s->size ();
      } else if (start == s->size ()) {
        s->append (indent, ' ');
        *s << "Scratched nodes: " << node->image ();
      } else {
        *s << ", " << node->image ();
      }
    }
  }
  if (s->size () - start > 0) {
    *s << (char) UtIO::endl;
  }
}

//! Dump a human readable representation of the graph
void ConcatAnalysis::NetMap::pr () const
{
  UtString buffer;
  compose (&buffer, 0);
  UtIO::cout () << buffer;
}

//! remove cycles from the net adjacency graph
bool ConcatAnalysis::NetMap::remove_cycles (const ConcatAnalysis &parent)
{
  // Look for cycles in the port map by finding the strongly connected
  // components and then diving into the cyclic components.
  GraphSCC scc;
  scc.compute (this);
  // Remove the cycles by deleting all the nodes in every cyclic component.
  GraphNodeSet doomed_nodes;
  GraphSCC::ComponentLoop loop = scc.loopComponents ();
  while (!loop.atEnd ()) {
    GraphSCC::Component *component = *loop;
    if (component->isCyclic ()) {
      GraphSCC::Component::NodeLoop nodes = component->loopNodes ();
      while (!nodes.atEnd ()) {
        NetMap::Node *node = static_cast <NetMap::Node *> (*nodes);
        doomed_nodes.insert (node);
        ++nodes;
      }
    }
    ++loop;
  }
  // delete all nodes that are part of a cycle
  bool changed;
  if (doomed_nodes.empty ()) {
    changed = false;
  } else {
    changed = true;
    parent.note_cycles (doomed_nodes);
    removeNodes (doomed_nodes);
  }
  return changed;
}

//! scratch cycles from the net adjacency graph
bool ConcatAnalysis::NetMap::scratch_cycles (const ConcatAnalysis &parent)
{
  // Look for cycles in the port map by finding the strongly connected
  // components and then diving into the cyclic components.
  GraphSCC scc;
  scc.compute (this);
  // Scratch the cycles by deleting all the nodes in every cyclic component.
  GraphNodeSet doomed_nodes;
  GraphSCC::ComponentLoop loop = scc.loopComponents ();
  while (!loop.atEnd ()) {
    GraphSCC::Component *component = *loop;
    if (component->isCyclic ()) {
      GraphSCC::Component::NodeLoop nodes = component->loopNodes ();
      while (!nodes.atEnd ()) {
        NetMap::Node *node = static_cast <NetMap::Node *> (*nodes);
        doomed_nodes.insert (node);
        ++nodes;
      }
    }
    ++loop;
  }
  // scratch all nodes that are part of a cycle
  bool changed;
  if (doomed_nodes.empty ()) {
    changed = false;
  } else {
    changed = true;
    parent.note_cycles (doomed_nodes);
    scratchNodes (doomed_nodes);
  }
  return changed;
}

bool ConcatAnalysis::NetMap::remove_branches ()
{
  bool pruned = false;
  // Forward pass follows paths from sources to sinks, recursively computing
  // the length of the longest paths out each node. If there are multiple
  // branches out of a node, then the leading edge of the shorter branch is
  // pruned.
  zero_length ();                       // reset all length in the graph
  NetMap::Edge::Set doomed;
  for (Iter <GraphNode *> loop = nodes (); !loop.atEnd (); ++loop) {
    // Scan through the nodes looking for source nodes. At each source
    // node, start a forwards traversal looking for branches.
    Node *node = static_cast <NetMap::Node *> (*loop);
    if (node->isSource ()) {
      // forward traversal from nodes with no incoming edges
      pruned |= prune_forwards (node, &doomed);
    }
  }
  if (!doomed.empty ()) {
    mConcatAnalysis.note_forward_pruning (doomed);
    removeEdges (doomed);
  }
  // Backward pass follows paths from sinks to sources, recursively computing
  // the length of the longest paths out each node. If there are multiple
  // branches into a node, then the leading edge of the shorter branch is
  // pruned.
  zero_length ();                       // reset all length in the graph
  doomed.clear ();
  for (Iter <GraphNode *> loop = nodes (); !loop.atEnd (); ++loop) {
    // Scan through the nodes looking for source and sink nodes. At each sink,
    // start a backwards traversal.
    Node *node = static_cast <NetMap::Node *> (*loop);
    if (node->isSink ()) {
      // forward traversal from nodes with no incoming edges
      pruned |= prune_backwards (node, &doomed);
    }
  }
  if (!doomed.empty ()) {
    mConcatAnalysis.note_backward_pruning (doomed);
    removeEdges (doomed);
  }
  return pruned;
}

//! zero the length field of every node
void ConcatAnalysis::NetMap::zero_length ()
{
  for (NodeMap::iterator it = mNodes.begin (); it != mNodes.end (); it++) {
    it->second->mLength = 0;
  }
}

//! traverse forwards along edges looking for branches.
/*! Forward prune walks from source nodes towards sink nodes looking for nodes
 *  with out degree greater than one. If such a node is found, then the edge
 *  towards the shorted path is added to the set of edges tagged for removal
 *
 *  \return iff the graph was modified
 *  \post The outdegree of node is one or zero.
 *  \post length is the weighted length of the path from node to the sink.
 */
bool ConcatAnalysis::NetMap::prune_forwards (NetMap::Node *node, NetMap::Edge::Set *doomed)
{
  bool pruned = false;
  if (node->outDegree () == 0) {
    // this is the end of the branch
    node->mLength = 0;                  // degenerate branch out of the node
    return false;                       // nothing pruned here
  }
  // The node has at least one out edge, so iterate across the out edges,
  // computing the length of the branch.
  if (node->mLength > 0) {
    // node is not a sink, and the length of the branch through this has been
    // stored on the node, so we have been here before and pruned any branches
    // out of node. So, just return.
    return false;
  }
  // This is the first time we have seen this node, so iterate over the edges
  // coming out and select the longest path
  UInt32 max_length = 0;
  NetMap::Edge *longest_edge = NULL;
  for (NetMap::Edge::Map::iterator it = node->out_begin (); it != node->out_end (); it++) {
    NetMap::Edge *edge = it->second;
    // first prune the rest of this branch
    Node *to_node = edge->getTo ();
    pruned |= prune_forwards (to_node, doomed);
    // length of path out of this is the length of the path out of the next
    // node, plus the weight of the edge
    UInt32 path_length = to_node->mLength + edge->getWeight ();
    if (longest_edge == NULL) {
      // this is the first edge we have looked at out of the node
      max_length = path_length;
      longest_edge = edge;
    } else if (path_length < max_length) {
      // the path out of this edge is shorter than the existing longest path so
      // mark this edge for deletion
      doomed->insert (edge);
      pruned = true;
    } else {
      // this is a new longest edge, so mark the so-far-longest edge for
      // deletion and make this the new longest
      doomed->insert (longest_edge);
      max_length = path_length;
      longest_edge = edge;
      pruned = true;
    }
  }
  node->mLength = max_length;           // cache the length in the node
  return pruned;
}

/*! Backward prune walks from sink nodes towards source nodes looking for nodes
 *  with in degree greater than one. If such a node is found, then the edge
 *  towards the shorted path is added to the set of edges tagged for removal.
 *
 *  \return iff the graph was modified
 *  \post The indegree of node is one or zero.
 */
bool ConcatAnalysis::NetMap::prune_backwards (NetMap::Node *node, NetMap::Edge::Set *doomed)
{
  bool pruned = false;
  if (node->inDegree () == 0) {
    // this is the end of the branch
    node->mLength = 0;                  // degenerate branch in to the node
    return false;                       // nothing pruned here
  }
  // The node has at least one in edge, so iterate across the in edges,
  // computing the length of the branch.
  if (node->mLength > 0) {
    // This is not a source, and the length of the branch through this has been
    // stored on the node, so we have been here before and pruned any branches
    // in to this. So, just return.
    return false;
  }
  // This is the first time we have seen this node, so iterate over the edges
  // coming in and select the longest path
  UInt32 max_length = 0;
  NetMap::Edge *longest_edge = NULL;
  for (NetMap::Edge::Map::iterator it = node->in_begin (); it != node->in_end (); it++) {
    NetMap::Edge *edge = it->second;
    // first prune the rest of this branch
    Node *to_node = edge->getTo ();
    pruned |= prune_forwards (to_node, doomed);
    // length of path in to this is the length of the path in to the next
    // node, plus the weight of the edge
    UInt32 path_length = to_node->mLength + edge->getWeight ();
    if (longest_edge == NULL) {
      // this is the first edge we have looked at in to the node
      max_length = path_length;
      longest_edge = edge;
    } else if (path_length < max_length) {
      // the path in to this edge is shorter than the existing longest path so
      // mark this edge for deletion
      doomed->insert (edge);
      pruned = true;
    } else {
      // this is a new longest edge, so mark the so-far-longest edge for
      // deletion and make this the new longest
      doomed->insert (longest_edge);
      max_length = path_length;
      longest_edge = edge;
      pruned = true;
    }
  }
  node->mLength = max_length;           // cache the length in the node
  return pruned;
}

//! Check whether a net is scratched, if not then scratch it from the graph
/*! \return whether or not the net had to be scratched */
bool ConcatAnalysis::NetMap::maybeScratchNet (NUNet *net)
{
  Node *node = getNode (net);
  if (node->isScratched ()) {
    return false;                       // already scratched
  } else {
    node->snip ();                      // remove all edges to or from the node
    node->setClassification (cSCRATCHED);
    return true;                        // had to scratch it
  }
}

void ConcatAnalysis::NetMap::scratchEdge (Edge *edge)
{
  edge->snip ();                        // take it out of the graph
  edge->getFrom ()->scratchEdge (edge);
  delete edge;
}

//! Possibly add an edge to this graph.
bool ConcatAnalysis::NetMap::maybeAddEdge (NUNet *from, NUNet *to, Edge **new_edge)
{
  // check that the nets are interesting
  if (!mConcatAnalysis.isInteresting (from) || !mConcatAnalysis.isInteresting (to)) {
    return false;
  }
  // do not make cycles out of single nets
  if (from == to) {
    return false;
  }
  // get nodes in this graph for each net
  Node *from_node = getNode (from);     // creates a node for from net if required
  if (from_node->isScratched ()) {
    // never add edges from scratched nodes
    return false;                       // from node is scratched
  } else if (from_node->mScratched.find (to) != from_node->mScratched.end ()) {
    // Scratched edges are indicated by storing the to net in the mScratched
    // field of the node.
    return false;                       // edge has been scratched
  }
  // check that the nets have compatible flags
  if (!mConcatAnalysis.isVectorisable (from->getFlags (), to->getFlags ())) {
    // not allowed to combine the two nets
    return false;
  }
  Node *to_node = getNode (to);         // creates a node for to net if return
  if (to_node->isScratched ()) {
    // never add edges into a scratched node
    return false;                       // to node is scratched
  }
  Edge::Map::iterator found;
  if (from_node->getClassification () != to_node->getClassification ()) {
    // only make edges between like nodes
    return false;
  } else if ((found = from_node->mEdges.find (to)) != from_node->mEdges.end ()) {
    // there is already an edge from from_node to to, so bump the weight
    // of the edge.
    ++(*found->second);
    return false;                       // no new edges were added
  } else {
    // create a new directed edge between the nodes
    *new_edge = from_node->addEdge (to_node);
    return true;                        // created a new edge
  }
}

// Implementation of class ConcatAnalysis::NetMap::Node
// ****************************************************

/*virtual*/ ConcatAnalysis::NetMap::Node::~Node ()
{
  Edge::Map::iterator it;
  for (it = mEdges.begin (); it != mEdges.end (); it++) {
    delete it->second;
  }
}

//! Add an edge from this to another node.
ConcatAnalysis::NetMap::Edge *ConcatAnalysis::NetMap::Node::addEdge (Node *node)
{
  Edge::Map::iterator it;
  ConcatAnalysis::NetMap::Edge *edge;
  NU_ASSERT (mEdges.find (node->mNet) == mEdges.end (), mNet);
  NU_ASSERT (mClassification == node->mClassification, mNet);
  // create an edge with weight 1
  edge = new ConcatAnalysis::NetMap::Edge (this, node);
  insert (edge);
  return edge;
}

//! Insert an edge into this node.
void ConcatAnalysis::NetMap::Node::insert (Edge *edge)
{
  NU_ASSERT (edge->getFrom ()->getNet () == mNet, mNet);
  NUNet *to_net = edge->getTo ()->mNet;
  mEdges.insert (Edge::Map::value_type (to_net, edge));
  mEdgeSet.insert (edge);
  Edge::Map &back_edges = edge->getTo ()->mBackEdges;
  NU_ASSERT (back_edges.find (mNet) == back_edges.end (), mNet);
  back_edges.insert (Edge::Map::value_type (mNet, edge));
}

//! Write a human readable representation of this node.
void ConcatAnalysis::NetMap::Node::pr () const
{
  UtString b;
  compose (&b);
  UtIO::cout () << b;
}

void ConcatAnalysis::NetMap::Node::compose (UtString *s, const UInt32 options) const
{
  *s << "node for " << image ();
  if (options & cLOCATION) {
    UtString b;
    mNet->getLoc ().compose (&b);
    *s << " at " << b;
  }
  *s << (char) UtIO::endl;
  *s << "  edges into the node:";
  Edge::Map::const_iterator it0;
  Edge::Set bad_back_edges;
  for (it0 = mBackEdges.begin (); it0 != mBackEdges.end (); it0++) {
    Edge *edge = it0->second;
    *s << " ";
    edge->compose (s);
    if (edge->getTo () != this) {
      // The back edge is bogus so add it to the set that gets yelled out loud
      bad_back_edges.insert (edge);
    }
  }
  *s << (char) UtIO::endl;
  *s << "  edges out of the node:";
  Edge::Map::const_iterator it1;
  for (it1 = mEdges.begin (); it1 != mEdges.end (); it1++) {
    Edge *edge = it1->second;
    *s << " ";
    edge->compose (s);
  }
  *s << (char) UtIO::endl;
  // yell out the bogus back edges in mBackEdges.
  if (!bad_back_edges.empty ()) {
    *s << "BAD BACK EDGES:";
    Edge::Set::iterator it2;
    for (it2 = bad_back_edges.begin (); it2 != bad_back_edges.end (); it2++) {
      *s << " ";
      (*it2)->compose (s);
    }
    *s << (char) UtIO::endl;
  }
}

//! Remove all the edges coming sourced or sunk at this node.
void ConcatAnalysis::NetMap::Node::snip ()
{
  // delete all the edges coming out of the node
  for (Edge::Map::iterator it0 = mEdges.begin (); it0 != mEdges.end (); it0++) {
    Edge *edge = it0->second;
    // remove the reference in the back edges of the to node
    Node *to = edge->getTo ();
    Edge::Map::iterator back_ref = to->mBackEdges.find (mNet);
    NU_ASSERT (back_ref != to->mBackEdges.end (), mNet);
    to->mBackEdges.erase (back_ref);
    GraphEdgeSet::iterator ref = mEdgeSet.find (edge);
    NU_ASSERT (ref != mEdgeSet.end (), mNet);
    mEdgeSet.erase (ref);
    // trash the edge
    delete edge;
  }
  mEdges.clear ();
  // delete all the edges coming into the node
  for (Edge::Map::iterator it1 = mBackEdges.begin (); it1 != mBackEdges.end (); it1++) {
    Edge *edge = it1->second;
    // remove the reference in the edges of the from node
    Node *from = edge->getFrom ();
    Edge::Map::iterator forward_ref = from->mEdges.find (mNet);
    NU_ASSERT (forward_ref != from->mEdges.end (), mNet);
    from->mEdges.erase (forward_ref);
    // remove the reference from the edge set in the from node
    GraphEdgeSet::iterator from_ref = from->mEdgeSet.find (edge);
    NU_ASSERT (from_ref != from->mEdgeSet.end (), from->mNet);
    from->mEdgeSet.erase (from_ref);
    // trash the edge
    delete edge;
  }
  mBackEdges.clear ();
}

// Implementation of class ConcatAnalysis::NetMap::Edge
// ****************************************************

/*virtual*/ ConcatAnalysis::NetMap::Edge::~Edge ()
{}

void ConcatAnalysis::NetMap::Edge::pr () const
{
  UtString b;
  compose (&b);
  UtIO::cout () << b << UtIO::endl;
}

//! Construct a string describing this edge
void ConcatAnalysis::NetMap::Edge::compose (UtString *s) const
{
  (*s) << "(" << mFrom->getNet ()->getName ()->str () << ","
    << mTo->getNet ()->getName ()->str () << ")";
}

void ConcatAnalysis::compose (UtString *s, const NUNet *net) const
{
  *s << net->getName ()->str () << ": ";
#define PROBENET(_net_, _iodb_, _tag_)          \
  if (_net_->is##_tag_ (_iodb_)) {              \
    *s << " " #_tag_;                            \
  }
  PROBENET (net, mIODB, Protected);
  PROBENET (net, mIODB, ProtectedObservable);
  PROBENET (net, mIODB, ProtectedMutable);
  PROBENET (net, mIODB, ProtectedMutableNonHierref);
  PROBENET (net, mIODB, ProtectedObservableNonHierref);
#undef PROBENET
#define PROBENET(_net_, _iodb_, _tag_)          \
  if (_iodb_->is##_tag_ (_net_)) {              \
    *s << " " #_tag_;                            \
  }
  PROBENET (net, mIODB, DepositableNet);
  PROBENET (net, mIODB, ForcibleNet);
  PROBENET (net, mIODB, AsyncResetNet);
  PROBENET (net, mIODB, BlastedNet);
#undef PROBENET
  compose (s, net->getFlags ());
}

void ConcatAnalysis::compose (UtString *s, const NetFlags flags) const
{
  switch (flags & 0x00f) {
#define CASE(_x_) case e##_x_: *s << " " #_x_ ":"; break;
  CASE (InputNet);
  CASE (OutputNet);
  CASE (BidNet);
  CASE (FlopNet);
  CASE (Latch);
#undef CASE
  default: break;
  }
  switch (flags & eDeclareMask) {
#define CASE(_x_) case e##_x_: *s << " " #_x_ ":"; break;
  CASE (DMTimeNet)
  CASE (DMWireNet)
  CASE (DMTriNet)
  CASE (DMTri1Net)
  CASE (DMSupply0Net)
  CASE (DMWandNet)
  CASE (DMTriandNet)
  CASE (DMTri0Net)
  CASE (DMSupply1Net)
  CASE (DMWorNet)
  CASE (DMTriorNet)
  CASE (DMTriregNet)
  CASE (DMRegNet)
  CASE (DMRealNet)
  CASE (DMIntegerNet)
  CASE (2DNetMask)
#undef CASE
  default: break;
  }
#define MASK(_x_) if (flags & e##_x_) { *s << " " #_x_; }
  MASK (AliasedNet)
  MASK (AllocatedNet)
  MASK (InaccurateNet)
  MASK (TriWritten)
  MASK (BlockLocalNet)
  MASK (NonStaticNet)
  MASK (PullUp)
  MASK (PullDown)
  MASK (PrimaryZ)
  MASK (InClkPath)
  MASK (InDataPath)
  MASK (EdgeTrigger)
  MASK (TempNet)
  MASK (ForcibleNet)
  MASK (DepositNet)
  MASK (RecordPort)
  MASK (ReadNet)
  MASK (WrittenNet)
  MASK (ConstZ)
  MASK (DeadNet)
  MASK (Reset)
  MASK (Signed)
  MASK (ClearAtEnd)
#undef MASK
}

