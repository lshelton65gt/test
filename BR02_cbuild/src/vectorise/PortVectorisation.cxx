// -*-C++-*-    $Revision: 1.7 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "reduce/Inference.h"
#include "reduce/OccurrenceLogger.h"
#include "reduce/InferenceLogger.h"
#include "util/ArgProc.h"
#include "util/UtIOStream.h"
#include "util/GenericDigraph.h"
#include "util/Stats.h"
#include "vectorise/PortVectorisation.h"

/*! \file
 *  Implementation of port vectorisation.
 *
 *  Port vectorisation looks for scalar nets that occur next to each other in
 *  concat expressions or concat assignments and replaces suitable sets of
 *  scalar nets with vector nets.
 */


static InferenceLogger Log;

// TODO: investigate moving the port vectorisation pass out of
// LocalAnalysis::analyzeDesignPorts and into runGlobalOptimizations so that
// it runs before the first concat rewrite. Then, any concats that are broken
// by rewrite and cannot be glued back together by the collapser may be
// considered as port vectorisation opportunities.
//
// TODO: test for nets that are primary ports and implement three forms of
// handling, determined by a command line argument:
// * totally ignore primary ports.
// * analyse and propagate through the primary ports but do not vectorise
//   (this may result in concats of primary ports being used as actual
//   parameters in port instantiations).
// * vectorise primary ports just like any other ports.

PortCollapser::PortCollapser (AtomicCache *strCache, 
  IODBNucleus *iodb, 
  Stats *stats, 
  MsgContext *msgContext,
  NUNetRefFactory *netRefFactory,
  bool phaseStats,
  PortVectoriser::Options &options) : 
  mStrCache (strCache), 
  mIODB (iodb), 
  mStats (stats), 
  mMsgContext (msgContext), 
  mNetRefFactory (netRefFactory), 
  mPhaseStats (phaseStats),
  mOptions (options)
{
  if (mOptions & PortVectoriser::cVERBOSE_CONFIG) {
    UtString b;
    mOptions.compose (&b);
    UtIO::cout () << "  port vectorisation options: " << b << UtIO::endl;
  }
}

void PortCollapser::design (NUDesign *design)
{
  // Phase 1 - setup
  // Build a port vectoriser instance for each module. The instance are
  // constructed in bottom-up order over the module hierarchy and this order
  // kept in a list of the instance. Propagation of opportunities and
  // constraints uses this ordering to try to reduce the number of iterations
  // required.
  if (mPhaseStats) {
    mStats->pushIntervalTimer ();
  }
  ProtectedAlias protect (mIODB, design);
  PortVectoriser::ManagedMap pvmap;
  PortVectoriser::List sorted;
  NUModuleList bottom_up;
  design->getModulesBottomUp (&bottom_up);
  PortVectoriser::Set workset;
  for (NUModuleList::iterator it = bottom_up.begin (); it != bottom_up.end (); it++) {
    NUModule *module = *it;
    PortVectoriser *vectoriser = 
      new PortVectoriser (this, mStrCache, mIODB, mStats,  mMsgContext, mNetRefFactory, protect,
        module, mOptions);
    pvmap.insert (PortVectoriser::Map::value_type (module, vectoriser));
    sorted.push_back (vectoriser);
    workset.insert (vectoriser);
  }
  // build an actual map for each module instance
  build_parameter_maps (design, pvmap);
  if (mOptions & PortVectoriser::cVERBOSE_PARAM_MAPS) {
    for (PortVectoriser::List::iterator it = sorted.begin (); it != sorted.end (); it++) {
      UtString b;
      (*it)->compose (&b, PortVectoriser::cPARAM_MAPS);
      UtIO::cout () << b;
    }
  }
  // winnow out ports where there is an actual parameters that can never get
  // vectorised
  for (PortVectoriser::Map::iterator it = pvmap.begin (); it != pvmap.end (); it++){
    it->second->winnowNeverVectorisableFormals ();
  }
  if (mPhaseStats) {
    mStats->printIntervalStatistics ("PV: Setup");
  }
  UInt32 n_iterations = 0;
  // Phase 2 - discovery
  // The collapser does not yet use potential vectorisations embedded in the
  // netmaps to locate new vectorisations. Thus, subsequent discovery passes
  // can never find new edges in the net maps. Once the collapser is extended
  // to use this information, the call to discovery should move into the loop.
  discovery (sorted);
  // Phase 3 - propagation
  // Vectorisation opportunities and constraints are propagated up and down the
  // module hierarchy.
  bool changed = true;
  if (mPhaseStats) {
      // start a subinterval for port vec propagation
      mStats->pushIntervalTimer ();
  }
  while (changed && n_iterations < mOptions.maxIterations ()) {
    if (mOptions & PortVectoriser::cVERBOSE_PROPAGATE) {
      UtString b;
      workset.compose (&b);
      UtIO::cout () << "Propagation iteration " << n_iterations << ": " << b << UtIO::endl;
    }
    Log (PortVectoriser::cITERATION, n_iterations);
    changed = false;
    // Dump the snapshot before propagation... the final iteration will dump
    // the converged snapshot.
    if (mOptions | (PortVectoriser::cLOGGING|PortVectoriser::cVERBOSE_ITERATION)) {
      for (PortVectoriser::Map::iterator it = pvmap.begin (); it != pvmap.end (); it++) {
        Log (PortVectoriser::cSNAPSHOT, it->first->getModule (), n_iterations, &it->second->getMap ());
        if (mOptions & PortVectoriser::cVERBOSE_ITERATION) {
          UtString b;
          it->second->getMap ().compose (&b, 2);;
          UtIO::cout () << "Netmap for " << it->first->getName ()->str () << UtIO::endl << b;
        }
      }
    }
    PortVectoriser::Set altered;
    // propagate bottom up...
    for (PortVectoriser::List::iterator it = sorted.begin (); it != sorted.end (); it++) {
      PortVectoriser::Set::iterator entry = workset.find (*it);
      if (entry == workset.end ()) {
        // the netmap of this vectoriser has not changed since the last time it
        // was examined
      } else {
        // this vectoriser is in the work set so has had information propagated
        // since the last time we looked
        changed |= (*it)->propagate (&altered);
      }
    }
    // propagate top down...
    for (PortVectoriser::List::reverse_iterator it = sorted.rbegin (); it != sorted.rend (); it++) {
      PortVectoriser::Set::iterator entry = workset.find (*it);
      if (entry == workset.end ()) {
        // the netmap of this vectoriser has not changed since the last time it
        // was examined
      } else {
        // this vectoriser is in the work set so has had information propagated
        // since the last time we looked
        changed |= (*it)->propagate (&altered);
      }
    }
    workset = altered;
    if (mPhaseStats) {
      char banner [64];
      snprintf (banner, sizeof (banner), "PV: %d", n_iterations);
      mStats->printIntervalStatistics (banner);
    }
    n_iterations++;
  }
  if (mPhaseStats) {
    mStats->popIntervalTimer ();
    mStats->printIntervalStatistics ("PV: Propagate");
  }
  // If we exited the loop because nothing changed rather than just hitting
  // the iteration limit, then the workset should be empty.
  ASSERT (changed || workset.empty ());
  if (changed) {
    mMsgContext->VEPortVecIterationFence (n_iterations);
    ASSERT (!(mOptions & PortVectoriser::cSTRICT));
  } else if (mOptions & PortVectoriser::cVERBOSE_PROPAGATE) {
    Log (PortVectoriser::cCONVERGED, n_iterations);
    UtIO::cout () << "Port vectorisation propagation converged after "
      << n_iterations << " iterations." << UtIO::endl;
  } else {
    Log (PortVectoriser::cCONVERGED, n_iterations);
  }
  // Propagation may have produced cycles and branches in the netmaps. A cycle
  // indicates conflicts between vectorisation opportunities (see the comment
  // at the beginning of PortVectorisation.h for an example of a cycle induced
  // by propagation). A branch indicates ambiguous vectorisations. Structural
  // winnowing removes cycles and breaks branches.
  for (PortVectoriser::Map::iterator it = pvmap.begin (); it != pvmap.end (); it++) {
    it->second->structural_winnowing ();
  }
  if (mPhaseStats) {
    mStats->printIntervalStatistics ("PV: Winnow");
  }
  // construct the vectorisation plans
  PortVectoriser::Set vectorised;       // instance with a non-degenerate vectorisation
  construct (pvmap, &vectorised);
  if (mPhaseStats) {
    mStats->printIntervalStatistics ("PV: Construct");
  }
  // apply the plans
  apply (pvmap, vectorised);
  // diagnostic output
  if (mOptions & PortVectoriser::cDUMP_VERILOG) {
    char filename [64];
    mOptions.makeFilename ("portvec", "v", filename, sizeof (filename));
    design->printVerilog (filename);
  }
  if (mPhaseStats) {
    mStats->printIntervalStatistics ("PV: Apply");
  }
  if (!(mOptions & PortVectoriser::cSTRICT)) {
    // do not perform sanity checks
  } else if (!isSane (sorted)) {
    mMsgContext->VEPortVecInsane ();  // a fatal error :-(
  } else {
    mStats->printIntervalStatistics ("PV: Sanity");
  }
  UtOStream *s;
  if ((s = mOptions.getReportFile ()) != NULL) {
    UInt32 flags = mOptions & PortVectoriser::cREPORT_NET_DETAILS ? 
      ConcatVectorisation::cNET_FLAGS|ConcatVectorisation::cNET : ConcatVectorisation::cDEFAULT;
    for (PortVectoriser::Map::iterator it = pvmap.begin (); it != pvmap.end (); it++) {
      it->second->report (*s, flags);
    }
  }
  if (mPhaseStats) {
    mStats->popIntervalTimer ();
  }
}

PortCollapser::~PortCollapser ()
{
}

//! Perform discovery on all the port vectorisation instances
bool PortCollapser::discovery (PortVectoriser::List &pvlist)
{
  // perform discovery on all the port vectorisation instances
  bool changed = false;
  for (PortVectoriser::List::iterator it = pvlist.begin (); it != pvlist.end (); it++) {
    // just pass it down to the instance...
    changed |= (*it)->discovery ();
  }
  return changed;
}

//! Propagate constraints to parents and children in the module hierarchy
bool PortVectoriser::propagate (PortVectoriser::Set *workset)
{
  bool changed = false;
  UInt32 count = 0;                     // the number of iterations the propagation
  UInt32 delta = 0;                     // the number of changed edges
  ConcatAnalysis::NetMap::Edge::Set::iterator edges;
  ConcatAnalysis::NetMap::Edge::Set scratched; // edges to scratch
  Iter <GraphNode *> nodes = mMap.nodes ();
  UInt64 t0 = OSGetRusageTime (true, true); // start time
  // Propagation is driven by considering each edge in the netmap and
  // propagating down through the actual parameters of child modules and up
  // through formal parameters into parent modules.
  while (!nodes.atEnd ()) {
    // all the nodes
    ConcatAnalysis::NetMap::Node *node = static_cast <ConcatAnalysis::NetMap::Node *> (*nodes);
    if (!node->isScratched ()) {
      // the node has been removed from consideration
      Iter <GraphEdge *> edges = mMap.edges (node);
      while (!edges.atEnd ()) {
        ConcatAnalysis::NetMap::Edge *edge = static_cast <ConcatAnalysis::NetMap::Edge *> (*edges);
        // Propagate down through actual parameter occurrrences of the source
        // of the edge.
        ActualIndex::Range actual_range = mActualIndex.equal_range (edge->getFrom ()->getNet ());
        // actual_range.first to actual_range.second yields the maps of all the
        // instances using the source of the edge as an actual parameter
        for (ActualIndex::iterator it = actual_range.first; it != actual_range.second; it++) {
          ActualMap *actuals = it->second; // includes the source of the edge as an actual
          bool scratch_edge = false;
          if (actuals->propagate (actuals->getChild (), this, edge, &scratch_edge, &count, &delta)) {
            // Note that the propagate methods for actuals iterates over all
            // occurrences of both the source and the sink of the edge in the
            // actual parameter bindings in the module instance modelled by
            // actuals.
            workset->insert (actuals->getChild ());
            changed = true;
          }
          if (scratch_edge) {
            // Propagation determined that the edge conflicts with
            // vectorisation in the child module. The edge should not be
            // deleted immediately because we are still iterating over the
            // edges out of the current node. Instead it is marked for deletion
            // after this propagation step for the module.
            scratched.insert (edge);
          }
        }
        // Propagate up through the formal parameters into all parent modules.
        FormalIndex::Range formal_range = mFormalIndex.equal_range (edge->getFrom ()->getNet ());
        // formal_range.first to formal_range.second yields the maps of all the
        // instances of this module in which the source of edge is bound as a
        // formal parameter.
        for (FormalIndex::iterator it = formal_range.first; it != formal_range.second; it++) {
          FormalMap *formals = it->second; // source of the edge is in formals
          bool scratch_edge = false;
          NU_ASSERT (formals->getChild () == this, formals->getInstance ());
          if (formals->propagate (formals->getParent (), this, edge, &scratch_edge, &count, &delta)) {
            // note that the propagate method for formals looks for the sink of
            // edge in the formal map.
            workset->insert (formals->getParent ());
            changed = true;
          }
          if (scratch_edge) {
            // The edge conflicted with a vectorisation in the parent.
            scratched.insert (edge);
          }
        }
        ++edges;
      }
    }
    ++nodes;
  }
  // now delete all the edges that were found to conflict with a parent or a child.
  ConcatAnalysis::NetMap::Edge::Set::iterator it;
  for (it = scratched.begin (); it != scratched.end (); it++) {
    mMap.scratchEdge (*it);
  }
  // record the time and size of this propagation step.
  UInt64 t1 = OSGetRusageTime (true, true);
  UInt32 elapsed = (t1 - t0) / 1000000;
  Log (cPROPAGATE_MODULE, mModule, count, delta);
  Log (cPROPAGATE_TIME, mModule, elapsed);
  return changed;
}

void PortCollapser::build_parameter_maps (NUDesign *design, PortVectoriser::Map &pvmap)
{
  // iterate over the module instances and construct an actual map for each instance
  NUDesign::ModuleInstances moduleInstances;
  design->findModuleInstances(&moduleInstances);
  for (NUDesign::ModuleInstancesLoop modules (moduleInstances); !modules.atEnd (); ++modules) {
    // module by modules...
    NUModule *module = modules.getKey ();
    PortVectoriser::Map::iterator found = pvmap.find (module);
    NU_ASSERT (found != pvmap.end (), module);
    PortVectoriser *vectoriser = found->second;
    NUDesign::Instances *instances = modules.getValue();
    for (NUDesign::InstancesLoop i (*instances); !i.atEnd (); ++i) {
      // instance by instance...
      NUModuleInstance *instance = *i;
      NUModule* instMod = dynamic_cast<NUModule*>(instance->getParentModule());
      NU_ASSERT(instMod, instance);
      // find the port vectoriser instances for the module
      PortVectoriser::Map::iterator parent = pvmap.find (instMod);
      NU_ASSERT (parent != pvmap.end (), instance);
      // Build the maping from the formal parameter nets in the module to the
      // actual parameters in the parent.
      PortVectoriser::FormalMap *formals = 
        new PortVectoriser::FormalMap (*vectoriser, instance, 
          mOptions & PortVectoriser::cVERBOSE_PROPAGATE, parent->second, vectoriser);
      vectoriser->addParent (parent->second, formals);
      // Build the mapping from the scalar actual parameter nets in the parent
      // to the formals in the module.
      PortVectoriser::ActualMap *actuals = 
        new PortVectoriser::ActualMap (*formals, mOptions & PortVectoriser::cVERBOSE_PROPAGATE,
          parent->second, vectoriser);
      parent->second->addChild (vectoriser, actuals);
      // Add them to the lists for deletion by a destructor
      mActuals.push_back (actuals);
      mFormals.push_back (formals);
    }
  }
  // cleanup
  for (NUDesign::ModuleInstancesLoop modules (moduleInstances); !modules.atEnd (); ++modules) {
    delete modules.getValue ();
  }
}

void PortCollapser::construct (PortVectoriser::Map &pvmap, PortVectoriser::Set *vectorised)
{
  for (PortVectoriser::Map::iterator it = pvmap.begin (); it != pvmap.end (); it++) {
    PortVectoriser *vectoriser = it->second;
    if (!vectoriser->construct ()) {
      // there is no vectorisation for this module
      Log (PortVectoriser::cNO_VECTORISATION, it->first);
    } else {
      // found a non-degenerate vectorisation
      vectorised->insert (vectoriser);  // note that we have a vectorisation
    }
  }
}

void PortCollapser::apply (PortVectoriser::Map &pvmap, PortVectoriser::Set &vectorised)
{
  // Apply the vectorisation scheme to the module ports and definitions
  bool failed = false;
  for (PortVectoriser::Map::iterator it = pvmap.begin (); it != pvmap.end (); it++) {
    PortVectoriser *vectoriser = it->second;
    PortVectoriser::Set::iterator found;
    if ((found = vectorised.find (vectoriser)) == vectorised.end ()) {
      // no vectorisation was found in this module
    } else if (!vectoriser->apply ()) {
      failed = true;
      Log (PortVectoriser::cAPPLICATION_FAILED, it->first);
      // application of the plan failed; something bad happened
      mMsgContext->VEPortVecFailed (it->first, it->first->getName ()->str ());
      vectorised.erase (found);
      vectoriser->verbose ();
    }
  }
  // Apply the vectorised scheme to the module instantiations
  for (PortVectoriser::Map::iterator it = pvmap.begin (); it != pvmap.end (); it++) {
    PortVectoriser *vectoriser = it->second;
    if (vectorised.find (vectoriser) == vectorised.end ()) {
      // no vectorisation was found in this module
    } else if (!vectoriser->applyToInstances ()) {
      failed = true;
      Log (PortVectoriser::cAPPLICATION_FAILED, it->first);
      // application of the plan failed; something bad happened
      mMsgContext->VEPortVecFailed (it->first, it->first->getName ()->str ());
      vectoriser->verbose ();
    }
    if (mOptions & PortVectoriser::cVERBOSE) {
      vectoriser->verbose ();           // dump the vectorisation
    }
  }
  // Crash and burn for inconsistent vectorisations
  if (failed) {
    // Some part of port vectorisation application has failed. This is not
    // recoverable because module instances may be inconsistent with the
    // vectorised module definitions. Madness results.
    mMsgContext->VEPortVecApplicationFailed (); // fatal error... 
  }
}

//! Run sanity checks on the modified design
bool PortCollapser::isSane (const PortVectoriser::List &pvlist) const
{
  bool crazy = false;
  for (PortVectoriser::List::const_iterator it = pvlist.begin (); it != pvlist.end (); it++) {
    if (!(*it)->isSane ()) {
      mMsgContext->VEPortVecInsaneModule ((*it)->getModule (), (*it)->getModule ()->getName ()->str ());
      crazy = true;
    }
  }
  return !crazy;
}

PortVectoriser::PortVectoriser (
  PortCollapser *collapser,
  AtomicCache *strCache, 
  IODBNucleus *iodb, 
  Stats *stats, 
  MsgContext *msgContext,
  NUNetRefFactory *netRefFactory,
  ProtectedAlias &protect,
  NUModule *module,
  const Options &options) :
  mOptions (options), mCollapser (collapser), mStrCache (strCache), mIODB (iodb), 
  mStats (stats), mMsgContext (msgContext), mNetRefFactory (netRefFactory), mProtect (protect),
  mModule (module), 
  mMap (mBitNetAnalysis), 
  mBitNetAnalysis (strCache, iodb, msgContext, netRefFactory, protect, module, options),
  mVectorisation (iodb, msgContext, mBitNetAnalysis, module, mOptions.getSummaryFile (),
    (mOptions & cVERBOSE_NETS)),
  mActualIndex (), mFormalIndex ()
{
}

//! Set the options word from the command line.
void PortVectoriser::Options::parse (ArgProc *args, MsgContext *msg_context)
{
  // now the port vectorisation options
  if (args->getBoolValue (CRYPT ("-verbosePortVec"))) {
    mOptions |= cVERBOSE;
  }
  if (args->getBoolValue (CRYPT ("-logPortVec"))) {
    mOptions |= cLOGGING;
  }
  if (args->getBoolValue (CRYPT ("-verboseCollapse"))) {
    mOptions |= cVERBOSE_COLLAPSE;
  }
  if (args->getBoolValue (CRYPT ("-portVecPrimary"))) {
    mOptions |= cVECTORISE_PRIMARY;
  }
  if (args->getBoolValue (CRYPT ("-portVecThroughPrimary"))) {
    mOptions |= cVECTORISE_THROUGH_PRIMARY;
  }
  if (args->getStrValue ("-portVecSuffix", &mSuffix) != ArgProc::eKnown || mSuffix == NULL) {
    // no suffix requested
  } else {
    while (*mSuffix == '-' && *mSuffix != '\0') {
      mSuffix++;
    }
    mArgs |= cSUFFIX;
  }
  // If -portVecPrimary is requested, then vectorisation opportunities will
  // flow through the primary ports, so selecting -noPortVecThroughPrimary is
  // bogus.
  if ((mOptions & cVECTORISE_PRIMARY) && !(mOptions & cVECTORISE_THROUGH_PRIMARY)) {
    msg_context->VEPortVecInconsistentPrimaryOptions ();
  }
  SInt32 value;
  // Look for -portVecMaxIterations
  if (args->getIntFirst ("-portVecMaxIterations", &value) != ArgProc::eKnown) {
    msg_context->VEPortVecExpectedArgument ("-portVecMaxIterations");
  } else if (value <= 1) {
    msg_context->VEPortVecBadIntegerArgument ("-portVecMaxIterations", "greater than 1", value);
  } else {
    mMaxIterations = value;
  }
  // Set the options mask from the various values of -portVec. Note that these
  // options are intended only for development and debugging not for customers.
  ArgProc::StrIter it;
  if (args->getStrIter (CRYPT ("-portVec"), &it) == ArgProc::eKnown) {
    while (!it.atEnd ()) {
      const char *s = *it;
      UInt32 n = 0;                   // index into s
      UInt32 size = strlen (s);
      while (n < size) {
        bool erroneous = false;
        // look for the start of the next word in s
        while (n < size && (isspace (s [n]) || s [n] == ',')) {
          n++;
        }
        if (n < size && !isalnum (s [n])) {
          msg_context->VEPortVecBadArgument (s, "badly formed option");
          // scan for the next reasonable option in the string
          while (n < size && !isalnum (s [n])) {
            n++;
          }
        }
        char ident [64];              // buffer for the identifier
        UInt32 value = 0;             // cumulative value of integer
        bool all_digits = true;
        UInt32 length = 0;
        while (n < size && (isalnum (s [n]) || s [n] == '_')) {
          if (length >= sizeof (ident) - 1) {
            // identifier is too long
            msg_context->VEPortVecBadArgument (s, "option is too long");
            erroneous = true;
            length = 0;
            // scan to the end of the argument
            while (n < size && ((isalnum (s [n]) || s [n] == '_'))) {
              n++;
            }
          } else if (all_digits && isdigit (s [n])) {
            value = 10 * value + (UInt32) (s [n]) - (UInt32) '0';
            ident [length++] = s [n++];
          } else {
            all_digits = false;
            ident [length++] = s [n++];
          }
        }
        ident [length] = '\0';          // null terminate
        // the word was either all digits, in which case value contains the
        // integer value of the word, or an identifier cached in ident.
        if (erroneous) {
          // ignore the option
        } else if (length == 0) {
          // no more words to look at
        } else if (all_digits) {
          // it is all digits, so treat the argument as the cluster size
          if (value < 2) {
            msg_context->VEPortVecBadIntegerArgument ("-portVec N: cluster size",
              "greater than 1", value);
          } else if ((mArgs & cMIN_CLUSTER_SIZE) && (UInt32) value != mMinCluserSize) {
            msg_context->VEPortVecBadArgument (s, "conflicting minimum cluster sizes specified");
          } else {
            mArgs |= cMIN_CLUSTER_SIZE;
            mMinCluserSize = value;
          } 
        } else if (cvt (ident, &mOptions)) {
          // successfully matched the port vec options
        } else {
          char verbose [64];
          snprintf (verbose, sizeof (verbose), "verbose_%s", ident);
          if (cvt (verbose, &mOptions)) {
            // successfully matched verbose_ident
          } else {
            msg_context->VEPortVecBadArgument (ident, "unrecognised portVec option");
          }
        }
      }
      ++it;
    }
  }
  // Look for -portVecMinCluster after processing -portVec so that the -portVec
  // N shortcut can be used.
  if (args->getIntFirst ("-portVecMinCluster", &value) != ArgProc::eKnown) {
    // -portVecMinCluster not specified
  } else if (value < 2) {
    msg_context->VEPortVecBadIntegerArgument ("-portVecMinCluster", "greater than 1", value);
  } else if ((mArgs & cMIN_CLUSTER_SIZE) && (UInt32) value != mMinCluserSize) {
    char s [64];
    sprintf (s, "%d", mMinCluserSize);
    msg_context->VEPortVecBadArgument (s, "conflicting minimum cluster sizes specified");
  } else {
    mArgs |= cMIN_CLUSTER_SIZE;
    mMinCluserSize = value;
  }
  // check that the minimum cluster size was set either from -portVec or the
  // longer argument
  if (!(mArgs & cMIN_CLUSTER_SIZE)) {
    mArgs |= cMIN_CLUSTER_SIZE;
    mMinCluserSize = cDEFAULT_MIN_CLUSTER_SIZE;
  }
  // debug mode hacks
  if ((mOptions & cDEBUGGING) && !(mOptions & cLOGGING)) {
    // -portVec debug was found but not -logPortVec. Open a logfile...
    char filename [PATH_MAX];
    makeFilename ("portvec", "log", filename, sizeof (filename));
    PortVectoriser::openOccurrenceLog (*msg_context, NULL, filename);
    mOptions |= cLOGGING;
  }
  if (args->getStrValue ("-reportPortVec", &mReportFileName) != ArgProc::eKnown
    || mReportFileName == NULL) {
    // no vectorisation report requested on the command line
    if (mOptions & cDEBUGGING) {
      // create a report of the form pv-N.lst where N is the minimum cluster size
      char filename [PATH_MAX];
      makeFilename ("portvec", "lst", filename, sizeof (filename));
      mReportFile = new UtOFStream (filename);
      mArgs |= cREPORT_FILE;
    }
  } else if (mReportFileName [0] == '-' && mReportFileName [1] == '\0') {
    mArgs |= cREPORT_STDOUT;
    mReportFile = &UtIO::cout ();
  } else {
    mArgs |= cREPORT_FILE;
    mReportFile = new UtOFStream (mReportFileName);
  }
  if ((mArgs & cREPORT_FILE) && !mReportFile->is_open ()) {
    delete mReportFile;
    mReportFile = NULL;
    msg_context->VEPortVecReportBadFile (mReportFileName);
  }
  // open the net vectorisation summary file
  char filename [MAXPATHLEN];
  if (mFileRoot == NULL) {
    // weird... no file root was passed to this
    snprintf (filename, sizeof (filename), "libdesign.netvec");
  } else {
    snprintf (filename, sizeof (filename), "%s.netvec", mFileRoot);
  }
  mSummaryFile = new UtOFStream (filename);
  if (!mSummaryFile->is_open ()) {
    delete mSummaryFile;
    mSummaryFile = NULL;
    msg_context->VEPortVecReportBadFile (filename);
  }
}

PortVectoriser::Options::~Options ()
{
  if (mArgs & cREPORT_FILE) {
    delete mReportFile;
  }
  if (mSummaryFile != NULL) {
    delete mSummaryFile;
  }
}

//! Construct a filename suffix based on the option bits.
bool PortVectoriser::Options::makeFilename (const char *prefix, const char *suffix, 
  char *s, const size_t size) const
{
#define bound(n, size, delta) if (n < size) { n += delta; }
  UInt32 n = 0;
  if (mFileRoot != NULL) {
    bound (n, size, snprintf (s + n, size - n, "%s.%s-%d", mFileRoot, prefix, mMinCluserSize));
  } else {
    bound (n, size, snprintf (s + n, size - n, "%s-%d", prefix, mMinCluserSize));
  }
  if (mOptions & cNO_VECTORISE_REGISTERS) {
    bound (n, size, snprintf (s + n, size - n, "-noreg"));
  }
  if (mOptions & cNO_VECTORISE_LOCALS) {
    bound (n, size, snprintf (s + n, size - n, "-nolocal"));
  }
  if (mOptions & cNO_VECTORISE_PORTS) {
    bound (n, size, snprintf (s + n, size - n, "-noports"));
  }
  if (mOptions & cVECTORISE_PRIMARY) {
    bound (n, size, snprintf (s + n, size - n, "-pri"));
  }
  if (!(mOptions & cVECTORISE_THROUGH_PRIMARY)) {
    bound (n, size, snprintf (s + n, size - n, "-nopri"));
  }
  if (mArgs & cSUFFIX) {
    bound (n, size, snprintf (s + n, size - n, "-%s", mSuffix));
  }
  bound (n, size, snprintf (s + n, size - n, ".%s", suffix));
#undef bound
  return (n < size);
}

void PortVectoriser::Set::compose (UtString *s) const
{
  *s << "{";
  for (const_iterator it = begin (); it != end (); it++) {
    if (it != begin ()) {
      *s << ", ";
    }
    *s << (*it)->image ();
  }
  *s << "}";
}

void PortVectoriser::Set::pr () const
{
  UtString b;
  compose (&b);
  UtIO::cout () << b << UtIO::endl;
}

// Discovery
// *********

//! interesting nets for concat analysis are the ports... not surprisingly...
/*virtual*/ bool PortVectoriser::BitnetConcatAnalysis::isInteresting (NUNet *net)
{ 
  NetFlags flags = net->getFlags ();
  bool retval = true;
  // first throw out any declarations that preclude vectorisation
  switch (flags & eDeclareMask) {
  case eDMRealNet:
  case eDMTimeNet:
  case eDMSupply0Net:
  case eDMSupply1Net:
  case eDMIntegerNet:
  case eDMTime2DNet:
  case eDMWire2DNet:
  case eDMReg2DNet:
  case eDMRTime2DNet:
  case eDMInteger2DNet: 
    // never vectorise these
    retval = false;
    break;
  case eNoneNet:
  case eDMWireNet:
  case eDMTriNet:
  case eDMTri1Net:
  case eDMWandNet:
  case eDMTriandNet:
  case eDMTri0Net:
  case eDMWorNet:
  case eDMTriorNet:
  case eDMTriregNet:
  case eDMRegNet:
    // these are OK
    break;
  default:
    // A new type must have been added to the net flags. It is always safe to
    // just add it to the class of nets that are thrown out of
    // vectorisation. However, its suitability for vectorisation really should
    // be considered.
    mMsgContext->VEPortVecBogusNetFlags (net);
    if (mOptions & cSTRICT) {
      NU_ASSERT (false, net);
    }
    return false;
    break;
  }
  // For debugging it can be usefull to avoid certain classes of
  // vectorisations. The flags are set with the -portVec command line argument.
  if ((flags & eDMRegNet) && (mOptions & cNO_VECTORISE_REGISTERS)) {
    // not vectorising registers
    retval = false;
  }
  if (!(flags & ePortMask) && (mOptions & cNO_VECTORISE_LOCALS)) {
    // not vectorising local nets
    retval = false;
  }
  if ((flags & ePortMask) && (mOptions & cNO_VECTORISE_PORTS)) {
    // not vectorising ports... note that this will stop all propagation up and
    // down the hierarchy, leaving only structural vectorisation of local nets
    retval = false;
  }
#if PORTVEC_SINGLE_BIT_VECTORS
  // Treat single bit vectors as scalars. In particular, this will pick up the
  // nets created with a blastNet directive.
  bool is_scalar = net->isBitNet () || net->getBitSize () == 1;
#else
  // Only vectorise bit nets. Single bit vectors are considered as vectors.
  bool is_scalar = net->isBitNet ();
#endif
  retval = retval
    && is_scalar                        // must be a scalar net
    && !(flags & cNEVER_VECTORISE)      // some flags make it unvectorisable
    && !mProtect (net)                  // closure of protected net
    && !net->isProtected (mIODB)        // don't touch protected nets
    && !net->isMem ()
    ;
  if (mOptions & cVECTORISE_THROUGH_PRIMARY) {
    // Propagation of vectorisation through primary ports is permited in this
    // instance.
  } else if (net->isPrimaryPort ()) {
    // It is a primary port and vectorisation through primary ports is not
    // permitted.
    retval = false;
  }
  if (mOptions & cVERBOSE_INTEREST) {
    UtString b;
    compose (&b, net);
    UtIO::cout () << (retval ? "Interesting" : "Uninteresting") << " net: "
                  << b << UtIO::endl;
  }
  if (!(mOptions & cSTRICT)) {
    retval = retval
      && ((flags & cASSERT_ZERO) == 0)
      && ((flags & cASSERT_ONE) == cASSERT_ONE);
  } else if ((flags & cASSERT_ZERO) != 0) {
    UtString b0, b1;
    compose (&b0, (NetFlags) cASSERT_ZERO);
    compose (&b1, net);
    UtIO::cout () << "Unexpected net flags set: " << b0 << ": " << b1 << UtIO::endl;
    NU_ASSERT ((flags & cASSERT_ZERO) == 0, net);
  } else if ((flags & cASSERT_ONE) != cASSERT_ONE) {
    UtString b0, b1;
    compose (&b0, (NetFlags) cASSERT_ZERO);
    compose (&b1, net);
    UtIO::cout () << "Unexpected net flags reset: " << b0 << ": " << b1 << UtIO::endl;
    NU_ASSERT ((flags & cASSERT_ONE) == cASSERT_ONE, net);
  }
  return retval;
}

/*virtual*/ UInt32 PortVectoriser::BitnetConcatAnalysis::classifyNet (NUNet *net)
{
  NetFlags flags = net->getFlags ();
  return ((UInt32) flags & 0x1ff);
}

//! look for potential port vectorisations in the module
bool PortVectoriser::discovery ()
{
  bool changed = mBitNetAnalysis.discovery (&mMap);
  if (changed && mOptions & cVERBOSE_DISCOVERY) {
    UtString b0, b1;
    mModule->getLoc ().compose (&b0);
    UtIO::cout () << "discovery on " << mModule->getName ()->str () 
      << ":\t" << b0 << "\n";
    mMap.compose (&b1, 2);
    UtIO::cout () << b1;
  }
  if (changed) {
    mMap.scratch_cycles (*this);
  }
  return changed;
}

//! diagnostic callback from ConcatAnalysis
/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_extract (const NUAssign *first, const NUAssign *last, 
  const UInt32 length, ConcatAnalysis::NetMap &netmap) const
{
  if (!(mOptions & cVERBOSE_EXTRACT)) {
    // quiet
  } else if (first == last) {
    NU_ASSERT (first == last, first);
    UtIO::cout () << "extract from single statement cluster" << UtIO::endl;
    UtString b0, b1;
    first->compose (&b0, NULL, 2, true);
    netmap.compose (&b1, ConcatAnalysis::NetMap::cWEIGHT|2);
    UtIO::cout () << b0;
    UtIO::cout () << b1;
  } else {
    NU_ASSERT (length > 1, first);
    UtIO::cout () << "extract from " << length << " statement cluster" << UtIO::endl;
    UtString b0, b1, b2;
    first->compose (&b0, NULL, 2, true);
    last->compose (&b1, NULL, 2, true);
    netmap.compose (&b2, ConcatAnalysis::NetMap::cWEIGHT|2);
    UtIO::cout () << b0;
    UtIO::cout () << b1;
    UtIO::cout () << b2;
  }
  if (first == last) {
    Log (cEXTRACT_STATEMENT, first, &netmap);
  } else {
    Log (cEXTRACT_COLLAPSER, first, last, length, &netmap);
  }
}

//! diagnostic callback from ConcatAnalysis
/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_cycles (GraphNodeSet &doomed) const
{
  if (mOptions & cVERBOSE_DISCOVERY) {
    UtIO::cout () << "deleting cyclic nodes:";
    for (GraphNodeSet::iterator it = doomed.begin (); it != doomed.end (); it++) {
      UtIO::cout () << " " << static_cast <ConcatAnalysis::NetMap::Node *> (*it)->image ();
    }
    UtIO::cout () << (char) UtIO::endl;
  }
  if (mOptions & cLOGGING) {
    for (GraphNodeSet::iterator it = doomed.begin (); it != doomed.end (); it++) {
      Log (cPORT_CONSTRAINT_CYCLE, mModule, static_cast <ConcatAnalysis::NetMap::Node *> (*it)->getNet ());
    }
  }
}

//! diagnostic callback from ConcatAnalysis
/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_forward_pruning (ConcatAnalysis::NetMap::Edge::Set &set) const
{
  if (mOptions & cVERBOSE_DISCOVERY) {
    UtIO::cout () << "removing forward branches:";
    for (ConcatAnalysis::NetMap::Edge::Set::iterator it = set.begin (); it != set.end (); it++) {
      UtString b;
      (*it)->compose (&b);
      UtIO::cout () << " " << b;
    }
    UtIO::cout () << (char) UtIO::endl;
  }
  if (mOptions & cLOGGING) {
    for (ConcatAnalysis::NetMap::Edge::Set::iterator it = set.begin (); it != set.end (); it++) {
      ConcatAnalysis::NetMap::Edge *edge = *it;
      Log (cPRUNE_FORWARD, edge->getFrom ()->getNet (), edge->getTo ()->getNet ());
    }
  }
}

//! diagnostic callback from ConcatAnalysis
/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_backward_pruning (ConcatAnalysis::NetMap::Edge::Set &set) const
{
  if (mOptions & cVERBOSE_DISCOVERY) {
    UtIO::cout () << "removing backward branches:";
    for (ConcatAnalysis::NetMap::Edge::Set::iterator it = set.begin (); it != set.end (); it++) {
      UtString b;
      (*it)->compose (&b);
      UtIO::cout () << " " << b;
    }
    UtIO::cout () << (char) UtIO::endl;
  }
  if (mOptions & cLOGGING) {
    for (ConcatAnalysis::NetMap::Edge::Set::iterator it = set.begin (); it != set.end (); it++) {
      ConcatAnalysis::NetMap::Edge *edge = *it;
      Log (cPRUNE_BACKWARD, edge->getFrom ()->getNet (), edge->getTo ()->getNet ());
    }
  }
}

/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_vectorisation (NUStmtList::iterator first, const UInt32 length) const
{
  if (mOptions & PortVectoriser::cVERBOSE_COLLAPSE) {
    UtIO::cout () << "Matched " << length << " assignments for port vectorisation:\n";
    Inference::dump (UtIO::cout (), first, length);
  }
}

/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_vector_edge (ConcatAnalysis::NetMap::Edge *edge) const
{
  if (mOptions & PortVectoriser::cVERBOSE_DISCOVERY) {
    UtString b;
    edge->compose (&b);
    UtIO::cout () << "discovered edge in " << mModule->getName ()->str () << ": " << b << UtIO::endl;
  }
}

/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_apply (NUModule *module) const
{
  if (mOptions & PortVectoriser::cVERBOSE_APPLY) {
    UtIO::cout () << "apply vectorisation to " << module->getName ()->str () << UtIO::endl;
  }
  Log (cAPPLY, module);
}

/*virtual*/ void PortVectoriser::BitnetConcatAnalysis::note_apply (NUModuleInstance *instance) const
{
  if (mOptions & PortVectoriser::cVERBOSE_APPLY) {
    UtString b0;
    instance->compose (&b0, NULL, 2, true);
    UtIO::cout () << "apply vectorisation to instance " << instance->getName ()->str ()
                  << " of " << instance->getModule ()->getName ()->str ()
                  << " in " << instance->getParent ()->getName ()->str () << UtIO::endl << b0;
  }
  Log (cAPPLY, instance);
}

PortVectoriser::FormalMap::FormalMap (ConcatAnalysis &context, NUModuleInstance *instance, bool verbose,
  PortVectoriser *parent, PortVectoriser *child) : 
  Binding::Map (), mInstance (instance), mVerbose (verbose), mParent (parent), mChild (child)
{
  NU_ASSERT (parent->getModule () == instance->getParentModule (), instance);
  NU_ASSERT (child->getModule () == instance->getModule (), instance);
  for (NUPortConnectionLoop loop = mInstance->loopPortConnections (); !loop.atEnd (); ++loop) {
    NUPortConnection *connection = *loop;
    NUNet *formal = connection->getFormal ();
    if (connection->isUnconnectedActual ()) {
      // the formal parameter is unbound at this instance
    } else if (!context.isInteresting (formal)) {
      // this is not a formal parameter that we can ever vectorise
    } else {
      switch (connection->getDir ()) {
      case eInput:
        {
          NUExpr *actual = dynamic_cast <NUPortConnectionInput *> (connection)->getActual ();
          switch (actual->getType ()) {
          case NUExpr::eNUIdentRvalue:
            insert (value_type (formal, new IdentBinding <eInput, NUIdentRvalue> (formal, 
                  dynamic_cast <NUIdentRvalue *> (actual))));
            break;
          case NUExpr::eNUVarselRvalue:
            insert (value_type (formal, new VarselBinding <eInput, NUVarselRvalue> (formal,
                  dynamic_cast <NUVarselRvalue *> (actual))));
            break;
          default:
            // Do not make an entry in the map. Initial winnowing will scratch
            // any corresponding parameters from consideration.
            break;
          }
        }
        break;
      case eOutput:
        {
          NULvalue *actual = dynamic_cast <NUPortConnectionOutput *> (connection)->getActual ();
          switch (actual->getType ()) {
          case eNUIdentLvalue:
            insert (value_type (formal, new IdentBinding <eOutput, NUIdentLvalue> (formal,
                  dynamic_cast <NUIdentLvalue *> (actual))));
            break;
          case eNUVarselLvalue:
            insert (value_type (formal, new VarselBinding <eOutput, NUVarselLvalue> (formal,
                  dynamic_cast <NUVarselLvalue *> (actual))));
            break;
          default:
            // Do not make an entry in the map. Initial winnowing will scratch
            // any corresponding parameters from consideration.
            break;
          }
        }
        break;
      case eBid:
        {
          NULvalue *actual = dynamic_cast <NUPortConnectionBid *> (connection)->getActual ();
          switch (actual->getType ()) {
          case eNUIdentLvalue:
            insert (value_type (formal, new IdentBinding <eBid, NUIdentLvalue> (formal,
                  dynamic_cast <NUIdentLvalue *> (actual))));
            break;
          case eNUVarselLvalue:
            insert (value_type (formal, new VarselBinding <eBid, NUVarselLvalue> (formal,
                  dynamic_cast <NUVarselLvalue *> (actual))));
            break;
          default:
            // Do not make an entry in the map. Initial winnowing will scratch
            // any corresponding parameters from consideration.
            break;
          }
        }
        break;
      default:
        NU_ASSERT (false, instance);
        break;
      }
    }
      
  }
}

//! Apply the vectorisation plan to the module body
bool PortVectoriser::apply ()
{ 
  // apply the vectorisation scheme to the module definition
  return mVectorisation.apply ();
}

//! Apply the vectorisation plan to module instantiations
bool PortVectoriser::applyToInstances ()
{
  bool retval = true;
  for (FormalMap::MultiMap::iterator it = mParents.begin (); it != mParents.end (); it++) {
    PortVectoriser *parent = it->first;
    NUModuleInstance *instance = it->second->getInstance ();
    if (!mVectorisation.apply (parent->mVectorisation, instance)) {
      retval = false;
    }
  }
  return retval;
}

//! \class SanityWalker
/*! Walks the design performing sanity checks on the transformed nucleus
 *  \note Currently this only checks that there are no nets marked for
 *  portvec replacement extant in the design.
 *  \note This test is run if cSTRICT is set by either -portVec strict or
 *  -portVec debug
 */

class SanityWalker : public NUDesignCallback {
public:
  SanityWalker (MsgContext *msg_context, const NUModule *module, const NUNetSet &replaced) : 
    mMsgContext (msg_context), mCrazy (false), mModule (module), mReplacedScalarNets (replaced)
  {}

  virtual ~SanityWalker () {}

  //! \return the result of sanity checking
  bool isSane () const { return !mCrazy; }

private:

  MsgContext *mMsgContext;            //!< hook for error messages
  bool mCrazy;                        //!< set when a problem is found
  const NUModule *mModule;            //!< module under analysis
  const NUNetSet &mReplacedScalarNets; //!< all the scalars that have been replaced

  virtual Status operator () (Phase, NUBase *)
  {
    return eNormal;
  }

  virtual Status operator () (Phase, NUNet *net)
  {
    if (mReplacedScalarNets.find (net) != mReplacedScalarNets.end ()) {
      mCrazy = true;
      mMsgContext->VEPortVecOrphanedScalar (mModule, mModule->getName ()->str (), net->getName ()->str ());
    }
    return eNormal;
  }

};

//! Run sanity checks on the modified module
bool PortVectoriser::isSane () const
{
  bool not_so_crazy = true;
  // first walk the module checking that there are no remaining references to
  // vectorised scalar nets
  NUNetSet replaced;
  mVectorisation.getEliminatedNets (&replaced);
  SanityWalker callback (mMsgContext, mModule, replaced);
  NUDesignWalker walker (callback, false);
  walker.module (mModule);
  not_so_crazy = not_so_crazy && callback.isSane ();
  return not_so_crazy;
}

PortVectoriser::FormalMap::~FormalMap ()
{
  for (iterator it = begin (); it != end (); it++) {
    delete it->second;
  }
}

void PortVectoriser::FormalMap::MultiMap::pr () const
{
  UtString b;
  compose (&b, 0);
  UtIO::cout () << b;
}

void PortVectoriser::FormalMap::MultiMap::compose (UtString *s, const UInt32 options) const
{
  UInt32 indent = options & 0xFF;       // indent in bottom eight bits
  UtString leaders (indent, ' ');
  for (const_iterator it = begin (); it != end (); it++) {
    FormalMap *map = it->second;
    NUModuleInstance *instance = map->getInstance ();
    UtString b;
    instance->getLoc ().compose (&b);
    *s << leaders << instance->getModule ()->getName ()->str () << ": " << b << (char) UtIO::endl;
    it->second->compose (s, indent+2);
  }
}

PortVectoriser::ActualMap::ActualMap (const FormalMap &formals, bool verbose,
  PortVectoriser *parent, PortVectoriser *child) :
  Binding::MultiMap (), mInstance (formals.getInstance ()), mVerbose (verbose),
  mParent (parent), mChild (child)
{
  NU_ASSERT (parent->getModule () == mInstance->getParentModule (), mInstance);
  NU_ASSERT (child->getModule () == mInstance->getModule (), mInstance);
  for (FormalMap::const_iterator it = formals.begin (); it != formals.end (); it++) {
    Binding *binding = it->second;
    if (binding->isScalar ()) {
      add (binding);
    }
  }
}

void PortVectoriser::ActualMap::MultiMap::pr () const
{
  UtString b;
  compose (&b, 0);
  UtIO::cout () << b;
}

void PortVectoriser::ActualMap::MultiMap::compose (UtString *s, const UInt32 options) const
{
  UInt32 indent = options & 0xFF;
  UtString leaders (indent, ' ');
  for (const_iterator it = begin (); it != end (); it++) {
    ActualMap *map = it->second;
    NUModuleInstance *instance = map->getInstance ();
    UtString b;
    instance->getLoc ().compose (&b);
    *s << leaders << instance->getName ()->str () << ": " << b << (char) UtIO::endl;
    it->second->compose (s, indent + 2);
  }
}

const SourceLocator &PortVectoriser::FormalMap::getLoc () const
{ 
  return mInstance->getLoc (); 
}

const SourceLocator &PortVectoriser::ActualMap::getLoc () const
{ 
  return mInstance->getLoc (); 
}

bool PortVectoriser::ActualMap::lookup (NUNet *net, ActualMap::iterator &begin, ActualMap::iterator &end)
{
  std::pair <iterator, iterator> it = equal_range (net);
  if (it.first == it.second) {
    return false;
  } else {
    begin = it.first;
    end = it.second;
    return true;
  }
}

bool PortVectoriser::FormalMap::lookup (NUNet *net, Binding **binding)
{
  iterator it = find (net);
  if (it == end ()) {
    return false;
  } else {
    *binding = it->second;
    return true;
  }
}

//! Scratch any formal parameters that can never be vectorised
/*! This winnowing is performed after construction of the parameter maps but
 *  before initial discovery. It identifies formal parameters that can never
 *  have a consistent vectorisation and scratches them from the graph.
 *
 *  For each formal parameter of the module, check that in every instance of
 *  the module there is an actual parameter binding. If no actual binding was
 *  constructed at the instance for this formal when the parameter maps were
 *  constructed, then the actual was neither a whole identifier nor a bitsel.
 *
 *  If a formal is scratched, discovery will never create an edge to or from
 *  the node for that formal, so no edge derived from the formal can ever
 *  propagate into or out of this module.
 */

void PortVectoriser::winnowNeverVectorisableFormals ()
{
  NUNetList ports;
  mModule->getPorts (&ports);
  for (FormalMap::MultiMap::iterator it = mParents.begin (); it != mParents.end (); it++) {
    FormalMap *actuals = it->second;
    for (NUNetList::iterator formal = ports.begin (); formal != ports.end (); formal++) {
      if (actuals->find (*formal) == actuals->end ()) {
        // There is no vectorisable binding for the formal in this instance so
        // scratch the port from the netmap for this module. This will occur if
        // the parameter is unbound at the instance or if it is not a whole
        // identifier or a bitsel.
        mMap.maybeScratchNet (*formal);
        if (mOptions & cVERBOSE_WINNOWING) {
          UtString b;
          actuals->getLoc ().compose (&b);
          UtIO::cout () << "winnow never vectorisable port " << (*formal)->getName ()->str ()
            << " at " << b << UtIO::endl;
        }
        Log (cWINNOW_NEVER_VECTORISABLE, actuals->getLoc (), mModule, *formal);
      }
    }
  }
}

//! Propagate vectorisation opportunities down the module hierarchy via an module instance
/*! \param edge is an edge between nets in the parent module.
 *  \param parent is the parent of the instance
 *  \param module is the instantiated module
 *  \note this is the multimap from actual parameters to bindings for the
 *  instance
 *  \note edge indicated that the two actual parameters are concat adjacent in
 *  the parent module.
 *  \note propagation pushes the edge down the hierarchy by potentially
 *  creating an edge in the child module for the formals that correspond to
 *  the actuals related by edge.
 */
bool PortVectoriser::ActualMap::propagate (PortVectoriser *module, PortVectoriser *parent,
  ConcatAnalysis::NetMap::Edge *edge, bool *scratch_edge, UInt32 *count, UInt32 *delta)
{
  bool changed = false;
  NUNet *from = edge->getFrom ()->getNet ();
  NUNet *to = edge->getTo ()->getNet ();
  iterator from_begin, from_end;
  iterator to_begin, to_end;
  if (!lookup (from, from_begin, from_end)) {
    return false;                       // no occurrences of from net as an actual
  } else if (!lookup (to, to_begin, to_end)) {
    return false;                       // no occurrences of to net as an actual
  }
  UInt32 n = 0;                         // counting the laps...
  UInt32 delta0 = 0;                    // counting the number of edges applied
  UInt64 t0 = OSGetRusageTime (true, true);
  // It's a multimap because an actual may occur multiple times in the module
  // instantiation. An edge is pushed down for each pair of occurrences.
  for (iterator from_it = from_begin; from_it != from_end; from_it++) {
    for (iterator to_it = to_begin; to_it != to_end; to_it++) {
      // There is a edge between two of the actual parameters in this module
      // instance. So first identify the nets that correspond to the formal
      // parameters in the child module.
      Binding *from_binding = from_it->second;
      Binding *to_binding = to_it->second;
      NUNet *from_formal = from_binding->getFormal ();
      NUNet *to_formal = to_binding->getFormal ();
      ConcatAnalysis::NetMap::Edge *new_edge = NULL;
      // Now try to add an edge between the formal paramters in the child
      // module.
      if (module->maybeAddEdge (from_formal, to_formal, &new_edge)) {
        delta0++;
        changed = true;
        Log (cPUSH_DOWN, parent->mModule, module->mModule, edge, from_binding, to_binding);
        if (mVerbose) {
          UtString b0, b1, b2;
          edge->compose (&b0);
          new_edge->compose (&b1);
          getLoc ().compose (&b2);
          UtIO::cout () << "push down " << b0 << " in " << parent->image ()
            << " to " << b1 << " in " << module->image () 
            << " at " << b2
            << UtIO::endl;
        }
      }
    }
  }
  UInt64 t1 = OSGetRusageTime (true, true);
  UInt32 elapsed = (t1 - t0) / 1000000;
  Log (cPROPAGATE_TIME, mInstance, elapsed);
  Log (cPROPAGATE_ACTUAL, mInstance, n, delta0);
  *count += n;
  *delta += delta0;
  // Down-propagation never explicitly scratches edges. Of course, if the
  // propagation induces a cycle then the propagation has discovered
  // conflicting vectorisations and the vectorisation opportunities are
  // eliminated during final structural winnowing.
  *scratch_edge = false;
  return changed;
}

//! Propagate vectorisation opportunities up the module hierarchy via and instance
/*! \param edge is an edge between nets in the child module
 *  \param parent is the parent of the instance
 *  \param module is the instantiated module
 *  \note this is the map from formap to binding in the instance
 *  \note The edge indicates that the formals of the module are potentially
 *  concat adjacent.
 *  \note propagation pushes edge up the module hierarchy potential inducing
 *  and edge between the actual parameters in the parent module.
 *  \noote if the actual parameters corresponding to the potentially concat
 *  adjacent formals do no vectorise then the vectorisation opportunities
 *  indicated by the edge does not vectorise in the parent. In this case,
 *  scratch_edge is set and the caller should scratch the edge from the graph.
 */
bool PortVectoriser::FormalMap::propagate (PortVectoriser *parent, PortVectoriser *module,
  ConcatAnalysis::NetMap::Edge *edge, bool *scratch_edge, UInt32 *count, UInt32 *delta)
{
  bool changed = false;
  NUNet *from = edge->getFrom ()->getNet ();
  NUNet *to = edge->getTo ()->getNet ();
  Binding *from_binding;
  Binding *to_binding;
  *scratch_edge = false;
  UInt32 n = 0;
  UInt32 delta0 = 0;
  // Look for the parameter binding
  if (!lookup (from, &from_binding)) {
    return false;                       // no binding for the from net
  } else if (!lookup (to, &to_binding)) {
    return false;                       // no binding for the to net
  } else if (from_binding->isScalar () && to_binding->isScalar ()) {
    // both nets in the parent module are scalars so propagate a possible
    // vectorisation to the parent
    delta0++;
    ConcatAnalysis::NetMap::Edge *new_edge;
    if (parent->maybeAddEdge (from_binding->getActual (), to_binding->getActual (), &new_edge)) {
      changed = true;
      Log (cPUSH_UP, module->mModule, parent->mModule, edge, from_binding, to_binding);
    }
    if (changed && mVerbose) {
      UtString b0, b1, b2;
      edge->compose (&b0);
      new_edge->compose (&b1);
      getLoc ().compose (&b2);
      UtIO::cout () << "push up " << b0 << " in " << module->image ()
        << " to " << b1 << " in " << parent->image () 
        << " at " << b2
        << UtIO::endl;
    }
  } else if (!from_binding->vectorisesWith (*parent, to_binding)) {
    // At least one of the actuals is either a bit sel or a partsel and it does
    // not vectorise with the other actuals. This indicates that the
    // vectorisation opportunity discovered in the child does not vectorise in
    // the parent. Set scratch_edge so that the caller scratches the edge from
    // the netmap.
    Log (cSCRATCH_UP, parent->mModule, module->mModule, edge, from_binding, to_binding);
    if (mVerbose) {
      UtString b0, b1;
      edge->compose (&b0);
      getLoc ().compose (&b1);
      UtIO::cout() << "scratch edge " << b0 << " in " << module->image ()
        << " because actuals do not vectorise at " << b1 << UtIO::endl;
    }
    *scratch_edge = true;
  } else {
    // The edge in the module is consistent with the actual parameters in the
    // parent
    *scratch_edge = false;
  }
  Log (cPROPAGATE_FORMAL, mInstance, n, delta0);
  n++;
  *count += n;
  *delta += delta0;
  return changed || *scratch_edge;
}

void PortVectoriser::addParent (PortVectoriser *parent, FormalMap *map)
{
  NU_ASSERT (map->getInstance ()->getModule () == mModule, map->getInstance ());
  NU_ASSERT (map->getInstance ()->getParentModule () == parent->getModule (), map->getInstance ());
  mParents.insert (FormalMap::MultiMap::value_type (parent, map));
  mFormalIndex.acrete (map);
}

void PortVectoriser::addChild (PortVectoriser *child, ActualMap *map)
{
  NU_ASSERT (map->getInstance ()->getModule () == child->getModule (), map->getInstance ());
  NU_ASSERT (map->getInstance ()->getParentModule () == mModule, map->getInstance ());
  mChildren.insert (ActualMap::MultiMap::value_type (child, map));
  mActualIndex.acrete (map);
}

//! Diagnostic output
void PortVectoriser::Binding::pr () const
{
  UtString b;
  compose (&b, 0);
  UtIO::cout () << b << UtIO::endl;
}

template <>
/*virtual*/ void PortVectoriser::IdentBinding <eInput, NUIdentRvalue>::compose (UtString *s, 
  const UInt32 indent) const
{
  s->append (indent, ' ');
  *s << ".";
  mFormal->compose (s, NULL);
  *s << " (";
  mActual->compose (s, NULL);
  *s << ")";
}

template <>
/*virtual*/ void PortVectoriser::IdentBinding <eOutput, NUIdentLvalue>::compose (UtString *s, 
  const UInt32 indent) const
{
  s->append (indent, ' ');
  *s << ".";
  mFormal->compose (s, NULL);
  *s << " (";
  mActual->compose (s, NULL, 0, false);
  *s << ")";
}

template <>
/*virtual*/ void PortVectoriser::IdentBinding <eBid, NUIdentLvalue>::compose (UtString *s, 
  const UInt32 indent) const
{
  s->append (indent, ' ');
  *s << ".";
  mFormal->compose (s, NULL);
  *s << " (";
  mActual->compose (s, NULL, 0, false);
  *s << ")";
}

template <>
/*virtual*/ void PortVectoriser::VarselBinding <eInput, NUVarselRvalue>::compose (UtString *s, 
  const UInt32 indent) const
{
  s->append (indent, ' ');
  *s << ".";
  mFormal->compose (s, NULL);
  *s << " (";
  mActual->compose (s, NULL);
  *s << ")";
}

template <>
/*virtual*/ void PortVectoriser::VarselBinding <eOutput, NUVarselLvalue>::compose (UtString *s, 
  const UInt32 indent) const
{
  s->append (indent, ' ');
  *s << ".";
  mFormal->compose (s, NULL);
  *s << " (";
  mActual->compose (s, NULL, 0, false);
  *s << ")";
}

template <>
/*virtual*/ void PortVectoriser::VarselBinding <eBid, NUVarselLvalue>::compose (UtString *s, 
  const UInt32 indent) const
{
  s->append (indent, ' ');
  *s << ".";
  mFormal->compose (s, NULL);
  *s << " (";
  mActual->compose (s, NULL, 0, false);
  *s << ")";
}

bool PortVectoriser::test (const NUIdentRvalue *a, const NUIdentRvalue *b) const
{
  bool retval = mMap.hasEdge (a->getIdent (), b->getIdent ());
  if (mOptions & cVERBOSE_TEST) {
    UtString b0, b1;
    a->compose (&b0, NULL);
    b->compose (&b1, NULL);
    UtIO::cout () << "test (" << b0 << ", " << b1 << ") return " << retval << UtIO::endl;
  }
  return retval;
}

bool PortVectoriser::test (const NUIdentLvalue *a, const NUIdentLvalue *b) const
{
  bool retval = mMap.hasEdge (a->getIdent (), b->getIdent ());
  if (mOptions & cVERBOSE_TEST) {
    UtString b0, b1;
    a->compose (&b0, NULL, 0, true);
    b->compose (&b1, NULL, 0, true);
    UtIO::cout () << "test (" << b0 << ", " << b1 << ") return " << retval << UtIO::endl;
  }
  return retval;
}

bool PortVectoriser::test (const NUVarselRvalue *a, const NUVarselRvalue *b) const
{
  bool retval;
  if (a == NULL || b == NULL) {
    retval = false;
  } else if (!a->isConstIndex () || !b->isConstIndex () || a->getConstIndex () != b->getConstIndex ()) {
    // TODO - extend to deal with indexed varsels
    retval =  false;
  } else if (a->getIdentExpr ()->getType () != NUExpr::eNUIdentRvalue
    || b->getIdentExpr ()->getType () != NUExpr::eNUIdentRvalue) {
    // just doing simple varsels for now
    retval = false;
  } else if (a->getIdent () != b->getIdent ()) {
    // varsels on different nets
    retval = false;
  } else if (a->getRange ()->getMsb () + 1 != b->getRange ()->getLsb ()) {
    // partitions do not abut
    retval = false;
  } else {
    retval = true;
  }
  if (mOptions & cVERBOSE_TEST) {
    UtString b0, b1;
    a->compose (&b0, NULL);
    b->compose (&b1, NULL);
    UtIO::cout () << "test (" << b0 << ", " << b1 << ") return " << retval << UtIO::endl;
  }
  return retval;
}

bool PortVectoriser::test (const NUVarselLvalue *a, const NUVarselLvalue *b) const
{
  bool retval;
  if (a == NULL || b == NULL) {
    retval = false;
  } else if (!a->isConstIndex () || !b->isConstIndex () || a->getConstIndex () != b->getConstIndex ()) {
    // TODO - extend to deal with indexed varsels
    retval = false;
  } else if (a->getLvalue ()->getType () != eNUIdentLvalue 
    || b->getLvalue ()->getType () != eNUIdentLvalue) {
    // just doing simple varsels for now
    retval = false;
  } else if (static_cast <NUIdentLvalue *> (a->getLvalue ())->getIdent ()
    != static_cast <NUIdentLvalue *> (b->getLvalue ())->getIdent ()) {
    // varsels on different nets
    retval = false;
  } else if (a->getRange ()->getMsb () + 1 != b->getRange ()->getLsb ()) {
    // partitions do not abut
    retval = false;
  } else {
    retval = true;
  }
  if (mOptions & cVERBOSE_TEST) {
    UtString b0, b1;
    a->compose (&b0, NULL, 0, true);
    b->compose (&b1, NULL, 0, true);
    UtIO::cout () << "test (" << b0 << ", " << b1 << ") return " << retval << UtIO::endl;
  }
  return retval;
}

void PortVectoriser::FormalMap::compose (UtString *s, const UInt32 indent) const
{
  UtString leaders (indent, ' ');
  NUScope *parent = mInstance->getParent ();
  NUModule *child = mInstance->getModule ();
  *s << leaders << "instance " << mInstance->getName ()->str ()
     << " of " << child->getName ()->str ()
     << " in " << parent->getName ()->str ()
     << "\n";
  for (const_iterator it = begin (); it != end (); it++) {
    Binding *binding = it->second;
    binding->compose (s, indent+2);
    if (binding->isScalar ()) {
      *s << " (scalar)";
    }
    *s << (char) UtIO::endl;
  }
}

void PortVectoriser::ActualMap::compose (UtString *s, const UInt32 indent) const
{
  for (const_iterator it = begin (); it != end (); it++) {
    Binding *binding = it->second;
    binding->compose (s, indent);
    if (binding->isScalar ()) {
      *s << " (scalar)";
    }
    *s << (char) UtIO::endl;
  }
}

void PortVectoriser::FormalMap::pr () const
{
  UtString b;
  compose (&b, 0);
  UtIO::cout () << b;
}

void PortVectoriser::ActualMap::pr () const
{
  UtString b;
  compose (&b, 0);
  UtIO::cout () << b;
}

//! Remove cycles and branches from the netmap
void PortVectoriser::structural_winnowing ()
{

  /* Cycles in the netmap indicate mutually inconsistent vectorisations. For
   *  example, module M below has netmap (a,b), module L has netmap (b,a):
   *
   *  module M (a, b, x);
   *    input a, b;
   *    output [1:0] x;
   *    // edge (a,b) is added to this modules netmap
   *    assign       x [1] = a;
   *    assign       x [0] = b;
   *  endmodule
   *
   *  module L (a, b, x);
   *    input a, b;
   *    output [1:0] x;
   *    // edge (b,a) is added to this modules netmap
   *    assign       x [1] = b;
   *    assign       x [0] = a;
   *  endmodule
   *
   *  When instantiated in N, propagation from M induces the edge (i,j),
   *  propagation from L induces the edge (j,i). (j,i) propagates down to M as
   *  the edge (b,a), and down to L as the edge (a,b), creating cycles in the
   *  netmaps for each of the three modules.

   *  module N (...);
   *    M m (i,j);
   *    L l (i,j);
   *  endmodule
   *
   *  The cycles in the netmaps indicate the conflicting
   *  vectorisations. Removing the cycles squashes the vectorisation of all of
   *  these nets.
   */

  mMap.remove_cycles (mBitNetAnalysis);

  /* Branches in the netmaps indicate ambiguity in the vectorisation
   * opportunities.
   *
   * Note: I think that initial winnowing, propagation and cycle elimination
   * will remove all branching opportunities. However, I have not yet come up
   * with either and argument sound enough to convince myself, nor a counter
   * examples. The only instance of a branch that I have seen resulted from a
   * vectorisation bug.
   *
   * TODO: turn off branch removal but leave strict branch checking enabled and
   * run it over all the test cases to see if a counter example magically pops
   * out.
   */

  mMap.remove_branches ();

  if (mOptions & cSTRICT) {
    // check for branches
    Iter <GraphNode *> it;
    bool failed = false;
    for (it = mMap.nodes (); !it.atEnd (); ++it) {
      ConcatAnalysis::NetMap::Node *node = static_cast <ConcatAnalysis::NetMap::Node *> (*it);
      if (node->outDegree () > 1) {
        Log (cBRANCH, mModule, node);
        failed = true;
        UtString b;
        node->compose (&b, ConcatAnalysis::NetMap::cLOCATION);
        mMsgContext->VEPortVecBranchInGraph (node->getNet (), 
          node->image (), mModule->getName ()->str (), b);
      }
    }
    if (failed) {
      UtString b;
      mMap.compose (&b, ConcatAnalysis::NetMap::cLOCATION|2);
      UtIO::cout () << b;
    }
    NU_ASSERT (!failed, mModule);
  }

}

// diagnostics
// ***********

/*static*/ void PortVectoriser::Options::compose (UtString *b) const
{
  UInt32  n = 0;
#define PORTVEC_OPTION(_tag_, _bit_) \
  if (!(mOptions & c##_tag_)) { \
  } else { \
    if (n > 0) { \
      *b << "|"; \
    } \
    *b << #_tag_; \
    n++; \
  }
#include "vectorise/PortVectorisation.def"
  if (mArgs & cREPORT_FILE) {
    *b << "+REPORT_FILE";
  }
  if (mArgs & cREPORT_STDOUT) {
    *b << "+REPORT_STDOUT";
  }
  if (mReportFile != NULL) {
    *b << ", report=" << mReportFileName;
  }
  if (mArgs & cSUFFIX) {
    *b << ", suffix=" << mSuffix;
  }
  if (mArgs & cMIN_CLUSTER_SIZE) {
    *b << ", mincluster=" << mMinCluserSize;
  }
}

void PortVectoriser::Options::pr () const
{
  UtString b;
  compose (&b);
  UtIO::cout () << b << UtIO::endl;
}

/*static*/ bool PortVectoriser::Options::cvt (const char *s, UInt32 *option)
{
#define PORTVEC_OPTION(_tag_, _bit_) \
  if (!strcasecmp (s, #_tag_)) { \
    *option |= PortVectoriser::c##_tag_;                             \
    return true; \
  }
#define PORTVEC_OPTION_MASK(_tag_, _mask_) PORTVEC_OPTION (_tag_, _mask_)
#include "vectorise/PortVectorisation.def"
  return false;
}

/*static*/ OccurrenceLogger *PortVectoriser::sLog = NULL;

//! Log file manager for CommonConcat.
/*! Provides logging of common concat instances. */
class PortVectoriserLogger : public OccurrenceLogger {
public:
  PortVectoriserLogger (MsgContext &msg_context, const char *root, const char *ident, const UInt32 flags) : 
    OccurrenceLogger (msg_context, root, ident, flags) {}
  virtual ~PortVectoriserLogger () {}
  virtual const char *keyToName (const UInt32 n) const
  {
    switch (n) {
#define PORTVEC_OCCURRENCE(_tag_) case PortVectoriser::c##_tag_: return #_tag_;
#include "vectorise/PortVectorisation.def"
    default: return "###error###";
    }
  }
};

//! Open the concat log file building the filename from root
/*static*/ void PortVectoriser::openOccurrenceLog (MsgContext &msg_context, const char *root,
  const char *ident)
{
  if (sLog == NULL) {
    sLog = new PortVectoriserLogger (msg_context, root, ident, OccurrenceLogger::cSUMMARY);
    Log.bind (sLog);
  }
}

//! Close the concat log file
/*static*/ void PortVectoriser::closeOccurrenceLog ()
{
  if (sLog != NULL) {
    sLog->summarise ();
    sLog->summarise (UtIO::cout ());
    Log.bind ();
    delete sLog;
    sLog = NULL;
  }
}

void PortVectoriser::verbose ()
{
  // the vectorisable ports are embedded the non-trivial weakly connected components
  mVectorisation.verbose ();
}

void PortVectoriser::compose (UtString *s, const UInt32 options) const
{
  UInt32 indent = options & cINDENT_MASK;
  UtString leaders (indent, ' ');
  if (!(options & cNO_HEADING)) {
    *s << leaders << "Port vectoriser context for " << mModule->getName ()->str () << (char) UtIO::endl;
  }
  if (options & cNETMAP) {
    mMap.compose (s);
  }
  if (!(options & cFORMAL_MAP)) {
    // do not emit the formal parameter maps
  } else if (mParents.empty ()) {
    *s << leaders << "  Formal maps (mParents): <empty>" << (char) UtIO::endl;
  } else {
    *s << leaders << "  Formal maps (mParents): " << (char) UtIO::endl;
    mParents.compose (s, indent + 4);
  }
  if (!(options & cACTUAL_MAP)) {
    // do not emit the actual parameter maps
  } else if (mChildren.empty ()) {
    *s << leaders << "  Actual maps (mChildren): <empty>" << (char) UtIO::endl;
  } else {
    *s << leaders << "  Actual maps (mChildren): " << (char) UtIO::endl;
    mChildren.compose (s, indent + 4);
  }
}
