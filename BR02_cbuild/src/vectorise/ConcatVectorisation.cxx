// -*-C++-*-    $Revision: 1.6 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "exprsynth/ExprFactory.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "reduce/Inference.h"
#include "reduce/REUtil.h"
#include "util/ArgProc.h"
#include "util/UtIOStream.h"
#include "util/UtIndent.h"
#include "util/GraphSCC.h"
#include "util/GraphWCC.h"
#include "vectorise/Collapser.h"
#include "vectorise/ConcatAnalysis.h"
#include "vectorise/ConcatVectorisation.h"
#include "vectorise/PortVectorisation.h"

ConcatVectorisation::~ConcatVectorisation ()
{
  if (mSummary == NULL) {
    // do not write the summary
  } else if (mVectorNets.empty ()) {
    // did not create any vector nets in this model
  } else {
    *mSummary << "Made " << mVectorNets.size () << " vector nets in " 
      << mModule->getName ()->str () << ":" << UtIO::endl;
    for (VectorNet::List::iterator vectors = mVectorNets.begin (); vectors != mVectorNets.end (); vectors++) {
      VectorNet *vector = *vectors;
      const char *vector_name = vector->getNet ()->getName ()->str ();
      for (ScalarNet::Vector::const_iterator scalars = vector->begin (); scalars != vector->end (); scalars++) {
        ScalarNet *scalar = *scalars;
        *mSummary << "  " << scalar->getNet ()->getName ()->str () << ": "
          << vector_name << "[" << scalar->getBit () << "]" << UtIO::endl;
      }
    }
  }
  for (ScalarNet::Map::iterator it = mScalarNets.begin (); it != mScalarNets.end (); it++) {
    delete it->second;
  }
  for (VectorNet::List::iterator it = mVectorNets.begin (); it != mVectorNets.end (); it++) {
    delete *it;
  }
}

//! Construct an instance for a scalar net that will be replaced
void ConcatVectorisation::makeScalar (VectorNet *vector, NUNet *net, const UInt32 bit)
{
  NU_ASSERT (vector->getBitSize () == bit, net);
  ScalarNet *scalar = new ScalarNet (net, vector, bit);
  mScalarNets.insert (ScalarNet::Map::value_type (net, scalar));
  vector->append (scalar);
}

ConcatVectorisation::ScalarNet::~ScalarNet ()
{
  if (mReplaced) {
    // The scalar net has been replaced in the design. If it was a port, then
    // demote it to a local.
    NetFlags flags = mNet->getFlags ();
    if (flags & ePortMask) {
      // The net should not be deleted, however it is no longer in the set of
      // ports for the module, so just add it to the local nets of the scope.
      mNet->setFlags (NetFlags (flags & ~ePortMask)); // reset the port bits
      mNet->getScope ()->addLocal (mNet); // make it local
    }
  }
}

//! Construct an instance of a new vector net;
ConcatVectorisation::VectorNet *ConcatVectorisation::makeVector (NUNetList &trace)
{
  VectorNet *vector = new VectorNet (mConcatAnalysis, mModule, mVerbose);
  mVectorNets.push_back (vector);
  for (NUNetList::iterator it = trace.begin (); it != trace.end (); it++) {
    makeScalar (vector, *it, vector->getBitSize ());
  }
  return vector;
}

//! \class VectorTraceWalker
/*! Once a vectorisation component has been extracted, as a weakly
 *  connected component of the netmap for a module, this subgraph will be a
 *  list of nets. A reverse depth first traversal of this graph yield the scalar
 *  nets, in bit order, for vectorisation.
 */
class VectorTraceWalker : public GraphWalker {
public:

  VectorTraceWalker (ConcatVectorisation &vectorisation, NUNetList *trace) : 
    mVectorisation (vectorisation), mTrace (trace), mContainsPrimaryPorts (false)
  {}

  virtual ~VectorTraceWalker () {}

  //! visit the next net in the set of vectorisable nets.
  virtual Command visitNodeBefore (Graph *, GraphNode *gnode)
  {
    ConcatAnalysis::NetMap::Node *node = static_cast <ConcatAnalysis::NetMap::Node *> (gnode);
    NUNet *net = node->getNet ();
    NU_ASSERT (node->outDegree () <= 1, net);
    if (net->isPrimaryPort ()) {
      mContainsPrimaryPorts = true;
    }
    mTrace->push_back (net);
    return GW_CONTINUE;
  }

  //! the traversel must start at the first bit
  virtual bool isValidStartNode (Graph *, GraphNode *gnode)
  {
    ConcatAnalysis::NetMap::Node *node = static_cast <ConcatAnalysis::NetMap::Node *> (gnode);
    return node->isSource ();
  }

  //! \return whether the trace contains primary ports
  bool containsPrimaryPorts () const { return mContainsPrimaryPorts; }

private:

  ConcatVectorisation &mVectorisation;  //!< handle onto the vectorisation scheme
  NUNetList *mTrace;                    //!< the ordered list of vectorised ports
  bool mContainsPrimaryPorts;           //!< set when a primary port is found in the trace

  // forbidden...
  VectorTraceWalker (const VectorTraceWalker &);
  VectorTraceWalker &operator = (const VectorTraceWalker &);

};

bool ConcatVectorisation::construct (ConcatAnalysis::NetMap &port_map, const UInt32 min_cluster_size,
  const UInt32 options)
{
  // The remaining weakly connected components of the port map are vectorisable
  // sets of port.
  GraphWCC wcc;
  wcc.compute (&port_map);
  for (GraphWCC::ComponentLoop loop = wcc.loopComponents (); !loop.atEnd (); ++loop) {
    GraphWCC::Component *component = *loop;
    if (component->size () < min_cluster_size) {
      // component is too small to vectorise
      continue;
    }
    ConcatAnalysis::NetMap *subgraph = static_cast <ConcatAnalysis::NetMap *> (wcc.extractComponent (component));
    NUNetList trace;
    VectorTraceWalker walker (*this, &trace);
    walker.walk (subgraph);
    if (!walker.containsPrimaryPorts () || (options & cVECTORISE_PRIMARY)) {
      // The subgraph may contain primary ports even if vectorisation of
      // primaries is not performed. The user can allow propagation of
      // vectorisations through primaries while not allowing vectorisation of
      // primary ports.
      makeVector (trace);
    }
    delete subgraph;
  }
  return !mVectorNets.empty ();
}

//! \return iff a scalar net has been replaced in the vectorisation
/*! \param scalar the scalar net under consideration
 *  \param vector if replaced, this returns the new vector net
 *  \param bit if replaced, this is the bit position in the vector
 */
bool ConcatVectorisation::isReplaced (NUNet *scalar_net, NUNet **vector_net, SInt32 *bit) const
{
  ScalarNet::Map::const_iterator it;
  if ((it = mScalarNets.find (scalar_net)) == mScalarNets.end ()) {
    return false;
  } else {
    ScalarNet *scalar = it->second;
    VectorNet *vector = scalar->getVector ();
    *vector_net = vector->getNet ();
    *bit = scalar->getBit ();
    return true;
  }
}

//! \class VectorisedNetMapper
/*! Functor for replace leaves that maps scalar nets to bit positions in vector nets.
 */

class VectorisedNetMapper : public NuToNuFn {
public:

  VectorisedNetMapper (ConcatVectorisation &vectorisation, bool verbose) :
    NuToNuFn (), mVectorisation (vectorisation), mVerbose (verbose)
  {}

  virtual ~VectorisedNetMapper () {}

private:

  ConcatVectorisation &mVectorisation;
  bool mVerbose;                        // noisy or quiet

  virtual NUExpr * operator() (NUExpr *, Phase);
  virtual NULvalue * operator() (NULvalue *, Phase);

};

//! Map scalar nets to bitsels for replaceLeaves
/*virtual*/ NUExpr *VectorisedNetMapper::operator () (NUExpr *expr, Phase)
{
  NUNet *vector;                        // vector net that replaces the scalar
  SInt32 bit;                           // bit position in that vector net
  switch (expr->getType ()) {
  case NUExpr::eNUIdentRvalue:
    {
      NUIdentRvalue *value;
      if ((value = dynamic_cast <NUIdentRvalue *> (expr)) == NULL) {
        // if getType says it is an ident, then, damn it, it should be an ident!
        NU_ASSERT (value != NULL, expr);
        return NULL;
      } else if (!mVectorisation.isReplaced (value->getIdent (), &vector, &bit)) {
        // this is not a scalar that was tagged for vectorisation
        return NULL;
      } else {
        ConstantRange range (bit, bit);
        NUExpr *replacement = new NUVarselRvalue (vector, range, expr->getLoc ());
        replacement->resize (expr->getBitSize ());
        if (mVerbose) {
          UtString b0, b1;
          expr->compose (&b0, NULL);
          replacement->compose (&b1, NULL);
          UtIO::cout () << "replace " << b0 << " with " << b1 << "\n";
        }
        delete expr;
        return replacement;
      }
    }
    break;
#if PORTVEC_SINGLE_BIT_VECTORS
    // under construction
  case NUExpr::eNUVarselRvalue:
    {
      NUVarselRvalue *value;
      NUIdentRvalue *ident;
      if ((value = dynamic_cast <NUVarselRvalue *> (expr)) == NULL) {
        // if getType says it is a varsel, then, damn it, it should be an ident!
        NU_ASSERT (value != NULL, expr);
        return NULL;
      } else if ((ident = dynamic_cast <NUIdentRvalue *> (value->getIdentExpr ())) == NULL) {
        // varsel is, most likely, a memsel
        return NULL;
      } else if (!mVectorisation.isReplaced (value->getIdent (), &vector, &bit)) {
        // this is not a scalar that was tagged for vectorisation
        return NULL;
      } else {
        NU_ASSERT (value->getIdentNet ()->getBitSize () == 1, value->getIdentNet ());
        ConstantRange range (bit, bit);
        NUExpr *replacement = new NUVarselRvalue (vector, range, expr->getLoc ());
        replacement->resize (expr->getBitSize ());
        if (mVerbose) {
          UtString b0, b1;
          expr->compose (&b0, NULL);
          replacement->compose (&b1, NULL);
          UtIO::cout () << "replace " << b0 << " with " << b1 << "\n";
        }
        delete expr;
        return replacement;
      }
    }
    break;
#endif
  default:
    return NULL;
    break;
  }
}

/*virtual*/ NULvalue *VectorisedNetMapper::operator () (NULvalue *lvalue, Phase)
{
  NUNet *vector;                        // vector net that replaces the scalar
  SInt32 bit;                           // bit position in that vector net
  switch (lvalue->getType ()) {
  case eNUIdentLvalue:
    {
      NUIdentLvalue *value;
      if ((value = dynamic_cast <NUIdentLvalue *> (lvalue)) == NULL) {
        NU_ASSERT (value != NULL, lvalue);
        return NULL;
      } else if (!mVectorisation.isReplaced (value->getIdent (), &vector, &bit)) {
        // this is not a scalar that was tagged for vectorisation
        return NULL;
      } else {
        ConstantRange range (bit, bit);
        NULvalue *replacement = new NUVarselLvalue (vector, range, lvalue->getLoc ());
        if (mVerbose) {
          UtString b0, b1;
          lvalue->compose (&b0, NULL, 0, true);
          replacement->compose (&b1, NULL, 0, true);
          UtIO::cout () << "replace " << b0 << " with " << b1 << "\n";
        }
        delete lvalue;
        return replacement;
      }
    }
    break;
#if PORTVEC_SINGLE_BIT_VECTORS
    // under construction
  case eNUVarselLvalue:
    {
      NUVarselLvalue *value;
      if ((value = dynamic_cast <NUVarselLvalue *> (lvalue)) == NULL) {
        NU_ASSERT (value != NULL, lvalue);
        return NULL;
      } else if (!mVectorisation.isReplaced (value->getIdentNet (), &vector, &bit)) {
        // this is not a scalar that was tagged for vectorisation
        return NULL;
      } else {
        NU_ASSERT (value->getIdentNet ()->getBitSize () == 1, value->getIdentNet ());
        ConstantRange range (bit, bit);
        NULvalue *replacement = new NUVarselLvalue (vector, range, lvalue->getLoc ());
        if (mVerbose) {
          UtString b0, b1;
          lvalue->compose (&b0, NULL, 0, true);
          replacement->compose (&b1, NULL, 0, true);
          UtIO::cout () << "replace " << b0 << " with " << b1 << "\n";
        }
        delete lvalue;
        return replacement;
      }
    }
    break;
#endif
  default:
    return NULL;
    break;
  }
}

bool ConcatVectorisation::apply ()
{
  // should only call this when there is a non-degenerate schema
  NU_ASSERT (!mVectorNets.empty (), mModule);
  mConcatAnalysis.note_apply (mModule);
  // freeze all the vector nets
  for (VectorNet::List::iterator it = mVectorNets.begin (); it != mVectorNets.end (); it++) {
    // Construct all the new vector nets for the module. Once this net has been
    // created, the destructor for the VectorNet will delete all the original
    // scalar nets.
    (*it)->freeze (mModule);
  }
  
  // construct a new port list
  NUNetList old_ports;                  // original port list of the module
  NUNetVector new_ports;                // the new set of ports
  mModule->getPorts (&old_ports);
  for (NUNetList::iterator it = old_ports.begin (); it != old_ports.end (); it++) {
    NUNet *net = *it;
    ScalarNet::Map::iterator scalar;
    UtString b0;
    net->composeUnelaboratedName (&b0);
    if ((scalar = mScalarNets.find (net)) == mScalarNets.end ()) {
      // this port is unaffected by vectorisation
      new_ports.push_back (net);
    } else if (scalar->second->isMsb ()) {
      // this scalar corresponds to the msb of the new vector, this is the
      // location in the port list that the new vector port gets inserted.
      new_ports.push_back (scalar->second->getVector ()->getNet ());
    } else {
      // this net will be thrown out
    }
  }
  mModule->replacePorts (new_ports);
  // use replace leaves to rewrite the module definition
  VectorisedNetMapper netmapper (*this, mVerbose);
  mModule->replaceLeaves (netmapper);
  return true;
}

//! \class PortVecInstanceMangler
/*! Rewrites the instantiation of a module
 */

class PortVecInstanceMangler {
public:

  //! PortVecInstanceMangler

  PortVecInstanceMangler (
    MsgContext *msg_context,
    ConcatAnalysis &context, 
    ConcatVectorisation &parent, //!< vectorisation of the parent
    ConcatVectorisation &child,  //!< vectorisation of the child
    NUModuleInstance *instance) :          //!< instance to rewrite
    mMsgContext (msg_context), mConcatAnalysis (context), 
    mParent (parent), mChild (child), mInstance (instance)
  {
    // abandon all hope is child is not the child, or parent is not the parent
    NU_ASSERT (instance->getModule () == child.getModule (), instance);
    NU_ASSERT (instance->getParentModule () == parent.getModule (), instance);
  }

  ~PortVecInstanceMangler () { }

  //! return the instance that is under the knife
  NUModuleInstance *getInstance () const { return mInstance; }

  //! apply the mangler to the instance
  bool apply ()
  {
    NUModule *module = mInstance->getModule ();
    NUPortConnectionList old_connections;
    NUPortConnectionVector new_connections (module->getNumPorts ());
    mInstance->getPortConnections (old_connections);
    bool retval = true;
    // We need to construct, for each vectorised actual parameter, a mapping
    // from bit position to the old actual parameter. map aggregates the maps
    // for each vectorised actual. The members of map are mappings from bit
    // positions to old actual
    ActualMap map (mMsgContext, mConcatAnalysis, mInstance);
    // A connection can either be left alone if the corresponding formal
    // paramter was not vectorised in the child module, or a vectorised actual
    // parameter must be formed from the collection of actual parameters.
    for (NUPortConnectionList::iterator it = old_connections.begin (); it != old_connections.end (); it++) {
      NUPortConnection *connection = *it;
      NUNet *formal = connection->getFormal ();
      NUNet *new_formal_net;
      UInt32 new_formal_bit;
      if (!mChild.lookup (formal, &new_formal_net, &new_formal_bit)) {
        // The formal was not vectorised in the child module so just put it
        // into the new connections vector.
        SInt32 port_index = module->getPortIndex (formal);
        NU_ASSERT (port_index >= 0, connection);
        new_connections [port_index] = connection;
        continue;
      }
      // This connection corresponds to a formal parameter that has been
      // vectorised. So first get the mapping for the new formal parameter at
      // this instance.
      Actual *actual = map.getActual (new_formal_net, connection->getDir ());
      if (connection->getDir () != actual->getDir ()) {
        // looks like a pair of incompatible nets have been vectorised
        mMsgContext->VEPortVecBadDirection (connection->getFormal ()->getName ()->str ());
        retval = false;
      } else {
        // Add the connection to the map for the new actual parameter. The
        // parameter is construction once all the old connections have been
        // examined.
        NU_ASSERT (actual->find (new_formal_bit) == actual->end (), mInstance);
        actual->insert (Actual::value_type (new_formal_bit, connection));
      }
    }
    // Now we have a map from bit position to old actual parameter for each of
    // the new vector parameters of the child module. So iterate over the 
    for (ActualMap::iterator it = map.begin (); it != map.end (); it++) {
      NUNet *formal = it->first;        // the new formal parameter
      Actual *actual = it->second;      // the mapping from bit position to old binding
      // construct the new connection
      NUPortConnection *new_connection = NULL;
      switch (actual->getDir ()) {
      case eInput:
        {
          NUPortConnectionInput *input_connection =
            actual->vectorise <eInput, NUPortConnectionInput, NUVarselRvalue, 
              NUIdentRvalue, NUExprVector, NUExprVector::iterator, NUConcatOp> ();
          if (input_connection != NULL) {
            // The rvalue input connections are the only type of connection
            // that must be resized, so resizing cannot hide inside the
            // templated vectorisation method. Vectorisation may have failed,
            // so we need to check the return value from vectorise before
            // resizing.
            input_connection->getActual ()->resize (formal->getBitSize ());
          }
          new_connection = input_connection;
        }
        break;
      case eOutput:
        new_connection = 
          actual->vectorise <eOutput, NUPortConnectionOutput, NUVarselLvalue, 
            NUIdentLvalue, NULvalueVector, NULvalueVector::iterator, NUConcatLvalue> ();
        break;
      case eBid:
        new_connection = 
          actual->vectorise <eBid, NUPortConnectionBid, NUVarselLvalue, NUIdentLvalue, 
            NULvalueVector, NULvalueVector::iterator, NUConcatLvalue> ();
        break;
      default:
        NU_ASSERT (false, mInstance);
        break;
      }
      if (new_connection == NULL) {
        // construction failed
        mMsgContext->VEPortVecActualConstructionFailed (mInstance->getName ()->str (),
          formal->getName ()->str (), mInstance->getParent ()->getName ()->str ());
        retval = false;
      } else {
        // wire the connection into the vector of new connections
        new_connection->setModuleInstance (mInstance);
        SInt32 port_index = module->getPortIndex (actual->getFormal ());
        NU_ASSERT (port_index >= 0, new_connection);
        new_connections [port_index] = new_connection;
      }
    }
    // bind the new set of connections to the instance
    mInstance->setPortConnections (new_connections);
    return retval;
  }

private:

  //! \class Actual
  /*! Maps the bit position of a connect in the new formal vectorised formal
   *   parameter to the port connection to the original scalar formal.
   */

  class Actual : public UtMap <UInt32, NUPortConnection *> {
  public:

    Actual (
      MsgContext *msg_context,
      ConcatAnalysis &concat_analysis, 
      NUModuleInstance *instance, 
      NUNet *formal, PortDirectionT direction) : 
      UtMap <UInt32, NUPortConnection *> (), 
      mMsgContext (msg_context),
      mConcatAnalysis (concat_analysis),
      mInstance (instance),
      mFormal (formal), 
      mDirection (direction)
    {}

    virtual ~Actual ()
    {
      for (iterator it = begin (); it != end (); it++) {
        delete it->second;
      }
    }

    //! return the formal parameter
    NUNet *getFormal () const { return mFormal; }

    //! form a human readable representation
    void compose (UtString *s) const
    {
      for (const_iterator it = begin (); it != end (); it++) {
        UtString b;
        it->second->compose (&b, NULL, 0, false);
        *s << it->first << ": " << b << (char) UtIO::endl;
      }
    }

    //! diagnostic output
    virtual void pr () const
    // make it a virtual to stop the compiler from inlining pr... I want to use if from the debugger
    {
      UtString b;
      compose (&b);
      UtIO::cout () << b;
    }

    //! \return the direction of the connection
    PortDirectionT getDir () const { return mDirection; }


    //! Construct the vectorised port connection
    template <PortDirectionT _dir,      //!< eInput, eOutput or eBid
              class _Connection,        //!< subclass of NUPortConnection
              class _Varsel,            //!< either NUVarselLvalue or NUVarselRvalue
              class _Ident,             //!< either NUIdentLvalue or NUIdentRvalue
              class _Vector,            //!< either NULvalueVector or NUExprVector
              class _VectorIterator,    //!< iterator over _Vector
              class _Concat>            //!< either NUConcatLvalue or NUConcatOp
    _Connection *vectorise ()
    {
      NUNet *net = NULL;
      SInt32 lsb = 0;
      SInt32 msb = 0;
      _Vector concat;
      UInt32 size = 0;                  // cumulative size of the concat
      bool use_concat = false;
      bool use_vector = false;
      // iterate across the port connections in reverse bit order
      for (reverse_iterator it = rbegin (); it != rend (); it++) {
        // Reverse iteration so that, for constructed concats, the bits are
        // pushed onto the back of the vector in the correct order.
        NUPortConnection *generic = it->second;
        _Connection *connection;
        // Verify that the connection is the right direction and the right
        // type. If this fails then something was screwed up way in the
        // construction of the vectorisation scheme. Recover would not be
        // possible from here.
        if (generic->getDir () != _dir) {
          // one of the connections is in a different direction... not allowed
          mMsgContext->VEPortVecActualConstructionBadDirection (generic);
          return NULL;
        } else if ((connection = dynamic_cast <_Connection *> (generic)) == NULL) {
          // did not cast to the correct flavour
          mMsgContext->VEPortVecActualConstructionBadType (generic);
          return NULL;
        }
        // If the original actual parameter is a contiguous set of bitsels of
        // the same net, then we can construct a partsel as the actual
        // parameter, otherwise a concat is necessary.
        _Varsel *varsel = dynamic_cast <_Varsel *> (connection->getActual ());
        _Ident *ident = dynamic_cast <_Ident *> (connection->getActual ());
        NU_ASSERT (ident != NULL || varsel != NULL, connection);
        if (ident == NULL) {
          // push the varsel onto the concat vector in case this turns out to
          // be a mixed actual parameter
          concat.push_back (varsel);
        } else {
          // It is a whole identifier so add it to the vector for
          // concatentation.
          NUNet *net = ident->getWholeIdentifier ();
          size += net->getBitSize ();
          concat.push_back (ident);
          use_concat = true;
        }
        if (varsel == NULL) {
          // Not a varsel
          NU_ASSERT (ident != NULL, generic);
        } else if (net == NULL) {
          // The lsb of the varsel
          net = varsel->getIdentNet ();
          lsb = varsel->getRange ()->getLsb ();
          msb = lsb;
          use_vector = true;
        } else if (varsel->getIdentNet () != net) {
          // it is a varsel of a different net so a concat is needed
          use_concat = true;
        } else if (varsel->getRange ()->getLsb () + 1 != lsb) {
          // The bits are not contiguous.
          use_concat = true;
        } else {
          // Still looking good for the partition
          lsb = varsel->getRange ()->getLsb () ;
          use_vector = true;
        }
      }
      // The original connections corresponding to the old non-vectorised
      // scalar formal parameters have been scanned. Now we need to construct
      // the connection to the new vectorised formal parameter. It's either a
      // varsel, or a concat. Note that concats can occur when non-vectorised
      // primary ports are used as actual paremeters.
      if (use_concat) {
        return makeConnection <_Connection, _Vector, _VectorIterator, _Concat> (concat);
      } else {
        return makeConnection <_Connection, _Varsel, _Ident> (msb, lsb, net);
      }
    }

    //! make a new connection where the actual is a varsel
    template
    <class _Connection, class _Varsel, class _Ident>
    _Connection *makeConnection (SInt32 msb, SInt32 lsb, NUNet *net)
    {
      UInt32 width = (UInt32) (msb - lsb) + 1;
      if (net->getBitSize () == width) {
        // it is the whole net so form an ident rather than a varsel
        _Ident *new_actual = new _Ident (net, mInstance->getLoc ());
        return new _Connection (new_actual, mFormal, mInstance->getLoc ());
      } else {
        // not the whole net so a partition is required
        ConstantRange range (msb, lsb);
        _Varsel *new_actual = new _Varsel (new _Ident (net, mInstance->getLoc ()), 
          net->makeRange (range), mInstance->getLoc ());
        return new _Connection (new_actual, mFormal, mInstance->getLoc ());
      }
    }

    //! make a new connection where the actual is a concat
    template
    <class _Connection, class _Vector, class _VectorIterator, class _Concat>
    _Connection *makeConnection (_Vector &concat)
    {
      CopyContext ctx (NULL, NULL);
      _Vector copies;
      for (_VectorIterator item = concat.begin (); item != concat.end (); item++) {
        copies.push_back ((*item)->copy (ctx));
      }
      // Form a concat
      _Concat *new_actual = makeConcat <_Vector, _Concat> (copies);
      return new _Connection (new_actual, mFormal, mInstance->getLoc ());
    }

    //! Construct the appropriate flavour of concat.
    /*! This must be templated, with a explicit implementation for NUConcatOp
     *   and NUConcatLvalue because the constructors for these classes differ.
     */
    template <class _Vector, class _Concat> _Concat *makeConcat (_Vector &);

  private:

    MsgContext *mMsgContext;            //!< error message handle
    ConcatAnalysis &mConcatAnalysis;    //!< parent context
    NUModuleInstance *mInstance;        //!< enclosing instance 
    NUNet *mFormal;                     //!< formal parameter
    const PortDirectionT mDirection;    //!< direction of the connection

  };
  
  //! \class ActualMap
  /*! This maps the new vector formal parameters of the child module to the
   *  Actual instance under construction by the instance mangler.
   */
  class ActualMap : public UtMap <NUNet *, Actual *> {
  public:

    ActualMap (MsgContext *msg_context, 
      ConcatAnalysis &concat_analysis, 
      NUModuleInstance *instance) : 
      UtMap <NUNet *, Actual *> (), 
      mMsgContext (msg_context), mConcatAnalysis (concat_analysis), mInstance (instance)
    {}

    virtual ~ActualMap ()
    {
      // blow them all away...
      for (iterator it = begin (); it != end (); it++) {
        delete it->second;
      }
    }

    //! \return the representation for a formal parameter
    Actual *getActual (NUNet *net, PortDirectionT direction)
    {
      iterator found;
      if ((found = find (net)) != end ()) {
        // already created
        return found->second;
      } else {
        // first reference to this formal, so create, install and return the
        // actual parameter representation
        Actual *actual = new Actual (mMsgContext, mConcatAnalysis, mInstance, net, direction);
        insert (value_type (net, actual));
        return actual;
      }
    }

  private:

    MsgContext *mMsgContext;            //!< error message handle 
    ConcatAnalysis &mConcatAnalysis;    //!< context
    NUModuleInstance *mInstance;        //!< instance getting mangled

  };

  MsgContext *mMsgContext;              //!< error message handle 
  ConcatAnalysis &mConcatAnalysis;      //!< context
  ConcatVectorisation &mParent;         //!< parent vectorisation
  ConcatVectorisation &mChild;          //!< child vectorisation
  NUModuleInstance *mInstance;          //!< instance getting mangled

};

template <> 
NUConcatOp *PortVecInstanceMangler::Actual::makeConcat <NUExprVector, NUConcatOp> (
  NUExprVector &exprs)
{
  return new NUConcatOp (exprs, 1, mInstance->getLoc ());
}

template <> 
NUConcatLvalue *PortVecInstanceMangler::Actual::makeConcat <NULvalueVector, NUConcatLvalue> (
  NULvalueVector &lvalues)
{
  return new NUConcatLvalue (lvalues, mInstance->getLoc ());
}

//! Test whether a net is replaced
bool ConcatVectorisation::lookup (NUNet *oldnet, NUNet **newnet, UInt32 *bit) const
{
  ScalarNet::Map::const_iterator found;
  if ((found = mScalarNets.find (oldnet)) == mScalarNets.end ()) {
    return false;
  } else {
    ScalarNet *scalar = found->second;
    *newnet = scalar->getVector ()->getNet ();
    *bit    = scalar->getBit ();
    return true;
  }
}

bool ConcatVectorisation::apply (ConcatVectorisation &parent, NUModuleInstance *instance)
{
  PortVecInstanceMangler mangler (mMsgContext, mConcatAnalysis, parent, *this, instance);
  instance->printUDSets (mConcatAnalysis.getNetRefFactory ());
  if (mangler.apply ()) {
    mConcatAnalysis.note_apply (instance);
    // Make the note callback after mangling because the instance mangler
    // modifies the instance rather than constructing a new instance.
    instance->printUDSets (mConcatAnalysis.getNetRefFactory ());
    return true;
  } else {
    return false;
  }
}

//! Populate a set with all the nets eliminated by this.
void ConcatVectorisation::getEliminatedNets (NUNetSet *nets) const
{
  for (VectorNet::List::const_iterator it = mVectorNets.begin (); it != mVectorNets.end (); it++) {
    (*it)->getEliminatedNets (nets);
  }
}

//! verbose output for a vectorisation.
void ConcatVectorisation::verbose ()
{
  report (UtIO::cout (), cDEEP|cNET|cPRIMARIES);
}

bool ConcatVectorisation::ScalarNet::Vector::makeName (NUModule *module, StringAtom **atom) const
{
  UtList <const char *> scalar_names;
  for (const_iterator it = begin (); it != end (); it++) {
    scalar_names.push_back ((*it)->getNet ()->getName ()->str ());
  }
  *atom = module->gensymFromList ("vec", scalar_names, module->getName ()->str ());
  return (*atom != NULL);
}

//! Write a report of this vectorisation to a stream.
void ConcatVectorisation::report (UtOStream &s, const UInt32 flags) const
{
  // nothing for degenerate cases...
  if (isDegenerate ()) {
    return;
  } 
  UInt32 n_scalars = mScalarNets.size ();
  UInt32 n_components = mVectorNets.size ();
  UtString header;
  UtIndent indenter (&header);
  // header for the module
  if (n_components == 1) {
    header << mModule->getName ()->str () << " has " << n_scalars << " vectorisation opportunities in "
      << "1 cluster: ";
  } else {
    header << mModule->getName ()->str () << " has " << n_scalars << " vectorisation opportunities in "
      << n_components << " clusters: ";
  }
  indenter.tabToLocColumn ();
  header << " // ";
  mModule->getLoc ().compose (&header);
  s << header << UtIO::endl;
  // collect all the vectorised nets into a set for optional details
  UtSet <NUNet *, NUNetCmp> all_nets;
  // now the individual components
  ScalarNet::Vector primaries;
  UtSet <NUNet *, NUNetCmp> new_nets;
  typedef UtMultiMap <NUNet *, NUNet *, NUNetCmp> ScalarMap;
  ScalarMap replaced;
  // form a set of the vector net instance so that the sorting order on the set
  // ensures a consistent ordering in the verbose output
  VectorNet::Set vectors;
  for (VectorNet::List::const_iterator it = mVectorNets.begin (); it != mVectorNets.end (); it++) {
    if (!(*it)->isDegenerate ()) {
      vectors.insert (*it);
    }
  }
  if (flags & cDEEP) {
    VectorNet::Set::const_iterator vector;
    UInt32 n = 0;
    for (vector = vectors.begin (); vector != vectors.end (); vector++) {
      s << n++ << ":";
      NUNet *new_net = NULL;
      if ((*vector)->isFrozen () && flags & cNET) {
        UtString b;
        new_net = (*vector)->getNet ();
        new_net->composeDeclaration (&b);
        s << " " <<  b << ":";
        new_nets.insert (new_net);
      } else {
      }
      ScalarNet::Vector::const_reverse_iterator scalar;
      for (scalar = (*vector)->rbegin (); scalar != (*vector)->rend (); scalar++) {
        NUNet *scalar_net = (*scalar)->getNet ();
        s << " " << (*scalar)->image ();
        if (scalar_net->isPrimaryPort ()) {
          primaries.push_back (*scalar);
          s << "*";
        }
        if (new_net != NULL) {
          replaced.insert (ScalarMap::value_type (new_net, scalar_net));
        }
      }
      s << UtIO::endl;
    }
    if (!primaries.empty ()) {
      s << primaries.size () << " primary port vectorisation opportunities in " 
        << mModule->getName ()->str () << ":" << UtIO::endl;
      UtString line;
      for (ScalarNet::Vector::iterator it = primaries.begin (); it != primaries.end (); it++) {
        const char *name = (*it)->image ();
        if (line.size () + strlen (name) > 79) {
          s << line << UtIO::endl;
          line.clear ();
        }
        if (line.size () == 0) {
          s << " ";                     // add space so that indent is two spaces
        }
        line << " " << name;
      }
      if (line.size () > 0) {
        s << line << UtIO::endl;
      }
    }
  }
  if (flags & cNET_FLAGS && !replaced.empty ()) {
    UtSet <NUNet *, NUNetCmp>::iterator it0;
    for (it0 = new_nets.begin (); it0 != new_nets.end (); it0++) {
      NUNet *vector_net = *it0;
      UtString b;
      mConcatAnalysis.compose (&b, vector_net->getFlags ());
      s << "New net: " << vector_net->getName ()->str () << ": " << b << UtIO::endl;
      std::pair <ScalarMap::iterator, ScalarMap::iterator> range = replaced.equal_range (vector_net);
      for (ScalarMap::iterator it1 = range.first; it1 != range.second; it1++) {
        UtString b;
        NUNet *oldnet = it1->second;
        mConcatAnalysis.compose (&b, oldnet);
        s << "  " << b << UtIO::endl;
      }
    }
  }
}

// VectorNets
// **********

ConcatVectorisation::VectorNet::VectorNet (ConcatAnalysis &context, NUModule *module, bool verbose) : 
  mConcatAnalysis (context), mFrozen (false), mBitSize (0), mModule (module), mVerbose (verbose),
  mNet (NULL), mScalars (), mFirstScalar (NULL)
{
}

//! \return the new net created for this vector
NUNet *ConcatVectorisation::VectorNet::getNet () const
{ 
  NU_ASSERT (mFrozen, mScalars [0]->getNet ());
  return mNet; 
}

//! Append a scalar to the end of the list of scalars for this
void ConcatVectorisation::VectorNet::append (ScalarNet *scalar)
{ 
  NU_ASSERT (!mFrozen, scalar->getNet ());
  mScalars.push_back (scalar); 
  mBitSize += scalar->getBitSize ();
  if (mFirstScalar == NULL) {
    mFirstScalar = scalar->getNet ();
  }
}

//! Populate a set with all the nets eliminated by this.
void ConcatVectorisation::VectorNet::getEliminatedNets (NUNetSet *nets) const
{
  for (ScalarNet::Vector::const_iterator it = mScalars.begin (); it != mScalars.end (); it++) {
    nets->insert ((*it)->getNet ());
  }
}

#define DESCRIBE(_key_, _net_) if (mVerbose) { \
    UtString b; \
    mConcatAnalysis.compose (&b, _net_); \
    UtIO::cout () << _key_ << ": " << b << "\n"; \
 }

//! Construct the new net.
//! \note No new nets may be added after calling freeze
void ConcatVectorisation::VectorNet::freeze (NUModule *module)
{
  // there must be at least two nets
  NU_ASSERT (!mScalars.empty (), module);
  ScalarNet::Vector::iterator it = mScalars.begin ();
  UInt32 combined_flags = (UInt32) (*it)->getNet ()->getFlags ();
  (*it)->markAsReplaced ();
  it++;
  while (it != mScalars.end ()) {
    ScalarNet *scalar = *it;
    combined_flags = ConcatAnalysis::combineFlags (combined_flags, (UInt32) scalar->getNet ()->getFlags ());
    scalar->markAsReplaced ();
    DESCRIBE ("Old net", scalar->getNet ());
    it++;
  }
  // create the new net
  StringAtom *atom;
  if (!mScalars.makeName (mModule, &atom)) {
    NU_ASSERT (false, module);
  }

  ConstantRange range (mBitSize - 1, 0);
  mNet = module->createTempVectorNet (atom, range, false, module->getLoc (), (NetFlags) combined_flags,
    eNoneVectorNet);
  
  if (combined_flags & ePortMask) {
    module->removeLocal (mNet);
  }

  NUDesign* design = module->getDesign();
  NUAliasBOM * alias_bom = design->getAliasBOM();
  ESFactory * expr_factory = alias_bom->getExprFactory();

  /*
    Run through the scalar list and create a vector of idents to make
    a concat. This is needed to create the backpointer of the
    temp vector net.

    Note: the order of mScalars is begin->end == lsb->msb
    Therefore, in order to create a concat where the msb is the first
    element in the concat list we need to run through the list in
    reverse.
  */
  CarbonExprVector identVector;
  identVector.reserve(mScalars.size());
  UInt32 prev = mNet->getBitSize();
  for (ScalarNet::Vector::reverse_iterator i = mScalars.rbegin(), e = mScalars.rend();
         i != e; ++i)
  {
    ScalarNet *scalar = *i;
    NUNet* scalarNet = scalar->getNet();
    // The net may not have an ident associated with it yet. So make
    // sure we have one. 
    CarbonIdent* ident = alias_bom->createCarbonIdent(scalarNet);
    identVector.push_back(ident);

    NU_ASSERT(scalar->getBit() < prev, scalarNet);
  }
  
  CarbonExpr* backPointer = expr_factory->createConcatOp(&identVector, 1, mNet->getBitSize(), mNet->isSigned());
  CarbonIdent* vecIdentBP = alias_bom->createCarbonIdentBP(mNet, backPointer);
  for (ScalarNet::Vector::iterator i = mScalars.begin(), e = mScalars.end();
         i != e; ++i)
  {
    ScalarNet *scalar = *i;
    NUNet* replacedNet = scalar->getNet();
    // We created the ident above. So, no need to go through that
    // process again. Just grab it.
    CarbonIdent* ident = replacedNet->getIdent();
    NU_ASSERT(ident, replacedNet);
    UInt32 bitSize = replacedNet->getBitSize();
    CarbonExpr* vectorRef = NULL;
    if (bitSize == 1)
    {
      CarbonConst* bitIndex = expr_factory->createConst(scalar->getBit(), 32, true);
      vectorRef = expr_factory->createBinaryOp(CarbonExpr::eBiBitSel, vecIdentBP, bitIndex, 1, false, false);
    }
    else
    {
      // this code path is not yet taken. It will be when vectors are
      // also considered for vectorisation.
      // Assert out for now.
      NU_ASSERT(0, replacedNet);

      NUVectorNet* vecNet = replacedNet->castVectorNet();
      // We don't handle multi-dim nets here, so we better be a vector
      NU_ASSERT(vecNet, replacedNet);
      const ConstantRange* operationalRange = vecNet->getRange();
      vectorRef = expr_factory->createPartsel(vecIdentBP, *operationalRange, operationalRange->getLength(), false);
    }

    NU_ASSERT(vectorRef, replacedNet);
    alias_bom->mapExpr(ident,
                       vectorRef);
    
  }

  DESCRIBE ("New net", mNet);
  mFrozen = true;
}
#undef DESCRIBE
