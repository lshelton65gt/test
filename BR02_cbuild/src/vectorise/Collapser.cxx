// -*-C++-*-    $Revision: 1.12 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/Nucleus.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUDesignWalker.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"
#include "reduce/Inference.h"
#include "reduce/CommonConcat.h"
#include "vectorise/Collapser.h"

// TODO:
//
// - concat operations in case statements are not getting collapsed because the
//   concat expression gets split before case collapsing is tried.

// If ASSERT_ON_ERROR is non-zero, then failure of internal consistency check
// induces an assertion failure, otherwise the matcher just fails. This should
// not be set for for production code. It's better to just miss an optimisation
// opportunity than to assert out on a customer site.
//
// If TRACK_INSTRUCTION_LINE is non-zero, then the line number in this source
// file is recorded in the pattern matcher instruction word. Usefull for
// debugging the matcher, not worth the (admittedly rather small) overhead
// otherwise.
//

#ifdef CDB
#define ASSERT_ON_ERROR 0
#define TRACK_INSTRUCTION_LINE 1
#define TEST_NOISE 1
#else
#define ASSERT_ON_ERROR 0               // always 0 in production code
#define TRACK_INSTRUCTION_LINE 0        // always 0 in production code
#define TEST_NOISE 0
#endif

#if ASSERT_ON_ERROR

#define MATCHER_ASSERT(_cond_, _anchor_) NU_ASSERT ((_cond_), (_anchor_))
#define MATCHER_ASSERT2(_cond_, _anchor1_, _anchor2_) NU_ASSERT2 ((_cond_), (_anchor1_), (_anchor2_))
#define MATCHER_VERIFY(_cond_) VERIFY ((_cond_))
#define MATCHER_TEST(_cond_, _anchor_) NU_ASSERT ((_cond_), (_anchor_))

#else

#define MATCHER_ASSERT(_cond_, _anchor_) if (!(_cond_)) { \
  internalError (__FILE__, __LINE__, #_cond_, _anchor_); \
}

#define MATCHER_ASSERT2(_cond_, _anchor1_, _anchor2_)  if (!(_cond_)) { \
  internalError (__FILE__, __LINE__, #_cond_, _anchor1_, _anchor2_); \
}

#define MATCHER_VERIFY(_cond_) \
  ((_cond_) ? 1 : (internalError (__FILE__, __LINE__, #_cond_, NULL), 0))

#define MATCHER_TEST(_cond_, _anchor_) if (!(_cond_)) { \
  internalError (__FILE__, __LINE__, #_cond_, _anchor_); \
  return false; \
}
  
#endif

#ifndef USE_RHS_CONCAT
#define USE_RHS_CONCAT 1
// This enables experimental code that acretes corresponding rvalue expressions
// during a match. I'm not sure if this is a win yet... need benchmarking...
// IMPORTANT: change the comment in Collapser.h when this is either trashed or
// promoted from experimental to real code.
// NOTE: Enabling USE_RHS_CONCAT currently induces regression failures.
#endif

#ifndef USE_LHS_CONCAT
#define USE_LHS_CONCAT 1
// This enables experimental code that acretes corresponding lvalues during a
// match. I'm not sure if this is a win yet.
#endif

// Setting NOISY_SIZE to a non-zero value at compile time causes all sorts of
// diagnostics to be output about the size of expressions constructed by the
// matcher. This must be zero when committing.

#ifndef NOISY_SIZE
#define NOISY_SIZE 0
#endif

//
// class Collapse
//

// build a Collapser instance for vectorising sequences of assignment
// statements.
Collapser::Collapser (const Inference::Params &params, NUScope *scope, IODBNucleus *iodb,
  MsgContext *msg_context, const NUAssign *head, CommonConcat *concats) : 
  mMsgContext (msg_context),
  mMatcher (params, scope, iodb, msg_context, concats, Matcher::eASSIGNS)
{
  mMatcher.buildProxyMatcher (head);
}

// build a Collapser instance for collapsing the complete set of case items
// from a single case statement.
Collapser::Collapser (const Inference::Params &params, NUScope *scope, IODBNucleus *iodb,
  MsgContext *msg_context, const NUCaseItem *head, CommonConcat *concats) : 
  mMatcher (params, scope, iodb, msg_context, concats, Matcher::eCASE)
{
  mMatcher.buildProxyMatcher (head);
}

//
// Collapsed statement synthesis - note how the structure mirrors the structure
// of the associated apply and build method
// 

bool Collapser::createCollapsed (NUStmt **replacement)
{
  return mMatcher.collapse (replacement);
}

bool Collapser::createCollapsed (const NUCase *case_stmt, NUAssign **stmt, NUAssign **selector)
{
  return mMatcher.collapse (case_stmt, stmt, selector);
}

//
// class Collapser::Instruction
//

// When building in debug mode track the line number in Collapse.cxx that
// induced the instruction. Usefull for debugging the matcher... not worth
// the overhead in production...

Collapser::Instruction::Instruction (const Symbol symbol, const int lineno) : 
  mSymbol (symbol)
{
  switch (symbol) {
#define INSTRUCTION_SYMBOL(_tag_, _nargs_) case e##_tag_: mWidth = _nargs_ + 1; break;
#define INSTRUCTION_ARG(_tag_, _type_) case e##_tag_##Arg: ASSERT (false); break;
#include "vectorise/Collapser.def"
  default: ASSERT (false); break;
  }
  mArg.mLineno = lineno;
}

Collapser::Instruction::Instruction (const Symbol symbol) : 
  mSymbol (symbol)
{
  switch (symbol) {
#define INSTRUCTION_SYMBOL(_tag_, _nargs_) case e##_tag_: mWidth = _nargs_ + 1; break;
#define INSTRUCTION_ARG(_tag_, _type_) case e##_tag_##Arg: ASSERT (false); break;
#include "vectorise/Collapser.def"
  default: ASSERT (false); break;
  }
}

#define INSTRUCTION_ARG(_tag_, _type_) \
Collapser::Instruction::Instruction (_type_ arg) : mSymbol (e##_tag_##Arg), mWidth (1) \
{ mArg.m##_tag_ = arg; } \
_type_ &Collapser::Instruction::get##_tag_ () \
{ ASSERT (mSymbol == e##_tag_##Arg); return mArg.m##_tag_; }
#include "vectorise/Collapser.def"

#if TRACK_INSTRUCTION_LINE
#define INSTRUCTION(_operator_) Instruction (_operator_, __LINE__)
#define FAIL() fail (__LINE__)
#else
#define INSTRUCTION(_operator_) Instruction (_operator_)
#define FAIL() fail ()
#endif

//
// class Collapser::Matcher
//

Collapser::Matcher::Matcher (const Inference::Params &params, NUScope *scope, IODBNucleus *iodb,
  MsgContext *msg_context, CommonConcat *concats, const UInt32 flags) : 
  mConcats (concats),
  mParams (params), 
  mFlags (flags 
    | (params.doLeftConcatCollapse () ? eALLOW_LHSCONCAT : 0)
    | (params.doRightConcatCollapse () ? eALLOW_RHSCONCAT : 0)
    | (params.isVerboseAlerts () ? eVERBOSE_ALERTS : 0)),
  mAggregateCost (),
  mUseMetrics (params.mOptions & Inference::eEnableMetrics), 
  mVerboseMetrics (params.mOptions & Inference::eVerboseVectorMetrics),
  mThreshold (params.mCollapserThreshold),
  mScope (scope), mIODB (iodb), mMsgContext (msg_context), mHead (NULL), 
  mIsFailed (false), mNMatched (0), mNextMark (11), 
  mFirst (), mCurrent (&mFirst), 
  mLvalueConcat (NULL),
  mLHSNet (NULL), mLHSNets (), mRHSNets (), mSlicedLHS (NULL), mAssignFlavour (eNone),
  mFirstMatchedIndex (NULL), mLastMatchedIndex (NULL), mCollapsedIntoConcat (false),
  mSpan (), mSubspan (), 
  //mNextAbstractLsb (), 
  //mAbstractToConcrete (), 
  mConcreteToAbstract (),
  mCaseSelectorId (NULL)
{
  // must be either a matcher for assignments, or for case items
  MATCHER_ASSERT ((mFlags & (eASSIGNS|eCASE)) != 0, mHead);
  MATCHER_ASSERT ((mFlags & (eASSIGNS|eCASE)) != (eASSIGNS|eCASE), mHead);
}

Collapser::Matcher::~Matcher ()
{
  // free all the controlled objects
  UtList <Embedded *>::iterator it;
  for (it = mEmbedded.begin (); it != mEmbedded.end (); it++) {
    delete *it;
  }
  // delete any overflow instruction blocks
  Block *block = mFirst.mNext;
  while (block != NULL) {
    Block *tmp = block;
    block = block->mNext;
    delete tmp;
  }
}

#ifdef CDB
/*static*/ const char *Collapser::Matcher::cvt (char *s, const UInt32 size, const UInt32 flags)
{
  unsigned int n = 0;
  s [n] = '\0';
#define PROBE(_x_) \
  if ((flags & (e##_x_)) == 0) {             \
  } else if (n == 0) { \
    n += snprintf (s + n, size - n, #_x_); \
  } else { \
    n += snprintf (s + n, size - n, "|" #_x_); \
  }
  PROBE (ASSIGNS);
  PROBE (CASE);
  PROBE (SCALAR);
  PROBE (NO_XZ);
  PROBE (VARSEL_IDENT);
  PROBE (INDEX);
  PROBE (SCALAR_COND);
  PROBE (TEST_Z);
  PROBE (BUILD_IDENT_VEC);
  PROBE (INNER);
  PROBE (CONCAT);
  PROBE (LHSCONCAT);
  PROBE (ALLOW_LHSCONCAT);
  PROBE (IS_Z);
#undef PROBE
  return s;
}
#endif

bool Collapser::Matcher::allowCollapse (const NUAssign *replacement, 
  float *aggregate_cost, float *replacement_cost) const
{
  if (!mUseMetrics) {
    return true;                        // not looking at the metrics
  } else if (replacement == NULL) {
    NU_ASSERT (replacement != NULL, mHead); return false;                       // ugh!
  } else {
    NUCostContext context;
    NUCost cost;
    replacement->calcCost (&cost, &context);
    *replacement_cost = cost.complexity ();
    *aggregate_cost = mAggregateCost.complexity ();
    return *replacement_cost * 100 <= *aggregate_cost * mThreshold;
  }
}

// Construction of proxies for pattern matchers

void Collapser::Matcher::buildProxyMatcher (const NUAssign *prototype)
{
  // Rather than building a matcher for the prototype construct a proxy matcher
  // that embeds a handle on the prototype assignment. The matcher is only
  // built when an attempt to match the next statement is made.
  MATCHER_ASSERT (mHead == NULL, prototype);
  MATCHER_ASSERT (mFlags & eASSIGNS, prototype);
  mHead = prototype;
  if (prototype == NULL) {
    FAIL ();
    return;
  } else {
    append (INSTRUCTION (eAssignProxy));
    append (Instruction (prototype));
  }
}

void Collapser::Matcher::buildProxyMatcher (const NUCaseItem *prototype)
{
  // Rather than building a matcher for the prototype case item construct a
  // proxy matcher that embeds a handle on the prototype assignment. The
  // matcher is only built when an attempt to match the next case item is made.
  MATCHER_ASSERT (mHead == NULL, prototype);
  MATCHER_ASSERT (mFlags & eCASE, prototype);
  mHead = prototype;
  if (prototype == NULL) {
    FAIL ();
    return;
  } else {
    append (INSTRUCTION (eCaseItemProxy));
    append (Instruction (prototype));
  }
}

void Collapser::Matcher::expandProxies ()
{
  iterator n (this);
  while (!n.atEnd ()) {
    switch (n.getSymbol ()) {
    case eAssignProxy:
      {
        const NUAssign *assign = n[1].getAssign ();
        truncate (n);
        build (assign, mFlags);
        NU_ASSERT (n.getSymbol () != eAssignProxy, mHead);
      }
      break;
    case eCaseItemProxy:
      {
        const NUCaseItem *item = n[1].getCaseItem ();
        truncate (n);
        build (item, mFlags);
        NU_ASSERT (n.getSymbol () != eCaseItemProxy, mHead);
      }
      break;
    case eExprProxy:
      {
        const NUExpr *expr = n [1].getExpr ();
        truncate (n);
        UInt32 syn;
        build (expr, mFlags, &syn);
        NU_ASSERT (n.getSymbol () != eExprProxy, mHead);
      }
      break;
    default:
      n++;
      break;
    }
  }
}

// Many constructs cannot be collapsed. When a build method identifiers such an
// instance the fail method is called. This tags the matcher as having failed,
// and inserts an eFail into the instruction stream. Apply or build methods
// should never be called on a failed matcher.

void Collapser::Matcher::fail (const int lineno)
{
  append (Instruction (eFail, lineno));
  mIsFailed = true;
}

void Collapser::Matcher::fail ()
{
  append (Instruction (eFail));
  mIsFailed = true;
}

// Reserve space in the instruction stream and return a handle onto the
// reserved location.

void Collapser::Matcher::append (iterator *placeholder)
{
  if (mCurrent->mLength < eBLOCK_SIZE) {
    // It fits in the current block
  } else if (mCurrent->mNext != NULL) {
    // There is already a block chained onto this one... this can happen when
    // truncate overlaps a block boundary
    mCurrent = mCurrent->mNext;
    mCurrent->mLength = 0;
  } else {
    // Need to allocate a new instruction block
    mCurrent->mNext = new Block;
    mCurrent = mCurrent->mNext;
  }
  placeholder->bind (this);
  mCurrent->mInstruction [mCurrent->mLength++] = INSTRUCTION (eNone);
}

// Append an instruction to the matcher instruction stream.

void Collapser::Matcher::append (const Instruction &instruction)
{
  if (mCurrent->mLength < eBLOCK_SIZE) {
    // It fits in the current block
  } else if (mCurrent->mNext != NULL) {
    // There is already a block chained onto this one... this can happen when
    // truncate overlaps a block boundary
    mCurrent = mCurrent->mNext;
    mCurrent->mLength = 0;
  } else {
    // Need to allocate a new instruction block
    mCurrent->mNext = new Block;
    mCurrent = mCurrent->mNext;
  }
  mCurrent->mInstruction [mCurrent->mLength++] = instruction;
}

// Test an assignment statement against the matcher

bool Collapser::Matcher::acrete (const NUAssign *next)
{
  MATCHER_TEST (mFlags & eASSIGNS, mHead);
  if (next == NULL) {
    return false;
  } else if (mIsFailed) {
    return false;                       // no possible collapsable sequence
  }
  iterator it (this);
  if (apply (it, next, mFlags)) {
    extend (next, mSubspan);            // extend the global span
    mNMatched++;                        // do not increment before calling extend...
    return true;
  } else {
    return false;                       // did not match
  }
}

// Test a case item against the matcher
bool Collapser::Matcher::acrete (const NUCaseItem *next)
{
  MATCHER_TEST (mFlags & eCASE, mHead);
  if (next == NULL) {
    return false;
  } else if (mIsFailed) {
    return false;                       // no possible collapsable sequence
  }
  iterator it (this);
  if (apply (it, next, mFlags)) {
    mNMatched++;
    return true;
  } else {
    return false;                       // did not match
  }
}

// Construct a vectorised assignment statement from the equivalence class of
// statements matched by this matcher.

bool Collapser::Matcher::collapse (NUStmt **stmt)
{
  MATCHER_TEST ((mFlags & eASSIGNS), mHead);
  if (!MATCHER_VERIFY (!mIsFailed) || mNMatched == 0) {
    // should never be called on a failed pattern matcher
    *stmt = NULL;
    return false;
  }
  if (mSpan.isFloating ()) {
    // TODO - construct replacements where the span is floating by building an
    // arbitrary binding of the segments
    *stmt = NULL;
    return false;
  }
  // build and return the equivalent statement
  Matcher::iterator n (this);
  create (n, (NUAssign **) stmt, mFlags);
  return MATCHER_VERIFY (*stmt != NULL);
}

bool Collapser::Matcher::collapse (const NUCase *case_stmt, NUAssign **stmt, NUAssign **selector)
{
  // Initialise to NULL in case we return via a condition failure in a MATCHER_TEST.
  *stmt = NULL;
  *selector = NULL;
  MATCHER_TEST ((mFlags & eCASE) && !mIsFailed, mHead);
  Matcher::iterator n (this);
  if (n.getSymbol () == eCaseItemProxy) {
    expandProxies ();
  }
  if (mIsFailed) {
    // This can happen we collapse a single-item case statement and proxy
    // expansion hit an uncollapsible construct.
    NU_ASSERT (mNMatched == 0, mHead);
    return false;
  }
  NU_ASSERT (n.getSymbol () == eCaseItem, case_stmt);
  n++;                                  // advance to the assignment
  // create a temporary and then the copy of the selector to the temporary
  // (taken from the old Inference::copySelector method)
  CopyContext copy_context (NULL,NULL);
  NUExpr *sel = case_stmt->getSelect()->copy (copy_context);
  MATCHER_TEST (mScope, case_stmt);
  StringAtom* sym = mScope->gensym ("Inference", NULL, sel );
  mCaseSelectorId = mScope->createTempNet (sym, sel->determineBitSize(),
    false, case_stmt->getLoc() );
  NULvalue *lvalue = new NUIdentLvalue (mCaseSelectorId, case_stmt->getLoc() );
  *selector = new NUBlockingAssign(lvalue, sel, case_stmt->getUsesCFNet(), case_stmt->getLoc());
  // create the collapsed assignment statement
  create (n, stmt, mFlags);             // uses mCaseSelectorId
  MATCHER_TEST (*stmt != NULL && *selector != NULL, mHead);
  return true;
}

// There is an incestuous relationship between corresponding build, apply and
// create method. The build methods construct the matcher program from a
// prototype, the apply methods apply the matcher to a test and the create
// method constructs collapsed instances from the matcher state.
//
// Corresponding builds, applies and creates should follow each other in the source

//
// Matching of NUAssign
//

void Collapser::Matcher::build (const NUAssign *prototype, const UInt32 flags)
{
  if (prototype == NULL) {
    FAIL ();
    return;
  }
  mAssignFlavour = getFlavour (prototype);
  switch (mAssignFlavour) {
  case eAssignContinuous:
    {
      const NUContAssign *assign = dynamic_cast <const NUContAssign *> (prototype);
      MATCHER_ASSERT (assign != NULL, prototype);
      // Allow the span to extend in either direction.
      mSpan.bind (Span::eExpanding, prototype);
      append (INSTRUCTION (eAssignContinuous));
      append (Instruction (prototype));
      append (Instruction (assign->getStrength ()));
      build (prototype->getLvalue (), flags, &mLHSNet, &mSpan);
      buildProxyMatcher (prototype->getRvalue (), flags);
    }
    break;
  case eAssignBlocking:
    {
      // The span must either monotonically increase, or monotonically
      // decrease. We cannot tell which it is until we see the second
      // assignment in the series.
      append (INSTRUCTION (eAssignBlocking));
      append (Instruction (prototype));
      build (prototype->getLvalue (), flags, &mLHSNet, &mSpan);
      buildProxyMatcher (prototype->getRvalue (), flags);
    }
    break;
  case eAssignNonBlocking:
    {
      // Allow the span to extend in either direction.
      append (INSTRUCTION (eAssignNonBlocking));
      append (Instruction (prototype));
      build (prototype->getLvalue (), flags, &mLHSNet, &mSpan);
      buildProxyMatcher (prototype->getRvalue (), flags);
    }
    break;
  default:
    MATCHER_ASSERT (false, prototype);
    FAIL ();
    return;
    break;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUAssign *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;                       // nothing to match
  }
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eAssignContinuous:
    {
      const NUContAssign *assign = dynamic_cast <const NUContAssign *> (test);
      const Strength strength = n[2].getStrength ();
      Span span;
      n++;
      if (!MATCHER_VERIFY (assign != NULL)) {
        return false;                   // not a continuous assignment
      } else if (getFlavour (test) != eAssignContinuous) {
        return false;                   // wrong kind of assignment
      } else if (strength != assign->getStrength ()) {
        return false;                   // the strength does not match
      } else if (!apply (n, test->getLvalue (), flags, &span)) {
        return false;                   // lhs does not match
      }
      // If this is the first lhs match then it may be possible to bind the
      // direction of change of the span.
      if (mNMatched > 0) {
        // not the first match
      } else if (span.getDirection () != Span::eUnknownDirection) {
        mSpan.bind (span.getDirection (), test);
      }
      if (!MATCHER_VERIFY (mLHSNet != NULL)) {
        // This should have been set during build. Without a handle on the net
        // recursive assignments cannot be checked.
        return false;                   // the strength does not match
      } else if (!apply (n, test->getRvalue (), flags)) {
        return false;                   // one of the operands did not match
      } else {
        return true;                    // we win!
      }
    }
    break;
  case eAssignBlocking:
  case eAssignNonBlocking:
    {
      Span span;
      n++;
      if (getFlavour (test) != symbol) {
        return false;                     // wrong kind of assignment
      } else if (!apply (n, test->getLvalue (), flags, &span)) {
        return false;                   // assignment lhs does not match
      }
      // If this is the first lhs match then it may be possible to bind the
      // direction of change of the span.
      if (mNMatched > 0) {
        // not the first match
      } else if (span.getDirection () != Span::eUnknownDirection) {
        mSpan.bind (span.getDirection (), test);
      }
      if (!MATCHER_VERIFY (mLHSNet)) {
        // This should have been set during build. Without a handle on the net
        // recursive assignments cannot be checked... but it must be tested after
        // the lvalue matcher is applied.
        return false;
      } else if (!apply (n, test->getRvalue (), flags)) {
        return false;                     // assignment rhs does not match
      } else {
        return true;                      // we win!
      }
    }
    break;
  case eAssignProxy:
    {
      // The first time the matcher hits an assignment proxy, replace the proxy
      // instructions with the real matcher.
      const NUAssign *assign = n[1].getAssign ();
      truncate (n);
      build (assign, mFlags|flags);
      // ... and the try to match
      return !mIsFailed && apply (n, test, mFlags|flags);
    }
    break;
  default:
    MATCHER_TEST (false, test);
    return false;
    break;
  }
}

void Collapser::Matcher::create (iterator &n, NUAssign **result, const UInt32 flags)
{
  *result = NULL;
  // cache the assignment flavour and the prototype assignment
  const NUAssign *prototype = n[1].getAssign ();
  Symbol symbol = n.getSymbol ();
  Strength strength = eStrStrongest;
  if (symbol == eAssignContinuous) {
    strength = n[2].getStrength ();
  }
  n++;
  // construct the operands
  NULvalue *lvalue;
  NUExpr *rvalue;
  create (n, &lvalue, flags);
  create (n, &rvalue, flags);
  // sanity check that the operands were built successfully
  if ( /* MATCHER_VERIFY */ (lvalue != NULL && rvalue != NULL)) {
    // successfully constructed both sides
  } else if (lvalue != NULL) {
    delete lvalue;                      // made the lvalue but not the rvalue
    return;
  } else if (rvalue != NULL) {
    delete rvalue;                      // made the rvalue but not the lvalue
    return;
  } else {
    // all construction failed
    return;
  }
  // we have both lvalue and rvalue, so construct the appropriate flavour of assign
  switch (symbol) {
  case eFail:
    NU_ASSERT (symbol != eFail, mHead); // apply should have failed
    break;
  case eAssignContinuous:
    *result = new NUContAssign (prototype->getName (), lvalue, rvalue, prototype->getLoc (), strength);
    break;
  case eAssignBlocking:
    *result = new NUBlockingAssign (lvalue, rvalue, prototype->getUsesCFNet (), prototype->getLoc ());
    break;
  case eAssignNonBlocking:
    *result = new NUNonBlockingAssign (lvalue, rvalue, prototype->getUsesCFNet (), prototype->getLoc ());
    break;
  default:
    NU_ASSERT (false, mHead);      // matcher is jammed
    break;
  }
}

//
// Matching of NUConcatLvalue
//
//

void Collapser::Matcher::build (const NUConcatLvalue *prototype, const UInt32 flags,
  NUNet **net, Span *span)
{
  if (prototype == NULL) {
    FAIL ();
    return;
  } else if (!(flags & eCASE)) {
    // Can only match concats on the lhs of an assignment when collapsing case
    // statements.
    MATCHER_ASSERT (flags & eCASE, prototype);
    FAIL ();
    return;
  }
  append (INSTRUCTION (eLvalueCaseConcat));
  append (Instruction (prototype));
  UInt32 width = 0;
  for (UInt32 i = 0; i < prototype->getNumArgs (); i++) {
    NUNet *subnet = NULL;
    Span subspan;
    build (prototype->getArg (i), flags, &subnet, &subspan);
    width += subspan.getLength ();
    *net = subnet;
  }
  *span = ConstantRange (width - 1, 0);
}

bool Collapser::Matcher::apply (iterator &n, const NUConcatLvalue *test, const UInt32 flags,
  Span *span)
{
  if (test == NULL) {
    return false;
  }
  MATCHER_TEST (n.getSymbol () == eLvalueCaseConcat, mHead);
  const NUConcatLvalue *prototype = dynamic_cast <const NUConcatLvalue *> (n [1].getPrototype ());
  n++;
  if (prototype->getNumArgs () != test->getNumArgs ()) {
    return false;
  }
  UInt32 width = 0;
  for (UInt32 i = 0; i < test->getNumArgs (); i++) {
    Span subspan;
    if (!apply (n, test->getArg (i), flags, &subspan)) {
      return false;
    }
    width += subspan.getLength ();
  }
  *span = ConstantRange (width - 1, 0);
  return true;
}

void Collapser::Matcher::create (iterator &n, NUConcatLvalue **result, const UInt32 flags)
{
  const Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eLvalueCaseConcat:
    {
      const NUConcatLvalue *prototype = dynamic_cast <const NUConcatLvalue *> (n [1].getPrototype ());
      n++;
      NULvalueVector vector;
      for (UInt32 i = 0; i < prototype->getNumArgs (); i++) {
        NULvalue *lvalue;
        create (n, &lvalue, flags);
        vector.push_back (lvalue);
      }
      *result = new NUConcatLvalue (vector, prototype->getLoc ());
    }
    break;
  case eLvalueConcat:
    {
      const NULvalue *prototype = dynamic_cast <const NULvalue *> (n [1].getPrototype ());
      LvalueConcat *concat = n [2].getLvalueConcat ();
      NU_ASSERT (prototype != NULL, mHead);
      n++;
      CopyContext ctx (NULL, NULL);
      NULvalueVector vector;
      // check the complete set of lvalues
      bool is_abstract;
      if (!concat->resolve (&is_abstract)) {
        // this is not good... inconsistencies in the construction of the
        // lvalue concat should not have made it through matching
        NU_ASSERT (false, prototype);
      }
      // construct a vector of copies of the rvalues in the concat
      LvalueConcat::fixed_reverse_iterator it = concat->fixed_rbegin ();
      LvalueConcat::fixed_reverse_iterator end = concat->fixed_rend ();
      // Not yet matching fully floating vectorisation segments
      NU_ASSERT (!mSpan.isFloating (), prototype);
      ConstantRange span (mSpan.bindRange (*this));
      while (it != end) {
        const LvalueConcat::Segment segment = it->second;
        if (!span.contains (segment.mLsb)) {
          // If this operand of a rhs was matched, but the matched failed later on
          // a the same rhs then this segment reflects the partial match of the
          // rhs. It's easier to allow the segment to be added to the concat
          // during matching but then throw it out here, than to undo state
          // from partial matched.
          it++;
          continue;
        }
        NULvalue *copy = segment.mLvalue->copy (ctx);
        vector.push_back (copy);
        it++;
      }
      NU_ASSERT (vector.size () > 0, prototype);
      *result = new NUConcatLvalue (vector, prototype->getLoc ());
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
}

//
// Matching of NUCaseItem
//

bool Collapser::Matcher::isCollapsible (const NUCaseItem *item,
  SInt32 *index, NUStmt **stmt) const
{
  // first the conditions
  NUCaseItem::ConditionCLoop conditions = item->loopConditions();
  if (conditions.atEnd ()) {
    return false;                       // no conditions
  }
  const NUCaseCondition *condition = *conditions;
  ++conditions;
  if (!conditions.atEnd ()) {
    return false;                       // more than one condition
  }
  // OK, there is exactly one condition in the case item, so check that it is
  // constant
  NUConst *const_condition;
  if (!condition->isSwitchable () 
    || ((const_condition = condition->getExpr ()->castConst ()) == NULL)) {
    return false;                       // not a constant label
  } else if (!const_condition->getL (index)) {
    return false;
  } else if (*index < 0) {
    // Only non-negative case labels are currently allowed - bug 5256
    return false;
  }
  // now the statements
  NUStmtCLoop loop = item->loopStmts ();
  if (loop.atEnd ()) {
    return false;                       // no statements in the list
  }
  *stmt = *loop;
  ++loop;
  if (!loop.atEnd ()) {
    return false;                       // more than one statement in the list
  }
  return true;                          // one statement, one condition, one const
}

void Collapser::Matcher::build (const NUCaseItem *prototype, const UInt32 flags)
{
  MATCHER_ASSERT (flags & eCASE, prototype);
  NUStmt *stmt;
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  } else if (!isCollapsible (prototype, &mCaseBaseIndex, &stmt)) {
    FAIL ();
    return;
  }
  append (INSTRUCTION (eCaseItem));
  append (Instruction (prototype));
  build (dynamic_cast <NUAssign *> (stmt), flags);
  // Note that if it is not an assignment statement, then the build method for
  // NUAssign will deal with the NULL value by failing the pattern matcher.
}

bool Collapser::Matcher::apply (iterator &n, const NUCaseItem *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  MATCHER_TEST (mFlags & eCASE, mHead);
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eCaseItem:
    {
      NUStmt *stmt;
      if (!isCollapsible (test, &mCaseIndex, &stmt)) {
        return false;
      }
      n++;
      return apply (n, dynamic_cast <const NUAssign *> (stmt), flags);
    }
    break;
  case eCaseItemProxy:
    {
      // The first time the matcher hits case item proxy, replace the proxy
      // instructions with the real matcher.
      const NUCaseItem *item = n[1].getCaseItem ();
      truncate (n);
      build (item, mFlags|flags);
      // And then try to match
      return !mIsFailed && apply (n, test, mFlags|flags);
    }
    break;
  default:
    NU_ASSERT (false, test);
    return false;
    break;
  }
}

//
// Matching of NUExpr
//

void Collapser::Matcher::buildProxyMatcher (const NUExpr *prototype, const UInt32 flags)
{
  if (prototype == NULL) {
    FAIL ();
    return;
  } else {
    append (INSTRUCTION (eExprProxy));
    append (Instruction (prototype));
    append (Instruction (flags));
  }
}

void Collapser::Matcher::build (const NUExpr *prototype, const UInt32 flags, UInt32 *attr)
{
#if NOISY_SIZE
  UtIO::cout () << __FUNCTION__ << " ";
  if (prototype->isSignedResult ()) {
    UtIO::cout () << "(signed) ";
  }
  prototype->pr ();
#endif
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *attr = 0;
  switch (prototype->getType ()) {
  case NUExpr::eNUVarselRvalue:
    {
      NUNet *net = NULL;
      build (dynamic_cast <const NUVarselRvalue *> (prototype), flags, &net, attr);
    }
    break;
  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ:
    build (dynamic_cast <const NUConst *> (prototype), flags, attr);
    break;
  case NUExpr::eNUMemselRvalue:
    {
      NUNet *net = NULL;
      const NUMemselRvalue *memsel = dynamic_cast <const NUMemselRvalue *> (prototype);
      MATCHER_ASSERT (memsel != NULL, prototype);
      build (memsel, flags, &net, attr);
      MATCHER_ASSERT (memsel->getIdent () == net, memsel);
    }
    break;
  case NUExpr::eNUUnaryOp:
    build (dynamic_cast <const NUUnaryOp *> (prototype), flags, attr);
    break;
  case NUExpr::eNUBinaryOp:
    build (dynamic_cast <const NUBinaryOp *> (prototype), flags, attr);
    break;
  case NUExpr::eNUTernaryOp:
    build (dynamic_cast <const NUTernaryOp *> (prototype), flags, attr);
    break;
  case NUExpr::eNUConcatOp:
    {
      UInt32 syn;
      build (dynamic_cast <const NUConcatOp *> (prototype), flags, &syn);
    }
    break;
  case NUExpr::eNUIdentRvalue:
    {
      NUNet *net = NULL;
      UInt32 syn;
      build (dynamic_cast <const NUIdentRvalue *> (prototype), flags, &net, &syn);
    }
    break;
  default:
    FAIL ();                            // cannot collapse this expression
    return;
    break;
  }
}

void Collapser::Matcher::build (const NUTernaryOp *prototype, const UInt32 flags, UInt32 *attr)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *attr = 0;
  // A ternary condition will collapse into either a bitwise binary
  // expression or into a ternary condition with a scalar selector and
  // collapsed operands.
  const NUExpr *sel;
  UInt32 syn [3];
  if (!MATCHER_VERIFY (prototype->getOp () == NUOp::eTeCond)) {
    // eTeCond is the only ternary operator currently defined... if,
    // perchance, in production code we get to here and it's not eTeCond,
    // let's just generated a failing matcher and be done with it.
    FAIL ();
    return;
  } else if (!MATCHER_VERIFY ((sel = prototype->getArg (0)) != NULL)) {
    FAIL ();
    return;
  } else if (flags & eCASE) {
    append (INSTRUCTION (eScalarCond));
    append (Instruction (prototype));
    append (Instruction (prototype->getOp ()));
    build (prototype->getArg (1), flags, &syn [1]);
    build (prototype->getArg (2), flags, &syn [2]);
    build (prototype->getArg (0), flags|eCOND, &syn [0]);
  } else if ((flags & (eSCALAR_COND|eSCALAR)) || !isVectoredSel (sel)) {
    // The selector is not vectored so we will generate the ternary
    // conditional (sel ? then_part[] : else_part[]).
    append (INSTRUCTION (eScalarCond));
    append (Instruction (prototype));
    append (Instruction (prototype->getOp ()));
    build (prototype->getArg (1), flags|eBUILD_IDENT_VEC, &syn [1]);
    build (prototype->getArg (2), flags|eBUILD_IDENT_VEC, &syn [2]);
    build (prototype->getArg (0), flags|eSCALAR|eCOND, &syn [0]);
  } else {
    // The selector of the cond is vectored so we will generate the binary
    // bitwise expression (sel[] & then_part[]) | (~sel[] & else_part[]).
    // Special case: If either operand is an all-Z constant then generate
    // the scalar form.
    iterator symbol;
    append (&symbol);
    append (Instruction (prototype));
    append (Instruction (prototype->getOp ()));
    // Constants with X or Z cannot be mapped into bitwise operations.
    // For example, out[n] = in [n] ? 1'b0 : 1'bz cannot be mapped into the
    // bitwise out = (~in) & 4'bz (see the truth tables in LRM
    // 4.1.10). However, if one of the branches is just Z then we can
    build (prototype->getArg (1), flags|eNO_XZ|eTEST_Z|eBUILD_IDENT_VEC, &syn [1]);
    build (prototype->getArg (2), flags|eNO_XZ|eTEST_Z|eBUILD_IDENT_VEC, &syn [2]);
    if ((syn [1] & eIS_Z) || (syn [2] & eIS_Z)) {
      // Either the if or the then branch is Z, so generate a scalar condition
      build (prototype->getArg (0), flags|eSCALAR|eCOND, &syn [0]);
      *symbol = INSTRUCTION (eScalarCond);
    } else {
      build (prototype->getArg (0), flags|eNO_XZ|eCOND, &syn [0]);
      *symbol = INSTRUCTION (eVectorCond);
    }
  }
}

void Collapser::Matcher::build (const NUUnaryOp *prototype, const UInt32 flags, UInt32 *attr)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *attr = 0;
  UInt32 syn;
  switch (prototype->getOp ()) {
  case NUOp::eUnLogNot: case NUOp::eUnBitNeg: case NUOp::eUnVhdlNot:
    append (INSTRUCTION (eUnaryOp));
    append (Instruction (prototype));
    append (Instruction (prototype->getOp ()));
    build (prototype->getArg (0), flags|eNO_XZ, &syn);
    break;
  case NUOp::eUnRedAnd:
  case NUOp::eUnRedOr:
  case NUOp::eUnRedXor:
  case NUOp::eUnCount:
    append (INSTRUCTION (eReducingUnary));
    append (Instruction (prototype));
    append (Instruction (prototype->getOp ()));
    if (flags & eCASE) {
      build (prototype->getArg (0), flags|eNO_XZ|eREDUCING, &syn);
    } else {
      build (prototype->getArg (0), flags|eNO_XZ|eSCALAR|eREDUCING, &syn);
    }
    break;
  default:
    FAIL ();
    return;
    break;
  }
}

void Collapser::Matcher::build (const NUBinaryOp *prototype, const UInt32 flags, UInt32 *attr)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *attr = 0;
  const NUOp::OpT flavour = prototype->getOp ();
  const bool is_relational = NUOp::isRelationalOperator (flavour);
  const UInt32 width = prototype->determineBitSize ();
  if (width > 1 && is_relational && mSpan.getLength () > 1) {
    // Cannot vectorise relational operators when the result is more than one
    // bit wide.
    FAIL ();
    return;
  } if (!(flags & (eSCALAR|eCASE|eCOND)) && is_relational && mSpan.getLength () > 1) {
    // The relational operators return 1 if true, 0 otherwise. If the size of
    // the result is greater than one, then the result must be zero extended
    append (INSTRUCTION (eZxtRelational));
    append (Instruction (prototype));
    append (Instruction (prototype->getOp ()));
    append (Instruction (mSpan.getLength ()));
    UInt32 syn [2];
    build (prototype->getArg (0), flags|eSCALAR, &syn [0]);
    build (prototype->getArg (1), flags|eSCALAR, &syn [1]);
  } else {
    switch (flavour) {
    case NUOp::eBiEq:     case NUOp::eBiNeq:
    case NUOp::eBiTrieq:  case NUOp::eBiTrineq:
      {
        // relational operators that produce a one-bit wide result
        append (INSTRUCTION (eEquality));
        append (Instruction (prototype));
        append (Instruction (prototype->getOp ()));
        UInt32 syn [2];
        UInt32 inh = (flags|eNO_XZ);
        build (prototype->getArg (0), inh, &syn [0]);
        build (prototype->getArg (1), inh, &syn [1]);
      }
      break;
    case NUOp::eBiLogAnd: case NUOp::eBiLogOr:  
    case NUOp::eBiBitAnd: case NUOp::eBiBitOr:  case NUOp::eBiBitXor: 
      {
        // these are the bitwise binary operators that we can collapse
        append (INSTRUCTION (eBinaryOp));
        append (Instruction (prototype));
        append (Instruction (prototype->getOp ()));
        UInt32 syn [2];
        build (prototype->getArg (0), (flags|eNO_XZ), &syn [0]);
        build (prototype->getArg (1), (flags|eNO_XZ), &syn [1]);
      }
      break;
    case NUOp::eBiPlus:   
    case NUOp::eBiMinus:  
#if 0
      // under development
      if (flags & (eCASE|eINDEX)) {
        append (INSTRUCTION (eBinaryOp));
        append (Instruction (prototype));
        append (Instruction (prototype->getOp ()));
        UInt32 syn [2];
        build (prototype->getArg (0), flags, &syn [0]);
        build (prototype->getArg (1), flags, &syn [1]);
      } else if (mSpan.getLength () == 1 && !(flags & eSCALAR)) {
        // These can be mapped into xor
        append (Instruction (eBinaryToXor));
        append (prototype);
        append (Instruction (prototype->getOp ()));
        UInt32 syn [2];
        // Pass the eLSB_ONLY flag so that a matcher is generated only if the
        // expression can be vectorised to get just the lsb.
        build (prototype->getArg (0), flags|eLSB_ONLY, &syn [0]);
        build (prototype->getArg (1), flags|eLSB_ONLY, &syn [1]);
      } else {
        FAIL ();
        return;
      }
      break;
#endif
    case NUOp::eBiSMult:
    case NUOp::eBiUMult:  case NUOp::eBiSDiv:   case NUOp::eBiUDiv:
    case NUOp::eBiSMod:   case NUOp::eBiUMod:   case NUOp::eBiVhdlMod:
      // These may be collapsed in case collapsing or matched indices
      if (!(flags & (eCASE|eINDEX))) {
        FAIL ();
        return;
      } else {
        append (INSTRUCTION (eBinaryOp));
        append (Instruction (prototype));
        append (Instruction (prototype->getOp ()));
        UInt32 syn [2];
        build (prototype->getArg (0), flags, &syn [0]);
        build (prototype->getArg (1), flags, &syn [1]);
      }
      break;
    default:
      FAIL ();
      return;
      break;
    }
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUUnaryOp *test, const UInt32 flags)
{
  switch (n.getSymbol ()) {
  case eUnaryOp:
    if (test == NULL) {
      return false;
    } else if (test->getOp () != n [2].getOp ()) {
      return false;
    } else {
      n++;
      return apply (n, test->getArg (0), flags);
    }
    break;
  case eReducingUnary:
    if (test == NULL) {
      return false;
    } else if (test->getOp () != n [2].getOp ()) {
      return false;
    } else if (flags & eCASE) {
      n++;
      return apply (n, test->getArg (0), flags|eREDUCING);
    } else {
      // Reducing unary operators are vectorised into a concat with a repeat
      // count. The operand must match like a scalar for this to be correct.
      n++;
      return apply (n, test->getArg (0), flags|eSCALAR);
    }
    break;
  default:
    MATCHER_TEST (false, mHead);
    return false;
    break;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUBinaryOp *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eZxtRelational:
    if (test->getOp () != n [2].getOp ()) {
      return false;
    } else {
      UInt32 width = n [3].getUInt ();
      n++;
      if (mSubspan.getLength () != width) {
        // Each segment of the concat must be zero extended to this
        // width. Varying width are not allowed.
        return false;
      } else {
        return apply (n, test->getArg (0), flags|eSCALAR) && apply (n, test->getArg (1), flags|eSCALAR);
      }
    }
    break;
  case eEquality:
  case eBinaryOp:
    if (test->getOp () != n [2].getOp ()) {
      return false;
    } else if (NUOp::isRelationalOperator (test->getOp ()) && (flags & eSCALAR)) {
      n++;
      return apply (n, test->getArg (0), flags) 
        && apply (n, test->getArg (1), flags);
    } else if (NUOp::isRelationalOperator (test->getOp ()) && mSubspan.getLength () > 1) {
      return false;
    } else if (!(flags & (eSCALAR|eINDEX))
#if 0
      /* Depending on the binary operator, it may be permissible to still
       * vectorise when the size of one or both operands is different from
       * the size of the current lhs. For now, just disallow for any
       * operator.
       *
       * TODO: work out which operators admit zero extending and allow those
       *       ones through the collapser.
       */
      && NUOp::isRelationalOperator (test->getOp ())
#endif
      && (mSubspan.getLength () > test->getArg (0)->getBitSize ()
        || mSubspan.getLength () > test->getArg (1)->getBitSize ())) {
      // This is a binary operator that may be mapped into a bitwise
      // equivalent. However, the size of the expression less than the width
      // of the lhs, so the conversion to a binary operator is not
      // OK.... without knowing the operator, zero extending the operands is
      // not a great idea.
      return false;
    } else {
      n++;
      return apply (n, test->getArg (0), flags) && apply (n, test->getArg (1), flags);
    }
    break;
#if 0
    // under development
  case eBinaryToXor:
    if (test->getOp () != n [2].getOp ()) {
      return false;
    } else {
      n++;
      return apply (n, test->getArg (0), flags|eLSB_ONLY) 
        && apply (n, test->getArg (1), flags|eLSB_ONLY);
    }
    break;
#endif
  default:
    MATCHER_TEST (false, mHead);
    return false;
    break;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUTernaryOp *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eScalarCond:
  case eVectorCond:
    if (test->getOp () != n [2].getOp ()) {
      return false;
    }
    n++;                                // advance to ternary operands
    if (!apply (n, test->getArg (1), flags)) {
      return false;                     // if part did not vectorise
    } else if (!apply (n, test->getArg (2), flags)) {
      return false;                     // else part did not vectorise
    } else if (!apply (n, test->getArg (0), symbol == eScalarCond ? (flags|eSCALAR|eCOND) : flags|eCOND)) {
      return false;                     // condition did not vectorise
    }
    break;
  default:
    MATCHER_TEST (false, mHead);
    return false;
    break;
  }
  return true;
}

bool Collapser::Matcher::apply (iterator &n, const NUExpr *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eExprProxy:
    {
      // The first time the matcher hits an expression proxy, replace the proxy
      // instructions with the real matcher.
      const NUExpr *expr = n [1].getExpr ();
      truncate (n);
      UInt32 syn;
      build (expr, mFlags|flags, &syn);
      if (mSpan.isFloating ()) {
        // I haven't got the code that deals with abstract spans on both sides
        // working yet, so just throw out the match.
        return false;
      }
      // And then try to match
      return !mIsFailed && apply (n, test, mFlags|flags);
    }
    break;
  case eIndexedConstant:
  case eIndexedConstantSigned:
  case eScalarConstant:
  case eScalarConstantSigned:
  case eScalarConstantXZ:
  case eVectorConstant:
  case eVectorConstantZ:
    return apply (n, dynamic_cast <const NUConst *> (test), flags);
    break;
  case eRvalueConcat:
    // Multiple expression types can be thrown into a rhs concat.
    switch (test->getType ()) {
    case NUExpr::eNUMemselRvalue:
      return apply (n, dynamic_cast <const NUMemselRvalue *> (test), flags);
      break;
    case NUExpr::eNUIdentRvalue:
      return apply (n, dynamic_cast <const NUIdentRvalue *> (test), flags);
      break;
    default:
      return false;
      break;
    }
    break;
  case eRvalueIdent:
  case eRvalueIdentBit:
  case eRvalueIdentCase:
  case eRvalueIdentScalar:
    return apply (n, dynamic_cast <const NUIdentRvalue *> (test), flags);
    break;
  case eRvalueVarselVector:
  case eRvalueVarselCase:
  case eRvalueVarselScalar:
    return apply (n, dynamic_cast <const NUVarselRvalue *> (test), flags);
    break;
  case eReducingUnary:
  case eUnaryOp:
    return apply (n, dynamic_cast <const NUUnaryOp *> (test), flags);
    break;
  case eZxtRelational:
  case eEquality:
  case eBinaryOp:
#if 0
    // under development
  case eBinaryToXor:
#endif
    return apply (n, dynamic_cast <const NUBinaryOp *> (test), flags);
    break;
  case eScalarCond:
  case eVectorCond:
    return apply (n, dynamic_cast <const NUTernaryOp *> (test), flags);
    break;
  case eRvalueMemsel:
    return apply (n, dynamic_cast <const NUMemselRvalue *> (test), flags);
    break;
  case eRvalueConcatOp:
  case eRvalueZeroConcatOp:
    return apply (n, dynamic_cast <const NUConcatOp *> (test), flags);
    break;
  default:
    MATCHER_ASSERT (false, test);
    return false;
    break;
  }
}

void Collapser::Matcher::create (iterator &n, NUExpr **expr, const UInt32 flags)
{
  // cache the expression flavour
  Symbol symbol = n.getSymbol ();
  iterator head = n;
  // now split on the type
  switch (symbol) {
  case eScalarConstant:
  case eScalarConstantXZ:
  case eScalarConstantSigned:
  case eVectorConstant:
  case eVectorConstantZ:
    create (n, (NUConst **) expr, flags|eINNER);
    break;
  case eVectorCond:
  case eEquality:
  case eBinaryOp:
#if 0
    // under development
  case eBinaryToXor:
#endif
    create (n, (NUBinaryOp **) expr, flags|eINNER);
    break;
  case eUnaryOp:
    create (n, (NUUnaryOp **) expr, flags|eINNER);
    break;
  case eScalarCond:
    create (n, (NUTernaryOp **) expr, flags|eINNER);
    break;
  case eConcatOp:
    // not yet implemented 
    *expr = NULL;
    break;
  case eReducingUnary:
    if (flags & (eSCALAR|eCASE)) {
      create (n, (NUUnaryOp **) expr, flags);
    } else {
      create (n, (NUConcatOp **) expr, flags);
    }
    break;
  case eRvalueVarselVector:
  case eRvalueVarselCase:
  case eRvalueVarselScalar:
    {
      UInt32 local = n [3].getUInt ();
      if (local & eCONCAT) {
        create (n, (NUConcatOp **) expr, flags|eINNER);
      } else {
        create (n, (NUVarselRvalue **) expr, flags|eINNER);
      }
    }
    break;
  case eRvalueIdentCase:
  case eRvalueIdentScalar:
  case eRvalueIdent:
    create (n, (NUIdentRvalue **) expr, flags|eINNER);
    break;
  case eZxtRelational:
  case eRvalueConcatOp:
  case eRvalueIdentBit:
  case eRvalueConcat:
  case eRvalueZeroConcatOp:
    create (n, (NUConcatOp **) expr, flags|eINNER);
    break;
  case eRvalueMemsel:
    create (n, (NUMemselRvalue **) expr, flags|eINNER);
    break;
  case eIndexedConstantSigned:
    {
      // This is a constant on the rhs in a case collapsing. The constant
      // varies with the selector
      NU_ASSERT ((flags & eCASE), mHead);
      const NUExpr *prototype = n [1].getExpr ();
      SInt32 base_value = n [2].getSInt ();
      n++;
      NU_ASSERT (mCaseSelectorId != NULL, mHead);
      // Bug 5256 - the case labels are required to be non-negative, so always
      // substitute a zero-exended reference to the case label.
      const UInt32 size = prototype->getBitSize ();
      NUExpr *term = new NUBinaryOp (NUOp::eBiVhZxt,
        new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ()),
        NUConst::create (false, size, 32, prototype->getLoc ()),
        prototype->getLoc ());
      if (mCaseBaseIndex < base_value) {
        // generate an addition
        UInt32 offset = base_value - mCaseBaseIndex;
        NUConst *offset_expr = NUConst::create (false, offset, size, prototype->getLoc ());
        *expr = new NUBinaryOp (NUOp::eBiPlus, term, offset_expr, prototype->getLoc ());
      } else if (mCaseBaseIndex > base_value) {
        // generate a subtraction
        UInt32 offset = mCaseBaseIndex - base_value;
        NUConst *offset_expr = NUConst::create (false, offset, size, prototype->getLoc ());
        *expr  = new NUBinaryOp (NUOp::eBiMinus, term, offset_expr, prototype->getLoc ());
      } else {
        // just generate a reference
        *expr  = term;
      }
      (*expr)->setSignedResult (true);
    }
    break;
  case eIndexedConstant:
    {
      NU_ASSERT ((flags & eCASE), mHead);
      const NUExpr *prototype = n [1].getExpr ();
      const ConstantFunction *function = n [2].getFunction ();
      if (!function->create (mCaseSelectorId, expr)) {
        NU_ASSERT (false, prototype);
      }
      n++;
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
  NU_ASSERT (*expr, mHead);             // lost the expression.... this is not good....
  if (flags & eINNER) {
    // This is an inner node of an expression tree. The top-level resize will
    // walk it way here.
  } else if (flags & (eSCALAR|eCASE)) {
    // This is the root node of an expression tree for a scalar. The size of
    // the constructed tree should be the size of the prototype
    const NUExpr *prototype = head [1].getExpr ();
    (*expr)->resize (prototype->getBitSize ());
#if NOISY_SIZE
    UtIO::cout () << __FUNCTION__ << " (scalar|case)";
    if ((*expr)->isSignedResult ()) {
      UtIO::cout () << "(signed) ";
    }
    prototype->print (1, 0);
    (*expr)->print (1, 0);
#endif
  } else {
    // This is a vectorised expression so the size becomes the span width.
    (*expr) = new NUVarselRvalue (*expr, ConstantRange (mSpan.getLength ()-1, 0), (*expr)->getLoc ());
    (*expr)->resize (mSpan.getLength ());
#if NOISY_SIZE
    UtIO::cout () << __FUNCTION__ << " ";
    if ((*expr)->isSignedResult ()) {
      UtIO::cout () << "(signed) ";
    }
    (*expr)->print (1, 0);
#endif
  }
}

void Collapser::Matcher::create (iterator &n, NUUnaryOp **expr, const UInt32 flags)
{
  // grab the operator and the prototype
  Symbol symbol = n.getSymbol ();
  NUOp::OpT op = n [2].getOp ();
  const NUExpr *prototype = n [1].getExpr ();
  n++;
  // 1. form the operand
  NUExpr *arg0;
  create (n, &arg0, flags);
  // 2. now construct the unary operator
  if ((flags & (eSCALAR|eCASE)) == 0) {
    // map the operator into a bitwise operator
    switch (op) {
    case NUOp::eUnLogNot:
    case NUOp::eUnBitNeg:
    case NUOp::eUnVhdlNot:
      // map all the negations into a bitwise operator
      *expr = new NUUnaryOp (NUOp::eUnBitNeg, arg0, prototype->getLoc ());
      break;
    default:
      // should not have got here for any other operators
      NU_ASSERT((op == NUOp::eUnLogNot) or (op == NUOp::eUnBitNeg) or (op == NUOp::eUnVhdlNot),
        prototype);
      break;
    }
  } else {
    switch (symbol) {
    case eUnaryOp:
    case eReducingUnary:
      // keep the operator
      *expr = new NUUnaryOp (op, arg0, prototype->getLoc ());
      break;
    default:
      NU_ASSERT (false, prototype);
      break;
    }
  }
}

void Collapser::Matcher::create (iterator &n, NUBinaryOp **expr, const UInt32 flags)
{
  // grab the operator and the prototype
  Symbol symbol = n.getSymbol ();
  // now construct the binary operator
  switch (symbol) {
#if 0
    // under development
  case eBinaryToXor:
    {
      const NUExpr *prototype = dynamic_cast <const NUOp *> (n [1].getExpr ());
      n++;
      NU_ASSERT (prototype != NULL, mHead);
      NUExpr *arg0;
      NUExpr *arg1;
      create (n, &arg0, flags);
      create (n, &arg1, flags);
      *expr = new NUBinaryOp (NUOp::eBiBitXor, arg0, arg1, prototype->getLoc ());
    }
    break;
#endif
  case eEquality:
  case eBinaryOp:
    {
      const NUExpr *prototype = dynamic_cast <const NUOp *> (n [1].getExpr ());
      NUOp::OpT op = n [2].getOp ();
      n++;
      NU_ASSERT (prototype != NULL, mHead);
      if ((flags & eSCALAR) == 0 ) {
        // The operator must be mapped into a collapsed equivalent.
        // 1. form the operands
        NUExpr *arg0;
        NUExpr *arg1;
        create (n, &arg0, flags);
        create (n, &arg1, flags);
        // 2. deduce the actual vector equivalent operation
        NUOp::OpT vector_op;
        bool invert = false;
        if (flags & eCASE) {
          vector_op = op;
        } else {
          switch (op) {
          case NUOp::eBiEq: case NUOp::eBiTrieq:
            // map logical eq into bitwise xnor
            // logical equivalence (~a) ^ (b) will end up folding
            // into the computationally more efficient ~(a^b)
            vector_op = NUOp::eBiBitXor;
            invert = true;
            break;
          case NUOp::eBiNeq: case NUOp::eBiTrineq: 
            // map logical neq into bitwise xor
            vector_op = NUOp::eBiBitXor;
            break;
          case NUOp::eBiLogAnd: 
            // map logical and into bitwise and
            vector_op = NUOp::eBiBitAnd;
            break;
          case NUOp::eBiLogOr:  
            // map logical or into bitwise or
            vector_op = NUOp::eBiBitOr;
            break;
          case NUOp::eBiBitAnd: case NUOp::eBiBitOr: case NUOp::eBiBitXor:
            vector_op = op;
            break;
          default:
            // This should not have made it through the build method
            NU_ASSERT (false, prototype);
            vector_op = op;
            break;
          }
        }
        // 3. create the binary operator
        if (invert) {
          bool sign = arg0->isSignedResult ();
          arg0 = new NUUnaryOp (NUOp::eUnBitNeg, arg0, arg0->getLoc ());
          arg0->setSignedResult (sign);
        }
        *expr = new NUBinaryOp (vector_op, arg0, arg1, prototype->getLoc ());

      } else {
        // generate a scalar expression
        NUExpr *arg0;
        NUExpr *arg1;
        create (n, &arg0, flags);
        create (n, &arg1, flags);
        // 2. create the binary operator
        *expr = new NUBinaryOp (op, arg0, arg1, prototype->getLoc ());
      }
    }
    break;
  case eVectorCond:
    {
      // We have matched a ternary condition in which the selector is
      // collapsible.
      const NUExpr *prototype = dynamic_cast <const NUOp *> (n [1].getExpr ());
      n++;
      NUExpr *sel;
      NUExpr *then_part;
      NUExpr *else_part;
      create (n, &then_part, flags);    // collapsed
      create (n, &else_part, flags);    // collapsed
      create (n, &sel, (flags|eCOND)&~eINNER);  // collapsed
      CopyContext ctx (NULL, NULL);
      NUUnaryOp *not_sel = new NUUnaryOp (NUOp::eUnBitNeg, sel->copy (ctx), 
        sel->getLoc ());
      *expr = new NUBinaryOp (NUOp::eBiBitOr,
        new NUBinaryOp (NUOp::eBiBitAnd, sel, then_part, then_part->getLoc ()),
        new NUBinaryOp (NUOp::eBiBitAnd, not_sel, else_part, else_part->getLoc ()),
        prototype->getLoc ());
    }
    break;
  case eIndexedConstant:
  case eIndexedConstantSigned:
    // This should have been dealt with in the vanilla expression create method.
    NU_ASSERT (symbol != eIndexedConstant, mHead);
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
}

void Collapser::Matcher::create (iterator &n, NUTernaryOp **expr, const UInt32 flags)
{
  const Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eScalarCond:
    // a conditional
    {
      // grab the operator and the prototype
      const NUExpr *prototype = n [1].getExpr ();
      NU_ASSERT (n [2].getOp () == NUOp::eTeCond, prototype);
      n++;
      // form the operands
      NUExpr *arg0;
      NUExpr *arg1;
      NUExpr *arg2;
      create (n, &arg1, flags);
      create (n, &arg2, flags);
      create (n, &arg0, flags|eSCALAR|eCOND);
      *expr = new NUTernaryOp (NUOp::eTeCond, arg0, arg1, arg2, prototype->getLoc ());
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
}

//
// Matching of NULvalue
//

void Collapser::Matcher::build (const NULvalue *prototype, const UInt32 flags, 
  NUNet **net, Span *span)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  switch (prototype->getType ()) {
  case eNUVarselLvalue:
    build (dynamic_cast <const NUVarselLvalue *> (prototype), flags, net, span);
    break;
  case eNUMemselLvalue:
    build (dynamic_cast <const NUMemselLvalue *> (prototype), flags, net, span);
    break;
  case eNUIdentLvalue:
    build (dynamic_cast <const NUIdentLvalue *> (prototype), flags, net, span);
    break;
  case eNUConcatLvalue:
    if (!(flags & eCASE)) {
      // Currently this is only allowed for the lhs of assignments when
      // collapsing assignments embedded in case statements. The mechanism for
      // detected recursive assignment must be extended to deal with multiple
      // nets on the lhs...
      FAIL ();
      return;
    } else {
      build (dynamic_cast <const NUConcatLvalue *> (prototype), flags, net, span);
    }
    break;
  default:
    // this is an lvalue that we cannot yet collapse
    FAIL ();
    return;
    break;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NULvalue *test, const UInt32 flags,
  Span *span)
{
  if (test == NULL) {
    return false;         // nothing to match
  }
  Symbol symbol = n.getSymbol ();
  // match the lvalue
  switch (symbol) {
  case eLvalueVarsel:
  case eLvalueVarselIndexed:
  case eLvalueVarselCaseIndexed:
    if (!apply (n, dynamic_cast <const NUVarselLvalue *> (test), flags, span)) {
      return false;
    }
    break;
  case eLvalueIdent:
    if (!apply (n, dynamic_cast <const NUIdentLvalue *> (test), flags, span)) {
      return false;
    }
    break;
  case eLvalueMemsel:
    if (!apply (n, dynamic_cast <const NUMemselLvalue *> (test), flags, span)) {
      return false;
    }
    break;
  case eLvalueCaseConcat:
    if (!apply (n, dynamic_cast <const NUConcatLvalue *> (test), flags, span)) {
      return false;
    }
    break;
  case eLvalueConcat:
    switch (test->getType ()) {
    case eNUIdentLvalue:
      if (!apply (n, dynamic_cast <const NUIdentLvalue *> (test), flags, span)) {
        return false;
      }
      break;
    default:
      return false;
      break;
    }
    break;
  default:
    MATCHER_ASSERT (false, test);
    return false;
    break;
  }
  // validate the span overlap
  if ((flags & eCASE) && !span->isSameLength (mSpan)) {
    // For case expansion all the rhs widths must match the span width. Offsets
    // of the lsb w.r.t. the index are tested in the specific apply methods.
    return false;                   // interpret as a failed match
  } else if (flags & eCASE) {
    // it is a case vectorisation and the lvalues are consistent
    mSubspan = *span;
  } else if (span->isFloating ()) {
    // It's an abstract span so just let it pass through. The span may be
    // anchored by the rhs expression, but on the first application of the
    // lvalue matcher any anchoring nuclei are embedded in an expression proxy.
    mSubspan = *span;
  } else if (span->isJustAbove (*this, mSpan)) {
    // extending the span upwards
    span->bind (Span::eIncreasing, test);
    mSubspan = *span;
  } else if (span->isJustBelow (*this, mSpan)) {
    // extending the span downwards
    span->bind (Span::eDecreasing, test);
    mSubspan = *span;
  } else {
    return false;                   // partitions are not contiguous
  }
  return true;
}

void Collapser::Matcher::create (iterator &n, NULvalue **result, const UInt32 flags)
{
  *result = NULL;
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eFail:
    NU_ASSERT (symbol != eFail, mHead); // apply should have failed
    break;
  case eLvalueMemsel:
    create (n, (NUMemselLvalue **) result, flags);
    break;
  case eLvalueVarsel:
  case eLvalueVarselCaseIndexed:
  case eLvalueVarselIndexed:
    create (n, (NUVarselLvalue **) result, flags);
    break;
  case eLvalueIdent:
    {
      // This is the lhs of an assignment within a case statement item
      NU_ASSERT (flags & eCASE, mHead);
      const NUIdentLvalue *prototype = dynamic_cast <const NUIdentLvalue *> (n [1].getPrototype ());
      CopyContext ctx (NULL, NULL);
      *result = prototype->copy (ctx);
    }
    n++;
    break;
  case eLvalueConcat:
  case eLvalueCaseConcat:
    create (n, (NUConcatLvalue **) result, flags);
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
}

//
// Matching NUVarselLvalue
//

void Collapser::Matcher::build (const NUVarselLvalue *prototype, const UInt32 flags, 
  NUNet **net, Span *span)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *span = *prototype->getRange ();
  NULvalue *sliced = prototype->getLvalue ();
  // The span must be set before the matcher for the identifer is built so that
  // the Memsel identifier can examine the lhs index.
  //
  // For case collapsing we initially assume that the index will vary against
  // the case selector value. However, if on the first application, it turns
  // out that the same span is used for each partition, then change this flag
  // argument to zero and change the placeholder to be the lsb of the prototype
  // offset.
  Symbol symbol;
  iterator net_placeholder, case_offset_placeholder, case_multiple_placeholder;
  if (!prototype->isConstIndex ()) {
    symbol = eLvalueVarselIndexed;
    append (INSTRUCTION (symbol));
    append (Instruction (prototype));   // n [1]
    append (&net_placeholder);          // n [2]
  } else if (flags & eCASE) {
    symbol = eLvalueVarselCaseIndexed;
    append (INSTRUCTION (symbol));
    append (Instruction (prototype));   // n [1]
    append (&net_placeholder);          // n [2]
    append (&case_offset_placeholder);  // n [3]
    append (&case_multiple_placeholder); // n [4]
  } else {
    symbol = eLvalueVarsel;
    append (INSTRUCTION (symbol));
    append (Instruction (prototype));   // n [1]
    append (&net_placeholder);          // n [2]
  }
  // Now build the matcher for the identifier. For now, matchers are build for
  // identifiers or memory selectors.
  Span ident_span;
  switch (sliced->getType ()) {
  case eNUMemselLvalue:
    build (dynamic_cast <const NUMemselLvalue *> (sliced), flags|eVARSEL_IDENT, net, &ident_span);
    break;
  case eNUIdentLvalue:
    build (dynamic_cast <const NUIdentLvalue *> (sliced), flags|eVARSEL_IDENT, net, &ident_span);
    break;
  default:
    // This is a construct that this does not know how to collapse
    FAIL ();
    return;
    break;
  }
  MATCHER_ASSERT (*net != NULL, prototype);
  *net_placeholder = Instruction (*net);
  switch (symbol) {
  case eLvalueVarselIndexed:
    {
      UInt32 syn;
      build (prototype->getIndex (), flags|eINDEX|eSCALAR|eNO_XZ, &syn);
      if (span->getLength () != 1) {
        // TODO - not sure how to get a test case with a width greater than one
        FAIL ();
        return;
      }
    }
    break;
  case eLvalueVarselCaseIndexed:
    // match case indexing expressions of the form:
    // index = multiple * sel + offset
    // Of course, we cannot compute values for multiple or offset until we see
    // the second case item. Just glue the lsb into the case_offset operand
    *case_offset_placeholder = Instruction ((SInt32) ((ConstantRange &) *span).getLsb ());
    if (!isInBounds (*net, *span)) {
      FAIL ();
      return;
    }
    break;
  case eLvalueVarsel:
    if (!isInBounds (*net, *span)) {
      FAIL ();
      return;
    }
    break;
  default:
    MATCHER_ASSERT (false, mHead);
    FAIL ();
    return;
    break;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUVarselLvalue *test, const UInt32 flags,
  Span *span)
{
  if (test == NULL) {
    return false;         // nothing to match
  }
  iterator handle = n;
  const NUVarselLvalue *prototype = dynamic_cast <const NUVarselLvalue *> (n [1].getPrototype ());
  const NUNet *net = n [2].getNet ();
  MATCHER_TEST (prototype != NULL, test);
  n++;
  // Match the lvalue
  NULvalue *lvalue = test->getLvalue ();
  Span subspan;
  switch (n.getSymbol ()) {
  case eFail:
    return false;         // something we could not match
    break;
  case eLvalueMemsel:
    if (!apply (n, dynamic_cast <const NUMemselLvalue *> (lvalue), flags|eVARSEL_IDENT, &subspan)) {
      return false;
    }
    break;
  case eLvalueIdent:
    if (!apply (n, dynamic_cast <const NUIdentLvalue *> (lvalue), flags|eVARSEL_IDENT, &subspan)) {
      return false;
    }
    break;
  default:
    NU_ASSERT (false, test);
    return false;
    break;
  }
  if (test->isConstIndex () && !isInBounds (net, *test->getRange ())) {
    // the index is out of range so fail the match
    return false;
  }
  // Test the span
  *span = *test->getRange ();
  // Case collapsing of the lhs is for either a contanst index, or an index
  // that varies with the case selector. Which variety we have is not known
  // until the first application of the matcher.
  switch (handle.getSymbol ()) {
  case eLvalueVarselIndexed:
    if (mNMatched == 0) {
      mFirstMatchedIndex = test->getIndex ();
    } else {
      mLastMatchedIndex = test->getIndex ();
    }
    if (!apply (n, test->getIndex (), flags|eSCALAR|eINDEX)) {
      return false;
    }
    break;
  case eLvalueVarsel:
    break;
  case eLvalueVarselCaseIndexed:
    {
      SInt32 case_multiple;
      SInt32 case_offset;
      if (!test->isConstIndex () || !isInBounds (net, *span)) {
        return false;                     // just fail the match
      }
      if (span->isFloating ()) {
        return false;                   // can only do this tranformation on a concrete span
      } else if (mNMatched > 0) {
        // get the case multiple from the plan
        case_multiple = handle [4].getSInt ();
        case_offset = handle [3].getSInt ();
      } else if (span->isFloating ()) {
        // can only do this inference on a concrete span
      } else {
        MATCHER_TEST (mCaseBaseIndex != mCaseIndex, test);
        // compute the case offset and the case multiple
        // If x_0 is the lsb of the first lvalue and x_1 of this lvalue, then:
        // x_0 = case_multiple * mCaseBaseIndex + case_offset
        // x_1 = case_multiple * mCaseIndex + case_offset
        const SInt32 x_0 = handle [3].getSInt ();   // lsb of the first lvalue
        const SInt32 x_1 = ((ConstantRange &) *span).getLsb ();
        // Solve the equations for case_multiple and case_index:
        case_multiple = (x_0 - x_1) / (mCaseBaseIndex - mCaseIndex);
        case_offset = (mCaseIndex * x_0 - mCaseBaseIndex * x_1) / (mCaseIndex - mCaseBaseIndex);
        handle [3] = case_offset;
        handle [4] = case_multiple;
      }
      if (case_multiple < 0 && case_offset == 0) {
        // I haven't been able to come up with a test case for this one... if
        // assert fails during regression please add a test case to the
        // case_inference/lhs-slice-nn series of tests.
        MATCHER_TEST (case_multiple >= 0 || case_offset != 0, prototype);
        return false;
      }
      if (((ConstantRange &) *span).getLsb () != mCaseIndex * case_multiple + case_offset) {
        // does not match
        return false;
      }
    }
    break;
  default:
    NU_ASSERT (false, prototype);
    break;
  }
  return true;
}

void Collapser::Matcher::create (iterator &n, NUVarselLvalue **result, const UInt32 flags)
{
  const Symbol symbol = n.getSymbol ();
  const NUVarselLvalue *prototype = dynamic_cast <const NUVarselLvalue *> (n [1].getPrototype ());
  const NUNet *net = n [2].getNet ();
  NU_ASSERT (prototype != NULL, mHead);
  NULvalue *ident = NULL;
  NUMemselLvalue* msIdent;
  NUIdentLvalue* idIdent;

  iterator handle = n;
  n++;
  switch (n.getSymbol ()) {
  case eLvalueMemsel:
    create (n, &msIdent, flags|eVARSEL_IDENT);
    ident = msIdent;
    break;
  case eLvalueIdent:
    create (n, &idIdent, flags|eVARSEL_IDENT);
    ident = idIdent;
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
  switch (symbol) {
  case eLvalueVarselIndexed:
    {
      NUExpr *index;
      create (n, &index, flags|eSCALAR|eINDEX);
      *result = new NUVarselLvalue (ident, index, mSpan.bindRange (*this), prototype->getLoc ());
    }
    break;
  case eLvalueVarsel:
    // plain old partition
    *result = new NUVarselLvalue (ident, mSpan.bindRange (*this), prototype->getLoc ());
    break;
  case eLvalueVarselCaseIndexed:
    {
      UInt32 width = mSpan.getLength ();
      SInt32 case_multiple;
      SInt32 case_offset;
      if (mNMatched > 0) {
        case_multiple = handle [4].getSInt ();
        case_offset = handle [3].getSInt ();
      } else {
        case_multiple = 0;
        case_offset = handle [3].getSInt ();
      }
      //fprintf (stderr, "######### case_multiple=%d case_offset=%d\n", case_multiple, case_offset);
      if (case_multiple == 0) {
        // The index is constant.
        // test/case_inference/lhs-slice-01.v (big-endian)
        // test/case_inference/lhs-slice-02.v (little-endian)
        ConstantRange range (case_offset + width - 1, case_offset);
        *result = new NUVarselLvalue (ident, net->makeRange (range), prototype->getLoc ());
      } else if (case_multiple == 1 && case_offset == 0) {
        // For an expression that is index
        // test/case_inference/lhs-slice-03.v (big-endian)
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        *result = new NUVarselLvalue (ident, index, range, prototype->getLoc ());
      } else if (case_multiple == 1 && case_offset != 0) {
        // For an expression that is index + offset
        // test/case_inference/lhs-slice-04.v (big-endian)
        ConstantRange range (width - 1, 0);
        NUOp::OpT opt = case_offset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus;
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *difference = NUConst::create (false, std::abs (case_offset), 32, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (opt, index, difference, prototype->getLoc ());
        *result = new NUVarselLvalue (ident, expr, range, prototype->getLoc ());
      } else if (case_multiple == -1 && case_offset == 0) {
        // No test case - fail in apply
        NU_ASSERT (false, prototype);
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *expr = new NUUnaryOp (NUOp::eUnMinus, index, prototype->getLoc ());
        *result = new NUVarselLvalue (ident, expr, range, prototype->getLoc ());
      } else if (case_multiple == -1 && case_offset != 0) {
        // test/case_inference/lhs-slice-06.v (little-endian)
        // test/case_inference/lhs-slice-05.v (little-endian)
        ConstantRange range (width - 1, 0);
        NUOp::OpT opt = case_offset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus;
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *difference = NUConst::create (false, std::abs (case_offset), 32, prototype->getLoc ());
        NUExpr *term = new NUUnaryOp (NUOp::eUnMinus, index, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (opt, term, difference, prototype->getLoc ());
        *result = new NUVarselLvalue (ident, expr, range, prototype->getLoc ());
      } else if (case_multiple > 0 && case_offset == 0) {
        // test/case_inference/lhs-slice-07.v
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *factor = NUConst::create (false, case_multiple, 32, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (NUOp::eBiUMult, index, factor, prototype->getLoc ());
        *result = new NUVarselLvalue (ident, expr, range, prototype->getLoc ());
      } else if (case_multiple < 0 && case_offset == 0) {
        // No test case - fail in apply
        NU_ASSERT (false, prototype);
      } else if (case_multiple > 0 && case_offset != 0) {
        // test/case_inference/lhs-slice-08.v
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *factor = NUConst::create (false, case_multiple, 32, prototype->getLoc ());
        NUExpr *term = new NUBinaryOp (NUOp::eBiUMult, index, factor, prototype->getLoc ());
        NUOp::OpT opt = case_offset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus;
        NUExpr *difference = NUConst::create (false, std::abs (case_offset), 32, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (opt, term, difference, prototype->getLoc ());
        *result = new NUVarselLvalue (ident, expr, range, prototype->getLoc ());
      } else if (case_multiple < 0 && case_offset != 0) {
        // test/case_inference/lhs-slice-09.v
        ConstantRange range (width - 1, 0);
        const SourceLocator &loc = prototype->getLoc ();
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, loc);
        NUExpr *factor = new NUBinaryOp (NUOp::eBiSMult,
          index, NUConst::create (true, case_multiple, 32, loc), loc);
        factor->setSignedResult (true); // case_multiple is negative, so factor is signed
        NUExpr *expr = new NUBinaryOp (NUOp::eBiPlus,
          factor, NUConst::create (true, case_offset, 32, loc), loc);
        expr->setSignedResult (true);   // factor is signed so index expression is signed
        *result = new NUVarselLvalue (ident, expr, range, prototype->getLoc ());
      } else {
        NU_ASSERT (false, prototype);
      }
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
}

//
// Matching NUIdentLvalue
//

void Collapser::Matcher::build (const NUIdentLvalue *prototype, const UInt32 flags,
  NUNet **net, Span *span)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *net = prototype->getIdent ();
  const NUVectorNet *vector;
  if (!(flags & (eVARSEL_IDENT|eCASE)) 
      && ((*net)->isMem () 
        || !(flags & eALLOW_LHSCONCAT))) {
    // do not concat memory nets, protected nets, or hierarchical references
    FAIL ();
    return;
  } else if (!(flags & (eVARSEL_IDENT|eCASE)) 
    && (*net)->isInClkPath ()) {
    FAIL ();
    return;
#if USE_LHS_CONCAT
  } else if (!(flags & (eVARSEL_IDENT|eCASE)) 
    && (*net)->isTristate () && (mConcats == NULL || !mConcats->canBeEliminated (*net))) {
    FAIL ();
    return;
#if 0
  } else if ((*net)->isInClkPath () /* || (*net)->isEdgeTrigger () */) {
    FAIL ();
    return;
#endif
  } else if (!(flags & (eVARSEL_IDENT|eCASE)) && (flags & eALLOW_LHSCONCAT)) {
    // Try to build a vectorised set where the lvalue becomes a concat and the
    // rvalue a vectorised expression.
    *span = FloatingSpan (prototype, prototype->getBitSize (), 0); // an abstract span
    append (INSTRUCTION (eLvalueConcat));
    append (Instruction (prototype));
    NU_ASSERT (mLvalueConcat == NULL, prototype);
    mLvalueConcat = new LvalueConcat (*this, prototype);
    mLvalueConcat->acrete (prototype, mSpan.getLength ());
    append (mLvalueConcat);
    mFlags |= eLHSCONCAT;
    mLHSNets.insert (*net);
    mCollapsedIntoConcat = true;        // for diagnostics and recursive assignment detected
#else
    // This is an NUIdentLvalue on the LHS that is not part of a varsel. This
    // can only be collapsed if it's in a case
    FAIL ();
    return;
#endif
  } else if ((vector = dynamic_cast <const NUVectorNet *> (*net)) != NULL) {
    // The span must be the range of the underlying vector net.
    *span = *vector->getRange ();
    append (INSTRUCTION (eLvalueIdent));
    append (Instruction (prototype));
  } else if (flags & eCASE) {
    // It is a scalar net, used as the lhs of an assign in a case item. Only
    // the width of the span really matters so just fake the span as (0,0).
    *span  = ConstantRange (0, 0);
    append (INSTRUCTION (eLvalueIdent));
    append (Instruction (prototype));
  } else if (flags & eVARSEL_IDENT) {
    // This is a scalar net, but it has been used as the index expression of a
    // varsel. While this is odd, it's not necessarily an error.
    *span = FloatingSpan (/* *this, */ prototype, 1, 0);
    append (INSTRUCTION (eLvalueIdent));
    append (Instruction (prototype));
  } else {
    // The condition should have been exhaustive... if this assert gets tickled
    // then the set of conditions has been screwed up.
    NU_ASSERT (false, prototype);
  }
  if (mSlicedLHS == NULL) {
    mSlicedLHS = prototype;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUIdentLvalue *test, const UInt32 /*flags*/,
  Span *span)
{
  if (test == NULL) {
    return false;
  }
  const Symbol symbol = n.getSymbol ();
  const NUIdentLvalue *prototype = dynamic_cast <const NUIdentLvalue *> (n [1].getPrototype ());
  switch (symbol) {
  case eLvalueIdent:
    {
      const NUVectorNet *vectornet;
      if (!MATCHER_VERIFY (prototype != NULL)) {
        return false;
      } else if (prototype->getIdent () != test->getIdent ()) {
        return false;
      } else if ((vectornet = dynamic_cast <const NUVectorNet *> (test->getIdent ())) != NULL) {
        // Always set the subspan in this matcher in case the top level lvalue is
        // just an NUIdentLvalue. If it's a varsel, then the calling apply method
        // will set the subspan to the partition defined in the varsel.
        *span = *vectornet->getRange ();
      } else {
        // If it's not a vector, then just fake the subspan.
        *span = ConstantRange (0, 0);
      }
      n++;
    }
    break;
  case eLvalueConcat:
    {
      const NUNet *net = test->getIdent ();
      if (net->isMem ()) {
        return false;                   // do not concat memory net
      } else if (net->isInClkPath ()) {
        return false;
      } else if (net->isTristate () && mConcats != NULL && mConcats->canBeEliminated (net)) {
        // form the concat because if common concat factors it away the the
        // local net will be replaced by the common concat temporary and Z
        // values will be preserved
      } else if (net->isTristate ()) {
        return false;
      } else if (!addLhsNet (net)) {
        return false;                   // recursive reference to an earlier rhs net
      }
      UInt32 size = test->getBitSize ();
      LvalueConcat *concat = n [2].getLvalueConcat ();
      *span = FloatingSpan (test, size, mNMatched+1);
      concat->acrete (test, size);
      mLHSNets.insert (net);
      n++;
    }
    break;
  default:
    MATCHER_ASSERT (false, test);
    return false;
    break;
  }
  return true;
}

void Collapser::Matcher::create (iterator &n, NUIdentLvalue **result, const UInt32 /*flags*/)
{
  NU_ASSERT (n.getSymbol () == eLvalueIdent, mHead);
  const NUIdentLvalue *prototype = dynamic_cast <const NUIdentLvalue *> (n [1].getPrototype ());
  NU_ASSERT (prototype != NULL, mHead);
  CopyContext ctx (NULL, NULL);
  *result = dynamic_cast <NUIdentLvalue *> (prototype->copy (ctx));
  NU_ASSERT (*result != NULL, prototype);
  n++;
}

//
// Matching NUIdentRvalue
//

void Collapser::Matcher::build (const NUIdentRvalue *prototype, const UInt32 flags,
  NUNet **net, UInt32 *attr)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *net = prototype->getIdent ();
  *attr = 0;
#if !USE_RHS_CONCAT
  const NUVectorNet *vectornet;
#endif
  if (flags & eSCALAR) {
    append (INSTRUCTION (eRvalueIdentScalar));
    append (Instruction (prototype));
  } else if (flags & eVARSEL_IDENT) {
    // this is the ident part of a varsel
    append (INSTRUCTION (eRvalueIdent));
    append (Instruction (prototype));
  } else if (flags & eCASE) {
    // This is a net on the rhs of a case statement... just match it directly.
    MATCHER_ASSERT (!(flags & eVARSEL_IDENT), prototype); // don't move this branch!
    append (INSTRUCTION (eRvalueIdentCase));
    append (Instruction (prototype));
  } else if ((*net)->isMem ()) {
    FAIL ();
    return;
  } else if ((flags & eLSB_ONLY) && prototype->determineBitSize () != 1) {
    // Expecting a single bit only
    FAIL ();
    return;
#if 0
  } else if ((*net)->isInClkPath () /* || (*net)->isEdgeTrigger () */) {
    FAIL ();
    return;
#endif
#if USE_RHS_CONCAT
  } else if ((flags & eALLOW_RHSCONCAT) && mSpan.getLength () == prototype->determineBitSize ()) {
    // Note: use determineBitSize so that the width of the net is used.
    RvalueConcat *concat = new RvalueConcat (*this);
    concat->append (0, mSpan.getLength (), prototype);
    append (INSTRUCTION (eRvalueConcat));
    append (Instruction (prototype));
    append (Instruction (concat));
#else
  } else if ((vectornet = dynamic_cast <const NUVectorNet *> (*net)) == NULL
    && mSpan.getLength () == 1) {
    // this is just a scalar net that may be collapsed into a concat but only
    // if each assignment is just a single bit wide
    append (INSTRUCTION (eRvalueIdentBit));
    append (Instruction (prototype));
  } else if (vectornet == NULL) {
    FAIL ();                            // cannot collapse this one
    return;
#endif
  } else if (flags & eREDUCING) {
    append (Instruction (eRvalueIdent));
    append (Instruction (prototype));
  } else {
    // This is a vector net that is not part of a partition. The width of the
    // net has to match the width of the lhs so that a valid concat expression
    // can be built.
    FAIL ();
    return;
  }
  // Now check that this does not build a recursive concat.
  if (!(mFlags & eLHSCONCAT)) {
    // not a lhs concat
  } else if (!addRhsNet (*net)) {
    FAIL ();
    return;                             // recursive reference
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUIdentRvalue *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  const NUIdentRvalue *prototype = dynamic_cast <const NUIdentRvalue *> (n [1].getExpr ());
  const NUNet *net = test->getIdent ();
#if !USE_RHS_CONCAT
  const NUVectorNet *vectornet;
#endif
  if (prototype == NULL) {
    // This can happen if we are matching an eRvalueConcat and the prototype is
    // a memsel.
    return false;
#if USE_RHS_CONCAT
  } else if (net->isMem () || net->isHierRef ()) {
    return false;
#else
  } else if (net != prototype->getIdent ()) {
    return false;
#endif
  }
  switch (n.getSymbol ()) {
  case eRvalueIdentCase:
  case eRvalueIdentScalar:
  case eRvalueIdent:
#if USE_RHS_CONCAT
    if (net != prototype->getIdent ()) {
      return false;
    }
#endif
    // just matching the net is enough
    break;
  case eRvalueIdentBit:
    // This is just a scalar net on the rhs. We can collapse into a concat iff
    // the width of every lhs is 1
#if USE_RHS_CONCAT
    if (net != prototype->getIdent ()) {
      return false;
    }
#endif
    if (mSubspan.getLength () > 1) {
      return false;
    }
    break;
  case eRvalueConcat:
    if (net->isTristate ()) {
      return false;
    } else if (/*!mSubspan.isFloating () && */ mSubspan.getLength () == test->determineBitSize ()) {
      RvalueConcat *concat = n [2].getRvalueConcat ();
      concat->append (mNMatched+1, mSubspan.getLength (), test);
    } else {
      return false;
    }
    break;
  default:
    MATCHER_ASSERT (false, test);
    return false;
    break;
  }
  n++;
  // check that if we want only the LSB that we are either the ident of a
  // varsel or the net is a scalar
  if (flags & eVARSEL_IDENT) {
    // ident of a varsel
  } else if ((flags & eLSB_ONLY) && net->getBitSize () > 1) {
    return false;
  }
  // If this identifier is not part of a varsel, then we must check whether
  // there is a recursive reference to the lhs of an assignment.
  if (flags & eVARSEL_IDENT) {
    // this is the identfier of a varsel... recursive is tested in the calling
    // apply method
  } else if (mCollapsedIntoConcat && mLHSNets.find (net) != mLHSNets.end ()) {
    // Never allow a recursive reference when the lhs is collapsing into a concat.
    return false;
  } else if (mCollapsedIntoConcat) {
    // Collapsing into concat, but it's not recursive 
  } else if (net == mLHSNet) {
    // This is the net that is assigned to on the lhs. Strange...
    return false;
  }
  // Now check that this does not build a recursive concat.
  if (!(mFlags & eLHSCONCAT)) {
    // not a lhs concat
  } else if (!addRhsNet (net)) {
    return false;                       // recursive reference
  }
  return true;
}

void Collapser::Matcher::create (iterator &n, NUIdentRvalue **result, const UInt32 /*flags*/)
{
  switch (n.getSymbol ()) {
  case eRvalueIdent:
  case eRvalueIdentCase:
  case eRvalueIdentScalar:
    {
      // grab the prototype
      const NUIdentRvalue *prototype = dynamic_cast <const NUIdentRvalue *> (n [1].getExpr ());
      NU_ASSERT (prototype != NULL, mHead);
      // just make a copy
      CopyContext ctx (NULL, NULL);
      *result = static_cast <NUIdentRvalue *> (prototype->copy (ctx));
      n++;
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
}

//
// Matching NUMemselLvalue
//

void Collapser::Matcher::build (const NUMemselLvalue *prototype, const UInt32 flags,
  NUNet **net, Span *span)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  UInt32 num_dims = prototype->getNumDims ();
  *net = prototype->getIdent ();
  const NUVectorNet *vectornet;
  const NUMemoryNet *memorynet;
  if ((vectornet = dynamic_cast <const NUVectorNet *> (*net)) != NULL) {
    // Grab the range straight off the vector
    *span = *vectornet->getRange ();
  } else if ((memorynet = dynamic_cast <const NUMemoryNet *> (*net)) != NULL) {
    // There is a range for each dimension of a memory net. Use the range of
    // the last dimension as the range of the net.
    *span = *memorynet->getRange (num_dims - 1);
  } else {
    // it is a scalar net so just fudge the range
    *span = ConstantRange (0, 0);
  }
  UInt32 mark = mNextMark++;
  append (INSTRUCTION (eLvalueMemsel));
  append (Instruction (prototype));
  append (Instruction (mark));
  for (UInt32 i = 0; i < num_dims; i++) {
    build_memsel_index (prototype->getIndex (i), flags);
  }
  // put a marker into the instruction stream to check consistency in the
  // corresponding apply method.
  append (INSTRUCTION (eMark));
  append (Instruction (mark));
  if (mSlicedLHS == NULL) {
    mSlicedLHS = prototype;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUMemselLvalue *test, const UInt32 flags,
  Span *span)
{
  if (test == NULL) {
    return false;
  }
  MATCHER_TEST (n.getSymbol () == eLvalueMemsel, test);
  if (test == NULL) {
    return false;
  }
  const NUMemselLvalue *prototype = dynamic_cast <const NUMemselLvalue *> (n [1].getPrototype ());
  MATCHER_TEST (prototype != NULL, test);
  const UInt32 mark = n [2].getUInt ();
  n++;
  const NUNet *net = test->getIdent ();
  if (net != prototype->getIdent ()
    || test->getNumDims () != prototype->getNumDims ()) {
    return false;
  }
  for (UInt32 i = 0; i < prototype->getNumDims (); i++) {
    if (!apply_memsel_index (n, test->getIndex (i), flags)) {
      return false;
    }
  }
  // Check that we are at the same mark that was emitted by the corresponding
  // build method. If this assertion fails then there is a bug in either the
  // build method for one of the indices or in the apply method for one of the
  // indices.
  MATCHER_ASSERT (n.getSymbol () == eMark && n[1].getUInt () == mark, test);
  n++;
  const NUVectorNet *vectornet;
  const NUMemoryNet *memorynet;
  if ((vectornet = dynamic_cast <const NUVectorNet *> (net)) != NULL) {
    // Grab the range straight off the vector
    *span = *vectornet->getRange ();
  } else if ((memorynet = dynamic_cast <const NUMemoryNet *> (net)) != NULL) {
    // There is a range for each dimension of a memory net. Use the range of
    // the last dimension as the range of the net.
    *span = *memorynet->getRange (test->getNumDims () - 1);
  } else {
    // it is a scalar net so just fudge the range
    *span = ConstantRange (0, 0);
  }
  return true;
}

void Collapser::Matcher::create (iterator &n, NUMemselLvalue **result, const UInt32 flags)
{
  NU_ASSERT (n.getSymbol () == eLvalueMemsel, mHead);
  const NUMemselLvalue *prototype = dynamic_cast <const NUMemselLvalue *> (n [1].getPrototype ());
  NUNet *net = prototype->getIdent ();
  const UInt32 mark = n [2].getUInt ();
  n++;
  // build the address vector
  NUExprVector address;
  for (UInt32 i = 0; i < prototype->getNumDims (); i++) {
    NUExpr *index;
    create_memsel_index (n, prototype->getIndex (i), &index, flags);
    address.push_back (index);
  }
  // build the memory access
  *result = new NUMemselLvalue (net, &address, prototype->getLoc ());
  (*result)->putResynthesized (true);   // resynth has already occurred
  // Check that we are at the same mark that was emitted by the corresponding
  // build method. If this assertion fails then there is a bug in either the
  // build method for one of the indices or in the create method for one of the
  // indices.
  NU_ASSERT (n.getSymbol () == eMark && n [1].getUInt () == mark, prototype);
  n++;
}

//
// Matching of NUMemselRvalue
//


void Collapser::Matcher::build (const NUMemselRvalue *prototype, const UInt32 flags,
  NUNet **net, UInt32 *attr)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  UInt32 mark = mNextMark++;            // used to skip over operand pattern in create
  *net = prototype->getIdent ();
  *attr = 0;
  if (flags & eLSB_ONLY) {
    FAIL ();
    return;
  }
  if (flags & (eVARSEL_IDENT|eSCALAR|eCASE)) {
    append (INSTRUCTION (eRvalueMemsel));
    append (Instruction (prototype));
    append (Instruction (mark));
    for (UInt32 i = 0; i < prototype->getNumDims (); i++) {
      build_memsel_index (prototype->getIndex (i), flags);
    }
    // put a marker into the instruction stream to check consistency in the
    // corresponding apply method.
    append (INSTRUCTION (eMark));
    append (Instruction (mark));
#if USE_RHS_CONCAT
  } else if ((flags & eALLOW_RHSCONCAT) &&
    !mSpan.isFloating () && mSpan.getLength () == prototype->determineBitSize ()) {
    RvalueConcat *concat = new RvalueConcat (*this);
    concat->append (0, mSpan.getLength (), prototype);
    append (INSTRUCTION (eRvalueConcat));
    append (Instruction (prototype));
    append (Instruction (concat));
#endif
  } else {
    FAIL ();
    return;
  }
  // Now check that this does not build a recursive concat.
  if (!(mFlags & eLHSCONCAT)) {
    // not a lhs concat
  } else if (!addRhsNet (*net)) {
    FAIL ();
    return;                             // recursive reference
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUMemselRvalue *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  const Symbol symbol = n.getSymbol ();
  const NUNet *net = test->getIdent ();
  switch (symbol) {
  case eRvalueMemsel:
    {
      const NUMemselRvalue *prototype = dynamic_cast <const NUMemselRvalue *> (n [1].getExpr ());
      MATCHER_TEST (prototype, mHead);
      UInt32 mark;
      mark = n [2].getUInt ();
      n++;
      if (net != prototype->getIdent ()) {
        return false;                     // different memory
      }
      // Now check that the indices match
      for (UInt32 i = 0; i < prototype->getNumDims (); i++) {
        if (!apply_memsel_index (n, test->getIndex (i), flags)) {
          return false;
        }
      }
      // Check that we are at the same mark that was emitted by the
      // corresponding build method. If this assertion fails then there is a
      // bug in either the build method for one of the indices or in the apply
      // method for one of the indices.
      MATCHER_ASSERT (n.getSymbol () == eMark && n [1].getUInt () == mark, test);
      n++;
    }
    break;
  case eRvalueConcat:
    {
      const NUMemselRvalue *prototype = dynamic_cast <const NUMemselRvalue *> (n [1].getExpr ());
      if (prototype == NULL) {
        // The prototype was probably an ident.
        return false;
      } else if (!mSubspan.isFloating () && mSubspan.getLength () == test->determineBitSize ()) {
        RvalueConcat *concat = n [2].getRvalueConcat ();
        concat->append (mNMatched+1, mSubspan.getLength (), test);
      } else {
        return false;
      }
      n++;
      // We do not bother looking at the indices when the memsel will be squashed
      // into a concat.
    }
    break;
  default:
    MATCHER_TEST (false, mHead);
    break;
  }
  // Now check that this does not build a recursive concat.
  if (!(mFlags & eLHSCONCAT)) {
    // not a lhs concat
  } else if (!addRhsNet (net)) {
    return false;                       // recursive reference
  }
  return true;
}

void Collapser::Matcher::create (iterator &n, NUMemselRvalue **result, const UInt32 flags)
{
  NU_ASSERT (n.getSymbol () == eRvalueMemsel, mHead);
  const NUMemselRvalue *prototype = dynamic_cast <const NUMemselRvalue *> (n [1].getExpr ());
  const UInt32 mark = n [2].getUInt ();
  n++;
  // build the address vector
  NUExprVector address;
  for (UInt32 i = 0; i < prototype->getNumDims (); i++) {
    NUExpr *index;
    create_memsel_index (n, prototype->getIndex (i), &index, flags);
    address.push_back (index);
  }
  // build the memory access
  NUNet *net = prototype->getIdent ();
  *result = new NUMemselRvalue (net, &address, prototype->getLoc ());
  (*result)->putResynthesized (true);   // resynth has already occurred
  // Check that we are at the same mark that was emitted by the corresponding
  // build method. If this assertion fails then there is a bug in either the
  // build method for one of the indices or in the create method for one of the
  // indices.
  NU_ASSERT (n.getSymbol () == eMark && n [1].getUInt () == mark, prototype);
  n++;
}

//
// Matching of NUVarselRvalue
//

void Collapser::Matcher::build_index (const Symbol symbol, const NUNet *net, 
  const NUVarselRvalue *prototype, const UInt32 flags)
{
  ConstantRange range (*prototype->getRange ());
  if (!isInBounds (net, range)) {
    FAIL ();
    return;
  }
  const bool is_const_index = prototype->isConstIndex ();
  // If the span is floating, then fixate it against the constant range
  if (!mSpan.isFloating () || !is_const_index) {
    // if it's not floating, or this is not a constant index, then we cannot fixate
  } else if (!(mFlags & eLHSCONCAT) || mLvalueConcat == NULL) {
    // Currently the only way that the span may be floating is if we are
    // constructing a concat on the lhs of an assign.
    NU_ASSERT (mFlags & eLHSCONCAT, prototype);
    NU_ASSERT (mLvalueConcat != NULL, prototype);
  } else if (fixate (&mSpan, range.getLsb (), mLvalueConcat->getPrototype (), prototype)) {
    // The floating span has been fixed against this constant range
  } else {
    // This is the first fixation for this matcher... it really should not fail
    NU_ASSERT (false, prototype);
  }
  // Now we can assume that either the span is not floating, or we are dealing
  // with an indexed range.
  NU_ASSERT (!is_const_index || !mSpan.isFloating (), prototype);
  switch (symbol) {
  case eRvalueVarselCase:
    MATCHER_ASSERT (flags & eCASE, prototype);
    if (!is_const_index) {
      append (INSTRUCTION (eVarselCaseIndexed));
      append (Instruction (range.getLength ()));
      append (Instruction (range.getLsb ()));
      UInt32 syn;
      build (prototype->getIndex (), flags|eINDEX|eSCALAR|eNO_XZ, &syn);
    } else {
      // This is mapped into a*sel + b for some constants a and b. However, we
      // cannot infer a and b until the first application so in the initial
      // pattern just record the lsb and a placeholder.
      // width of the lhs.
      append (INSTRUCTION (eVarselCaseConst));
      append (Instruction (range.getLength ()));
      append (Instruction (range.getLsb ()));
      append (INSTRUCTION (eNone));     // placeholder
    }
    break;
  case eRvalueVarselScalar:
    MATCHER_ASSERT (flags & eSCALAR, prototype);
    if (!is_const_index) {
      FAIL ();
    } else {
      append (INSTRUCTION (eVarselFixedConst));
      append (Instruction (range.getLsb ()));
      append (Instruction (range.getLength ()));
    }
    break;
  case eRvalueVarselVector:
    {
      UInt32 width = range.getLength ();
      // This is on the rhs of a vectorisable assignment.
      MATCHER_ASSERT (!(flags & (eSCALAR|eCASE|eINDEX)), prototype);
      if (width != mSpan.getLength ()) {
        // The width differs from the lhs width. This could be vectorised, but
        // I'm just not dealing with this case now. Entered as bug 5221. Other
        // places that need changing are tagged with the string BUG5221.
        FAIL ();
      } else if (is_const_index) {
        // It is a constant partition. Record the different in the lsb of the
        // span and the range for comparison.
        SInt32 offset = mSpan.getLsb (*this) - range.getLsb ();
        append (INSTRUCTION (eVarselConst));
        append (Instruction (offset));
        append (Instruction (width));
      } else if (mSpan.isFloating ()) {
        // We do not want to know about fixating floating spans from indexed
        // partsels.
        // TODO - can this ever fixate the span
        FAIL ();
      } else {
        // It is not a constant partition, record the difference in the lsb of
        // the span and the range for comparison.
        SInt32 offset = mSpan.getLsb (*this) - range.getLsb ();
        append (INSTRUCTION (eVarselIndexed));
        append (Instruction (offset));
        append (Instruction (width));
        // Now construct a matcher for the non-constant index.
        UInt32 syn;
        build (prototype->getIndex (), flags|eINDEX|eSCALAR|eNO_XZ, &syn);
      }
    }
    break;
  default:
    NU_ASSERT (false, prototype);       // this is really bad...
    break;
  }
}

bool Collapser::Matcher::apply_index (iterator &n, const Symbol parent, const NUNet *net, 
  const NUVarselRvalue *prototype, const NUVarselRvalue *test, const UInt32 flags, ConstantRange *range,
  UInt32 *local)
{
  const Symbol symbol = n.getSymbol ();
  *range = *test->getRange ();
  const bool is_const_index = test->isConstIndex ();
  // If the span is floating, then fixate it against the constant range
  if (!mSubspan.isFloating () || !is_const_index) {
    // if it's not floating, or this is not a constant index, then we cannot
    // fixate
  } else if (range->getLength () != mSubspan.getLength ()) {
    // the anchor point for a floating span must be the same width as the
    // left-hand side
    return false;
  } else if (!(mFlags & eLHSCONCAT) || mLvalueConcat == NULL) {
    // Currently the only way that the span may be floating is if we are
    // constructing a concat on the lhs of an assign.
    NU_ASSERT (mFlags & eLHSCONCAT, prototype);
    NU_ASSERT (mLvalueConcat != NULL, prototype);
  } else if (fixate (&mSubspan, range->getLsb (), mLvalueConcat->getCurrent (), prototype)) {
    // The floating span has been fixed against this constant range
  } else {
    // Fixation will fail if there is an overlap with a previous subspan
    return false;
  }
  // Now we can assume that either the span is not floating, or we are dealing
  // with an indexed range.
  NU_ASSERT (!is_const_index || !mSubspan.isFloating (), test);
  switch (symbol) {
  case eVarselConst:
    {
      SInt32 offset = n [1].getSInt ();
      UInt32 width  = n [2].getUInt ();
      SInt32 difference = mSpan.getLsb (*this) - range->getLsb ();
      MATCHER_TEST (parent == eRvalueVarselVector, test);
      MATCHER_TEST ((flags & (eCASE|eSCALAR|eINDEX)) == 0, test);
      if (!is_const_index) {
        return false;                   // prototype was const, this is not
      } else if (mSubspan.isFloating ()) {
        // Weird... it should have fixated...
        MATCHER_ASSERT (!mSpan.isFloating (), test);
        return false;
#if USE_RHS_CONCAT
      } else if (mNMatched == 0 && difference == offset && width > 1) {
        // This is the first application and the lsb is the same as the lsb of
        // the prototype. However, the width of the partition is greater than
        // one so we do not want to ultimately create a concat with a repeat
        // count greater than one.
        return false;
      } else if (mNMatched == 0 && difference == offset && (flags & eALLOW_RHSCONCAT)) {
        // This is the first application, the lsb is the same as the lsb of the
        // prototype and the width of both the prototype and this partition are
        // one. Change the index operator to eVarselFixedConst so that
        // subsequent matches test for the same lsb.
        n [0] = INSTRUCTION (eVarselFixedConst);
        n [1] = Instruction (range->getLsb ());
        *local |= eCONCAT;              // this collapses into a concat
        return apply_index (n, parent, net, prototype, test, flags, range, local);
      } else if (mNMatched == 0 && difference == offset) {
        // concats are not allowed
        return false;
#endif
      } else if (!mSpan.isAdjacent (*this, *range, offset)) {
        // TODO: currently we must see the ranges in order. Ideally if out of
        // order but complete concat elements are vectorised a suitable order
        // will be assigned to the concat. For now, the range is constrained to
        // continuously grow up or down.
        return false;
      } else if (mSubspan.getLength () != range->getLength ()) {
        return false;                   // width differs from the lhs width - BUG5221.
      } else if (range->getLsb () + offset != mSubspan.getLsb (*this)) {
        return false;                   // endpoints do not line up
      }
      n++;
    }
    break;
  case eVarselFixedConst:
    {
      SInt32 lsb = n [1].getSInt ();
      UInt32 width = n [2].getUInt ();
      if (!is_const_index) {
        return false;                   // prototype was const, this is not
      } else if (width != range->getLength ()) {
        return false;                   // width differs from the lhs width - BUG5221.
      } else if (range->getLsb () != lsb) {
        return false;                   // must have the same index
      }
      n++;
    }
    break;
  case eVarselIndexed:
    {
      SInt32 offset = n [1].getSInt ();
      SInt32 difference = mSpan.getLsb (*this) - range->getLsb ();
      MATCHER_TEST (parent == eRvalueVarselVector, test);
      MATCHER_TEST ((flags & (eCASE|eSCALAR|eINDEX)) == 0, test);
      if (mSubspan.isFloating ()) {
        // we cannot fixate at an indexed partsel so just fail the match
        return false;
      }
      if (is_const_index) {
        return false;                   // prototype was const, this not
      } else if (mSubspan.isFloating ()) {
        // weird... if it is a constant range then the subspan should be fixed
        NU_ASSERT (!mSubspan.isFloating (), test);
        return false;                   // cannot mix indexed and floating
#if USE_RHS_CONCAT
      } else if (mNMatched == 0 && difference == offset 
        && mSpan.getLength () > 1 && range->getLength () > 1) {
        // This is the first application and the lsb is the same as the lsb of
        // the prototype. However, the width of the partition is greater than
        // one so we do not want to ultimately create a concat with a repeat
        // count greater than one.
        return false;
      } else if (mNMatched == 0 && difference == offset && (flags & eALLOW_RHSCONCAT)) {
        // This is the first application, and the lsb is the same as the lsb of
        // the prototype. Change the index operator to eVarselFixedConst so
        // that subsequent matches test for the same lsb.
        n [0] = INSTRUCTION (eVarselFixedIndexed);
        n [1] = Instruction (range->getLsb ());
        *local |= eCONCAT;              // this collapses into a concat
        return apply_index (n, parent, net, prototype, test, flags, range, local);
      } else if (mNMatched == 0 && difference == offset) {
        return false;
#endif
      } else if (mSubspan.getLength () != range->getLength ()) {
        return false;                   // width differs from the lhs width - BUG5221.
      } else if (range->getLsb () + offset != mSubspan.getLsb (*this)) {
        return false;                   // endpoints do not line up
      }
      n++;
      // match the index
      if (!apply (n, test->getIndex (), flags|eINDEX|eSCALAR|eNO_XZ)) {
        return false;
      }
    }
    break;
  case eVarselFixedIndexed:
    {
      SInt32 lsb = n [1].getSInt ();
      UInt32 width = n [2].getUInt ();
      if (is_const_index) {
        return false;                   // prototype was not const, this is const
      } else if (width != range->getLength ()) {
        return false;                   // width differs from the lhs width - BUG5221.
      } else if (range->getLsb () != lsb) {
        return false;                   // must have the same index
      }
      n++;
      if (!apply (n, test->getIndex (), flags|eINDEX|eSCALAR|eNO_XZ)) {
        return false;
      }
    }
    break;
  case eVarselCaseConst:
    {
      SInt32 multiplier, offset;
      const UInt32 width = n [1].getUInt ();
      // Recover or deduce the constants of the affine transformation of the constant.
      if (!is_const_index) {
        return false;                   // prototype was not const, this is
      } else if (mSubspan.isFloating ()) {
        return false;                   // TODO - investigate floating spans
                                        // and case matching
      } else if (range->getLength () != width) {
        return false;                   // width of each case varsel rvalue must match
      } else if (mNMatched == 0) {
        // Deduce the case offset and the case multiple
        // If x_0 is the lsb of the first lvalue and x_1 of this rvalue, then:
        // x_0 = multiple * mCaseBaseIndex + offset
        // x_1 = multiple * mCaseIndex + offset
        const SInt32 x_0 = n [2].getSInt (); // lsb of the first rvalue
        const SInt32 x_1 = range->getLsb ();
        // Solve the equations for multiple and offset:
        multiplier = (x_0 - x_1) / (mCaseBaseIndex - mCaseIndex);
        offset = (mCaseIndex * x_0 - mCaseBaseIndex * x_1) / (mCaseIndex - mCaseBaseIndex);
        // Save the constants in the pattern
        n [2] = Instruction (offset);
        n [3] = Instruction (multiplier);
      } else {
        // Recover the offset and multiple
        offset = n [2].getSInt ();
        multiplier = n [3].getSInt ();
      }
      // Now test against the multiple and offset.
      if (range->getLsb () == mCaseIndex * multiplier + offset) {
        // conforms to the affine transformation
      } else if (mNMatched == 1 && multiplier == 1 && offset == 1 && range->getLsb () == 1<<mCaseIndex) {
        return false;
      } else {
        return false;                   // does not match
      }
      n++;
    }
    break;
  case eVarselCaseIndexed:
    {
      const UInt32 width = n [1].getUInt ();
      SInt32 lsb = n [2].getSInt ();
      if (is_const_index) {
        return false;                   // prototype was nont const, this is
      } else if (mSubspan.isFloating ()) {
        return false;                   // TODO - investigate floating spans
                                        // and indexed partsels
      } else if (range->getLength () != width || range->getLsb () != lsb) {
        return false;                   // width of each case varsel rvalue must match
      }
      n++;
      // now match the non-constant expression
      if (!apply (n, test->getIndex (), flags)) {
        return false;
      }
    }
    break;
  default:
    MATCHER_TEST (false, mHead);
    break;
  }
  return true;
}

void Collapser::Matcher::build (const NUVarselRvalue *prototype, const UInt32 flags, 
                                NUNet **net, UInt32 *attr)
{
  if (prototype == NULL) {
    FAIL ();    // lost the expression... weird...
    return;
  }
  *attr = 0;
  //
  // Tag with the type of varsel
  //
  Symbol symbol;
  if (flags & eCASE) {
    // a varsel in a case item... look for transformations on the case selector
    symbol = eRvalueVarselCase;
  } else if (flags & eSCALAR) {
    // a varsel that will not be vectorised
    symbol = eRvalueVarselScalar;
  } else {
    // a varsel that will be vectorised
    symbol = eRvalueVarselVector;
  }
  iterator net_placeholder;
  append (INSTRUCTION (symbol));
  append (Instruction (prototype));
  append (&net_placeholder);
  append (Instruction (UInt32 (0)));    // local attributes
  //
  // Build the pattern for the identifier
  //
  const NUExpr *ident = prototype->getIdentExpr ();
  *net = NULL;
  switch (ident->getType ()) {
  case NUExpr::eNUMemselRvalue:
    {
      UInt32 syn;
      build (dynamic_cast <const NUMemselRvalue *> (ident), flags|eVARSEL_IDENT, net, &syn);
    }
    break;
  case NUExpr::eNUVarselRvalue:
    {
      UInt32 syn;
      build (dynamic_cast <const NUVarselRvalue *> (ident), flags|eVARSEL_IDENT, net, &syn);
    }
    break;
  case NUExpr::eNUIdentRvalue:
    {
      UInt32 syn;
      build (dynamic_cast <const NUIdentRvalue *> (ident), flags|eVARSEL_IDENT, net, &syn);
    }
    break;
  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ:
    // I have it on the best authority that VHDL can induce varsels where the
    // ident expression is a constant. The matcher just does not want to touch
    // anything quite this baroque.
    //
    // JDM: also, Verilog parameters can induce such expressions.
    break;
  case NUExpr::eNUConcatOp:
    // Complex VHDL clocking constructs can produce concats that can't
    // otherwise be created.  We'll skip them too.  Fall through to the
    // *net==NULL check below
    break;
  case NUExpr::eNUBinaryOp:
  case NUExpr::eNUUnaryOp:
  case NUExpr::eNUTernaryOp:
    break;
  default:
    MATCHER_ASSERT2 (false, ident, mHead);
    break;
  }
  // Now that we have plucked apart the ident, the replace the net placeholder
  // with a reference to the actual net.
  if (*net == NULL) {
    // recursive-call to build failed, so propagate the failure up.
    FAIL();
    return;
  }
  *net_placeholder = *net; 
  //
  // build a pattern for the varsel index
  //
  if (flags & eVARSEL_IDENT) {
    // this is a varsel used as the ident part of a varsel, so treat the index
    // like the index of a memsel
    build_memsel_index (prototype->getIndex (), flags);
  } else if ((flags & eLSB_ONLY) && mSpan.getLength () != 1) {
    FAIL ();
    return;
  } else {
    build_index (symbol, *net, prototype, flags);
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUVarselRvalue *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  //
  // Match the tag
  //
  iterator handle = n;
  const Symbol symbol = n.getSymbol ();
  const NUVarselRvalue *prototype = dynamic_cast <const NUVarselRvalue *> (n [1].getExpr ());
  const NUNet *net = n [2].getNet ();
  const UInt32 local = n [3].getUInt ();
  MATCHER_TEST (prototype, mHead);
  n++;
  //
  // Validate the matcher symbol against the inherited attributes
  //
  switch (symbol) {
  case eRvalueVarselVector:
    if (flags & (eINDEX|eSCALAR)) {
      return false;
    }
    break;
  case eRvalueVarselScalar:
    if (!(flags|eSCALAR)) {
      return false;
    }
    break;
  case eRvalueVarselCase:
    break;
  default:
    MATCHER_TEST (false, prototype);
    break;
  }
  //
  // Match the identifier
  //
  switch (n.getSymbol ()) {
  case eRvalueMemsel:
    if (!apply (n, dynamic_cast <const NUMemselRvalue *> (test->getIdentExpr ()), flags|eVARSEL_IDENT)) {
      return false;
    }
    break;
  case eRvalueVarselVector:
    if (!apply (n, dynamic_cast <const NUVarselRvalue *> (test->getIdentExpr ()), flags|eVARSEL_IDENT)) {
      return false;
    }
    break;
  case eRvalueIdentCase:
  case eRvalueIdent:
  case eRvalueIdentScalar:
    if (!apply (n, dynamic_cast <const NUIdentRvalue *> (test->getIdentExpr ()), flags|eVARSEL_IDENT)) {
      return false;
    }
    break;
  case eRvalueVarselScalar:
    if (!apply (n, dynamic_cast <const NUVarselRvalue *> (test->getIdentExpr ()), flags|eVARSEL_IDENT|eSCALAR)) {
      return false;
    }
    break;
  default:
    MATCHER_TEST (false, mHead);
    return false;
    break;
  }
  //
  // Match the index
  //
  ConstantRange range;
  UInt32 new_local = local;
  bool nested_varsel = (flags & eVARSEL_IDENT);
  if (nested_varsel 
    && (*prototype->getRange () != *test->getRange () || !apply_memsel_index (n, test->getIndex (), flags))) {
    // When varsels are nested the index of the ident varsel must be an exact
    // match with the prototype.
    return false;
  } else if (!nested_varsel && !apply_index (n, symbol, net, prototype, test, flags, &range, &new_local)) {
    return false;
  }
  if (!isInBounds (net, *test->getRange ())) {
    // the component is out of range for the net
    return false;
  }
  if ((flags & eLSB_ONLY) && test->getRange ()->getLength () > 1) {
    return false;
  }
  if (new_local != local) {
    handle [3] = Instruction (new_local);
  }

  //
  // Check for recursive assignments in vector collapse candidates
  //

  // If the RHS references the net on the LHS of the assignment statement, then
  // we need to insure that the recursive reference can be legitimately
  // collapsed.
  if (flags & eCASE) {
    // We are merely manipulating subscripts in case collapse so recursive
    // assignments are allowed.
  } else if (mCollapsedIntoConcat && mLHSNets.find (net) != mLHSNets.end ()) {
    // Never allow a recursive reference when the lhs is collapsing into a concat.
    return false;
  } else if (mCollapsedIntoConcat) {
    // Collapsing into concat, but it's not recursive 
  } else if (net != mLHSNet) {
    // Collapsing into a vector, but it's not recursive
  } else {
    switch (mAssignFlavour) {
    case eAssignContinuous:
      // If we are processing continuous assigns, do not allow
      // anything other than references to the same bit as
      // referenced on the RHS. See bug 399.
      if (mSubspan.isFloating () || mSubspan != range) {
        return false;
      }
      break;
    case eAssignBlocking:
      // If we're referring to the LHS net, only verify if this is a
      // reference in the same direction as our LHS preference. This
      // means that if we are increasing, the RHS offset needs to be >=
      // the LHS offset. 
      //
      // The LHS needs to have the value on the RHS computed before it
      // can be assigned..
      MATCHER_TEST (mLHSNet != NULL, test);
      if (mSubspan == range) {
        // slice on rhs is exactly the slice on the lhs
      } else if (mSubspan.isFloating ()) {
        // lhs is an abstract span... assume the worst
        return false;
      } else if (mSubspan.bindRange (*this).overlaps (range)) {
        // slice on rhs overlaps the slice on the lhs... fail
        return false;
      } else if (mSpan.isIncreasing ()) {
        if (mSubspan.getLsb (*this) < range.getLsb ()) {
          // The indices on the LHS are increasing, and the LHS index is less
          // than the RHS index... permitted.
        } else {
          return false;
        }
      } else if (mSpan.isDecreasing ()) {
        if (mSubspan.getLsb (*this) > range.getLsb ()) {
          // The indices on the LHS are decreasing, and the LHS index is
          // greater than the RHS index... permitted.
        } else {
          return false;
        }
      } else {
        // The direction should have been set when the lhs was matched. Just
        // fail the match in production code, but crash and burn in debug.
        MATCHER_TEST (mSpan.isIncreasing () || mSpan.isDecreasing (), test);
        return false;
      }
      break;
    case eAssignNonBlocking:
      // anything is permitted
      break;
    case eNone:
      // mAssignFlavour should have been set when the lhs pattern matcher was
      // constructed. Assert out in debugging, but just fail the match in
      // production code.
      MATCHER_TEST (mAssignFlavour != eNone, test);
      return false;
      break;
    default:
      MATCHER_TEST (false, test);          // this is just wrong
      break;
    }
  }
  return true;
}

void Collapser::Matcher::create (iterator &n, NUVarselRvalue **result, const UInt32 flags)
{
  *result = NULL;
  const Symbol symbol = n.getSymbol ();
  const NUVarselRvalue *prototype = dynamic_cast <const NUVarselRvalue *> (n [1].getExpr ());
  const NUNet *net = n [2].getNet ();
  // const SInt32 offset = n [3].getSInt ();
  // iterator handle = n;
  //  const UInt32 is_case_indexed = n [4].getUInt ();
  NU_ASSERT (prototype, mHead);
  NU_ASSERT (n.getSymbol () == eRvalueVarselCase
    || n.getSymbol () == eRvalueVarselScalar
    || n.getSymbol () == eRvalueVarselVector,
    prototype);
  NU_ASSERT (net, prototype);
  n++;
  //
  // Construct the ident part of the collapsed varsel
  //
  NUExpr *ident = NULL;
  NUMemselRvalue *memIdent;
  NUVarselRvalue *partIdent;
  NUIdentRvalue *valIdent;

  switch (n.getSymbol ()) {
  case eRvalueMemsel:
    create (n, &memIdent, flags|eVARSEL_IDENT);
    ident = memIdent;
    break;
  case eRvalueVarselVector:
    create (n, &partIdent, flags|eVARSEL_IDENT);
    ident = partIdent;
    break;
  case eRvalueVarselScalar:
    create (n, &partIdent, flags|eVARSEL_IDENT|eSCALAR);
    ident = partIdent;
    break;
  case eRvalueIdent:
  case eRvalueIdentScalar:
    create (n, &valIdent, flags|eVARSEL_IDENT);
    ident = valIdent;
    break;
  default:
    NU_ASSERT (false, mHead);
  }
  NU_ASSERT (ident != NULL, prototype);
  //
  // Construct the index expression
  //
  switch (n.getSymbol ()) {
  case eVarselConst:
    {
      SInt32 offset = n [1].getSInt ();
      NU_ASSERT (!mSpan.isFloating (), prototype);
      ConstantRange span (mSpan.bindRange (*this));
      ConstantRange offsets (span.getMsb () - offset, span.getLsb () - offset);
      *result = new NUVarselRvalue (ident, net->makeRange (offsets), prototype->getLoc ());
      NU_ASSERT (symbol == eRvalueVarselVector, prototype);
      n++;
    }
    break;
  case eVarselFixedConst:
    {
      SInt32 lsb = n [1].getSInt ();
      UInt32 width = n [2].getUInt ();
      ConstantRange range (lsb + width - 1, lsb);
      *result = new NUVarselRvalue (ident, net->makeRange (range), prototype->getLoc ());
      n++;
    }
    break;
  case eVarselCaseConst:
    {
      const UInt32 width = n [1].getUInt ();
      const SInt32 offset = n [2].getSInt ();
      const SInt32 multiplier = mNMatched > 0 ? n [3].getSInt () : 0;
      // screw case: when mNMatched is zero, it's a user full-case, with just
      // one item.
      if (multiplier == 0) {
        // The index is constant.
        // test/case_inference/rhs-slice-01.v (big-endian)
        // test/case_inference/rhs-slice-02.v (little-endian)
        ConstantRange range (offset + width - 1, offset);
        *result = new NUVarselRvalue (ident, net->makeRange (range), prototype->getLoc ());
      } else if (multiplier == 1 && offset == 0) {
        // For an expression that is index
        // test/case_inference/rhs-slice-03.v (big-endian)
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        *result = new NUVarselRvalue (ident, index, range, prototype->getLoc ());
      } else if (multiplier == 1 && offset != 0) {
        // For an expression that is index + offset
        // test/case_inference/rhs-slice-04.v (big-endian)
        ConstantRange range (width - 1, 0);
        NUOp::OpT opt = offset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus;
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *difference = NUConst::create (false, std::abs (offset), 32, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (opt, index, difference, prototype->getLoc ());
        *result = new NUVarselRvalue (ident, expr, range, prototype->getLoc ());
      } else if (multiplier == -1 && offset == 0) {
        // No test case - fail in apply
        NU_ASSERT (false, prototype);
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *expr = new NUUnaryOp (NUOp::eUnMinus, index, prototype->getLoc ());
        *result = new NUVarselRvalue (ident, expr, range, prototype->getLoc ());
      } else if (multiplier == -1 && offset != 0) {
        // test/case_inference/rhs-slice-05.v (little-endian)
        // test/case_inference/rhs-slice-06.v (little-endian)
        ConstantRange range (width - 1, 0);
        NUOp::OpT opt = offset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus;
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *difference = NUConst::create (false, std::abs (offset), 32, prototype->getLoc ());
        NUExpr *term = new NUUnaryOp (NUOp::eUnMinus, index, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (opt, term, difference, prototype->getLoc ());
        *result = new NUVarselRvalue (ident, expr, range, prototype->getLoc ());
      } else if (multiplier > 0 && offset == 0) {
        // test/case_inference/rhs-slice-07.v
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *factor = NUConst::create (false, multiplier, 32, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (NUOp::eBiUMult, index, factor, prototype->getLoc ());
        *result = new NUVarselRvalue (ident, expr, range, prototype->getLoc ());
      } else if (multiplier < 0 && offset == 0) {
        // No test case - fail in apply
        NU_ASSERT (false, prototype);
      } else if (multiplier > 0 && offset != 0) {
        // test/case_inference/rhs-slice-08.v
        ConstantRange range (width - 1, 0);
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, prototype->getLoc ());
        NUExpr *factor = NUConst::create (false, multiplier, 32, prototype->getLoc ());
        NUExpr *term = new NUBinaryOp (NUOp::eBiUMult, index, factor, prototype->getLoc ());
        NUOp::OpT opt = offset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus;
        NUExpr *difference = NUConst::create (false, std::abs (offset), 32, prototype->getLoc ());
        NUExpr *expr = new NUBinaryOp (opt, term, difference, prototype->getLoc ());
        *result = new NUVarselRvalue (ident, expr, range, prototype->getLoc ());
      } else if (multiplier < 0 && offset != 0) {
        // test/case_inference/lhs-slice-09.v
        ConstantRange range (width - 1, 0);
        const SourceLocator &loc = prototype->getLoc ();
        NUExpr *index = new NUIdentRvalue (mCaseSelectorId, loc);
        NUExpr *factor = new NUBinaryOp (NUOp::eBiSMult, 
          index, NUConst::create (true, multiplier, 32, loc), loc);
        factor->setSignedResult (true); // multiplier is negative, so factor is signed
        NUExpr *expr = new NUBinaryOp (NUOp::eBiPlus,
          factor, NUConst::create (true, offset, 32, loc), loc);
        expr->setSignedResult (true);   // factor is signed so index expression is signed
        *result = new NUVarselRvalue (ident, expr, range, prototype->getLoc ());
      } else {
        NU_ASSERT (false, prototype);
      }
      n++;
    }
    break;
  case eVarselCaseIndexed:
    {
      const UInt32 width = n [1].getUInt ();
      SInt32 lsb = n [2].getSInt ();
      NUExpr *expr;
      n++;
      create (n, &expr, flags|eINDEX|eSCALAR|eNO_XZ);
      ConstantRange range (lsb + width - 1, lsb);
      *result = new NUVarselRvalue (ident, expr, range, prototype->getLoc ());
    }
    break;
  case eVarselFixedIndexed:
    {
      SInt32 lsb = n [1].getSInt ();
      UInt32 width = n [2].getUInt ();
      n++;
      NUExpr *index;
      create (n, &index, flags|eINDEX|eSCALAR|eNO_XZ);
      ConstantRange range (lsb + width - 1, lsb);
      *result = new NUVarselRvalue (ident, index, net->makeRange (range), prototype->getLoc ());
    }
    break;
  case eVarselIndexed:
    {
      NU_ASSERT (!mSpan.isFloating (), prototype);
      ConstantRange span (mSpan.bindRange (*this));
      SInt32 offset = n [1].getSInt ();
      SInt32 lsb = span.getLsb () - offset;
      n++;
      NUExpr *index;
      create (n, &index, flags|eINDEX|eSCALAR|eNO_XZ);
      ConstantRange range (lsb + span.getLength () - 1, lsb);
      *result = new NUVarselRvalue (ident, index, net->makeRange (range), prototype->getLoc ());
    }
    break;
  case eMemselAddress:
  case eMemselConstAddress:
    {
      // The ident part is a nested varsel. The index of the ident part is not
      // vectorise, it get treated the same as a memsel index.
      NU_ASSERT (flags & eVARSEL_IDENT, prototype);
      NUExpr *index;
      create_memsel_index (n, prototype->getIdentExpr (), &index, flags);
      ConstantRange range (*prototype->getRange ());
      *result = new NUVarselRvalue (ident, index, net->makeRange (range), prototype->getLoc ());
    }
    break;
  default:
    NU_ASSERT (false, prototype);
    break;
  }
}

//
// Matching of NUConcatOp
//

void Collapser::Matcher::build (const NUConcatOp *prototype, const UInt32 flags,
  UInt32 *attr)
{
  if (flags & eLSB_ONLY) {
    FAIL ();
    return;
  } else  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  const NUConst *constant;
  if ((flags & eSCALAR)
    && prototype->getNumArgs () == 2 
    && (constant = prototype->getArg (0)->castConstNoXZ ()) != NULL
    && constant->isZero ()) {
    UInt32 syn;
    // New sizing creates concats of the form {n'b0,expr} to pad expressions out to a expected size.
    append (INSTRUCTION (eRvalueZeroConcatOp));
    append (Instruction (prototype));
    append (Instruction (prototype->getBitSize ()));
    append (Instruction (constant->getBitSize ()));
    build (prototype->getArg (1), flags, &syn);
  } else if (flags & eCASE) {
    // Matching concats in the items of a case statement
    *attr = 0;
    append (INSTRUCTION (eRvalueConcatOp));
    append (Instruction (prototype));
    for (UInt32 i = 0; i < prototype->getNumArgs (); i++) {
      // It is critical to track the widths of the concat entries... otherwise we
      // can be fooled by different width constants
      UInt32 syn;
      const NUExpr *expr = prototype->getArg (i);
      append (INSTRUCTION (eRvalueConcatOpEntry));
      append (Instruction (expr->getBitSize ()));
      build (prototype->getArg (i), flags, &syn);
    }
  } else {
    // Not really sure how to collapse this kind of rhs, other than collapsing
    // in a case. Just concating the concats does not seem like it would gain
    // us much... the vectorised statement will just get pulled apart later
    FAIL ();
    return;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUConcatOp *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  } else if (flags & eLSB_ONLY) {
    return false;
  }
  const Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eRvalueZeroConcatOp:
    {
      const UInt32 full_width = n [2].getUInt ();
      const UInt32 zeroes = n [3].getUInt ();
      n++;
      const NUConst *constant;
      if (!(flags & eSCALAR)
        || test->getNumArgs () != 2
        || (constant = test->getArg (0)->castConstNoXZ ()) == NULL
        || !constant->isZero ()
        || test->getBitSize () != full_width
        || constant->getBitSize () != zeroes) {
        return false;
      } else if (!apply (n, test->getArg (1), flags)) {
        return false;
      } else {
        return true;
      }
    }
    break;
  case eRvalueConcatOp:
    {
      if (!MATCHER_VERIFY ((flags & eCASE))) {
        return false;
      }
      const NUConcatOp *prototype = dynamic_cast <const NUConcatOp *> (n [1].getExpr ());
      n++;
      if (prototype->getNumArgs () != test->getNumArgs ()) {
        return false;
      } else if (prototype->getRepeatCount () != test->getRepeatCount ()) {
        return false;
      }
      for (UInt32 i = 0; i < prototype->getNumArgs (); i++) {
        MATCHER_TEST (n.getSymbol () == eRvalueConcatOpEntry, prototype);
        const UInt32 prototype_width = n [1].getUInt ();
        const NUExpr *expr = test->getArg (i);
        n++;
        if (prototype_width != expr->getBitSize ()) {
          return false;
        } else if (!apply (n, test->getArg (i), flags)) {
          return false;
        }
      }
      return true;
    }
    break;
  default:
    MATCHER_TEST (false, test);
    return false;
    break;
  }
}

void Collapser::Matcher::create (iterator &n, NUConcatOp **result, const UInt32 flags)
{
  *result = NULL;
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eFail:
    NU_ASSERT (symbol != eFail, mHead); // apply should have failed
    break;
  case eReducingUnary:
    {
      // construct a repeating concat of the reducing operator
      NUOp::OpT op = n [2].getOp ();
      const NUExpr *prototype = n [1].getExpr ();
      n++;
      NUExpr *arg0;
      NUExprVector vector;
      create (n, &arg0, flags|eSCALAR|eREDUCING);
      vector.push_back (new NUUnaryOp (op, arg0, prototype->getLoc ()));
      *result = new NUConcatOp (vector, mSpan.getLength (), prototype->getLoc ());
    }
    break;
  case eRvalueZeroConcatOp:
    {
      // construct a concat with leading zeros
      NU_ASSERT (flags & eSCALAR, mHead);
      const NUExpr *prototype = n [1].getExpr ();
      n++;
      CopyContext cc (NULL, NULL);
      NUExpr *leading_zeros = prototype->getArg (0)->copy (cc);
      NUExpr *expr;
      create (n, &expr, flags);
      NUExprVector v;
      v.push_back (leading_zeros);
      v.push_back (expr);
      (*result) = new NUConcatOp (v, 1, prototype->getLoc ());
    }
    break;
  case eRvalueConcatOp:
    {
      NU_ASSERT (flags & eCASE, mHead);
      // we cannot juse copy the rhs... there may be indexed expressions to
      // translated
      NUExprVector vect;
      const NUConcatOp *prototype = dynamic_cast <const NUConcatOp *> (n [1].getExpr ());
      n++;
      for (UInt32 i = 0; i < prototype->getNumArgs (); i++) {
        NUExpr *expr;
        MATCHER_ASSERT (n.getSymbol () == eRvalueConcatOpEntry, prototype);
        //const UInt32 prototype_width = n [1].getUInt ();
        n++;
        create (n, &expr, flags);
        vect.push_back (expr);
      }
      (*result) = new NUConcatOp (vect, prototype->getRepeatCount (), prototype->getLoc ());
    }
    break;
  case eRvalueVarselVector:
    {
      NUVarselRvalue *rvalue;
      create (n, &rvalue, flags);
      NUExprVector vect;
      vect.push_back (rvalue);
      NU_ASSERT (rvalue->determineBitSize () == 1, rvalue);
      (*result) = new NUConcatOp (vect, mSpan.getLength(), rvalue->getLoc());
    }
    break;
  case eRvalueIdentBit:
    {
      const NUIdentRvalue *prototype = dynamic_cast <const NUIdentRvalue *> (n [1].getExpr ());
      CopyContext ctx (NULL, NULL);
      NUExpr *rvalue = prototype->copy (ctx);
      NUExprVector vect;
      vect.push_back (rvalue);
      NU_ASSERT (rvalue->determineBitSize () == 1, prototype);
      (*result) = new NUConcatOp (vect, mSpan.getLength(), rvalue->getLoc());
      n++;
    }
    break;
  case eRvalueConcat:
    {
      const NUExpr *prototype = n [1].getExpr ();
      RvalueConcat *pool = n [2].getRvalueConcat ();
      n++;
      CopyContext ctx (NULL, NULL);
      // First construct a map from the lsb of the concat component to a copy
      // of the concat component.
      RvalueConcat::ConcatMap::reverse_iterator it = pool->mMap.rbegin ();
      ConstantRange span (mSpan.getMsb (*this), mSpan.getLsb (*this));
      UtMap <SInt32, NUExpr *> elements;
      while (it != pool->mMap.rend ()) {
        const RvalueConcat::Segment segment = it->second;
        if (segment.mIndex > mNMatched) {
          // If this operand of a rhs was matched, but the matched failed later on
          // a the same rhs then this segment reflects the partial match of the
          // rhs. It's easier to allow the segment to be added to the concat
          // during matching but then throw it out here, than to undo state
          // from partial matched.
          it++;
          continue;
        }
        NUExpr *copy = segment.mExpr->copy (ctx);
        UInt32 net_width = copy->determineBitSize ();
        copy->resize (net_width);
        SInt32 lsb;
        if (segment.mWidth < net_width) {
          // This should not have been matched
          NU_ASSERT (segment.mWidth >= copy->determineBitSize (), prototype);
        } else if (!mSpanBindings.get (segment.mIndex, &lsb)) {
          // Cannot identify the bit position of this concat component
          NU_ASSERT (mSpanBindings.get (segment.mIndex, &lsb), segment.mExpr);
        } else if (segment.mWidth == net_width) {
          // just use the naked expression in the expression vector
          elements [lsb] = copy;
#if 1
        } else {
          // Sign or zero extending can change the meaning if this is a bitwise
          // expression. For example if a is one bit wide in:
          //   assign x = b[1:0] & ~a;
          //   assign y = b[3:2] & ~a;
          // then just extending a into (a ZXT 2) is wrong!
          NU_ASSERT (segment.mWidth == net_width, prototype);
#else
       /* This code signed or zero extended the segment of the concat to the
        * correct length. However, as noted above, this is incorrect in some
        * contexts. Currently, extending of the concat item is just precluded,
        * and if the matched expression is not wide enough then apply should
        * have thrown out the match. Some inherited context is needed in the
        * matcher. For example adding a flag to the concat pool segment that
        * indicates whether the segment can be zero or sign extended, or
        * whether the width must be an exact match. Re-enable this code once
        * this context is installed into the matcher.
        *
        * TODO: implemented a flag in the concat pool segment indicating
        *       whether the segment may be extended or not.
        */
        } else if (prototype->isSignedResult ()) {
          // The net is smaller than the required width of the concat
          // element. It is a signed result so sign extend.
          elements [lsb] =
            new NUBinaryOp (NUOp::eBiVhExt, copy,
              NUConst::create (false, segment.mWidth, 32, prototype->getLoc ()), 
              prototype->getLoc ());
        } else {
          // The net is smaller than the required width of the concat
          // element. It is an unsigned result so zero extend.
          elements [lsb] =
            new NUBinaryOp (NUOp::eBiVhZxt, copy,
              NUConst::create (false, segment.mWidth, 32, prototype->getLoc ()), 
              prototype->getLoc ());
#endif
        }
        it++;
      }
      // Now construct a vector of the elements ordered from msb to lsb
      NUExprVector vector;
      UtMap <SInt32, NUExpr *>::reverse_iterator it1 = elements.rbegin ();
      SInt32 last = 0xFFFF;
      while (it1 != elements.rend ()) {
        NU_ASSERT (it1->first < last, mHead);
        last = it1->first;
        vector.push_back (it1->second);
        it1++;
      }
      *result = new NUConcatOp (vector, 1, prototype->getLoc ());
    }
    break;
  case eZxtRelational:
    // A relational operator that gets mapped to a concat of the
    // expression. Folding will turn this into a condional when the concat is
    // just a single bit wide.
    {
      // grab the operator and the prototype
      NUOp::OpT op = n [2].getOp ();
      const NUExpr *prototype = n [1].getExpr ();
      const UInt32 width = n [3].getUInt ();
      n++;
      NUExpr *arg0, *arg1;
      create (n, &arg0, flags|eSCALAR);
      create (n, &arg1, flags|eSCALAR);
      NUExpr *cond = new NUBinaryOp (op, arg0, arg1, prototype->getLoc ());
      if (width > 1) {
        cond = new NUBinaryOp (NUOp::eBiVhZxt, cond,
          NUConst::create (false, width, 32, prototype->getLoc ()),
          prototype->getLoc ());
      }
      NUExprVector vector;
      vector.push_back (cond);
      NU_ASSERT (cond->determineBitSize () == width, prototype);
      // This assertion is satisfied if each matched piece is the same
      // width. This is enforced in the apply code for eZxtRelational.
      NU_ASSERT (mSpan.getLength () % width == 0, prototype);
      *result = new NUConcatOp (vector, mSpan.getLength ()/width, prototype->getLoc ());
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
}

//
// Matching of NUConst
//

void Collapser::Matcher::build (const NUConst *prototype, const UInt32 flags, UInt32 *attr)
{
  if (!MATCHER_VERIFY (prototype != NULL)) {
    FAIL ();
    return;
  }
  *attr = eCONSTANT;
  switch (prototype->getType ()) {
  case NUExpr::eNUConstXZ:
    if ((flags & eTEST_Z) && prototype->drivesOnlyZ ()) {
      *attr |= eIS_Z;
      append (INSTRUCTION (eVectorConstantZ));
      append (Instruction (prototype));
    } else if (flags & eNO_XZ) {
      FAIL ();
      return;
    } else if (prototype->drivesOnlyZ () && prototype->effectiveBitSize () == mSpan.getLength ()) {
      // allow collapsing of all Z constants into a wider all Z constant
      *attr |= eIS_Z;
      append (INSTRUCTION (eVectorConstantZ));
      append (Instruction (prototype));
    } else if (prototype->drivesZ ()) {
      // Note: test/case_inference/rhs-const-z.v gets broken when the Z
      // assignments are collapsed into a single assignment. It's probably a
      // bad idea to collapse undriven constant assignments anyway...
      FAIL ();
      return;
    } else if (flags & (eSCALAR|eCASE)) {
      // This is a non-collapsible part of an expression. Only an identical
      // constant will match. Fix the range and store the constant
      ConstantRange range (prototype->getBitSize (), 0);
      Constant *constant = new Constant (*this, mMsgContext, range, prototype, !(flags & eSCALAR));
      append (INSTRUCTION (eScalarConstantXZ));
      append (Instruction (prototype));
      append (Instruction (constant));
    } else if (mSpan.isFloating ()) {
      // Don't know how to deal with this yet
      FAIL ();
      return;
    } else if ((flags & eLSB_ONLY) && mSpan.getLength () != 1) {
      // The Constant constructor picks out the number of bits given by the
      // span. Thus the span must contain a single bit for eLSB_ONLY to work
      // correctly.
      FAIL ();
      return;
    } else {
      // Any value will match, the value constant is constructed as successive
      // assignments are matched
      ConstantRange span (mSpan.bindRange (*this));
      Constant *constant = new Constant (*this, mMsgContext, span, prototype, !(flags & eSCALAR));
      append (INSTRUCTION (eVectorConstant));
      append (Instruction (prototype));
      append (Instruction (constant));
    }
    break;
  case NUExpr::eNUConstNoXZ:
    {
      const bool is_signed = prototype->isSignedResult ();
      SInt32 value;
      UInt32 uvalue;
      if ((is_signed && (flags & (eCASE|eSCALAR)) && !prototype->getL (&value))
        || (!is_signed && (flags & (eCASE|eSCALAR)) && !prototype->getUL (&uvalue))) {
        // For both eCASE and eSCALAR the value of the constant is embedded in
        // the pattern matcher instruction stream. getL can fail if either the
        // constant will not fit into 32 bits, or if the constant was defined
        // with a symbol. In any case, we are just s.o.l. if a value cannot be
        // produced.
        FAIL ();
        return;
      } else if (is_signed && (flags & eCASE)) {
        // A constant on the rhs of an assignment in a case collapse may be
        // either fixed, or varying with the case selector. Initially assume that
        // it varies with the selector...
        append (INSTRUCTION (eIndexedConstantSigned));
        append (Instruction (prototype));
        append (Instruction (value));
      } else if (!is_signed && (flags & eCASE)) {
        // A constant on the rhs of an assignment in a case collapse may be
        // either fixed, or varying with the case selector. Initially assume that
        // it varies with the selector...
        ConstantFunction *function = new ConstantFunction (*this, prototype,
          prototype->getBitSize (), mCaseBaseIndex, uvalue);
        append (INSTRUCTION (eIndexedConstant));
        append (Instruction (prototype));
        append (Instruction (function));
      } else if (is_signed && (flags & eSCALAR)) {
        // This is a non-collapsible part of an expression. Only an identical
        // constant will match. Fix the range and store the constant in the
        // instruction stream.
        append (INSTRUCTION (eScalarConstantSigned));
        append (Instruction (prototype));
        append (Instruction (value));
      } else if (!is_signed && (flags & eSCALAR)) {
        // This is a non-collapsible part of an expression. Only an identical
        // constant will match. Fix the range and store the constant in the
        // instruction stream.
        append (INSTRUCTION (eScalarConstant));
        append (Instruction (prototype));
        append (Instruction (uvalue));
      } else if (mSpan.isFloating ()) {
        FAIL ();
        return;
      } else if (is_signed && (flags & eLSB_ONLY)) {
        // This is not adequately nutted out...
        FAIL ();
        return;
      } else if ((flags & eLSB_ONLY) && mSpan.getLength () != 1) {
        // The Constant constructor picks out the number of bits given by the
        // span. Thus the span must contain a single bit for eLSB_ONLY to work
        // correctly.
        FAIL ();
        return;
      } else {
        // Any value will match, the value constant is constructed as successive
        // assignments are matched
        ConstantRange span (mSpan.bindRange (*this));
        Constant *constant = new Constant (*this, mMsgContext, span, prototype, !(flags & eSCALAR));
        append (INSTRUCTION (eVectorConstant));
        append (Instruction (prototype));
        append (Instruction (constant));
      }
    }
    break;
  default:
    MATCHER_ASSERT (false, prototype);
    FAIL ();
    return;
    break;
  }
}

bool Collapser::Matcher::apply (iterator &n, const NUConst *test, const UInt32 flags)
{
  if (test == NULL) {
    return false;
  }
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eFail:
    return false;                       // something we could not match
    break;
  case eVectorConstantZ:
    {
      if (!test->drivesOnlyZ () || test->effectiveBitSize () != mSubspan.getLength ()) {
        return false;
      }
    }
    break;
  case eVectorConstant:
    {
      Constant *constant = n[2].getConstant ();
      // match any value of constant in test, but the value is acreted into the
      // constant.
      if (test->drivesZ ()) {
        // Note: test/case_inference/rhs-const-z.v gets broken when the Z
        // assignments are collapsed into a single assignment. It's probably a
        // bad idea to collapse undriven constant assignments anyway...
        return false;
      } else if (mSpan.isFloating () || mSubspan.isFloating ()) {
        return false;
      } else if ((flags & eLSB_ONLY) && mSubspan.getLength () > 1) {
        return false;
      } else if (!constant->set (mSubspan.bindRange (*this), test)) {
        // unable to add these bits to the constant value... this is always a bug
        // with the matcher
        return false;
      }
    }
    break;
  case eScalarConstantXZ:
    {
      // the actual constant must match the prototype
      Constant *constant = n [2].getConstant ();
      if (!constant->test (test)) {
        return false;
      }
    }
    break;
  case eScalarConstantSigned:
    {
      // the actual constant must match the prototype
      SInt32 base_value = n [2].getSInt ();
      SInt32 value;
      if (flags & eLSB_ONLY) {
        return false;
      }
      if (!test->getL (&value)) {
        // This can fail if either the constant is symbolic or just will not fit
        // into 32 bits.
        return false;
      } else if (base_value != value) {
        return false;
      }
    }
    break;
  case eScalarConstant:
    {
      // the actual constant must match the prototype
      UInt32 base_value = n [2].getUInt ();
      UInt32 value;
      if (!test->getUL (&value)) {
        // This can fail if either the constant is symbolic or just will not fit
        // into 32 bits.
        return false;
      } else if (base_value != value) {
        return false;
      }
    }
    break;
  case eIndexedConstantSigned:
    {
      // MATCHER_TEST (flags & eCASE, test);
      SInt32 base_value = n [2].getSInt ();
      SInt32 value;
      if (!test->getL (&value)) {
        // This can fail if either the constant will not fit into the 32 bit
        // word, or if it's symbolic.
        return false;
      } else if (mNMatched == 0 && value == base_value) {
        // OK, this is the first expression tested against the prototype, and
        // they have the same value. So, it's not an indexed constant after
        // all. Change the operator to eScalarConstant
        n [0] = INSTRUCTION (eScalarConstantSigned);
      } else if (mCaseBaseIndex - base_value != mCaseIndex - value) {
        // The difference with the case selector is not consistent
        return false;
      }
    }
    break;
  case eIndexedConstant:
    {
      ConstantFunction *function = n [2].getFunction ();
      UInt32 value;
      if (!test->getUL (&value)) {
        // This can fail if either the constant will not fit into the 32 bit
        // word, or if it's symbolic.
        return false;
      } else if (!function->matchNext (mCaseIndex, value)) {
        return false;
      }
    }
    break;
  default:
    MATCHER_TEST (false, test);
    return false;
    break;
  }
  n++;
  return true;
}

void Collapser::Matcher::create (iterator &n, NUConst **result, const UInt32 /*flags*/)
{
  Symbol symbol = n.getSymbol ();
  const NUConst *prototype = dynamic_cast <const NUConst *> (n [1].getExpr ());
  NU_ASSERT (prototype != NULL, mHead);
  switch (symbol) {
  case eFail:
    NU_ASSERT (symbol != eFail, mHead); // apply should have failed
    break;
  case eVectorConstantZ:
    {
      const size_t size = mSpan.getLength ();
      DynBitVector drive (size);
      DynBitVector value (size);
      drive.setRange (0, size, 1);  // all Z
      value.setRange (0, size, 0);  // all 0
      (*result) = NUConst::createXZ (false, value, drive, size, prototype->getLoc ());
    }
    break;
  case eVectorConstant:
    {
      Constant *constant = n[2].getConstant ();
      constant->rewind (mNMatched);
      if (!constant->create (result)) { 
        *result = NULL;
      }
    }
    break;
  case eScalarConstantXZ:
  case eScalarConstantSigned:
  case eScalarConstant:
    {
      CopyContext ctx (NULL, NULL);
      *result = dynamic_cast <NUConst *> (prototype->copy (ctx));
      NU_ASSERT (*result != NULL, prototype);
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    break;
  }
  n++;
}

//
// Helpers for Memsel indices
//

void Collapser::Matcher::build_memsel_index (const NUExpr *index, const UInt32 flags)
{
  const NUConst *const_index;
  UInt32 index_mark = mNextMark++;
  SInt32 value;
  if (!(flags & eCASE) 
    || ((const_index = index->castConst ()) == NULL) 
    || !const_index->getL (&value)) {
    UInt32 syn;
    append (INSTRUCTION (eMemselAddress));
    append (Instruction (index_mark));
    build (index, flags|eSCALAR|eINDEX, &syn);
  } else {
    // Initially we assume that the memory index will vary with the case
    // selector. This assumption gets undone if this index of the first
    // corresponding test item has the same constant value.
    UInt32 is_case_indexed = (flags & eCASE);
    append (INSTRUCTION (eMemselConstAddress));
    append (Instruction (index_mark));
    append (Instruction (is_case_indexed));
    append (Instruction (value));
  }
  // put a marker into the instruction stream to check consistency in the
  // corresponding apply method.
  append (INSTRUCTION (eMark));
  append (Instruction (index_mark));
}

bool Collapser::Matcher::apply_memsel_index (iterator &n,  const NUExpr *index, const UInt32 flags)
{
  const UInt32 index_mark = n [1].getUInt ();
  switch (n.getSymbol ()) {
  case eMemselAddress:
    n++;
    if (!apply (n, index, flags)) {
      return false;
    }
    break;
  case eMemselConstAddress:
    {
      UInt32 is_case_indexed = n [2].getUInt ();
      SInt32 offset = n [3].getSInt ();
      SInt32 value;
      const NUConst *const_index = index->castConst ();
      if (const_index == NULL) {
        // this index is not a constant expression
        return false;
      } else if  (!const_index->getL (&value)) {
        // The constant either does not fit into 32 bits or it's symbolic and
        // we cannot process it.
        return false;
      } else if (mNMatched == 0 && offset == value) {
        // this is the same index as the prototype so change to non-indexed match
        is_case_indexed = 0;
        n [2] = Instruction (is_case_indexed);
        n++;                          // matched against this index
      } else if (is_case_indexed && mCaseBaseIndex - offset == mCaseIndex - value) {
        // same difference with the case selector as the prototype
        n++;                            // matched against this index
      } else if (is_case_indexed) {
        return false;                   // incorrect difference with selector
      } else if (value == offset) {
        n++;                            // same index as prototype
      } else {
        return false;
      }
    }
    break;
  default:
    NU_ASSERT (false, mHead);
    return false;
  }
  // Check that we are at the same mark that was emitted by the corresponding
  // build method. If this assertion fails then there is a bug in either the
  // build method for one of the indices or in the apply method for one of the
  // indices.
  MATCHER_ASSERT (n.getSymbol () == eMark && n[1].getUInt () == index_mark, index);
  n++;
  return true;
}

void Collapser::Matcher::create_memsel_index (iterator &n, const NUExpr *anchor,
  NUExpr **index, const UInt32 flags)
{
  const UInt32 index_mark = n [1].getUInt ();
  switch (n.getSymbol ()) {
  case eMemselAddress:
    n++;
    create (n, index, flags|eSCALAR);
    break;
  case eMemselConstAddress:
    {
      UInt32 is_case_indexed = n [2].getUInt ();
      SInt32 offset = n [3].getSInt ();
      SInt32 index_offset = offset - mCaseBaseIndex;
      if (!is_case_indexed && offset < 0) {
        *index = 
          new NUUnaryOp (NUOp::eUnMinus,
            NUConst::create (false, std::abs (offset), 32, anchor->getLoc ()),
            anchor->getLoc ());
        n++;
      } else if (mNMatched == 0 || (!is_case_indexed && offset >= 0)) {
        *index = NUConst::create (false, std::abs (offset), 32, anchor->getLoc ());
        n++;
      } else if (index_offset == 0) {
        // just build an rvalue that accesses the net
        *index = new NUIdentRvalue (mCaseSelectorId, anchor->getLoc ());
        n++;                          // skip the constant expression
      } else {
        // construct an expression that is the sum of the net and the offset
        NUOp::OpT op = index_offset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus;
        *index = new NUBinaryOp (op, 
          new NUIdentRvalue (mCaseSelectorId, anchor->getLoc ()),
          NUConst::create (false, std::abs (index_offset), 32, anchor->getLoc ()),
          anchor->getLoc ());
        n++;                              // skip the constant expression
      }
    }
    break;
  default:
    NU_ASSERT (false, anchor);
    break;
  }
  // Check that we are at the same mark that was emitted by the corresponding
  // build method. If this assertion fails then there is a bug in either the
  // build method for one of the indices or in the apply method for one of the
  // indices.
  MATCHER_ASSERT (n.getSymbol () == eMark && n [1].getUInt () == index_mark, anchor);
  n++;
}

//
// Helper methods for Collapse
// 

bool Collapser::Matcher::addLhsNet (const NUNet *net)
{
  if (mRHSNets.isMember (net)) {
    return false;
  } else if (mLHSNets.isMember (net)) {
    return false;
  } else {
    mLHSNets.insert (net);
    return true;
  }
}

bool Collapser::Matcher::addRhsNet (const NUNet *net)
{
  if (mLHSNets.isMember (net)) {
    return false;
  } else {
    mRHSNets.insert (net);
    return true;
  }
}

Collapser::Symbol Collapser::Matcher::getFlavour (const NUAssign *assign)
{
  if (dynamic_cast <const NUContAssign*> (assign) != NULL) {
    return eAssignContinuous;
  } else if (dynamic_cast <const NUBlockingAssign*> (assign) != NULL) {
    return eAssignBlocking;
  } else if (dynamic_cast <const NUNonBlockingAssign*> (assign) != NULL) {
    return eAssignNonBlocking;
  } else {
    MATCHER_ASSERT(0, assign);
    return eFail;
  }
}

/*static*/ bool Collapser::Matcher::isInBounds (const NUNet *net, const ConstantRange &index)
{
  const ConstantRange* range;
  if (net->isVectorNet()) {
    const NUVectorNet * vector_net = dynamic_cast<const NUVectorNet*>(net);
    range = vector_net->getRange();
  } else if (net->isMemoryNet()) {
    const NUMemoryNet *mem = net->getMemoryNet();
    range = mem->getWidthRange();
  } else {
    return false;
  }
  return range->contains (index);
}

/*static*/ bool Collapser::Matcher::isInBounds (const NUNet *net, const Span &index)
{
  const ConstantRange* range;
  if (net->isVectorNet()) {
    const NUVectorNet * vector_net = dynamic_cast<const NUVectorNet*>(net);
    range = vector_net->getRange();
  } else if (net->isMemoryNet()) {
    const NUMemoryNet * mem = net->getMemoryNet();
    range = mem->getWidthRange();
  } else {
    return false;
  }
  if (index.isFloating ()) {
    return false;
  } else {
    return range->contains ((ConstantRange &) index);
  }
}

void Collapser::Matcher::extend (const NUStmt *stmt, const Span &extension)
{
  // TODO - review this assertion... this is an interim stage... correc
  NU_ASSERT (mSpan.getFlavour () == extension.getFlavour (), stmt);
  if (!mSpan.isConcrete ()) {
    // Either the span is floating or an entry was made into mSpanBindings when
    // the span was fixated, either way, no entry should be made here.
    NU_ASSERT (mSpan.isFloating () || mSpanBindings.find (mNMatched+1) != mSpanBindings.end (), mHead);
  } else if (mNMatched == 0) {
    // Need the initial entry and the entry for the extension
    NU_ASSERT (mSpanBindings.find (0) == mSpanBindings.end (), mHead);
    NU_ASSERT (mSpanBindings.find (mNMatched+1) == mSpanBindings.end (), mHead);
    mSpanBindings.insert (0, mSpan.getLsb (*this));
    mSpanBindings.insert (mNMatched+1, extension.getLsb (*this));
  } else {
    // Add an entry to the span binding map for the concrete span. This will be
    // need if there are any rhs concats to construct.
    NU_ASSERT (mSpanBindings.find (mNMatched+1) == mSpanBindings.end (), mHead);
    mSpanBindings.insert (mNMatched+1, extension.getLsb (*this));
  }
  if (mNMatched == 0) {
    mFirstMatchedRange = mSpan;
  }
  mLastMatchedRange = extension;
  if (mSpan.isJustBelow (*this, extension)) {
    mSpan.extendUp (extension.getLength ());
  } else if (mSpan.isJustAbove (*this, extension)) {
    mSpan.extendDown (extension.getLength ());
  } else if (extension.isFloating () && mSpan.isFloating ()) {
    // arbitrarily extend it upwards... we really only care about the width
    // when everything is floating.
    mSpan.extendUp (extension.getLength ());
  } else {
    // The spans should be contiguous unless both the span and the extension
    // are floating.
    NU_ASSERT (extension.isFloating () && mSpan.isFloating (), stmt); 
  }
  if (mUseMetrics) {
    NUCostContext context;
    NUCost cost;
    stmt->calcCost (&cost, &context);
    mAggregateCost.addCost (cost);
  }
}

//
// Shamelessly cribbed from Inference.cxx
//

inline
bool Collapser::Matcher::isVectoredSel (const NUOp *op) const 
{
  for (UInt32 i = 0 ; i < op->getNumArgs() ; ++i ) {
    if (isVectoredSel (op->getArg (i))) {
      return true;
    }
  }
  return false;
}

bool Collapser::Matcher::isVectoredSel (const NUExpr *expr) const
{
  const NUOp *op;
  if ((op = dynamic_cast <const NUOp *> (expr)) != NULL) {
    return isVectoredSel (op);
  } else if (expr->getType () != NUExpr::eNUVarselRvalue) {
    return false;
  } else if (dynamic_cast <const NUVarselRvalue *> (expr)->getRange ()->getLength ()
    != mSpan.getLength ()) {
    // There is all sorts of pain if the length of the selector expression does
    // not match the span.
    return false;
  } else {
    // The selector is a vector slice with the same length as the span.
    return true;
  }
}

UInt32 Collapser::Matcher::computeIndexForNet(const NUNet * net, SInt32 offset) const
{
  const ConstantRange * range = NULL;
  if (net->isVectorNet()) {
    const NUVectorNet * vector_net = dynamic_cast<const NUVectorNet*>(net);
    range = vector_net->getRange();
  } else if (net->isMemoryNet()) {
    const NUMemoryNet * memory_net = net->getMemoryNet();
    range = memory_net->getWidthRange();
  }  else {
    // others not expected.
    NU_ASSERT(0, net);
  }
  return range->index(offset);
}


//
// Constant manipulation
//

// Set NOISY_CONSTANT to one for debugging the Constant part of this
// code. Beware, it's quite noisy and probably outputs way too much crap for
// any real problems.

#define NOISY_CONSTANT 0

#ifndef NOISY_CONSTANT
#define NOISY_CONSTANT 0
#endif
Collapser::Constant::Constant (Matcher &matcher, MsgContext *msg_context, 
  const ConstantRange &range, const NUConst *x, bool size_to_range) :
  Embedded (matcher),
  mPrototype (x), mMsgContext (msg_context),
  mValue (), mDrive (), mPrevRange (), mRange (range), mHasXZ (false), mNMatched (0)
{
  MATCHER_ASSERT (range.getMsb () >= range.getLsb (), x); // MUST be normalised
  const NUExpr::Type flavour = x->getType ();
  switch (flavour) {
  case NUExpr::eNUConstNoXZ:
    {
      x->getSignedValue (&mValue);
#if NOISY_CONSTANT
      UtString b1, b2;
      x->compose (&b1, NULL);
      fprintf (stdout, "%d: %s (%d:%d,%s) -> %s\n", x->getLoc ().getLine (), __FUNCTION__, 
        mRange.getMsb (), mRange.getLsb (), b1.c_str (), format (&b2));
#endif
    }
    break;
  case NUExpr::eNUConstXZ:
    {
      x->getValueDrive (&mValue, &mDrive);
      mHasXZ = true;
#if NOISY_CONSTANT
      UtString b1, b2;
      x->compose (&b1, NULL);
      fprintf (stdout, "%d: %s (%d:%d,%s) -> %s\n", x->getLoc ().getLine (), __FUNCTION__, 
        mRange.getLsb (), mRange.getMsb (), b1.c_str (), format (&b2));
#endif
    }
    break;
  default:
    MATCHER_ASSERT (false, x);
    break;
  }
  if (size_to_range) {
    // The constant must be sized to the given range after get value so that
    // the "natural" size of the constant does not override the width of the
    // subspan in a vectorisatin.
    mValue.resize (mRange.getLength ());
    mDrive.resize (mRange.getLength ());
  }
}

bool Collapser::Constant::set (const ConstantRange &range, const NUConst *x)
{
#if NOISY_CONSTANT
  // For debugging stash away a picture of the constant before anything is
  // screwed with...
  UtString b0;
  format (&b0);
#endif
  if (!MATCHER_VERIFY (!mRange.overlaps (range))) {
    // Something has gone wrong with the matcher is there is an overlap in the
    // current range of the constant, and the new slice attaching
    // itself. Assert out in debug mode, but just break the match in production.
    return false;
  }
  MATCHER_TEST (range.getMsb () >= range.getLsb (), x); // MUST be normalised
  // First adjust the range.
  UInt32 offset;
  mPrevRange = mRange;
  if (range.getLsb () < mRange.getLsb ()) {
    // Need to grow downwards...
    UInt32 delta = mRange.getLsb () - range.getLsb ();
    mValue.resize (mRange.getLength () + delta);
    mDrive.resize (mRange.getLength () + delta);
    // ... so the bits need to be shifted up
    mValue <<= delta;
    mDrive <<= delta;
    mRange.setLsb (range.getLsb ());
    offset = 0;                         // add to bottom of bit vector
  } else if (range.getMsb () > mRange.getMsb ()) {
    // Need to grow upwards
    offset = mRange.getLength ();       // add to top of bit vector
    UInt32 delta = range.getMsb () - mRange.getMsb ();
    mValue.resize (mRange.getLength () + delta);
    mDrive.resize (mRange.getLength () + delta);
    mRange.setMsb (range.getMsb ());
  } else {
    NU_ASSERT (false, x);               // then it must be overlapping
    offset = 0;                         // eat the gcc warning
  }
  // Adjust the value
  const NUExpr::Type flavour = x->getType ();
  switch (flavour) {
  case NUExpr::eNUConstNoXZ:
    {
      DynBitVector value;
      x->getSignedValue (&value);
#if NOISY_CONSTANT
      UtString b1, b2, b3;
      x->compose (&b3, NULL);
      value.format (&b1, eCarbonBin);
#endif
      value.resize (mRange.getLength ());
      value <<= offset;
      mValue |= value;
#if NOISY_CONSTANT
      fprintf (stdout, "%d: %s %s (%d:%d,%s=%s) -> %s\n", x->getLoc ().getLine (), __FUNCTION__, 
        b0.c_str (), range.getMsb (), range.getLsb (), b3.c_str (), b1.c_str (), format (&b2));
#endif
    }
    break;
  case NUExpr::eNUConstXZ:
    {
#if NOISY_CONSTANT
      UtString b0;
      format (&b0);
#endif
      DynBitVector value, drive;
      mHasXZ = true;
      x->getValueDrive (&value, &drive);
      value.resize (mRange.getLength ());
      drive.resize (mRange.getLength ());
      value <<= offset;
      drive <<= offset;
      mValue |= value;
      mDrive |= drive;
#if NOISY_CONSTANT
      UtString b1, b2, b3, b4;
      x->compose (&b4, NULL);
      value.format (&b1, eCarbonBin);
      drive.format (&b1, eCarbonBin);
      fprintf (stdout, "%d: %s %s (%d:%d,%s=%s#%s) -> %s\n", x->getLoc ().getLine (), __FUNCTION__, 
        b0.c_str (), range.getMsb (), range.getLsb (), b4.c_str (), b1.c_str (), b2.c_str (), 
        format (&b3));
#endif
    }
    break;
  default:
    MATCHER_TEST (flavour == NUExpr::eNUConstXZ || flavour == NUExpr::eNUConstNoXZ, x);
    return false;
    break;
  }
  mNMatched++;
  return true;
}

bool Collapser::Constant::test (const NUConst *x) const
{
#if NOISY_CONSTANT
  UtString b1, b2;
  x->compose (&b2, NULL);
  fprintf (stdout, "%d: %s (%s): %s", mPrototype->getLoc ().getLine (), __FUNCTION__, b2.c_str (), format (&b1));
#endif
  bool result = false;
  const NUExpr::Type flavour = x->getType ();
  switch (flavour) {
  case NUExpr::eNUConstNoXZ:
    if (mHasXZ && mDrive != 0) {
      result = false;                   // this contains X or Z
    } else {
      DynBitVector value;
      x->getSignedValue (&value);
      result = (value == mValue);
    }
    break;
  case NUExpr::eNUConstXZ:
    {
      DynBitVector value, drive;
      x->getValueDrive (&value, &drive);
      result = (mValue == value && mDrive == drive);
    }
    break;
  default:
    MATCHER_TEST (flavour == NUExpr::eNUConstXZ || flavour == NUExpr::eNUConstNoXZ, x);
    result = false;
    break;
  }
#if NOISY_CONSTANT
  fprintf (stdout, " -> %s\n", result ? "true" : "false");
#endif
  return result;
}

bool Collapser::Constant::create (NUConst **result) const
  // post: result is an NUConst instance representing this constant
  // post: (*result)->getLoc () == mAnchor
  // returns: iff the instance was successfully created
{
#if NOISY_CONSTANT
  UtString b1;
  fprintf (stdout, "%d: %s: %s\n", mPrototype->getLoc ().getLine (), __FUNCTION__, format (&b1));
#endif
  if (mHasXZ) {
    *result = NUConst::createXZ (false, mValue, mDrive, mRange.getLength (), mPrototype->getLoc ());
  } else {
    *result = NUConst::create (false, mValue, mRange.getLength (), mPrototype->getLoc ());
  }
  return true;
}

const char *Collapser::Constant::format (UtString *buffer) const
{
  *buffer << "(" << mRange.getMsb () << ":" << mRange.getLsb () << ",";
  mValue.format (buffer, eCarbonBin);
  if (mHasXZ) {
    *buffer << "#";
    mDrive.format (buffer, eCarbonBin);
  }
  *buffer << ")";
  return buffer->c_str ();
}

void Collapser::Constant::pr () const
{
  UtString buffer;
  UtIO::cout () << format (&buffer) << "\n";
}

void Collapser::Constant::pr2 () const
{
  NUConst *constant;
  if (!create (&constant)) {
    UtIO::cout () << "create failed\n";
  } else if (constant == NULL) {
    UtIO::cout () << "create returned a NULL pointer\n";
  } else {
    constant->pr ();
    delete constant;
  }
}

void Collapser::Constant::rewind (const UInt32 n_matched)
{
  if (n_matched == mNMatched) {
    return;                             // nothing to rewind
  }
#if NOISY_CONSTANT
  UtString b0;
  mValue.format (&b0, eCarbonBin);
#endif
  // can only rewind to the state before the last match
  NU_ASSERT (mNMatched == n_matched + 1, mPrototype);
  if (mRange.getLsb () < mPrevRange.getLsb ()) {
    // need to trim from the low side
    UInt32 delta = mPrevRange.getLsb () - mRange.getLsb ();
    mValue >>= delta;
    mDrive >>= delta;
    mValue.resize (mPrevRange.getLength ());
    mDrive.resize (mPrevRange.getLength ());
#if NOISY_CONSTANT
    UtString b1;
    mValue.format (&b1, eCarbonBin);
    fprintf (stdout, "%d: %s: (%d:%d,%s) -> (%d,%d,%s)\n",
      mPrototype->getLoc ().getLine (), __FUNCTION__,
      mRange.getMsb (), mRange.getLsb (), b0.c_str (),
      mPrevRange.getMsb (), mPrevRange.getLsb (), b1.c_str ());
#endif
    mRange = mPrevRange;
  } else if (mRange.getMsb () > mPrevRange.getMsb ()) {
    // need to trim from the high side
    mValue.resize (mPrevRange.getLength ());
    mDrive.resize (mPrevRange.getLength ());
#if NOISY_CONSTANT
    UtString b1;
    mValue.format (&b1, eCarbonBin);
    fprintf (stdout, "%d: %s: (%d:%d,%s) -> (%d,%d,%s)\n",
      mPrototype->getLoc ().getLine (), __FUNCTION__,
      mRange.getMsb (), mRange.getLsb (), b0.c_str (),
      mPrevRange.getMsb (), mPrevRange.getLsb (), b1.c_str ());
#endif
    mRange = mPrevRange;
  } else {
    // this is broken... the match *has* to extend at least one end of the
    // constant
    NU_ASSERT (mRange.getLsb () != mPrevRange.getLsb ()
      || mRange.getMsb () != mPrevRange.getMsb (), mPrototype);
  }
}

//
// Collapser::Matcher::Span
//

Collapser::Matcher::Span::Span () : 
  ConstantRange (), mFlavour (cBOGUS), mLvalue (NULL), mDirection (eUnknownDirection), mAnchor (NULL),
  mIndex (-1)
{}

Collapser::Matcher::Span::Span (const ConstantRange &range) : 
  ConstantRange (range), mFlavour (cCONCRETE), mLvalue (NULL), mDirection (eUnknownDirection),
  mAnchor (NULL), mIndex (-1)
{}

Collapser::Matcher::Span::Span (const NULvalue *lvalue,
  const SInt32 lsb, const SInt32 msb, const Flavour flavour, const SInt32 index) : 
  ConstantRange (lsb, msb), mFlavour (flavour), mLvalue (lvalue), mDirection (eUnknownDirection), 
  mAnchor (NULL), mIndex (index)
{
  NU_ASSERT (mIndex >= 0 || flavour == cCONCRETE, lvalue);
}

Collapser::Matcher::FloatingSpan::FloatingSpan (/* Matcher &matcher, */ const NULvalue *lvalue, 
  const UInt32 width, const SInt32 index) :
  //  Span (matcher.mNextAbstractLsb + width - 1, matcher.mNextAbstractLsb, cFLOATING)
// DO NOT COMMIT - cleanup
  Span (lvalue, width - 1, 0, cFLOATING, index)
{
  // matcher.mNextAbstractLsb += width;
}

SInt32 Collapser::Matcher::Span::getLsb (const Matcher &matcher) const
{
  switch (mFlavour) {
  case cBOGUS:
    NU_ASSERT (mFlavour != cBOGUS, matcher.getHead ());
    return -1;
    break;
  case cCONCRETE:
    return ConstantRange::getLsb ();
    break;
  case cFLOATING:
    NU_ASSERT (mFlavour != cFLOATING, matcher.getHead ());
    return -1;
    break;
  case cFIXED:
    {
      SInt32 offset=0;
      if (!matcher.getFixationOffset (mIndex, &offset)) {
        NU_ASSERT (matcher.getFixationOffset (mIndex, &offset), matcher.getHead ());
      }
      return ConstantRange::getLsb () + offset;
    }
    break;
  default:
    NU_ASSERT (false, matcher.getHead ());
    return -1;
    break;
  }
}

SInt32 Collapser::Matcher::Span::getMsb (const Matcher &matcher) const
{
  switch (mFlavour) {
  case cBOGUS:
    NU_ASSERT (mFlavour != cBOGUS, matcher.getHead ());
    return -1;
    break;
  case cCONCRETE:
    return ConstantRange::getMsb ();
    break;
  case cFLOATING:
    NU_ASSERT (mFlavour != cFLOATING, matcher.getHead ());
    return -1;
    break;
  case cFIXED:
    {
      SInt32 offset=0;
      if (!matcher.getFixationOffset (mIndex, &offset)) {
        NU_ASSERT (matcher.getFixationOffset (mIndex, &offset), matcher.getHead ());
      }
      return ConstantRange::getMsb () + offset;
    }
    break;
  default:
    NU_ASSERT (false, matcher.getHead ());
    return -1;
    break;
  }
}

// operations
      
Collapser::Matcher::Span &Collapser::Matcher::Span::operator = (const ConstantRange &range)
{ 
  ConstantRange::operator = (range);
  mLvalue = NULL;
  mDirection = eUnknownDirection;
  mFlavour = cCONCRETE;
  mAnchor = false;
  mIndex = -1;                          // index is uninteresting for concrete spans
  return *this;
}

Collapser::Matcher::Span &Collapser::Matcher::Span::operator = (const Span &x)
{ 
  ConstantRange::operator = (x);
  mLvalue = x.mLvalue;
  mDirection = x.mDirection;
  mFlavour = x.mFlavour;
  mAnchor = x.mAnchor;
  mIndex = x.mIndex;
  return *this;
}

void Collapser::Matcher::Span::extendDown (const UInt32 delta)
{ 
  setLsb (ConstantRange::getLsb () - delta); 
}

void Collapser::Matcher::Span::extendUp (const UInt32 delta)
{ 
  setMsb (ConstantRange::getMsb () + delta); 
}

void Collapser::Matcher::Span::bind (const Direction x, const NUBase *context) 
{ 
  switch (mDirection) {
  case eExpanding:
    // We are allowing extension at either end of the span
    NU_ASSERT (x == eIncreasing || x == eDecreasing, context);
    break;
  case eIncreasing:
    NU_ASSERT (x == eIncreasing, context);
    break;
  case eDecreasing:
    NU_ASSERT (x == eDecreasing, context);
    break;
  case eUnknownDirection:
    mDirection = x;
    break;
  default:
    NU_ASSERT (false, context);
    break;
  }
}

void Collapser::Matcher::Span::fixate (const SInt32, const NUBase *context)
{
  NU_ASSERT (mFlavour == cFLOATING, context);
  mFlavour = cFIXED;
  mAnchor = context;
}

bool Collapser::Matcher::fixate (Span *span, const SInt32 lsb, const NULvalue *lvalue, 
  const NUBase *anchor)
{
  NU_ASSERT (mLvalueConcat != NULL, mHead);
  // check that there is no overlap
  UInt32 i;
  UInt32 width = span->getLength ();
  for (i = 0; i < width; i++) {
    // check that this concrete bit is not already mapped to an abstract bit
    if (mConcreteToAbstract.find (lsb + i) != mConcreteToAbstract.end ()) {
      return false;
    }
    // OK, this is the first attempt to bind to this bit location in this
    // matcher. So install into the concrete-to-abstract map.
    mConcreteToAbstract.insert (lsb + i, span->getIndex ());
  }
  // insert the lsb into the offset table
  mSpanBindings.insert (span->getIndex (), lsb);
  mLvalueConcat->fixate (lvalue, lsb);
  span->fixate (lsb, anchor);
  return true;
}

const char *Collapser::Matcher::Span::image (char *buffer, const int size) const
{
  int n = 0;
  switch (mFlavour) {
  case cBOGUS:
    n += snprintf (buffer + n, size - n, "<>");
    break;
  case cCONCRETE:
    n += snprintf (buffer + n, size - n, "[%d:%d]", ConstantRange::getMsb (), ConstantRange::getLsb ());
    break;
  case cFLOATING:
    n += snprintf (buffer + n, size - n, "<%d>", getLength ());
    break;
  case cFIXED:
    n += snprintf (buffer + n, size - n, "<%d:%d>", ConstantRange::getMsb (), ConstantRange::getLsb ());
    break;
  default:
    n += snprintf (buffer + n, size - n, "###error###");
    break;
  }
  switch (mDirection) {
  case eIncreasing:
    n += snprintf (buffer + n, size - n, " (inc)");
    break;
  case eDecreasing:
    n += snprintf (buffer + n, size - n, " (dec)");
    break;
  case eUnknownDirection:
    break;
  default:
    n += snprintf (buffer + n, size - n, "<Direction##error##>");
    break;
  }
  return buffer;
}

void Collapser::Matcher::Span::pr () const
{
  char buffer [32];
  UtIO::cout () << image (buffer, sizeof (buffer)) << "\n";
}

//
// Debugging
//

void Collapser::Matcher::NUNetSet::pr () const
{
  int n = 0;
  UtIO::cout () << "{";
  const_iterator it = begin ();
  while (it != end ()) {
    if (n++ == 0) {
      (*it)->print (false, 0);
    } else {
      UtIO::cout () << ",\n";
      (*it)->print (false, 2);
    }
    it++;
  }
  UtIO::cout () << "}\n";
}

void Collapser::pr () const
{
  mMatcher.pr ();
}

void Collapser::print (bool deep, int indent) const
{
  mMatcher.print (deep, indent);
}

void Collapser::Matcher::pr () const
{
  print (false, 0);
}

void Collapser::Matcher::IntegerMap::print (UtOStream &s, int indent, int width) const
{
  UtString indent_buffer (indent, ' ');
  s << "{";
  int column = indent + 1;
  const_iterator it = begin ();
  while (it != end ()) {
    if (it != begin ()) {
      column += 2;
      s << ", ";
    }
    char buffer [256];
    int n = snprintf (buffer, sizeof (buffer), "<%d:%d>", it->first, it->second);
    if (column + n >= width) {
      s << UtIO::endl << indent_buffer;
      column = indent;
    }
    s << buffer;
    it++;
  }
  s << "}";
}

void Collapser::Matcher::IntegerMap::pr () const
{
  print (UtIO::cout ());
  UtIO::cout () << UtIO::endl;
}

void Collapser::Matcher::print (bool deep, int indent) const
{
  char b0 [32], b1 [32];
  UtIO::cout () << "mSpan =    " << mSpan.image (b0, sizeof (b0)) << "\n";
  UtIO::cout () << "mSubspan = " << mSubspan.image (b1, sizeof (b1)) << "\n";
  UtIO::cout () << "mConcreteToAbstract = ";
  mConcreteToAbstract.print (UtIO::cout (), 2);
  UtIO::cout () << UtIO::endl;
  UtIO::cout () << "mSpanBindings = ";
  mSpanBindings.print (UtIO::cout (), 2);
  UtIO::cout () << UtIO::endl;
  iterator it (this);
  print (it, deep, indent);
}

void Collapser::Matcher::print (iterator n, bool deep, int indent) const
{
  iterator it (n);
  unsigned int n_instructions = 0;
  while (!it.atEnd ()) {
    UtIO::cout () << UtIO::setw (4) << n_instructions << ": ";
    it->print (deep, indent);
    n_instructions += it->getWidth ();
    for (int i = 1; i < it->getWidth (); i++) {
      if (it.isInBounds (i)) {
        it [i].print (deep, indent + 6);
      } else {
        UtIO::cout () << "*** missing argument " << i << "***\n";
      }
    }
    it++;
  }
}

//! produce a human readable identifier for matcher instructions
/*static*/ const char *Collapser::Instruction::cvt (const Symbol x)
{
  switch (x) {
#define INSTRUCTION_SYMBOL(_tag_, _nargs_) case e##_tag_: return "e" #_tag_;
#define INSTRUCTION_ARG(_tag_, _type_) case e##_tag_##Arg: return "e" #_tag_ "Arg";
#include "vectorise/Collapser.def"
  default:
    return "Collapser::Symbol###error###";
  }
}


void Collapser::Instruction::print (bool deep, int indent)
{
  UtString indent_buffer (indent, ' ');
#if TRACK_INSTRUCTION_LINE
  char buffer [32];
  sprintf (buffer, "%4d: ", mArg.mLineno);
  indent_buffer += buffer;
#endif
  switch (mSymbol) {
#define INSTRUCTION_SYMBOL(_tag_, _nargs_) case e##_tag_: \
    UtIO::cout () << indent_buffer << "e" #_tag_  "\n"; \
    break;
#define INSTRUCTION_ARG(_tag_, _type_) case e##_tag_##Arg: \
    print_##_tag_ (deep, indent); \
    break;
#include "vectorise/Collapser.def"
  default:
    UtIO::cout () << indent_buffer << "Instruction##error##\n";
    break;
  }
}

void Collapser::Instruction::print_Prototype (bool deep, int indent) const
{
  if (mSymbol != ePrototypeArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else if (mArg.mPrototype == NULL) {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "prototype = NULL";
  } else {
    mArg.mPrototype->print (deep, indent);
  }
}

void Collapser::Instruction::print_Assign (bool deep, int indent) const
{
  if (mSymbol != eAssignArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else if (mArg.mAssign == NULL) {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "assign = NULL";
  } else {
    mArg.mAssign->print (deep, indent);
  }
}

void Collapser::LvalueConcat::pr (UtOStream &s, bool deep, int indent) const
{
  UtString indent_buffer (indent, ' ');
  fixed_const_iterator it0 = fixed_begin ();
  UInt32 n = 0;
  s << indent_buffer << "{";
  while (it0 != fixed_end ()) {
    if (n > 0) {
      s << UtIO::endl << indent_buffer << " ";
    }
    UInt32 lsb = it0->first;
    Segment segment (it0->second);
    const UInt32 width = segment.mLvalue->getBitSize ();
    s << "[" << (lsb + width - 1) << ":" << lsb << "]" << UtIO::endl;
    segment.mLvalue->print (false, indent + 1);
    it0++;
    n++;
  }
  floating_const_iterator it1 = floating_begin ();
  while (it1 != floating_end ()) {
    if (n > 0) {
      s << UtIO::endl << indent_buffer << " ";
    }
    Segment segment (it1->second);
    s << "<<" << segment.mWidth << ">>" << UtIO::endl;
    segment.mLvalue->print (deep, indent + 1);
    it1++;
    n++;
  }
  if (n > 0) {
    s << indent_buffer << "}" << UtIO::endl;
  } else {
    s << "}" << UtIO::endl;
  }
}

void Collapser::LvalueConcat::pr () const
{
  pr (UtIO::cout (), false, 0);
}

void Collapser::Instruction::print_LvalueConcat (bool /*deep*/, int indent) const
{
  if (mSymbol != eLvalueConcatArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else {
    mArg.mLvalueConcat->pr (UtIO::cout (), false, indent);
  }
}

void Collapser::RvalueConcat::pr (UtOStream &s, bool deep, int indent)
{
  UtString indent_buffer (indent, ' ');
  ConcatMap::iterator it = mMap.begin ();
  UInt32 n = 0;
  s << indent_buffer << "{" << UtIO::endl;
  while (it != mMap.end ()) {
    if (n > 0) {
      s << "," << UtIO::endl;
    }
    UInt32 index = it->first;
    struct Segment segment = it->second;
    s << indent_buffer << index << " =>\n";
    segment.mExpr->print (deep, indent + 2);
    it++;
    n++;
  }
  if (n > 0) {
    s << indent_buffer << "}" << UtIO::endl;
  } else {
    s << "}" << UtIO::endl;
  }
}

void Collapser::RvalueConcat::pr ()
{
  pr (UtIO::cout ());
}

void Collapser::Instruction::print_RvalueConcat (bool /*deep*/, int indent) const
{
  if (mSymbol != eRvalueConcatArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "<concat with " << mArg.mRvalueConcat->mMap.size () << " entries ";
    mArg.mRvalueConcat->pr (UtIO::cout (), true, indent);
  }
}

void Collapser::ConstantFunction::print (UtOStream &s, const bool, const UInt32 indent) const
{
  UtString indent_buffer (indent, ' ');
  s << indent_buffer;
  if (mFlags == 0) {
    s << "[NO FUNCTION]" << UtIO::endl;
    return;
  }
  bool need_space = false;
  if (mFlags & cIS_AFFINE) {
    s << "[AFFINE * " << mAffine.mMultiplier << " + " << mAffine.mOffset << "]";
    need_space = true;
  } 
  if (mFlags & cIS_POWER) {
    if (need_space) {
      s << " ";
    }
    s << "[POWER +" << mPower.mOffset << "]";
    need_space = true;
  }
  s << UtIO::endl;
}

void Collapser::Instruction::print_Function (bool deep, int indent) const
{
  if (mSymbol != eFunctionArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else if (mArg.mFunction == NULL) {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "function = NULL";
  } else {
    mArg.mFunction->print (UtIO::cout (), deep, indent);
  }
}

void Collapser::Instruction::print_CaseItem (bool deep, int indent) const
{
  if (mSymbol != eCaseItemArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else if (mArg.mCaseItem == NULL) {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "case_item = NULL";
  } else {
    mArg.mCaseItem->print (deep, indent);
  }
}

void Collapser::Instruction::print_Expr (bool deep, int indent) const
{
  if (mSymbol != eExprArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else if (mArg.mExpr == NULL) {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "expr=NULL";
  } else {
    mArg.mExpr->print (deep, indent);
  }
}

void Collapser::Instruction::print_Net (bool deep, int indent) const
{
  if (mSymbol != eNetArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else if (mArg.mNet == NULL) {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "net=NULL";
  } else {
    mArg.mNet->print (deep, 6);
  }
  UtIO::cout () << "\n";
}

void Collapser::Instruction::print_Constant (bool, int indent) const
{
  if (mSymbol != eConstantArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else {
    UtString indent_buffer (indent, ' ');
    UtString buffer;
    UtIO::cout () << indent_buffer << mArg.mConstant->format (&buffer) << "\n";
  }
}

void Collapser::Instruction::print_SInt (bool, int indent) const
{
  if (mSymbol != eSIntArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "sint = " << mArg.mSInt << "\n";
  }
}

void Collapser::Instruction::print_UInt (bool, int indent) const
{
  if (mSymbol != eUIntArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "uint = " << mArg.mUInt << "\n";
  }
}

void Collapser::Instruction::print_Op (bool, int indent) const
{
  if (mSymbol != eOpArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "op = " << mArg.mOp << "\n";
  }
}

void Collapser::Instruction::print_Strength (bool, int indent) const
{
  if (mSymbol != eStrengthArg) {
    UtIO::cout () << "*** missing argument ***\n";
  } else {
    UtString indent_buffer (indent, ' ');
    UtIO::cout () << indent_buffer << "strength = ";
    switch (mArg.mStrength) {
    case eStrPull:  UtIO::cout () << "eStrPull\n";  break;
    case eStrDrive: UtIO::cout () << "eStrDrive\n"; break;
    case eStrTie:   UtIO::cout () << "eStrTie\n";   break;
    default: UtIO::cout () << "### error ###\n";    break;
    }
  }
}

//! Output the complexity of the replacement and the aggregate
void Collapser::Matcher::dumpComplexity (UtOStream &s, const NUAssign *replacement) const
{
  float replacement_cost, aggregate_cost;
  if (!mUseMetrics) {
    // complexity is not computed
    // s << "No complexity information available." << UtIO::endl;
  } else if (replacement == NULL) {
    s << "Aggregate complexity: " << mAggregateCost.complexity () << UtIO::endl;
  } else if (allowCollapse (replacement, &aggregate_cost, &replacement_cost)) {
    s << "Aggregate complexity: " << aggregate_cost
      << ", Replacement complexity: " << replacement_cost
      << " (allowed)"
      << UtIO::endl;
  } else {
    s << "Aggregate complexity: " << aggregate_cost
      << ", Replacement complexity: " << replacement_cost
      << " (disallowed)"
      << UtIO::endl;
  }
}

bool Collapser::LvalueConcat::fixate (const NULvalue *lvalue, SInt32 lsb)
{
  // check that the lvalue is associated with a floating segment
  floating_iterator floating;
  if ((floating = mFloatingSegments.find (lvalue)) == mFloatingSegments.end ()) {
    MATCHER_ASSERT (mFloatingSegments.find (lvalue) != mFloatingSegments.end (), mPrototype);
    return false;
  }
  // now check that there is no overlap
  UInt32 width = floating->second.mWidth;
  SInt32 n = lsb;
  SInt32 msb = lsb + width;             // ok, so it's really msb+1
  while (n < msb) {
    if (mBound.find (n) == mBound.end ()) {
      mBound.insert (n);
    } else {
      // there is an overlap
      return false;
    }
    n++;
  }
  mFloatingSegments.erase (floating);
  mFixedSegments [lsb] = Segment (lsb, width, lvalue);
  return true;
}

bool Collapser::LvalueConcat::resolve (bool *floating)
{
  if (mFloatingSegments.size () == 0 && mFixedSegments.size () == 0) {
    // it's empty...
    return false;
  } else if (mFloatingSegments.size () == 0 && mFixedSegments.size () > 0) {
    // all the floating segments were anchored
    *floating = false;
    return true;
  } else if (mFloatingSegments.size () == 1 && mFixedSegments.size () > 0) {
    // the last item passed to the matcher may have failed on the rhs match
    // after the segment was added to the concat... that's OK...
    *floating = false;
    return true;
  } else if (mFloatingSegments.size () > 0 && mFixedSegments.size () > 0) {
    // There are unanchored floating segments as well as anchored floating
    // segments after successfull rhs matches. This should not happen! If a
    // segment is bound, then any subsequent matches should also bind segments.
    return false;
  } else if (mFloatingSegments.size () > 0 && mFixedSegments.size () == 0) {
    // all the segments are floating so we can arbitrarily define an ordering
    *floating = true;
    UInt32 lsb = 0;
    floating_iterator it = floating_begin ();
    while (it != floating_end ()) {
      mFixedSegments [lsb] = Segment (lsb, it->second.mWidth, it->second.mLvalue);
      lsb += it->second.mWidth;
      it++;
    }
    return true;
  } else {
    // conditions should be exhaustive
    NU_ASSERT (false, mPrototype);      // bang!
    return false;
  }
}

void Collapser::LvalueConcat::acrete (const NULvalue *lvalue, UInt32 width)
{
  mLvalue = lvalue;
  mFloatingSegments [lvalue] = Segment (0, width, lvalue);
}

// ConcatAnalysis requires the extraction of adjacency information from the
// collapser. A pair of nets is considered adjacent if there is a concat,
// either left-side or right-side, in which the nets are adjacent. The
// collapser can construct concats from nets appearing on either side of an
// assignment. The extract methods examine the vectorisation plan in the
// matcher and insert edges between adjacent nodes in a ConcatAnalysis::NetMap
// graph.


//! Top-level collapser extract method
bool Collapser::extract (ConcatAnalysis &context, ConcatAnalysis::NetMap *netmap)
{
  return mMatcher.extract (context, netmap);
}

template <> bool Collapser::Matcher::extract <NUConcatOp> (iterator &n, ConcatAnalysis &, ConcatAnalysis::NetMap *);
template <> bool Collapser::Matcher::extract <NUConcatLvalue> (iterator &n, ConcatAnalysis &, ConcatAnalysis::NetMap *);

//! Extract edges between concat-adjacent nets in a matcher plan.
bool Collapser::Matcher::extract (ConcatAnalysis &context, ConcatAnalysis::NetMap *netmap)
{
  bool changed = false;
  iterator n (this);
  while (!n.atEnd ()) {
    const Symbol symbol = n.getSymbol ();
    switch (symbol) {
    case eRvalueConcat:
      changed |= extract <NUConcatOp> (n, context, netmap);
      break;
    case eLvalueConcat:
      changed |= extract <NUConcatLvalue> (n, context, netmap);
      break;
    case eAssignProxy:
      {
        // There should be no proxies because extract can only be called when
        // at least one statement has matched.
        const NUAssign *assign = n[1].getAssign ();
        NU_ASSERT (mNMatched > 0, assign);
        NU_ASSERT (symbol != eAssignProxy, assign);
      }
      break;
    case eExprProxy:
      {
        // There should be no proxies because extract can only be called when
        // at least one statement has matched.
        const NUExpr *expr = n[1].getExpr ();
        NU_ASSERT (mNMatched > 0, expr);
        NU_ASSERT (symbol != eExprProxy, expr);
      }
      break;
    case eFail:
      return false;
      break;
    default:
      n++;
      break;
    }
  }
  return changed;
}

//! Extract of concat-adjacency information for a right-side concat.
/*! The matcher has matched against a plan containing an eRvalueConcat match
 *  operator. This vectorised into a NUConcatOp. This method creates edges in
 *  netmap for the qualified nets that would be adjacent in the vectorised
 *  statement.
 */

template <> bool Collapser::Matcher::extract <NUConcatOp> (iterator &n, ConcatAnalysis &context, ConcatAnalysis::NetMap *netmap)
{
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eRvalueConcat:
    {
      bool changed = false;
      RvalueConcat *pool = n [2].getRvalueConcat ();
      n++;
      // construct a map from the lsb of the component to the component
      UtMap <SInt32, const NUExpr *> elements;
      for (RvalueConcat::ConcatMap::iterator it = pool->mMap.begin (); it != pool->mMap.end (); it++) {
        const RvalueConcat::Segment segment = it->second;
        if (segment.mIndex > mNMatched) {
          continue;
          // this operand of the rhs was matched but the match failed later in the expression.
        }
        SInt32 lsb;
        if (!mSpanBindings.get (segment.mIndex, &lsb)) {
          // Cannot identify the bit position of this concat component
          NU_ASSERT (mSpanBindings.get (segment.mIndex, &lsb), segment.mExpr);
        } else {
          elements [lsb] = segment.mExpr;
        }
      }
      // now iterate across the map from lsb to msb create edges as we go
      NUNet *previous = NULL;
      for (UtMap <SInt32, const NUExpr *>::iterator it = elements.begin (); it != elements.end (); it++) {
        const NUExpr *expr = it->second;
        NUNet *net = NULL;
        ConcatAnalysis::NetMap::Edge *new_edge;
        if (!expr->isWholeIdentifier ()) {
          // not a scalar
          previous = NULL;
        } else if ((net = expr->getWholeIdentifier ()) == NULL) {
          // wierd... isWholeIdentifier implies that getWholeIdentifier should
          // work...
          NU_ASSERT (expr->getWholeIdentifier () != NULL, expr);
        } else if (!context.isInteresting (net)) {
          previous = NULL;              // an uninteresting net
        } else if (previous == NULL) {
          previous = net;               // not combinable with a previous component
        } else if (netmap->maybeAddEdge (previous, net, &new_edge)) {
          changed = true;
          previous = net;
          context.note_vector_edge (new_edge);
        } else {
          previous = net;
        }
      }
      return changed;
    }
    break;
  case eFail:
    return false;
    break;
  default:
    NU_ASSERT (false, context.getModule ());
    return false;
    break;
  }
}

//! Extract of concat-adjacency information for a left-side concat.
/*! The matcher has matched against a plan containing an eLvalueConcat match
 *  operator. This vectorised into a NUConcatLvalue. This method creates edges
 *  in netmap for the qualified nets that would be adjacent in the vectorised
 *  statement.
 */

template <> bool Collapser::Matcher::extract <NUConcatLvalue> (iterator &n, ConcatAnalysis &context, 
  ConcatAnalysis::NetMap *netmap)
{
  Symbol symbol = n.getSymbol ();
  switch (symbol) {
  case eLvalueConcat:
    {
      bool changed = false;
      LvalueConcat *concat = n [2].getLvalueConcat ();
      n++;
      bool is_abstract;
      if (!mLvalueConcat->resolve (&is_abstract) || is_abstract) {
        return false;
      }
      // construct a vector of copies of the lvalues in the concat
      NUNet *previous = NULL;
      UInt32 n = 0;
      LvalueConcat::fixed_iterator it;
      for (it = concat->fixed_begin (); it != concat->fixed_end () && n++ <= mNMatched; it++) {
        const NULvalue *lvalue = it->second.mLvalue;
        NUNet *net = NULL;
        ConcatAnalysis::NetMap::Edge *new_edge;
        if (!lvalue->isWholeIdentifier ()) {
          // not a scalar
          previous = NULL;
        } else if ((net = lvalue->getWholeIdentifier ()) == NULL) {
          // it is a whole identifier so getWholeIdentifier should give me a net
          NU_ASSERT (lvalue->getWholeIdentifier () != NULL, lvalue);
        } else if (!context.isInteresting (net)) {
          previous = NULL;              // not an interesting concat component
        } else if (previous == NULL) {
          previous = net;               // not combinable with the previous
        } else if (netmap->maybeAddEdge (previous, net, &new_edge)) {
          changed = true;
          previous = net;
          context.note_vector_edge (new_edge);
        } else {
          previous = net;
        }
      }
      return changed;
    }
    break;
  case eFail:
    return false;
    break;
  default:
    NU_ASSERT (false, context.getModule ());
    return false;
    break;
  }
}

Collapser::ConstantFunction::ConstantFunction (Matcher &matcher, const NUExpr *prototype,
                                               const UInt32 width, const UInt32 base_index, 
                                               const UInt32 base_value) :
  Embedded (matcher),
  mPrototype (prototype), mWidth (width), mFlags (0), mNMatched (0), 
  mBaseIndex (base_index), mBaseValue (base_value),
  mPower (mPrototype, base_index, base_value),
  mAffine (mPrototype)
{
  mFlags |= cIS_AFFINE;                 // trivially affine but will not know constants yet
  mFlags |= cIS_POWER;                  // trivially a power
}

bool Collapser::ConstantFunction::matchNext (const UInt32 index, const UInt32 value)
{
  UInt32 matched = 0;
  // check whether it still matches the power function
  if (!(mFlags & cIS_POWER)) {
    // no longer matching the power function
  } else if (mPower.next (mNMatched, index, value)) {
    matched |= cIS_POWER;
  }
  // check whether it conforms to an affine transformation
  if (!(mFlags & cIS_AFFINE)) {
    // no longer matching the affine function
  } else if (mAffine.next (mNMatched, mBaseIndex, mBaseValue, index, value)) {
    matched |= cIS_AFFINE;
  }
  if (matched != 0) {
    mFlags = matched;
    mNMatched++;
    return true;
  } else {
    return false;
  }
}

bool Collapser::ConstantFunction::Affine::next (const UInt32 n_matched, 
  const UInt32 base_index, const UInt32 base_value, const UInt32 index, const UInt32 value)
{
  if (n_matched > 0) {
    // The coefficients have been computed so check that the current index and
    // value match the function.
    return (value == index * mMultiplier + mOffset);
  } else if ((value & 0x80000000) || (index & 0x80000000)) {
    // The top bit is set in the unsigned value so it will not cast to int for
    // computing the multiplier or offset, so just fail the match.
    return false;
  } else {
    // This is the first match attempt so compute the coefficients of the
    // affine mapping.
    NU_ASSERT (index != base_index, mPrototype);
    // Something bad has happened if we are matching the same index more than
    // once. If this assert fails, then check that the same case item is not
    // getting matched multiple times.
    mMultiplier = ((int) base_value - (int) value) / ((int) base_index - (int) index);
    mOffset = ((int) (index * base_value) - (int) (base_index * value)) / ((int) index - (int) base_index);
    // The casts to int are required otherwise both numerator and denominator
    // may be treated as unsigned before the division. This is bad if either
    // needs to be negative.
    return (mMultiplier >= 0);
    // There are issues with negative multipliers that I have not tracked down adequately.
  }
}

bool Collapser::ConstantFunction::Power::next (const UInt32 n_matched, 
  const UInt32 index, const UInt32 value)
{
  if (n_matched > 31) {
    return false;                       // too big
  } else {
    return (value == (UInt32) ((1<<index) + mOffset));
  }
}

bool Collapser::ConstantFunction::Affine::create (const UInt32 width, NUNet *net, NUExpr **expr) const
{
  const SourceLocator &loc = mPrototype->getLoc ();
  NUExpr *index = (mMultiplier == 0) ? NULL :
    new NUBinaryOp (NUOp::eBiVhZxt, 
      new NUIdentRvalue (net, loc), 
      NUConst::create (false, width, 32, loc), 
      loc);
  if (mMultiplier == 0 && mOffset >= 0) {
    *expr = NUConst::create (false, mOffset, width, loc);
  } else if (mMultiplier == 0 && mOffset < 0) {
    *expr = new NUUnaryOp (NUOp::eUnMinus, 
      NUConst::create (false, std::abs (mOffset), width, loc),
      loc);
  } else if (mMultiplier == 1 && mOffset == 0) {
    *expr = index;
  } else if (mMultiplier == 1 && mOffset != 0) {
    *expr = new NUBinaryOp ((mOffset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus), 
      index,
      NUConst::create (false, std::abs (mOffset), width, loc),
      loc);
  } else if (mMultiplier == -1 && mOffset == 0) {
    *expr = new NUUnaryOp (NUOp::eUnMinus, index, loc);
  } else if (mMultiplier == -1 && mOffset != 0) {
    *expr = new NUBinaryOp ((mOffset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus), 
      new NUUnaryOp (NUOp::eUnMinus, index, loc),
      NUConst::create (false, std::abs (mOffset), width, loc),
      loc);
  } else if (mMultiplier > 0 && mOffset == 0) {
    *expr = new NUBinaryOp (NUOp::eBiUMult,
      index,
      NUConst::create (false, mMultiplier, width, loc),
      loc);
  } else if (mMultiplier < 0 && mOffset == 0) {
    *expr = new NUBinaryOp (NUOp::eBiSMult,
      index,
      new NUUnaryOp (NUOp::eUnMinus,
        NUConst::create (false, std::abs (mMultiplier), width, loc),
        loc),
      loc);
  } else if (mMultiplier > 0 && mOffset != 0) {
    *expr = new NUBinaryOp ((mOffset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus), 
      new NUBinaryOp (NUOp::eBiUMult,
        index,
        NUConst::create (false, mMultiplier, width, loc),
        loc),
      NUConst::create (false, std::abs (mOffset), width, loc),
      loc);
  } else if (mMultiplier < 0 && mOffset != 0) {
    *expr = new NUBinaryOp ((mOffset < 0 ? NUOp::eBiMinus : NUOp::eBiPlus), 
      new NUBinaryOp (NUOp::eBiUMult,
        new NUUnaryOp (NUOp::eUnMinus, index, loc),
        NUConst::create (false, mMultiplier, width, loc),
        loc),
      NUConst::create (false, std::abs (mOffset), width, loc),
      loc);
  } else {
    *expr = NULL;
    return false;                       // conditions should be exhaustive
  }
  (*expr)->resize (width);
  (*expr)->setSignedResult (mPrototype->isSignedResult ());
  return true;
}

bool Collapser::ConstantFunction::Power::create (const UInt32 width, NUNet *net, NUExpr **expr) const
{
  const SourceLocator &loc = mPrototype->getLoc ();
  NUExpr *power = new NUBinaryOp (NUOp::eBiLshift, 
    NUConst::create (false, 1, width, loc), 
    new NUIdentRvalue (net, loc),
    loc);
  if (mOffset == 0) {
    *expr = power;
  } else if (mOffset > 0) {
    *expr = new NUBinaryOp (NUOp::eBiPlus, power,
      NUConst::create (false, mOffset, width, loc),
      loc);
  } else {
    *expr = new NUBinaryOp (NUOp::eBiMinus, power,
      NUConst::create (false, std::abs (mOffset), width, loc),
      loc);
  }
  (*expr)->resize (width);
  return true;
}

bool Collapser::ConstantFunction::create (NUNet *net, NUExpr **expr) const
{
  if (mFlags & cIS_POWER) {
    return mPower.create (mWidth, net, expr);
  } else if (mFlags & cIS_AFFINE) {
    return mAffine.create (mWidth, net, expr);
  } else {
    *expr = NULL;
    return false;
  }
}

//! Output internal state dump when verboseAlert is specified
void Collapser::Matcher::internalError (const char *file, const int line, const char *cond, 
  const NUBase *context0, const NUBase *context1) const
{
  // Infer a source locator for the message... it's a little irritating in that
  // mHead can be either a statement or a case item. Their only common
  // superclass is NUBase with does not come equipped with a source locator.
  const SourceLocator *loc;
  if (mHead != NULL && dynamic_cast <const NUAssign *> (mHead) != NULL) {
    loc = &dynamic_cast <const NUAssign *> (mHead)->getLoc ();
  } else if (mHead != NULL  && dynamic_cast <const NUCaseItem *> (mHead) != NULL) {
    loc = &dynamic_cast <const NUCaseItem *> (mHead)->getLoc ();
  } else {
    loc = NULL;
  }
  // Now issue the alert
  if (!(mFlags & eVERBOSE_ALERTS)) {
    // Do not dump a huge amount of state, just issue a sanitised alert that
    // does not scare the customer!
    mMsgContext->VESanitisedInternalMatcherError (loc);
    mMsgContext->VEPleaseReportAlert (loc);
  } else {
    // -verboseAlerts was requested so dump internal state... lots of internal state
    mMsgContext->VEInternalMatcherError (loc, file, line, cond);
    if (context0) {
      context0->printAssertInfo ();
    }
    if (context1) {
      context1->printAssertInfo ();
    }
    mHead->pr ();
    print (true, 2);
  }
}
