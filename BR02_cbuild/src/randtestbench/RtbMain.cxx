// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



//#include "RtbApplication.h"


int main(int argc, char **argv)
{
#if 0
  RtbApplication app(&argc, argv);
  return app.run();
#endif
  return 0;
}
