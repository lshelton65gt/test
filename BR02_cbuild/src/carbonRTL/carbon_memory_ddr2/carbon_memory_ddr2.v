//******************************************************************************
// Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// model:
// carbon ddr2 memory model

// parameters:
// CS_BITS:      number of chip selects (ranks)
// DATA_BITS:    number of DM and DQS pins (1 per byte), also determines data bit width
// MAX_ADDR_BITS: the maximum number of address bits that can be used with this model, the BANK,ROW, and COLUMN bits are set by model variables (or SoCD parameters)
// DQS_IN_DELAY: default (1) assumes EARLY DQS input where DQS is high during first data transfer so it must
//               be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
//               For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
// DQS_OUT_DELAY: default (0) generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
//                data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1

`timescale 1ps / 1ps

module carbon_memory_ddr2
  #(parameter CS_BITS=4, DATA_BITS=4, MAX_ADDR_BITS=34, DQS_IN_DELAY=1, DQS_OUT_DELAY=0, DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1)
  (
   input                    ck,
   input                    ckbar,
   input [CS_BITS-1:0]      cke,
   input [CS_BITS-1:0]      cs_n,
   input                    ras_n,
   input                    cas_n,
   input                    we_n,
   input [2:0]              ba,   // maximum width supported - tie all unused bits to 0
   input [15:0]             addr, // maximum width supported - tie all unused bits to 0
   input [DM_BITS-1:0]      dm,

   inout [DM_BITS-1:0]      dqs,  // all dqs strobes must have identical timing in this cycle based model.
                                  // the vector form is only provided for convenience as only a single bit of DQS is used internally
   inout [DATA_BITS-1:0]    dq    // carbon observeSignal
   );



   `define CARBON_DDR2_MODEL

   carbon_ddrx_sdram #(
                       .CS_BITS(CS_BITS),
                       .DATA_BITS(DATA_BITS),
                       .MAX_ADDR_BITS(MAX_ADDR_BITS),
                       .DQS_IN_DELAY(DQS_IN_DELAY),
                       .DQS_OUT_DELAY(DQS_OUT_DELAY)
                       ) carbon_ddrx_sdram(
                                           .ck         (ck),
                                           .ckbar      (ckbar),
                                           .cke        (cke),
                                           .cs_n       (cs_n),
                                           .ras_n      (ras_n),
                                           .cas_n      (cas_n),
                                           .we_n       (we_n),
                                           .dm         (dm),
                                           .ba         (ba),
                                           .addr       (addr),
                                           .dq         (dq),
                                           .dqs        (dqs));



endmodule
