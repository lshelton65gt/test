//******************************************************************************
// Copyright (c) 2013 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// model:
// carbon ddr4 memory model

// parameters:
// CS_BITS:      number of chip selects (ranks)
// DATA_BITS:    number of data pins - also used to determine number of DM and DQS pins (1 per byte), 
// MAX_ADDR_BITS: the maximum number of address bits that can be used with this model, the BANK,ROW, and COLUMN bits are set by model variables (or SoCD parameters)
// WRITE_PREAMBLE:  number of write preamble DQS strobes expected - for DDR4 this can be 0 (disabled - default), 1 or 2 - this is set based on the PHY preamble generation logic and mode register settings
// READ_PREAMBLE:  number of read preamble DQS strobes to generate - for DDR4 this can be 0 (disabled - default), 1 or 2 - this is set based on the PHY preamble receive logic capabilities 
// DQS_IN_DELAY: default (1) assumes EARLY DQS input where DQS is high during first data transfer so it must
//               be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
//               For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
// DQS_OUT_DELAY: default (0) generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
//                data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1

`timescale 1ps / 1ps

module carbon_memory_ddr4
  #(parameter CS_BITS=4, DATA_BITS=4, MAX_ADDR_BITS=34, DQS_IN_DELAY=1, DQS_OUT_DELAY=0, WRITE_PREAMBLE=0, READ_PREAMBLE=0, DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1)
  (
   input                    ck,
   input                    ckbar,
   input [CS_BITS-1:0]      cke,
   input [CS_BITS-1:0]      cs_n,
   input                    act_n,
   input                    ras_n, // also A16
   input                    cas_n, // also A15
   input                    we_n, // also A14
   inout [DM_BITS-1:0]      dm, // also dbi_n which makes it birectional (tdqs function not supported)
   input [1:0]              bg, 
   input [1:0]              ba, 
   input [13:0]             addr, // maximum width supported since a16-a14 use ras/cas/we - tie all unused bits to 0
   input                    reset_n,
   inout [DATA_BITS-1:0]    dq,   // carbon observeSignal
   inout [DM_BITS-1:0]      dqs,  // all dqs strobes must have identical timing in this cycle based model.
                                  // the vector form is only provided for convenience as only a single bit of DQS is used internally
   inout [DM_BITS-1:0]      dqsbar,
   input                    par,
   output                   alert_n
   );



   `define CARBON_DDR4_MODEL

   
   carbon_ddrx_sdram #(
                       .CS_BITS(CS_BITS),
                       .DATA_BITS(DATA_BITS),
                       .MAX_ADDR_BITS(MAX_ADDR_BITS),
		       .WRITE_PREAMBLE(WRITE_PREAMBLE),
		       .READ_PREAMBLE(READ_PREAMBLE),
                       .DQS_IN_DELAY(DQS_IN_DELAY),
                       .DQS_OUT_DELAY(DQS_OUT_DELAY)
                       ) carbon_ddrx_sdram(
                                           .act_n      (act_n),
                                           .reset_n    (reset_n),
                                           .dqsbar     (dqsbar),
					   .par        (par),
					   .alert_n    (alert_n),
                                           .ck         (ck),
                                           .ckbar      (ckbar),
                                           .cke        (cke),
                                           .cs_n       (cs_n),
                                           .ras_n      (ras_n),
                                           .cas_n      (cas_n),
                                           .we_n       (we_n),
                                           .dm         (dm),
                                           .ba         ( {bg[1:0], ba[1:0]} ),
                                           .addr       ({2'h0, addr[13:0]}),
                                           .dq         (dq),
                                           .dqs        (dqs));



endmodule
