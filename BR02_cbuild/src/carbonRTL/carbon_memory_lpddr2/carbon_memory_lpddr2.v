//******************************************************************************
// Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// model:
// carbon lpddr2 memory model

// parameters:
// CS_BITS:      number of chip selects (ranks)
// DATA_BITS:    number of DM and DQS pins (1 per byte), also determines data bit width
// MAX_ADDR_BITS: the maximum number of address bits that can be used with this model, the BANK,ROW, and COLUMN bits are set by model variables (or SoCD parameters)
// DQS_IN_DELAY: default (1) assumes EARLY DQS input where DQS is high during first data transfer so it must
//               be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
//               For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
// DQS_OUT_DELAY: default (0) generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
//                data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1
// MR8_SETTING:  This parameter will define the reset value of Mode Register 8 (also known as Basic Configuration 4)
//               which defines the I/O width, Density, and Type of memory

`timescale 1ps / 1ps

module carbon_memory_lpddr2
  #(parameter CS_BITS=4, DATA_BITS=4, MAX_ADDR_BITS=34, DQS_IN_DELAY=1, DQS_OUT_DELAY=0, MR8_SETTING=1, DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1)
  (
   input                    ck_t,
   input                    ck_c,
   input [CS_BITS-1:0]      cke,
   input [CS_BITS-1:0]      cs_n,
   input [9:0]              ca,
   inout [DATA_BITS-1:0]    dq,    // carbon observeSignal
   inout [DM_BITS-1:0]      dqs_t,
   inout [DM_BITS-1:0]      dqs_c,
   input [DM_BITS-1:0]      dm
   );

   `define CARBON_LPDDR2_MODEL

   carbon_lpddrx_sdram #(
                         .CS_BITS(CS_BITS),
                         .DATA_BITS(DATA_BITS),
                         .MAX_ADDR_BITS(MAX_ADDR_BITS),
                         .DQS_IN_DELAY(DQS_IN_DELAY),
                         .DQS_OUT_DELAY(DQS_OUT_DELAY),
			 .MR8_SETTING(MR8_SETTING)
                         ) carbon_lpddrx_sdram(
                                               .ck_t         (ck_t),
                                               .ck_c         (ck_c), // unused
                                               .cke          (cke),
                                               .cs_n         (cs_n),
                                               .ca           (ca),
                                               .dq           (dq),
                                               .dqs_t        (dqs_t),
                                               .dqs_c        (dqs_c),
                                               .dm           (dm)
                                               );

endmodule
