
// Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.


///////////////////////////////////////////////////////////////////////////////////
// WARNING:  DO NOT MODIFY THIS FILE.                                            //
//    This file contains and uses an undocumented and unsuported API that will   //
//    change without notice                                                      //
///////////////////////////////////////////////////////////////////////////////////

include("$CARBON_HOME/lib/carbonRTL/carbon_memory_shared/src/JavaScript/DDRx.js");
