//******************************************************************************
// Copyright (c) 2012 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// model:
// carbon gddr5 memory model
//
// This model represents a collection of multiple physical gddr2 devices incorporated
// into a wide memory model.  Each device can be controlled individually, but the overall
// memory (as seen by debug accesses) is a single wide memory.  For example, a 64 bit wide
// memory subsystem may be created as 2x32 bit devices or 4x16 bit devices.  Both implementations
// can be modeled using this subsystem model, and both will appear to be a single 64 memory
// for debug purposes.  Device signals are available in vector form to allow connection to the
// memory controller as needed, allowing direct access to individual device models.

// parameters:
// DEVICE_BYTE_LANES:  determines data bit width (must be 2 or 4)
// DEVICES_PER_RANK:   determines data bit width 
// MAX_ADDR_BITS:      the maximum supported number of address bits
//                     see also the three registers carbon_current_BA_BITS carbon_current_ROW_BITS carbon_current_COL_BITS

`timescale 1ps / 1ps

module carbon_memory_gddr5
  #(parameter DEVICE_BYTE_LANES=4, DEVICES_PER_RANK=1, MAX_ADDR_BITS=20)
  (
   input [DEVICES_PER_RANK-1:0]				    reset_n,
   input 						    ck,   
   input 						    wck,  
   input [DEVICES_PER_RANK-1:0]				    ras_n,
   input [DEVICES_PER_RANK-1:0]				    cas_n,
   input [DEVICES_PER_RANK-1:0]				    we_n,
   input [DEVICES_PER_RANK-1:0] 			    cke,
   input [DEVICES_PER_RANK-1:0] 			    cs_n,
   
   // Wide Signals where only a subvector is applied to a "device" 
   // (Note addr is replicated modulo 12 to facilitate connections and reading waveforms)
   input [(12*DEVICES_PER_RANK)-1:0] 			    a, // 12 = arbitrary maximum width supported - any unused bit should be tied to 0 - in reality this is only 8/9
   input [DEVICES_PER_RANK-1:0] 			    abi_n,
   inout [(DEVICES_PER_RANK*8*DEVICE_BYTE_LANES)-1:0] 	    dq, 
   inout [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	    dbi_n,
   output [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	    edc
   );

   
`define CARBON_GDDR5_MODEL

   carbon_gddr5_sdram #(
			.DEVICE_BYTE_LANES(DEVICE_BYTE_LANES),
			.DEVICES_PER_RANK(DEVICES_PER_RANK),
			.MAX_ADDR_BITS(MAX_ADDR_BITS)
			) carbon_gddr5_sdram(
                                             .reset_n    (reset_n),
                                             .ck         (ck),
                                             .wck        (wck),
                                             .cke        (cke),
                                             .cs_n       (cs_n),
                                             .ras_n      (ras_n),
                                             .cas_n      (cas_n),
                                             .we_n       (we_n),
                                             .a          (a),
                                             .abi_n      (abi_n),
                                             .dq         (dq),
                                             .dbi_n      (dbi_n),
					     .edc        (edc));



endmodule
