// Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.


///////////////////////////////////////////////////////////////////////////////////
// WARNING:  DO NOT MODIFY THIS FILE.                                            //
//    This file contains and uses an undocumented and unsuported API that will   //
//    change without notice                                                      //
///////////////////////////////////////////////////////////////////////////////////


// common ccfg related utility functions

function addMemLocRTL(memBlock, path,
                      dispStart, dispEnd, dispLsb, dispMsb,
                      rtlStart, rtlEnd, rtlLsb, rtlMsb) {
  memLocRTL = memBlock.AddLocRTL();

  memLocRTL.Path = path;
  memLocRTL.DisplayStartWordOffset = dispStart;
  memLocRTL.DisplayEndWordOffset = dispEnd;
  memLocRTL.DisplayMSB = dispMsb;
  memLocRTL.DisplayLSB = dispLsb;
  memLocRTL.StartWordOffset = rtlStart;
  memLocRTL.EndWordOffset = rtlEnd;
  memLocRTL.MSB = rtlMsb;
  memLocRTL.LSB = rtlLsb;

  return memLocRTL;
}


// this function looks for _name_ as if it were the original name of a subComponent (defined in Construct)
// and returns the name of that SubComponent that is used after the user had a chance to rename it
// if the user had not renamed the subcomponent then the original name is returned  
function findFinalNameofSubComponent(ccfg,name){
  var subComp;
  for ( i = 0; i < ccfg.NumSubComponents; i++){
    subComp = ccfg.GetSubComponent(i);
    if ( subComp.PersistentCompName == name){
      return subComp.CompName;
    }
  }
  return name;
}


// adapted from modelKits CfgFunctions.js, this function should be moved to an area common to all DDRx scripts
//
// the first argument is a ccfg object; either the top level ccfg or a subcomponent,
function addCustomCode(component, code, section, position, genType)
{
  var customCodeObj = null;

  // See if this already exists
  for (var i = 0; i < component.GetNumCustomCodes(); ++i) {
    var c = component.GetCustomCode(i);
    if ((c.GetSection() == section) && (c.GetPosition() == position)) {
      customCodeObj = c;
      break;
    }
  }

  if (customCodeObj == null) {
    // Create a new one
    customCodeObj = component.AddCustomCode(genType);
    customCodeObj.SetSection(section);
    customCodeObj.SetPosition(position);
    customCodeObj.SetCode(code);
  } else {
    // Append the new code to what's already there
    var curCode = customCodeObj.GetCode();
    customCodeObj.SetCode(curCode + code);
  }
}


// remove any custom code that is currently in the specif  
function clearCustomCode(ccfg, section, position)
{
  var customCodeObj = null;

  // See if this section/position already exists
  for (var i = 0; i < ccfg.GetNumCustomCodes(); ++i) {
    var c = ccfg.GetCustomCode(i);
    if ((c.GetSection() == section) && (c.GetPosition() == position)) {
      customCodeObj = c;
      break;
    }
  }

  if (customCodeObj != null) {
    customCodeObj.SetCode("");
  }
}
