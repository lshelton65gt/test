 // Copyright (c) 2012 by Carbon Design Systems, Inc., All Rights Reserved.

// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.


///////////////////////////////////////////////////////////////////////////////////
// WARNING:  DO NOT MODIFY THIS FILE.                                            //
//    This file contains and uses an undocumented and unsuported API that will   //
//    change without notice                                                      //
///////////////////////////////////////////////////////////////////////////////////

include("$CARBON_HOME/lib/carbonRTL/carbon_memory_shared/src/JavaScript/ccfg_util.js");

var gVerbose = false;
function Construct(ccfg, db, items) {
  if ( gVerbose ) debug("Construct started");
  var userdata = new Array();

  var memName;
  var hierPathToMemory;
  var instanceIndex;
  var instanceNode;
  var instancePath;
  var moduleName;
  var instanceSuffixeChoices = "abcdefghijklmnopqrstuvwxyz";
  for (instanceIndex = 0; instanceIndex < items.length; instanceIndex++) {
    instanceNode = items[instanceIndex];
    instancePath = db.nodeGetFullName(instanceNode); // something like top.myDDR2
    moduleName = db.nodeComponentName(instanceNode);

    switch ( moduleName ){
    case "carbon_memory_ddr":
      memName = "ddr";
      hierPathToMemory = instancePath + ".carbon_ddrx_sdram.";    //  (up to and including "carbon_ddrx_sdram.")
      break;
    case "carbon_memory_ddr2":
      memName = "ddr2";
      hierPathToMemory = instancePath + ".carbon_ddrx_sdram.";    //  (up to and including "carbon_ddrx_sdram.")
      break;
    case "carbon_memory_ddr3":
      memName = "ddr3";
      hierPathToMemory = instancePath + ".carbon_ddrx_sdram.";    //  (up to and including "carbon_ddrx_sdram.")
      break;
    case "carbon_memory_ddr4":
      memName = "ddr4";
      hierPathToMemory = instancePath + ".carbon_ddrx_sdram.";    //  (up to and including "carbon_ddrx_sdram.")
      break;
    case "carbon_memory_lpddr2":
      memName = "lpddr2";
      hierPathToMemory = instancePath + ".carbon_lpddrx_sdram.";    //  (up to and including "carbon_lpddrx_sdram.")
      break;
    case "carbon_memory_lpddr3":
      memName = "lpddr3";
      hierPathToMemory = instancePath + ".carbon_lpddrx_sdram.";    //  (up to and including "carbon_lpddrx_sdram.")
      break;
    case "carbon_memory_gddr5":
      memName = "gddr5";
      hierPathToMemory = instancePath + ".carbon_gddr5_sdram.";    //  (up to and including "carbon_gddr5_sdram.")
      break;
    default:
      debug("Error: Unsupported memory module: " + moduleName);
    }

    if ( items.length > 1 ){
      //  use a suffix for the memName only if there is more than a single instance to keep them unique
      memName += "_" + instanceSuffixeChoices.charAt(instanceIndex);
    }
    
    // find out number of chip selects based on ranks
    var numChipSelects = GetChipSelects(db, hierPathToMemory);
    var numBlocks = GetBlocks(db, hierPathToMemory);
    var numDevices = 1;

    // a net that has ADDR_WIDTH bits
    var hierPathToAnAddressWidthNet;
    var maxDepth = 0;
    if (moduleName == "carbon_memory_gddr5") {
      numDevices = GetDevices(db, hierPathToMemory);
      maxDepth = GetMaxGDDR5Depth(db, numDevices, hierPathToMemory);
    } else {
      maxDepth = GetMaxDepth(db, hierPathToMemory);
    }
    if (maxDepth == 0){
      throw("Error: unable to determine memory depth based on : " + hierPathToMemory);
    }

    // define the dimensions of the memory we want to display
    var memWordWidth = GetMemoryWordSize(db, hierPathToMemory);
    if (memWordWidth == 0){
      throw("Error: unable to determine memory word width for: " + hierPathToMemory);
    }
    var maxAddr = (maxDepth * numBlocks)-1;  // assumes each block is as wide as the display memory, and that all blocks are the same size

    // create subcomponent with this name
    var ccfgSubComp = ccfg.AddSubComp(memName, CcfgEnum.Generated, memName);

    // rememember the Persistent memNames (the names of the subcomponents, and the number of chipSelects each has)
    userdata[instanceIndex] = {persistentName :memName, moduleName :moduleName, numCS: numChipSelects, numDev: numDevices, ccfgSubComp: ccfgSubComp, hierPathToMemory: hierPathToMemory};

    // the name is editable
    ccfgSubComp.EditFlags = CcfgFlags.EditComponentName;

    try {
      // for each chip select
      for (var indexChipSelect = 0; indexChipSelect < numChipSelects; indexChipSelect++) {
        // Create a memory space, Mark subcomponent as generated type
        var memory = ccfgSubComp.AddMemory("CS" + indexChipSelect, memWordWidth, maxAddr, CcfgEnum.Generated);
	// define the fields that are user editable
        memory.EditFlags = CcfgFlags.EditMemorySystemAddressMapping | CcfgFlags.EditMemoryBlockBaseAddress;
        memory.EditFlags |= CcfgFlags.EditMemorySystemAddressESLPortName | CcfgFlags.EditMemorySystemAddressESLPortBaseAddress;
        memory.EditFlags |= CcfgFlags.EditMemoryInitializationReadmemFormat | CcfgFlags.EditMemoryInitializationReadmemFile;

        memory.InitType = CcfgEnum.MemInitNone;
        memory.SetMAU(memWordWidth/8);
        memory.SetBigEndian(false);
        memory.SetProgramMemory(false);
        memory.SetDisplayAtZero(false);


        // Add Memory Block
        var memBlock = memory.AddMemoryBlock();
        memBlock.Name = "mem";
        memBlock.SetBase(0);
        memBlock.SetSize(numBlocks*(maxDepth*(memWordWidth/8)));  // the 'size' is in bytes

        var thisDepthStart = 0;
        for (var whichBlock = 0; whichBlock < numBlocks; whichBlock++) {
          // Add RTL Locators
          var locName = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.block[" + whichBlock + "].block_mem.mem";
          var locNameTry1 = locName;
          var locNode = db.findNode(locName);
          if ( locNode == null ){
            locName = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .\\block[" + whichBlock + "]_block_mem .mem"; //legacy style names
            locNode = db.findNode(locName);
          }
          if ( locNode == null ) {
            throw("Error: unable to find path to memory : " + locName + " or " + locNameTry1);
          }

          var nodeWidth = db.nodeGetWidth(locNode);
          addMemLocRTL(memBlock, locName,
		       thisDepthStart, thisDepthStart + maxDepth-1, 0, nodeWidth - 1,
		       0,              maxDepth-1,                  0, nodeWidth - 1);
          thisDepthStart = thisDepthStart + maxDepth;
        }
      }
    }
    catch (err) {
      debug("Duplicate subComponent: " + err);
    }
  }
  if ( gVerbose ) debug("Construct finished");
  return userdata;
}


// this function is called after the user has had a chance to edit the DDR memory, (only the parts that we have allowed to be editable)
// In this function we create stuff that is not editable by the user but uses a name based on an editable field
// Modify objects here if they must be created in the Construct function, but need adjustment based on user editing.
// userdata is an array of objects, it is created in Construct function
function Restore(ccfg, db, items, userdata) {
  
  if ( gVerbose ) debug("Restore started");
  
  var persistentName;
  var moduleName;
  var numChipSelects;
  var numDevices;
  var devNum;
  var ccfgSubComp;
  var memName;
  var cfgGroup;
  var indexChipSelect;
  var i,j;
  var hierPathToMemory;
  var cfgReg;
  var regField;
  var pathToMem;
  var regLoc;
  var regName;
  var regComment;
  var code;
  var extended;
  var modeRegisters;

  
  for( i = 0; i < userdata.length; i++)
  {
    persistentName = userdata[i].persistentName;
    moduleName = userdata[i].moduleName;
    numChipSelects = userdata[i].numCS;
    numDevices = userdata[i].numDev;
    ccfgSubComp = userdata[i].ccfgSubComp;
    hierPathToMemory = userdata[i].hierPathToMemory;

    switch ( moduleName ){
    case "carbon_memory_ddr":
      modeRegisters = ["MR", "EMR"];                      // DDR has 2 registers: MR, EMR.  Here we use names that are compatible with C++ identifiers
      break;
    case "carbon_memory_ddr2":
      modeRegisters = ["MR", "EMR_1", "EMR_2", "EMR_3"];  // DDR2 has 4 registers: MR EMR(1) EMR(2) EMR(3).  Here we use names that are compatible with C++ identifiers
      break;
    case "carbon_memory_ddr3":
      modeRegisters = ["MR0", "MR1", "MR2", "MR3"];       // DDR3 has 4 registers: MR0 MR1 MR2 MR3.
      break;
    case "carbon_memory_ddr4":
      modeRegisters = ["MR0", "MR1", "MR2", "MR3", "MR4", "MR5", "MR6"];  // DDR4 has 7 registers: MR0 MR1 MR2 MR3 MR4 MR5 MR6.
      break;
    case "carbon_memory_lpddr2":
      // LPDDR2 has many registers.  These are the non-reserved ones
      // present for SDRAM.  Because they're implemented in a memory
      // in the RTL, we need to store both the name and its offset
      // into the memory.
      modeRegisters = [ {name: "Device Info",                  offset: 0},
                        {name: "Device Feature 1",             offset: 1},
                        {name: "Device Feature 2",             offset: 2},
                        {name: "I/O Config-1",                 offset: 3},
                        {name: "Refresh Rate",                 offset: 4},
                        {name: "Basic Config-1",               offset: 5},
                        {name: "Basic Config-2",               offset: 6},
                        {name: "Basic Config-3",               offset: 7},
                        {name: "Basic Config-4",               offset: 8},
                        {name: "Test Mode",                    offset: 9},
                        {name: "IO Calibration",               offset: 10},
                        {name: "PASR_Bank",                    offset: 16},
                        {name: "PASR_Seg",                     offset: 17},
                        {name: "DQ Calibration Pattern A",     offset: 32},
                        {name: "DQ Calibration Pattern B",     offset: 40}];
      break;
    case "carbon_memory_lpddr3":
      // LPDDR3 has many registers.  These are the non-reserved ones
      // present for SDRAM.  Because they're implemented in a memory
      // in the RTL, we need to store both the name and its offset
      // into the memory.
      modeRegisters = [ {name: "Device Info",                  offset: 0},
                        {name: "Device Feature 1",             offset: 1},
                        {name: "Device Feature 2",             offset: 2},
                        {name: "I/O Config-1",                 offset: 3},
                        {name: "Refresh Rate",                 offset: 4},
                        {name: "Basic Config-1",               offset: 5},
                        {name: "Basic Config-2",               offset: 6},
                        {name: "Basic Config-3",               offset: 7},
                        {name: "Basic Config-4",               offset: 8},
                        {name: "Test Mode",                    offset: 9},
                        {name: "IO Calibration",               offset: 10},
                        {name: "ODT Feature",                  offset: 11},
                        {name: "PASR_Bank",                    offset: 16},
                        {name: "PASR_Seg",                     offset: 17},
                        {name: "DQ Calibration Pattern A",     offset: 32},
                        {name: "DQ Calibration Pattern B",     offset: 40}];
      break;
    case "carbon_memory_gddr5":
      modeRegisters = ["MR0","MR1","MR2","MR3","MR4","MR5","MR6","MR7","MR8","MR9","MR10","MR11","MR12","MR13","MR14","MR15"]; // Each GDDR5 device has 16 registers
      break;
    default:
      throw("Error: unsupported memory type: " + moduleName);
    }
    
    memName = findFinalNameofSubComponent(ccfg,persistentName);

    // define the parameters
    if ( moduleName == "carbon_memory_gddr5") {
      addRBCParameter(ccfg, ccfgSubComp, memName, hierPathToMemory);
      addCurrentBitParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_ROW_BITS", "Row", "13", "|12|13|");
      addCurrentBitParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_BA_BITS", "Bank", "4",  "|3|4|");
      addCurrentBitParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_COL_BITS", "Column", "7", "|6|7|");
    } else {
      addRBCParameter(ccfg, ccfgSubComp, memName, hierPathToMemory);
      addCurrentBitParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_ROW_BITS", "Row", "16", "|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|");
      addCurrentBitParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_BA_BITS", "Bank", "3",  "|1|2|3|");
      addCurrentBitParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_COL_BITS", "Column", "15", "|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|");
    }
    
    if ( moduleName  == "carbon_memory_lpddr2"){
      // We're removing these for the first release, but may bring
      // them back at some time.  The issue is that these are used to
      // initialize memory in the RTL in an initial block, and CMS
      // doesn't support non-constant expressions in that context.
      // Instead, we're hardcoding the initial values in the RTL, but
      // the user can still modify the corresponding register values
      // through CADI.
      //
      // addMRRegParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_MR6_VALUE", "MR6", "1");
      // addMRRegParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_MR7_VALUE", "MR7", "0");
      // addMRRegParameter(ccfg, ccfgSubComp, memName, hierPathToMemory + "carbon_current_MR8_VALUE", "MR8", "1");
      
      addTdqsckParameter(ccfg, ccfgSubComp, memName, hierPathToMemory);
//      addIntParameter(ccfg, ccfgSubComp, memName + "_tdqsck", hierPathToMemory + "carbon_current_READ_DELAY", "value for tdqsck -- additional delay for read", 0, "|0|.5|1.0|1.5|");
    }
    if ( moduleName  == "carbon_memory_lpddr3"){
      addTdqsckParameter(ccfg, ccfgSubComp, memName, hierPathToMemory);
    }
    
    // now define the registerGroups and registers
    // (this is done in Restore because:
    //    the user cannot edit them,
    //    they are not displayed,
    //    we need to use a name based on any edits the user made to the subcomponent name)

    cfgGroup = ccfgSubComp.AddGroup(memName);
    // for each chip select
    for (indexChipSelect = 0; indexChipSelect < numChipSelects; indexChipSelect++) {
      if ((moduleName == "carbon_memory_lpddr2") || (moduleName == "carbon_memory_lpddr3")) {
        // LPDDR2/3 implementation is different from DDR/DDR2/DDR3
        for (j = 0; j < modeRegisters.length; ++j) {
          regName =  "CS" + indexChipSelect + " " + modeRegisters[j].name;
          regComment = "CS" + indexChipSelect + " " + memName + " Mode Register " + modeRegisters[j].name + " - Register Number " + modeRegisters[j].offset;
          cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, 8, false, CcfgEnum.Hex, regComment);
          regField = cfgReg.AddRegisterField(7, 0, CcfgEnum.RW);
          regField.SetName("Value");
          pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.MR_reg";
          var pathToMemTry1 = pathToMem;
          if (db.findNode(pathToMem) == null){
            pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .MR_reg"; // legacy style names
          }
          if ( db.findNode(pathToMem) == null ) {
            throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1);
          }

          regLoc = regField.AddLocArray(pathToMem, modeRegisters[j].offset);
        }
      } else if (moduleName == "carbon_memory_gddr5") {
        // GDDR5 implementation is also different from DDR/DDR2/DDR3
 	var devString = "";
 	for (devNum = 0; devNum < numDevices; devNum++) {
 	  if (numDevices > 1) {
 	    devString = "Device "+devNum+" ";
 	  } 
 	  for (j = 0; j < modeRegisters.length; ++j) {
 	    regName =  "CS" + indexChipSelect + " " + devString + modeRegisters[j];
 	    regComment = "CS" + indexChipSelect + " " + memName + devString + " Mode Register " + modeRegisters[j];

 	    cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, 16, false, CcfgEnum.Hex, regComment);
 	    regField = cfgReg.AddRegisterField(15, 0, CcfgEnum.RW);
 	    regField.SetName("Value");
 	    pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.device["+devNum+"].device.MR"+j+"_reg";
            var pathToMemTry1 = pathToMem;
            if (db.findNode(pathToMem) == null){
              pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .\\device["+devNum+"]_device .MR"+j+"_reg"; // legacy style name
            }
            if (db.findNode(pathToMem) == null){
              throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
            }

 	    regLoc = regField.AddLocReg(pathToMem, 31, 0);
 	  }
	  // if addition to the Mode Registers, provide access to the Vendor ID data, a special register optionally used during init
	  regName =  "CS" + indexChipSelect + " " + devString + "Vendor ID";
	  regComment = "CS" + indexChipSelect + " " + memName + devString + "Vendor ID Data driven on the device DQ pins when MR3[7:6] == 01";

	  cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, 16, false, CcfgEnum.Hex, regComment);
	  regField = cfgReg.AddRegisterField(31, 0, CcfgEnum.RW);
	  regField.SetName("Value");
	  pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.device["+devNum+"].device.raw_vendor_id_dq";
          var pathToMemTry1 = pathToMem;
          if (db.findNode(pathToMem) == null){
            pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .\\device["+devNum+"]_device .raw_vendor_id_dq"; // legacy style name
          }
          if (db.findNode(pathToMem) == null){
            throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
          }

	  regLoc = regField.AddLocReg(pathToMem, 31, 0);
 	}
      } else {
        extended = ""; // all but reg 0 have "Extended" in the comment (not used for LPDDR2/3)
        for (j = 0; j < modeRegisters.length; ++j) {
          regName =  "CS" + indexChipSelect + " " + modeRegisters[j];
          regComment = "CS" + indexChipSelect + " " + memName + extended + " Mode Register " + modeRegisters[j];
          extended = " Extended";
          //   create register [#CS] EmbeddedName Register  at location rank[#CS]_rank_mem .MR3_reg
          cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, 16, false, CcfgEnum.Hex, regComment);
          regField = cfgReg.AddRegisterField(15, 0, CcfgEnum.RW);

          regField.SetName("Value");

          //  locreg
          pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.MR" + j + "_reg";
          var pathToMemTry1 = pathToMem;
          if (db.findNode(pathToMem) == null){
            pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .MR" + j + "_reg"; // legacy style name
          }
          if (db.findNode(pathToMem) == null){
            throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
          }
          regLoc = regField.AddLocReg(pathToMem, 31, 0);
        }
      }
    }
    if (moduleName != "carbon_memory_gddr5") {
      // Profile Count Registers
      cfgGroup = ccfgSubComp.AddGroup(memName+" Commands");
      profileCountRegs = ["ACTIVATE", "READ", "WRITE", "PRECHARGE", "REFRESH", "READ DATA", "WRITE DATA", "Current RD Utilization", "Current WR Utilization", "Current Bus Utilization"];
      profileCountSize = [32, 32, 32, 32, 32, 32, 32, 8, 8, 8];
      profileCountPath = ["profile_activate_total_counter", "profile_read_total_counter", "profile_write_total_counter", "profile_precharge_total_counter", "profile_refresh_total_counter", "profile_read_data_total_counter", "profile_write_data_total_counter", "profile_read_utilization", "profile_write_utilization", "profile_total_utilization"];
      profileCountDesc = ["Total number of ACTIVATE commands (decimal)", "Total number of READ commands (decimal)", "Total number of WRITE commands (decimal)", "Total number of PRECHARGE commands (decimal)", "Total number of REFRESH commands (decimal)", "Total number of READ data beats (decimal)", "Total number of WRITE data beats (decimal)", "Data Bus Utilization (decimal percent) due to READs (100 cycle average)", "Data Bus Utilization (decimal percentage) due to WRITEs (100 cycle average)", "Total Data Bus Utilization (decimal percentage - 100 cycle average)"];
      // for each chip select
      for (indexChipSelect = 0; indexChipSelect < numChipSelects; indexChipSelect++) {
	for (j = 0; j < profileCountRegs.length; ++j) {
	  regName =  "CS" + indexChipSelect + " " + profileCountRegs[j];
	  regComment = "CS" + indexChipSelect + profileCountDesc[j];
	  //   create register [#CS] EmbeddedName Register  at location rank[#CS]_rank_mem .MR3_reg
	  cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, profileCountSize[j], false, CcfgEnum.UnsignedInt, regComment);
	  regField = cfgReg.AddRegisterField(profileCountSize[j]-1, 0, CcfgEnum.RW);
	  regField.SetName("Value");
	  //  locreg
	  pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem." + profileCountPath[j];
	  var pathToMemTry1 = pathToMem;
	  if (db.findNode(pathToMem) == null){
	    pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem ." + profileCountPath[j]; // legacy style name
	  }
	  if (db.findNode(pathToMem) == null){
	    throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
	  }
	  regLoc = regField.AddLocReg(pathToMem, profileCountSize[j]-1, 0);
	}
      }
      maxBanks = 8;
      if (moduleName == "carbon_memory_ddr4") {
	maxBanks = 16;
      }
      // Profile Bank Activate Counter Registers
      cfgGroup = ccfgSubComp.AddGroup(memName+" Bank Commands");
      // for each chip select
      for (indexChipSelect = 0; indexChipSelect < numChipSelects; indexChipSelect++) {
	for (j = 0; j < maxBanks; ++j) {
	  regName =  "CS" + indexChipSelect + " ACTIVATE Bank" + j;
	  regComment = "CS" + indexChipSelect + " Number of ACTIVATE commands sent to Bank " + j + " (Decimal)";
	  cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, 32, false, CcfgEnum.UnsignedInt, regComment);
	  regField = cfgReg.AddRegisterField(31, 0, CcfgEnum.RW);
	  regField.SetName("Value");
	  //  locreg
	  pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.profile_activate_counter[" + j + "]";
	  var pathToMemTry1 = pathToMem;
	  if (db.findNode(pathToMem) == null){
	    pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .profile_activate_counter[" + j + "]"; // legacy style name
	  }
	  if (db.findNode(pathToMem) == null){
	    throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
	  }
	  regLoc = regField.AddLocReg(pathToMem, 31, 0);
	}
 	for (j = 0; j < maxBanks; ++j) {
 	  regName =  "CS" + indexChipSelect + " READ Bank" + j;
 	  regComment = "CS" + indexChipSelect + " Number of READ commands sent to Bank " + j + " (Decimal)";
 	  cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, 32, false, CcfgEnum.UnsignedInt, regComment);
 	  regField = cfgReg.AddRegisterField(31, 0, CcfgEnum.RW);
 	  regField.SetName("Value");
 	  //  locreg
 	  pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.profile_read_counter[" + j + "]";
 	  var pathToMemTry1 = pathToMem;
 	  if (db.findNode(pathToMem) == null){
 	    pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .profile_read_counter[" + j + "]"; // legacy style name
 	  }
 	  if (db.findNode(pathToMem) == null){
 	    throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
 	  }
 	  regLoc = regField.AddLocReg(pathToMem, 31, 0);
 	}
	for (j = 0; j < maxBanks; ++j) {
	  regName =  "CS" + indexChipSelect + " WRITE Bank" + j;
	  regComment = "CS" + indexChipSelect + " Number of WRITE commands sent to Bank " + j + " (Decimal)";
	  cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, 32, false, CcfgEnum.UnsignedInt, regComment);
	  regField = cfgReg.AddRegisterField(31, 0, CcfgEnum.RW);
	  regField.SetName("Value");
	  //  locreg
	  pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem.profile_write_counter[" + j + "]";
	  var pathToMemTry1 = pathToMem;
	  if (db.findNode(pathToMem) == null){
	    pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem .profile_write_counter[" + j + "]"; // legacy style name
	  }
	  if (db.findNode(pathToMem) == null){
	    throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
	  }
	  regLoc = regField.AddLocReg(pathToMem, 31, 0);
	}
      }
      // Profile Event Registers
      cfgGroup = ccfgSubComp.AddGroup(memName+" Events");
      profileEventRegs = ["ACTIVATE", "READ", "WRITE", "BST", "PRECHARGE", "REFRESH", "READ DATA", "WRITE DATA"];
      profileEventSize = [16, 16, 16, 16, 16, 16, 16, 8, 8, 8, 8, 8];
      profileEventPath = ["profile_activate", "profile_read", "profile_write", "profile_bst", "profile_precharge", "profile_refresh", "profile_read_data", "profile_write_data"];
      profileEventDesc = ["ACTIVATE command bank event vector (hex)", "READ command bank event vector (hex)", "WRITE command bank event vector (hex)", "BST command bank vector (hex)", "PRECHARGE command bank event vector (hex)", "REFRESH command bank vector (hex)", "READ Data Beat event", "WRITE Data Beat event"];
      // for each chip select
      for (indexChipSelect = 0; indexChipSelect < numChipSelects; indexChipSelect++) {
	for (j = 0; j < profileEventRegs.length; ++j) {
	  regName =  "CS" + indexChipSelect + " " + profileEventRegs[j] + " Event";
	  regComment = "CS" + indexChipSelect + profileEventDesc[j];
	  cfgReg = ccfgSubComp.AddRegister(regName, cfgGroup, profileEventSize[j], false, CcfgEnum.Hex, regComment);
	  regField = cfgReg.AddRegisterField(profileEventSize[j]-1, 0, CcfgEnum.RW);
	  regField.SetName("Value");
	  //  locreg
	  pathToMem = hierPathToMemory + "rank[" + indexChipSelect + "].rank_mem." + profileEventPath[j];
	  var pathToMemTry1 = pathToMem;
	  if (db.findNode(pathToMem) == null){
	    pathToMem = hierPathToMemory + "\\rank[" + indexChipSelect + "]_rank_mem ." + profileEventPath[j]; // legacy style name
	  }
	  if (db.findNode(pathToMem) == null){
	    throw("Error: unable to find path to memory : " + pathToMem + " or " + pathToMemTry1 );
	  }
	  regLoc = regField.AddLocReg(pathToMem, profileEventSize[j]-1, 0);
	}
      }
    }
  }

  if ( gVerbose ) debug("Restore finished");
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// here are some routines that examine the db to extract information about the current configuration //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GetChipSelects(db, hierPathToMemory) {
  var numChipSelects = 0;
  for (var indexCS = 0; indexCS < 1000; indexCS++) {
    var pathToMem = hierPathToMemory + "rank[" + indexCS + "].rank_mem";
    if (db.findNode(pathToMem) == null) {
      pathToMem = hierPathToMemory + "\\rank[" + indexCS + "]_rank_mem "; // legacy style
    }
    if (db.findNode(pathToMem) != null)
      numChipSelects = indexCS + 1;
    else break;
  }
  return numChipSelects;
}

function GetDevices(db, hierPathToMemory) {
  // use width of address/command port to determine device count
  var pathToPort = hierPathToMemory + "rank[0].rank_mem.abi_n";
  var abi_n_node = db.findNode(pathToPort);
  if ( abi_n_node == null) {
    pathToPort = hierPathToMemory + "\\rank[0]_rank_mem .abi_n"; // legacy style
    abi_n_node = db.findNode(pathToPort);
  }
  
  if ( abi_n_node == null) {
    throw("Error: unable to determine number of devices based on : " + hierPathTomemory + " with either rank[0].rank_mem.abi_n or \\rank[0]_rank_mem .abi_n");
    return 0;
  }

  return db.nodeGetWidth(abi_n_node);
}

function GetBlocks(db, hierPathToMemory) {
  var numBlocks = 0;
  // first determine which style of generate block names is being used (look for non-legacy style names)
  var pathToMem = hierPathToMemory + "rank[0].rank_mem.block[0].block_mem"; // this path is the first we will search for
  var legacy_style = !(db.findNode(pathToMem) != null);

  for (var indexBS = 0; indexBS < 1000; indexBS++) {
    if ( legacy_style ) {
      pathToMem = hierPathToMemory + "\\rank[0]_rank_mem .\\block[" + indexBS + "]_block_mem "; // legacy style
    } else {
      pathToMem = hierPathToMemory + "rank[0].rank_mem.block[" + indexBS + "].block_mem";
    }

    if (db.findNode(pathToMem) != null) {
      numBlocks = indexBS + 1;
    } else {
      break;
    }
  }
  
  return numBlocks;
}

// The goal in GetMaxDepth is to find the depth (or # of words) by getting the width of the address signal. 
// Then, given this width, the maximum possible address is calculated. This is merely 2 to the width.
// 
function GetMaxDepth(db, hierPathToMemory) {

  var hierPathToAnAddressWidthNet = hierPathToMemory + "rank[0].rank_mem.block[0].block_mem.waddr_a";
  var addrWidthNode = db.findNode(hierPathToAnAddressWidthNet);
  if (addrWidthNode == null) {
    hierPathToAnAddressWidthNet = hierPathToMemory + "\\rank[0]_rank_mem .\\block[0]_block_mem .waddr_a"; // legacy style
    addrWidthNode = db.findNode(hierPathToAnAddressWidthNet);
  }
  if (addrWidthNode == null) {
    throw("Error: unable to determine memory depth based on : " +hierPathTomemory + " with either rank[0].rank_mem.block[0].block_mem.waddr_a or \\rank[0]_rank_mem .\\block[0]_block_mem .waddr_a");
    return 0;
  }
    

  var maxDepth = 0;

  // this represents the number of data bits.
  var addrWidth = db.nodeGetWidth(addrWidthNode);
  if (addrWidth != 0) 
     maxDepth = Math.pow(2, addrWidth);

  return maxDepth;
}

// The goal in GetMaxGDDR5Depth is to find the depth (or # of words) by getting the width of the address signal
// and then dividing by the number of devices in the rank (each device uses a subvector of the overall bus)
// Then, given this width, the maximum possible address is calculated. This is merely 2 to the width.
// 
function GetMaxGDDR5Depth(db, numDevices, hierPathToMemory) {

  var hierPathToAnAddressWidthNet = hierPathToMemory + "rank[0].rank_mem.block[0].block_mem.addr_a";
  var addrWidthNode = db.findNode(hierPathToAnAddressWidthNet);
  if (addrWidthNode == null){
    hierPathToAnAddressWidthNet = hierPathToMemory + "\\rank[0]_rank_mem .\\block[0]_block_mem .addr_a"; // legacy style
    addrWidthNode = db.findNode(hierPathToAnAddressWidthNet);
  }

  if (addrWidthNode == null){
    throw("Error: unable to determine memory depth based on : " +hierPathTomemory + " with either rank[0].rank_mem.block[0].block_mem.addr_a or \\rank[0]_rank_mem .\\block[0]_block_mem .addr_a");
    return 0;
  }

  var maxDepth = 0;

  // this represents the number of data bits.
  var addrWidth = db.nodeGetWidth(addrWidthNode);
  addrWidth = addrWidth / numDevices;
  if (addrWidth != 0) 
     maxDepth = Math.pow(2, addrWidth);
  return maxDepth;
}

function GetMemoryWordSize(db, hierPathToMemory) {
  var hierPathNet = hierPathToMemory + "dq"; // we use a node that is declared the same width as the memory
  var node = db.findNode(hierPathNet);
  if ( node == null) {
    throw("Error: unable to determine memory word size based on : " + hierPathNet);
    return 0;
  }
  var width = db.nodeGetWidth(node);
  return width;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
// support routines below this line  
////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
function addRBCParameter(topComponent, subComponent, memName, hierPathToMemory){
  var code;
  var paramName = memName + "_Address_Format";
  // note that the parameter is a top level parameter, while the custom code for it goes in the subcomponent

  topComponent.AddParam(paramName, CcfgEnum.XtorString, "RBC", CcfgEnum.Init, "select between row-bank_column or bank_row_column address configuration", "|RBC|BRC|", CcfgEnum.Generated);
  // define necessary custom code for this parameter, (converts string to integer with values 0 or 1)
  code = "";
  code += "public:\n";
  code += "  CarbonNetID* m"+ paramName + ";\n";
  code += "private:\n";
  addCustomCode(subComponent, code, CcfgEnum.CompClass, CcfgEnum.Pre, CcfgEnum.Generated);

  code = "";
  code += "  m"+ paramName + " = NULL;\n";
  addCustomCode(subComponent, code, CcfgEnum.CompConstructor, CcfgEnum.Post, CcfgEnum.Generated);

  code = "";
  code += "  m"+ paramName + " = carbonFindNet(mCarbonObj, \"" + hierPathToMemory +"carbon_row_bank_column_format\");\n";
  code += "  if ( m"+ paramName + " ) {\n";
  code += "    CarbonUInt32 localValue = (\"RBC\" == getTopComp()->getParameter(\"" + paramName + "\")) ? 1 : 0;\n";
  code += "    carbonDeposit(mCarbonObj, m" + paramName + ", &localValue, NULL);\n";
  code += "  }\n";
  addCustomCode(subComponent, code, CcfgEnum.CompInit, CcfgEnum.Post, CcfgEnum.Generated);
}

function addTdqsckParameter(topComponent, subComponent, memName, hierPathToMemory){
  var code;
  var paramName = memName + "_tdqsck";
  // note that the parameter is a top level parameter, while the custom code for it goes in the subcomponent

  topComponent.AddParam(paramName, CcfgEnum.XtorString, "0.0", CcfgEnum.Init, "select value for tdqsck -- additional delay for read", "|0.0|0.5|1.0|1.5|2.0|2.5|3.0|3.5|", CcfgEnum.Generated);
  // define necessary custom code for this parameter, (converts string to integer with half cycle delay value)
  code = "";
  code += "public:\n";
  code += "  CarbonNetID* m"+ paramName + ";\n";
  code += "private:\n";
  addCustomCode(subComponent, code, CcfgEnum.CompClass, CcfgEnum.Pre, CcfgEnum.Generated);

  code = "";
  code += "  m"+ paramName + " = NULL;\n";
  addCustomCode(subComponent, code, CcfgEnum.CompConstructor, CcfgEnum.Post, CcfgEnum.Generated);

  code = "";
  code += "  m"+ paramName + " = carbonFindNet(mCarbonObj, \"" + hierPathToMemory +"carbon_current_READ_DELAY\");\n";
  code += "  if ( m"+ paramName + " ) {\n";
  code += "    string strValue = getTopComp()->getParameter(\"" + paramName + "\");\n";
  code += "    CarbonUInt32 halfCycleDelayValue = 0;\n";
  code += "    if      (strValue == \"0.0\") {halfCycleDelayValue = 0;}\n";
  code += "    else if (strValue == \"0.5\") {halfCycleDelayValue = 1;}\n";
  code += "    else if (strValue == \"1.0\") {halfCycleDelayValue = 2;}\n";
  code += "    else if (strValue == \"1.5\") {halfCycleDelayValue = 3;}\n";
  code += "    else if (strValue == \"2.0\") {halfCycleDelayValue = 4;}\n";
  code += "    else if (strValue == \"2.5\") {halfCycleDelayValue = 5;}\n";
  code += "    else if (strValue == \"3.0\") {halfCycleDelayValue = 6;}\n";
  code += "    else if (strValue == \"3.5\") {halfCycleDelayValue = 7;}\n";
  code += "    else    /* default */       {halfCycleDelayValue = 0;}\n";
  code += "    carbonDeposit(mCarbonObj, m" + paramName + ", &halfCycleDelayValue, NULL);\n";
  code += "  }\n";
  addCustomCode(subComponent, code, CcfgEnum.CompInit, CcfgEnum.Post, CcfgEnum.Generated);
}


// adds a parameter for the ROW BANK or COLUMN internal configuration register  
function addCurrentBitParameter(topComponent, subComponent, memName, hierPathToRegister, typeName, defaultValue, enumChoices){
  var paramName = memName + "_" + typeName +"_Bits";
  var comment = "specify the number of bits in for the " + typeName + " segment of the address";
  addIntParameter(topComponent, subComponent, paramName, hierPathToRegister, comment, defaultValue, enumChoices);
}

// adds a parameter for the MR* type registers
function addMRRegParameter(topComponent, subComponent, memName, hierPathToRegister, typeName, defaultValue, enumChoices){
  var paramName = memName + "_" + typeName;
  var comment = "specify the value that will be returned for the " + typeName + " register";
  addIntParameter(topComponent, subComponent, paramName, hierPathToRegister, comment, defaultValue, enumChoices);
}


// add a parameter for an integer type value, enums are supported
function addIntParameter(topComponent, subComponent, paramName, hierPathToRegister, comment, defaultValue, enumChoices){
  var code;
  var paramType = (enumChoices == "") ? CcfgEnum.XtorUInt32 : CcfgEnum.XtorString; 

  // note that the parameter is a top level parameter, while the custom code for it goes in the subcomponent
  topComponent.AddParam(paramName, paramType,  defaultValue, CcfgEnum.Init, comment, enumChoices, CcfgEnum.Generated);
  // define necessary custom code for this parameter, (ties parameter value to internal register)
  code = "";
  code += "public:\n";
  code += "  CarbonNetID* m"+ paramName + ";\n";
  code += "private:\n";
  addCustomCode(subComponent, code, CcfgEnum.CompClass, CcfgEnum.Pre, CcfgEnum.Generated);

  code = "";
  code += "  m"+ paramName + " = NULL;\n";
  addCustomCode(subComponent, code, CcfgEnum.CompConstructor, CcfgEnum.Post, CcfgEnum.Generated);

  code = "";
  code += "  m"+ paramName + " = carbonFindNet(mCarbonObj, \"" + hierPathToRegister + "\");\n";
  code += "  if ( m"+ paramName + " ) {\n";
  code += "    CarbonUInt32 localValue;\n";
  code += "    eslapi::CASIConvertErrorCodes status = CASIConvertStringToValue((getTopComp()->getParameter(\"" + paramName + "\")),&localValue);\n";
  code += "    if (status == eslapi::CASIConvert_SUCCESS) {\n";
  code += "      carbonDeposit(mCarbonObj, m" + paramName + ", &localValue, NULL);\n";
  code += "    }\n";
  code += "  }\n";
  addCustomCode(subComponent, code, CcfgEnum.CompInit, CcfgEnum.Post, CcfgEnum.Generated);
}

