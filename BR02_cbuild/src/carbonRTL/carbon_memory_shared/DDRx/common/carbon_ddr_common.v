//******************************************************************************
// Copyright (c) 2010-2011 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// Common subcomponents used to build double date rate memory models
`timescale 1ps / 1ps
`define CARBON_COMMON_DELAY 1

// `define CARBON_DEBUG_BLOCK_MEM_WRITES 1

// ***************************************************************************
// 
// 

module carbon_dual_edge_flip_flop(clk, d, q);
   parameter DATA_BITS = 8;
   
   input clk;
   input [DATA_BITS-1:0] d;
   output reg [DATA_BITS-1:0] q;

   always @(posedge clk)
     q <= #`CARBON_COMMON_DELAY d;
   
   always @(negedge clk)
     q <= #`CARBON_COMMON_DELAY d;
   
endmodule

// ***************************************************************************
// Retimes and delays the in signal by DELAY 1/2 clock cycles
// This is a fixed delay
module carbon_retiming_element(clk, in, out);
   parameter DATA_BITS = 8;
   parameter DELAY = 0;

   input clk;
   input [DATA_BITS-1:0] in;
   output [DATA_BITS-1:0] out;
   
   genvar g;
   generate
      if (DELAY == 0) begin: NO_DELAY
	 assign #`CARBON_COMMON_DELAY out = in;
      end
      else if (DELAY == 1) begin: DELAY_1
	 carbon_dual_edge_flip_flop #(DATA_BITS) cdeff(clk, in, out);
      end
      else begin: DELAY_N
	 // each delay stage value
	 reg [DATA_BITS-1:0]     qout_reg[0:DELAY-1];
	 
	 wire [DATA_BITS-1:0]    qout_wire;
	 
	 always @(qout_wire)
	   qout_reg[0] <= #`CARBON_COMMON_DELAY qout_wire;
	 
	 
	 carbon_dual_edge_flip_flop #(DATA_BITS) cdeff(clk, in, qout_wire);
	 
	 for (g = 0; g < (DELAY-1); g = g + 1) begin : DELAY_ELEMENT
	    wire [DATA_BITS-1:0]    qout_wire;
	    carbon_dual_edge_flip_flop #(DATA_BITS) cdeff(clk, qout_reg[g], qout_wire);
	 
	    always @(qout_wire)
	      qout_reg[g+1] <= #`CARBON_COMMON_DELAY qout_wire;
	 end
	 
	 assign #`CARBON_COMMON_DELAY out = qout_reg[DELAY-1];
      end
	
   endgenerate
endmodule

// ***************************************************************************
// Retimes and delays the in signal by a variable number of 1/2 clock cycles
// This is a runtime changeable delay

module carbon_variable_delay_element(clk, select, in, out);
   parameter DATA_BITS = 8;

   input clk;
   input [3:0] select;
   input [DATA_BITS-1:0]   in;
   output reg [DATA_BITS-1:0]  out;

   wire [DATA_BITS-1:0]        qout_wire1;
   wire [DATA_BITS-1:0]        qout_wire2;
   wire [DATA_BITS-1:0]        qout_wire3;
   wire [DATA_BITS-1:0]        qout_wire4;
   wire [DATA_BITS-1:0]        qout_wire5;
   wire [DATA_BITS-1:0]        qout_wire6;
   wire [DATA_BITS-1:0]        qout_wire7;
   wire [DATA_BITS-1:0]        qout_wire8;
   wire [DATA_BITS-1:0]        qout_wire9;
   wire [DATA_BITS-1:0]        qout_wire10;
   wire [DATA_BITS-1:0]        qout_wire11;
   wire [DATA_BITS-1:0]        qout_wire12;
   wire [DATA_BITS-1:0]        qout_wire13;
   wire [DATA_BITS-1:0]        qout_wire14;
   wire [DATA_BITS-1:0]        qout_wire15;
   
   
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff1(clk, in,          qout_wire1);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff2(clk, qout_wire1,  qout_wire2);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff3(clk, qout_wire2,  qout_wire3);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff4(clk, qout_wire3,  qout_wire4);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff5(clk, qout_wire4,  qout_wire5);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff6(clk, qout_wire5,  qout_wire6);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff7(clk, qout_wire6,  qout_wire7);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff8(clk, qout_wire7,  qout_wire8);
   carbon_dual_edge_flip_flop #(DATA_BITS)  cdeff9(clk, qout_wire8,  qout_wire9);
   carbon_dual_edge_flip_flop #(DATA_BITS) cdeff10(clk, qout_wire9,  qout_wire10);
   carbon_dual_edge_flip_flop #(DATA_BITS) cdeff11(clk, qout_wire10, qout_wire11);
   carbon_dual_edge_flip_flop #(DATA_BITS) cdeff12(clk, qout_wire11, qout_wire12);
   carbon_dual_edge_flip_flop #(DATA_BITS) cdeff13(clk, qout_wire12, qout_wire13);
   carbon_dual_edge_flip_flop #(DATA_BITS) cdeff14(clk, qout_wire13, qout_wire14);
   carbon_dual_edge_flip_flop #(DATA_BITS) cdeff15(clk, qout_wire14, qout_wire15);

   // mux to select the desired delay
   always @(*) begin
      case(select)
	3'h0: out = in;
	3'h1: out = qout_wire1;
	3'h2: out = qout_wire2;
	3'h3: out = qout_wire3;
	3'h4: out = qout_wire4;
	3'h5: out = qout_wire5;
	3'h6: out = qout_wire6;
	3'h7: out = qout_wire7;
	3'h8: out = qout_wire8;
	3'h9: out = qout_wire9;
	3'ha: out = qout_wire10;
	3'hb: out = qout_wire11;
	3'hc: out = qout_wire12;
	3'hd: out = qout_wire13;
	3'he: out = qout_wire14;
	3'hf: out = qout_wire15;
      endcase
   end

endmodule

// ***************************************************************************
// ***************************************************************************
// 
// Implemented as module (instead of function) to allow parameters
// NOTE there is no error checking that DM_BITS and DATA_BITS are consistent!

module carbon_dm_to_dmask(dm, dmask);
   parameter DM_BITS = 1;
   parameter DATA_BITS = 8;
   
   input [DM_BITS-1:0] dm;
   output reg [DATA_BITS-1:0] dmask;

   integer 		       i;
   
   always @(dm) begin
      // default is bits will be written in case where 8 * DM_BITS < DATA_BITS
      dmask = { DATA_BITS { 1'b1 } };
      for ( i = 0; i < DM_BITS; i = i + 1 ) begin
	 // if dm is set, data is not written
         if (dm[i])
           dmask[i*8 +: 8] = 8'h00;
      end
   end
   
endmodule

// ***************************************************************************
// ***************************************************************************
// Simple model of a RAM, to be used for each block.
// Dual ported to allow a DDR transfer to be converted into a SDR transfer
// A single Write Enable is provided as both ports will be written at the same time
// Separate write and read address signal are provided, as well as write address clock
// This allows we/waddr signals to be sampled independently from the din/dmask, allowing
// the driving device to determine the ultimate timing for write data

module carbon_ddr_block_mem(wdclk, we, waddr_a, din_a, dmask_a, raddr_a, dout_a, waddr_b, din_b, dmask_b, raddr_b, dout_b);
   parameter ADDR_WIDTH = 16;
   parameter DATA_BITS = 8;
   parameter DEPTH = 1 << ADDR_WIDTH;

   input                    wdclk; // write data clock - data sampled on posedge only
   input                    we; // write enable

   wire 		    wdclk_in;
   
   assign #`CARBON_COMMON_DELAY wdclk_in = wdclk; // glitch filter for event sim

   // Port A
   input [ADDR_WIDTH-1:0]   waddr_a; // carbon observeSignal
   input [DATA_BITS-1:0]    dmask_a; // data bit mask for write operations - high mask bit means bit will be written
   input [DATA_BITS-1:0]    din_a;
   input [ADDR_WIDTH-1:0]   raddr_a;
   output [DATA_BITS-1:0]   dout_a;

   // Port B
   input [ADDR_WIDTH-1:0]   waddr_b;
   input [DATA_BITS-1:0]    dmask_b;
   input [DATA_BITS-1:0]    din_b;
   input [ADDR_WIDTH-1:0]   raddr_b;
   output [DATA_BITS-1:0]   dout_b;

   // Main memory storage
   reg [DATA_BITS-1:0] 	    mem [0:DEPTH-1]; // carbon depositSignal
                                             // carbon observeSignal

   // Read output data
   assign dout_a = mem[raddr_a];
   assign dout_b = mem[raddr_b];


   // Data write operations 
   always @(posedge wdclk_in) begin
      if (we)
	begin
	   mem[waddr_a] <= (din_a & dmask_a) | (mem[waddr_a] & ~dmask_a);
	   mem[waddr_b] <= (din_b & dmask_b) | (mem[waddr_b] & ~dmask_b);
`ifdef CARBON_DEBUG_BLOCK_MEM_WRITES
	   $display("%m WRITE xDDRx BLOCK MEM:  waddr_a = 0x%08x  data_a = 0x%08x  mem[waddr_a] = 0x%08x:  waddr_b = 0x%08x  data_b = 0x%08x   mem[waddr_a] = 0x%08x\n", 
		    waddr_a, ((din_a & dmask_a) | (mem[waddr_a] & ~dmask_a)), mem[waddr_a],
		    waddr_b, ((din_b & dmask_b) | (mem[waddr_b] & ~dmask_b)), mem[waddr_b]);
`endif
	end
   end
   
endmodule        

