//******************************************************************************
// Copyright (c) 2010-2014 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// Parameterized model of LPDDR2-Sx and LPDDR3 memory for general use in Carbon compiler environments.
// This is a minimal implementation, excluding things like refresh and error checking.
// Interface DQS retiming options are provided to help with the remodeling of the PHY interface.

// this model is defined such that it can support many different memory size
// configurations, however the maximum sizes are defined by the following:
`define MAX_BA_BITS 3
`define MAX_ROW_BITS 16
`define MAX_COL_BITS 15


`define VERSION_MAJOR 1
`define VERSION_MINOR 0

`timescale 1ps / 1ps

// ****************************************
// Supported Commands
// cmd = {cs_n_pe, ca_pe[0], ca_pe[1], ca_pe[2], ca_pe[3]};
`define CMD_INVALID    5'b11111
`define CMD_MRW        5'b00000
`define CMD_MRR        5'b00001
`define CMD_REF_BANK   5'b00010
`define CMD_REF_ALL    5'b00011
`define CMD_ACTIVATE   3'b001 // ca_pe[2:3] are don't care
`define CMD_WRITE      4'b0100 // ca_pe[3] is don't care
`define CMD_READ       4'b0101 // ca_pe[3] is don't care
`define CMD_PRECHARGE  5'b01101
`ifdef CARBON_LPDDR2_MODEL
 `define CMD_BST        5'b01100 // burst terminate
`endif
`define CMD_NOP        4'b0111

`ifdef CARBON_LPDDR2_MODEL
// ****************************************
// Supported LPDDR2 Register Fields/Values

// burst length (bl)
 `define vBL4 3'b010
 `define vBL8 3'b011
 `define vBL16 3'b100

// burst type (bt)
 `define vBT_SEQ 1'b0
 `define vBT_INT 1'b1
`endif 

`ifdef CARBON_LPDDR3_MODEL
// ****************************************
// Supported LPDDR3 Register Fields/Values

// burst length (bl)
 `define vBL8 3'b011

// burst type (bt)
 `define vBT_SEQ 1'b0
`endif 
// ********************************************************************************
// Top Level LPDDRx Memory Subsystem

module carbon_lpddrx_sdram(
                           ck_t,
                           ck_c, // unused!
                           cke,
                           cs_n,
                           ca,
                           dq,
                           dqs_t,
                           dqs_c,
                           dm
                           );

   // number of chip selects (ranks) and data width
   parameter CS_BITS = 4;  // determines number of cs_n and cke pins
   parameter DATA_BITS = 8;  // determines data bit width and number of DM and DQS pins (1 per byte)

   parameter MAX_ADDR_BITS = 34;  // the maximum supported number of address bits
                                  // see also the three registers carbon_current_BA_BITS carbon_current_ROW_BITS carbon_current_COL_BITS

   // cycle accurate timing parameters

   // DQS_IN_DELAY: default assumes EARLY DQS input where DQS is high during first data transfer so it must
   // be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
   // For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
   parameter DQS_IN_DELAY = 1;

   // DQS_OUT_DELAY: default generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
   // data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1
   parameter DQS_OUT_DELAY = 0;

   // Basic Configuration 4 setting - this parameter will define the reset value of 
   // Mode Register 8 which defines the I/O width, Density, and Type of memory
   parameter MR8_SETTING = 1;
   
   // Support for write preamble - off by default for legacy reasons
   parameter WRITE_PREAMBLE = 0;

   // calculated parameter
   parameter DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1;

   input                    ck_t;
   input                    ck_c; // unused
   input [CS_BITS-1:0]      cke;
   input [CS_BITS-1:0]      cs_n;
   input [9:0]              ca;
   // from cycle based perspective all dqs strobes must have identical timing
   // only a single DQS signal is use internally - vector form provided for convenience
   // DQS_C is not used internally for writes, but is generated for convenience on reads
   inout [DATA_BITS-1:0]    dq;	// carbon observeSignal
   inout [DM_BITS-1:0]      dqs_t;
   inout [DM_BITS-1:0]      dqs_c;
   input [DM_BITS-1:0]      dm;

   pullup dqs_c_pu[DM_BITS-1:0](dqs_c);

   // check for supported range of parameter values, if the configuration is within range then this generate block will not produce any verilog
   generate
      if ( DATA_BITS  > 255 ) begin : checkDATA_BITS
	 always @(posedge ck_t)
	   begin   // carbon enableOutputSysTasks
	      $display("\nConfiguration Error - DATA_BITS: %d is larger than supported value: %d.\n", DATA_BITS, 255);
	      $stop;
	   end
      end

      if ( MAX_ADDR_BITS > (`MAX_BA_BITS + `MAX_ROW_BITS + `MAX_COL_BITS)) begin  : checkMAX_ADDR_BITS
	 always @(posedge ck_t)
	   begin  // carbon enableOutputSysTasks
	      $display("\nConfiguration Error - MAX_ADDR_BITS: %d is larger than the supported value %d.\n", MAX_ADDR_BITS, (`MAX_BA_BITS + `MAX_ROW_BITS + `MAX_COL_BITS));
	      $stop;
	   end
      end
      
   endgenerate

   // ********************************************************************************
   // ********************************************************************************
   // Internal parameters

   // Each block of internal memory is limited to 2^32 bits, so calcuate how many blocks are needed
   // This needs to consider the data width in addition to the bank/row/column bits
   parameter LOG2_DATA_BITS = (DATA_BITS < 2) ? 1 :
                              (DATA_BITS < 4) ? 2 :
                              (DATA_BITS < 8) ? 3 :
                              (DATA_BITS < 16) ? 4 :
                              (DATA_BITS < 32) ? 5 :
                              (DATA_BITS < 64) ? 6 :
                              (DATA_BITS < 128) ? 7 :
                              (DATA_BITS < 256) ? 8 :
                              0; // greater than 255 data bits is not supported

   parameter TOTAL_MEM_BITS = LOG2_DATA_BITS + MAX_ADDR_BITS;
   parameter BLOCK_BITS = (TOTAL_MEM_BITS > 32) ? TOTAL_MEM_BITS - 32 : 0;
   parameter NUM_BLOCKS = 1 << BLOCK_BITS;

   parameter BLOCK_ADDR_BITS = (NUM_BLOCKS > 1) ? (32 - LOG2_DATA_BITS):MAX_ADDR_BITS;

   // define some registers that are driven by SoCD parameter values or Carbon C API to configure this memory
   
   // actual RBC mode used for device addressing; choices are:  1 == row_bank_column; 0 == bank_row_colum;   
   reg                      carbon_row_bank_column_format; // carbon depositSignal
                                                           // carbon observeSignal

   // actual bit widths used for device addressing - see JEDEC standard for specific device configurations
   reg [31:0] 		    carbon_current_BA_BITS;    // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0] 		    carbon_current_ROW_BITS;   // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0] 		    carbon_current_COL_BITS;   // carbon depositSignal
                                                       // carbon observeSignal

   // carbon_current_READ_DELAY, holds value of tdqsck (see specification)
   //  This value is used add read delay in 1/2 clock cycle increments defaults to 0 and is set through runtime deposits (or SoCD parameter)
   reg [3:0] 		    carbon_current_READ_DELAY; // carbon forceSignal
                                                       // carbon observeSignal

   // Parameters used during compilation (made available through Carbon C API)
   wire [7:0] 		    MAX_ADDR_BITS_VALUE = MAX_ADDR_BITS; // carbon observeSignal
   wire [7:0]               CS_BITS_VALUE = CS_BITS;      // carbon observeSignal
   wire [7:0]               DATA_BITS_VALUE = DATA_BITS;  // carbon observeSignal
   wire [7:0]               BA_BITS_VALUE  = carbon_current_BA_BITS;   // carbon observeSignal
   wire [7:0]               ROW_BITS_VALUE  = carbon_current_ROW_BITS;   // carbon observeSignal
   wire [7:0]               COL_BITS_VALUE  = carbon_current_COL_BITS;   // carbon observeSignal
   wire [7:0]               DM_BITS_VALUE = DM_BITS;      // carbon observeSignal
   wire [7:0]               DQS_IN_DELAY_VALUE  = DQS_IN_DELAY;      // carbon observeSignal
   wire [7:0]               DQS_OUT_DELAY_VALUE  = DQS_OUT_DELAY;    // carbon observeSignal
   wire [7:0]               NUM_BLOCKS_VALUE = NUM_BLOCKS;           // carbon observeSignal
   wire [7:0]               BLOCK_ADDR_BITS_VALUE = BLOCK_ADDR_BITS; // carbon observeSignal

   initial
      begin
	 carbon_row_bank_column_format = 1;
	 carbon_current_BA_BITS = 2;
	 carbon_current_ROW_BITS = 16;
	 carbon_current_COL_BITS = 14;
      end

   wire [CS_BITS-1:0] external_dqs_expected; // carbon observeSignal

   // check for error condition where more than 1 rank is expecting the external DQS - this can occur on back-to-back writes to different ranks with a data delay
   // this case will cause incorrect data to be written and is a problem with the data timing since there is no way for the second rank written to know that the data is late
   // and really destined for the first rank
   integer 	      i;
   reg [7:0] 	      expected_count;
   always @(posedge ck_t)
     begin
	expected_count = 0;
	for (i = 0; i < CS_BITS; i = i + 1) begin
	   if (external_dqs_expected[i])
	     expected_count = expected_count + 1;
	end
	if (expected_count > 1)
          begin  // carbon enableOutputSysTasks
             $display("\nModeling Error - Multiple DDRx Memory Rank Chip Selects are expecting to receive write DQS signals at the same time.\nThis indicates the data is arriving too late for the configured memory latencies,\nor the memory controller is not allowing sufficient time between writes to different ranks.  Data corruption may result.\n");
             $stop;
          end
     end
   
   genvar                   g;
   generate
      // Create multiple ranks
      for ( g = 0; g < CS_BITS; g = g + 1 ) begin : rank
         carbon_lpddrx_rank #(.DATA_BITS(DATA_BITS),
			      .MAX_ADDR_BITS(MAX_ADDR_BITS),
                              .DM_BITS(DM_BITS),
                              .DQS_IN_DELAY(DQS_IN_DELAY),
                              .DQS_OUT_DELAY(DQS_OUT_DELAY),
			      .MR8_SETTING(MR8_SETTING),
			      .WRITE_PREAMBLE(WRITE_PREAMBLE),
                              .BLOCK_BITS(BLOCK_BITS),
                              .NUM_BLOCKS(NUM_BLOCKS),
                              .BLOCK_ADDR_BITS(BLOCK_ADDR_BITS)) rank_mem(
                                                                          .ck_t(ck_t),
                                                                          .cke(cke[g]),
                                                                          .cs_n(cs_n[g]),
                                                                          .ca(ca),
                                                                          .dm(dm),
                                                                          .dq(dq),
                                                                          .dqs_t(dqs_t),
                                                                          .dqs_c(dqs_c),
									  .carbon_external_dqs_expected(external_dqs_expected[g]),
									  .carbon_row_bank_column_format(carbon_row_bank_column_format),
									  .carbon_current_BA_BITS(carbon_current_BA_BITS),
									  .carbon_current_ROW_BITS(carbon_current_ROW_BITS),
									  .carbon_current_COL_BITS(carbon_current_COL_BITS),
									  .carbon_current_READ_DELAY(carbon_current_READ_DELAY)
                                                                          );
      end
   endgenerate

endmodule

// ********************************************************************************
// LPDDRx Memory for each rank

module carbon_lpddrx_rank(
                          ck_t,
                          cke,
                          cs_n,
                          ca,
                          dm,
                          dq,
                          dqs_t,
                          dqs_c,
			  carbon_external_dqs_expected,
			  carbon_row_bank_column_format,
			  carbon_current_BA_BITS,
			  carbon_current_ROW_BITS,
			  carbon_current_COL_BITS,
			  carbon_current_READ_DELAY
                          );


   parameter DATA_BITS = 1;
   parameter MAX_ADDR_BITS = 34;
   parameter DM_BITS = 1;
   parameter DQS_IN_DELAY = 1;
   parameter DQS_OUT_DELAY = 0;
   parameter MR8_SETTING = 1;
   parameter WRITE_PREAMBLE = 0;
   
   parameter BLOCK_BITS = 1;
   parameter NUM_BLOCKS = 1;
   parameter BLOCK_ADDR_BITS = 0;

   // port list
   input                    ck_t;
   input                    cke;
   input                    cs_n;
   input [9:0]              ca;

   input [DM_BITS-1:0]      dm;
   // from cycle based perspective all dqs strobes must have identical timing
   // only a single DQS signal is use internally - vector form provided for convenience
   inout [DM_BITS-1:0]      dqs_t;
   inout [DM_BITS-1:0]      dqs_c;
   inout [DATA_BITS-1:0]    dq;

   output 		    carbon_external_dqs_expected;
   input                    carbon_row_bank_column_format;
   input [31:0] 	    carbon_current_BA_BITS;
   input [31:0] 	    carbon_current_ROW_BITS;
   input [31:0] 	    carbon_current_COL_BITS;
   input [3:0] 		    carbon_current_READ_DELAY;
   
   wire [31:0] 		    carbon_current_BA_BITS_mask = (1 << carbon_current_BA_BITS)-1;
   wire [31:0] 		    carbon_current_ROW_BITS_mask = ( 1 << carbon_current_ROW_BITS)-1;
   wire [31:0] 		    carbon_current_COL_BITS_mask = ( 1 << carbon_current_COL_BITS)-1;

   wire [31:0] 		    carbon_current_COMBINED_ADDR_BITS = carbon_current_BA_BITS + carbon_current_ROW_BITS + carbon_current_COL_BITS;
   // use 64'b1 here to force verilog sizing rule to do the shift and subtraction with enough bits to always fill MAX_ADDR_BITS (we assume 64 > MAX_ADDR_BITS)
   wire [MAX_ADDR_BITS-1:0] carbon_current_COMBINED_ADDR_BITS_mask = (64'b1 << carbon_current_COMBINED_ADDR_BITS)-1;

   reg 			    write_preamble_enabled;
 			    
   integer 		    i;
   reg  		    internal_reset;
   reg [1:0] 		    internal_reset_cnt;
   initial
     begin
	internal_reset = 1;
	internal_reset_cnt = 0;
     end

   always @(posedge ck_t)
     begin
	if (internal_reset_cnt != 2'b11) 
	  internal_reset_cnt <= internal_reset_cnt + 1;
	else
	  internal_reset <= 0;
     end

     
   // ********************************************************************************
   // ********************************************************************************
   // Profiling event registers - bank vector form 
   
   reg                      profile_mrw;       // carbon observeSignal
   reg                      profile_mrr;       // carbon observeSignal
   reg [15:0]               profile_activate;  // carbon observeSignal
   reg [15:0]               profile_read;      // carbon observeSignal
   reg [15:0]               profile_write;     // carbon observeSignal
   reg [15:0]               profile_bst;       // carbon observeSignal
   reg [15:0]               profile_precharge; // carbon observeSignal
   reg [15:0]               profile_refresh;   // carbon observeSignal

   reg  		    profile_read_data; // carbon observeSignal
   reg  		    profile_write_data;// carbon observeSignal
   
   // Per bank counters
   reg [31:0] 		    profile_activate_counter[0:15]; // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_read_counter[0:15];     // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_write_counter[0:15];    // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_precharge_counter[0:15];// carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_refresh_counter[0:15];  // carbon observeSignal
                                                            // carbon depositSignal

   // Total counts
   reg [31:0] 		    profile_activate_total_counter; // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_read_total_counter;     // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_write_total_counter;    // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_precharge_total_counter;// carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_refresh_total_counter;  // carbon observeSignal
                                                            // carbon depositSignal

   reg [31:0] 		    profile_read_data_total_counter;  // carbon observeSignal
                                                              // carbon depositSignal
   reg [31:0] 		    profile_write_data_total_counter; // carbon observeSignal
                                                              // carbon depositSignal

   always @(posedge ck_t)
     begin
	for (i = 0; i < 16; i = i + 1) begin
	   if (profile_activate[i]) begin
	      profile_activate_counter[i] = profile_activate_counter[i] + 1;
	      profile_activate_total_counter = profile_activate_total_counter + 1;
	   end
	   if (profile_read[i]) begin
	      profile_read_counter[i] = profile_read_counter[i] + 1;
	      profile_read_total_counter = profile_read_total_counter + 1;
	   end
	   if (profile_write[i]) begin
	      profile_write_counter[i] = profile_write_counter[i] + 1;
	      profile_write_total_counter = profile_write_total_counter + 1;
	   end
	   if (profile_precharge[i]) begin
	      profile_precharge_counter[i] = profile_precharge_counter[i] + 1;
	      profile_precharge_total_counter = profile_precharge_total_counter + 1;
	   end
	   if (profile_refresh[i]) begin
	      profile_refresh_counter[i] = profile_refresh_counter[i] + 1;
	      profile_refresh_total_counter = profile_refresh_total_counter + 1;
	   end
	end
	if (profile_read_data)
	  profile_read_data_total_counter = profile_read_data_total_counter + 1;
	if (profile_write_data)
	  profile_write_data_total_counter = profile_write_data_total_counter + 1;
     end
   

   // Utilization: Sliding window average (100 cycles)
   reg [99:0] 		    profile_read_data_window;       // carbon observeSignal
   reg [99:0] 		    profile_write_data_window;      // carbon observeSignal
   reg [7:0] 		    profile_read_utilization;       // carbon observeSignal
   reg [7:0] 		    profile_write_utilization;      // carbon observeSignal
   reg [7:0] 		    profile_total_utilization;      // carbon observeSignal
 		    
   always @(posedge ck_t)
     begin
	profile_read_data_window  <= {profile_read_data_window[98:0],  profile_read_data};
	profile_write_data_window <= {profile_write_data_window[98:0], profile_write_data};
	profile_read_utilization = 0;
	profile_write_utilization = 0;
	for (i = 0; i < 100; i = i + 1) begin
	   if (profile_read_data_window[i])
	     profile_read_utilization = profile_read_utilization + 1;
	   if (profile_write_data_window[i])
	     profile_write_utilization = profile_write_utilization + 1;
	end
	profile_total_utilization = profile_write_utilization + profile_read_utilization;
     end
   
   // ********************************************************************************
   // ********************************************************************************
   // Internal Mode registers
   // All possible registers are allocated storage, but not all provide function

   reg [15:0]               MR_reg[0:255]; // carbon depositSignal
                                           // carbon observeSignal

   // Some have default values that must be initialized - use a task as it must be called by MRW Reset command as well
   task initialize_MR_reg;
      begin
         for (i = 0; i < 256; i = i + 1) begin
            MR_reg[i] = 8'h00;
         end
`ifdef CARBON_LPDDR2_MODEL
         MR_reg[1] = 8'h22;
         MR_reg[2] = 8'h01;
         MR_reg[3] = 8'h02;
         MR_reg[4] = 8'h03;
         MR_reg[5] = 8'h0c;
         MR_reg[6] = `VERSION_MAJOR;
         MR_reg[7] = `VERSION_MINOR;
         MR_reg[8] = MR8_SETTING; // value for MR8 {width,density,type} width(2bit field),density(4bit field),type(2bit field)
`endif
`ifdef CARBON_LPDDR3_MODEL
         MR_reg[0] = 8'hc0; // supports WL Set B and RL3 options
         MR_reg[1] = 8'h43;
         MR_reg[2] = 8'h18;
         MR_reg[3] = 8'h02;
         MR_reg[4] = 8'h03;
         MR_reg[5] = 8'h0c;
         MR_reg[6] = `VERSION_MAJOR;
         MR_reg[7] = `VERSION_MINOR;
         MR_reg[8] = MR8_SETTING; // value for MR8 {width,density,type} width(2bit field),density(4bit field),type(2bit field)
`endif	 
      end
   endtask

   initial
     begin
        initialize_MR_reg;
	write_preamble_enabled = WRITE_PREAMBLE;
     end

   // ********************************************************************************
   // ********************************************************************************
   // Bidirectional signal generation and timing modification

   // generation of strobes using data techniques 
   reg 			pe_clk_div_2;
   reg 			ne_clk_div_2;
   reg 			dqs_clk;

   always @(posedge ck_t)
     pe_clk_div_2 <= ~pe_clk_div_2;
   
   always @(negedge ck_t)
     ne_clk_div_2 <= pe_clk_div_2;

   always @(*)
     dqs_clk <= pe_clk_div_2 ^ ne_clk_div_2; // this is a data signal that looks like the clock (guaranteed to be in phase)
   
   
   reg                      dqs_out_internal;
   wire                     dqs_out_delayed;
   wire                     dqs_out;

   reg                      dqs_oe_internal;
   wire                     dqs_oe_delayed;

   reg [DATA_BITS-1:0]      dq_out_internal;
   wire [DATA_BITS-1:0]     dq_out_delayed;

   reg                      dq_oe_internal;
   wire                     dq_oe_delayed;

   // ********************************************************************************
   // This logic allows an output delay to be added to the read DQ/DQS signals to
   // simulate tDQSCK relative to the programmed latency.
   // controlled by carbon_current_READ_DELAY which is used to add read delay in
   // 1/2 clock cycle increments.  Defaults to 0, connected to SoCD parameter.

   carbon_variable_delay_element #(DATA_BITS) dq_out_delay(.clk(ck_t),
                                                           .select(carbon_current_READ_DELAY),
                                                           .in(dq_out_internal),
                                                           .out(dq_out_delayed));

   carbon_variable_delay_element #(1) dq_out_oe_delay(.clk(ck_t),
                                                      .select(carbon_current_READ_DELAY),
                                                      .in(dq_oe_internal),
                                                      .out(dq_oe_delayed));


   carbon_variable_delay_element #(1) dqs_out_delay(.clk(ck_t),
                                                    .select(carbon_current_READ_DELAY),
                                                    .in(dqs_out_internal),
                                                    .out(dqs_out_delayed));

   carbon_variable_delay_element #(1) dqs_out_oe_delay(.clk(ck_t),
                                                       .select(carbon_current_READ_DELAY),
                                                       .in(dqs_oe_internal),
                                                       .out(dqs_oe_delayed));

   // ****************************************
   // This logic changes the relation between output DQ and DQS, allowing DQS to be shifted
   // 1/2 clock cycle to convert from EARLY DQS to LATE DQS which can be used to directly
   // clock the data. Note that only DQS is delayed

   carbon_retiming_element #(1, DQS_OUT_DELAY) dqs_out_retimer(ck_t, dqs_out_delayed, dqs_out);

   // ****************************************
   // generate the bidirectional output signals

   assign dqs_oe = dqs_oe_delayed; // dqs oe timing should not change, even though dqs out timing does by 1/2 cycle

   assign dqs_t = dqs_oe_delayed ? { DM_BITS {  dqs_out }} : { DM_BITS { 1'bz } };
   assign dqs_c = dqs_oe_delayed ? { DM_BITS { ~dqs_out }} : { DM_BITS { 1'bz } };

   assign dq = dq_oe_delayed ? dq_out_delayed : { DATA_BITS { 1'bz } };


   pulldown dqs_t_pd[DM_BITS-1:0](dqs_t);
   pulldown dm_pd[DM_BITS-1:0](dm);
   
   // ********************************************************************************
   // Bidirectional signals input path

   // NOTE:  Only a single dqs signal is used internally!
   // general form for generating internal write dqs (DM should provide final qualification of data)
   wire                     dqs_in = |dqs_t & !dqs_oe; // ignore dqs that we generate
   // alternate form using lsb for generating internal write dqs - just use a single bit
   // wire                  dqs_in = dqs_t[0] & !dqs_oe; // ignore dqs that we generate

   wire                     dqs_in_internal;
   wire                     dqs_in_delayed;

   wire [DM_BITS-1:0]       dm_in = dm;
   wire [DM_BITS-1:0]       dm_in_delayed;
   wire [DATA_BITS-1:0]     dq_in = dq;
   wire [DATA_BITS-1:0]     dq_in_delayed;

   // ********************************************************************************
   // This logic allows an input delay to be added to the read DQ/DM/DQS signals to
   // simulate tDQSCK relative to the programmed latency.

   // CARBON_WRITE_DELAY select is used add write delay in 1/2 clock cycle increments
   // defaults to 0 and is only be set through runtime deposits
   // currently this is NOT exposed as a SoCD parameter

   reg [3:0]                CARBON_WRITE_DELAY; // carbon forceSignal
                                                // carbon observeSignal

   initial begin
      CARBON_WRITE_DELAY = 0;
   end

   carbon_variable_delay_element #(1) dqs_in_delay(.clk(ck_t),
                                                   .select(CARBON_WRITE_DELAY),
                                                   .in(dqs_in_internal),
                                                   .out(dqs_in_delayed));

   carbon_variable_delay_element #(DM_BITS) dm_in_delay(.clk(ck_t),
                                                        .select(CARBON_WRITE_DELAY),
                                                        .in(dm_in),
                                                        .out(dm_in_delayed));

   carbon_variable_delay_element #(DATA_BITS) dq_in_delay(.clk(ck_t),
                                                          .select(CARBON_WRITE_DELAY),
                                                          .in(dq_in),
                                                          .out(dq_in_delayed));

   // ****************************************
   // This logic changes the relation between input DQ and DQS, allowing DQS to be shifted
   // 1/2 clock cycle to convert from EARLY DQS to LATE DQS which can be used to directly
   // clock the data.

   carbon_retiming_element #(1, DQS_IN_DELAY) dqs_in_retimer(ck_t, dqs_in, dqs_in_internal);

   // ********************************************************************************
   // ********************************************************************************
   // Current Input Command Processing

   // ****************************************
   // These are helper wires/regs that reflect the current state during input command processing
   //

   reg [9:0] ca_pe; // pe = positive edge
   reg       cs_n_pe;
   reg       cke_pe;

   // sample and decode posedge CA pins and CS_N
   always @(posedge ck_t) begin
      ca_pe <= ca;
      cs_n_pe <= cs_n;
      cke_pe <= cke;
   end

   reg [9:0] ca_ne; // ne = negative edge

   // sample and decode negedge CA pins
   always @(negedge ck_t) begin
      ca_ne <= ca;
   end

   // The actual command as a concatenation of the appropriate signals (follows JEDEC standard doc)
   wire [4:0]               cmd = {cs_n_pe, ca_pe[0], ca_pe[1], ca_pe[2], ca_pe[3]};
   reg [4:0]                last_valid_cmd;
   reg [4:0]                early_last_valid_cmd;

   // MRW/MRR commands
   wire [7:0]               ma = {ca_ne[1:0], ca_pe[9:4]};
   wire [7:0]               op = ca_ne[9:2];

   wire [2:0]               ba = ca_pe[9:7];
   wire [14:0]              row = {ca_ne[9:8], ca_pe[6:2], ca_ne[7:0]};

   wire [11:0]              col_addr = {ca_ne[9:1], ca_pe[6:5], 1'b0}; // col[0] is not sent on bus and is implied to be zero - explicitly set it here
   wire                     ap = ca_ne[0]; // autoprecharge

   wire                     ab = ca_pe[4]; // all banks indication for precharge command

`ifdef CARBON_LPDDR2_MODEL
   // LPDDR2 has programmable burst lengths, burst types, and wrap types
   // decode the burst length and type from MR_reg[1] setting
   reg [2:0]                     bl;
   reg                           bt;
   reg                           wc;

   always @(*) begin
      bl = MR_reg[1] & 3'b111;
      bt = (MR_reg[1] & 4'b1000) ? 1 : 0;
      wc = (MR_reg[1] & 5'b10000) ? 1 : 0;
   end

   // decode the read/write latency from MR_reg[2] setting
   reg [4:0]                     read_latency;
   reg [4:0]                     write_latency;
   wire [4:0]                    cmd_latency;
   assign cmd_latency = (cmd[4:1] == `CMD_WRITE) ? write_latency:read_latency;

   always @(*) begin
      case (MR_reg[2] & 4'hf)
        4'b0001: begin
           read_latency = 3;
           write_latency = 1;
        end
        4'b0010: begin
           read_latency = 4;
           write_latency = 2;
        end
        4'b0011: begin
           read_latency = 5;
           write_latency = 2;
        end
        4'b0100: begin
           read_latency = 6;
           write_latency = 3;
        end
        4'b0101: begin
           read_latency = 7;
           write_latency = 4;
        end
        4'b0110: begin
           read_latency = 8;
           write_latency = 4;
        end
        default: begin
           read_latency = 3;
           write_latency = 1;
        end
      endcase
   end
`endif

`ifdef CARBON_LPDDR3_MODEL
   // LPDDR3 has fixed values for burst length, burst type, and wrap type
   wire [2:0] bl = `vBL8;
   wire       bt = `vBT_SEQ;
   wire       wc = 0;

   // decode the read/write latency from MR_reg[2] setting
   reg [4:0]                     read_latency;
   reg [4:0]                     write_latency;
   wire [4:0]                    cmd_latency;
   assign cmd_latency = (cmd[4:1] == `CMD_WRITE) ? write_latency:read_latency;

   always @(*) begin
      case (MR_reg[2] & 8'h4f)
	// WL Set A
        8'b00000001: begin
           read_latency = 3;
           write_latency = 1;
        end
        8'b00000100: begin
           read_latency = 6;
           write_latency = 3;
        end
        8'b00000110: begin
           read_latency = 8;
           write_latency = 4;
        end
        8'b00000111: begin
           read_latency = 9;
           write_latency = 5;
        end
        8'b00001000: begin
           read_latency = 10;
           write_latency = 6;
        end
        8'b00001001: begin
           read_latency = 11;
           write_latency = 6;
        end
        8'b00001010: begin
           read_latency = 12;
           write_latency = 6;
        end
	// WL Set B
        8'b01000001: begin
           read_latency = 3;
           write_latency = 1;
        end
        8'b01000100: begin
           read_latency = 6;
           write_latency = 3;
        end
        8'b01000110: begin
           read_latency = 8;
           write_latency = 4;
        end
        8'b01000111: begin
           read_latency = 9;
           write_latency = 5;
        end
        8'b01001000: begin
           read_latency = 10;
           write_latency = 8;
        end
        8'b01001001: begin
           read_latency = 11;
           write_latency = 9;
        end
        8'b01001010: begin
           read_latency = 12;
           write_latency = 9;
        end
        default: begin
           read_latency = 10;
           write_latency = 6;
        end
      endcase
   end
`endif



   // ****************************************
   // We need a queue of commands that are pending for upcoming clock
   // cycles. Input commands are captured and all relevant data is placed in a queue
   // for later processing (after the required latency) when the data is available
   // for writing or needs to be driven for reading. The double data rate aspect will
   // be handled when the command is processed.

   // The queue operates on a system clock, so a single burst command will put multiple commands
   // in the queue. For example, with a burst length of 4, a there will be two
   // cycles on which data is transferred.  In this case, two entries
   // will be inserted into the queue, at offsets N and N+1, where N is
   // the initial latency for the command.
   //

   parameter CMD_QUEUE_DEPTH = 32;

   // ****************************************
   // Command state information

   reg [4:0]                queue_ptr; // Current location in circular queue
   reg 			    preamble_q [0:CMD_QUEUE_DEPTH-1]; // preamble
   reg [4:0]                command_q [0:CMD_QUEUE_DEPTH-1]; // command
   reg [MAX_ADDR_BITS-1:0]  addr_q [0:CMD_QUEUE_DEPTH-1]; // combined bank, row & column address
   reg 			    preamble_immediate;

   initial
     preamble_immediate = 0;
   
   parameter MAX_NUM_BANKS = 1 << `MAX_BA_BITS;
   reg [`MAX_ROW_BITS-1:0]      row_addr[0:MAX_NUM_BANKS-1]; // row address captured during bank activation
   reg [7:0]                    needs_precharge; // state info for bank activation precharge checking

   // Combined address is the current bank, the saved row for the selected bank and the current column
   reg [MAX_ADDR_BITS-1:0] 	comb_addr;

   // Combined address is the current bank, the saved row for the selected bank and the current column
   always @(*) begin
      if ( carbon_row_bank_column_format )
	 begin
//         comb_addr = { row_addr[(ba & carbon_current_BA_BITS_mask)], (ba & carbon_current_BA_BITS_mask), col_addr};
	    comb_addr = ((row_addr[(ba & carbon_current_BA_BITS_mask)] & carbon_current_ROW_BITS_mask) << (carbon_current_BA_BITS+ carbon_current_COL_BITS)) |
			( (ba                                          & carbon_current_BA_BITS_mask) << (carbon_current_COL_BITS) ) |
			( (col_addr                                    & carbon_current_COL_BITS_mask) << 0 );
	 end
      else
	begin
//           comb_addr = { (ba & carbon_current_BA_BITS_mask), row_addr[(ba & carbon_current_BA_BITS_mask)], col_addr};
	    comb_addr = ( (ba                                           & carbon_current_BA_BITS_mask) << (carbon_current_ROW_BITS+ carbon_current_COL_BITS) ) |
			( (row_addr[(ba & carbon_current_BA_BITS_mask)] & carbon_current_ROW_BITS_mask) << (carbon_current_COL_BITS)) |
			( (col_addr                                     & carbon_current_COL_BITS_mask) << 0 );
	   
	end
   end


   // Registers for reporting errors to C++ interface
   reg [7:0]                    error_code; // carbon observeSignal
   reg [7:0]                    error_bank; // carbon observeSignal

`define COLLISION_ERROR 8'h10
`define PRECHARGE_ERROR 8'h20
`define ACTIVATE_ERROR  8'h30
`ifdef CARBON_LPDDR2_MODEL
 `define BST_ERROR       8'h40
`endif
   
   initial begin
      queue_ptr = 0;
      // invalidate the queue so false errors are not reported at startup
      for (i = 0; i < CMD_QUEUE_DEPTH; i = i + 1) begin
         preamble_q[i] = 0;
         command_q[i] = `CMD_INVALID;
         addr_q[i] = 0;
      end
      for (i = 0; i < MAX_NUM_BANKS; i = i + 1) begin
         row_addr[i] = 0;
      end

      needs_precharge = 0;  // don't check on startup
      error_code = 0;
      error_bank = 0;
   end

   task check_command_q;
      input [4:0] local_queue_ptr;
      begin
         if (command_q[local_queue_ptr] != `CMD_INVALID) begin
            // writes can interrupt other writes and reads can interrupt other reads, but that is it
            if (command_q[local_queue_ptr] != early_last_valid_cmd) begin
               error_code = `COLLISION_ERROR;
               error_bank = 0;
`ifndef CARBON
               $display("Error - command collision occurred\n");
`endif
               $stop;
               error_code = 0;
               error_bank = 0;
            end
         end
      end
   endtask

   task check_precharge;
      begin
         if (needs_precharge & (1 << (ba & carbon_current_BA_BITS_mask))) begin
            error_code = `PRECHARGE_ERROR;
            error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
            $display("Error - Activation of bank %d without precharge\n", (ba & carbon_current_BA_BITS_mask));
`endif
            $stop;
            error_code = 0;
            error_bank = 0;
         end
      end
   endtask

   task check_activated;
      begin
	 if (!(needs_precharge & (1 << (ba & carbon_current_BA_BITS_mask)))) begin
            error_code = `ACTIVATE_ERROR;
            error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
            $display("Error - Command issued to bank %d without prior activation\n", (ba & carbon_current_BA_BITS_mask));
`endif
            $stop;
            error_code = 0;
            error_bank = 0;
         end
      end
   endtask

   // ********************************************************************************
   // ********************************************************************************
   // Process input commands and either act on them or place them in queue for
   // processing after appropriate latency

   always @(negedge ck_t) begin
      if ((~cmd[4]) && (cmd[4:1] != `CMD_NOP)) begin
         early_last_valid_cmd <= cmd;
      end
   end

   always @(posedge ck_t) begin
      profile_mrw <= 0;
      profile_mrr <= 0;
      profile_activate <= 0;
      profile_read <= 0;
      profile_write <= 0;
      profile_bst <= 0;
      profile_precharge <= 0;
      profile_refresh <= 0;
      profile_read_data <= 0;
      profile_write_data <= 0;

      if (cke_pe) begin
         if ((~cmd[4]) && (cmd[4:1] != `CMD_NOP)) begin
            last_valid_cmd <= cmd;
         end
         if (cmd == `CMD_MRW) begin
	    profile_mrw <= 1;
            // Is this a MRW Reset?
            if (ma == 63) begin
               initialize_MR_reg;
            end
            else begin
               // Immediately store in the appropriate mode register
               MR_reg[ma] = op;
            end
         end
         if (cmd == `CMD_MRR) begin
	    profile_mrr <= 1;
            // This is always a burst length of 4
            check_command_q(queue_ptr + read_latency + 5'h0);
            command_q[queue_ptr + read_latency + 5'h0] <= cmd;
            // for this command, put the register address in the address queue, along with bit to indicate which transfer cycle this is
            addr_q[queue_ptr + read_latency + 5'h0] <= { {(MAX_ADDR_BITS-6){1'b0}}, 1'b0, ma };
            check_command_q(queue_ptr + read_latency + 5'h1);
            command_q[queue_ptr + read_latency + 5'h1] <= cmd;
            addr_q[queue_ptr + read_latency + 5'h1] <= { {(MAX_ADDR_BITS-6){1'b0}}, 1'b1, ma };
         end
         else if (cmd == `CMD_REF_BANK) begin
	    profile_refresh <= 1 << (ba & carbon_current_BA_BITS_mask);
	 end
         else if (cmd == `CMD_REF_ALL) begin
	    profile_refresh <= (1 << (carbon_current_BA_BITS_mask+1)) - 1;
	 end
         else if (cmd == `CMD_PRECHARGE) begin
            if (ab) begin
	       profile_precharge <= (1 << (carbon_current_BA_BITS_mask+1)) - 1; // all banks
               needs_precharge <= 0;
	    end
            else begin
	       profile_precharge <= 1 << (ba & carbon_current_BA_BITS_mask);
               needs_precharge <= needs_precharge & (8'hff ^ ( 1 << (ba & carbon_current_BA_BITS_mask)));
	    end
         end
         else if (cmd[4:2] == `CMD_ACTIVATE) begin
	    profile_activate <= 1 << (ba & carbon_current_BA_BITS_mask);
            check_precharge; // Check that the bank is precharged
            row_addr[(ba & carbon_current_BA_BITS_mask)] <= (row & carbon_current_ROW_BITS_mask);  // Save the row address
            needs_precharge <= needs_precharge | (1 << (ba & carbon_current_BA_BITS_mask));
         end
         else if ((cmd[4:1] == `CMD_WRITE) || (cmd[4:1] == `CMD_READ)) begin
	    if (cmd[4:1] == `CMD_WRITE)
	      profile_write <= 1 << (ba & carbon_current_BA_BITS_mask);
	    if (cmd[4:1] == `CMD_READ)
	      profile_read <= 1 << (ba & carbon_current_BA_BITS_mask);

            check_activated; // Check that the bank is activated

            // The following cases will add new commands to the queue.
            // There will be 2 entries for vBL4, 4 entries for vBL8, and 8 entries for vBL16 for LPDDR2.
            // We need to add 1 to each index, because we're updating the queue pointer
            // as well this cycle.
            //
            // A base address is saved for the two pairs of data transferred each cycle.
            // The actual address for each data element will be determined when the entry
            // is extracted from the queue.  For details on the address generation, see
            // Table 21 in section 3.5.1 of the LPDDR2 spec.
            //

            // Do each burst length separately as the addressing varies


`ifdef CARBON_LPDDR2_MODEL	    
            if (bl == `vBL4) begin
               // same for sequential or interleaved
               check_command_q(queue_ptr + cmd_latency + 5'h0);
               command_q[queue_ptr + cmd_latency + 5'h0] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h0] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), comb_addr[1:0] + 2'b00 };

               check_command_q(queue_ptr + cmd_latency + 5'h1);
               command_q[queue_ptr + cmd_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h1] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), comb_addr[1:0] + 2'b10 };
            end
`endif //  `ifdef CARBON_LPDDR2_MODEL
	    
	    // LPDDR3 only supports vBL8 sequential
            if (bl == `vBL8) begin
`ifdef CARBON_LPDDR3_MODEL
	       if (write_preamble_enabled)
		 begin
		    if ((cmd[4:1] == `CMD_WRITE) && (command_q[queue_ptr + cmd_latency - 5'h1] == `CMD_INVALID))  // note cmd_latency = 1 is a special case!
		      begin
			 // this is a write preamble and the command slot is open
			 preamble_q[queue_ptr + cmd_latency - 5'h1] <= 1;
			 command_q[queue_ptr + cmd_latency - 5'h1] <= cmd;
			 addr_q[queue_ptr + cmd_latency - 5'h1] <= 0;  // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		      end
		    else
		      if ((cmd[4:1] == `CMD_WRITE) && (command_q[queue_ptr + cmd_latency - 5'h1] != `CMD_WRITE))
			begin
			   // perform check to force error if preamble was requested on active command slot that was not a write (to allow seamless writes)
			   check_command_q(queue_ptr + cmd_latency - 5'h1);  
			end
		 end
`endif
               if (bt == `vBT_SEQ) begin
                  // sequential
                  check_command_q(queue_ptr + cmd_latency + 5'h0);
                  command_q[queue_ptr + cmd_latency + 5'h0] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h0] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b000 };

                  check_command_q(queue_ptr + cmd_latency + 5'h1);
                  command_q[queue_ptr + cmd_latency + 5'h1] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h1] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b010 };

                  check_command_q(queue_ptr + cmd_latency + 5'h2);
                  command_q[queue_ptr + cmd_latency + 5'h2] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h2] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b100 };

                  check_command_q(queue_ptr + cmd_latency + 5'h3);
                  command_q[queue_ptr + cmd_latency + 5'h3] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h3] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b110 };
               end
               else begin
                  // interleaved
                  check_command_q(queue_ptr + cmd_latency + 5'h0);
                  command_q[queue_ptr + cmd_latency + 5'h0] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h0] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] ^ 3'b000 };

                  check_command_q(queue_ptr + cmd_latency + 5'h1);
                  command_q[queue_ptr + cmd_latency + 5'h1] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h1] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] ^ 3'b010 };

                  check_command_q(queue_ptr + cmd_latency + 5'h2);
                  command_q[queue_ptr + cmd_latency + 5'h2] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h2] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] ^ 3'b100 };

                  check_command_q(queue_ptr + cmd_latency + 5'h3);
                  command_q[queue_ptr + cmd_latency + 5'h3] <= cmd;
                  addr_q[queue_ptr + cmd_latency + 5'h3] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] ^ 3'b110 };
               end
            end

`ifdef CARBON_LPDDR2_MODEL	    
            if (bl == `vBL16) begin
               // always sequential
               check_command_q(queue_ptr + cmd_latency + 5'h0);
               command_q[queue_ptr + cmd_latency + 5'h0] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h0] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'h0 };

               check_command_q(queue_ptr + cmd_latency + 5'h1);
               command_q[queue_ptr + cmd_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h1] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'h2 };

               check_command_q(queue_ptr + cmd_latency + 5'h2);
               command_q[queue_ptr + cmd_latency + 5'h2] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h2] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'h4 };

               check_command_q(queue_ptr + cmd_latency + 5'h3);
               command_q[queue_ptr + cmd_latency + 5'h3] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h3] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'h6 };

               check_command_q(queue_ptr + cmd_latency + 5'h4);
               command_q[queue_ptr + cmd_latency + 5'h4] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h4] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'h8 };

               check_command_q(queue_ptr + cmd_latency + 5'h5);
               command_q[queue_ptr + cmd_latency + 5'h5] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h5] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'ha };

               check_command_q(queue_ptr + cmd_latency + 5'h6);
               command_q[queue_ptr + cmd_latency + 5'h6] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h6] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'hc };

               check_command_q(queue_ptr + cmd_latency + 5'h7);
               command_q[queue_ptr + cmd_latency + 5'h7] <= cmd;
               addr_q[queue_ptr + cmd_latency + 5'h7] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>4), comb_addr[3:0] + 4'he };
            end
`endif //  `ifdef CARBON_LPDDR2_MODEL
	    
            // Auto-precharge option
            if (ap) begin
	       profile_precharge <= 1 << (ba & carbon_current_BA_BITS_mask);
               needs_precharge <= needs_precharge & (8'hff ^ (1 << (ba & carbon_current_BA_BITS_mask)));
            end
         end
`ifdef CARBON_LPDDR2_MODEL
         else if (cmd == `CMD_BST) begin
	    profile_bst <= 1 << (ba & carbon_current_BA_BITS_mask);
            // Note:  The timing rules for S2 vs S4 BST commands are not checked.  Some checking is done though, such as
            // not in a read/write command or back to back BST commands
            if (last_valid_cmd[4:1] == `CMD_READ) begin
               // After the read latency, invalidate the queue entries - just use the maximum
               command_q[queue_ptr + read_latency + 5'h0] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h1] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h2] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h3] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h4] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h5] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h6] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h7] <= `CMD_INVALID;
            end
            else if (last_valid_cmd[4:1] == `CMD_WRITE) begin
               // After the write latency, invalidate the queue entries - just use the maximum
               command_q[queue_ptr + write_latency + 5'h0] <= `CMD_INVALID;
               command_q[queue_ptr + write_latency + 5'h1] <= `CMD_INVALID;
               command_q[queue_ptr + write_latency + 5'h2] <= `CMD_INVALID;
               command_q[queue_ptr + write_latency + 5'h3] <= `CMD_INVALID;
               command_q[queue_ptr + write_latency + 5'h4] <= `CMD_INVALID;
               command_q[queue_ptr + write_latency + 5'h5] <= `CMD_INVALID;
               command_q[queue_ptr + write_latency + 5'h6] <= `CMD_INVALID;
               command_q[queue_ptr + write_latency + 5'h7] <= `CMD_INVALID;
            end
            else begin
               error_code = `BST_ERROR;
               error_bank = 0;
`ifndef CARBON
               $display("Error - BST command issued when not in a READ or WRITE command\n");
`endif
               $stop;
               error_code = 0;
               error_bank = 0;
            end
         end
`endif
	 // generate profiling info for data transfers on the DQ bus - this is based on current command expectations and not actual data arrival 
	 if (command_q[queue_ptr][4:1] == `CMD_READ)
	   profile_read_data <= 1;
	 
	 if ((command_q[queue_ptr][4:1] == `CMD_WRITE) && !preamble_q[queue_ptr])
	   profile_write_data <= 1;

         // Clear finished entries from the queue.  We only need to
         // invalidate the command and preamble.
         if (write_preamble_enabled)
	   preamble_q[queue_ptr] <= 0;
         command_q[queue_ptr] <= `CMD_INVALID;
         queue_ptr <= queue_ptr + 5'h1;
      end
   end

   always @(*)
     begin
	if ((write_preamble_enabled) && 
	    ((cmd[4:1] == `CMD_WRITE) && (command_q[queue_ptr + cmd_latency - 5'h1] == `CMD_INVALID) &&
	     (cmd_latency == 1)))  // note cmd_latency = 1 is a special case!
	  begin
	     preamble_immediate = 1; // signal that we must immediately start a preamble
	  end
	else
	  begin
	     preamble_immediate = 0;
	  end
     end
   
   
   // ********************************************************************************
   // ********************************************************************************
   // Deferred Command Processing


   // Here is where we process the commands that were queued for deferred execution - MODE REGISTER READ, READ, and WRITE
   // For MMR and READ, the values are read and the DQS strobes are generated when the command is extracted from the queue.
   // For WRITE operations though, the data arrival is dictated by the externally generated DQS which may be delayed relative 
   // to dequeueing the WRITE command at this stage. This means a second level of command queueing may be needed to accomodate
   // the delayed DQS/DQ write data which gets written to the actual underlying memory as it comes in.  Note that previously this
   // functionality was implemented in the underlying block memory in a distributed fashion, but this resulted in a bug where back
   // to back writes to different blocks would cause the second write to be written to the wrong address.
   // Note that the case where write data arrives on time (no queueing needed) must also be accomodated.

   
   // ********************************************************************************
   // Look at the head of the queue to determine what commands are currently ready for execution
   wire [4:0] curr_command = command_q[queue_ptr];
   wire [MAX_ADDR_BITS:0] curr_addr = addr_q[queue_ptr]; // Note this is 1 larger than it needs to be to avoid compiler warning on curr_addr[] below
   wire [BLOCK_BITS-1:0]         curr_block = (NUM_BLOCKS > 1) ? curr_addr[BLOCK_ADDR_BITS+BLOCK_BITS:BLOCK_ADDR_BITS] : 0;

   // Addressing mode - sequential or interleaved
   wire                          seq_addr = (bt == `vBT_SEQ) ? 1 : 0;

   // ********************************************************************************
   // MRR/READ processing
   
   // block read address
   reg [BLOCK_ADDR_BITS-1:0]     block_raddr_1;
   reg [BLOCK_ADDR_BITS-1:0]     block_raddr_2;
   // array of memory block outputs (indexed by block number)
   reg [DATA_BITS-1:0]           block_dout_1[0:NUM_BLOCKS-1];
   reg [DATA_BITS-1:0]           block_dout_2[0:NUM_BLOCKS-1];


   always @(*) begin
      block_raddr_1 = curr_addr[BLOCK_ADDR_BITS-1:0];
      block_raddr_2 = 'h0;
      dqs_out_internal = 1'b0;
      dqs_oe_internal = 1'b0;
      dq_out_internal = 'h0;
      dq_oe_internal = 'h0;
      if (curr_command == `CMD_MRR) begin
         // This is a Mode Register Read

         // check if this is a data calibration pattern or regular read register
         if (curr_addr[7:0] == 8'd32) begin
            // dqs_out_internal is the same as clock
            dqs_out_internal = dqs_clk;
            dq_out_internal = dqs_clk ? {DATA_BITS{1'b1}} : {DATA_BITS{1'b0}};
            dqs_oe_internal = 1;
            dq_oe_internal = 1;
         end
         else if (curr_addr[7:0] == 8'd40) begin
            // dqs_out_internal is the same as clock
            dqs_out_internal = dqs_clk;
            dq_out_internal = dqs_clk ? {DATA_BITS{curr_addr[8]}} : {DATA_BITS{curr_addr[8]}};
            dqs_oe_internal = 1;
            dq_oe_internal = 1;
         end
         else begin
            // dqs_out_internal is the same as clock
            dqs_out_internal = dqs_clk;
            if (curr_addr[8] == 0) begin
               dq_out_internal = dqs_clk ? {MR_reg[curr_addr[7:0]]} : {DATA_BITS{1'b0}};
            end
            else begin
               dq_out_internal = {DATA_BITS{1'b0}};
            end
            dqs_oe_internal = 1;
            dq_oe_internal = 1;
         end
      end // if (curr_command == `CMD_MRR)
      else if (curr_command[4:1] == `CMD_READ) begin
         // dqs_out_internal is the same as clock
         dqs_out_internal = dqs_clk;
         dqs_oe_internal = 1;
         dq_oe_internal = 1;
         // For details on the address generation, see the table in section 3.6.2 of
         // the spec.
         if (seq_addr) begin
            // Low address is one after the high address, but
            // we need to wrap on the 4-word boundary.  Note that not all burst length/type combinations
            // are valid by the LPDDR2 - we do not check for the validity but cover the valid cases
            block_raddr_2 = {curr_addr[BLOCK_ADDR_BITS-1:2], curr_addr[1:0] + 2'b01};
         end
         else begin
            // Interleaving is easy.  Just flip the low bit.
            block_raddr_2 = {curr_addr[BLOCK_ADDR_BITS-1:1], ~curr_addr[0]};
         end
         // Select high or low output from the correct block
         dq_out_internal = dqs_clk ? block_dout_1[curr_block] : block_dout_2[curr_block];
      end
   end


   // ********************************************************************************
   // WRITE processing

   // wires that directly drive the write port of the underlying memory block - these may be driven directly from
   // the deferred command decoding (if the data arrives at the nominal time) or from the
   // queued command decoding (if the data arrives late)

   // IMPORTANT:  For every command generated waclk strobe there must be a corresponding externally generated wdclk (write dqs) (coincident or later)
   // The waclk captures the address of a current or pending write while the wdclk captures the
   // actual data - if these do not have a one-to-one relation, the data will be written to the
   // wrong address.  This implementation allows flexibility in the timing of the data strobes
   // since they can be delayed relative to their nominal cycle time.
   // In general wptr and rptr should only be different when the data strobe is delayed relative to the
   // nominal cycle time.

   // interface signals to block memories
   reg [NUM_BLOCKS-1:0]          int_block_we;
   reg [BLOCK_ADDR_BITS-1:0]     int_block_waddr_1;
   reg [BLOCK_ADDR_BITS-1:0]     int_block_waddr_2;
   reg                           int_waclk;
   reg 				 int_preamble;

   // Generate the write enable strobe and write addresses values for the blocks
   // Note that if this is a preamble, no WE in generated but a FIFO entry is generated (since the preamble DQS input will cause the FIFO to be poppped)
   always @(*) begin
      int_waclk = 1;
      int_block_we = 'h0;
      int_block_waddr_1 = curr_addr[BLOCK_ADDR_BITS-1:0];
      int_block_waddr_2 = 'h0;
      int_preamble = 0;
      if ((preamble_immediate) || (curr_command[4:1] == `CMD_WRITE)) begin
         // Generate write enable to the correct block
         int_waclk = dqs_clk;

	 // Is this a actual write or a preamble?
	 if (preamble_immediate || preamble_q[queue_ptr])
           int_block_we = { NUM_BLOCKS{1'b0} }; // A preamble does not write any data
	 else
           int_block_we = 1'b1 << curr_block; // Select the corrent block WE

         // For details on the address generation, see the table in section 3.6.2 of
         // the spec.
         if (seq_addr) begin
            // Low address is one after the high address, but
            // we need to wrap on the 4-word boundary.  Note that not all burst length/type combinations
            // are valid by the LPDDR2 - we do not check for the validity but cover the valid cases
            int_block_waddr_2 = {curr_addr[BLOCK_ADDR_BITS-1:2], curr_addr[1:0] + 2'b01};
         end
         else begin
            // Interleaving is easy.  Just flip the low bit.
            int_block_waddr_2 = {curr_addr[BLOCK_ADDR_BITS-1:1], ~curr_addr[0]};
         end
      end
   end

   // FIFO to store current address and block write enable vector for the next DQS strobe received
   // These pointers must track as they correspond to waclk and wdclk strobes
   reg [3:0] 			 wptr; 
   reg [3:0] 			 rptr;
   reg [NUM_BLOCKS-1:0] 	 fifo_block_we[0:15];
   reg [BLOCK_ADDR_BITS-1:0] 	 fifo_block_waddr_1[0:15];
   reg [BLOCK_ADDR_BITS-1:0] 	 fifo_block_waddr_2[0:15];

   initial begin
      wptr = 0;
      rptr = 0;
   end

   always @(posedge int_waclk or posedge internal_reset)
     if (internal_reset)
       begin
	  wptr <= 0;
       end
     else
       begin
	  fifo_block_we[wptr] <= int_block_we;
	  fifo_block_waddr_1[wptr] <= int_block_waddr_1;
	  fifo_block_waddr_2[wptr] <= int_block_waddr_2;
	  wptr <= wptr + 1;
       end
   

   // ********************************************************************************
   // Write data signals that are processed immediately as it arrives from external DQ/DQS/DM

   reg    external_dqs_expected;  // carbon observeSignal
   assign carbon_external_dqs_expected = external_dqs_expected;
   
   always @(*) begin
      if ((preamble_immediate) || (curr_command[4:1] == `CMD_WRITE) || (wptr != rptr))
	external_dqs_expected <= ~internal_reset;
      else
	external_dqs_expected <= 0;
   end
   
   
   wire block_wdclk = ~dqs_in_delayed;  // Simplified block_wdclk generation to work around CMS issues

   // always pop the FIFO when an external DQS is seen
   always @(posedge block_wdclk or posedge internal_reset)
     if (internal_reset)
       rptr <= 0;
     else if (external_dqs_expected)
       rptr <= rptr + 1;
   
   // Build the current write mask based on current dm value
   wire [DATA_BITS-1:0] write_mask;
   carbon_dm_to_dmask #(.DM_BITS(DM_BITS), .DATA_BITS(DATA_BITS)) dmask(dm_in_delayed, write_mask);

   // Capture first half of data transfer (and mask) using rising edge of dqs
   // This value and the second half will be written to memory on falling edge of dqs
   reg [DATA_BITS-1:0] dq_in_delayed_r;
   reg [DATA_BITS-1:0] write_mask_r;
   
   always @(posedge dqs_in_delayed) begin
      dq_in_delayed_r <= dq_in_delayed;
      write_mask_r <= write_mask;
   end

   reg [DATA_BITS-1:0] block_din_1;   // carbon observeSignal
   reg [DATA_BITS-1:0] block_din_2;   // carbon observeSignal
   reg [DATA_BITS-1:0] block_dmask_1; // carbon observeSignal
   reg [DATA_BITS-1:0] block_dmask_2; // carbon observeSignal
   always @(*) begin
      // First write data is what we registered earlier.  Second write data is
      // what's on the bus now.
      block_din_1 = dq_in_delayed_r;
      block_din_2 = dq_in_delayed;
      block_dmask_1 = write_mask_r;
      block_dmask_2 = write_mask;
   end

   
   // ********************************************************************************
   // Drive the block memory with either the direct input (when data comes at nominal timing)
   // or the FIFO outpu (when data is late) 
   
   reg [NUM_BLOCKS-1:0] 	 block_we;
   reg [BLOCK_ADDR_BITS-1:0] 	 block_waddr_1;
   reg [BLOCK_ADDR_BITS-1:0] 	 block_waddr_2;

   always @(*) begin
      if (wptr == rptr) begin
	 // use the signals directly
	 block_we <= int_block_we & {NUM_BLOCKS{external_dqs_expected}}; // Only allow writes when we are expecting them
	 block_waddr_1 <= int_block_waddr_1;
	 block_waddr_2 <= int_block_waddr_2;
      end
      else begin
	 // use the FIFO value
	 block_we <= fifo_block_we[rptr] & {NUM_BLOCKS{external_dqs_expected}}; // Only allow writes when we are expecting them
	 block_waddr_1 <= fifo_block_waddr_1[rptr];
	 block_waddr_2 <= fifo_block_waddr_2[rptr];
      end
   end

   
   // ********************************************************************************
   // ********************************************************************************

   // Since this is DDR, we need to read/write two words (at different addresses) each clock
   // cycle.  We'll call these 1 and 2, referring to their order on the data bus.
   // This has no relationship to the addresses.  We always read/write both together,
   // so only one write enable is needed.
   //
   // For writes, the high data is registered on the posedge of internal dqs, and both values are written
   // on the negedge of the internal dqs.
   //

   // ********************************************************************************
   // ********************************************************************************

   genvar                          g;
   generate
      for (g = 0; g < NUM_BLOCKS; g = g + 1) begin : block
         wire [DATA_BITS-1:0]      block_dout_wire_1;
         wire [DATA_BITS-1:0]      block_dout_wire_2;

         always @(*)
           block_dout_1[g] <= block_dout_wire_1;

         always @(*)
           block_dout_2[g] <= block_dout_wire_2;

         carbon_ddr_block_mem #(
				.ADDR_WIDTH(BLOCK_ADDR_BITS), 
				.DATA_BITS(DATA_BITS)
				) block_mem(.wdclk(block_wdclk),
                                            .we(block_we[g]),
                                            .raddr_a(block_raddr_1),
                                            .waddr_a(block_waddr_1),
                                            .din_a(block_din_1),
                                            .dmask_a(block_dmask_1),
                                            .dout_a(block_dout_wire_1),
                                            .raddr_b(block_raddr_2),
                                            .waddr_b(block_waddr_2),
                                            .din_b(block_din_2),
                                            .dmask_b(block_dmask_2),
                                            .dout_b(block_dout_wire_2));

      end
   endgenerate

endmodule


