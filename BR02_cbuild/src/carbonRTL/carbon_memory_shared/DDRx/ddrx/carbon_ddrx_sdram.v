//******************************************************************************
// Copyright (c) 2010-2014 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// Parameterized model of DDR2/3 memory for general use in Carbon compiler environments.  
// This is a minimal implementation, excluding things like refresh and timing error checking.  
// Interface DQS retiming options are provided to help with the remodeling of the PHY interface.

// enable this parameter to help debug row/bank/col address issues - model must be compiled with -enableOutputSysTasks cbuild option
// `define CARBON_DEBUG_ADDRESSES 1

// This model MUST be configured for a specific protocol by defining ONE AND ONLY ONE of the following either
// at the command line level or by uncommenting one of these lines
// `define CARBON_DDR_MODEL
// `define CARBON_DDR2_MODEL
// `define CARBON_DDR3_MODEL
// `define CARBON_DDR4_MODEL

// combo macro for common code
`undef CARBON_DDR3_OR_DDR4_MODEL
`ifdef CARBON_DDR4_MODEL
`define CARBON_DDR3_OR_DDR4_MODEL
`endif
`ifdef CARBON_DDR3_MODEL
`define CARBON_DDR3_OR_DDR4_MODEL
`endif

// this model is defined such that it can support many different memory size
// configurations, however the maximum sizes are defined by the following:
`define MAX_BA_BITS 4   // DDR4 has BG[1:0], BA[1:0] that comprise the BA
`define MAX_ROW_BITS 17  
`define MAX_COL_BITS 15


`timescale 1ps / 1ps

// ****************************************
// Supported Commands 
// {cs_n, ras_n, cas_n, we_n}
`define CMD_INVALID       4'b1111
`define CMD_MODE_REG      4'b0000
`define CMD_REFRESH       4'b0001
`define CMD_PRECHARGE     4'b0010
`define CMD_ACTIVATE      4'b0011  // this is handled differently for DDR4
`define CMD_WRITE         4'b0100
`define CMD_READ          4'b0101
`define CMD_NOP           4'b0111
`ifdef CARBON_DDR_MODEL
`define CMD_BST           4'b0110
`endif

`ifdef CARBON_DDR4_MODEL
// ****************************************
// Supported DDR4 Register Fields/Values

// MR0
`define MR0_BURST_LENGTH 1:0
`define vMR0_BURST_LENGTH_8 2'b00
`define vMR0_BURST_LENGTH_BC4or8 2'b01
`define vMR0_BURST_LENGTH_BC4 2'b10
`define MR0_CAS_LATENCY_LSB 2
`define MR0_READ_BURST_TYPE 3
`define vMR0_RBT_NIBBLE_SEQ 1'b0
`define vMR0_RBT_INTERLEAVE 1'b1
`define MR0_CAS_LATENCY_MSB 6:4

// MR1
`define MR1_ADDITIVE_LATENCY 4:3

// MR2
`define MR2_CAS_WRITE_LATENCY 5:3
`define MR2_WRITE_CRC 12

// MR3
`define MR3_MPR_LOCATION 1:0
`define MR3_MPR_ENABLE 2
`define MR3_WRITE_CMD_LATENCY_WITH_CRC_AND_DM 10:9

// MR4
`define MR4_CS_LATENCY 8:6
`define MR4_RD_PREAMBLE_TRAINING_MODE 10
`define MR4_RD_PREAMBLE_LENGTH 11
`define MR4_WR_PREAMBLE_LENGTH 12

// MR5
`define MR5_PARITY_LATENCY_MODE 2:0
`define MR5_DM_ENABLE 10
`define MR5_WR_DBI_ENABLE 11
`define MR5_RD_DBI_ENABLE 12

// MR6
// not used

`endif

`ifdef CARBON_DDR3_MODEL
// ****************************************
// Supported DDR3 Register Fields/Values

`define MR0_BURST_LENGTH 1:0
`define vMR0_BURST_LENGTH_8 2'b00
`define vMR0_BURST_LENGTH_BC4or8 2'b01
`define vMR0_BURST_LENGTH_BC4 2'b10
`define MR0_CAS_LATENCY_MSB 2
`define MR0_READ_BURST_TYPE 3
`define vMR0_RBT_NIBBLE_SEQ 1'b0
`define vMR0_RBT_INTERLEAVE 1'b1
`define MR0_CAS_LATENCY 6:4
// `define MR0_TEST_MODE 7
// `define MR0_DLL_RESET 8
// `define MR0_WRITE_RECOVERY 11:9
// `define MR0_POWER_DOWN 12
// `define MR1_DLL_ENABLE 0
// `define MR1_DIC_LOW 1
// `define MR1_RTT_LOW 2
`define MR1_ADDITIVE_LATENCY 4:3
// `define MR1_DIC_HIGH 5
// `define MR1_RTT_MID 6
// `define MR1_WRITE_LEVELING_ENABLE 7
// `define MR1_RTT_HIGH 9
// `define MR1_TDQS_ENABLE 11
// `define MR1_QOFF 12
// `define MR2_PASR 2:0
`define MR2_CAS_WRITE_LATENCY 5:3
// `define MR2_ASR 6
// `define MR2_SRT 7
// `define MR2_RTT_WR 10:9
`define MR3_MPR_LOCATION 1:0
`define MR3_MPR_ENABLE 2
`endif

`ifdef CARBON_DDR2_MODEL
// ****************************************
// Supported DDR2 Register Fields/Values

`define MR_BURST_LENGTH 2:0
`define vMR_BURST_LENGTH_BL4 3'b010
`define vMR_BURST_LENGTH_BL8 3'b011
`define MR_BURST_TYPE 3
`define vMR_BURST_TYPE_SEQ 1'b0
`define vMR_BURST_TYPE_INT 1'b1
`define MR_CAS_LATENCY 6:4
// `define MR_TEST_MODE 7
// `define MR_DLL_RESET 8
// `define MR_WRITE_RECOVERY 11:9
// `define MR_POWER_DOWN 12
// `define EMR1_DLL_ENABLE 0
// `define EMR1_DRIVER_IMPEDANCE_CONTROL 1
// `define EMR1_RTT_LOW 2
`define EMR1_ADD_LATENCY 5:3
// `define EMR1_RTT_HIGH 6
// `define EMR1_OCD_CALIBRATION 7:9
// `define EMR1_DQS_BAR_DISABLE 10
// `define EMR1_RDQS_ENABLE 11
// `define EMR1_QOFF
// `define EMR2_PASR 2:0
// `define EMR2_DCC_ENABLE 3
// `define EMR2_SRF_ENABLE 7
`endif

`ifdef CARBON_DDR_MODEL
// ****************************************
// Supported DDR Register Fields/Values

`define MR_BURST_LENGTH 2:0
`define vMR_BURST_LENGTH_BL2 3'b001
`define vMR_BURST_LENGTH_BL4 3'b010
`define vMR_BURST_LENGTH_BL8 3'b011
`define MR_BURST_TYPE 3
`define vMR_BURST_TYPE_SEQ 1'b0
`define vMR_BURST_TYPE_INT 1'b1
`define MR_CAS_LATENCY 6:4
`endif

// ********************************************************************************
// Top Level DDRx Memory Subsystem

module carbon_ddrx_sdram(
`ifdef CARBON_DDR4_MODEL
                         act_n,
			 reset_n,
                         dqsbar,
			 par,  // unused!
			 alert_n,  // unused!
`endif
`ifdef CARBON_DDR3_MODEL
                         reset_n,
                         dqsbar,
`endif
                         ck,
                         ckbar, // unused!
                         cke,
                         cs_n,
                         ras_n,
                         cas_n,
                         we_n,
                         dm,
                         ba,
                         addr,
                         dq,
                         dqs
                         );

   // number of chip selects (ranks) and data width
   parameter CS_BITS = 4;  // determines number of cs_n and cke pins
   parameter DATA_BITS = 4;  // determines data bit width and number of DM and DQS pins (1 per byte)

   parameter MAX_ADDR_BITS = 34;  // the maximum supported number of address bits
                                  // see also the three registers carbon_current_BA_BITS carbon_current_ROW_BITS carbon_current_COL_BITS

   parameter WRITE_PREAMBLE = 0; // max number of write preamble DQS pulses expected by model (if supported by protocol - e.g. DDR4)
   parameter READ_PREAMBLE = 0;  // max number of read  preamble DQS pulses generated by modle (if support by protocol - e.g. DDR4)
   
   // cycle accurate timing parameters
   
   // DQS_IN_DELAY: default assumes EARLY DQS input where DQS is high during first data transfer so it must
   // be delayed by 1/2 cycle to convert it to LATE DQS so it can be directly used as a clock
   // For "normal" JEDEC timing where DQS is already centered on the data, set DQS_IN_DELAY = 0
   parameter DQS_IN_DELAY = 1;

   // DQS_OUT_DELAY: default generates EARLY DQS timing which is the JEDEC standard.  To allow DQS to capture
   // data directly without further modification, it can be converted to LATE DQS timing by setting DQS_OUT_DELAY = 1
   parameter DQS_OUT_DELAY = 0;
   
   // calculated parameter
   parameter DM_BITS = ((DATA_BITS % 8) == 0 ) ? (DATA_BITS / 8) : (DATA_BITS / 8 ) + 1; 

`ifdef CARBON_DDR4_MODEL
   input                    act_n;
   input                    reset_n;
   // DQSBAR is not used internally for writes, but is generated for convenience on reads
   inout [DM_BITS-1:0]      dqsbar;
   input 		    par;  // unused!
   output 		    alert_n;  // unused!

   assign alert_n = 1;

`endif
`ifdef CARBON_DDR3_MODEL
   input                    reset_n;
   // DQSBAR is not used internally for writes, but is generated for convenience on reads
   inout [DM_BITS-1:0]      dqsbar;
`endif
   input                    ck;
   input                    ckbar; // unused!
   input [CS_BITS-1:0]      cke;
   input [CS_BITS-1:0]      cs_n;
   input                    ras_n;
   input                    cas_n;
   input                    we_n;
   input [3:0]              ba; // maximum width supported - any unused bit should be tied to 0
   input [15:0]             addr; // maximum width supported - any unused bit should be tied to 0
   
`ifdef CARBON_DDR4_MODEL
   inout [DM_BITS-1:0]      dm;  // this doubles as dbi_n so it becomes bidirectional
`else
   input [DM_BITS-1:0]      dm;
`endif
   // from cycle based perspective all dqs strobes must have identical timing 
   // only a single DQS signal is use internally - vector form provided for convenience
   inout [DM_BITS-1:0]      dqs;
   inout [DATA_BITS-1:0]    dq; // carbon observeSignal
   
   // check for supported range of parameter values, if the configuration is within range then this generate block will not produce any verilog
   generate
      if ( DATA_BITS  > 255 ) begin : checkDATA_BITS
         always @(posedge ck)
           begin   // carbon enableOutputSysTasks
              $display("\nConfiguration Error - DATA_BITS: %d is larger than supported value: %d.\n", DATA_BITS, 255);
              $stop;
           end
      end

      if ( MAX_ADDR_BITS > (`MAX_BA_BITS + `MAX_ROW_BITS + `MAX_COL_BITS)) begin  : checkMAX_ADDR_BITS
         always @(posedge ck)
           begin  // carbon enableOutputSysTasks
              $display("\nConfiguration Error - MAX_ADDR_BITS: %d is larger than the supported value %d.\n", MAX_ADDR_BITS, (`MAX_BA_BITS + `MAX_ROW_BITS + `MAX_COL_BITS));
              $stop;
           end
      end
      
   endgenerate


   // ********************************************************************************
   // ********************************************************************************
   // Internal parameters
   
   // Each block of internal memory is limited to 2^32 bits, so calcuate how many blocks are needed
   // This needs to consider the data width in addition to the bank/row/column bits  
   parameter LOG2_DATA_BITS = (DATA_BITS < 2) ? 1 :
                              (DATA_BITS < 4) ? 2 :
                              (DATA_BITS < 8) ? 3 :
                              (DATA_BITS < 16) ? 4 :
                              (DATA_BITS < 32) ? 5 :
                              (DATA_BITS < 64) ? 6 :
                              (DATA_BITS < 128) ? 7 :
                              (DATA_BITS < 256) ? 8 :
                              0; // greater than 255 data bits is not supported

   parameter TOTAL_MEM_BITS = LOG2_DATA_BITS + MAX_ADDR_BITS;
   parameter BLOCK_BITS = (TOTAL_MEM_BITS > 32) ? TOTAL_MEM_BITS - 32 : 0;
   parameter NUM_BLOCKS = 1 << BLOCK_BITS;

   parameter BLOCK_ADDR_BITS = (NUM_BLOCKS > 1) ? (32 - LOG2_DATA_BITS):MAX_ADDR_BITS;

   // define some registers that are driven by SoCD parameter values or Carbon C API to configure this memory
   
   // actual RBC mode used for device addressing; choices are:  1 == row_bank_column; 0 == bank_row_colum;   
   reg                      carbon_row_bank_column_format; // carbon depositSignal
                                                           // carbon observeSignal

   // actual bit widths used for device addressing - see JEDEC standard for specific device configurations
   reg [31:0]               carbon_current_BA_BITS;    // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0]               carbon_current_ROW_BITS;   // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0]               carbon_current_COL_BITS;   // carbon depositSignal
                                                       // carbon observeSignal

   // Parameters used during compilation (made available through Carbon C API)
   wire [7:0]               MAX_ADDR_BITS_VALUE = MAX_ADDR_BITS; // carbon observeSignal
   wire [7:0]               CS_BITS_VALUE = CS_BITS;      // carbon observeSignal
   wire [7:0]               DATA_BITS_VALUE = DATA_BITS;  // carbon observeSignal
   wire [7:0]               BA_BITS_VALUE  = carbon_current_BA_BITS;   // carbon observeSignal
   wire [7:0]               ROW_BITS_VALUE  = carbon_current_ROW_BITS;   // carbon observeSignal
   wire [7:0]               COL_BITS_VALUE  = carbon_current_COL_BITS;   // carbon observeSignal
   wire [7:0]               DM_BITS_VALUE = DM_BITS;      // carbon observeSignal
   wire [7:0]               DQS_IN_DELAY_VALUE  = DQS_IN_DELAY;      // carbon observeSignal
   wire [7:0]               DQS_OUT_DELAY_VALUE  = DQS_OUT_DELAY;    // carbon observeSignal
   wire [7:0]               NUM_BLOCKS_VALUE = NUM_BLOCKS;           // carbon observeSignal
   wire [7:0]               BLOCK_ADDR_BITS_VALUE = BLOCK_ADDR_BITS; // carbon observeSignal

   
   initial
      begin
         carbon_row_bank_column_format = 1;
         carbon_current_BA_BITS = 2;
         carbon_current_ROW_BITS = 16;
         carbon_current_COL_BITS = 14;
      end
   
   wire [CS_BITS-1:0] external_dqs_expected; // carbon observeSignal

   // check for error condition where more than 1 rank is expecting the external DQS - this can occur on back-to-back writes to different ranks with a data delay
   // this case will cause incorrect data to be written and is a problem with the data timing since there is no way for the second rank written to know that the data is late
   // and really destined for the first rank
   integer 	      i;
   reg [7:0] 	      expected_count;
   always @(posedge ck)
     begin
	expected_count = 0;
	for (i = 0; i < CS_BITS; i = i + 1) begin
	   if (external_dqs_expected[i])
	     expected_count = expected_count + 1;
	end
	if (expected_count > 1)
          begin  // carbon enableOutputSysTasks
             $display("\nModeling Error - Multiple DDRx Memory Rank Chip Selects are expecting to receive write DQS signals at the same time.\nThis indicates the data is arriving too late for the configured memory latencies,\nor the memory controller is not allowing sufficient time between writes to different ranks.  Data corruption may result.\n");
             $stop;
          end
     end
   
   genvar                   g;
   generate
      // Create multiple ranks
      for ( g = 0; g < CS_BITS; g = g + 1 ) begin : rank
         carbon_ddrx_rank #(.DATA_BITS(DATA_BITS),
                            .MAX_ADDR_BITS(MAX_ADDR_BITS),
			    .WRITE_PREAMBLE(WRITE_PREAMBLE),
			    .READ_PREAMBLE(READ_PREAMBLE),
                            .DM_BITS(DM_BITS),
                            .DQS_IN_DELAY(DQS_IN_DELAY),
                            .DQS_OUT_DELAY(DQS_OUT_DELAY),
                            .BLOCK_BITS(BLOCK_BITS),
                            .NUM_BLOCKS(NUM_BLOCKS),
                            .BLOCK_ADDR_BITS(BLOCK_ADDR_BITS)) rank_mem(
`ifdef CARBON_DDR4_MODEL
									.act_n(act_n),
									.reset_n(reset_n),
									.dqsbar(dqsbar),
									.par(par), // unused!
									.alert_n(), // unused!
`endif
`ifdef CARBON_DDR3_MODEL
                                                                        .reset_n(reset_n),
                                                                        .dqsbar(dqsbar),
`endif
                                                                        .ck(ck),
                                                                        .cke(cke[g]),
                                                                        .cs_n(cs_n[g]),
                                                                        .ras_n(ras_n),
                                                                        .cas_n(cas_n),
                                                                        .we_n(we_n),
                                                                        .dm(dm),
                                                                        .ba(ba),
                                                                        .addr(addr),
                                                                        .dq(dq),
                                                                        .dqs(dqs),
									.carbon_external_dqs_expected(external_dqs_expected[g]),
                                                                        .carbon_row_bank_column_format(carbon_row_bank_column_format),
                                                                        .carbon_current_BA_BITS(carbon_current_BA_BITS),
                                                                        .carbon_current_ROW_BITS(carbon_current_ROW_BITS),
                                                                        .carbon_current_COL_BITS(carbon_current_COL_BITS)
                                                                        );
      end
   endgenerate
endmodule

// ********************************************************************************
// DDRx Memory for each rank
module carbon_ddrx_rank(
`ifdef CARBON_DDR4_MODEL
                         act_n,
			 reset_n,
                         dqsbar,
			 par,  // unused!
			 alert_n,  // unused!
`endif
`ifdef CARBON_DDR3_MODEL
                        reset_n,
                        dqsbar,
`endif
                        ck,
                        cke,
                        cs_n,
                        ras_n,
                        cas_n,
                        we_n,
                        dm,
                        ba,
                        addr,
                        dq,
                        dqs,
			carbon_external_dqs_expected,
                        carbon_row_bank_column_format,
                        carbon_current_BA_BITS,
                        carbon_current_ROW_BITS,
                        carbon_current_COL_BITS
                        );
   

   parameter DATA_BITS = 1;
   parameter MAX_ADDR_BITS = 34;
   parameter WRITE_PREAMBLE = 0; // number of write preamble DQS pulses expected by model (if supported by protocol - e.g. DDR4)
   parameter READ_PREAMBLE = 0;  // number of read  preamble DQS pulses generated by modle (if support by protocol - e.g. DDR4)
   parameter DM_BITS = 1;
   parameter DQS_IN_DELAY = 1;
   parameter DQS_OUT_DELAY = 0;

   parameter BLOCK_BITS = 1;
   parameter NUM_BLOCKS = 1;
   parameter BLOCK_ADDR_BITS = 0;


   // derived parameters
   parameter BLOCK_BITS_mask = (( 1<< BLOCK_BITS )-1);
   parameter BLOCK_ADDR_BITS_mask = (1<<BLOCK_ADDR_BITS)-1;
   
   // port list
`ifdef CARBON_DDR4_MODEL
   input                    act_n;
   input                    reset_n;
   // DQSBAR is not used internally for writes, but is generated for convenience on reads
   inout [DM_BITS-1:0]      dqsbar;
   input 		    par;  // unused!
   output 		    alert_n;  // unused!

   assign alert_n = 1;
   
`endif
`ifdef CARBON_DDR3_MODEL
   input                    reset_n;
   // DQSBAR is not used internally for writes, but is generated for convenience on reads
   inout [DM_BITS-1:0]      dqsbar;
`endif
   input                    ck;
   input                    cke;
   input                    cs_n;
   input                    ras_n;
   input                    cas_n;
   input                    we_n;
   input [3:0]              ba;
   input [15:0]             addr;
   
`ifdef CARBON_DDR4_MODEL
   inout [DM_BITS-1:0]      dm;  // this doubles as dbi_n so it becomes bidirectional
`else
   input [DM_BITS-1:0]      dm;
`endif
   // from cycle based perspective all dqs strobes must have identical timing 
   // only a single DQS signal is use internally - vector form provided for convenience
   inout [DM_BITS-1:0]      dqs; 
   inout [DATA_BITS-1:0]    dq;

   output 		    carbon_external_dqs_expected;
   input                    carbon_row_bank_column_format;
   input [31:0]             carbon_current_BA_BITS;
   input [31:0]             carbon_current_ROW_BITS;
   input [31:0]             carbon_current_COL_BITS;

   wire [31:0]              carbon_current_BA_BITS_mask = (1 << carbon_current_BA_BITS)-1;
   wire [31:0]              carbon_current_ROW_BITS_mask = ( 1 << carbon_current_ROW_BITS)-1;
   wire [31:0]              carbon_current_COL_BITS_mask = ( 1 << carbon_current_COL_BITS)-1;

   wire [31:0]              carbon_current_COMBINED_ADDR_BITS = carbon_current_BA_BITS + carbon_current_ROW_BITS + carbon_current_COL_BITS;
   // use 64'b1 here to force verilog sizing rule to do the shift and subtraction with enough bits to always fill MAX_ADDR_BITS (we assume 64 > MAX_ADDR_BITS)
   wire [MAX_ADDR_BITS-1:0] carbon_current_COMBINED_ADDR_BITS_mask = (64'b1 << carbon_current_COMBINED_ADDR_BITS)-1;


   integer 		    i;
   reg  		    internal_reset;
   reg [1:0] 		    internal_reset_cnt;
   initial
     begin
	internal_reset = 1;
	internal_reset_cnt = 0;
     end

   always @(posedge ck)
     begin
	if (internal_reset_cnt != 2'b11) 
	  internal_reset_cnt <= internal_reset_cnt + 1;
	else
	  internal_reset <= 0;
     end

     
   // ********************************************************************************
   // ********************************************************************************
   // Profiling event registers - bank vector form (note for MRS, this means which mode register)
   
   reg [15:0]               profile_mrs;       // carbon observeSignal
   reg [15:0]               profile_activate;  // carbon observeSignal
   reg [15:0]               profile_read;      // carbon observeSignal
   reg [15:0]               profile_write;     // carbon observeSignal
   reg [15:0]               profile_bst;       // carbon observeSignal
   reg [15:0]               profile_precharge; // carbon observeSignal
   reg [15:0]               profile_refresh;   // carbon observeSignal

   reg  		    profile_read_data; // carbon observeSignal
   reg  		    profile_write_data;// carbon observeSignal

   // Per bank counters
   reg [31:0] 		    profile_activate_counter[0:15]; // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_read_counter[0:15];     // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_write_counter[0:15];    // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_precharge_counter[0:15];// carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_refresh_counter[0:15];  // carbon observeSignal
                                                            // carbon depositSignal

   // Total counts
   reg [31:0] 		    profile_activate_total_counter; // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_read_total_counter;     // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_write_total_counter;    // carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_precharge_total_counter;// carbon observeSignal
                                                            // carbon depositSignal
   reg [31:0] 		    profile_refresh_total_counter;  // carbon observeSignal
                                                            // carbon depositSignal

   reg [31:0] 		    profile_read_data_total_counter;  // carbon observeSignal
                                                              // carbon depositSignal
   reg [31:0] 		    profile_write_data_total_counter; // carbon observeSignal
                                                              // carbon depositSignal

   always @(posedge ck)
     begin
	for (i = 0; i < 16; i = i + 1) begin
	   if (profile_activate[i]) begin
	      profile_activate_counter[i] = profile_activate_counter[i] + 1;
	      profile_activate_total_counter = profile_activate_total_counter + 1;
	   end
	   if (profile_read[i]) begin
	      profile_read_counter[i] = profile_read_counter[i] + 1;
	      profile_read_total_counter = profile_read_total_counter + 1;
	   end
	   if (profile_write[i]) begin
	      profile_write_counter[i] = profile_write_counter[i] + 1;
	      profile_write_total_counter = profile_write_total_counter + 1;
	   end
	   if (profile_precharge[i]) begin
	      profile_precharge_counter[i] = profile_precharge_counter[i] + 1;
	      profile_precharge_total_counter = profile_precharge_total_counter + 1;
	   end
	   if (profile_refresh[i]) begin
	      profile_refresh_counter[i] = profile_refresh_counter[i] + 1;
	      profile_refresh_total_counter = profile_refresh_total_counter + 1;
	   end
	end
	if (profile_read_data)
	  profile_read_data_total_counter = profile_read_data_total_counter + 1;
	if (profile_write_data)
	  profile_write_data_total_counter = profile_write_data_total_counter + 1;
     end
   

   // Utilization: Sliding window average (100 cycles)
   reg [99:0] 		    profile_read_data_window;       // carbon observeSignal
   reg [99:0] 		    profile_write_data_window;      // carbon observeSignal
   reg [7:0] 		    profile_read_utilization;       // carbon observeSignal
   reg [7:0] 		    profile_write_utilization;      // carbon observeSignal
   reg [7:0] 		    profile_total_utilization;      // carbon observeSignal
 		    
   always @(posedge ck)
     begin
	profile_read_data_window  <= {profile_read_data_window[98:0],  profile_read_data};
	profile_write_data_window <= {profile_write_data_window[98:0], profile_write_data};
	profile_read_utilization = 0;
	profile_write_utilization = 0;
	for (i = 0; i < 100; i = i + 1) begin
	   if (profile_read_data_window[i])
	     profile_read_utilization = profile_read_utilization + 1;
	   if (profile_write_data_window[i])
	     profile_write_utilization = profile_write_utilization + 1;
	end
	profile_total_utilization = profile_write_utilization + profile_read_utilization;
     end
   
   // ********************************************************************************
   // ********************************************************************************
   // Internal Mode and Extended Mode registers
   reg [15:0]               MR0_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR1_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR2_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR3_reg; // carbon depositSignal
                                     // carbon observeSignal
`ifdef CARBON_DDR4_MODEL
   reg [15:0]               MR4_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR5_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR6_reg; // carbon depositSignal
                                     // carbon observeSignal
`endif
   
`ifdef CARBON_DDR2_MODEL
   wire [15:0]              MR_reg = MR0_reg;  
   wire [15:0]              EMR1_reg = MR1_reg;
   wire [15:0]              EMR2_reg = MR2_reg;
   wire [15:0]              EMR3_reg = MR3_reg;
`endif
`ifdef CARBON_DDR_MODEL
   wire [15:0]              MR_reg = MR0_reg;  
   wire [15:0]              EMR_reg = MR1_reg;  
`endif
   
   // ********************************************************************************
   // ********************************************************************************
   // Bidirectional signal generation and timing modification
   
   wire                     dqs_out_internal;
   wire                     dqs_out_delayed;
   wire                     dqs_out;

   wire                     dqs_oe_internal;
   wire                     dqs_oe_delayed;

   wire [DATA_BITS-1:0]     dq_out_internal;
   wire [DATA_BITS-1:0]     dq_out_delayed;

   wire                     dq_oe_internal;
   wire                     dq_oe_delayed;


   // ********************************************************************************
   // This logic allows an output delay to be added to the read DQ/DQS signals to
   // simulate tDQSCK relative to the programmed latency.

   // CARBON_READ_DELAY select is used add read delay in 1/2 clock cycle increments
   // defaults to 0 and is only be set through runtime deposits

   reg [3:0]                CARBON_READ_DELAY;   // carbon forceSignal
                                                 // carbon observeSignal

   initial
     begin
        CARBON_READ_DELAY = 0;
     end
   
   carbon_variable_delay_element #(DATA_BITS) dq_out_delay(.clk(ck),
                                                           .select(CARBON_READ_DELAY),
                                                           .in(dq_out_internal),
                                                           .out(dq_out_delayed));
   
   carbon_variable_delay_element #(1) dq_out_oe_delay(.clk(ck),
                                                      .select(CARBON_READ_DELAY),
                                                      .in(dq_oe_internal),
                                                      .out(dq_oe_delayed));
   

   carbon_variable_delay_element #(1) dqs_out_delay(.clk(ck),
                                                          .select(CARBON_READ_DELAY),
                                                          .in(dqs_out_internal),
                                                          .out(dqs_out_delayed));
   
   carbon_variable_delay_element #(1) dqs_out_oe_delay(.clk(ck),
                                                       .select(CARBON_READ_DELAY),
                                                       .in(dqs_oe_internal),
                                                       .out(dqs_oe_delayed));
   
   // ****************************************
   // This logic changes the relation between output DQ and DQS, allowing DQS to be shifted
   // 1/2 clock cycle to convert from EARLY DQS to LATE DQS which can be used to directly
   // clock the data. Note that only DQS is delayed

   carbon_retiming_element #(1, DQS_OUT_DELAY) dqs_out_retimer(ck, dqs_out_delayed, dqs_out);
   
   // ****************************************
   // generate the bidirectional output signals
   
   assign dqs_oe = dqs_oe_delayed; // dqs oe timing should not change, even though dqs out timing does by 1/2 cycle
   
   assign dqs    = dqs_oe_delayed ? { DM_BITS {  dqs_out }} : { DM_BITS { 1'bz } };
`ifdef CARBON_DDR3_OR_DDR4_MODEL
   assign dqsbar = dqs_oe_delayed ? { DM_BITS { ~dqs_out }} : { DM_BITS { 1'bz } };
   pullup dqsbar_pu[DM_BITS-1:0](dqsbar);
`endif
   
`ifdef CARBON_DDR4_MODEL
   wire 		    dm_enable = MR5_reg[`MR5_DM_ENABLE];
   wire 		    wr_dbi_enable = MR5_reg[`MR5_WR_DBI_ENABLE];
   wire 		    rd_dbi_enable = MR5_reg[`MR5_RD_DBI_ENABLE];
   
   reg [DM_BITS-1:0] dbi_n_out;

   function [DM_BITS-1:0] generate_dbi_n;
      input [DATA_BITS-1:0] dq_out;
      integer 		    count;
      integer 		    i, j;
      begin
	 generate_dbi_n = 0;
	 for (i = 0; i < DM_BITS; i = i + 1) 
	   begin
	      count = 0;
	      for (j = 0; j < 8; j = j + 1) begin
		 if (dq_out[8*i+j] == 0)
		   count = count + 1; // note we are count zeroes here
	      end
	      if (count > 4) // invert if 5 or more zeroes
		generate_dbi_n = generate_dbi_n | (1 << i);  // this is wrong polarity - it will be inverted prior to returning
	   end
	 generate_dbi_n = ~generate_dbi_n;
      end
   endfunction 
 	
   function [DATA_BITS-1:0] generate_dbi_n_mask;
      input [DM_BITS-1:0] dbi_n;
      integer 			    i;
      begin
	 generate_dbi_n_mask = 0;
	 for (i = 0; i < DM_BITS; i = i + 1)
	   begin
	      if (~dbi_n[i])
		generate_dbi_n_mask = generate_dbi_n_mask | (8'hff << (8*i));
	   end
      end
   endfunction 
   

   reg [DATA_BITS-1:0] dq_out_delayed_dbi;
   reg [DATA_BITS-1:0] dq_out_dbi_n_mask;
   
   always @(*)
     if (rd_dbi_enable)
       begin
	  dbi_n_out = generate_dbi_n(dq_out_delayed);
	  dq_out_dbi_n_mask = generate_dbi_n_mask(dbi_n_out);
	  dq_out_delayed_dbi = dq_out_delayed ^ dq_out_dbi_n_mask;
       end
     else
       begin
	  dq_out_delayed_dbi = dq_out_delayed;
       end
   
   assign dm = dm_enable ? { DM_BITS { 1'bz } } : ((rd_dbi_enable && dq_oe_delayed) ? dbi_n_out : { DM_BITS { 1'b1 } });

`else
   wire 		    dm_enable = 1;
   wire 		    wr_dbi_enable = 0;
   wire 		    rd_dbi_enable = 0;
   wire [DATA_BITS-1:0]     dq_out_delayed_dbi = dq_out_delayed;
`endif
       
   assign dq = dq_oe_delayed ? dq_out_delayed_dbi : { DATA_BITS { 1'bz } };

   pulldown dqs_pd[DM_BITS-1:0](dqs);

   // ********************************************************************************
   // Bidirectional signals input path

   // NOTE:  Only a single dqs signal is used internally!
   // general form for generating internal write dqs (DM should provide final qualification of data)
   wire                     dqs_in = |dqs & !dqs_oe; // ignore dqs that we generate
   // alternate form using lsb for generating internal write dqs - just use a single bit
   // wire                  dqs_in = dqs[0] & !dqs_oe; // ignore dqs that we generate

   wire                     dqs_in_internal;
   wire                     dqs_in_delayed;

   wire [DM_BITS-1:0]       dm_in = dm;
   wire [DM_BITS-1:0]       dm_in_delayed;
   wire [DATA_BITS-1:0]     dq_in = dq;
   wire [DATA_BITS-1:0]     dq_in_delayed;

   // ********************************************************************************
   // This logic allows an input delay to be added to the read DQ/DM/DQS signals to
   // simulate tDQSCK relative to the programmed latency.

   // CARBON_WRITE_DELAY is used add write delay in 1/2 clock cycle increments
   // defaults to 0 and is only be set through runtime deposits

   reg [3:0]                CARBON_WRITE_DELAY;  // carbon forceSignal
                                                 // carbon observeSignal

   initial
     begin
        CARBON_WRITE_DELAY = 0;
     end
   
   carbon_variable_delay_element #(1) dqs_in_delay(.clk(ck),
                                                   .select(CARBON_WRITE_DELAY),
                                                   .in(dqs_in_internal),
                                                   .out(dqs_in_delayed));
   
   carbon_variable_delay_element #(DM_BITS) dm_in_delay(.clk(ck),
                                                        .select(CARBON_WRITE_DELAY),
                                                        .in(dm_in),
                                                        .out(dm_in_delayed));
   
   carbon_variable_delay_element #(DATA_BITS) dq_in_delay(.clk(ck),
                                                          .select(CARBON_WRITE_DELAY),
                                                          .in(dq_in),
                                                          .out(dq_in_delayed));
   
   // ****************************************
   // This logic changes the relation between input DQ and DQS, allowing DQS to be shifted
   // 1/2 clock cycle to convert from EARLY DQS to LATE DQS which can be used to directly
   // clock the data.

   carbon_retiming_element #(1, DQS_IN_DELAY) dqs_in_retimer(ck, dqs_in, dqs_in_internal);
   
   

   // ********************************************************************************
   // ********************************************************************************
   // Current Input Command Processing

`ifdef CARBON_DDR4_MODEL
   // DDR4 allows a cs_n to cmd/addr latency (so cs_n is early relative to the command) so this logic realigns it for internal use
   reg cs_n_internal;
   reg [8:0] cs_n_shifter;
   
   always @(posedge ck or negedge reset_n)
     if (!reset_n)
       cs_n_shifter <= 9'h1ff;
     else
       cs_n_shifter = { cs_n_shifter[7:0], (cke ? cs_n: 1'b1) };

   always @(*)
     case (MR4_reg[`MR4_CS_LATENCY])
       0: cs_n_internal <= cs_n;
       1: cs_n_internal <= cs_n_shifter[2];
       2: cs_n_internal <= cs_n_shifter[3];
       3: cs_n_internal <= cs_n_shifter[4];
       4: cs_n_internal <= cs_n_shifter[5];
       5: cs_n_internal <= cs_n_shifter[7];
       6: cs_n_internal <= cs_n; // reserved
       7: cs_n_internal <= cs_n; // reserved
     endcase
`else
   wire cs_n_internal = cs_n;
`endif

   
   // The actual command as a concatenation of the appropriate signals
   wire [3:0]               cmd = {cs_n_internal, ras_n, cas_n, we_n};
   reg [3:0]                last_valid_cmd;
   reg [3:0]                last_valid_cmd_d;
   
   // ****************************************
   // We need a queue of commands that are pending for upcoming clock
   // cycles. Input commands are captured and all relevant data is placed in a queue
   // for later processing (after the required latency) when the data is available
   // for writing or needs to be driven for reading. The double data rate aspect will 
   // be handled when the command is processed.

   // The queue operates on a system clock, so a single burst command will put multiple commands 
   // in the queue. For example, with a burst length of 4, a there will be two
   // cycles on which data is transferred.  In this case, two entries
   // will be inserted into the queue, at offsets N and N+1, where N is
   // the initial latency for the command.
   //
   // The queue depth is chosen to accomodate the worst case latencies

   parameter CMD_QUEUE_DEPTH = 32;

   // ****************************************
   // Command state information
   
   reg [4:0]                queue_ptr; // Current location in circular queue
   reg 			    preamble_q [0:CMD_QUEUE_DEPTH-1]; // preamble
   reg [4:0]                command_q [0:CMD_QUEUE_DEPTH-1]; // command 
   reg [MAX_ADDR_BITS-1:0] addr_q [0:CMD_QUEUE_DEPTH-1]; // combined bank, row & column address
   reg 			    preamble_immediate;
   
   initial
     preamble_immediate = 0;
   
   parameter MAX_NUM_BANKS = 1 << `MAX_BA_BITS;
   reg [`MAX_ROW_BITS-1:0]              row_addr[0:MAX_NUM_BANKS-1]; // row address captured during bank activation
   reg [15:0]                    needs_precharge; // state info for bank activation precharge checking

   // Registers for reporting errors to C++ interface
   reg [7:0]                    error_code; // carbon observeSignal
   reg [7:0]                    error_bank; // carbon observeSignal

`define COLLISION_ERROR 8'h01
`define PRECHARGE_ERROR 8'h02
`define ACTIVATE_ERROR  8'h04
`define BST_ERROR       8'h08

   initial begin
      queue_ptr = 0;
      // invalidate the queue so false errors are not reported at startup
      for (i = 0; i < CMD_QUEUE_DEPTH; i = i + 1) begin
         preamble_q[i] = 0;
         command_q[i] = `CMD_INVALID;
         addr_q[i] = 0;
      end
      for (i = 0; i < MAX_NUM_BANKS; i = i + 1) begin
         row_addr[i] = 0;
      end

      needs_precharge = 0;  // don't check on startup
      error_code = 0;
      error_bank = 0;
   end

        
   task check_command_q;
      input [4:0] queue_ptr;
      begin
         if (command_q[queue_ptr] != `CMD_INVALID) begin
`ifdef CARBON_DDR_MODEL
            // for DDR writes cannot interrupt reads, but the other 3 permutations are allowed (rd/rd, wr/wr, wr/rd)
            if ((command_q[queue_ptr] == `CMD_WRITE) && (last_valid_cmd_d == `CMD_READ)) begin
`else
            // writes can interrupt other writes and reads can interrupt other reads, but that is it
            if (command_q[queue_ptr] != last_valid_cmd_d) begin
`endif
               error_code = error_code | `COLLISION_ERROR;
               // error_bank = 0;
`ifndef CARBON
               $display("Error - command collision occurred\n");
`endif
               $stop;
               // error_code = 0;
               // error_bank = 0;
            end
         end
      end
   endtask
   
   task check_precharge;
      begin
         if (needs_precharge & (1 << (ba & carbon_current_BA_BITS_mask))) begin
            error_code =  error_code | `PRECHARGE_ERROR;
            error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
            $display("Error - Activation of bank %d without precharge\n", (ba & carbon_current_BA_BITS_mask));
`endif
            $stop;
            // error_code = 0;
            // error_bank = 0;
         end
      end
   endtask
   
   task check_activated;
      begin
         if (!(needs_precharge & (1 << (ba & carbon_current_BA_BITS_mask)))) begin
            error_code = error_code | `ACTIVATE_ERROR;
            error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
            $display("Error - Command issued to bank %d without prior activation\n", (ba & carbon_current_BA_BITS_mask));
`endif
            $stop;
            // error_code = 0;
            // error_bank = 0;
         end
      end
   endtask
   
   // ****************************************
   // These are helper wires/regs that reflect the current state during input command processing

   // column address - skip over the A[10]/AP bit when extracting the column address from the address pins
   // for DDR3, must also skip over the A[12]/BC# bit
   reg [`MAX_COL_BITS-1:0]          col_addr;
   always @(addr or carbon_current_COL_BITS or carbon_current_COL_BITS_mask) begin
      col_addr = 0;
      if (carbon_current_COL_BITS < 11) begin: COL_BITS_LT_11
        col_addr = {`MAX_COL_BITS'b0, addr[9:0]};
      end
      else if (carbon_current_COL_BITS == 11) begin: COL_BITS_EQ_11
        col_addr[9:0] = addr[9:0];
        col_addr[10] = addr[11];
      end
`ifdef CARBON_DDR3_OR_DDR4_MODEL
      else if (carbon_current_COL_BITS == 12) begin: COL_BITS_EQ_12
        col_addr[9:0] = addr[9:0];
        col_addr[10] = addr[11];
        col_addr[11] = addr[13];
      end
      else begin: COL_BITS_GT_12
        col_addr[9:0] = addr[9:0];
        col_addr[10] = addr[11];
        col_addr[`MAX_COL_BITS-1:11] = (addr & carbon_current_COL_BITS_mask) >> 13;
      end
`endif
`ifdef CARBON_DDR2_MODEL
      else begin: COL_BITS_GT_11
        col_addr[9:0] = addr[9:0];
        col_addr[`MAX_COL_BITS-1:10] = (addr & carbon_current_COL_BITS_mask) >> 11;
      end
`endif
`ifdef CARBON_DDR_MODEL
      else begin: COL_BITS_GT_11
        col_addr[9:0] = addr[9:0];
        col_addr[`MAX_COL_BITS-1:10] = ( addr & carbon_current_COL_BITS_mask) >> 11;
      end
`endif
   end

   reg [MAX_ADDR_BITS-1:0] comb_addr;
   
   // Combined address is the current bank, the saved row for the selected bank and the current column
   always @(*) begin
      if ( carbon_row_bank_column_format )
	 begin
//         comb_addr = { row_addr[(ba & carbon_current_BA_BITS_mask)], (ba & carbon_current_BA_BITS_mask), col_addr};
	    comb_addr = ((row_addr[(ba & carbon_current_BA_BITS_mask)] & carbon_current_ROW_BITS_mask) << (carbon_current_BA_BITS+ carbon_current_COL_BITS)) |
			( (ba                                          & carbon_current_BA_BITS_mask) << (carbon_current_COL_BITS) ) |
			( (col_addr                                    & carbon_current_COL_BITS_mask) << 0 );
	 end
      else
	begin
//           comb_addr = { (ba & carbon_current_BA_BITS_mask), row_addr[(ba & carbon_current_BA_BITS_mask)], col_addr};
	    comb_addr = ( (ba                                           & carbon_current_BA_BITS_mask) << (carbon_current_ROW_BITS+ carbon_current_COL_BITS) ) |
			( (row_addr[(ba & carbon_current_BA_BITS_mask)] & carbon_current_ROW_BITS_mask) << (carbon_current_COL_BITS)) |
			( (col_addr                                     & carbon_current_COL_BITS_mask) << 0 );
	   
	end
   end
   
`ifdef CARBON_DDR4_MODEL
   // DDR4
   // From section 3.5 (Mode Register Definitions) of the DDR4 spec:  Read latency is CAS latency
   // plus additive latency, and write latency is Cas Write lantency plus additive latency.
   // Zero-pad so sub-values are treated as unsigned.
   reg  [4:0]               cas_latency;

   always @(*)
     begin
	case ({MR0_reg[`MR0_CAS_LATENCY_MSB], MR0_reg[`MR0_CAS_LATENCY_LSB]})
	  0: cas_latency <= 5'd9;
	  1: cas_latency <= 5'd10;
	  2: cas_latency <= 5'd11;
	  3: cas_latency <= 5'd12;
	  4: cas_latency <= 5'd13;
	  5: cas_latency <= 5'd14;
	  6: cas_latency <= 5'd15;
	  7: cas_latency <= 5'd16;
	  8: cas_latency <= 5'd18;
	  9: cas_latency <= 5'd20;
	  10: cas_latency <= 5'd22;
	  11: cas_latency <= 5'd24;
	  default: cas_latency <= 0;
	endcase
     end
   
   reg  [4:0]               cas_write_latency;

   always @(*)
     begin
	case (MR2_reg[`MR2_CAS_WRITE_LATENCY])
	  0: cas_write_latency <= 5'd9;
	  1: cas_write_latency <= 5'd10;
	  2: cas_write_latency <= 5'd11;
	  3: cas_write_latency <= 5'd12;
	  4: cas_write_latency <= 5'd14;
	  5: cas_write_latency <= 5'd16;
	  6: cas_write_latency <= 5'd18;
	  default: cas_write_latency <= 0;
	endcase
     end
   
   wire [4:0]               AL_setting = { 3'h0, MR1_reg[`MR1_ADDITIVE_LATENCY] } ;
   wire [4:0]               additive_latency = (AL_setting == 0) ? 0 : cas_latency - AL_setting;  // See Section 3.4.3.4
   
   wire [4:0]               read_latency =  additive_latency + cas_latency;
   wire [4:0]               write_latency = additive_latency + cas_write_latency;
`endif
`ifdef CARBON_DDR3_MODEL
   // DDR3
   // From section 3.4.3.4 of the DDR3 spec:  Read latency is CAS latency
   // plus additive latency, and write latency is Cas Write lantency plus additive latency.
   // Zero-pad so sub-values are treated as unsigned.
   wire [4:0]               cas_latency = { 1'b0, MR0_reg[`MR0_CAS_LATENCY_MSB], MR0_reg[`MR0_CAS_LATENCY] } + 5'h4 ; // See 3.4.2
   wire [4:0]               cas_write_latency = { 2'b0, MR2_reg[`MR2_CAS_WRITE_LATENCY] } + 5'h5 ;  // See Section 3.4.4
   wire [4:0]               AL_setting = { 3'h0, MR1_reg[`MR1_ADDITIVE_LATENCY] } ;
   wire [4:0]               additive_latency = (AL_setting == 0) ? 0 : cas_latency - AL_setting;  // See Section 3.4.3.4
   
   wire [4:0]               read_latency =  additive_latency + cas_latency;
   wire [4:0]               write_latency = additive_latency + cas_write_latency;
`endif
`ifdef CARBON_DDR2_MODEL
   // DDR2
   // From section 3.6.1 of the DDR2 spec:  Read latency is CAS latency
   // plus additive latency, and write latency is read lantency minus 1.
   // Zero-pad so sub-values are treated as unsigned.
   wire [4:0]               read_latency = {1'b0, MR_reg[`MR_CAS_LATENCY]} + {1'b0, EMR1_reg[`EMR1_ADD_LATENCY]};
   wire [4:0]               write_latency = read_latency - 5'h1;
`endif
`ifdef CARBON_DDR_MODEL
   // DDR
   // From Figure 4 of the DDR spec, with 0.5 latencies rounded down.
   // 0.5 latency is added back through a separate delay element as needed
   wire [4:0]               read_latency = (MR_reg[`MR_CAS_LATENCY] == 5) ? 1 : ((MR_reg[`MR_CAS_LATENCY] == 3) ? 3 : 2);  // not a full decode - defaults to 2!
   wire                     add_half_cycle = (MR_reg[`MR_CAS_LATENCY] == 5) || (MR_reg[`MR_CAS_LATENCY] == 6) ? 1 : 0;
   wire [4:0]               write_latency = 5'h1;  // write latency is fixed at the nominal value of 1

   // bank accessed by current operation 
   // This varies based on how the address is formed as it must be extracted from the total address!
   reg [`MAX_BA_BITS-1:0]           read_bank;
   reg [`MAX_BA_BITS-1:0]           write_bank;
   
   always @(*) begin
      if ( carbon_row_bank_column_format ) begin
         read_bank =  (addr_q[queue_ptr + read_latency + 5'h1] >> carbon_current_COL_BITS) & carbon_current_BA_BITS_mask;
         write_bank = (addr_q[queue_ptr + write_latency + 5'h1]>> carbon_current_COL_BITS) & carbon_current_BA_BITS_mask;
      end
      else begin
         read_bank =  (addr_q[queue_ptr + read_latency  + 5'h1] >> (carbon_current_ROW_BITS + carbon_current_COL_BITS)) & carbon_current_BA_BITS_mask;
         write_bank = (addr_q[queue_ptr + write_latency + 5'h1] >> (carbon_current_ROW_BITS + carbon_current_COL_BITS)) & carbon_current_BA_BITS_mask;
      end 
   end
`endif
   
   // ********************************************************************************
   // ********************************************************************************
   // Process input commands and either act on them or place them in queue for 
   // processing after appropriate latency
   
   // The read and write operations will add new commands to the queue. 
   // There will be 2 entries for burst 4, and 4 entries for burst 8 operations.
   // We need to add 1 to each index as well, because we're updating the queue pointer
   // as well this cycle.
   //
   // A base address is saved for the two pairs of data transferred each cycle.
   // The actual address for each data element will be determined when the entry
   // is extracted from the queue.  
   
   always @(posedge ck) begin
      profile_mrs <= 0;
      profile_activate <= 0;
      profile_read <= 0;
      profile_write <= 0;
      profile_bst <= 0;
      profile_precharge <= 0;
      profile_refresh <= 0;
      profile_read_data <= 0;
      profile_write_data <= 0;
      
      last_valid_cmd_d <= last_valid_cmd;
      if (cke) begin
         if ((~cs_n_internal) && (cmd != `CMD_NOP)) begin
           last_valid_cmd <= cmd;
         end
         
`ifdef CARBON_DDR4_MODEL
	 // DDR4 Activate command is a special input signal and must be done first since for DDR4 "cmd" will be all zeros during activate which aliases to `CMD_MODE_REG
         if (!cs_n_internal && !act_n) begin
	    profile_activate <= 1 << (ba & carbon_current_BA_BITS_mask);
            check_precharge; // Check that the bank is precharged
            row_addr[(ba & carbon_current_BA_BITS_mask)] <= ({ras_n, cas_n, we_n, addr[13:0]} & carbon_current_ROW_BITS_mask); // Save the row address
            needs_precharge <= needs_precharge | (1 << (ba & carbon_current_BA_BITS_mask));
         end
`else
         if (cmd == `CMD_ACTIVATE) begin
	    profile_activate <= 1 << (ba & carbon_current_BA_BITS_mask);
            check_precharge; // Check that the bank is precharged
            row_addr[(ba & carbon_current_BA_BITS_mask)] <= (addr & carbon_current_ROW_BITS_mask); // Save the row address
            needs_precharge <= needs_precharge | (1 << (ba & carbon_current_BA_BITS_mask));
         end
`endif
         else if (cmd == `CMD_MODE_REG) begin
	    profile_mrs <= 1 << (ba & carbon_current_BA_BITS_mask);
            // Store in the appropriate mode register
            if ((ba & carbon_current_BA_BITS_mask) == 'd0) begin
               MR0_reg <= addr;
            end
            else if ((ba & carbon_current_BA_BITS_mask) == 'd1) begin
               MR1_reg <= addr;
            end
            else if ((ba & carbon_current_BA_BITS_mask) == 'd2) begin
               MR2_reg <= addr;
            end
            else if ((ba & carbon_current_BA_BITS_mask) == 'd3) begin
               MR3_reg <= addr;
            end
`ifdef CARBON_DDR4_MODEL
            else if ((ba & carbon_current_BA_BITS_mask) == 'd4) begin
               MR4_reg <= addr;
            end
            else if ((ba & carbon_current_BA_BITS_mask) == 'd5) begin
               MR5_reg <= addr;
            end
            else if ((ba & carbon_current_BA_BITS_mask) == 'd6) begin
               MR6_reg <= addr;
            end
`endif
         end
         else if (cmd == `CMD_REFRESH) begin
	    profile_refresh <= (1 << (carbon_current_BA_BITS_mask+1)) - 1;  // all banks
	 end
         else if (cmd == `CMD_PRECHARGE) begin
            if (addr[10]) begin
	       profile_precharge <= (1 << (carbon_current_BA_BITS_mask+1)) - 1;  // all banks
               needs_precharge <= 0;
	    end
            else begin
	       profile_precharge <=  1 << (ba & carbon_current_BA_BITS_mask);
               needs_precharge <= needs_precharge & (16'hffff ^ ( 1 << (ba & carbon_current_BA_BITS_mask)));
	    end
`ifdef CARBON_DDR_MODEL
            // For DDR, a PRECHARGE command can act as a burst terminate if executed on a the current read/write bank
            if (last_valid_cmd == `CMD_READ) begin
               // look ahead to see which bank it is accessing
               if (addr[10] || (read_bank == ba)) begin
                  // After the read latency, invalidate the queue entries - just use the maximum
                  command_q[queue_ptr + read_latency + 5'h1] <= `CMD_INVALID;
                  command_q[queue_ptr + read_latency + 5'h2] <= `CMD_INVALID;
                  command_q[queue_ptr + read_latency + 5'h3] <= `CMD_INVALID;
                  command_q[queue_ptr + read_latency + 5'h4] <= `CMD_INVALID;
               end
            end
            else if (last_valid_cmd == `CMD_WRITE) begin
               if (addr[10] || (write_bank == ba)) begin
                  // After the read latency, invalidate the queue entries - just use the maximum
                  command_q[queue_ptr + write_latency + 5'h1] <= `CMD_INVALID;
                  command_q[queue_ptr + write_latency + 5'h2] <= `CMD_INVALID;
                  command_q[queue_ptr + write_latency + 5'h3] <= `CMD_INVALID;
                  command_q[queue_ptr + write_latency + 5'h4] <= `CMD_INVALID;
               end
            end
`endif
         end
         else if (cmd == `CMD_WRITE) begin
	    profile_write <= 1 << (ba & carbon_current_BA_BITS_mask);
            check_activated; // Check that the bank is activated
`ifdef CARBON_DEBUG_ADDRESSES
	    $display("%m WRITE DDRx DEBUG ADDRESS:  BA = %d (%x)  ROW = 0x%04x (0x%04x)  COL = 0x%04x (0x%04x)\n", 
		     ba, carbon_current_BA_BITS_mask,
		     row_addr[(ba & carbon_current_BA_BITS_mask)], carbon_current_ROW_BITS_mask,
		     col_addr, carbon_current_COL_BITS_mask);
`endif
`ifdef CARBON_DDR4_MODEL
	    // 2nCK Preamble handling 
	    if (MR4_reg[`MR4_WR_PREAMBLE_LENGTH] && (WRITE_PREAMBLE == 2))
	      begin
		 if (command_q[queue_ptr + write_latency - 5'h1] == `CMD_INVALID) 
		   begin
		      // this is a write preamble and the command slot is open
		      preamble_q[queue_ptr + write_latency - 5'h1] <= 1;
		      command_q[queue_ptr + write_latency - 5'h1] <= cmd;
		      addr_q[queue_ptr + write_latency - 5'h1] <= 0;  // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		   end
		 else
		   if (command_q[queue_ptr + write_latency - 5'h1] != `CMD_WRITE)
		     begin
			// perform check to force error if preamble was requested on active command slot that was not a write (to allow seamless writes)
			check_command_q(queue_ptr + write_latency - 5'h1);  
		     end
	      end
	    // 1nCK Preamble handling (also part of 2nCK case)
	    if ((WRITE_PREAMBLE == 1) || (WRITE_PREAMBLE == 2))
	      begin
		 if (command_q[queue_ptr + write_latency - 5'h0] == `CMD_INVALID) 
		   begin
		      // this is a write preamble and the command slot is open
		      preamble_q[queue_ptr + write_latency - 5'h0] <= 1;
		      command_q[queue_ptr + write_latency - 5'h0] <= cmd;
		      addr_q[queue_ptr + write_latency - 5'h0] <= 0;  // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		   end
		 else
		   if (command_q[queue_ptr + write_latency - 5'h0] != `CMD_WRITE)
		     begin
			// perform check to force error if preamble was requested on active command slot that was not a write (to allow seamless writes)
			check_command_q(queue_ptr + write_latency - 5'h0);  
		     end
	      end
`endif //  `ifdef CARBON_DDR4_MODEL
`ifdef CARBON_DDR3_MODEL
	    // Write preamble
	    if (WRITE_PREAMBLE == 1)
	      begin
		 if (command_q[queue_ptr + write_latency - 5'h0] == `CMD_INVALID) 
		   begin
		      // this is a write preamble and the command slot is open
		      preamble_q[queue_ptr + write_latency - 5'h0] <= 1;
		      command_q[queue_ptr + write_latency - 5'h0] <= cmd;
		      addr_q[queue_ptr + write_latency - 5'h0] <= 0;  // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		   end
		 else
		   if (command_q[queue_ptr + write_latency - 5'h0] != `CMD_WRITE)
		     begin
			// perform check to force error if preamble was requested on active command slot that was not a write (to allow seamless writes)
			check_command_q(queue_ptr + write_latency - 5'h0);  
		     end
	      end
`endif //  `ifdef CARBON_DDR3_MODEL
`ifdef CARBON_DDR3_OR_DDR4_MODEL
            // DDR3 and DDR4
            // For details on the address generation, see Table 3 in section 3.4.2.1 of the DDR3 spec or Table 18 in section 4.3 of the DDR4 spec
            // Determine the burst length for this write operation
            if ((MR0_reg[`MR0_BURST_LENGTH] == `vMR0_BURST_LENGTH_8) ||
                ((MR0_reg[`MR0_BURST_LENGTH] == `vMR0_BURST_LENGTH_BC4or8) && (addr[12] == 1))) begin
               // Burst 8 - addresses are always 0,1,2,3,4,5,6,7 regardless of col_addr and
               // seq or interleaved settings for write operations so just set
               // the base address accordingly for each transfer pair
               check_command_q(queue_ptr + write_latency + 5'h1);
               command_q[queue_ptr + write_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h1] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), 3'b000 };  
               
               check_command_q(queue_ptr + write_latency + 5'h2);
               command_q[queue_ptr + write_latency + 5'h2] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h2] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), 3'b010 };
               
               check_command_q(queue_ptr + write_latency + 5'h3);
               command_q[queue_ptr + write_latency + 5'h3] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h3] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), 3'b100 };  
               
               check_command_q(queue_ptr + write_latency + 5'h4);
               command_q[queue_ptr + write_latency + 5'h4] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h4] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), 3'b110 };
 `ifdef CARBON_DDR4_MODEL
	       if (MR2_reg[`MR2_WRITE_CRC]) begin
		  // if write CRC is enabled, the CRC value at the end of the transfer will be clocked in with a DQS strobe
		  // we will treat that as preamble and cause it to be ignored 
		  check_command_q(queue_ptr + write_latency + 5'h5);
		  preamble_q[queue_ptr + write_latency + 5'h5] <= 1;
		  command_q[queue_ptr + write_latency + 5'h5] <= cmd;
		  addr_q[queue_ptr + write_latency + 5'h5] <= 0;   // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
	       end
 `endif
            end
            else begin
               // Burst 4 - addresses are either 0,1,2,3 or 4,5,6,7 depending on col_addr[2]
               // Note this is same if seq or interleaved for write operations so just set
               // the base address accordingly for each transfer pair
               check_command_q(queue_ptr + write_latency + 5'h1);
               command_q[queue_ptr + write_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h1] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), 2'b00 };  
               
               check_command_q(queue_ptr + write_latency + 5'h2);
               command_q[queue_ptr + write_latency + 5'h2] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h2] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), 2'b10 };
 `ifdef CARBON_DDR4_MODEL
	       if (MR2_reg[`MR2_WRITE_CRC]) begin
		  // if write CRC is enabled, BC4 must still perform a full Burst8 + CRC transfer (the last 4 data beats will be all ones, followed by CRC data)  
		  // the last 4 data beats and CRC values at the end of the transfer will be clocked in with a DQS strobe
		  // we will treat them all as preamble and cause it to be ignored when writing to the underlying memory block
		  check_command_q(queue_ptr + write_latency + 5'h3);
		  preamble_q[queue_ptr + write_latency + 5'h3] <= 1;
		  command_q[queue_ptr + write_latency + 5'h3] <= cmd;
		  addr_q[queue_ptr + write_latency + 5'h3] <= 0;   // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		  check_command_q(queue_ptr + write_latency + 5'h4);
		  preamble_q[queue_ptr + write_latency + 5'h4] <= 1;
		  command_q[queue_ptr + write_latency + 5'h4] <= cmd;
		  addr_q[queue_ptr + write_latency + 5'h4] <= 0;   // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		  check_command_q(queue_ptr + write_latency + 5'h5);
		  preamble_q[queue_ptr + write_latency + 5'h5] <= 1;
		  command_q[queue_ptr + write_latency + 5'h5] <= cmd;
		  addr_q[queue_ptr + write_latency + 5'h5] <= 0;   // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
	       end
 `endif
            end
`endif
`ifdef CARBON_DDR2_MODEL
            // DDR2
            // For details on the address generation, see the table in section 3.6.2 of the DDR2 spec.
            check_command_q(queue_ptr + write_latency + 5'h1);
            command_q[queue_ptr + write_latency + 5'h1] <= cmd;
            addr_q[queue_ptr + write_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);

            // Base address for second pair is starting address plus 2, wrapped on a 4-word
            // boundary.  This is the case even for BL=8.
            check_command_q(queue_ptr + write_latency + 5'h2);
            command_q[queue_ptr + write_latency + 5'h2] <= cmd;
            addr_q[queue_ptr + write_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), comb_addr[1:0] + 2'b10};
            
            if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL8) begin
               // Address for the third pair is the same offset as the first, but in the
               // other 4-word half of the 8-word block.
               check_command_q(queue_ptr + write_latency + 5'h3);
               command_q[queue_ptr + write_latency + 5'h3] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h3] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1:0]};

               // Final pair is a combination of the two
               check_command_q(queue_ptr + write_latency + 5'h4);
               command_q[queue_ptr + write_latency + 5'h4] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h4] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1:0] + 2'b10};
            end
`endif
`ifdef CARBON_DDR_MODEL
            // DDR
            // For details on the address generation, see Table 3 of the DDR spec.
            if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL2) begin
               check_command_q(queue_ptr + write_latency + 5'h1);
               command_q[queue_ptr + write_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL4) begin
               check_command_q(queue_ptr + write_latency + 5'h1);
               command_q[queue_ptr + write_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);
               
               check_command_q(queue_ptr + write_latency + 5'h2);
               command_q[queue_ptr + write_latency + 5'h2] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), comb_addr[1:0] + 2'b10};
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL8) begin
               check_command_q(queue_ptr + write_latency + 5'h1);
               command_q[queue_ptr + write_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + write_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);
               
               check_command_q(queue_ptr + write_latency + 5'h2);
               command_q[queue_ptr + write_latency + 5'h2] <= cmd;
               if (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) 
                 addr_q[queue_ptr + write_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b010};
               else
                 addr_q[queue_ptr + write_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), ~comb_addr[1], comb_addr[0]};

               check_command_q(queue_ptr + write_latency + 5'h3);
               command_q[queue_ptr + write_latency + 5'h3] <= cmd;
               if (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) 
                 addr_q[queue_ptr + write_latency + 5'h3] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b100};
               else
                 addr_q[queue_ptr + write_latency + 5'h3] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1], comb_addr[0]};
               
               check_command_q(queue_ptr + write_latency + 5'h4);
               command_q[queue_ptr + write_latency + 5'h4] <= cmd;
               if (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) 
                 addr_q[queue_ptr + write_latency + 5'h4] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b110};
               else
                 addr_q[queue_ptr + write_latency + 5'h4] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], ~comb_addr[1], comb_addr[0]};
            end
`endif
            // Auto-precharge option
            if (addr[10]) begin
	       profile_precharge <= 1 << (ba & carbon_current_BA_BITS_mask);
               needs_precharge <= needs_precharge & (16'hffff ^ (1 << (ba & carbon_current_BA_BITS_mask)));
            end
         end
         else if (cmd == `CMD_READ) begin
	    profile_read <= 1 << (ba & carbon_current_BA_BITS_mask);
            check_activated; // Check that the bank is activated
`ifdef CARBON_DEBUG_ADDRESSES
	    $display("%m READ  DDRx DEBUG ADDRESS:  BA = %d (%x)  ROW = 0x%04x (0x%04x)  COL = 0x%04x (0x%04x)\n", 
		     ba, carbon_current_BA_BITS_mask,
		     row_addr[(ba & carbon_current_BA_BITS_mask)], carbon_current_ROW_BITS_mask,
		     col_addr, carbon_current_COL_BITS_mask);
`endif
`ifdef CARBON_DDR3_OR_DDR4_MODEL
	    // Read preamble generation (DDR4) 
	    if (READ_PREAMBLE == 2)
	      begin
		 if (command_q[queue_ptr + read_latency - 5'h1] == `CMD_INVALID) 
		   begin
		      // this is a read preamble and the command slot is open
		      preamble_q[queue_ptr + read_latency - 5'h1] <= 1;
		      command_q[queue_ptr + read_latency - 5'h1] <= cmd;
		      addr_q[queue_ptr + read_latency - 5'h1] <= 0;  // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		   end
		 else
		   if (command_q[queue_ptr + read_latency - 5'h1] != `CMD_READ)
		     begin
			// perform check to force error if preamble was requested on active command slot that was not a read (to allow seamless reads)
			check_command_q(queue_ptr + read_latency - 5'h1);  
		     end
	      end
	    if ((READ_PREAMBLE == 1) || (READ_PREAMBLE == 2))
	      begin
		 if (command_q[queue_ptr + read_latency - 5'h0] == `CMD_INVALID) 
		   begin
		      // this is a read preamble and the command slot is open
		      preamble_q[queue_ptr + read_latency - 5'h0] <= 1;
		      command_q[queue_ptr + read_latency - 5'h0] <= cmd;
		      addr_q[queue_ptr + read_latency - 5'h0] <= 0;  // preamble is always "written to" address 0 (it will be squelched though as it is a preamble after all)
		   end
		 else
		   if (command_q[queue_ptr + read_latency - 5'h0] != `CMD_READ)
		     begin
			// perform check to force error if preamble was requested on active command slot that was not a read (to allow seamless reads)
			check_command_q(queue_ptr + read_latency - 5'h0);  
		     end
	      end
            // DDR3/DDR4
            // Burst 4 or 8 - first address is the initial address, while the second address is the first + 2, wrapped on 4 word boundary.
            // this is true for interleaved or sequential mode (and even burst 8)
            check_command_q(queue_ptr + read_latency + 5'h1);
            command_q[queue_ptr + read_latency + 5'h1] <= cmd;
            addr_q[queue_ptr + read_latency + 5'h1] <= (comb_addr & carbon_current_COMBINED_ADDR_BITS_mask);
            
            check_command_q(queue_ptr + read_latency + 5'h2);
            command_q[queue_ptr + read_latency + 5'h2] <= cmd;
            addr_q[queue_ptr + read_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), comb_addr[1:0] + 2'b10};

            // Determine if additional transfers needed for this read operation
            if ((MR0_reg[`MR0_BURST_LENGTH] == `vMR0_BURST_LENGTH_8) ||
                ((MR0_reg[`MR0_BURST_LENGTH] == `vMR0_BURST_LENGTH_BC4or8) && (addr[12] == 1))) begin
               // Address for the third pair is the same offset as the first, but in the
               // other 4-word half of the 8-word block.
               check_command_q(queue_ptr + read_latency + 5'h3);
               command_q[queue_ptr + read_latency + 5'h3] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h3] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1:0]};

               // Final pair is a combination of the two
               check_command_q(queue_ptr + read_latency + 5'h4);
               command_q[queue_ptr + read_latency + 5'h4] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h4] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1:0] + 2'b10};
            end
`endif
`ifdef CARBON_DDR2_MODEL            
            // DDR2
            // For details on the address generation, see the table in section 3.6.2 of the DDR2 spec.
            check_command_q(queue_ptr + read_latency + 5'h1);
            command_q[queue_ptr + read_latency + 5'h1] <= cmd;
            addr_q[queue_ptr + read_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);

            // Base address for second pair is starting address plus 2, wrapped on a 4-word
            // boundary.  This is the case even for BL=8.
            check_command_q(queue_ptr + read_latency + 5'h2);
            command_q[queue_ptr + read_latency + 5'h2] <= cmd;
            addr_q[queue_ptr + read_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), comb_addr[1:0] + 2'b10};
            
            if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL8) begin
               // Address for the third pair is the same offset as the first, but in the
               // other 4-word half of the 8-word block.
               check_command_q(queue_ptr + read_latency + 5'h3);
               command_q[queue_ptr + read_latency + 5'h3] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h3] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1:0]};

               // Final pair is a combination of the two
               check_command_q(queue_ptr + read_latency + 5'h4);
               command_q[queue_ptr + read_latency + 5'h4] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h4] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1:0] + 2'b10};
            end

`endif
`ifdef CARBON_DDR_MODEL
            // DDR
            // For details on the address generation, see Table 3 of the DDR spec.
            if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL2) begin
               check_command_q(queue_ptr + read_latency + 5'h1);
               command_q[queue_ptr + read_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL4) begin
               check_command_q(queue_ptr + read_latency + 5'h1);
               command_q[queue_ptr + read_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);
               
               check_command_q(queue_ptr + read_latency + 5'h2);
               command_q[queue_ptr + read_latency + 5'h2] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), comb_addr[1:0] + 2'b10};
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL8) begin
               check_command_q(queue_ptr + read_latency + 5'h1);
               command_q[queue_ptr + read_latency + 5'h1] <= cmd;
               addr_q[queue_ptr + read_latency + 5'h1] <= (comb_addr&carbon_current_COMBINED_ADDR_BITS_mask);
               
               check_command_q(queue_ptr + read_latency + 5'h2);
               command_q[queue_ptr + read_latency + 5'h2] <= cmd;
               if (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) 
                 addr_q[queue_ptr + read_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b010};
               else
                 addr_q[queue_ptr + read_latency + 5'h2] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>2), ~comb_addr[1], comb_addr[0]};

               check_command_q(queue_ptr + read_latency + 5'h3);
               command_q[queue_ptr + read_latency + 5'h3] <= cmd;
               if (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) 
                 addr_q[queue_ptr + read_latency + 5'h3] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b100};
               else
                 addr_q[queue_ptr + read_latency + 5'h3] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], comb_addr[1], comb_addr[0]};
               
               check_command_q(queue_ptr + read_latency + 5'h4);
               command_q[queue_ptr + read_latency + 5'h4] <= cmd;
               if (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) 
                 addr_q[queue_ptr + read_latency + 5'h4] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0] + 3'b110};
               else
                 addr_q[queue_ptr + read_latency + 5'h4] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ~comb_addr[2], ~comb_addr[1], comb_addr[0]};
            end
`endif
            // Auto-precharge option
            if (addr[10]) begin
	       profile_precharge <= 1 << (ba & carbon_current_BA_BITS_mask);
               needs_precharge <= needs_precharge & (16'hffff ^ (1 << (ba & carbon_current_BA_BITS_mask)));
            end
         end
`ifdef CARBON_DDR_MODEL
         else if (cmd == `CMD_BST) begin
	    profile_bst <= 1 << (ba & carbon_current_BA_BITS_mask);
            if (last_valid_cmd == `CMD_READ) begin
               // After the read latency, invalidate the queue entries - just use the maximum
               command_q[queue_ptr + read_latency + 5'h1] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h2] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h3] <= `CMD_INVALID;
               command_q[queue_ptr + read_latency + 5'h4] <= `CMD_INVALID;
            end
            else begin
               error_code = error_code | `BST_ERROR;
               error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
               $display("Error - BST command issued when not in a READ command\n");
`endif
               $stop;
               // error_code = 0;
               // error_bank = 0;
            end
         end
`endif 
	 // generate profiling info for data transfers on the DQ bus - this is based on current command expectations and not actual data arrival 
	 if ((command_q[queue_ptr] == `CMD_READ) && !preamble_q[queue_ptr])
	   profile_read_data <= 1;
	 
	 if ((command_q[queue_ptr] == `CMD_WRITE) && !preamble_q[queue_ptr])
	   profile_write_data <= 1;
	
         // Clear finished entries from the queue.  We only need to
         // invalidate the command and preamble
	 preamble_q[queue_ptr] <= 0;
         command_q[queue_ptr] <= `CMD_INVALID;
         queue_ptr <= queue_ptr + 5'h1;
      end
   end

   // ********************************************************************************
   // ********************************************************************************
   // Deferred Command Processing

   // Here is where we process the commands that were queued for deferred execution - READ and WRITE
   // For READ, the values are read and the DQS strobes are generated when the command is extracted from the queue.
   // For WRITE operations though, the data arrival is dictated by the externally generated DQS which may be delayed relative 
   // to dequeueing the WRITE command at this stage. This means a second level of command queueing may be needed to accomodate
   // the delayed DQS/DQ write data which gets written to the actual underlying memory as it comes in.  Note that previously this
   // functionality was implemented in the underlying block memory in a distributed fashion, but this resulted in a bug where back
   // to back writes to different blocks would cause the second write to be written to the wrong address.
   // Note that the case where write data arrives on time (no queueing needed) must also be accomodated.

   reg                      dqs_out_internal_sm;
   reg                      dqs_oe_internal_sm;
   reg [DATA_BITS-1:0]      dq_out_internal_sm;
   reg                      dq_oe_internal_sm;

`ifdef CARBON_DDR_MODEL
   // DDR can have read latencies of 1.5 and 2.5 but since the internal state machines only operate on posedge of clock
   // introduce a 1/2 delay on the output as required to meet read timing
   wire                     dqs_out_internal_sm_d;
   wire                     dqs_oe_internal_sm_d;
   wire [DATA_BITS-1:0]     dq_out_internal_sm_d;
   wire                     dq_oe_internal_sm_d;
   
   carbon_retiming_element #(.DATA_BITS(1), .DELAY(1)) dqs_out_internal_retimer(ck, dqs_out_internal_sm, dqs_out_internal_sm_d);
   carbon_retiming_element #(.DATA_BITS(1), .DELAY(1)) dqs_oe_internal_retimer(ck, dqs_oe_internal_sm, dqs_oe_internal_sm_d);
   carbon_retiming_element #(.DATA_BITS(DATA_BITS), .DELAY(1)) dq_out_internal_retimer(ck, dq_out_internal_sm, dq_out_internal_sm_d);
   carbon_retiming_element #(.DATA_BITS(1), .DELAY(1)) dq_oe_internal_retimer(ck, dq_oe_internal_sm, dq_oe_internal_sm_d);
   
   assign dqs_out_internal = add_half_cycle ? dqs_out_internal_sm_d : dqs_out_internal_sm;
   assign dqs_oe_internal  = add_half_cycle ? dqs_oe_internal_sm_d  : dqs_oe_internal_sm;
   assign dq_out_internal  = add_half_cycle ? dq_out_internal_sm_d  : dq_out_internal_sm;
   assign dq_oe_internal   = add_half_cycle ? dq_oe_internal_sm_d   : dq_oe_internal_sm;
`else
   // other DDRx memories do not support 1/2 cycle latencies
   assign dqs_out_internal = dqs_out_internal_sm;
   assign dqs_oe_internal = dqs_oe_internal_sm;
   assign dq_out_internal = dq_out_internal_sm;
   assign dq_oe_internal = dq_oe_internal_sm;
`endif

   
   // ****************************************
   // Look at the head of the queue to determine what commands are currently ready for execution
   wire [3:0] curr_command = command_q[queue_ptr];  // carbon observeSignal
   // Note this is 1 larger than it needs to be to avoid compiler warning on curr_addr[] below
   wire [MAX_ADDR_BITS:0] curr_addr = addr_q[queue_ptr];  // carbon observeSignal 
   wire [BLOCK_BITS-1:0]         curr_block = (NUM_BLOCKS > 1) ? ((curr_addr>>BLOCK_ADDR_BITS) & BLOCK_BITS_mask) : 0;  // carbon observeSignal

   // Addressing mode - sequential or interleaved
`ifdef CARBON_DDR3_OR_DDR4_MODEL
   wire                          seq_addr = (MR0_reg[`MR0_READ_BURST_TYPE] == `vMR0_RBT_NIBBLE_SEQ) ? 1 : 0;
`endif
`ifdef CARBON_DDR2_MODEL
   wire                          seq_addr = (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) ? 1 : 0;
`endif
`ifdef CARBON_DDR_MODEL
   wire                          seq_addr = (MR_reg[`MR_BURST_TYPE] == `vMR_BURST_TYPE_SEQ) ? 1 : 0;
`endif
   
   // ********************************************************************************
   // READ processing
   
   // block read address
   reg [BLOCK_ADDR_BITS-1:0]     block_raddr_1;
   reg [BLOCK_ADDR_BITS-1:0]     block_raddr_2;
   // array of memory block outputs (indexed by block number)
   reg [DATA_BITS-1:0]           block_dout_1[0:NUM_BLOCKS-1];  // carbon observeSignal
   reg [DATA_BITS-1:0]           block_dout_2[0:NUM_BLOCKS-1];  // carbon observeSignal
   
   // Generate the values for the blocks
   always @(*) begin
      block_raddr_1 = curr_addr[BLOCK_ADDR_BITS-1:0];
      block_raddr_2 = 'h0;
      dqs_out_internal_sm = 1'b0;
      dqs_oe_internal_sm = 1'b0;
      dq_out_internal_sm = 'h0;
      dq_oe_internal_sm = 'h0;
      if (curr_command == `CMD_READ) begin
         // dqs_out_internal_sm is the same as clock
         dqs_out_internal_sm = ck;
         dqs_oe_internal_sm = 1;
         dq_oe_internal_sm = 1;
`ifdef CARBON_DDR3_OR_DDR4_MODEL
         if (seq_addr) begin
            // Low address is one after the high address, but
            // we need to wrap on the 4-word boundary.
            block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>2)<<2) | ((curr_addr[1:0] + 2'b01) & 2'b11) ;
         end
         else begin
            // Interleaving is easy.  Just flip the low bit.
            block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>1)<<1) | ((~curr_addr[0]) & 1'b1);
         end
`endif
`ifdef CARBON_DDR2_MODEL
         if (seq_addr) begin
            // Low address is one after the high address, but
            // we need to wrap on the 4-word boundary.
            block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>2)<<2) | ((curr_addr[1:0] + 2'b01) & 2'b11) ;
         end
         else begin
            // Interleaving is easy.  Just flip the low bit.
            block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>1)<<1) | ((~curr_addr[0]) & 1'b1);
         end
`endif
`ifdef CARBON_DDR_MODEL
         // DDR
         // For details on the address generation, see Table 3 of the DDR spec.
         if (seq_addr) begin
            // Low address is one after the high address, but
            // we need to wrap on the burst size boundary.
            if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL2) begin
               block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>1)<<1) | ((~curr_addr[0]) & 1'b1);
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL4) begin
               block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>2)<<2) | ((curr_addr[1:0] + 2'b01) & 2'b11) ;
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL8) begin
               block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>3)<<3) | ((curr_addr[2:0] + 3'b001) & 3'b111) ;
            end
         end
         else begin
            // Interleaving is easy.  Just flip the low bit.
            // block_raddr_2 = {curr_addr[BLOCK_ADDR_BITS-1:1], ~curr_addr[0]};
            block_raddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>1)<<1) | ((~curr_addr[0]) & 1'b1);
         end
`endif
         // If preamble, drive 0, otherwise, select high or low output from the correct block
	 dq_out_internal_sm = preamble_q[queue_ptr] ? 0 : (ck ? block_dout_1[curr_block] : block_dout_2[curr_block]);

`ifdef CARBON_DDR3_OR_DDR4_MODEL
         // Check if we are in Multi-Purpose Register Mode as this affects the Read operation
         // Technically, this is of no use for our simulation purposes, but it is implemented
         // to minimize the impact on the end system so the operation can be performed and we can
         // move on to the rest of the simulation.
         if (MR3_reg[`MR3_MPR_ENABLE] == 1) begin
            // drive out a predefined value
            // Select high or low output from the correct block
            dq_out_internal_sm = ck ? { DATA_BITS { 1'b0 } }: { DATA_BITS { 1'b1 } };
         end
`endif
      end
   end


   // ********************************************************************************
   // WRITE processing

   // wires that directly drive the write port of the underlying memory block - these may be driven directly from
   // the deferred command decoding (if the data arrives at the nominal time) or from the
   // queued command decoding (if the data arrives late)

   // IMPORTANT:  For every command generated waclk strobe there must be a corresponding externally generated wdclk (write dqs) (coincident or later)
   // The waclk captures the address of a current or pending write while the wdclk captures the
   // actual data - if these do not have a one-to-one relation, the data will be written to the
   // wrong address.  This implementation allows flexibility in the timing of the data strobes
   // since they can be delayed relative to their nominal cycle time.
   // In general wptr and rptr should only be different when the data strobe is delayed relative to the
   // nominal cycle time.

   // interface signals to block memories
   reg [NUM_BLOCKS-1:0]          int_block_we;
   reg [BLOCK_ADDR_BITS-1:0]     int_block_waddr_1;
   reg [BLOCK_ADDR_BITS-1:0]     int_block_waddr_2;
   reg                           int_waclk;
   reg 				 int_preamble;

   // Generate the write enable strobe and write addresses values for the blocks
   // Note that if this is a preamble, no WE in generated but a FIFO entry is generated (since the preamble DQS input will cause the FIFO to be poppped)
   always @(*) begin
      int_waclk = 1;
      int_block_we = 'h0;
      int_block_waddr_1 = curr_addr[BLOCK_ADDR_BITS-1:0];
      int_block_waddr_2 = 'h0;
      int_preamble = 0;
      if ((preamble_immediate) || (curr_command == `CMD_WRITE)) begin
         // Generate write enable to the correct block
         int_waclk = ~ck;

	 // Is this a actual write or a preamble?
`ifdef CARBON_DDR3_OR_DDR4_MODEL
	 // generate the preamble
	 int_preamble = preamble_q[queue_ptr];
`endif
	 if (preamble_immediate || int_preamble)
           int_block_we = { NUM_BLOCKS{1'b0} }; // A preamble does not write any data
	 else
           int_block_we = 1'b1 << curr_block; // Select the corrent block WE

`ifdef CARBON_DDR3_OR_DDR4_MODEL
         // For details on the address generation, see the Table 3 in section 3.4.2.1 of the DDR3 spec.
         // For details on the address generation, see the Table 18 in section 4.3 of the DDR4 spec.
         // Low address is one after the high address, regardless of start and mode (alway seq)
         // Input command processing aligns the address to the appropriate block boundary
         int_block_waddr_2 = (curr_addr & BLOCK_ADDR_BITS_mask) + 1;
`endif
`ifdef CARBON_DDR2_MODEL
         // For details on the address generation, see the table in section 3.6.2 of
         // the DDR2 spec.
         if (seq_addr) begin
            // Low address is one after the high address, but
            // we need to wrap on the 4-word boundary.
            int_block_waddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>2)<<2) | ((curr_addr[1:0] + 2'b01) & 2'b11) ;
         end
         else begin
            // Interleaving is easy.  Just flip the low bit.
            int_block_waddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>1)<<1) | ((~curr_addr[0]) & 1'b1);

         end
`endif
`ifdef CARBON_DDR_MODEL
         // DDR
         // For details on the address generation, see Table 3 of the DDR spec.
         if (seq_addr) begin
            // Low address is one after the high address, but
            // we need to wrap on the burst size boundary.
            if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL2) begin
               int_block_waddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>1)<<1) | ((~curr_addr[0]) & 1'b1);
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL4) begin
               int_block_waddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>2)<<2) | ((curr_addr[1:0] + 2'b01) & 2'b11) ;
            end
            else if (MR_reg[`MR_BURST_LENGTH] == `vMR_BURST_LENGTH_BL8) begin
               int_block_waddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>3)<<3) | ((curr_addr[2:0] + 3'b001) & 3'b111) ;
            end
         end
         else begin
            // Interleaving is easy.  Just flip the low bit.
            int_block_waddr_2 = (((curr_addr & BLOCK_ADDR_BITS_mask)>>1)<<1) | ((~curr_addr[0]) & 1'b1);
         end
`endif
      end
   end

   // FIFO to store current address and block write enable vector for the next DQS strobe received
   // These pointers must track as they correspond to waclk and wdclk strobes
   reg [3:0] 			 wptr; 
   reg [3:0] 			 rptr;
   reg [NUM_BLOCKS-1:0] 	 fifo_block_we[0:15];
   reg [BLOCK_ADDR_BITS-1:0] 	 fifo_block_waddr_1[0:15];
   reg [BLOCK_ADDR_BITS-1:0] 	 fifo_block_waddr_2[0:15];

   initial begin
      wptr = 0;
      rptr = 0;
   end

   always @(posedge int_waclk or posedge internal_reset) 
     if (internal_reset)
       begin
	  wptr <= 0;
       end
     else
       begin
	  fifo_block_we[wptr] <= int_block_we;
	  fifo_block_waddr_1[wptr] <= int_block_waddr_1;
	  fifo_block_waddr_2[wptr] <= int_block_waddr_2;
	  wptr <= wptr + 1;
       end
   

   // ********************************************************************************
   // Write data signals that are processed immediately as it arrives from external DQ/DQS/DM

   reg    external_dqs_expected;  // carbon observeSignal
   assign carbon_external_dqs_expected = external_dqs_expected;
   
   initial
     begin
	external_dqs_expected = 0;
     end
   
   always @(*) begin
      if ((preamble_immediate) || (curr_command == `CMD_WRITE) || (wptr != rptr))
	external_dqs_expected <= ~internal_reset;
      else
	external_dqs_expected <= 0;
   end
   
   
   wire block_wdclk = ~dqs_in_delayed; // Simplified block_wdclk generation to work around CMS issues

   // always pop the FIFO when an external DQS is seen
   always @(posedge block_wdclk or posedge internal_reset)
     if (internal_reset)
       rptr <= 0;
     else if (external_dqs_expected)
       rptr <= rptr + 1;
   
   // Build the current write mask based on current dm value
   wire [DATA_BITS-1:0] write_mask;
   carbon_dm_to_dmask #(.DM_BITS(DM_BITS), .DATA_BITS(DATA_BITS)) dmask(dm_in_delayed, write_mask);

   // Capture first half of data transfer (and mask) using rising edge of dqs
   // This value and the second half will be written to memory on falling edge of dqs
   reg [DATA_BITS-1:0] dq_in_delayed_r;
   reg [DATA_BITS-1:0] write_mask_r;
   
   always @(posedge dqs_in_delayed) begin
      dq_in_delayed_r <= dq_in_delayed;
      write_mask_r <= write_mask;
   end

   reg [DATA_BITS-1:0] block_din_1;   // carbon observeSignal
   reg [DATA_BITS-1:0] block_din_2;   // carbon observeSignal
   reg [DATA_BITS-1:0] block_dmask_1; // carbon observeSignal
   reg [DATA_BITS-1:0] block_dmask_2; // carbon observeSignal
   always @(*) begin
      // First write data is what we registered earlier.  Second write data is
      // what's on the bus now.
      // For DBI operation, the write mask signal performs as the DBI_n signal expanded to data bit masks.
      // Since it DBI_n is active low (inversion occurs when low) the mask generated by carbon_dm_to_dmask is directly XORed with the data to perform the inversion 
      block_din_1 = wr_dbi_enable ? (dq_in_delayed_r ^ (write_mask_r)) : dq_in_delayed_r;
      block_din_2 = wr_dbi_enable ? (dq_in_delayed   ^ (write_mask  )) : dq_in_delayed;
`ifdef CARBON_DDR4_MODEL
      // DDR4 uses DM_n (inverted mask polarity)
      block_dmask_1 = dm_enable ? ~write_mask_r : {DATA_BITS{1'b1}};  
      block_dmask_2 = dm_enable ? ~write_mask   : {DATA_BITS{1'b1}};
`else
      block_dmask_1 = dm_enable ? write_mask_r : {DATA_BITS{1'b1}};
      block_dmask_2 = dm_enable ? write_mask   : {DATA_BITS{1'b1}};
`endif
   end

   
   // ********************************************************************************
   // Drive the block memory with either the direct input (when data comes at nominal timing)
   // or the FIFO outpu (when data is late) 
   
   reg [NUM_BLOCKS-1:0] 	 block_we;
   reg [BLOCK_ADDR_BITS-1:0] 	 block_waddr_1;
   reg [BLOCK_ADDR_BITS-1:0] 	 block_waddr_2;

   always @(*) begin
      if (wptr == rptr) begin
	 // use the signals directly
	 block_we <= int_block_we & {NUM_BLOCKS{external_dqs_expected}}; // Only allow writes when we are expecting them
	 block_waddr_1 <= int_block_waddr_1;
	 block_waddr_2 <= int_block_waddr_2;
      end
      else begin
	 // use the FIFO value
	 block_we <= fifo_block_we[rptr] & {NUM_BLOCKS{external_dqs_expected}}; // Only allow writes when we are expecting them
	 block_waddr_1 <= fifo_block_waddr_1[rptr];
	 block_waddr_2 <= fifo_block_waddr_2[rptr];
      end
   end

   
   // ********************************************************************************
   // ********************************************************************************

   // Since this is DDR, we need to read/write two words (at different addresses) each clock
   // cycle.  We'll call these 1 and 2, referring to their order on the data bus.
   // This has no relationship to the addresses.  We always read/write both together,
   // so only one write enable is needed.
   //
   // For writes, the high data is registered on the posedge of internal dqs, and both values are written
   // on the negedge of the internal dqs.
   //

   // ********************************************************************************
   // ********************************************************************************

   genvar                          g;
   generate
      for (g = 0; g < NUM_BLOCKS; g = g + 1) begin : block
         wire [DATA_BITS-1:0]      block_dout_wire_1;
         wire [DATA_BITS-1:0]      block_dout_wire_2;

         always @(*)
           block_dout_1[g] <= block_dout_wire_1;

         always @(*)
           block_dout_2[g] <= block_dout_wire_2;

         carbon_ddr_block_mem #(
				.ADDR_WIDTH(BLOCK_ADDR_BITS), 
				.DATA_BITS(DATA_BITS)
				) block_mem(.wdclk(block_wdclk),
                                            .we(block_we[g]),
                                            .raddr_a(block_raddr_1),
                                            .waddr_a(block_waddr_1),
                                            .din_a(block_din_1),
                                            .dmask_a(block_dmask_1),
                                            .dout_a(block_dout_wire_1),
                                            .raddr_b(block_raddr_2),
                                            .waddr_b(block_waddr_2),
                                            .din_b(block_din_2),
                                            .dmask_b(block_dmask_2),
                                            .dout_b(block_dout_wire_2));

      end
   endgenerate

endmodule

`undef CMD_INVALID
`undef CMD_MODE_REG
`undef CMD_PRECHARGE
`undef CMD_ACTIVATE
`undef CMD_WRITE
`undef CMD_READ

`ifdef CARBON_DDR4_MODEL
`undef MR0_BURST_LENGTH
`undef vMR0_BURST_LENGTH_8
`undef vMR0_BURST_LENGTH_BC4or8
`undef vMR0_BURST_LENGTH_BC4
`undef MR0_CAS_LATENCY_LSB
`undef MR0_READ_BURST_TYPE
`undef vMR0_RBT_NIBBLE_SEQ
`undef vMR0_RBT_INTERLEAVE
`undef MR0_CAS_LATENCY_MSB
`undef MR1_ADDITIVE_LATENCY
`undef MR2_CAS_WRITE_LATENCY
`undef MR2_WRITE_CRC
`undef MR3_MPR_LOCATION
`undef MR3_MPR_ENABLE
`undef MR3_WRITE_CMD_LATENCY_WITH_CRC_AND_DM
`undef MR4_CS_LATENCY
`undef MR4_RD_PREAMBLE_TRAINING_MODE
`undef MR4_RD_PREAMBLE_LENGTH
`undef MR4_WR_PREAMBLE_LENGTH
`undef MR5_PARITY_LATENCY_MODE
`undef MR5_DM_ENABLE
`undef MR5_WR_DBI_ENABLE
`undef MR5_RD_DBI_ENABLE
`endif //  `ifdef CARBON_DDR4_MODEL

`ifdef CARBON_DDR3_MODEL
`undef MR0_BURST_LENGTH 
`undef vMR0_BURST_LENGTH_8 
`undef vMR0_BURST_LENGTH_BC4or8 
`undef vMR0_BURST_LENGTH_BC4 
`undef MR0_CAS_LATENCY_MSB 
`undef MR0_READ_BURST_TYPE 
`undef vMR0_RBT_NIBBLE_SEQ 
`undef vMR0_RBT_INTERLEAVE 
`undef MR0_CAS_LATENCY 
`undef MR1_ADDITIVE_LATENCY 
`undef MR2_CAS_WRITE_LATENCY 
`undef MR3_MPR_LOCATION 
`undef MR3_MPR_ENABLE 
`endif //  `ifdef CARBON_DDR3_MODEL

`ifdef CARBON_DDR2_MODEL
`undef MR_BURST_LENGTH
`undef vMR_BURST_LENGTH_BL4
`undef vMR_BURST_LENGTH_BL8
`undef MR_BURST_TYPE
`undef vMR_BURST_TYPE_SEQ
`undef vMR_BURST_TYPE_INT
`undef MR_CAS_LATENCY
`undef EMR1_ADD_LATENCY
`endif

`ifdef CARBON_DDR_MODEL
`undef MR_BURST_LENGTH
`undef vMR_BURST_LENGTH_BL2
`undef vMR_BURST_LENGTH_BL4
`undef vMR_BURST_LENGTH_BL8
`undef MR_BURST_TYPE
`undef vMR_BURST_TYPE_SEQ
`undef vMR_BURST_TYPE_INT
`undef MR_CAS_LATENCY
`endif
