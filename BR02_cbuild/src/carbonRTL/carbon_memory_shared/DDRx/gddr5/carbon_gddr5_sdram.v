//******************************************************************************
// Copyright (c) 2010-2012 by Carbon Design Systems, Inc., All Rights Reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE. NO PERMISSION IS GRANTED TO USE, 
// MODIFY OR COPY THIS SOFTWARE FOR USE WITH ANY TOOL OR ENVIRONMENT WHICH HAS 
// NOT BEEN LICENSED FROM CARBON DESIGN SYSTEMS.
//
//******************************************************************************

// Parameterized model of GDDR5 memory for general use in Carbon compiler environments.  
// This is a minimal implementation, excluding things like refresh and timing error checking.  

// This model represents a complete GDDR5 memory subsystem, consisting of multiple physical devices.
// For this implementation, in order to present a single logical debug view of the entire memory subsystem
// (rather than a view into each individual device), certain requirements are placed on the signals.

// One controller implementation might be to provide dedicated signals for each individual device.  This can be viewed
// as a "device port" from the controller, as each device gets its own complete set of signals, often for signal integrity.
// To support this reason, control signals for each individual device are made available in vector form where signals from 
// multiple controller "device ports" are concatenated to create the vector form presented by the model.

// If the memory controller outputs a smaller signal set (i.e. some signals are shared by the underlying devices),
// the user must explictly provide the connectivity to each device.

// this model is defined such that it can support many different memory size
// configurations, however the maximum sizes are defined by the following:
`define MAX_BA_BITS 4
`define MAX_ROW_BITS 13
`define MAX_COL_BITS 7

`timescale 1ps / 1ps

// ****************************************
// Error Handling Macros
//
// The following macros define device level task names that are
// executed when an unsupported cmd or mode is issued

`define UNSUPPORTED_LDFF_CMD unsupported_ldff_cmd
`define UNSUPPORTED_RDTR_CMD unsupported_rdtr_cmd
`define UNSUPPORTED_WSM_CMD unsupported_wsm_cmd
`define UNSUPPORTED_WDM_CMD unsupported_wdm_cmd
`define UNSUPPORTED_WRTR_CMD unsupported_wrtr_cmd

`define UNSUPPORTED_WCK2CK_MODE unsupported_wck2ck_mode
`define UNSUPPORTED_RDQS_MODE unsupported_rdqs_mode
`define UNSUPPORTED_RDCRC_MODE unsupported_rdcrc_mode
`define UNSUPPORTED_WRCRC_MODE unsupported_wrcrc_mode
`define UNSUPPORTED_DQPREA_MODE unsupported_dqprea_mode
`define UNSUPPORTED_ADT_MODE unsupported_adt_mode
`define UNSUPPORTED_MREMF1_MODE unsupported_mremf1_mode
   
// ****************************************
// Commands
// {cs_n, ras_n, cas_n, we_n, a11, a10, a8}
`define CMD_BITS          7

`define MASK_CMD_INVALID  7'b1000000
`define MASK_CMD          7'b1111000
`define MASK_CMD_FULL     7'b1111111
`define MASK_CMD_PRE      7'b1111001


// MASK_CMD_INVALID 
`define CMD_INVALID       7'b1000000

// MASK_CMD
`define CMD_NOP           7'b0111000
`define CMD_MRS           7'b0000000
`define CMD_ACT           7'b0011000

// MASK_CMD_FULL
`define CMD_RD            7'b0101000
`define CMD_RDA           7'b0101001
`define CMD_LDFF          7'b0101100  // UNSUPPORTED
`define CMD_RDTR          7'b0101110  // UNSUPPORTED

`define CMD_WOM           7'b0100000
`define CMD_WOMA          7'b0100001
`define CMD_WSM           7'b0100010  // UNSUPPORTED
`define CMD_WSMA          7'b0100011  // UNSUPPORTED
`define CMD_WDM           7'b0100100  // UNSUPPORTED
`define CMD_WDMA          7'b0100101  // UNSUPPORTED
`define CMD_WRTR          7'b0100110  // UNSUPPORTED

// MASK_CMD_PRE
`define CMD_PRECHARGE     7'b0010000
`define CMD_PRECHARGE_ALL 7'b0010001

// ****************************************
// GDDR5 Register Fields/Values

`define MR0_WLmrs     2:0
`define MR0_CLmrs     6:3

`define MR1_RDBI      8
`define MR1_WDBI      9
`define MR1_ABI       10

`define MR3_WCK2CK    4    // UNSUPPORTED
`define MR3_RDQS      5    // UNSUPPORTED
`define MR3_INFO      7:6

`define MR4_EDC_HOLD  3:0
`define MR4_EDC_HOLD0 0
`define MR4_EDC_HOLD1 1
`define MR4_EDC_HOLD2 2
`define MR4_EDC_HOLD3 3

`define MR4_CRCWL     6:4
`define MR4_CRCRL     8:7
`define MR4_RDCRC     9    // UNSUPPORTED
`define MR4_WRCRC     10   // UNSUPPORTED
`define MR4_EDC13Inv  11

`define MR7_DQPREA    5    // UNSUPPORTED

`define MR8_CLEHF     0

`define MR15_MREMF0   8    // MF is always 0
`define MR15_MREMF1   9    // UNSUPPORTED
`define MR15_ADT      10   // UNSUPPORTED


// ********************************************************************************
// Top Level GDDR5 Memory Subsystem - Note that only a single ck and wck are provided - assumes (requires) 50/50 duty cycle, phase aligned

module carbon_gddr5_sdram(
                          reset_n,
                          ck,
			  wck,
                          cke,
                          cs_n,
                          ras_n,
                          cas_n,
                          we_n,
			  a,
			  abi_n,
			  dq,
			  dbi_n,
			  edc
                          );

   // number of chip selects (ranks), device width and devices per rank
   parameter DEVICE_BYTE_LANES = 4;  // determines data bit width (must be 2 or 4)
   parameter DEVICES_PER_RANK = 1;  // determines data bit width 

   parameter MAX_ADDR_BITS = 24;  // the maximum supported number of address bits
                                  // see also the three registers carbon_current_BA_BITS carbon_current_ROW_BITS carbon_current_COL_BITS

   // Calculated parameter
   parameter DATA_BITS = DEVICES_PER_RANK*8*DEVICE_BYTE_LANES;
   

   // Signals COMMON to all "devices"
   input [DEVICES_PER_RANK-1:0]				    reset_n;
   input 						    ck;   
   input 						    wck;  
   input [DEVICES_PER_RANK-1:0] 			    ras_n;
   input [DEVICES_PER_RANK-1:0] 			    cas_n;
   input [DEVICES_PER_RANK-1:0] 			    we_n;
   input [DEVICES_PER_RANK-1:0] 			    cke;
   input [DEVICES_PER_RANK-1:0]				    cs_n;

   // Wide Signals where only a subvector is applied to a "device" 
   // (Note addr is replicated modulo 12 to facilitate connections and reading waveforms)
   input [(12*DEVICES_PER_RANK)-1:0] 			    a; // 12 = arbitrary maximum width supported - any unused bit should be tied to 0 -in reality this is only 8/9
   input [DEVICES_PER_RANK-1:0] 			    abi_n;
   inout [DATA_BITS-1:0] 				    dq; // carbon observeSignal
   inout [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	    dbi_n;
   output [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	    edc;
   
   // check for supported range of parameter values, if the configuration is within range then this generate block will not produce any verilog
   generate

      if ( (DEVICE_BYTE_LANES != 2) && (DEVICE_BYTE_LANES != 4) ) begin : checkDEVICE_BYTE_LANES
         always @(posedge ck)
           begin   // carbon enableOutputSysTasks
              $display("\nConfiguration Error - DEVICE_BYTE_LANES: %d is not a supported value: Must be 2 or 4.\n", DEVICE_BYTE_LANES);
              $stop;
           end
      end

      if ( DATA_BITS  > 256 ) begin : checkDATA_BITS
         always @(posedge ck)
           begin   // carbon enableOutputSysTasks
              $display("\nConfiguration Error - DATA_BITS: %d is larger than supported value: %d.\n", DATA_BITS, 255);
              $stop;
           end
      end

      if ( MAX_ADDR_BITS > (`MAX_BA_BITS + `MAX_ROW_BITS + `MAX_COL_BITS)) begin  : checkMAX_ADDR_BITS
         always @(posedge ck)
           begin  // carbon enableOutputSysTasks
              $display("\nConfiguration Error - MAX_ADDR_BITS: %d is larger than the supported value %d.\n", MAX_ADDR_BITS, (`MAX_BA_BITS + `MAX_ROW_BITS + `MAX_COL_BITS));
              $stop;
           end
      end
      
   endgenerate


   // ********************************************************************************
   // ********************************************************************************
   // Internal parameters
   
   // Each block of internal memory is limited to 2^32 bits, so calcuate how many blocks are needed
   // This needs to consider the data width in addition to the bank/row/column bits  
   parameter LOG2_DATA_BITS = (DATA_BITS < 2) ? 1 :
                              (DATA_BITS < 4) ? 2 :
                              (DATA_BITS < 8) ? 3 :
                              (DATA_BITS < 16) ? 4 :
                              (DATA_BITS < 32) ? 5 :
                              (DATA_BITS < 64) ? 6 :
                              (DATA_BITS < 128) ? 7 :
                              (DATA_BITS < 256) ? 8 :
                              (DATA_BITS < 512) ? 9 :
                              0; // greater than 512 data bits is not supported

   parameter TOTAL_MEM_BITS = LOG2_DATA_BITS + MAX_ADDR_BITS;
   parameter BLOCK_BITS = (TOTAL_MEM_BITS > 32) ? TOTAL_MEM_BITS - 32 : 1;
   parameter NUM_BLOCKS = 1 << BLOCK_BITS;

   parameter BLOCK_ADDR_BITS = (NUM_BLOCKS > 1) ? (32 - LOG2_DATA_BITS):MAX_ADDR_BITS;

   // define some registers that are driven by SoCD parameter values or Carbon C API to configure this memory
   
   // actual RBC mode used for device addressing; choices are:  1 == row_bank_column; 0 == bank_row_colum;   
   reg                      carbon_row_bank_column_format; // carbon depositSignal
                                                           // carbon observeSignal

   // actual bit widths used for device addressing - see JEDEC standard for specific device configurations
   reg [31:0]               carbon_current_BA_BITS;    // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0]               carbon_current_ROW_BITS;   // carbon depositSignal
                                                       // carbon observeSignal
   reg [31:0]               carbon_current_COL_BITS;   // carbon depositSignal
                                                       // carbon observeSignal

   // Parameters used during compilation (made available through Carbon C API)
   wire [7:0]               MAX_ADDR_BITS_VALUE = MAX_ADDR_BITS; // carbon observeSignal
   wire [7:0]               DATA_BITS_VALUE = DATA_BITS;  // carbon observeSignal
   wire [7:0]               BA_BITS_VALUE  = carbon_current_BA_BITS;   // carbon observeSignal
   wire [7:0]               ROW_BITS_VALUE  = carbon_current_ROW_BITS;   // carbon observeSignal
   wire [7:0]               COL_BITS_VALUE  = carbon_current_COL_BITS;   // carbon observeSignal
   wire [7:0]               NUM_BLOCKS_VALUE = NUM_BLOCKS;           // carbon observeSignal
   wire [7:0]               BLOCK_ADDR_BITS_VALUE = BLOCK_ADDR_BITS; // carbon observeSignal

   
   initial
      begin
         carbon_row_bank_column_format = 1;
         carbon_current_BA_BITS = 4;
         carbon_current_ROW_BITS = 13;
         carbon_current_COL_BITS = 7;
      end
   
   
   genvar                   g;
   generate
      // Create single rank, but use generate to be consistent with other DDR models
      for ( g = 0; g < 1; g = g + 1 ) begin : rank
	 carbon_gddr5_rank #(.DEVICE_BYTE_LANES(DEVICE_BYTE_LANES),
			     .DEVICES_PER_RANK(DEVICES_PER_RANK),
			     .MAX_ADDR_BITS(MAX_ADDR_BITS),
			     .BLOCK_BITS(BLOCK_BITS),
			     .NUM_BLOCKS(NUM_BLOCKS),
			     .BLOCK_ADDR_BITS(BLOCK_ADDR_BITS)) rank_mem(
									 .reset_n(reset_n),
									 .ck(ck),
									 .wck(wck),
									 .cke(cke),
									 .cs_n(cs_n),
									 .ras_n(ras_n),
									 .cas_n(cas_n),
									 .we_n(we_n),
									 .a(a),
									 .abi_n(abi_n),
									 .dq(dq),
									 .dbi_n(dbi_n),
									 .edc(edc),
									 .carbon_row_bank_column_format(carbon_row_bank_column_format),
									 .carbon_current_BA_BITS(carbon_current_BA_BITS),
									 .carbon_current_ROW_BITS(carbon_current_ROW_BITS),
									 .carbon_current_COL_BITS(carbon_current_COL_BITS)
									 );
      end // block: rank
   endgenerate
endmodule // carbon_gddr5_sdram

// ********************************************************************************
// GDDR5 Memory for each rank
module carbon_gddr5_rank(
                         reset_n,
                         ck,
                         wck,
                         cke,
                         cs_n,
                         ras_n,
                         cas_n,
                         we_n,
                         a,
			 abi_n,
                         dq,
                         dbi_n,
			 edc,
                         carbon_row_bank_column_format,
                         carbon_current_BA_BITS,
                         carbon_current_ROW_BITS,
                         carbon_current_COL_BITS
                         );
   
   
   parameter DEVICE_BYTE_LANES = 4;
   parameter DEVICES_PER_RANK = 1;
   parameter MAX_ADDR_BITS = 34;
   parameter BLOCK_BITS = 1;
   parameter NUM_BLOCKS = 1;
   parameter BLOCK_ADDR_BITS = 0;


   // derived parameters
   parameter BLOCK_BITS_mask = (( 1<< BLOCK_BITS )-1);
   parameter BLOCK_ADDR_BITS_mask = (1<<BLOCK_ADDR_BITS)-1;
   
   // Calculated parameter
   parameter DATA_BITS = DEVICES_PER_RANK*8*DEVICE_BYTE_LANES;

   // port list
   input [DEVICES_PER_RANK-1:0]			        reset_n;
   input 						ck;   
   input 						wck;  
   input [DEVICES_PER_RANK-1:0]				ras_n;
   input [DEVICES_PER_RANK-1:0]				cas_n;
   input [DEVICES_PER_RANK-1:0]				we_n;
   input [DEVICES_PER_RANK-1:0]				cke;
   input [DEVICES_PER_RANK-1:0]				cs_n;
   input [(12*DEVICES_PER_RANK)-1:0] 			a; // 12 = maximum width supported - any unused bit should be tied to 0
   input [DEVICES_PER_RANK-1:0] 			abi_n;
   inout [DATA_BITS-1:0] 				dq; // carbon observeSignal
   inout [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	dbi_n;
   output [(DEVICES_PER_RANK*DEVICE_BYTE_LANES)-1:0] 	edc;
   
   input 						carbon_row_bank_column_format;
   input [31:0] 					carbon_current_BA_BITS;
   input [31:0] 					carbon_current_ROW_BITS;
   input [31:0] 					carbon_current_COL_BITS;
   
   wire [31:0] 						carbon_current_BA_BITS_mask = (1 << carbon_current_BA_BITS)-1;
   wire [31:0] 						carbon_current_ROW_BITS_mask = ( 1 << carbon_current_ROW_BITS)-1;
   wire [31:0] 						carbon_current_COL_BITS_mask = ( 1 << carbon_current_COL_BITS)-1;
   
   wire [31:0] 						carbon_current_COMBINED_ADDR_BITS = carbon_current_BA_BITS + carbon_current_ROW_BITS + carbon_current_COL_BITS;
   // use 64'b1 here to force verilog sizing rule to do the shift and subtraction with enough bits to always fill MAX_ADDR_BITS (we assume 64 > MAX_ADDR_BITS)
   wire [MAX_ADDR_BITS-1:0] 				carbon_current_COMBINED_ADDR_BITS_mask = (64'b1 << carbon_current_COMBINED_ADDR_BITS)-1;

   // ******************************************************************************************
   // Within a rank, a single wide logical memory is implemented as a set of block memories
   // However, multiple device models are used to create the full width, so each device services 
   // a slice of the overall memory.  This places a unique requirement on the block memory to
   // have 2n ports so each of n "device slices" can be independently accessed.  
   // (The factor of 2 is due to double data rate mechanism for each device)
   //  
   // ******************************************************************************************

   // Top level connections to full block mem
   wire [DEVICES_PER_RANK-1:0] 				bm_wdclk;
   reg  [DEVICES_PER_RANK-1:0] 				bm_we[0:NUM_BLOCKS-1];
   wire [DEVICES_PER_RANK*BLOCK_ADDR_BITS-1:0] 		bm_addr_a;
   wire [DATA_BITS-1:0] 				bm_din_a;
   wire [DATA_BITS-1:0] 				bm_dmask_a;
   wire [DEVICES_PER_RANK*BLOCK_ADDR_BITS-1:0] 		bm_addr_b;
   wire [DATA_BITS-1:0] 				bm_din_b;
   wire [DATA_BITS-1:0] 				bm_dmask_b;

   reg [DATA_BITS-1:0] 					block_dout_1[0:NUM_BLOCKS-1];
   reg [DATA_BITS-1:0] 					block_dout_2[0:NUM_BLOCKS-1];
   reg [DATA_BITS-1:0] 					bm_dout_a;
   reg [DATA_BITS-1:0] 					bm_dout_b;

   genvar 						d;
   generate
      for (d = 0; d < DEVICES_PER_RANK; d = d + 1) begin: device

	 wire [BLOCK_BITS-1:0] 				device_bm_we;
	 wire [BLOCK_BITS-1:0] 				device_bm_block;
	 
	 always @(*) begin
	    bm_dout_a <= block_dout_1[device_bm_block];
	    bm_dout_b <= block_dout_2[device_bm_block];
	 end


	 integer 					i;
	 always @(*)
	   for (i = 0; i < NUM_BLOCKS; i = i + 1) begin
	      bm_we[i][d] = device_bm_we[i];
	   end
	   
	 // Connect each device to its appropriate slice of the data or address bus
	 carbon_gddr5_rank_device #(.DEVICE_BYTE_LANES(DEVICE_BYTE_LANES),
				    .DEVICES_PER_RANK(DEVICES_PER_RANK),
				    .MAX_ADDR_BITS(MAX_ADDR_BITS),
				    .BLOCK_BITS(BLOCK_BITS),
				    .NUM_BLOCKS(NUM_BLOCKS),
				    .BLOCK_ADDR_BITS(BLOCK_ADDR_BITS)) device(
									      .reset_n(reset_n[d]),
									      .ck(ck),
									      .wck(wck),
									      .ras_n(ras_n[d]),
									      .cas_n(cas_n[d]),
									      .we_n(we_n[d]),
									      .cke(cke[d]),
									      .cs_n(cs_n[d]),
									      .a(a[12*(d+1)-1:12*(d)]),
									      .abi_n(abi_n[d]),
									      .dq(dq[8*DEVICE_BYTE_LANES*(d+1)-1:8*DEVICE_BYTE_LANES*(d)]),
									      .dbi_n(dbi_n[DEVICE_BYTE_LANES*(d+1)-1:DEVICE_BYTE_LANES*(d)]),
									      .edc(edc[DEVICE_BYTE_LANES*(d+1)-1:DEVICE_BYTE_LANES*(d)]),
									      .bm_wdclk(bm_wdclk[d]),
									      .bm_we(device_bm_we),
									      .bm_addr_a(bm_addr_a[BLOCK_ADDR_BITS*(d+1)-1:BLOCK_ADDR_BITS*(d)]),
									      .bm_din_a(bm_din_a[8*DEVICE_BYTE_LANES*(d+1)-1:8*DEVICE_BYTE_LANES*(d)]),
									      .bm_dmask_a(bm_dmask_a[8*DEVICE_BYTE_LANES*(d+1)-1:8*DEVICE_BYTE_LANES*(d)]),
									      .bm_dout_a(bm_dout_a[8*DEVICE_BYTE_LANES*(d+1)-1:8*DEVICE_BYTE_LANES*(d)]),
									      .bm_addr_b(bm_addr_b[BLOCK_ADDR_BITS*(d+1)-1:BLOCK_ADDR_BITS*(d)]),
									      .bm_din_b(bm_din_b[8*DEVICE_BYTE_LANES*(d+1)-1:8*DEVICE_BYTE_LANES*(d)]),
									      .bm_dmask_b(bm_dmask_b[8*DEVICE_BYTE_LANES*(d+1)-1:8*DEVICE_BYTE_LANES*(d)]),
									      .bm_dout_b(bm_dout_b[8*DEVICE_BYTE_LANES*(d+1)-1:8*DEVICE_BYTE_LANES*(d)]),
									      .bm_block(device_bm_block),
									      .carbon_row_bank_column_format(carbon_row_bank_column_format),
									      .carbon_current_BA_BITS(carbon_current_BA_BITS),
									      .carbon_current_ROW_BITS(carbon_current_ROW_BITS),
									      .carbon_current_COL_BITS(carbon_current_COL_BITS)
									      );
      end
   endgenerate
   
   // ********************************************************************************
   // ********************************************************************************
   // Rank Memory Blocks
   // Since this is DDR, we need to read/write two words (at different addresses) each clock
   // cycle.  We'll call these 1 and 2, referring to their order on the data bus. 
   // This has no relationship to the addresses.  We always read/write both together,
   // so only one write enable is needed.
   //
   // ********************************************************************************
   // ********************************************************************************
   
   genvar                          g;
   generate
      for (g = 0; g < NUM_BLOCKS; g = g + 1) begin : block
         wire [DATA_BITS-1:0]      bm_dout_wire_a;
         wire [DATA_BITS-1:0]      bm_dout_wire_b;
         
         always @(*)
           block_dout_1[g] <= bm_dout_wire_a;
         
         always @(*)
           block_dout_2[g] <= bm_dout_wire_b;

         carbon_gddr5_block_mem #(
				  .DEVICE_BYTE_LANES(DEVICE_BYTE_LANES),
				  .DEVICES_PER_RANK(DEVICES_PER_RANK), 
				  .BLOCK_ADDR_BITS(BLOCK_ADDR_BITS)
				  ) 
	 block_mem(.wdclk(bm_wdclk),
		   .we(bm_we[g]),
		   .addr_a(bm_addr_a),
		   .din_a(bm_din_a),
		   .dmask_a(bm_dmask_a),
		   .dout_a(bm_dout_wire_a),
		   .addr_b(bm_addr_b),
		   .din_b(bm_din_b),
		   .dmask_b(bm_dmask_b),
		   .dout_b(bm_dout_wire_b));
         
      end
   endgenerate
   

   
endmodule

// ********************************************************************************
// Unlike other DDRx models, we need to create device level control logic for each device in the rank, so each "device" is exposed
// This is required as the address/command signals (including byte enables) come from different signals ports so the controller, *MAY*
// not issue the same command to all devices simulataneously as was assumed for the other DDRx models.  Hence, independent state for each device is required.

// Each device will then control a portion of the data path of the single, full width memory module for the rank (implemented as such for debug access)
module carbon_gddr5_rank_device(
				reset_n,
				ck,
				wck,
				ras_n,
				cas_n,
				we_n,
				cke,
				cs_n,
				a,
				abi_n,
				dq,
				dbi_n,
				edc,
				bm_wdclk,
				bm_we,
				bm_addr_a,
				bm_din_a,
				bm_dmask_a,
				bm_dout_a,
				bm_addr_b,
				bm_din_b,
				bm_dmask_b,
				bm_dout_b,
				bm_block,
				carbon_row_bank_column_format,
				carbon_current_BA_BITS,
				carbon_current_ROW_BITS,
				carbon_current_COL_BITS
				);
   
   parameter DEVICE_BYTE_LANES = 4;
   parameter DEVICES_PER_RANK = 1;
   parameter MAX_ADDR_BITS = 34;
   parameter BLOCK_BITS = 1;
   parameter NUM_BLOCKS = 1;
   parameter BLOCK_ADDR_BITS = 0;


   // derived parameters
   parameter BLOCK_BITS_mask = (( 1<< BLOCK_BITS )-1);
   parameter BLOCK_ADDR_BITS_mask = (1<<BLOCK_ADDR_BITS)-1;
   
   // Calculated parameter - note this DATA_BITS is PER DEVICE
   parameter DATA_BITS = 8*DEVICE_BYTE_LANES;

   
   input 					        reset_n;
   input 						ck;   
   input 						wck;  
   input 						ras_n;
   input 						cas_n;
   input 						we_n;
   input 						cke;
   input 						cs_n;
   input [11:0] 					a; // 12 = maximum width supported - any unused bit should be tied to 0
   input 						abi_n;
   inout [(8*DEVICE_BYTE_LANES)-1:0] 			dq; // carbon observeSignal
   inout [DEVICE_BYTE_LANES-1:0] 			dbi_n;
   output [DEVICE_BYTE_LANES-1:0] 			edc;

   // interface to block memory
   output 						bm_wdclk;
   output [NUM_BLOCKS-1:0] 				bm_we;
   output [BLOCK_ADDR_BITS-1:0] 			bm_addr_a;
   output [(8*DEVICE_BYTE_LANES)-1:0] 			bm_din_a;
   output [(8*DEVICE_BYTE_LANES)-1:0] 			bm_dmask_a;
   input [(8*DEVICE_BYTE_LANES)-1:0] 			bm_dout_a;
   output [BLOCK_ADDR_BITS-1:0] 			bm_addr_b;
   output [(8*DEVICE_BYTE_LANES)-1:0] 			bm_din_b;
   output [(8*DEVICE_BYTE_LANES)-1:0] 			bm_dmask_b;
   input [(8*DEVICE_BYTE_LANES)-1:0] 			bm_dout_b;
   output [BLOCK_BITS-1:0] 				bm_block;
   
   input 						carbon_row_bank_column_format;
   input [31:0] 					carbon_current_BA_BITS;
   input [31:0] 					carbon_current_ROW_BITS;
   input [31:0] 					carbon_current_COL_BITS;
   
   wire [31:0] 						carbon_current_BA_BITS_mask = (1 << carbon_current_BA_BITS)-1;
   wire [31:0] 						carbon_current_ROW_BITS_mask = ( 1 << carbon_current_ROW_BITS)-1;
   wire [31:0] 						carbon_current_COL_BITS_mask = ( 1 << carbon_current_COL_BITS)-1;
   
   wire [31:0] 						carbon_current_COMBINED_ADDR_BITS = carbon_current_BA_BITS + carbon_current_ROW_BITS + carbon_current_COL_BITS;
   // use 64'b1 here to force verilog sizing rule to do the shift and subtraction with enough bits to always fill MAX_ADDR_BITS (we assume 64 > MAX_ADDR_BITS)
   wire [MAX_ADDR_BITS-1:0] 				carbon_current_COMBINED_ADDR_BITS_mask = (64'b1 << carbon_current_COMBINED_ADDR_BITS)-1;

   // ********************************************************************************
   // ********************************************************************************
   // Mode registers
   reg [15:0]               MR0_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR1_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR2_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR3_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR4_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR5_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR6_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR7_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR8_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR9_reg; // carbon depositSignal
                                     // carbon observeSignal
   reg [15:0]               MR10_reg; // carbon depositSignal
                                      // carbon observeSignal
   reg [15:0]               MR11_reg; // carbon depositSignal
                                      // carbon observeSignal
   reg [15:0]               MR12_reg; // carbon depositSignal
                                      // carbon observeSignal
   reg [15:0]               MR13_reg; // carbon depositSignal
                                      // carbon observeSignal
   reg [15:0]               MR14_reg; // carbon depositSignal
                                      // carbon observeSignal
   reg [15:0]               MR15_reg; // carbon depositSignal
                                      // carbon observeSignal

   reg [31:0] 		    raw_vendor_id_dq;  // carbon depositSignal
                                               // carbon observeSignal

   initial
     begin
	MR4_reg = 12'h00f;  // EDC Hold Pattern is initialized to 4'b1111
     end
   
   // ********************************************************************************
   // ********************************************************************************
   // Current Input Command Processing

   reg [6:0]  cmd;
   reg [3:0]  ba;
   reg [12:0] addr;

   // ****************************************
   // We need a queue of commands that are pending for upcoming clock
   // cycles. Input commands are captured and all relevant data is placed in a queue
   // for later processing (after the required latency) when the data is available
   // for writing or needs to be driven for reading. The double data rate aspect will 
   // be handled when the command is processed.

   // The queue operates on a system clock, so a single burst command will put multiple commands 
   // in the queue. For example, with a GDDR5 burst length of 8, a there will be two command
   // cycles on which data is transferred (there are 8 active wck edges during 2 command cycles).  
   // In this case, two entries will be inserted into the queue, at offsets N and N+1, where N is
   // the initial latency for the command.
   //
   // The queue depth is chosen to accomodate the worst case latencies

   parameter CMD_QUEUE_DEPTH = 64;
   parameter CMD_QUEUE_BITS = 6;
   
   // ****************************************
   // Command state information
   
   reg [CMD_QUEUE_BITS-1:0] queue_ptr; // Current location in circular queue
   reg [`CMD_BITS-1:0] 	    command_q [0:CMD_QUEUE_DEPTH-1]; // command 
   reg [MAX_ADDR_BITS-1:0]  addr_q [0:CMD_QUEUE_DEPTH-1]; // combined bank, row & column address
   
   parameter MAX_NUM_BANKS = 1 << `MAX_BA_BITS;
   reg [`MAX_ROW_BITS-1:0]  row_addr[0:MAX_NUM_BANKS-1]; // row address captured during bank activation
   reg [MAX_NUM_BANKS-1:0]  needs_precharge; // state info for bank activation precharge checking

   // Registers for reporting errors to C++ interface
   reg [7:0]                    error_code; // carbon observeSignal
   reg [7:0]                    error_bank; // carbon observeSignal

`define COLLISION_ERROR 8'h10
`define PRECHARGE_ERROR 8'h20
`define ACTIVATE_ERROR  8'h30
   
`define UNSUPPORTED_LDFF_CMD_CODE  8'h40
`define UNSUPPORTED_RDTR_CMD_CODE  8'h50
`define UNSUPPORTED_WSM_CMD_CODE   8'h60
`define UNSUPPORTED_WDM_CMD_CODE   8'h70
`define UNSUPPORTED_WRTR_CMD_CODE  8'h80

`define UNSUPPORTED_WCK2CK_MODE_CODE 8'h90
`define UNSUPPORTED_RDQS_MODE_CODE   8'h91
`define UNSUPPORTED_RDCRC_MODE_CODE  8'h92
`define UNSUPPORTED_WRCRC_MODE_CODE  8'h93
`define UNSUPPORTED_DQPREA_MODE_CODE 8'h94
`define UNSUPPORTED_ADT_MODE_CODE    8'h95
`define UNSUPPORTED_MREMF1_MODE_CODE 8'h90
   
   integer                      i;
   initial begin
      queue_ptr = 0;
      // invalidate the queue so false errors are not reported at startup
      for (i = 0; i < CMD_QUEUE_DEPTH; i = i + 1) begin
         command_q[i] = `CMD_INVALID;
         addr_q[i] = 0;
      end
      for (i = 0; i < MAX_NUM_BANKS; i = i + 1) begin
         row_addr[i] = 0;
      end

      needs_precharge = 0;  // don't check on startup
      error_code = 0;
      error_bank = 0;
   end

        
   task check_command_q;
      input [CMD_QUEUE_BITS-1:0] queue_ptr;
      begin
         if (command_q[queue_ptr] != `CMD_INVALID) begin
	   // bursts cannot be interrupted, so report all collisions
            error_code = `COLLISION_ERROR;
            error_bank = 0;
`ifndef CARBON
            $display("Error - command collision occurred\n");
`endif
            $stop;
            error_code = 0;
            error_bank = 0;
         end
      end
   endtask
   
   task check_precharge;
      begin
         if (needs_precharge & (1 << (ba & carbon_current_BA_BITS_mask))) begin
            error_code = `PRECHARGE_ERROR;
            error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
            $display("Error - Activation of bank %d without precharge\n", (ba & carbon_current_BA_BITS_mask));
`endif
            $stop;
            error_code = 0;
            error_bank = 0;
         end
      end
   endtask
   
   task check_activated;
      begin
         if (!(needs_precharge & (1 << (ba & carbon_current_BA_BITS_mask)))) begin
            error_code = `ACTIVATE_ERROR;
            error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
            $display("Error - Command issued to bank %d without prior activation\n", (ba & carbon_current_BA_BITS_mask));
`endif
            $stop;
            error_code = 0;
            error_bank = 0;
         end
      end
   endtask
   
   task unsupported_ldff_cmd;
      begin
         error_code = `UNSUPPORTED_LDFF_CMD_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported LDFF Command detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask
   
   task unsupported_rdtr_cmd;
      begin
         error_code = `UNSUPPORTED_RDTR_CMD_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported RDTR Command detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask
   
   task unsupported_wsm_cmd;
      begin
         error_code = `UNSUPPORTED_WSM_CMD_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported WSM Command detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask
   
   task unsupported_wdm_cmd;
      begin
         error_code = `UNSUPPORTED_WDM_CMD_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported WDM Command detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask
   
   task unsupported_wrtr_cmd;
      begin
         error_code = `UNSUPPORTED_WRTR_CMD_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported WRTR Command detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   // ****************************************
   // Unsupported Mode Bits
   
   task unsupported_wck2ck_mode;
      begin
         error_code = `UNSUPPORTED_WCK2CK_MODE_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported WCK2CK Mode detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   task unsupported_rdqs_mode;
      begin
         error_code = `UNSUPPORTED_RDQS_MODE_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported RDQS Mode detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   task unsupported_rdcrc_mode;
      begin
         error_code = `UNSUPPORTED_RDCRC_MODE_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported RDCRC Mode detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   task unsupported_wrcrc_mode;
      begin
         error_code = `UNSUPPORTED_WRCRC_MODE_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported WRCRC Mode detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   task unsupported_dqprea_mode;
      begin
         error_code = `UNSUPPORTED_DQPREA_MODE_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported DQPREA Mode detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   task unsupported_adt_mode;
      begin
         error_code = `UNSUPPORTED_ADT_MODE_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported ADT Mode detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   task unsupported_mremf1_mode;
      begin
         error_code = `UNSUPPORTED_MREMF1_MODE_CODE;
         error_bank = (ba & carbon_current_BA_BITS_mask);
`ifndef CARBON
         $display("Error - Unsupported MREMF1 Mode detected\n");
`endif
         $stop;
         error_code = 0;
         error_bank = 0;
      end
   endtask

   // ****************************************
   // These are helper wires/regs that reflect the current state during input command processing

   // column address 
   wire [`MAX_COL_BITS-1:0] col_addr = addr & carbon_current_COL_BITS_mask;

   reg [MAX_ADDR_BITS-1:0]  comb_addr;
   
   // Combined address is the current bank, the saved row for the selected bank and the current column
   always @(*) begin
      if ( carbon_row_bank_column_format )
	 begin
	    comb_addr = ( (row_addr[(ba & carbon_current_BA_BITS_mask)] & carbon_current_ROW_BITS_mask) << (carbon_current_BA_BITS+ carbon_current_COL_BITS)) |
			( (ba                                           & carbon_current_BA_BITS_mask)  << (carbon_current_COL_BITS) ) |
			( (col_addr                                     & carbon_current_COL_BITS_mask) << 0 );
	 end
      else
	begin
	    comb_addr = ( (ba                                           & carbon_current_BA_BITS_mask)  << (carbon_current_ROW_BITS+ carbon_current_COL_BITS) ) |
			( (row_addr[(ba & carbon_current_BA_BITS_mask)] & carbon_current_ROW_BITS_mask) << (carbon_current_COL_BITS)) |
			( (col_addr                                     & carbon_current_COL_BITS_mask) << 0 );
	   
	end
   end
   
   wire [2:0]               WLmrs      =      MR0_reg[`MR0_WLmrs];
   wire [5:0]               CLmrs      =      MR0_reg[`MR0_CLmrs] + 16*MR8_reg[`MR8_CLEHF] + 5;
   
   // Address Bus Inversion active - takes mode register enable into account as well.
   wire 		    abi_active = !abi_n & MR1_reg[`MR1_ABI];

   // ********************************************************************************
   // ********************************************************************************
   // Process input commands and either act on them or place them in queue for 
   // processing after appropriate latency

   // Due to the dual rate address portion, we need to sample the command signals on the posedge of ck,
   // then get the rest of the address on the negedge of ck.  Therefore we will not process the COMPLETE command
   // until the negedge of ck.  The loading of command q is done on the negedge of ck, while the unloading is done on 
   // the posedge of ck.
   // Additionally, the ABI signal is taken into account here so invert the values as needed

   // expand the multiplexed address input to the logical signals - See Table 6 (Section 4.1 of JEDEC Standard No. 212 - Dec 2009) 
   //                                   ba3   ba2   ba1   ba0
   wire [3:0] 		    ba_wire = { a[3], a[4], a[5], a[2] };
   //                                   a12   a11   a10   a9    a8    a[7:0]
   wire [12:0] 		    a_wire =  { a[8], a[6], a[0], a[1], a[7], a[7:0] };

   // The actual command as a concatenation of the appropriate signals
   wire [6:0]               cmd_wire = {cs_n, ras_n, cas_n, we_n, a_wire[11], a_wire[10], a_wire[8]};


   always @(posedge ck) begin
      cmd      <= abi_active ? (cmd_wire ^ 7'h07): cmd_wire;
      ba       <= abi_active ? ~ba_wire : ba_wire;
      addr[12:8] <= abi_active ? ~a_wire[12:8] : a_wire[12:8];
   end

   always @(negedge ck) begin
      addr[7:0] <= abi_active ? ~a_wire[7:0] : a_wire[7:0];
   end
   
   // The read and write operations will add new commands to the queue. 
   // There will be 2 entries for burst 8 operations since the wck is 2x the ck frequency.
   // We need to add 1 to each index as well, because we're updating the queue pointer
   // as well this cycle.  
   //
   // A base address is saved for the two pairs of data transferred each cycle.
   // The actual address for each data element will be determined when the entry
   // is extracted from the queue.  
   
   always @(posedge ck) begin
      if (cke) begin

         if ((cmd & `MASK_CMD) == `CMD_MRS) begin
	    // check if this is MR15, which can always be written
	    if ((ba & 4'hf) == 4'd15) 
	      begin
		 MR15_reg <= addr;
		 if (addr[`MR15_MREMF1])  `UNSUPPORTED_MREMF1_MODE;
		 if (addr[`MR15_ADT])     `UNSUPPORTED_ADT_MODE;
	      end
	    else
	      // check to see if MR0-14 accesses are enabled (we only allow MF=0)
	      if (~MR15_reg[8])
		begin
		   // Store in the appropriate mode register - always allow store, but indicate if not supported
		   case (ba & 4'hf)  // always use all 4 BA bits, regardless of current BA settings
		     4'd0: MR0_reg <= addr;
		     4'd1: MR1_reg <= (addr & 12'h7ff); // MR1_reg[11] (PLL/DLL Reset) is self clearing, so don't set it
		     4'd2: MR2_reg <= addr;
		     4'd3: begin
			MR3_reg <= addr;
			if (addr[`MR3_WCK2CK]) `UNSUPPORTED_WCK2CK_MODE;
			if (addr[`MR3_RDQS])   `UNSUPPORTED_RDQS_MODE;
		     end
		     4'd4: begin
			MR4_reg <= addr;
			if (addr[`MR4_RDCRC]) `UNSUPPORTED_RDCRC_MODE;
			if (addr[`MR4_WRCRC]) `UNSUPPORTED_WRCRC_MODE;
		     end
		     4'd5: MR5_reg <= addr;
		     4'd6: MR6_reg <= addr;
		     4'd7: begin
			MR7_reg <= addr;
			if (addr[`MR7_DQPREA]) `UNSUPPORTED_DQPREA_MODE;
		     end
		     4'd8: MR8_reg <= addr;
		     4'd9: MR9_reg <= addr;
		     4'd10: MR10_reg <= addr;
		     4'd11: MR11_reg <= addr;
		     4'd12: MR12_reg <= addr;
		     4'd13: MR13_reg <= addr;
		     4'd14: MR14_reg <= addr;
		     default: MR14_reg <= 16'hdead;  // should never happen, but write to reserved reg if it does...
		   endcase
		end // if (~MR15_reg[8])
	 end
	 
         else if ((cmd & `MASK_CMD) == `CMD_ACT) begin
            check_precharge; // Check that the bank is precharged
            row_addr[(ba & carbon_current_BA_BITS_mask)] <= (addr & carbon_current_ROW_BITS_mask); // Save the row address
            needs_precharge <= needs_precharge | (1 << (ba & carbon_current_BA_BITS_mask));
	 end
	    
         else if (((cmd & `MASK_CMD_FULL) == `CMD_RD) || ((cmd & `MASK_CMD_FULL) == `CMD_RDA)) begin
            check_activated; // Check that the bank is activated

            check_command_q((queue_ptr + CLmrs + 0) & (CMD_QUEUE_DEPTH-1));
            command_q[(queue_ptr + CLmrs + 0) & (CMD_QUEUE_DEPTH-1)] <= `CMD_RD; 
            addr_q[(queue_ptr + CLmrs + 0) & (CMD_QUEUE_DEPTH-1)] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0]};
	    
            // Final pair is a combination of the two
            check_command_q((queue_ptr + CLmrs + 1) & (CMD_QUEUE_DEPTH-1));
            command_q[(queue_ptr + CLmrs + 1) & (CMD_QUEUE_DEPTH-1)] <= `CMD_RD;
            addr_q[(queue_ptr + CLmrs + 1) & (CMD_QUEUE_DEPTH-1)] <= {((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ((comb_addr[2:0] + 3'b100) & 3'h7) };

	    // handle AUTOPRECHARGE aspect now
	    if ((cmd & `MASK_CMD_FULL) == `CMD_RDA) begin
               needs_precharge <= needs_precharge & (8'hff ^ ( 1 << (ba & carbon_current_BA_BITS_mask)));
	    end
	 end

         else if ((cmd & `MASK_CMD_FULL) == `CMD_LDFF) begin
	    `UNSUPPORTED_LDFF_CMD;
	 end

         else if ((cmd & `MASK_CMD_FULL) == `CMD_RDTR) begin
	    `UNSUPPORTED_RDTR_CMD;
	 end

         else if (((cmd & `MASK_CMD_FULL) == `CMD_WOM) || ((cmd & `MASK_CMD_FULL) == `CMD_WOMA)) begin
            check_activated; // Check that the bank is activated
	    
            check_command_q((queue_ptr + WLmrs + 0) & (CMD_QUEUE_DEPTH-1));
            command_q[(queue_ptr + WLmrs + 0) & (CMD_QUEUE_DEPTH-1)] <= `CMD_WOM;
            addr_q[(queue_ptr + WLmrs + 0) & (CMD_QUEUE_DEPTH-1)] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), comb_addr[2:0]};  
            
            check_command_q((queue_ptr + WLmrs + 1) & (CMD_QUEUE_DEPTH-1));
            command_q[(queue_ptr + WLmrs + 1) & (CMD_QUEUE_DEPTH-1)] <= `CMD_WOM;
            addr_q[(queue_ptr + WLmrs + 1) & (CMD_QUEUE_DEPTH-1)] <= { ((comb_addr&carbon_current_COMBINED_ADDR_BITS_mask)>>3), ((comb_addr[2:0] + 3'b100) & 3'h7) };
               
	    // handle AUTOPRECHARGE aspect now
	    if ((cmd & `MASK_CMD_FULL) == `CMD_WOMA) begin
               needs_precharge <= needs_precharge & (8'hff ^ ( 1 << (ba & carbon_current_BA_BITS_mask)));
	    end

	 end

         else if (((cmd & `MASK_CMD_FULL) == `CMD_WSM) || ((cmd & `MASK_CMD_FULL) == `CMD_WSMA)) begin
            check_activated; // Check that the bank is activated
	    `UNSUPPORTED_WSM_CMD;
	 end

         else if (((cmd & `MASK_CMD_FULL) == `CMD_WDM) || ((cmd & `MASK_CMD_FULL) == `CMD_WDMA)) begin
            check_activated; // Check that the bank is activated
	    `UNSUPPORTED_WDM_CMD;
	 end
	 
         else if ((cmd & `MASK_CMD_FULL) == `CMD_WRTR) begin
            check_activated; // Check that the bank is activated
	    `UNSUPPORTED_WRTR_CMD;
	 end

         else if ((cmd & `MASK_CMD_PRE) == `CMD_PRECHARGE) begin
            needs_precharge <= needs_precharge & (8'hff ^ ( 1 << (ba & carbon_current_BA_BITS_mask)));
	 end
	 
         else if ((cmd & `MASK_CMD_PRE) == `CMD_PRECHARGE_ALL) begin
            needs_precharge <= 0;
	 end

         // Clear finished entries from the queue.  We only need to
         // invalidate the command.
         command_q[queue_ptr] <= `CMD_INVALID;
         queue_ptr <= queue_ptr + 1;
      end
   end

   // ********************************************************************************
   // ********************************************************************************
   // Deferred Command Processing

   reg [DATA_BITS-1:0]      dq_out_internal_sm;
   reg                      dq_oe_internal_sm;

   // ********************************************************************************
   // Bidirectional signals input path

   wire [DATA_BITS-1:0]     dq_in = dq;
   wire [DEVICE_BYTE_LANES-1:0] dbi_n_in = dbi_n;

   reg [DATA_BITS-1:0] 		dq_out; //  = dq_out_internal_sm;
   reg [DEVICE_BYTE_LANES-1:0] 	dbi_n_out;
   wire 			vendor_id_oe = (MR3_reg[`MR3_INFO] == 2'b01) ? 1'b1 : 1'b0;
   wire 			dq_oe = dq_oe_internal_sm | vendor_id_oe;

   always @(*) begin
      dq_out <= dq_oe_internal_sm ? (dq_out_internal_sm ^ get_dbi_n_mask(dbi_n_out)) :
		vendor_id_oe ? raw_vendor_id_dq[DATA_BITS-1:0]: 0;
   end
   
   assign dq = dq_oe ? dq_out : { DATA_BITS{1'bz} };
   assign dbi_n = dq_oe ? dbi_n_out : { DEVICE_BYTE_LANES{1'bz} };
   
   // Capture first half of data transfer using rising edge of wck
   // This value and the second half will be written to memory on falling edge of wck
   wire [DATA_BITS-1:0] 	dq_in_delayed = dq_in;
   reg [DATA_BITS-1:0] 		dq_in_delayed_r;
   wire [DEVICE_BYTE_LANES-1:0] dbi_n_in_delayed = dbi_n;
   reg [DEVICE_BYTE_LANES-1:0] 	wdbi_active_delayed_r;
   
   // Read/Write Data Bus Inversion active - takes mode register enble into account as well
   wire [DEVICE_BYTE_LANES-1:0 ] wdbi_active_delayed = ~dbi_n_in_delayed & { (DEVICE_BYTE_LANES) {MR1_reg[`MR1_WDBI]}};

   function [DATA_BITS-1:0] get_dbi_n_mask;
      input [DEVICE_BYTE_LANES-1:0] dbi_n;
      integer 			    i;
      begin
	 get_dbi_n_mask = 0;
	 for (i = 0; i < DEVICE_BYTE_LANES; i = i + 1)
	   begin
	      if (~dbi_n[i])
		get_dbi_n_mask = get_dbi_n_mask | (8'hff << (8*i));
	   end
      end
   endfunction 
   
   function [DEVICE_BYTE_LANES-1:0] get_dbi_n;
      input [DATA_BITS-1:0] 	 dq_out;
      integer 			 count;
      integer 			 i, j;
      begin
	 get_dbi_n = 0;
	 for (i = 0; i < DEVICE_BYTE_LANES; i = i + 1) 
	   begin
	      count = 0;
	      for (j = 0; j < 8; j = j + 1) begin
		 if (dq_out[8*i+j])
		   count = count + 1; // note we are count ones here
	      end
	      if (count > 4)
		get_dbi_n = get_dbi_n | (1 << i);  // do not invert this case
	   end
      end
   endfunction 
   
   always @(*) begin
      if (MR1_reg[`MR1_RDBI]) 
	dbi_n_out <= get_dbi_n(dq_out_internal_sm);
      else
	dbi_n_out <= { {DEVICE_BYTE_LANES}{1'b1} };
   end
   
   always @(negedge wck) begin
      dq_in_delayed_r <= dq_in_delayed;
      wdbi_active_delayed_r <= wdbi_active_delayed;
   end

   reg  [8*DEVICE_BYTE_LANES-1:0] wdbi_xor_mask;
   reg  [8*DEVICE_BYTE_LANES-1:0] wdbi_xor_mask_r;

   integer 			  j;
   always @(*) begin
      wdbi_xor_mask = 0;
      wdbi_xor_mask_r = 0;
      for ( j = 0; j < DEVICE_BYTE_LANES; j = j + 1 ) begin
	 if (wdbi_active_delayed[j])
	   wdbi_xor_mask = wdbi_xor_mask | (8'hff << (8*j));
	 if (wdbi_active_delayed_r[j])
	   wdbi_xor_mask_r = wdbi_xor_mask | (8'hff << (8*j));
      end
   end
   
   // ****************************************
   // Look at the head of the queue to determine what commands are currently ready for execution
   wire [`CMD_BITS-1:0]          curr_command = command_q[queue_ptr];
   wire [MAX_ADDR_BITS:0]        curr_addr = addr_q[queue_ptr];  // Note this is 1 larger than it needs to be to avoid compiler warning on curr_addr[] below
   wire [BLOCK_BITS-1:0]         curr_block = (NUM_BLOCKS > 1) ? ((curr_addr>>BLOCK_ADDR_BITS) & BLOCK_BITS_mask) : 0;

   // interface signals to block memories
   reg [NUM_BLOCKS-1:0]          bm_we;
   reg [BLOCK_ADDR_BITS-1:0]     bm_addr_a;
   reg [BLOCK_ADDR_BITS-1:0]     bm_addr_b;
   reg [(8*DEVICE_BYTE_LANES)-1:0] bm_din_a;
   reg [(8*DEVICE_BYTE_LANES)-1:0] bm_din_b;
   reg [(8*DEVICE_BYTE_LANES)-1:0] bm_dmask_a;
   reg [(8*DEVICE_BYTE_LANES)-1:0] bm_dmask_b;
   
   reg 				   bm_wdclk;
   reg [BLOCK_BITS-1:0] 	   bm_block;
   
   // address offset
   reg [1:0] 			   addr_offset; // This is used to allow 4 DDR transfers per ck cycle

   always @(ck) begin
      if (ck)
	addr_offset <= 0;
      else
	addr_offset <= 2;
   end
   
   // Generate the values for the blocks
   always @(*) begin
      // we need to wrap on the 8-word boundary.
      bm_addr_a = (((curr_addr & BLOCK_ADDR_BITS_mask)>>3)<<3) | ((curr_addr[2:0] + addr_offset) & 3'b111) ;
      bm_addr_b = 'h0;
      bm_wdclk = 1;
      bm_block = curr_block;
      bm_we = 'h0;
      dq_out_internal_sm = 'h0;
      dq_oe_internal_sm = 'h0;
      if (curr_command == `CMD_WOM) begin
         // Generate write enable to the correct block
         bm_wdclk = wck;
         bm_we = 1'b1 << curr_block;
         // Low address is one after the high address but
         // we need to wrap on the 8-word boundary.
         bm_addr_b = (((curr_addr & BLOCK_ADDR_BITS_mask)>>3)<<3) | ((curr_addr[2:0] + 3'b001 + addr_offset) & 3'b111) ;
      end
      else if (curr_command == `CMD_RD) begin
         dq_oe_internal_sm = 1;
         // Low address is one after the high address, but
         // we need to wrap on the 8-word boundary.
         bm_addr_b = (((curr_addr & BLOCK_ADDR_BITS_mask)>>3)<<3) | ((curr_addr[2:0] + 3'b001 + addr_offset) & 3'b111) ;
         // Select high or low output from the correct block
         dq_out_internal_sm = wck ? bm_dout_a : bm_dout_b;
      end
   end
   
   always @(*) begin
      // First write data is what we registered earlier.  
      // Second write data is what's on the bus now.
      // both apply the appropriate xor mask based on the wdbi setting
      bm_din_a = dq_in_delayed_r ^ wdbi_xor_mask_r;
      bm_din_b = dq_in_delayed ^ wdbi_xor_mask;
      bm_dmask_a = { DATA_BITS{1'b1} };
      bm_dmask_b = { DATA_BITS{1'b1} };
   end

   // ***************************************************************************
   // EDC Driver
   //
   // Currently only the hold pattern (and inverted 1/3) are supported on the EDC pins

   reg [DEVICE_BYTE_LANES-1:0] edc_hold;
   
   always @(*)
     begin
	if (ck) 
	  begin
	     edc_hold <= { DEVICE_BYTE_LANES {wck ? MR4_reg[`MR4_EDC_HOLD0] : MR4_reg[`MR4_EDC_HOLD1] ^ MR4_reg[`MR4_EDC13Inv]}};
	  end
	else 
	  begin
	     edc_hold <= { DEVICE_BYTE_LANES {wck ? MR4_reg[`MR4_EDC_HOLD2] : MR4_reg[`MR4_EDC_HOLD3] ^ MR4_reg[`MR4_EDC13Inv]}};
	  end 
     end // always @ (*)

   assign edc = edc_hold;
   
   
endmodule 

// ***************************************************************************
// ***************************************************************************
// Simple model of a RAM, to be used for each block.
// Dual ported to allow a DDR transfer to be converted into a SDR transfer
// A single Write Enable is provided as both ports will be written at the same time
// Separate write and read address signal are provided, as well as write address clock
// This allows we/waddr signals to be sampled independently from the din/dmask, allowing
// the driving device to determine the ultimate timing for write data

module carbon_gddr5_block_mem(wdclk, we, addr_a, din_a, dmask_a, dout_a, addr_b, din_b, dmask_b, dout_b);
   
   parameter DEVICE_BYTE_LANES = 4;
   parameter DEVICES_PER_RANK = 1;
   parameter BLOCK_ADDR_BITS = 16;

   parameter DATA_BITS = 8*DEVICES_PER_RANK*DEVICE_BYTE_LANES;
   parameter DEPTH = 1 << BLOCK_ADDR_BITS;

   input [DEVICES_PER_RANK-1:0]  wdclk; // write data clock - data sampled on posedge only
   input [DEVICES_PER_RANK-1:0]  we; // write enable

   wire [DEVICES_PER_RANK-1:0]   wdclk_in = wdclk;
   
   // Port A
   input [DEVICES_PER_RANK*BLOCK_ADDR_BITS-1:0]   addr_a; // carbon observeSignal
   input [DATA_BITS-1:0]    dmask_a; // data bit mask for write operations - high mask bit means bit will be written
   input [DATA_BITS-1:0]    din_a;
   output [DATA_BITS-1:0]   dout_a;

   // Port B
   input [DEVICES_PER_RANK*BLOCK_ADDR_BITS-1:0]   addr_b;
   input [DATA_BITS-1:0]    dmask_b;
   input [DATA_BITS-1:0]    din_b;
   output [DATA_BITS-1:0]   dout_b;

   // Main memory storage
   reg [DATA_BITS-1:0] 	    mem [0:DEPTH-1]; // carbon depositSignal
                                             // carbon observeSignal

   // For each slice, generate the logic needed to access that part of the slice

   genvar 		    g;
   generate
      for (g = 0; g < DEVICES_PER_RANK; g = g + 1) begin : block_mem_slice
	 // Read output data
	 wire [BLOCK_ADDR_BITS-1:0] slice_addr_a = addr_a[(BLOCK_ADDR_BITS*(g+1))-1:(BLOCK_ADDR_BITS*(g))];
	 wire [BLOCK_ADDR_BITS-1:0] slice_addr_b = addr_b[(BLOCK_ADDR_BITS*(g+1))-1:(BLOCK_ADDR_BITS*(g))];

	 wire [DATA_BITS-1:0] 	    full_mem_line_rdata_a = mem[slice_addr_a];
	 wire [DATA_BITS-1:0] 	    full_mem_line_rdata_b = mem[slice_addr_b];
	 
	 assign dout_a[(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))] = full_mem_line_rdata_a[(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))];
	 assign dout_b[(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))] = full_mem_line_rdata_b[(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))];

	 
	 // Data write operations
	 wire [DATA_BITS-1:0] full_mem_line_wdata_a = (din_a & dmask_a) | (full_mem_line_rdata_a & ~dmask_a);
	 wire [DATA_BITS-1:0] full_mem_line_wdata_b = (din_b & dmask_b) | (full_mem_line_rdata_b & ~dmask_b);
 
	 
	 always @(posedge wdclk_in[g]) begin
	    if (we[g]) begin
	       mem[slice_addr_a][(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))] <= full_mem_line_wdata_a[(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))];
	       mem[slice_addr_b][(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))] <= full_mem_line_wdata_b[(8*DEVICE_BYTE_LANES*(g+1))-1:(8*DEVICE_BYTE_LANES*(g))];
	    end
	 end
      end
   endgenerate

   
endmodule        


`undef MAX_BA_BITS
`undef MAX_ROW_BITS
`undef MAX_COL_BITS
`undef CMD_BITS
`undef MASK_CMD_INVALID
`undef MASK_CMD
`undef MASK_CMD_FULL
`undef MASK_CMD_PRE
`undef CMD_INVALID
`undef CMD_NOP
`undef CMD_MRS
`undef CMD_ACT
`undef CMD_RD
`undef CMD_RDA
`undef CMD_LDFF
`undef CMD_RDTR
`undef CMD_WOM
`undef CMD_WOMA
`undef CMD_WSM
`undef CMD_WSMA
`undef CMD_WDM
`undef CMD_WDMA 
`undef CMD_WRTR
`undef CMD_PRECHARGE
`undef CMD_PRECHARGE_ALL
`undef MR0_WLmrs
`undef MR0_CLmrs
`undef MR1_RDBI
`undef MR1_WDBI
`undef MR1_ABI
`undef MR3_WCK2CK
`undef MR3_RDQS
`undef MR3_INFO
`undef MR4_EDC_HOLD
`undef MR4_EDC_HOLD0
`undef MR4_EDC_HOLD1
`undef MR4_EDC_HOLD2
`undef MR4_EDC_HOLD3
`undef MR4_CRCWL
`undef MR4_CRCRL
`undef MR4_RDCRC
`undef MR4_WRCRC
`undef MR4_EDC13Inv
`undef MR7_DQPREA
`undef MR8_CLEHF
`undef MR15_MREMF0
`undef MR15_MREMF1
`undef MR15_ADT

`undef COLLISION_ERROR
`undef PRECHARGE_ERROR
`undef ACTIVATE_ERROR
   
`undef UNSUPPORTED_LDFF_CMD_CODE
`undef UNSUPPORTED_RDTR_CMD_CODE
`undef UNSUPPORTED_WSM_CMD_CODE
`undef UNSUPPORTED_WDM_CMD_CODE
`undef UNSUPPORTED_WRTR_CMD_CODE

`undef UNSUPPORTED_WCK2CK_MODE_CODE
`undef UNSUPPORTED_RDQS_MODE_CODE
`undef UNSUPPORTED_RDCRC_MODE_CODE
`undef UNSUPPORTED_WRCRC_MODE_CODE
`undef UNSUPPORTED_DQPREA_MODE_CODE
`undef UNSUPPORTED_ADT_MODE_CODE
`undef UNSUPPORTED_MREMF1_MODE_CODE

`undef UNSUPPORTED_LDFF_CMD
`undef UNSUPPORTED_RDTR_CMD
`undef UNSUPPORTED_WSM_CMD
`undef UNSUPPORTED_WDM_CMD
`undef UNSUPPORTED_WRTR_CMD

`undef UNSUPPORTED_WCK2CK_MODE
`undef UNSUPPORTED_RDQS_MODE
`undef UNSUPPORTED_RDCRC_MODE
`undef UNSUPPORTED_WRCRC_MODE
`undef UNSUPPORTED_DQPREA_MODE
`undef UNSUPPORTED_ADT_MODE
`undef UNSUPPORTED_MREMF1_MODE


