project(CMS)

if(NOT(${WIN32}))
  add_definitions(-DCARBON_USE_SYS_INCLUDES)
endif()

set(LOG4NET_VER "log4net-1.2.11")
set(NUNIT_VER "NUnit-2.6")

if(${WIN32})
  set(NUNIT_HOME "$ENV{TOOLS_DRIVE}/windows/${NUNIT_VER}")
  set(LOG4NET_HOME "$ENV{TOOLS_DRIVE}/windows/${LOG4NET_VER}")
else()
  set(NUNIT_HOME "/tools/windows/${NUNIT_VER}")
  set(LOG4NET_HOME "/tools/windows/${LOG4NET_VER}")
endif()

set(LOG4NET_ASSY_REF "${LOG4NET_HOME}/bin/mono/2.0/release")

set(MONO /tools/linux/mono-2.10.8)

if(NOT(${WIN32}))
  set(MSBUILD ${MONO}/bin/xbuild)
else()
  set(MSBUILD set LIB=&& call \"%VS100COMNTOOLS%/vsvars32.bat\" && msbuild)
endif()


if("${ARCH}" STREQUAL "Linux")
  set(archFlags "-march=i686")
elseif("${ARCH}" STREQUAL "Linux64")
  set(archFlags "-m64 -D__LP64__ -DCHEETAH64")
endif()


if(${MSVC})
  add_definitions(/DWIN32=1 /D_REENTRANT)
  set(CMAKE_CXX_FLAGS
      "/MD /GR /WL /FC"
      )
  set(CMAKE_C_FLAGS
      "/MD /GR /WL /FC"
      )
else()
  set(CMAKE_CXX_FLAGS
      "-Wall -Werror -W ${archFlags}"
      )


  if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DCDB")
  endif()


  if(${WIN32})
    set(CMAKE_C_FLAGS
        "-std=gnu99 -pipe -Wall -Wextra -Werror ${archFlags}"
        )
  else()
    set(CMAKE_C_FLAGS
        "-std=gnu99 -fgnu89-inline -pipe -Wall -Wextra -Werror ${archFlags}"
        )
  endif()
  
  # Default release flags have -DNDEBUG, which doesn't work for us
  # because we have a few live asserts.
  set(CMAKE_CXX_FLAGS_RELEASE
      "-O3 -fno-omit-frame-pointer -finline-functions"
      )
  set(CMAKE_C_FLAGS_RELEASE
      "-O3 -fno-omit-frame-pointer -finline-functions"
      )
endif()

set(VerificIncludes
                   ${CARBON_HOME}/src/Vendors/verific/commands
                   ${CARBON_HOME}/src/Vendors/verific/containers
                   ${CARBON_HOME}/src/Vendors/verific/util
                   ${CARBON_HOME}/src/Vendors/verific/tclmain
                   ${CARBON_HOME}/src/Vendors/verific/verilog
                   ${CARBON_HOME}/src/Vendors/verific/vhdl
                   ${CARBON_HOME}/src/Vendors/verific/vhdl_sort
)


include_directories(
                   ${CMAKE_SOURCE_DIR}/inc
                   ${TOOLS_DIR}/include
                   ${CARBON_HOME}/include
                   ${CARBON_HOME}/src/verific2nucleus
                   ${CMAKE_BINARY_DIR}
                   ${CARBON_OBJ}
                   ${VerificIncludes}
                   )
if(${WIN32})
  include_directories(//dfs/acton/tools/win/include
                      ${TOOLS_DIR}/zlib/src)
endif()


# Include the target to copy files from the sandbox to $CARBON_HOME
include(PrepCarbonHome.cmake)
# and the post-compile targets
include(PostCompile.cmake)

add_subdirectory(hdl)
add_subdirectory(util)
add_subdirectory(codegen)
add_subdirectory(backend)
add_subdirectory(bdd)
add_subdirectory(pli-wrapper)
add_subdirectory(carbonsim)
add_subdirectory(langcpp)
add_subdirectory(compiler_driver)
# TODO add_subdirectory(doc)
add_subdirectory(exprsynth)
add_subdirectory(flow)
add_subdirectory(globopts)
add_subdirectory(interp)
add_subdirectory(iodb)
add_subdirectory(localflow)
add_subdirectory(modshell)
add_subdirectory(nucleus)
# PV stuff is not built by default, and is stale - doesn't compile cleanly
# add_subdirectory(pv)
# not used add_subdirectory(randtestbenchgen)
add_subdirectory(reduce)
add_subdirectory(schedule)
add_subdirectory(cfg)
add_subdirectory(python/Carbon)
add_subdirectory(cmm/ModelstudioAPI)
add_subdirectory(Mono/Mono.TextTemplating)
add_subdirectory(Mono/Carbon.ModelKitServices)
add_subdirectory(Mono/ModelBuilder)
add_subdirectory(Mono/RegisterTool)
add_subdirectory(symtab)
add_subdirectory(vectorise)
add_subdirectory(Vendors)
add_subdirectory(verific2nucleus)
add_subdirectory(svinspector)
add_subdirectory(socdexport)
add_subdirectory(shell)
add_subdirectory(waveform)
add_subdirectory(wavetestbench)
add_subdirectory(wavetestbenchgen)
add_subdirectory(xactors)

# No DesignWare on Windows
if(NOT(${WIN32}))
  add_subdirectory(dw)
endif()

# Testsuites come last because they depend on libraries built above
add_subdirectory(carbonsim/testsuite)
add_subdirectory(codegen/testsuite)
add_subdirectory(exprsynth/testsuite)
add_subdirectory(hdl/testsuite)
add_subdirectory(shell/testsuite)
add_subdirectory(symtab/testsuite)
add_subdirectory(util/testsuite)
add_subdirectory(waveform/testsuite)
