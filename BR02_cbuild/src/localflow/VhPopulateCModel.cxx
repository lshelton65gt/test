// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "localflow/VhdlPopulate.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModel.h"
#include "util/CbuildMsgContext.h"
#include "LFContext.h"


// Method to populate c-model corresponding to the dummy/blackbox entity 
// This function is first copied from the VerilogPopulate::cModule and
// then made necessary changes to work with jaguar node.
Populate::ErrorCode 
VhdlPopulate::cModule( vhNode vh_entity, 
                       const char* modName,
                       NUModule* module,
                       LFContext* context )
{
  ErrorCode err_code = eSuccess;

  // Create a c-model that represents this c-model. This will be
  // shared between all the c-model functions and calls.
  NUCModel* cmodel = 0;
  ErrorCode temp_err_code = cModel(modName, module, context, &cmodel, true);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Get the various module ports. We currently don't support
  // bidirectional pins so we only accumulate the input and output
  // nets. The function will issue an error if there are I/O ports and
  // return that there are invalid ports.
  NetLookup inputNetMap;
  NetLookup outputNetMap;
  temp_err_code = cModulePorts(module, cmodel, context,
                               inputNetMap, 
                               outputNetMap);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Accumulate the users directives and validate them against the
  // module information. If there are any issues, the routine returns
  // failure. If everything is ok, a timing flow map is created which
  // has information mapped from nets to the directives.
  TimingFlowMap timingFlowMap;
  temp_err_code = cModuleDirectives(modName, module, context,
                                    inputNetMap, 
                                    outputNetMap,
                                    timingFlowMap);

  if ( errorCodeSaysReturnNowIfIncomplete( temp_err_code, &err_code) ){
    return err_code;
  }

  // Collect any parameters so that we can pass them to the c-model.
  temp_err_code = cGenerics(cmodel, vh_entity);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Create the c-model function structures. This routine fills the
  // edge functions map and the asyncFn pointer.
  NUCModelInterface* cmodelInterface = cmodel->getCModelInterface();
  CModelFunctions functions;
  temp_err_code = cModelFunctions(context, cmodelInterface, module, 
                                  inputNetMap, 
                                  outputNetMap,
                                  timingFlowMap, 
                                  functions);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Create the statements and always blocks to represent these
  // c-model functions
  
  for (CModelFunctions::iterator p = functions.begin(); 
       p != functions.end(); 
       ++p) 
  {
    const EdgeNet* edgeNet = p->first;
    NUCModelFn* cmodelFn = p->second;
    temp_err_code = cModelAlwaysBlock(module, context, edgeNet, 
                                      cmodel, 
                                      cmodelFn,
                                      inputNetMap, 
                                      outputNetMap);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  // All done with the functions memory
  for (CModelFunctions::iterator p = functions.begin(); 
       p != functions.end(); 
       ++p) 
  {
    delete p->first;
  }

  return err_code;
}


// Method to populate c-model parameter information  with the generic values
// of the corresponding dummy/blackbox entity.
//
Populate::ErrorCode
VhdlPopulate::cGenerics(NUCModel* cmodel, vhNode vh_entity)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;

  // Go through the generics one by one and accumulate the generic
  // name and value.
  vhNode vh_generic_clause = vhGetGenericClause(vh_entity);
  if (NULL == vh_generic_clause) // This entity does not have any generic
  {
    return eSuccess;
  }
  JaguarList vh_generic_list(vhGetFlatGenericList(vh_generic_clause));
  vhNode vh_generic;
  while ((vh_generic = vhGetNextItem(vh_generic_list)) != NULL)
  {
    // Get the name
    NUCModel::Param param;
    JaguarString generic_name(vhGetName(vh_generic));
    param.first = generic_name;

    vhExpr vh_value = vhGetInitialValue(vh_generic);
    if(NULL == vh_value)
    {
      // Report error that this generic is not initialized yet.
      SourceLocator lineNo = locator(vh_generic);
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&lineNo,
                                                      "Generic parameter does not have initial value"),
                       &err_code);
      continue;
    }
    vhExpr evaluatedVal = evaluateJaguarExpr(vh_value);
    int intVal = 0; 
    // Only integer or it's subtype is allowed as generic. See section
    // 8.4.3.2 of the manual IEEE VHDL synthesis STD 1076.6-1999
    // But we may relax this restriction for string type generics.
    // TBD: Add support for string type generics
    temp_err_code = getVhExprValue(evaluatedVal, &intVal, NULL);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    // As of now we support integer generics. 
    // TBD: Add suuport for string type generic as well.
    UtString str;
    str << intVal;

    // Remove any leading and trailing quotes
    StringUtil::strip(&str, "\"");
    param.second = str;

    // Add this information to the c-model instance. We allocated our
    // strings on the stack, so the below routine better make a copy
    // of the strings.
    cmodel->addParam(param);
  } // while
  return err_code;
}

