// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/DumpHierarchy.h"
#include "localflow/TicProtectedNameManager.h"
#include "localflow/VerilogPopulate.h"
#include "localflow/VhdlPopulate.h"
#include "iodb/IODBNucleus.h"

#define XML_ENCODING "ISO-8859-1"

UtString
getLanguageType(mvvLanguageType langType, VHDLCaseT vhdlCase)
{
  UtString language;
  switch (langType) {
  case MVV_VERILOG:
    language = "verilog";
    break;
  case MVV_VHDL:
    language = "vhdl";
    switch (vhdlCase) {
    case eVHDLLower:
      language.lowercase();
      break;
    case eVHDLUpper:
      language.uppercase();
      break;
    case eVHDLPreserve:
      break;
    }
    break;
  default:
    break;
  }
  return language;
}

UtString
getFileType(mvvNode node, mvvLanguageType langType)
{
  File::FileType fileType = File::eUnknown;

  if (node == NULL) {
    return File::getFileType(fileType);
  }

  switch (langType) {
  case MVV_VERILOG:
    switch (veNodeGetObjType(node->castVeNode())) {
    case VE_MODULE:
      fileType = File::eModule;
      break;
    default:
      break;
    }
    break;
  case MVV_VHDL:
    switch (vhGetObjType(node->castVhNode())) {
    case VHENTITY:
      fileType = File::eEntity;
      break;
    case VHCONFIGDECL:
      fileType = File::eConfiguration;
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }
  return File::getFileType(fileType);
}

UtString 
setVhdlCase(const char* name, VHDLCaseT vhdlCase, mvvLanguageType langType = MVV_VHDL) 
{
  UtString str;

  if (name == NULL) {
    return str;
  }

  str = name;

  if (langType == MVV_VERILOG)
    return str;

  switch(vhdlCase)
    {
    case eVHDLLower:
      str.lowercase();
      break;
    case eVHDLUpper:
      str.uppercase();
      break;
    case eVHDLPreserve:
      break;
    }
  return str;
}

// Returns Module/Entity Name for the passed Node
void getHdlEntityName(mvvNode                  entity, 
	 	      mvvLanguageType          langType, 
		      UtString*                nameBuf, 
		      bool*                    isUDP,
                      TicProtectedNameManager* mTicProtectedNameMgr,
		      VHDLCaseT                vhdlCase) 
{
  *isUDP = false;

  if(langType == MVV_VERILOG) {
    veNode ve_node = entity->castVeNode();

    mTicProtectedNameMgr->getVisibleName(ve_node, nameBuf);
    *isUDP = (veNodeGetObjType(ve_node) == VE_UDP);
  }
  else if(langType == MVV_VHDL) {
    vhNode       vh_entity = entity->castVhNode();
    JaguarString entityName(vhGetEntityName(vh_entity));

    *nameBuf = setVhdlCase(entityName, vhdlCase);
    *isUDP   = false;
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }
 
}

bool isTicProtected(mvvNode node, mvvLanguageType langType) 
{
  bool result = false;

  if (node == NULL) {
    return result;
  }

  if(langType == MVV_VERILOG) {
    veNode     ve_node = node->castVeNode();
    UtString   fileName;
    CheetahStr vlogFileName(veNodeGetFileName(ve_node));
    if (vlogFileName != NULL) {
      fileName = vlogFileName;
    }

    if(fileName[0] == '-')
      result = true;
  }

  return result;
}

// Returns the library name associated with design unit or module
UtString getLibraryName(mvvNode         node,
			mvvLanguageType langType,
			VHDLCaseT       vhdlCase) 
{
  if (node == NULL) {
    return "";
  }

  if (langType == MVV_VERILOG) {
    veNode vlogNode = node->castVeNode();
    switch (veNodeGetObjType(vlogNode)) {
    case VE_MODULE:
      {
	UtString name;
	CheetahStr libName(veModuleGetLogicalLibName(vlogNode));
	if (libName != NULL) {
	  name = libName;
	}
	return name;
      }
      break;
    case VE_UDP:
      {
	UtString name;
	CheetahStr libName(veUDPGetLogicalLibName(vlogNode));
	if (libName != NULL) {
	  name = libName;
	}
	return name;
      }
      break;
    default:
      INFO_ASSERT(false, "Unsupported object type");
      break;
    }
  }
  else if (langType == MVV_VHDL) {
    JaguarString libName(vhGetLibName(node->castVhNode()));
    return setVhdlCase(libName, vhdlCase);
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }

  return "";
}

// Returns the library path associated with design unit or module
UtString getLibraryPath(mvvNode node,
			mvvLanguageType langType,
			VHDLCaseT       vhdlCase) 
{
  if (node == NULL) {
    return "";
  }

  if (langType == MVV_VERILOG) {
    UtString libName = getLibraryName(node, langType, vhdlCase);
    UtString path;
    CheetahStr libPath(veGetPhysicalPath(libName.c_str()));
    if (libPath != NULL) {
      path = libPath;
    }
    return path;
  }
  else if (langType == MVV_VHDL) {
    JaguarString libName(vhGetLibName(node->castVhNode()));
    JaguarString libPath(vhGetLibPathName(libName));
    return UtString(libPath);
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }

  return "";
}

// Returns Absolute Source File Name for the passed Node
UtString getFileName(mvvNode         node,
	             mvvLanguageType langType) 
{
  if (node == NULL) {
    return "";
  }

  if(langType == MVV_VERILOG) {
    UtString fileName;
    CheetahStr vlogFileName(veNodeGetAbsoluteSourceFileName(static_cast<veNode>(node)));
    if (vlogFileName != NULL) {
      fileName = vlogFileName;
    }
    return fileName;
  }
  else if(langType == MVV_VHDL) {
    UtString fileName;
    JaguarString vhdlFileName(vhGetAbsoluteSourceFileName(static_cast<vhNode>(node)));
    if (vhdlFileName != NULL) {
      fileName = vhdlFileName;
    }
    if (fileName.empty()) {
      JaguarString vhdlFileName(vhGetSourceFileName(static_cast<vhNode>(node)));
      if (vhdlFileName != NULL) {
	fileName = vhdlFileName;
      }
    }
    return fileName;
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }

  return "";
}

// Returns Start Line Number in the Source File for the passed Node
SInt32 getStartLineNo(mvvNode         node,
	              mvvLanguageType langType) 
{
  SInt32 lineNo = -1;

  if (node == NULL) {
    return lineNo;
  }

  if(langType == MVV_VERILOG) {
    lineNo = veNodeGetLineNumber(static_cast<veNode>(node));
  }
  else if(langType == MVV_VHDL) {
    lineNo = vhGetLineNumber(static_cast<vhNode>(node));
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }

  return lineNo;
}

// Returns End Line Number in the Source File for the passed Node
SInt32 getEndLineNo(mvvNode         node,
	            mvvLanguageType langType) 
{
  SInt32 lineNo = -1;

  if (node == NULL) {
    return lineNo;
  }

  if(langType == MVV_VERILOG) {
    lineNo = veNodeGetEndLineNumber(static_cast<veNode>(node));
  }
  else if(langType == MVV_VHDL) {
    lineNo = vhGetEndLineNumber(static_cast<vhNode>(node));
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }

  return lineNo;
}

// Returns the Type for the passed vhNode
vhNode getVhType(vhNode node, 
		 UtString* parameterTypeName, 
		 UtString* parameterTypeLibrary, 
		 UtString* parameterTypePackage, 
		 VHDLCaseT vhdlCase) 
{
  vhNode              typeInd = vhGetSubTypeInd(node);
  UtString            type(vhStrDecompileNode(typeInd));
  UtString::size_type pos;

  if((pos = type.find("\n")) != UtString::npos)
    type.erase(pos);
  if((pos = type.find_first_of(" ")) != UtString::npos)
    type.erase(pos, 1);
  
  vhNode              typeDecl = vhGetOrgType(typeInd);

  if (VHSELDECL == vhGetObjType(typeDecl)) {

    *parameterTypeLibrary = type.substr(0,type.find_first_of("."));
    *parameterTypePackage = type.substr(type.find_first_of(".")+1, 
					type.find_last_of(".") - (type.find_first_of(".")+1));
    *parameterTypeName    = type.substr(type.find_last_of(".")+1);

    return node;
  }
  else {    
    *parameterTypeName = setVhdlCase(type.c_str(), vhdlCase);
    
    JaguarString libName(vhGetLibName(typeDecl));
    if (libName != NULL) {
      *parameterTypeLibrary = libName;
    }
    
    JaguarString packageName(vhGetActualObjScopeName(typeDecl));
    if (packageName != NULL) {
      *parameterTypePackage = packageName;
    }
    
    return (VHEXTERNAL == vhGetObjType(typeDecl)) ? vhGetActualObj(typeDecl) : typeDecl;
  }
}

// Returns the Type for the passed vhNode
UtString getVeType(veNode node)
{
  if (node == NULL) {
    return "UNSET";
  }

  UtString typeName;

  switch (veNodeGetObjType(node)) {
  case VE_PARAM:
    return getVeType(veParamGetParamValue(node));
    break;
  case VE_LOCALPARAM:
    return getVeType(veLocalParamGetValue(node));
    break;
  case VE_CONST:
    typeName = "INTEGER";
    break;
  case VE_STRING:
    typeName = "STRING";
    break;
  case VE_BASEDNUMBER:
    typeName = "BASED";
    break;
  case VE_REAL:
    typeName = "REAL";
    break;
  case VE_REALTIME:
    typeName = "REALTIME";
    break;
  case VE_EXPNUMBER:
    typeName = "EXPNUMBER";
    break;
  case VE_FLOATNUMBER:
    typeName = "FLOAT";
    break;
  case VE_MULTIPLECONCAT:
    typeName = "MULTIPLECONCAT";
    break;
  case VE_CONCAT:
    typeName = "CONCAT";
    break;
  default:
    typeName = "EXPRESSION";
    break;
  }

  return typeName;
}

UtString getVeActualValue(veNode node)
{
  if (node == NULL) {
    return "";
  }

  UtString valueType = getVeType(node);
  UtString value;

  if (valueType == "INTEGER") {
    value << veExprEvaluateValue(node);
  }
  else if (valueType == "BASED") {
    value << veBasedNumberGetWidth(node);

    value += "'";
    
    if (veBasedNumberGetIsSigned(node)) {
      value += "s";
    }

    switch (veBasedNumberGetBaseType(node)) {
    case VE_BIN:
      value += "b";
      break;
    case VE_OCTAL:
      value += "o";
      break;
    case VE_DECIMAL:
      value += "d";
      break;
    case VE_HEX:
      value += "h";
      break;
    default:
      break;
    }

    CheetahStr parameterValue(veBasedNumberGetValue(node));
    if (parameterValue != NULL) {
      value += parameterValue;
    }

  }
  else if (valueType == "REAL" || valueType == "FLOAT") {
    value << veExprEvaluateValueInDouble(node);
  }
  else if (valueType == "STRING") {
    CheetahStr parameterValue(veExprDecompileToString(node));
    if (parameterValue != NULL) {
      value = parameterValue;
    }
  }
  else {
    veBool isXZ;
    CheetahStr parameterValue(veExprEvaluateValueInString(node, &isXZ, VE_BIN));
    if (parameterValue != NULL) {
      value = parameterValue;
    }
  }
  
  return value;

}

UtString getVeValue(veNode node)
{
  if (node == NULL) {
    return "";
  }

  UtString value;

  CheetahStr parameterValue(veExprDecompileToString(node));
  if (parameterValue != NULL) {
    value = parameterValue;
  }
  
  return value;
}

// Returns the Name for the passed Node Name based on its Object Type
UtString getMvvName(mvvNode                  node,
                    mvvLanguageType          lang,
                    TicProtectedNameManager* mTicProtectedNameMgr,
		    VHDLCaseT                vhdlCase) 
{
  if(node == NULL) {
    return "";
  }

  if(lang == MVV_VERILOG) {
    switch(veNodeGetObjType(static_cast<veNode>(node))) {
    case VE_PARAM: {
      UtString parameterName;
      mTicProtectedNameMgr->getVisibleName(static_cast<veNode>(node), &parameterName);
      return parameterName;
      break; 
    }
    case VE_PARAMCONN:
    case VE_PORTCONNECTION: {
      UtString paramName;
      CheetahStr parameterName(veParamConnGetParamName(static_cast<veNode>(node)));
      if (parameterName != NULL) {
	paramName = parameterName;
      }
      return paramName;
      break;
    }
    case VE_PORT: {
      UtString portName;
      mTicProtectedNameMgr->getVisibleName(static_cast<veNode>(node), &portName);
      return portName;
      break;
    }
    case VE_FILE: {
      UtString fileName;
      mTicProtectedNameMgr->getVisibleName(static_cast<veNode>(node), &fileName);
      return fileName;
      break;
    }
    default: {
      UtString unknown("Unknown Name");
      
      return unknown;
      break;
    }
    }
  }
  else if(lang == MVV_VHDL) {
    vhNode vhdlNode = node->castVhNode();
    switch(vhGetObjType(vhdlNode)) {
    case VHARCH:
      {
	JaguarString name(vhGetArchName(vhdlNode));
	return setVhdlCase(name, vhdlCase);
	break;
      }
    default: 
      {
	JaguarString name(vhGetName(vhdlNode));
	return setVhdlCase(name, vhdlCase);
	break;
      }
    }
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }
  UtString unknown("Unknown Name");

  return unknown;
}

// Returns the Value for the passed Node based on its Object Type
UtString getMvvValue(mvvNode                  node,
                     mvvLanguageType          lang,
                     bool                     elab,
                     TicProtectedNameManager* mTicProtectedNameMgr,
		     VHDLCaseT                vhdlCase) 
{
  if(node == NULL) {
    return "";
  }

  if (lang == MVV_VERILOG) {
    veNode actualNode = static_cast<veNode>(node);
    veNode parentNode;

    if (veNodeGetObjType(actualNode) == VE_NAMEDOBJECTUSE) {
      parentNode = veNamedObjectUseGetParent(actualNode);
    }
    else {
      parentNode = actualNode;
    }

    switch(veNodeGetObjType(parentNode)) {
      case VE_NET: {
        UtString netName;
        mTicProtectedNameMgr->getVisibleName(parentNode, &netName);
        return netName;
        break; 
      }
      case VE_PARAM: {
        veNode     paramValue;
	if (elab) {
	  paramValue = veParamGetCurrentValue(parentNode);
	}
	else {
	  paramValue = veParamGetParamValue(parentNode);
	}
	return getVeActualValue(paramValue);
        break;
      }
      case VE_LOCALPARAM: {
        veNode     paramValue = veLocalParamGetValue(parentNode);
        return getVeActualValue(paramValue);
        break;
      }      
      case VE_PORTCONNECTION:
      case VE_PARAMCONN: {
        veNode     paramValue = veParamConnGetValue(parentNode);
        return getVeActualValue(paramValue);
        break;
      }
      default: {
        return getVeActualValue(actualNode);
        break; 
      }
    }
  }
  else if(lang == MVV_VHDL) {
    switch(vhGetObjType(static_cast<vhNode>(node))) {
      case VHOBJECT:
        return getMvvValue(static_cast<mvvNode>(vhGetActualObj(static_cast<vhNode>(node))), lang, elab, mTicProtectedNameMgr, vhdlCase);
        break;
      case VHCONSTANT:
        if(elab)
          return getMvvValue(static_cast<mvvNode>(vhGetInitialValue(static_cast<vhNode>(node))), lang, elab, mTicProtectedNameMgr, vhdlCase);
        else
          return getMvvValue(static_cast<mvvNode>(vhGetElaboratedInitialValue(static_cast<vhNode>(node))), lang, elab, mTicProtectedNameMgr, vhdlCase);
        break;
      case VHSIGNAL: {
        JaguarString sigName(vhGetName(static_cast<vhNode>(node)));

        return setVhdlCase(sigName, vhdlCase);
        break;
      }
      default:
        return UtString(vhEvaluateExprInString(vhEvaluateExpr(static_cast<vhExpr>(static_cast<vhNode>(node)))));
        break;
    }
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }
  UtString unknown("Unknown Value");

  return unknown;
}


// Returns the Actual Port Expression for the passed Node based on its Object Type
UtString getMvvPortActual(mvvNode         node,
                          mvvLanguageType lang,
			  VHDLCaseT       vhdlCase) 
{
  if(node == NULL) {
    return "";
  }

  if(lang == MVV_VERILOG) {
    UtString            actual;
    CheetahStr          chActual(veExprDecompileToString(static_cast<veNode>(node)));
    if (chActual != NULL) {
      actual = chActual;
    }
    UtString::size_type pos;

    if((pos = actual.find("\n")) != UtString::npos)
      actual.erase(pos);
    if((pos = actual.find("/*")) != UtString::npos)
      actual.erase(pos);
    if((pos = actual.find_first_of(" ")) != UtString::npos)
      actual.erase(pos, 1);
    
    return actual;
  }
  else if(lang == MVV_VHDL) {
    UtString            actual(vhStrDecompileNode(static_cast<vhExpr>(static_cast<vhNode>(node))));
    UtString::size_type pos;
    
    if((pos = actual.find("\n")) != UtString::npos)
      actual.erase(pos);
    if((pos = actual.find("--")) != UtString::npos)
      actual.erase(pos);
    if((pos = actual.find_first_of(" ")) != UtString::npos)
      actual.erase(pos, 1);
    
    return setVhdlCase(actual.c_str(), vhdlCase);
  }
  else {
    INFO_ASSERT(false, "Unsupported language type");
  }
  UtString unknown("Unknown Value");

  return unknown;
}

DumpHierarchyCB::DumpHierarchyCB(UtString fileName, 
				 TicProtectedNameManager* tic_protected_nm_mgr, 
				 VHDLCaseT vhdlCaseSpec,
				 IODBNucleus* iodb)
  : mDesignHierarchy(new DesignHierarchy(fileName)),
    mTicProtectedNameMgr(tic_protected_nm_mgr),
    mInProtectedRegion(false), 
    mVhdlCase(vhdlCaseSpec),
    mIODB(iodb)
{

}

DumpHierarchyCB::~DumpHierarchyCB() 
{
  delete mDesignHierarchy;
}

void
DumpHierarchyCB::dumpXml() {
  mDesignHierarchy->dumpXml();
}

InterraDesignWalkerCB::Status DumpHierarchyCB::design(Phase           phase, 
			                              mvvNode         node, 
						      mvvNode         architecture,
 			                              mvvLanguageType langType) 
{
  if(phase == ePre) {
    UtString designName;
    bool     isUDP;

    getHdlEntityName(node, langType, &designName, 
		     &isUDP, mTicProtectedNameMgr, mVhdlCase);

    // Check if Protected
    if(!mInProtectedRegion && !isTicProtected(node, langType)) {

      UtString libName = getLibraryName(node, langType, mVhdlCase);
      UtString libPath = getLibraryPath(node, langType, mVhdlCase);
      UtString language = getLanguageType(langType, mVhdlCase);

      Instance* topLevel = new Instance(designName,
					libName,
					designName,
					language);

      mDesignHierarchy->addTopLevelDesignUnit(topLevel);
      mDesignHierarchy->pushInstance(topLevel);

      topLevel->isTopLevel();

      if (langType == MVV_VHDL) {
	UtString archNameStr;
	JaguarString archName(vhGetArchName(static_cast<vhNode>(architecture)));
	archNameStr = setVhdlCase(archName, mVhdlCase);
	topLevel->addArchitectureName(archNameStr.c_str());
      }

      UtString fileName = getFileName(node, langType);

      if (!mDesignHierarchy->fileExists(fileName)) {
	addFile(node, fileName, getFileType(node, langType), language);
      }

      topLevel->setStart(fileName, getStartLineNo(node, langType));

      if (!mDesignHierarchy->libraryExists(libName)) {
	Library* library = new Library(libName, 
				       libPath, 
				       language);

	library->setAsDefaultLibrary();

	mDesignHierarchy->addLibrary(libName, library);
      }
    }
    else {
      mInProtectedRegion = true;
      Instance* instance = new Instance(mInProtectedRegion);
      mDesignHierarchy->addTopLevelDesignUnit(instance);
    }
  }
  else if (phase == ePost) {
    if (!mInProtectedRegion) {
      mDesignHierarchy->popInstance();
    }
  }

  return eNormal;
}

void
getFormalPorts(mvvNode instMaster, mvvLanguageType instLang, UtArray<mvvNode>* formalArray)
{
  if (instLang == MVV_VHDL) {
    vhNode vh_entity = instMaster->castVhNode();
    vhNode port_clause = vhGetPortClause(vh_entity);
    JaguarList ports(vhGetFlatSignalList(port_clause));
    while (mvvNode formal = mvvGetNextItem(ports)) {
      formalArray->push_back(formal);
    }
  }
  else { // instLang == MVV_VERILOG 
    veNode ve_module = instMaster->castVeNode();
    CheetahList ports(veNodeIsA(ve_module, VE_UDP) == VE_TRUE ? veUDPGetPortList(ve_module) :
                      veModuleGetPortList(ve_module));
    while (mvvNode formal = mvvGetNextItem(ports)) {
      formalArray->push_back(formal);
    }
  }
}

InterraDesignWalkerCB::Status DumpHierarchyCB::hdlEntity(Phase           phase, 
			                                 mvvNode         node, 
			                                 mvvLanguageType langType, 
			                                 const char*     entityName, 
			                                 bool*         /*isCmodel*/) 
{
  if(phase == ePre) {
    UtString libraryName = getLibraryName(node, langType, mVhdlCase);
    UtString entityStr   = setVhdlCase(entityName, mVhdlCase, langType);
    UtString searchName  = libraryName + "." + entityStr;

    // Check if Protected
    if(isTicProtected(node, langType)) {
      if(!mInProtectedRegion) {
        mInProtectedRegion = true;
      }
      if (!mDesignHierarchy->interfaceExists(searchName)) {
	Interface* interface = new Interface(mInProtectedRegion);
	mDesignHierarchy->addInterface(searchName, interface);
      }
      return eSkip;
    }

    if (mDesignHierarchy->interfaceExists(searchName)) {
      mDesignHierarchy->setCurrentInterface(NULL);
    } 
    else {

      UtString   libraryPath = getLibraryPath(node, langType, mVhdlCase);
      UtString   language    = getLanguageType(langType, mVhdlCase);

      Interface* interface   = new Interface(entityStr, 
					     libraryName,
					     language);

      UtString   fileName    = getFileName(node, langType);
      UInt32     start       = getStartLineNo(node, langType);
      UInt32     end         = getEndLineNo(node, langType);
      
      interface->setStart(fileName, start);
      interface->setEnd(fileName, end);

      if (!mDesignHierarchy->fileExists(fileName)) {
	addFile(node, fileName, getFileType(node, langType), language);
      }

      mDesignHierarchy->addInterface(searchName, interface);
      mDesignHierarchy->setCurrentInterface(interface);

      if (!mDesignHierarchy->libraryExists(libraryName)) {
	Library* library = new Library(libraryName, libraryPath, language);
	mDesignHierarchy->addLibrary(libraryName, library);
      }
      
      // For Instances in Verilog
      if (langType == MVV_VERILOG) {
        if (veNodeGetObjType(static_cast<veNode>(node)) != VE_UDP) {
          // Getting Parameter List from the Instance
          CheetahList parameterList(veModuleGetParamList(static_cast<veNode>(node)));
          veNode      vlogNode;
          
          // For each Parameter
          while ((vlogNode = veListGetNextNode(parameterList))) {
            UtString masterName;
            bool     isUDP;
            // Find Instance Master
            veNode   master = veNodeGetMasterScope(vlogNode);
            // Getting the Parameter Name from the Parameter List (Instance)
            UtString parameterName(getMvvName(static_cast<mvvNode>(vlogNode), langType, mTicProtectedNameMgr, mVhdlCase));
            // Getting the Parameter Value from the Parameter List (Instance)
            if (!parameterName.empty()) {
              UtString parameterValue(getVeValue(veParamGetParamValue(vlogNode)));
              // Setting Parameter Type
              UtString parameterType(getVeType(vlogNode));
              
              // Getting Module Name form the Instance Master
              getHdlEntityName(master, langType, &masterName, 
			       &isUDP, mTicProtectedNameMgr, mVhdlCase);
	      // Get the library associated with the master
	      UtString libraryName = getLibraryName(master, langType, mVhdlCase);
              // Create and Add the Parameter to the Instance Interface
              Parameter* parameter = new Parameter(parameterName, 
						   parameterType, 
						   parameterValue);

	      parameter->setStart(fileName, getStartLineNo(vlogNode, langType));

	      interface->addParameter(parameter);
            }
          }
        }
      }
      else if (langType == MVV_VHDL) {
	// Get the generic clause, if one exists
	vhNode genericClause = vhGetGenericClause(static_cast<vhNode>(node));

	if (genericClause != NULL) {
	  JaguarList genericList(vhGetFlatGenericList(genericClause));
	  vhNode generic;
	  while ((generic = vhGetNextItem(genericList))) {
	    UtString parameterTypeName;
	    UtString parameterTypeLibrary;
	    UtString parameterTypePackage;
	    vhNode typeNode = getVhType(generic, 
					&parameterTypeName, 
					&parameterTypeLibrary, 
					&parameterTypePackage,
					mVhdlCase);

	    if (!mDesignHierarchy->typeExists(parameterTypeLibrary, parameterTypePackage, parameterTypeName)) {
	      Type* type = new Type(parameterTypeLibrary, parameterTypePackage, parameterTypeName);
	      UtString parameterTypeFile = getFileName(typeNode, langType);
	      UInt32 start               = getStartLineNo(typeNode, langType);
	      UInt32 end                 = getEndLineNo(typeNode, langType);
	      type->setStart(parameterTypeFile, start);
	      type->setEnd(parameterTypeFile, end);
	      mDesignHierarchy->addType(parameterTypeLibrary, parameterTypePackage, parameterTypeName, type);
	    }

	    UtString parameterName(getMvvName(static_cast<mvvNode>(generic), MVV_VHDL, mTicProtectedNameMgr, mVhdlCase));
	    UtString parameterValue(getMvvValue(static_cast<mvvNode>(generic), MVV_VHDL, false, mTicProtectedNameMgr, mVhdlCase));

	    Parameter* parameter = new Parameter(parameterName, 
						 parameterTypeName, 
						 parameterValue);

	    parameter->setStart(fileName, getStartLineNo(generic, langType));


	    interface->addParameter(parameter);
	  }	  
	}

	// Get all the architectures associated with this entity
	// even if they are not used in the design
	JaguarList vhdlArchs = vhGetAllArchitectures(libraryName.c_str(), entityName);
	vhString vhdlArchName;
	while ((vhdlArchName = (vhString)vhGetNextItem(vhdlArchs))) {
	  vhNode vhdlArch = vhOpenArch(libraryName.c_str(),
				       entityName,
				       vhdlArchName);
	  UtString archName = getMvvName(vhdlArch, langType, mTicProtectedNameMgr, mVhdlCase);
	  Architecture* architecture = new Architecture(archName,
							libraryName);
	  UtString archFileName = getFileName(vhdlArch, langType);
	  UInt32 startLn = getStartLineNo(vhdlArch, langType);
	  UInt32 endLn = getEndLineNo(vhdlArch, langType);

	  architecture->setStart(fileName, startLn);
	  architecture->setEnd(fileName, endLn);
	  interface->addArchitecture(architecture);	  
	}
      }
    }
  }
  else {
    mDesignHierarchy->setCurrentInterface(NULL);
  }

  return eNormal;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::unsupportedConcStmt(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::unsupportedInstanceType(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::generateElabFail(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::generateElabError(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::nestedGenerateInstance(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::unsupportedGenerateItem(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::componentElabFail(mvvNode, mvvLanguageType, int)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::unsupportedDeclaration(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::implicitGuardNullInGuardedBlock(mvvNode, mvvLanguageType)
{
  return eSkip;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::hdlComponentInstance(Phase           phase, 
				                                    mvvNode         inst, 
                                                                    const char*     instName,
                                                                    mvvNode         instMaster,
                                                                    mvvNode       /*oldEntity*/,
                                                                    mvvNode       /*newEntity*/,
                                                                    const char*     entityName,
                                                                    mvvNode         arch,
                                                                    const SInt32* /*vectorIndex*/, 
                                                                    const UInt32* /*rangeOffset*/,
                                                                    mvvLanguageType instLang, 
                                                                    mvvLanguageType scopeLang) 
{
  // Check if Protected
  if(isTicProtected(inst, scopeLang)) {
    if(!mInProtectedRegion) {
      mInProtectedRegion = true;
      Instance* instance = new Instance(mInProtectedRegion);
      Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
      currentInstance->addInstance(instance);
    }
    return eSkip;
  }
  else if(mInProtectedRegion) {
    mInProtectedRegion = false;
  }

  UtString instNameStr = setVhdlCase(instName, mVhdlCase, scopeLang);
  UtString entityNameStr = setVhdlCase(entityName, mVhdlCase, instLang);
  UtString libraryNameStr = getLibraryName(instMaster, instLang, mVhdlCase);
  UtString language = getLanguageType(scopeLang, mVhdlCase);

  if(phase == ePre) {

    Instance* instance = new Instance(instNameStr,
				      libraryNameStr,
				      entityNameStr,
				      language);

    if (mDesignHierarchy->hasSubstituteInterface()) {
      instance->setOriginalInterface(mDesignHierarchy->getSubstituteInterfaceName());
    }

    instance->setStart(getFileName(inst, scopeLang), 
		       getStartLineNo(inst, scopeLang));

    Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
    currentInstance->addInstance(instance);
    mDesignHierarchy->pushInstance(instance);
    
    if (arch != NULL)
    {
      JaguarString archName(vhGetArchName(static_cast<vhNode>(arch)));
      UtString archNameStr = setVhdlCase(archName, mVhdlCase, instLang);
      instance->addArchitectureName(archNameStr);
    }

    // For Instances in Verilog
    if(scopeLang == MVV_VERILOG) {
          
      // Getting Actual Parameter List from the Instance
      CheetahList parameterList(veInstanceGetDelayOrParamAssignList(static_cast<veNode>(inst)));
          
      // For Instance Masters in VHDL
      if(instLang == MVV_VHDL) {
        // Getting Formal Feneric Clause from the Instance Master
        vhNode formalParamClause = vhGetGenericClause(static_cast<vhNode>(instMaster));
        
        if(formalParamClause != NULL) {
          // Getting Formal Generic List from the Instance Master
          JaguarList formalParamList(vhGetFlatGenericList(formalParamClause));
          veNode     actualParam;
          vhNode     formalParam;

	  if (veListGetSize(parameterList) > 0) {

	    // For each Actual/Formal Parameter/Generic Pair
	    while((actualParam = veListGetNextNode(parameterList))) {
	      UtString parameterName;
	      // Getting Parameter Value from Actual Parameter List (Instance)
	      UtString parameterValue(getMvvValue(static_cast<mvvNode>(actualParam), scopeLang, true, mTicProtectedNameMgr, mVhdlCase));
	      
	      formalParam = vhGetNextItem(formalParamList);
	      
	      // Getting Parameter Type from Formal Generic List (Instance Master)
	      UtString parameterTypeName;
	      UtString parameterTypeLibrary;
	      UtString parameterTypePackage;
	      vhNode typeNode = getVhType(formalParam, 
					  &parameterTypeName, 
					  &parameterTypeLibrary, 
					  &parameterTypePackage,
					  mVhdlCase);

	      if (!mDesignHierarchy->typeExists(parameterTypeLibrary, parameterTypePackage, parameterTypeName)) {
		Type* type = new Type(parameterTypeLibrary, parameterTypePackage, parameterTypeName);
		UtString parameterTypeFile = getFileName(typeNode, instLang);
		UInt32 start               = getStartLineNo(typeNode, instLang);
		UInt32 end                 = getEndLineNo(typeNode, instLang);
		type->setStart(parameterTypeFile, start);
		type->setEnd(parameterTypeFile, end);
		mDesignHierarchy->addType(parameterTypeLibrary, parameterTypePackage, parameterTypeName, type);
	      }

	      // Getting Parameter Name
	      if(veNodeGetObjType(actualParam) == VE_PORTCONNECTION)
		// From the Actual Parameter List (Instance), if Port Connection
		parameterName = getMvvName(static_cast<mvvNode>(actualParam), scopeLang, mTicProtectedNameMgr, mVhdlCase);
	      else
		// From the Formal Generic List (Instance Master), otherwise
		parameterName = getMvvName(static_cast<mvvNode>(formalParam), instLang, mTicProtectedNameMgr, mVhdlCase);
	      
	      if(!parameterName.empty()) {
		Parameter* parameter = new Parameter(parameterName,
						     parameterTypeName,
						     parameterValue);
		instance->addParameter(parameter);
	      }
	    }
	  }
        }
      }
      // For Instance Masters in Verilog
      else if(instLang == MVV_VERILOG) {
        // Process instance parameters if there are any
        if (veNodeGetObjType(static_cast<veNode>(instMaster)) != VE_UDP) {
          // Validate that we have parameters to process
          CheetahList formalParamList(veModuleGetParamList(static_cast<veNode>(instMaster)));
          if (veListGetSize(formalParamList) > 0) {
            // Walk the formals and get the values from there. The
            // callback system elaborate function resolves parameters
            // for us.
            veNode      formalParam;
            while ((formalParam = veListGetNextNode(formalParamList)) != NULL) {
              // Get the parameter name and value
              UtString parameterName;
              UtString parameterValue;
              parameterName = getMvvName(static_cast<mvvNode>(formalParam), instLang, mTicProtectedNameMgr, 
                                         mVhdlCase);
              parameterValue = getMvvValue(static_cast<mvvNode>(formalParam), scopeLang, true, 
                                           mTicProtectedNameMgr, mVhdlCase);

              // Setting Parameter Type 
              UtString parameterType(getVeType(formalParam));
	      
              if(!parameterName.empty()) {
                Parameter* parameter = new Parameter(parameterName,
                                                     parameterType,
                                                     parameterValue);
                instance->addParameter(parameter);
              }
            }
          } // if
        } // if
      } // else if
      
      // Getting Actual Port List from the Instance
      CheetahList portList(veInstanceGetTerminalConnectionList(static_cast<veNode>(inst)));
      
      // For Instance Masters in VHDL
      if(instLang == MVV_VHDL) {
        // Getting Formal Port Clause from the Instance Master
        vhNode formalPortClause = vhGetPortClause(static_cast<vhNode>(instMaster));
        if(formalPortClause != NULL) {
          // Getting Formal Port List from the Instance Master
          JaguarList formalPortList(vhGetFlatSignalList(formalPortClause));
          veNode     actualPort;

	  if (veListGetSize(portList) > 0) {

	    // For each Actual/Formal Port Pair
	    while((actualPort = veListGetNextNode(portList))) {
	      vhNode   formalPort     = vhGetNextItem(formalPortList);
	      // Getting Port Name from the Formal Port List (Instance Master)
	      UtString formalPortName(getMvvName(static_cast<mvvNode>(formalPort), instLang, mTicProtectedNameMgr, mVhdlCase));
	      if(!formalPortName.empty()) {

		UtString substituteFormalPortName = mDesignHierarchy->getSubstitutePortMap(formalPortName);
		if (!substituteFormalPortName.empty()) {
		  formalPortName = substituteFormalPortName;
		}
		
		// Getting Port Value from the Actual Port List (Instance)
		veNode   actualPortExpr = vePortConnGetConnectedExpr(actualPort);
		UtString portValue(getMvvPortActual(static_cast<mvvNode>(actualPortExpr), scopeLang, mVhdlCase));

		Port* port = new Port(portValue, formalPortName);
		instance->addPort(port);
	      }
	    }
	  }
        }
      }
      // For Instance Masters in Verilog
      else if(instLang == MVV_VERILOG) {
        veNode  actualPort;

	if (veListGetSize(portList) > 0) {
	  UtArray<mvvNode> formalPortArray;
	  getFormalPorts(instMaster, instLang, &formalPortArray);
	  UtArray<mvvNode>::iterator formal_iter = formalPortArray.begin();

	  // For each Actual/Formal Port Pair
	  while((actualPort = veListGetNextNode(portList))) {
	    // Getting Port Name from the Formal Port List (Instance Master)
	    UtString formalPortName = vePortConnGetPortName(actualPort);

	    if(formalPortName.empty()) {
	      // The port connection is positional, not named
	      mvvNode formalPort = *formal_iter;
	      formalPortName = getMvvName(formalPort, instLang, mTicProtectedNameMgr, mVhdlCase);
	      ++formal_iter;
	    }
	    
	    if (!formalPortName.empty()) {
	      // Getting Port Value from the Actual Port List (Instance)
	      veNode   actualPortExpr = vePortConnGetConnectedExpr(actualPort);
	      UtString portValue(getMvvPortActual(static_cast<mvvNode>(actualPortExpr), scopeLang, mVhdlCase));

	      UtString substituteFormalPortName = mDesignHierarchy->getSubstitutePortMap(formalPortName);
	      if (!substituteFormalPortName.empty()) {
		formalPortName = substituteFormalPortName;
	      }

	      Port* port = new Port(portValue, formalPortName);
	      instance->addPort(port);
	    }
	  }
	}
      }
    }
    // For Instances in VHDL
    else if(scopeLang == MVV_VHDL) {

      // Getting Actual Generic Map from Instance
      vhArray actualGenericMap = vhGetGenericMap(static_cast<vhNode>(inst));

      // For Instance Masters in VHDL
      if(instLang == MVV_VHDL) {
        // Getting Formal Generic Caluse from Instance Master
        vhNode formalGenericClause = vhGetGenericClause(static_cast<vhNode>(instMaster));
        if(formalGenericClause != NULL) {
          // Getting Formal Generic List from Instance Master
          JaguarList formalGenericList(vhGetFlatGenericList(formalGenericClause));

	  if (vhGetArraySize(actualGenericMap) > 0) {

	    // For each Actual/Formal Generic Pair
	    for(int i=0; i<vhGetArraySize(actualGenericMap); i++) {
	      vhNode   actualParam = vhGetNodeAt(i, actualGenericMap);
	      vhNode   formalParam = vhGetNextItem(formalGenericList);
	      // Getting Generic Name from the Formal Genric List (Instance Master)
	      UtString parameterName(getMvvName(static_cast<mvvNode>(formalParam), instLang, mTicProtectedNameMgr, mVhdlCase));
	      if(!parameterName.empty()) {
		// Getting Generic Type from the Formal Genric List (Instance Master)
		UtString parameterTypeName;
		UtString parameterTypeLibrary;
		UtString parameterTypePackage;
		vhNode typeNode = getVhType(formalParam, 
					    &parameterTypeName, 
					    &parameterTypeLibrary, 
					    &parameterTypePackage,
					    mVhdlCase);
		
		if (!mDesignHierarchy->typeExists(parameterTypeLibrary, parameterTypePackage, parameterTypeName)) {
		  Type* type = new Type(parameterTypeLibrary, parameterTypePackage, parameterTypeName);
		  UtString parameterTypeFile = getFileName(typeNode, scopeLang);
		  UInt32 start               = getStartLineNo(typeNode, scopeLang);
		  UInt32 end                 = getEndLineNo(typeNode, scopeLang);
		  type->setStart(parameterTypeFile, start);
		  type->setEnd(parameterTypeFile, end);
		  mDesignHierarchy->addType(parameterTypeLibrary, parameterTypePackage, parameterTypeName, type);
		}

		// Getting Generic Value from the Actual Genric List (Instance)
		UtString parameterValue(getMvvValue(static_cast<mvvNode>(actualParam), scopeLang, true, mTicProtectedNameMgr, mVhdlCase));

		Parameter* parameter = new Parameter(parameterName,
						     parameterTypeName,
						     parameterValue);

		instance->addParameter(parameter);
	      }
	    }
	  }
        }
      }
      // For Instance Masters in Verilog
      else if(instLang == MVV_VERILOG) {
        if(veNodeGetObjType(static_cast<veNode>(instMaster)) != VE_UDP) {
          // Getting Formal Parameter List from Instance Master
          CheetahList formalGenericList(veModuleGetParamList(static_cast<veNode>(instMaster)));

	  if (vhGetArraySize(actualGenericMap) > 0) {

	    // For each Actual/Formal Parameter/Generic Pair
	    for(int i=0; i<vhGetArraySize(actualGenericMap); i++) {
	      vhNode   actualParam = vhGetNodeAt(i, actualGenericMap);
	      veNode   formalParam = veListGetNextNode(formalGenericList);
	      // Getting Generic Name from the Formal Genric List (Instance Master)
	      UtString parameterName(getMvvName(static_cast<mvvNode>(formalParam), instLang, mTicProtectedNameMgr, mVhdlCase));
	      if(!parameterName.empty()) {
		// Setting Generic Type
		UtString parameterType("wire");
		// Getting Generic Value from the Actual Genric List (Instance)
		UtString parameterValue(getMvvValue(static_cast<mvvNode>(actualParam), scopeLang, true, mTicProtectedNameMgr, mVhdlCase));

		Parameter* parameter = new Parameter(parameterName,
						     parameterType,
						     parameterValue);

		instance->addParameter(parameter);

	      }
	    }	    
	  }
        }
      }

      // Getting Actual Port Map from Instance
      vhArray actualPortMap = vhGetPortMap(static_cast<vhNode>(inst));

      // For Instance Masters in VHDL
      if(instLang == MVV_VHDL) {
        // Getting Formal Port Clause from Instance Master
        vhNode formalPortClause = vhGetPortClause(static_cast<vhNode>(instMaster));
        if(formalPortClause != NULL) {
          // Getting Formal Port List from Instance Master
          JaguarList formalPortList(vhGetFlatSignalList(formalPortClause));

	  if (vhGetArraySize(actualPortMap) > 0) {

	    // For each Actual/Formal Port Pair
	    for(int i=0; i<vhGetArraySize(actualPortMap); i++) {
	      vhNode   actualPort = vhGetNodeAt(i, actualPortMap);
	      vhNode   formalPort = vhGetNextItem(formalPortList);
	      // Getting Actula Port Name from the Actual Port List (Instance)
	      UtString actualPortName(getMvvPortActual(static_cast<mvvNode>(actualPort), scopeLang, mVhdlCase));
	      if(!actualPortName.empty()) {
		// Getting Formal Port Name from the Formal Port List (Instance Master)
		UtString formalPortName(getMvvName(static_cast<mvvNode>(formalPort), instLang, mTicProtectedNameMgr, mVhdlCase));

		UtString substituteFormalPortName = mDesignHierarchy->getSubstitutePortMap(formalPortName);
		if (!substituteFormalPortName.empty()) {
		  formalPortName = substituteFormalPortName;
		}

		Port* port = new Port(actualPortName, formalPortName);

		instance->addPort(port);
	      }
	    }
	  }
        }
      }
      // For Instance Masters in Verilog
      else if(instLang == MVV_VERILOG) {
        // Getting Formal Port List from Instance Master
        CheetahList formalPortList;
        if(veNodeGetObjType(static_cast<veNode>(instMaster)) == VE_UDP)
          formalPortList = veUDPGetPortList(static_cast<veNode>(instMaster));
        else
          formalPortList = veModuleGetPortList(static_cast<veNode>(instMaster));

	if (vhGetArraySize(actualPortMap) > 0) {

	  // For each Actual/Formal Port Pair
	  for(int i=0; i<vhGetArraySize(actualPortMap); i++) {
	    vhNode   actualPort = vhGetNodeAt(i, actualPortMap);
	    veNode   formalPort = veListGetNextNode(formalPortList);
	    // Getting Actula Port Name from the Actual Port List (Instance)
	    UtString actualPortName(getMvvPortActual(static_cast<mvvNode>(actualPort), scopeLang, mVhdlCase));
	    // Getting Formal Port Name from the Formal Port List (Instance Master)
	    UtString formalPortName(getMvvName(static_cast<mvvNode>(formalPort), instLang, mTicProtectedNameMgr, mVhdlCase));
	    
	    if(!formalPortName.empty()) {

	      UtString substituteFormalPortName = mDesignHierarchy->getSubstitutePortMap(formalPortName);
	      if (!substituteFormalPortName.empty()) {
		formalPortName = substituteFormalPortName;
	      }

	      Port* port = new Port(actualPortName, formalPortName);

	      instance->addPort(port);
	    }
	  }
	}
      }
    }
  }
  else {
    mDesignHierarchy->popInstance();
  }

  return eNormal;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::port(Phase           phase, 
		                                    mvvNode         node, 
		                                    mvvLanguageType langType) 
{
  if(phase == ePre) {
    // Check if Protected
    if(isTicProtected(node, langType)) {
      if(!mInProtectedRegion) 
        mInProtectedRegion = true;
      return eSkip;
    }

    Interface* interface = mDesignHierarchy->getCurrentInterface();

    if (interface == NULL) {
      return eSkip;
    }

    // For Instances in Verilog
    if(langType == MVV_VERILOG) {
      veNode     vlogNode = static_cast<veNode>(node);
      UtString   masterName;
      bool       isUDP;
      // Find Instance Master
      veNode     master   = veNodeGetMasterScope(vlogNode);
      // Getting Port Name from the Instance
      UtString portName;
      mTicProtectedNameMgr->getVisibleName(vlogNode, &portName);

      if(portName != NULL) {
        // Getting Port Direction from the Instance
        UtString   portDirection;
        switch(vePortGetPortDirection(vlogNode)) {
          case INPUT_DIR:
            portDirection = "IN";
            break;
          case OUTPUT_DIR:
            portDirection = "OUT";
            break;
          case INOUT_DIR:
            portDirection = "BIDI";
            break;
          default:
            INFO_ASSERT(false, "Unset port direction");
            break;
        }
        // Getting Module Name from the Instance Master
        getHdlEntityName(master, langType, &masterName, &isUDP, mTicProtectedNameMgr, mVhdlCase);
	// Get the library associated with the master
	UtString libraryName = getLibraryName(master, langType, mVhdlCase);

	// For Verilog, the port type doesn't make any sense.
	UtString portType;

        // Getting port Size from Instance
        UtString portSize;
	portSize << vePortGetPortWidth(vlogNode);

        // Create and Add Port to the Instance Interface
	Port* port = new Port(portName,
			      portType,
			      portDirection,
			      portSize);

	port->setStart(getFileName(vlogNode, langType), getStartLineNo(vlogNode, langType));

	interface->addPort(port);
      }
    }
    // For Instances in VHDL
    else if(langType == MVV_VHDL) {
      vhNode       vhdlNode = static_cast<vhNode>(node);
      UtString     masterName;
      bool         isUDP;
      // Find Instance Master
      vhNode       master = vhGetMasterScope(vhdlNode);
      // Getting Port Name from the Instance
      JaguarString portName(vhGetName(vhdlNode));
      UtString portNameStr = setVhdlCase(portName, mVhdlCase);
      // Getting Port Type from the Instance
      UtString portTypeName;
      UtString portTypeLibrary;
      UtString portTypePackage;
      vhNode typeNode = getVhType(vhdlNode, 
				  &portTypeName, 
				  &portTypeLibrary, 
				  &portTypePackage,
				  mVhdlCase);
      
      if (!mDesignHierarchy->typeExists(portTypeLibrary, portTypePackage, portTypeName)) {
	Type* type = new Type(portTypeLibrary, portTypePackage, portTypeName);
	UtString portTypeFile = getFileName(typeNode, langType);
	UInt32 start          = getStartLineNo(typeNode, langType);
	UInt32 end            = getEndLineNo(typeNode, langType);
	type->setStart(portTypeFile, start);
	type->setEnd(portTypeFile, end);
	mDesignHierarchy->addType(portTypeLibrary, portTypePackage, portTypeName, type);
      }
      
      // Getting Port Direction from the Instance
      UtString     portDirection;
      switch(vhGetPortType(vhdlNode)) {
        case VH_IN:
	  portDirection = "IN";
  	  break;
        case VH_OUT:
	  portDirection = "OUT";
	  break;
        case VH_INOUT:
	  portDirection = "BIDI";
	  break;
        case VH_BUFFER:
	  portDirection = "OUT";
	  break;
        case VH_LINKAGE:
	  INFO_ASSERT(false, "Unknown port direction");
          break;
        case VH_DEFAULT_IN:
	  portDirection = "IN";
  	  break;
        case VH_NOT_PORT: 
	  INFO_ASSERT(false, "Unknown port direction");
          break;
        case VH_ERROR_PORTTYPE:
	  INFO_ASSERT(false, "Unknown port direction");
          break;
        default:
	  INFO_ASSERT(false, "Unset port direction");
	  break;
      }
      // Getting Module Name form the Instance Master
      getHdlEntityName(master, langType, &masterName, &isUDP, mTicProtectedNameMgr, mVhdlCase);
      // Get the library associated with this master
      UtString libraryName = getLibraryName(master, langType, mVhdlCase);
      // Getting port Size from Instance
      UtString portSize;
      portSize << vhGetVectorSignalSize(vhdlNode);
      if(portSize == "-1")
        portSize = "1";

      Port* port = new Port(portNameStr,
			    portTypeName,
			    portDirection,
			    portSize);

      port->setStart(getFileName(vhdlNode, langType), getStartLineNo(vhdlNode, langType));

      interface->addPort(port);
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::net(Phase           phase, 
		                                   mvvNode         node, 
		                                   mvvLanguageType langType) 
{
  if(phase == ePre) {
    // Check if Protected
    if(isTicProtected(node, langType)) {
      if(!mInProtectedRegion) 
        mInProtectedRegion = true;
      return eSkip;
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status DumpHierarchyCB::packageDecl(Phase phase, 
							   mvvNode node, 
							   mvvLanguageType langType,
							   const char* /*libName*/, 
							   const char* /*packName*/,
							   bool isStdLib) 
{
  if (phase == ePre) {
    if (langType == MVV_VHDL) {
      if (!isStdLib) {
	UtString fileName = getFileName(node, langType);
	if (!mDesignHierarchy->fileExists(fileName)) {
	  addFile(node, 
		  fileName, 
		  File::getFileType(File::ePackageDeclaration), 
		  getLanguageType(langType, mVhdlCase));
	}
      }
    }
  }
  return eNormal;
}

//! For generate labels callback. These have to be represented in the design hierarchy
InterraDesignWalkerCB::Status DumpHierarchyCB::forGenerate(Phase phase,
                                                           mvvNode node,
                                                           mvvNode indexExpr,
                                                           mvvLanguageType langType)
{
   Status ret = eNormal;
   if (langType == MVV_VHDL) {
     if (phase == ePre) {
          vhExpr vh_expr = static_cast<vhExpr>(indexExpr);
          if (vhGetObjType(vh_expr) != VHDECLIT)
             ret = eSkip;
          else {
             vhExpr evaluatedExpr = vhEvaluateExpr(vh_expr);
             int value = (int)vhGetDecLitValue(evaluatedExpr);
             JaguarString vh_label_str(vhGetLabel(node->castVhNode()));
             UtString label(vh_label_str);
             label << "__" << value;
             // UtIO::cout() << "DEBUG: DumpHierarchyCB::forGenerate PRE: " << label << UtIO::endl;
             UtString instNameStr = setVhdlCase(label.c_str(), mVhdlCase, langType); 
             UtString language = getLanguageType(langType, mVhdlCase);
             Instance* instance = new Instance(instNameStr, "", "", language);
             instance->setStart(getFileName(node, langType),
                                getStartLineNo(node, langType));
             // Tell the modelstudio hierarchy browser that this is a generate label
             // (and not a module/entity instance, or a process label)
             instance->setInterfaceType("generate");
             Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
             currentInstance->addInstance(instance);
             mDesignHierarchy->pushInstance(instance);
          }
     }
     else if (phase == ePost) {
        // UtIO::cout() << "DEBUG: DumpHierarchyCB::forGenerate POST " << UtIO::endl;
        mDesignHierarchy->popInstance();
     }
   }

   return ret;
}

//! Concurrent process callback. If labelled, these have to be 
// represented in the design hierarchy
InterraDesignWalkerCB::Status DumpHierarchyCB::concProcess(Phase phase,
                                                           mvvNode node,
                                                           mvvLanguageType langType)
{
   Status ret = eNormal;
   if (langType == MVV_VHDL) { 
     JaguarString procLabel(vhGetLabel(node->castVhNode()));
     if (phase == ePre) {
        if (procLabel != NULL) {
           UtString label(procLabel);
           // UtIO::cout() << "DEBUG: DumpHierarchyCB::concProssess PRE: " << label << UtIO::endl;
           UtString instNameStr = setVhdlCase(label.c_str(), mVhdlCase, langType); // Has to be VHDL
           UtString language = getLanguageType(langType, mVhdlCase);
           Instance* instance = new Instance(instNameStr, "", "", language);
           instance->setStart(getFileName(node, langType),
                              getStartLineNo(node, langType));
           // Tell the modelstudio hierarchy browser that this is a process label
           // (and not a module/entity instance or generate label)
           instance->setInterfaceType("process");
           Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
           currentInstance->addInstance(instance);
           mDesignHierarchy->pushInstance(instance);
        }
     }
     else if (phase == ePost) {
        if (procLabel != NULL) {
           // UtIO::cout() << "DEBUG: DumpHierarchyCB::concProcess POST " << UtIO::endl;
           mDesignHierarchy->popInstance();
        }
     }
   }

   return ret;
}

//! if generate callback. These must be represented in the design hierarchy
// for the modelstudio design explorer
InterraDesignWalkerCB::Status DumpHierarchyCB::ifGenerate(Phase phase,
                                                          mvvNode node,
                                                          mvvLanguageType langType)
{
   Status ret = eNormal;
   // if generates must have a label, so this is being defensive
   if (langType == MVV_VHDL) { 
     JaguarString ifGenLabel(vhGetLabel(node->castVhNode()));
     if (phase == ePre) {
        if (ifGenLabel != NULL) {
           UtString label(ifGenLabel);
           // UtIO::cout() << "DEBUG: DumpHierarchyCB::if generate PRE: " << label << UtIO::endl;
           UtString instNameStr = setVhdlCase(label.c_str(), mVhdlCase, langType); 
           UtString language = getLanguageType(langType, mVhdlCase);
           Instance* instance = new Instance(instNameStr, "", "", language);
           instance->setStart(getFileName(node, langType),
                              getStartLineNo(node, langType));
           // Tell the modelstudio hierarchy browser that this is a generate label
           // (and not a module/entity instance, or a process label)
           instance->setInterfaceType("generate");
           Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
           currentInstance->addInstance(instance);
           mDesignHierarchy->pushInstance(instance);
        }
     }
     else if (phase == ePost) {
        if (ifGenLabel != NULL) {
           // UtIO::cout() << "DEBUG: DumpHierarchyCB::concProcess POST " << UtIO::endl;
           mDesignHierarchy->popInstance();
        }
     }
   }

   return ret;
}

//! Generate block callback places Verilog generate labels in design hierarchy file.
// These allow modelstudio to browse instances, regs, wires underneath generates.
InterraDesignWalkerCB::Status DumpHierarchyCB::generateBlock(Phase phase, 
                                                             mvvNode node, 
                                                             const char* blockName,
                                                             mvvLanguageType langType, 
                                                             const SInt32* genForIndex)
{
   (void)genForIndex;

   // If this is not a nameable generate block, there's no work to be done.
   if (blockName == NULL || strlen(blockName) == 0)
     return eNormal;
   
   // This is only called for Verilog, but check anyway
   if (langType == MVV_VERILOG) {
     // Check if Protected
     if(isTicProtected(node, langType)) {
       // UtIO::cout() << "DEBUG: Verilog generate block PROTECTED '" << blockName << "'" << UtIO::endl;
       if(!mInProtectedRegion) {
         mInProtectedRegion = true;
         Instance* instance = new Instance(mInProtectedRegion);
         Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
         currentInstance->addInstance(instance);
       }
       return eSkip;
     }
     else if(mInProtectedRegion) {
       mInProtectedRegion = false;
     }

     if (phase == ePre) {
       // UtIO::cout() << "DEBUG: Verilog generate block PRE '" << blockName << "'" << UtIO::endl; 
       UtString instNameStr = setVhdlCase(blockName, mVhdlCase, langType); 
       UtString language = getLanguageType(langType, mVhdlCase);
       Instance* instance = new Instance(instNameStr, "", "", language);
       instance->setStart(getFileName(node, langType),
                          getStartLineNo(node, langType));
       // Tell the modelstudio hierarchy browser that this is a generate label
       // (and not a module/entity instance, or a process label)
       instance->setInterfaceType("vlog_generate");
       Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
       currentInstance->addInstance(instance);
       mDesignHierarchy->pushInstance(instance);
     }
     else {
       // UtIO::cout() << "DEBUG: Verilog generate block POST '" << blockName << "'" << UtIO::endl;
       mDesignHierarchy->popInstance();
     }
   }

   return eNormal;
}

  //! Package body callback. Walked to from package use clause.
InterraDesignWalkerCB::Status DumpHierarchyCB::packageBody(Phase phase, 
							   mvvNode node, 
							   mvvLanguageType langType) 
{
  if (phase == ePre) {
    if (langType == MVV_VHDL) {
      JaguarString libName(vhGetLibName(node->castVhNode()));
      if (0 != strcasecmp(libName,"STD") && 0 != strcasecmp(libName, "IEEE")) {
	UtString fileName = getFileName(node, langType);
	if (!mDesignHierarchy->fileExists(fileName)) {
	  addFile(node, 
		  fileName,
		  File::getFileType(File::ePackageBody),
		  getLanguageType(langType, mVhdlCase));
	}
      }
    }
  }
  return eNormal;

}

void DumpHierarchyCB::addFile(mvvNode node, 
			      UtString fileName, 
			      UtString fileType, 
			      UtString language)
{
  File* file = new File(fileName,
			fileType,
			language);

  mDesignHierarchy->addFile(fileName, file);

  if (strcasecmp(language.c_str(), "verilog") == 0) {
    veNode vlogNode = node->castVeNode();

    if (veNodeGetObjType(vlogNode) == VE_MODULE &&
	veModuleGetIsLibrary(vlogNode) == VE_TRUE) {
      file->setFileType(File::getFileType(File::eVlogLibrary));
    }

    veNode vlogFile;
    if (veNodeGetObjType(vlogNode) != VE_FILE) {
      vlogFile = veNodeGetFile(vlogNode);
    }
    else {
      vlogFile = vlogNode;
    }

    // Check for `includes
    checkForIncludes(vlogFile);
    
    // Check for `define
    checkForDefines(vlogFile, file);
  }
}

void DumpHierarchyCB::checkForIncludes(veNode file) 
{
  CheetahList compilerDirectiveList(veFileGetCompilerDirectiveList(file));
  veNode compilerDirective;
  while ((compilerDirective = veListGetNextNode(compilerDirectiveList))) {
    if (VE_INCLUDE != veCompilerDirectiveGetType(compilerDirective))
      continue;
    veNode includedFile = veIncludeGetFile(compilerDirective);
    UtString fileName;
    CheetahStr name(veNodeGetAbsoluteSourceFileName(includedFile));
    if (name != NULL) {
      fileName = name;
    }
    if (!mDesignHierarchy->fileExists(fileName)) {
      addFile(includedFile,
	      fileName, 
	      File::getFileType(File::eIncludedFile),
	      "verilog");
    }
  }
}

void 
DumpHierarchyCB::checkForDefines(veNode file, File* filePtr)
{
  CheetahList defineList(veFileGetDefineList(file));
  veNode vlogDefine;
  while ((vlogDefine = veListGetNextNode(defineList))) {
    UtString name;
    mTicProtectedNameMgr->getVisibleName(vlogDefine, &name, true);
    UtString valueStr;
    CheetahStr value(veDefineGetValue(vlogDefine));
    if (value != NULL) {
      valueStr = value;
    }
    UInt32 startLineNo(veNodeGetLineNumber(vlogDefine));
    UtString start;
    start << startLineNo;
    Define* define = new Define(name, valueStr, start);
    filePtr->addDefine(define);
  }

  CheetahList macroList(veFileGetDefineMacroList(file));
  veNode vlogMacro;
  while ((vlogMacro = veListGetNextNode(macroList))) {
    UtString name;
    mTicProtectedNameMgr->getVisibleName(vlogDefine, &name, true);
    UtString valueStr;
    CheetahStr value(veDefineGetValue(vlogMacro));
    if (value != NULL) {
      valueStr = value;
    }
    UInt32 startLineNo(veNodeGetLineNumber(vlogMacro));
    UtString start;
    start << startLineNo;
    Define* define = new Define(name, valueStr, start);
    filePtr->addDefine(define);
  }
}

mvvNode DumpHierarchyCB::substituteEntity(mvvNode entityDecl, 
					  mvvLanguageType entityLanguage, 
					  const UtString& entityDeclName, 
					  mvvNode /*instance*/, 
					  bool isUDP, 
					  mvvLanguageType callingLanguage)
{
  NUPortMap* portMap = NULL;
  bool found = false;
  UtString new_mod_name;
  Interface* currentInterface = mDesignHierarchy->getCurrentInterface();
  Instance* currentInstance = mDesignHierarchy->getCurrentInstance();
  UtString instanceName = currentInstance->getName();
  UtString parentName;

  if (currentInterface) {
    parentName = currentInterface->getName();
  }
  else {
    // This is the case of the top level module or entity.
    parentName = instanceName;
  }

  mvvNode node = getSubstituteEntity(parentName.c_str(),
				     entityDecl,
				     entityLanguage,
				     entityDeclName,
				     instanceName,
				     isUDP,
				     callingLanguage,
				     mTicProtectedNameMgr,
				     mIODB,
				     &new_mod_name,
				     &found,
				     &portMap);

  if (node) {
    mDesignHierarchy->setSubstituteInterfaceName(setVhdlCase(entityDeclName.c_str(), mVhdlCase, callingLanguage));

    if (portMap != NULL) {
      for (NUPortMap::iterator iter = portMap->begin(); iter != portMap->end(); ++iter) {
	mDesignHierarchy->addSubstitutePortMap((*iter).first, (*iter).second);
      }
    }

    // Add the replaced interface to the design hierarchy file if it doesn't already exist.
    bool isCmodel;
    hdlEntity(ePre, entityDecl, entityLanguage, entityDeclName.c_str(), &isCmodel);

    // Also get the corresponding formal ports
    if (entityLanguage == MVV_VERILOG) {
      veNode ve_entityDecl = entityDecl->castVeNode();
      if (veNodeIsA(ve_entityDecl, VE_UDP)) {
	CheetahList portList(veUDPGetPortList(ve_entityDecl));
	veNode portNode;
	while ((portNode = veListGetNextNode(portList))) {
	  port(ePre, portNode, MVV_VERILOG);
	}
      }
      else {
	CheetahList portList(veModuleGetPortList(ve_entityDecl));
	veNode portNode;
	while ((portNode = veListGetNextNode(portList))) {
	  port(ePre, portNode, MVV_VERILOG);
	}
      }
    }
    else { // entityLanguage == MVV_VHDL
      vhNode vh_entityDecl = entityDecl->castVhNode();
      vhNode portClause = vhGetPortClause(vh_entityDecl);
      if (portClause) {
	JaguarList portList(vhGetFlatSignalList(portClause));
	vhNode portNode;
	while ((portNode = vhGetNextItem(portList))) {
	  port(ePre, portNode, MVV_VHDL);
	}
      }
    }
  }
  else {
    mDesignHierarchy->clearSubstituteInterfaceName();
    mDesignHierarchy->clearSubstitutePortMap();
  }

  return node;
}

InterraDesignWalkerCB::Status
DumpHierarchyCB::vlogForeignAttribute(mvvNode module, const UtString&, mvvNode* ent, mvvNode* arch) 
{
  InterraDesignWalkerCB::Status status = getEntArchPairFromForeignAttr(module, ent, arch);

  if (status == InterraDesignWalkerCB::eInvalid) {
    status = InterraDesignWalkerCB::eStop;
  }

  return status;
}
