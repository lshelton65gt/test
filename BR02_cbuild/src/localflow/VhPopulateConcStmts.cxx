// -*- C++ -*-
/******************************************************************************
  Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/CbuildMsgContext.h"
#include "compiler_driver/CarbonContext.h"
#include "hdl/HdlVerilogPath.h"

// Function to check if the waveform of a conditional signal assignment is 
// convertable into equivalent ternary expression.
//
// The conditional waveform will not be convertable into equivalent
// ternary expression if the waveform contains UNAFFECTED or if there
// is no uncoditional ELSE block . Examples:
// 1. out1 <= in1 when enable = '1' else
//            in2 when ena2 = '0' ; -- unconditional ELSE missing
// 2. out2 <= in1 when ena1 = '1' else UNAFFECTED;
//
// The waveform in the above 2 examples are not convertable into equivalent
// ternary expression. All other type of waveform of a conditional signal
// assignment is convertable into equivalent ternary expression.
//
static bool 
sIsConditionalWaveFrmConvertableIntoTernaryExpr( vhNode vh_conc_stmt,
                                                 bool &drivesZ)
{
  bool retval = true;
  drivesZ = false;
  JaguarList waveFrmList(vhGetConditionalWaveFrmList(vh_conc_stmt));
  vhNode vh_cond_wave_alt = NULL;
  vhExpr lastValue = NULL;
  vhExpr cond = NULL;
  while ((vh_cond_wave_alt = vhGetNextItem( waveFrmList )))
  {
    cond = vhGetCondition( vh_cond_wave_alt );
    vhNode waveFrm = vhGetWaveFrm( vh_cond_wave_alt );
    if (vhIsUnaffected( waveFrm ))
    {
      retval = false;
      break;
    }
    vhArray eleList  = vhGetWaveFrmEleList( waveFrm );
    lastValue = static_cast<vhExpr>(vhGetNodeAt( 0, eleList ));
  }
  if (cond != NULL) // cond will be null if the last ELSE is unconditional
  {
    retval = false;
  }
  UtString str_val;
  switch (vhGetObjType(lastValue))
  {
    case VHCHARLIT:
      str_val += vhGetCharVal(lastValue);
      break;
    case VHSTRING:
      str_val += vhGetString(lastValue); 
      break;
    default:
      break;
  }
  // Check if the string contains 'Z' and if that 'Z' corresponds to tristate
  // value.
  if ( not str_val.empty() && 
       (str_val.find_first_of("zZ") != UtString::npos))
  {
    // We can have 'Z' value which is not tristate 'Z' if and only if
    // the type of the 'Z' or "Z" is CHARACTER or STRING. The type
    // CHARACTER or STRING is defined only in the STD library
    // package. So, if the library of this 'Z'/"Z" is IEEE we can say
    // that this 'Z'/"Z" corresponds to tristate value.
    //
    vhNode expr_type = vhGetExpressionType(static_cast<vhExpr>(lastValue));
    vhNode n_package = vhGetMasterScope(expr_type);
    if (n_package && vhGetObjType(n_package) == VHPACKAGEDECL)
    {
      JaguarString library_name( vhGetLibName(n_package));
      if ( !strncasecmp( library_name, "IEEE",5 ))
      {
        drivesZ = true;
      }
    }
  }
  return retval;
}

// This method acclerates various Synopsys GTECH components.  GTECH_BUF
// and GTECH_INBUF become continuous assigns.
Populate::ErrorCode
VhdlPopulate::accelerateGtechComponent( vhNode vh_conc_stmt, LFContext *context )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  if ( vhGetObjType( vh_conc_stmt ) != VHCOMPINSTANCE )
    return eNotApplicable;
  // Accelerate certain components out of the Synopsys GTECH library
  vhNode vh_comp_decl = vhGetCompDecl( vh_conc_stmt );
  vhNode vh_comp_scope = vhGetScope( vh_comp_decl );
  if ( vhGetObjType( vh_comp_scope ) == VHPACKAGEDECL &&
       !strcasecmp ( vhGetLibName( vh_comp_scope ), "GTECH" ) &&
       !strcasecmp ( vhGetPackName( vh_comp_scope ), "GTECH_COMPONENTS" ))
  {
    const char *compName = vhGetCompName( vh_comp_decl );
    if ( !strcasecmp( compName, "GTECH_BUF" ) ||
         !strcasecmp( compName, "GTECH_INBUF" ))
    {
      vhArray vh_portmap = vhGetPortMap( vh_conc_stmt );
      vhExpr vh_rvalue = static_cast<vhExpr>( vhGetNodeAt( 0, vh_portmap ));
      vhNode vh_lvalue = vhGetNodeAt( 1, vh_portmap );
      // If a buffer has an unconnected out port, there's nothing to do
      // for it.  In VHDL, input ports may not be left unconnected.
      if ( vhGetObjType( vh_lvalue ) == VHOPEN )
        return eSuccess; // eSuccess means we've finished handling this,
                         // and the caller does not need to instantiate
                         // a component.
      NULvalue *the_lvalue;
      temp_err_code = lvalue( vh_lvalue, context, &the_lvalue, false );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code))
        return temp_err_code;
      NUExpr *the_rvalue;
      temp_err_code = expr( vh_rvalue, context, 1, &the_rvalue );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code))
        return temp_err_code;
      SourceLocator loc = locator( vh_conc_stmt );
      StringAtom *block_name = 
        context->getModule()->newBlockName(context->getStringCache(), loc);
      NUContAssign *the_assign = new NUContAssign(block_name, the_lvalue, the_rvalue, loc);
      context->getModule()->addContAssign( the_assign );

      return err_code;
    }
  }
  return eNotApplicable;
}

// A concurrent procedure call is modelled as a process statement with a
// sequential procedure call and a wait stmt on the procedure args.
Populate::ErrorCode 
VhdlPopulate::concProcedureCall(vhNode vh_proc_call,
                                LFContext *context)
{
  NUAlwaysBlock *the_process = NULL;
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator(vh_proc_call);

  NUBlock *the_block = NULL;
  sequentialBlock( vh_proc_call, NULL, context, &loc, &the_block );

  NUStmtList stmts;
  context->pushStmtList(&stmts);

  // populate the procedure call as a sequential stmt inside a process stmt.
  vhNode vh_seq_proc_call = vhGetProcedureCall(vh_proc_call);
  temp_err_code = sequentialStmt(vh_seq_proc_call, context);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
    return temp_err_code;

  the_block->addStmts(&stmts);
  context->popStmtList();
  // Pop off the stack elements pushed on in sequentialBlock
  context->popJaguarScope();
  context->popDeclarationScope();
  context->popBlockScope();

  NUModule* module = context->getModule();
  StringAtom *name = module->newBlockName(context->getStringCache(), loc);
  the_process = new NUAlwaysBlock(name, the_block, context->getNetRefFactory(), loc, false);

  if (the_process) {
    module->addAlwaysBlock(the_process);
  }
  return err_code;
}

// This function processes the block statement. All the declarations
// in a block scope will get promoted in the module scope. While
// promoting the declared object, we will generate a mangled name
// for the declared object in order to maintain it's uniqueness. 
// All the concurrent statements inside the block will get processed 
// like any other concurrent statement in the architecture.
//
Populate::ErrorCode VhdlPopulate::blockStmt(vhNode vh_block,
                                            const char* blockName,
                                            LFContext *context)
{
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(vh_block);
  LOC_ASSERT(VHBLOCK == vhGetObjType(vh_block), loc);
  StringAtom* label = context->vhdlIntern(blockName);

  NUNamedDeclarationScope *block_scope = new 
                   NUNamedDeclarationScope( label, 
                                            context->getDeclarationScope(), 
                                            mIODB, context->getNetRefFactory(),
                                            loc, eHTNamedBlock);
  context->pushDeclarationScope(block_scope);
  context->pushJaguarScope(vh_block);
  return err_code;
}


// This function handles creating a new reset net whenever a reset
// condition does not fit our simple format of the various forms of
// 'reset = 0|1'.
Populate::ErrorCode
VhdlPopulate::ComplexResetHelper( vhNode vh_cond, NUExpr *cond_expr,
                                  LFContext* context )
{
  NU_ASSERT( vh_cond != NULL, cond_expr );
  ErrorCode err_code = eSuccess, temp_err_code;
  // Manufacture a net to distill the reset down to, create a
  // continuous assign of cond_expr to it, and create an edge
  // expression for it.

  // Create a name for the new reset net
  UtString name;
  getVhdlObjName( context->getJaguarScope(), &name );
  NUModule *module = context->getModule();
  StringAtom *resetName = module->gensym( "$reset", name.c_str());
  const SourceLocator loc = locator( vh_cond );
  // Create the new reset net
  NetFlags flags = NetFlags( eDMWireNet | eAllocatedNet | eTempNet );
  NUNet *resetNet = new NUBitNet( resetName, flags, module, loc );
  module->addLocal( resetNet );

  // Create the assign of the original condition to the new reset net
  cond_expr->resize( 1 );
  NULvalue *the_lvalue = new NUIdentLvalue( resetNet, loc );
  StringAtom *block_name = module->newBlockName(context->getStringCache(), loc);
  NUContAssign *the_assign = new NUContAssign( block_name, the_lvalue,
                                               cond_expr, loc );

  // Create the edge expression for the new reset net
  NUEdgeExpr* the_edge_expr = 0;
  NUExpr *sr_expr = createIdentRvalue( resetNet, loc );
  sr_expr->resize( 1 );
  temp_err_code = edgeExpr( sr_expr, eClockPosedge, context, &the_edge_expr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    delete sr_expr;
    delete the_assign;
    delete the_lvalue;
    delete resetNet;
    delete cond_expr;
    return err_code;
  }
  if ( the_edge_expr )
  {
    the_edge_expr->resize( 1 );
    context->addEdgeExpr( the_edge_expr );
  }
  // Add the assign to the module scope
  module->addContAssign( the_assign );

  // Insert a copy of the reset expr into the map; this will be placed
  // into the nucleus tree in VhdlPopulate::ifStmt.
  CopyContext cc( NULL, NULL );
  sr_expr = sr_expr->copy( cc );
  mCurrentProcessInfo->mComplexResetMap[vh_cond] = sr_expr;
  return err_code;
}


StringAtom* VhdlPopulate::getInstanceName( vhNode vh_inst, LFContext *context)
{
  SourceLocator loc = locator( vh_inst );
  
  UtString instName;
  Populate::composeInstanceName(vh_inst, context, &instName, mTicProtectedNameMgr);
  StringAtom* instAtom = context->vhdlIntern(instName.c_str());
  return instAtom;
}


bool sDoesHaveOpenElem(vhExpr vh_node)
{
  bool is_any_open = false;
  
  JaguarList eleList(vhGetEleAssocList(vh_node));
  vhExpr element;
  while ((element = static_cast<vhExpr>(vhGetNextItem(eleList)))) 
  {
    vhObjType objType = vhGetObjType(element);
    switch(objType)
    {
      case VHOPEN:
	is_any_open = true;
        break;
        
      case VHSLICENODE:
      {
        vhNode actualObj = vhGetActual(element );

	if (vhGetObjType(actualObj) ==  VHOPEN)
        {
	  is_any_open = true;
	  break;
        }
        break;
      }

      default:     
        break;
    }
  }

  return is_any_open;
}

Populate::ErrorCode VhdlPopulate::componentInstancePost( vhNode vh_inst,
                                                         vhNode n_entity_decl,
                                                         vhNode old_entity_decl,
                                                         LFContext *context)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;

  NUModule *the_module = context->getLastVisitedModule();
  StringAtom* name = context->getInstanceName();
  SourceLocator loc = locator( vh_inst );



  UtString inst_name;
  StringAtom *instName = NULL;
  if ( mUseLegacyGenerateHierarchyNames ){
    instName = Populate::getInstanceMangledName(vh_inst, context, loc, mTicProtectedNameMgr);
  } else {
    (void) Populate::composeInstanceName(vh_inst, context, &inst_name, mTicProtectedNameMgr);
  }

  // Compose the partial instance path, like <parent_module_name>.<instance_name>
  NUModule* parentModule = context->getModule();
  HdlVerilogPath vhdl_path;
  UtString instPathName(parentModule->getOriginalName()->str());
  if ( mUseLegacyGenerateHierarchyNames ){
    vhdl_path.compPathAppend(&instPathName, instName->str());
  } else {
    vhdl_path.compPathAppend(&instPathName, inst_name.c_str());
  }

  // Check for self instantiation
  if (context->getModule() == the_module)
  {
    UtString inst_str;
    mMsgContext->RecursiveInstantion( &loc, decompileJaguarNode(vh_inst, &inst_str));
    return eSuccess; // This is not an erroneous situation as MTI/NC does 
  }
  
  NUScope* parent = context->getDeclarationScope();
  
  NUModuleInstance *the_instance = new NUModuleInstance(name, parent,
                                                        the_module, 
                                                        context->getNetRefFactory(), 
                                                        loc);
  
  
  unsigned int num_ports = the_module->getNumPorts();
  NUPortConnectionVector portconns(num_ports);
  
  // Initialize to 0 because not all ports may have connections.
  std::fill(portconns.begin(), portconns.end(), (NUPortConnection*)0);
  // Since IEEE std 1076.6-1999 (sec 8.5.2) does not allow binding
  // indication through config, the component name should match with
  // the declared entity name. So, we will not process the port-map of
  // the instantiation explicitly. Instead of that we will explore the
  // Jaguar's support which already has processed the port mapping.
  // When we open an instance, Jaguar sets the actual values as the
  // initial value of that master entity's port.
  vhNode n_port_clause = vhGetPortClause(n_entity_decl);
  JaguarList n_port_list;
  if (n_port_clause)
  {
    n_port_list = vhGetFlatSignalList(n_port_clause);
  }
  unsigned int portIndex = 0;
  unsigned int totalPorts = n_port_list.size();
  
  // Collect all the ports and actuals into portArray and actualArray,
  // expanding record ports correctly
  UtPtrArray portArray;
  VhNodeArray actualArray;
  UtSet<unsigned int> explicit_open_actuals; // indicies of any actualArray entries that were specified as open

  // Get the list of ports from the instance. Check which ones are intentionally left open.
  vhArray inst_port_array = vhGetPortMap(vh_inst);
  for (int i = 0; i < vhGetArraySize(inst_port_array); i++) 
  {
    vhNode port = vhGetNodeAt(i, inst_port_array);
    if (vhGetObjType(port) == VHOPEN && vhGetLineNumber(port) != 0)
    {
      explicit_open_actuals.insert(i);
    }
    else if(vhGetObjType(port) == VHAGGREGATE)
    {
      if(sDoesHaveOpenElem(static_cast<vhExpr> (port)))
      {
        POPULATE_FAILURE(mMsgContext->PortSubElemIsOpen(&loc),  &err_code);
        return err_code;
      }
    }
  }

  while ( portIndex < totalPorts )
  {
    NUNet* port = 0;
    vhNode formal_port = vhGetNextItem( n_port_list );
    vhNode actual_expr = NULL;

    if(n_entity_decl == old_entity_decl)
       	actual_expr = vhGetInitialValue( formal_port );
    else {
	// This is the case of substituttion. A VHDL master is being
	// substituted by another VHDL master.
       	actual_expr = NULL;
	JaguarString instName(vhGetName(vhGetLabelObj(vh_inst)));
	JaguarString formal_port_name(vhGetName(formal_port));
	JaguarString oldEntityName(vhGetEntityName(old_entity_decl));

	vhNode old_port_clause = vhGetPortClause(old_entity_decl);
	JaguarList old_port_list;
	if (old_port_clause) { old_port_list = vhGetFlatSignalList(old_port_clause); }

	for (IODBNucleus::SubstituteLoop p(mIODB->loopSubstituteModules(IODBNucleus::VHDL)); !p.atEnd(); ++p)
	{
	    IODBNucleus::NUSubstituteModuleData* sub = p.getValue();
	    UtString oldDU = p.getKey();

	    // Entity name gets priority over instance name
	    if( (0 == strcasecmp(oldDU.c_str(), oldEntityName))  
		    ||
                (0 == strcasecmp(oldDU.c_str(), instPathName.c_str())))
	    {
		// Found module in iodb. Now get the portmap
		UtMap<UtString,UtString>::iterator itr = sub->mPortMap->begin();
		while(itr != sub->mPortMap->end())
	       	{
		    if(0 == strcasecmp(formal_port_name,  itr->second.c_str()))
		    {
			vhListRewind(old_port_list);
			vhNode old_formal_port = NULL;
			while( (old_formal_port = vhGetNextItem( old_port_list )) )
		       	{
			    JaguarString old_formal_port_name(vhGetName(old_formal_port));
			    if(0 == strcasecmp(old_formal_port_name, itr->first.c_str())) {
				actual_expr = vhGetInitialValue( old_formal_port );
				break;
			    }
			}
			break;
		    }
		    itr++;
		}
		break;
	    }
	}
    }

    // Jaguar doesn't distinguish between actual ports and default values.
    // Here we check the scope of the actual expression. If it's in a package or 
    // in the entity, it's the default value.
    bool openPortWithDefault = false;
    if(actual_expr != NULL)
    {
      vhNode actualScope = vhGetScope(actual_expr);
    
      if(vhGetObjType(actualScope) == VHPACKAGEDECL)
        openPortWithDefault = true;
      else if(vhGetObjType(actualScope) == VHENTITY)
      {
        StringAtom *entityName = the_module->getOriginalName();
        if ( strcasecmp( vhGetEntityName(actualScope), entityName->str()) == 0)
          openPortWithDefault = true;
      }
    }

    // Check whether it is an open port or not. 
    if ( actual_expr == NULL || openPortWithDefault )
    {
      if ( !mCarbonContext->isNewRecord() && isRecordNet( formal_port, context ))
      {
        // Disconnected record. Add an entry for each element of the record.
        // Drill down in Jaguar to get the list of record elements
        vhNode typeDef;
        getVhdlRecordType( formal_port, &typeDef );
        RecordIterator ri( this, typeDef );
        
        for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
        {
          if ( isRecordNet( fieldElem, context )) {
            continue; // A nested record node is not another port
          }
          // code below expects a NULL entry in both port and actual array for disconnected ports.
          portArray.push_back(NULL); 
          actualArray.push_back(NULL);
        }
      }
      else 
      {
        portArray.push_back( NULL );
        actualArray.push_back( NULL );
      }
    }
    else if ( !mCarbonContext->isNewRecord() && isRecordNet( formal_port, context ))
    {
      temp_err_code = expandRecord( formal_port, portArray, the_module, context );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      {
        return err_code;
      }
      // Push each element of the actual_expr into actualArray also
      expandRecordPortActuals( actual_expr, actualArray, context );
    }
    else
    {
      temp_err_code = lookupPort( the_module, formal_port, context, &port );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
        return temp_err_code;
      }
      portArray.push_back( port );
      actualArray.push_back( actual_expr );
    }
    portIndex++;
  }
  
  // Walk the arrays of ports and actuals, and create port connections for them
  for ( portIndex = 0; portIndex < portArray.size(); ++portIndex )
  {
    NUNet *port = static_cast<NUNet*>( portArray[portIndex] );
    vhExpr actual_expr = static_cast<vhExpr>( actualArray[portIndex] );
    if ( port ) 
    {
      NUPortConnection* portconn = 0;
      temp_err_code = portConnection( actual_expr, context, port, &portconn );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
        return temp_err_code;
      }
        if (portconn) 
        {
          portconn->setModuleInstance(the_instance);
        }
        if ( portIndex >= num_ports )
        {
          SourceLocator loc = locator(actual_expr);
          mMsgContext->JaguarConsistency(&loc, "Out of bounds port index");
          return eFatal;
        }
        portconns[portIndex] = portconn;
    }
  }
  
  // Create port connections for unconnected ports.
  for (unsigned int i = 0; i < num_ports; ++i) 
  {
    if (portconns[i] == 0) 
    {
      NUNet *port = the_module->getPortByIndex(i);
      NUPortConnection* portconn = 0;
      ErrorCode port_err_code = unconnectedPort( vh_inst,
                                                 port, context, &portconn );
      if ( errorCodeSaysReturnNowWhenFailPopulate(port_err_code, &err_code ))
      {
        return port_err_code;
      }
      portconn->setModuleInstance(the_instance);
      portconns[i] = portconn;

      if ( explicit_open_actuals.find (i) == explicit_open_actuals.end ()) {
        mMsgContext->UnconnectedPort(&loc, port->getName()->str());
      }
    }
  }
  the_instance->setPortConnections(portconns);
  
  
  // Add the instance to the calling module or enclosing NamedDeclarationScope
  addModuleInstance(the_instance, parent);
  
  return err_code;
}

// This function processes the conditional signal assignment statements.
//
Populate::ErrorCode
VhdlPopulate::conditionalSigAsgn( vhNode vh_conc_assign, 
                                  LFContext *context,
                                  JaguarList& condWaveFrmList )
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  const SourceLocator loc = locator( vh_conc_assign );
  LOC_ASSERT( vhGetObjType( vh_conc_assign ) == VHCONDSIGASGN && condWaveFrmList, loc );
  vhNode vh_cond_wave_alt = vhGetNextItem( condWaveFrmList );
  if ( 0 == vh_cond_wave_alt )
  {
    return err_code;
  }
  vhExpr cond = vhGetCondition( vh_cond_wave_alt );
  NUExpr* cond_expr = 0;
  if ( cond )
    temp_err_code = condExpr( cond, context, &cond_expr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  // Get the waveform 
  vhNode waveFrm = vhGetWaveFrm( vh_cond_wave_alt );

  // Set up the scope to add statements to
  NUStmtList if_stmts;
  if ( cond_expr )
    context->pushStmtList( &if_stmts );

  // Check for VHDL-93 construct 'UNAFFECTED'
  bool unaffected = vhIsUnaffected( waveFrm );
  if ( !unaffected )
  {
    // Get the waveform element list
    vhArray eleList  = vhGetWaveFrmEleList( waveFrm );
    vhNode vh_wave_elem = vhGetNodeAt( 0, eleList );
    vhExpr valueExpr = vhGetValueExpr( vh_wave_elem );
    vhExpr targetExpr = vhGetTarget( vh_conc_assign );

    if ( !mCarbonContext->isNewRecord() && isRecordNet( targetExpr, context ))
    {
       temp_err_code = recordAssign( eSequentialNonBlocking, targetExpr, 
                                     valueExpr, context, loc );
       if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
     }
     else
     {
       NULvalue* the_lvalue = 0;
       temp_err_code = lvalue( targetExpr, context, &the_lvalue );
       if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
 
       int lWidth = 0;
       int numDims = 0;
       temp_err_code = getVhExprBitSize( targetExpr, &lWidth, &numDims, loc, context );
       if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
 
       NUExpr* the_rvalue = 0;
       temp_err_code = expr( valueExpr, context, lWidth, &the_rvalue );
       if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
     
       bool assignCreated = false;
       temp_err_code = helperAssign( the_lvalue, the_rvalue, 
                                     eSequentialNonBlocking,
                                     context, loc, assignCreated );
       if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
 
       if ( !assignCreated )
       {
         NUNonBlockingAssign *the_assign = NULL;
         the_assign = new NUNonBlockingAssign( the_lvalue, the_rvalue, false, 
                                               loc );
         context->addStmt( the_assign );
       }
     }
  }

  if (cond_expr)
  {
    context->popStmtList(); // pop the if_stmts list
    NUStmtList else_stmts;
    context->pushStmtList( &else_stmts );
    temp_err_code = conditionalSigAsgn( vh_conc_assign, context, condWaveFrmList );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    context->popStmtList(); // pop the else_stmts list

    NUIf *the_if = new NUIf( cond_expr, if_stmts, else_stmts,
                             context->getNetRefFactory(), false, loc );
    context->addStmt( the_if );
  }

  return err_code;
}

// This function processes the selected signal assignment statements.
// For the selected signal assignment, we can have more than one
// choices for a particular waveform. So, we need to process each
// choice.
// 
Populate::ErrorCode
VhdlPopulate::selectedSigAsgn( vhNode vh_conc_assign, 
                               LFContext *context,
                               JaguarList& selWaveFrmList,
                               vhNode vh_last_sel_wave,
                               JaguarList& choiceList )
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  const SourceLocator loc = locator( vh_conc_assign );
  LOC_ASSERT( vhGetObjType( vh_conc_assign ) == VHSELSIGASGN && selWaveFrmList, loc);
  // If last wave alternative is NULL, then get the next alternative
  // from the selWaveFrmList.
  if ( 0 == vh_last_sel_wave )
  {
    vh_last_sel_wave = vhGetNextItem( selWaveFrmList );
  }
  if ( 0 == vh_last_sel_wave )
  {
    return err_code;
  }

  if ( choiceList.empty())
  {
    choiceList = vhGetChoiceList( static_cast<vhExpr>( vh_last_sel_wave ));
  }

  vhNode vh_choice = vhGetNextItem( choiceList );
  // If the choice is NULL then take the next selected waveform
  if (0 == vh_choice)
  {
    // Free the previous choice list
    // Get the next waveform from the waveform list
    vh_last_sel_wave = vhGetNextItem( selWaveFrmList );
    if ( 0 == vh_last_sel_wave )
    {
      // No more waveform. Nothing to do ...
      return err_code;
    }
    // Get the corresponding choice list and choice
    choiceList = vhGetChoiceList( static_cast<vhExpr>( vh_last_sel_wave ));
    vh_choice = vhGetNextItem( choiceList );
  }
  // At this point we must have the choice expression
  LOC_ASSERT( vh_choice, loc );

  // Get the select expression
  vhExpr vh_select = vhGetSelExpr( vh_conc_assign );
  NUExpr* cond_expr = 0;
  bool isOthers = vhIsA( vh_choice, VHOTHERS );
  if ( !isOthers )
  {
    // Create the conditon expression like vh_select == vh_choice
    temp_err_code = caseAlternativeCond( vh_select, static_cast<vhExpr>( vh_choice ),
                                         context, &cond_expr );
    if ( 0 == cond_expr || 
         errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return temp_err_code;
    }
  }

  // Get the waveform 
  vhNode waveFrm = vhGetWaveFrm( vh_last_sel_wave );

  // Set up the scope to add statements to
  NUStmtList if_stmts;
  if ( !isOthers )
    context->pushStmtList( &if_stmts );

  // Check for VHDL-93 construct 'UNAFFECTED'
  bool unaffected = vhIsUnaffected( waveFrm );
  if ( !unaffected )
  {
    // Get the first waveform element
    vhArray eleList  = vhGetWaveFrmEleList( waveFrm );
    vhNode vh_wave_elem = vhGetNodeAt( 0, eleList );
    vhExpr valueExpr = vhGetValueExpr( vh_wave_elem );

     vhExpr targetExpr = vhGetTarget( vh_conc_assign );
     if (!mCarbonContext->isNewRecord() && isRecordNet(targetExpr, context))
     {
       temp_err_code = recordAssign( eSequentialNonBlocking, targetExpr, 
                                     valueExpr, context, loc );
       if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
     }
     else
     {
       NULvalue* the_lvalue = 0;
       temp_err_code = lvalue( targetExpr, context, &the_lvalue );
       if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
 
       int lWidth = 0;
       temp_err_code = getVhExprWidth( targetExpr, &lWidth, loc, context );
       if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
 
       NUExpr* the_rvalue = 0;
       temp_err_code = expr( valueExpr, context, lWidth, &the_rvalue );
       if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
 
       bool assignCreated = false;
       temp_err_code = helperAssign( the_lvalue, the_rvalue, 
                                     eSequentialNonBlocking,
                                     context, loc, assignCreated );
       if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
       
       if ( !assignCreated )
       {
         NUNonBlockingAssign *the_assign;
         the_assign = new NUNonBlockingAssign( the_lvalue, the_rvalue, false, 
                                               loc );
         the_assign->resize ();
         context->addStmt( the_assign );
       }
     }
  }

  if ( !isOthers )
  {
    context->popStmtList(); // pop the if_stmts list
    NUStmtList else_stmts;
    context->pushStmtList( &else_stmts );
    temp_err_code = selectedSigAsgn( vh_conc_assign, context, selWaveFrmList,
                                     vh_last_sel_wave, choiceList );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    context->popStmtList(); // pop the else_stmts list

    NUIf *the_if = new NUIf( cond_expr, if_stmts, else_stmts,
                             context->getNetRefFactory(), false, loc );
    context->addStmt( the_if );
  }

  return err_code;
}


// This function creates an uniquified module name based on the
// generic values. 
Populate::ErrorCode VhdlPopulate::uniquifyEntityName(vhNode vh_entity,
                                                     UtString &uniquifiedModName)
{
  ErrorCode err_code = eSuccess;
  vhNode vh_generic_clause = vhGetGenericClause(vh_entity);
  if (NULL == vh_generic_clause) // This entity does not have any generic
  {
    return eSuccess;
  }
  JaguarList vh_generic_list(vhGetFlatGenericList(vh_generic_clause));
  vhNode vh_generic = 0;
  //char generic_value_str[12]; // Since the generic value can only be integer
                              // as per IEEE 1076.6-1999 STD, we need only
                              // 10(for INT_MAX) + 1(for - sign) + 1( for
                              // terminating NULL) chars to represent the 
                              // generic value as string.
  while((vh_generic = vhGetNextItem(vh_generic_list)))
  {
    // The API vhGetInitialValue() will return the elaborated value of the
    // generic since we have already elaborated the instance.
    vhExpr vh_value = vhGetInitialValue(vh_generic);
    if(NULL == vh_value)
    {
      // Report error that this generic is not initialized yet. 
      SourceLocator lineNo = locator(vh_generic);
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&lineNo, 
                                                      "Generic parameter does not have initial value"),
                       &err_code);
      continue;
    }
    vhExpr evaluatedVal = evaluateJaguarExpr(vh_value);
    switch(vhGetObjType(evaluatedVal))
    {
    case VHDECLIT:
    {
      uniquifiedModName << "_" << (int)vhGetDecLitValue(evaluatedVal);
      break;
    }
    case VHBASELIT:
    {
      double value = vhGetBaseLitValue(evaluatedVal);
      uniquifiedModName << "_" << (int)value;
      break;
    }
    case VHSTRING:
    case VHBITSTRING:
    {
      uniquifiedModName += (char*)vhGetStringVal( evaluatedVal );
      break;
    }
    case VHCHARLIT:
    {
      uniquifiedModName << "_" << vhGetCharLitValue( evaluatedVal );
      break;
    }
    case VHIDENUMLIT:
    {
      uniquifiedModName << "_" << vhGetIdEnumLitValue( evaluatedVal );
      break;
    }
    default:
    {
      uniquifiedModName += "_unsupGenType";
      break;
    }
    } // End switch
  } // End while
  return err_code;
}



// This function processes the waveform of a conditional signal assignment 
// statement into equivalent ternary expression. This is required in order to 
// enable the tristate analysis of 'cbuild'.  To enable the tristate analysis, 
// we will populate the nucleus as below.
// Example:
// out1 <= v1 when c1 else 
//         v2 when c2 else
//         ...
//         vn when cn else
//         vn+1;
//  
// If the last value i.e. vn+1 contains tristate 'Z'  value then we will
// populate the nucleus as:
// assign out1 = (C1 || C2 || Cn) ? ( C1 ? V1 : (C2 ? V2 : ... (Cn-1 ? Vn-1 : Vn))) : Vn+1;
//  
// Othewise we will populate the nucleus as :
// assign out1 = C1 ? V1 : (C2 ? V2 : ... (Cn ? Vn : Vn+1 ));
Populate::ErrorCode
VhdlPopulate::convertCondWaveFrmIntoTernaryExpr( LFContext *context, 
                                                 JaguarList& condWaveFrmList,
                                                 NUExpr** valueExpr,
                                                 NUExpr** oredConds,
                                                 NUExpr** uncondRval,
                                                 const bool drivesZ)
{ 
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  bool firstCall = false; 
  vhNode vh_cond_wave_alt = vhGetNextItem( condWaveFrmList );
  if ( 0 == vh_cond_wave_alt )
  {
    return err_code;
  } 
  vhExpr cond = vhGetCondition( vh_cond_wave_alt );
  NUExpr* cond_expr = 0;
  if ( cond )
  {
    temp_err_code = condExpr( cond, context, &cond_expr );
  } 
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    return temp_err_code;
  } 
  CopyContext cp_context(NULL,NULL);
  if (drivesZ)
  {
    if (*oredConds && cond_expr)
    {
      SourceLocator loc = locator( cond );
      *oredConds = new NUBinaryOp( NUOp::eBiLogOr, *oredConds, 
                                  cond_expr->copy(cp_context), loc );
      (*oredConds)->resize(1);
    }
    else if (cond_expr)
    {
      // This is the first call becuase the '*oredConds' is NULL when this
      // function is called first.
      firstCall = true;
      *oredConds = cond_expr->copy(cp_context);
    }
  }
  // Get the waveform 
  vhNode waveFrm = vhGetWaveFrm( vh_cond_wave_alt );

  // Get the waveform element list
  vhArray eleList  = vhGetWaveFrmEleList( waveFrm );
  vhNode vh_wave_elem = vhGetNodeAt( 0, eleList );
  vhExpr vh_value = vhGetValueExpr( vh_wave_elem );
  NUExpr* trueValExpr = 0;
  temp_err_code = expr( vh_value, context, 0, &trueValExpr );
  if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    return temp_err_code;
  }

  if ( !cond_expr )
  {
    // The last value of the waveform i.e. the value of the ELSE part without
    // any condition. So, there is no ather value.
    *valueExpr = trueValExpr;
    return err_code;
  }
  NUExpr* falseValExpr = NULL;
  temp_err_code = convertCondWaveFrmIntoTernaryExpr( context,
                                                     condWaveFrmList,
                                                     &falseValExpr,
                                                     oredConds,
                                                     uncondRval,
                                                     drivesZ);
  if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    return temp_err_code;
  }
  SourceLocator loc = locator( waveFrm );
  if (trueValExpr && falseValExpr)
  {
    if (drivesZ)
    {
      if (firstCall)
      {
        // This is the time when we will create the final ternary expr
        // corresponding to the given conditional waveforms. So, create the 
        // final expression as:
        // (c1 | c2 | c3 ) ? (c1 ? v1 : (c2 ? v2 : v3 ) : v4

        // First create the expr (c1 ? v1 : (c2 ? v2 : v3 )
        NUExpr* temp = new NUTernaryOp( NUOp::eTeCond, cond_expr, trueValExpr,
                                        falseValExpr, loc );
        // Now create the final expr
        if (NULL == *uncondRval) //simple case: out1 <= in1 when enable else 'z'
        {
          *valueExpr = temp;
          delete (*oredConds);
        }
        else
        {
          *valueExpr = new NUTernaryOp( NUOp::eTeCond, *oredConds, temp,
                                        *uncondRval, loc );
        }
      }
      else
      {
        // create ternary expression
        if ( (falseValExpr->getType() == NUOp::eNUTernaryOp) || 
             (NULL != *uncondRval))
        {
          *valueExpr = new NUTernaryOp( NUOp::eTeCond, cond_expr, trueValExpr,
                                        falseValExpr, loc );
        }
        else 
        {
          *uncondRval = falseValExpr;
          *valueExpr = trueValExpr;
          delete cond_expr;
        }
      }
    }
    else
    {
      *valueExpr = new NUTernaryOp( NUOp::eTeCond, cond_expr, trueValExpr,
                                    falseValExpr, loc );
    }
  }
  return err_code;
} 

Populate::ErrorCode
VhdlPopulate::unsupportedConcStmt(vhNode vh_conc_stmt)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator( vh_conc_stmt );
  UtString msgStr;
  msgStr << "Unsupported concurrent statement ("
         << gGetVhNodeDescription( vh_conc_stmt ) << ").";
  POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, msgStr.c_str( )), &err_code);
  return err_code;
}

Populate::ErrorCode
VhdlPopulate::implicitGuardNullInGuardedBlock(vhNode vh_block)
{
  SourceLocator loc = locator( vh_block );
  LOC_ASSERT(0, loc); // The GUARD signal must be available
  return eFailPopulate;
}

Populate::ErrorCode
VhdlPopulate::assertStmt(vhNode vh_conc_stmt, LFContext*)
{
  SourceLocator loc = locator( vh_conc_stmt );
  mMsgContext->ConcAssertUnsupported( &loc );
  // Ignore for now
  return eSuccess;
}

Populate::ErrorCode
VhdlPopulate::forGenerate(vhNode vh_conc_stmt, StringAtom* label, LFContext* context)
{
  SourceLocator loc = locator(vh_conc_stmt);
  NUNamedDeclarationScope *block_scope = new 
    NUNamedDeclarationScope( label, context->getDeclarationScope(), 
                             mIODB, context->getNetRefFactory(), loc, eHTGenerBlock);
  context->pushDeclarationScope(block_scope);
  context->pushJaguarScope(vh_conc_stmt);
  return eSuccess;
}

Populate::ErrorCode
VhdlPopulate::ifGenerateTrue(vhNode vh_conc_stmt, const char* vh_label_str, LFContext* context)
{
  SourceLocator loc = locator(vh_conc_stmt);
  LOC_ASSERT(vh_label_str, loc);

  NUNamedDeclarationScope *block_scope = NULL;
  StringAtom* label = context->vhdlIntern(vh_label_str);
  
  block_scope = new NUNamedDeclarationScope( label, 
                                             context->getDeclarationScope(), 
                                             mIODB, context->getNetRefFactory(),
                                             loc, eHTGenerBlock);
  context->pushDeclarationScope(block_scope);
  context->pushJaguarScope(vh_conc_stmt);
  return eSuccess;
}


Populate::ErrorCode
VhdlPopulate::concurrentConditionalSigAssign(vhNode vh_conc_stmt, 
                                             LFContext* context)
{
  ErrorCode err_code = eSuccess;
  ErrorCode tmp_err_code = eSuccess;
  NUModule* module = context->getModule();
  SourceLocator loc = locator(vh_conc_stmt);

  // We shall try our best to create ternary expression corresponding
  // to the conditional waveforms. This is required in order enable our
  // tristate analysis. If we find that creation of the ternary expression
  // is not straight forward then we will convert this conditional signal
  // assignment into IF-ELSIF-ELSE statements. For example, if the conditional
  // waveform contains the keyword 'UNAFFECTED' we will not create ternary
  // expression instead we will create IF[-ELSIF] statement. 
  //
  // If the target net is a memory net then we will not create ternanry
  // expression instead we will populate IF-ELSIF-ELSE statement. This is
  // because tristate analysis does not happen for memory nets.
  //
  // For information pls. have a look into the description of the bug3633 and
  // bug3640 . --KLG 01/12/2005
  //
  bool drivesZ = false;
  NUNet * targetNet = NULL;
  vhNode vh_target = vhGetTarget( vh_conc_stmt );
  vhObjType target_type = vhGetObjType(vh_target);
  // The target net can be of type aggregate, indexed name, slice name
  // signal or selected name. Let us retrieve the NUNet corresponding to 
  // the target signal.
  switch (target_type)
  {
  case VHAGGREGATE:
    // We will not do tristate enabling related processing for
    // aggregate type lvalues.
    break;
  case VHSELECTEDNAME:
    // Tristate analysis can be done for record elements just like any other net.
    tmp_err_code = lookupNet( context->getDeclarationScope(), vh_target, context,
                              &targetNet );
    break;
  case VHINDNAME:
  case VHSLICENAME:
    vh_target = vhGetPrefix( static_cast<vhExpr>( vh_target ));
    if ( vhGetObjType(vh_target) != VHSIGNAL && 
         vhGetObjType(vh_target) != VHOBJECT )
    {
      break;
    }
    // Fall thru for the net lookup with this signal.
  case VHSIGNAL:
  case VHOBJECT:
    // Tristate analysis will not be enabled if the LHS is full record
    // object.
    if ( vh_target && not isRecordNet( vh_target, context ))
    {
      if (vhGetObjType(vh_target) == VHOBJECT)
        vh_target = vhGetActualObj(vh_target);
      tmp_err_code = lookupNet(context->getDeclarationScope(), vh_target,
                               context, &targetNet);
    }
    break;
  default:
    break;
  }
  if (errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code,&err_code))
  {
    return tmp_err_code;
  }

  bool targetNetIsNotRecordOrMemory = false;
  if ( targetNet and ( not targetNet->isMem()) )
  {
    targetNetIsNotRecordOrMemory = true; // we didn't check for record-ness!
  }
  if ( targetNetIsNotRecordOrMemory and 
       sIsConditionalWaveFrmConvertableIntoTernaryExpr(vh_conc_stmt,drivesZ)) 
  {
    JaguarList waveFrmList(vhGetConditionalWaveFrmList(vh_conc_stmt));
    NUExpr* valueExpr = NULL;
    NUExpr* oredConds = NULL;
    NUExpr* uncondRval = NULL;
    tmp_err_code = convertCondWaveFrmIntoTernaryExpr(context, waveFrmList, 
                                                     &valueExpr, &oredConds,
                                                     &uncondRval, drivesZ);
    if (errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code,&err_code))
    {
      return tmp_err_code;
    }
    NULvalue* the_lvalue = 0;
    vhNode target_node = vhGetTarget( vh_conc_stmt );
    tmp_err_code = lvalue( target_node, context, &the_lvalue );
    if (errorCodeSaysReturnNowWhenFailPopulate( tmp_err_code, &err_code ))
    {
      return tmp_err_code;
    }
    if ( vhIsGuarded(vh_conc_stmt))
    {
      // For the GUARDED assignment, we will create the assignment as:
      // out1 <= GUARD ? valueExpr : out1 ; 
      vhNode vh_block = vhGetScope(vh_conc_stmt);
      vhExpr vh_guard_expr = vhGetGuardExpr(vh_block);
      int dummy_edge;
      if ( detectClock( vh_guard_expr, &dummy_edge ))
      {
        // A guarded block sensitive to a clock edge is unsupported
        mMsgContext->ClockOnGuardedBlock(&loc); 
        return eFatal;
      }
      NUExpr *the_guard_expr = 0;
      tmp_err_code = condExpr(vh_guard_expr, context, &the_guard_expr);
      if ( tmp_err_code == eNotApplicable ) {
        // fall through to try again (with expr())
      } else if ( errorCodeSaysReturnNowWhenFailPopulate( tmp_err_code, &err_code )) {
        return tmp_err_code;
      }
      NUExpr *lvalExpr = NULL;
      tmp_err_code = expr( static_cast<vhExpr>(target_node), context, 0, &lvalExpr );
      if (errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ))
      {
        return tmp_err_code;
      }
      valueExpr = new NUTernaryOp( NUOp::eTeCond, the_guard_expr, 
                                   valueExpr, lvalExpr, loc );
    }
    bool assignCreated = false;
    tmp_err_code = helperAssign( the_lvalue, valueExpr, eConcurrent, context,
                                 loc, assignCreated );
    if ( errorCodeSaysReturnNowWhenFailPopulate( tmp_err_code, &err_code ))
    {
      return tmp_err_code;
    }
    if ( !assignCreated )
    {
      StringAtom *block_name = context->getModule()->newBlockName(
                                                                  context->getStringCache(), loc );
      NUContAssign *the_assign = new NUContAssign( block_name, the_lvalue,
                                                   valueExpr, loc );
      module->addContAssign( the_assign );
    }
  }
  else
  {
    // Hmm... This waveform is not convertable in ternary expr.
    // No break stmt. because we want to create a dummy process and put the
    // equivalent IF-ELSIF-ELSE stmt corresponding to this conditional signal 
    // assignment into that process.
    // treat as selsigassign
    return concurrentSelSigAssign(vh_conc_stmt, context);
  }
  
  return err_code;
}

Populate::ErrorCode
VhdlPopulate::concurrentSelSigAssign(vhNode vh_conc_stmt, 
                                     LFContext* context)
{
  ErrorCode err_code = eSuccess;
  ErrorCode tmp_err_code = eSuccess;

  // We will internally convert the cond. signal assignment
  // into sequential if/elsif statements inside a dummy process
  // stmt. The selected signal assignment will be mapped into 
  // sequential if/else statement inside a dummy process stmt.
  NUAlwaysBlock* the_process = 0;
  NUModule* module = context->getModule();
  
  tmp_err_code = dummyAlwaysBlock(vh_conc_stmt, context, &the_process);
  if (errorCodeSaysReturnNowOnlyWithFatal(tmp_err_code,&err_code))
  {
    return tmp_err_code;
  }
  if (the_process) 
  {
    module->addAlwaysBlock(the_process);
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::maybeGuardedConcSigAssign(vhNode vh_conc_stmt, 
                                        LFContext* context)
{
  ErrorCode err_code = eSuccess;
  ErrorCode tmp_err_code = eSuccess;
  
  NUModule* module = context->getModule();
  if (vhIsGuarded(vh_conc_stmt))
  {
    // Create a process stmt for this sig assign, put this signal
    // assignment under the IF (GUARD_EXPR) condition.
    NUAlwaysBlock* the_process = 0;
    tmp_err_code = dummyAlwaysBlock(vh_conc_stmt, context, &the_process);
    if (errorCodeSaysReturnNowOnlyWithFatal(tmp_err_code,&err_code))
    {
      return tmp_err_code;
    }
    if (the_process) 
    {
      module->addAlwaysBlock(the_process);
    }
  }
  else
  {
    NUContAssign *the_assign = 0;
    tmp_err_code = concurrentSignalAssign(vh_conc_stmt,context,&the_assign);
    if (errorCodeSaysReturnNowOnlyWithFatal(tmp_err_code,&err_code))
    {
      return tmp_err_code;
    }
    if (the_assign) 
    {
      module->addContAssign(the_assign);
    }
  }

  return err_code;
}
