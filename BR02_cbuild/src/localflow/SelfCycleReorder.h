// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SELFCYCLEREORDER_H_
#define SELFCYCLEREORDER_H_

#include "nucleus/Nucleus.h"

class Reorder;
class UD;
class AtomicCache;
class NUNetRefFactory;
class IODBNucleus;
class ArgProc;
class MsgContext;
class ReduceUtility;
class AllocAlias;

//! SelfCycleReorderClass
/*!
 * This class tries to eliminate self-cycles with combinational always blocks
 * by reordering the assigns within the always block.
 *
 * Note that it is not always possible to eliminate self-cycles with
 * statement reordering, so this class will not eliminate all combinational
 * self-cycles.
 *
 * An example of a case which reordering can solve is (assuming there are
 * no other defs of b):
 *
\code
  always
  begin
    a = b;
    b = c;
  end
\endcode
 *
 * This cycle settles after 2 executions, but we can just reorder the
 * code so that there is no cycle:
 *
\code
  always
  begin
    b = c;
    a = b;
  end
\endcode
 *
 * See bug 3403 for a customer example.
 *
 * This analysis assumes non-blocking reordering has already taken place
 * (it will assert if not).
 */
class SelfCycleReorder
{
public:
  //! constructor
  SelfCycleReorder(NUModule *module,
                   AtomicCache *str_cache,
                   NUNetRefFactory *netref_factory,
                   IODBNucleus *iodb,
                   ArgProc *args,
                   MsgContext *msg_ctx,
                   AllocAlias *alloc_alias);

  //! Destructor
  ~SelfCycleReorder();

  //! Perform the analysis.
  void analyze();

private:
  //! Hide copy and assign constructors.
  SelfCycleReorder(const SelfCycleReorder&);
  SelfCycleReorder& operator=(const SelfCycleReorder&);

  //! Analyze the given always block.
  void alwaysBlock(NUAlwaysBlock *block, ReduceUtility &re_util);

  /*!
   * Return true if the given block has feedback and should be attempted to be
   * reordered.  If true is returned, the cycles set will be populated with
   * the nets which feedback.
   */
  bool qualifyTryReorder(NUAlwaysBlock *block,
                         NUNetRefSet &cycles,
                         ReduceUtility &re_util);

  //! Return true if the given net has any drivers other than the disregarded always block.
  bool multipleDrivers(NUNetRefHdl &ref, NUAlwaysBlock *disregard);

  //! Reorder object used to lay out the statements.
  Reorder *mReorder;

  //! UD analysis object.
  UD *mUD;

  //! Module to analyze
  NUModule *mModule;

  //! Netref factory
  NUNetRefFactory *mNetRefFactory;

  //! IODB
  IODBNucleus *mIODB;

  //! Will be set to true if any work was done so that UD is recomputed.
  bool mRecomputeUD;
};

#endif
