// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/SensitivityList.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUAlwaysBlock.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "iodb/IODBNucleus.h"

//! Iterates through always blocks and check the sensitivity list
class CheckSensitivityList : public NUDesignCallback
{
public:
  CheckSensitivityList(IODBNucleus* iodb, MsgContext* context, NUNetRefFactory* factory, NUAlwaysBlockNetRefSetMap* netref) 
    : mIODB(iodb), mMsgContext(context), mFactory(factory),
      mNetRefSet(netref)
  {
  }

  ~CheckSensitivityList()
  {
  }


  //! By default, walk through everything
  Status operator() (Phase , NUBase * ) { return eNormal; }

  Status operator() (Phase, NUModuleInstance* ) {return eSkip;}


  Status operator() (Phase phase, NUModule* module)
  {
    if (phase == ePre)
    {
      // We are entering a module. If it has any always block,
      // to be removed, assert.
      NU_ASSERT(mRemoveAlwaysBlocks.size() == 0, module);
    }
    if (phase == ePost)
    {
      // Remove always blocks, that were found not triggered.
      // See bug 8793.
      for (NUAlwaysBlockVector::iterator  itr = mRemoveAlwaysBlocks.begin();
           itr != mRemoveAlwaysBlocks.end(); ++itr)
      {
        NUAlwaysBlock* always = *itr;
        module->removeAlwaysBlock(always);
        delete always;
      }
    }
    return eNormal;
  }
  
  Status operator() (Phase phase, NUAlwaysBlock *always_block)
  {
    NUModule* module = always_block->findParentModule();
    if(module->getSourceLanguage() == eSLVerilog )
    {
      if (phase == ePre)
      {
        if((always_block->isSequential() == false)  &&
           (always_block->isOrigLevelExprList() == true))
        {
          checkAlwaysForExpressions(always_block);

          checkAlwaysSensitivityList(always_block);

          always_block->setLevelExprList(false);
        }
      }
      else if(phase == ePost)
      {
        if(always_block->isSequential() == false)
          removeExpressions(always_block);
      }
    }

    return eNormal;
  }


  // This function checks that all the nets used in the always block
  // are included in the sensitivity list. (when there is a sensitivity list).
  // If not, then a message about the missing signals in sensitivity list is printed.
  void checkAlwaysSensitivityList(NUAlwaysBlock* always)
  {
    // first check to see if this always block had a sensitivity list that should be checked
    if( ( always->isWildCard())    || // always @(*)
        ( always->isAlwaysComb() ) || // always_comb  (SV only)
        ( always->isAlwaysLatch() )   // always_latch (SV only)
      ) {
      return;  // nothing needs to be checked
    }
    
    // Find the nets that are used in the always block.
    NUNetRefSet net_uses(mFactory);
    always->getUses(&net_uses);

    NUNetRefSet sense_uses(mFactory);
    NUExprList* level_expr_list =  always->getLevelExprList();
    for(NUExprList::const_iterator itr = level_expr_list->begin();
          itr !=  level_expr_list->end(); ++itr)
    {
      // Find the nets that are included in the sensitivity list
      NUExpr* expr = *itr;
      expr->getUses(&sense_uses);
    }
    
    // Now we subtract from used nets  all the nets that are in the
    // sensitivity list.
    // The set_subtract does operation 
    // net_uses = net_uses - sense_uses 
    NUNetRefSet::set_subtract(sense_uses, net_uses);

    // We can't just subtract sence_uses from orig_net_uses, because
    // loop unrolling in some cases removes net_uses (see test/synth/synth_test_13.v)
    // Now we check, whether any of used defs was created by loop unrolling.
    // If yes, then the diff should be not empty. 
    // If we have new used nets, remove them as well.
    NUAlwaysBlockNetRefSetMap::iterator iter =  mNetRefSet->find(always);
    if(iter != mNetRefSet->end())
    {
      NUNetRefSet& orig_net_uses = iter->second;
      NUNetRefSet  diff(mFactory);
      NUNetRefSet::set_difference(net_uses, orig_net_uses, diff);
      if(diff.empty() == false)
      {
        NUNetRefSet::set_subtract(diff, net_uses);
      }
    }
    

    for (NUNetRefSet::SortedLoop l = net_uses.loopSorted(); !l.atEnd(); ++l) 
    {
      UtString name;
      const NUNetRefHdl netRef = *l;
      NUNet* net = netRef->getNet();
      if(net->isHierRef())
      {
        HdlVerilogPath hdl;
        net->getHierRef()->getName(&name, hdl);
      }

      if(net->isTemp())
      {
        StringAtom* orig_name = net->getOriginalName();
        if(orig_name != NULL)
          name = orig_name->str();
      }

      if(name.size() == 0)
        netRef->compose(&name, NULL, true, true, ".", false, true);
      
      // Check whether this net should be suppressed.
      // The message could be suppressed for a particular net 
      // by ignoreSynthCheck directive.
      bool ignore_found = false;
      for (IODB::NameSetLoop p = mIODB->loopIgnoreSynthCheck(); 
           !p.atEnd(); ++p)
      {
        const STSymbolTableNode* leaf = *p;
        NUNet* leaf_net = dynamic_cast<NUNet*> (mIODB->getNucleusObject(leaf));
        if(leaf_net == net)
        {
          ignore_found = true;
          break;
        }
      }

      // Print the message
      if(ignore_found == false)
        mMsgContext->MissingInSensitivityList(&(always->getLoc()), name.c_str());
    }
    return;
  }


  void checkAlwaysForExpressions(NUAlwaysBlock* always)
  {
    NUExprList* level_expr_list =  always->getLevelExprList();
    int num_expr = level_expr_list->size();
    int num_consts = 0;
    for(NUExprList::const_iterator itr = level_expr_list->begin();
          itr !=  level_expr_list->end(); ++itr)
    {
      NUExpr* expr = *itr;
      NUExpr::Type type = expr->getType();
      if(type == NUExpr::eNUUnaryOp)
      {
        NUUnaryOp* op = dynamic_cast<NUUnaryOp*>(expr);
        if((op->getOp() == NUOp::eUnLogNot) ||
           (op->getOp() == NUOp::eUnBitNeg))
        {
          expr = op->getArg(0);
          type = expr->getType();
        }
      }

      // We exclude the eNUConstNoXZ expression here and process them out of the loop
      if((type != NUExpr::eNUIdentRvalue) && (type != NUExpr::eNUVarselRvalue)
         && (type != NUExpr::eNUMemselRvalue) && (type != NUExpr::eNUConstNoXZ))
      {
        // Print the message
        mMsgContext->InvalidExprInSensitivityList(&(always->getLoc()));
      }

      if(type == NUExpr::eNUConstNoXZ)
         num_consts++;
    }

    // If num_consts == num_expr, then all the expression in the sensitivity 
    // list are constants. Print the message.
    // If there is at least one not constant net in the sensitivity list,
    // then the block gets triggered based on that net. Don't print the message.
    if((num_expr != 0) && (num_consts == num_expr))
    {
      mMsgContext->ConstExprOnlyInSensitivityList(&(always->getLoc()));
      mRemoveAlwaysBlocks.push_back(always);
    }

    return;
  }

  void removeExpressions(NUAlwaysBlock* always)
  {
    NUExprList* level_expr_list =  always->getLevelExprList();
    for(NUExprList::const_iterator itr = level_expr_list->begin();
          itr !=  level_expr_list->end(); ++itr)
    {
      NUExpr* expr = *itr;
      delete expr;
    }
    level_expr_list->clear();

    return;
  }

private:
  IODBNucleus*         mIODB;
  MsgContext*          mMsgContext;
  NUNetRefFactory*     mFactory;
  NUAlwaysBlockVector  mRemoveAlwaysBlocks;

  NUAlwaysBlockNetRefSetMap* mNetRefSet;  // Map of always blocks and net references
};



// This class is used to gather all the used nets in the combinational
// always blocks. It stores that info in the mNetRefSet map.
class GatherUsedNets : public NUDesignCallback
{
public:
  GatherUsedNets(IODBNucleus* iodb, MsgContext* context, NUNetRefFactory* factory, NUAlwaysBlockNetRefSetMap* netref) 
    : mIODB(iodb), mMsgContext(context), mFactory(factory),
      mNetRefSet(netref)
  {
  }

  ~GatherUsedNets()
  {
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * ) { return eNormal; }


  Status operator() (Phase phase, NUAlwaysBlock *always_block)
  {
    NUModule* module = always_block->findParentModule();
    if(module->getSourceLanguage() == eSLVerilog )
    {
      if (phase == ePre)
      {
        if((always_block->isSequential() == false)  &&
           (always_block->isOrigLevelExprList() == true))
        {
          NUNetRefSet net_uses(mFactory);
          always_block->getUses(&net_uses);
          mNetRefSet->insert(NUAlwaysBlockNetRefSetMap::value_type(always_block, net_uses));
        }
      }
    }

    return eNormal;
  }


private:
  IODBNucleus*         mIODB;
  MsgContext*          mMsgContext;
  NUNetRefFactory*     mFactory;

  NUAlwaysBlockNetRefSetMap* mNetRefSet;   // Map of always blocks and net references
};



SensitivityList::SensitivityList(IODBNucleus* iodb, MsgContext* msgCtx, NUNetRefFactory* factory)
  : mIODB(iodb), mMsgContext(msgCtx), mFactory(factory)
{
  mIsGatherDone = false;
}

SensitivityList::~SensitivityList()
{
}

bool SensitivityList::gatherInfo(NUModule* mod)
{
  GatherUsedNets gather_used_nets_cb(mIODB, mMsgContext, mFactory, &mAlwaysBlockNetRefSet);
  NUDesignWalker module_walker(gather_used_nets_cb, false );
  module_walker.module(mod);

  // Gather phase was completed
  mIsGatherDone = true; 

  return true;
}
 
bool SensitivityList::check(NUModule* mod)
{
  NU_ASSERT(mIsGatherDone == true, mod);
  
  CheckSensitivityList check_sensitivity_cb(mIODB, mMsgContext, mFactory, &mAlwaysBlockNetRefSet);
  NUDesignWalker module_walker(check_sensitivity_cb, false );
  module_walker.module(mod);

  return true;
}
