// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implementation of clock driver package.
 */

#include "localflow/ClockAndDriver.h"
#include "nucleus/NUNet.h"


ClockAndDriver::ClockAndDriver()
{
  mClocks = new NUCNetElabSet;
  mClockDrivers = new FLNodeElabSet;
}


ClockAndDriver::~ClockAndDriver()
{
  delete mClocks;
  delete mClockDrivers;
}


void ClockAndDriver::addClock(const NUNetElab * net_elab)
{
  mClocks->insert(net_elab);
  for (NUNetElab::ConstDriverLoop loop(net_elab->loopContinuousDrivers());
       not loop.atEnd();
       ++loop) {
    mClockDrivers->insert(*loop);
  }
}


bool ClockAndDriver::isClock(const NUNetElab * net_elab) const
{
  return (mClocks->find(net_elab) != mClocks->end());
}


bool ClockAndDriver::isClockDriver(const FLNodeElab * flow_elab) const
{
  return (mClockDrivers->find(const_cast<FLNodeElab*>(flow_elab)) !=
          mClockDrivers->end());
}
