// -*-C++-*-
/******************************************************************************
 Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/InLineDirectivesCB.h"
#include "compiler_driver/CheetahContext.h"
#include "compiler_driver/JaguarContext.h"
#include "localflow/DesignPopulate.h"
#include "localflow/PrePopInterraCB.h"
#include "localflow/TicProtectedNameManager.h"
#include "util/UtStackPOD.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "iodb/IODBDesignData.h"
#include "LFContext.h"


// InLineDirectivesCB class is used by the InterraDesignWalker as the
// callback class.  It is used to process in-line directives in the
// verilog files, currently directives from any VHDL files is
// processed in JaguarContext::processMetaDirectives
InLineDirectivesCB::InLineDirectivesCB(CarbonContext* context)
  : mCarbonContext(context) 
{
  mTicProtectedNameMgr = mCarbonContext->getTicProtectedNameManager();
  mIODB = mCarbonContext->getIODB();
  mErrCode = Populate::eSuccess;
}

InLineDirectivesCB::~InLineDirectivesCB()
{
}

InterraDesignWalkerCB::Status 
InLineDirectivesCB::hdlEntity(Phase phase, mvvNode node, mvvLanguageType langType, const char* /*entityName*/, bool* /*isCmodel*/)
{
  InterraDesignWalkerCB::Status walk_status = eNormal;
  
  if (phase == ePre) {
    if (langType == MVV_VERILOG) {
      veNode ve_module = node->castVeNode();
      UtString mod_name;
      mTicProtectedNameMgr->getVisibleName(ve_module, &mod_name);
      mModuleNameStack.push(mod_name);
      Populate::ErrorCode directive_status = mCarbonContext->getCheetahContext()->declarePragmaDirectives(mod_name.c_str(), NULL, ve_module);
      if ( directive_status != Populate::eSuccess) {
        putErrCode(directive_status);
        walk_status = eStop;
      }
      mBlockNameStack.push(""); // each module starts a new nested block path
    }
  } else if (phase == ePost) {
    if (langType == MVV_VERILOG) {
      mModuleNameStack.pop();
      mBlockNameStack.pop();
    }
  }
  
  return walk_status;
}

// common routine for handling net like objects (nets, localnet, variables...)
InterraDesignWalkerCB::Status
InLineDirectivesCB::processVerilogNetLikeObject(mvvNode node)
{
  InterraDesignWalkerCB::Status walk_status = eNormal;

  veNode ve_net = node->castVeNode();

  UtString net_name;
  mTicProtectedNameMgr->getVisibleName(ve_net, &net_name);

  INFO_ASSERT(!mBlockNameStack.empty(),"Module name stack inconsistency.");
  UtString mod_name = mModuleNameStack.top();

  INFO_ASSERT(!mBlockNameStack.empty(),"Block name stack inconsistency.");
  UtString hier_net_name(mBlockNameStack.top());
  hier_net_name += net_name;

  Populate::ErrorCode directive_status = mCarbonContext->getCheetahContext()->declarePragmaDirectives(mod_name.c_str(), hier_net_name.c_str(), ve_net);
  if ( directive_status != Populate::eSuccess) {
    putErrCode(directive_status);
    walk_status = eStop;
  }
  return walk_status;
}




InterraDesignWalkerCB::Status
InLineDirectivesCB::generateBlock(Phase phase, mvvNode /*node*/, const char* blockName, mvvLanguageType /*langType*/, const SInt32* /*genForIndex*/)
{
  UtString nestedBlockPath;
  
  if (phase == ePre) {
    INFO_ASSERT(!mBlockNameStack.empty(),"Block name stack inconsistency.");
    nestedBlockPath = mBlockNameStack.top();
    nestedBlockPath << blockName << ".";
    mBlockNameStack.push(nestedBlockPath);
  }
  else {
    mBlockNameStack.pop();
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
InLineDirectivesCB::alwaysBlock(Phase phase, mvvNode node, mvvLanguageType)
{
  INFO_ASSERT(!mBlockNameStack.empty(),"Block name stack inconsistency.");
  UtString nestedBlockPath(mBlockNameStack.top());
  veNode block = node->castVeNode();
  veNode ve_stmt = veAlwaysGetStatement(block);
  veObjType type = veNodeGetObjType(ve_stmt);
  veNode ve_block;
  if(type == VE_DELAY_OR_EVENT) {
    ve_block = veDelOrEventControlGetStatement(ve_stmt);
    type = veNodeGetObjType(ve_block);
  } else {
    ve_block = ve_stmt;
  }

  if ( phase == ePre ) {
    if((type == VE_SEQ_BLOCK) || (type == VE_PAR_BLOCK)) {

      UtString block_name;
      bool has_name = mTicProtectedNameMgr->getVisibleName(ve_block, &block_name);
      if (has_name) {
        nestedBlockPath << block_name << ".";
      }
    }
    mBlockNameStack.push(nestedBlockPath);
  } else {
    mBlockNameStack.pop();
  }
      
  return eNormal;
}


InterraDesignWalkerCB::Status 
InLineDirectivesCB::net(Phase phase, mvvNode node, mvvLanguageType langType)
{
  InterraDesignWalkerCB::Status walk_status = eNormal;

  if (phase == ePre)
  {
    if (langType == MVV_VERILOG) {

      walk_status = processVerilogNetLikeObject(node);
    }
  }
  return walk_status;
}

InterraDesignWalkerCB::Status 
InLineDirectivesCB::localNet(Phase phase, mvvNode node, mvvLanguageType langType)
{
  InterraDesignWalkerCB::Status walk_status = eNormal;
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG) {
      walk_status = processVerilogNetLikeObject(node);
    }
  }
  
  return walk_status;
}

InterraDesignWalkerCB::Status 
InLineDirectivesCB::variable(Phase phase, mvvNode node, mvvLanguageType langType)
{
  InterraDesignWalkerCB::Status walk_status = eNormal;
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG) {
      walk_status = processVerilogNetLikeObject(node);
    }
  }
  return walk_status;
}


mvvNode InLineDirectivesCB::substituteEntity(mvvNode entityDecl,
                                             mvvLanguageType entityLanguage,
                                             const UtString& entityDeclName,
                                             mvvNode instance,
                                             bool isUDP, 
                                             mvvLanguageType callingLanguage)
{
  NUPortMap* portMap = NULL;
  bool found = false;
  UtString new_mod_name;

  
  
  UtString instanceName;
  VHDLCaseT vhdlCase = mCarbonContext->getJaguarContext()->getCase();
  
  Populate::composeInstanceName(instance, vhdlCase, &instanceName, mTicProtectedNameMgr);


  INFO_ASSERT(!mBlockNameStack.empty(),"Module name stack inconsistency.");
  UtString parentName = mModuleNameStack.top();

  mvvNode node = getSubstituteEntity(parentName.c_str(),
				     entityDecl,
				     entityLanguage,
				     entityDeclName,
				     instanceName,
				     isUDP,
				     callingLanguage,
				     mTicProtectedNameMgr,
                                     mIODB,
				     &new_mod_name,
				     &found,
				     &portMap);

  return node;
}
