// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/VerilogPopulate.h"
#include "LFContext.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "util/CbuildMsgContext.h"


class ReplaceTempClocks : public NuToNuFn {
public:
  ReplaceTempClocks(LFContext* lf) :
    mLFContext(lf)
  {}

  NUExpr* operator()(NUExpr* expr, Phase phase) {
    if (isPre(phase)) {
      NUNet* temp_clock = mLFContext->getEdgeNet(expr);
      if (temp_clock != NULL) {
        NUExpr* rval = new NUIdentRvalue(temp_clock, expr->getLoc());
        rval->resize(1);
        delete expr;
        return rval;
      }
    }
    return NULL;
  }

  // I thought about using replaceLeaves on the entire block, but it's
  // too aggressive.  I only want to replace uses in if-conditions
  // when those if-conditions are reached before any other statements
  // are reached, except higher level if-statements
  void walkFirstStmt(NUStmtLoop stmtLoop) {
    if (stmtLoop.atEnd()) {
      return;
    }

    // Only consider the first statement in the stmt loop
    NUIf* if_stmt = dynamic_cast<NUIf*>(*stmtLoop);
    if (if_stmt != NULL) {
      // hack the conditional
      NUExpr* cond = if_stmt->getCond();
      NUExpr* new_cond = cond->translate(*this);
      if (new_cond != NULL) {
        if_stmt->replaceCond(new_cond);
      }

      // recursively traverse the then/else branches
      walkFirstStmt(if_stmt->loopThen());
      walkFirstStmt(if_stmt->loopElse());
    }
  }

private:
  LFContext* mLFContext;
};

Populate::ErrorCode VerilogPopulate::alwaysBlock(veNode ve_always,
                                                 LFContext *context,
                                                 NUAlwaysBlock **the_always,
                                                 NUExprLocList* expr_loc_list)
{
  ErrorCode err_code = eSuccess;
  *the_always = 0;

  SourceLocator loc = locator(ve_always);

  veNode ve_stmt = veAlwaysGetStatement(ve_always);

  NUEdgeExprList edge_expr_list;
  context->pushEdgeExprList(&edge_expr_list);
  context->pushEventExprLocList(expr_loc_list);
  context->clearRequiresSensitivityEvents();

  // The isWildCard is true, when the always block uses 
  // wild card in the sensitivity lst, like this
  // always&(*)
  bool isWildCard = false;
  NUBlock * block = 0;
  ErrorCode temp_err_code = structuredProcBlock(ve_stmt, context, &block, &isWildCard);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    // does this not incorrectly leave the edgeExprList on the stack?  (rjc)
    return temp_err_code;
  }

  context->popEdgeExprList();
  context->popEventExprLocList();

  StringAtom * always_name = context->getModule()->newBlockName(context->getStringCache(), loc);
  *the_always = new NUAlwaysBlock(always_name, block, 
                                  context->getNetRefFactory(), loc, isWildCard);

  // If it has an edge expression list it is sequential. Otherwise it
  // is combinational.
  bool has_edge_exprs = !edge_expr_list.empty();
  bool remove_expr_loc_list = false;
  if (has_edge_exprs) {
    // Populate the edge expressions for the sequential block
    for (NUEdgeExprListIter expr_iter = edge_expr_list.begin();
         expr_iter != edge_expr_list.end();
         expr_iter++) {
      NUEdgeExpr * edge_expr = (*expr_iter);
      edge_expr->getNet()->putIsEdgeTrigger(true);
      (*the_always)->addEdgeExpr(edge_expr);
    }
    remove_expr_loc_list = true;
    if(expr_loc_list->empty() == false)
    {
      for (NUExprLocListIter expr_iter = expr_loc_list->begin();
           expr_iter != expr_loc_list->end(); expr_iter++) 
      {
        NUExprLocPair expr_loc = *expr_iter;
        NUExpr* expr = expr_loc.first;
        delete expr;
      }
      mMsgContext->UnsuppMixedEvent(&loc);
    }

    // bug6225.  Walk the if-statement conditions, substituting any
    // temp-clocks we have created, so that reset-expressions use the
    // same temp in the edge condition as they do in the condition.
    ReplaceTempClocks replace_temp_clocks(context);
    replace_temp_clocks.walkFirstStmt(block->loopStmts());
  } else {
    // It is a combinational block.
    if (!context->getRequiresSensitivityEvents()) 
    {
      if(expr_loc_list->empty() == false)
        remove_expr_loc_list = true;

      if(context->getSynthSwitch() && (has_edge_exprs == false))
      {
        // Save the expressions for synth check.
        for (NUExprLocListIter expr_iter = expr_loc_list->begin();
             expr_iter != expr_loc_list->end(); expr_iter++) 
        {
          NUExprLocPair expr_loc = *expr_iter;
          (*the_always)->addLevelExpr(expr_loc.first);
        }
        (*the_always)->setLevelExprList(true);
      }
      else
      {
        // remove the expressions
        for (NUExprLocListIter expr_iter = expr_loc_list->begin();
             expr_iter != expr_loc_list->end(); expr_iter++) 
        {
          NUExprLocPair expr_loc = *expr_iter;
          NUExpr* expr = expr_loc.first;
          delete expr;
        }
      }
    }
  }

  if(remove_expr_loc_list)
    expr_loc_list->clear();

  return err_code;
}


Populate::ErrorCode VerilogPopulate::initialBlock(veNode ve_initial,
                                                  LFContext* context,
                                                  NUInitialBlock **the_block)
{
  ErrorCode err_code = eSuccess;
  *the_block = 0;

  veNode ve_stmt = veInitialGetStatement(ve_initial);

  NUBlock * block = 0;
  ErrorCode temp_err_code = structuredProcBlock(ve_stmt, context, &block);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  SourceLocator loc = locator(ve_initial);

  StringAtom * initial_name = context->getModule()->newBlockName(context->getStringCache(), loc);
  *the_block = new NUInitialBlock(initial_name, block, 
                                  context->getNetRefFactory(), loc);

  return err_code;
}


/*
 * Always and Initial blocks have a block as the statement container, so manage
 * the creation of that block.
 * The statement walk will create a block if it finds a block in the walk, so
 * just need to detect the case where the statement inside the block is not a begin/end
 * block statement.
 */
Populate::ErrorCode VerilogPopulate::structuredProcBlock(
    veNode ve_stmt,
    LFContext *context,
    NUBlock **the_block,
    bool *isWildCard)
{
  ErrorCode err_code = eSuccess;

  // Create a block.
  SourceLocator loc = locator(ve_stmt);

  // Find the ve_block, which may be nested inside a delay/event
  // statement.  We need this for ve_block->NUBlock mapping, which
  // is needed for disable statements.

  (*the_block) = new NUBlock(NULL,
                             context->getModule(),
                             mIODB,
                             context->getNetRefFactory(),
                             false,
                             loc);
  veNode ve_block = ve_stmt;
  if (veNodeGetObjType(ve_block) == VE_DELAY_OR_EVENT) {
    ve_block = veDelOrEventControlGetStatement(ve_stmt);
  }

  mBlockMap[ve_block] = *the_block;

  context->pushBlockScope(*the_block);

  NUStmtList stmt_list;
  context->pushStmtList(&stmt_list);

  // Recursively handle its contents.
  ErrorCode temp_err_code = stmt(ve_stmt, context, *the_block, isWildCard);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  context->popStmtList();

  (*the_block)->addStmts(&stmt_list);
  context->popBlockScope();

  return err_code;
}


