// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "Reset.h"
#include "localflow/UD.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUIf.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "util/SetOps.h"
#include "reduce/Fold.h"

/*!
  \file
  Implementation of asynchronous reset detection and rewriting.
*/


Reset::Reset(AtomicCache *str_cache,
	     NUNetRefFactory *netref_factory,
	     MsgContext *msg_ctx,
	     IODBNucleus *iodb,
             ArgProc* args,
	     bool verbose,
	     bool enableSyncReset,
             bool enableAsyncReset,
             Fold* fold) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mBDDContext(),
  mVerbose(verbose),
  mEnableSyncReset(enableSyncReset),
  mEnableAsyncReset(enableAsyncReset),
  mIODB( iodb ),
  mFold( fold )
{
  mUD = new UD(mNetRefFactory, mMsgContext, iodb, args, mVerbose);
}


Reset::~Reset()
{
  delete mUD;
}


void Reset::module(NUModule *this_module)
{
  NUEdgeExprSet directiveEdges;
  if (this_module->hasAsyncResetNets()) {
    findDirectiveEdges(this_module, &directiveEdges);
  }

  NUAlwaysBlockList always_blocks;
  this_module->getAlwaysBlocks(&always_blocks);
  NUAlwaysBlockList accumulator_list;
  bool did_rewrite = false;
  bool module_needs_new_ud = false; // if this gets set to true then we will recalc after new always blocks are installed
  for (NUAlwaysBlockListIter iter = always_blocks.begin();
       iter != always_blocks.end();
       ++iter) {
    did_rewrite |= alwaysBlock(this_module, *iter, &accumulator_list,
                               &module_needs_new_ud, directiveEdges);
  }

  if (did_rewrite) {
    // replace all of the always blocks with the accumulated list
    this_module->replaceAlwaysBlocks(accumulator_list);

    if ( module_needs_new_ud ) {
      mUD->module(this_module);
      SourceLocator loc(this_module->getLoc());
      mMsgContext->LocalsMovedToModuleLevel(&loc);
    }
  }

  cleanDirectiveEdges(&directiveEdges);
}


NUStmt* Reset::createResetTree(CopyContext &copy_context,
			       IfEdgeList::iterator if_edge_iter,
			       IfEdgeList &if_edge_list)
{
  if (if_edge_iter == if_edge_list.end()) {
    return 0;
  }

  NUIf *if_stmt = if_edge_iter->first;

  // Copy the body.
  NUStmtList then_body;
  for (NUStmtLoop then_loop = if_stmt->loopThen(); not then_loop.atEnd(); ++then_loop) {
    then_body.push_back((*then_loop)->copy(copy_context));
  }

  // Recursively copy the other lower priority conditions
  NUStmtList else_body;
  IfEdgeList::iterator next_if_edge_iter = if_edge_iter;
  ++next_if_edge_iter;
  NUStmt *else_body_stmt = createResetTree(copy_context, next_if_edge_iter, if_edge_list);
  if (else_body_stmt) {
    else_body.push_back(else_body_stmt);
  }

  // Create if stmt which checks for condition; this will be the body of the block.
  NUIf *new_if = new NUIf(if_stmt->getCond()->copy(copy_context),
			  then_body,
			  else_body,
			  mNetRefFactory,
                          if_stmt->getUsesCFNet(),
			  if_stmt->getLoc());

  return new_if;
}


NUAlwaysBlock *Reset::createFunkyResetBlock(NUModule *module,
                                            IfEdgeList::iterator if_edge_iter,
                                            IfEdgeList &if_edge_list,
                                            NUAlwaysBlock *prev_always,
                                            NUAlwaysBlock *prio_always,
                                            NUAlwaysBlock *clock_always,
                                            SourceLocator loc)
{
  NUBlock *new_block = new NUBlock(NULL, 
                                   module,
                                   mIODB,
                                   mNetRefFactory,
                                   false,
                                   loc);

  CopyContext copy_context(new_block, mStrCache);
  NUStmt *body_stmt = createResetTree(copy_context, if_edge_iter, if_edge_list);
  NU_ASSERT(body_stmt,prev_always);
  new_block->addStmt(body_stmt);

  StringAtom * name = module->newBlockName(mStrCache, loc);
  NUAlwaysBlock *new_always = new NUAlwaysBlock(name, new_block, mNetRefFactory, loc, false);

  NUEdgeExpr *opposite_edge = dynamic_cast<NUEdgeExpr*>(prev_always->getEdgeExpr()->copyOpposite());
  NU_ASSERT(opposite_edge, prev_always->getEdgeExpr());
  new_always->addEdgeExpr(opposite_edge);

  if (prio_always != 0) {
    new_always->setPriorityBlock(prio_always);
  }

  // Point back to the main clock block
  new_always->setClockBlock(clock_always);

  return new_always;
}


bool Reset::alwaysBlock(NUModule *module,
                        NUAlwaysBlock *block,
                        NUAlwaysBlockList *always_block_accumulator_list,
                        bool* module_needs_new_ud,
                        const NUEdgeExprSet& directiveEdges)
{
  NUBlock *orig_block = block->getBlock();
  NUEdgeExprList edge_list;
  block->getEdgeExprList(&edge_list);
  NUEdgeExprSet edge_set;
  edge_set.insert(edge_list.begin(), edge_list.end());

  // Associate the edges with if stmts.
  NUStmtLoop stmt_loop = orig_block->loopStmts();
  NUStmtList stmts(stmt_loop.begin(), stmt_loop.end());
  IfEdgeList if_edge_list;

  // If the always-block is empty, remove it from the module, by
  // returning true without inserting it into the accumulator list.
  // This causes the warning 'empty always block' to be printed.
  if (stmts.empty()) {
    mMsgContext->EmptyAlwaysBlock(block);
    delete block;
    return true;
  }

  // If 0 edges, then nothing to do.
  if (edge_set.empty()) {
    always_block_accumulator_list->push_back(block);
    return false;
  }
  
  bool ok = stmtList(block, &stmts, &edge_set, directiveEdges, &if_edge_list);

  if (not ok) {
    always_block_accumulator_list->push_back(block);
    return false;
  }

  // Make sure 1 edge left, which will be the clock.
  NU_ASSERT(not edge_set.empty(),block);
  NU_ASSERT(edge_set.size() == 1,block);

  NUEdgeExpr *mainEdge = *(edge_set.begin());
  mainEdge->setIsClock();
  
  // Go through the if-edge list, creating new always blocks for each.
  SourceLocator loc(block->getLoc());
  NUAlwaysBlockList block_list;
  NUAlwaysBlock *prio_block = 0;
  NUAlwaysBlock *prev_prio_block = 0;
  bool did_rewrite = false;
  for (IfEdgeList::iterator iter = if_edge_list.begin();
       iter != if_edge_list.end();
       ++iter) {
    NUIf *if_stmt = iter->first;
    EdgeBool& edgeBool = iter->second;
    NUEdgeExpr *edge = edgeBool.first;
    bool fromAlways = edgeBool.second;

    // If we are transforming async resets to sync resets, then
    // just remove the edge condition from the original block,
    // but don't mutate the if-statements.
    if (! mEnableAsyncReset || module->isNoAsyncResets()) {
      if (fromAlways) {
        block->removeEdgeExpr( edge );
        delete edge;
      }
      else {
        // The user has explicitly requested that async resets be
        // turned off in this module, and has also explicitly requested that a
        // specific reset be treated as asynchronous. I can't compute!
        NUNet* net = edge->getNet();
        ClockEdge clockEdge = edge->getEdge();
        const char* edgeStr = (clockEdge == eClockPosedge) ?
          "posedge" : "negedge";
        UtString buf;
        net->compose(&buf, NULL);
        mMsgContext->asyncResetConflict(block, buf.c_str(), edgeStr);
      }
      continue;
    }

    // If there is a current priority block, then we have multiple set/reset edges;
    // create a block to handle funky cases (bug 238).
    IfEdgeList::iterator next_iter = iter;
    ++next_iter;
    if (prio_block != 0) {
      NUAlwaysBlock *reset_block =
	createFunkyResetBlock(module, iter, if_edge_list, prio_block, prev_prio_block, block, loc);
      mUD->structuredProc(reset_block);
      block_list.push_back(reset_block);
      always_block_accumulator_list->push_back(reset_block);
    }

    // The stmts in the then branch become the body of the new always block.
    NUStmtList *body = if_stmt->getThen();
    if (body->empty()) {
      mMsgContext->AResetNoResetCode(block);
    }

    if (!body->empty())
      loc = (*(body->begin()))->getLoc();
    NUBlock *new_block =
      new NUBlock(body, 
                  module,
                  mIODB,
                  mNetRefFactory,
                  if_stmt->getUsesCFNet(),
                  loc);
    delete body;
    // After moving statements the child's parent scope pointers need updating.
    // test/syncreset/named.v
    new_block->adoptChildren();

    StringAtom * name = module->newBlockName(mStrCache, loc);
    NUAlwaysBlock *new_always = new NUAlwaysBlock(name, new_block, mNetRefFactory, loc, false);
    // For asynchronous resets, the reset block triggers on the edge of reset.
    if (fromAlways) {
      block->removeEdgeExpr( edge );
    }
    else {
      CopyContext cc(NULL, NULL);
      edge = edge->copyEdgeExpr(cc);
    }
    new_always->addEdgeExpr( edge );
    // Point back to the main clock block
    new_always->setClockBlock(block);

    if (prio_block != 0) {
      new_always->setPriorityBlock(prio_block);
    }
    edge->setIsReset();

    // The stmts in the else branch replace the if statement.
    body = if_stmt->getElse();
    orig_block->replaceStmt(if_stmt, body);
    if (!body->empty())
    {
      const SourceLocator& elseLoc = (*(body->begin()))->getLoc();
      orig_block->putLoc(elseLoc);
      block->putLoc(elseLoc);
    }
    delete body;

    // Clear out the then and else branches so don't delete the statements in those
    // branches.
    NUStmtList empty;
    if_stmt->replaceThen(empty);
    if_stmt->replaceElse(empty);
    delete if_stmt;

    if (mVerbose) {
      UtIO::cout() << "Async Reset created new always block" << UtIO::endl;
      new_always->print(true,2);
    }

    did_rewrite = true;
    prev_prio_block = prio_block;
    prio_block = new_always;

    mUD->structuredProc(new_always);
    block_list.push_back(new_always);
    always_block_accumulator_list->push_back(new_always);
  }
  
  // After moving statements the child's parent scope pointers need updating.
  // test/syncreset/named.v
  orig_block->adoptChildren();

  if( (prio_block != 0)) {
    block->setPriorityBlock( prio_block );
      
    if (mVerbose) {
      UtIO::cout() << "Async Reset last always block" << UtIO::endl;
      block->print(true,2);
    }
  }
  
  
  mUD->structuredProc(block);
  block_list.push_back(block);
  always_block_accumulator_list->push_back(block);

  if ( did_rewrite ){
    reDistributeLocals(module, &block_list, module_needs_new_ud);
  }
  
  return did_rewrite;
}

/*! Assumptions:
 * \a block_list contains a list of alwaysblocks from \a module.
 * Some action has taken place that has moved assignments from one of
 * these blocks to another (such as task inline during async reset splitting)
 * 
 * Goal:
 * move the net declarations for local nets to the proper block, or to
 * the module level if there is no single block that is correct.
 * 
 * A net declaration should be in the same block where a net is
 * written/or read, unless it is written/read from multiple blocks and
 * then the proper location is at the containing module level.
 *
 * take the local nets from all the blocks in block_list and
 * redistribute each to the block that actually defs that net.  Nets that
 * are def'ed by more than one block are moved to the module level.
 * \a module_needs_new_ud will be set to true when a new net is added
 * to the module, if this is true on entry or found to be true in this
 * call then UD is not recalculated for items from block_list and it
 * is expected that UD will be calculated on whole module when it is
 * reassembled.
 */
void Reset::reDistributeLocals(NUModule *module,
                               NUAlwaysBlockList *block_list,
                               bool* module_needs_new_ud)
{
  NUNetSet multi_block_refs;   // nets that are def'ed in multiple blocks
  NUNetSet all_refs_seen;      // a running tally of nets that have been referenced in the blocks already examined
  NUNetSet all_local_nets;     // nets that are to be distributed at the local level

  for (NUAlwaysBlockListIter iter = block_list->begin();
       iter != block_list->end();
       ++iter) {
    NUAlwaysBlock* always_block = *iter;
    NUBlock* block = always_block->getBlock();
    NUNetList block_local_nets;

    // identify all local nets, they are the only ones that might need to be moved.

    // convert list of local nets into a netSet
    block->getAllNets(&block_local_nets);
    all_local_nets.insert(block_local_nets.begin(),block_local_nets.end());
    // get the use and def set for each always block and look for
    // any nets that are referenced in multiple blocks they need to be declared
    // at the module level.
    NUNetSet block_refs;      // nets referenced in this block
    NUNetSet new_module_defs; // newly discovered nets that need a module def

    block->getBlockingDefs(&block_refs);
    block->getBlockingUses(&block_refs);
    block->getNonBlockingUses(&block_refs);
    block->getNonBlockingDefs(&block_refs);


    set_intersection(block_refs, all_refs_seen, &new_module_defs);

    // any net that is ref'ed in multiple blocks needs to be moved to module level
    multi_block_refs.insert(new_module_defs.begin(), new_module_defs.end());

    all_refs_seen.insert(block_refs.begin(),block_refs.end());
    
  }

  // Here we once checked to see if there is any non-blocking def to one of the
  // local nets then the user has a problem because they are making a
  // non-blocking assignment to a local variable but the value of this assignment
  // will not make it out of the local block.
#if 0
  NUNetSet nonblocking_defs;  // non-blocking nets def'ed in any block
  for (NUAlwaysBlockListIter iter = block_list->begin();
       iter != block_list->end();
       ++iter) {
    NUAlwaysBlock* always_block = *iter;
    NUBlock* block = always_block->getBlock();

    block->getNonBlockingDefs(&nonblocking_defs);
  }  
  NUNetSet local_nonblocking_defed_nets;  // nets that were local, but were def'ed by a non-blocking assign
  set_intersection(all_local_nets, nonblocking_defs, &local_nonblocking_defed_nets);
  for (NUNetSet::iterator iter = local_nonblocking_defed_nets.begin(); iter != local_nonblocking_defed_nets.end(); ++iter) {
    NUNet* net = *iter;
    mMsgContext->NonBlockingAssignToLocalVar(net);
  }
#endif
  
  if ( all_local_nets.empty() ){
    return;                     // nothing to do
  }

  {
    // I am not sure that this block of code is necessary. Currently we have no testcase that requires it

    NUNetSet module_nets;  // nets that are to be moved to module level are are no longer in all_local_nets
    set_intersection(all_local_nets, multi_block_refs, &module_nets);
    set_subtract(module_nets, &all_local_nets); 

    // place the module level nets at module level
    NUScope* module_scope = module;
    for (NUNetSet::iterator iter = module_nets.begin(); iter != module_nets.end(); ++iter) {
      NUNet* net = *iter;
      NUScope * old_scope = net->getScope();
      NU_ASSERT (( old_scope != module_scope ), net); 

      old_scope->disconnectLocal(net);
      net->replaceScope(old_scope, module_scope);
      module->addLocal(net);
      net->putIsNonStatic(false); // net is no longer non-static
      (*module_needs_new_ud) = true;
    }
  }

  
  for (NUAlwaysBlockListIter iter = block_list->begin();
       ((not all_local_nets.empty()) and (iter != block_list->end()));
       ++iter) {
    NUAlwaysBlock* always_block = *iter;
    NUBlock* block = always_block->getBlock();
    NUScope* block_scope = block;

    NUNetSet block_refs;        // nets that are referenced in the current block
    block->getBlockingDefs(&block_refs);
    block->getBlockingUses(&block_refs);
    block->getNonBlockingDefs(&block_refs);
    block->getNonBlockingUses(&block_refs);

    // for each ref, if the net is still in all_local_nets then remove from that
    // list and make sure the net is local to the current block.
    NUNetSet nets_for_this_block;

    // select the nets for this block, and remove from the set to be distributed
    set_intersection(all_local_nets, block_refs, &nets_for_this_block);
    set_subtract(nets_for_this_block, &all_local_nets);

    bool this_block_needs_ud = false;
    for (NUNetSet::iterator iter = nets_for_this_block.begin();
         iter != nets_for_this_block.end();
         ++iter) {
      NUNet* net = *iter;
      NUScope * old_scope = net->getScope();

      // only need to move net if it is not already in correct block
      if ( old_scope != block_scope ) {
        old_scope->disconnectLocal(net);
        block->addLocal(net);
        net->replaceScope(old_scope, block_scope);
        this_block_needs_ud = true;
      }
    }

    // if caller will update ud for whole module then no need to do it here
    if ( ( not *module_needs_new_ud ) and ( this_block_needs_ud ) ) {
      mUD->structuredProc(always_block);
    }
  }

  // nets that remain  in all_local_nets can just stay where they are, they
  // were not referenced and dead code will remove them later.

}


bool Reset::stmtList(NUAlwaysBlock *block,
                     NUStmtList *stmts,
                     NUEdgeExprSet *edge_set,
                     const NUEdgeExprSet& directive_edges,
                     IfEdgeList *if_edge_list)
{
  UInt32 num_required_edges = edge_set->size();
  NU_ASSERT(num_required_edges != 0, block);
  NUIf* if_stmt = NULL;
  NUEdgeExpr* edge = NULL;
  bool ret = false;
  UInt32 num_stmts = stmts->size();

  if (num_stmts == 1) {
    if_stmt = dynamic_cast<NUIf*>(*(stmts->begin()));
  }

  if (if_stmt != NULL) {
    // If we are out of explicitly specified edges (only the 1 clock
    // remains), then see if any of edges specified as directives
    // match the next if-statement.

    bool use_edge_from_always_block = num_required_edges > 1;
    if (use_edge_from_always_block) {
      // In this required path, failure to convert an edge to a reset is
      // also fatal

      // test/vhdl/clocking/trailingreset25 has both a posedge and negedge
      // reset.  We need to check the 'matching' edge first, before allowing a
      // flip.  Otherwise we grab the wrong edge, swap then & else blocks, and
      // then the rest of the if-statement is not structured properly to match
      // the remaining edges.  This test is sensitive to the order of edges.
      edge = matchEdge(if_stmt, *edge_set, false);
      if (edge == NULL) {
        edge = matchEdge(if_stmt, *edge_set, true);
      }

      if (edge != NULL) {
        edge_set->erase(edge);
      }
      else {
        mMsgContext->AResetCondNoMatchEdge(if_stmt);
      }
    }
    else {
      // In this optional path, the lack of a matching edge is not an
      // error or a warning.  However, I think it's a good idea if we
      // print a note saying we auto-converted a sync-reset to an async
      // reset, and which edge we found.
      ret = true;
      if (! directive_edges.empty()) {
        edge = matchEdge(if_stmt, directive_edges, false);
        if (edge != NULL) {
          NUNet* net = edge->getNet();
          ClockEdge clockEdge = edge->getEdge();
          const char* edgeStr = (clockEdge == eClockPosedge) ?
            "posedge" : "negedge";
          UtString buf;
          net->compose(&buf, NULL);
          mMsgContext->convertSyncResetToAsync(if_stmt, buf.c_str(), edgeStr);
        }
      }
    }
    if (edge != NULL) {
      // The single statement in the always block is an if statement.
      // Pick apart the if statement; line up conditionals with the
      // edge expression that we've already found
      ret = ifStmt(block, if_stmt, edge_set, directive_edges,
                   if_edge_list, edge, use_edge_from_always_block);
    }
  }
  else {
    if (num_required_edges > 1) {
      if (num_stmts == 0) {
        mMsgContext->AResetUnmatchedEdges(block); // error
      }
      else if (num_stmts > 1) {
        mMsgContext->AResetNotOnlyIfStmt(block); // error
      }
      else if (if_stmt == NULL) {
        mMsgContext->AResetNoIfStmt(block); // error
      }
    }

    else {
      if (num_stmts == 0) {
        // no conversion occurred, only 1 edge left, no always-block body left
        // for clock 
        mMsgContext->AResetNoClockCode(block); // warn
      }

      // We got to the clock-block, and so we are done with the recursion
      // and can start returning 'true' up the stack
      ret = true;
    }
  }
  return ret;
} // bool Reset::stmtList


bool Reset::ifStmt(NUAlwaysBlock *block,
		   NUIf *if_stmt,
		   NUEdgeExprSet *edge_set,
		   const NUEdgeExprSet& directive_edges,
		   IfEdgeList *if_edge_list,
                   NUEdgeExpr* edge,
                   bool from_always)
{

  if_edge_list->push_back(IfEdge(if_stmt, EdgeBool(edge, from_always)));

  NUStmtList *stmts = if_stmt->getElse();
  bool retval = stmtList(block, stmts, edge_set, directive_edges, if_edge_list);
  delete stmts;

  if (mVerbose) {
    UtIO::cout() << "Matched Edge to If conditional" << UtIO::endl;
    if_stmt->print(false,2);
    edge->print(false,2);
  }

  return retval;
}


NUEdgeExpr *Reset::matchEdge(NUIf* if_stmt, const NUEdgeExprSet& edge_set,
                             bool allow_flip)
{
  NUExpr *cond = if_stmt->getCond();

  // Do not fold the entire if-statement, as that could make it
  // impossible to recognize.  But do fold the condition.
  NUExpr* c2 = mFold->fold(cond);
  if ((c2 != cond) && (c2 != NULL)) {
    if_stmt->replaceCond(c2);
    cond = c2;
  }

  for (NUEdgeExprSet::const_iterator iter = edge_set.begin();
       iter != edge_set.end();
       ++iter) {
    NUEdgeExpr *edge = *iter;
    ClockEdge edge_type = edge->getEdge();
    NUExpr *edge_expr = edge->getExpr();
    BDD edge_bdd = mBDDContext.bdd(edge_expr);
    BDD cond_bdd;
    switch (edge_type) {
    case eLevelHigh:
    case eClockPosedge:
      cond_bdd = mBDDContext.bdd(cond);
      break;
    case eLevelLow:
    case eClockNegedge:
      cond_bdd = mBDDContext.opNot(mBDDContext.bdd(cond));
      break;
    default:
      NU_ASSERT("Unexpected edge type"==NULL,edge);
      break;
    }
    if (cond_bdd == edge_bdd) {
      return edge;
    }
    
    // If the edge is reversed, then just swap the then/else clauses.
    //
    // We do not flip the polarity for sync resets that are promoted
    // via directive, because we don't have a 'negedge' or 'posedge'
    // to determine what the correct polarity is, we only have the
    // if-statement.
    if (allow_flip && (cond_bdd == mBDDContext.opNot(edge_bdd))) {
      NUStmtList* then_stmts = if_stmt->getThen();
      NUStmtList* else_stmts = if_stmt->getElse();
      if_stmt->replaceThen(*else_stmts);
      if_stmt->replaceElse(*then_stmts);
      delete then_stmts;
      delete else_stmts;
      return edge;
    }
  }

  return 0;
}


// Return the active edge expression for a synchronous reset, if this block
// has a synchronous reset.
void
Reset::getSyncResetEdge( NUAlwaysBlock* always,
                         NUEdgeExpr **sr_edge, NUEdgeExpr **sr_edge_bar )
{
  (*sr_edge) = NULL;
  (*sr_edge_bar) = NULL;
  
  // Make sure this is an edge triggered block with only one edge.
  NUEdgeExprList edge_list;
  
  always->getEdgeExprList(&edge_list);
  if( edge_list.size() != 1 )
    return;
  
  NUBlock * block = always->getBlock();
  NUStmtLoop stmt_loop  = block->loopStmts();

  // Make sure there is only one statement.
  if (stmt_loop.atEnd()) {
    return;
  }
  NUStmt *s = (*stmt_loop);
  ++stmt_loop;
  if (not stmt_loop.atEnd()) {
    return;
  }
    
  // Make sure it is an "if" statement.
  NUIf *if_stmt = dynamic_cast<NUIf*>(s);
  if( if_stmt == NULL )
    return;

  // Check that the condition expression uses a single bit.
  NUExpr *cond = if_stmt->getCond();
  NUNetRefSet uses(mNetRefFactory);
  cond->getUses( &uses );

  if( uses.size() != 1 )
    return;
        
  NUNetRefHdl cond_ref = *(uses.begin());

  if( cond_ref->getNumBits() != 1 )
    return;

  // Check that the "then" uses are all empty.
  // The rhs for the then clause need to all be constants.
  NUNetRefSet thenUses(mNetRefFactory);
  for( NUStmtLoop sl = if_stmt->loopThen(); !sl.atEnd(); ++sl ) {
    (*sl)->getBlockingUses( &thenUses );
    (*sl)->getNonBlockingUses( &thenUses );
    if( !thenUses.empty())
      return;
  }
      
  // Create the single bit reset reference expression.
  NUNet *sr = cond_ref->getNet();
  NUExpr *sr_expr = NULL;
  const SourceLocator loc = always->getLoc();
    
  // Bitsel resets  (reset[0]) should be handled.  For now just punt.
  // We would need to create a temporary since posedge reset[0] is not legal.
  //if( sr->getBitSize() != 1) {
  //  ConstantRange r(0,0);
  //  cond_ref->getRange( r );
  //  UInt64 bit = r.getLsb();
  //  NUConst *bit_expr = NUConst::createKnownIntConst( bit, 
  //                                                    32, false, loc );
  //  sr_expr = new NUBitselRvalue( sr, bit_expr, loc );
  //}
  //else
  if( sr->getBitSize() != 1 )
    return;
  sr_expr = new NUIdentRvalue( cond_ref->getNet(), loc );
  sr_expr->resize(1);
  
  // Figure out which edge to add.
  BDD sr_bdd = mBDDContext.bdd( sr_expr );
  BDD cond_bdd = mBDDContext.bdd( cond );
  BDD not_cond_bdd = mBDDContext.opNot( mBDDContext.bdd( cond ) );
  //  Needs to simplify to posdege or negedge.  If not, we have something
  //  funky like a 'bx comparison.  In that case punt.
  //  Example: test/cust/star/ba/bcore
  if( (sr_bdd != cond_bdd) && (sr_bdd != not_cond_bdd) ) {
    delete sr_expr;
    return;
  }
  
  ClockEdge ce = ( sr_bdd == cond_bdd ? eLevelHigh : eLevelLow );
  
  (*sr_edge) = new NUEdgeExpr( ce, sr_expr, loc );
  (*sr_edge)->resize(1);
  (*sr_edge_bar) = (NUEdgeExpr *) (*sr_edge)->copyOpposite();
  
  if( mVerbose ) {
    UtIO::cout() << "Found a synchronous reset at: ";
    loc.print();
    UtIO::cout() << "\n  If statement: ";
    if_stmt->print( false, 0 );
    UtIO::cout() << "  Edge expr: ";
    (*sr_edge)->printVerilog( true );
    UtIO::cout() << "\n";
  }
}


void Reset::findDirectiveEdges(NUModule* mod,
                               NUEdgeExprSet* directiveEdges)
{
  for (NUModule::NodeSet::SortedLoop p = mod->loopAsyncResetNets();
       !p.atEnd(); ++p)
  {
    STSymbolTableNode* node = *p;
    NUNet* net = NUNet::findUnelab(node);
    if (net != NULL) {
      // Only 1-bit nets can be async resets
      if (net->getBitSize() != 1) {
        mMsgContext->LFAsyncResetOnWideNet(net);
      }
      else {
        // Add both edges to the set -- that way we don't have to burden
        // the user with two different directives
        const SourceLocator& loc = net->getLoc();
        NUExpr* id = new NUIdentRvalue(net, loc);
        directiveEdges->insert(new NUEdgeExpr(eClockPosedge, id, loc));
        id = new NUIdentRvalue(net, loc);
        id->resize(1);
        directiveEdges->insert(new NUEdgeExpr(eClockNegedge, id, loc));
      }
    }
  }
} // void Reset::findDirectiveEdges

void Reset::cleanDirectiveEdges(NUEdgeExprSet* directiveEdges) {
  for (NUEdgeExprSet::iterator p = directiveEdges->begin(),
         e = directiveEdges->end(); p != e; ++p)
  {
    NUEdgeExpr* ee = *p;
    delete ee;
  }
}
