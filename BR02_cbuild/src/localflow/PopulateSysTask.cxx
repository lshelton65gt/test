// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/VerilogPopulate.h"
#include "LFContext.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUElabHierHelper.h"
#include "util/ArgProc.h"
#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "compiler_driver/CarbonContext.h" // for CRYPT

Populate::ErrorCode VerilogPopulate::sysTask(veNode ve_systask,
                                             LFContext* context,
                                             NUStmt **the_stmt)
{
  bool use_disabled_message = false;
  ErrorCode err_code = eSuccess;
  *the_stmt = 0;

  SourceLocator loc = locator(ve_systask);

  // Get the task name, we only support a subset so far
  CheetahStr ve_string(veSysTaskEnableGetName(ve_systask));
  StringAtom* name = context->getStringCache()->intern(ve_string);
  const char* nameString = name->str();
  IODBNucleus* iodb = context->getIODB();
  StringAtom* orig_module_name = context->getModule()->getOriginalName();

  bool doingOutputSysTasks = mArg->getBoolValue ("-enableOutputSysTasks");
  if ( doingOutputSysTasks ){
    doingOutputSysTasks = (not iodb->isDisableOutputSysTasks(orig_module_name) );
  } else {
    doingOutputSysTasks = (iodb->isEnableOutputSysTasks(orig_module_name) );
  }

  if ( ( 0 == strcmp( nameString, "$readmemb") ) or
       ( 0 == strcmp( nameString, "$readmemh") )   ) {
    bool useHexFormat = ( 0 == strcmp( nameString, "$readmemh") );
    return readMemX(ve_systask, useHexFormat, context, loc, the_stmt);
  }

  if ( ( 0 == strcmp( nameString, "$display" ) ) or
       ( 0 == strcmp( nameString, "$fdisplay") ) or
       ( 0 == strcmp( nameString, "$write"   ) ) or
       ( 0 == strcmp( nameString, "$fwrite"  ) )   ) {
    if ( doingOutputSysTasks ) {
      return verilogOutputSysTask(ve_systask, context, loc, the_stmt);
    } else {
      use_disabled_message = true;
    }
  }

  if ( ( 0 == strcmp( nameString, "$fclose" ) ) or
       ( 0 == strcmp( nameString, "$fflush" ) )   ) {
    if ( doingOutputSysTasks ) {
      bool isClose = ( 0 == strcmp( nameString, "$fclose" ) );
      return verilogFCloseOrFFlushSysTask(ve_systask, isClose, context, loc, the_stmt);
    } else {
      use_disabled_message = true;
    }
  }

  if ( ( ( 0 == strcmp( nameString, "$stop" ) ) ) or
       ( ( 0 == strcmp( nameString, "$finish" ) ) ) )
  {
    return verilogControlTask(ve_systask, context, loc, the_stmt);
  }
  
  if (iodb->isCModel(nameString))
  {
    return userTask(ve_systask, name, context, loc, the_stmt);
  }

  // in all other cases we fall through to here

  if ( use_disabled_message ) {
    mMsgContext->DisabledSysTask(&loc, nameString, "-enableOutputSysTasks");
  } else {
    mMsgContext->UnsupportedSysTask(&loc, nameString);
  }

  // Return a NULL statement that our caller will ignore.
  *the_stmt = NULL;

  return err_code;
}


Populate::ErrorCode VerilogPopulate::readMemX(veNode ve_systask,
                                              bool hexFormat,
                                              LFContext* context, 
                                              const SourceLocator& loc,
                                              NUStmt **the_stmt)
{
  ErrorCode err_code = eSuccess;
  NUExprVector dummy_expr_vector; // for now a placeholder
  *the_stmt = 0;

  // Get the required arguments
  CheetahList ve_arg_list(veSysTaskEnableGetExprList(ve_systask));
  veNode ve_file_arg = veListGetNextNode(ve_arg_list);
  veNode ve_mem_arg = veListGetNextNode(ve_arg_list);

  veObjType fileType = veNodeGetObjType(ve_file_arg);
  if ( ( fileType != VE_STRING ) and ( fileType != VE_NAMEDOBJECTUSE ) ) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "First argument of a $readmem call must be filename, it is not"), &err_code);
    return err_code;
  }
  veObjType memType = veNodeGetObjType(ve_mem_arg);
  if ( ( memType != VE_NAMEDOBJECTUSE ) and ( memType != VE_SCOPEVARIABLE) ) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Second argument not a memory in $readmem call"), &err_code);
    return err_code;
  }

  // Need an lvalue for this memory, it must be just an identifier
  NUIdentLvalue* lvalue = 0;
  ErrorCode temp_err_code = identLvalue(ve_mem_arg, context, context->getModule(), &lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  NUNet* net = lvalue->getWholeIdentifier();
  NUMemoryNet* mem_net = net->getMemoryNet();
  if (not mem_net) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Non-memory net used in $readmem call"), &err_code);
    return err_code;
  }

  const UInt32 numMemDims = mem_net->getNumDims();
  if ( numMemDims != 2 ){
    // currently only readmem* is only supported for 2 dim memories
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "$readmem is only supported for memories with 1 packed and 1 unpacked dimension."), &err_code);
    return err_code;
  }

  // Get the optional arguments
  SInt64 start_addr;
  SInt64 end_addr;
  bool end_specified = false;
  veNode ve_start_addr = veListGetNextNode(ve_arg_list);
  const ConstantRange* range = mem_net->getRange(1); // change this when support is added for arrays with more dimensions
  if (ve_start_addr != NULL)
  {
    // Start address is available
    NUConst* start_addr_const = 0;
    temp_err_code = constant(ve_start_addr, &start_addr_const);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    bool ok = start_addr_const->getLL(&start_addr);
    delete start_addr_const;
    if (not ok) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad start address"), &err_code);
      return err_code;
    }

    veNode ve_end_addr = veListGetNextNode(ve_arg_list);
    if (ve_end_addr != NULL)
    {
      // End address is available
      NUConst* end_addr_const = 0;
      temp_err_code = constant(ve_end_addr, &end_addr_const);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      ok = end_addr_const->getLL(&end_addr);
      delete end_addr_const;
      if (not ok) {
	POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad end address"), &err_code);
	return err_code;
      }
      end_specified = true;
    }
    else {
      // Use the memories end address
      // TBD - range is a 32 bit constant range but I think we need 64 bits
      end_addr = range->getLsb();
    }
  }
  else
  {
    // Use the memories start and end address
    // TBD - range is a 32 bit constant range but I think we need 64 bits
    start_addr = range->getMsb();
    end_addr = range->getLsb();
  }

  // No more arguments
  if (not (veListGetNextNode(ve_arg_list) == NULL)) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Too many arguments"), &err_code);
  }

  // And the file name
  StringAtom* file;
  ParamValue paramBuf;
  {
    temp_err_code = getParamValueString(&paramBuf, ve_file_arg);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    if (paramBuf.getType() != VE_STRING) {
      POPULATE_FAILURE(mMsgContext->LFMemXFileNotString(&loc), &err_code);
      return err_code;
    }
  }
  
  UtString::size_type idx;
  UtString& buf = paramBuf.getStr();
  while ((idx = buf.find('"')) != UtString::npos)
    buf.erase(idx, 1);

  // Create the atom
  file = context->getStringCache()->intern(buf.c_str());
  
  NUModule* module = context->getModule();
  StringAtom* task_name = module->gensym("readmemx");
  *the_stmt = new NUReadmemX(task_name, dummy_expr_vector, module,
                             lvalue, file, hexFormat, start_addr,
			     end_addr, end_specified, mNetRefFactory, false, loc);

  return err_code;
}


//! currently handles: $display, $fdisplay, $write, $fwrite
Populate::ErrorCode VerilogPopulate::verilogOutputSysTask(veNode ve_systask,
                                                          LFContext* context, 
                                                          const SourceLocator& loc,
                                                          NUStmt **the_stmt)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  *the_stmt = 0;

  CheetahList ve_arg_list(veSysTaskEnableGetExprList(ve_systask));
  UInt32 number_args = veListGetSize(ve_arg_list);
  UInt32 remaining_args = number_args; // we need to track the args
                                       // since some may be null
  
  // Get the task name, we only support a subset so far
  CheetahStr ve_string(veSysTaskEnableGetName(ve_systask));
  StringAtom* name = context->getStringCache()->intern(ve_string);
  const char* nameString = name->str();

  bool isDisplay = ( 0 == strcmp( nameString, "$display" ) );
  bool isFDisplay = ( 0 == strcmp( nameString, "$fdisplay" ) );
  bool isWrite = ( 0 == strcmp( nameString, "$write" ) );
  bool isFWrite = ( 0 == strcmp( nameString, "$fwrite" ) );
  bool hasFileArg = ( isFDisplay || isFWrite );
  bool addNewline = ( isDisplay || isFDisplay);
  bool canHaveInstName = ( isDisplay || isFDisplay || isWrite || isFWrite );

  NUExprVector expr_vector;
  NUModule* module = context->getModule();


  // if a file arg is required then make sure the first arg satisifies
  // the requirements that it not be null and it is 32 bits.
  bool argMustBeFD = hasFileArg; // note this variable is unconditionally set to
                                 // false for all but the first argument.

  // Assume there is no %m. We search for it in the expressions below.
  bool hasInstName = false;
  STBranchNode* instanceWhere = NULL;

  // we do not use exprVec to convert arguments because it cannot
  // handle null arguments, instead we process the arguments by position.
  veNode ve_next_arg;
  while ( (remaining_args > 0)) {
    ve_next_arg = veListGetNextNode(ve_arg_list);
    remaining_args--;

    NUExpr* rvalue = NULL;

    if (ve_next_arg == NULL)
    {
      if ( argMustBeFD ) {
        POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc,"The first argument of $fdisplay or $fwrite must not be null."), &err_code);
        return err_code;
      }

      rvalue = new NUNullExpr(loc);      // null expression found in list
    }
    else
    {
      // except for file descriptors(which must be 32 bits),
      // expressions in the argument list are self determined.
      // However for some hierarchical refs veExprEvaluateWidth
      // returns the wrong value but at those times isLocalSize is
      // true. 
      int lWidth = ( argMustBeFD ? 32 : veExprEvaluateWidth(ve_next_arg));
      bool isLocalSize = veExprIsSizeLocallyStatic(ve_next_arg) == VE_TRUE;
      temp_err_code = expr(ve_next_arg, context, module, lWidth, &rvalue);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        expr_vector.clear();
        return temp_err_code;
      }
      int selfDeterminedWidth = rvalue->determineBitSize();
      if ( argMustBeFD ){
        if ( selfDeterminedWidth != 32 ) {
          // warn user 
          mMsgContext->MCDShouldBe32(&loc, (number_args - remaining_args),
                                     ((selfDeterminedWidth>32)?"truncated":"extended"),
                                     selfDeterminedWidth);
        }
        // always resize to 32 bits
        rvalue = rvalue->makeSizeExplicit(32);
      } else if (isLocalSize or ( lWidth == selfDeterminedWidth )) {
        rvalue->resize(selfDeterminedWidth);
      } else {
        POPULATE_FAILURE(mMsgContext->ExpressionSizeInconsistency(&loc, (number_args - remaining_args),
                                                                  lWidth, selfDeterminedWidth), &err_code);
        expr_vector.clear();
        return err_code;
      }
    }
    expr_vector.push_back(rvalue);

    // Check if we have a %<digits>m in a format string for display and its
    // variants. We don't need to check for it if we already found
    // one.
    if (canHaveInstName && !hasInstName) {
      NUConst* constExpr =  rvalue->castConstNoXZ ();
      if (constExpr != NULL
          && constExpr->isDefinedByString()) {
        UtString formatstring;
        // the following uses composeNoProtected since we need the actual
        // string even if it was `protected
        constExpr->composeNoProtected(&formatstring, NULL, false, false);
        UtString::size_type nextPercentPos=0;
        while((nextPercentPos = formatstring.find ("%", nextPercentPos)) != UtString::npos) {
          // At least one format effector...
          // skip over all the decorations...
          UtString::size_type formatPos = formatstring.find_first_of ("%bcdefghlmostx",
                                                                          nextPercentPos+1);
          if (formatPos == UtString::npos)
            break;              // no  more formats found in string
          if (formatstring[formatPos] == 'm') {
            hasInstName = true;
            break;              // we've seen enough...
          }

          // Start looking after the last format char
          nextPercentPos = formatPos+1;
        }

        NUScope* declaration_scope = context->getDeclarationScope();
        if ( module != declaration_scope ){
          // we are within is a named declaration scope, so need to
          // save the path to current $display
          instanceWhere = NUElabHierHelper::findUnelabBranchForScope(module,declaration_scope);
        }
      }
    }

    argMustBeFD = false;        // only the first arg ever needs to be a FD
  }


  // Create the atom
  
  StringAtom* task_name = module->gensym(nameString); // or should we use "outputSysTask"

  // The statement must remember its original time unit, in case it gets
  // flattened later on down the road
  *the_stmt = new NUOutputSysTask(task_name, expr_vector, module,
                                  hasFileArg, addNewline,
                                  module->getTimeUnit(),
                                  module->getTimePrecision(),
                                  mNetRefFactory, false,
                                  true /*isVerilogtask*/, false,
                                  hasInstName, instanceWhere, loc);

  // and tell the current always block that it needs to be guarded by
  // change logic (if it is not already guarded by an edge) so that we
  // do not execute this $display stmt too many times. bug 4122
  context->putRequiresSensitivityEvents(LFContext::eOutSysTask);
  return err_code;
}


//! currently handles: $fclose/$fflush
Populate::ErrorCode VerilogPopulate::verilogFCloseOrFFlushSysTask(veNode ve_systask,
                                                                  bool isClose,
                                                                  LFContext* context, 
                                                                  const SourceLocator& loc,
                                                                  NUStmt **the_stmt)
{
  ErrorCode err_code = eSuccess;
  *the_stmt = 0;

  CheetahList ve_arg_list(veSysTaskEnableGetExprList(ve_systask));

  // Get the task name, we only support a subset so far
  CheetahStr ve_string(veSysTaskEnableGetName(ve_systask));
  StringAtom* name = context->getStringCache()->intern(ve_string);
  const char* nameString = name->str();
  NUModule* module = context->getModule();

  int lWidth = 32;            // the width of file descriptors are always 32
  NUExprVector expr_vector;
  ErrorCode temp_err_code = exprVec(ve_arg_list, context, module, lWidth, &expr_vector);

  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return err_code;
  }


  // Create the atom
  StringAtom* task_name = module->gensym(nameString); // or should we use "FCloseSysTask | FFlushSysTask"

  UtString errmsg;
  bool isvalid = false;
  if ( isClose ) {
    NUFCloseSysTask *fclose = new NUFCloseSysTask(task_name, expr_vector, 
                                                  module, mNetRefFactory,
                                                  false, true /*isVerilogTask*/,
                                                  false/*useInFileSystem*/,
                                                  loc);
    *the_stmt = fclose;
    isvalid = fclose->isValid(&errmsg);
  } else {
    NUFFlushSysTask *fflush = new NUFFlushSysTask(task_name, expr_vector, module, mNetRefFactory, false, loc);
    *the_stmt = fflush;
    isvalid = fflush->isValid(&errmsg);
  }

  if ( not isvalid )
  {
    delete (*the_stmt);
    *the_stmt = NULL;
    POPULATE_FAILURE(mMsgContext->InvalidSysFuncArg(&loc, nameString, errmsg.c_str()), &err_code);
  }

  return err_code;
}

//! only handles: $stop and $finish
Populate::ErrorCode VerilogPopulate::verilogControlTask(veNode ve_systask,
                                                        LFContext* context, 
                                                        const SourceLocator& loc,
                                                        NUStmt **the_stmt)
{
  ErrorCode err_code = eSuccess;
  *the_stmt = 0;

  CheetahList ve_arg_list(veSysTaskEnableGetExprList(ve_systask));

  // Get the task name, we only support a subset so far
  CheetahStr ve_string(veSysTaskEnableGetName(ve_systask));
  StringAtom* name = context->getStringCache()->intern(ve_string);
  const char* nameString = name->str();

  CarbonControlType controlType = eCarbonStop;
  if ( 0 == strcmp( nameString, "$stop") ) {
    controlType = eCarbonStop;
  }
  else if ( 0 == strcmp( nameString, "$finish") ) {
    controlType = eCarbonFinish;
  }
  else {
    LOC_ASSERT("Unknown control type" == 0, loc);
  }

  NUExprVector expr_vector;
  NUModule* module = context->getModule();

  int lWidth = 32;            // the width of the verbosity item should be 32 bits
  ErrorCode temp_err_code = exprVec(ve_arg_list, context, module, lWidth, &expr_vector);

  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    return err_code;
  }

  if ( expr_vector.empty() ) {
    // no arg defined, create a constant 1 as the default 
    NUExpr* verboseExpr = NUConst::create(false, 1,  32, loc);
    expr_vector.push_back(verboseExpr);
  }

  // Create the atom
  StringAtom* task_name = module->gensym(nameString); // or should we use "ControlSysTask"

  *the_stmt = new NUControlSysTask(task_name, controlType, expr_vector, module, mNetRefFactory, false, loc);

  return err_code;
}
