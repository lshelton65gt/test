// -*- C++ -*-
/******************************************************************************
  Copyright (c) 2005-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUCompositeLvalue.h"
#include "reduce/Fold.h"
#include "util/CbuildMsgContext.h"
#include "bdd/BDD.h"

// This is a wrapper around vhDetectClock.  It allows for the dissection
// of a condition statement to see if it is composed of a clock and a
// condition. (e.g. if clk'EVENT and clk = '1' and en = '1')
vhNode
VhdlPopulate::detectClock( vhExpr vh_expr, int *vh_edge )
{
  vhNode vh_clock = NULL;
  vh_clock = vhDetectClock( vh_expr, vh_edge );
  if ( vh_clock && vhGetObjType( vh_expr ) == VHBINARY &&
       vhGetOpCat( vh_expr ) == VH_LOGICAL )
  {
    if ( vhGetOpType( vh_expr ) != VH_AND_OP )
    {
      // A complex clock expression is only supported if the clock is
      // ANDed with the rest of the condition.
      const SourceLocator loc = locator( vh_expr );
      mMsgContext->ClockingError( &loc, "A clock condition may only be part of a more complex expression when an AND operand is used." );
      // Don't return a clock
      vh_clock = NULL;
      *vh_edge = -1;
    }
    else
    {
      // recursively try both the left and right branches to find a clock
      int edge = -1;
      vhExpr vh_left = vhGetLeftOperand( vh_expr );
      (void)detectClock( vh_left, &edge );
      vhExpr vh_right = vhGetRightOperand( vh_expr );
      (void)detectClock( vh_right, &edge );
    }
  }
  if ( vh_clock && *vh_edge != 0 && *vh_edge != 1 )
  {
    // If vh_edge is not a posedge or negedge, it's not a clock to us.
    // This is after the vh_clock==NULL check because we have no need to
    // recurse into this expression.
    vh_clock = NULL;
  }
  return vh_clock;
}


// Locate a clock in this if statement, if it exists.  Return NULL and
// vh_edge == -1 if there is no clock.  This does not handle nested if
// statements, other than one nested as the first statement in the else
// clause.
vhNode
VhdlPopulate::getIfStmtClock( vhNode vh_stmt, int *vh_edge )
{
  vhNode vh_clock = NULL;
  *vh_edge = -1;
  if ( vhGetObjType( vh_stmt ) == VHIF )
  {
    // Look in the if condition
    vh_clock = detectClock( vhGetCondition( vh_stmt ), vh_edge );
    if ( vh_clock == NULL )
    {
      // Look in each elsif condition
      vhNode node;
      JaguarList elsifs( vhGetElsifSeqStmtList( vh_stmt ));
      while (( node = vhGetNextItem( elsifs )))
      {
        vh_clock = detectClock( vhGetCondition( node ), vh_edge );
        if ( vh_clock != NULL )
          break;
      }
    }
    if ( vh_clock == NULL )
    {
      // Look at the first statement in the else statements
      JaguarList elsestmts( vhGetElsSeqStmtList( vh_stmt ));
      vhNode node = vhGetNextItem( elsestmts );
      if ( vhGetObjType( node ) == VHIF )
      {
        vh_clock = detectClock( vhGetCondition( node ), vh_edge );
      }
    }
  }
  return vh_clock;
}

vhNode
VhdlPopulate::getWaitStmtClock(  vhNode vh_process, JaguarList *seq_stmts, int *vh_edge )
{
  vhNode vh_clock = NULL;
  *vh_edge = -1;

  // If the process has a sensitivity list it is immediately disqualified
  if ( !vhIsSensitive( vh_process ) )
  {
    vhNode vh_stmt;
    UInt32 numWaitStmts = 0;
    while ( ( vh_stmt = vhGetNextItem( *seq_stmts ) ) )
    {
      vhObjType stmt_type = vhGetObjType( vh_stmt );
      if ( numWaitStmts == 0 )
      {
	if ( stmt_type == VHWAIT )
	{
	  numWaitStmts += 1;
	  vh_clock = vhGetProcessClockExpr( vh_process );
	  if ( vh_clock == NULL )
	    vh_clock = vhGetProcessClock( vh_process );
	  if ( vh_clock != NULL )
	    *vh_edge  = vhGetProcessClockEdge( vh_process );
	}
	else if ( stmt_type != VHASSERT && stmt_type != VHREPORT && stmt_type != VHNULL )
	{
	  // The first statement in the process has to be the wait statement
	  return vh_clock;
	}
      }
      else if ( hasWaitStmt( vh_stmt ) != NULL )
      {
	// Only one wait statement is allowed in the process
	return vh_clock;
      }
    }
  }

  return vh_clock;
}

// Helper function returning a boolean indicating if an if/else chain
// contains a clock edge in any of its conditions.
bool
VhdlPopulate::ifStmtContainsClock( vhNode vh_stmt )
{
  int vh_edge = -1;
  vhNode vh_clock = getIfStmtClock( vh_stmt, &vh_edge );
  return vh_clock != NULL;
}


Populate::ErrorCode
VhdlPopulate::resetExprHelper( vhExpr if_cond, LFContext *context,
                               NUExpr **the_cond_expr )
{
  ErrorCode err_code = eSuccess;
  *the_cond_expr = NULL;
  // If this is a complex reset expression, get the synthetic reset expr for it.
  VhNodeEdgeExprMap::iterator iter;
  if (mCurrentProcessInfo != NULL) {
    iter = mCurrentProcessInfo->mComplexResetMap.find( if_cond );
  }
  if ( mCurrentProcessInfo && iter != mCurrentProcessInfo->mComplexResetMap.end() )
  {
    CopyContext cc( context->getModule(), NULL );
    *the_cond_expr = iter->second->copy( cc );
  }
  else
  {
    err_code = condExpr( if_cond, context, the_cond_expr );
  }
  return err_code;
}


void
VhdlPopulate::processCleanup( LFContext *context )
{
  // Pop off the stack elements pushed on in sequentialBlock
  context->popStmtList();
  context->popJaguarScope();
  context->popDeclarationScope();
  context->popBlockScope();
  // These class members are only valid for one process--clean up on
  // exit, deleting the values.
  delete mCurrentProcessInfo;
  mCurrentProcessInfo = NULL;
}


// This function returns true if second_if_conf == !first_if_conf 
// It is used to compare rst and !rst statements and valudate 
// that they are mutually exclusive.
//
// if rst then              
//   <reset stmts>           
// elsiif !rst then         
//   if <clkedge> then         
//     <clock stmts>       
//   endif;                   
// else                           
//  <else stmts>             
// endif;                    

bool
VhdlPopulate::validateCustomReset( vhExpr first_if_conf, vhExpr second_if_conf )
{
  bool rtn = false;
  vhObjType vh_obj_type1 = vhGetObjType(first_if_conf);
  vhObjType vh_obj_type2 = vhGetObjType(second_if_conf);
  // The conditions have to be binary
  if((vh_obj_type1 == VHBINARY) && (vh_obj_type2 == VHBINARY))
  {
    vhExpr vh_left_operand1 = vhGetLeftOperand(first_if_conf);
    vhExpr vh_left_operand2 = vhGetLeftOperand(second_if_conf);
    // In VHDL rst and !rst is presented as
    // if(rst = '1') and if (rst = '0'.
    // So, the left signal of both conditions should be the same
    if(vh_left_operand1 == vh_left_operand2)
    {
      vhOpType vh_op_type1 = vhGetOpType(first_if_conf);
      vhOpType vh_op_type2 = vhGetOpType(second_if_conf);
      // Make sure that it's equal operation for both of them
      if((vh_op_type1 == VH_EQ_OP) && (vh_op_type2 == VH_EQ_OP))
      {
        vhExpr vh_right_operand1 = vhGetRightOperand(first_if_conf);
        vhExpr vh_right_operand2 = vhGetRightOperand(second_if_conf);
        vhObjType vh_right_obj_type1 = vhGetObjType(vh_right_operand1);        
        vhObjType vh_right_obj_type2 = vhGetObjType(vh_right_operand2); 
        // On the right side of the condition, we should have character literal.
        if((vh_right_obj_type1 == VHCHARLIT) && (vh_right_obj_type2 == VHCHARLIT))
        {
          char value1 = vhGetCharVal(vh_right_operand1);
          char value2 = vhGetCharVal(vh_right_operand2);
          if((value1 == '1' && value2 == '0') || (value1 == '0' && value2 == '1'))
          {
            // If they are '1' and '0', or '0' and '1', then we have rst and !rst.
            rtn = true;
          }
        }
      }
    }
  }

  return rtn;
}



// This routine checks for two type of not synthesizable clock constructs
// and modifies them so, that CBUILD can process them later. 
// The first one is nested clock edge in if stmt (see bug 5282).
// 
// The example on the left get's transformed into one with the form of the
// right example.
//
// 
// if cond then               if not cond then
//   if <clkedge> then          <reset stmts>
//     <clock stmts>              else
//   endif;                     if <clkedge> then
// else                           <clock stmts>
//  <reset stmts>              endif;
// endif;                     endif;
// 
//
// The crucial difference here is that we cannot detect the clock in the
// left example, but can detect the clock in the example on the right.
// The current limitation is limited to only the form shown above.  For
// example, if there are any statements preceding the if statement the
// normalization will not occur.  Both the reset and clock statement
// lists may have zero or more statements in them.
//
// The second type clock construct has two issues; nested clock in 
// the elsif and else block (see bug 12178).
//
// if rst then               if rst then
//   <reset stmts>             <reset stmts
// elsiif !rst then          elsif <clkedge> then
//   if <clkedge> then         <clock stmts>  
//     <clock stmts>         endif;
//   endif;                   
// else                           
//  <else stmts>             
// endif;                    
// 
// The else block is nether executed in CBUILD, so we drop it.
// The nested clock condition is moved up and replaced the !rst condition.
// The right side if construct is synthesizable and supported by CBUILD.

bool
VhdlPopulate::maybeNormalizeClockStmt( JaguarList *seq_stmts )
{
  bool retval = false;
  if ( seq_stmts->size() == 1 ) // only handle single-statement list
  {
    seq_stmts->rewind();
    vhNode vh_ifstmt = vhGetNextItem( *seq_stmts );
    if ( vhGetObjType( vh_ifstmt ) == VHIF )
    {
      int dummyEdge;
      // There is no elsifs, it could be the first construct
      if ( !getIfStmtClock( vh_ifstmt, &dummyEdge ) &&
           vhGetElsifSeqStmtList( vh_ifstmt ) == NULL )
      {
        // If there's no clock to be found in the normal places, look
        // inside the then statements.
        JaguarList then_stmts( vhGetThenSeqStmtList( vh_ifstmt ));
        vhNode vh_clock = vhGetNextItem( then_stmts );
        if ( vhGetObjType( vh_clock ) == VHIF )
        {
          vhExpr vh_ifcond = vhGetCondition( vh_clock );
          if ( detectClock( vh_ifcond, &dummyEdge ))
          {
            // There's a clock we can deal with in the then_stmts.  Now
            // we normalize.  First, set up the creation environment.
            // We need to use vhGetMasterScope so that the statement
            // doesn't get dropped into the original process's statement list.
            vhNode scope = vhGetMasterScope( vh_ifstmt );
            JaguarString fileName( vhGetSourceFileName( scope ));
            vhwSetCreateScope( scope, fileName.get_nonconst() );
            // Create a new condition.
            vhExpr if_cond = vhGetCondition( vh_ifstmt );
            vhNode vh_exprtype = vhGetExpressionType( if_cond );
            vhNode new_cond = vhwCreateUnary( VH_NOT_OP, if_cond, vh_exprtype );
            vhSetLineNumber( new_cond, vhGetLineNumber( if_cond ));
            vhSetEndLineNumber( new_cond, vhGetEndLineNumber( if_cond ));
            // Create the normalized if statement
            vhNode if_label = vhGetLabelObj( vh_ifstmt );
            JaguarList new_else_stmts = then_stmts;
            JaguarList else_stmts( vhGetElsSeqStmtList( vh_ifstmt ));
            JaguarList new_then_stmts = else_stmts;
            vhNode new_if = vhwCreateIfStmt( new_cond, new_then_stmts, NULL,
                                             new_else_stmts, if_label );
            vhSetLineNumber( new_if, vhGetLineNumber( vh_ifstmt ));
            vhSetEndLineNumber( new_if, vhGetEndLineNumber( vh_ifstmt ));
            // Finally, replace the old if statement with the new one
            vhwReplaceInList( *seq_stmts, vh_ifstmt, new_if );
            retval = true;
          }
        }
      }
      // There are elsif statements, it could be the second construct.
      else if( !getIfStmtClock( vh_ifstmt, &dummyEdge ) &&
               vhGetElsifSeqStmtList( vh_ifstmt ) != NULL )
      {
        JaguarList elsif_stmts( vhGetElsifSeqStmtList( vh_ifstmt ));
        vhNode vh_elsif = vhGetNextItem( elsif_stmts );
        if ( vhGetObjType( vh_elsif ) == VHELSIF )
        {
          vhExpr vh_elsif_cond = vhGetCondition(vh_elsif);          
          JaguarList elsif_then_stmts( vhGetThenSeqStmtList( vh_elsif ));
          vhNode vh_clock = vhGetNextItem( elsif_then_stmts );
          // Is it nested if statement in the elsif block ?
          if ( vhGetObjType( vh_clock ) == VHIF )
          {
            vhExpr vh_ifcond = vhGetCondition( vh_clock );
            // Is there clock in it?
            if ( detectClock( vh_ifcond, &dummyEdge ))
            {
              // There's a clock we can't deal with it the elsif_then_stmts.  Now
              // we normalize.  First, set up the creation environment.
              // We need to use vhGetMasterScope so that the statement
              // doesn't get dropped into the original process's statement list.
              vhNode scope = vhGetMasterScope( vh_ifstmt );
              JaguarString fileName( vhGetSourceFileName( scope ));
              vhwSetCreateScope( scope, fileName.get_nonconst() );

              // The vh_if_stmt_if_cond is rst conditon
              vhExpr vh_if_stmt_if_cond =  vhGetCondition(vh_ifstmt);

              // Now we validate the reset
              if(validateCustomReset(vh_if_stmt_if_cond, vh_elsif_cond) == true)
              {
                // Now we create the new elsif statement to replace the original one
                JaguarList inner_then_stmts(vhGetThenSeqStmtList( vh_clock ));
                vhNode vh_new_elsif_stmt = vhwCreateElsIfStmt(vh_ifcond, inner_then_stmts);
                vhSetLineNumber( vh_new_elsif_stmt, vhGetLineNumber( vh_ifcond ));
                vhSetEndLineNumber( vh_new_elsif_stmt, vhGetEndLineNumber( vh_ifcond ));

                JaguarList new_elsif_stmts = vhwCreateNewList( VH_TRUE );
                vhwAppendToList( new_elsif_stmts, vh_new_elsif_stmt );
              
                JaguarList new_then_stmts(vhGetThenSeqStmtList( vh_ifstmt ));
 
                vhNode vh_if_label = vhGetLabelObj( vh_ifstmt );
                vhNode vh_new_if = vhwCreateIfStmt( vh_if_stmt_if_cond, new_then_stmts, new_elsif_stmts, NULL, vh_if_label);
                vhSetLineNumber( vh_new_if, vhGetLineNumber( vh_ifstmt ));
                vhSetEndLineNumber( vh_new_if, vhGetEndLineNumber( vh_ifstmt ));

                vhwReplaceInList( *seq_stmts, vh_ifstmt, vh_new_if );
                retval = true;
              }
            }
          }
        }
      }
    }
  }
  return retval;
}

VhdlPopulate::VhdlProcessInfo::VhdlProcessInfo()
  : mJaguarClockExpr(NULL),
    mJaguarClockEdge(-1),
    mClockExpr(NULL),
    mProcess(NULL),
    mBlock(NULL)
{
}

VhdlPopulate::VhdlProcessInfo::~VhdlProcessInfo()
{
  mComplexResetMap.clearPointerValues();
  mJaguarClockExpr = NULL;
  mJaguarClockEdge = -1;
  if (mProcess != NULL) {
    delete mProcess;
    mProcess = NULL;
  }
  if (mBlock != NULL) {
    delete mBlock;
    mBlock = NULL;
  }
  delete mClockExpr;
  mClockExpr = NULL;
}

void VhdlPopulate::prePopulateProcessStmt(vhNode vh_process, StringAtom* procDeclScope,
                                          LFContext* context)
{
  SourceLocator loc = locator(vh_process);
  LOC_ASSERT(mCurrentProcessInfo == NULL, loc);
  mCurrentProcessInfo = new VhdlProcessInfo();

  StringAtom *name = context->getModule()->newBlockName( context->getStringCache(), loc );
  mCurrentProcessInfo->mProcess =
    context->stmtFactory()->createAlwaysBlockForJaguarFlow(name, loc);

  // Begin sequential statement block for process.
  sequentialBlock( vh_process, procDeclScope, context, &loc,
                   &(mCurrentProcessInfo->mBlock) );

  context->pushStmtList(&(mCurrentProcessInfo->mStmts));
}

Populate::ErrorCode
VhdlPopulate::postPopulateProcessStmt(vhNode vh_process,
                                      LFContext *context,
                                      NUAlwaysBlock** the_process)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator(vh_process);
  LOC_ASSERT( mCurrentProcessInfo != NULL, loc );

  // Identify the clock and resets from the process and process them.
  NUEdgeExprList edge_expr_list;
  context->pushEdgeExprList(&edge_expr_list);

  JaguarList seq_stmts; // empty list, no vhlist inside.
  JaguarList process_stmts( vhGetSeqStmtList( vh_process ));
  bool elaborated = elaborateSeqStmtList( &process_stmts, &seq_stmts, loc );
  bool normalized = maybeNormalizeClockStmt( &seq_stmts );
  bool has_trailing_reset = false;

  bool is_wait_clock = false;
  temp_err_code = validateWaitClock( vh_process, &seq_stmts );
  if ( temp_err_code == eSuccess )
  {
    // eSuccess for validateWaitClock means,
    // that the process has a wait clock
    is_wait_clock = true;
  }
  else if ( temp_err_code != eNotApplicable )
  {
    processCleanup( context );
    return temp_err_code;
  }
    
  // This method returns eSuccess even though a process may not have trailing
  // reset statements. The argument has_reset_stmts indicates if they are present.
  // A process with wait clock can not have asynchronous resets.
  if(is_wait_clock == false)
  {
    temp_err_code = clockWithMaybeTrailingReset( vh_process, &seq_stmts,
                                                 elaborated | normalized, context,
                                                 &has_trailing_reset );
  }

  if ((is_wait_clock == true) || ((is_wait_clock == false) && temp_err_code == eNotApplicable) )
  {
    // eNotApplicable means that it is not a trailing reset process; try
    // a traditional reset
    temp_err_code = clockAndResets( vh_process, &seq_stmts, elaborated, context, is_wait_clock);
  }
  context->popEdgeExprList();
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    processCleanup( context );
    return temp_err_code;
  }

  // The statement list is already pushed on the stack.
  (void)seq_stmts.rewind();

  // Get all the sequential statements and process them.
  temp_err_code = sequentialStmtList( &seq_stmts, context, is_wait_clock);

  // We should return here only if we have eFatal. 
  // sequentialStmtList gets populated even if population of one or more 
  // sequential statements failed.
  // For example, look into test case test/langcov/vhdlCase/case1.vhdl
  if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
  {
    // Leave our state in as sane and complete of condition as we can
    return temp_err_code;
  }
  mCurrentProcessInfo->mBlock->addStmts(&(mCurrentProcessInfo->mStmts));
  if (!context->stmtFactory()->next(mCurrentProcessInfo->mBlock)) { // Done adding statements to block.
    processCleanup( context );
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }

  mCurrentProcessInfo->mProcess->replaceBlock(NULL, mCurrentProcessInfo->mBlock);
  if (!context->stmtFactory()->next(mCurrentProcessInfo->mProcess)) { // Done adding statements to process.
    processCleanup( context );
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }

  for (NUEdgeExprListIter expr_iter = edge_expr_list.begin();
       expr_iter != edge_expr_list.end();
       expr_iter++) {
    NUEdgeExpr *edge_expr = *expr_iter;
    edge_expr->getNet()->putIsEdgeTrigger(true);
    mCurrentProcessInfo->mProcess->addEdgeExpr(edge_expr);
  }

  *the_process = mCurrentProcessInfo->mProcess;
  // Used up current process and block.
  mCurrentProcessInfo->mProcess = NULL;
  mCurrentProcessInfo->mBlock = NULL;
  processCleanup( context );
  return err_code;
}


// Return values: 0 => valid static bool expression, evals to false
//                1 => valid static bool expression, evals to true
//                2 => not a valid static boolean expression
enum vhBoolEvalT { eVhFalse = 0, eVhTrue = 1, eVhNotStatic = 2 };
static vhBoolEvalT
sEvaluateBooleanCondition( vhExpr vh_condition )
{
  vhBoolEvalT retval = eVhNotStatic;
  // in the following call you must set arg 3 (staticnessCheckRequired)
  // to true  because we do not want to use the initial value for
  // operands of the vh_condition test/vhdl/clocking/bug6082_3.vhdl
  vhExpr vh_boolval = VhdlPopulate::evaluateJaguarExpr( vh_condition, true, true );
  // Does this evaluate to either TRUE or FALSE?
  if ( vh_boolval && vhGetObjType( vh_boolval ) == VHIDENUMLIT )
  {
    int boolval = vhGetIdEnumLitValue( vh_boolval );
    if ( boolval == 0 )
      retval = eVhFalse;
    else if ( boolval == 1 )
      retval = eVhTrue;
  }
  return retval;
}


bool
exprMatchesChoice( vhExpr vh_expr, vhExpr vh_choice )
{
  bool match = false;
  // We currently only evaluate integer case selectors.  Reals are not
  // permitted to be a case selector.
  if ( vhGetObjType( vh_expr ) == VHDECLIT )
  {
    const SInt32 exprVal = (SInt32)vhGetDecLitValue( vh_expr );
    VhObjType choiceType = vhGetObjType( vh_choice );
    if ( choiceType == VHDECLIT )
    {
      SInt32 choiceVal = (SInt32)vhGetDecLitValue( vh_choice );
      if ( exprVal == choiceVal )
      {
        match = true;
      }
    }
    else if ( choiceType == VHRANGE )
    {
      vhExpr vh_left = vhGetLtOfRange( vh_choice );
      vh_left = VhdlPopulate::evaluateJaguarExpr( vh_left );
      vhExpr vh_right = vhGetRtOfRange( vh_choice );
      vh_right = VhdlPopulate::evaluateJaguarExpr( vh_right );
      if ( vhGetObjType( vh_left ) == VHDECLIT &&
           vhGetObjType( vh_right ) == VHDECLIT )
      {
        SInt32 left = (SInt32)vhGetDecLitValue( vh_left );
        SInt32 right = (SInt32)vhGetDecLitValue( vh_right );
        vhDirType dir = vhGetRangeDir( vh_choice );
        // Avoid handling null ranges; they never match in a case statement
        if (( left <= right && dir == VH_TO ) ||
            ( left >= right && dir == VH_DOWNTO ))
        {
          // Normalize the bounds
          SInt32 upper, lower;
          if ( left > right )
          {
            upper = left;
            lower = right;
          }
          else
          {
            upper = right;
            lower = left;
          }
          if ( exprVal <= upper && exprVal >= lower )
            match = true;
        }
      }
    }
    else if ( choiceType == VHOTHERS )
    {
      match = true;
    }
    else
    {
      // Don't match if we don't explicitly handle the node type
      //INFO_ASSERT( false, "Unexpected case choice node type encountered during VHDL static case statement elaboration" );
    }
  }
  return match;
}


void
replaceInList( vhNode vh_stmt, JaguarList *stmtsToMove, JaguarList *elab_stmts,
               vhNode *vh_last )
{
  // Remove the old statement from the outgoing list
  vhwDeleteFromList( *elab_stmts, vh_stmt );
  // Move the stmtsToMove into the process block, starting after vh_last
  vhNode node;
  while (( node = vhGetNextItem( *stmtsToMove )))
  {
    if ( *vh_last == NULL )
    {
      vhwPrependToList( *elab_stmts, node );
    }
    else
    {
      vhwAddAfterNode( *elab_stmts, *vh_last, node );
    }
    *vh_last = node;
  }
}


bool
elaborateCaseStmt( vhNode vh_stmt, JaguarList *elab_stmts, vhNode *vh_last )
{
  bool list_changed = false;
  vhExpr vh_expr = vhGetCaseExpr( vh_stmt );
  vh_expr = VhdlPopulate::evaluateJaguarExpr( vh_expr, true, true );
  // A nonstatic case selector will not be elaborated
  if ( vh_expr == NULL )
  {
    *vh_last = vh_stmt;
    return list_changed;
  }

  // Walk the list of case alternatives
  JaguarList vh_caselist( vhGetAlternativeList( vh_stmt ));
  while ( vhExpr vh_case = static_cast<vhExpr>( vhGetNextItem( vh_caselist )))
  {
    // Each alternative may have multiple choices; walk the choice list
    JaguarList vh_choices( vhGetChoiceList( vh_case ));
    while ( vhExpr vh_choice = static_cast<vhExpr>( vhGetNextItem( vh_choices )))
    {
      vh_choice = VhdlPopulate::evaluateJaguarExpr( vh_choice );
      if ( exprMatchesChoice( vh_expr, vh_choice ))
      {
        JaguarList stmtsToMove( vhGetSeqStmtList( vh_case ));
        // replace vh_stmt in elab_stmts with stmtsToMove
        replaceInList( vh_stmt, &stmtsToMove, elab_stmts, vh_last );
        list_changed = true;
        break; // once we match and replace break from the case choice loop
      }
    }
    if ( list_changed )
      break; // break from the case alternative loop if we've found our match
  }
  return list_changed;
}


// This function accepts a process and attempts to elaborate its IF
// statements.  It will evaluate the condition of the if statements and
// choose one or the other branch if the condition is statically
// evaluatable.  If it is elaboratable, it replaces the if statement
// with the statements in the chosen branch for further clock
// evaluation.  It always passes the result, even if unchanged, back in
// elab_stmts.  The return boolean indicates whether the list was
// changed.
bool
VhdlPopulate::elaborateSeqStmtList( const JaguarList * const orig_stmts,
                                    JaguarList *elab_stmts,
                                    const SourceLocator& loc)
{
  bool list_changed = false;
  if (!elab_stmts->empty()) {
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "Static elaboration must start with an empty list!";
    INFO_ASSERT(elab_stmts->empty(), errMsg.c_str());
  }
  *elab_stmts = *orig_stmts; // creates a new list with the old contents

  // The statement to append the chosen block after
  vhNode vh_last = NULL;
  vhNode vh_stmt;
  while (( vh_stmt = vhGetNextItem( *orig_stmts )))
  {
    if ( vhGetObjType( vh_stmt ) == VHIF )
    {
      JaguarList stmtsToMove;
      JaguarList vh_elsifs( vhGetElsifSeqStmtList( vh_stmt ));
      vhExpr vh_condition = vhGetCondition( vh_stmt );
      vhBoolEvalT static_cond = sEvaluateBooleanCondition( vh_condition );
      if ( static_cond != eVhNotStatic )
      {
        if ( static_cond == eVhTrue )
        {
          // Put the if code as the process body
          stmtsToMove = vhGetThenSeqStmtList( vh_stmt );
        }
        else
        {
          // We don't want the 'if' code; we want the else code, or one
          // of the elsif's codes.
          JaguarList vh_elsifs( vhGetElsifSeqStmtList( vh_stmt ));
          vhNode vh_elsif = vhGetNextItem( vh_elsifs );
          bool nonstatic_condition_found = false;
          // Evaluate each of the elsif conditions in order,
          // short-circuiting on the first one that's true.
          while ( vh_elsif != NULL )
          {
            vh_condition = vhGetCondition( vh_elsif );
            static_cond = sEvaluateBooleanCondition( vh_condition );
            if ( static_cond == eVhNotStatic )
            {
              // we can't statically elaborate this if there are any
              // nonstatic conditions anywhere
              nonstatic_condition_found = true;
              break; // leave the elsif loop
            }

            if ( static_cond == eVhTrue )
            {
              stmtsToMove = vhGetThenSeqStmtList( vh_elsif );
              break;
            }
            vh_elsif = vhGetNextItem( vh_elsifs );
          }

          // end processing this if statement if we found a nonstatic condition
          if ( nonstatic_condition_found == true )
          {
            vh_last = vh_stmt;
            continue; // continue looping through the statement list
          }
          // Handle things if there are no elsifs or if no elsif evals to true
          if ( vh_elsif == NULL )
          {
            // Put the else code as the process body
            stmtsToMove = vhGetElsSeqStmtList( vh_stmt );
          }
        }

        replaceInList( vh_stmt, &stmtsToMove, elab_stmts, &vh_last );
        list_changed = true;
      }
      else
      {
        // It's not a static condition; continue.
        vh_last = vh_stmt;
      }
    }
    else if ( vhGetObjType( vh_stmt ) == VHCASE )
    {
      list_changed = elaborateCaseStmt( vh_stmt, elab_stmts, &vh_last );
    }
    else
    {
      // It's not an if statement; continue.
      vh_last = vh_stmt;
    }
  }

  elab_stmts->rewind();
  while (( vh_stmt = vhGetNextItem( *elab_stmts )))
  {
    if ( vhGetObjType( vh_stmt ) == VHIF )
    {
      JaguarList then_stmts( vhGetThenSeqStmtList( vh_stmt ));
      JaguarList else_stmts( vhGetElsSeqStmtList( vh_stmt ));
      JaguarList elsifs( vhGetElsifSeqStmtList( vh_stmt ));
      JaguarList elab_then_stmts;
      JaguarList elab_elsifs;
      JaguarList elab_else_stmts;

      // Handle the then statements
      bool createNewIf = elaborateSeqStmtList( &then_stmts, &elab_then_stmts, loc );

      // Handle the else statements
      if ( !else_stmts.empty() )
      {
        bool elaborated = elaborateSeqStmtList( &else_stmts, &elab_else_stmts, loc );
        if ( elaborated == true )
          createNewIf = true;
      }

      // Handle the elsifs
      if ( !elsifs.empty( ))
      {
        elab_elsifs = vhwCreateNewList( VH_TRUE );
        vhNode vh_elsif;
        while (( vh_elsif = vhGetNextItem( elsifs )))
        {
          // vh_elsif is a VHELSIF node
          JaguarList then_stmts( vhGetThenSeqStmtList( vh_elsif ));
          JaguarList elab_elsif_stmts;
          vhNode new_elsif = vh_elsif;
          bool elaborated = elaborateSeqStmtList( &then_stmts, &elab_elsif_stmts, loc );
          if ( elaborated )
          {
            createNewIf = true;
            // All node creation needs a creation scope set.  We need to
            // use vhGetMasterScope so that the statement doesn't get
            // dropped into the original process's statement list.
            vhNode scope = vhGetMasterScope( vh_stmt );
            JaguarString fileName( vhGetSourceFileName( scope ));
            vhwSetCreateScope( scope, fileName.get_nonconst() );
            
            vhNode elsif_cond = vhGetCondition( vh_elsif );
            new_elsif = vhwCreateElsIfStmt( elsif_cond, elab_elsif_stmts );
            vhSetLineNumber( new_elsif, vhGetLineNumber( vh_elsif ));
            vhSetEndLineNumber( new_elsif, vhGetEndLineNumber( vh_elsif ));
          }
          vhwAppendToList( elab_elsifs, new_elsif );
        }
      }

      if ( createNewIf == true )
      {
        // Create a new if, replace node in *elab_stmts with the new if statement
        vhNode if_label = vhGetLabelObj( vh_stmt );
        vhExpr if_cond = vhGetCondition( vh_stmt );
        // All node creation needs a creation scope set.  We need to use
        // vhGetMasterScope so that the statement doesn't get dropped
        // into the original process's statement list.
        vhNode scope = vhGetMasterScope( vh_stmt );
        JaguarString fileName( vhGetSourceFileName( scope ));
        vhwSetCreateScope( scope, fileName.get_nonconst() );
        vhNode new_if = vhwCreateIfStmt( if_cond, elab_then_stmts, elab_elsifs,
                                         elab_else_stmts, if_label );
        vhSetLineNumber( new_if, vhGetLineNumber( vh_stmt ));
        vhSetEndLineNumber( new_if, vhGetEndLineNumber( vh_stmt ));
        vhwReplaceInList( *elab_stmts, vh_stmt, new_if );
        list_changed = true;
      }
    }
  }
  // reset the lists so the caller does not need to
  elab_stmts->rewind();
  orig_stmts->rewind();
  return list_changed;
}


// This function recursively checks for a wait statement within in a
// vh_stmt.  It returns the first found wait (vhNode for locator in
// message) immediately.
vhNode
VhdlPopulate::hasWaitStmt( vhNode vh_stmt)
{
  switch(vhGetObjType(vh_stmt))
    {
    case VHWAIT:
      {
        return vh_stmt;
        break;
      }
    case VHWHILE:
    case VHLOOP:
    case VHFOR:
      {
        vhNode vh_loopstmt = vh_stmt;
        if (VHLOOP != vhGetObjType(vh_stmt))
          vh_loopstmt = vhGetLoopStmt(vh_stmt);
        JaguarList loop_stmts( vhGetSeqStmtList( vh_loopstmt) );
        vhNode vs = 0;
        while ((vs = vhGetNextItem(loop_stmts)) != NULL )
          {
            if ( vhNode waitStmt = hasWaitStmt(vs) )
              return waitStmt;
          }
        break;
      }
    case VHIF:
      {
        JaguarList vh_if_else_list(vhGetElsSeqStmtList(vh_stmt));
        vhNode vs = 0;
        while ((vs = vhGetNextItem(vh_if_else_list)))
          {
            if ( vhNode waitStmt = hasWaitStmt(vs) )
              return waitStmt;
          }
        JaguarList vh_if_elsif_item(vhGetElsifSeqStmtList(vh_stmt));
        vhNode vh_elsif = 0;
        while ((vh_elsif = vhGetNextItem(vh_if_elsif_item)))
          {
            if ( vhNode waitStmt = hasWaitStmt(vh_elsif) )
              return waitStmt;
          }

        // fall through for then statement list
      }    

    case VHELSIF:
      {
        JaguarList vh_if_then_list(vhGetThenSeqStmtList(vh_stmt));
        vhNode vs = 0;
        while ((vs = vhGetNextItem(vh_if_then_list)))
          {
            if ( vhNode waitStmt = hasWaitStmt(vs) )
              return waitStmt;
          }
      break;
      }

    case VHCASE:
      {
        JaguarList vh_case_item_list(vhGetAlternativeList(vh_stmt));
        vhNode vh_case_item = 0;
        while ((vh_case_item = vhGetNextItem(vh_case_item_list)) != NULL)
          {
            JaguarList vh_stmt_list(vhGetSeqStmtList(vh_case_item));
            vhNode vs = 0;
            while ((vs = vhGetNextItem(vh_stmt_list)) != NULL )
              {
                if ( vhNode waitStmt = hasWaitStmt( vs ) )
                  return waitStmt;
              }
          }
      break;
      }
    case VHPROCEDURECALL:
    {
      vhNode vh_node;
      if (( vh_node = vhGetMaster(vh_stmt)))
        if (( vh_node = vhGetSubProgBody( vh_node ))) 
          if (vhHasWait(vh_node))
            return vh_stmt;
    }
    default:
      break;
    }
  return NULL;
}


// This function is used only for processes with wait until clk construct.
// It rearanges the statements such, that all the statements above the
// wait appear at the end of the process.
Populate::ErrorCode
VhdlPopulate::rearangeWaitClock(JaguarList *vh_seq_stmts, JaguarList *vh_new_list)
{
  ErrorCode err_code = eNotApplicable; 

  vhNode vh_stmt,  vh_first_stm = NULL;
  bool hit_wait = false;
  int index = 0; 
  while (( vh_stmt = vhGetNextItem( *vh_seq_stmts )))
  {
    if(index == 0)
      vh_first_stm = vh_stmt;

    vhObjType stmt_type = vhGetObjType( vh_stmt );
    // We need to rearange the stmts only, when the wait clock stmt is not
    // the first one
    if((stmt_type == VHWAIT) && (index != 0))
    {
      hit_wait = true;
      err_code = eSuccess;
    }
    else
    {
      if(hit_wait)
        vhwAddBeforeNode(*vh_new_list, vh_first_stm, vh_stmt);
      else
        vhwAppendToList(*vh_new_list, vh_stmt);
    }

    index++;
  }
  
  return err_code;
}

// If the process has no sensitivity list, the clock is because of a
// wait statement.  Wait statement clocks cannot have an async reset.
Populate::ErrorCode
VhdlPopulate::validateWaitClock( vhNode vh_process, JaguarList *seq_stmts )
{
  ErrorCode err_code = eNotApplicable;

  if ( !vhIsSensitive( vh_process ))
  {
    vhNode vh_stmt = vhGetNextItem( *seq_stmts );
    UInt32 numwaits = 0;
    do
    {
      vhObjType stmt_type = vhGetObjType( vh_stmt );
      if ( numwaits == 0 )
      {
        if ( stmt_type == VHWAIT )
          numwaits += 1;
      }
      else if ( hasWaitStmt( vh_stmt ) )
      {
        // We don't support multiple wait statements in a process
        SourceLocator loc = locator(vh_process);
        UtString reason( "Multiple wait statements in a process are not supported." );
        mMsgContext->ClockingError( &loc, reason.c_str( ));
        if ( !mClockErrorsDemoted )
        {
          return eFailPopulate;
        }
      }
    }
    while (( vh_stmt = vhGetNextItem( *seq_stmts )));
    err_code = eSuccess;
  }
  return err_code;
}


// Make sure that we can properly populate the Jaguar into nucleus, with
// respect to clocks.
Populate::ErrorCode
VhdlPopulate::validateClock( vhNode vh_process, vhNode vh_clock_stmt,
                             vhNode vh_clock )
{
  ErrorCode err_code = eSuccess;
  
  // We have an if-then based clock.  Make sure the clock is in the
  // sensitivity list.
  bool foundClock = false;
  JaguarList sensList( vhGetSensitivityList( vh_process ));
  vhNode vh_baseclksig = ( vhGetObjType( vh_clock ) != VHINDNAME ) ? vh_clock :
    vhGetActualObj( vhGetPrefix( static_cast<vhExpr>( vh_clock )));
  while ( vhNode sens_sig = vhGetNextItem( sensList ))
  {
    if ( vhGetObjType( sens_sig ) == VHOBJECT )
    {
      sens_sig = vhGetActualObj( sens_sig );
    }
    if ( vhGetObjType( sens_sig ) == VHINDNAME )
    {
      sens_sig = vhGetActualObj( vhGetPrefix( static_cast<vhExpr>( sens_sig )));
    } 
    if ( sens_sig == vh_baseclksig )
    {    
      foundClock = true;
      break;
    }
  }
  if ( foundClock == false )
  {
    SourceLocator loc = locator( vh_process );
    UtString reason, name;
    getVhdlObjName( vh_clock, &name );
    reason << "Clock " << name.c_str()
           << " not found in process sensitivity list.";
    mMsgContext->ClockingError( &loc, reason.c_str( ));
    if ( !mClockErrorsDemoted )
    {
      return eFailPopulate;
    }
  }

  // beforeClockBlock indicates whether we have found and processed a
  // clock-and-reset block yet.  Statements before the block in a
  // supported process should not affect reset processing.  (Variable
  // assignments, at least, are permitted before the clock block.)  I
  // think we need to be liberal in what we let pass at this point.  So,
  // what this code does is not error out at this point on any
  // statements before the clock if-then-elseif tree.  Statements after
  // the clock block do cause errors.
  SourceLocator if_loc;
  foundClock = false;
  vhObjType stmt_type = vhGetObjType( vh_clock_stmt );
  vhNode clock_cond;
  if ( stmt_type == VHIF )
  {
    if ( !ifStmtContainsClock( vh_clock_stmt ))
      return eNotApplicable;

    if_loc = locator( vh_clock_stmt );
         
    // Check to see if clock is the last condition here. If not, it's
    // not valid, since we know we have a clock.
    JaguarList elseList( vhGetElsSeqStmtList( vh_clock_stmt ));
    if ( elseList.size() > 0 )
    {
      // We have an else statement.  The else must contain only a
      // single IF statement that implements the clock.
      // Get the first statement
      clock_cond = vhGetNextItem( elseList );
      if ( elseList.size() > 1 || vhGetObjType( clock_cond ) != VHIF )
      {
        // Error out if we have either too many or the wrong type of statements.
        SourceLocator else_loc = locator( clock_cond );
        UtString reason( "An else block must only contain a single 'if clock_edge' compound statement" );
        mMsgContext->ClockingError( &else_loc, reason.c_str( ));
        if ( !mClockErrorsDemoted )
        {
          return eFailPopulate;
        }
      }
    }
    else
    {
      // No else statement; find the last elsif.
      JaguarList elsifs( vhGetElsifSeqStmtList( vh_clock_stmt ));
      vhNode elsif;
      clock_cond = vh_clock_stmt;
      while (( elsif = vhGetNextItem( elsifs )))
      {
        clock_cond = elsif;
      }
    }

    // Verify that the condition of vh_clock_stmt is a clock.
    int dummy_edge;
    if (( vhGetObjType( clock_cond ) == VHIF || 
          vhGetObjType( clock_cond ) == VHELSIF ) &&
        detectClock( vhGetCondition( clock_cond ), &dummy_edge ) != NULL )
    {
      foundClock = true;
    }
  }
  else
  {
    return eNotApplicable;
  }

  if ( foundClock == false )
  {
    UtString reason( "The last clause of the if statement must be an elsif containing the process clock." );
    mMsgContext->ClockingError( &if_loc, reason.c_str( ));
    if ( !mClockErrorsDemoted )
    {
      err_code = eFailPopulate;
    }
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::detectResets( JaguarList *seq_stmts, const SourceLocator &loc, LFContext *context )
{
  ErrorCode err_code = eSuccess, temp_err_code;

  // Get all the if conditions to identify the reset signal(s)
  while ( vhNode vh_stmt = vhGetNextItem( *seq_stmts ))
  {
    vhObjType stmt_type = vhGetObjType( vh_stmt );
    if ( stmt_type == VHIF )
    {
      // Check to see if this 'if' ends in an 'elsif clock_edge'.
      JaguarList elsifs( vhGetElsifSeqStmtList( vh_stmt ));
      JaguarList elseList( vhGetElsSeqStmtList( vh_stmt ));
      vhNode elsif, vh_clock_stmt = NULL;
      if ( elseList.size() == 1 )
      {
        // If we have an else, the clock might be an if stmt in the else block
        vh_clock_stmt = vhGetNextItem( elseList );
        if ( vhGetObjType( vh_clock_stmt ) != VHIF )
        {
          vh_clock_stmt = NULL;
        }
      }
      else
      {
        // find the last elsif--it's the clock
        while (( elsif = vhGetNextItem( elsifs )))
        {
          vh_clock_stmt = elsif;
        }
      }
      int dummy_edge;
      if ( vh_clock_stmt == NULL || 
           detectClock( vhGetCondition( vh_clock_stmt ), &dummy_edge ) == NULL )
      {
        // If there's no elsif clause, or if the last elsif condition is
        // not a clock, this if statement can't be a reset.
        continue;
      }

      vhExpr cond_expr = vhGetCondition(vh_stmt);
      temp_err_code = resetEdge(cond_expr, context);
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
        mMsgContext->JaguarConsistency(&loc, "Failed to process reset signal in if clause");
        return temp_err_code;
      }
      elsifs.rewind();
      while (( elsif = vhGetNextItem( elsifs )))
      {
        cond_expr = vhGetCondition( elsif );
        temp_err_code = resetEdge( cond_expr, context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        {
          mMsgContext->JaguarConsistency(&loc, "Failed to process reset signal in elsif clause");
          return temp_err_code;
        }
      }
    }
  }
  return err_code;
}

// Detect a process that has its reset(s) in (a) separate trailing if statement(s).
Populate::ErrorCode
VhdlPopulate::clockWithMaybeTrailingReset( vhNode vh_process, JaguarList *seq_stmts, 
                                           bool modified, LFContext *context,
                                           bool* has_trailing_reset )
{
  ErrorCode err_code = eNotApplicable, temp_err_code;
  bool foundClock = false;
  vhNode vh_clock;
  ClockEdge edge;

  // seq_stmts will contain either the temporarily elaborated statements
  // from this process, or the unmodified process seq_stmt list if no
  // elaboration was performed.
  err_code = findClock( vh_process, &vh_clock, &edge, seq_stmts, modified );
  if ( err_code != eSuccess )
  {
    // No clock found, or no valid edge specified for the clock returns
    // eNotApplicable.  Any other non-success is an error condition.
    return err_code;
  }

  (void)seq_stmts->rewind();

  SourceLocator clock_loc;
  (void)seq_stmts->rewind();
  vhNode vh_stmt;
  while (( vh_stmt = vhGetNextItem( *seq_stmts ))) 
  {
    vhObjType stmt_type = vhGetObjType( vh_stmt );
    if ( stmt_type == VHIF )
    {
      if ( !foundClock )
      {
        temp_err_code = validateClock( vh_process, vh_stmt, vh_clock );
        if ( temp_err_code == eNotApplicable )
        {
          // this wasn't a clock statement; continue on.
          continue;
        }
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
          return temp_err_code;
        }
        foundClock = true;
        err_code = eSuccess; // have zero trailing resets at this point
      }
      else
      {
        // If we get a second clock edge, it is not a reset
        if ( !ifStmtContainsClock( vh_stmt ))
        {
          err_code = eSuccess; // At this point we have recognized this style
          // This condition better be a reset
          vhExpr if_cond = vhGetCondition( vh_stmt );
          temp_err_code = resetEdge( if_cond, context );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          {
            const SourceLocator loc = locator( vh_stmt );
            mMsgContext->JaguarConsistency(&loc, "Failed to process reset signal in trailing if clause");
            return temp_err_code;
          }
          // We've found the trailing reset if-statement.
          *has_trailing_reset = true;
          // Process any elsif statements too
          JaguarList elsifs( vhGetElsifSeqStmtList( vh_stmt ));
          vhNode vh_elsif = vhGetNextItem( elsifs );
          while ( vh_elsif != NULL )
          {
            vhExpr if_cond = vhGetCondition( vh_elsif );
            temp_err_code = resetEdge( if_cond, context );
            if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            {
              const SourceLocator loc = locator( vh_stmt );
              mMsgContext->JaguarConsistency(&loc, "Failed to process reset signal in trailing elsif clause");
              return temp_err_code;
            }
            vh_elsif = vhGetNextItem( elsifs );
          }
        }
      }
    }
  }

  if ( foundClock == false )
  {
    SourceLocator loc = locator( vh_process );
    POPULATE_FAILURE(mMsgContext->ClockingError( &loc, "Could not locate the clock in this process." ), &err_code);
    return err_code;
  }

  if ( err_code == eSuccess )
  {
    (void)seq_stmts->rewind();
    temp_err_code = detectResets( seq_stmts, locator( vh_process ), context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    err_code = createClockEdgeExpr( vh_clock, edge, clock_loc, context );
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::findClock( vhNode vh_process, vhNode *vh_clock, ClockEdge *edge, 
                         JaguarList *seq_stmts, bool elaborated )
{
  int vh_edge = -1;
  *vh_clock = NULL;
  if ( not elaborated )
  {
    *seq_stmts = vhGetSeqStmtList( vh_process );
    // This block of code will get the clock from Jaguar, if it could find it.
    *vh_clock = vhGetProcessClockExpr(vh_process);
    if ( *vh_clock == NULL )
    {
      *vh_clock = vhGetProcessClock(vh_process);
    }
    if ( *vh_clock != NULL )
    {
      if ( vhGetObjType(*vh_clock) == VHOBJECT ){
        *vh_clock = vhGetActualObj(*vh_clock);
      }
      // Determine which edge of the clock we're sensitive to and exit successfully.
      vh_edge = vhGetProcessClockEdge(vh_process);
    }
  }

  if ( *vh_clock == NULL )
  {
    // We might have removed the clock from the process, depending on
    // the elaboration, or Jaguar might not have found it in the first
    // place, if the clock was part of a logical condition.  So, we must
    // explicitly check for the clock in the body of statements rather
    // than use the original process info.
    (void)seq_stmts->rewind();
    while ( vhNode vh_ifstmt = vhGetNextItem( *seq_stmts ))
    {
      *vh_clock = getIfStmtClock( vh_ifstmt, &vh_edge );
      if ( *vh_clock )
      {
        *vh_clock = vhGetActualObj( *vh_clock );
        break;
      }
    }

    // If we couldn't find the clock in the elaborated code, try
    // to find it in a wait statement. The wait statement must be
    // the first statement in the process and there cannot be
    // any other wait statements in the process. The process cannot
    // have any signals in it is sensitivity list.
    if ( *vh_clock == NULL )
    {
      (void)seq_stmts->rewind();
      *vh_clock = getWaitStmtClock( vh_process, seq_stmts, &vh_edge );

      if ( *vh_clock )
      {
	*vh_clock = vhGetActualObj( *vh_clock );
      }
    }

    if ( *vh_clock == NULL )
    {
      return eNotApplicable;
    }
  }

  // Convert from Jaguar to Nucleus edge polarity
  if (vh_edge == 0)
  {
    *edge = eClockNegedge;
  }
  else if (vh_edge == 1)
  {
    *edge = eClockPosedge;
  }
  else
  {
    // Level sensitive clock? Ignore it.
    return eNotApplicable;
  }
  mCurrentProcessInfo->mJaguarClockExpr = *vh_clock;
  mCurrentProcessInfo->mJaguarClockEdge = vh_edge;
  return eSuccess;
}


// This function processes clock and resets inside a process statement
// in three phases.  First, it locates the clock and creates an edge
// expression it, or just returns if there is no clock.  Second, it
// verifies that the clocking constructs used are supported by cbuild.
// Finally, it detects and creates the edge expressions for the resets.
Populate::ErrorCode
VhdlPopulate::clockAndResets(vhNode vh_process, JaguarList *seq_stmts,
                             bool elaborated, LFContext *context, bool is_wait_clock)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator( vh_process );
  vhNode vh_clock = NULL;
  ClockEdge edge;

  // seq_stmts will contain either the temporarily elaborated statements
  // from this process, or the unmodified process seq_stmt list if no
  // elaboration was performed.
  temp_err_code = findClock( vh_process, &vh_clock, &edge, seq_stmts, elaborated );
  if ( temp_err_code == eNotApplicable )
  {
    // No clock found, or no valid edge specified for the clock
    return eSuccess;
  }
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
    return temp_err_code;
  }

  // Validate that the clocking is in a supported format
  (void)seq_stmts->rewind();
  if(is_wait_clock == false)
  {
    // Validate a traditional if-based clock
    vhNode vh_clock_stmt;
    while (( vh_clock_stmt = vhGetNextItem( *seq_stmts )))
    {
      if ( vhGetObjType( vh_clock_stmt ) == VHIF )
        break;
    }
    temp_err_code = validateClock( vh_process, vh_clock_stmt, vh_clock );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
  }
//  else if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
//    return temp_err_code;
//  }

  (void)seq_stmts->rewind();
  temp_err_code = detectResets( seq_stmts, loc, context );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
    return temp_err_code;
  }

  if (err_code == eSuccess)
  {
    // We've already found and recorded the resets; create the edge
    // expression for clock
    err_code = createClockEdgeExpr( vh_clock, edge, loc, context );
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::createClockEdgeExpr( vhNode vh_clock, ClockEdge edge, 
                                   const SourceLocator &loc, LFContext *context )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  NUExpr* clkExpr = NULL;
  temp_err_code = expr( static_cast<vhExpr>( vh_clock ), context, 1, &clkExpr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    mMsgContext->JaguarConsistency(&loc, "Failed to process clock signal");
    return temp_err_code;
  }
  // Save the current clock expression copy. This is valid in the current 
  // sequential process only. It is deleted when population of process is done.
  CopyContext cc(NULL, NULL);
  mCurrentProcessInfo->mClockExpr = clkExpr->copy(cc);
  mCurrentProcessInfo->mClockExpr->resize(mCurrentProcessInfo->mClockExpr->determineBitSize());

  NUEdgeExpr* the_edge_expr = NULL;
  temp_err_code = edgeExpr( clkExpr, edge, context, &the_edge_expr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    mMsgContext->JaguarConsistency(&loc, "Failed to create clock edge");
    return temp_err_code;
  }
  the_edge_expr->resize( 1 );
  the_edge_expr->setIsClock();
  context->addEdgeExpr( the_edge_expr );
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::resetEdge( vhExpr vh_cond, NUExpr *cond_expr, LFContext *context )
{
  // Check that the condition expression uses a single bit and allocate a 
  NUNetRefSet uses( mNetRefFactory );
  cond_expr->getUses( &uses );
  if( uses.size() == 0 )
  {
    // Not a reset condition
    return eSuccess;
  }
  else if ( uses.size() > 1 )
  {
    return ComplexResetHelper( vh_cond, cond_expr, context );
  }

  // From here on we are assured to have exactly one reset net; both 0
  // and >1 have already been handled.
  NUNetRefHdl cond_ref = *(uses.begin());
  NUNet *sr = cond_ref->getNet();
  if ( cond_ref->getNumBits() != 1 || sr->getBitSize() != 1 )
  {
    return ComplexResetHelper( vh_cond, cond_expr, context );
  }

  NUExpr *sr_expr = NULL;
  SourceLocator loc;
  if ( vh_cond != NULL )
    loc = locator( vh_cond );
  else
    loc = sr->getLoc();
  sr_expr = createIdentRvalue(sr, loc);
  sr_expr->resize(1);
  // Figure out which edge to add.
  BDDContext bdd_context;
  BDD sr_bdd = bdd_context.bdd(sr_expr);
  BDD cond_bdd = bdd_context.bdd(cond_expr);
  BDD not_cond_bdd = bdd_context.opNot(bdd_context.bdd(cond_expr));
  //  Needs to simplify to posdege or negedge.  If not, we have
  //  something funky like a 'bx comparison.  In that case create a
  //  synthetic reset net and use that.
  if( (sr_bdd != cond_bdd) && (sr_bdd != not_cond_bdd))
  {
    delete sr_expr;
    return ComplexResetHelper( vh_cond, cond_expr, context );
  }
  ClockEdge edge = (sr_bdd == cond_bdd) ? eClockPosedge : eClockNegedge;

  NUEdgeExpr* the_edge_expr = 0;
  ErrorCode err_code = edgeExpr( sr_expr, edge, context, &the_edge_expr );
  if ( err_code != eSuccess )
  {
    delete sr_expr;
    if ( vh_cond != NULL )
      delete cond_expr;
    return err_code;
  }
  if (the_edge_expr)
  {
    if ( vh_cond != NULL )
      delete cond_expr;
    the_edge_expr->resize(1);
    the_edge_expr->setIsReset();
    context->addEdgeExpr(the_edge_expr);
  }
  return err_code;
}


// Function: resetEdge()
// Description: This function parses the IF condition and detects the
//              edge of the reset and accordingly creates NUEdge and
//              sensitizes the process on that edge
//              Currently supported reset conditions:
//              reset = 1|0 , rst /= 1|0, not reset, reset, not (reset  = 0|1 )
//              not (reset /= 01), 0|1 = reset, not (0|1 = reset) ...
Populate::ErrorCode
VhdlPopulate::resetEdge(vhExpr vh_cond, LFContext *context)
{
  // Check whether this condition corresponds to any clock edge or
  // not. We will not do anything in case of clock edge condition.
  int dummy_edge;
  if ( detectClock( vh_cond, &dummy_edge ) != NULL )
    return eSuccess;

  ErrorCode err_code = eSuccess;
  NUExpr* cond_expr;

  ErrorCode temp_err_code = condExpr( vh_cond, context, &cond_expr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    SourceLocator loc = locator( vh_cond );
    mMsgContext->JaguarConsistency( &loc, "Failed to create reset expression" );
    return err_code;
  }

  // Check that the condition expression doesn't use zero bits.  This
  // code is duplicated in the child resetEdge() call, but it makes
  // memory management much clearer.
  NUNetRefSet uses( mNetRefFactory );
  cond_expr->getUses( &uses );
  if( uses.size() == 0 )
  {
    // Not a reset condition
    delete cond_expr;
    return eSuccess;
  }
  return resetEdge( vh_cond, cond_expr, context );
}
