// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "LFContext.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUBase.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "util/CarbonAssert.h"
#include "util/ArgProc.h"
#include "flow/FLFactory.h"
#include "util/StringAtom.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUDesign.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "localflow/PplLocalConstProp.h"
#include "reduce/Fold.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/JaguarContext.h"

// Trigger an assert if the provided assert is not one of block, task, or module.
#define BLOCK_CHECK_ASSERT(scope) NU_ASSERT(((scope)->getScopeType()==NUScope::eBlock or \
                                             (scope)->getScopeType()==NUScope::eTask or \
                                             (scope)->getScopeType()==NUScope::eModule), ((NUUseDefNode*)(scope)))
#define DECL_CHECK_ASSERT(scope) NU_ASSERT(((scope)->getScopeType()==NUScope::eNamedDeclarationScope or \
                                             (scope)->getScopeType()==NUScope::eTask or \
                                             (scope)->getScopeType()==NUScope::eModule), ((NUUseDefNode*)(scope)))

#ifdef CDB
//#define DUMP_SANITY_CHECK 1
#endif

#ifdef DUMP_SANITY_CHECK
void sDumpSourceLoc(const SourceLocator& loc, UtOFStream& debugFile)
{
  UtString line;
  loc.compose(&line);
  debugFile << line.c_str();
}

void sDumpVlog(UtString& vlog, UtOFStream& debugFile)
{
  StrToken tok(vlog.c_str(), "\n");
  debugFile << *tok << UtIO::endl;
}

void sDumpName(const char* prefix, StringAtom* name, UtOFStream& debugFile)
{
  debugFile << prefix << name->str() << UtIO::endl;
}

void sDumpStmtOrCaseItem(NUBase* base, UtOFStream& debugFile)
{
  NUStmt* stmt = dynamic_cast<NUStmt*>(base);
  if (stmt != NULL) {
    sDumpSourceLoc(stmt->getLoc(), debugFile);
    UtString vlog;
    stmt->compose(&vlog, NULL, 1, false);
    sDumpVlog(vlog, debugFile);
  } else {
    NUCaseItem* item = dynamic_cast<NUCaseItem*>(base);
    if (item != NULL) {
      sDumpSourceLoc(item->getLoc(), debugFile);
      UtString vlog;
      item->compose(&vlog, NULL, 1, false);
      sDumpVlog(vlog, debugFile);
    }
  }
}

void sDumpSeqBlock(const char* prefix, NUBase* node, UtOFStream& debugFile)
{
  NUTask* task = dynamic_cast<NUTask*>(node);
  if (task != NULL) {
    sDumpSourceLoc(task->getLoc(), debugFile);
    debugFile << prefix;
    sDumpName("Task: ", task->getName(), debugFile);
  } else {
    NUStructuredProc* sp = dynamic_cast<NUStructuredProc*>(node);
    sDumpSourceLoc(sp->getLoc(), debugFile);
    debugFile << prefix;
    sDumpName("StructuredProc: ", sp->getName(), debugFile);
  }
}
#endif

//! Populated nucleus sanity checker.
/*!
  Performs following checks on the factory created statements:
  a. All the tasks have been created by the factory.
  b. All the structured procs have been created by the factory.
  c. All sequential statements and statement blocks have been created
     by the factory and appear in the same order as factory created them.
*/
class PplSanityChecker : public NUDesignCallback
{
public:

  PplSanityChecker(const UtMap<NUBase*,SInt32>* knownBlocks,
                   const UtSet<NUBase*>* markedForDelete,
                   MsgContext* msgContext)
    : mKnownBlocks(knownBlocks),
      mMarkedForDelete(markedForDelete),
      mMsgContext(msgContext),
      mSuccess(true)
#ifdef DUMP_SANITY_CHECK
      , mDebugFile("SanityCheck.debug")
#endif
  {
  }
    
  ~PplSanityChecker()
  {
  }

  //! Catch all.
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Have we seen the given statement in order?
  Status operator()(Phase phase, NUStmt* stmt) {
    return sanityCheck(phase, stmt);
  }

  //! Have we seen the given for loop in order?
  Status operator()(Phase phase, NUFor* forStmt) {
    if (phase == ePre) {
      
      // Check statement ordering in the statement execution order.
      if ( (sanityCheck(phase, forStmt) == eStop) ||
           !sanityCheckStmtList(forStmt->loopInitial()) ||
           !sanityCheckStmtList(forStmt->loopBody()) ||
           !sanityCheckStmtList(forStmt->loopAdvance()) )
        return eStop;
    }
    return eSkip;
  }

  //! Have we seen the given case item in order?
  Status operator()(Phase phase, NUCaseItem* item) {
    return sanityCheck(phase, item);
  }

  //! Have we seen this task?
  Status operator()(Phase phase, NUTask * task) {
    return sanityCheckSeqBlock(phase, task);
  }
 
  //! Have we seen this initial block or always block?
  Status operator()(Phase phase, NUStructuredProc* proc) {
    return sanityCheckSeqBlock(phase, proc);
  }

  bool checkPassed() const { return mSuccess; }

  static const SInt32 cIgnoreStmtIndex;

private:

  Status sanityCheck(Phase phase, NUBase* base);
  Status sanityCheckSeqBlock(Phase phase, NUBase* node);
  bool   sanityCheckStmtList(NUStmtLoop loop);

  const SourceLocator* getStmtOrCaseItemLoc(NUBase* base)
  {
    NUStmt* stmt = dynamic_cast<NUStmt*>(base);
    if (stmt != NULL) {
      return &(stmt->getLoc());
    }
    NUCaseItem* item = dynamic_cast<NUCaseItem*>(base);
    if (item != NULL) {
      return &(item->getLoc());
    }
    NU_ASSERT(0, base);
    return NULL;
  }

  //! Class that keeps current statement ordering index and
  //! a boolean to indicate if the ordering needs to be checked
  //! for a sequential block.
  class SeqBlock
  {
  public:
    SeqBlock(bool check)
      : mIndex(0),
        mCheck(check)
    {
    }
 
    SInt32 mIndex; // Order index for current statement in sequential block.
    bool   mCheck; // Used to enable/disable checking statement order.
  };
 
  const UtMap<NUBase*,SInt32>* mKnownBlocks;
  const UtSet<NUBase*>* mMarkedForDelete;
  MsgContext* mMsgContext;
  bool mSuccess;

  UtList<SeqBlock> mSeqBlockStack;
#ifdef DUMP_SANITY_CHECK
  UtOFStream mDebugFile;
#endif
};

const SInt32 PplSanityChecker::cIgnoreStmtIndex = -2;

bool PplSanityChecker::sanityCheckStmtList(NUStmtLoop loop)
{
  NUDesignWalker walker(*this, false);
  walker.putWalkTasksFromTaskEnables(false);
      
  for (/*loop is initialized*/; mSuccess && !loop.atEnd(); ++loop) {
    walker.stmt(*loop);
  }
  return mSuccess;
}

NUDesignCallback::Status PplSanityChecker::sanityCheck(Phase phase, NUBase* base)
{
  if (!mSeqBlockStack.empty() && mSeqBlockStack.back().mCheck && (phase == ePre))
  {
    // The statement should be known.
    UtMap<NUBase*,SInt32>::const_iterator itr = mKnownBlocks->find(base);
    if (itr == mKnownBlocks->end())
    {
      mSuccess = false;
      mMsgContext->PplSanityCheckFailed(getStmtOrCaseItemLoc(base),
                                        "Statement not found. This statement was probably not created using population statement factory.");
#ifdef DUMP_SANITY_CHECK
      sDumpStmtOrCaseItem(base, mDebugFile);
      mDebugFile << "Statement not found. This statement was probably not created using population statement factory.";
#endif
      return eStop;
    }

    if (itr->second == cIgnoreStmtIndex) {
      return eSkip; // Avoid walking into ignored statement.
    }

    // And it should be in the order the design walker would walk over nucleus.
    ++(mSeqBlockStack.back().mIndex);
#ifdef DUMP_SANITY_CHECK
    sDumpStmtOrCaseItem(base, mDebugFile);
    mDebugFile << "Index: " << mSeqBlockStack.back().mIndex << UtIO::endl;
#endif
    if (itr->second != mSeqBlockStack.back().mIndex)
    {
      mSuccess = false;
      UtString msg;
      msg << "Statement was not created in the execution order. Expected order index is "
          << mSeqBlockStack.back().mIndex << ". Actual order index is " << itr->second << ".";
      mMsgContext->PplSanityCheckFailed(getStmtOrCaseItemLoc(base), msg.c_str());
#ifdef DUMP_SANITY_CHECK
      mDebugFile << "Statement not created in the execution order." << UtIO::endl;
      mDebugFile << "Actual index: " << itr->second << UtIO::endl;
#endif
      return eStop;
    }
  } // if
  else if ((phase == ePost) &&
           (mMarkedForDelete->find(base) != mMarkedForDelete->end()))
  {
    // Delete nodes marked for deletion. For ex: Default caseitem populated and then deleted
    // because other case items cover all cases.
    return eDelete;
  }
  return eNormal;
}

NUDesignCallback::Status 
PplSanityChecker::sanityCheckSeqBlock(Phase phase, NUBase* node)
{
  if (phase == ePre)
  {
    // Start checking new block if it is a known block i.e a block
    // enrolled for checking during population.
    UtMap<NUBase*,SInt32>::const_iterator itr = mKnownBlocks->find(node);
    mSeqBlockStack.push_back(SeqBlock(itr != mKnownBlocks->end()));
#ifdef DUMP_SANITY_CHECK
    sDumpSeqBlock("Push: ", node, mDebugFile);
#endif
  }
  else if (phase == ePost)
  {
    // Finish checkin the block.
    mSeqBlockStack.pop_back();
#ifdef DUMP_SANITY_CHECK
    sDumpSeqBlock("Pop: ", node, mDebugFile);
#endif
  }
  return eNormal;
}

//! Represents sequential statement blocks like if-then, if-else, case,
//! case-items, for-initial, for-body, for-advance, tasks, always blocks.
class StmtBlock
{
public:
  StmtBlock(NUBase* block, PplLocalConstProp::StmtBlockT type,
            Fold* fold, IODBNucleus* iodb, StmtBlock* parentBlock, 
            VhdlPopulate::EntityAliasMap* alias_map)
    : mBlock(block),
      mType(type),
      mIndex(0),
      mIgnoreStmts(false),
      mLCP(NULL),
      mOwnLCP(false),
      mAliasMap(alias_map)
  {
    check(parentBlock);
    // Copy index and constant propagation object from parent if this is not
    // a new sequential block.
    if ( (mType != PplLocalConstProp::eTask) &&
         (mType != PplLocalConstProp::eStrucProc) ) {
      mIndex = parentBlock->getIndex();
      mLCP = parentBlock->mLCP;
    } else {
      mLCP = new PplLocalConstProp(fold, iodb, mAliasMap);
      mOwnLCP = true;
    }
  }

  ~StmtBlock()
  {
    if (mOwnLCP) {
      delete mLCP;
    }
    mLCP = NULL;
  }

  void begin() {
    // Notify constant propagation of beginning of new block context.
    mLCP->next(PplLocalConstProp::eBegin, mBlock, mType);
  }

  void end() {
    // Notify local constant propagation of the end of current block context.
    mLCP->next(PplLocalConstProp::eEnd, mBlock, mType);
  }

  void add(NUStmt* stmt, NUNetSet* forStmtDefs, NUScope* declScope) {
    ++mIndex;
    mLCP->next(stmt, forStmtDefs, declScope);
  }

  void add(NUCaseItem*) {
    ++mIndex;
    // No need to notify LCP. This has been done in the constructor.
  }

  void addCopy(NUStmt* stmt) {
    // No need to increment index. Just invalid defs of this statement.
    mLCP->applyNonPropagatedDefs(stmt);
  }

  NUExpr* optimizeExpr(NUExpr* expr) {
    return mLCP->optimizeExpr(expr);
  }

  PplLocalConstProp::StmtBlockT getType() const { return mType; }

  NUBase* getBlock() const { return mBlock; }

  SInt32 getIndex() const { return mIndex; }

  void poppedOutChild(StmtBlock* childBlock) {
    // Child block is used to update index after it is done, if
    // it's not a sequential block.
    if ((childBlock->mType != PplLocalConstProp::eTask) &&
        (childBlock->mType != PplLocalConstProp::eStrucProc)) {
      mIndex = childBlock->getIndex();
    }
  }

  void setIgnoreStmts(bool ignoreStmts) {
    mIgnoreStmts = ignoreStmts;
  }

  bool getIgnoreStmts() const {
    return mIgnoreStmts;
  }

private:

  // Check that the class arguments are consistent.
  void check(StmtBlock* parentBlock)
  {
    // Only tasks/structured procs can be root statement blocks.
    NU_ASSERT( (parentBlock != NULL) || (mType == PplLocalConstProp::eStrucProc) ||
               (mType == PplLocalConstProp::eTask), mBlock );
    switch(mType)
    {
    case PplLocalConstProp::eIfThen:
    case PplLocalConstProp::eIfElse:
    {
      NU_ASSERT(dynamic_cast<NUIf*>(mBlock) != NULL, mBlock);
      break;
    }
    case PplLocalConstProp::eForInitial:
    case PplLocalConstProp::eForBody:
    case PplLocalConstProp::eForAdvance:
    {
      NU_ASSERT(dynamic_cast<NUFor*>(mBlock) != NULL, mBlock);
      break;
    }
    case PplLocalConstProp::eCase:
    {
      NU_ASSERT(dynamic_cast<NUCase*>(mBlock) != NULL, mBlock);
      break;
    }
    case PplLocalConstProp::eCaseItem:
    {
      NU_ASSERT(dynamic_cast<NUCaseItem*>(mBlock) != NULL, mBlock);
      break;
    }
    case PplLocalConstProp::eTask:
    {
      NU_ASSERT(dynamic_cast<NUTask*>(mBlock) != NULL, mBlock);
      break;
    }
    case PplLocalConstProp::eStrucProc:
    {
      NU_ASSERT(dynamic_cast<NUStructuredProc*>(mBlock) != NULL, mBlock);
      break;
    }
    case PplLocalConstProp::eBlock:
    {
      NU_ASSERT(dynamic_cast<NUBlock*>(mBlock) != NULL, mBlock);
      break;
    }
    default:
    {
      NU_ASSERT(0, mBlock);
      break;
    }
    }
  }

  NUBase*    mBlock; // If, Case, CaseItem etc sequential statement block
  PplLocalConstProp::StmtBlockT mType; // Type of sequential block
  SInt32     mIndex; // Ordering index.
  bool       mIgnoreStmts; // Ignore statements created but not added to this block.
  PplLocalConstProp* mLCP;
  bool       mOwnLCP;
  VhdlPopulate::EntityAliasMap* mAliasMap;   // Map of vhdl Aliases
};

class StmtBlockContext
{
public:

  StmtBlockContext(Fold* fold, IODBNucleus* iodb,   VhdlPopulate::EntityAliasMap* alias_map)
    : mFold(fold),
      mIODB(iodb),
      mAliasMap(alias_map)
#ifdef DUMP_SANITY_CHECK
     , mDebugFile("SanityPrep.debug")
#endif
  {
  }

  ~StmtBlockContext()
  {
    for (UtList<StmtBlock*>::reverse_iterator itr = mBlockStack.rbegin();
         itr != mBlockStack.rend(); ++itr) {
      delete *itr;
    }
  }

  // Push a sequential block context.
  void pushContext(NUBase* block, PplLocalConstProp::StmtBlockT blockType)
  {
    StmtBlock* parentBlock = NULL;
    if (!isEmpty()) {
      parentBlock = mBlockStack.back();
    }

    StmtBlock* stmtBlock = new StmtBlock(block, blockType, mFold, mIODB, parentBlock, mAliasMap);
    mBlockStack.push_back(stmtBlock);
    stmtBlock->begin();

    if(blockType != PplLocalConstProp::eIfElse)
    {
      // Mark the block as known.
      mKnownNodes.insert(UtMap<NUBase*,SInt32>::value_type(block,
                                                           stmtBlock->getIndex()));
    }
#ifdef DUMP_SANITY_CHECK
    if ((blockType == PplLocalConstProp::eTask) ||
        (blockType == PplLocalConstProp::eStrucProc))
    {
      sDumpSeqBlock("Push: ", block, mDebugFile);
    }
#endif
  }

  // Pop a sequential block context.
  void popContext(NUBase* nextForBlock)
  {
    NU_ASSERT(!mBlockStack.empty(), nextForBlock);
    StmtBlock* stmtBlock = mBlockStack.back();
    mBlockStack.pop_back();
    if (!isEmpty()) {
      mBlockStack.back()->poppedOutChild(stmtBlock);
    }
    stmtBlock->end();
    delete stmtBlock;
#ifdef DUMP_SANITY_CHECK
    if ((stmtBlock->getType() == PplLocalConstProp::eTask) ||
        (stmtBlock->getType() == PplLocalConstProp::eStrucProc))
    {
      sDumpSeqBlock("Pop: ", stmtBlock->getBlock(), mDebugFile);
    }
#endif
  }

  //! Add a newly populated statement to current block context.
  void add(NUStmt* stmt, NUNetSet* forStmtDefs, NUScope* declScope) { 
    if (!isEmpty())
    {
      mBlockStack.back()->add(stmt, forStmtDefs, declScope);
      mKnownNodes.insert(UtMap<NUBase*,SInt32>::value_type(stmt, getIndex()));
#ifdef DUMP_SANITY_CHECK
      sDumpStmtOrCaseItem(stmt, mDebugFile);
      mDebugFile << "Index: " << getIndex() << UtIO::endl;
#endif
    }
  }

  //! Add a newly created case item to current block context.
  void add(NUCaseItem* item) {
    if (!isEmpty())
    {
      mBlockStack.back()->add(item);
      mKnownNodes.insert(UtMap<NUBase*,SInt32>::value_type(item, getIndex()));
#ifdef DUMP_SANITY_CHECK
      sDumpStmtOrCaseItem(item, mDebugFile);
      mDebugFile << "Index: " << getIndex() << UtIO::endl;
#endif
    }
  }

  void addCopies(NUStmtList* stmts) {
    if (!isEmpty())
    {
      for (NUStmtListIter itr = stmts->begin(); itr != stmts->end(); ++itr)
      {
        NUStmt* the_stmt = *itr;
        mKnownNodes.insert(
          UtMap<NUBase*,SInt32>::value_type(the_stmt,
                                            PplSanityChecker::cIgnoreStmtIndex));
        mBlockStack.back()->addCopy(the_stmt);
      }
    }
  }

  //! Mark indicated nucleus object for deletion. It will be deleted once
  //! sanity check is complete.
  void markForDeletion(NUBase* deletedNode) {
    mMarkedForDelete.insert(deletedNode);
  }

  //! Enable/Disable ignoring statements added henceforth.
  void setIgnoreStmts(bool ignoreStmts) {
    if (!isEmpty()) {
      mBlockStack.back()->setIgnoreStmts(ignoreStmts);
    }
  }

  PplLocalConstProp::StmtBlockT getType() const {
    if (isEmpty()) {
      return PplLocalConstProp::eNotBlock;
    }
    return mBlockStack.back()->getType();
  }

  NUBase* getBlock() const {
    if (isEmpty()) {
      return NULL;
    }
    return mBlockStack.back()->getBlock();
  }

  bool isEmpty() const { return mBlockStack.empty(); }
  bool getIgnoreStmts() const { return mBlockStack.back()->getIgnoreStmts(); }

  const UtMap<NUBase*,SInt32>* getKnownNodes() const { return &mKnownNodes; }
  const UtSet<NUBase*>* getMarkedForDelete() const { return &mMarkedForDelete; }

  NUExpr* optimizeExpr(NUExpr* expr) {
    if (!isEmpty()) {
      return mBlockStack.back()->optimizeExpr(expr);
    }
    return NULL;
  }

  const SourceLocator* getLoc(NUBase* block)
  {
    // The block would either be a statement.
    NUStmt* stmt = dynamic_cast<NUStmt*>(block);
    if (stmt != NULL) {
      return &(stmt->getLoc());
    }

    // Or a case item
    NUCaseItem* item = dynamic_cast<NUCaseItem*>(block);
    if (item != NULL) {
      return &(item->getLoc());
    }

    // Or a structure proc
    NUStructuredProc* sp = dynamic_cast<NUStructuredProc*>(block);
    if (sp != NULL) {
      return &(sp->getLoc());
    }

    // Or a task
    NUTask* task = dynamic_cast<NUTask*>(block);
    if (task != NULL) {
      return &(task->getLoc());
    }
    return NULL;
  }

private:

  SInt32 getIndex() { return mBlockStack.back()->getIndex(); }

  UtList<StmtBlock*> mBlockStack;
  UtMap<NUBase*,SInt32> mKnownNodes;
  UtSet<NUBase*> mMarkedForDelete;

  Fold* mFold;
  IODBNucleus* mIODB;
  VhdlPopulate::EntityAliasMap* mAliasMap;   // Map of vhdl Aliases
#ifdef DUMP_SANITY_CHECK
  UtOFStream mDebugFile;
#endif
};


PplStmtFactory::PplStmtFactory(Fold* fold, NUNetRefFactory* factory,
                               IODBNucleus* iodb, ArgProc* args,
                               MsgContext* msgContext,
                               VhdlPopulate::EntityAliasMap* alias_map)
  : mFold(fold),
    mFactory(factory),
    mIODB(iodb),
    mMsgContext(msgContext),
    mBlockContext(NULL),
    mCurrentDeclarationScope(NULL),
    mIsConstPropEnabled(true),
    mAliasMap(alias_map)
{
  mBlockContext = new StmtBlockContext(mFold, iodb, mAliasMap);
  mIsConstPropEnabled = !args->getBoolValue(JaguarContext::scNoPplConstProp);
}

PplStmtFactory::~PplStmtFactory()
{
  delete mBlockContext;
}

NUTask* 
PplStmtFactory::createTask(NUModule *parent, STBranchNode * name_branch,
                           const SourceLocator& loc)
{
  NUTask* task = new NUTask(parent, name_branch, mFactory, loc, mIODB);
  addSeqScope(task, PplLocalConstProp::eTask);
  return task;
}

NUTask*
PplStmtFactory::createTask(NUModule *parent, StringAtom* name,
                           StringAtom* origName, const SourceLocator& loc)
{
  NUTask* task = new NUTask(parent, name, origName, mFactory, loc, mIODB);
  addSeqScope(task, PplLocalConstProp::eTask);
  return task;
}
  
NUAlwaysBlock*
PplStmtFactory::createAlwaysBlockForJaguarFlow(StringAtom * name, const SourceLocator& loc)
{
  NUAlwaysBlock* block = new NUAlwaysBlock(name, NULL, mFactory, loc, false);
  addSeqScope(block, PplLocalConstProp::eStrucProc);
  return block;
}

NUInitialBlock*
PplStmtFactory::createInitialBlock(StringAtom * name, const SourceLocator& loc)
{
  NUInitialBlock* block = new NUInitialBlock(name, NULL, mFactory, loc);
  addSeqScope(block, PplLocalConstProp::eStrucProc);
  return block;
}

NUBlockingAssign* 
PplStmtFactory::createBlockingAssign(NULvalue* lvalue, NUExpr* rvalue,
                                     bool usesCFNet, const SourceLocator& loc,
                                     bool do_resize)
{
  NUExpr* opt_rvalue = optimizeExpr(rvalue);
  if (opt_rvalue != NULL) {
    rvalue = opt_rvalue;
  }
 
  NUBlockingAssign* stmt = 
    new NUBlockingAssign(lvalue, rvalue, usesCFNet, loc, do_resize);
  addStmt(stmt);
  return stmt;
}

NUNonBlockingAssign* 
PplStmtFactory::createNonBlockingAssign(NULvalue *lvalue, NUExpr *rvalue,
                                        bool usesCFNet,
                                        const SourceLocator& loc,
                                        bool do_resize)
{
  NUNonBlockingAssign* stmt = 
    new NUNonBlockingAssign(lvalue, rvalue, usesCFNet, loc, do_resize);
  addStmt(stmt);
  return stmt;
}

NUContAssign* 
PplStmtFactory::createContAssign(StringAtom *name, NULvalue *lvalue,
                                 NUExpr *rvalue, const SourceLocator& loc,
                                 Strength strength, bool do_resize)
{
  NUContAssign* stmt = 
    new NUContAssign(name, lvalue, rvalue, loc, strength, do_resize);
  // Continuous statements are always outside the sequential scope.
  //addStmt(stmt);
  return stmt;
}

NUIf* PplStmtFactory::createIf(NUExpr *cond, bool usesCFNet, const SourceLocator& loc)
{
  NUStmtList then_stmts;
  NUStmtList else_stmts;
  NUIf* stmt = new NUIf(cond, then_stmts, else_stmts, mFactory,
                        usesCFNet, loc);
  addStmt(stmt);
  return stmt;
}


void PplStmtFactory::createElse(NUIf *else_stmt)
{
  if (!mBlockContext->isEmpty() && !mBlockContext->getIgnoreStmts())
  {
    mBlockContext->pushContext(else_stmt, PplLocalConstProp::eIfElse);
  }
}


NUCase* PplStmtFactory::createCase(NUExpr* sel, NUCase::CaseType type, bool usesCFNet,
                                   const SourceLocator& loc)
{
  NUCase* stmt = new NUCase(sel, mFactory, type, usesCFNet, loc);
  addStmt(stmt);
  return stmt;
}

NUFor* PplStmtFactory::createFor(NUExpr* condition, bool usesCFNet,
                                 const SourceLocator & loc, NUNetSet& forStmtDefs)
{
  NUStmtList initial;
  NUStmtList advance;
  NUStmtList body;
  NUFor* stmt = new NUFor(initial, condition, advance, body, mFactory,
                          usesCFNet, loc);
  addStmt(stmt, &forStmtDefs);
  return stmt;
}

NUBlock* PplStmtFactory::createBlock(NUScope * parent, bool uses_cf_net,
                                     const SourceLocator & loc)
{
  NUBlock* stmt = new NUBlock(NULL, parent, mIODB, mFactory, uses_cf_net, loc);
  addStmt(stmt);
  return stmt;
}

NUTaskEnable* PplStmtFactory::createTaskEnable(NUTask *task,
                                               NUTFArgConnectionVector& argConns,
                                               NUModule* parentModule,
                                               bool usesCFNet,
                                               const SourceLocator& loc)
{
  NUTaskEnable* stmt = new NUTaskEnable(task, parentModule, mFactory,
                                        usesCFNet, loc);
  stmt->setArgConnections(argConns);
  addStmt(stmt);
  return stmt;
}

NUTaskEnableHierRef* 
PplStmtFactory::createTaskEnableHierRef(const AtomArray& path,
                                        StringAtom *taskName, NUScope *scope,
                                        bool usesCFNet,
                                        const SourceLocator &loc)
{
  NUTaskEnableHierRef* stmt = 
    new NUTaskEnableHierRef(path, taskName, scope, mFactory, usesCFNet, loc);
  addStmt(stmt);
  return stmt;
}

NUBreak* PplStmtFactory::createBreak(NUBlock* block, StringAtom* keyword, StringAtom* targetName,
                                     const SourceLocator& loc)
{
  NUBreak* stmt = new NUBreak(block, keyword, targetName, mFactory, loc);
  addStmt(stmt);
  return stmt;
}

NUReadmemX* 
PplStmtFactory::createReadmemX(StringAtom* name, NUExprVector& exprs,
                               const NUModule* module, NULvalue *lvalue,
                               StringAtom* file, bool hexFormat, SInt64 startAddress,
                               SInt64 endAddress, bool endSpecified,
                               bool usesCFNet, const SourceLocator& loc)
{
  NUReadmemX* stmt = new NUReadmemX(name, exprs, module, lvalue,
                                    file, hexFormat, startAddress,
                                    endAddress, endSpecified,
                                    mFactory, usesCFNet, loc);
  addStmt(stmt);
  return stmt;
}

NUOutputSysTask* 
PplStmtFactory::createOutputSysTask(StringAtom* name, NUExprVector& exprs,
                                    const NUModule* module, bool hasFileSpec,
                                    bool appendNewline, SInt8 timeUnit,
                                    SInt8 timePrecision,
                                    bool usesCFNet, bool isVerilogTask,
                                    bool isWriteLineTask, bool hasInstanceName,
                                    STBranchNode* where, const SourceLocator& loc)
{
  NUOutputSysTask* stmt = new NUOutputSysTask(name, exprs, module, hasFileSpec,
                                              appendNewline, timeUnit, timePrecision,
                                              mFactory, usesCFNet, isVerilogTask,
                                              isWriteLineTask, hasInstanceName,
                                              where, loc);
  addStmt(stmt);
  return stmt;
}

NUFCloseSysTask* 
PplStmtFactory::createFCloseSysTask(StringAtom* name, NUExprVector& exprs,
                                    const NUModule* module,
                                    bool usesCFNet, bool isVerilogTask,
                                    bool useInputFileSystem,
                                    const SourceLocator& loc)
{
  NUFCloseSysTask* stmt = 
    new NUFCloseSysTask(name, exprs, module, mFactory, usesCFNet, isVerilogTask,
                        useInputFileSystem, loc);
  addStmt(stmt);
  return stmt;
}

NUFOpenSysTask* 
PplStmtFactory::createFOpenSysTask(StringAtom* name, NUNet* outNet,
                                   NUIdentLvalue* statusLvalue,
                                   NUExprVector& exprs, const NUModule* module,
                                   bool usesCFNet,
                                   bool isVerilogTask, bool useInputFileSystem,
                                   const SourceLocator& loc)
{
  NUFOpenSysTask* stmt = 
    new NUFOpenSysTask(name, outNet, statusLvalue, exprs, module,
                       mFactory, usesCFNet, isVerilogTask, useInputFileSystem, loc);
  addStmt(stmt);
  return stmt;
}

NUInputSysTask* 
PplStmtFactory::createInputSysTask(StringAtom* name, NUExprVector& exprs,
                                   NULvalue *lvalue, NULvalue *statusLvalue,
                                   const NUModule* module,
                                   bool usesCFNet, bool isVerilogTask,
                                   bool isReadLineTask, const SourceLocator& loc)
{
  NUInputSysTask* stmt = new NUInputSysTask(name, exprs, lvalue, statusLvalue,
                                            module, mFactory, usesCFNet,
                                            isVerilogTask, isReadLineTask, loc);
  addStmt(stmt);
  return stmt;
}

NUFFlushSysTask* 
PplStmtFactory::createFFlushSysTask(StringAtom* name, NUExprVector& exprs,
                                    const NUModule* module,
                                    bool usesCFNet, const SourceLocator& loc)
{
  NUFFlushSysTask* stmt = new NUFFlushSysTask(name, exprs, module, mFactory,
                                              usesCFNet, loc);
  addStmt(stmt);
  return stmt;
}

NUControlSysTask* 
PplStmtFactory::createControlSysTask(StringAtom* name,
                                     CarbonControlType controlType,
                                     NUExprVector& exprs, const NUModule* module,
                                     bool usesCFNet, const SourceLocator& loc)
{
  NUControlSysTask* stmt = new NUControlSysTask(name, controlType, exprs,
                                                module, mFactory, usesCFNet, loc);
  addStmt(stmt);
  return stmt;
}

NUCaseItem*
PplStmtFactory::createCaseItem(const SourceLocator& loc)
{
  NUCaseItem* item = new NUCaseItem(loc);
  addItem(item);
  return item;
}

void PplStmtFactory::copyStmtList(NUStmtList& srcList, NUStmtList* dstList,
                                  CopyContext cc)
{
  // First copy the statements.
  deepCopyStmtList(srcList, dstList, cc, NULL);
  if (!mBlockContext->isEmpty() && !mBlockContext->getIgnoreStmts()) {
    mBlockContext->addCopies(dstList);
  }
}

void PplStmtFactory::addSeqScope(NUBase* scope, PplLocalConstProp::StmtBlockT scopeType)
{
  mBlockContext->pushContext(scope, scopeType);
}

void PplStmtFactory::addItem(NUCaseItem* item)
{
  if (!mBlockContext->isEmpty() && !mBlockContext->getIgnoreStmts())
  {
    // The current context state better be case statement population.
    NU_ASSERT(mBlockContext->getType() == PplLocalConstProp::eCase, item);

    // Add case item to case statement.
    mBlockContext->add(item);

    // Case item is a statement block. Push it onto stack.
    mBlockContext->pushContext(item, PplLocalConstProp::eCaseItem);
  }
}

void PplStmtFactory::addStmt(NUStmt* stmt, NUNetSet* forStmtDefs)
{
  // Sequential statement block statements.
  if (!mBlockContext->isEmpty() && !mBlockContext->getIgnoreStmts())
  {
    // Add statement to current statement block.
    mBlockContext->add(stmt, forStmtDefs, mCurrentDeclarationScope);

    // If statement is a statement block itself, push it onto stack.
    switch (stmt->getType())
    {
    case eNUIf:
      mBlockContext->pushContext(stmt, PplLocalConstProp::eIfThen);
      break;
    case eNUCase:
      mBlockContext->pushContext(stmt, PplLocalConstProp::eCase);
      break;
    case eNUFor:
      mBlockContext->pushContext(stmt, PplLocalConstProp::eForInitial);
      break;
    case eNUBlock:
      mBlockContext->pushContext(stmt, PplLocalConstProp::eBlock);
      break;
    default:
      break;
    }
  }
}

bool PplStmtFactory::next(NUBase* nextForBlock)
{
  if (!mBlockContext->getIgnoreStmts())
  {
    NUBase* block = mBlockContext->getBlock();
    if (nextForBlock != block) {
      const SourceLocator* expLoc = mBlockContext->getLoc(block);
      UtString expect;
      expLoc->compose(&expect);
      expect << " Type: " << block->typeStr();
      const SourceLocator* actLoc = mBlockContext->getLoc(nextForBlock);
      UtString actual;
      actLoc->compose(&actual);
      actual << " Type: " << nextForBlock->typeStr();
      mMsgContext->StmtFactoryOutOfSync(expect.c_str(), actual.c_str());
      return false;
    }
    NU_ASSERT(nextForBlock == block, block);
    PplLocalConstProp::StmtBlockT poppedOutBlockType = mBlockContext->getType();
    mBlockContext->popContext(nextForBlock);

    switch(poppedOutBlockType)
    {
    case PplLocalConstProp::eForInitial:
    {
      // Add a new statement context for the for loop body for the same for statement.
      mBlockContext->pushContext(block, PplLocalConstProp::eForBody);
      break;
    }
    case PplLocalConstProp::eForBody:
    {
      // Add a new statement context for for loop body with same for statement.
      mBlockContext->pushContext(block, PplLocalConstProp::eForAdvance);
      break;
    }
    default:
    {
      break;
    }
    }
  } // if
  return true;
}

void PplStmtFactory::removeCaseItem(NUCase* caseStmt, NUCaseItem* caseItem)
{
  if (mBlockContext->isEmpty()) {
    caseStmt->removeItem(caseItem);
    delete caseItem;
  } else {
    mBlockContext->markForDeletion(caseItem);
  }
}

void PplStmtFactory::outOfContextStmtsBegin()
{
  if (!mBlockContext->isEmpty()) {
    mBlockContext->setIgnoreStmts(true);
  }
}

void PplStmtFactory::outOfContextStmtsEnd()
{
  if (!mBlockContext->isEmpty()) {
    mBlockContext->setIgnoreStmts(false);
  }
}

bool PplStmtFactory::sanityCheckPopulatedNucleus(NUDesign* design)
{
  PplSanityChecker sanityChecker(mBlockContext->getKnownNodes(), 
                                 mBlockContext->getMarkedForDelete(), mMsgContext);
  NUDesignWalker walker(sanityChecker, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.design(design);
  return sanityChecker.checkPassed();
}

NUExpr* PplStmtFactory::optimizeExpr(NUExpr* expr)
{
  NUExpr* opt_expr = NULL;
  if (mIsConstPropEnabled && !mBlockContext->isEmpty() && expr != NULL) {
    opt_expr = mBlockContext->optimizeExpr(expr);
  }
  return opt_expr;
}

void PplStmtFactory::setCurrentDeclarationScope(NUScope* declScope)
{
  mCurrentDeclarationScope = declScope;
}

LFContext::LFContext(STSymbolTable *symtab,
		     AtomicCache *str_cache,
		     SourceLocatorFactory *loc_factory,
		     FLNodeFactory *flow_factory,
		     FLNodeElabFactory *flow_elab_factory,
		     NUNetRefFactory *netref_factory,
                     ArgProc* args,
		     MsgContext *msgContext,
		     IODBNucleus* iodb,
                     Fold* fold,
                     VhdlPopulate::EntityAliasMap* alias_map,
                     VHDLCaseT caseMode) :
  mSymtab(symtab),
  mStrCache(str_cache),
  mLocFactory(loc_factory),
  mFlowFactory(flow_factory),
  mFlowElabFactory(flow_elab_factory),
  mNetRefFactory(netref_factory),
  mMsgContext(msgContext),
  mDesign(0),
  mIODB(iodb),
  mRootModule(0),
  mRequiresSensitivityEvents(0),
  mCoercePorts(false),
  mShareConstants(false),
  mInlineTasks(false),
  mCheckForPullConflicts(false),
  mCaseMode(caseMode),
  mStmtFactory(fold, netref_factory, iodb, args, msgContext, alias_map),
  mAliasMap(alias_map)
{
  mCoercePorts = args->getBoolValue(CRYPT("-coercePorts"));
  mShareConstants = args->getBoolValue(CRYPT("-shareConstants"));
  mInlineTasks = args->getBoolValue(CRYPT("-inlineTasks"));
  mCheckForPullConflicts = args->getBoolValue(CRYPT("-noCheckPullConflicts"));

  mSynth = args->getBoolValue("-synth");

  mChangeDetects = new ChangeDetectMap;
  mLastVisitedModule = NULL;
}

LFContext::~LFContext()
{
  // if we have recovered from a parsing error then the mEdgeNetMapStack may not be empty, clean it up now
  while (not mEdgeNetMapStack.empty()) {
    popEdgeNetMap();
  }

  delete mChangeDetects;
}

/*
 * Initialize this context for a design walk.
 */
void LFContext::initForDesignWalk()
{
  mVisitedModules.clear();
}


void LFContext::markVisited(NUModule* module)
{
  mVisitedModules.insert(module);
}

bool LFContext::queryVisited(NUModule* module)
{
  return (mVisitedModules.find(module) != mVisitedModules.end());
}

void LFContext::setDesign(NUDesign *design)
{
  ASSERT(mDesign == 0);
  mDesign = design;
}

NUDesign* LFContext::getDesign() const
{
  ASSERT(mDesign);
  return mDesign;
}

void LFContext::pushBlockScope(NUScope *scope)
{
  BLOCK_CHECK_ASSERT(scope);
  mBlockScopeStack.push(scope);
}

NUScope* LFContext::popBlockScope()
{
  NUScope *scope = mBlockScopeStack.top();
  mBlockScopeStack.pop();
  return scope;
}

NUScope* LFContext::getBlockScope() const
{
  if (mBlockScopeStack.empty()) {
    return 0;
  } else {
    return mBlockScopeStack.top();
  }
}


void LFContext::pushDeclarationScope(NUScope *scope)
{
  DECL_CHECK_ASSERT(scope);
  mDeclarationScopeStack.push(scope);
  mStmtFactory.setCurrentDeclarationScope(getDeclarationScope());
}

NUScope* LFContext::popDeclarationScope()
{
  NUScope *scope = mDeclarationScopeStack.top();
  // keep track of the most recent NUModule that has been
  // popped. We use this in the NucleusInterraCB to populate instances
  if (scope && (scope->getParentScope() == NULL))
    // This is a module that we are popping
    putLastVisitedModule(scope->getModule());
  
  mDeclarationScopeStack.pop();
  mStmtFactory.setCurrentDeclarationScope(getDeclarationScope());
  return scope;
}

void LFContext::putLastVisitedModule(NUModule* module)
{
  mLastVisitedModule = module;
}

NUScope* LFContext::getDeclarationScope() const
{
  if (mDeclarationScopeStack.empty()) {
    return 0;
  } else {
    return mDeclarationScopeStack.top();
  }
}

NUModule *LFContext::getRootModule() const
{
  return mRootModule;
}

NUModule* LFContext::getLastVisitedModule() const
{ 
  return mLastVisitedModule; 
}

NUModule *LFContext::maybeSetRootModule( NUModule *module )
{
  if ( mRootModule == NULL )
    mRootModule = module;
  return mRootModule;
}


NUModule* LFContext::getModule() const
{
  NUScope *scope = getDeclarationScope();

  if (scope == 0) {
    return 0;
  } else {
    return scope->getModule();
  }
}


void
LFContext::pushJaguarScope( vhNode vh_scope )
{
  VhObjType objType = vhGetObjType( vh_scope );
  if ( objType == VHFOR || objType == VHWHILE || objType == VHLOOP )
  {
    // All constant propagation bets are off if we enter a loop scope
    mJaguarScopeStack.top()->doVariableCleanup();
  }
  JaguarScope *cur_scope = NULL;
  if (!mJaguarScopeStack.empty())
    cur_scope = mJaguarScopeStack.top();
  JaguarScope *new_scope = new JaguarScope( vh_scope );
  // If current scope is in trailing reset process scope, then the new one is too.
  if ((cur_scope != NULL) && cur_scope->isTrailingResetProcessScope())
    new_scope->setIsTrailingResetProcessScope();
  mJaguarScopeStack.push( new_scope );
}

vhNode
LFContext::popJaguarScope()
{
  INFO_ASSERT(not mJaguarScopeStack.empty(), "Fatal VHDL parser scope stack error--stack is empty." );
  JaguarScope *scope = mJaguarScopeStack.top();
  scope->restoreInitialValues();
  mJaguarScopeStack.pop();
  vhNode vh_scope = scope->getVhScope();
  delete scope;
  return vh_scope;
}

void LFContext::getJaguarNodeStack(SInt32 stackLimit,
                                   UtList<vhNode>& node_stack) const
{
  UtStack<JaguarScope*> stack_cpy(mJaguarScopeStack);
  while ((--stackLimit >= 0) && (!stack_cpy.empty()))
  {
    JaguarScope* js = stack_cpy.top();
    stack_cpy.pop();
    // Add node to the list in the order they were on stack.
    vhNode jn = js->getVhScope();
    node_stack.push_back(jn);
  }
}

void LFContext::setIsTrailingResetProcessScope()
{
  JaguarScope *scope = mJaguarScopeStack.top();
  scope->setIsTrailingResetProcessScope();
}

bool LFContext::isTrailingResetProcessScope()
{
  JaguarScope *scope = mJaguarScopeStack.top();
  return scope->isTrailingResetProcessScope();
}

vhNode
LFContext::getJaguarScope() const
{
  if (mJaguarScopeStack.empty()) {
    return 0;
  } else {
    return mJaguarScopeStack.top()->getVhScope();
  }
}

void
LFContext::addJaguarUninitVariable( vhNode vh_var )
{
  INFO_ASSERT( !mJaguarScopeStack.empty(), "Fatal VHDL parser scope stack error." );
  mJaguarScopeStack.top()->addCleanupVariable( vh_var );
}


void
LFContext::saveJaguarInitialVariableValue( vhNode vh_decl, vhExpr vh_value )
{
  INFO_ASSERT( !mJaguarScopeStack.empty(), "Fatal VHDL parser scope stack error." );
  mJaguarScopeStack.top()->saveInitialValue( vh_decl, vh_value );
}


void
LFContext::saveJaguarInitialVariableValues( JaguarList* decl_list, bool isRecursive )
{
  INFO_ASSERT( !mJaguarScopeStack.empty(), "Fatal VHDL parser scope stack error." );
  mJaguarScopeStack.top()->saveInitialValues( decl_list, isRecursive, false );
}

void LFContext::saveRecursiveFuncCallArgInitValues(vhNode vh_function)
{
  // List of function arguments.
  JaguarList vh_arg_iter(vhGetFlatParamList(vh_function));
  // List to hold parameters assigned to function arguments in function call.
  JaguarList vh_call_arg_list = vhwCreateNewList(VH_TRUE);
  vhNode vh_arg = 0;
  while ((vh_arg = vhGetNextItem(vh_arg_iter)) != NULL)
  {
    vhNode vh_call_arg = vhGetInitialValue(vh_arg); // parameter
    vhObjType objType = vhGetObjType(vh_call_arg);
    if (objType == VHOBJECT) {
      vh_call_arg = vhGetActualObj(vh_call_arg);
    }
    vhwAppendToList(vh_call_arg_list, vh_call_arg);
  }
  mJaguarScopeStack.top()->saveInitialValues(&vh_call_arg_list, true,
                                             true /*allow signals*/);
}

void
LFContext::restoreJaguarInitialVariableValues()
{
  INFO_ASSERT( !mJaguarScopeStack.empty(), "Fatal VHDL parser scope stack error." );
  mJaguarScopeStack.top()->restoreInitialValues();
}

void LFContext::addEdgeExpr(NUEdgeExpr *expr)
{
  NU_ASSERT(not mEdgeListStack.empty(), expr);
  mEdgeListStack.top()->push_back(expr);
}

NUEdgeExprList* LFContext::getEdgeExprList()
{
  ASSERT(not mEdgeListStack.empty());
  return mEdgeListStack.top();
}

void LFContext::pushEdgeExprList(NUEdgeExprList *expr_list)
{
  mEdgeListStack.push(expr_list);
}

NUEdgeExprList* LFContext::popEdgeExprList()
{
  ASSERT(not mEdgeListStack.empty());
  NUEdgeExprList *ret = mEdgeListStack.top();
  mEdgeListStack.pop();
  return ret;
}

void LFContext::addEventExpr(NUExpr *expr, const SourceLocator &use_loc)
{
  NU_ASSERT(not mEventListStack.empty(), expr);
  mEventListStack.top()->push_back(NUExprLocPair(expr,use_loc));
}

NUExprLocList* LFContext::getEventExprList()
{
  ASSERT(not mEventListStack.empty());
  return mEventListStack.top();
}

void LFContext::pushEventExprLocList(NUExprLocList *expr_loc_list)
{
  mEventListStack.push(expr_loc_list);
}

NUExprLocList* LFContext::popEventExprLocList()
{
  ASSERT(not mEventListStack.empty());
  NUExprLocList *ret = mEventListStack.top();
  mEventListStack.pop();
  return ret;
}

void LFContext::addStmt(NUStmt *stmt)
{
  NU_ASSERT(not mStmtListStack.empty(), stmt);
  mStmtListStack.top()->push_back(stmt);
}

void LFContext::addStmts(NUStmtList *stmts)
{
  ASSERT(not mStmtListStack.empty());
  mStmtListStack.top()->insert(mStmtListStack.top()->end(), stmts->begin(), stmts->end());
}

NUStmtList* LFContext::getStmtList()
{
  ASSERT(not mStmtListStack.empty());
  return mStmtListStack.top();
}

void LFContext::pushStmtList(NUStmtList *stmt_list)
{
  mStmtListStack.push(stmt_list);
}

NUStmtList* LFContext::popStmtList()
{
  ASSERT(not mStmtListStack.empty());
  NUStmtList *ret = mStmtListStack.top();
  mStmtListStack.pop();
  return ret;
}

bool LFContext::isStmtStackEmpty() const
{
  return mStmtListStack.empty();
}

void LFContext::pushExtraTaskEnableContext()
{
  NUStmtList* new_list = new NUStmtList();
  mExtraTaskEnablesStack.push(new_list);
}

void LFContext::popExtraTaskEnableContext()
{
  ASSERT(not mExtraTaskEnablesStack.empty());
  NUStmtList * the_list = mExtraTaskEnablesStack.top();
  ASSERT(the_list->empty());  mExtraTaskEnablesStack.pop();
  delete the_list;
}

void LFContext::addExtraTaskEnable(NUStmt * task_enable)
{
  NU_ASSERT( not mExtraTaskEnablesStack.empty(), task_enable );
  NUStmtList * current_list = mExtraTaskEnablesStack.top();
  current_list->push_back(task_enable);
}


NUStmt * LFContext::extractAnExtraTaskEnable()
{
  ASSERT(not mExtraTaskEnablesStack.empty());
  NUStmtList * current_list = mExtraTaskEnablesStack.top();
  if ( current_list->empty() ){
    return NULL;
  }
  NUStmt * task_enable = current_list->front();
  if ( task_enable ){
    current_list->pop_front();
  }
  return task_enable;
}

void LFContext::pushFlowMaps(FLNodeMultiMap *use_map, FLNodeMap *def_map,
			     FLNodeMultiMap *edge_use_map)
{
  mUseStack.push(use_map);
  mEdgeUseStack.push(edge_use_map);
  mDefStack.push(def_map);
}

FLNodeMultiMap *LFContext::getUseMap()
{
  return mUseStack.top();
}

FLNodeMultiMap *LFContext::getEdgeUseMap()
{
  return mEdgeUseStack.top();
}

FLNodeMap *LFContext::getDefMap()
{
  return mDefStack.top();
}

void LFContext::popFlowMaps()
{
  mUseStack.pop();
  mEdgeUseStack.pop();
  mDefStack.pop();
}

NUModule* LFContext::lookupModule(StringAtom* name) const
{
  UtMap<StringAtom*, NUModule*, StringAtom_less>::const_iterator p = mModuleMap.find(name);
  if (p != mModuleMap.end()) {
    return p->second; // Can't use [] due to const blech
  } else {
    return 0;
  }
}

void LFContext::mapModule(StringAtom* name, NUModule* module)
{
  NU_ASSERT(mModuleMap.find(name) == mModuleMap.end(), module);
  mModuleMap[name] = module;
}

void LFContext::clearTFVisitSet()
{
  mTFVisitSet.clear();
}


bool LFContext::getTFVisit(veNode ve_node) const
{
  return (mTFVisitSet.find(ve_node) != mTFVisitSet.end());
}


void LFContext::addTFVisit(veNode ve_node)
{
  mTFVisitSet.insert(ve_node);
}


void LFContext::removeTFVisit(veNode ve_node)
{
  mTFVisitSet.erase(ve_node);
}


bool LFContext::atTopHierLevel() const
{
  return mHierStack.empty();
}

STBranchNode* LFContext::popHier()
{
  STBranchNode *top = mHierStack.top();
  mHierStack.pop();
  return top;
}

STBranchNode* LFContext::getHier() const
{
  return mHierStack.top();
}

void LFContext::pushHier(STBranchNode* node)
{
  mHierStack.push(node);
}

//! called by associative table for compare
bool LFContext::StringAtom_less::operator() (const StringAtom* p,
                                             const StringAtom* q) const 
{
  return strcmp(p->str(), q->str()) < 0;
}

void LFContext::putEdgeNet(NUExpr* edgeExpr, NUNet* net)
{
  ASSERT(not mEdgeNetMapStack.empty());
  NUEdgeNetMap *net_map = mEdgeNetMapStack.top();
  (*net_map)[edgeExpr] = net;
}

//! Find the net associated with an edge-expression
NUNet* LFContext::getEdgeNet(NUExpr* edgeExpr)
{
  ASSERT(not mEdgeNetMapStack.empty());
  NUEdgeNetMap *net_map = mEdgeNetMapStack.top();
  return (*net_map)[edgeExpr];
}

void LFContext::pushEdgeNetMap()
{
  NUEdgeNetMap *net_map = new NUEdgeNetMap();
  mEdgeNetMapStack.push(net_map);
}

void LFContext::popEdgeNetMap()
{
  ASSERT(not mEdgeNetMapStack.empty());
  NUEdgeNetMap *net_map = mEdgeNetMapStack.top();
  mEdgeNetMapStack.pop();
  delete net_map;
}



SInt32 LFContext::numEdgeNets()
  const
{
  ASSERT(not mEdgeNetMapStack.empty());
  NUEdgeNetMap *net_map = mEdgeNetMapStack.top();
  return net_map->size();
}

void LFContext::mapTopLevelPortIndices()
{
  /*
    The top level ports have to be saved in order before any
    optimization to the IODB so that testdriver can have a properly
    ordered port declaration.
    This is needed because port splitting replaces ports and thus
    loses information
  */
  NUModuleList topMods;
  mDesign->getTopLevelModules(&topMods);
  INFO_ASSERT(topMods.size() == 1, "More than one top module exists.");
  NUModule* topMod = topMods.back();
  NUNetList portList;
  topMod->getPorts(&portList);
  mIODB->putPrimaryPorts(portList);
}

size_t LFContext::HashExpr::hash(const NUExpr* expr) const
{
  return expr->hash();
}

bool LFContext::HashExpr::lessThan(const NUExpr* expr1, const NUExpr* expr2)
  const
{
  return expr1 < expr2;
}

bool LFContext::HashExpr::lessThan1(const NUExpr* expr1, const NUExpr* expr2)
  const
{
  return expr1 < expr2;
}

bool LFContext::HashExpr::equal(const NUExpr* expr1, const NUExpr* expr2) const
{
  return *expr1 == *expr2;
}
NUNet* LFContext::findChangeDetectBool(NUExpr* expr) const
{
  ChangeDetectMap::iterator pos = mChangeDetects->find(expr);
  if (pos == mChangeDetects->end()) {
    return NULL;
  } else {
    return pos->second;
  }
}
  
void LFContext::addChangeDetectBool(NUExpr* expr, NUNet* net)
{
  mChangeDetects->insert(ChangeDetectMap::value_type(expr, net));
}

void LFContext::clearChangeDetects()
{
  mChangeDetects->clear();
}

StringAtom* LFContext::hdlIntern(const char* name, mvvLanguageType langType)
{
  StringAtom* ret = NULL;
  if (langType == MVV_VERILOG)
    ret = mStrCache->intern(name);
  else if (langType == MVV_VHDL)
    ret = vhdlIntern(name);
  return ret;
}

//! Intern a VHDL name -- handles lower-casing based on command-line arg
StringAtom* LFContext::vhdlIntern(const char* name) {
  StringAtom* atom = NULL;
  switch (mCaseMode)
  {
  case eVHDLLower:
    {
      UtString cpy(name);
      cpy.lowercase();
      atom = mStrCache->intern(cpy.c_str(), cpy.size());
    }
    break;
  case eVHDLPreserve:
    {
      // In this case we intern the case sensitive name.
      // We intern the lower case string as well for 
      // case insensitive search.
      atom = mStrCache->intern(name);
      UtString nameLower(name);
      nameLower.lowercase();
      mStrCache->intern(nameLower.c_str());
    }
    break;
  case eVHDLUpper:
    {
      UtString cpy(name);
      cpy.uppercase();
      atom = mStrCache->intern(cpy.c_str(), cpy.size());
    }
    break;
  } // switch
  ASSERT(atom);
  return atom;
} // StringAtom* LFContext::vhdlIntern

//! Intern a VHDL name -- handles lower-casing based on command-line arg
StringAtom* LFContext::vhdlGetIntern(const char* name) {
  StringAtom* atom = NULL;
  switch(mCaseMode)
  {
  case eVHDLLower:
    {
      UtString cpy(name);
      cpy.lowercase();
      atom = mStrCache->getIntern(cpy.c_str());
    }
    break;
  case eVHDLPreserve:
    {
      // First we search case sensitive way.
      // If we don't find, we look case insensitive way.
      atom = mStrCache->getIntern(name);
      if(atom == NULL)
      {
         UtString nameLower(name);
         nameLower.lowercase();
         atom = mStrCache->getIntern(nameLower.c_str());
      }
    }
    break;
  case eVHDLUpper:
    {
      UtString cpy(name);
      cpy.uppercase();
      atom = mStrCache->getIntern(cpy.c_str());
    }
    break;
  } // switch
  return atom;
}


//! Get the STBranchNode for the current scope, as determined by
//getDeclarationScope()
// TJM This goes away when inNewRecord is always true!
STBranchNode *
LFContext::getDeclScopeSymtabNode() const
{
  NUScope *scope = getDeclarationScope();
  return getScopeSymtabNode( scope );
}


//! Get the STBranchNode for the supplied scope
// TJM This goes away when inNewRecord is always true!
STBranchNode *
LFContext::getScopeSymtabNode( const NUScope *scope ) const
{
  // First, push the current scope elements onto a local stack, which
  // reverses them from bottom-up into the correct top-down order.
  NUScopeStack scopeStack;
  while ( scope && scope->getScopeType() != NUScope::eModule )
  {
    scopeStack.push( const_cast<NUScope*>( scope ));
    scope = scope->getParentScope();
  }
  // Now, unwind the local stack, looking up the next branch node down
  // as we go.  This results in the correct parent scope for the record
  // lookup being in 'parent'.
  STBranchNode *parent = NULL;
  NUModule *module = dynamic_cast<NUModule *>( const_cast<NUScope*> ( scope) );
  STSymbolTable *aliasDB = module->getModule()->getAliasDB();
  while ( !scopeStack.empty( ))
  {
    scope = scopeStack.top();
    scopeStack.pop();
    STSymbolTableNode *stnode = aliasDB->find( parent, scope->getName( ));
    // Some scopes will not have a corresponding symtab node--for
    // example, the scope for a VHDL process that has no local
    // declarations won't have one.  It might be possible for a subscope
    // of the process (or whatever) to have a symtab node for it, so
    // we'll keep looking through the rest of the local stack, but
    // keeping the current parent.
    if ( stnode != NULL )
      parent = stnode->castBranch();
  }
  return parent;
}

void LFContext::addDepositNet(NUNet *net)
{
  mDepositNets.insert(net);
}

void LFContext::addObserveNet(NUNet *net)
{
  mObserveNets.insert(net);
}


static void sApplyDirective(const char *directive, NUNet *net, IODBNucleus *iodb)
{
  UtString netName, text;
  net->composeUnelaboratedName(&netName);
  text << directive << " " << netName;
  StrToken dirTok(text.c_str());
  ++dirTok;
  iodb->parseDirective(directive, dirTok.curPos(), dirTok, net->getLoc());  
}

void LFContext::markDepositObserveNets()
{
  // Generate directives for the deposit and observable nets.
  // This causes the designations to persist across elaboration.

  for (NetSet::iterator iter = mDepositNets.begin(); iter != mDepositNets.end(); iter++)
  {
    NUNet *net = *iter;
    sApplyDirective("depositSignal", net, mIODB);
  }
  mDepositNets.clear();

  for (NetSet::iterator iter = mObserveNets.begin(); iter != mObserveNets.end(); iter++)
  {
    NUNet *net = *iter;
    sApplyDirective("observeSignal", net, mIODB);
  }
  mObserveNets.clear();
}

void LFContext::pushInstanceName(StringAtom* name)
{
  mInstanceNameStack.push(name);
}

StringAtom* LFContext::getInstanceName()
{
  return mInstanceNameStack.top();
}

void LFContext::popInstanceName()
{
  INFO_ASSERT(! mInstanceNameStack.empty(), "Instance Name Stack is empty.");
  mInstanceNameStack.pop();
}

void LFContext::pushPortMap(NUPortMap* portMap)
{
  mPortMapStack.push(portMap);
}

NUPortMap* LFContext::getPortMap()
{
  return mPortMapStack.top();
}

void LFContext::popPortMap()
{
  INFO_ASSERT(! mPortMapStack.empty(), "PortMap Stack is empty.");
  mPortMapStack.pop();
}


void
LFContext::JaguarScope::addCleanupVariable( vhNode vh_var )
{
  (void)mCleanupVariableSet.insert( vh_var );
}


void
LFContext::JaguarScope::doVariableCleanup()
{
  for ( UtSet<vhNode>::iterator p = mCleanupVariableSet.begin();
        p != mCleanupVariableSet.end();
        ++p )
  {
    unsetVariableInitialValue( *p );
  }
}


void
LFContext::JaguarScope::saveInitialValues( JaguarList *vh_decls, bool /*isRecursive*/,
                                           bool allowSignals)
{
  while ( vhNode vh_net = vhGetNextItem( *vh_decls ))
  {
    VhObjType objType = vhGetObjType( vh_net );
    if (( objType == VHVARIABLE ) || (allowSignals && (objType == VHSIGNAL)))
    {
      vhExpr vh_initial = vhGetInitialValue( vh_net );
      if ( vh_initial ) {
        mSavedInitialValuesMap[vh_net] = vh_initial;
      }
    }
  }
}

void
LFContext::JaguarScope::saveInitialValue( vhNode vh_net, vhExpr vh_value )
{
  VhObjType objType = vhGetObjType( vh_net );
  if ( objType == VHVARIABLE )
  {
    mSavedInitialValuesMap[vh_net] = vh_value;
    vhSetInitialValue( vh_net, NULL );
  }
}


void
LFContext::JaguarScope::restoreInitialValues()
{
  for ( UtMap<vhNode, vhExpr>::iterator p = mSavedInitialValuesMap.begin();
        p != mSavedInitialValuesMap.end();
        ++p )
  {
    vhNode vh_net = p->first;
    vhExpr vh_value = p->second;
    vhSetInitialValue( vh_net, vh_value );
  }
}

void LFContext::JaguarScope::setIsTrailingResetProcessScope()
{
  mIsTrailingResetProcessScope = true;
}

bool LFContext::JaguarScope::isTrailingResetProcessScope()
{
  return mIsTrailingResetProcessScope;
}
