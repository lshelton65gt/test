// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/BreakResynth.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUUseDefNode.h"
#include "util/UtHashMap.h"
#include "util/UtVector.h"

/*!
  \file
  Finds break-statements and resynthesizes them using block-scoped enable nets.

  Here is a simple example:

  BEFORE RESYNTH                  AFTER RESYNTH
  
  begin: b1                       begin: b1
                                    reg $b1_ena; $b1_ena = 1;
    for (i = 0; i < 10; i = i + 1)  for (i = 0; $b1_ena && (i < 10); i = i + 1)
      begin: b2                       begin: b2;
                                        reg b2_ena; $b2_ena = 1'b1;
        if (b[i] == 1'b0)               if (b[i] == 1'b0)
          disable b2;                     $b2_ena = 1'b0; // will be optimized away
                                        else begin // 'if ($b2_ena)' --> 'else'
        if (b[i] == 1'b1)                 if (b[i] == 1'b1)
          disable brk1;                     b1_ena = 1'b0;
                                          else
        a[i] = b[i];                        a[i] = b[i];
                                        end
      end // cont1                    end // cont1
  end // b1                       end // b1

  Note that the Verilog syntax is 'disable', the VHDL syntax is 'exit', or 
  'return' but we've picked the neutral 'C' keyword of 'break' to represent
  either concept.

  In the example above, you can see that every statement following a 'break'
  has to be conditionalized by the synthesized enable net.  This happens
  recursively up the syntax tree.  

  Also note that when the 'break' occurs as the last executable statement in
  an if's 'then' clause, then the rest of the statement-list at the level
  of the 'if' gets moved into the 'else' block.  At every lexical level
  above that 'if', continuance must be conditionalized on the 'ena' variable.

  Let's also consider a nested loop, and what it takes to break out of
  one or two levels.

  begin: b1                          begin: b1
                                       reg $b1_ena; $b1_ena = 1;
    for (i = 0; i < 10; i = i + 1)     for (i = 0; $b1_ena && (i < 10); i = i + 1)
      begin: b2                        begin: b2
                                         reg b2_ena; $b2_ena = 1'b1;
        for (j = 0; j < 10; j = j + 1)   for (j = 0; $b2_ena & (j < 10); j = j + 1)
        begin                            begin
          if (j == check1)                 if (j == check1) begin
            disable b1;                      $b1_ena = 1'b0; // turn off both
                                             $b2_ena = 1'b0; // blocks
                                           end
                                           if ($b2_ena) begin
          if (j == check2)                   if (j == check2)
            disable b2;                        $b2_ena = 1'b0;  // just the inner block
                                             if ($b2_ena)
          a[i][j] = b[i][j]                    a[i][j] = b[i][j];
        end                                end
      end                                end
                                       end
  end                                end
 */

#define NO_ACTIVE_BREAK (-1)

//! Information associated with each block that we traverse.
/*! This is kept on the stack as we recurse down the syntax tree.  If an
 *! enable net needs to be synthesized for a block, then it is kept
 *! in the BlockInfo so it can be re-used in case there are multiple
 *! disables to the same block.
 */
struct BreakResynth::BlockInfo {
  CARBONMEM_OVERRIDES

  BlockInfo(NUBlock* blk, SInt32 blockIndex):
    block(blk),
    enable(NULL),    // Create on demand for synthesized test in if or for
    index(blockIndex)
  {}

  NUBlock* block;
  NUNet* enable;              // enable-net created for the block if needed
  SInt32 index;               // index of the block in mBlockVector

  struct HashStringAtom: public HashPointer<StringAtom*> {
    bool lessThan(const StringAtom* s1, const StringAtom* s2) {
      return strcmp(s1->str(), s2->str()) < 0;
    }
  };
  typedef UtHashSet<StringAtom*,HashStringAtom> StringAtomSet;
  StringAtomSet mAnnotation;
};


BreakResynth::BreakResynth(NUNetRefFactory* nrf, MsgContext* mc):
  mNetRefFactory(nrf),
  mMsgContext(mc)
{
  mBreakIndex = NO_ACTIVE_BREAK;
  mBlockMap = new BlockMap;
  mBlockVector = new BlockVector;
  mModuleTasks = new TaskSet;
  mDesignTasksCovered = new TaskSet;
  mModule = NULL;
  mPass = eControlResynthesis;
}


BreakResynth::~BreakResynth() {
  delete mBlockMap;
  delete mBlockVector;
  delete mModuleTasks;
  delete mDesignTasksCovered;
}


// Iterate over a statement list, resynthesizing break statements into enable
// nets and if-statements, bottom-up.
//
// If this stmtList directly includes a 'break' anywhere underneath it, return
// true.  Even if there are no breaks, the statements are rewritten into
// *newStmts, and the caller is expected to replace the statement list in 
// whatever container he is looking at
void BreakResynth::stmtList(NUStmtLoop stmts,
                            BlockInfo* blockInfo,
                            NUStmtList* newStmts)
{
  bool breakFound = false;

  // Note that if any guarding needs to be done based on upstream
  // break statements, it was done a level *above* this call to
  // stmtList.  By saving the mBreakIndex state while recursing,
  // and merge it back later, we avoid pessimistic guards added
  // in (say) the second branch of a 'case'.  See bug3553.
  SInt32 saveBreakIndex = mBreakIndex;
  mBreakIndex = NO_ACTIVE_BREAK;

  for (bool cont = true; cont && !stmts.atEnd(); ++stmts) {
    NUStmt* stmt = *stmts;
    bool addStmt = true;

    // Warn about statements that can't be reached, but only in the first pass.
    // In that first pass we turn multi-level 'break' into a sequence of n 'breaks'
    // for each nested block we are breaking out of, so that in pass 2 we can
    // construct the ena=0 assignments that are really needed.  We do not want
    // to prune out the last n-1 of these n breaks.
    if (breakFound && (mPass == eControlResynthesis)) {
      const SourceLocator& loc = stmt->getLoc();
      mMsgContext->StatementNotReached(&loc);
      delete stmt;
      continue;
    }

    // If a previous statement has an embedded conditionalized 'break',
    // then guard the rest of the statements in an if.
    if (mBreakIndex != NO_ACTIVE_BREAK) {
      // Recurse through the rest of the statements, collecting
      // them into a new if-statement, gated by the block enable
      NUStmtList thenClause;
      // This recursive call to stmtList may delete the current stmt,
      // so copy the SourceLocator before calling.
      SourceLocator loc = stmt->getLoc();
      stmtList(stmts, blockInfo, &thenClause);
      NUNet* ena = getBlockEnableNet(blockInfo, loc);
      loc = blockInfo->enable->getLoc();
      NUExpr* cond = new NUIdentRvalue(ena, loc);
      cond->resize(1);
      NUIf* ifStmt = new NUIf(cond, thenClause, NUStmtList(),
                              mNetRefFactory, false, // is this legit?
                              loc);
      newStmts->push_back(ifStmt);
      cont = false;
      break;
    }

    switch (stmt->getType()) {
    case eNUCase:
      caseStmt((NUCase*) stmt, blockInfo);
      break;
    case eNUIf:
      // The if-statement may "consume" the rest of the statements
      // in the current list by inserting them into the "else" clause,
      // which is a more efficient way to realize a 'disable'.
      cont = ifStmt((NUIf*) stmt, blockInfo, stmts);
      break;
    case eNUFor:
      forStmt((NUFor*) stmt, blockInfo);
      break;
    case eNUBlock: {
      NUBlock* block = (NUBlock*) stmt;

      // In the first pass, we will leave the blocks intact.  In the second
      // pass, we will move the block statements into their parent statement
      // list, synthesizing any "ena" guard initializations at the beginning
      // of the statement list.
      if (mPass == eControlResynthesis) {
        NUStmtList blockStmts;
        blockStmt(block, &blockStmts);
        block->replaceStmtList(blockStmts);
      }
      else {
        blockStmt(block, newStmts);
        addStmt = false;
        deleteBlock(block);
      }
      break;
    }
    case eNUBreak:
      breakStmt((NUBreak*) stmt, newStmts);
      breakFound = true;
      addStmt = false;
      delete stmt;
      break;

    case eNUTaskEnable:
      taskEnable((NUTaskEnable*) stmt);
      break;

      // These cases for which we will do nothing in this routine are
      // explicitly left in here, rather than using a default, so that we
      // if someone adds a new type then he will get a compile warning
      // and be given the opportunity to consider whether this new statement
      // type must be explicity handled here.
    case eNUAlwaysBlock:
    case eNUAssign:
    case eNUBlockingAssign:
    case eNUCModelCall:
    case eNUCModel:
    case eNUCModelArgConnectionInput:
    case eNUCModelArgConnectionOutput:
    case eNUCModelArgConnectionBid:
    case eNUConcatLvalue:
    case eNUContAssign:
    case eNUControlSysTask:
    case eNUCycle:
    case eNUEnabledDriver:
    case eNUIdentLvalue:
    case eNUInitialBlock:
    case eNULvalue:
    case eNUMemselLvalue:
    case eNUModule:
    case eNUModuleInstance:
    case eNUNonBlockingAssign:
    case eNUOutputSysTask:
    case eNUInputSysTask:
    case eNUFOpenSysTask:
    case eNUFCloseSysTask:
    case eNUFFlushSysTask:
    case eNUPortConnectionBid:
    case eNUPortConnectionInput:
    case eNUPortConnectionOutput:
    case eNUStructuredProc:
    case eNUTF:
    case eNUTFArgConnectionBid:
    case eNUTFArgConnectionInput:
    case eNUTFArgConnectionOutput:
    case eNUTFElab:
    case eNUTask:
    case eNUTriRegInit:
    case eNUPartselIndexLvalue:
    case eNUReadmemX:
    case eNUSysRandom:
    case eNUVarselLvalue:
    case eNUBlockingEnabledDriver:
    case eNUContEnabledDriver:
    case eNUNamedDeclarationScope:
    case eNUCompositeLvalue:
    case eNUCompositeIdentLvalue:
    case eNUCompositeSelLvalue:
    case eNUCompositeFieldLvalue:
      break;
    } // switch

    if (addStmt) {
      newStmts->push_back(stmt);
    }
  } // for

  // Merge back the previous BreakIndex state
  updateBreakIndex(saveBreakIndex);
} // void BreakResynth::stmtList

NUNet* BreakResynth::getBlockEnableNet(BlockInfo* blockInfo,
                                       const SourceLocator& loc)
{
  NUNet* enaNet = blockInfo->enable;
  if (enaNet == NULL) {

    // Put the enable-net in the module scope, because it might need
    // to be disabled from inside a task call. Downstream, it may be
    // rescoped if a task did not access it.

    UtString buf;
    for (BlockInfo::StringAtomSet::SortedLoop p =
           blockInfo->mAnnotation.loopSorted();
         !p.atEnd(); ++p)
    {
      StringAtom* atom = *p;
      if (! buf.empty()) {
        buf << "_";
      }
      buf << atom->str();
    }
    StringAtom* enaSym = mModule->gensym("ena", buf.c_str());
    enaNet = mModule->createTempBitNet(enaSym, false, loc);
    blockInfo->enable = enaNet;

    // Defer the construction of the assignment to initialize the
    // enable net to '1', because that assign would get deleted when
    // we delete the block.  Instead, at the time we delete the block,
    // we will put the initialization assign into the parent statement-list
    // if an enable was needed.
  }
  return blockInfo->enable;
}

void BreakResynth::caseStmt(NUCase* caseStmt, BlockInfo* blockInfo)
{
  for (NUCase::ItemLoop items_iter = caseStmt->loopItems();
       !items_iter.atEnd();
       ++items_iter)
  {
    NUCaseItem* caseItem = *items_iter;

    NUStmtList newStmts;
    stmtList(caseItem->loopStmts(), blockInfo, &newStmts);
    caseItem->replaceStmts(newStmts);
  }
}

static bool sHasDisable(NUStmtLoop stmts) {
  for (; !stmts.atEnd(); ++stmts) {
    NUStmt* stmt = *stmts;
    if (stmt->getType() == eNUBreak) {
      return true;
    }
    else if (stmt->getType() == eNUBlock) {
      NUBlock* block = dynamic_cast<NUBlock*>(stmt);
      if (sHasDisable(block->loopStmts())) {
        return true;
      }
    }
  }
  return false;
}

bool BreakResynth::ifStmt(NUIf* ifStmt, BlockInfo* blockInfo,
                          NUStmtLoop stmts)
{
  // Consider this:
  //
  //   begin: blk
  //     if (cond)
  //       disable blk;
  //     a = b;
  //   end
  //
  // Naively we would generate this
  //
  //   begin: blk
  //     reg $ena_blk; $ena_blk = 1'b1;
  //     if (cond)
  //       ena_blk = 1'b0;
  //     if ($ena_blk)
  //       a = b;
  //   end
  //
  // But it's nicer to generate this:
  //
  //   begin: blk
  //     if (cond)
  //       ena_blk = 1'b0;
  //     else
  //       a = b;
  //   end
  //
  // Note that we don't even need the 'ena' net, and that will
  // likely be eliminated downstream

  // Strategy -- prescan the then & else branches to see if they
  // have direct disables.  If either branch has a disable, then merge the
  // statements following the "if" into the other branch, so they
  // can be considered contiguous.  We get slightly better results
  // that way in test/vhdl/lang_func/func_with_multiple_return.v,
  // because that final 'return' statement gets placed into a new
  // synthesized 'else', rather than having to create an explicit
  // new "if (ena) return..." for it.
  bool breakInThen = sHasDisable(ifStmt->loopThen());
  bool breakInElse = sHasDisable(ifStmt->loopElse());
  bool cont = false;

  // If both branches have an unconditional 'break' in them, then the
  // statements following the 'if' cannot be reached.  Kill 'em
  if (breakInThen && breakInElse) {
    ++stmts;                    // skip this if
    for (; !stmts.atEnd(); ++stmts) {
      NUStmt* stmt = *stmts;
      const SourceLocator& loc = stmt->getLoc();
      mMsgContext->StatementNotReached(&loc);
      delete stmt;
    }
  }
  else if (breakInThen) {
    // Move all the subsequent statements into the 'else' branch
    ++stmts;                    // skip this if
    for (; !stmts.atEnd(); ++stmts) {
      NUStmt* stmt = *stmts;
      ifStmt->addElseStmt(stmt);
    }
  }
  else if (breakInElse) {
    // Move all the subsequent statements into the 'then' branch
    ++stmts;                    // skip this if
    for (; !stmts.atEnd(); ++stmts) {
      NUStmt* stmt = *stmts;
      ifStmt->addThenStmt(stmt);
    }
  }
  else {
    // Continue processing the statements after the 'if'
    cont = true;
  }

  NUStmtList thenStmts, elseStmts;

  stmtList(ifStmt->loopThen(), blockInfo, &thenStmts);
  ifStmt->replaceThen(thenStmts);

  stmtList(ifStmt->loopElse(), blockInfo, &elseStmts);
  ifStmt->replaceElse(elseStmts);

  return cont;
} // bool BreakResynth::ifStmt

// Update the current minimum break index if it's higher level (lower)
// than the current one
void BreakResynth::updateBreakIndex(SInt32 breakIndex) {
  if ((breakIndex != -1) &&
      ((mBreakIndex == NO_ACTIVE_BREAK) || (breakIndex < mBreakIndex)))
  {
    mBreakIndex = breakIndex;
  }
}  

void BreakResynth::blockStmt(NUBlock* block, NUStmtList* stmts) {
  // Keep track of the stack of blocks so that if an inner 'break' breaks out
  // of multiple levels we can 'turn off' all the blocks that need to be
  // turned off.  The .first of the BlockInfo pair is the 'enable' net that
  // we will synthesize as needed if we see a 'break' for that block.
  BlockInfo* blockInfo = pushBlockInfo(block);

  // We employ a two-pass strategy over each module.  In the first
  // pass, we:
  //    - re-arrange control structure (see optimization comments in ::ifStmt)
  //    - Add explicit NUBreak statements for every level of block being
  //      broken out of
  //    - Add "if (ena) ..." tests, and for (...; ena && ...; ...) conditions
  //      wherever required (where the if-statement optimization was not
  //      enough.
  //
  // This way, we first determine the exact set of ena variables we really
  // need, and we can avoid synthesizing those that no one looks at.
  if (mPass == eControlResynthesis) {
    stmtList(block->loopStmts(), blockInfo, stmts);
  }
  else {
    NUStmtList blockStmts;
    stmtList(block->loopStmts(), blockInfo, &blockStmts);

    // If an enable was synthesized, initialize it to '1'
    if (blockInfo->enable != NULL) {
      const SourceLocator& loc = blockInfo->enable->getLoc();
      NULvalue* lval = new NUIdentLvalue(blockInfo->enable, loc);
      NUExpr* rval = NUConst::create(false, 1, 1, loc); // 1'b1
      rval->resize(1);
      NUAssign* assign = new NUBlockingAssign(lval, rval, false, loc);
      stmts->push_back(assign);
    }

    for (NUStmtLoop p(blockStmts); !p.atEnd(); ++p) {
      stmts->push_back(*p);
    }
  }

  // If we are popping up past the last potential break then clear
  // out the index, indicating there is no break pending
  if (mBreakIndex == blockInfo->index) {
    mBreakIndex = NO_ACTIVE_BREAK;
  }

  popBlockInfo(blockInfo);
} // void BreakResynth::blockStmt

BreakResynth::BlockInfo* BreakResynth::pushBlockInfo(NUBlock* block) {
  BlockMap::iterator findBlock = mBlockMap->find(block);
  BlockInfo* blockInfo = NULL;
  SInt32 blockIndex = mBlockVector->size();
  if (findBlock == mBlockMap->end()) {
    NU_ASSERT(mPass == eControlResynthesis, block);
    blockInfo = new BlockInfo(block, blockIndex);
    (*mBlockMap)[block] = blockInfo;
  }
  else {
    NU_ASSERT(mPass == eEnableConstruction, block);
    blockInfo = findBlock->second;
    blockInfo->index = blockIndex;
  }

  mBlockVector->push_back(blockInfo);
  return blockInfo;
}

void BreakResynth::popBlockInfo(BlockInfo* blockInfo) {
  // Remove this block from the stack (which is represented with a vector)
  mBlockVector->resize(blockInfo->index);

  if (mPass == eEnableConstruction) {
    // 2nd pass: destroy the block & remove it from the map
    BlockMap::iterator findBlock = mBlockMap->find(blockInfo->block);
    NU_ASSERT(findBlock != mBlockMap->end(), blockInfo->block);
    mBlockMap->erase(findBlock);
    delete blockInfo;
  }
}


static bool sIsConstantTrue(NUExpr* expr) {
  NUConst* k = expr->castConst();
  bool ret = ((k != NULL) && !k->isZero());
  return ret;
}    

void BreakResynth::forStmt(NUFor* forStmt, BlockInfo* blockInfo) {
  // Examine the initial block without seeing the uses for the condition
  NUStmtList initials, advance, body;
  stmtList(forStmt->loopInitial(), blockInfo, &initials);
  stmtList(forStmt->loopAdvance(), blockInfo, &advance);
  stmtList(forStmt->loopBody(), blockInfo, &body);

  NUExpr* cond = forStmt->getCondition();
  const SourceLocator& loc = forStmt->getLoc();

  // If there is a break anywhere in the for loop that breaks out of
  // the loop, then AND into the condition whether the enclosing block
  // is still enabled.
  if (mBreakIndex != NO_ACTIVE_BREAK) {
    NUNet* enaNet = getBlockEnableNet(blockInfo, loc);
    NUExpr* ena = new NUIdentRvalue(enaNet, loc);
    if (sIsConstantTrue(cond)) {
      delete cond;
      cond = ena;
    }
    else {
      cond = new NUBinaryOp(NUOp::eBiLogAnd, ena, cond, loc);
      cond->resize(1);
    }
    forStmt->setCondition(cond);

    // Bug 6586 shows us that we should not execute the for-loop 'advance'
    // if a break has occurred.  So move the 'advance' statements into
    // the body underneath a new if-statement.  Note: this may affect
    // performance if it disturbs an optimization that looks specifically
    // for advance-statements.  We could presumably avoid this block of
    // code if there are no uses if the variables def'd by the 'advance'
    // statements do not reach anywhere outside the for-loop.  Worse, it
    // will add an extra branch on every loop.  But for the
    // moment let's just do this unconditionally.
    //
    // If we every want to conditinalize this block of code on whether
    // anyone outside the for-loop looks at the loop variable, then we
    // need to do a fairly complete flow-analysis, which we generally
    // can't do at this point because non-blocking assigns have not
    // yet been removed.  And we need to worry about the loop-variable
    // being used outside the module via a port or hierarchical
    // reference.
    if (! advance.empty()) {
      CopyContext cc(NULL, NULL);
      NUExpr* adv_cond = ena->copy(cc);
      NUStmtList else_clause;
      NUStmt* adv_if = new NUIf(adv_cond, advance, else_clause,
                                mNetRefFactory,
                                false, // usesCFNet
                                loc);
      body.push_back(adv_if);
      advance.clear();          // below, ->replaceAdvance will mutate for-loop
    }
  }
  else if ((mPass == eControlResynthesis) && sIsConstantTrue(cond)) {
    // The VHDL "loop" keyword results in an NUFor with a cond of '1',
    // and we are counting on finding a 'break' to be able to get out
    // of the loop.  Check it now.
    mMsgContext->REInfiniteLoop( &loc );
  }

  forStmt->replaceInitial(initials);
  forStmt->replaceAdvance(advance);
  forStmt->replaceBody(body);
} // void BreakResynth::forStmt

void BreakResynth::taskEnable(NUTaskEnable* taskEnable) {
  NUTask* theTask = taskEnable->getTask();
  TaskSet::iterator p = mModuleTasks->find(theTask);
  if (p == mModuleTasks->end()) {
    mModuleTasks->insert(theTask);
    task(theTask);
  }
}

// If we encounter a 'break' statement, then we will replace it with
// assignments of zero to the appropriate enable nets -- one for each
// intervening block.  this will cause the target block to terminate
// (along with any intervening blocks) due to the conditional guards
// that were setup in BreakResynth::stmtList
void BreakResynth::breakStmt(NUBreak* breakStmt, NUStmtList* stmts)
{
  NUBlock* target = breakStmt->getTarget();
  const SourceLocator& loc = breakStmt->getLoc();

  BlockMap::iterator p = mBlockMap->find(target);
  if (p == mBlockMap->end()) {
    // This should theoretically be an assert, but it is an active
    // if-statement with an error message, because there could be
    // some situations with hierarchical task-enables where the
    // population is legal, but we cannot find the block we are
    // breaking across modules.  Print an error rather than asserting.
    //
    // Note that disables across in-module task-enables should work fine.
    mMsgContext->UnsupportedBreak(&loc, breakStmt->getKeyword()->str(),
                                  breakStmt->getTargetName()->str());
    return;
  }

  BlockInfo* targetInfo = p->second;

  if (mPass == eControlResynthesis) {
    SInt32 maxDepth = mBlockVector->size();
    if ( targetInfo->index >= maxDepth ){
      mMsgContext->UnsupportedBreak(&loc, breakStmt->getKeyword()->str(),
                                    breakStmt->getTargetName()->str());
      return;

    }
    updateBreakIndex(targetInfo->index);
    for (SInt32 i = targetInfo->index; i < maxDepth; ++i) {

      // Set all the block enable nets to 'false'
      BlockInfo* blockInfo = (*mBlockVector)[i];
      blockInfo->mAnnotation.insert(breakStmt->getTargetName());
      NUBreak* blockBreak = new NUBreak(blockInfo->block,
                                        breakStmt->getKeyword(),
                                        breakStmt->getTargetName(),
                                        mNetRefFactory,
                                        loc);
      stmts->push_back(blockBreak);
    }
  }
  else {
    // In the first pass generated a discrete Break statement for
    // every block we needed to break out of, so now we just have to
    // worry about generating assignment statements to zero out the
    // block enable variable.
    if (targetInfo->enable != NULL) {
      NULvalue* lval = new NUIdentLvalue(targetInfo->enable, loc);
      NUExpr* rval =  NUConst::create(false, 0, 1, loc); // 1'b0
      NUAssign* assign = new NUBlockingAssign(lval, rval, false, loc);
      stmts->push_back(assign);
    }
  }
} // void BreakResynth::breakStmt

void BreakResynth::module(NUModule* mod, Pass pass) {
  mModule = mod;

  mPass = pass;
  mModuleTasks->clear();

  for (NUModule::AlwaysBlockLoop p = mod->loopAlwaysBlocks(); !p.atEnd(); ++p) {
    structuredProc(*p);
  }
  for (NUModule::InitialBlockLoop p = mod->loopInitialBlocks(); !p.atEnd(); ++p) {
    structuredProc(*p);
  }

  mModule = NULL;
}

void BreakResynth::design(NUDesign* theDesign) {
  NUModuleList mods;
  theDesign->getModulesBottomUp(&mods);
  TaskSet designTasks;

  for (Loop<NUModuleList> p = Loop<NUModuleList>(mods); !p.atEnd(); ++p) {
    NUModule* mod = *p;

    // GC all the tasks.  Mark them here.  When we process them
    // in ::task() we will remove them from the mAllTasks set.
    // Any that are left cannot be reached, will not have been
    // processed, and should be removed!  But if we are feeling
    // queezy about that we should just resynthesize them on their
    // own.
    for (NUTaskLoop q = mod->loopTasks(); !q.atEnd(); ++q) {
      NUTask* theTask = *q;
      designTasks.insert(theTask);
    }

    module(mod, eControlResynthesis);
    module(mod, eEnableConstruction);
  }

  // Any remaining tasks were not hit by task enables.  We should probably
  // delete them, but for now just make sure we resynthesize them
  TaskSet::iterator not_found = mDesignTasksCovered->end();
  for (TaskSet::UnsortedLoop p = designTasks.loopUnsorted(); !p.atEnd(); ++p) {
    NUTask* theTask = *p;
    if (mDesignTasksCovered->find(theTask) == not_found) {
      mPass = eControlResynthesis;
      task(theTask);
      mPass = eEnableConstruction;
      mModuleTasks->clear();
      task(theTask);
    }
  }
}

void BreakResynth::structuredProc(NUStructuredProc* proc) {
  NUStmtList stmts;
  NUBlock* block = proc->getBlock();
  blockStmt(block, &stmts);
  block->replaceStmtList(stmts);
}

// At some point this should be modified to return a bool to support
// disabling an outer task from an inner task.  For now we don't support
// that, so just return void.
void BreakResynth::task(NUTask* taskDef) {
  // The task may be called hierarchically.  We need to populate enable
  // nets in the task's module.  We will not be able to resolve a disable
  // from within a hierarchiccally called task to its calling task
  // enable's block.  But we should be able to support a disable within
  // a hierarchically called task.
  NUModule* saveModule = mModule;
  mModule = taskDef->getParentModule();
  if (mPass == eControlResynthesis) {
    mDesignTasksCovered->insert(taskDef);
  }

  // Population should guarantee that tasks have a statement list
  // that contains only one statement: an NUblock.

  // We expect that coming out of population, tasks have exactly one
  // statement: an NUBlock, which can be used as the target for an NUBreak.
  // As of 3/14/05, this is only true for Verilog.  VHDL doesn't conform,
  // so in places where I'd like to assert, just return and do not process
  // the task.  I will address VHDL in a separate commit.

// # define NEED_ONE_BLOCK(cond, obj) NU_ASSERT(cond, obj)
# define NEED_ONE_BLOCK(cond, obj) if (!(cond)) return

  NUStmtLoop stmts = taskDef->loopStmts();
  NEED_ONE_BLOCK(!stmts.atEnd(), taskDef);
  NUStmt* stmt = *stmts;
  ++stmts;
  if (!stmts.atEnd()) {
/*
    // Print info spew before asserting
    UtIO::cerr() << "Task has more than one statement:\n";
    UInt32 i = 1;
    for (NUStmtLoop s = taskDef->loopStmts(); !s.atEnd(); ++s, ++i) {
      UtString buf;
      NUStmt* stmt = *s;
      stmt->compose(&buf, NULL);
      UtIO::cerr() << i << ".\t" << buf << "\n";
    }
*/
    NEED_ONE_BLOCK(stmts.atEnd(), taskDef);
  }
  NUBlock* block = dynamic_cast<NUBlock*>(stmt);
  NEED_ONE_BLOCK(block, taskDef);

  NUStmtList stmtList;
  blockStmt(block, &stmtList);
  if (mPass == eControlResynthesis) {
    block->replaceStmtList(stmtList);
  }
  else {
    taskDef->replaceStmtList(stmtList);
    deleteBlock(block);
  }

  mModule = saveModule;
} // void BreakResynth::task

void BreakResynth::deleteBlock(NUBlock* block) {
  NUStmtList empty;
  block->replaceStmtList(empty); // do not delete block contents ...

  NUScope* parentScope = block->getParentScope();
  block->moveLocals(parentScope);

  // There should be no local variables declared in the block when
  // we delete it
  for (NUNetLoop p = block->loopLocals(); !p.atEnd(); ++p) {
    NUNet* localNet = *p;
    NU_ASSERT(localNet == NULL, localNet);
  }
  delete block;
}
