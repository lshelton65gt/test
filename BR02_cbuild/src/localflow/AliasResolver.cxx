// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/AliasResolver.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetSet.h"
#include "reduce/RewriteUtil.h"
#include "reduce/Fold.h"
#include "util/CbuildMsgContext.h"

// Helps fold memsels of memsels and varsels of varsels generated
// as a result of alias resolution into one memsel and varsel respectively.
class Folder
{
public:

  Folder(Fold* fold)
    : mFold(fold)
  {
  }

  bool foldExpr(NUExpr* expr, NUExpr** foldedExpr)
  {
    bool success = true;
    *foldedExpr = NULL;

    switch (expr->getType())
    {
    case NUExpr::eNUVarselRvalue:
      success = foldVarselRvalue(dynamic_cast<NUVarselRvalue*>(expr), foldedExpr);
      break;
    case NUExpr::eNUMemselRvalue:
      success = foldMemselRvalue(dynamic_cast<NUMemselRvalue*>(expr), foldedExpr);
      break;
    case NUExpr::eNUCompositeSelExpr:
      success = foldCompSelExpr(dynamic_cast<NUCompositeSelExpr*>(expr), foldedExpr);
      break;
    default:
      break;
    }
    return success;
  }

  // Folds varsel of varsel into a single varsel.
  bool foldVarselRvalue(NUVarselRvalue* varsel, NUExpr** foldedExpr)
  {
    bool success = true;
    *foldedExpr = NULL;

    NUExpr* identExpr = varsel->getIdentExpr();
    if (identExpr->getType() == NUExpr::eNUVarselRvalue)
    {
      NUVarselRvalue* identVarsel =
        dynamic_cast<NUVarselRvalue*>(identExpr);
      // There are 4 cases here. 
      // 1. Select on select {x[3]}[2]. This is not practical.
      // 2. Part-select on select {x[3]}[3:2]. This is not practical either.
      // 3. Select on part-select {x[3:2]}[3]. This is practical and should
      //    be folded to x[3].
      // 4. Part-select on part-select {x[3:1]}[3:2]. This is practical and 
      //    should be folded to x[3:2].
      bool isIdentPartSel = isPartSelect(identVarsel);
      if (isIdentPartSel && identVarsel->isConstIndex())
      {
        CopyContext cc(0,0);
        NUExpr* identIdentExpr = identVarsel->getIdentExpr();
        NUExpr* identIdentExprCpy = identIdentExpr->copy(cc);

        if (isPartSelect(varsel)) // Case# 4
        {
          if (varsel->isConstIndex()) // x[3:1][3:2] => x[3:2]
          {
            // Verify that the range on the top is less than or equal to
            // the range at bottom.
            const ConstantRange* topRange = varsel->getRange();
            const ConstantRange* botRange = identVarsel->getRange();
            NU_ASSERT(botRange->contains(*topRange), varsel);

            // Create x[3:2].
            *foldedExpr = new NUVarselRvalue(identIdentExprCpy, *topRange,
                                             varsel->getLoc());
          }
          else // x[3:1][i+3:i+5] => x[i+3:i+5]
          {
            // The best we can do here is verify that the width of 
            // variable part-select is less than or equal to the base 
            // part-select range.
            const ConstantRange* topRange = varsel->getRange();
            const ConstantRange* botRange = identVarsel->getRange();
            NU_ASSERT(botRange->getLength() >= topRange->getLength(), varsel);

            NUExpr* indexCpy = varsel->getIndex()->copy(cc);
            // Create x[i+3:i+5]
            *foldedExpr = new NUVarselRvalue(identIdentExprCpy, indexCpy,
                                             *topRange, varsel->getLoc());
          }
        }
        else // Case# 3
        {
          NUExpr* index = varsel->getIndex();
          // Fold x[3:1][2] => x[2] and x[3:1][i] => x[i]
          // We can't verify that the variable range is within the part-select
          // bounds. Simply fold the part-select away.
          *foldedExpr = new NUVarselRvalue(identIdentExprCpy, index->copy(cc),
                                           varsel->getLoc());
        }
      }
      else if (isIdentPartSel)
      {
        // A case where ident is a variable part-select and there's a
        // part-select on top of that. This is not supported.
        success = false;
      }
      else
      {
        // Cannot have a varsel on varsel, since varsels are created only
        // on vectors and not on memories.
        NU_ASSERT(0, varsel);
      }
    }
    return success;
  }

  // Folds memsel of memsel into a single memsel.
  bool foldMemselRvalue(NUMemselRvalue* memsel, NUExpr** foldedExpr)
  {
    bool success = true;
    *foldedExpr = NULL;
    NUExpr* identExpr = memsel->getIdentExpr();
    NUExpr::Type identType = identExpr->getType();

    switch (identType)
    {
    case NUExpr::eNUMemselRvalue:
    {
      // Case of memsel of memsel. Ex: {x[3]}[2] => x[3][2]
      CopyContext cc(0,0);
      NUMemselRvalue* identMemsel = dynamic_cast<NUMemselRvalue*>(identExpr);
      NUNet* ident = identMemsel->getIdent();
      NUMemoryNet* mem = ident->getMemoryNet();
      UInt32 botDims = identMemsel->getNumDims();
      UInt32 topDims = memsel->getNumDims();

      NU_ASSERT(mem != NULL, identMemsel);
      NU_ASSERT(mem->getNumDims() > botDims + topDims, memsel);

      NUMemselRvalue* identMemselCpy = dynamic_cast<NUMemselRvalue*>(identMemsel->copy(cc));
      for (UInt32 dimIdx = 0; dimIdx < memsel->getNumDims(); ++dimIdx)
      {
        NUExpr* dimIdxExpr = memsel->getIndex(dimIdx);
        identMemselCpy->addIndexExpr(dimIdxExpr->copy(cc));
      }
      *foldedExpr = identMemselCpy;
      break;
    }
    case NUExpr::eNUIdentRvalue:
    {
      // Nothing to do here.
      break;
    }
    case NUExpr::eNUConcatOp:
    {
      // A part-select on a memory dimension other than the innermost one,
      // is blown up during population into a concat of each address
      // select. For ex: Net x[3:1][1:0], x[3:2] => {x[3], x[2]}
      // 
      // A select on such a concat Ex: {x[3], x[2]}[3] => x[3], should
      // be resolved to picking the right one.
      NUConcatOp* concat = dynamic_cast<NUConcatOp*>(identExpr);
      const NUExpr* identExpr = getIdentExpr(concat, NULL);
          
      if ((identExpr != NULL) &&
          (identExpr->getType() == NUExpr::eNUIdentRvalue))
      {
        const NUMemselRvalue* arg0 =
          dynamic_cast<const NUMemselRvalue*>(concat->getArg(0));

        NUExprVector exprs;
        // Skip the last dimension which corresponds to part select.
        copyIndices(arg0, exprs, true);
        // Add the select on top of part-select.
        copyIndices(memsel, exprs, false);

        CopyContext cc(0,0);
        *foldedExpr = new NUMemselRvalue(identExpr->copy(cc), &exprs,
                                         concat->getLoc());
        break;
      }
      // else fall through to default for failure.
    }
    default:
    {
      success = false;
      break;
    }
    } // switch

    return success;
  }

  // Fold composite select of composite select into one composite select.
  bool foldCompSelExpr(NUCompositeSelExpr* csel, NUExpr** foldedExpr)
  {
    bool success = true;
    *foldedExpr = NULL;

    NUCompositeInterfaceExpr* identExpr = csel->getIdentExpr();
    if (identExpr->getType() == NUExpr::eNUCompositeSelExpr)
    {
      NUCompositeSelExpr* identCompositeSel = 
        dynamic_cast<NUCompositeSelExpr*>(identExpr);

      // Four possible cases:
      // 1. Select of select {rec[3]}[2] => rec[3][2]
      // 2. Select of part-select {rec[3:2]}[2] => rec[2]
      // 3. Part-select of select {rec[3]}[5:6] => rec[3][5:6]
      // 4. Part-select of part-select {rec[3:1]}[3:2] => rec[3:2]

      bool isIdentPartSel = identCompositeSel->isPartSelect();
      if (isIdentPartSel && !identCompositeSel->isPartSelectIndexDummy())
      {
        // A case where ident is a variable part-select and there's a
        // part-select on top of that. This is not supported.
        success = false;
      }
      else
      {
        CopyContext cc(0,0);
        NUCompositeInterfaceExpr* identIdentExpr = identExpr->getIdentExpr();
        NUCompositeInterfaceExpr* identIdentExprCpy = 
          dynamic_cast<NUCompositeInterfaceExpr*>(identIdentExpr->copy(cc));

        // Add a copy of all but the part-select expr from ident to index list.
        NUExprVector indices;
        copyIndices(identCompositeSel, indices, isIdentPartSel);
        copyIndices(csel, indices, false);

        if (csel->isPartSelect()) // case 3 or 4
        {
          // If ident is a part-select, we can check that the top part-select
          // is contained by the ident part-select.
          const ConstantRange cselRge = csel->getPartSelRange();
          if (isIdentPartSel) {
            const ConstantRange identRge = identCompositeSel->getPartSelRange();
            NU_ASSERT(identRge.contains(cselRge), csel);
          }

          *foldedExpr = new NUCompositeSelExpr(identIdentExprCpy, &indices,
                                               cselRge, csel->isPartSelectIndexDummy(),
                                               csel->getLoc());
        }
        else // case 1 or 2
        {
          // For case 2, we're not going to be able to check if the index
          // is within the ident part-select range, since it could be variable.
          *foldedExpr = new NUCompositeSelExpr(identIdentExprCpy, &indices,
                                               csel->getLoc());
        }
      } // else
    } // if

    return success;
  }

  bool foldLvalue(NULvalue* lval, NULvalue** foldedLvalue)
  {
    bool success = true;
    *foldedLvalue = NULL;

    switch (lval->getType())
    {
    case eNUVarselLvalue:
      success = foldVarselLvalue(dynamic_cast<NUVarselLvalue*>(lval), foldedLvalue);
      break;
    case eNUMemselLvalue:
      success = foldMemselLvalue(dynamic_cast<NUMemselLvalue*>(lval), foldedLvalue);
      break;
    case eNUCompositeSelLvalue:
      success = foldCompSelLvalue(dynamic_cast<NUCompositeSelLvalue*>(lval), foldedLvalue);
      break;
    default:
      break;
    }
    return success;
  }

  // Folds varsel of varsel into a single varsel.
  bool foldVarselLvalue(NUVarselLvalue* varsel, NULvalue** foldedLvalue)
  {
    bool success = true;
    *foldedLvalue = NULL;

    NULvalue* identLvalue = varsel->getLvalue();
    if (identLvalue->getType() == eNUVarselLvalue)
    {
      NUVarselLvalue* identVarsel =
        dynamic_cast<NUVarselLvalue*>(identLvalue);
      // There are 4 cases here. 
      // 1. Select on select {x[3]}[2]. This is not practical.
      // 2. Part-select on select {x[3]}[3:2]. This is not practical either.
      // 3. Select on part-select {x[3:2]}[3]. This is practical and should
      //    be folded to x[3].
      // 4. Part-select on part-select {x[3:1]}[3:2]. This is practical and 
      //    should be folded to x[3:2].
      bool isIdentPartSel = isPartSelect(identVarsel);
      if (isIdentPartSel && identVarsel->isConstIndex())
      {
        CopyContext cc(0,0);
        NULvalue* identIdentLvalue = identVarsel->getLvalue();
        NULvalue* identIdentLvalueCpy = identIdentLvalue->copy(cc);

        if (isPartSelect(varsel)) // Case# 4
        {
          if (varsel->isConstIndex()) // x[3:1][3:2] => x[3:2]
          {
            // Verify that the range on the top is less than or equal to
            // the range at bottom.
            const ConstantRange* topRange = varsel->getRange();
            const ConstantRange* botRange = identVarsel->getRange();
            NU_ASSERT(botRange->contains(*topRange), varsel);

            // Create x[3:2].
            *foldedLvalue = new NUVarselLvalue(identIdentLvalueCpy, *topRange,
                                               varsel->getLoc());
          }
          else // x[3:1][i+3:i+5] => x[i+3:i+5]
          {
            // The best we can do here is verify that the width of 
            // variable part-select is less than or equal to the base 
            // part-select range.
            const ConstantRange* topRange = varsel->getRange();
            const ConstantRange* botRange = identVarsel->getRange();
            NU_ASSERT(botRange->getLength() >= topRange->getLength(), varsel);

            NUExpr* indexCpy = varsel->getIndex()->copy(cc);
            // Create x[i+3:i+5]
            *foldedLvalue = new NUVarselLvalue(identIdentLvalueCpy, indexCpy,
                                               *topRange, varsel->getLoc());
          }
        }
        else // Case# 3
        {
          NUExpr* index = varsel->getIndex();
          // Fold x[3:1][2] => x[2] and x[3:1][i] => x[i]
          // We can't verify that the variable range is within the part-select
          // bounds. Simply fold the part-select away.
          *foldedLvalue = new NUVarselLvalue(identIdentLvalueCpy, index->copy(cc),
                                             varsel->getLoc());
        }
      }
      else if (isIdentPartSel)
      {
        // A case where ident is a variable part-select and there's a
        // part-select on top of that. This is not supported.
        success = false;
      }
      else
      {
        // Cannot have a varsel on varsel, since varsels are created only
        // on vectors and not on memories.
        NU_ASSERT(0, varsel);
      }
    }
    return success;
  }

  // Folds memsel of memsel into a single memsel.
  bool foldMemselLvalue(NUMemselLvalue* memsel, NULvalue** foldedLvalue)
  {
    bool success = true;
    *foldedLvalue = NULL;
    NULvalue* identLvalue = memsel->getIdentLvalue();
    NUType identType = identLvalue->getType();

    switch (identType)
    {
    case eNUMemselLvalue:
    {
      // Case of memsel of memsel. Ex: {x[3]}[2] => x[3][2]
      CopyContext cc(0,0);
      NUMemselLvalue* identMemsel = dynamic_cast<NUMemselLvalue*>(identLvalue);
      NUNet* ident = identMemsel->getIdent();
      NUMemoryNet* mem = ident->getMemoryNet();
      UInt32 botDims = identMemsel->getNumDims();
      UInt32 topDims = memsel->getNumDims();

      NU_ASSERT(mem != NULL, identMemsel);
      NU_ASSERT(mem->getNumDims() > botDims + topDims, memsel);

      NUMemselLvalue* identMemselCpy = dynamic_cast<NUMemselLvalue*>(identMemsel->copy(cc));
      for (UInt32 dimIdx = 0; dimIdx < memsel->getNumDims(); ++dimIdx)
      {
        NUExpr* dimIdxLvalue = memsel->getIndex(dimIdx);
        identMemselCpy->addIndexExpr(dimIdxLvalue->copy(cc));
      }
      *foldedLvalue = identMemselCpy;
      break;
    }
    case eNUIdentLvalue:
    {
      // Nothing to do here.
      break;
    }
    case eNUConcatLvalue:
    {
      // A part-select on a memory dimension other than the innermost one,
      // is blown up during population into a concat of each address
      // select. For ex: Net x[3:1][1:0], x[3:2] => {x[3], x[2]}
      // 
      // A select on such a concat Ex: {x[3], x[2]}[3] => x[3], should
      // be resolved to picking the right one.
      NUConcatLvalue* concat = dynamic_cast<NUConcatLvalue*>(identLvalue);
      const NULvalue* identLvalue = getIdentLvalue(concat, NULL);
          
      if ((identLvalue != NULL) &&
          (identLvalue->getType() == eNUIdentLvalue))
      {
        const NUMemselLvalue* arg0 =
          dynamic_cast<const NUMemselLvalue*>(concat->getArg(0));

        NUExprVector exprs;
        // Skip the last dimension which corresponds to part select.
        copyIndices(arg0, exprs, true);
        // Add the select on top of part-select.
        copyIndices(memsel, exprs, false);

        CopyContext cc(0,0);
        *foldedLvalue = new NUMemselLvalue(identLvalue->copy(cc), &exprs,
                                           concat->getLoc());
        break;
      }
      // else fall through to default for failure.
    }
    default:
    {
      success = false;
      break;
    }
    } // switch

    return success;
  }

  bool foldCompSelLvalue(NUCompositeSelLvalue* csel, NULvalue** foldedLvalue)
  {
    bool success = true;
    *foldedLvalue = NULL;

    NULvalue* identLvalue = csel->getIdentExpr();
    if (identLvalue->getType() == eNUCompositeSelLvalue)
    {
      NUCompositeSelLvalue* identCompositeSel = 
        dynamic_cast<NUCompositeSelLvalue*>(identLvalue);

      // Four possible cases:
      // 1. Select of select {rec[3]}[2] => rec[3][2]
      // 2. Select of part-select {rec[3:2]}[2] => rec[2]
      // 3. Part-select of select {rec[3]}[5:6] => rec[3][5:6]
      // 4. Part-select of part-select {rec[3:1]}[3:2] => rec[3:2]

      bool isIdentPartSel = identCompositeSel->isPartSelect();
      if (isIdentPartSel && !identCompositeSel->isPartSelectIndexDummy())
      {
        // A case where ident is a variable part-select and there's a
        // part-select on top of that. This is not supported.
        success = false;
      }
      else
      {
        CopyContext cc(0,0);
        NUCompositeInterfaceLvalue* identIdentLval = identCompositeSel->getIdentExpr();
        NUCompositeInterfaceLvalue* identIdentLvalCpy = 
          dynamic_cast<NUCompositeInterfaceLvalue*>(identIdentLval->copy(cc));

        // Add a copy of all but the part-select expr from ident to index list.
        NUExprVector indices;
        copyIndices(identCompositeSel, indices, isIdentPartSel);
        copyIndices(csel, indices, false);

        if (csel->isPartSelect()) // case 3 or 4
        {
          // If ident is a part-select, we can check that the top part-select
          // is contained by the ident part-select.
          const ConstantRange cselRge = csel->getPartSelRange();
          if (isIdentPartSel) {
            const ConstantRange identRge = identCompositeSel->getPartSelRange();
            NU_ASSERT(identRge.contains(cselRge), csel);
          }

          *foldedLvalue = new NUCompositeSelLvalue(identIdentLvalCpy, &indices,
                                                   cselRge, csel->isPartSelectIndexDummy(),
                                                   csel->getLoc());
        }
        else // case 1 or 2
        {
          // For case 2, we're not going to be able to check if the index
          // is within the ident part-select range, since it could be variable.
          *foldedLvalue = new NUCompositeSelLvalue(identIdentLvalCpy, &indices,
                                                   csel->getLoc());
        }
      } // else
    } // if

    return success;
  }

  // Part selects on memory addresses are turned into a concat
  // of select on every address. Detect these as valid alias
  // replacement expressions.
  const NUExpr* getIdentExpr(const NUConcatOp* concat,
                             NUCExprVector* indices) const
  {
    const NUExpr* identExpr = NULL;
    UInt32 numSelects = 0;
    UInt32 repeatCount = concat->getRepeatCount();
    UInt32 numArgs = concat->getNumArgs();
    bool done = false;
    for (UInt32 argIdx = 0;
         (repeatCount == 1) && !done && (argIdx < numArgs);
         ++argIdx)
    {
      const NUExpr* arg = concat->getArg(argIdx);
      const NUExpr* argIdentExpr = NULL;
      UInt32 selects = 0;
      if (arg->getType() == NUExpr::eNUMemselRvalue)
      {
        const NUMemselRvalue* memsel = dynamic_cast<const NUMemselRvalue*>(arg);
        argIdentExpr = memsel->getIdentExpr();
        selects = memsel->getNumDims();
        if (indices != NULL) {
          indices->push_back(memsel->getIndex(selects-1));
        }
      }
      if (argIdentExpr == NULL) { // Not a concat of memsels.
        done = true;
      } else if (identExpr == NULL) { // First time around.
        identExpr = argIdentExpr;
        numSelects = selects;
      } else if ((*identExpr != *argIdentExpr) || (selects != numSelects)) {
        // Not a concat of same memory net.
        identExpr = NULL;
        done = true;
      }
    }
    return identExpr;
  }

  const NULvalue* getIdentLvalue(const NUConcatLvalue* concat,
                                 NUCExprVector* indices) const
  {
    const NULvalue* identLvalue = NULL;
    UInt32 numSelects = 0;
    UInt32 numArgs = concat->getNumArgs();
    bool done = false;
    for (UInt32 argIdx = 0; !done && (argIdx < numArgs); ++argIdx)
    {
      const NULvalue* arg = concat->getArg(argIdx);
      const NULvalue* argIdentLvalue = NULL;
      UInt32 selects = 0;
      if (arg->getType() == eNUMemselLvalue)
      {
        const NUMemselLvalue* memsel = dynamic_cast<const NUMemselLvalue*>(arg);
        argIdentLvalue = memsel->getIdentLvalue();
        selects = memsel->getNumDims();
        if (indices != NULL) {
          indices->push_back(memsel->getIndex(selects-1));
        }
      }
      if (argIdentLvalue == NULL) { // Not a concat of memsels.
        done = true;
      } else if (identLvalue == NULL) { // First time around.
        identLvalue = argIdentLvalue;
        numSelects = selects;
      } else if ((!(*identLvalue == *argIdentLvalue)) || (selects != numSelects)) {
        // Not a concat of same memory net.
        identLvalue = NULL;
        done = true;
      }
    }
    return identLvalue;
  }

  // Is given expression constant with or without folding?
  bool isConstant(const NUExpr* expr) const
  {
    bool isConst = false;
    if (expr->getType() == NUExpr::eNUConstNoXZ) {
      isConst = true;
    } else {
      // Try folding.
      CopyContext cc(0,0);
      NUExpr* exprCpy = expr->copy(cc);
      NUExpr* folded = mFold->fold(exprCpy);
      if (folded) {
        if (folded->isConstant()) {
          isConst = true;
        }
        delete folded;
      } else {
        delete exprCpy;
      }
    }
    return isConst;
  }

  static bool isPartSelect(NUVarselRvalue* varsel)
  {
    const ConstantRange* rge = varsel->getRange();
    return (*rge != ConstantRange(0,0));
  }

  static bool isPartSelect(NUVarselLvalue* varsel)
  {
    const ConstantRange* rge = varsel->getRange();
    return (*rge != ConstantRange(0,0));
  }

private:

  void copyIndices(const NUMemselRvalue* memsel, NUExprVector& indices,
                   bool skipLastIndex)
  {
    UInt32 numIndices = memsel->getNumDims();
    if (skipLastIndex) {
      --numIndices;
    }
    CopyContext cc(0,0);
    for (UInt32 i = 0; i < numIndices; ++i)
    {
      const NUExpr* index = memsel->getIndex(i);
      NUExpr* indexCpy = index->copy(cc);
      indices.push_back(indexCpy);
    }
  }

  void copyIndices(const NUCompositeSelExpr* csel, NUExprVector& indices,
                   bool skipLastIndex)
  {
    UInt32 numIndices = csel->getNumIndices();
    if (skipLastIndex) {
      --numIndices;
    }
    CopyContext cc(0,0);
    for (UInt32 i = 0; i < numIndices; ++i)
    {
      const NUExpr* index = csel->getIndex(i);
      NUExpr* indexCpy = index->copy(cc);
      indices.push_back(indexCpy);
    }
  }

  void copyIndices(const NUMemselLvalue* memsel, NUExprVector& indices,
                   bool skipLastIndex)
  {
    UInt32 numIndices = memsel->getNumDims();
    if (skipLastIndex) {
      --numIndices;
    }
    CopyContext cc(0,0);
    for (UInt32 i = 0; i < numIndices; ++i)
    {
      const NUExpr* index = memsel->getIndex(i);
      NUExpr* indexCpy = index->copy(cc);
      indices.push_back(indexCpy);
    }
  }

  void copyIndices(const NUCompositeSelLvalue* csel, NUExprVector& indices,
                   bool skipLastIndex)
  {
    UInt32 numIndices = csel->getNumIndices();
    if (skipLastIndex) {
      --numIndices;
    }
    CopyContext cc(0,0);
    for (UInt32 i = 0; i < numIndices; ++i)
    {
      const NUExpr* index = csel->getIndex(i);
      NUExpr* indexCpy = index->copy(cc);
      indices.push_back(indexCpy);
    }
  }

  Fold* mFold;
};

// Holds mapping between alias net and original net ranges for each
// alias net dimension.
class AliasInfo
{
public:

  AliasInfo(const NUNet* aliasNet, const NUNet* origNet,
            ConstantRangeVector& aliasRanges,
            ConstantRangeVector& origRanges)
    : mAliasNet(aliasNet), mOrigNet(origNet),
      mAliasRanges(aliasRanges), mOrigRanges(origRanges)
  {
    NU_ASSERT2(mAliasRanges.size() == mOrigRanges.size(), aliasNet, origNet);
  }
  
  // Determine the offset for the given index dimension between the
  // original and alias range. The isFlipped is set if range directions are
  // not the same.
  SInt32 getOffset(UInt32 indexDim, bool isVarsel, bool* isFlipped) const
  {
    UInt32 numAliasDims = mAliasRanges.size();
    NU_ASSERT(indexDim < numAliasDims, mAliasNet);
    UInt32 dim = 0;
    if (!isVarsel) {
      dim = numAliasDims - indexDim - 1;
    }
    // else for variable selects, the range and index is already normalized.
    const ConstantRange& aliasRange = mAliasRanges[dim];
    const ConstantRange& origRange = mOrigRanges[dim];
    return getOffsetHelper(aliasRange, origRange, isFlipped);
  }

  const NUNet* getOrigNet() const {
    return mOrigNet;
  }

  const ConstantRangeVector& getOrigRanges() const {
    return mOrigRanges;
  }

private:

  // Given the alias and original ranges, determine the offset that 
  // should be applied to the index and whether the ranges are flipped.
  SInt32 getOffsetHelper(const ConstantRange& aliasRange,
                         const ConstantRange& origRange, bool* isFlipped) const
  {
    NU_ASSERT2(aliasRange.getLength() == origRange.getLength(),
               mAliasNet, mOrigNet);
    *isFlipped = origRange.isFlipped(aliasRange);
    SInt32 offset = 0;
    if (*isFlipped) {
      offset += aliasRange.getLsb() + aliasRange.getMsb();
      offset += origRange.getLsb() - aliasRange.getMsb();
    } else {
      offset = origRange.getLsb() - aliasRange.getLsb();
    }
    return offset;
  }

private:
  const NUNet* mAliasNet;
  const NUNet* mOrigNet;
  ConstantRangeVector mAliasRanges;
  ConstantRangeVector mOrigRanges;
};

// When aliases are resolved to originals, the alias selects also have to
// be resolved. This involves offsetting the select to fall in the
// original net range. This class creates a map of alias ranges to
// original net ranges for each alias net dimension to help with that process.
// Note: Only rvalues are handled here and not lvalues since alias replacements
//       can only be rvalues.
class SelectOffsetCB : public NUDesignCallback
{
public:

  typedef UtMap<const NUNet*, AliasInfo> NetAliasInfoMap;
  typedef NetAliasInfoMap::const_iterator NetAliasInfoMapIter;

  SelectOffsetCB(NUExprReplacementMap& replMap,
                 NetAliasInfoMap& aliasInfoMap, Fold* fold, MsgContext* msgCtx)
    : mReplMap(replMap), mAliasInfoMap(aliasInfoMap), mFold(fold),
      mMsgCtx(msgCtx), mSuccess(true)
  {
  }

  Status operator() (Phase , NUBase* ) { return eNormal; }

  Status operator() (Phase phase, NUIdentRvalue* rval)
  {
    if (phase == ePre) {
      NUIdentRvalue* identRval = dynamic_cast<NUIdentRvalue*>(rval);
      handleNet(identRval->getIdent());
    }
    return (mSuccess ? eNormal : eStop);
  }

  Status operator() (Phase phase, NUCompositeIdentRvalue* rval)
  {
    if (phase == ePre) {
      NUCompositeIdentRvalue* identRval =
        dynamic_cast<NUCompositeIdentRvalue*>(rval);
      handleNet(identRval->getIdent());
    }
    return (mSuccess ? eNormal : eStop);
  }

  static bool computeOffsets(NUExprReplacementMap& replMap,
                             NetAliasInfoMap& aliasInfoMap, const NUNet* aliasNet,
                             Fold* fold, MsgContext* msgCtx)
  {
    SelectOffsetCB socb(replMap, aliasInfoMap, fold, msgCtx);
    socb.handleNet(aliasNet);
    return socb.getSuccess();
  }

private:

  bool getSuccess() {
    return mSuccess;
  }

  void handleNet(const NUNet* net)
  {
    NUExpr* repl = getNetRepl(net);
    // Is alias net and offsets not yet computed.
    if ((repl != NULL) && !isMapped(net, repl))
    {
      // The replacement could have unresolved alias nets in them.
      // Resolve them first so that we have the alias net to original net
      // range mapping ready for them.
      mSuccess &= computeOffsets(repl, mFold, mMsgCtx);

      if (mSuccess)
      {
        // Get the replacement expression ranges that are not selected
        // in the replacement expression. These ranges map to alias
        // net ranges.
        SInt32 partSelOffset = 0;
        ConstantRangeVector origRanges;
        const NUNet* ident = getExprRanges(repl, origRanges, &partSelOffset);

        if (ident != NULL)
        {
          // Get the alias net ranges for each dimension.
          ConstantRangeVector aliasRanges;
          getNetRanges(net, aliasRanges);
          UInt32 origRangesSize = origRanges.size();
          NU_ASSERT2(origRangesSize == aliasRanges.size(), net, repl);

          NetAliasInfoMapIter iter = mAliasInfoMap.find(ident);
          if (iter != mAliasInfoMap.end())
          {
            // The ident is an alias and has been resolved to the original.
            // We need to remap the origRanges with origRanges of alias.
            const AliasInfo& ai = iter->second;
            const ConstantRangeVector& aliasOrigRanges = ai.getOrigRanges();
            UInt32 aliasOrigRangesSize = aliasOrigRanges.size();
            NU_ASSERT2(aliasOrigRangesSize >= origRangesSize, ident, repl);

            ConstantRangeVector::const_iterator aliasOrigItr
              = aliasOrigRanges.begin();
            ConstantRangeVector::iterator origItr = origRanges.begin();
        
            // This is a case where alias net replacement expression is made 
            // up of another alias net. These could be called multi-level aliases.
            // All multi-level aliases need to be resolved to the net that is
            // not an alias. Since computeOffsets() is called recursively, for
            // multi-level aliases the ranges of alias net in replacement has
            // already been mapped to the range of non-alias net. All that remains
            // to be done is to remap the replacement expression ranges to the
            // non-alias net range.

            // Start from the innermost dimension of both the alias and original
            // ranges and remap the alias range to non-alias net range. Here's an
            // example that explains why:
            //
            // type a is array(natural range<>) of std_logic;
            // type b is array(natural range<>) of a(1 downto 0);
            // type c is array(natural range<>) of b(3 downto 2);
            // type d is array(natural range<>) of c(5 downto 4);
            // type e is array(natural range<>) of d(7 downto 6);
            // 
            // signal y : e(9 downto 8);
            // alias x : d(11 downto 10) is y(8);
            // alias z : c(13 downto 12) is x;
            //
            // Net y ranges : [1:0][3:2][5:4][7:6]
            // Net x ranges : [1:0][3:2][11:10]    maps to y ranges : [1:0][3:2][5:4]
            // Net z ranges : [1:0][3:2][13:12]    maps to x ranges : [1:0][3:2][11:10]   maps to y ranges : [1:0][3:2][5:4]
            //
            // Before executing this loop the map for alias net z is:
            // Net z ranges : [1:0][3:2][13:12]    maps to x ranges : [1:0][3:2][11:10]
            //
            // After executing this loop the map for alias net z is:
            // Net z ranges : [1:0][3:2][13:12]    maps to y ranges : [1:0][3:2][5:4]
            //
            // This way a select on z changes as follows:
            // z[12] => y[4]
            //
            while (origItr != origRanges.end()) {
              *origItr = maybeRemapRanges(*aliasOrigItr, *origItr, partSelOffset);
              ++origItr;
              ++aliasOrigItr;
            }

            ident = ai.getOrigNet();
          }
      
          AliasInfo ai(net, ident, aliasRanges, origRanges);
          mAliasInfoMap.insert(NetAliasInfoMap::value_type(net, ai));
        } // if
      } // if
    } // if
  } // void handleNet

  // If the aliasRange had a part-select on it, then the original range need
  // to be part-selected too!.
  ConstantRange maybeRemapRanges(const ConstantRange& origRange, const ConstantRange& aliasRange,
                                 SInt32 partSelOffset)
  {
    ConstantRange cr = origRange;
    if (origRange.getLength() != aliasRange.getLength())
    {
      // The alias range is a part-select with partSelOffset given.
      UInt32 numBits = aliasRange.getLength();
      if (origRange.bigEndian() ^ aliasRange.bigEndian()) {
        // Opposite range direction.
        cr.setMsb(origRange.getMsb() + partSelOffset);
      } else {
        // Same range direction.
        cr.setMsb(origRange.getMsb() - partSelOffset);
      }
      if (origRange.bigEndian()) {
        cr.setLsb(cr.getMsb() - numBits + 1);
      } else {
        cr.setLsb(cr.getMsb() + numBits - 1);
      }
    }
    return cr;
  }

  void getNetRanges(const NUNet* net, ConstantRangeVector& ranges)
  {
    if (net->isVectorNet())
    {
      const NUVectorNet* vn = net->castVectorNet();
      const ConstantRange* rge = vn->getRange();
      ranges.push_back(*rge);
    }
    else if (net->isMemoryNet())
    {
      const NUMemoryNet* mn = net->getMemoryNet();
      UInt32 numDims = mn->getNumDims();
      for (UInt32 dim = 0; dim < numDims; ++dim) {
        const ConstantRange* rge = NULL;
        if (dim == 0) {
          rge = &(mn->getOperationalRange(dim));
        } else {
          rge = mn->getRange(dim);
        }
        ranges.push_back(*rge);
      }
    }
    else if (net->isCompositeNet())
    {
      const NUCompositeNet* cn = net->getCompositeNet();
      UInt32 numDims = cn->getNumDims();
      for (UInt32 dim = 0; dim < numDims; ++dim) {
        const ConstantRange* rge = cn->getRange(dim);
        ranges.push_back(*rge);
      }
    }
  }

  // Shrink the range of net dimension to part-select range.
  // The part-select should be contained inside net range.
  void maybeApplyPartSelect(const ConstantRange& psRange,
                            ConstantRangeVector& ranges,
                            SInt32* partSelOffset, const NUExpr* expr)
  {
    if (psRange.getLength() > 1)
    {
      ConstantRange cr = ranges.back();
      ranges.pop_back();
      // The dimension that's being part-selected ought to have a range
      // that's greater than or equal to the part-select range. Jaguar
      // ought to have checked for it.
      NU_ASSERT(cr.contains(psRange), expr);
      // Modify the top most range.
      ranges.push_back(psRange);
      *partSelOffset = cr.getMsb() - psRange.getMsb();
    }
    else
    {
      // Chop off the preselected top most dimension.
      ranges.pop_back();
    }
  }

  // Convert selects to integers if they're constants. Check that
  // the selects are contiguous and construct a part-select out
  // of the minimum and maximum.
  bool selectsToRange(NUCExprVector& selects, ConstantRange& psRange)
  {
    bool success = true;
    CopyContext cc(0,0);
    UtVector<SInt32> intSelects;
    for (NUCExprVector::const_iterator itr = selects.begin();
         success && (itr != selects.end()); ++itr)
    {
      const NUExpr* selExpr = *itr;
      NUExpr* selExprCpy = selExpr->copy(cc);
      {
        NUExpr* foldedSelExpr = mFold->fold(selExprCpy);
        if (foldedSelExpr != NULL) {
          selExprCpy = foldedSelExpr;
        }
      }
      NUConst* constSel = selExprCpy->castConstNoXZ();
      if (constSel == NULL) {
        success = false;
      } else {
        // Index is expected to be a 32 bit integer.
        SInt32 intSel = 0;
        success &= constSel->getL(&intSel);
        intSelects.push_back(intSel);
      }
      delete selExprCpy;
    }

    if (success)
    {
      // Sort the integer selects.
      SInt32 numSels = intSelects.size();
      SInt32 incr = 1;
      SInt32 selVal = intSelects[0];
      if (selVal > intSelects[numSels-1]) {
        incr = -1;
      }
      // Check that the entire range exists in the vector.
      for (SInt32 i = 0; success && (i < numSels); ++i)
      {
        if (selVal != intSelects[i]) {
          success = false;
        }
        selVal += incr;
      }
      // Pick the first and last select and make a part select range from it.
      if (success) {
        psRange.setMsb(intSelects[0]);
        psRange.setLsb(intSelects[numSels-1]);
      }
    }
    return success;
  }

  // Skip the dimensions that are selected in the expression and return the
  // ranges for all the unselected dimensions. If there's a part-select on
  // any dimension, shrink the range to part-select. For example:
  //
  // Net x[3:2][4:7], x[4] gives [3:2]. x[4:5] gives [3:2][4:5].
  NUNet* getExprRanges(const NUExpr* expr, ConstantRangeVector& ranges,
                       SInt32* partSelOffset)
  {
    NUNet* ident = NULL;
    NUExpr::Type typ = expr->getType();
    switch (typ)
    {
    case NUExpr::eNUIdentRvalue:
    {
      const NUIdentRvalue* identExpr =
        dynamic_cast<const NUIdentRvalue*>(expr);
      ident = identExpr->getIdent();
      getNetRanges(ident, ranges);
      // Straight one to one alias. No pre-selections.
      break;
    }
    case NUExpr::eNUVarselRvalue:
    {
      const NUVarselRvalue* varselExpr =
        dynamic_cast<const NUVarselRvalue*>(expr);
      // We expect constant index for aliased expressions, since they're
      // declarations and the ranges should be known at compile time.
      NU_ASSERT(varselExpr->isConstIndex(), varselExpr);
      const NUExpr* identExpr = varselExpr->getIdentExpr();
      ident = getExprRanges(identExpr, ranges, partSelOffset);
      const ConstantRange* rge = varselExpr->getRange();
      // We ought to have at least one dimension to select/part-select on.
      NU_ASSERT(!ranges.empty(), varselExpr);
      maybeApplyPartSelect(*rge, ranges, partSelOffset, expr);
      break;
    }
    case NUExpr::eNUMemselRvalue:
    {
      const NUMemselRvalue* memselExpr =
        dynamic_cast<const NUMemselRvalue*>(expr);
      const NUExpr* identExpr = memselExpr->getIdentExpr();
      ident = getExprRanges(identExpr, ranges, partSelOffset);
      UInt32 numSelects = memselExpr->getNumDims();
      for (UInt32 dim = 0; dim < numSelects; ++dim) {
        // Chop off the preselected top most dimension.
        ranges.pop_back();
      }
      break;
    }
    case NUExpr::eNUCompositeIdentRvalue:
    case NUExpr::eNUCompositeFieldExpr:
    {
      const NUCompositeInterfaceExpr* identExpr =
        dynamic_cast<const NUCompositeInterfaceExpr*>(expr);
      ident = identExpr->getNet();
      getNetRanges(ident, ranges);
      break;
    }
    case NUExpr::eNUCompositeSelExpr:
    {
      const NUCompositeSelExpr* selExpr =
        dynamic_cast<const NUCompositeSelExpr*>(expr);
      const NUExpr* identExpr = selExpr->getIdentExpr();
      ident = getExprRanges(identExpr, ranges, partSelOffset);
      // Chop off all the preselected top most dimensions except the last one.
      UInt32 numSelects = selExpr->getNumIndices();
      for (UInt32 dim = 0; dim < numSelects-1; ++dim) {
        ranges.pop_back();
      }
      // The last select could be a part select.
      const ConstantRange psRange = selExpr->getPartSelRange();
      maybeApplyPartSelect(selExpr->getPartSelRange(), ranges, partSelOffset,
                           expr);
      break;
    }
    case NUExpr::eNUConcatOp:
    {
      const NUConcatOp* concatExpr =
        dynamic_cast<const NUConcatOp*>(expr);
      NUCExprVector partSelIndices;
      Folder fd(mFold);
      const NUExpr* identExpr = fd.getIdentExpr(concatExpr, &partSelIndices);
      // An expression of type not supported.
      NU_ASSERT(identExpr != NULL, concatExpr);
      ident = getExprRanges(identExpr, ranges, partSelOffset);
      // Chop off all the preselected top most dimensions except the
      // last one (which corresponds to part-select).
      const NUMemselRvalue* arg0 =
        dynamic_cast<const NUMemselRvalue*>(concatExpr->getArg(0));
      UInt32 numSelects = arg0->getNumDims();
      for (UInt32 dim = 0; dim < numSelects-1; ++dim) {
        ranges.pop_back();
      }
      ConstantRange psRange;
      bool success = selectsToRange(partSelIndices, psRange);
      NU_ASSERT(success, expr);
      maybeApplyPartSelect(psRange, ranges, partSelOffset, expr);
      break;
    }
    default:
    {
      // Other expression types are not supported.
      mSuccess = false;
      mMsgCtx->ErrorResolvingVhdlAliases(&(expr->getLoc()),
                                         "Unsupported alias replacement expression type.");
      break;
    }
    } // switch
    return ident;
  }

  NUExpr* getNetRepl(const NUNet* net)
  {
    NUExprReplacementMap::iterator itr = mReplMap.find(net);
    if (itr != mReplMap.end()) { // alias net
      return itr->second;
    }
    return NULL;
  }

  // Are the net ranges already mapped? Ignore the nets
  // like bit nets that don't have ranges.
  bool isMapped(const NUNet* net, const NUExpr* repl)
  {
    if (net->isCompositeNet()) {
      const NUCompositeNet* cn = net->getCompositeNet();
      if (cn->getNumDims() == 0) {
        return true;
      }
    } else if (!net->isVectorNet() && !net->isMemoryNet()) {
      return true;
    }
    Folder fd(mFold);
    if (fd.isConstant(repl)) {
      return true;
    }
    return mAliasInfoMap.find(net) != mAliasInfoMap.end();
  }

  bool computeOffsets(NUExpr* repl, Fold* fold, MsgContext* msgCtx)
  {
    SelectOffsetCB socb(mReplMap, mAliasInfoMap, fold, msgCtx);
    NUDesignWalker walker(socb, false);
    walker.expr(repl);
    return socb.getSuccess();
  }

  NUExprReplacementMap& mReplMap;
  NetAliasInfoMap& mAliasInfoMap;
  Fold* mFold;
  MsgContext* mMsgCtx;
  bool  mSuccess;
};

// When aliases are resolved to originals, the alias selects also have to
// be resolved. This involves offsetting the select to fall in the
// original net range. This class creates a map of index to it's
// offsetted replacement using the range mapping created by SelectOffsetCB.
// It also replaces the index with offsetted index.
class SelectReplacerCB : public NuToNuFn
{
public:

  SelectReplacerCB(SelectOffsetCB::NetAliasInfoMap& aliasInfoMap)
    : mAliasInfoMap(aliasInfoMap)
  {
  }

  virtual ~SelectReplacerCB()
  {
    if (!mIndexMap.empty()) {
      NUExprExprMap::iterator itr = mIndexMap.begin();
      NU_ASSERT2(0, itr->first, itr->second);
    }
    for (UtList<NUUseNode*>::iterator itr = mDeleteList.begin();
         itr != mDeleteList.end(); ++itr) {
      delete *itr;
    }
  }

  virtual NULvalue * operator()(NULvalue * lvalue, Phase phase)
  {
    NULvalue* repl = NULL;

    if (phase == ePre)
    {
      NUType lvalType = lvalue->getType();
      switch (lvalType)
      {
      case eNUVarselLvalue:
      {
        NUVarselLvalue* varsel = dynamic_cast<NUVarselLvalue*>(lvalue);
        NUNet* ident = varsel->getIdentNet(false);
        const AliasInfo* ai = getAliasInfo(ident);
        if (ai != NULL) { // select on an alias net.
          // We need to cover following cases:
          // x[3]   => isConstIndex() && (getRange() == ConstantRange(0,0))
          // x[i]   => !isConstIndex() && (getRange() == ConstantRange(0,0))
          // x[3:2] => isConstIndex() && (getRange() != ConstantRange(0,0))
          // x[i+3:i+1] => !isConstIndex() && (getRange() != ConstantRange(0,0))
          // x[j][3] => isArraySelect() && isConstIndex() && (getRange() == ConstantRange(0,0))
          // x[j][i] => isArraySelect() && !isConstIndex() && (getRange() == ConstantRange(0,0))
          // x[j][3:2] => isArraySelect() && isConstIndex() && (getRange() != ConstantRange(0,0))
          // x[j][i+3:i+1] => isArraySelect() && !isConstIndex() && (getRange() != ConstantRange(0,0))

          bool indexInvalid = false;
          if (Folder::isPartSelect(varsel))
          {
            const ConstantRange* rge = varsel->getRange();
            ConstantRange offsettedRange = offsetRange(rge, 0, ai, true);
            CopyContext cc(0,0);
            NULvalue* identLval = varsel->getLvalue();
            NULvalue* identLvalCpy = identLval->copy(cc);

            // For expressions like x[3:2], there's a range but no index. The population
            // phase (Verilog and VHDL) incorrectly creates varsels with 0 index, instead
            // of using the appropriate varsel constructor. This code is protecting against
            // that - Bug 7965.
            // When the bug is fixed, varsel's mIsIndexInBounds flag can be used here.
            if (varsel->isConstIndex()) {
              indexInvalid = true;
              repl = new NUVarselLvalue(identLvalCpy, offsettedRange,
                                        varsel->getLoc());
            } else {
              CopyContext cc(0,0);
              NUExpr* indexCpy = varsel->getIndex()->copy(cc);
              repl = new NUVarselLvalue(identLvalCpy, indexCpy, offsettedRange, 
                                        varsel->getLoc());
            }
          }

          if (!indexInvalid)
          {
            NUExpr* index = NULL;
            if (repl != NULL) {
              index = (dynamic_cast<NUVarselLvalue*>(repl))->getIndex();
            } else {
              index = varsel->getIndex();
            }
            // The identifier is alias that will be replaced. Create a
            // replacement for index with offsetted index.
            maybeAddIndexRepl(index, 0, ai, true);
          }
        }
        break;
      }
      case eNUMemselLvalue:
      {
        NUMemselLvalue* memsel = dynamic_cast<NUMemselLvalue*>(lvalue);
        NUNet* ident = memsel->getIdent();
        const AliasInfo* ai = getAliasInfo(ident);
        if (ai != NULL) // select on an alias net.
        {
          // The identifier is alias that will be replaced. Create a
          // replacement for index in each dimension with offsetted index.
          for (UInt32 dim = 0; dim < memsel->getNumDims(); ++dim)
          {
            NUExpr* idxExpr = memsel->getIndex(dim);
            maybeAddIndexRepl(idxExpr, dim, ai, false);
          }
        }
        break;
      }
      case eNUCompositeSelLvalue:
      {
        NUCompositeSelLvalue* csel =
          dynamic_cast<NUCompositeSelLvalue*>(lvalue);
        NUNet* ident = csel->getNet();
        const AliasInfo* ai = getAliasInfo(ident);
        if (ai != NULL) // select on an alias net.
        {
          UInt32 numIndices = csel->getNumIndices();
          if (csel->isPartSelect())
          {
            const ConstantRange psRange = csel->getPartSelRange();
            UInt32 psDim = csel->getNumDims() - numIndices;
            ConstantRange offsettedRange = offsetRange(&psRange, psDim, ai, false);
            csel->setPartSelRange(offsettedRange);
            if (csel->isPartSelectIndexDummy()) {
              // Avoid offsetting dummy index for part-selected dimension.
              --numIndices;
            }
          }

          // The identifier is alias that will be replaced. Create a
          // replacement for index in each dimension with offsetted index.
          for (UInt32 dim = 0; dim < numIndices; ++dim)
          {
            NUExpr* idxExpr = csel->getIndex(dim);
            maybeAddIndexRepl(idxExpr, dim, ai, false);
          }
        }
        break;
      }
      default:
        break;
      }
    } // if

    if (repl != NULL) {
      mDeleteList.push_back(lvalue);
    }

    return repl;
  }

  virtual NUExpr * operator()(NUExpr * rvalue, Phase phase)
  {
    NUExpr* repl = NULL;

    if (phase == ePre)
    {
      NUExpr::Type rvalType = rvalue->getType();
      switch (rvalType)
      {
      case NUExpr::eNUVarselRvalue:
      {
        NUVarselRvalue* varsel = dynamic_cast<NUVarselRvalue*>(rvalue);
        NUNet* ident = varsel->getIdentNet(false);
        const AliasInfo* ai = getAliasInfo(ident);
        if (ai != NULL) { // select on an alias net.
          bool indexInvalid = false;
          if (Folder::isPartSelect(varsel))
          {
            const ConstantRange* rge = varsel->getRange();
            ConstantRange offsettedRange = offsetRange(rge, 0, ai, true);
            CopyContext cc(0,0);
            NUExpr* identExpr = varsel->getIdentExpr();
            NUExpr* identExprCpy = identExpr->copy(cc);
            if (varsel->isConstIndex()) {
              indexInvalid = true;
              repl = new NUVarselRvalue(identExprCpy, offsettedRange,
                                        varsel->getLoc());
            } else {
              CopyContext cc(0,0);
              NUExpr* indexCpy = varsel->getIndex()->copy(cc);
              repl = new NUVarselRvalue(identExprCpy, indexCpy, offsettedRange, 
                                        varsel->getLoc());
            }
          }

          if (!indexInvalid) {
            NUExpr* index = NULL;
            if (repl != NULL) {
              index = (dynamic_cast<NUVarselRvalue*>(repl))->getIndex();
            } else {
              index = varsel->getIndex();
            }
            // The identifier is alias that will be replaced. Create a
            // replacement for index with offsetted index.
            maybeAddIndexRepl(index, 0, ai, true);
          }
        }
        break;
      }
      case NUExpr::eNUMemselRvalue:
      {
        NUMemselRvalue* memsel = dynamic_cast<NUMemselRvalue*>(rvalue);
        NUNet* ident = memsel->getIdent();
        const AliasInfo* ai = getAliasInfo(ident);
        if (ai != NULL) // select on an alias net.
        {
          // The identifier is alias that will be replaced. Create a
          // replacement for index in each dimension with offsetted index.
          for (UInt32 dim = 0; dim < memsel->getNumDims(); ++dim)
          {
            NUExpr* idxExpr = memsel->getIndex(dim);
            maybeAddIndexRepl(idxExpr, dim, ai, false);
          }
        }
        break;
      }
      case NUExpr::eNUCompositeSelExpr:
      {
        NUCompositeSelExpr* csel =
          dynamic_cast<NUCompositeSelExpr*>(rvalue);
        NUNet* ident = csel->getNet();
        const AliasInfo* ai = getAliasInfo(ident);
        if (ai != NULL) // select on an alias net.
        {
          UInt32 numIndices = csel->getNumIndices();
          if (csel->isPartSelect())
          {
            const ConstantRange psRange = csel->getPartSelRange();
            UInt32 psDim = csel->getNumDims() - numIndices;
            ConstantRange offsettedRange = offsetRange(&psRange, psDim, ai, false);
            csel->setPartSelRange(offsettedRange);
            if (csel->isPartSelectIndexDummy()) {
              // Avoid offsetting dummy index for part-selected dimension.
              --numIndices;
            }
          }

          // The identifier is alias that will be replaced. Create a
          // replacement for index in each dimension with offsetted index.
          for (UInt32 dim = 0; dim < numIndices; ++dim)
          {
            NUExpr* idxExpr = csel->getIndex(dim);
            maybeAddIndexRepl(idxExpr, dim, ai, false);
          }
        }
        break;
      }
      default:
      {
        // If this expr is an index to be replaced, replace it and
        // add original to delete list.
        NUExprExprMap::iterator itr = mIndexMap.find(rvalue);
        if (itr != mIndexMap.end()) {
          repl = itr->second;
          mIndexMap.erase(itr);
        }
        break;
      }
      } // switch
    } // if

    if (repl != NULL) {
      mDeleteList.push_back(rvalue);
    }

    return repl;
  }

private:

  void maybeAddIndexRepl(NUExpr* idxExpr, UInt32 dim, const AliasInfo* ai,
                         bool isVarsel)
  {
    bool isFlipped = false;
    SInt32 offset = ai->getOffset(dim, isVarsel, &isFlipped);
    NUExpr* offsettedExpr = offsetIndex(offset, isFlipped, idxExpr);
    if (offsettedExpr != NULL) {
      mIndexMap.insert(NUExprExprMap::value_type(idxExpr, offsettedExpr));
    }
  }

  ConstantRange offsetRange(const ConstantRange* rge, UInt32 dim,
                            const AliasInfo* ai, bool isVarsel) const
  {
    bool isFlipped = false;
    SInt32 offset = ai->getOffset(dim, isVarsel, &isFlipped);
    SInt32 msb = offsetIndex(offset, isFlipped, rge->getMsb());
    SInt32 lsb = offsetIndex(offset, isFlipped, rge->getLsb());
    return ConstantRange(msb, lsb);
  }

  const AliasInfo* getAliasInfo(const NUNet* net)
  {
    SelectOffsetCB::NetAliasInfoMapIter itr = mAliasInfoMap.find(net);
    if (itr != mAliasInfoMap.end()) {
      return &(itr->second);
    }
    return NULL;
  }

  NUExpr* offsetIndex(SInt32 offset, bool isFlipped, NUExpr* index)
  {
    CopyContext cc(0,0);
    NUExpr* offsettedIndex = NULL;

    if ((offset != 0) || isFlipped)
    {
      NUExpr* indexCpy = index->copy(cc);
      // Index expressions are resized to be 32 bits wide.
      NUExpr* offsetExpr = NUConst::create(true, offset, 32, index->getLoc());
      if (isFlipped) {
        // [offset - idx]
        offsettedIndex = new NUBinaryOp(NUOp::eBiMinus, offsetExpr, indexCpy,
                                        index->getLoc());
      } else {
        // [idx - offset]
        offsettedIndex = new NUBinaryOp(NUOp::eBiPlus, indexCpy, offsetExpr,
                                        index->getLoc());
      }
    }
         
    return offsettedIndex;
  }
  
  SInt32 offsetIndex(SInt32 offset, bool isFlipped, SInt32 index) const
  {
    SInt32 offsettedIndex = index;
    if ((offset != 0) || isFlipped)
    {
      if (isFlipped) {
        offsettedIndex = offset - index;
      } else {
        offsettedIndex = index + offset;
      }
    }
    return offsettedIndex;
  }

  SelectOffsetCB::NetAliasInfoMap mAliasInfoMap;
  NUExprExprMap mIndexMap;
  UtList<NUUseNode*> mDeleteList;
};

// Replaces alias nets with their original expressions. It also resolves
// illegal nucleus objects created as a result of replacement to original.
class ExprReplaceCB : public NuToNuFn
{
public:
  
  ExprReplaceCB(NUExprReplacementMap& replMap, NULvalueSet& skipLvals,
                Fold* fold, MsgContext* msgCtx)
    : mReplMap(replMap),
      mSkipLvals(skipLvals),
      mFold(fold),
      mMsgCtx(msgCtx),
      mSuccess(true)
  {
  }

  virtual ~ExprReplaceCB()
  {
    for (UtList<NUUseNode*>::iterator itr = mDeleteList.begin();
         itr != mDeleteList.end(); ++itr) {
      delete *itr;
    }
  }

  virtual NULvalue * operator()(NULvalue * lvalue, Phase phase)
  {
    if (!mSuccess || (mSkipLvals.find(lvalue) != mSkipLvals.end())) {
      return NULL; // Skip lvalue.
    }

    NULvalue* repl = NULL;
    NUType lvalType = lvalue->getType();
    
    switch (lvalType)
    {
    case eNUIdentLvalue:
    {
      if (phase == ePre) {
        NUIdentLvalue* identLval = dynamic_cast<NUIdentLvalue*>(lvalue);
        repl = getReplLvalue(identLval->getIdent(), identLval->getLoc());
      }
      break;
    }
    case eNUCompositeIdentLvalue:
    {
      if (phase == ePre) {
        NUCompositeIdentLvalue* compIdentLval =
          dynamic_cast<NUCompositeIdentLvalue*>(lvalue);
        repl = getReplLvalue(compIdentLval->getIdent(), compIdentLval->getLoc());
      }
      break;
    }
    case eNUVarselLvalue:
    case eNUMemselLvalue:
    case eNUCompositeSelLvalue:
    {
      if (phase == ePost) {
        Folder fd(mFold);
        mSuccess = fd.foldLvalue(lvalue, &repl);
      }
      break;
    }
    default:
      break;
    }

    if (repl != NULL) {
      mDeleteList.push_back(lvalue);
    }

    return repl;
  }

  virtual NUExpr * operator()(NUExpr * rvalue, Phase phase)
  {
    NUExpr* repl = NULL;

    if (!mSuccess) {
      return repl;
    }

    const NUNet*  aliasNet = NULL;
    NUExpr::Type exprType = rvalue->getType();

    switch (exprType)
    {
    case NUExpr::eNUIdentRvalue:
    {
      if (phase == ePre) {
        NUIdentRvalue* identRval = dynamic_cast<NUIdentRvalue*>(rvalue);
        aliasNet = identRval->getIdent();
        repl = getReplRvalue(aliasNet);
      }
      break;
    }
    case NUExpr::eNUCompositeIdentRvalue:
    {
      if (phase == ePre) {
        NUCompositeIdentRvalue* compIdentRval = 
          dynamic_cast<NUCompositeIdentRvalue*>(rvalue);
        aliasNet = compIdentRval->getIdent();
        repl = getReplRvalue(aliasNet);
      }
      break;
    }
    case NUExpr::eNUVarselRvalue:
    case NUExpr::eNUMemselRvalue:
    case NUExpr::eNUCompositeSelExpr:
    {
      if (phase == ePost) {
        Folder fd(mFold);
        mSuccess = fd.foldExpr(rvalue, &repl);
      }
      break;
    }
    default:
      break;
    } // switch

    if (repl != NULL) {
      mDeleteList.push_back(rvalue);
    }

    if (!mSuccess) {
      mMsgCtx->ErrorResolvingVhdlAliases(&(rvalue->getLoc()),
                                         "Unsupported expression type found.");
    }

    return repl;
  }
    
  bool getSuccess() const {
    return mSuccess;
  }

private:

  NULvalue* getReplLvalue(const NUNet* net, const SourceLocator& loc)
  {
    NULvalue* repl = NULL;
    const NUExpr* replRvalue = getNetRepl(net);
    if (replRvalue != NULL) {
      repl = replRvalue->Lvalue(loc);
    }
    return repl;
  }

  NUExpr* getReplRvalue(const NUNet* net)
  {
    NUExpr* repl = NULL;
    const NUExpr* replExpr = getNetRepl(net);
    if (replExpr != NULL) {
      CopyContext cc(0,0);
      repl = replExpr->copy(cc);
    }
    return repl;
  }

  const NUExpr* getNetRepl(const NUNet* net)
  {
    const NUExpr* repl = NULL;
    NUExprReplacementMap::iterator itr = mReplMap.find(net);
    if (itr != mReplMap.end()) {
      repl = recurseResolve(itr->second);
    }
    return repl;
  }

  // This resolves the case where an alias identifier resolves to another
  // alias identifer. For example: alias foo is bar; alias foo1 is foo;
  // The foo1's replacement is foo. This method resolves it to bar.
  const NUExpr* recurseResolve(const NUExpr* expr)
  {
    const NUNet* ident = NULL;
    if (expr->getType() == NUExpr::eNUIdentRvalue) {
      ident = (dynamic_cast<const NUIdentRvalue*>(expr))->getIdent();
    } else if (expr->getType() == NUExpr::eNUCompositeIdentRvalue) {
      ident = (dynamic_cast<const NUCompositeIdentRvalue*>(expr))->getIdent();
    }
    const NUExpr* repl = expr;
    if (ident != NULL)
    {
      const NUExpr* multiLevelRepl = getNetRepl(ident);
      if (multiLevelRepl != NULL) {
        repl = multiLevelRepl;
      }
    }
    return repl;
  }

  UtList<NUUseNode*> mDeleteList;
  NUExprReplacementMap& mReplMap;
  NULvalueSet& mSkipLvals;
  Fold* mFold;
  MsgContext* mMsgCtx;
  bool mSuccess;
};

// Creates continuous assign statements to assign original expressions to
// alias nets. This is done for visibility purposes. If user adds an
// observeSignal on an alias net, this statement will ensure that the
// alias net is observable.
class AliasAssigner
{
public:

  AliasAssigner(AtomicCache* strCache, NULvalueSet& skipLvals)
    : mStrCache(strCache), mSkipLvals(skipLvals)
  {
  }

  ~AliasAssigner()
  {
  }

  void addAssigns(NUExprReplacementMap& replMap, NUModule* mod)
  {
    for (NUExprReplacementMap::iterator itr = replMap.begin();
         itr != replMap.end(); ++itr)
    {
      // Create lvalue.
      NUNet* aliasNet = const_cast<NUNet*>(itr->first);
      NUExpr* rval = itr->second;
      // Get the scope in which the alias net is declared.
      NUScope* aliasScope = aliasNet->getScope();
      // Assign statements to update the alias from the original net
      // are needed to allow user to add directives on aliases to make
      // them visible. However aliases that in task/function scopes
      // (transient scopes) are not visible. An assign statement is not
      // needed for them.
      if (!aliasScope->inTFHier())
      {
        // Need to create a new ident lvalue from alias net.
        NULvalue* lval = NULL;
        if (aliasNet->isCompositeNet()) {
          NUCompositeNet* cn = aliasNet->getCompositeNet();
          lval = new NUCompositeIdentLvalue(cn, aliasNet->getLoc());
        } else {
          lval = new NUIdentLvalue(aliasNet, aliasNet->getLoc());
        }
        StringAtom *blockName = mod->newBlockName(mStrCache,
                                                  aliasNet->getLoc());
        CopyContext cc(0,0);
        NUExpr* rvalCpy = rval->copy(cc);
        NUContAssign *aliasAssign = 
          new NUContAssign(blockName, lval, rvalCpy, aliasNet->getLoc());
        mod->addContAssign(aliasAssign);
        lval = aliasAssign->getLvalue();
        mSkipLvals.insert(lval);
      }
    }
  }
    
private:
  AtomicCache* mStrCache;
  NULvalueSet& mSkipLvals;
};

// Checks to ensure that no lvalues and rvalues that use aliases are left
// unresolved ..except for the lvalues that are specifically used to update
// aliases. Those are listed in skipLvals map.
class AliasSanityChecker : public NUDesignCallback
{
public:

  AliasSanityChecker(SelectOffsetCB::NetAliasInfoMap& aliasInfoMap,
                     NULvalueSet& skipLvals, MsgContext* msgCtx)
    : mAliasInfoMap(aliasInfoMap), mSkipLvals(skipLvals), mMsgCtx(msgCtx),
      mCheckFailed(false)
  {
  }

  Status operator() (Phase , NUBase* ) { return eNormal; }

  Status operator() (Phase phase, NUIdentRvalue* expr)
  {
    if (phase == ePre) {
      return checkNet(expr->getIdent(), expr, NULL);
    }
    return eNormal;
  }

  Status operator() (Phase phase, NUIdentLvalue* lval)
  {
    if (phase == ePre) {
      return checkNet(lval->getIdent(), NULL, lval);
    }
    return eNormal;
  }

  Status operator() (Phase phase, NUCompositeIdentRvalue* expr)
  {
    if (phase == ePre) {
      return checkNet(expr->getNet(), expr, NULL);
    }
    return eNormal;
  }

  Status operator() (Phase phase, NUCompositeIdentLvalue* lval)
  {
    if (phase == ePre) {
      return checkNet(lval->getNet(), NULL, lval);
    }
    return eNormal;
  }

  bool getCheckFailed() { return mCheckFailed; }

private:

  Status checkNet(NUNet* net, NUExpr* expr, NULvalue* lval)
  {
    const SourceLocator* loc = NULL;
    bool isLval = false;

    if (lval != NULL)
    {
      if (mSkipLvals.find(lval) != mSkipLvals.end()) {
        return eSkip; // Skip this lvalue.
      }
      loc = &(lval->getLoc());
      isLval = true;
    }
    else
    {
      loc = &(expr->getLoc());
    }

    if (mAliasInfoMap.find(net) != mAliasInfoMap.end())
    {
      mMsgCtx->AliasResolutionSanityCheckFailed(loc, isLval ? "lvalue" : "expression");
      mCheckFailed = true;
      return eStop;
    }
    return eNormal;
  }

  bool skipLvalue(NULvalue* lval)
  {
    return (mSkipLvals.find(lval) != mSkipLvals.end());
  }

  SelectOffsetCB::NetAliasInfoMap mAliasInfoMap;
  NULvalueSet& mSkipLvals;
  MsgContext*  mMsgCtx;
  bool mCheckFailed;
};

AliasResolver::AliasResolver(MsgContext* msgCtx, AtomicCache* strCache,
                             Fold* fold)
  : mMsgCtx(msgCtx), mStrCache(strCache), mFold(fold)
{
}

AliasResolver::~AliasResolver()
{
}

VhdlPopulate::ErrorCode
AliasResolver::design(NUDesign* design, VhdlPopulate::EntityAliasMap* aliasMap)
{
  bool success = true;
  Populate::ErrorCode err_code = Populate::eSuccess;

  NUModuleList mods;
  design->getAllModules(&mods);

  // Resolve aliases in all modules in the design.
  for (NUModuleListIter itr = mods.begin(); success && itr != mods.end(); ++itr)
  {
    NUModule* module = *itr;
    VhdlPopulate::EntityAliasMap::iterator mapItr = aliasMap->find(module);
    // If the module has an alias map, it has aliases to resolve.
    if (mapItr != aliasMap->end())
    {
      NUExprReplacementMap& replMap = mapItr->second;
      if (!replMap.empty())
      {
        // Step 1: Build the map of alias net ranges to replacement expression
        //         ranges for every alias net dimension.
        SelectOffsetCB::NetAliasInfoMap aliasInfoMap;
        for (NUExprReplacementMap::iterator itr = replMap.begin();
             success && itr != replMap.end(); ++itr) {
          success &= SelectOffsetCB::computeOffsets(replMap, aliasInfoMap,
                                                    itr->first, mFold, mMsgCtx);
        }

        // Step 2 : Create assign statements that assign replacement
        //          expressions to alias nets.
        NULvalueSet skipLvals;
        // Add lvalues in the assign statements to skip map so that
        // they don't get resolved to original. 
        if (success) {
          AliasAssigner aliasAssigner(mStrCache, skipLvals);
          aliasAssigner.addAssigns(replMap, module);
        }

        // Step 3a : Using range map, modify the selects of all alias nets
        //           such that they index into the original net.
        if (success) {
          SelectReplacerCB srcb(aliasInfoMap);
          module->replaceLeaves(srcb);
        }

        // Step 3b : Using range map, modify the selects of all alias nets
        //           in the alias replacements such that they index into
        //           the original net.
        for (NUExprReplacementMap::iterator itr = replMap.begin();
             success && itr != replMap.end(); ++itr)
        {
          NUExpr* repl = itr->second;
          SelectReplacerCB srcb(aliasInfoMap);

          NUExpr::replaceExpr(&repl, NuToNuFn::ePre, srcb);
          repl->replaceLeaves(srcb);
          NUExpr::replaceExpr(&repl, NuToNuFn::ePost, srcb);

          itr->second = repl;
        }

        // Step 4 : Replace all uses and defs of alias nets (except the skipped
        //          lvalues) with original nets.
        if (success) {
          ExprReplaceCB ercb(replMap, skipLvals, mFold, mMsgCtx);
          module->replaceLeaves(ercb);
          success &= ercb.getSuccess();
        }

        // Step 5 : Perform a sanity check to ensure that all expressions and
        //          lvalues that contain alias nets have been resolved.
        if (success) {
          // AliasInfo map is used for sanity checking since the nets that are
          // keys in this map are the only ones that have been resolved.
          AliasSanityChecker asc(aliasInfoMap, skipLvals, mMsgCtx);
          NUDesignWalker walker(asc, false);
          walker.module(module);
          if (asc.getCheckFailed()) {
            success = false; // Error message has been printed by sanity checker already.
          }
        }
      } // if
    } // if

    if (!success) {
      // Resolution of aliases in this module failed.
      POPULATE_FAILURE(mMsgCtx->FailedToResolveVhdlAliases(&(module->getLoc()),
                       module->getOriginalName()->str()), &err_code);
    }
  }

  return err_code;
}
