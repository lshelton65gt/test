// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Declaration of latch cycle reduction process.
 */

#ifndef _REMOVELATCHCYCLES_H_
#define _REMOVELATCHCYCLES_H_

class NUNetRefFactory;
class MsgContext;
class IODBNucleus;
class ArgProc;
class NUModule;
class NUContAssign;
class NUAlwaysBlock;

//! Class to perform an optimization pass designed to reduce the number of cycles with certain latch encodings:
/*!

  If a continuous assignment is coded with an explicit hold:

  \code
    assign net = sel ? data : net;
  \endcode

  this code will transform the continuous assignment into a
  combinational always block:
  \code
    always 
    begin
        if (sel)
          net = data;
    end
  \endcode

  Currently handled:
  assign net = en ? net : data;
  assign net = en ? data : net;

  Not currently handled:
  assign net = en ? net : 1'bz;
  assign net = en1 ? en2 ? net : data2 : data1;
 */
class RemoveLatchCycles
{
public:
  //! Constructor
  RemoveLatchCycles(NUNetRefFactory * netref_factory,
                    MsgContext * msg_ctx,
                    IODBNucleus * iodb,
                    ArgProc* args);

  //! Destructor
  ~RemoveLatchCycles();

  //! Process a module looking for continuous assigns with cycles.
  void module(NUModule * module);

private:
  //! Hide copy and assign constructors.
  RemoveLatchCycles(const RemoveLatchCycles&);
  RemoveLatchCycles& operator=(const RemoveLatchCycles&);

  //! Convert one cycle assign.
  NUAlwaysBlock * convertCycleAssign(NUModule * module, const NUContAssign * cassign);


  NUNetRefFactory * mNetRefFactory;
  MsgContext * mMsgContext;
  IODBNucleus * mIODB;
  ArgProc * mArgs;
};

#endif
