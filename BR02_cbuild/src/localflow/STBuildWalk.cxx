// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/STBuildWalk.h"
#include "localflow/DesignPopulate.h"
#include "localflow/PrePopInterraCB.h"
#include "localflow/TicProtectedNameManager.h"
#include "util/UtStackPOD.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "iodb/IODBDesignData.h"
#include "LFContext.h"

//! Helper to STBuildWalkCB.
/*!
  Helps add branch/leaf nodes to elab/unelab symtabs.
*/
class STBuildWalkHelper
{
public:

  enum UnelabScopeT {
    eModule, eTask, eNamedDecl
  };

  STBuildWalkHelper(IODBDesignDataSymTabs* symtabs)
    : mSymTabs(symtabs), mSavedPrePopScope(NULL)
  {
  }

  ~STBuildWalkHelper() {
  }

  bool addNet(StringAtom* netName)
  {
    // No current scope to add net to.
    if (mUnelabScopes.empty()) {
      return false;
    }
    mSymTabs->createNode(mUnelabScopes.top(), netName, 
                         IODBDesignDataSymTabs::eUnelab,
                         IODBDesignDataSymTabs::eLeaf);
    mSymTabs->createNode(mElabScopes.top(), netName,
                         IODBDesignDataSymTabs::eElab,
                         IODBDesignDataSymTabs::eLeaf);
    return true;
  }

  STBranchNode* pushUnelabScope(StringAtom* unelab, UnelabScopeT scopeType)
  {
    STBranchNode* parent = NULL;
    if (!mUnelabScopes.empty() && (scopeType != eModule)) {
      parent = mUnelabScopes.top();
    }
    STSymbolTableNode* node =
      mSymTabs->createNode(parent, unelab, IODBDesignDataSymTabs::eUnelab,
                           IODBDesignDataSymTabs::eBranch);
    STBranchNode* newUnelabScope = node->castBranch();
    mUnelabScopes.push(newUnelabScope);

    if (mElabScopes.empty()) {
      // push the root elaborated branch.
      pushElabScope(unelab);
    }
    return newUnelabScope;
  }

  void popUnelabScope()
  {
    if (!mUnelabScopes.empty()) {
      mUnelabScopes.pop();
      if (mUnelabScopes.empty()) {
        // pop the root elaborated branch.
        popElabScope();
      }
    }
  }

  STBranchNode* pushElabScope(StringAtom* elab)
  {
    STBranchNode* parent = NULL;
    if (!mElabScopes.empty()) {
      parent = mElabScopes.top();
    }
    STSymbolTableNode* node =
      mSymTabs->createNode(parent, elab, IODBDesignDataSymTabs::eElab,
                           IODBDesignDataSymTabs::eBranch);
    STBranchNode* newElabScope = node->castBranch();
    mElabScopes.push(newElabScope);

    // The current unelaborated scope is the unelaborated scope corresponding
    // to this elaborated scope.
    IODBDesignNodeData* scopeInfo = IODBDesignDataBOM::castBOM(newElabScope);
    // Establish a link from elaborated ST branch node to unelaborated ST
    // branch node.
    STBranchNode* unelabScope = mUnelabScopes.top();
    scopeInfo->putUnelabScope(unelabScope);

    return newElabScope;
  }

  void popElabScope()
  {
    if (!mElabScopes.empty()) {
      mElabScopes.pop();
    }
  }

  void saveStacksAndBeginAtRoot(STBranchNode* prePopScope)
  {
    mUnelabScopes.saveStackAndBeginAtRoot();
    mElabScopes.saveStackAndBeginAtRoot();
    mSavedPrePopScope = prePopScope;
  }

  STBranchNode* restoreStacks()
  {
    mUnelabScopes.restoreStack();
    mElabScopes.restoreStack();
    STBranchNode* prePopScope = mSavedPrePopScope;
    mSavedPrePopScope = NULL;
    return prePopScope;
  }

  STSymbolTableNode* findNode(StringAtom* child, IODBDesignDataSymTabs::SymTabT stTyp)
  {
    STBranchNode* parent = mElabScopes.top();
    if (stTyp == IODBDesignDataSymTabs::eUnelab) {
      parent = mUnelabScopes.top();
    }
    return mSymTabs->findNode(parent, child, stTyp);
  }

  STBranchNode* getUnelabScope()
  {
    STBranchNode* unelabScope = NULL;
    if (!mUnelabScopes.empty()) {
      unelabScope = mUnelabScopes.top();
    }
    return unelabScope;
  }

private:
  IODBDesignDataSymTabs* mSymTabs;
  BranchStack mUnelabScopes;
  BranchStack mElabScopes;
  STBranchNode* mSavedPrePopScope;
};


// STBuildWalkCB class creates unelaborated and elaborated symbol table which
// has all scope names and the declarated net names. Associated with scopes
// and net names is the design information object which stores directives
// for now. In future it may contain user design type information.

STBuildWalkCB::STBuildWalkCB(DesignPopulate* populator, 
                             PrePopInterraCB* prePopCB,
                             IODBDesignDataSymTabs* symtabs)
  : mErrCode(Populate::eSuccess), mPopulator(populator), mPrePopInfo(prePopCB),
    mHelper(NULL), mPrePopElabScope(NULL), mLFContext(NULL)
{
  mHelper = new STBuildWalkHelper(symtabs);
  mLFContext = mPopulator->getLFContext();
}

STBuildWalkCB::~STBuildWalkCB()
{
  delete mHelper;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::hdlEntity(Phase phase, mvvNode, mvvLanguageType langType,
                         const char* entityName, bool* isCmodel)
{
  if (phase == ePre)
  {
    IODBNucleus* iodb = mLFContext->getIODB();
    StringAtom *entityAtom = mLFContext->hdlIntern(entityName, langType);
    *isCmodel = iodb->isCModel(entityAtom->str());

    mHelper->pushUnelabScope(entityAtom, STBuildWalkHelper::eModule);

    if (mPrePopElabScope == NULL) {
      // For root module, elab/unelab names are same and added by symbol tables
      // automatically. We only need to setup the elaborated scope branch node.
      mPrePopElabScope = mPrePopInfo->findElabScope(NULL, entityName, langType);
      mHelper->findNode(entityAtom, IODBDesignDataSymTabs::eElab);
    }
  }
  else if (phase == ePost)
  {
    mHelper->popUnelabScope();
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::blockStmt(Phase phase, mvvNode, const char* blockName, mvvLanguageType langType)
{
  if (langType == MVV_VHDL)
  {
    if (phase == ePre) {
      pushElabScope(blockName, langType);
    } else if (phase == ePost) {
      popElabScope();
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::forGenerate(Phase phase, mvvNode node, mvvNode indexVal, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhExpr vh_expr = (vhExpr)indexVal;
      StringAtom* forGen = NULL;
      Populate::ErrorCode tmp_err_code = mPopulator->mVhdlPopulate.calcForGenerateBlockName(node->castVhNode(), vh_expr, mLFContext, &forGen);
      if ( tmp_err_code != Populate::eSuccess ) {
        mErrCode = tmp_err_code;
        return eStop;
      }
      pushElabScope(forGen);
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VHDL) {
      popElabScope();
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::ifGenerateTrue(Phase phase, mvvNode /*node*/, const char* blockLabel, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (blockLabel) {
      pushElabScope(blockLabel, langType);
    }
  }
  else if (phase == ePost)
  {
    if (blockLabel) {
      popElabScope();
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::generateBlock(Phase phase, mvvNode /*node*/, 
                             const char* blockName, 
                             mvvLanguageType langType, 
                             const SInt32* /*genForIndex*/)
{
  if (strlen(blockName) == 0)
    return eNormal;

  if (phase == ePre)
  {
    if (langType == MVV_VERILOG) {
      pushElabScope(blockName, langType);
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VERILOG) {
      popElabScope();
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::port(Phase phase, mvvNode node, mvvLanguageType langType)
{
  addNet(phase, node, langType);
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::packageDecl(Phase phase, mvvNode, mvvLanguageType langType,
                           const char* libName, const char* packName, bool isStdLib)
{
  if (isStdLib) {
    return eSkip;
  }

  if (langType == MVV_VHDL)
  {
    if (phase == ePre)
    {
      // The parent for package is root module. Save the current stacks and
      // begin at root. NOTE: Nested packages are not legal in VHDL.
      mHelper->saveStacksAndBeginAtRoot(mPrePopElabScope);
      mPrePopElabScope = 
        mPrePopInfo->findElabScope(NULL, mHelper->getUnelabScope()->strObject());
      // Push the library declaration/elaborated scope first. The library
      // name for declaration and elaborated scope are the same.
      UtString actualLibName;
      VhdlPopulate::getActualVhdlLibName(libName, actualLibName);
      pushElabAndUnelabScopes(actualLibName.c_str(), STBuildWalkHelper::eNamedDecl,
                              langType);
      // Package declaration scope.
      pushElabAndUnelabScopes(packName, STBuildWalkHelper::eNamedDecl, langType);
    }
    else if (phase == ePost)
    {
      // Backup from package and library.
      popElabAndUnelabScopes();
      popElabAndUnelabScopes();
      mPrePopElabScope = mHelper->restoreStacks();
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::net(Phase phase, mvvNode node, mvvLanguageType langType)
{
  addNet(phase, node, langType);
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::localNet(Phase phase, mvvNode node, mvvLanguageType langType)
{
  addNet(phase, node, langType);
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::variable(Phase phase, mvvNode node, mvvLanguageType langType)
{
  addNet(phase, node, langType);
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::alias(Phase phase, mvvNode node, mvvLanguageType langType)
{
  addNet(phase, node, langType);
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::file(Phase phase, mvvNode node, mvvLanguageType langType)
{
  addNet(phase, node, langType);
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::concProcess(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (langType == MVV_VHDL)
  {
    // Process is declaration scope. Add it to both unelab/elab symtabs.
    if (phase == ePre)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      StringAtom* proc_name = 
        mPrePopInfo->getUniqueVhdlProcessName(vh_conc_stmt);
      mHelper->pushUnelabScope(proc_name, STBuildWalkHelper::eNamedDecl);
      pushElabScope(proc_name);
    }
    else if (phase == ePost)
    {
      popElabScope();
      mHelper->popUnelabScope();
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::hdlComponentInstance(Phase phase, mvvNode,
                                 const char* instName,
                                 mvvNode, mvvNode, mvvNode,
                                 const char*, mvvNode, const SInt32*, 
                                 const UInt32*, mvvLanguageType, 
                                 mvvLanguageType scopeLang)
{
  if (phase == ePre) {
    pushElabScope(instName, scopeLang);
  } else if (phase == ePost) {
    popElabScope();
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
STBuildWalkCB::taskFunction(Phase phase, mvvNode /*node*/, 
                         const char* tfName, mvvLanguageType langType)
{
  if (phase == ePre) {
    pushElabScope(tfName, langType);
  } else if (phase == ePost) {
    popElabScope();
  }
  return eNormal;
}

mvvNode 
STBuildWalkCB::substituteEntity(mvvNode entityDecl, 
				mvvLanguageType entityLanguage,
                                const UtString& entityDeclName, 
				mvvNode instance,
                                bool isUDP, 
				mvvLanguageType callingLanguage)
{
  STBranchNode* parentEntity = mHelper->getUnelabScope();
  NUPortMap* portMap = NULL;
  bool found = false;
  UtString new_mod_name;
  UtString instanceName;
  Populate::composeInstanceName(instance, mLFContext, &instanceName, mPopulator->mVlogPopulate.getTicProtectedNameManager()); 

  mvvNode node =  getSubstituteEntity(parentEntity->str(),
				      entityDecl,
				      entityLanguage,
				      entityDeclName,
				      instanceName,
				      isUDP,
				      callingLanguage,
				      mPopulator->mVlogPopulate.getTicProtectedNameManager(),
				      mLFContext->getIODB(),
				      &new_mod_name,
				      &found,
				      &portMap);

  if (node == NULL && found) {
    mLFContext->getMsgContext()->SubstituteModuleNotFound(new_mod_name.c_str());
  }

  return node;
}

InterraDesignWalkerCB::Status
STBuildWalkCB::vlogForeignAttribute(mvvNode module, const UtString& entityDeclName, mvvNode* ent, mvvNode* arch) 
{
  InterraDesignWalkerCB::Status status = getEntArchPairFromForeignAttr(module, ent, arch);

  if (status == InterraDesignWalkerCB::eStop) {
    SourceLocator loc = mPopulator->mVlogPopulate.locator(module->castVeNode());
    mLFContext->getMsgContext()->EntityOrArchitectureSpecifiedInForeignAttributeNotFound(&loc, entityDeclName.c_str());
  }
  else if (status == InterraDesignWalkerCB::eInvalid) {
    SourceLocator loc = mPopulator->mVlogPopulate.locator(module->castVeNode());
    mLFContext->getMsgContext()->IncorrectForeignAttribute(&loc, entityDeclName.c_str());
    status = InterraDesignWalkerCB::eStop;
  }

  return status;
}


void STBuildWalkCB::addNet(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    StringAtom* name = NULL;
    if (langType == MVV_VERILOG)
    {
      veNode ve_net = node->castVeNode();
      UtString buf;
      mPopulator->mVlogPopulate.getVisibleName(ve_net, &buf);
      name = mLFContext->hdlIntern(buf.c_str(), langType);
    }
    else if (langType == MVV_VHDL)
    {
      vhNode vh_net = node->castVhNode();
      name = mLFContext->vhdlIntern(vhGetName(vh_net));
    }
    
    mHelper->addNet(name);
  } // pre
}

void STBuildWalkCB::pushElabScope(const char* scope, mvvLanguageType lang)
{
  StringAtom* scopeAtom = mLFContext->hdlIntern(scope, lang);
  pushElabScope(scopeAtom);
}

void STBuildWalkCB::pushElabScope(StringAtom* scopeAtom)
{
  mPrePopElabScope = 
    mPrePopInfo->findElabScope(mPrePopElabScope, scopeAtom);
  mHelper->pushElabScope(mPrePopElabScope->strObject());
}

void STBuildWalkCB::popElabScope()
{
  mHelper->popElabScope();
  mPrePopElabScope = mPrePopElabScope->getParent();
}

void 
STBuildWalkCB::pushElabAndUnelabScopes(const char* scope,
                                       SInt32 scopeType,
                                       mvvLanguageType lang)
{
  StringAtom* scopeAtom = mLFContext->hdlIntern(scope, lang);
  mHelper->pushUnelabScope(scopeAtom, static_cast<STBuildWalkHelper::UnelabScopeT>(scopeType));
  pushElabScope(scopeAtom);
}

void STBuildWalkCB::popElabAndUnelabScopes()
{
  popElabScope();
  mHelper->popUnelabScope();
}
