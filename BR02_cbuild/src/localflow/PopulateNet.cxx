// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "LFContext.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "reduce/Fold.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "iodb/IODBNucleus.h"
#include "util/ArgProc.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"

Populate::ErrorCode VerilogPopulate::netFlags(veNode ve_net,
                                              StringAtom* name,
                                              LFContext *context,
                                              NetFlags *flags)
{
  *flags = eNoneNet;
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(ve_net);

  veObjType obj_type = veNodeGetObjType(ve_net);
  bool isOutputVariableType = ((obj_type == VE_INTEGER) or (obj_type == VE_TIME));

  bool isPort = false;
  veDirectionType ve_dir(UNSET_DIRECTION);
  
  if ( isOutputVariableType and ( veVariableGetIsPort(ve_net) ) ){
      ve_dir = veVariableGetPortDirection(ve_net);
      isPort = true;
  } else if ( veNetGetIsPortNet(ve_net)) {
     ve_dir = veNetGetNetDirection(ve_net);
     isPort = true;
  }
  if ( isPort ) {
    switch (ve_dir) {
    case INPUT_DIR:
      *flags = eInputNet;
      break;
    case OUTPUT_DIR:
      *flags = eOutputNet;
      break;
    case INOUT_DIR:
      *flags = eBidNet;
      break;
    default:
      {
	POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown port direction"), &err_code);
      }
      break;
    }
  }


  // Let the user override inout nets to be inputs if desired
  NUModule* mod = dynamic_cast<NUModule*>(context->getDeclarationScope());
  if ((mod != NULL) && mIODB->isInputNet(mod->getName(), name))
    *flags = eInputNet;

//  if (veNetGetIsImplicit(ve_net)) {
//    flags = NetRedeclare(flags, eImplicitNet);
//  }
  
  if ( isOutputVariableType ){
    switch (obj_type){
    case VE_INTEGER:
    {
      *flags = NetRedeclare(*flags, eDMIntegerNet);
      break;
    }
    case VE_TIME:
    {
      *flags = NetRedeclare(*flags, eDMTimeNet);
      break;
    }
    default:
    {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown variable type"), &err_code);
      break;
    }
    }
  } else {
    switch (veNetGetNetType(ve_net)) {
    case UNSET_NET:
      //flags = NetFlags(flags | eNoneNet);
      break;

    case WIRE_NET:
      *flags = NetRedeclare(*flags, eDMWireNet);
      break;

    case TRI_NET:
      *flags = NetRedeclare(*flags, eDMTriNet);
      break;

    case TRI1_NET:
      *flags = NetRedeclare(*flags, eDMTri1Net);
      break;

    case SUPPLY0_NET:
      *flags = NetRedeclare(*flags, eDMSupply0Net);
      break;

    case WAND_NET:
      *flags = NetRedeclare(*flags, eDMWandNet);
      break;

    case TRIAND_NET:
      *flags = NetRedeclare(*flags, eDMTriandNet);
      break;

    case TRI0_NET:
      *flags = NetRedeclare(*flags, eDMTri0Net);
      break;

    case SUPPLY1_NET:
      *flags = NetRedeclare(*flags, eDMSupply1Net);
      break;

    case WOR_NET:
      *flags = NetRedeclare(*flags, eDMWorNet);
      break;

    case TRIOR_NET:
      *flags = NetRedeclare(*flags, eDMTriorNet);
      break;

    case TRIREG_NET:
      *flags = NetRedeclare(*flags, eDMTriregNet);
      break;

    case REG_NET:
      *flags = NetRedeclare(*flags, eDMRegNet);
      break;

    default:
    {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown net type"), &err_code);
    }
    break;
    }
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::var(veNode ve_var,
                                         LFContext *context,
                                         NUNet **the_net)
{
  *the_net = 0;
  ErrorCode err_code = eSuccess;

  UtString name_buf;
  mTicProtectedNameMgr->getVisibleName(ve_var, &name_buf);
  StringAtom* name = context->getStringCache()->intern(name_buf);
  SourceLocator loc = locator(ve_var);

  veObjType this_type = veNodeGetObjType(ve_var);

  NUScope * declaration_scope = context->getDeclarationScope();

  NetFlags flags(eAllocatedNet);
  if (declaration_scope->getScopeType() != NUScope::eModule) {
    flags = NetFlags(flags | eBlockLocalNet);
  }

  if (SIGN_USE == veGetSignType (ve_var)) {
    flags = NetFlags (flags | eSigned);
  }
  
  // We do not infer state for any locals to tasks or functions
  // But we take this opportunity to handle the task/function args
  if (declaration_scope->inTFHier()) {
    flags = NetFlags(flags | eNonStaticNet);

    if ( veNodeIsA( ve_var, VE_VARIABLE )) {
      // veVariableGetParentTfArg only accepts objects of type
      // VE_VARIABLE (or derived from that type)
      veNode ve_arg = veVariableGetParentTfArg(ve_var);
      if (ve_arg) {
        switch (veTfArgGetDirection(ve_arg)) {
        case INPUT_DIR:
          flags = NetFlags(flags | eInputNet);
          break;
        case OUTPUT_DIR:
          flags = NetFlags(flags | eOutputNet);
          break;
        case INOUT_DIR:
          flags = NetFlags(flags | eBidNet);
          break;
        default:
        {
          POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown port direction"), &err_code);
        }
        break;
        }
      }
    }
  }

  ConstantRangeArray rangeArray;
  ErrorCode temp_err_code = netRange(ve_var, &rangeArray);

  // test/bugs/bug2886/test.v returns here eFailPopulate and rangeArray[0] contains {mMsb = 11, mLsb = 0} and 
  // rangeArray[1] contains  {mMsb = 0, mLsb = -1}.
  // test/langcov/WideMemSel/bug7.v returns here eFailPopulate and rangeArray[0] contains {mMsb = 5, mLsb = 0},
  // and rangeArray[1] contains  {mMsb = 0, mLsb = -1}.
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  ConstantRange *range1 = NULL;
  if (rangeArray.size() > 0) {
    range1 = rangeArray[0];
  }


  switch (this_type) {
  case VE_MEMORY:               // this is not populated if -2000 specified
  {
    flags = NetRedeclare(flags, eDMReg2DNet);
    *the_net = new NUMemoryNet(name, 1, &rangeArray, flags, VectorNetFlags (0), declaration_scope, loc);
    if ((*the_net)->getBitSize() == 0)
    {
      POPULATE_FAILURE(mMsgContext->ObjectTooLarge( &loc ), &err_code);
    }
    break;
  }
  
  case VE_NETARRAY:
  {
    // this can be an array of wire or reg under a module, function, or block scope, 
    // or an array of tri, tri0,tri1,triand,supply0,supply1,wor,trior,trireg
    veNetType nettype = veArrayGetNetType(ve_var);
    switch ( nettype ){
    case REG_NET: {
      flags = NetRedeclare(flags, eDMReg2DNet);
      break;
    }
    case WIRE_NET: {
      flags = NetRedeclare(flags, eDMWire2DNet);
      break;
    }

    default: {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Array of unsupported nettype"), &err_code);
      break;
    }
    }
    *the_net = new NUMemoryNet(name, 1, &rangeArray, flags, VectorNetFlags (0), declaration_scope, loc);
    if ((*the_net)->getBitSize() == 0)
    {
      POPULATE_FAILURE(mMsgContext->ObjectTooLarge( &loc ), &err_code);
      return err_code;
    }
    break;
  }

  case VE_ARRAY:
  case VE_INTORTIMEARRAY:
    {
      veObjType array_type = veArrayGetArrayType(ve_var);
      switch (array_type) {
      case VE_TIME: {
        flags = NetRedeclare(flags, eDMTime2DNet);
        break;
      }
      case VE_INTEGER: {
        flags = NetRedeclare(flags, eDMInteger2DNet|eSigned);
        break;
      }
      case VE_REG:{
        flags = NetRedeclare(flags, eDMReg2DNet);
        break;
      }
      default: 
      {
        POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Array of unsupported type"), &err_code);
        break;
      }
      }

      if ( err_code != eFailPopulate ) {
        *the_net = new NUMemoryNet(name, 1, &rangeArray, flags, VectorNetFlags (0), declaration_scope, loc);
        if ((*the_net)->getBitSize() == 0)
        {
          POPULATE_FAILURE(mMsgContext->ObjectTooLarge( &loc ), &err_code);
          return err_code;
        }
      }
    }
    break;

  case VE_INTEGER:
    {
      flags = NetRedeclare(flags, eDMIntegerNet|eSigned);
      *the_net = new NUVectorNet(name, range1, flags, VectorNetFlags (0), declaration_scope, loc);
    }
    break;

  case VE_TIME:
    {
      flags = NetRedeclare(flags, eDMTimeNet);
      *the_net = new NUVectorNet(name, range1, flags, VectorNetFlags (0), declaration_scope, loc);
    }
    break;

  case VE_REAL:
  case VE_REALTIME:
    {
      flags = NetRedeclare(flags, eDMRealNet|eSigned);
      *the_net = new NUVectorNet(name, range1, flags, VectorNetFlags (0), declaration_scope, loc);
    }
    break;

  case VE_REG:
    {
      if (range1) {
	*the_net = new NUVectorNet(name, range1, flags, VectorNetFlags (0), declaration_scope, loc);
      } else {
	*the_net = new NUBitNet(name, flags, declaration_scope, loc);
      }
    }
    break;

  default:
    {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown net type"), &err_code);
      return err_code;
    }
    break;
  }

  temp_err_code = mPopulate->mapNet(context->getDeclarationScope(), ve_var, *the_net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    delete *the_net;
    *the_net = NULL;
    return temp_err_code;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::netRange(veNode ve_net,
                                              ConstantRangeArray *rangeArray)
                                              
{

  ConstantRange *range1 = NULL;
  ConstantRange *range2 = NULL;

  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(ve_net);

  veObjType this_type = veNodeGetObjType(ve_net);

  switch (this_type) {
  case VE_NET:
    {
      // This code is used for both locals and ports.
      veNode ve_range = veNetGetNetRange(ve_net);
      veNode ve_portrange = veNetGetPortRange(ve_net);
      ConstantRange* netRange = NULL;
      ConstantRange* portRange = NULL;  
      if (ve_range) {
        ErrorCode temp_err_code = constantRange(ve_range, &netRange);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
      }

      if (ve_portrange) {
        ErrorCode temp_err_code = constantRange(ve_portrange, &portRange);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
      }

      if (netRange && portRange) {
        if (*netRange != *portRange) {
          // not equal ranges. warn
          UtString buf;
          mTicProtectedNameMgr->getVisibleName(ve_net, &buf);
          mMsgContext->DiffSizePortRegConnect(&loc, buf.c_str());
          range1 = ConstantRange::getMax(netRange, portRange);
          if (range1 != portRange) {
            delete portRange;
            portRange = NULL;
          }
          else {
            delete netRange;
            netRange = NULL;
          }
        }
        else {
          // take either.
          range1 = netRange;
          delete portRange;
          portRange = NULL;
        }
      }
      else if (netRange)
        range1 = netRange;
      else if (portRange)
        range1 = portRange;

      if (range1) {
        rangeArray->push_back( range1 );
      }
    }
    break;

  case VE_NETARRAY: {
    veNode ve_width_range = veArrayGetArrayBitRange(ve_net);
    if (veNodeGetObjType(ve_width_range) == VE_RANGE) {
       ErrorCode temp_err_code = constantRange(ve_width_range, &range1);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
         return temp_err_code;
      }
    } else {
      range1 = new ConstantRange(0, 0);
    }
    rangeArray->push_back( range1 );

    ErrorCode temp_err_code = arrayRangeDimensions(ve_net, loc, rangeArray);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }

    break;
  } // case VE_NETARRAY

  case VE_MEMORY:
    {
      veNode ve_width_range = veMemoryGetWordSize(ve_net);
      if (veNodeGetObjType(ve_width_range) == VE_RANGE) {
        ErrorCode temp_err_code = constantRange(ve_width_range, &range1);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
      } else {
        range1 = new ConstantRange(0, 0);
      }
      rangeArray->push_back( range1 );

      veNode ve_depth_range = veMemoryGetMemSize(ve_net);
      ErrorCode temp_err_code = constantRange(ve_depth_range, &range2);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      rangeArray->push_back( range2 );
    }
    break;

  case VE_ARRAY:
  case VE_INTORTIMEARRAY:
    {
      veObjType array_type = veArrayGetArrayType(ve_net);
      switch (array_type) {
      case VE_TIME:
        range1 = new ConstantRange(63, 0);
        rangeArray->push_back( range1 );
        break;

      case VE_INTEGER:
        range1 = new ConstantRange(31, 0);
        rangeArray->push_back( range1 );
        break;

      case VE_REG:
      {
        // in -2001 this case represents an array of reg type, if the
        // reg is declared under a task or a function scope.
        // Declarations like "reg myreg[3:0]" don't have range on 
        // LHS and are still arrays of 1 bit registers. Add range [0:0]
        // if this is the case.
        veNode ve_bit_range = veArrayGetArrayBitRange(ve_net);
        if (veNodeGetObjType(ve_bit_range) == VE_RANGE) {
          ErrorCode temp_err_code = constantRange(ve_bit_range, &range1);
          if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
            return temp_err_code;
          }
        } else {
          range1 = new ConstantRange(0, 0);
        }
        rangeArray->push_back( range1 );
        break;
      }
      
      default:
        POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Array of unsupported type"), &err_code);
        return err_code;
        break;
      }

      veNode ve_depth_range = 0;
      if (veNodeIsA(ve_net, VE_INTORTIMEARRAY)) {
        ve_depth_range = veIntOrTimeArrayGetArrayRange(ve_net);
        ErrorCode temp_err_code = constantRange(ve_depth_range, &range2);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }
        rangeArray->push_back( range2 );
      } else {
        ErrorCode temp_err_code = arrayRangeDimensions(ve_net, loc, rangeArray);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }
      }
    }
    break;

  case VE_INTEGER:
  {
    // do NOT try to get the range of the integer here with the code:
    //   veNode ve_width_range = veIntegerGetSize(ve_net);
    // If the design file contains a declared range for an integer
    // then Cheetah should have reported that the range will be
    // ignored. We always use 32 bits for integers. test/langcov/bug6184.v
    range1 = new ConstantRange(31, 0);
    rangeArray->push_back( range1 );
    break;
  }
  case VE_TIME:
  {
    veNode ve_width_range = veTimeGetSize(ve_net);
    if (veNodeGetObjType(ve_width_range) == VE_RANGE) {
      ErrorCode temp_err_code = constantRange(ve_width_range, &range1);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
    } else {
      range1 = new ConstantRange(63, 0); // default to 64 bits
    }
    rangeArray->push_back( range1 );
    break;
  }
  case VE_REAL:
  case VE_REALTIME:
  {
    range1 = new ConstantRange(63, 0);
    rangeArray->push_back( range1 ); 
    break;
  }
  case VE_REG:
    {
      veNode ve_range = veRegGetRange(ve_net);
      if (ve_range) {
        ErrorCode temp_err_code = constantRange(ve_range, &range1);
  
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }
        rangeArray->push_back(range1);
      }
    }
    break;

  default:
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown net type"), &err_code);
    return err_code;
    break;
  }

  return err_code;
}

Populate::ErrorCode VerilogPopulate::arrayRangeDimensions(veNode ve_net, const SourceLocator &/*loc*/, ConstantRangeArray *rangeArray) {
  ErrorCode err_code = eSuccess;
  ConstantRange *range2 = NULL;
  veNode ve_depth_range = NULL;

  CheetahList dims(veArrayGetDimensionList(ve_net));
  UtStack<ConstantRange*> range_stack;
  while ((ve_depth_range = veListGetNextNode(dims)) != NULL) {
    ErrorCode temp_err_code = constantRange(ve_depth_range, &range2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }
    range_stack.push(range2);
  }
  while (!range_stack.empty()) {
    rangeArray->push_back( range_stack.top() );
    range_stack.pop();
  }
  return err_code;
}
    




Populate::ErrorCode VerilogPopulate::net(veNode ve_net,
                                         LFContext *context,
                                         NUNet **the_net)
{
  *the_net = 0;
  SourceLocator loc = locator(ve_net);
  ErrorCode err_code = eSuccess;
  veObjType obj_type = veNodeGetObjType(ve_net);
  UtString buf;
  mTicProtectedNameMgr->getVisibleName(ve_net, &buf);
  StringAtom* name = context->getStringCache()->intern(buf.c_str());
  NetFlags flags;

  ErrorCode temp_err_code = netFlags(ve_net, name, context, &flags);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  flags = NetFlags(flags | eAllocatedNet);

  if ( loc.isTicProtected() ){
    // mark nets that are declared within `protected regions as temp so they are not dumped in waveforms
    flags = NetFlags(flags | eTempNet); 
  }
  
  switch ( obj_type ){
  case VE_INTEGER:{
    flags = NetFlags (flags | eSigned);
    break;
  }
  case VE_TIME:{
    break;    // this is not signed (LRM 3.9)
  }
  default:{
    if (veNetGetIsSigned (ve_net)) {
      flags = NetFlags (flags | eSigned);
    }
    break;
  }
  }

  NUScope * declaration_scope = context->getDeclarationScope();
  if (declaration_scope->getScopeType() != NUScope::eModule) {
    flags = NetFlags(flags | eBlockLocalNet);
  }

  // We do not infer state for any locals to tasks or functions
  if (declaration_scope->inTFHier()) {
    flags = NetFlags(flags | eNonStaticNet);
  }

  ConstantRangeArray rangeArray;
  temp_err_code = netRange(ve_net, &rangeArray);
  ConstantRange *range = NULL;

  if (rangeArray.size() > 0) {
    range = rangeArray[0];
  }
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  if (range) {
    *the_net = new NUVectorNet(name, range, flags, VectorNetFlags (0), declaration_scope, loc);
  } else {
    *the_net = new NUBitNet(name, flags, declaration_scope, loc);
  }
  
  // Detect trireg on primary port.
  if ((*the_net)->atTopLevel() and (*the_net)->isPort() and (*the_net)->isTrireg()) {
    mMsgContext->TriregOnPrimaryPort(*the_net);
  }

  temp_err_code = mPopulate->mapNet(declaration_scope, ve_net, *the_net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    delete *the_net;
    *the_net = NULL;
    return temp_err_code;
  }

  return err_code;
}

//#define DEBUG_HIER_REF 1
#ifdef DEBUG_HIER_REF
//! Helper function to browse Cheetah's scope variable datastructure
static void browseScopeVar(veNode ve_scopevar)
{
  UtIO::cout() << "Scope variable browse" << UtIO::endl;
  veNodeBrowse(ve_scopevar);

  UtIO::cout() << "Scope variable element list browse" << UtIO::endl;
  CheetahList ve_iter(veScopeVariableGetElementList(ve_scopevar));
  if (ve_iter) {
    veNode ve_node;
    while ((ve_node = VeListGetNextNode(ve_iter)) != NULL) {
      veNodeBrowse(ve_node);
    }
  }
  UtIO::cout() << "Scope variable start object list browse" << UtIO::endl;
  ve_iter = veScopeVariableGetStartObjectList(ve_scopevar);
  if (ve_iter) {
    veNode ve_node;
    while ((ve_node = VeListGetNextNode(ve_iter)) != NULL) {
      veNodeBrowse(ve_node);

      UtIO::cout() << "Scope variable unique element list browse" << UtIO::endl;
      CheetahList ve_ele_iter(veScopeVariableGetUniqueElementList(ve_scopevar, ve_node));
      if (ve_ele_iter) {
	veNode ve_ele_node;
	while ((ve_ele_node = VeListGetNextNode(ve_ele_iter)) != NULL) {
	  veNodeBrowse(ve_ele_node);
	}
      }
      UtIO::cout() << "Scope variable unique parent browse" << UtIO::endl;
      veNodeBrowse(veScopeVariableGetUniqueParentObject(ve_scopevar, ve_node));
    }
  }
}
#endif


Populate::ErrorCode VerilogPopulate::findObjectResolution( 
    LFContext *context,
    CheetahList& scope_list,     // list of hier path objects
    UtStack<veNode>& inst_stack, // stack to hold the elab instances
    StringAtom*& parent_mod_name,// parent module of the resolved object
    veNode& resolvedObj,         // the found resolved object
    SourceLocator& loc )         
{
  ErrorCode err_code = eSuccess;
  resolvedObj = NULL;
  parent_mod_name = 0;

  // Iterate over all the resolved path objects (will be one object per path component).
  // The last path object will be the resolved net.
  // As we iterate, elaborate the instance objects we encounter so that parameters will
  // be set up correctly.
  // Also, we remember the instances that were elaborated so we can unelaborate them
  // at the end.  We need to elaborate the whole path to get parameters correct.
  //
  // TODO: we elaborate the relative path.  However, if a parameter has been overridden
  // higher up, then we may need to elaborate the absolute path.  Currently this is
  // not done.  See TFAIL test/hierref/task12.v
  bool stop = false;
  while( not stop and ((resolvedObj = VeListGetNextNode(scope_list)) != NULL) ) {
    veObjType obj_type = veNodeGetObjType(resolvedObj);
    switch (obj_type) {
      case VE_MODULEORUDPINSTANCE: {
        veModuleInstanceElaborate( resolvedObj, NULL ); // set parameter values
        inst_stack.push( resolvedObj );
        // Get name from parameters.
        veNode ve_module = veInstanceGetMaster( resolvedObj );
 
        bool isUDP = false;
        ErrorCode module_err_code = findIfModuleOrUDP(ve_module, &isUDP);
        if ( errorCodeSaysReturnNowWhenFailPopulate(module_err_code, &err_code ) )
        {
          return module_err_code;
        }

        UtString this_name;
        mTicProtectedNameMgr->getVisibleName(ve_module, &this_name);

        if (isUDP == false)
        {
          ErrorCode tcode = analyzeInstanceParameters( ve_module,
                                                     &this_name );
          parent_mod_name = context->getStringCache()->intern( this_name.c_str(),
                                                             this_name.size() );
          if (errorCodeSaysReturnNowWhenFailPopulate(tcode, &err_code)) {
            return tcode;
          }
        } 
        else 
        {
          parent_mod_name = context->getStringCache()->intern( this_name.c_str(),
                                                             this_name.size() );
        }
        break;
      }
      case VE_GENERATE_BLOCK: {
        // currently we do not support hierarchical refs that
        // refer to something from within a generate block.
        POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical reference resolves into a generate block -- This is currently unsupported."), &err_code);
        return err_code;
        // if we were to support this we will need to save the
        // name of the generate block such that we can look it up
        // to resolve the reference in VerilogPopulate::possibleNetResolutions
        // something like the following:
        //            veModuleElaborateGenerateStatement(ve_ele_node, NULL); // set parameter values
        //            inst_stack.push(ve_ele_node);
        // Get the name of this generate block
        //            UtString this_name;
        //            mTicProtectedNameMgr->getVisibleName(ve_ele_node, &this_name);
        //            ErrorCode temp_err_code = analyzeGenerateParameters(ve_module,
        //                                                                context,
        //                                                                &this_name);
        //            parent_mod_name = context->getStringCache()->intern(this_name.c_str(),
        //                                                                   this_name.size());
        //            if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) {
        //              return temp_err_code;
        //            }
        //            break;
      }
      case VE_MODULE: {
        // This is for the top module of the design, there will be no parameters.
        UtString buf;
        mTicProtectedNameMgr->getVisibleName(resolvedObj, &buf);
        parent_mod_name = context->getStringCache()->intern(buf.c_str());
        break;
      }
      default: {
        stop = true;
        break;
      }
    }
  }

  if (not resolvedObj) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported hierarchical reference"), &err_code);
    return err_code;
  }

  return err_code;
}


void VerilogPopulate::undoElaboration( UtStack<veNode>& inst_stack )
{
  // Undo the elaboration we did to the instance objects in the path
  // to this resolution.
  while (not inst_stack.empty()) {
    veNode ve_inst = inst_stack.top();
    veModuleInstanceResetParamValues(ve_inst);
    inst_stack.pop();
  }
}

/*!
 * Traverse the cheetah hierarchical reference resolutions, make sure
 * they are consistent.
 * Keep track of the possible resolutions so we can add pointers from the ref
 * to the resolution.
 * Populate a representative net type.
 */
Populate::ErrorCode VerilogPopulate::netHierRefResolve(veNode ve_scopevar,
                                                       const AtomArray& path,
                                                       LFContext *context,
                                                       NUNet **the_net)
{
  *the_net = 0;
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_scopevar);

  veObjType type = UNSET_OBJECT;
  veNetType net_type = UNSET_NET;

  ConstantRange *net_range1 = 0;
  ConstantRangeArray rangeArray_1;
  ConstantRangeArray rangeArray_2;
  VeNodeNamePairList possible_resolutions;

  // Iterate over all the resolution start objects.
  CheetahList ve_start_iter(veScopeVariableGetStartObjectList(ve_scopevar));
  if (ve_start_iter) {
    veNode ve_start_node;
    while ((ve_start_node = VeListGetNextNode(ve_start_iter)) != NULL) {

      CheetahList scope_list(veScopeVariableGetUniqueElementList(ve_scopevar, ve_start_node));
      UtStack<veNode> inst_stack; // used to undo elaboration applied during this analysis
      StringAtom *parent_mod_name = 0; // Name of the module containing the resolved net.
      veNode ve_resolved_obj = NULL;
      
      // List is empty, do nothing.
      if( !scope_list )
        continue;

      ErrorCode ecode = findObjectResolution( context, scope_list, inst_stack, parent_mod_name,
                                              ve_resolved_obj, loc );
      if( errorCodeSaysReturnNowWhenFailPopulate( ecode, &err_code ) )
        return ecode;
      
      veObjType obj_type = veNodeGetObjType(ve_resolved_obj);
      if ( obj_type == VE_SEQ_BLOCK ){
        // this is a reference to a block not a net (Possibly from a disable stmt?)
        return eNotApplicable;
      }

      veNode ve_obj_node = ve_resolved_obj;

      // Remember this as a possible resolution.
      possible_resolutions.push_back(VeNodeNamePair(ve_obj_node, parent_mod_name));

      // Get the range of this resolved net.
      ErrorCode temp_err_code = netRange(ve_obj_node, &rangeArray_1);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      ConstantRange *this_range1 = NULL;
      if (rangeArray_1.size() > 0) {
        this_range1 = rangeArray_1[0];
      }

      // Check ranges for consistency.
      if (type == UNSET_OBJECT) {
	net_range1 = this_range1;
        rangeArray_2 = rangeArray_1;
      } else {
        ConstantRangeArray::iterator iter;
        ConstantRangeArray::iterator prev_iter;
        for (iter = rangeArray_1.begin(), prev_iter = rangeArray_2.begin();; ++iter, ++prev_iter) {
           bool iter_end = (iter == rangeArray_1.end()); 
           bool prev_iter_end = (prev_iter == rangeArray_2.end()); 

           if (iter_end and prev_iter_end) {
               break;
           }
           ConstantRange *range_1 = *iter;
           ConstantRange *range_2 = *prev_iter;
           if (iter_end != prev_iter_end) {
            
             if (!iter_end) {
               delete range_1;
             }
             else {
               delete range_2;
             }
             POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical reference resolves to different net ranges in different instantiations"), &err_code);
	     return err_code;
           }
           if ((range_1 and not range_2) or
               (range_2 and not range_1) or
               ((*range_1 != *range_2))) {
             POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical reference resolves to different net ranges in different instantiations"), &err_code);
             return err_code;
           }
           if (range_1) {
             delete range_1;
           }
        }

      }

      while (rangeArray_1.size() > 0) {
        rangeArray_1.pop_back();
      }
      // Check net types for consistency.
      veObjType this_type = veNodeGetObjType(ve_obj_node);
      veNetType this_nettype = UNSET_NET;
      if ((this_type == VE_NETARRAY ) or (this_type == VE_INTORTIMEARRAY)) {
        this_nettype = veArrayGetNetType(ve_obj_node);
      }
      
      if (type == UNSET_OBJECT) {
	type = this_type;
        net_type = this_nettype;
      } else {
	if ( (type != this_type) and (net_type != this_nettype) ) {
	  POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical reference resolves to different net types in different instantiations"), &err_code);
	  return err_code;
	}
      }
      undoElaboration( inst_stack );
    }
  } else {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Cannot resolve hierarchical reference"), &err_code);
    return err_code;
  }

  if (possible_resolutions.empty()) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Cannot resolve hierarchical reference"), &err_code);
    return err_code;
  }

  // Compute the flags for this type
  ErrorCode temp_err_code = err_code;
  NetFlags flags(eAllocatedNet);
  switch(type) {
  case VE_MEMORY: {
    flags = NetRedeclare(flags, eDMReg2DNet);
    break;
  }

  case VE_NETARRAY: {
    // a VE_NETARRAY can be an array of reg,wire,tri,tri0,tri1,supply0,supply1,trior,triand,trireg,wor
    switch ( net_type ) {
    case REG_NET: {
      flags = NetRedeclare(flags, eDMReg2DNet);
      break;
    }
    case WIRE_NET: {
      flags = NetRedeclare(flags, eDMWire2DNet);
      break;
    }
    default: {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Nettype not supported by hierarchical reference consitency checking."), &err_code);
      return err_code;
    }
    }
    break;
  }

  case VE_INTEGER: {
    flags = NetRedeclare(flags, eDMIntegerNet|eSigned);
    break;
  }

  case VE_TIME: {
    flags = NetRedeclare(flags, eDMTimeNet);
    break;
  }

  case VE_REAL:
  case VE_REALTIME: {
    flags = NetRedeclare(flags, eDMRealNet|eSigned);
    break;
  }

  case VE_REG:
  case VE_NET: {
    break;
  }

  default:
  {
    SourceLocator loc = locator(ve_scopevar);
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown net type"), &temp_err_code);
    break;
  }
  }
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }

  if (rangeArray_2.size() == 0) {
    *the_net = new NUBitNetHierRef(path, flags, context->getModule(), loc);
  } else if (net_range1 and rangeArray_2.size() == 1) {
    *the_net = new NUVectorNetHierRef(path, net_range1, flags, VectorNetFlags (0), context->getModule(), loc);
  } else {
    *the_net = new NUMemoryNetHierRef(path, &rangeArray_2, flags, VectorNetFlags (0),
                                      context->getModule(), false, loc);
    if ((*the_net)->getBitSize() == 0)
    {
      POPULATE_FAILURE(mMsgContext->ObjectTooLarge( &loc ), &err_code);
      return err_code;
    }
  }
  NUNetHierRef *the_net_hier_ref = (*the_net)->getHierRef();

  temp_err_code = rememberPossibleResolutions(the_net_hier_ref, possible_resolutions, loc);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::netHierRef(veNode ve_scopevar,
                                                LFContext *context,
                                                NUNet **the_net)
{
  *the_net = 0;
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_scopevar);

  // For some reason Cheetah puts defparams on the scope variable list.
  // Weed them out. Avoid making Cheetah call on VHDL object.
  veNode parentObj = veScopeVariableGetParentObject(ve_scopevar);
  if ( (mvvGetLanguageType((mvvNode)parentObj) == MVV_VERILOG) &&
       (veNodeIsA(parentObj, VE_PARAM)) ) {
    return eSuccess;
  }

  // Cheetah can create many scope variable nodes for the same hierarchical
  // reference.  So, detect if we have already seen this hierarchical reference
  // as a different scope variable, and don't create another net in that case.
  ErrorCode temp_err_code = makeHierRefMapping(context->getModule(), ve_scopevar, the_net);
  ErrorCode dummy;
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &dummy)) {
    return temp_err_code;
  } else if (*the_net != 0) {
    *the_net = 0;
    return eSuccess;
  }

  AtomArray path;
  temp_err_code = scopeVariableToPath(ve_scopevar, &path);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  temp_err_code = netHierRefResolve(ve_scopevar, path, context, the_net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  temp_err_code = mPopulate->mapNet(context->getModule(), ve_scopevar, *the_net); // use module as scope here
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

#ifdef DEBUG_HIER_REF
  browseScopeVar(ve_scopevar);
#endif

  return err_code;
}

Populate::ErrorCode VerilogPopulate::scopeVariableToPath(veNode ve_scopevar, AtomArray* path)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_scopevar);

  // veScopeVariableGetElementList gives a list of nodes that
  // make up the path to ve_scopevar, but it does not provide a way to
  // access the instance array information for any of these nodes.
  // The name from veGetScopeVarIndexedName does include the
  // instance array information.  We use the nodes from
  // veScopeVariableGetElementList to get the protected/unprotected
  // names and the parts of the string from veScopeVariableGetHierName
  // to get the instance array information, and combine them to form
  // the path to be returned.
  CheetahStr hierName(veGetScopeVarIndexedName(ve_scopevar));
  CheetahList scopes(veScopeVariableGetElementList(ve_scopevar));
  if ( scopes.isNull() ) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical reference to non-existant variable."), &err_code); // bug 14728
    return err_code;
  }
  HdlHierPath::Status status;
  const char* hname = hierName;
  UtString buf;
  HdlId info;
  do {
    status = mHdlVerilogPath->parseToken(&hname, &buf, &info, true);
    veNode node = veListGetNextNode(scopes);
    if ( node == NULL ){
      // ran out of scopes before we ran out of name -- the hierref points to something that does not exist
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Cannot resolve hierarchical reference"), &err_code);
      status = HdlHierPath::eEndPath;
      break;
    }
    UtString str;
    mTicProtectedNameMgr->getVisibleName(node, &str);
    if ( str != buf ){
      size_t range_front = buf.find_first_of("[");
      if ( range_front != UtString::npos ){
        size_t range_back = buf.find_last_of("]");
        if ((range_back != UtString::npos) && (range_back > range_front)){
          str.append(buf, range_front, 1+range_back-range_front);
        }
      }
    }
    path->push_back(mStrCache->intern(str));
    buf.clear();
  } while (status == HdlHierPath::eLegal);

  if (NULL != veListGetNextNode(scopes)){
    // ran out of name before we ran out of scopes
    status = HdlHierPath::eEndPath;
  }
  
  if ( status != HdlHierPath::eEndPath ){
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Problem creating HdlHierPath"), &err_code);
  }
  return err_code;
}

Populate::ErrorCode VerilogPopulate::local(veNode ve_net,
                                           LFContext *context,
                                           NUNet **the_net)
{
  *the_net = 0;
  if (veNetGetIsPortNet(ve_net)) {
    return eSuccess;
  } else {
    return net(ve_net, context, the_net);
  }
}


Populate::ErrorCode VerilogPopulate::port(veNode ve_port,
                                          LFContext *context,
                                          NUNet **the_net)
{
  *the_net = 0;
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(ve_port);

  veNode ve_expr = vePortGetNetExpr(ve_port);
  veNode ve_ident = 0;

  if (not ve_expr) {
    // Empty port
    return eSuccess;
  }

  ConstantRange port_selected_range(0,0);
  bool whole_net = false;
  veObjType expr_type = veNodeGetObjType(ve_expr);
  switch (expr_type) {
  case VE_NAMEDOBJECTUSE:
    {
      whole_net = true;
      ve_ident = veNamedObjectUseGetParent(ve_expr);
      ErrorCode temp_err_code = net(ve_ident, context, the_net);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
    }
    break;

  case VE_BITSELECT:
  case VE_PARTSELECT:
  {
    bool is_bitselect = (expr_type == VE_BITSELECT);

    // get ident, check to see if partselect or bitselect is for full
    // range, if so then this is ok
    NUNet * possible_net;
    veNode ve_named = is_bitselect ? veBitSelectGetVariable(ve_expr) : vePartSelectGetVariable(ve_expr);
    veObjType type = veNodeGetObjType(ve_named);
    if ( type != VE_NAMEDOBJECTUSE ){
      POPULATE_FAILURE(mMsgContext->UnimplPort(&loc), &err_code);
      return err_code;
    }
    ve_ident = veNamedObjectUseGetParent(ve_named);
    // first check to see if net is already populated
    // (for example when net appears multiple times in port list)
    ErrorCode temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_ident, &possible_net, false);
    if ( temp_err_code != eSuccess ) {
      // net not seen yet, so create it here
      temp_err_code = net(ve_ident, context, &possible_net);
    }

    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }
    ConstantRange declared_range(0,0);
    NUVectorNet* vn = dynamic_cast<NUVectorNet*>(possible_net);
    if (vn) {
      declared_range = *vn->getDeclaredRange ();
    } else {
      // a bitselect or partselect only makes sense on a vector
      POPULATE_FAILURE(mMsgContext->UnimplPort(&loc), &err_code);
      return err_code;
    }

    bool acceptable_port_expression = false;
    if ( is_bitselect ) {
      // a bitselect is acceptable if the selected bit is within the declared range of the net
      veNode ve_sel_bit = veBitSelectGetSelectedBit(ve_expr);
      SInt32 bit = veExprEvaluateValue(ve_sel_bit);
      port_selected_range = ConstantRange(bit,bit);
      acceptable_port_expression =  declared_range.contains(bit);
    } else {
      // a partselect is acceptable if it is a static range and within the declared range of the net
      veNode ve_range = vePartSelectGetSelectedBit(ve_expr);
      veSignType sType = veRangeGetSignType (ve_range);
      port_selected_range = ConstantRange(veExprEvaluateValue(veRangeGetLeftRange(ve_range)),veExprEvaluateValue(veRangeGetRightRange(ve_range)));
      if ( ( sType == UNSET_SIGN ) and ( declared_range.contains(port_selected_range)) ) {
        acceptable_port_expression = true;
      }
    }

    if ( not acceptable_port_expression  ) {
      POPULATE_FAILURE(mMsgContext->UnimplPort(&loc), &err_code);
      return err_code;
    }
    *the_net = possible_net;
    break;
  }

  default:
    {
      POPULATE_FAILURE(mMsgContext->UnimplPort(&loc), &err_code);
      return err_code;
      break;
    }
  }
  
  NULvalue* the_lvalue;
  if ( whole_net ){
    the_lvalue = new NUIdentLvalue(*the_net, loc);
  } else {
    the_lvalue = new NUVarselLvalue (*the_net, port_selected_range, loc);
  }

  NUModule* the_module = context->getModule();
  ErrorCode temp_err_code = mPopulate->mapPort(the_module, ve_port, the_lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::lvalueActualFixup(NULvalue *lvalue,
                                                       UInt32 formal_width,
                                                       const UInt32 *offset,
                                                       NULvalue **result_lvalue)
{
  ErrorCode err_code = eSuccess;
  *result_lvalue = lvalue;

  // there are three possibilities,
  // 1. this instance had no range declared for it (thus it is not an
  //    array of instances) so no fixup required
  // 2. this actual is the same width as the formal, and all of it is to be
  //    connected to the formal in each instance, no fixup required (bug2037)
  // 3. this actual is wider than a single bit, and only part of it is
  //    to be connected to the formal

  if (not offset) {
    return eSuccess;            // case 1
  }

  UInt32 actual_width = lvalue->determineBitSize();
  if ( actual_width == formal_width ){
    return eSuccess;
  }
  SourceLocator loc = lvalue->getLoc();

  // determine which bits of lvalue are to be connected to this
  // instance (defined by *offset and formal_width).
  const ConstantRange required_range(formal_width-1,0);

  // build a new lvalue 
  NUExpr *index = NUConst::create(false, ((*offset)*formal_width),  32, loc);
  *result_lvalue = new NUVarselLvalue(lvalue, index, required_range, loc);
  *result_lvalue = mFold->fold(*result_lvalue); // simplify actuals like i[3:0][1] -> i[1]

  return err_code;
}


Populate::ErrorCode VerilogPopulate::rvalueActualFixup(NUExpr *rvalue,
                                                       UInt32 formal_width,
                                                       const UInt32 *offset,
                                                       NUExpr **result_rvalue)
{
  ErrorCode err_code = eSuccess;
  *result_rvalue = rvalue;

  // there are three possibilities,
  // 1. this instance had no range declared for it (thus it is not an
  //    array of instances) so no fixup required
  // 2. this actual is the same width as the formal, and all of it is to be
  //    connected to the formal in each instance, no fixup required
  // 3. this actual is wider than a single bit, and only part of it is
  //    to be connected to the formal

  if (not offset) {
    return eSuccess;            // case 1
  }

  UInt32 actual_width = rvalue->determineBitSize();
  if ( actual_width == formal_width ){
    return eSuccess;
  }
  SourceLocator loc = rvalue->getLoc();

  // determine which bits of lvalue are to be connected to this
  // instance (defined by *offset and formal_width).
  const ConstantRange required_range(formal_width-1,0);

  // build a new rvalue
  NUExpr *index = NUConst::create(false, ((*offset)*formal_width),  32, loc);
  *result_rvalue = new NUVarselRvalue(rvalue, index, required_range, loc);
  (*result_rvalue)->resize(formal_width);
  *result_rvalue = mFold->fold(*result_rvalue); // simplify actuals like i[3:0][1] -> i[1]

  return err_code;
}


Populate::ErrorCode VerilogPopulate::portConnection(const VeNodeList &ve_portconns,
                                                    LFContext *context,
                                                    NUNet* formal, 
                                                    NUModule* instantiated,
                                                    const UInt32 *offset,
                                                    const SourceLocator& loc,
                                                    NUPortConnection **the_conn)
{
  *the_conn = 0;
  ErrorCode err_code = eSuccess;
  bool formal_is_input = formal->isInput();
  bool formal_is_output = formal->isOutput();
  bool formal_is_bid = formal->isBid();
  NUModule* instantiator = context->getModule();

  // Actual may be 0 if the port is unconnected.
  // We go by the type of the formal instead of the type of port connection which
  // Cheetah tells us, because we may have done port coercion.
  UInt32 formal_width = formal->getBitSize();
  SInt32 num_actuals = ve_portconns.size();
  if (formal_is_input) {
    NUExpr *actual = 0;
    if (num_actuals != 0) {
      NUExpr *temp_actual = 0;
      NUExprVector expr_vector;
      	
      for (VeNodeList::const_iterator p = ve_portconns.begin(), e = ve_portconns.end(); p != e; ++p){
        const veNode ve_portconn = *p;
        const veNode ve_actual = vePortConnGetConnectedExpr(ve_portconn);
        NUExpr *this_actual = 0;
        if ( ve_actual ) {
          ErrorCode temp_err_code = expr(ve_actual, context, instantiator, 0, &this_actual);
          if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
            return temp_err_code;
          }
        } else {
          if ( num_actuals > 1 ){
            // if there are more than 1 actuals then we will create a concat of actuals for this
            // port, and the current bit(s) are undriven, so we put z in their place
            // to determine the necessary size we use the size of the formal that is associated with
            // the gap in the actuals
            NUNet* this_port = 0;
            bool partial_port = false;
            ConstantRange port_range;
            ErrorCode temp_err_code = mPopulate->lookupPort( instantiated, vePortConnGetParentPort( ve_portconn ),
                                                             NULL, NULL, &this_port, &partial_port, &port_range );
            if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
              return temp_err_code;
            }
            NU_ASSERT(this_port, instantiated); // multiple actuals were being connected to a collapsed
                                                // (wacky) port, but the formal port was
                                                // undefined. something is wrong.
            SInt32 size(port_range.getLength());
            UtString zval(size, 'z');
            this_actual = NUConst::createXZ(zval, false, size, loc);
          }
        }
        expr_vector.push_back(this_actual);
      }
      
      if ( num_actuals == 1 ){
        temp_actual = expr_vector.front();
      } else if ( num_actuals > 1 ) {
        temp_actual = new NUConcatOp(expr_vector, 1, loc);
      }
      if ( temp_actual ) {
        ErrorCode temp_err_code = rvalueActualFixup(temp_actual, formal_width, offset, &actual);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }
        if ( actual ) {
          actual->resize(actual->determineBitSize());
        }
      }
      if ( actual && (! actual->isSignedResult() && formal->isSigned()) ){
        UInt32 actual_width = actual->determineBitSize ();
        if ( formal_width == actual_width ) {
          // bug 14514 only appears to be an issue if the widths match
          UtString buf;
          formal->compose(&buf);
          mMsgContext->SignedMisMatch( &loc, buf.c_str());
        }
      }
    }
    *the_conn = new NUPortConnectionInput(actual, formal, loc);
  } else if (formal_is_output or formal_is_bid) {
    NULvalue *actual = 0;
    if ( num_actuals != 0 ) {
      NULvalue *temp_actual = 0;
      NULvalueVector lvalue_vector;

      for (VeNodeList::const_iterator p = ve_portconns.begin(), e = ve_portconns.end(); p != e; ++p){
        const veNode ve_portconn = *p;
        const veNode ve_actual = vePortConnGetConnectedExpr(ve_portconn);
        NULvalue *this_actual = 0;
        if ( ve_actual ) {
          ErrorCode temp_err_code = lvalue(ve_actual, context, instantiator, &this_actual);
          if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
            return temp_err_code;
          }
        } else {
          if ( num_actuals > 1 ){
            // I don't know how to represent the actuals of a compressed wacky port where one or
            // more of the bits (but not all) of an output port are unconnected. So we don't support it
            // this could be fixed by defining a new net and using it here as a placeholder
            POPULATE_FAILURE(mMsgContext->MultiPortNet(formal, "Some fraction of the actuals needed for this formal are unconnected."), &err_code);
            return err_code;
          }
        }
        lvalue_vector.push_back (this_actual);
      }
      if ( num_actuals == 1 ){
        temp_actual = lvalue_vector.front();
      } else if ( num_actuals > 1 ) {
        temp_actual = new NUConcatLvalue(lvalue_vector, loc);
      }

      if ( temp_actual ) {
        ErrorCode temp_err_code = lvalueActualFixup(temp_actual, formal_width, offset, &actual);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }

        if ( actual ) {
          actual->resize();
        }
      }
    }
    if (formal_is_output) {
      *the_conn = new NUPortConnectionOutput(actual, formal, loc);
    } else if (formal_is_bid) {
      *the_conn = new NUPortConnectionBid(actual, formal, loc);
    }
  }  else {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown port type"), &err_code);
    return err_code;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::unconnectedPort(veNode ve_inst,
                                                     NUNet *formal,
                                                     LFContext *,
                                                     NUPortConnection **the_conn)
{
  SourceLocator loc = locator(ve_inst);
  ErrorCode err_code = eSuccess;

  *the_conn = 0;

  if (formal->isInput()) {
    *the_conn = new NUPortConnectionInput(0, formal, loc);
  } else if (formal->isOutput()) {
    *the_conn = new NUPortConnectionOutput(0, formal, loc);
  } else if (formal->isBid()) {
    *the_conn = new NUPortConnectionBid(0, formal, loc);
  } else {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown port type"), &err_code);
  }

  return err_code;
}


void VerilogPopulate::checkCoercionLvalue(NUNet *net,
                                          MsgContext *msg_context,
                                          LFContext *context,
                                          const SourceLocator& loc)
{
  if (not context->doCoercePorts()) {
    if (net->isPrimaryInput()) {
      if ( loc.isTicProtected() or net->getLoc().isTicProtected() ){
        msg_context->DrivenPrimaryInput(&loc, "(from a protected region)");
      } else {
        UtString buf;
        buf << net->getScope()->getName()->str() << "."; // this is needed for test/bugs/bug95
        net->compose(&buf, NULL);
        msg_context->DrivenPrimaryInput(&loc, buf.c_str());
      }
    }
  }
}

