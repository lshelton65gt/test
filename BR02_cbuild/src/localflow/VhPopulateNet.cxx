// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUBitNet.h"
#include "util/StringAtom.h"
#include "iodb/IODBNucleus.h"
#include "util/CbuildMsgContext.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STBranchNode.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "compiler_driver/VhdlPortTypeMap.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"

Populate::ErrorCode VhdlPopulate::netFlags(vhNode vh_net,
                                           StringAtom* name,
                                           LFContext *context,
                                           NetFlags *flags)
{
  *flags = eNoneNet;
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(vh_net);
  VhPortType vh_dir;
  if ( vhGetObjType( vh_net ) == VHELEMENTDECL ||
       ( vhGetObjType( vh_net ) == VHCONSTANT && vhIsGeneric( vh_net )))
    vh_dir = VH_NOT_PORT;
  else
    vh_dir = vhGetPortType(vh_net);

  // We will not set any direction flag for the GUARD signal. FYI,
  // the direction of the GUARD signal is 'IN' . We will not set the
  // direction of the NUNet corresponding to the 'GUARD' signal because
  // we will assign the guard expression into it.
  if (vh_dir != VH_NOT_PORT && 0 != strcmp(vhGetName(vh_net),"GUARD") &&
      not vhIsAliasObject(vh_net) ) 
  {
    switch (vh_dir) 
    {
    case VH_IN:
    case VH_DEFAULT_IN:
      *flags = eInputNet;
      break;
    case VH_BUFFER:
    case VH_OUT:
      *flags = eOutputNet;
      break;
    case VH_INOUT:
      *flags = eBidNet;
    case VH_LINKAGE:
      break;
    default: 
    {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unknown port direction"), &err_code);
      break;
    }
    }
  }

  // Let the user override inout nets to be inputs if desired
  NUModule* mod = dynamic_cast<NUModule*>(context->getDeclarationScope());
  if ((mod != NULL) && mIODB->isInputNet(mod->getName(), name))
    *flags = eInputNet;

  *flags = NetRedeclare(*flags, eDMWireNet); // KLG:Treat all the nets as wire
  // Set the signed flag of this net
  calcSignedNetFlags(vh_net, flags, context);
  return err_code;
}

Populate::ErrorCode VhdlPopulate::initializeDeclaration(vhExpr initialVal,
                                                        LFContext *context,
                                                        NUNet* net)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  // Signals everywhere and non-loop variables everywhere except in 
  // subprograms need to have the assignment 'net <= initialVal' 
  // populated into an initialBlock. So find/create the block in the module.
  // And then use block->addStmt()

  // Variables in subprogram scope need to have initialization statements
  // created in the subprogram sequential block before any of the sequential
  // code in the block. So we use context->addStmt()

  // Indicate population of declaration initialization statement outside
  // the current scope, in the module's initial block.
  DeclInitIndicator dii(context);

  NUModule* mod = context->getModule();
  NUExpr* rhs;
  NU_ASSERT(net, mod);
  // Collect all statements created during expression population like
  // function calls into the statement list.
  NUStmtList init_stmts;
  context->pushStmtList(&init_stmts);
  temp_err_code = expr(initialVal, context, net->getBitSize(), &rhs);
  context->popStmtList();
  if ( not errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    const SourceLocator& loc = net->getLoc();
    // First add all collected statements to initial block. Function calls have
    // to appear before their return values can be used to initialize net.
    for (NUStmtListIter itr = init_stmts.begin(); itr != init_stmts.end(); ++itr) {
      addStmtToInitialBlock(context, mod, *itr, net, loc);
    }

    rhs->resize (rhs->determineBitSize ());
    NUMemoryNet* mem = net->getMemoryNet();
    if(mem && rhs->getType() == NUExpr::eNUConstNoXZ)
    {
      const UInt32 outerDim = mem->getNumDims() - 1;
      const ConstantRange *thisRange = mem->getRange( outerDim );
      const UInt32 noOfRows = thisRange->getLength();
      SInt32 rowLsb = thisRange->getLsb();
      SInt32 rowMsb = thisRange->getMsb();
      SInt32 incr = (rowLsb > rowMsb) ? 1 : -1;

      for (UInt32 i = 0; i < noOfRows ; i++)
      {
        NUExpr* indexExpr = (NUExpr*) NUConst::create(true, rowMsb, 32, loc);
        NULvalue* lv = new NUMemselLvalue(mem, indexExpr, loc);

        NUStmt* assign = context->stmtFactory()->createBlockingAssign(lv, rhs, false, loc);

        // Create an initial block if there is none in module and then add stmt
        addStmtToInitialBlock(context, mod, assign, net, loc);
        rowMsb += incr;
      }
    }
    else if (mem && rhs->getType() != NUExpr::eNUIdentRvalue )
    {
      // It's a constant array initialization. The Rvalue might be a
      // NUConcatOp or a NUIdentRvalue.  If it's not, we're screwed.
      // The concat is handled here, the rvalue case gets handled like
      // the non-memory case.
      CopyContext cc(0,0);
      NUConcatOp *concat = dynamic_cast<NUConcatOp*>(rhs);
      NU_ASSERT( concat != NULL, rhs );
      const UInt32 outerDim = mem->getNumDims() - 1;
      const ConstantRange *thisRange = mem->getRange( outerDim );
      const UInt32 noOfRows = thisRange->getLength();
      // Sanity check
      if ( concat->getNumArgs() != noOfRows )
      {
        POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "RHS of constant array assignment is not of a compatible aggregate type"), &err_code);
      }
      else
      {
        // Full assignment of NUMemoryNet is not allowed. Break it up into 
        // multiple row assignments.
        SInt32 rowLsb = thisRange->getLsb();
        SInt32 rowMsb = thisRange->getMsb();
        SInt32 incr = (rowLsb > rowMsb) ? 1 : -1;
        for (UInt32 i = 0; i < noOfRows ; i++)
        {
          NUExpr* indexExpr = (NUExpr*) NUConst::create(true, rowMsb, 32, loc);
          NULvalue* lv = new NUMemselLvalue(mem, indexExpr, loc);

          NUExpr* rv = concat->getArg(i)->copy( cc );
          NUStmt* assign = context->stmtFactory()->createBlockingAssign(lv, rv, false, loc);

          // Create an initial block if there is none in module and then add stmt
          addStmtToInitialBlock(context, mod, assign, net, loc);

          rowMsb += incr;
        }
      } // else

      delete concat;
    }
    else
    {
      NULvalue* lv = new NUIdentLvalue(net, loc);
      NUStmt* assign = context->stmtFactory()->createBlockingAssign(lv, rhs, false, loc);
      
      // Create an initial block if there is none in module and then add stmt
      addStmtToInitialBlock(context, mod, assign, net, loc);
    }
  } // if
  else
  {
    err_code = eFailPopulate;
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::var(vhNode vh_var,
                  LFContext *context,
                  NUNet **the_net,
                  const char* prefix,
                  bool  isDeclaredVar)
{
  // Create an aggressive evaluation enabler. When populating
  // declarations evaluate expressions even when they're not static. In
  // declaration scope, there's no chance of variable/signal values being modified.
  // So their initial values can be used by declaration sizing/initializing.
  AggressiveEvalEnabler aee(this);

  *the_net = 0;
  ErrorCode err_code = eSuccess;
  StringAtom* name;
  
  if (!prefix)
    name = context->vhdlIntern(vhGetName(vh_var));
  else
    name = context->getModule()->gensym(prefix, vhGetName(vh_var));
  SourceLocator loc = locator(vh_var);

  if ( vhGetObjType( vh_var ) != VHFORINDEX )
  {
    // Make sure type is not a physical type.  We don't do them.
    vhNode vh_type = vhGetTypeDef( vhGetType( vh_var ));
    if ( vhGetObjType( vh_type ) == VHPHYSICALTYPE )
    {
      mMsgContext->PhysicalTypesUnsupported( &loc, name->str( ));
      return eSuccess;
    }
  }

  NetFlags flags(eAllocatedNet);
  if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_var, context ))
  {
    err_code = processRecordNetDecl( vh_var, name, flags, context );
  }
  else
  {
    NUScope *declaration_scope = context->getDeclarationScope();

    if (declaration_scope->getScopeType() != NUScope::eModule) {
      flags = NetFlags(flags | eBlockLocalNet);
    }

    // We do not infer state for any locals to tasks or functions
    // Also handle task/function args
    if (declaration_scope->inTFHier()) {
      flags = NetFlags(flags | eNonStaticNet);

      // Do Not check port type for those variables which are been
      // created within cbuild (Not declared in the design)
      if(isDeclaredVar)
      {
        switch (vhGetPortType(vh_var)) 
        {
        case VH_DEFAULT_IN:
        case VH_IN:
          flags = NetFlags(flags | eInputNet);
          break;
        case VH_OUT:
        case VH_BUFFER:
          flags = NetFlags(flags | eOutputNet);
          break;
        case VH_INOUT:
          flags = NetFlags(flags | eBidNet);
          break;
        case VH_NOT_PORT:
          if ( VHFILEDECL == vhGetObjType(vh_var) ) {
            // files in function parameter lists have an input direction
            // but it is not explicit in the source code so we force it here
            flags = NetFlags(flags | eInputNet);
          }
          break; // This is only a variable declared in declarative_region.
        case VH_LINKAGE:
          POPULATE_FAILURE(mMsgContext->IllegalPortType(&loc, "LINKAGE"), &err_code);
          break;
        case VH_ERROR_PORTTYPE:
        {
          POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unable to determine subprogram parameter mode(input,output)."), &err_code);
        }
        break;
        }
      }
    }
    // Set sign flag of the var 
    calcSignedNetFlags(vh_var, &flags, context);

    ErrorCode temp_err_code = allocNet( vh_var, name, flags, declaration_scope, the_net,
                                        context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code))
    {
      return temp_err_code;
    }
    temp_err_code = mapNet(declaration_scope, vh_var, *the_net);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code))
    {
      return temp_err_code;
    }

    // Intialize Declarations, if possible. 
    // Avoid ports and loop-variable, VHFORINDEX (which is not declared).
    // Avoid Alias Declarations.
    if (!(*the_net)->isPort() && isDeclaredVar && (not isAliasObject(vh_var))) {
      vhExpr initVal = vhGetInitialValue(vh_var);
      if (initVal != NULL) {
        temp_err_code = initializeDeclaration(initVal, context, *the_net);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code) )
          return temp_err_code;

      } // if
    }
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::netRange( vhNode vh_net, ConstantRangeArray *rangeArray,
                        LFContext *context )
{
  ErrorCode err_code = eSuccess, temp_err_code;

  SourceLocator loc = locator(vh_net);
  VhdlRangeInfoVector dimVector;
  dimVector.reserve( 3 );

  temp_err_code = getVhExprRange( static_cast<vhExpr>( vh_net ), dimVector, loc,
                                  false, false, false, context );
  if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, NULL ))
  {
    if (temp_err_code != eFatal ) {
      // try again using the initial value if available
      vhExpr initialValue = vhGetInitialValue( vh_net );
      if ( initialValue )
      {
        temp_err_code = getVhExprRange( initialValue, dimVector, loc,
                                        false, false, false, context );
      }
    }
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      UtString msg( "Unable to determine the width of " );
      getVhdlObjName( vh_net, &msg );
      mMsgContext->JaguarConsistency( &loc, msg.c_str( ));
      return err_code;
    }
  }

  VhdlRangeInfo vrange;
  ConstantRange *crange = NULL;

  // This code deliberately reverses ranges.  The way dimVector is
  // populated, dimVector[0] is the outermost range, where
  // rangeArray[0] needs to be the innermost range in order to
  // allocate the net properly.
  for ( SInt32 i = dimVector.size() - 1; i >= 0; --i )
  {
    vrange = dimVector[i];
    if ( !vrange.isVector() )
    {
      // handle a decl like e.g. bit_vector(0 to 0)
      if ( vhGetObjType(vh_net) == VHSIGNAL || 
           vhGetObjType(vh_net) == VHVARIABLE ||
           vhGetObjType(vh_net) == VHCONSTANT )
      {
        vhNode sind = vhGetSubTypeInd(vh_net);
        vhNode vh_typedef = vhGetTypeDef(sind);
        if (vhGetObjType(vh_typedef) == VHCONSARRAY ||
            vhGetObjType(vh_typedef) == VHUNCONSARRAY )
        {
          crange = new ConstantRange( vrange.getMsb(loc), vrange.getLsb(loc) );
        }
      }
    }
    else
    {
      crange = new ConstantRange( vrange.getMsb(loc), vrange.getLsb(loc) );
    }
    LOC_ASSERT( crange != NULL, loc );
    rangeArray->push_back( crange );
  }
  return err_code;
}


VectorNetFlags VhdlPopulate::calcSignedNetFlags(vhNode node, NetFlags* flags, LFContext* context, VhdlRangeInfo* copyRange)
{
  VhdlRangeInfo subrange;
  VectorNetFlags vflags = eNoneVectorNet;

  vhNode vhType = evaluateIntegerSubtype (node, &subrange, context);
  if (copyRange)
    *copyRange = subrange;

  bool intType = isIntegerType(vhType);
  bool vhSignedType = (eVHSIGNED == getVhExprSignType((vhExpr)node));

  if (intType) {
    // If this is an integer type, it's signed if its range constraint
    // includes negative values.
    SourceLocator loc = locator(node);
    // Check the bounds of the type
    if (subrange.getMsb (loc) < 0 || subrange.getLsb (loc) < 0) {
      vflags = eSignedSubrange; // one or more of the range values are negative
      // force this to be a signed integral value
      *flags = NetFlags (*flags | eSigned);
    } else 
      vflags = eUnsignedSubrange; // both ranges were >= 0
  } else if (vhSignedType) {
    *flags = NetFlags (*flags | eSigned);
  }
  return vflags;
}

// Determine whether NODE refers to a subrange of an integral type and fill in the
// bounds of  SUBRANGE.
vhNode VhdlPopulate::evaluateIntegerSubtype (vhNode node, VhdlRangeInfo* subrange, LFContext *context)
{
  if( not subrange->isValid ())
    subrange->putVector(UtSINT32_MAX, UtSINT32_MIN); // Assume unbounded integer

  vhNode vhType=0;

  switch (vhGetObjType (node)) {
  case VHRANGE:
    vhType = vhGetExpressionType ((vhExpr)node);
    getLeftAndRightBounds (node, subrange, context);
    break;

  case VHFORINDEX:
    // Variable implicitly typed to match range expression for iteration.
    vhType = vhGetExpressionType ((vhExpr)vhGetDisRange ((vhExpr)node));
    break;

  case VHINTTYPE:
    vhType = evaluateIntegerSubtype (vhGetRangeConstraint (node), subrange, context);
    break;
    
  case VHEXTERNAL:
    vhType = evaluateIntegerSubtype (vhGetActualObj (node), subrange, context);
    break;

  case VHRANGECONSTRAINT:
    vhType = evaluateIntegerSubtype (vhGetRange (node), subrange, context);
    break;

  case VHACCESSTYPE:
    vhType = evaluateIntegerSubtype (vhGetAccessType (node), subrange, context);
    break;

  case VHINDNAME:
  case VHFUNCCALL:
  case VHSLICENAME:
    vhType = evaluateIntegerSubtype (vhGetExpressionType ((vhExpr)node), subrange, context);
    break;

  case VHSELECTEDNAME:
    vhType = evaluateIntegerSubtype (vhGetActualObj (node), subrange, context);
    break;

  case VHSTRING:
  case VHBITSTRING:
  case VHBINARY:
  case VHUNARY:
  case VHINDEXCONSTRAINT:
  case VHFLOATTYPE:
  case VHFILEDECL:
  case VHRECORD:
    // irrelevant cases - not integer subtypes
    break;
    
  case VHCONSARRAY:
  case VHUNCONSARRAY:
    vhType = evaluateIntegerSubtype (vhGetEleSubTypeInd (node), subrange, context);
    break;

  case VHSUBTYPEIND:
    vhType = vhGetConstraint (node);
    if (vhType && VHINDEXCONSTRAINT!=vhGetObjType (vhType))
      vhType = evaluateIntegerSubtype (vhType, subrange, context);
    else
      vhType = evaluateIntegerSubtype (vhGetType (static_cast<vhExpr>(node)), subrange, context);
    break;

  case VHVARIABLE:
  case VHSIGNAL:
  case VHCONSTANT:
  case VHSUBTYPEDECL:
    vhType  = evaluateIntegerSubtype (vhGetSubTypeInd (node), subrange, context);
    break;

  case VHENUMTYPE:
    evaluateIntegerSubtype (vhGetRangeConstraint (node), subrange, context);
    break;

  case VHELEMENTDECL:
    vhType = vhGetEleSubTypeInd (node);
    vhType = evaluateIntegerSubtype (vhType, subrange, context);
    break;

  case VHTYPEDECL:
    vhType = evaluateIntegerSubtype (vhGetTypeDef (node), subrange, context);
    break;

  default:
    vhType = vhGetType (node);
    if (vhType)
      evaluateIntegerSubtype (vhType, subrange, context);
    break;
  }

  if (not vhType)
    return node;

  return vhType;
}

Populate::ErrorCode
VhdlPopulate::allocNet( vhNode node, StringAtom *name, NetFlags flags,
                        NUScope *declaration_scope, NUNet **the_net,
                        LFContext *context,
                        VhNodeArray* recConstraintArray /*=NULL*/ )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  
  if ( mCarbonContext->isNewRecord() && isRecordNet( node, context ))
  {
    VhNodeArray localArray;
    // If we don't have a constraint array, pass in an empty one
    if ( recConstraintArray == NULL )
      recConstraintArray = &localArray;
    err_code = allocRecordNet( node, name, recConstraintArray, flags,
                               declaration_scope, context, NULL, the_net );
  }
  else
  {
    ConstantRangeArray dimArray;
    temp_err_code = netRange( node, &dimArray, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      *the_net = 0;
      return temp_err_code;
    }

    // Compute vector net flags (signed/unsigned integral subranges)
    VhdlRangeInfo subrange;
    VectorNetFlags vflags = calcSignedNetFlags (node, &flags, context, &subrange);
    bool isIntegerSubrange = (vflags == eSignedSubrange) || ( vflags == eUnsignedSubrange);
  
    const SourceLocator loc = locator( node );
    temp_err_code = allocNet( name, flags, vflags, &dimArray, declaration_scope,
                              loc, the_net, context, recConstraintArray );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return temp_err_code;
    }

    if (mConstraintRange && *the_net)
    {
      temp_err_code = mapConstraintRange(*the_net);
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
	  return temp_err_code;
      }
    }

    if (isIntegerSubrange && *the_net && (*the_net)->isPrimaryPort())
    {
      // integer is a primary port. We need to save off port information
      // for testdriver, db, etc.
      if (subrange.isValid())
      {
        SInt32 msb = subrange.getMsb(loc);
        SInt32 lsb = subrange.getLsb(loc);
        if ((msb != UtSINT32_MIN) || (lsb != UtSINT32_MAX))
        {
          // sub range
          VhdlPortTypeMap* portTypeMap = mCarbonContext->getVhdlPortTypeMap();
          VhdlPortType* portType = portTypeMap->genPortTypeBasic(name->str());
          portType->putIntegerValueRange(msb, lsb);
          //UtIO::cout() << "Found integer subrange: " << name->str() << ": " << msb << "," << lsb << UtIO::endl;
        }
      }
    }

    // Additional processing to make sure that LINE objects are flagged
    // correctly and initialized appropriately.
    if ( mArg->getBoolValue( CarbonContext::scVhdlEnableFileIO ))
    {
      if ( isLineVariable( node ) )
      {
        NUVectorNet *vn = (*the_net)->castVectorNet();
        vflags = VectorNetFlags( vn->getVectorNetFlags() | eVhdlLineNet );
        vn->putVectorNetFlags( vflags );

        // Filter out formal parameters to functions and tasks
        if (! (vn->isPort () && vn->isBlockLocal () && vn->isNonStatic ())) {
          temp_err_code = initializeLineVar( context, vn, loc );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          {
            return temp_err_code;
          }
        }
      }
    }
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::allocNet( StringAtom *name, NetFlags flags, VectorNetFlags vflags,
                        ConstantRangeArray *dimArray,
                        NUScope *declaration_scope, const SourceLocator &loc,
                        NUNet **the_net, LFContext *context, 
                        UtArray<vhNode>* recConstraintArray /*=NULL*/ )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  if ( recConstraintArray != NULL )
  {
    const SInt32 size = recConstraintArray->size();
    for ( SInt32 i = size - 1; i >= 0; --i )
    {
      vhNode vh_constraint = (*recConstraintArray)[i];
      // Extract the range specified in vh_constraint into externalRange.
      if ( vh_constraint != NULL )
      {
        ConstantRangeArray externRanges;
        temp_err_code = netRange( vh_constraint, &externRanges, context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        {
          return temp_err_code;
        }
        LOC_ASSERT( externRanges.size() == 1, loc );
        dimArray->insert( dimArray->end(), externRanges.begin(), externRanges.end() );
      }
    }
  }

  if ( dimArray->size() > 1 )
  {
    // array of arrays to be mapped to memory.
    flags = NetRedeclare( flags, eDMReg2DNet );
    *the_net = new NUMemoryNet( name, 1, dimArray, flags, vflags, declaration_scope, loc );
    if ((*the_net)->getBitSize() == 0)
    {
      POPULATE_FAILURE(mMsgContext->ObjectTooLarge( &loc ), &err_code);
      return err_code;
    }
  }
  else if ( dimArray->size() == 1 )
  {
    *the_net = new NUVectorNet( name, (*dimArray)[0], flags, vflags, declaration_scope, loc );
  } 
  else 
  {
    LOC_ASSERT (vflags==0, loc);
    *the_net = new NUBitNet( name, flags, declaration_scope, loc );
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::net(vhNode vh_net, LFContext *context, NUNet **the_net)
{
  *the_net = 0;
  ErrorCode err_code = eSuccess;

  JaguarString vh_name(vhGetName( vh_net ));
  // Make sure type is not a physical type.  We don't do them.
  vhNode vh_type = vhGetTypeDef( vhGetType( vh_net ));
  if ( vhGetObjType( vh_type ) == VHPHYSICALTYPE )
  {
    const SourceLocator loc = locator( vh_net );
    mMsgContext->PhysicalTypesUnsupported( &loc, vh_name );
    return eSuccess;
  }

  StringAtom* name = context->vhdlIntern( vh_name );
  NetFlags flags;
  ErrorCode temp_err_code = netFlags(vh_net, name, context, &flags);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;
  flags = NetFlags(flags | eAllocatedNet);

  if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_net, context ))
  {
    temp_err_code = processRecordNetDecl( vh_net, name, flags, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
  }
  else
  {
    NUScope *declaration_scope = context->getDeclarationScope();
    if (declaration_scope->getScopeType() != NUScope::eModule) {
      flags = NetFlags(flags | eBlockLocalNet);
    }

    // We do not infer state for any locals to tasks or functions
    if (declaration_scope->inTFHier()) {
      flags = NetFlags(flags | eNonStaticNet);
    }

    if ( vhGetObjType( vh_type ) == VHFLOATTYPE )
    {
      flags = NetRedeclare(flags, eDMRealNet|eSigned);
    }

    temp_err_code = allocNet( vh_net, name, flags, declaration_scope, the_net, context );
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
    {
      return temp_err_code;
    }

    // Detect trireg on primary port.
    if ((*the_net)->atTopLevel() and (*the_net)->isPort() and 
        (*the_net)->isTrireg())
    {
      mMsgContext->TriregOnPrimaryPort(*the_net);
    }

    temp_err_code = mapNet(declaration_scope, vh_net, *the_net);
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
    {
      return temp_err_code;
    }

    // Initialize Declarations, if possible. 
    // Avoid ports and alias declarations
    if ((!(*the_net)->isPort() && not isAliasObject(vh_net)) ||
        ( (vhGetObjType(vh_net) == VHCONSTANT) && vhIsGeneric(static_cast<vhExpr>( vh_net ))))
    {
      vhExpr initVal = vhGetInitialValue(vh_net);
      if (initVal != NULL)
      {
        temp_err_code = initializeDeclaration(initVal, context, *the_net);
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
      }
    } // if
  }

  return err_code;
}


Populate::ErrorCode VhdlPopulate::local(vhNode vh_net,
                                        LFContext *context,
                                        NUNet **the_net)
{
  // Create an aggressive evaluation enabler. When populating
  // declarations evaluate expressions even when they're not static. In
  // declaration scope, there's no chance of variable/signal values being modified.
  // So their initial values can be used by declaration sizing/initializing.
  AggressiveEvalEnabler aee(this);

  *the_net = 0;
  // Do not treat the 'GUARD' signal as block interface, rather treat it
  // as a signal without any direction so that we can assign the guard
  // expression to it.
  if (vhGetPortType(vh_net) != VH_NOT_PORT && 
      0 != strcmp(vhGetName(vh_net),"GUARD")) 
  {
    return eSuccess;
  } 
  else 
  {
    return net(vh_net, context, the_net);
  }
}


Populate::ErrorCode
VhdlPopulate::port(vhNode vh_port,
                   LFContext *context,
                   NUNet **the_net)
{
  *the_net = 0;
  ErrorCode err_code = eSuccess;

  if (not vh_port) 
  {
    // Empty port
    return eSuccess;
  }

  vhObjType expr_type = vhGetObjType(vh_port);
  switch (expr_type) 
  {
  case VHOBJECT:
  case VHSIGNAL:
  {
    vhNode vh_ident = vhGetActualObj(vh_port);
    if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_ident, context ))
    {
      err_code = processRecordPortDecl( vh_ident, context );
      return err_code;
    }
    ErrorCode temp_err_code = net(vh_ident, context, the_net);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }
  break;
  case VHCONSTANT: // TBD
  default:
  {
    SourceLocator loc = locator(vh_port);
    POPULATE_FAILURE(mMsgContext->UnimplPort(&loc), &err_code);
    return err_code;
    break;
  }
  }

  SourceLocator loc = locator(vh_port);
  NULvalue *the_lvalue = new NUIdentLvalue(*the_net, loc);
  ErrorCode temp_err_code = mPopulate->mapPort(context->getModule(), vh_port, the_lvalue);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
  {
    return temp_err_code;
  }

  return err_code;
}

Populate::ErrorCode VhdlPopulate::portConnection(vhExpr vh_actual,
                                                 LFContext *context,
                                                 NUNet* formal, 
                                                 NUPortConnection **the_conn)
{
  *the_conn = 0;
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(vh_actual);

  if (formal->isInput()) 
  {
    NUExpr *actual = 0;
    if (vh_actual != 0) 
    {
      ErrorCode temp_err_code = expr(vh_actual, context, 0, &actual);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      actual->resize(actual->determineBitSize());
    }
    *the_conn = new NUPortConnectionInput(actual, formal, loc);
  } 
  else if (formal->isOutput()) 
  {
    NULvalue *actual = 0;
    if (vh_actual != 0) 
    {
      ErrorCode temp_err_code = lvalue(vh_actual, context, &actual);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      actual->resize();
    }
    *the_conn = new NUPortConnectionOutput(actual, formal, loc);
  } 
  else if (formal->isBid()) 
  {
    NULvalue *actual = 0;
    if (vh_actual != 0) 
    {
      ErrorCode temp_err_code = lvalue(vh_actual, context, &actual);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      actual->resize();
    }
    *the_conn = new NUPortConnectionBid(actual, formal, loc);
  } 
  else 
  {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unknown port type"), &err_code);
    return err_code;
  }

  return err_code;
}

// Function to process unconnected ports
Populate::ErrorCode VhdlPopulate::unconnectedPort(mvvNode mvv_inst,
                                                  NUNet *formal,
                                                  LFContext *,
                                                  NUPortConnection **the_conn)
{
  SourceLocator loc = mPopulate->locator(mvv_inst);
  ErrorCode err_code = eSuccess;

  *the_conn = 0;
    
  if (formal->isInput()) 
  {
    *the_conn = new NUPortConnectionInput(0, formal, loc);
  } 
  else if (formal->isOutput()) 
  {
    *the_conn = new NUPortConnectionOutput(0, formal, loc);
  } 
  else if (formal->isBid()) 
  {
    *the_conn = new NUPortConnectionBid(0, formal, loc);
  } 
  else 
  {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unknown port type"), &err_code);
  }
  return err_code;
}


// Yet one more of those methods bizarrely unimplemented by Interra.
// There's probably cases I haven't hit yet that are not covered.
bool
VhdlPopulate::getVhdlObjName( vhNode node, UtString* name )
{
  if ( node != NULL )
  {
    vhObjType objtype = vhGetObjType( node );
    if ( objtype == VHOBJECT ) {
      return getVhdlObjName( vhGetActualObj( node ), name);
    }
    else if ( objtype == VHSIMPLENAME ) {
      JaguarString simpName(vhGetSimpleName( static_cast<vhExpr>( node )));
      *name << simpName;
    }
    else if ( objtype == VHINDNAME || objtype == VHSLICENAME ) {
      return getVhdlObjName( vhGetPrefix( static_cast<vhExpr>( node )), name);
    }
    else if ( objtype == VHSELECTEDNAME ) {
      VhdlPopulate::createRecordName( static_cast<vhExpr>( node ), name);
    }
    else if ( objtype == VHFUNCCALL ) {
      return getVhdlObjName( vhGetMaster( node ), name);
    }
    else if ( objtype == VHSUBPROGDECL ) {
      *name << vhGetSubProgName( node );
    }
    else if ( objtype == VHENTITY || objtype == VHARCH ) {
      JaguarString entName( vhGetEntityName( node ));
      *name << entName;
    }
    else if ( objtype == VHPROCESS ) {
      JaguarString processName( vhGetLabel( node ));
      if (processName != NULL) {
        *name << processName;
      }
    }
    else if ( objtype == VHALL ) {
      *name << "(ALL)";
    }
    else if ( objtype == VHOPSTRING ){
      JaguarString opstring(vhGetStringVal(static_cast<vhExpr>(node)));
      *name << opstring;
    }
    else {
      JaguarString nodeName(vhGetName( node ));
      *name << nodeName;
    }
  }

  // Be paranoid and make sure that we put something here.
  if (name->empty()) {
    *name = "(null)";
    return false;
  }
  else {
    return true;
  }
} // void VhdlPopulate::getVhdlObjName

Populate::ErrorCode 

VhdlPopulate::packageNet( vhNode vh_net, LFContext *context, NUNet **the_net )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator( vh_net );


  *the_net = NULL;
  vhNode vh_scope = vhGetScope( vh_net );
  if ( ( vhGetObjType( vh_scope ) != VHPACKAGEDECL) && (vhGetObjType( vh_scope ) != VHPACKAGEBODY) )
  {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unexpected scope for package signal" ), &err_code);
    return err_code;
  }

  // Construct the package hierarchical name
  JaguarString libName( vhGetLibName( vh_scope ));
  JaguarString packName( vhGetPackName( vh_scope ));
  StringAtom *packIdStr = createPackIdStr( libName, packName, context );
  NUNamedDeclarationScope *packScope;
  lookupPackage( packIdStr, &packScope, context );
  
  // Create the package scope and the net we're looking for in it.
  if ( packScope == NULL )
  {
    // We can't find the package for this node.
    temp_err_code = createPackageScope( vh_scope, context, &packScope );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
  }

  // See if this net already exists; if not, create it
  NUNet *packNet = NULL;
  StringAtom* netAtom = context->vhdlIntern(vhGetName( vh_net ));
  for ( NUNetLoop p = packScope->loopLocals(); !p.atEnd(); ++p)
  {
    NUNet *localnet = *p;
    if ( !strcmp( localnet->getName()->str(), netAtom->str() ))
    {
      packNet = localnet;
      break;
    }
  }

  // The net doesn't exist; create it
  if ( packNet == NULL )
  {
    context->pushDeclarationScope( packScope );

    temp_err_code = net( vh_net, context, &packNet );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    if ( packNet )
      packScope->addLocal( packNet );

    (void)context->popDeclarationScope();
  }
  
  // packNet is the hieriarchical resolution to vh_net.  We need to make
  // a hierref net for vh_net and connect it up to packNet now.

  // First, do we have one in our scope already?
  NUModule *module = context->getModule();
  StringAtom *moduleAtom = context->getRootModule()->getName();
  UtString libString;
  getActualVhdlLibName(libName, libString);
  StringAtom *libAtom = context->vhdlIntern( libString.c_str( ));
  StringAtom *packAtom = context->vhdlIntern( packName );
  AtomArray netHierName;
  netHierName.push_back( moduleAtom );
  netHierName.push_back( libAtom );
  netHierName.push_back( packAtom );
  netHierName.push_back( netAtom );
  *the_net = module->findNetHierRef( netHierName );

  // We didn't find it in our scope, so create it
  if ( *the_net == NULL )
  {
#if DEBUG_VHDL_HIERREFS
    UtIO::cout() << "Creating hiername " << moduleAtom->str() << "." 
                 << libAtom->str() << "." << packAtom->str() << "." << netAtom->str()
                 << " in module " << module->getName()->str() << "\n";
#endif
    *the_net = packNet->buildHierRef(module, netHierName);

  } // if
  
  return err_code;
} // VhdlPopulate::packageNet

