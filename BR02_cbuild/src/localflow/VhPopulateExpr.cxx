// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUTaskEnable.h"
#include "reduce/Fold.h"
#include "util/CbuildMsgContext.h"
#include "util/CarbonTypeUtil.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/JaguarContext.h"
#include "compiler_driver/JaguarBrowser.h"

#include <math.h> // needed for log, rint, and pow

vhExpr sGetConstNode(vhExpr node);
 
VhdlRangeInfo::VhdlRangeInfo() {
  mIsValid = false;
  mIsVector = false;
  mIsWidthValid = false;
  mIsMsbLsbValid = false;
  mIsNullRange = false;
  mMsb = 0;
  mLsb = 0;
  mMsbExpr = mLsbExpr = 0;
}

VhdlRangeInfo::VhdlRangeInfo (SInt32 msb, SInt32 lsb) {
  mIsValid = true;
  mIsVector = (msb != lsb);
  mIsWidthValid = true;
  mIsMsbLsbValid = true;
  mIsNullRange = false;
  mMsb = msb;
  mLsb = lsb;
  mMsbExpr = mLsbExpr = 0;
}

VhdlRangeInfo::~VhdlRangeInfo () {
  delete mMsbExpr;
  delete mLsbExpr;
}

void VhdlRangeInfo::putVector(SInt32 msb, SInt32 lsb) {
  mMsb = msb;
  mLsb = lsb;
  mIsVector = true;
  mIsValid = true;
  mIsWidthValid = true;
  mIsMsbLsbValid = true;
}

void VhdlRangeInfo::putVector(NUExpr* msb, NUExpr* lsb) {
  delete mMsbExpr;
  mMsbExpr = msb;
  delete mLsbExpr;
  mLsbExpr = lsb;
  mIsVector = true;
  mIsValid = true;
  mIsWidthValid = true;
  mIsMsbLsbValid = false;
}

void VhdlRangeInfo::putScalar() {
  mLsb = 0;
  mMsb = 0;
  delete mMsbExpr;
  delete mLsbExpr;
  mMsbExpr = mLsbExpr = 0;

  mIsVector = false;
  mIsWidthValid = true;         // will be 1
  mIsValid = true;
  mIsMsbLsbValid = false;
}

void VhdlRangeInfo::putInvalid() {
  mIsWidthValid = false;
  mIsVector = false;
  mIsMsbLsbValid = false;
  mIsValid = false;
  delete mMsbExpr;
  delete mLsbExpr;
  mMsbExpr = mLsbExpr = 0;
  mMsb = mLsb = 0;
}

// We've inferred the width of the slice without being able to determine the absolute
// value of the range.  
void VhdlRangeInfo::putWidth(UInt32 width) {
  mLsb = 0;
  mMsb = width - 1;
  mIsValid = true;
  mIsMsbLsbValid = false;
  mIsWidthValid = true;
  mIsVector = ( width > 1 );
}

void VhdlRangeInfo::putNullRange(bool isNullRange) {
  mIsNullRange = isNullRange;
}

SInt32 VhdlRangeInfo::getLsb(const SourceLocator& loc) const {
  if (!mIsMsbLsbValid) {
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "VHDL range error: Not a valid integer range; cannot retrieve LSB";
    INFO_ASSERT(mIsMsbLsbValid, errMsg.c_str());
  }
  return mLsb;
}

SInt32 VhdlRangeInfo::getMsb(const SourceLocator& loc) const {
  if (!mIsMsbLsbValid) {
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "VHDL range error: Not a valid integer range; cannot retrieve LSB";
    INFO_ASSERT(mIsMsbLsbValid, errMsg.c_str());
  }
  return mMsb;
}

NUExpr* VhdlRangeInfo::getLsbExpr (const SourceLocator& loc) const {
  if (!mLsbExpr) {
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "VHDL range error: Not a valid expression range; cannot retrieve LSB expression";
    INFO_ASSERT(mLsbExpr, errMsg.c_str());
  }
  return mLsbExpr;
}

NUExpr* VhdlRangeInfo::getMsbExpr (const SourceLocator& loc) const {
  if (!mMsbExpr) {
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "VHDL range error: Not a valid expression range; cannot retrieve MSB expression";
    INFO_ASSERT(mMsbExpr, errMsg.c_str());
  }
  return mMsbExpr;
}

//! Get the width of this
UInt32 VhdlRangeInfo::getWidth(const SourceLocator& loc) const {
  if (!mIsWidthValid) {
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "VHDL range error: Range width not valid";
    INFO_ASSERT(mIsWidthValid, errMsg.c_str());
  }

  // Should do something clever for dynamic sized vs.
  // static range expression.
  if (isDynamic()) {
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "VHDL range error: Cannot determine width of dynamic range";
    INFO_ASSERT(not isDynamic(), errMsg.c_str());
  }

  // gcc 3.4.3 seems to dislike std::abs(long long int)
  //
  SInt32 wtemp = SInt64 (mLsb) - SInt64 (mMsb);

  UInt64 maxWidth = ((wtemp < SInt64 (0)) ? -wtemp : wtemp) + SInt64 (1);

  if (maxWidth > UtUINT32_MAX)
    // Josh thinks this ought to ASSERT if the range is larger than 100k or so.
    // I'm not so sure about this - I suspect that there are ways that we want
    // to allow a range of 2^32 to be valid - and only an attempt to allocate an
    // object that large should trigger any assertions or errors.
    //
    return UtUINT32_MAX;
  else
    return maxWidth;
}

//! Assignment
VhdlRangeInfo& VhdlRangeInfo::operator=(const VhdlRangeInfo& src)
{
  mIsWidthValid = src.mIsWidthValid;
  mIsVector = src.mIsVector;
  mIsMsbLsbValid = src.mIsMsbLsbValid;
  mIsValid = src.mIsValid;
  CopyContext cc (0,0);
  if (src.mMsbExpr) {
    delete mMsbExpr;
    mMsbExpr = src.mMsbExpr->copy (cc);
  }

  if (src.mLsbExpr) {
    delete mLsbExpr;
    mLsbExpr = src.mLsbExpr->copy (cc);
  }

  mMsb = src.mMsb;
  mLsb = src.mLsb;

  return *this;
}

//! swap members
void VhdlRangeInfo::swap (void) {
  std::swap (mMsb, mLsb);
  std::swap (mMsbExpr, mLsbExpr);
}


static bool
sTypeIsSIGNED( vhNode vh_type )
{
  JaguarString vh_name(vhGetName( vh_type ));
  vhNode vh_scope = vhGetScope( vh_type );
  if ( !strcasecmp( vh_name, "SIGNED" ) &&
       ( vhGetObjType( vh_scope ) == VHPACKAGEDECL &&
         !strcasecmp( vhGetLibName( vh_scope ), "IEEE" )))
    return true;
  return false;
}

//! This very simple class just looks for the signal
//! variable or constant that may be part of a more complicated
//! expression and returns it.
class DeclarationFinder : public JaguarBrowserCallback
{
public:

  DeclarationFinder()
    : mPort(NULL) {
  }

  virtual ~DeclarationFinder() {
  }

  Status vhsignal(Phase phase, vhNode node) {
    if (phase == ePre) {
      mPort = node;
    }
    return eNormal;
  }

  Status vhvariable(Phase phase, vhNode node) {
    if (phase == ePre) {
      mPort = node;
    }
    return eNormal;
  }

  Status vhconstant(Phase phase, vhNode node)
  {
    if ((phase == ePre) && vhIsSubProgInterfaceConstant(node)) {
      mPort = node;
    }
    return eNormal;
  }

  vhNode getDeclaration() const { return mPort; }

private:
  vhNode mPort;
};

//! Saves current dynamic values and applies propagated constants to nets.
//! Restores the saved dynamic values upon deletion.
/*!
  This is used to push propagated constants across function boundary by
  setting constant values as dynamic values on function arguments. When
  the function population is done, these values are restored.
*/
class DynValCache : public JaguarBrowserCallback
{
public:
  DynValCache(VhdlPopulate* populator, LFContext* context, bool isNewRecord)
    : mPopulator(populator),
      mContext(context),
      mIsNewRecord(isNewRecord),
      mSuccess(true)
  {
  }

  virtual ~DynValCache()
  {
    restoreAll();
  }
  
  Status vhvariable(Phase phase, vhNode node)
  {
    if (phase == ePre) {
      // Cache variable's current dynamic value and apply
      // new constant propagated value.
      mSuccess = cacheDynVal(node);
      if (!mSuccess) {
        return eStop;
      }
    }
    return eNormal;
  }

  Status vhforindex(Phase phase, vhNode node)
  {
    if (phase == ePre) {
      // Cache for index's current dynamic value and apply
      // new constant propagated value.
      mSuccess = cacheDynVal(node);
      if (!mSuccess) {
        return eStop;
      }
    }
    return eNormal;
  }

  Status vhconstant(Phase phase, vhNode node)
  {
    if ((phase == ePre) && vhIsSubProgInterfaceConstant(node)) {
      // func/proc args are treated as constants. Check if their
      // actuals evaluate to constant and if they do, use them as
      // dynamic values
      vhExpr initVal;
      if (vhIsDeferred(node)) {
	initVal = vhGetDeferredConstantValue(node);
      }
      else {
	initVal = vhGetInitialValue(node);
      }
      if (initVal != NULL) {
        vhExpr dynVal = mPopulator->evaluateJaguarExpr(initVal, true, true);
        if (dynVal != NULL) {
          vhExpr nodeExpr = static_cast<vhExpr>(node);
          save(nodeExpr);
          vhStoreDynamicValue(nodeExpr, dynVal);
        }
	else {
	  // Check if the actual connected to this formal port is actually a constant
	  if (vhGetAttr(node, "IS_NOT_CONSTANT") == NULL) {
	    // Get the signal, variable or constant declaration
	    DeclarationFinder declFind;
	    {
	      JaguarBrowser jb(declFind);
	      jb.browse(initVal);
	    }
	    vhNode decl = declFind.getDeclaration();
	    VhObjType type = vhGetObjType(decl);
	    // If the decl is a signal or a variable, mark the node as being not 
	    // constant even though it is a VHCONSTANT
	    if (((type == VHSIGNAL) || (type == VHVARIABLE)) && 
		(vhGetAttr(node, "IS_NOT_CONSTANT") == NULL)) {
	      vhNode declLit = vhwCreateDecLit("1", vhGetExpressionType(initVal));
	      vhSetAttr(node, declLit, "IS_NOT_CONSTANT");
	    }
	  }
	}
      }
    }
    return eNormal;
  }

  bool getSuccess() { return mSuccess; }

private:

  bool cacheDynVal(vhNode node)
  {
    // Support for propagation of constants for record nets from function
    // arguments into functions is missing. It could be supported in future
    // for new record processing.
    if (!mIsNewRecord && mPopulator->isRecordNet(node, mContext)) {
      return true;
    }

    NUNet* net = 0;
    // Lookup net corresponding to identifier (variable).
    Populate::ErrorCode err_code = 
      mPopulator->lookupNet(mContext->getDeclarationScope(), node,
                            mContext, &net, true);
    if (err_code != Populate::eSuccess) {
      return false; // lookupNet prints the error.
    }
    if (net != NULL)
    {
      const SourceLocator loc = mPopulator->locator(node);
      NUIdentRvalue* ident_expr = new NUIdentRvalue(net, loc);
      // Fetch constant value for net from constant propgation.
      NUExpr* const_expr = mContext->stmtFactory()->optimizeExpr(ident_expr);
      if (const_expr != NULL)
      {
        NUConst* const_val = dynamic_cast<NUConst*>(const_expr);
        if (const_val && !const_val->hasXZ())
        {
          // Create a Jaguar node for constant.
          vhExpr const_vh_expr = 
            static_cast<vhExpr>(createConstLit(node, const_val, net));
          if (const_vh_expr != NULL)
          {
            vhExpr expr = static_cast<vhExpr>(node);
            // Save the original dynamic value before setting a new one.
            save(expr);
            // Set the created constant as the new dynamic value.
            vhStoreDynamicValue(expr, const_vh_expr);
          }
        }
        delete const_expr;
      }
      else {
        delete ident_expr;
      }
    } // if
    return true;
  }

  // Save current dynamic value for net.
  void save(vhExpr expr)
  {
    if (mCache.find(expr) == mCache.end()) {
      vhNode oldDynVal = vhGetDynamicValue(expr);
      mCache[expr] = static_cast<vhExpr>(oldDynVal);
    }
  }

  // Restore saved dynamic values for all nets.
  void restoreAll()
  {
    for (UtMap<vhExpr, vhExpr>::iterator itr = mCache.begin();
         itr != mCache.end(); ++itr) {
      vhStoreDynamicValue(itr->first, itr->second);
    }
  }

  // Create a jaguar node from the constant expression.
  vhNode createConstLit(vhNode node, NUConst* constExpr, NUNet* net)
  {
    vhNode const_lit = 0;
    // Get the declaration type if it is decimal.
    vhNode decl_type = getIntegerDeclType(node);
    if (decl_type != NULL)
    {
      UtString const_str;
      if (getDecLitStr(constExpr, net, &const_str)) {
        const_lit = createDecLit(const_str, node, decl_type);
      }
    }
    return const_lit;
  }

  // Create a decimal constant literal.
  vhNode createDecLit(UtString& const_str, vhNode node, vhNode decl_type)
  {
    // Set the scope to create literal in.
    setLitScope(node);
    return vhwCreateDecLit(const_cast<char*>(const_str.c_str()), decl_type);
  }

  // Get the declaration type for given node.
  vhNode getIntegerDeclType(vhNode node)
  {
    vhObjType obj_type = vhGetObjType(node);
    vhNode int_decl_type = 0;
    if (obj_type == VHFORINDEX) {
      vhNode range = vhGetDisRange(static_cast<vhExpr>(node));
      int_decl_type = vhGetExpressionType(static_cast<vhExpr>(range));
    } else {
      int_decl_type = vhGetType(node);
    }
    if (mPopulator->isIntegerType(int_decl_type)) {
      return int_decl_type;
    }
    return 0;
  }

  // Create a decimal literal with constant value in the string.
  bool getDecLitStr(NUConst* constExpr, NUNet* net, UtString* const_str)
  {
    // Get the self-determined size of constant. This is non-contextual size.
    UInt32 const_size = constExpr->determineBitSize();
    if (const_size <= 64)
    {
      // Construct a string from constant value, based in signedness of
      // the net being replaced.
      if ( (net->isIntegerSubrange() && net->isSignedSubrange()) ||
           (!net->isIntegerSubrange() && net->isSigned()) )
      {
        SInt64 val;
        if (!constExpr->getLL(&val)) {
          return false;
        }
        (*const_str) << val;
      }
      else
      {
        UInt64 val;
        if (!constExpr->getULL(&val)) {
          return false;
        }
        (*const_str) << val;
      }
    }
    return true;
  }

  // Set the scope of new jaguar literal to be created.
  // This sets the source locator of newly created node.
  void setLitScope(vhNode node)
  {
    vhNode scope = vhGetScope(node);
    JaguarString fileName(vhGetSourceFileName(scope));
    vhwSetCreateScope(scope, fileName.get_nonconst());
  }

  VhdlPopulate* mPopulator;
  LFContext* mContext;
  bool mIsNewRecord;
  UtMap<vhExpr,vhExpr> mCache;
  bool mSuccess;
};

// Utility functions on the top of jaguar object:

// createActualObjSelNameFromAliasObjSelName():
// This function takes a selected name whose parent object is alias
// and then resolves that alias object. After resolving alias, it creates
// a selected name with the resolved object. Example:
// signal REC_OBJ : my_rec; -- my_rec is a record type
// alias REC_ALIAS   is REC_OBJ;
// alias REC_ALIAS_SELNAME1: std_logic_vector(0 to 7)   is REC_ALIAS.S1;
// It will take REC_ALIAS.S1 as input and create REC_OBJ.S1 .
//
// TBD: enhance this function for nested records and indexed name actuals
// 
vhNode 
VhdlPopulate::createActualObjSelNameFromAliasObjSelName(vhNode alias_selname)
{
  vhNode actualSelName = NULL;
  if (VHSELECTEDNAME != vhGetObjType(alias_selname))
  {
    return actualSelName;
  }
  vhNode actual_obj = vhGetActualObj(alias_selname);
  if (vhGetObjType(actual_obj) == VHOBJECT)
    actual_obj = vhGetActualObj(actual_obj);
  bool actualIsAlias = isAliasObject(actual_obj);
  if (not actualIsAlias) // The given input is not alias, so return the org node
    return alias_selname;
  // Resolve this alias record object iteratively. 
  while (actualIsAlias)
  {
    actual_obj = vhGetInitialValue(actual_obj);
    if (vhGetObjType(actual_obj) == VHOBJECT)
      actual_obj = vhGetActualObj(actual_obj);
    actualIsAlias = VhdlPopulate::isAliasObject(actual_obj);
  }
  JaguarList exprList(vhGetExprList(static_cast<vhExpr>(alias_selname)));
  // We will discard the first element of the exprList because the
  // first element is the record object . We need the selected field
  // of this record which is the 2nd element of this exprList . 
  vhNode recordEle = vhGetNextItem(exprList);
  recordEle = vhGetNextItem(exprList); 
  // The length of this list must be 2 since nested record is not
  // supported.
  if ( NULL != vhGetNextItem(exprList) )
  {
    // TBD: nested record support
    SourceLocator loc = locator(alias_selname);
    UtString buf;
    mMsgContext->FailedToResolveAilasSelName(&loc,
                                             decompileJaguarNode(alias_selname, &buf));
    return NULL;
  }
  // We need create selected name with the actual object so that
  // we can do operation like create expression, lookup net etc. 
  // with that.
  vhNode scope = vhGetScope(actual_obj);
  JaguarString fileName(vhGetSourceFileName(scope));
  vhwSetCreateScope(scope, fileName.get_nonconst());
  JaguarList selList(vhwCreateNewList( VH_TRUE ));
  vhwAppendToList( selList, actual_obj );
  vhwAppendToList( selList, recordEle );
  actualSelName = vhwCreateSelectedName( selList, actual_obj,
                     vhGetExpressionType( static_cast<vhExpr>(alias_selname )));
  return actualSelName;
}

static vhExpr sFindConstant(vhNode vh_named) {
  bool cont = true;
  vhExpr k = NULL;
  while (cont) {
    cont = false;
    if ((vhGetObjType(vh_named) == VHCONSTANT) && vhIsDeferred(vh_named)) {
      k = vhGetDeferredConstantValue(vh_named);
    } else {
      k = vhGetInitialValue(vh_named);
    }
    vh_named = NULL;
    if (k != NULL) {
      if (vhGetObjType(k) == VHOBJECT) {
        vh_named = vhGetActualObj(k);
        cont = vhGetObjType(vh_named) == VHCONSTANT;
      }
    }
  }
  return k;
}

// This is called for a constant rvalue node. Get the node's initial
// value and create an expression for it.
Populate::ErrorCode
VhdlPopulate::initialValueExpr(vhNode vh_named, LFContext* context,
                               NUExpr** the_expr, bool propagateRvalue,
                               bool reportError)
{
  *the_expr = 0;
  SourceLocator loc = locator(vh_named);
  // Get the constant initial value
  vhExpr initialValue = sFindConstant(vh_named);
  
  if (initialValue == NULL)
    return eNotApplicable;

  // Create an expression for initial value.
  return expr(initialValue, context, 0, the_expr, propagateRvalue, reportError);
}

Populate::ErrorCode
VhdlPopulate::identRvalue(vhNode vh_named,
                          LFContext *context,
                          NUExpr **the_expr,
                          bool propagateRvalue,
                          bool reportError)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(vh_named);

  NUNet* net = 0;
  ErrorCode temp_err_code = lookupNet( context->getDeclarationScope(), vh_named,
                                       context, &net, reportError );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  if (vhGetObjType(vh_named) == VHCONSTANT)
  {
    // See if this variable has an initial value, and if it does, see
    // if the value is a constant
    vhExpr initialValue = sFindConstant(vh_named);
    if (initialValue != NULL)
    {
      // Attempt folding the initial value if it is a constant, so that we
      // may get a constant literal if we're lucky.
      initialValue = evaluateJaguarExpr(initialValue, false, true);
      vhObjType type = vhGetObjType(initialValue);
      bool createExpr = false;
      if (!createExpr) {
        // See if this constant has an initial value, and if it does,
        // create an expression if the value is a literal constant.
        switch (type) {
        case VHCHARLIT:
        case VHDECLIT:
        case VHBASELIT:
        case VHIDENUMLIT:
        case VHSTRING:
        case VHBITSTRING:
        case VHATTRBNAME: 
          createExpr = true;
          break;
        default:
          break;
        }
      }
      // If createExpr is set, we have a constant literal. Make an expression
      // for it, instead of creating an identifier.
      if (createExpr)
        return expr(initialValue, context, net->getBitSize(), the_expr, propagateRvalue);
    } // if
  } // if

  *the_expr = createIdentRvalue( net, loc );
  // Optimize the new identrvalue if net has a propagated constant.
  NUExpr* opt_expr = context->stmtFactory()->optimizeExpr(*the_expr);
  if (opt_expr != NULL) {
    *the_expr = opt_expr;
  }
  // we know our signedness already, VHDL gets it wrong for integer subranges
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::edgeExpr(NUExpr *clkExpr,
                       ClockEdge edge,
                       LFContext *context,
                       NUEdgeExpr **the_edge_expr)
{
  ErrorCode err_code = eSuccess;
  // The following block of code has been borrowed from Verilog edge
  // population routine(s).
  SourceLocator loc = clkExpr->getLoc();
  NUIdentRvalue* rval = dynamic_cast<NUIdentRvalue*>(clkExpr);
  if (clkExpr->determineBitSize() != 1)
  {
    NUVectorNet* vn = NULL;
    const ConstantRange* range = NULL;
    //We will be putting the expression text into a message one way or another
    UtString buf; 
    clkExpr->compose(&buf, NULL);
    // Right now all I know how to prune is part-selects and raw vector-nets.
    if (rval == NULL)
    {
      NUVarselRvalue* ps = dynamic_cast<NUVarselRvalue*>(clkExpr);
      NU_ASSERT(ps->isConstIndex (), clkExpr);
      if (ps != NULL)
      {
        range = ps->getRange();
        vn = dynamic_cast<NUVectorNet*>(ps->getIdent());
      }
    }
    else
    {
      vn = dynamic_cast<NUVectorNet*>(rval->getIdent());
      if (vn != NULL)
        range = vn->getRange();
    }

    if (vn == NULL)
    {
      POPULATE_FAILURE(context->getMsgContext()->LFUnsupEdge(&loc, buf.c_str()), &err_code);
      return err_code;
    }

    SInt32 idx = range->getLsb ();
    delete clkExpr;        // range is used above, don't delete before use
    clkExpr = new NUVarselRvalue(vn, ConstantRange (idx, idx), loc);
    context->getMsgContext()->LFWideEdge(&loc, buf.c_str());
    rval = NULL;
  }
  NUNet* clockNet = NULL;
  if (rval == NULL)
  {
    // I hereby declare that edge expressions have a useful size of 1.
    // Resizing these up front makes the hashing of clkExprs much cleaner.
    clkExpr->resize(1);

    // See if we have seen this expression before in an edge condition
    clockNet = context->getEdgeNet(clkExpr);
    if (clockNet == NULL)
    {
      // must create temp.  Name it for the vector bit
      NUModule* mod = context->getModule();
      UtString buf(NetClkTempPrefix);
      clkExpr->compose(&buf, NULL, true);   // include root in name, we need an
                                            // unescaped name here
      // Ensure a unique name relative to the other synthesized clock scalars
      buf << "_" << (1 + context->numEdgeNets());
      StringAtom* name = context->vhdlIntern(buf.c_str());
      clockNet = new NUBitNet(name, NetFlags(eDMWireNet | eAllocatedNet | eTempNet),
                              mod, loc);
      mod->addLocal(clockNet);
      NUIdentLvalue *temp_lvalue = new NUIdentLvalue(clockNet, loc);
      NUContAssign* assign = context->stmtFactory()->createContAssign(clockNet->getName(),
                                                                      temp_lvalue, clkExpr, loc);
      mod->addContAssign(assign);
      clkExpr = assign->getRvalue(); // The clkExpr is owned by NUContAssign.
      context->putEdgeNet(clkExpr, clockNet);
    }
    else
      delete clkExpr;
    rval = new NUIdentRvalue(clockNet, loc);
  } // if
  // Here ends the block of code taken from Verilog edge population 
  *the_edge_expr = new NUEdgeExpr(edge, rval, loc);
  return err_code;
}

// This static function takes an unsigned 32-bit integer as an argument
// and returns true if and only if the argument is a non-zero power of
// two.  That is, it returns true if exactly one bit is set in the
// argument.
static bool
sIsPowerOf2( UInt32 v )
{
  if ( v == 0 ) return false;
  v &= v - 1;
  if ( v == 0 ) return true;
  return false;
}

static bool sRequiresOpsSameSize(NUOp::OpT op) {
  switch (op) {
    // The list below was taken from NUBinaryOp::determineBitSize.  These are
    // the ones that size to max(op1,op2)
   
  case NUOp::eBiPlus:
  case NUOp::eBiMinus:
  case NUOp::eBiSDiv:
  case NUOp::eBiUDiv:
  case NUOp::eBiSMod:
  case NUOp::eBiUMod:
  case NUOp::eBiVhdlMod:
  case NUOp::eBiBitAnd:
  case NUOp::eBiBitOr:
  case NUOp::eBiBitXor:
  case NUOp::eBiDownTo:
    /*  Exp should be on here but it causes problems:
        case NUOp::eBiExp:

        test/vhdl/lang_miscops/pow6.vhd crashes with a seg-fault if
        you uncomment this.
    */
    return true;
  default:
    break;
  } // switch
  
  return false;
} // static bool sRequiresOpsSameSize

static bool sIsDirty(NUExpr* expr) {
  NUOp* op = dynamic_cast<NUOp*>(expr);
  if (op != NULL) {
    switch (op->getOp()) {
    case NUOp::eUnMinus:
    case NUOp::eBiMinus:
    case NUOp::eBiSMult:
    case NUOp::eBiUMult:
    case NUOp::eBiPlus:
/* -- these were affected by having eBiPlus in here previously
test/vhdl/lang_rotate
test/cust/matrox/sunex/carbon/cwtb/dmatop
test/vhdl/beacon7
test/bugs/test.run.group3100
test/cust/matrox/phoenix/carbon/cwtb/rttop
test/vhdl/lang_miscops

 */
    case NUOp::eBiVhLshift:
    case NUOp::eBiVhLshiftArith:
    case NUOp::eBiLshift:
      return true;
    default:
      break;
    }
  }
  return false;
}

// Make sure we zero- or sign-extend from a particular bit position without modifying
// the size of the subexpression we compute.  This is used to protect a computation from
// later resizing by verilog rules.
//
NUExpr* VhdlPopulate::buildMask(NUExpr* expr, UInt32 sz, bool signExtend)
{
  const SourceLocator& loc = expr->getLoc();

  NUExpr* sizeExpr = NUConst::create(false, sz, 32, loc);
  NUOp::OpT op = signExtend ? NUOp::eBiVhExt : NUOp::eBiVhZxt;

  // Consider
  //    (POSVAR ZXT 20) EXT 32 
  // the "expr" is presently marked as isSignedResult() - but that leads to problems
  // because it implies sign extension when resizing the value from 20 bits. 
  //
  if (NUBinaryOp* bexpr = dynamic_cast<NUBinaryOp*>(expr))
    if (bexpr->getOp () == NUOp::eBiVhZxt)
      expr->setSignedResult (false);

  expr = new NUBinaryOp(op, expr, sizeExpr, loc);
  expr->resize(sz);
  expr->setSignedResult(signExtend);
  return expr;
} // NUExpr* buildMask

bool VhdlPopulate::isConstrainedIntegerType(vhNode node)
{
  if (node && isIntegerType(node)) {
    vhNode constraint = vhGetConstraint(node);
    if (constraint && (VHRANGECONSTRAINT == vhGetObjType(constraint))) {
      return true;
    }
  }
  return false;
}

// This function will return true if the type mark of absolute basetype of
// this node is some sort of integer
//
bool VhdlPopulate::isIntegerType(vhNode node)
{
  if ( node == NULL )
  {
    return false;
  }

  vhObjType type = vhGetObjType(node);
  switch (type) {
  case VHSUBTYPEIND:
    return isIntegerType (vhGetExpressionType (static_cast<vhExpr>(node)));

  case VHSUBTYPEDECL:
  case VHTYPEDECL:
  {
    if ( type == VHTYPEDECL && vhGetObjType( vhGetTypeDef( node )) == VHINTTYPE )
    {
      return true;
    }
    if ( type == VHSUBTYPEDECL &&
         vhGetObjType( vhGetSubTypeBaseType( node )) == VHINTTYPE )
    {
      return true;
    }

    vhNode basetype = vhGetAbsoluteBaseType(node);
    JaguarString type_mark(vhGetName(basetype));
    vhNode n_package = vhGetMasterScope(basetype);
    if (!n_package || vhGetObjType(n_package) != VHPACKAGEDECL)
    {
      return false;
    }
    JaguarString package_name(vhGetPackName(n_package));
    JaguarString library_name(vhGetLibName(n_package));
    if (((strcasecmp(type_mark,"INTEGER") == 0) ||
         (strcasecmp(type_mark,"UNIVERSAL_INTEGER") == 0)) &&
        (strcasecmp(package_name, "STANDARD" ) == 0) &&
        (strcasecmp( library_name, "STD" ) == 0))
    {
      return true;
    }
    break;
  }
  case VHTYPECONV:
  case VHQEXPR:
    return isIntegerType( vhGetType( node ));
    break;
  case VHINTTYPE:
    return true;
    break;
  default:
    break;
  }
  return false;
} //  bool isIntegerType

static bool sUnsignedOrNatural (NUExpr* e)
{
  if (e->isSignedResult ())
    if (NUConst* c = e->castConst ())
      return not c->isNegative ();
    else
      return false;             // acts like signed value
  else
    return true;
}

Populate::ErrorCode
VhdlPopulate::chooseBinOp (vhExpr vh_expr, NUExpr* lop, NUExpr* rop, NUOp::OpT* opcode, bool* invertExpr)
{
  ErrorCode err_code = eSuccess;
  // Arithmetic expressions want to know if they are signed or unsigned.
  // Jaguar will tell us that based on the operands supplied.  The type
  // of the resulting arithmetic expression is exactly what we need.
  //
  bool isArithSigned = (getVhExprSignType (vh_expr) == eVHSIGNED);

  // Jaguar stupidly (or consistently) tells us that relationals are 'boolean'
  // which is absolutely useless in determining whether to do signed or
  // unsigned comparisons.  Instead depend on the fact that VHDL packages
  // seem to consistently do signed compares if EITHER operand is a signed
  // number.
  // 
  bool lsign = not sUnsignedOrNatural (lop);
  bool rsign = not sUnsignedOrNatural (rop);

  bool isRelOpSigned = (lsign || rsign);
  bool invert = false;
  NUOp::OpT op = NUOp::eInvalid;

  vhOpType op_type = vhGetOpType(vh_expr);
  switch (op_type) {
  case VH_OR_OP: op = NUOp::eBiBitOr; break;
  case VH_AND_OP: op = NUOp::eBiBitAnd; break;
  case VH_XOR_OP: op = NUOp::eBiBitXor; break;
  case VH_EQ_OP: op = NUOp::eBiEq; ;break;
  case VH_GEQ_OP: op = isRelOpSigned ? NUOp::eBiSGtre : NUOp::eBiUGtre; break;
  case VH_LEQ_OP: op = isRelOpSigned ? NUOp::eBiSLte : NUOp::eBiULte; break;
  case VH_GTH_OP: op = isRelOpSigned ? NUOp::eBiSGtr : NUOp::eBiUGtr; break;
  case VH_LTH_OP: op = isRelOpSigned ? NUOp::eBiSLt : NUOp::eBiULt; break;
  case VH_PLUS_OP: op = NUOp::eBiPlus; break;
  case VH_MINUS_OP: op = NUOp::eBiMinus; break;
  case VH_NEQ_OP: op = NUOp::eBiNeq; break;
  case VH_NAND_OP: op = NUOp::eBiBitAnd; invert = true; break;
  case VH_NOR_OP: op = NUOp::eBiBitOr; invert = true; break;
  case VH_XNOR_OP: op = NUOp::eBiBitXor; invert = true; break;
  case VH_SLL_OP: op = NUOp::eBiVhLshift; break; 
  case VH_SRL_OP: op = NUOp::eBiVhRshift; break; 
  case VH_CONCAT_OP: break; // Creating NUConcatOp
  case VH_ROR_OP: op = NUOp::eBiRoR; break;
  case VH_ROL_OP: op = NUOp::eBiRoL; break;
  case VH_MULT_OP: op = isArithSigned ? NUOp::eBiSMult : NUOp::eBiUMult; break;
  case VH_DIV_OP: op = isArithSigned ? NUOp::eBiSDiv : NUOp::eBiUDiv; break;
  case VH_REM_OP: op = isArithSigned ? NUOp::eBiSMod : NUOp::eBiUMod; break;
  case VH_EXPONENT_OP: op = NUOp::eBiExp; break;
  case VH_MOD_OP: op = NUOp::eBiVhdlMod; break;
  case VH_SLA_OP: op = NUOp::eBiVhLshiftArith; break;
  case VH_SRA_OP: op = NUOp::eBiVhRshiftArith; break;
  default:
  {
    SourceLocator loc = locator (vh_expr);
    POPULATE_FAILURE(mMsgContext->JagInvalidType(&loc,
                                                 gGetVhNodeDescription(vh_expr),
                                                 "binary expression"), &err_code);
    return err_code;
  }
  break;
  }

  *opcode = op;
  *invertExpr = invert;
  return err_code;
}

// Are two memories equivalently dimensioned?
bool sMemoryEquivDimension (const NUMemoryNet &m1, const NUMemoryNet &m2)
{
  UInt32 numDims = m1.getNumDims ();
  if (numDims != m2.getNumDims ())
    return false;

  for(UInt32 i=0; i<numDims; ++i)
    if (m1.getRangeSize (i) != m2.getRangeSize (i))
      return false;

  return true;
}

// Are we operating on an object within the scope of either
//      use ieee.std_logic_signed;
//      use ieee.numeric_signed;
// If so, we need to treat std_logic_vector's as if they were signed
//
bool
sIsDeclaredInSignedPackage (vhExpr vh_expr)
{
  // Now adjust the sign if necessary, depending on the package
  vhNode vh_master = vhGetMaster( vh_expr );
  if ( vh_master != NULL )
  {
    vhNode vh_scope = vhGetScope( vh_master );
    if ( vhGetObjType( vh_scope ) == VHPACKAGEDECL )
    {
      const char *packName = vhGetPackName( vh_scope );
      if ( !strcasecmp( vhGetLibName( vh_scope ), "IEEE" ))
      {
        if ( !strcasecmp( packName, "STD_LOGIC_SIGNED" ) ||
             !strcasecmp( packName, "NUMERIC_SIGNED" ))
        {
          return true;
        }
      }
    }
  }
  return false;
}

// Determine if an operator has been overloaded or not
bool sIsOperatorOverloaded(vhNode vh_expr)
{
  vhNode master = vhGetMaster( vh_expr );

  if (master != NULL)
  {
    if (vhIsOverloadedOp(master))
      return true;
  }

  return false;
}

NUExpr* sBuildCompositeConcatExpr(NUExprVector& expr_vector, 
				  SourceLocator& loc,
                                  bool isNewRecord,
				  bool exprIsSigned)
{
  bool are_args_composites = false;
  if (isNewRecord)
  {
    for (UInt32 i = 0; i < expr_vector.size(); ++i)
    {
      // This is a concat of composite types like records/aggregates/functions returning
      // records or composite concat itself.
      bool isBadMix = false;
      if ( (dynamic_cast<NUCompositeInterfaceExpr*>(expr_vector[i]) != NULL) ||
           (expr_vector[i]->getType() == NUExpr::eNUCompositeExpr) ) {
        if ((i > 0) && !are_args_composites) {
          isBadMix = true;
        }
        are_args_composites = true;
      } else if (are_args_composites) {
        isBadMix = true;
      }
      if (isBadMix) {
        // This means the concat arguments are a mix of composites and non-composites.
        // Unless there's a bug somewhere, we shouldn't get here.
        NU_ASSERT("Cannot mix composites and non-composites in a concat.", expr_vector[i]);
      }
    }
  }

  NUExpr* ret_expr = NULL;
  if (are_args_composites) {
    NUCompositeExpr* new_expr = new NUCompositeExpr(expr_vector, loc);
    // Concat operator is always used to assign to arrays on lvalue.
    new_expr->setIsCompositeArray(true);
    ret_expr = new_expr;
  } else {
    UInt64 repeat_count = 1;
    ret_expr = new NUConcatOp(expr_vector, repeat_count, loc);
    ret_expr->setSignedResult( exprIsSigned );
  }

  return ret_expr;
}

Populate::ErrorCode
VhdlPopulate::createConditionalOperand(vhExpr vh_expr, 
				       bool sizeVaries, 
				       NUExpr* orig_expr,
				       NUExpr** the_expr, 
				       LFContext* context,
				       const SourceLocator& loc)
{
  ErrorCode err_code = Populate::eSuccess;
  ErrorCode tmp_err_code = Populate::eSuccess;

  if (sizeVaries && vhGetObjType(vh_expr) == VHSLICENAME) {
    vhNode range = vhGetDisRange(vh_expr);

    vhExpr lhs = vhGetLtOfRange(range);
    NUExpr* lhsExpr = NULL;
    tmp_err_code = expr(lhs, context, 0, &lhsExpr, true /*propagateRvalue*/, true /*reportError*/);
    if (errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code)) {
      return err_code;
    }

    vhExpr rhs = vhGetRtOfRange(range);
    NUExpr* rhsExpr = NULL;
    tmp_err_code = expr(rhs, context, 0, &rhsExpr, true /*propagateRvalue*/, true /*reportError*/);
    if (errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code)) {
      return err_code;
    }

    NUExpr* firstTerm = NULL;

    if (VH_TO == vhGetRangeDir(range)) {
      firstTerm = new NUBinaryOp(NUOp::eBiMinus, rhsExpr, lhsExpr, loc);
    }
    else {
      firstTerm = new NUBinaryOp(NUOp::eBiMinus, lhsExpr, rhsExpr, loc);
    }

    NUExpr* secondTerm = NUConst::create(false /*resultsign*/, 1 /*result*/, 1 /*width*/, loc);

    *the_expr = new NUBinaryOp(NUOp::eBiPlus, firstTerm, secondTerm, loc);
  }
  else {
    *the_expr = NUConst::create(false /*resultsign*/, orig_expr->getBitSize(), 32 /*width*/, loc);
  }
  
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::binaryExpr(vhExpr vh_expr,
                         LFContext *context,
                         int lWidth,
                         NUExpr **the_expr,
                         bool propagateRvalue,
                         bool reportError)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code1, temp_err_code2;
  *the_expr = 0;

  SourceLocator loc = locator(vh_expr);

  NUOp::OpT op = NUOp::eInvalid;
  bool invert = false;          // parsing NAND, NOR, XNOR

  vhExpr vh_left = vhGetLeftOperand(vh_expr);
  vhExpr vh_right = vhGetRightOperand(vh_expr);
  vhOpType op_type = vhGetOpType(vh_expr);
  NUExpr *expr1 = NULL, *expr2 = NULL;
  bool expr1SizeVaries = false, expr2SizeVaries = false;

  // We have to special-case record equality and inequality
  if ( !mCarbonContext->isNewRecord() &&
       ( op_type == VH_EQ_OP || op_type == VH_NEQ_OP ) && 
       isRecordNet( vh_left, context ) && isRecordNet( vh_right, context ))
  {
    op = ( op_type == VH_EQ_OP ) ? NUOp::eBiEq : NUOp::eBiNeq;
    err_code = recordCompare( op, vh_left, vh_right, context, loc, the_expr );
    return err_code;
  }

  if (( op_type == VH_GEQ_OP || op_type == VH_LEQ_OP || op_type == VH_GTH_OP ||
        op_type == VH_LTH_OP || op_type == VH_EQ_OP  || op_type == VH_NEQ_OP ) &&
      lWidth == 1 )
  {
    // If we're processing a relational op, and we think the size is 1,
    // it's because the output of a relop is a boolean, which is one
    // bit.  Set the context width to 0 so that we don't try to use the
    // width==1 as a valid value.
    lWidth = 0;
  }

  temp_err_code1 = expr(vh_left, context, 0, &expr1, propagateRvalue, reportError);

  if ( temp_err_code1 == eNotApplicable ) {
    // expr1 failed to create, this might be ok,
    // we check for acceptable condition ~70 lines below.
  } else if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code1, &err_code ) &&
             ( vhGetObjType( vh_left ) != VHATTRBNAME ) ) {
    return err_code;
  }

  // Examine the RHS of a VHDL division op and see if it's a nonzero
  // static power of 2.  We don't really care about the synthesis
  // limitations but we don't have full support for these operators in
  // BitVector so we can only handle 64 bits and less.
  if ((lWidth > 64) &&
      ( op_type == VH_DIV_OP || op_type == VH_REM_OP || op_type == VH_MOD_OP ))
  {
    bool validRHS = false;
    vhExpr rhsExprVal = evaluateJaguarExpr( vh_right, true );
    if (rhsExprVal)
    {
      double value = 3.14159; // bogus value that's not an integer
      switch( vhGetObjType( rhsExprVal ))
      {
      case VHDECLIT:
      {
        value = vhGetDecLitValue( rhsExprVal );
        UInt32 intval = (UInt32)value;
        if ( intval == value && sIsPowerOf2( intval ))
          validRHS = true;
        break;
      }
      case VHBASELIT:
      {
        value = vhGetBaseLitValue( rhsExprVal );
        UInt32 intval = (UInt32)value;
        if ( intval == value && sIsPowerOf2( intval ))
          validRHS = true;
        break;
      }
      default:
        // If the RHS is not a number (it could be bits, an aggregate,
        // an enum, a signal/variable, etc.), let it by.
        validRHS = true;
        break;
      }
    }

    if ( validRHS == false )
    {
      if (reportError) {
        if ( op_type == VH_DIV_OP )
          mMsgContext->CGNotPowerOfTwo( &loc, "Right", " \"/\" " );
        else if ( op_type == VH_REM_OP )
          mMsgContext->CGNotPowerOfTwo( &loc, "Right", " \"REM\" " );
        if ( op_type == VH_MOD_OP )
          mMsgContext->CGNotPowerOfTwo( &loc, "Right", " \"MOD\" " );
      }
      return err_code;
    }
  } // if

  temp_err_code2 = expr( vh_right, context, 0, &expr2, propagateRvalue, reportError );

  if ( temp_err_code2 == eNotApplicable ) {
    // expr2 failed to create, this might be ok,
    // we check for acceptable condition ~18 lines below.
  } else if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code2, &err_code ) &&
             ( vhGetObjType( vh_right ) != VHATTRBNAME ) ) {
    return err_code;
  }

  bool exprIsSigned = ( eVHSIGNED == getVhExprSignType(vh_expr));

  // Warning 
  // The handling of err_code is convoluted below. A path exists where
  // err_code is used but it looks like that that path is only active if
  // tmp_err_code1 and tmp_err_code2 are eSuccess (and thus err_code
  // is also eSuccess).
  // If tmp_err_code1 or tmp_err_code2 is not eSuccess then neither is err_code.
  // To simplify the code we force err_code to be eSuccess here.
  err_code = eSuccess;

  NUExpr *new_expr = 0;
  if (temp_err_code2 != eSuccess || temp_err_code1 != eSuccess)
  {
    vhExpr vh_event = (0 == expr1) ? vh_left : vh_right;
    int dummy_edge = -1;
    // We will accept the failure of creation of expr1 or expr2 if one
    // of them is 'EVENT or 'STABLE, or if one of them is a clock, as
    // defined by Jaguar.  Does the new handling of clocks here totally
    // supplant the old attribute-based handling?
    if (vhGetObjType(vh_event) == VHATTRBNAME)
    {
      VhAttrbType att_type = vhGetAttrbType(vh_event);
      switch(att_type)
      {
      case VH_EVENT:
      case VH_STABLE:
        // OK, these attributes are used to designate a CLOCK.
        break;
      default:
      {
        // Delete allocated operand expressions, print error if reportError
        // is true and return error.
        delete expr1;
        delete expr2;
        if (reportError) {
          mMsgContext->UnsupportedAttribute(&loc, vhGetAttribName(vh_event));
        }
        return eFailPopulate;
      }
      }
    }
    else if ( vhDetectClock( vh_event, &dummy_edge ) != NULL )
    {
      // Nothing to do here except avoid the error clause!
    }
    else
    {
      // Delete allocated operand expressions, print error if reportError
      // is true and return error.
      delete expr1;
      delete expr2;
      if (reportError) {
        mMsgContext->JagInvalidType(&loc, 
                                    gGetVhNodeDescription((vhNode) vh_event),
                                    "sub-expression");
      }
      return eFailPopulate;
    }
    new_expr = expr1 ? expr1 : expr2;
    if ( 0 == new_expr )
      return eFailPopulate;      
  }
  else
  {
    switch (op_type)
    {
    case VH_CONCAT_OP:
    {
      NUExprVector expr_vector;
      if (expr1)
      {
        expr1->resize (expr1->determineBitSize ());
        expr_vector.push_back (expr1);
      }
      if (expr2)
      {
        expr2->resize (expr2->determineBitSize ());
        expr_vector.push_back (expr2);
      }
      *the_expr = sBuildCompositeConcatExpr( expr_vector, 
					     loc, 
					     mCarbonContext->isNewRecord(), 
					     exprIsSigned );
      return err_code;
    }
    break;
    case VH_MULT_OP:
    {
      UInt32 msize;
      bool leftint = isIntegerType(vhGetExpressionType(vh_left));
      bool rightint = isIntegerType(vhGetExpressionType(vh_right));
      UInt32 leftsize = expr1->determineBitSize();
      UInt32 rightsize = expr2->determineBitSize();
      // calculate the result width
      if ( leftint && rightint )
      {
        // The result of (int * int) is sizeof(int).  Our ints may not
        // be 32 bits, so the same size as (vec*vec) will be big enough.
        // But, we still need to limit ourselves to 32 bits max as
        // that's the max size of a VHDL integer in cbuild.
        msize = std::min( leftsize + rightsize, 32u );
      }
      else if ( leftint && !rightint )
      {
        // The width of (int * vector) is vectorlength * 2
        msize = rightsize * 2;
      }
      else if ( !leftint && rightint )
      {
        // The width of (vector * int) is vectorlength * 2
        msize = leftsize * 2;
      }
      else
      {
        // The result size of (vec * vec) is sizeof(lop) + sizeof(rop).
        msize = leftsize + rightsize;
      }

      // Now adjust the sign if necessary, depending on the package
      if (sIsDeclaredInSignedPackage (vh_expr)) {
        expr1->setSignedResult( true );
        expr2->setSignedResult( true );
      }

      // Perform the resizing of integer arguments that happens
      // explicitly in VHDL
      if ( leftint && !rightint )
      {
        expr1 = buildMask(expr1, rightsize, expr1->isSignedResult( ));
        expr1->setSignedResult( expr2->isSignedResult( ));
      }
      if ( !leftint && rightint )
      {
        expr2 = buildMask(expr2, leftsize, expr2->isSignedResult( ));
        expr2->setSignedResult( expr1->isSignedResult( ));
      }

      // Extend the operands to the correct size
      if (leftsize != msize)
        expr1 = buildMask(expr1, msize, expr1->isSignedResult());
      if (rightsize != msize)
        expr2 = buildMask(expr2, msize, expr2->isSignedResult());

      if (eSuccess != chooseBinOp (vh_expr, expr1, expr2, &op, &invert))
      {
        delete expr1;
        delete expr2;
        return eFailPopulate;
      }
      new_expr = new NUBinaryOp(op, expr1, expr2, loc);
      break;    
    }

    case VH_EQ_OP:
    case VH_NEQ_OP:
    {
      vhNode leftType = vhGetExpressionType(vh_left);
      if (vhGetObjType(leftType) == VHSUBTYPEDECL)
	leftType = vhGetSubTypeBaseType(leftType);

      vhNode rightType = vhGetExpressionType(vh_right);
      if (vhGetObjType(rightType) == VHSUBTYPEDECL)
	rightType = vhGetSubTypeBaseType(rightType);
      
      JaguarString leftTypeName(vhGetTypeName(leftType));
      JaguarString rightTypeName(vhGetTypeName(rightType));
      
      UInt32 expr1Size = expr1->getBitSize();
      UInt32 expr2Size = expr2->getBitSize();

      expr1SizeVaries = expr1->sizeVaries();
      expr2SizeVaries = expr2->sizeVaries();

      // The operands of a relational equal or not equal comparison
      // will get replaced with constants if the operands are of the
      // same base type and their sizes are not equal, unless an
      // overloaded operator has been provided. The assumption here
      // is that the overloaded operator deals with operands of different
      // sizes, like the IEEE overloaded operators do. 
      // See bug 9000 for more details on this change.

      // Bug 12143 revealed that expressions of different sizes can still match
      // in size, but cbuild cannot figure this out statically because the sizes
      // are determined at run time.

      // Bug 15910 revealed that integer expressions of different sizes are
      // legitimate operands in EQ and NEQ relational expressions. The requirement
      // that both sides of the expression be the same size in order to 
      // be equal pertains to arrays (std_logic_vector, bit_vector, etc). Not
      // integers or integer subtypes. So if both sides of the '=' and '\='
      // relational expression are integers (or integer subtypes), then don't
      // force a false evaluation, even if they are of different sizes.
      bool bothIntegers = isIntegerType(vhGetExpressionType(vh_left)) && isIntegerType(vhGetExpressionType(vh_right));

      if (!sIsOperatorOverloaded(vh_expr) 
	  && (strcmp(leftTypeName, rightTypeName) == 0) 
	  && (expr1Size != expr2Size)
          && (!bothIntegers)
	  && (!expr1SizeVaries && !expr2SizeVaries))
      {
	delete expr1;
	delete expr2;
	expr1 = NUConst::create(false /*resultsign*/, 1 /*result*/, 1 /*width*/, loc);
	expr2 = NUConst::create(false /*resultsign*/, 0 /*result*/, 1 /*width*/, loc);
      }
    }
    // fall-thru

    default:
      if (sIsDeclaredInSignedPackage (vh_expr)) {
        bool lsign = true, rsign = true;
        if (op_type == VH_PLUS_OP || op_type == VH_MINUS_OP) {
          // Operands of +/- operators in std_logic_signed package are signed except for
          // +(STD_LOGIC, STD_LOGIC_VECTOR)   +(STD_LOGIC_VECTOR,STD_LOGIC)
          // -(STD_LOGIC, STD_LOGIC_VECTOR)   -(STD_LOGIC_VECTOR,STD_LOGIC)
          // where we zero-extend the bit operator
          //
          lsign = expr1->determineBitSize () != 1;
          rsign = expr2->determineBitSize () != 1;
        }
        
        expr1->setSignedResult (lsign);
        expr2->setSignedResult (rsign);
      }
      

      if (eSuccess != chooseBinOp (vh_expr, expr1, expr2, &op, &invert))
      {
        delete expr1;
        delete expr2;
        return eFailPopulate;
      }

      // For binary operators that meet the following conditions, insert
      // masks to avoid getting into trouble with verilog sizing rules.
      // See bug 4027.
      //
      // The conditions:
      //   1. This operation requires size(operand1)==size(operand2)
      //   2. size(operand1) != size(operand2)
      //   3. smaller operand can leave behind dirty bits (e.g. + - << *, unary -)
      //
      // We will test condition 3 first because it's faster than testing condition 2
      if (sRequiresOpsSameSize(op)) {
        if ( op == NUOp::eBiPlus || op == NUOp::eBiMinus ) {
          // Handle VHDL's labyrinthine sizing rules for addition and
          // subtraction in the full detail required to keep the Verilog
          // sizing code's hands off of this.
          SInt32 size;
          ErrorCode temp_err_code = getVhExprWidth ( vh_expr, &size, loc,
                                                     context, false, true );

          // Later, getVhExprWidth function should return eNotApplicable, when it fails to 
          // find the width of an expression. The code below should be updated to handle that situation.
          if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code )) {
            if (temp_err_code == eFatal)
              return temp_err_code;

            size = lWidth;  // No static width for the expression, use the lWidth 
          }

          const SInt32 sz1 = expr1->determineBitSize();
          if ( size != sz1 ) {
            expr1 = buildMask( expr1, size, expr1->isSignedResult());
            expr1->setSignedResult (exprIsSigned);
          }
          const SInt32 sz2 = expr2->determineBitSize();
          if ( size != sz2 ) {
            expr2 = buildMask( expr2, size, expr2->isSignedResult());
            expr2->setSignedResult (exprIsSigned);
          }
        }
        else
        {
          bool dirty1 = sIsDirty(expr1);
          bool dirty2 = sIsDirty(expr2);
          if (dirty1 || dirty2) {
            UInt32 sz1 = expr1->determineBitSize();
            UInt32 sz2 = expr2->determineBitSize();
            if ((sz1 < sz2) && dirty1) {
              expr1 = buildMask(expr1, sz1, expr1->isSignedResult ());
              expr1->setSignedResult (exprIsSigned);
            }
            else if ((sz2 < sz1) && dirty2) {
              expr2 = buildMask(expr2, sz2, expr2->isSignedResult ());
              expr2->setSignedResult (exprIsSigned);
            }
          }
        }

        // See std_logic_arith.vhd.  When mixing vectors with integers,
        // the size of the vector wins, not the size of the max.
        // Also (Ashenden, pp247-8

        bool integer1 = isIntegerType(vhGetExpressionType(vh_left));
        bool integer2 = isIntegerType(vhGetExpressionType(vh_right));
        if (integer1 != integer2) {
          if (integer1) {
            expr1 = buildMask(expr1, expr2->determineBitSize(), expr1->isSignedResult ());
            expr1->setSignedResult (exprIsSigned);
          }
          else {
            expr2 = buildMask(expr2, expr1->determineBitSize(), expr2->isSignedResult ());
            expr2->setSignedResult (exprIsSigned);
          }
        }
      } else if (VH_RELATIONAL == vhGetOpCat (vh_expr)) {
        // When mixing vectors with integers the vector sizes are preferred, but
        // if the vector is unsigned, we must WIDEN the vector and integer by one bit
        //
        // If we do widen, then we must widen the RESULT, not the actual computation
        // (bug4027):  ( unsigned(fourbit_a) - unsigned(fourbit_b) - 8) >= 8
        // wants to be computed as 4 bits, zero extended to 5 bits and then compared
        // to a 5 bit 8.
        //
        bool integer1 = isIntegerType(vhGetExpressionType(vh_left));
        bool integer2 = isIntegerType(vhGetExpressionType(vh_right));

        if (integer1 != integer2) {
          if (integer1) {
            UInt32 prefLen = expr2->determineBitSize ();
            bool otherOk = false;
            if (eVHSIGNED != getVhExprSignType (vh_right))
                ++prefLen;
            else
              otherOk = true;
            
            expr1 = buildMask(expr1, prefLen, expr1->isSignedResult ());
            if (!otherOk) {
              expr2 = buildMask (expr2, expr2->determineBitSize (), expr2->isSignedResult ());
            }
          }
          else {
            UInt32 prefLen = expr1->determineBitSize ();
            bool otherOk = false;
            if (eVHSIGNED != getVhExprSignType (vh_left))
              ++prefLen;
            else
              otherOk = true;
            expr2 = buildMask(expr2, prefLen, expr2->isSignedResult ());
            if (!otherOk)
              expr1 = buildMask (expr1, expr1->determineBitSize (), expr1->isSignedResult ());
          }
        }
      }

      // Check to see if the operands vary in size and we don't have an overloaded
      // operator that may deal with size differences.
      if (!sIsOperatorOverloaded(vh_expr) && (expr1SizeVaries || expr2SizeVaries)) {

	// As per bug 12143, when the sizes of the operands in a relational operator vary because
	// we have a variable size select, we need to check if they match in size. 
	// If they do, then the relation can proceed normal. 
	// If they don't, then the result is always false for the EQUAL (=) or true for
	// NOT EQUAL (\=).
	// What is built here is a ternary operator which incorporates the original binary operator:
	// result = (condition) ? (relation) : 0 or 1 (depending on the operator)
	// where condition is ( (op1_RHS - op1_LHS + 1) == (op2_RHS - op2_LHS + 1) )
	//       relation  is ( expr(op1_LHS to op1_RHS) == expr(op2_LHS to op2_RHS) )
	// if the range is DOWNTO instead of TO, then the expression is ( op1_LHS - op1_RHS + 1)
	// This ensure that if during runtime the operand sizes do not match we produce the 
	// correct output.
	NUExpr* trueExpr = new NUBinaryOp(op, expr1, expr2, loc);
	trueExpr->setSignedResult( exprIsSigned );
	NUExpr* cleanTrueExpr = NULL;
	err_code = cleanupXZBinaryExpr(trueExpr, lWidth, loc, &cleanTrueExpr);
	if (invert && cleanTrueExpr) {
	  cleanTrueExpr = Populate::invert(cleanTrueExpr);
	}

	NUExpr* falseExpr = NULL;
	  
	if (op_type == VH_EQ_OP) {
	  falseExpr = NUConst::create(false /*resultsign*/, 0 /*result*/, 1 /*width*/, loc);
	}
	else {
	  falseExpr = NUConst::create(false /*resultsign*/, 1 /*result*/, 1 /*width*/, loc);
	}

	NUExpr* condLexpr = NULL;
	createConditionalOperand(vh_left, expr1SizeVaries, expr1, &condLexpr, context, loc);

	NUExpr* condRexpr = NULL;
	createConditionalOperand(vh_right, expr2SizeVaries, expr2, &condRexpr, context, loc);

	NUExpr* condExpr = new NUBinaryOp(NUOp::eBiEq, condLexpr, condRexpr, loc);

	*the_expr = new NUTernaryOp(NUOp::eTeCond, condExpr, cleanTrueExpr, falseExpr, loc);

	return err_code;
      }
      else {
	new_expr = new NUBinaryOp(op, expr1, expr2, loc);
      }
      break;
    }
  }
  new_expr->setSignedResult( exprIsSigned );
  err_code = cleanupXZBinaryExpr(new_expr, lWidth, loc, the_expr);

  if (invert && *the_expr) {
    *the_expr = Populate::invert (*the_expr);
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::cleanupXZBinaryExpr(NUExpr *in_expr,
                                  int lWidth,
                                  const SourceLocator &loc,
                                  NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = in_expr;

  NUBinaryOp *binary_expr = dynamic_cast<NUBinaryOp*>(in_expr);
  if (not binary_expr) {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Binary expression expected"), &err_code);
    return err_code;
  }

  NUOp::OpT op = binary_expr->getOp();

  NUExpr *expr1 = binary_expr->getArg(0);
  NUExpr *expr2 = binary_expr->getArg(1);

  // analyze the expressions. If either is constant and contains
  // unknowns then this binary op must be resynthesized.
  bool unknown1 = expr1->castConstXZ () != NULL;
  bool unknown2 = expr2->castConstXZ () != NULL;

  // For non-comparison operators, we leave the expression alone.
  // Except for mod and divide, in that case change to an x if the
  // second arg is an x.
  switch (op) {
  case NUOp::eBiPlus:
  case NUOp::eBiMinus:
  case NUOp::eBiSMult:
  case NUOp::eBiUMult:
  case NUOp::eBiLshift:
  case NUOp::eBiRshift:
  case NUOp::eBiVhLshift:
  case NUOp::eBiVhRshift:
  case NUOp::eBiVhLshiftArith:
  case NUOp::eBiVhRshiftArith:
  case NUOp::eBiBitOr:
  case NUOp::eBiBitAnd:
  case NUOp::eBiBitXor:
  case NUOp::eBiRoR:
  case NUOp::eBiRoL:
    return eSuccess;
    break;

   case NUOp::eBiSDiv:
  case NUOp::eBiSMod:
  case NUOp::eBiUDiv:
  case NUOp::eBiUMod:
    if (unknown2) {
      break;
    } else {
      return eSuccess;
    }
    break;

  default:
    // fallthrough
    break;
  }

  if (unknown1 || unknown2) {
    // oh swell. We get to resynth

    // we need the maximum size of the expression to properly
    // size the constants

    // size the expressions
    expr1->resize(expr1->determineBitSize());
    expr2->resize(expr2->determineBitSize());

    UInt32 maxSize = std::max(expr1->getBitSize(), expr2->getBitSize());
    // check against the lwidth
    maxSize = std::max(maxSize, static_cast<UInt32>(lWidth));

    UtString constValStr;
    NUConst* constVal = NULL;
    char polarity = 'm';
    UInt64 intVal = 0;

    // Next, if both are constants then take the Rvalue's
    // polarity. The polarity is used for value-creating binary ops.
    if (unknown1 && expr1->drivesZ())
      polarity = 'z';
    else
      polarity = 'x';
    
    if (unknown2 && expr2->drivesZ())
      polarity = 'z';
    else
      polarity = 'x';
    
    if (polarity == 'm') {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unknown constant"), &err_code);
      return err_code;
    }

    switch (op)
    {
    case NUOp::eBiUDiv:
    case NUOp::eBiUMod:
    case NUOp::eBiSDiv:
    case NUOp::eBiSMod:
      constValStr.append(maxSize, polarity);
      constVal = NUConst::createXZ(constValStr, false, maxSize, loc);
      break;

      // These are always false if either expression has unknowns
    case NUOp::eBiEq:
    case NUOp::eBiNeq:
    case NUOp::eBiLogOr:
    case NUOp::eBiLogAnd:
    case NUOp::eBiSLt:
    case NUOp::eBiSLte:
    case NUOp::eBiSGtr:
    case NUOp::eBiSGtre:
    case NUOp::eBiULt:
    case NUOp::eBiULte:
    case NUOp::eBiUGtr:
    case NUOp::eBiUGtre:
      constVal = NUConst::create(false, 0ULL, 32, loc);
      break;
      
      // === and !===, result in false and true, respectively, except
      // if both operands are constants, in which case we let this
      // fall through (not setting constVal)
    case NUOp::eBiTrieq:
    case NUOp::eBiTrineq:
      if (! (unknown1 && unknown2))
      {
        intVal = 0;
        if (op == NUOp::eBiTrineq)
          intVal = 1;
        constVal = NUConst::create(false, intVal, 32, loc);
      }
      break;
      
    default:
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unsupported binary expression"), &err_code);
      return err_code;
      break;
    }
    
    if (constVal != NULL) {
      delete *the_expr;
      *the_expr = constVal;
    }
  }
  
  return err_code;
}


// Function to process unary expressions. 
// TBD: need to set expression sign correctly
Populate::ErrorCode
VhdlPopulate::unaryExpr(vhExpr vh_expr,
                        LFContext *context,
                        int lWidth,
                        NUUnaryOp **the_expr,
                        bool propagateRvalue,
                        bool reportError)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(vh_expr);

  NUOp::OpT op = NUOp::eInvalid;

  switch (vhGetOpType(vh_expr)) {
  case VH_UPLUS_OP: op  = NUOp::eUnPlus; break;
  case VH_UMINUS_OP: op = NUOp::eUnMinus; break;
  case VH_NOT_OP: op   = NUOp::eUnVhdlNot; break;
  case VH_ABS_OP: op   = NUOp::eUnAbs; break;
  default:
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unsupported unary expression"), &err_code);
    return err_code;
    break;
  }

  NUExpr *expr1 = 0;
  ErrorCode temp_err_code = expr(vhGetOperand(vh_expr), context, lWidth,
                                 &expr1, propagateRvalue, reportError);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  if (sIsDeclaredInSignedPackage (vh_expr))
    // Is this from ieee.std_logic_signed?  If so, we want to 
    // treat the expression as if it was a signed value.
    expr1->setSignedResult (true);

  *the_expr = new NUUnaryOp(op, expr1, loc);

  if (eVHSIGNED == getVhExprSignType(vh_expr))
    (*the_expr)->setSignedResult(true);
  else
    (*the_expr)->setSignedResult(false);

  return err_code;
}


// This function detects if an operator is a user-defined overloaded
// operator.  Any operator defined in a library other than IEEE/STD will
// be treated as user-defined overloaded operator.
bool VhdlPopulate::isOperatorOverloadedByUser(vhExpr vh_expr)
{
  bool retval = false;
  vhNode master = vhGetMaster( vh_expr );

  if ( master == NULL )
  {
    vhNode vh_overload = vhOpenOverloadedExpr( vh_expr ); // must be closed later
    if ( vh_overload != NULL )
    {
      // We really do want the master of vh_expr here, not vh_overload.
      master = vhGetMaster( vh_expr );
      vhCloseFeature(); // close the opened overloaded expression

    }
  }
  if ( master != NULL )
  {
    vhNode scope = vhGetScope( master );
    if ( scope != NULL )
    {
      JaguarString libname(vhGetLibName( scope ));
      // If the operator is not defined in STD/IEEE then it is user
      // defined.
      if ( strncasecmp( "STD", libname, 4 ) != 0 &&
           strncasecmp( "IEEE", libname, 5 ) != 0 )
        retval = true;
    }
  }

  return retval;
}

Populate::ErrorCode
VhdlPopulate::expr(vhExpr jag_expr,
                   LFContext *context,
                   int lWidth,
                   NUExpr **the_expr,
                   bool propagateRvalue,
                   bool reportError)
{
  *the_expr = 0;
  vhExpr vh_expr = NULL;
  vhObjType expr_type = vhGetObjType( jag_expr );
  if ( expr_type == VHCONSTANT ||
       ( expr_type == VHOBJECT && vhGetObjType( vhGetActualObj( jag_expr )) == VHCONSTANT ))
  {
    // Don't evaluate a multidim constant; it'll cause codegen errors as
    // evaluating it will result in an aggregate, which will get
    // smooshed into a constant.  We really do want to use the net
    // created for the constant here.
    int width, numDims;
    const SourceLocator loc = locator( jag_expr );
    ErrorCode err_code = getVhExprBitSize( jag_expr, &width, &numDims, loc,
                                           context, false, true );
    if ( err_code == eSuccess && numDims > 1 )
    {
      if ( expr_type == VHOBJECT )
      {
        vh_expr = static_cast<vhExpr>( vhGetActualObj( jag_expr ));
      }
      else
      {
        vh_expr = jag_expr;
      }
    }
  }
  if ( vh_expr == NULL )
  {
    vh_expr = evaluateJaguarExpr(jag_expr, false, true);
  }

  expr_type = vhGetObjType(vh_expr);
  ErrorCode ret = eFailPopulate;
  switch (expr_type) {
  case VHCHARLIT:
  case VHDECLIT:
  case VHBASELIT:
  case VHIDENUMLIT:
  case VHSTRING:
  case VHBITSTRING:
    ret = constant(vh_expr, lWidth, context, the_expr);
    break;
  case VHOBJECT:
  case VHEXTERNAL:
  {
    vhNode vh_actual_expr = vhGetActualObj(vh_expr);
    ret = expr(static_cast<vhExpr>(vh_actual_expr), context, lWidth, the_expr,
               propagateRvalue, reportError);
    break;
  }
  case VHFORINDEX:
  {
    vhExpr currentVal = vhGetInitialValue(vh_expr);
    if (currentVal) {
      ret = expr(currentVal, context, lWidth, the_expr, propagateRvalue,
                 reportError);
    } else { // Unelaborated FOR LOOP index
      ret = identRvalue(vh_expr, context, the_expr, propagateRvalue, reportError);
    }
    break;
  }
  case VHSIGNAL:
  case VHVARIABLE:
  case VHFILEDECL:
    ret = identRvalue(vh_expr, context, the_expr, propagateRvalue, reportError);
    // When propagating rvalue, a non-constant shouldn't result in an error.
    // Caller will take not applicable as a non-constant and continue to use
    // other means to find the constant value.
    // Testcase: test/vhdl/beacon11/misc/SUB3.vhdl
    if (propagateRvalue && (ret != eSuccess)) {
      ret = eNotApplicable;
    }
    break;
  case VHCONSTANT:
  {
    vhExpr evald_expr = evaluateJaguarExpr(vh_expr, true);
    if (evald_expr)
      ret = expr(evald_expr, context, lWidth, the_expr, propagateRvalue,
                 reportError);
    else
    {
      vhExpr initialValue;
      if ( vhIsDeferred( vh_expr ) ) 
      {
	initialValue = vhGetDeferredConstantValue( vh_expr );
      }
      else 
      {
	initialValue = vhGetInitialValue( vh_expr );
      }
      // Workaround for the Jaguar bug recorded in bug4325
      if ( vhIsGeneric( vh_expr ) && initialValue != NULL )
      {
        ret = expr( initialValue, context, lWidth, the_expr, propagateRvalue,
                    reportError );
      }
      // If propagateRvalue is set, create an expression for initializer instead
      // of creating a constant NUIdentRvalue. This allows user to construct
      // an expression with literals, and possibly fold it down to a constant.
      else if (propagateRvalue)
      {
        ret = initialValueExpr(vh_expr, context, the_expr, propagateRvalue,
                               reportError);
      }
      else
      {
        ret = identRvalue(vh_expr, context, the_expr, propagateRvalue,
                          reportError);
      }
    }
    break;
  }
  case VHBINARY:
    if (isOperatorOverloadedByUser(vh_expr))
    {
      NUExprArray expr_array;
      ret = functionCall(vh_expr, context, lWidth, &expr_array);
      *the_expr = expr_array[0];
    }
    else
    {
      ret = binaryExpr(vh_expr, context, lWidth, the_expr, propagateRvalue,
                       reportError);
    }
    break;
  case VHUNARY:
    if (isOperatorOverloadedByUser(vh_expr))
    {
      NUExprArray expr_array;
      ret = functionCall(vh_expr, context, lWidth, &expr_array);
      *the_expr = expr_array[0];
    }
    else
    {
      ret = unaryExpr(vh_expr, context, lWidth, (NUUnaryOp**)the_expr,
                      propagateRvalue, reportError);
    }
    break;
  case VHINDNAME: 
    ret = indexedNameRvalue(vh_expr, context, lWidth, the_expr);
    break;
  case VHSLICENAME:
    ret = sliceNameRvalue(vh_expr, context, lWidth, the_expr);
    break;
  case VHSLICENODE:
  case VHFUNCNODE:
  case VHASSOCIATION:
  {
    vhExpr actualObj = vhGetActual( vh_expr );
    ret = expr( actualObj, context, lWidth, the_expr, propagateRvalue, reportError );
    break;
  }
  case VHSELECTEDNAME: 
  {
    ret = selectedNameExpr( vh_expr, context, the_expr, propagateRvalue, reportError );
    break;
  }
  case VHFUNCCALL:
  {
    NUExprArray expr_array;
    ret = functionCall(vh_expr, context, lWidth, &expr_array);
    if ( ret == eSuccess )
    {
      *the_expr = expr_array[0];
    }
    else if(expr_array[0] == NULL)
    {
      // functionCall failed to populate the expr.
      const SourceLocator loc = locator( vh_expr );
      LOC_ASSERT(0, loc);
    }
    break;
  }
  case VHTYPECONV:
  case VHQEXPR:
  {
    vhExpr actual_expr = vhGetOperand(vh_expr);

    ErrorCode err_code = eSuccess;
    
    ErrorCode temp_err_code = expr(actual_expr, context, lWidth,
                                   the_expr, propagateRvalue, reportError);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    // We need to set the sign of the expression depending on the prefix
    // Problem here:
    //  If we are converting a positive value to a larger signed integer
    //  type, we need to zero extend it.
    
    if (*the_expr)
    {
      (*the_expr)->setSignedResult( eVHSIGNED == getVhExprSignType( vh_expr ));
    }
    ret = err_code;
    break;
  }
  case VHAGGREGATE:
  {
    ret = aggregate(vh_expr, context, lWidth, the_expr);
    break;
  }
  case VHATTRBNAME: 
  {
    ret = attribute( vh_expr, context, the_expr );
    break;
  }

  case VHRANGE:                 // comes up in "with z select x <= y when 0 to 100, 2 when others;"
  {
    // VHRANGE object could be of attribute type or non-attribute i.e index
    // constraint range type. - bug6787
    if (vhIsRangeAttribute(vh_expr) == VH_TRUE)
    {
      vhExpr attrib_range = vhGetRangeAttrib(vh_expr);
      ret = expr(attrib_range, context, lWidth, the_expr, propagateRvalue,
                 reportError);
    }
    else
    {
      NUExpr* left, *right;

      vhExpr vh_left = vhGetLtOfRange (vh_expr);
      ret = expr (vh_left, context, lWidth, &left, propagateRvalue, reportError);
      if (eSuccess != ret)
        break;

      vhExpr vh_right = vhGetRtOfRange (vh_expr);
      ret = expr (vh_right, context, lWidth, &right, propagateRvalue, reportError);
      if (eSuccess != ret)
        break;

      if (vhGetRangeDir (vh_expr) == VH_TO)
        std::swap (left, right);

      *the_expr = new NUBinaryOp (NUOp::eBiDownTo, left, right, locator (vh_expr));
    }
    break;
  }
/*
 * does not work :(
 * case VHSLICENODE:
 *   return sliceNameRvalue(vhGetActual(vh_expr), context, module,
 *                          lWidth, the_expr);
 */
  default:
  {
    SourceLocator loc = locator(vh_expr);
    UtString buf("Unsupported expression: ");
    buf << gGetVhNodeDescription(jag_expr);
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, buf.c_str()), &ret);
    return ret;
    break;
  }
  } // switch

  return ret;
}


static NUExpr*
sGenBitOrPartsel (NUExpr* expr, NUExpr* index, const SourceLocator& loc)
{
  if (NUConst* c = index->castConst())
  {
    UInt32 bound;
    c->getUL (&bound);
    delete c;

    ConstantRange range (bound, bound);
    return new NUVarselRvalue (expr, range, loc);
  }
  else
    return new NUVarselRvalue (expr, index, loc);
}


// Method to resolve an alias object. For a given alias object and a
// given left and right of it, this function finds out the actual of
// this alias and translates this left and right bounds onto the
// actual. The translated left and right bounds are stored into the
// input references actualSliceLeft and actualSliceRight . If this
// actual itself turns out to an alias of another object, this funtion
// will call itself with this actual as the input vh_alias and with this
// translated actualSliceLeft and actualSliceRight as the alias left and
// right. In this way this function calls itself recursively till it
// finds the non-alias actual object. At this point it looks up for the
// NUNet corresponding this actual object.
// 
// For the scalar object aliases, call this function with content of
// aliasSliceLeft and aliasSliceRight as 0. This function will put the
// actual bit index if the actual is indexed name in the content of
// actualSliceLeft and actualSliceRight . 
Populate::ErrorCode
VhdlPopulate::resolveAlias( vhNode vh_alias,
                            VhdlRangeInfo* aliasSlice,
                            VhdlRangeInfo* actualSlice,
                            LFContext *context,
                            NUNet**  actualNet )
{
  ErrorCode err_code = eSuccess;
  ErrorCode tmp_err_code = eSuccess;
  if (VHOBJECT == vhGetObjType(vh_alias))
  {
    vh_alias = vhGetActualObj(vh_alias);
  }
  vhNode alias_range = NULL;
  vhNode actual_range = NULL;
  VhdlRangeInfo aliasBounds (0,0);
  // The alias object can be a selected name where the parent object is an alias.
  // In that case resolve this alias from here itsleft. See more comments below.
  if (VHSELECTEDNAME == vhGetObjType(vh_alias))
  {
    // Get the bounds of this selected name.
    tmp_err_code = getVhRangeNode(vh_alias, &alias_range, NULL);
    if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
    {
      return tmp_err_code;
    }
    if (alias_range)
    {

      tmp_err_code = getLeftAndRightBounds( alias_range, &aliasBounds, context);
      if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
      {
        return tmp_err_code;
      }
    }
    // Interesting case with alias selected name ... We do not need to 
    // translate aliasSlice because the translated values will be same as that of the
    // aliasSlice.msb and aliasSlice.lsb
    //
    // Example:
    // signal REC_OBJ : my_record_type; 
    // alias REC_ALIAS   is REC_OBJ;
    // For the REC_ALIAS.SIG1(0 to 5), the range (0 to 5) will be same in the
    // REC_OBJ.SIG1(0 to 5) . To get the actual NU net, we need to create the
    // selected name jaguar object REC_OBJ.SIG1 from the input REC_ALIAS.SIG1 .
    // This creation is required in order to do the net lookup.
    vhNode actualObjSelName = createActualObjSelNameFromAliasObjSelName(vh_alias);
    if (!actualObjSelName)
    {
      return eFailPopulate;
    }
    // For the selected name aliases, the alias's left right will be
    // same the actual's left and bound.
    *actualSlice = aliasBounds;

    // Now lookup for the net and return
    tmp_err_code = lookupNet( context->getDeclarationScope(), actualObjSelName,
                              context, actualNet );
    // We have got the actual left and right and the actual net. So, return
    // from here.
    return tmp_err_code;
  }
  else if (isAliasObject(vh_alias))
  {
    // Get the left and right bound of this alias
    tmp_err_code = getVhRangeNode(vh_alias, &alias_range, NULL);
    if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
    {
      return tmp_err_code;
    }
    if (alias_range)
    {
      tmp_err_code = getLeftAndRightBounds( alias_range, &aliasBounds, context);
      if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
      {
        return tmp_err_code;
      }
    }
  }
  else
  {
    SourceLocator loc = locator(vh_alias);
    POPULATE_FAILURE(mMsgContext->UnExpectedObjectType(&loc, gGetVhNodeDescription(vh_alias) ,
                                                       "as alias while resolving aliases." ), &err_code);
    return err_code;
  }

  // At this point we have calculated the left and right bounds of the 
  // input alias object. Now we need to calculate the corresponding left and
  // right bound of the actual of this alias.
  VhdlRangeInfo actualBounds (0,0);
  vhNode vh_actual = vhGetInitialValue(vh_alias);
  vhNode actual_obj = NULL;
  if (vhGetObjType(vh_actual) == VHOBJECT)
    vh_actual = vhGetActualObj(vh_actual);

  // We need to consider the case of scalar object aliases
  bool actualIsScalarObj = false;

  switch (vhGetObjType(vh_actual))
  {
    case VHINDNAME:
    {
      actualIsScalarObj = true;
      JaguarList indexList(vhGetExprList(static_cast<vhExpr>(vh_actual)));
      vhExpr index = static_cast<vhExpr>(vhGetNextItem(indexList));
      int inexdVal = 0;
      tmp_err_code = getVhExprValue(index, &inexdVal, NULL);
      if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
      {
        return tmp_err_code;
      }
      actualBounds.putVector (inexdVal, inexdVal);
      actual_obj = vhGetPrefix(static_cast<vhExpr>(vh_actual));
      break;
    }
    case VHSLICENAME:
    {
      tmp_err_code = getVhRangeNode(vh_actual, &actual_range, NULL);
      if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
      {
        return tmp_err_code;
      }
      if (actual_range)
      {
        tmp_err_code = getLeftAndRightBounds( actual_range, &actualBounds, context );
        if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
        {
          return tmp_err_code;
        }
      }
      actual_obj = vhGetPrefix(static_cast<vhExpr>(vh_actual));
      break;
    }
    case VHSELECTEDNAME:
    case VHVARIABLE:
    case VHSIGNAL:
    case VHCONSTANT:
    {
      tmp_err_code = getVhRangeNode(vh_actual, &actual_range, NULL);
      if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
      {
        return tmp_err_code;
      }
      if (actual_range)
      {
        tmp_err_code = getLeftAndRightBounds( actual_range, &actualBounds, context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( tmp_err_code, &err_code ) )
        {
          return tmp_err_code;
        }
      }
      else
      {
        actualIsScalarObj = true;
        actualBounds.putVector (0,0);
      }
      actual_obj = vh_actual;
      break;
    }
    default: {
      SourceLocator loc = locator(vh_actual);
      POPULATE_FAILURE(mMsgContext->UnExpectedObjectType(&loc, gGetVhNodeDescription(vh_actual) ,
                                                         "as actual while resolving aliases." ), &err_code);
      return err_code;
      break;
    }
  }

  // Now we have got alias's left/right and its actual's left/right. Now
  // for a given left/right bound of the alias, we need to calculate the
  // translated left/right of the actual.
  if (not actualIsScalarObj)
  {
    vhDirType actual_dir = vhGetRangeDir(actual_range);
    vhDirType alias_dir = vhGetRangeDir(alias_range);
    SourceLocator loc = locator(vh_alias);
    if (alias_dir == actual_dir)
    {
      int offset = actualBounds.getMsb (loc) - aliasBounds.getMsb (loc);
      actualSlice->putVector (aliasSlice->getMsb (loc) + offset,
                              aliasSlice->getLsb (loc) + offset);
    }
    else
    {
      if (VH_TO == alias_dir)
      {
        SInt32 alias_left_offset = aliasSlice->getMsb(loc) - aliasBounds.getMsb (loc);
        SInt32 newLeft = actualBounds.getMsb (loc) - alias_left_offset;
        actualSlice->putVector( newLeft,
                                newLeft - ( aliasSlice->getLsb (loc) - aliasSlice->getMsb(loc) ));
     }
      else
      {
        SInt32 alias_left_offset = aliasBounds.getMsb (loc) - aliasSlice->getMsb(loc);
        SInt32 newLeft = actualBounds.getMsb (loc) + alias_left_offset;

        actualSlice->putVector( newLeft,
                                newLeft + 
                                 ( aliasSlice->getMsb(loc) - aliasSlice->getLsb (loc)));
      }
    }
  }
  else 
  {
    *actualSlice = actualBounds;
  }
  // Now check, if this actual itselft is alias, if so resolve it till
  // we ge the non-alias object
  if (VHOBJECT == vhGetObjType(actual_obj))
    actual_obj = vhGetActualObj(actual_obj);

  if ( VHSELECTEDNAME != vhGetObjType(actual_obj) &&
       isAliasObject(actual_obj) ) 
  { 
    VhdlRangeInfo act_bounds (0,0);
    tmp_err_code = resolveAlias( actual_obj, actualSlice,
                                 &act_bounds, context, actualNet );
    if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
    {
      return tmp_err_code;
    }
    *actualSlice = act_bounds;
  }
  else 
  {
    if (VHSELECTEDNAME == vhGetObjType(actual_obj))
    {
      // Interesting case with alias selected name ... We do not need to 
      // translate the aliasSliceLeft and aliasSliceRight because the translated
      // values will be same as that of the aliasSliceLeft and aliasSliceRight.
      // Example:
      // signal REC_OBJ : my_record_type; 
      // alias REC_ALIAS   is REC_OBJ;
      // For the REC_ALIAS.SIG1(0 to 5), the range (0 to 5) will be same in the
      // REC_OBJ.SIG1(0 to 5) . To get the actual net, we need to create the
      // selected name jaguar object REC_OBJ.SIG1 from the input REC_ALIAS.SIG1
      // This creation is required in order to do the net lookup.
      actual_obj = createActualObjSelNameFromAliasObjSelName(actual_obj);
      if (!actual_obj)
      {
        return eFailPopulate;
      }
    }
    tmp_err_code = lookupNet( context->getDeclarationScope(), actual_obj, context,
                              actualNet );
    if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) )
    {
      return tmp_err_code;
    }
  }
  return err_code;
}

// Method to map bitselect index on an alias obj to actual obj's index
// Example:
// variable TEMP : std_logic_vector ( 6 downto 0);
// alias aTEMP : std_logic_vector ( 16 downto 10) is TEMP;
//    aTEMP(i) := ....;
//
// Here, the indexed-object aTEMP(i) is actually TEMP(i - 10)
// For a given index expression of the alias object, this function creates
// the translated index expression of the actual object. To create the
// translated index epression, this function takes the MSB and LSB of the 
// alias object and the translated same of the actual object.
//  
Populate::ErrorCode 
VhdlPopulate::resolveIndexAliasBitSel(NUExpr** bitsel_expr,
                                      vhNode   vh_bitsel,
                                      VhdlRangeInfo& aliasBounds,
                                      VhdlRangeInfo& actualBounds,
                                      const SourceLocator& loc)
{
  ErrorCode err_code = eSuccess;
  vhDirType actual_dir = actualBounds.getMsb (loc) > actualBounds.getLsb (loc) ? VH_DOWNTO : VH_TO;
  vhDirType alias_dir = aliasBounds.getMsb (loc) > aliasBounds.getLsb (loc) ? VH_DOWNTO : VH_TO;
  if (alias_dir == actual_dir)
  {
    // alias OUTAL1 : std_logic_vector(1 downto 0) is OUTPUT(3 downto 2);
    // OUTAL1(1) ==> OUTPUT(3) , OUTAL1(0) ==> OUTPUT(2)
    // offset = actualMsb - aliasMsb;
    // generalised: OUTAL1(i) = OUTPUT(i + offset)
    //
    NUExpr* offsetExpr = NUConst::create( true, actualBounds.getMsb (loc) - aliasBounds.getMsb (loc),
                                          32, loc);
    *bitsel_expr = new NUBinaryOp(NUOp::eBiPlus, *bitsel_expr,
                                  offsetExpr, loc);
  }
  else
  {
    // Calculation of the equivalent index of the actual net:
    //
    // Case: Alias range direction is 'TO' and actual range 'DOWNTO'
    // alias OUTAL1 : std_logic_vector(0 to 1) is OUTPUT(3 downto 2);
    // OUTAL1(0) ==> OUTPUT(3), OUTAL1(1) ==> OUTPUT(2)
    // Another example:
    // alias OUTAL1 : std_logic_vector(5 to 6) is OUTPUT(3 downto 2);
    // OUTAL1(5) ==> OUTPUT(3), OUTAL1(6) ==> OUTPUT(2)
    // generalised: OUTAL1(i) = OUTPUT( actualMsb - ( i - aliasMsb ) )
    //                        = OUTPUT( actualMsb + aliasMsb - i) )
    //
    // Case: Alias range direction is 'DOWNTO' and actual range 'TO'
    // alias OUTAL1 : std_logic_vector(2 downto 1) is OUTPUT(3 to 4);
    // OUTAL1(2) ==> OUTPUT(3), OUTAL1(1) ==> OUTPUT(4)
    // Another example:
    // alias OUTAL1 : std_logic_vector(8 downto 7) is OUTPUT(1 to 4);
    // OUTAL1(8) ==> OUTPUT(1), OUTAL1(7) ==> OUTPUT(2) ...
    // generalised: OUTAL1(i) = OUTPUT( actualMsb + ( aliasMsb - i) )
    //                        = OUTPUT( actualMsb + aliasMsb - i) )
    //
    // from the above it is found the the generalied index expression is
    // same, no matter what the range direction of the actual and the 
    // alias is.
    //
    NUExpr* sumOfMsbExpr 
      = NUConst::create( true,
                         actualBounds.getMsb(loc) + aliasBounds.getMsb (loc),
                         32, loc);
    *bitsel_expr = new NUBinaryOp( NUOp::eBiMinus, sumOfMsbExpr,
                                   *bitsel_expr, loc);
  }
  if (NULL == *bitsel_expr)
  {
    UtString buf;
    POPULATE_FAILURE(mMsgContext->FailedToResolveAliasIndex(&loc, decompileJaguarNode(vh_bitsel, &buf)), &err_code);
    return err_code;
  }
  UInt32 size = (*bitsel_expr)->determineBitSize();
  (*bitsel_expr)->resize(size);
  return err_code;
}


void
VhdlPopulate::maybeAdjustIndex( vhNode vh_index, NUExpr **bitsel, 
                                const SourceLocator &loc )
{
  vhNode vh_index_type = vhGetExpressionType( static_cast<vhExpr>( vh_index ));
  vh_index_type = vhGetAbsoluteBaseType( vh_index_type );
  if ( !strcmp( vhGetName( vh_index_type ), "STD_ULOGIC" ))
  {
    mMsgContext->StdLogicAsArrayIndex( &loc );
    // Here we are using an object with an absolute base type of
    // STD_ULOGIC as an index in an expression.  This expression can
    // only have the values [01] in cbuild.  However, in VHDL, this
    // expression can be [UX01ZWLH-].  So, if our index expression will
    // come back either as 0 or 1, we need to or 0x2 to it to make it
    // refer to the proper index in an array using a std_[u]logic as an
    // index.
    NU_ASSERT( (*bitsel)->determineBitSize() == 1, *bitsel ); // sanity check
    NUConst *offset;
    // Create our constant 2, 2 bits wide.
    offset = NUConst::create( false, 2, 2, loc );
    // Create a bitwise or of the original expression and the 2
    *bitsel = new NUBinaryOp( NUOp::eBiBitOr, *bitsel, offset, loc );
    (*bitsel)->resize( (*bitsel)->determineBitSize( ));
  }
}


Populate::ErrorCode
VhdlPopulate::indexedNameRvalue(vhExpr vh_expr,
                                LFContext *context,
                                int lWidth,
                                NUExpr **the_expr)
{
  *the_expr = 0;
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator( vh_expr );

  JaguarList indexList( vhGetExprList( vh_expr ));
  NUExprVector indexVector;
  vhExpr vh_index;
  NUExpr *index = NULL;
  while (( vh_index = static_cast<vhExpr>( vhGetNextItem( indexList ))))
  {
    temp_err_code = expr( vh_index, context, 0, &index );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    index->resize( index->determineBitSize( ));
    maybeAdjustIndex( vh_index, &index, loc );
    indexVector.push_back( index );
  }
  const UInt32 numIndices = indexVector.size();

  NUNet *net = NULL;
  vhExpr vh_named = vhGetPrefix( vh_expr );
  vhObjType expr_type = vhGetObjType(vh_named);
  if ( expr_type == VHOBJECT ) {
    vh_named = static_cast<vhExpr>(vhGetActualObj(vh_named));
    expr_type = vhGetObjType( vh_named );
  }

  switch ( expr_type )
  {
  case VHINDNAME:
  {
    NUExpr *the_memsel = NULL;
    temp_err_code = indexedNameRvalue( vh_named, context, lWidth,
                                       &the_memsel );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    if ( the_memsel->getType() == NUExpr::eNUCompositeSelExpr )
    {
      // If this is a record index selector, add the indices to the
      // selector and return.  No need to traverse the range normalizing
      // code.
      NUCompositeSelExpr *csel = dynamic_cast<NUCompositeSelExpr*>( the_memsel );
      const UInt32 sz = indexVector.size();
      for ( UInt32 i = 0; i < sz; ++i )
        csel->addIndexExpr( indexVector[i] );
      *the_expr = csel;
      return temp_err_code;
    }

    NUMemselRvalue *memsel_rval = dynamic_cast<NUMemselRvalue*>( the_memsel );
    if ( memsel_rval == NULL )
    {
      mMsgContext->JaguarConsistency( &loc, "Unsupported rvalue memory expression." );
      return eFatal;
    }
    NUMemoryNet *mem = memsel_rval->getIdent()->getMemoryNet();
    NU_ASSERT( mem, memsel_rval );

    // Here, we need to know what memory range we are grabbing, whether
    // this index refers to a word or to a submemory.  If it refers to a
    // submemory, the index needs to be jammed into the memsel.  If it
    // refers to a word, we need to generate a varsel using the code
    // below here.
    const UInt32 numMemDims = mem->getNumDims();
    const UInt32 numSelDims = memsel_rval->getNumDims();
    UInt32 remainingDims = numMemDims - numSelDims;
    
    // Go through all the index expressions.  If there is more than
    // one dimension remaining in the overall memory, create another
    // memsel.  If it's the last one, create a bitselect into the
    // word.
    for (NUExprVector::iterator iter = indexVector.begin(); iter != indexVector.end(); ++iter) {
      NU_ASSERT(remainingDims != 0, memsel_rval);
      NUExpr* indexExpr = *iter;
      if (remainingDims == 1) {
        // normalize indexVector[numIndices-1] for the memory's operational width range
        NUExpr *normalized_expr = mem->normalizeSelectExpr(indexExpr, 0, 0,
                                                           !isUnrolledLoopScope());
        if (normalized_expr == NULL)
        {
          ExprRangeError( mem, indexExpr, loc );
          delete the_memsel;
          return eFailPopulate;
        }
        *the_expr = sGenBitOrPartsel( the_memsel, normalized_expr, loc );
      } else {
        // extend the memsel with the new dimension here.
        memsel_rval->addIndexExpr(indexExpr);
        *the_expr = memsel_rval;
      }
      --remainingDims;
    }
    break;
  }
  case VHSLICENAME:
  {
    NUExpr *the_slice = NULL;
    LOC_ASSERT( numIndices == 1, loc ); // Not correct for a multidim slice (does that even make sense?)
    temp_err_code = sliceNameRvalue( vh_named, context, lWidth,
                                     &the_slice );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    *the_expr = sGenBitOrPartsel( the_slice, indexVector[numIndices-1], loc );
    break;
  }
  case VHFUNCCALL:
  {
    NUExprArray expr_array;
    temp_err_code = functionCall(vh_named, context, lWidth, &expr_array);
    NUExpr *the_fctcall_expr = expr_array[0];
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      delete the_fctcall_expr;
      return temp_err_code;
    }
    NUIdentRvalue *ident = dynamic_cast<NUIdentRvalue*>(the_fctcall_expr);
    NUCompositeInterfaceExpr *identComInt =  dynamic_cast <NUCompositeInterfaceExpr *> (the_fctcall_expr);

    if (ident != NULL)
    {
      net = ident->getIdent();
      delete the_fctcall_expr; // the expression is no longer needed, only the net was needed
      temp_err_code = indexedNameRvalueHelper( net, &indexVector, loc, the_expr );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
    }
    else if(identComInt != NULL)
    {
      *the_expr = new NUCompositeSelExpr(identComInt, &indexVector, loc);

    }
    else
    {
      // Getting here means the function call was not converted to a
      // task.  Or, it could mean that the function was accellerated,
      // and we really do want what we've got here.
      NUBinaryOp *binOp = dynamic_cast<NUBinaryOp*>(the_fctcall_expr);
      NUConcatOp *concOp = dynamic_cast<NUConcatOp*>(the_fctcall_expr);
      if ( binOp != NULL )
      {
        LOC_ASSERT( numIndices == 1, loc ); // Not correct for multidim index yet
        *the_expr = new NUVarselRvalue( binOp, indexVector[numIndices - 1], loc );
      }
      else if (concOp != NULL)
      {
        *the_expr = new NUVarselRvalue( concOp, indexVector[numIndices - 1], loc );
      }
      else
      {
        POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unable to take the index of a function call result." ), &err_code);
        delete the_fctcall_expr;
        return err_code;
      }
    }
  }
  break;
  case VHSELECTEDNAME:
  {
    vhExpr functCallExpr;
    bool functCall = isFunctionRecField(vh_named, &functCallExpr);

    if ( isRecordNet( vhGetActualObj( vh_named ), context ) || functCall)
    {
      if ( mCarbonContext->isNewRecord( ))
      {
        NUExpr *prefix;
        temp_err_code = selectedNameExpr( vh_named, context, &prefix, false, true );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
          return temp_err_code;
        }
        NUCompositeInterfaceExpr *cPrefix = dynamic_cast<NUCompositeInterfaceExpr*>( prefix );
        if ( cPrefix )
        {
          // Index a composite
          if ( indexVector.size() == 1 && cPrefix->getNet()->isVectorNet( ))
          {
            NUVectorNet *vn = cPrefix->getNet()->castVectorNet();
            // If this index references a vector net, it needs to be normalized
            indexVector[0] = vn->normalizeSelectExpr( indexVector[0], 0, true );
          }
          *the_expr = new NUCompositeSelExpr( cPrefix, &indexVector, loc );
          (*the_expr)->resize(( *the_expr )->determineBitSize( ));
          return temp_err_code;
        }
        else
        {
          NU_ASSERT( prefix->getType() == NUExpr::eNUIdentRvalue, prefix );
          net = dynamic_cast<NUIdentRvalue*>( prefix )->getIdent();
          delete prefix;
        }
      }
      else
      {
        // Get the correct NUNet
        temp_err_code = lookupNet( context->getDeclarationScope(),
                                   vh_named, context, &net );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
          return temp_err_code;
        }

        temp_err_code = getVhdlRecordIndicies( vh_named, &indexVector, context );
        if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code )) {
          return temp_err_code;
        }
      }

      temp_err_code = indexedNameRvalueHelper( net, &indexVector, loc, the_expr );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
        return temp_err_code;
      }
      break;
    }
    // deliberate fallthrough to the default case if this is not a record net
  }
  default:
  {
    temp_err_code = lookupNet( context->getDeclarationScope(), vh_named, context, &net );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    temp_err_code = indexedNameRvalueHelper( net, &indexVector, loc, the_expr );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    break;
  }
  }

  (*the_expr)->setSignedResult( eVHSIGNED == getVhExprSignType( vh_expr ));
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::indexedNameRvalueHelper( NUNet *net, NUExpr *bitsel_expr,
                                       const SourceLocator &loc,
                                       NUExpr **the_expr )
{
  NUExprVector indexVector;
  bitsel_expr->resize( bitsel_expr->determineBitSize( ));
  indexVector.push_back( bitsel_expr );
  return indexedNameRvalueHelper( net, &indexVector, loc, the_expr );
}


Populate::ErrorCode
VhdlPopulate::indexedNameRvalueHelper( NUNet *net, NUExprVector *indexVector,
                                       const SourceLocator &loc,
                                       NUExpr **the_expr )
{
  // An array access is just like a bit select in that we only get one entry
  // of the object.  We want to handle them differently so we distinguish
  // them now.
  const UInt32 numIndices = indexVector->size();
  NUExpr *bitsel_expr = NULL;
  NUVectorNet *vn = NULL;
  NUMemoryNet *mn = NULL;
  NUCompositeNet *cn = NULL;
  NUExpr *baseExpr = NULL;
  if (net->is2DAnything()) 
  {
    mn = net->getMemoryNet();
    const UInt32 numDims = mn->getNumDims();

    if ( numIndices < numDims )
    {
      NUExprVector memselVector;
      bool was_truncated = false;
      for ( NUExprVectorIter iter = indexVector->begin(); iter != indexVector->end(); iter++ )
      {
        memselVector.push_back( (*iter)->limitMemselIndexExpr(&was_truncated,mMsgContext,loc) );
      }
      if ( was_truncated and mn->isDeclaredWithNegativeIndex() ){
        mMsgContext->NegativeIndicesWithTruncation( net->getMemoryNet() );
      }
      *the_expr = createMemselRvalue(net, &memselVector, true, loc);
      return eSuccess;
    }
    else
    {
      // make a new vector without the last expr; these go in the memsel
      NUExprVector memselVector;
      bool was_truncated = false;
      for ( UInt32 i = 0; i < numIndices - 1; ++i )
      {
        NUExpr* index_expr = ((*indexVector)[i])->limitMemselIndexExpr(&was_truncated,mMsgContext,loc);
        memselVector.push_back( index_expr );
      }
      // We need to make a varsel with the last expr
      if ( was_truncated and mn->isDeclaredWithNegativeIndex() ){
        mMsgContext->NegativeIndicesWithTruncation( mn );
      }

      bitsel_expr = (*indexVector)[numIndices - 1];
      baseExpr = createMemselRvalue( mn, &memselVector, true, loc );
    }
  }
  else if ( net->isCompositeNet( ))
  {
    cn = net->getCompositeNet();
    NUCompositeIdentRvalue *ident = new NUCompositeIdentRvalue( cn, loc );
    *the_expr = new NUCompositeSelExpr( ident, indexVector, loc );
    ( *the_expr )->resize(( *the_expr )->determineBitSize( ));
    return eSuccess;
  }
  else 
  {
    NU_ASSERT( indexVector->size() == 1, net );
    bitsel_expr = (*indexVector)[0];
    vn = net->castVectorNet();
    baseExpr = createIdentRvalue( vn, loc );
  }

  // normalize all bit references, synthesizing corrective arithmetic as
  // needed.
  *the_expr = Populate::genVarselRvalue(net, baseExpr, bitsel_expr, loc,
                                        !isUnrolledLoopScope());
  if (*the_expr == NULL) {
    VectOrMemExprRangeError(net, bitsel_expr, loc);
    return eFailPopulate;
  }
  return eSuccess;
}

Populate::ErrorCode 
VhdlPopulate::getIndexExprs(UtVector<vhExpr>& dimVector, const SourceLocator& loc,
                            int lWidth, LFContext* context, bool* was_truncated,
                            NUExprVector* idxVec)
{
  ErrorCode err_code = eSuccess;
  const UInt32 numIdxs = dimVector.size();
  for ( UInt32 idx = 0; idx < numIdxs; ++idx )
  {
    vhExpr vh_index = dimVector[idx];
    NUExpr *idxExpr;
    ErrorCode temp_err_code = expr( vh_index, context, lWidth, &idxExpr );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    idxExpr->resize( idxExpr->determineBitSize( ));
    idxVec->push_back( idxExpr->limitMemselIndexExpr( was_truncated,
                                                      mMsgContext, loc ));
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::sliceNameRvalue(vhExpr vh_expr,
                              LFContext *context,
                              int lWidth,
                              NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  const SourceLocator loc = locator( vh_expr );
  SignType expr_sign = getVhExprSignType(vh_expr);
  NUExpr* index_expr = 0; // Holds the possibly variable part of the slice index
  ConstantRange range;    // holds the fixed part of the range of the slice
  NUNet* net = NULL;      // the net being sliced
  UtVector<vhExpr> dimVector; // the memsel indices if we're slicing a memsel


  vhExpr vh_exprCheck = vhGetPrefix(static_cast<vhExpr>(vh_expr));
  if (vhGetObjType(vh_exprCheck) == VHSLICENAME)
  {
    UtString buf;
    POPULATE_FAILURE(mMsgContext->SliceOfSlice(&loc, decompileJaguarNode(vh_expr, &buf)), &err_code);
    return err_code;
  }

  NUBase *base = NULL;
  bool isConstIndex = false;
  temp_err_code = partsel( vh_expr, context, 0, &base,
                           &index_expr, &range, &dimVector, &isConstIndex, false );

  // Find the net, if it's here
  net = dynamic_cast<NUNet *>( base );
  NUIdentRvalue *ident = NULL;
  if ( !net )
  {
    ident = dynamic_cast<NUIdentRvalue *>( base );
    if ( ident )
      net = ident->getIdent();
  }

  switch (temp_err_code)
  {
  case eFatal:
  case eFailPopulate:
    delete index_expr;
    return temp_err_code;
    break;
  case eNotApplicable:
    LOC_ASSERT(net, loc);
    temp_err_code = variableWidthSliceNameRvalue(vh_expr, context,
                                                 net, the_expr, lWidth);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return err_code;
    }
    (*the_expr)->setSignedResult(eVHSIGNED == expr_sign);
    return err_code;
    break;
  case eSuccess:
    break;
  }

  if ( net == NULL )
  {
    NUCompositeInterfaceExpr *rvalue = dynamic_cast<NUCompositeInterfaceExpr*>( base );
    NU_ASSERT( rvalue, base );
    // Create expressions for selects of all dimensions.
    NUExprVector idxVec;
    bool wasTruncated = false;
    temp_err_code = getIndexExprs(dimVector, loc, 0, context, &wasTruncated, &idxVec);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    // Add expression for last dimension's select.
    idxVec.push_back(index_expr);
    *the_expr = new NUCompositeSelExpr(rvalue, &idxVec, range /*part select*/,
                                       isConstIndex, loc);
    (*the_expr)->setSignedResult( eVHSIGNED == expr_sign );
    return err_code;
  }

  if ( net->isMemoryNet( ))
  {
    // Delete any unnecessary ident
    delete ident;
    ident = NULL;

    NUExprVector idxVector;
    bool was_truncated = false;
    const UInt32 numIdxs = dimVector.size();
    temp_err_code = getIndexExprs(dimVector, loc, 0, context, &was_truncated, &idxVector);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    if ( net->getMemoryNet()->getNumDims() - numIdxs == 1 )
    {
      if ( was_truncated and net->getMemoryNet()->isDeclaredWithNegativeIndex() ){
        mMsgContext->NegativeIndicesWithTruncation( net->getMemoryNet() );
      }

      // Create a NUMemselRValue here
      NUExpr *memRval = createMemselRvalue(net, &idxVector, true, loc );
      *the_expr = new NUVarselRvalue( memRval, index_expr, range, loc );
    }
    else
    {
      // This is partselect on a MemoryNet. Break it up into separate
      // MemoryNet row selects and concat them together. 
      SInt32 rowLsb = range.getLsb();
      SInt32 rowMsb = range.getMsb();
      UInt32 noOfRows = range.getLength();
      NUExprVector expr_vector;
      SInt32 incr = (rowLsb > rowMsb) ? 1 : -1;
      CopyContext cc(0,0);
      for (UInt32 i = 0; i < noOfRows ; i++)
      {
        // The index_expr plus the row address forms the row select.
        NUExpr* offset_expr = (NUExpr*)NUConst::create(true, rowMsb, 32, loc);
        NUExpr* addr_expr = new NUBinaryOp(NUOp::eBiPlus, index_expr->copy(cc),
                                           offset_expr, loc);
        NUExprVector pselVector;
        for ( UInt32 idx = 0; idx < numIdxs; ++idx )
        {
          if ( i == 0 )
            pselVector.push_back( idxVector[idx] ); // don't make a copy the first one
          else
            pselVector.push_back( idxVector[idx]->copy( cc ));
        }
        pselVector.push_back(addr_expr);
        NUExpr* rv = createMemselRvalue(net, &pselVector, false, loc);
        expr_vector.push_back(rv);
        rowMsb += incr;
      }
      *the_expr = new NUConcatOp(expr_vector, 1, loc);
      delete index_expr; // We've used it's copies.
      index_expr = NULL;
    }
  }
  else
  {
    if ( ident )
      *the_expr = new NUVarselRvalue( ident, index_expr, range, loc );
    else
      *the_expr = new NUVarselRvalue( net, index_expr, range, loc );
  }

  (*the_expr)->setSignedResult(eVHSIGNED == expr_sign);
  return err_code;
}


// Method to create NUExpr corresponding to a slice name object whose 
// width is variable. As verilog language standard does not allow this,
// we can not populate variable width bus directly to the nucleus. But
// we can create equivalent nucleus expression and we will follow that here.
// Here is how we will create equivalent NUExpr:
//
// Signal INPUT : bit_vector(H downto L );
// INPUT[MSB downto LSB] ==> EXT( INPUT ( H 'downto' L),
//                                 (MSB - LSB + 1) ) '
// Signal INPUT : bit_vector(L to H );
// INPUT[MSB to LSB] ==> 'EXT( (INPUT ( H 'downto' L),
//                                 (LSB - MSB + 1) ) '
// In the above, 'EXT' corresponds to VHDL extension operator and 'downto'
// corresponds to the Vhdl range-deferred sizing operator
//
Populate::ErrorCode
VhdlPopulate::variableWidthSliceNameRvalue(vhExpr vh_slice,
                                           LFContext *context,
                                           NUNet *net,
                                           NUExpr **the_expr,
                                           int lWidth)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  SourceLocator loc = locator(vh_slice);

  // In contexts where the size is self-evident, such as
  //     a(7 to 0) = b(x to y)
  // then take on faith the context size.  If x to y is anything else,
  // at runtime, a VHDL-LRM compliant simulator would give a runtime error.
  //

  vhExpr vh_named = vhGetPrefix(vh_slice);
  vhNode vh_range = vhGetDisRange(vh_slice);
  vhNode actual_range = NULL;
  temp_err_code = getVhRangeNode(vh_range, &actual_range, NULL);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  vh_range = actual_range;
  vhExpr vh_left = vhGetLtOfRange(vh_range);
  vhExpr vh_right = vhGetRtOfRange(vh_range);
  NUExpr* sliceLtBound = NULL;
  temp_err_code = expr ( vh_left, context, 0, &sliceLtBound );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  // As per IEEE std 1076.6-1999 section 8.3.2.1, array index must be 
  // of type integer or it's subtype. As per jaguar supplied STD package, 
  // INTEGER type is : TYPE integer is range -2147483648 to +2147483647;
  // which is of 32 bit, so with that STD package it can never be of more 
  // than 32 bit wide. Hence, we will set the size of the slice/net bounds
  // to 32 bit. 
  sliceLtBound->resize(32);
  NUExpr* sliceRtBound = NULL;
  temp_err_code = expr ( vh_right, context, 0, &sliceRtBound );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  sliceRtBound->resize(32);
  NUExpr* netExpr = NULL;
  temp_err_code = expr ( vh_named, context, 0, &netExpr );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // variable width of a whole memory is not yet supported, bug4850
  if ((netExpr->getType() == NUExpr::eNUIdentRvalue) && net->isMemoryNet()) {
    UtString buf;
    SourceLocator sliceLoc = locator(vh_slice);
    mMsgContext->VariableWidthMemory(&sliceLoc, decompileJaguarNode(vh_slice, &buf));
    return eFatal;
  }

  if (net->isCompositeNet())
  {
    UtString buf;
    SourceLocator slice_loc = locator(vh_slice);
    mMsgContext->VariableWidthRecordSlice(&slice_loc, decompileJaguarNode(vh_slice, &buf));
    return eFatal;
  }

  ConstantRange netRange;
  NUExpr* downTo = NULL;

  if (NUVectorNet* vn = dynamic_cast<NUVectorNet*>(net)) {
    netRange = *vn->getRange ();
    downTo = new NUBinaryOp (NUOp::eBiDownTo,
                             vn->normalizeSelectExpr (sliceLtBound, 0, false),
                             vn->normalizeSelectExpr (sliceRtBound, 0, false), loc);

  } else if (NUMemoryNet* mn = dynamic_cast<NUMemoryNet*>(net)) {
    netRange = *mn->getWidthRange ();
    downTo = new NUBinaryOp (NUOp::eBiDownTo,
                             mn->normalizeSelectExpr (sliceLtBound, 0, 0, false),
                             mn->normalizeSelectExpr (sliceRtBound, 0, 0, false), loc);
  } else if (NUCompositeNet* cn = net->getCompositeNet()) {
    netRange = *cn->getRange(0); // Index 0 is the innermost range.
    downTo = new NUBinaryOp (NUOp::eBiDownTo, sliceLtBound, sliceRtBound, loc);
  }

  // Invalid range expression?
  NU_ASSERT (downTo, netExpr);

  downTo->resize (32);


  // Choose between the passed in 'expected width' and the size of the physical object
  // favoring the smaller range.
  if (lWidth < (int)netRange.getLength () && lWidth > 0)
    netRange.setMsb (lWidth-1);
  else
    lWidth = netRange.getLength ();

  if (net->isCompositeNet()) {
    NUCompositeInterfaceExpr* comp_expr = dynamic_cast<NUCompositeInterfaceExpr*>(netExpr);
    // The lvalue better be a composite since the net is composite.
    NU_ASSERT(comp_expr, netExpr);
    NUExprVector idxVec;
    idxVec.push_back(downTo);
    *the_expr = new NUCompositeSelExpr(comp_expr, &idxVec, netRange, false, loc);
  } else {
    *the_expr = new NUVarselRvalue (netExpr, downTo, netRange, loc);
  }

  (*the_expr)->resize (lWidth);

  return eSuccess;
}

// Check if this is a standard library function. We will try  internalizing
// it based on presence of Meta-comments within the function body.
//   
// For eg, To_bit, To_bitvector, To_StdULogic, To_StdLogicVector, To_X01 etc
//        can be treated as no-op and hence do not require normal subprogram
//        population. So a Meta comment has been added to the body of these
//        functions which helps the populator to recognize it as a no-op. 
//   Jaguar recognizes the Meta-comment if It has been registered with it 
//   using the APIs vhDefineSynthPrefix() and vhAddMetaDirective() 
//   eg of a Meta-comment used
//    -- pragma built_in SYN_FEED_THRU
//   The last argument 'SYN_FEED_THRU' denotes no-op functions. For these
//   functions we would simply return back the first input argument.
//   We could similarly categorize other standard functions and  internalize 
//   them appropriately here.
bool
VhdlPopulate::internalizeFuncCall(vhNode vh_function,
                                  vhExpr vh_func_call,
                                  LFContext *context,
                                  int        lhsWidth,
                                  NUExpr   **the_expr,
                                  ErrorCode *returnErrCode)
{
  bool bIsSynFeedThru = false;
  ErrorCode err_code = eSuccess;
  *the_expr = 0;
  
  vhNode vh_meta_com;
  JaguarList vh_meta_com_list(vhGetListofMetaComments(vh_function));
  while ((vh_meta_com = vhGetNextItem(vh_meta_com_list)) != NULL)
  {
    const char* meta_comm_str = vhGetAssociatedDirectiveString(vh_meta_com);
    StrToken tok(meta_comm_str, " \t");

    if (!tok.atEnd() && (strcasecmp(*tok, "pragma") == 0))
    {
      ++tok;
      if (!tok.atEnd() && (strcasecmp(*tok, "built_in") == 0))
      {
        ++tok;
        if (!tok.atEnd() && (strcasecmp(*tok, "SYN_FEED_THRU") == 0))
        {
          bIsSynFeedThru = true;
        }
        // Add other categories of internalizable functions here.
        // SYN_FEED_THRU is just one of them.
      }
    }

  }

  if (bIsSynFeedThru)
  {
    // For this category of functions we just need to return the first input
    // parameter of the function call.
    JaguarList vh_arg_iter(vhGetAssocList(vh_func_call));
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    
    ErrorCode temp_err_code = expr(static_cast<vhExpr>(vh_arg), context,
                                   lhsWidth, the_expr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      *returnErrCode = temp_err_code;
      return true;
    }
  } 

  *returnErrCode = err_code;
  return bIsSynFeedThru;
}

// Used here and in VhPopulateLvalue.cxx
bool isUnconstrainedArray(vhNode constraint)
{
  bool retval = false;
  if (!constraint)
    return retval;
  switch (vhGetObjType(constraint))
  {
    case VHOBJECT:
      retval = isUnconstrainedArray(vhGetActualObj(constraint));
      break;
    case VHCONSTANT:
    case VHSIGNAL:
    case VHVARIABLE:
      retval = isUnconstrainedArray(vhGetSubTypeInd(constraint));
      break;
    case VHSUBTYPEIND:
    {
      vhNode cons = vhGetConstraint(constraint);
      if (!cons)
      {
        retval = isUnconstrainedArray(vhGetType(constraint));
      }
      break;
    }
    case VHTYPEDECL:
    case VHSUBTYPEDECL:
    {
      vhNode subTypeInd = NULL;
      vhNode range = NULL;
      if (vhGetObjType(constraint) == VHSUBTYPEDECL)
      {
        subTypeInd = vhGetSubTypeInd(constraint);
      }
      if (subTypeInd)
      {
        range = vhGetConstraint(subTypeInd);
      }  
      if (!range)
      {
        vhNode vh_typedef = vhGetTypeDef(constraint);
        retval = isUnconstrainedArray(vh_typedef);
      }
      break;
    }
    case VHUNCONSARRAY:
    {
      retval = true;
      break;
    }
    default:
      retval = false;
  }
  return retval;
}

vhExpr sGetConstNode(vhExpr node)
{
  vhExpr const_node = NULL;
  // Evaluate jaguar expression if it's static.
  node = VhdlPopulate::evaluateJaguarExpr(node, false, true);
  vhObjType type = vhGetObjType( node );
  switch (type)
  {
  case VHOBJECT:
    const_node = sGetConstNode(static_cast<vhExpr>(vhGetActualObj(node)));
    break;
  case VHCONSTANT:
    {
      vhExpr initVal;
      if (vhIsDeferred(node)) {
	initVal = vhGetDeferredConstantValue(node);
      }
      else {
	initVal = vhGetInitialValue(node);
      }
      const_node = sGetConstNode(initVal);
      break;
    }
  case VHAGGREGATE:
    {
      // Make sure all the elements of the aggregate are constant themselves.
      JaguarList eleList(vhGetEleAssocList(node));
      vhExpr element;
      bool allElementsConstant = true;

      while ((element = static_cast<vhExpr>(vhGetNextItem(eleList)))) {
	if (NULL == sGetConstNode(element)) {
	  allElementsConstant = false;
	  break;
	}
      }

      if (allElementsConstant)
	const_node = node;
    }
    break;
  case VHBASELIT:
  case VHDECLIT:
  case VHCHARLIT:
  case VHIDENUMLIT:
  case VHSTRING:
  case VHBITSTRING:
    const_node = node;
    break;
  default:
    break;
  } // switch
  return const_node;
}


// This static helper function is used by createSubprogSignature to emit
// "-<value>" for subprogram actual parameters with a constant value.
// This is used to disambiguate subprogram calls that differ only by the
// value of an integer parameter, where that param is passed down though
// the a function call hierarchy.
// Also indicates if a constant has been emitted.
void VhdlPopulate::emitValueIfConst( vhExpr node, UtString *name )
{
  vhExpr const_node = sGetConstNode(node);
  if (const_node != NULL)
  {
    // If the actual is a literal, add its value to the signature.
    vhObjType type = vhGetObjType( const_node );
    switch (type) {
    case VHDECLIT:
      if ( vhIsReal( const_node ))
        *name << "-" << vhGetDecLitValue( const_node );
      else
        *name << "-" << (int)vhGetDecLitValue( const_node );
      break;
    case VHBASELIT:
      *name << "-" << vhGetBaseLit( const_node );
      break;
    case VHCHARLIT: {
      SInt32 litVal = vhGetCharVal( const_node );
      *name << "-char-" << litVal;
      break;
    }
    case VHIDENUMLIT: {
      int val = (int)vhGetIdEnumLitValue(const_node);
      vhNode type_decl =  vhGetActualObj(vhGetExpressionType(const_node));
      UtString buf;
      if (getEncodedStringFromAttrib(&buf, (vhExpr)(type_decl), val)) {
        *name << "-enum-" << buf;
      }
      else {
        *name << "-enum-" << val;
      }
      break;
    }
    case VHSTRING: {
      JaguarString str(vhGetStringVal(const_node));
      *name << "-str-" << str;
      break;
    }
    case VHBITSTRING: {
      JaguarString str(vhGetStringVal(const_node));
      *name << "-bitstr-" << str;
      break;
    }
    case VHAGGREGATE: {
      // Aggregates can be constant too, if all their elements are constant.
      // If that's the case, then they should contribute to the function name.
      // This code was added to fix bug 8659.
      JaguarList eleList(vhGetEleAssocList(const_node));
      vhExpr element;
      while ((element = static_cast<vhExpr>(vhGetNextItem(eleList)))) {
	emitValueIfConst(element, name);
      }
      break;
    }
    default:
      // This shouldn't happen unless sGetConstNode() has been modified
      // to identify additional types as constants that this method doesn't.
      // Modifying this method to bring it in sync should fix it.
      SourceLocator loc = locator(const_node);
      LOC_ASSERT(0, loc);
      break;
    } // switch
  } // if
} // void VhdlPopulate::emitValueIfConst


// Function to create subprogram signature by concatenating the width of the
// subprogram argument with the PATHNAME of the subprogram. 
StringAtom*
VhdlPopulate::createSubprogSignature(LFContext *context, 
                                     vhNode vh_function_call)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  UtString name; 
  vhNode vh_func = vhGetMaster(vh_function_call);
  if ( vh_func )
  {
    vhNode vh_subprog = vhGetSubProgBody(vh_func);
    if ( vh_subprog )
    {
      vhNode scope = vhGetScope(vh_subprog);
      switch (vhGetObjType(scope))
      {
      case VHARCH:
        name << vhGetLibName(scope) << '_';
        name << vhGetEntityName(scope) << '_';
        name << vhGetArchName(scope);
        break;
      case VHENTITY:
        name << vhGetLibName(scope) << '_';
        name << vhGetEntityName(scope);
        break;
      case VHPACKAGEBODY:
        name << vhGetLibName(scope) << '_';
        name << vhGetPackName(scope);
        break;
      default:
        break;
      }

      // Now append the line number of this subprogram with the signature so
      // that we can differentiate between two subprograms of same name, same
      // argument width but the argument types are different.
      SourceLocator loc = locator(vh_subprog);
      name << "_L" << loc.getLine() << '_' << vhGetSubProgName(vh_subprog);
    }
    else
    {
      name << "subprog_with_no_body";
    }
  }
  else
  {
    name << "subprog_with_no_masternode";
  }

  int width = 0, numDims = 0;

  vhObjType function_call_type = vhGetObjType( vh_function_call );

  // Include in the signature the computed width for the function call
  const SourceLocator loc = locator( vh_function_call );
  if ( function_call_type != VHPROCEDURECALL ) {
    temp_err_code = getVhExprBitSize( static_cast<vhExpr>(vh_function_call),
                                      &width, &numDims, loc, context,
                                      false,   // not an lvalue
                                      false ); // do not report error

    if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ) ) 
    {
      // Couldn't figure out the return size of the function.
      // This currently only happens when there are multiple return statements of
      // different sizes and the compiler cannot statically determine which return statement
      // will be used.
      return NULL;
    }

    if (temp_err_code != eSuccess) {
      // We cannot figure out how wide this function-call is, so
      // make the signature unique by writing the a function-call index
      // into the name
      name << "_?" << mUniqueFunctionIndex++;
    }
    else {
      name << "_" << numDims << 'd' << width;
    }
  }

  if ( function_call_type == VHUNARY )
  {
    vhExpr op = vhGetOperand( static_cast<vhExpr>( vh_function_call ));
    temp_err_code = getVhExprWidth( op, &width, loc, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return NULL;
    }
    name << '_'; // Arg widths separated with the separator '_'
    name << width;
    emitValueIfConst( op, &name );
  }
  else if ( function_call_type == VHBINARY )
  {
    vhExpr op = vhGetLeftOperand( static_cast<vhExpr>( vh_function_call ));
    temp_err_code = getVhExprWidth( op, &width, loc, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return NULL;
    }
    name << '_'; // Arg widths separated with the separator '_'
    name << width;
    emitValueIfConst( op, &name );
    op = vhGetRightOperand( static_cast<vhExpr>( vh_function_call ));
    temp_err_code = getVhExprWidth( op, &width, loc, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return NULL;
    }
    name << '_'; // Arg widths separated with the separator '_'
    name << width;
    emitValueIfConst( op, &name );
  }
  else
  {
    JaguarList vh_arg_iter(vhGetAssocList(vh_function_call));
    vhNode vh_arg;
    while ((vh_arg = vhGetNextItem(vh_arg_iter)))
    {
      name << '_'; // Arg widths separated with the separator '_'
      temp_err_code = getVhExprWidth( static_cast<vhExpr>(vh_arg), &width, loc,
                                      context, false,   // not an lvalue
                                      false ); // do not report error
      if (temp_err_code != eSuccess) {
        // We cannot figure out how wide this argument is, probably because
        // it's a record and we haven't implemented the finding of widths
        // from records yet.  But this does not affect our ability to
        // populate a unique signature for this function, because Jaguar
        // will already have found the correct vh_func for this record
        // type for us, and we use (vh_func,signature) as a mechanism to
        // map a function-call to an NUTask*.
        name << "?";
      }
      else {
        name << width;
      }
      emitValueIfConst( static_cast<vhExpr>( vh_arg ), &name );
    }
  }

  // Now internize this name so that this string remains in the memory
  // till the current module. The beauty of the function intern() is, it will
  // keep only one copy of this string in the memory, so there will not be
  // any duplication.
  StringAtom* internName = context->vhdlIntern(name.c_str());
  return internName;
}

/* This is an overly long function.  In general it is organized like so:
 * 1) Accelerate the function if possible.
 * 2) Create the signature and elaborate the function in Jaguar
 * 3) Determine the return value width from context
 * 4) Get or create the function itself
 * 5) Create connections for the output(s) of the function
 * 6) Get the list of actuals for this function call
 * 7) Get the formals, create the actuals in Nucleus, and connect them.
 * 8) Create and populate the Nucleus for the function call itself.
 * 9) Clean up.
 */
VhdlPopulate::ErrorCode
VhdlPopulate::functionCall(vhExpr vh_function_call,
                           LFContext *context,
                           int lhsWidth,
                           NUExprArray *expr_array)
{
  ErrorCode err_code = eSuccess, temp_err_code = eSuccess;
  NUExpr *the_expr;
  vhNode elaborated_subp = NULL;
  StringAtom* internName = NULL;
  SourceLocator loc = locator( vh_function_call );

  if ( vhGetObjType(vh_function_call) == VHFUNCCALL )
  {
    // Accelerate builtin functions and exit before attempting to process the call
    // Check if this function corresponds to any predefined std. function.
    // If so then get it accelerated ...
    temp_err_code = acceleratedExprForFunctionCall( vh_function_call,
                                                    context, &the_expr,
                                                    lhsWidth);
    if (eSuccess == temp_err_code) {
      expr_array->push_back( the_expr );
      return eSuccess;
    }
    if ( errorCodeSaysReturnNowIfIncomplete( temp_err_code, NULL )) {
      return temp_err_code;
    }

    // Process if it is feed thru function or builtin/mapped operator/function
    temp_err_code = processBuiltInFuncOrMappedOperator( vh_function_call,
                                                        context, &the_expr,
                                                        lhsWidth );
    if (eSuccess == temp_err_code) {
      expr_array->push_back( the_expr );
      return eSuccess;
    }
    if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, NULL )) {
      return temp_err_code;
    }

    // We can't populate function "NOW", so don't try.
    vhNode scope = vhGetScope( vhGetMaster( vh_function_call ));
    JaguarString lib_name( vhGetLibName( scope ));
    if ( !strcasecmp( (const char *)lib_name, "STD" ))
    {
      JaguarString scope_name( vhGetPackName( scope ));
      if ( !strcasecmp( (const char *)scope_name, "STANDARD" ))
      {
        JaguarString func_name( vhGetName( vhGetMaster( vh_function_call )));
        if ( !strcasecmp( (const char *)func_name, "NOW" ))
        {
          POPULATE_FAILURE(mMsgContext->VhdlNowNotSupported( &loc ), &err_code);
          return err_code;
        }
      }
    }
  }

  if ((SInt32)mRecursionStack.size() > mRecursionLimit) {
    mMsgContext->MaximumRecursionLimitReached(&loc);
    return eFatal;
  }

  // Cache current dynamic values on function call arguments. Apply propagated constants
  // on the arguments, so that they propagate into the function call. This helps determine
  // sizes or declarations within function that depend on these constants for successful
  // population ..especially recursive functions.
  DynValCache dvc(this, context, mCarbonContext->isNewRecord());
  {
    JaguarBrowser jb(dvc);
    jb.browse(static_cast<vhNode>(vh_function_call));
  }

  if (!dvc.getSuccess()) {
    return eFailPopulate;
  }

  // The DynValCache above might replace some function calls with constants.
  // We are checking here whether that replacement happened. If yes, we return
  // the replaced expression.
  vhObjType obj_type = vhGetObjType(vh_function_call);
  vh_function_call = evaluateJaguarExpr(static_cast<vhExpr> (vh_function_call), false, true);
  if(vhGetObjType(vh_function_call) != obj_type)
  {
    NUExpr* const_ret;
    temp_err_code = expr(vh_function_call, context, 0, &const_ret);
    if(errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ))
      return temp_err_code;
    else
    {
      expr_array->push_back(const_ret);
      return temp_err_code;
    }
  }

  // NOTE: For recursive functions, the vhOpenSubProgram changes the
  // vh_function_call by unwinding recursion once. The vh_function_call
  // won't represent a call to the elaborated subprogram, but a
  // call made from within the subprogram. See test/vhdl/lang_func/bug6597.vhdl.

  // Have to get size of return expression before opening the sub program,
  // otherwise we'll get the return size of sub program.
  vhExpr vh_size = vhGetExprSize( vh_function_call );
  if ( vhGetObjType(vh_function_call) == VHFUNCCALL )
  {
    // Create the subprogram signature before opening the subprogram so that
    // we can set the width of all of its argument which are builtin/mapped
    // operator/function . See bug3679
    internName = createSubprogSignature(context,vh_function_call);

    // Could not determine the name of the subprogram
    if (internName == NULL)
    {
      UtString msg;
      getVhdlObjName( vhGetFuncCall( vh_function_call ), &msg);
      mMsgContext->CouldNotDetermineTaskName( &loc, msg.c_str());
      return eFatal;
    }

    // Open this subprogram for processing
    elaborated_subp = vhOpenSubProgram(vh_function_call);
  }
  else
  {
    // Create the subprogram signature before opening the operator so that
    // we can set the width of all of its argument which are builtin/mapped
    // operator/function . See bug3679
    internName = createSubprogSignature(context,vh_function_call);

    // Could not determine the name of the subprogram
    if (internName == NULL)
    {
      UtString msg;
      getVhdlObjName( vhGetFuncCall( vh_function_call ), &msg );
      mMsgContext->CouldNotDetermineTaskName( &loc, msg.c_str());
      return eFatal;
    }

    // Open the body of this overloaded operator
    elaborated_subp = vhOpenOverloadedExpr(vh_function_call);
  }

  if ( elaborated_subp == NULL )
  {
    UtString functionName;
    getVhdlObjName( vhGetFuncCall( vh_function_call ), &functionName );
    POPULATE_FAILURE(mMsgContext->VhdlMissingSubprogramBody( &loc, functionName.c_str( )), &err_code);
    // No need to call vhCloseFeature()--it didn't get opened
    return err_code;
  }

  vhNode vh_function = vhGetSubProgBody(elaborated_subp);

  // If any actuals have been marked with an user defined attribute, make sure
  // that the corresponding formal is also marked in the same way.
  // This is done to make sure that actuals that are not constant transfer that
  // fact no matter how many times the function actually recurses.
  // This code was added as a fix for supporting aggregates as constants (bug 8659).
  if ( vhGetObjType(vh_function_call) == VHFUNCCALL )
    transferUserDefinedAttributes(vh_function_call, vh_function);

  // Check if this is an call to standard library function that can be 
  // internalized. The Populator needn't create any NU object for them.
  if (internalizeFuncCall(vh_function, vh_function_call, context, 
                          lhsWidth, &the_expr, &err_code))
  {
    expr_array->push_back( the_expr );
    vhCloseFeature(); // close the subprogram body
    return err_code;
  }

  const bool isRecursive = vhIsSubProgRecursive( vh_function );
  const VhPureType purity = vhGetPureType( vhGetSubProgSpec( vh_function ));
  if ( isRecursive )
  {
    if ( purity == VH_IMPURE || purity == VH_ERROR_PURETYPE )
    {
      const SourceLocator func_loc = locator( vh_function );
      POPULATE_FAILURE(mMsgContext->ImpureRecursion( &func_loc ), &err_code);
      return err_code;
    }
    mRecursionStack.push( vh_function );
  }
  // Always save off function call variable initial values, because
  // multiple instantiations might need the original values.
  JaguarList func_decls( vhGetDeclItemList( vh_function ));
  context->saveJaguarInitialVariableValues( &func_decls, isRecursive );

  vhOpenDeclarativeRegionNoFolding(vh_function, VH_TRUE);

  NUModule *module = context->getModule();

  NUNet* actual_net = NULL; // holds the results of the task being used in
                            // place of the function
  NUNet* formal_net = NULL; // net that is the output of the task being
                            // used in place of the function
  NUTask *task = 0;

  temp_err_code = lookupTask(module, internName, &task);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code)) {
    return temp_err_code;
  }
  if ( !task )
  {
    temp_err_code = function(vh_function, vh_function_call, internName, lhsWidth, 
                             vh_size, context, isRecursive, &task);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }

    temp_err_code = mapTask(module, internName, task);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }

  }
  else
  {
    internName = task->getName();
  }

  if (isRecursive) {
    mRecursionStack.pop();
  } else if (mIsFuncConstPropEnabled) {
    // Check if function returns a constant. If it does, then return the
    // constant expression instead of a task enable.
    NUConst* constRet = lookupConstantReturnForTask(task);
    if (constRet != NULL)
    {
      vhCloseFeature(); // Close the declarative region
      vhCloseFeature(); // close the subprogram body

      // Make a copy of the constant task return expression.
      CopyContext cc(0, 0);
      NUExpr* constRetCopy = constRet->copy(cc);
      // The signedness of constant expression should match that of
      // the return argument. The first task arg is always the return arg.
      formal_net = task->getArg(0);
      constRetCopy->setSignedResult(formal_net->isSigned());
      NU_ASSERT(constRetCopy->getBitSize() == formal_net->getBitSize(), constRetCopy);
      expr_array->push_back(constRetCopy);
      return err_code;
    }
  }

  NUTFArgConnectionVector arg_conns;
  // Create the output net(s) for the function.  Use formal_net to get
  // proper size.
  UInt32 numReturnArgs = 1;
  const bool returnsRecord = !mCarbonContext->isNewRecord() && isRecordNet( vh_function_call, context );
  if ( returnsRecord )
    numReturnArgs = numRecordElements( vh_function_call, context );

  for ( UInt32 i = 0; i < numReturnArgs; ++i )
  {
    formal_net = task->getArg( i );
    bool isBitNet = formal_net->isBitNet();
    bool isSigned = formal_net->isSigned();
    NUVectorNet *vector_net = formal_net->castVectorNet();
    NUMemoryNet *memory_net = formal_net->getMemoryNet();
    NUCompositeNet *composite_net = formal_net->getCompositeNet();
    NU_ASSERT(( isBitNet ) or ( vector_net != NULL ) or ( memory_net != NULL ) 
              or ( composite_net != NULL ), formal_net );
    StringAtom *tempname = module->gensym( "output", internName->str(),
                                          NULL, false );

    // Due to bug4288, we need to, for the time being, put the
    // function-output variables in the module scope.  When
    // that bug is fixed, we can go back to putting them in the
    // block scope.
    //
    // [APL] ALSO WE SHOULD COPY THE NETFLAGS INTO THE TEMP NET.  Don't do that
    // NOW, because it sets all sorts of nasty things like eNonStaticNet and
    // eBlockLocalNet that confuse cbuild.
    //
    //NUScope *block_scope = context->getBlockScope();
    NUScope *block_scope = module;

    if ( vector_net != NULL )
    {
      const ConstantRange *function_range = vector_net->getRange();
      ConstantRange temprange(*function_range);
      actual_net = block_scope->createTempVectorNet( tempname, temprange,
                                                     isSigned, loc,
                                                     NetFlags (0),
                                                     vector_net->getVectorNetFlags ());
    }
    else if ( memory_net != NULL )
    {
      NetFlags flags = NetFlags (memory_net->getFlags () & eDeclareMask);
      NU_ASSERT(memory_net->isSigned() == formal_net->isSigned(), formal_net);
      if ( isSigned )
        flags = NetFlags(flags | eSigned);
      actual_net = block_scope->createTempMemoryNetFromImage( tempname, memory_net, flags);
    }
    else if ( composite_net != NULL )
    {
      actual_net = block_scope->createTempCompositeNetFromImageForInterraFlowUseOnly( tempname, composite_net,
                                                                 NetFlags( composite_net->getFlags( ) && !ePortMask ),
                                                                 context->getNetRefFactory( ));
    }
    else
    {
      actual_net = block_scope->createTempBitNet(tempname, isSigned, loc);
    }

    actual_net->putIsBlockLocal( false ); // bug4288

    // Process the connection for the outputs of the task that was
    // created for this function (note that vhdl does not require that
    // there be any arguments to a function, so this is done even when
    // vh_arg_iter is NULL).  This must precede the function call itself
    // so that the arg exprs are properly evaluated.

    // Add this output net to the arg connection vector.
    NUTFArgConnection *arg_conn;
    NULvalue *actual_lvalue = new NUIdentLvalue(actual_net, loc);
    actual_lvalue->resize ();
    arg_conn = new NUTFArgConnectionOutput( actual_lvalue, formal_net,
                                            task, loc );
    arg_conns.push_back(arg_conn);


    the_expr = createIdentRvalue( actual_net, loc );
    the_expr->resize( the_expr->determineBitSize () );
    the_expr->setSignedResult( isSigned );
    expr_array->push_back( the_expr );
  }

  // Get the argument list of the function.  If this is actually an
  // operator, we need to create an argument list for later processing.
  // Operators have either one or two arguments.
  JaguarList vh_arg_iter;
  if (vhGetObjType(vh_function_call) == VHFUNCCALL)
  {
    getFuncProcActualArgList(vh_function_call, &vh_arg_iter, NULL);
  }
  else
  {
    vh_arg_iter = vhwCreateNewList(VH_TRUE /*Can be deleted*/);
    if (vhGetObjType(vh_function_call) == VHUNARY)
    {
      vhwAppendToList(vh_arg_iter, vhGetOperand(vh_function_call));
    }
    else
    {
      vhwAppendToList(vh_arg_iter, vhGetLeftOperand(vh_function_call));
      vhwAppendToList(vh_arg_iter, vhGetRightOperand(vh_function_call));
    }
  }

  vhCloseFeature(); // Close the declarative region
  vhCloseFeature(); // close the subprogram body

  temp_err_code = procArgConnections( vh_arg_iter, task, numReturnArgs,
                                      context, &arg_conns );
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code)) {
    return temp_err_code;
  }

  // Make the task_enable statement for this function, set the argument
  // connections, and put it into the stmt list
  NUTaskEnable *task_enable = 
    context->stmtFactory()->createTaskEnable( task, arg_conns, context->getModule(),
                                              false, loc );
  if ( context->isStmtStackEmpty( ))
    context->addExtraTaskEnable( task_enable );
  else
    context->addStmt( task_enable );

  return err_code;
}

Populate::ErrorCode
VhdlPopulate::transferUserDefinedAttributes(vhExpr vh_func_call,
					    vhNode elaboratedFunctionBody)
{
  ErrorCode err_code = eSuccess;

  JaguarList actualIter(vhGetAssocList(vh_func_call));
  JaguarList formalIter(vhGetFlatParamList(elaboratedFunctionBody));

  vhNode vhActual = NULL;

  while ((vhActual = vhGetNextItem(actualIter)) != NULL)
  {
    vhNode vhFormal = vhGetNextItem(formalIter);
    if (vhGetObjType(vhActual) != VHOPEN)
    {
      // Find the declaration
      DeclarationFinder declFind;
      {
	JaguarBrowser jb(declFind);
	jb.browse(vhActual);
      }

      vhNode actualPort = declFind.getDeclaration();

      // Check if the actual port was marked as not being constant. If it
      // was, make sure that the corresponding formal port is marked as well
      vhNode value = vhGetAttr(actualPort, "IS_NOT_CONSTANT");

      if (value != NULL)
      {
	// Find the declaration that corresponds to the formal port
	DeclarationFinder declFind;
	{
	  JaguarBrowser jb(declFind);
	  jb.browse(vhFormal);
	}

	vhNode formalPort = declFind.getDeclaration();

	// Mark the formalPort as being not constant, even though it is a VHCONSTANT
	if (vhGetAttr(formalPort, "IS_NOT_CONSTANT") == NULL) {
	  vhSetAttr(formalPort, value, "IS_NOT_CONSTANT");
	}
      }
    }
  }

  return err_code;
}

Populate::ErrorCode
VhdlPopulate::subProgArgConnection(vhNode vh_arg,
                                   NUNet *formal_net,
                                   NUTF *tf,
                                   LFContext *context,
                                   NUTFArgConnection **the_conn)
{
  ErrorCode err_code = eSuccess;
  *the_conn = 0;
  SourceLocator loc = locator(vh_arg);

  if (formal_net->isInput()) 
  {
    NUExpr *actual_expr = 0;
    ErrorCode temp_err_code = expr(static_cast<vhExpr>(vh_arg), context, 
                                   0, &actual_expr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    UInt32 bsize = std::max(formal_net->getBitSize(), actual_expr->determineBitSize());
    actual_expr = actual_expr->makeSizeExplicit(bsize);

    *the_conn = new NUTFArgConnectionInput(actual_expr, formal_net, tf, loc);

  } 
  else if (formal_net->isOutput()) 
  {
    NULvalue *actual_lvalue = 0;
    ErrorCode temp_err_code = lvalue(vh_arg, context, &actual_lvalue);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    actual_lvalue->resize ();
    *the_conn = new NUTFArgConnectionOutput(actual_lvalue,formal_net, tf, loc);

  } 
  else if (formal_net->isBid()) 
  {
    NULvalue *actual_lvalue = 0;
    ErrorCode temp_err_code = lvalue(vh_arg, context, &actual_lvalue);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    actual_lvalue->resize ();
    *the_conn = new NUTFArgConnectionBid(actual_lvalue, formal_net, tf, loc);

  } 
  else 
  {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unsupported sub-program argument"), &err_code);
    return err_code;
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::constantRange(vhNode vh_range,
                            LFContext * context,
                            ConstantRange **the_range)
{
  ErrorCode err_code = eSuccess;
  *the_range = 0;

  NUExpr *msb_const_expr = 0;
  ErrorCode temp_err_code = constant(vhGetLtOfRange(vh_range), 0, context, 
                                     &msb_const_expr);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
 
  NUConst *lsb_const = 0;
  NUExpr *lsb_const_expr = 0;
  temp_err_code = constant(vhGetRtOfRange(vh_range),0,context,&lsb_const_expr);
  lsb_const = lsb_const_expr->castConst();

  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NUConst *msb_const = msb_const_expr->castConst();
  SInt32 msb, lsb;
  bool ok = msb_const->getL(&msb) and lsb_const->getL(&lsb);

  if (not ok) {
    SourceLocator loc = locator(vh_range);
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unsupported range"), &err_code);
    return err_code;
  }
  *the_range = new ConstantRange(msb, lsb);

  delete msb_const;
  delete lsb_const;

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::constant(vhExpr vh_expr,
                       int lWidth,
                       LFContext *context,
                       NUExpr **the_const)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  *the_const = 0;

  SourceLocator loc = locator(vh_expr);

  if ( vhGetObjType( vh_expr ) == VHPHYSICALTYPE )
  {
    UtString physConst;
    decompileJaguarNode(vh_expr, &physConst);
    mMsgContext->PhysicalTypesUnsupported( &loc, physConst.c_str( ));
    return eSuccess;
  }
  if ( vhGetObjType( vh_expr ) == VHCONSTANT ||
       vhGetObjType( vh_expr ) == VHGENERIC )
  {
    vhNode vh_type = vhGetTypeDef( vhGetType( vh_expr ));
    if ( vhGetObjType( vh_type ) == VHPHYSICALTYPE )
    {
      JaguarString vh_name( vhGetName( vh_expr ));
      mMsgContext->PhysicalTypesUnsupported( &loc, vh_name );
      return eSuccess;
    }
  }

  vhExpr evaldVal = evaluateJaguarExpr(vh_expr, true, !mIsAggressiveEvalEna);
  if (NULL == evaldVal)
  {
    // Before giving up, attempt to use the constant propagated value by
    // constructing an expression.
    NUExpr* the_expr = NULL;
    ErrorCode temp_err_code = expr(vh_expr, context, 0, &the_expr, false, false);
    if (temp_err_code == eSuccess) {
      NUExpr* folded = mFold->fold(the_expr);
      if (folded) {
        if (folded->castConst() != NULL) {
          *the_const = folded;
          return eSuccess;
        }
        the_expr = folded;
      }
    }
    delete the_expr;
    POPULATE_FAILURE(mMsgContext->NonStaticConstant(&loc), &err_code);
    return err_code;
  }
  vh_expr = evaldVal;
  UInt32 bit_width = 0;

  int value = 0;
  if(lWidth == 0)
  {
    temp_err_code = getVhExprWidth(vh_expr, &value, loc, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      return temp_err_code;
  }
  else
    value = lWidth;
    
  bit_width = UInt32(value);

  value = 0;
  UtString str_val;
  if (vhGetObjType( vh_expr ) == VHDECLIT)
  {
    if ( vhIsReal( vh_expr ) == VH_TRUE ) {
      CarbonReal dval = vhGetDecLitValue( vh_expr );
      *the_const = NUConst::create( dval, loc );
      return eSuccess;
    }
  }

  temp_err_code = getVhExprValue(vh_expr,&value, &str_val);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  if ( vhGetObjType( vh_expr ) == VHSTRING || (vhGetObjType(vh_expr) == VHBITSTRING) ||
       not str_val.empty() )
  {
    bool isLogic = false;
    if(str_val.empty() == false)
      isLogic = true;

    int char_bitsize = 0;
    bool isCharacter = false;
    vhNode elmType = NULL;
    if ((vhGetObjType(vh_expr) == VHSTRING) || (vhGetObjType(vh_expr) == VHBITSTRING))
    {
      isLogic = false;
      vhNode expr_type = vhGetExpressionType(vh_expr);
      vhNode typeDef = vhGetTypeDef(expr_type);
      vhNode elem_subtype_ind = vhGetEleSubTypeInd(typeDef);
      elmType = vhGetType(elem_subtype_ind);
      
      vhNode basetype = vhGetAbsoluteBaseType(elmType);
      JaguarString type_mark(vhGetName(basetype));
      // type_mark will either be CHARACTER, custom enumeration or STD_ULOGIC/STD_LOGIC/bit. Do strcasecmp()    
      if(strcasecmp("STD_LOGIC", type_mark)  == 0 || 
         strcasecmp("STD_ULOGIC", type_mark) == 0 ||
         strcasecmp("BIT", type_mark) == 0)
        isLogic = true;

      isCharacter = (strcasecmp("CHARACTER", type_mark) == 0);
        
      // If this is a character, find the character bit size. It is
      // 8 bits for VHDL-93 and 7 bits for VHDL-87.
      if (!isLogic) {
        int num_dims = 0;
        temp_err_code = getVhExprBitSize(static_cast<vhExpr>(basetype), &char_bitsize, &num_dims, loc, context);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }
      }
    }

    if (isLogic)
    {
      // Check whether the string contains any x/z or not and create the
      // constant accordingly
      bool hasXZ = (str_val.find_first_of("xXzZwWuU-") != UtString::npos);
      if (hasXZ)
        *the_const = NUConst::createXZ(str_val, false, bit_width, loc);
      else
        *the_const = NUConst::createKnownConst(str_val,  bit_width,
                                               false/*source_is_string*/, loc);
    }
    else
    {
      // we have a string literal. Break it up into a concatenation of
      // ascii values
      NUExprVector concatExpr;
      
      if ( str_val.empty( ))
      {
        // deal with an empty string; create 1-element 0
        NUConst* charConst = NUConst::createKnownIntConst (0ULL, char_bitsize, true, loc);
        concatExpr.push_back(charConst);
      }
      else
      {
        UtString::const_iterator e = str_val.end();
        for (UtString::const_iterator p = str_val.begin(); p != e; ++p )
        {
          if(isCharacter)
          {
            UInt64 theChar = *p;
            NUConst* charConst = NUConst::createKnownIntConst (theChar, char_bitsize, true, loc);
            concatExpr.push_back(charConst);
          }
          else
          {
            vhNode elemTypeDef = vhGetTypeDef(elmType);
            JaguarList elem_list = vhGetEnumEleList(elemTypeDef);
            vhNode vh_next;
            unsigned int index = 0;
            while ((vh_next = vhGetNextItem(elem_list)) != NULL)
            {
               unsigned int val = vhGetCharEnumElement(static_cast<vhExpr> (vh_next));
               if(val == ((unsigned int) *p))
                 break;
               else
                 index++;
            }
            NUConst* enum_elem = NUConst::create (false, index, char_bitsize, loc);
            concatExpr.push_back(enum_elem);
          }
        }
      }
      
      UInt64 repeat_count = 1;      
      *the_const =  new NUConcatOp(concatExpr, repeat_count, loc);
      
    }
  }
  else
  {
    SInt32 signed_val = value;
    *the_const = NUConst::create( false, signed_val,  bit_width, loc);
  }
  if (not bit_width or not *the_const) {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unsupported constant"), &err_code);
    return err_code;
  }
  if (eVHSIGNED == getVhExprSignType(vh_expr))
    (*the_const)->setSignedResult(true);
  else
    (*the_const)->setSignedResult(false);

  if (*the_const)
    (*the_const)->resize ((*the_const)->determineBitSize ());

  return err_code;
}


// Function to process array aggregates. The array aggregates conform
// with the verilog concat operator and hence we will create NU concat
// expression from the aggregate expression.
Populate::ErrorCode
VhdlPopulate::aggregate(vhExpr vh_expr,
                        LFContext* context,
                        int lWidth,
                        NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  *the_expr = 0;
  SourceLocator loc = locator(vh_expr);
  LOC_ASSERT((vhGetObjType(vh_expr) == VHAGGREGATE), loc);

  if ( mCarbonContext->isNewRecord() && isRecordNet( vh_expr, context ))
    return recordAggregate( vh_expr, context, the_expr );

  NUExprVector expr_vector;
  vhExpr exprSize = vhGetExprSize(vh_expr);
  LOC_ASSERT(exprSize, loc);
  if (vhGetObjType(exprSize) == VHINDEXCONSTRAINT)
  {
    JaguarList rangeList(vhGetDiscRangeList(exprSize));
    exprSize = static_cast<vhExpr>(vhGetNextItem(rangeList));
  }
  if ((exprSize == NULL) /*|| (vhGetObjType(exprSize) != VHRANGE) */) {
     POPULATE_FAILURE(mMsgContext->JagInvalidType(&loc, gGetVhNodeDescription(exprSize),
                                                  "aggregate range"), &err_code);
    return err_code;
  }

  VhdlRangeInfo indexBounds (0,0);
  // Check for a variable width aggregate.  If it is variable, use the
  // size (width-1 downto 0), because we don't really care about the
  // actual bounds.
  int localWidth = 0;
  bool isVariableIndexed = false;
  temp_err_code = getVariableRangeWidth( exprSize, context, &localWidth, 
                                         &isVariableIndexed );
  if ( temp_err_code == eNotApplicable ) {
    // isVariableIndexed indicates what we should do in this case, fall through
  } else if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) { 
    return err_code;
  }
  if ( isVariableIndexed && localWidth > 0 )
  {
    indexBounds.putVector( localWidth - 1, 0 );
  }
  else
  {
    temp_err_code = getLeftAndRightBounds( exprSize, &indexBounds, context, false );
    // If we have width from lvalue, use it instead of failing.
    if ((temp_err_code != eSuccess) && (lWidth > 0))
    {
      indexBounds.putVector(lWidth-1, 0);
    }
    else
    {
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
      if ( !indexBounds.isValidMsbLsb( ))
      {
        POPULATE_FAILURE(mMsgContext->BadAggregateBounds( &loc ), &err_code);
        return err_code;
      }
    }
  }

  bool downto=false;
  if (indexBounds.getMsb (loc) > indexBounds.getLsb (loc))
  {
    indexBounds.swap ();
    downto = true;
  }

  //! Note that the index to the aggregate can be anywhere between msb 
  /*! and lsb (negative or positive), whereas the index to the expr_vector 
   *  is 0 to size, where size = lsb - msb + 1;  negative index with c/c++
   *  is causing trouble.
   *  use base to adjust the offset into the expr_vector.
   */
  const SInt32 size = indexBounds.getWidth(loc);
  LOC_ASSERT( size > 0, loc );

  // Ignore any sizing info passed in; use the size Jaguar tells us.
  int aggWidth, numDims, element_width = 0;

  // There are two options:
  // First, it's constant range - use getVhExprBitSize to get the bit size, then
  // calculate the element_width.
  // Second, it's variable range - we are using the whole length of the aggregate
  // and we don't need to know the size of element_width. The size calculated above 
  // is used later for aggregate creation.
  if(isVariableIndexed == false)
  {
    temp_err_code = getVhExprBitSize( vh_expr, &aggWidth, &numDims, loc, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    // if we are in a situation where we know the size of the LHS then
    // make sure it is consistent with the current expression
    element_width = aggWidth / size;
    LOC_ASSERT( element_width > 0, loc );
    LOC_ASSERT(( aggWidth % size ) == 0, loc );
  }


  // Set NULL to all the elements of the array. This is required
  // in order to process the 'OTHERS' clause in the choice of the
  // element association.
  expr_vector.resize(size);
  for ( int i = 0; i < size; i++)
  {
    expr_vector[i] = NULL;
  }

  JaguarList  vh_ele_list(vhGetEleAssocList(vh_expr));
  vhExpr element = NULL;
  bool foundOthers = false;
  int index = indexBounds.getMsb (loc);
  SInt32 base = indexBounds.getMsb(loc);
  while(( element = static_cast<vhExpr>( vhGetNextItem( vh_ele_list ))))
  {
    NUExpr* currentExpr = 0;
    vhExpr assoc_expr = element;
    vhObjType elem_type = vhGetObjType(element);
    switch (elem_type)
    {
    case VHELEMENTASS:
    {
      assoc_expr = vhGetExpr(element);
      JaguarList choice_l(vhGetChoiceList(element));
      vhNode choice;
      while ((choice = vhGetNextItem(choice_l))) 
      {
        choice = evaluateJaguarExpr(static_cast<vhExpr>(choice));
        switch(vhGetObjType(choice))
        {
        case VHDECLIT:
        case VHBASELIT:
        case VHIDENUMLIT:
        case VHCHARLIT:
        {
          temp_err_code = getVhExprValue(static_cast<vhExpr>(choice),
                                                   &index, NULL);
          if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
          {
            return temp_err_code;
          }
          if ( downto == true )
          {
            // If we have adjusted the indices into ascending order, we
            // need to re-adjust any literals here to prevent reversing
            // the aggregate's direction.
            index = indexBounds.getLsb (loc) + indexBounds.getMsb (loc) - index;
          }

          temp_err_code = expr( assoc_expr, context, element_width, &currentExpr );
          if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
          {
            return temp_err_code;
          }
          LOC_ASSERT(currentExpr, loc);
          currentExpr->resize (currentExpr->determineBitSize());
          expr_vector[index - base] = currentExpr;
          break;
        }
        case VHRANGE:
        {
          VhdlRangeInfo indxBounds (0,0);

          temp_err_code = getLeftAndRightBounds(choice, &indxBounds, context );
          if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
          {
            return temp_err_code;
          }

          if (indxBounds.getMsb (loc) > indxBounds.getLsb (loc))
            indxBounds.swap ();

          for (SInt32 tmp = indxBounds.getMsb (loc); tmp <= indxBounds.getLsb (loc); tmp++)
          {
            temp_err_code = expr( assoc_expr, context, element_width, &currentExpr );
            if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
            {
              return temp_err_code;
            }
            LOC_ASSERT(currentExpr, loc);
            currentExpr->resize (currentExpr->determineBitSize());
            SInt32 sub_range_index = tmp;
            if( downto == true )
            {
            // If we have adjusted the indices into ascending order, we
            // need to re-adjust any ranges here to prevent reversing
            // the aggregate's direction.
              sub_range_index = indexBounds.getLsb (loc) + indexBounds.getMsb (loc) - sub_range_index;
            }

            expr_vector[sub_range_index - base] = currentExpr;
          }
          break;
        }
        case VHOTHERS:
        {
          int emptyIndex;
          // Iterate over all the elements from the leftIndex to the right
          // index and assign the NU expr corresponding to this assoc_expr
          // to all the empty elements of the expr_vector . The OTHERS
          // clause corresponds to those empty elements.
          for (emptyIndex=0; emptyIndex < size; emptyIndex++)
          {
            if (NULL == expr_vector[emptyIndex])
            {
              temp_err_code = expr( assoc_expr, context, element_width, &currentExpr );
              if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
              {
                return temp_err_code;
              }
              LOC_ASSERT(currentExpr, loc);
              currentExpr->resize (currentExpr->determineBitSize());
              expr_vector[emptyIndex] = currentExpr;
            }
          }
          foundOthers = true;
          break;
        }
        default:
        {
          SourceLocator loc = locator(choice);
          POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, 
                                                          "Unsupported choice in the aggregate element association."),
                           &err_code);
          return err_code;
          break;
        }
        }
        if (foundOthers) 
        {
          break;
        }
      }
      break;
    }
    default:
    { 
      // A VHSLICENODE is a slice in a port map.  One of these knows how
      // big it is, and also we don't want to create a varsel.  I hate
      // special cases.
      if ( vhGetObjType( assoc_expr ) == VHSLICENODE )
        element_width = 0;
      temp_err_code = expr( assoc_expr, context, element_width, &currentExpr );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
        return temp_err_code;
      }
      LOC_ASSERT(currentExpr, loc);
      int temp_size = currentExpr->determineBitSize();
      if (( element_width != 0 ) && ( temp_size > element_width )) {
        ConstantRange range(element_width-1, 0);
        currentExpr = new NUVarselRvalue( currentExpr, range, loc );
      } else {
        currentExpr->resize( temp_size );
      }

      expr_vector[index - base] = currentExpr;
      index++;
      break;
    }
    }
    if (foundOthers)
    {
      break;
    }
  }
  NUExprVector actualAggrExpr;
  // At this point we have populated the expr_vector with NU
  // expressions.  However, if the range of the aggregate is like 5 to
  // 10, we will have first 5 elements( 0 to 4) NULL in the expr_vector
  // . We need to discard those initial NULLs, or any NULLs that are
  // following.  If we have slices instead of individual elements, we'll
  // have NULLs after all the non-nulls, so non-NULLs are OK anywhere.
  for (index=0; index < size; index++)
  {
    if ( expr_vector[index] != NULL )
      actualAggrExpr.push_back(expr_vector[index]);
  }
  UInt64 repeat_count = 1;
  *the_expr =  new NUConcatOp(actualAggrExpr, repeat_count, loc);
  if (eVHSIGNED == getVhExprSignType(vh_expr))
    (*the_expr)->setSignedResult(true);
  else
    (*the_expr)->setSignedResult(false);
  return err_code;
}


// This function will return true if the type mark of absolute basetype of
// this node is BIT or STD_ULOGIC from STD/IEEE packages.
//
bool
isPredefined3StateLogicOrBit(vhNode node)
{
  if (0 == node)
    return false;
  switch(vhGetObjType(node))
  {
  case VHSUBTYPEDECL:
  case VHTYPEDECL:
  {
    vhNode basetype = vhGetAbsoluteBaseType(node);
    JaguarString type_mark(vhGetName(basetype));
    vhNode n_package = vhGetMasterScope(basetype);
    if (!n_package || vhGetObjType(n_package) != VHPACKAGEDECL)
    {
      return false;
    }
    JaguarString package_name(vhGetPackName(n_package));
    JaguarString library_name(vhGetLibName(n_package));
    if (( strcasecmp(type_mark,"STD_ULOGIC") == 0 &&
          strcasecmp( package_name, "STD_LOGIC_1164" ) == 0 &&
          strcasecmp( library_name, "IEEE" ) == 0) ||
        ( ((strcasecmp(type_mark,"BIT") == 0) ||
            strcasecmp(type_mark,"BOOLEAN") == 0) &&
          strcasecmp( package_name, "STANDARD" ) == 0 &&
          strcasecmp( library_name, "STD" ) == 0))
    {
      return true;
    }
    break;
  }
  default :
    break;
  }
  return false;
}

// FUNCTION: getVhExprValue()
// INPUTS:   vh_expr   => A Jaguar node of type vhExpr
//           the_value => A reference to put the value
// DESCRIPTION: This function extracts the integer value
//              from the corresponding vh node
// AUTHOR: Kanai L. Ghosh
// LIMITATION : Add support for multilevel std_ulogic to 3 level
//              logic conversion
//              Add handling for X/Z values
//              This method is only valid for integral values; it will truncate reals
//
// will return eSuccess if eval was successful, eFailPopulate if this
// could not be converted to a value.
Populate::ErrorCode
VhdlPopulate::getVhExprValue(vhExpr vh_expr, 
                             int* the_value,
                             UtString* str_val)
{
  Populate::ErrorCode err_code = eSuccess;

  vh_expr = evaluateJaguarExpr(vh_expr);
  switch (vhGetObjType(vh_expr)) {
  case VHDECLIT :
    *the_value = (int)vhGetDecLitValue(vh_expr);
    break;
  case VHBASELIT :
    *the_value = (int)vhGetBaseLitValue(vh_expr);
    break;
  case VHCHARLIT :
  {
    vhNode expr_type = vhGetExpressionType(vh_expr);
    if (isPredefined3StateLogicOrBit(expr_type))
    {
      if (str_val)
	*str_val += vhGetCharVal(vh_expr);
      else
	*the_value = (int)vhGetCharLitValue(vh_expr);
    }
    else
    {
      *the_value = (int)vhGetCharLitValue(vh_expr);
    }
  }
  break;
  case VHIDENUMLIT :
  {
    *the_value = (int)vhGetIdEnumLitValue(vh_expr);
    vhNode type_decl =  vhGetActualObj(vhGetExpressionType(vh_expr));
    // Check if ENUM_ENCODING attribute provides the value.
    // If yes, *the_value acts as an index into the string vector which 
    // stores the values provided by ENUM_ENCODING
    if (str_val)
      getEncodedStringFromAttrib( str_val, static_cast<vhExpr>(type_decl), 
                               *the_value);
  }
  break;
  case VHPHYSICALLIT :
  {
    err_code = getVhExprValue(vhGetAbsLit(vh_expr),the_value, str_val);
    break;
  }
  case VHOBJECT :
  {
    //vhNode actual_obj = vhGetActualObj(vh_expr);
    vhExpr evald_expr = evaluateJaguarExpr(vh_expr);
    err_code = getVhExprValue(evald_expr,the_value, str_val);
    break;
  }
  case VHCONSTANT:
  {
    vhExpr initial_val;
    if (vhIsDeferred(vh_expr)) 
    {
      initial_val = vhGetDeferredConstantValue(vh_expr);
    }
    else 
    {
      initial_val = vhGetInitialValue(vh_expr);
    }
    // For uninitialized generics the actual will come as NULL. Report such
    // case as error.
    if(NULL == initial_val)
    {
      SourceLocator loc = locator(vh_expr);
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Constant object does not have initial value."), &err_code);
    }
    vhExpr evald_expr = evaluateJaguarExpr(initial_val);
    err_code = getVhExprValue(evald_expr,the_value, str_val);
    break;
  }
  case VHSTRING:
  {
    JaguarString str(vhGetString(vh_expr));
    *str_val = UtString(str);
    break; 
  }
  case VHBITSTRING:
  {
    SourceLocator loc = locator( vh_expr );
    VhBaseType spec = vhGetBaseSpec(vh_expr);
    switch( spec )
    {
    case VH_BINARY:
    {
      JaguarString str(vhGetStringVal(vh_expr));
      *str_val = UtString(str);
      break;
    }
    case VH_OCTAL:
    case VH_HEX:
    {
      JaguarString str(vhGetStringVal( vh_expr ));
      const char *ptr = str;
      str_val->clear();
      // Convert octal or hex strings to a binary string Assume that the
      // source string is valid VHDL syntax of 0-9, a-f, and _ only.
      while ( *ptr != '\0' )
      {
        // Add leading zero when converting char from hex
        if ( spec == VH_HEX && *ptr >= '0' && *ptr <= '7' )
        {
          *str_val += "0";
        }
        switch ( *ptr )
        {
        case '0':
          *str_val += "000";
          break;
        case '1':
          *str_val += "001";
          break;
        case '2':
          *str_val += "010";
          break;
        case '3':
          *str_val += "011";
          break;
        case '4':
          *str_val += "100";
          break;
        case '5':
          *str_val += "101";
          break;
        case '6':
          *str_val += "110";
          break;
        case '7':
          *str_val += "111";
          break;
        case '8':
          *str_val += "1000";
          break;
        case '9':
          *str_val += "1001";
          break;
        case 'a':
        case 'A':
          *str_val += "1010";
          break;
        case 'b':
        case 'B':
          *str_val += "1011";
          break;
        case 'c':
        case 'C':
          *str_val += "1100";
          break;
        case 'd':
        case 'D':
          *str_val += "1101";
          break;
        case 'e':
        case 'E':
          *str_val += "1110";
          break;
        case 'f':
        case 'F':
          *str_val += "1111";
          break;
        case '_':
          // Silently eat underscores
          break;
        default:
        {
          POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unexpected character in bit string literal" ), &err_code);
        }
        } // end character decode
        ptr++;
      } // end while loop
      break;
    } // end case VH_OCTAL || VH_HEX
    default:
    {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unsupported base specified for bit string literal"), &err_code);
    }
    break;
    }
    break;
  } 
  default :
  {
    vhExpr evald_expr = evaluateJaguarExpr(vh_expr, true, true);
    if ( evald_expr != NULL ){
      err_code = getVhExprValue(evald_expr, the_value, str_val);
    } else {
      err_code = eFailPopulate;
    }
    break;
  }
  }
  return err_code;
}


// Function to get the bit count to represent an integer value.
// if the value is signed, it is represented in [-2**N .. (2**N)-1].
// if it is  unsigned, it's represented in [0 ..(2**N)-1]
static int
sGetBitSize(int value, bool isSigned)
{
  int width = 1;
  
  if (0 == value%2)
    value++;
  if (value >= 2)
  {
    width = (int)(log((double)value)/log(2.0) + .5);
    if (value > pow(2.0,width))
      width++;
  }
  if (isSigned)
    width++;
  return width;
}

// We take max(abs(l_bound), abs(r_bound)) for num of Bits calculation. 
// but if bounds are signed and not symmetric, need an extra bit (consider "-4 to 4"
// which requires 4 bits, while "-4 to 3" only requires 3 bits.
static int sGetBitWidth(SInt32 r1, SInt32 r2)
{
  SInt32 msb = std::max(r1, r2);
  SInt32 lsb = std::min(r1, r2);

  int width = 0;

  // JDM: I don't like the policy of sizing nets to be the smallest number
  // of bits to fit the range.  However, the symptom I was seeing with
  // -newSizing has disappeared due to other changes.  For the moment,
  // I am keeping the minimal-sized range nets.
  // 
  // The issue was clarified in test/vhdl/beacon2, the file ARITH2.vhdl,
  // which puts a value of "14" in a 0:15 ranged net.  When we try to
  // make the size explicit, the DynBitVector will be resized to 4 bits
  // to match the net width, but the value will be interpreted as a
  // negative number.
  //
  // I also question the optimization merit of saving 3 bytes on the
  // net declaration and costing us a lot of complexity in the sizing
  // management, where we have to extend the net by 32-N bits in order
  // to do arithmetic on it, since VHDL requires that integer subranges
  // have arithmetic behavior that is identical to integers.
  //
  // The issue of whether we guard against illegal values is rather
  // weak in either case.  If the has made range 0..16, then we would
  // need 5 bits to represent that, meaning we can easily store illegal
  // values.  If the range is 0..15 then we can use 4 bits, and there
  // will be no illegal values, but in-range wrong answers in place
  // of the error message.
  //
  // So I don't think the current behavior is particularly correct
  // in any case.
  if (lsb == UtSINT32_MIN)
    width = 32;
  else if (msb <= 0)
    // The signed value is represented as [-2**N .. (2**N)-1].
    // For example,if N=2, the range is [-4 to 3]
    // That's why we need to add 1 to the min value.
    width = sGetBitSize (std::abs(lsb+1), true);
  else if (lsb >= 0)
    width = sGetBitSize (msb, false);
  else {
    // We should add 1 to the negative max range, because
    // the signed value is represented as [-2**N .. (2**N)-1].
    int maxValue = std::max (msb, (std::abs (lsb+1) - 1) );
    width = sGetBitSize (maxValue, true);
  }
  return width;
}

//! class to enforce that on entry to a function, we suppress jaguar
//! warnings, and on exit we undo that.
class VhdlSuppressWarnings {
public:
  //! Constructor
  VhdlSuppressWarnings(bool suppress, CarbonContext* cc):
    mSuppress(suppress),
    mCarbonContext(cc)
  {
    if (mSuppress) {
      mCarbonContext->pushSuppressJaguarWarnings();
    }
  }
  //! Destructor
  ~VhdlSuppressWarnings() {
    if (mSuppress) {
      mCarbonContext->popSuppressJaguarWarnings();
    }
  }
private:
  //! Indicates whether we are suppressing warnings or not
  bool mSuppress;
  //! The CarbonContext
  CarbonContext* mCarbonContext;
};


// Method to determine the width of the expression corresponding to a
// builtin/mapped operator/function call. For such functions, we will
// determine the width of the expression from the actual arg(, the value(s) 
// with which the function has been called).
Populate::ErrorCode
VhdlPopulate::getWidthOfBuiltinFunc(vhExpr vh_expr,
                                    VhdlRangeInfoVector &dimVector,
                                    const SourceLocator& loc, LFContext *context)
{
  ErrorCode err_code = eSuccess;
  vhBuiltInFuncOrMappedOperatorType functype = 
    getBuiltInFuncOrMappedOperatorType(vh_expr);
  JaguarList vh_arg_iter(vhGetAssocList(vh_expr));
  vhExpr vh_arg1 = static_cast<vhExpr>(vhGetNextItem(vh_arg_iter));
  vhExpr vh_arg2 = static_cast<vhExpr>(vhGetNextItem(vh_arg_iter)); 
  switch (functype)
  {
  case eVHSYN_S_TO_S:
  case eVHSYN_S_TO_I:
  case eVHSYN_U_TO_I:
  case eVHSYN_FEED_THRU:
  case eVHSYN_S_TO_U:
  case eVHSYN_AND:
  case eVHSYN_NAND:
  case eVHSYN_OR:
  case eVHSYN_NOR:
  case eVHSYN_XOR:
  case eVHSYN_XNOR: 
  case eVHSYN_SIGN_STAR:
  case eVHSYN_SIGN_MULT:
  case eVHSYN_UNS_MULT:
  case eVHSYN_UNS_STAR:
  case eVHSYN_NUMERIC_SIGN_DIV:
  case eVHSYN_NUMERIC_UNS_DIV:
  case eVHSYN_BINARY_MINUS:
  case eVHSYN_NUMERIC_BINARY_MINUS:
  case eVHSYN_NUMERIC_BINARY_PLUS:
  case eVHSYN_BINARY_PLUS:
  case eVHSYN_SHL:
  case eVHSYN_SHR:
  case eVHSYN_SHLA:
  case eVHSYN_SHRA:
  case eVHSYN_ROL:
  case eVHSYN_ROR:     
  case eVHSYN_MOD:     
  case eVHSYN_ABS:
  case eVHSYN_NUMERIC_UNARY_MINUS:
  case eVHSYN_NOT:
    err_code = getVhExprRange( vh_arg1, dimVector, loc, false, false, true, context );
    break;
  case eVHSYN_S_XT:
  case eVHSYN_RESIZE:
  case eVHSYN_U_TO_U:
  case eVHSYN_UNS_RESIZE:
  {
    int sizeValue = 0;
    if (vh_arg2)
    {
      vh_arg2 = evaluateJaguarExpr(vh_arg2, true, true);
    }
    if (vh_arg2)
    {
      err_code = getVhExprValue( vh_arg2, &sizeValue, NULL );
      if (eSuccess == err_code)
      {
        VhdlRangeInfo range;
        range.putVector(sizeValue-1, 0);
        dimVector.push_back( range );
      }
    }
    else
    {
      // The resize value is not known to us. So, let's continue
      // with the size of the original expression. BTW, the final
      // size will be set by the context size as implemented by Josh.
      err_code = getVhExprRange( vh_arg1, dimVector, loc, false, false, true, context );
    }
    break;
  }
  case eVHSYN_EQUAL:
  case eVHSYN_NOT_EQUAL:
  case eVHSYN_UNS_LT: 
  case eVHSYN_UNS_GT:         
  case eVHSYN_GT:          
  case eVHSYN_LT:
  case eVHSYN_UNS_GT_EQUAL:
  case eVHSYN_UNS_LT_EQUAL:
  case eVHSYN_GT_EQUAL:
  case eVHSYN_LT_EQUAL:
  case eVHSYN_REDUCT_XOR:
  case eVHSYN_REDUCT_AND:
  case eVHSYN_REDUCT_OR:      
  {
    VhdlRangeInfo range;
    range.putScalar();
    dimVector.push_back( range );
  }
  break;
  case eVHFLATTEN:
  case eVHSYN_NONE:
    err_code = eNotApplicable;
    break;
  }

  // Check if this function body is empty. Sometime designers put the body 
  // of builtin/mapped operators/functions inside the synthesis/translate
  // off/on region. Since we do not dump the off/on region, JAGUAR will not
  // be able to determine the width of such functions because the return
  // statement will be missing(, since it will be in the off/on region).
  // As we have calculated the width, we can create a dummy return statement
  // and hook that with this function so that jaguar can determine the width
  // of this function call for the rest of the flow.
  if (eSuccess == err_code && dimVector.back().isVector())
  {
    vhNode vh_func = vhGetMaster(vh_expr);
    vhNode vh_subprog = vhGetSubProgBody(vh_func);
    JaguarList returnList(vhGetReturnList(vh_subprog));
    if (returnList.size() != 0 )
    {
      // Make sure that the return in this list is not created by us earlier
      vhNode returnObj = vhGetNextItem(returnList);
      if ( VHOBJECT == vhGetObjType(returnObj) )
      {
        vhNode returnVar = vhGetActualObj(returnObj);
        if ( strncmp(vhGetName(returnVar), "$func_return", 12 ) != 0 )
        {
          // This return is not the dummy return created from here
          return err_code;
        }
        else
        {
          vhwDeleteFromList( returnList, returnObj );
        }
      }
      else
      {
        return err_code;
      }
    }
    vhNode typeDecl = vhGetExpressionType(vh_expr);
    vhNode vh_typedef = vhGetTypeDef(typeDecl);
    JaguarList indexTypeList(vhGetIndexSubTypeDefList(vh_typedef));
    vhNode indexType = vhGetNextItem(indexTypeList);
    indexType = vhGetType(indexType);
    int msb = dimVector.back().getMsb(loc);
    int lsb = dimVector.back().getLsb(loc);
    UtString msbStr;
    msbStr << msb;
    UtString lsbStr;
    lsbStr << lsb;
    // Set the jaguar create scope to this subprogram
    JaguarString fileName(vhGetSourceFileName( vh_subprog ) );
    vhwSetCreateScope( vh_subprog, fileName.get_nonconst());
    vhNode leftExpr = vhwCreateDecLit( const_cast<char*>(msbStr.c_str()),
                                       indexType );
    vhNode rightExpr = vhwCreateDecLit( const_cast<char*>(lsbStr.c_str()),
                                        indexType );
    vhDirType dirType = VH_TO;
    if (msb > lsb)
    {
      dirType = VH_DOWNTO;
    }
    vhNode subtypeInd = typeDecl;
    if (vhGetObjType(subtypeInd) != VHSUBTYPEIND)
    {
      vhNode range = vhwCreateRange(dirType, leftExpr, rightExpr);
      JaguarList indexList(vhwCreateNewList(VH_TRUE));
      vhwAppendToList(indexList,range);
      vhNode indexCons = vhwCreateIndexConstraint(indexList);
      subtypeInd = vhwCreateSubTypeInd(typeDecl, indexCons, VH_FALSE, NULL);
    }
    // Create a variable in this subprogram scope with the created subtype
    // indication. This subtype indication contains the proper constraint to
    // reflect the width we need down the flow.
    //rjc this was once VH_NOT_PORT, now VH_OUT
    vhNode var = vhwCreateVariable( "$func_return_", subtypeInd, NULL, VH_OUT, VH_FALSE );
    vhNode returnValue = vhwCreateVhObject( var );
    vhwAppendToList( returnList, returnValue );
  }
  return err_code;
}


// This method is a wrapper over getVhExprRange().  It is to be used
// only at places where all we care about is the outermost dimension of
// the object.  The common case for this is when there is only one
// dimension.
Populate::ErrorCode
VhdlPopulate::getVhExprWidth(vhExpr vh_expr, 
                             int *the_width,
                             const SourceLocator& loc,
                             LFContext *context,
                             bool isLvalue,
                             bool reportError)
{
  VhdlSuppressWarnings suppress( !reportError, mCarbonContext );
  VhdlRangeInfoVector dimVector;
  dimVector.reserve( 3 );
  ErrorCode err_code = getVhExprRange( vh_expr, dimVector, loc,
                                       false, isLvalue, reportError, context );
  *the_width = dimVector.empty()?1:dimVector[0].getWidth(loc);
  return err_code;
}


// This function is an abstraction over getVhExprRange() and is similar
// to getVhExprWidth().  The difference is that it will return the total
// bit size of the object, not simply the outermost dimension.
Populate::ErrorCode
VhdlPopulate::getVhExprBitSize(vhExpr vh_expr, int *the_size,
                               int *num_dims, const SourceLocator& loc, 
                               LFContext *context, bool isLvalue, bool reportError)
{
  VhdlSuppressWarnings suppress(!reportError, mCarbonContext);
  VhdlRangeInfoVector dimVector;
  dimVector.reserve( 3 );
  ErrorCode err_code = getVhExprRange(vh_expr, dimVector, loc,
                                      false, isLvalue, reportError, context );
  *num_dims = dimVector.size();
  if (err_code == eSuccess)
  {
    vhNode vh_typedef;
    // If it's a record we need the base record size.  If it's not, the base size is 1.
    if ( mCarbonContext->isNewRecord() && isRecordNet( vh_expr, context ))
    {
      getVhdlRecordType( vh_expr, &vh_typedef );
      *the_size = getVhdlRecordSize( vh_typedef, context );
    }
    else
    {
      *the_size = 1;
    }
    for ( int i = 0; i < *num_dims; ++i )
    {
      (*the_size) *= dimVector[i].getWidth(loc);
    }
  }

  return err_code;
}

// Check if a subtype indicator (or its subtypedefs) has an index or range
// constraint (recursion is required for tests like test/vhdl/lang_memory/bug3543g.vhdl)
// The check for range constraints was added due to bug 7578.
static bool sCheckForIndexOrRangeConstraint (vhNode subtype)
{
  if (subtype == 0)
    return false;

  vhNode vh_range = vhGetConstraint (subtype);
  if (not vh_range) {
    vhNode retType = vhGetType(subtype);
    
    if ( vhGetObjType(retType) == VHSUBTYPEDECL ) {
      return sCheckForIndexOrRangeConstraint(vhGetSubTypeInd(retType));
    }

    if ( vhGetObjType(retType) != VHTYPEDECL ) {
      return false;
    }

    vhNode retTypeDef = vhGetTypeDef(retType);

    vhObjType objType = vhGetObjType(retTypeDef);
    
    if (objType == VHCONSARRAY) {
      vh_range = vhGetIndexConstr(retTypeDef);
    }
    else if (objType == VHINTTYPE) {
      vh_range = vhGetRangeConstraint(retTypeDef);
    }
    else {
      return false;
    }
  }

  vh_range = vhElaborateSizeNode (vh_range);
  if (NULL == vh_range)
    return false;

  vhObjType rangeObjType = vhGetObjType(vh_range);

  if (rangeObjType != VHINDEXCONSTRAINT && rangeObjType != VHRANGECONSTRAINT) {
    return false;
  }

  return true;
}

// Determine if the currentReturnStmt is larger than the largestReturnStmt.
// If it is, then it becomes the largest return statement.
// If for some reason it can't determine the size of a return statement, error out.
Populate::ErrorCode
VhdlPopulate::compareReturnStmts(vhNode func_body, vhNode* largestReturnStmt)
{
  ErrorCode temp_err_code, err_code = eSuccess;

  JaguarList returnList( vhGetReturnList( func_body ) );

  *largestReturnStmt = vhGetNextItem( returnList );

  if ( *largestReturnStmt == NULL ) 
  {
    temp_err_code = eNotApplicable;
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) {
      return err_code;
    }
  }

  vhExpr initialSize = vhGetExprSize(static_cast<vhExpr>( *largestReturnStmt ) );

  if (initialSize == NULL) 
  {
    // Handle this as a special case. If the function only has one return statement
    // and the initialSize is NULL, check the type to see if its an unconstrained
    // array. If it is, there is nothing we can do at this point. Just let the code
    // continue. If its anything other than a unconstrained array, error out with an
    // eFatal.
    vhNode type = vhGetExpressionType(static_cast<vhExpr>( *largestReturnStmt ));
    if ( isUnconstrainedArray( type ) && returnList.size() == 1 ) 
    {
      temp_err_code = eNotApplicable;
      if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) {
	return err_code;
      }
    }
    else 
    {
      temp_err_code = eFatal;
      if (errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ) )
      {
	SourceLocator loc = locator( *largestReturnStmt );
	UtString name;
	getVhdlObjName( func_body, &name );
	UtString expr;
	decompileJaguarNode(*largestReturnStmt, &expr);
	// Couldn't determine the size of a return statement
	mMsgContext->CannotDetermineReturnSizeOfFunction( &loc, name.c_str(), expr.c_str() );
	return err_code;
      }
    }
  }

  vhNode initialRange = vhGetNextItem( vhGetDiscRangeList( initialSize ) );
  vhExpr initialLtExpr = NULL;
  double initialLtVal  = 0;
  vhExpr initialRtExpr = NULL;
  double initialRtVal  = 0;

  if ( vhGetObjType( initialRange ) == VHRANGE )
  {
    initialLtExpr = vhGetLtOfRange( initialRange );
    initialLtVal = vhGetNumericValue( initialLtExpr );
    initialRtExpr = vhGetRtOfRange( initialRange );
    initialRtVal = vhGetNumericValue( initialRtExpr );
  }
  else 
  {
    temp_err_code = eFatal;
    if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ) )
    {
      SourceLocator loc = locator( *largestReturnStmt );
      UtString name;
      getVhdlObjName( func_body, &name );
      UtString expr;
      decompileJaguarNode(*largestReturnStmt, &expr);
      // Couldn't determine the size of a return statement
      mMsgContext->CannotDetermineReturnSizeOfFunction( &loc, name.c_str(), expr.c_str() );
      return err_code;
    }
  }

  vhNode currentReturnStmt;

  while (( currentReturnStmt = vhGetNextItem( returnList ) ))
  {
    vhExpr currentSize = vhGetExprSize( static_cast<vhExpr>( currentReturnStmt ) );
    if (currentSize == NULL)
    {
      temp_err_code = eFatal;
      if (errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ) )
	{
	  SourceLocator loc = locator( currentReturnStmt );
	  UtString name;
	  getVhdlObjName( func_body, &name );
	  UtString expr;
	  decompileJaguarNode(currentReturnStmt, &expr);
	  // Couldn't determine the size of a return statement
	  mMsgContext->CannotDetermineReturnSizeOfFunction( &loc, name.c_str(), expr.c_str() );
	  return err_code;
	}
    }

    vhNode currentRange = vhGetNextItem( vhGetDiscRangeList( currentSize ) );
    vhExpr currentLtExpr = NULL;
    double currentLtVal  = 0;
    vhExpr currentRtExpr = NULL;
    double currentRtVal  = 0;

    if ( vhGetObjType( currentRange ) == VHRANGE)
    {
      currentLtExpr = vhGetLtOfRange( currentRange );
      currentLtVal = vhGetNumericValue( currentLtExpr );
      currentRtExpr = vhGetRtOfRange( currentRange );
      currentRtVal = vhGetNumericValue( currentRtExpr );
    }
    else 
    {
      temp_err_code = eFatal;
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ) )
      {
	SourceLocator loc = locator( currentReturnStmt );
	UtString name;
	getVhdlObjName( func_body, &name );
	UtString expr;
	decompileJaguarNode(currentReturnStmt, &expr);
	// Couldn't determine the size of a return statement
	mMsgContext->CannotDetermineReturnSizeOfFunction( &loc, name.c_str(), expr.c_str() );
	return err_code;
      }
    }
    
    if ( fabs( initialLtVal - initialRtVal ) < fabs( currentLtVal - currentRtVal ) )
    {
      *largestReturnStmt = currentReturnStmt;
    }
  }

  return err_code;
}

// Compute the return type range of a function by examining return statements
// in the elaborated function body and computing their sizes. We can't assume
// all the return statements are of the same size, so check them all and pick
// the largest one.
Populate::ErrorCode
VhdlPopulate::getFunctionReturnRange(vhNode func_body, VhdlRangeInfoVector& dimVector,
                                     const SourceLocator& loc, bool isIndexConstraint,
                                     bool isLvalue, bool reportError, LFContext* context)
{
  ErrorCode temp_err_code, err_code = eSuccess;

  vhNode largestReturnStmt = NULL;

  temp_err_code = compareReturnStmts(func_body, &largestReturnStmt);

  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code) ||
      errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
  {
    return err_code;
  }

  vhExpr vh_exprsize = vhGetExprSize(static_cast<vhExpr>(largestReturnStmt));
  vhNode vh_type = vhGetExpressionType(static_cast<vhExpr>(largestReturnStmt));

  if (vh_exprsize != NULL)
  {
    temp_err_code = getUnconstrainedArrayRange(vh_type, vh_exprsize, dimVector,
                                               loc, isIndexConstraint, isLvalue,
                                               reportError, context);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return err_code;
    }
  }

  return err_code;
}

/* Get the range bounds for a Jaguar expression, all possible dimensions. */
Populate::ErrorCode
VhdlPopulate::getVhExprRange(vhExpr vh_expr, VhdlRangeInfoVector &dimVector,
                             const SourceLocator& loc, 
                             bool isIndexConstraint, bool isLvalue, bool reportError,
                             LFContext *context)
{
  VhdlRangeInfo range;
  ErrorCode err_code = eSuccess, temp_err_code = eSuccess;
  vh_expr = evaluateJaguarExpr(vh_expr);
  vhObjType expr_type = vhGetObjType(vh_expr);
  switch(expr_type)
  {
  case VHENUMTYPE:
  {
    vhNode range = vhGetRangeConstraint(vh_expr);
    temp_err_code = getVhExprRange( static_cast<vhExpr>(range), dimVector, loc,
                                    isIndexConstraint, isLvalue, reportError, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    if (! isIndexConstraint) {
      // Since element values are '0' to 'numOfElems - 1', we need to know 
      // width required to store the largest value (ie numOfElems - 1).
      int the_width = sGetBitSize(dimVector.back().getWidth(loc) - 1, false /* unsigned*/);
      // Always represent as a vector, even if the width is 1.
      // Otherwise an array of enums will be populated as a vector
      // instead of a memory.
      dimVector.back().putVector(the_width - 1, 0);
    }
    break;
  }
  break;
  case VHCONSTANT:
  {
    // For aliases and interface constants, use the defined range, not
    // the range from the initial value.
    if ( !isAliasObject( vh_expr ) )
    {
      if (vhIsGeneric(vh_expr))
      {
        vhNode subtype = vhGetSubTypeInd(vh_expr);
        err_code = getVhExprRange( static_cast<vhExpr>(subtype), dimVector, loc,
                                   isIndexConstraint, isLvalue, reportError, context );
        // Detects an empty string initializer for generic.
        if ((dimVector.size() == 2) && dimVector[0].isNullRange()) {
          vhNode subtype_type = vhGetType(subtype);
          JaguarString type_mark(vhGetName(subtype_type));
          if (strcasecmp(type_mark, "STRING") == 0) { // If this is a string
            dimVector.erase(dimVector.begin());
          }
        }
        break;
      }

      // Only use the range from the initial expression if we don't have a
      // declared range (index or range constraint) and the constant is not a 
      // function/procedure argument. See bug 7578 for more information.
      // Allow unconstrained array function/procedure arguments to also
      // use initial expression for range. For example, an integer function
      // argument should always be 32 bits wide, irrespective of the size of value passed
      // to the function. However an unconstrained std_logic_vector size should
      // be dependant on the size of value passed to the function.
      vhNode subType = vhGetSubTypeInd(vh_expr);
      if ( ( false == sCheckForIndexOrRangeConstraint(subType) ) &&
           ( VH_FALSE == vhIsSubProgInterfaceConstant(vh_expr) || isUnconstrainedArray(subType) ) ) {
        vhExpr initial_val;
	if ( vhIsDeferred( vh_expr ) )
	{
	  initial_val = vhGetDeferredConstantValue(vh_expr);
	}
	else
	{
	  initial_val = vhGetInitialValue(vh_expr);
	}
        if ( initial_val )
        {
          err_code = getVhExprRange( initial_val, dimVector, loc, 
                                     isIndexConstraint, isLvalue, reportError, context );
        }
        break;
      }
    }
    // Deliberate fallthrough after handling the special cases of unconstrained constant
    // initialized with a constrained initial value.
  }
  case VHVARIABLE:
  case VHSIGNAL:
  case VHALIASDECL:
  case VHELEMENTDECL:
  {
    vhNode subtype;
    if ( isLineVariable( vh_expr ))
    {
      range.putVector( 31, 0 );
      dimVector.push_back( range );
      break;
    }
    if ( expr_type == VHELEMENTDECL )
    {
      subtype = vhGetEleSubTypeInd( vh_expr );
    }
    else
    {
      subtype = vhGetSubTypeInd(vh_expr);
    }

    vhNode retType = vhGetType(subtype);
    // When retType is std_logic, vhGetConstraint will return the enum bounds
    // like 'U' to '-'. This is not a range so avoid considering constraint as range.
    vhNode vh_range = NULL;
    if (!isPredefined3StateLogicOrBit(retType)) {
      vh_range = vhGetConstraint(subtype);
      vh_range = vhElaborateSizeNode( vh_range );
    }
      
    if (vh_range)
    {
      temp_err_code = getVhExprRange( static_cast<vhExpr>(vh_range), dimVector,
                                      loc, isIndexConstraint, isLvalue, reportError,
                                      context );
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      if (! vhIsA(vh_range, VHINDEXCONSTRAINT)) 
      {
        return err_code;
      }
      // Recurse into the typedef, for arrays of arrays
      vhNode retTypeDef = vhGetTypeDef( retType );
      vhExpr elem_subtype_ind = static_cast<vhExpr>( vhGetEleSubTypeInd( retTypeDef ));
      // Detect empty string. A string with null range is an empty string.
      // Remove null range from dimension vector, to make string of 1 character size.
      if ((dimVector.size() > 0) && dimVector.back().isNullRange()) {
        JaguarString type_mark(vhGetName(retType));
        if (strcasecmp(type_mark, "STRING") == 0) { // If this is a string.
          dimVector.pop_back();
        }
      }
          
      err_code = getVhExprRange( elem_subtype_ind, dimVector, loc,
                                 isIndexConstraint, isLvalue, reportError,
                                 context );
    }
    else 
    {
      err_code = getVhExprRange( static_cast<vhExpr>(retType), dimVector,
                                 loc, isIndexConstraint, isLvalue, reportError,
                                 context );
    }
    break;
  }
  case VHOBJECT:
  {
    vhExpr actual_obj = static_cast<vhExpr>( vhGetActualObj( vh_expr ));
    err_code = getVhExprRange( actual_obj, dimVector, loc, isIndexConstraint,
                               isLvalue, reportError, context );
    break;
  }
  case VHCHARLIT:
  case VHIDENUMLIT:
  case VHINDNAME:
  case VHDECLIT:
  case VHBASELIT:
  {
    vhNode vh_type = vhGetExpressionType(vh_expr);
    if (( expr_type == VHCHARLIT || expr_type == VHINDNAME ) && 
        isPredefined3StateLogicOrBit( vh_type ))
    {
      range.putScalar();
      dimVector.push_back( range );
    }
    else if ( expr_type == VHDECLIT && vhIsReal( vh_expr ) == VH_TRUE )
    {
      range.putVector( 63, 0 );
      dimVector.push_back( range );
    }
    else if (! isUnconstrainedArray(vh_type) )
    {
      temp_err_code = getVhExprRange( static_cast<vhExpr>(vh_type), dimVector,
                                      loc, isIndexConstraint, isLvalue, reportError,
                                      context );
      
    }
    else
    {
      vhExpr vh_exprsize =  vhGetExprSize( vh_expr );
      temp_err_code = getUnconstrainedArrayRange( vh_type, vh_exprsize, dimVector,
                                                  loc, isIndexConstraint, isLvalue,
                                                  reportError, context );
    }
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    break;
  }
  case VHFUNCCALL:
  {
    vhNode vh_type = vhGetExpressionType( vh_expr );
    if ( isUnconstrainedArray( vh_type ))
    {
      vhExpr vh_exprsize = vhGetExprSize(vh_expr);
      // This can happen if the body of the function is within the
      // translate/synthesis off/on region. Check if it is a
      // builtin/mapped operator/function because we can determine the
      // width of such function calls.
      temp_err_code = getWidthOfBuiltinFunc( vh_expr, dimVector, loc, context );
      // If we still don't know the function return expression size, 
      // get the list of return types from the function body. Pick one of
      // them to determine the size of function return type.
      if (vh_exprsize != 0)
      {
        temp_err_code = getUnconstrainedArrayRange(vh_type, vh_exprsize, dimVector,
                                                   loc, isIndexConstraint, isLvalue,
                                                   reportError, context);
      }
      else if (temp_err_code == eNotApplicable)
      {
        vhNode elab_subprog = vhOpenSubProgram(vh_expr);
        vhNode subprog_body = vhGetSubProgBody(elab_subprog);
        temp_err_code = getFunctionReturnRange(subprog_body, dimVector, loc,
                                               isIndexConstraint, isLvalue,
                                               reportError, context);
        vhCloseFeature();
      }
    }
    else
    {
      temp_err_code = getVhExprRange( static_cast<vhExpr>(vh_type), dimVector,
                                      loc, isIndexConstraint, isLvalue, reportError,
                                      context );
    }

    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    break;
  }
  case VHUNARY:
  {
    vhNode vh_type = vhGetExpressionType( vh_expr );
    if ( isUnconstrainedArray( vh_type ))
    {
      vhNode vh_exprsize = vhGetExprSize( vh_expr );
      // If Jaguar cannot evaluate the size of unary expression as a whole, get
      // the size of it's only operand, which should be the same as the size of
      // unary expression.
      vhOpType opType = vhGetOpType(vh_expr);
      if ((vh_exprsize == NULL) &&
          ((opType == VH_UPLUS_OP) || (opType == VH_UMINUS_OP) ||
           (opType == VH_NOT_OP) || (opType == VH_ABS_OP)))
      {
        vhExpr vh_operand = vhGetOperand(vh_expr);
        temp_err_code = getVhExprRange(vh_operand, dimVector, loc, isIndexConstraint,
                                       isLvalue, reportError, context);
      }
      else
      {
        vhNode vh_exprsize = vhGetExprSize( vh_expr );
        temp_err_code = getUnconstrainedArrayRange(vh_type, vh_exprsize, dimVector,
                                                   loc, isIndexConstraint, isLvalue,
                                                   reportError, context);
      }
    }
    else
    {
      temp_err_code = getVhExprRange( static_cast<vhExpr>( vh_type ), dimVector,
                                      loc, isIndexConstraint, isLvalue, reportError,
                                      context );
    }
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ))
      return temp_err_code;
    break;
  }
  case VHBINARY:
  {
    vhNode vh_type = vhGetExpressionType( vh_expr );
    if ( isUnconstrainedArray( vh_type ))
    {
      vhNode vh_exprsize = vhGetExprSize(vh_expr);
      if (( vhGetOpType( vh_expr ) == VH_PLUS_OP || 
            vhGetOpType( vh_expr ) == VH_MINUS_OP ) &&
          vh_exprsize == NULL )
      {
        // VHDL sizing rules for add and subtract as defined inside
        // std_logic_arith are quite involved.  This code implements them
        // correctly.
        vhExpr vh_left = vhGetLeftOperand( vh_expr );
        vhExpr vh_right = vhGetRightOperand( vh_expr );
        bool leftint = isIntegerType(vhGetExpressionType(vh_left));
        bool rightint = isIntegerType(vhGetExpressionType(vh_right));
        int lwidth, rwidth;
        temp_err_code = getVhExprWidth( vh_left, &lwidth, loc, context,
                                        isLvalue, reportError );
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code )) {
          return temp_err_code;
        }
        temp_err_code = getVhExprWidth( vh_right, &rwidth, loc, context,
                                        isLvalue, reportError );
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code )) {
          return temp_err_code;
        }
        int width;
        if ( leftint != rightint )
        {
          if ( leftint )
          {
            width = rwidth;
            if ( getVhExprSignType(vh_right) != eVHSIGNED )
              width++;
          }
          else
          {
            width = lwidth;
            if ( getVhExprSignType(vh_left) != eVHSIGNED )
              width++;
          }
        }
        else
        {
          width = std::max( lwidth, rwidth );
        }
        range.putWidth( width );
        dimVector.push_back( range );
      }
      else if((vhGetOpType( vh_expr ) == VH_CONCAT_OP) &&
              vh_exprsize == NULL)
      {
        vhExpr vh_left = vhGetLeftOperand( vh_expr );
        vhExpr vh_right = vhGetRightOperand( vh_expr );
        int lwidth, rwidth;
        temp_err_code = getVhExprWidth( vh_left, &lwidth, loc, context,
                                        isLvalue, reportError );
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code )) {
          return temp_err_code;
        }
        temp_err_code = getVhExprWidth( vh_right, &rwidth, loc, context,
                                        isLvalue, reportError );
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code )) {
          return temp_err_code;
        }

        int width = lwidth + rwidth;
        range.putWidth( width );
        dimVector.push_back( range );
      }
      else
      {
        // unconstrained array, not + or -
        temp_err_code = getUnconstrainedArrayRange( vh_type, vh_exprsize, dimVector,
                                                    loc, isIndexConstraint, isLvalue,
                                                    reportError, context );
      }
    }
    else
    {
      temp_err_code = getVhExprRange( static_cast<vhExpr>(vh_type), dimVector,
                                      loc, isIndexConstraint, isLvalue, reportError,
                                      context );
    }
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ))
      return temp_err_code;
    break;
  }
  case VHQEXPR:
  case VHTYPECONV:
  {
    vhExpr actual_expr = vhGetOperand(vh_expr);
    err_code = getVhExprRange( actual_expr, dimVector, loc, isIndexConstraint,
                           isLvalue, reportError, context );
    break;
  }
  case VHTYPEDECL:
  case VHSUBTYPEDECL:
  {
    if (isPredefined3StateLogicOrBit(vh_expr))
    {
      if ( isIndexConstraint ){
        // should use vhenumtype
        vhExpr vh_typedef = static_cast<vhExpr>( vhGetTypeDef( vh_expr ));
        err_code = getVhExprRange( vh_typedef, dimVector, loc, isIndexConstraint,
                                   isLvalue, reportError, context );
      } else{
        range.putScalar();
        dimVector.push_back( range );
      }
      break;
    }
    if ( !strcasecmp( vhGetName( vhGetAbsoluteBaseType( vh_expr )), "REAL" ))
    {
      // real types are always 64 bits wide
      range.putVector( 63, 0 );
      dimVector.push_back( range );
      break;
    }
    vhNode subTypeInd = 0;
    if (vhGetObjType(vh_expr) == VHSUBTYPEDECL)
    {
      subTypeInd = vhGetSubTypeInd(vh_expr);
    }
    if (subTypeInd)
    {
      vhNode vh_range = vhGetConstraint(subTypeInd);
      if (vh_range)
      {
        err_code = getVhExprRange( static_cast<vhExpr>(vh_range), dimVector, loc,
                                   isIndexConstraint, isLvalue, reportError,
                                   context );

        // Need to get the size of the constraint's element
        vhNode vh_typedef = vhGetTypeDef(vh_expr);
        vhObjType vh_constraint = vhGetObjType (vh_typedef);
        switch (vh_constraint) {
        case VHUNCONSARRAY:
          vh_expr = static_cast<vhExpr>(vhGetEleSubTypeInd (vh_typedef));
          err_code = getVhExprRange (vh_expr, dimVector, loc,
                                     isIndexConstraint, isLvalue, reportError,
                                     context );
          break;

        case VHRANGE:
          vh_expr = static_cast<vhExpr>(vh_typedef);
          err_code = getVhExprRange (vh_expr, dimVector, loc,
                                     isIndexConstraint, isLvalue, reportError,
                                     context );
          break;

        case VHINTTYPE:
        case VHENUMTYPE:
        case VHPHYSICALTYPE:
          break;

        default:
          LOC_ASSERT ("Unexpected array bounds syntax"==0, loc);
          break;
        }
        break;
      }
      vhExpr vh_type = static_cast<vhExpr>( vhGetOrgType( subTypeInd ));
      err_code = getVhExprRange( vh_type, dimVector, loc, isIndexConstraint, isLvalue,
                                 reportError, context );
      break;
    }
    vhNode vh_typedef = vhGetTypeDef(vh_expr);
    if (vhIsA(vh_typedef, VHENUMTYPE)) 
    {
      // Check if width can be found from ENUM_ENCODING attribute
      int width;
      if ( getWidthFromAttribute( vh_expr, &width )) 
      {
        // Always represent as a vector, even if the width is 1.
        // Otherwise an array of enums will be populated as a vector
        // instead of a memory.
        range.putVector(width - 1, 0);
        dimVector.push_back( range );
        break;
      }
    }
    err_code = getVhExprRange( static_cast<vhExpr>(vh_typedef), dimVector,
                               loc, isIndexConstraint, isLvalue, reportError,
                               context );
    break;
  }
  case VHCONSARRAY:
  {
    // Get the index constraint for the array size.
    vhExpr index_constraint = static_cast<vhExpr>(vhGetIndexConstr(vh_expr));
    temp_err_code = getVhExprRange(index_constraint, dimVector, loc,
                                   isIndexConstraint, isLvalue, reportError,
                                   context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    // Get the element type for array element width
    vhExpr ele_type = static_cast<vhExpr>( vhGetEleSubTypeInd( vh_expr ));
    temp_err_code = getVhExprRange( ele_type, dimVector, loc, isIndexConstraint,
                                    isLvalue, reportError, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    break;
  }
  case VHBITSTRING:
  case VHSTRING:
  {
    vhExpr vh_range = vhGetExprSize(vh_expr);
    if (vh_range)
    {
      temp_err_code = getVhExprRange( vh_range, dimVector, loc, isIndexConstraint,
                                      isLvalue, reportError, context );
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }

      // Add the range of character as another dimension of string. String is
      // represented as a memory.
      if (expr_type == VHSTRING) {
        vhNode typ = vhGetExpressionType(vh_expr);
        vhNode typDef = vhGetTypeDef(typ);
        vhExpr elem_subtype = static_cast<vhExpr>(vhGetEleSubTypeInd(typDef));
        // A string with null range is an empty string. Remove the null range
        // from dimension vector, to make string of 1 character size.
        if ((dimVector.size() > 0) && dimVector.back().isNullRange())
          dimVector.pop_back();
        temp_err_code = getVhExprRange(elem_subtype, dimVector, loc, isIndexConstraint,
                                       isLvalue, reportError, context);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }
      }
    }
    else
    {
      vhNode expr_type = vhGetExpressionType(vh_expr);
      err_code = getVhExprRange( static_cast<vhExpr>(expr_type), dimVector, loc,
                                 isIndexConstraint, isLvalue, reportError, context );
    }
    break;
  }
  case VHINDEXCONSTRAINT:
  {
    JaguarList rangeList(vhGetDiscRangeList(vh_expr));
    vhNode vh_range;
    while (( vh_range = vhGetNextItem( rangeList )))
    {
      temp_err_code = getVhExprRange( static_cast<vhExpr>(vh_range), dimVector,
                                      loc, true, isLvalue, reportError, context );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
        return temp_err_code;
      }
    }
    break;
  }
  case VHSUBTYPEIND:
  {
    vhNode vh_range = vhGetConstraint( vh_expr );
    if (vh_range)
    {
      temp_err_code = getVhExprRange( static_cast<vhExpr>(vh_range), dimVector, loc,
                                      isIndexConstraint, isLvalue, reportError, context );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
        return temp_err_code;
      }
      vhNode vh_typedef = vhGetTypeDef( vh_expr );
      if ( vhGetObjType( vh_typedef ) == VHUNCONSARRAY ||
           vhGetObjType( vh_typedef ) == VHCONSARRAY )
      {
        vhExpr ele_type = static_cast<vhExpr>( vhGetEleSubTypeInd( vh_typedef ));
        temp_err_code = getVhExprRange( ele_type, dimVector, loc, isIndexConstraint,
                                        isLvalue, reportError, context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
          return temp_err_code;
        }
      }
    }
    else
    {
      vhExpr vh_type = static_cast<vhExpr>( vhGetType( vh_expr ));
      err_code = getVhExprRange( vh_type, dimVector, loc, isIndexConstraint,
                                 isLvalue, reportError, context );
    }
    break;
  }
  case VHRANGE:
  {
    // Look for non-constant range expressions
    int localWidth = 0;
    bool isVariableIndexed = false;
    temp_err_code = getVariableRangeWidth( vh_expr, NULL, &localWidth, &isVariableIndexed );

    if ( temp_err_code == eSuccess && isVariableIndexed && localWidth > 0 )
    {
      // The range is variable
      range.putVector( localWidth - 1, 0 );
    }
    else
    {
      // The range might be constant
      temp_err_code = getLeftAndRightBounds(vh_expr, &range, context, reportError );
    }

    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return err_code;
    }

    dimVector.push_back( range );
    break;
  }
  case VHRANGECONSTRAINT:
  {
    vhNode vh_range = vhGetRange(vh_expr);
    temp_err_code = getLeftAndRightBounds(vh_range, &range, context, reportError );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return temp_err_code;
    }
    if (isIndexConstraint)
    {
      dimVector.push_back( range );
    }
    else
    {
      if (mConstraintRange == NULL) 
	mConstraintRange = new ConstantRange(range.getMsb(loc), range.getLsb(loc));
      else
	*mConstraintRange = ConstantRange(range.getMsb(loc), range.getLsb(loc));
      
      int width = sGetBitWidth(range.getMsb(loc), range.getLsb(loc));
      LOC_ASSERT(width > 0, loc);

      range.putVector(width - 1, 0);
      dimVector.push_back( range );
    }
    break;
  }
  case VHINTTYPE:
  {
    vhNode range_cons = vhGetRangeConstraint(vh_expr);
    if (range_cons)
    {
      err_code = getVhExprRange( static_cast<vhExpr>(range_cons), dimVector,
                                 loc, isIndexConstraint, isLvalue, reportError,
                                 context );
    }
    else
    {
      // Unconstrained INTEGER, so it's width will be 32 bit per IEEE 1076.6
      range.putVector(31, 0);
      dimVector.push_back( range );
    }
    break;
  }
  case VHSLICENAME:
  {
    // First, get the bounds of the entire net
    VhdlRangeInfoVector prefixVector;
    vhExpr vh_prefix = static_cast<vhExpr>(vhGetPrefix( vh_expr ));
    temp_err_code = getVhExprRange( vh_prefix, prefixVector, loc, 
                                    isIndexConstraint, isLvalue, reportError,
                                    context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    // Now examine the slice selected from the prefix
    bool isVariableIndexPartSel = false;
    int width;
    temp_err_code = getVariableRangeWidth( static_cast<vhNode>(vh_expr), context,
                                           &width, &isVariableIndexPartSel);
    if ( temp_err_code == eNotApplicable)
    {
      // isVariableIndexPartSel indicates what we should do in this case, fall through
    } else if ( isVariableIndexPartSel && 
                errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return err_code;
    }
    if ( isVariableIndexPartSel )
    {
      // Attempt to use the variable's constant propagated value before failing.
      vhNode vh_range = vhGetDisRange(vh_expr);
      vhNode actual_range = NULL;
      temp_err_code = getVhRangeNode(vh_range, &actual_range, NULL);
      if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) {
        return temp_err_code;
      }
      temp_err_code = getLeftAndRightBounds(actual_range, &range, context, false);
      if (temp_err_code == eSuccess)
      {
        dimVector.push_back(range);
      }
      else
      {
        range.putWidth( width );
        if ( width == 0 )
        {
          // We can't even represent a 0 width in a VhdlRangeInfo
          err_code = eFailPopulate;
        }
        dimVector.push_back( range );
      }
    }
    else
    {
      vhExpr exprSize = vhGetExprSize(vh_expr);
      if (exprSize)
      {
        temp_err_code = getVhExprRange( exprSize, dimVector, loc,
                                        isIndexConstraint, isLvalue, reportError,
                                        context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
      }
      else
      {
        vhExpr range = static_cast<vhExpr>( vhGetDisRange( vh_expr ));
        temp_err_code = getVhExprRange( range, dimVector, loc,
                                        isIndexConstraint, isLvalue, reportError, context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
      }
    }

    // Now the first range will be correct for the slice range.  We need
    // to return any other dimensions from the full net, though.  This
    // is certainly incorrect for 3D objects.  TJM
    if ( prefixVector.size() > 1 && prefixVector[1].isValid() )
    {
      LOC_ASSERT( dimVector.size() == 1, loc );
      dimVector.push_back( prefixVector[1] );
    }
    break;
  }
  case VHSELECTEDNAME: 
  {
    if ( isPredefined3StateLogicOrBit( vhGetExpressionType( vh_expr )))
    {
      range.putScalar();
      dimVector.push_back( range );
    }
    else
    {
      vhExpr vh_expr_size = vhGetExprSize( vh_expr );
      if ( NULL == vh_expr_size)
      {
        vhExpr actual = static_cast<vhExpr>(vhGetActualObj( vh_expr ));
        err_code = getVhExprRange( actual, dimVector, loc,
                                   isIndexConstraint, isLvalue, reportError, context );
      }
      else
      { 
        temp_err_code = getVhExprRange( vh_expr_size, dimVector, loc,
                                        isIndexConstraint, isLvalue, reportError,
                                        context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
        vhNode expr_type = vhGetExpressionType( vh_expr );
        vhNode type_def = vhGetTypeDef( expr_type );
        vhObjType type_def_obj_type = vhGetObjType(type_def);
        if ( type_def_obj_type == VHUNCONSARRAY ||
             type_def_obj_type == VHCONSARRAY )
        {
          // Get the range/size information for the array element
          VhdlRangeInfoVector arangeVector;
          vhExpr ele_type = static_cast<vhExpr>( vhGetEleSubTypeInd( type_def ));
          temp_err_code = getVhExprRange( ele_type, arangeVector, loc,
                                          isIndexConstraint, isLvalue, reportError,
                                          context );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            return temp_err_code;
          LOC_ASSERT( !dimVector.empty() && dimVector[0].isValid( ), loc );
          if ( !arangeVector.empty() && arangeVector[0].isValid() && arangeVector[0].getWidth(loc) != 1 )
          {
            dimVector.insert( dimVector.end(), arangeVector.begin(), arangeVector.end( ));
          }
        }

        if (type_def_obj_type == VHINTTYPE)
        {
          // Unfortunately, getVhExprRange is not consistent. If the expr passed
          // is type of VHRANGE, it returns the range.
          // For example, for  integer it will return -2147483648E0 to 2147483647E0.
          // But if the expr passed is of VHRANGECONSTRAINT, it returns bit width.
          // For example, for integer range 0 to 1000, it will return 10.
          if(vhGetObjType(vh_expr_size) == VHRANGE)
          {
            VhdlRangeInfo& ri = dimVector.back();
            dimVector.pop_back();
            int width = sGetBitWidth(ri.getMsb(loc), ri.getLsb(loc));
            range.putVector(width - 1, 0);
            dimVector.push_back(range);
          }
        }
      }
    }
    break;
  }
  case VHAGGREGATE:
  {
    vhNode vh_expr_type = vhGetExpressionType(vh_expr);
    vhNode vh_typedef = vhGetTypeDef(vh_expr_type);
    if ( VHRECORD == vhGetObjType(vh_typedef) ) 
    {
      err_code =  getVhExprRange( static_cast<vhExpr>( vh_typedef ), dimVector, loc,
                                  isIndexConstraint, isLvalue, reportError, context );
    }
    else
    {
      if ( vhGetObjType( vh_typedef ) == VHUNCONSARRAY )
      {
        // Unconstrained arrays have their outer dimensions in the size node
        vhExpr exprSize = vhGetExprSize(vh_expr);

	// Make sure that the expression size is not NULL.
	LOC_ASSERT(exprSize, loc);
	
	temp_err_code = getVhExprRange( exprSize, dimVector, loc, isIndexConstraint,
					isLvalue, reportError, context );
	if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
	  return temp_err_code;
	// After dealing with the size info, get the element ranges
	vhExpr vh_elesubtype = static_cast<vhExpr>( vhGetEleSubTypeInd( vh_typedef ));
	err_code = getVhExprRange( vh_elesubtype, dimVector, loc, isIndexConstraint,
				     isLvalue, reportError, context );
      }
      else
      {
        err_code = getVhExprRange( static_cast<vhExpr>( vh_typedef ), dimVector, loc,
                                   isIndexConstraint, isLvalue, reportError, context );
      }
    }
    break;
  }
  case VHFORINDEX:
  {
    // We should be getting VHFORINDEX only from for_loop parameter spec
    // Since the range of for_index belongs to only of integer type, lets
    // keep its size to that of integer.
    // TBD : We can optimize this to reduce the size, so that it contains
    //       only the allowed range constraint values. But since it is being
    //       used only as for loop index and the generated expressions in 
    //       ::forStmt() are anyway resized to 32 bits, lets keep it 32.
    range.putVector(31, 0);
    dimVector.push_back( range );
    break;
  }
  case VHRECORD:
  {
    // For the new record implementation, a record does not have a range, so just break from the case.
    if ( !mCarbonContext->isNewRecord( ))
    {
      // For a record we return a single range of the total size of the record.
      UInt32 width = getVhdlRecordSize( vh_expr, context );
      range.putWidth( width );
      dimVector.push_back( range );
    }
    break;
  }
  case VHELEMENTASS:
  {
    err_code = getVhExprRange( static_cast<vhExpr>( vhGetExpr( vh_expr )),
                               dimVector, loc, isIndexConstraint,
                               isLvalue, reportError, context );
    break;
  }
  case VHATTRBNAME:
  {
    vhAttrbType atr_type = vhGetAttrbType( vh_expr );
    switch (atr_type)
    {
    case VH_RANGE:
    case VH_REVERSE_RANGE:
    case VH_LEFT:
    case VH_RIGHT:
    case VH_HIGH:
    case VH_LOW:
    case VH_LENGTH:
    {
      // Determine the range (x downto y) or (y to x) for the attribute
      // prefix and add it to dimension vector.
      vhNode node = vhGetPrefix( vh_expr );
      err_code = getVhExprRange( static_cast<vhExpr>( node ), dimVector, loc,
                                 false, isLvalue, reportError, context ); // TJM5450
      if ( VH_REVERSE_RANGE == atr_type )
      {
        dimVector.back().swap(); // TJM5450
      }
      break;
    }
    default:
      break;
    } // switch
    break;
  }
  case VHEXTERNAL:
    err_code = getVhExprRange( static_cast<vhExpr>( vhGetActualObj( vh_expr )),
                               dimVector, loc, isIndexConstraint, isLvalue,
                               reportError, context );
    break;
  case VHFLOATTYPE:
    range.putVector( 63, 0 );
    dimVector.push_back( range );
    break;

  case VHFILEDECL: {
    range.putVector( 31, 0 ); // file variables are 32 bits long
    dimVector.push_back( range );
    break;
  }
  case VHUNCONSARRAY:
    err_code = eNotApplicable;
    break;
  case VHACCESSTYPE:
    err_code = eFatal;
    mMsgContext->AccessTypeUnsupported( &loc );
    break;
  default:
  {
    // We will not report error physical type data because we ignore those data
    if (reportError && VHPHYSICALTYPE != expr_type ) {
      mMsgContext->UnsupportedDataType(&loc, gGetVhNodeDescription(vh_expr));
    }
    range.putScalar();
    dimVector.push_back( range );
    return eFailPopulate;
    break;
  }
  }

  // If the final dimension detected is a scalar, get rid of it.
  if ( dimVector.size() > 0 )
  {
    if ( !dimVector.back().isVector() )
    {
      dimVector.pop_back();
    }
  }
  return err_code;
}

Populate::ErrorCode
VhdlPopulate::getUnconstrainedArrayRange( vhNode vh_type,
                                          vhNode vh_exprsize,
                                          VhdlRangeInfoVector &dimVector,
                                          const SourceLocator &loc, 
                                          bool isIndexConstraint, bool isLvalue,
                                          bool reportError, LFContext *context )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  temp_err_code = getVhExprRange( static_cast<vhExpr>( vh_exprsize ), dimVector,
                                  loc, isIndexConstraint, isLvalue, reportError,
                                  context );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;
  
  VhObjType typeType = vhGetObjType( vh_type );
  LOC_ASSERT( vhGetObjType( vh_exprsize ) == VHINDEXCONSTRAINT &&
              ( typeType == VHTYPEDECL || typeType == VHSUBTYPEDECL ), loc );
  if ( typeType == VHSUBTYPEDECL )
  {
    vh_type = vhGetAbsoluteBaseType( vh_type );
  }
  vhNode vh_typedef = vhGetTypeDef( vh_type );
  vhNode vh_eletype = vhGetEleSubTypeInd( vh_typedef );
  temp_err_code = getVhExprRange( static_cast<vhExpr>( vh_eletype ), dimVector,
                                  loc, isIndexConstraint, isLvalue, reportError,
                                  context );
  return err_code;
}


// Get the ENUM_ENCODING attribute spec if specified on this enum_decl object
vhExpr
getEnumEncodingAttrib(vhExpr obj)
{
  vhNode scope = vhGetMasterScope(obj);

  JaguarList allAttList(vhGetAttrbSpecList(scope));
  vhNode att = NULL;
  while ((att = vhGetNextItem(allAttList)) != NULL)
  {
    JaguarString att_name(vhGetName(att));
    if (!strcasecmp(att_name,"ENUM_ENCODING"))
    {
      JaguarList entDesigList(vhGetEntityDesigList(att));
      vhNode entDesig = NULL;
      while ((entDesig = vhGetNextItem(entDesigList)) != NULL)
      {
        JaguarList declList(vhGetDeclItemList(entDesig));
        vhNode decl = NULL;
        while ((decl = vhGetNextItem(declList)) != NULL)
        {
          if (decl == obj) {
            return static_cast<vhExpr>(att);
          }
        }
      }
    }
  }

  return NULL;
}


// Get the width of the enum_type object from the string specified through
// ENUM_ENCODING attribute.
bool
VhdlPopulate::getWidthFromAttribute(vhExpr vh_expr,
                                    int *the_width)
{
  vhNode enum_enco_att = vhGetAttribSpecOnObject("ENUM_ENCODING", vh_expr);
  if ( !enum_enco_att) 
  {
    // No ENUM_ENCODING attribute specified for this ENUMTYPE
    return false;
  }
  // lookup EnumEncoding values if it has already been visited earlier
  EnumEncodingMap::const_iterator p = mEnumMap.find(vh_expr);
  if ( p != mEnumMap.end() )
  {
    StrVec  enum_enco_arr = p->second;
    if ( enum_enco_arr.empty() )
    { 
      return false;
    }
    else
    {
      // Calulate the width from the first value of enum_encoding.
      UtString  fVal = enum_enco_arr[0];
      *the_width = fVal.size();
      return true;
    }
  }
  
    
  // vhExpr enum_enco_att = getEnumEncodingAttrib(vh_expr);
  vhExpr string_lit = vhGetAttribValue(static_cast<vhExpr>(enum_enco_att));
  if (!vhIsA(string_lit,VHSTRING))
  {
    return false;
  }
  StrVec  enum_encode_values;
  UtString enum_encod_str(vhGetStringVal(string_lit));
  UtString::iterator i = enum_encod_str.begin();
  UtString::iterator s = enum_encod_str.begin();
  while ( i != enum_encod_str.end() )
  {
    while (*i == ' ' ) 
    { 
      i++;
      s++;
    }
    if (i == enum_encod_str.end() )
    {
      break;
    }
    while (*i != ' ' && i != enum_encod_str.end() ) i++;
      
    UtString value;
    value.assign(s, i);
    enum_encode_values.push_back(value);
      
    if ( i == enum_encod_str.end() )
    {
      break;
    }
    else
    {
      s = i;
    }
  }
  // Now Map the enum_encode_value to enum_decl
  mEnumMap[vh_expr] = enum_encode_values;
  *the_width = enum_encode_values[0].size();
  return true;
}


// Get the string value corresponding to index element in an enum type from
// its ENUM_ENCODING attribute string.
bool
VhdlPopulate::getEncodedStringFromAttrib(UtString*           str_val,
                                         vhExpr              vh_expr,
                                         unsigned long       index)
{
  // lookup EnumEncoding values
  EnumEncodingMap::const_iterator p = mEnumMap.find(vh_expr);
  if ( p != mEnumMap.end() )
  {
    // Check if the value array is of size zero. This will signify that there
    // there is no enum_encoding defined for this enum_decl. return
    StrVec  enum_enco_arr = p->second;
    if (enum_enco_arr.size() > index)
    {
      // Get the string from the string vector through index
      *str_val = enum_enco_arr[index];
    }
  }
  // no enum_encoding found. Default encoding (positional value of enum)
  // will be used.
  else {
      JaguarList enumElelist = vhGetEnumEleList(vhGetTypeDef(vh_expr));
      if(enumElelist) {
	  int enumElelistLength = enumElelist.size();
	  // compute minimum bitwidth for the enum
	  unsigned int bitwidth = (unsigned int)ceil((log(enumElelistLength)/log(2)));
	  // simple int to binary string conversion
	  str_val->resize(bitwidth);
	  char *buf = str_val->getBuffer();
	  for(unsigned int i=0; i < bitwidth; i++) {
	      if( 0x1u & index ) 
		  buf[bitwidth - i - 1] = '1';
	      else
		  buf[bitwidth - i - 1] = '0';
	      index = index >> 1;
	  }
      }
  }
  return true;
}


// Function that determines the SIGN of an expression. The criteria of
// considering an expresion as signed is available in the file
// doc/functional-specs/VHDL/vhdl-functional-spec.txt.
VhdlPopulate::SignType
VhdlPopulate::getVhExprSignType(vhExpr vh_expr)
{
  ErrorCode err_code = eSuccess;
  ExprSignMap::iterator p = mExprSignMap.find(vh_expr);
  if (p != mExprSignMap.end()) {
    return p->second;
  }
  SourceLocator loc = locator(vh_expr);
  SignType resultSign = eVHUNSIGNED;

  vhObjType expr_type = vhGetObjType(vh_expr);
  switch (expr_type)
  {
  case VHCHARLIT:
  case VHIDENUMLIT:
  case VHACCESSTYPE: // LINE 
  case VHRECORD:     // Records are composites, and as such unsigned
  case VHFILEDECL:
    resultSign = eVHUNSIGNED;
    break;
  case VHDECLIT:
  case VHBASELIT:
  case VHPHYSICALLIT:
    resultSign = getVhExprSignType ( static_cast<vhExpr>( vhGetExpressionType ( vh_expr)));
    break;
  case VHOBJECT:
    resultSign = getVhExprSignType( static_cast<vhExpr>( vhGetActualObj( vh_expr )));
    break;
  case VHBINARY:
  {
    vhOpType op = vhGetOpType(vh_expr);
    SignType ltSign = getVhExprSignType(vhGetLeftOperand(vh_expr));
    SignType rtSign = getVhExprSignType(vhGetRightOperand(vh_expr));
    // The sign of an relational binary expression is always unsigned
    switch(op)
    {
    case VH_EQ_OP:
    case VH_NEQ_OP:
    case VH_GTH_OP:
    case VH_LTH_OP:
    case VH_GEQ_OP:
    case VH_LEQ_OP:
      resultSign = eVHUNSIGNED;
      break;
    case VH_SLL_OP: // The sign of the shift/rotate expr is the
    case VH_SRL_OP: // sign of the left operand.
    case VH_SLA_OP:
    case VH_SRA_OP:
    case VH_ROL_OP:
    case VH_ROR_OP:
      resultSign = ltSign;
      break;
    case VH_REM_OP:
      resultSign = ltSign;
      break;
    case VH_MOD_OP:
      resultSign = rtSign;
      break;
    case VH_EXPONENT_OP:
    case VH_ABS_OP:
      resultSign = ltSign;
      break;
    default:
    {
      if (ltSign == eVHSIGNED && rtSign == eVHSIGNED)
      {
        resultSign = eVHSIGNED;
      }
      else
      {
        vhNode expr_type = vhGetExpressionType(vh_expr);
        resultSign = getVhExprSignType(static_cast<vhExpr>(expr_type));
      }
      break; // Break for default choice
    }
    }
    break; // Break for the binary  expression
  } 
  case VHSLICENAME:
    resultSign = getVhExprSignType(vhGetPrefix(vh_expr));
    break;
  case VHSELECTEDNAME:
  {
    vhNode exprType = vhGetExpressionType(vh_expr);
    resultSign = getVhExprSignType(static_cast<vhExpr>(exprType));
    break;
  }
  case VHUNARY:
  {
    vhOpType op = vhGetOpType(vh_expr);
    // The unary plus and minus are always signed.
    switch(op)
    {
    case VH_UPLUS_OP:
    case VH_UMINUS_OP:
      resultSign = eVHSIGNED;
      break;
    case VH_ABS_OP:
    {
      // There are two overloaded operators named
      // IEEE.STD_LOGIC_ARITH."ABS".  One of them returns a SLV, not an
      // object of the same type as the operand.  In this case the ABS
      // operator is unsigned, not the sign of the operand.
      vhNode vh_type = vhGetExpressionType( vh_expr );
      vhString type_name = vhGetName( vh_type );
      if ( !strcasecmp( type_name, "STD_LOGIC_VECTOR" ))
        resultSign = eVHUNSIGNED;
      else
        resultSign = getVhExprSignType(vhGetOperand(vh_expr));
      break;
    }
    default:
      resultSign = getVhExprSignType(vhGetOperand(vh_expr));
      break;
    }
    break;
  }
  case VHSIGNAL:
  case VHVARIABLE:
  case VHCONSTANT:
  {
    vhNode subtype = vhGetSubTypeInd(vh_expr);
    vhNode range_cons = vhGetConstraint(subtype);
    vhNode type = vhGetType(subtype);
    vhNode absBase = vhGetAbsoluteBaseType(type);
    if ( sTypeIsSIGNED( absBase ))
    {
      resultSign = eVHSIGNED;
    }
    else
    {
      if (0 == range_cons && vhGetObjType(type) == VHSUBTYPEDECL)
      {
        vhNode subtypeind = vhGetSubTypeInd(type);
        range_cons = vhGetConstraint(subtypeind);
      }
      if (range_cons && VHRANGECONSTRAINT == vhGetObjType(range_cons))
      {
        resultSign = getVhExprSignType(static_cast<vhExpr>(range_cons));
      }
      else
      {
        resultSign = getVhExprSignType(static_cast<vhExpr>(type));
      }
    }
    break;
  }
  case VHRANGECONSTRAINT:
  {
    vhNode range = vhGetRange(vh_expr);
    VhdlRangeInfo bounds (0,0);

    ErrorCode tmp_err = getLeftAndRightBounds(range, &bounds, NULL);
    if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err, &err_code ) )
    {
      return eVHUNKNOWN;
    }
    if (bounds.getMsb (loc) < 0 || bounds.getLsb (loc) < 0)
    {
      resultSign = eVHSIGNED;
    }
    break;
  }
  case VHSUBTYPEDECL:
    resultSign = getVhExprSignType( static_cast<vhExpr>(
                                      vhGetSubTypeInd( vh_expr )));
    break;
  case VHPHYSICALTYPE:
    resultSign = getVhExprSignType ( static_cast<vhExpr>( vhGetRangeConstraint ( vh_expr )));
    break;
  case VHTYPEDECL:
  {
    vhNode vh_typedef = vhGetTypeDef(vh_expr);
    vhObjType obj_type = vhGetObjType(vh_typedef);
    if (VHINTTYPE == obj_type)
    {
      vhNode range_cons = vhGetRangeConstraint(vh_typedef);
      if (range_cons)
      {
        resultSign = getVhExprSignType(static_cast<vhExpr>(range_cons));
      }
      else
      {
        resultSign = getVhExprSignType(static_cast<vhExpr>(vh_typedef));
      }
    }
    else if (VHENUMTYPE == obj_type)
    {
      resultSign = eVHUNSIGNED;
    }
    else if ( sTypeIsSIGNED( vh_expr ))
    {
      resultSign = eVHSIGNED;
    }
    else
    {
      resultSign = getVhExprSignType(static_cast<vhExpr>(vh_typedef));
    }
    break;
  }
  case VHSUBTYPEIND:
  {
    vhNode vh_type = vhGetType(vh_expr);
    resultSign = getVhExprSignType(static_cast<vhExpr>( vh_type ));
    JaguarString type_name(vhGetName( vh_type ));
    if ( !strcasecmp( type_name, "INTEGER" ))
    {
      // If we have an integer type, but are constrained to unsigned
      // values, we are unsigned.  Fix for bug3880.
      vhNode vh_constraint = vhGetConstraint( vh_expr );
      if ( vh_constraint != NULL )
        resultSign = getVhExprSignType( static_cast<vhExpr>( vh_constraint ));
    }
    break;
  }
  case VHINDNAME:
    resultSign = getVhExprSignType(static_cast<vhExpr>(
                                                vhGetExpressionType(vh_expr)));
    break;
  case VHCONSARRAY:
  case VHUNCONSARRAY:
  {
    // Get the element type for array element width
    vhNode elem_subtype_ind = vhGetEleSubTypeInd(vh_expr);
    resultSign = getVhExprSignType(static_cast<vhExpr>(elem_subtype_ind));
    break;
  }
  case VHFUNCCALL:
  case VHTYPECONV:
  case VHQEXPR:
  case VHAGGREGATE:
  case VHATTRBNAME:
  case VHSTRING:
  case VHBITSTRING:
    resultSign = getVhExprSignType(static_cast<vhExpr>(
                                                vhGetExpressionType(vh_expr)));
    break;
  case VHFORINDEX:
    // for-index is signed when iterating over an integer range, otherwise
    // it would be unsigned when iterating over an enumeration.
    resultSign = getVhExprSignType (static_cast<vhExpr>(
                                      vhGetDisRange (vh_expr)));
    break;
  case VHELEMENTDECL:
    resultSign = getVhExprSignType( static_cast<vhExpr>( vhGetType( 
                                    vhGetEleSubTypeInd( vh_expr ))));
    break;
  case VHRANGE:
  {
    // This is a range constraint on a type; if either bound is signed
    // (i.e. is negative) consider the range signed.  If they are both
    // unsigned, return unsigned.  This is for bug3380, where we were
    // reporting that the NATURAL subtype was a signed value.
    vhNode vh_left = vhGetLtOfRange( vh_expr );
    vhNode vh_right = vhGetRtOfRange( vh_expr );
    SignType leftSign = getVhExprSignType( static_cast<vhExpr>( vh_left ));
    SignType rightSign = getVhExprSignType( static_cast<vhExpr>( vh_right ));
    if ( leftSign == eVHSIGNED || rightSign == eVHSIGNED )
      resultSign = eVHSIGNED;
    break;
  }
  case VHINDEXCONSTRAINT:
  {
    JaguarList rangeList(vhGetDiscRangeList(vh_expr));
    vhNode vh_range = vhGetNextItem(rangeList);
    resultSign = getVhExprSignType( static_cast<vhExpr>( vh_range ));
    break;
  }
  case VHALIASDECL:
    resultSign = getVhExprSignType( static_cast<vhExpr>( vhGetSubTypeInd( vh_expr )));
    break;
  case VHFLOATTYPE:
    resultSign = eVHSIGNED;
    break;
  default:
    mMsgContext->UnExpectedObjectType(&loc,
                                      gGetVhNodeDescription(vh_expr),
                                      " - Unsupported expression for sign detection.");
    break;
  } // switch
  mExprSignMap[vh_expr] = resultSign;
  return resultSign;
}


Populate::ErrorCode
VhdlPopulate::evaluateStaticExpr( vhExpr vh_expr, SInt32 *value, LFContext *context,
                                  bool reportError )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  NUExpr *constVal;
  vhExpr vh_node = vh_expr;
  if ( vhGetObjType( vh_expr ) == VHOBJECT )
  {
    vh_node = static_cast<vhExpr>( vhGetActualObj( vh_expr ));
  }
  if ( vhGetObjType( vh_node ) == VHCONSTANT )
  {
    if ( vhIsDeferred( vh_node ) )
    {
      vh_node = vhGetDeferredConstantValue( vh_node );
    }
    else 
    {
      vh_node = vhGetInitialValue( vh_node );
    }
    if ( vhGetObjType( vh_node ) == VHOBJECT )
    {
      vh_node = static_cast<vhExpr>( vhGetActualObj( vh_node ));
    }
  }
  if (vh_node == NULL)
  {
    if (reportError) {
      const SourceLocator loc = locator( vh_expr );
      UtString msg("Cannot determine static value of expression \"" );
      UtString dummy;
      msg << decompileJaguarNode( vh_expr, &dummy ) << "\".";
      mMsgContext->JaguarConsistency( &loc, msg.c_str());
    }
    return eFailPopulate;
  }
  // Presume the expression is static and attempt to recurse down the
  // intializers down to literals to construct and expression of literals.
  // Such expressions can be successfully folded to a constant value.
  temp_err_code = expr( vh_node, context, 0, &constVal, true /*propageRvalue*/,
                        reportError);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
    if (reportError) {
      const SourceLocator loc = locator( vh_expr );
      mMsgContext->JagInvalidType( &loc, gGetVhNodeDescription( vh_node ),
                                   "constant expression" );
    }
    return temp_err_code;
  }

  constVal->resize( constVal->determineBitSize() );
  NUExpr *folded = mFold->fold( constVal );
  NUConst *constExpr;
  if ( folded && ( constExpr = folded->castConst( )))
  {
    bool retval = constExpr->getL( value );
    NU_ASSERT( retval, constExpr );
  }
  else
  {
    if (reportError) {
      const SourceLocator loc = locator( vh_expr );
      mMsgContext->JagInvalidType( &loc, gGetVhNodeDescription( vh_node ),
                                   "constant expression" );
    }
    err_code = eFailPopulate;
  }
  if ( folded != NULL )
    delete folded;
  else
    delete constVal;

  return err_code;
}


// Function to calculate the left and right bounds of a VHRANGE object.
Populate::ErrorCode 
VhdlPopulate::getLeftAndRightBounds(vhNode vh_range, VhdlRangeInfo* bounds,
                                    LFContext *context, bool reportError,
                                    bool checkStaticness)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  SourceLocator loc = locator(vh_range);
  vhAttrbType attribType = VH_ERROR_ATTRBTYPE;
  vhNode actual_range = NULL;
  // VHRANGE object could be of attribute type or non-attribute i.e index
  // constraint range type. - bug6787
  if ((vhGetObjType(vh_range) == VHRANGE) &&
      (vhIsRangeAttribute(vh_range) == VH_TRUE))
  {
    vh_range = vhGetRangeAttrib(vh_range);
  }

  if (vhGetObjType(vh_range) == VHATTRBNAME)
  {
    attribType = vhGetAttrbType( vh_range );
    if ( VH_RANGE == attribType || VH_REVERSE_RANGE == attribType)
    {
      vhNode node = vhGetPrefix( static_cast<vhExpr>(vh_range ));
      temp_err_code = getVhRangeNode(node, &actual_range, NULL);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      vh_range = actual_range;
    }
  }
  if (vh_range && vhGetObjType(vh_range) != VHRANGE)
  {
    actual_range = NULL;
    temp_err_code = getVhRangeNode(vh_range, &actual_range, NULL);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    vh_range = actual_range;
  }
  if (NULL == vh_range || VHRANGE != vhGetObjType(vh_range))
  {
    POPULATE_FAILURE(mMsgContext->JagInvalidType(&loc, gGetVhNodeDescription(vh_range),
                                                 "getBounds"),  &err_code);
    return err_code;
  }

  vhExpr vh_left_bound = vhGetLtOfRange(vh_range);
  vhExpr vh_right_bound = vhGetRtOfRange(vh_range);
  SInt32 ltBound, rtBound;

  // Try to statically evaluate the left side of the range. When populating
  // declarations evaluate expressions even when they're not static. In
  // declaration scope, there's no chance of variable/signal values being modified.
  // So their initial values can be used by declaration sizing/initializing.
  vhExpr evald_val;
  evald_val = evaluateJaguarExpr( vh_left_bound, true,
                                  checkStaticness && !mIsAggressiveEvalEna );
  if ( evald_val )
  {
    temp_err_code = getVhExprValue( evald_val, &ltBound, NULL );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return temp_err_code;
    }
  }
  else
  {
    temp_err_code = evaluateStaticExpr( vh_left_bound, &ltBound, context,
                                        reportError);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return temp_err_code;
    }
  }


  // Try to statically evaluate the right side of the range. When populating
  // declarations evaluate expressions even when they're not static. In
  // declaration scope, there's no chance of variable/signal values being modified.
  // So their initial values can be used by declaration sizing/initializing.
  evald_val = evaluateJaguarExpr( vh_right_bound, true,
                                  checkStaticness && !mIsAggressiveEvalEna );
  if ( evald_val )
  {
    temp_err_code = getVhExprValue( evald_val, &rtBound, NULL );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return temp_err_code;
    }
  }
  else
  {
    temp_err_code = evaluateStaticExpr( vh_right_bound, &rtBound, context,
                                        reportError);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      return temp_err_code;
    }
  }

  vhDirType dir = vhGetRangeDir( vh_range );
  bool isNullRange = false;
  if (( dir == VH_TO && ltBound > rtBound ) ||
      ( dir == VH_DOWNTO && ltBound < rtBound ))
  {
    // This is a null range! Don't mark it as invalid. Instead fix it to
    // match the direction by reversing them, and let the compile continue.
    // Since this is a synthesizable VHDL, the null range code is not expected
    // to be executed anyway.
    bounds->putVector(rtBound, ltBound); // Deliberately populated swapped.
    isNullRange = true; // Indicate that this is a null range.
  }
  else
  {
    bounds->putVector( ltBound, rtBound );
    if ( VH_REVERSE_RANGE == attribType )
      bounds->swap();
  }
  bounds->putNullRange(isNullRange);

  return err_code;
}


// This function takes an expression and parses it recursively till
// it hits any dynamic object. If it finds any dynamic object, it will
// set that object into vh_dyna_obj_decl so that caller can work on it.
// It will return true if the input expression contains any dynamic object.
// This function has been implemented in order to support variable index 
// part-select . Here the term 'dynamic object' means any non-static object
// (, i.e. variable, signal) and loop index .
static bool 
sExprContainsDynamicObject( vhExpr vh_expr, vhNode &vh_dyn_obj )
{
  bool retval = false;
  if ( vh_dyn_obj )
  {
    // We have already found a dynamic object. So, don't parse the expression
    // further.
    return true;
  }
  switch ( vhGetObjType( vh_expr ))
  {
  case VHINDNAME:
  {
    JaguarList exprlist( vhGetExprList( vh_expr ));
    vhExpr vh_idx = static_cast<vhExpr>( vhGetNextItem( exprlist ));
    while ( vh_idx != NULL )
    {
      retval |= sExprContainsDynamicObject( vh_idx, vh_dyn_obj );
      vh_idx = static_cast<vhExpr>( vhGetNextItem( exprlist ));
    }
    retval |= sExprContainsDynamicObject( vhGetPrefix( vh_expr ), vh_dyn_obj );
    break;
  }
  case VHOBJECT:
  {
    vhNode vh_actual_obj = vhGetActualObj( vh_expr );
    switch ( vhGetObjType( vh_actual_obj ))
    {
    case VHFORINDEX:
    case VHSIGNAL:
    case VHVARIABLE:
      vh_dyn_obj = vh_actual_obj;
      return true;
      break;
    case VHCONSTANT:
      if (vhIsSubProgInterfaceConstant(vh_actual_obj)) {
        vh_dyn_obj = vh_actual_obj;
        return true;
        break;
      }
      else
      {
        // Constant object doesn't mean that the value is not-dynamic.
        // It means that it's not modifiable.
        // see test/vhdl/lang_misc/bug12384_01.vhdl
        vhExpr vh_init_value = NULL;
        if (vhIsDeferred(vh_actual_obj)) {
          vh_init_value = vhGetDeferredConstantValue(vh_actual_obj);
        }
        else
        {
          vh_init_value = vhGetInitialValue(vh_actual_obj);
        }
        retval |= sExprContainsDynamicObject( vh_init_value, vh_dyn_obj );

      }
      break;

    default:
      return false;
      break;
    }
    break;
  }
  case VHBINARY:
  {
    vhExpr vh_left = vhGetLeftOperand( vh_expr );
    vhExpr vh_right = vhGetRightOperand( vh_expr );
    retval |= sExprContainsDynamicObject( vh_left, vh_dyn_obj );
    retval |= sExprContainsDynamicObject( vh_right, vh_dyn_obj );
    break;
  }
  case VHUNARY:
  {
    retval |= sExprContainsDynamicObject( vhGetOperand( vh_expr ), vh_dyn_obj );
    break;
  }
  case VHFUNCCALL:
  {
    JaguarList assoc_list( vhGetAssocList( vh_expr ));
    while ( vhExpr assoc_item = static_cast<vhExpr>( vhGetNextItem( assoc_list )))
    {
      retval |= sExprContainsDynamicObject( assoc_item, vh_dyn_obj );
    }
    break;
  }
  case VHTYPECONV:
    retval |= sExprContainsDynamicObject( vhGetOperand( vh_expr ), vh_dyn_obj );
    break;
  default:
    break;
  }
  return retval;
}


Populate::ErrorCode 
VhdlPopulate::getVariableRangeWidth( vhNode vh_slice, LFContext *context,
                                     int *constantWidth, bool *isVariableIndexed)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  const SourceLocator loc = locator( vh_slice );
  *constantWidth = 0; // *constantWidth==0 is an error condition.  We're
                      // only successful here if we return a nonzero value
  vhNode vh_range = NULL;
  temp_err_code = getVhRangeNode( vh_slice, &vh_range, NULL );
  if ( vhGetObjType( vh_range ) != VHRANGE )
  {
    vh_range = vhGetDisRange( static_cast<vhExpr>( vh_slice ));
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    if ( vhGetObjType( vh_range ) == VHATTRBNAME &&
         ( vhGetAttrbType( vh_range ) == VH_RANGE ||
           vhGetAttrbType( vh_range ) == VH_REVERSE_RANGE ) )
    {
      vhNode evaldRange = evaluateJaguarExpr( static_cast<vhExpr>( vh_range ),
                                              true );
      if (NULL == evaldRange)
      {
        *isVariableIndexed = true;
        return eNotApplicable;
      }
      else
      {
        vh_range = evaldRange;
      }
    }
  }

  vhExpr vh_left_bound = vhGetLtOfRange(vh_range);
  vh_left_bound  = evaluateJaguarExpr( static_cast<vhExpr>(vh_left_bound), false,
                                       !mIsAggressiveEvalEna );
  vhExpr vh_right_bound = vhGetRtOfRange(vh_range);
  vh_right_bound  = evaluateJaguarExpr( static_cast<vhExpr>(vh_right_bound), false,
                                        !mIsAggressiveEvalEna );
  vhNode dynamicObj = NULL;
  bool rangeContainsDynamicBound = sExprContainsDynamicObject(vh_left_bound, 
                                                       dynamicObj);
  if (not rangeContainsDynamicBound)
  {
    rangeContainsDynamicBound = sExprContainsDynamicObject(vh_right_bound,
                                                       dynamicObj);
  }
  if (not rangeContainsDynamicBound)
  {
    // This means that both the bounds of this slice are constant i.e.
    // non-dynamic.  So, set the input flag 'isVariableIndexed' to false
    // so that caller will know that it is not a variable index slice.
    *isVariableIndexed = false;
    // Since this is not a variable index slice, return non-success status
    // eNotApplicable so that caller can process this slice as constant 
    // indexed slice. 
    return eNotApplicable;
  }

  *isVariableIndexed = true;
  // At this point we have found at least one bound of the RANGE contains
  // dynamic object. We support FORINDEX, TO and DOWNTO as dynamic object
  vhObjType rangeType = vhGetObjType (dynamicObj);

  if (rangeType == VHVARIABLE || rangeType == VHSIGNAL || 
      ((rangeType == VHCONSTANT) && (vhIsSubProgInterfaceConstant(dynamicObj) == VH_TRUE)))
  {
    // No error message since the caller is likely to process it as
    // variable width partselect
    return eNotApplicable;
  }

  if (rangeType != VHFORINDEX)
  {
    mMsgContext->UnExpectedObjectType( &loc, gGetVhNodeDescription( dynamicObj ),
                                       "Cannot determine size of dynamic-ranged object." );
    return eFatal;
  }

  // Elaborate the for loop and evaluate the range and calculate
  // width for every iteration. If the width changes in any of the iteration,
  // we will report error and return eNotApplicable so that we do a dynamic-range
  // partselect
  vhNode forScope = vhGetScope(dynamicObj);
  // We need to make sure that the width of this part-select expression
  // is fixed. This can be done by iterating the loop whose index is
  // this dynamic object.
  bool inGenerate = ( vhGetObjType( forScope ) == VHFORGENERATE );
  if ( vhElaborateGenerateRange(forScope) == false && inGenerate )
  {
    SourceLocator loopLoc = locator(forScope);
    POPULATE_FAILURE(mMsgContext->JaguarElaborationFailure(&loopLoc, "for loop"), &err_code);
    return err_code;
  }
  VhdlRangeInfo indexBounds (0,0);
  int currentWidth  = 0;
  int previousWidth = 0;
  // We need to make sure that only one dynamic object is present
  // in both the bounds of the RANGE. If so, then only we will be able to 
  // calculate the left and the right bounds, otherwise getLeftAndRightBounds()
  // will report error. To support variable width part select, we should not
  // report this error. So, we need to check both the bounds of the range.
  vhExpr evaluatedLeft = evaluateJaguarExpr(vh_left_bound, true );
  vhExpr evaluatedRight = evaluateJaguarExpr(vh_right_bound, true);
  if ( (NULL == evaluatedLeft) || (NULL == evaluatedRight) ) 
  {
    // We have more than one dynamic object in the left/right bound of
    // this slice. We should not report any error because we will
    // create variable width part select for this slice.  Since we had
    // started the for loop unrolling, we need to reset the for loop
    // index before exiting.
    vhResetGenerateIndex(forScope);
    // No error message since the caller is likely to process it as 
    // variable width partselect.
    return eNotApplicable;
  }
  switch (vhGetObjType(evaluatedLeft))
  {
    case VHDECLIT:
    case VHBASELIT:
    case VHCHARLIT:
    case VHIDENUMLIT:
    case VHCONSTANT:
      break;
    default:
      // Since we had started the for loop unrolling, we need to
      // reset the for loop index before exiting.
      vhResetGenerateIndex(forScope);
      return eNotApplicable;
  }
  switch (vhGetObjType(evaluatedRight))
  {
    case VHDECLIT:
    case VHBASELIT:
    case VHCHARLIT:
    case VHIDENUMLIT:
    case VHCONSTANT:
      break;
    default:
      // Since we had started the for loop unrolling, we need to
      // reset the for loop index before exiting.
      vhResetGenerateIndex(forScope);
      return eNotApplicable;
  }

  // Disable staticness check to allow for loop index to take effect.
  ErrorCode tmp_err = getLeftAndRightBounds(vh_range, &indexBounds, context,
                                            true, false);
  if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err, &err_code ) )
  {
    vhResetGenerateIndex(forScope);
    return eNotApplicable;
  }

  // If VhdlRangeInfo is a null range, consider it's width to be 0
  currentWidth = indexBounds.isNullRange()?0:indexBounds.getWidth(loc);
  previousWidth = currentWidth;
  while ( vhUpdateGenerateIndex(forScope) )
  {
    // Disable staticness check to allow for loop index to take effect.
    tmp_err = getLeftAndRightBounds(vh_range, &indexBounds, context, true, false);
    currentWidth = indexBounds.isNullRange()?0:indexBounds.getWidth(loc);
    if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err, &err_code ) || 
         (currentWidth != previousWidth) )
    {
      // Since we had started the for loop unrolling, we need to
      // reset the for loop index before exiting.
      vhResetGenerateIndex(forScope);
      return eNotApplicable;
    }
  }
  // Since we had started the for loop unrolling, we need to
  // reset the for loop index before exiting.
  vhResetGenerateIndex(forScope);
  // Null range check
  if ( ( currentWidth == 0 ) ||
       ( indexBounds.isNullRange() ) )
  {
    UtString slice_str;
    SourceLocator sliceLoc = locator(vh_slice);
    POPULATE_FAILURE(mMsgContext->NullRangedSlice(&sliceLoc, decompileJaguarNode(vh_slice, &slice_str)), &err_code);
    return err_code;
  }
  *constantWidth = currentWidth;
  return eSuccess;
}


// Given a VHSLICENAME token, return the index expression and range
// normalized to a [width-1:0] object.
Populate::ErrorCode 
VhdlPopulate::partsel (vhExpr vh_slice,
                       LFContext *context,
                       int lhsWidth,
                       NUBase** the_node,
                       NUExpr** index_expr,
                       ConstantRange* range,
                       UtVector<vhExpr> *dimVector,
                       bool* isConstIndex,
                       const bool isLvalue)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  // If prefixIsMemory is true, then we are slicing a memory and will
  // need to construct an aggregate.
  bool prefixIsMemory = true;
  *index_expr = 0;
  *range = ConstantRange (0,0);
  *isConstIndex = false;

  SourceLocator loc = locator(vh_slice);

  vhNode vh_actual_obj = NULL; // used only for alias resolution
  VhdlRangeInfo actualBounds (0,0), aliasBounds (0,0);
  vhExpr vh_named = vhGetPrefix(vh_slice);
  if (VHOBJECT == vhGetObjType(vh_named))
    vh_named = static_cast<vhExpr>(vhGetActualObj(vh_named));

  if ( vhGetObjType( vh_named ) == VHINDNAME )
  {
    // grab this set of indices
    exprIndices( static_cast<vhExpr>( vh_named ), context, dimVector, true );
    // set vh_named to the base object
    while ( vhGetObjType(vh_named) == VHINDNAME )
      vh_named = vhGetPrefix( static_cast<vhExpr>( vh_named ));
    vh_actual_obj = vh_named;

    int size = 0, numDims = 0;
    temp_err_code = getVhExprBitSize( static_cast<vhExpr>( vh_actual_obj ), &size,
                                      &numDims, loc, context, isLvalue, true );
    // N-1 dimensions means we're slicing a word, not a (sub) memory
    if ( (int)dimVector->size() == numDims - 1 )
      prefixIsMemory = false; 
  }
  else if ( vhGetObjType( vh_named ) == VHSELECTEDNAME )
  {
    if ( mCarbonContext->isNewRecord( ))
    {
      vh_actual_obj = vh_named;
    }
    else
    {
      // Collect any index expressions from the selected name and place
      // them in dimVector.
      exprIndices( static_cast<vhExpr>( vh_named ), context, dimVector, true );
      vh_actual_obj = vhGetActualObj( vh_named );
    }
  }
  else
  {
    vh_actual_obj = vh_named;
  }
  // We need to resolve aliases if it appears in the lvalue. We do not
  // need to resolve aliases because we have assigned actual to the alias.
  // Example:
  // alias IN1_AL1 : std_logic_vector(7 downto 0) is IN1(7 downto 0);
  // For this declaration we have created a net named IN1_AL1 and
  // assigned IN1 to IN1_AL1. So, value of IN1 will be visible in IN1_AL1.
  // But when this alias IN1_AL1 appears in the lvalue, we need the 
  // alias resolution because any change in IN1_AL1 should get reflected 
  // in the IN1 . Since IN1_AL1 and IN1 are different nets, change in
  // IN1_AL1 will not change IN1 . Hence the resolution is required.

  NULvalue *recordLvalue = NULL;
  NUExpr *recordExpr = NULL; 
  NUNet *the_net = NULL;
//   bool isAlias = (isAliasObject(vh_actual_obj) == true);
//   if (isLvalue and isAlias)
//   {
//     vhNode vhRange = NULL;
//     temp_err_code = getVhRangeNode(vh_named, &vhRange, NULL);
//     if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
//     {
//       return temp_err_code;
//     }
//     temp_err_code = getLeftAndRightBounds(vhRange, &aliasBounds, context );
//     if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
//     {
//       return temp_err_code;
//     }
//     temp_err_code = resolveAlias( vh_named, &aliasBounds, &actualBounds,
//                                   context, &the_net );
//   }
//   else
  {
    vhObjType expr_type = vhGetObjType(vh_named);
    if ( expr_type == VHFUNCCALL && !(mCarbonContext->isNewRecord() && isRecordNet(vh_named, context) ))
    {
      NUExprArray expr_array;
      temp_err_code = functionCall( vh_named, context, lhsWidth, &expr_array );
      NUExpr *the_fctcall_expr = expr_array[0];
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
        delete the_fctcall_expr;
        return temp_err_code;
      }
      NUIdentRvalue *ident = dynamic_cast<NUIdentRvalue*>(the_fctcall_expr);
      if(ident == NULL)
      {
        // This means the function call was not converted to a task
        delete the_fctcall_expr;
        POPULATE_FAILURE(mMsgContext->JagInvalidType(&loc,
                                                     gGetVhNodeDescription(vh_named),
                                                     "function call"), &err_code);
        return err_code;
      }
      the_net = ident->getIdent();
      delete the_fctcall_expr; // the expression is no longer needed, only the_net was needed
    }
    // Selected names like rec.field, need an eNUCompositeIdent[Rvalue|Lvalue] expression
    // to select on so that composite resynthesis can identify and resynthesize it. Same
    // applies to slices of variable, signal or constant nets of record types.
    else if ( mCarbonContext->isNewRecord() && 
              ( (expr_type == VHSELECTEDNAME) || isRecordNet(vh_named, context) ) )
    {
      if ( isLvalue )
        temp_err_code = lvalue( vh_named, context, &recordLvalue );
      else
        temp_err_code = expr( vh_named, context, 0, &recordExpr );

      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

      if ( recordExpr )
        recordExpr->resize( recordExpr->determineBitSize( ));
      
      // This sets the_net to point to _a_ copy of the sliced net, not
      // necessarily the precise one.  We may not knnow the precise one
      // due to a record index.  But, it should have the correct sizes
      // associated with it.
      vhExpr functCallExpr = NULL;
      bool functCall = isFunctionRecField(vh_named, &functCallExpr);
      if(expr_type == VHFUNCCALL || functCall)
      {
	NUCompositeInterfaceExpr *comRvalue = dynamic_cast<NUCompositeInterfaceExpr*> (recordExpr);
	the_net = comRvalue->getNet();
      }
      else
        temp_err_code = lookupNet( context->getDeclarationScope(), vh_named,
                                   context, &the_net );
      


    }
    else
    {
      temp_err_code = lookupNet( context->getDeclarationScope(), vh_named,
                                 context, &the_net );
    }
  }
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    *the_node = the_net;
    delete recordLvalue;
    delete recordExpr;
    return temp_err_code;
  }

  ConstantRange netRange (0,0);
  NUVectorNet *vn = NULL;
  NUMemoryNet *mn = NULL;
  NUCompositeNet *cn = NULL;
  if ( the_net->isVectorNet( ))
  {
    prefixIsMemory = false;
    vn = the_net->castVectorNet();
    netRange = *vn->getDeclaredRange ();
  }
  else if ( the_net->isMemoryNet( ))
  {
    // In Case indexName access of memoryNet
    // type memory is array (POSITIVE range <>) of integer;
    // signal my_mem1 : memory(1 to 4) ;
    // my_mem1(1) <= 777;
    //
    // 'netRange' is the word width (0, 31)

    // In case of sliceName access of memoryNet
    // my_mem1(0 to 2) <= ( 1, 2, 3);

    // 'netRange' is the memory depth (1, 4)
    // The above sliceName access would be converted into a 
    // NUConcat[R|L]value of multiple indexName access of memoryNet.
    // my_mem1(0 to 2) converted to {my_mem(0),my_mem(1),my_mem(2)}
    mn = the_net->getMemoryNet();
    const UInt32 dim = mn->getNumDims() - dimVector->size() - 1;
    if ( dim == 0 ) // prefixIsMemory means a memory slice; this is a word slice
      prefixIsMemory = false;
    netRange = *(mn->getRange( dim ));
  }
  else if(the_net->isCompositeNet())
  {
    prefixIsMemory = false;
    cn = the_net->getCompositeNet();
    if(cn != NULL)
    {
      const UInt32 dim = cn->getNumDims() - dimVector->size() - 1;
      netRange = *(cn->getRange( dim ));
    }
  }

  // Calculate the index expression to a precision sufficient to address the
  // entire object.
  UInt32 lWidth = Log2 (netRange.getLength()) + 1;

  vhNode vh_range = vhGetDisRange(vh_slice);
  vhNode actual_range = NULL;
  temp_err_code = getVhRangeNode(vh_range, &actual_range, NULL);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    *the_node = the_net;
    delete recordLvalue;
    delete recordExpr;
    return temp_err_code;
  }
  vh_range = actual_range;
  if ( vhGetObjType( vh_range ) == VHATTRBNAME &&
       ( vhGetAttrbType( vh_range ) == VH_RANGE ||
         vhGetAttrbType( vh_range ) == VH_REVERSE_RANGE ) )
  {
    vhNode evaldRange = evaluateJaguarExpr( static_cast<vhExpr>( vh_range ),
                                            true );
    if (NULL == evaldRange)
    {
      UtString slice_str, index_str;
      SourceLocator sliceLoc = locator( vh_slice );
      *the_node = the_net;
      delete recordLvalue;
      delete recordExpr;
      POPULATE_FAILURE(mMsgContext->UnsupportedRangeBound( &sliceLoc,
                                                           decompileJaguarNode( vh_range, &index_str ),
                                                           decompileJaguarNode( vh_slice, &slice_str ) ),
                       &err_code);
      return err_code;
    }
    else
    {
      vh_range = evaldRange;
    }
  }

  vhDirType partselDir = vhGetRangeDir( vh_range );
  const bool inForScope = ( VHFOR == vhGetObjType( vhGetScope( vh_slice )));
  ConstantRange partselRange;
  bool isVariableIndexPartSel = false;
  SInt32 width = 0;
  temp_err_code = getVariableRangeWidth( vh_slice, context, &width, 
                                         &isVariableIndexPartSel );
  if ( isVariableIndexPartSel && temp_err_code != eSuccess )
  {
    VhdlRangeInfo ri_range;
    if (getLeftAndRightBounds(vh_range, &ri_range, context, false) == eSuccess) {
      temp_err_code = eSuccess;
      partselRange = ConstantRange(ri_range.getMsb(loc), ri_range.getLsb(loc));
      *isConstIndex = true;
      *index_expr = NUConst::create (false, 0,  lWidth, loc);
    } else {
      // This slice corresponds a variable index part select but it's
      // width is not fixed. So, we can't create NUVarsel* for this slice.
      *the_node = the_net;
      delete recordLvalue;
      delete recordExpr;
      return temp_err_code;
    }
  }
  else
  {
    if ( inForScope and isVariableIndexPartSel )
    {
      if ( the_net->isMemoryNet() ) 
      {
	// Memory nets deal differently with the case where the slice is a VH_TO
	// than a VH_DOWNTO. Since the slice can correspond to the higher dimensions which
	// are not normalized, the index_expr and the created range have to reflect that fact.
	// See bug 12587 for more details.
	vhExpr vh_bound;
	if (partselDir == VH_DOWNTO) 
	{
	  vh_bound = vhGetLtOfRange(vh_range);
	}
	else 
	{
	  vh_bound = vhGetRtOfRange(vh_range);
	}
	temp_err_code = expr (vh_bound, context, 0, index_expr);
	if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
	  *the_node = the_net;
	  delete recordLvalue;
	  delete recordExpr;
	  return temp_err_code;
	}
	// Range is specified in normalized form offset from the index.
	LOC_ASSERT( width > 0, loc );
	if (partselDir == VH_DOWNTO) 
	{
	  partselRange = ConstantRange( width - 1, 0 );
	}
	else 
	{
	  partselRange = ConstantRange( 0, width - 1 );
	}
      }
      else
      {
	// This is the case for vector or composite nets
	vhExpr vh_lt_bound = vhGetLtOfRange(vh_range);
	temp_err_code = expr (vh_lt_bound, context, 0, index_expr);
	if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
	  *the_node = the_net;
	  delete recordLvalue;
	  delete recordExpr;
	  return temp_err_code;
	}
	// Range is specified in normalized form offset from the index.
	LOC_ASSERT( width > 0, loc );
	partselRange = ConstantRange( width - 1, 0 );
      }
    }
    else
    {
      // Populate the range of the partsel into partselRange (eventually)
      ConstantRange* copiedRange = NULL;
      temp_err_code = constantRange( vh_range, context, &copiedRange );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
        *the_node = the_net;
        delete recordLvalue;
        delete recordExpr;
        return temp_err_code;
      }
      partselRange = *copiedRange;
      delete copiedRange;
      *index_expr = NUConst::create (false, 0,  lWidth, loc);
      *isConstIndex = true;
    }
  } // else


  // We might need to reverse the parselRange/Dir data if the VHDL had a
  // null range in it.  Don't correct if we're processing a variably
  // indexed partsel.
  ConstantRange correctedRange( partselRange );
  vhDirType correctedDir = partselDir;
  if ( !(inForScope && isVariableIndexPartSel ) &&
       (( partselRange.getMsb() < partselRange.getLsb() && partselDir == VH_DOWNTO ) ||
        ( partselRange.getMsb() > partselRange.getLsb() && partselDir == VH_TO )))
  {
    // This is a null range or invalid range.  Create something to
    // populate, but we never expect it to be executed.
    UtString slice_str, index_str;
    mMsgContext->NullRangeDetected(&loc,
                                   "A Null range was found for the part-select",
                                   decompileJaguarNode(vh_slice, &slice_str), partselRange.format(&index_str));
    // We will reverse the range and use it.  This makes
    // correctedDir and correctedRange consistent with each other.
    correctedRange.switchSBs();
  }

  if ((the_net->isVectorNet() || the_net->isMemoryNet( ))&&
      *isConstIndex && !netRange.contains(correctedRange))
  {
    // Can't tell at compile time if  v[k+:3] is "in-range or not..."
    UtString buf;
    the_net->compose(&buf, NULL);
    mMsgContext->LFPartRangeWarning(&loc,
                                    correctedRange.getMsb(),
                                    correctedRange.getLsb(),
                                    buf.c_str(),
                                    netRange.getMsb(), netRange.getLsb());
    // This might reverse the bounds, not just truncate it!  We need to
    // put the correct direction back.
    if (netRange.overlaps (correctedRange))
    {
      ConstantRange overlapRange = netRange.overlap( correctedRange );
      if ( correctedRange.bigEndian() != overlapRange.bigEndian() )
        overlapRange.switchSBs();
      correctedRange = overlapRange;
    }
  }

  // normalize all bit references, synthesizing corrective
  // arithmetic as needed.

  // Convert to internal representation.  Always remap the range
  // into [width-1 : 0], and if the index expression is non-constant
  // must renormalize it also.
  //
  UInt32 self_determined_width = (*index_expr)->determineBitSize();
  (*index_expr)->resize (self_determined_width);

  if (not *isConstIndex)
  {
    // if the direction of the declared range and the created range do
    // not agree then they must be a null range, we will adjust them
    // to something that can be populated but we expect to never execute.
    SInt32 adjust = (netRange.bigEndian() ^ (correctedDir == VH_TO)) ? correctedRange.getLength() - 1 : 0;
    if ( vn )
      *index_expr = vn->normalizeSelectExpr (*index_expr, adjust, false);
    else if ( mn )
      *index_expr = mn->normalizeSelectExpr( *index_expr, 0, adjust, false );
    else if(cn)
      *index_expr = cn->normalizeSelectExpr (*index_expr, 0, adjust, false);
  }
  else
  {
    if (!prefixIsMemory && !the_net->isCompositeNet())
    {
      // See if the net's declared range direction differs from the partsel's direction
      if (( netRange.getMsb() < netRange.getLsb() && correctedDir == VH_DOWNTO ) ||
          ( netRange.getMsb() > netRange.getLsb() && correctedDir == VH_TO ))
      {
        // The range directions of the base net and the partsel differ.  Create
        // something to populate, but we never expect it to be executed.
        UtString prefix_text, slice_str, index_str;
        prefix_text << "The direction of the net's declared range (";
        prefix_text << netRange.getMsb() << " ";
        if ( netRange.bigEndian( ))
          prefix_text << "down";
        prefix_text << "to " << netRange.getLsb();
        prefix_text << ") does not match the direction of the index expression range.";
        mMsgContext->NullRangeDetected( &loc,
                                        prefix_text.c_str(),
                                        decompileJaguarNode( vh_slice, &slice_str ),
                                        correctedRange.format( &index_str ));
        // We will just reverse the range and use it.
        correctedRange.switchSBs();
      }
      // correctedRange is in units of the declared net.  Convert to
      // [len-1:0].
      correctedRange.normalize( &netRange, false );
    }
  }

  *range = correctedRange;
  if ( recordExpr != NULL )
    *the_node = recordExpr;
  else if ( recordLvalue != NULL )
    *the_node = recordLvalue;
  else
    *the_node = the_net;

  return err_code;
}


// This function takes a jaguar node and finds out the VHRANGE
// of that node. The range is then set to the content of the input pointers
// vh_range1 and vh_range2 . If an unexpected node type is encountered an error
// message is emitted.
// INPUTS: vh_obj => The given jaguar node from which the VHRANGE information
//                   has to find.
//         vh_range1 => The VHRANGE extracted from the given 'vh_obj' node.
//         vh_range2 => The 2nd VHRANGE extracted from the given 'vh_obj' node
//                      if the 'vh_obj' corresponds to 2 dimensional array.
//
//
Populate::ErrorCode
VhdlPopulate::getVhRangeNode( vhNode vh_obj, 
                              vhNode* vh_range1, 
                              vhNode* vh_range2 )
{
  // Stop!! something is wrong.
  if (vh_obj == NULL) 
    return eFailPopulate;

  ErrorCode err_code = eSuccess, temp_err_code = eSuccess;
  *vh_range1 = NULL;
  if (vh_range2)
    *vh_range2 = NULL;
  vhObjType obj_type = vhGetObjType(vh_obj);
  vhNode range_cons1 = NULL;
  vhNode range_cons2 = NULL;
  switch (obj_type)
  {
    case VHOBJECT:
      return getVhRangeNode(vhGetActualObj(vh_obj), vh_range1, vh_range2);
      break;
    case VHSUBTYPEDECL:
      {
        vhNode vh_con = vhGetConstraint(vh_obj);
        if (vh_con) 
          return getVhRangeNode(vh_con, vh_range1, vh_range2);
        break;
      }
    case VHTYPEDECL:
      {
        if (! isPredefined3StateLogicOrBit(vh_obj))
          return getVhRangeNode(vhGetTypeDef(vh_obj), vh_range1, vh_range2);
        break;
      }
    case VHRANGECONSTRAINT:
      {
        range_cons1 = vhGetRange(vh_obj);
        break;
      }
    case VHRANGE:
      {
        // VHRANGE object could be of attribute type or non-attribute i.e index
        // constraint range type. - bug6787
        if (vhIsRangeAttribute(vh_obj) == VH_TRUE) {
          vhNode attrib = vhGetRangeAttrib(vh_obj);
          temp_err_code = getVhRangeNode(attrib, &range_cons1, &range_cons2);
        }
        else
          range_cons1 = vh_obj;
        break;
      }
    case VHCONSARRAY:
      {
        vhNode index_con = vhGetIndexConstr(vh_obj);
        temp_err_code = getVhRangeNode(index_con, &range_cons1, &range_cons2);
        break;
      }
    case VHINDEXCONSTRAINT:
    {
      JaguarList rangeList(vhGetDiscRangeList(vh_obj));
      if (rangeList.size() > 2 )
      {
        SourceLocator loc = locator(vh_obj);
        POPULATE_FAILURE(mMsgContext->MultiDimensionalArray(&loc), &err_code);
        return err_code;
      }
      else
      {
        range_cons1 = vhGetNextItem(rangeList);
        range_cons2 = vhGetNextItem(rangeList);
      }
      break;
    }
    case VHSLICENAME:
    {
      range_cons1 = vhGetDisRange(static_cast<vhExpr>(vh_obj));
      vhNode actual_range = NULL;
      temp_err_code = getVhRangeNode(range_cons1, &actual_range, &range_cons2);
      range_cons1 = actual_range;
      break;
    }
    case VHSELECTEDNAME:
      range_cons1 = static_cast<vhNode>(vhGetExprSize(
                                                 static_cast<vhExpr>(vh_obj)));
      break;
    case VHVARIABLE:
    case VHSIGNAL:
    case VHCONSTANT:
    {
      vhNode subtype = vhGetSubTypeInd(vh_obj);
      range_cons1 = vhGetConstraint(subtype);
      if (! range_cons1)
        temp_err_code = getVhRangeNode(subtype, &range_cons1, &range_cons2);
      break;
    }
    case VHINDNAME:
    {
      // Trying to take the range of an indexed name implies that this is
      // a multidimensional object.
      vhNode vh_size = vhGetExprSize( static_cast<vhExpr>( vh_obj ));
      return getVhRangeNode( vh_size, vh_range1, vh_range2 );
      break;
    }
    case VHSUBTYPEIND:
    {
      vhNode range = vhGetConstraint(vh_obj);
      if (range)
      {
        return getVhRangeNode( range, vh_range1, vh_range2);
      }
      vhNode vh_type = vhGetType(vh_obj);
      return getVhRangeNode( vh_type, vh_range1, vh_range2);
      break;
    }
    case VHATTRBNAME:
    {
      vhAttrbType atr_type = vhGetAttrbType( vh_obj );
      if ( VH_RANGE == atr_type )
      {
        vhNode node = vhGetPrefix( static_cast<vhExpr>( vh_obj ) );
        return getVhRangeNode( node, vh_range1, vh_range2 );
      }
      else if (VH_REVERSE_RANGE == atr_type )
      {
        range_cons1 = vh_obj;
        break;
      }
      // Unknown attribute, so fall thru the error message
    }
    default: {
        SourceLocator loc = locator(vh_obj);
        POPULATE_FAILURE(mMsgContext->UnExpectedObjectType( &loc, gGetVhNodeDescription(vh_obj),
                                                            " (while calculating range)" ), &err_code);
        return err_code;
        break;
    }
  }
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  if (!range_cons1)
    return eSuccess; // Case of scalar object
  if (VHINDEXCONSTRAINT == vhGetObjType(range_cons1))
  {
    JaguarList rangeList(vhGetDiscRangeList(range_cons1));
    vhNode vh_ind_range1, vh_ind_range2;
    vh_ind_range1 = vhGetNextItem(rangeList);
    vh_ind_range2 = vhGetNextItem(rangeList);
    if ( ( NULL != vhGetNextItem(rangeList) ) )
    {
      SourceLocator loc = locator(range_cons1);
      POPULATE_FAILURE(mMsgContext->MultiDimensionalArray(&loc), &err_code); 
    }
    temp_err_code = getVhRangeNode( vh_ind_range1, vh_range1, NULL );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
    if ( vh_range2 != NULL && vh_ind_range2 != NULL )
      return getVhRangeNode( vh_ind_range2, vh_range2, NULL );
    return err_code;
  }
  else
  {
    *vh_range1 = range_cons1;
    if (vh_range2)
      *vh_range2 = range_cons2;
  }
  return err_code;
}


// Wrapper function on the jaguar API vhIsAliasObject() . This function will
// take any jaguar node as input and return true if it is alias or if
// it is created out of an alias object, false otherwise.
bool 
VhdlPopulate::isAliasObject(vhNode vh_obj)
{
  if (VHOBJECT == vhGetObjType(vh_obj))
  {
    vh_obj = vhGetActualObj(vh_obj);
  }
  switch (vhGetObjType(vh_obj))
  {
    case VHSIGNAL:
    case VHCONSTANT:
    case VHVARIABLE:
    case VHFILEDECL:
      return vhIsAliasObject(vh_obj);
      break;
    case VHSLICENAME:
    case VHINDNAME:
    {
      vhNode prefix = vhGetPrefix(static_cast<vhExpr>(vh_obj));
      return isAliasObject(prefix);
      break;
    }
    case VHSELECTEDNAME:
      return isAliasObject(vhGetActualObj(vh_obj));
      break;
    default:
      return false;
  }
  return false;
}

// Checks to look for signals/variables in the expression and calls expression
// static if it doesn't find any.
class ExprStaticnessChecker : public JaguarBrowserCallback
{
public:

  ExprStaticnessChecker()
    : mIsStatic(true) {
  }
    
  virtual ~ExprStaticnessChecker() {
  }

  Status vhvariable(Phase phase, vhNode node) {
    if ((phase == ePre) && (vhGetDynamicValue(node) == NULL)) {
      mIsStatic = false;
      return eStop;
    }
    return eNormal;
  }

  Status vhsignal(Phase, vhNode) {
    mIsStatic = false;
    return eStop;
  }

  Status vhforindex(Phase phase, vhNode node) {
    if ((phase == ePre) && (vhGetDynamicValue(node) == NULL)) {
      mIsStatic = false;
      return eStop;
    }
    return eNormal;
  }

  Status vhconstant(Phase phase, vhNode node) {
    // Subprogram interfaces are called VHCONSTANT when they may not
    // be constant. Look for their initial values. If they're constant,
    // then the interface is too.
    if ( (phase == ePre) && vhIsSubProgInterfaceConstant(node) ) {
      bool isStatic = false;

      // Determine if this constant is actually a constant.
      // If one or more of the original function arguments was a variable
      // or a signal, that fact may have been lost after the function has
      // been elaborated a few times due to recursion. If the node was
      // marked with the IS_NOT_CONSTANT attribute, then it is not really
      // a constant.
      vhNode value = vhGetAttr(node, "IS_NOT_CONSTANT");
      if (value == NULL) {
	vhExpr initVal;
	if (vhIsDeferred(node)) {
	  initVal = vhGetDeferredConstantValue(node);
	}
	else {
	  initVal = vhGetInitialValue(node);
	}
	if (initVal != NULL) {
	  // Use the same method used to create function names with constant
	  // argument values embedded in them. This way we avoid creating
	  // non-unique functions with unique expectation of argument values.
	  // See test/vhdl/beacon3/procedure13 testcase.
	  vhExpr const_node = sGetConstNode(initVal);
	  if (const_node != NULL) {
	    isStatic = true;
	  }
	}
      }
      if (!isStatic) {
        mIsStatic = false;
        return eStop;
      }
    }
    return eNormal;
  }

  Status vhfunccall(Phase phase, vhNode node) {
    // The subprogram actuals are browsed separately and looked for staticness.
    // If the subprogram itself is impure, then the expression cannot be static.
    if (phase == ePre) {
      bool isStatic = false;
      vhExpr func = static_cast<vhExpr>(vhGetMaster(node));
      if (func != NULL) {
        vhPureType typ = vhGetPureType(func);
        if ((typ == VH_PURE) || (typ == VH_DEFAULT_PURE)) {
          isStatic = true;
        }
      }
      if (!isStatic) {
        mIsStatic = false;
        return eStop;
      }
    }
    return eNormal;
  }

  bool isStatic() const { return mIsStatic; }

private:
  bool mIsStatic;
};

// A wrapper on the jaguar API vhEvaluateExpr() to factor out common code
// in this function.
vhExpr
VhdlPopulate::evaluateJaguarExpr( vhExpr vh_expr, 
                                  bool returnNullForNonStaticExpr /* = false */, 
                                  bool staticnessCheckRequired /* = false */ ) 
{
  vhExpr evaluatedExpr = NULL;

  if ( staticnessCheckRequired && vhIsA(vh_expr,VHEXPR))
  {
    vhStaticType staticness = vhGetStaticType(vh_expr);
    if ( VH_LOCALLY_STATIC == staticness || VH_GLOBALLY_STATIC == staticness )
    {
      evaluatedExpr = vhEvaluateExpr(vh_expr);
    }
    else
    {
      // Jaguar staticness detection is weak. It cannot determine for example that
      // BOOLEAN'POS(1) is constant. Try our staticness checker.
      ExprStaticnessChecker esc;
      {
        JaguarBrowser jb(esc);
        jb.browse(vh_expr);
      }

      // Check that the expr size is not constant as well
      if (esc.isStatic())
      {
        vhExpr vh_size = vhGetExprSize(vh_expr);
        // It's not clear why vhGetExprSize will return NULL,
        // but I observed that it returns NULL for expr (if in1 = '0'),
        // where in1 is a signal 
        if((vh_size == NULL) || (vhGetStaticType(vh_size) != VH_NOT_STATIC))
          evaluatedExpr = vhEvaluateExpr(vh_expr);
      }
    }
  }
  else
  {
    evaluatedExpr = vhEvaluateExpr(vh_expr);
  }
  if ( NULL == evaluatedExpr && not returnNullForNonStaticExpr )
  {
    evaluatedExpr = vh_expr;
  }
  return evaluatedExpr;
}


Populate::ErrorCode
VhdlPopulate::attribute( vhExpr vh_attribute, LFContext *context, NUExpr **the_expr )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  // We support any ATTRIBUTE which is statically evaluatable.  We do
  // not report error for the non-static attributes 'EVENT and 'STABLE
  // because they are presumed to be involved in clock edges.
  
  // The last parameter was changed to true to allow dynamic scalar 
  // attribute processing. 
  vhExpr evald_expr = evaluateJaguarExpr( vh_attribute, true, true);
  if (evald_expr)
  {
    temp_err_code = expr( evald_expr, context, 0, the_expr );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
  }
  else
  {
    VhAttrbType att_type = vhGetAttrbType( vh_attribute );
    const SourceLocator loc = locator( vh_attribute );
    const vhNode vh_scope = context->getJaguarScope();
    VhObjType objType = vhGetObjType( vh_scope );
    if (( att_type == VH_EVENT || att_type == VH_STABLE ) &&
        ( objType != VHIF && objType != VHBLOCK && objType != VHPROCESS ))
    {
      mMsgContext->AttributeNotOnClock( &loc, vhGetAttribName( vh_attribute ));
      return eFatal;
    }
    vhExpr vh_prefix = vhGetPrefix( vh_attribute );
    NUExpr *prefix;
    switch( att_type )
    {
    case VH_EVENT:
    case VH_STABLE:
    {
      temp_err_code = expr( vh_prefix, context, 0, &prefix );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
      *the_expr = new NUAttribute( prefix, (att_type == VH_EVENT) ? NUAttribute::eEvent : NUAttribute::eStable, loc );
      break;
    }
    case VH_LEFT:
    case VH_RIGHT:
    case VH_HIGH:
    case VH_LOW:
    case VH_LENGTH:
    {
      VhdlRangeInfoVector dimVec;
      temp_err_code = getVhExprRange(vh_prefix, dimVec, loc,
				     false, 
                                     false, // not lvalue
                                     true, // report errors
                                     context);
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
      // Can only do the following on vector nets.
      if ( dimVec.size() == 1 )
      {
        SInt32 value = 0;
        bool isValidValue = false;
        VhdlRangeInfo& ri = dimVec.back();
        // Based on attribute type, use the range to compute the value.
        // Note that the range width could be valid however Msb/Lsb may
        // not be valid, in variable ranges with constant width cases.
        switch( att_type )
        {
        case VH_LEFT:
        case VH_HIGH: // all vectors are normalized
          if (ri.isValidMsbLsb()) {
            value = ri.getMsb(loc);
            isValidValue = true;
          }
          break;
        case VH_RIGHT:
        case VH_LOW: // all vectors are normalized
          if (ri.isValidMsbLsb()) {
            value = ri.getLsb(loc);
            isValidValue = true;
          }
          break;
        case VH_LENGTH: // 'length is range abs(msb - lsb) + 1.
          if (ri.isValidWidth()) {
            value = ri.getWidth(loc);
            isValidValue = true;
          }
          break;
        default:
          UtString msg;
          msg << "Attribute: " << vhGetAttribName(vh_attribute)
              << " not currently supported on a vector net";
          POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, msg.c_str()), &err_code);
          break;
        }

        if (err_code != eFailPopulate) {
          if (isValidValue) {
            *the_expr = NUConst::create( true, value, 32, loc);
          } else {
            // This indicates that the width or msb/lsb is not valid in
            // the vector range. Tell use we cannot determine the attribute
            // value statically i.e at compile time.
            UtString msg;
            msg << "Attribute: " << vhGetAttribName(vh_attribute)
                << " value cannot be determined at compile time.";
            POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, msg.c_str()), &err_code);
          }
        }
      }
      else
      {
        UtString msg;
        msg << "Attribute " << vhGetAttribName(vh_attribute)
            << " not currently supported on a non-vector net";
        POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, msg.c_str( )), &err_code);
      }
      break;
    }
    case VH_VAL:
    case VH_POS:
    case VH_SUCC:
    case VH_PRED:
    case VH_LEFTOF:
    case VH_RIGHTOF:
    {
	temp_err_code = attributeExpr(att_type, vh_attribute, context, loc, the_expr);
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;

        break;
    }


    case VH_ASCENDING:
    {
      vhExpr ev_expr = evaluateJaguarExpr( vh_attribute, true, false);
      if ( ev_expr != NULL )
      {
        temp_err_code = expr( ev_expr, context, 0, the_expr );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;
      }
      else
      {
	// we should not ever get here 
	LOC_ASSERT( ev_expr != NULL , loc );
      }

      break;
    }

    default:
    {
      POPULATE_FAILURE(mMsgContext->UnsupportedAttribute(&loc, vhGetAttribName(vh_attribute)), &err_code);
    }
    }
  }

  return err_code;
}

// Resizes an integerRvalue by wrapping it in a NUConcatOp so that
// the new size is 32 bits. For signed expressions it preserves the
// signed bit. For unsigned expressions, it zero extends.
// As of 6/2/09, the current thinking is that this resizing is necessary
// because nucleus has no way to encode the fact that these ranged
// integers are actually integers. Creating this wrapper on the original
// expression lets optimizations (like fold) recognize the fact that these
// expressions can be treated as integers and optimized in the same way.
// Bug 12542 has a more detailed explanation on how this resizing affects
// record aggregates being assigned to signals of record type.
NUExpr*
VhdlPopulate::resizeIntegerRvalue( NUExpr *origExpr, bool isSigned )
{
  NUExpr *retval;
  origExpr->resize( origExpr->determineBitSize( ));
  if ( isSigned )
  {
    // create SXT( the_expr, 32 )
    retval = VhdlPopulate::buildMask( origExpr, 32, true );
  }
  else
  {
    // Create a concat that extends the length of this, with zeros, to
    // 32 bits.  Doing it this way instead of the more straightforward
    // ZXT approach folds better and avoids codegen issues.
    origExpr->setSignedResult( false );
    const SourceLocator &loc = origExpr->getLoc();
    const UInt32 currentLength = origExpr->getBitSize();
    const UInt32 extendLength = 32 - currentLength;
    NUExpr* aZero = NUConst::create( false, 0, 1, loc );
    NUExprVector zeroVec;
    zeroVec.push_back( aZero );
    NUConcatOp *zeros = new NUConcatOp( zeroVec, extendLength, loc );
    NUExprVector concatVec;
    concatVec.push_back( zeros );
    concatVec.push_back( origExpr );
    retval = new NUConcatOp( concatVec, 1, loc );
    retval->resize( 32 );
    retval->setSignedResult( true );
  }
  return retval;
}

NUExpr*
VhdlPopulate::createIdentRvalue( NUNet *net, const SourceLocator &loc )
{
  NUExpr *the_expr;
  if ( net->isCompositeNet( ))
    the_expr = new NUCompositeIdentRvalue( net->getCompositeNet(), loc );
  else
    the_expr = new NUIdentRvalue( net, loc );

  if ( net->isIntegerSubrange() && !net->is2DAnything() && net->getBitSize() < 32 )
  {
    the_expr = resizeIntegerRvalue( the_expr, net->isSignedSubrange() );
  }
  return the_expr;
}


NUExpr*
VhdlPopulate::createMemselRvalue( NUNet *net, NUExpr *expr,
                                  const SourceLocator &loc )
{
  NUMemoryNet *memnet = net->getMemoryNet();
  NUExpr *the_expr = new NUMemselRvalue( memnet, expr, loc );
  const UInt32 rowSize = memnet->getRowSize();
  if ( memnet->isIntegerSubrange() && rowSize < 32 &&
       ( the_expr->determineBitSize() == rowSize ))
  {
    the_expr = resizeIntegerRvalue( the_expr, net->isSignedSubrange() );
  }
  return the_expr;
}


NUExpr*
VhdlPopulate::createMemselRvalue( NUNet *net, NUExprVector *exprs, bool resize_ranged_integers,
                                  const SourceLocator &loc )
{
  NUMemoryNet *memnet = net->getMemoryNet();
  NUExpr *the_expr = new NUMemselRvalue( memnet, exprs, loc );
  if ( resize_ranged_integers ) {
    const UInt32 rowSize = memnet->getRowSize();
    if ( memnet->isIntegerSubrange() && rowSize < 32 && 
         ( the_expr->determineBitSize() == rowSize ))
    {
      the_expr = resizeIntegerRvalue( the_expr, net->isSignedSubrange() );
    }
  }
  return the_expr;
}


Populate::ErrorCode
VhdlPopulate::attributeExpr(VhAttrbType attrType,
			    vhExpr vh_expr,
			    LFContext *context,
			    SourceLocator loc,
                            NUExpr **ppExpr
			    )
{
  ErrorCode  err_code = eSuccess, temp_err_code ;
  NUExpr *pBinOp = NULL;
  NUExpr *expr1, *expr2;
  if( attrType == VH_POS  || attrType == VH_VAL ||
      attrType == VH_SUCC || attrType == VH_PRED ||
      attrType == VH_LEFTOF || attrType == VH_RIGHTOF)
  { 
    UInt64 expr_value = 0ULL;
    switch(attrType)
    {
      case VH_POS:
      case VH_VAL:
        expr_value = 0ULL;
        break;
      case VH_SUCC:
      case VH_RIGHTOF:
        expr_value = 1ULL;
        break;
      case VH_PRED:
      case VH_LEFTOF:
        expr_value = -1ULL;
        break;
      default:
        expr_value = 0ULL;
        break;
    }

    int width, numDims;
    vhExpr vh_signature = vhGetExpr( vh_expr  );
    temp_err_code = getVhExprBitSize(vh_signature, &width, &numDims, loc,
                                     context, false, true );
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    temp_err_code = expr( vh_signature, context, width , &expr1, false, true);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    expr2 = NUConst::create(true, expr_value, width , loc);
    pBinOp = new NUBinaryOp (NUOp::eBiPlus, expr1, expr2 , loc);

    *ppExpr = pBinOp;
  }
  return err_code;
}

bool VhdlPopulate::isCharacterType( vhNode node )
{
  if ( node == NULL )
  {
    return false;
  }

  vhNode vh_object = vhGetActualObj( node );
  vhObjType basetype = vhGetObjType( vh_object );
  switch (basetype) 
  {
    case VHSUBTYPEIND:
    case VHSUBTYPEDECL:
    {
      // The code for enumaration and subtype should be here
      //vhNode subTypeNode = vhGetSubTypeBaseType( vh_object );
      break;
    }
    case VHTYPEDECL:
    {
      JaguarString type_mark( vhGetTypeName( vh_object ));
      if( strcasecmp( type_mark,"character") == 0)
        return true;

      break;
    }
    default:
      break;
  }
  return false;
} 

// This function breaks memory net into vectors
Populate::ErrorCode
VhdlPopulate::breakMemoryNet(NUNet *netExpr, NUExprVector *expr_vector, const SourceLocator &loc)
{
  ErrorCode  err_code = eSuccess, temp_err_code;

  NUMemoryNet *memNet = dynamic_cast <NUMemoryNet *> (netExpr);
  const ConstantRange *memNetRange = memNet->getNetRefRange();
  SInt32 rowLsb = memNetRange->getLsb();
  SInt32 rowMsb = memNetRange->getMsb();
  UInt32 noOfRows = memNetRange->getLength();
  SInt32 incr = (rowLsb > rowMsb) ? 1 : -1;
  for(UInt32 i = 0; i < noOfRows; i++)
  {
    NUExpr *the_const = NUConst::create( false, rowMsb, 32, loc);
    NUExpr *indexedMemExp;

    temp_err_code = indexedNameRvalueHelper( memNet, the_const, loc, &indexedMemExp );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
	      
    expr_vector->push_back (indexedMemExp);
    rowMsb += incr;
  }

  return err_code;
}
