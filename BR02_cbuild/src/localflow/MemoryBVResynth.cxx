// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/MemoryBVResynth.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAlwaysBlock.h"
#include "util/StringAtom.h"
#include "util/ConstantRange.h"
#include "util/CarbonTypeUtil.h"
#include "reduce/Fold.h"


//! Add the net with the exclusion reason to the map.
static void sAddExclusion(NUNet *net,
                          MemoryBVResynth::ExclusionReason reason,
                          MemoryBVResynth::NetExclusionReason &exclusions)
{
  MemoryBVResynth::NetExclusionReason::iterator iter = exclusions.find(net);
  if (iter != exclusions.end()) {
    (*iter).second = MemoryBVResynth::ExclusionReason((*iter).second | reason);
  } else {
    exclusions[net] = reason;
  }
}


//! Add the given nets with the exclusion reason to the map.
static void sAddExclusions(NUNetSet &nets,
                           MemoryBVResynth::ExclusionReason reason,
                           MemoryBVResynth::NetExclusionReason &exclusions)
{
  for (NUNetSet::iterator nets_iter = nets.begin();
       nets_iter != nets.end();
       ++nets_iter) {
    sAddExclusion(*nets_iter, reason, exclusions);
  }
}


//! Tree rewriter to replace memories with vectors.
class MemBVRewriteFn : public NuToNuFn
{
public:
  //! constructor
  MemBVRewriteFn(Fold *fold) :
    mFold(fold)
  {}

  //! destructor
  ~MemBVRewriteFn() {}

  NUNet *operator()(NUNet *net, Phase phase)
  {
    // Only do the rewrite on the pre walk.
    if (isPost(phase)) {
      return 0;
    }

    NUNetReplacementMap::iterator iter = mReplacements.find(net);
    if (iter == mReplacements.end()) {
      return 0;
    }

    return (*iter).second;
  }

  NULvalue *operator()(NULvalue *lvalue, Phase phase)
  {
    // Only do the rewrite on the pre walk.
    if (isPost(phase)) {
      return 0;
    }

    switch (lvalue->getType()) {
    case eNUIdentLvalue:
      return xform(dynamic_cast<NUIdentLvalue*>(lvalue));
      break;

    case eNUVarselLvalue:
      return xform(dynamic_cast<NUVarselLvalue*>(lvalue));
      break;

    case eNUMemselLvalue:
      return xform(dynamic_cast<NUMemselLvalue*>(lvalue));
      break;

    default:
      return 0;
    }
  }

  NUExpr *operator()(NUExpr *expr, Phase phase)
  {
    // Only do the rewrite on the pre walk.
    if (isPost(phase)) {
      return 0;
    }

    switch (expr->getType()) {
    case NUExpr::eNUVarselRvalue:
      return xform(dynamic_cast<NUVarselRvalue*>(expr));
      break;

    case NUExpr::eNUIdentRvalue:
      return xform(dynamic_cast<NUIdentRvalue*>(expr));
      break;

    case NUExpr::eNUMemselRvalue:
      return xform(dynamic_cast<NUMemselRvalue*>(expr));
      break;

    default:
      return 0;
    }
  }

  //! Lvalue containing a whole memory.
  NULvalue *xform(NUIdentLvalue* lvalue)
  {
    NUNet *old_net = lvalue->getIdent();
    NUNetReplacementMap::iterator iter = mReplacements.find(old_net);
    if (iter == mReplacements.end()) {
      return 0;
    }

    NUNet *new_net = (*iter).second;
    NUIdentLvalue *new_lvalue = new NUIdentLvalue(new_net, lvalue->getLoc());
    new_lvalue->resize();
    delete lvalue;
    return new_lvalue;
  }

  //! Memory index with possible range part select.
  NULvalue *xform(NUVarselLvalue* lvalue)
  {
    if (not lvalue->getLvalue()->isWholeIdentifier()) {
      return 0;
    }

    NUNet *old_net = lvalue->getIdentNet(false);
    NUNetReplacementMap::iterator iter = mReplacements.find(old_net);
    if (iter == mReplacements.end()) {
      return 0;
    }

    NUMemoryNet *mem = old_net->getMemoryNet();
    NUNet *new_net = (*iter).second;
    NULvalue *new_lvalue = makeVarselLvalue(mem, lvalue->getIndex(), *(lvalue->getRange()), new_net);
    delete lvalue;
    return new_lvalue;
  }

  //! Memory index with possible range part select.
  NULvalue *xform(NUMemselLvalue* lvalue)
  {
    NUNet *old_net = lvalue->getIdent();
    NUNetReplacementMap::iterator iter = mReplacements.find(old_net);
    if (iter == mReplacements.end()) {
      return 0;
    }

    // Memory resynth should have taken care of multi-dim accesses.
    NU_ASSERT(lvalue->getNumDims() == 1, lvalue);

    NUMemoryNet *mem = old_net->getMemoryNet();
    NUNet *new_net = (*iter).second;
    ConstantRange range(mem->getRowSize() - 1, 0);
    NULvalue *new_lvalue = makeVarselLvalue(mem, lvalue->getIndex(0), range, new_net);
    delete lvalue;
    return new_lvalue;
  }

  //! Expression containing a whole memory
  NUExpr *xform(NUIdentRvalue* expr)
  {
    NUNet *old_net = expr->getIdent();
    NUNetReplacementMap::iterator iter = mReplacements.find(old_net);
    if (iter == mReplacements.end()) {
      return 0;
    }

    NUNet *new_net = (*iter).second;
    NUIdentRvalue *new_expr = new NUIdentRvalue(new_net, expr->getLoc());
    new_expr->resize(new_expr->getBitSize());
    new_expr->setSignedResult(expr->isSignedResult());
    delete expr;
    return new_expr;
  }

  //! Memory index with possible range part select.
  NUExpr *xform(NUVarselRvalue* expr)
  {
    if (not expr->getIdentExpr()->isWholeIdentifier()) {
      return 0;
    }

    NUNet *old_net = expr->getIdent();
    NUNetReplacementMap::iterator iter = mReplacements.find(old_net);
    if (iter == mReplacements.end()) {
      return 0;
    }

    NUMemoryNet *mem = old_net->getMemoryNet();
    NUNet *new_net = (*iter).second;
    NUExpr *new_expr = makeVarselRvalue(mem, expr->getIndex(), *(expr->getRange()), new_net, expr->getBitSize(), expr->isSignedResult());
    delete expr;
    return new_expr;
  }

  //! Memory index with possible range part select.
  NUExpr *xform(NUMemselRvalue* expr)
  {
    NUNet *old_net = expr->getIdent();
    NUNetReplacementMap::iterator iter = mReplacements.find(old_net);
    if (iter == mReplacements.end()) {
      return 0;
    }

    // Memory resynth should have taken care of multi-dim accesses.
    NU_ASSERT(expr->getNumDims() == 1, expr);

    NUMemoryNet *mem = old_net->getMemoryNet();
    NUNet *new_net = (*iter).second;
    ConstantRange range(mem->getRowSize() - 1, 0);
    NUExpr *new_expr = makeVarselRvalue(mem, expr->getIndex(0), range, new_net, expr->getBitSize(), expr->isSignedResult());
    delete expr;
    return new_expr;
  }

  /*!
   * Construct a NUVarselLvalue for the replacement bitvector.
   *   mem[idx][range.hi:range.lo] -> bv[normalize(idx)*stride][range.hi:range.lo]
   * Where stride is the width of a memory word.
   */
  NULvalue *makeVarselLvalue(NUMemoryNet *mem,
                             const NUExpr *old_index_expr,
                             const ConstantRange &range,
                             NUNet *new_net)
  {
    NUExpr *new_index_expr = computeIndexExpr(mem, old_index_expr);
    NULvalue *new_lvalue = new NUVarselLvalue(new_net,
                                              new_index_expr,
                                              range,
                                              old_index_expr->getLoc());
    new_lvalue->resize();
    return mFold->fold(new_lvalue);
  }

  /*!
   * Construct a NUVarselRvalue for the replacement bitvector.
   *   mem[idx][range.hi:range.lo] -> bv[normalize(idx)*stride][range.hi:range.lo]
   * Where stride is the width of a memory word.
   */
  NUExpr *makeVarselRvalue(NUMemoryNet *mem,
                           const NUExpr *old_index_expr,
                           const ConstantRange &range,
                           NUNet *new_net,
                           UInt32 bit_size,
                           bool is_signed)
  {
    NUExpr *new_index_expr = computeIndexExpr(mem, old_index_expr);
    NUExpr *new_expr = new NUVarselRvalue(new_net,
                                          new_index_expr,
                                          range,
                                          old_index_expr->getLoc());
    new_expr->setSignedResult(is_signed);
    new_expr = new_expr->makeSizeExplicit(bit_size);
    return mFold->fold(new_expr);
  }

  /*!
   * Construct the index expression for a bitvector which is replacing the memory:
   *   normalize(idx)*stride
   */
  NUExpr *computeIndexExpr(NUMemoryNet *mem,
                           const NUExpr *old_index_expr)
  {
    bool is_signed_index = old_index_expr->isSignedResult();
    CopyContext copy_ctx(0,0);

    // Compute the size of the constant stride.
    // Trying to get a tight bound here so that we emit less bound checking code.
    UInt32 constant_size = old_index_expr->getBitSize() + Log2(mem->getRowSize());
    constant_size = std::min(UInt32(32), constant_size);
    NUConst *stride = NUConst::createKnownIntConst(mem->getRowSize(),
                                                   constant_size,
                                                   false,
                                                   old_index_expr->getLoc());

    // Normalize the index, since bitvectors have normalized indexing.
    NUExpr *norm_index_expr = old_index_expr->copy(copy_ctx);
    if ((not mem->isResynthesized()) or
        (mem->isResynthesized() and mem->getNumDims() == 2)) {
      norm_index_expr = mem->normalizeSelectExpr(norm_index_expr,
                                                 1,
                                                 0,
                                                 false);
    }

    NUExpr *new_index_expr = new NUBinaryOp(is_signed_index ? NUOp::eBiSMult : NUOp::eBiUMult,
                                            norm_index_expr,
                                            stride,
                                            old_index_expr->getLoc());

    return new_index_expr;
  }

  //! Map of memories to their bitvector replacements.
  NUNetReplacementMap mReplacements;

private:
  //! Folder
  Fold *mFold;
};


/*!
 * Find nets used in places which may cause an exclusion:
 *  . Port and arg connection formals and actuals are placed in the same equivalence class
 *  . readmem
 *  . memory to memory assign, lhs and rhs are placed in the same equivalence class
 *  . memory to memory comparison, both args are placed in the same equivalence class
 *  . size-based exclusion based on dynamic indexing
 *  . complex indexing due to restrictions in codegen
 *  . TBD handle cmodel arg connections
 */
class MemoryExclusionCallback : public NUDesignCallback
{
public:
  //! constructor
  MemoryExclusionCallback(MemoryBVResynth::NetExclusionReason &exclusions,
                          MemoryBVResynth::NetEquivClasses &equivalences,
                          UInt32 dyn_idx_threshold) :
    mDynIndexThreshold(dyn_idx_threshold),
    mExclusions(exclusions),
    mEquivalences(equivalences)
  {}

  //! destructor
  ~MemoryExclusionCallback() {}

  //! Walk the whole tree
  Status operator()(Phase, NUBase *) { return eNormal; }

  //! Remember formal, actual as pairs.
  Status operator()(Phase phase, NUPortConnection *node)
  {
    if (phase == ePre) {
      NUNetSet nets;
      node->getActuals(&nets);
      for (NUNetSet::iterator iter = nets.begin(); iter != nets.end(); ++iter) {
        rememberWholePair(*iter, node->getFormal());
      }
    }
    return eNormal;
  }

  //! Remember formal, actual as pairs.
  Status operator()(Phase phase, NUTFArgConnection *node)
  {
    if (phase == ePre) {
      NUNetSet nets;
      node->getActuals(&nets);
      for (NUNetSet::iterator iter = nets.begin(); iter != nets.end(); ++iter) {
        rememberWholePair(*iter, node->getFormal());
      }
    }
    return eNormal;
  }

  //! Exclude memories used in readmem
  Status operator()(Phase phase, NUReadmemX *node)
  {
    if (phase == ePre) {
      NUNetSet nets;
      node->getLvalue()->getDefs(&nets);
      sAddExclusions(nets, MemoryBVResynth::eExclReadmem, mExclusions);
    }
    return eNormal;
  }

  //! Remember lhs,rhs as pairs if this is a whole identifier to whole identifier assign.
  Status operator()(Phase phase, NUAssign *node)
  {
    if (phase == ePre) {
      NULvalue *lhs = node->getLvalue();
      NUExpr *rhs = node->getRvalue();
      if (lhs->isWholeIdentifier() and rhs->isWholeIdentifier()) {
        rememberWholePair(lhs->getWholeIdentifier(), rhs->getWholeIdentifier());
      }
    }
    return eNormal;
  }

  //! Remember op1, op2 as pairs if this is a whole identifier operation.
  Status operator()(Phase phase, NUOp *node)
  {
    if ((phase == ePre) and (node->getType() == NUExpr::eNUBinaryOp)) {
      NUExpr *op0 = node->getArg(0);
      NUExpr *op1 = node->getArg(1);
      if (op0->isWholeIdentifier() and op1->isWholeIdentifier()) {
        rememberWholePair(op0->getWholeIdentifier(), op1->getWholeIdentifier());
      }
    }
    return eNormal;
  }

  //! Exclude based on size if this is a dynamic index.
  Status operator()(Phase phase, NUMemselLvalue *node)
  {
    if (phase == ePre) {
      NU_ASSERT(node->getNumDims() == 1, node);
      NUExpr *index = node->getIndex(0);
      if (not index->isConstant()) {
        NUNetSet nets;
        node->getDefs(&nets);
        addSizeExclusions(nets, MemoryBVResynth::eExclDynIndex, mDynIndexThreshold);
      }
    }
    return eNormal;
  }

  //! Exclude dynamic index based on size, and also due to codegen restrictions.
  Status operator()(Phase phase, NUVarselLvalue *node)
  {
    if (phase == ePre) {
      bool outer_const_idx = node->isConstIndex();

      // Due to codegen restrictions:
      // 1) Disallow outer dyn indexing where inner identifier is not simple.
      // 2) Disallow outer const indexing where inner identifier is not constant or simple.
      NULvalue *lvalue = node->getLvalue();
      switch (lvalue->getType()) {
      case eNUVarselLvalue:
        {
          NUVarselLvalue *varsel_lvalue = dynamic_cast<NUVarselLvalue*>(lvalue);
          if ((not outer_const_idx) or (not varsel_lvalue->isConstIndex())) {
            NUNetSet nets;
            lvalue->getDefs(&nets);
            sAddExclusions(nets, MemoryBVResynth::eExclCmplxDynIndex, mExclusions);
          }
        }
        break;
      case eNUMemselLvalue:
        {
          NUMemselLvalue *memsel_lvalue = dynamic_cast<NUMemselLvalue*>(lvalue);
          if ((not outer_const_idx) or (not memsel_lvalue->isConstIndex(0))) {
            NUNetSet nets;
            lvalue->getDefs(&nets);
            sAddExclusions(nets, MemoryBVResynth::eExclCmplxDynIndex, mExclusions);
          }
        }
        break;
      default:
        {
          // Not a complex indexing situation, so if dynamic, filter based on size.
          if (not outer_const_idx) {
            NUNetSet nets;
            node->getDefs(&nets);
            addSizeExclusions(nets, MemoryBVResynth::eExclDynIndex, mDynIndexThreshold);
          }
        }
        break;
      }
    }
    return eNormal;
  }

  //! Exclude based on size if this is a dynamic index.
  Status operator()(Phase phase, NUMemselRvalue *node)
  {
    if (phase == ePre) {
      NU_ASSERT(node->getNumDims() == 1, node);
      NUExpr *index = node->getIndex(0);
      if (not index->isConstant()) {
        NUNet *net = node->getIdent();
        if (net) {
          addSizeExclusion(net, MemoryBVResynth::eExclDynIndex, mDynIndexThreshold);
        }
      }
    }
    return eNormal;
  }

  //! Exclude dynamic index based on size, and also due to codegen restrictions.
  Status operator()(Phase phase, NUVarselRvalue *node)
  {
    if (phase == ePre) {
      if (not node->isConstIndex()) {
        // Disallow dynamic indexing where the identifier is not simple.
        // Due to codegen restrictions.
        NUExpr *index = node->getIndex();
        switch (index->getType()) {
        case NUExpr::eNUVarselRvalue:
          {
            NUVarselRvalue *varsel = dynamic_cast<NUVarselRvalue*>(index);
            NUNet *net = varsel->getIdentNet(false);
            if (net) {
              sAddExclusion(net, MemoryBVResynth::eExclCmplxDynIndex, mExclusions);
            }
          }
          break;
        case NUExpr::eNUMemselRvalue:
          {
            NUMemselRvalue *memsel = dynamic_cast<NUMemselRvalue*>(index);
            NUNet *net = memsel->getIdent();
            if (net) {
              sAddExclusion(net, MemoryBVResynth::eExclCmplxDynIndex, mExclusions);
            }
          }
          break;
        default:
          {
            // Not a complex dynamic indexing situation, so filter based on size.
            NUNet *net = node->getIdentNet(false);
            if (net) {
              addSizeExclusion(net, MemoryBVResynth::eExclDynIndex, mDynIndexThreshold);
            }
          }
          break;
        }
      }
    }
    return eNormal;
  }

  // TBD CModelArgConnection

private:

  //! Size threshold for memories which have dynamic indexing.
  UInt32 mDynIndexThreshold;

  //! Map to keep track of exclusions.
  MemoryBVResynth::NetExclusionReason &mExclusions;

  //! Equivalence classes based on pairwise operations.
  MemoryBVResynth::NetEquivClasses &mEquivalences;

  //! Place the two nets into an equivalence class.
  void rememberWholePair(NUNet *net1, NUNet *net2)
  {
    mEquivalences.equate(net1, net2);
  }

  //! Determine if net should be excluded based on size.
  void addSizeExclusion(NUNet *net,
                        MemoryBVResynth::ExclusionReason reason,
                        UInt32 size_threshold)
  {
    if (net->getBitSize() > size_threshold) {
      sAddExclusion(net, reason, mExclusions);
    }
  }

  //! Determine if the given nets should be excluded based on size.
  void addSizeExclusions(NUNetSet &net_set,
                         MemoryBVResynth::ExclusionReason reason,
                         UInt32 size_threshold)
  {
    for (NUNetSet::iterator iter = net_set.begin();
         iter != net_set.end();
         ++iter) {
      addSizeExclusion(*iter, reason, size_threshold);
    }
  }
};


MemoryBVResynth::MemoryBVResynth(UInt32 size_threshold,
                                 UInt32 dyn_index_threshold,
                                 UInt32 width_threshold,
                                 bool dump_stats,
                                 Fold *fold,
                                 IODBNucleus *iodb,
                                 MsgContext *msg_context) :
  mSizeThreshold(size_threshold),
  mDynIndexThreshold(dyn_index_threshold),
  mWidthThreshold(width_threshold),
  mDumpStats(dump_stats),
  mFold(fold),
  mMsgContext(msg_context),
  mIODB(iodb)
{
}


MemoryBVResynth::~MemoryBVResynth()
{
}


void MemoryBVResynth::design(NUDesign *design)
{
  MemBVRewriteFn rewriter(mFold);
  NetExclusionReason exclusions;
  NetEquivClasses equivalences;

  // Find exclusion nets (nets used in readmem, etc).
  // These exclusions can be relaxed later, if we determine it to be worth the
  // implementation cost to support bitvectors in certain places.
  MemoryExclusionCallback exclusion_callback(exclusions, equivalences, mDynIndexThreshold);
  NUDesignWalker walker(exclusion_callback, false);
  walker.design(design);

  NUModuleList module_list;
  design->getAllModules(&module_list);

  // Exclude based on size and port/protected.  At the end of
  // this, can use the union find to see what should be excluded due to
  // appearance in whole operations.
  for (NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i) {
    NUModule *module = *i;
    NUNetList net_list;
    module->getAllNets(&net_list);
    for (NUNetList::iterator nets_iter = net_list.begin();
         nets_iter != net_list.end();
         ++nets_iter) {
      NUNet *net = *nets_iter;
      if (not net->isMemoryNet()) {
        continue;
      }
      determineSizeProtectedExclusion(net->getMemoryNet(), exclusions);
    }
  }

  // Exclude all the memory nets in the expressions, that are included
  // in the sensitivity list.
  for (NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i) {
    NUModule *module = *i;
    NUAlwaysBlockList always_list;
    module->getAlwaysBlocks(&always_list);
    for (NUAlwaysBlockListIter itr = always_list.begin();
         itr != always_list.end(); ++itr)
    {
      NUAlwaysBlock* always = *itr;
      NUExprList* level_expr_list =  always->getLevelExprList();
      for(NUExprList::const_iterator itr_expr = level_expr_list->begin();
           itr_expr !=  level_expr_list->end(); ++itr_expr)
      {
       // Find the nets in the sensitivity list
        NUExpr* expr = *itr_expr;
        NUExpr::Type type = expr->getType();
        if(type == NUExpr::eNUUnaryOp)
        {
          expr = expr->getArg(0);
          type = expr->getType();
        }
        
        if(type == NUExpr::eNUMemselRvalue)
        {
          NUMemselRvalue* memsel = dynamic_cast <NUMemselRvalue*> (expr);
          NUNet* mem = memsel->getIdent();
          sAddExclusion(mem, eExclSensitivity, exclusions);
        }
      }
    }
  }

  // Use union/find to determine which nets should be excluded due to
  // appearance in whole operations.
  NetEquivClasses::EquivalencePartition *partition = equivalences.getPartition();
  for (NetEquivClasses::EquivalencePartition::iterator part_iter = partition->begin();
       part_iter != partition->end();
       ++part_iter) {
    NetEquivClasses::EquivalenceSet &equiv_set = (*part_iter).second;
    bool do_exclude = false;
    for (NetEquivClasses::EquivalenceSet::iterator set_iter = equiv_set.begin();
         set_iter != equiv_set.end();
         ++set_iter) {
      NUNet *net = *set_iter;
      if (exclusions.find(net) != exclusions.end()) {
        do_exclude = true;
        break;
      }
    }
    if (do_exclude) {
      for (NetEquivClasses::EquivalenceSet::iterator set_iter = equiv_set.begin();
           set_iter != equiv_set.end();
           ++set_iter) {
        NUNet *net = *set_iter;
        sAddExclusion(net, eExclWholeOp, exclusions);
      }
    }
  }
  delete partition;

  // Make the net replacements, keep a map of the replacements.
  for (NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i) {
    NUModule *module = *i;
    NUNetList net_list;
    module->getAllNets(&net_list);
    for (NUNetList::iterator nets_iter = net_list.begin();
         nets_iter != net_list.end();
         ++nets_iter) {
      NUNet *net = *nets_iter;

      if (not net->isMemoryNet()) {
        continue;
      }

      // Do not touch the scopes which are for vhdl records, these nets are not
      // utilized in the nucleus tree; they are just used to keep the names.
      if (net->isBlockLocal() and not net->isAllocated()) {
        continue;
      }

      NetExclusionReason::iterator excl_iter = exclusions.find(net);
      ExclusionReason reason = eExclNone;
      if (excl_iter != exclusions.end()) {
        reason = (*excl_iter).second;
      }

      NUMemoryNet *mem = net->getMemoryNet();
      if (shouldMakeBV(mem, reason)) {
        NUVectorNet *replacement = makeBV(mem);
        rewriter.mReplacements[mem] = replacement;
      }
    }
  }

  // Do the replacements.
  for (NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i) {
    NUModule *module = *i;
    module->replaceLeaves(rewriter);
  }

  //! Ensure there are no references to nets we are about to delete
  mFold->clearExprFactory();

  // Cleanup old nets.
  for (NUNetReplacementMap::iterator map_iter = rewriter.mReplacements.begin();
       map_iter != rewriter.mReplacements.end();
       ++map_iter) {
    NUNet *old_net = const_cast<NUNet*>(map_iter->first);
    old_net->getScope()->removeLocal(old_net);
    delete old_net;
  }

  if (mDumpStats) {
    dumpStats();
  }
}


void MemoryBVResynth::determineSizeProtectedExclusion(NUMemoryNet *mem,
                                                      NetExclusionReason &exclusions)
{
  // For now, discard any primary ports or protected nets.
  // This can be relaxed in the future if we determine it to be worth the implementation
  // effort.
  if (mem->isPrimaryPort() or mem->isProtected(mIODB) or mem->isHierRef()) {
    sAddExclusion(mem, eExclPrimaryProtect, exclusions);
  }

  // getBitSize() may return 0 if the memory is really big
  if (mem->getBitSize() == 0) {
    sAddExclusion(mem, eExclTooBig, exclusions);
  }

  // Any width > 32 bits seems to really make the BV
  // be too expensive; codegen uses generic fetch/deposit
  // routines in this case, which blows out the instruction count.
  if (mem->getRowSize() > mWidthThreshold) {
    sAddExclusion(mem, eExclWidth, exclusions);
  }

  if (mem->getBitSize() > mSizeThreshold) {
    sAddExclusion(mem, eExclTooBig, exclusions);
  }
}


bool MemoryBVResynth::shouldMakeBV(NUMemoryNet *, ExclusionReason reason)
{
  stats.mTotalTries++;

  if (reason & eExclPrimaryProtect) {
    stats.mExclPrimaryProtect++;
  }
  if (reason & eExclReadmem) {
    stats.mExclReadmem++;
  }
  if (reason & eExclWholeOp) {
    stats.mExclWholeOp++;
  }
  if (reason & eExclDynIndex) {
    stats.mExclDynIndex++;
  }
  if (reason & eExclWidth) {
    stats.mExclWidth++;
  }
  if (reason & eExclTooBig) {
    stats.mExclTooBig++;
  }
  if (reason & eExclCmplxDynIndex) {
    stats.mExclCmplxDynIndex++;
  }

  if (reason == eExclNone) {
    return true;
  } else {
    stats.mTotalExclusions++;
    return false;
  }
}


NUVectorNet *MemoryBVResynth::makeBV(NUMemoryNet *mem)
{
  NUScope *scope = mem->getScope();
  StringAtom *name = scope->gensym("memorybv", mem->getName()->str());
  ConstantRange range(mem->getBitSize() - 1, 0);
  NUNet *new_generic_net = scope->createTempVectorNet(name,
                                                      range,
                                                      mem->isSigned(),
                                                      mem->getLoc(),
                                                      eDMRegNet,
                                                      eNoneVectorNet);
  NUVectorNet *new_net = dynamic_cast<NUVectorNet*>(new_generic_net);
  new_net->setOriginalName(mem->getName());
  NU_ASSERT(new_net, new_generic_net);

  // Handle port replacement.
  if (mem->isPort()) {
    switch (scope->getScopeType()) {
    case NUScope::eModule:
      {
        new_net->setFlags(NetFlags(new_net->getFlags() | (mem->getFlags() & ePortMask)));
        mem->setFlags(NetFlags(mem->getFlags() & ~ePortMask));
        NUModule *module = scope->getModule();
        module->removeLocal(new_net); // scope->createTempVectorNet adds to locals, but this is a port
        NUNetVector net_vector;
        net_vector.push_back(new_net);
        module->replacePort(mem, net_vector);
        module->addLocal(mem);
      }
      break;
    case NUScope::eTask:
      {
        new_net->setFlags(NetFlags(new_net->getFlags() | (mem->getFlags() & ePortMask)));
        mem->setFlags(NetFlags(mem->getFlags() & ~ePortMask));
        NUTF *tf = dynamic_cast<NUTF*>(scope->getBase());
        tf->removeLocal(new_net); // scope->createTempVectorNet adds to locals, but this is a port
        tf->replaceArg(mem, new_net);
      }
      break;
    default:
      NU_ASSERT(false, mem); // Shouldn't have a port scoped anywhere else.
    }
  }

  return new_net;
}


void MemoryBVResynth::dumpStats()
{
  UtIO::cout() << "Memory BV Summary:" << UtIO::endl;
  UtIO::cout() << "  Total Memories Seen:        " << stats.mTotalTries << UtIO::endl;
  UtIO::cout() << "  Total Memories Excluded:    " << stats.mTotalExclusions << UtIO::endl;
  UtIO::cout() << "  Construct Exclusions:" << UtIO::endl;
  UtIO::cout() << "    Primary Port or Protect:  " << stats.mExclPrimaryProtect << UtIO::endl;
  UtIO::cout() << "    Readmem:                  " << stats.mExclReadmem << UtIO::endl;
  UtIO::cout() << "    Whole Memory Op:          " << stats.mExclWholeOp << UtIO::endl;
  UtIO::cout() << "    Complex Dyn Index:        " << stats.mExclCmplxDynIndex << UtIO::endl;
  UtIO::cout() << "  Size Exclusions:" << UtIO::endl;
  UtIO::cout() << "    Dyn Index:                " << stats.mExclDynIndex << UtIO::endl;
  UtIO::cout() << "    Width:                    " << stats.mExclWidth << UtIO::endl;
  UtIO::cout() << "    Size:                     " << stats.mExclTooBig << UtIO::endl;
}
