// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "localflow/LocalAnalysis.h"

#include "localflow/UD.h"
#include "localflow/UpdateUD.h"
#include "localflow/SensitivityList.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUAliasDB.h"

#include "PortCoercion.h"
#include "localflow/LowerPortConnections.h"
#include "Reset.h"
#include "localflow/Reorder.h"
#include "LatchRecognition.h"
#include "LFTristate.h"
#include "localflow/UnelabFlow.h"
#include "TristateWarning.h"
#include "InitialBlockCleanup.h"
#include "localflow/TFRewrite.h"
#include "localflow/RedundantAssigns.h"
#include "BreakNetCycles.h"
#include "BreakZConsts.h"
#include "RemoveLatchCycles.h"
#include "LocalHierRefResolver.h"
#include "SelfCycleReorder.h"
#include "BlastNets.h"

#include "reduce/Congruent.h"
#include "reduce/Fold.h"
#include "reduce/REMux.h"
#include "reduce/CodeMotion.h"
#include "reduce/MarkSweepUnusedTasks.h"
#include "reduce/RemoveDeadCode.h"
#include "reduce/RemoveUnelabFlow.h"
#include "reduce/SanityUnelab.h"
#include "reduce/AssignAlias.h"
#include "reduce/REIf.h"
#include "reduce/RETriSimplify.h"
#include "reduce/PriorityEncoderRewrite.h"
#include "reduce/TieNets.h"
#include "reduce/Atomize.h"
#include "reduce/BlockResynth.h"
#include "reduce/AllocAlias.h"
#include "reduce/ConvertLatches.h"
#include "reduce/TernaryGateOptimization.h"
#include "reduce/LocalConstProp.h"
#include "reduce/LocalCopyProp.h"
#include "reduce/ClockGate.h"
#include "vectorise/PortVectorisation.h"
#include "reduce/DynIdxToCase.h"
#include "reduce/DeadNetBits.h"

#include "util/ArgProc.h"
#include "util/Stats.h"
#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"
#include "compiler_driver/CarbonContext.h"
#include "localflow/MarkReadWriteNets.h"
#include "localflow/WiredNetResynth.h"

#include "iodb/IODBNucleus.h"


static const char* scVerboseAliasing = NULL;
static const char* scNoFunctionalAliases = NULL;
static const char* scVerboseCleanup = NULL;
static const char* scNoMux = NULL;
static const char* scVerboseMux = NULL;
static const char* scNoIfCase = NULL;
static const char* scNoFold = NULL;
static const char* scNoFoldCopyPropagation = NULL;
static const char* scVerboseFold = NULL;
static const char* scFlatten = NULL;
static const char* scNoFlatten = NULL;
static const char* scFlattenThreshold = NULL;
static const char* scFlattenBranches = NULL;
static const char* scFlattenModuleThreshold = NULL;
static const char* scFlattenSingleInstances = NULL;
static const char* scNoFlattenSingleInstances = NULL;
static const char* scNoFlattenHierRefTasks = NULL;
static const char* scFlattenEarly = NULL;
static const char* scFlattenEarlyThreshold = NULL;
static const char* scPreserveAllFlattenedNames = NULL;
static const char* scVerboseFlattening = NULL;
static const char* scFlattenTinyThreshold = NULL;
static const char* scFlattenParentThreshold = NULL;
static const char* scNoAtomize = NULL;
static const char* scVerboseAtomize = NULL;
static const char* scNoCopyPropagation = NULL;
static const char* scVerboseCopyPropagation = NULL;
static const char* scVerboseLocalPropagation = NULL;
static const char* scNoVectorInference = NULL;
static const char* scVerboseInference = NULL;
static const char* scVerboseCollapse = NULL;
static const char* scNoFoldCollaspedVector = NULL;
static const char* scCoercePorts = NULL;
static const char* scNoCoercePorts = NULL;
static const char* scVerboseCoercePorts = NULL;
static const char* scNoSplitPorts = NULL;
static const char* scVerboseSplitPorts = NULL;
static const char* scVectorDiscovery = NULL;
static const char* scVerboseVectorDiscovery = NULL;
static const char* scVerboseVectorPortDiscovery = NULL;
static const char* scNoRescope = NULL;
static const char* scVerboseRescope = NULL;
static const char* scRescopeMemoryLimit = NULL;
static const char* scNoSeparation = NULL;
static const char* scVerboseSeparation = NULL;
static const char* scNoUnroll = NULL;
static const char* scForceUnroll = NULL;
static const char* scVerboseUnroll = NULL;
static const char* scNoCodeMotion = NULL;
static const char* scExpressionMotion = NULL;
static const char* scNoExpressionMotion = NULL;
static const char* scVerboseCodeMotion = NULL;
static const char* scNoMergeUnelab = NULL;
static const char* scVerboseMergeUnelab = NULL;
static const char* scNoTernaryCreation = NULL;
static const char* scVerboseTernaryCreation = NULL;
static const char* scNoTernaryGateOptimization = NULL;
static const char* scNoMergeControl = NULL;
static const char* scNoExtractControl = NULL;
static const char* scVerboseMergeControl = NULL;
static const char* scVerboseExtractControl = NULL;
static const char* scVerboseConcatRewrite = NULL;
static const char* scNoConcatRewrite = NULL;
static const char* scVerboseUD = NULL;
static const char* scVerboseReset = NULL;
static const char* scVerboseReorder = NULL;
static const char* scVerboseLatches = NULL;
static const char* scVerboseTristate = NULL;
static const char* scVerboseFlow = NULL;
static const char* scVerboseSanity = NULL;
static const char* scVerboseTFRewrite = NULL;
static const char* scShareConstants = NULL;
static const char* scInlineTasks = NULL;
static const char* scInlineSingleTaskCalls = NULL;
static const char* scInlineLowCostTaskCalls = NULL;
static const char* scFoldBDD = NULL;
static const char* scNoFoldBDDCodegen = NULL;
static const char* scDisAssignAlias = NULL;
static const char* scVerboseRewritePriorityEncoders = NULL;
static const char* scNoRewritePriorityEncoders = NULL;
static const char* scRewritePriorityEncoders = NULL;
static const char* scNoCheckPullConflicts = NULL;
static const char* scVectorThreshold = NULL;
static const char* scDumpDesignLocal = NULL;
static const char* scEnableOutputSysTasks = NULL;
static const char* scNoEnableOutputSysTasks = NULL;
static const char* scVerboseBreakNetCycles = NULL;
static const char* scBlockResynthesis = NULL;
static const char* scVerboseBlockResynthesis = NULL;
static const char* scForceBlockResynthesis = NULL;
static const char* scRjcDebug = NULL;
static const char* scNoAsyncResets = NULL;
static const char* scClockGateLatch = NULL;
static const char* scVerboseClockGateLatch = NULL;
static const char* scNoGatedClock = NULL;
static const char* scVerboseGatedClock = NULL;
static const char* scDisableNetVec = NULL;
static const char* scEnableNetVec = NULL;
static const char* scLogNetVec = NULL;
static const char* scNoNetVecThroughPrimary = NULL;
static const char* scNetVecFlags = NULL;
static const char* scNetVecMaxIterations = NULL;
static const char* scNetVecMinCluster = NULL;
static const char* scNetVecPrimary = NULL;
static const char* scNetVecSuffix = NULL;
static const char* scNetVecThroughPrimary = NULL;
static const char* scReportNetVec = NULL;
static const char* scVerboseNetVec = NULL;
static const char* scDynIdxToCaseLimit = NULL;
static const char* scVerboseDynIdxToCase = NULL;
static const char* scExprMotionThreshold = NULL;
static const char* scNoDeadNetBits = NULL;

LocalAnalysis::LocalAnalysis(bool phase_stats,
			     bool module_phase_stats,
			     Stats *stats,
			     AtomicCache *string_cache,
			     NUNetRefFactory *netref_factory,
			     FLNodeFactory *flow_factory,
			     MsgContext *msg_context,
			     IODBNucleus *iodb,
			     ArgProc *arg,
			     STSymbolTable *symtab,
			     SourceLocatorFactory *loc_factory,
			     FLNodeElabFactory *flow_elab_factory,
                             const char* congruencyInfoFilename,
                             WiredNetResynth* wiredNetResynth,
                             TristateModeT tristateMode,
                             const char *file_root) :
  mPhaseStats(phase_stats),
  mModulePhaseStats(module_phase_stats),
  mStats(stats),
  mStrCache(string_cache),
  mNetRefFactory(netref_factory),
  mFlowFactory(flow_factory),
  mMsgContext(msg_context),
  mIODB(iodb),
  mArg(arg),
  mSymtab(symtab),
  mLocFactory(loc_factory),
  mFlowElabFactory(flow_elab_factory),
  mDesignCostCtx(new NUCostContext),
  mAllocAlias(NULL),
  mFlattenVerbosity(false),
  mInferenceVerbosity(false),
  mCollapseVerbosity(false),
  mLoopUnrollVerbosity(false),
  mEncoderVerbosity(false),
  mTernaryVerbosity(false),
  mControlMergeVerbosity(false),
  mControlExtractVerbosity(false),
  mUnelabMergeVerbosity(false),
  mRescopeVerbosity(false),
  mSeparationVerbosity(false),
  mCodeMotionVerbosity(false),
  mVectorMatchVerbosity(false),
  mVectorMatchPortVerbosity(false),
  mConcatRewriteVerbosity(false),
  mAtomizeVerbosity(false),
  mVerboseBreakNetCycles(false),
  mVerboseLocalPropagation(false),
  mAliasingVerbosity(0),
  mFlatteningTraceFile(NULL),
  mDesign(NULL),
  mWiredNetResynth(wiredNetResynth),
  mCongruencyInfoFilename(congruencyInfoFilename),
  mTristateMode(tristateMode),
  mFileRoot (file_root)
{
  mEncoderStats = new EncoderStatistics;
  mCodeMotionStats = new CodeMotionStatistics;
  mSplitAssigns = new LFSplitAssigns(msg_context);
  mClockGateStats = new REClockGateStats;
  mLocalConstantPropagationStatistics = new LocalPropagationStatistics;
  mLocalCopyPropagationStatistics = new LocalPropagationStatistics;
}


LocalAnalysis::~LocalAnalysis()
{
  delete mDesignCostCtx;
  delete mEncoderStats;
  delete mSplitAssigns;
  delete mCodeMotionStats;
  delete mClockGateStats;
  delete mLocalConstantPropagationStatistics;
  delete mLocalCopyPropagationStatistics;
}


void LocalAnalysis::addCommandlineArgs(ArgProc *arg)
{
  scVerboseAliasing = CRYPT("-verboseAliasing");
  scNoFunctionalAliases = CRYPT("-noFunctionalAliases");
  scVerboseCleanup = CRYPT("-verboseCleanup");
  scNoMux = CRYPT("-noMux");
  scVerboseMux = CRYPT("-verboseMux");
  scNoIfCase = CRYPT("-noIfCase");
  scNoFold = CRYPT("-nofold");
  scNoFoldCopyPropagation = CRYPT("-noFoldCopyPropagation");
  scVerboseFold = CRYPT("-verboseFold");
  scFlatten = CRYPT("-flatten");
  scNoFlatten = CRYPT("-noFlatten");
  scFlattenThreshold = CRYPT("-flattenThreshold");
  scFlattenBranches = CRYPT("-flattenBranches");
  scFlattenModuleThreshold = CRYPT("-flattenModuleThreshold");
  scFlattenSingleInstances = CRYPT("-flattenSingleInstances");
  scNoFlattenSingleInstances = CRYPT("-noFlattenSingleInstances");
  scNoFlattenHierRefTasks = CRYPT("-noFlattenHierRefTasks");
  scFlattenEarly = CRYPT("-flattenEarly");
  scFlattenEarlyThreshold = CRYPT("-flattenEarlyThreshold");
  scPreserveAllFlattenedNames = CRYPT("-preserveAllFlattenedNames");
  scVerboseFlattening = CRYPT("-verboseFlattening");
  scFlattenTinyThreshold = CRYPT("-flattenTinyThreshold");
  scFlattenParentThreshold = CRYPT("-flattenParentThreshold");
  scNoAtomize = CRYPT("-noAtomize");
  scVerboseAtomize = CRYPT("-verboseAtomize");
  scNoCopyPropagation = CRYPT("-noCopyPropagation");
  scVerboseCopyPropagation = CRYPT("-verboseCopyPropagation");
  scVerboseLocalPropagation = CRYPT("-verboseLocalPropagation");
  scNoVectorInference = CRYPT("-noVectorInference");
  scVerboseCollapse = CRYPT("-verboseCollapse");
  scNoFoldCollaspedVector = CRYPT("-noFoldCollapsedVector");
  scVerboseInference = CRYPT("-verboseInference");
  scCoercePorts = CRYPT("-coercePorts");
  scNoCoercePorts = CRYPT("-noCoercePorts");
  scVerboseCoercePorts = CRYPT("-verboseCoercePorts");
  scNoSplitPorts = CRYPT("-noSplitPorts");
  scVerboseSplitPorts = CRYPT("-verboseSplitPorts");
  scVectorDiscovery = CRYPT("-vectorDiscovery");
  scVerboseVectorDiscovery = CRYPT("-verboseVectorDiscovery");
  scVerboseVectorPortDiscovery = CRYPT("-verboseVectorPortDiscovery");
  scNoRescope = CRYPT("-noRescope");
  scVerboseRescope = CRYPT("-verboseRescope");
  scRescopeMemoryLimit = CRYPT("-rescopeMemoryLimit");
  scNoSeparation = CRYPT("-noSeparation");
  scVerboseSeparation = CRYPT("-verboseSeparation");
  scNoUnroll = CRYPT("-noUnroll");
  scForceUnroll = CRYPT("-forceUnroll");
  scVerboseUnroll = CRYPT("-verboseUnroll");
  scNoCodeMotion = CRYPT("-noCodeMotion");
  scExpressionMotion = CRYPT("-expressionMotion");
  scNoExpressionMotion = CRYPT("-noExpressionMotion");
  scExprMotionThreshold = CRYPT("-expressionMotionThreshold");
  scVerboseCodeMotion = CRYPT("-verboseCodeMotion");
  scNoMergeUnelab = CRYPT("-noMergeUnelab");
  scVerboseMergeUnelab = CRYPT("-verboseMergeUnelab");
  scNoTernaryCreation = CRYPT("-noTernaryCreation");
  scVerboseTernaryCreation = CRYPT("-verboseTernaryCreation");
  scNoTernaryGateOptimization = CRYPT("-noTernaryGateOptimization");
  scNoMergeControl = CRYPT("-noMergeControl");
  scNoExtractControl = CRYPT("-noExtractControl");
  scVerboseMergeControl = CRYPT("-verboseMergeControl");
  scVerboseExtractControl = CRYPT("-verboseExtractControl");
  scVerboseConcatRewrite = CRYPT("-verboseConcatRewrite");
  scNoConcatRewrite = CRYPT("-noConcatRewrite");
  scVerboseUD = CRYPT("-verboseUD");
  scVerboseReset = CRYPT("-verboseReset");
  scVerboseReorder = CRYPT("-verboseReorder");
  scVerboseLatches = CRYPT("-verboseLatches");
  scVerboseTristate = CRYPT("-verboseTristate");
  scVerboseFlow = CRYPT("-verboseFlow");
  scVerboseSanity = CRYPT("-verboseSanity");
  scVerboseTFRewrite = CRYPT("-verboseTFRewrite");
  scShareConstants = CRYPT("-shareConstants");
  scInlineTasks = CRYPT("-inlineTasks");
  scInlineSingleTaskCalls = CRYPT("-inlineSingleTaskCalls");
  scInlineLowCostTaskCalls = CRYPT("-inlineLowCostTaskCalls");
  scVerboseRewritePriorityEncoders = CRYPT("-verboseRewritePriorityEncoders");
  scNoRewritePriorityEncoders = CRYPT("-noRewritePriorityEncoders");
  scRewritePriorityEncoders = CRYPT("-rewritePriorityEncoders");
  scNoCheckPullConflicts = CRYPT("-noCheckPullConflicts");
  scVectorThreshold = CRYPT("-vectorThreshold");
  scDumpDesignLocal = CRYPT("-dumpDesignLocal");
  scNoEnableOutputSysTasks = CRYPT("-noEnableOutputSysTasks");
  scEnableOutputSysTasks = CRYPT("-enableOutputSysTasks");
  scVerboseBreakNetCycles = CRYPT("-verboseBreakNetCycles");
  scBlockResynthesis = CRYPT("-blockResynthesis");
  scVerboseBlockResynthesis = CRYPT("-verboseBlockResynthesis");
  scForceBlockResynthesis = CRYPT("-forceBlockResynthesis");
  scRjcDebug = CRYPT("-rjcDebug");
  scNoAsyncResets = CRYPT("-noAsyncResets");
  scClockGateLatch = CRYPT("-clockGateLatch");
  scVerboseClockGateLatch = CRYPT("-verboseClockGateLatch");
  scNoGatedClock = CRYPT("-noGatedClock");
  scVerboseGatedClock = CRYPT("-verboseGatedClock");
  scFoldBDD = CRYPT("-foldBDD");
  scNoFoldBDDCodegen = CRYPT("-noFoldBDDCodegen");
  scDisAssignAlias = CRYPT("-disableAssignmentAliasing");
  scDisableNetVec = CRYPT("-noNetVec");
  scEnableNetVec = CRYPT("-doNetVec");
  scLogNetVec = CRYPT("-logNetVec");
  scNoNetVecThroughPrimary = CRYPT ("-noNetVecThroughPrimary");
  scNetVecFlags = CRYPT("-netVec");
  scNetVecMaxIterations = CRYPT("-netVecMaxIterations");
  scNetVecMinCluster = CRYPT("-netVecMinCluster");
  scNetVecPrimary = CRYPT("-netVecPrimary");
  scNetVecSuffix = CRYPT("-netVecSuffix");
  scNetVecThroughPrimary = CRYPT ("-netVecThroughPrimary");
  scReportNetVec = CRYPT("-reportNetVec");
  scVerboseNetVec = CRYPT("-verboseNetVec");
  scDynIdxToCaseLimit = CRYPT("-dynIdxToCaseLimit");
  scVerboseDynIdxToCase = CRYPT("-verboseDynIdxToCase");
  scNoDeadNetBits = CRYPT("-noDeadNetBits");

  arg->addBool(scDisAssignAlias,
               CRYPT("Skip assignment aliasing (known to cause functional failures"),
               false, CarbonContext::ePassCarbon);

  arg->addBool(scNoMux, CRYPT("Disable mux identification and optimization."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseMux, CRYPT("Trace mux identification and optimization."), false, CarbonContext::ePassCarbon);

  arg->addBool(CRYPT("-nofoldIfCase"), 
               CRYPT("Disable the collapse of equivalent if and case statements"), false, CarbonContext::ePassCarbon);
  
  arg->addBool(CRYPT("-nofoldCaseIntoLut"), 
               CRYPT("Disable the collapsing of case statements into lookup tables"), false, CarbonContext::ePassCarbon);
  
  arg->addBool(scFoldBDD,
               CRYPT("Use BDD processing to improve expression folding effectiveness at the cost of compile speed and memory"), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(CRYPT("-noFoldBDD"), scFoldBDD);

  arg->addBool(scNoFoldBDDCodegen,
               CRYPT("Disable the use of BDD processing to improve expression folding in Codegen phase, at the cost of compile speed and memory"), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoIfCase, 
               CRYPT("Suppress converting if to switch"), false,
               CarbonContext::ePassCarbon);
  arg->addBool(scNoFold, CRYPT("Optimize expressions involving constants"), false, CarbonContext::ePassCarbon);
  arg->addBool(scNoFoldCopyPropagation, 
               CRYPT("Do not allow Fold to perform copy propagation"), false,
               CarbonContext::ePassCarbon);
  arg->putIsDeprecated(scNoFoldCopyPropagation, true);
  arg->addBool (scVerboseFold, CRYPT("Be verbose about constant folding"), 
                false, CarbonContext::ePassCarbon);

  arg->addBool (CRYPT("-traceFold"), 
                CRYPT("Trace expression folding"), false, CarbonContext::ePassCarbon);

  arg->addInt (CRYPT("-traceBDDFold"), 
                CRYPT("Flags used to trace the use of BDDs in expression folding. default(0): silent, non-zero: show source locators, bit 0x2: show expressions"), 0, false, true, CarbonContext::ePassCarbon);

  arg->addBool(scFlatten, CRYPT("Attempt to flatten modules based on cost."), true, CarbonContext::ePassCarbon);
  arg->allowUsage(scFlatten);
  arg->addBoolOverride(scNoFlatten, scFlatten, CRYPT("Disable the flattening of modules based on cost."));
  arg->allowUsage(scNoFlatten);
  arg->addToSection(CarbonContext::scModuleControl, scNoFlatten);
  arg->addSynonym(scFlatten, CRYPT("-flattenLibraries"));

  arg->addInt (scFlattenThreshold, CRYPT("Maximum module size for flattening."), 25, false, true, CarbonContext::ePassCarbon);
  arg->allowUsage(scFlattenThreshold);
  arg->addToSection(CarbonContext::scModuleControl, scFlattenThreshold);
  arg->addSynonym(scFlattenThreshold, CRYPT("-flattenLibraryThreshold"));
  arg->addSynonym(scFlattenThreshold, CRYPT("-flattenInstanceThreshold"));
  
  arg->addBool(scFlattenBranches, CRYPT("Consider internal hierarchy for flattening as well as leaf modules."), false, CarbonContext::ePassCarbon);

  arg->addInt (scFlattenModuleThreshold, CRYPT("Obsolete flattening parameter."), 500, false, false, CarbonContext::ePassCarbon);
  arg->allowUsage(scFlattenModuleThreshold);

  arg->addBool (scFlattenSingleInstances, CRYPT("Flatten singly-instantiated modules regardless of size."), true, CarbonContext::ePassCarbon);
  arg->allowUsage(scFlattenSingleInstances);
  arg->addBoolOverride(scNoFlattenSingleInstances, scFlattenSingleInstances, CRYPT("Disable the flattening of singly-instantiated modules."));
  arg->allowUsage(scNoFlattenSingleInstances);

  arg->addBool(scNoFlattenHierRefTasks, CRYPT("Disables the flattening of modules which contain hierarchically-referenced tasks."), false, CarbonContext::ePassCarbon);

  arg->addBool(scFlattenEarly, CRYPT("Perform an early, lightweight flattening pass."), false, CarbonContext::ePassCarbon);
  arg->allowUsage(scFlattenEarly);

  arg->addInt (scFlattenEarlyThreshold, CRYPT("Maximum module size for early flattening."), 5, false, true, CarbonContext::ePassCarbon);
  arg->allowUsage(scFlattenEarlyThreshold);

  arg->addBool(scPreserveAllFlattenedNames, CRYPT("Preserve hierarchy names at the expense of performance."), false, CarbonContext::ePassCarbon);
  arg->allowUsage(scPreserveAllFlattenedNames);

  arg->addInt(scVerboseFlattening, CRYPT("Verbosity level as module flattening occurs."), 0, false, true, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scOutputControl, scVerboseFlattening);

  arg->addInt(scFlattenTinyThreshold,
              CRYPT("Flatten modules of this size or smaller, even if their parents are large."),
              10, false, true, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scModuleControl, scFlattenTinyThreshold);

  arg->addInt(scFlattenParentThreshold,
              CRYPT("Avoid flattening any more children into modules of this size, unless the child moduls are tiny (see -flattenTinyThreshold)."),
              10000, false, true, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scModuleControl, scFlattenParentThreshold);

  arg->addBool(scNoAtomize, CRYPT("Do not atomize blocks."), false, CarbonContext::ePassCarbon);
  arg->allowUsage(scNoAtomize);
  arg->addBool(scVerboseAtomize, CRYPT("Be verbose as blocks are atomized."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoCopyPropagation, CRYPT("Do not perform copy-propagation."), false, CarbonContext::ePassCarbon);
  arg->allowUsage(scNoCopyPropagation);
  arg->addBool(scVerboseCopyPropagation, CRYPT("Be verbose as copy-propagation is performed."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseLocalPropagation, CRYPT("Display summary statistics for constant and copy propagation."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoVectorInference, CRYPT("Do not collapse bit-level statements into vector-level ones."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseCollapse, CRYPT("Be really verbose with collapser inference."), false, CarbonContext::ePassCarbon);
  arg->addBool(scNoFoldCollaspedVector, CRYPT("Do not fold a collapsed vector (this is useful only to debug vectorisation issues)."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseInference, CRYPT("Be verbose as vectors are inferred."), false, CarbonContext::ePassCarbon);

  arg->addBool(scCoercePorts, CRYPT("Redeclare ports based on the read/write characteristics of the design."), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scNoCoercePorts, scCoercePorts, "By default ports are redeclared based on the read/write characteristics of the design. This overrides that functionality. Turning off the feature disables the ability to process complex bidirectional ports.");

  arg->addBool(scVerboseCoercePorts, CRYPT("Generate trace information as coercion is computed."), false, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scNetControl, scNoCoercePorts);

  arg->addBool(scNoSplitPorts, CRYPT("Disable port subdivision in situations where concats and partial nets are connected to inouts."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseSplitPorts, CRYPT("Generate trace information as port splitting occurs."), false, CarbonContext::ePassCarbon);

  arg->addBool(scVectorDiscovery, CRYPT("Construct vectors from single-bit registers."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseVectorDiscovery, CRYPT("Be verbose as vectors are discovered."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseVectorPortDiscovery, CRYPT("Be verbose as vector-ports are discovered."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoRescope, CRYPT("Disable search for nets which can be made block-local."), false, CarbonContext::ePassCarbon);
  arg->addInt(scRescopeMemoryLimit, CRYPT("Do not rescope memories with more than N addresses."), 2048, false, true, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseRescope, CRYPT("Be verbose as identifying local nets."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoSeparation, CRYPT("Disable search for vector nets which can be separated into distinct parts."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseSeparation, CRYPT("Be verbose when separating vector nets."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoUnroll, CRYPT("Disable loop unrolling."), false, CarbonContext::ePassCarbon);
  arg->addBool(scForceUnroll, CRYPT("Force unrolling of loops whenever possible"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseUnroll, CRYPT("Be verbose about loop unrolling"), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoCodeMotion, CRYPT("Delay assignment execution."), false, CarbonContext::ePassCarbon);
  arg->addBool(scExpressionMotion, CRYPT("Perform simple expression propagation."), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scNoExpressionMotion, scExpressionMotion, CRYPT("Disable simple expression propagation."));
  arg->addInt(scExprMotionThreshold, CRYPT("Limit the size of expressions that can be generated as a result of expression motion."), CodeMotion::cDefExprMotionThreshold, false, false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseCodeMotion, CRYPT("Be verbose when performing code motion."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoMergeUnelab, CRYPT("Disable unelaborated merging."), false,
               CarbonContext::ePassCarbon);
  arg->addBool(scVerboseMergeUnelab, CRYPT("Be verbose when merging statements."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoTernaryCreation, CRYPT("Do not attempt to convert if-then-else trees into ternary assigns."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseTernaryCreation, CRYPT("Be verbose about if-then-else conversion."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoTernaryGateOptimization, CRYPT("Do not attempt to perform the ternary gate optimization."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoMergeControl, CRYPT("Disable control merging."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseMergeControl, CRYPT("Be verbose when merging control blocks."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoExtractControl, CRYPT("Disable control extraction."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseExtractControl, CRYPT("Be verbose when extracting control expressions."), false, CarbonContext::ePassCarbon);

  arg->addBool(scNoConcatRewrite, CRYPT("Disable the rewriting of concatenations."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseConcatRewrite, CRYPT("Be verbose when doing concat rewrite."), false, CarbonContext::ePassCarbon);

  arg->addBool(scVerboseUD, CRYPT("Be verbose as use def analysis occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseReset, CRYPT("Be verbose as reset analysis occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseReorder, CRYPT("Be verbose as reordering analysis occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseLatches, CRYPT("Be verbose as we detect latches"), 
               false, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scOutputControl, scVerboseLatches);
  arg->addBool(scVerboseTristate, CRYPT("Be verbose as tristate analysis occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseFlow, CRYPT("Be verbose as DFG creation occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseCleanup, CRYPT("Be verbose as DFG/Code cleanup occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseSanity, CRYPT("Be verbose as sanity checking occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseTFRewrite, CRYPT("Be verbose as task/function rewrite occurs"), false, CarbonContext::ePassCarbon);
  arg->addBool(scShareConstants, CRYPT("Merge constants used to initialize ports"), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(CRYPT("-noShareConstants"), scShareConstants);

  arg->addBool(scInlineTasks, "Inline tasks/functions. This switch may improve performance if there are not a large number of tasks. A large number of tasks will cause backend code bloat, which could degrade performance. The use of this switch will disable task parameter usage checks such as an uninitialized output reads. Checks are also disabled for each inlined task specified in a directive.", false, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scModuleControl, scInlineTasks);
  arg->addBool(scInlineSingleTaskCalls, "Inline tasks/functions with a single call.", false, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scModuleControl, scInlineSingleTaskCalls);
  arg->addBool(scInlineLowCostTaskCalls, "Inline tasks/functions low cost.", false, CarbonContext::ePassCarbon);

  arg->addBool(scNoCheckPullConflicts, CRYPT("Don't check for pull conflicts."), true, CarbonContext::ePassCarbon);
  arg->addBool(scNoRewritePriorityEncoders,
                     CRYPT("Disable rewriting priority encoders."), false,
               CarbonContext::ePassCarbon);
  arg->addBool(scVerboseRewritePriorityEncoders, CRYPT("Be verbose about rewriting priority encoders"), false, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scRewritePriorityEncoders,
		       scNoRewritePriorityEncoders);

  arg->addInt(scVectorThreshold, CRYPT("Maximum bit size of vectors at which no scalarization will occur"), 32, false, true, CarbonContext::ePassCarbon);

  arg->addInt(scVerboseAliasing, CRYPT("Be verbose as alias generation occurs, 1 is very verbose, 2 is only minimal output"), 0, false, true, CarbonContext::ePassCarbon);
  arg->addBool(scNoFunctionalAliases, CRYPT("Do not discover aliases through functional equivalence."), false, CarbonContext::ePassCarbon);

  arg->addBool(scDumpDesignLocal, CRYPT("Dump the design to stdout after local analysis, before graph creation"), false, CarbonContext::ePassCarbon);
  
  arg->addBool(scNoEnableOutputSysTasks, "Disables support for system tasks like: $display, $write, $fdisplay, $fwrite.", true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scEnableOutputSysTasks, scNoEnableOutputSysTasks, CRYPT("By default the Carbon compiler ignores the system tasks: $display, $write, $fdisplay, $fwrite. This option enables their inclusion in the generated model."));
  arg->addToSection(CarbonContext::scVerilog, scEnableOutputSysTasks); // makes this switch visible to customer

  arg->addBool(CarbonContext::scVhdlEnableFileIO, "By default the Carbon compiler ignores the vhdl file-IO subprograms: file_open, read, write, readline, file_close etc. This option enables their inclusion in the generated model.", false, CarbonContext::ePassCarbon);
  arg->addToSection(CarbonContext::scVHDL, CarbonContext::scVhdlEnableFileIO); // makes this switch visible to customer

  arg->addBool(scVerboseBreakNetCycles, CRYPT("Dump the statements that were broken up to break net cycles"), false, CarbonContext::ePassCarbon);

  arg->addBool(scBlockResynthesis, 
               CRYPT("Resynthesize logic within a module."), 
               false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseBlockResynthesis, 
               CRYPT("Be verbose during block resynthesis."), 
               false, CarbonContext::ePassCarbon);
  arg->addBool(scForceBlockResynthesis, 
               CRYPT("Do block resynthesis if possible, even if it looks like it makes things worse.  This is intended to help test block resynthesis correctness."), 
               false, CarbonContext::ePassCarbon);

  arg->addInt(scRjcDebug, CRYPT("Try it you might like it!"), 0, false, false, CarbonContext::ePassCarbon);

  arg->addBool(scNoAsyncResets, CRYPT("Convert all async resets into sync resets.  WARNING -- this will cause differences in simulation and hardware behavior in the timing of assertion of reset.  The switch is included to aid in reducing DCL (derived clock logic) cycles"), false, CarbonContext::ePassCarbon);
  arg->addBool(scClockGateLatch, CRYPT("Enables converting clock gating latches into trailing edge flops."), false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseClockGateLatch, CRYPT("Prints a message for every latch converted to a flop."), false, CarbonContext::ePassCarbon);
  arg->addBool(scNoGatedClock, CRYPT("Disables the gated clock transformation that reduces the number of clocks."),  false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseGatedClock, CRYPT("Prints a message for each transformed flop that uses a gated clock."), false, CarbonContext::ePassCarbon);

  arg->addInt(scDynIdxToCaseLimit, CRYPT("Rewrite dynamic vector accesses into case statements if the selector size is less than or equal to this threshold.  A value of 0 will turn off this conversion."), 5, false, false, CarbonContext::ePassCarbon);
  arg->addBool(scVerboseDynIdxToCase, CRYPT("Dump dynamic indexing to case conversion as it occurs"), false, CarbonContext::ePassCarbon);

  // Net vectorisation flags
  arg->addBool(scEnableNetVec, CRYPT("Enable net vectorisation"), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scDisableNetVec, scEnableNetVec, CRYPT("Disable net vectorisation (override -doNetVec)"));
  arg->addBool(scLogNetVec, CRYPT("Log common net vectorisation"), false, CarbonContext::ePassCarbon);
  arg->addBool(scNetVecPrimary, CRYPT("Vectorise primary ports"), false, CarbonContext::ePassCarbon); 
  arg->addBool(scNetVecThroughPrimary, CRYPT("Propagate vectorisation through primary ports but do not replace scalar primary ports with vector ports"), true, CarbonContext::ePassCarbon);
  arg->addBoolOverride(scNoNetVecThroughPrimary, scNetVecThroughPrimary, CRYPT ("Do not allow propagation of net vectorisation through primary ports"));
  arg->addBool(scVerboseNetVec, CRYPT("Writes net vectorisation results"), false, CarbonContext::ePassCarbon);
  arg->addInt(scNetVecMaxIterations, CRYPT("Set an upper bound on the number of net vectorisation propagation iterations"), 64, false, false, CarbonContext::ePassCarbon);
  arg->addInt(scNetVecMinCluster, CRYPT("Minimum size of a vector created during net vectorisation"), 0, true, false, CarbonContext::ePassCarbon);
  arg->addString(scNetVecFlags, CRYPT("Set a net vectorisation switch (developer use only)"), NULL, true, true, CarbonContext::ePassCarbon);
  arg->addString(scNetVecSuffix, CRYPT("Additional suffix for the net vectorisation report files"), NULL, true, false, CarbonContext::ePassCarbon);
  arg->addString(scReportNetVec, CRYPT("Create a net vectorisation report"), NULL, true, false, CarbonContext::ePassCarbon);
  arg->addBool(scNoDeadNetBits, CRYPT("Disable optimizing statements that write to partially dead nets."), false, CarbonContext::ePassCarbon);

  arg->addToSection (CarbonContext::scOutputControl, scVerboseNetVec);
  arg->addToSection (CarbonContext::scOutputControl, scReportNetVec);
  arg->addToSection (CarbonContext::scNetControl, scEnableNetVec);
  arg->addToSection (CarbonContext::scNetControl, scDisableNetVec);
  arg->addToSection (CarbonContext::scNetControl, scNetVecMinCluster);
  arg->addToSection (CarbonContext::scNetControl, scNetVecThroughPrimary);
  arg->addToSection (CarbonContext::scNetControl, scNetVecPrimary);
  arg->addToSection (CarbonContext::scNetControl, scNoNetVecThroughPrimary);

  // Alias the -netVec arguments to -portVec for backward compatibility
  arg->addSynonym (scDisableNetVec, "-noPortVec", true);
  arg->addSynonym (scEnableNetVec, "-doPortVec", true);
  arg->addSynonym (scLogNetVec, "-logPortVec", true);
  arg->addSynonym (scNoNetVecThroughPrimary, "-noPortVecThroughPrimary", true);
  arg->addSynonym (scNetVecFlags, "-portVec", true);
  arg->addSynonym (scNetVecMaxIterations, "-portVecMaxIterations", true);
  arg->addSynonym (scNetVecMinCluster, "-portVecMinCluster", true);
  arg->addSynonym (scNetVecPrimary, "-portVecPrimary", true);
  arg->addSynonym (scNetVecSuffix, "-portVecSuffix", true);
  arg->addSynonym (scNetVecThroughPrimary, "-portVecThroughPrimary", true);
  arg->addSynonym (scReportNetVec, "-reportPortVec", true);
  arg->addSynonym (scVerboseNetVec, "-verbosePortVec", true);
}


bool LocalAnalysis::shouldExit()
{
  return ( (mMsgContext->getSeverityCount(MsgContext::eFatal) != 0) ||
	   (mMsgContext->getSeverityCount(MsgContext::eError) != 0) );
}


bool LocalAnalysis::cleanDeadModulesEnabled(UInt32)
{
  return true;
}


void LocalAnalysis::runCleanDeadModules(NUModule*,UInt32) {
  NUModuleList mods;
  NUModuleSet liveMods;

  mDesign->getModulesBottomUp(&mods);
  for (NUModuleList::iterator p = mods.begin(), e = mods.end(); p != e; ++p) {
    NUModule* mod = *p;
    liveMods.insert(mod);
  }

  NUModuleSet::iterator not_found = liveMods.end();
  for (NUDesign::ModuleLoop p = mDesign->loopAllModules(); !p.atEnd(); ++p) {
    NUModule* mod = *p;
    if (liveMods.find(mod) == not_found) {
      mod->removeBlocks();
    }
  }
}    


void LocalAnalysis::flattenModule(NUModule *module, 
                                  bool early_flattening_pass)
{
  ArgProc::OptionStateT arg_status;
  
  bool debug_mode = (mArg->getBoolValue(scPreserveAllFlattenedNames) or 
                     mArg->getBoolValue(CRYPT("-g")));

  bool flatten_only_leaves = (not mArg->getBoolValue(scFlattenBranches));
  bool flatten_single_instances = mArg->getBoolValue(scFlattenSingleInstances);

  SInt32 tiny_threshold, parent_threshold;
  mArg->getIntLast(scFlattenTinyThreshold, &tiny_threshold);
  mArg->getIntLast(scFlattenParentThreshold, &parent_threshold);

  SInt32 verbosity_value = 0;
  arg_status = mArg->getIntLast(scVerboseFlattening,
                                &verbosity_value);
  INFO_ASSERT(arg_status==ArgProc::eKnown, "No value for verbose flattening flag");
  mFlattenVerbosity = (verbosity_value != 0);

  SInt32 flatten_threshold;
  arg_status = mArg->getIntLast(scFlattenThreshold,
                                &flatten_threshold);
  INFO_ASSERT(arg_status==ArgProc::eKnown, "No value for flattening threshold");

  bool flatten_hier_ref_tasks = not mArg->getBoolValue(scNoFlattenHierRefTasks);

  INFO_ASSERT(mFlatteningTraceFile != NULL, "No file to write flattening info into");
  Flatten flatten(mDesign, 
                  mStrCache, mNetRefFactory, mMsgContext, mIODB, mArg,
                  mDesignCostCtx,
                  mFlatteningTraceFile,
                  flatten_threshold,
                  flatten_only_leaves,
                  flatten_single_instances,
                  flatten_hier_ref_tasks,
                  early_flattening_pass,
                  debug_mode,
                  mFlattenVerbosity,
                  tiny_threshold,
                  parent_threshold,
                  &mFlattenStats);
  flatten.module(module,mFlattenMode);
} // void LocalAnalysis::flattenModule


bool LocalAnalysis::congruenceEnabled(UInt32)
{
  return mArg->getBoolValue(CRYPT("-congruency"));
}


void LocalAnalysis::runCongruence(NUModule * module, UInt32)
{
  // Run congruency on the module tasks.  This is useful to do after
  // flattening because if I flatten two instances of a module with
  // a task into its parent, I will wind up with two identical copies of the
  // task in the parent
  UInt32 paramCostLimit = 0;
  Congruent congruent(mIODB,
                      mArg->getBoolValue(CRYPT("-verboseCongruency")),
                      paramCostLimit,
                      mCongruencyInfoFilename.c_str(),
                      mMsgContext,
                      false);  // do not factor constants
  if (congruent.moduleTasks(module)) {
    clearUD(module);
    buildUD(module, true);
    buildFlow(module, false);
    clearDeadLogic(module, true); // start at all nets
    removeFlow();
    clearUD(module);
  }
}


// Perform assignment aliasing, functional equivalence, and a new continuous
// assignment aliasing pass that handles multiple drivers to a net
void LocalAnalysis::performLocalAliasing(NUModule *module,
                                         bool doBuildFlow,
                                         bool doFold,
                                         bool doRemoveFlow,
                                         bool doClearDeadLogic)
{
  // Start a new interval timer, it pops on destruction
  UtStatsInterval stats(mPhaseStats, mStats);

  if (doBuildFlow) {
    buildFlow(module, false); // do not recurse
  }

  if (doClearDeadLogic) {
    // Remove dead code from the design, and dead flow from the graph.
    // Start at all nets on this first pass. This allows assignment
    // aliasing to uncover all nets, not only those with live drivers.
    clearDeadLogic(module, true); // start at all nets
  }

  bool find_functional_aliases = not mArg->getBoolValue(scNoFunctionalAliases);

  // functional & assignment aliasing that can handled cont. and blocking 
  // assigns, plus functional equivalence, but cannot handle multiply driven
  // nets
  {
    ArgProc::OptionStateT arg_status = mArg->getIntLast(scVerboseAliasing,
                                                        &mAliasingVerbosity);
    INFO_ASSERT(arg_status == ArgProc::eKnown, "No value for verbose aliasing");
    AssignAlias alias(mNetRefFactory, mFlowFactory, mIODB, mArg, mMsgContext, 
                      mStats, mPhaseStats,
                      find_functional_aliases,
                      false,  // cont assigns only
                      mAliasingVerbosity,
                      &mAssignAliasStats);
    alias.module(module);
    stats.printIntervalStatistics("AssignAliasing");
  }

  // A separate pass is executed for continuous assignment aliasing to handle
  // multiply driven nets because:
  // 
  //   - assignment aliasing of multiply driven nets is needed for correct
  //     tristate behavior
  //   - the complex flow correction made during functional aliasing is hard
  //     to implement for multiply driven nets
  {
    AssignAlias alias(mNetRefFactory, mFlowFactory, mIODB, mArg, mMsgContext, 
                      mStats, mPhaseStats,
                      false, // find_functional_aliases,
                      true,  // cont_assigns_only
                      mAliasingVerbosity,
                      &mAssignAliasStats);
    alias.module(module);
    stats.printIntervalStatistics("ContAssignAliasing");
  }

  if (not shouldExit() && doFold && not mArg->getBoolValue (scNoFold)) {
    Fold fold(mNetRefFactory,
              mArg,
              mMsgContext, 
              mStrCache,
              mIODB,
              mArg->getBoolValue (CRYPT("-verboseFold")),
              eFoldUDValid | eFoldUDKiller | eFoldComputeUD);

    do {
      fold.clearChanged ();
      fold.module (module, false);
    } while (fold.isChanged ());
    stats.printIntervalStatistics("FoldConst");
  }

  if (doRemoveFlow && not shouldExit()) {
    removeFlow();
  }
} // void LocalAnalysis::performLocalAliasing

bool LocalAnalysis::motionAndExtractionEnabled(UInt32)
{
  return not mArg->getBoolValue("-g");
}

void
LocalAnalysis::applyMotionAndExtraction(NUModule * module, UInt32 flags)
{
  UInt32 max_iterations = 0x0000ffff & flags;
  UInt32 pass           = (0xffff0000 & flags) >> 16;
    
  // Start a new interval timer. It pops the timer on destruction
  UtStatsInterval stats(mPhaseStats, mStats);

  bool do_motion = not mArg->getBoolValue(scNoCodeMotion);
  bool do_expression_motion = (pass > 1) and mArg->getBoolValue(scExpressionMotion);
  bool do_extraction = not mArg->getBoolValue(scNoExtractControl);
  bool do_inference = not mArg->getBoolValue(scNoVectorInference);

  SInt32 expr_motion_threshold = CodeMotion::cDefExprMotionThreshold;
  mArg->getIntLast(scExprMotionThreshold, &expr_motion_threshold);

  mCodeMotionVerbosity = mArg->getBoolValue(scVerboseCodeMotion);
  mControlExtractVerbosity = mArg->getBoolValue(scVerboseExtractControl);
  mInferenceVerbosity = mArg->getBoolValue(scVerboseInference);
  mCollapseVerbosity = mArg->getBoolValue(scVerboseCollapse);

  AllocAlias * alloc_alias = NULL;
  
  if (pass > 1) {
    alloc_alias = mAllocAlias;
  } else {
    // Compute pessimistic set of possible allocation aliases for use by reordering.
    alloc_alias = new UnelabAllocAlias(module, AllocAlias::eMatchedPortSizes);
  }

  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold), 
             eFoldUDValid);     // Don't destroy defs (avoid q=q deletion)

  CodeMotion mover(mStrCache,
                   mNetRefFactory,
                   mMsgContext,
                   mIODB,
                   mArg,
                   alloc_alias,
                   &fold,
                   do_expression_motion,
                   expr_motion_threshold,
                   mCodeMotionVerbosity,
                   mCodeMotionStats);

  ControlExtract controlExtract(mStrCache,
                                mNetRefFactory,
                                mMsgContext,
                                mIODB,
                                mArg,
                                mControlExtractVerbosity,
                                &mControlExtractStats);

  Inference::Params params (mArg, mMsgContext);
  Inference infer(&fold, 
                  mNetRefFactory,
                  mMsgContext,
                  mStrCache,
                  mIODB,
                  mArg,
                  params,
                  &mInferenceStats);

  Reorder reorder(mStrCache, mNetRefFactory, mIODB, mArg,
                  mMsgContext, alloc_alias, true, false, true, false);

  UInt32 iterations = 0;

  // First, iteratively apply CodeMotion and ControlExtract. This
  // combines control structures and moves statements into these
  // control structures without affecting the incoming order. 
  
  // Code motion can make extration possibilities apparant and vice
  // versa, thus the need for iteration.
  
  // If the user has coded their design such that these
  // transformations are apparant, this first iterative pass will
  // succeed.

  bool do_another_pass = false;
  do {
    do_another_pass = false;
    ++iterations;

    if (do_motion and not shouldExit()) {
      do_another_pass |= mover.module(module);
      stats.printIntervalStatistics("CodeMotion");
    }

    if (do_extraction and not shouldExit()) {
      do_another_pass |= controlExtract.module(module);
      stats.printIntervalStatistics("ExtractControl");
    }

    if (do_inference and not shouldExit()) {
      bool success = infer.module(module); // inference does not trigger another iteration.
      stats.printIntervalStatistics("Inference");
      if (success)
        buildUD(module, true);
    }

  } while (do_another_pass and (iterations < max_iterations));

  // The second round of iterations incooperates a Reorder step.
  // Reordering exposes optimization opportunities which were not
  // visible in the original design (for ControlExtract, in
  // particular).

  do {
    do_another_pass = false;
    ++iterations;

    if (do_motion and not shouldExit()) {
      do_another_pass |= mover.module(module);
      stats.printIntervalStatistics("CodeMotion");
    }

    if (do_extraction and not shouldExit()) {
      reorder.module(module);
      stats.printIntervalStatistics("ReorderMod");
    }

    if (do_extraction and not shouldExit()) {
      do_another_pass |= controlExtract.module(module);
      stats.printIntervalStatistics("ExtractControl");
    }

    if (do_inference and not shouldExit()) {
      bool success = infer.module(module); // inference does not trigger another iteration.
      stats.printIntervalStatistics("Inference");
      if (success)
        buildUD(module, true);
    }

  } while (do_another_pass and (iterations < max_iterations));

  if (pass <= 1) {
    delete alloc_alias;
  }
} // void LocalAnalysis::applyMotionAndExtraction


void LocalAnalysis::dumpGlobalStatistics() const
{
  if (mFlatteningTraceFile) {
    mFlattenStats.print(*mFlatteningTraceFile);
  }
  if (mFlattenVerbosity) {
    mFlattenStats.print(UtIO::cout());
  }

  if (mInferenceVerbosity) {
    mInferenceStats.print();
  }

  if (mVectorMatchVerbosity) {
    mVectorMatchStats.print();
  }

  if (mLoopUnrollVerbosity) {
    mLoopUnrollStats.print();
  }

  if (mEncoderVerbosity) {
    mEncoderStats->print();
  }

  if (mTernaryVerbosity) {
    mTernaryStats.print();
  }

  if (mControlMergeVerbosity) {
    mControlMergeStats.print();
  }

  if (mControlExtractVerbosity) {
    mControlExtractStats.print();
  }

  if (mUnelabMergeVerbosity) {
    mUnelabMergeStats.print("Design");
  }

  if (mRescopeVerbosity) {
    mRescopeStats.print();
  }

  if (mSeparationVerbosity) {
    mSeparationStats.print();
  }

  if (mCodeMotionVerbosity) {
    mCodeMotionStats->print();
  }

  if (mConcatRewriteVerbosity) {
    mConcatRewriteStats.print();
  }

  if (mAtomizeVerbosity) {
    mAtomizeStats.print();
  }

  if (mVerboseBreakNetCycles) {
    mSplitAssigns->print();
  }

  if (mAliasingVerbosity) {
    mAssignAliasStats.print();
  }

  if (mArg->getBoolValue(scVerboseGatedClock)) {
    mClockGateStats->print();
  }

  if (mVerboseLocalPropagation) {
    mLocalConstantPropagationStatistics->print("Local Constant Propagation");
    mLocalCopyPropagationStatistics->print("Local Copy Propagation");
  }
}


void LocalAnalysis::analyzeDesignPorts(NUDesign * design)
{
  INFO_ASSERT(mDesign==NULL, "Local analysis design already set.");
  mDesign = design;

  if (mPhaseStats) {
    mStats->pushIntervalTimer();
  }

  // Collapse vectorisable ports
  if (mArg->getBoolValue(scEnableNetVec) && !mArg->getBoolValue("-g")) {
    PortVectoriser::Options options (mArg, mMsgContext, mFileRoot);
    PortCollapser portvec (mStrCache, mIODB, mStats, mMsgContext, mNetRefFactory, mPhaseStats, options);
    portvec.design (design);
    if (mPhaseStats) {
      mStats->printIntervalStatistics ("PortVectorisation");
    }
  }
  // Coerce port directions to match use
  if (mArg->getBoolValue(scCoercePorts)) {
    PortCoercion coerce(mStrCache, 
                        mIODB, 
                        mStats, 
                        mArg, 
                        mMsgContext, 
                        mNetRefFactory, 
                        mLocFactory, 
                        (not mArg->getBoolValue(scNoSplitPorts)),
                        mArg->getBoolValue(scVerboseSplitPorts),
                        mArg->getBoolValue(scVerboseCoercePorts));
    coerce.design(design, mPhaseStats);
    if (mPhaseStats)
      mStats->printIntervalStatistics("PortCoercion");
  }

  if (mPhaseStats) {
    mStats->popIntervalTimer();
  }

  mDesign = NULL;
}


void LocalAnalysis::analyzeDesign(NUDesign *design, 
                                  AnalysisMode analysisMode, 
                                  UtOStream * flattening_trace_file)
{
  INFO_ASSERT(mDesign==NULL, "Local analysis design already set.");
  mDesign = design;

  INFO_ASSERT(mFlatteningTraceFile==NULL, "Local analysis tracefile already set.");
  mFlatteningTraceFile = flattening_trace_file;

  Stats * global_stats = mStats;
  AccumStats *accum_stats = 0;

  if (mPhaseStats) {
    mStats->pushIntervalTimer();
  }

  // Try to resolve any hierarchical references locally.
  // Only need to do this once in early minimal analysis.
  // Do this as early as possible, since this potentially
  // gets rid of hierrefs.
  if (analysisMode == eAnalysisModeEarlyFlatten) {
    LocalHierRefResolver resolver(mStrCache);
    resolver.design(design);
    if (mPhaseStats)
      mStats->printIntervalStatistics("LocalHR");
  }

  // Handle blastNet directives.
  // This is done before port coercion and lowering, because blasting
  // a net could leave a concat as a port actual.  We only do this once in
  // the early mimimal analysis.
  if (analysisMode == eAnalysisModeEarlyFlatten) {
    if (mIODB->hasBlastedNets()) {
      bool verboseFold = mArg->getBoolValue(scVerboseFold);
      mSeparationVerbosity = mArg->getBoolValue(scVerboseSeparation);
      BlastNets blastNets(mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
                          verboseFold, mSeparationVerbosity);
      blastNets.design(design, &mSeparationStats);
      if (mPhaseStats)
        mStats->printIntervalStatistics("BlastNets");
    }
  }

  // Apply the tie nets so that we create the design with all those
  // things already optimized to some extent. We only do this once in
  // the early mimimal analysis.
  if (analysisMode == eAnalysisModeEarlyFlatten) {
    NUNetSet* tieNetTemps = design->getTieNetTemps();
    Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
               mArg->getBoolValue(scVerboseFold), 
               eFoldUDValid);     // Don't destroy defs (avoid q=q deletion)
    RETieNets tieNets(design, mStrCache, mIODB, mMsgContext, &fold,
                      true, true, tieNetTemps);
    tieNets.processDirectives();
    tieNets.optimize(true);
    if (mPhaseStats)
      mStats->printIntervalStatistics("TieNets");
  }

  // Perform task and function rewrite.  Do this before net marking so that get
  // non-task-local nets marked correctly.
  //
  // Note there is no harm in calling this multiple times, but there is also
  // no reason for it.  So, just call during first analysis phases.
  if (analysisMode == eAnalysisModeEarlyFlatten) {
    TFRewrite tf_rewrite(mNetRefFactory,
                         mMsgContext,
                         mStrCache,
                         mArg->getBoolValue(scInlineSingleTaskCalls),
                         mArg->getBoolValue(scInlineLowCostTaskCalls),
                         mArg->getBoolValue(scVerboseTFRewrite),
                         mIODB,
                         mArg);
    tf_rewrite.design(design);
    if (mPhaseStats)
      mStats->printIntervalStatistics("TFRewrite");
  }

  // Mark nets as read/written so that the protected sets work correctly.
  {
    MarkReadWriteNets marker(mIODB, mArg, true);
    marker.design(design);
    if (mPhaseStats)
      mStats->printIntervalStatistics("MarkReadWriteNets");
  }

  // For the analyses where flattening is not happening, use a more optimistic AllocAlias.
  if (analysisMode == eAnalysisModeMinimal or
      analysisMode == eAnalysisModePostFlatten) {
    // If we are after flattening, all port-lowering and port
    // transformations have been completed. Therefore, use a less
    // pessimistic form of AllocAlias.
    AllocAlias::PortStatus portStatus = (analysisMode == eAnalysisModePostFlatten) ?
      AllocAlias::eMatchedPortSizes : AllocAlias::eMismatchedPortSizes;
    mAllocAlias = new DesignAllocAlias(design, portStatus);
  }

  // Calculate an initial costing over the design.
  // This is used by flattening to determine how many times a module is instantiated.
  {
    mDesignCostCtx->calcDesign(design);
    if (mPhaseStats)
      mStats->printIntervalStatistics("DesignCost");
  }

  // Create a stats accumulator when module-level stats are not desired.
  if (mPhaseStats and not mModulePhaseStats) {
    accum_stats = new AccumStats(mStats);
    mStats = accum_stats;
  }

  // Setup the phases for the analysis of each module
  Phases phases;
  switch (analysisMode) {
    case eAnalysisModeEarlyFlatten:
      setupEarlyFlattenAnalyzeModule(&phases);
      break;

    case eAnalysisModeMinimal:
      setupMinimumAnalyzeModule(&phases);
      break;

    case eAnalysisModeNormal:
      setupAnalyzeModule(&phases);
      break;

    case eAnalysisModePostFlatten:
      setupPostFlattenAnalyzeModule(&phases);
      break;

    case eAnalysisModePort:
      INFO_ASSERT(0, "Unexpected module traversal during port analysis.");
      break;
  }

  // Recursively walk the modules bottom up and analyze them. 
  mVisitedModules.clear();
  for (NUDesign::ModuleLoop loop = design->loopTopLevelModules();
       not loop.atEnd();
       ++loop) {
    analyzeModuleRecurse(*loop, analysisMode, FlattenAnalyzer::eModeUnknown,
                         phases);
  }
  dumpGlobalStatistics();
  
  if (mPhaseStats) {
    if (not mModulePhaseStats) {
      accum_stats->printAllAccumulated();
      delete accum_stats;
      mStats = global_stats;
    }
    mStats->popIntervalTimer();
  }

  switch (analysisMode) {
    case eAnalysisModeEarlyFlatten:
      mStats->printIntervalStatistics("EarlyFlatModuleAnalysis");
      break;
    case eAnalysisModeMinimal:
      mStats->printIntervalStatistics("GO ModuleAnalysis");
      break;
    case eAnalysisModePort:
      INFO_ASSERT(0, "Unexpected module traversal during port analysis.");
      break;
    case eAnalysisModeNormal:
      mStats->printIntervalStatistics("ModuleAnalysis");
      break;
    case eAnalysisModePostFlatten:
      mStats->printIntervalStatistics("PostFlattenAnalysis");
      break;
  }

  // For the minimum analysis, delete the more optimistic AllocAlias
  if (analysisMode == eAnalysisModeMinimal or
      analysisMode == eAnalysisModePostFlatten) {
    delete mAllocAlias;
  }

  // Dump a design if requested
  if (analysisMode == eAnalysisModePostFlatten) {
    if (mArg->getBoolValue(scDumpDesignLocal)) {
      design->print(true, 0);
    }
  }

  if (analysisMode == eAnalysisModeNormal) {
    // Clean the expression map in the NUAliasDB.
    NUAliasBOM* alias_bom = mDesign->getAliasBOM();
    alias_bom->sanitizeExpressions();
  }

  // Create persistent unelaborated flow graph.
  if ((analysisMode == eAnalysisModeMinimal) || 
      (analysisMode == eAnalysisModePostFlatten)) {
    if (not shouldExit()) {
      bool removeDeadTasks = ! mArg->getBoolValue(CRYPT("-noRemoveUnusedTasks"));
      if (removeDeadTasks) {
        // Remove unused constructs. Currently, only unused tasks are
        // removed.  Don't warn though because it wasn't user
        // sloppiness -- it was an optimization
        // (e.g. flattening+congruence) that we eliminated the need
        // for a task body.
        MarkSweepUnusedTasks msUnused(mMsgContext, false /* verbose */);
        // if a recursive task enable is found then shouldExit will protect the execution path
        bool recursion_found = false;
        (void) msUnused.walkAndRemove(mDesign,&recursion_found);
        if (mPhaseStats) {
          mStats->printIntervalStatistics(CRYPT("DeadTaskCleanup"));
        }
      }
    }

    bool recomputeExtUD = true;

    // Perform Congruence analysis, factoring constants
    if ((analysisMode == eAnalysisModePostFlatten) and
        mArg->getBoolValue(CRYPT("-congruency")) and
        not mArg->getBoolValue(CRYPT("-noLateCongruency")) and
        not shouldExit())
    {
      SInt32 paramCostLimit;
      (void) mArg->getIntLast(CRYPT("-congruencyParamCostLimit"),
                              &paramCostLimit);
      Congruent congruent(mIODB,
                          mArg->getBoolValue(CRYPT("-verboseCongruency")),
                          paramCostLimit,
                          mCongruencyInfoFilename.c_str(),
                          mMsgContext,
                          true);  // factor constants
      if (congruent.designModules(mDesign)) {
        // Recompute all of UD, therefore we don't have to compute ext UD
        recomputeExtUD = false;
        NUModuleList mods;
        design->getModulesBottomUp(&mods);
        UD ud(mNetRefFactory, mMsgContext, mIODB, mArg,
              mArg->getBoolValue(scVerboseUD));
        for (NUModuleList::iterator p = mods.begin(), e = mods.end();
             p != e; ++p)
        {
          NUModule* mod = *p;
          lowerPorts(mod, true);
          ud.module(mod, true);
        }
        if (mPhaseStats)
          mStats->printIntervalStatistics("UseDef");
      }
    } // if

    // Re-compute UD to update hierarchical task-enables whose tasks have been
    // optimized after the task-enable was processed.  This should only be
    // needed if there is a hierarchical task call to a task in a higher
    // level module.  See test/hierref_unique/call_up.v, which if run without
    // flattening, will crash without the next 4 lines.  This distills a
    // netEffect testcase.
    if (not shouldExit() && recomputeExtUD) {
      UDCallback udCallback(mNetRefFactory, mMsgContext, mIODB, mArg);
      NUDesignWalker updateExtNetUDWalker(udCallback,false);
      updateExtNetUDWalker.design(mDesign);
    }

    if (not shouldExit()) {
      UnelabFlow unelab_flow(mFlowFactory, mNetRefFactory, mMsgContext,
                             mIODB, mArg->getBoolValue(scVerboseFlow));
      unelab_flow.design(design);
      if (mPhaseStats)
        mStats->printIntervalStatistics("Flow");
    }

    // NOTE: Do not perform a dead code pass here; REAlias needs to see a complete flow graph.

    // Sanity check
    if (not shouldExit()) {
      SanityUnelab sanity(mMsgContext, mNetRefFactory, mFlowFactory, 
                          true, mArg->getBoolValue(scVerboseSanity));
      sanity.design(design);
      if (mPhaseStats)
        mStats->printIntervalStatistics("SanityUnelab");
    }
  }

  mFlatteningTraceFile = NULL;
  mDesign = NULL;
}


void
LocalAnalysis::analyzeModuleRecurse(NUModule *module, AnalysisMode analysisMode,
                                    FlattenAnalyzer::Mode flattenMode,
                                    const Phases& phases)
{
  if (haveVisited(module)) {
    return;
  }

  FlattenAnalyzer::Mode myFlattenMode = FlattenAnalyzer::eModeUnknown;
  if (analysisMode!=eAnalysisModeMinimal) {
    myFlattenMode = FlattenAnalyzer::determineFlattenMode(module, NULL,
                                                          flattenMode);
  }

  NUModuleList inst_list;
  module->getInstantiatedModules(&inst_list);
  for (NUModuleListIter iter = inst_list.begin();
       iter != inst_list.end();
       ++iter) {
    analyzeModuleRecurse(*iter, analysisMode, myFlattenMode, phases);
  }

  mFlattenMode = myFlattenMode;
  analyzeModule(module, phases);
  rememberVisited(module);
}

void LocalAnalysis::analyzeModule(NUModule* module, const Phases& phases)
{
  if (mPhaseStats && mModulePhaseStats) {
    UtIO::cout() << "SLx: Begin analyze module " << module->getName()->str()
                 << UtIO::endl;
  }

  // In a loop run the phases, unless we get to a point where we should exit
  for (UInt32 i = 0, n = phases.size(); !shouldExit() && (i < n); ++i) {
    // Test whether this phase is enabled. This is done during
    // processing instead of at setup time, because it gives the
    // passes more flexibility.
    const Phase& phase = phases[i];
    PhaseEnabled enabled = phase.getEnabledFn();
    UInt32 flags = phase.getFlags();
    if ((this->*enabled)(flags)) {
      // Run the phase
      PhaseFn fn = phase.getPhaseFn();
      (this->*fn)(module, flags);

      module->sanityCheckAlwaysBlockPrioChain();

      // If they require stats, print them now
      if (mPhaseStats && phase.getPrintStats()) {
        mStats->printIntervalStatistics(phase.getName().c_str());
      }
    }
  }
    
  if (mPhaseStats && mModulePhaseStats) {
    UtIO::cout() << "SLx: End analyze module " << module->getName()->str()
                 << UtIO::endl;
  }
} // void LocalAnalysis::analyzeModule


bool LocalAnalysis::earlyFlattenEnabled(UInt32)
{
  return mArg->getBoolValue(scFlattenEarly);
}

void LocalAnalysis::runEarlyFlattening(NUModule* module, UInt32)
{
  flattenModule(module, true);
}

void
LocalAnalysis::setupEarlyFlattenAnalyzeModule(Phases* phases)
{
  // During this early flattening pass, we do not want to allow any
  // port-lowering. Port coercion/splitting require un-lowered ports.

  // Flatten skinny modules
  Phase flatten("Flattening", 0, true /* early flattening */,
                &LocalAnalysis::runEarlyFlattening,
                &LocalAnalysis::earlyFlattenEnabled);
  phases->push_back(flatten);

  // Remove the blocks from dead modules, but leave the nets
  Phase cleanDeadModules("CleanDeadModules", 0, 0,
                         &LocalAnalysis::runCleanDeadModules,
                         &LocalAnalysis::cleanDeadModulesEnabled);
  phases->push_back(cleanDeadModules);
}

bool LocalAnalysis::lowerPortsEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runLowerPorts(NUModule* module, UInt32 flags)
{
  lowerPorts(module, flags != 0);
}

bool LocalAnalysis::flattenModuleEnabled(UInt32)
{
  return mArg->getBoolValue(scFlatten);
}

void LocalAnalysis::runFlattenModule(NUModule* module, UInt32)
{
  flattenModule(module, false);
}

bool LocalAnalysis::vectorMatchEnabled(UInt32)
{
  return (mArg->getBoolValue(scVectorDiscovery) and
          (not mArg->getBoolValue("-g")));
}

void LocalAnalysis::runVectorMatch(NUModule* module, UInt32)
{
  mVectorMatchVerbosity     = mArg->getBoolValue(scVerboseVectorDiscovery);
  mVectorMatchPortVerbosity = mArg->getBoolValue(scVerboseVectorPortDiscovery);
  VectorMatch vector_match(mStrCache,
                           mNetRefFactory,
                           mMsgContext,
                           mIODB,
                           mArg,
                           mVectorMatchVerbosity,
                           mVectorMatchPortVerbosity,
                           &mVectorMatchStats);
  vector_match.module(module);
}

bool LocalAnalysis::inferenceEnabled(UInt32)
{
  return (not (mArg->getBoolValue(scNoVectorInference) or
               mArg->getBoolValue("-g")));
}

void LocalAnalysis::runInference(NUModule* module, UInt32 flags)
{
  mInferenceVerbosity = mArg->getBoolValue(scVerboseInference);
  mCollapseVerbosity = mArg->getBoolValue(scVerboseCollapse);
  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold), 
             eFoldUDKiller);

  Inference::Params params (mArg, mMsgContext, flags);
  Inference infer(&fold, 
                  mNetRefFactory,
                  mMsgContext,
                  mStrCache,
                  mIODB,
                  mArg,
                  params,
                  &mInferenceStats);
  infer.module(module);
}

bool LocalAnalysis::separationEnabled(UInt32)
{
  return (not mArg->getBoolValue(scNoSeparation));
}

void LocalAnalysis::runSeparation(NUModule* module, UInt32)
{
  UInt32 separationMode = Separation::eSeparateVectors;
  if (mArg->getBoolValue("-g"))
    separationMode |= Separation::eCreateContAssigns;
  SInt32 threshold;
  ArgProc::OptionStateT arg_status = mArg->getIntLast(scVectorThreshold, &threshold);
  INFO_ASSERT(arg_status == ArgProc::eKnown, "No value for vector threshold");
  mSeparationVerbosity = mArg->getBoolValue(scVerboseSeparation);
  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold), eFoldUDKiller);
  Separation separate(threshold,
                      mStrCache,
                      mNetRefFactory,
                      mMsgContext,
                      mIODB,
                      &fold,
                      separationMode,
                      mSeparationVerbosity,
                      &mSeparationStats);
  separate.module(module);
} // void LocalAnalysis::runSeparation

bool LocalAnalysis::blockResynthesisEnabled(UInt32)
{
  return mArg->getBoolValue(scBlockResynthesis);
}

void LocalAnalysis::runBlockResynthesis(NUModule* module, UInt32)
{
  // Block resynthesis needs 'fold' to run to ensure that assignment
  // RHS are sized according to the LHS, otherwise flatten/concat5.v
  // substitutes a 3-bit RHS for a 2-bit LHS and makes a concatenation
  // out of that, getting the wrong answer.
  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold), eFoldUDKiller);
  fold.module(module);

  // Block resynthesis needs accurate UD information
  bool verbose_block_resynth = mArg->getBoolValue(scVerboseBlockResynthesis);
  bool force_block_resynth = mArg->getBoolValue(scForceBlockResynthesis);
  buildUD(module, true);
  BlockResynth blkResynth(mNetRefFactory,
                          mFlowFactory,
                          mArg,
                          mMsgContext,
                          mIODB,
                          mStrCache,
                          verbose_block_resynth,
                          force_block_resynth,
                          mAllocAlias);
  bool modified = blkResynth.module(module);
  if (modified) {
    clearUD(module);
    buildUD(module, true);
    buildFlow(module, false);
    clearDeadLogic(module, true); // start at all nets
    removeFlow();
    buildUD(module, true);
  }
  MarkReadWriteNets marker(mIODB, mArg, true);
  marker.module(module);
} // void LocalAnalysis::runBlockResynthesis

bool LocalAnalysis::remuxEnabled(UInt32)
{
  return (not mArg->getBoolValue (scNoMux));
}

void LocalAnalysis::runRemux(NUModule* module, UInt32 flags)
{
  UInt32 min_bit_size = 0;
  UInt32 min_term_count = 0;
  UInt32 processTernaryOps = false;
  UInt32 updateUD = false;

  // Flags is a pass variable. In the first pass (pass 0) it does less
  // as shown by the variables set below.
  if (flags == 0) {
    min_bit_size = 1;
    min_term_count = 0;
    processTernaryOps = false;
    updateUD = false;
  } else if (flags == 1) {
    min_bit_size = 32;
    min_term_count = 2;
    processTernaryOps = true;
    updateUD = true;
  } else {
    INFO_ASSERT((flags == 0) || (flags == 1), "Unknown Remux pass");
  }
  
  REMux muxes(mNetRefFactory, mMsgContext, mStrCache, mIODB,
              mArg,
              min_bit_size, // don't make this transformation
              // at all for scalars
              min_term_count,
              processTernaryOps,
              updateUD,
              mArg->getBoolValue(scVerboseMux));
  muxes.module(module);
} // void LocalAnalysis::runRemux

bool LocalAnalysis::redundantAssignsEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runRedundantAssigns(NUModule* module, UInt32)
{
  RedundantAssigns redundant;
  redundant.module(module);
}

bool LocalAnalysis::foldEnabled(UInt32)
{
  return (not mArg->getBoolValue (scNoFold));
}

void LocalAnalysis::runFold(NUModule* module, UInt32 foldFlags)
{
  // Keep folding while something continues to change
  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold),
             foldFlags);

  do {
    fold.clearChanged ();
    fold.module (module, false);
  } while (fold.isChanged ());
}

bool LocalAnalysis::priorityEncoderRewriteEnabled(UInt32)
{
  return (not (mArg->getBoolValue(scNoRewritePriorityEncoders) or
               mArg->getBoolValue("-g")));
}

void LocalAnalysis::runPriorityEncoderRewrite(NUModule* module, UInt32)
{
// Compute pessimistic set of possible allocation aliases for use by reordering.
  UnelabAllocAlias alloc_alias(module, AllocAlias::eMatchedPortSizes);

  mEncoderVerbosity = mArg->getBoolValue(scVerboseRewritePriorityEncoders);
  PriorityEncoderRewrite rewrite(mEncoderStats, mNetRefFactory,
                                 mMsgContext, mStrCache, mIODB, 
                                 mArg, &alloc_alias, mEncoderVerbosity);
  rewrite.module(module);
}

bool LocalAnalysis::ternaryEnabled(UInt32)
{
  return (not (mArg->getBoolValue(scNoTernaryCreation) or
               mArg->getBoolValue("-g")));
}

void LocalAnalysis::runTernary(NUModule* module, UInt32)
{
  mInferenceVerbosity = mArg->getBoolValue(scVerboseInference);
  mTernaryVerbosity   = mArg->getBoolValue(scVerboseTernaryCreation);
  Ternary ternary(mStrCache,
                  mNetRefFactory,
                  mMsgContext,
                  mIODB,
                  mArg,
                  true,  // allow bitsel conditions
                  true,  // allow balanced conditionals.
                  true,  // allow bitsel definitions.
                  false, // do not fold results.
                  mTernaryVerbosity,
                  &mTernaryStats,
                  &mInferenceStats);
  ternary.module(module);
} // void LocalAnalysis::runTernary

bool LocalAnalysis::controlMergeEnabled(UInt32)
{
  return (not (mArg->getBoolValue(scNoMergeControl) or
               mArg->getBoolValue("-g")));
}

void LocalAnalysis::runControlMerge(NUModule* module, UInt32)
{
  mInferenceVerbosity    = mArg->getBoolValue(scVerboseInference);
  mControlMergeVerbosity = mArg->getBoolValue(scVerboseMergeControl);
  ControlMerge controlMerge(mStrCache,
                            mNetRefFactory,
                            mMsgContext,
                            mIODB,
                            mArg,
                            mControlMergeVerbosity,
                            &mControlMergeStats,
                            &mInferenceStats);
  controlMerge.module(module);
} // void LocalAnalysis::runControlMerge

bool LocalAnalysis::aliasingEnabled(UInt32)
{
  return not mArg->getBoolValue(scDisAssignAlias);
}

void LocalAnalysis::runAliasing(NUModule* module, UInt32 flags)
{
  bool doBuildFlow = (flags & eAliasingBuildFlow) != 0;
  bool doFold = (flags & eAliasingDoFold) != 0;
  bool doRemoveFlow = (flags & eAliasingRemoveFlow) != 0;
  bool doClearDeadLogic = (flags & eAliasingClearDeadLogic) != 0;

  performLocalAliasing(module, doBuildFlow, doFold, doRemoveFlow, doClearDeadLogic);
}

bool LocalAnalysis::breakNetCyclesEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runBreakNetCycles(NUModule* module, UInt32)
{
  // break net cycles uses fold to make the new assignment
  // statements.
  Fold fold(mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB, false,
            /* eFoldUDValid | */ eFoldComputeUD | eFoldUDKiller);

  mVerboseBreakNetCycles = mArg->getBoolValue(scVerboseBreakNetCycles);
  LFBreakNetCycles breakNetCycles(mNetRefFactory, mStrCache, &fold,
                                  mVerboseBreakNetCycles, mSplitAssigns);
  breakNetCycles.module(module);
}

bool LocalAnalysis::removeLatchCyclesEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runRemoveLatchCycles(NUModule* module, UInt32)
{
  RemoveLatchCycles removeLatchCycles(mNetRefFactory, 
                                      mMsgContext,
                                      mIODB,
                                      mArg);
  removeLatchCycles.module(module);
}

bool LocalAnalysis::deadNetBitsEnabled(UInt32)
{
  return (not (mArg->getBoolValue(scNoDeadNetBits) or mArg->getBoolValue("-g")));
}

void LocalAnalysis::runDeadNetBits(NUModule* module, UInt32)
{
  // This optimization needs flow to find all live used bits
  buildFlow(module, false);

  // Optimize the used bits, this invalidates UD and flow
  REDeadNetBits deadNetBits(mNetRefFactory, mIODB, mStrCache, mArg, mMsgContext);
  bool changed = deadNetBits.module(module);

  // Clean up, we remove the temporary flow and possibly lower ports
  // and rebuild the UD if optimizations occured.
  removeFlow();
  if (changed) {
    lowerPorts(module, true);
    clearUD(module);
    buildUD(module, true);
  }
}


bool LocalAnalysis::latchRecognitionEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runLatchRecognition(NUModule* module, UInt32)
{
  LatchRecognition lr(mNetRefFactory, mMsgContext, 
                      mArg->getBoolValue(scVerboseLatches));
  lr.module(module);
}

bool LocalAnalysis::clockGateEnabled(UInt32)
{
  return (not mArg->getBoolValue(scNoGatedClock));
}

void LocalAnalysis::runClockGate(NUModule* module, UInt32)
{
  buildFlow(module, false);
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArg,
        mArg->getBoolValue(scVerboseUD));
  bool verbose = mArg->getBoolValue(scVerboseGatedClock);
  REClockGate clockGate(mFlowFactory, mNetRefFactory, mStrCache,
                        mMsgContext, mIODB, mArg, &ud, mClockGateStats, verbose);
  clockGate.module(module);
  removeFlow();
}

bool LocalAnalysis::convertLatchesEnabled(UInt32)
{
  return (mArg->getBoolValue(scClockGateLatch) or
          mArg->getBoolValue(scVerboseGatedClock));
}

void LocalAnalysis::runConvertLatches(NUModule* module, UInt32)
{
  buildFlow(module, false);
  bool convert = mArg->getBoolValue(scClockGateLatch);
  bool verbose = mArg->getBoolValue(scVerboseClockGateLatch);
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArg,
        mArg->getBoolValue(scVerboseUD));
  REConvertLatches convertLatches(mFlowFactory, mNetRefFactory, mStrCache,
                                  mMsgContext, mIODB, &ud, mArg, convert, verbose);
  convertLatches.module(module);
  removeFlow();
}

bool LocalAnalysis::mergeUnelabEnabled(UInt32)
{
  return (not (mArg->getBoolValue(scNoMergeUnelab) or
               mArg->getBoolValue("-g")));
}

void LocalAnalysis::runMergeUnelab(NUModule* module, UInt32)
{
  mUnelabMergeVerbosity = mArg->getBoolValue(scVerboseMergeUnelab);
  UnelabMerge unelabMerge(mStrCache,
                          mNetRefFactory,
                          mMsgContext,
                          mIODB,
                          mArg,
                          mUnelabMergeVerbosity,
                          &mUnelabMergeStats);
  unelabMerge.module(module);
}

bool LocalAnalysis::rescopeEnabled(UInt32)
{
  // rescope has visibility effect. disable under -g.
  return ( not ( mArg->getBoolValue(scNoRescope) or
                 mArg->getBoolValue("-g") ) );
}

void LocalAnalysis::runRescope(NUModule* module, UInt32)
{
  mRescopeVerbosity = mArg->getBoolValue(scVerboseRescope);

  SInt32 memory_limit = 0;
  ArgProc::OptionStateT arg_status;
  arg_status = mArg->getIntLast(scRescopeMemoryLimit,
                                &memory_limit);
  INFO_ASSERT(arg_status == ArgProc::eKnown, "No value for rescope memory limit");

  Rescope rescope(mStrCache,
                  mNetRefFactory,
                  mMsgContext,
                  mIODB,
                  mArg,
                  memory_limit,
                  mRescopeVerbosity,
                  &mRescopeStats);
  rescope.module(module);
} // void LocalAnalysis::runRescope

bool LocalAnalysis::dynIdxToCaseEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runDynIdxToCase(NUModule *module, UInt32)
{
  SInt32 size_limit = 0;
  mArg->getIntLast(scDynIdxToCaseLimit, &size_limit);
  
  DynIdxToCase analysis(mNetRefFactory, mMsgContext, mIODB, mArg, mStrCache, size_limit, mArg->getBoolValue(scVerboseDynIdxToCase));
  analysis.module(module);
}

bool LocalAnalysis::ifCaseEnabled(UInt32)
{
  return (not (mArg->getBoolValue (scNoIfCase) or
               mArg->getBoolValue("-g")));
}

void LocalAnalysis::runIfCase(NUModule* module, UInt32)
{
  if (not mArg->getBoolValue (scNoFold)) {
    Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
               mArg->getBoolValue(scVerboseFold),
               /* eFoldUDValid |*/ eFoldUDKiller | eFoldComputeUD);

    fold.module (module, false);
  }

  REIf reif (mNetRefFactory, mMsgContext, mStrCache, mIODB,
             mArg, mArg->getBoolValue (scVerboseFold));
  reif.reduceModule (module);
} // void LocalAnalysis::runIfCase

bool LocalAnalysis::localConstPropEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runLocalConstProp(NUModule* module, UInt32)
{
  buildFlow(module, false);

  mVerboseLocalPropagation = mArg->getBoolValue(scVerboseLocalPropagation);

  // Do another round of local constant propagation, which is helpful for
  // tristate simplification.  There was an initial round of local constant
  // propagation run as part of global constant propagation, but 
  // flattening may reveal more of these.
  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold),
             /* eFoldUDValid |*/ eFoldUDKiller | eFoldComputeUD);
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArg,
        mArg->getBoolValue(scVerboseUD));
  const char* scCProp = CRYPT("-verboseConstantPropagation");
  SInt32 verboseConstProp = 0;
  mArg->getIntLast(scCProp, &verboseConstProp);
  RELocalConstProp cprop(mIODB, &ud, &fold, 
                         mLocalConstantPropagationStatistics, 
                         verboseConstProp >= 2,
                         false);
                         //true); // use flow order.
  // JDM: I wanted to turn flow-order on, but this causes Sanity failures in 
  // test/schedule/schedule8: randbug2b.v, which is run in three ways.

  cprop.module(module);

  removeFlow();
} // void LocalAnalysis::runLocalConstProp

bool LocalAnalysis::localCopyPropEnabled(UInt32)
{
  return not mArg->getBoolValue (scNoCopyPropagation);
}

void LocalAnalysis::runLocalCopyProp(NUModule* module, UInt32)
{
  buildFlow(module, false);

  mVerboseLocalPropagation = mArg->getBoolValue(scVerboseLocalPropagation);

  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold),
             /* eFoldUDValid |*/ eFoldUDKiller | eFoldComputeUD | eFoldNoStmtDelete);
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArg,
        mArg->getBoolValue(scVerboseUD));
  bool verboseCopyPropagation = mArg->getBoolValue (scVerboseCopyPropagation);
  RELocalCopyProp cprop(mIODB, &ud, &fold, 
                        mLocalCopyPropagationStatistics,
                        verboseCopyPropagation);
  cprop.module(module);

  removeFlow();
} // void LocalAnalysis::runLocalCopyProp

bool LocalAnalysis::tristateAnalysisEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runTristateAnalysis(NUModule* module, UInt32)
{
  bool verboseTristate = mArg->getBoolValue(scVerboseTristate);

  // Before tristate analysis, do another round of Z splitting, in case
  // one of the earlier optimizations made some more Z constants or
  // concat assigns
  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             false, // used only to simplify synthesized expressions
             eFoldUDKiller | eFoldComputeUD);
  {
    BreakZConsts breakZConsts(mMsgContext, verboseTristate, &fold,
                              mIODB, mNetRefFactory);
    (void) breakZConsts.module(module);
  }

  LFTristate tristate(mStrCache, mNetRefFactory, mMsgContext,
                      verboseTristate, mIODB);
  if (tristate.createEnabledDrivers(module)) {
    if (not mArg->getBoolValue(scNoFold)) {
      fold.module(module, false); // remove excess NUBlock levels
    }
    clearUD(module);
    buildUD(module, true);
  }
  if (mPhaseStats)
    mStats->printIntervalStatistics("LFTristate");

  // Simplify multiple tristate assigns in the same module
  if (not shouldExit()) {
    removeFlow();
    buildFlow(module, false);
    RETriSimplify triSimplify(&fold, verboseTristate, mMsgContext,
                              mTristateMode, mIODB);
    if (triSimplify.module(module)) {
      clearUD(module);
      buildUD(module, false);
    }
    removeFlow();
  }
} // void LocalAnalysis::runTristateAnalysis

bool LocalAnalysis::buildFlowEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runBuildFlow(NUModule* module, UInt32 flags)
{
  bool recurse = (flags != 0);
  UnelabFlow unelab_flow(mFlowFactory, mNetRefFactory, mMsgContext,
                         mIODB, mArg->getBoolValue(scVerboseFlow));
  unelab_flow.module(module, recurse);
}

bool LocalAnalysis::markReadWriteNetsEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runMarkReadWriteNets(NUModule* module, UInt32)
{
  MarkReadWriteNets marker(mIODB, mArg, true);
  marker.module(module);
}

bool LocalAnalysis::clearDeadLogicEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runClearDeadLogic(NUModule* module, UInt32 flags)
{
  bool startAtAllNets = (flags != 0);
  RemoveDeadCode cleanup(mNetRefFactory, mFlowFactory, 
                           mIODB, mArg, mMsgContext, startAtAllNets,
                           true, // always delete dead code
                           true, // delete module instances
                           mArg->getBoolValue(scVerboseCleanup));
  cleanup.module(module);
}

bool LocalAnalysis::tristateWarningEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runTristateWarning(NUModule* module, UInt32)
{
  TristateWarning tristate_warning(mMsgContext);
  tristate_warning.module(module);
}

bool LocalAnalysis::removeFlowEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runRemoveFlow(NUModule* module, UInt32)
{
  removeFlow();

  // Purge the factory so that if the next module is small, we don't
  // have as much memory allocated. This makes iteration of all flow
  // nodes for the smaller module, faster.
  NU_ASSERT(mFlowFactory->numAllocated() == 0,module);
  mFlowFactory->purge();
}

void LocalAnalysis::setupAnalyzeModule(Phases* phases)
{
  // Setup lower ports
  Phase ports1("LowerPorts", false, true,
               &LocalAnalysis::runLowerPorts,
               &LocalAnalysis::lowerPortsEnabled);
  phases->push_back(ports1);

  // Setup flattening
  Phase flattening("Flattening", 0, true,
                   &LocalAnalysis::runFlattenModule,
                   &LocalAnalysis::flattenModuleEnabled);
  phases->push_back(flattening);

  // Remove the blocks from dead modules, but leave the nets
  Phase cleanDeadModules("CleanDeadModules", 0, 0,
                         &LocalAnalysis::runCleanDeadModules,
                         &LocalAnalysis::cleanDeadModulesEnabled);
  phases->push_back(cleanDeadModules);

  // Run congruence to eliminate duplicate tasks.
  //
  // There is not a call to task congruence after early flattening
  // because it attempts to construct UnelabFlow. This construction
  // fails because non-blocking assignments still exist (and are
  // unsupported by UnelabFlow).
  Phase congruence("TaskCongruence", 0, true,
                   &LocalAnalysis::runCongruence,
                   &LocalAnalysis::congruenceEnabled);
  phases->push_back(congruence);

  // Setup vector match
  Phase vectorMatch("VectorReconstruction", 0, true,
                    &LocalAnalysis::runVectorMatch,
                    &LocalAnalysis::vectorMatchEnabled);
  phases->push_back(vectorMatch);

  // Setup lower ports again
  Phase ports2("LowerPorts", true, true,
               &LocalAnalysis::runLowerPorts,
               &LocalAnalysis::lowerPortsEnabled);
  phases->push_back(ports2);

  // Setup Look for vector-inference possibilities
  Phase inference("Inference", Inference::eConcatAnalysis, true,
                  &LocalAnalysis::runInference,
                  &LocalAnalysis::inferenceEnabled);
  phases->push_back(inference);

  // Inference may construct somewhat suboptimal concats. ConcatRewrite must
  // always be run after inference.

  // Setup Pass 1 : Rewrite certain concat assigns into simpler assigns
  Phase concat1("ConcatRewrite", false, true,
                &LocalAnalysis::runConcatRewrite,
                &LocalAnalysis::concatRewriteEnabled);
  phases->push_back(concat1);

  // Setup separate vectors into distinct parts
  Phase separate("Separation", 0, true,
                 &LocalAnalysis::runSeparation,
                 &LocalAnalysis::separationEnabled);
  phases->push_back(separate);

  // Setup reduce muxes into simpler form
  Phase remux1("REMux", 0, true,
               &LocalAnalysis::runRemux,
               &LocalAnalysis::remuxEnabled);
  phases->push_back(remux1);

  // Get rid of continuous assigns of the form "a = a".
  // NOTE: This is a cheap pass for now, done quickly to solve bug 2024.
  // We need to do a much better job of redundancy elimination,
  // implement value numbering, cse, partial redundancy elimination, ...
  // Once that is implemented, get rid of this.
  Phase assigns("Redundant Assigns", 0, true,
                &LocalAnalysis::runRedundantAssigns,
                &LocalAnalysis::redundantAssignsEnabled);
  phases->push_back(assigns);

  // Traverse the design performing constant value propagation and
  // expression simplification.
  Phase fold1("Constant Folding", eFoldUDKiller | eFoldCaseEquivalent, true,
              &LocalAnalysis::runFold,
              &LocalAnalysis::foldEnabled);
  phases->push_back(fold1);

  // Setup Look for vector-inference possibilities
  phases->push_back(inference);         // concat rewrite happends in concat2 below

  // Always run concat rewrite after inference.
  phases->push_back(concat1);

  // Build UD
  Phase ud("UseDef", true, true,
           &LocalAnalysis::runBuildUD,
           &LocalAnalysis::buildUDEnabled);
  phases->push_back(ud);

  // Compute pessimistic set of possible allocation aliases for use by reordering
  // and atomize.
  Phase unelabAllocAlias("UnelabAllocAlias", AllocAlias::eMismatchedPortSizes,
                         false /* don't print stats */,
                         &LocalAnalysis::runUnelabAllocAlias,
                         &LocalAnalysis::unelabAllocAliasEnabled);
  phases->push_back(unelabAllocAlias);

  // Run atomize before concat-rewrite. This transforms
  // single-assignment always blocks into continuous assigns before
  // rewriting contained concats.
  Phase atomize("Atomize", true, true,
                &LocalAnalysis::runAtomize,
                &LocalAnalysis::atomizeEnabled);
  phases->push_back(atomize);

  // Remove the unelab alloc alias class
  Phase deleteUnelabAllocAlias("DelAllocAlias", 0, false /* no phase stats */,
                               &LocalAnalysis::runDeleteUnelabAllocAlias,
                               &LocalAnalysis::
                               deleteUnelabAllocAliasEnabled);
  phases->push_back(deleteUnelabAllocAlias);

  // Search for loops to unroll. do this before reordering, as we may
  // reduce the number of generated tmps
  Phase unroll("Unroll Loops", 0, true,
               &LocalAnalysis::runLoopUnroll,
               &LocalAnalysis::loopUnrollEnabled);
  phases->push_back(unroll);
  // Loop unrolling performs vector inference and there must be a subsequent
  // concat rewrite pass... concat2 below is enough...

  // Search for priority encoders and re-write them
  Phase encoder("PrioEnc", 0, true,
                &LocalAnalysis::runPriorityEncoderRewrite, 
                &LocalAnalysis::priorityEncoderRewriteEnabled);
  phases->push_back(encoder);

  // Try and convert certain types of if-then-else trees into ternary assignments.
  Phase ternary("TernaryCreation", 0, true,
                &LocalAnalysis::runTernary,
                &LocalAnalysis::ternaryEnabled);
  phases->push_back(ternary);

  // Pass 2 : Rewrite certain concat assigns into simpler assigns
  Phase concat2("ConcatRewrite", true, true,
                &LocalAnalysis::runConcatRewrite, 
                &LocalAnalysis::concatRewriteEnabled);
  phases->push_back(concat2);

  // Try and merge always blocks which define different parts of the same net.
  Phase control("MergeControl", 0, true,
                &LocalAnalysis::runControlMerge,
                &LocalAnalysis::controlMergeEnabled);
  phases->push_back(control);

  // Two passes of assignment aliasing.  After the first one do a fold,
  // which might reveal more optimizations for the second one.  We could
  // do this until it settles but for now we just do 2 passes.  If more
  // passes are added, the last one should have 'false' passed in for
  // doFold
  UInt32 aliasingFlags = eAliasingBuildFlow|eAliasingDoFold|eAliasingRemoveFlow|eAliasingClearDeadLogic;
  Phase aliasing1("Aliasing", aliasingFlags, true,
                  &LocalAnalysis::runAliasing,
                  &LocalAnalysis::aliasingEnabled);
  phases->push_back(aliasing1);
  aliasingFlags = eAliasingBuildFlow|eAliasingRemoveFlow;
  Phase aliasing2("Aliasing", aliasingFlags, true,
                  &LocalAnalysis::runAliasing,
                  &LocalAnalysis::aliasingEnabled);
  phases->push_back(aliasing2);

  // Look for self referential cycles through different bits of a
  // vector. By breaking these up we can avoid iterating until things
  // settle or worse, getting the wrong answer.
  Phase netCycles("BrkNetCyc", 0, true,
                  &LocalAnalysis::runBreakNetCycles,
                  &LocalAnalysis::breakNetCyclesEnabled);
  phases->push_back(netCycles);

  // Get rid of continuous assigns of the form "a = a".
  // NOTE: This is a cheap pass for now, done quickly to solve bug 2024.
  // We need to do a much better job of redundancy elimination,
  // implement value numbering, cse, partial redundancy elimination, ...
  // Once that is implemented, get rid of this.
  phases->push_back(assigns);

  // Look for self-referential cycles involving ternary assigns.
  Phase latchCycles("LatchCycles", 0, true,
                    &LocalAnalysis::runRemoveLatchCycles,
                    &LocalAnalysis::removeLatchCyclesEnabled);
  phases->push_back(latchCycles);

  // Remove any dead net bits
  Phase deadNetBits("DeadNetBits", 0, true,
                    &LocalAnalysis::runDeadNetBits,
                    &LocalAnalysis::deadNetBitsEnabled);
  phases->push_back(deadNetBits);

  // Traverse the design looking for latches. This should be done
  // after non-blocking re-ordering since we count on there only being
  // blocking assignments.
  Phase latches("Latches", 0, true, &LocalAnalysis::runLatchRecognition,
                &LocalAnalysis::latchRecognitionEnabled);
  phases->push_back(latches);

  // Reduce muxes into simpler form
  Phase remux2("REMux", 1, true,
               &LocalAnalysis::runRemux,
               &LocalAnalysis::remuxEnabled);
  phases->push_back(remux2);

  // Traverse the design performing local identification
  Phase rescope("Rescope", 0, true,
                &LocalAnalysis::runRescope,
                &LocalAnalysis::rescopeEnabled);
  phases->push_back(rescope);

  // Rewrite dynamic indexing to case statements.
  Phase dyn_idx_to_case("DynIdxToCase", 0, true,
                        &LocalAnalysis::runDynIdxToCase,
                        &LocalAnalysis::dynIdxToCaseEnabled);
  phases->push_back(dyn_idx_to_case);

  // Repeat extract/motion until fixed point.
  Phase motionExtract("MotionExtraction", 0x10000 /* first pass */ | 1 /* max iterations */, true,
                      &LocalAnalysis::applyMotionAndExtraction,
                      &LocalAnalysis::motionAndExtractionEnabled);
  phases->push_back(motionExtract);

  // Look for if-then-else that can be transformed into case statements
  Phase ifCase("IfCase", 0, true,
               &LocalAnalysis::runIfCase,
               &LocalAnalysis::ifCaseEnabled);
  phases->push_back(ifCase);

  // Propagate local constants.
  Phase localConstProp("LocalConst", 0, true,
                       &LocalAnalysis::runLocalConstProp,
                       &LocalAnalysis::localConstPropEnabled);
  phases->push_back(localConstProp);

  // Propagate local assignments.
  Phase localCopyProp("LocalCopy", 0, true,
                      &LocalAnalysis::runLocalCopyProp,
                      &LocalAnalysis::localCopyPropEnabled);
  phases->push_back(localCopyProp);

  // Copy propagation may have enabled more vector-inference possibilities.
  Phase inference_noconcat("Inference", 0, true,
                           &LocalAnalysis::runInference,
                           &LocalAnalysis::inferenceEnabled);
  phases->push_back(inference_noconcat);

  // Always run concat rewrite after inference.
  phases->push_back(concat2);

  // Traverse the design performing tristate analysis
  Phase tristate("TristateAnal", 0, true,
                 &LocalAnalysis::runTristateAnalysis,
                 &LocalAnalysis::tristateAnalysisEnabled);
  phases->push_back(tristate);

  // After tristate analysis we need to re-run the read/write analysis
  // because bus=e?a:1'bz should show us as TriWritten, not Written.
  // Failure to re-run this here causes spurious warnings:
  //    hier1.v:14 top.S2.out: Warning 4003: Non-tristate and tristate nets are being aliased top.bus
  //    hier1.v:14 top.S1.out: Warning 4003: Non-tristate and tristate nets are being aliased top.bus
  // in test/exprsynth/hier1.v
  Phase readWrite("MarkReadWriteNets", 0, true,
                  &LocalAnalysis::runMarkReadWriteNets,
                  &LocalAnalysis::markReadWriteNetsEnabled);
  phases->push_back(readWrite);

  // Create the unelaborated flow graph for this module, temporarily.
  // We will throw this one away when we are done analyzing this
  // module.  We will create a persistent one when we are done
  // analyzing the complete design.
  Phase flow("Flow", false, true,
             &LocalAnalysis::runBuildFlow,
             &LocalAnalysis::buildFlowEnabled);
  phases->push_back(flow);

  // One last pass of assignment aliasing -- not sure why this is needed
  aliasingFlags = 0;
  Phase aliasing3("Aliasing", aliasingFlags, true,
                  &LocalAnalysis::runAliasing,
                  &LocalAnalysis::aliasingEnabled);
  phases->push_back(aliasing3);

  // Remove dead code from the design, and dead flow from the graph.
  // Start only at output/observable nets on this second pass.
  //
  Phase deadLogic("RemoveDeadCode", false, true,
                  &LocalAnalysis::runClearDeadLogic, 
                  &LocalAnalysis::clearDeadLogicEnabled);
  phases->push_back(deadLogic);

  // Issue warnings against z/non-z driver conflicts
  Phase tristateWarning("TristateWarning", 0, true,
                        &LocalAnalysis::runTristateWarning,
                        &LocalAnalysis::tristateWarningEnabled);
  phases->push_back(tristateWarning);

  // Throw away the local flow graph.
  Phase removeFlow("UnelabFlowPurge", 0, true,
                   &LocalAnalysis::runRemoveFlow,
                   &LocalAnalysis::removeFlowEnabled);
  phases->push_back(removeFlow);
}

void LocalAnalysis::setupPostFlattenAnalyzeModule(Phases* phases)
{
  // Build UD
  Phase ud("UseDef", true, true,
           &LocalAnalysis::runBuildUD,
           &LocalAnalysis::buildUDEnabled);
  phases->push_back(ud);

  // Look for clock gating and remodel. This must run before
  // REConvertLatches and UnelabMerge for full effectiveness.
  Phase clockGate("ClockGate", 0, true,
                  &LocalAnalysis::runClockGate,
                  &LocalAnalysis::clockGateEnabled);
  phases->push_back(clockGate);

  // Look for latches that could be converted to flops. These are
  // latches that feed an AND gate where the one leg of the AND is the
  // oppositive of the latch enable.
  //
  // Note that for now the clock gate optimization and gate latch
  // optimization are exclusive. That is because there is not good way
  // to enable both with the current local analysis structure.  To
  // make them both work right they should occur after flattening has
  // settled and before UnelabMerge.
  Phase gateLatch("GateLatch", 0, true,
                  &LocalAnalysis::runConvertLatches,
                  &LocalAnalysis::convertLatchesEnabled);
  phases->push_back(gateLatch);

  // Old comment was:
  //   I think block resynthesis must precede REMux because it would
  //   otherwise undo the beneficial effects of REMux.  I'm hoping it
  //   doesn't destroy the ability of REMux to find opportunities...that
  //   should be tested before turning it on by default.  I think many of
  //   the REMux testcases are in test/simple.
  // But BlockResynthesis must run in PostFlatten mode so it can use the
  // more accurate DesignAllocAlias.  UnelabAllocAlias is way too pessimistic.
  // We will have to rely on costing to avoid having block resynthesis undo
  // the beneficial effects of REMux.  It is important to have it happen before
  // MergeUnelab & Rescope though.
  Phase blkResynth("BlkResynth", 0, true,
                   &LocalAnalysis::runBlockResynthesis,
                   &LocalAnalysis::blockResynthesisEnabled);
  phases->push_back(blkResynth);

  // Try and merge cont. assigns and always blocks. This needs to
  // happen after reorder and before rescope.
  Phase merge("MergeUnelab", 0, true,
              &LocalAnalysis::runMergeUnelab,
              &LocalAnalysis::mergeUnelabEnabled);
  phases->push_back(merge);

  // Traverse the design performing local identification
  Phase rescope("Rescope", 0, true,
                &LocalAnalysis::runRescope,
                &LocalAnalysis::rescopeEnabled);
  phases->push_back(rescope);

  // Repeat extract/motion until fixed point.
  Phase motionExtract("MotionExtraction", 0x20000 /* second pass */ | 20 /* max iterations */, true,
                      &LocalAnalysis::applyMotionAndExtraction,
                      &LocalAnalysis::motionAndExtractionEnabled);
  phases->push_back(motionExtract);

  // Copy propagation may have enabled more vector-inference possibilities.
  Phase inference_noconcat("Inference", 0, true,
                           &LocalAnalysis::runInference,
                           &LocalAnalysis::inferenceEnabled);
  phases->push_back(inference_noconcat);

  // Always run concat rewrite after inference.
  Phase concat("ConcatRewrite", false, true,
               &LocalAnalysis::runConcatRewrite,
               &LocalAnalysis::concatRewriteEnabled);
  phases->push_back(concat);

  // Run a copy prop pass to get rid of any temps introduced by inference.
  Phase localCopyProp("LocalCopy", 0, true,
                      &LocalAnalysis::runLocalCopyProp,
                      &LocalAnalysis::localCopyPropEnabled);
  phases->push_back(localCopyProp);
}

bool LocalAnalysis::ternaryGateEnabled(UInt32)
{
  return (not (mArg->getBoolValue(scNoTernaryGateOptimization)));
}

void LocalAnalysis::runTernaryGate(NUModule* module, UInt32)
{
  mControlExtractVerbosity = mArg->getBoolValue(scVerboseExtractControl);
  TernaryGateOptimization optimizer(mStrCache, mNetRefFactory, mMsgContext, 
                                    mIODB, mArg, true, &mControlExtractStats);
  optimizer.module(module);
}

bool LocalAnalysis::unelabAllocAliasEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runUnelabAllocAlias(NUModule* module, UInt32 flags)
{
  INFO_ASSERT(mAllocAlias == NULL,
              "Configuration problem: reconstructing unelab alloc alias data");
  mAllocAlias = new UnelabAllocAlias(module, (AllocAlias::PortStatus)flags);
}

bool LocalAnalysis::deleteUnelabAllocAliasEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runDeleteUnelabAllocAlias(NUModule*, UInt32)
{
  INFO_ASSERT(mAllocAlias != NULL,
              "Configuration problem: deleting an invalid unelab alloc alias");
  delete mAllocAlias;
  mAllocAlias = NULL;
}

bool LocalAnalysis::breakZConstsEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runBreakZConsts(NUModule* module, UInt32)
{
  Fold fold(mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB, false,
            /* eFoldUDValid | */ eFoldComputeUD | eFoldUDKiller);

  bool verboseTristate = mArg->getBoolValue(scVerboseTristate);
  BreakZConsts breakZConsts(mMsgContext, verboseTristate, &fold,
                            mIODB, mNetRefFactory);
  if (breakZConsts.module(module)) {
    buildUD(module, true);
  }

  // Resynthesize nets declared as wired (wor/wand/trior/triand).
  /*
    For example
    wor a;
    assign a = b;
      
    becomes
    assign a = a | b;
      
    and 
    wand c;
    assign c = d;
      
    becomes
    assign c = c & d;
      
    RETriInit has to create initial drivers for wired nets.

    This assumes that Post-Population Resynth has run flag
    propagation and consistency checks (and uniquification).

    This will fail on complex expressions - e.g., an unbroken concat
    on either the lhs which has a wired net or the rhs.
      
    Currently, a lhs must be an identlvalue or a part-select.
    The rhs must be not have concats or contain partial driving
    constants.
      
    BreakZConstants and ConcatRewrite have run at this point, so
    that should minimize most of what we can't handle.

    Note that this does NOT fix assignments to wired nets through
    ports. Currently, wired net connections through ports are not
    supported.
  */
  if (mWiredNetResynth && mWiredNetResynth->module(module)) {
    buildUD(module, true);
  }
} // void LocalAnalysis::runBreakZConsts

bool LocalAnalysis::synthEnabled(UInt32)
{
  return mArg->getBoolValue(CRYPT("-synth"));
}


void LocalAnalysis::runCheckSensitivity(NUModule* module, UInt32 pass)
{
  if(pass == 0)
  {
    // In the first pass we gather the nets, which are defs in the
    // combinational always blocks
    mSensList = new SensitivityList(mIODB, mMsgContext, mNetRefFactory);
    mSensList->gatherInfo(module);
  }
  else
  {
    // In the second path we check it.
    mSensList->check(module);
    delete mSensList;
    mSensList = NULL;
 }
}


bool LocalAnalysis::asyncResetEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runAsyncReset(NUModule* module, UInt32)
{
  // this causes compilation failures in some testcases due to mismatched 
  // resets.  This can be fixed by converting that error to a an alert or
  // warning -- I'm thinking warning.  But this is deferred for now.
  //
  // Also numerous regolds are needed if we turn this on because upstream
  // folding changes the cbuild messages in many cases.
  //
  // Do a simple Fold pass to eliminate spurious X detection in Matrox
  Fold fold (mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
             mArg->getBoolValue(scVerboseFold),
             eFoldUDKiller | eFoldPreserveReset | eFoldUDValid |
             eFoldComputeUD);
#if 0
  do {
    ++numLoops;
    fold.clearChanged ();
    fold.module (module, false);
  } while (fold.isChanged ());
#endif

  Reset reset( mStrCache, mNetRefFactory, mMsgContext, mIODB, mArg,
               mArg->getBoolValue( scVerboseReset ),
               false /* Sync reset disabled for now */,
               ! mArg->getBoolValue (scNoAsyncResets ),
               &fold);

  reset.module(module);
} // void LocalAnalysis::runAsyncReset

bool LocalAnalysis::tristateResynthEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runTristateResynth(NUModule* module, UInt32)
{
  bool verboseTristate = mArg->getBoolValue(scVerboseTristate);
  LFTristate tristate(mStrCache, mNetRefFactory, mMsgContext,
                      verboseTristate, mIODB);

  if (tristate.resynthesizeZBlocks(module)) {
    clearUD(module);
    buildUD(module, true);
  }
}

bool LocalAnalysis::reorderEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runReorder(NUModule* module, UInt32)
{
  Reorder reorder(mStrCache, mNetRefFactory, mIODB, mArg,
                  mMsgContext, mAllocAlias, false, false, false, false);
  reorder.module(module);
}

bool LocalAnalysis::selfCycleReorderEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runSelfCycleReorder(NUModule* module, UInt32)
{
  SelfCycleReorder reorder(module, mStrCache, mNetRefFactory, mIODB, mArg,
                           mMsgContext, mAllocAlias);
  reorder.analyze();
}

bool LocalAnalysis::initialBlockCleanupEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runInitialBlockCleanup(NUModule* module, UInt32)
{
  InitialBlockCleanup cleanup(mNetRefFactory, mIODB, mArg, mMsgContext);
  cleanup.module(module);
}

void LocalAnalysis::setupMinimumAnalyzeModule(Phases* phases)
{
  // Run ternary gate optimization
  Phase ternary("TernaryGate", 0, true,
                &LocalAnalysis::runTernaryGate,
                &LocalAnalysis::ternaryGateEnabled);
  phases->push_back(ternary);

  // Build UD
  Phase ud("UseDef", true, true,
           &LocalAnalysis::runBuildUD,
           &LocalAnalysis::buildUDEnabled);
  phases->push_back(ud);


  // First, we gather the sensitivity list info before loop unrolling.
  // This is needed because in some cases (bug11794) the loop unrolling 
  // modifies expression such way, that new nets appear in the used nets.
  // This info is used in the second pass of sensitivity list
  // checking after loop unrolling.
  Phase sensList0("SensitivityList", 0, true,
                  &LocalAnalysis::runCheckSensitivity,
                  &LocalAnalysis::synthEnabled);
  phases->push_back(sensList0);


  Phase unroll("Unroll Loops", 0, true,
               &LocalAnalysis::runLoopUnroll,
               &LocalAnalysis::loopUnrollEnabled);
  phases->push_back(unroll);
  // Loop unrolling performs vector inference so run concat rewrite afterwards.
  Phase concat("ConcatRewrite", true, true,
               &LocalAnalysis::runConcatRewrite,
               &LocalAnalysis::concatRewriteEnabled);
  phases->push_back(concat);


  // Sensitivity list analysis should be done after UseDef analysis,
  // but before atomize, because atomize migth remove and rearange 
  // some always blocks.
  // This is the second pass of sensitivity check.
  Phase sensList1("SensitivityList", 1, true,
                  &LocalAnalysis::runCheckSensitivity,
                  &LocalAnalysis::synthEnabled);
  phases->push_back(sensList1);


  // Run atomize before concat-rewrite. This transforms
  // single-assignment always blocks into continuous assigns before
  // rewriting contained concats.
  Phase atomize("Atomize", true, true,
                &LocalAnalysis::runAtomize,
                &LocalAnalysis::atomizeEnabled);
  phases->push_back(atomize);

  // Rewrite certain concat assigns into simpler assigns. This is to
  // improve constant propagation. It cannot handle left hand side
  // concats. See bug 3877. test case added to
  // test/constprop/lhsconcat2.v
  phases->push_back(concat);

  // Break up assigns with partial Z constants on the RHS
  Phase breakZConsts("BreakZConsts", 0, true,
                     &LocalAnalysis::runBreakZConsts,
                     &LocalAnalysis::breakZConstsEnabled);
  phases->push_back(breakZConsts);

  // Traverse the design performing asynchronous reset handling.
  Phase asyncReset("AsyncReset", 0, true,
                   &LocalAnalysis::runAsyncReset,
                   &LocalAnalysis::asyncResetEnabled);
  phases->push_back(asyncReset);


  // Resynthesize Z-assigns in always blocks into continuous assigns
  // with z expressions.  This is done prior to reordering because
  // that solves testcase nonblock1.v, where the temp assign introduced by
  // the non-blocking assign does not propagate the Z. 
  Phase tristateResynth("TristateResynth", 0, true,
                        &LocalAnalysis::runTristateResynth,
                        &LocalAnalysis::tristateResynthEnabled);
  phases->push_back(tristateResynth);

  // Traverse the design performing non-blocking assign
  // reordering. This will not be recomputed in the full pass.
  Phase reorder("Reorder", 0, true,
                &LocalAnalysis::runReorder,
                &LocalAnalysis::reorderEnabled);
  phases->push_back(reorder);

  // Do a reordering pass to reduce the number of self-cycles with
  // combinational always blocks.
  Phase cycleReorder("SelfReorder", 0, true,
                     &LocalAnalysis::runSelfCycleReorder,
                     &LocalAnalysis::selfCycleReorderEnabled);
  phases->push_back(cycleReorder);

  // Remove non-constant assignments in initial blocks.
  Phase initialBlockCleanup("Initial", 0, true,
                            &LocalAnalysis::runInitialBlockCleanup,
                            &LocalAnalysis::initialBlockCleanupEnabled);
  phases->push_back(initialBlockCleanup);


  // Run an aliasing pass here, as it is before port splitting, and this can allow
  // us to support more tristate testcases.  This was done to solve situations such
  // as in bug 12099, where we rely on aliasing in order to get "z propagation" but
  // if port splitting gets involved, the aliasing opportunity goes away.
  UInt32 aliasingFlags = eAliasingBuildFlow|eAliasingRemoveFlow;
  Phase aliasing("Aliasing", aliasingFlags, true,
                 &LocalAnalysis::runAliasing,
                 &LocalAnalysis::aliasingEnabled);
  phases->push_back(aliasing);

} // void LocalAnalysis::setupMinimumAnalyzeModule

bool LocalAnalysis::atomizeEnabled(UInt32)
{
  return not mArg->getBoolValue(scNoAtomize);
}

void LocalAnalysis::runAtomize(NUModule * module, UInt32 flags)
{
  bool recalculate_ud = (flags != 0);
  mAtomizeVerbosity  = mArg->getBoolValue(scVerboseAtomize);
  Atomize atomize(mStrCache, mNetRefFactory, mIODB, mArg, mMsgContext, 
                  mAllocAlias, mAtomizeVerbosity, &mAtomizeStats,
                  recalculate_ud);
  atomize.module(module);
}


bool LocalAnalysis::concatRewriteEnabled(UInt32)
{
  return not mArg->getBoolValue(scNoConcatRewrite);
}

void LocalAnalysis::runConcatRewrite(NUModule * module,
                                     UInt32 flags)
  
{
  // Create an alloc alias class if we don't have one. Some local
  // analysis passes create them for the whole pass, others have to
  // recreate as needed.
  AllocAlias* alias = mAllocAlias;
  bool allocated = false;
  if (alias == NULL) {
    alias = new UnelabAllocAlias(module, AllocAlias::eMismatchedPortSizes);
    allocated = true;
  }

  bool recalculate_ud = (flags != 0);
  mConcatRewriteVerbosity = mArg->getBoolValue(scVerboseConcatRewrite);
  ConcatRewrite concat(mNetRefFactory,
                       mMsgContext,
                       mStrCache,
                       mIODB,
                       mArg,
                       &mConcatRewriteStats,
                       alias,
                       recalculate_ud);
  concat.module(module);
  if (allocated) {
    delete alias;
  }
}

bool LocalAnalysis::buildUDEnabled(UInt32)
{
  return true;
}

void LocalAnalysis::runBuildUD(NUModule* module, UInt32 flags)
{
  bool computeModuleUD = (flags != 0);
  buildUD(module, computeModuleUD);
}

bool LocalAnalysis::loopUnrollEnabled(UInt32)
{
  return not mArg->getBoolValue(scNoUnroll);
}

void LocalAnalysis::runLoopUnroll(NUModule * module, UInt32)
{
  mInferenceVerbosity  = mArg->getBoolValue(scVerboseInference);
  mTernaryVerbosity    = mArg->getBoolValue(scVerboseTernaryCreation);
  mLoopUnrollVerbosity = mArg->getBoolValue(scVerboseUnroll);
  LoopUnroll unroll(mStrCache, mNetRefFactory, mMsgContext, mIODB, mArg,
                    mArg->getBoolValue(scForceUnroll),
                    mLoopUnrollVerbosity,
                    &mLoopUnrollStats,
                    &mInferenceStats,
                    &mTernaryStats);
  unroll.module(module);
}


void LocalAnalysis::rememberVisited(NUModule *module)
{
  mVisitedModules.insert(module);
}


bool LocalAnalysis::haveVisited(NUModule *module)
{
  return (mVisitedModules.find(module) != mVisitedModules.end());
}

void LocalAnalysis::clearUD(NUModule* module) {
  if (not shouldExit()) {
    ClearUDCallback clearUseDef(false); // do not recurse
    NUDesignWalker clearUDWalker(clearUseDef, false);
    clearUDWalker.module(module);
  }
  // module->clearUseDef();
}

// Traverse the design performing u/d computation.
void LocalAnalysis::buildUD(NUModule* module, bool computeModuleUD) {
  if (not shouldExit()) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArg,
          mArg->getBoolValue(scVerboseUD));

    ud.module(module, computeModuleUD);
    if (mPhaseStats)
      mStats->printIntervalStatistics("UseDef");
  }
}

void LocalAnalysis::removeFlow() {
  RemoveUnelabFlow remove(mFlowFactory);
  remove.remove();
  if (mPhaseStats)
    mStats->printIntervalStatistics("RemoveUnelabFlow");
}

void LocalAnalysis::buildFlow(NUModule* module, bool recurse) {
  if (not shouldExit()) {
    runBuildFlow(module, (UInt32)recurse);
    if (mPhaseStats)
      mStats->printIntervalStatistics("Flow");
  }
}

void LocalAnalysis::clearDeadLogic(NUModule* module, bool startAtAllNets) {
  if (not shouldExit()) {
    runClearDeadLogic(module, (UInt32)startAtAllNets);
    if (mPhaseStats)
      mStats->printIntervalStatistics("RemoveDeadCode");
  }
}
