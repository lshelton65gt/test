// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include <ctype.h> // for isalnum()
#include "localflow/Populate.h"
#include "localflow/TicProtectedNameManager.h"
#include "compiler_driver/Interra.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUModuleInstance.h"
#include "reduce/Fold.h"
#include "util/SourceLocator.h"
#include "util/UtString.h"
#include "util/CbuildMsgContext.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUDesign.h"
#include "reduce/Fold.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "LFContext.h"
#include "localflow/DesignPopulate.h"
#include "bdd/BDD.h"

Populate::Populate( MsgContext *msg_context, IODBNucleus* iodb, 
                    DesignPopulate *design_populate,
                    STSymbolTable *symtab,
                    AtomicCache *str_cache,
                    SourceLocatorFactory *loc_factory,
                    FLNodeFactory *flow_factory,
                    NUNetRefFactory *netref_factory,
                    ArgProc *arg, 
                    CarbonContext* cc ) :
  mCarbonContext(cc),
  mMsgContext( msg_context ), mIODB( iodb ),
  mPopulate(design_populate),
  mSymtab(symtab),
  mStrCache(str_cache),
  mLocFactory(loc_factory),
  mFlowFactory(flow_factory),
  mNetRefFactory(netref_factory),
  mArg(arg)
{}

bool
Populate::errorCodeSaysReturnNowWhenFailPopulate( ErrorCode tempCode,
                                                  ErrorCode *resultErrorCode )
{
  switch (tempCode) {
  case eSuccess: {
    return false;
  }
  case eNotApplicable:
  case eFailPopulate:
  case eFatal: {
    if ( resultErrorCode != 0 && (tempCode > *resultErrorCode ) ) {
      // if returning true then only allow resultErrorCode to become more severe
      *resultErrorCode = tempCode; 
    }

    return true;
  }
  }
  INFO_ASSERT(0, "errorCodeSaysReturnNowWhenFailPopulate tempCode corrupted, has value outside of enum.");
  return true;                  // will never be executed
}


bool
Populate::errorCodeSaysReturnNowOnlyWithFatal( ErrorCode tempCode,
                                               ErrorCode *resultErrorCode )
{
  if ( resultErrorCode != 0 && (tempCode > *resultErrorCode ) ) {
    // only allow resultErrorCode to become more severe
    *resultErrorCode = tempCode; 
  }

  switch (tempCode) {
  case eSuccess:
  case eNotApplicable:
  case eFailPopulate: {
    return false;
  }
  case eFatal: {
    return true;
  }
  }
  INFO_ASSERT(0, "errorCodeSaysReturnNowOnlyWithFatal tempCode corrupted, has value outside of enum.");
  return true;                  // will never be executed
}

bool
Populate::errorCodeSaysReturnNowIfIncomplete( ErrorCode tempCode,
                                              ErrorCode *resultErrorCode )
{
  if ( resultErrorCode != 0 && (tempCode > *resultErrorCode ) ) {
    // only allow resultErrorCode to become more severe
    *resultErrorCode = tempCode; 
  }

  switch (tempCode) {
  case eSuccess:
  case eNotApplicable:
  {
    return false;
  }
  case eFailPopulate:
  case eFatal:
  {
    return true;
  }
  }
  INFO_ASSERT(0, "errorCodeSaysReturnNowIfIncomplete tempCode corrupted, has value outside of enum.");
  return true;                  // will never be executed
}

void
Populate::VectOrMemExprRangeError(NUNet* net, NUExpr* bitsel_expr,
                                  const SourceLocator& loc)
{
  if (net->isVectorNet())
    ExprRangeError(net->castVectorNet(), bitsel_expr, loc);
  else if (net->isMemoryNet())
    ExprRangeError(net->getMemoryNet(), bitsel_expr, loc);
  else
    NU_ASSERT(0, net);
}

void
Populate::ExprRangeError( NUVectorNet *vn, NUExpr *bitsel_expr,
                          const SourceLocator &loc )
{
  UtString buf, exprBuf;
  vn->compose( &buf, NULL );
  bitsel_expr->compose( &exprBuf, NULL );
  const ConstantRange* vect_range = vn->getDeclaredRange();
  mMsgContext->LFExprRangeError( &loc, exprBuf.c_str(), buf.c_str(),
                                 vect_range->getMsb(), vect_range->getLsb() );
}

void
Populate::ExprRangeError( NUMemoryNet *mem, NUExpr *bitsel_expr, const SourceLocator &loc )
{
  UtString buf, exprBuf;
  mem->compose( &buf, NULL );
  bitsel_expr->compose( &exprBuf, NULL );
  const ConstantRange* width_range = mem->getDeclaredWidthRange();
  mMsgContext->LFExprRangeError( &loc, exprBuf.c_str(), buf.c_str(),
                                 width_range->getMsb(), width_range->getLsb() );
}

// create always blocks for any task enables that have not yet been
// created (needed for function to task conversion).
// This must come after all other components since anything might
// have added an entry to the extra TaskEnable list
Populate::ErrorCode Populate::extraTaskEnables(LFContext *context, IODBNucleus * iodb)
{
  NUModule* module = context->getModule();
  AtomicCache* string_cache = context->getStringCache();
  NUNetRefFactory* factory = context->getNetRefFactory();
  ErrorCode err_code = eSuccess;

  while ( NUStmt* task_enable = context->extractAnExtraTaskEnable() ) 
  {
    // now create an always block to hold this single stmt

    // build a scope (named block) for the always block
    const SourceLocator &loc = task_enable->getLoc();

    // setup context
    NUBlock * top_block = new NUBlock(NULL,
                                      module,
                                      iodb,
                                      factory,
                                      false,
                                      loc);

    top_block->addStmt(task_enable);

    StringAtom * name = module->newBlockName(string_cache, loc);
    NUAlwaysBlock *the_always = new NUAlwaysBlock(name,
                                                  top_block, 
                                                  context->getNetRefFactory(), 
                                                  loc, false);

    module->addAlwaysBlock(the_always);
  }

  return err_code;
}

Populate::ErrorCode Populate::singleBitEdgeExpr( ClockEdge edge,
                                                 NUExpr* clkExpr,
                                                 LFContext* context,
                                                 NUEdgeExpr** the_expr)
{
  ErrorCode  err_code = eSuccess;
  NUIdentRvalue* rval = dynamic_cast<NUIdentRvalue*>(clkExpr);

  // clock expressions of size >1 get truncated with a warning
  SourceLocator loc = clkExpr->getLoc(); // loc can be used after clkExpr is deleted
  if (clkExpr->determineBitSize() != 1)
  {
    NUVectorNet* vn = NULL;
    const ConstantRange* range = NULL;

    // We will be putting the expression text into a message one way or another
    UtString buf;
    clkExpr->compose(&buf, NULL);

    // Right now all I know how to prune is part-selects and raw vector-nets.
    if (rval == NULL)
    {
      NUVarselRvalue* ps = dynamic_cast<NUVarselRvalue*>(clkExpr);
      if (ps != NULL)
      {
        NU_ASSERT (ps->isConstIndex (), ps);
        range = ps->getRange();
        vn = dynamic_cast<NUVectorNet*>(ps->getIdent());
      }
    }
    else
    {
      vn = dynamic_cast<NUVectorNet*>(rval->getIdent());
      if (vn != NULL) 
        range = vn->getRange();
    }

    if (vn == NULL)
    {
      POPULATE_FAILURE(context->getMsgContext()->LFUnsupEdge(&loc, buf.c_str()), &err_code);
      return err_code;
    }
    
    SInt32 idx = range->getLsb ();
    delete clkExpr;
    clkExpr = new NUVarselRvalue(vn, ConstantRange (idx, idx), loc);
    context->getMsgContext()->LFWideEdge(&loc, buf.c_str());
    rval = NULL;
  } // if

  NUNet* clockNet = NULL;

  if (rval == NULL)
  {
    // I hereby declare that edge expressions have a useful size of 1.
    // Resizing these up front makes the hashing of clkExprs much cleaner.
    clkExpr->resize(1);

    // See if we have seen this expression before in an edge condition
    clockNet = context->getEdgeNet(clkExpr);
    if (clockNet == NULL)
    {
      // must create temp.  Name it for the vector bit
      NUModule* mod = context->getModule();
      UtString buf(NetClkTempPrefix);
      clkExpr->compose(&buf, NULL, true);   // include root in name, we need an unescaped name here

#define BUG993
#ifdef BUG993
      StringUtil::makeIdent(&buf);
#endif
  
      // Ensure a unique name relative to the other synthesized clock scalars
      buf << "_" << (1 + context->numEdgeNets());
      StringAtom* name = context->getStringCache()->intern(buf.c_str(),
                                                           buf.size());
      clockNet = new NUBitNet(name, NetFlags(eDMWireNet | eAllocatedNet | eTempNet),
                              mod, loc);
      mod->addLocal(clockNet);
      NUIdentLvalue *temp_lvalue = new NUIdentLvalue(clockNet, loc);
      NUContAssign* assign = new NUContAssign(clockNet->getName(),
                                              temp_lvalue, clkExpr, loc);
      mod->addContAssign(assign);
      context->putEdgeNet(clkExpr, clockNet);
    }
    else
      delete clkExpr;
    rval = new NUIdentRvalue(clockNet, loc);
  } // if 

  *the_expr = new NUEdgeExpr(edge, rval, loc);
  return err_code;
} // Populate::ErrorCode Populate::singleBitEdgeExpr


//! \param excludeThisNet if not null and this net is found in the port map, it is not connected to \a outPort
Populate::ErrorCode
Populate::cModelConnectAllPorts( NUCModelInterface * cmodelInterface, 
                                 NUCModelFn * cmodelFn, 
                                 NUCModelPort * outPort,
                                 NUNet* excludeThisNet,
                                 NetLookup & portNetMap)
{
  ErrorCode err_code = eSuccess;

  cmodelFn->addDef(outPort);
  for (NetLookup::iterator i = portNetMap.begin(); 
       i != portNetMap.end(); 
       ++i) {
    const NUNet* inPortNet = i->second;

    if ( ( excludeThisNet != NULL ) and ( inPortNet == excludeThisNet )) {
      continue;
    }

    const UtString& inPortName = i->first;
    NUCModelPort* inPort = cmodelInterface->findPort(&inPortName);
    if (not inPort) {
      const SourceLocator &loc = cmodelInterface->getLoc();
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Cannot find input port for output port"), &err_code);
      return err_code;
    }
    cmodelFn->addUse(outPort, inPort);
  }

  return err_code;
} // Populate::cModelConnectAllPorts


Populate::ErrorCode
Populate::findCModelNet( NUCModelPort * port,
                         NetLookup & primaryMap,
                         const SourceLocator &loc,
                         NUNet **the_net)
{
  ErrorCode err_code = eSuccess;

  *the_net = NULL;
  const UtString * name = port->getName();

  {
    NetLookup::iterator location = primaryMap.find(*name);
    if (location != primaryMap.end()) {
      *the_net = (*location).second;
    }
  }

  if (not *the_net) {
     POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Could not find c-model port"), &err_code);
    return err_code;
  }

  return err_code;
} // Populate::findCModelNet

Populate::ErrorCode
Populate::taskCModelFn( NUCModel* cmodel,
                        const SourceLocator& loc, NUCModelFn** cmodelFn)
{
  ErrorCode err_code = eSuccess;

  // Check if the cmodel already exists. If so we don't need another
  // c-model fucntion.  We only need one since the signatures are all
  // the same, we always pass all the nets to them.
  NUCModelInterface* cmodelInterface = cmodel->getCModelInterface();
  *cmodelFn = NULL;
  ErrorCode temp_err_code = err_code;
  for (NUCModelFnLoop l = cmodelInterface->loopFunctions(); !l.atEnd(); ++l)
  {
    if ((*cmodelFn) != NULL)
    {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Found duplicate task"), &temp_err_code);
    }
    *cmodelFn = *l;
  }
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Create the NUCModelFn.
  if ((*cmodelFn) == NULL)
  {
    // Need to create it
    *cmodelFn = new NUCModelFn(cmodelInterface, NULL);
    cmodelInterface->addFunction(*cmodelFn);

    // Need to add the UD information. We have all output ports fanin
    // from all input ports. First gather all the ports
    NUCModelPortVector outputPorts;
    NUCModelPortVector inputPorts;
    NUCModelInterface::PortsLoop l;
    for (l = cmodelInterface->loopPorts(); !l.atEnd(); ++l)
    {
      NUCModelPort* port = *l;
      PortDirectionT dir = port->getDirection();
      switch(dir)
      {
        case eInput:
          inputPorts.push_back(port);
          break;

        case eOutput:
          outputPorts.push_back(port);
          break;

        case eBid:
          outputPorts.push_back(port);
          inputPorts.push_back(port);
          break;
      }
    }

    // Visit all the output ports and create UD from the input ports
    for (NUCModelInterface::PortsLoop o(outputPorts); !o.atEnd(); ++o)
    {
      NUCModelPort* outPort = *o;
      (*cmodelFn)->addDef(outPort);
      for (NUCModelInterface::PortsLoop i(inputPorts); !i.atEnd(); ++i)
      {
        NUCModelPort* inPort = *i;
        (*cmodelFn)->addUse(outPort, inPort);
      }
    }
  } // if
  return err_code;
} // Populate::taskCModelFn

Populate::ErrorCode
Populate::taskPorts( NUCModel* cmodel,
                     const char* name,
                     LFContext* context,
                     const SourceLocator& loc)
{
  // Check if it was already created
  NUCModelInterface* cmodelInterface = cmodel->getCModelInterface();
  UInt32 variant = cmodel->getVariant();
  if (variant != 0) {
    return eSuccess;
  }

  // check if it was already created by looking for port existence
  // (checking the variant is not foolproof for tasks)
  NUCModelInterface::PortsLoop loop = cmodelInterface->loopPorts();
  if (not loop.atEnd()) {
    return eSuccess;
  }

  // Loop over the UDTF arguments creating the appropriate ports and
  // inserting them into the cmodel
  IODBNucleus* iodb = context->getIODB();
  int index = 0;
  ErrorCode err_code = eSuccess;
  IODBNucleus::CTFArgsLoop l;
  for (l = iodb->loopTFArgs(name); !l.atEnd() && (err_code == eSuccess); ++l) {
    // Create the port
    IODBNucleus::CTFArg* arg = *l;
    const char* portName = arg->getName()->c_str();
    PortDirectionT dir = arg->getDirection();
    UInt32 bitSize = arg->getBitSize();
    bool isNullPort = arg->isNullPort();
    ErrorCode temp_err_code = cModelPort(cmodelInterface, variant, portName,
                                         dir, bitSize, index++, loc,
                                         isNullPort);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }
  } // for
  return err_code;
} // Populate::taskPorts

Populate::ErrorCode Populate::cModel( const char* name,
                                      NUModule* module,
                                      LFContext* context,
                                      NUCModel **the_cmodel,
                                      bool isModule)
{
  ErrorCode err_code = eSuccess;
  *the_cmodel = 0;

  // Check if it already exists.
  NUDesign* design = context->getDesign();
  UtString cName(name);
  NUCModelInterface* cmodelInterface = design->findCModelInterface(cName);

  // Create the c-model if it doesn't exist
  UInt32 variant;
  if (cmodelInterface == NULL) {
    StringAtom* cmodelNameAtom = context->getStringCache()->intern(name);
    const SourceLocator& loc = module->getLoc();
    bool isCallOnPlayback = mIODB->isCallOnPlayback(name, isModule);
    // OnDemand requires that all cmodels are run while idle.  This
    // uses the same mechanism as replay uses for running cmodels in
    // playback mode, so we need to force the flag.
    bool ondemand = mArg->getBoolValue("-onDemand");
    if (ondemand && !isCallOnPlayback) {
      bool replay = mArg->getBoolValue("-enableReplay");
      if (replay) {
        // If replay is enabled, too, and the directive wasn't
        // specified for this cmodel, issue a message that it's being
        // forced.
        mMsgContext->LFCModelForceCallOnPlayback(module, name);
      }
      isCallOnPlayback = true;
    }

    cmodelInterface = new NUCModelInterface(loc, cmodelNameAtom, isCallOnPlayback);
    design->addCModelInterface(cName, cmodelInterface);
    variant = 0;
  } else {
    if (isModule) {
      // All PLI task calls get their own variant because the args can be
      // different.  Eventually try to share matching variants.
      variant = cmodelInterface->nextVariant();
    } else {
      variant = 0; // non-PLI tasks only have variant of 0
    }
  }

  // Create the instances
  *the_cmodel = new NUCModel(cmodelInterface, module, variant);

  module->addCModel(*the_cmodel);

  return err_code;
}


Populate::ErrorCode
Populate::cModulePorts( NUModule * module, 
                        NUCModel * cmodel,
                        LFContext *,
                        NetLookup & inputNetMap,
                        NetLookup & outputNetMap )
{
  ErrorCode err_code = eSuccess;

  NUCModelInterface* cmodelInterface = cmodel->getCModelInterface();
  UInt32 variant = cmodel->getVariant();

  // Create a reverse map of names to nets (for inputs and outputs).
  // If we encounter any bi-directionals then we have a problem. Let
  // the user know.
  //
  // Also create the set of c-model ports
  NUNetList netList;
  module->getPorts(&netList);
  UInt32 index = 0;
  for (NUNetList::iterator p = netList.begin(); p != netList.end(); ++p)
  {
    NUNet* net = *p;
    const char* name = net->getName()->str();
    UtString netName(name);

    UInt32 size = net->getBitSize();
    ErrorCode temp_err_code = eSuccess;
    if (net->isInput())
    {
      // Create the net lookup
      inputNetMap.insert(NetLookup::value_type(netName, net));

      // Create and attach the c-model port
      temp_err_code = cModelPort(cmodelInterface, variant, name, eInput, size,
				 index++, net->getLoc(), false);
    }
    else if (net->isOutput())
    {
      // Create the net lookup
      outputNetMap.insert(NetLookup::value_type(netName, net));

      // Create and attach the c-model port
      temp_err_code = cModelPort(cmodelInterface, variant,name, eOutput, size,
				 index++, net->getLoc(), false);
    }
    else if (net->isBid())
    {
      POPULATE_FAILURE(mMsgContext->LFCModelInout(net), &temp_err_code);
    }
    else {
      const SourceLocator &loc = net->getLoc();
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown port direction"), &temp_err_code);
    }

    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  } // for

  // If the c-model has a null port, add it as an output here. This
  // makes the directives processing much simpler.
  if (mIODB->hasNullPort(cmodelInterface->getName()->str()))
  {
    NUNet* net = module->getOutputSysTaskNet();
    outputNetMap.insert(NetLookup::value_type(UtString(), net));
    ErrorCode temp_err_code = cModelPort(cmodelInterface, variant, "", eOutput,
                                         1, index++, net->getLoc(), true);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  return err_code;
}


UInt32 Populate::getSize(NUNet* net)
{
  UInt32 size;
  NUVectorNet* vecNet = dynamic_cast<NUVectorNet*>(net);
  if (vecNet != NULL)
  {
    UInt32 bitSize = vecNet->getBitSize();
    size = (bitSize + 31) / 32;
  }
  else
    size = 1;
  return size;
}

Populate::ErrorCode
Populate::cModelPort( NUCModelInterface* cmodelInterface,
                      UInt32 variant, const char* name,
                      PortDirectionT dir, UInt32 size, UInt32 index,
                      const SourceLocator& loc,
                      bool isNullPort)
{
  ErrorCode err_code = eSuccess;

  UtString search(name);
  if (variant == 0)
  {
    // This is the first variant of this port; there can be multiple
    // variants if the c-model(module) is parameterized
    if (not (cmodelInterface->findPort(&search) == NULL)) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Port encountered more than once"), &err_code);
    } else {
      NUCModelPort* port;
      port = new NUCModelPort(name, isNullPort, dir, size, index);
      cmodelInterface->addPort(port);
    }
  }
  else
  {
    // This is a variant with a potentially different size port. But
    // make sure all the other data is the same!
    NUCModelPort* port = cmodelInterface->findPort(&search);
    if (not ((port != NULL) && (port->getDirection() == dir) &&
	     (port->getIndex() == index))) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Port inconsistent between variants"), &err_code);
    } else {
      port->addSize(variant, size);
    }
  }

  return err_code;
}


Populate::ErrorCode
Populate::cModuleDirectives( const char* modName,
                             NUModule * module,
                             LFContext *,
                             NetLookup & inputNetMap,
                             NetLookup & outputNetMap,
                             TimingFlowMap & timingFlowMap)
{
  ErrorCode err_code = eSuccess;

  // Loop through the known output ports and validate them. We should
  // not have any ports in the directives that are not in the module
  // definition. During this process we can create a map from nets to
  // the timing and flow information.
  IODBNucleus::CPortsLoop l;
  for (l = mIODB->loopOutputPorts(modName); !l.atEnd(); ++l)
  {
    // Make sure this port exists
    const UtString* portName = l.getKey();
    NetLookup::iterator pos = outputNetMap.find(*portName);
    if (pos == outputNetMap.end())
    {
      // It doesn't exist. We can either have the wrong direction or a
      // non-existant port.
      if (inputNetMap.find(*portName) == inputNetMap.end())
      {
	POPULATE_FAILURE(mMsgContext->LFCModelInvalidPort(module, portName->c_str()), &err_code);
      }
      else
	POPULATE_FAILURE(mMsgContext->LFCModelInvalidPortDir(module, portName->c_str(),
                                                             "output", "input"), &err_code);
    }
    else
    {
      // It does exist, we can update our map for processing
      IODBNucleus::CPort* port = l.getValue();
      NUNet* net = pos->second;
      timingFlowMap.insert(TimingFlowMap::value_type(net, port));
    }
  } // for

  // Loop through the known input ports and validate them. The same
  // issues go for inputs as outputs defined above.
  IODBNucleus::CPortNamesLoop n;
  for (n = mIODB->loopInputPorts(modName); !n.atEnd(); ++n)
  {
    // Make sure this port exists
    const UtString* portName = *n;
    if (inputNetMap.find(*portName) == inputNetMap.end())
    {
      // It doesn't exist. We can either have the wrong direction or a
      // non-existant port.
      if (outputNetMap.find(*portName) == outputNetMap.end())
      {
	POPULATE_FAILURE(mMsgContext->LFCModelInvalidPort(module, portName->c_str()), &err_code);
      }
      else
	POPULATE_FAILURE(mMsgContext->LFCModelInvalidPortDir(module, portName->c_str(),
                                                             "output", "input"), &err_code);
    }
  }

  return err_code;
}


Populate::ErrorCode
Populate::cModelFunctions( LFContext *,
                           NUCModelInterface* cmodelInterface,
                           NUUseDefNode* useDef,
                           NetLookup& inputNetMap,
                           NetLookup& outputNetMap,
                           TimingFlowMap& timingFlowMap,
                           CModelFunctions& functions)
{
  ErrorCode err_code = eSuccess;

  const SourceLocator &loc = cmodelInterface->getLoc();

  // Loop through the output nets and process them. While we are at it
  // we may warn the user if they have not provided timing or flow
  // information for any given port.
  NetLookup::iterator o;
  for (o = outputNetMap.begin(); o != outputNetMap.end(); ++o)
  {
    // Get the port information for the UD database
    const UtString& name = o->first;
    NUCModelPort* outPort = cmodelInterface->findPort(&name);
    if (not outPort) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Could not find port"), &err_code);
      return err_code;
    }
    

    // Check if there is any timing and/or flow information
    bool dataAdded = false;
    NUNet* outNet = o->second;
    TimingFlowMap::iterator pos = timingFlowMap.find(outNet);
    if (pos != timingFlowMap.end())
    {
      // We have information on this port; Get the entries
      IODBNucleus::CPort* port = pos->second;
      IODBNucleus::CPortLoop l;
      for (l = mIODB->loopPortEntries(port); !l.atEnd(); ++l)
      {
	// Get the timing information for this entry
	IODBNucleus::CPortEntry* portEntry = *l;
	IODBNucleus::CPortTimingType type;
	const UtString* clkPortName;
	type = portEntry->getPortTiming(&clkPortName);
	dataAdded = true;

	// Get the clock net if this is an edge call
	NUNet* clkNet = NULL;
	if (type != IODBNucleus::eCTAsync)
	{
	  if (not clkPortName) {
	    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Could not find clock"), &err_code);
	    return err_code;
	  }
	  clkNet = inputNetMap[*clkPortName];
	  if (not clkNet) {
	    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Could not find clock"), &err_code);
	    return err_code;
	  }
	}

	// Create the cmodel function if it doesn't already exist
	NUCModelFn* cmodelFn = 0;
	ErrorCode temp_err_code = cModelFunction(type, clkNet, cmodelInterface,
						 functions, &cmodelFn);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }

        // Add this output port
        cmodelFn->addDef(outPort);

        // if there is a clock add it as a use. This is because we do
        // pass the clock to the c-model.
        if (clkPortName != NULL) {
          NUCModelPort* clkPort = cmodelInterface->findPort(clkPortName);
          cmodelFn->addUse(outPort, clkPort);
        }

	// Gather the flow information for this output net
        switch(portEntry->getFaninType())
        {
          // cFanin <nothing>
          case IODBNucleus::CPortEntry::eEmptyFanin:
            break;

          // cFanin *
          case IODBNucleus::CPortEntry::eFullFanin:
          {
            // Create the fanin from the module inputs
            ErrorCode temp_err_code = cModelConnectAllPorts(cmodelInterface,
                                                            cmodelFn, outPort,
                                                            clkNet, inputNetMap);
            if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code))
            {
              return temp_err_code;
            }
            break;
          } // case IODBNucleus::CPortEntry::eEmptyFanin:

          // cFanin <list of nets>
          case IODBNucleus::CPortEntry::eListedFanin:
          {
            IODBNucleus::CPortNamesLoop l;
            for (l = portEntry->loopFanin(); !l.atEnd(); ++l)
            {
              const UtString* inPortName = *l;
              NUCModelPort* inPort = cmodelInterface->findPort(inPortName);
              if (not inPort) {
                mMsgContext->CheetahConsistency(&loc, "Could not find port");
                return eFailPopulate;
              }
              cmodelFn->addUse(outPort, inPort);
            }
            break;
          }

          // cFanin not specified
          case IODBNucleus::CPortEntry::eUnspecified:
            // These should cause an error
            INFO_ASSERT(portEntry->getFaninType() == IODBNucleus::CPortEntry::eUnspecified,
                        "Unspecified fanin type");
            break;
        } // switch
      } // for
    } // if

    // Make sure the timing information for this output net is not
    // empty. If it is then we warn the user and create and
    // asynchronous output that has all the inputs as fanin.
    if (!dataAdded)
    {
      // Print warnings.
      mMsgContext->LFCModelUntimedPort(useDef, name.c_str());
      mMsgContext->LFCModelNoFlowPort(useDef, name.c_str(), "(async)");

      // Create the async function
      NUCModelFn* cmodelFn = 0;
      ErrorCode temp_err_code = cModelFunction(IODBNucleus::eCTAsync, NULL,
					       cmodelInterface, functions,
					       &cmodelFn);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      // Add a full output to input flow
      temp_err_code = cModelConnectAllPorts(cmodelInterface, cmodelFn, outPort,
                                            NULL, inputNetMap);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
    }
  } // for

  return err_code;
}


Populate::ErrorCode
Populate::cModelAlwaysBlock( NUModule * module, 
                             LFContext * context,
                             const EdgeNet * edgeNet,
                             NUCModel * cmodel,
                             NUCModelFn * cmodelFn,
                             NetLookup & inputNetMap,
                             NetLookup & outputNetMap)
{
  ErrorCode err_code = eSuccess;

  // Create the cmodel call statement
  NUNetRefFactory* netRefFactory = context->getNetRefFactory();
  NUCModelCall* cmodelCall;
  cmodelCall = new NUCModelCall(cmodel, cmodelFn, cmodelFn->getLoc(),
                                netRefFactory, false);
  // Create the list of output arg connections
  UtSet<NUCModelPort*> covered;
  for (NUCModelFn::DefsLoop l = cmodelFn->loopDefs(); !l.atEnd(); ++l) {
    // Create the output port connection, but not for the null port
    // Note that we don't create an arg connection because it really
    // isn't a true argument. In addition, it has side effects as was
    // shown by bug 2231.
    NUCModelPort* outPort = *l;
    if ((covered.find(outPort) == covered.end()) && !outPort->isNullPort()) {
      NUNet * outNet = 0;
      const SourceLocator loc = module->getLoc();
      ErrorCode temp_err_code = findCModelNet(outPort, outputNetMap,
					      loc, &outNet);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
      if (not outNet) {
	continue;
      }
      temp_err_code = cModelArgConnection(cmodelCall, outPort, outNet);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
      covered.insert(outPort);
    }
  } // for

  // Get the clkNet if there is one
  NUNet* clkNet = NULL;
  if (edgeNet != NULL) {
    clkNet = edgeNet->second;
  }

  // Create the list of c-model input arg connections
  NUNetSet netCovered;
  for (NUCModelFn::DefUseMapLoop l = cmodelFn->loopUD(); !l.atEnd(); ++l) {
    // Create the input port connections (but not the clock
    // connection. It is done below.)
    NUCModelPort* inPort = l.getValue();
    if (covered.find(inPort) == covered.end()) {
      NUNet * inNet = 0;
      const SourceLocator &loc = module->getLoc();
      ErrorCode temp_err_code = findCModelNet(inPort, inputNetMap,
					      loc, &inNet);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
      if (not inNet || (inNet == clkNet)) {
	continue;
      }
      temp_err_code = cModelArgConnection(cmodelCall, inPort, inNet);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
      covered.insert(inPort);
      netCovered.insert(inNet);
    }

  }

  // Create the clock port connection now. It has been delayed till
  // here because it may not be a fanin (which is what the above code
  // covers).
  AtomicCache* strCache = context->getStringCache();
  NUStmtList stmtLst;
  if (clkNet != NULL) {
    // Create a temp net that is assigned to a constant
    const SourceLocator& loc = clkNet->getLoc();
    NUNet* tmpNet;
    tmpNet = module->createTempNetFromImage(clkNet, "cmodel");
    NUExpr* rhs = NULL;
    if (edgeNet->first == eClockPosedge) {
      rhs = NUConst::create(false, 1, 1, loc);
    } else {
      rhs = NUConst::create(false, 0, 1, loc);
    }
    rhs->resize(1);
    NULvalue* lhs = new NUIdentLvalue(tmpNet, loc);
    NUBlockingAssign* assign = new NUBlockingAssign(lhs, rhs, false, loc);
    stmtLst.push_back(assign);
    
    // Create the port connection
    UtString str(clkNet->getName()->str());
    NUCModelPort* clkPort = cmodelFn->getCModelInterface()->findPort(&str);
    ErrorCode temp_err_code = cModelArgConnection(cmodelCall, clkPort,
                                                  tmpNet);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }
  }

  // Create the always block for it
  const SourceLocator& loc = module->getLoc();
  stmtLst.push_back(cmodelCall);
  NUBlock * block = new NUBlock(&stmtLst, 
                                module, 
                                mIODB,
                                netRefFactory, 
                                false, 
                                loc);

  StringAtom* alwaysName = module->newBlockName(strCache, loc);
  NUAlwaysBlock* always = new NUAlwaysBlock(alwaysName, block, 
                                            netRefFactory, loc, false);

  // Create the edge expression if there is an edge net
  if (edgeNet != NULL)
  {
    NUNet* clkNet = edgeNet->second;
    clkNet->putIsEdgeTrigger(true);
    NUIdentRvalue* rvalue = new NUIdentRvalue(clkNet, loc);
    rvalue->resize(rvalue->determineBitSize());
    NUEdgeExpr* edgeExpr;
    ClockEdge edge = edgeNet->first;
    ErrorCode temp_err_code = singleBitEdgeExpr(edge, rvalue, context,
                                                &edgeExpr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }
    edgeExpr->resize(edgeExpr->determineBitSize());
    always->addEdgeExpr(edgeExpr);
  }

  // Add the always block to the module
  module->addAlwaysBlock(always);

  return err_code;
}


Populate::ErrorCode
Populate::cModelFunction( IODBNucleus::CPortTimingType type,
                          NUNet* clkNet,
                          NUCModelInterface* cmodelInterface,
                          CModelFunctions& functions,
                          NUCModelFn **the_fn)
{
  ErrorCode err_code = eSuccess;
  *the_fn = 0;

  const SourceLocator &loc = cmodelInterface->getLoc();

  // Create the edge net for synchronous calls
  EdgeNet* edgeNet = NULL;
  if (type != IODBNucleus::eCTAsync)
  {
    // This is an edge net. Figure out the edge and net
    if (not clkNet) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "No cmodel clock net for edge"), &err_code);
      return err_code;
    }
    ClockEdge edge;
    if (type == IODBNucleus::eCTRise)
      edge = eClockPosedge;
    else
      edge = eClockNegedge;
    edgeNet = new EdgeNet(edge, clkNet);
  }

  // Check if this function has already been created
  *the_fn = 0;
  CModelFunctions::iterator pos = functions.find(edgeNet);
  if (pos == functions.end())
  {
    // Create the context string that we will use to create an enum of
    // all the contexts. We build this off the edgenet
    UtString context;
    context += "eCDS";
    context += cmodelInterface->getName()->str();
    switch(type)
    {
      case IODBNucleus::eCTAsync:
	context += "Async";
	break;

      case IODBNucleus::eCTRise:
	context += "Rise";
	break;

      case IODBNucleus::eCTFall:
	context += "Fall";
	break;

      case IODBNucleus::eCTInvalid:
	POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Invalid cmodel timing type"), &err_code);
	return err_code;
    }
    if (type != IODBNucleus::eCTAsync)
      context += clkNet->getName()->str();

    // Create the c-model function
    *the_fn = new NUCModelFn(cmodelInterface, &context);
    cmodelInterface->addFunction(*the_fn);
    functions.insert(CModelFunctions::value_type(edgeNet, *the_fn));
  }
  else
  {
    // Already created
    *the_fn = pos->second;
    delete edgeNet;
  }

  return err_code;
}


bool
Populate::CompareEdgeNet::operator()(const EdgeNet* en1,
                                     const EdgeNet* en2) const
{
  if ((en1 == NULL) && (en2 == NULL))
    return false;
  else if (en1 == NULL)
    return true;
  else if (en2 == NULL)
    return false;
  else
    return *en1 < *en2;
}


Populate::ErrorCode
Populate::cModelArgConnection( NUCModelCall* cmodelCall, 
                               NUCModelPort* port, 
                               NUNet* net)
{
  ErrorCode err_code = eSuccess;

  PortDirectionT dir = port->getDirection();

  NUCModelArgConnection* conn = NULL;
  const SourceLocator& loc = net->getLoc();

  // Create the port
  NU_ASSERT(!port->isNullPort(), port);
  switch(dir)
  {
    case eInput:
    {
      // Create an rvalue for the net and attach it to the port
      NUExpr* rhs = new NUIdentRvalue(net, loc);
      rhs->resize(rhs->determineBitSize());
      conn = new NUCModelArgConnectionInput(port, rhs, cmodelCall, loc);
      break;
    }

    case eOutput:
    {
      NULvalue* lhs = new NUIdentLvalue(net, loc);
      conn = new NUCModelArgConnectionOutput(port, lhs, cmodelCall, loc);
      break;
    }

    case eBid:
      NULvalue * lhs = new NUIdentLvalue(net, loc);
      conn = new NUCModelArgConnectionBid(port, lhs, cmodelCall, loc);
      break;
  }

  if (not conn) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Could not create argument connection"), &err_code);
    return err_code;
  }

  // Add this connection to the c-model call
  cmodelCall->addArgConnection(conn);

  return err_code;
}

mvvLanguageType Populate::composeInstanceName(mvvNode instance,
					      LFContext* context,
                                              UtString* inst_name,
					      TicProtectedNameManager* ticProtectedNameMgr,
                                              const UtString* suffix)
{
  mvvLanguageType langType = mvvGetLanguageType(instance);

  if (langType == MVV_VERILOG) {
    veNode ve_instance = instance->castVeNode();
    ticProtectedNameMgr->getVisibleName(ve_instance, inst_name);
    if (suffix){
      (*inst_name) += suffix->c_str();
    }
  }
  else if (langType == MVV_VHDL) {
    StringAtom* name = context->vhdlIntern(vhGetName(vhGetLabelObj(reinterpret_cast<vhNode>(instance))));
    (*inst_name) += name->str();
    if (suffix){
      (*inst_name) += suffix->c_str();
    }
  }
  return langType;
}




mvvLanguageType Populate::composeInstanceName(mvvNode instance, 
					      VHDLCaseT vhdlCase,
                                              UtString* inst_name,
					      TicProtectedNameManager* ticProtectedNameMgr)
{
  mvvLanguageType langType = mvvGetLanguageType(instance);

  if (langType == MVV_VERILOG) {
    veNode ve_instance = instance->castVeNode();
    ticProtectedNameMgr->getVisibleName(ve_instance, inst_name);
  }
  else if (langType == MVV_VHDL) {
    JaguarString vh_name(vhGetName(vhGetLabelObj(reinterpret_cast<vhNode>(instance))));
    UtString name (vh_name);
    switch (vhdlCase ){
    case eVHDLLower: name.lowercase(); break;
    case eVHDLUpper: name.uppercase(); break;
    case eVHDLPreserve:                break;
    }
    (*inst_name) += name;
  }
  return langType;
}

StringAtom*
Populate::getInstanceMangledName(mvvNode   instance,
		                 LFContext *context,
                                 const SourceLocator& loc,
				 TicProtectedNameManager* ticProtectedNameMgr)
{

  UtString inst_name;
  mvvLanguageType langType = composeInstanceName(instance, context, &inst_name, ticProtectedNameMgr);
  if (langType == MVV_UNKNOWN_LANGUAGE) {
    LOC_ASSERT( "Unknown language type" == NULL, loc );
  }

  NUScope *scope = context->getDeclarationScope();
  bool slashify = false;
  while ( scope->getScopeType() != NUScope::eModule )
  {
    inst_name.insert( 0, "_" );
    const char* scope_name = scope->getName()->str();

    // Strip the backslash from any slashified name.  Set the bool so
    // we re-slashify the mangled name afterward
    int len = strlen(scope_name);
    if (*scope_name == '\\') {
      if (scope_name[len - 1] == ' ') {
        len -= 2;
        ++scope_name;
        slashify = true;
      }
    }

    // If the name we are inserting in the beginning has any vector
    // syntax, then we are creating an illegal name.  See
    // test/langcov/Generate/genfor8.v
    slashify = slashify || (strchr(scope_name, '[') != NULL);

    inst_name.insert( 0, scope_name, len );
    scope = scope->getParentScope();
  }

  if (slashify) {
    inst_name.insert(0, 1, '\\');
    inst_name << ' ';
  }

  if (langType == MVV_VHDL) {
    return context->vhdlIntern( inst_name.c_str() );
  }
  else {
    return context->getStringCache()->intern( inst_name.c_str() );   
  }
}

Populate::ErrorCode Populate::computeFullCase(NUCase** the_stmt,
                                              LFContext* context)
{
  ErrorCode err_code = eSuccess;

  NUExpr* sel = (*the_stmt)->getSelect();

  // Check with bit width of the select.
  UInt32 maxExprSize = sel->getBitSize();
  UInt32 naturalSelWidth = maxExprSize;
  if (dynamic_cast<NUIdentRvalue*>(sel)   or
      dynamic_cast<NUVarselRvalue*>(sel)  or
      dynamic_cast<NUConcatOp*>(sel)) {

    // if we can determine the max number of bits sel can affect, use
    // that size instead of the one based on the branch constants.
    naturalSelWidth = sel->determineBitSize();
  }

  sel->resize(maxExprSize);

  if (naturalSelWidth > 32) {
    return eSuccess;            // selector too wide for consideration
  }

  // If there are no case items, give up, it is not a full case
  NUCase::ItemLoop itemsIter = (*the_stmt)->loopItems();
  if (itemsIter.atEnd()) {
    return eSuccess;
  }

  // Get binary value of select if it is constant
  NUConst* constSel = sel->castConst();
  DynBitVector selVal;
  DynBitVector selDrv;
  if (constSel != NULL)
    constSel->getValueDrive(&selVal, &selDrv);
    

  // Go through the case items and get a bit vector for each case item. We
  // convert the digits to either 0, 1, or x. 0 and 1 map to 0 and
  // 1. x, z, or ? map to x or to 0 as appropriate for casex and
  // casez. These strings are put in an ordered set for processing
  // later on.

  CaseItemStrings caseItemStr;  // set of strings that represent
                                // constant values of the case labels
                                // (Note the strings are reversed,
                                // that is the first char of each
                                // string represents the lsb of the
                                // case label)
  NUCaseItem* caseItem = NULL;
  bool fullySpecified = false;
  for (; !itemsIter.atEnd(); ++itemsIter)
  {
    // We may have a default case because even though there may be a
    // default the net may not be assigned in the default. If it isn't
    // we still need to see if we have a full case
    caseItem = *itemsIter;

    for (NUCaseItem::ConditionLoop condIter = caseItem->loopConditions();
	 !condIter.atEnd();
	 ++condIter)
    {
      NUCaseCondition* caseCond = *condIter;
      NUConst* constVal = dynamic_cast<NUConst*>(caseCond->getExpr());
      // Only process constants
      if (constVal != NULL)
      {
        UtString itemStr;
        DynBitVector value;
        DynBitVector drive;
        DynBitVector mask;
	// Get the binary vector value for the constant
	constVal->getValueDrive(&value, &drive);
        // Get the case condition mask for casez and casex. This mask
        // has bit set for 1's and 0's, and unset for x/z/?
        NUConst* constMask = dynamic_cast<NUConst*>(caseCond->getMask());
        if (constMask != NULL)
          constMask->getSignedValue(&mask);

        // If mask is anded with drive, then the result has bits set for
        // all x's (for case or casez) and z's (for case). Since there's
        // no x/z in carbon simulation, such conditions would never match
        // select and can be ignored from fully specified computation.
        size_t numBits = value.size();
        DynBitVector driveAndMask(drive);
        if (constMask != NULL)
          driveAndMask &= mask;
        else
        {
          // Prepare a mask with all 1's. This indicates no x/z's.
          mask.resize(numBits);
          mask.set();
        }

        bool ignoreCaseItem = false;
        if (driveAndMask.any())
          ignoreCaseItem = true;

        // If the case select is a constant, try matching the case item
        // constant with it while ignoring the don't cares.
        if (!ignoreCaseItem && (constSel != NULL))
        {
          size_t selBits = selVal.size();
          DynBitVector tmpVec1(selBits);
          tmpVec1 = (mask & selDrv);
          if (tmpVec1.none())
          {
            DynBitVector tmpVec2(selBits);
            tmpVec1 = (mask & selVal);
            tmpVec2 = (mask & value);
            if (tmpVec1 == tmpVec2)
              fullySpecified = true;
          }
          ignoreCaseItem = true;
        }

        for (UInt32 bit = 0; !ignoreCaseItem && (bit < numBits); ++bit)
        {
          // Check if this bit is beyond the size of the select. If
          // so, we only care about non-zeros. If there are any,
          // this condition could never match the select.
          if (bit < naturalSelWidth)
          {
            // If mask is set, then value is don't care (represented with 'x'),
            // otherwise just use value vector. 
            if (!mask.test(bit))
              itemStr << 'x';
            else if (value.test(bit))
              itemStr << '1';
            else
              itemStr << '0';
          }
          // If this value is a don't care, then ignore it. If it's
          // not, then it should be 0/1, since x/z would have resulted
          // in this case item being ignored before this.
          else if (mask.test(bit) && value.test(bit))
            ignoreCaseItem = true;
        }

        if (!ignoreCaseItem)
          caseItemStr.insertWithCheck(itemStr);
      }
    }
  }

  // now check to see if the select expression contains some bits that
  // are constant.  If so then it is not necessary to enumerate all
  // possible case tags in order to set the fully specified flag.
  // (Two possible expressions I can think of are a concat with
  // constants, and a left shift by a constant amount.
  // e.g. {1'b0,a,b} and ({1'b1,a}<<1)
  if ( not fullySpecified and (constSel == NULL) ){
    // add items to the caseItemStr string set that cover any constant
    // bits in the select expression.  e.g. If the expression is only
    // partially constant like: case ({1'b0,1'b1,a3}) then
    // we add to the caseItemStr two values: xx1 and x0x so that
    // they cover all values that cannot be selected by the selector.
    // (remember that the order of chars in each caseItemStr is
    // reversed, that is the left character represents the LSB)
    size_t numSelBits = sel->getBitSize();
    const SourceLocator &loc = sel->getLoc();
    for (UInt32 bit = 0; (bit < numSelBits); ++bit){
      // if sel[bit] is a 1/0 then gen a string that is the inversion of
      // that bit (in the appropriate position) and x's elsewhere

      CopyContext cc(NULL,NULL);
      NUExpr* sel_copy = sel->copy(cc);
      NUExpr* curBit = new NUVarselRvalue(sel_copy, ConstantRange (bit, bit), loc);
      curBit->resize(1);
      curBit = mFold->fold(curBit,eFoldAggressive);

      NUConst* constCurBit = curBit->castConst();
      if (constCurBit == NULL){
        delete curBit;
        continue;
      }
      DynBitVector curVal;
      DynBitVector curDrv;
      constCurBit->getValueDrive(&curVal, &curDrv);
      delete curBit;            // not needed anymore
      if (curDrv.any()){
        continue;  // this must be either z or x in the case condition
      }
      UtString itemStr;
      for (UInt32 pattern_bit = 0; (pattern_bit < numSelBits); ++pattern_bit)
      {
        if (bit != pattern_bit)
          itemStr << 'x';
        else {
          // curVal is a single bit, put the inversion of this bit
          // into the itemStr at this position (see comment above 
          // "add items to the caseItemStr" for why we use the inverted value)
          itemStr << (curVal.any() ? '0' : '1');
        }
      }
      caseItemStr.insertWithCheck(itemStr);
    }
  }
  
  // If all possibilities are fully specified, indicate that in the statement.
  if (fullySpecified || reduceCaseItems(caseItemStr))
  {
    (*the_stmt)->putFullySpecified(true);
    // Delete the default statements as they're never executed.
    if ((*the_stmt)->hasDefault() && (caseItem != NULL))
    {
      (*the_stmt)->putHasDefault(false);
      // The default item is at the end. 
      context->stmtFactory()->removeCaseItem(*the_stmt, caseItem);
    }
  }

  return err_code;
} // Populate::ErrorCode Populate::computeFullCase

bool Populate::reduceCaseItems(const CaseItemStrings& caseItemStrings)
{
  // Go through the constant value vectors and create the appropriate
  // BDD, and check the sel expression for any constant 
  BDDContext bddContext;

  // Initially, the value of this equation is false.
  BDD resultBDD = bddContext.val0();
  BDD constantOne = bddContext.val1();
  // Go through this constant value vector and create the sum of products for all
  // conditions associated with this case statement.

  // We build a BDD with the following rule:
  //   1. iPOS is the identifier for position POS
  //   2. Negate the identifier if there is a '0' in that position.
  //   3. Ignore 'x'.
  //
  // The case statement:
  //   case(sel)
  //   01: BLAH
  //   xx: BLAH
  //   endcase
  // will create a BDD with the following equation:
  //   ((~i0) and i1) or (true)
  for (CaseItemStrings::const_iterator iter = caseItemStrings.begin(),
         e = caseItemStrings.end(); iter != e; ++iter)
  {
    const UtString& constStr = (*iter);
#ifdef DEBUG_BDD
    UtIO::cout() << "string = " << constStr << UtIO::endl;
#endif
    // Initially, the value of a single term is true.
    BDD itemBDD = constantOne;
    UInt32 index;
    UtString ident;
    UtString::const_iterator s;
    for (s = constStr.begin(), index = 0; s != constStr.end(); ++s, ++index)
    {
      const char digit = *s;
      // If the bit is 'x', it's because it's value is x/z for casex
      // or z for casez. Let '1' go into BDD to indicate that any value (0/1)
      // will match.
      if (digit != 'x')
      {
	// Create the identifier for this select bit
        ident.clear();
	ident << "i" << index;
        
	// Create the bdd for this digit
	BDD digitBDD = bddContext.bdd(ident);
	if (digit == '0') {
          // If this item is '0', invert the BDD.
	  digitBDD = bddContext.opNot(digitBDD);
        }
        
	// And it to the item BDD
        itemBDD = bddContext.opAnd(itemBDD, digitBDD);
      }
    } // for

    // The item BDD has been created, OR it into the result
    resultBDD = bddContext.opOr(resultBDD, itemBDD);

#ifdef DEBUG_BDD
    UtIO::cout() << "BDD size = " << bddContext.size(resultBDD) << UtIO::endl;
#endif
  } // for

  // Check if the BDD indicates all possibilities are covered. This
  // occurs if the bdd is 1.
  bool result (resultBDD == constantOne);
  return result;
} // bool Populate::reduceCaseItems


NUVarselLvalue*
Populate::genVarselLvalue(NUNet* net, NULvalue* lval, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselLvalue(lval, sel, loc);
}

NUVarselLvalue*
Populate::genVarselLvalue(NUNet* net, NULvalue* lval, ConstantRange range,
                          NUExpr* selExpr, const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselLvalue(lval, sel, range, loc);
}

NUVarselLvalue*
Populate::genVarselLvalue(NUNet* net, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselLvalue(net, sel, loc);
}

NUVarselLvalue*
Populate::genVarselLvalue(NUNet* net, ConstantRange range, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselLvalue(net, sel, range, loc);
}

NUVarselRvalue*
Populate::genVarselRvalue(NUNet* net, NUExpr* rval, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselRvalue(rval, sel, loc);
}

NUVarselRvalue*
Populate::genVarselRvalue(NUNet* net, NUExpr* rval, ConstantRange range,
                          NUExpr* selExpr, const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselRvalue(rval, sel, range, loc);
}

NUVarselRvalue*
Populate::genVarselRvalue(NUNet* net, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselRvalue(net, sel, loc);
}

NUVarselRvalue*
Populate::genVarselRvalue(NUNet* net, ConstantRange range, NUExpr* selExpr,
                          const SourceLocator& loc,
                          bool retNullForOutOfRange, SInt32 adjust)
{
  NUExpr* sel = normalizeSelect(net, selExpr, adjust, retNullForOutOfRange);
  if (sel == NULL)
    return NULL;
  return new NUVarselRvalue(net, sel, range, loc);
}

NUExpr*
Populate::normalizeSelect(NUNet* net, NUExpr* selExpr, SInt32 adjust,
                          bool retNullForOutOfRange)
{
  NUExpr* sel = selExpr;
  if (net->isMemoryNet())
  {
    NUMemoryNet* mn = net->getMemoryNet();
    sel = mn->normalizeSelectExpr(selExpr, 0, adjust, retNullForOutOfRange);
  }
  else if (net->isVectorNet())
  {
    NUVectorNet* vn = net->castVectorNet();
    sel = vn->normalizeSelectExpr(selExpr, adjust, retNullForOutOfRange);
  }
  return sel;
}

void Populate::addModuleInstance(NUModuleInstance* inst, NUScope* parent)
{
  // Add the instance to the parent module or NamedDeclarationScope
  if (parent->getScopeType() == NUScope::eModule) {
    NUModule* parentMod = dynamic_cast<NUModule*>(parent);
    parentMod->addModuleInstance(inst);
  } else if (parent->getScopeType() == NUScope::eNamedDeclarationScope) {
    NUNamedDeclarationScope* declScope = dynamic_cast<NUNamedDeclarationScope*>(parent);
    declScope->addModuleInstance(inst);
  } else {
    NU_ASSERT(0, inst);
  }
}
