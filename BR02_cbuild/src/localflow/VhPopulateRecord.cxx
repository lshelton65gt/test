// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "util/CbuildMsgContext.h"
#include "compiler_driver/CarbonContext.h"

#include "nucleus/NUAliasDB.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUTF.h"
#include "symtab/STBranchNode.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"


// From VhPopulateExpr.cxx
extern bool isUnconstrainedArray( vhNode );
// From VhPopulateAssign.cxx
extern NUContAssign *helperPopulateContAssign(NULvalue *lvalue,
                                              NUExpr *rvalue,
                                              const SourceLocator& loc,
                                              LFContext *context);

// Return vhNodes for a record net's type definition and the set of
// constraints, if any.
void
VhdlPopulate::getVhdlRecordType( vhNode net, vhNode *vh_typedef,
                                 VhNodeArray *constraintArray /* = NULL */ )
{
  vhNode type, subtype;
  VhObjType objType = vhGetObjType( net );
  if ( objType == VHOBJECT )
  {
    net = vhGetActualObj( net );
    objType = vhGetObjType( net );
  }
  switch( objType )
  {
  case VHAGGREGATE:
  case VHFUNCCALL:
  case VHSELECTEDNAME:
    type = vhGetExpressionType( static_cast<vhExpr>( net )); 
    subtype = NULL;
    break;
  case VHTYPEDECL:
    type = net;
    subtype = NULL;
    break;
  case VHELEMENTDECL:
    subtype = vhGetEleSubTypeInd( net );
    type = vhGetExpressionType( static_cast<vhExpr>( subtype ));
    break;
  case VHINDNAME:
  {
    vhNode prefix = vhGetExpressionType( static_cast<vhExpr>( net ));
    getVhdlRecordType( prefix, vh_typedef, constraintArray );
    return;
    break;
  }
  case VHELEMENTASS:
    getVhdlRecordType( vhGetExpr( net ), vh_typedef, constraintArray );
    return;
    break;
  case VHCONSARRAY:
    type = net;
    subtype = NULL;
    break;
  case VHSUBTYPEIND:
    subtype = net;
    type = vhGetType( net );
    break;
  case VHSIMPLENAME:
    type = vhGetExpressionType( static_cast<vhExpr>( net ));
    subtype = NULL;
    break;
  case VHSLICENAME:
  {
    // Get the type def from the net prefix. Ex: if net is:
    // mem(2 downto 0), it's prefix is mem.
    vhNode prefix = vhGetPrefix(static_cast<vhExpr>(net));
    getVhdlRecordType(prefix, vh_typedef, NULL);
    // Get constraint from net itself. In above example, (2 downto 0)
    if (constraintArray != NULL) {
      vhNode constraint = vhGetDisRange(static_cast<vhExpr>(net));
      constraintArray->push_back(constraint);
    }
    return;
    break;
  }
  default:
    subtype = vhGetSubTypeInd( net );
    type = vhGetExpressionType( static_cast<vhExpr>( subtype ));
    if ( type == NULL )
      type = vhGetType( subtype );
    break;
  }
  if ( objType != VHCONSARRAY )
    *vh_typedef = vhGetTypeDef( type );

  // Here we deal with an arrayed record type.  We'll push the array
  // down into the subelements.
  vhNode vh_constraint;
  objType = vhGetObjType( *vh_typedef );
  if ( objType == VHUNCONSARRAY || objType == VHCONSARRAY )
  {
    if ( objType == VHUNCONSARRAY )
    {
      // The constraint could be specified in element subtype indication (test/vhdl/records-new/bug7771.vhdl):
      //
      // type z is array(integer range <>) of std_logic;
      // subtype y is z;
      // type x is array(3 downto 0) of y(7 downto 0);
      //                                ^^^^^^^^^^^^^ element subtype indication.
      if ( subtype ) {
        vh_constraint = vhGetConstraint( subtype );
      } else {
        if ( vhGetObjType( net ) == VHAGGREGATE )
          vh_constraint = vhGetExprSize( static_cast<vhExpr>( net ));
        else
          vh_constraint = NULL;
      }

      // The constraint could be specified in subtype declaration (test/vhdl/records-new/bug7771.vhdl):
      //
      // type z is array(integer range <>) of std_logic;
      // subtype y is z(7 downto 0);
      //              ^^^^^^^^^^^^^  subtype declaration has constraints.
      // type x is array(3 downto 0) of y;
      //
      if ((vhGetObjType(type) == VHSUBTYPEDECL) && (vh_constraint == NULL)) {
        vh_constraint = vhGetConstraint(type);
      }
    }
    else
    {
      vh_constraint = vhGetIndexConstr( *vh_typedef );
    }

    // Only concern about populating constraintArray if it's non-NULL.
    if ( constraintArray != NULL )
    {
      vhObjType netObjType = vhGetObjType(net);
      // The net's initial value can be fetched if it is of
      // signal/variable/constant/forindex object type. - bug7771
      if ( objType == VHUNCONSARRAY && vh_constraint == NULL &&
           (netObjType == VHSIGNAL || netObjType == VHVARIABLE ||
            netObjType == VHCONSTANT || netObjType == VHFORINDEX) )
      {
        // We do not have our outer bounds available here.  We _have_ to
        // get it from the initial value.  If we can't get an evaluated
        // initial value, we're stuck.
        vhExpr initialValue = vhGetInitialValue( net );
        initialValue = evaluateJaguarExpr( initialValue, true, false );
        if ( initialValue == NULL )
        {
          UtString errMsg;
          errMsg << vhGetSourceFileName( net ) << ":" << vhGetLineNumber( net )
                 << " Unable to determine width of unconstrained record array type.";
          INFO_ASSERT( initialValue != NULL, errMsg.c_str( ));
        }
        vh_constraint = vhGetExprSize( initialValue );
      }
      if ( vh_constraint != NULL )
      {
        JaguarList indexList( vhGetDiscRangeList( vh_constraint ));
        while ( vhNode index = vhGetNextItem( indexList ))
        {
          if ( vhGetObjType( index ) == VHSUBTYPEIND )
          {
            vhNode vh_range = vhGetExprSize( static_cast<vhExpr>( index ));
            if(vh_range == NULL)
            {
              // If vh_range is NULL, then index is VHRANGECONSTRAINT.
              // Get the suptype and the range of it
              vhNode vh_type = vhGetType( index );
              if(vh_type == NULL)
              {
                UtString errMsg;
                errMsg << vhGetSourceFileName( index ) << ":" << vhGetLineNumber( index )
                       << " Unable to determine the size of constrained record array type.";
                INFO_ASSERT( vh_type != NULL, errMsg.c_str( ));
              }
              
              vhNode vh_subtype_ind = vhGetSubTypeInd(vh_type);
              vh_range =  vhGetConstraint(vh_subtype_ind);
            }
            constraintArray->push_back( vh_range );
          }
          else
            constraintArray->push_back( index );
        }
      }
    }

    subtype = vhGetEleSubTypeInd( *vh_typedef );
    type = vhGetExpressionType( static_cast<vhExpr>( subtype ));
    *vh_typedef = vhGetTypeDef( type );
    // If the next type down is also a unconstrained array type (with
    // supplied constraint, hopefully!) recurse through the subtype,
    // which should have the constraint information attached.
    if ( vhGetObjType( *vh_typedef ) == VHUNCONSARRAY )
    {
      getVhdlRecordType( subtype, vh_typedef, constraintArray );
    }

    if ( vhGetObjType( *vh_typedef ) != VHRECORD )
      getVhdlRecordType( *vh_typedef, vh_typedef, constraintArray );
  }
}


UInt32
VhdlPopulate::getVhdlRecordSize( vhNode vh_record, LFContext *context )
{
  const SourceLocator loc = locator( vh_record );
  LOC_ASSERT( vhGetObjType( vh_record ) == VHRECORD, loc );

  UInt32 width = 0;
  RecordIterator ri( this, vh_record );
  for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
  {
    if ( !isRecordNet( fieldElem, NULL ))
    {
      UInt32 fieldWidth = 1;
      VhdlRangeInfoVector recVector;
      recVector.resize( 0 );
      recVector.reserve( 2 );
      ErrorCode err_code = getVhExprRange( static_cast<vhExpr>( fieldElem ), recVector,
                                           loc, false, false, true, context );
      if ( err_code != eSuccess )
      {
        // I almost put an assert here; this should never happen.
        UtString buf( "Unable to determine the size of field " );
        (void)getVhdlObjName( fieldElem, &buf );
        buf << " of record ";
        (void)getVhdlObjName( vh_record, &buf );        
        mMsgContext->JaguarConsistency( &loc, buf.c_str( ));
        return 0;
      }

      const UInt32 numDims = recVector.size();
      for ( UInt32 i = 0; i < numDims; ++i )
      {
        VhdlRangeInfo &dim = recVector[i];
        fieldWidth *= dim.getWidth(loc);
      }
      width += fieldWidth;
    }
  }
  return width;
}


Populate::ErrorCode
VhdlPopulate::getVhdlRecordIndicies( vhExpr vh_named, NUExprVector *indexVector,
                                     LFContext *context )
{
  ErrorCode err_code = eSuccess, temp_err_code;

  // Collect the set of vhExprs for this selected name
  UtVector<vhExpr> selExprVector;
  exprIndices( vh_named, context, &selExprVector );

  // Create a local NUExprVector of the array indices extracted from
  // the record selected name.  These go before any indices already in
  // indexVector, extracted off before calling this method.
  NUExprVector recIdxVector;
  for ( UtVector<vhExpr>::iterator node = selExprVector.begin();
        node != selExprVector.end(); ++node )
  {
    NUExpr *index;
    temp_err_code = expr( *node, context, 0, &index );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    index->resize( index->determineBitSize( ));
    recIdxVector.push_back( index );    
  }

  // Complete recIdxVector with the dimension(s) in indexVector
  recIdxVector.insert( recIdxVector.end(), indexVector->begin(), indexVector->end() );
  // Put the results back in indexVector
  indexVector->swap( recIdxVector );
  return err_code;
}


//! Count the total number of nested record elements.  Return 1 if not a record type.
UInt32
VhdlPopulate::numRecordElements( vhNode net, LFContext *context ) const
{
  UInt32 retval = 0;
  if ( isRecordNet( net, context ))
  {
    // Drill down in Jaguar to get the list of record elements
    vhNode typeDef;
    getVhdlRecordType( net, &typeDef );
    JaguarList elemList(vhGetRecEleList( typeDef ));

    // Loop through the record elements and count them
    vhNode fieldElem;
    while (( fieldElem = vhGetNextItem( elemList )))
    {
      retval += numRecordElements( fieldElem, context );
    }
  }
  else
  {
    retval = 1;
  }

  return retval;
}


// Create a dot-separated record name, e.g. rec.subrec.field
void 
VhdlPopulate::createRecordName( vhExpr node, UtString* retval )
{
  bool needDot = false;
  JaguarList selList(vhGetExprList( node ));
  for ( vhNode element = vhGetNextItem( selList );
        element != NULL; element = vhGetNextItem( selList ))
  {
    if ( needDot == true )
      *retval += '.';
    else
      needDot = true;
    UtString buf;
    (void) VhdlPopulate::getVhdlObjName( element, &buf );
    *retval += buf;
  }
}


//! \brief Create Jaguar VHAGGREGATE nodes for record fields
/*
 * This complex and nonintuitive routine was refactored from
 * recordArrayAggregate when the same sort of behavior was needed for
 * the multidimensional version of the method.  It takes an array of
 * arrays of vhNodes, turning each individual inner array into a Jaguar
 * VHAGGREGATE.  These VHAGGREGATEs become the initializers for each
 * record field.
 *
 * \param [in] aggElems The array of arrays of vhNodes
 * \param [in] typeArray Jaguar type nodes for the concat elements
 * \param [in] vh_scope The scope to create the Jaguar nodes in
 * \param [in] arrayRange The range of each record field subarray
 * \param [in] numRecElems The number of record fields; is the size of the outer array
 * \param [out] array Array to hold the created VHAGGREGATES
 */
static void
sCreateJagAggregates( UtNormalizedArray<vhNode> *aggElems, vhNode *typeArray, vhNode vh_scope,
                      const ConstantRange *arrayRange, UInt32 numRecElems, UtPtrArray &array,
                      vhNode integerType )
{
  // Set up the Jaguar node creation environment
  JaguarString fileName( vhGetSourceFileName( vh_scope ));
  vhwSetCreateScope( vh_scope, fileName.get_nonconst( ));
  // Create the constraint for the new array types
  UtString leftVal;
  leftVal << arrayRange->getMsb();
  vhNode left = vhwCreateDecLit( leftVal.c_str( ), integerType );
  UtString rightVal;
  rightVal << arrayRange->getLsb();
  vhNode right = vhwCreateDecLit( rightVal.c_str( ), integerType );
  const SInt32 arrayIncr = ( arrayRange->getMsb() > arrayRange->getLsb() ? -1 : 1 );
  vhDirType dir = (arrayIncr == -1)?VH_DOWNTO:VH_TO;
  vhNode range = vhwCreateRange( dir, left, right );
  vhList disRangeList = vhwCreateNewList( VH_TRUE );
  vhwAppendToList( disRangeList, range );
  vhNode indexConstraint = vhwCreateIndexConstraint( disRangeList );

  // Create each aggregate and push it into the output array
  for ( UInt32 recElem = 0; recElem < numRecElems; recElem++ )
  {
    // We are creating an array of Jaguar objects of type
    // typeArray[recElem], so we need to manufacture a vhNode that
    // defines this type.
    vhNode vh_subtype = typeArray[recElem];
    if ( vhGetObjType( vh_subtype ) == VHTYPEDECL )
    {
      vh_subtype = vhGetTypeDef( vh_subtype );
    }
    vhNode typeDef = vhwCreateConstrainedArray( 1, indexConstraint, vh_subtype );
    UtString typeName;
    typeName << "ARRAY__OF_"
             << vhGetName( vhGetExpressionType( static_cast<vhExpr>( typeArray[recElem] )));
    vhNode typeDecl = vhwCreateTypeDecl( typeName.c_str(), typeDef );

    // Assemble the Jaguar list from the VhNodeArray created above
    JaguarList newAggList( vhwCreateNewList( VH_TRUE ));
    for ( SInt32 arrayIdx = arrayRange->getMsb();
          arrayRange->contains( arrayIdx );
          arrayIdx += arrayIncr )
    {
      vhwAppendToList( newAggList, aggElems[recElem][arrayIdx] );
    }
    vhNode newAgg = vhwCreateAggregate( newAggList, typeDecl );
    array.push_back( newAgg );
  }
}


// provide support for aggregates of multidimensional arrays of records.
// Accept an a array of dimensions to traverse and recurse down them,
// building a concat of the arrays returned by the lower elements as we
// go.  The end result should be an N element array where N is the
// number of record elements.  Each element in this array is a aggregate
// nested M levels deep, where we were called with M dimensions in the
// range vector.
Populate::ErrorCode
VhdlPopulate::recordArrayAggregate( vhExpr aggregate, const VhdlRangeInfoVector *dimVector,
                                    UtPtrArray &array, LFContext *context )
{
  SourceLocator loc = locator( aggregate );
  const ConstantRange arrayRange( (*dimVector)[0].getMsb(loc), (*dimVector)[0].getLsb(loc) );
  const UInt32 numDims = dimVector->size();
  if ( numDims > 1 )
  {
    ErrorCode err_code = eSuccess, temp_err_code;
    // Build a new range vector without dimension 0, for recursion.
    VhdlRangeInfoVector recurseVector;
    for ( UInt32 i = 1; i < numDims; ++i )
      recurseVector.push_back( (*dimVector)[i] );

    // Value to iterate across the ConstantRange with
    SInt32 incr = 1;
    if ( arrayRange.bigEndian( ))
      incr = -1;
    // How many record elements are there?
    const UInt32 numRecElems = numRecordElements( aggregate, context );
    UtNormalizedArray<vhNode> *aggElems = new UtNormalizedArray<vhNode>[numRecElems];
    UInt32 recElem;
    for ( recElem = 0; recElem < numRecElems; ++recElem )
    {
      aggElems[recElem].putRange( &arrayRange );
    }
    // Create an array to store the type of each record element into
    vhNode *typeArray = new vhNode[numRecElems];

    JaguarList aggrList( vhGetEleAssocList( aggregate ));
    for ( SInt32 i = arrayRange.getMsb(); arrayRange.contains( i ); i += incr )
    {
      vhExpr subAggregate = static_cast<vhExpr>( vhGetNextItem( aggrList ));
      UtPtrArray intermediateVector;
      temp_err_code = recordArrayAggregate( subAggregate, &recurseVector, intermediateVector, context );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
        return temp_err_code;
      }

      const UInt32 intSize = intermediateVector.size();
      // Fill in typeArray from the first array element
      if ( i == arrayRange.getMsb() )
      {
        for ( recElem = 0; recElem < intSize; ++recElem )
        {
          vhExpr vhNewSubAgg = static_cast<vhExpr>( intermediateVector[recElem] );
          vhNode vh_type = vhGetExpressionType( vhNewSubAgg );
          vhNode vh_size = vhGetExprSize( vhNewSubAgg );
          vhNode vh_scope = vhGetScope( vhNewSubAgg );
          JaguarString fileName(vhGetSourceFileName( vh_scope ));
          vhwSetCreateScope( vh_scope, fileName.get_nonconst( ));
          typeArray[recElem] = vhwCreateSubTypeInd( vh_type, vh_size, VH_FALSE, NULL );
        }
      }
      for ( recElem = 0; recElem < intSize; ++recElem )
      {
        aggElems[recElem][i] = static_cast<vhExpr>( intermediateVector[recElem] );
      }

    }
    // Create a new VHAGGREGATE with the contents of intermediateVector
    sCreateJagAggregates( aggElems, typeArray, vhGetScope( aggregate ),
                          &arrayRange, numRecElems, array, getVhIntegerType() );
    delete [] typeArray;
    delete [] aggElems;
    return err_code;
  }
  else
  {
    return recordArrayAggregate( aggregate, &arrayRange, array, context );    
  }
}


// Walk through the aggregate Jaguar record array node.  Convert the
// array aggregate of record aggregates to an array of arrayed record
// elements, all in the Jaguar domain.
Populate::ErrorCode
VhdlPopulate::recordArrayAggregate( vhExpr aggregate, const ConstantRange *arrayRange,
                                    UtPtrArray &array, LFContext *context )
{
  JaguarList arrayAggElems( vhGetEleAssocList( aggregate ));
  SourceLocator loc = locator( aggregate );
  ErrorCode err_code = eSuccess;
  vhNode recAgg = vhGetNextItem( arrayAggElems );

  const UInt32 numRecElems = numRecordElements( recAgg, context );
  // Create one aggList for each record element.  We need a correctly
  // sized array of vhNodes so that we can populate OTHERS correctly.
  UtNormalizedArray<vhNode> *aggElems = new UtNormalizedArray<vhNode>[numRecElems];

  UInt32 recElem;
  for ( recElem = 0; recElem < numRecElems; ++recElem )
  {
    aggElems[recElem].putRange( arrayRange );
  }
  // Create an array to store the type of each record element into
  vhNode *typeArray = new vhNode[numRecElems];

  // Get the type information for the record elements by iterating through
  // the record type information.
  recElem = 0;
  vhNode typeDef;
  getVhdlRecordType( aggregate, &typeDef );
  RecordIterator typeIterator( this, typeDef );
  for ( vhExpr fieldElem = static_cast<vhExpr>( typeIterator.next( ));
        fieldElem != NULL; fieldElem = static_cast<vhExpr>( typeIterator.next( )))
  {
    switch ( vhGetObjType( fieldElem ))
    {
    case VHDECLIT:
    case VHBASELIT:
    case VHIDENUMLIT:
    case VHCHARLIT:
      typeArray[recElem] = vhGetExpressionType( fieldElem );
      break;
    case VHSTRING:
      typeArray[recElem] = vhGetSubTypeInd( vhGetExpressionType( fieldElem ));
      break;
    case VHAGGREGATE:
    {
      vhNode vh_type = vhGetExpressionType( fieldElem );
      if ( vhGetObjType( vh_type ) != VHSUBTYPEIND )
      {
        vhNode vh_size = vhGetExprSize( fieldElem );
        vhNode vh_scope = vhGetScope( fieldElem );
        JaguarString fileName(vhGetSourceFileName( vh_scope ));
        vhwSetCreateScope( vh_scope, fileName.get_nonconst( ));
        vh_type = vhwCreateSubTypeInd( vh_type, vh_size, VH_FALSE, NULL );
      }
      typeArray[recElem] = vh_type;
      break;
    }
    default:
      typeArray[recElem] = vhGetEleSubTypeInd( fieldElem );
      break;
    }
    LOC_ASSERT( recElem < numRecElems, loc );
    LOC_ASSERT( typeArray[recElem] != NULL, loc );
    recElem++;
  }

  // Walk through the aggregate array to construct the arrays for each
  // record element.
  arrayAggElems.rewind();
  // We are walking from left to right through the ConstantRange of this
  // array.  Start at arrayIdx, increment by arrayIncr, and stop when
  // the index is no longer contained in our ConstantRange.
  SInt32 arrayIdx = arrayRange->getMsb();
  const SInt32 arrayIncr = ( arrayIdx > arrayRange->getLsb() ? -1 : 1 );
  // Take each array aggregate item, representing a full record, and
  // split it up into the individual record fields.  These go into the
  // record field arrays.
  while (( recAgg = vhGetNextItem( arrayAggElems )))
  {
    recElem = 0;
    if ( vhGetObjType( recAgg ) == VHELEMENTASS )
    {
      // With named association we need to figure out what index
      // values the record expression is associated with.
      vhNode vh_ele_expr = vhGetExpr( recAgg );
      VhNodeArray fieldArray;
      // First, break the record expression into an array of record field expressions.
      if ( vhGetObjType( vh_ele_expr ) == VHAGGREGATE )
      {
        UtPtrArray tempArray;
        err_code = recordAggregate( vh_ele_expr, tempArray, context, false, false );
        if ( err_code != eSuccess )
        {
          delete [] aggElems;
          delete [] typeArray;
          return err_code;
        }
        const UInt32 size = tempArray.size();
        for ( UInt32 i = 0; i < size; ++i )
        {
          fieldArray.push_back( static_cast<vhNode>( tempArray[i] ));
        }
      }
      else
      {
        createRecordFieldNames( vh_ele_expr, fieldArray, context );
      }
      LOC_ASSERT( fieldArray.size() == numRecElems, loc );

      // Now, figure out what array indices are to get this record expression
      JaguarList choiceList( vhGetChoiceList( static_cast<vhExpr>( recAgg )));
      while ( vhExpr vh_choice = static_cast<vhExpr>( vhGetNextItem( choiceList )))
      {
        VhObjType choiceType = vhGetObjType( vh_choice );
        bool unaryInvert = false;
        switch( choiceType )
        {
        case VHOTHERS:
          // Handle OTHERS, the last possible association.
          for ( arrayIdx = arrayRange->getMsb();
                arrayRange->contains( arrayIdx );
                arrayIdx += arrayIncr )
          {
            // Testing record element 0 is sufficient; the record
            // dimension is either all or none.
            if ( aggElems[0][arrayIdx] == NULL )
            {
              for ( recElem = 0; recElem < numRecElems; ++recElem )
              {
                aggElems[recElem][arrayIdx] = fieldArray[recElem];
              }
            }
          }
          break;
        case VHUNARY:
          // Handle unary +/-
          if ( vhGetOpType( vh_choice ) == VH_UMINUS_OP )
          {
            unaryInvert = true;
          }
          else
          {
            LOC_ASSERT( vhGetOpType( vh_choice ) == VH_UPLUS_OP, loc );
          }
          // Get the operand and fall through to handle the literal underneath
          vh_choice = vhGetOperand( vh_choice );
          choiceType = vhGetObjType( vh_choice );
          LOC_ASSERT( choiceType == VHDECLIT || choiceType == VHBASELIT, loc );
        case VHDECLIT:
        case VHBASELIT:
        {
          // Handle decimal and based literals.  Stupid Interra named
          // the two value access functions differently.
          double val;
          if ( choiceType == VHDECLIT )
          {
            val = vhGetDecLitValue( vh_choice );
          }
          else
          {
            val = vhGetBaseLitValue( vh_choice );
          }
          arrayIdx = SInt32( val );
          if ( unaryInvert == true )
          {
            arrayIdx = -arrayIdx;
          }
          for ( recElem = 0; recElem < numRecElems; ++recElem )
          {
            aggElems[recElem][arrayIdx] = fieldArray[recElem];
          }
        }
        break;
        case VHRANGE:
        {
          // Handle a range.  Interestingly, VHDL does not require that
          // this range direction match that of the target expression.
          VhdlRangeInfo bounds;
          err_code = getLeftAndRightBounds( vh_choice, &bounds, context );
          LOC_ASSERT( bounds.isValidMsbLsb(), loc );
          SInt32 lsb = bounds.getLsb(loc), msb = bounds.getMsb(loc);
          SInt32 high, low;
          if ( msb > lsb )
          {
            high = msb;
            low = lsb;
          }
          else
          {
            high = lsb;
            low = msb;
          }
          for ( arrayIdx = low; arrayIdx <= high; ++arrayIdx )
          {
            for ( recElem = 0; recElem < numRecElems; ++recElem )
            {
              aggElems[recElem][arrayIdx] = fieldArray[recElem];
            }
          }
        }
        break;
        default:
          POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unexpected node type for record array association choice." ), &err_code);
          delete [] aggElems;
          delete [] typeArray;
          return err_code;
          break;
        }
      }
    }
    else if ( vhGetObjType( recAgg ) == VHAGGREGATE )
    {
      // Positional association with an aggregate as the element
      JaguarList recAggList( vhGetEleAssocList( static_cast<vhExpr>( recAgg )));
      LOC_ASSERT( numRecElems == recAggList.size(), loc );
      while ( vhNode fieldElem = vhGetNextItem( recAggList ))
      {
        vhExpr fieldExpr = evaluateJaguarExpr( static_cast<vhExpr>( fieldElem ));
        LOC_ASSERT(( recElem < numRecElems ) && arrayRange->contains( arrayIdx ), loc );
        aggElems[recElem][arrayIdx] = fieldExpr;
        recElem++;
      }
    }
    else
    {
      // Positional notation with a record array aggregate element that
      // is not an aggregate itself--that means that it is a whole
      // record of some sort.
      VhNodeArray fieldArray;
      createRecordFieldNames( recAgg, fieldArray, context );
      LOC_ASSERT( fieldArray.size() == numRecElems, loc );
      for ( recElem = 0; recElem < numRecElems; ++recElem )
      {
        aggElems[recElem][arrayIdx] = fieldArray[recElem];
      }
    }
    // NOTE: advancing arrayIdx is only useful if we're doing positional association.
    arrayIdx += arrayIncr;
  }

  // Now, Create the new Jaguar aggregates for the transformed values.
  // Obtain the jaguar node for the integer type decl.
  sCreateJagAggregates( aggElems, typeArray, vhGetScope( aggregate ), arrayRange,
                        numRecElems, array, getVhIntegerType() );

  // Clean up and exit.
  delete [] aggElems;
  delete [] typeArray;
  return err_code;
}


// Walk through the aggregate Jaguar node and populate the UtPtrArray
// with pointers to NUExprs or Jaguar nodes for each record element.
Populate::ErrorCode
VhdlPopulate::recordAggregate( vhNode aggregate, UtPtrArray &array,
                                   LFContext *context, bool isLvalue,
				   bool createNucleusObjs )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  // First we need to size the UtPtrArray correctly to deal with element
  // associations and others handling
  vhNode typeDef;
  VhNodeArray recConstraintArray;
  getVhdlRecordType( aggregate, &typeDef, &recConstraintArray );
  SourceLocator loc = locator( aggregate );
  LOC_ASSERT( !mCarbonContext->isNewRecord(), loc )
  VhdlRangeInfoVector dimVector;
  if ( !recConstraintArray.empty( ))
  {
    // Convert the vhNodes to VhdlRangeInfos
    const SInt32 numDims = recConstraintArray.size();
    for ( SInt32 i = 0; i < numDims; ++i )
    {
      vhNode vh_constraint = recConstraintArray[i];
      temp_err_code = getVhExprRange( static_cast<vhExpr>( vh_constraint ),
                                      dimVector, loc, false, isLvalue,
                                      true, context );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
        return temp_err_code;
      }
    }
  }

  RecordIterator ri( this, typeDef );
  if ( dimVector.size() > 0 )
  {
    // This is an array of records.  This means that things are going to
    // get ugly.  Jaguar has nicely (for them, not us!) organized this
    // as an array of records.  We need to turn these individual records
    // into vector or memory aggregates for each record field.
    const UInt32 startpos = array.size();
    LOC_ASSERT( startpos == 0, loc );  // I think we have to be starting at 0 here
    temp_err_code = recordArrayAggregate( static_cast<vhExpr>( aggregate ),
                                          &dimVector, array, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    const UInt32 endpos = array.size();
    if ( createNucleusObjs == true )
    {
      for ( UInt32 i = startpos; i < endpos; ++i )
      {
        vhExpr node = static_cast<vhExpr>( array[i] );
        // Turn node (a Jaguar node) into a NUExpr of some form, and
        // place it back into array[i].
        NUExpr *tempExpr = NULL;
        temp_err_code = expr( node, context, 0, &tempExpr );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
          return temp_err_code;
        }
        array[i] = tempExpr;
      }
    }
    return err_code;
  }

  // where to start for positional association
  UInt32 position = array.size();
  // where to index from when counting array elements
  const UInt32 start = position;
  // Make enough space for the new elements
  const UInt32 numRecElems = numRecordElements( aggregate, context );
  array.resize( array.size() + numRecElems );
  const UInt32 max_array_index = array.size()-1;

  if ( vhGetObjType( aggregate ) != VHAGGREGATE )
  {
    mMsgContext->JaguarConsistency( &loc, "Unexpected node type encountered when handling record aggregate." );
    return eFatal;
  }

  union {
    NUExpr* rv;
    NULvalue* lv;} term;

  JaguarList aggList;
  AggregateIterator aggIterator( static_cast<vhExpr>( aggregate ));
  vhNode fieldElem = aggIterator.next();
  while ( fieldElem != NULL )
  {
    if ( aggIterator.isBranch( ))
    {
      // We just descended into an aggregate.  If this isn't a record,
      // process it.  If it is a record, keep going until we find a
      // non-record leaf node.
      if ( isRecordNet( fieldElem, context ))
      {
        fieldElem = aggIterator.next();
        continue; // skip record branch nodes
      }
      // We have a non-record aggregate; we want to use it for the assignment
    }
    if ( vhGetObjType( fieldElem ) == VHELEMENTASS ) // named or others association
    {
      // Get the expression for this element
      vhNode expr_node = vhGetExpr( fieldElem );

      // Figure out which elements we're associating with
      JaguarList choiceList( vhGetChoiceList( (static_cast<vhExpr>( fieldElem ))));
      LOC_ASSERT( choiceList.size() == 1, loc ); // I don't think >1 is possible
      vhExpr choice = static_cast<vhExpr>( vhGetNextItem( choiceList ));
      vhObjType objType = vhGetObjType( choice );

      VhNodeArray actualArray;
      if ( isRecordNet( expr_node, context )) // expr_node is association actual
        createRecordFieldNames( expr_node, actualArray, context ); // list of actuals
      else
        actualArray.push_back( expr_node );
      const UInt32 numSubElems = actualArray.size();

      if ( objType == VHOTHERS ) // others clause
      {
        // Set any remaining NULL elements in the array to the others expression
        for ( UInt32 i = start; i < start + numRecElems; ++i )
        {
          if ( array[i] == NULL )
          {
            // Sanity check: make sure that numSubElems consecutive
            // elements are NULL
            for ( UInt32 sanityIdx = i + 1; sanityIdx < i + numSubElems; ++sanityIdx )
            {
              if ( array[ sanityIdx ] != NULL )
              {
                SourceLocator loc = locator( fieldElem );
                UtString msg, dummy;
                msg << "Not enough room in record aggregate to expand OTHERS expression: ";
                msg << decompileJaguarNode( expr_node, &dummy ) << ".  ";
                msg << "Trying to expand " << numRecElems << " record elements into ";
                msg << sanityIdx - i << " aggregate fields.";
                mMsgContext->JaguarConsistency( &loc, msg.c_str( ));
                return eFatal;
              }
            }
            
            position = i;
            for ( UInt32 j = 0; j < numSubElems; ++j )
            {
              vhExpr elem = static_cast<vhExpr>( actualArray[j] );
              if ( createNucleusObjs )
              {

                if (isLvalue)
                  temp_err_code = lvalue( elem, context, &term.lv);
                else
                  temp_err_code = expr( elem, context, 0, &term.rv );
                if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
                {
                  return temp_err_code;
                }
                NU_ASSERT((position <= max_array_index), term.rv);
                array[ position++ ] = term.rv;
              }
              else {
                NU_ASSERT((position <= max_array_index), term.rv);
                array[ position++ ] = expr_node;
              }
            }
          }
        }
      }
      else if ( objType == VHSIMPLENAME )
      {
        // actualArray contains the list of elements we need; now we
        // need to figured out where to insert them in 'array', and
        // whether to insert Jaguar or Nucleus nodes.
        JaguarString the_name( vhGetSimpleName( choice ));  // The name we're looking for
        ri.rewind(); // start looking at the beginning of the list of record elems each time
        position = start;
        for ( vhNode eleNode = ri.next(); eleNode != NULL; eleNode = ri.next() )
        {
          JaguarString ele_name( vhGetRecEleName( static_cast<vhExpr>( eleNode )));
          // Match the element association by name
          if ( strcasecmp( the_name, ele_name ) == 0 )
          {
            for ( UInt32 j = 0; j < numSubElems; ++j )
            {
              vhExpr elem = static_cast<vhExpr>( actualArray[j] );
              if ( createNucleusObjs )
              {
                // Create the NUExpr or NULvalue
                if (isLvalue)
                  temp_err_code = lvalue( elem, context, &term.lv);
	        else
                  temp_err_code = expr( elem, context, 0, &term.rv);
                if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
                {
                  return temp_err_code;
                }
                NU_ASSERT((position <= max_array_index), term.rv);
                array[ position++ ] = term.rv;
              }
              else{
                NU_ASSERT((position <= max_array_index), term.rv);
                array[ position++ ] = elem;
              }
            }
            break;
          }
          else
          {
            // Not our element; increment our position if this was a
            // leaf node.  'position' is our index into the array we're
            // filling up; we're filling it with leaf nodes only, so we
            // only need to advance position when we encoutered a leaf
            // node.
            if ( !isRecordNet( eleNode, context ))
              position++;
          }
        }
      }
      else
      {
        // error out
        SourceLocator loc = locator( fieldElem );
        UtString buf("Unsupported expression: ");
        buf << gGetVhNodeDescription( fieldElem );
        buf << " in aggregate record assignment.";
        POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, buf.c_str()), &err_code);
        return err_code;
      }
    }
    else // positional association
    {
      VhNodeArray assignArray;
      if ( isRecordNet( fieldElem, context ))
        createRecordFieldNames( fieldElem, assignArray, context );
      else
        assignArray.push_back( fieldElem );

      const UInt32 numElems = assignArray.size();
      for ( UInt32 i = 0; i < numElems; ++i )
      {
        vhExpr elem = static_cast<vhExpr>( assignArray[i] );
        // Get the expression for this assocation
        if ( createNucleusObjs )
        {
          if (isLvalue)
            temp_err_code = lvalue( elem, context, &term.lv);
          else
            temp_err_code = expr( elem, context, 0, &term.rv );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
            return temp_err_code;
          }
          NU_ASSERT((position <= max_array_index), term.rv);
          array[ position++ ] = term.rv;
        }
        else {
          NU_ASSERT((position <= max_array_index), term.rv);
          array[ position++ ] = elem;
        }
      }
    }

    // Advance the iterator
    if ( aggIterator.isBranch( ))
    {
      do
      {
        fieldElem = aggIterator.next();
      }
      while ( aggIterator.numScopesExited() == 0 );
    }
    else
    {
      fieldElem = aggIterator.next();
    }
  }
  return err_code;
}


// Given a vhNode that is a whole record reference or a record element,
// return the STSymbolTableNode that represents it.
const STSymbolTableNode*
VhdlPopulate::lookupSTNodeForRecord( vhNode node, const STSymbolTableNode *parent,
                                     LFContext *context )
{
  const STSymbolTableNode *stnode;
  vhNode vhObj = vhGetActualObj(node);
  vhNode vhScope = vhGetScope(vhObj);
  VhObjType vhScopeObjType = vhGetObjType(vhScope);
  bool isInPackage = false;
  if((vhScopeObjType ==  VHPACKAGEDECL) || (vhScopeObjType == VHPACKAGEBODY) )
    isInPackage = true;

  if ( vhGetObjType( node ) == VHSELECTEDNAME ) 
  {
    stnode = parent;
    JaguarList selNameList( vhGetExprList( static_cast<vhExpr>( node )));
    for ( vhNode nameElem = vhGetNextItem( selNameList ); 
          nameElem != NULL;
          nameElem = vhGetNextItem( selNameList ))
    {
      // A nested array of records can have VHINDNAME nodes show up.
      // Since we're trying to locate the whole net, strip them off.
      if ( vhGetObjType( nameElem ) == VHINDNAME )
      {
        nameElem = vhGetPrefix( static_cast<vhExpr>( nameElem ));
      }
      // Skip over design units or other hierarchy in selected names--we
      // don't care about them.  We only want to have to handle record
      // field names and signals/variables in here.
      if ( vhGetObjType( nameElem ) == VHSIMPLENAME &&
           vhGetSimpleNameType( static_cast<vhExpr>( nameElem )) != VH_RECORD_FIELD_NAME )
        continue;

      if ( vhGetObjType( nameElem ) == VHSELECTEDNAME )
      {
        stnode = lookupSTNodeForRecord( nameElem, stnode, context );
      }
      else
      {
        stnode = lookupSTNodeHelper( nameElem, stnode, context, isInPackage );
      }
      if ( stnode == NULL ) // selected name element not part of a record name
        return NULL;
    }
  }
  else if(vhGetObjType( node ) == VHRECORD)
  {
    stnode = lookupSTNodeForRecord( node, parent, context );
    parent = stnode;
  }
  else
    stnode = lookupSTNodeHelper( node, parent, context, isInPackage );
  return stnode;
}

// Given a vhNode that is a whole record reference or a record element,
// return the STSymbolTableNode that represents it.
const STSymbolTableNode*
VhdlPopulate::lookupSTNodeHelper( vhNode node, const STSymbolTableNode *parent,
                                  LFContext *context, bool isInPackage  )
{
  UtString name;
  (void) getVhdlObjName( node, &name );
  StringAtom *nameAtom = context->vhdlGetIntern( name.c_str( ));
  if ( nameAtom == NULL ) // Selected name element not representing a record
    return NULL;
  
  STSymbolTable *aliasDB = NULL;
  if ( isInPackage )
    aliasDB = context->getRootModule()->getAliasDB();
  else
    aliasDB = context->getModule()->getAliasDB();


  STSymbolTableNode *stnode = NULL;
  while ( parent != NULL )
  {
    stnode = aliasDB->find( parent, nameAtom );
    if ( stnode != NULL )
      break;
    // It's not declared at this level--try the parent
    parent = parent->getParent();
  }
  // We exited the loop before checking the final scope, with a NULL
  // parent.  We need to check this if we haven't found the node yet,
  // since this correstponds to module scope.
  if ( stnode == NULL )
    stnode = aliasDB->find( NULL, nameAtom );
  return stnode;
}


// Assign a value to a record net or aggregate.  This is accomplished by 
// creating an individual assign for each element of the lval record.  
// Both Lvalue and Rvalue aggregates are supported.
Populate::ErrorCode
VhdlPopulate::recordAssign( assignType assign_type, vhExpr vh_lvalue_expr,
                            vhExpr vh_rvalue_expr, LFContext *context, 
                            const SourceLocator &loc )
{
  ErrorCode err_code = eSuccess, temp_err_code = eSuccess;
  if (mCarbonContext->isNewRecord()) 
  {
    const SourceLocator loc = locator( vh_lvalue_expr );
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "Old record method called for new record implementation";
    INFO_ASSERT(!mCarbonContext->isNewRecord(), errMsg.c_str());
  }

  // Get the lvalue of the assign.  We know from the calling context
  // that this is a record or an aggregate.
  vhNode vh_lvalue = NULL;
  if ( vhGetObjType( vh_lvalue_expr ) == VHOBJECT )
    vh_lvalue = vhGetActualObj( vh_lvalue_expr );
  else
    vh_lvalue = vh_lvalue_expr;

  // Get the rvalue of the assign--the context says that this is a
  // record net or an aggregate.
  vhNode vh_rvalue = NULL;
  if ( vhGetObjType( vh_rvalue_expr ) == VHOBJECT )
    vh_rvalue = vhGetActualObj( vh_rvalue_expr );
  else
    vh_rvalue = vh_rvalue_expr;

  vhNode lvalRecIndex = NULL, rvalRecIndex = NULL;
  UtPtrArray lvalArray;
  NUExprArray rvalArray;
  // Resolve vh_lvalue to the Jaguar node we really want to deal with
  switch ( vhGetObjType( vh_lvalue ))
  {
  case VHSIGNAL:
  case VHVARIABLE:
  case VHCONSTANT:
  case VHAGGREGATE:
  case VHSUBPROGBODY:
  case VHSELECTEDNAME:
    break;
  case VHTYPECONV:
  case VHQEXPR:
    vh_lvalue = vhGetActualObj( vhGetOperand( static_cast<vhExpr>( vh_lvalue )));
    break;
  case VHINDNAME:
  {
    LOC_ASSERT( vhGetNoOfDim( vh_lvalue ) == 1, loc );
    JaguarList exprList(vhGetExprList( static_cast<vhExpr>( vh_lvalue )));
    lvalRecIndex = vhGetNextItem( exprList );
    vh_lvalue = vhGetPrefix( static_cast<vhExpr>( vh_lvalue ));
    if ( vhGetObjType( vh_lvalue ) != VHSELECTEDNAME )
    {
      vh_lvalue = vhGetActualObj( vh_lvalue );
    }
    break;
  }
  case VHSLICENAME:
  {
    // Allow record slices with range explicitly specified ex: rec(3 downto 0)
    lvalRecIndex = vhGetDisRange(static_cast<vhExpr>(vh_lvalue));
    if (vhGetObjType(lvalRecIndex) == VHRANGE) {
      vh_lvalue = vhGetPrefix( static_cast<vhExpr>( vh_lvalue ));
      break;
    }
    // else deliberate fall through to the error message.
  }
  default:
  {
    UtString msgStr;
    msgStr << "Unexpected l-value node type (" << gGetVhNodeDescription( vh_rvalue )
           << ") in record assignment.";
    POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, msgStr.c_str( )), &err_code);
    return err_code;
    break;
  }
  }

  // Resolve vh_rvalue to the Jaguar node we really want to deal with
  switch ( vhGetObjType( vh_rvalue ))
  {
  case VHBINARY:
  case VHSIGNAL:
  case VHVARIABLE:
  case VHCONSTANT:
  case VHAGGREGATE:
  case VHFUNCCALL:
  case VHSELECTEDNAME:
    break;
  case VHTYPECONV:
  case VHQEXPR:
    vh_rvalue = vhGetActualObj( vhGetOperand( static_cast<vhExpr>( vh_rvalue )));
    break;
  case VHINDNAME:
  {
    LOC_ASSERT( vhGetNoOfDim( vh_rvalue ) == 1, loc );
    JaguarList exprList(vhGetExprList( static_cast<vhExpr>( vh_rvalue )));
    rvalRecIndex = vhGetNextItem( exprList );
    vh_rvalue = vhGetPrefix( static_cast<vhExpr>( vh_rvalue ));
    break;
  }
  case VHSLICENAME:
  {
    // Allow record slices with range explicitly specified ex: rec(3 downto 0)
    rvalRecIndex = vhGetDisRange(static_cast<vhExpr>(vh_rvalue));
    if (vhGetObjType(rvalRecIndex) == VHRANGE) {
      vh_rvalue = vhGetPrefix( static_cast<vhExpr>( vh_rvalue ));
      break;
    }
    // else deliberate fall through to the error message.
  }
  default:
  {
    UtString msgStr;
    msgStr << "Unexpected r-value node type (" << gGetVhNodeDescription( vh_rvalue )
           << ") in record assignment.";
    POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, msgStr.c_str( )), &err_code);
    return err_code;
    break;
  }
  }

  // fill lvalArray with the lvalue elements to be assigned to.
  if ( vhGetObjType( vh_lvalue ) == VHSUBPROGBODY )
  {
    // This is where we handle a return statement that returns a record
    // type.  The assignment we're dealing with here is of the actual
    // result to the implicit interface variable we create.  This code
    // should only be entered via a call from VhdlPopulate::returnStmt.
    // First we get the NUTF so we can get the set of outputs from it.
    NUScope *scope = context->getDeclarationScope();
    NUTF *task = dynamic_cast<NUTF*>( scope );
    LOC_ASSERT( task, loc ); // I wish NUScopes were derived from NUBase...
    // Grab each output NUNet from the task and put in in lvalArray
    for ( UInt32 i = 0; i < numRecordElements( vh_rvalue, context ); ++i )
    {
      lvalArray.push_back( task->getArg( i ));
    }
  }
  // fill lvalArray with the lvalue record aggregate to be assigned to.
  else if ( vhGetObjType( vh_lvalue ) == VHAGGREGATE)
  {
    UtPtrArray tempArray;
    temp_err_code = recordAggregate( vh_lvalue, tempArray, context, true );
    if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      return err_code;
    const UInt32 tempSize = tempArray.size();
    for ( UInt32 i = 0; i < tempSize; ++i )
      lvalArray.push_back( static_cast<NUExpr*>( tempArray[i] ));
  }
  else
  {
    temp_err_code = expandRecord( vh_lvalue, lvalArray, NULL, context );
    if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      return err_code;
  }

  // fill rvalArray with the rvalue exprs to be assigned from
  temp_err_code = getRvalueExprs(static_cast<vhExpr>(vh_rvalue),
                                 rvalRecIndex, context, loc, rvalArray);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  // Converts :
  //  assign record_vector = record1 & record2; 
  // to:
  //  assign record_vector.f1[1:0] = {record1.f1, record2.f1};
  //  assign record_vector.f2[1:0] = {record1.f2, record2.f2};

  // Walk the lists of nets corresponding to the record elements. Apply
  // the record array index if it exists.  Create a NULvalue for the
  // assignment destination.  (The RHS is already a NUExpr.)  Finally,
  // create an assign for each record field.
  for ( unsigned int index = 0; index < lvalArray.size(); ++index )
  {
    NULvalue *lvalue;
    NUNet *lhsNet = static_cast<NUNet*>( lvalArray[ index ]);
    if ( lvalRecIndex == NULL )
    {
      // LHS record aggregate is already a NULvalue.
      if (vhGetObjType( vh_lvalue ) == VHAGGREGATE )
	lvalue = static_cast<NULvalue*>( lvalArray[ index ]);
      else
	lvalue = new NUIdentLvalue( lhsNet, loc );
    }
    else if (vhGetObjType(lvalRecIndex) == VHRANGE)
    {
      // Get the range explicitly specified for record slice.
      VhdlRangeInfo range;
      temp_err_code =
        getLeftAndRightBounds(static_cast<vhExpr>(lvalRecIndex),
                              &range, context, true);
      if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
        return temp_err_code;
      SInt32 msb = range.getMsb(loc);
      SInt32 lsb = range.getLsb(loc);
      SInt32 incr = 1;
      if (msb > lsb)
        incr = -1;
      // For every index in the range, create a select lvalue expr and
      // finally concat them all.
      NULvalueVector exprs;
      for (SInt32 index = msb;
           (msb > lsb) ? (index >= lsb) : (index <= lsb);
           index += incr)
      {
        NUExpr* bitsel_expr = NUConst::create(false, index, 32, loc);
        temp_err_code = indexedNameLvalueHelper(lhsNet, bitsel_expr, context,
                                                loc, &lvalue);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
          return temp_err_code;
        exprs.push_back(lvalue);
      }
      lvalue = new NUConcatLvalue(exprs, loc);
    }
    else
    {
      // This pushes a record array down onto the record element as a memsel
      NUExpr *bitsel_expr;
      temp_err_code = expr( static_cast<vhExpr>( lvalRecIndex ),
                            context, 0, &bitsel_expr );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
      temp_err_code = indexedNameLvalueHelper( lhsNet, bitsel_expr, context,
                                               loc, &lvalue );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
    }

    NU_ASSERT(rvalArray.size () > index, lvalue );
    NUExpr *rvalue = static_cast<NUExpr*>( rvalArray[ index ]);
    if ( lvalue->getType() == eNUIdentLvalue &&
         static_cast<NUIdentLvalue*>( lvalue )->getIdent()->isMemoryNet() &&
         rvalue->getType() == NUExpr::eNUConcatOp )
    {
      temp_err_code = memoryAssignByAggregate( assign_type, context, 
                                               dynamic_cast<NUMemoryNet*>( lhsNet ),
                                               lvalue,
                                               dynamic_cast<NUConcatOp*>( rvalue ),
                                               loc );
      delete lvalue;
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
    }
    else
    {
      switch ( assign_type )
      {
      case eConcurrent:
      {
        NUContAssign *the_assign;
        the_assign = helperPopulateContAssign( lvalue, rvalue, loc, context );
        context->getModule()->addContAssign( the_assign );
        break;
      }
      case eSequentialNonBlocking:
      {
        NUNonBlockingAssign *the_assign;
        the_assign = context->stmtFactory()->createNonBlockingAssign( lvalue, rvalue, false, loc );
        context->addStmt( the_assign );
        break;
      }
      case eSequentialBlocking:
      {
        NUBlockingAssign *the_assign;
        the_assign = context->stmtFactory()->createBlockingAssign( lvalue, rvalue, false, loc );
        context->addStmt( the_assign );
        break;
      }
      case eInitialValue:
      {
        {
          // Indicate population of declaration initialization statement outside
          // the current scope, in the module's initial block.
          DeclInitIndicator dii(context);
          NUBlockingAssign *the_assign;
          the_assign = context->stmtFactory()->createBlockingAssign( lvalue, rvalue, false, loc );
          addStmtToInitialBlock( context, context->getModule(), the_assign,
                                 lhsNet, loc );
        }
        break;
      }
      }
    }
  }

  return err_code;
}

// Construct expression for each field of the record lvalue from the given
// rvalue expression.
Populate::ErrorCode
VhdlPopulate::getRvalueExprs(vhExpr vh_rvalue, vhNode rvalRecIndex, LFContext* context,
                             const SourceLocator& loc, NUExprArray& rvalArray)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code;
  vhObjType objTyp = vhGetObjType(vh_rvalue);

  switch (objTyp)
  {
  case VHAGGREGATE:
  {
    UtPtrArray tempArray;
    temp_err_code = recordAggregate( vh_rvalue, tempArray, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }

    const UInt32 tempSize = tempArray.size();
    for ( UInt32 i = 0; i < tempSize; ++i )
      rvalArray.push_back( static_cast<NUExpr*>( tempArray[i] ));
  }
  break;
  case VHFUNCCALL:
  {
    temp_err_code = functionCall( static_cast<vhExpr>( vh_rvalue ),
                                context, 0, &rvalArray );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
  }
  break;
  case VHTYPECONV: // Type conversion
  case VHQEXPR:    // Qualified expression ex: type_mark'expression
  {
    vhExpr actual_expr = vhGetOperand(vh_rvalue);
    temp_err_code = getRvalueExprs(actual_expr, rvalRecIndex, context, loc,
                                   rvalArray);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
  }
  break;
  case VHBINARY: // A binary concat of records
  {
    if (vhGetOpType(vh_rvalue) != VH_CONCAT_OP) {
      UtString msgStr;
      msgStr << "Unexpected r-value node type (" << gGetVhNodeDescription( vh_rvalue )
             << ") in record assignment.";
      POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, msgStr.c_str( )), &err_code);
      return err_code;
    }

    // Get field net expressions for left and right operands of concat binary expression.
    // For example: in statement
    //    record_vector <= record1 & record2;
    //
    //    record1  gives expressions list: record1.f1, record1.f2 ..
    //    record2  gives expressions list: record2.f1, record2.f2 ..
    NUExprArray tempArrayLeft;
    vhExpr vhLeft = vhGetLeftOperand(static_cast<vhExpr>(vh_rvalue));
    temp_err_code = getRvalueExprs(vhLeft, rvalRecIndex, context, loc, tempArrayLeft);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    
    NUExprArray tempArrayRight;
    vhExpr vhRight = vhGetRightOperand(static_cast<vhExpr>(vh_rvalue));
    temp_err_code = getRvalueExprs(vhRight, rvalRecIndex, context, loc, tempArrayRight);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    
    // Iterate through left and right operand expression list simulateneously,
    // concatenating operands. For the example above, we build the following
    // concat expression list
    // 
    // {record1.f1, record2.f1}, {record1.f2, record2.f2}
    // 
    const UInt32 tempSize = tempArrayLeft.size();
    // Jaguar would've checked for concat of equal sized records/aggregates.
    // We wouldn't get here unless there's something wrong with cbuild.
    if (tempSize != tempArrayRight.size()) {
      UtString errMsg;
      loc.compose(&errMsg);
      errMsg << "Elements of unequal size being concatenated in rvalue";
      INFO_ASSERT(tempSize == tempArrayRight.size(), errMsg.c_str());
    }
    for (UInt32 exprIdx = 0; exprIdx < tempSize; ++exprIdx)
    {
      NUExprVector exprVector;
      exprVector.push_back(tempArrayLeft[exprIdx]);
      exprVector.push_back(tempArrayRight[exprIdx]);
      
      NUConcatOp* concatExpr = new NUConcatOp(exprVector,
                                              1, /* repeat count of 1 */
                                              loc);
      rvalArray.push_back(concatExpr);
    }
  }
  break;
  default: // These are full records on the RHS
  {
    UtPtrArray tempArray;
    temp_err_code = expandRecord( vh_rvalue, tempArray, NULL, context );

    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }

    // tempArray has NUNets that need to be turned into NUExprs.
    const UInt32 tempSize = tempArray.size();
    for ( UInt32 index = 0; index < tempSize; ++index )
    {
      NUExpr *rvalue;
      NUNet *rhsNet = static_cast<NUNet*>( tempArray[ index ]);
      if ( rvalRecIndex == NULL )
      {
        rvalue = createIdentRvalue( rhsNet, loc );
      }
      else if (vhGetObjType(rvalRecIndex) == VHRANGE)
      {
        VhdlRangeInfo range;
        // Get the range explicitly specified for record slice.
        temp_err_code =
          getLeftAndRightBounds(static_cast<vhExpr>(rvalRecIndex),
                                &range, context, true);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
          return temp_err_code;
        SInt32 msb = range.getMsb(loc);
        SInt32 lsb = range.getLsb(loc);
        SInt32 incr = 1;
        if (msb > lsb)
          incr = -1;
        // For every index in the range, create a select lvalue expr and
        // finally concat them all.
        NUExprVector exprs;
        for (SInt32 index = msb;
             (msb > lsb) ? (index >= lsb) : (index <= lsb);
             index += incr)
        {
          NUExpr* bitsel_expr = NUConst::create(false, index, 32, loc);
          temp_err_code = indexedNameRvalueHelper(rhsNet, bitsel_expr, loc, &rvalue);
          if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
            return temp_err_code;
          exprs.push_back(rvalue);
        }
        rvalue = new NUConcatOp(exprs, 1, loc);
      }
      else
      {
        // This pushes a record array down onto the record element as a memsel
        NUExpr *bitsel_expr;
        temp_err_code = expr(static_cast<vhExpr>(rvalRecIndex),
                             context, 0, &bitsel_expr);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
          return temp_err_code;
        temp_err_code = indexedNameRvalueHelper(rhsNet, bitsel_expr, loc, &rvalue);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
          return temp_err_code;
      }
      rvalArray.push_back( rvalue );
    }
  }
  break;
  } // switch
  
  return err_code;
}


// Take a Jaguar node corresponding to a record and expand it into an
// array of NUNet record elements, looking the NUNets up in the symtab.
// We start searching from the supplied scope.  The result is placed in
// fieldArray.  This must only be called with a net for which
// isRecordNet( net ) is true.
Populate::ErrorCode
VhdlPopulate::expandRecord( vhNode net, UtPtrArray &fieldArray,
                            NUModule *module, LFContext *context )
{
  if (mCarbonContext->isNewRecord()) {
    const SourceLocator loc = locator( net );
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "Old record method called for new record implementation";
    INFO_ASSERT(!mCarbonContext->isNewRecord(), errMsg.c_str());
  }
  if ( vhGetObjType( net ) == VHOBJECT )
    net = vhGetActualObj( net );
  
  const STSymbolTableNode *recBranch = NULL, *parent = NULL;
  if ( module == NULL )
  {
    module = context->getModule();
    parent = context->getDeclScopeSymtabNode();
    recBranch = lookupSTNodeForRecord( net, parent, context );
  }
  else
  { 
    // We need lookupSTNodeForRecord to look in the module provided
    // in the call, not in the current scope.
    context->pushDeclarationScope( module );
    recBranch = lookupSTNodeForRecord( net, parent, context );
    context->popDeclarationScope();
  }

  if ( recBranch == NULL )
  {
    const SourceLocator loc = locator( net );
    UtString name;
    (void) getVhdlObjName( net, &name );
    const char *mname = module->getName()->str();
    mMsgContext->RecordSymtabEntryMissing( &loc, name.c_str(), mname );
    return eFatal;
  }

  // Drill down in Jaguar to get the list of record elements
  vhNode typeDef;
  getVhdlRecordType( net, &typeDef );
  RecordIterator ri( this, typeDef );

  for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
  {
    // If the iterator has exited scopes, update our context appropriately
    for ( UInt32 n = ri.numScopesExited(); n > 0; --n )
      recBranch = recBranch->getParent();

    JaguarString fieldName( vhGetName( fieldElem ));
    StringAtom *fieldAtom = context->vhdlGetIntern( fieldName );

    STBranchNodeIter branchIter( const_cast<STBranchNode*>( recBranch->castBranch( )));
    const STSymbolTableNode *fieldNode;
    int index;
    while (( fieldNode = branchIter.next( &index )))
    {
      if ( fieldNode->strObject() == fieldAtom )
        break;
    }

    if ( fieldNode == NULL )
    {
      const SourceLocator loc = locator( net );
      UtString name;
      (void) getVhdlObjName( net, &name );
      const char *mname = module->getName()->str();
      mMsgContext->RecordSymtabEntryMissing( &loc, name.c_str(), mname );
      return eFatal;
    }

    if ( isRecordNet( fieldElem, context ))
    {
      recBranch = fieldNode;
    }
    else
    {
      const STAliasedLeafNode *fieldLeaf = fieldNode->castLeaf();
      fieldLeaf = fieldLeaf->getStorage(); // The storage node of a port record is the actual port
      NUNet *leafNet = NUAliasBOM::castBOM( fieldLeaf->getBOMData( ))->getNet( );
      fieldArray.push_back( leafNet );
    }
  }
  return eSuccess;
}


void
VhdlPopulate::createRecordFieldNames( vhNode net, VhNodeArray &fieldArray,
                                      LFContext *context )
{
  // Create a list of selected name pieces to create new names with
  VhNodeArray namePartArray;
  vhNode vh_actual;
  vhNode vh_index = NULL;
  vhObjType objType = vhGetObjType( net );
  if ( objType == VHSELECTEDNAME )
  {
    JaguarList netEleList( vhGetExprList( static_cast<vhExpr>( net )));
    for ( vhNode eName = vhGetNextItem( netEleList );
          eName != NULL; eName = vhGetNextItem( netEleList ))
    {
      namePartArray.push_back( eName );
    }
    vh_actual = vhGetActualObj( net );
  }
  else if ( objType == VHINDNAME )
  {
    vh_actual = vhGetActualObj( vhGetPrefix( static_cast<vhExpr>( net )));
    JaguarList indexList( vhGetExprList( static_cast<vhExpr>( net )));
    vh_index = vhGetNextItem( indexList );
    if ( vhGetNoOfDim( net ) > 1 )
    {
      SourceLocator loc = locator( net );
      mMsgContext->JaguarConsistency( &loc, "Only 1-dimensional arrays of records are supported" );
      return;
    }
    namePartArray.push_back( vh_actual );
  }
  else if (objType == VHSLICENAME)
  {
    vh_actual = vhGetActualObj(vhGetPrefix(static_cast<vhExpr>(net)));
    vh_index = vhGetDisRange(static_cast<vhExpr>(net));
    if (vhGetObjType(vh_index) != VHRANGE)
    {
      SourceLocator loc = locator(net);
      UtString msg_str;
      msg_str << "Unsupported record slice type (" << gGetVhNodeDescription(net)
              << ")";
      mMsgContext->JaguarConsistency(&loc, msg_str.c_str());
      return;
    }
    net = vh_actual;
    namePartArray.push_back( vh_actual );
  }
  else
  {
    namePartArray.push_back( net );
    vh_actual = net;
  }
  if ( vhGetObjType( vh_actual ) == VHOBJECT )
  {
    vh_actual = vhGetActualObj( vh_actual );
  }

  // Drill down in Jaguar to get the list of record elements
  vhNode typeDef;
  getVhdlRecordType( net, &typeDef );
  RecordIterator ri( this, typeDef );

  for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
  {
    for ( UInt32 n = ri.numScopesExited(); n > 0; --n )
      namePartArray.pop_back();
    JaguarString fieldName( vhGetName( fieldElem ));
    if ( isRecordNet( fieldElem, context ))
    {
      vhNode simpleName = vhwCreateSimpleName( fieldName, VH_RECORD_FIELD_NAME );
      namePartArray.push_back( simpleName );
    }
    else
    {
      // We need to set jaguar scope in order to create any object properly.
      vhNode scope = vhGetScope( fieldElem );
      JaguarString fileName( vhGetSourceFileName( fieldElem ));
      vhwSetCreateScope(scope, fileName.get_nonconst( ));
      // List to list selected name elements in
      JaguarList selList( vhwCreateNewList( VH_TRUE ));
      const UInt32 size = namePartArray.size();
      for ( UInt32 i = 0; i < size; ++i )
        vhwAppendToList( selList, namePartArray[i] );

      vhNode simpleName = vhwCreateSimpleName( fieldName, VH_RECORD_FIELD_NAME );
      vhwAppendToList( selList, simpleName );

      vhNode vh_type = vhGetExpressionType( static_cast<vhExpr>( vhGetEleSubTypeInd( fieldElem )));
      vhNode actual = vhwCreateSelectedName( selList, vh_actual, vh_type );
      if ( vh_index != NULL )
      {
        vhObjType obj_type = vhGetObjType(vh_index);
        if (obj_type == VHRANGE)
        {
          actual = vhwCreateSliceName(vh_index, actual, vh_type);
        }
        else
        {
          // If we had an indexed name coming in, we need each element of
          // fieldArray to be an indexed name.  Create an indexed name
          // from actual and vh_index.
          JaguarList indexList( vhwCreateNewList( VH_TRUE ));
          vhwAppendToList( indexList, vh_index );
          actual = vhwCreateIndexName( 1, actual, indexList, vh_type );
        }
      }

      fieldArray.push_back( actual );
    }
  }
}


// Take a Jaguar node corresponding to a record and expand it into an
// array of vhNode record elements, creating new Jaguar nodes for each
// of the needed selected names.  The result is placed in fieldArray.
// This must only be called with a net for which isRecordNet( net ) is
// true.
void
VhdlPopulate::expandRecordPortActuals( vhNode net, VhNodeArray &fieldArray,
                                       LFContext *context )
{
  if (mCarbonContext->isNewRecord()) {
    const SourceLocator loc = locator( net );
    UtString errMsg;
    loc.compose(&errMsg);
    errMsg << "Old record method called for new record implementation";
    INFO_ASSERT(!mCarbonContext->isNewRecord(), errMsg.c_str());
  }
  if ( vhGetObjType( net ) == VHQEXPR ){
    net = vhGetOperand( static_cast<vhExpr>( net ));
  }
  if ( vhGetObjType( net ) == VHOBJECT )
    net = vhGetActualObj( net );
    
  if ( vhGetObjType( net ) == VHAGGREGATE )
  {
    UtPtrArray tempArray;
    ErrorCode err_code = recordAggregate( net, tempArray, context, false, false );
    if ( err_code != eSuccess )
    {
      return;
    }
    const UInt32 aSize = tempArray.size();
    for ( UInt32 i = 0; i < aSize; ++i )
      fieldArray.push_back( static_cast<vhNode>( tempArray[i] ));
  }
  else
    createRecordFieldNames( net, fieldArray, context );
}


bool
VhdlPopulate::isRecordNet( vhNode vh_net, LFContext *context )
{
  vhObjType objType = vhGetObjType( vh_net );
  vhNode type;
  switch( objType )
  {
  case VHQEXPR:
    type = vhGetType( vh_net );
    break;
  case VHOBJECT:
  case VHAGGREGATE:
    type = vhGetExpressionType( static_cast<vhExpr>( vh_net ));
    break;
  case VHSIGNAL:
  case VHVARIABLE:
  case VHCONSTANT:
  {
    vhExpr subtype = static_cast<vhExpr>( vhGetSubTypeInd( vh_net ));
    type = vhGetType( subtype );
    break;
  }
  case VHELEMENTDECL:
  case VHCONSARRAY:
  case VHUNCONSARRAY:
  {
    vhExpr subtype = static_cast<vhExpr>( vhGetEleSubTypeInd( vh_net ));
    type = vhGetType( subtype );
    break;
  }
  case VHINDNAME:
    return isRecordNet( vhGetPrefix( static_cast<vhExpr>( vh_net )), context );
    break;
  case VHTYPEDECL:
    type = vh_net;
    break;
  case VHFUNCCALL:
    type = vhGetExpressionType( static_cast<vhExpr>( vh_net ));
    break;
  case VHSELECTEDNAME:
    if ( context != NULL )
    {
      const STSymbolTableNode *parent = context->getDeclScopeSymtabNode();
      const STSymbolTableNode *stnode = lookupSTNodeForRecord( vh_net, parent,
                                                               context );
      if ( stnode && stnode->castBranch() != NULL )
        return true;
    }
    return false;
    break;
  case VHSLICENAME:
    return isRecordNet( vhGetPrefix( static_cast<vhExpr>(  vh_net )), context );
    break;
  case VHELEMENTASS:
    return isRecordNet( vhGetExpr( vh_net ), context );
    break;
  case VHSUBTYPEIND:
    return isRecordNet( vhGetType( vh_net ), context );
    break;
  default:
    return false;
    break;
  }

  vhNode typeDef = vhGetTypeDef( type );
  if ( vhGetObjType( typeDef ) == VHUNCONSARRAY ||
       vhGetObjType( typeDef ) == VHCONSARRAY )
    return isRecordNet( typeDef, context );
  if ( vhGetObjType( typeDef ) == VHRECORD )
    return true;
  return false;
}


// Handle VHDL records.  A new NUNamedDeclarationScope will be
// constructed for the record type, and its elements will be data
// members of that scope.
Populate::ErrorCode
VhdlPopulate::processRecordNetDecl(vhNode vh_net, StringAtom *recName,
                                   NetFlags baseFlags, LFContext *context )
{
  SourceLocator loc = locator( vh_net );
  LOC_ASSERT( !mCarbonContext->isNewRecord(), loc );
  ErrorCode err_code = eSuccess, temp_err_code;

  // Create the declaration scope for the top-level record net
  NUNamedDeclarationScope *the_record;
  the_record = new NUNamedDeclarationScope( recName, context->getDeclarationScope(),
                                            mIODB, context->getNetRefFactory(),
                                            loc );
  // Push the new scope onto the stack
  context->pushDeclarationScope( the_record );

  // Drill down in Jaguar to get the list of record elements
  UtStack<UInt32> constraintArrayPos;
  VhNodeArray recConstraintArray;
  vhNode typeDef;
  getVhdlRecordType( vh_net, &typeDef, &recConstraintArray );
  RecordIterator ri( this, typeDef );
  NetFlags flags;

  for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
  {
    // If the iterator has exited scopes, update our context appropriately
    for ( UInt32 n = ri.numScopesExited(); n > 0; --n )
    {
      context->popDeclarationScope();
      // remove the last N sets of constraints as we ascend the record hierarchy
      recConstraintArray.resize( constraintArrayPos.top( ));
      constraintArrayPos.pop();
    }

    UtString newName;
    getVhdlObjName( fieldElem, &newName );
    StringAtom *fieldName = context->vhdlIntern( newName.c_str( ));
    if ( isRecordNet( fieldElem, context ))
    {
      constraintArrayPos.push( recConstraintArray.size()); // save where we were
      getVhdlRecordType( fieldElem, &typeDef, &recConstraintArray );
      // This is a nested record field--create a new declaration scope for it
      const SourceLocator loc = locator( fieldElem );
      the_record = new NUNamedDeclarationScope( fieldName,
                                                context->getDeclarationScope(),
                                                mIODB, context->getNetRefFactory(),
                                                loc );
      // Push the new scope onto the stack
      context->pushDeclarationScope( the_record );
    }
    else
    {
      // Not a nested record; allocate the leaf net
      NUNet *the_net = NULL;
      NUScope *declScope = context->getDeclarationScope();

      flags = NetFlags( baseFlags | eDMWireNet | eBlockLocalNet );
      // We do not infer state for any locals to tasks or functions
      if ( declScope->inTFHier( ))
        flags = NetFlags( flags | eNonStaticNet );
      // Set the signed flag of this record element if it's a signed net
      calcSignedNetFlags(fieldElem, &flags, context);

      temp_err_code = allocNet( fieldElem, fieldName, flags, declScope,
                                &the_net, context, &recConstraintArray );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

      if ( the_net )
        declScope->addLocal( the_net );
    }
  }
  // If the iterator has exited scopes, update our context appropriately
  for ( UInt32 n = ri.numScopesExited(); n > 0; --n )
    context->popDeclarationScope();

  // Assign the initial value, if any, but not for ports or aliases.
  if ( ((baseFlags & ePortMask) == 0) && !isAliasObject( vh_net ))
  {
    vhExpr initVal = vhGetInitialValue( vh_net );
    if ( initVal != NULL )
    {
      initVal = evaluateJaguarExpr( initVal );
      temp_err_code = recordAssign( eInitialValue,
                                    static_cast<vhExpr>( vh_net ),
                                    initVal, context, loc );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
     }
  }

  return err_code;
}


// This function returns the net.
// Input:  node - this is jaguar node. It could be the whole record (rec1), selected field (rec1.a) or the field of a record (a). 
//         rec  - this argument is used only when the node argument is a field of the record. The rec is the whole rec name,
// Output: the_net - this is the net. If the net is in the same module, the_net is the pointer to the existing net.
//                   If the net is in a package, a hiearchical reference to that net gets created and the_net points to it.
Populate::ErrorCode
VhdlPopulate::lookupRecordElement( NUScope* parentScope, vhExpr node, vhExpr rec,  LFContext *context,
                                   NUNet **the_net )
{
  bool arrayOfRecords = false;
  ErrorCode err_code = eSuccess;

  const SourceLocator loc = locator( node );
  if ( (vhGetObjType( node ) != VHSELECTEDNAME) &&  
       (vhGetObjType( node ) != VHELEMENTDECL) )
  {
    vhNode typeDef = vhGetTypeDef(node);

    switch(vhGetObjType( typeDef ))
    {
      case VHCONSARRAY:
      case VHUNCONSARRAY:
	arrayOfRecords = true;
	break;

      case VHRECORD:
        break;

      default:
	POPULATE_FAILURE(mMsgContext->RecordNoResol( &loc ), &err_code);
        return err_code;
    }
  }

  vhNode  nodeAct;
  if(vhGetObjType( node ) == VHELEMENTDECL)
    nodeAct = rec;
  else
    nodeAct = vhGetActualObj( node );

  // Check for the scope of the node
  vhNode vh_scope = vhGetScope(nodeAct);
  VhObjType scopeObjType = vhGetObjType(vh_scope);
  NUModule *curModule = context->getModule();
  NUModule *rootModule = context->getRootModule();

  bool isInPackage = false;
  if( (scopeObjType == VHPACKAGEDECL) || (scopeObjType == VHPACKAGEBODY))
    isInPackage = true;

  // Walk over the Jaguar selected name list and descend through the
  // symbol table to find the leaf node corresponding to the full
  // selected name.

  // If the record is not declared at module scope (like a record
  // variable in a process or subprogram, or a signal declared in a
  // block), we need to locate the scope that the record is located in.
  const STSymbolTableNode *parent = context->getScopeSymtabNode(parentScope);
  const STSymbolTableNode *targetNode;

  if(vhGetObjType( node ) == VHELEMENTDECL)
  {
    targetNode = lookupSTNodeHelper( node, parent, context, isInPackage );    
  }
  else
    targetNode = lookupSTNodeForRecord( node, parent, context );


  // We should not ever get here with targetNode = NULL
  LOC_ASSERT( targetNode != NULL, loc );
  const STAliasedLeafNode *leaf = targetNode->castLeaf();
  if ( leaf != NULL )
  {
    // we found the leaf node we want.  Extract the net.
    NUNet *realNet;
    realNet = ( NUAliasBOM::castBOM( leaf->getStorage()->getBOMData( )))->getNet( );

    NU_ASSERT(realNet != NULL, realNet);

    if ( isInPackage && (curModule != rootModule))
    {
      // The node is in the package and it is not in the root module.
      // We have to create hierarchical reference to it

      // First find the package scope
      JaguarString libName( vhGetLibName( vh_scope ));
      JaguarString packName( vhGetPackName( vh_scope ));
      StringAtom *packIdStr = createPackIdStr( libName, packName, context );
      NUNamedDeclarationScope *packScope;
      lookupPackage( packIdStr, &packScope, context );

      StringAtom* netAtom = NULL;

      JaguarString recName( vhGetName(nodeAct));
      StringAtom *recNameAtom = context->vhdlIntern(recName);
      netAtom = context->vhdlIntern(realNet->getName()->str());
      // we should never fail to find the field of the record
      LOC_ASSERT( netAtom != NULL, loc );
 
      // create hierarchical reference to the net
      StringAtom *moduleAtom = rootModule->getName();
      UtString libString;
      getActualVhdlLibName(libName, libString);
      StringAtom *libAtom = context->vhdlIntern( libString.c_str( ));
      StringAtom *packAtom = context->vhdlIntern( packName );
      AtomArray netHierName;
      netHierName.push_back( moduleAtom );
      netHierName.push_back( libAtom );
      netHierName.push_back( packAtom );
      netHierName.push_back( recNameAtom );
      netHierName.push_back( netAtom );
      *the_net = curModule->findNetHierRef( netHierName );
      // the hierarchical reference would not be found first time,
      // but the second time it would be found.
      if( *the_net == NULL)
      {
        *the_net = realNet->buildHierRef(curModule, netHierName);
      }
    }
    else
    { 
      // The node is in the current module, just return it
      *the_net = realNet;
    }
  }
  else
  {
    // If we have a branch node we need to find the leaf, at the same
    // level of hierarchy as the branch, that references the
    // NUCompositeNet that we really want.
    const STBranchNode *branch = targetNode->castBranch();
    LOC_ASSERT( branch != NULL, loc );
    NUScope *scope = ( NUAliasBOM::castBOM( branch->getBOMData( )))->getScope( );
    LOC_ASSERT( scope->getScopeType() == NUScope::eNamedDeclarationScope, loc );
    NUNamedDeclarationScope *dScope = dynamic_cast<NUNamedDeclarationScope*>( scope );
    NUCompositeNet *cNet = NULL;

    err_code = mPopulate->lookupCompositeNet( dScope, &cNet );
    if ( err_code == eSuccess )
    {
      if ( isInPackage && (curModule != rootModule))
      {
	// The node is in the package and it is not in the root module.
	// We have to create hierarchical reference to it

	// First find the package scope
	JaguarString libName( vhGetLibName( vh_scope ));
	JaguarString packName( vhGetPackName( vh_scope ));
	StringAtom *packIdStr = createPackIdStr( libName, packName, context );
	NUNamedDeclarationScope *packScope;
	lookupPackage( packIdStr, &packScope, context );

	// create hierarchical reference to the net
	StringAtom *moduleAtom = rootModule->getName();
	UtString libString;
	libString << "$lib_" << libName;
	StringAtom *libAtom = context->vhdlIntern( libString.c_str( ));
	StringAtom *packAtom = context->vhdlIntern( packName );
	AtomArray netHierName;
	netHierName.push_back( moduleAtom );
	netHierName.push_back( libAtom );
	netHierName.push_back( packAtom );

	NUCompositeNet *cNetHierRef = (NUCompositeNet *) cNet->createHierRef(netHierName, curModule, eNoneNet, cNet->getLoc());
        *the_net = cNetHierRef;
      }
      else
        *the_net = cNet;
    }
    else
    {
      UtString msg_text;
      bool found_name = getVhdlObjName( node, &msg_text );
      POPULATE_FAILURE(mMsgContext->CouldNotFindNet( &loc, "record", 
                                                     scope->getParentScope()->getName()->str(), 
                                                     msg_text.c_str()), &err_code);
      LOC_ASSERT( found_name, loc );
      *the_net = NULL;
    }
  }
  return err_code;
}



Populate::ErrorCode
VhdlPopulate::processRecordPortDecl( vhNode port, LFContext *context )
{
  LOC_ASSERT( !mCarbonContext->isNewRecord(), locator(port) );
  ErrorCode err_code = eSuccess;

  // Get the base record name
  JaguarString recName(vhGetName( port ));
  StringAtom *recAtom = context->vhdlIntern( recName );

  NetFlags flags, baseFlags;
  ErrorCode temp_err_code = netFlags( port, recAtom, context, &baseFlags );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;
  baseFlags = NetFlags(baseFlags | eAllocatedNet);

  // Create the scoped nets (master nets in the symtab)
  temp_err_code = processRecordNetDecl( port, recAtom, baseFlags, context );
  if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  // Locate the record root in the symbol table
  NUModule *module = context->getModule();
  STSymbolTable *aliasDB = module->getAliasDB();
  NUScope *declaration_scope = context->getDeclarationScope();
  const STSymbolTableNode *parent = NULL;
  if ( declaration_scope->getScopeType() != NUScope::eModule )
    parent = context->getDeclScopeSymtabNode();
  const STSymbolTableNode *recBranch = aliasDB->find( parent, recAtom );

  // Drill down in Jaguar to get the list of record elements
  UtStack<UInt32> constraintArrayPos;
  VhNodeArray recConstraintArray;
  vhNode typeDef;
  getVhdlRecordType( port, &typeDef, &recConstraintArray );
  RecordIterator ri( this, typeDef );

  // Loop through the record elements and create NUNets for all of them
  for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
  {
    // If the iterator has exited scopes, update our context appropriately
    for ( UInt32 n = ri.numScopesExited(); n > 0; --n )
    {
      recBranch = recBranch->getParent();
      // Remove the last N set of constraints as we ascend the record hierarchy
      recConstraintArray.resize( constraintArrayPos.top( ));
    }

    JaguarString fieldName( vhGetName( fieldElem ));
    StringAtom *fieldAtom = context->vhdlGetIntern( fieldName );

    if ( isRecordNet( fieldElem, context ))
    {
      constraintArrayPos.push( recConstraintArray.size()); // save where we were
      getVhdlRecordType( fieldElem, &typeDef, &recConstraintArray );
      recBranch = aliasDB->find( recBranch, fieldAtom );
    }
    else
    {
      NUNet *the_port = NULL;
      StringAtom *portAtom = module->gensym( recName, fieldName );

      flags = baseFlags;
      temp_err_code = netFlags( port, portAtom, context, &flags );
      if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

      flags = NetFlags( flags | eAllocatedNet );

      if (declaration_scope->getScopeType() != NUScope::eModule)
        flags = NetFlags(flags | eBlockLocalNet);

      // We do not infer state for any locals to tasks or functions
      // Also handle task/function args
      if ( declaration_scope->inTFHier( ))
        flags = NetFlags( flags | eNonStaticNet );
      // Set the signed flag of this record element if it's a signed net
      calcSignedNetFlags(fieldElem, &flags, context);

      temp_err_code = allocNet( fieldElem, portAtom, flags, declaration_scope,
                                &the_port, context, &recConstraintArray );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

      // Continue on and hook up storage and master aliases.
      STAliasedLeafNode *fieldLeaf = aliasDB->find( recBranch, fieldAtom )->castLeaf();
      STAliasedLeafNode *portLeaf = the_port->getNameLeaf();
      portLeaf->linkAlias( fieldLeaf );
      portLeaf->setThisStorage();
      fieldLeaf->setThisMaster();
      NUNet *leafNet = NUAliasBOM::castBOM( fieldLeaf->getBOMData( ))->getNet();
      leafNet->putIsRecordPort( true );
      leafNet->putIsAllocated( false );
      leafNet->putIsAliased( true );

      the_port->putIsAliased( true );
      the_port->putIsRecordPort( true );
      switch ( declaration_scope->getScopeType( ))
      {
      case NUScope::eModule:
      {
        module->addPort( the_port );
        break;
      }
      case NUScope::eTask:
      {
        NUTF::CallByMode call_mode = getParamMode( vhGetObjType( port ) == VHSIGNAL );
        (dynamic_cast<NUTF*>( declaration_scope ))->addArg( the_port, call_mode);
        break;
      }
      default:
      {
        const SourceLocator loc = locator( fieldElem );
        POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unexpected scope for a record port" ), &err_code);
        return err_code;
      }
      }
    }
  }

  return err_code;
}


void
VhdlPopulate::exprIndices( vhExpr vh_expr, LFContext *context,
                            UtVector<vhExpr> *selExprVector, bool leafOnly )
{
  const VhObjType objType = vhGetObjType( vh_expr );
  if ( objType == VHINDNAME )
  {
    vhExpr vh_prefix = vhGetPrefix( vh_expr );
    exprIndices( vh_prefix, context, selExprVector, leafOnly );

    JaguarList indexList( vhGetExprList( vh_expr ));
    while ( vhExpr vh_index = static_cast<vhExpr>( vhGetNextItem( indexList )))
    {
      selExprVector->push_back( vh_index );
    }
  }
  else if (!leafOnly) // If leafOnly is true, don't go looking for indices in parent nodes.
  {
    if ( objType == VHSELECTEDNAME )
    {
      // For a selected name, walk the name list and process any indices found
      JaguarList selNameList( vhGetExprList( vh_expr ));
      while ( vhExpr name = static_cast<vhExpr>( vhGetNextItem( selNameList )))
      {
        if ( vhGetObjType( name ) == VHINDNAME ) {
          exprIndices( name, context, selExprVector, leafOnly );
        }
      }
    }
  }
}


Populate::ErrorCode
VhdlPopulate::exprIndices( vhExpr vh_expr, LFContext *context,
                            NUExprVector *nuExprVector )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  UtVector<vhExpr> selExprVector;
  exprIndices( vh_expr, context, &selExprVector );
  const UInt32 size = selExprVector.size();
  for ( UInt32 i = 0; i < size; ++i )
  {
    NUExpr *sel_expr;
    vhExpr vh_index = selExprVector[i];
    temp_err_code = expr( vh_index, context, 0, &sel_expr );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    sel_expr->resize( sel_expr->determineBitSize( ));
    nuExprVector->push_back( sel_expr );
  }
  return err_code;
}


UInt32
VhdlPopulate::getVhRecordFieldIndex( vhNode vh_element, vhNode vh_record, LFContext *context ) const
{
  UInt32 fieldIndex = 0;
  StringAtom *fieldAtom = context->vhdlGetIntern( vhGetString( static_cast<vhExpr>( vh_element )));
  vhNode typeDef;
  getVhdlRecordType( vh_record, &typeDef );
  RecordIterator ri( this, typeDef );
  ri.setRecurse( false );
  for ( vhNode eleNode = ri.next(); eleNode != NULL; eleNode = ri.next() )
  {
    JaguarString ele_name( vhGetRecEleName( static_cast<vhExpr>( eleNode )));
    StringAtom *eleAtom = context->vhdlGetIntern( ele_name );
    // Exit when the correct element is found
    if ( fieldAtom == eleAtom )
      break;
    ++fieldIndex;
  }

  return fieldIndex;
}

vhNode VhdlPopulate::getRecordElement(vhNode recType, UInt32 fieldIndex)
{
  vhNode typeDef;
  getVhdlRecordType( recType, &typeDef );
  RecordIterator ri( this, typeDef );
  ri.setRecurse( false );

  for ( vhNode eleNode = ri.next(); eleNode != NULL; eleNode = ri.next() )
  {
    if ( fieldIndex == 0 ) {
      return eleNode;
    }
    --fieldIndex;
  }

  return NULL;
}

Populate::ErrorCode
VhdlPopulate::selectedNameExpr( vhExpr vh_expr, LFContext *context, NUExpr **the_expr,
                                bool propagateRvalue, bool reportError )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  if ( mCarbonContext->isNewRecord( ))
  {
    bool isAliasName = isAliasObject(vh_expr);
    bool isIndexedName = false;
    bool isFunctCall = false;
    vhExpr functCallExpr = NULL;
    if (!isAliasName) {
      // See if there are any indices; all we need to know is 0 or !0.
      UtVector<vhExpr> vhExprVector;
      exprIndices( static_cast<vhExpr>( vh_expr ), context, &vhExprVector );
      if (!vhExprVector.empty()) {
        isIndexedName = true;
      } else {
        isFunctCall = isFunctionRecField(vh_expr, &functCallExpr);
      }
    }
    if (!isAliasName && !isIndexedName && !isFunctCall)
    {
      err_code = identRvalue( vh_expr, context, the_expr, propagateRvalue, reportError );
    }
    else
    {
      const SourceLocator loc = locator( vh_expr );
      NUExprArray expr_array;
      JaguarList selList( vhGetExprList( vh_expr ));
      NUExpr* subExpr = NULL;

      vhNode lastElement = NULL; // used for record scope of current element
      while ( vhNode element = vhGetNextItem( selList ))
      {
        VhObjType objType = vhGetObjType( element );
        if (objType == VHOBJECT) {
          element = vhGetActualObj(element);
          objType = vhGetObjType(element);
        }
        switch ( objType )
        {
	case VHFUNCCALL:
          temp_err_code = functionCall(functCallExpr, context, 0, &expr_array);
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            return temp_err_code;
          subExpr = expr_array[0];
	  
	  break;
        case VHINDNAME:
        {
          NUExpr *tempExpr;
          temp_err_code = expr( static_cast<vhExpr>( element ), context, 0, &tempExpr,
                                propagateRvalue, reportError );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            return temp_err_code;
          NU_ASSERT( tempExpr->getType() == NUExpr::eNUCompositeSelExpr, tempExpr );
          subExpr = tempExpr;
          break;
        }
        case VHSIGNAL:
        case VHVARIABLE:
        {
          // Non-alias signals/variables are handled above.
          if (vhIsAliasObject(element))
          {
            NUExpr *tempExpr;
            temp_err_code = expr( static_cast<vhExpr>( element ), context, 0, &tempExpr,
                                  propagateRvalue, reportError );
            if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
              return temp_err_code;
            }
            NU_ASSERT( tempExpr->getType() == NUExpr::eNUCompositeIdentRvalue, tempExpr );
            subExpr = tempExpr;
          }
          break;
        }
        case VHSIMPLENAME:
        {
          vhNode vh_rec_type = NULL;
          // Figure out which field we've got and get its index
          if ((vhGetObjType(lastElement) == VHSIGNAL) ||
              (vhGetObjType(lastElement) == VHVARIABLE)) {
            // Field of record alias case.
            vhNode subType = vhGetSubTypeInd(lastElement);
            vh_rec_type = vhGetType(subType);
          } else if (vhGetObjType(lastElement) == VHELEMENTDECL) {
            // Field of record field case.
            vhNode subType = vhGetEleSubTypeInd(lastElement);
            vh_rec_type = vhGetType(subType);
          } else {
            // Field of indexed name case.
            vh_rec_type = vhGetExpressionType( static_cast<vhExpr>( lastElement ));
          }
          UInt32 fieldIndex = getVhRecordFieldIndex( element, vh_rec_type, context );

          // Allocate the fieldExpr
          NUCompositeInterfaceExpr *composite = NULL;
          if (( subExpr->getType() == NUExpr::eNUCompositeSelExpr ) ||
	      ( subExpr->getType() == NUExpr::eNUCompositeIdentRvalue) ||
              ( subExpr->getType() == NUExpr::eNUCompositeFieldExpr ))
            composite = reinterpret_cast<NUCompositeInterfaceExpr*>( subExpr );
          else
            NU_ASSERT( false, subExpr );

	  NUExpr *cfExpr = NULL;

	  if (isFunctCall)
	  {
	    NUNet *exprNet = composite->getNet();
	    NUCompositeNet * compNet = dynamic_cast <NUCompositeNet *> (exprNet);
	    if(compNet == NULL)
	      NU_ASSERT( false, composite );
	    else
	    {
              // Figure out which field we've got and get its index.
	      UInt32 numFields = compNet->getNumFields();
	      JaguarString simpleName(vhGetSimpleName(static_cast<vhExpr> (element)));
	      for(UInt32 i = 0; i < numFields; i++)
	      {		
		NUNet *subNet = compNet->getField(i);
		JaguarString subNetName;
		
		if(subNet->isCompositeNet())
		{
		  NUCompositeNet *comNet = subNet->getCompositeNet();
		  subNetName = comNet->getOriginalName()->str();
		}
		else
		  subNetName = subNet->getName()->str();
		
                if(strcasecmp(simpleName, subNetName) == 0)
		{
		  fieldIndex = i;
                  cfExpr = new NUCompositeFieldExpr( composite, fieldIndex, loc );
		  break;
		}
	      }
	    }
	  }
	  else
	  {
            // Figure out which field we've got and get its index
            fieldIndex = getVhRecordFieldIndex( element, vh_rec_type, context );
            
            cfExpr = new NUCompositeFieldExpr( composite, fieldIndex, loc );

            // Get the size of this field from Jaguar
            vhNode eleNode = getRecordElement(vh_rec_type, fieldIndex);
            int fieldSize, numDims;
            temp_err_code = getVhExprBitSize( static_cast<vhExpr>( eleNode ), &fieldSize, &numDims, 
                                              locator( eleNode ), context, false, true );
            if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
              return temp_err_code;

            cfExpr->resize( fieldSize );

	    // Determine if the field is an integer subrange. If it is, it may need to be padded
	    NUNet *exprNet = ((NUCompositeFieldExpr*)cfExpr)->getField(fieldIndex);
	    if (exprNet->isIntegerSubrange() && !exprNet->is2DAnything() && exprNet->getBitSize() < 32)
	    {
	      cfExpr = resizeIntegerRvalue( cfExpr, exprNet->isSignedSubrange() );
	    }

            element = eleNode;
	  }
          subExpr = cfExpr;
          break;
        }
        default:
        {
          UtString buf( "Unexpected node type in selected name expression: " );
          buf << gGetVhNodeDescription( element );
          mMsgContext->JaguarConsistency( &loc, buf.c_str( ));
          return eFatal;
        }
        }
        lastElement = element;
      }
      *the_expr = subExpr;
    }
  }
  else
  {
    // If this selected name is for an array of records, as evidenced by
    // any element in the selected name being an indexed name, then we
    // need to grab that index and push it down to the bottom of the
    // record.
    NUNet *net;
    NUExprVector selExprVector;
    JaguarList selList( vhGetExprList( vh_expr ));
    while ( vhNode element = vhGetNextItem( selList ))
    {
      if ( vhGetObjType( element ) == VHINDNAME )
      {
        temp_err_code = lookupNet( context->getDeclarationScope(), vh_expr,
                                   context, &net );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
        
        temp_err_code = exprIndices( static_cast<vhExpr>( element ),
                                     context, &selExprVector );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
      }
    }
    if ( !selExprVector.empty( ))
    {
      SourceLocator loc = locator( vh_expr );
      err_code = indexedNameRvalueHelper( net, &selExprVector, loc, the_expr );
    }
    else
    {
      err_code = identRvalue( vh_expr, context, the_expr, propagateRvalue, reportError );
    }
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::selectedNameLvalue( vhNode vh_lvalue, LFContext *context,
                                  NULvalue **the_lvalue, bool isAliasDecl )
{
 ErrorCode err_code = eSuccess, temp_err_code;
  if ( mCarbonContext->isNewRecord( ))
  {
    bool isAliasName = isAliasObject(vh_lvalue);
    bool isIndexedName = false;
    if (!isAliasName) {
      // See if there are any indices; all we need to know is 0 or !0.
      UtVector<vhExpr> vhExprVector;
      exprIndices( static_cast<vhExpr>( vh_lvalue ), context, &vhExprVector );
      if (!vhExprVector.empty()) {
        isIndexedName = true;
      }
    }
    if (!isAliasName && !isIndexedName)
    {
      err_code = identLvalue( vh_lvalue, context, the_lvalue, false );
    }
    else
    {
      const SourceLocator loc = locator( vh_lvalue );
      NULvalue *subLvalue = NULL;
      JaguarList selList( vhGetExprList( static_cast<vhExpr>( vh_lvalue )));
      vhNode lastElement = NULL;
      while ( vhNode element = vhGetNextItem( selList ))
      {
        VhObjType objType = vhGetObjType( element );
        if (objType == VHOBJECT) {
          element = vhGetActualObj(element);
          objType = vhGetObjType(element);
        }
        switch ( objType )
        {
        case VHINDNAME:
        {
          NULvalue *tempLvalue;
          temp_err_code = lvalue( element, context, &tempLvalue, false );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            return temp_err_code;
          NU_ASSERT( tempLvalue->getType() == eNUCompositeSelLvalue, tempLvalue );
          subLvalue = tempLvalue;
        break;
        }
        case VHSIGNAL:
        case VHVARIABLE:
        {
          // Non-alias signals/variables are handled above.
          if (vhIsAliasObject(element))
          {
            NULvalue *tempLvalue;
            temp_err_code = lvalue( element, context, &tempLvalue, false );
            if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
              return temp_err_code;
            }
            NU_ASSERT( tempLvalue->getType() == eNUCompositeIdentLvalue, tempLvalue );
            subLvalue = tempLvalue;
          }
          break;
        }
        case VHSIMPLENAME:
        {
          vhNode vh_rec_type = NULL;
          // Figure out which field we've got and get its index
          if ((vhGetObjType(lastElement) == VHSIGNAL) ||
              (vhGetObjType(lastElement) == VHVARIABLE)) {
            // Field of record alias case.
            vhNode subType = vhGetSubTypeInd(lastElement);
            vh_rec_type = vhGetType(subType);
          } else if (vhGetObjType(lastElement) == VHELEMENTDECL) {
            // Field of record field case.
            vhNode subType = vhGetEleSubTypeInd(lastElement);
            vh_rec_type = vhGetType(subType);
          } else {
            // Field of indexed name case.
            vh_rec_type = vhGetExpressionType( static_cast<vhExpr>( lastElement ));
          }
          // Figure out which field we've got and get its index
          UInt32 fieldIndex = getVhRecordFieldIndex( element, vh_rec_type, context );

          // Allocate the fieldLvalue
          NUCompositeInterfaceLvalue *composite = dynamic_cast<NUCompositeInterfaceLvalue*>( subLvalue );
          NU_ASSERT( composite != NULL, subLvalue );
          NUCompositeFieldLvalue *cfLvalue = new NUCompositeFieldLvalue( composite, fieldIndex, loc );
          subLvalue = cfLvalue;

          element = getRecordElement(vh_rec_type, fieldIndex);
          break;
        }
        default:
        {
          UtString buf( "Unexpected node type in selected name lvalue: " );
          buf << gGetVhNodeDescription( element );
          mMsgContext->JaguarConsistency( &loc, buf.c_str( ));
          return eFatal;
        }
        }
        lastElement = element;
      }
      *the_lvalue = subLvalue;
    }
  }
  else
  {
    // If this selected name is for an array of records, as evidenced by
    // any element in the selected name being an indexed name, then we
    // need to grab that index and push it down to the bottom of the
    // record.
    NUNet *net;
    NUExprVector selExprVector;
    JaguarList selList( vhGetExprList( static_cast<vhExpr>( vh_lvalue )));
    while ( vhNode element = vhGetNextItem( selList ))
    {
      if ( vhGetObjType( element ) == VHINDNAME )
      {
        temp_err_code = lookupNet( context->getDeclarationScope(), vh_lvalue,
                                   context, &net );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;

        temp_err_code = exprIndices( static_cast<vhExpr>( element ),
                                     context, &selExprVector );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
      }
    }
    if ( !selExprVector.empty( ))
    {
      SourceLocator loc = locator( vh_lvalue );
      err_code = indexedNameLvalueHelper( net, &selExprVector, context, loc, the_lvalue );
    }
    else
    {
      err_code = identLvalue( vh_lvalue, context, the_lvalue, isAliasDecl );
    }
  }
  return err_code;
}


VhdlPopulate::JaguarIterator::JaguarIterator() :
  mVhdlPopulate( NULL ),
  mCurrentNode( NULL ),
  mNumScopesExited( 0 ),
  mIsBranch( false )
{}


VhdlPopulate::JaguarIterator::JaguarIterator( const VhdlPopulate* vhdlPop,
                                              vhNode /*node*/ ) :
  mVhdlPopulate( vhdlPop ),
  mCurrentNode( NULL ),
  mNumScopesExited( 0 ),
  mIsBranch( false )
{}


VhdlPopulate::JaguarIterator::~JaguarIterator()
{
  while ( !mVhListStack.empty( )) {
    JaguarList *deleteMe = mVhListStack.top();
    delete deleteMe;
    mVhListStack.pop();
  }
}


// Delete all but the root list, and rewind that list.  This resets us
// back to the beginning of the nested record.
void
VhdlPopulate::JaguarIterator::rewind()
{
  JaguarList *root = NULL;
  while ( !mVhListStack.empty( ))
  {
    root = mVhListStack.top();
    mVhListStack.pop();
    if ( !mVhListStack.empty( ))
    {
      JaguarList *toDelete = root;
      root = mVhListStack.top();
      delete toDelete;
      mNumScopesExited++;
    }
  }
  root->rewind();
  mVhListStack.push( root );
  mIsBranch = false;
}


VhdlPopulate::RecordIterator::RecordIterator( const VhdlPopulate *vhdlPop,
                                              vhNode recordNode )
{
  mVhdlPopulate = vhdlPop;
  JaguarList *newList = new JaguarList( vhGetRecEleList( recordNode ));
  mVhListStack.push( newList );
  mRecurse = true;
}


VhdlPopulate::AggregateIterator::AggregateIterator( vhNode aggregateNode )
{
  mVhdlPopulate = NULL;
  JaguarList *newList;
  newList = new JaguarList( vhGetEleAssocList( static_cast<vhExpr>( aggregateNode )));
  mVhListStack.push( newList );
}


VhdlPopulate::RecordIterator*
VhdlPopulate::RecordIterator::castRecordIterator()
{
  return static_cast<RecordIterator*>( this );
}


VhdlPopulate::AggregateIterator*
VhdlPopulate::AggregateIterator::castAggregateIterator()
{
  return static_cast<AggregateIterator*>( this );
}


vhNode
VhdlPopulate::RecordIterator::next()
{
  mNumScopesExited = 0;
  vhNode nextNode = vhGetNextItem( *mVhListStack.top( ));
  while ( nextNode == NULL )
  {
    // We're past the last element of this level; pop up the stack
    JaguarList *toDelete = mVhListStack.top();
    mVhListStack.pop();
    delete toDelete;
    mNumScopesExited++;
    if ( mVhListStack.empty( ))
    {
      // No more stacked elements; we're done
      mCurrentNode = NULL;
      return mCurrentNode;
    }
    nextNode = vhGetNextItem( *mVhListStack.top( ));
  }
  if ( mRecurse && mVhdlPopulate->isRecordNet( nextNode, NULL/* no context available */ ))
  {
    vhNode typeDef;
    getVhdlRecordType( nextNode, &typeDef );
    JaguarList *newList = new JaguarList( vhGetRecEleList( typeDef ));
    mVhListStack.push( newList );
    mIsBranch = true;
    return nextNode;
  }
  mCurrentNode = nextNode;
  mIsBranch = false;
  return mCurrentNode;
}


vhNode
VhdlPopulate::AggregateIterator::next()
{
  mNumScopesExited = 0;
  vhNode nextNode = vhGetNextItem( *mVhListStack.top( ));
  while ( nextNode == NULL )
  {
    // We're past the last element of this level; pop up the stack
    JaguarList *toDelete = mVhListStack.top();
    mVhListStack.pop();
    delete toDelete;
    mNumScopesExited++;
    if ( mVhListStack.empty( ))
    {
      // No more stacked elements; we're done
      mCurrentNode = NULL;
      return mCurrentNode;
    }
    nextNode = vhGetNextItem( *mVhListStack.top( ));
  }

  if ( vhGetObjType( nextNode ) == VHAGGREGATE )
  {
    JaguarList *newList;
    newList = new JaguarList( vhGetEleAssocList( static_cast<vhExpr>( nextNode )));
    mVhListStack.push( newList );
    mIsBranch = true;
    return nextNode;
  }
  if ( vhGetObjType( nextNode ) == VHOBJECT )
    nextNode = vhGetActualObj( nextNode );
  mCurrentNode = nextNode;
  mIsBranch = false;
  return mCurrentNode;
}
 

// Perform a compare of two objects of record type.  VHDL syntax tells
// us that they will always be of the same type.
Populate::ErrorCode
VhdlPopulate::recordCompare( const NUOp::OpT op, const vhExpr vh_left_expr,
                             const vhExpr vh_right_expr, LFContext *context, 
                             const SourceLocator &loc, NUExpr **the_expr )
{
  LOC_ASSERT( !mCarbonContext->isNewRecord(), loc );
  // Get the left operand of the compare.  We know from the calling
  // context that this is a record or an aggregate.
  ErrorCode err_code = eSuccess, temp_err_code = eSuccess;
  vhNode vh_expr[2];
  vhNode recIndex[2];
  recIndex[0] = recIndex[1] = NULL;
  NUExprArray exprArray[2];

  if ( vhGetObjType( vh_left_expr ) == VHOBJECT )
    vh_expr[0] = vhGetActualObj( vh_left_expr );
  else
    vh_expr[0] = vh_left_expr;

  // Get the right operand of the compare.
  if ( vhGetObjType( vh_right_expr ) == VHOBJECT )
    vh_expr[1] = vhGetActualObj( vh_right_expr );
  else
    vh_expr[1] = vh_right_expr;

  for ( UInt32 i = 0; i < 2; ++i )
  {
    // Resolve vh_expr[i] to the Jaguar node we really want to deal with
    switch ( vhGetObjType( vh_expr[i] ))
    {
    case VHSIGNAL:
    case VHVARIABLE:
    case VHCONSTANT:
    case VHAGGREGATE:
    case VHFUNCCALL:
    case VHSELECTEDNAME:
      break;
    case VHTYPECONV:
    case VHQEXPR:
      vh_expr[i] = vhGetActualObj( vhGetOperand( static_cast<vhExpr>( vh_expr[i] )));
      break;
    case VHINDNAME:
    {
      LOC_ASSERT( vhGetNoOfDim( vh_expr[i] ) == 1, loc );
      JaguarList exprList(vhGetExprList( static_cast<vhExpr>( vh_expr[i] )));
      recIndex[i] = vhGetNextItem( exprList );
      vh_expr[i] = vhGetPrefix( static_cast<vhExpr>( vh_expr[i] ));
      if ( vhGetObjType( vh_expr[i] ) != VHSELECTEDNAME )
      {
        vh_expr[i] = vhGetActualObj( vh_expr[i] );
      }
      break;
    }
    case VHSLICENAME:
      // Need to implement this
      POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unexpected l-value record array slice in record comparison" ), &err_code);
      return err_code;
      break;
    default:
      POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unexpected l-value object type in record comparison" ), &err_code);
      return err_code;
      break;
    }

    // fill exprArray[i] with the exprs to be compared
    if ( vhGetObjType( vh_expr[i] ) == VHAGGREGATE )
    {
      UtPtrArray tempArray;
      temp_err_code = recordAggregate( vh_expr[i], tempArray, context );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return err_code;

      const UInt32 tempSize = tempArray.size();
      for ( UInt32 j = 0; j < tempSize; ++j )
        exprArray[i].push_back( static_cast<NUExpr*>( tempArray[j] ));
    }
    else if ( vhGetObjType( vh_expr[i] ) == VHFUNCCALL )
    {
      temp_err_code = functionCall( static_cast<vhExpr>( vh_expr[i] ),
                                    context, 0, &exprArray[i] );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return err_code;
    }
    else // This is a full record for comparison
    {
      UtPtrArray tempArray;
      temp_err_code = expandRecord( vh_expr[i], tempArray, NULL, context );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return err_code;

      // tempArray has NUNets that need to be turned into NUExprs.
      const UInt32 tempSize = tempArray.size();
      for ( UInt32 index = 0; index < tempSize; ++index )
      {
        NUExpr *tempExpr;
        NUNet *exprNet = static_cast<NUNet*>( tempArray[ index ]);
        if ( recIndex[i] == NULL )
        {
          tempExpr = createIdentRvalue( exprNet, loc );
        }
        else
        {
          // This pushes a record array down onto the record element as a memsel
          NUExpr *bitsel_expr;
          temp_err_code = expr( static_cast<vhExpr>( recIndex[i] ),
                                context, 0, &bitsel_expr );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            return temp_err_code;
          temp_err_code = indexedNameRvalueHelper( exprNet, bitsel_expr, loc, &tempExpr );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            return temp_err_code;
        }
        exprArray[i].push_back( tempExpr );
      }
    }
  }
  
  // Walk the lists of nets corresponding to the record elements. Apply
  // the record array index if it exists.  Create a comparison of each
  // element, with all the elements ANDed or ORed together, depending on
  // if we are implementing equality or inequality.
  LOC_ASSERT( exprArray[0].size() == exprArray[1].size(), loc );
  bool firstElem = true;
  for ( unsigned int index = 0; index < exprArray[0].size(); ++index )
  {
    NUExpr *left = ( exprArray[0] )[ index ];
    NUExpr *right = ( exprArray[1] )[ index ];

    NUExpr *subExpr = new NUBinaryOp( op, left, right, loc );
    if ( firstElem ) {
      firstElem = false;
      *the_expr = subExpr;
    }
    else {
      // And the terms together for equality, or them together for inequality
      NUOp::OpT nuop = (op == NUOp::eBiEq)?NUOp::eBiLogAnd:NUOp::eBiLogOr;
      *the_expr = new NUBinaryOp( nuop, *the_expr, subExpr, loc );
    }
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::allocRecordNet( vhNode node, StringAtom *recName,
                              VhNodeArray *recConstraintArray,
                              NetFlags baseFlags, NUScope *declaration_scope,
                              LFContext *context, vhExpr vh_size, NUNet **the_net )
{
  ErrorCode err_code = eSuccess, temp_err_code;

  // Create a declaration scope for the record fields to be created in.
  // This mirrors the record net itself.
  NUNamedDeclarationScope *record_scope;
  const SourceLocator loc = locator( node );
  LOC_ASSERT( mCarbonContext->isNewRecord(), loc );
  record_scope = new NUNamedDeclarationScope( recName, declaration_scope,
                                              mIODB, context->getNetRefFactory(), loc, eHTRecord );
  context->pushDeclarationScope( record_scope );

  // Drill down in Jaguar to get the list of record elements
  vhNode typeDef;
  bool uncons_array_net_size_available = false; // is array net size available?
  if (isUnconstrainedArray(node))
  {
    // If we have the size of unconstrained array type of node, use it, instead
    // of attempting to use the initial value, which may or may not be available.
    if ( vhGetObjType( vh_size ) == VHINDEXCONSTRAINT )
    {
      JaguarList indexList( vhGetDiscRangeList( vh_size ));
      while ( vhNode idxNode = vhGetNextItem( indexList ))
        recConstraintArray->push_back( idxNode );
      uncons_array_net_size_available = true;
    }
    else if (vh_size)
    {
      recConstraintArray->push_back( vh_size );
      uncons_array_net_size_available = true;
    }
  }

  if (uncons_array_net_size_available) {
    // Don't bother looking for constraints. We know them from vh_size.
    getVhdlRecordType( node, &typeDef );
  } else {
    // Since we don't have the size, use constraints of the net. If the net is
    // unconstrained, use the initial value. If there's no initial value, error out.
    getVhdlRecordType( node, &typeDef, recConstraintArray );
  } 

  RecordIterator ri( this, typeDef );
  ri.setRecurse( false );
  NetFlags flags;
  NUNetVector fields;

  for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
  {
    UtString newName;
    getVhdlObjName( fieldElem, &newName );
    StringAtom *fieldName = context->vhdlIntern( newName.c_str( ));
    flags = NetFlags( baseFlags | eDMWireNet | eBlockLocalNet );
    calcSignedNetFlags(fieldElem, &flags, context);

    NUNet *the_field = NULL;

    temp_err_code = allocNet( fieldElem, fieldName, flags, record_scope, &the_field,
                              context, NULL );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
   
    if ( the_field )
    {
      fields.push_back( the_field );
      record_scope->addLocal( the_field );
    }
  }
  context->popDeclarationScope();

  vhString vh_typename;
  if ( vhGetObjType( node ) == VHELEMENTDECL )
    vh_typename = vhGetName( vhGetType( vhGetEleSubTypeInd( node )));
  else if ( vhGetObjType( node ) == VHTYPEDECL )
    vh_typename = vhGetName( node );
  else
    vh_typename = vhGetName( vhGetType( vhGetSubTypeInd( node )));

  const StringAtom *typeAtom = context->vhdlIntern( vh_typename );
  StringAtom *netAtom = declaration_scope->getModule()->gensym( "$recnet", recName->str() );

  // Transform the vhNodeArray of constraints into ConstantRanges
  ConstantRangeVector ranges;
  const SInt32 numDims = recConstraintArray->size();
  // This will reverse the ranges intentionally.  This puts the
  // dimensional ranges for a composite in the same order as those in a
  // memory net; that is, with dimension 0 being the smallest dimension
  // and N-1 being the outer dimension.  VhdlPopulate::allocNet() does
  // the same thing to make memory nets the way they are.
  for ( SInt32 i = numDims - 1; i >= 0; --i )
  {
    vhNode vh_constraint = (*recConstraintArray)[i];
    if (vh_constraint != NULL)
    {
      const SourceLocator locConstraint = locator( vh_constraint );
      VhdlRangeInfoVector dimVector;
      dimVector.reserve(1);
      temp_err_code = getVhExprRange( static_cast<vhExpr>(vh_constraint), dimVector, locConstraint,
                                  true, false, false, context );


      if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) {
        return temp_err_code;
      }

      LOC_ASSERT(dimVector.size() == 1, loc);
      ConstantRange cr(dimVector[0].getMsb(loc), dimVector[0].getLsb(loc));
      ranges.push_back(cr);
    }
  }

  NUCompositeNet *cNet = new NUCompositeNet( typeAtom,
                                             fields,
                                             ranges,
                                             false,
                                             declaration_scope,
                                             netAtom,
                                             loc,
                                             baseFlags);

  mPopulate->mapCompositeNet( record_scope, cNet );

  *the_net = cNet;
  return err_code;
}


template<class NodeType>
static NodeType*
concatHelper( UtVector<NodeType*> *, bool isCompositeArray, const SourceLocator & loc)
{
  UtString errMsg;
  loc.compose(&errMsg);
  errMsg << "Should never be called!";
  INFO_ASSERT( false, errMsg.c_str() );
  return NULL;
}


template<>
NUExpr*
concatHelper<NUExpr>( UtVector<NUExpr*> *elem_vector, bool isCompositeArray,
                      const SourceLocator &loc )
{
  NUCompositeExpr *the_retval = new NUCompositeExpr( *elem_vector, loc );
  the_retval->resize( the_retval->determineBitSize( ));
  the_retval->setIsCompositeArray(isCompositeArray);
  return the_retval;
}


template<>
NULvalue*
concatHelper<NULvalue>( UtVector<NULvalue*> *elem_vector, bool isCompositeArray,
                        const SourceLocator &loc )
{
  NUCompositeLvalue *the_retval = new NUCompositeLvalue( *elem_vector, loc );
  the_retval->resize();
  the_retval->setIsCompositeArray(isCompositeArray);
  return the_retval;
}


template <>
Populate::ErrorCode
VhdlPopulate::exprOrLvalue<NULvalue>( vhExpr elem, LFContext *context, NULvalue **nuFieldNode, int /*lWidth*/)
{
  return lvalue( elem, context, nuFieldNode );
}


template <>
Populate::ErrorCode
VhdlPopulate::exprOrLvalue<NUExpr>( vhExpr elem, LFContext *context, NUExpr **nuFieldNode, int lWidth)
{
  ErrorCode errCode = eSuccess;

  ErrorCode tempErrCode = expr( elem, context, lWidth, nuFieldNode );

  if (errorCodeSaysReturnNowWhenFailPopulate(tempErrCode, &errCode)) {
    return tempErrCode;
  }

  // Making sure that the element being created here has the same size as its
  // corresponding field in the record. If a difference in size is found, apply
  // a part-select to make it the same size. 
  // This situation is common in the case that one of the fields of a record is
  // a ranged integer subtype.
  // For example, in the following type declaration:
  // type rec_t is record
  //   f1 : integer range 0 to 3;
  //   f2 : integer range 4 to 7;
  // end rec_t;
  // When a signal of this type is assigned using an aggregate, cbuild will
  // resize all ranged integers to 32 bits on the RHS of the assignment, but
  // to make them the same size as their corresponding field on the LHS, these
  // RHS expressions need to be truncated back to their original size.
  // See bug 12542 for more details.
  if ((unsigned)lWidth != (*nuFieldNode)->getBitSize()) {
    *nuFieldNode = (*nuFieldNode)->makeSizeExplicit(lWidth);
  }

  return tempErrCode;
}


// Populate a record aggregate, dealing with named association in all
// its funky glory if necessary.
template <class NodeType>
Populate::ErrorCode
VhdlPopulate::recordAggregate( vhNode aggregate, 
                               LFContext *context, NodeType **the_retval )
{
  const SourceLocator &loc = locator( aggregate );
  LOC_ASSERT( mCarbonContext->isNewRecord(), loc );

  ErrorCode err_code = eSuccess, temp_err_code;
  // First we need to size the UtPtrArray correctly to deal with element
  // associations and others handling
  vhNode typeDef;
  VhNodeArray recConstraintArray;
  getVhdlRecordType(aggregate, &typeDef, &recConstraintArray);

  bool isArrayAggregate = false;
  ConstantRange elemRange;
  if (!recConstraintArray.empty()) // Record array aggregate
  {
    // Will always calling this with 'false' in the lvalue indication
    // trip us up?  Probably not (if at all) until we're doing array
    // stuff too.
    vhNode vh_constraint = recConstraintArray[0];
    VhdlRangeInfoVector dimVector;
    temp_err_code = getVhExprRange(static_cast<vhExpr>(vh_constraint), dimVector,
                                   loc, /*isIndexConstraint*/true, /*isLvalue*/false, true, context);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    // Set the range to the top most dimension of aggregate.
    elemRange.setLsb(dimVector[0].getLsb(loc));
    elemRange.setMsb(dimVector[0].getMsb(loc));
    isArrayAggregate = true;
  }
  else // Record aggregate
  {
    JaguarList recEleList(vhGetRecEleList(typeDef));
    // Field index range is set to numFields-1 downto 0.
    elemRange.setMsb(recEleList.size() - 1);
    elemRange.setLsb(0);
  }

  // Create a concat of aggregates for a record or single/multi dimensional 
  // array of records.
  err_code = createAggregateConcat<NodeType>(aggregate, elemRange, isArrayAggregate,
                                             loc, typeDef, context, the_retval);

  return err_code;
}



// Explicit instantiation declarations that we know we need.  These
// allow us to link successfully.  These methods are actually called in
// other files.
template
Populate::ErrorCode
VhdlPopulate::recordAggregate<NUExpr>( vhNode aggregate, 
                                       LFContext *context, NUExpr **the_retval );

template
Populate::ErrorCode
VhdlPopulate::recordAggregate<NULvalue>( vhNode aggregate, 
                                         LFContext *context, NULvalue **the_retval );


template <class NodeType>
Populate::ErrorCode
VhdlPopulate::createAggregateConcat(vhNode aggregate, ConstantRange& elemRange,
                                    bool isArrayAggregate, const SourceLocator& loc,
                                    vhNode typeDef, LFContext *context,
                                    NodeType **the_retval)
{
  ErrorCode errCode = eSuccess;

  // Fill all array expressions with NULL to help deal with "others" VHDL choice.
  UInt32 numElems = elemRange.getLength();
  UtVector<NodeType*> nodeVec;
  nodeVec.resize(numElems);
  for (UInt32 i = 0; i < numElems; ++i)
    nodeVec[i] = NULL;

  ErrorCode temp_err_code;
  UInt32 position = 0;
  JaguarList aggList(vhGetEleAssocList(static_cast<vhExpr>(aggregate)));

  // We need the rec element types to find out the types of aggregate elements.
  JaguarList vh_elem_list(vhGetRecEleList(typeDef));

  while (vhExpr aggElem = static_cast<vhExpr>(vhGetNextItem(aggList)))
  {
    // Walk through the record fields or record array elements.  Find the
    // correct aggregate element to match it, make a nucleus node for it, and
    // stuff it in the node vector.
    vhObjType elemObjType = vhGetObjType(aggElem);
    if (elemObjType == VHELEMENTASS) // named association
    {
      // An element association has a list of choices and
      // an expression for those choices. For example: ( 1 | 3 | (4 to 7) => x),
      // where choices are 1, 3, 4, 5, 6, 7 and expression is x.
      vhExpr expr_node = vhGetExpr(aggElem);
      JaguarList choiceList(vhGetChoiceList((static_cast<vhExpr>(aggElem))));

      while (vhExpr choice = static_cast<vhExpr>(vhGetNextItem(choiceList)))
      {
        // For the given choice, get the list of positions and widths in concat vector nodeVec.
        AggElem elem_list;
        temp_err_code = getElementPositions<NodeType>(choice, typeDef, isArrayAggregate,
                                                      loc, nodeVec, context, elemRange, 
                                                      elem_list);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &errCode)) {
          return temp_err_code;
        }

        // For every position in the list, create an expression and put it in
        // concat vector at those positions.
        temp_err_code = createElementExprOrLvalue(expr_node, context, elem_list, nodeVec);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &errCode)) {
          return temp_err_code;
        }
      } // while
    } // if
    else // positional association
    {
      // We are getting the size (value)  of each element of aggregate from the 
      // rec element types.
      // Then createElementExprOrLvalue uses it to create right size of the expr.
      int value = 0;
      int dims  = 0;
      if(isArrayAggregate == false)
      {
        vhNode vh_recElem = vhGetNextItem(vh_elem_list);
        if(vhGetObjType(vh_recElem) == VHELEMENTDECL)
        {
          vhNode vh_type = vhGetEleSubTypeInd(vh_recElem);
          ErrorCode temp_err_code = getVhExprBitSize(static_cast<vhExpr> (vh_type), &value, &dims, loc, context );
          if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &errCode ) )
            return temp_err_code;
        }
      }

      // Create an expression and put it in the next concat vector position.
      AggElem elem_list;
      aggr_elem elem;
      elem.position = position++;
      elem.width = value;
      elem_list.push_back(elem);
      temp_err_code = createElementExprOrLvalue<NodeType>(aggElem, context, elem_list, nodeVec);
      if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &errCode)) {
        return temp_err_code;
      }
    }
  } // while

  // All fields should be available here. Jaguar checks for this. So we shouldn't
  // get this assert unless there's some bug.
  UInt32 vecSize = nodeVec.size();
  for (UInt32 i = 0; i < vecSize; ++i) {
    if (nodeVec[i] == NULL) {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Mismatch between aggregate rvalue and lvalue."), &errCode);
      return errCode;
    }
  }
  
  // Mark the concat composite to be of array type to help resynthesis.
  *the_retval = concatHelper<NodeType>(&nodeVec, isArrayAggregate, loc);

  return eSuccess;
}

template <class NodeType>
Populate::ErrorCode
VhdlPopulate::createElementExprOrLvalue(vhExpr expr_node, LFContext* context,
                                        AggElem& elem_list,UtVector<NodeType*>& nodeVec)
{
  ErrorCode errCode = eSuccess;
  for (Loop< UtList<aggr_elem> > elem(elem_list); !elem.atEnd(); ++elem)
  {
    // Multiple aggregates for same position is already checked by Jaguar.
    // So we shouldn't get this assert unless there's some bug.
    int pos = (*elem).position;
    int width = (*elem).width;
    if (nodeVec[pos] != NULL) {
      const SourceLocator loc = locator(expr_node);
      UtString errMsg;
      errMsg << "Multiple aggregates found for same element position : "
             << pos << " in aggregate.";
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, errMsg.c_str()), &errCode);
      return errCode;
    }

    NodeType *nuFieldNode = NULL;
    ErrorCode tempErrCode = exprOrLvalue(expr_node, context, &nuFieldNode, width);// lWidth);
    if (errorCodeSaysReturnNowWhenFailPopulate(tempErrCode, &errCode)) {
      return tempErrCode;
    }
    nodeVec[pos] = nuFieldNode; // place the nucleus in the array
  }
  return errCode;
}

static UInt32 sNormalizeIndex(UInt32 idx, ConstantRange& range)
{
  if (range.getMsb() <= range.getLsb()) // Range like: x to y
    return (idx - range.getMsb());
  return (range.getMsb() - idx); // Range like: x downto y
}

template <class NodeType>
Populate::ErrorCode
VhdlPopulate::getElementPositions(vhExpr choice, vhNode typeDef,
                                  bool isArrayAggregate, const SourceLocator& loc,
                                  UtVector<NodeType*>& nodeVec,
                                  LFContext* context, ConstantRange& elemRange,
                                  AggElem& elem_list)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  vhObjType objType = vhGetObjType(choice);
  switch (objType)
  {
  case VHSIMPLENAME:
  {
    if (isArrayAggregate) {
      UtString buf;
      buf << "Unexpected name choice " << gGetVhNodeDescription(choice)
          << " in array aggregate";
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, buf.c_str()), &err_code);
      return err_code;
    }
    JaguarString choiceName(vhGetSimpleName(choice));  // This choice name
    JaguarList recEleList(vhGetRecEleList(typeDef));
    UInt32 pos = 0;
    vhNode recField;
    while (elem_list.empty() && (recField = vhGetNextItem(recEleList)))
    {
      // The name of this record field that we might match
      JaguarString fieldName(vhGetRecEleName(recField));
      if (strcmp(fieldName, choiceName) == 0) 
      {
        int width = 0;
	int dims  = 0;
        // We need to calculate the width of the recField to be used for LHS size.
        // For example, in the bug11631 the record field is initialized with function call.
        // Sometimes, we fail to calculate the size of return from the function body and
        // use the LHS size for it.
        temp_err_code = getVhExprBitSize(static_cast<vhExpr> (recField), &width, &dims, loc, context );
        if(errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
          return err_code;
        
        aggr_elem elem; 
        elem.position = pos;
        elem.width = width;
        elem_list.push_back(elem);
      }
      pos++;
    }

    if (elem_list.empty()) {
      UtString buf;
      buf << "Name " << choiceName << " does not match any record field";
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, buf.c_str()), &err_code);
      return err_code;
    }
  }
  break;
  case VHOTHERS:
  {
    UInt32 vecSize = nodeVec.size();
    for (UInt32 i = 0; i < vecSize; ++i) {
      if (nodeVec[i] == NULL) {
        aggr_elem elem; 
        elem.position = i;
        elem.width = 0;
        elem_list.push_back(elem);
      }
    }
    // Jaguar should catch this. So we shouldn't get this assert unless
    // there's some bug.
    if (elem_list.empty()) {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Could not find unspecified elements for OTHERS choice"), &err_code);
      return err_code;
    }
  }
  break;
  case VHRANGE:
  {
    VhdlRangeInfoVector dimVector;
    getVhExprRange(choice, dimVector, loc, false, false, true, context);
    // A VHRANGE should always return one range.
    if (dimVector.size() != 1) {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Incorrect range specification found"), &err_code);
      return err_code;
    }
    VhdlRangeInfo& ri = dimVector.back();
    UInt32 first = std::min(ri.getLsb(loc), ri.getMsb(loc));
    UInt32 last  = std::max(ri.getLsb(loc), ri.getMsb(loc));
    for (UInt32 i = first; i <= last; ++i) {
      UInt32 pos = sNormalizeIndex(i, elemRange);
      aggr_elem elem; 
      elem.position = pos;
      elem.width = 0;
      elem_list.push_back(elem);
    }
  }
  break;
  default:
  {
    // Since this choice isn't others, range or simple name, it's an expression
    // that computes to the array index.
    if (!isArrayAggregate) {
      UtString buf;
      buf << "Unexpected array index choice " << gGetVhNodeDescription(choice)
          << " in record aggregate";
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, buf.c_str()), &err_code);
      return err_code;
    }
    SInt32 value;
    ErrorCode tempErrCode = evaluateStaticExpr(choice, &value, context, true);
    if (errorCodeSaysReturnNowWhenFailPopulate(tempErrCode, NULL)) {
      return tempErrCode;
    }
    // This should never happen.
    if (value < 0) {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Negative composite aggregate index."), &err_code);
      return err_code;
    }
    UInt32 pos = sNormalizeIndex((UInt32)value, elemRange);
    aggr_elem elem; 
    elem.position = pos;
    elem.width = 0;
    elem_list.push_back(elem);
  }
  break;
  } // switch

  return err_code;
}

Populate::ErrorCode
VhdlPopulate::fieldRefLvalue( vhNode field_ref, LFContext *context,
                              NUCompositeFieldLvalue **the_lvalue, bool /*isAliasDecl*/ )
{
  const SourceLocator &loc = locator( field_ref );
  LOC_ASSERT( mCarbonContext->isNewRecord(), loc );
  *the_lvalue = NULL;

  JaguarList selList(vhGetExprList( static_cast<vhExpr>( field_ref )));
  vhNode baseElem = vhGetNextItem( selList );
  LOC_ASSERT( vhGetObjType( baseElem ) == VHINDNAME, loc );

  // Create a vector of index expressions
  ErrorCode err_code = eSuccess, temp_err_code;
  NUExprVector indexVector;
  JaguarList indexList( vhGetExprList( static_cast<vhExpr>( baseElem )));
  while ( vhExpr vh_index = static_cast<vhExpr>( vhGetNextItem( indexList )))
  {
    NUExpr *index;
    temp_err_code = expr( vh_index, context, 0, &index );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    index->resize( index->determineBitSize( ));
    indexVector.push_back( index );
  }

  // Get the base compositeNet
  NUNet *net = NULL;
  vhNode vh_base_net = vhGetPrefix( static_cast<vhExpr>( baseElem ));
  temp_err_code = lookupNet( context->getDeclarationScope(), vh_base_net, context, &net );
  NUCompositeNet *cNet = net->getCompositeNet();
  NU_ASSERT( cNet != NULL, net );

  // Create a composite selected lvalue from the net and the vector
  NUCompositeIdentLvalue *ident = new NUCompositeIdentLvalue( cNet, loc );
  NUCompositeSelLvalue *csLval = new NUCompositeSelLvalue( ident, &indexVector, loc );
  NUCompositeSelLvalue *intermediateLvalue = csLval; // either selLvalue or fieldLvalue(not yet supported)

  while ( vhNode fieldElem = vhGetNextItem( selList ))
  {
    if ( vhGetObjType( fieldElem ) == VHSIMPLENAME )
    {
      UtString fieldName;
      getVhdlObjName( fieldElem, &fieldName );
      vhNode vh_typedef;
      (void)getVhdlRecordType( vh_base_net, &vh_typedef, NULL );
      JaguarList recElems ( vhGetRecEleList( vh_typedef ));
      UInt32 fieldIdx = 0;
      while( vhNode elem = vhGetNextItem( recElems ))
      {
        vhString eleName = vhGetRecEleName( elem );
        if ( !strcasecmp( eleName, fieldName.c_str( )))
        {
          break;
        }
        fieldIdx++;
      }
      if ( fieldIdx >= recElems.size( ))
      {
        UtString errMsg;
        errMsg << "Unable to locate record element " << fieldName << " in record "
               << vhGetName( vh_base_net );
        POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, errMsg.c_str( )), &err_code);
        delete intermediateLvalue;
        return err_code;
      }
      *the_lvalue = new NUCompositeFieldLvalue( intermediateLvalue, fieldIdx, loc );
      // I can't handle rec(i).f1.f2 yet, so I'm bailing at rec(i).f1...
      if ( vhGetNextItem( selList ) != NULL )
      {
        POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Cannot currently populate this record lvalue" ), &err_code);
        delete *the_lvalue;
        return err_code;
      }
    }
    else if ( vhGetObjType( fieldElem ) == VHINDNAME )
    {
      vhDecompileNode( fieldElem );
      LOC_ASSERT( false, loc );
    }
  }

  return err_code;
}

bool 
VhdlPopulate::isFunctionRecField( vhExpr vh_expr, vhExpr *funcCallexpr)
{
  bool functCall = false;

  vhObjType vh_ObjType = vhGetObjType( vh_expr);
  if(vh_ObjType == VHFUNCCALL)
  {
    functCall = true;
    *funcCallexpr = vh_expr;
  }
  else if(vh_ObjType == VHSELECTEDNAME)
  {
    JaguarList selListTemp( vhGetExprList( vh_expr ));
    vhExpr exprPrefix = static_cast<vhExpr>( vhGetNextItem( selListTemp ));
    vhObjType exprType = vhGetObjType( exprPrefix );

    if(exprType == VHINDNAME || exprType == VHSLICENAME)
      exprPrefix = vhGetPrefix(exprPrefix); 

    if ( vhGetObjType( exprPrefix ) == VHFUNCCALL )
    {
      functCall = true;
      *funcCallexpr = exprPrefix;
    }
  }

  return functCall;
}
