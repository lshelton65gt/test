// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Class implementation to bit-blast continuous assigns if there is a
  self-referential cycle in the assign or if there are constants in
  the assign that are partially z (partially driven constants or mixed
  constants). 
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NULvalue.h"

#include "reduce/Fold.h"

#include "util/Loop.h"
#include "util/CbuildMsgContext.h"
#include "util/UtHashSet.h"

#include "BreakNetCycles.h"

LFBreakNetCycles::LFBreakNetCycles(NUNetRefFactory* netRefFactory,
                                   AtomicCache* strCache, Fold* fold,
                                   bool verbose,
                                   LFSplitAssigns* splitAssigns) :
  mDeletedAssigns(new NUUseDefSet), mNewAssigns(new NUContAssignList),
  mVerbose(verbose), mNetRefFactory(netRefFactory), mStrCache(strCache),
  mFold(fold), mCopyContext(new CopyContext(0, 0)), mSplitAssigns(splitAssigns)
{}

LFBreakNetCycles::~LFBreakNetCycles()
{
  INFO_ASSERT(mDeletedAssigns->empty(), "deleted assigns cleaned up");
  delete mDeletedAssigns;
  INFO_ASSERT(mNewAssigns->empty(), "new assigns cleaned up");
  delete mNewAssigns;
  delete mCopyContext;
}


bool LFBreakNetCycles::module(NUModule* module)
{
  // Process all the continous assignments in the module
  bool workDone = false;
  NUModule::ContAssignLoop l;
  for (l = module->loopContAssigns(); !l.atEnd(); ++l) {
    NUContAssign* assign = *l;
    workDone |= optimizeAssign(module, assign);
  }

  // If we modified some continous assigns then replace the cont
  // assign list. We go through the list again and prune the deleted
  // assigns. We then add the new assigns.
  if (workDone) {
    // Prune the deleted assigns
    NUContAssignList newAssigns;
    for (l = module->loopContAssigns(); !l.atEnd(); ++l) {
      NUContAssign* assign = *l;
      if (mDeletedAssigns->find(assign) == mDeletedAssigns->end()) {
        // This is still a valid assign
        newAssigns.push_back(assign);
      } else {
        // We no longer need this assign
        delete assign;
      }
    }

    // Add the new assigns
    for (NUModule::ContAssignLoop l(*mNewAssigns); !l.atEnd(); ++l) {
      NUContAssign* assign = *l;
      newAssigns.push_back(assign);
    }

    // Update the module and clear out data
    module->replaceContAssigns(newAssigns);
    mDeletedAssigns->clear();
    mNewAssigns->clear();
  } // if
  return workDone;
} // bool LFBreakNetCycles::module

class BitNetRefCallback : public NUDesignCallback
{
public:
  //! constructor
  BitNetRefCallback(NUNetRefFactory* netRefFactory);

  //! Get whether we hit an invalid operator or not
  bool invalidOperatorFound() const { return mInvalidOperator; }

  //! Return whether or not we found a partially driven constant
  bool hasMixedConstant() const { return mHasMixedConstant; }

  //! Get the net ref set for a given bit
  const NUNetRefSet& getBitNetRefSet(UInt32 i);

  //! Catchall for things we don't support
  virtual Status operator()(Phase, NUBase*);

  //! Compute the bit net refs for an identity
  virtual Status operator()(Phase phase, NUIdentRvalue* ident);

  //! Compute the bit net refs for a part select
  virtual Status operator()(Phase phase, NUVarselRvalue* partsel);

  //! Compute the bit net refs for a constant (empty)
  virtual Status operator()(Phase phase, NUConst* con);

  //! Compute the bit net refs for an op
  virtual Status operator()(Phase phase, NUOp* op);

  //! Compute the bit net refs for an identity lvlalue
  virtual Status operator()(Phase phase, NUIdentLvalue* ident);

  //! Compute the bit net refs for a part select lvlalue
  virtual Status operator()(Phase phase, NUVarselLvalue* partsel);

  //! Compute the bit net refs for a concat lvalue
  virtual Status operator()(Phase phase, NUConcatLvalue* concat);

private:
  //! Hide copy and assign constructors.
  BitNetRefCallback(const BitNetRefCallback&);
  BitNetRefCallback& operator=(const BitNetRefCallback&);

  // Types to keep track of bit based defs and uses
  typedef UtVector<NUNetRefSet> BitsNetRefSet;
  typedef Loop<BitsNetRefSet> BitsNetRefSetLoop;
  typedef UtVector<BitsNetRefSet> BitsNetRefSetStack;
  BitsNetRefSetStack mBitsNetRefSetStack;
  void createBitsNetRefSet(const NUNetRefHdl& netRef, UInt32 bitSize);
  void combineAllNetRefs(NUNetRefSet* allBits);
  void pushBitsNetRefSet(BitsNetRefSet& bitsNetRefSet);
  void popBitsNetRefSet(BitsNetRefSet* bitsNetRefSet);

  void resizeAndInit(BitsNetRefSet & bitsNetRefSet, UInt32 size);

  // If set, we found an operator we cannot handle
  bool mInvalidOperator;

  // If set, we found a mixed constant (partially driven)
  bool mHasMixedConstant;

  // Utility classes
  NUNetRefFactory* mNetRefFactory;
}; // class BitNetRefCallback : public NUDesignCallback

bool LFBreakNetCycles::optimizeAssign(NUModule* module, NUAssign* assign)
{
  // Check if there is overlap between the defs and uses. Note that we
  // assume that the non-blocking to blocking transformation has
  // already happened.
  NUNetRefSet defNetRefs(mNetRefFactory);
  NUNetRefSet useNetRefs(mNetRefFactory);

  if (assign->useBlockingMethods())
  {
    assign->getBlockingDefs(&defNetRefs);
    assign->getNonBlockingDefs(&defNetRefs);
    assign->getBlockingUses(&useNetRefs);
    assign->getNonBlockingUses(&useNetRefs);
  }
  else
  {
    assign->getDefs(&defNetRefs);
    assign->getUses(&useNetRefs);
  }

  bool selfReferential = NUNetRefSet::set_has_intersection(defNetRefs, useNetRefs);
  
  // Compute the bit level uses
  BitNetRefCallback useCallback(mNetRefFactory);
  NUDesignWalker useWalker(useCallback, false);
  NUExpr* rvalue = assign->getRvalue();
  useWalker.expr(rvalue);
  if (useCallback.invalidOperatorFound()) {
    return false;
  }

  // Compute the bit level defs
  BitNetRefCallback defCallback(mNetRefFactory);
  NUDesignWalker defWalker(defCallback, false);
  NULvalue* lvalue = assign->getLvalue();
  defWalker.lvalue(lvalue);
  if (defCallback.invalidOperatorFound()) {
    return false;
  }

  // check if we encountered a partially driven constant.
  bool hasMixedConst = defCallback.hasMixedConstant();
  hasMixedConst |= useCallback.hasMixedConstant();

  // Check if none of the bits overlap. If so, we can break this up to
  // break the self referential cycle.
  bool overlap = false;
  UInt32 size = lvalue->getBitSize();

  if (selfReferential)
  {
    for (UInt32 i = 0; (i < size) && !overlap; ++i) {
      const NUNetRefSet& bitDefNetRefSet = defCallback.getBitNetRefSet(i);
      const NUNetRefSet& bitUseNetRefSet = useCallback.getBitNetRefSet(i);
      if (NUNetRefSet::set_has_intersection(bitDefNetRefSet, bitUseNetRefSet)) {
        overlap = true;
      }
    }
  }
  
  if (!hasMixedConst) {
    if (! selfReferential || overlap) {
      return false;
    }
  }
  
  // Create the new atomized assignment statements and keep track of
  // the old/new data for fixups and printing.
  const SourceLocator& loc = assign->getLoc();
  for (UInt32 i = 0; (i < size); ++i) {
    NUContAssign* newAssign = createSubAssign(module, lvalue, rvalue, loc, i, i);
    if (newAssign != NULL) {    // suppress assigns to 1'bz
      mNewAssigns->push_back(newAssign);
    }
  }
  mDeletedAssigns->insert(assign);
  if (mVerbose) {
    LFSplitAssigns::Flags flags = LFSplitAssigns::eNone;
    if (selfReferential && ! overlap)
      flags = LFSplitAssigns::eCycle;
    if (hasMixedConst)
      LFSplitAssigns::sSetFlag(&flags, LFSplitAssigns::eMixedConst);
    mSplitAssigns->add(assign->getLoc(), flags);
  }
  return true;
} // bool LFBreakNetCycles::optimizeAssign

BitNetRefCallback::BitNetRefCallback(NUNetRefFactory* netRefFactory) :
  mInvalidOperator(false), mHasMixedConstant(false), mNetRefFactory(netRefFactory)
{}

const NUNetRefSet& BitNetRefCallback::getBitNetRefSet(UInt32 i)
{
  INFO_ASSERT(mBitsNetRefSetStack.size() == 1, "stack needs to be one");
  BitsNetRefSet& bitsNetRefSet = mBitsNetRefSetStack.back();
  if (i >= bitsNetRefSet.size()) {
    resizeAndInit(bitsNetRefSet, i+1);
  }
  return bitsNetRefSet[i];
}

BitNetRefCallback::Status BitNetRefCallback::operator()(Phase, NUBase*)
{
  mInvalidOperator = true;
  return NUDesignCallback::eStop;
}

BitNetRefCallback::Status
BitNetRefCallback::operator()(Phase phase, NUIdentRvalue* ident)
{
  // Only do work in the post phase
  if (phase != ePost)
    return eNormal;

  // We only support vector or scalar nets
  NUNet* net = ident->getIdent();
  if (!net->isBitNet() && !net->isVectorNet()) {
    mInvalidOperator = true;
    return NUDesignCallback::eStop;
  }

  // Create a bits net ref set for the entire expression and push it
  // on the stack.
  NUNetRefHdl netRef = mNetRefFactory->createNetRef(net);
  createBitsNetRefSet(netRef, ident->getBitSize());
  return eNormal;
} // BitNetRefCallback::operator

BitNetRefCallback::Status
BitNetRefCallback::operator()(Phase phase, NUVarselRvalue* bitsel)
{
  // Only do work in the pre phase, and don't visit index expression
  if (phase != ePre)
    return eSkip;

  // We don't support anything but selects of nets with a constant
  // index
  NUExpr* ident = bitsel->getIdentExpr();
  if (!ident->isWholeIdentifier() || !bitsel->isConstIndex()) {
    mInvalidOperator = true;
    return eStop;
  }

  // Create a bits net ref set for the entire expression and push it
  // on the stack.
  NUNet* net = bitsel->getIdent();
  const ConstantRange &range (*bitsel->getRange ());

  NUNetRefHdl netRef = mNetRefFactory->createVectorNetRef(net, range);

  // If the index is out-of-range (may occur with -nofold, see bug 3472)
  // then reject this as unsupported.
  if (!netRef.isValid() || (*netRef).empty())
  {
    mInvalidOperator = true;
    return eStop;
  }

  NU_ASSERT (range.getLength () <= 65536, bitsel);
  createBitsNetRefSet(netRef, range.getLength ());

  // We don't want to keep visiting since we don't want to visit the
  // index (it's a constant anyway...).
  return eSkip;
} // BitNetRefCallback::operator

BitNetRefCallback::Status
BitNetRefCallback::operator()(Phase phase, NUConst* constExpr)
{
  // Only do work in the post phase
  if (phase != ePost)
    return eNormal;

  mHasMixedConstant |= constExpr->isPartiallyDriven();
  
  // Just create a vector of empty net ref sets
  BitsNetRefSet bitsNetRefSet;
  resizeAndInit(bitsNetRefSet,constExpr->getBitSize());
  pushBitsNetRefSet(bitsNetRefSet);
  return eNormal;
} // BitNetRefCallback::operator

BitNetRefCallback::Status
BitNetRefCallback::operator()(Phase phase, NUOp* op)
{
  // Only do work in the post phase so that we do a depth first search
  if (phase != ePost)
    return eNormal;

  // Process this operator
  switch (op->getOp()) {
    case NUOp::eUnBuf:
    case NUOp::eUnLogNot:
    case NUOp::eUnBitNeg:
    case NUOp::eUnVhdlNot:
    case NUOp::eUnPlus:
    case NUOp::eUnMinus:
    case NUOp::eUnAbs:
    case NUOp::eUnItoR:
    case NUOp::eUnRtoI:
    case NUOp::eUnRealtoBits:
    case NUOp::eUnBitstoReal:
      // Just pass the same net ref sets on so do nothing
      break;

    case NUOp::eUnRedAnd:
    case NUOp::eUnRedOr:
    case NUOp::eUnRedXor:
    case NUOp::eUnCount:
    case NUOp::eUnFFZ:
    case NUOp::eUnFFO:
    case NUOp::eUnFLO:
    case NUOp::eUnFLZ:
    case NUOp::eUnRound:
    case NUOp::eUnChange:
    {
      // Combine all the net ref sets from the input argument into one
      // net ref set
      NUNetRefSet allBits(mNetRefFactory);
      combineAllNetRefs(&allBits);

      // Create a vector of one net ref
      BitsNetRefSet allBitsNetRefSet;
      resizeAndInit(allBitsNetRefSet,op->getBitSize());
      allBitsNetRefSet[0] = allBits;

      // Push the result on the stack
      pushBitsNetRefSet(allBitsNetRefSet);
      break;
    }

    case NUOp::eBiEq:
    case NUOp::eBiNeq:
    case NUOp::eBiTrieq:
    case NUOp::eBiTrineq:
    case NUOp::eBiLogAnd:
    case NUOp::eBiLogOr:
    case NUOp::eBiSLt:
    case NUOp::eBiSLte:
    case NUOp::eBiSGtr:
    case NUOp::eBiSGtre:
    case NUOp::eBiULt:
    case NUOp::eBiULte:
    case NUOp::eBiUGtr:
    case NUOp::eBiUGtre:
    {
      // Combine all the net ref sets from both arguments into one net
      // ref set
      NUNetRefSet allBits(mNetRefFactory);
      combineAllNetRefs(&allBits);
      combineAllNetRefs(&allBits);

      // Create a vector of one net ref
      BitsNetRefSet allBitsNetRefSet;
      resizeAndInit(allBitsNetRefSet,op->getBitSize());
      allBitsNetRefSet[0] = allBits;

      // Push the result on the stack
      pushBitsNetRefSet(allBitsNetRefSet);
      break;
    }

    case NUOp::eBiBitAnd:
    case NUOp::eBiBitOr:
    case NUOp::eBiBitXor:
    {
      // Gather the net refs for both legs and resize them so that we
      // can do an easy transfer.
      BitsNetRefSet bitsNetRefSet1;
      popBitsNetRefSet(&bitsNetRefSet1);
      BitsNetRefSet bitsNetRefSet2;
      popBitsNetRefSet(&bitsNetRefSet2);
      resizeAndInit(bitsNetRefSet1,op->getBitSize());
      resizeAndInit(bitsNetRefSet2,op->getBitSize());
      BitsNetRefSet combBitsNetRefSet;
      resizeAndInit(combBitsNetRefSet,op->getBitSize());

      // Combine the net refs by bit
      for (UInt32 i = 0; i < op->getBitSize(); ++i) {
        const NUNetRefSet& bitNetRefSet1 = bitsNetRefSet1[i];
        const NUNetRefSet& bitNetRefSet2 = bitsNetRefSet2[i];
        NUNetRefSet& combBitNetRefSet = combBitsNetRefSet[i];
        combBitNetRefSet.insert(bitNetRefSet1.begin(), bitNetRefSet1.end());
        combBitNetRefSet.insert(bitNetRefSet2.begin(), bitNetRefSet2.end());
      }

      // Push the resulon the the stack
      pushBitsNetRefSet(combBitsNetRefSet);
      break;
    } // case NUOp::eBiBitXor:


    case NUOp::eNaConcat:
    {
      // Gather the parts of the concat op. Note that we can have a
      // repeat count so populate a temporary vector
      BitsNetRefSet argsBitsNetRefSet;
      UInt32 narg = op->getNumArgs();
      for (SInt32 i = narg - 1; i >= 0 ; --i) {
        BitsNetRefSet bitsNetRefSet;
        popBitsNetRefSet(&bitsNetRefSet);
        for (UInt32 j = 0; j < bitsNetRefSet.size(); ++j) {
          const NUNetRefSet& bitNetRefSet = bitsNetRefSet[j];
          argsBitsNetRefSet.push_back(bitNetRefSet);
        }
      }

      // Now create the final by repeatedly copying the bit net refs
      NUConcatOp* concat = dynamic_cast<NUConcatOp*>(op);
      BitsNetRefSet concatBitsNetRefSet;
      for (UInt32 i = 0; i < concat->getRepeatCount(); ++i) {
        // Copy the args from back to front to put them in the correct
        // order
        for (BitsNetRefSetLoop l(argsBitsNetRefSet); !l.atEnd(); ++l) {
          const NUNetRefSet& netRefSet = *l;
          concatBitsNetRefSet.push_back(netRefSet);
        }
      }

      // Resize the concat in case we need to truncate or extend
      // it. Then we can finally push the result.
      resizeAndInit(concatBitsNetRefSet,op->getBitSize());
      pushBitsNetRefSet(concatBitsNetRefSet);
      break;
    }

    case NUOp::eTeCond:
    {
      // Gather the net refs for both legs and resize them so that we
      // can do an easy transfer.
      BitsNetRefSet bitsNetRefSetSelect;
      popBitsNetRefSet(&bitsNetRefSetSelect);
      BitsNetRefSet bitsNetRefSetThen;
      popBitsNetRefSet(&bitsNetRefSetThen);
      BitsNetRefSet bitsNetRefSetElse;
      popBitsNetRefSet(&bitsNetRefSetElse);
      resizeAndInit(bitsNetRefSetSelect,1);
      resizeAndInit(bitsNetRefSetThen,op->getBitSize());
      resizeAndInit(bitsNetRefSetElse,op->getBitSize());

      BitsNetRefSet combBitsNetRefSet;
      resizeAndInit(combBitsNetRefSet,op->getBitSize());

      // Combine the net refs by bit
      const NUNetRefSet& bitNetRefSetSelect = bitsNetRefSetSelect[0];
      for (UInt32 i = 0; i < op->getBitSize(); ++i) {
        const NUNetRefSet& bitNetRefSetThen = bitsNetRefSetThen[i];
        const NUNetRefSet& bitNetRefSetElse = bitsNetRefSetElse[i];

        NUNetRefSet& combBitNetRefSet = combBitsNetRefSet[i];
        combBitNetRefSet.insert(bitNetRefSetSelect.begin(), bitNetRefSetSelect.end());
        combBitNetRefSet.insert(bitNetRefSetThen.begin(), bitNetRefSetThen.end());
        combBitNetRefSet.insert(bitNetRefSetElse.begin(), bitNetRefSetElse.end());
      }

      // Push the result on the stack
      pushBitsNetRefSet(combBitsNetRefSet);
      break;
    }

    case NUOp::eBiPlus:
    case NUOp::eBiMinus:
    case NUOp::eBiSMult:
    case NUOp::eBiSDiv:
    case NUOp::eBiSMod:
    case NUOp::eBiUMult:
    case NUOp::eBiUDiv:
    case NUOp::eBiUMod:
    case NUOp::eBiVhdlMod:
    case NUOp::eBiExp:
    case NUOp::eBiDExp:
    case NUOp::eBiRoR:
    case NUOp::eBiRoL:
    case NUOp::eBiRshift:
    case NUOp::eBiLshift:
    case NUOp::eBiVhZxt:
    case NUOp::eBiVhExt:
    case NUOp::eBiVhLshift:
    case NUOp::eBiVhRshift:
    case NUOp::eBiRshiftArith:
    case NUOp::eBiLshiftArith:
    case NUOp::eBiVhRshiftArith:
    case NUOp::eBiVhLshiftArith:
    case NUOp::eNaFopen:
    case NUOp::eNaLut:
    case NUOp::eZEndFile:
    case NUOp::eZSysTime:
    case NUOp::eZSysStime:
    case NUOp::eZSysRealTime:
      // Unsupported operators, they cross bits or I'm not really sure
      // what to do.
      mInvalidOperator = true;
      break;
      
    case NUOp::eStart:
    case NUOp::eInvalid:
    case NUOp::eBiDownTo:
      NU_ASSERT("Unexpected op type" == NULL, op);
      break;
  }; // switch

  if (mInvalidOperator)
    return NUDesignCallback::eStop;
  else
    return NUDesignCallback::eNormal;
} // BitNetRefCallback::operator

BitNetRefCallback::Status
BitNetRefCallback::operator()(Phase phase, NUIdentLvalue* ident)
{
  // Only do work in the post phase
  if (phase != ePost)
    return eNormal;

  // We only support vector or scalar nets
  NUNet* net = ident->getIdent();
  if (!net->isBitNet() && !net->isVectorNet()) {
    mInvalidOperator = true;
    return NUDesignCallback::eStop;
  }

  // Create a bits net ref set for the entire expression and push it
  // on the stack.
  NUNetRefHdl netRef = mNetRefFactory->createNetRef(net);
  createBitsNetRefSet(netRef, ident->getBitSize());
  return eNormal;
} // BitNetRefCallback::operator

BitNetRefCallback::Status
BitNetRefCallback::operator()(Phase phase, NUVarselLvalue* partsel)
{
  // Only do work in the pre phase
  if (phase != ePre)
    return eSkip;

  if ((not partsel->isConstIndex()) or (partsel->isArraySelect())
    or (not partsel->getLvalue ()->isWholeIdentifier ()))
  {
    mInvalidOperator = true;
    return eStop;
  }

  // Create a bits net ref set for the entire expression and push it
  // on the stack.
  NUNet* net = partsel->getIdentNet();
  NU_ASSERT (net, partsel);

  ConstantRange range = *partsel->getRange();
  
  NUNetRefHdl netRef = mNetRefFactory->createVectorNetRef(net, range);

  // If the index is out-of-range (may occur with -nofold, see bug 3472)
  // then reject this as unsupported.
  if (!netRef.isValid() || (*netRef).empty())
  {
    mInvalidOperator = true;
    return eStop;
  }

  createBitsNetRefSet(netRef, partsel->getBitSize());
  return eSkip;                 // Don't look into the index expression
} // BitNetRefCallback::operator

BitNetRefCallback::Status
BitNetRefCallback::operator()(Phase phase, NUConcatLvalue* concat)
{
  if (phase != ePost) 
    return eNormal;

  // Gather the parts of the concat op.
  BitsNetRefSet concatBitsNetRefSet;
  UInt32 narg = concat->getNumArgs();
  for (SInt32 i = narg - 1; i >= 0 ; --i) {
    BitsNetRefSet bitsNetRefSet;
    popBitsNetRefSet(&bitsNetRefSet);
    for (UInt32 j = 0; j < bitsNetRefSet.size(); ++j) {
      const NUNetRefSet& bitNetRefSet = bitsNetRefSet[j];
      concatBitsNetRefSet.push_back(bitNetRefSet);
    }
  }

  // Resize the concat in case we need to truncate or extend
  // it. Then we can finally push the result.
  resizeAndInit(concatBitsNetRefSet,concat->getBitSize());
  pushBitsNetRefSet(concatBitsNetRefSet);

  return eNormal;
}

NUContAssign*
LFBreakNetCycles::createSubAssign(NUModule* module, 
                                  const NULvalue* lvalue,
                                  const NUExpr* rvalue, 
                                  const SourceLocator& loc,
                                  UInt32 lhs_idx,
                                  UInt32 rhs_idx)
{
  // Gather the net and bit for the new lvalue
  NUNet* net = NULL;
  UInt32 index = 0;
  switch (lvalue->getType()) {
    case eNUVarselLvalue:
    {
      const NUVarselLvalue* partsel = dynamic_cast<const NUVarselLvalue*>(lvalue);
      net = partsel->getIdentNet(false);
      ConstantRange range (*partsel->getRange());
      NU_ASSERT (partsel->isConstIndex(), lvalue);   // variable bitselect...
      NU_ASSERT (!partsel->isArraySelect(), lvalue); // array select...

      index = range.index(lhs_idx, true);
      break;
    }

    case eNUIdentLvalue:
    {
      const NUIdentLvalue* ident = dynamic_cast<const NUIdentLvalue*>(lvalue);
      net = ident->getIdent();
      index = lhs_idx;
      break;
    }

    case eNUConcatLvalue:
    {
      const NUConcatLvalue * concat = dynamic_cast<const NUConcatLvalue*>(lvalue);
      SInt32 idx = 0;
      for (SInt32 argnum = concat->getNumArgs() - 1; argnum >= 0; --argnum) {
        const NULvalue *arg = concat->getArg((UInt32)argnum);
        ConstantRange this_part_range(idx + arg->getBitSize() - 1, idx);
        if (this_part_range.contains(lhs_idx)) {
          return createSubAssign(module, arg, rvalue, loc, this_part_range.offsetBounded(lhs_idx), rhs_idx);
        }
        idx += arg->getBitSize();
      }
      NU_ASSERT ("Unexpected end of concat when breaking up assign statement"==NULL,
                 lvalue);
      break;
    }

    default:
      NU_ASSERT("Unexpected lvalue when breaking up assign statement" == NULL,
                lvalue);
      break;
  } // switch

  // Create the new lvalue (with constant index)
  ConstantRange lrange (index,index);
  NULvalue* lhs = new NUVarselLvalue(net, lrange, loc);
  lhs = mFold->fold(lhs);

  // Create a bit select of the rhs
  NUExpr* rhs = rvalue->copy(*mCopyContext);
  ConstantRange rrange (rhs_idx, rhs_idx);
  rhs = new NUVarselRvalue(rhs, rrange, loc);
  rhs->resize(1);
  rhs = mFold->fold(rhs);

  // Don't build an assign to 1'bz, just skip it.
  NUConst* z = rhs->castConst();
  if ((z != NULL) && z->drivesZ()) {
    NU_ASSERT(z->getBitSize() == 1, z);
    delete lhs;
    delete rhs;
    return NULL;
  }

  // Create the new assignment
  StringAtom* blockName = module->newBlockName(mStrCache, loc);
  NUContAssign* newAssign = new NUContAssign(blockName, lhs, rhs, loc);
  return newAssign;
}

void BitNetRefCallback::pushBitsNetRefSet(BitsNetRefSet& bitsNetRefSet)
{
  mBitsNetRefSetStack.push_back(bitsNetRefSet);
}

void BitNetRefCallback::popBitsNetRefSet(BitsNetRefSet* bitsNetRefSet)
{
  INFO_ASSERT(!mBitsNetRefSetStack.empty(), "stack cannot be empty");
  *bitsNetRefSet = mBitsNetRefSetStack.back();
  mBitsNetRefSetStack.pop_back();
}


void BitNetRefCallback::resizeAndInit(BitsNetRefSet & bitsNetRefSet,
                                      UInt32 size)
{
  UInt32 old_size = bitsNetRefSet.size();
  NUNetRefSet empty(mNetRefFactory);

  bitsNetRefSet.reserve(size);
  for (UInt32 i = old_size ; i < size ; ++i) {
    bitsNetRefSet.push_back(empty);
  }
}


void BitNetRefCallback::createBitsNetRefSet(const NUNetRefHdl& netRef,
                                            UInt32 bitSize)
{
  // Make sure it is sized properly. This will created empty nets refs
  // for the bits we don't assign below.
  BitsNetRefSet bitsNetRefSet;
  resizeAndInit(bitsNetRefSet,bitSize);

  // Get the range for the slice. We should not have ranges with holes
  // because we are dealing with simple bit, part, ident expressions.
  ConstantRange range;
  bool validRange = netRef->getRange(range);
  NU_ASSERT(validRange, netRef);

  // Create the split net refs.  Note that because we don't
  // follow verilog sizing rules in FoldIStmt::foldAssign, we
  // can get a net-ref range for the whole net, when the bitSize
  // we are looking for is for fewer bits.  That wouldn't happen
  // if we were strict about the sizing rules.
  SInt32 index = range.getLsb();
  UInt32 size = std::min(bitSize, range.getLength());
  for (UInt32 i = 0; i < size; ++i, ++index) {
    NU_ASSERT(i < bitsNetRefSet.size(), netRef->getNet());
    NUNetRefSet& netRefSet = bitsNetRefSet[i];
    NUNetRefHdl bitNetRef = mNetRefFactory->sliceNetRef(netRef, index);
    netRefSet.insert(bitNetRef);
  }

  // Push this on our stack
  pushBitsNetRefSet(bitsNetRefSet);
} // void BitNetRefCallback::createBitsNetRefSet

void BitNetRefCallback::combineAllNetRefs(NUNetRefSet* allBits)
{
  // Grab the argument of the stack
  BitsNetRefSet bitsNetRefSet;
  popBitsNetRefSet(&bitsNetRefSet);

  // Apply the net ref sets to one net ref set
  for (BitsNetRefSetLoop l(bitsNetRefSet); !l.atEnd(); ++l) {
    const NUNetRefSet& netRefSet = *l;
    allBits->insert(netRefSet.begin(), netRefSet.end());
  }
}


//! A private container for split assigns
struct LFSplitAssigns::SplitContainer
{
  //! A split assign object which contains a loc and flags
  struct SplitAssign
  {
    SplitAssign(const SourceLocator& loc, LFSplitAssigns::Flags flags)
      : mLoc(loc), mFlags(flags)
    {}
    
    SourceLocator mLoc;
    LFSplitAssigns::Flags mFlags;

    //! Hash function
    size_t hash() const {
      return mLoc.hash() + (size_t) mFlags;
    }

    //! Returns true if this assign was split because of a cycle.
    bool isSplitCycle() const { 
      return ((mFlags & LFSplitAssigns::eCycle) != 0); 
    }

    //! Returns true if this assign was split because of a mixed constant
    bool isSplitConstant() const { 
      return ((mFlags & LFSplitAssigns::eMixedConst) != 0); 
    }
    
    //! Get the SourceLocator
    const SourceLocator& getLoc() const { return mLoc; }

    //! operator== for hashing
    bool operator==(const SplitAssign& other) const
    {
      return ((mLoc == other.mLoc) && (mFlags == other.mFlags));
    }

    //! Sort operator<
    bool operator<(const SplitAssign& other) const
    {
      bool isLess = false;
      if (mLoc == other.mLoc)
        isLess = mFlags < other.mFlags;
      else
        isLess = mLoc < other.mLoc;
      return isLess;
    }
  };

  //! Set type of SplitAssign structures
  typedef UtHashSet<SplitAssign, HashValue<SplitAssign> > SplitAssignSet;

  //! The set of split assigns
  SplitAssignSet mSplits;

  //! Add a split assign.
  void add(const SourceLocator& loc, LFSplitAssigns::Flags flags)
  {
    SplitAssign tmp(loc, flags);
   
    mSplits.insert(tmp);
  }
  
  //! Print the split assigns
  void print(MsgContext* msgContext) const
  {
    for (SplitAssignSet::SortedLoop l = mSplits.loopSorted(); !l.atEnd(); ++l)
    {
      const SplitAssign& sa = *l;
      const SourceLocator& loc = sa.getLoc();
      if (sa.isSplitCycle())
        msgContext->LFSplitAssign(&loc);
      if (sa.isSplitConstant())
        msgContext->LFSplitAssignMixedConst(&loc);
    }
  }
};

// The following routines are used to keep track of the split assigns
// for verbosity printing.
LFSplitAssigns::LFSplitAssigns(MsgContext* msgContext) :
  mMsgContext(msgContext)
{
  mSplitAssigns = new SplitContainer;
}

LFSplitAssigns::~LFSplitAssigns()
{
  delete mSplitAssigns;
}

void LFSplitAssigns::add(const SourceLocator& loc, Flags flags)
{
  mSplitAssigns->add(loc, flags);
}

void LFSplitAssigns::print(void) const
{
  mSplitAssigns->print(mMsgContext);
}

void LFSplitAssigns::sSetFlag(Flags* flags, Flags add)
{
  *flags = Flags(*flags | add);
}
