// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  A class to test if an elaborated net represents a latch or flop
*/

#include "nucleus/NUNet.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"

#include "localflow/ElabStateTest.h"

LFElabStateTest::LFElabStateTest()
{
  mElabLatchFlag = new ElabLatchFlag;
}

LFElabStateTest::~LFElabStateTest()
{
  delete mElabLatchFlag;
}

bool LFElabStateTest::isLatch(const NUNetElab* netElab) const
{
  StateFlags flags = getStateFlags(netElab);
  return (flags & eSFLatch) != eSFNone;
}

bool LFElabStateTest::isLatch(FLNodeElab* flowElab) const
{
  // This test does not use the cache because there is a simple test
  // for testing an elaborated flow. Just get the unelaborated flow's
  // def net and that is the net that is driven by this elaborated
  // flow's always block.
  return flowElab->getFLNode()->getDefNet()->isLatch();
}

bool LFElabStateTest::isFlop(const NUNetElab* netElab) const
{
  StateFlags flags = getStateFlags(netElab);
  return (flags & eSFFlop) != eSFNone;
}

bool LFElabStateTest::isFlop(FLNodeElab* flowElab) const
{
  // This test does not use the cache because there is a simple test
  // for testing an elaborated flow. Just get the unelaborated flow's
  // def net and that is the net that is driven by this elaborated
  // flow's always block.
  return flowElab->getFLNode()->getDefNet()->isFlop();
}

bool LFElabStateTest::isState(const NUNetElab* netElab) const
{
  StateFlags flags = getStateFlags(netElab);
  return flags != eSFNone;
}

bool LFElabStateTest::isState(FLNodeElab* flowElab) const
{
  // This test does not use the cache because there is a simple test
  // for testing an elaborated flow. Just get the unelaborated flow's
  // def net and that is the net that is driven by this elaborated
  // flow's always block.
  NUNet* net = flowElab->getFLNode()->getDefNet();
  return net->isLatch() || net->isFlop();
}

LFElabStateTest::StateFlags
LFElabStateTest::getStateFlags(const NUNetElab* netElab) const
{
  // Check the cache
  StateFlags flags = eSFNone;
  ElabLatchFlag::iterator pos = mElabLatchFlag->find(netElab);
  if (pos != mElabLatchFlag->end()) {
    flags = pos->second;
  } else {
    // Doesn't exist yet, create it
    if (netElab->queryAliases(&NUNet::isLatch)) {
      flags = StateFlags(flags | eSFLatch);
    }
    if (netElab->queryAliases(&NUNet::isFlop)) {
      flags = StateFlags(flags | eSFFlop);
    }
    mElabLatchFlag->insert(ElabLatchFlag::value_type(netElab, flags));
  }
  return flags;
}
