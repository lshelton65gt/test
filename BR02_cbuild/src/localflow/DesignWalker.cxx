// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "localflow/DesignWalker.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "iodb/IODB.h"

void DesignWalker::design(NUDesign *design)
{
  // We might have walked this design already and be rewalking (FoldI for example)
  // so reset the memory of visited modules.
  mScopeSet.clear ();

  for (NUDesign::ModuleLoop loop = design->loopTopLevelModules(); 
       not loop.atEnd(); 
       ++loop ) {
    module(*loop);
  }
}

#if 0
// the original intent was for the DesignWalker subclasses to use the
// module() recursive traversal method and call an operate() method to
// perform per-module analysis. unfortunately, many do not perform a
// pre-recursion operation -- they have both pre and post-recursion
// steps. Another pass of refactoring is required to pull the module
// traversal into DesignWalker.
void DesignWalker::module(NUModule * this_module)
{
  if (haveVisited(this_module)) {
    return;
  }

  operate(this_module);

  // Recurse to handle sub-modules
  NUModuleList inst_list;
  this_module->getInstantiatedModules(&inst_list);
  for (NUModuleListIter iter = inst_list.begin();
       iter != inst_list.end();
       ++iter) {
    module(*iter);
  }

  rememberVisited(this_module);
}
#endif



bool DesignWalker::haveVisited(NUScope *scope) const
{
  return (mScopeSet.find(scope) != mScopeSet.end());
}


void DesignWalker::rememberVisited(NUScope *scope)
{
  mScopeSet.insert(scope);
}
