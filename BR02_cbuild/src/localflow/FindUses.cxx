// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"

#include "localflow/FindUses.h"

LFFindUsesCallback::LFFindUsesCallback(NUNetSet* netSet) :
  mNets(netSet), mNetRefs(NULL)
{}

LFFindUsesCallback::LFFindUsesCallback(NUNetRefSet* netRefSet) :
  mNets(NULL), mNetRefs(netRefSet)
{}

NUDesignCallback::Status
LFFindUsesCallback::operator()(Phase phase, NUExpr* expr)
{
  if (phase == ePre) {
    if (mNets != NULL) {
      expr->getUses(mNets);
    }
    if (mNetRefs != NULL) {
      expr->getUses(mNetRefs);
    }
  }
  return eSkip; // no need to walk through sub-exprs.
}
