// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef LOCALHIERREFRESOLVER_H_
#define LOCALHIERREFRESOLVER_H_

class NUDesign;
class NUModule;
class AtomicCache;

//! LocalHierRefResolver class
/*!
 * Try to resolve hierarchical references locally to the
 * module in which they appear.
 *
 * Note: this does not update UD or the flow graph, so
 * it must be called very early in the analysis phase.
 */
class LocalHierRefResolver
{
public:
  //! constructor
  LocalHierRefResolver(AtomicCache *str_cache);

  //! destructor
  ~LocalHierRefResolver();

  //! Walk all the modules in the given design.
  void design(NUDesign *design);

  //! Walk the given module.
  void module(NUModule *module);

private:
  //! Hide copy and assign constructors.
  LocalHierRefResolver(const LocalHierRefResolver&);
  LocalHierRefResolver& operator=(const LocalHierRefResolver&);

  //! Try to resolve hierref nets in the given module.
  void moduleNets(NUModule *module);

  //! Try to resolve hierref tasks in the given module.
  void moduleTasks(NUModule *module);

  //! String cache
  AtomicCache *mStrCache;
};

#endif
