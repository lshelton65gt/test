// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignHierarchy.h"
#include "util/UtLibXmlWriter.h"

DesignHierarchy::DesignHierarchy(UtString fileName) 
  : mFileName(fileName),
    mCurrentInterface(NULL)
{
}

DesignHierarchy::~DesignHierarchy()
{
  // Clean up
  for (InstancesIter iter = mTopLevelInstances.begin();
       iter != mTopLevelInstances.end();
       ++iter) {
    delete *iter;
  }

  for(InterfaceIter iter = mInterfaces.begin(); 
      iter != mInterfaces.end(); 
      ++iter) {
    delete iter->second;
  }
  mInterfaces.clear();

  for(LibraryIter iter = mLibraries.begin(); 
      iter != mLibraries.end(); 
      ++iter) {
    delete iter->second;
  }
  mLibraries.clear();

  for(FileIter iter = mFiles.begin(); 
      iter != mFiles.end(); 
      ++iter) {
    delete iter->second;
  }
  mFiles.clear();

  for (TypeIter iter = mTypes.begin();
       iter != mTypes.end();
       ++iter) {
    delete iter->second;
  }
  mTypes.clear();
}

bool
DesignHierarchy::interfaceExists(UtString searchName)
{
  return mInterfaces.find(searchName) == mInterfaces.end() ? false : true;
}

void
DesignHierarchy::addInterface(UtString searchName, Interface* interface)
{
  mInterfaces[searchName] = interface;
}

bool
DesignHierarchy::libraryExists(UtString searchName)
{
  return mLibraries.find(searchName) == mLibraries.end() ? false : true;
}

void
DesignHierarchy::addLibrary(UtString libraryName, Library* library)
{
  mLibraries[libraryName] = library;
}

bool
DesignHierarchy::fileExists(UtString searchName)
{
  return mFiles.find(searchName) == mFiles.end() ? false : true;
}

void
DesignHierarchy::addFile(UtString fileName, File* file) 
{
  mFiles[fileName] = file;
}

bool
DesignHierarchy::typeExists(UtString libraryName, UtString packageName, UtString typeName)
{
  return mTypes.find(libraryName + packageName + typeName) == mTypes.end() ? false : true;
}

void
DesignHierarchy::addType(UtString libraryName, UtString packageName, UtString typeName, Type* type)
{
  mTypes[libraryName + packageName + typeName] = type;
}

void
DesignHierarchy::addTopLevelDesignUnit(Instance* instance)
{
  mTopLevelInstances.push_back(instance);
}

void
DesignHierarchy::pushInstance(Instance* instance)
{
  mInstanceStack.push_front(instance);
}

void
DesignHierarchy::popInstance() 
{
  return mInstanceStack.pop_front();
}

Instance*
DesignHierarchy::getCurrentInstance()
{
  return mInstanceStack.front();
}

void
DesignHierarchy::setCurrentInterface(Interface* interface)
{
  mCurrentInterface = interface;
}

Interface*
DesignHierarchy::getCurrentInterface()
{
  return mCurrentInterface;
}

void
DesignHierarchy::dumpXml()
{
  UtLibXmlWriter xmlWriter;

  xmlWriter.StartWriter(mFileName);

  xmlWriter.StartElement("DesignHierarchy");

  // Dump all the instances
  dumpInstances(&xmlWriter);

  // Dump all the interfaces
  dumpInterfaces(&xmlWriter);

  // Dump all the libraries
  dumpLibraries(&xmlWriter);

  // Dump all the files
  dumpFiles(&xmlWriter);

  // Dump all the types
  dumpTypes(&xmlWriter);

  xmlWriter.EndElement();

  xmlWriter.EndWriter();
}

void
DesignHierarchy::dumpInterfaces(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Interfaces");

  for (InterfaceIter iter = mInterfaces.begin();
       iter != mInterfaces.end();
       ++iter) {
    iter->second->dumpInterface(xmlWriter);
  }

  xmlWriter->EndElement();
}

void
DesignHierarchy::dumpLibraries(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Libraries");

  for (LibraryIter iter = mLibraries.begin();
       iter != mLibraries.end();
       ++iter) {
    iter->second->dumpLibrary(xmlWriter);
  }

  xmlWriter->EndElement();
}

void
DesignHierarchy::dumpFiles(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Files");

  for (FileIter iter = mFiles.begin();
       iter != mFiles.end();
       ++iter) {
    iter->second->dumpFile(xmlWriter);
  }

  xmlWriter->EndElement();
}

void
DesignHierarchy::dumpTypes(UtXmlWriter* xmlWriter)
{
  if (mTypes.empty()) {
    return;
  }

  xmlWriter->StartElement("Types");

  for (TypeIter iter = mTypes.begin();
       iter != mTypes.end();
       ++iter) {
    iter->second->dumpType(xmlWriter);
  }

  xmlWriter->EndElement();
}

void
DesignHierarchy::dumpInstances(UtXmlWriter* xmlWriter)
{
  for (InstancesIter iter = mTopLevelInstances.begin();
       iter != mTopLevelInstances.end();
       ++iter) {
    (*iter)->dumpInstance(xmlWriter);
  }
}

bool
DesignHierarchy::hasSubstituteInterface()
{
  return !mSubstituteInterfaceName.empty();
}

void
DesignHierarchy::setSubstituteInterfaceName(UtString substituteName)
{
  mSubstituteInterfaceName = substituteName;
}

void
DesignHierarchy::clearSubstituteInterfaceName()
{
  mSubstituteInterfaceName = "";
}

UtString
DesignHierarchy::getSubstituteInterfaceName()
{
  return mSubstituteInterfaceName;
}

void
DesignHierarchy::addSubstitutePortMap(UtString origName, UtString subsName)
{
  mSubstitutePortMap[origName] = subsName;
}

UtString
DesignHierarchy::getSubstitutePortMap(UtString origName)
{
  SubstitutePortMap::iterator iter = mSubstitutePortMap.find(origName);
  if (iter != mSubstitutePortMap.end())
    return (*iter).second;
  else
    return "";
}

void
DesignHierarchy::clearSubstitutePortMap()
{
  mSubstitutePortMap.clear();
}

Parameter::Parameter(UtString name, 
		     UtString type, 
		     UtString value)
  : mName(name),
    mType(type),
    mValue(value)
{
}

Parameter::~Parameter()
{
}

UtString
Parameter::getName()
{
  return mName;
}

void
Parameter::setStart(UtString fileName, UInt32 lineNo) 
{
  mStart = fileName;
  mStart += ":";
  UtString number;
  number << lineNo;
  mStart += number;
}

void
Parameter::dumpParameter(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Parameter");
  xmlWriter->WriteAttribute("name", mName);
  xmlWriter->WriteAttribute("type", mType);

  if(!mValue.empty()) {
    xmlWriter->WriteAttribute("value", mValue);
  }

  if (!mStart.empty()) {
    xmlWriter->WriteAttribute("locator", mStart);
  }

  xmlWriter->EndElement();
}

Port::Port(UtString name, 
	   UtString type, 
	   UtString direction, 
	   UtString width)
  : mName(name),
    mType(type),
    mDirection(direction),
    mWidth(width)
{
}

Port::Port(UtString actual, 
	   UtString formal)
  : mActual(actual),
    mFormal(formal)
{
}

Port::~Port()
{
}

UtString
Port::getName()
{
  return mName;
}

void
Port::setStart(UtString fileName, UInt32 lineNo) 
{
  mStart = fileName;
  mStart += ":";
  UtString number;
  number << lineNo;
  mStart += number;
}

void
Port::dumpPort(UtXmlWriter* xmlWriter) 
{
  xmlWriter->StartElement("Port");
  if (!mName.empty()) {
    xmlWriter->WriteAttribute("name", mName);
  }
  if (!mType.empty()) {
    xmlWriter->WriteAttribute("type", mType);
  }
  if (!mDirection.empty()) {
    xmlWriter->WriteAttribute("direction", mDirection);
  }
  if (!mWidth.empty()) {
    xmlWriter->WriteAttribute("width", mWidth);
  }
  if (!mActual.empty()) {
    xmlWriter->WriteAttribute("actual", mActual);
  }
  if (!mFormal.empty()) {
    xmlWriter->WriteAttribute("formal", mFormal);
  }
  if (!mStart.empty()) {
    xmlWriter->WriteAttribute("locator", mStart);
  }
  xmlWriter->EndElement();

}

Architecture::Architecture(UtString name, 
			   UtString libraryName)
  : mName(name),
    mLibraryName(libraryName)
{
}

Architecture::~Architecture()
{
}

UtString
Architecture::getName()
{
  return mName;
}

void
Architecture::setStart(UtString fileName, UInt32 lineNo)
{
  mStart = fileName;
  mStart += ":";
  UtString number;
  number << lineNo;
  mStart += number;
}

void
Architecture::setEnd(UtString fileName, UInt32 lineNo)
{
  mEnd = fileName;
  mEnd += ":";
  UtString number;
  number << lineNo;
  mEnd += number;
}

void
Architecture::dumpArchitecture(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Architecture");
  xmlWriter->WriteAttribute("name", mName);
  xmlWriter->WriteAttribute("library", mLibraryName);
  xmlWriter->WriteAttribute("start", mStart);
  xmlWriter->WriteAttribute("end", mEnd);
  xmlWriter->EndElement();
}

Library::Library(UtString name, 
		 UtString path, 
		 UtString language)
  : mName(name),
    mPath(path),
    mLanguage(language),
    mIsDefault(false)
{
}

Library::~Library()
{
}

UtString
Library::getName()
{
  return mName;
}

void
Library::setAsDefaultLibrary()
{
  mIsDefault = true;
}

void
Library::dumpLibrary(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Library");

  if (mIsDefault) {
    xmlWriter->WriteAttribute("default", "true");
  }

  xmlWriter->WriteAttribute("name", mName);
  xmlWriter->WriteAttribute("path", mPath);

  xmlWriter->EndElement();

}

Define::Define(UtString name, 
	       UtString value, 
	       UtString startLineNo)
  : mName(name),
    mValue(value),
    mStart(startLineNo)
{
}

Define::~Define()
{
}

void
Define::dumpDefine(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Define");
  xmlWriter->WriteAttribute("name", mName);
  xmlWriter->WriteAttribute("value" , mValue);
  xmlWriter->WriteAttribute("start", mStart);
  xmlWriter->EndElement();
}

File::File(UtString name, 
	   UtString fileType, 
	   UtString language)
  : mName(name),
    mFileType(fileType),
    mLanguage(language)
{
}

File::~File()
{
  for (DefinesIter iter = mDefines.begin();
       iter != mDefines.end();
       ++iter) {
    delete *iter;
  }

  mDefines.clear();
}

void
File::setFileType(UtString type)
{
  mFileType = type;
}

void
File::addDefine(Define* define)
{
  mDefines.push_back(define);
}

void
File::dumpFile(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("File");
  xmlWriter->WriteAttribute("name", mName);
  xmlWriter->WriteAttribute("type", mFileType);
  xmlWriter->WriteAttribute("language", mLanguage);

  if (!mDefines.empty()) {
    xmlWriter->StartElement("Defines");
    
    for (DefinesIter iter = mDefines.begin(); 
	 iter != mDefines.end(); 
	 ++iter)
      (*iter)->dumpDefine(xmlWriter);

    xmlWriter->EndElement();
  }

  xmlWriter->EndElement();
}

UtString
File::getFileType(FileType fileType) 
{
  
  UtString fileTypeStr = "unknown";

  switch (fileType)
    {
    case eEntity:
      fileTypeStr = "entity";
      break;
    case eArchitecture:
      fileTypeStr = "architecture";
      break;
    case ePackageDeclaration:
      fileTypeStr = "package declaration";
      break;
    case ePackageBody:
      fileTypeStr = "package body";
      break;
    case eConfiguration:
      fileTypeStr = "configuration";
      break;
    case eModule:
      fileTypeStr = "module";
      break;
    case eVlogLibrary:
      fileTypeStr = "verilog library";
      break;
    case eIncludedFile:
      fileTypeStr = "included file";
      break;
    default:
      break;
    }

  return fileTypeStr;
}

Type::Type(UtString libraryName, UtString packageName, UtString typeName)
  : mLibraryName(libraryName),
    mPackageName(packageName),
    mTypeName(typeName)
{
}

Type::~Type()
{
}

void
Type::setStart(UtString fileName, UInt32 lineNo)
{
  mStart = fileName;
  mStart += ":";
  UtString number;
  number << lineNo;
  mStart += number;
}

void
Type::setEnd(UtString fileName, UInt32 lineNo)
{
  mEnd = fileName;
  mEnd += ":";
  UtString number;
  number << lineNo;
  mEnd += number;
}

void
Type::dumpType(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("Type");
  xmlWriter->WriteAttribute("name", mTypeName);
  xmlWriter->WriteAttribute("library", mLibraryName);
  xmlWriter->WriteAttribute("package", mPackageName);
  xmlWriter->WriteAttribute("start", mStart);
  xmlWriter->WriteAttribute("end", mEnd);
  xmlWriter->EndElement();
}

Interface::Interface(UtString name, 
		     UtString libraryName, 
		     UtString language)
  : mProtected(false),
    mName(name),
    mLibraryName(libraryName),
    mLanguage(language)
{
}

Interface::Interface(bool inProtectedRegion)
  : mProtected(inProtectedRegion)
{
}

Interface::~Interface()
{
  for (ParametersIter iter = mParameters.begin();
       iter != mParameters.end();
       ++iter) {
    delete *iter;
  }
  mParameters.clear();

  for (PortsIter iter = mPorts.begin();
       iter != mPorts.end();
       ++iter) {
    delete *iter;
  }
  mPorts.clear();

  for (ArchitecturesIter iter = mArchitectures.begin();
       iter != mArchitectures.end();
       ++iter) {
    delete iter->second;
  }
  mArchitectures.clear();
}

void
Interface::setStart(UtString fileName, UInt32 lineNo)
{
  mStart = fileName;
  mStart += ":";
  UtString number;
  number << lineNo;
  mStart += number;
}

void
Interface::setEnd(UtString fileName, UInt32 lineNo)
{
  mEnd = fileName;
  mEnd += ":";
  UtString number;
  number << lineNo;
  mEnd += number;
}

void
Interface::addParameter(Parameter* param)
{
  mParameters.push_back(param);
}

void
Interface::addPort(Port* port)
{
  mPorts.push_back(port);
}

void
Interface::addArchitecture(Architecture* architecture)
{
  mArchitectures[architecture->getName()] = architecture;
}

void
Interface::dumpInterface(UtXmlWriter* xmlWriter)
{
  if (mProtected) {
    xmlWriter->StartElement("Protected");
    xmlWriter->EndElement();
  }
  else {
    xmlWriter->StartElement("Interface");
    xmlWriter->WriteAttribute("name", mName);
    xmlWriter->WriteAttribute("library", mLibraryName);
    xmlWriter->WriteAttribute("start", mStart);
    xmlWriter->WriteAttribute("end", mEnd);
    xmlWriter->WriteAttribute("language", mLanguage);
    
    if (!mParameters.empty()) {
      xmlWriter->StartElement("Parameters");
      for (ParametersIter iter = mParameters.begin();
	   iter != mParameters.end();
	   ++iter) {
	(*iter)->dumpParameter(xmlWriter);
      }
      xmlWriter->EndElement();
    }
    
    if (!mPorts.empty()) {
      xmlWriter->StartElement("Ports");
      for (PortsIter iter = mPorts.begin();
	   iter != mPorts.end();
	   ++iter) {
	(*iter)->dumpPort(xmlWriter);
      }
      xmlWriter->EndElement();
    }
    
    if (!mArchitectures.empty()) {
      xmlWriter->StartElement("Architectures");
      for (ArchitecturesIter iter = mArchitectures.begin();
	   iter != mArchitectures.end();
	   ++iter) {
	iter->second->dumpArchitecture(xmlWriter);
      }
      xmlWriter->EndElement();
    }
    
    xmlWriter->EndElement();
  }
}

Instance::Instance(UtString name, 
		   UtString library, 
		   UtString interface,
		   UtString language)
  : Interface(name, library, language),
    mTopLevel(false),
    mInterface(interface)
{
}

Instance::Instance(bool inProtectedRegion)
  : Interface(inProtectedRegion)
{
}

Instance::~Instance()
{
  for (InstancesIter iter = mInstances.begin(); 
       iter != mInstances.end(); 
       ++iter) {
    delete *iter;
  }
  mInstances.clear();
}

void
Instance::isTopLevel() 
{
  mTopLevel = true;
}

void
Instance::addArchitectureName(UtString name)
{
  mArchitecture = name;
}

void
Instance::addInstance(Instance* instance)
{
  mInstances.push_back(instance);
}

void
Instance::setOriginalInterface(UtString originalInterfaceName)
{
  mOriginalInterfaceName = originalInterfaceName;
}

void
Instance::setInterfaceType(UtString typeName)
{
  mInterfaceType = typeName;
}

void
Instance::dumpInstance(UtXmlWriter* xmlWriter)
{
  if (mProtected) {
    xmlWriter->StartElement("Protected");
    xmlWriter->EndElement();
  }
  else {
    if (mTopLevel) {
      xmlWriter->StartElement("ToplevelInterface");
      xmlWriter->WriteAttribute("name", mName);
    }
    else {
      xmlWriter->StartElement("Instance");
      xmlWriter->WriteAttribute("id", mName);
    }
    xmlWriter->WriteAttribute("library", mLibraryName);
    xmlWriter->WriteAttribute("locator", mStart);
    xmlWriter->WriteAttribute("interface", mInterface);
    
    if (!mInterfaceType.empty())
      xmlWriter->WriteAttribute("type", mInterfaceType);

    if (!mArchitecture.empty()) {
      xmlWriter->WriteAttribute("architecture", mArchitecture);
    }

    if (!mOriginalInterfaceName.empty()) {
      xmlWriter->WriteAttribute("original_interface", mOriginalInterfaceName);
    }
    
    if (!mParameters.empty()) {
      xmlWriter->StartElement("Parameters");
      for (ParametersIter iter = mParameters.begin();
	   iter != mParameters.end();
	   ++iter) {
	(*iter)->dumpParameter(xmlWriter);
      }
      xmlWriter->EndElement();
    }
    
    if (!mPorts.empty()) {
      xmlWriter->StartElement("Ports");
      for (PortsIter iter = mPorts.begin();
	   iter != mPorts.end();
	   ++iter) {
	(*iter)->dumpPort(xmlWriter);
      }
      xmlWriter->EndElement();
    }

    if (!mInstances.empty()) {
      for (InstancesIter iter = mInstances.begin();
	   iter != mInstances.end();
	   ++iter) {
	(*iter)->dumpInstance(xmlWriter);
      }
    }
    
    xmlWriter->EndElement();
  }
}
