// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "localflow/LowerPortConnections.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUScope.h"
#include "util/AtomicCache.h"

#include "localflow/LocalAnalysis.h"
#include "localflow/VerilogPopulate.h"
#include "LFContext.h"

#include "util/ArgProc.h"
#include "util/CbuildMsgContext.h"
#include "compiler_driver/CarbonContext.h"

void LocalAnalysis::lowerPorts(NUModule *module, bool lower_everything)
{
  LowerPortConnections LP(mStrCache,
                          mMsgContext,
                          mArg->getBoolValue(CRYPT("-shareConstants")),
                          lower_everything);
  LP.module(module);
}

/*!
 * Check to see if we already have a suitably initialized continuous assignment
 * that we could reuse.
 */
NUNet * LowerPortConnections::reuseTempAssign (NUModule* m, NUExpr *actual, UInt32 bits)
{
  // Unconnected input - we could just connect to ANY old member that's the same size, but
  // that would be awfully confusing.  For now, just ignore this case.
  if (actual == NULL)
    return NULL;

  // Look at the existing continuous assignments and see if there's one that
  // has the same RHS expression.
  for (NUModule::ContAssignLoop loop = m->loopContAssigns (); 
       not loop.atEnd (); 
       ++loop) {
    NUContAssign *assign = (*loop);
    NULvalue * lhs = assign->getLvalue();
    NUExpr   * rhs = assign->getRvalue();

    // we're trying to lower hier-refs. don't allow them as lowering
    // replacements (bug 2451).
    if (lhs->isHierRef() or rhs->isHierRef()) {
      continue;
    }

    // Don't share constant assigns. The problem is some of them
    // represent z values (undriven nets) and others represent true
    // constants. If we alias them we can make a driven net look
    // undriven. We don't think there is a benefit to aliasing
    // constants.
    if (rhs->castConst() != NULL) {
      continue;
    }

    if (lhs->isWholeIdentifier() and
        lhs->getBitSize() == bits and
        (*rhs) == (*actual) ) {
      // If we return an already coded temporary, we will never use the actual
      // expression in any tree and we must delete it here.
      delete actual;
      return assign->getLvalue()->getWholeIdentifier();
    }
  }
  return NULL;
}

void LowerPortConnections::module(NUModule * one)
{
  mModule = one;		// remember the active scope

  // loop over each module instance, buffering port connections as we go.
  for (NUModuleInstanceMultiLoop iter = one->loopInstances();
       !iter.atEnd(); ++iter) {
    NUModuleInstance *inst = (*iter);
    instance(inst);
  }


  if (mLowerEverything) {
    // loop over each always block searching for cmodel connections.
    for (NUModule::AlwaysBlockLoop loop = one->loopAlwaysBlocks();
	 not loop.atEnd();
	 ++loop) {
      NUAlwaysBlock * always = (*loop);
      structuredProc(always);
    }

    // loop over each initial block searching for cmodel connections.
    for (NUModule::InitialBlockLoop loop = one->loopInitialBlocks();
	 not loop.atEnd();
	 ++loop) {
      NUInitialBlock * initial = (*loop);
      structuredProc(initial);
    }

    // Potentially lower statements inside tasks
    for (NUTaskLoop loop = one->loopTasks(); not loop.atEnd(); ++loop) {
      NUTask* task = *loop;
      tf(task);
    }
  }
  mModule = NULL;		// clear the active scope
}

void LowerPortConnections::instance(NUModuleInstance * inst)
{
  FlattenQualifyPort qualifier(inst->getModule());
  for (NUPortConnectionLoop loop = inst->loopPortConnections();
       not loop.atEnd();
       ++loop) {
    NUPortConnection * portconn = (*loop);
    port(portconn, qualifier);
  }
}

void LowerPortConnections::port(NUPortConnection * portconn,
				FlattenQualifyPort &qualifier)
{
  NUPortConnectionInput  * inport  = dynamic_cast<NUPortConnectionInput*>(portconn);
  NUPortConnectionOutput * outport = dynamic_cast<NUPortConnectionOutput*>(portconn);
  NUPortConnectionBid    * biport  = dynamic_cast<NUPortConnectionBid*>(portconn);
  if (inport) {
    input(inport, qualifier);
  } else if (outport) {
    output(outport, qualifier);
  } else {
    NU_ASSERT(biport, portconn);
    bid(biport, qualifier);
  }
}

void LowerPortConnections::input(NUPortConnectionInput * inport,
				 FlattenQualifyPort &qualifier)
{
  NUExpr * actual = inport->getActual();
  NUNet  * formal = inport->getFormal();

  if (not qualify(actual, formal, qualifier)) {
    return;
  }

  if ( not inport->isAliasable() ) {

    NUExpr * real_actual = lower(mModule, actual, formal->getBitSize(), inport->getLoc());
    inport->setActual(real_actual);
  }
}

// We pass loc by value because it might have been interior to 'actual', something
// we might be deleting in reuseTempAssign()
//
NUExpr * LowerPortConnections::lower(NUModule * module,
                                     NUExpr * actual, 
                                     UInt32 bits,
                                     const SourceLocator loc)
{
  // Look to see if we already have a continuous assignment of this value
  // to a lhs within this module - if so, reuse the location.
  NUNet *temp = mReuseAssigns ? reuseTempAssign (module, actual, bits): NULL;

  if (not temp) {
    StringAtom *block_name = module->newBlockName(mStrCache, loc);
    StringAtom* netsym = module->gensym("lower", NULL, actual);
    bool netSign = (actual and actual->isSignedResult());
    temp = module->createTempNet(netsym, bits, netSign, loc);
    // declare as a wire so we get initialization correct.
    temp->setFlags(NetRedeclare(temp->getFlags(),eDMWireNet));

    if (actual) {
      // This is a connected port; create an assignment to the
      // original actual. For disconnected ports, we leave
      // _temp_ disconnected.
      NULvalue *lhs = new NUIdentLvalue(temp, loc);
      NUContAssign *assign = new NUContAssign(block_name, lhs, actual, loc);
      module->addContAssign(assign);
    }
  }

  NUExpr * real_actual = new NUIdentRvalue(temp, loc);
  real_actual->resize(temp->getBitSize());
  return real_actual;
}

NUExpr * LowerPortConnections::lower(NUModule * module,
                                     NUExpr * actual, 
                                     NUNet * formal_net,
                                     const SourceLocator loc)
{
  UInt32 bits = formal_net->getBitSize();
  // Look to see if we already have a continuous assignment of this value
  // to a lhs within this module - if so, reuse the location.
  NUNet *temp = mReuseAssigns ? reuseTempAssign (module, actual, bits): NULL;

  if (not temp) {
    StringAtom *block_name = module->newBlockName(mStrCache, loc);
    StringAtom* netsym = module->gensym("lower", NULL, actual);
    bool netSign = (actual and actual->isSignedResult());
    temp = module->createTempNet(netsym, bits, netSign, loc);
    // declare as a wire so we get initialization correct.
    temp->setFlags(NetRedeclare(temp->getFlags(),eDMWireNet));

    if (actual) {
      // This is a connected port; create an assignment to the
      // original actual. For disconnected ports, we leave
      // _temp_ disconnected.
      NULvalue *lhs = new NUIdentLvalue(temp, loc);
      NUContAssign *assign = new NUContAssign(block_name, lhs, actual, loc);
      module->addContAssign(assign);
    }
  }

  NUExpr * real_actual = new NUIdentRvalue(temp, loc);
  real_actual->resize(temp->getBitSize());
  return real_actual;
}

void LowerPortConnections::output(NUPortConnectionOutput * outport,
				  FlattenQualifyPort &qualifier)
{
  NULvalue * actual = outport->getActual();
  NUNet    * formal = outport->getFormal();

  if (not qualify(actual, formal, qualifier)) {
    return;
  }

  // lower disconnected ports or in situations where full-net
  // aliasing is impossible.
  if ( not outport->isAliasable() ) {
    
    NULvalue * real_actual = lower(mModule, actual, formal, outport->getLoc());
    outport->setActual(real_actual);
  }
}

NULvalue * LowerPortConnections::lower(NUModule * module,
                                       NULvalue * actual,
                                       UInt32 bits,
                                       const SourceLocator & loc)
{
  StringAtom *block_name = module->newBlockName(mStrCache, loc);
  StringAtom* sym = module->gensym("lower", NULL, actual);
  bool netSign = (actual and actual->isWholeIdentifier() and
                  actual->getWholeIdentifier()->isSigned());
  NUNet * temp = module->createTempNet(sym, bits, netSign, loc);
  // declare as a wire so we get initialization correct.
  temp->setFlags(NetRedeclare(temp->getFlags(),eDMWireNet));

  if (actual != NULL)
  {
    NUExpr *rhs = new NUIdentRvalue(temp, loc);
    NUContAssign *assign = new NUContAssign(block_name, actual, rhs, loc);
    module->addContAssign(assign);
  }

  NULvalue * real_actual = new NUIdentLvalue(temp, loc);
  real_actual->resize();
  return real_actual;
}


NULvalue * LowerPortConnections::lower(NUModule * module,
                                       NULvalue * actual,
                                       NUNet * formal_net,
                                       const SourceLocator & loc)
{
  StringAtom *block_name = module->newBlockName(mStrCache, loc);
  NUNet * temp = NULL;
  if(actual == NULL)
  {
    temp = module->createTempNetFromImage(formal_net, "lower", true, false);
    // declare as a wire so we get initialization correct.
    if(temp->isMemoryNet())
      temp->setFlags(NetRedeclare(temp->getFlags(),eDMWire2DNet));
    else
      temp->setFlags(NetRedeclare(temp->getFlags(),eDMWireNet));
  }
  else
  {
    StringAtom* sym = module->gensym("lower", NULL, actual);
    UInt32 bits = actual->getBitSize();
    bool netSign = (actual and actual->isWholeIdentifier() and
                  actual->getWholeIdentifier()->isSigned());
    temp = module->createTempNet(sym, bits, netSign, loc);
    // declare as a wire so we get initialization correct.
    temp->setFlags(NetRedeclare(temp->getFlags(),eDMWireNet));
  }

  if (actual != NULL)
  {
    NUExpr *rhs = new NUIdentRvalue(temp, loc);
    NUContAssign *assign = new NUContAssign(block_name, actual, rhs, loc);
    module->addContAssign(assign);
  }

  NULvalue * real_actual = new NUIdentLvalue(temp, loc);
  real_actual->resize();
  return real_actual;
}


void LowerPortConnections::bid(NUPortConnectionBid * biport,
			       FlattenQualifyPort &qualifier)
{
  NULvalue * actual = biport->getActual();
  NUNet    * formal = biport->getFormal();

  NULvalue * real_actual = actual;

  if (not qualify(actual, formal, qualifier)) {
    return;
  }

  if ( actual and not biport->isAliasable() ) {
    mMsgContext->ComplexBid(actual);
    real_actual = NULL;
    delete actual;
  }

  // lower disconnected ports.
  if (not actual) {
    // Josh -- we may have to tell ::lower to flag the generated
    // temporary as eDMWireNet so we properly initialize it.
    // See the similar code in the ::lower for NUExpr*.
    // This is only meaningful for the inout. In the output
    // case, the formal will be driving; we need nothing locally.
    real_actual = lower(mModule, actual, formal, biport->getLoc());
  }

  if (real_actual != actual) {
    biport->setActual(real_actual);
  }
}


bool LowerPortConnections::qualify(NUExpr * actual,
				   NUNet *formal,
				   FlattenQualifyPort &qualifier)
{
  if (mLowerEverything) {
    return true;
  }

  return not qualifier.qualify(actual, formal);
}


bool LowerPortConnections::qualify(NULvalue * actual,
				   NUNet *formal,
				   FlattenQualifyPort &qualifier)
{
  if (mLowerEverything) {
    return true;
  }

  return not qualifier.qualify(actual, formal);
}


bool LowerPortConnections::structuredProc(NUStructuredProc * proc)
{
  NUStmtList dummy;
  return block(proc->getBlock());
}

bool LowerPortConnections::tf(NUTF* tf)
{
  NUStmtList stmts;
  bool modified = lower(tf->loopStmts(), tf, stmts);
  if (modified)
    tf->replaceStmtList(stmts);
  return modified;
}

bool LowerPortConnections::block(NUBlock * block)
{
  NUStmtList stmts;
  bool success = lower(block->loopStmts(),block,stmts);
  if (success) {
    block->replaceStmtList(stmts);
  }
  return false;
}

void LowerPortConnections::preCallTaskEnable(NUTaskEnable*)
{}

bool LowerPortConnections::lower(NUStmt * stmt, 
				 NUScope * scope, 
				 NUStmtList & pre_stmts,
				 NUStmtList & post_stmts) 
{
  // handle the stmt types we want to recurse into; for others, return NULL.
  NUBlock *oneBlock = dynamic_cast<NUBlock*>(stmt);
  if (oneBlock) {
    return block(oneBlock);
  }

  NUIf *oneIf = dynamic_cast<NUIf*>(stmt);
  if (oneIf) {
    return ifStmt(oneIf,scope);
  }

  NUCase *oneCase = dynamic_cast<NUCase*>(stmt);
  if (oneCase) {
    return caseStmt(oneCase,scope);
  }

  NUFor *oneFor = dynamic_cast<NUFor*>(stmt);
  if (oneFor) {
    return forStmt(oneFor,scope);
  }

  NUCModelCall *oneCmodelCall = dynamic_cast<NUCModelCall*>(stmt);
  if (oneCmodelCall) {
    return call<NUCModelCall,
      NUCModelArgConnectionLoop,
      NUCModelArgConnection,
      NUCModelArgConnectionInput,
      NUCModelArgConnectionOutput,
      NUCModelArgConnectionBid,
      NUCModelPort, true>
      (oneCmodelCall,scope,pre_stmts,post_stmts);
  }

  NUTaskEnable *oneTaskEnable = dynamic_cast<NUTaskEnable*>(stmt);
  if (oneTaskEnable) {
    preCallTaskEnable(oneTaskEnable);

    return call<NUTaskEnable,
      NUTFArgConnectionLoop,
      NUTFArgConnection,
      NUTFArgConnectionInput,
      NUTFArgConnectionOutput,
      NUTFArgConnectionBid,
      NUNet, false>
      (oneTaskEnable,scope,pre_stmts,post_stmts);
  }

  return false;
}


bool LowerPortConnections::ifStmt(NUIf * stmt, NUScope * scope)
{
  NUStmtList stmts;
  bool result;
  result = lower(stmt->loopThen(), scope, stmts);
  if (result) {
    stmt->replaceThen(stmts);
  }
  stmts.clear();

  result = lower(stmt->loopElse(), scope, stmts);
  if (result) {
    stmt->replaceElse(stmts);
  }
  stmts.clear();
  return false;
}


bool LowerPortConnections::caseStmt(NUCase * stmt, NUScope * scope)
{
  for (NUCase::ItemLoop loop = stmt->loopItems(); 
       not loop.atEnd() ;
       ++loop) {
    NUCaseItem *item = (*loop);

    NUStmtList stmts;
    bool result = lower(item->loopStmts(), scope, stmts);
    if (result) {
      item->replaceStmts(stmts);
    }
  }
  return false;
}


bool LowerPortConnections::forStmt(NUFor * stmt, NUScope * scope)
{
  NUStmtList stmts;
  bool result;
  result = lower(stmt->loopBody(), scope, stmts);
  if (result) {
    stmt->replaceBody(stmts);
  }
  stmts.clear();
  return false;
}

template <typename ConnType> bool sDoLowerInput(ConnType* in_conn)
{
  bool ret = false;
  NUExpr     * actual = in_conn->getActual();
  UInt32 formal_size = in_conn->getBitSize();
  if ((not actual->isWholeIdentifier()) or
      (actual->getBitSize() != formal_size)) {
    // 2D is OK here because if the types are otherwise compatible and the
    // formal is not a memory a call to the memory read method is generated.
    ret = true;
  }
  return ret;
}

template <typename ConnType> bool sDoLowerBidOrOut(ConnType* out_conn)
{
  bool ret = false;
  NULvalue * actual = out_conn->getActual();
  UInt32 formal_size = out_conn->getBitSize();
  if ((not actual->isWholeIdentifier()) or
    (actual->getBitSize() != formal_size) or
    actual->is2DAnything ()) {
    // Always lower if the actual parameter is a memory. In some instances, for
    // example in VHDL when the formal is also a memory, lowering is not
    // necessary. However, it's OK to lower these too because if the formal and
    // actual are identically typed then assignment aliasing will undo the
    // lowering. If the formal is not a memory then this is the right thing,
    // even if the actual is a whole identifier (see bug8535). -m
    ret = true;
  }
  return ret;
}


bool LowerPortConnections::doLowerInput(NUTFArgConnectionInput* in_conn)
{
  return sDoLowerInput(in_conn);
}

bool LowerPortConnections::doLowerOutput(NUTFArgConnectionOutput* out_conn)
{
  return sDoLowerBidOrOut(out_conn);
}

bool LowerPortConnections::doLowerBid(NUTFArgConnectionBid* bid_conn)
{
  return sDoLowerBidOrOut(bid_conn);
}

bool LowerPortConnections::doLowerInput(NUCModelArgConnectionInput* in_conn)
{
  return sDoLowerInput(in_conn);
}

bool LowerPortConnections::doLowerOutput(NUCModelArgConnectionOutput* out_conn)
{
  return sDoLowerBidOrOut(out_conn);
}

bool LowerPortConnections::doLowerBid(NUCModelArgConnectionBid* bid_conn)
{
  return sDoLowerBidOrOut(bid_conn);
}


template<class CallType,
	 class ArgLoopType,
         class ArgType,
	 class InputArgType,
	 class OutputArgType,
	 class BidArgType,
	 class FormalType,
	 bool LowerInputs>
bool LowerPortConnections::call(CallType * call, 
				NUScope * scope, 
				NUStmtList & pre_stmts,
				NUStmtList & post_stmts)
{
  bool anyLowered = false;

  const SourceLocator & loc = call->getLoc();

  for (ArgLoopType loop = call->loopArgConnections();
       not loop.atEnd();
       ++loop) {
    ArgType * connection = (*loop);
    InputArgType  * in_conn = dynamic_cast<InputArgType*>(connection);
    OutputArgType * out_conn = dynamic_cast<OutputArgType*>(connection);
    BidArgType    * bid_conn = dynamic_cast<BidArgType*>(connection);

    bool lowerArg = false;
    if (in_conn) {
      if (LowerInputs) {
        lowerArg = doLowerInput(in_conn);
        if (lowerArg)
        {
          NUNet *temp = NULL;
          NUExpr *actual = in_conn->getActual();
          UInt32 formal_size = in_conn->getBitSize();
           
          StringAtom* sym = scope->gensym("lower", NULL, actual);
          temp = scope->createTempNet(sym, formal_size, 
                                      in_conn->isSigned(), loc);

          // create assignment of temp pre-call.
          NULvalue *lhs = new NUIdentLvalue(temp, loc);
          NUBlockingAssign * assign = new NUBlockingAssign(lhs,actual,false,loc);
          pre_stmts.push_back(assign);

          // rvalue to use as actual.
          NUExpr * new_actual = new NUIdentRvalue(temp, loc);
          new_actual->resize(formal_size);
          in_conn->setActual(new_actual);
	}
      }
    } else {
      NU_ASSERT (out_conn or bid_conn, connection);
      NULvalue   * actual = NULL;
      bool formalIsSigned = false;
      UInt32     formal_size = 0;
      if (out_conn) {
	actual = out_conn->getActual();
	formal_size = out_conn->getBitSize();
        formalIsSigned = out_conn->isSigned();
        lowerArg = doLowerOutput(out_conn);
      } else {
	actual = bid_conn->getActual();
	formal_size = bid_conn->getBitSize();
        formalIsSigned = bid_conn->isSigned();
        lowerArg = doLowerBid(bid_conn);
      }
      NU_ASSERT(actual!=NULL, connection);
      NU_ASSERT(formal_size!=0, connection);
      
      if (lowerArg) {
        NUNet * temp = NULL;
        if (actual->isWholeIdentifier() 
          && actual->getWholeIdentifier()->is2DAnything()
          && connection->getFormal ()->is2DAnything ()) {
          // createTempNetFromImage will construct a memory net if the
          // prototype net (in this case coming from the actual) is a memory
          // net. This is only correct if both the formal and the actual are
          // memories (see bug8535) -m
          temp = scope->createTempNetFromImage( actual->getWholeIdentifier(), "lower", true);
        } else {
          // for cases where actual is not a net
          StringAtom* sym = scope->gensym("lower", NULL, actual);
          temp = scope->createTempNet(sym, formal_size, formalIsSigned, loc);
        }

	if (bid_conn) {
	  // create assign of temp pre-call.
	  NUIdentLvalue * lhs = new NUIdentLvalue(temp,loc);
	  NUExpr        * rhs = actual->NURvalue();
	  NUBlockingAssign * assign = new NUBlockingAssign(lhs,rhs,false,loc);
	  pre_stmts.push_back(assign);
	}
        
	// create assign of actual post-call.
        NUExpr * rhs = new NUIdentRvalue(temp, loc);
	NUBlockingAssign * assign = new NUBlockingAssign(actual,rhs,false,loc);
	post_stmts.push_back(assign);

	// lvalue to use as actual.
	NULvalue * new_actual = new NUIdentLvalue(temp,loc);
	new_actual->resize();

	// update the connection.
	if (out_conn) {
	  out_conn->setActual(new_actual);
	} else {
	  bid_conn->setActual(new_actual);
	}
      }
    }
    anyLowered = anyLowered || lowerArg;
  }
  return anyLowered;
}


bool LowerPortConnections::lower(NUStmtLoop loop, NUScope * scope, NUStmtList & stmts)
{
  bool success = false;
  for (; not loop.atEnd();
       ++loop) {
    NUStmt * stmt = (*loop);
    NUStmtList pre_stmts;
    NUStmtList post_stmts;
    success |= lower(stmt,scope,pre_stmts,post_stmts);

    stmts.insert(stmts.end(),pre_stmts.begin(),pre_stmts.end());
    stmts.push_back(stmt);
    stmts.insert(stmts.end(),post_stmts.begin(),post_stmts.end());
  }
  return success;
}
