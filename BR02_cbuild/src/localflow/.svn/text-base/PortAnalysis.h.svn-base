/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef PORTANALYSIS_H_
#define PORTANALYSIS_H_

#include "nucleus/NUNetRef.h"

//! Interface for common analysis methods used by both port coercion and port splitting.
class PortAnalysis
{
public:
  //! Constructor
  PortAnalysis(NUNetRefFactory *net_ref_factory);

  //! Destructor
  ~PortAnalysis();

  //! Does this net have a strong, continuous driver?
  /*!
   * A net has a strong continuous driver if it has any strong non-Z
   * driver over the full net.
   *
   * Deposits/forces and hierarchical writes do not imply that a net
   * has a continuous, strong driver.
   */
  bool hasStrongContDriver(const NUNetRefHdl point, bool include_ports=false) const;

private:
  //! The net ref factory
  NUNetRefFactory * mNetRefFactory;
};

#endif
