// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/Elaborate.h"
#include "LFContext.h"
#include "symtab/STSymbolTable.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUHierRef.h"
#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"
#include "compiler_driver/CarbonDBWrite.h"


void Elaborate::elabNet(NUNet *net, LFContext *context)
{
  STSymbolTable* symtab = context->getSymbolTable();
  STBranchNode* hier = context->getHier();
  if (not hier->hasChild(net->getSymtabIndex())) {
    net->createElab(hier, symtab);
  }
}


void Elaborate::elabNetHierRef(NUNet *net, LFContext *context)
{
  NU_ASSERT(net->isHierRef(), net);

  STSymbolTable* symtab = context->getSymbolTable();
  STBranchNode* parent_hier = context->getHier();

  // Create a local symbol table entry of this hierref.
  ST_ASSERT(not parent_hier->hasChild(net->getSymtabIndex()), parent_hier);
  net->createElab(parent_hier, symtab);

  // Resolve the hierref.  Find the elaborated net which is referenced.
  STSymbolTableNode *resolved_node;
  NUHierRef* hier_ref = net->getHierRef();
  NU_ASSERT(hier_ref, net);
  resolved_node = NUHierRef::resolveHierRef(hier_ref->getPath(), 0,
                                            symtab, parent_hier);

  // I don't believe this can occur; Cheetah should catch these.  Just in case...
  if (not resolved_node) {
    mMsgContext->CannotResolveNetHierRef(net, parent_hier->str());
    return;
  }

  // Get the resolved net for aliasing
  STAliasedLeafNode *master = resolved_node->castLeaf()->getMaster();
  NUBase* base = CbuildSymTabBOM::getNucleusObj(master);
  NUNetElab *resolved_net = dynamic_cast<NUNetElab*>(base);

  // Get the resolved net's storage net. We have to do this before
  // aliasing because aliasing will choose the hier ref as the storage
  // net. And we want to override it.
  STAliasedLeafNode* storage_node = resolved_net->getStorageSymNode();

  // Now alias the local net with the resolved entry.
  NUNetElab *local_net = net->lookupElab(parent_hier);
  local_net->alias(resolved_net);

  // Propagate tristate flags.
  NUNet *resolved_net_unelab = resolved_net->getNet();
  if (net->isZ()) {
    // resolved_net_unelab->putIsZ(true); JDM: isZ is computed now, not stored
    if (resolved_net_unelab->isPrimaryBid()) {
      resolved_net_unelab->putIsPrimaryZ(true);
      net->putIsPrimaryZ(true);
    }
  }

  // After aliasing is done, we don't need the local net, so delete it.
  delete local_net;

  // Now that aliasing is done, we can override what it did and set
  // the resolved net's storage net as the true storage.
  storage_node->setThisStorage();
}


void Elaborate::elabNetReconData(NUNet *net, LFContext *context)
{
  // Only use the storage net in each unelaborated alias ring as a
  // starting point. This ensures that we only process each ring once
  // per elaboration.
  if (net != net->getStorage()) {
    return;
  }

  // if any element in the unelaborated alias ring has reconstruction
  // data, translate into an elaborated form
  STAliasedLeafNode * master = net->getMasterNameLeaf();
  STAliasedLeafNode * alias  = master;
  do {
    elabLeafReconData(alias, context);
  } while ( (alias = alias->getAlias()) != master );

}
