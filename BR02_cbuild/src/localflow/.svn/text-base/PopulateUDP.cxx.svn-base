// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/TicProtectedNameManager.h"
#include "localflow/VerilogPopulate.h"
#include "LFContext.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "util/CbuildMsgContext.h"
#include "util/SetOps.h"

//! Make an Lvalue for the output of a udp.
Populate::ErrorCode VerilogPopulate::udpMakeLhs( 
  veNode ve_udp, 
  LFContext* context,
  NUIdentLvalue** lhs )  // Return the new Lvalue for the UDP output
{
  ErrorCode err_code = eSuccess;
  ErrorCode ec;

  CheetahList portList(veUDPGetPortList( ve_udp ));
  veNode port = veListGetNextNode( portList );
  ec = identLvalue( vePortGetNetExpr(port), context, context->getModule(), lhs );
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) ) {
    return ec;
  }
  
  return err_code;
}

//! Return true if a level list has any 'x' entries.
bool VerilogPopulate::udpLevelListHasX( CheetahList& levelList )
{
  while( veNode level = veListGetNextNode( levelList ) ) {
    veLevelSymbolType levelType = veLevelSymbolGetSymbol( level );
    if( levelType == LEVEL_X )
      return true;
  }
  veListRewind( levelList );
  return false;
}

//! Summarize the edge type for a UDP.
//! UDP edges have two types: single symbol and two symbol.
//! Analyze the edge symbols and summarize them into a single enum.
VerilogPopulate::udpEdgeType VerilogPopulate::udpGetEdgeType( veNode inputList )
{
  udpEdgeType edge_type = udpPosedge;
  veNode edge = veUdpEdgeInputListGetEdge( inputList );
  if( veNodeGetObjType ( edge ) == VE_EDGESYMBOLTYPE1 ) {
    veNode symbol1 = veEdgeSymbolType1GetLevelSymbol1( edge );
    veLevelSymbolType level1 = veLevelSymbolGetSymbol( symbol1 );
    
    veNode symbol2 = veEdgeSymbolType1GetLevelSymbol2( edge );
    veLevelSymbolType level2 = veLevelSymbolGetSymbol( symbol2 );
    if( level1 == LEVEL_B)
      level1 = LEVEL_UNKNOWN;
    if( level2 == LEVEL_B)
      level2 = LEVEL_UNKNOWN;
      
    if( level1 == LEVEL_X || level2 == LEVEL_X ) {
      edge_type = udpX;
    } else if( (level1 == LEVEL_ZERO && level2 == LEVEL_ZERO) ||
               (level1 == LEVEL_ZERO && level2 == LEVEL_UNKNOWN) ||
	       (level1 == LEVEL_UNKNOWN && level2 == LEVEL_ZERO) ) {
      edge_type = udpLevelLow;
    } else if( (level1 == LEVEL_ONE && level2 == LEVEL_ONE) ||
               (level1 == LEVEL_ONE && level2 == LEVEL_UNKNOWN) ||
	       (level1 == LEVEL_UNKNOWN && level2 == LEVEL_ONE) ) {
      edge_type = udpLevelHigh;
    } else if( level1 == LEVEL_ZERO && level2 == LEVEL_ONE ){
      edge_type = udpPosedge;
    } else if( level1 == LEVEL_ONE && level2 == LEVEL_ZERO ){
      edge_type = udpNegedge;
    } else {
      edge_type = udpDontCare;
    }
  } else {
    veEdgeSymbolType edgeSymbol = veEdgeSymbolType2GetSymbol( edge );
    switch( edgeSymbol ) {
      case RISING_EDGE:
      case POSITIVE_EDGE:
        edge_type = udpPosedge;
	break;
	
      case FALLING_EDGE:
      case NEGATIVE_EDGE:
        edge_type = udpNegedge;
	break;
	
      case ALL_EDGE:
        edge_type = udpDontCare;
	break;
	
      case UNKNOWN_EDGE:
        edge_type = udpX;
	break;
    }
  }
  
  return edge_type;
}

//! Add a term to the current product.
Populate::ErrorCode  VerilogPopulate::udpAddTermToProduct( 
  LFContext* context, //
  NUExpr** product,   // Product of literals from previous calls
  veNode port,        // port for the net to be used as a literal
  bool invert )       // Whether to invert this literal before anding it in.
{
  ErrorCode err_code = eSuccess;
  NUExpr* term;
  ErrorCode ec = identRvalue( vePortGetNetExpr(port), context,
                                context->getModule(), &term );
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) ) {
    return ec;
  }
  
  SourceLocator loc = locator( port );
  
  if( invert) 
    term = new NUUnaryOp( NUOp::eUnLogNot, term, loc );

  // Add the term to the product.
  if( (*product) )
    (*product) = new NUBinaryOp( NUOp::eBiBitAnd, (*product), term, loc );
  else
    (*product) = term;
  return eSuccess;
}


//! \return iff the level inputs contain an X
bool VerilogPopulate::udpLevelsContainX (veNode inputList) const
{
  CheetahList levelList (veUdpEdgeInputListGetUdpLevelInputList1( inputList ));
  veNode level = veListGetNextNode (levelList);
  while (level) {
    if (veLevelSymbolGetSymbol( level ) == LEVEL_X) {
      return true;
    }
    level = veListGetNextNode (levelList);
  }
  return false;
}

//! Return the product of level inputs in a UDP table.
Populate::ErrorCode VerilogPopulate::udpLevelEntryProduct( 
  CheetahList& levelList,   // List of table entry levels
  CheetahList& portList,    // List of inputs queued to be in sync with the levelList    
  LFContext* context, // 
  NUExpr** product )  // Returned product expression
{
  ErrorCode err_code = eSuccess;
  while( veNode level = veListGetNextNode( levelList ) ) {
    SourceLocator loc = locator( level );
    veNode port = veListGetNextNode( portList );
    LOC_ASSERT( port, loc );
    veLevelSymbolType levelType = veLevelSymbolGetSymbol( level );
    // Entries with 'x' are ignored altogether.
    if( levelType == LEVEL_X ) {
      if( (*product) ) {
	delete (*product);
	(*product) = NULL;
      }
      break;
    }
    // Skip don't care inputs
    if( levelType == LEVEL_B || levelType == LEVEL_UNKNOWN )
      continue;
    
    ErrorCode ec = udpAddTermToProduct( context, product, port,
                                        (levelType == LEVEL_ZERO) );
    if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) ) {
      if( (*product) )  {
        delete (*product);
        *product = NULL;
      }
      return ec;
    }
  }
  if( (*product) )
    (*product)->resize( 1 );

  return eSuccess;
}


//! Parse an edge entry of a sequential UDP and update the expressions.
Populate::ErrorCode VerilogPopulate::udpEdgeEntry( 
   veNode entry,        // The entry to be analyzed
   veNode inputList,    // Input list for the entry
   CheetahList& portList,     // Portlist (queued to start at inputs)
   LFContext* context,  //
   veNode* clkNode,     // Saved clock from a previous call to check consistency
   NUExpr** clock,      // Return a new clock expr if not created already
   udpEdgeType* edge,   // Saved edge from a previous call to check for consistency
   NUExpr** enable,     // Returned expr representing additional level conditions of
                        // the entry.
   bool* isValidEntry ) // Returned true if no 'x' levels were found.
{
  ErrorCode err_code = eSuccess;
  CheetahList list1(veUdpEdgeInputListGetUdpLevelInputList1( inputList ));
  CheetahList list2(veUdpEdgeInputListGetUdpLevelInputList2( inputList ));
  CheetahList qList(vewCreateNewList());
  vewAppendToList( qList, veSeqEntryGetCurrentState( entry ) );
  udpEdgeType edgeType = udpGetEdgeType( inputList );

  // Ignore entries with 'x'.
  if( udpLevelListHasX( list1 ) || udpLevelListHasX( list2 ) ||
      udpLevelListHasX( qList )  || edgeType == udpX ) {
    (*isValidEntry) = false;
    return eSuccess;
  }
  (*isValidEntry) = true;

  ErrorCode ec = udpLevelEntryProduct( list1, portList, context, enable);
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
    return ec;
    
  veNode newClkNode = veListGetNextNode( portList );
  // Verify that we only have one input that looks like a clock.
  INFO_ASSERT( newClkNode, "No clock found in UDB edge list" );
  if( !(*clkNode) ) {
    (*clkNode) = newClkNode;
    ec = identRvalue( vePortGetNetExpr(newClkNode), context, context->getModule(), clock );
    if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) ) {
      return ec;
    }
  }
  if( (*clkNode) != newClkNode ) {
    SourceLocator loc = locator( newClkNode );
    POPULATE_FAILURE(mMsgContext->UDPEdgeError( &loc, context->getModule()->getName()->str(),
                                                " has multiple inputs with edge entries" ), &err_code);
    return err_code;
  }
  // Verify that it is on the same edge
  if( (*edge) == udpDontCare )
    (*edge) = edgeType;
  if( (*edge) != edgeType ) {
    SourceLocator loc = locator( newClkNode );
    POPULATE_FAILURE(mMsgContext->UDPEdgeError( &loc, context->getModule()->getName()->str(),
                                                " has edge entries for both posedge and negedge" ), &err_code);
    return err_code;
  }

  ec = udpLevelEntryProduct( list2, portList, context, enable );
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
    return ec;
    
  // Rewind the portlist back to 'Q'.  Add the current state to the enable.
  veListRewind( portList );
  ec = udpLevelEntryProduct( qList, portList, context, enable );
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
    return ec;

  return eSuccess;
}

//! Determine if the expression is a binary and.  Return the sub-expressions.
//! Also, the sub-expressions must be idents or inversions of idents.
bool VerilogPopulate::udpIsBinaryAnd( NUExpr* expr,
                                      NUNet** net1, NUExpr** expr1,
                                      NUNet** net2, NUExpr** expr2 )
{
  if( expr->getType() != NUExpr::eNUBinaryOp )
    return 0;

  NUBinaryOp* op = dynamic_cast<NUBinaryOp*>(expr);
  if( op->getOp() != NUOp::eBiBitAnd )
    return 0;
  
  *expr1 = op->getArg(0);
  *expr2 = op->getArg(1);

  NUExpr* ident1 = *expr1;
  if( (*expr1)->getType() == NUExpr::eNUUnaryOp ) {
    NUUnaryOp* op = dynamic_cast<NUUnaryOp*>(*expr1);
    ident1 = op->getArg(0);
  }

  NUExpr* ident2 = *expr2;
  if( (*expr2)->getType() == NUExpr::eNUUnaryOp ) {
    NUUnaryOp* op = dynamic_cast<NUUnaryOp*>(*expr2);
    ident2 = op->getArg(0);
  }

  if( (ident1->getType() != NUExpr::eNUIdentRvalue) ||
      (ident2->getType() != NUExpr::eNUIdentRvalue) )
    return 0;

  *net1 = ident1->getWholeIdentifier();
  *net2 = ident2->getWholeIdentifier();

  return 1;
}


//! Examine set/clear level expresssions.  Look to see if the expressions
//! fit the following template:
//!   set = data & enable
//!   clear = !data & enable
//! The template also matches commuted expressions and inversions of
//! data and enable.
//! If the template matches then create enable/data statements instead of
//! set/clear.
//! Stmt is updated to be:
//!   if( enable )
//!     udpOut = data;
//! If set/clear do not match the template, return a null enable expression,
//! and do not add any new statement.
bool VerilogPopulate::udpAddLevelLatchStatement( 
    veNode ve_udp,       //
    LFContext* context,  //
    NUStmt** stmt,       // Current statement.  Also the returned updated statement.
    NUExpr* setExpr,     // Set expression.
    NUExpr* clearExpr)   // Clear expression.
{
  if( !setExpr || !clearExpr )
    return 0;

  NUExpr *exprA1, *exprA2, *exprB1, *exprB2;
  NUNet *netA1, *netA2, *netB1, *netB2;

  if( ! udpIsBinaryAnd( setExpr, &netA1, &exprA1, &netA2, &exprA2 ) )
    return 0;
  if( ! udpIsBinaryAnd( clearExpr, &netB1, &exprB1, &netB2, &exprB2) )
    return 0;

  // If the nets are not the same, swap them.
  if( netA1 != netB1 ) {
    NUNet* net = netB1;
    NUExpr* expr = exprB1;
    netB1 = netB2;
    exprB1 = exprB2;
    netB2 = net;
    exprB2 = expr;
  }
  // The nets have to match.
  if( netA1 != netB1 || netA2 != netB2 )
    return 0;

  // If the inversions match, 1 is the enable.
  // Swap to make 1 the data and 2 the enable.
  if( exprA1->getType() == exprB1->getType() ) {
    NUNet* net = netA1;
    NUExpr* expr = exprA1;
    netA1 = netA2;
    exprA1 = exprA2;
    netA2 = net;
    exprA2 = expr;

    net = netB1;
    expr = exprB1;
    netB1 = netB2;
    exprB1 = exprB2;
    netB2 = net;
    exprB2 = expr;
  }

  // The data expressions must be inverses to match the template.
  if( exprA1->getType() == exprB1->getType() )
    return 0;

  // The set/clear expression match the template.
  // Create the data/enable statement.
    
  ErrorCode err_code = eSuccess;
  NUModule* module = context->getModule();
  const SourceLocator& loc = module->getLoc();
  
  NUIdentLvalue* lhs;
  VerilogPopulate::ErrorCode ec = udpMakeLhs( ve_udp, context, &lhs );
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
    return 0;

  CopyContext cc(0,0);
  NUStmt* thenStmt = new NUBlockingAssign( lhs, exprA1->copy(cc), false, loc );
  NUStmtList thenList;
  thenList.push_back( thenStmt );
  NUStmtList elseList;
  // For a simple latch, we should not see any current statement.
  NU_ASSERT( *stmt == NULL, *stmt );

  (*stmt) = new NUIf( exprA2->copy(cc), thenList, elseList, mNetRefFactory,
                      false, loc );

  // The set/clear exprs are replaced with data/enable.  Delete them.
  delete setExpr;
  delete clearExpr;
      
  return 1;
}


//! For a level set or clear expression, create a continuous assign and
//! add a conditional statement over the current statement.
//! Example:
//! Cont. assign added to module:
//!   assign lhsName = expr;
//!
//! Stmt is updated to be:
//!   if( lhsName )
//!     udpOut = value
//!   else
//!     stmt;
NUExpr* VerilogPopulate::udpAddLevelStatement( 
    veNode ve_udp,       //
    LFContext* context,  //
    NUStmt** stmt,       // Current statement.  Also the returned updated statement.
    NUExpr* expr,        // Set or clear expression.
    int value,           // Value to be assigned (1=set, 0=clear)
    const char* lhsName )// Name to use if the expression is not already an ident.
{
  if( !expr )
    return NULL;
    
  ErrorCode err_code = eSuccess;
  NUModule* module = context->getModule();
  const SourceLocator& loc = module->getLoc();
  NUExpr* identExpr = NULL;
  
  // Create a continuous assign if the expr is not an ident.
  if( expr->getType() != NUExpr::eNUIdentRvalue ) {
    StringAtom* sym = module->gensym( lhsName );
    NUNet* exprNet = module->createTempNet(sym, 1, false, loc);
    NUIdentLvalue* exprLhs = new NUIdentLvalue( exprNet, loc );
    NUContAssign* newAssign = helperPopulateContAssign( exprLhs, expr, loc,
                                                        context );
    module->addContAssign( newAssign );
    identExpr = new NUIdentRvalue( exprNet, loc );
  } else {
    identExpr = expr;
  }
  
  NUIdentLvalue* lhs;
  VerilogPopulate::ErrorCode ec = udpMakeLhs( ve_udp, context, &lhs );
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
    return NULL;
  NUExpr* rhs = NUConst::create( false, value,  1, loc );
  NUStmt* thenStmt = new NUBlockingAssign( lhs, rhs, false, loc );
  NUStmtList thenList;
  thenList.push_back( thenStmt );
  NUStmtList elseList;
  if( (*stmt) )
    elseList.push_back( (*stmt) );

  (*stmt) = new NUIf( identExpr, thenList, elseList, mNetRefFactory, false, loc );
      
  return identExpr;
}


//! Create an always block to represent the sequential UDP.
Populate::ErrorCode VerilogPopulate::udpAlways(
  veNode ve_udp,
  LFContext* context,
  NUExpr* clock,
  udpEdgeType edge,
  NUExpr* edgeEnable,
  NUExpr* edgeData,
  NUExpr* levelSet,
  NUExpr* levelClear  )
{
  ErrorCode err_code = eSuccess;
  ErrorCode ec;
  NUModule* module = context->getModule();
  const SourceLocator& loc = module->getLoc();

  if( !clock && !levelSet && !levelClear ) {
    POPULATE_FAILURE(mMsgContext->NoUDPOutputExpr( &loc ), &err_code);
    return err_code;
  }

  // Check to see if the edge nets intersect the level nets.
  // If so, the recognition is probably not optimal.  Fix for
  // bug 5997.
  NUNetSet clockUse;
  if( clock)
    clock->getUses( &clockUse );

  NUNetSet levelUse;
  if( levelSet )
    levelSet->getUses( &levelUse );
  if( levelClear )
    levelClear->getUses( &levelUse );

  NUNetSet intersection;
  set_intersection( clockUse, levelUse, &intersection);
  if( !intersection.empty() ) {
    mMsgContext->udpClockUseWarning( &loc );
  }
    

    
  // Make the statement to insert in the always.
  NUStmt* newStmt = NULL;
  if( edgeData ) {
    NUIdentLvalue* lhs;
    ec = udpMakeLhs( ve_udp, context, &lhs );
    if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
      return ec;
    newStmt = new NUBlockingAssign( lhs, edgeData, false, loc );
    if( edgeEnable ) {
      NUStmtList thenList;
      thenList.push_back( newStmt );
      NUStmtList elseList;
      newStmt = new NUIf( edgeEnable, thenList, elseList, mNetRefFactory, false, loc );
    }
  }
  // Look for simple latches as a special case.
  // If we find a simple latch, then use data/enable logic instead of set/clear
  // Fix for bug 5240.
  bool simpleLatch = 0;

  if( !clock ) {
    simpleLatch = udpAddLevelLatchStatement( ve_udp, context,
                                             &newStmt, levelSet, levelClear);
  }

  NUExpr* levelClearIdent = NULL;
  NUExpr* levelSetIdent = NULL;
  if( !simpleLatch ) {
    levelClearIdent = udpAddLevelStatement( ve_udp, context, &newStmt,
                                            levelClear, 0, "udpClear");
    levelSetIdent = udpAddLevelStatement( ve_udp, context, &newStmt,
                                          levelSet, 1, "udpSet");
  }
  NUStmtList newStmtList;
  newStmtList.push_back( newStmt );
  
  NUScope* parent_scope = context->getModule();
  NUBlock* block = new NUBlock( &newStmtList, 
                                parent_scope, 
                                mIODB,
                                mNetRefFactory, 
                                false, 
                                loc );
  
  StringAtom* blockName = context->getModule()->newBlockName( context->getStringCache(), loc );
  NUAlwaysBlock* always = new NUAlwaysBlock( blockName, block, mNetRefFactory, loc, false );
  module->addAlwaysBlock( always );
  
  // Add edge expressions for clk and async set/reset
  if( clock ) {
    ClockEdge edgyEdge = (edge == udpPosedge) ? eClockPosedge : eClockNegedge;
    NUEdgeExpr* clkEdge = new NUEdgeExpr( edgyEdge, clock, loc );
    clkEdge->getNet()->putIsEdgeTrigger( true );
    always->addEdgeExpr( clkEdge );
    
    if( levelSetIdent ) {
      CopyContext cc(0,0);
      NUEdgeExpr* asyncSet = new NUEdgeExpr( eClockPosedge,
                                             levelSetIdent->copy(cc),
                                             loc );
      asyncSet->getNet()->putIsEdgeTrigger( true );
      always->addEdgeExpr( asyncSet );
    }
    if( levelClearIdent ) {
      CopyContext cc(0,0);
      NUEdgeExpr* asyncClear = new NUEdgeExpr( eClockPosedge,
                                               levelClearIdent->copy(cc),
                                               loc );
      asyncClear->getNet()->putIsEdgeTrigger( true );
      always->addEdgeExpr( asyncClear );
    }
  }
  return eSuccess;
}

//! Top level call to populate a sequential UDP.
//! Analyze a sequential UDP table, and build the nucleus structures to
//! represent it.
Populate::ErrorCode VerilogPopulate::udpSequentialComponents(
                                       veNode ve_udp, LFContext* context )
{
  ErrorCode err_code = eSuccess;
  ErrorCode ec = eSuccess;
  NUExpr* levelSet = NULL;
  NUExpr* levelClear = NULL;
  NUExpr* clock = NULL;
  udpEdgeType edge = udpDontCare;
  NUExpr* edgeData = NULL;
  NUExpr* edgeEnable = NULL;
  veNode clkNode = NULL;

  // Process the sequential UDP table and build the expressions.
  CheetahList tableEntries(veUDPGetTableEntryList( ve_udp ));
  while( veNode entry = veListGetNextNode( tableEntries ) ) {
    // Only process this entry if it sets or clears the state.
    veNode outputSymbol = veTableEntryGetOutputSymbol( entry );
    veOutputSymbolType outputLevel = veOutputSymbolGetSymbol( outputSymbol );
    veNode stateSymbol = veSeqEntryGetCurrentState( entry );
    veLevelSymbolType stateLevel = veLevelSymbolGetSymbol( stateSymbol );
    bool setEntry = (outputLevel == OUTPUT_ONE) && (stateLevel != LEVEL_ONE);
    bool clearEntry = (outputLevel == OUTPUT_ZERO) && (stateLevel != LEVEL_ZERO);
    if( !setEntry && !clearEntry )
      continue;
      
    veNode inputList = veSeqEntryGetUdpInputList( entry );
    CheetahList portList(veUDPGetPortList( ve_udp ));
    // Start at the the portList at the inputs.
      veListGetNextNode( portList );
    
    bool isLevelEntry = false;
    NUExpr* product = NULL;
    // Determine whether we have a level entry.  This is complicated by the
    // fact that some edges (e.g. (?1) ) are effectively levels.
    
    if( veNodeGetObjType( inputList ) == VE_UDPLEVELINPUTLIST ) {
      isLevelEntry = true;
      CheetahList levelList(veUdpLevelInputListGetUdpLevelInputList( inputList ));
      
      ec = udpLevelEntryProduct( levelList, portList, context, &product );
      if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
        return ec;
    } else if (udpLevelsContainX (inputList)) {
      // This entry contains an X. We just ignore these entries in UDPs
    } else {
      udpEdgeType et = udpGetEdgeType( inputList );
      if( et == udpLevelHigh || et == udpLevelLow || et == udpDontCare ) {
        // This edge entry is really just a level.  Build the level list
	// including the phony edge which is really a level.
        isLevelEntry = true;
	
        CheetahList list1(veUdpEdgeInputListGetUdpLevelInputList1( inputList ));
        ec = udpLevelEntryProduct( list1, portList, context, &product);
        if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
          return ec;
        veNode port = veListGetNextNode( portList );

        if( et == udpLevelHigh || et == udpLevelLow ) {
          ErrorCode ec = udpAddTermToProduct( context, &product, port,
            (et == udpLevelLow)  );
          if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
            return ec;
        }
	
        CheetahList list2(veUdpEdgeInputListGetUdpLevelInputList2( inputList ));
        ec = udpLevelEntryProduct( list2, portList, context, &product );
        if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
          return ec;
      }
    }

    SourceLocator loc = locator( inputList );
    if( isLevelEntry ) {
      if( product ) {
        if( setEntry ) {
	  if( levelSet ) {
            levelSet = new NUBinaryOp( NUOp::eBiBitOr, levelSet, product, loc );
	    levelSet->resize( 1 );
	  } else {
	    levelSet = product;
	  }
	} else {
	  if( levelClear ) {
            levelClear = new NUBinaryOp( NUOp::eBiBitOr, levelClear, product, loc );
	    levelClear->resize( 1 );
	  } else {
	    levelClear = product;
	  }
	}
      }
    } else {
      NUExpr* enable = NULL;
      bool isValidEntry = true;
      ec = udpEdgeEntry( entry, inputList, portList, context, &clkNode, &clock,
                         &edge, &enable, &isValidEntry);
      if( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) ) 
        return ec;
      if( isValidEntry ) {
        if( edgeData && !edgeEnable ) {
          // rjc we have no testcase for this condition
          UtString udp_name;
          mTicProtectedNameMgr->getVisibleName(ve_udp, &udp_name);
          POPULATE_FAILURE(mMsgContext->UDPEdgeError( &loc, udp_name.c_str(),
                                                      " has an edge assignment followed by an unconditional edge assignment" ),
                           &err_code);
          return err_code;
        }
	
	if( enable ) {
	  if( !edgeEnable ) {
            edgeEnable = enable;
          } else {
	    edgeEnable = new NUBinaryOp( NUOp::eBiBitOr, enable, edgeEnable, loc );
	    edgeEnable->resize( 1 );	
	  }		
	}
	
	if( edgeData ) {
	  if( setEntry ) {
	    CopyContext cc(0,0);
	    edgeData = new NUBinaryOp( NUOp::eBiBitOr, enable->copy(cc), edgeData, loc );
	    edgeData->resize( 1 );
	  }			
	} else {
	  if( setEntry ) {
            if ( enable ) {
              CopyContext cc(0,0);
              edgeData = enable->copy(cc);
            }
	  } else {
            edgeData = NUConst::create( false, 0,  1, loc );
          }
	} 
      }
    }
  }
  
  // Create the always block to represent the table.
  ec = udpAlways( ve_udp, context, clock, edge, edgeEnable, edgeData,
                  levelSet, levelClear );
  if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
    return ec;
  
  return err_code;
}


/*! 
  Build a continuous assign to represent a combinational UDP table.
  Only entries with output=1 and no x values are counted towards the
  sum of products representation.

  \verbatim
  Example:
   table
   // a b c : out
      0 0 1 : 1
      x b ? : 0
      1 1 0 : x
      1 1 1 : 1
   endtable
 out = !a&!b&c | a&b&c;
 \endverbatim
*/
Populate::ErrorCode VerilogPopulate::udpContAssign(
                                   veNode ve_udp,
                                   LFContext* context,
                                   NUContAssign **the_assign )
{
  ErrorCode err_code = eSuccess;
  *the_assign = NULL;
  
  NUIdentLvalue* lhs;
  ErrorCode ec = udpMakeLhs( ve_udp, context, &lhs );
  if( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) )
    return ec;
  
  SourceLocator loc = locator( ve_udp );
  NUExpr* sum = NULL;
  CheetahList tableEntries(veUDPGetTableEntryList( ve_udp ));
  while( veNode entry = veListGetNextNode( tableEntries ) ) {
    // Build a sum of products where the output is 1.
    // If the output is not 1, ignore this entry.
    veNode outputSymbol = veTableEntryGetOutputSymbol( entry );
    if( veOutputSymbolGetSymbol( outputSymbol ) != OUTPUT_ONE )
      continue;

    CheetahList portList(veUDPGetPortList( ve_udp ));
    // skip the output
    veListGetNextNode( portList );
    CheetahList levelList(veCombEntryGetLevelInputList( entry ));
    
    NUExpr* product = NULL;
    ErrorCode ec = udpLevelEntryProduct( levelList, portList, context, &product );
    if ( errorCodeSaysReturnNowWhenFailPopulate( ec, &err_code ) ) {
      delete lhs;
      if( sum )  delete sum;
      if( product )  delete product;
      return ec;
    }
    
    if( product ) {
      if( sum ) {
        sum = new NUBinaryOp( NUOp::eBiBitOr, sum, product, loc );
	sum->resize( 1 );			
      } else {
	sum = product;
      }
    }
    
  }
  
  if( sum )
    *the_assign = helperPopulateContAssign( lhs, sum, loc, context);
  else
  {
    POPULATE_FAILURE(mMsgContext->NoUDPOutputExpr( &loc ), &err_code);
    return err_code;
  }
  
  return err_code;
}

