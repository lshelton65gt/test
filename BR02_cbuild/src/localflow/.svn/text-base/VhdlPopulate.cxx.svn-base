// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/VhdlPopulate.h"
#include "localflow/DesignPopulate.h"
#include "util/AtomicCache.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUTF.h"
#include "util/CbuildMsgContext.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/JaguarContext.h"
#include "util/ArgProc.h"
#include "LFContext.h"
#include "localflow/TicProtectedNameManager.h"


VhdlPopulate::VhdlPopulate(DesignPopulate *design_populate,
                           STSymbolTable *symtab,
                           AtomicCache *str_cache,
                           IODBNucleus * iodb,
                           SourceLocatorFactory *loc_factory,
                           FLNodeFactory *flow_factory,
                           NUNetRefFactory *netref_factory,
                           MsgContext *msg_context,
                           ArgProc *arg,
                           bool useLegacyGenerateHierarchyNames,
			   CarbonContext* carbonContext,
			   TicProtectedNameManager* tic_protected_nm_mgr):
  Populate(msg_context, iodb, design_populate, symtab, str_cache, loc_factory, flow_factory, netref_factory, arg, carbonContext),
  mUseLegacyGenerateHierarchyNames(useLegacyGenerateHierarchyNames),
  mTicProtectedNameMgr(tic_protected_nm_mgr),
  mCurrentProcessInfo( NULL ), mUniqueFunctionIndex(0),
  mRecursionLimit(0), mUnrolledLoopScopeCount(0), mIsAggressiveEvalEna(false),
  mReturnExprs(NULL), mConstraintRange(NULL)
{
  // Get the severity we are using for clocking errors, to know if it's
  // a real error or not.
  MsgContextBase::Severity sev;
  mMsgContext->getMsgSeverity( "ClockingError", &sev );
  mClockErrorsDemoted = ( mMsgContext->isErrorSeverity( sev ))?false:true;
  // Get the maximum recursion limit for a recursive function.
  mArg->getIntLast(JaguarContext::scVhdlRecursionLimit, &mRecursionLimit);
  mIsFuncConstPropEnabled = !mArg->getBoolValue(JaguarContext::scNoVhdlConstPropFuncCalls);
}


VhdlPopulate::~VhdlPopulate()
{
  // Delete all the tasks that we created, found that they returned a constant
  // value and never added them to the module. The expressions for constant
  // values replaced the calls to it.
  for (TaskConstExprMap::const_iterator e = mConstTaskMap.begin();
       e != mConstTaskMap.end(); ++e) {
    delete e->first; // Delete the task which is map key.
  }
  // Clean up the expression replacement map expressions.
  for (EntityAliasMap::iterator itr1 = mAliasMap.begin();
       itr1 != mAliasMap.end(); ++itr1)
  {
    NUExprReplacementMap& replMap = itr1->second;
    for (NUExprReplacementMap::iterator itr2 = replMap.begin();
         itr2 != replMap.end(); ++itr2) {
      delete itr2->second;
    }
  }

  // Make sure to remove the constraint range
  delete mConstraintRange;
}


SourceLocator VhdlPopulate::locator(vhNode vh_node)
{
  if (vh_node == NULL) {
    // We really should assert here but there can be no useful message
    // printed at this point, so let's just create a null file reference
    // and hopefully an error printed above will give more context
    return mLocFactory->create("(null)", 0);
  }
  return mLocFactory->create(vhGetSourceFileName(vh_node), vhGetLineNumber(vh_node));
}

// Method to map a VHDL scope with a NU scope
void VhdlPopulate::mapPackage(StringAtom *packName, NUNamedDeclarationScope *package,
                              LFContext* context)
{
  UtString pack_name(packName->str());
  pack_name.lowercase();
  
  StringAtom* pack = context->vhdlIntern( pack_name.c_str() );
  mPackageMap[pack] = package;
  return;
}

// Method to lookup for a NU scope for a given the VHDL scope 
void
VhdlPopulate::lookupPackage(StringAtom *packName, NUNamedDeclarationScope **the_package,
                            LFContext* context) const
{
  *the_package = 0;

  UtString pack_name(packName->str());
  pack_name.lowercase();
  
  StringAtom* pack = context->vhdlIntern( pack_name.c_str() );
  StringAtomPackageMap::const_iterator q = mPackageMap.find(pack);
  if (q != mPackageMap.end()) 
  {
    *the_package = q->second;
  }
  return;
}

// Method to map a VHDL scope with a NU block
void
VhdlPopulate::mapBlock(vhNode vh_scope, NUBlock *block)
{
  if ( mBlockMap.find(vh_scope) == mBlockMap.end() )
  {
    UtStack<NUBlock*> *newStack = new UtStack<NUBlock*>;
    newStack->push( block );
    mBlockMap[vh_scope] = newStack;
  }
  else
  {
    mBlockMap[vh_scope]->push( block );
  }
}

void
VhdlPopulate::unmapBlock(vhNode vh_scope, NUBlock *block)
{
  VhNodeBlockStackMap::iterator p = mBlockMap.find(vh_scope);
  NU_ASSERT(p != mBlockMap.end(), block);
  UtStack< NUBlock*> *blockStack = p->second;
  NU_ASSERT(blockStack->top() == block, block);
  blockStack->pop();
  if ( blockStack->empty( ))
  {
    mBlockMap.erase(p);
    delete blockStack;
  }
}

// Method to map a VHDL scope with a NU block
void VhdlPopulate::mapNextBlock(vhNode vh_scope, NUBlock *block)
{
  mNextBlockMap[vh_scope] = block;
}

void VhdlPopulate::unmapNextBlock(vhNode vh_scope, NUBlock *block)
{
  VhNodeBlockMap::iterator p = mNextBlockMap.find(vh_scope);
  NU_ASSERT(p != mNextBlockMap.end(), block);
  NU_ASSERT(p->second == block, block);
  mNextBlockMap.erase(p);
}

void VhdlPopulate::mapTaskConstantReturn(NUTask* task, NUConst* constExpr)
{
  mConstTaskMap[task] = constExpr;
}

NUConst* VhdlPopulate::lookupConstantReturnForTask(NUTask* task)
{
  TaskConstExprMap::const_iterator e = mConstTaskMap.find(task);
  if (e == mConstTaskMap.end()) {
    return NULL;
  }
  return e->second;
}

Populate::ErrorCode VhdlPopulate::mapTask(NUModule *module,
                                          StringAtom* signature,
                                          NUTask *task)
{
  VhSignatureTaskMap& taskMap = mTaskMap[module];
  taskMap[signature] = task;
  return eSuccess;
}


Populate::ErrorCode VhdlPopulate::lookupTask(NUModule *module,
                                             StringAtom* signature,
                                             NUTask **the_task)
{
  *the_task = 0;

  ModuleNodeTaskMap::const_iterator p = mTaskMap.find(module);
  if (p == mTaskMap.end()) {
    return eSuccess;
  }

  const VhSignatureTaskMap& taskMap = p->second;
  VhSignatureTaskMap::const_iterator q = taskMap.find(signature);
  if (q != taskMap.end()) {
    *the_task = q->second;
  }
  return eSuccess;
}


Populate::ErrorCode
VhdlPopulate::mapNet(NUScope* scope, mvvNode mvv_node, NUNet* net)
{
  vhNode node = reinterpret_cast<vhNode>( mvv_node );
  if ( vhGetObjType( node ) == VHOBJECT )
    node = vhGetActualObj( node );
  if ( vhGetObjType( node ) == VHSELECTEDNAME )
  {
    node = vhGetActualObj( node );
  }
  // Get the scope of this net. If the scope is enity/architecture or
  // package then map it in the module scope via the 
  // DesignPopulate::mapNet(). Otherwise map it in the declaration
  // scope of the net.
  vhNode vh_scope = vhGetScope(node);
  if (vhGetObjType(vh_scope) ==  VHSUBPROGDECL)
    vh_scope = vhGetSubProgBody(vh_scope);

    return mPopulate->mapNet(scope, mvv_node, net);
}

Populate::ErrorCode
VhdlPopulate::mapConstraintRange(NUNet* net)
{
  if (mConstraintRange == NULL)
    return Populate::eSuccess;

  Populate::ErrorCode error = mPopulate->mapConstraintRange(net, mConstraintRange);

  delete mConstraintRange;

  mConstraintRange = NULL;

  return error;
}

// Look up a VHDL net in either the symbol table (for a record net) or
// in the net map (for everyone else)
Populate::ErrorCode
VhdlPopulate::lookupNet( NUScope *scope, vhNode node, LFContext *context,
                         NUNet **the_net, bool error_if_missing )
{
  VhObjType nodeObjType = vhGetObjType(node);
  if ( nodeObjType == VHOBJECT )
    node = vhGetActualObj( node );
  if ( nodeObjType == VHSELECTEDNAME )
  {
    vhNode selected_record = node; // Preserve original selected name
    node = vhGetActualObj( node ); // This might be the whole record

    if ( isRecordNet( node, context ))
    {
      // This is selected name representing a record element
      vhNode vh_scope = vhGetScope(node);
      VhObjType scopeObjType = vhGetObjType(vh_scope);
      if (scopeObjType ==  VHPACKAGEDECL || scopeObjType ==  VHPACKAGEBODY)
      {
	// Construct the package hierarchical name
	JaguarString libName( vhGetLibName( vh_scope ));
	JaguarString packName( vhGetPackName( vh_scope ));
	StringAtom *packIdStr = createPackIdStr( libName, packName, context );
	NUNamedDeclarationScope *packScope;
	lookupPackage( packIdStr, &packScope, context );

        return lookupRecordElement( packScope, static_cast<vhExpr>( selected_record ), NULL, context,
                                    the_net );
      }
      else
      {
        return lookupRecordElement( scope, static_cast<vhExpr>( selected_record ), NULL, context,
                                    the_net );
      }
    }
  }

  //  If the scope is a package, we need to create a hierref for it.
  vhNode vh_scope = vhGetScope(node);
  VhObjType scopeObjType = vhGetObjType(vh_scope);
  if (scopeObjType ==  VHSUBPROGDECL)
    vh_scope = vhGetSubProgBody(vh_scope);

  // There are three ways we can get here. First, if we have signal or  constant declared in the
  // package declaration. It could be a simple net or a record. Second, if the node is a constant 
  // defined in the package body. Third, if the node is the function body. See comment for
  // function  functionResult in the VhPopulateProgram.cxx file.
  if ( (scopeObjType == VHPACKAGEDECL) || 
       ( (scopeObjType == VHPACKAGEBODY) && (vhGetObjType(node) == VHCONSTANT) ) )
  {
    // Construct the package hierarchical name
    JaguarString libName( vhGetLibName( vh_scope ));
    JaguarString packName( vhGetPackName( vh_scope ));
    StringAtom *packIdStr = createPackIdStr( libName, packName, context );
    NUNamedDeclarationScope *packScope;
    lookupPackage( packIdStr, &packScope, context );


    if ( isRecordNet( node, context ))
      return lookupRecordElement( packScope, static_cast<vhExpr>( node ), NULL, context, the_net );
    else
       return packageNet( node, context, the_net );      
  }

  return mPopulate->lookupNet( scope, node, the_net, error_if_missing );
}


// Look up a VHDL port in either the symbol table (for a record port) or
// in the port map (for everyone else)
Populate::ErrorCode
VhdlPopulate::lookupPort( NUModule *module, vhNode node, LFContext *context,
                         NUNet **the_port )
{
  if ( not (!mCarbonContext->isNewRecord() && isRecordNet( vhGetActualObj( node ), context )))
  {
    // This is a non-record selected name
    bool partial_port = false;
    ConstantRange port_range;
    return mPopulate->lookupPort( module, vhGetActualObj( node ), NULL,
                                  NULL, the_port, &partial_port, &port_range );
  }
  return eFatal;
}

// Method to decompile a jaguar node into string so that we can
// print this string in the error message txt. 
const char* VhdlPopulate::decompileJaguarNode(vhNode jag_node, UtString* buf)
{
  JaguarUserString slice_str(vhStrDecompileNode(jag_node));
  buf->clear();
  *buf << slice_str;
  StringUtil::strip(buf, "\n");
  return buf->c_str();
}


//! Method to get the accelerated function type if it is falls into
//! the accelerated function category. A function will fall in this
//! category if the body of this function contains metacomment directive
//! to builtin/mapped operator/function or feedthru.
vhBuiltInFuncOrMappedOperatorType 
VhdlPopulate::getBuiltInFuncOrMappedOperatorType(vhNode vh_func_call)
{
  vhBuiltInFuncOrMappedOperatorType funcType = eVHSYN_NONE;

  if (vhGetObjType(vh_func_call) != VHFUNCCALL)
  {
    return funcType;
  }
  vhNode vh_func = vhGetMaster(vh_func_call);
  if (VHSUBPROGDECL == vhGetObjType(vh_func))
    vh_func = vhGetSubProgBody(vh_func);
  // Get the list of metacomments 
  JaguarList vh_mc_list(vhGetListofMetaComments(vh_func));
  vhNode vh_metacomment;
  // We need to visit all the metacomment directives because the last
  // directive will be in effect. In the example below, the
  // effective directive is : 'synopsys built_in SYN_OR' 
  // -- synopsys built_in SYN_AND
  // -- synopsys built_in SYN_OR
  //
  while ( (vh_metacomment = vhGetNextItem(vh_mc_list)) != NULL )  
  {
    const char* mc_directive = vhGetAssociatedDirectiveString(vh_metacomment);
    StrToken tok(mc_directive, " \t");
    const char* word = NULL;
    if (!tok.atEnd())
    {
      // Discard the first token since the first token is always a valid
      // metacomment prefix otherwise Jaguar would not have considered it as
      // a metacomment.
      ++tok;
    }
    else
    {
      continue;
    }
    if (!tok.atEnd())
    {
      word = *tok;
      ++tok;
    }
    else
    {
      continue;
    }
    // The second word must be 'built_in' or 'map_to_operator'
    bool isMappedOperator = false;
    if (!strncasecmp(word, "map_to_operator",16))
    {
      isMappedOperator = true;
    }
    else 
    {
      if (0 != strncasecmp(word, "built_in",9))
      {
        continue;
      }
    }
    if (!tok.atEnd())
    {
      word = *tok;
      ++tok;
    }
    else
    {
      continue;
    }
    // At this point we have got the directive '<synth_prefix>
    // built_in|map_to_operator'
    // Now we need to retrieve the functionality directive of this metacomment

    // All the directives except the 'FLATTEN' directive starts with the
    // pattern SYN_... . So, to have an efficient string comparison, first
    // compare the first 4 chars of word .
    bool isPrefixSYN = false;
    if (!strncasecmp(word, "SYN_",4))
    {
      isPrefixSYN = true;
    }
    if ( !isPrefixSYN )
    {
      if ( !strcasecmp(word, "FLATTEN") )
      {
        funcType = eVHFLATTEN;
      }
      else if (not isMappedOperator)
      {
        // This pragma is not SYN_... , so it is not a pragma of our interest.
        // All the pragmas of our interest start with the prefix SYN_ except
        // the pragma 'FLATTEN' or built in operators.
        continue;
      }
    }
    const char* mcDirStr = word + 4;
    if (isMappedOperator)
    {
      if  ( !strcasecmp(word, "MULT_TC_OP") ) 
      {
        funcType = eVHSYN_SIGN_MULT;
      }
      else if ( !strcasecmp(word, "MULT_UNS_OP") ) 
      {
        funcType = eVHSYN_UNS_MULT;
      }
      else if ( !strcasecmp(word, "SUB_TC_OP") || 
                !strcasecmp(word,"SUB_UNS_OP") )
      {
        funcType = eVHSYN_NUMERIC_BINARY_MINUS;
      }
      else if ( !strcasecmp(word, "ADD_TC_OP") ||
                !strcasecmp(word, "ADD_UNS_OP") )
      {
        funcType = eVHSYN_NUMERIC_BINARY_PLUS;
      }
      else if ( !strcasecmp(word, "LT_UNS_OP") )
      {
        funcType = eVHSYN_UNS_LT;
      }
      else if ( !strcasecmp(word, "LT_TC_OP") )
      {
        funcType = eVHSYN_LT;
      }
      else if ( !strcasecmp(word, "LEQ_UNS_OP") )
      {
        funcType = eVHSYN_UNS_LT_EQUAL;
      }
      else if ( !strcasecmp(word, "LEQ_TC_OP") )
      {
        funcType = eVHSYN_LT_EQUAL;
      }
      else
      {
        continue;
      }
    } // End of the block if (isMappedOperator)
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "FEED_THRU") ||
              !strcasecmp(mcDirStr, /*SYN_*/ "FEED_THROUGH") )
    {
      funcType = eVHSYN_FEED_THRU;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "BUF") )
    {
      funcType = eVHSYN_FEED_THRU;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "INTEGER_TO_SIGNED") )
    {
      funcType = eVHSYN_S_TO_S;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "SIGNED_TO_INTEGER") )
    {
      funcType = eVHSYN_S_TO_I;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "INTEGER_TO_UNSIGNED") )
    {
      funcType = eVHSYN_S_TO_U;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "UNSIGNED_TO_INTEGER") )
    {
      funcType = eVHSYN_U_TO_I;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "SIGN_EXTEND") )
    {
      funcType = eVHSYN_S_XT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "ZERO_EXTEND") )
    {
      funcType = eVHSYN_U_TO_U;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "UNS_RESIZE") )
    {
      funcType = eVHSYN_UNS_RESIZE;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "RESIZE") )
    {
      funcType = eVHSYN_RESIZE;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "EQL") )
    {
      funcType = eVHSYN_EQUAL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NEQ") )
    {
      funcType = eVHSYN_NOT_EQUAL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "AND") )
    {
      funcType = eVHSYN_AND;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NAND") )
    {
      funcType = eVHSYN_NAND;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "OR") )
    {
      funcType = eVHSYN_OR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NOR") )
    {
      funcType = eVHSYN_NOR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "XOR") )
    {
      funcType = eVHSYN_XOR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "XNOR") )
    {
      funcType = eVHSYN_XNOR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NOT") )
    {
      funcType = eVHSYN_NOT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "SIGNED_MULT") )
    {
      funcType = eVHSYN_SIGN_STAR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "UNSIGNED_MULT") )
    {
      funcType = eVHSYN_UNS_STAR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_SIGNED_MULT") )
    {
      funcType = eVHSYN_SIGN_MULT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_UNSIGNED_MULT") )
    {
      funcType = eVHSYN_UNS_MULT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_SIGN_DIV") )
    {
      funcType = eVHSYN_NUMERIC_SIGN_DIV;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_UNS_DIV") )
    {
      funcType = eVHSYN_NUMERIC_UNS_DIV;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "MINUS") )
    {
      funcType = eVHSYN_BINARY_MINUS;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_MINUS") )
    {
      funcType = eVHSYN_NUMERIC_BINARY_MINUS;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_UNARY_MINUS") )
    {
      funcType = eVHSYN_NUMERIC_UNARY_MINUS;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "PLUS") )
    {
      funcType = eVHSYN_BINARY_PLUS;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_PLUS") )
    {
      funcType = eVHSYN_NUMERIC_BINARY_PLUS;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "ABS") )
    {
      funcType = eVHSYN_ABS;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_UNS_LESS") )
    {
      funcType = eVHSYN_UNS_LT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_UNS_GT") )
    {
      funcType = eVHSYN_UNS_GT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_GT") )
    {
      funcType = eVHSYN_GT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_LESS") )
    {
      funcType = eVHSYN_LT;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_UNS_GT_OR_EQUAL") )
    {
      funcType = eVHSYN_UNS_GT_EQUAL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_UNS_LESS_OR_EQUAL") )
    {
      funcType = eVHSYN_UNS_LT_EQUAL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_GT_OR_EQUAL") )
    {
      funcType = eVHSYN_GT_EQUAL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "IS_LESS_OR_EQUAL") )
    {
      funcType = eVHSYN_LT_EQUAL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "AND_REDUCE") )
    {
      funcType = eVHSYN_REDUCT_AND;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "OR_REDUCE") )
    {
      funcType = eVHSYN_REDUCT_OR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "SHL") )
    {
      funcType = eVHSYN_SHL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "SHR") )
    {
      funcType = eVHSYN_SHR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "SHLA") )
    {
      funcType = eVHSYN_SHLA;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "SHRA") )
    {
      funcType = eVHSYN_SHRA;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "ROL") )
    {
      funcType = eVHSYN_ROL;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "ROR") )
    {
      funcType = eVHSYN_ROR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "XOR_REDUCE") )
    {
      funcType = eVHSYN_REDUCT_XOR;
    }
    else if ( !strcasecmp(mcDirStr, /*SYN_*/ "NUMERIC_MOD") )
    {
      funcType = eVHSYN_MOD;
    }
  }
  return funcType;
}

const char* VhdlPopulate::getActualVhdlLibName(const char* libName,
                                               UtString& libStr)
{
  libStr.clear();
  libStr << "$lib_" << libName;
  return libStr.c_str();
}

StringAtom*
VhdlPopulate::createPackIdStr( const char* libName, const char *packName,
                               LFContext *context )
{
  UtString packIdStr;
  UtString actualLibName;
  packIdStr << getActualVhdlLibName(libName, actualLibName) << "_" << packName;
  return context->vhdlIntern( packIdStr.c_str( ));
}


Populate::ErrorCode
VhdlPopulate::createPackageScope( vhNode vh_pack, LFContext *context,
                                  NUNamedDeclarationScope **the_package )
{
  // Get the symbol table
  NUModule *topModule = context->getRootModule();
  STSymbolTable *aliasDB = topModule->getAliasDB();
  SourceLocator loc = locator( vh_pack );

  // Find or create the library scope
  NUNamedDeclarationScope *the_library = NULL;
  JaguarString libName( vhGetLibName( vh_pack ));
  UtString libString;
  getActualVhdlLibName(libName, libString);
  StringAtom *libAtom = context->vhdlIntern( libString.c_str( ));
  JaguarString packName( vhGetPackName( vh_pack ));
  StringAtom *packAtom = context->vhdlIntern( packName );
  STSymbolTableNode *packNode;
  STSymbolTableNode *libNode = aliasDB->find( NULL, libAtom );
  if ( libNode == NULL )
  {
    the_library = new NUNamedDeclarationScope( libAtom, topModule, mIODB,
                                               context->getNetRefFactory(),
                                               loc );
    packNode = NULL;
  }
  else
  {
    STBranchNode *branch = libNode->castBranch();
    the_library = static_cast<NUNamedDeclarationScope*>(( NUAliasBOM::castBOM( branch->getBOMData( )))->getScope());
    if ( the_library == NULL )
    {
      UtString reason;
      reason << "Cannot find scope for library " << libName << ".";
      mMsgContext->JaguarConsistency( &loc, reason.c_str( ));
      return eFatal;
    }
    // sanity check
    if ( the_library->getScopeType() != NUScope::eNamedDeclarationScope )
    {
      UtString reason;
      reason << "Incorrect internal scope type for library " << libName << ".";
      mMsgContext->JaguarConsistency( &loc, reason.c_str( ));
      return eFatal;
    }
    packNode = aliasDB->find( libNode, packAtom );
#if DEBUG_VHDL_HIERREFS
    UtIO::cout() << "Not ";
#endif
  }
#if DEBUG_VHDL_HIERREFS
    UtIO::cout() << "Creating library scope " << libAtom->str() << "\n";
#endif
  // libNode will either be correct (if it was previously there) or NULL
  // (if we just created it).

  // Find or create the package scope
  if ( packNode == NULL )
  {
    *the_package = new NUNamedDeclarationScope( packAtom, the_library, mIODB,
                                                context->getNetRefFactory(),
                                                loc );
    // Add this package to the map
    StringAtom *packIdStr = createPackIdStr( libName, packName, context );
    mapPackage( packIdStr, *the_package, context);
  }
  else
  {
    STBranchNode *branch = packNode->castBranch();
    *the_package = static_cast<NUNamedDeclarationScope*>(( NUAliasBOM::castBOM( branch->getBOMData( )))->getScope());
    if ( *the_package == NULL )
    {
      UtString reason;
      reason << "Cannot find scope for package " << packName << " in library "
             << libName << ".";
      mMsgContext->JaguarConsistency( &loc, reason.c_str( ));
      return eFatal;
    }
    // sanity check
    if ( (*the_package)->getScopeType() != NUScope::eNamedDeclarationScope )
    {
      UtString reason;
      reason << "Incorrect internal scope type for package " << packName 
             << " in library " << libName << ".";
      mMsgContext->JaguarConsistency( &loc, reason.c_str( ));
      return eFatal;
    }

#if DEBUG_VHDL_HIERREFS
    UtIO::cout() << "Not ";
#endif
  }
#if DEBUG_VHDL_HIERREFS
  UtIO::cout() << "Creating package scope " << (*the_package)->getPrintableName()
               << " in library scope " << libAtom->str() << "\n";
#endif

  return eSuccess;
}

Populate::ErrorCode VhdlPopulate::calcForGenerateBlockName(vhNode vh_forgen, 
                                                           vhExpr vhIndexExpr,
                                                           LFContext* context,
                                                           StringAtom **name)
{
  JaguarString vh_label_str(vhGetLabel(vh_forgen));
  int value = 0;
  ErrorCode temp_err_code = getVhExprValue(vhIndexExpr, &value, NULL);
  if ( temp_err_code == eSuccess ) {
    // Debussy follows the following way of mangling
    UtString label(vh_label_str);
    label << "__" << value;
    *name = context->vhdlIntern(label.c_str());
  } else {
    SourceLocator loc = locator( vhIndexExpr );
    mMsgContext->JaguarConsistency( &loc, "Unable to determine value for index expression of For Generate Block.");
    *name = NULL;
  }
  return temp_err_code;
}

void VhdlPopulate::mapVhdlAlias(NUModule* ent, NUNet* aliasNet,
                                NUExpr* aliasedExpr)
{
  NUExprReplacementMap& replMap = mAliasMap[ent];
  replMap.insert(NUExprReplacementMap::value_type(aliasNet, aliasedExpr));
}

VhdlPopulate::EntityAliasMap* VhdlPopulate::getVhdlAliasMap()
{
  return &mAliasMap;
}

mvvNode VhdlPopulate:: substituteEntity(mvvNode oldMod,
                                        mvvLanguageType old_mod_lang,
                                        bool /*is_udp*/,
					UtString& new_mod_name,
					TicProtectedNameManager* ticProtectedNameMgr)
{
  vhNode newEntity = ticProtectedNameMgr->JaguarFindModule(new_mod_name.c_str());
  
  if(NULL == newEntity) return NULL;

  // Copy the value of generic from old DU to the new DU.
  // Only copy the one which match in the new DU.
  if(MVV_VHDL == old_mod_lang) {
      vhNode genericClause = vhGetGenericClause(oldMod->castVhNode());
      if (genericClause == NULL) { return newEntity; }

      UtMap<UtString, vhNode> oldGenericNameInitialValVhNodeMap;
      vhNode generic;
      JaguarList genericList(vhGetFlatGenericList(genericClause));
      while ((generic = vhGetNextItem(genericList))) {
	  vhNode genericInitialVal = vhGetInitialValue(generic);
	  if(genericInitialVal) {
	      JaguarString name(vhGetName(generic));
	      UtString nameStr(name);
	      nameStr.lowercase();
	      oldGenericNameInitialValVhNodeMap[nameStr] = genericInitialVal;
	  }
      }
      genericClause = vhGetGenericClause(newEntity);
      if (genericClause == NULL) { return newEntity; }
      JaguarList newGenericList(vhGetFlatGenericList(genericClause));
      while ((generic = vhGetNextItem(newGenericList))) {
	  JaguarString name(vhGetName(generic));
	  UtString nameStr(name);
	  nameStr.lowercase();
	  UtMap<UtString, vhNode>::iterator oldGenericIter;
	  oldGenericIter = oldGenericNameInitialValVhNodeMap.find(nameStr);
	  if(oldGenericIter == oldGenericNameInitialValVhNodeMap.end()) continue;
	  // TODO check the type of initial value
	  vhSetInitialValue(generic, static_cast<vhExpr>(oldGenericIter->second));
      }

  } 
  else { // old_module_lang == MVV_VERILOG

    CheetahList param_list(veModuleGetParamList(oldMod->castVeNode()));

    if (param_list == NULL) return newEntity;

    veNode old_param = NULL;
    UtMap<UtString, veNode> param_val_map;
    while ((old_param = veListGetNextNode(param_list))) {
      veNode param_val = veParamGetCurrentValue(old_param);
      UtString oldParamName;
      ticProtectedNameMgr->getVisibleName(old_param, &oldParamName);
      oldParamName.lowercase();
      param_val_map[oldParamName] = param_val;
    }

    vhNode genericClause = vhGetGenericClause(newEntity);
    
    if (genericClause == NULL) return newEntity;
    UtMap<UtString, veNode>::iterator iter;
    JaguarList newGenericList(vhGetFlatGenericList(genericClause));
    vhNode generic;
    while ((generic = vhGetNextItem(newGenericList))) {
      JaguarString name(vhGetName(generic));
      UtString nameStr(name);
      nameStr.lowercase();
      iter = param_val_map.find(nameStr);
      if (iter != param_val_map.end()) {
	veNode paramVal = (*iter).second;
	UtString paramValStr(veExprEvaluateValueInBinaryString(paramVal));
	vhNode genericVal = vhCreateExprFromString(paramValStr.c_str(),newEntity);
	if (genericVal != NULL) {
	  vhSetLineNumber(genericVal, vhGetLineNumber(generic));
	  vhSetEndLineNumber(genericVal, vhGetEndLineNumber(generic));
	  vhSetInitialValue(generic, static_cast<vhExpr>(genericVal));
	}
      }
    }
  }

  return newEntity;
}

// END FILE

