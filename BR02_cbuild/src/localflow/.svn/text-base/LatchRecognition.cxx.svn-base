// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "LatchRecognition.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUModuleInstance.h"
#include "util/SetOps.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"

// please do not use "using namespace std"

/*!
  \file
  Implementation of latch recognition
*/

void LatchRecognition::module(NUModule *this_module)
{
  // Look through tasks
  NUNetSet latches;
  for (NUTaskLoop loop = this_module->loopTasks(); not loop.atEnd(); ++loop) {
    findLatches(*loop, latches);
  }

  // Look through combinational always blocks
  NUAlwaysBlockList always_blocks;
  this_module->getAlwaysBlocks(&always_blocks);
  for (NUAlwaysBlockListIter iter = always_blocks.begin();
       iter != always_blocks.end();
       ++iter)
    findLatches(*iter, latches);

  // Look through continous assignments
  NUContAssignList cont_assigns;
  this_module->getContAssigns(&cont_assigns);
  for (NUContAssignListIter iter = cont_assigns.begin();
       iter != cont_assigns.end();
       ++iter)
    findLatches(*iter, latches);

  // Check if the latches are used, if not we can remove them from the
  // set. We do this for functions/tasks, always blocks and cont assigns
  SortedNetSet used_latches;

  // Check tasks
  for (NUTaskLoop loop = this_module->loopTasks(); not loop.atEnd(); ++loop) {
    checkUses(*loop, latches, used_latches);
  }

  // Look through combinational always blocks
  for (NUAlwaysBlockListIter iter = always_blocks.begin();
       iter != always_blocks.end();
       ++iter)
    checkUses(*iter, latches, used_latches);

  // Look through continous assignments
  for (NUContAssignListIter iter = cont_assigns.begin();
       iter != cont_assigns.end();
       ++iter)
    checkUses(*iter, latches, used_latches);

  // Look through the sub instances for uses
  for (NUModuleInstanceMultiLoop loop = this_module->loopInstances();
       !loop.atEnd();
       ++loop)
    checkUses(*loop, latches, used_latches);

  // Look through the module ports for uses
  NUNetList port_list;
  this_module->getPorts(&port_list);
  for (NUNetListIter i = port_list.begin(); i != port_list.end(); ++i)
  {
    NUNet* net = *i;
    if (latches.find(net) != latches.end())
      used_latches.insert(net);
  }

  // Go through latches and add them
  for (SortedNetSet::iterator iter = used_latches.begin();
       iter != used_latches.end();
       ++iter) {
    maybeMarkLatch(*iter);
  }
} // void LatchRecognition::module

void LatchRecognition::maybeMarkLatch(NUNet* net)
{
  // Test for net being a task local.
  // If it is, issue a warning but do not mark the net.
  // Note that task bidirects will show up here but are not latches, so weed them out.
  NUScope::ScopeT scope_type = net->getScope()->getScopeType();
  if (scope_type == NUScope::eTask) {
    if (not net->isBid()) {
      mMsgContext->TFLatch(net);
    }
    return;
  }

  // non static variables can't be latches
  if (!net->isNonStatic())
  {
    net->putIsLatch(true);
    if (mVerbose)
    {
      const SourceLocator& loc = net->getLoc();
      UtIO::cout() << loc.getFile() << ":" << loc.getLine();
      UtIO::cout() << " Net (" << net->getName()->str() << ") is a latch.\n";
    }
  }
}

void LatchRecognition::findLatches(NUUseDefNode* use_def,
                                   NUNetSet& latched_nets)
{
  // Sequential blocks can't be latches
  if (use_def->isSequential())
    return;

  // Set of net refs being latched.
  NUNetRefSet latches(mNetRefFactory);

  // Get all the def net refs
  NUNetRefSet defs(mNetRefFactory);
  if (use_def->useBlockingMethods())
  {
    NUUseDefStmtNode* stmtNode = dynamic_cast<NUUseDefStmtNode*>(use_def);
    stmtNode->getBlockingDefs(&defs);
    stmtNode->getNonBlockingDefs(&defs);
  }
  else
  {
    use_def->getDefs(&defs);
  }

  // Check each net to see if it is a latch. In this first pass we are
  // checking for explicit feedback. So if the customer does something
  // like: a = a ^ 1; we treat it like a latch.
  for (NUNetRefSet::iterator p = defs.begin(); p != defs.end(); ++p)
  {
    NUNetRefHdl net_ref = *p;

    // Get the uses and test for the presence of the net ref
    NUNetRefSet uses(mNetRefFactory);
    if (use_def->useBlockingMethods())
    {
      NUUseDefStmtNode* stmtNode = dynamic_cast<NUUseDefStmtNode*>(use_def);
      stmtNode->getBlockingUses(net_ref, &uses);
      stmtNode->getNonBlockingUses(net_ref, &uses);
    }
    else
    {
      use_def->getUses(net_ref, &uses);
    }
    if (uses.find(net_ref, &NUNetRef::overlapsSameNet) != uses.end()) {
      latches.insert(net_ref);
    }
  }

  // This next pass uses the kill information to determine if there
  // are latches. The only statements that don't necessarily kill a
  // def are if statements and case statements. An example is when the
  // customer does if (clk) out = in; The else is missing so that
  // statement can retain a value. This means out is not killed by the
  // if statement. So we simply go through all the statements at the
  // top level for this block and gather up the killed defs. These are
  // not latches.
  //
  // We only do this for always blocks, cont assigns, and tasks
  NUStructuredProc* proc = dynamic_cast<NUStructuredProc*>(use_def);
  NUTF* tf = dynamic_cast<NUTF*>(use_def);
  NUContAssign* assign = dynamic_cast<NUContAssign*>(use_def);
  bool do_this_node = ((tf != 0) or (proc != 0) or (assign != 0));
  if (do_this_node)
  {
    NUNetRefSet kills(mNetRefFactory);

    // Get the list of killed nets and sort them (by pointer is fine
    // for set_difference)
    if (proc) {
      NUBlock* block = proc->getBlock();
      block->getNonBlockingKills(&kills);
      block->getBlockingKills(&kills);
    } else if (tf) {
      tf->getNonBlockingKills(&kills);
      tf->getBlockingKills(&kills);
    } else if (assign) {
      // use the complete defs for cont assigns because kills don't
      // exist. But they are effectively the same information that
      // allows us to determine if some bits are not always defed.
      assign->getCompleteDefs(&kills);
    } else {
      NU_ASSERT("Unexpected block type."==0,use_def);
    }

    // The set of latches are the defs that are not killed
    NUNetRefSet::set_difference(defs, kills, latches);

#ifdef DEBUG_LATCHES
    if (!latches.empty())
    {
      UtIO::cout() << "Defs:\n";
      NUNetRefSet::iterator p;
      for (p = defs.begin(); p != defs.end(); ++p)
	(*p)->print(2);
      UtIO::cout() << "Kills:\n";
      for (p = kills.begin(); p != kills.end(); ++p)
	(*p)->print(2);
    }
#endif
  }

  // At the present time, we latch the net and not the bits, so create a set of
  // nets to latch.
  for (NUNetRefSetIter iter = latches.begin(); iter != latches.end(); ++iter) {
    latched_nets.insert((*iter)->getNet());
  }
}

void LatchRecognition::checkUses(NUUseDefNode* use_def, NUNetSet& latches,
                                 SortedNetSet& used_latches)
{
  // Get all the uses
  NUNetRefSet uses(mNetRefFactory);
  use_def->getUses(&uses);

  // For any uses that are a latch, add them to the used_latches set
  for (NUNetRefSet::iterator p = uses.begin(); p != uses.end(); ++p)
  {
    NUNetRefHdl net_ref = *p;
    NUNet* net = net_ref->getNet();
    if (latches.find(net) != latches.end())
      used_latches.insert(net);
  }
}
