// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "localflow/VerilogPopulate.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"
#include "util/UtConv.h"

#include "LFContext.h"


Populate::ErrorCode VerilogPopulate::cModule(veNode ve_module, 
                                             const char* modName,
                                             NUModule* module,
                                             LFContext* context)
{
  ErrorCode err_code = eSuccess;

  // Create a c-model that represents this c-model. This will be
  // shared between all the c-model functions and calls.
  NUCModel* cmodel = 0;
  ErrorCode temp_err_code = cModel(modName, module, context, &cmodel, true);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Get the various module ports. We currently don't support
  // bidirectional pins so we only accumulate the input and output
  // nets. The function will issue an error if there are I/O ports and
  // return that there are invalid ports.
  NetLookup inputNetMap;
  NetLookup outputNetMap;
  temp_err_code = cModulePorts(module, cmodel, context,
			       inputNetMap, 
			       outputNetMap);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Accumulate the users directives and validate them against the
  // module information. If there are any issues, the routine returns
  // failure. If everything is ok, a timing flow map is created which
  // has information mapped from nets to the directives.
  TimingFlowMap timingFlowMap;
  temp_err_code = cModuleDirectives(modName, module, context,
				    inputNetMap, 
				    outputNetMap,
				    timingFlowMap);
  
  if ( errorCodeSaysReturnNowIfIncomplete( temp_err_code, &err_code) ){
    return err_code;
  }

  // Collect any parameters so that we can pass them to the c-model.
  temp_err_code = cModelModuleParameters(cmodel, ve_module);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Create the c-model function structures. This routine fills the
  // edge functions map and the asyncFn pointer.
  NUCModelInterface* cmodelInterface = cmodel->getCModelInterface();
  CModelFunctions functions;
  temp_err_code = cModelFunctions(context, cmodelInterface, module, 
				  inputNetMap, 
				  outputNetMap,
				  timingFlowMap, 
				  functions);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Create the statements and always blocks to represent these
  // c-model functions
  
  for (CModelFunctions::iterator p = functions.begin(); 
       p != functions.end(); 
       ++p) {
    const EdgeNet* edgeNet = p->first;
    NUCModelFn* cmodelFn = p->second;
    temp_err_code = cModelAlwaysBlock(module, context, edgeNet, 
				      cmodel, 
				      cmodelFn,
				      inputNetMap, 
				      outputNetMap);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  // All done with the functions memory
  for (CModelFunctions::iterator p = functions.begin(); 
       p != functions.end(); 
       ++p) {
    delete p->first;
  }

  return err_code;
}

Populate::ErrorCode
VerilogPopulate::cModelModuleParameters(NUCModel* cmodel,
					veNode ve_module)
{
  ErrorCode err_code = eSuccess;

  // Go through the parameters one by one and accumulate the parameter
  // name and value. If there is no actual value then we use the
  // formal value. We traverse the two lists in order
  CheetahList formalParamIter(veModuleGetParamList(ve_module));
  veNode formalParam;
  while ((formalParam = veListGetNextNode(formalParamIter)) != NULL)
  {
    // Get the name
    NUCModel::Param param;
    UtString name_buf;
    mTicProtectedNameMgr->getVisibleName(formalParam, &name_buf);
    param.first = name_buf.c_str();

    veNode curExpr = veParamGetCurrentValue(formalParam);
    ParamValue actualParam;
    actualParam.putDoNoValOpt(true);
    actualParam.putConvertBasedNumber(false);
    ErrorCode temp_err_code = getParamValueString(&actualParam, curExpr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    
    // Convert the value into a string
    UtString str;
    if (actualParam.hasValidDblVal())
      str << actualParam.getDblValue();
    else if (actualParam.hasValidParamVal())
      str << actualParam.getParamVal();
    else
      str << actualParam.getStr();
    
    param.second = str;
    
    // Add this information to the c-model instance. We allocated our
    // strings on the stack, so the below routine better make a copy
    // of the strings.
    cmodel->addParam(param);
  } // while
  
  return err_code;
}

Populate::ErrorCode
VerilogPopulate::userTask(veNode systask, StringAtom* name,
			  LFContext* context, const SourceLocator& loc,
			  NUStmt** stmt)
{
  ErrorCode err_code = eSuccess;

  // If the model name starts with a $ sign skip it
  const char* cName = name->str();
  if (cName[0] == '$')
    ++cName;

  // Create a c-model that represents this c-model. This will be
  // shared between all the c-model functions and calls.
  NUModule* module = context->getModule();
  NUCModel* cmodel = NULL;
  ErrorCode temp_err_code = cModel(cName, module, context, &cmodel, false);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  {
    // Create the c-model ports
    temp_err_code = taskPorts(cmodel, cName, context, loc);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    // If the ctask has a null port it means it has side effects. We
    // want to only run this task if all of the sensitivity nets have
    // changed.
    if (cmodel->getCModelInterface()->hasNullPort()) {
      context->putRequiresSensitivityEvents(LFContext::eSideEffectCmodel);
    }

    // Create the cmodel call and the arg connections
    NUCModelCall* cmodelCall = NULL;
    temp_err_code = taskCall(systask, cmodel, context, loc,&cmodelCall);
    *stmt = cmodelCall;
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }
  return err_code;
} // VerilogPopulate::userTask

Populate::ErrorCode
VerilogPopulate::taskCall(veNode ve_systask, NUCModel* cmodel,
			  LFContext* context, const SourceLocator& loc,
			  NUCModelCall** cmodelCall)
{
  ErrorCode err_code = eSuccess;

  // Create the cmodel function. There is only one
  NUCModelFn* cmodelFn = NULL;
  ErrorCode tmp_err_code = taskCModelFn(cmodel, loc, &cmodelFn);
  if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) ) {
    return err_code;
  }

  // Create the cmodel call
  NUNetRefFactory* netRefFactory = context->getNetRefFactory();
  NUModule* module = context->getModule();
  *cmodelCall = new NUCModelCall(cmodel, cmodelFn, loc, netRefFactory, false);

  // Go through the task information and create the arg connections
  CheetahList ve_arg_list(veSysTaskEnableGetExprList(ve_systask));
  NUCModelInterface* cmodelInterface = cmodel->getCModelInterface();
  NUCModelInterface::PortsLoop loop = cmodelInterface->loopPorts();
  veNode ve_arg = NULL;
  UInt32 arg_count = 0;
  if (ve_arg_list != NULL) {
    ve_arg = veListGetNextNode(ve_arg_list);
    while ((ve_arg != NULL) && !loop.atEnd()) {
      // Populate the argument based on the direction
      NUCModelPort* port = *loop;
      PortDirectionT dir = port->getDirection();
      NUCModelArgConnection* conn = NULL;
      bool isNullPort = false;
      switch(dir) {
	case eInput:
	{
	  NUExpr* rhs = NULL;
	  UInt32 size = port->getBitSize(0);
	  ErrorCode tmp_err_code = expr(ve_arg, context, module, size, &rhs);
          rhs = rhs->makeSizeExplicit(size);

          if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) ) {
            return err_code;
          }
	  conn = new NUCModelArgConnectionInput(port, rhs, *cmodelCall, loc);
	}
	break;

	case eOutput:
	{
          // If this is a NULL port then, we don't match it against
          // the argument list. This is a NULL port to represent that
          // the user defined task has side effects. Note that we
          // don't create an arg connection because it really isn't a
          // true argument. In addition, it has side effects as was
          // shown by bug 2231.
	  NULvalue* lhs = NULL;
          if (port->isNullPort()) {
            isNullPort = true;

          } else {
            ErrorCode tmp_err_code = lvalue(ve_arg, context, module, &lhs);
            if (errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code)) {
              return err_code;
            }
            conn = new NUCModelArgConnectionOutput(port, lhs, *cmodelCall,loc);
          }
	}
	break;

	case eBid:
	{
	  NULvalue* lhs = NULL;
	  ErrorCode tmp_err_code = lvalue(ve_arg, context, module, &lhs);
          if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err_code, &err_code ) ) {
            return err_code;
          }
	  conn = new NUCModelArgConnectionBid(port, lhs, *cmodelCall, loc);
	}
	break;
      }

      // Connect the arg connection. This only occurs if this is not
      // the NULL port. The NULL port represents an artificial design
      // output so it does not need an actual arg connection. Also
      // note that the Cheetah routines know nothing of this port so we
      // only go to the next arg if it isn't the NULL port.
      if (!isNullPort) {
        NU_ASSERT((conn != NULL), cmodel);

        UInt32 cmodel_declared_width = port->getBitSize(0);
        UInt32 hdl_width = veExprEvaluateWidth(ve_arg);
        if ( cmodel_declared_width != hdl_width ) {
          mMsgContext->UDTFPortWidthMismatch(&loc, arg_count+1, cmodel_declared_width, hdl_width, cmodel_declared_width);
          // no change to error_code since this is just a warning, and
          // might even be what the user intended
        }
        
        (*cmodelCall)->addArgConnection(conn);
        ve_arg = veListGetNextNode(ve_arg_list);
        ++arg_count;
      }

      // We always move the nucleus iterator because it does contain
      // the null port.
      ++loop;
    } // while
  } // if

  // If the customer put the cHasSideEffect directive at the end we
  // will exit the above loop with one last argument in the c-model
  // interface loop (the NULL port). So remove that now.
  if (!loop.atEnd()) {
    NUCModelPort* port = *loop;
    if (port->isNullPort()) {
      ++loop;
    }
  }

  // Both lists should be at the end
  if ((ve_arg != NULL) || !loop.atEnd()) {
    UInt32 actual = arg_count;
    UInt32 expected = cmodelInterface->numPorts();
    if (cmodelInterface->hasNullPort()) {
      --expected;
    }
    for (; ve_arg != NULL; ve_arg = veListGetNextNode(ve_arg_list), actual++) ;
    mMsgContext->UDTFPortMismatch(&loc, expected, actual);
  }

  return err_code;
} // VerilogPopulate::taskCall
