// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/Elaborate.h"
#include "LFContext.h"
#include "symtab/STSymbolTable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUElabHierHelper.h"

/*
 * Elaborate the given task/function in the given context; create NUNetElab object.
 */
void Elaborate::elabTF(NUTF *tf, LFContext *context, ElabPass pass)
{
  STSymbolTable* symtab = context->getSymbolTable();

  // First pass (locals), create an elaborated TF object.
  // Second pass (hierrefs), just look it up.
  STBranchNode *branch = 0;
  switch (pass) {
    case eElabLocals: {
      NUTFElab *elab = tf->createElab(context->getHier(), symtab);
      branch = elab->getHier();
      break;
    }
    case eElabHierRefs:
    case eElabReconData: {
      branch = NUElabHierHelper::findRealHier(tf->getNameBranch(), context->getHier(), symtab);
      break;
    }
  }
  NU_ASSERT(branch,tf);

  context->pushHier(branch);

  // Elaborate locals during first pass.
  if (pass == eElabLocals) {
    for (NUNetVectorLoop iter = tf->loopArgs();
         not iter.atEnd();
         ++iter) {
      elabNet(*iter, context);
    }
    for (NUNetLoop iter = tf->loopLocals();
         not iter.atEnd();
         ++iter) {
      elabNet(*iter, context);
    }
  }

  // NOTE: once hierrefs are put into TFs, will need to call elabNetHierref
  // for each hierref here.

  for (NUNamedDeclarationScopeLoop iter = tf->loopDeclarationScopes();
       not iter.atEnd();
       ++iter) {
    elabDeclarationScope(*iter, context, pass);
  }
  for (NUBlockLoop iter = tf->loopBlocks();
       not iter.atEnd();
       ++iter) {
    elabBlock(*iter, context, pass);
  }

  context->popHier();
}
