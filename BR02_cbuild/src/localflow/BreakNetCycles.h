// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _BREAKNETCYCLES_H_
#define _BREAKNETCYCLES_H_

class NUNetRefFactory;
class NUModule;
class AtomicCache;
class Fold;
class LFSplitAssigns;
class MsgContext;

//! LFBreakNetCycles class breaks up self referential assignments if possible
/*! This class looks for assignments that appear to be a cycle but in
 *  reality the data flows through different bits of a net. The
 *  following are two test cases taken from customer designs:
 *
 *  \verbatim
 *
 *     wire [8:0] z;
 *     assign z = { ^z[7:0]^par, in };
 *
 *  \endverbatim
 *
 *  In the above example, the assignment has to execute twice to
 *  settle. In the first iteration, the value of in gets propagated to
 *  z[7:0]. In the second iteration z[8] is computed.
 *
 *  \verbatim
 *
 *     wire [3:0] out;
 *     wire [2:0] in;
 *     assign out[0] = 1'b0;
 *     assign out[3:1] = out[2:0] | in[2:0];
 *
 *  \endverbatim
 *
 *  In the above example, the second assignment to out takes three
 *  iterations to settle. This is equivalent to the following code:
 *
 *  \verbatim
 *
 *     assign out[0] = 1'b0;
 *     assign out[1] = out[0] | in[0];
 *     assign out[2] = out[1] | in[1];
 *     assign out[3] = out[2] | in[2];
 *
 *  \endverbatim
 *
 *  Instead of running these statements as cycles, it is better to
 *  break them up into bit assignments which can be executed correctly
 *  in the correct order.
 *
 *  This class attempts to find cases like this and break them up.
 */
class LFBreakNetCycles
{
public:
  //! constructor
  LFBreakNetCycles(NUNetRefFactory* netRefFactory, AtomicCache* strCache,
                   Fold* fold, bool verbose, LFSplitAssigns* splitAssigns);

  //! destructor
  ~LFBreakNetCycles();

  //! Look for self-referential assignments and break them up if possible
  /*! returns true if work was done */
  bool module(NUModule* module);

private:
  //! Hide copy and assign constructors.
  LFBreakNetCycles(const LFBreakNetCycles&);
  LFBreakNetCycles& operator=(const LFBreakNetCycles&);

  // Worker functions
  bool optimizeAssign(NUModule* module, NUAssign* assign);
  NUContAssign* createSubAssign(NUModule* module, 
                                const NULvalue* lvalue,
                                const NUExpr* rvalue, 
                                const SourceLocator& loc,
                                UInt32 lhs_idx,
                                UInt32 rhs_idx);

  // The new and deleted assignments statements due to our work
  NUUseDefSet* mDeletedAssigns;
  NUContAssignList* mNewAssigns;

  // If set, we should print what we do
  bool mVerbose;

  // Utility classes
  NUNetRefFactory* mNetRefFactory;
  AtomicCache* mStrCache;
  Fold* mFold;
  CopyContext* mCopyContext;
  LFSplitAssigns* mSplitAssigns;
};

//! Class to keep track of split up statements
class LFSplitAssigns
{
public:
  //! constructor
  LFSplitAssigns(MsgContext* msgContext);

  //! constructor
  ~LFSplitAssigns();

  //! Enumeration for split reasons
  enum Flags {
    eNone, //!< No flags set
    eCycle = 0x1, //!< Cycle split
    eMixedConst = 0x2 //!< Mixed constant split
  };

  //! Add a split assign
  /*!
    \param loc The location in source of the assign that is being
    split
    \param flags Reason(s) that the assign is being split.
  */
  void add(const SourceLocator& loc, Flags flags);

  //! Print the split assigns in a sorted way
  void print(void) const;

  //! Utility function to help set flags
  static void sSetFlag(Flags* flags, Flags add);

private:
  //! Hide copy and assign constructors.
  LFSplitAssigns(const LFSplitAssigns&);
  LFSplitAssigns& operator=(const LFSplitAssigns&);

  //! Container that does all the work
  struct SplitContainer;
  //! Container for the split assigns
  SplitContainer* mSplitAssigns;

  // Utility classes
  MsgContext* mMsgContext;
}; // class LFSplitAssigns

#endif // _BREAKNETCYCLES_H_
