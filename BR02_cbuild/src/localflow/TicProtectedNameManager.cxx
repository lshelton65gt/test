// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/TicProtectedNameManager.h"


TicProtectedNameManager::TicProtectedNameManager(SourceLocatorFactory *loc_factory) :
  mSourceLocatorFactory(loc_factory)
{}

TicProtectedNameManager::~TicProtectedNameManager()
{}


static void sGenUniqueProtectedName (UtString* s)
{
  char buf[32];
  static int uuid = 0;

  sprintf (buf, "$Prot%0x", uuid++);
  *s += buf;
}


SourceLocator TicProtectedNameManager::locator(veNode ve_node)
{
  CheetahStr filename( veNodeGetFileName(ve_node ) );
  INFO_ASSERT(filename, "unable to determine filename");
  SourceLocator loc;
  loc = mSourceLocatorFactory->create(filename, veNodeGetLineNumber(ve_node));
  return loc;
}


bool TicProtectedNameManager::getVisibleName( veNode ve_node, UtString * buf, bool get_orig_name)
{
  SourceLocator loc = locator(ve_node);

  VeNodeToVisibleNameMap::iterator iter = mVeNodeToVisibleNameMap.find(ve_node);
  if ( iter != mVeNodeToVisibleNameMap.end()) {
    *buf = iter->second;
    return true;
  }

  if ( loc.isTicProtected() ){
    UtString new_protected_name;
    sGenUniqueProtectedName(&new_protected_name);
    *buf = new_protected_name;
    mVeNodeToVisibleNameMap[ve_node] = new_protected_name;
    return true;
  }
  
  if ( get_orig_name && ( VE_MODULE == veNodeGetObjType(ve_node))){
    // special case to handle original name request
    CheetahStr module_name(veModuleGetOriginalName(ve_node));
    *buf = module_name;
    return true;
  }
  return gCheetahGetName(buf, ve_node);
}

veNode TicProtectedNameManager::CheetahFindModule( const char* newModName, veNode ve_root ) {
  CheetahList fileIter(veRootGetFileList( ve_root ));
  while( veNode file = veListGetNextNode( fileIter ) ) {
    CheetahList modIter(veFileGetModuleList( file ));
    while( veNode mod = veListGetNextNode( modIter ) ) {
      UtString buf;
      getVisibleName(mod, &buf);
      if (strcmp( buf.c_str(), newModName ) == 0) {
        return mod;
      }
    }
  }
  // Module not found.
  return NULL;
}

vhNode TicProtectedNameManager::JaguarFindModule( const char* newEntityName ) {
  
  JaguarString libName;
  JaguarList libList(vhGetAllLibraries());
  while (libName = (String) vhGetNextItem(libList)) {
      vhNode e =  vhOpenEntity (libName, newEntityName);
      if(e) return e;
  }
  // Module not found.
  return NULL;
}

// END FILE

