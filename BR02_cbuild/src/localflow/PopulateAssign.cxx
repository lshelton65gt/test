// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "util/CbuildMsgContext.h"
#include <cmath>

//! Helper function to create a continuous assign; code consolidation.
NUContAssign* VerilogPopulate::helperPopulateContAssign(NULvalue *lvalue,
					      NUExpr *rvalue,
					      const SourceLocator& loc,
					      LFContext *context)
{
  StringAtom *block_name = 
    context->getModule()->newBlockName(context->getStringCache(), loc);

  if (!lvalue->isReal () && rvalue->isReal ())
    {
      rvalue = new NUUnaryOp (NUOp::eUnRound, rvalue, loc);
    }

  return new NUContAssign(block_name, lvalue, rvalue, loc);
}


Populate::ErrorCode VerilogPopulate::declAssign(veNode ve_net,
                                                LFContext* context,
                                                NUContAssign **the_assign)
{
  ErrorCode err_code = eSuccess;
  *the_assign = 0;

  veNode ve_rvalue = veNetGetRhsExpr(ve_net);
  if (ve_rvalue == 0) {
    switch (veNetGetNetType(ve_net)) {
    case SUPPLY0_NET:
    case SUPPLY1_NET:
      break;    // Treat supply0/1 as declaration assignments.

    default:
      return eSuccess;
    }
  }

  // Arrays of SUPPLY0 or SUPPLY1 are currently not supported
  if (veNodeGetObjType(ve_net) == VE_NETARRAY)
    return eSuccess;

  SourceLocator loc = locator(ve_net);

  NUNet* net = 0;
  ErrorCode temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_net, &net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  UInt32 lSize = net->getBitSize();
  NUExpr* rvalue = 0;
  if (net->isSupply0()) {
    UtString val(lSize, '0');
    rvalue = NUConst::createKnownConst(val,  net->getBitSize(), loc);
  } else if (net->isSupply1()) {
    UtString val(lSize, '1');
    rvalue = NUConst::createKnownConst(val,  net->getBitSize(), loc);
  } else {
    temp_err_code = expr(ve_rvalue, context, context->getModule(), 0, &rvalue);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  NULvalue* lvalue = new NUIdentLvalue(net, loc);

  *the_assign = helperPopulateContAssign(lvalue, rvalue, loc, context);
  return err_code;
}


// Overloaded
Populate::ErrorCode VerilogPopulate::declAssign(veNode ve_var,
                                                LFContext* context,
                                                NUBlockingAssign **the_assign)
{
  ErrorCode err_code = eSuccess;
  *the_assign = 0;

  //Assume the variable was readonly-initialized...
  veNode ve_rvalue = veVariableGetInitialValue(ve_var);

  if (!ve_rvalue) {
    // Nope - if a 'reg', it might be initialized with a RhsExpr
    switch (veNodeGetObjType (ve_var)) {
    case VE_MEMORY:
    case VE_NETARRAY:
    case VE_ARRAY:
    case VE_INTORTIMEARRAY:
      return eSuccess;          // Don't handle initializations for these yet


    case VE_NET:
      ve_rvalue = veNetGetRhsExpr (ve_var);
      break;

    case VE_REG:
    case VE_INTEGER:
    case VE_TIME:
    case VE_REALTIME:
      if (!ve_rvalue)
        // Nope - look to see if there's an initialValue stored as an attribute
        // on the var decl
        ve_rvalue = veVariableGetInitVal(ve_var);
      break;

    default:
      break;
    }
  }

  if (ve_rvalue == 0)
    return err_code; // No assigned value - should we return eNotApplicable???

  SourceLocator loc = locator(ve_var);

  NUNet* net = 0;
  ErrorCode temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_var, &net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NUExpr* rvalue = 0;
  temp_err_code = expr(ve_rvalue, context, context->getModule(), 0, &rvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NULvalue* lvalue = new NUIdentLvalue(net, loc);
  *the_assign = new NUBlockingAssign (lvalue, rvalue, false, loc);

  return err_code;
}

static const char* strengthString(veDriveStrengthType strength)
{
  const char* strength_string = NULL;
  switch(strength) {
    case UNSET_STRENGTH:
      strength_string = "unset";
      break;

    case SUPPLY_0:
      strength_string = "supply0";
      break;

    case SUPPLY_1:
      strength_string = "supply1";
      break;

    case STRONG_0:
      strength_string = "strong0";
      break;

    case STRONG_1:
      strength_string = "strong1";
      break;

    case PULL_0:
      strength_string = "pull0";
      break;

    case PULL_1:
      strength_string = "pull1";
      break;

    case WEAK_0:
      strength_string = "weak0";
      break;

    case WEAK_1:
      strength_string = "weak1";
      break;

    case HIGHZ_0:
      strength_string = "highz0";
      break;

    case HIGHZ_1:
      strength_string = "highz1";
      break;
  } // switch

  return strength_string;
} // static const char* strengthString

Populate::ErrorCode VerilogPopulate::contAssign(veNode ve_assign,
                                                LFContext* context,
                                                NUContAssign **the_assign)
{
  ErrorCode err_code = eSuccess;
  *the_assign = 0;

  SourceLocator loc = locator(ve_assign);

  // Make sure this isn't a non-supported strength. We only support
  // unset and STRONG_0 for strength0 and STRONG_1 for strength1. Not
  // sure if we should allow STRONG_0 for strength1 and STRONG_1 for
  // strength0.
  veDriveStrengthType strength0 = veContAssignGetStrength0(ve_assign);
  veDriveStrengthType strength1 = veContAssignGetStrength1(ve_assign);
  if ((strength0 != UNSET_STRENGTH) && (strength0 != STRONG_0) &&
      (strength1 != UNSET_STRENGTH) && (strength1 != STRONG_1)) {
    mMsgContext->UnsupportedStrengths(&loc, strengthString(strength0),
                                      strengthString(strength1));
  }

  veNode ve_lvalue = veContAssignGetLvalue(ve_assign);
  veNode ve_rvalue = veContAssignGetRhsExpr(ve_assign);

  NULvalue *the_lvalue = 0;
  ErrorCode temp_err_code = lvalue(ve_lvalue, context, context->getModule(), &the_lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    delete the_lvalue;
    return temp_err_code;
  }

  NUExpr* rvalue = 0;
  temp_err_code = expr(ve_rvalue, context, context->getModule(), 0, &rvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    delete the_lvalue;
    delete rvalue;
    return temp_err_code;
  }

  *the_assign = helperPopulateContAssign(the_lvalue, rvalue, loc, context);
  return err_code;
}


// Create a new integer constant of width 'width' containing the rounded
// value of the real constant supplied.
// in Verilog reals are rounded to the nearest integer, but when there
// are ties they are rounded away from zero:
//  2.9 -> 3,   2.5 -> 3,
// -2.9 -> -3, -2.5 -> -3

static NUConst*
sConvertRealConstantToIntConstant( NUConst *the_const, UInt32 width,
                                   MsgContext *msgContext, SourceLocator &loc )
{
  CarbonReal realval, rounded;
  bool isReal = the_const->getCarbonReal( &realval );
  NU_ASSERT (isReal, the_const);

  // Convert real into an integer part and a fraction -1.0 < frac < 1.0
  CarbonReal frac = std::modf (realval, &rounded);
  if (frac >= 0.5) {
    ++rounded;
  } else if (frac <= -0.5) {
    --rounded;
  }

  if (frac != 0.0 )              // There was a loss of precision
  {
    msgContext->RealConstantRounded( &loc, (double)realval, (SInt64)rounded);
  }
  // Create a new constant the same width as the lvalue of the assignment
  // verilog lrm 4.5.1 says reals converted to integer by type coercion are signed
  NUConst *new_const = NUConst::create(true, (SInt64)rounded, width, loc);


  return new_const;
}


Populate::ErrorCode
VerilogPopulate::blockingAssign(veNode ve_assign,
                                LFContext* context,
                                NUBlockingAssign **the_assign)
{
  ErrorCode err_code = eSuccess;
  *the_assign = 0;

  NUModule *module = context->getModule();

  SourceLocator loc = locator(ve_assign);

  veNode ve_lvalue = veAssignGetLvalue(ve_assign);
  veNode ve_rvalue = veAssignGetRhsExpr(ve_assign);

  NULvalue* the_lvalue = 0;
  ErrorCode temp_err_code = lvalue(ve_lvalue, context, module, &the_lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NUExpr* rvalue = 0;
  temp_err_code = expr(ve_rvalue, context, module, 0, &rvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  
  if ( the_lvalue->isReal() !=  rvalue->isReal() )
  {
    if ( rvalue->isReal() ) {
      // needs to be converted from real
      NUConst *the_const = rvalue->castConst ();
      if ( the_const != NULL )
      {
        NUConst *new_const;
        int lWidth = veExprEvaluateWidth(ve_lvalue);
        new_const = sConvertRealConstantToIntConstant( the_const, lWidth,
                                                       mMsgContext, loc );
        if ( new_const != NULL )
        {
          delete rvalue;
          rvalue = new_const;
        }
      }
      else
      {
        rvalue = new NUUnaryOp (NUOp::eUnRound, rvalue, loc);
      }
    } else {
      // needs to be converted to real
      rvalue = new NUUnaryOp (NUOp::eUnItoR, rvalue, loc);
    }
  }

  *the_assign = new NUBlockingAssign(the_lvalue, rvalue, false, loc);

  return err_code;
}


Populate::ErrorCode
VerilogPopulate::nonBlockingAssign(veNode ve_assign,
                                   LFContext* context,
                                   NUNonBlockingAssign **the_assign)
{
  ErrorCode err_code = eSuccess;
  *the_assign = 0;

  SourceLocator loc = locator(ve_assign);

  veNode ve_lvalue = veAssignGetLvalue(ve_assign);
  veNode ve_rvalue = veAssignGetRhsExpr(ve_assign);

  NULvalue* the_lvalue = 0;
  ErrorCode temp_err_code = lvalue( ve_lvalue, context, context->getModule(),
                                    &the_lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  int lWidth = veExprEvaluateWidth(ve_lvalue);
  NUExpr* rvalue = 0;
  temp_err_code = expr(ve_rvalue, context, context->getModule(), 0, &rvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Allow real constants to be assigned to non-real nets
  if ( !the_lvalue->isReal() && rvalue->isReal() )
  {
    NUConst *the_const = rvalue->castConst ();
    if ( the_const )
    {
      NUConst *new_const;
      new_const = sConvertRealConstantToIntConstant( the_const, lWidth,
                                                     mMsgContext, loc );
      if ( new_const != NULL )
      {
        delete rvalue;
        rvalue = new_const;
      }
    }
    else
    {
      rvalue = new NUUnaryOp (NUOp::eUnRound, rvalue, loc);
    }
  }

  *the_assign = new NUNonBlockingAssign(the_lvalue, rvalue, false, loc);

  return err_code;
}
