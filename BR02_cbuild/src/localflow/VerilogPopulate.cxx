// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "hdl/HdlVerilogPath.h"
#include "localflow/VerilogPopulate.h"
#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUTF.h"
#include "util/CbuildMsgContext.h"
#include "util/AtomicCache.h"
#include "util/CarbonTypeUtil.h"
#include "LFContext.h"
#include "compiler_driver/CarbonContext.h"

VerilogPopulate::VerilogPopulate(DesignPopulate *design_populate,
                                 STSymbolTable *symtab,
				 AtomicCache *str_cache,
				 IODBNucleus * iodb,
				 SourceLocatorFactory *loc_factory,
				 FLNodeFactory *flow_factory,
				 NUNetRefFactory *netref_factory,
				 MsgContext *msg_context,
				 ArgProc *arg,
                                 TicProtectedNameManager* tic_protected_nm_mgr,
				 bool allow_hierrefs,
                                 bool useLegacyGenerateHierarchyNames,
                                 CarbonContext* cc) :
  Populate(msg_context, iodb, design_populate, symtab, str_cache, loc_factory, flow_factory, netref_factory, arg, cc),
  mTicProtectedNameMgr(tic_protected_nm_mgr),
  mAllowHierRefs(allow_hierrefs),   mCheetahContext(cc->getCheetahContext()),
  mUseLegacyGenerateHierarchyNames(useLegacyGenerateHierarchyNames)
{
  createParamModuleMap(mCarbonContext->getFileRoot(), msg_context);
  mHdlVerilogPath = new HdlVerilogPath;
}


VerilogPopulate::~VerilogPopulate()
{
  destroyParamModuleMap();
  delete mHdlVerilogPath;
}


SourceLocator VerilogPopulate::locator(veNode ve_node)
{
  CheetahStr filename( veNodeGetFileName(ve_node ) );
  INFO_ASSERT(filename, "unable to determine filename");
  SourceLocator loc;
  loc = mLocFactory->create(filename, veNodeGetLineNumber(ve_node));
  return loc;
}


veNode
VerilogPopulate::getHierRefMapping(NUModule *module, veNode ve_scopevar)
{
  UtString hier_name(veScopeVariableGetHierName(ve_scopevar));
  StringVeNodeMap& nodeMap = mModuleHiernameNodeMap[module];
  StringVeNodeMap::iterator q = nodeMap.find(hier_name);

  // If we have a hierarchical reference node for this name, return that.
  // Otherwise, just return the given ve_scopevar (an error will end up being printed
  // at the caller, something has gone wrong).
  if (q != nodeMap.end()) {
    return q->second;
  } else {
    return ve_scopevar;
  }
}


Populate::ErrorCode VerilogPopulate::makeHierRefMapping(NUModule *module,
                                                        veNode ve_scopevar,
                                                        NUNet **the_net)
{
  *the_net = 0;

  UtString hier_name(veScopeVariableGetHierName(ve_scopevar));
  StringVeNodeMap& nodeMap = mModuleHiernameNodeMap[module];
  StringVeNodeMap::iterator q = nodeMap.find(hier_name);
  if (q != nodeMap.end()) {
    // hierrefs are looked up via modules not scope
    return mPopulate->lookupNet(module, q->second, the_net);
  } else {
    nodeMap[hier_name] = ve_scopevar;
  }

  return eSuccess;
}


Populate::ErrorCode
VerilogPopulate::rememberPossibleResolutions(NUNetHierRef *net,
                                             VeNodeNamePairList &resolutions,
                                             SourceLocator &loc)
{
  ErrorCode err_code =  eSuccess;
  NetHierRefVeNodeNamePairListMap::iterator iter = mNetPossibleResolutions.find(net);
  if (iter != mNetPossibleResolutions.end()) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical reference net encountered more than once"), &err_code);
    return err_code;
  }
  mNetPossibleResolutions[net] = resolutions;
  return err_code;
}


Populate::ErrorCode VerilogPopulate::mapTask(NUModule *module,
						    NUTask *task,
						    veNode node)
{
  ErrorCode err_code = eSuccess;
  VeNodeTaskMap& taskMap = mTaskMap[module];

  if (taskMap.find(node) != taskMap.end()) {
    SourceLocator loc = locator(node);
    UtString name_buf;
    mTicProtectedNameMgr->getVisibleName(node, &name_buf);

    POPULATE_FAILURE(mMsgContext->TaskRedeclare(&loc, name_buf.c_str()), &err_code);
    return err_code;
  }
  taskMap[node] = task;
  return err_code;
}


Populate::ErrorCode VerilogPopulate::lookupTask(NUModule *module,
						       veNode node,
						       NUTask **the_task)
{
  *the_task = 0;

  ModuleNodeTaskMap::const_iterator p = mTaskMap.find(module);
  if (p == mTaskMap.end()) {
    return eSuccess;
  }

  const VeNodeTaskMap& taskMap = p->second;
  VeNodeTaskMap::const_iterator q = taskMap.find(node);
  if (q != taskMap.end()) {
    *the_task = q->second;
  }
  return eSuccess;
}

//! save information about a hierarchical reference to a task or function that must be resolved at end of population
/*!
 * the first two parameters are mutually exclusive (only one can be non null)
 * \param ve_task  if hier ref is for a task enable this is the cheetah task enable
 * \param ve_function_call if hier ref is for a function call this is the cheetah function call
 * \param enable the nucleus object for this task enable (that will be resolved later)
 * \param scope  the scope of the task enable or function call
 * \param block_scope  the scope for new net declarations
 * \param function_output, the actual net that will hold the output of the function when it is
 * converted to a task.  This is only specified and used if ve_function_call is non null.
 */
Populate::ErrorCode VerilogPopulate::addTaskHierRefToResolve(veNode ve_task_enable,
                                                             veNode ve_function_call,
                                                             NUTaskEnableHierRef *enable,
                                                             NUScope *scope,
                                                             NUScope *block_scope,
                                                             NUNet *function_output)
{
  ErrorCode err_code = eSuccess;  
  NU_ASSERT(( (ve_task_enable == 0) xor (ve_function_call == 0) ), enable);

  if (not mTaskHierRefSet.insertWithCheck(enable)) {
    SourceLocator loc = enable->getLoc();
    if ( ve_task_enable ) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical task enable encountered multiple times"), &err_code);
    } else {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical function call encountered multiple times"), &err_code);
    }
    return err_code;
  }

  
  mTaskHierRefVec.push_back(new TaskHierRefInfo(ve_task_enable,
                                                ve_function_call,
                                                scope, block_scope,
                                                function_output,
                                                enable));
  return err_code;
}


/*!
 * Resolve all tasks which are hierarchically referenced; set up the task pointer
 * and set up the argument connections.
 * This also handles tasks that were created to replace functions.
 */
Populate::ErrorCode VerilogPopulate::resolveTaskHierRefs(LFContext *context)
{
  ErrorCode err_code = eSuccess;

  // Note that the mTaskHierRevVec can expand while we are iterating over it,
  // as we elaborate function calls for the form a.b(c.d())
  // See test/langcov/TaskFunction/tf11_f_3.v
  //
  // So mTaskHierRefVec.size() must be re-called each time (you can't cache
  // the size in a local) because the vector will expand while the loop is
  // running.
  for (UInt32 i = 0; i < mTaskHierRefVec.size(); ++i) {
    TaskHierRefInfo* info = mTaskHierRefVec[i];
    NUTaskEnableHierRef *task_enable = info->mEnable;
    veNode ve_task_enable = info->mVeTaskEnable;
    veNode ve_function_call = info->mVeFunctionCall;
    NUScope *scope = info->mScope;
    NUScope *block_scope = info->mBlockScope;
    NUNet *function_output = info->mFunctionOutput; // non-null if this task is from
                                                    // a function converted to a task
    delete info;

    // Set the appropriate local scope for net lookups
    //    - the scope in which the task enable appeared.
    context->pushDeclarationScope(scope);
    context->pushBlockScope(block_scope);
    context->pushExtraTaskEnableContext();

    ErrorCode temp_err_code = eFatal;
    if ( ve_task_enable != NULL ){
      temp_err_code = resolveTaskEnableOrFunctionCallHierRef(task_enable, ve_task_enable, context, NULL);
    } else if (ve_function_call != NULL) {
      temp_err_code = resolveTaskEnableOrFunctionCallHierRef(task_enable, ve_function_call, context, function_output);
    }
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }
    
    // Some extra taskEnables may need to be added.  Fix for test/hierref/task13.v
    ErrorCode ecode = extraTaskEnables( context, mIODB );
    if ( errorCodeSaysReturnNowOnlyWithFatal( ecode, &err_code ) )
      return ecode;

    context->popBlockScope();
    context->popDeclarationScope();
    context->popExtraTaskEnableContext();
  }

  return err_code;
}



//! checks to make sure there is a unique (and single) resolution for a hier ref for task_enable or function_call
/*!
 * \param actual_for_function_output if not null then we are processing a function call.
 */
Populate::ErrorCode VerilogPopulate::resolveTaskEnableOrFunctionCallHierRef(NUTaskEnableHierRef *task_enable,
                                                                            veNode ve_task_enable_or_function_call,
                                                                            LFContext *context,
                                                                            NUNet *actual_for_function_output)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator( ve_task_enable_or_function_call );
  veNode ve_scopevar = NULL;
  bool processing_task_enable = (actual_for_function_output == NULL);

  if ( processing_task_enable ) {
    ve_scopevar = veTaskEnableGetTask(ve_task_enable_or_function_call);
  } else {
    ve_scopevar = veFuncCallGetFunction(ve_task_enable_or_function_call);
  }
  veNode ve_task_or_function = veScopeVariableGetParentObject(ve_scopevar);
   
  // Iterate over all the resolution start objects.
  // Suppose a scope variable is I.p where master of I is an entity. So it is better
  // to call mvvScopeVariableGetElementList mvv PI rather than cheetah PI 
  // veScopeVariableGetStartObjectList , which works only on verilog boundary.
  CheetahList ve_start_iter( veScopeVariableGetStartObjectList(ve_scopevar) );
  if( !ve_start_iter) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Cannot resolve hierarchical reference"), &err_code);
    return err_code;
  }  
  
  veNode ve_start_node;
  StringAtom *parent_mod_name = 0; // Name of the module containing the resolved net.
  while ((ve_start_node = VeListGetNextNode(ve_start_iter)) != NULL) {
    CheetahList scope_list(veScopeVariableGetUniqueElementList(ve_scopevar, ve_start_node));
    UtStack<veNode> inst_stack; // used to undo elaboration applied during this analysis
    veNode ve_resolved_obj = NULL;
     
    // List is empty, do nothing.
    if( !scope_list )
      continue;

    ErrorCode ecode = findObjectResolution( context, scope_list, inst_stack, parent_mod_name,
                                            ve_resolved_obj, loc );
    if( errorCodeSaysReturnNowWhenFailPopulate( ecode, &err_code ) )
      return ecode;

    undoElaboration( inst_stack );
      
    // For tasks stop when we find the first valid resolution.
    // If there are multiple resolutions, we are screwed anyway.
    if( parent_mod_name && ve_resolved_obj ) 
      break;
  }
  
  if( !parent_mod_name ) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Can not resolve hierarchical reference"), &err_code);
    return err_code;
  } 
  
  NUModule *parent_module = context->lookupModule( parent_mod_name );
  if (not parent_module) {
    POPULATE_FAILURE(mMsgContext->ResolveParent( &loc, parent_mod_name->str() ), &err_code);
    return err_code;
  }

  NUTask *the_task = 0;
  ErrorCode tcode = eFatal;
  tcode = lookupTask(parent_module, ve_task_or_function, &the_task);
  if ( errorCodeSaysReturnNowWhenFailPopulate(tcode, &err_code ) ) {
    return tcode;
  }

  if (not the_task) {
    if ( processing_task_enable ) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Could not resolve task for task enable"), &err_code);
    } else {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Could not resolve function for function call"), &err_code);
    }
    return err_code;
  }

  task_enable->getHierRef()->addResolution(the_task);
  the_task->addHierRef(task_enable);

  NUTFArgConnectionVector arg_conns;
  tcode = tfArgConnections(ve_task_enable_or_function_call, the_task, context, &arg_conns, actual_for_function_output);
  if ( errorCodeSaysReturnNowOnlyWithFatal(tcode, &err_code ) )
  {
    return tcode;
  }

  task_enable->setArgConnections(arg_conns);

  return err_code;
}


Populate::ErrorCode VerilogPopulate::allPossibleNetResolutions(LFContext *context)
{
  ErrorCode err_code = eSuccess;

  for (NetHierRefVeNodeNamePairListMap::iterator iter = mNetPossibleResolutions.begin();
       iter != mNetPossibleResolutions.end();
       ++iter) {
    NUNetHierRef *net_hier_ref = (*iter).first;
    VeNodeNamePairList &possible_resolutions = (*iter).second;
    for (VeNodeNamePairList::iterator iter = possible_resolutions.begin();
	 iter != possible_resolutions.end();
	 ++iter) {
      veNode ve_possible_resolution = (*iter).first;
      StringAtom *module_name = (*iter).second;
      ErrorCode temp_err_code = possibleNetResolutions(net_hier_ref,
                                                       ve_possible_resolution,
                                                       module_name,
                                                       context);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
    }
  }

  return err_code;
}


Populate::ErrorCode
VerilogPopulate::possibleNetResolutions(NUNetHierRef *net_hier_ref,
                                        veNode ve_possible_resolution,
                                        StringAtom *module_name,
                                        LFContext *context)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_possible_resolution);

  // Get the module for the module name.
  NUModule *module = context->lookupModule(module_name);
  if (not module) {
    UtString netName;
    net_hier_ref->getNet()->compose( &netName, NULL );
    POPULATE_FAILURE(mMsgContext->CantFindHierRefModule(&loc, module_name->str(), netName.c_str() ), &err_code);
    return err_code;
  }

  // Get the net map for the module.
  // NOTE: at some time in the future we should probably do the lookup
  // starting at a NUNamedDeclarationScope, and working up to the
  // module, this will support hierrefs to nets declared within a
  // generate FOR loop.
  ScopeToNodeNetMap::iterator iter = mPopulate->mNetMap.find(module);
  if (iter == mPopulate->mNetMap.end()) {
    UtString netName;
    net_hier_ref->getNet()->compose( &netName, NULL );
    POPULATE_FAILURE(mMsgContext->CantFindHierRefModule(&loc, module_name->str(), netName.c_str() ), &err_code);
    return err_code;
  }

  // Lookup the net in the module's map.
  MvvNodeNetMap &node_map = (*iter).second;
  MvvNodeNetMap::iterator resolution_iter;
  resolution_iter = node_map.find( ve_possible_resolution );
  if (resolution_iter != node_map.end()) {
    NUNet *net_resolution = (*resolution_iter).second;
    ErrorCode temp_err_code = possibleNetResolution(net_hier_ref, net_resolution);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }
  } else {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Can not find resolved net in parent module"), &err_code);
    return err_code;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::possibleNetResolution(NUNetHierRef *net_hier_ref,
								  NUNet *net_resolution)
{
  net_hier_ref->addResolution(net_resolution);
  net_resolution->addHierRef(net_hier_ref->getNet());
  return eSuccess;
}


// Given a VE_PARTSEL token, return the net(or value), index expression, and range
// normalized to a [width-1:0] object.
Populate::ErrorCode VerilogPopulate::partsel (veNode ve_partsel,
                                              LFContext *context,
                                              NUModule* module,
                                              NUNet**the_net,
                                              SInt32* value, // used for genvars only 
                                              NUExpr**row_expr,
                                              NUExpr**col_expr,
                                              NUExpr**index_expr,
                                              ConstantRange**range)
{
  ErrorCode err_code = eSuccess;
  *row_expr = 0;
  *index_expr = 0;
  *range = 0;
  *value = -1;                  // indicates invalid value
  *the_net = NULL;


  SourceLocator loc = locator(ve_partsel);

  // Is this an array select or a normal part-select?
  bool arraySelect = veNodeIsA(ve_partsel, VE_ARRAYPARTSELECT) == VE_TRUE;

  // now determine which (of 5) types of a partselect we have: array, genvar, paramater, localparam or net

  veNode ve_named = arraySelect ? veArrayPartSelectGetVariable(ve_partsel) : vePartSelectGetVariable(ve_partsel);
  NUNet* net = 0;
  veObjType type = veNodeGetObjType(ve_named);

  bool isGenvar = false;
  bool isParamOrLocalParam = false;
  if ((not arraySelect) and ( VE_NAMEDOBJECTUSE == type )){
    veNode parent = veNamedObjectUseGetParent(ve_named);
    if ( veNodeGetObjType(parent) == VE_GENVAR ) {
      isGenvar = true;
      CheetahStr temp (veNamedObjectEvaluateValueInBinaryString(parent)); 
      SInt32  genvar_value = veGetDecimalNumberFromBinaryString(temp);
      // *the_net was set to NULL above, genvars have no net
      *value  = genvar_value;
    }
    else if ( sVeNodeIsParameter(parent) ) {
      isParamOrLocalParam = true;
    }
  }

  ErrorCode temp_err_code = eFatal;
  // get the net or value when not genvar
  if ( not isGenvar && not isParamOrLocalParam ){
    temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_named, &net);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }

    // Base object we're selecting into
    *the_net = net;

    NU_ASSERT(!net->is2DAnything() || arraySelect, net);
  }

  veNode ve_range;
  NUVectorNet* vn  = NULL;
  NUMemoryNet* mem = NULL;
  // Collect range and base information for both array and normal selects
  ConstantRange width_range(0,0);
  if (arraySelect)
  {
    mem = net->getMemoryNet();
    if (mem) {
      if ( mem->isResynthesized() ){
        width_range = *(mem->getDeclaredWidthRange());
      } else {
        width_range = *(mem->getRange(0));
      }
    }
    CheetahList partsel_list(veArrayPartSelectGetSelBitList(ve_partsel));
    int list_size = veListGetSize(partsel_list);
    if ( list_size > 2 ) {
      UtString errmsg("Range expressions with ");
      errmsg << list_size;
      errmsg << " index expressions are not supported.";
      mMsgContext->CheetahConsistency(&loc,errmsg.c_str());
      temp_err_code = eFailPopulate;
      return temp_err_code;
    }
        
                                      
    veNode ve_row_select = veListGetNextNode(partsel_list);
    ve_range = veArrayPartSelectGetRange(ve_partsel);
    temp_err_code = expr(ve_row_select,
                         context,
                         module,
                         0,
                         row_expr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    if (list_size > 1) {
      veNode ve_col_select = veListGetNextNode(partsel_list);
      temp_err_code = expr(ve_col_select,
                         context,
                         module,
                         mem->getRangeSize(1), // must use index 1 here because current code does not support more than 2 index expressions (see list_size>2 check above), and this is the second index
                         col_expr);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
    } else {
      *col_expr = NULL;
    }

  }
  else
  {
    if ( isGenvar ) {
      width_range = ConstantRange (31,0); // genvars are the size of verilog integers
    } else if ( isParamOrLocalParam ) {
      (void) sGetParameterRange(veNamedObjectUseGetParent(ve_named), &width_range);
    } else {
      vn = dynamic_cast<NUVectorNet*>(net);
      if (vn)
        width_range = *vn->getDeclaredRange ();
    }
    ve_range = vePartSelectGetSelectedBit(ve_partsel);
  }
  
#if 0
  // We are doing signed indexing if the declared range crosses zero, or if
  // the declared range is little endian.
  //
  bool signedIndexing = ((width_range.getMsb () ^ width_range.getLsb ()) > 0)
    or not width_range.bigEndian ();

  // Calculate the index expression to a precision sufficient to address the
  // entire object.
  //
  UInt32 lWidth = 1 + (arraySelect ? Log2(mem->getBitSize())
                       : isGenvar ? Log2(32)
                       : Log2(net->getBitSize()));

  if (signedIndexing)
    ++lWidth;                   // extra bit of precision for signed arithmetic
#else
  UInt32 lWidth = 32;           // should be good for all nets/memories
#endif

  ConstantRange *parsedRange;

  // Three cases:
  //
  //	1) Constant Range : x[3:1]
  // 	2) Variable LSB    : x[i +: 2]
  // 	3) Variable MSB    : x[i -: 2]
  //
  // They are distinguished by the sign-type
  bool isConstIndex = false;
  veSignType sType = veRangeGetSignType (ve_range);
  switch (sType)
  {
    case POS_SIGN:
    case NEG_SIGN:
    {
      veNode ve_index = veRangeGetLeftRange (ve_range);
      temp_err_code = expr (ve_index, context, module, 0, index_expr);
      veNode ve_width = veRangeGetRightRange (ve_range);
      SInt32 width = veExprEvaluateValue (ve_width);
      
      // Range is specified in normalized form offset from the index..
      // 
      // For a +:width, and little-endian vector, we adjust the index
      //
      // For a -:width, and big-endian vector, we adjust the index
      //
      parsedRange = new ConstantRange (width-1, 0);
    }
    break;
    
    case UNSET_SIGN: 
    {
      // constantRange initializes the range pointer
      temp_err_code = constantRange(ve_range, &parsedRange);
      *index_expr = NUConst::create (false, 0,  lWidth, loc);
      isConstIndex = true;
    }
    break;
  }

  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    delete *row_expr;
    row_expr = 0;
    delete *col_expr;
    col_expr = 0;
    delete *index_expr;
    index_expr = 0;
    delete parsedRange;
    return temp_err_code;
  }
  
  if ( isConstIndex ) 
  {
    if ( !width_range.contains(*parsedRange) ) 
    {
      // Can't tell at compile time if  v[k+:3] is "in-range or not..."
      UtString buf;
      if ( vn ) {
	vn->compose(&buf, NULL);
      } else if ( mem ) {
	mem->compose(&buf, NULL);
      } else if ( isGenvar ) {
	veNode parent = veNamedObjectUseGetParent(ve_named);
	UtString name_buf;
	mTicProtectedNameMgr->getVisibleName(parent, &name_buf);
	buf << "(really a genvar) " << name_buf;
      } else {        // must be a scalar
	net->compose(&buf,NULL);
      }
      mMsgContext->LFPartRangeWarning(&loc,
				      parsedRange->getMsb(), parsedRange->getLsb(),
				      buf.c_str(),
				      width_range.getMsb(), width_range.getLsb());
      if (width_range.overlaps (*parsedRange))
	*parsedRange = width_range.overlap(*parsedRange);
    }

    if ( width_range.isFlipped(*parsedRange) ) {
      UtString buf;
      if ( vn ) {
	vn->compose(&buf, NULL);
      } else if ( mem ) {
	mem->compose(&buf, NULL);
      } else if ( isGenvar ) {
	veNode parent = veNamedObjectUseGetParent(ve_named);
	UtString name_buf;
	mTicProtectedNameMgr->getVisibleName(parent, &name_buf);
	buf << "(really a genvar) " << name_buf;
      } else {        // must be a scalar
	net->compose(&buf,NULL);
      }
      POPULATE_FAILURE(mMsgContext->FlippedPartRange(&loc,
						     parsedRange->getMsb(), parsedRange->getLsb(),
						     buf.c_str(),
						     width_range.getMsb(), width_range.getLsb()),
		       &err_code);


      delete parsedRange;
      parsedRange = 0;

      return err_code;
    }
  }

  // normalize all bit references, synthesizing corrective
  // arithmetic as needed.

  // Convert to internal representation.  Always remap the range
  // into [width-1 : 0], and if the index expression is non-constant
  // must renormalize it also.
  //

  if (not isConstIndex) {
    SInt32 adjust = (width_range.bigEndian() ^ (sType == POS_SIGN)) ?
      parsedRange->getLength() -1 : 0;
    
    if (vn)
      *index_expr = vn->normalizeSelectExpr(*index_expr, adjust, false);
    else if (mem)
      *index_expr = mem->normalizeSelectExpr(*index_expr, 0, adjust, false);
    else {
      // must be index on genvar or parameter
      *index_expr = NUNet::sNormalizeExpr(*index_expr, width_range, width_range, adjust,  false);
    }
  } else {
    // Constant range is still in units of the declared net, convert to [n:0] normalized form
    parsedRange->normalize(&width_range, false);
  }

  *range = parsedRange;
  return err_code;
}


Populate::ErrorCode VerilogPopulate::findIfModuleOrUDP( veNode ve_module,
                                                        bool   *isUDP)
{
  ErrorCode err_code = eSuccess;  
  veObjType type = veNodeGetObjType(ve_module);
  if( type == VE_MODULE ) {
    *isUDP = false;
  }
  else if( type == VE_UDP ) {
    *isUDP = true;
  }
  else {
    UtString name_buf;
    mTicProtectedNameMgr->getVisibleName(ve_module, &name_buf);
    SourceLocator loc = locator(ve_module);
    POPULATE_FAILURE(mMsgContext->UnsupportedInstanceType(&loc, name_buf.c_str(), gGetObjTypeStr(type)), &err_code);
    return err_code;
  }
  return err_code;
}

bool VerilogPopulate::getVisibleName(veNode ve_node, UtString* buf, bool get_orig_name) {
  return mTicProtectedNameMgr->getVisibleName(ve_node, buf, get_orig_name);
}
