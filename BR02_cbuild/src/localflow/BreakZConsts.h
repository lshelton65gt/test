// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef __BREAKZCONTS_H_
#define __BREAKZCONTS_H_

#include "nucleus/Nucleus.h"

class MsgContext;
class Fold;
class ConstantRange;
class DynBitVector;
class IODBNucleus;
class NUNetRefFactory;

/*!
  \file
  Declaration of local tristate package.
 */

//! Class which detects local tristates.
/*!
  Detect tristate drivers, and create NUEnabledDriver nodes for them.
*/
class BreakZConsts
{
public:
  //! constructor
  /*!
    \param msg_ctx Context to issue error messages.
    \param verbose If true, will be verbose while analysis occurs.
    \param fold Folding context to simplify split assignment LHS and RHS
   */
  BreakZConsts(MsgContext *msg_ctx,
               bool verbose,
               Fold* fold,
               IODBNucleus* iodb,
               NUNetRefFactory* netRefFactory) :
    mMsgContext(msg_ctx),
    mVerbose(verbose),
    mFold(fold),
    mScope(NULL),
    mIODB(iodb),
    mNetRefFactory(netRefFactory)
  {}

  //! destructor
  ~BreakZConsts() {}

  //! Perform tristate analysis over this module.
  bool module(NUModule *module);

  //! Split Z assignments on this continuous assign.
  /*!
    Take assignments to constants that include Z bits and split those
    bits out.  Leave no assignment behind to constant Z -- just eliminate
    the assign altogether, and only assign bits that are non-Z.

   \param assign The continuous assign to analyze
   \param assign_list List to which continuous assigns will be added
   \param mixedConst assignment split due to a mixed z constant
   \param concat assignment split due to an LHS concat with Z on the RHS
   \return true if the continuous assign list was changed
  */
  bool processAssign(NUAssign *assign,
                     NUStmtList *assign_list);

  //! helper class to break up Zs in procedural assignments
  class ProceduralZCallback;

private:
  //! Hide copy and assign constructors.
  BreakZConsts(const BreakZConsts&);
  BreakZConsts& operator=(const BreakZConsts&);

  //! Make an assignment that just covers range endBit:startBit,
  //! and check to see whether it's either all z-producing or totally
  //! z-free
  bool splitAssignment(NUAssign *assign, NUStmtList *assign_list,
                       const ConstantRange& range, bool expectZ);

  //! Split an expression, hopefully into one that's pure Z or purely non-Z.
  //! Issue a warning/alert if an impure conversion occurred, but return the
  //! split expression anyway
  NUExpr* splitExpr(NUExpr* expr, const ConstantRange& range, bool expectZ);

   //! Split an lvalue based on the specified range, return NULL upon failure
  NULvalue* splitLvalue(NULvalue* lval, const ConstantRange& range);

  //! Split an assignment into a set of assigns that produce z's uniformly
  bool splitZAssigns(NUAssign *assign, NUStmtList *assign_list,
                     const DynBitVector& zbits);

  //! Split concat-assigns where the RHS drives Z
  bool splitConcatAssigns(NUAssign *assign, NUStmtList *assign_list,
                          NUConcatLvalue* clval);

  //! We only delete continuous assigns, because the DesignWalker
  //! will take care of deleting blocking assigns when the callback
  //! returns eDelete
  static bool shouldDeleteAssign(NUAssign* assign);

  //! Split Z assignments on this continuous assign.
  /*!
    Take assignments to constants that include Z bits and split those
    bits out.  Leave no assignment behind to constant Z -- just eliminate
    the assign altogether, and only assign bits that are non-Z.

   \param assign The continuous assign to analyze
   \param assign_list List to which continuous assigns will be added
   \param mixedConst assignment split due to a mixed z constant
   \param concat assignment split due to an LHS concat with Z on the RHS
   \return true if the continuous assign list was changed
  */
  bool processAssignHelper(NUAssign *assign,
                           NUStmtList *assign_list,
                           bool* mixedConst,
                           bool* concat);

  //! Message context.
  MsgContext *mMsgContext;

  //! Be verbose as processing occurs?
  bool mVerbose;

  //! Fold context
  Fold* mFold;

  //! Scope for calling gensym
  NUScope* mScope;

  IODBNucleus* mIODB;
  NUNetRefFactory* mNetRefFactory;
};

#endif
