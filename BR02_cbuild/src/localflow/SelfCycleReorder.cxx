// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "SelfCycleReorder.h"
#include "localflow/UD.h"
#include "localflow/Reorder.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUDesignWalker.h"
#include "reduce/REUtil.h"
#include "util/Graph.h"


SelfCycleReorder::SelfCycleReorder(NUModule *module,
                                   AtomicCache *str_cache,
                                   NUNetRefFactory *netref_factory,
                                   IODBNucleus *iodb,
                                   ArgProc *args,
                                   MsgContext *msg_ctx,
                                   AllocAlias *alloc_alias) :
  mReorder(new Reorder(str_cache, netref_factory, iodb, args, msg_ctx, alloc_alias, true, false, false, false)),
  mUD(new UD(netref_factory, msg_ctx, iodb, args, false)),
  mModule(module),
  mNetRefFactory(netref_factory),
  mIODB(iodb),
  mRecomputeUD(false)
{
}


SelfCycleReorder::~SelfCycleReorder()
{
  delete mReorder;
  delete mUD;
}


void SelfCycleReorder::analyze()
{
  ReduceUtility re_util(mModule, mNetRefFactory);
  re_util.update();

  for (NUModule::AlwaysBlockLoop loop = mModule->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock *block = *loop;

    // Skip sequential blocks; they don't have self-cycle problems.
    if (not block->isSequential()) {
      alwaysBlock(block, re_util);
    }
  }

  if (mRecomputeUD) {
    mUD->module(mModule);
  }
}


/*!
 * Specialized graph walker to determine if the graph has cycles.
 * The traversal will "fail" if a cycle has been found.
 */
class GraphCycleFinder : public GraphWalker
{
public:
  GraphCycleFinder() {}
  ~GraphCycleFinder() {}

  Command visitBackEdge(Graph*, GraphNode*, GraphEdge*)
  {
    return GW_FAIL;
  }
};


void SelfCycleReorder::alwaysBlock(NUAlwaysBlock *block, ReduceUtility &re_util)
{
  NUNetRefSet cycles(mNetRefFactory);
  bool try_reorder = qualifyTryReorder(block, cycles, re_util);

  if (not try_reorder) {
    return;
  }

  NUStmtLoop stmt_loop = block->getBlock()->loopStmts();
  NUStmtList stmt_list(stmt_loop.begin(), stmt_loop.end());

  StmtGraph *graph = mReorder->createStmtGraph(&stmt_list, &cycles, &re_util);

  // If there is a cycle in the statement graph then we cannot eliminate the
  // self-cycle by reordering.
  GraphCycleFinder cycle_finder;
  bool is_cycle = not cycle_finder.walk(graph);
  if (is_cycle) {
    mReorder->freeGraph(graph);
    return;
  }

  // Layout the statement list to remove the self-cycle.
  // layoutStmtList will free the graph.
  NUStmtList new_list;
  mReorder->layoutStmtList(&stmt_list, &new_list, graph);
  // dump our progress, if there was any.
  mReorder->dumpOrdering(&stmt_list, &new_list);
  block->getBlock()->replaceStmtList(new_list);

  mRecomputeUD = true;
}


bool SelfCycleReorder::qualifyTryReorder(NUAlwaysBlock *block,
                                         NUNetRefSet &cycles,
                                         ReduceUtility &re_util)
{
  // Test if any of the defs of this block are in the use set.
  NUNetRefSet defs(mNetRefFactory);
  NUNetRefSet uses(mNetRefFactory);

  block->getDefs(&defs);
  block->getUses(&uses);

  NUNetRefSet::set_intersection(defs, uses, cycles);

  // Discard any nets which are driven by this always block and also driven
  // somewhere else (has more than 1 continuous def).  This will mean that
  // we will not reorder statements which def these nets.
  //
  // This is being conservative, there might be cases where this is ok.
  NUNetRefSet discards(mNetRefFactory);
  for (NUNetRefSet::iterator iter = cycles.begin();
       iter != cycles.end();
       ++iter) {
    NUNetRefHdl ref = *iter;
    if (re_util.getDefCount(ref) > 1) {
      discards.insert(ref);
    }
  }

  NUNetRefSet::set_subtract(discards, cycles);

  // If no overlap, nothing to do.
  if (cycles.empty()) {
    return false;
  } else {
    return true;
  }
}
