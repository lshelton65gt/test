// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/UserTypePopulate.h"
#include "iodb/IODBDesignData.h"
#include "iodb/IODBUserTypes.h"
#include "iodb/IODBNucleus.h"
#include "localflow/DesignPopulate.h"
#include "compiler_driver/JaguarBrowser.h"
#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCompositeNet.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"

static StringAtom* sGetNetName(NUNet* net)
{
  StringAtom* netAtom = NULL;
  if (net->isCompositeNet()) { // Use original name of composite net.
    NUCompositeNet* cnet = net->getCompositeNet();
    netAtom = cnet->getOriginalName();
  } else {
    netAtom = net->getName();
  }
  return netAtom;
}

// A scalar is considered a vector type if arrays of them are equivalent
// (from a cbuild memory resynthesis standpoint) to a Verilog vector.
// This is used to populate the isPacked UserArray flag.
static bool sIsVectorType(const UserType* ut) 
{
  bool vec = false;
  if (ut && (ut->getType() == UserType::eScalar)) {
    CarbonVhdlType type = ut->getVhdlType();
    if (   (eVhdlTypeStdLogic  ==  type)
        || (eVhdlTypeStdULogic == type)
        || (eVhdlTypeBit       == type)
        || (eVhdlTypeBoolean   == type)
      )
      vec = true;
  }
  return vec;
}
  

class TypeInfo
{
public:
  TypeInfo() :
      mType(UserType::eScalar),
      mVhdlType(eVhdlTypeUnknown),
      mIsRange(false),
      mCompositeTypeName(NULL),
      mNet(NULL),
      mRangeRequiredInDeclaration(false),
      mLibraryName(NULL),
      mPackageName(NULL)
  {
  }
 
  TypeInfo(UserType::Type type, CarbonVhdlType vhdlType) :
      mType(type),
      mVhdlType(vhdlType),
      mIsRange(false),
      mCompositeTypeName(NULL),
      mNet(NULL),
      mRangeRequiredInDeclaration(false),
      mLibraryName(NULL),
      mPackageName(NULL)
  {
  }

  TypeInfo(UserType::Type type, CarbonVhdlType vhdlType, const char* compositeTypeName,
           UtString *libName = NULL, UtString *packName = NULL) :
      mType(type),
      mVhdlType(vhdlType),
      mIsRange(false),
      mNet(NULL),
      mRangeRequiredInDeclaration(false),
      mLibraryName(NULL),
      mPackageName(NULL)
  {
    if (strlen(compositeTypeName) > 0)
    {
      mCompositeTypeName = new UtString(compositeTypeName);
      mCompositeTypeName->lowercase();
    }
    else
    {
      mCompositeTypeName = NULL;
    }

    if((libName != NULL) && (packName != NULL) &&
       (libName->empty() == false) && (packName->empty() == false))
    {
      mLibraryName = new UtString(libName->c_str());
      mPackageName = new UtString(packName->c_str());
    }
  }

  TypeInfo(const TypeInfo& typeInfo)
  {
    mType = typeInfo.getType();
    mVhdlType = typeInfo.getVhdlType();
    mIsRange = typeInfo.isRange();
    mRange = typeInfo.getRange();
    UtString* compositeTypeName = typeInfo.getCompositeTypeName();
    if (compositeTypeName != NULL)
    {
      mCompositeTypeName = new UtString(*compositeTypeName);
      mCompositeTypeName->lowercase();
    }
    else
    {
      mCompositeTypeName = NULL;
    }

    mLibraryName = NULL;
    mPackageName = NULL;
    UtString* libName = typeInfo.getLibraryName();
    UtString* packName = typeInfo.getPackageName();
    if(libName != NULL)
      mLibraryName = new UtString(libName->c_str());

    if(packName != NULL)
      mPackageName = new UtString(packName->c_str());

    mNet = typeInfo.getNet();
    mEnumElems = typeInfo.getEnumElems();    
    mEnumEncodings = typeInfo.getEnumEncodings();
    mRangeRequiredInDeclaration = typeInfo.isRangeRequiredInDeclaration();
  }


  virtual ~TypeInfo() {
    delete mCompositeTypeName;
    delete mLibraryName;
    delete mPackageName;
  }

  void setType(UserType::Type type) {
    mType = type;
  }

  UserType::Type getType() const {
    return mType;
  }

  void setVhdlType(CarbonVhdlType vhdlType) {
    mVhdlType = vhdlType;
  }

  CarbonVhdlType getVhdlType() const {
    return mVhdlType;
  }

  void setRange(ConstantRange range) {
    mRange = range;
    mIsRange = true;
  }

  bool isRange() const {return mIsRange;};

  // Array range or integer constraint.
  ConstantRange getRange() const {
    return mRange;
  }

  UtString* getCompositeTypeName() const {
    return mCompositeTypeName;
  }

  UtString* getLibraryName() const {
    return mLibraryName;
  }

  UtString* getPackageName() const {
    return mPackageName;
  }
  

  NUNet* getNet() const { return mNet; }

  void setNet(NUNet* net) { mNet = net; }

  void setEnumElems(JaguarList enumElems) { mEnumElems = enumElems; }

  const JaguarList& getEnumElems() const { return mEnumElems; }

  void addEnumEncoding(const UtString& enumEncoding) 
  { 
    mEnumEncodings.push_back(enumEncoding); 
  }

  const UtStringArray& getEnumEncodings() const { return mEnumEncodings; }

  bool isRangeRequiredInDeclaration() const { return mRangeRequiredInDeclaration; }

  void setIsRangeRequiredInDeclaration(bool rangeRequiredInDeclaration) { mRangeRequiredInDeclaration = rangeRequiredInDeclaration; }

private:
  TypeInfo& operator=(const TypeInfo&);

  UserType::Type         mType;
  CarbonVhdlType   mVhdlType;
  
  bool          mIsRange; // does this type have a range
  ConstantRange mRange;
  UtString*     mCompositeTypeName;
  NUNet*        mNet;       // This is used only for records
  JaguarList    mEnumElems; // This is only used for enum
  UtStringArray mEnumEncodings; // Only used if enum has the ENUM_ENCODINGS attribute
  bool          mRangeRequiredInDeclaration;
  UtString*     mLibraryName;
  UtString*     mPackageName;
};

typedef  UtList<TypeInfo> TypeInfoStack;
typedef TypeInfoStack::const_reverse_iterator TypeInfoStackCRIter;
typedef TypeInfoStack::const_iterator TypeInfoStackCIter;

class TypeInfoCB : public JaguarBrowserCallback
{
public:

  TypeInfoCB(UserTypeFactory* typeFact, 
	     NUNet* net, 
	     TypeInfoStack* tiStack, 
	     MsgContext* msgContext,
	     const NUNetToConstraintRangeMap* constraintRangeMap)
    : mTypeFactory(typeFact),
      mNet(net),
      mMemDim(0),
      mCompDim(0),
      mTypeInfoStack(tiStack),
      mCurrentNet(net),
      mConstraintRangeMap(constraintRangeMap),
      mMsgContext(msgContext),
      mEnumAttrb(NULL),
      mNeedsRangeInDeclaration(false)
  {
  }

  virtual ~TypeInfoCB()
  {
  }

  Status vhsignal(Phase phase, vhNode node)
  {
    if (phase == ePre)
    {
      vhNode subTypeInd = vhGetSubTypeInd(node);
      vhNode constraint = vhGetConstraint(subTypeInd);
      // if the signal was declared with a range, store that fact!
      if (constraint != NULL)
      {
	mNeedsRangeInDeclaration = true;
      }
    }
    return eNormal;
  }

  Status vhrecord(Phase phase, vhNode /*node*/)
  {
    // Record type definition
    if (phase == ePre) 
    {
      TypeInfo tiRec(UserType::eStruct, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
      mTypeName = "";
      tiRec.setNet(mCurrentNet);
      mTypeInfoStack->push_back(tiRec);
    }
    
    // End of record type definition
    if (phase == ePost) 
    {
      TypeInfo tiRec(UserType::eStructEnd, eVhdlTypeUnknown);
      mTypeInfoStack->push_back(tiRec);
    }
    return eNormal;
  }

  Status vhrecordelement(Phase phase, vhNode node) {
    if (phase == ePre) 
    {
      JaguarString vhStr(vhGetRecEleName(node));
      if(mCurrentNet->isCompositeNet())
      {
        NUCompositeNet * cnet = mCurrentNet->getCompositeNet();
        UInt32 numFields = cnet->getNumFields();
        for(UInt32 i = 0; i < numFields; i++)
        {
          NUNet* field = cnet->getField(i);
          StringAtom* fName = sGetNetName(field);
          if(strcasecmp(fName->str(), vhStr) == 0)
          {
            NUNamedDeclarationScope  *scope = dynamic_cast <NUNamedDeclarationScope*> (field->getScope());
            if(scope != NULL)
            {
              NUScopeNetMap::const_iterator citr = mScopeToCompMap.find(scope);
              if (citr == mScopeToCompMap.end()) 
                mScopeToCompMap[scope] = dynamic_cast <NUCompositeNet*> (mCurrentNet);
            }

            mCurrentNet = field;
            mCompDim = 0;
            mMemDim = 0;
            break;
          }
          
        }
      }
      else
        NU_ASSERT(false, mNet);
    }

    if (phase == ePost) 
    {
      NUNamedDeclarationScope *scope = dynamic_cast <NUNamedDeclarationScope*> (mCurrentNet->getScope());
      NUCompositeNet *cnet = mScopeToCompMap[scope];
      if(cnet != NULL)
        mCurrentNet = cnet;
    }
    return eNormal;
  }

  Status vhconstrainedarray(Phase phase, vhNode node)
  {
    if (phase == ePre) {
      UInt32 numDims = getConstrainedArrayDims(node);
      if (!maybeHandleNullString(numDims)) {
        pushArrayType(numDims);
      }
    }
    return eNormal;
  }

  Status vhunconstrainedarray(Phase phase, vhNode node)
  {
    if (phase == ePre) {
      UInt32 numDims = getUnconstrainedArrayDims(node);
      if (!maybeHandleNullString(numDims)) {
	pushArrayType(numDims);
      }
    }
    return eNormal;
  }

  Status vhindexunconstrained(Phase /*phase*/, vhNode /*node*/)
  {
    return eSkip;
  }
 
  Status vhrange(Phase /*phase*/, vhNode /*node*/)
  {
    return eSkip;
  }
 
  Status vhrangeconstraint(Phase /*phase*/, vhNode /*node*/)
  {
    return eSkip;
  }
  
  Status vhindexconstraint(Phase /*phase*/, vhNode /*node*/)
  {
    return eSkip;
  }

  Status vhinttype(Phase phase, vhNode /*node*/)
  {
    if (phase == ePre)
    {
      // The integer type declared in standard.vhd.93 package is handled as a special
      // case in vhsubtypeind, even though it is an inttype as well.
      TypeInfo tiIntType(UserType::eScalar, eVhdlTypeIntType, mTypeName.c_str());
      const ConstantRange* cr = getConstraintRange();
      if (cr != NULL)
      {
	tiIntType.setRange(*cr);
	tiIntType.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
      }
  
      mTypeInfoStack->push_back(tiIntType);
    }
    return eNormal;
  }

  Status vhenumtype(Phase phase, vhNode node)
  {
    if (phase == ePre) 
    {
       TypeInfo tiEnum(UserType::eEnum, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
       tiEnum.setEnumElems(vhGetEnumEleList(node));

       // If the ENUM_ENCODING attribute exists, preserve the encodings
       if (mEnumAttrb)
       {
	 vhExpr stringLit = vhGetAttribValue(static_cast<vhExpr>(mEnumAttrb));

	 // Only save the enum encodings if it is a simple string
	 if (vhIsA(stringLit, VHSTRING))
	 {
	   UtString enumStr(vhGetStringVal(stringLit));
	   UtString::iterator i = enumStr.begin();
	   UtString::iterator s = enumStr.begin();
	   while ( i != enumStr.end() )
	   {
	     while (*i == ' ' ) 
	     { 
	       i++;
	       s++;
	     }
	     if (i == enumStr.end() )
	     {
	       break;
	     }
	     while (*i != ' ' && i != enumStr.end() ) i++;
      
	     UtString value;
	     value.assign(s, i);
	     tiEnum.addEnumEncoding(value);
      
	     if ( i == enumStr.end() )
	     {
	       break;
	     }
	     else
	     {
	       s = i;
	     }
	   }
	 }
       }
       mTypeInfoStack->push_back(tiEnum);
       return eSkip;
    }
    return eNormal;
  }


  Status vhenumelement(Phase phase, vhNode node) 
  {
    if (phase == ePre)
    {
      if (node != NULL)
      {
        return eNormal;
      }
    }
    return eNormal;
  }

  Status vhtypedecl(Phase phase, vhNode node)
  {
    if (phase == ePre)
    {
      // If this is an enum with an ENUM_ENCODING attribute, preserve it
      mEnumAttrb = vhGetAttribSpecOnObject("ENUM_ENCODING", node);
      if (mTypeName.empty())
      {
	JaguarString typeName(vhGetTypeName(node));
	mTypeName = static_cast<const char*>(typeName);

        vhNode vh_scope = vhGetScope(node);
        if(vhGetObjType(vh_scope) == VHPACKAGEDECL)
        {
          JaguarString libName(vhGetLibName(node));
          mLibraryName = static_cast<const char*>(libName);

          JaguarString packName(vhGetPackName(vh_scope));
          mPackageName = static_cast<const char*>(packName);
        }
      }
    }
    return eNormal;
  }

  Status vhsubtypedecl(Phase phase, vhNode node)
  {
    if (phase == ePre)
    {
      if (mTypeName.empty())
      {
	JaguarString typeName(vhGetTypeName(node));
	mTypeName = static_cast<const char*>(typeName);

        vhNode vh_scope = vhGetScope(node);
        if(vhGetObjType(vh_scope) == VHPACKAGEDECL)
        {
          JaguarString libName(vhGetLibName(node));
          mLibraryName = static_cast<const char*>(libName);

          JaguarString packName(vhGetPackName(vh_scope));
          mPackageName = static_cast<const char*>(packName);
        }
      }
    }
    return eNormal;
  }

  Status vhsubtypeindication(Phase phase, vhNode node)
  {
    vhNode origType = vhGetOrgType(node);
    if (phase == ePre) 
    {
      if (vhGetObjType(origType) == VHALIASDECL) 
      {
        mTypeInfoStack->push_back(TypeInfo());
        vhNode typeDef = vhGetTypeDef(node);
        sGetTypeInfo(mTypeFactory, typeDef, mNet, mConstraintRangeMap,
		     mTypeInfoStack, mMsgContext);
        return eSkip;
      }

      JaguarString objType(vhGetName(origType));
      CarbonVhdlType stdLogicType = eVhdlTypeUnknown;
      if(strcasecmp(objType, "STD_LOGIC") == 0)
        stdLogicType = eVhdlTypeStdLogic;
      else if(strcasecmp(objType, "STD_ULOGIC") == 0)
        stdLogicType = eVhdlTypeStdULogic;
      else if(strcasecmp(objType, "BIT") == 0)
        stdLogicType = eVhdlTypeBit;
      else if(strcasecmp(objType, "BOOLEAN") == 0)
        stdLogicType = eVhdlTypeBoolean;
      
      if(stdLogicType != eVhdlTypeUnknown)
      {
	TypeInfo ti(UserType::eScalar, stdLogicType, mTypeName.c_str());
	mTypeInfoStack->push_back(ti);
        return eSkip;
      }
      
      CarbonVhdlType stdLogicVectorType = eVhdlTypeUnknown;
      if(strcasecmp(objType, "STD_LOGIC_VECTOR") == 0)
        stdLogicVectorType = eVhdlTypeStdLogic;
      else if(strcasecmp(objType, "STD_ULOGIC_VECTOR") == 0)
        stdLogicVectorType = eVhdlTypeStdULogic;
      else if(strcasecmp(objType, "BIT_VECTOR") == 0)
        stdLogicVectorType = eVhdlTypeBit;
      else if(strcasecmp(objType, "STRING") == 0)
        stdLogicVectorType = eVhdlTypeChar;

      // If the unknown vector type is constrained, make sure that
      // information is propagated so that unconsrained arrays are
      // not created.
      if (stdLogicVectorType != eVhdlTypeUnknown)
      {
        TypeInfo tiAr(UserType::eArray, 
		      stdLogicVectorType, 
		      (mTypeName.empty()) ? static_cast<const char*>(objType) : mTypeName.c_str(),
                      &mLibraryName, &mPackageName);
	tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
        const ConstantRange *range = NULL;
        if(mCurrentNet->isVectorNet())
        {
          NUVectorNet *vecNet = mCurrentNet->castVectorNet();
          range = vecNet->getDeclaredRange();
        }
        else if(mCurrentNet->isMemoryNet())
        {
          NUMemoryNet *memNet = mCurrentNet->getMemoryNet();
	  if(strcasecmp(objType, "STRING") == 0)
	    range = memNet->getRange(1);
	  else
	    range = memNet->getRange(0);
        }
	else if(mCurrentNet->isBitNet())
	{
	  // No user type will be created here. See bug 8360 for more details.
	  mMsgContext->ConstantWithNoSize(mCurrentNet);
	  return eSkip;
	}
        else
        {
          // It has to be either vector or memory net
          NU_ASSERT(false, mNet);
        }

        tiAr.setRange(*range);
        mTypeInfoStack->push_back(tiAr);

        TypeInfo ti(UserType::eScalar, stdLogicVectorType);
        mTypeInfoStack->push_back(ti);
        
        return eSkip;
      }


      CarbonVhdlType subType = eVhdlTypeUnknown;
      if(strcasecmp(objType, "INTEGER") == 0)
        subType = eVhdlTypeInteger;
      else if(strcasecmp(objType, "NATURAL") == 0)
        subType = eVhdlTypeNatural;
      else if(strcasecmp(objType, "POSITIVE") == 0)
        subType = eVhdlTypePositive;
      else if(strcasecmp(objType, "REAL") == 0)
        subType = eVhdlTypeReal;
      else if(strcasecmp(objType, "CHARACTER") == 0)
        subType = eVhdlTypeChar;

      if (subType != eVhdlTypeUnknown)
      {
        TypeInfo ti(UserType::eScalar, subType, mTypeName.c_str(),  &mLibraryName, &mPackageName);

	const ConstantRange* cr= getConstraintRange();
	if (cr != NULL) {
	  ti.setRange(*cr);
	  ti.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
	}
        
        mTypeInfoStack->push_back(ti);
        return eSkip;
      }

    }

    if (phase == ePost) 
    {
      int ltDec = 0; 
      int rtDec = 0;
      if (getRangeOfIntegerEnum(node, ltDec, rtDec))
      {
        ConstantRange cr(ltDec,rtDec); 
        TypeInfo& ti = mTypeInfoStack->back();
        ti.setRange(cr);
	ti.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
      }

      // Reset the type name field
      clearTypeName();
    }
    return eNormal;
  }

  static void sGetTypeInfo(UserTypeFactory* typeFact, 
			   vhNode node, 
			   NUNet* net,
			   const NUNetToConstraintRangeMap* constraintRangeMap,
                           TypeInfoStack* tiStack,
			   MsgContext* msgContext)
  {
    TypeInfoCB jt(typeFact, net, tiStack, msgContext, constraintRangeMap);
    JaguarBrowser browser(jt);
    browser.putTypeBrowseEnable(true);
    browser.browse(node);
  }

private:

  bool maybeHandleNullString(UInt32 numDims)
  {
    if (numDims > 0 && mNet->isBitNet()) {
      mTypeInfoStack->pop_back();
      return true;
    }
    return false;
  }

  UInt32 getConstrainedArrayDims(vhNode node)
  {
    vhNode constraint = vhGetIndexConstr(node);
    NU_ASSERT(vhGetObjType(constraint) == VHINDEXCONSTRAINT, mNet);
    JaguarList rangeList(vhGetDiscRangeList(static_cast<vhExpr>(constraint)));
    return rangeList.size();
  }

  UInt32 getUnconstrainedArrayDims(vhNode node)
  {
    JaguarList indices(vhGetIndexSubTypeDefList(node));
    return indices.size();
  }

  UInt32 getIndexConstrainedArrayDims(vhNode node)
  {
    JaguarList rangeList(vhGetDiscRangeList(static_cast<vhExpr>(node)));
    return rangeList.size();
  }

  // This function gets the range for both integers and enumerations
  bool getRangeOfIntegerEnum(vhNode node, int &ltDec, int& rtDec)
  {
    vhNode constraint =  vhGetConstraint(node);
    bool isRange = false;
    if(constraint != NULL)
    {
      vhObjType objType = vhGetObjType(constraint);
      if(objType == VHRANGECONSTRAINT)
      {
        vhNode range = vhGetRange(constraint);
        if(range != NULL)
        {
          vhExpr exprLt = vhGetLtOfRange(range);
          vhExpr exprRt = vhGetRtOfRange(range);
          if((exprLt != NULL) && (exprRt != NULL))
          {
            vhExpr exprDecLt = vhEvaluateExpr(exprLt);
            vhExpr exprDecRt = vhEvaluateExpr(exprRt);
	    vhObjType exprTypeLt = vhGetObjType(exprDecLt);
	    vhObjType exprTypeRt = vhGetObjType(exprDecRt);
            if(exprTypeLt == VHIDENUMLIT && exprTypeRt == VHIDENUMLIT)
            {
              // This is for enum
              ltDec = vhGetIdEnumLitValue(exprDecLt);
              rtDec = vhGetIdEnumLitValue(exprDecRt);
            }
	    else if (exprTypeLt == VHCHARLIT && exprTypeRt == VHCHARLIT)
	    {
              // This is for enum with character literals
              ltDec = vhGetCharLitValue(exprDecLt);
              rtDec = vhGetCharLitValue(exprDecRt);
	    }
            else if (exprTypeLt == VHDECLIT && exprTypeRt == VHDECLIT)
            {
              // this is for integer
              ltDec = (int) vhGetDecLitValue(exprDecLt);
              rtDec = (int) vhGetDecLitValue(exprDecRt);
            }
	    else
	    {
	      JaguarString objTypeNameLt(vhGetObjTypeName(exprTypeLt));
	      JaguarString objTypeNameRt(vhGetObjTypeName(exprTypeRt));

	      UtString errMsg = UtString("Unsupported object type(s) (Left: ") + 
		UtString(objTypeNameLt) + UtString(" Right: ") + 
		UtString(objTypeNameRt) + UtString(")");
	      INFO_ASSERT(false, errMsg.c_str());
	    }
            isRange = true;
          }
        }
      }
    }
    return isRange;
  }

  const ConstantRange* getConstraintRange() const
  {
    NUNetToConstraintRangeMap::const_iterator p = mConstraintRangeMap->find(mCurrentNet);

    if (p == mConstraintRangeMap->end())
      return NULL;
    else
      return &(p->second);
  }

  void pushArrayType(UInt32 numDims)
  {
    if (mCurrentNet->isVectorNet())
    {
      NUVectorNet* vnet = mCurrentNet->castVectorNet();
      NU_ASSERT(numDims == 1, vnet);
      TypeInfo tiAr(UserType::eArray, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
      tiAr.setRange(*(vnet->getDeclaredRange()));
      tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
      mTypeInfoStack->push_back(tiAr);

    }
    else if (mCurrentNet->isMemoryNet())
    {
      NUMemoryNet* mnet = mCurrentNet->getMemoryNet();
      UInt32 numMemDims = mnet->getNumDims();
      NU_ASSERT(numDims <= numMemDims && numDims > 0, mnet);
      for (UInt32 dim = 0; dim < numDims; ++dim)
      {
        const ConstantRange* cr = mnet->getRange(numMemDims - mMemDim - 1);

        mMemDim++; // Look for next dimension in multi-dimensional array.
        TypeInfo tiAr(UserType::eArray, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
        tiAr.setRange(*cr);
	tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
        mTypeInfoStack->push_back(tiAr);
      }
    }
    else if (mCurrentNet->isCompositeNet())
    {
      NUCompositeNet* cnet = mCurrentNet->getCompositeNet();
      UInt32 numArrayDims = cnet->getNumDims();
      NU_ASSERT(numDims <= numArrayDims && numDims > 0, cnet);
      for (UInt32 dim = 0; dim < numDims; ++dim)
      {
        const ConstantRange* cr = cnet->getRange(numArrayDims - mCompDim - 1);
        TypeInfo tiAr(UserType::eArray, eVhdlTypeUnknown, mTypeName.c_str(), &mLibraryName, &mPackageName);
        tiAr.setRange(*cr);
	tiAr.setIsRangeRequiredInDeclaration(mNeedsRangeInDeclaration);
        mTypeInfoStack->push_back(tiAr);

        mCompDim++; // Look for next dimension in multi-dimensional array.
      }
    }
    else
    {
      NU_ASSERT(0, mNet);
    }

    // Make sure the array type name is reset
    clearTypeName();
  }

  void clearTypeName()
  {
    mTypeName = "";
  }

  UserTypeFactory* mTypeFactory;
  NUNet* mNet;
  UInt32 mMemDim;
  UInt32 mCompDim;
  TypeInfoStack* mTypeInfoStack;

  NUNet* mCurrentNet; // This is used only for composite nets. It points to the net
                      // that currently is browsed.
  const NUNetToConstraintRangeMap* mConstraintRangeMap; // This is used only for scalar nets 
                                                        // that have constraints.

  NUScopeNetMap mScopeToCompMap;
  UtString mTypeName;
  MsgContext *mMsgContext;
  vhNode mEnumAttrb;
  bool mNeedsRangeInDeclaration;
  UtString mLibraryName;
  UtString mPackageName;
};

class UserTypePopulatorCB : public NUInstanceCallback
{
public:

  UserTypePopulatorCB(STSymbolTable* symTab, 
		      const ScopeToNodeNetMap* netMap,
		      const NUNetToConstraintRangeMap* constraintRangeMap,
                      UserTypeFactory* fact, 
		      bool verbose,
		      MsgContext* msgContext)
    : NUInstanceCallback(symTab, true),
      mSymTab(symTab),
      mNetMap(netMap),
      mConstraintRangeMap(constraintRangeMap),
      mTypeFactory(fact),
      mVerbose(verbose),
      mSuccess(true),
      mMsgContext(msgContext)
  {
  }

  ~UserTypePopulatorCB()
  {
  }

  NUDesignCallback::Status operator()(Phase phase, NUModuleInstance* inst)
  {
    NUDesignCallback::Status stat = NUDesignCallback::eNormal;
    if (phase == NUDesignCallback::ePre)
    {
      // The branch node for instances within verilog generate blocks could be missing
      // in the symbol table. Their names are constructed during population and not
      // during pre-population walk. This method can be removed once the name construction
      // for such instances moves to pre-population.
      STBranchNode* parentNode = getCurrentNode();
      STSymbolTableNode* node = mSymTab->find(parentNode, inst->getName());
      if (node != NULL) {
        stat = NUInstanceCallback::operator()(phase, inst);
      } else {
        stat = NUDesignCallback::eSkip;
      }
    }
    else
    {
      stat = NUInstanceCallback::operator()(phase, inst);
    }
    return stat;
  }

  bool getSuccess() const {
    return mSuccess;
  }

private:

  void handleModule(STBranchNode* branch, NUModule* module);
  void handleDeclScope(STBranchNode* branch, NUNamedDeclarationScope* declScope);
  void handleInstance(STBranchNode* branch, NUModuleInstance* instance);

  void populateNetTypes(STBranchNode* branch, NUScope* scope);
  const UserType* populateNetType(STBranchNode* branch, vhNode node, NUNet* net);
  const UserType* populateVerilogNetType(STBranchNode* branch, veNode node, NUNet* net);
  const UserType* maybeCreateUserType(vhNode node, NUNet* net);
  const UserType* maybeCreateVerilogNet(veNode node, NUNet* net);
  void  getStructFieldNames(NUNet* net, AtomVector&  fieldNames);
  void  getEnumElements(const TypeInfo& ti, ConstantRange *range, AtomVector&  enumElements);
  void  getEnumEncodingElements(const TypeInfo& ti, ConstantRange *range, AtomVector& enumEncodingElements);
  const MvvNodeNetMap* getNodeNetMap(NUScope* scope);
  void printVerbose(STAliasedLeafNode* net, const UserType* ut);

  CarbonVerilogType  getVerilogNetType(veNode node);
  CarbonVerilogType getVerilogObjTypeStr(veNode node);
private:
  STSymbolTable* mSymTab;
  const ScopeToNodeNetMap* mNetMap;
  const NUNetToConstraintRangeMap* mConstraintRangeMap;
  UserTypeFactory* mTypeFactory;
  bool mVerbose;
  bool mSuccess;
  MsgContext* mMsgContext;
};

void UserTypePopulatorCB::handleModule(STBranchNode* branch, NUModule* module)
{
  populateNetTypes(branch, module);
}

void UserTypePopulatorCB::handleDeclScope(STBranchNode* branch,
                                          NUNamedDeclarationScope* declScope)
{
  populateNetTypes(branch, declScope);
}

void UserTypePopulatorCB::handleInstance(STBranchNode* branch,
                                         NUModuleInstance* instance)
{
  populateNetTypes(branch, instance->getModule());
}

void UserTypePopulatorCB::populateNetTypes(STBranchNode* branch, NUScope* scope)
{
  const MvvNodeNetMap* nodeNetMap = getNodeNetMap(scope);
  
  if (nodeNetMap != NULL)
  {
    // Convert this to net node map.
    for (MvvNodeNetMap::const_iterator itr = nodeNetMap->begin();
         (itr != nodeNetMap->end()); ++itr)
    {
      mvvNode node = itr->first;
      NUNet* net = itr->second;
      if (mvvGetLanguageType(node) == MVV_VHDL) {
        populateNetType(branch, node->castVhNode(), net);
      }
      if (mvvGetLanguageType(node) == MVV_VERILOG) {
        populateVerilogNetType(branch, node->castVeNode(), net);
      }
    }
  }
}

const UserType* UserTypePopulatorCB::populateNetType(STBranchNode* branch, vhNode node,
                                                     NUNet* net)
{
  STSymbolTableNode* symNode = mSymTab->find(branch, sGetNetName(net));
  STAliasedLeafNode* leafNode = NULL;
  if (symNode != NULL) {
    leafNode = symNode->castLeaf();
  }
  NU_ASSERT(leafNode != NULL, net);

  IODBDesignNodeData* bomData = IODBDesignDataBOM::castBOM(leafNode);
  const UserType* ut = maybeCreateUserType(node, net);
  if (ut != NULL) {
    bomData->putUserType(ut);
    if (mVerbose) {
      printVerbose(leafNode, ut);
    }
  }
  return ut;
}

const UserType* UserTypePopulatorCB::populateVerilogNetType(STBranchNode* branch, veNode node,
                                                            NUNet* net)
{
  veObjType objType = veNodeGetObjType(node);
  if(objType == VE_SCOPEVARIABLE)
  {
    // if it's hier ref, skip the user type population
    return NULL;
  }

  STSymbolTableNode* symNode = mSymTab->find(branch, sGetNetName(net));
  NU_ASSERT(symNode != NULL, net);

  STAliasedLeafNode* leafNode = NULL;
  if (symNode != NULL) {
    leafNode = symNode->castLeaf();
  }
  NU_ASSERT(leafNode != NULL, net);

  IODBDesignNodeData* bomData = IODBDesignDataBOM::castBOM(leafNode);
  const UserType* ut = maybeCreateVerilogNet(node, net);
  if (ut != NULL) {
    bomData->putUserType(ut);
    if (mVerbose) {
      printVerbose(leafNode, ut);
    }
  }

  return ut;
}


void UserTypePopulatorCB::getStructFieldNames(NUNet* net, AtomVector&  fieldNames)
{
  AtomicCache* atomCache = mTypeFactory->getAtomicCache();
  NUCompositeNet* compNet = net->getCompositeNet();
  for(UInt32 i = 0; i < compNet->getNumFields(); i++)
  {
    NUNet* net = compNet->getField(i);
    if(net != NULL)
    {
      UtString fieldName(sGetNetName(net)->str());
      StringAtom* elemAtom = atomCache->intern(fieldName.c_str());
      fieldNames.push_back(elemAtom);
    }
  }
}


void UserTypePopulatorCB::getEnumElements(const TypeInfo& ti, ConstantRange* /*range*/, AtomVector&  enumElements)
{
  AtomicCache* atomCache = mTypeFactory->getAtomicCache();
  vhNode nodeElem;
  while ((nodeElem = vhGetNextItem(ti.getEnumElems())))
  {
    
    if(nodeElem != NULL)
    {
      vhObjType objType = vhGetObjType(vhGetEnumLit(static_cast<vhExpr> (nodeElem)));
      switch(objType)
      {
        case VHIDENUMLIT:
          {
            JaguarString elem (vhGetIdEnumLit(static_cast<vhExpr> (nodeElem)) );
	    UtString elemStr(elem);
	    elemStr.lowercase();
            StringAtom* elemAtom = atomCache->intern(elemStr.c_str());
            enumElements.push_back(elemAtom);
          }
          break;
        case VHCHARLIT:
          {
            char enumChar = vhGetCharEnumElement(static_cast<vhExpr> (nodeElem) );
            UtString  strChar(1, '\'');
            strChar.append(1, enumChar);
            strChar.append(1, '\''); 
            StringAtom* elemAtom = atomCache->intern(strChar);
            enumElements.push_back(elemAtom);
          }          
          break;

        default:
          INFO_ASSERT(false, "This type of enumaration elements is not supported.");
          break;
      }
    }
  }
}


void UserTypePopulatorCB::getEnumEncodingElements(const TypeInfo& ti, 
						  ConstantRange *range, 
						  AtomVector&  enumEncodingElements)
{
  AtomicCache* atomCache = mTypeFactory->getAtomicCache();
  const UtStringArray& enumEncodings = ti.getEnumEncodings();

  if (enumEncodings.empty())
    return;

  int i = 0;

  for (UtStringArray::const_iterator iter = enumEncodings.begin();
       iter != enumEncodings.end();
       ++iter)
  {
    if (range == NULL || range->contains(i))
    {
      StringAtom* encodingAtom = atomCache->intern(*iter);
      enumEncodingElements.push_back(encodingAtom);
    }
    i++;
  }
}


const UserType* UserTypePopulatorCB::maybeCreateUserType(vhNode node, NUNet* net)
{
  TypeInfoStack tiStack;
  TypeInfoCB::sGetTypeInfo(mTypeFactory, node, net, mConstraintRangeMap, 
			   &tiStack, mMsgContext);

  const UserType* ut = NULL;
  TypeInfoStackCRIter itrEnd   = tiStack.rend();
  UserTypeVector* vecUserType = NULL;
  VectorUserTypeVector vecUserTypeVec;

  for (TypeInfoStackCRIter itr = tiStack.rbegin(); itr != itrEnd; ++itr)
  {
    const TypeInfo& ti = *itr;
    ConstantRange *range = NULL, rangeObject;
    
    if(ti.isRange())
    {
      rangeObject = ti.getRange();
      range = &rangeObject;
    }
 
    if(ti.getType() == UserType::eScalar)
    {
      ut = mTypeFactory->maybeAddScalarType(eLanguageTypeVhdl,
					    eVerilogTypeUnknown, 
					    ti.getVhdlType(), 
					    range, 
					    ti.getCompositeTypeName(),
					    ti.isRangeRequiredInDeclaration(),
                                            ti.getLibraryName(),
                                            ti.getPackageName());
      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
    else if(ti.getType() == UserType::eArray)
    {
      if(vecUserType != NULL)
        vecUserType->pop_back();

      // In vhdl, arrays of scalars (std_logic, std_ulogic, etc) are packed.
      // However, arrays of scalars (integer, real, etc, are NOT packed)
      // 'ut' is previously created UserType, to which this UserArray will point.
      bool isPacked = sIsVectorType(ut);
      ut = mTypeFactory->maybeAddArrayType(eLanguageTypeVhdl, 
					   eVerilogTypeUnknown, 
					   ti.getVhdlType(), 
                                           range, 
					   ut, 
                                           isPacked,
					   ti.getCompositeTypeName(),
					   ti.isRangeRequiredInDeclaration(),
                                           ti.getLibraryName(),
                                           ti.getPackageName());
      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
    else if(ti.getType() == UserType::eStruct)
    {
      // reverse the user types for records
      UserTypeVector rVecUserType;
      for(UserTypeVectorRIter ritr = vecUserType->rbegin(); 
          ritr != vecUserType->rend(); 
          ++ritr)
      {
        const UserType* utVal = *ritr;
        rVecUserType.push_back(utVal);
      }
      
      AtomVector  fieldNames;
      getStructFieldNames(ti.getNet(), fieldNames);
      ut = mTypeFactory->maybeAddStructType(eLanguageTypeVhdl, 
					    eVerilogTypeUnknown, 
					    ti.getVhdlType(), 
                                            rVecUserType, 
					    fieldNames,
					    ti.getCompositeTypeName(),
                                            ti.getLibraryName(),
                                            ti.getPackageName() );
      vecUserType->clear();

      vecUserTypeVec.pop_back();
      delete vecUserType;
      if (vecUserTypeVec.size() > 0)
        vecUserType = vecUserTypeVec.back();
      else
        vecUserType = NULL;

      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
    else if(ti.getType() == UserType::eStructEnd)
    {
      vecUserType = new UserTypeVector();
      vecUserTypeVec.push_back(vecUserType);
    }
    else if(ti.getType() == UserType::eEnum)
    {
      AtomVector  enumElems;
      getEnumElements(ti, range, enumElems);

      // An enum may have a ENUM_ENCODING attribute associated with it
      // which needs to be preserved in the database.
      AtomVector enumEncodingElems;
      getEnumEncodingElements(ti, range, enumEncodingElems);

      ut = mTypeFactory->maybeAddEnumType(eLanguageTypeVhdl, 
					  eVerilogTypeUnknown, 
					  ti.getVhdlType(),
                                          range, 
					  enumElems,
					  enumEncodingElems,
					  ti.getCompositeTypeName(),
					  ti.isRangeRequiredInDeclaration(),
                                          ti.getLibraryName(),
                                          ti.getPackageName());
      if(vecUserType != NULL)
        vecUserType->push_back(ut);
    }
  }

  return ut;
}


CarbonVerilogType  UserTypePopulatorCB::getVerilogNetType(veNode node)
{
  veObjType nodeType  = veNodeGetObjType(node);
  switch (nodeType)
  {
    case VE_MEMORY:
      // When neither of -2000, -2001, v2k is used,
      // Cheetah creates an array with type VE_MEMORY. 
      // In this mode only two types of arrays are supported:
      // reg    [2:0]  arrayReg     [0:3];
      // reg           arrayReg     [0:3];
      // Neither multidimensional, nor any other type of net type (wire, tri, etc) is
      // supported in this mode.
      return eVerilogTypeReg;
      break;

    case VE_INTEGER:
      return eVerilogTypeInteger;
      break;
    case VE_REAL:
      return eVerilogTypeReal;
      break;
    case VE_TIME:
      return eVerilogTypeTime;
      break;
    case VE_REALTIME:
      return eVerilogTypeRealTime;
      break;

    // Integer and Time array without -2000 switch
    // is populated as VE_INTORTIMEARRAY, and as 
    // VE_ARRAY with -2000 switch.
    case VE_INTORTIMEARRAY:
    case VE_ARRAY:
      return eVerilogTypeReg;
      break;

    default:
      break;
  }

  CarbonVerilogType  netTypeEnum;
  veNetType netType = veNetGetNetType(node);
  switch(netType)
  {
    case UNSET_NET:
      netTypeEnum = eVerilogTypeUnsetNet;      // user type is not known
      break;
    case WIRE_NET:
      netTypeEnum = eVerilogTypeWire;
      break;
    case TRI_NET:
      netTypeEnum = eVerilogTypeTri;
      break;
    case TRI1_NET:
      netTypeEnum = eVerilogTypeTri1;
      break;
    case SUPPLY0_NET:
      netTypeEnum = eVerilogTypeSupply0;
      break;
    case WAND_NET:
      netTypeEnum = eVerilogTypeWAnd;
      break;
    case TRIAND_NET:
      netTypeEnum = eVerilogTypeTriAnd;
      break;
    case TRI0_NET:
      netTypeEnum = eVerilogTypeTri0;
      break;
    case SUPPLY1_NET:
      netTypeEnum = eVerilogTypeSupply1;
      break;
    case WOR_NET:
      netTypeEnum = eVerilogTypeWOr;
      break;
    case TRIOR_NET:
      netTypeEnum = eVerilogTypeTriOr;
      break;
    case TRIREG_NET:
      netTypeEnum = eVerilogTypeTriReg;
      break;
    case REG_NET:
      netTypeEnum = eVerilogTypeReg;
      break;
    default:
      netTypeEnum = eVerilogTypeUnsetNet;      // user type is not known
      break;
  }

  return netTypeEnum;
}

CarbonVerilogType UserTypePopulatorCB::getVerilogObjTypeStr(veNode node)
{
  CarbonVerilogType  objTypeEnum;

  veObjType objType =  VeNodeGetObjType(node);
  switch(objType)
  {
    case VE_NET:
    case VE_NETBIT:
    case VE_NETARRAY:
    case VE_MEMORY:
      objTypeEnum = eVerilogTypeUnsetNet;
      break;

    case VE_INTEGER:
      objTypeEnum = eVerilogTypeInteger;
      break;

    case VE_REAL:
      objTypeEnum = eVerilogTypeReal;
      break;

    case VE_TIME:
      objTypeEnum = eVerilogTypeTime;
      break;

    case VE_REALTIME:
      objTypeEnum = eVerilogTypeRealTime;
      break;

    case VE_INTORTIMEARRAY:
    {
      // this is for an array of integers and time
      // without -2000 switch
      veObjType intOrTime = veIntOrTimeArrayGetArrayType(node);
      if(intOrTime == VE_INTEGER)
        objTypeEnum = eVerilogTypeInteger;
      else
        objTypeEnum = eVerilogTypeTime;

      break;
    }

    case VE_ARRAY:
    {
      // this is for an array of integers, reals and time
      // without -2000 switch
      veObjType intOrRealOrTime = veArrayGetArrayType(node);
      if (intOrRealOrTime == VE_INTEGER)
        objTypeEnum = eVerilogTypeInteger;
      else if (intOrRealOrTime == VE_REAL)
        objTypeEnum = eVerilogTypeReal;
      else 
        objTypeEnum = eVerilogTypeTime;
      
      break;
    }

    default:
      objTypeEnum = eVerilogTypeUnsetNet;
      break;

  }
  return objTypeEnum;
}



const UserType* UserTypePopulatorCB::maybeCreateVerilogNet(veNode node, NUNet* net)
{
  const UserType* ut = NULL;

  TypeInfoStack tiStack;

  CarbonVerilogType  netTypeEnum, objTypeEnum;

  if (net->isBitNet())
  {
    netTypeEnum = getVerilogNetType(node);
    ut = mTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					  netTypeEnum, 
					  eVhdlTypeUnknown);
  }
  else if (net->isVectorNet())
  {

    NUVectorNet *vectorNet =  net->castVectorNet();
    netTypeEnum = getVerilogNetType(node);
    ut = mTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					  netTypeEnum, 
					  eVhdlTypeUnknown);

    // Keep integers, reals and times as a scalar
    if( (net->isInteger() == false) && (net->isReal() == false) && (net->isTime() == false))
    {
      ut = mTypeFactory->maybeAddArrayType(eLanguageTypeVerilog, 
					   netTypeEnum, eVhdlTypeUnknown, 
                                           const_cast <ConstantRange*> (vectorNet->getDeclaredRange()), 
					   ut, 
                                           true /* packed array */);
    }
  }
  else if (net->isMemoryNet()) // eArray
  {
    NUMemoryNet *memNet =  net->getMemoryNet();
    objTypeEnum = getVerilogObjTypeStr(node);

    if((objTypeEnum == eVerilogTypeInteger) || (objTypeEnum == eVerilogTypeTime))
    {
      // if -2000 is not selected and the net is 
      // array of int or time
      ut = mTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					    objTypeEnum, 
					    eVhdlTypeUnknown);
      UInt32 numDim = memNet->getNumDims();
      for(UInt32 i = 1; i < numDim; i++)
      {
        ut = mTypeFactory->maybeAddArrayType(eLanguageTypeVerilog, 
					     objTypeEnum, 
					     eVhdlTypeUnknown, 
                                             const_cast <ConstantRange*> (memNet->getRange(i)), 
					     ut, 
                                             false /* unpacked */);
      }
    }
    else
    {
      netTypeEnum = getVerilogNetType(node);
      ut = mTypeFactory->maybeAddScalarType(eLanguageTypeVerilog, 
					    netTypeEnum, 
					    eVhdlTypeUnknown);

      UInt32 numDim = memNet->getNumDims();
      // Keep track of whether this is packed or unpacked
      UInt32 firstUnpacked = memNet->getFirstUnpackedDimensionIndex();
      for(UInt32 i = 0; i < numDim; i++)
      {
        bool isPacked = (i < firstUnpacked);
        ut = mTypeFactory->maybeAddArrayType(eLanguageTypeVerilog, 
					     netTypeEnum, 
					     eVhdlTypeUnknown, 
                                             const_cast <ConstantRange*> (memNet->getRange(i)), 
					     ut, 
                                             isPacked);
      }
    }
  } // else : we don't care about type info for the rest.
  return ut;
}


const MvvNodeNetMap* UserTypePopulatorCB::getNodeNetMap(NUScope* scope)
{
  const MvvNodeNetMap* nodeNetMap = NULL;
  ScopeToNodeNetMap::const_iterator sitr = mNetMap->find(scope);
  if (sitr != mNetMap->end()) {
    nodeNetMap = &(sitr->second);
  }
  return nodeNetMap;
}

void UserTypePopulatorCB::printVerbose(STAliasedLeafNode* net, const UserType* ut)
{
  UtIO::cout() << "Net: ";
  net->print();
  ut->print();
  UtIO::cout() << UtIO::endl;
}

UserTypePopulate::UserTypePopulate(const DesignPopulate* populator,
                                   IODBNucleus* iodb, 
				   bool verbose,
				   MsgContext* msgContext)
  : mPopulator(populator),
    mIODB(iodb),
    mVerbose(verbose),
    mMsgContext(msgContext)
{
}

bool UserTypePopulate::populate(NUDesign* design)
{
  IODBDesignDataSymTabs* symTabs = mIODB->getDesignDataSymbolTables();
  UserTypePopulatorCB utp(symTabs->getElabSymTab(), 
			  mPopulator->getJaguarNodeNetMap(),
			  mPopulator->getConstraintRangeMap(),
                          mIODB->userTypeFactory(), 
			  mVerbose, 
			  mMsgContext);

  mIODB->userTypeFactory()->setUserTypesAtomicCache(symTabs->getAtomicCache());

  NUDesignWalker walker(utp, false, false);
  walker.putWalkTasksFromTaskEnables(true);
  walker.design(design);
  return utp.getSuccess();
}
