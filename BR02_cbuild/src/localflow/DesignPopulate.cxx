// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "localflow/AliasResolver.h"
#include "compiler_driver/CarbonContext.h" // All I need is CRYPT....
#include "compiler_driver/JaguarContext.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NULvalue.h"
#include "util/ArgProc.h"
#include "util/CbuildMsgContext.h"
#include "LFContext.h"
#include "reduce/Fold.h"

extern const char* gGetObjTypeStr(veObjType objType);

bool DesignPopulate::MVVNameAlloc( mvvNode mvv_node, UtString* objName ) {
  if ( mvv_node == NULL ){
    return false;
  }
  if ( mvvGetLanguageType( mvv_node ) == MVV_VHDL ) {
    return mVhdlPopulate.getVhdlObjName( reinterpret_cast<vhNode>( mvv_node ), objName);
  }
  else {
    veNode ve_node = mvv_node->castVeNode();
    return mVlogPopulate.mTicProtectedNameMgr->getVisibleName(ve_node, objName);
  }
}

DesignPopulate::DesignPopulate( STSymbolTable *symtab,
                                AtomicCache *str_cache,
                                IODBNucleus * iodb,
                                SourceLocatorFactory *loc_factory,
                                FLNodeFactory *flow_factory,
                                NUNetRefFactory *netref_factory,
                                MsgContext *msg_context,
                                ArgProc *arg,
                                TicProtectedNameManager* tic_protected_nm_mgr,
                                bool allow_hierrefs,
                                bool verbose_hierref_analysis,
                                bool useLegacyGenerateHierarchyNames,
                                VHDLCaseT caseMode,
                                CarbonContext* carbonContext):
  mVlogPopulate( this,
                 symtab,
                 str_cache,
                 iodb,
                 loc_factory,
                 flow_factory,
                 netref_factory,
                 msg_context,
                 arg,
                 tic_protected_nm_mgr,
                 allow_hierrefs,
                 useLegacyGenerateHierarchyNames,
                 carbonContext),
  mVhdlPopulate( this,
                 symtab,
                 str_cache,
                 iodb,
                 loc_factory,
                 flow_factory,
                 netref_factory,
                 msg_context,
                 arg,
                 useLegacyGenerateHierarchyNames,
                 carbonContext,
                 tic_protected_nm_mgr),
  mSymtab( symtab ),
  mStrCache( str_cache ),
  mIODB( iodb ),
  mLocFactory( loc_factory ),
  mFlowFactory( flow_factory ),
  mNetRefFactory( netref_factory ),
  mMsgContext( msg_context ),
  mArg( arg ),
  mUseLegacyGenerateHierarchyNames(useLegacyGenerateHierarchyNames),
  mLFContext(NULL),
  mVerboseHierRefAnalysis(verbose_hierref_analysis),
  mInterraDesignWalker(NULL),
  mNucleusInterraCB(NULL),
  mCaseMode(caseMode),
  mCarbonContext(carbonContext),
  mMsgCB(NULL),
  mStackLimit(0),
  mFold(NULL)
{
  init();
}


DesignPopulate::~DesignPopulate()
{
  // delete all NULvalues from the mPortMap, they were used to track
  // partial nets in the port list but are not part of the design
  for (ScopeToNodeLvalueMap::iterator pm = mPortMap.begin(); pm != mPortMap.end(); ++pm){
    MvvNodeLvalueMap& nodeMap = pm->second;
    for ( MvvNodeLvalueMap::iterator nlvm = nodeMap.begin(); nlvm != nodeMap.end(); ++nlvm){
      NULvalue* lvalue = nlvm->second;
      delete lvalue;
    }
  }

  mIODB->freeSubstituteModules();

  // Remove message callback that was added during construction.
  mMsgContext->removeMessageCallback(mMsgCB);
  delete mMsgCB;
  mMsgCB = NULL;

  delete mFold;
  delete mLFContext;
}

// The callback invoked by MsgContext before a message is printed. 
// Print the jaguar object stack to help user locate the code that generated
// the message.
static eCarbonMsgCBStatus sMsgCallback(CarbonClientData clientData,
                                       CarbonMsgSeverity severity,
                                       int msgNumber, const char* text, unsigned int len)
{
  DesignPopulate* populator = (DesignPopulate*)clientData;
  switch (severity)
  {
  case eCarbonMsgError:
  case eCarbonMsgFatal:
  case eCarbonMsgAlert:
  {
    // Only error/fatal/alert severity messages result in stack dump.
    populator->maybePrintJaguarStackSrcLoc();
    break;
  }
  default:
    break;
  }
  
  return populator->mMsgCountManager.getMsgCBStatus(severity, msgNumber, text, len);
}

void DesignPopulate::init()
{
  // use eFoldBottomUp for speed
  mFold = new Fold(mNetRefFactory, mArg, mMsgContext, mStrCache, mIODB,
                   false, eFoldPreserveReset );
  mVlogPopulate.mFold = mFold;
  mVhdlPopulate.mFold = mFold;
  VhdlPopulate::EntityAliasMap* alias_map = mVhdlPopulate.getVhdlAliasMap();
  

  mLFContext = new LFContext( mSymtab,
                              mStrCache,
                              mLocFactory,
                              mFlowFactory,
                              0,
                              mNetRefFactory,
                              mArg,
                              mMsgContext,
                              mIODB,
                              mFold,
                              alias_map,
                              mCaseMode);

  if (mArg->getBoolValue(JaguarContext::scVhdlErrorStack)) {
    // The stack limit provides the maximum depth of stack to be dumped.
    mArg->getIntLast(JaguarContext::scVhdlErrorStackLimit, &mStackLimit);
  }

  // Retrieve max message repeat count command line argument, and
  // place it in the design manager.
  SInt32 maxMsgRepeatCount;
  mArg->getIntLast(CarbonContext::scMaxMsgRepeatCount, &maxMsgRepeatCount);
  mMsgCountManager.setMaxMsgRepeatCount(maxMsgRepeatCount);
  
  // Register message callback with MsgContext. It calls the callback before
  // issuing a message.
  mMsgCountManager.setMsgContext(mMsgContext);
  mMsgCB = new MsgCallback(sMsgCallback, this);
  mMsgContext->addMessageCallback(mMsgCB);

}

Populate::ErrorCode
DesignPopulate::designFinish(LFContext *context, Populate::ErrorCode start_err_code)
{
  Populate::ErrorCode err_code = start_err_code;

  Populate::ErrorCode temp_err_code = mVlogPopulate.postDesignFixup(context);
  if ( mVlogPopulate.errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }

  if (err_code == Populate::eSuccess)
  {
    context->mapTopLevelPortIndices();
    context->markDepositObserveNets();
  }

  if (err_code == Populate::eSuccess)
  {
    AliasResolver ar(mMsgContext, context->getStringCache(), mFold);
    err_code = ar.design(context->getDesign(), mVhdlPopulate.getVhdlAliasMap());
  }
  return err_code;
}



SourceLocator
DesignPopulate::locator( mvvNode node )
{
  mvvLanguageType lang = mvvGetLanguageType( node );
  switch ( lang ){
  case MVV_VERILOG:
    return mVlogPopulate.locator( reinterpret_cast<veNode>( node ));
  case MVV_VHDL:
    return mVhdlPopulate.locator( reinterpret_cast<vhNode>( node ));
  default:
    INFO_ASSERT((lang == MVV_VERILOG || lang == MVV_VHDL), "Unknown source language");
    return SourceLocator();           // this is never executed
  }
}


SourceLocator
DesignPopulate::locator( veNode node )
{
  return mVlogPopulate.locator( node );
}


SourceLocator
DesignPopulate::locator( vhNode node )
{
  return mVhdlPopulate.locator( node );
}


//! Return the Interra node to use to identify the net
mvvNode
DesignPopulate::getLookupNetNode(NUScope *scope, mvvNode node)
{
  mvvNode mvv_retval = NULL;
  mvvLanguageType lang = mvvGetLanguageType( node );
  if ( lang == MVV_VERILOG )
  {
    veNode ve_node = reinterpret_cast<veNode>( node );
    if ( veNodeIsA( ve_node, VE_SCOPEVARIABLE )) {
      mvv_retval = mVlogPopulate.getHierRefMapping( scope->getModule(), ve_node );
    }
    else if ( veNodeIsA( ve_node, VE_NAMEDOBJECTUSE )) {
      mvv_retval = veNamedObjectUseGetParent( ve_node );
    }
    else {
      mvv_retval = node;
    }
  }
  else if ( lang == MVV_VHDL )
  {
    vhNode vh_node = reinterpret_cast<vhNode>( node );
    if ( vhIsA( vh_node, VHOBJECT ))
      mvv_retval = vhGetActualObj( vh_node );
    else
      mvv_retval = node;
  }
  else
    INFO_ASSERT((lang == MVV_VERILOG || lang == MVV_VHDL), "Unknown source language");

  return mvv_retval;
}

// add with checking an entry to the nodeMap of _scope_.  This entry associates the  mvv_id (defined by the pair _node_ and  _scope_)  with the NU object _net_.
// if an entries for mvv_id already exists for the nodeMap of _scope_ then check if we are within a for loop or subprogram, if we are not in
// such a context then this duplicate entry is an error condition.
Populate::ErrorCode
DesignPopulate::mapNet(NUScope* scope, mvvNode node, NUNet* net)
{
  MvvNodeNetMap& nodeMap = mNetMap[scope];

  mvvNode mvv_ident = getLookupNetNode( scope, node );

  if (nodeMap.find( mvv_ident ) != nodeMap.end( ))
  {
    bool emitError = true;
    if ( mvvGetLanguageType( node ) == MVV_VHDL )
    {
      // We will get same vhNode corresponding to the net/subprog if the
      // subprogram is called more than once, or if we populate the same
      // for loop more than once. This is not a redeclaration of the
      // net, so we don't report an error.  We do need to update the
      // NUNet associated with the mvv_node because we are done using
      // the old one and want to use the new one now.
      VhObjType objType = vhGetObjType( static_cast<vhNode>( node ));
      if ( objType == VHSUBPROGBODY || objType == VHFORINDEX )
      {
        emitError = false;
      }
    }
    if ( emitError == true )
    {
      SourceLocator loc = locator(node);
      UtString objName;
      bool found_name = MVVNameAlloc( mvv_ident, &objName );
      if ( not found_name ){
        LOC_ASSERT("Cannot find a name for net" == 0, loc);
      }
      Populate::ErrorCode err_code;
      POPULATE_FAILURE(mMsgContext->NetIsRedeclared( &loc, objName.c_str() ), &err_code);
      return err_code;
    }
  }

  nodeMap[mvv_ident] = net;
  return Populate::eSuccess;
}

Populate::ErrorCode
DesignPopulate::mapConstraintRange(NUNet* net, ConstantRange* constraintRange)
{
  mConstraintRangeMap[net] = *constraintRange;
  return Populate::eSuccess;
}

Populate::ErrorCode
DesignPopulate::lookupNet(NUScope* scope, mvvNode node, NUNet **the_net, bool error_if_missing)
{
  Populate::ErrorCode err_code = Populate::eSuccess;
  *the_net = 0;

  mvvNode mvv_ident = getLookupNetNode( scope, node );

  NUScope* current_scope = scope;

  while ( current_scope ) {
    ScopeToNodeNetMap::const_iterator p = mNetMap.find( current_scope );
    if ( p != mNetMap.end() ){
      const MvvNodeNetMap& nodeMap = p->second;

      MvvNodeNetMap::const_iterator q = nodeMap.find( mvv_ident );
      if ( q != nodeMap.end( )){
        *the_net = q->second;
        return err_code;
      }
    }

    // keep working our way up the scope hierarchy until we process the enclosing module
    if ( current_scope->getScopeType() == NUScope::eTask )
    {
      NUScope *tfParent = current_scope->getHdlParentScope();
      if ( tfParent != NULL )
        current_scope = tfParent;
      else
        current_scope = current_scope->getParentScope();
    }
    else
      current_scope = current_scope->getParentScope();
  }

  if ( error_if_missing ) {
    // if we get here then could not find the net in any scope (up to and
    // including the enclosing module)
    SourceLocator loc = locator( node );
    UtString msg_text;
    bool found_name = MVVNameAlloc( mvv_ident, &msg_text );
    if ( not found_name ){
      LOC_ASSERT("Cannot find a name for net" == 0, loc);
    }

    POPULATE_FAILURE(mMsgContext->CouldNotFindNet( &loc, scope->getScopeTypeStr(),
                                                   scope->getName()->str(), msg_text.c_str()), 
                     &err_code);
  }
  // It is strange that it returns eFailPopulate here.
  // Probably, it should be replaced with eNotApplicable
  return Populate::eFailPopulate;
}

Populate::ErrorCode
DesignPopulate::mapPort(NUModule* module, mvvNode node, NULvalue* lvalue)
{
  Populate::ErrorCode err_code = Populate::eSuccess;
  MvvNodeLvalueMap& nodeMap = mPortMap[module];

  // May see this node->lvalue mapping several times for ranged
  // instantiations. (array of instances) Allow it, but make sure the
  // mapping is consistent.
  MvvNodeLvalueMap::iterator iter = nodeMap.find( node );
  if (( iter != nodeMap.end( )) and ( (*iter).second != lvalue )) {
    SourceLocator loc = locator( node );
    UtString objName;
    bool found_name = MVVNameAlloc( node, &objName );
    if ( not found_name ){
      LOC_ASSERT("Cannot find a name for net" == 0, loc);
    }
    POPULATE_FAILURE(mMsgContext->PortRedeclare(&loc, objName.c_str() ), &err_code);
    return err_code;
  }

  nodeMap[node] = lvalue;
  return err_code;
}


Populate::ErrorCode
DesignPopulate::lookupPort(NUModule* module, 
			   mvvNode node, 
			   mvvNode newModule,
                           NUPortMap* portMap, 
			   NUNet **the_net,
                           bool *partial_port, 
			   ConstantRange* range)
{
  Populate::ErrorCode err_code = Populate::eSuccess;
  ScopeToNodeLvalueMap::const_iterator pm = mPortMap.find(module);
  UtString namebuf;
  SourceLocator loc = locator(node);
  bool found_name = MVVNameAlloc( node, &namebuf );
  if ( not found_name ){
      *partial_port = false;
      *the_net = NULL;
      return Populate::eFailPopulate;
  }

  if ( pm == mPortMap.end()) {
    if ( not found_name ){
      namebuf << "(unknown name)";
    }
    POPULATE_FAILURE(mMsgContext->CouldNotFindPort( &loc, namebuf.c_str()), &err_code);
    return err_code;
  }

  mvvLanguageType lang = mvvGetLanguageType(node);

  if (lang == MVV_VHDL) 
  {
    namebuf.lowercase();
  }

  const char* name = namebuf.c_str();

  mvvNode nodeToLookup;

  // Find the substituted module formal.
  if( newModule )
  {
    // Lookup the name if substituteModule specified a port mapping.
    if( portMap )
    {
      const char* mapped_name = (*portMap)[name].c_str();
      if( !strcmp(mapped_name,"") )
      {
        SourceLocator loc = locator(node);
        POPULATE_FAILURE(mMsgContext->CouldNotFindSubstituteMappedPort( &loc, name ), &err_code);
        return err_code;
      }
      name = const_cast<char*>( mapped_name );
    }

    mvvLanguageType newModuleLang = mvvGetLanguageType(newModule);

    if (newModuleLang == MVV_VERILOG)
    {

      veNode substitutePort = NULL;

      veNode ve_module = newModule->castVeNode();

      // Lookup the port name in the newModule portlist.
      CheetahList portIter(veModuleGetPortList( ve_module ));
      for (substitutePort = veListGetNextNode( portIter ); 
	   substitutePort != NULL;
	   substitutePort = veListGetNextNode( portIter )) {
	UtString name_buf;
	mVlogPopulate.mTicProtectedNameMgr->getVisibleName(substitutePort, &name_buf);
	if (strcmp(name, name_buf.c_str()) == 0)
	  break;
      }
	   
      SourceLocator newModLoc = locator(ve_module);

      if( !substitutePort )
      {
	POPULATE_FAILURE(mMsgContext->CouldNotFindSubstitutePort( &newModLoc, name ), &err_code);
	return err_code;
      }

      if (lang == MVV_VERILOG)
      {
	// Check port existence, direction and size.
	if (( vePortGetPortDirection( reinterpret_cast<veNode>( node )) != 
	      vePortGetPortDirection( substitutePort )))
	{
	  POPULATE_FAILURE(mMsgContext->SubstitutePortDirection( &newModLoc, name), &err_code);
	  return err_code;
	}

	if ( vePortGetPortWidth( reinterpret_cast<veNode>( node )) !=
	     vePortGetPortWidth( substitutePort ))
	{
	  POPULATE_FAILURE(mMsgContext->SubstitutePortWidth( &newModLoc, name), &err_code);
	  return err_code;
	}
      } 
      else // lang == MVV_VHDL
      {
	vhPortType old_typ = vhGetPortType(node->castVhNode());
	veDirectionType new_typ = vePortGetPortDirection(substitutePort);
	// We can't handle VH_BUFFER and VH_LINKAGE.
	if (!( ((old_typ == VH_IN) && (new_typ == INPUT_DIR)) ||
	       ((old_typ == VH_OUT) && (new_typ == OUTPUT_DIR)) ||
	       ((old_typ == VH_INOUT) && (new_typ == INOUT_DIR)) ))
	{
	  POPULATE_FAILURE(mMsgContext->SubstitutePortDirection( &newModLoc, name), &err_code);
	  return err_code;
	}

	// No checks for port width
      }

      nodeToLookup = substitutePort;
    }  
    else // newModuleLang == MVV_VHDL
    {
      vhNode substitutePort = NULL;
      vhNode vh_entity = newModule->castVhNode();
      vhNode port_clause = vhGetPortClause(vh_entity);
      JaguarList port_list;
      if (port_clause)
      {
	port_list = vhGetFlatSignalList(port_clause);
      }

      while ((substitutePort = vhGetNextItem(port_list)))
      {
	UtString substitute_port_name;
	bool found_name = MVVNameAlloc(substitutePort, &substitute_port_name);
	if ( not found_name ){
	  *partial_port = false;
	  *the_net = NULL;
	  return Populate::eFailPopulate;
	}
	substitute_port_name.lowercase();
	if (strcmp(substitute_port_name.c_str(), name) == 0) {
	  break;
	}
      }

      SourceLocator newModLoc = locator(vh_entity);

      if (!substitutePort)
      {
	POPULATE_FAILURE(mMsgContext->CouldNotFindSubstitutePort( &newModLoc, name ), &err_code);
	return err_code;
      }

      vhPortType substitutePortType = vhGetPortType(substitutePort);
      
      if (lang == MVV_VERILOG)
      {
	veDirectionType oldPortType = vePortGetPortDirection(node->castVeNode());

	if (! (((oldPortType == INPUT_DIR) && (substitutePortType == VH_IN)) ||
	       ((oldPortType == OUTPUT_DIR) && (substitutePortType == VH_OUT)) ||
	       ((oldPortType == INOUT_DIR) && (substitutePortType == VH_INOUT))))
	{
	  POPULATE_FAILURE(mMsgContext->SubstitutePortDirection( &newModLoc, name), &err_code);
	  return err_code;
	}

	// No checks for port width
      }
      else // lang == MVV_VHDL
      {
	vhPortType oldPortType = vhGetPortType(node->castVhNode());
	// We can't handle VH_BUFFER and VH_LINKAGE.
	if (oldPortType != substitutePortType)
	{
	  POPULATE_FAILURE(mMsgContext->SubstitutePortDirection( &newModLoc, name), &err_code);
	  return err_code;
	}

	// No checks for port width
      }

      nodeToLookup = substitutePort;
    } 
  }
  else // newModule == NULL
  {
    nodeToLookup = node;
  }

  const MvvNodeLvalueMap& nodeMap = pm->second;
  MvvNodeLvalueMap::const_iterator q = nodeMap.find( nodeToLookup );
  if ( q != nodeMap.end( ))
  {
    NULvalue *lvalue = q->second;
    if (lvalue->isWholeIdentifier()) {
      *the_net = lvalue->getWholeIdentifier();
    } else if ( lvalue->getType() == eNUVarselLvalue ) {
      NUVarselLvalue* varsel_lvalue = dynamic_cast<NUVarselLvalue*>(lvalue);
      *partial_port = true;
      const ConstantRange *lvalue_range = varsel_lvalue->getRange();
      range->setMsb(lvalue_range->getMsb());
      range->setLsb(lvalue_range->getLsb());
      *the_net = varsel_lvalue->getIdentNet();
    } else {
      *the_net = NULL;
    }
    NU_ASSERT((*the_net), lvalue);
    return err_code;
  }
  else
  {
    SourceLocator loc = locator(node);
    POPULATE_FAILURE(mMsgContext->CouldNotFindPort( &loc, found_name? name: "(null)" ), &err_code);
    return err_code;
  }
}


void 
DesignPopulate::mapCompositeNet( NUNamedDeclarationScope *scope, NUCompositeNet *net )
{
  mCompositeNetMap[scope] = net;
}


Populate::ErrorCode
DesignPopulate::lookupCompositeNet( NUNamedDeclarationScope *scope,
                                    NUCompositeNet **net )
{
  Populate::ErrorCode err_code = Populate::eSuccess;
  *net = mCompositeNetMap[scope];
  if ( *net == NULL )
    err_code = Populate::eFailPopulate;
  return err_code;
}

void DesignPopulate::maybePrintJaguarStackSrcLoc()
{
  // If user requested error stack, print it.
  if (mStackLimit > 0)
  {
    UtList<vhNode> cur_stack;
    mLFContext->getJaguarNodeStack(mStackLimit, cur_stack);
    SInt32 index = 0;
    for (UtList<vhNode>::iterator itr = cur_stack.begin();
         itr != cur_stack.end(); ++itr)
    {
      vhNode node = *itr;
      if (node != NULL)
      {
        //vhObjType obj_type = vhGetObjType(node);
        //JaguarString type_name(vhGetObjTypeName(obj_type));
        UtString node_str;
        VhdlPopulate::decompileJaguarNode(node, &node_str);
        StrToken tok(node_str.c_str(), "\n");
        SourceLocator loc = locator(node);
        mMsgContext->PrintSourceLocation(&loc, "VHDL Error Stack:", index, *tok);
        ++index;
      }
    }
  } // if
}
