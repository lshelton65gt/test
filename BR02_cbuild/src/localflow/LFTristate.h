// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef TRISTATE_H_
#define TRISTATE_H_

#include "nucleus/Nucleus.h"
#include "reduce/UseBeforeKill.h"

class AtomicCache;
class ConstantRange;
class CopyContext;
class DisjointRange;
class DynBitVector;
class Fold;
class MsgContext;
class NUStmtReplaceCallback;
class IODBNucleus;

/*!
  \file
  Declaration of local tristate package.
 */

//! Class which detects local tristates.
/*!
  Detect tristate drivers, and create NUEnabledDriver nodes for them.
*/
class LFTristate
{
public:
  //! constructor
  /*!
    \param str_cache String cache to use to create temporaries.
    \param msg_ctx Context to issue error messages.
    \param verbose If true, will be verbose while analysis occurs.
   */
  LFTristate(AtomicCache* str_cache,
	     NUNetRefFactory *netref_factory,
             MsgContext *msg_ctx,
             bool verbose,
             IODBNucleus* iodb) :
    mAtomicCache(str_cache),
    mNetRefFactory(netref_factory),
    mMsgContext(msg_ctx),
    mVerbose(verbose),
    mIODB(iodb)
  {}

  //! destructor
  ~LFTristate() {}

  //! Transform continuous assigns of Z exprs into NUContEnabledDrivers
  bool createEnabledDrivers(NUModule *module);

  //! Resynthesize blocks with z-assigns to pull out continuous assigns to z exprs
  bool resynthesizeZBlocks(NUModule *module);

  //! Perform tristate analysis on this continuous assign.
  /*!
   * Attempt tristate analysis on the continuous assign, try to turn the
   * continuous assign into an enabled driver.  If successful, the given
   * assign will be deleted and an enabled driver created.

   \param assign The continuous assign to analyze
   \param assign_list List to which continuous assigns will be added
   \param assign_list List to which enabled drivers will be added
   \param ignoreRHS bool indicating if all rhs expressions are accepted.
   \return true if an enabled driver was added, false otherwise
   */
  bool contAssign(NUContAssign *assign,
		  NUContAssignList *assign_list,
		  NUEnabledDriverList *drivers_list);

  typedef std::pair<NUAssign*,ConstantRange> AssignRange;
  typedef UtVector<AssignRange> AssignRangeVec;
  typedef UtHashMap<NUNet*,AssignRangeVec> NetAssignRangeVecMap;

  //! helper class to find nets assigned to Z and convert conditional
  //! expressions involving Z to if-then-else statements with assign to
  //! constant Z.
  class ZNetConvertCallback;

  //! helper class to find all assigns to interesting nets (those driven by Z)
  class TristateDriverCallback;

  //! Helper class to find uses of tristateEnable nets and replace them with
  //! their current drive value (which is initialized at the top of the module
  class ReplaceTriNetUses;

private:
  //! Hide copy and assign constructors.
  LFTristate(const LFTristate&);
  LFTristate& operator=(const LFTristate&);

  //! Mark the net flags as tristate, in particular, primaryZ gets marked here
  void markTristateNet(NUNet* znet);

  //! Process an structuredProc block, looking for nets that can be driven
  //! by Z, and constructing a continuous enabled driver to reflect
  //! the drive-status of the structuredProc block
  bool structuredProc(NUStructuredProc* structuredProc);

  //! Find the nets assigned to Z in a structured proc
  void findTristateNets(NUStructuredProc* structuredProc,
                        NUNetSet* tristateNets,
                        NUNetSet* usedNets);

  //! Find all assignments to the specified set of nets, building a map
  void findNetAssignments(NUStructuredProc* structuredProc,
                          const NUNetSet& tristateNets,
                          NetAssignRangeVecMap* netAssignRangeVecMap);
  
  //! If a net is only driven by Z, then delete the assign rather than
  //! making it a tristate net.  Return true if it did this
  bool handleZOnly(NUStructuredProc* structuredProc,
                   const AssignRangeVec& assignRangeVec,
                   NUStmtReplaceCallback* replacements);

  //! Analyze a procedurally driven tristate net's drivers,
  //! maintaining drive-bits and enable-bits separately.  Build
  //! continuous enabled drivers to create a schedulable representation
  //! for the rest of build
  void resynthTriDrivers(NUStructuredProc* structuredProc,
                         NUNet* triNet, 
                         const AssignRangeVec& assignRangeVec,
                         const DisjointRange& enaRanges,
                         NUStmtReplaceCallback* replacements,
                         NUNetReplacementMap* tristateToDriveVal,
                         NUStmtList* initializers,
                         const UseBeforeKill::NetToLoc& usedNets);

  //! Resynthesize the driver to tristate nets to separate the
  //! drive value from the enable value.  Don't do the block rewrite
  //! yet -- keep track of the replacement map in replacements
  void createReplacementStmts(NUScope* scope,
                              NUStmtReplaceCallback* replacements,
                              CopyContext& xlateDrive, NUNet* ena,
                              const DisjointRange& enaRanges,
                              const AssignRangeVec& assignRangeVec);

  //! String cache
  AtomicCache *mAtomicCache;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context.
  MsgContext *mMsgContext;

  //! Be verbose as processing occurs?
  bool mVerbose;

  IODBNucleus* mIODB;
};

#endif
