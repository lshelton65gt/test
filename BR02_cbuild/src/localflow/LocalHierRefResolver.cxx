// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "LocalHierRefResolver.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUStmtReplace.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTable.h"
#include "reduce/RewriteUtil.h"


LocalHierRefResolver::LocalHierRefResolver(AtomicCache *str_cache) :
  mStrCache(str_cache)
{
}


LocalHierRefResolver::~LocalHierRefResolver()
{
}


void LocalHierRefResolver::design(NUDesign *design)
{
  for (NUDesign::ModuleLoop modules = design->loopAllModules();
       not modules.atEnd();
       ++modules) {
    NUModule *this_module = *modules;
    module(this_module);
  }
}


void LocalHierRefResolver::module(NUModule *module)
{
  moduleNets(module);
  moduleTasks(module);
}


void LocalHierRefResolver::moduleNets(NUModule *module)
{
  NUNetReplacementMap replacement_map;

  // Try to resolve all the net hierrefs.
  NUNetList hier_ref_nets;
  module->getAllHierRefNets(&hier_ref_nets);
  for (NUNetList::iterator iter = hier_ref_nets.begin();
       iter != hier_ref_nets.end();
       ++iter)
  {
    NUNet *net = *iter;
    NUNetHierRef *hier_ref = net->getHierRef();
    NU_ASSERT(hier_ref, net);

    NUModuleInstanceVector path;
    NUNet *resolved_net = 0;
    if (hier_ref->isLocallyRelative(&path, &resolved_net)) {
      // Resolved to this module if the instance path is empty.
      if (path.size() == 0) {
        replacement_map[net] = resolved_net;
      } else {
        // Resolved to a child module.  But, it is posible the
        // hierref has this module's name as the start of
        // the hierref.  Reconstruct a path name from the
        // path returned and use that.
        AtomArray atomArray;
        hier_ref->localizeNameForParent(module, 0, &atomArray);
        if (atomArray != hier_ref->getPath()) {
          // Possible that there is already a net with this name,
          // so use that as a replacement if it exists.
          NUNet* existing_net = module->findNetHierRef(atomArray);
          if (existing_net) {
            replacement_map[net] = existing_net;
          } else {
            hier_ref->putPath(atomArray);
          }
        }
      }
    }
  } // for

  // Replace any resolved net hierrefs with their resolutions.
  // RewriteLeaves can handle a bunch of replacements, so need
  // to make a bunch of dummy maps for it.
  NULvalueReplacementMap dummy_lvalue_replacements;
  NUExprReplacementMap dummy_expr_replacements;
  NUTFReplacementMap dummy_tf_replacements;
  NUCModelReplacementMap dummy_cmodel_replacements;
  NUAlwaysBlockReplacementMap dummy_always_replacements;
  RewriteLeaves rewriter(replacement_map,
                         dummy_lvalue_replacements,
                         dummy_expr_replacements,
                         dummy_tf_replacements,
                         dummy_cmodel_replacements,
                         dummy_always_replacements,
                         0 /* Don't pass in a fold object. */);
  module->replaceLeaves(rewriter);

  // Cleanup the resolved net hierrefs.
  for (NUNetReplacementMap::iterator iter = replacement_map.begin();
       iter != replacement_map.end();
       ++iter) {
    NUNet *net = const_cast<NUNet*>((*iter).first);
    NUNet *resolution = (*iter).second;
    module->removeNetHierRef(net, true);

    // Possible that we are replacing one hierref with another.
    // Only cleanup the resolution when that is not the case.
    if (not resolution->isHierRef()) {
      resolution->removeHierRef(net);
    }
    delete net;
  }
}

void LocalHierRefResolver::moduleTasks(NUModule *module)
{
  NUStmtReplaceCallback rewriter;

  // Try to resolve all the task hierrefs.
  NUTaskEnableVector hier_ref_tasks;
  module->getTaskEnableHierRefs(&hier_ref_tasks);
  for (NUTaskEnableVector::iterator iter = hier_ref_tasks.begin();
       iter != hier_ref_tasks.end();
       ++iter) {
    NUTaskEnable *enable = *iter;
    NUTaskHierRef *hier_ref = enable->getHierRef();
    NU_ASSERT(hier_ref, enable);

    NUModuleInstanceVector path;
    NUTask *task = 0;
    if (hier_ref->isLocallyRelative(&path, &task)) {
      // Resolved to this module if the instance path is empty.
      if (path.size() == 0) {
        // Replicate the arg connections.
        CopyContext copy_ctx(0,0);
        NUTFArgConnectionVector connections;
        for (NUTFArgConnectionLoop conn_iter = enable->loopArgConnections();
             not conn_iter.atEnd();
             ++conn_iter) {
          connections.push_back((*conn_iter)->copy(copy_ctx));
        }

        // Create the new enable and remove the task's pointer
        // to the hierref.
        NUTaskEnable *new_enable = new NUTaskEnable(task, module,
                                                    enable->getNetRefFactory(),
                                                    enable->getUsesCFNet(),
                                                    enable->getLoc());
        new_enable->setArgConnections(connections);
        task->removeHierRef(enable);
        rewriter.addReplacement(enable, new_enable);
      } else {
        // Resolved to a child module.  But, it is posible the
        // hierref has this module's name as the start of
        // the hierref.  Reconstruct a path name from the
        // path returned and use that.
        AtomArray atomArray;
        hier_ref->localizeNameForParent(module, 0, &atomArray);
        hier_ref->putPath(atomArray);
      }
    }
  }

  // Walk the module, replacing the resolved task enables.
  NUDesignWalker walker(rewriter, false);
  walker.module(module);

  // Nothing to cleanup; the walker deletes the stmts, and
  // we removed the task's pointer to the hierref when we
  // created the replacement enable.
}
