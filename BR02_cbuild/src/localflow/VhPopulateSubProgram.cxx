// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUNet.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/ArgProc.h"
#include "util/CbuildMsgContext.h"
#include "util/AtomicCache.h"
#include "reduce/Fold.h"

extern bool isPredefined3StateLogicOrBit(vhNode node); // in VhPopulateExpr.cxx
extern bool isUnconstrainedArray( vhNode constraint ); // in VhPopulateExpr.cxx

//! Helper to determine mode of parameter
NUTF::CallByMode VhdlPopulate::getParamMode (bool isSignal)
{
  // signals must be call by ref, let codegen determine style for other cases
  return isSignal ? NUTF::eCallByReference : NUTF::eCallByAny;
}

vhNode VhdlPopulate::getVhIntegerType()
{
  static vhNode sVhIntegerType = NULL;

  if (sVhIntegerType == NULL) {
    vhNode pkgStandard = vhOpenPackDecl( "STD", "STANDARD" );
    JaguarList stdDeclList( vhGetTypeDeclList( pkgStandard ));
    vhNode intType;
    while (( intType = vhGetNextItem( stdDeclList )))
    {
      if ( !strcmp( vhGetName( intType ), "INTEGER" ))
        break;
    }
    sVhIntegerType = intType;
    //vhCloseDesignUnit( pkgStandard ); // Don't close this or sVhIntegerType goes away
  }
  return sVhIntegerType;
}

//
// This routine handles the population steps common for procedure and functions
// It processes their parameter list, declaration_items and sequential_stmts.
//
Populate::ErrorCode VhdlPopulate::subProgCommon(NUTF* subprog,
                                                vhNode vh_subprog,
                                                LFContext* context)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  context->pushBlockScope(subprog);
  context->pushDeclarationScope(subprog);
  context->pushJaguarScope(vh_subprog);

  // Make a block to hold the statements of the subprogram, so that if
  // it gets disabled from inside, the break-processing can find an
  // NUBlock target via mBlockMap.  The NUBlock will be deleted during
  // BreakResynth.
  SourceLocator loc = locator(vh_subprog);
  NUBlock* block = context->stmtFactory()->createBlock(context->getBlockScope(),
                                                       false,
                                                       loc);
  mapBlock(vh_subprog, block);
  context->pushBlockScope(block);

  JaguarList vh_arg_iter(vhGetFlatParamList(vh_subprog));
  vhNode vh_arg;
  while ((vh_arg = vhGetNextItem(vh_arg_iter))) 
  {
    NUNet *net = 0;
    if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_arg, context ))
    {
      temp_err_code = processRecordPortDecl( vh_arg, context );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      {
        context->popBlockScope();
        context->popBlockScope();
        unmapBlock(vh_subprog, block);
        return temp_err_code;
      }
    }
    else
    {
      temp_err_code = subProgArg( vh_arg, context, &net );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      {
        context->popBlockScope();
        context->popBlockScope();
        unmapBlock(vh_subprog, block);
        return temp_err_code;
      }
      if (net) 
      {
        bool isSignal = (VHSIGNAL == vhGetObjType (vh_arg));
        if (isSignal)
          // Because we do non-blocking writes to signals, we get in trouble
          // in the NBA reordering for signals - always inline any subroutine that
          // passes signal parameters in or out.
          if (NUTask* task = dynamic_cast<NUTask*>(subprog))
            task->putRequestInline (true);

        subprog->addArg(net, getParamMode (isSignal));
      }
    }
  }

  // stmt_list could be used by processDeclaration to add sequential statements
  // that initialize declared variables.
  NUStmtList stmt_list;
  context->pushStmtList(&stmt_list);
  
  // Process the declarations
  temp_err_code = processDeclarations(vh_subprog, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    context->popStmtList();
    context->popBlockScope();
    context->popBlockScope();
    unmapBlock(vh_subprog, block);
    return temp_err_code;
  }

  JaguarList vh_stmt_iter( vhGetSeqStmtList( vh_subprog ));
#if 0  
  // for the test test/vhdl/lang_func/bug6116.vhd this code
  // (instead of the following call to sequentialStmtList(&vh_stmt_iter,context))
  // will reduce the code that is populated, but you can only detect
  // this as a dumpVerilog test and we should have more tests before
  // inserting this optimization.
  JaguarList elab_list;
  (void) elaborateSeqStmtList(&vh_stmt_iter, &elab_list);
  temp_err_code = sequentialStmtList( &elab_list, context );
#endif

  temp_err_code = sequentialStmtList( &vh_stmt_iter, context );
  // We can use errorCodeSaysReturnNowOnlyWithFatal here, because population of some of the sequential 
  // statements may fail, but we want to continue anyway.
  // One of the test cases that demonstrates this action is test/bugs/bug4193/now.vhdl
  if (errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
  {
    context->popStmtList();
    context->popBlockScope();
    context->popBlockScope();
    unmapBlock(vh_subprog, block);
    return temp_err_code;
  }
  context->popStmtList();

  block->addStmts(&stmt_list);
  context->popBlockScope();     // for NUBlock
  unmapBlock(vh_subprog, block);

  stmt_list.clear(); // reuse of stmt_list
  stmt_list.push_back(block);

  subprog->addStmts(&stmt_list);

  context->popBlockScope();
  context->popDeclarationScope();
  context->popJaguarScope();

  if (!context->stmtFactory()->next(block)) { // Done populating block statements.
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }

  return err_code;
}

// Enrolls the task as a constant value returning task if all return expressions
// are constant and identical.
bool VhdlPopulate::checkFunctionConstness(NUTask* task, FuncRetValCollector& frvc)
{
  NUConst* retConstExpr = NULL;
  bool done = false;

  NUNet* formalNet = task->getArg(0);
  // Allow only bit and vector constants and disallow everything else like
  // composites and memories.
  if (!formalNet->isBitNet() && !formalNet->isVectorNet()) {
    done = true; // Don't look for constant return values.
  }

  const NUExprList& funcRetExprs = frvc.getRetValExprs();
  for (NUExprListIter itr = funcRetExprs.begin();
       (itr != funcRetExprs.end()) && !done; ++itr)
  {
    NUConst* nextConstExpr = (*itr)->castConstNoXZ();
    if (nextConstExpr == NULL)
    {
      // Found a non-const expr.
      retConstExpr = NULL;
      done = true;
    }
    else if (retConstExpr == NULL)
    {
      retConstExpr = nextConstExpr;
    }
    else if (*retConstExpr != *nextConstExpr)
    {
      // Found a non-matching const expr.
      retConstExpr = NULL;
      done = true;
    }
  }

  if (retConstExpr != NULL) { // Function returns a constant
    mapTaskConstantReturn(task, retConstExpr);
    return true;
  }
  return false;
}

// NOTE: FOR RECURSIVE FUNCTIONS, THE vh_func_call ARGUMENT DOES NOT
// REPRESENT A CALL TO THE FUNCTION GIVEN BY vh_function ARGUMENT. THE CALL
// CORRESPONDS TO THE CALL MADE FROM WITHIN THE vh_function. 
// See test/vhdl/lang_func/bug6597.vhdl.
Populate::ErrorCode
VhdlPopulate::function( vhNode vh_function, vhExpr vh_func_call, StringAtom* name,
                        int returnWidth, vhExpr vh_size, LFContext *context,
                        const bool isRecursive, NUTask **the_task )
{
  // Turn off aggressive evaluation in function. Using initial values of 
  // variables declared within function declaration scope for expression evaluation
  // may result in incorrect population. Constant propagation will provide the
  // correct instantaneous constant values for variables.
  AggressiveEvalEnabler aee(this, false);

  const SourceLocator loc = locator(vh_function);
  ErrorCode err_code = eSuccess;

  NUModule *module = context->getModule();
  StringAtom *origName = context->getStringCache()->intern(vhGetSubProgName(vh_function));
  *the_task = context->stmtFactory()->createTask(module, name, origName, loc);

  // Create a list to collect return values of this function. This might help
  // us identify if this function returns a constant value. Avoid recusive functions
  // and impure functions. The code isn't geared up to handle returns from various
  // recursions of same function. Impure functions that return constant cannot
  // be optimized away because they modify variables outside it's scope.
  const VhPureType purity = vhGetPureType(vhGetSubProgSpec(vh_function));
  bool enableRetValCollection = false;
  if (!isRecursive && ((purity == VH_DEFAULT_PURE) || (purity == VH_PURE))) {
    enableRetValCollection = true;
  }

  FuncRetValCollector frvc(this, enableRetValCollection);

  // The task may be declared in a package. The package may or may
  // not have been populated, depending on whether it had any nets
  // declared in package declaration or body. The test
  // test/vhdl/lang_misc/bug6896.vhdl 
  // has nets in package body.
  NUScope* hdlScope = NULL;
  vhNode vh_func_scope = vhGetScope(vh_function);
  vhObjType obj_typ = vhGetObjType(vh_func_scope);
  if ((obj_typ == VHPACKAGEBODY) || (obj_typ == VHPACKAGEDECL))
  {
    JaguarString libname(vhGetLibName(vh_func_scope));
    JaguarString packname(vhGetPackName(vh_func_scope));
    StringAtom* pack_id = createPackIdStr(libname, packname, context);
    NUNamedDeclarationScope* pack_scope = NULL;
    lookupPackage(pack_id, &pack_scope, context);
    hdlScope = pack_scope;
  }

  if (hdlScope == NULL) {
    hdlScope = context->getDeclarationScope();
  }

  if ( hdlScope->getScopeType() != NUScope::eModule )
    (*the_task)->setHdlParentScope( hdlScope );
  (*the_task)->putRequestInline(context->doInlineTasks());
  // Create the function result 
  context->pushBlockScope(*the_task);
  context->pushDeclarationScope(*the_task);
  context->pushJaguarScope(vh_function);
  NUNetVector function_result;
  // pass the name of the NUNet to be created. It has to be the new unique
  // name generated for the function by gensym()
  ErrorCode temp_err_code = functionResult(vh_function, vh_func_call, returnWidth,
                                           vh_size, context, &function_result);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
    return temp_err_code;
  }

  context->popBlockScope();
  context->popDeclarationScope();
  context->popJaguarScope();

  // add the nets to the argument list (as the first n args)
  for( NUNetVector::iterator iter = function_result.begin();
       iter != function_result.end();
       ++iter )
  {
    // turn net into an output, and add it to argument list (as first arg)
    NUNet* resultArg = *iter;
    resultArg->putIsOutput(true);
    (*the_task)->addArg(resultArg, NUTF::eCallByCopyOut);
  }

  temp_err_code = subProgCommon(*the_task, vh_function, context);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
  {
    return temp_err_code;
  }

  if (mIsFuncConstPropEnabled)
  {
    // Look through returned expressions and find out if this function returns
    // a constant. If it does, save this info.
    bool isConstRet = checkFunctionConstness(*the_task, frvc);
    // Only add task to module if it does not return a constant value. If it does,
    // it will be deleted and all calls to it will be replaced with constant value.
    if (!isConstRet) {
      module->addTask(*the_task);
    }
  }
  else
  {
    module->addTask(*the_task);
  }

  // Function population done.
  if (!context->stmtFactory()->next(*the_task)) {
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }
  return err_code;
}


Populate::ErrorCode VhdlPopulate::procedure(vhNode vh_procedure,
                                            StringAtom* name,
                                            LFContext *context,
                                            NUTask **the_procedure)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(vh_procedure);

  StringAtom *origName = context->getStringCache()->intern(vhGetSubProgName(vh_procedure));
  *the_procedure = 
    context->stmtFactory()->createTask(context->getModule(), name, origName, loc);

  // The procedure may be declared in a package. The package may or may not
  // have been populated depending on whether it had any nets declared in it.
  NUScope* hdlScope = NULL;
  vhNode vh_proc_scope = vhGetScope(vh_procedure);
  vhObjType obj_typ = vhGetObjType(vh_proc_scope);
  if ((obj_typ == VHPACKAGEBODY) || (obj_typ == VHPACKAGEDECL))
  {
    JaguarString libname(vhGetLibName(vh_proc_scope));
    JaguarString packname(vhGetPackName(vh_proc_scope));
    StringAtom* pack_id = createPackIdStr(libname, packname, context);
    NUNamedDeclarationScope* pack_scope = NULL;
    lookupPackage(pack_id, &pack_scope, context);
    hdlScope = pack_scope;
  }

  if (hdlScope == NULL) {
    hdlScope = context->getDeclarationScope();
  }

  if ( hdlScope->getScopeType() != NUScope::eModule )
    (*the_procedure)->setHdlParentScope( hdlScope );
  (*the_procedure)->putRequestInline(context->doInlineTasks());

  ErrorCode temp_err_code = subProgCommon(*the_procedure,vh_procedure,context);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code))
  {
    return temp_err_code;
  }
  context->getModule()->addTask(*the_procedure);

  // Procedure population done.
  if (!context->stmtFactory()->next(*the_procedure)) {
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }
  return err_code;
}

// For function's return statement, a set of NUNets is created. These
// nets will have the same name as the subprogram's mangled name, with a
// uniquifier appended, and will be made the first n output ports of the
// NUTask corresponding to this function.
//
// A set of NUBlockingAssigns is populated using these net.  During the
// processing of a return statement the NUNets created here are used as
// the lvalues, and the return expression is used as the rvalues of the
// assignment.

// NOTE: FOR RECURSIVE FUNCTIONS, THE vh_func_call ARGUMENT DOES NOT
// REPRESENT A CALL TO THE FUNCTION GIVEN BY vh_function ARGUMENT. THE CALL
// CORRESPONDS TO THE CALL MADE FROM WITHIN THE vh_function.
// See test/vhdl/lang_func/bug6597.vhdl.
Populate::ErrorCode
VhdlPopulate::functionResult(vhNode vh_function, vhExpr vh_func_call,
                             int returnWidth, vhExpr vh_size,
                             LFContext *context, NUNetVector *ret_vector)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator(vh_func_call);
  NUNet *the_net = 0;

  // This is a temporary net; use the block as scope, not declaration context.
  NUScope *parent_scope = context->getBlockScope();
  NetFlags flags = NetFlags(eTempNet|eAllocatedNet|eBlockLocalNet|eNonStaticNet|eOutputNet);

  VhNodeArray recConstraintArray;
  vhNode retType = vhGetReturnType(vh_function);
  if ( !mCarbonContext->isNewRecord() && isRecordNet( retType, context ))
  {
    flags = NetFlags( flags | eRecordPort );
    vhNode typeDef;
    if ( isUnconstrainedArray(retType))
    {
      // If retType is unconstrained, getVhdlRecordType cannot handle
      // it, since a functionResult will not have an initial value.  The
      // bounds that we want for that array dimension is vh_size.  So,
      // strip the unconstrained array off and put vh_size in
      // recConstraintArray.
      vhNode vh_typedef = vhGetTypeDef( retType );
      if ( ( vhGetObjType( vh_typedef ) == VHUNCONSARRAY ) && ( vh_size != NULL ) )
      {
        recConstraintArray.push_back( vh_size );
        retType = vhGetExpressionType( static_cast<vhExpr>( vhGetEleSubTypeInd( vh_typedef )));
      }
    }

    getVhdlRecordType( retType, &typeDef, &recConstraintArray );
    RecordIterator ri( this, typeDef );

    for( vhNode fieldElem = ri.next(); fieldElem != NULL; fieldElem = ri.next( ))
    {
      NetFlags recFlags = flags;
      if ( !isRecordNet( fieldElem, context ))
      {
        JaguarString fieldName(vhGetName( fieldElem ));
        StringAtom *eleAtom = parent_scope->gensym( "return" , fieldName );
        // Set the signed flag of this record element if it's a signed net
        calcSignedNetFlags(fieldElem, &flags, context);
        temp_err_code = allocNet( fieldElem, eleAtom, recFlags, parent_scope,
                                  &the_net, context, &recConstraintArray );
        ret_vector->push_back( the_net );
      }
    }
  }
  else
  {
    VhdlRangeInfoVector dimVector; 
    VhdlRangeInfo vrange;
    temp_err_code = eNotApplicable;
    temp_err_code = getVhExprRange( static_cast<vhExpr>( retType ), dimVector, loc,
                                    false, false, true, context );
    if ( temp_err_code == eNotApplicable )
    {
      // Use the elaborated size node from the function call; this is an
      // unconstrained array.
      if ( vh_size )
      {
        temp_err_code = getVhExprRange( vh_size, dimVector, loc,
                                        false, false, true, context );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;
        vhNode vh_typedef = vhGetTypeDef( retType );
        LOC_ASSERT( vhGetObjType( vh_typedef ) == VHUNCONSARRAY, loc );
        vhExpr vh_eletype = static_cast<vhExpr>( vhGetEleSubTypeInd( vh_typedef ));
        temp_err_code = getVhExprRange( vh_eletype, dimVector, loc,
                                        false, false, true, context );
      }
      else
      {
        // If size of function call was not provided by Jaguar i.e vh_size is NULL,
        // compute the return value range by looking at return statements in
        // the elaborated function body. This works in the case where the function
        // return type constraints are based on function call arguments, which
        // could be dynamic.
        temp_err_code = getFunctionReturnRange(vh_function, dimVector, loc,
                                               false, false, false, context);
      }

      // We could not determine the width from the function itself, so
      // we must rely on the width coming from the lvalue.
      if (temp_err_code == eNotApplicable || temp_err_code == eFailPopulate)
      {
        // We give up if all attempts do not yield function call return width.
        if ( (returnWidth == 0) ||
             (mCarbonContext->isNewRecord() && isRecordNet(vh_func_call, context)) )
        {
          UtString buf;
          POPULATE_FAILURE(mMsgContext->FailedToDetermineRange(&loc, decompileJaguarNode(vh_func_call, &buf)), &err_code);
          return err_code;
        }
        // our last choice is to use the width defined by lValue,
        temp_err_code = eSuccess;         // make this look like a success
        if ( returnWidth == 1 )
        {
          vrange.putScalar();
        }
        else
        {
          vrange.putVector( returnWidth - 1, 0 );
        }
        dimVector.push_back( vrange );
      }
    }

    if (errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code) ) {
      UtString buf;
      mMsgContext->FailedToDetermineRange(&loc, decompileJaguarNode(vh_func_call, &buf));
      return err_code;
    }

    VectorNetFlags vflags = calcSignedNetFlags(retType, &flags, context);

    // I only need to uniquify the name of the formal return arg in the
    // context of the NUTask.  It is not necessary to:
    //   1. put then name of the function itself into the return-arg name
    //   2. uniquify it in the context of the entire module
    // doing so is redundant, and disrupts congruence between otherwise
    // identical functions.
    // 
    StringAtom* name = parent_scope->gensym( "return" );
    if ( mCarbonContext->isNewRecord() && isRecordNet( retType, context ))
    {
      temp_err_code = allocRecordNet( retType, name, &recConstraintArray, flags,
                                      parent_scope, context, vh_size, &the_net );
    }
    else
    {
      ConstantRangeArray dimArray;
      ConstantRange *range;
      // This code deliberately reverses ranges.  The way dimVector is
      // populated, dimVector[0] is the outermost range, where
      // dimArray[0] needs to be the innermost range in order to
      // allocate the net properly.
      for ( SInt32 i = dimVector.size() - 1; i >= 0; --i )
      {
        vrange = dimVector[i];
        if (!vrange.isValidMsbLsb()) {
          // Range may not be known for function return types. However
          // width should be known. 
          LOC_ASSERT(vrange.isValidWidth(), loc);
          range = new ConstantRange(vrange.getWidth(loc) - 1, 0);
        } else {
          range = new ConstantRange( vrange.getMsb(loc), vrange.getLsb(loc) );
        }
        dimArray.push_back( range );
      }
      temp_err_code = allocNet( name, flags, vflags, &dimArray, parent_scope, loc,
                                &the_net, context );
    }
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    temp_err_code = mapNet( parent_scope, vh_function, the_net );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    ret_vector->push_back( the_net );
  }

  return err_code;
}


Populate::ErrorCode VhdlPopulate::subProgArg(vhNode vh_arg,
                                             LFContext *context,
                                             NUNet **the_net)
{
  vhObjType type = vhGetObjType(vh_arg);
  if (type == VHSIGNAL)
    return net(vh_arg, context, the_net);
  else
    return var(vh_arg, context, the_net); 
}

/*
 * This routine will handle the processing of the declarative_items of 
 * subprogram body.
 * Supported items : 
 *       variable declaration
 *  
 * Indirectly supported :
 *       subprogram_decl :
 *       subprogram_body : 
 *       type declaration :
 *       subtype declaration :
 *       attribute declaration :( only ENUM_ENCODING )
 *                 These are populated when used in subprogram_statement part
 *
 */
Populate::ErrorCode VhdlPopulate::subProgLocal(vhNode vh_decl_item,
                                               LFContext *context,
                                               NUNet **the_net)
{
  ErrorCode err_code = eSuccess;
  // TBD do we need to check for redeclaration of interface arguments (?)
  *the_net = 0;
  SourceLocator loc = locator(vh_decl_item);

  switch(vhGetObjType(vh_decl_item))
  {
  case VHVARIABLE:
    return var(vh_decl_item, context, the_net);
  case VHSUBPROGBODY:
  case VHSUBPROGDECL:
  case VHALIASDECL: 
  case VHTYPEDECL: 
  case VHSUBTYPEDECL: 
  case VHATTRBDECL:
  case VHATTRIBUTESPEC:
  case VHCONSTANT:
    break; // All these would be processed as and when used in statements/expr.
  default:
  {
    UtString msgStr;
    msgStr << "Unsupported declarative item ("
           << gGetVhNodeDescription( vh_decl_item ) << ") in a subprogram.";
    POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, msgStr.c_str( )), &err_code);
    return err_code;
    break;
  }
  }
  return err_code;
}

bool
VhdlPopulate::functionSignExtendsOperand (const char *package, const char *func_name, vhExpr vh_arg)
{
  return ( 
    	   // SXT obviously SigneXTends its operand
    	   !strcasecmp(func_name,"SXT") || 
           // Complex rules here
           // Package	   	 Argument         Extends
           // ===============    ========         =======
           // STD_LOGIC_ARITH    SIGNED           yes
           //       "            UNSIGNED         no
           //       "            INTEGER          yes (but tautology)
           //       "            STD_ULOGIC       no
           // STD_LOGIC_UNSIGNED STD_LOGIC_VECTOR no
           // STD_LOGIC_SIGNED   STD_LOGIC_VECTOR yes
           //
           (!strcasecmp(func_name,"CONV_INTEGER")
            && ((!strcasecmp (package, "STD_LOGIC_ARITH")
                 && eVHSIGNED == getVhExprSignType ((vhExpr)vh_arg))
                || !strcasecmp (package, "STD_LOGIC_SIGNED"))
           ) ||
           // remaining cases are sensitive to the input argument's signedness
           ( ( VhdlPopulate::eVHSIGNED == getVhExprSignType((vhExpr)vh_arg) ) &&
             ( !strcasecmp(func_name,"RESIZE") ||
               !strcasecmp(func_name,"TO_INTEGER") ||
               !strcasecmp(func_name,"CONV_SIGNED") ||
               !strcasecmp(func_name,"CONV_UNSIGNED")
             )
           )
           // CONV_STD_LOGIC_VECTOR should always return false
    );
}

/*
 * This routine will handle the population of the FILE-IO functions.
 *      Currently, Its only ENDFILE. 
 */
Populate::ErrorCode VhdlPopulate::sysFunctionCall(vhExpr vh_func_call,
                                                  LFContext *context,
                                                  NUExpr **the_expr)
{
  ErrorCode     err_code = eSuccess;
  SourceLocator loc = locator(vh_func_call);
  NUOp::OpT     op = NUOp::eZEndFile;
  NUExprVector  expr_vector;
  NUNet        *net = 0;
  
  if ( not mArg->getBoolValue (CarbonContext::scVhdlEnableFileIO) )
  {
    *the_expr = NUConst::create (false, 0u, 32, loc); // dummy
    mMsgContext->DisabledSysFunc(&loc, "endfile", CarbonContext::scVhdlEnableFileIO);
    return err_code;
  }
  
  JaguarList        associationList(vhGetAssocList(vh_func_call));
  vhNode        element = vhGetNextItem(associationList);
  if ((vhGetObjType(element) == VHOBJECT) &&
        (element = vhGetActualObj(element)))
  {
    ErrorCode temp_err_code = lookupNet(context->getDeclarationScope(), element,
                                        context, &net);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
  }
  if (net == 0) 
  {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc,"File object not found in the argument list of file_open()."), &err_code);
    return err_code; // File object/descriptor not found
  }
 
  NUExpr *expr = 0;
  expr = createIdentRvalue(net, loc);
  expr->resize (expr->determineBitSize ());
  expr_vector.push_back (expr);

  NUSysFunctionCall* sysCall = new NUSysFunctionCall(op, 
                                                     0,0, /* Not Applicable */
                                                     expr_vector, loc);
  *the_expr = sysCall;
 
  return err_code;

}

// eBiVhExt is requires special consideration because the self-determined
// size is based on the *value* of its second arg.  So we need to fold
// it to do arithmetic simplification.  If we don't get a constant out,
// and size is not known from context, then we can't populate it.
NUExpr* VhdlPopulate::createSizeExpr(NUExpr* val, NUExpr* sz,
                                     const SourceLocator& loc,
                                     UInt32 lhsWidth, bool sign)
{
  sz = sz->makeSizeExplicit(32);

  // The size expression should always be unsigned.
  sz->setSignedResult(false);
  sz = mFold->fold(sz);
  if (lhsWidth == 0) {
    NUConst* k = sz->castConst();
    if ((k == NULL) || !k->getUL(&lhsWidth)) {
      mMsgContext->VhdlNoStaticSize(&loc);
      return NULL;
    }
  }
  NUExpr* ret = new NUBinaryOp(sign ? NUOp::eBiVhExt : NUOp::eBiVhZxt, val, sz, loc);
  ret->resize(lhsWidth);
  ret->setSignedResult (sign);
  return ret;
}

// The VHDL function RESIZE() handles the size truncation in different way.
// Here the MSB of the truncated expr is the MSB of the original
// expr. So, simple part select won't work.
// example: out1 <= resize(in1, 4), we will convert the
// rvalue as : bitSize(in1) > 4 ? (  ( (in1 >> (bitSize(in1) -1)) << (4 -1) ) |
//                                (eBiVhExt,in1,4) ) : (eBiVhExt,in1,4);
// If in1 is "11111011", RESIZE(in1,3) should return "111" . In the above case
// we will get this result as:
// 8 > 3 ? ( ( ( "11111011" >> (8 -1) ) << ( 3 -1 ) ) |
// (eBiVhExt,"11111011",3 -1) ) : (eBiVhExt,"11111011",3)
// ==> ( ( ( "11111011" >> (8 -1) ) << ( 3 -1 ) ) | (eBiVhExt,"11111011",2) )
// ==> ( ( ( "00000001" ) << 2 ) | ( "11" ) )
// ==> ( (  "100" ) | ( "11" ) )
// ==> ( "111" )
//
// Another example, consider the in1 is "01111111", here the exprected result 
// is" "011" . You will get this result if you fit this into the above formula.
//
NUExpr* 
VhdlPopulate::createExprForResizeFunc( NUExpr* extdExpr, 
                                       UInt32 lopWidth, 
                                       const SourceLocator& loc,
                                       UInt32 lhsWidth)
{
  CopyContext copy_context(NULL, NULL);

  // The second expression of the ternary expression is the original VhExt
  NUBinaryOp* extendedExpr = dynamic_cast<NUBinaryOp*>(extdExpr);
  NU_ASSERT (extendedExpr, extdExpr);

  // First check if the ReSizeExpr is constant. If so, then we will not
  // create the ternary expression because we know that whether the call
  // to RESIZE() is for size extension or truncation. Accordingly we will 
  // create the truncated expression or return the extended expression.
  //
  // VHDL says 2nd arg to RESIZE is 'natural', and internally, we use the eBiLshift
  // and eBiRshift operators to do the shifting.  These operators require UNSIGNED
  // shift counts (and NU_ASSERT otherwise!).
  //
  NUExpr* resizeAmount = extendedExpr->getArg(1);
  NUConst *constResize = resizeAmount->castConst();
  bool isSizeTruncation = false;
  if (constResize)
  {
    UInt32 sizeValue = 0;
    constResize->getUL(&sizeValue);
    if (sizeValue >= lopWidth) // THe call to resize was for size extension
    {
      return extendedExpr;
    }
    else
    {
      isSizeTruncation = true;
    }
  }
  NUExpr* sizeExpr = resizeAmount->copy(copy_context);

  // VHDL treats the size argument as a NATURAL number, so remove any
  // signedness from the result expression.  If we were doing runtime checking,
  // VHDL would require us to assert on a negative signed value.
  sizeExpr->setSignedResult (false);

  // Create the expression: final_size -1 i.e. ( 3 -1 ) in the example
  NUExpr *one = NUConst::create(false, UInt64(1),  32, loc);

  NUExpr *finalSizeMinusOne = new NUBinaryOp(NUOp::eBiMinus, sizeExpr, one,loc);
  finalSizeMinusOne->resize(finalSizeMinusOne->determineBitSize());

  // Create given input_expression_bit_count - 1 i.e. ( 7 - 1 ) in the example
  NUExpr* orgBitCount = NUConst::create(false, UInt64(lopWidth), 32, loc);
  orgBitCount->resize(orgBitCount->determineBitSize());

  NUExpr *bitCountMinusOne = new NUBinaryOp(NUOp::eBiMinus, 
                                    orgBitCount, one->copy(copy_context), loc);
  bitCountMinusOne->resize(bitCountMinusOne->determineBitSize());

  // Now create the right shift expression i.e. in1 >> 7 in the example
  NUExpr* lop = extendedExpr->getArg(0)->copy(copy_context);
  NUExpr* rshiftExpr = new NUBinaryOp(NUOp::eBiRshift, lop, bitCountMinusOne,
                                      loc);
  UInt32 new_size = std::max (lhsWidth, rshiftExpr->determineBitSize ());
  rshiftExpr = rshiftExpr->makeSizeExplicit(new_size);
  rshiftExpr->setSignedResult(true);

  // Create the left shift expression i.e. ( (in1 >> 7) << 2 ) in the example
  NUExpr* orgMsb = new NUBinaryOp(NUOp::eBiLshift, rshiftExpr, 
                                  finalSizeMinusOne, loc); 
  orgMsb->resize(orgMsb->determineBitSize());
  orgMsb->setSignedResult(true);

  NUConst* k = NUConst::create(false, lhsWidth, 32, loc);
  orgMsb = createSizeExpr(orgMsb, k, loc, lhsWidth, true);
  if (orgMsb == NULL) {
    return NULL;
  }
    
  orgMsb->resize(lhsWidth);

  // Now create the temporary VHDL extension expression i.e. EXT(in1, 2) example
  // 
  NUExpr* tempExpr = createSizeExpr(lop->copy(copy_context),
                                    finalSizeMinusOne->copy(copy_context),
                                    loc, lhsWidth, false);
  if (tempExpr == NULL) {
    return NULL;
  }
  tempExpr->setSignedResult(true);
  tempExpr->resize(lhsWidth);

  // Finally create the truncated expression.
  NUExpr* truncatedExpr = new NUBinaryOp(NUOp::eBiBitOr, tempExpr, 
                                         orgMsb, loc);
  truncatedExpr->setSignedResult(true);
  truncatedExpr->resize(truncatedExpr->determineBitSize());
  if (isSizeTruncation)
  {
    delete extendedExpr;
    return truncatedExpr;
  }
  // Create the ternary condition i.e (8 > 4) in the given example
  NUExpr* condExpr = new NUBinaryOp( NUOp::eBiUGtr, 
                                     orgBitCount->copy(copy_context), 
                                     sizeExpr->copy(copy_context), loc );
  condExpr->resize(1);

  // The final ternary expression corresponding to the RESIZE() function
  // call with signed argument.
  NUExpr* finalExpr = new NUTernaryOp(NUOp::eTeCond, condExpr, truncatedExpr, 
                                      extendedExpr, loc);
  finalExpr->setSignedResult(true);
  finalExpr->resize(lhsWidth);

  return finalExpr;
} // createExprForResizeFunc()

// Converts IEEE function calls RISING_EDGE(clk), FALLING_EDGE(clk)
// into (clk'EVENT and clk = '1'), (clk'EVENT and clk = '1'), which
// are equivalent. This uses up the clk_expr.
NUExpr* sConvertToEventExpr(NUExpr* clk_expr, bool is_rising, const SourceLocator& loc)
{
  // Create clk'EVENT.
  NUExpr* event_expr = new NUAttribute(clk_expr, NUAttribute::eEvent, loc);
  // Since we've already used the clk_expr in event_expr, make a copy for level_expr.
  CopyContext cc(0, 0);
  NUExpr* clk_expr_cpy = clk_expr->copy(cc);
  // Create appropriate level value like: 1'b1 for rising and 1'b0 for falling edge.
  NUExpr* const_expr = NUConst::create(false, (is_rising ? 1 : 0), 1, loc);
  // Create clk = 0 for falling edge and clk = 1 for rising edge.
  NUExpr* level_expr = new NUBinaryOp(NUOp::eBiEq, clk_expr_cpy, const_expr, loc);
  // Create the final by anding the event and level expressions.
  return new NUBinaryOp(NUOp::eBiBitAnd, event_expr, level_expr, loc);
}

//! Create NUExpr corresponding to a predefined standard package function 
//! call. This function will return eFailPopulate if the called VHDL function
//! does not correspond to any standard package function.
//! only functions from IEEE package and std.textio.endfile are supported here
Populate::ErrorCode 
VhdlPopulate::acceleratedExprForFunctionCall( vhExpr vh_func, 
                                              LFContext *context, 
                                              NUExpr **the_expr,
                                              UInt32 lhsWidth )
{
  SourceLocator loc = locator(vh_func);
  bool createBitTemp = false;
  ErrorCode err_code = eSuccess; 
  LOC_ASSERT(VHFUNCCALL == vhGetObjType(vh_func), loc);

  ErrorCode temp_err_code = acceleratedExprForStdioFunctionCall(vh_func, context, the_expr, lhsWidth);
  if ( temp_err_code == eSuccess ){
    return eSuccess;
  } else if ( temp_err_code == eNotApplicable ) {
    // was not stdio, so just fall through
  } else if ( errorCodeSaysReturnNowIfIncomplete(temp_err_code, &err_code ) ){
    return err_code;
  }

  // look for the function in the IEEE packages
  vhNode func_body = vhGetMaster(vh_func);
  if (NULL == func_body)
    return eNotApplicable;

  vhNode scope = vhGetScope(func_body);
  if (NULL == scope)
    return eNotApplicable;

  // Make sure it is IEEE library
  JaguarString lib_name(vhGetLibName(scope));
  if (0 != strcasecmp(lib_name,"IEEE"))
    return eNotApplicable;

  // Make sure it is the package of our interest
  JaguarString scope_name(vhGetPackName(scope));
  if ( !(!strcasecmp(scope_name,"NUMERIC_BIT") || 
         !strcasecmp(scope_name,"NUMERIC_STD") ||
         !strcasecmp(scope_name,"STD_LOGIC_1164") ||
         !strcasecmp(scope_name,"STD_LOGIC_ARITH") ||
         !strcasecmp(scope_name,"NUMERIC_SIGNED") ||
         !strcasecmp(scope_name,"NUMERIC_UNSIGNED") ||
         !strcasecmp(scope_name,"STD_LOGIC_SIGNED") ||
         !strcasecmp(scope_name,"STD_LOGIC_UNSIGNED") ))
    return eNotApplicable;

  // We have a function call to a function defined in one of the IEEE
  // library packages (of our interest). Now we need to process or
  // ignore it depending on the function prototype. All the predefined
  // functions of our interest fall into 4 categories:
  // 1. Simple feedthru functions
  // 2. Type conversion functions where size and sign are involved
  // 3. Functions that get mapped to an existing operator
  // 4. Functions that get mapped to a constant value

  // Get the function argument list, the actual expression with which the
  // function has been called.
  JaguarList vh_arg_iter(vhGetAssocList(vh_func));
  vhNode vh_arg1 = NULL;

  JaguarString func_name(vhGetName(func_body));
  LOC_ASSERT( func_name, loc );

  bool is_unsigned_result = false;
  bool is_func_call = true;
  if (!strcasecmp(func_name,"TO_01") || // Simple feed thru functions
      !strcasecmp(func_name,"TO_BIT") ||
      !strcasecmp(func_name,"TO_BITVECTOR") || 
      !strcasecmp(func_name,"TO_STDULOGIC") ||
      !strcasecmp(func_name,"TO_STDLOGICVECTOR") ||
      !strcasecmp(func_name,"TO_STDULOGICVECTOR") ||
      !strcasecmp(func_name,"TO_X01") ||
      !strcasecmp(func_name,"TO_X01Z") ||
      !strcasecmp(func_name,"TO_UX01") )
  {
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    ErrorCode temp_err_code = expr((vhExpr)vh_arg, context, 0, the_expr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    if ( !strcasecmp( func_name, "TO_BIT" ) ||
         !strcasecmp( func_name, "TO_STDULOGIC" ))
    {
      // These must return a bit, not a vector
      createBitTemp = true;
    }
    else if ( !strcasecmp( func_name, "TO_X01" ) ||
              !strcasecmp( func_name, "TO_X01Z" ) ||
              !strcasecmp( func_name, "TO_UX01" ))
    {
      vhNode vh_type = vhGetExpressionType( static_cast<vhExpr>( vh_arg ));
      if ( isPredefined3StateLogicOrBit( vh_type ))
      {
        // These functions must return a bit when called with a bit
        createBitTemp = true;
      }
    }
  }
  else if (!strcasecmp(func_name,"TO_INTEGER") ||// Feed thru but size/sign 
           !strcasecmp(func_name,"TO_UNSIGNED") || // handling is required
           !strcasecmp(func_name,"TO_SIGNED") ||
           !strcasecmp(func_name,"CONV_INTEGER") ||
           !strcasecmp(func_name,"CONV_UNSIGNED") ||
           !strcasecmp(func_name,"CONV_SIGNED") ||
           !strcasecmp(func_name,"CONV_STD_LOGIC_VECTOR") ||
           !strcasecmp(func_name,"EXT") ||
           !strcasecmp(func_name,"SXT") ||
           !strcasecmp(func_name,"RESIZE") )
  {
    vh_arg1 = vhGetNextItem(vh_arg_iter);
    NUExpr* expr1 = NULL;
    temp_err_code = expr((vhExpr)vh_arg1, context, 0, &expr1);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    // Get the size of the final expr and extend it with signed bit if the
    // function corresponds to the signed extension function.
    vhNode vh_arg2 = vhGetNextItem(vh_arg_iter);
    if (vh_arg2)
    {
      vh_arg2 = evaluateJaguarExpr( static_cast<vhExpr>( vh_arg2 ));
      NUExpr* sizeExpr = NULL;
      // Construct an expression of constant literals to help fold it to
      // a constant.
      temp_err_code = expr( static_cast<vhExpr>( vh_arg2 ), context, 0, &sizeExpr,
                            true /* propagateRvalue */, false );

      // If we can't get a constant expression, get a non-constant one.
      if (temp_err_code != eSuccess) {
        temp_err_code = expr( static_cast<vhExpr>( vh_arg2 ), context, 0,
                              &sizeExpr );
      }

      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      {
        return temp_err_code;
      }

      // Fold the size expression to a constant.
      bool widthNotStatic = false;
      if (sizeExpr)
      {
        sizeExpr->resize(sizeExpr->determineBitSize());
        NUExpr* foldedSizeExpr = mFold->fold(sizeExpr);
        if (foldedSizeExpr != NULL) {
          sizeExpr = foldedSizeExpr;
        } else {
          widthNotStatic = true;
        }
      }

      NUConst* constSize = sizeExpr->castConst();
      UInt32 sizeValue = 0;
      if (constSize)
      {
        constSize->getUL(&sizeValue);
        if ( sizeValue == 0 )
        {
          POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Width expression evaluates to 0" ), &err_code);
          return err_code;
        }
      }
      else
      {
        sizeValue = lhsWidth;
      }
      if ( sizeValue == 0 )
      {
        sizeValue = expr1->determineBitSize();
      }
      if ( sizeValue == 0 )
      {
        POPULATE_FAILURE(mMsgContext->NonStaticConstant( &loc ), &err_code);
        return err_code;
      }
      if (widthNotStatic) {
        mMsgContext->UnableToDetermineConstant(&loc, func_name, sizeValue);
      }

      bool extending = functionSignExtendsOperand( scope_name, func_name,
                                                   static_cast<vhExpr>( vh_arg1 ));
      if (extending)
        expr1->setSignedResult (true);
      (*the_expr) = createSizeExpr( expr1, sizeExpr, loc, sizeValue,
                                    extending );
      if (*the_expr == NULL) {
        POPULATE_FAILURE(mMsgContext->JaguarConsistency (&loc, "Unable to set the proper size on sign extended function."), &err_code);
        return err_code;
      }

      if (extending)
      {
        if (!strcasecmp(func_name,"RESIZE"))
        {
          int width1 = 0;
          temp_err_code = getVhExprWidth( static_cast<vhExpr>( vh_arg1 ), &width1,
                                          loc, context );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          {
            return temp_err_code;
          }
          // signed RESIZE() has to be handles separately because size
          // truncation is handled in a different way in this function.
          (*the_expr) = createExprForResizeFunc((*the_expr), width1, loc,
                                                sizeValue);
        }
        if ( !strcasecmp( func_name, "CONV_STD_LOGIC_VECTOR" ))
        {
          (*the_expr)->setSignedResult( false );
          is_unsigned_result = true;
        }
        else
        {
          (*the_expr)->setSignedResult( true );
        }
      }
      else {
        if (!( !strcasecmp( func_name, "CONV_SIGNED" ) ||
               !strcasecmp( func_name, "TO_SIGNED" )))
        {
          // EXT and CONV_STD_LOGIC_VECTOR don't want to sign extend, and
          // needs to be zero-extended to destination width.
          is_unsigned_result = true;
        }
        (*the_expr)->resize (sizeValue);
      }
    } // if (vh_arg2) 
    else
    {
      if (!functionSignExtendsOperand (scope_name, func_name, (vhExpr) vh_arg1))
      {
        is_unsigned_result = true;
      }

      *the_expr = expr1;
    }
  }  
  else if (!strcasecmp(func_name,"SHIFT_LEFT") ||
           !strcasecmp(func_name,"SHL"))
  {
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg1 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg1);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg2 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    *the_expr = new NUBinaryOp(NUOp::eBiVhLshift, arg1, arg2, loc);
  }
  else if (!strcasecmp(func_name,"SHIFT_RIGHT") ||
           !strcasecmp(func_name,"SHR" ))
  {
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    vhNode vh_arg1 = vh_arg;
    NUExpr* arg1 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg1);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg2 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    if ( eVHSIGNED == getVhExprSignType((vhExpr)vh_arg1) )
    {
      *the_expr = new NUBinaryOp(NUOp::eBiVhRshiftArith, arg1, arg2, loc);
    }
    else
    {
      *the_expr = new NUBinaryOp(NUOp::eBiVhRshift, arg1, arg2, loc);
    }
  }
  else if (!strcasecmp(func_name,"ROTATE_LEFT"))
  {
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg1 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg1);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg2 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    *the_expr = new NUBinaryOp(NUOp::eBiRoL, arg1, arg2, loc);
  }
  else if (!strcasecmp(func_name,"ROTATE_RIGHT"))
  {
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg1 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg1);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg2 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    *the_expr = new NUBinaryOp(NUOp::eBiRoR, arg1, arg2, loc);
  }
  else if (!strcasecmp(func_name,"STD_MATCH"))
  {
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg1 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg1);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    vh_arg = vhGetNextItem(vh_arg_iter);
    NUExpr* arg2 = NULL;
    temp_err_code = expr((vhExpr)vh_arg, context, 0, &arg2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    *the_expr = new NUBinaryOp(NUOp::eBiEq, arg1, arg2, loc);
    createBitTemp = true;
  }
  else if ( !strcasecmp( func_name, "IS_X" ))
  {
    // An is_x function call always populates as  false
    *the_expr = NUConst::create ( false, 0u, 1, loc );
  }
  else if ( !strcasecmp( func_name, "RISING_EDGE" ) ||
            !strcasecmp( func_name, "FALLING_EDGE" ))
  {
    // This looks like : RISING_EDGE(clk)/FALLING_EDGE(clk)
    // Is equivalent to: clk'EVENT and clk = '1'/clk'EVENT and clk = '0'
    // Populate the latter so that post population process to
    // always block resynthesis can recognize it.

    // There has to be just one argument, that is the clock argument.
    vhNode vh_arg = vhGetNextItem(vh_arg_iter);
    if ((vh_arg == NULL) || (vhGetNextItem(vh_arg_iter) != NULL))
    {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "The RISING_EDGE/FALLING_EDGE function call can have only 1 argument, which is the clock."), &err_code);
      return err_code;
    }
    
    // Create expression for clk function argument.
    NUExpr* clk_expr = NULL;
    ErrorCode err_code = eSuccess;
    ErrorCode temp_err_code = expr(static_cast<vhExpr>(vh_arg), context, 0, &clk_expr);
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) {
      return temp_err_code;
    }

    // Convert to event expression.
    bool is_rising = (strcasecmp(func_name, "RISING_EDGE") == 0);
    *the_expr = sConvertToEventExpr(clk_expr, is_rising, loc);
    is_func_call = false; // Not a function call anymore.
  }
//   else if ( !strcasecmp( func_name, "RISING_EDGE" ) ||
//             !strcasecmp( func_name, "FALLING_EDGE" ))
//   {
//     // Clock edge functions always populate as true
//     *the_expr = NUConst::create ( false, 1u, 1, loc );
//   }
  else
  {
    // all other cases must be something not handled here
    *the_expr = NULL;
    return eNotApplicable;
  }


  // Finished looking at all of the various accelerated functions and processing
  // their parameters.

  if (*the_expr)
  {
    // If this is an integral conversion, we should treat it as a 32-bit integer value
    if (!strcasecmp (func_name, "TO_INTEGER") || !strcasecmp (func_name, "CONV_INTEGER")) {
      if (!is_unsigned_result)
        (*the_expr)->setSignedResult (true);
      if ((*the_expr)->determineBitSize () < 32)
        // Force a SXT | ZXT here.
        (*the_expr) = buildMask ((*the_expr), 32, !is_unsigned_result);

      (*the_expr)->setSignedResult (true);
    }
    else if ( !strcasecmp (func_name, "SXT") )
    {
      // do a specific check for SXT here because we know that this particular
      // SXT is from the IEEE package.  getVhExprSignType could be changed
      // to recognize this same fact. bug 6629
      (*the_expr)->setSignedResult(true);
    }
    else if ( eVHSIGNED != getVhExprSignType( vh_func ) || is_unsigned_result)
    {
      (*the_expr)->setSignedResult(false);
    }
    else
    {
      (*the_expr)->setSignedResult(true);
    }
  }

  // A function returns an IdentRvalue; we've computed a possibly
  // arbitrary complex expression.  To reconcile that, we are going to
  // create a TEMPORARY variable in the current scope and assign the
  // expression to that and then return a reference to that variable.
  NUExpr* rvalue = *the_expr;
  if (is_func_call && rvalue && ! rvalue->isWholeIdentifier ()) {
    // Find the nearest containing declaration scope to place our
    // function return value
    NUScope* block_scope = context->getDeclarationScope ();
    
    // Create a function result - use the self-determined size of the expression, NOT
    // the (possibly) lhs-influenced width
    UInt32 retSize = rvalue->getBitSize ();
    UInt32 tempSize = std::max (rvalue->determineBitSize (), retSize);
    rvalue->resize (tempSize);
    StringAtom *tempName = block_scope->gensym ("retval", func_name, NULL, false);
    NUNet *temp;
    if ( createBitTemp )
    {
      temp = block_scope->createTempNet( tempName, tempSize, rvalue->isSignedResult(),
                                         loc );
    }
    else
    {
      // We need to enforce creating 1-bit vectors, sometimes
      temp = block_scope->createTempVectorNet( tempName,
                                               ConstantRange( tempSize - 1, 0 ),
                                               rvalue->isSignedResult(), loc );
    }      

    // Initialize the accelerated result variable
    bool isSignedResult = false;
    NULvalue* lval = new NUIdentLvalue (temp, loc);
    if (context->isStmtStackEmpty ()) {
      // Must be doing concurrent signal assignments
      StringAtom* block_name = context->getModule ()->newBlockName (context->getStringCache (), loc);
      NUContAssign* init = context->stmtFactory()->createContAssign (block_name, lval, rvalue, loc);
      context->getModule ()->addContAssign (init);
      isSignedResult = init->getRvalue()->isSignedResult(); // rvalue is owned by assign.
    } else {
      // normal always block or functional assignments
      NUBlockingAssign* init = context->stmtFactory()->createBlockingAssign (lval, rvalue, false, loc);
      context->addStmt (init);
      isSignedResult = init->getRvalue()->isSignedResult(); // rvalue is owned by assign.
    }
    NUExpr *value = createIdentRvalue (temp, loc);
    if (retSize)
      value->resize (retSize);
    value->setSignedResult (isSignedResult);
    (*the_expr) = value;
  }

  return err_code;
}


//! determine if this is a supported accelerated function call from
//! the std.stdio library (currently only endfile is supported)
Populate::ErrorCode 
VhdlPopulate::acceleratedExprForStdioFunctionCall( vhExpr vh_func, 
                                                   LFContext *context, 
                                                   NUExpr **the_expr,
                                                   UInt32 /* lhsWidth */ )
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(vh_func);

  if ( vhGetMaster(vh_func) ) {
    return eNotApplicable;               // all supported functions will have no master
  }

  // make sure this is from the std.stdio package, and is a supported function

  vhExpr func_call = vhGetFuncCall(vh_func);
  vhObjType type = vhGetObjType(func_call);
  switch ( type ){
  case VHSELECTEDNAME:
  {
    // check that the selected name specifies STD.TEXTIO.ENDFILE
    JaguarList netEleList( vhGetExprList( static_cast<vhExpr>( func_call )));

    vhNode eName = vhGetNextItem( netEleList );
    if ( ( vhGetObjType(eName) != VHSIMPLENAME ) || ( 0 != strcasecmp(vhGetSimpleName(static_cast<vhExpr>(eName)),"STD") ) ) {
      return eNotApplicable;
    }
    eName = vhGetNextItem( netEleList );
    if ( ( vhGetObjType(eName) != VHSIMPLENAME ) || ( 0 != strcasecmp(vhGetSimpleName(static_cast<vhExpr>(eName)),"TEXTIO") ) ) {
      return eNotApplicable;
    }
    eName = vhGetNextItem( netEleList );
    if ( ( vhGetObjType(eName) != VHSIMPLENAME ) || ( 0 != strcasecmp(vhGetSimpleName(static_cast<vhExpr>(eName)),"ENDFILE") ) ) {
      return eNotApplicable;
    }
    return sysFunctionCall(vh_func, context, the_expr);
    break;
  }
  case VHSIMPLENAME:
  {
    if ( 0 != strcasecmp(vhGetSimpleName(static_cast<vhExpr>(func_call)),"ENDFILE") ) {
      return eNotApplicable;
    }
    return sysFunctionCall(vh_func, context, the_expr);
    break;
  }
  case VHOPSTRING:
  {
    // this is a function of the form "not"(foo);  currently not supported
    JaguarString opstring(vhGetStringVal(static_cast<vhExpr>(func_call)));;
    POPULATE_FAILURE(mMsgContext->VhdlNamedFctsNotSupported(&loc, opstring), &err_code);
    return err_code;
  }
  default:
    return eNotApplicable;
  }
  return eNotApplicable;
}



//! Create NUExpr corresponding to a function whose body comtains
//! builtin/mapped operator/function or feedthru metacomment directive.
Populate::ErrorCode 
VhdlPopulate::processBuiltInFuncOrMappedOperator( vhExpr vh_func,
                                                  LFContext *context, 
                                                  NUExpr **the_expr,
                                                  UInt32 lhsWidth )
{
  SourceLocator loc = locator(vh_func);
  ErrorCode err_code = eSuccess; 
  if (VHFUNCCALL != vhGetObjType(vh_func))
  {
    return eNotApplicable;
  }
  vhBuiltInFuncOrMappedOperatorType acclFuncType = getBuiltInFuncOrMappedOperatorType(vh_func);
  if ( ( acclFuncType == eVHSYN_NONE ) or
       ( acclFuncType == eVHFLATTEN  )   ){
    // these types are not builtin
    return eNotApplicable;
  }

  JaguarList vh_arg_iter(vhGetAssocList(vh_func));
  // So far we have seen, accelerable functions can have at the most
  // 2 arguments. 
  vhNode vh_arg1 = vhGetNextItem(vh_arg_iter);
  vhNode vh_arg2 = vhGetNextItem(vh_arg_iter);

  NUExpr* expr1 = NULL;
  NUExpr* expr2 = NULL;
  ErrorCode temp_err_code = expr( (vhExpr)vh_arg1, context, 0, &expr1);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  if (NULL != vh_arg2)
  {
    temp_err_code = expr( (vhExpr)vh_arg2, context, 0, &expr2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  NUOp::OpT op = NUOp::eInvalid;
  int exprNaryCount = 2;
  bool isSigned = false;
  bool invert = false;
  switch (acclFuncType)
  {
    case eVHSYN_S_TO_S:
      *the_expr = createSizeExpr( expr1, expr2, loc, 0, true );
      exprNaryCount = 0;
      break;
    case eVHSYN_S_TO_I:
    case eVHSYN_U_TO_I:
      expr1->setSignedResult(true);
      // Fall thru to the next case statement
    case eVHSYN_FEED_THRU:
      *the_expr = expr1;
      delete expr2;
      exprNaryCount = 0;
      break;
    case eVHSYN_S_TO_U:
      expr1->setSignedResult(false);
      *the_expr = expr1;
      delete expr2;
      exprNaryCount = 0;
      break;
    case eVHSYN_S_XT:
    case eVHSYN_RESIZE:  
    {
      int width = 0;
      temp_err_code = getVhExprWidth((vhExpr)vh_arg1, &width, loc, context);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      expr1->resize(width);

      // Create VHEXT operator
      (*the_expr) = createSizeExpr(expr1, expr2, loc, lhsWidth, true);
      if (*the_expr == NULL) {
        return eFailPopulate;
      }
      if (eVHSYN_RESIZE == acclFuncType)
      {
        // signed RESIZE() has to be handles separately because size
        // truncation is handled in a different way in this function.
        (*the_expr) = createExprForResizeFunc((*the_expr), width, loc, lhsWidth);
      }
      (*the_expr)->setSignedResult (true);
      exprNaryCount = 0;
      break;
    }
    case eVHSYN_U_TO_U:
    case eVHSYN_UNS_RESIZE:
      (*the_expr) = expr1;
      // We will resize it if the size value is constant. Otherwise leave
      // it since in our flow it hardly matters if the do the size  extension
      // in a unsigned way.
      if (expr2)
      {
        NUConst* constSize = expr2->castConst();
        UInt32 sizeValue = 0;
        if (constSize)
        {
          constSize->getUL(&sizeValue);
          if (0 >= sizeValue) // The size must be >= 1
          {
            mMsgContext->JaguarConsistency(&loc, 
                                              "Illegal SIZE has been passed.");
            return eFatal;
          }
        }
        if (sizeValue)
        {
          expr1 = createSizeExpr (expr1, expr2, loc, sizeValue, false);
          *the_expr = expr1;
        }
      }
      exprNaryCount = 0;
      break;

    // Binary operators
    case eVHSYN_EQUAL:             
      op = NUOp::eBiEq; 
      break;
    case eVHSYN_NOT_EQUAL:         
      op = NUOp::eBiNeq; 
      break;
    case eVHSYN_AND:               
      op = NUOp::eBiBitAnd; 
      break;
    case eVHSYN_NAND:              
      invert = true;
      op = NUOp::eBiBitAnd; 
      break;
    case eVHSYN_OR:                
      op = NUOp::eBiBitOr; 
      break;
    case eVHSYN_NOR:               
      invert = true;
      op = NUOp::eBiBitOr; 
      break;
    case eVHSYN_XOR:               
      op = NUOp::eBiBitXor; 
      break;
    case eVHSYN_XNOR:              
      invert = true;
      op = NUOp::eBiBitXor; 
      break;
    case eVHSYN_SIGN_STAR:         
    case eVHSYN_SIGN_MULT:         
      op = NUOp::eBiSMult; 
      isSigned = true;
      break;
    case eVHSYN_UNS_MULT:          
    case eVHSYN_UNS_STAR:          
      op = NUOp::eBiUMult; 
      break;
    case eVHSYN_NUMERIC_SIGN_DIV:  
      op = NUOp::eBiSDiv; 
      isSigned = true;
      break;
    case eVHSYN_NUMERIC_UNS_DIV:   
      op = NUOp::eBiUDiv; 
      break;
    case eVHSYN_BINARY_MINUS:      
      op = NUOp::eBiMinus; 
      isSigned = true;
      break;
    case eVHSYN_NUMERIC_BINARY_MINUS:
      op = NUOp::eBiMinus; 
      isSigned = true;
      break;
    case eVHSYN_NUMERIC_BINARY_PLUS:       
      op = NUOp::eBiPlus; 
      isSigned = true;
      break;
    case eVHSYN_BINARY_PLUS:       
      op = NUOp::eBiPlus; 
      break;
    case eVHSYN_UNS_LT:            
      op = NUOp::eBiULt; 
      break;
    case eVHSYN_UNS_GT:            
      op = NUOp::eBiUGtr; 
      break;
    case eVHSYN_GT:                
      op = NUOp::eBiSGtr; 
      break;
    case eVHSYN_LT:                
      op = NUOp::eBiSLt; 
      break;
    case eVHSYN_UNS_GT_EQUAL:      
      op = NUOp::eBiUGtre; 
      break;
    case eVHSYN_UNS_LT_EQUAL:      
      op = NUOp::eBiULte; 
      break;
    case eVHSYN_GT_EQUAL:          
      op = NUOp::eBiSGtre; 
      break;
    case eVHSYN_LT_EQUAL:          
      op = NUOp::eBiSLte; 
      break;
    case eVHSYN_SHL:               
      op = NUOp::eBiVhLshift; 
      break;
    case eVHSYN_SHR:               
      op = NUOp::eBiVhRshift; 
      break;
    case eVHSYN_SHLA:              
      op = NUOp::eBiVhLshiftArith; 
      break;
    case eVHSYN_SHRA:              
      op = NUOp::eBiVhRshiftArith; 
      break;
    case eVHSYN_ROL:               
      op = NUOp::eBiRoL; 
      break;
    case eVHSYN_ROR:               
      op = NUOp::eBiRoR; 
      break;
    case eVHSYN_MOD:               
      op = NUOp::eBiVhdlMod; 
      break;

    // Unary operators
    case eVHSYN_REDUCT_XOR:        
      op = NUOp::eUnRedXor; 
      exprNaryCount = 1; 
      break;
    case eVHSYN_REDUCT_AND:        
      op = NUOp::eUnRedAnd; 
      exprNaryCount = 1; 
      break;
    case eVHSYN_REDUCT_OR:         
      op = NUOp::eUnRedOr; 
      exprNaryCount = 1; 
      break;
    case eVHSYN_ABS:               
      op = NUOp::eUnAbs; 
      exprNaryCount = 1; 
      break;
    case eVHSYN_NUMERIC_UNARY_MINUS: 
      op = NUOp::eUnMinus; 
      exprNaryCount = 1; 
      isSigned = true;
      break;
    case eVHSYN_NOT:               
      op = NUOp::eUnVhdlNot; 
      exprNaryCount = 1; 
      break;

    case eVHSYN_NONE:
    case eVHFLATTEN:
      NU_ASSERT(0, expr1);
      exprNaryCount = 0;
      delete expr1;
      delete expr2;
      return eFailPopulate;
      break;
  }

  LOC_ASSERT( expr1, loc );
  NUExpr* e;
  if ( exprNaryCount == 2 )
  {
    LOC_ASSERT( expr2, loc );
    e = new NUBinaryOp(op, expr1, expr2, loc);
    e->setSignedResult(isSigned);
    if (invert)
      e = Populate::invert (e);

    *the_expr = e;
  }
  else if ( exprNaryCount == 1)
  {
    NUExpr* e = new NUUnaryOp(op, expr1, loc);
    e->setSignedResult(isSigned);
    if (invert)
      e = Populate::invert (e);

    *the_expr = e;
  }

  
  return err_code;
}
