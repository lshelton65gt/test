// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/PrePopInterraCB.h"
#include "localflow/DesignPopulate.h"
#include "util/UtStackPOD.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "util/AtomicCache.h"
#include "util/UtStringArray.h"
#include "util/UtXmlWriter.h"
#include "hdl/HdlVerilogPath.h"
#include "LFContext.h"
#include "util/CbuildMsgContext.h"

static const char* scElabBOMSig = "PrePopInterraCB_ElabBOM";

enum NucleusType {
  eNone, //!< No type assigned.
  eTaskFunction, //!< NUTask (tasks and functions)
  eModule, //!< NUModule
  eNamedDeclScope, //!< NUNamedDeclarationScope
  eContAssign //!< NUContAssign (gate primitive, e.g)
};

static const char* sNucleusTypeStr(NucleusType type)
{
  switch (type)
  {
  case eNone: return "None"; break;
  case eTaskFunction: return "NUTask"; break;
  case eModule: return "NUModule"; break;
  case eNamedDeclScope: return "NUNamedDeclarationScope"; break;
  case eContAssign: return "NUContAssign"; break;
  }
  return NULL;
}

BranchStack::BranchStack()
  : mStack(NULL),
    mSavedStack(NULL)
{
  mStack = new STBStack;
}

BranchStack::~BranchStack()
{
  delete mStack;
  delete mSavedStack;
}

void BranchStack::push(STBranchNode* scope) {
  mStack->push_back(scope);
}

void BranchStack::pop() {
  mStack->pop_back();
}
  
STBranchNode* BranchStack::top() {
  return mStack->back();
}
  
bool BranchStack::empty() {
  return mStack->empty();
}

void BranchStack::saveStackAndBeginAtRoot() {
  INFO_ASSERT(mSavedStack == NULL, "Unexpected multi-level stack save.");
  mSavedStack = mStack;
  mStack = new STBStack;
  // Start at the root node in saved stack.
  mStack->push_back(mSavedStack->front());
}

void BranchStack::restoreStack()
{
  INFO_ASSERT(mSavedStack != NULL, "No stack saved for restore.");
  delete mStack;
  mStack = mSavedStack;
  mSavedStack = NULL;
}

class PrePopInterraCB::Helper
{
  //! Module signature class
  /*!
    A module signature is based on a the signatures of its children.
  */
  class ModuleSignature
  {
  public:
    ModuleSignature() : mRef(0) {}
    ~ModuleSignature() {}

    //! Append a child signature.
    void append(const char* partialSig)
    {
      mSignature.push_back(partialSig);
    }

    //! Compose this module's signature.
    const char* compose(UtString* buf) const
    {
      for (UtStringArray::UnsortedCLoop p = mSignature.loopCUnsorted();
           ! p.atEnd(); ++p)
      {
        if (! buf->empty())
          *buf << "_";
        *buf << *p;
      }
      return buf->c_str();
    }

    //! For hashmap/sets.
    size_t hash() const {
      UtString buf;
      compose(&buf);
      size_t ret = buf.hash();
      ret += mRef;
      return ret;
    }

    //! compare two module signatures.
    bool operator==(const ModuleSignature& other) const
    {
      if (this == &other)
        return true;
      
      const UtStringArray& otherSig = other.mSignature;
      UInt32 mySize = mSignature.size();
      if (mySize != otherSig.size())
        return false;
      for (UInt32 i = 0; i < mySize; ++i)
      {
        if (strcmp(mSignature[i], otherSig[i]) != 0)
          return false;
      }
      return true;
    }

    //! Increment the reference count
    /*!
      This happens when a module is uniquified.
    */
    UInt32 incrRefCnt() 
    {
      ++mRef;
      return mRef;
    }

  private:
    UtStringArray mSignature;
    UInt32 mRef;
  };

  class ElabBranchData
  {
  public:
    CARBONMEM_OVERRIDES
    
    ElabBranchData() : mDeclarationScope(NULL), 
                       mNucleusType(eNone),
                       mSignature(NULL)
    {}
    ~ElabBranchData() {}

    //! Get the current signature
    ModuleSignature* getSignature() { return mSignature; }

    //! Put a signature
    /*!
      Used to replace a current signature, usually.
    */
    void putSignature(ModuleSignature* newSig)
    {
      mSignature = newSig;
    }

    //! Set the uniquename to the StringAtom
    void putUniqueBlockName(const StringAtom* nameBr) {
      mUniqueBlockName.assign(nameBr->str());
    }

    //! Set the uniquename to the constructed name
    void putUniqueBlockName(const UtString& name) {
      mUniqueBlockName.assign(name);
    }
    
    void putDeclarationScope(STBranchNode* branchNode)
    {
      mDeclarationScope = branchNode;
    }

    STBranchNode* getDeclarationScope()
    {
      return mDeclarationScope;
    }

    void putNucleusType(NucleusType type)
    {
      mNucleusType = type;
    }

    NucleusType getNucleusType() const {
      return mNucleusType;
    }
    
    bool isModule() const {
      return mNucleusType == eModule;
    }

    bool isTaskFunction() const {
      return mNucleusType == eTaskFunction;
    }

    const char* getUniqueBlockName() const
    {
      INFO_ASSERT(! mUniqueBlockName.empty(), "Block name of elaborated block was not set.");
      return mUniqueBlockName.c_str();
    }
    
    UtString& getUniqueBlockNameStr()
    {
      return mUniqueBlockName;
    }

    void print() const
    {
      UtIO::cout() << "\tUniqueBlockName: " << getUniqueBlockName() << UtIO::endl;
      UtIO::cout() << "\tDeclarationScope: ";
      if (mDeclarationScope)
      {
        UtIO::cout() << UtIO::hex << mDeclarationScope << UtIO::dec << ": ";
        UtString buf;
        mDeclarationScope->compose(&buf);
        UtIO::cout() << buf;
      }
      else 
        UtIO::cout() << "(null)";
      UtIO::cout() << UtIO::endl;
      
      UtIO::cout() << "\tNucleusType: " << sNucleusTypeStr(mNucleusType) << UtIO::endl;
      
      UtIO::cout() << "\tSignature: ";
      if (mSignature)
      {
        UtString buf;
        UtIO::cout() << mSignature->compose(&buf);
      }
      else 
        UtIO::cout() << "(null)";
      UtIO::cout() << UtIO::endl;
    }

    void writeXml(UtXmlWriter*  xmlWriter) const
    {
      xmlWriter->WriteAttribute("Type", "ElabBranchData*");

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",  "mUniqueBlockName");
      xmlWriter->WriteAttribute("Type",  "UtString");
      xmlWriter->WriteAttribute("Value", mUniqueBlockName.c_str());
      xmlWriter->EndElement();

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mDeclarationScope");
      xmlWriter->WriteAttribute("Type",     "STBranchNode*");
      xmlWriter->WriteAttribute("Pointer",  (const void*) mDeclarationScope);
      if (mDeclarationScope != NULL ) {
        UtString buf;
        mDeclarationScope->compose(&buf);
        xmlWriter->WriteAttribute("Value",buf.c_str());
      }

      xmlWriter->EndElement();

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",  "mNucleusType");
      xmlWriter->WriteAttribute("Type",  "NucleusType");
      xmlWriter->WriteAttribute("Value", sNucleusTypeStr(mNucleusType));
      xmlWriter->EndElement();


      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mSignature");
      xmlWriter->WriteAttribute("Type",     "ModuleSignature*");
      xmlWriter->WriteAttribute("Pointer",  (const void*) mSignature);
      if (mSignature) {
        UtString buf;
        mSignature->compose(&buf);
        xmlWriter->WriteAttribute("Value",buf.c_str());
      }
      xmlWriter->EndElement();
      return;

    }

    
  private:
    UtString mUniqueBlockName;
    STBranchNode* mDeclarationScope;
    NucleusType mNucleusType;
    ModuleSignature* mSignature;
  };
  
  friend class ElabBranchData;

  class ElabBOM : public STFieldBOM
  {
  public:
    CARBONMEM_OVERRIDES

    ElabBOM() {}
    virtual ~ElabBOM() {}

    static ElabBranchData* castBOM(STBranchNode* node) {
      ElabBranchData* ret = static_cast<ElabBranchData*>(node->getBOMData());
      return ret;
    }

    static const ElabBranchData* castBOM(const STBranchNode* node) {
      // gcc 2.95.3 is brain dead. You need to explicitly cast the
      // void* to const void* before casting to ElabBranchData*.
      const void* bomdata = node->getBOMData();
      const ElabBranchData* ret = static_cast<const ElabBranchData*>(bomdata);
      return ret;
    }
    
    virtual void writeBOMSignature(ZostreamDB& out) const
    {
      out << scElabBOMSig;
    }
    
    virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString* errMsg)
    {
      UtString tmpSig;
      in >> tmpSig;
      if (in.fail())
        return eReadFileError;
      
      ReadStatus stat = eReadOK;    
      if (tmpSig.compare(scElabBOMSig) != 0)
      {
        *errMsg << "Signature mismatch - expected '" << scElabBOMSig << "', got '" << tmpSig << "'";
        stat = eReadIncompatible;
      }
      return stat;
    }
    
    virtual Data allocBranchData()
    {
      return new ElabBranchData;
    }
    
    virtual Data allocLeafData()
    {
      return NULL;
    }
    
    virtual void freeBranchData(const STBranchNode*, Data* bomdata)
    {
      if (bomdata)
      {
        ElabBranchData* brData = static_cast<ElabBranchData*>(*bomdata);
        delete brData;
      }
    }
    
    virtual void freeLeafData(const STAliasedLeafNode* leaf, Data* bomdata)
    {
      if (bomdata)
        ST_ASSERT(*bomdata == NULL, leaf);
    }
    
    virtual void preFieldWrite(ZostreamDB&)
    {}
    
    virtual void writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const
    {}
    
    virtual void writeBranchData(const STBranchNode*, ZostreamDB&,
                                 AtomicCache*) const
    {}
    
    virtual ReadStatus preFieldRead(ZistreamDB&)
    {
      return eReadOK;
    }
    
    virtual ReadStatus readLeafData(STAliasedLeafNode*, ZistreamDB&, MsgContext*)
    {
      return eReadIncompatible;
    }
    
    virtual ReadStatus readBranchData(STBranchNode*, ZistreamDB&, MsgContext*)
    {
      return eReadIncompatible;
    }
    
    virtual void printBranch(const STBranchNode* branch) const
    {
      UtString buf;
      branch->compose(&buf);
      UtIO::cout() << "FullName: " << buf << UtIO::endl;
      const ElabBranchData* brData = castBOM(branch);
      brData->print();
    }
    
    virtual void printLeaf(const STAliasedLeafNode* leaf) const
    {
      UtString buf;
      leaf->compose(&buf);
      UtIO::cout() << "FullName: " << buf << UtIO::endl;
    }
    //! Return BOM class name
    virtual const char* getClassName() const
    {
      return "ElabBOM";
    }
    
    //! Write the BOMData for a branch node
    virtual void xmlWriteBranchData(const STBranchNode* branch ,UtXmlWriter* xmlWriter) const
    {
      const ElabBranchData* elabData = ElabBOM::castBOM(branch);
      if(elabData == NULL) {
        return;
      }

      elabData->writeXml(xmlWriter);

    }
    
    //! Write the BOMData for a leaf node
    virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/ ,UtXmlWriter* /*writer*/ ) const
    {
      
    }



  };

  friend class ElabBOM;

public:
  CARBONMEM_OVERRIDES

  Helper(AtomicCache* atomicCache, bool verbose)
    : mVerbose(verbose)
  {
    mDeclarationSymTab = new STSymbolTable(&mEmptyBOM, atomicCache);
    mElabSymTab = new STSymbolTable(&mElabBOM, atomicCache);
  }

  ~Helper() {
    mModuleSignatures.clearPointerValues();
    delete mDeclarationSymTab;
    delete mElabSymTab;
  }


  void print() const
  {
    UtIO::cout() << "UNELABORATED SYMBOLTABLE" << UtIO::endl;
    mDeclarationSymTab->print();
    UtIO::cout() << UtIO::endl << UtIO::endl;
    UtIO::cout() << "ELABORATED SYMBOLTABLE" << UtIO::endl;
    mElabSymTab->print();
  }

  void pushDeclarationScope(StringAtom* unelab)
  {
    STBranchNode* parent = NULL;
    if (! mDeclarationBranches.empty())
      parent = mDeclarationBranches.top();
    
    STBranchNode* curUnelabScope = mDeclarationSymTab->createBranch(unelab, parent);
    mDeclarationBranches.push(curUnelabScope);
    
    if (mElabBranches.empty())
    {
      // The unelab branch stack better have been empty too.
      ST_ASSERT(parent == NULL, parent);
      
      // We are at the top level. Push the unelaborated scope as the
      // root elaborated scope
      STBranchNode* elabScope = mElabSymTab->createBranch(unelab, NULL);
      mElabBranches.push(elabScope);
      ElabBranchData* elabData = ElabBOM::castBOM(elabScope);
      elabData->putUniqueBlockName(unelab);
      elabData->putNucleusType(eModule);
    }
  }

  void popDeclarationScope()
  {
    if (! mDeclarationBranches.empty())
    {
      mDeclarationBranches.pop();
      if (mDeclarationBranches.empty())
      {
        // pop the root elaborated branch
        INFO_ASSERT(! mElabBranches.empty(), "Elaborated branch stack inconsistent with declaration branch stack.");
        mElabBranches.pop();
        ST_ASSERT(mElabBranches.empty(), mElabBranches.top());
      }
    }
  }

  STBranchNode* pushElabScope(StringAtom* scopeName, NucleusType blockType)
  {
    INFO_ASSERT(! mElabBranches.empty(), scopeName->str());
    STBranchNode* parent = mElabBranches.top();
    STBranchNode* elabScope = mElabSymTab->createBranch(scopeName, parent);
    mElabBranches.push(elabScope);

    ElabBranchData* elabData = ElabBOM::castBOM(elabScope);
    // Default the uniquename to the name of the pushed elaborated
    // scope name. If this is a block within a module, this will not
    // change, but if this is a module/entity then this will change to
    // the unique blockname
    elabData->putUniqueBlockName(scopeName);
    // Set the nucleus type of the block that is being instanced.
    elabData->putNucleusType(blockType);
    
    STBranchNode* declScope = mDeclarationBranches.top();
    elabData->putDeclarationScope(declScope);
    return elabScope;
  }

  void popElabScope()
  {
    if (! mElabBranches.empty())
      mElabBranches.pop();
  }
  
  void saveStacksAndBeginAtRoot()
  {
    mDeclarationBranches.saveStackAndBeginAtRoot();
    mElabBranches.saveStackAndBeginAtRoot();
  }

  void restoreStacks()
  {
    mDeclarationBranches.restoreStack();
    mElabBranches.restoreStack();
  }

  //! Gets the declaration scope for the elab'd instance
  STBranchNode* getInstanceDeclarationScope(STBranchNode* instance)
  {
    ElabBranchData* elabBranchData = ElabBOM::castBOM(instance);
    return elabBranchData->getDeclarationScope();
  }

  void putUniqueBlockName(STBranchNode* scope, const UtString& uniqueName)
  {
    ElabBranchData* elabBranchData = ElabBOM::castBOM(scope);
    elabBranchData->putUniqueBlockName(uniqueName);
  }
  
  STBranchNode* getElabScope()
  {
    return mElabBranches.top();
  }

  STBranchNode* getDeclarationScope()
  {
    return mDeclarationBranches.top();
  }

  void uniquify()
  {
    for (STSymbolTable::RootIter rIter = mElabSymTab->getRootIter(); 
         ! rIter.atEnd(); ++rIter)
    {
      STSymbolTableNode* symNode = *rIter;
      STBranchNode* bnode = symNode->castBranch();
      if (bnode)
      {
        ElabBranchData* elabBranchData = ElabBOM::castBOM(bnode);
        if (elabBranchData->isModule())
          calculateUnique(bnode);
      }
    }
  }

  STBranchNode* findElabScope(STBranchNode* parent, StringAtom* instAtom)
  {
    STSymbolTableNode* symNode = mElabSymTab->find(parent, instAtom);
    ST_ASSERT(symNode, parent);
    STBranchNode* scope = symNode->castBranch();
    ST_ASSERT(scope, symNode);
    return scope;
  }

  const char* getUniqueBlockName(const STBranchNode* instance) const
  {
    const ElabBranchData* bnodeData = ElabBOM::castBOM(instance);
    return bnodeData->getUniqueBlockName();
  }

  //! Generate a unique name for the given process based either on it's label
  //! or file/line number if it has no label.
  StringAtom* uniquifyProcess(vhNode concProc, SourceLocator* loc, LFContext* context)
  {
    // Does process already have a unique atom?
    UtMap<vhNode,StringAtom*>::iterator itr = mProcs.find(concProc);
    StringAtom* procName = NULL;
    if (itr == mProcs.end()) {
      // Is process labeled?
      JaguarString blockName(vhGetLabel(concProc));
      if (blockName != NULL) {
        procName = context->vhdlIntern(blockName);
      } else {
        // Construct process name based on source file and line number since
        // it is not labeled. Also attach a unique integer so that multiple
        // processes on the same line don't have same names.
        UtString procStr("$block_"); // prefix with $ to make it illegal verilog.
        UtString path, fileStr;
        OSParseFileName(loc->getFile(), &path, &fileStr);
        size_t dotPlace = fileStr.find(".");
        if (dotPlace != UtString::npos) {
          fileStr.replace(dotPlace, 1, "_");
        }
        procStr << fileStr << "_L" << loc->getLine() << ";" << mProcs.size();
        AtomicCache* atomCache = context->getStringCache();
        procName = atomCache->intern(procStr.c_str(), procStr.size());
      }
      mProcs.insert(UtMap<vhNode,StringAtom*>::value_type(concProc, procName));
    } else {
      procName = itr->second;
    }
    return procName;
  }

  StringAtom* getUniqueVhdlProcessName(vhNode concProc)
  {
    UtMap<vhNode,StringAtom*>::iterator itr = mProcs.find(concProc);
    return itr->second;
  }
  STSymbolTable* getDeclarationsSymTab() 
  {
    return mDeclarationSymTab;
  }

  STSymbolTable* getElaborationsSymTab() 
  {
    return mElabSymTab;
  }

private:
  BranchStack mDeclarationBranches;
  BranchStack mElabBranches;

  STEmptyFieldBOM mEmptyBOM;
  ElabBOM mElabBOM;
  // The unelaborated symboltable. Module names and their parent(s)
  STSymbolTable* mDeclarationSymTab;
  // The elaborated symboltable (instance path names)
  STSymbolTable* mElabSymTab;

  typedef UtHashMap<UtString, ModuleSignature*> StrModSigMap;
  // Map of module/entity names to module signatures
  StrModSigMap mModuleSignatures;
  
  typedef UtHashMap<ModuleSignature*, StringAtom*, HashPointerValue<ModuleSignature*> > ModSigToAtomMap;
  // Map of ModuleSignatures used to create a unique module name. Only
  // signatures that triggered a uniquifying event are put into this map.
  ModSigToAtomMap mDerivedNames;

  bool mVerbose;
  // Map of VHDL processes to their name atoms.
  UtMap<vhNode,StringAtom*> mProcs;

  // Recursive function that traverses to the end of the branch and
  // calculates unique module names as it pops back up.
  void calculateUnique(STBranchNode* bnode)
  {
    STBranchNodeIter bIter(bnode);
    STSymbolTableNode* child;
    SInt32 index;
    
    ModuleSignature* calcSignature = new ModuleSignature;
    
    // calculate children first, so we calculate bottom-up
    while ((child = bIter.next(&index)))
    {
      STBranchNode* childBranch = child->castBranch();
      if (childBranch)
      {
        ElabBranchData* elabBranchData = ElabBOM::castBOM(childBranch);
        // Do not add the tasks/functions to the signature. There
        // could be a ton of those and they don't offer any
        // uniqueness.
        if (! elabBranchData->isTaskFunction())
        {
          // RECURSIVE CALL TO CALCULATEUNIQUE
          calculateUnique(childBranch);
          
          // After calculating the uniq name for the childBranch, the
          // signature for the containing branch can now append the child.
          const char* uniqueModuleName = elabBranchData->getUniqueBlockName();
          calcSignature->append(uniqueModuleName);
        }
      }
    }
    
    // Get the current name and see if we have a non-matching signature
    ElabBranchData* bnodeData = ElabBOM::castBOM(bnode);
    UtString& curUniqName = bnodeData->getUniqueBlockNameStr();
    bnodeData->putSignature(calcSignature);

    ModuleSignature* savedSig = mModuleSignatures.insertInit(curUniqName, calcSignature);
    
    // Check if savedSig and calcSignature are the same value.
    // If savedSig and calcSignature are the same pointer then this
    // was the first occurrence of this module name. So just move
    // on. 
    if (savedSig != calcSignature)
    {
      if (*savedSig == *calcSignature)
      {
        bnodeData->putSignature(savedSig);

        // Delete the calculated signature.
        delete calcSignature;
      }
      else
      {
        // We have a signature that is different than the first
        // occurrence of this module. We may have already uniquified
        // the module with this signature. 
        StringAtom* uniqDeclScopeAtom = NULL;

        ModSigToAtomMap::iterator p = mDerivedNames.find(calcSignature);
        if (p != mDerivedNames.end())
        {
          // We already have uniquified with this signature. So, just
          // use that uniquename.
          uniqDeclScopeAtom = p->second;
          ModuleSignature* sameSig = p->first;
          bnodeData->putSignature(sameSig);

          // delete the redundant signature
          delete calcSignature;
          calcSignature = sameSig;
        }
        else
        {
          // This means we must
          // uniquify the module now, so upscopes can properly be
          // uniquified.
          
          // Up the reference count on the saved signature so we don't
          // duplicate a generated name.
          UInt32 currentCount = savedSig->incrRefCnt();
          
          // Create a declaration scope. Note, we are not filling this
          // scope with the scopes it is instancing. We only need a name
          // here for uniquification purposes. And we want to track what
          // we've created and where.
          UtString uniqDeclScopeName(curUniqName);
          uniqDeclScopeName << "_#" << currentCount;
          AtomicCache* atomCache = mDeclarationSymTab->getAtomicCache();
          STBranchNode* declScope = bnodeData->getDeclarationScope();
          uniqDeclScopeAtom = atomCache->intern(uniqDeclScopeName.c_str(), uniqDeclScopeName.size());
          mDeclarationSymTab->createBranch(uniqDeclScopeAtom, declScope);
          
          ModuleSignature* newSavedSig = mModuleSignatures.insertInit(uniqDeclScopeName, calcSignature);
          // make sure this name is unique.
          ST_ASSERT(newSavedSig == calcSignature, bnode);
        }
        
        if (mVerbose)
        {
          UtString buf;
          bnode->compose(&buf);
          UtIO::cout() << "Uniquifying block " << buf << ": " 
                       << curUniqName << " -> " << uniqDeclScopeAtom->str()
                       << UtIO::endl;
        }
        
        // Set the elaborated path to point to the new declaration
        // scope
        bnodeData->putUniqueBlockName(uniqDeclScopeAtom);
        
        
        const StringAtom* scopeNameChk = mDerivedNames.insertInit(calcSignature, uniqDeclScopeAtom);
        // make sure that we haven't saved this signature before.
        ST_ASSERT(uniqDeclScopeAtom == scopeNameChk, bnode);
        
        // write the mapping to disk
        // TBD
      }
    }
  }
};

PrePopInterraCB::PrePopInterraCB(DesignPopulate* populator, bool verbose)
  : mPopulator(populator)
{
  mLFContext = mPopulator->getLFContext();
  mHelper = new Helper(mLFContext->getStringCache(), verbose);
}

PrePopInterraCB::~PrePopInterraCB()
{
  delete mHelper;
}

void PrePopInterraCB::print() const
{
  mHelper->print();
}

InterraDesignWalkerCB::Status 
PrePopInterraCB::hdlEntity(Phase phase, mvvNode /*node*/,
                           mvvLanguageType langType, 
                           const char* entityName, 
                           bool* isCmodel)
{
  if (phase == ePre)
  {
    IODBNucleus* iodb = mLFContext->getIODB();
    // Have to atomize first because of vhdl caseness
    StringAtom* entityAtom = mLFContext->hdlIntern(entityName, langType);
    *isCmodel = iodb->isCModel(entityAtom->str());
    
    mHelper->pushDeclarationScope(entityAtom);
  }
  else if (phase == ePost)
  {
    mHelper->popDeclarationScope();
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
PrePopInterraCB::blockStmt(Phase phase, mvvNode /*node*/, const char* blockName, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    StringAtom* blockAtom = mLFContext->hdlIntern(blockName, langType);
    mHelper->pushElabScope(blockAtom, eNamedDeclScope);
  }
  else if (phase == ePost)
    mHelper->popElabScope();
  return eNormal;
}

InterraDesignWalkerCB::Status 
PrePopInterraCB::forGenerate(Phase phase, mvvNode node, mvvNode indexVal, mvvLanguageType langType)
{
  Status ret = eNormal;
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhExpr vh_expr = (vhExpr)indexVal;
      StringAtom* forGen = NULL;
      if ( mPopulator->mVhdlPopulate.calcForGenerateBlockName(node->castVhNode(), vh_expr, mLFContext, &forGen) != Populate::eSuccess ) {
        ret = eStop;
      } else {
        mHelper->pushElabScope(forGen, eNamedDeclScope);
      }
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VHDL)
      mHelper->popElabScope();
  }
  return ret;
}

InterraDesignWalkerCB::Status 
PrePopInterraCB::ifGenerateTrue(Phase phase, mvvNode /*node*/, const char* blockLabel, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (blockLabel)
    {
      INFO_ASSERT(langType != MVV_VERILOG, blockLabel);
      StringAtom* ifGen = mLFContext->hdlIntern(blockLabel, langType);
      mHelper->pushElabScope(ifGen, eNamedDeclScope);
    }
  }
  else if (phase == ePost)
  {
    if (blockLabel)
      mHelper->popElabScope();
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
PrePopInterraCB::generateBlock(Phase phase, mvvNode /*node*/, 
                               const char* blockName, 
                               mvvLanguageType langType, 
                               const SInt32* /*genForIndex*/)
{
  if (strlen(blockName) == 0)
    return eNormal;

  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      StringAtom* blockAtom = mLFContext->hdlIntern(blockName, MVV_VERILOG);
      mHelper->pushElabScope(blockAtom, eNamedDeclScope);
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VERILOG)
      mHelper->popElabScope();
  }
  return eNormal;
}

InterraDesignWalkerCB::Status
PrePopInterraCB::unsupportedGenerateItem(mvvNode node, mvvLanguageType langType)
{
  UtString locAndType;
  addLocationAndType(node, langType, &locAndType);
  SourceLocator loc;
  if ( langType == MVV_VHDL ) {
    loc = mPopulator->mVhdlPopulate.locator(node->castVhNode());
  } else {
    loc = mPopulator->mVlogPopulate.locator(node->castVeNode());
  }
  mPopulator->getMsgContext()->UnsupportedConstructInGenerateBlock(&loc, locAndType.c_str());
  return eStop;
}

InterraDesignWalkerCB::Status 
PrePopInterraCB::hdlComponentInstance(Phase phase, mvvNode /*inst*/,
                                      const char* instName,
                                      mvvNode instMaster,
                                      mvvNode /*oldEntity*/,
                                      mvvNode /*newEntity*/,
                                      const char* entityName,
                                      mvvNode arch,
                                      const SInt32* /*vectorIndex*/, 
                                      const UInt32* /*rangeOffset*/,
                                      mvvLanguageType instLang, 
                                      mvvLanguageType scopeLang)
{
  Status ret = eNormal;
  if (phase == ePre)
  {
    StringAtom* instNameAtom = mLFContext->hdlIntern(instName, scopeLang);
    STBranchNode* elabScope = mHelper->pushElabScope(instNameAtom, eModule);
    
    // Now uniquify the module name of the instance
    StringAtom* entityAtom = mLFContext->hdlIntern(entityName, instLang);
    
    UtString uniqModuleName;
    if (instLang == MVV_VERILOG)
    {
      veNode ve_module = instMaster->castVeNode();
      uniqModuleName.assign(entityAtom->str());
      if (mPopulator->mVlogPopulate.analyzeInstanceParameters(ve_module, &uniqModuleName) != Populate::eSuccess)
        ret = eStop;
          
    }
    else if (instLang == MVV_VHDL)
    {
      vhNode vh_entity = instMaster->castVhNode();
      
      uniqModuleName.assign(vhGetLibName(vh_entity));
      uniqModuleName << "_" << entityAtom->str();
      if (arch)
      {
        vhNode vh_arch = arch->castVhNode();
        uniqModuleName << "_" << vhGetArchName(vh_arch);
      }
      if (mPopulator->mVhdlPopulate.uniquifyEntityName(vh_entity, uniqModuleName) != Populate::eSuccess)
        ret = eStop;
    }
    mHelper->putUniqueBlockName(elabScope, uniqModuleName);
  }
  else if (phase == ePost)
    mHelper->popElabScope();
  
  return ret;
}

mvvNode PrePopInterraCB::substituteEntity(mvvNode entityDecl, mvvLanguageType entityLanguage, const UtString& entityDeclName, mvvNode instance, bool isUDP, mvvLanguageType callingLanguage)
{
  const STBranchNode* parentEntity = mHelper->getDeclarationScope();
  NUPortMap* portMap = NULL;
  bool found = false;
  UtString new_mod_name;
  UtString instanceName;
  Populate::composeInstanceName(instance, mLFContext, &instanceName, mPopulator->mVlogPopulate.getTicProtectedNameManager()); 

  mvvNode node = getSubstituteEntity(parentEntity->str(),
				     entityDecl,
				     entityLanguage,
				     entityDeclName,
				     instanceName,
				     isUDP,
				     callingLanguage,
				     mPopulator->mVlogPopulate.getTicProtectedNameManager(),
				     mLFContext->getIODB(),
				     &new_mod_name,
				     &found,
				     &portMap);

  if (node == NULL && found) {
    mLFContext->getMsgContext()->SubstituteModuleNotFound(new_mod_name.c_str());
  }

  return node;
}

InterraDesignWalkerCB::Status
PrePopInterraCB::vlogForeignAttribute(mvvNode module, const UtString& entityDeclName, mvvNode* ent, mvvNode* arch) 
{
  InterraDesignWalkerCB::Status status = getEntArchPairFromForeignAttr(module, ent, arch);

  if (status == InterraDesignWalkerCB::eStop) {
    SourceLocator loc = mPopulator->mVlogPopulate.locator(module->castVeNode());
    mLFContext->getMsgContext()->EntityOrArchitectureSpecifiedInForeignAttributeNotFound(&loc, entityDeclName.c_str());
  }
  else if (status == InterraDesignWalkerCB::eInvalid) {
    SourceLocator loc = mPopulator->mVlogPopulate.locator(module->castVeNode());
    mLFContext->getMsgContext()->IncorrectForeignAttribute(&loc, entityDeclName.c_str());
    status = InterraDesignWalkerCB::eStop;
  }

  return status;
}

InterraDesignWalkerCB::Status
PrePopInterraCB::componentElabFail(mvvNode, mvvLanguageType, int)
{
  // FIXME
  // Hmmm...NucleusInterraCB returns eNormal for this. Need to find a
  // way to share that code.
  return eNormal;
}

InterraDesignWalkerCB::Status 
PrePopInterraCB::taskFunction(Phase phase, mvvNode /*node*/, 
                              const char* tfName, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    StringAtom* instNameAtom = mLFContext->hdlIntern(tfName, langType);
    mHelper->pushElabScope(instNameAtom, eTaskFunction);
  }
  if (phase == ePost)
    mHelper->popElabScope();

  return eNormal;
}

InterraDesignWalkerCB::Status
PrePopInterraCB::concProcess(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (langType == MVV_VHDL)
  {
    if (phase == ePre)
    {
      vhNode concProc = node->castVhNode();
      SourceLocator loc = mPopulator->mVhdlPopulate.locator(concProc);
      StringAtom* procNameAtom = mHelper->uniquifyProcess(concProc, &loc, mLFContext);
      // Process name is a declaration scope. Add it to both elaborated and
      // unelaborated symbol tables.
      mHelper->pushDeclarationScope(procNameAtom);
      mHelper->pushElabScope(procNameAtom, eNamedDeclScope);
    }
    else
    {
      mHelper->popElabScope();
      mHelper->popDeclarationScope();
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status
PrePopInterraCB::packageDecl(Phase phase, mvvNode, mvvLanguageType langType,
                             const char* libName, const char* packName, bool isStdLib)
{
  if (langType == MVV_VHDL)
  {
    if (isStdLib) {
      return eSkip; // Don't need to pre-populate standard libraries.
    }

    if (phase == ePre)
    {
      // The parent for package is root module. Save the current stacks and
      // begin at root. NOTE: Nested packages are not legal in VHDL.
      mHelper->saveStacksAndBeginAtRoot();
      // Push the library declaration/elaborated scope first. The library
      // name for declaration and elaborated scope are the same.
      UtString actualLibName;
      VhdlPopulate::getActualVhdlLibName(libName, actualLibName);
      StringAtom* libAtom = mLFContext->vhdlIntern(actualLibName.c_str());
      mHelper->pushDeclarationScope(libAtom);
      mHelper->pushElabScope(libAtom, eNamedDeclScope);
      // Push the package declaration/elaborated scope first. The package
      // name for declaration and elaborated scope are the same.
      StringAtom* packAtom = mLFContext->vhdlIntern(packName);
      mHelper->pushDeclarationScope(packAtom);
      mHelper->pushElabScope(packAtom, eNamedDeclScope);
    }
    else if (phase == ePost)
    {
      // Pop elaborated and declaration scopes for library and package.
      mHelper->popElabScope();
      mHelper->popDeclarationScope();
      mHelper->popElabScope();
      mHelper->popDeclarationScope();
      // Restore saved stacks and continue where we left off at the beginning of
      // package declaration.
      mHelper->restoreStacks();
    }
  }
  return eNormal;
}

void PrePopInterraCB::uniquify()
{
  mHelper->uniquify();
}

STBranchNode* 
PrePopInterraCB::findElabScope(STBranchNode* parent, const char* instName, 
                               mvvLanguageType langType)
{
  StringAtom* instAtom = mLFContext->hdlIntern(instName, langType);
  return mHelper->findElabScope(parent, instAtom);
}

STBranchNode* 
PrePopInterraCB::findElabScope(STBranchNode* parent, StringAtom* instAtom)
{
  return mHelper->findElabScope(parent, instAtom);
}

STBranchNode* 
PrePopInterraCB::getDeclarationScope(STBranchNode* instance)
{
  return mHelper->getInstanceDeclarationScope(instance);
}

const char* 
PrePopInterraCB::getUniqueBlockName(const STBranchNode* scope) const
{
  return mHelper->getUniqueBlockName(scope);
}

StringAtom* PrePopInterraCB::getUniqueVhdlProcessName(vhNode concProc) const
{
  return mHelper->getUniqueVhdlProcessName(concProc);
}

STSymbolTable*
PrePopInterraCB::getDeclarationsSymTab()
{
  return mHelper->getDeclarationsSymTab();
}

STSymbolTable*
PrePopInterraCB::getElaborationsSymTab()
{
  return mHelper->getElaborationsSymTab();
}

// END FILE

