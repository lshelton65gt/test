// -*-C++-*-
/******************************************************************************
 Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/PplLocalConstProp.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "iodb/IODBNucleus.h"
#include "iodb/IODBDesignData.h"
#include "reduce/Fold.h"
#include "reduce/LocalPropagationBase.h"
#include "localflow/UD.h"

class StmtDefCallback : public NUDesignCallback
{
public:

  StmtDefCallback(NUNetSet* defSet, VhdlPopulate::EntityAliasMap* alias_map)
    : mDefSet(defSet),
      mAliasMap(alias_map)
  {
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase* ) { return eNormal; }

  Status operator() (Phase phase, NUIdentLvalue* lval)
  {
    if (phase == ePre) {
      NUNet* ident = lval->getIdent();
      if (ident != NULL) {
        mDefSet->insert(ident);
        checkVhdlAlias(ident);
      }
    }
    return eNormal;
  }


  // This function gets the actual net for an alias expression
  NUNet* getActualNet(const NUExpr* expr)
  {
    NUNet* ident = NULL;
    NUExpr::Type typ = expr->getType();
    switch (typ)
    {
      case NUExpr::eNUIdentRvalue:
      {
        const NUIdentRvalue* identExpr = dynamic_cast<const NUIdentRvalue*>(expr);
        ident = identExpr->getIdent();
        break;
      }
      case NUExpr::eNUVarselRvalue:
      {
        const NUVarselRvalue* varselExpr = dynamic_cast<const NUVarselRvalue*>(expr);
        const NUExpr* identExpr = varselExpr->getIdentExpr();
        ident = getActualNet(identExpr);
        break;
      }
      case NUExpr::eNUMemselRvalue:
      {
        const NUMemselRvalue* memselExpr = dynamic_cast<const NUMemselRvalue*>(expr);
        ident = memselExpr->getIdent();
        break;
      }
      default:
        break;
    }
    
    return ident;
  }

  // If the net is an alias net, then this function gets the actual expr for it.
  NUExpr* isVhdlAlias(NUNet* net, NUModule* module)
  {
    NUExpr* actual_expr = NULL;
    VhdlPopulate::EntityAliasMap::iterator mapItr = mAliasMap->find(module);
    if (mapItr != mAliasMap->end())
    {
      NUExprReplacementMap& replMap = mapItr->second;
      if (!replMap.empty())
      {
        for (NUExprReplacementMap::iterator itr = replMap.begin();
             itr != replMap.end(); ++itr)
        {
          NUNet* aliasNet = const_cast<NUNet*>(itr->first);
          if(net == aliasNet)
          {
            actual_expr = itr->second;
            break;
          }
        }
      }
    }
    return actual_expr;

    
  }

private:  
  
  void checkVhdlAlias(NUNet* net)
  {
    NUExpr* actual_expr = NULL;
    NUNet*  actual_net = NULL;
    NUModule* module = net->getScope()->getModule();
    do 
    {
      // if the net is alias, find all the aliases and 
      // add them to the defs.
      actual_expr = isVhdlAlias(net, module);
      if(actual_expr)
      {
        actual_net = getActualNet(actual_expr);
        if(actual_net)
        {
          mDefSet->insert(actual_net);
          net = actual_net;
        }
      }

    } while((actual_expr != NULL)  && (actual_net != NULL));
  }

  NUNetSet* mDefSet;
  VhdlPopulate::EntityAliasMap* mAliasMap;   // Map of vhdl Aliases
};

PplLocalConstProp::PplLocalConstProp(Fold* fold, IODBNucleus* iodb, 
                                     VhdlPopulate::EntityAliasMap* alias_map)
  : mFold(fold),
    mIODB(iodb),
    mStats(NULL),
    mLPBase(NULL),
    mAliasMap(alias_map)
{
  mStats = new LocalPropagationStatistics();
  //! Population never exits cleanly on an error. Avoid asserting if constant
  //! propagation vectors are not empty.
  mLPBase = new LocalPropagationBase(mFold, mStats, false, true);
}

PplLocalConstProp::~PplLocalConstProp()
{
  delete mStats;
  delete mLPBase;
}

void PplLocalConstProp::next(PplLocalConstProp::PhaseT phase, NUBase* block,
                             StmtBlockT blockType)
{
  switch (blockType)
  {
  case eBlock:
  {
    // Do nothing since a block always gets executed.
    break;
  }
  case eIfThen:
  {
    if (phase == eBegin) {
      // Start a new nesting context for the if so we can gather the
      // then and else assignments and apply them to parent context.
      pushNesting();
    }
    // Walk in/out of then context depending on phase.
    pushOrPop(phase);
    break;
  }
  case eIfElse:
  {
    // Walk in/out of else context depending on phase.
    pushOrPop(phase);
    if (phase == eEnd) {
      // Apply the nested constant nets.
      popNesting();
    }
    break;
  }
  case eCase:
  {
    // Walk all the case items separately, each with its own set of
    // constant nets. If this is not a full case, we create an empty
    // context to indicate that case.
    if (phase == eBegin) {
      // Start a new nesting context for the case so we can gather the
      // case item assignments and apply them to the parent context
      pushNesting();
    } else if (phase == eEnd) {
      // If this is not a full case, do a push and pop context to
      // indicate that the nets are not defined in some case item branch.
      NUCase* caseStmt = dynamic_cast<NUCase*>(block);
      if (!caseStmt->isFullySpecified() && !caseStmt->isUserFullCase() &&
          !caseStmt->hasDefault())
      {
        pushContext();
        popContext();
      }
      popNesting();
    }
    break;
  }
  case eCaseItem:
  case eForInitial:
  case eForBody:
  case eForAdvance:
  case eTask:
  case eStrucProc:
  {
    // Walk in/out of context depending on phase.
    pushOrPop(phase);
    break;
  }
  case eNotBlock:
  {
    NU_ASSERT(0, block);
    break;
  }
  }
}

void PplLocalConstProp::next(NUStmt* stmt, NUNetSet* forStmtDefs, NUScope* declScope)
{
  // Kill the defs for loops and non-block statements ..block statements are if,
  // for and case, right away.
  NUType stmtType = stmt->getType();
  switch (stmtType)
  {
  case eNUFor:
  {
    // Anything that's def'ed in the initial,body, advance of the loop
    // should not propagate out of the loop.
    applyNonPropagatedDefs(*forStmtDefs);
    break;
  }
  case eNUIf:
  case eNUCase:
  case eNUBlock:
    // Avoid block statements. Their defs are collected on the fly.
    break;
  default:
  {
    applyNonPropagatedDefs(stmt);
    if (stmtType == eNUBlockingAssign) {
      NUBlockingAssign* ba = dynamic_cast<NUBlockingAssign*>(stmt);
      // Enroll the LHS for constant propagation if RHS is a constant.
      enrollAssign(ba, declScope);
      enrollVhdlAliasAssign(ba, declScope);
    }
    break;
  }
  } // switch
}

void PplLocalConstProp::applyNonPropagatedDefs(NUStmt* stmt)
{
  NUNetSet defs;
  {
    StmtDefCallback sdc(&defs, mAliasMap);
    NUDesignWalker walker(sdc, false);
    // Allow walking into tasks, since tasks can def nets outside it's scope.
    walker.putWalkTasksFromTaskEnables(true);
    walker.stmt(stmt);
  }
  applyNonPropagatedDefs(defs);
}

void PplLocalConstProp::pushOrPop(PplLocalConstProp::PhaseT phase)
{
  if (phase == eBegin) {
    pushContext();
  } else if (phase == eEnd) {
    popContext();
  }
}

NUExpr* PplLocalConstProp::optimizeExpr(NUExpr* expr)
{
  NUNetSet replacedNets;
  NUExpr* newExpr = mLPBase->optimizeExpr(expr, NULL, &replacedNets);
  mIODB->addPopulationOptimizedNets(replacedNets);
  return newExpr;
}


void PplLocalConstProp::propagateConst(NUNet* net, NUBlockingAssign* assign, NUScope* declScope)
{
  // Avoid propagating constants from pull assigns if there are other drivers.
  // Also avoid propagation non local variable constants. Only variables declared
  // in current declaration scope are propagated in local constant propagation.
  const IODBDesignDataSymTabs* ddst = mIODB->getDesignDataSymbolTables();
  if ( (net->getScope() == declScope) and
           !ddst->isUnelabProtMutable(net) and
           ((assign->getStrength() >= eStrDrive) or (not net->isMultiplyDriven())) )
  {
    NUConst* constExpr = getConstantExpr(assign);
    if (constExpr)
    {
      constExpr = checkCopyConstantExpr(net, constExpr);
      if (constExpr) 
      {
        mLPBase->addNet(net, constExpr);
        delete constExpr;
      }
    }
  }
}

void PplLocalConstProp::enrollVhdlAliasAssign(NUBlockingAssign* assign, NUScope* declScope) 
{
  NULvalue* lvalue = assign->getLvalue();
  if(lvalue->getType() != eNUIdentLvalue)
    return;

  NUNet* net = lvalue->getWholeIdentifier();

  NUNetSet defs;
  NUModule* module = net->getScope()->getModule();
  StmtDefCallback sdc(&defs, mAliasMap);

  NUNet* actual_net = NULL;
  NUExpr* actual_expr = NULL;

  do 
  {
    // if the net is alias, find all the actuals and 
    // add propagate constant for them.
    actual_expr = sdc.isVhdlAlias(net, module);
    if((actual_expr != NULL) && actual_expr->isWholeIdentifier())
    {
      actual_net = sdc.getActualNet(actual_expr);;
      if(actual_net)
      {
        net = actual_net;
        propagateConst(net, assign, declScope);
      }
    }
  } while((actual_expr != NULL) && (actual_expr->isWholeIdentifier() == true) && (actual_net != NULL));
}


void PplLocalConstProp::enrollAssign(NUBlockingAssign* assign, NUScope* declScope)
{
  NULvalue* lvalue = assign->getLvalue();
  if (lvalue->isWholeIdentifier())
  {
    NUNet* net = lvalue->getWholeIdentifier();
    propagateConst(net, assign, declScope);
  } // if
}

NUConst* PplLocalConstProp::getConstantExpr(NUAssign* assign)
{
  NUExpr* expr = assign->getRvalue();
  NUConst* constExpr = expr->castConst();
  // See if the rhs is a constant as. If so, return it
  if (!constExpr)
  {
    // Try folding to see if this is a constant expression.
    CopyContext cc(0, 0);
    NUExpr* copyExpr = expr->copy(cc);
    NUExpr* newExpr = mFold->fold(copyExpr);
    constExpr = newExpr->castConst();
    // If we folded it into a constant we can also fixup the assign
    // otherwise we give up and return NULL.
    if (constExpr) {
      assign->replaceRvalue(constExpr);
      delete expr;
    } else {
      delete newExpr;
    }
  }
  return constExpr;
}

NUConst* PplLocalConstProp::checkCopyConstantExpr(NUNet* net, NUConst* constExpr)
{
  NUConst* retExpr = NULL;
  // Avoid propagating memory constants, XZs ..since we're not sure about
  // it's semantics.
  if (!constExpr->hasXZ() && !net->is2DAnything())
  {
    // Make a copy of the expression.
    if (net->isIntegerSubrange()) {
      retExpr = constExpr->rebuild(net->isSignedSubrange(), 32);
    } else {
      retExpr = constExpr->rebuild(net->isSigned(), net->getBitSize());
    }
  }
  return retExpr;
}

void PplLocalConstProp::pushContext()
{
  mLPBase->pushContext();
}

void PplLocalConstProp::popContext()
{
  mLPBase->popContext();
}

void PplLocalConstProp::pushNesting()
{
  mLPBase->pushNesting();
}

void PplLocalConstProp::popNesting()
{
  mLPBase->popNesting(true);
}

void PplLocalConstProp::addNet(NUNet* net, NUExpr* constExpr)
{
  mLPBase->addNet(net, constExpr);
}

void PplLocalConstProp::applyNonPropagatedDefs(NUNetSet& defs)
{
  mLPBase->applyNonPropagatedDefs(defs);
}
