/***************************************************************************************
  Copyright (c) 2002-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef PORTCOERCION_H_
#define PORTCOERCION_H_

#include "nucleus/NUNetElabRef.h"
#include "util/SetClosure.h"


/*!
  \file
  Declaration of port coercion package.
*/


class AtomicCache;
class IODBNucleus;
class Stats;
class ArgProc;
class MsgContext;
class SourceLocatorFactory;

class FLNodeFactory;
class FLNodeElabFactory;

class UnelabFlow;
class ReachableAliases;

class CbuildSymTabBOM;
class STSymbolTable;

class PortAnalysis;

//! Closure class; performs the set closure operation over sets of NUNetElabRefSets.
/*!
 * Port coercion uses closure to determine distinct, connected
 * sub-graphs of an elaborated port-connection graph.
 */
typedef SetClosure< NUNetElabRefHdl, NUNetElabRefSet, HashValue<NUNetElabRefHdl> > NUNetElabRefClosure;


//! Container object containing two NUNetElabRef; used to model fanin/fanout relations.
typedef std::pair<NUNetElabRefHdl,NUNetElabRefHdl> DoubleNetElabRef;


struct DoubleNetElabRefHashValue
{
  //! hash operator -- requires class method hash()
  size_t hash(const DoubleNetElabRef& var) const {
    const NUNetElabRefHdl & a = var.first;
    const NUNetElabRefHdl & b = var.second;
    return a.hash() + b.hash();
  }

  //! equal function
  bool equal(const DoubleNetElabRef& v1, const DoubleNetElabRef& v2) const {
    const NUNetElabRefHdl & a1 = v1.first;
    const NUNetElabRefHdl & b1 = v1.second;
    const NUNetElabRefHdl & a2 = v2.first;
    const NUNetElabRefHdl & b2 = v2.second;
    return (a1==a2) && (b1==b2);
  }

  //! shallow less-than function -- for Microsoft hash tables
  bool lessThan1(const DoubleNetElabRef& v1, const DoubleNetElabRef& v2) const {
    return lessThan(v1,v2);
  }

  //! lessThan operator -- for sorted iterations
  bool lessThan(const DoubleNetElabRef& v1, const DoubleNetElabRef& v2) const {
    const NUNetElabRefHdl & a1 = v1.first;
    const NUNetElabRefHdl & a2 = v2.first;
    if (a1 < a2) {
      return true;
    } else if (a1==a2) {
      const NUNetElabRefHdl & b1 = v1.second;
      const NUNetElabRefHdl & b2 = v2.second;
      return b1 < b2;
    } else {
      // a1 > a2
      return false;
    }
  }

};

//! Set of NUNetElabRef pairs.
/*!
 * Port coercion uses this container as poor-man's graph. The nice
 * benefit of using a set is that we can easily perform common set
 * operations (union, difference, etc.) over the graph.
 */
typedef UtHashSet<DoubleNetElabRef,DoubleNetElabRefHashValue> DoubleNetElabRefSet;


//! Stores read/write data about some hierarchical point.
/*!
 * ReadWriteData objects are usually associated with either NUNetElabRef
 * or NUNet*. They model the read/write characteristics of that point
 * in three ways:
 *
 * 1. Above Read/Write
 * 2. Local Read/Write
 * 3. Below Read/Write
 *
 * These six characteristics can be used to determine if a point is
 * used as input, output, or inout.
 */
class ReadWriteData
{
public:
  //! Constructor
  /*!
   * The local read/write characteristics can be determined when
   * analyzing a net in isolation. For that reason, we do not allow
   * default values for the local attributes. 
   */
  ReadWriteData(bool local_reader,
                bool local_writer,
                bool above_reader=false,
                bool above_writer=false,
                bool below_reader=false,
                bool below_writer=false) :
    mLocalReader(local_reader),
    mLocalWriter(local_writer),
    mAboveReader(above_reader),
    mAboveWriter(above_writer),
    mBelowReader(below_reader),
    mBelowWriter(below_writer)
  {}
  
  //! Destructor
  ReadWriteData() {}

  //! Are there any readers?
  bool hasReader() const { return mAboveReader or mLocalReader or mBelowReader; }

  //! Are there any writers?
  bool hasWriter() const { return mAboveWriter or mLocalWriter or mBelowWriter; }

  //! Is there a local reader?
  bool hasLocalReader() const { return mLocalReader; }

  //! Set that there is a local reader.
  void setLocalReader(bool reader) { mLocalReader = reader; }

  //! Is there a local writer?
  bool hasLocalWriter() const { return mLocalWriter; }

  //! Set that there is a local writer.
  void setLocalWriter(bool writer) { mLocalWriter = writer; }

  //! Is there a reader above?
  bool hasAboveReader() const { return mAboveReader; }

  //! Set that there is a reader above.
  void setAboveReader(bool reader) { mAboveReader = reader; }

  //! Is there a writer above?
  bool hasAboveWriter() const { return mAboveWriter; }

  //! Set that there is a writer above.
  void setAboveWriter(bool writer) { mAboveWriter = writer; }

  //! Is there a reader below?
  bool hasBelowReader() const { return mBelowReader; }

  //! Is there a writer below?
  bool hasBelowWriter() const { return mBelowWriter; }

  //! Is there a reader here or lower in the hierarchy?
  bool coversReader() const { return mLocalReader or mBelowReader; }

  //! Is there a writer here or lower in the hierarchy?
  bool coversWriter() const { return mLocalWriter or mBelowWriter; }

  //! Does this r/w data characterize an input?
  /*!
   * Note: Inputs with local writers have to be considered inout.
   * Codegen makes all inputs const; writers would cause g++ errors.
   *
   * \sa isInout
   */
  bool isInput() const {
    // KEEP THIS IN SYNC WITH ::isInout()!
    bool is_input = false;

    // If there are no writers, consider it an input, even if there
    // are no readers.
    if (not hasWriter()) {
      //      is_input = true;
      // At one time if there were no writers we would say this was an input,
      // even if there where no readers, but this caused unnecessary coercion.
    } else {    
      // If all writers are above us, this is a true input.

      // Otherwise, all inputs with writers become inouts -- even if
      // there is no external reader. This is because codegen makes
      // all inputs const.
      is_input = ((not coversWriter()));
    }

    return is_input;
  }

  //! Does this r/w data characterize an output?
  bool isOutput() const {
    return (hasAboveReader() and 
            // Someone is writing.
            coversWriter());
  }

  //! Does this r/w data characterize an internal inout?
  /*!
   * DO NOT USE THIS METHOD FOR PRIMARY PORTS.
   *
   * \sa isPrimaryInout
   *
   * Note: Inputs with local writers have to be considered inout.
   * Codegen makes all inputs const; writers would cause g++ errors.
   * 
   * \sa isInput
   */
  bool isInout() const {
    // KEEP THIS IN SYNC WITH ::isInput()!
    bool input_treated_as_inout = (hasAboveWriter() and
                                   coversReader() and
                                   coversWriter());
    return (input_treated_as_inout or
	    (isInput() and isOutput())); // as of 05/19/09 it is impossible for (isInput() and isOutput()) to be true
  }

  //! Does this r/w data characterize a primary inout?
  /*!
   * DO NOT USE THIS METHOD FOR INTERNAL PORTS.
   *
   * Primary ports need to consider the external read/write
   * contributions.
   *
   * \sa isInout
   */
  bool isPrimaryInout() const {
    return ( (isInout()) or
             (isInput()  and hasAboveReader()) or
             (isOutput() and hasAboveWriter()) );
  }

  //! Augment read/write data with information from a higher level
  /*!
   * This method is used when augmenting the read/write information of
   * some point with the read/write information from some connected
   * point at a higher level in the hierarchy.
   *
   * A new ReadWriteData object is created; only the above attributes
   * may differ from the current object.
   */
  ReadWriteData addFromAbove(const ReadWriteData & above) const {
    return ReadWriteData(mLocalReader,
                         mLocalWriter,
                         mAboveReader or above.mAboveReader or above.mLocalReader,
                         mAboveWriter or above.mAboveWriter or above.mLocalWriter,
                         mBelowReader,
                         mBelowWriter);

  }

  //! Augment read/write data with information from a lower level
  /*!
   * This method is used when augmenting the read/write information of
   * some point with the read/write information from some connected
   * point at a lower level in the hierarchy.
   *
   * A new ReadWriteData object is created; only the below attributes
   * may differ from the current object.
   */
  ReadWriteData addFromBelow(const ReadWriteData & below) const {
    return ReadWriteData(mLocalReader,
                         mLocalWriter,
                         mAboveReader,
                         mAboveWriter,
                         mBelowReader or below.mBelowReader or below.mLocalReader,
                         mBelowWriter or below.mBelowWriter or below.mLocalWriter);
  }

  //! EQ comparison operator
  bool operator==(const ReadWriteData & other) const {
    return (mLocalReader == other.mLocalReader and
            mLocalWriter == other.mLocalWriter and
            mAboveReader == other.mAboveReader and
            mAboveWriter == other.mAboveWriter and
            mBelowReader == other.mBelowReader and
            mBelowWriter == other.mBelowWriter);
  }

  //! NEQ comparison operator
  bool operator!=(const ReadWriteData & other) const {
    return (not ((*this)==other));
  }

  //! += operator.
  ReadWriteData operator+=(const ReadWriteData & other) {
    mLocalReader |= other.mLocalReader;
    mLocalWriter |= other.mLocalWriter;
    mAboveReader |= other.mAboveReader;
    mAboveWriter |= other.mAboveWriter;
    mBelowReader |= other.mBelowReader;
    mBelowWriter |= other.mBelowWriter;

    return (*this);
  }

  //! Print debugging information about this read/write structure.
  void print(int indent = 2) const;

private:
  //! Is there a local reader?
  bool mLocalReader;

  //! Is there a local writer?
  bool mLocalWriter;

  //! Is there a reader above?
  bool mAboveReader;

  //! Is there a writer above?
  bool mAboveWriter;

  //! Is there a reader below?
  bool mBelowReader;

  //! Is there a writer below?
  bool mBelowWriter;
};


//! Store read/write data by NUNetElab.
typedef UtHashMap<NUNetElabRefHdl,
                  ReadWriteData,
                  HashValue<NUNetElabRefHdl>
> NUNetElabRefReadWriteData;

//! Store read/write data by NUNet.
typedef UtMap<NUNet*,ReadWriteData> NUNetReadWriteData;


//! Analyze a design and infer port direction based on dataflow.
/*!
 *
 * I. Purpose
 *
 * Perform a design walk to determine net connectivity through
 * hierarchy. Use this connectivity information to analyze the
 * read/write characteristics of each connected net. Propagate the
 * necessary information to other connected relations. Use the
 * resulting read/write requirements to determine the true port
 * direction; redeclare ports as necessary.
 *
 * II. Connectivity Analysis
 *
 * The design is traversed in an elaborated manner
 * (multiply-instantiated modules are processed once for each
 * instantiation). During this traversal, two pieces of information
 * are generated:
 *
 * 1. Which nets are connected to which other nets?
 *
 *    If net 'top.a' is connected to net 'top.mid.b' through a port
 *    connection and net 'top.mid.b' is connected to net
 *    'top.mid.bot.c', two sets modelling these relations are
 *    generated:
 *
 * \verbatim
 *    { ['top.a', 'top.mid.b'], ['top.mid.b', 'top.mid.bot.c'] }
 * \endverbatim
 *
 *    Set closure is performed over these connection-sets. Set closure
 *    provides sets of all inter-connected nets; connected
 *    sub-components of the graph defined by all port connections.
 *
 *    Throughout this documentation and the code, we refer to these
 *    sets defined by set closure as 'clusters' and refer to their
 *    elements as either 'points' or 'elements'.
 *
 *    Hierarchical references are handled through elaboration of all
 *    design declarations. Upon elaboration, each hierarchical
 *    reference becomes aliased to its resolution.
 *
 * 2. What is the hierarchical fanin/fanout relationship?
 *
 *    Using a port connection as an arc, create a graph where all
 *    formals are fanin of all connected actuals.
 *
 * Example:
 *
 * \verbatim
 *    module top(a, b);
 *      inout a;
 *      inout b;
 *      middle m1 (a, b);
 *      middle m2 (b, a);
 *    endmodule
 *    module middle(c, d);
 *      inout c;
 *      inout d;
 *    endmodule 
 * \endverbatim
 *
 * This design would produce connectivity sets:
 *
 * \verbatim
 *    { ['top.a', 'top.m1.c'], ['top.b', 'top.m1.d'],
 *      ['top.b', 'top.m2.c'], ['top.a', 'top.m2.d'] }
 * \endverbatim
 *
 * Set closure creates the 'clusters':
 *
 * \verbatim
 *    { ['top.a', 'top.m1.c', 'top.m2.d'],
 *      ['top.b', 'top.m1.d', 'top.m2.c'] }
 * \endverbatim
 *
 * The generated hierarchical fanin relations are:
 *
 * \verbatim
 *    { 'top.a' => 'top.m1.c', 
 *      'top.b' => 'top.m1.d',
 *      'top.b' => 'top.m2.c', 
 *      'top.a' => 'top.m2.d' }
 * \endverbatim
 *
 * III. Propagation
 *
 * The propagation algorithm assumes that we know two pieces of
 * information about each elaborated net:
 *
 * 1. Is this net locally written? 
 *
 *    Writing includes continuous drivers, undriven flags, pulls,
 *    tri0/1, supply0/1 and externally driven (depositable/hier-write)
 *    attributes.
 *
 * 2. Is this net locally read?
 *
 * With this local information, we need to compute the four additional
 * pieces of information:
 *
 * 1. Is this net written above?
 * 2. Is this net read above?
 * 3. Is this net written below?
 * 4. Is this net read below?
 *
 * Above and below readers/writers mean that readers/writers exist
 * higher/lower in the hierarchy.
 *
 * Note: For primary ports, we can calculate the above read/write
 * characteristics statically.
 *
 * 1. A primary output has an external reader; it is considered to
 *    have an external writer if there are internal readers and there
 *    is no strong internal driver.
 *
 * 2. A primary input has an external writer; it has an external
 *    reader if there is any internal writer.
 *
 * 3. A primary inout has an external reader; it has an external
 *    writer if there is no strong internal driver.
 *
 * The following algorithm is used to propagate read/write information
 * across each cluster:
 * 
 * 1. Hierarchically sort all elements (elaborated nets).
 *
 *    This hierarchical sort allows easy top-down/bottom-up traversal.
 *    Elements at higher levels in the hierarchy appear earlier in the
 *    sort. We use a data-structure that allows both forward and
 *    reverse-iteration (reverse iteration allowing bottom-up).
 *
 * 2. Until no further propagation occurs:
 *
 *    a. Top-down propagation
 *
 *       For each element in the sorted cluster:
 *
 *           Parent-to-Child:
 *           For each (hierarchical) fanin of this element:
 * \verbatim
 *               rwData[ fanin ].aboveReader |= rwData[ element ].aboveReader
 *               rwData[ fanin ].aboveReader |= rwData[ element ].localReader
 *               rwData[ fanin ].aboveWriter |= rwData[ element ].aboveWriter
 *               rwData[ fanin ].aboveWriter |= rwData[ element ].localWriter
 * \endverbatim
 *
 *           Sibling-to-Sibling:
 *           For each (hierarchical) fanin of this element:
 *               rwData[ fanin ].aboveReader = true if there is a sibling reader.
 *               rwData[ fanin ].aboveWriter = true if there is a sibling writer.
 *
 *    b. Bottom-up propagation
 *
 *       For each element in the reverse-sorted cluster:
 *
 *           Child-to-Parent:
 *           For each (hierarchical) fanin of this element:
 * \verbatim
 *               rwData[ fanin ].belowReader |= rwData[ element ].belowReader
 *               rwData[ fanin ].belowReader |= rwData[ element ].localReader
 *               rwData[ fanin ].belowWriter |= rwData[ element ].belowWriter
 *               rwData[ fanin ].belowWriter |= rwData[ element ].localWriter
 * \endverbatim
 *
 * We now have complete read/write information for all elements in all
 * clusters.
 *
 * IV. Determining Port Direction
 *
 * 1. Use the elaborated read/write information to calculate the
 *    read/write characteristics for unelaborated nets.
 *
 * 2. Determine if a port is input, output, or inout based on the
 *    following (ordered) rules:
 *
 *    a. inout: ReadWriteData::isInout() returns true.
 *    b. inout: The net is a primary port and
 *              ReadWriteData::isPrimaryInout() returns true.
 *    c. input: ReadWriteData::isInput() returns true.
 *    d. output: ReadWriteData::isOutput() returns true.
 *
 *    See each of those methods for the different direction rules.
 *
 * \sa ReadWriteData::isInout
 * \sa ReadWriteData::isPrimaryInout
 * \sa ReadWriteData::isInput
 * \sa ReadWriteData::isOutput
 */
class PortCoercion
{
public:
  //! Constructor
  PortCoercion(AtomicCache* strCache, 
               IODBNucleus* iodb, 
               Stats* stats, 
               ArgProc* args,
               MsgContext* msgContext, 
               NUNetRefFactory* netRefFactory,
               SourceLocatorFactory* sourceLocatorFactory,
               bool do_port_splitting,
               bool verbose_port_splitting,
               bool verbose);

  //! Destructor
  ~PortCoercion();

  //! Process the entire design for port coercion opportunities.
  /*!
   * \param design     The design.
   * \param phaseStats Should we calculate intermediate performance information?
   */
  void design(NUDesign * design, bool phaseStats);

private:
  //! Conditionally perform port-lowering if there are complicated port connections.
  /*!
   * \sa SplittingLowerCallback
   */
  void lower();

  //! Create temporary infrastructure necessary for coercion.
  /*!
   * Currently, this process performs the following operations (there
   * is opportunity to make this more minimal):
   *
   * 1. Calculate UD for all modules.
   *
   * 2. Update read/write data for nets; do not consider simple port
   *    connections as reads/writes.
   *
   * 3. Create the unelaborated DFG.
   *
   * All of these operations populate temporary data structures.
   * Private symbol table and flow factories (elab & unelab) are
   * created. These are thrown away by ::destroyFramework().
   *
   * \param phaseStats Should we calculate intermediate performance information?
   * \sa destroyFramework
   */
  void createFramework(bool phaseStats);

  //! Destroy temporary infrastructure
  /*!
   * Destroy all the private, temporary data structures created by
   * ::createFramework().
   *
   * \param phaseStats Should we calculate intermediate performance information?
   * \sa createFramework, cleanup
   */
  void destroyFramework(bool phaseStats);

  //! Perform all design analysis required to discover coercion points.
  /*!
   * This is a simple wrapper around ::traverse() and ::process. See
   * those methods for more complete documentation.
   *
   * \sa traverse, process
   */
  void discoverCoercion(bool phaseStats);

  //! Perform a design walk computing connectivity and fanin relationships.
  /*!
   * Undirected connectivity information is stored in 'closure' as a
   * set of nets. Set closure is performed over this set of sets to
   * determine distinct, connected components.
   *
   * The hierarchical fanin/fanout relations are stored in 'fanin'.
   * Here, we build arcs from nets on the actual side of port
   * connections to nets on the formal side.
   * 
   * \param closure Set closure defining the distinct, connected port-connection sub-graphs.
   * \param fanin   The fanin relations of the port-connection graph.
   */
  void traverse(NUNetElabRefClosure * closure,
                DoubleNetElabRefSet * fanin,
                NUNetElabRefSet     * written_ports);

  //! Traverse the connected sub-graphs (cluster) in turn.
  /*!
   * 1. Process each of the connected components in turn using
   *    ::processCluster(). This generates and propagates read/write
   *    information across the entire design.
   *
   * 2. Convert the elaborated read/write data into unelaborated
   *     read/write data.
   *
   * 3. Analyze the unelaborated read/write data and determine which
   *    ports act as inputs, outputs, and inouts. Mark nets with
   *    changed direction for redeclaration.
   *
   * \param closure Set closure defining the distinct, connected port-connection sub-graphs.
   * \param fanin   The fanin relations of the port-connection graph.
   * \sa processCluster
   */
  void process(NUNetElabRefClosure * closure,
               DoubleNetElabRefSet * fanin,
               const NUNetElabRefSet * written_ports);

  //! Determine the necessary connectivity arcs.
  /*!
   * 1. Determine the read/write information for each point in isolation.
   *
   * 2. Propagate read/write data using ::walkCluster().
   */
  void processCluster(NUNetElabRefSet * cluster,
                      NUNetElabRefToSetHashMap * fanin_map,
                      const NUNetElabRefSet * written_ports,
                      NUNetElabRefReadWriteData * read_write_data);

  //! Iteratively walk this cluster top-down and bottom-up to propagate read/write data.
  /*!
   * Propagate read/write information top-down, across, and bottom-up
   * to fully propagate the read/write data.
   *
   * If this walk is properly ordered, we should need fewer than 4
   * iterations.
   */
  void walkCluster(NUNetElabRefSet * cluster,
                   NUNetElabRefToSetHashMap * fanin_map,
                   NUNetElabRefReadWriteData * read_write_data);

  //! Walk cluster top-down.
  bool walkClusterTopDown(NUNetElabRefSet * cluster,
                          NUNetElabRefToSetHashMap * fanin_map,
                          NUNetElabRefReadWriteData * read_write_data);

  //! Transfer information across siblings.
  bool walkClusterAcrossSiblings(NUNetElabRefSet * cluster,
                                 NUNetElabRefToSetHashMap * fanin,
                                 NUNetElabRefReadWriteData * read_write_data);

  //! Walk cluster bottom-up.
  bool walkClusterBottomUp(NUNetElabRefSet * cluster,
                           NUNetElabRefToSetHashMap * fanin_map,
                           NUNetElabRefReadWriteData * read_write_data);

  //! Determine the read/write information for each point in isolation.
  /*!
   * The cluster is processed twice. The first pass determines local
   * read/write characteristics and whether or not there is a strongly
   * driven component.
   *
   * The second pass updates the read/write information for primary
   * ports based on whether or not the cluster has readers or strong
   * writers.
   */
  void determineReadWrite(NUNetElabRefSet * cluster,
                          NUNetElabRefToSetHashMap * fanin_map,
                          const NUNetElabRefSet * written_ports,
                          NUNetElabRefReadWriteData * read_write_data);

  //! Determine from unelab read/write data the proper net declarations.
  void updatePortDirection(const NUNetElabRefReadWriteData * read_write_data);

  //! Convert elaborated read/write data into unelaborated read/write data.
  void determineUnelabReadWrite(const NUNetElabRefReadWriteData * read_write_data,
                                NUNetReadWriteData * unelab_read_write_data) const;

  //! Determine if a given point has a strong continuous driver.
  /*!
   * Each unelaborated reachable alias is processed via
   * PortAnalysis::hasStrongContDriver.
   *
   * \sa PortAnalysis::hasStrongContDriver
   */
  bool hasStrongContDriver(const NUNetElabRefHdl & point) const;

  //! Determine if a given net has a tieNet directive specified.
  bool isTieNet(NUNet * net) const;

  //! Determine if a given point has a local writer.
  /*!
   * All elaborated aliases are considered when determining if there
   * is a local writer. As a result, hierarchical writing references
   * are seen as local writers during this analysis.
   *
   * \sa isWritten
   */
  bool isWrittenPoint(const NUNetElabRefHdl & point) const;

  //! Does a net have local writers?
  /*!
   * Consider mutability, tristate and other weak drivers in this computation.
   */
  bool isWritten(const NUNet * net) const;

  //! Determine if a given point has a local reader.
  /*!
   * All elaborated aliases are considered when determining if there
   * is a local reader. As a result, hierarchical reading references
   * are seen as local readers during this analysis.
   *
   * \sa isRead
   */
  bool isReadPoint(const NUNetElabRefHdl & point) const;

  //! Does a net have local readers?
  /*!
   * Consider observability as well as actual readers.
   */
  bool isRead(const NUNet * net) const;

  //! Convert from a set containing the fanin/fanout pairs to the fanin/fanout map format.
  void relationToConnectivity(const DoubleNetElabRefSet * in, NUNetElabRefToSetHashMap * out) const;

  //! Convert from the fanin/fanout map format to a set containing the fanin/fanout pairs.
  void connectivityToRelation(const NUNetElabRefToSetHashMap * in, DoubleNetElabRefSet * out) const;

  //! Remember that a directed port should be coerced to inout.
  void rememberInout(NUNet * net);

  //! Remember that an output or bid should be coerced to input.
  void rememberInput(NUNet * net);

  //! Remember that an input or bid should be coerced to output.
  void rememberOutput(NUNet * net);

  //! Redeclare the direction of ports and update port connections
  /*! 
   * For each port with incorrect direction, redeclare with the new
   * direction. 
   *
   * The design is traversed and port connections are updated to
   * reflect the new port direction.
   *
   * \sa redeclareInouts,redeclareInputs,redeclareOutputs
   */
  void coerce();

  //! Redeclare ports that need transforming to inout.
  /*!
   * Warnings are generated for primary ports, notes are emitted for
   * non-primary ports.
   */
  void redeclareInouts() const;

  //! Redeclare ports that need transforming to input.
  /*!
   * Warnings are generated for primary ports, notes are emitted for
   * non-primary ports.
   */
  void redeclareInputs() const;

  //! Redeclare ports that need transforming to output.
  /*!
   * Warnings are generated for primary ports, notes are emitted for
   * non-primary ports.
   */
  void redeclareOutputs() const;

  //! Ensure that nets are not considered multiple coercion types.
  void checkCoercionConsistency() const;

  //! Eliminate stored information about how nets should be coerced.
  /*!
   * This cleanup process is independent from the design and DFG-based
   * framework which is created by ::createFramework() and then
   * destroyed by ::destroyFramework.
   *
   * The net coercion facts need to persist outside of this framework.
   *
   * \sa createFramework, destroyFramework
   */
  void cleanup();

  //! Print a set of NUNetElabRef; used as a connected sub-graph (cluster).
  void printCluster(const NUNetElabRefSet * cluster) const;

  //! Print the fanin relations from map.
  void printFaninMap(const NUNetElabRefToSetHashMap * fanin_map) const;

  //! Print the fanin relations from set.
  void printFanin(const DoubleNetElabRefSet * fanin) const;

  //! Print elaborated nets and associated r/w data.
  void printElabReadWriteData(const NUNetElabRefReadWriteData * read_write_data) const;
  
  //! Print nets and associated r/w data.
  void printReadWriteData(const NUNetReadWriteData * read_write_data) const;

  //! Directed ports that will become inout.
  NUNetSet * mInouts;

  //! Ports that will become output.
  NUNetSet * mOutputs;

  //! Ports that will become input.
  NUNetSet * mInputs;

  //! String cache (External)
  AtomicCache * mStrCache;

  //! IODB (External)
  IODBNucleus * mIODB;

  //! Performance statistics (External)
  Stats * mStats;

  //! Command-line arguments (External)
  ArgProc * mArgs;

  //! Message context  (External)
  MsgContext * mMsgContext;

  //! Net-ref factory (External)
  NUNetRefFactory * mNetRefFactory;
  
  //! Source-locator factory (External)
  SourceLocatorFactory * mSourceLocatorFactory;

  //! Is port splitting enabled?
  bool mDoPortSplitting;

  //! Should we be verbose during port splitting?
  bool mVerbosePortSplitting;

  //! Should we be verbose during processing?
  bool mVerbose;

  //! Port coercion phase.
  SInt32 mPhase;

  //! The design
  NUDesign * mDesign;

  //! Local flow factory; destroyed as we exit.
  FLNodeFactory     * mFlowFactory;

  //! Local elaborated flow factory; never populated; destroyed as we exit.
  FLNodeElabFactory * mFlowElabFactory;

  //! Local unelab-flow generator; destroyed as we exit.
  UnelabFlow * mUnelaborate;

  //! Reachable alias query interface;  destroyed as we exit.
  ReachableAliases * mReachables;

  //! Local BOM generator; destroyed as we exit.
  CbuildSymTabBOM * mTmpSymTabBOM;

  //! Local symbol table (used for elaboration); destroyed as we exit.
  STSymbolTable * mTmpSymbolTable;

  //! Private elaborated netref factory.
  NUNetElabRefFactory * mNetElabRefFactory;
  
  //! PortAnalysis interface object.
  PortAnalysis * mPortAnalysis;
};

#endif
