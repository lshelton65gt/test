// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/ProcessResynth.h"
#include "localflow/UD.h"
#include "localflow/UpdateUD.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUBitNet.h"
#include "util/CbuildMsgContext.h"
#include "util/ArgProc.h"
#include "bdd/BDD.h"
#include "reduce/Inference.h"
#include "reduce/Ternary.h"
#include "reduce/LoopUnroll.h"

//! Class that holds the collection of often needed contextual data.
class ResynthContext
{
public:
  ResynthContext(MsgContext* msgCtx, NUNetRefFactory* factory,
                 IODBNucleus* iodb, AtomicCache* atomicCache,
                 ArgProc* args, bool verbose)
    : mCurrentModule(NULL),
      mMsgCtx(msgCtx),
      mFactory(factory),
      mIODB(iodb),
      mAtomicCache(atomicCache),
      mArgs(args),
      mVerbose(verbose)
  {
  }

  void setCurrentModule(NUModule* module) {
    mCurrentModule = module;
  }

  NUModule* getCurrentModule() {
    return mCurrentModule;
  }

  MsgContext* getMsgContext() {
    return mMsgCtx;
  }

  NUNetRefFactory* getNetRefFactory() {
    return mFactory;
  }

  IODBNucleus* getIODB() {
    return mIODB;
  }

  AtomicCache* getStringCache() {
    return mAtomicCache;
  }

  ArgProc* getArgs() {
    return mArgs;
  }

  bool getVerbose() {
    return mVerbose;
  }

private:

  NUModule*        mCurrentModule;
  MsgContext*      mMsgCtx;
  NUNetRefFactory* mFactory;
  IODBNucleus*     mIODB;
  AtomicCache*     mAtomicCache;
  ArgProc*         mArgs;
  bool             mVerbose;
};

//! Functor that helps arrange net refs in increasing range order.
class NetRefCmp
{
public:
  NetRefCmp() {}

  bool operator()(const NUNetRefHdl& n1, const NUNetRefHdl& n2)
  {
    ConstantRange r1, r2;
    bool b1 = n1->getRange(r1);
    bool b2 = n2->getRange(r2);
    INFO_ASSERT(b1 && b2, "Expecting contiguous ranges");
    INFO_ASSERT(!r1.overlaps(r2), "Unexpected range overlap");
    return (r1.getMsb() > r2.getMsb());
  }
};

//! Call back that, given live defs, accepts lvalues, figures out which part
//! is alive and which is dead. It them replaces the lvalue with a concat
//! of live and dead parts. Example:
//! Live defs: q[2:1]
//! Lvalue: q[3:0]
//! Resultant Lvalue : {dead_q[3], q[2:1], dead_q[0]}
class KillDeadDefs : public NuToNuFn
{
public:

  KillDeadDefs(const NUNetRefSet& live_defs, ResynthContext* context)
    : mLiveDefs(live_defs),
      mContext(context),
      mDoneZone(0)
  {
  }

  ~KillDeadDefs()
  {
    // Delete lvalues marked for deletion.
    for (NULvalueVectorIter itr = mMarkedForDeletion.begin();
         itr != mMarkedForDeletion.end(); ++itr) {
      delete *itr;
    }
  }

  virtual NULvalue* operator()(NULvalue* lval, Phase phase)
  {
    if (phase == ePost) {
      if (mDoneZone > 0) {
        --mDoneZone;
      }
      return NULL;
    }

    if (mDoneZone > 0) {
      return NULL; // Children of processed parent, skip them.
    }

    if (lval->getType() == eNUConcatLvalue) {
      // Continue diving inside concat lvalue.
      return NULL;
    }

    ++mDoneZone; // Prevent children from being processed further.

    NUNetRefFactory* factory = mContext->getNetRefFactory();
    NUNetRefSet lval_defs(factory);
    lval->getDefs(&lval_defs);
    // Difference between live_defs and lval_defs will give us a 
    // set of dead lval defs.
    NUNetRefSet lval_dead_defs(factory);
    NUNetRefSet::set_difference(lval_defs, mLiveDefs, lval_dead_defs);
    // The entire lvalue is live. Leave it alone.
    if (lval_dead_defs.empty()) {
      return NULL;
    }

    // Either the entire or part of lvalue is dead.
    NULvalue* new_lval = NULL;
    NUNet* lval_net = getLvalueNet(lval);
    NUNet* dead_net =
      lval_net->getScope()->createTempNetFromImage(lval_net, "$dead", true);
    // Intersection between live_defs and lval_defs will give us a 
    // set of live lval defs.
    NUNetRefSet lval_live_defs(factory);
    NUNetRefSet::set_intersection(mLiveDefs, lval_defs, lval_live_defs);
    if (lval_live_defs.empty())
    {
      // No live defs. Replace lvalue with equivalent dead lvalue.
      lval->replaceDef(lval_net, dead_net);
    }
    else
    {
      // Part of the def is live. Construct a concat of live and dead nets.
      new_lval = constructPartiallyLiveLvalue(lval, lval_net, dead_net,
                                              lval_live_defs, lval_dead_defs);
    }
    // Mark the lvalue to be replaced for deletion.
    if (new_lval != NULL)
      mMarkedForDeletion.push_back(lval);
    return new_lval;
  }

private:

  // Get the underlying net from the lvalue.
  NUNet* getLvalueNet(NULvalue* lval)
  {
    NUNet* lval_net = NULL;
    switch (lval->getType())
    {
    case eNUIdentLvalue:
      lval_net = lval->getWholeIdentifier();
      break;
    case eNUVarselLvalue:
      lval_net = dynamic_cast<NUVarselLvalue*>(lval)->getIdentNet(false);
      break;
    case eNUMemselLvalue:
      lval_net = dynamic_cast<NUMemselLvalue*>(lval)->getIdent();
      break;
    default:
      NU_ASSERT("Unexpected node type" == NULL, lval);
      break;
    }
    return lval_net;
  }

  // For a partially live lvalue, figures out which part is alive and which is dead.
  // It them replaces the lvalue with a concat of live and dead parts. Example:
  // Live defs: q[2:1]
  // Lvalue: q[3:0]
  // Resultant Lvalue : {dead_q[3], q[2:1], dead_q[0]}
  NULvalue* constructPartiallyLiveLvalue(NULvalue* lval, NUNet* lval_net,
                                         NUNet* dead_net,
                                         NUNetRefSet& lval_live_defs,
                                         NUNetRefSet& lval_dead_defs)
  {
    if (lval_net->getMemoryNet() != NULL)
    {
      if (lval->getType() != eNUIdentLvalue) {
        // We can't split memsels
        mContext->getMsgContext()->BadMemoryReset(&lval->getLoc());
      }
      return NULL;
    }

    NU_ASSERT(lval_net->isVectorNet(), lval_net);

    NULvalueVector lval_vector;
    NUNetRefFactory* factory = mContext->getNetRefFactory();
    {
      // Create a new def set.
      UtVector<NUNetRefHdl> new_defs;
      // Add all live net refs to the new def set.
      for (NUNetRefSetIter itr = lval_live_defs.begin();
           itr != lval_live_defs.end(); ++itr) {
        addContiguousRangeDefs(*itr, factory, &new_defs);
      }
      // Add all dead net refs to the new def set.
      for (NUNetRefSetIter itr = lval_dead_defs.begin();
           itr != lval_dead_defs.end(); ++itr) {
        NUNetRefHdl dead_net_ref = factory->createNetRefImage(dead_net, *itr);
        addContiguousRangeDefs(dead_net_ref, factory, &new_defs);
      }
      // Sort the new def set net refs by ranges.
      std::sort(new_defs.begin(), new_defs.end(), NetRefCmp());
      // Convert net refs to lvalues and add them to lvalue vector
      convertNetRefToLvalue(factory, lval->getLoc(), new_defs, &lval_vector);
    }

    return createConcatLvalue(lval, lval_vector);
  }

  // Create a concat of given lvalues.
  NULvalue* createConcatLvalue(NULvalue* lval, NULvalueVector& lval_vector)
  {
    NULvalue* ret_lval = new NUConcatLvalue(lval_vector, lval->getLoc());
    // Stick the non-constant index on the concat if the original lvalue
    // is a varsel with dynamic index.
    if (lval->getType() == eNUVarselLvalue)
    {
      NUVarselLvalue* varsel_lval = dynamic_cast<NUVarselLvalue*>(lval);
      if (!varsel_lval->isConstIndex())
      {
        CopyContext cc(0, 0);
        NUExpr* varsel_index = varsel_lval->getIndex()->copy(cc);
        ret_lval = new NUVarselLvalue(ret_lval, varsel_index, lval->getLoc());
      }
    }
    return ret_lval;
  }

  void convertNetRefToLvalue(NUNetRefFactory* factory, const SourceLocator& loc,
                             UtVector<NUNetRefHdl> netrefs, NULvalueVector* lvals)
  {
    for (UtVector<NUNetRefHdl>::iterator itr = netrefs.begin();
         itr != netrefs.end(); ++itr)
    {
      UtList<NULvalue*> lval_list;
      (*itr)->createLvalues(factory, loc, &lval_list);
      for (UtList<NULvalue*>::const_iterator lval_itr = lval_list.begin();
           lval_itr != lval_list.end(); ++lval_itr)
      {
        lvals->push_back(*lval_itr);
      }
    }
  }

  void addContiguousRangeDefs(const NUNetRefHdl& def, NUNetRefFactory* factory,
                              UtVector<NUNetRefHdl>* new_defs)
  {
    for (NUNetRefRangeLoop nrr_loop = def->loopRanges(factory);
         !nrr_loop.atEnd(); ++nrr_loop) {
      new_defs->push_back(nrr_loop.getCurrentNetRef());
    }
  }

  const NUNetRefSet& mLiveDefs;
  ResynthContext*    mContext;
  UInt32 mDoneZone;
  NULvalueVector mMarkedForDeletion;
};

//! Check for varsel lvalues whose selects are dynamic. We can't
//! synthesize such processes into always blocks.
class DynamicSelectResetCheck : public NUDesignCallback
{
public:
  DynamicSelectResetCheck(MsgContext* msgContext, NUNetRefFactory* factory)
    : mMsgContext(msgContext),
      mNetRefFactory(factory)
  {
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase* ) { return eNormal; }
  
  Status operator() (Phase phase, NUVarselLvalue *lval)
  {
    if (phase == ePre) {
      NUExpr* index = lval->getIndex();
      if ((index != NULL) && !isConstant(index)) {
        mMsgContext->NonConstResetLval(&(lval->getLoc()));
      }
    }
    return eNormal;
  }
  
  Status operator() (Phase phase, NUMemselLvalue *lval)
  {
    if (phase == ePre) {
      const UInt32 numDims = lval->getNumDims();
      for (UInt32 dim = 0; dim < numDims; ++dim) {
        NUExpr* index = lval->getIndex(dim);
        if ((index != NULL) && !isConstant(index)) {
          mMsgContext->NonConstResetLval(&(lval->getLoc()));
        }
      }
    }
    return eNormal;
  }

private:

  bool isConstant(NUExpr* index)
  {
    bool is_constant = false;
    if (index->isConstant())
      is_constant = true;
    else
    {
      NUNetRefSet netrefs(mNetRefFactory);
      index->getUses(&netrefs);
      if (netrefs.empty()) {
        is_constant = true;
      }
    }
    return is_constant;
  }

  MsgContext* mMsgContext;
  NUNetRefFactory* mNetRefFactory;
};

// Detects clock expressions like clk'EVENT and not clk'STABLE and
// throws an error. Detector is run after all such expressions have been removed,
// to check if removal was complete.
class ClockExprDetector : public NUDesignCallback
{
public:

  ClockExprDetector(MsgContext* msg_context)
    : mMsgContext(msg_context),
      mErrorDetected(false)
  {
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase* ) { return eNormal; }
  
  Status operator() (Phase phase, NUAttribute* attr)
  {
    if (phase == ePre)
    {
      NUAttribute::AttribT attr_type = attr->getAttrib();
      if ((attr_type == NUAttribute::eEvent) ||
          (attr_type == NUAttribute::eStable)) {
        mMsgContext->ClockingError(&(attr->getLoc()),
                                   "Use of clock edge attribute in nested statements is not supported.");
        mErrorDetected = true;
        return eStop;
      }
    }
    return eNormal;
  }
  
  bool getErrorDetected() const { return mErrorDetected; }

private:
  MsgContext* mMsgContext;
  bool        mErrorDetected;
};

// Detects clock expressions like clk'EVENT and not clk'STABLE and replaces
// them with 1'b1 (cleaning) if requested by setting cleanClockExpr to true.
class ClockExprCleaner : public NuToNuFn
{
public:

  ClockExprCleaner(NUNetRefSet& clockSet, UInt32 clockEdge,
                   bool cleanClockExpr, ResynthContext* context)
    : mClockEdge(clockEdge),
      mClockSet(clockSet),
      mCleanClockExpr(cleanClockExpr),
      mContext(context),
      mErrorDetected(false),
      mClockDetected(false)
  {
  }

  ~ClockExprCleaner()
  {
    for (NUExprListIter itr = mDeleteExprs.begin();
         itr != mDeleteExprs.end(); ++itr) {
      delete *itr;
    }
  }

  virtual NUExpr *operator()(NUExpr *expr, Phase phase)
  {
    NUExpr *new_expr = NULL;
    const SourceLocator loc = expr->getLoc();
    if (phase == ePre && expr->getType() == NUExpr::eNUAttribute)
    {
      // During the ePre phase, optimize away any 'EVENT or 'STABLE attributes,
      // collecting clocks.
      NUAttribute *attrib = dynamic_cast<NUAttribute*>( expr );
      new_expr = detectMaybeCleanEdgeExpr(attrib);
    }
    else if (mCleanClockExpr && (phase == ePost) &&
             expr->getType() == NUExpr::eNUBinaryOp)
    {
      // During the ePost phase, optimize away the clock level expressions
      NUOp *op = dynamic_cast<NUOp*>( expr );
      new_expr = cleanLevelExpr(op);
    }
    return new_expr;
  }

  bool getErrorDetected() const { return mErrorDetected; }
  bool getClockDetected() const { return mClockDetected; }

private:

  ClockExprCleaner();
  ClockExprCleaner( const ClockExprCleaner& );
  ClockExprCleaner &operator=( const ClockExprCleaner& );

  // Cleans binary expressions like (clk = '1') or ('1' = clk) by
  // replacing them with 1'b1.
  NUExpr* cleanLevelExpr(NUOp* op)
  {
    NUExpr* new_expr = NULL;
    if (op->getOp() == NUOp::eBiEq)
    {
      // If a binary op is the clock level specifier (like clk='1'),
      // replace it with TRUE (-> '1').
      NUNetRefSet exprSet(mContext->getNetRefFactory());
      // For a clock expression, the numBits used will be 1 from one
      // arg and 0 on the other.  The side with no bits is the level
      // of the clock.
      NUExpr *level = op->getArg(1);
      op->getArg(0)->getUses( &exprSet );

      if (exprSet.getNumBits() == 0)
      {
        // clock is arg1, level is arg0
        op->getArg(1)->getUses( &exprSet );
        level = op->getArg(0);
      }
        
      if (exprSet == mClockSet)
      {
        // We know we have the clock; now verify the edge.
        UInt32 value = 0;
        if (NUConst* c = level->castConst( ))
        {
          c->getUL ( &value );
        }
        if ( ((value == 1) && (mClockEdge == eClockPosedge)) ||
             ((value == 0) && (mClockEdge == eClockNegedge)) )
        {
          new_expr = NUConst::create(false, 1u, 1, op->getLoc());
          mDeleteExprs.push_back(op);
        }
      }
    }
    return new_expr;
  }

  // Remove `EVENT and `STABLE attributes and replaces them
  // with 1'b1.
  NUExpr* detectMaybeCleanEdgeExpr(NUAttribute* attrib)
  {
    NUExpr* new_expr = NULL;
    NUNetRefSet exprClockSet(mContext->getNetRefFactory());
    attrib->getUses(&exprClockSet);

    if (exprClockSet == mClockSet)
    {
      mClockDetected = true;
      NUAttribute::AttribT attrType = attrib->getAttrib();
      if (attrType == NUAttribute::eEvent)
      {
        // Replace a 'event on the clock with TRUE
        if (mCleanClockExpr) {
          new_expr = NUConst::create(false, 1u, 1, attrib->getLoc());
          mDeleteExprs.push_back(attrib);
        }
      }
      else if (attrType == NUAttribute::eStable)
      {
        // Replace a 'stable on the clock with FALSE
        if (mCleanClockExpr) {
          new_expr = NUConst::create(false, 0u, 1, attrib->getLoc());
          mDeleteExprs.push_back(attrib);
        }
      }
      else
      {
        // Unsupported attribute
        const char *attribStr = attrib->getAttribChar();
        mContext->getMsgContext()->UnsupportedAttribute(&attrib->getLoc(), attribStr);
        mErrorDetected = true;
      }
    }
    else
    {
      // Attribute is not on a clock net
      const char *attribStr = attrib->getAttribChar();
      mContext->getMsgContext()->AttributeNotOnClock( &attrib->getLoc(), attribStr );
      mErrorDetected = true;
    }
    return new_expr;
  }

  UInt32          mClockEdge;
  NUNetRefSet&    mClockSet;
  bool            mCleanClockExpr;
  ResynthContext* mContext;
  bool            mErrorDetected;
  bool            mClockDetected;
  NUExprList      mDeleteExprs;
};

//! Converts an always block populated with VHDL process into one or
//! more always blocks that are synthesizable Verilog always blocks.
class ProcessToAlwaysResynth
{
public:

  // Map of priority bit vector to net ref set.
  typedef UtMap<UInt32, NUNetRefSet*> PriorityMap;
  typedef PriorityMap::const_iterator PriorityMapIter;
  typedef UtList<NUEdgeExprList*> ListOfEdgeExprLists;
  typedef ListOfEdgeExprLists::const_iterator ListOfEdgeExprListsIter;

  ProcessToAlwaysResynth(NUAlwaysBlock* always, ResynthContext* context,
                NUAlwaysBlockList* new_always_list)
    : mAlways(always),
      mContext(context),
      mNewAlwaysList(new_always_list),
      mClockEdgeExpr(NULL),
      mClockSet(context->getNetRefFactory())
  {
  }

  ~ProcessToAlwaysResynth()
  {
  }

  // Resynthesize the always block representing a VHDL process into
  // one or more synthesizable Verilog processes and add them to 
  // the new always block list.
  bool resynthAlwaysBlock(bool* isSynthesizable)
  {
    // Filter out combinational always blocks.
    if (!mAlways->isSequential()) {
      return true;
    }

    // Identify always block clock, reset nets and edge sensitivity.
    if (!identifyClkResetEdge()) {
      return true; // No clocks, nothing to do.
    }

    // Replace the clocked expressions like clk'EVENT and not clk'STABLE
    // with 1'b1, since the clock edges move to always block sensitivity list.
    if (!cleanAlwaysBlockClockedExprs()) {
      return false;
    }

    // Check if process has trailing reset and unroll loops if it does.
    UInt32 num_clocked_stmts = 0;
    bool is_var_state = false;
    bool has_trailing_reset =
      processHasTrailingReset(&is_var_state, &num_clocked_stmts);
    if (num_clocked_stmts == 0) {
      return true; // Nothing to do. These are typically wait based processes.
    } 

    if (has_trailing_reset) {
      // Unroll for loops in this process block to help correctly identify defs.
      unrollLoops();
    }

    if (num_clocked_stmts > 1) {
      // Merge multiple clocked statements into one. The code further 
      // understand how to deal with 1 clocked statement.
      if (!mergeClockedStmts(is_var_state, num_clocked_stmts)) {
        return false;
      }
    }

    NUStmtList prolog_stmts;
    NUStmtList non_reset_stmts;
    NUStmtList edge_stmts;
    // The statements are grouped into statements that appear before edge
    // statements ..prolog_stmts, edge statements, and the ones that appear
    // after that ..non_reset_stmts. Also check that the process is synthesizable.
    if ( !groupStmts(is_var_state, prolog_stmts, non_reset_stmts, edge_stmts) ||
         !(*isSynthesizable = isProcessSynthesizable(prolog_stmts, edge_stmts, non_reset_stmts)) )
    {
      return false;
    }

    // Construct a list of reset edge expression list for every edge statement.
    // Also clean all the clocked expressions like clk'EVENT or not clk'STABLE
    // by replacing them with 1'b1.
    ListOfEdgeExprLists reset_edges;
    constructResetEdgeExprsForStmts(edge_stmts, &reset_edges);

    // Construct a map of priority bit vector to net defs.
    bool success = true;
    PriorityMap priority_map;
    success = constructPriorityMap(edge_stmts, is_var_state, &priority_map);
    if (success) {
      // Construct always blocks based on the priority bit vector map
      constructAlwaysBlocks(edge_stmts, prolog_stmts, non_reset_stmts,
                            priority_map, reset_edges);
    }

    // Clean up after processing is complete.
    cleanup(reset_edges, priority_map);

    return true;
  }

private:

  // Multiple clock statements are merged into one clocked if statement, by
  // creating a new if-statement and inserting all clocked if statements into
  // it's then body.
  bool mergeClockedStmts(bool is_var_state, UInt32 num_clocked_stmts)
  {
    bool success = true;
    NUBlock* block = mAlways->getBlock();
    NUNetRefFactory* factory = mContext->getNetRefFactory();
    NUNetRefSet clocked_defs(factory);
    NUNetRefSet inbetween_defs(factory);
    NUStmtList new_block_stmts;
    NUStmtList clocked_stmts;
    NUStmtList other_stmts;
    UInt32 clk_stmt_idx = 0;
    const SourceLocator& block_loc = block->getLoc();

    for (NUStmtLoop stmt_loop = block->loopStmts();
         success && !stmt_loop.atEnd(); ++stmt_loop)
    {
      bool clocked_stmt = detectStmtClockedExprs(*stmt_loop);

      if (clocked_stmt)
      {
        getDefs(*stmt_loop, is_var_state, &clocked_defs);
        clocked_stmts.push_back(*stmt_loop);

        if (clk_stmt_idx > 0) // Not first clocked statement.
        {
          // Subsequent clocked if statements cannot have else or else-if.
          NUIf* if_stmt = dynamic_cast<NUIf*>(*stmt_loop);
          NU_ASSERT(if_stmt, *stmt_loop);
          if (!if_stmt->emptyElse())
          {
            mContext->getMsgContext()->ClockingError(&block_loc, "If a process has multiple clocked if statements, elsif or else statements on the clocked if statement are not supported." );
            success = false;
          }
        }
        ++clk_stmt_idx;
      }
      else if (clk_stmt_idx == 0)
      {
        // Prologue statements. Add them directly to new block statements.
        new_block_stmts.push_back(*stmt_loop);
      }
      else if (clk_stmt_idx < num_clocked_stmts)
      {
        // These are statements that lie in between two clocked statements.
        getDefs(*stmt_loop, is_var_state, &inbetween_defs);
        clocked_stmts.push_back(*stmt_loop);
      }
      else
      {
        // These are statements after all the clocked statements are done.
        other_stmts.push_back(*stmt_loop);
      }
    }

    // The inbetween statements cannot def any clocked statment defs, since
    // that would mean they're reset statements which are not allowed.
    if (success && NUNetRefSet::set_has_intersection(clocked_defs, inbetween_defs))
    {
      mContext->getMsgContext()->ClockingError(&block_loc, "A process with multiple clocked if statements, with reset statements embedded between them is not supported.");
      success = false;
    }
    // Add clocked and other statements to new block statements.
    if (success)
    {
      // Create a new if statements that has all clocked statements in it's body.
      NUStmt* first_stmt = *(clocked_stmts.begin());
      const SourceLocator& first_stmt_loc = first_stmt->getLoc();
      NUStmtList else_stmts; // No else statements.
      // Merge multiple clocked statements checking for the following:
      NUConst* new_clocked_if_cond = NUConst::create(false, 1, 1,
                                                     first_stmt_loc);
      NUIf* new_clocked_if = new NUIf(new_clocked_if_cond, clocked_stmts,
                                      else_stmts, factory, false,
                                      first_stmt_loc);
      // Replace the statement list in the process.
      new_block_stmts.push_back(new_clocked_if);
      // Since the new if statement is the new clocked statement, add it's
      // condition to clock expression set.
      mClockExprSet.insert(new_clocked_if_cond);
      if (!other_stmts.empty()) {
        new_block_stmts.insert(new_block_stmts.begin(), other_stmts.begin(),
                               other_stmts.end());
      }
      block->replaceStmtList(new_block_stmts);
    }
    return success;
  }

  // Unrolling loops enables statically determining all possible values for
  // the selects that use loop index. This allows differentiating between
  // real dynamic selects ..which are not synthesiable, and static selects
  // which are loop indicies and may look like dynamic selects since they're
  // variables.
  void unrollLoops()
  {
    ArgProc* args = mContext->getArgs();
    LoopUnrollStatistics unroll_stats;
    InferenceStatistics  inference_stats;
    TernaryStatistics    ternary_stats;
    LoopUnroll unroll(mContext->getStringCache(),
                      mContext->getNetRefFactory(),
                      mContext->getMsgContext(),
                      mContext->getIODB(), args, false,
                      mContext->getVerbose(), &unroll_stats,
                      &inference_stats, &ternary_stats);
    unroll.always(mAlways);
  }

  // Delete allocated memory.
  void cleanup(const ListOfEdgeExprLists& reset_edges,
               const PriorityMap& priority_map)
  {
    for (ListOfEdgeExprListsIter reset_itr = reset_edges.begin();
         reset_itr != reset_edges.end(); ++reset_itr) {
      for (NUEdgeExprListIter edge_expr_itr = (*reset_itr)->begin();
           edge_expr_itr != (*reset_itr)->end(); ++edge_expr_itr) {
        delete *edge_expr_itr;
      }
      delete *reset_itr;
    }

    for (PriorityMapIter itr = priority_map.begin();
         itr != priority_map.end(); ++itr) {
      delete itr->second;
    }
  }

  // Trailing reset statements may def some or all bits of nets that are
  // clocked.  We need to know which groups of scalars or bits of vectors
  // share the same clock and reset controls.  To identify these control
  // patterns a mask is constructed for each bit
  // that is clocked and/or reset.  Each position in the mask positionally
  // represents one of the clock or reset controls (currently limited to a
  // maximum of 32 clock or reset controls).  The bit in the mask is
  // set if the net is def'ed by the corresponding control statement.
  // The same mask will be defined for different def'ed bits if those bits
  // appear within identical control statements.  A map from the unique masks
  // to the def'ed bits for that mask is created, and then a separate
  // always block is defined for each unique mask.
  // The individual net set corresponding to that mask corresponds
  // to set of scalar or bits of vector nets that are alive in that always
  // block. Each asserted bit in the mask indicates which statements must be
  // defined in the always block. Here's an example:
  //
  //  always @(posedge clk or posedge rst1 or posedge rst2)
  //   if (clk)       // Bit 0
  //     a[1:0] <= d;
  //   if (rst1)      // Bit 1
  //     a[1] <= 0;
  //   if (rst2)      // Bit 2
  //     a[0] <= 0;

  //   Map looks like:
  //   (1,0,1) => a[0] 
  //   (1,1,0) => a[1]

  //   There will be two always blocks, for two unique bit vector values. The
  //   one for bit vector (1,0,1) looks like:

  //   always @(posedge clk or posedge rst1) // Bit vector (1,0,1)
  //     if (rst2)
  //       a[0] <= 0;
  //     else
  //       a[0] <= d[0];

  bool constructPriorityMap(const NUStmtList& edge_stmts,
                            bool is_var_state,
                            PriorityMap* priority_map)
  {
    UInt32 bit_vec_size = edge_stmts.size(); // A bit for every edge statement.
    if (bit_vec_size > cMaxTrailingResetsAllowed) // Exceeds the limit.
      return false;
    if (bit_vec_size == 1) // No trailing resets for priority ordering.
      return true;
    // Construct a map of net refs to bit vector first.
    UtMap<NUNetRefHdl, UInt32> net_ref_map;
    UInt32 itr_mask = 0x1;
    NUNetRefFactory* factory = mContext->getNetRefFactory();
    NUNetRefSet blocking_defs(factory);
    for (NUStmtListIter itr = edge_stmts.begin();
         itr != edge_stmts.end(); ++itr)
    {
      NUNetRefSet stmt_defs(factory);
      // Collect blocking defs, if variables don't hold state.
      getDefs(*itr, is_var_state, &stmt_defs, &blocking_defs);
      // Add all net refs to the map.
      setScalarNetRefMasks(factory, stmt_defs, itr_mask, &net_ref_map);
      itr_mask <<= 1;
    }

    // Reverse the map so that we have a map of bit vector to net ref set.
    for (UtMap<NUNetRefHdl, UInt32>::iterator itr = net_ref_map.begin();
         itr != net_ref_map.end(); ++itr)
    {
      PriorityMapIter pm_itr = priority_map->find(itr->second);
      NUNetRefSet* netref_set = NULL;
      if (pm_itr == priority_map->end()) {
        netref_set = new NUNetRefSet(blocking_defs);
        priority_map->insert(std::make_pair(itr->second, netref_set));
      } else {
        netref_set = pm_itr->second;
      }
      netref_set->insert(itr->first);
    }
    return true;
  }

  // Construct always blocks based on priority map as explained before.
  void constructAlwaysBlocks(const NUStmtList& edge_stmts,
                             const NUStmtList& prolog_stmts,
                             const NUStmtList& non_reset_stmts,
                             const PriorityMap& priority_map,
                             const ListOfEdgeExprLists& reset_edges)
  {
    const SourceLocator& always_loc = mAlways->getLoc();
    CopyContext cc(0, 0);
    // Add prolog and non reset statements into a separate always block.
    if (!prolog_stmts.empty() || !non_reset_stmts.empty())
    {
      NUBlock* new_blk = createNewBlock(always_loc);
      NUStmtList stmts;
      CopyContext cc(new_blk, mContext->getStringCache());
      copyInsertStmts(prolog_stmts, stmts, cc);
      copyInsertStmts(non_reset_stmts, stmts, cc);
      new_blk->addStmts(&stmts);
      addAlwaysBlock(new_blk, reset_edges, 0);
    }

    UInt32 num_edge_stmts = edge_stmts.size();
    if (!priority_map.empty()) // Do we have priority ordered defs.
    {
      for (PriorityMapIter itr = priority_map.begin();
           itr != priority_map.end(); ++itr)
      {
        UInt32 itr_mask  = itr->first;
        UInt32 stmt_mask = (0x1 << (num_edge_stmts-1));
        NUBlock* new_blk = createNewBlock(always_loc);
        CopyContext cc(new_blk, mContext->getStringCache());

        // Go through the edge statements in priority order. Create
        // an if-else chain by chaining trailing reset if-statements.
        NUIf* new_if_stmt = NULL;
        NUIf* cur_if_stmt = NULL;
        NUEdgeExprList edge_exprs;
        for (NUStmtList::const_reverse_iterator stmt_itr = edge_stmts.rbegin();
             stmt_itr != edge_stmts.rend(); ++stmt_itr)
        {
          if (itr_mask & stmt_mask)
          {
            const NUIf* orig_if_stmt = dynamic_cast<const NUIf*>(*stmt_itr);
            NU_ASSERT(orig_if_stmt, *stmt_itr);
            NUIf* cpy_if_stmt = dynamic_cast<NUIf*>(orig_if_stmt->copy(cc));
            if (cur_if_stmt == NULL) {
              cur_if_stmt = cpy_if_stmt;
              new_if_stmt = cpy_if_stmt;
            } else {
              NUIf* new_cur_if_stmt = getIfWithoutElse(cur_if_stmt);
              // We don't support trailing reset statements with if-else.
              NU_ASSERT(new_cur_if_stmt, cur_if_stmt);
              new_cur_if_stmt->addElseStmt(cpy_if_stmt);
              cur_if_stmt = cpy_if_stmt;
            }
          }
          stmt_mask >>= 1;
        }

        // This is a def set of live nets.
        NUNetRefSet* live_defs = itr->second;
        // Modify the new if statement we've created by chaining trailing reset
        // statements in priority order, by modifying the lvalues of assign 
        // statements within this new if statement to remove dead defs.
        KillDeadDefs kdd(*live_defs, mContext);
        new_if_stmt->replaceLeaves(kdd);

        // Add statements to new always block.
        new_blk->addStmt(new_if_stmt);
        addAlwaysBlock(new_blk, reset_edges, itr_mask /*reset edge mask*/);
      } // for
    } // if
    else // 1 edge statement. 
    {
      INFO_ASSERT(num_edge_stmts == 1, "Internal error constructing priority map.");
      NUBlock* new_blk = createNewBlock(always_loc);
      CopyContext cc(new_blk, mContext->getStringCache());
      NUStmt* orig_stmt = *(edge_stmts.begin());
      NUStmt* new_stmt  = orig_stmt->copy(cc);
      NUStmtList stmts;
      stmts.push_back(new_stmt);
      new_blk->addStmts(&stmts);
      addAlwaysBlock(new_blk, reset_edges, 0x1 /*Mask for the only statement*/);
    }
  }

  // Set mask for each of the scalar net defed.
  void setScalarNetRefMasks(NUNetRefFactory* factory, NUNetRefSet& src_defs,
                            UInt32 mask, UtMap<NUNetRefHdl, UInt32>* net_ref_map)
  {
    for (NUNetRefSetIter src_itr = src_defs.begin();
         src_itr != src_defs.end(); ++src_itr)
    {
      for (NUNetRefRangeLoop nrr_loop = (*src_itr)->loopRanges(factory);
           !nrr_loop.atEnd(); ++nrr_loop)
      {
        NUNetRefHdl nr_hdl = nrr_loop.getCurrentNetRef();
        NUNet* nr_net = nr_hdl->getNet();
        if (nr_net->isVectorNet() ||
            nr_net->isMemoryNet() ||
            nr_net->isBitNet() ||
            nr_net->isArrayNet())
        {
          const ConstantRange range = *nrr_loop;
          SInt32 idx = range.getMsb();
          SInt32 incr = 1;
          if (range.getMsb() > range.getLsb())
            incr = -1;
          while (range.contains(idx))
          {
            NUNetRefHdl bit_hdl = factory->sliceNetRef(nr_hdl, idx);
            (*net_ref_map)[bit_hdl] |= mask;
            idx += incr;
          }
        } else {
          (*net_ref_map)[nr_hdl] |= mask;
        }
      }
    }
  }

  // If the if-else part of if-statement does not have an if-elsif,
  // but some statements, return true.
  bool hasIfElse(NUIf* if_stmt)
  {
    bool has_if_else = false;
    if (!if_stmt->emptyElse()) {
      if (hasIfElseIf(if_stmt)) {
        return hasIfElse(dynamic_cast<NUIf*>(*(if_stmt->loopElse())));
      } else {
        has_if_else = true;
      }
    }
    return has_if_else;
  }

  // Does the if-statement have a VHDL if-elsif? If the if-else part
  // has only one if statement, it does.
  bool hasIfElseIf(NUIf* if_stmt)
  {
    NUStmtLoop else_loop = if_stmt->loopElse();
    if ( !else_loop.atEnd() &&                 // There are statements in if-else
         ((*else_loop)->getType() == eNUIf) && // The first statement is if-stmt
         (++else_loop == else_loop.end()) )    // There are no more statements
      return true;
    return false;
  }

  // Recurse down the if-elseif chain to find an if statement with no else.
  NUIf* getIfWithoutElse(NUIf* if_stmt)
  {
    NUIf* if_without_else = NULL;
    if (if_stmt->emptyElse()) {
      if_without_else = if_stmt;
    } else if (hasIfElseIf(if_stmt)) {
      if_without_else =
        getIfWithoutElse(dynamic_cast<NUIf*>(*(if_stmt->loopElse())));
    }
    return if_without_else;
  }

  // Adds reset expressions to expression list.
  void getResetExprs(NUIf* if_stmt, NUExprList* reset_exprs)
  {
    NUExpr* cond_expr = if_stmt->getCond();
    bool clocked_expr = detectClockedExpr(cond_expr);
    if (!clocked_expr /* && !if_stmt->emptyThen() */) {
      reset_exprs->push_back(if_stmt->getCond());
    }
    if (hasIfElseIf(if_stmt)) {
      getResetExprs(dynamic_cast<NUIf*>(*(if_stmt->loopElse())),
                    reset_exprs);
    }
  }

  // Constructs a new reset net and uses a continuous assign to assign the
  // reset expression to that net. The reset net is used to create the
  // edge expression.
  void constructComplexResetEdgeExpr(NUExpr* reset_expr, NUNetRefSet& uses,
                                     NUEdgeExprList* edge_exprs)
  {
    // Create a name for new reset net.
    UtString name;
    for (NUNetRefSetIter itr = uses.begin(); itr != uses.end(); ++itr) {
      name << (*itr)->getNet()->getName();
      ConstantRange range;
      (*itr)->getRange(range);
      name << "_" << range.getMsb() << "_" << range.getLsb() << "_";
    }
    NUModule* module = mContext->getCurrentModule();
    StringAtom* reset_net_name = module->gensym("$reset", name.c_str());

    // Create reset net with the name.
    NetFlags flags = NetFlags(eDMWireNet | eAllocatedNet | eTempNet);
    const SourceLocator& loc = reset_expr->getLoc();
    NUNet* reset_net = new NUBitNet(reset_net_name, flags, module, loc);
    module->addLocal(reset_net);

    // Create a continuous assign to new reset net of reset expression.
    CopyContext cc(0, 0);
    NULvalue* new_lval = new NUIdentLvalue(reset_net, loc);
    NUExpr* new_rval = reset_expr->copy(cc);
    new_rval->resize(1);
    StringAtom* block_name = module->newBlockName(mContext->getStringCache(), loc);
    NUContAssign* new_assign = new NUContAssign(block_name, new_lval, new_rval, loc);
    module->addContAssign(new_assign);

    // Create an edge expression for the new reset.
    NUExpr* new_reset_expr = new NUIdentRvalue(reset_net, loc);
    new_reset_expr->resize(1);
    NUEdgeExpr* new_edge_expr = new NUEdgeExpr(eClockPosedge, new_reset_expr, loc);
    edge_exprs->push_back(new_edge_expr);
  }

  // Given the reset condition, construct a reset expression based on the 
  // reset value that results in the condition evaluating to true.
  void constructResetEdgeExpr(NUExpr* reset_expr, NUEdgeExprList* edge_exprs)
  {
    NUNetRefSet uses(mContext->getNetRefFactory());
    reset_expr->getUses(&uses);

    UInt32 num_uses = uses.size();
    if (num_uses == 0) {
      return;
    }

    if (num_uses > 1) {
      //  More that one reset nets. Create a synthetic reset net and use that.
      constructComplexResetEdgeExpr(reset_expr, uses, edge_exprs);
    } 

    NUNetRefHdl reset_ref = *(uses.begin());
    NUNet* reset_net = reset_ref->getNet();
    if ((reset_ref->getNumBits() != 1) || (reset_net->getBitSize() != 1)) {
      //  More that one reset bit. Create a synthetic reset net and use that.
      constructComplexResetEdgeExpr(reset_expr, uses, edge_exprs);
    }

    const SourceLocator loc = reset_expr->getLoc();
    NUIdentRvalue* new_reset_expr = new NUIdentRvalue(reset_net, loc);
    new_reset_expr->resize(1);

    // Find the edge to add.
    BDDContext bdd_ctx;
    BDD new_reset_bdd = bdd_ctx.bdd(new_reset_expr);
    BDD reset_bdd = bdd_ctx.bdd(reset_expr);
    BDD not_reset_bdd = bdd_ctx.opNot(bdd_ctx.bdd(reset_expr));
    //  Needs to simplify to posdege or negedge.  If not, we have
    //  something funky like a 'bx comparison.  In that case create a
    //  synthetic reset net and use that.
    if ((new_reset_bdd != reset_bdd) && (new_reset_bdd != not_reset_bdd)) {
      constructComplexResetEdgeExpr(reset_expr, uses, edge_exprs);
    }

    ClockEdge edge = (new_reset_bdd == reset_bdd) ? eClockPosedge : eClockNegedge;
    NUEdgeExpr* new_edge_expr = new NUEdgeExpr(edge, new_reset_expr, loc);
    new_edge_expr->resize(1);
    edge_exprs->push_back(new_edge_expr);
  }

  // This constructs reset edge expression list that has expressions based
  // on statements with increasing priority order. Going from if-statements
  // from top to bottom automatically achieves that. For an if-elseif-else
  // statement though, they have to be added in reverse order, since first
  // condition has the highest priority.
  void constructResetEdgeExprsForStmts(const NUStmtList& edge_stmts,
                                       ListOfEdgeExprLists* reset_edges)
  {
    for (NUStmtListIter stmt_itr = edge_stmts.begin();
         stmt_itr != edge_stmts.end(); ++stmt_itr)
    {
      NUIf* if_stmt = dynamic_cast<NUIf*>(*stmt_itr);
      // Detect reset expressions and add them to the list. Also clean clock
      // expressions along the way.
      NUExprList reset_exprs;
      getResetExprs(if_stmt, &reset_exprs);
      // Add an if-statements reset expressions in reverse order. That way
      // the lower priority expressions go first and then the high priority.
      NUEdgeExprList* edge_exprs = new NUEdgeExprList;
      for (NUExprList::reverse_iterator expr_itr = reset_exprs.rbegin();
           expr_itr != reset_exprs.rend(); ++expr_itr) {
        constructResetEdgeExpr(*expr_itr, edge_exprs);
      }
      reset_edges->push_back(edge_exprs);
    }
  }

  NUBlock* createNewBlock(const SourceLocator& loc)
  {
    return new NUBlock(NULL, mContext->getCurrentModule(),
                       mContext->getIODB(), mContext->getNetRefFactory(),
                       false, loc);
  }

  // Create an always block and add it to new always block list. Also
  // add the provided reset edges and clock edges based on the edge mask.
  void addAlwaysBlock(NUBlock* new_blk, const ListOfEdgeExprLists& reset_edges,
                      UInt32 reset_edge_mask)
  {
    const SourceLocator& always_loc = mAlways->getLoc();
    NUModule* module = mContext->getCurrentModule();
    StringAtom* name = module->newBlockName(mContext->getStringCache(), always_loc);
    NUAlwaysBlock* new_always = 
      new NUAlwaysBlock(name, new_blk, mContext->getNetRefFactory(), always_loc, false);
    mNewAlwaysList->push_back(new_always);
    // Add clock and reset edges to new always block.
    if (reset_edge_mask > 0)
    {
      CopyContext cc(0, 0);
      UInt32 itr_mask = 0x1;
      // Add reset edges.
      for (ListOfEdgeExprListsIter reset_itr = reset_edges.begin();
           reset_itr != reset_edges.end(); ++reset_itr)
      {
        if (reset_edge_mask & itr_mask)
        {
          NUEdgeExprList* edge_expr_list = (*reset_itr);
          for (NUEdgeExprListIter edge_expr_itr = edge_expr_list->begin();
               edge_expr_itr != edge_expr_list->end(); ++edge_expr_itr)
          {
            NUEdgeExpr* new_reset_edge =
              dynamic_cast<NUEdgeExpr*>((*edge_expr_itr)->copy(cc));
            new_always->addEdgeExpr(new_reset_edge);
          }
        }
        itr_mask <<= 1;
      }
      // Add clock edge, if clocked if-statement ..the first edge statement
      // with mask 0x1, is mask-enabled.
      if (reset_edge_mask & 0x1)
      {
        NUEdgeExpr* new_clk_edge = dynamic_cast<NUEdgeExpr*>(mClockEdgeExpr->copy(cc));
        new_always->addEdgeExpr(new_clk_edge);
      }
    }
  }

  // Get the non-blocking and blocking defs (if requested) for given statement.
  // If blocking defs are not requested and a separate blocking def set is
  // provided, that set is populated with blocking defs.
  void getDefs(NUStmt* stmt, bool include_blocking_defs, NUNetRefSet* stmt_set,
               NUNetRefSet* blocking_set = NULL)
  {
    stmt->getNonBlockingDefs(stmt_set);
    if (include_blocking_defs) {
      stmt->getBlockingDefs(stmt_set);
    } else if (blocking_set != NULL) {
      // Collect blocking defs if a set is provided and we're not including
      // blocking defs into statements defs set.
      stmt->getBlockingDefs(blocking_set);
    }
  }

  // Make the following checks that tells us if the process is synthesizable:
  // 1. Statements outside of if/elseif/else clock block. It checks for signals 
  //    assigned both in clock block and outside of it. Variables are okay.
  // 2. All edge statements should be if statements.
  // 3. if-else statements not allowed, while if-elsif statements are allowed
  //    in a trailing reset statement.
  // 4. Dynamic select varsels are not allowed in trailing reset statements.
  //    We cannot correctly if-else chain them, since the bits def'ed cannot
  //    be correctly determined.
  bool isProcessSynthesizable(const NUStmtList& prolog_stmts,
                              const NUStmtList& edge_stmts,
                              const NUStmtList& non_reset_stmts)
  {
    bool isSynthesizable = true;


    // The code below checks for any signal being assigned
    // both asynchronously and synchronously. See IEEE STD 1076.6.2004,
    // $6.1.3.1, item b
    if (!prolog_stmts.empty() || !non_reset_stmts.empty()) 
    {
      NUNetRefSet asynch_stmt_defs(mContext->getNetRefFactory());
      NUNetRefSet synch_stmt_defs(mContext->getNetRefFactory());
      for (NUStmtListIter itr = prolog_stmts.begin(); itr != prolog_stmts.end(); ++itr)
      {
        getDefs(*itr, false, &asynch_stmt_defs);
      }

      for (NUStmtListIter itr = non_reset_stmts.begin(); itr != non_reset_stmts.end(); ++itr)
      {
        getDefs(*itr, false, &asynch_stmt_defs);
      }

      for (NUStmtListIter itr = edge_stmts.begin(); itr != edge_stmts.end(); ++itr)
      {
        getDefs(*itr, false, &synch_stmt_defs);
      }

      NUNetRefSet common_defs(mContext->getNetRefFactory());
      NUNetRefSet::set_intersection(asynch_stmt_defs, synch_stmt_defs, common_defs);
      if (common_defs.empty() == false) 
      {
        isSynthesizable = false;
        for (NUNetRefSet::iterator iter = common_defs.begin(); 
             iter != common_defs.end(); ++iter) 
        {
          NUNetRefHdl commonNet = *iter;
          const SourceLocator loc = commonNet->getNet()->getLoc();
          StringAtom *netName = commonNet->getNet()->getName();
          mContext->getMsgContext()->ProcessNotSynthesizable(&loc, netName->str( ) );
        }
      }
    }

    // Edge statements ..statements which drive clocked_defs, should all
    // be if-statements with no elses. We can't handle other kinds yet.
    for (NUStmtListIter itr = edge_stmts.begin();
         itr != edge_stmts.end(); ++itr)
    {
      if ((*itr)->getType() != eNUIf) {
        const SourceLocator loc = (*itr)->getLoc();
        mContext->getMsgContext()->JaguarConsistency(&loc, "Unexpected statement type in sequential process.");
        isSynthesizable = false;
      }
      // Check that the if statement has no else parts, if this is a reset
      // statement i.e not the first statement which is clocked statement.
      else if (itr != edge_stmts.begin())
      {
        NUIf* if_stmt = dynamic_cast<NUIf*>(*itr);
        if (hasIfElse(if_stmt)) {
          const SourceLocator loc = if_stmt->getLoc();
          mContext->getMsgContext()->ClockingError(&loc, "An else clause is not supported when using this style of reset modelling.");
          isSynthesizable = false;
        }
      }
    }

    if (isSynthesizable)
    {
      // If the same dynamic select is used to clock and reset a vector/memory, we can
      // if-else chain it, provided the select is never def'ed in the always block. 
      // However the use of dynamic selects in sequential blocks, make them non-synthesizable.
      // Simply checking to see that there are no dynamic selects in trailing reset
      // if-statements should be sufficient.
      DynamicSelectResetCheck checker(mContext->getMsgContext(), mContext->getNetRefFactory());
      NUDesignWalker walker(checker, false);
      NUStmtListIter itr = edge_stmts.begin();
      // The first edge statement is clocked. Skip it to reach trailing reset statements.
      for (++itr; itr != edge_stmts.end(); ++itr) {
        walker.stmt(*itr);
      }
    }

    return isSynthesizable;
  }

  // Do the if statements after the first clocked statement, def the clocked
  // defs? If they do, we have trailing resets. 
  bool processHasTrailingReset(bool* is_var_state, UInt32* num_clocked_stmts)
  {
    NUBlock* block = mAlways->getBlock();
    *is_var_state = false;
    bool has_trailing_reset = false;
    *num_clocked_stmts = 0;

    NUNetRefSet clocked_defs(mContext->getNetRefFactory());
    for (NUStmtLoop stmt_loop = block->loopStmts();
         !stmt_loop.atEnd(); ++stmt_loop)
    {
      bool is_clocked = detectStmtClockedExprs(*stmt_loop);

      if (is_clocked)
      {
        getDefs(*stmt_loop, false, &clocked_defs);
        if (clocked_defs.empty()) {
          *is_var_state = true;
          getDefs(*stmt_loop, *is_var_state, &clocked_defs);
        }
        ++(*num_clocked_stmts);
      }
      else if (*num_clocked_stmts > 0)
      {
        NUNetRefSet stmt_defs(mContext->getNetRefFactory());
        getDefs(*stmt_loop, *is_var_state, &stmt_defs);
        if (NUNetRefSet::set_has_intersection(stmt_defs, clocked_defs)) {
          has_trailing_reset = true;
        }
      }
    }
    return has_trailing_reset;
  }

  // All statements prior to first if-statement with clock condition are
  // grouped into prolog_stmts. The if-statements with clock and reset conditons
  // are grouped into edge_stmts. The rest are grouped into non_reset_stmts.
  bool groupStmts(bool is_var_state, NUStmtList& prolog_stmts,
                  NUStmtList& non_reset_stmts, NUStmtList& edge_stmts)
  {
    bool is_prologue = true;
    bool success = true;
    UInt32 numTrailingResetStmts = 0;
    NUBlock* block = mAlways->getBlock();
    NUNetRefSet clocked_defs(mContext->getNetRefFactory());

    for (NUStmtLoop stmt_loop = block->loopStmts();
         success && !stmt_loop.atEnd(); ++stmt_loop)
    {
      bool clocked_stmt = detectStmtClockedExprs(*stmt_loop);

      if (is_prologue)
      {
        // All statements prior to first clocked statement are prolog_stmts.
        if (!clocked_stmt) {
          prolog_stmts.push_back(*stmt_loop);
        } else {
          // Found first edge statement.
          edge_stmts.push_back(*stmt_loop);
          getDefs(*stmt_loop, is_var_state, &clocked_defs);
          is_prologue = false;
        }
      }
      else if (clocked_stmt)
      {
        // We don't support multiple clocked if statements in a VHDL process.
        mContext->getMsgContext()->ClockingError(&((*stmt_loop)->getLoc()),
                                                 "Multiple if statements with clocked conditions not supported.");
        success = false;
      }
      else
      {
        // Further edge statements are trailing reset or clocked if-statements,
        // if they def clocked defs. Otherwise they're other statements.
        NUNetRefSet stmt_defs(mContext->getNetRefFactory());
        getDefs(*stmt_loop, is_var_state, &stmt_defs);

        // Does this statement def clocked defs?
        if (NUNetRefSet::set_has_intersection(stmt_defs, clocked_defs)) {
          edge_stmts.push_back(*stmt_loop);
          ++numTrailingResetStmts;
          if (numTrailingResetStmts > cMaxTrailingResetsAllowed) {
            mContext->getMsgContext()->TooManyResets(&((*stmt_loop)->getLoc()),
                                                     cMaxTrailingResetsAllowed);
          }
        }
        else
        {
          // This is a non-reset statement.
          non_reset_stmts.push_back(*stmt_loop);
        }
      } // else
    } // for
    return success;
  }

  // Make a copy of statements in the src list and add to dest list.
  void copyInsertStmts(const NUStmtList& src, NUStmtList& dest,
                       CopyContext& copy_context)
  {
    for (NUStmtListIter itr = src.begin(); itr != src.end(); ++itr) {
      dest.push_back((*itr)->copy(copy_context));
    }
  }

  // Identify the clock and clock edge for this always block.
  bool identifyClkResetEdge()
  {
    NUAlwaysBlock::EdgeExprLoop eel = mAlways->loopEdgeExprs();
    bool clock_found = false;
    while (!eel.atEnd())
    {
      NUEdgeExpr* edge = *eel;
      ++eel;
      if (edge->isClock())
      {
        if (mClockEdgeExpr == NULL)
        {
          mClockEdgeExpr = edge;
          clock_found = true;
          mClockEdgeExpr->getUses(&mClockSet);
        }
        else if (*mClockEdgeExpr != *edge)
        {
          // We should never get here since Jaguar should catch this error. We may
          // get here due to some bug!.
          INFO_ASSERT(0, "Multiple clock edge sensitivities not allowed in a VHDL process.");
        }
      }
    }
    return clock_found;
  }

  // Does the given statement have clocked expressions in conditions?
  bool detectStmtClockedExprs(NUStmt* stmt)
  {
    bool clock_detected = false;
    if (stmt->getType() == eNUIf) // Only if statements can have clocked conditions
    {
      NUIf* if_stmt = dynamic_cast<NUIf*>(stmt);
      clock_detected = detectClockedExpr(if_stmt->getCond());
      if (!clock_detected && hasIfElseIf(if_stmt)) {
        clock_detected = detectStmtClockedExprs(*(if_stmt->loopElse()));
      }
    }
    return clock_detected;
  }

  // Is the given expression a clocked expression?
  bool detectClockedExpr(NUExpr* cond)
  {
    return (mClockExprSet.find(cond) != mClockExprSet.end());
  }

  // Clean the clock expressions of clk'EVENT or not clk`STABLE and replace them
  // with appropriate boolean constant, since the events move to always
  // sensitivity list. Check that cleaning is complete.
  bool cleanAlwaysBlockClockedExprs()
  {
    bool success = true;
    NUBlock* block = mAlways->getBlock();
    for (NUStmtLoop stmt_loop = block->loopStmts();
         success && !stmt_loop.atEnd(); ++stmt_loop) {
      cleanStmtClockedExprs(*stmt_loop, &success);
    }
    // Check to ensure that we've cleaned all clock expressions. Clock expressions
    // anywhere but in the top level if statement conditions is an error.
    if (success) {
      ClockExprDetector ced(mContext->getMsgContext());
      NUDesignWalker walker(ced, false);
      walker.alwaysBlock(mAlways);
      if (ced.getErrorDetected())
        success = false;
    }
    return success;
  }

  bool cleanStmtClockedExprs(NUStmt* stmt, bool* success)
  {
    bool clock_detected = false;
    if (stmt->getType() == eNUIf) // Only if statements can have clocked conditions
    {
      NUIf* if_stmt = dynamic_cast<NUIf*>(stmt);
      clock_detected = cleanClockedExpr(if_stmt->getCond(), success);
      // There shouldn't be an else or elseif for an if-stmt with clocked condition.
      if (clock_detected && !if_stmt->emptyElse()) {
        UtString reason( "The last clause of the if statement must be an elsif containing the process clock." );
        mContext->getMsgContext()->ClockingError(&if_stmt->getLoc(), reason.c_str());
        *success = false;
      }
      // Clean the else-if condition clocks
      if (*success && hasIfElseIf(if_stmt)) {
        clock_detected |= cleanStmtClockedExprs(*(if_stmt->loopElse()), success);
      }
    }
    return clock_detected;
  }

  bool cleanClockedExpr(NUExpr* cond, bool* success)
  {
    bool clock_detected = false;
    ClockExprCleaner ceclr(mClockSet, (UInt32)mClockEdgeExpr->getEdge(),
                           true, mContext);
    cond->replaceLeaves(ceclr);
    if (ceclr.getErrorDetected()) {
      *success = false;
    } else if (ceclr.getClockDetected()) {
      clock_detected = true;
      mClockExprSet.insert(cond);
    }
    return clock_detected;
  }

  NUAlwaysBlock*     mAlways;
  ResynthContext*    mContext;
  NUAlwaysBlockList* mNewAlwaysList;

  NUEdgeExpr*     mClockEdgeExpr;
  NUNetRefSet     mClockSet;
  NUExprSet       mClockExprSet;

  static const UInt32 cMaxTrailingResetsAllowed;
};

//! Maximum number of trailing reset statements that we can handle.
const UInt32 ProcessToAlwaysResynth::cMaxTrailingResetsAllowed = 30;

//! Iterates through modules in the design, synthesizing sequential
//! always blocks containing VHDL processes, into synthesizable
//! Verilog always blocks.
class ModuleProcessToAlwaysResynth : public NUDesignCallback
{
public:
  ModuleProcessToAlwaysResynth(ResynthContext* context)
    : mContext(context),
      mSuccess(true),
      mErrorAlwaysBlock(NULL),
      mErrorAlwaysBlockIsSynthesizable(true)
  {
  }

  ~ModuleProcessToAlwaysResynth()
  {
    for (NUAlwaysBlockListIter itr = mMarkedForDeletion.begin();
         itr != mMarkedForDeletion.end(); ++itr) {
      delete *itr;
    }
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * ) { return eNormal; }

  Status operator() (Phase phase, NUModule *node)
  {
    // Allow VHDL modules in pre phase only.
    if ((phase != ePre) || (node == NULL) ||
        (node->getSourceLanguage() != eSLVHDL))
      return eNormal;

    // Look for always blocks.
    NUAlwaysBlockList always_list;
    node->getAlwaysBlocks(&always_list);
    if (always_list.empty())
      return eNormal;

    // Build UD for this processing.
    buildUD(node);

    mContext->setCurrentModule(node);
    for (NUAlwaysBlockListIter itr = always_list.begin();
         mSuccess && (itr != always_list.end()); ++itr)
    {
      if (isEmptyAlwaysBlock(*itr))
      {
        node->removeAlwaysBlock(*itr);
        mMarkedForDeletion.push_back(*itr);
      }
      else
      {
        NUAlwaysBlockList new_always_list;
        ProcessToAlwaysResynth always_resynth(*itr, mContext, &new_always_list);
        bool isSynthesizable = true;
        mSuccess &= always_resynth.resynthAlwaysBlock(&isSynthesizable);
        if (!mSuccess) {
          mErrorAlwaysBlock = *itr;
          mErrorAlwaysBlockIsSynthesizable = isSynthesizable;
        }
        if (!new_always_list.empty()) {
          node->removeAlwaysBlock(*itr);
          mMarkedForDeletion.push_back(*itr);
        }
        for (NUAlwaysBlockListIter new_itr = new_always_list.begin();
             new_itr != new_always_list.end(); ++new_itr) {
          node->addAlwaysBlock(*new_itr);
        }
      }
    }

    // Clear UD information since further synthesis steps like memory bit
    // vector synthesis are not UD friendly.
    clearUD(node);

    if (!mSuccess) {
      return eStop; // Stop on error.
    }
    return eNormal;
  }

  bool getStatus() const { return mSuccess; }

  void printError() const {
    if (!mSuccess) {
      const SourceLocator* loc = &(mErrorAlwaysBlock->getLoc());
      UtString msg;
      if (mErrorAlwaysBlockIsSynthesizable) {
        msg << "This type of clocking and/or reset structure is not supported.";
      } else {
        msg << "The process could be unsynthesizable.";
      }
      mContext->getMsgContext()->UnsupportedSeqProcess(loc, msg.c_str());
    }
  }

  const SourceLocator* getErrorAlwaysBlockLoc() const {
    if (mErrorAlwaysBlock) {
      return &(mErrorAlwaysBlock->getLoc());
    }
    return NULL;
  }

private:  

  bool isEmptyAlwaysBlock(const NUAlwaysBlock* always_block)
  {
    const NUBlock* block = always_block->getBlock();
    NUStmtCLoop stmt_loop = block->loopStmts();
    if (stmt_loop.atEnd())
    {
      NUEdgeExpr  *pNUedgeExpr = always_block ->getEdgeExpr();
      if(pNUedgeExpr == NULL)
        mContext->getMsgContext()->EmptyProcessBlock(&(always_block->getLoc()));
      else
        mContext->getMsgContext()->EmptyClockBlock(&(always_block->getLoc()));
   
      return true;
    }
    return false;
  }

  void buildUD(NUModule* mod)
  {
    UD ud(mContext->getNetRefFactory(), mContext->getMsgContext(),
          mContext->getIODB(), mContext->getArgs(), false);
    ud.module(mod);
  }

  void clearUD(NUModule* mod)
  {
    ClearUDCallback clear_ud(false);
    NUDesignWalker clear_walker(clear_ud, false);
    clear_walker.module(mod);
  }

  ResynthContext*  mContext;
  bool             mSuccess;
  NUAlwaysBlock*   mErrorAlwaysBlock;
  bool             mErrorAlwaysBlockIsSynthesizable;

  NUAlwaysBlockList mMarkedForDeletion;
};

ProcessResynth::ProcessResynth(MsgContext* msgCtx, NUNetRefFactory* factory,
                               IODBNucleus* iodb, AtomicCache* atomicCache,
                               ArgProc* args, bool verbose)
  : mContext(NULL)
{
  mContext = new ResynthContext(msgCtx, factory, iodb, atomicCache, args, verbose);
}

ProcessResynth::~ProcessResynth()
{
  delete mContext;
}

bool ProcessResynth::design(NUDesign* design)
{
  ModuleProcessToAlwaysResynth module_cb(mContext);
  NUDesignWalker module_walker(module_cb, false /* verbose */);
  module_walker.design(design);

  bool success = module_cb.getStatus();
  if (!success) {
    module_cb.printError();
  }

#ifdef CDB
  // For checking that NUAttribute's of type 'EVENT and 'STABLE
  // don't survive past resynthesis.
  if (success) {
    ClockExprDetector ced(mContext->getMsgContext());
    NUDesignWalker walker(ced, false);
    walker.design(design);
  }
#endif

  return success;
}

