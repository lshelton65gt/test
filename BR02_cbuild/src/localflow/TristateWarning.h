// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef TRISTATEWARNING_H_
#define TRISTATEWARNING_H_

#include "nucleus/Nucleus.h"

class MsgContext;

/*!
  \file
  Declaration of local tristate warning package.
 */

//! Class which issues warnings for tristate drivers
/*!
  Detect when a net is driven by z and non-z drivers, issue a warning.
*/
class TristateWarning
{
public:
  //! constructor
  /*!
    \param msg_ctx Context to issue error messages.
   */
  TristateWarning(MsgContext *msg_ctx) :
    mMsgContext(msg_ctx)
  {}

  //! destructor
  ~TristateWarning() {}

  //! Perform analysis over this module.
  void module(NUModule *module);

  //! Issue messages for when a tristate net is driven by a non-tristate driver.
  void detectZConflicts(NUNet *net);

private:
  //! Hide copy and assign constructors.
  TristateWarning(const TristateWarning&);
  TristateWarning& operator=(const TristateWarning&);

  //! Message context.
  MsgContext *mMsgContext;
};

#endif
