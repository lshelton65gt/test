// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/MemoryResynth.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "util/CbuildMsgContext.h"
#include "util/UtStack.h"

//#define MEMRESYNTH_DEBUG 1

/*! \file

This file contains all the code necessary for memory resynthesis.
Memory resynthesis is the process of identifying and transforming memories
that have more than a single packed or more than a single unpacked dimension.
All such memories are modified so that they have just 1 packed and 1 unpacked
dimension, which is the simple memory model supported by the remainder of
cbuild.  Of course once the memories are transformed all references to these
memories must also be rewritten to use the new definition of dimensions.

Phase 0 uses a NUDesignWalker derivation to identify all memories that were
declared with more than 1 packed dimension and no unpacked dimensions into a
memory that still has the same packed dimensions but an 'implicit' unpacked
dimension of [0:0].  All references to this this memory are rewritten with an
additional dimension specification of [0] as the first dimension.
original                              rewritten
reg [11:10][8:1] mem1;                 reg [11:10][8:1] mem1 [0:0];
foo = mem1[10];                        foo = mem1[0][10];
bar = mem1;                            bar = mem1 [0];

Phase 1 ues a NUDesignWalker derivation to transform all NUMemoryNet
nodes with more than a total 2 dimensions (e.g. more than a single packed
and a single unpacked dimension) into a canonical and normalized form that
has a single packed and a single unpacked dimension.
original                              rewritten
reg [11:10][8:1] mem2 [1:2][4:5];     reg [15:0] mem1 [3:0];

Phase 2 uses a NuToNuFn derivation to convert all NUMemselLvalue
and NUMemselRvalue nodes that refer to memories modified during the first
phase, into a form that uses the adjusted memory bounds.  If a memsel needs
to be rewritten into a concat, it is rewritten in phase 2.
original                              rewritten
foo = mem2[2][5][10][1];              foo = mem2[0][0]; // 1 bit   
foo = mem2[1][4];                     foo = mem2[3]; // 1 word of resynthesized memory (16 bits)
foo = mem2[2];                        foo = {mem2[1],mem2[0]};

Phase 3 cleans up after phase2, which might have created
mem <= concat or concat <= mem assignments.  These are rewritten into
concat <= concat assignments. mem <= mem assignments are left unmodified.
*/


typedef enum {
  eIsUndefined,
  eIsWholeOfMemoryWord,
  eIsSliceOfMemoryWord,
  eIsPartOfMemoryWord
} ResynthType;


MemoryIterator::MemoryIterator( const NUMemoryNet *mem, const int dims )
  : mMem( mem ), mDimsToIterate( dims )
{
  mReturnedIndicies = new int[dims];
  mDimIncr = new int[dims];
  for( int i = 0; i < mDimsToIterate ; ++i )
  {
    const ConstantRange *range = mem->getRange( mMem->getNumDims() - mDimsToIterate + i );
    mReturnedIndicies[i] = range->getMsb();
    if ( range->getMsb() >= range->getLsb() )
    {
      mDimIncr[i] = -1;
    }
    else
    {
      mDimIncr[i] = 1;
    }
  }
}


void
MemoryIterator::reset()
{
  for ( int i = 0; i < mDimsToIterate; ++i )
  {
    mReturnedIndicies[i] = mMem->getRange( mMem->getNumDims() - mDimsToIterate + i )->getMsb();
  }
}

MemoryIterator::~MemoryIterator()
{
  delete [] mReturnedIndicies;
  delete [] mDimIncr;
}

int* const*
MemoryIterator::getIndexArray() const
{
  return &mReturnedIndicies;
}

bool
MemoryIterator::incrementDim( int dim )
{
  const ConstantRange *range = mMem->getRange(mMem->getNumDims() - mDimsToIterate + dim);
  int newIndex = mReturnedIndicies[dim] + mDimIncr[dim];
  if ( range->contains( newIndex ))
  {
    mReturnedIndicies[dim] = newIndex;
    return true;
  }
  else if ( dim + 1 < mDimsToIterate )
  {
    mReturnedIndicies[dim] = range->getMsb();
    return incrementDim( dim + 1 );
  }
  else
  {
    return false;
  }
}

bool
MemoryIterator::next()
{
  if ( mDimsToIterate > 0 && incrementDim( 0 ))
    return true;
  else
    return false;
}

/*!
  class: AddImplicitUnpackedDimensionCallback

  Used to make two changes: (implemented in 2 passes because we need to modify the NUMemoryNets (declarations) first)
  Pass 1. locate all NUMemoryNet declared with 2 or more packed dimensions and 0 unpacked dimensions:
          adds an unpacked dimension to this memory [0:0], mark these memories with flag indicating extra dimension.
  Pass 2. Locates all NUMemsel{L,R}value objects for the memories changed in Pass1:
          injects an index expression for this new dimension (simply [0])
          Also locates all NUIdentRvalue references to the memories changed in Pass1, creates a replacement expression that is a
          NUMemselRvalue that uses a select expression: [0] and adds the pair (NUIdentRvalue and NUMemselRvalue) to the
          mIdentRvalueReplacements map, this map is then used by an IdentToMemselCallback as we finish up a module to replace each of the
          original NUIdent{L,R}value expressions with the new NUMemsel{L,R}value.
 */

class AddImplicitUnpackedDimensionCallback : public NUDesignCallback {
typedef UtMap<const NUExpr *, NUExpr*> NUIdentRvalReplacementMap;
typedef UtMap<const NULvalue *, NULvalue*> NUIdentLvalReplacementMap; // NUIdentLvalue::NUMemselLvalue or NUIdentLvalue::NUIdentLvalue
typedef UtStack<NUIdentRvalReplacementMap*> NUIdentRvalReplacementMapStack;
typedef UtStack<NUIdentLvalReplacementMap*> NUIdentLvalReplacementMapStack;

public:
  AddImplicitUnpackedDimensionCallback( MsgContext *msgContext )
    : mPass(0)
      , mMsgContext( msgContext )
      , mIdentRvalueReplacements(NULL)
      , mIdentLvalueReplacements(NULL)
  {}

  ~AddImplicitUnpackedDimensionCallback() {}

  void setPass(UInt32 val) { mPass = val; }

  //! By default, walk through everything.
  Status operator() (Phase, NUBase* ) { return eNormal; }

  Status operator()(Phase phase, NUNet* id) {
    if ( (1 == mPass ) && (phase == ePre && id->isMemoryNet() )) {
      NUMemoryNet *mem = id->getMemoryNet();
      if ( mem && !mem->isResynthesized() )
      {
        UInt32 numDims = mem->getNumDims();
        const UInt32 firstUnpackedDimensionIdx = mem->getFirstUnpackedDimensionIndex();
        // firstUnpackedDimensionIdx could be 0 if a VHDL composite net, these have no packed dims, and by the time this code is executed they also have no dimensions
        bool declaredWithPackedDimsOnly = ((firstUnpackedDimensionIdx != 0 ) && (firstUnpackedDimensionIdx >= numDims));
        
        if ( declaredWithPackedDimsOnly && (numDims > 1 ) ){

          // this memory was declared with 2 or more packed dimensions.  Here we add a single unpacked dimension 
          mem->appendRange(ConstantRange(0,0));
          mem->setImplicitUnpackedRangeAdded(true);

#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for NUMemoryNet that required the addition of an implicit unpacked dimension [0:0]):\n";
          mem->print(1,1);
#endif
        }
      }
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUMemselLvalue* lval) {
    if ( ( 2 == mPass ) && (( phase == ePre ) &&  (!lval->isResynthesized())) ) {
        NUMemoryNet *mem = lval->getIdent()->getMemoryNet();
        NU_ASSERT( mem, lval );
        const SInt32 numMemDims = mem->getNumDims();
        const SInt32 maxMemDimensionIndex = numMemDims - 1; // index of last dimension for memory
        const SInt32 firstUnpackedDimensionIdx = (SInt32) mem->getFirstUnpackedDimensionIndex();
        if ( mem->isMemHaveImplicitUnpackedRange() && ( maxMemDimensionIndex == firstUnpackedDimensionIdx)  ) {
          // this is a NUMemselLvalue for a memory that had the implicit unpacked dimension added, inject the index expression for it
#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call before adding implicit unpacked dimension for NUMemselLvalue:\n";
          lval->print(1,1);
#endif
          const SourceLocator &loc = lval->getLoc();
          // insert an index expression for the implicit [0:0] dimension 
          NUExpr* implicitDimExpr = NUConst::create( true, 0, 32, loc );
          lval->prependIndexExpr(implicitDimExpr);

          // remember not to process the NUIdentLval under this NUMemselLvalue, but we cannnot return eSkip because there might be another
          // NUMemselLvalue in the index expression for this one.
          NULvalue* lvalue = lval->getIdentLvalue();
          if ( lvalue->getType() == eNUIdentLvalue ) {
            NUIdentLvalue* idLval = static_cast<NUIdentLvalue*>(lvalue);
            if ( idLval ){
              (*mIdentLvalueReplacements)[idLval] = idLval; // remember this identLvalue so we don't process it while traversing lval
            }
          }

#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for adding implicit unpacked dimension after injection of dimension:\n";
          retval->print(1,1);
#endif  

        }
      }
    return eNormal;
  }

  Status operator()(Phase phase, NUMemselRvalue* rval) {
    if ( ( 2 == mPass ) && (( phase == ePre ) &&  (!rval->isResynthesized())) ) {
        NUMemoryNet *mem = rval->getIdent()->getMemoryNet();
        NU_ASSERT( mem, rval );
        const SInt32 numMemDims = mem->getNumDims();
        const SInt32 maxMemDimensionIndex = numMemDims - 1; // index of last dimension for memory
        const SInt32 firstUnpackedDimensionIdx = (SInt32) mem->getFirstUnpackedDimensionIndex();
        if ( mem->isMemHaveImplicitUnpackedRange() && ( maxMemDimensionIndex == firstUnpackedDimensionIdx)  ) {
          // this is a NUMemselRvalue for a memory that had the implicit unpacked dimension added, inject the index expression for it
#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call before adding implicit unpacked dimension for NUMemselRvalue:\n";
          rval->print(1,1);
#endif
          const SourceLocator &loc = rval->getLoc();
          // insert an index expression for the implicit [0:0] dimension 
          NUExpr* implicitDimExpr = NUConst::create( true, 0, 32, loc );
          rval->prependIndexExpr(implicitDimExpr);

          // remember not to process the NUIdentRval under this NUMemselRvalue, but we cannnot return eSkip because there might be another
          // NUMemselRvalue in the index expression for this one.
          NUExpr* idRval = rval->getIdentExpr();
          if ( idRval && (idRval->getType() == NUExpr::eNUIdentRvalue ) ){
            (*mIdentRvalueReplacements)[idRval] = idRval; // remember this identRvalue so we don't process it while traversing rval
          }

#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for adding implicit unpacked dimension after injection of dimension:\n";
          retval->print(1,1);
#endif  

        }
      }
    return eNormal;
  }

  Status operator()(Phase phase, NUIdentRvalue* idRval) {
    if ( ( 2 == mPass ) && ( phase == ePre ) ) {
      NUNet* net = idRval->getIdent();
      if ( net->isMemoryNet() ) {
        NUMemoryNet *mem = net->getMemoryNet();
        NU_ASSERT(mem, idRval );
        // only process memories that had an implicit unpacked dimension added,
        // Note that some NUIdentRvalues (those that are part of the definition of a NUMemsel{L,R}value) were entered into mIdentRvalueReplacements so that we can skip them here
        if ( !mem->isResynthesized()
             && mem->isMemHaveImplicitUnpackedRange()
             && (mIdentRvalueReplacements->find( idRval ) == mIdentRvalueReplacements->end())) {
          
          const SourceLocator &loc = idRval->getLoc();
          CopyContext cc (0,0);

#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for NUIdentRvalue (for mem that had implicit unpacked range added):\n";
          idRval->print(1,1);
#endif
          // rewrite any full memory reference to a memory that had an implicit unpacked range added to it, as a NUMemselRvalue for that single word
          NUExpr* implicitDimExpr = NUConst::create( true, 0, 32, loc );
          NUMemselRvalue *newMemsel = new NUMemselRvalue( mem, implicitDimExpr, loc );
          newMemsel->resize( newMemsel->determineBitSize() );
          NU_ASSERT(( mIdentRvalueReplacements->find( idRval ) == mIdentRvalueReplacements->end() ), idRval );
          // and remember this new expression so we can replace the current NUIdentRvalue with it
          (*mIdentRvalueReplacements)[idRval] = newMemsel; // really a NUExprToExpr map, newMemsel now belongs to mIdentRvalueReplacements

#if MEMRESYNTH_DEBUG
          UtIO::cout() << "NUIdentRvalue->NUMemselRvalue after resynthesis:\n";
          newMemsel->print(1,1);
#endif
          // return eDelete;       // if eDelete were supported for NUDesignWalker::expr then we would use eDelete here
        }
      }
    }
    return eNormal;
   }

  Status operator()(Phase phase, NUIdentLvalue* idLval) {
    if ( ( 2 == mPass ) && ( phase == ePre ) ) {
      NUNet* net = idLval->getIdent();
      if ( net->isMemoryNet() ) {
        NUMemoryNet *mem = net->getMemoryNet();
        NU_ASSERT(mem, idLval );
        // only process memories that had an implicit unpacked dimension added,
        // Note that some NUIdentLvalues (those that are part of the definition of a NUMemsel{L,R}value) were entered into mIdentLvalueReplacements so that we can skip them here
        if ( !mem->isResynthesized()
             && mem->isMemHaveImplicitUnpackedRange()
             && (mIdentLvalueReplacements->find( idLval ) == mIdentLvalueReplacements->end())) {
          
          const SourceLocator &loc = idLval->getLoc();
          CopyContext cc (0,0);

#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for NUIdentLvalue (for mem that had implicit unpacked range added):\n";
          idLval->print(1,1);
#endif
          // rewrite any full memory reference to a memory that had an implicit unpacked range added to it, as a NUMemselLvalue for that single word
          NUExpr* implicitDimExpr = NUConst::create( true, 0, 32, loc );
          NUMemselLvalue *newMemsel = new NUMemselLvalue( mem, implicitDimExpr, loc );
          newMemsel->resize();
          NU_ASSERT(( mIdentLvalueReplacements->find( idLval ) == mIdentLvalueReplacements->end() ), idLval );
          // and remember this new expression so we can replace the current NUIdentLvalue with it
          (*mIdentLvalueReplacements)[idLval] = newMemsel; // really a NUIdentLvalue to NUMemselLvalue map, newMemsel now belongs to mIdentLvalueReplacements

#if MEMRESYNTH_DEBUG
          UtIO::cout() << "NUIdentLvalue->NUMemselLvalue after resynthesis:\n";
          newMemsel->print(1,1);
#endif
          // return eDelete;       // if eDelete were supported for NUDesignWalker::expr then we would use eDelete here
        }
      }
    }
    return eNormal;
   }

  // the following code is included as an example of what needs to be done if we add support for eDeletef for NUDesignWalker::expr
//   NUExpr* replacement( NUExpr *orig )
//   {
//     NUIdentRvalReplacementMap::iterator iter = mIdentRvalueReplacements->find( orig );
//     if ( iter != mIdentRvalueReplacements->end( )) {
//       NUExpr* newExpr =  iter->second;
//       iter->second = NULL;    // remove pointer to newExpr to make sure it is used only once.

//       // if newExpr != orig then this is a valid replacement, if newExpr == orig then it just means that this particular NUIdentRvalue is not to be processed
//       if ( newExpr != orig ){
//         return newExpr;
//       }
//     }
//     return NULL;
//   }





  Status operator()(Phase phase, NUTF *node) { return changeOfScope(phase, node); }
  Status operator()(Phase phase, NUBlock *node) { return changeOfScope(phase, node); }

  Status operator()(Phase phase, NUModule *node) {

    switch( phase ) {
    case ePre: {
      mIdentRvalueReplacements = new NUIdentRvalReplacementMap();
      mIdentRvalueReplacementsStack.push(mIdentRvalueReplacements);
      mIdentLvalueReplacements = new NUIdentLvalReplacementMap();
      mIdentLvalueReplacementsStack.push(mIdentLvalueReplacements);
      break;
    }
    case ePost: {
      // before leaving this module handle any expression or lvalue replacements that were identified as needing a new expression/lvalue.
      IdentToMemselCallback translator(mIdentRvalueReplacements, mIdentLvalueReplacements); // a NuToNuFn that will do the replacements.
      node->replaceLeaves(translator); // change any of the NUExprs or NUIdentLvalues  in this module that were recognized as needing rewrite

      // now make sure that mIdentRvalueReplacements is now nulled out, indicating that all replacements happened
      for ( NUIdentRvalReplacementMap::iterator iter = mIdentRvalueReplacements->begin(); iter != mIdentRvalueReplacements->end(); ++iter){
        NU_ASSERT((NULL == iter->second), iter->first);
      }
      delete mIdentRvalueReplacements;
      mIdentRvalueReplacementsStack.pop();
      mIdentRvalueReplacements = ( (mIdentRvalueReplacementsStack.empty()) ? NULL: mIdentRvalueReplacementsStack.top());

      // now make sure that mIdentLvalueReplacements is now nulled out, indicating that all replacements happened
      for ( NUIdentLvalReplacementMap::iterator iter = mIdentLvalueReplacements->begin(); iter != mIdentLvalueReplacements->end(); ++iter){
        NU_ASSERT((NULL == iter->second), iter->first);
      }
      delete mIdentLvalueReplacements;
      mIdentLvalueReplacementsStack.pop();
      mIdentLvalueReplacements = ( (mIdentLvalueReplacementsStack.empty()) ? NULL: mIdentLvalueReplacementsStack.top());
      break;
    }
    }

    return changeOfScope(phase, node);
  }



private:
  // common method for maintaining values on mScopeStack
  Status changeOfScope(Phase phase, NUScope *node){
    switch( phase ) {
    case ePre:  mScopeStack.push( node ); break;
    case ePost: mScopeStack.pop();        break;
    }
    return eNormal;
  }

private:

  UInt32 mPass;
  MsgContext *mMsgContext;
  NUScopeStack mScopeStack;
  NUIdentRvalReplacementMap* mIdentRvalueReplacements; // current replacement map, if .first == .second then not a replacement, but an indication that this NUExpr does not need to have an index addded to it
  NUIdentRvalReplacementMapStack mIdentRvalueReplacementsStack;

  NUIdentLvalReplacementMap* mIdentLvalueReplacements; // current replacement map for Lvals, if .first == .second then not a replacement, but indicates that this NUExpr does not need to have an index added to it
  NUIdentLvalReplacementMapStack mIdentLvalueReplacementsStack;





  // a simple NuToNuFn translator used replace selected expressions or lvalues (defined by mIdentRvalueReplacements, and mIdentLvalueReplacements)
  class IdentToMemselCallback : public NuToNuFn
  {
  public:
    //! constructor
    IdentToMemselCallback(NUIdentRvalReplacementMap* replacementRvalueMap, NUIdentLvalReplacementMap* replacementLvalueMap) :
      mIdentRvalueReplacements(replacementRvalueMap),
      mIdentLvalueReplacements(replacementLvalueMap)
    {}

    //! Overload the expr to expr call back to do the work for NUIdentRvalue->NUMemselRvalue
    // This assumes that each key entry in mIdentRvalueReplacements will only appear once in the design,
    // and that the replacement (.second) was new'ed before being placed in the map or that .second == .first.
    // if .second == .first then that means the expression is one that is not modified but can be skipped.
    NUExpr* operator()(NUExpr* expr, Phase phase)
    {
      if (phase == ePost) {
        NUIdentRvalReplacementMap::iterator iter = mIdentRvalueReplacements->find( expr );
        if ( iter != mIdentRvalueReplacements->end( )) {
          NUExpr* newExpr =  iter->second;
          NU_ASSERT(newExpr, expr);

          iter->second = NULL;  // null our pointer to make sure we don't try to use it a second time.

          if ( newExpr == expr ) {
            return NULL;       // this is just an entry that indicated that expr was to be skipped
          } else {
            delete expr;          // delete original as this is the last pointer to it that exists,
            return newExpr;       // ownership of the new expression is passed to the caller of this method,
          }
        }
      } 
      return NULL;
    }
    
    NULvalue* operator()(NULvalue* lval, Phase phase){
      if (phase == ePost) {
        NUIdentLvalReplacementMap::iterator iter = mIdentLvalueReplacements->find( lval );
        if ( iter != mIdentLvalueReplacements->end( )) {
          NULvalue* newLval =  iter->second;
          NU_ASSERT(newLval, lval);

          iter->second = NULL;  // null our pointer to make sure we don't try to use it a second time.

          if ( newLval == lval ) {
            return NULL;       // this is just an entry that indicated that lval was to be skipped
          } else {
            delete lval;          // delete original as this is the last pointer to it that exists,
            return newLval;       // ownership of the new expression is passed to the caller of this method,
          }
        }
      }
      return NULL;
    }

  private:
    NUIdentRvalReplacementMap *mIdentRvalueReplacements;
    NUIdentLvalReplacementMap *mIdentLvalueReplacements;
  };
};

/*!
The MemoryResynthCallback class is phase 1 of memory resynthesis.  It is
used as a callback for a NUDesignWalker.  The purpose of this is to:
  - rewrite all memory declarations of a 3+dimension memory into the
    canonical, 2-d form.
  - rewrite declarations of memories with multiple packed dimensions,
    into an equivalent form with a single packed dimension. 
  - lowers port actuals that are not NUIdentRvalues (for input ports)
    or NUIdentLvalues (for output and inout ports) when they are being
    assigned to a memory.
*/
class MemoryResynthCallback : public NUDesignCallback {
public:
  MemoryResynthCallback( MsgContext *msgContext )
    : mMsgContext( msgContext )
  {}

  ~MemoryResynthCallback() {}

  //! By default, walk through everything.
  Status operator() (Phase, NUBase* ) { return eNormal; }

  Status operator()(Phase phase, NUNet* id) {
    if (phase == ePre && id->isMemoryNet() ) {
      NUMemoryNet *mem = id->getMemoryNet();
      if ( mem && !mem->isResynthesized() )
      {
        UInt32 numDims = mem->getNumDims();

        UInt32 newDepth = 1;
        UInt32 newWidth = 1;
          

        if ( numDims > 2 )
        {
#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for NUMemoryNet:\n";
          mem->print(1,1);
#endif
          
          UInt32 firstUnpackedDim = mem->getFirstUnpackedDimensionIndex();
          // calculate the simplified memory depth
          for ( UInt32 i = firstUnpackedDim; i < numDims; ++i ) {
            newDepth *= mem->getRangeSize( i );
          }
          mem->putMemoryDepth( newDepth - 1, 0 );

          // calculate the simplified memory width
          for ( UInt32 ii = 0;  ii < firstUnpackedDim;  ++ii ) {
            newWidth *= mem->getRangeSize( ii );
          }
          mem->putMemoryWidth( newWidth - 1, 0 );

        }

        // Defer setting the resynthesized flag on the memories
        // until after all expressions have been resynthesized.
        // Otherwise the expression-resynthesis is going to be
        // get the post-resynth determineBitSize on NUMemselRvalues.
        mDeferResynthNets.push_back(mem);

#if MEMRESYNTH_DEBUG
        if ( numDims > 2 )
        {
          // in the following debug line we use the calculated newWidth/newDepth
          // because we have not marked the memory as being resynthesized
          // so mem->getWidthRange() and mem->getDepthRange() will return
          // the original ranges, not the resynthesized ranges
          UtIO::cout() << "NUMemoryNet after resynthesis width[" << (newWidth - 1) << ":0]  depth[" << (newDepth - 1) << ":0] :\n";
          mem->print(1,1);
          
        }
#endif
      }
    }
    return eNormal;
  }

  // mark all NUMemoryNets added to mDeferResynthNets as now having been resynthesized
  void resynthDeferredNets() {
    for (UInt32 i = 0; i < mDeferResynthNets.size(); ++i) {
      NUMemoryNet* mem = mDeferResynthNets[i];
      mem->putResynthesized(true);
    }
  }

  //! Lower memory actuals when they are not a NUIdentRvalue
  //! This means that the port, which by necessity a memory, is
  //! connect to something other than a memory, like a concat.
  //! In that case, the actual will be replaced by a net of the
  //! same type as the formal. That net in turn will be the LHS
  //! of an assign driven by the original actual expression.
  //! WARNING: If a change is required in this code, it may
  //! also be required in ()(Phase, NUPortConnectionOutput*)
  //! and ()(Phase, NUPortConnectionBid*)
  Status operator()(Phase phase, NUPortConnectionInput *node)
  {
    if (phase == ePre) {
      NUExpr *actual = node->getActual();
      NUNet *formal = node->getFormal();
      NUMemoryNet *mem = formal->getMemoryNet();
      if ( mem && actual && (NUExpr::eNUIdentRvalue != actual->getType()))
      {
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "Resynth call for input port connection:\n";
        node->print(1,1);
#endif
        NUScope* parent = node->getModuleInstance()->getParentModule();
        StringAtom *tempName = parent->gensym( "mr_lower_in", NULL, actual );
        NUMemoryNet *tempNet = cloneMemory(phase, mem, tempName, parent );
        const SourceLocator &loc = node->getLoc();
        NULvalue *tempLval = new NUIdentLvalue( tempNet, loc );
        StringAtom *blockName = parent->newBlockName( parent->getIODB()->getAtomicCache(),
                                                      loc );
        NUContAssign *assign = new NUContAssign( blockName, tempLval, actual, loc );
        NUIdentRvalue *tempRval = new NUIdentRvalue( tempNet, loc );
        NUModule* mod = dynamic_cast<NUModule*>(parent);
        NU_ASSERT(mod, node->getModuleInstance());
        mod->addLocal( tempNet );
        mod->addContAssign( assign );
        node->setActual( tempRval );
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "NUPortConnectionInput created assign:\n";
        assign->print(1,1);
        UtIO::cout() << "NUPortConnectionInput after resynthesis:\n";
        node->print(1,1);
#endif
      }
    }
    return eNormal;
  }


  //! Lower memory actuals when they are not a NUIdentLvalue
  //! What this means is that the port is connected to something
  //! other than a memory, which means it needs to be lowered.
  //! That in turns means that orignal actual will be replaced 
  //! by a net of the same type as the formal. The
  //! net then becomes the RHS of an assign which drives the
  //! original actual. See bug 11624 for more details.
  //! WARNING: If a change is required in this code, it may
  //! also be required in ()(Phase, NUPortConnectionOutput*)
  //! and ()(Phase, NUPortConnectionBid*)
  Status operator()(Phase phase, NUPortConnectionOutput *node)
  {
    if (phase == ePre) {
      NULvalue *actual = node->getActual();
      NUNet *formal = node->getFormal();
      NUMemoryNet *mem = formal->getMemoryNet();
      if (mem && actual && (eNUIdentLvalue != actual->getType())) 
      {
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "Resynth call for output port connection:\n";
        node->print(1,1);
#endif
        NUScope *parent = node->getModuleInstance()->getParentModule();
        StringAtom *tempName = parent->gensym( "mr_lower_out", NULL, actual );
        NUMemoryNet *tempNet = cloneMemory(phase, mem, tempName, parent );
        const SourceLocator &loc = node->getLoc();
        NUExpr *tempRval = new NUIdentRvalue( tempNet, loc );
        StringAtom *blockName = parent->newBlockName( parent->getIODB()->getAtomicCache(),
                                                      loc );
        NUContAssign *assign = new NUContAssign( blockName, actual, tempRval, loc );
        NUIdentLvalue *tempLval = new NUIdentLvalue( tempNet, loc );
        NUModule* mod = dynamic_cast<NUModule*>(parent);
        NU_ASSERT(mod, node->getModuleInstance());
        mod->addLocal( tempNet );
        mod->addContAssign( assign );
        node->setActual( tempLval );
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "NUPortConnectionOutput created assign:\n";
        assign->print(1,1);
        UtIO::cout() << "NUPortConnectionOutput after resynthesis:\n";
        node->print(1,1);
#endif
      }
    }
    return eNormal;
  }

  //! If an inout port is connected to anything but a memory,
  //! error out because the code is not setup to handle this
  //! situation.
  Status operator()(Phase phase, NUPortConnectionBid *node)
  {
    if (phase == ePre) {
      NULvalue *actual = node->getActual();
      NUNet *formal = node->getFormal();
      NUMemoryNet *mem = formal->getMemoryNet();
      if ( mem && actual && (eNUIdentLvalue != actual->getType()))
      {
        // This is not supported for bidirectional ports
        mMsgContext->ComplexBid( actual );
        return eStop;
      }
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUTF *node)
  {
    switch( phase )
    {
    case ePre:
      mScopeStack.push( node );
      break;
    case ePost:
      mScopeStack.pop();
      break;
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUModule *node)
  {
    switch( phase )
    {
    case ePre:
      mScopeStack.push( node );
      break;
    case ePost:
      mScopeStack.pop();
      break;
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUBlock *node)
  {
    switch( phase )
    {
    case ePre:
      mScopeStack.push( node );
      break;
    case ePost:
      mScopeStack.pop();
      break;
    }
    return eNormal;
  }

  Status operator()(Phase, NUTFArgConnectionBid *node)
  {
    NUMemoryNet *mem = node->getFormal()->getMemoryNet();
    NULvalue *actual = node->getActual();
    if ( mem && !actual->isWholeIdentifier() )
    {
      // This is not supported for bidirectional ports
      mMsgContext->ComplexBid( node->getActual() );
      return eStop;
    }
    return eNormal;
  }


  Status operator()( Phase phase, NUTFArgConnectionInput *node )
  {
    if ( phase == ePre )
    {
      NUNet *formal = node->getFormal();
      NUMemoryNet *mem = formal->getMemoryNet();
      NUExpr *actual = node->getActual();
      // If actual is not a whole identifer and formal is a memory, temp
      // the memory.
      if ( mem && !actual->isWholeIdentifier() )
      {
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "Resynth call for TF input port connection:\n";
        node->print(1,1);
#endif
        NUScope *parent = mScopeStack.top();
        if ( parent->inTFHier())
        {
          parent = parent->getTFScope();
        }
        else
        {
          parent = parent->getModule();
        }
        StringAtom *tempName = parent->gensym( "mrtf_lower", NULL, actual );
        NUMemoryNet *tempNet = cloneMemory(phase, mem, tempName, parent );
        const SourceLocator &loc = node->getLoc();
        NULvalue *tempLval = new NUIdentLvalue( tempNet, loc );
        NUBlockingAssign *assign = new NUBlockingAssign( tempLval, actual, false, loc );
        NUIdentRvalue *tempRval = new NUIdentRvalue( tempNet, loc );
        parent->addLocal( tempNet );
        mPreStmts.push_back( assign );
        node->setActual( tempRval );
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "NUTFArgConnectionInput created assign:\n";
        assign->print(1,1);
        UtIO::cout() << "NUTFArgConnectionInput after resynthesis:\n";
        node->print(1,1);
#endif
      }
    }
    return eNormal;
  }
   

  Status operator()( Phase phase, NUTFArgConnectionOutput *node )
  {
    if ( phase == ePre )
    {
      NUNet *formal = node->getFormal();
      NUMemoryNet *mem = formal->getMemoryNet();
      NULvalue *actual = node->getActual();
      // If actual is not a whole identifer and formal is a memory, temp
      // the memory.
      if ( mem && !actual->isWholeIdentifier() )
      {
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "Resynth call for TF output port connection:\n";
        node->print(1,1);
#endif
        NUScope *parent = mScopeStack.top();
        if ( parent->inTFHier())
        {
          parent = parent->getTFScope();
        }
        else
        {
          parent = parent->getModule();
        }
        StringAtom *tempName = parent->gensym( "mrtf_lower", NULL, actual );
        NUMemoryNet *tempNet = cloneMemory(phase, mem, tempName, parent );
        const SourceLocator &loc = node->getLoc();
        NUExpr *tempRval = new NUIdentRvalue( tempNet, loc );
        NUBlockingAssign *assign = new NUBlockingAssign( actual, tempRval, false, loc );
        NUIdentLvalue *tempLval = new NUIdentLvalue( tempNet, loc );
        parent->addLocal( tempNet );
        mPostStmts.push_back( assign );
        node->setActual( tempLval );
#if MEMRESYNTH_DEBUG
        UtIO::cout() << "NUTFArgConnectionOutput created assign:\n";
        assign->print(1,1);
        UtIO::cout() << "NUTFArgConnectionOutput after resynthesis:\n";
        node->print(1,1);
#endif
      }
    }
    return eNormal;
  }


  Status operator()( Phase phase, NUTaskEnable *node )
  {
    if ( phase == ePre )
    {
      NU_ASSERT( mPreStmts.empty(), node );
      NU_ASSERT( mPostStmts.empty(), node );
    }
    else if ( phase == ePost )
    {
      // If this task enable is hierarchically referenced, do not
      // recursively process called tasks. During normal processing,
      // they will be processed independently.
      if ( node->isHierRef() )
      {
        return eSkip;
      }

      NU_ASSERT(( mStmtReplacements.find( node ) == mStmtReplacements.end() ), node );
      NUStmt *new_stmt = rewriteTaskEnable( node, mScopeStack.top( ));
      if ( new_stmt )
      {
        mStmtReplacements[node] = new_stmt;
        return eDelete;
      }
    }
    return eNormal;
  }

  NUStmt* replacement( NUStmt *orig )
  {
    NUStmtStmtMap::iterator iter = mStmtReplacements.find( orig );
    if ( iter != mStmtReplacements.end( ))
    {
      NUStmt *stmt = iter->second;

      // Erase this map element just in case someone else gets the
      // memory used by 'orig' on a subsequent allocation.
      mStmtReplacements.erase( iter );
      return stmt;
    }
    else
    {
      return NULL;
    }
  }

private:

  NUStmt *rewriteTaskEnable( NUTaskEnable *node, NUScope *scope )
  {
    NUStmt *new_stmt = NULL;
    if ( mPreStmts.empty() && mPostStmts.empty() )
    {
      new_stmt = NULL;
    }
    else
    {
      switch( scope->getScopeType() )
      {
      case NUScope::eBlock:
      case NUScope::eTask:
      {
        // Create the new statement list with prestmts, a copy of the task
        // enable, and the poststmts
        NUStmtList blockStmts( mPreStmts );
        CopyContext cc = CopyContext( 0, 0 );
        blockStmts.push_back( node->copy( cc ));
        blockStmts.insert( blockStmts.end(), mPostStmts.begin(), mPostStmts.end() );

        mPreStmts.clear();
        mPostStmts.clear();

        // Create a new block
        const SourceLocator &loc = node->getLoc();
        new_stmt = new NUBlock( &blockStmts, mScopeStack.top(), mScopeStack.top()->getIODB(),
                                node->getNetRefFactory(), false, loc );
        break;
      }
      case NUScope::eModule:
      case NUScope::eNamedDeclarationScope:
      case NUScope::eUserTask:
        new_stmt = NULL;
        break;
      }
    }

    return new_stmt;
  }

  //! Clone a memory, given a new name and a scope to put it into
  NUMemoryNet *cloneMemory( Phase phase, const NUMemoryNet *mem, StringAtom *tempName,
                            NUScope *scope )
  {
    ConstantRangeArray range_array;
    const UInt32 numDims = mem->getNumDims();
    const UInt32 firstUnpackedDimension = mem->getFirstUnpackedDimensionIndex();
    const bool implicitUnpackedRangeAdded = mem->isMemHaveImplicitUnpackedRange();
    for ( UInt32 i = 0; i < numDims; ++i )
    {
      ConstantRange *range = new ConstantRange( mem->getRange(i)->getMsb(),
                                                mem->getRange(i)->getLsb());
      range_array.push_back( range );
    }
    NUMemoryNet *tempNet = new NUMemoryNet( tempName, firstUnpackedDimension, &range_array, mem->getFlags(),
                                            mem->getVectorNetFlags (),
                                            scope, mem->getLoc() );
    tempNet->setImplicitUnpackedRangeAdded(implicitUnpackedRangeAdded);

    // Make it a wire for initialization purposes
    NetFlags flags = NetRedeclare( tempNet->getFlags(), eDMWire2DNet );
    // This is not a port; clear the port flag and make it a temp net
    flags = NetFlags(( flags & ~ePortMask ) | scope->getTempFlags( ));
    tempNet->setFlags( flags );
    // Make sure the memory resynth processing is done on the cloned node.
    // There are cases in which the cloned node is made in a scope (e.g.
    // an entity) which has already been traversed by the MemoryResynthCallback
    // walker, and thus will not get processed unless it is done here.
    (*this)(phase, tempNet);
    return tempNet;
  }

private:
  MsgContext *mMsgContext;
  NUScopeStack mScopeStack;
  NUStmtStmtMap mStmtReplacements;
  NUStmtList mPreStmts, mPostStmts;
  UtArray<NUMemoryNet*> mDeferResynthNets;
};


/*!

The MemselResynthCallback class is phase 2 of memory resynthesis.  It is
used as a callback for a replaceLeaves() in a module.  It rewrites
NUMemsel[LR]value nodes that index a single word in a multidimensional
object to reference the correct word of the resynthesized memory.  If
the memsel selects a m-dimensional subarray out of an n-dimensional net
(m < n), then a concat is created of all the necessary resynthesized
array elements.
*/
class MemselResynthCallback : public NuToNuFn {
public:
  MemselResynthCallback( MsgContext *msgContext ) : mMsgContext( msgContext ) {}

  //! Resynthesize NUMemselLvalues into new NUMemselLvalues (or concats of NUMemselLvalues) that use the index values adjusted for the resynthesized memory range
  /*  Also can resynthesize a NUMemselLvalue into a concat of NUMemselLvalues that use the resynthesized indices
   */
  virtual NULvalue* operator()(NULvalue *node, Phase phase) {
    if ( phase == ePost && node->getType() == eNUMemselLvalue )
    {
      NUMemselLvalue *lval = dynamic_cast<NUMemselLvalue*>( node );
      NU_ASSERT( lval, node );
      if ( !lval->isResynthesized() )
      {
        NUMemoryNet *mem = lval->getIdent()->getMemoryNet();
        NU_ASSERT( mem, lval );
        const SInt32 numMemDims = mem->getNumDims();
        const SInt32 maxMemDimensionIndex = numMemDims - 1; // index of last dimension for memory
        if ( numMemDims > 2 ) {
          // this is a NUMemselLvalue for a memory with more than 2
          // dimensions, it needs to be resynthesized to a simplifiedMemory
#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for NUMemselLvalue:\n";
          lval->print(1,1);
#endif
          const SourceLocator &loc = lval->getLoc();
          CopyContext cc (0,0);

          const SInt32 numLvalDims = lval->getNumDims();
          const SInt32 firstUnpackedDimensionIdx = (SInt32) mem->getFirstUnpackedDimensionIndex();
          const SInt32 numUnpackedMemDims = (numMemDims-firstUnpackedDimensionIdx);
          
          const ResynthType typeOfResynthNeeded = determineTypeOfResynthNeeded(numLvalDims, mem);

          
          // Generate the base index expression for the word in the resynthesized memory,
          // so here we handle the unpacked dimensions only.
          NUExpr *baseIdxExpr = NULL;


          // When we get here we know:
          // maxMemDimensionIndex is always at least 2
          //    (if it were 1 then this memory was already normalized
          //     if it were 0 then it is not a memory)
          // firstUnpackedDimensionIdx is always at least 1
          //   Most memories have at least 1 packed dimension and in the special case where the
          //   memory was written with no packed dimensions, an implicit packed dimension of [0:0]
          //   was added during phase 0 of MemoryResynth

          // Convert the unpacked dimensions specified in lval into a single dimension in terms of the resynthesized memory.
          //  (only the unpacked dimensions are considered here, defined by [maxMemDimensionIndex downto firstUnpackedDimensionIdx]

          // this once was wordConvertCount = std::min((numMemDims - firstUnpackedDimensionIdx), numLvalDims);
          // but this required for eIsSliceOfMemoryWord we needed to adjust baseIdxExpr before using it to do the concat,
          // using (numMemDims - firstUnpackedDimensionIdx) here  means that compressIndexExpressions does this adjustment for us.
          SInt32 wordConvertCount = (numMemDims - firstUnpackedDimensionIdx); 
          baseIdxExpr = compressIndexExpressions(wordConvertCount, lval, 0, mem);

          NULvalue *retval = NULL;
          
          switch ( typeOfResynthNeeded ){
          case eIsWholeOfMemoryWord:
          case eIsPartOfMemoryWord: {
            // create a new NUMemselLvalue to have the new index
            NUMemselLvalue *newlval = new NUMemselLvalue( mem, baseIdxExpr, loc );
            newlval->resize();
            newlval->putResynthesized( true );
            retval = newlval;
            
            if ( eIsWholeOfMemoryWord == typeOfResynthNeeded ){
              break;            // we only needed the word, we are done 
            }

            // when we get here-- need to create an expression for the bit or part select
            const SInt32 startLvalIdx = numUnpackedMemDims;
            const SInt32 partselConvertCount = (numLvalDims - numUnpackedMemDims);
            NUExpr* packedIdxExpr = compressIndexExpressions(partselConvertCount, lval, startLvalIdx, mem);

            // create bit or part select the memsel
            if ( numLvalDims == numMemDims ){
              // bitselect
              retval = new NUVarselLvalue( newlval, packedIdxExpr, loc );
            } else {
              // partselect, of the unmentioned dimensions, calculate the width of the partselect
              SInt32 selWidth = 1;
              for( SInt32 jj = (numMemDims-numLvalDims-1); 0 <= jj ; --jj ) {
                SInt32 dimWidth = mem->getRangeSize(jj);
                selWidth = selWidth * dimWidth;
              }

              NUConst *selWidthExpr = NUConst::create( true, selWidth, 32, loc );
              NUExpr* offsetExpr = new NUBinaryOp( NUOp::eBiUMult, packedIdxExpr, selWidthExpr, loc );
              // don't normalize this expression using index 0, it was already defined for the normalized range of the resynthesized memory,
              ConstantRange range = ConstantRange(selWidth-1, 0);

              retval = new NUVarselLvalue( newlval, offsetExpr, range, loc );
              retval->resize(); // necessary?
            }
            
            break;
          }
          case eIsSliceOfMemoryWord: {
            NULvalueVector concatVector;
            UInt32 concatElems = 1;

            const SInt32 specifiedLvalueDims = lval->getNumDims(); // number of dimensions in the NUMemselLvalue
            // dimsToConcat is the number of unpacked dimensions that are not specified in this NUMemselLvalue
            const SInt32 dimsToConcat = (maxMemDimensionIndex - firstUnpackedDimensionIdx + 1) - specifiedLvalueDims;

            // calculate the number of elements in this concat.
            // It is the product of the widths of the unspecified dimensions of the NUMemselLvalue
            // limited to those that whould refer to unpacked dimensions
            // for a memory declared as:  reg [2:0][1:0] mem [5:0][4:0][3:0];
            // if we have a NUMemselLvalue: mem [5] = ,
            // then we need to concat elements for the [4:0] and [3:0] ranges or a total
            // of 20 elements (each with a width of 3*2 which is defined by the packed dimensions).
            for ( SInt32 i = 0; i < dimsToConcat; ++i ) {
              UInt32 dimSize = mem->getRangeSize( maxMemDimensionIndex - specifiedLvalueDims - i );
              concatElems *= dimSize;
            }

            for ( UInt32 offsetIdx = 0; offsetIdx < concatElems ; ++offsetIdx ) {
              // We already found the base location of this memsel. We
              // now need to generate a memsel for each element iterated
              // across.
              NUExpr *baseCopy = baseIdxExpr->copy( cc );
              NUConst *offsetIdxExpr = NUConst::create( true, offsetIdx, 32, loc );
              NUExpr *concatIdxExpr = new NUBinaryOp( NUOp::eBiMinus, baseCopy, offsetIdxExpr, loc );
              concatIdxExpr->resize( concatIdxExpr->determineBitSize() );
              NUMemselLvalue *concatElem = new NUMemselLvalue( mem, concatIdxExpr, loc );
              concatElem->resize();
              concatElem->putResynthesized( true );
              concatVector.push_back( concatElem );
            }

            delete baseIdxExpr;
            NUConcatLvalue *concat = new NUConcatLvalue( concatVector, loc );
            concat->resize();
            retval = concat;
            break;
          }
          case eIsUndefined: {
            NU_ASSERT( 0, lval );            // undefined type of conversion
            retval = NULL;
            break;
          }
          }
          delete node;
#if MEMRESYNTH_DEBUG
          UtIO::cout() << "NUMemselLvalue after resynthesis:\n";
          retval->print(1,1);
#endif  
          return retval;
        }
        else
        {
          lval->putResynthesized( true );
        }
      }
    }
    return 0;
  }

  //! Resynthesize NUMemselRvalues into new memsels or into concats
  virtual NUExpr* operator()(NUExpr *node, Phase phase) {
    if ( phase == ePost && node->getType() == NUExpr::eNUMemselRvalue )
    {
      NUMemselRvalue *rval = dynamic_cast<NUMemselRvalue*>( node );
      NU_ASSERT( rval, node );
      if ( !rval->isResynthesized() )
      {
        NUMemoryNet *mem = rval->getIdent()->getMemoryNet();
        NU_ASSERT( mem, rval );
        const SInt32 numMemDims = mem->getNumDims();
        const SInt32 maxMemDimensionIndex = numMemDims - 1; // index of last dimension for memory
        if ( numMemDims > 2 ) {
          // this is a NUMemselRvalue for a memory with more than 2
          // dimensions, it needs to be resynthesized to a simplifiedMemory
#if MEMRESYNTH_DEBUG
          UtIO::cout() << "Resynth call for NUMemselRvalue:\n";
          rval->print(1,1);
#endif
          const SourceLocator &loc = rval->getLoc();
          CopyContext cc (0,0);

          const SInt32 numRvalDims = rval->getNumDims();
          const SInt32 firstUnpackedDimensionIdx = (SInt32) mem->getFirstUnpackedDimensionIndex();
          const SInt32 numUnpackedMemDims = (numMemDims-firstUnpackedDimensionIdx);
          
          const ResynthType typeOfResynthNeeded = determineTypeOfResynthNeeded(numRvalDims, mem);

          // Generate the base index expression for the word in the resynthesized memory,
          // so here we handle the unpacked dimensions only.
          NUExpr *baseIdxExpr = NULL;


          // When we get here we know:
          // maxMemDimensionIndex is always at least 2
          //    (if it were 1 then this memory was already normalized
          //     if it were 0 then it is not a memory)
          // firstUnpackedDimensionIdx is always at least 1
          //   Most memories have at least 1 packed dimension and in the special case where the
          //   memory was written with no packed dimensions, an implicit packed dimension of [0:0]
          //   was added during population.

          // Convert the unpacked dimensions specified in rval into a single dimension in terms of the resynthesized memory.
          //  (only the unpacked dimensions are considered here, defined by [maxMemDimensionIndex downto firstUnpackedDimensionIdx]

          SInt32 wordConvertCount = (numMemDims - firstUnpackedDimensionIdx); 
          baseIdxExpr = compressIndexExpressions(wordConvertCount, rval, 0, mem);

          NUExpr *retval = NULL;

          switch ( typeOfResynthNeeded ){
          case eIsWholeOfMemoryWord:
          case eIsPartOfMemoryWord: {
            // create a new NUMemselRvalue to have the new index
            bool was_truncated = false;
            NUMemselRvalue *newrval = new NUMemselRvalue( mem, baseIdxExpr->limitMemselIndexExpr(&was_truncated, mMsgContext,loc), loc );
            if ( was_truncated and mem->isDeclaredWithNegativeIndex() ){
              mMsgContext->NegativeIndicesWithTruncation( mem );
            }
            newrval->resize( newrval->determineBitSize() );
            newrval->putResynthesized( true );
            newrval->setSignedResult (rval->isSignedResult ());
            retval = newrval;
            
            if ( eIsWholeOfMemoryWord == typeOfResynthNeeded ){
              break;            // we only needed the word, we are done 
            }

            // when we get here-- need to create an expression for the bit or part select
            const SInt32 startRvalIdx = numUnpackedMemDims;
            const SInt32 partselConvertCount = (numRvalDims - numUnpackedMemDims);
            NUExpr* packedIdxExpr = compressIndexExpressions(partselConvertCount, rval, startRvalIdx, mem);

            // create bit or part select the memsel
            if ( numRvalDims == numMemDims ){
              // bitselect
              retval = new NUVarselRvalue( newrval, packedIdxExpr, loc );
              retval->resize( 1 ); // This is a bitsel
            } else {
              // partselect, of the unmentioned dimensions, calculate the width of the partselect
              SInt32 selWidth = 1;
              for( SInt32 jj = (numMemDims-numRvalDims-1); 0 <= jj ; --jj ) {
                SInt32 dimWidth = mem->getRangeSize(jj);
                selWidth = selWidth * dimWidth;
              }

              NUConst *selWidthExpr = NUConst::create( true, selWidth, 32, loc );
              NUExpr* offsetExpr = new NUBinaryOp( NUOp::eBiUMult, packedIdxExpr, selWidthExpr, loc );
              // don't normalize this expression using index 0, it was already defined for the normalized range of the resynthesized memory,
              ConstantRange range = ConstantRange(selWidth-1, 0);

              retval = new NUVarselRvalue( newrval, offsetExpr, range, loc );
              retval->resize(selWidth);
            }
            
            break;
          }
          case eIsSliceOfMemoryWord: {
            NUExprVector concatVector;
            UInt32 concatElems = 1;

            const SInt32 specifiedRvalueDims = rval->getNumDims(); // number of dimensions in the NUMemselRvalue
            // dimsToConcat is the number of unpacked dimensions that are not specified in this NUMemselRvalue
            const SInt32 dimsToConcat = (maxMemDimensionIndex - firstUnpackedDimensionIdx + 1) - specifiedRvalueDims;

            // calculate the number of elements in this concat.
            // It is the product of the widths of the unspecified dimensions of the NUMemselRvalue
            // limited to those that whould refer to unpacked dimensions
            // for a memory declared as:  reg [2:0][1:0] mem [5:0][4:0][3:0];
            // if we have a NUMemselRvalue: ... = mem [5] 
            // then we need to concat elements for the [4:0] and [3:0] ranges or a total
            // of 20 elements (each with a width of 3*2 which is defined by the packed dimensions).
            for ( SInt32 i = 0; i < dimsToConcat; ++i ) {
              UInt32 dimSize = mem->getRangeSize( maxMemDimensionIndex - specifiedRvalueDims - i );
              concatElems *= dimSize;
            }

            SInt32 element_width = (rval->getBitSize())/concatElems;
            for ( UInt32 offsetIdx = 0; offsetIdx < concatElems ; ++offsetIdx ) {
              // We already found the base location of this memsel. We
              // now need to generate a memsel for each element iterated
              // across.
              NUExpr *baseCopy = baseIdxExpr->copy( cc );
              NUConst *offsetIdxExpr = NUConst::create( true, offsetIdx, 32, loc );
              NUExpr *concatIdxExpr = new NUBinaryOp( NUOp::eBiMinus, baseCopy, offsetIdxExpr, loc );
              concatIdxExpr->resize( concatIdxExpr->determineBitSize() );
              bool was_truncated = false;
              NUMemselRvalue *concatElem = new NUMemselRvalue( mem, concatIdxExpr->limitMemselIndexExpr(&was_truncated,mMsgContext,loc), loc );
              if ( was_truncated and mem->isDeclaredWithNegativeIndex() ){
                mMsgContext->NegativeIndicesWithTruncation( mem );
              }
              SInt32 temp_size = concatElem->determineBitSize();
              NUExpr* concatExpr;
              if ( temp_size != element_width ){
                NUVarselRvalue* concatRval;
                ConstantRange range(element_width-1, 0);
                concatRval = new NUVarselRvalue( concatElem, range, loc );
                concatRval->setSignedResult(rval->isSignedResult ());
                concatExpr = concatRval;
              } else {
                concatElem->resize( concatElem->determineBitSize() );
                concatElem->setSignedResult (rval->isSignedResult ());
                concatExpr = concatElem;
              }
              concatElem->putResynthesized( true );
              concatVector.push_back( concatExpr );
            }
            delete baseIdxExpr;

            NUConcatOp *concat = new NUConcatOp( concatVector, 1, loc );
            concat->resize( concat->determineBitSize() );
            retval = concat;
            break;
          }
          case eIsUndefined: {
            NU_ASSERT( 0, rval );            // undefined type of conversion
            retval = NULL;
            break;
          }
          }
          delete node;
#if MEMRESYNTH_DEBUG
          UtIO::cout() << "NUMemselRvalue after resynthesis:\n";
          retval->print(1,1);
#endif  
          return retval;
        }
        else
        {
          rval->putResynthesized( true );
        }
      }
    }
    return 0;
  }


private:

  //! This converts a set of dimensions from \a lval into a compressed range based on the range information from \a mem
  /*!
    only \a numToCompress dimensions from lval are converted.
    The first dimension to be converted is \a startLvalIdx (those before this index are skipped),
    so the range of indices: [startLvalIdx : startLvalIdx+(numToCompress-1)] is converted.

    The order of dimensions in lval are from the slowest changing (index 0) to the fastest changing (lval->getNumDims()-1).
    This method assumes (relies upon) that the order of dimensions in \a mem are from the fastest changing (index 0) to the slowest changing (mem->getNumDims()-1).
    This method aligns the slowest lval index with the slowest mem index.

    convert the dimensions in lval, in the range startLvalIdx:endLvalIdx, into a compressed range for the range of mem indices in the range startMemIdx:endMemIdx

    * requirements:
    *  startLvalIdx  <= number of dimension indices in lval
    *
    */

  NUExpr * compressIndexExpressions( SInt32 numToCompress, NUMemselLvalue *lval, SInt32 startLvalIdx, NUMemoryNet *mem) {
    CopyContext cc (0,0);  
    const SourceLocator &loc = lval->getLoc();
    const SInt32 numLvalDims = lval->getNumDims();
    const SInt32 endLvalIdx = numLvalDims-1;
    const SInt32 numMemDims = mem->getNumDims();
    const SInt32 maxMemIdx = numMemDims -1;
    NUExpr *returnIdxExpr = NULL;

    NU_ASSERT ( (0 < numToCompress), lval ); // caller should know not to try and convert less than 1 index
    NU_ASSERT ( (startLvalIdx  <= endLvalIdx), lval);
    NU_ASSERT ( numLvalDims <= numMemDims, mem); // something is wrong if the number of indices in lval is more than the number in the memory

    SInt32 memIdx = maxMemIdx;
    SInt32 lvalIdx = 0;
    for ( SInt32 ii = 0; ii < startLvalIdx; ++ii ){
      // count lvalIdx up and memIdx down (keeping them in sync) to get to the position of the first index to compress
      ++lvalIdx;
      --memIdx;
    }

    for ( SInt32 dimensionCounter = 0; dimensionCounter < numToCompress; --memIdx, ++lvalIdx, ++dimensionCounter ) {
      NU_ASSERT((memIdx >= 0), mem);
      NUExpr* normIdxExpr = NULL;

      // Create the normalized select expression
      if ( lvalIdx <= endLvalIdx ) {
        // index was specified in lVal for this dimension
        // note retNullForOutOfRange is false in the following call to normalizeSelectExpr, to handle situation where user has demoted the Alert for out-of-range index
        normIdxExpr = mem->normalizeSelectExpr( lval->getIndex(lvalIdx)->copy( cc ), memIdx, 0, false );
      } else {
        // unspecified index for this dimension, use full size for this index position
        normIdxExpr = NUConst::create( true, mem->getOperationalRange(memIdx).getMsb(), 32, loc );
      }
      if ( NULL == normIdxExpr ){
        NU_ASSERT( normIdxExpr, lval);        // this means that there was an incorrect index
      }
      // Create the next level of the new index expression
      if ( NULL == returnIdxExpr ) {
        returnIdxExpr = normIdxExpr;              // first index to be handled, save for use in next iteration of this loop
      } else {
        // multiply the index computed so far by the size of the next range
        NU_ASSERT( (( maxMemIdx - lvalIdx ) == memIdx), lval);
        UInt32 memDimLength = mem->getRangeSize( maxMemIdx - lvalIdx ); // if the above assert never fails then assert should be removed and this should be mem->getRangeSize( memIdx )
        NUConst *multSize = NUConst::create( true, memDimLength, 32, loc );
        NUExpr* multExpr = new NUBinaryOp( NUOp::eBiUMult, returnIdxExpr, multSize, loc );
        returnIdxExpr = new NUBinaryOp( NUOp::eBiPlus, multExpr, normIdxExpr, loc );
      }
    }
    returnIdxExpr->resize( returnIdxExpr->determineBitSize() ); // returnIdxExpr is an integer
    return returnIdxExpr;
  }


  //! this is the rval version of the compressIndexExpressions method (see the lval version above).
  // Most of these two should be combined into a single shared method (requries that the
  // shared method get passed an array of index expressions from lval or rval.
  NUExpr * compressIndexExpressions( SInt32 numToCompress, NUMemselRvalue *rval, SInt32 startRvalIdx, NUMemoryNet *mem) {
    CopyContext cc (0,0);  
    const SourceLocator &loc = rval->getLoc();
    const SInt32 numRvalDims = rval->getNumDims();
    const SInt32 endRvalIdx = numRvalDims-1;
    const SInt32 numMemDims = mem->getNumDims();
    const SInt32 maxMemIdx = numMemDims -1;
    NUExpr *returnIdxExpr = NULL;

    NU_ASSERT ( (0 < numToCompress), rval ); // caller should know not to try and convert less than 1 index
    NU_ASSERT ( (startRvalIdx  <= endRvalIdx), rval);
    NU_ASSERT ( numRvalDims <= numMemDims, mem); // something is wrong if the number of indices in rval is more than the number in the memory

    SInt32 memIdx = maxMemIdx;
    SInt32 rvalIdx = 0;
    for ( SInt32 ii = 0; ii < startRvalIdx; ++ii ){
      // count rvalIdx up and memIdx down (keeping them in sync) to get to the position of the first index to compress
      ++rvalIdx;
      --memIdx;
    }

    for ( SInt32 dimensionCounter = 0; dimensionCounter < numToCompress; --memIdx, ++rvalIdx, ++dimensionCounter ) {
      NU_ASSERT((memIdx >= 0), mem);
      NUExpr* normIdxExpr = NULL;

      // Create the normalized select expression
      if ( rvalIdx <= endRvalIdx ) {
        // index was specified in rval for this dimension
        // note retNullForOutOfRange is false in the following call to normalizeSelectExpr, to handle situation where user has demoted the Alert for out-of-range index
        normIdxExpr = mem->normalizeSelectExpr( rval->getIndex(rvalIdx)->copy( cc ), memIdx, 0, false );
      } else {
        // unspecified index for this dimension, use full size for this index position
        normIdxExpr = NUConst::create( true, mem->getOperationalRange(memIdx).getMsb(), 32, loc );
      }
      if ( NULL == normIdxExpr ){
        NU_ASSERT( normIdxExpr, rval);        // this means that there was an incorrect index, this was Alert an alert at population time unless demoted by user
      }
      // Create the next level of the new index expression
      if ( NULL == returnIdxExpr ) {
        returnIdxExpr = normIdxExpr;              // first index to be handled, save for use in next iteration of this loop
      } else {
        // multiply the index computed so far by the size of the next range
        NU_ASSERT( (( maxMemIdx - rvalIdx ) == memIdx), rval);
        UInt32 memDimLength = mem->getRangeSize( maxMemIdx - rvalIdx ); // if the above assert never fails then assert should be removed and this should be mem->getRangeSize( memIdx )
        NUConst *multSize = NUConst::create( true, memDimLength, 32, loc );
        NUExpr* multExpr = new NUBinaryOp( NUOp::eBiUMult, returnIdxExpr, multSize, loc );
        returnIdxExpr = new NUBinaryOp( NUOp::eBiPlus, multExpr, normIdxExpr, loc );
      }
    }
    returnIdxExpr->resize( returnIdxExpr->determineBitSize() ); // returnIdxExpr is an integer
    return returnIdxExpr;
  }



  //! returns one of enum values from ResynthType based on the dimensions specified (from NUMemselLvalue or NUMemselRvalue) and the memory declaration
  ResynthType determineTypeOfResynthNeeded(const SInt32 dimensionsSpecified, const NUMemoryNet* mem){
    const SInt32 numMemDims = mem->getNumDims();
    const SInt32 firstUnpackedDimensionIdx = (SInt32) mem->getFirstUnpackedDimensionIndex();
    const SInt32 numUnpackedMemDims = (numMemDims-firstUnpackedDimensionIdx);

    ResynthType typeOfResynthNeeded = eIsUndefined;
    
    if (dimensionsSpecified == numUnpackedMemDims){
      // the lval includes only enough dimensions to exactly cover the unpacked dimensions, no more -- no less
      // thus the desired expression is a whole word from the resynthesized memory
      typeOfResynthNeeded = eIsWholeOfMemoryWord;
    } else if (dimensionsSpecified < numUnpackedMemDims) {
      // lval leaves some unpacked dimensions unspecified,
      // thus a slice of memory -- requires a concat of memory words
      typeOfResynthNeeded = eIsSliceOfMemoryWord;
    } else if (dimensionsSpecified > numUnpackedMemDims){
      // the lval includes enough dimensions to cover all unpacked and one or more packed dimensions
      // thus the desired expression is a partselect or bitselect from the resynthesized word
      // if (dimensionsSpecified == numMemDims) then this is a single bit, otherwise it is a partselect
      typeOfResynthNeeded = eIsPartOfMemoryWord;
    }
    return typeOfResynthNeeded;
  }

private:
  MsgContext *mMsgContext;
};


/*!

The MemAssignRewrite class is phase 3 of memory resynthesis.  It is a
callback for a NUDesignWalker.  The purpose of this is to locate all
assignments of the form mem <= concat or concat <= mem, and rewrite them
into concat <= concat.  It also seeks concat of entire memories like:
{mem1, mem2} <= concat or concat <= {mem1, mem2}, and rewrite them.
These are generated as a result of record resynthesis. This is necessary
here because phase 2, MemResynthCallback, can create such assignments
when processing a statement like 2dmem <= 3dmem(x).  It will expand
3dmem(x) into a concat, and mem <= concat and concat <= mem are not supported.
*/ 
class MemAssignRewrite : public NUDesignCallback {
public:
  MemAssignRewrite()
  {
  }

  ~MemAssignRewrite() {
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * ) { return eNormal; }

  /*! \brief convert mem <= concat or concat <= mem into concat <= concat
   */
  Status operator()(Phase phase, NUAssign *node)
  { 
    if (phase == ePre)
    {
      NULvalue *lval = node->getLvalue();
      NUExpr *rval = node->getRvalue();

      // Avoid mem <= mem case since those are handled well by codegen.
      if (rval->isWholeIdentifier() &&
          (rval->getWholeIdentifier()->getMemoryNet() != NULL) &&
          lval->isWholeIdentifier() &&
          (lval->getWholeIdentifier()->getMemoryNet() != NULL))
        return eNormal;

#if MEMRESYNTH_DEBUG
      UtIO::cout() << "NUAssign with ";
      if ( lval->isWholeIdentifier() )
        UtIO::cout() << "LHS";
      else
        UtIO::cout() << "RHS";
      UtIO::cout() << " memory before resynthesis:\n";
      node->print(1,1);
#endif

      NUExpr* rewritten_rval = rewrite(rval);
      if (rewritten_rval != NULL) {
        node->replaceRvalue(rewritten_rval);
        delete rval;
      }

      NULvalue* rewritten_lval = rewrite(lval);
      if (rewritten_lval != NULL) {
        node->replaceLvalue(rewritten_lval, true); // This deletes the old lvalue
      }

#if MEMRESYNTH_DEBUG
      UtIO::cout() << "NUAssign after resynthesis:\n";
      node->print(1,1);
#endif
    }
    return eNormal;
  }
  
private:

  // Rewrite concat of entire memories like {mem1, mem2} into per word
  // memory access concat.
  void rewrite(NUConcatOp* concat)
  {
    for (UInt32 idx = 0; idx < concat->getNumArgs(); ++idx)
    {
      NUExpr* arg = concat->getArg(idx);
      NUExpr* rewritten_arg = rewrite(arg);
      if (rewritten_arg != NULL) {
        NUExpr* old_arg = concat->putArg(idx, rewritten_arg);
        delete old_arg;
      }
    }
  }

  // Rewrite an expression with entire memory into per word memory access concat.
  NUExpr* rewrite(NUExpr* expr)
  {
    if (expr->getType() == NUExpr::eNUConcatOp) {
      NUConcatOp* concat = dynamic_cast<NUConcatOp*>(expr);
      rewrite(concat);
      return NULL;
    }

    NUExpr* rewritten_expr = NULL;
    NUMemoryNet* mem = NULL;
    if (expr->isWholeIdentifier()) {
      mem = expr->getWholeIdentifier()->getMemoryNet();
    }

    if (mem != NULL)
    {
      const SourceLocator &loc = mem->getLoc();
      const ConstantRange *depth = mem->getDepthRange();
      SInt32 index = depth->getMsb();
      const SInt32 incr = (index > depth->getLsb())?-1:1;
      // Convert a RHS memory to a concat of its words.  mem will
      // always have been resynthesized.
      NUExprVector exprs;
      // Loop through each word in the memory
      while ( depth->contains( index ))
      {
        // Create the index of the memsel
        NUExpr *indexExpr = NUConst::create( true, index, 32, loc );
        // Create a NUMemselRvalue (with only 1 expr) for this index
        // (use default signedness of the memory...)
        NUMemselRvalue *rmem = new NUMemselRvalue( mem, indexExpr, loc );
        rmem->resize( rmem->determineBitSize( ));
        rmem->putResynthesized( true );
        exprs.push_back( rmem );
        index += incr;
      }
      rewritten_expr = new NUConcatOp( exprs, 1, loc );
      rewritten_expr->resize( rewritten_expr->determineBitSize() );
    }

    return rewritten_expr;
  }

  // Rewrite concat of entire memories like {mem1, mem2} into per word
  // memory access concat.
  void rewrite(NUConcatLvalue* concat)
  {
    for (UInt32 idx = 0; idx < concat->getNumArgs(); ++idx)
    {
      NULvalue* arg = concat->getArg(idx);
      NULvalue* rewritten_arg = rewrite(arg);
      if (rewritten_arg != NULL) {
        NULvalue* old_arg = concat->setArg(idx, rewritten_arg);
        delete old_arg;
      }
    }
  }

  // Rewrite an lvalue with entire memory into per word memory access concat.
  NULvalue* rewrite(NULvalue* lval)
  {
    if (lval->getType() == eNUConcatLvalue) {
      NUConcatLvalue* concat = dynamic_cast<NUConcatLvalue*>(lval);
      rewrite(concat);
      return NULL;
    }

    NULvalue* rewritten_lval = NULL;
    NUMemoryNet* mem = NULL;
    if (lval->isWholeIdentifier()) {
      mem = lval->getWholeIdentifier()->getMemoryNet();
    }

    if (mem != NULL)
    {
      const SourceLocator &loc = mem->getLoc();
      const ConstantRange *depth = mem->getDepthRange();
      SInt32 index = depth->getMsb();
      const SInt32 incr = (index > depth->getLsb())?-1:1;
      // Convert a LHS memory to a concat of its words.  mem will
      // always have been resynthesized.
      NULvalueVector lvalues;
      // Loop through each word in the memory
      while ( depth->contains( index ))
      {
        // Create the index of the memsel
        NUExpr *indexExpr = NUConst::create( true, index, 32, loc );
        // Create a NUMemselLvalue (with only 1 expr) for this index
        NUMemselLvalue *lmem = new NUMemselLvalue( mem, indexExpr, loc );
        lmem->resize();
        lmem->putResynthesized( true );
        lvalues.push_back( lmem );
        index += incr;
      }
      rewritten_lval = new NUConcatLvalue( lvalues, loc );
      rewritten_lval->resize();
    }

    return rewritten_lval;
  }
};


MemoryResynth::MemoryResynth(MsgContext *msgContext)
  : mMsgContext( msgContext )
{
}

MemoryResynth::~MemoryResynth() {
}


//! Helper class to clear all the expression bit-size cache
/*!
 *! This tells Nucleus that it must recalculate the sizes following
 *! memory resynthesis
 *!
 */
class MemRefSizeFixCallback : public NUDesignCallback {
public:
  //! By default, walk through everything.
  Status operator()(Phase, NUBase*) { return eNormal; }

  Status operator()(Phase phase, NUExpr *expr) {
    if (phase == ePost) {
      expr->clearBitSizeCache();
    }
    return eNormal;
  } // Status operator
};


/*! \brief resynthesize memories with more than 1 packed and 1 unpacked dimension down to the canonical 2-D format.
 * returns true if successful.
 *
 * Memory resynthesis is performed in four stages.
 * Phase 0. add an implicit unpacked dimension [0:0] to any memory that currently has no unpacked dimensions,
 *          rewrite all refrences to these memories with an added [0] dimension specification.
 * Phase 1. Find all NUMemoryNets that have more than 2 dimensions, Update them with the depth/width
 *          information based on the resynthesized memory size. (original ranges are also kept)
 *          Also rewrites any port actuals that are not NUIdent{L,R}values but connect to a resynthesized memory
 *          into a NUIdent{L,R}value for a temp net, and an appropriate assignment to/from the temp net from the
 *          original actual.
 * Phase 2. Locate all NUMemsel{L,R}value references to memories that were modified in Phase 1, and rewrite the
 *          NUMemsel{L,R}value reference to use the range of the resynthesized dimensions.
 * Phase 3. Find and rewrite any assignments of the form mem <= concat or concat <= mem into concat <= concat.
 */
bool
MemoryResynth::design(NUDesign* design) {
  NUDesignCallback::Status status;

  // Phase 0: add implicit unpacked dimension to memories that were declared with no unpacked dimensions.
  AddImplicitUnpackedDimensionCallback addDimensionCallback(mMsgContext);
  NUDesignWalker addDimensionWalker( addDimensionCallback, false/*verbose*/ );
  addDimensionWalker.putWalkTasksFromTaskEnables( false );
  addDimensionWalker.putWalkDeclaredNets( true );
  addDimensionCallback.setPass(1);
  status = addDimensionWalker.design( design ); // first pass is to add implicit dimension to appropriate NUMemoryNet
  if ( NUDesignCallback::eNormal != status ) return false; // bail out early 
  addDimensionWalker.resetRememberVisited();
  addDimensionCallback.setPass(2);
  status = addDimensionWalker.design( design ); // walk again to handle any NUMemsel{R,L}value ref to these NUMemoryNets (that had the implicit dimension added)
  if ( NUDesignCallback::eNormal != status ) return false; // bail out early 

  // Phase 1: Resynthesize all the net declarations
  MemoryResynthCallback memResynthCallback( mMsgContext );
  NUDesignWalker memwalker( memResynthCallback, false/*verbose*/ );
  memwalker.putWalkTasksFromTaskEnables( false );
  memwalker.putWalkDeclaredNets( true );
  status = memwalker.design( design );
  if ( NUDesignCallback::eNormal != status ) return false; // bail out early 

  // Phase 2: Resynthesize all the memsels
  MemselResynthCallback memselResynthRewrite( mMsgContext );
  NUModuleList module_list;
  design->getAllModules( &module_list );
  for ( NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i )
  {
    (*i)->replaceLeaves( memselResynthRewrite );
  }

  {
    // mark the nets as now having been resynthesized
    memResynthCallback.resynthDeferredNets();


    // tell nucleus that it must recalculate bit-sizes for all NUExpr
    //RJC RC#2013/12/01 (cloutier) why is this needed, how can mem resynthesis change an expression size?
    MemRefSizeFixCallback memRefSizeFixCallback;
    NUDesignWalker fixup(memRefSizeFixCallback, false);
    fixup.putWalkTasksFromTaskEnables( false );
    status = fixup.design(design);
    if ( NUDesignCallback::eNormal != status ) return false; // bail out early 
  }

  // Phase 3: 
  // Rewrite mem <= concat and concat <= mem assignments into 
  // concat <= concat.
  // Leave mem <= mem assigns alone.
  MemAssignRewrite memAssignRewrite;
  NUDesignWalker assignwalker( memAssignRewrite, false/*verbose*/ );
  assignwalker.putWalkTasksFromTaskEnables( true );
  status = assignwalker.design( design );
  return true;
}
