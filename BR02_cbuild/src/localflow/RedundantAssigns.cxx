// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/RedundantAssigns.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "util/UtSet.h"
#include "util/UtMap.h"

//! Design walker callback which will delete redundant continuous assigns.
class AssignEliminator : public NUDesignCallback
{
public:
  //! constructor
  AssignEliminator() : mDidWork(false) {}

  //! destructor
  ~AssignEliminator() {}

  //! Only want to visit continuous assigns and modules.
  Status operator()(Phase, NUBase*) { return eSkip;}
  Status operator()(Phase, NUModule*) { return eNormal; }
  Status operator()(Phase, NUStmt*) { return eNormal; }
  Status operator()(Phase phase, NUContAssign *assign) {
    return operator()(phase,(NUAssign*)assign);
  }
  Status operator()(Phase phase, NUAssign *assign)
  {
    if ((phase == ePre) && (redundantAssign(assign) || duplicateAssign(assign)))
    {
      mDidWork = true;
      return eDelete;
    } 
    return eNormal;
  }

  //! Was anything done to change the nucleus?
  bool didWork() const { return mDidWork; }

private:
  //! Only handle really simple "a = a;" assigns for now.
  bool redundantAssign(NUAssign *assign)
  {
    NULvalue *lhs = assign->getLvalue();
    NUExpr *rhs = assign->getRvalue();
    if (lhs->isWholeIdentifier() and rhs->isWholeIdentifier()) {
      return (lhs->getWholeIdentifier() == rhs->getWholeIdentifier());
    } else {
      return false;
    }
  }

  typedef UtSet<NUAssign*> AssignSet;
  typedef UtMap<NUNet*,AssignSet> AssignsToNet;

  //! Look for cont. assignments which duplicate previously seen assignments
  bool duplicateAssign(NUAssign *assign)
  {
    // Only check continuous assignments
    if (assign->getType() != eNUContAssign)
      return false;

    // See if we have seen this assign before -- only need to check one def
    NUNetSet defs;
    assign->getDefs(&defs);
    NUNetSet::iterator p = defs.begin();
    NUNet* net = *p;
    AssignSet& assigns = mAssignsToNet[net];
    for (AssignSet::iterator iter = assigns.begin(); iter != assigns.end(); ++iter)
    {
      NUAssign* prevAssign = *iter;
      if (*prevAssign == *assign) // we have seen this assign before
        return true;
    }

    // If we got here, we have not seen the assign before.
    // Record it in the assign set for each net it defs.
    for (NUNetSet::iterator iter = defs.begin(); iter != defs.end(); ++iter)
    {
      NUNet* net = *iter;
      AssignSet& assigns = mAssignsToNet[net];
      assigns.insert(assign);
    }

    return false;
  }

private:
  //! Was anything done?
  /*!
   * This is current not utilized because this analysis runs
   * before original UD is computed.  If this analysis ever gets
   * run after UD is computed, use this boolean to determine when
   * to recompute UD.
   */
  bool mDidWork;

  //! A map from NUNets to sets of NUContAssigns that def that net
  /*! This is used to recognize when an identical NUContAssign has been
   *  previously encountered.  Mapping to sets of assigns by defs
   *  may use more memory in the presence of LHS concats (rare),
   *  but will reduce the number of comparisons when most of the
   *  assignments are to different nets (the common case).
   */
  AssignsToNet mAssignsToNet;
};


RedundantAssigns::RedundantAssigns()
{
}


RedundantAssigns::~RedundantAssigns()
{
}


void RedundantAssigns::module(NUModule *module)
{
  AssignEliminator eliminator;
  NUDesignWalker walker(eliminator, false);
  walker.module(module);
}

void RedundantAssigns::stmt(NUStmt *stmt)
{
  AssignEliminator eliminator;
  NUDesignWalker walker(eliminator, false);
  walker.stmt(stmt);
}

