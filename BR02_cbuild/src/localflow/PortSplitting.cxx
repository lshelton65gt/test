/***************************************************************************************
  Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "PortSplitting.h"
#include "PortAnalysis.h"

#include "nucleus/NUNetSet.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"

#include "nucleus/NUNetClosure.h"

#include "nucleus/NUAliasDB.h"

#include "reduce/RewriteUtil.h"
#include "reduce/RewriteCheck.h"
#include "reduce/Fold.h"

#include "localflow/LowerPortConnections.h"

#include "iodb/IODBNucleus.h"

#include "util/SetOps.h"
#include "util/Stats.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"

/*!
  \file
  Implementation of port splitting package.
*/


#if !pfICC && !defined(__COVERITY__)
// force instantiation
template class NUNetRefMultiMap<PortConnectionData*>;
#endif

template <>
PortConnectionData* NUNetRefMultiMap<PortConnectionData*>::nullT() const {
  return NULL;
}

template<>
void NUNetRefMultiMap<PortConnectionData*>::helperTPrint(PortConnectionData * const & val, int /*indent*/) const {
  val->print();
}



#if !pfICC && !defined(__COVERITY__)
// force instantiation
template class NUNetRefMultiMap<NUNet*>;
#endif

template <>
NUNet* NUNetRefMultiMap<NUNet*>::nullT() const {
  return NULL;
}

template <>
void NUNetRefMultiMap<NUNet*>::helperTPrint(NUNet * const & val, int indent) const {
  if (val) {
    val->print(false,indent);
  } else {
    for (int i = 0; i < indent; i++) {
      UtIO::cout() << " ";
    }
    UtIO::cout() << "(NULL)" << UtIO::endl;
  }
}


PortSplitting::PortSplitting(AtomicCache* strCache, 
                             IODBNucleus* iodb, 
                             Stats* stats, 
                             ArgProc* args,
                             MsgContext* msgContext, 
                             NUNetRefFactory* netRefFactory,
                             SourceLocatorFactory* sourceLocatorFactory,
                             NUNetSet * inouts,
                             NUNetSet * inputs,
                             NUNetSet * outputs,
                             bool verbose) :
  mStrCache(strCache),
  mIODB(iodb),
  mStats(stats),
  mArgs(args),
  mMsgContext(msgContext),
  mNetRefFactory(netRefFactory),
  mSourceLocatorFactory(sourceLocatorFactory),
  mInouts(inouts),
  mInputs(inputs),
  mOutputs(outputs),
  mVerbose(verbose)
{
  mConnectionData         = new PortConnectionDataMap;
  mActualToConnectionData = new NUNetRefPortConnectionDataMap(mNetRefFactory);
  mFormalToConnectionData = new NUNetRefPortConnectionDataMap(mNetRefFactory);
  mNetDepth               = new NUNetDepthMap;
  mNetFormalLists         = new NUNetToNetRefListMap;
  mFold = new Fold(mNetRefFactory, 
                   mArgs, 
                   mMsgContext, 
                   mStrCache, 
                   mIODB,
                   false,       // non-verbose
                   eFoldUDKiller);

  mPortAnalysis = new PortAnalysis(mNetRefFactory);
  mInoutCover = new NUNetSet;
  mHierRefUnionFind = new NUNetUnionFind;
}


PortSplitting::~PortSplitting()
{
  delete mInoutCover;
  delete mPortAnalysis;
  delete mFold;
  delete mNetDepth;
  delete mActualToConnectionData;
  delete mFormalToConnectionData;
  cleanupConnectionData();
  delete mConnectionData;
  cleanupNetRefLists();
  delete mNetFormalLists;
  delete mHierRefUnionFind;
}


void PortSplitting::design(NUDesign * design, bool phaseStats)
{
  mDesign = design;
  
  if (phaseStats)
    mStats->pushIntervalTimer();

  discoverSplitting(phaseStats);

#if 0
  if (mVerbose) {
    printConnectionData();
  }
#endif

  if (phaseStats)
    mStats->printIntervalStatistics("PS: Discovery");

  split();
  if (phaseStats)
    mStats->printIntervalStatistics("PS: Rewrite");

  if (mVerbose) {
    mStatistics.print();
  }

  if (phaseStats)
    mStats->popIntervalTimer();
}


void PortSplitting::discoverSplitting(bool phaseStats)
{
  if (phaseStats)
    mStats->pushIntervalTimer();

  NUNetClosure port_closure;

  traverse(&port_closure);
  if (phaseStats)
    mStats->printIntervalStatistics("PS: Traverse");

  populateActualToPortConnectionData();
  populateFormalToPortConnectionData();
  if (phaseStats)
    mStats->printIntervalStatistics("PS: Actual/Formal Population");

  populateHierRefData(&port_closure);
  if (phaseStats)
    mStats->printIntervalStatistics("PS: HierRef Population");

  propagatePrimaryInout();
  if (phaseStats)
    mStats->printIntervalStatistics("PS: Primary IO");

  determineDepth();
  if (phaseStats)
    mStats->printIntervalStatistics("PS: Depth");

  process(&port_closure);
  if (phaseStats)
    mStats->printIntervalStatistics("PS: Process");

  if (phaseStats)
    mStats->popIntervalTimer();
}


class DiscoverHierRefRelations : public NUDesignCallback
{
public:
  //! Constructor
  DiscoverHierRefRelations(NUNetClosure * port_closure, 
                           NUNetUnionFind * hier_ref_union_find) :
    NUDesignCallback(),
    mPortClosure(port_closure),
    mHierRefUnionFind(hier_ref_union_find)
  {}

  ~DiscoverHierRefRelations() {}

  //! Skip everything by default.
  Status operator()(Phase, NUBase*) { return eSkip; }

  //! Walk through the design.
  Status operator()(Phase, NUDesign*) { return eNormal; }

  //! Walk through module instances.
  Status operator()(Phase, NUModuleInstance*) { return eNormal; }
  
  //! Walk through declaration scopes.
  Status operator()(Phase, NUNamedDeclarationScope*) { return eNormal; }
  
  //! Walk through modules; process all references and referees.
  Status operator()(Phase phase, NUModule * module) {
    if (phase==ePre) {
      NUNetList all_nets;
      module->getAllNonTFNets(&all_nets);

      for (NUNetList::iterator iter = all_nets.begin();
           iter != all_nets.end();
           ++ iter) {
        NUNet * net = (*iter);
        if (net->hasHierRef()) {
          addReferences(net);
        } else if (net->isHierRef()) {
          addResolutions(net);
        }
      }
    }
    return eNormal;
  }
  
private:
  /*! 
   * Add a net and all its references to our closure. Together with
   * addResolutions, this ensures that we propagate splits across all
   * references/referees at once.
   *
   * \sa addResolutions
   */
  void addReferences(NUNet * net) {
    NU_ASSERT(net->hasHierRef(),net);
    NUNetSet equivalent;
    equivalent.insert(net);
    for ( NUNetVectorLoop loop = net->loopHierRefs(); 
          not loop.atEnd(); 
          ++loop) {
      equivalent.insert(*loop);
    }
    mPortClosure->addSet(&equivalent);
    mHierRefUnionFind->equate(equivalent);
  }

  /*! 
   * Add a net and all its resolutions to our closure. Together with
   * addReferences, this ensures that we propagate splits across all
   * references/referees at once.
   *
   * \sa addReferences
   */
  void addResolutions(NUNet * net) {
    NU_ASSERT(net->isHierRef(),net);
    NUNetHierRef * hier_ref = net->getHierRef();
    NUNetSet equivalent;
    equivalent.insert(net);
    for ( NUNetHierRef::NetVectorLoop loop = hier_ref->loopResolutions(); 
          not loop.atEnd(); 
          ++loop) {
      equivalent.insert(*loop);
    }
    mPortClosure->addSet(&equivalent);
    mHierRefUnionFind->equate(equivalent);
  }

  //! Net closure.
  NUNetClosure * mPortClosure;
  NUNetUnionFind * mHierRefUnionFind;
};


void PortSplitting::populateHierRefData(NUNetClosure * port_closure)
{
  DiscoverHierRefRelations hier_ref_callback(port_closure,mHierRefUnionFind);
  NUDesignWalker walker(hier_ref_callback,false);
  NUDesignCallback::Status status = walker.design(mDesign);
  INFO_ASSERT(status==NUDesignCallback::eNormal, "Abnormal callback");
}


void PortSplitting::propagatePrimaryInout()
{
  for (PortConnectionDataMap::iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;
    propagatePrimaryInout(data, false);
  }
}


void PortSplitting::propagatePrimaryInout(PortConnectionData * data, bool primary_above)
{
  if (data->coversInout()) {
    return; // already marked as covering inout.
  }

  bool covers_inout = primary_above;

  // loop actuals to take any primary inouts into account.
  for (NUNetRefListLoop loop = data->loopActuals();
       not loop.atEnd();
       ++loop) {
    const NUNetRefHdl & actual_net_ref = (*loop);
    NUNet * actual_net = actual_net_ref->getNet();

    if (actual_net->isPrimaryPort()) {
      covers_inout |= isInout(actual_net);
    }
  }

  if (covers_inout) {
    data->putCoversInout(covers_inout);
    for (NUNetRefListLoop loop = data->loopFormals();
         not loop.atEnd();
         ++loop) {
      const NUNetRefHdl & formal_net_ref = (*loop);
      NUNet * formal_net = formal_net_ref->getNet();
      mInoutCover->insert(formal_net);

      // find all the port connections referencing this formal as an actual.
      for (NUNetRefPortConnectionDataMap::CondLoop loop = mActualToConnectionData->loop(formal_net_ref, &NUNetRef::overlapsSameNet);
           not loop.atEnd();
           ++loop) {
        PortConnectionData * formal_data = (*loop).second;
        propagatePrimaryInout(formal_data, true);
      }
    }
  }
}


void PortSplitting::determineDepth()
{
  for (PortConnectionDataMap::iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;
    (void) determineDepth(data);
  }

  for (PortConnectionDataMap::iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;
    determineNetDepth(data);
  }
}


SInt32 PortSplitting::determineDepth(PortConnectionData * data)
{
  if (data->getDepth() > 0) {
    return data->getDepth();
  }

  SInt32 depth = 0;
  bool covers_inout = false;
  for (NUNetRefListLoop loop = data->loopFormals();
       not loop.atEnd();
       ++loop) {
    const NUNetRefHdl & formal_net_ref = (*loop);
    NUNet * formal_net = formal_net_ref->getNet();
    
    covers_inout |= isInout(formal_net);

    covers_inout |= (mInoutCover->find(formal_net) != mInoutCover->end());

    // If we have something that acts like an output/inout, look at
    // the strength of the driver. If it is an input, we don't need to
    // consider drive strength. Pure inputs should be split only when
    // connected to an inout at a lower level.
    if (not isInput(formal_net)) {
      covers_inout |= hasWeakWriter(formal_net);
    }

    // find all the port connections referencing this formal as an actual.
    for (NUNetRefPortConnectionDataMap::CondLoop loop = mActualToConnectionData->loop(formal_net_ref, &NUNetRef::overlapsSameNet);
         not loop.atEnd();
         ++loop) {
      PortConnectionData * formal_data = (*loop).second;
      SInt32 formal_depth = determineDepth(formal_data);
      depth = std::max(formal_depth,depth);

      covers_inout |= formal_data->coversInout();
    }
  }

  depth = depth + 1;
  data->setDepth(depth);

  data->putCoversInout(covers_inout);

  return depth;
}


void PortSplitting::determineNetDepth(PortConnectionData * data)
{
  for (NUNetRefListLoop loop = data->loopActuals();
       not loop.atEnd();
       ++loop) {
    const NUNetRefHdl & actual_net_ref = (*loop);
    NUNet * actual_net = actual_net_ref->getNet();
    
    SInt32 actual_depth = 0;
    NUNetDepthMap::iterator location = mNetDepth->find(actual_net);
    if (location != mNetDepth->end()) {
      actual_depth = (*location).second;
    }
    if (data->getDepth() > actual_depth) {
      (*mNetDepth)[actual_net] = data->getDepth();
    }
  }
}


//! Traverse the design and remember connectivity across port connections.
class SplittingTraverseCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Constructor.
  SplittingTraverseCallback(NUNetRefFactory * netRefFactory,
                            NUNetClosure * port_closure,
                            PortConnectionDataMap * connection_data,
                            NUNetToNetRefListMap  * net_formal_lists) :
    NUDesignCallback(),
    mNetRefFactory(netRefFactory),
    mPortClosure(port_closure),
    mConnectionData(connection_data),
    mNetFormalLists(net_formal_lists)
  {}

  //! Destructor.
  ~SplittingTraverseCallback() {}

  //! By default, skip everything.
  Status operator()(Phase, NUBase *) { return eSkip; }

  //! Walk through the design.
  Status operator()(Phase, NUDesign *) { return eNormal; }

  //! Walk through modules so we can find module instances.
  Status operator()(Phase /*phase*/, NUModule * /*module*/) {
    return eNormal;
  }

  //! Walk through declaration scopes so we can find module instances.
  Status operator()(Phase /*phase*/, NUNamedDeclarationScope * /*declScope*/) {
    return eNormal;
  }

  //! Walk through module instances to find its port connections and the instantiated module.
  Status operator()(Phase /*phase*/, NUModuleInstance * /*instance*/) {
    return eNormal;
  }

  Status operator()(Phase phase, NUPortConnectionInput * connection) {
    if (phase==ePre) {
      if (not connection->getFormal()->is2DAnything()) { // skip all 2-D/memory ports for now.
        rememberConnectionData(connection);
        rememberInputConnections(connection->getFormal(), connection->getActual());
      }
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUPortConnectionOutput * connection) {
    if (phase==ePre) {
      if (not connection->getFormal()->is2DAnything()) { // skip all 2-D/memory ports for now.
        rememberConnectionData(connection);
        rememberOutputConnections(connection->getFormal(), connection->getActual());
      }
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUPortConnectionBid * connection) {
    if (phase==ePre) {
      if (not connection->getFormal()->is2DAnything()) { // skip all 2-D/memory ports for now.
        rememberConnectionData(connection);
        rememberOutputConnections(connection->getFormal(), connection->getActual());
      }
    }
    return eNormal;
  }
private:
  void rememberConnectionData(NUPortConnection * connection) {
    NUMappedNetRefList * formal_net_ref_list = getFormalNetRefList(connection);
    PortConnectionData * data = new PortConnectionData(mNetRefFactory,
                                                       connection,
                                                       formal_net_ref_list);
    mConnectionData->insert(PortConnectionDataMap::value_type(connection,data));
  }
  void rememberInputConnections(NUNet * formal, NUExpr * actual) {
    if (actual) {
      NUNetSet uses;
      actual->getUses(&uses);
      rememberConnections(formal,&uses);
    } else {
      rememberPoint(formal);
    }
  }
  void rememberOutputConnections(NUNet * formal, NULvalue * actual) {
    if (actual) {
      NUNetSet defs;
      actual->getDefs(&defs);
      rememberConnections(formal,&defs);
    } else {
      rememberPoint(formal);
    }
  }
  void rememberConnections(NUNet * formal, NUNetSet * actuals) {
    NUNetSet connection;
    connection.insert(actuals->begin(),actuals->end());
    connection.insert(formal);
    mPortClosure->addSet(&connection);
  }
  void rememberPoint(NUNet * a) {
    NUNetSet connection;
    connection.insert(a);
    mPortClosure->addSet(&connection);
  }
  NUMappedNetRefList * getFormalNetRefList(NUPortConnection * connection) {
    NUNet * formal = connection->getFormal();
    if (mNetFormalLists->find(formal) == mNetFormalLists->end()) {
      NUMappedNetRefList* refs = new NUMappedNetRefList(mNetRefFactory);
      mNetFormalLists->insert(NUNetToNetRefListMap::value_type(formal,refs));
    }
    return (*mNetFormalLists)[formal];
  }
  NUNetRefFactory * mNetRefFactory;
  NUNetClosure * mPortClosure;
  PortConnectionDataMap * mConnectionData;
  NUNetToNetRefListMap * mNetFormalLists;
};


void PortSplitting::traverse(NUNetClosure * port_closure)
{
  SplittingTraverseCallback callback(mNetRefFactory,
                                     port_closure,
                                     mConnectionData,
                                     mNetFormalLists);
  NUDesignWalker walker(callback,false);
  NUDesignCallback::Status status = walker.design(mDesign);
  INFO_ASSERT(status==NUDesignCallback::eNormal, "Abnormal callback");
}


void PortSplitting::populateActualToPortConnectionData()
{
  for (PortConnectionDataMap::iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;
    for (NUNetRefListLoop loop = data->loopActuals();
         not loop.atEnd();
         ++loop) {
      const NUNetRefHdl & net_ref = (*loop);
      mActualToConnectionData->insert(net_ref, data);
    }
  }
}


void PortSplitting::populateFormalToPortConnectionData()
{
  for (PortConnectionDataMap::iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;
    for (NUNetRefListLoop loop = data->loopFormals();
         not loop.atEnd();
         ++loop) {
      const NUNetRefHdl & net_ref = (*loop);
      mFormalToConnectionData->insert(net_ref, data);
    }
  }
}

void PortSplitting::process(NUNetClosure * port_closure)
{
  NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition = mHierRefUnionFind->getPartition();
  for (NUNetClosure::SetLoop loop = port_closure->loopSets();
       not loop.atEnd();
       ++loop) {
    NUNetSet * cluster = (*loop);
    processCluster(cluster,hier_ref_equiv_partition);
  }
  delete hier_ref_equiv_partition;
}


struct SplittingOrderByDepthCmp
{
  bool operator()(const PortConnectionData * a, const PortConnectionData * b) {
    return (a->getDepth() < b->getDepth());
  }
};


void PortSplitting::orderClusterData(NUNetSet * cluster, OrderedClusterData * orderedData)
{
  // order the port connections of this cluster based on depth calculation.
  PortConnectionDataMap clusterData;
  for (NUNetSet::iterator iter = cluster->begin();
       iter != cluster->end();
       ++iter) {
    NUNet * point = (*iter);
    NUNetRefHdl point_ref = mNetRefFactory->createNetRef(point);
    for (NUNetRefPortConnectionDataMap::CondLoop loop = mActualToConnectionData->loop(point_ref,&NUNetRef::overlapsSameNet);
         not loop.atEnd();
         ++loop) {
      PortConnectionData * data = (*loop).second;
      clusterData[data->getPortConnection()] = data;
    }
    for (NUNetRefPortConnectionDataMap::CondLoop loop = mFormalToConnectionData->loop(point_ref,&NUNetRef::overlapsSameNet);
         not loop.atEnd();
         ++loop) {
      PortConnectionData * data = (*loop).second;
      clusterData[data->getPortConnection()] = data;
    }
  }

  for (PortConnectionDataMap::iterator iter = clusterData.begin();
       iter != clusterData.end();
       ++iter) {
    PortConnectionData * data = (*iter).second;

    orderedData->push_back(data);
  }

  std::sort(orderedData->begin(), orderedData->end(),
            SplittingOrderByDepthCmp());
}


void PortSplitting::processCluster(NUNetSet * cluster,
                                   NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition)
{
  if (not containsInouts(cluster)) {
    return;
  }

  OrderedClusterData orderedData;
  orderClusterData(cluster, &orderedData);

  markClusterForced(&orderedData);


  // top-down/bottom-up
  bool changed;
  do {
    changed = false;

    // top-down
    changed |= processClusterTopDown(&orderedData);

    // bottom-up
    changed |= processClusterBottomUp(&orderedData);

    // local propagation for all members of the cluster.
    changed |= processClusterLocal(cluster);

    // Propagate splits across hierarchical references.
    changed |= processClusterHierRefs(cluster, hier_ref_equiv_partition);

    // propagation across the port connection
    for (OrderedClusterData::iterator iter = orderedData.begin();
         iter != orderedData.end();
         ++iter) {
      PortConnectionData * data = (*iter);

      // only propagate across a port connection when this port
      // connection covers an inout.
      if (data->coversInout()) {
        changed |= data->propagate();
      }
    }
      
  } while (changed);

}


void PortSplitting::markClusterForced(OrderedClusterData * orderedData)
{
  for (OrderedClusterData::iterator iter = orderedData->begin();
       iter != orderedData->end();
       ++iter) {
    PortConnectionData * data = (*iter);
    if ( isInout(data->getFormal()) and not data->isSimpleActual() ) {
      data->forceSplitting();
    }
  }
}


bool PortSplitting::processClusterTopDown(OrderedClusterData * orderedData)
{
  bool changed = false;
  for (OrderedClusterData::iterator iter = orderedData->begin();
       iter != orderedData->end();
       ++iter) {
    const PortConnectionData * above_data = (*iter);

    // Do not process elements below port connections which do not
    // cover inouts.
    if (not above_data->coversInout()) {
      continue;
    }

    for (NUNetRefListLoop loop = above_data->loopFormals();
         not loop.atEnd();
         ++loop) {
      const NUNetRefHdl & above_net_ref = (*loop);

      for (NUNetRefPortConnectionDataMap::CondLoop data_loop = mActualToConnectionData->loop(above_net_ref,&NUNetRef::overlapsSameNet);
           not data_loop.atEnd();
           ++data_loop) {
        PortConnectionData * below_data = (*data_loop).second;
        changed |= below_data->addFromAbove(above_net_ref);
      }
    }
  }
  return changed;
}


bool PortSplitting::processClusterBottomUp(OrderedClusterData * orderedData)
{
  bool changed = false;
  for (OrderedClusterData::iterator iter = orderedData->begin();
       iter != orderedData->end();
       ++iter) {
    PortConnectionData * original_above_data = (*iter);

    // Do not process elements below port connections which do not
    // cover inouts.
    if (not original_above_data->coversInout()) {
      continue;
    }

    const PortConnectionData above_data(*original_above_data);

    // find all the connected port connections and then propagate
    // their splits. this is so we can modify the net-ref lists
    // independent from iterating over them.
    PortConnectionDataSet connected_to_actual;
    for (NUNetRefListLoop loop = above_data.loopFormals();
         not loop.atEnd();
         ++loop) {
      const NUNetRefHdl & above_net_ref = (*loop);

      for (NUNetRefPortConnectionDataMap::CondLoop data_loop = mActualToConnectionData->loop(above_net_ref,&NUNetRef::overlapsSameNet);
           not data_loop.atEnd();
           ++data_loop) {
        PortConnectionData * below_data = (*data_loop).second;
        connected_to_actual.insert(below_data);
      }
    }

    for (PortConnectionDataSet::iterator actual_iter = connected_to_actual.begin();
         actual_iter != connected_to_actual.end();
         ++actual_iter) {
      PortConnectionData * below_data = (*actual_iter);

      for (NUNetRefListLoop actual_loop = below_data->loopActuals();
           not actual_loop.atEnd();
           ++actual_loop) {
        const NUNetRefHdl & below_net_ref = (*actual_loop);
        changed |= original_above_data->addFromBelow(below_net_ref);
      }
    }
  }
  return changed;
}


bool PortSplitting::processClusterHierRefs(const NUNetSet * cluster,
                                           NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition)
{
  bool changed = false;
  for (NUNetSet::const_iterator iter = cluster->begin();
       iter != cluster->end();
       ++iter) {
    NUNet * point = (*iter);
    if (point->isHierRef() or point->hasHierRef()) {

      NUNet * representative = mHierRefUnionFind->find(point);
      NUNetSet & hier_ref_equiv_set = (*hier_ref_equiv_partition)[representative];

      // Hierarchical references can only appear as actuals.
      // Resolutions can appear as both formals and actuals.
      changed |= transferHierRefSplitsFromActuals(point,&hier_ref_equiv_set);

      if (point->hasHierRef()) {
        changed |= transferHierRefSplitsFromFormals(point,&hier_ref_equiv_set);
      }
    }
  }
  return changed;
}


bool PortSplitting::transferHierRefSplitsFromActuals(NUNet * point, NUNetSet * destinations)
{
  bool changed = false;

  NUNetRefHdl point_net_ref = mNetRefFactory->createNetRef(point);

  for (NUNetRefPortConnectionDataMap::CondLoop outer_loop = mActualToConnectionData->loop(point_net_ref,&NUNetRef::overlapsSameNet);
       not outer_loop.atEnd();
       ++outer_loop) {
    PortConnectionData * outer_data = (*outer_loop).second;
    changed |= transferHierRefSplitsToActuals(point_net_ref, outer_data->loopActuals(), destinations);
    changed |= transferHierRefSplitsToFormals(point_net_ref, outer_data->loopActuals(), destinations);
  }
  return changed;
}


bool PortSplitting::transferHierRefSplitsFromFormals(NUNet * point, NUNetSet * destinations)
{
  bool changed = false;

  NUNetRefHdl point_net_ref = mNetRefFactory->createNetRef(point);

  for (NUNetRefPortConnectionDataMap::CondLoop loop = mFormalToConnectionData->loop(point_net_ref,&NUNetRef::overlapsSameNet);
       not loop.atEnd();
       ++loop) {
    PortConnectionData * outer_data = (*loop).second;
    changed |= transferHierRefSplitsToActuals(point_net_ref, outer_data->loopFormals(), destinations);
    changed |= transferHierRefSplitsToFormals(point_net_ref, outer_data->loopFormals(), destinations);
  }

  return changed;
}


bool PortSplitting::transferHierRefSplitsToActuals(NUNetRefHdl & point_net_ref, 
                                                   NUNetRefListLoop outer_loop,
                                                   NUNetSet * destinations)
{
  bool changed = false;

  for ( /* no initial */;
       not outer_loop.atEnd();
       ++outer_loop) {
    const NUNetRefHdl & outer_net_ref = (*outer_loop);

    // Only propagate split information for the requested netref.
    if (point_net_ref->overlapsSameNet(*outer_net_ref)) {

      for (NUNetSet::iterator iter = destinations->begin();
           iter != destinations->end();
           ++iter) {
        NUNet * destination = (*iter);
        if (point_net_ref->getNet()==destination) continue;

        NUNetRefHdl destination_net_ref = mNetRefFactory->createNetRefImage(destination,
                                                                            outer_net_ref);

        // Transfer to appearances as actual.
        for (NUNetRefPortConnectionDataMap::CondLoop inner_loop = mActualToConnectionData->loop(destination_net_ref,&NUNetRef::overlapsSameNet);
             not inner_loop.atEnd();
             ++inner_loop) {
          PortConnectionData * inner_data = (*inner_loop).second;

          changed |= inner_data->addFromAbove(destination_net_ref);
        }
      }
    }
  }
  return changed;
}


bool PortSplitting::transferHierRefSplitsToFormals(NUNetRefHdl & point_net_ref, 
                                                   NUNetRefListLoop outer_loop,
                                                   NUNetSet * destinations)
{
  bool changed = false;
  for ( /* no initial */;
       not outer_loop.atEnd();
       ++outer_loop) {
    const NUNetRefHdl & outer_net_ref = (*outer_loop);

    // Only propagate split information for the requested netref.
    if (point_net_ref->overlapsSameNet(*outer_net_ref)) {

      for (NUNetSet::iterator iter = destinations->begin();
           iter != destinations->end();
           ++iter) {
        NUNet * destination = (*iter);

        NUNetRefHdl destination_net_ref = mNetRefFactory->createNetRefImage(destination,
                                                                            outer_net_ref);

        // Transfer to appearances as formal.
        for (NUNetRefPortConnectionDataMap::CondLoop loop = mFormalToConnectionData->loop(destination_net_ref,&NUNetRef::overlapsSameNet);
             not loop.atEnd();
             ++loop) {
          PortConnectionData * inner_data = (*loop).second;
          changed |= inner_data->addFromBelow(destination_net_ref);
        }
      }
    }
  }
  return changed;
}


bool PortSplitting::processClusterLocal(const NUNetSet * cluster) 
{
  bool changed = false;
  ConstantRange outer_range;

  for (NUNetSet::const_iterator iter = cluster->begin();
       iter != cluster->end(); ++iter)
  {
    NUNet * point = (*iter);
    NUNetRefHdl point_net_ref = mNetRefFactory->createNetRef(point);

    // Collect the PortConnectionData in the design whose actuals affect 'point',
    // and analyze which ranges of the net are affected
    UtVector<PortConnectionData*> data_set;
    DisjointRange disjoint_range;
    for (NUNetRefPortConnectionDataMap::CondLoop outer_loop = mActualToConnectionData->loop(point_net_ref,&NUNetRef::overlapsSameNet);
         not outer_loop.atEnd();
         ++outer_loop) {
      PortConnectionData * outer_data = (*outer_loop).second;
      NUMappedNetRefList* outer_actuals = outer_data->getActuals();
      data_set.push_back(outer_data);

      for (NUNetRefList::iterator iter = outer_actuals->begin();
           iter != outer_actuals->end(); ++iter)
      {
        NUNetRefHdl outer_ref = *iter;
        if (outer_ref->getNet() == point) {
          bool contiguous = outer_ref->getRange(outer_range);
          NU_ASSERT(contiguous, point);
          disjoint_range.insert(outer_range);
        }
      }
    }

    // Now take another pass through all the PortConnectionData we've
    // collected.  This time, mutate the refs of more than one bit
    // where any sub-ranges are referenced.
    for (UInt32 i = 0; i < data_set.size(); ++i) {
      PortConnectionData * outer_data = data_set[i];
      NUMappedNetRefList* outer_actuals = outer_data->getActuals();

      for (NUNetRefList::iterator iter = outer_actuals->begin();
           iter != outer_actuals->end(); ++iter)
      {
        NUNetRefHdl outer_ref = *iter;
        if (outer_ref->getNet() == point) {
          (void) outer_ref->getRange(outer_range); // already asserted above

          // outer_range has been subdivided into disjoint slots by DisjointRange,
          // based on all the other range references into this net.  Find out
          // what those are and use them to carve up the net.
          DisjointRange::Request* request = disjoint_range.findRequest(outer_range);
          if (request != NULL) {
            NU_ASSERT(request->numSlots() > 0, point);

            // If the original range was split up by another request,
            // then there will be more than one slot
            if (request->numSlots() > 1) {
              NUNetRefList replacements;
              for (DisjointRange::SortedSlotLoop p(request->loopSortedSlots());
                   !p.atEnd(); ++p)
              {
                DisjointRange::Slot* slot = *p;
                const ConstantRange& slot_range = slot->getRange();
                NUNetRefHdl slot_ref = mNetRefFactory->createVectorNetRef(point, slot_range);
                replacements.push_back(slot_ref);
              }
              outer_actuals->insert(iter, replacements.begin(), replacements.end());
              iter = outer_actuals->erase(iter);
              changed = true;
            }
            else {
              DisjointRange::Slot* slot = *request->loopSlots();
              NU_ASSERT(slot->getRange() == outer_range, point);
            }
          } // if
        } // if
      } // for
    } // for
  } // for
  return changed;
} // bool PortSplitting::processClusterLocal

bool PortSplitting::containsInouts(const NUNetSet * cluster) const
{
  bool inouts = false;
  for (NUNetSet::const_iterator iter = cluster->begin();
       (not inouts) and iter != cluster->end();
       ++iter) {
    NUNet * net = (*iter);
    inouts = isInout(net);
  }
  return inouts;
}


bool PortSplitting::isInout(NUNet * net) const
{
  bool is_bid = net->isBid();
  if (is_bid) {
    // respect the declaration unless port coercion has found
    // otherwise.
    if ( (mOutputs->find(net) != mOutputs->end()) or
         (mInputs->find(net) != mInputs->end()) ) {
      // port coercion has discovered that this bid really acts as a
      // directed port.
      is_bid = false;
    }
  } else {
    // respect port coercion if the net was not declared as inout.
    is_bid = (mInouts->find(net) != mInouts->end());
  }
  return is_bid;
}


bool PortSplitting::isInput(NUNet * net) const
{
  bool is_input = net->isInput();
  if (is_input) {
    // respect the declaration unless port coercion has found
    // otherwise.
    if ( (mOutputs->find(net) != mOutputs->end()) or
         (mInouts->find(net) != mInouts->end()) ) {
      // port coercion has discovered that this input really acts as
      // something else.
      is_input = false;
    }
  } else {
    // respect port coercion if the net was not declared as input.
    is_input = (mInputs->find(net) != mInputs->end());
  }
  return is_input;
}


bool PortSplitting::isOutput(NUNet * net) const
{
  bool is_output = net->isOutput();
  if (is_output) {
    // respect the declaration unless port coercion has found
    // otherwise.
    if ( (mInputs->find(net) != mInputs->end()) or
         (mInouts->find(net) != mInouts->end()) ) {
      // port coercion has discovered that this output really acts as
      // something else.
      is_output = false;
    }
  } else {
    // respect port coercion if the net was not declared as output.
    is_output = (mOutputs->find(net) != mOutputs->end());
  }
  return is_output;
}


bool PortSplitting::hasWeakWriter(NUNet * net) const
{
  bool weak = (net->isTristate() or
               net->isSupply0() or
               net->isSupply1() or
               net->isPulled() or 
               net->isProtectedMutable(mIODB));
  if (not weak) {
    const NUNetRefHdl net_ref = mNetRefFactory->createNetRef((NUNet*)net);
    // we consider ports as strong drivers; if there isn't a strong
    // driver underneath, we'll consider that lower thing weakly
    // written.
    weak = not mPortAnalysis->hasStrongContDriver(net_ref, true);
  }
  return weak;
}


//! TBD
class SplittingRewriteCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif
  
public:
  //! Constructor
  SplittingRewriteCallback(PortConnectionDataMap * connection_data,
                           NUNetRefNetMap        * net_subdivisions,
                           Fold * fold,
                           PortSplitting * splitter) :
    NUDesignCallback(),
    mConnectionData(connection_data),
    mNetSubdivisions(net_subdivisions),
    mFold(fold),
    mSplitter(splitter)
  {}
  
  ~SplittingRewriteCallback() {}

  //! By default, skip everything.
  Status operator()(Phase, NUBase *) { return eSkip; }

  //! Walk through the design.
  Status operator()(Phase, NUDesign *) { return eNormal; }

  //! Walk through modules so we can find module instances.
  Status operator()(Phase /*phase*/, NUModule * /*module*/) {
    return eNormal;
  }

  //! Walk through named declaration scopes so we can find module instances.
  Status operator()(Phase /*phase*/, NUNamedDeclarationScope * /*module*/) {
    return eNormal;
  }

  //! Process the module instance and rewrite its port connections.
  Status operator()(Phase phase, NUModuleInstance * instance) {
    if (phase==ePre) {
      bool changed = false;
      NUPortConnectionVector connections;
      for (NUPortConnectionLoop loop = instance->loopPortConnections();
           not loop.atEnd();
           ++loop) {
        NUPortConnection * connection = (*loop);

        if (connection->getFormal()->is2DAnything()) { // skip all 2-D/memory ports for now.
          connections.push_back(connection);
          continue;
        }

        PortConnectionData * data = (*mConnectionData)[connection];
        NU_ASSERT(data,connection);

        if ((not data->isModified()) or
            (not data->coversInout())) {
          connections.push_back(connection); // no split needed, so just save existing connection
          continue;
        }

        changed = true;
        generateReplacements(instance,data,&connections);
        delete connection;      // it has been replaced
      }

      if (changed) {
        instance->setPortConnections(connections);
      }
    }
    return eNormal;
  }

private:
  //! tbd.
  template<class ConnectionType>
  void generateOutputReplacements(const NULvalue * /*actual*/,
                                  const SourceLocator & loc,
                                  NUModuleInstance * instance,
                                  PortConnectionData * data,
                                  NUPortConnectionVector * connections) {
    CopyContext cc(NULL,NULL);

    NUNetRefListLoop actual_loop = data->loopActuals();
    for (NUNetRefListLoop loop = data->loopFormals();
         not loop.atEnd();
         ++loop) {
      const NUNetRefHdl & formal_net_ref = (*loop);
      NUNetRefNetMap::CondLoop location = mNetSubdivisions->loop(formal_net_ref,&NUNetRef::overlapsSameNet);

      NUNet * formal_replacement = getFormalReplacement(formal_net_ref);

      NULvalue * actual_replacement = NULL;
      if (not actual_loop.atEnd()) {
        const NUNetRefHdl & actual_net_ref = (*actual_loop);
      
        bool got_range;
      
        ConstantRange formal_range;
        ConstantRange actual_range;
      
        got_range = formal_net_ref->getRange(formal_range);
        NU_ASSERT(got_range,formal_net_ref);
      
        got_range = actual_net_ref->getRange(actual_range);
        NU_ASSERT(got_range,actual_net_ref);
      
        NU_ASSERT2(actual_range.getLength()==formal_range.getLength(),actual_net_ref,formal_net_ref);
      
        actual_replacement = new NUVarselLvalue(actual_net_ref->getNet(),
                                                actual_range,
                                                loc);
        // use fold to reduce things like {a,b,c}[1:0] and bitsels of scalars.
        actual_replacement = mFold->fold(actual_replacement);
      }

      NUPortConnection * replacement = 
        new ConnectionType(actual_replacement,
                           formal_replacement,
                           loc);
      replacement->setModuleInstance(instance);
      connections->push_back(replacement);

      ++actual_loop;
    }

    NU_ASSERT(actual_loop.atEnd(),data->getPortConnection());
  }


  //! tbd.
  void generateInputReplacements(const NUExpr * actual,
                                 const SourceLocator & loc,
                                 NUModuleInstance * instance,
                                 PortConnectionData * data,
                                 NUPortConnectionVector * connections) {
    CopyContext cc(NULL,NULL);
    for (NUNetRefListLoop loop = data->loopFormals();
         not loop.atEnd();
         ++loop) {
      const NUNetRefHdl & formal_net_ref = (*loop);

      NUNet * formal_replacement = getFormalReplacement(formal_net_ref);

      NUExpr * actual_replacement = NULL;
      if (actual) {
        ConstantRange range;
        bool got_range = formal_net_ref->getRange(range);
        NU_ASSERT(got_range, formal_net_ref);
        
        actual_replacement = mFold->createPartsel (actual, range,
                                                   actual->getLoc());
        // use fold to reduce things like {a,b,c}[1:0]
        actual_replacement = mFold->fold(actual_replacement);
      }

      NUPortConnection * replacement = 
        new NUPortConnectionInput(actual_replacement,
                                  formal_replacement,
                                  loc);
      replacement->setModuleInstance(instance);
      connections->push_back(replacement);
    }
  }


  //! tbd
  void generateReplacements(NUModuleInstance * instance,
                            PortConnectionData * data,
                            NUPortConnectionVector * connections) {
    NUPortConnection * connection = data->getPortConnection();
    switch(connection->getType()) {
    case eNUPortConnectionInput: {
      NUPortConnectionInput * input = dynamic_cast<NUPortConnectionInput*>(connection);
      generateInputReplacements(input->getActual(), input->getLoc(), instance, data, connections);
      break;
    }
    case eNUPortConnectionOutput: {
      NUPortConnectionOutput * output = dynamic_cast<NUPortConnectionOutput*>(connection);
      generateOutputReplacements<NUPortConnectionOutput>(output->getActual(), output->getLoc(),
                                                         instance, data, connections);
      break;
    }
    case eNUPortConnectionBid: {
      NUPortConnectionBid * inout = dynamic_cast<NUPortConnectionBid*>(connection);
      generateOutputReplacements<NUPortConnectionBid>(inout->getActual(), inout->getLoc(),
                                                      instance, data, connections);
      break;
    }
    default:
      NU_ASSERT("Unexpected port type."==0,connection);
      break;
    }
  }

  //! tbd.
  NUNet * getFormalReplacement(const NUNetRefHdl & formal_net_ref) {
    NUNetRefNetMap::CondLoop location = mNetSubdivisions->loop(formal_net_ref,&NUNetRef::overlapsSameNet);
    NU_ASSERT(not location.atEnd(),formal_net_ref);

    NUNet * formal_replacement = (*location).second;
    NU_ASSERT(formal_replacement,formal_net_ref);
    ++location;
    NU_ASSERT(location.atEnd(),formal_net_ref);

    return formal_replacement;
  }

  PortConnectionDataMap * mConnectionData;
  NUNetRefNetMap        * mNetSubdivisions;
  Fold * mFold;
  PortSplitting * mSplitter;
};

void PortSplitting::split()
{
  NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition = mHierRefUnionFind->getPartition();

  // 1. remember all nets used in formals/actuals.
  //    at the same time, remember how each net is subdivided.
  NUNetSet             nets;
  NUNetRefNetMap       net_subdivisions(mNetRefFactory);
  findAllModifiedNets(&nets,&net_subdivisions,hier_ref_equiv_partition);

  // 1a. fill in any holes in the decompositions.
  complete(&nets, &net_subdivisions);
  
  // 2. for each net, if it has been subdivided, perform redeclaration. 
  //    remember how a net has been decomposed.
  NUNetSet             processed_nets;
  ModuleToExprReplacementMap all_expr_replacements;
  createReplacements(&nets, &net_subdivisions, &processed_nets, &all_expr_replacements, hier_ref_equiv_partition);

  delete hier_ref_equiv_partition;

  if (all_expr_replacements.empty()) {
    return;
  }

  NUNetReplacementMap     dummy_net_replacements;
  NUTFReplacementMap      dummy_tf_replacements;
  NUCModelReplacementMap      dummy_cmodel_replacements;
  NUAlwaysBlockReplacementMap      dummy_always_replacements;
  NULvalueReplacementMap  dummy_lvalue_replacements;

  bool success = false;


  // 3. traverse the entire design. 
  //    a. rewrite module port declarations to respect the decompositions.
  //    b. rewrite modified port connections to respect the decompositions.
  //    if the original isn't a port, add replacements as locals.

  SplittingRewriteCallback callback(mConnectionData,
                                    &net_subdivisions,
                                    mFold, this);
  NUDesignWalker walker(callback,false);
  NUDesignCallback::Status status = walker.design(mDesign);
  INFO_ASSERT(status==NUDesignCallback::eNormal, "Abnormal callback");

  // 4. use rewriteleaves to rewrite everything except for port-connection formals.
  RewriteCheck consistency(processed_nets);

  NUModuleList mods;
  mDesign->getModulesBottomUp(&mods);
  for (NUModuleList::iterator iter = mods.begin();
       iter != mods.end();
       ++iter) {
    NUModule * mod = *iter;
    ModuleToExprReplacementMap::iterator location = all_expr_replacements.find(mod);
    if (location == all_expr_replacements.end()) {
      continue;
    }

    NUExprReplacementMap & expr_replacements = location->second;

    RewriteLeaves translator(dummy_net_replacements,
                             dummy_lvalue_replacements,
                             expr_replacements,
                             dummy_tf_replacements,
                             dummy_cmodel_replacements,
                             dummy_always_replacements,
                             mFold);

    // rewrite all of the module contents (port connections formals
    // were already handled).
    success |= mod->replaceLeaves(translator);

    // we should no longer refer to any of the split nets.
    mod->replaceLeaves(consistency);

    // cleanup generated expressions.
    for (NUExprReplacementMap::const_iterator iter = expr_replacements.begin();
         iter != expr_replacements.end();
         ++iter) {
      delete (*iter).second;
    }
    expr_replacements.clear();
  }
    
  // Connect new hierrefs to new resolutions.
  updateHierRefs(&net_subdivisions);

  // Disconnect old hierrefs.
  disconnectHierRefs(nets);

  // Clean up the expressions in the database
  /*
    This removes nested expressions, and looks
    for potential aliasing.
  */
  NUAliasBOM* alias_bom = mDesign->getAliasBOM();
  alias_bom->sanitizeExpressions();
}


void PortSplitting::findAllModifiedNets(NUNetSet * nets,
                                        NUNetRefNetMap * net_subdivisions,
                                        NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition)
{
  for (PortConnectionDataMap::iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;

    if ((not data->needsSplitting()) or
        (not data->coversInout()) ) {
      continue;
    }

    findModifiedNets(data->loopFormals(), nets, net_subdivisions, hier_ref_equiv_partition);
    findModifiedNets(data->loopActuals(), nets, net_subdivisions, hier_ref_equiv_partition);
  }  
}


void PortSplitting::findModifiedNets(NUNetRefListLoop loop,
                                     NUNetSet * nets,
                                     NUNetRefNetMap * net_subdivisions,
                                     NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition)
{
  for ( ; not loop.atEnd();
        ++loop) {
    const NUNetRefHdl & net_ref = (*loop);
    NUNet * net = net_ref->getNet();

    if (net->isHierRef() or net->hasHierRef()) {
      // If this net is a hierarchical reference, use this net-ref to
      // store modifications for all members of its equivalence
      // partition. This means that we will end up calling
      // addOneModifiedNetRef N^2 times (where N is the number of bits
      // in the equivalence class). 
      
      // One nice side-effect is that we end up checking that the
      // entire hierarchical reference equivalence class has been
      // consistently subdivided. If we need to improve performance of
      // this code, process only the representative element of the
      // equivalence class.

      NUNet * representative = mHierRefUnionFind->find(net);
      NUNetSet hier_ref_equiv_set = (*hier_ref_equiv_partition)[representative];
      
      for (NUNetSet::iterator iter = hier_ref_equiv_set.begin();
           iter != hier_ref_equiv_set.end(); 
           ++iter) {
        NUNet * one_net = (*iter);
        NUNetRefHdl one_net_ref = mNetRefFactory->createNetRefImage(one_net,net_ref);
        addOneModifiedNetRef(one_net_ref,nets,net_subdivisions);
      }

    } else {
      addOneModifiedNetRef(net_ref,nets,net_subdivisions);
    }
  }
}


void PortSplitting::addOneModifiedNetRef(const NUNetRefHdl & net_ref,
                                         NUNetSet * nets,
                                         NUNetRefNetMap * net_subdivisions)
{
  // A net-ref covering all bits of its net is not modified.
  if (net_ref->all()) {
    return;
  }

  nets->insert(net_ref->getNet());

  bool have_this_net_ref = false;
  for (NUNetRefNetMap::CondLoop sub_loop = net_subdivisions->loop(net_ref,&NUNetRef::overlapsSameNet);
       not sub_loop.atEnd();
       ++sub_loop) {
    const NUNetRefHdl processed_net_ref = (*sub_loop).first;
    NU_ASSERT2((*net_ref)==(*processed_net_ref),net_ref,processed_net_ref);
    have_this_net_ref = true;
  }

  if (not have_this_net_ref) {
    net_subdivisions->insert(net_ref,NULL);
  }
}

void PortSplitting::complete(const NUNetSet * nets,
                             NUNetRefNetMap * net_subdivisions)
{
  for (NUNetSet::const_iterator iter = nets->begin();
       iter != nets->end();
       ++iter) {
    NUNet * net = (*iter);
    
    NUNetRefHdl whole_net_ref = mNetRefFactory->createNetRef(net);
    NUNetRefHdl processed_net_ref = mNetRefFactory->createNetRef(net);
    for (NUNetRefNetMap::CondLoop part_loop = net_subdivisions->loop(whole_net_ref, &NUNetRef::overlapsSameNet);
         not part_loop.atEnd();
         ++part_loop) {
      const NUNetRefHdl net_ref = (*part_loop).first;
      NU_ASSERT2(net_ref->overlaps(*processed_net_ref),net_ref,processed_net_ref);
      processed_net_ref = mNetRefFactory->subtract(processed_net_ref, net_ref);
    }

    for (NUNetRefRangeLoop range_loop = processed_net_ref->loopRanges(mNetRefFactory);
         not range_loop.atEnd();
         ++range_loop) {
      net_subdivisions->insert(range_loop.getCurrentNetRef(), NULL);
    }
  }
}


void PortSplitting::createReplacements(NUNetSet             * nets,
                                       NUNetRefNetMap       * net_subdivisions,
                                       NUNetSet             * processed_nets,
                                       ModuleToExprReplacementMap * all_expr_replacements,
                                       NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition)
{
  NUNetRefNetMap net_subdivision_rewrites(mNetRefFactory);

  for (NUNetSet::SortedLoop loop = nets->loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    if (net->isHierRef() or net->hasHierRef()) {
      NUNet * representative = mHierRefUnionFind->find(net);
      if (representative != net) {
        continue; // only process hierref representatives.
      } else {
        NUNetSet & hier_ref_equiv_set = (*hier_ref_equiv_partition)[representative];
        for (NUNetSet::SortedLoop hloop = hier_ref_equiv_set.loopSorted();
             not hloop.atEnd();
             ++hloop) {
          NUNet * hnet = (*hloop);
          NUExprReplacementMap & one_expr_replacements = (*all_expr_replacements)[hnet->getScope()->getModule()];
          createOneReplacement(hnet,net_subdivisions,&net_subdivision_rewrites,processed_nets,&one_expr_replacements);
        }
      }
    } else {
      NUExprReplacementMap & one_expr_replacements = (*all_expr_replacements)[net->getScope()->getModule()];
      createOneReplacement(net,net_subdivisions,&net_subdivision_rewrites,processed_nets,&one_expr_replacements);
    }
  }

  net_subdivisions->clear();
  for (NUNetRefNetMap::MapLoop loop = net_subdivision_rewrites.loop();
       not loop.atEnd();
       ++loop) {
    net_subdivisions->insert((*loop).first,(*loop).second);
  }
}
    
void PortSplitting::createOneReplacement(NUNet * net,
                                         NUNetRefNetMap       * net_subdivisions,
                                         NUNetRefNetMap       * net_subdivision_rewrites,
                                         NUNetSet             * processed_nets,
                                         NUExprReplacementMap * expr_replacements)
{
  NUAliasBOM * alias_bom = mDesign->getAliasBOM();
  ESFactory * expr_factory = alias_bom->getExprFactory();

  bool is_protected_mutable    = net->isProtectedMutableNonHierref(mIODB,
                                                                   false);
  bool is_protected_observable = net->isProtectedObservableNonHierref(mIODB);
    
  NUNetRefHdl whole_net_ref = mNetRefFactory->createNetRef(net);
  NUNetRefHdl covered_net_ref = mNetRefFactory->createEmptyNetRef();

  // Expression representing the decomposition of the original net.
  // Used when performing Nucleus substitutions.
  NUExprVector components;

  // Create a symbol-table entry for non-hierref nets. This allows the
  // shell to access the partial nets through the original name.
  CarbonExprVector symtab_components;

  // List of replacement nets - used when replacing a port with its
  // decompositions.
  NUNetVector replacements;

  bool processed = false;
  for (NUNetRefNetMap::CondLoop part_loop = net_subdivisions->loop(whole_net_ref, &NUNetRef::overlapsSameNet);
       not part_loop.atEnd();
       ++part_loop) {
    const NUNetRefHdl net_ref = (*part_loop).first;

    NU_ASSERT2(not net_ref->overlaps(*covered_net_ref),net_ref,covered_net_ref);
    covered_net_ref = mNetRefFactory->merge(covered_net_ref, net_ref);

    if (net_ref->all()) {
      continue;
    }

    if (not processed) {
      mStatistics.source();
      if (mVerbose) {
        printNet(net, "SplitPort");
      }
    }
    processed = true;

    NUNet * replacement = createReplacementNet(net_ref);

    mStatistics.target();
    if (mVerbose) {
      printNet(replacement, "  Replacement");
    }

    if (is_protected_observable) {
      replacement->putIsProtectedObservableNonHierref(mIODB);
    }
    if (is_protected_mutable) {
      replacement->putIsProtectedMutableNonHierref(mIODB);
    }

    replacements.push_back(replacement);

    net_subdivision_rewrites->insert(net_ref, replacement);

    NUIdentRvalue * ident = new NUIdentRvalue(replacement,
                                              replacement->getLoc());
    ident->resize(replacement->getBitSize());
    components.push_back(ident);

    if (not net->isHierRef()) {
      // Hierrefs do not need symtab expressions.
      CarbonIdent * symtab_ident = alias_bom->createCarbonIdentBP(replacement, &net_ref);
      symtab_components.push_back(symtab_ident);
    }
  }

  NU_ASSERT(covered_net_ref->all(), net);

  if (processed) {
    processed_nets->insert(net);

    std::reverse(components.begin(), components.end());
    NUConcatOp * concat = new NUConcatOp(components, 1,
                                         net->getLoc());
    bool isSigned = net->isSigned();
    concat->setSignedResult(isSigned);

    concat->resize(net->getBitSize());
    (*expr_replacements)[net] = concat;

    if (not net->isHierRef()) {
      // Hierrefs do not need symtab expressions.
      std::reverse(symtab_components.begin(),
                   symtab_components.end());
      CarbonExpr * symtab_concat = expr_factory->createConcatOp(&symtab_components,
                                                                1, net->getBitSize(),
                                                                isSigned);

      CarbonIdent * symtab_ident = alias_bom->createCarbonIdent(net);
      NU_ASSERT(alias_bom->getExpr(symtab_ident)==NULL,net);
      alias_bom->mapExpr(symtab_ident,
                         symtab_concat);
    }

    updateNet(net,replacements);
  }
}

NUNet * PortSplitting::createReplacementNet(const NUNetRefHdl & net_ref)
{
  NUNet * net = net_ref->getNet();
  NUModule * module = net->getScope()->getModule();

  ConstantRange range(0,0);
  bool success = net_ref->getRange(range);
  NU_ASSERT(success, net_ref);

  NetFlags net_flags(NetFlags(net->getFlags()|eTempNet));

  NUNet * replacement = NULL;
  if (net->isHierRef()) {
    // convert 'a.b.c.name' into 'a.b.c.$portsplit_name_31_0'

    NUHierRef* href = net->getHierRef();
    NU_ASSERT(href, net);
    AtomArray path = href->getPath();
    UInt32 lastIndex = path.size() - 1;

    UtString postscript;
    postscript << range.getMsb() << "_" << range.getLsb();
    StringAtom * name = module->gensymHierRef("portsplit",
                                              path[lastIndex]->str(),
                                              postscript.c_str());

    path[lastIndex] = name;
    replacement = new NUVectorNetHierRef(path,
                                         new ConstantRange(range),
                                         net_flags,
                                         VectorNetFlags (0),
                                         module,
                                         net->getLoc());
  } else {
    // convert 'name' into '$portsplit_name_31_0' unless it is
    // protected, then create a unique name from the orignal. We will
    // lose the _31_0 because of that but we don't want to expose the
    // size of anything.
    UtString name_string;
    bool isUnique = true;
    if (net->getLoc().isTicProtected()) {
      // It is protected so just use the original name and uniquify it
      isUnique = false;
      name_string << net->getName()->str();
    } else {
      // It is not protected so use compose. That way if this is a sub
      // field of a record, the record instance name is added to the
      // temp name which makes it unique (and it is easier to debug
      // issues).
      net->compose(&name_string, NULL, true, true, "_");
      name_string << "_" << range.getMsb() << "_" << range.getLsb();
    }

    // Create the unique name and replacement net
    StringAtom * name = module->gensym("portsplit", name_string.c_str(), NULL,
                                       isUnique);
    replacement = new NUVectorNet(name,
                                  new ConstantRange(range),
                                  net_flags,
                                  VectorNetFlags (0),
                                  module,
                                  net->getLoc());
  }

  return replacement;
}


void PortSplitting::updateNet(NUNet * net, NUNetVector & replacements)
{
  NUModule * module = net->getScope()->getModule();
  if (net->isPort()) {
    // update the module port declarations.
    std::reverse(replacements.begin(), replacements.end());
    module->replacePort(net,replacements);
    NetFlags flags = net->getFlags();
    flags = NetFlags(flags & ~(eInputNet | eOutputNet | eBidNet));
    net->setFlags(flags);
    module->addLocal(net);
  } else if (net->isHierRef()) {
    // Add hierrefs.
    for (NUNetVector::iterator iter = replacements.begin();
         iter != replacements.end();
         ++iter) {
      NUNet * replacement = (*iter);
      module->addNetHierRef(replacement);
    }
  } else {
    // if this isn't a port, add as locals.
    for (NUNetVector::iterator iter = replacements.begin();
         iter != replacements.end();
         ++iter) {
      NUNet * replacement = (*iter);
      module->addLocal(replacement);
    }
  }
}


void PortSplitting::updateHierRefs(NUNetRefNetMap * net_subdivisions)
{
  for (NUNetRefNetMap::MapLoop loop = net_subdivisions->loop();
       not loop.atEnd();
       ++loop) {
    NUNetRefHdl original_net_ref = (*loop).first;
    NUNet * original = original_net_ref->getNet();
    NUNet * replacement = (*loop).second;

    if (original->isHierRef()) {
      NU_ASSERT2(replacement->isHierRef(),original,replacement);
      
      NUNetHierRef * original_hier_ref = original->getHierRef();
      NUNetHierRef * replacement_hier_ref = replacement->getHierRef();

      for ( NUNetHierRef::NetVectorLoop loop = original_hier_ref->loopResolutions(); 
            not loop.atEnd(); 
            ++loop) {
        NUNet * original_resolution = *loop;
        NUNetRefHdl original_resolution_net_ref = mNetRefFactory->createNetRefImage(original_resolution,
                                                                                    original_net_ref);
        
        NUNetRefNetMap::CondLoop resloop = net_subdivisions->loop(original_resolution_net_ref,&NUNetRef::overlapsSameNet);
        NU_ASSERT(not resloop.atEnd(),original_resolution); // we must have a replacement.

        NUNet * replacement_resolution = (*resloop).second;
        ++resloop;
        NU_ASSERT(resloop.atEnd(),original_resolution); // we must have only one replacement.

        replacement_hier_ref->addResolution(replacement_resolution);
        replacement_resolution->addHierRef(replacement);
      }
    }
  }
}


void PortSplitting::disconnectHierRefs(const NUNetSet & nets)
{
  for (NUNetSet::const_iterator iter = nets.begin();
       iter != nets.end();
       ++iter) {
    NUNet * net = (*iter);
    for (NUNetVectorLoop loop = net->loopHierRefs();
         not loop.atEnd();
         ++loop) {
      NUNet * net_reference = *loop;
      NUNetHierRef * net_reference_hier_ref = net_reference->getHierRef();
      net_reference_hier_ref->removeResolution(net);

      net->removeHierRef(net_reference);
    }
  }
}


void PortSplitting::printNet(NUNet * net, const char * prefix) const
{
  if (prefix) {
    UtIO::cout() << prefix << ": ";
  }
  UtIO::cout() << net->getScope()->getName()->str() << "."
               << net->getName()->str() 
               << UtIO::endl;
}


void PortSplitting::printCluster(const NUNetSet * cluster) const
{
  for (NUNetSet::SortedLoop loop = cluster->loopSorted();
       not loop.atEnd();
       ++loop) {
    printNet(*loop);
  }
}


void PortSplitting::cleanupConnectionData()
{
  for (PortConnectionDataMap::iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;
    delete data;
  }
  mConnectionData->clear();
}


void PortSplitting::cleanupNetRefLists()
{
  for (NUNetToNetRefListMap::iterator iter = mNetFormalLists->begin();
       iter != mNetFormalLists->end();
       ++iter) {
    NUMappedNetRefList * formal_list = (*iter).second;
    delete formal_list;
  }
  mNetFormalLists->clear();
}

void PortSplitting::printConnectionData()
{
  for (PortConnectionDataMap::const_iterator iter = mConnectionData->begin();
       iter != mConnectionData->end();
       ++iter) {
    PortConnectionData * data = (*iter).second;
    data->print();
  }
}


PortConnectionData::PortConnectionData(NUNetRefFactory * netRefFactory,
                                       NUPortConnection * connection,
                                       NUMappedNetRefList * formals) :
  mNetRefFactory(netRefFactory),
  mConnection(connection),
  mFormals(formals),
  mActuals(netRefFactory),
  mDepth(0),
  mModified(false),
  mSplittingForced(false),
  mCoversInout(false)
{
  NU_ASSERT(mFormals,mConnection);
  populate();
}


PortConnectionData::PortConnectionData(const PortConnectionData & other) :
  mNetRefFactory(other.mNetRefFactory),
  mConnection(other.mConnection),
  mFormals(other.mFormals),     // by ptr (shared)
  mActuals(other.mActuals),     // by value (copied)
  mDepth(other.mDepth),
  mModified(other.mModified),
  mSplittingForced(other.mSplittingForced)
{
  NU_ASSERT(mFormals,mConnection);
  populationConsistency();
}


PortConnectionData::~PortConnectionData()
{
}


void PortConnectionData::populate()
{
  populateFormal();
  switch(mConnection->getType()) {
  case eNUPortConnectionInput:
    populateActual(dynamic_cast<NUPortConnectionInput*>(mConnection)->getActual());
    break;
  case eNUPortConnectionOutput:
    populateActual(dynamic_cast<NUPortConnectionOutput*>(mConnection)->getActual());
    break;
  case eNUPortConnectionBid:
    populateActual(dynamic_cast<NUPortConnectionBid*>(mConnection)->getActual());
    break;
  default:
    NU_ASSERT("Unexpected port type."==0,mConnection);
    break;
  }
  populationConsistency();
}


void PortConnectionData::populateFormal()
{
  if (mFormals->empty()) {
    NUNet * formal = mConnection->getFormal();
    NUNetRefHdl net_ref = mNetRefFactory->createNetRef(formal);
    mFormals->push_back(net_ref);
  }
}


void PortConnectionData::populateActual(const NUExpr * actual)
{
  if (not actual) {
    return;
  }

  switch(actual->getType()) {
  case NUExpr::eNUConcatOp: {
    const NUConcatOp * concat = dynamic_cast<const NUConcatOp*>(actual);
    NU_ASSERT(concat->getRepeatCount()==1,concat);

    for (NUExprCLoop loop = concat->loopExprs();
         not loop.atEnd();
         ++loop) {
      populateActual(*loop);
    }
    break;
  }
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUVarselRvalue: {
    NUNetRefSet uses(mNetRefFactory);
    actual->getUses(&uses);
    NUNetRefSet::iterator first = uses.begin();
    NU_ASSERT(first != uses.end(),actual);
    mActuals.push_back(*first);
    ++first;
    NU_ASSERT(first==uses.end(),actual);
    break;
  }
  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ:
    // constants imply empty actuals net ref lists.
    break;

  case NUExpr::eNUUnaryOp: {
    // A unary operator. Only bit-flips should persist.
    const NUUnaryOp * unary = dynamic_cast<const NUUnaryOp*>(actual);
    NU_ASSERT(unary->isIdentifierBitFlip(), actual);

    const NUExpr * unary_arg = unary->getArg(0);
    populateActual(unary_arg);
    break;
  }

  default:
    // everything else should have been eliminated before constructing
    // the PCData.
    NU_ASSERT("Unexpected expression type."==0,actual); 
    break;
  }
}


void PortConnectionData::populateActual(const NULvalue * actual)
{
  if (not actual) {
    return;
  }

  switch(actual->getType()) {
  case eNUConcatLvalue: {
    const NUConcatLvalue * concat = dynamic_cast<const NUConcatLvalue*>(actual);

    for (NULvalueCLoop loop = concat->loopLvalues();
         not loop.atEnd();
         ++loop) {
      populateActual(*loop);
    }
    break;
  }
  case eNUIdentLvalue:
  case eNUVarselLvalue: {
    NUNetRefSet defs(mNetRefFactory);
    actual->getDefs(&defs);
    populateActuals(&defs);
    break;
  }
  default:
    // everything else should have been eliminated before constructing
    // the PCData.
    NU_ASSERT("Unexpected lvalue type."==0,actual); 
    break;
  }
}


void PortConnectionData::populateActuals(NUNetRefSet * actuals)
{
  NUNetRefSet::iterator first = actuals->begin();
  NU_ASSERT(first != actuals->end(),mConnection);
  mActuals.push_back(*first);
  ++first;
  NU_ASSERT(first==actuals->end(),mConnection);
}


void PortConnectionData::populationConsistency() const
{
  UInt32 formal_size = mFormals->numBits();
  NU_ASSERT(formal_size > 0,mConnection);

  if (not mActuals.empty()) {
    UInt32 actual_size = mActuals.numBits();
    NU_ASSERT(formal_size == actual_size,mConnection);
  }
}


bool PortConnectionData::addFromAbove(const NUNetRefHdl & above)
{
  return addToTarget(&mActuals,above);
}


bool PortConnectionData::addFromBelow(const NUNetRefHdl & below)
{
  return addToTarget(mFormals,below);
}


bool PortConnectionData::addFromAdjacent(const NUNetRefHdl & adjacent)
{
  return addFromAbove(adjacent);
}


bool PortConnectionData::addToTarget(NUMappedNetRefList * target, const NUNetRefHdl & part)
{
  bool i_changed = false;

  NUNetRefListIterList iter_list;
  target->findOverlap(part, &iter_list);

  for (Loop<NUNetRefListIterList> p(iter_list); !p.atEnd(); ++p) {
    NUNetRefList::iterator iter = *p;
    const NUNetRefHdl & target_net_ref = (*iter);
    NU_ASSERT(part->overlaps(*target_net_ref), part->getNet());
    i_changed |= processIntersection(target, &iter, target_net_ref, part);
  }
  mModified |= i_changed;
  return i_changed;
}

bool PortConnectionData::processIntersection(NUMappedNetRefList * target,
                                             NUNetRefList::iterator* iter,
                                             const NUNetRefHdl& target_net_ref,
                                             const NUNetRefHdl& part)
{  
  bool i_changed = false;
  bool success;

  ConstantRange target_range;
  success = target_net_ref->getRange(target_range);
  NU_ASSERT(success, target_net_ref);

  ConstantRange part_range;
  success = part->getRange(part_range);
  NU_ASSERT(success, part);

  NUNetRefList replacements;

  if (part_range.getMsb() >= target_range.getMsb()) {
    if (part_range.getLsb() <= target_range.getLsb()) {
      // target    [***]
      // part   [**********]

      // The two netrefs are either the same or target is already
      // split up more than the incoming part.

    } else {
      // target     [********]
      // part   [*******]
      ConstantRange left_range(target_range.getMsb(),part_range.getLsb());
      NUNetRefHdl left_target = mNetRefFactory->sliceNetRef(target_net_ref, left_range);
      replacements.push_back(left_target);

      ConstantRange right_range(part_range.getLsb()-1,target_range.getLsb());
      NUNetRefHdl right_target = mNetRefFactory->sliceNetRef(target_net_ref, right_range);
      replacements.push_back(right_target);
    }
  } else {
    if (part_range.getLsb() <= target_range.getLsb()) {
      // target [*******]
      // part      [********]
      ConstantRange left_range(target_range.getMsb(),part_range.getMsb()+1);
      NUNetRefHdl left_target = mNetRefFactory->sliceNetRef(target_net_ref, left_range);
      replacements.push_back(left_target);

      ConstantRange right_range(part_range.getMsb(),target_range.getLsb());
      NUNetRefHdl right_target = mNetRefFactory->sliceNetRef(target_net_ref, right_range);
      replacements.push_back(right_target);
    } else {
      // target [*************]
      // part        [***]
      ConstantRange left_range(target_range.getMsb(),part_range.getMsb()+1);
      NUNetRefHdl left_target = mNetRefFactory->sliceNetRef(target_net_ref, left_range);
      replacements.push_back(left_target);

      ConstantRange middle_range(part_range.getMsb(),part_range.getLsb());
      NUNetRefHdl middle_target = mNetRefFactory->sliceNetRef(target_net_ref, middle_range);
      replacements.push_back(middle_target);

      ConstantRange right_range(part_range.getLsb()-1,target_range.getLsb());
      NUNetRefHdl right_target = mNetRefFactory->sliceNetRef(target_net_ref, right_range);
      replacements.push_back(right_target);
    }
  }

  if (not replacements.empty()) {
    target->insert(*iter, replacements.begin(), replacements.end());
    *iter = target->erase(*iter);
    i_changed = true;
  }
  return i_changed;
} // bool PortConnectionData::processIntersection


bool PortConnectionData::propagate()
{
  bool i_changed = propagateFromAbove();
  i_changed |= propagateFromBelow();
  populationConsistency();
  return i_changed;
}


bool PortConnectionData::propagateFromAbove()
{
  return propagate(&mActuals, mFormals);
}


bool PortConnectionData::propagateFromBelow()
{
  return propagate(mFormals, &mActuals);
}


bool PortConnectionData::propagate(const NUMappedNetRefList * source,
                                   NUMappedNetRefList * target) 
{
  if (source->empty() or target->empty()) {
    return false;
  }

  bool i_changed = false;
  NUNetRefList::iterator target_iter = target->begin();
  for (NUNetRefList::const_iterator iter = source->begin();
       iter != source->end();
       ++iter) {
    const NUNetRefHdl & source_net_ref = (*iter);

    UInt32 source_size = source_net_ref->getNumBits();
    UInt32 source_size_available = source_size;

    bool keep_looking;
    do {
      NU_ASSERT(target_iter != target->end(), source_net_ref);
      keep_looking = false;

      NUNetRefHdl target_net_ref = (*target_iter);
      UInt32 target_size = target_net_ref->getNumBits();

      if (target_size > source_size_available) {
        // need to subdivide the target.
        UInt32 needed_bits = source_size_available;
        UInt32 extra_bits  = target_size - source_size_available;

        ConstantRange target_range;
        bool success = target_net_ref->getRange(target_range);
        NU_ASSERT(success, target_net_ref);

        // create netref from [MSB-used:MSB-used-needed_bits]
        SInt32 left_msb = target_range.getMsb();
        SInt32 left_lsb = left_msb-needed_bits+1;
        ConstantRange left_range(left_msb,left_lsb);
        NUNetRefHdl left_target  = mNetRefFactory->sliceNetRef(target_net_ref, left_range);
        NU_ASSERT2(left_target->getNumBits()==needed_bits, target_net_ref, left_target);

        // create netref from [MSB-used-needed_bits-1:LSB]
        SInt32 right_msb = left_lsb-1;
        SInt32 right_lsb = target_range.getLsb();
        ConstantRange right_range(right_msb,right_lsb);
        NUNetRefHdl right_target = mNetRefFactory->sliceNetRef(target_net_ref, right_range);
        NU_ASSERT2(right_target->getNumBits()==extra_bits, target_net_ref, right_target);

        // add the splits to the target list and remove the original.
        NUNetRefList::iterator drop_target;
        drop_target = target->erase(target_iter);
        target->insert(drop_target, left_target);
        target_iter = target->insert(drop_target, right_target);

        i_changed = true;
      } else if (target_size == source_size_available) {
        // both source and target have been fully consumed.
        ++target_iter;
      } else {
        NU_ASSERT2(target_size < source_size_available, source_net_ref, target_net_ref);
        // advance until we find enough bits.
        keep_looking = true;
        source_size_available -= target_size;
      }
    } while (keep_looking);
  }
  mModified |= i_changed;
  return i_changed;
}

NUNet * PortConnectionData::getFormal() const
{ 
  return mConnection->getFormal();
}


bool PortConnectionData::isSimpleActual() const
{
  bool simple = true;
  for (NUNetRefListLoop loop = loopActuals();
       simple and not loop.atEnd();
       ++loop) {
    const NUNetRefHdl & net_ref = (*loop);
    simple = net_ref->all();
  }
  return simple;
}


void PortConnectionData::print() const
{
  UtIO::cout() << "PortConnectionData (" 
               << "Depth=" << mDepth 
               << ", Modified=" << mModified 
               << ", Force=" << mSplittingForced 
               << ", Covers=" << mCoversInout
               << ")" << UtIO::endl;
  UtIO::cout() << "    PortConnection:" << UtIO::endl;
  mConnection->print(true,8);
  UtIO::cout() << "    Formal NetRefs:" << UtIO::endl;
  printNetRefList(mFormals,8);
  UtIO::cout() << "    Actual NetRefs:" << UtIO::endl;
  printNetRefList(&mActuals,8);
}


void PortConnectionData::printNetRefList(const NUMappedNetRefList * refs, int indent) const
{
  for (NUNetRefList::const_iterator iter = refs->begin();
       iter != refs->end();
       ++iter) {
    const NUNetRefHdl & net_ref = (*iter);
    net_ref->print(indent);
  }
}


SplittingLowerCallback::SplittingLowerCallback(NUNetRefFactory * netRefFactory,
                                               ArgProc * args,
                                               AtomicCache * str_cache,
                                               MsgContext * msg_context,
                                               IODBNucleus * iodb) :
  NUDesignCallback()
{
  mLP = new LowerPortConnections(str_cache,
                                 msg_context,
                                 true, false);
  mFold = new Fold(netRefFactory,
                   args,
                   msg_context,
                   str_cache,
                   iodb,
                   false,
                   eFoldUDKiller);
}

SplittingLowerCallback::~SplittingLowerCallback() 
{
  INFO_ASSERT(mModuleStack.empty(), "SplittingLowerCallback stack not empty at destruction time");
  delete mLP;
  delete mFold;
}


NUDesignCallback::Status SplittingLowerCallback::operator()(Phase, NUBase *) 
{ 
  return eSkip; 
}


NUDesignCallback::Status SplittingLowerCallback::operator()(Phase, NUDesign *) 
{
  return eNormal;
}


NUDesignCallback::Status SplittingLowerCallback::operator()(Phase phase, NUModule * module) 
{
  if (phase==ePre) {
    mModuleStack.push(module);
  } else {
    mModuleStack.pop();
  }
  return eNormal;
}

NUDesignCallback::Status SplittingLowerCallback::operator()(Phase , NUNamedDeclarationScope *) 
{
  return eNormal;
}

NUDesignCallback::Status SplittingLowerCallback::operator()(Phase /*phase*/, NUModuleInstance * /*instance*/) 
{
  return eNormal;
}


NUDesignCallback::Status SplittingLowerCallback::operator()(Phase phase, NUPortConnectionInput * connection) 
{
  if (phase==ePre) {
    NUNet * formal = connection->getFormal();
    NUExpr * actual = connection->getActual();
    NUExpr * replacementActual = NULL;
    if (actual) {
      replacementActual = maybeLowerActual(actual,formal->getBitSize(), false);
      // maybeLowerActual has taken care of any memory management;
      // deleting the old actual is unnecessary.
    } else {
      // null actual. force lower so we have a net to analyze.
      replacementActual = lowerActual(actual,formal->getBitSize(),connection->getLoc());
    }
    connection->setActual(replacementActual);
  }
  return eNormal;
}


NUDesignCallback::Status SplittingLowerCallback::operator()(Phase phase, NUPortConnectionOutput * connection) {
  if (phase==ePre) {
    NUNet * formal = connection->getFormal();
    NULvalue * actual = connection->getActual();
    NULvalue * replacementActual = NULL;
    if (actual) {
      replacementActual = maybeLowerActual(actual,formal->getBitSize());
      // maybeLowerActual has taken care of any memory management;
      // deleting the old actual is unnecessary.
    } else {
      // null actual. force lower so we have a net to analyze.
      replacementActual = lowerActual(actual,formal,connection->getLoc());
    }
    connection->setActual(replacementActual);
  }
  return eNormal;
}


NUDesignCallback::Status SplittingLowerCallback::operator()(Phase phase, NUPortConnectionBid * connection) {
  if (phase==ePre) {
    NUNet * formal = connection->getFormal();
    NULvalue * actual = connection->getActual();
    NULvalue * replacementActual = NULL;
    if (actual) {
      replacementActual = maybeLowerActual(actual,formal->getBitSize());
      // maybeLowerActual has taken care of any memory management;
      // deleting the old actual is unnecessary.
    } else {
      // null actual. force lower so we have a net to analyze.
      replacementActual = lowerActual(actual,formal->getBitSize(),connection->getLoc());
    }
    connection->setActual(replacementActual);
  }
  return eNormal;
}


NUExpr * SplittingLowerCallback::maybeLowerActual(NUExpr * actual, UInt32 bits, bool embedded) 
{
  UInt32 actual_size = actual->determineBitSize();
  if (actual_size > bits) {
    NUExpr * replacement = mFold->createPartsel (actual, ConstantRange(bits-1,0),
                                                 actual->getLoc());
    if (not actual->isManaged ())
      delete actual;
    actual = mFold->fold(replacement);
  } else if (actual_size < bits) {
    // TBD: what if actual is signed?
    NUExpr * padding = NUConst::create( false, 0,  
                                        bits - actual_size, 
                                        actual->getLoc());
    NUExprVector parts;
    // resize the actual, so that getBitSize() == determineBitSize().
    // this is necessary to avoid possibly creating an infinite
    // recursion creating concats for this actual and then embedding
    // the actual unchanged in the concat.
    actual->resize(actual_size);
    parts.push_back(padding);
    parts.push_back(actual);
    NUExpr * replacement = new NUConcatOp(parts, 1, actual->getLoc());
    replacement->resize(bits);
    actual = replacement;
  }

  NUExpr * replacement = actual;
  switch(actual->getType()) {
  case NUExpr::eNUIdentRvalue:
    // No need to lower idents.
    break;

  case NUExpr::eNUVarselRvalue: {
    bool lower = true;
    NUVarselRvalue * bitsel = dynamic_cast<NUVarselRvalue*>(actual);
    NU_ASSERT(bitsel, actual);
    if (bitsel->isConstIndex() && bitsel->getIdentExpr()->isWholeIdentifier()) {
      // TBD: This should prevent splitting and should cause a
      // complex bidi error, as we cannot determine how bits should
      // be aliased.

      // Need to lower; dynamic index will give full net ref, which
      // throws off our sizing assumptions.

      // If we have a constant index, only lower if that index is
      // out-of-bounds or we have a non bit/vector net.

      const ConstantRange &r (*bitsel->getRange ());

      NUNet* ident = bitsel->getIdent();
      NUVectorNet * vect = dynamic_cast<NUVectorNet*>(ident);
      if (ident->isBitNet()) {
        if (r.getLsb () == 0) {
          // in-bounds reference.
          lower = false;
        }
      } else if (vect) {
        const ConstantRange * range = vect->getRange();
        if (range->contains(r)) {
          // in-bounds reference.
          lower = false;
        }
      }
    }

    if (lower) {
      replacement = lowerActual(actual, bits,
                                actual->getLoc());
    }
    break;
  }

  case NUExpr::eNUConcatOp: {
    NUConcatOp * concat = dynamic_cast<NUConcatOp*>(actual);
    if (concat->getRepeatCount()!=1) {
      // Concat with repeat; need to lower the entire expression.

      // TBD: Replicate the concat and process each in-turn?
      replacement = lowerActual(actual, bits,
                                actual->getLoc());
    } else {

      // Recursively process and rewrite concat in-place.
      for (UInt32 i = 0; i < concat->getNumArgs(); i++) {
        NUExpr * child = concat->getArg(i);
        NUExpr * child_replacement = maybeLowerActual(child,child->getBitSize(),true);
        concat->putArg(i,child_replacement);
      }
    }
    break;
  }

  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ: {
    if (embedded) {
      // Constants within concats must be lowered; if there are
      // multiple elements to the concat, everything needs to be
      // properly sized.
      replacement = lowerActual(actual, bits,
                                actual->getLoc());
    }
    break;
  }

  case NUExpr::eNUUnaryOp: {
    // A unary operator. Support bit-flips; lower everything else. We
    // support bit-flips because we want to be able to flatten some
    // !/~ operations without incurring a port-lowering temporary.
    NUUnaryOp * unary = dynamic_cast<NUUnaryOp*>(actual);

    bool bit_flip = unary->isIdentifierBitFlip();
    if (not bit_flip) {
      replacement = lowerActual(actual, bits,
                                actual->getLoc());
    }
    break;
  }

  default: {
    // An arbitrary boolean expression; definitely need to lower.
    replacement = lowerActual(actual, bits,
                              actual->getLoc());
    break;
  }
  }
  return replacement;
}


NULvalue * SplittingLowerCallback::maybeLowerActual(NULvalue * actual, UInt32 bits)
{
  UInt32 actual_size = actual->determineBitSize();
  if (actual_size > bits) {
    NULvalue * replacement = new NUVarselLvalue(actual,
                                                ConstantRange(bits-1,0),
                                                actual->getLoc());
    replacement->resize();
    replacement = mFold->fold(replacement);
    return maybeLowerActual(replacement,bits);
  } else if (actual_size < bits) {
    // Create a dummy net to use as padding.
    NULvalue * padding = lowerActual(((NULvalue*)NULL), bits-actual_size, 
                                     actual->getLoc());
    NULvalueVector parts;
    parts.push_back(padding);
    parts.push_back(actual);
    NULvalue * replacement = new NUConcatLvalue(parts, actual->getLoc());
    replacement->resize();
    NU_ASSERT2(replacement->determineBitSize()==bits,actual,replacement);
    return maybeLowerActual(replacement, bits);
  }

  NULvalue * replacement = actual;
  switch(actual->getType()) {
  case eNUIdentLvalue:
    // no need to lower.
    break;

  case eNUVarselLvalue: {
    bool lower = false;
    NUVarselLvalue * bitsel = dynamic_cast<NUVarselLvalue*>(actual);
    if (not bitsel->isConstIndex()) {
      // TBD: This should prevent splitting and should cause a
      // complex bidi error, as we cannot determine how bits should
      // be aliased.

      // need to lower; dynamic index will give full net ref, which
      // throws off our sizing assumptions.
      lower = true;
    } else {
       // If we have a constant index, only lower if that index is
      // out-of-bounds or we have a non bit/vector net.

      ConstantRange r (*bitsel->getRange ());

      NUNet* ident = bitsel->getIdentNet(false);
      if (ident && ident->isBitNet()) {
        if (r.getLsb () != 0) {
          // out-of-bounds reference.
          lower = true;
        }
      } else if (ident && ident->isVectorNet()) {
        NUVectorNet * vect = dynamic_cast<NUVectorNet*>(ident);
        const ConstantRange * range = vect->getRange();
        if (not range->contains(r)) {
          // out-of-bounds reference.
          lower = true;
        }
      } else {
        // non bit/vector net.

        // TBD: What sort of net is this?
        lower = true;
      }
    }

    if (lower) {
      replacement = lowerActual(actual, bits,
                                actual->getLoc());
    }
    break;
  }

  case eNUConcatLvalue: {
    NUConcatLvalue * concat = dynamic_cast<NUConcatLvalue*>(actual);
    // Recursively process and rewrite concat in-place.
    for (UInt32 i = 0; i < concat->getNumArgs(); i++) {
      NULvalue * child = concat->getArg(i);
      NULvalue * child_replacement = maybeLowerActual(child,child->getBitSize());
      concat->setArg(i,child_replacement);
    }
    break;
  }

  default: {
    // definitely need to lower.
    replacement = lowerActual(actual, bits,
                              actual->getLoc());
    break;
  }
  }
  return replacement;
}


NUExpr * SplittingLowerCallback::lowerActual(NUExpr * actual, UInt32 bits,
                                             const SourceLocator & loc) 
{
  NUModule * module = mModuleStack.top();

  return mLP->lower(module, actual, bits, loc);
}

NUExpr * SplittingLowerCallback::lowerActual(NUExpr * actual, NUNet* formal_net,
                                             const SourceLocator & loc) 
{
  NUModule * module = mModuleStack.top();

  return mLP->lower(module, actual, formal_net, loc);
}

NULvalue * SplittingLowerCallback::lowerActual(NULvalue * actual, UInt32 bits,
                                               const SourceLocator & loc) 
{
  NUModule * module = mModuleStack.top();

  return mLP->lower(module, actual, bits, loc);
}

NULvalue * SplittingLowerCallback::lowerActual(NULvalue * actual, NUNet* formal_net,
                                               const SourceLocator & loc) 
{
  NUModule * module = mModuleStack.top();

  return mLP->lower(module, actual, formal_net, loc);
}

PortSplittingStatistics::PortSplittingStatistics()
{
  clear();
}


PortSplittingStatistics::~PortSplittingStatistics()
{
}


void PortSplittingStatistics::clear()
{
  mSources = 0;
  mTargets = 0;
}


void PortSplittingStatistics::source()
{
  ++mSources;
}


void PortSplittingStatistics::target()
{
  ++mTargets;
}


void PortSplittingStatistics::print() const
{
  UtIO::cout() << "PortSplit Summary: "
               << mSources << " nets split into "
               << mTargets << " sub-nets." 
               << UtIO::endl;
}

