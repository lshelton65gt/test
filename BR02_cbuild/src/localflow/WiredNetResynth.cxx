// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/WiredNetResynth.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"

#include "util/UtIOStream.h"

//! Flag propagation callback
class WiredNetResynth::FlagPropagateCallback : public NUDesignCallback
{
public:
  //! Enumeration for describing wired nets.
  /*!
    Position is important here. isWired() depends on wired attributes
    being greater in value than eRegister.
  */
  enum WiredType {
    eWiredNone, //!< No wired net attribute
    eRegister, //!< Register = not wired net
    eWiredOr, //!< Either wor or trior
    eWiredAnd, //!< Either wand or triand
  };

  //! Constructor
  FlagPropagateCallback(MsgContext* msgContext) :
    mMsgContext(msgContext), mModified(false)
  {
  }

  //! Virtual Destructor
  virtual ~FlagPropagateCallback()
  {}

  //! Clear the 'modified' boolean
  /*!
    Called after processing so we can detect modification on
    subsequent walks.
  */
  void clearModified() 
  {
    mModified = false;
  }

  //! Were flags propagated?
  bool wasModified() const {
    return mModified;
  }

  //! Skip everything we don't care about.
  virtual Status operator()(Phase, NUBase*)
  {
    return eSkip;
  }
  virtual Status operator()(Phase, NUDesign*) 
  { 
    return eNormal;
  }

  virtual Status operator()(Phase, NUPortConnection* port) 
  {
    // Do this on both the pre and the post phases 
    /*
      We have to propagate both up and down the hierarchy. To
      eliminate unneeded walks, we propagate in both the pre and post
      phases. 
    */
    NUNet* formalNet = port->getFormal();
    WiredType formalType = selfCheck(formalNet);
    
    NUNetSet actuals;
    port->getActuals(&actuals);
    bool locallyChanged = false;
    for (NUNetSet::UnsortedLoop p = actuals.loopUnsorted(); ! p.atEnd(); ++p)
    {
      NUNet* aNet = *p;
      if (mBadNets.count(aNet) == 0)
      {
        WiredType actualType = selfCheck(aNet);
        // Check for conflicting wired types
        if (isWired(actualType) && isWired(formalType))
        {
          if (actualType != formalType)
          {
            // conflicting. Only report the net once.
            mMsgContext->LFConflictingWiredPortConn(aNet, getWiredName(aNet, actualType), getWiredName(formalNet, formalType));
            mBadNets.insert(aNet);
          } // if
          // else the formaltype and actualtype already match
          
        } // if 
#define SUPPORTING_WIREDNETS_THRU_PORTS 0
#if SUPPORTING_WIREDNETS_THRU_PORTS
        else if (isWired(actualType) && ! isReg(formalType))
        {
          // always run propagate, but locallyChanged is sticky
          locallyChanged |= propagateType(actualType, aNet, formalNet);
          formalType = actualType;
        }
        else if (isWired(formalType) && ! isReg(actualType))
          // always run propagate, but locallyChanged is sticky
          locallyChanged |= propagateType(formalType, formalNet, aNet);
#else
        else if ((isWired(actualType) && ! isWired(formalType)) || 
                 (isWired(formalType) && ! isWired(actualType)))
        {
          mMsgContext->LFUnsupportedWiredPortConn(aNet, getWiredName(aNet, actualType), getWiredName(formalNet, formalType));
          mBadNets.insert(aNet);
        }
      } // if not a bad net
#endif
    } // for


    // This catches any changes that might have been made to this code
    // where locallyChanged has become true.
#if ! SUPPORTING_WIREDNETS_THRU_PORTS
    NU_ASSERT(! locallyChanged, port);
#endif

    // if there was more than 1 actual and we propagated a flag,
    // propagate the flag to all the actuals. The formal net has the
    // propagated wired flag.
    if (locallyChanged && (actuals.size() > 1))
    {
      for (NUNetSet::UnsortedLoop p = actuals.loopUnsorted(); ! p.atEnd(); ++p)
      {
        NUNet* aNet = *p;
        propagateType(formalType, formalNet, aNet);
      }
    }
    return eNormal;
  }
  
  virtual Status operator()(Phase, NUModule*) 
  { 
    return eNormal;
  }
  
  virtual Status operator()(Phase, NUNamedDeclarationScope*) 
  { 
    return eNormal;
  }
  
  virtual Status operator()(Phase, NUModuleInstance*) 
  { 
    return eNormal; 
  }

  static bool isWired(WiredType type) 
  {
    return type > eRegister;
  }

  static WiredType selfCheck(const NUNet* net)
  {
    bool isWired = net->isWiredNet();
    bool isReg = net->isReg();

    WiredType type = eWiredNone;

    if (isReg)
    {
      NU_ASSERT(! isWired, net);
      type = eRegister;
    }      

    if (isWired)
    {
      if (net->isWiredOr())
        type = eWiredOr;
      else
        type = eWiredAnd;
    }
    return type;
  }

  static bool isReg(WiredType type)
  {
    return type == eRegister;
  }

private:

  // Returns true if the flag was propagated.
  bool propagateType(WiredType type, const NUNet* srcNet, NUNet* dstNet)
  {
    /* Try to keep the declaration consistent in the hierarchy. This
     * obviously won't hold if through the hierarchy an actual is
     * connected to both a wor and a trior. One will eventually win
     * out. But, in most cases, that won't be an issue.
     * The main reason for this is to give the best possible
     * error/warning message if conflicts arise. If a net is declared
     * as trior and we say that it is 'wor'. It might be confusing.
     */
    
    bool locallyChanged = false;
    switch (type)
    {
    case eWiredNone:
    case eRegister:
      break;
    case eWiredOr:
      if (! dstNet->isWiredOr())
      {
        // NetRedeclare is destructive so you have to really know what
        // you want to set it to before you set it.
        if (srcNet->isWor())
          dstNet->putIsWor(true);
        else if (srcNet->isTrior())
          dstNet->putIsTrior(true);
        
        mModified = true;
        locallyChanged = true;

        mMsgContext->RedeclaringNetType(dstNet, getWiredName(srcNet, eWiredOr));
      }
      break;
    case eWiredAnd:
      if (! dstNet->isWiredAnd())
      {
        // NetRedeclare is destructive so you have to really know what
        // you want to set it to before you set it.
        if (srcNet->isWand())
          dstNet->putIsWand(true);
        else if (srcNet->isTriand())
          dstNet->putIsTriand(true);

        mModified = true;
        locallyChanged = true;

        mMsgContext->RedeclaringNetType(dstNet, getWiredName(srcNet, eWiredAnd));
      }
      break;
    }
    return locallyChanged;
  }

  const char* getWiredName(const NUNet* net, WiredType type) const
  {
    const char* ret = "wire"; // should be overwritten
    switch (type)
    {
    case eWiredNone:
      break;
    case eRegister:
      ret = "reg";
      break;
    case eWiredOr:
      if (net->isWor())
        ret = "wor";
      else if (net->isTrior())
        ret = "trior";
      else
        NU_ASSERT(0, net);
      break;
    case eWiredAnd:
      if (net->isWand())
        ret = "wand";
      else if (net->isTriand())
        ret = "triand";
      else
        NU_ASSERT(0, net);
      break;
    }
    return ret;
  }



  NUNetSet mBadNets;
  MsgContext* mMsgContext;
  bool mModified;
};

//! Class to deal with resynthing assigns to wired nets.
class WiredNetResynth::AssignmentCallback : public NUDesignCallback
{
  // This deals with ternary resynthesis 
  class TernaryResynth
  {
  public:
    TernaryResynth( AssignmentCallback *assignCB, NULvalue* lval,
                    FlagPropagateCallback::WiredType lhsWiredType) : 
      mAssignCB(assignCB),
      mLval(lval),
      mLhsWiredType(lhsWiredType)
    {}
    
    void resynth(NUTernaryOp* ternary)
    {
      synthTernarySubExpr(ternary->getArg(1), 1, ternary);
      synthTernarySubExpr(ternary->getArg(2), 2, ternary);
    }

  private:
    AssignmentCallback* mAssignCB;
    NULvalue* mLval;
    FlagPropagateCallback::WiredType mLhsWiredType;

    void synthTernarySubExpr(NUExpr* subExpr, UInt32 index, 
                             NUTernaryOp* ternary)
    {
      if (subExpr->getType() == NUExpr::eNUTernaryOp)
      {
        NUTernaryOp* subTernary = static_cast<NUTernaryOp*>(subExpr);
        resynth(subTernary);
      }
      else if (mAssignCB->checkRval(subExpr))
      {
        NUExpr* newSub = mAssignCB->getWiredExpr(mLval, subExpr, mLhsWiredType);
        if (newSub != subExpr)
          ternary->putArg(index, newSub);
      }
    }
  };
  
  friend class TernaryResynth;

public:

  AssignmentCallback(MsgContext* msgContext, NUModuleSet* visitedModules, 
                     bool recurse) : 
    mMsgContext(msgContext), mVisitedModules(visitedModules), mRecurse(recurse)
  {}
  
  virtual ~AssignmentCallback() {}

  bool isGood() const 
  {
    return ( (mMsgContext->getSeverityCount(MsgContext::eError) == 0) &&
             (mMsgContext->getSeverityCount(MsgContext::eAlert) == 0) );
  }
  
  virtual Status operator()(Phase, NUBase*)
  {
    return eSkip;
  }

  virtual Status operator()(Phase, NUDesign*) 
  { 
    return eNormal;
  }

  virtual Status operator()(Phase, NUNamedDeclarationScope*) 
  { 
    return eNormal;
  }

  virtual Status operator()(Phase, NUModule* module)
  {
    Status stat = eNormal;
    if (mVisitedModules->count(module) != 0)
      stat = eSkip;
    else
      mVisitedModules->insert(module);
    return stat;
  }
  
  virtual Status operator()(Phase, NUModuleInstance*) 
  { 
    Status stat = eSkip;
    if (mRecurse)
      stat = eNormal;
    return stat;
  }

  virtual Status operator()(Phase phase, NUContAssign* stmt) 
  {
    if (phase == ePre)
    {
      NULvalue* lval = stmt->getLvalue();
      NUNetSet lvalNets;
      lval->getDefs(&lvalNets);

      NUType type = lval->getType();
      bool unsupportedLval = ((type != eNUIdentLvalue) && (type != eNUVarselLvalue));

      // I want to report on all lhs nets if the lvalue is
      // unsupported.

      // Use the FlagPropagateCallback structure to help determine
      // wiredness and type.
      FlagPropagateCallback::WiredType lhsWiredType = 
        FlagPropagateCallback::eWiredNone;
      
      // Unfortunately, i have to do a sorted loop to keep messaging consistent.
      for (NUNetSet::SortedLoop lhsP = lvalNets.loopSorted(); ! lhsP.atEnd(); ++lhsP)
      {
        NUNet* lhsNet = *lhsP;
        if (lhsNet->isHierRef())
        {
          // loop all possible resolutions. If one is wired, the rest
          // have to be the same currently.
          NUNetHierRef* hierRef = lhsNet->getHierRef();
          bool supportedHierRef = true;
          
          
          bool first = true;
          for (NUNetHierRef::NetVectorLoop hierP = hierRef->loopResolutions();
               ! hierP.atEnd(); ++hierP)
          {
            NUNet* resNet = *hierP;
            FlagPropagateCallback::WiredType resWiredType =
              FlagPropagateCallback::selfCheck(resNet);
            
            if (first)
              // initialize the lhsWiredType
              lhsWiredType = resWiredType;
            
            if ((lhsWiredType != resWiredType) &&
                (FlagPropagateCallback::isWired(lhsWiredType) ||
                 FlagPropagateCallback::isWired(resWiredType)))
            {
              // unsupported hierarchical reference
              supportedHierRef = false;
            }

            if (supportedHierRef && FlagPropagateCallback::isWired(lhsWiredType))
              // for as long as the hierref is supported, check the
              // resolution
              (void) isSupportedLHSNet(resNet, lval, unsupportedLval);

            first = false;
          }

          if (! supportedHierRef)
          {
            mMsgContext->LFUnsupportedHierRefAssign(lval, "All resolutions of a hierarchical reference assign must have the same wired net (wand/wor) attributes.");
            // make sure we treat the lhs as a wire if the message is
            // demoted.
            lhsWiredType = FlagPropagateCallback::eWiredNone;
          }
        }
        else if (lhsNet->isWiredNet())
        {
          if (isSupportedLHSNet(lhsNet, lval, unsupportedLval))
            lhsWiredType = FlagPropagateCallback::selfCheck(lhsNet);
        }
      }
      
      if (FlagPropagateCallback::isWired(lhsWiredType))
      {
        NUExpr* rval = stmt->getRvalue();
        // Check the rhs. If we don't support it, don't do anything
        if (checkRval(rval))
        {
          // we support it. If it is a simple assign just &= or |= the
          // expression. If it is a ternary, apply the self-reference
          // to the appropriate leaves.

          if (rval->getType() == NUExpr::eNUTernaryOp)
          {
            NUTernaryOp* ternary = static_cast<NUTernaryOp*>(rval);
            TernaryResynth ternaryResynth(this, lval, lhsWiredType);
            ternaryResynth.resynth(ternary);
          }
          else
          {
            // simple assignment.
            NUExpr* wiredExpr = getWiredExpr(lval, rval, lhsWiredType);
            if (wiredExpr != rval)
              stmt->replaceRvalue(wiredExpr);
            // The assign statement should now have a fixed rhs.
          }
        } // if lhsHasWiredNet
      } // if lhs has wired net
    } // if phase == pre
    
    return eNormal;
  }
  
private:
  
  MsgContext* mMsgContext;
  
  NUModuleSet* mVisitedModules;
  bool mRecurse;
  
  // Using 'unsupported' boolean report nets we do not support.
  bool isSupportedLHSNet(const NUNet* net, NULvalue* lval, bool unsupported)
  {
    if (unsupported)
    {
      UtString nameBuf;
      net->composeUnelaboratedName(&nameBuf);
      mMsgContext->UnsupportedLHSWithWiredNet(lval, nameBuf.c_str());
    }
    return ! unsupported;
  }
  
  // This transforms the rval into its wired form, deleting rval on
  // the way out if was transformed. If not transformed, returns rval.
  NUExpr* getWiredExpr(NULvalue* lval, NUExpr* rval, 
                       FlagPropagateCallback::WiredType lhsWiredType)
  {
    NUExpr* wiredExpr = rval;
    // If it drives only Z, then fah getabout it.
    // Don't wire z's as they only pertain to strength, not value.
    if (! rval->drivesOnlyZ())
    {
      NUExpr* lhsToRhs = NULL;
      
      NUIdentLvalue* identLval = dynamic_cast<NUIdentLvalue*>(lval);
      NUVarselLvalue* varselLval = NULL;
   
      if (identLval)
      {
        // simple ident assign
        NUNet* ident = identLval->getIdent();
        lhsToRhs = new NUIdentRvalue(ident, ident->getLoc());
      }
      else if ((varselLval = dynamic_cast<NUVarselLvalue*>(lval)))
      {
        // varsel. Get the ident and have the varsel generate the
        // rvalue.
        lhsToRhs = varselLval->NURvalue();
        NUNet* ident = varselLval->getIdentNet();
        NU_ASSERT(ident, lval);
      }
      
      // At this point we should have rhs rep for the lvalue. If not,
      // we let something we don't support through.
      NU_ASSERT(lhsToRhs, lval);
      
      // create the &= (wand) or |= (wor)
      CopyContext ctx(NULL, NULL);
      NUExpr* rhsCopy = rval->copy(ctx);
      
      NUOp::OpT op = NUOp::eBiBitOr;
      switch (lhsWiredType)
      {
      case FlagPropagateCallback::eWiredNone:
      case FlagPropagateCallback::eRegister:
        NU_ASSERT(0, lval);
        break;
      case FlagPropagateCallback::eWiredOr:
        op = NUOp::eBiBitOr;
        break;
      case FlagPropagateCallback::eWiredAnd:
        op = NUOp::eBiBitAnd;
        break;
      }

      wiredExpr = new NUBinaryOp(op, lhsToRhs, rhsCopy, rval->getLoc());
      wiredExpr->resize(wiredExpr->determineBitSize());
      
      // Delete the expression which will be replaced.
      delete rval;
    }
    return wiredExpr;
  }

  // Check if we support rval. Should only get called if we have a
  // wired net on the lhs.
  bool checkRval(NUExpr* rval)
  {
    DynBitVector zbits;
    bool supported = true;
    if (rval->drivesZ(&zbits))
    {
      // if this drives z only partially, we don't support it
      UInt32 rhsStartBit;
      UInt32 rhsSize;
      if (! zbits.getContiguousRange(&rhsStartBit, &rhsSize))
        supported = false;
    }
    else if (NUConcatOp* cat = dynamic_cast<NUConcatOp*>(rval)) {
      for (UInt32 i = 0; supported && (i < cat->getNumArgs()); ++i) {
        supported = checkRval(cat->getArg(i));
      }
    }

    if (! supported)
      mMsgContext->UnsupportedRHSWithWiredNet(rval);
    return supported;
  }

};


WiredNetResynth::WiredNetResynth(MsgContext* msgContext) :
  mMsgContext(msgContext)
{
  mVisitedModules = new NUModuleSet;
}

WiredNetResynth::~WiredNetResynth()
{
  delete mVisitedModules;
}


void WiredNetResynth::propagate(NUDesign* design)
{
  FlagPropagateCallback flagCallback(mMsgContext);
  NUDesignWalker walker(flagCallback, false, false);
  // No wired nets assigned to in tasks. No reason to go through them.
  walker.putWalkTasksFromTaskEnables(false);
  do {
    flagCallback.clearModified();
    walker.design(design);
  } while (flagCallback.wasModified());
}

bool WiredNetResynth::module(NUModule* module, bool recurse)
{
  AssignmentCallback cb(mMsgContext, mVisitedModules, recurse);
  // The callback remembers visited modules. No reason to do it twice.
  NUDesignWalker walker(cb, false, false);
  // Don't walk replacements
  walker.putWalkReplacements(false);
  // Don't care about tasks. They can't assign to wired nets. And
  // register connections will be lowered.
  walker.putWalkTasksFromTaskEnables(false);
  walker.putWalkDeclaredNets(false);
  walker.module(module);
  return cb.isGood();
}
