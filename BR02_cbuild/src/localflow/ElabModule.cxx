// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/Elaborate.h"
#include "LFContext.h"
#include "localflow/ClockAndDriver.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNamedDeclarationScope.h"

#include "symtab/STSymbolTable.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "flow/FLFactory.h"
#include "flow/FLElabUseCache.h"

#include "compiler_driver/CarbonDBWrite.h"
#include "compiler_driver/CarbonContext.h"

#include "util/CarbonAssert.h"
#include "util/CbuildMsgContext.h"
#include "util/ArgProc.h"

void Elaborate::elabFlow(FLNode *flow_node,
                         FlowElabMultiMap *flow_map,
                         LFContext *context,
                         FLNodeSet * stoppers,
                         ClockAndDriver * clock_and_driver,
                         bool elaboratePorts)
{
  STBranchNode *hier = context->getHier();
  FLNodeElabFactory *flow_factory = context->getFlowElabFactory();

  FlowConnectionMap moduleFanouts;
  ConnectionMap deferredConnections;

  // Note:  This routine used to be recursive, where the recursion
  // occurred when block nesting was encountered.  This was innefficient
  // because there was no keeping track of what nodes were in the
  // process of being traversed, and so nodes were traversed many times.
  //
  // This was resolved by having the iterator branch at block nesting,
  // so that it handles traversal both over and into block nesting.
  // This will cause more state to be maintained in the iterator, but
  // will cause less work to be done overall, because nodes will be
  // guaranteed to be traversed only once.

  FLNodeIter* fanin_iter =
    flow_node->makeFaninIter(FLIterFlags::eAll,
                             FLIterFlags::eBound,
                             FLIterFlags::eIntoNestedHier | FLIterFlags::eBranchAtNestedBlock,
                             true,
                             FLIterFlags::eIterFaninPairOnce);

  for (; !fanin_iter->atEnd(); fanin_iter->next()) {
    FLNode* this_flow_node = **fanin_iter;
    NUNet* defNet = this_flow_node->getDefNet();
    NUUseDefNode* useDef = this_flow_node->getUseDefNode();
    FLNode* fanout = fanin_iter->getCurParent();

    bool isStopper = stoppers && (stoppers->find(this_flow_node) != stoppers->end());
    bool isModuleInstance = this_flow_node->hasNestedHier() ||
                            ((useDef != NULL) && (useDef->getType()==eNUModuleInstance));
    bool isEdge = (fanin_iter->getSensitivity() == eSensEdge);
    bool stopHere = false;
    bool ignore = false;
    bool seenBefore = false;
    bool inMap = false;

    // First, we want to track all fanouts of module instances, because
    // we will need this information to set up correct deferred connections
    // for output ports of sub-instances.  We also want to decide if we have
    // seen this flow before.
    if (isModuleInstance && fanout)
    {
      FLN_ASSERT(!fanin_iter->isNestedRelationship(),fanout);
      seenBefore = (moduleFanouts.find(this_flow_node) != moduleFanouts.end());
      ConnectionDesc cd(fanout,isEdge);
      moduleFanouts[this_flow_node].insert(cd);
    }

    inMap = (flow_map->find(this_flow_node) != flow_map->end());
    seenBefore |= inMap;

    // Next, examine the current flow to decide what action to take.
    // stopHere means not to visit any fanin or nesting from this node.
    // ignore means not to elaborate this node.

    // Do not elaborate ExtNet drivers.  ExtNet drivers are only
    // needed for unelaborated analysis to handle references in
    // sub-modules (see the NUExtNet class doc).
    if (defNet->isExtNet()) {
      ignore = true;
    }

    // We don't intentionally elaborate task/function internals, but
    // we may reach task/function locals through an ExtNet flow
    // traversal. Prevent elaboration through these internals.
    if (this_flow_node->getDefNet()->isTFNet()) {
      stopHere = ignore = true;
    }

    // Make sure we just visit things once
    if (seenBefore)
      stopHere = true;

    // If there is a stopper set and this node is in it, stop the
    // traversal here.
    if (isStopper)
      stopHere = true;

    // If this is a ModuleInstance, but not a stopper, we want to skip
    // it.  See comment in Elaborate::elabScopeNetListFlow for
    // detailed information.
    if (isModuleInstance && !isStopper)
      ignore = true;

    // For the sys task net and ControlFlowNets we only care about
    // blocks that compute values (like always blocks), we don't want
    // to elaborate modules or module instances
    if (defNet->isVirtualNet() && (useDef != NULL)) {
      NUType type = useDef->getType();
      if ((type == eNUModuleInstance) || (type == eNUModule)) {
        ignore = true;
      }
    }

    // Stop iterating at port connections if we are not elaborating
    // them
    NUPortConnection *portconn = dynamic_cast<NUPortConnection*>(useDef);
    if (!elaboratePorts && (portconn != NULL)) {
      stopHere = true;
    }

    // Now act on the decisions made above.

    if (stopHere)
      fanin_iter->doNotExpand();

    if (ignore)
        continue;

    // If we get here, we want to elaborate this node.  This node may
    // or may not be a stopper.  Investigate the node to decide if we
    // want to create an elaborated copy, or if we just want to
    // redirect this to the elaborated drivers of its def net.
    bool makeElabCopy = false;
    FLNodeBound *bound_node = dynamic_cast<FLNodeBound*>(this_flow_node);
    bool isInputPort = (portconn && (portconn->getFormal() == defNet));
    NUNetElab* net_elab = isInputPort ? helperGetFormalElab(portconn, hier) : defNet->lookupElab(hier);

    if (stoppers != NULL)
    {
      // If a stopper set was provided, then we want to elaborate any non-stopper
      // nodes and redirect connections at stopper nodes.
      makeElabCopy = !isStopper;
    }
    else if (!elaboratePorts && portconn)
    {
      // If we are not elaborating ports, and this is one, then redirect
      // instead of elaborating
      makeElabCopy = false;
    }
    else 
    {
      // If no stopper set was provided, then we want to elaborate everything
      // except for certain bound nodes.

      // Replicate bound nodes only under the following conditions:
      //
      // 1. The bound node represents the contribution from a port and
      //    the current net is a primary port.
      //
      // 2. The bound node represents the contribution due from a
      //     protected mutable net (ie. deposit or force).
      //
      // 3. The bound node represents a hierarchical contribution and
      //    this net elab does not already have a hierarchical bound.
      //
      // 4. The bound node represents an undriven contribution and this
      //    net elab does not already have an undriven bound.
      //
      // Otherwise, bound nodes are not replicated and they are redirected
      // to the elaborated drivers of their def net.
      bool primary_port = ((fanin_iter->getCurFlags() & FLIterFlags::ePrimaryPorts) != 0);
      makeElabCopy = ((bound_node == NULL) ||
                      (bound_node->getType() == eFLBoundPort && primary_port) ||
                      (bound_node->getType() == eFLBoundProtect) ||
                      ((bound_node->getType() == eFLBoundHierRef) && !net_elab->hasContinuousDriver(eFLBoundHierRef)) ||
                      ((bound_node->getType() == eFLBoundUndriven) && !net_elab->hasContinuousDriver(eFLBoundUndriven)));
    }


    // We are finally ready to perform the actual elaboration
    if (makeElabCopy) {
      if (!seenBefore) // Don't re-elaborate a node we have seen before
      {
        // Create an elaborated copy of the flow node and add it to the flow map
        FLNodeElab* elab_flow = NULL;
        if (bound_node)
          elab_flow = flow_factory->createBound(bound_node, net_elab, hier);
        else
          elab_flow = flow_factory->create(this_flow_node, net_elab, hier);
        if (!inMap) {
          flow_map->insert(FlowElabMultiMap::value_type(this_flow_node, elab_flow));
        }

        // Handle port connections for input and bid ports.
        bool is_connected_port = isInputPort && net_elab->getStorageNet()->isContinuouslyDriven();
        if (is_connected_port) {
          // If we have a ClockAndDriver structure, our caller wants to
          // preserve only a known set of clock drivers. If this net
          // elab is a clock, only allow the addition of known drivers
          // as elaborated drivers.
          bool add_this_driver = (!clock_and_driver ||
                                  !clock_and_driver->isClock(net_elab) ||
                                  clock_and_driver->isClockDriver(elab_flow));
          if (add_this_driver) {
            addContinuousDriver(net_elab, elab_flow);
          }
        }
      }
    } else if ((isStopper and 
                (bound_node and (bound_node->getType() == eFLBoundUndriven or bound_node->getType() == eFLBoundProtect))) or
               (useDef and useDef->isContDriver() and useDef->getType() != eNUModuleInstance)) {

      // fyi: we have no testcase that gets here when isStopper is false 12/13/10

      // Certain local continuous drivers can be immediately
      // connected. These nodes indicate no other driver (like a
      // module instance or a port or hier-ref bound). Deferring these
      // fanin arcs can introduce unneeded pessimism into the graph.
      FLNodeElab* elab_flow = NULL;
      if (bound_node) {
        if (bound_node->getType() == eFLBoundUndriven) {
          // we only create one undriven bound per net elab.
          elab_flow = net_elab->getFirstContinuousDriverOfType(eFLBoundUndriven);
        } else {
          elab_flow = flow_factory->createBound(bound_node, net_elab, hier);
        }
      } else {
        elab_flow = flow_factory->create(this_flow_node, net_elab, hier);
      }
      if (elab_flow && !inMap) {
        flow_map->insert(FlowElabMultiMap::value_type(this_flow_node, elab_flow));
      }
    } else if (!inMap && fanout) {
      // Do not create an elaborated flow for this.  Instead, schedule this
      // as a deferred connection, to be made after all drivers of the
      // net elab are known.
      ConnectionDesc cd(fanout,isEdge);
      deferredConnections[net_elab].insert(cd);
    }
  }
  delete fanin_iter;

  // Transfer information from from local deferred map to global map,
  // now that all module instance fanout is known for this traversal.
  for (ConnectionMap::iterator n = deferredConnections.begin();
       n != deferredConnections.end();
       ++n)
  {
    NUNetElab* net_elab = n->first;
    ConnectionDescSet& connections = n->second;
    addDeferredConnections(net_elab, connections, moduleFanouts, flow_map);
  }
}

void Elaborate::addDeferredConnections(NUNetElab* net_elab,
                                       const ConnectionDescSet& connections,
                                       const FlowConnectionMap& moduleFanouts,
                                       FlowElabMultiMap *flow_map)
{
  for (ConnectionDescSet::const_iterator i = connections.begin();
       i != connections.end();
       ++i)
  {
    const ConnectionDesc& cd = *i;
    FLNode* fanout = cd.first;

    // If this fanout is a module, use its fanouts instead
    FlowConnectionMap::const_iterator pos = moduleFanouts.find(fanout);
    if (pos != moduleFanouts.end())
    {
      const ConnectionDescSet& moduleConnections = pos->second;
      addDeferredConnections(net_elab, moduleConnections, moduleFanouts, flow_map);
    }
    else
    {
      // Not a module, so find the elaborated fanouts and add them
      bool isEdge = cd.second;
      FlowElabMultiMapRange elab_range = flow_map->equal_range(fanout);
      for (FlowElabMultiMapIter next = elab_range.first; next != elab_range.second; ++next)
      {
        FLNodeElab* elab_fanout = next->second;
        if (isEdge) {
          mDeferredEdgeUseMultiMap[elab_fanout].insert(net_elab);
        } else {
          mDeferredUseMultiMap[elab_fanout].insert(net_elab);
        }
      }
    }
  }
}

/*
 * Create FLNodeElab objects for each FLNode and create a map of FLNode objects to
 * FLNodeElab objects.
 *
 */
void Elaborate::elabScopeLocalsFlow(NUScope *scope,
                                    FlowElabMultiMap *flow_map,
                                    LFContext *context,
                                    bool elaboratePorts)
{
  STBranchNode *hier = context->getHier();

  // Replicate the FLNode objects
  NUNetList net_list;
  scope->getAllNonTFNets(&net_list);

  elabScopeNetListFlow( hier, net_list, flow_map, context, NULL, NULL, elaboratePorts);
}

void Elaborate::elabScopeNetListFlow(STBranchNode * hier,
                                     NUNetList & net_list,
                                     FlowElabMultiMap *flow_map,
                                     LFContext *context,
                                     FLNodeSet * stoppers,
                                     ClockAndDriver * clock_and_driver,
                                     bool elaboratePorts)
{
  for (NUNetListIter net_iter = net_list.begin();
       net_iter != net_list.end();
       ++net_iter) {

    NUNet *net = *net_iter;

    NUNetElab *elab_net = net->lookupElab(hier);

    // Create a set of drivers; we may see some drivers multiple times, just add them
    // once.
    FLNodeElabSet driver_set;

    for (NUNet::DriverLoop p = net->loopContinuousDrivers(); !p.atEnd(); ++p) {
      FLNode *flow_node = *p;

      // Skip module instances; hierarchy does not need to be represented in
      // elaborated flow.
      //
      // Module instances will have all of the port connections driving the net
      // as nested flows under the module instance.
      //
      // Note: we just let SysTask nets, controlFlow nets, and extNets
      // through here (even for module instances).  The elaboration will be
      // skipped for module instances in Elaborate::elabFlow, and all
      // all the respective nets will all be aliased together once
      // elaboration is complete (\sa NUDesign::handleElabInternalNets)

      bool internal_net_that_will_be_aliased = (flow_node->getDefNet()->isExtNet() or
                                                flow_node->getDefNet()->isSysTaskNet() or
                                                flow_node->getDefNet()->isControlFlowNet() );

      if (not flow_node->isBoundNode() and
          (flow_node->getUseDefNode()->getType() == eNUModuleInstance) and
          not internal_net_that_will_be_aliased )
      {
        for (Iter<FLNode*> iter = flow_node->loopNested(); !iter.atEnd(); ++iter)
        {
          FLNode* fanin = *iter;

          // Do some sanity checking; the only fanin we should see here is the
          // port connections for this module instance which drive this net.
          FLN_ASSERT(not fanin->isBoundNode(), fanin);
          NUPortConnection *conn = dynamic_cast<NUPortConnection*>(fanin->getUseDefNode());
          NU_ASSERT(conn,fanin->getUseDefNode());
          NU_ASSERT2(conn->getModuleInstance() == flow_node->getUseDefNode(),
                     conn->getModuleInstance(), flow_node->getUseDefNode());

          elabSingleNetDriverFlow(fanin, &driver_set, flow_map, context, stoppers, clock_and_driver, elaboratePorts);
        }
      }
      else
      {
        elabSingleNetDriverFlow(flow_node, &driver_set, flow_map, context, stoppers, clock_and_driver, elaboratePorts);
      }
    }

    // Once flow is elaborated, point the nets to their top-level drivers.
    if (not stoppers) {
      for (FLNodeElabSetIter driver_iter = driver_set.begin();
	   driver_iter != driver_set.end();
	   ++driver_iter) {
        addContinuousDriver(elab_net, *driver_iter);
      }
    }
  }
}


void Elaborate::elabSingleNetDriverFlow(FLNode *flow_node,
                                        FLNodeElabSet *driver_set,
                                        FlowElabMultiMap *flow_map,
                                        LFContext *context,
                                        FLNodeSet *stoppers,
                                        ClockAndDriver * clock_and_driver,
                                        bool elaboratePorts)
{
  elabFlow(flow_node, flow_map, context, stoppers, clock_and_driver, elaboratePorts);
  FlowElabMultiMapRange fanin_range = flow_map->equal_range(flow_node);
  for (FlowElabMultiMapIter fanin_iter = fanin_range.first;
       fanin_iter != fanin_range.second;
	   ++fanin_iter) {
    if (fanin_iter->second != 0) {
      driver_set->insert(fanin_iter->second);
    }
  }
}


void Elaborate::elabScopeFlowSetFlow(FLNodeSet & starting_flow,
                                     FlowElabMultiMap * flow_map,
                                     LFContext * context,
                                     FLNodeSet * stoppers,
                                     ClockAndDriver * clock_and_driver,
                                     bool elaboratePorts)
{
  for (FLNodeSet::iterator iter = starting_flow.begin();
       iter != starting_flow.end();
       ++iter) {
    FLNode * flow = (*iter);

    elabFlow(flow, flow_map, context, stoppers, clock_and_driver, elaboratePorts);
  }
}


/*
 * Hookup the local fanin by using the local flow node map.
 */
void Elaborate::elabConnectFlow(FlowElabMultiMap *flow_map,
                                FLNodeSet * stoppers,
                                ClockAndDriver * clock_and_driver,
                                FLNodeElabSet* covered)
{
  FLElabUseCache levelCache(mNetRefFactory, false);
  FLElabUseCache edgeCache(mNetRefFactory, true);

  // Connect the FLNodeElab objects
  for (FlowElabMultiMapIter iter = flow_map->begin();
       iter != flow_map->end();
       iter++) {
    FLNode *flow_node = iter->first;
    FLNodeElab *flow_node_elab = iter->second;

    if (flow_node_elab == 0) {
      continue;
    }

    if (covered != NULL) {
      if (covered->find(flow_node_elab) != covered->end()) {
        continue;
      } else {
        covered->insert(flow_node_elab);
      }
    }

    if (stoppers and 
        stoppers->find(flow_node) != stoppers->end()) {
      // stoppers are needed to connect as fanin to their fanout, but
      // do not need to be reprocessed themselves.
      continue;
    }

    // Handle block nesting, and make sure there is only 1 elaborated flow node mapped
    // to this nested unelaborated flow.
    if (flow_node->hasNestedBlock()) {
      for (Iter<FLNode*> loop = flow_node->loopNested(); !loop.atEnd(); ++loop) {
        FLNode* nested = *loop;
        FlowElabMultiMapRange nested_range = flow_map->equal_range(nested);
        FLN_ASSERT(nested_range.first != flow_map->end(), nested);
        FlowElabMultiMapIter next = nested_range.first;
        ++next;
        FLN_ASSERT(nested_range.second == next, nested);
        flow_node_elab->addNestedBlock(nested_range.first->second);
      }
    }

    // Bound nodes we see here represent top-level inputs or bids only.
    // Bounds for port connections are not replicated.
    FLNodeBoundElab *flow_node_bound_elab = dynamic_cast<FLNodeBoundElab*>(flow_node_elab);
    if (flow_node_bound_elab != 0) continue;

    // If this flow node is for an output or bid port connection, then connect
    // the fanin from the nested module.
    NUPortConnection *portconn =
      dynamic_cast<NUPortConnection*>(flow_node_elab->getUseDefNode());
    if (portconn and
        (portconn->getFormal() != flow_node_elab->getFLNode()->getDefNet())) {
      // Instead of using the current context->getHier(), use the hier from
      // the flow node.  This is due to hierarchical references; we may be
      // seeing this node from outside the hierarchy of the port connection.
      NUNetElab *formal_elab = helperGetFormalElab(portconn, flow_node_elab->getHier());
      for (NUNetElab::DriverLoop p = formal_elab->loopContinuousDrivers(); !p.atEnd(); ++p)
        connectFanin(flow_node_elab, *p, &levelCache);
      continue;
    }

    // Connect fanin for non-port-connection case
    for (Iter<FLNode*> l = flow_node->loopFanin(); !l.atEnd(); ++l)
    {
      FLNode *fanin_node = *l;
      connectOneFlowFanin(flow_map, stoppers, clock_and_driver, 
                          flow_node_elab, fanin_node,
                          &levelCache,
                          &Elaborate::connectFanin);
    }

    // Connect fanin for non-port-connection case
    for (Iter<FLNode*> l = flow_node->loopEdgeFanin(); !l.atEnd(); ++l)
    {
      FLNode *fanin_node = *l;
      connectOneFlowFanin(flow_map, stoppers, clock_and_driver,
                          flow_node_elab, fanin_node,
                          &edgeCache,
                          &Elaborate::connectEdgeFanin);
    }
  }
}


void Elaborate::connectOneFlowFanin(FlowElabMultiMap *flow_map,
                                    FLNodeSet * stoppers,
                                    ClockAndDriver * clock_and_driver,
                                    FLNodeElab * flow_node_elab, 
                                    FLNode * unelab_fanin,
                                    FLElabUseCache* cache,
                                    void (Elaborate::* connectFaninFn)(FLNodeElab*,FLNodeElab*, FLElabUseCache* cache) )
{
  // Skip module instances.

  // See comment in Elaborate::elabScopeNetListFlow for detailed
  // information.
  if ((not stoppers) and unelab_fanin->hasNestedHier())
  {
    for (Iter<FLNode*> iter = unelab_fanin->loopNested(); !iter.atEnd(); ++iter)
    {
      FLNode *port_connection_fanin = *iter;
      connectOneElabFlowFanin(flow_map, clock_and_driver, flow_node_elab, port_connection_fanin, cache, connectFaninFn);
    }
  }
  else
  {
    connectOneElabFlowFanin(flow_map, clock_and_driver, flow_node_elab, unelab_fanin, cache, connectFaninFn);
  }
}


void Elaborate::connectOneElabFlowFanin(FlowElabMultiMap *flow_map,
                                        ClockAndDriver * clock_and_driver,
                                        FLNodeElab * flow_node_elab, 
                                        FLNode * unelab_fanin,
                                        FLElabUseCache* cache,
                                        void (Elaborate::* connectFaninFn)(FLNodeElab*,FLNodeElab*,FLElabUseCache* cache) )
{
  FlowElabMultiMapRange fanin_range = flow_map->equal_range(unelab_fanin);
  for (FlowElabMultiMapIter fanin_iter = fanin_range.first;
       fanin_iter != fanin_range.second;
       ++fanin_iter) {
    FLNodeElab * fanin_flow_elab = fanin_iter->second;
    NUNetElab  * fanin_net_elab  = fanin_flow_elab->getDefNet();
    bool add_this_arc = true;
    if (clock_and_driver) {
      // If we have a ClockAndDriver structure, our caller wants
      // to preserve only a known set of clock drivers. If this
      // net elab is a clock, only allow the addition of known
      // drivers as elaborated fanin arcs.
      if (clock_and_driver->isClock(fanin_net_elab) and
          not clock_and_driver->isClockDriver(fanin_flow_elab)) {
        add_this_arc = false;
      }
    }

    if (add_this_arc) {
      (this->*connectFaninFn)(flow_node_elab, fanin_flow_elab, cache);
    }
  }
}


void Elaborate::elabModuleDecl(NUModule *module, LFContext *context, ElabPass pass)
{
  bool at_top = context->atTopHierLevel();

  // For top-level modules there is no instance, so need to create
  // a symbol table node.
  if (pass == eElabLocals) {
    if (at_top) {
      STSymbolTable* symtab = context->getSymbolTable();
      STBranchNode *new_branch = NULL;
      new_branch = symtab->createBranch(module->getName(), 0);

      NUModuleElab* modElab = new NUModuleElab(module, 0, new_branch);
      CbuildSymTabBOM::putNucleusObj(new_branch, modElab);
    }
  }

  // Setup context hierarchy for top-level.  If not top-level, caller has setup hierarchy.
  if (at_top) {
    NUModuleElab *module_elab = module->lookupElab(context->getSymbolTable());
    STBranchNode *branch = module_elab->getHier();
    context->pushHier(branch);
  }

  switch (pass) {
  case eElabLocals: {
    // Local elaboration pass, virtual nets included.
    NUNetList net_list;
    module->getAllTopNets(&net_list);
    for (NUNetListIter iter = net_list.begin();
         iter != net_list.end();
         iter++) {
      elabNet(*iter, context);
    }
    elabModuleVirtualNets(module, at_top, context);
    break;
  }
  case eElabHierRefs: {
    // Hierref elaboration pass.
    for (NUNetLoop loop = module->loopNetHierRefs(); not loop.atEnd(); ++loop) {
      elabNetHierRef(*loop, context);
    }
    break;
  }
  case eElabReconData: {
    // Reconstruction data pass.
    NUNetList net_list;
    module->getAllTopNets(&net_list);
    for (NUNetListIter iter = net_list.begin();
         iter != net_list.end();
         iter++) {
      elabNetReconData(*iter, context);
    }
    break;
  }
  }
  
  // Loop only instances at the module level of hierarchy. The
  // instances inside name spaces within the module will be covered later.
  for (NUModuleInstanceMultiLoop iter = module->loopModuleInstances();
       !iter.atEnd(); 
       ++iter) {
    elabModuleInstanceDecl(*iter, context, pass);
  }
  
  for (NUBlockLoop iter = module->loopBlocks();
       not iter.atEnd();
       ++iter) {
    elabBlock(*iter, context, pass);
  }
  for (NUNamedDeclarationScopeLoop iter = module->loopDeclarationScopes();
       not iter.atEnd();
       ++iter) {
    elabDeclarationScope(*iter, context, pass);
  }

  for (NUTaskLoop iter = module->loopTasks();
       not iter.atEnd();
       ++iter) {
    elabTF(*iter, context, pass);
  }

  if (at_top) {
    context->popHier();
  }
}

/*!
  \brief
  Handle the elaboration for the single virtual net returned by the given getVirtualNetFn
  NUModule member function.
  \param module the module (not the instance) that has the virtual nets
  \param getVirtualNetFn NUModule member function which returns the virtual net to elaborate
  \param atTop set to true if the module is the top of the hierarchy
  \param context the context for module (that is the hierarchy for
         module has already been pushed)
 */
void Elaborate::elabModuleVirtualNet(NUModule* module,
                                     NUNet *(NUModule::* getVirtualNetFn)() const,
                                     bool atTop,
                                     LFContext* context)
{
  if ( atTop )
  {
    // at the top we just need to elaborate the virtual net
    elabNet((module->*getVirtualNetFn)(), context);
  }
  else
  {
    // if not at top, make the virtual net of this module be an alias of the
    // virtual net of the parent, 
    // we do this the-hard-way:
    //  1. elab the net;
    //  2. alias it to the parent virtual net; 
    //  3. remove the local net


    STBranchNode* parentHier = context->getHier()->getParent();
    NUModule* parentModule = NUModule::lookup(parentHier, true);

    ST_ASSERT (parentModule, parentHier);

    NUNet *net_unelab = (module->*getVirtualNetFn)();
    elabNet(net_unelab, context);
    NUNetElab *net_elab = net_unelab->lookupElab(context->getHier());
    NU_ASSERT (net_elab, net_unelab);
    NUNetElab* master_net_elab = (parentModule->*getVirtualNetFn)()->lookupElab(parentHier);
    NU_ASSERT2 ( net_elab != master_net_elab, net_elab, master_net_elab);
    net_elab->alias(master_net_elab, false);
    delete net_elab;
  }
}


/*!
  \brief
  Handle the elaboration for the different types of virtual nets in a module
  \param module the module (not the instance) that has the virtual nets
  \param atTop set to true if the module is the top of the hierarchy
  \param context the context for module (note that is the hierarchy for
         module should already have been pushed before the call to this function)
 */
void Elaborate::elabModuleVirtualNets(NUModule* module, bool atTop, LFContext* context)
{
  elabModuleVirtualNet(module, &NUModule::getExtNet, atTop, context);
  elabModuleVirtualNet(module, &NUModule::getOutputSysTaskNetAsNUNet, atTop, context);
  elabModuleVirtualNet(module, &NUModule::getControlFlowNetAsNUNet, atTop, context);
}


/*
 * Elaborate the flow of the given module in the given context, and
 * recurse into its instances.
 *
 * At the end, we will end up with a global flow graph which is connected
 * across hierarchy boundaries.
 *
 * TBD Probably a better way to do this; should we have a list of flow nodes
 *     at the module level that can just be traversed?  The method implemented
 *     here may leave some FlowNodes un-traversed.
 */
void Elaborate::elabModuleFlow(NUModule *module, LFContext *context, bool elaboratePorts)
{
  bool at_top = context->atTopHierLevel();
  if (at_top) {
    NUModuleElab *module_elab = module->lookupElab(context->getSymbolTable());
    STBranchNode *branch = module_elab->getHier();
    context->pushHier(branch);
  }

  // Map of flow nodes to their elaborated counterparts.  An unelaborated flow node may
  // map to multiple elaborated flow nodes (for example, unelaborated flow node for
  // a bound node representing a bidirect port).
  FlowElabMultiMap flow_map;

  // Elaborate the local flow before recurse; this sets up the input port
  // connections first, and makes elabortion of instantiated-modules easier.
  elabScopeLocalsFlow(module, &flow_map, context, elaboratePorts);
  
  for (NUModuleInstanceMultiLoop iter = module->loopInstances();
       !iter.atEnd();
       ++iter)
  {
    elabModuleInstanceFlow(*iter, context, elaboratePorts);
  }
  
  // Connect the local flow after recurse; this makes hooking up to output port
  // connections easier.
  elabConnectFlow(&flow_map);
  
  if (at_top) {
    context->popHier();
  }
}

void Elaborate::elabScopeDecl(STBranchNode * hier,
			      NUBlock * block)
{
  LFContext context(mSymtab,
		    mStrCache,
		    mLocFactory,
		    mFlowFactory,
		    mFlowElabFactory,
		    mNetRefFactory,
		    mArgs,
		    mMsgContext,
                    mIODB, NULL);
  context.initForDesignWalk();
  context.pushHier(hier);
  elabBlock(block, &context, eElabLocals);
  elabBlock(block, &context, eElabHierRefs);
  context.popHier();
}

void Elaborate::elabScopeFlow(STBranchNode * hier, 
			      FLNodeSet & starting_flow,
			      NUNetList & net_list,
			      FLNodeSet * stoppers,
                              ClockAndDriver * clock_and_driver,
                              bool elaboratePorts,
                              FLNodeElabSet* covered)
{
  LFContext context(mSymtab,
		    mStrCache,
		    mLocFactory,
		    mFlowFactory,
		    mFlowElabFactory,
		    mNetRefFactory,
		    mArgs,
		    mMsgContext,
                    mIODB, NULL);
  context.initForDesignWalk();

  context.pushHier(hier);

  // Setup for elaboration
  preFlowElaboration();

  // First, elaborate the flow we want, within the stoppers boundaries.
  FlowElabMultiMap flow_map;
  elabScopeFlowSetFlow(starting_flow, &flow_map, &context, stoppers, clock_and_driver, elaboratePorts);
  elabScopeNetListFlow(hier, net_list, &flow_map, &context, stoppers, clock_and_driver, elaboratePorts);
  elabConnectFlow(&flow_map, stoppers, clock_and_driver, covered);

  // Cleanup after elaboration
  postFlowElaboration();

  context.popHier();
}


void Elaborate::elabModuleInstanceDecl(NUModuleInstance *module_instance,
				       LFContext *context,
                                       ElabPass pass)
{
  STSymbolTable* symtab = context->getSymbolTable();

  STBranchNode * branch = NULL;
  NUModuleElab *modElab = 0;

  // First pass (locals), create a branch node in the symtab for this instance,
  // and also create an elaborated module object.
  //
  // Second pass (hierrefs) & third pass (recondata), just look it up.
  switch (pass) {
  case eElabLocals: {
    STSymbolTableNode *node = symtab->find(context->getHier(), module_instance->getName());
    ST_ASSERT(not node, node);  // if you get this then we already have seen this instance name
    branch = symtab->createBranch(module_instance->getName(),
                                  context->getHier(),
                                  module_instance->getSymtabIndex());
    modElab = new NUModuleElab(module_instance->getModule(),
                               module_instance,
                               branch);
    CbuildSymTabBOM::putNucleusObj(branch, modElab);  
    break;
  }
  case eElabHierRefs: 
  case eElabReconData: {
    STSymbolTableNode *node = symtab->find(context->getHier(), module_instance->getName());
    branch = node->castBranch();
    break;
  }
  }
  NU_ASSERT(branch,module_instance);

  context->pushHier(branch);
  elabModuleDecl(module_instance->getModule(), context, pass);
  context->popHier();
}


/*
 * Elaborate the flow of the given module instance in the given context.
 */
void Elaborate::elabModuleInstanceFlow(NUModuleInstance *module_instance,
                                       LFContext *context,
                                       bool elaboratePorts)
{
  NUModuleElab *module_elab = module_instance->lookupElab(context->getHier());
  STBranchNode *branch = module_elab->getHier();

  context->pushHier(branch);
  elabModuleFlow(module_instance->getModule(), context, elaboratePorts);
  context->popHier();
}
