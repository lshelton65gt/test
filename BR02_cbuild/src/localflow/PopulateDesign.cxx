// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/VerilogPopulate.h"
#include "LFContext.h"
#include "nucleus/NUDesign.h"
#include "util/ArgProc.h"
#include "compiler_driver/CarbonContext.h"
#include "util/CbuildMsgContext.h"
#include "util/AtomicCache.h"
#include "nucleus/NUBitNet.h"
#include "shell/OnDemandMgr.h"

Populate::ErrorCode VerilogPopulate::postDesignFixup(LFContext *context)
{
  ErrorCode err_code = eSuccess;

  ErrorCode temp_err_code = resolveTaskHierRefs(context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  temp_err_code = allPossibleNetResolutions(context);


  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Synthesize change-detect logic for combinational always blocks
  // that have statements inside them with side effects, such as
  // C-Models and system tasks.  The mSensitivityInfoList is created
  // during always block processing, in VerilogPopulate::addAlwaysBlock,
  // but cannot be safely processed until postDesignFixup, when all the
  // hier-refs are resolved.
  NUModule* prevModule = NULL;
  for (SensitivityInfoList::iterator p = mSensitivityInfoList.begin(),
         e = mSensitivityInfoList.end(); p != e; ++p)
  {
    SensitivityInfo& si = *p;
    if (si.module != prevModule) {
      context->clearChangeDetects();
      prevModule = si.module;
    }
    temp_err_code = addSensitivityEvents(context, si.module,
                                         si.always, *si.exprLocList,
                                         si.reasons);
    if (temp_err_code != Populate::eSuccess) {
      err_code = temp_err_code;
    }
    delete si.exprLocList;
  }

  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Add top-level net for OnDemand state
  if (mArg->getBoolValue("-onDemand")) {
    NUModule *topModule = context->getRootModule();
    // Use the top module's source locator
    const SourceLocator &loc = topModule->getLoc();
    StringAtom *name = context->getStringCache()->intern(OnDemandMgr::sIdleSignalName());
    // Create the new net as a scalar.
    NUBitNet *odIdleNet = new NUBitNet(name, static_cast<NetFlags>(eDMRegNet | eAllocatedNet), topModule, loc);
    // Make it protected mutable so it's seen as having a driver
    odIdleNet->putIsProtectedMutableNonHierref(mIODB);
    topModule->addLocal(odIdleNet);
    // The net also needs to be marked observable, but that is done in
    // a pass after the rest of the user-specified directives are
    // parsed.  It can't be done here because the other directives are
    // parsed using a symbol table that hasn't been created yet.
  }

  return err_code;
}
