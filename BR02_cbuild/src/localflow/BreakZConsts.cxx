// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "BreakZConsts.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUTF.h"
#include "reduce/Fold.h"
#include "util/CbuildMsgContext.h"
#include "util/ConstantRange.h"

/*!
  \file
  Break up assigns to constants that include Z, getting rid of the Z parts
 */

//! helper class to break up Zs in procedural assignments. 
/*! Takes assignmens to mixed expressions that include constant Z bits and
 *! breaks them up into segments that are all Z or all non-Z.  For example,
 *!    reg [3:0] r;
 *!    always @(*)
 *!      r = {1'bz,invar,2'bz};
 *! will be transformed to
 *!    always @(*) begin
 *!      r[3] = 1'bz;
 *!      r[2] = invar;
 *!      r[1:0] = 2'bz;
 *!    end
 *! Note that contiguous regions of Z or non-Z will be kept together, 
 *! rather than totally bit-blasting them.
 *! 
 *! This employs BreakZConsts::processAssign(), which is a shared
 *! implementation between continuous assigns and procedural assigns
 */
class BreakZConsts::ProceduralZCallback : public NUDesignCallback {
public:
  //! constructor.
  ProceduralZCallback(BreakZConsts* bzc, IODBNucleus* iodb,
                  NUNetRefFactory* netRefFactory)
    : mBreakZConsts(bzc),
      mScope(NULL),
      mIODB(iodb),
      mNetRefFactory(netRefFactory),
      mWorkDone(false)
  {}

  //! destructor
  ~ProceduralZCallback() {}

  //! keep track of what scope we are in
  Status operator()(Phase phase, NUStructuredProc* proc) {
    if (phase == ePre) {
      mScope = proc->getBlock();
    }
    else {
      mScope = NULL;
    }
    return eNormal;
  }    

  //! We will walk tasks from the module, but not from task-enables.
  //! That way we don't have to maintain stack of scopes
  Status operator()(Phase phase, NUTask* task) {
    if (phase == ePre) {
      mScope = task;
    }
    else {
      mScope = NULL;
    }
    return eNormal;
  }    

  //! break up Z constants in procedural assignment
  Status operator()(Phase phase, NUAssign* assign) {
    if (phase == ePre) {
      NUStmtList newAssigns;
      if (mBreakZConsts->processAssign(assign, &newAssigns)) {
        // We can only get here on procedural assignments, because any
        // mixed-z constants in continuous assignment would already
        // have been broken by the loop over continuous assigns
        // in BreakZConsts::module.
        NU_ASSERT(dynamic_cast<NUContAssign*>(assign) == NULL, assign);

        mWorkDone = true;
        NUBlock* block = new NUBlock(&newAssigns,
                                     mScope, mIODB, mNetRefFactory,
                                     assign->getUsesCFNet(),
                                     assign->getLoc());
        mReplacements[assign] = block;
        return eDelete;
      }
    }
    return eSkip;
  }

  NUStmt* replacement(NUStmt *orig) {
    NUStmtStmtMap::iterator iter = mReplacements.find(orig);
    NU_ASSERT(iter != mReplacements.end(), orig);
    NUStmt *stmt = iter->second;

    // Erase this map element just in case someone else gets the
    // memory used by 'orig' on a subsequent allocation.
    mReplacements.erase(iter);

    return stmt;
  }

  Status operator()(Phase, NUBase*) {
    return eNormal;
  }

  bool workDone() const {return mWorkDone;}

private:
  BreakZConsts* mBreakZConsts;
  NUScope* mScope;
  IODBNucleus* mIODB;
  NUNetRefFactory* mNetRefFactory;
  bool mWorkDone;
  NUStmtStmtMap mReplacements;
}; // class BreakZConsts::ProceduralZCallback : public NUDesignWalker


bool BreakZConsts::module(NUModule *this_module)
{
  mScope = this_module;
  NUStmtList new_cont_assigns;
  NUContAssignList cont_assigns;
  this_module->getContAssigns(&cont_assigns);
  bool cont_assign_changed = false;

  for (NUContAssignListIter iter = cont_assigns.begin();
       iter != cont_assigns.end();
       ++iter)
  {
    NUContAssign* assign = *iter;
    cont_assign_changed |= processAssign(assign, &new_cont_assigns);
  }

  if (cont_assign_changed) {
    NUContAssignList ca_list;
    for (NUStmtList::iterator p = new_cont_assigns.begin(),
           e = new_cont_assigns.end(); p != e; ++p)
    {
      NUStmt* assign = *p;
      NUContAssign* contAssign = dynamic_cast<NUContAssign*>(assign);
      NU_ASSERT(contAssign, assign);
      ca_list.push_back(contAssign);
    }
    this_module->replaceContAssigns(ca_list);
  }

  ProceduralZCallback alwaysZCallback(this, mIODB, mNetRefFactory);
  NUDesignWalker walker(alwaysZCallback, false, false);

  // Walk tasks from modules, not from task-enables
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(this_module);

  mScope = NULL;
  return cont_assign_changed || alwaysZCallback.workDone();
}


//! Split an expression, hopefully into one that's pure Z or purely non-Z.
//! Issue a warning/alert if an impure conversion occurred, but return the
//! split expression anyway
NUExpr* BreakZConsts::splitExpr(NUExpr* expr, const ConstantRange& range,
                                bool expectZ)
{
  CopyContext cc(NULL, NULL);
  NUExpr* split = mFold->createPartsel(expr, range, expr->getLoc());

  // If RHS was not purely Z or non-Z then it must have been something
  // like ena?(a+b):2'b1z, and Fold had trouble reducing it.  Issue
  // an alert and keep going, allowing the user to hang himself by
  // demoting it.
  DynBitVector bits;
  bool anyZ = split->drivesZ(&bits);
  bool allZ = bits.all();
  if ((expectZ && anyZ && !allZ) || (!expectZ && anyZ)) {
    mMsgContext->UnsupportedZExpr(split);
  }
  return split;
}

// Split an lvalue based on the specified range, return NULL upon failure
NULvalue* BreakZConsts::splitLvalue(NULvalue* lval, const ConstantRange& range) {
  CopyContext cc(NULL, NULL);
  lval = lval->copy(cc);
  NULvalue* varselLval = new NUVarselLvalue(lval, range, lval->getLoc());
  varselLval->resize();
  lval = mFold->fold(varselLval);
  return lval;
}

// We only delete continuous assigns, because the DesignWalker
// will take care of deleting blocking assigns when the callback
// returns eDelete
bool BreakZConsts::shouldDeleteAssign(NUAssign* assign) {
  return assign->getType() == eNUContAssign;
}

//! Make an assignment that just covers 'range' and check to see 
//! whether it's either all z-producing or totally z-free
bool BreakZConsts::splitAssignment(NUAssign* assign,
                                   NUStmtList* assign_list,
                                   const ConstantRange& range,
                                   bool expectZ)
{
  NU_ASSERT(range.getMsb() >= range.getLsb(), assign);
  NUExpr* rhs = splitExpr(assign->getRvalue(), range, expectZ);

  // If we are driving a pure constant Z with a continuous assign,
  // then don't even make the assignment, and don't bother to split
  // the lvalue.  Leave procedural assigns to Z in, as we need to
  // treat these as an active "release" of a previously set value
  if (expectZ && (assign->getType() == eNUContAssign)) {
    NUConst* k = rhs->castConst();
    if (k != NULL) {
      DynBitVector zbits;
      if (k->drivesZ(&zbits) && zbits.all()) {
        delete k;
        return true;
      }
    }
  }

  NULvalue* lval = splitLvalue(assign->getLvalue(), range);

  if (lval == NULL) {
    // Couldn't convert back to an lvalue.  Abort the whole splitting
    // operation and leave the original assign as it was, issuing an
    // alert first.  Don't forget to delete all the splits we had
    // created so far.
    mMsgContext->UnsupportedZLhs(assign->getLvalue());
    for (NUStmtList::iterator p = assign_list->begin(),
           e = assign_list->end(); p != e; ++p)
    {
      NUAssign* assign = dynamic_cast<NUAssign*>(*p);
      if (shouldDeleteAssign(assign)) {
        delete assign;
      }
    }
    return false;
  }

  UtString buf;
  buf << "zsplit_" << range.getMsb() << "_" << range.getLsb();
  NUAssign* splitAssign = assign->create(lval, rhs, mScope, buf.c_str());
  splitAssign->resize();
  assign_list->push_back(splitAssign);
  return true;
} // bool BreakZConsts::splitAssignment

//! Split out assignments based on zbits -- we want as few possible
//! splits where every bit in the split is all Z or all non-Z
bool BreakZConsts::splitZAssigns(NUAssign *assign,
                                 NUStmtList *assign_list,
                                 const DynBitVector& zbits)
{
  // Walk through the bits generating new assigns from the
  // old assigns, based on contiguous ranges
  bool ret = true;
  for (DynBitVector::RangeLoop p(zbits, true); ret && !p.atEnd(); ++p) {
    const ConstantRange& range = *p;
    bool z = p.getCurrentState();
    ret = splitAssignment(assign, assign_list, range, z);
  }
  return ret;
}

//! Split out assignments with a concat-lvalue.  This is done
//! because we can't currently codegen enabled drivers with
//! concat-lvalues 
bool BreakZConsts::splitConcatAssigns(NUAssign *assign,
                                      NUStmtList *assign_list,
                                      NUConcatLvalue* clval)
{
  UInt32 startBit = 0;
  for (SInt32 i = clval->getNumArgs() - 1; i >= 0; --i) {
    NULvalue* lv = clval->getArg(i);
    UInt32 sz = lv->getBitSize();
    ConstantRange range(startBit + sz - 1, startBit);
    if (!splitAssignment(assign, assign_list, range, true)) {
      return false;
    }
    startBit += sz;
  }
  return true;
}

//! Process a continuous assign, breaking it out into one or more
//! enabled drivers and smaller continuous assign
bool BreakZConsts::processAssign(NUAssign *assign,
                                 NUStmtList *assign_list)
{
  bool mixedConst = false;
  bool concat = false;
  SourceLocator loc = assign->getLoc(); // assign may be deleted
  bool workDone = processAssignHelper(assign, assign_list, &mixedConst,
                                      &concat);

  if (workDone && mVerbose) {
    if (mixedConst) {
      mMsgContext->LFSplitAssignMixedConst(&loc);
    }
    if (concat) {
      mMsgContext->LFSplitAssignLHSConcatZ(&loc);
    }
  }
  return workDone;
}

// Recursive walker that breaks an assign as much as needed.
bool BreakZConsts::processAssignHelper(NUAssign *assign,
                                       NUStmtList *assign_list,
                                       bool* mixedConst,
                                       bool* concat)
{
  // Detect if a Z exists on the RHS.  If not, analysis is not needed.
  NUExpr *rhs = assign->getRvalue();
  NULvalue *lhs = assign->getLvalue();
  DynBitVector zbits;
  bool modified = false;

  // Joe suggested this assert, but it fires in checkin tests:
  //    test/langcov/binaryop.v
  //    test/langcov/unaryop.v
  //    test/fold/ihk.v
  //    test/case_inference/lhs-memsel-idx.v
  //    test/vhdl/memory/operators_carbon.vhd
  //    test/vhdl/memory/user_meta_comm_dir.vhd
  // plus 14 other nightly tests
  //    
  //NU_ASSERT(rhs->getBitSize() >= lhs->getBitSize(), assign);

  if (rhs->drivesZ(&zbits)) {
    NUStmtList newAssigns;

    if (not zbits.all()) {
      // Split the assignment into segments that are either each zbits.all() or
      // zbits.none().  Extend zbits, if necessary, so it covers the lvalue.
      // ??? zero-extend it or sign-extend it ???
      zbits.resize(lhs->getBitSize()); // this is a zero-extend.
                                       // is this what we want???
      modified = splitZAssigns(assign, &newAssigns, zbits);
      *mixedConst = true;
    }
    else {
      // If the LHS is a concat-lvalue, we can't handle enabled-drivers to
      // concat-lvalues for reasons I don't understand.  Split it up.
      NULvalue* lval = assign->getLvalue();
      NUConcatLvalue* clval = dynamic_cast<NUConcatLvalue*>(lval);
      if (clval != NULL) {
        modified = splitConcatAssigns(assign, &newAssigns, clval);
        *concat = true;
      }
    }

    if (modified) {
      // Get to the bottom of any concat-lvalue tree
      for (NUStmtList::iterator p = newAssigns.begin(),
             e = newAssigns.end(); p != e; ++p)
      {
        NUAssign* newAssign = dynamic_cast<NUAssign*>(*p);
        (void) processAssignHelper(newAssign, assign_list,
                                   mixedConst, concat);
      }
    }
  } // if

  if (not modified) {
    assign_list->push_back(assign);
    return false;               // no change made
  }
  else if (shouldDeleteAssign(assign)) {
    delete assign;
  }

  return modified;
} // bool BreakZConsts::processAssignHelper
