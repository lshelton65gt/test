// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "nucleus/NUModule.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNet.h"
#include "util/CbuildMsgContext.h"


Populate::ErrorCode VerilogPopulate::identLvalue(veNode ve_named,
                                                 LFContext* context,
                                                 NUModule* /* module*/,
                                                 NUIdentLvalue **the_lvalue)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_named);

  NUNet* net = 0;
  ErrorCode temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_named, &net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  checkCoercionLvalue(net, context->getMsgContext(), context, loc);

  *the_lvalue = new NUIdentLvalue(net, loc);
  return err_code;
}

Populate::ErrorCode VerilogPopulate::bitselLvalue(veNode ve_lvalue,
                                                  LFContext* context,
                                                  NUModule* module,
                                                  NULvalue **the_lvalue)
{
  *the_lvalue = 0;
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(ve_lvalue);

  // Is this an array select or a normal bit-select?
  bool arraySelect = veNodeIsA(ve_lvalue, VE_ARRAYBITSELECT) == VE_TRUE;

  veNode ve_named = arraySelect ? veArrayBitSelectGetVariable(ve_lvalue) : veBitSelectGetVariable(ve_lvalue);
  NUNet* net = 0;
  ErrorCode temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_named, &net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  checkCoercionLvalue(net, context->getMsgContext(), context, loc);

  NUExpr *bitsel_expr = 0;

  UInt32 dimension = net->getDeclaredDimensions();
  
  // Cheetah overloads bit-select functionality to represent row-level memory
  // access, so a memory read will get here as a VE_BITSELECT.
  // For bit-level memory access, Cheetah uses VE_ARRAYBITSELECT, which will
  // also get us here.  The two cases can be distinguished by the arraySelect
  // boolean.  When arraySelect is true, we have to use a list of bit-selects,
  // but when it is false we have only a single bit-select expression.

  if (dimension == 2) {
    if (!arraySelect) {
      // this is normal row-level memory access
      temp_err_code = getBitOrArrayBitSelExpr(veBitSelectGetSelectedBit(ve_lvalue),
                                              context, module, 0, loc, 
                                              net->getMemoryNet(), true,
                                              &bitsel_expr);

      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      *the_lvalue = new NUMemselLvalue(net, bitsel_expr, loc);
    } else {
      // this is a bit-level memory access
      CheetahList bitsel_list(veArrayBitSelectGetSelBitList(ve_lvalue));
      int list_size = veListGetSize(bitsel_list);
      if (list_size != 2) {
        POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Array select has unsupported dimensionality"), &err_code);
        return err_code;
      }
      veNode ve_row_select = veListGetNextNode(bitsel_list);
      veNode ve_bit_select = veListGetNextNode(bitsel_list);
      NUExpr* rowsel_expr = 0;
      NUMemoryNet* mem = net->getMemoryNet();

      temp_err_code = getBitOrArrayBitSelExpr(ve_row_select, context, module,
                                              mem->getDepth(), loc, mem, true,
                                              &rowsel_expr);

      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      temp_err_code = getBitOrArrayBitSelExpr(ve_bit_select, context, module, 0,
                                              loc, mem, false, &bitsel_expr);

      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      NUMemselLvalue* memsel = new NUMemselLvalue(mem, rowsel_expr, loc);

      // normalize the bitsel_expr for the memory's operational width range
      *the_lvalue = Populate::genVarselLvalue(mem, memsel, bitsel_expr, loc, true);
      if (*the_lvalue == NULL)
      {
        ExprRangeError( mem, bitsel_expr, loc );
        delete memsel;
        return eFailPopulate;
      }
    }
  } else if (dimension >= 3) {
    CheetahList ve_bitsel_list(veArrayBitSelectGetSelBitList(ve_lvalue));
    veNode ve_bit_element = NULL;
    NUMemoryNet* mem = net->getMemoryNet();
    NUExpr* rowsel_expr = 0;
    ve_bit_element = veListGetNextNode (ve_bitsel_list);

    temp_err_code = getBitOrArrayBitSelExpr(ve_bit_element, context, module,
                                            0/*selfdetermined*/, loc, mem, true,
                                            &rowsel_expr);

    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    NUMemselLvalue* memsel = new NUMemselLvalue(mem, rowsel_expr, loc);
    if ( memsel == NULL )
    {
      mMsgContext->CheetahConsistency( &loc, "Unsupported lvalue memory expression." );
      return eFatal;
    }

    while ((ve_bit_element = veListGetNextNode (ve_bitsel_list)) != NULL) {
    
      NUMemoryNet *memory = memsel->getIdent()->getMemoryNet();
      // Here, we need to know what memory range we are grabbing, whether
      // this index refers to a word/bit.  If it refers to a
      // word the index needs to be jammed into the memsel.  If it
      // refers to a bit, we need to generate a varsel.
      const UInt32 numMemDims = memory->getNumDims();
      const UInt32 numSelDims = memsel->getNumDims();
      bool normalize = false;

      if (numMemDims == numSelDims + 1)
        normalize = true;

      temp_err_code = getBitOrArrayBitSelExpr(ve_bit_element, context, module,
                                              0, loc, mem, !normalize,
                                              &bitsel_expr);

      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      if (normalize)
      {
        *the_lvalue = Populate::genVarselLvalue(mem, memsel, bitsel_expr, loc, true);
        if (*the_lvalue == NULL) {
          ExprRangeError( mem, bitsel_expr, loc );
          delete memsel;
          return eFailPopulate;
        }
      }
      else
      {
        memsel->addIndexExpr( bitsel_expr );
        *the_lvalue = memsel;
      }
    }
  } else {
    NU_ASSERT(!arraySelect, module);
    // normalize all bit references, synthesizing corrective
    // arithmetic as needed.  interra2/misc/MISC132/cbuild.log
    // has a bitsel-lvalue of a scalar, so be careful
    temp_err_code = expr(veBitSelectGetSelectedBit(ve_lvalue),
                         context,
                         module,
                         0,
                         &bitsel_expr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    bitsel_expr->resize(bitsel_expr->determineBitSize()); // self-determined
    *the_lvalue = Populate::genVarselLvalue(net, bitsel_expr, loc, true);
    if (*the_lvalue == NULL)
    {
      VectOrMemExprRangeError( net, bitsel_expr, loc );
      return eFailPopulate;
    }
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::partselLvalue(veNode ve_lvalue,
                                                   LFContext *context,
                                                   NUModule* module,
                                                   NUVarselLvalue **the_lvalue)
{
  *the_lvalue = 0;
  NUNet* net = 0;
  NUExpr* index_expr;
  NUExpr* row_expr;
  NUExpr* col_expr;
  ConstantRange *range;
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(ve_lvalue);
  SInt32 const_value = -1;
  ErrorCode temp_err_code = partsel (ve_lvalue, context, module,
                                     &net, &const_value, &row_expr, &col_expr, &index_expr, &range);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  if ( const_value != -1 ){
    return eFailPopulate;       // it would be an error to have a constant Lvalue (from a genvar)
  }
  checkCoercionLvalue(net, context->getMsgContext(), context, loc);

  if (row_expr)
  {
    row_expr->resize(row_expr->determineBitSize());
    // Limit the size of memory select if it exceeds 32 bits.
    row_expr = limitSelectIndexExpr(net->getMemoryNet(), loc, row_expr);
    NUMemselLvalue* memsel = new NUMemselLvalue(net, row_expr, loc);
    if (col_expr) {
      col_expr->resize(col_expr->determineBitSize());
      // Limit the size of memory select if it exceeds 32 bits.
      col_expr = limitSelectIndexExpr(net->getMemoryNet(), loc, col_expr);
      memsel->addIndexExpr(col_expr);
    }
    *the_lvalue = new NUVarselLvalue (memsel, index_expr, *range, loc);
  }
  else
    *the_lvalue = new NUVarselLvalue (net, index_expr, *range, loc);
  delete range;

  return err_code;
}


Populate::ErrorCode VerilogPopulate::concatLvalue(veNode ve_lvalue,
							 LFContext *context,
							 NUModule* module,
							 NUConcatLvalue **the_lvalue)
{
  *the_lvalue = 0;
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(ve_lvalue);

  NULvalueVector lvalue_vector;
  CheetahList ve_lvalue_iter(veConcatGetConcatList(ve_lvalue));
  if (ve_lvalue_iter) {
    while (veNode ve_lvalue = VeListGetNextNode(ve_lvalue_iter)) {
      NULvalue *cur_lvalue = 0;
      ErrorCode temp_err_code = lvalue(ve_lvalue, context, module, &cur_lvalue);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      if (cur_lvalue) {
	lvalue_vector.push_back(cur_lvalue);
      }
    }
  }

  *the_lvalue = new NUConcatLvalue(lvalue_vector, loc);
  return err_code;
}


Populate::ErrorCode VerilogPopulate::lvalue(veNode ve_lvalue,
                                            LFContext* context,
                                            NUModule* module,
                                            NULvalue **the_lvalue)
{
  *the_lvalue = 0;

  SourceLocator loc = locator(ve_lvalue);

  switch (veNodeGetObjType(ve_lvalue)) {
  case VE_NAMEDOBJECTUSE:
  case VE_SCOPEVARIABLE:
    return identLvalue(ve_lvalue, context, module, (NUIdentLvalue**)the_lvalue);
    break;

  case VE_BITSELECT:
  case VE_ARRAYBITSELECT:
    return bitselLvalue(ve_lvalue, context, module, the_lvalue);
    break;

  case VE_PARTSELECT:
  case VE_ARRAYPARTSELECT:
    return partselLvalue(ve_lvalue, context, module, (NUVarselLvalue**)the_lvalue);
    break;

  case VE_CONCAT:
    return concatLvalue(ve_lvalue, context, module, (NUConcatLvalue**)the_lvalue);
    break;

  default:
    {
      SourceLocator loc = locator(ve_lvalue);
      mMsgContext->CheetahConsistency(&loc, "Unknown lvalue type");
    }
    break;
  }

  return eFailPopulate;
}
