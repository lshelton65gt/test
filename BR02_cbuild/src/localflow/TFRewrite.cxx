// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/TFRewrite.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUTFWalker.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUCost.h"

#include "reduce/RewriteUtil.h"
#include "reduce/RewriteCheck.h"
#include "reduce/Fold.h"

#include "symtab/STSymbolTable.h"
#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"
#include "util/SetOps.h"
#include "util/AtomicCache.h"
#include "iodb/IODBNucleus.h"
#include "hdl/HdlVerilogPath.h"
#include "compiler_driver/CarbonContext.h" // for CRYPT

/*!
  \file
  Implementation of task/function rewrite package.
 */

//! Callback to find invoked tasks and functions
/*!
 * When a task or function invocation is found, call back to the rewriter.
 */
class FindTFCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
  using NUDesignCallback::replacement;
#endif

public:
  FindTFCallback(TFRewrite *rewriter) :
    mRewriter(rewriter)
  {}

  ~FindTFCallback() {}

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * ) { return eNormal; }

  //! Do not walk through module instances.
  Status operator() (Phase, NUModuleInstance *) {
    // We perform the rewrite analysis one module at a time
    // (see TFRewrite::design()), so do not recurse into instantiated modules here.
    return eSkip;
  }

  Status operator() (Phase phase, NUTaskEnable *task_enable)
  {
    if (phase == ePre) {
      mRewriter->rememberTFCall(task_enable->getTask());

      mRewriter->tf(task_enable->getTask());
    }
    return eNormal;
  }

  TFRewrite *mRewriter;
};


class InlineTFCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
  using NUDesignCallback::replacement;
#endif

public:
  InlineTFCallback(TFRewrite *rewriter) :
    mRewriter(rewriter)
  {}

  ~InlineTFCallback() {}

  Status operator() (Phase /* phase */, NUBase * /* node */)
  {
    return eNormal;
  }

  Status operator() (Phase phase, NUModule *module)
  {
    if (phase == ePre)  mScopes.push(module);
    if (phase == ePost) mScopes.pop();
    return eNormal;
  }

  Status operator() (Phase, NUModuleInstance *)
  {
    // We perform the rewrite analysis one module at a time
    // (see TFRewrite::design()), so do not recurse into instantiated modules here.
    return eSkip;
  }

  Status operator() (Phase phase, NUBlock *block)
  {
    if (phase == ePre)  mScopes.push(block);
    if (phase == ePost) mScopes.pop();
    return eNormal;
  }

  Status operator() (Phase phase, NUTF *tf)
  {
    if (phase == ePre)  mScopes.push(tf);
    if (phase == ePost) mScopes.pop();
    return eNormal;
  }

  Status operator() (Phase phase, NUTaskEnable *task_enable)
  {
    if (phase == ePre) {
      // If this task enable is hierarchically referenced, do not
      // recursively process called tasks. During normal processing,
      // they will be processed independently. During block-splitting,
      // we want to allow inlining only if it will trickle back out to
      // the always block.

      if (task_enable->isHierRef()) {
        return eSkip;
      }

      NU_ASSERT((mStmtReplacements.find(task_enable) == mStmtReplacements.end()),task_enable);

      NUStmt *new_stmt = mRewriter->rewriteTaskEnable(task_enable, mScopes.top());
      if (new_stmt) {
        mStmtReplacements[task_enable] = new_stmt;
        return eDelete;
      }
    }
    return eNormal;
  }

  NUStmt* replacement(NUStmt *orig)
  {
    NUStmtStmtMap::iterator iter = mStmtReplacements.find(orig);
    if (iter != mStmtReplacements.end()) {
      NUStmt *stmt = iter->second;

      // Erase this map element just in case someone else gets the
      // memory used by 'orig' on a subsequent allocation.
      mStmtReplacements.erase(iter);

      return stmt;
    } else {
      return 0;
    }
  }

  TFRewrite *mRewriter;
  NUStmtStmtMap mStmtReplacements;
  NUScopeStack mScopes;
};


/*!
 * Inline a task by creating a named block.
 * Make all task locals be local to the named block, including formals.
 * Create blocking assigns from the input actuals to the input formals at the
 * beginning of the block.
 * Create blocking assigns from the output formals to the output actuals at the
 * end of the block.
 *
 * All tasks containing output or inout reference parameters MUST be inlined or
 * we will end up miscomputing UD (according to Aron).
 */
class TaskInliner
{
public:
  TaskInliner(NUTaskEnable *task_enable,
	      NUScope *parent,
	      NUNetRefFactory *netref_factory,
	      AtomicCache *string_cache,
              IODBNucleus *iodb,
              Fold* fold) :
    mTaskEnable(task_enable),
    mTask(task_enable->getTask()),
    mParent(parent),
    mNetRefFactory(netref_factory),
    mStringCache(string_cache),
    mCopyCtx(0),
    mLoc(task_enable->getLoc()),
    mIODB( iodb ),
    mFold( fold )
  {}

  ~TaskInliner()
  {
    if (mCopyCtx) {
      delete mCopyCtx;
      mCopyCtx = 0;
    }
  }

  //! Create a named block to inline the given task enable
  NUBlock *operator()()
  {
    NUBlock *new_block = new NUBlock(NULL, mParent, 
                                     mIODB, mNetRefFactory, 
                                     false, mLoc);

    NU_ASSERT( not mCopyCtx, mTaskEnable);
    mCopyCtx = new CopyContext(new_block, mStringCache, true);

    createLocals(new_block);
    createInputAssignments(new_block);
    replicateBody(new_block);
    createOutputAssignments(new_block);
    rewrite(new_block);
    consistencyCheck(new_block);

    return new_block;
  }

private:

  //! Get mode for this formal parameter
  bool isPassByCopy (const NUNet* formal) const {
    int argIndex = mTask->getArgIndex (formal);
    NU_ASSERT (argIndex >= 0, mTask);

    NUTF::CallByMode mode = mTask->getArgMode (argIndex);

    switch (mode) {
    case NUTF::eCallByCopyOut:
    case NUTF::eCallByCopyInOut:
    case NUTF::eCallByValue:
      return true;

    default:
      return false;
    }
  }


  //! Create locals for the block
  void createLocals(NUBlock *block)
  {
    for (NUNetFilterLoop loop = mTask->loopArgs(eInputNet); not loop.atEnd(); ++loop) {
      if (isPassByCopy (*loop))
        createLocal(*loop, block);
    }
    for (NUNetFilterLoop loop = mTask->loopArgs(eOutputNet); not loop.atEnd(); ++loop) {
      if (isPassByCopy (*loop))
        createLocal(*loop, block);
    }
    for (NUNetFilterLoop loop = mTask->loopArgs(eBidNet); not loop.atEnd(); ++loop) {
      if (isPassByCopy (*loop))
        createLocal(*loop, block);
    }

    createScopeLocals(mTask,block);

    // re-set the master of any NUTempMemoryNets to their new masters.
    for ( NUNetReplacementMap::iterator i = mCopyCtx->mNetReplacements.begin();
          i != mCopyCtx->mNetReplacements.end();
          ++i )
    {
      NUNet *old_net = const_cast<NUNet*>( (*i).first );
      NUTempMemoryNet *old_tmnet = dynamic_cast<NUTempMemoryNet*>( old_net );
      if ( old_tmnet != NULL )
      {
        NUNet *new_net = mCopyCtx->mNetReplacements[old_tmnet];
        NUTempMemoryNet *new_tmnet = dynamic_cast<NUTempMemoryNet*>( new_net );
        NU_ASSERT( new_tmnet != NULL, old_tmnet );
        NUNet *oldmaster = new_tmnet->getMaster();
        NUNet *newmaster = mCopyCtx->mNetReplacements[oldmaster];
        NU_ASSERT( newmaster != NULL && newmaster->isMemoryNet(), new_tmnet );
        new_tmnet->putMaster( newmaster->getMemoryNet( ));
      }
    }
  }

  //! Create locals for a given scope -- including sub declaration scopes.
  /*!
   * nets created in blocks are not handled because they are processed
   * when copying the statements.
   */
  void createScopeLocals(NUScope * scope, NUBlock * block)
  {
    for (NUNetLoop loop = scope->loopLocals(); not loop.atEnd(); ++loop) {
      createLocal(*loop, block);
    }
    for (NUNamedDeclarationScopeLoop loop = scope->loopDeclarationScopes();
         not loop.atEnd();
         ++loop) {
      NUNamedDeclarationScope * declaration_scope = (*loop);
      createScopeLocals(declaration_scope,block);
    }
  }

  //! Create a local based on the original net, scoped to given scope
  NUNet *createLocal(NUNet *orig, NUBlock *block)
  {
    NUNet *net = block->createTempNetFromImage(orig, "taskinline", true);

    // Args will come through here also, clear out the port flags
    net->putIsInput(false);
    net->putIsOutput(false);
    net->putIsBid(false);

    NU_ASSERT((mCopyCtx->mNetReplacements.find(orig) == mCopyCtx->mNetReplacements.end()),orig);
    mCopyCtx->mNetReplacements[orig] = net;
    return net;
  }

  void createInitializer(NUBlock * block, NULvalue * lhs) {
    NUExpr * rhs = NUConst::create(false, 0,  8, lhs->getLoc());
    NUAssign * assign = new NUBlockingAssign(lhs,rhs,mTaskEnable->getUsesCFNet(),lhs->getLoc());
    block->addStmt(assign);
  }

  //! Create a copy of the task body, append onto the given block
  void replicateBody(NUBlock *block)
  {
    for (NUStmtLoop loop = mTask->loopStmts(); not loop.atEnd(); ++loop) {
      NUStmt *copy = (*loop)->copy(*mCopyCtx);
      block->addStmt(copy);
    }
  }

  //! Create blocking assignments from the input actuals to the formals
  void createInputAssignments(NUBlock *block)
  {
    for (NUNetFilterLoop loop = mTask->loopArgs(eInputNet); not loop.atEnd(); ++loop) {
      NUStmt *stmt = createInputAssign(*loop);
      if (stmt)
        block->addStmt(stmt);
    }
    for (NUNetFilterLoop loop = mTask->loopArgs(eBidNet); not loop.atEnd(); ++loop) {
      NUStmt *stmt = createInputAssign(*loop);
      if (stmt)
        block->addStmt(stmt);
    }
  }

  //! Create a blocking assignment from the actual to the input formal
  NUStmt *createInputAssign(NUNet *orig_formal)
  {
    NUNet *formal = NULL;

    int idx = mTask->getArgIndex(orig_formal);
    NU_ASSERT2(idx >= 0,mTask,orig_formal);

    // Only need the assignment if we are passing parameters by copy; If passing
    // by reference, we just substitute the actual for the formal
    bool copyInputs = isPassByCopy (orig_formal);

    NUTFArgConnection *conn = mTaskEnable->getArgConnectionByIndex(idx);
    NUExpr *rhs = 0;
    switch (conn->getDir()) {
      case eInput:
      {
        NUTFArgConnectionInput *in_conn = dynamic_cast<NUTFArgConnectionInput*>(conn);
        if (copyInputs)
          rhs = in_conn->getActual()->copy (*mCopyCtx);
        else {
          rhs = in_conn->getActual ();
          mExprReplacements[orig_formal] = rhs;
          return 0;
        }
        
        break;
      }
      case eOutput:
        NU_ASSERT("Unexpected output connection"==NULL, conn);
        break;
      case eBid:
      {
        NUTFArgConnectionBid *bid_conn = dynamic_cast<NUTFArgConnectionBid*>(conn);
        if (not copyInputs) {
          mLvalueReplacements[orig_formal] = bid_conn->getActual ();
          return 0;
        } else
          rhs = bid_conn->getActual ()->NURvalue ();
        break;
      }
      break;
      default:
        NU_ASSERT("Unknown connection direction"==NULL, conn);
        break;
    }


    // VHDL signal parameters or other call-by-reference situations will not
    // reach here
    NU_ASSERT (copyInputs, orig_formal);

    formal = mCopyCtx->mNetReplacements[orig_formal];
    NU_ASSERT(formal, orig_formal);
    NULvalue *lhs = new NUIdentLvalue(formal, mLoc);
    NUBlockingAssign *assign = new NUBlockingAssign(lhs, rhs, mTaskEnable->getUsesCFNet(), mLoc);    
    return assign;
  }

  //! Create blocking assignments from the output formals to the actuals
  void createOutputAssignments(NUBlock *block)
  {
    for (NUNetFilterLoop loop = mTask->loopArgs(eOutputNet); not loop.atEnd(); ++loop) {
      NUNet * output = (*loop);
      if (isPassByCopy (output)) {
        NUStmt *stmt = createOutputAssign(output);
        block->addStmt(stmt);
      } else {
        addOutputToReplacementMap(output);
      }
    }
    for (NUNetFilterLoop loop = mTask->loopArgs(eBidNet); not loop.atEnd(); ++loop) {
      NUNet *bidi = (*loop);
      if (isPassByCopy (bidi)) {
        NUStmt *stmt = createOutputAssign(bidi);
        block->addStmt(stmt);
      } else {
        addOutputToReplacementMap(bidi);
      }
    }
  }

  NULvalue * getOutputActual(NUNet * formal)
  {
    int idx = mTask->getArgIndex(formal);
    NU_ASSERT2(idx >= 0,mTask,formal);

    NUTFArgConnection *conn = mTaskEnable->getArgConnectionByIndex(idx);
    NULvalue *lhs = 0;
    switch (conn->getDir()) {
      case eInput:
        NU_ASSERT("Unexpected input connection"==NULL, conn);
        break;
      case eOutput:
      {
        NUTFArgConnectionOutput *out_conn = dynamic_cast<NUTFArgConnectionOutput*>(conn);
        lhs = out_conn->getActual();
        break;
      }
      case eBid:
      {
        NUTFArgConnectionBid *bid_conn = dynamic_cast<NUTFArgConnectionBid*>(conn);
        lhs = bid_conn->getActual();
        break;
      }
      break;
      default:
        NU_ASSERT("Unknown connection direction"==NULL, conn);
        break;
    }
    return lhs;
  }

  //! Add a map from output to its lvalue.
  void addOutputToReplacementMap(NUNet * output)
  {
    NULvalue * lvalue = getOutputActual(output);
    mLvalueReplacements[output] = lvalue;
  }

  //! Create a blocking assignment from the formal to the output actual
  NUStmt *createOutputAssign(NUNet *orig_formal)
  {
    NUNet *formal = mCopyCtx->mNetReplacements[orig_formal];
    NU_ASSERT(formal,orig_formal);

    NULvalue * actual = getOutputActual(orig_formal);
    NU_ASSERT(actual,orig_formal);
    NULvalue * lhs = actual->copy(*mCopyCtx);
    NUExpr *rhs = new NUIdentRvalue(formal, mLoc);
    NUBlockingAssign *assign = new NUBlockingAssign(lhs, rhs, mTaskEnable->getUsesCFNet(), mLoc);
    return assign;
  }

  bool rewrite(NUBlock * new_block)
  {
    NULvalueReplacementMap      dummy_lvalue_replacements;
    NUTFReplacementMap          dummy_tf_replacements;
    NUCModelReplacementMap      dummy_cmodel_replacements;
    NUAlwaysBlockReplacementMap dummy_always_replacements;
    RewriteLeaves translator(mCopyCtx->mNetReplacements,
                             mLvalueReplacements,
                             mExprReplacements,
                             dummy_tf_replacements,
                             dummy_cmodel_replacements,
                             dummy_always_replacements,
                             mFold);
    // tbd: add fold object.
    bool success = new_block->replaceLeaves(translator);
    return success;
  }


  void consistencyCheck(NUBlock * new_block)
  {
    NUNetSet task_internals;
    {
      NUNetList task_internals_list;
      mTask->getAllNets(&task_internals_list);
      task_internals.insert(task_internals_list.begin(),task_internals_list.end());
    }

    RewriteCheck consistency(task_internals);
    new_block->replaceLeaves(consistency);
  }

  NUTaskEnable *mTaskEnable;
  NUTask *mTask;
  NUScope *mParent;
  NULvalueReplacementMap mLvalueReplacements;
  NUExprReplacementMap mExprReplacements;
  NUNetRefFactory *mNetRefFactory;
  AtomicCache *mStringCache;
  CopyContext *mCopyCtx;
  SourceLocator mLoc;
  IODBNucleus *mIODB;
  Fold* mFold;
};


TFRewrite::TFRewrite(NUNetRefFactory *netref_factory,
		     MsgContext *msg_ctx,
		     AtomicCache *string_cache,
                     bool inlineSingleTaskCalls,
                     bool inlineLowCostTaskCalls,
		     bool verbose,
                     IODBNucleus *iodb,
                     ArgProc* args) :
  mNetRefFactory(netref_factory),
  mStringCache(string_cache),
  mMsgContext(msg_ctx),
  mInlineNonLocalReferences(false),
  mInlinedTask(false),
  mInlineSingleTaskCalls(inlineSingleTaskCalls),
  mInlineLowCostTaskCalls(inlineLowCostTaskCalls),
  mVerbose(verbose),
  mIODB( iodb ),
  mArgs(args),
  mCost(NULL)
{
  mMultiLevelHierTasks = mArgs->getBoolValue(CRYPT("-multiLevelHierTasks"));
  mFold =  new Fold(mNetRefFactory, mArgs, mMsgContext, mStringCache,
                    mIODB, false, eFoldPreserveReset);
  mDeleteTasks = new NUTaskList;
} // TFRewrite::TFRewrite


TFRewrite::~TFRewrite()
{
  delete mFold;

  // We can't delete the tasks until after Fold is deleted because
  // Fold's expression factory can reference nets owned by the
  // tasks we are going to delete
  for (NUTaskList::iterator p = mDeleteTasks->begin(), e = mDeleteTasks->end();
       p != e; ++p)
  {
    NUTask* task = *p;
    delete task;
  }
  delete mDeleteTasks;
}


void TFRewrite::design(NUDesign *this_design)
{
  mCost = new NUCostContext;
  mCost->calcDesign(this_design);
  NUModuleList modules;
  this_design->getModulesBottomUp(&modules);
  for (NUModuleList::iterator iter = modules.begin();
       iter != modules.end();
       ++iter) {
    NUModule * this_module = *iter;
    module(this_module);
  }
  delete mCost; mCost = NULL;
}


void TFRewrite::module(NUModule *this_module)
{
  mInlinedTask = false;

  // Rewrite local task and functions
  findTFToRewrite(this_module);

  // Stop if there were errors found.
  if (mMsgContext->getSeverityCount(MsgContext::eError) != 0) {
    return;
  }

  // Perform the analysis of tasks before trying to inline them. This
  // allows the inlining pass to inline the task without walking into
  // it -- tasks should not be modified.

  // Find invoked tasks/functions & modify as appropriate
  FindTFCallback find_callback(this);
  NUDesignWalker find_walker(find_callback, false);
  find_walker.module(this_module);

  // Mark singly-called tasks for inline
  if (mInlineSingleTaskCalls or mInlineLowCostTaskCalls) {
    markSingleInstanceTasks(this_module);
  }

  // Inline the marked tasks.
  InlineTFCallback inline_callback(this);
  NUDesignWalker inline_walker(inline_callback, false);
  inline_walker.putWalkReplacements(true);
  inline_walker.module(this_module);

  // Delete any tasks which were inlined.  We do this so that later processing
  // doesn't get confused, since these tasks will not be referenced any more.
  removeInlinedTasks(this_module);


  // If any work was done, call fold.  This will remove any named blocks
  // which were created by the inlining.  This is necessary so that the UD
  // is correct for block-local temporaries.
  if (mInlinedTask) {
    mFold->module(this_module);
  }

  mTaskCalls.clear();
}


void TFRewrite::rememberTFCall(NUTask * this_tf)
{
  (mTaskCalls[this_tf])++; // increment the call count 
}


void TFRewrite::markSingleInstanceTasks(NUModule * this_module)
{
  for (NUTaskLoop loop = this_module->loopTasks();
       not loop.atEnd();
       ++loop) {
    NUTask * task = *loop;
    SInt32 calls = mTaskCalls[task];

    // Request inlining for single-call tasks.
    if (mInlineSingleTaskCalls) {
      if (calls == 1) {
        task->putRequestInline(true);
      }
    }

    // Request inlining for low-cost(local) and low-cost(total) tasks.

    // This does not yet happen by default because a couple of large
    // designs (notably Stargen/Merlin) show a 4-10% drop in
    // performance.

    if (mInlineLowCostTaskCalls) {
      NUCost * cost = mCost->findCost(task);
      // Costs are not computed for zero-call tasks.
      if (cost) {
        SInt32 run_instructions = cost->mInstructionsRun;
        if ((calls == 1) or (run_instructions < 100) or ((calls * run_instructions) < 500)) {
          task->putRequestInline(true);
        }
      }
    }
  }
}


bool TFRewrite::alwaysBlock(NUAlwaysBlock * always)
{
  mInlineNonLocalReferences = true;
  mInlinedTask = false;

  // Perform the analysis of tasks before trying to inline them. This
  // allows the inlining pass to inline the task without walking into
  // it -- tasks should not be modified.

  // Find task calls within this always block with out-of-scope
  // reads/writes and inline them.
  FindTFCallback find_callback(this);
  NUDesignWalker find_walker(find_callback, false);
  find_walker.alwaysBlock(always);

  // Inline the marked tasks.
  InlineTFCallback inline_callback(this);
  NUDesignWalker inline_walker(inline_callback, false);
  inline_walker.putWalkReplacements(true);
  inline_walker.putWalkTasksFromTaskEnables(false);
  inline_walker.alwaysBlock(always);

  mInlineNonLocalReferences = false;
  return mInlinedTask;
}


void TFRewrite::removeInlinedTasks(NUModule *module)
{
  UtSet<NUTask*> removed_tasks;
  for (NUTaskLoop loop = module->loopTasks(); not loop.atEnd(); ++loop) {
    NUTask *task = *loop;
    if (task->shouldInline()) {
      if (mVerbose) {
	UtIO::cout() << "Due to inlining, removing task " 
                     << module->getName()->str() << "."
                     << task->getName()->str() << UtIO::endl;
      }
      removed_tasks.insert(task);
    }
  }

  for (UtSet<NUTask*>::iterator iter = removed_tasks.begin();
       iter != removed_tasks.end();
       ++iter)
  {
    NUTask* task = *iter;
    module->removeTask(task);
    mDeleteTasks->push_back(task);
  }
}


void TFRewrite::findTFToRewrite(NUModule *module)
{
  for (NUTaskLoop loop = module->loopTasks(); not loop.atEnd(); ++loop) {
    tf(*loop);
  }
}


void TFRewrite::tf(NUTF *this_tf) 
{
  if (haveVisited(this_tf)) {
    return;
  }

  // Set that we've visited immediately to avoid infinite recursion
  rememberVisited(this_tf);

  // Find invoked tasks/functions & modify as appropriate
  FindTFCallback find_callback(this);
  NUDesignWalker find_walker(find_callback, false);
  find_walker.tf(this_tf);

  // Handle rewrite of this task.
  analyzeRewrite(this_tf);
}


NUStmt* TFRewrite::rewriteTaskEnable(NUTaskEnable *task_enable, NUScope *scope)
{
  NUTask *task = task_enable->getTask();
  if (task->shouldInline()) {
    return inlineTask(task_enable, scope);
  }
  
  return 0;
}


NUBlock *TFRewrite::inlineTask(NUTaskEnable *task_enable, NUScope *scope)
{
  mInlinedTask = true;
  TaskInliner inliner(task_enable, scope, mNetRefFactory, mStringCache, mIODB,
                      mFold);
  NUBlock *block = inliner();
  return block;
}


void TFRewrite::analyzeRewrite(NUTF *tf)
{
  NUTask *task = 0;
  if (tf->getType() == eNUTask) {
    task = dynamic_cast<NUTask*>(tf);
    NU_ASSERT(task,tf);
  }

  // If this is going to be inlined, don't perform any analysis here.
  if (task and task->shouldInline()) {
    return;
  }

  // Find non-blocking assigns.
  // Find defs/refs to non-locals.
  NUTFWalkerCallback callback;
  NUDesignWalker walker(callback, false);
  callback.setWalker(&walker);
  walker.tf(tf);

  // If we must inline, do not create args for out-of-scope references; that
  // will be resolved through inlining.
  if (callback.mMustInline) {
    if (task) {
      task->putRequestInline(true);
      if (!task->canInline()) {
        if (task->hasHierRef()) {
          mMsgContext->TaskNonBlockingHierRefs(tf);
        }
        else if (task->isRecursive()) {
          mMsgContext->TaskNonBlockingRecursive(tf);
        }
        else {
          // There must be another reason, so a message needs to be
          // added here
          NU_ASSERT(0, tf);
        }
      }
      return;
    } else {
      mMsgContext->FunctionNonBlock(tf);
      return;
    }
  }

  // If a task is hierarchically referenced, and the task has any hierarchical
  // task enables, then issue an error message.  This is currently not supported
  // (see bug 2153).
  // There is limited support for this using the -multiLevelHierTasks switch.
  if( task and task->hasHierRef() and callback.mHasTaskHierRefs and
      not mMultiLevelHierTasks ) {
    mMsgContext->TaskHierRefInHierRef(task);
  }

  // If a task is hierarchically referenced, and the task has any non-local
  // references, then need to make the modules containing the hierarchical
  // references to the tasks to also contain hierarchical references to
  // the non-locals used by the task.
  //
  // If we are supporting multi level hier tasks, then the UD for the task will
  // not include the non-locals.  So we do not need to create them here.
  if (task and task->hasHierRef() and not mMultiLevelHierTasks and not mInlineNonLocalReferences) {
    createTaskNonLocalHierrefs(task, callback.mNonLocalUses, callback.mNonLocalDefs);
  }

  // We may want to inline all tasks with non-local references.
  if (task and mInlineNonLocalReferences) {
    if ( (not callback.mNonLocalUses.empty()) or
         (not callback.mNonLocalDefs.empty()) ) {
      task->putRequestInline(true);
      return;
    }
  }
}


bool TFRewrite::haveVisited(NUScope *scope) const
{
  return (mScopeSet.find(scope) != mScopeSet.end());
}


void TFRewrite::rememberVisited(NUScope *scope)
{
  mScopeSet.insert(scope);
}


void TFRewrite::createTaskNonLocalHierrefs(NUTask *task,
                                           NUNetSet &non_local_uses,
                                           NUNetSet &non_local_defs)
{
  NUNetSet non_locals(non_local_uses);
  non_locals.insert(non_local_defs.begin(), non_local_defs.end());

  for (NUNetSet::iterator iter = non_locals.begin();
       iter != non_locals.end();
       ++iter) {
    createTaskNonLocalHierref(task, *iter);
  }
}


void TFRewrite::createTaskNonLocalHierref(NUTask *task, NUNet *non_local)
{
  for (NUTaskEnableLoop loop = task->loopHierRefTaskEnables();
       not loop.atEnd();
       ++loop) {
    NUTaskEnableHierRef *task_enable = dynamic_cast<NUTaskEnableHierRef*>(*loop);
    NU_ASSERT(task_enable,*loop);

    // If the non_local is not a hierarchical reference, create a hierarchical
    // reference to it in the local module's scope.
    if (not non_local->isHierRef()) {
      AtomArray basePath;
      basePath.push_back(non_local->getName());
      createTaskEnableNonLocalHierref(task_enable, non_local, basePath);
    } else { 
      // The non_local is itself a hierarchical reference.
      NUNetHierRef *non_local_hier_ref = non_local->getHierRef();
      NU_ASSERT(non_local_hier_ref,non_local);
      if (non_local_hier_ref->isLocallyRelative()) {
        // If the non_local is locallyResolvable wrt the task's module, create
        // a hierref to the resolution.
        createTaskEnableNonLocalHierref(task_enable,
                                        non_local_hier_ref->getLocallyResolvedNet(),
                                        non_local_hier_ref->getPath());
      } else {
        // Otherwise, replicate the hierref into this task_enable's module.
        NUModule *module = task_enable->getModule();
        cloneHierRef(non_local, module);
      }
    }
  }
}


void TFRewrite::createTaskEnableNonLocalHierref(NUTaskEnableHierRef *task_enable,
                                                NUNet *non_local,
                                                const AtomArray& baseName)
{
  // Prepend the invoked task's path so that the reference will be
  // relative to the module in which the task enable occurs.
  AtomArray full_net_name = task_enable->getHierRef()->getPath();

  // the last element is the task name.  Replace that with the net name
  NU_ASSERT(full_net_name.size() >= 1, non_local);
  full_net_name.resize(full_net_name.size() - 1);
  full_net_name.insert(full_net_name.end(), baseName.begin(), baseName.end());

  NUModule *module = task_enable->getModule();
  NUNet *existing_hierref = module->findNetHierRef(full_net_name);

  // If there is already a hierarchical reference for this net, don't create
  // another one.
  if (not existing_hierref) {
    NetFlags flags = NetFlags(non_local->getDeclarationFlags() | eAllocatedNet);
    NUNet *new_hierref = non_local->createHierRef(full_net_name, module, flags, task_enable->getLoc());
    non_local->addHierRef(new_hierref);
    new_hierref->getHierRef()->addResolution(non_local);
    module->addNetHierRef(new_hierref);
  }
}


NUNet* TFRewrite::cloneHierRef(NUNet *net_hier_ref, NUModule *module)
{
  NUHierRef* href = net_hier_ref->getHierRef();
  NU_ASSERT(href, net_hier_ref);
  return net_hier_ref->cloneHierRef(module, href->getPath());
}
