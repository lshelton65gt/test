// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef LFCONTEXT_H_
#define LFCONTEXT_H_

#include "compiler_driver/Interra.h"
#include "flow/Flow.h"
#include "util/SourceLocator.h"
#include "util/UtStack.h"
#include "util/UtSet.h"
#include "util/UtStringUtil.h"      // for StringHashInsensitive
#include "iodb/IODBNucleus.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUCase.h"
#include "localflow/PplLocalConstProp.h"
#include "localflow/VhdlPopulate.h"

class MsgContext;
class STSymbolTable;
class STBranchNode;
class AtomicCache;
class PplLocalConstProp;
class LFContext;
class StmtBlockContext;
class Fold;
class ArgProc;

extern void unsetVariableInitialValue( vhNode node ); // defined in VhPopulateAssign.cxx

//! Statement factory to be used by population for statement creation.
/*!
  All statements populated should be created using this class. If they're directly
  newed, this class won't see them and the sanity check will fail. The statements should
  be created in the order they'll execute. For example, for initial statements should
  be created prior to for body statements and finally the for advance statements. The
  sanity check will fail if this is not followed. This is currently used just
  by VHDL population code.
*/
class PplStmtFactory
{
public:

  PplStmtFactory(Fold* fold, NUNetRefFactory* factory, IODBNucleus* iodb,
                 ArgProc* args, MsgContext* msgContext,  
                 VhdlPopulate::EntityAliasMap* alias_map = NULL);
  ~PplStmtFactory();

  // Tasks and Structured Procs ..initial/always blocks 

  NUTask* createTask(NUModule *parent, STBranchNode * name_branch,
                     const SourceLocator& loc);
  NUTask* createTask(NUModule *parent, StringAtom* name, StringAtom* origName,
                     const SourceLocator& loc);
  
  NUAlwaysBlock* createAlwaysBlockForJaguarFlow(StringAtom * name, const SourceLocator& loc);
  NUInitialBlock* createInitialBlock(StringAtom * name, const SourceLocator& loc);

  // Statements

  // Assign statement creation routines.
  NUBlockingAssign* createBlockingAssign(NULvalue* lvalue, NUExpr* rvalue,
                                         bool usesCFNet, const SourceLocator& loc,
                                         bool do_resize = true);
  NUNonBlockingAssign* createNonBlockingAssign(NULvalue *lvalue, NUExpr *rvalue,
                                               bool usesCFNet,
                                               const SourceLocator& loc,
                                               bool do_resize = true);
  NUContAssign* createContAssign(StringAtom *name, NULvalue *lvalue,
                                 NUExpr *rvalue, const SourceLocator& loc,
                                 Strength strength=eStrDrive,
                                 bool do_resize = true);

  // Task enables creation routines.
  NUTaskEnable* createTaskEnable(NUTask *task, NUTFArgConnectionVector& argConns,
                                 NUModule* parentModule, bool usesCFNet,
                                 const SourceLocator& loc);
  NUTaskEnableHierRef* createTaskEnableHierRef(const AtomArray& path,
                                               StringAtom *taskName, NUScope *scope,
                                               bool usesCFNet,
                                               const SourceLocator &loc);

  // Break statement creation routine.
  NUBreak* createBreak(NUBlock* block, StringAtom* keyword, StringAtom* targetName,
                       const SourceLocator& loc);

  // System task creation routines.
  NUReadmemX* createReadmemX(StringAtom* name, NUExprVector& exprs,
                             const NUModule* module, NULvalue *lvalue,
                             StringAtom* file, bool hexFormat, SInt64 startAddress,
                             SInt64 endAddress, bool endSpecified,
                             bool usesCFNet, const SourceLocator& loc);
  NUOutputSysTask* createOutputSysTask(StringAtom* name, NUExprVector& exprs,
                                       const NUModule* module, bool hasFileSpec,
                                       bool appendNewline, SInt8 timeUnit,
                                       SInt8 timePrecision,
                                       bool usesCFNet, bool isVerilogTask,
                                       bool isWriteLineTask, bool hasInstanceName,
                                       STBranchNode* where, const SourceLocator& loc);
  NUFCloseSysTask* createFCloseSysTask(StringAtom* name, NUExprVector& exprs,
                                       const NUModule* module,
                                       bool usesCFNet, bool isVerilogTask,
                                       bool useInputFileSystem,
                                       const SourceLocator& loc);
  NUFOpenSysTask* createFOpenSysTask(StringAtom* name, NUNet* outNet,
                                     NUIdentLvalue* statusLvalue,
                                     NUExprVector& exprs, const NUModule* module,
                                     bool usesCFNet,
                                     bool isVerilogTask, bool useInputFileSystem,
                                     const SourceLocator& loc);
  NUInputSysTask* createInputSysTask(StringAtom* name, NUExprVector& exprs,
                                     NULvalue *lvalue, NULvalue *statusLvalue,
                                     const NUModule* module,
                                     bool usesCFNet, bool isVerilogTask,
                                     bool isReadLineTask, const SourceLocator& loc);
  NUFFlushSysTask* createFFlushSysTask(StringAtom* name, NUExprVector& exprs,
                                       const NUModule* module,
                                       bool usesCFNet, const SourceLocator& loc);
  NUControlSysTask* createControlSysTask(StringAtom* name,
                                         CarbonControlType controlType,
                                         NUExprVector& exprs, const NUModule* module,
                                         bool usesCFNet, const SourceLocator& loc);

  //! Block statements creation routines.
  /*!
    The block statements like if-then, if-else, if-then-else, for, case, caseitems etc
    should be first created with empty bodies. Then statements should be populated in
    constant propagation order and added to block statements with notification
    to statement factory by calling next().

    Here's an example for population of if-then-else statement.

    NUIf* ifstmt = createIf(cond, false, loc);
    // Create then statements using factory's statement creation routines..
    ...
    // Indicate factory that then statements are done.
    ifstmt->replaceThen(&then_stmts);
    stmtFactory->next(ifstmt);
    // Create else statements using factory's statement creation routines..
    ...
    // Indicate factory that else statement and therefore the entire if statement
    // is done.
    ifstmt->replaceElse(&else_stmts);
    stmtFactory->next(ifstmt);

    Note that factory remembers that if statement was created. Unless you call next(),
    the factory will continue under assumption that all statements created are for
    the then block. If you hit another if statement in the then block, then next()
    should be called for it's then and else blocks prior to calling next() for the
    then block.
  */

  //! Populate if statement with empty then and else blocks. Factory expects for
  //! next() to be called for then() block prior to populating else() statement.
  NUIf* createIf(NUExpr *cond, bool usesCFNet, const SourceLocator& loc);

  //! Push the else context.Factory expects for next() to be called for else()
  //! statement prior to the statement after it.
  void createElse(NUIf *else_stmt);

  //! Populate case statement with no case items. Factory expects next() to be
  //! called after all case items are done.
  NUCase* createCase(NUExpr* sel, NUCase::CaseType type, bool usesCFNet,
                     const SourceLocator& loc);
  //! Populate case item with no statements. Factory expects next() to be called
  //! after all statements have been populated.
  NUCaseItem* createCaseItem(const SourceLocator& loc);
  //! Populate for with empty initial/advance/body statement blocks. Factory()
  //! expects next() to be called for initial, then body and finally advance
  //! before populating the statement after it.
  NUFor* createFor(NUExpr* condition, bool usesCFNet, const SourceLocator & loc,
                   NUNetSet& forStmtDefs);
  //! Populate empty block. Factory() expects next() to be called to finish
  //! populating the block.
  NUBlock* createBlock(NUScope * parent, bool uses_cf_net, const SourceLocator & loc);

  //! Closes the current statement block context and moves to next context if
  //! any or pops the one on the stack.
  /*!
    Supply the statement block the next is being called for. It is used to
    check that factory and it's user is in sync. If it's not an error message
    is display and the function returns false.
  */
  bool next(NUBase* nextForBlock);

  //! Notify factory that a case item was optimized out during population,
  //! after it was populated.
  void removeCaseItem(NUCase* caseStmt, NUCaseItem* caseItem);

  //! Perform a sanity check on populated nucleus to see if all the
  //! statements and statement blocks have been notified.
  bool sanityCheckPopulatedNucleus(NUDesign* design);

  //! Indicates beginning of statements that are created and added to
  //! a context other that current context.
  /*!
    It doesn't happen very often that statements are created that don't
    belong to current context. However initialization declarations for
    VHDL processes are done outside the always block for the process.
    These initializations ought not be seen by statement factory
    to be in current context, thus failing sanity check.
  */
  void outOfContextStmtsBegin();

  //! End of population of out of context statements.
  void outOfContextStmtsEnd();

  //! Copy given statement list and return the copy.
  void copyStmtList(NUStmtList& srcList, NUStmtList* dstList, CopyContext cc);

  //! Optimize the given expression with current propagated constant values.
  NUExpr* optimizeExpr(NUExpr* expr);

  void setCurrentDeclarationScope(NUScope* declScope);

private:

  void addSeqScope(NUBase* scope, PplLocalConstProp::StmtBlockT scopeType);
  void addItem(NUCaseItem* item);
  void addStmt(NUStmt* stmt, NUNetSet* forStmtDefs = NULL);

  Fold*                mFold;
  NUNetRefFactory*     mFactory;
  IODBNucleus*         mIODB;
  MsgContext*          mMsgContext;
  StmtBlockContext*    mBlockContext;
  NUScope*             mCurrentDeclarationScope;
  bool                 mIsConstPropEnabled;
  VhdlPopulate::EntityAliasMap* mAliasMap;   // Map of vhdl Aliases
};

// please do not use "using namespace std"

//! LFContext class
/*!
 * Convenience class to help the local flow walk.
 */
class LFContext
{
public:
  LFContext(STSymbolTable *symtab,
	    AtomicCache *str_cache,
	    SourceLocatorFactory *loc_factory,
	    FLNodeFactory *flow_factory,
	    FLNodeElabFactory *flow_elab_factory,
	    NUNetRefFactory *netref_factory,
            ArgProc* args,
	    MsgContext *msgContext,
	    IODBNucleus*, Fold*,
            VhdlPopulate::EntityAliasMap* alias_map = NULL,
            VHDLCaseT caseMode = eVHDLPreserve);

  ~LFContext();

  void initForDesignWalk();

  void markVisited(NUModule* module);
  bool queryVisited(NUModule* module);

  void setDesign(NUDesign *design);
  NUDesign* getDesign() const;

  //! The factory that population stage MUST use to create new statements.
  /*!
    Any statements that are created directly by new'ing them will not be
    known to the factory and will cause the sanity check performed at the end
    of population to fail.
  */
  PplStmtFactory* stmtFactory() { return &mStmtFactory; }

  /*! \class JaguarScope
   * \brief A simple object to hold the data to be tracked by the Jaguar Scope stack
   *
   * All the management of this object is done by the
   * LFContext::push/popJaguarScope().  The insertion of variables into
   * the cleanup set should only be done in VhdlPopulate::variableAssign.
   */
  class JaguarScope
  {
  public:
    JaguarScope(vhNode scope)
      : mScopeNode(scope), mIsTrailingResetProcessScope(false) {}
    ~JaguarScope()
    {
      doVariableCleanup();
    }
    /*! We use Jaguar to perform a poor-mans constant propagation.  When
     *  we exit a scope where a variable has been written, we need to
     *  clear the jaguar initial value.  This routine records the fact
     *  that we need to perform this cleanup on exit.
     */
    void addCleanupVariable( vhNode vh_var );
    /*! Perform the variable cleanup required upon exiting a scope.
     *  This also occurs on entering a loop scope, because statements we
     *  haven't populated yet may change a value we think is constant.
     */
    void doVariableCleanup();

    vhNode getVhScope() const { return mScopeNode; }

    //! Saves any initial values for variables in the list. If this is for
    //! a recursive function, their initial values are disconnected and
    //! restored when restoreInitialValues() is called.
    void saveInitialValues( JaguarList *vh_decls, bool isRecursive,
                            bool allowSignals = false );
    //! Saves the initial value for the variable
    void saveInitialValue( vhNode vh_var, vhExpr vh_value );
    //! restores the initial value for the variable after recursion completes
    void restoreInitialValues();
    //! Set the current context scope as a trailing reset process scope
    void setIsTrailingResetProcessScope();
    //! Is the current context scope a trailing reset process scope?
    bool isTrailingResetProcessScope();
  private:
    //! hidden & unimplemented
    JaguarScope();
    //! hidden & unimplemented
    JaguarScope( const JaguarScope &scope );
    //! hidden & unimplemented
    JaguarScope& operator=( const JaguarScope & scope );

    //! The actual scope node
    vhNode mScopeNode;
    //! Is this scope a trailing reset process scope?
    bool mIsTrailingResetProcessScope;
    /*! \brief Variables that need to have their initial values cleared
     *  upon exiting the scope.
     *
     * In order to be able to populate certain constructs we need to be
     * able to track the current static value of a variable in the Jaguar
     * domain.  This is kind of a poor-man's constant propagation.  The
     * initial value is set whenever assigning a static value to a
     * variable in VhdlPopulate::variableAssign, and is cleared upon
     * either assigning a nonstatic value, or leaving the scope of the
     * assignment.  This set tracks the variables that need to have their
     * initial values cleared upopn scope exit.  In this context, a scope
     * may be a if, case, or loop sequential statement as well as a
     * process or subprogram body.
     */
    UtSet<vhNode> mCleanupVariableSet;
    UtMap<vhNode, vhExpr> mSavedInitialValuesMap;
  };

  void pushJaguarScope(vhNode vh_scope);
  vhNode getJaguarScope() const;
  vhNode popJaguarScope();
  //! Set the current Jaguar scope as a trailing reset process scope
  void setIsTrailingResetProcessScope();
  //! Is the current Jaguar scope a trailing reset process scope?
  bool isTrailingResetProcessScope();
  //! Provide a stack of current jaguar nodes. The list contains elements ordered
  //! by their stack location. The node at top of the stack is first in the list.
  void getJaguarNodeStack(SInt32 stackLimit, UtList<vhNode>& node_stack) const;
  void addJaguarUninitVariable( vhNode vh_var );
  void saveJaguarInitialVariableValue( vhNode vh_decl, vhExpr vh_value );
  void saveJaguarInitialVariableValues( JaguarList* decl_list, bool isRecursive );
  //! Remove initial values for function parameters and save them. This way
  //! they don't participate in aggresive evaluation of expressions in recursive
  //! functions. They're restored when recursive function population is done.
  void saveRecursiveFuncCallArgInitValues(vhNode vh_function);
  void restoreJaguarInitialVariableValues();

  void pushDeclarationScope(NUScope* scope);
  NUScope* getDeclarationScope() const;
  NUScope* popDeclarationScope();

  void pushBlockScope(NUScope* scope);
  NUScope* getBlockScope() const;
  NUScope* popBlockScope();

  NUModule* getModule() const;
  NUModule* getLastVisitedModule() const;
  

  void addEdgeExpr(NUEdgeExpr* expr);
  NUEdgeExprList *getEdgeExprList();
  void pushEdgeExprList(NUEdgeExprList* expr_list);
  NUEdgeExprList *popEdgeExprList();

  bool isValidForEdgeExpr() {
    return (not mEdgeListStack.empty());
  }

  /*! save expr with the location of actual use in the current event list
   * 
   * Note expr->getLoc() points to one of the places where expr appears,
   * most likely not the place where the expression is used as an event.
   * use_loc points to the specific line where the expression is used.
   */
  void addEventExpr(NUExpr* expr, const SourceLocator &use_loc);
  NUExprLocList *getEventExprList();
  void pushEventExprLocList(NUExprLocList* expr_loc_list);
  NUExprLocList *popEventExprLocList();

  bool isValidForEventExpr() {
    return (not mEventListStack.empty());
  }

  bool getSynthSwitch()
  {
    return mSynth;
  }

  //! Reasons for needing sensitivity events
  enum SensitivityReasons
  {
    eOutSysTask,        //!< Contains Verilog output systems tasks
    ePLICall,           //!< Contains a Verilog PLI call
    eSideEffectCmodel   //!< Contains a c-model with side effects
  };

  void clearRequiresSensitivityEvents() {
    mRequiresSensitivityEvents = 0;
  }

  void putRequiresSensitivityEvents(SensitivityReasons reason) {
    mRequiresSensitivityEvents |= (1 << reason);
  }
  bool getRequiresSensitivityEvents(UtString* reasons = NULL) const
  {
    if (mRequiresSensitivityEvents == 0) {
      // It does not require sensitivity events
      return false;
    }

    // Create a string with the events
    if (reasons != NULL) {
      const char* sep = "";
      const char* allreasons[] = { "Output Sys Task",
                                   "PLI Call",
                                   "C-Model w/ Side Effects" };
      for (int i = eOutSysTask; i <= eSideEffectCmodel; ++i) {
        if ((mRequiresSensitivityEvents & (1 << i)) != 0) {
          *reasons << sep << allreasons[i];
          sep = "; ";
        }
      }
    }
    return true;
  }

  void addStmt(NUStmt* stmt);
  void addStmts(NUStmtList* stmts);
  NUStmtList *getStmtList();
  void pushStmtList(NUStmtList *stmt_list);
  NUStmtList *popStmtList();
  
  //! Return true if the statement stack is empty
  bool isStmtStackEmpty() const;

  //! saves a task enable for later inclusion in the current module.
  void addExtraTaskEnable(NUStmt * task_enable);

  //! remove a task enable from the saved list, and return it
  NUStmt *extractAnExtraTaskEnable();

  //! push the context for saving extra task enable stmts
  void pushExtraTaskEnableContext();
  //! pop the context for saving extra task enable stmts
  void popExtraTaskEnableContext();

  //! Clear out the task/function visit set for a new task/function population
  void clearTFVisitSet();

  //! Return true if the given node is in the task/function visit set
  bool getTFVisit(veNode ve_tf) const;

  //! Remember the given node as being visited in task/function traversal
  void addTFVisit(veNode ve_tf);

  //! Un-remember the given node as being visited in task/function traversal
  void removeTFVisit(veNode ve_tf);

  //! Lookup a module based on module name
  /*!
    All parameterized module names are unique. This is done by
    appending with underscores the value of each parameter in the
    instance list.
  */
  NUModule* lookupModule(StringAtom* name) const;

  //! Map a module name to a module
  /*!
    \sa lookupModule
  */
  void mapModule(StringAtom* name, NUModule* module);

  void pushFlowMaps(FLNodeMultiMap *use_map, FLNodeMap *def_map,
		    FLNodeMultiMap* edge_use_map = 0);
  FLNodeMultiMap *getUseMap();
  FLNodeMultiMap *getEdgeUseMap();
  FLNodeMap *getDefMap();
  void popFlowMaps();

  STSymbolTable* getSymbolTable() const { return mSymtab; }
  AtomicCache* getStringCache() const { return mStrCache; }

  SourceLocatorFactory* getSourceLocatorFactory() const { return mLocFactory; }
  FLNodeFactory* getFlowFactory() const { return mFlowFactory; }
  FLNodeElabFactory* getFlowElabFactory() const { return mFlowElabFactory; }
  NUNetRefFactory *getNetRefFactory() const { return mNetRefFactory; }

  // Return true if currently at the top level of the design (no hierarchy yet)
  bool atTopHierLevel() const;
  STBranchNode* popHier();
  void pushHier(STBranchNode* node);
  STBranchNode* getHier() const;

  MsgContext* getMsgContext() {return mMsgContext;}

  bool doCoercePorts() const { return mCoercePorts; }
  bool doShareConstants() const { return mShareConstants; }
  bool doInlineTasks() const { return mInlineTasks; }

  //! Get the IODB
  IODBNucleus* getIODB() const { return mIODB; }

  //! Associate an edge-expression with a net
  void putEdgeNet(NUExpr* edgeExpr, NUNet* net);

  //! Find the net associated with an edge-expression
  NUNet* getEdgeNet(NUExpr* edgeExpr);

  //! push the current edgeNetMap onto the stack, and put a new map on the top of the stack
  void pushEdgeNetMap();

  //! pop the top edgeNetMap off the stack
  void popEdgeNetMap();

  //! How many edge-nets are cached
  SInt32 numEdgeNets() const;

  bool checkForPullConflicts() const { return mCheckForPullConflicts; }

  //! Map the top level port names to their port indices
  /*!
    This maps the top level port names of the \e original design to
    their port indices.
    \note This \e must be called after population in order for test
    driver to work.
  */
  void mapTopLevelPortIndices();

  //! Find in the change detects cache
  NUNet* findChangeDetectBool(NUExpr* expr) const;

  //! Add to the change detects cache
  void addChangeDetectBool(NUExpr* expr, NUNet* net);

  //! Clear the change detects (at the end of the module processing)
  void clearChangeDetects();

  //! Return the root module of the design
  NUModule *getRootModule() const;
  //! Set the root module of the design if it has not been previously set
  /*! \param module the module that might be root
      \returns the root module
   */
  NUModule *maybeSetRootModule(NUModule *module);

  //! Intern any HDL name based on language and command line arg
  /*!
    Handles verilog and vhdl names. For vhdl, calls vhdlIntern. For
    vlog, calls the intern function on the AtomicCache.
  */
  StringAtom* hdlIntern(const char*, mvvLanguageType);
  
  //! Intern a VHDL name -- handles lower-casing based on command-line arg
  StringAtom* vhdlIntern(const char*);

  //! Get an interned VHDL name  -- handles lower-casing based on command-line arg
  StringAtom* vhdlGetIntern(const char*);

  //! Get the STBranchNode for the current scope, as detemined by getDeclarationScope()
  /*! This just calls getScopeSymtabNode() with the value of
   *  getDeclarationScope() as its argument.
      \returns The symbol table node for the current declaration scope
   *  \sa getScopeSymtabNode
   */
  STBranchNode *getDeclScopeSymtabNode() const;
  //! Get the STBranchNode for the supplied scope
  /*! \param[in] scope The scope to find the symtab node for
      \returns The symbol table node for the supplied scope
      \sa getDeclScopeSymtabNode
   */
  STBranchNode *getScopeSymtabNode( const NUScope *scope ) const;

  //! Get the scope uniquified name
  /*!
    This is only temporary to integrate the InterraDesignWalker into
    the current population code. It will be removed when the
    uniquification project is complete.
  */
  const char* getScopeUniquifiedName() const;

  //! Pop the scope uniquified name
  /*!
    This is only temporary to integrate the InterraDesignWalker into
    the current population code. It will be removed when the
    uniquification project is complete.
  */
  void popScopeUniquifiedName();

  //! Push the current instance name. No instance for top module
  void pushInstanceName(StringAtom* name);
  
  //! Get the current Instance name.
  StringAtom* getInstanceName();
  
  //! Pop the current instance name
  void popInstanceName();
  
  //! Push an NUPortMap
  void pushPortMap(NUPortMap* portMap);
  //! Get an NUPortMap (can be NULL)
  NUPortMap* getPortMap();
  //! Pop an NUPortMap
  void popPortMap();

  //! Put last visited/populated module
  void putLastVisitedModule(NUModule* module);

  //! Add a net to the set to be marked depositable
  void addDepositNet(NUNet *net);

  //! Add a net to the set to be marked observable
  void addObserveNet(NUNet *net);

  //! Mark observe and deposit nets, and empty the sets.
  void markDepositObserveNets();

private:

  //! Hide copy and assign constructors.
  LFContext(const LFContext&);
  LFContext& operator=(const LFContext&);

  //! Generic method for popping both task and structured proc.
  void popSeqBlock(NUUseDefNode* node);

  //! less than operator object for char*
  class StringAtom_less 
  {
  public:
    //! called by associative table for compare
    bool operator() (const StringAtom* p, const StringAtom* q) const;
  };

  typedef UtSet<veNode> VeNodeSet;

  UtStack<FLNodeMultiMap*> mUseStack;
  UtStack<FLNodeMultiMap*> mEdgeUseStack;
  UtStack<FLNodeMap*> mDefStack;
  UtMap<StringAtom*, NUModule*, StringAtom_less> mModuleMap;
  UtStack<STBranchNode*> mHierStack;
  UtStack<NUStmtList*> mStmtListStack;
  UtStack<NUEdgeExprList*> mEdgeListStack;
  UtStack<NUExprLocList*> mEventListStack;
  UtStack<NUEdgeNetMap*> mEdgeNetMapStack;
  
  //! Stack of declaration scopes for nets declared during population.
  /*!
   * This stack of scopes is used for:
   * 1. Declaration of user-defined variables.
   * 2. Connecting the NUNamedDeclarationScope hierarchy.
   */
  NUScopeStack mDeclarationScopeStack;

  //! Stack of block scopes for blocks created during population.
  /*!
   * This stack of scopes is used for:
   * 1. Declaration of compiler-generated temporary variables.
   * 2. Connecting the NUBlock hierarchy.
   */
  NUScopeStack mBlockScopeStack;

  UtStack<JaguarScope*> mJaguarScopeStack;
  NUModuleSet mVisitedModules;
  VeNodeSet mTFVisitSet;

  STSymbolTable* mSymtab;
  AtomicCache* mStrCache;
  SourceLocatorFactory* mLocFactory;
  FLNodeFactory* mFlowFactory;
  FLNodeElabFactory* mFlowElabFactory;
  NUNetRefFactory *mNetRefFactory;
  MsgContext* mMsgContext;
  NUDesign* mDesign;
  
  IODBNucleus* mIODB;
  //! A pointer to the root module.  Needed for VHDL package support
  NUModule* mRootModule;
  //! A pointer to the last module visited/popped in the stack
  NUModule* mLastVisitedModule;
  
  typedef UtStack<StringAtom*> AtomStack;
  AtomStack mInstanceNameStack;

  typedef UtStack<NUPortMap*> NUPortMapStack;
  NUPortMapStack mPortMapStack;
  
  /*! \brief a stack of ptrs to lists of task enable statements that each need their own always block
   *
   * Used for the conversion of function calls to tasks when the
   * function call was in a context where there cannot be a task
   * enable, such as a continious assignment or module instantiation.
   * \sa extractAnExtraTaskEnable
   */
  UtStack<NUStmtList *> mExtraTaskEnablesStack;
  bool mSaveTaskEnablesAsExtra;

  // Types and data for population of change detect logic in a
  // module. These are cached to reduce the number of change detect
  // operators.
  //
  // I am using UtHashMap because there is no operator< in NUExpr and it
  // doesn't seem worth it to add it since we already have the
  // operator==.
  struct HashExpr
  {
    //! hash operator -- uses NUExpr::hash()
    size_t hash(const NUExpr* expr) const;

    //! lessThan operator -- for sorted iterations -- pointer sort
    bool lessThan(const NUExpr* expr1, const NUExpr* expr2) const;

    //! less-than function -- for Microsoft hash tables -- pointer sort
    bool lessThan1(const NUExpr* v1, const NUExpr* v2) const;

    //! equal operator -- uses NUExpr::operator==
    bool equal(const NUExpr* v1, const NUExpr* v2) const;
  };
  typedef UtHashMap<NUExpr*, NUNet*, HashExpr> ChangeDetectMap;
  ChangeDetectMap* mChangeDetects;

  int  mRequiresSensitivityEvents;
  bool mSynth;
  bool mCoercePorts;
  bool mShareConstants;
  bool mInlineTasks;
  bool mCheckForPullConflicts;
  VHDLCaseT mCaseMode;

  // These sets keep trak of nets to be marked depositable and observable.
  // Used by the PLI code to preserve input and inout parameters to PLI 
  // tasks.  We cannot mark the nets during PLI task population because
  // at that time the names of parameterized modules are mangled, and 
  // the only way to make the markings persist through elaboration is to
  // generate directvies for each net, which requires the un-mangled
  // module names.
  typedef UtSet<NUNet *> NetSet;
  NetSet mDepositNets;
  NetSet mObserveNets;

  //! Statement factory that MUST be used by population stage to create new stmts.
  PplStmtFactory mStmtFactory;

  //! Map of vhdl Aliases
  VhdlPopulate::EntityAliasMap* mAliasMap;
};

#endif
