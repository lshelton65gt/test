// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/MarkReadWriteNets.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUDesign.h"
#include "compiler_driver/CarbonContext.h" // for CRYPT


//! Callback class for walker to discover read and written nets.
class ReadWriteFinder : public NUDesignCallback
{
public:
  //! constructor.
  /*!
    \param module_recurse  If false, will only visit 1 module
   */
  ReadWriteFinder(IODBNucleus * iodb,
                  bool process_simple_connections,
                  bool module_recurse,
                  bool multi_level_hier_tasks) :
    NUDesignCallback(),
    mIODB(iodb),
    mProcessSimplePortConnections(process_simple_connections),
    mModuleRecurse(module_recurse),
    mStopRecursion(false),
    mMultiLevelHierTasks(multi_level_hier_tasks)
  {}

  //! destructor
  ~ReadWriteFinder() {}

  //! By default, visit all nodes
  Status operator()(Phase, NUBase*) { return eNormal;}

  //! Check if we should recurse into submodules
  Status operator()(Phase phase, NUModule* the_module)
  {
    if (phase==ePre) {
      if (mStopRecursion) {
        return eSkip;
      }
      if (not mModuleRecurse) {
        mStopRecursion = true;
      }

      markScopeObserves(the_module);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUTaskEnable *task_enable) {
    if (phase == ePre) {
      // Need to mark out-of-task-scope nets as read/written if this is a
      // hierarchical task enable.  Needed because the out-of-task-scope nets
      // don't appear on a Nucleus walk of the task enable.
      //
      // When supporting multiLevel hierarchically called tasks, the UD information
      // for the taskEnable does not include the non-local nets.  Therefore do not
      // try to mark them if -multiLevelHierTasks_g is enabled.
      if (task_enable->isHierRef() && !mMultiLevelHierTasks) {
        NUTaskEnableHierRef *task_enable_hierref = dynamic_cast<NUTaskEnableHierRef*>(task_enable);
        NUNetSet net_set;
        task_enable_hierref->getNonLocalDefs(&net_set);
        for (NUNetSet::iterator iter = net_set.begin(); iter != net_set.end(); ++iter) {
          NUNet* net = *iter;
          net->putIsWritten(true);
        }
        net_set.clear();
        task_enable_hierref->getNonLocalUses(&net_set);
        for (NUNetSet::iterator iter = net_set.begin(); iter != net_set.end(); ++iter) {
          NUNet* net = *iter;
          net->putIsRead(true);
        }
      }
      return eNormal;
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUPortConnectionInput * connection) {
    if (phase==ePre) {
      bool process_this_connection = true;
      if (not mProcessSimplePortConnections) {
        if (connection->isSimple()) {
          process_this_connection = false;
        }
      } else {
	markInCon (connection->getFormal (), connection->getActual ());
      }
      return (process_this_connection ? eNormal : eSkip);
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUPortConnection * ) {
    if (phase==ePre) {
      return (mProcessSimplePortConnections ? eNormal : eSkip);
    } else {
      return eNormal;
    }
  }

  //! Hit all expr and lvalue constructs, mark their nets
  Status operator()(Phase phase, NULvalue *lvalue)
  {
    if (phase == ePre) {
      markLvalue(lvalue, false);
    }
    return eSkip;
  }

  Status operator()(Phase phase, NUPortConnectionOutput *con)
  {
    if ((phase == ePre) && mProcessSimplePortConnections)
      markOutCon(con->getFormal(), con->getActual(), false);
    return eSkip;
  }

  Status operator()(Phase phase, NUPortConnectionBid *con)
  {
    if ((phase == ePre) && mProcessSimplePortConnections)
      markOutCon(con->getFormal(), con->getActual(), true);
    return eSkip;
  }

  //! Hit all enabled drivers and, mark their nets
  Status operator()(Phase phase, NUEnabledDriver *tri)
  {
    NULvalue* lvalue = tri->getLvalue();
    if (phase == ePre) {
      markLvalue(lvalue, true);
      markExpr(tri->getEnable());
      markExpr(tri->getDriver());
    }
    return eSkip;
  }

  Status operator()(Phase phase, NUExpr *expr)
  {
    if (phase == ePre) {
      markExpr(expr);
    }
    return eSkip;
  }

private:
  //! Hide copy and assign constructors.
  ReadWriteFinder(const ReadWriteFinder&);
  ReadWriteFinder& operator=(const ReadWriteFinder&);

  //! Mark any observable (not by reason of hierref) as read.
  /*!
   * Bug 4468 is an example of why this is needed.
   */
  void markScopeObserves(NUScope * scope) 
  {
    for (NUNetLoop loop = scope->loopLocals(); 
         not loop.atEnd(); 
         ++loop) {
      NUNet * net = (*loop);
      if (net->isProtectedObservableNonHierref(mIODB)) {
        net->putIsRead(true);
      }
    }
    for (NUNamedDeclarationScopeLoop loop = scope->loopDeclarationScopes();
         not loop.atEnd();
         ++loop) {
      markScopeObserves(*loop);
    }
  }

  /*!
   * NOTE: this routine is used to mark the lvalue actuals of bidirects, which is the
   * reason for the markWrite and markRead arguments.  It may be that the bidirect actuals
   * are not written, or that they are read.  The bidirect case should be the only
   * usage of those arguments.
   */
  void markLvalue(NULvalue* lvalue, bool isTristate, bool markWrite=true, bool markRead = false)
  {
    if (lvalue != NULL)
    {
      NUNetSet nets;
      lvalue->getDefs(&nets);
      for (NUNetSet::iterator iter = nets.begin();
           iter != nets.end();
           ++iter) {
        NUNet *net = *iter;
        if (markWrite) {
          if (isTristate)
            net->putIsTriWritten(true);
          else
            net->putIsWritten(true);
        }
        if (markRead) {
          net->putIsRead(true);
        }
      }
      nets.clear();
      lvalue->getUses(&nets);
      for (NUNetSet::iterator iter = nets.begin();
           iter != nets.end();
           ++iter) {
        NUNet *net = *iter;
        net->putIsRead(true);
      }
    } // if
  }

  void markRvalue (NUExpr* expr, bool isTristate)
  {
    if (expr == NULL || not expr->isWholeIdentifier ())
      return;

    NUNet *net = expr->getWholeIdentifier ();
    if (isTristate)
      net->putIsTriWritten (true);
    else
      net->putIsWritten (true);
  }

  void markInCon (NUNet* formal, NUExpr* expr)
  {
    if (formal->isWritten () || formal->isHierRefWritten ())
      markRvalue (expr, false);
    if (formal->isTriWritten ())
      markRvalue (expr, true);
  }

  void markOutCon(NUNet* formal, NULvalue* lvalue, bool isBid)
  {
    bool markRead = isBid && (formal->isRead() || formal->isHierRefRead());
    bool markWrite = formal->isWritten() || formal->isHierRefWritten ();
    if (markWrite or markRead)
      markLvalue(lvalue, false, markWrite, markRead);
    if (formal->isTriWritten())
      markLvalue(lvalue, true);
  }
    
  void markExpr(NUExpr* expr)
  {
    NUNetSet nets;
    expr->getUses(&nets);
    for (NUNetSet::iterator iter = nets.begin();
         iter != nets.end();
         ++iter) {
      NUNet *net = *iter;
      net->putIsRead(true);
    }
  }

  //! The IODB.
  IODBNucleus * mIODB;

  //! Process simple port connections?
  bool mProcessSimplePortConnections;

  //! Recurse into submodules?
  bool mModuleRecurse;

  //! Stop recursion?
  bool mStopRecursion;

  //! Multi-level hier task support enabled?
  bool mMultiLevelHierTasks;
};


//! Callback class for walker to clear read/write flags from nets.
/*
 * The flags are cleared before being set because the design may
 * have been modified such that previously read or written nets are no longer.
 */
class ClearReadWriteFlags : public NUDesignCallback
{
public:
  //! constructor.
  /*!
    \param module_recurse  If false, will only visit 1 module
   */
  ClearReadWriteFlags(bool module_recurse) :
    NUDesignCallback(),
    mModuleRecurse(module_recurse),
    mStopRecursion(false)
  {}

  //! destructor
  ~ClearReadWriteFlags() {}

  //! By default, visit all nodes
  Status operator()(Phase, NUBase*) { return eNormal;}

  //! Check if we should recurse into submodules, and handle locals, ports, etc
  Status operator()(Phase phase, NUModule* module)
  {
    if (phase == ePre) {
      if (mStopRecursion) {
	return eSkip;
      }
      if (not mModuleRecurse) {
	mStopRecursion = true;
      }

      // Visit all ports
      clearNetFlags(module->loopPorts());

      // For now, only modules have hier ref list.  This may change.
      clearNetFlags(module->loopNetHierRefs());

      clearScopeNetFlags(module);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUTF *tf)
  {
    if (phase == ePre) {
      clearNetFlags(tf->loopArgs());
      clearScopeNetFlags(tf);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUBlock *block)
  {
    if (phase == ePre) {
      clearScopeNetFlags(block);
    }
    return eNormal;
  }

private:
  //! Hide copy and assign constructors.
  ClearReadWriteFlags(const ClearReadWriteFlags&);
  ClearReadWriteFlags& operator=(const ClearReadWriteFlags&);

  //! Clear the flags on the local nets of all scopes
  void clearScopeNetFlags(NUScope *scope)
  {
    clearNetFlags(scope->loopLocals());
    for (NUNamedDeclarationScopeLoop loop = scope->loopDeclarationScopes();
         not loop.atEnd();
         ++loop) {
      clearScopeNetFlags(*loop);
    }
  }

  //! Helper functions to clear flags on a bunch of nets
  void clearNetFlags(NUNetLoop loop)
  {
    for (; not loop.atEnd(); ++loop) {
      NUNet *net = *loop;
      net->putIsWritten(false);
      net->putIsTriWritten(false);
      net->putIsRead(false);
    }
  }
  void clearNetFlags(NUNetVectorLoop loop)
  {
    for (; not loop.atEnd(); ++loop) {
      NUNet *net = *loop;
      net->putIsWritten(false);
      net->putIsTriWritten(false);
      net->putIsRead(false);
    }
  }

  //! Recurse into submodules?
  bool mModuleRecurse;

  //! Stop recursion?
  bool mStopRecursion;
};


void MarkReadWriteNets::design(NUDesign *this_design)
{
  NUModuleList mods;
  this_design->getModulesBottomUp(&mods);

  // Clear then set the flags
  {
    ClearReadWriteFlags clearer(true);
    NUDesignWalker clearer_walker(clearer, false);

    for (NUModuleList::iterator p = mods.begin(); p != mods.end(); ++p)
    {
      NUModule* mod = *p;
      NUDesignCallback::Status status = clearer_walker.module(mod);
      NU_ASSERT(status == NUDesignCallback::eNormal, mod);
    }
  }

  // Need two passes for hierarchical read/writes to settle.  First pass
  // finds all locally set nets and all hierarchical references.  Then the
  // second pass propagates those settings thru the port connections.
  //
  for(int i=0; i<2; ++i)
  {
    ReadWriteFinder finder(mIODB, mProcessSimplePortConnections, true, mMultiLevelHierTasks);

    NUDesignWalker finder_walker(finder, false);
    for (NUModuleList::iterator p = mods.begin(); p != mods.end(); ++p)
    {
      NUModule* mod = *p;
      NUDesignCallback::Status status = finder_walker.module(mod);
      NU_ASSERT(status == NUDesignCallback::eNormal, mod);
    }
  }

} // void MarkReadWriteNets::design


void MarkReadWriteNets::module(NUModule *this_module, bool recurse)
{
  // Clear then set the flags
  ClearReadWriteFlags clearer(recurse);
  NUDesignWalker clearer_walker(clearer, false);
  NUDesignCallback::Status status = clearer_walker.module(this_module);
  NU_ASSERT(status == NUDesignCallback::eNormal, this_module);

  ReadWriteFinder finder(mIODB, mProcessSimplePortConnections, recurse, mMultiLevelHierTasks);
  NUDesignWalker finder_walker(finder, false);
  status = finder_walker.module(this_module);
  NU_ASSERT(status == NUDesignCallback::eNormal, this_module);
}

MarkReadWriteNets::MarkReadWriteNets(IODBNucleus * iodb,
                                     ArgProc* args,
                                     bool process_simple_connections) :
  mIODB(iodb),
  mArgs(args),
  mProcessSimplePortConnections(process_simple_connections)
{
  mMultiLevelHierTasks = mArgs->getBoolValue(CRYPT("-multiLevelHierTasks"));
}
