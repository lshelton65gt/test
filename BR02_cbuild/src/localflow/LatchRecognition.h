// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Declaration of latch recognition process
*/

#ifndef _LATCHRECOGNITION_H_
#define _LATCHRECOGNITION_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"

class MsgContext;

//! Used to create a sorted net set for stable output order
class NetComparison
{
public:
  bool operator()(const NUNet* net1, const NUNet* net2) const
  {
    return (NUNet::compare(net1,net2) < 0);
  }
};


//! Class to perform latch recognition in blocks
/*!
 * This class is used to walk a module and check for latches in all
 * combinational blocks. This process depends on the UD class to set
 * up the use/def information. The basic idea is that if there is
 * self-feedback, then the device is a latch.
 *
 * So for all def nets, if the net is in its own use set, then mark it
 * a latch.
 *
 * The use/def analysis takes care to insert self-feedback explicitly
 * when it only existed implicitly in the original Verilog.
 */
class LatchRecognition
{
public:
  //! constuctor
  LatchRecognition(NUNetRefFactory *netref_factory,
		   MsgContext *msg_ctx,
		   bool verbose) :
    mNetRefFactory(netref_factory),
    mMsgContext(msg_ctx),
    mVerbose(verbose)
  {}

  //! destructor
  ~LatchRecognition() {}

  //! Look for latches in this module
  /*!
   * Iterate through all the combinational blocks in this module and
   * determine if any of them have nets that behave like latches
   * (retain a value).
   */
  void module(NUModule *module);

private:
  //! Mark a net as a latch, depending on net type
  void maybeMarkLatch(NUNet *net);

  // Abstraction for finding latched nets
  typedef UtSet<NUNet*,NetComparison> SortedNetSet;

  //! Test a block to see if it is a latch
  /*! The test checks to make sure the block is not sequential. If it
   *  is combinational it looks for explicit feedback on nets. If
   *  there is feedback, then the net is marked as a latch.
   *
   *  \param use_def The block to check.
   */
  void findLatches(NUUseDefNode* use_def, NUNetSet& latched_nets);

  // Make sure latches are used by something
  void checkUses(NUUseDefNode* use_def, NUNetSet& latches,
                 SortedNetSet& used_latches);

  //! Hide copy and assign constructors.
  LatchRecognition(const LatchRecognition&);
  LatchRecognition& operator=(const LatchRecognition&);

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context.
  MsgContext *mMsgContext;

  //! Be verbose as processing occurs?
  bool mVerbose;
};

#endif // _LATCHRECOGNITION_H_
