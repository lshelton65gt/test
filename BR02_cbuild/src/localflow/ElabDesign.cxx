// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/Elaborate.h"
#include "LFContext.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "util/ArgProc.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUDesignWalker.h"
#include "localflow/UpdateUD.h"
#include "flow/FlowGraph.h"
#include "flow/FLElabUseCache.h"

// enable this to dump flow graphs for elaborated flow and unelab flow for each module
#define DUMP_FLOW_GRAPHS 0

#if DUMP_FLOW_GRAPHS
// counter of total Elaborate::design() calls used for debugging output
static int sElabCount = 0;
#endif

/*!
 * Return the NUNetElab for the given port connection's formal
 * at the given hierarchy.
 */
NUNetElab* Elaborate::helperGetFormalElab(NUPortConnection *portconn,
                                          STBranchNode* hier)
{
  NUNet *formal = portconn->getFormal();
  NUModuleInstance *inst = portconn->getModuleInstance();
  STBranchNode *nested_hier = inst->lookupElab(hier)->getHier();
  NUNetElab *formal_elab = formal->lookupElab(nested_hier);
  return formal_elab;
}

void Elaborate::design(NUDesign *design, bool elaboratePorts, bool elaborateFlow)
{
  // Elaborate declarations before the graph, so that we have the
  // elaborated nets when creating the flow nodes.
  designDecl(design);

  if (elaborateFlow) {
    designFlow(design, elaboratePorts);
  }

#if DUMP_FLOW_GRAPHS
  if (getenv("CARBON_DUMP_FLOW_GRAPHS") != NULL)
  {
    UtIO::cout() << "Dumping flow graphs after elaboration #" << ++sElabCount << "\n";
    UtString name;
    ElabFlowGraph* efg = ElabFlowGraph::createElab(design,mSymtab);
    name << "elab_flow_" << sElabCount << ".dot";
    efg->write(name.c_str());
    delete efg;
    NUModuleList allModules;
    design->getAllModules(&allModules);
    for (NUModuleList::iterator m = allModules.begin(); m != allModules.end(); ++m)
    {
      NUModule* module = *m;
      UnelabFlowGraph* ufg = UnelabFlowGraph::createUnelab(module);
      name.clear();
      name << module->getName()->str() << "_" << sElabCount << ".dot";
      ufg->write(name.c_str());
      delete ufg;
    }
  }
#endif
}

void Elaborate::designDecl(NUDesign * design)
{
  LFContext context(mSymtab,
		    mStrCache,
		    mLocFactory,
		    mFlowFactory,
		    mFlowElabFactory,
		    mNetRefFactory,
                    mArgs,
		    mMsgContext,
                    mIODB, NULL);
  context.initForDesignWalk();
  context.setDesign(design);

  NUModuleList modules;
  design->getTopLevelModules(&modules);

  // First, elaborate local nets throughout the design
  for (NUModuleListIter iter = modules.begin(); iter != modules.end(); iter++) {
    elabModuleDecl(*iter, &context, eElabLocals);
  }

  // Second, elaborate hierrarchical references
  for (NUModuleListIter iter = modules.begin(); iter != modules.end(); iter++) {
    elabModuleDecl(*iter, &context, eElabHierRefs);
  }

  // Third, elaborate reconstruction data
  for (NUModuleListIter iter = modules.begin(); iter != modules.end(); iter++) {
    elabModuleDecl(*iter, &context, eElabReconData);
  }
}


void Elaborate::designFlow(NUDesign * design, bool elaboratePorts)
{
  LFContext context(mSymtab,
                    mStrCache,
                    mLocFactory,
                    mFlowFactory,
                    mFlowElabFactory,
                    mNetRefFactory,
                    mArgs,
                    mMsgContext,
                    mIODB, NULL);
  context.initForDesignWalk();

  // Setup for elaboration
  preFlowElaboration();

  NUModuleList modules;
  design->getTopLevelModules(&modules);

  // First, elaborate local graphs throughout the design.
  // During this stage, we keep track of what nets have hierarchical
  // readers and writers, and fix those up during the second pass.
  for (NUModuleListIter iter = modules.begin(); iter != modules.end(); iter++) {
    elabModuleFlow(*iter, &context, elaboratePorts);
  }

  // Cleanup after elaboration
  postFlowElaboration();
}


void Elaborate::fixupDeferredFlows(FLNodeElabMultiMap &deferred_multimap, bool edge)
{
  // Cache for hasBitFanin
  FLElabUseCache levelCache(mNetRefFactory, false);
  FLElabUseCache edgeCache(mNetRefFactory, true);

  for (FLNodeElabMultiMap::iterator fiter = deferred_multimap.begin();
       fiter != deferred_multimap.end();
       ++fiter) {
    FLNodeElab * fanout = fiter->first;
    NUNetElabSet & deferred_nets = fiter->second;

    for (NUNetElabSet::iterator niter = deferred_nets.begin();
         niter != deferred_nets.end();
         ++niter) {
      NUNetElab *deferred_net = (*niter);

      NUNetRefHdl fanout_def_ref = fanout->getFLNode()->getDefNetRef();

      // If this flow node is for an output or bid port connection, then connect
      // the fanin from the nested module.
      NUPortConnection *portconn =
        dynamic_cast<NUPortConnection*>(fanout->getUseDefNode());
      if (portconn and
          (portconn->getFormal() != fanout->getFLNode()->getDefNet())) {
        deferred_net = helperGetFormalElab(portconn, fanout->getHier());
      }

      for (NUNetElab::DriverLoop loop = deferred_net->loopContinuousDrivers();
           not loop.atEnd();
           ++loop) {
        FLNodeElab* driver = *loop;
        if (edge) {
          connectEdgeFaninNoDefer(fanout, driver, &edgeCache);
        } else {
          connectFaninNoDefer(fanout, driver, &levelCache);
        }
      }
    }
  }
  deferred_multimap.clear();
}

void Elaborate::connectFanin(FLNodeElab *fanout, FLNodeElab *fanin, 
                             FLElabUseCache* cache)
{
  connectFaninWorker(fanout, fanin, cache, &mDeferredUseMultiMap, &FLNodeElab::connectFanin);
}

void Elaborate::connectEdgeFanin(FLNodeElab *fanout, FLNodeElab *fanin,
                                 FLElabUseCache* cache)
{
  connectFaninWorker(fanout, fanin, cache, &mDeferredEdgeUseMultiMap, &FLNodeElab::connectEdgeFanin);
}

void Elaborate::connectFaninNoDefer(FLNodeElab *fanout, FLNodeElab *fanin,
                                    FLElabUseCache* cache)
{
  connectFaninWorker(fanout, fanin, cache, NULL, &FLNodeElab::connectFanin);
}

void Elaborate::connectEdgeFaninNoDefer(FLNodeElab *fanout, FLNodeElab *fanin, 
                                        FLElabUseCache* cache)
{
  connectFaninWorker(fanout, fanin, cache, NULL, &FLNodeElab::connectEdgeFanin);
}

void Elaborate::connectFaninWorker(FLNodeElab *fanout,
                                   FLNodeElab *fanin,
                                   FLElabUseCache* cache,
                                   FLNodeElabMultiMap *deferred_multimap,
                                   void (FLNodeElab::* connectFaninFn)(FLNodeElab*))
{
  // We check the unelab def net for hier-refs because it exists at
  // the location where 'fanin' is a driver. This is different from
  // the elaborated def net, which could be somewhere else in the
  // hierarchy and may not be marked as hier-ref.
  NUNet * unelab_def_net = fanin->getFLNode()->getDefNet();

  // If deferred_multimap is non-NULL, it means we should check for hierrefs
  // and defer processing until all of the continuous drivers of the hierref
  // are known.
  if ((deferred_multimap != NULL) &&
      ((fanin->isBoundNode() or fanin->getUseDefNode()->isContDriver()) and
       (unelab_def_net->isHierRef() or unelab_def_net->hasHierRef()))) {
    NUNetElab *def_net = fanin->getDefNet();
    (*deferred_multimap)[fanout].insert(def_net);
  } else {
    // Make sure this fanin defs bits which the fanout flow node uses.
    bool add_fanin = false;

    // The port connections are not handled here -- REAlias will apply
    // the overlap test when the hierarchical aliases are resolved.
    // A bound node will have fanin_ud_node == NULL, we want to
    // skip the port connection tests in that case and apply the normal
    // overlap check in the else clause.
    NUUseDefNode *fanin_ud_node = fanin->getUseDefNode();
    NUUseDefNode *fanout_ud_node = fanout->getUseDefNode();

    bool fanin_is_portconn = fanin_ud_node and fanin_ud_node->isPortConn();
    bool fanout_is_portconn = fanout_ud_node->isPortConn();
    if (fanin_is_portconn or fanout_is_portconn) {
      add_fanin = true;
    } else {
      if (fanout->hasBitFanin(fanin, cache)) {
        add_fanin = true;
      }
    }
    
    if (add_fanin) {
      (fanout->*connectFaninFn)(fanin);
    }
  }
}


void Elaborate::addContinuousDriver(NUNetElab *net_elab, FLNodeElab *driver)
{
  // Do not add elaborated bound nodes when the unelaborated bound node is
  // a placeholder for a hierarchical writer.
  //
  // This is needed because there will be an unelaborated bound
  // node when a hierarchical net is read if the writer is in another module.
  //
  // Note: if you change this, you should also consider changing cleanupBoundHierRefs.
  if (driver->getType() != eFLBoundHierRef) {
    net_elab->addContinuousDriver(driver);
  }
}


void Elaborate::preFlowElaboration()
{
  mDeferredUseMultiMap.clear();
  mDeferredEdgeUseMultiMap.clear();
}


void Elaborate::postFlowElaboration()
{
  fixupDeferredFlows(mDeferredUseMultiMap, false);
  fixupDeferredFlows(mDeferredEdgeUseMultiMap, true);

  // Get rid of now-unnecessary elaborated hierref bound nodes
  cleanupBoundHierRefs();
}

void Elaborate::cleanupBoundHierRefs()
{
  FLNodeElab *flnode = 0;
  for (FLNodeElabFactory::FlowLoop p = mFlowElabFactory->loopFlows(); p(&flnode); ) {
    if (!flnode->isEncapsulatedCycle() && (flnode->getType() == eFLBoundHierRef)) {
      mFlowElabFactory->destroy(flnode);
    }
  }
}
