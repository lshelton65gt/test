//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "InitialBlockCleanup.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUControlFlowNet.h"
#include "localflow/UD.h"
#include "util/CbuildMsgContext.h"


/*!
  \file
  Implementation of initial block cleanup package.
 */


InitialBlockCleanup::InitialBlockCleanup(NUNetRefFactory *netref_factory,
					 IODBNucleus *iodb,
                                         ArgProc* args,
					 MsgContext *msg_ctx) :
  mNetRefFactory(netref_factory),
  mIODB(iodb),
  mArgs(args),
  mMsgContext(msg_ctx)
{
}


InitialBlockCleanup::~InitialBlockCleanup()
{
}


void InitialBlockCleanup::module(NUModule *module)
{
  bool work_done = false;
  NUNet *known_const_net = module->getControlFlowNet();

  for (NUModule::InitialBlockLoop loop = module->loopInitialBlocks();
       not loop.atEnd();
       ++loop) {
    work_done |= initialBlock(*loop, known_const_net);
  }

  // Need to recompute UD if any statements were removed; the fanin
  // is guaranteed to be different.
  if (work_done) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.module(module);
  }
}


bool InitialBlockCleanup::initialBlock(NUInitialBlock *block, NUNet *extra_const_net)
{
  NUNetRefSet known_const_set(mNetRefFactory);
  if ( extra_const_net ){
    known_const_set.insert(mNetRefFactory->createNetRef(extra_const_net));
  }

  mKnownConstSetStack.push(known_const_set);

  bool work_done = blockScope(block->getBlock());

  mKnownConstSetStack.pop();
  return work_done;
}


bool InitialBlockCleanup::blockScope(NUBlockScope *block)
{
  bool work_done = false;

  NUStmtLoop loop = block->loopStmts();
  NUStmtList stmts(loop.begin(), loop.end());
  work_done = stmtList(stmts);

  if (work_done) {
    block->replaceStmtList(stmts);
  }

  return work_done;
}


bool InitialBlockCleanup::stmtList(NUStmtList &stmts)
{
  bool work_done = false;

  NUStmtList::iterator iter = stmts.begin();
  while (iter != stmts.end()) {
    bool remove = false;
    work_done |= stmt(*iter, remove);
    work_done |= remove;

    if (remove) {
      delete *iter;
      iter = stmts.erase(iter);
    } else {
      ++iter;
    }
  }

  return work_done;
}


bool InitialBlockCleanup::stmt(NUStmt *stmt, bool &remove)
{
  bool work_done = false;
  remove = false;

  switch (stmt->getType()) {
  case eNUNonBlockingAssign:
    // This is run after reordering
    NU_ASSERT("Unexpected non-blocking assign."==0,stmt);
    break;

  case eNUIf:
    work_done = ifStmt(dynamic_cast<NUIf*>(stmt), remove);
    break;

  case eNUCase:
    work_done = caseStmt(dynamic_cast<NUCase*>(stmt), remove);
    break;

  case eNUFor:
    work_done = forStmt(dynamic_cast<NUFor*>(stmt), remove);
    break;

  case eNUBlock:
    work_done = blockScope(dynamic_cast<NUBlock*>(stmt));
    break;

  case eNUBlockingAssign:
  default:
    remove = handleStmt(stmt);
    if (remove) {
      work_done = true;
    }
    break;
  }

  // Recompute UD for this stmt if any work was done, so that the kill
  // set will be correct.
  if (work_done) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.stmt(stmt);
  }

  // If this stmt is not being removed, add any kills to the ongoing
  // known-constant set.
  if (not remove) {
    NUNetRefSet &known_const_set = mKnownConstSetStack.top();
    stmt->getBlockingKills(&known_const_set);
  }

  return work_done;
}


bool InitialBlockCleanup::handleStmt(NUStmt *stmt)
{
  bool has_nonconst_uses = stmtIsNonConstant(stmt);
  if (has_nonconst_uses)
  {
    mMsgContext->InitialBlockNonConstantStmt(&stmt->getLoc());
  }
  return has_nonconst_uses;
}


bool InitialBlockCleanup::stmtIsNonConstant(NUStmt *stmt)
{
  NUNetRefSet uses(mNetRefFactory);
  stmt->getBlockingUses(&uses);
  return usesAreNonConstant(uses);
}


bool InitialBlockCleanup::usesAreNonConstant(NUNetRefSet &uses)
{
  NUNetRefSet &known_const_set = mKnownConstSetStack.top();

  NUNetRefSet nonconst_uses(mNetRefFactory);
  NUNetRefSet::set_difference(uses, known_const_set, nonconst_uses);

  return (not nonconst_uses.empty());
}


bool InitialBlockCleanup::ifStmt(NUIf *stmt, bool &remove)
{
  // First, test the selector for constant-ness.
  NUNetRefSet uses(mNetRefFactory);
  stmt->getCond()->getUses(&uses);
  if (usesAreNonConstant(uses)) {
    mMsgContext->InitialBlockNonConstantIf(&stmt->getLoc());
    remove = true;
    return false;
  }

  NUNetRefSet &known_const_set = mKnownConstSetStack.top();
  NUNetRefSet then_known_const_set(known_const_set);
  NUNetRefSet else_known_const_set(known_const_set);

  mKnownConstSetStack.push(then_known_const_set);
  NUStmtLoop loop = stmt->loopThen();
  NUStmtList then_stmts(loop.begin(), loop.end());
  bool work_done_then = stmtList(then_stmts);
  if (work_done_then) {
    stmt->replaceThen(then_stmts);
  }
  mKnownConstSetStack.pop();

  mKnownConstSetStack.push(else_known_const_set);
  loop = stmt->loopElse();
  NUStmtList else_stmts(loop.begin(), loop.end());
  bool work_done_else = stmtList(else_stmts);
  if (work_done_else) {
    stmt->replaceElse(else_stmts);
  }
  mKnownConstSetStack.pop();

  return (work_done_then or work_done_else);
}


bool InitialBlockCleanup::caseStmt(NUCase *stmt, bool &remove)
{
  // First, test the selector and the case conditions for constant-ness
  NUNetRefSet uses(mNetRefFactory);
  stmt->getSelect()->getUses(&uses);
  for (NUCase::ItemLoop item_loop = stmt->loopItems();
       not item_loop.atEnd();
       ++item_loop) {
    for (NUCaseItem::ConditionLoop cond_loop = (*item_loop)->loopConditions();
	 not cond_loop.atEnd();
	 ++cond_loop) {
      (*cond_loop)->getUses(&uses);
    }
  }
  if (usesAreNonConstant(uses)) {
    mMsgContext->InitialBlockNonConstantCase(&stmt->getLoc());
    remove = true;
    return false;
  }

  NUNetRefSet &known_const_set = mKnownConstSetStack.top();
  bool work_done = false;

  for (NUCase::ItemLoop item_loop = stmt->loopItems();
       not item_loop.atEnd();
       ++item_loop) {
    NUNetRefSet local_known_const_set(known_const_set);
    mKnownConstSetStack.push(local_known_const_set);
    NUStmtLoop stmt_loop = (*item_loop)->loopStmts();
    NUStmtList item_stmts(stmt_loop.begin(), stmt_loop.end());
    bool this_work_done = stmtList(item_stmts);
    work_done |= this_work_done;
    if (this_work_done) {
      (*item_loop)->replaceStmts(item_stmts);
    }
    mKnownConstSetStack.pop();
  }

  return work_done;
}


bool InitialBlockCleanup::forStmt(NUFor *for_stmt, bool &remove)
{
  // First, handle the initial statement(s).  If it is non-constant, need
  // to remove the for loop.
  NUStmtLoop initial_loop = for_stmt->loopInitial();
  NUStmtList initial_stmts(initial_loop.begin(), initial_loop.end());
  remove = stmtList(initial_stmts);
  if ( remove ) {
    for_stmt->replaceInitial(initial_stmts);
    return true;
  }

  // Test the condition for constant-ness.
  NUNetRefSet uses(mNetRefFactory);
  for_stmt->getCondition()->getUses(&uses);
  if (usesAreNonConstant(uses)) {
    mMsgContext->InitialBlockNonConstantFor(&for_stmt->getLoc());
    remove = true;
    return false;
  }

  // Handle the advance statement(s).  If any are non-constant, need
  // to remove the for loop.
  NUStmtLoop advance_loop = for_stmt->loopAdvance();
  NUStmtList advance_stmts(advance_loop.begin(), advance_loop.end());
  remove = stmtList(advance_stmts);
  if ( remove ) {
    for_stmt->replaceAdvance(advance_stmts);
    return true;
  }

  // Now that have tested the conditions of the for loop, handle the body.
  NUStmtLoop loop = for_stmt->loopBody();
  NUStmtList body_stmts(loop.begin(), loop.end());
  bool work_done = stmtList(body_stmts);
  if (work_done) {
    for_stmt->replaceBody(body_stmts);
  }

  return work_done;
}
