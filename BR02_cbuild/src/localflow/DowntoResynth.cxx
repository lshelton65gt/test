// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Resynthesize "a downto b" expressions into a shift-mask exprs that have
// correct sizing
//
//
// for an expression
// 
//     a[K-1:0] = {b[x:y],c[w:z]};
// 
// where K is constant, but x,y,w,z are not, is generate a logically equivalent
// statement using explicit shifts of variables:
// 
//     a[K-1:0] = ((b>>y)&GEN_MASK(x,y) << (w-z+1))   |
//                 (c>>z)&GEN_MASK(w,z));
// 
// where GEN_MASK(msb,lsb,size) is the expression:
// 
//     ((1 << (msb-lsb+1)) - 1)
// 
// I think that will produce a representation that will work fine throughout
// cbuild and codegen, even if x,y,w,z never resolve to constants.
// 
// There still remains the question of what to do if there are variable-size
// partselects on the LHS.  That should be solvable in some fashion, but would be
// more complex than what needs to be done for the RHS.
//
// Please see the testcases that were added for this:
//     test/vhdl/lang_misc/dyn_slice*.vhd

#include "localflow/DowntoResynth.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "reduce/Fold.h"

DowntoResynth::DowntoResynth(NUNetRefFactory* nrf, ArgProc* args,
                             MsgContext* msgContext, AtomicCache* atomicCache,
                             IODBNucleus* iodb)
{
  mFold = new Fold(nrf, args, msgContext, atomicCache, iodb, false,
                   eFoldSimplifyArithmetic);
}

DowntoResynth::~DowntoResynth() {
  delete mFold;
}

class DowntoResynthMutator : public NuToNuFn {
public:
public:
  DowntoResynthMutator(Fold* fold) : mFold(fold) {}

  NULvalue* operator()(NULvalue* lval, Phase phase) {
    if (!isPre(phase))
      return NULL;

    NULvalue* ret_lval = NULL;
    NULvalue* ident = NULL;
    NUExpr* msb = NULL;
    NUExpr* lsb = NULL;

    if (isDownto(lval, &ident, &msb, &lsb))
    {
      ret_lval = resynthDownto(ident, msb, lsb);
      if (ret_lval != NULL)
      {
        delete lval;
      }
    }

    return ret_lval;
  }

  NUExpr* operator()(NUExpr *e, Phase phase) {
    NUExpr* ret = NULL;
    if (isPre(phase)) {

      // We can only make sense of "downto" operators in the
      // context of a VHDL concatenation, which I think does not
      // support repeat-counts.
      NUConcatOp* cat = dynamic_cast<NUConcatOp*>(e);
      if ((cat != NULL) && (cat->getRepeatCount() == 1)) {
        // Scan the concatenation for any DownTo operators which
        // have to be rebuilt as shift-masks that are depending on
        // other concat members.
        bool resynth = false;
        for (UInt32 i = 0; !resynth && (i < cat->getNumArgs()); ++i) {
          NUExpr* arg = cat->getArg(i);
          NUExpr* ident, *msb, *lsb;
          resynth = isDownto(arg, &ident, &msb, &lsb);
        }

        if (resynth) {
          ret = resynthCat(cat);
          delete cat;
        }
      }
      else {
        // a raw downto should also be resynthesized

        NUExpr* ident, *msb, *lsb;
        if (isDownto(e, &ident, &msb, &lsb)) {
          ret = resynthDownto(ident, msb, lsb, ident->getBitSize(), NULL);
          delete e;
        }
      }
    } // if
    return ret;
  } // NUExpr* operator

  // Is this an expression a DownTo?  If so, extract the components
  bool isDownto(NUExpr* arg, NUExpr** ident, NUExpr** msb, NUExpr** lsb) {
    NUVarselRvalue* var = dynamic_cast<NUVarselRvalue*>(arg);
    if ((var == NULL) || var->isConstIndex()) {
      return false;
    }
    
    if (!isDownto(var->getIndex(), msb, lsb)) {
      return false;
    }
    *ident = var->getIdentExpr();
    return true;
  }

  // Is this lvalue a DownTo?  If so, extract the components
  bool isDownto(NULvalue* arg, NULvalue** ident, NUExpr** msb, NUExpr** lsb) {
    NUVarselLvalue* var = dynamic_cast<NUVarselLvalue*>(arg);
    if ((var == NULL) || var->isConstIndex()) {
      return false;
    }
    
    if (!isDownto(var->getIndex(), msb, lsb)) {
      return false;
    }
    *ident = var->getLvalue();
    return true;
  }

  bool isDownto(NUExpr* index, NUExpr** msb, NUExpr** lsb)
  {
    NUBinaryOp* bop = dynamic_cast<NUBinaryOp*>(index);
    if ((bop == NULL) || (bop->getOp() != NUOp::eBiDownTo)) {
      return false;
    }
    *msb = bop->getArg(0);
    *lsb = bop->getArg(1);
    return true;
  }

  NUExpr* createFoldDowntoBounds(const NUExpr* msb, const NUExpr* lsb,
                                 const SourceLocator& loc)
  {
    CopyContext cc(NULL, NULL);

    NUExpr* one = NUConst::create(msb->isSignedResult(), 1, 32, loc);
    NUExpr* sz = new NUBinaryOp(NUOp::eBiMinus, msb->copy(cc), lsb->copy(cc), loc);
    sz = new NUBinaryOp(NUOp::eBiPlus, sz, one, loc);
    
    // First, see if we can optimize this downto into a simpler
    // varsel, by seeing if msb-lsb can be reduced to a constant.
    // I was hoping, in test/vhdl/lang_misc/dyn_slice5.vhd, that we
    // would simplify (((idx_int + 31) - idx_int) + 1) to 32.
    // But Fold is not that clever (yet).  Still it doesn't hurt to
    // try this simplification.
    sz = mFold->fold(sz);

    return sz;
  }

  // Resynthesize a downto expression only if msb-lsb can be reduced to a constant.
  NULvalue* resynthDownto(const NULvalue* ident, const NUExpr* msb, const NUExpr* lsb)
  {
    CopyContext cc(NULL, NULL);
    NULvalue* resynthed_lval = NULL;
    // First, create  sz = msb - lsb + 1
    const SourceLocator& loc = ident->getLoc();
    NUExpr* sz = createFoldDowntoBounds(msb, lsb, loc);
    NUConst* k = sz->castConst();

    UInt32 val;
    if ((k != NULL) && k->getUL(&val)) {
      // We can simplify the varsel(downto) to a fixed-width varsel!
      resynthed_lval = new NUVarselLvalue(ident->copy(cc), lsb->copy(cc),
                                          ConstantRange(val - 1, 0), loc);
    }
    delete sz;
    return resynthed_lval;
  }

  // Resynthesize a downto expression into a shift/mask combination.
  // Returns the resultant shift/mask expression, and also returns
  // a Nucleus expression for the sz in *sizep, if sizep!=NULL
  //
  NUExpr* resynthDownto(const NUExpr* ident, const NUExpr* msb, const NUExpr* lsb,
                        UInt32 bitSize, NUExpr** sizep)
  {
    CopyContext cc(NULL, NULL);

    // Mask off the value based on msb & lsb, which are assumed
    // to be variables.  Otherwise Fold would have turned the DownTo
    // into something else.

    // First, create  sz = msb - lsb + 1
    const SourceLocator& loc = ident->getLoc();
    NUExpr* sz = createFoldDowntoBounds(msb, lsb, loc);
    if (sizep != NULL) {
      *sizep = sz->copy(cc);
    }

    NUConst* k = sz->castConst();

    UInt32 val;
    if ((k != NULL) && k->getUL(&val)) {
      // We can simplify the varsel(downto) to a fixed-width varsel!
      delete sz;
      return new NUVarselRvalue(ident->copy(cc),
                                lsb->copy(cc),
                                ConstantRange(val - 1, 0),
                                loc);
    }

    // It is tempting to combine the offsets.  For example, if I have
    //            {id[5:4],a[1:0]}
    // The two-bit a[1:0] causes id[5:4] to be left-shifted by 2.
    // But the LSB of 4 causes us to want to right-shift id by 4-2=2,
    // and then mask with 3.  But let's avoid the temptation because
    // it means that whether I left-shift or right-shift is data-dependent,
    // which I'm not confident is a great thing for us to populate.
    //
    // So first, let's right-shift ident and mask it
    NUExpr* shifted_value = new NUBinaryOp(NUOp::eBiRshift, ident->copy(cc),
                                           lsb->copy(cc), loc);
    NUExpr* one = NUConst::create(false, 1, bitSize, loc);
    NUExpr* two_exp_sz = new NUBinaryOp(NUOp::eBiLshift, one, sz, loc);
    one = NUConst::create(false, 1, bitSize, loc);
    NUExpr* mask = new NUBinaryOp(NUOp::eBiMinus, two_exp_sz, one, loc);
    return new NUBinaryOp(NUOp::eBiBitAnd, shifted_value, mask, loc);
  } // NUExpr* resynthDownto

  // Resynthesize a concatenation that's got DownTo exprs in it, so
  // shift-offsets are not known.
  NUExpr* resynthCat(NUConcatOp* cat) {
    NUExpr* offset = NULL;
    UInt32 catSize = cat->getBitSize();
    const SourceLocator& loc = cat->getLoc();
    NUExpr* result = NULL;
    CopyContext cc(NULL, NULL);

    // Start at LSB, accumulate constant offsets until we hit
    // a DownTo.  Then we will have to convert to variable offsets.
    // But we are going to blast the entire concatenation into
    // shifts & masks.
    for (SInt32 i = cat->getNumArgs() - 1; i >= 0; --i) {
      NUExpr* arg = cat->getArg(i);
      UInt32 argSize = arg->getBitSize();

      NUExpr* sz = NULL;
      NUExpr* ident, *msb, *lsb;
      if (isDownto(arg, &ident, &msb, &lsb)) {
        arg = resynthDownto(ident, msb, lsb, catSize, &sz);
      } // if

      else {
        // If this concat-arg is not a DownTo, but there was a less significant
        // DownTo, then we have to carry on the variable offset calculation as
        // an expression, so keep track of sz.
        sz = NUConst::create(false, argSize, 32, loc);
        arg = arg->copy(cc);
      }

      arg = arg->makeSizeExplicit(catSize);

      // Now, left-shift the arg by the required amount based on previous cat elts
      if (offset != NULL) {
        arg = new NUBinaryOp(NUOp::eBiLshift, arg, offset->copy(cc), loc);
        offset = new NUBinaryOp(NUOp::eBiPlus, offset, sz, loc);
        NU_ASSERT(result != NULL, cat);
        result = new NUBinaryOp(NUOp::eBiBitOr, result, arg, loc);
      }
      else {
        offset = sz;
        result = arg;
      }
    } // for
    delete offset;
    result->resize(cat->getBitSize());
    return result;
  } // NUExpr* resynthCat

private:
  Fold* mFold;
}; // class DowntoResynthMutator : public NuToNuFn

// Find every expression everywhere, and replace its sizing
void DowntoResynth::design(NUDesign* design) {
  DowntoResynthMutator mutator(mFold);
  NUModuleList mods;
  design->getModulesBottomUp(&mods);
  for (NUModuleList::iterator p = mods.begin(), e = mods.end(); p != e; ++p) {
    NUModule* mod = *p;
    mod->replaceLeaves(mutator);
  }
}
