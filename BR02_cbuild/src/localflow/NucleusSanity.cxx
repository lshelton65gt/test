//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "localflow/NucleusSanity.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeExpr.h"
#include "symtab/STBranchNode.h"

class NucleusSanityCallback : public NUDesignCallback {
public:
  NucleusSanityCallback( NucleusSanity::SanityStatus status ) : mSanityStatus(status)
  {
    mModule = NULL;
    mScope = NULL;
  }

  ~NucleusSanityCallback() {
    NU_ASSERT(mModule == NULL, mModule);
  }

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * ) { return eNormal; }

  Status operator() (Phase phase, NUModule* mod) {
    if (phase == ePre) {
      if (mModule != NULL) {
        return eSkip;           // do not recurse through port conns
      }
      NU_ASSERT(mScope == NULL, mod);
      mModule = mod;
      mScope = mod;
      //mAliasDB = mod->getAliasDB();
    }
    else {
      NU_ASSERT(mModule == mod, mod);
      NU_ASSERT(mScope == mod, mod);
      mModule = NULL;
      mScope = NULL;
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUIdentRvalue* id) {
    if (phase == ePre) {
      checkNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUIdentLvalue* id) {
    if (phase == ePre) {
      checkNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUMemselLvalue* id) {
    if (phase == ePre) {
      checkNet(id->getIdent());
      if ( mSanityStatus >= NucleusSanity::eAfterResynth )
        NU_ASSERT( id->isResynthesized(), id );
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUMemselRvalue* id) {
    if (phase == ePre) {
      checkNet(id->getIdent());
      if ( mSanityStatus >= NucleusSanity::eAfterResynth )
        NU_ASSERT( id->isResynthesized(), id );
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUNet* id) {
    if (phase == ePre) {
      if ( mSanityStatus >= NucleusSanity::eAfterResynth )
      {
        if ( id->isMemoryNet())
          NU_ASSERT( id->getMemoryNet()->isResynthesized(), id );
        if ( id->isCompositeNet() || id->isArrayNet())
          NU_ASSERT( !(id->isCompositeNet() || id->isArrayNet()), id );
      }
    }
    return eNormal;
  }

  // Make sure nets are declared in the expected module
  void checkNet(NUNet* net) {
    NUScope* scope = net->getScope();
    NU_ASSERT(scope, net);
    NUModule* mod = scope->getModule();
    NU_ASSERT(mod == mModule, net);
    if ( mSanityStatus >= NucleusSanity::eAfterResynth )
      NU_ASSERT( !net->isCompositeNet() || !net->isArrayNet(), net );

    STAliasedLeafNode* leaf = net->getNameLeaf();
    //STBranchNode* branch = leaf->getParent();
    NUAliasDataBOM* bom = NUAliasBOM::castBOM(leaf->getBOMData());
    SInt32 bomIndex = bom->getIndex();
    //NU_ASSERT(branch->getChild(bomIndex) == leaf, net);
    NU_ASSERT(bomIndex == net->getSymtabIndex(), net);
  }

  Status operator()(Phase /*phase*/, NUModuleInstance* /*inst*/) {
    return eNormal;
  }

  Status operator()(Phase, NUPortConnection*) {
    return eNormal;
  }

  Status operator()(Phase, NUBlock*) {
    return eNormal;
  }

  Status operator()(Phase phase, NUUseDefNode* node) {
    if (phase == ePre) {
      NUModule* mod = node->findParentModule();
      if (mod != NULL) {
        NU_ASSERT(mod == mModule, node);
      }
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUTaskEnable* node) {
    if (phase == ePre) {
      NUModule* mod = node->findParentModule();
      if (mod != NULL) {
        NU_ASSERT(mod == mModule, node);
      }
    }
    return eSkip;
  }

  Status operator()(Phase phase, NUTF* tf) {
    if (phase == ePre) {
      NUModule* mod = tf->findParentModule();
      NU_ASSERT(mod == mModule, tf);
    }
    return eNormal;
  }

  Status operator()(Phase, NUAttribute *node) {
    if ( mSanityStatus >= NucleusSanity::eAfterResynth ) {
      NU_ASSERT( false, node );
      return eStop;
    }
    return eNormal;
  }

  Status operator()(Phase, NUCompositeSelLvalue *node) {
    if ( mSanityStatus >= NucleusSanity::eAfterResynth )
    {
      NU_ASSERT( false, node );
      return eStop;
    }
    return eNormal;
  }

  Status operator()(Phase, NUCompositeLvalue *node) {
    if ( mSanityStatus >= NucleusSanity::eAfterResynth )
    {
      NU_ASSERT( false, node );
      return eStop;
    }
    return eNormal;
  }

  Status operator()(Phase, NUCompositeFieldLvalue *node) {
    if ( mSanityStatus >= NucleusSanity::eAfterResynth )
    {
      NU_ASSERT( false, node );
      return eStop;
    }
    return eNormal;
  }

  Status operator()(Phase, NUCompositeSelExpr *node) {
    if ( mSanityStatus >= NucleusSanity::eAfterResynth )
    {
      NU_ASSERT( false, node );
      return eStop;
    }
    return eNormal;
  }

  Status operator()(Phase, NUCompositeExpr *node) {
    if ( mSanityStatus >= NucleusSanity::eAfterResynth )
    {
      NU_ASSERT( false, node );
      return eStop;
    }
    return eNormal;
  }

  Status operator()(Phase, NUCompositeFieldExpr *node) {
    if ( mSanityStatus >= NucleusSanity::eAfterResynth )
    {
      NU_ASSERT( false, node );
      return eStop;
    }
    return eNormal;
  }

private:
  NUModule* mModule;
  NUScope* mScope;
  NucleusSanity::SanityStatus mSanityStatus;
};

class NucleusSanityExprSize : public NUDesignCallback {
public:
  //! By default, walk through everything.
  Status operator()(Phase, NUBase*) { return eNormal; }

  Status operator()(Phase phase, NUExpr *expr) {
    if ((phase == ePost) && (expr->getNumArgs() != 0)) {
      // Clear the mBitSize cache for the expr size so it recomputes,
      // without actually changing it
      UInt32 old_size = expr->getBitSize();
      expr->putArg(0, expr->getArg(0));
      UInt32 new_size = expr->determineBitSize();
      NU_ASSERT(new_size != old_size, expr);
    }
    return eNormal;
  } // Status operator
};


NucleusSanity::NucleusSanity() {
}

NucleusSanity::~NucleusSanity() {
}

void NucleusSanity::design(NUDesign* design, SanityStatus status) {
  NucleusSanityCallback sanity( status );
  NUDesignWalker walker(sanity, false, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.putWalkDeclaredNets(true);
  for (NUDesign::ModuleLoop p = design->loopAllModules(); !p.atEnd(); ++p) {
    NUModule* mod = *p;
    walker.module(mod);
    if (status < NucleusSanity::eAfterSchedule) {
      mod->sanityCheckAlwaysBlockPrioChain();
    }      
  }

  {
    NucleusSanityExprSize exprSize;
    NUDesignWalker size_walker(exprSize, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.design(design);
  }
}

void NucleusSanity::module(NUModule* mod, SanityStatus status) {
  NucleusSanityCallback sanity( status );
  NUDesignWalker walker(sanity, false, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.putWalkDeclaredNets(true);
  walker.module(mod);

  {
    NucleusSanityExprSize exprSize;
    NUDesignWalker size_walker(exprSize, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.module(mod);
  }

  if (status < NucleusSanity::eAfterSchedule) {
    mod->sanityCheckAlwaysBlockPrioChain();
  }
}
