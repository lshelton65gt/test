// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "BlastNets.h"

#include "util/ArgProc.h"
#include "nucleus/NUDesign.h"
#include "reduce/Separation.h"
#include "reduce/Fold.h"

class NUModule;

BlastNets::BlastNets(NUNetRefFactory* net_ref_factory, ArgProc* args, MsgContext* msg_context,
                     AtomicCache* str_cache, IODBNucleus* iodb, bool verbose_fold,
                     bool verbose_separation)
  : mNetRefFactory(net_ref_factory), mArgs(args), mMsgContext(msg_context),
    mStrCache(str_cache), mIODB(iodb), mVerboseFold(verbose_fold),
    mVerboseSeparation(verbose_separation)
{
  mSeparationMode = Separation::eBlastNets;
  mSeparationMode |= Separation::eCreateContAssigns;
}

BlastNets::~BlastNets()
{
}


void BlastNets::design(NUDesign* design, SeparationStatistics* separation_stats)
{
  for (NUDesign::ModuleLoop modules = design->loopAllModules();
       not modules.atEnd();
       ++modules) {
    NUModule *this_module = *modules;
    module(this_module, separation_stats);
  }
}


void BlastNets::module(NUModule* module, SeparationStatistics* separation_stats)
{
  Fold fold (mNetRefFactory, mArgs, mMsgContext, mStrCache, mIODB,
             mVerboseFold, eFoldUDKiller);
  Separation separate(0, mStrCache, mNetRefFactory, mMsgContext,
                      mIODB, &fold, mSeparationMode, mVerboseSeparation,
                      separation_stats);
  separate.module(module);
}
