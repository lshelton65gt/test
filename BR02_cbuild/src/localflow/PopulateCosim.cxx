// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "LFContext.h"
#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "util/CbuildMsgContext.h"
#include "util/AtomicCache.h"

SourceLocator
VerilogPopulate::locator( mvvNode mvv_node )
{
  return locator( reinterpret_cast<veNode>( mvv_node ));
}


SourceLocator
VhdlPopulate::locator( mvvNode mvv_node )
{
  return locator( reinterpret_cast<vhNode>( mvv_node ));
}


// Create a NUModule for a VHDL ent/arch to be used as a Verilog module instance
Populate::ErrorCode
VhdlPopulate::VhdlModule( veNode ve_inst,
                          vhNode primaryUnit,
                          const UInt32 *offset,
                          LFContext *context )
{
  ErrorCode temp_err_code, err_code = eSuccess;
  temp_err_code = processVhdlModule(ve_inst, primaryUnit, offset, context);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ))
  {
    SourceLocator loc = mPopulate->locator(ve_inst);
    mMsgContext->CheetahConsistency(&loc, "Unable to create instance, Module name not found?");
    
    return eFatal;
  }
  
  return err_code;
}


Populate::ErrorCode
VerilogPopulate::VlogEntity(vhNode vh_inst, mvvNode old_module,
                            veNode new_module, LFContext *context )
{
  ErrorCode temp_err_code, err_code = eSuccess;
  temp_err_code = processVlogEntity(vh_inst, old_module, new_module, context);
  
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ))
  {
    SourceLocator loc = mPopulate->locator( vh_inst );
    mMsgContext->VlogInVhdlFailedToElaborate( &loc );
    return eFatal;
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::processVhdlModule( veNode ve_inst,
                                 vhNode primaryUnit,
                                 const UInt32 *offset,
                                 LFContext *context )
{
  ErrorCode temp_err_code, err_code = eSuccess;

  NUModule* the_module = context->getLastVisitedModule();
  NUScope* parent = mUseLegacyGenerateHierarchyNames ? context->getModule() : context->getDeclarationScope();

  StringAtom* name = context->getInstanceName();
  
  NUModuleInstance *the_instance;
  int num_ports = the_module->getNumPorts();
  NUPortConnectionVector portconns( num_ports );

  SourceLocator instloc = mPopulate->locator( ve_inst );

  the_instance = new NUModuleInstance( name, parent, the_module,
                                       context->getNetRefFactory(), instloc );
  
  // Initialize to 0 because not all ports may have connections.
  std::fill( portconns.begin(), portconns.end(), (NUPortConnection*)0 );
  
  // Get the Verilog port list
  CheetahList ve_portconn_iter( veInstanceGetTerminalConnectionList( ve_inst ));
  if ( ve_portconn_iter )
  {
    // Get the VHDL port list
    JaguarList entPortList;
    vhNode entPortClause = vhGetPortClause( primaryUnit );
    if ( entPortClause )
      entPortList = vhGetFlatSignalList( entPortClause );

    NU_ASSERT( entPortList != NULL, the_instance ); // can't find ports of VHDL being instantiated

    // Determine named or positional association
    veNode ve_portconn = veListGetNextNode( ve_portconn_iter );
    bool positional = ( vePortConnGetPortName( ve_portconn ) != NULL ) ?false:true;
    SourceLocator loc = mPopulate->locator( ve_portconn );

    if ( positional == true )
    {
      // Ports are connected using positional association
      do
      {
        // The Verilog port iterator has already advanced; advance VHDL's
        vhNode vh_port = vhGetNextItem( entPortList );

        vhNode localSubTypeInd = vhGetSubTypeInd(vh_port);
        vhNode localTypeDecl   = vhGetType(localSubTypeInd);
        vhNode pTypeDef        = vhGetTypeDef(localTypeDecl);

        while (vhGetObjType(pTypeDef) == VHCONSARRAY || 
                   vhGetObjType(pTypeDef) == VHUNCONSARRAY) {
          vhNode elmType = vhGetEleSubTypeInd(pTypeDef);
          vhNode typeDecl = vhGetType(elmType);
          pTypeDef = vhGetTypeDef(typeDecl);
        }
        // In mixed mode record type port in vhdl formal side is not supported. 
        if (vhGetObjType(pTypeDef) == VHRECORD) {
          POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Verilog port connection mapped to VHDL record port is unsupported"), &err_code);
          return err_code;
        } 
        NUNet* port = 0;
        bool partial_port = false;
        ConstantRange port_range;
        temp_err_code = mPopulate->lookupPort( the_module, vh_port, NULL, NULL, &port, &partial_port, &port_range);
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        {
          return temp_err_code;
        }
        if ( partial_port ) {
          mMsgContext->VhdlMultiPortNet(&loc,port);
          return eFatal;
        }

        if ( port )
        {
          vhNode localSubTypeInd = vhGetSubTypeInd(vh_port);
          vhNode localTypeDecl   = vhGetType(localSubTypeInd);
          StringAtom *vhPortAtom = context->vhdlIntern( vhGetName(localTypeDecl) );
          // multi dimensional array on VHDL formal side is not supported
          if (strcasecmp(vhPortAtom->str(), "string") != 0 && port->is2DAnything()) {
            POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc,"Verilog port connection mapped to VHDL port having dimension greater than 1 is unsupported"), &err_code);
             return err_code;
          }
          NUPortConnection* portconn = 0;
          VeNodeList portconns_for_this_port;
          portconns_for_this_port.push_back(ve_portconn);
          temp_err_code = mPopulate->mVlogPopulate.portConnection( portconns_for_this_port,
                                                                   context,
                                                                   port,
                                                                   the_module,
                                                                   offset,
                                                                   loc,
                                                                   &portconn );

          if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
            return temp_err_code;

          if ( portconn )
            portconn->setModuleInstance( the_instance );

          int i = the_module->getPortIndex( port );
          if ( not (( i >= 0 ) && ( i < num_ports ))) {
            mMsgContext->CheetahConsistency( &loc, "Out of bounds port index" );
            return eFatal;
          }
          portconns[i] = portconn;
        }
      }
      while (( ve_portconn = veListGetNextNode( ve_portconn_iter )));
    }
    else
    {
      // Named association handled here
      UtHashMap< StringAtom*, vhNode > vhdlNameToPortMap;
      
      // Walk the VHDL ports and hash port name to vhNode
      while ( vhNode vh_port = vhGetNextItem( entPortList ))
      {
        JaguarString portName(vhGetName( vh_port ));
        UtString formalPortName(portName);
        // At the mixed language boundary, the names of ports are
        // found in case insensitive match, that's why we lower case it.
        formalPortName.lowercase();
        StringAtom *vhPortAtom = context->vhdlIntern(formalPortName.c_str());

        vhdlNameToPortMap[ vhPortAtom ] = vh_port;
      }

      do
      {
        UtString actualPortName = vePortConnGetPortName( ve_portconn );
        // search for it in case insensitive way.
        actualPortName.lowercase();
        StringAtom *vePortAtom = context->vhdlGetIntern( actualPortName.c_str() );
        vhNode vh_port = vhdlNameToPortMap[ vePortAtom ];
        if ( vh_port == NULL ){
          SourceLocator loc = mPopulate->locator( ve_portconn );
          UtString message( "Unable to find formal port named: " );
          message += actualPortName;
          mMsgContext->CheetahConsistency( &loc, message.c_str() );
          return eFatal;
        }
        // Look up the VHDL port we are instantiating
        NUNet* port = 0;
        bool partial_port = false;
        ConstantRange port_range;
        temp_err_code = mPopulate->lookupPort( the_module, vh_port, NULL, NULL, &port, &partial_port, &port_range);
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        {
          return temp_err_code;
        }
        if ( partial_port ) {
          mMsgContext->VhdlMultiPortNet(&loc,port);
          return eFatal;
        }

        if ( port )
        {
          NUPortConnection* portconn = 0;
          VeNodeList portconns_for_this_port;
          portconns_for_this_port.push_back(ve_portconn);
          SourceLocator loc = mPopulate->locator( ve_portconn );
          temp_err_code = mPopulate->mVlogPopulate.portConnection( portconns_for_this_port,
                                                                   context,
                                                                   port,
                                                                   the_module,
                                                                   offset,
                                                                   loc,
                                                                   &portconn );

          if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
            return temp_err_code;

          if ( portconn )
            portconn->setModuleInstance( the_instance );

          int i = the_module->getPortIndex( port );
          if ( not (( i >= 0 ) && ( i < num_ports ))) {
            SourceLocator loc = mPopulate->locator( ve_portconn );
            mMsgContext->CheetahConsistency( &loc, "Out of bounds port index" );
            return eFatal;
          }
          portconns[i] = portconn;
        }
      }
      while (( ve_portconn = veListGetNextNode( ve_portconn_iter )));
    }

    // Create port connections for unconnected ports.
    for ( int i = 0; i < num_ports; ++i )
    {
      if ( portconns[i] == 0 )
      {
        NUNet *port = the_module->getPortByIndex( i );
        NUPortConnection* portconn = 0;

        // warn the user that this was left disconnected, no need to 
        // check if this was implicit or explicit because this
        // method is only used when processing a verilog instantion of
        // a vhdl entity.  The only way to have an explicit open
        // connection in a verilog instantation is by the null
        // argument (e.g. mod1 u1(a, , c); // note middle argument is null
        // and in this case the do loop above will still fill in the
        // portconns entry for this index (with the actual side of the
        // portconn as null).  Note that mvv will also print a warning
        // of the disconnected port in this case.
        mMsgContext->UnconnectedPort(&loc, port->getName()->str());
        
        ErrorCode port_err_code = unconnectedPort( ve_inst, port, context, &portconn );
        if ( errorCodeSaysReturnNowOnlyWithFatal( port_err_code, &err_code ))
        {
          return port_err_code;
        }
        portconn->setModuleInstance( the_instance );
        portconns[i] = portconn;
      }
    }

    the_instance->setPortConnections( portconns );

    // Add the instance to the calling module or enclosing NamedDeclarationScope
    addModuleInstance(the_instance, parent);
 
  }
  return err_code;
}

void sGetPortList(mvvNode entity, 
		  mvvLanguageType lang, 
		  bool is_udp,
                  UtArray<mvvNode>* formal_array, 
		  UtArray<mvvNode>* actual_array,
                  UtArray<mvvNode>* initial_array,
		  UtMap<UtString,int>* port_name_to_index_map,
		  TicProtectedNameManager* ticProtectedNameManager)
{
  if (lang == MVV_VHDL)
  {
    int index = 0;
    vhNode vh_entity = entity->castVhNode();
    vhNode port_clause = vhGetPortClause(vh_entity);
    JaguarList ports(vhGetFlatSignalList(port_clause));
    while (mvvNode vh_formal = mvvGetNextItem(ports)) {
      vhNode formal = vh_formal->castVhNode();
      mvvNode vh_actual = vhGetInitialValue(formal);
      UtString port_name(vhGetName(formal));
      formal_array->push_back(vh_formal);
      actual_array->push_back(vh_actual);
      initial_array->push_back(NULL);
      (*port_name_to_index_map)[UtString(port_name)] = index;
      index++;
    }
  }
  else if (lang == MVV_VERILOG)
  {
    int index = 0;
    veNode ve_module = entity->castVeNode();
    CheetahList ports(is_udp ? veUDPGetPortList(ve_module) :
                      veModuleGetPortList(ve_module));
    while (mvvNode ve_formal = mvvGetNextItem(ports)) {
      veNode formal = ve_formal->castVeNode();
      UtString port_name;
      ticProtectedNameManager->getVisibleName(formal, &port_name);
      mvvNode vh_actual = mvvElabModulePortGetVhActual(ve_formal);
      mvvNode vh_initial = mvvElabModulePortGetVhInitial(ve_formal);
      formal_array->push_back(ve_formal);
      actual_array->push_back(vh_actual);
      initial_array->push_back(vh_initial);
      (*port_name_to_index_map)[UtString(port_name)] = index;
      index++;
    }
  }
  else
    INFO_ASSERT(0, "Unrecognized language type");
}

Populate::ErrorCode
VerilogPopulate::processVlogEntity(vhNode vh_inst, mvvNode oldEntity,
                                   veNode new_module, LFContext *context)
{
  ErrorCode temp_err_code, err_code = eSuccess;
  SourceLocator loc = mPopulate->locator( vh_inst );

  NUModule* the_module = context->getLastVisitedModule();
  NUScope* parent = context->getDeclarationScope();
  StringAtom* name = context->getInstanceName();

  mvvLanguageType oldEntityLang = mvvGetLanguageType(oldEntity);
  bool is_udp = false;
  if (oldEntityLang == MVV_VERILOG)
  {
    veNode ve_module = oldEntity->castVeNode();
    ErrorCode module_err_code = findIfModuleOrUDP(ve_module, &is_udp);
    if ( errorCodeSaysReturnNowWhenFailPopulate(module_err_code, &err_code ) )
    {
      return module_err_code;
    }
  }
  
  NUModuleInstance *the_instance = new NUModuleInstance(name, parent, the_module,
                                                        context->getNetRefFactory(),
                                                        loc);

  // the Jaguar documentation is wrong about calling vhDeleteList on the array 
  // returned by vhGetPortMap, it should NOT be called (confirmed by vipul 10/27/06)
  vhArray vh_ent_portmap(vhGetPortMap( vh_inst ));

  int num_ports = the_module->getNumPorts();
  NUPortConnectionVector portconns( num_ports );
  // Initialize to 0 because not all ports may have connections.
  std::fill(portconns.begin(), portconns.end(), (NUPortConnection*)0);

  UtArray<mvvNode> formal_array;
  UtArray<mvvNode> actual_array;
  UtArray<mvvNode> initial_array;
  UtArray<vhNode> inst_actual_array;
  UtMap<UtString, int> port_name_to_index_map;

  sGetPortList(oldEntity, 
	       oldEntityLang, 
	       is_udp, 
	       &formal_array,
               &actual_array, 
	       &initial_array,
	       &port_name_to_index_map,
	       mTicProtectedNameMgr);
  SInt32 port_index = 0;

  UtArray<mvvNode>::iterator formal_itr = formal_array.begin();
  UtArray<mvvNode>::iterator actual_itr = actual_array.begin();
  UtArray<mvvNode>::iterator initial_itr = initial_array.begin();

  for ( ; formal_itr != formal_array.end();
        ++formal_itr, ++actual_itr, ++initial_itr, ++port_index)
  {
    mvvNode formal = *formal_itr;
    mvvNode vh_actual = *actual_itr;
    mvvNode vh_initial = *initial_itr;
    // mvv/jaguar converts named associations into a positional
    // association array so we only need to worry about a positional
    // association here.
    vhNode vh_unelab_actual = vhGetNodeAt(port_index, vh_ent_portmap);

    NUNet *port = NULL;
    bool explicit_open = false;
    if ( vh_actual == NULL ){
      if ( vh_initial ) {
        explicit_open = vhGetObjType(static_cast<vhNode>(vh_initial)) == VHOPEN;
      } else if ( vh_unelab_actual ){
        explicit_open = vhGetObjType(static_cast<vhNode>(vh_unelab_actual)) == VHOPEN;
      }
    }
    if (vh_actual && vhGetObjType(static_cast<vhNode>(vh_actual)) != VHOPEN) {
      vhNode expr_type = vhGetExpressionType(static_cast<vhExpr>(vh_actual));
      vhNode actual_type    = vhGetActualObj(expr_type);
      StringAtom *type_name = context->vhdlIntern( vhGetName( actual_type ));
      if (actual_type && strcmp( type_name->str(), "string") == 0) {
        POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc,"Carbon does not support VHDL objects of type string used as actual arguments for a Verilog module."), &err_code);
        return err_code;
      }
    }
    bool partial_port = false;
    ConstantRange port_range;
    temp_err_code = mPopulate->lookupPort(the_module, formal, new_module,
                                          context->getPortMap(), &port,
                                          &partial_port, &port_range );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    if ( partial_port and port ){
      POPULATE_FAILURE(mMsgContext->MultiPortNet( port, "Combine all bits of this vector into a single port to allow the module to be used as an entity in a VHDL design."), &err_code);
      return err_code;
    }
    if ( port ) {
      NUPortConnection* portconn = NULL;
      if ( vh_actual != NULL && VHOPEN != vhGetObjType (static_cast<vhNode>(vh_actual)) )
      {
        temp_err_code = mPopulate->mVhdlPopulate.portConnection( static_cast<vhExpr>( vh_actual ),
                                                                 context, port,
                                                                 &portconn );  
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
          return temp_err_code;
        }
      }
      else
      {
        if ( ( vh_actual == NULL ) && ! explicit_open ) {
          mMsgContext->UnconnectedPort(&loc, port->getName()->str());
        }

        temp_err_code = mPopulate->mVhdlPopulate.unconnectedPort( vh_inst, port,
                                                                  context, &portconn );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
          return temp_err_code;
        }
      }
      
      portconn->setModuleInstance( the_instance );
      
      int i = the_module->getPortIndex( port );
      if ( i < 0 || i >= num_ports )
      {
        SourceLocator loc = mPopulate->locator( vh_actual );
        mMsgContext->CheetahConsistency( &loc, "Out of bounds port index" );
        return eFatal;
      }
      portconns[i] = portconn;
    }
  }

  the_instance->setPortConnections( portconns );

  // Add the Verilog instance to the VHDL parent entity/architecture
  addModuleInstance(the_instance, parent);

  return err_code;
}

Populate::ErrorCode
DesignPopulate::moduleInstance(mvvNode inst,
			       mvvNode oldEntity,
			       mvvNode newEntity,
			       const UInt32* rangeOffset,
			       LFContext* context)
{
  Populate::ErrorCode err_code = Populate::eSuccess;

  SourceLocator    loc = locator(inst);
  NUScope* the_parent = mUseLegacyGenerateHierarchyNames ? context->getModule() : context->getDeclarationScope();
  NUModule* the_module = context->getLastVisitedModule();
  StringAtom*     name = context->getInstanceName();

  // Create the NUModuleInstance
  NUModuleInstance* the_instance = new NUModuleInstance(name,
                                                        the_parent,
                                                        the_module,
                                                        context->getNetRefFactory(),
                                                        loc);

  NUPortMap* portMap = context->getPortMap();
  NUPortMap reversePortMap;

  // Create the reverse map only if the port map exists!
  if (portMap)
  {
    for (NUPortMap::iterator pIter = portMap->begin(); pIter != portMap->end(); ++pIter)
    {
	reversePortMap[pIter->second] = pIter->first;
    }
  }

  int num_ports = the_module->getNumPorts();

  NUPortConnectionVector portconns(num_ports);

  // Initialize the port connection vector
  std::fill(portconns.begin(), portconns.end(), (NUPortConnection*)0);

  mvvLanguageType oldEntityLang = mvvGetLanguageType(oldEntity);

  // If the oldEntity is Verilog, determine if it is a module or an udp
  bool is_udp = false;
  if (oldEntityLang == MVV_VERILOG)
  {
    veNode ve_module = oldEntity->castVeNode();
    Populate::ErrorCode module_err_code = mVlogPopulate.findIfModuleOrUDP(ve_module, &is_udp);
    if (mVlogPopulate.errorCodeSaysReturnNowWhenFailPopulate(module_err_code, &err_code))
    {
      return module_err_code;
    }
  }

  UtArray<mvvNode> formal_array;
  UtArray<mvvNode> actual_array;
  UtArray<mvvNode> initial_array;
  UtArray<vhNode>  inst_actual_array;
  UtMap<UtString, int> port_name_to_index_map;
  UtMap<UtString, int>::iterator p2indexIter;

  // Get the port information from the oldEntity
  sGetPortList(oldEntity, 
	       oldEntityLang, 
	       is_udp, 
	       &formal_array,
	       &actual_array,
	       &initial_array,
	       &port_name_to_index_map,
	       mVlogPopulate.mTicProtectedNameMgr);

  mvvLanguageType instLang = mvvGetLanguageType(inst);

  UtArray<mvvNode> formal_to_actual_array;
  UtArray<veNode> portconn_array;
  int port_index = 0;

  // Get the port connections
  if (instLang == MVV_VERILOG) 
  {
    veNode ve_inst = inst->castVeNode();

    // Get the Verilog port list
    CheetahList ve_portconn_iter(veInstanceGetTerminalConnectionList(ve_inst));

    if (ve_portconn_iter)
    {
      veNode ve_portconn;
      
      while ((ve_portconn = veListGetNextNode(ve_portconn_iter)))
      {
	SourceLocator loc = locator(ve_portconn);
	
	bool namedAssociation = (vePortConnGetPortName(ve_portconn) != NULL) ? true : false;
	
	if (namedAssociation)
	{
	  // Look up the port index
	  UtString port_name(CheetahStr(vePortConnGetPortName(ve_portconn)));
	  p2indexIter = port_name_to_index_map.find(port_name);

	  // Check if the port map can supply the mappings between the new and old entities.
	  if (p2indexIter == port_name_to_index_map.end())
	  {
	    if (portMap)
	    {
	      NUPortMap::iterator portMapIter = portMap->find(port_name.c_str());
	      if (portMapIter != portMap->end()) 
	      {
		p2indexIter = port_name_to_index_map.find(portMapIter->second);
	      }
	      else
	      {
		// Try the reverse map as a final attemp
		portMapIter = reversePortMap.find(port_name.c_str());
		if (portMapIter != reversePortMap.end())
		{
		  p2indexIter = port_name_to_index_map.find(portMapIter->second);
		}
	      }
	    }

	    // Can't find anything. Issue the error.
	    if (p2indexIter == port_name_to_index_map.end())
	    {
	      POPULATE_FAILURE(mMsgContext->CouldNotFindSubstituteMappedPort(&loc,port_name.c_str()), &err_code);
	      return err_code;
	    }
	  }
	  port_index = (*p2indexIter).second;
	}

	// Store the mapping between actual (the current port conn) and the formal
	formal_to_actual_array.push_back(formal_array[port_index]);

	portconn_array.push_back(ve_portconn);

	if (!namedAssociation)
	{
	  port_index++;
	}
      }
    }
  }
  else // instLang == MVV_VHDL
  {
    vhArray vh_portmap(vhGetPortMap(inst->castVhNode()));

    while (port_index < num_ports)
    {
      formal_to_actual_array.push_back(formal_array[port_index]);
      inst_actual_array.push_back(vhGetNodeAt(port_index, vh_portmap));
      port_index++;
    }
  }

  UtArray<mvvNode>::iterator formal_iter = formal_to_actual_array.begin();
  UtArray<mvvNode>::iterator actual_iter = actual_array.begin();
  UtArray<mvvNode>::iterator initial_iter = initial_array.begin();
  UtArray<vhNode>::iterator inst_actual_iter = inst_actual_array.begin();
  UtArray<veNode>::iterator portconn_iter = portconn_array.begin();

  // Create the NUPortConnections for each of the ports that are actually connected
  for ( ;
	formal_iter != formal_to_actual_array.end();
	++formal_iter, ++actual_iter, ++initial_iter, ++inst_actual_iter, ++portconn_iter)
  {
    mvvNode mvvPort = *formal_iter;

    NUNet* port = NULL;
    bool partial_port = false;
    ConstantRange port_range;
  
    // Lookup the port, taking into account that a new entity or module has substituted the old entity or module
    Populate::ErrorCode temp_err_code = lookupPort(the_module,
						   mvvPort,
						   newEntity,
						   portMap,
						   &port,
						   &partial_port,
						   &port_range);

    // Check that everything worked
    if (instLang == MVV_VERILOG)
    {
      if (mVlogPopulate.errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
      {
	return temp_err_code;
      }
    }
    else
    {
      if (mVhdlPopulate.errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
      {
	return temp_err_code;
      }
    }

    // Partial ports are not allowed.
    if (partial_port)
    {
      mMsgContext->VhdlMultiPortNet(&loc, port);
      return Populate::eFatal;
    }

    if (port)
    {
      NUPortConnection* portconn = NULL;
      if (instLang == MVV_VERILOG) 
      {
	veNode ve_portconn = (*portconn_iter)->castVeNode();
	UtList<veNode> portconns_for_this_port;
	portconns_for_this_port.push_back(ve_portconn);
      
	// Create the NUPortConnection.
	Populate::ErrorCode temp_err_code = mVlogPopulate.portConnection(portconns_for_this_port,
									 context,
									 port,
									 the_module,
									 rangeOffset,
									 loc,
									 &portconn );
	
	if (mVlogPopulate.errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
	{
	  return temp_err_code;
	}
      }
      else // instLang == MVV_VHDL
      {
	vhNode vh_actual = (*actual_iter)->castVhNode();
	if (vh_actual && vhGetObjType(vh_actual) != VHOPEN)
	{
	  // Create the NUPortConnection
	  Populate::ErrorCode temp_err_code = mVhdlPopulate.portConnection(static_cast<vhExpr>(vh_actual),
									   context,
									   port,
									   &portconn);

	  if (mVhdlPopulate.errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) 
	  {
	    return temp_err_code;
	  }
	}
	else 
	{
	  bool explicit_open = false;
	  vhNode vh_initial = (*initial_iter)->castVhNode();
	  if (vh_initial)
	  {
	    explicit_open = vhGetObjType(vh_initial) == VHOPEN;
	  }
	  else if (*inst_actual_iter) 
	  {
	    explicit_open = vhGetObjType(*inst_actual_iter) == VHOPEN;
	  }

	  // If the actual is NULL, meaning it is open, but it wasn't left open
	  // on purpose, then issue a message to that effect.
	  if (vh_actual == NULL && !explicit_open)
	  {
	    mMsgContext->UnconnectedPort(&loc, port->getName()->str());
	  }
	}
      }

      // Associate the port connection to the instance
      if (portconn)
      {
	portconn->setModuleInstance(the_instance);
      }
      
      // Just making sure
      int i = the_module->getPortIndex(port);
      if ( not (( i >= 0 ) && ( i < num_ports ))) 
      {
	mMsgContext->CheetahConsistency( &loc, "Out of bounds port index" );
	return Populate::eFatal;
      }
      
      portconns[i] = portconn;
    }    
  }

  // Create port connections for unconnected ports.
  for ( int i = 0; i < num_ports; ++i )
  {
    if ( portconns[i] == 0 )
    {
      NUNet *port = the_module->getPortByIndex( i );
      NUPortConnection* portconn = 0;
      
      // warn the user that this was left disconnected, no need to 
      // check if this was implicit or explicit because this
      // method is only used when processing a verilog instantion of
      // a vhdl entity.  The only way to have an explicit open
      // connection in a verilog instantation is by the null
      // argument (e.g. mod1 u1(a, , c); // note middle argument is null
      // and in this case the do loop above will still fill in the
      // portconns entry for this index (with the actual side of the
      // portconn as null).  Note that mvv will also print a warning
      // of the disconnected port in this case.
      mMsgContext->UnconnectedPort(&loc, port->getName()->str());
      
      Populate::ErrorCode port_err_code;
      if (instLang == MVV_VERILOG)
      {
        port_err_code = mVlogPopulate.unconnectedPort( inst->castVeNode(), port, context, &portconn );
	if (mVlogPopulate.errorCodeSaysReturnNowOnlyWithFatal( port_err_code, &err_code))
	{
	  return port_err_code;
	}
      }
      else // instLang == MVV_VHDL
      {
        port_err_code = mVhdlPopulate.unconnectedPort( inst->castVhNode(), port, context, &portconn );
	if (mVhdlPopulate.errorCodeSaysReturnNowOnlyWithFatal( port_err_code, &err_code))
	{
	  return port_err_code;
	}
      }

      portconn->setModuleInstance( the_instance );
      portconns[i] = portconn;
    }
  }

  // Store all the port connections in the instance
  the_instance->setPortConnections(portconns);

  // Associate the instance with the parent module/namedDeclarationScope
  if (instLang == MVV_VERILOG)
  {
    mVlogPopulate.addModuleInstance(the_instance, the_parent);
  }
  else // instLang == MVV_VHDL
  {
    mVhdlPopulate.addModuleInstance(the_instance, the_parent);
  }

  return err_code;
}

