// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/InterraDesignWalker.h"
#include "localflow/TicProtectedNameManager.h"
#include "localflow/VerilogPopulate.h"
#include "localflow/VhdlPopulate.h"
#include "util/ConstantRange.h"
#include "util/CarbonAssert.h"
#include "util/SourceLocator.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/MsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "iodb/IODBNucleus.h"

//! Use this assert in lieu of INFO_ASSERT. This assert prints assert
//! message with source location. (this can be used in InterraDesignWalkerCB or InterraDesignWalker functions
#define CB_ISSUE_ASSERT(condition, node, langType, msg) \
  if (!condition) { \
    UtString assert_msg; \
    addLocationAndType(node, langType, &assert_msg); \
    assert_msg << " " << msg; \
    INFO_ASSERT(condition, assert_msg.c_str()); \
  } /* caller adds semicolon */


//! Function-level status check. Returns unless eNormal
/*!
  This should only be used when starting or ending a function or
  whenever non-normal status, including skip, should pop the execution
  stack.
*/
#define CB_STATUS_CHK(node, langType, status) \
  do { \
    switch ((status)) { \
    case InterraDesignWalkerCB::eNormal: break; \
    case InterraDesignWalkerCB::eStop: return InterraDesignWalkerCB::eStop; break; \
    case InterraDesignWalkerCB::eSkip: return InterraDesignWalkerCB::eNormal; break; \
    case InterraDesignWalkerCB::eInvalid: CB_ISSUE_ASSERT(0, node, langType, "Invalid callback status"); break; \
    } \
  } while (0) /* caller adds semicolon */

//! Status check after function call. Does not allow eSkip.
/*!
  This should only be used after calling a design walker function
  (traversing a node). No design walker function should ever return
  eSkip.
*/
#define CB_STATUS_NESTED_CHK(node, langType, status) \
  do { \
    switch ((status)) { \
    case InterraDesignWalkerCB::eNormal: break; \
    case InterraDesignWalkerCB::eStop: return InterraDesignWalkerCB::eStop; break; \
    case InterraDesignWalkerCB::eSkip: CB_ISSUE_ASSERT(0, node, langType, "Cannot skip nested status check"); break; \
    case InterraDesignWalkerCB::eInvalid: CB_ISSUE_ASSERT(0, node, langType, "Invalid callback status"); break; \
    } \
  } while (0) /* caller adds semicolon */

//! Status check with skip detection 
/*!
  This should be used after a callback is called within a function
  that is not at the beginning and the end of a design walker function
  (CB_STATUS_CHK is used for that). This allows for the design walker
  to skip things within a function. So, for example, if we were
  in a for generate loop we might want to skip the next
  generate item. So, our callback return eSkip. This catches it and
  skips the next generate item.
*/
#define CB_STATUS_SKIP_CHK(node, langType, status, isSkip) \
  isSkip = false; \
  do { \
    switch ((status)) { \
    case InterraDesignWalkerCB::eNormal: break; \
    case InterraDesignWalkerCB::eStop: return InterraDesignWalkerCB::eStop; break; \
    case InterraDesignWalkerCB::eSkip: isSkip = true; status = InterraDesignWalkerCB::eNormal; break; \
    case InterraDesignWalkerCB::eInvalid: CB_ISSUE_ASSERT(0, node, langType, "Invalid callback status"); break; \
    } \
  } while (0) /* caller adds semicolon */

 //! Change eSkip status to eNormal.
#define CB_STATUS_RMSKIP_CHK(node, langType, status) \
  do { \
    switch ((status)) { \
      case InterraDesignWalkerCB::eNormal: break; \
      case InterraDesignWalkerCB::eStop: return InterraDesignWalkerCB::eStop; break; \
      case InterraDesignWalkerCB::eSkip: status = InterraDesignWalkerCB::eNormal; break; \
      case InterraDesignWalkerCB::eInvalid: CB_ISSUE_ASSERT(0, node, langType, "Invalid callback status"); break; \
      } \
  } while (0) /* caller adds semicolon */

//! Status check within the loop. Returns unless eNormal or eSkip.
/*!
  This should only be used when inside a loop for ending/continuing a loop iteration
  for non-normal/skip status.
*/
#define CB_STATUS_LOOP_CHK(node, langType, status) \
  if (1) { \
    switch ((status)) { \
    case InterraDesignWalkerCB::eNormal: break; \
    case InterraDesignWalkerCB::eStop: return InterraDesignWalkerCB::eStop; break; \
    case InterraDesignWalkerCB::eSkip: continue; break; \
    case InterraDesignWalkerCB::eInvalid: CB_ISSUE_ASSERT(0, node, langType, "Invalid callback status"); break; \
    } \
  } /* caller adds semicolon */

InterraDesignWalkerCB::~InterraDesignWalkerCB()
{}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::design(Phase, mvvNode, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::hdlEntity(Phase, mvvNode, mvvLanguageType, const char*, bool*)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::hdlComponentInstanceDeclaration(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::hdlComponentInstance(Phase, mvvNode, 
					    const char*, 
					    mvvNode, 
					    mvvNode, 
					    mvvNode, 
					    const char*, 
					    mvvNode, 
					    const SInt32*, 
					    const UInt32*, 
					    mvvLanguageType, 
					    mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::portList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::declarations(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::declaration(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::declaredAssignList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::alwaysBlockList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::specifyBlockList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::scopeVariable(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::declaredAssign(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::specifyBlock(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::generateBlock(Phase, mvvNode, const char*, mvvLanguageType, const SInt32*)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::generateBlockItemList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::generateItem(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::localNetList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::localVariableList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::scopeVariableList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::generateBlockNetList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::taskFunctionList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::initialBlockList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::contAssignList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::forGenerateLoop(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::forGenerate(Phase, mvvNode, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::forGenerateIntIndex(Phase, mvvNode, SInt32, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::ifGenerate(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::ifGenerateTrue(Phase, mvvNode, const char*, mvvLanguageType)
{
  return eNormal;
}

// currently only vlog, but could be applied to vhdl as well
mvvNode InterraDesignWalkerCB::substituteEntity(mvvNode, mvvLanguageType, const UtString&, mvvNode, bool, mvvLanguageType)
{
  return NULL;
}

InterraDesignWalkerCB::Status
InterraDesignWalkerCB::vlogForeignAttribute(mvvNode, const UtString&, mvvNode*, mvvNode*) 
{
  return eNormal;
}

// Following are the vhdl v. vlog callbacks. Not sure if some of
// these should get merged somehow.

// currently verilog only
InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::udp(Phase, mvvNode, const char*, const char*, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::module(Phase, mvvNode, const char*, const char*, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::cModel(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::caseGenerate(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::caseGenerateTrue(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::gatePrimitive(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::net(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::localNet(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::contAssign(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::initialBlock(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::alwaysBlock(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::variable(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::port(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::alias(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::file(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::taskFunction(Phase, mvvNode, const char*, mvvLanguageType)
{
  return eNormal;
}


// currently vhdl only
InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::architecture(Phase, mvvNode, const char*, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::blockStmt(Phase, mvvNode, const char*, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::concurrentStmtList(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::concurrentStmt(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::concSigAssign(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::concProcess(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::conditionalSigAssign(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::selSigAssign(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::concProcCall(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::assertStmt(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::useClause(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::genericClause(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::package(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::packageDecl(Phase, mvvNode, mvvLanguageType, const char*,
                                   const char*, bool)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::packageBody(Phase, mvvNode, mvvLanguageType)
{
  return eNormal;
}

// error cases
InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::unsupportedConcStmt(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Unsupported concurrent statement in this context");
  return eStop;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::unsupportedInstanceType(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Unsupported instance in this context");
  return eStop;
}

InterraDesignWalkerCB::Status
InterraDesignWalkerCB::generateElabFail(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Unable to elaborate generate statement");
  return eStop;
}

InterraDesignWalkerCB::Status
InterraDesignWalkerCB::generateElabError(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Generation elaboration error");
  return eStop;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::nestedGenerateInstance(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Unsupported nested generate instances");
  return eStop;
}

void InterraDesignWalkerCB::unnamedGenerateBlock(veNode, const char*, mvvLanguageType)
{
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::emptyArchitecture(mvvNode, const char*, mvvLanguageType)
{
  return eNormal;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::unsupportedGenerateItem(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Unsupported item type within a generate block");
  return eStop;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::componentElabFail(mvvNode node, mvvLanguageType langType, int)
{
  CB_ISSUE_ASSERT(0, node, langType, "Failed to elaborate");
  return eStop;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::unsupportedDeclaration(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Unsupported declaration type");
  return eStop;
}

InterraDesignWalkerCB::Status 
InterraDesignWalkerCB::implicitGuardNullInGuardedBlock(mvvNode node, mvvLanguageType langType)
{
  CB_ISSUE_ASSERT(0, node, langType, "Unsupported Guard statment");
  return eStop;
}

mvvNode
InterraDesignWalkerCB::getSubstituteEntity(UtString parentEntityName,
					   mvvNode entityDecl, 
					   mvvLanguageType entityLanguage, 
					   const UtString& entityDeclName, 
					   const UtString& instanceName,
					   bool isUDP, 
					   mvvLanguageType callingLanguage,
					   TicProtectedNameManager* ticProtectedNameMgr,
					   IODBNucleus* iodb,
					   UtString* new_mod_name,
					   bool *found,
					   UtMap<UtString,UtString>** portMap)
{
    // Compose the partial instance path, like <parent_module_name>.<instance_name>
    HdlVerilogPath vlog_path;
    UtString instPathName(parentEntityName);
    vlog_path.compPathAppend(&instPathName, instanceName.c_str());

    bool case_insensitive_search = false;
    if (callingLanguage == MVV_VHDL) {
	case_insensitive_search = true;
    }

    IODBNucleus::Lang lang;
    // Look for <module>
    *found = iodb->findSubstituteModule( entityDeclName, 
					 case_insensitive_search, 
					 new_mod_name, 
					 portMap, 
					 lang );

    // If not found look for <module.instance>
    if (!*found) {
	*found = iodb->findSubstituteModule( instPathName, 
					     case_insensitive_search, 
					     new_mod_name, 
					     portMap, 
					     lang );
    }
    
    if (!*found) return NULL;

    mvvNode newMod = NULL;

    if (IODBNucleus::VERILOG == lang) {
      newMod = VerilogPopulate::substituteModule( entityDecl,
						  entityLanguage,
						  isUDP,
						  *new_mod_name,
						  ticProtectedNameMgr );
    } 
    else if (IODBNucleus::VHDL == lang) {
      newMod = VhdlPopulate::substituteEntity ( entityDecl,
						entityLanguage,
						isUDP,
						*new_mod_name,
						ticProtectedNameMgr );
    }

    return newMod;
}

void InterraDesignWalkerCB::addLocationAndType(mvvNode node, mvvLanguageType langType, UtString* str)
{
  switch ( langType ){
  case MVV_VHDL: {
    vhNode vh_node = node->castVhNode();
    JaguarString type_name(vhGetObjTypeName(vhGetObjType(vh_node)));
    (*str) << vhGetSourceFileName(vh_node) << ":" << vhGetLineNumber(vh_node) << " " << (const char*)type_name;
    break;
  }
  case MVV_VERILOG: {
    veNode ve_node = node->castVeNode();
    CheetahStr filename(veNodeGetFileName(ve_node));
    CheetahStr type_name(veObjTypeGetString(veNodeGetObjType(ve_node)));
    (*str) << (const char*)filename << ":" << (UInt32)veNodeGetLineNumber(ve_node) << " " << (const char*)type_name;
    break;
  }
  default:
      INFO_ASSERT(0, "Unknown language type.");
  }
}




InterraDesignWalker::InterraDesignWalker(InterraDesignWalkerCB* callback, TicProtectedNameManager* tic_protected_nm_mgr) :
  mCallback(callback), mTicProtectedNameMgr(tic_protected_nm_mgr)
{}

InterraDesignWalker::~InterraDesignWalker()
{}


InterraDesignWalkerCB::Status 
InterraDesignWalker::design(mvvNode rootMod, mvvNode architecture, mvvNode blk_cfg,
                            const char* logical_work)
{
  if (rootMod == NULL)
    return InterraDesignWalkerCB::eNormal;

  if (logical_work != NULL) {
    mLogicalWorkLib = logical_work; // Save logical work library.
  }

  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  mvvLanguageType langType = mvvGetLanguageType(rootMod);
  
  status = mCallback->design(InterraDesignWalkerCB::ePre, rootMod, architecture, langType);

  CB_STATUS_CHK(rootMod, langType, status);

  switch(langType)
  {
  case MVV_VHDL:
  case MVV_VERILOG:
    status = hdlEntity(rootMod, architecture, blk_cfg);
    break;
  case MVV_UNKNOWN_LANGUAGE:
    INFO_ASSERT(0, "Unknown language type.");
    break;
  }
  CB_STATUS_NESTED_CHK(rootMod, langType, status);

  status = mCallback->design(InterraDesignWalkerCB::ePost, rootMod, architecture, langType);

  CB_STATUS_CHK(rootMod, langType, status);  

  mLogicalWorkLib.clear(); // Clear logical work library.

  return status;
}



void InterraDesignWalker::getHdlEntityName(mvvNode entity, mvvLanguageType langType, 
                                            UtString* nameBuf, bool* isUDP)
{
  *isUDP = false;
  if (langType == MVV_VERILOG)
  {
    veNode ve_node = entity->castVeNode();
    mTicProtectedNameMgr->getVisibleName(ve_node, nameBuf);
    *isUDP = (veNodeGetObjType(ve_node) == VE_UDP);
  }
  else if (langType == MVV_VHDL)
  {
    vhNode vh_entity = entity->castVhNode();
    JaguarString entityName(vhGetEntityName(vh_entity));
    *nameBuf = entityName;
  }
}

InterraDesignWalkerCB::Status
InterraDesignWalkerCB::getEntArchPairFromForeignAttr(mvvNode vlogNode, mvvNode* ent, mvvNode* arch)
{
  // If this module doesn't contain an instance attribute just return
  veNode moduleInstAttribute = veNodeGetInstanceAttribute(static_cast<veNode>(vlogNode));
  if (moduleInstAttribute == NULL) {
    return InterraDesignWalkerCB::eNormal;
  }

  UtString libName, entName, archName;
  UtString sep(".: ");
  CheetahList attributeList(veInstanceAttributeGetDeclList(moduleInstAttribute));
  veNode attrib = NULL;

  // Go thru the list of attributes (if any) and extract the attribute declarations
  while ((attrib = veListGetNextNode(attributeList))) {
    if (VE_ATTRIBUTEDECL == veNodeGetObjType(attrib)) {
      veAttributeType type = veAttributeDeclGetAttributeType(attrib);
      CheetahStr attribName(veAttributeDeclGetName(attrib));

      // Make sure it is an integer attribute and that its name is foreign
      if ((type == INTEGER_TYPE) && (UtString(attribName) == "foreign")) {
	CheetahList exprList(veAttributeDeclGetExpressionList(attrib));
	veNode expr = veListGetNextNode(exprList);
	UtString exprString(veStringGetString(expr));
	size_t first = exprString.find(" ");
	size_t size = exprString.find_last_of("\"") - first;
	
	// extract the library, entity and architecture name from the specified string.
	getLibEntArch(exprString.substr(first,size), sep, libName, entName, archName);
	
	// If we failed to extract the entity and architecture name, make sure to make
	// everything null so no replacements are made.
	if (libName.empty() || entName.empty() || archName.empty()) {
	  *ent = NULL;
	  *arch = NULL;
	  return InterraDesignWalkerCB::eInvalid;
	}
	else {
	  char* lib = strdup((char*)libName.c_str());
	  char* ent_name = strdup((char*)entName.c_str());
	  char* arch_name = strdup((char*)archName.c_str());
	  *ent = static_cast<mvvNode>(vhOpenEntity(lib, ent_name));
	  *arch = static_cast<mvvNode>(vhOpenArch(lib, ent_name, arch_name));
	  if ((*ent == NULL) || (*arch == NULL)) {
            // CB_ISSUE_ASSERT(0, vlogNode, MVV_VERILOG, "Unable to locate entity/arch in this context"); // the caller is responsible for error messages 
	    return InterraDesignWalkerCB::eStop;
	  }
	}
	return InterraDesignWalkerCB::eNormal;
      }
    }
  }
  return InterraDesignWalkerCB::eNormal;
}

void InterraDesignWalkerCB::getLibEntArch(UtString libEntArch, UtString sep, UtString& libName, UtString& entName, UtString &archName)
{
  libName = "";
  entName = "";
  archName = "";

  // Tokenize the supplied string using the specified separators
  StrToken token(libEntArch.c_str(), sep.c_str());
  if (token.atEnd()) {
    return;
  }
  libName = *token;
  ++token;
  if (token.atEnd()) {
    return;
  }
  entName = *token;
  ++token;
  if (token.atEnd()) {
    return;
  }
  archName = *token;
}

static mvvNode sFindBlockConfig(mvvNode block, mvvNode blk_cfg, mvvLanguageType lang)
{
  mvvNode retCfg = blk_cfg;
  
  if (blk_cfg != NULL)
  {
    switch (lang)
    {
    case MVV_VHDL:
      {
        vhNode block_config = blk_cfg->castVhNode();
	vhNode stmt = block->castVhNode();
        if (vhGetObjType(block_config) == VHBLOCKCONFIG)
          retCfg = vhFindInBlockConfig(stmt, block_config);
      }
      break;
    case MVV_VERILOG:
      INFO_ASSERT(0, "Verilog configurations not supported.");
      break;
    case MVV_UNKNOWN_LANGUAGE:
      INFO_ASSERT(0, "Unknown language type.");
      break;
    }
  }
  return retCfg;
}

static void sFindUniqueVlogGenerateBlockName(UtString* bname, veNode scope, UInt32 genBlockCount)
{
  // Make sure the generated name is unique
  *bname = "genblk";
  *bname << genBlockCount;
  UtString padding = "0";

  // If the name is already being used, prepend a "0" to the number of this block
  // until the name is unique
  int count = 0;
  while (veScopeGetNamedObject(scope, (char*)bname->c_str())) {
    *bname = "genblk";
    *bname << padding << genBlockCount;
    padding += "0";
    count++;
    INFO_ASSERT(count <= 100, "Cannot find unique name for a unnamed generate block");
  }
}

InterraDesignWalkerCB::Status 
InterraDesignWalker::hdlEntity(mvvNode node, mvvNode mvv_arch, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  
  mvvLanguageType langType = mvvGetLanguageType(node);
  
  bool isCmodel = false;

  // mvvDesignUnitGetName is deprecated, so don't use this
  // MvvStr entityName(mvvDesignUnitGetName(node));
  
  // Note that the pre phase callback is called within each language
  // type so we can get the name of the hdl entity first.
  
  UtString entityName;
  bool isUDP;
  getHdlEntityName(node, langType, &entityName, &isUDP);
  
  status = mCallback->hdlEntity(InterraDesignWalkerCB::ePre, node, langType, entityName.c_str(), &isCmodel);
  
  CB_STATUS_CHK(node, langType, status);
  

  switch(langType)
  {
  case MVV_VHDL:
    {
      vhNode vh_entity = node->castVhNode();

      // Handle anything we need to out of the entity's visible
      // packages
      status = processVhdlUseClause(vh_entity);
      CB_STATUS_NESTED_CHK(node, langType, status);

      // Create the ports      
      status = processVhdlPortClause(vh_entity);
      CB_STATUS_NESTED_CHK(node, langType, status);

      // Process generics
      status = processVhdlGenericClause(vh_entity);
      CB_STATUS_NESTED_CHK(node, langType, status);

      // Process all the declarations 
      status = processVhdlDeclarations(vh_entity);
      CB_STATUS_NESTED_CHK(node, langType, status);
      
      if (! isCmodel)
      {
        vhNode vh_arch = mvv_arch->castVhNode();
        status = architecture(vh_arch, blk_cfg);
        CB_STATUS_NESTED_CHK(node, langType, status);
      }
      else
      {
        status = mCallback->cModel(InterraDesignWalkerCB::ePre, vh_entity, MVV_VHDL);
        bool isSkip;
        CB_STATUS_SKIP_CHK(node, langType, status, isSkip);
        if (! isSkip)
        {
          status = mCallback->cModel(InterraDesignWalkerCB::ePost, vh_entity, MVV_VHDL);
          CB_STATUS_RMSKIP_CHK(node, langType, status);
        }
      }
    }
    break;
  case MVV_VERILOG:
    {
      veNode ve_module = node->castVeNode();

      if (isUDP)
        status = vlogUDP(ve_module, isCmodel);
      else if (veNodeGetObjType(ve_module) == VE_MODULE)
        status = vlogModule(ve_module, blk_cfg, isCmodel);
      else
      {
        status = mCallback->unsupportedInstanceType(ve_module, MVV_VERILOG);
        CB_STATUS_RMSKIP_CHK(node, langType, status);
      }
    }
    break;
  case MVV_UNKNOWN_LANGUAGE:
    INFO_ASSERT(0, "Unknown language type.");
    break;
  }

  CB_STATUS_NESTED_CHK(node, langType, status);

  status = mCallback->hdlEntity(InterraDesignWalkerCB::ePost, node, langType, entityName.c_str(), &isCmodel);
  
  CB_STATUS_CHK(node, langType, status);
  
  return status;
}

InterraDesignWalkerCB::Status 
InterraDesignWalker::architecture(vhNode vh_arch, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  
  const char* archName = vhGetArchName( vh_arch );
  status = mCallback->architecture(InterraDesignWalkerCB::ePre, vh_arch, archName, MVV_VHDL);
  CB_STATUS_CHK(vh_arch, MVV_VHDL, status);
  
  status = processVhdlDeclarations(vh_arch);
  CB_STATUS_NESTED_CHK(vh_arch, MVV_VHDL, status);
  
  // Process concurrent statements
  JaguarList concStmtList( vhGetConcStmtList( vh_arch ));
  if (concStmtList.empty())
  {
    status = mCallback->emptyArchitecture( vh_arch, archName, MVV_VHDL );
    CB_STATUS_RMSKIP_CHK(vh_arch, MVV_VHDL, status);
  }
  else
  {
    status = concurrentStmtList(vh_arch, blk_cfg);
    CB_STATUS_NESTED_CHK(vh_arch, MVV_VHDL, status);
  }

  status = mCallback->architecture(InterraDesignWalkerCB::ePost, vh_arch, archName, MVV_VHDL);
  
  CB_STATUS_CHK(vh_arch, MVV_VHDL, status);
  
  return status;
}


InterraDesignWalkerCB::Status 
InterraDesignWalker::concurrentStmtList(vhNode vh_conc, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  
  status = mCallback->concurrentStmtList(InterraDesignWalkerCB::ePre, vh_conc, MVV_VHDL);
  CB_STATUS_CHK(vh_conc, MVV_VHDL, status);
  
  JaguarList concStmtList( vhGetConcStmtList( vh_conc ));
  if (! concStmtList.empty())
  {
    vhNode stmt = 0;
    while((stmt = vhGetNextItem(concStmtList)))
    {
      status = concurrentStmt(stmt, blk_cfg);
      CB_STATUS_NESTED_CHK(stmt, MVV_VHDL, status);
    }
  }

  status = mCallback->concurrentStmtList(InterraDesignWalkerCB::ePost, vh_conc, MVV_VHDL);
  CB_STATUS_CHK(vh_conc, MVV_VHDL, status);
  return status;
}

InterraDesignWalkerCB::Status 
InterraDesignWalker::concurrentStmt(vhNode vh_conc, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  
  status = mCallback->concurrentStmt(InterraDesignWalkerCB::ePre, vh_conc, MVV_VHDL);
  
  CB_STATUS_CHK(vh_conc, MVV_VHDL, status);

  switch(vhGetObjType(vh_conc)) 
  {
  case VHCONCSIGASGN:
    status = vhdlConcSigAssign(vh_conc);
    break;
  case VHPROCESS:
    status = vhdlConcProcess(vh_conc);
    break;
  case VHCONDSIGASGN:
    status = vhdlConditionalSigAssign(vh_conc);
    break;
  case VHSELSIGASGN:
    status = vhdlSelSigAssign(vh_conc);
    break;
  case VHCONCPROCCALL:
    status = vhdlConcProcCall(vh_conc);
    break;
  case VHBLOCK:
    {
      mvvNode block_cfg = sFindBlockConfig(vh_conc, blk_cfg, MVV_VHDL);
      status = vhdlBlockStmt(vh_conc, block_cfg);
    }
    break;
  case VHCOMPINSTANCE:
  case VHENTITYINSTANCE:
  case VHCONFINSTANCE:
    {
      status = hdlComponentInstance(vh_conc, blk_cfg, MVV_VHDL);
    }
    break;
  case VHFORGENERATE:
    {
      status = vhdlForGenerate(vh_conc, blk_cfg);
    }
    break;
  case VHIFGENERATE:
    {
      status = vhdlIfGenerate(vh_conc, blk_cfg);
    }
    break;
  case VHCONCASSERT:
    status = vhdlAssertStmt(vh_conc);
    break;
  default:
    status = mCallback->unsupportedConcStmt(vh_conc, MVV_VHDL);
    break;
  }
  CB_STATUS_NESTED_CHK(vh_conc, MVV_VHDL, status);
  
  status = mCallback->concurrentStmt(InterraDesignWalkerCB::ePost, vh_conc, MVV_VHDL);
  
  CB_STATUS_CHK(vh_conc, MVV_VHDL, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlForGenerate(vhNode vh_conc_stmt, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  status = mCallback->forGenerateLoop(InterraDesignWalkerCB::ePre, vh_conc_stmt, MVV_VHDL);

  CB_STATUS_CHK(vh_conc_stmt, MVV_VHDL, status);

  // If a for generate fails to elaborate it's because its
  // elaboration range is a null range.  So, do absolutely nothing
  // for it.
  if (vhElaborateGenerateRange(vh_conc_stmt) == true)
  {
    // Iterate over the for-generate loop and process the generate block.
    // Use Jaguar's API to update the for generate index. At the end 
    // of the iteration, the vhUpdateGenerateIndex() will return NULL
    // which is used as the loop terminating condition.
    do 
    {
      vhNode forIndex = vhGetParamSpec(vh_conc_stmt);
      vhExpr vh_val = vhGetInitialValue(forIndex);
      
      status = mCallback->forGenerate(InterraDesignWalkerCB::ePre, vh_conc_stmt, vh_val, MVV_VHDL);
      bool isSkip;
      CB_STATUS_SKIP_CHK(vh_conc_stmt, MVV_VHDL, status, isSkip);
      
      if (! isSkip)
      {
        status = processVhdlDeclarations(vh_conc_stmt);
        CB_STATUS_NESTED_CHK(vh_conc_stmt, MVV_VHDL, status);

        mvvNode generate_config = sFindBlockConfig(vh_conc_stmt, blk_cfg, MVV_VHDL);
        status = concurrentStmtList(vh_conc_stmt, generate_config);
        CB_STATUS_NESTED_CHK(vh_conc_stmt, MVV_VHDL, status);
        
        mCallback->forGenerate(InterraDesignWalkerCB::ePost, vh_conc_stmt, vh_val, MVV_VHDL);
        CB_STATUS_RMSKIP_CHK(vh_conc_stmt, MVV_VHDL, status);
        // skip detection has no meaning on a post call
      }
    } while(vhUpdateGenerateIndex(vh_conc_stmt));
    // Now reset the for generate index
    vhResetGenerateIndex(vh_conc_stmt); // Don't remove this line
  } // if keepGoing
  
  status = mCallback->forGenerateLoop(InterraDesignWalkerCB::ePost, vh_conc_stmt, MVV_VHDL);
  
  CB_STATUS_CHK(vh_conc_stmt, MVV_VHDL, status);
  return status;
}


InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlIfGenerate(vhNode vh_conc_stmt, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  
  status = mCallback->ifGenerate(InterraDesignWalkerCB::ePre, vh_conc_stmt, MVV_VHDL);
  CB_STATUS_CHK(vh_conc_stmt, MVV_VHDL, status);

  
  VhBoolean condition = VH_TRUE;
  vhNode evalCond = NULL;
  vhExpr condExpr = vhGetCondition(vh_conc_stmt);
  if (VH_LOCALLY_STATIC == vhGetStaticType(condExpr))
  {
    evalCond = vhGetStaticValue(condExpr);
  }
  else
  {
    evalCond = vhEvaluateExpr(condExpr);
  }
  if ( evalCond )
  {
    condition = (VhBoolean)vhGetIdEnumLitValue(evalCond);
    if ( condition == VH_TRUE )
    {
      JaguarString vh_label_str(vhGetLabel(vh_conc_stmt));
      status = mCallback->ifGenerateTrue(InterraDesignWalkerCB::ePre, vh_conc_stmt, vh_label_str, MVV_VHDL);
      bool isSkip;
      CB_STATUS_SKIP_CHK(vh_conc_stmt, MVV_VHDL, status, isSkip);
      
      if (! isSkip)
      {
        // declarations
        status = processVhdlDeclarations(vh_conc_stmt);
        CB_STATUS_NESTED_CHK(vh_conc_stmt, MVV_VHDL, status);
        
        // Process the statements
        mvvNode generate_config = sFindBlockConfig(vh_conc_stmt, blk_cfg, MVV_VHDL);
        status = concurrentStmtList(vh_conc_stmt, generate_config);
        CB_STATUS_NESTED_CHK(vh_conc_stmt, MVV_VHDL, status);
        
        status = mCallback->ifGenerateTrue(InterraDesignWalkerCB::ePost, vh_conc_stmt, vh_label_str, MVV_VHDL);
        CB_STATUS_RMSKIP_CHK(vh_conc_stmt, MVV_VHDL, status);
        // skip detection has no meaning on a post call
      }
    }
  }

  status = mCallback->ifGenerate(InterraDesignWalkerCB::ePost, vh_conc_stmt, MVV_VHDL);
  CB_STATUS_CHK(vh_conc_stmt, MVV_VHDL, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlBlockStmt(vhNode vh_conc_stmt, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  
  JaguarString vh_label_str(vhGetLabel(vh_conc_stmt));  
  status = mCallback->blockStmt(InterraDesignWalkerCB::ePre, vh_conc_stmt, vh_label_str, MVV_VHDL);
  CB_STATUS_CHK(vh_conc_stmt, MVV_VHDL, status);
  
  status = processVhdlDeclarations(vh_conc_stmt);
  CB_STATUS_NESTED_CHK(vh_conc_stmt, MVV_VHDL, status);

  // If it is GUARDED block, then process the implicit signal GUARD
  vhExpr vh_guard_expr = vhGetGuardExpr(vh_conc_stmt);
  if (vh_guard_expr)
  {
    status = processVhdlBlockGuardSignal(vh_conc_stmt);
    CB_STATUS_NESTED_CHK(vh_conc_stmt, MVV_VHDL, status);
  }

  status = concurrentStmtList(vh_conc_stmt, blk_cfg);
  CB_STATUS_NESTED_CHK(vh_conc_stmt, MVV_VHDL, status);

  status = mCallback->blockStmt(InterraDesignWalkerCB::ePost, vh_conc_stmt, vh_label_str, MVV_VHDL);
  CB_STATUS_CHK(vh_conc_stmt, MVV_VHDL, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::hdlComponentInstance(mvvNode component, mvvNode blk_cfg, mvvLanguageType lang)
{
  // If the calling language is verilog and the component instance is
  // a udp then we cannot elaborate.
  if (lang == MVV_VERILOG)
  {
    veNode ve_inst = component->castVeNode();
    veNode ve_udp = veInstanceGetMaster( ve_inst );
    veObjType type = veNodeGetObjType(ve_udp);
    if (type == VE_UDP)
    {
      // process the component instance declaration now, without
      // elaboration. UDPs cannot be elaborated.
      
      UtString resolvedEntityName;
      mTicProtectedNameMgr->getVisibleName(ve_udp, &resolvedEntityName);
      
      // pass in the calling language, since this is an entity
      // substitution. elabLang refers to the instance language.
      mvvNode entityDecl = ve_udp;
      mvvNode new_entity = mCallback->substituteEntity(ve_udp, lang, resolvedEntityName, component, true, lang);
      if (new_entity)
      {
        entityDecl = new_entity;
        bool isUDP = false;
        getHdlEntityName(entityDecl, lang, &resolvedEntityName, &isUDP);

      }
      return hdlComponentInstanceDeclaration(component, entityDecl, (mvvNode)ve_udp, new_entity, resolvedEntityName.c_str(), NULL, blk_cfg, MVV_VERILOG, MVV_VERILOG);
    }
  }

  // elaborate before calling the callback so the callback can use the
  // elaborated information
  mvvNode mvv_elab  = mvvElaborateInstance( component, blk_cfg);

  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  // If there was a problem during the elaboration then handle error
  // condition before returning now.  Problems might be that nothing
  // returned from mvvElaborateInstance, or mvvElabGetStatus returns
  // something other than 0.  mvvElabGetStatus() will return 1 if
  // elaboration fails (in some cases) or 2 if elaborated master is a
  // black box.
  // Since we set black box creation to MVV_FALSE, the only acceptable
  // outcome is elab_status of 0, which means elaboration succeeded.
  
  int elab_status = ( mvv_elab != 0 ) ? mvvElabGetStatus(mvv_elab) : 1;
  if ( elab_status != 0 ) {
    if (mvv_elab != NULL) {
      mvvUnElaborateInstance( component, mvv_elab );  // do an unelab (bug7629)
    }
    status = mCallback->componentElabFail(component, lang, elab_status);
    // return here 
    CB_STATUS_CHK(component, lang, status);
    return status; // in case eNormal is returned.
  }
  
  mvvLanguageType elabLang = mvvGetLanguageType( mvv_elab );
  mvvNode entityDecl = mvvElabGetPrimaryUnit(mvv_elab);
  mvvNode archDecl = mvvElabGetSecondaryUnit(mvv_elab);
  mvvNode instance_config = mvvElabGetBlockConfig(mvv_elab);

  // Check for the case where a Verilog module contains an attribute that
  // specifies a VHDL library, entity and architecture which will replace
  // the Verilog module, using the ovi2 foreign interface. See bug 12894.
  if (elabLang == MVV_VERILOG) {
    mvvNode Ent = NULL;
    mvvNode Arch = NULL;
    UtString entityDeclName;
    bool isUDP = false;
    getHdlEntityName(entityDecl, elabLang, &entityDeclName, &isUDP);
    status = mCallback->vlogForeignAttribute(entityDecl, entityDeclName, &Ent, &Arch);
    CB_STATUS_NESTED_CHK(component, lang, status);
    
    // If an entity and architecture was found, use it.
    if (Ent != NULL && Arch != NULL) {
      entityDecl = Ent;
      archDecl = Arch;
      elabLang = MVV_VHDL;
    }
  }

  mvvNode new_entity = NULL;
  mvvNode old_entity = entityDecl;
  
  UtString resolvedEntityName;
  // Replace the master with a substitute entity if one exists.
  {
    bool isUDP = false;
    getHdlEntityName(entityDecl, elabLang, &resolvedEntityName, &isUDP);

    // pass in the elaborated language, since that refers to the
    // unelaborated block type.
    // pass in the scope language because the instance type is still
    // of the scope language, even if its block type is a different
    // language. E.g., A vhdl architecture instantiates a verilog
    // module. The instantiation is still vhdl, but the module is verilog.
    new_entity = mCallback->substituteEntity(entityDecl, elabLang, resolvedEntityName, component, isUDP, lang);
    if (new_entity)
    {
      entityDecl = new_entity;
      // We should reset the
      // language of the new_entity here. We could be substituting a
      // verilog module with a vhdl entity.
      elabLang = mvvGetLanguageType(entityDecl);
      if(MVV_VHDL == elabLang) {
	  archDecl = vhHandleOpenMRArch (new_entity->castVhNode());
      }
      getHdlEntityName(entityDecl, elabLang, &resolvedEntityName, &isUDP);
    }
  }
  
  status = hdlComponentInstanceDeclaration(component, entityDecl, old_entity, new_entity, resolvedEntityName.c_str(), archDecl, instance_config, lang, elabLang);
  mvvUnElaborateInstance( component, mvv_elab );
  
  // Check nested because skip should have return normal at this point.
  CB_STATUS_NESTED_CHK(component, lang, status);
  
  // Do normal check too in case we put something in-between the
  // nested check and this.
  CB_STATUS_CHK(component, lang, status);  
  return status;
}


InterraDesignWalkerCB::Status
InterraDesignWalker::hdlComponentInstanceDeclaration(mvvNode component, mvvNode entityDecl, mvvNode old_entity, mvvNode new_entity, const char* entityName, mvvNode archDecl, mvvNode instance_config, mvvLanguageType lang, mvvLanguageType elabLang)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  status = mCallback->hdlComponentInstanceDeclaration(InterraDesignWalkerCB::ePre, component, lang);
  CB_STATUS_CHK(component, lang, status);
  
  if (lang == MVV_VERILOG)
  {
    veNode ve_inst = component->castVeNode();
    veNode ve_range = veInstanceGetRange(ve_inst);
    if (ve_range) {
      ConstantRange range;
      SInt32 left = veExprEvaluateValue(veRangeGetLeftRange(ve_range));
      SInt32 right = veExprEvaluateValue(veRangeGetRightRange(ve_range));
      range.setMsb(left);
      range.setLsb(right);
      
      // Create one instance per range value
      for (UInt32 offset = 0; offset < range.getLength(); ++offset) {
        SInt32 cur_idx = range.index(offset);
        status = processHdlComponentInstance(component, lang, elabLang, entityDecl, old_entity, new_entity, entityName, archDecl, instance_config, &cur_idx, &offset);
        CB_STATUS_NESTED_CHK(component, lang, status);
      }
      
    } else {
      status = processHdlComponentInstance(component, lang, elabLang, entityDecl, old_entity, new_entity, entityName, archDecl, instance_config, NULL, NULL);
      // no status check needed here. Handled before return.
    }
  }
  else if (lang == MVV_VHDL)
    status = processHdlComponentInstance(component, lang, elabLang, entityDecl, old_entity, new_entity, entityName, archDecl, instance_config, NULL, NULL);

  CB_STATUS_NESTED_CHK(component, lang, status);

  status = mCallback->hdlComponentInstanceDeclaration(InterraDesignWalkerCB::ePost, component, lang);
  CB_STATUS_CHK(component, lang, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogUDP(veNode ve_udp, bool isCmodel)
{
  CheetahStr unit(veUDPGetTimeUnit(ve_udp));
  CheetahStr precision(veUDPGetTimePrecision(ve_udp));

  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->udp(InterraDesignWalkerCB::ePre, ve_udp, unit, precision, MVV_VERILOG);
  CB_STATUS_CHK(ve_udp, MVV_VERILOG, status);

  CheetahList ve_port_iter(veUDPGetPortList(ve_udp));
  status = processVlogPortList(ve_udp, ve_port_iter);
  CB_STATUS_NESTED_CHK(ve_udp, MVV_VERILOG, status);

  // if this is a cmodel return after processing the ports
  if (! isCmodel)
  {
    CheetahList ve_net_iter(veUDPGetNetList(ve_udp));
    status = processVlogLocalNetList(ve_udp, ve_net_iter);
    CB_STATUS_NESTED_CHK(ve_udp, MVV_VERILOG, status);
    
    // currently don't do anything else for udps. The post function
    // should handle getting whatever information it needs from the udp
    // for now.
  }

  status = mCallback->udp(InterraDesignWalkerCB::ePost, ve_udp, unit, precision, MVV_VERILOG);
  CB_STATUS_CHK(ve_udp, MVV_VERILOG, status);
  
  return status;
}


InterraDesignWalkerCB::Status
InterraDesignWalker::vlogModule(veNode ve_module, mvvNode blk_cfg, bool isCmodel)
{
  CheetahStr unit(veModuleGetTimeUnit(ve_module));
  CheetahStr precision(veModuleGetTimePrecision(ve_module));

  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->module(InterraDesignWalkerCB::ePre, ve_module, unit, precision, MVV_VERILOG);

  CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

  {
    CheetahList ve_port_iter(veModuleGetPortList(ve_module));
    status = processVlogPortList(ve_module, ve_port_iter);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
  }

  // if this is a cmodel don't process anything after the ports, call
  // the user's callback for cmodule.
  if (! isCmodel)
  {
    status = processVlogModuleDeclarations(ve_module);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    
    // Do generates. 
    status = vlogGenerateStmtList(ve_module, blk_cfg);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    

    {
      CheetahList ve_hier_iter(veModuleGetScopeVariableList(ve_module));
      status = processVlogScopeVarList(ve_module, ve_hier_iter);
      CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    }
    
    status = processVlogTFList(ve_module);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    
    status = processVlogInitialBlockList(ve_module);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    
    status = processVlogContAssignList(ve_module);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    
    status = processVlogDeclAssignList(ve_module);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    
    status = processVlogAlwaysBlockList(ve_module);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
  
    {
      // loop instances
      CheetahList ve_inst_iter(veModuleGetInstanceList(ve_module));
      if (ve_inst_iter) {
        veNode ve_inst;
        while ((ve_inst = veListGetNextNode(ve_inst_iter))) {
          switch (veNodeGetObjType(ve_inst)) {
          case VE_GATEINSTANCE:
            {
              // This method adds the conts assigns to the current module
              // in the context.
              status = vlogGatePrimitive(ve_inst);
              break;
            }
          case VE_MODULEORUDPINSTANCE:
            status = hdlComponentInstance(ve_inst, blk_cfg, MVV_VERILOG);
            break;
          default:
            status = mCallback->unsupportedInstanceType(ve_inst, MVV_VERILOG);
            break;
          }
          
          CB_STATUS_RMSKIP_CHK(ve_module, MVV_VERILOG, status);
          // skip detection is not used. This is needed in case we want to fall
          // through an unsupported instance type.
        }
      }
    }
    
    status = processVlogSpecifyBlockList(ve_module);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
  } // if ! cmodel
  else 
  {
    status = mCallback->cModel(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    bool isSkip;
    CB_STATUS_SKIP_CHK(ve_module, MVV_VERILOG, status, isSkip);
    if (! isSkip)
    {
      status = mCallback->cModel(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
      CB_STATUS_RMSKIP_CHK(ve_module, MVV_VERILOG, status);
    }
  }
  
  status = mCallback->module(InterraDesignWalkerCB::ePost, ve_module, unit, precision, MVV_VERILOG);
  CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogGenerateStmtList(veNode ve_module, mvvNode blk_cfg)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;

  VlogGenItemClosure genItemClosure;
  
  CheetahList ve_gen_iter(veModuleGetGenerateStmtList(ve_module));
  if ( ve_gen_iter )
  {
    int start_errors=0;
    int start_warnings=0;
    int start_rtl_errors=0;
    veGetErrorWarningCount(&start_errors, &start_warnings, &start_rtl_errors);
      
    bool retval = veModuleElaborateGenerateStatement( ve_module ) == VE_TRUE;
    if ( retval == false )
    {
      status = mCallback->generateElabFail(ve_module, MVV_VERILOG);
      // Return from the function here
      CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
      return status; // in case eNormal is returned.
    }
      
    int end_errors=0;
    int end_warnings=0;
    int end_rtl_errors=0;
    veGetErrorWarningCount(&end_errors, &end_warnings, &end_rtl_errors);
    if ( ( start_errors != end_errors ) or ( start_rtl_errors != end_rtl_errors ) ){
      status = mCallback->generateElabError(ve_module, MVV_VERILOG);
      // Return from the function here
      CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
      return status; // in case eNormal is returned.
    }
    // We have generate statements
    veNode ve_gen;
    while ((ve_gen = veListGetNextNode(ve_gen_iter)))
    {
      status = vlogGenerateItem( ve_gen, blk_cfg, NULL, &genItemClosure);
      CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    }
  }

  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogGenerateItem(veNode ve_item,
				      mvvNode blk_cfg,
				      const SInt32* genForIndex,
				      VlogGenItemClosure* genItemClosure) 
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->generateItem(InterraDesignWalkerCB::ePre, ve_item, MVV_VERILOG);
  CB_STATUS_CHK(ve_item, MVV_VERILOG, status);


  veObjType obj_type = veNodeGetObjType( ve_item );        
  switch ( obj_type )
  {
  case VE_NET:
    if (veNetGetIsImplicit(ve_item) == VE_FALSE) {
      status = net(ve_item, MVV_VERILOG);
      CB_STATUS_NESTED_CHK(ve_item, MVV_VERILOG, status);
      status = vlogDeclAssign(ve_item);      
      // status check happens after switch
    }
    break;
  case VE_CONTASSIGN:
    status = vlogContAssign(ve_item);
    break;
  case VE_INITIAL:
    status = vlogInitialBlock(ve_item);
    break;
  case VE_ALWAYS:
    status = vlogAlwaysBlock(ve_item);
    break;
  case VE_GATEINSTANCE:
    status = vlogGatePrimitive(ve_item);
    break;
  case VE_MODULEORUDPINSTANCE:
    status = hdlComponentInstance(ve_item, blk_cfg, MVV_VERILOG);
    break;
  case VE_MEMORY:
  case VE_NETARRAY:
  case VE_REG:
  case VE_INTEGER:
  case VE_TIME:
  case VE_REAL:
  case VE_REALTIME:
    status = variable(ve_item, MVV_VERILOG);
    break;
  case VE_TASK:
  case VE_FUNCTION:
    status = vlogTF(ve_item);
    break;
  case VE_GENERATE:
    status = mCallback->nestedGenerateInstance(ve_item, MVV_VERILOG);
    CB_STATUS_RMSKIP_CHK(ve_item, MVV_VERILOG, status);
    // skip detection not used here. Allow fall through.
    break;
  case VE_GENERATE_BLOCK:
    status = vlogGenerateBlock( ve_item, blk_cfg, genForIndex, genItemClosure );
    break;
  case VE_GENERATE_FOR:
    // The generate block is always named
    status = vlogForGenerate( ve_item, blk_cfg, genItemClosure );
    break;
  case VE_GENERATE_IF:
    status = vlogIfGenerate( ve_item, blk_cfg, genItemClosure );
    break;
  case VE_GENERATE_CASE:
    status = vlogCaseGenerate( ve_item, blk_cfg, genItemClosure );
    break;
  case VE_GENVAR:
    // Silently ignore genvar declarations--they don't get populated
    break;
  case VE_LOCALPARAM:
    // Silently ignore localparam definitions, -- nothing to populate, cheetah handles their use
    break;
  case VE_EVENT:
    // We don't support named events at all yet.  Deliberate fallthrough.
  default:
    status = mCallback->unsupportedGenerateItem(ve_item, MVV_VERILOG);
    CB_STATUS_RMSKIP_CHK(ve_item, MVV_VERILOG, status);
    // skip detection not used here. Allow fall through.
    break;
  }

  CB_STATUS_NESTED_CHK(ve_item, MVV_VERILOG, status);  

  status = mCallback->generateItem(InterraDesignWalkerCB::ePost, ve_item, MVV_VERILOG);
  CB_STATUS_CHK(ve_item, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogGenerateBlockItemList(veNode veGenerate, 
						      mvvNode blk_cfg, 
						      const SInt32* genForIndex, 
						      VlogGenItemClosure* genItemClosure)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->generateBlockItemList(InterraDesignWalkerCB::ePre, veGenerate, MVV_VERILOG);
  CB_STATUS_CHK(veGenerate, MVV_VERILOG, status);

  CheetahList item_iter(veGenerateBlockGetItemList( veGenerate ));
  if ( item_iter )
  {
    veNode ve_item;
    while (( ve_item = veListGetNextNode( item_iter )))
    {
      status = vlogGenerateItem( ve_item, blk_cfg, genForIndex, genItemClosure );
      CB_STATUS_NESTED_CHK(ve_item, MVV_VERILOG, status);
    }      
  }
  
  status = mCallback->generateBlockItemList(InterraDesignWalkerCB::ePost, veGenerate, MVV_VERILOG);
  CB_STATUS_CHK(veGenerate, MVV_VERILOG, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogForGenerate(veNode veGenerate, 
				     mvvNode blk_cfg, 
				     VlogGenItemClosure* genItemClosure)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->forGenerateLoop(InterraDesignWalkerCB::ePre, veGenerate, MVV_VERILOG);
  CB_STATUS_CHK(veGenerate, MVV_VERILOG, status);
  
  /*
    According to Rich Cloutier, these PI's do NOT work, and are
    special-cased probably for some other Interra customer. So, unless
    you're feeling lucky don't use them.

    VeInitialiseGenerateForIndex(veNode genStmt, const char* File, int Line);
    VeResetGenerateForIndex(veNode genStmt, const char* File, int Line);
    VeUpdateGenerateForIndex(veNode genStmt, const char* File, int Line);
    VeGetGenerateForIndexValue(veNode genStmt, const char* File, int Line);
  */

  veNode loopInitial = veGenerateForGetInitial( veGenerate ); // VE_ASSIGN
  veNode loopCondition = veGenerateForGetCondition( veGenerate ); // VE_BINARY
  veNode loopStep = veGenerateForGetStep( veGenerate ); // VE_ASSIGN
  
  // determine the generate index start value
  veNode rhs = veAssignGetRhsExpr( loopInitial );
  CheetahStr value (veExprEvaluateValueInBinaryString( rhs ));
  
  // Set the lvalue of the initial expression to the initial value
  veNode lhs = veAssignGetLvalue( loopInitial );
  veSetObjectValue( lhs, value );
  
  // integer value of start index (we can use a SInt32 here because
  // a verilog genvar is handled as an integer)
  SInt32 genIndex = veExprEvaluateValue( rhs );
  
  // set up for the loop over the generate index
  lhs = veAssignGetLvalue( loopStep );
  rhs = veAssignGetRhsExpr( loopStep );
  
  // Iterate over the generate range.  notDone will eval to 0 when we're done.
  value = veExprEvaluateValueInBinaryString( loopCondition );
  SInt32  notDone = veGetDecimalNumberFromBinaryString(value);

  bool first = true;
  while ( notDone )
  {
    // sure would be nice to use forGenerate, but
    // veGenerateForGetBlock changes the genIndex (rhs) expression.
    status = mCallback->forGenerateIntIndex(InterraDesignWalkerCB::ePre, veGenerate, genIndex, MVV_VERILOG);
    bool isSkip;
    CB_STATUS_SKIP_CHK(veGenerate, MVV_VERILOG, status, isSkip);
    
    SInt32 prevIndex = genIndex;
    
    // this updates the genIndex
    veNode loopBlock = veGenerateForGetBlock( veGenerate ); // VE_GENERATE_BLOCK
    
    if (! isSkip)
    {
      // Handle incrementing the generate block count, need to do it here because
      // just want to increment once for the for loop (and not every iteration).
      if (first) {
        genItemClosure->incrGenBlkCnt();
      }

      status = vlogGenerateItem( loopBlock, blk_cfg, &genIndex, genItemClosure );
      CB_STATUS_NESTED_CHK(veGenerate, MVV_VERILOG, status);
      
      status = mCallback->forGenerateIntIndex(InterraDesignWalkerCB::ePost, veGenerate, prevIndex, MVV_VERILOG);
      CB_STATUS_RMSKIP_CHK(veGenerate, MVV_VERILOG, status);
    }
    
    // Get the integer index for the next iteration
    SInt32 old_genIndex = genIndex;
    genIndex = veExprEvaluateValue( rhs );
    if ( old_genIndex == genIndex ) {
      CB_ISSUE_ASSERT(0, veGenerate, MVV_VERILOG, "This Generate FOR appears to be an infinite loop, unable to proceed");
      return InterraDesignWalkerCB::eStop;
    }
    // Iteration code--update the string value in Cheetah
    value = veExprEvaluateValueInBinaryString( rhs );
    veSetObjectValue( lhs, value );
    value = veExprEvaluateValueInBinaryString( loopCondition );
    notDone = veGetDecimalNumberFromBinaryString(value);
    first = false;
  }
  
  status = mCallback->forGenerateLoop(InterraDesignWalkerCB::ePost, veGenerate, MVV_VERILOG);
  CB_STATUS_CHK(veGenerate, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogIfGenerate(veNode veGenerate, 
				    mvvNode blk_cfg, 
				    VlogGenItemClosure* genItemClosure) 
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->ifGenerate(InterraDesignWalkerCB::ePre, veGenerate, MVV_VERILOG);
  CB_STATUS_CHK(veGenerate, MVV_VERILOG, status);

  veNode veTruePath = NULL;

  // here we determine our own version of the true path because
  // veGenerateStatementGetTruePath does not work for If stmts
  // with a genvar in the condition
  veNode cond = veGenerateIfGetCondition(veGenerate);
  CheetahStr cond_str (veExprEvaluateValueInBinaryString( cond ));
  bool condition = (strchr(cond_str,'1') != NULL);
  
  if ( condition ) {
    veTruePath = veGenerateIfGetThenPart(veGenerate);
  } else {
    veTruePath = veGenerateIfGetElsePart(veGenerate);
  }
  
  if ( veTruePath != NULL ) {
    status = mCallback->ifGenerateTrue(InterraDesignWalkerCB::ePre, veTruePath, NULL, MVV_VERILOG);
    bool isSkip;
    CB_STATUS_SKIP_CHK(veGenerate, MVV_VERILOG, status, isSkip);
    
    if (! isSkip)
    {
      status = vlogGenerateItem( veTruePath, blk_cfg, NULL, genItemClosure );
      CB_STATUS_NESTED_CHK(veGenerate, MVV_VERILOG, status);
      
      status = mCallback->ifGenerateTrue(InterraDesignWalkerCB::ePost, veTruePath, NULL, MVV_VERILOG);
      CB_STATUS_RMSKIP_CHK(veGenerate, MVV_VERILOG, status);
      // skip detection not meaningful on post call
    }
  }
  
  status = mCallback->ifGenerate(InterraDesignWalkerCB::ePost, veGenerate, MVV_VERILOG);
  CB_STATUS_CHK(veGenerate, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogCaseGenerate(veNode veGenerateCase, 
				      mvvNode blk_cfg, 
				      VlogGenItemClosure* genItemClosure)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->caseGenerate(InterraDesignWalkerCB::ePre, veGenerateCase, MVV_VERILOG);
  CB_STATUS_CHK(veGenerateCase, MVV_VERILOG, status);

  veNode veTruePath = NULL;

  // veGenerateStatementGetTruePath does not select the proper branch
  // if the condition includes a genvar that has been assigned a
  // specific value.  Therefore we evaluate the condition (which must
  // be a constant) and then find the correct branch.  Note that there
  // is no such thing as a generate_casex so the identification of the
  // proper branch should be easy.
  veNode cond = veGenerateCaseGetExpr (veGenerateCase);
  // do branch identification as binary strings because the width can
  // be greater than 32 bits.
  CheetahStr cond_str (veExprEvaluateValueInBinaryString( cond ));
  
  // look through the branches of the case for something that matches
  // condition
  CheetahList  caseItemItr(veGenerateCaseGetItemList (veGenerateCase));
  
  // look through branches to identify the one activated by caseExpr
  for (veNode caseItem = veListGetNextNode (caseItemItr); caseItem; caseItem = veListGetNextNode (caseItemItr))
  {
    CheetahList generateCaseItemExprList;
    generateCaseItemExprList = veGenerateCaseItemGetExprList (caseItem);
    bool found_right_condition = false;
    
    if ( generateCaseItemExprList == NULL ){
      // default case
      found_right_condition = true;
    }
    else {
      // walk this list looking for a match to condition
      veNode expr;
      expr = veListGetNextNode (generateCaseItemExprList);
      for (; (expr != NULL); (expr = veListGetNextNode (generateCaseItemExprList))) {
        CheetahStr expr_str (veExprEvaluateValueInBinaryString( expr ));
        
        if (strcmp(expr_str, cond_str) == 0){
          found_right_condition = true;
          break;
        }
      }
    }
    
    if ( found_right_condition ) {
      veTruePath = veGenerateCaseItemGetStmt(caseItem);
      break;
    }
  }
  
  if ( veTruePath != NULL ) {
    status = mCallback->caseGenerateTrue(InterraDesignWalkerCB::ePre, veTruePath, MVV_VERILOG);
    bool isSkip;
    CB_STATUS_SKIP_CHK(veTruePath, MVV_VERILOG, status, isSkip);

    if (! isSkip)
    {
      status = vlogGenerateItem( veTruePath, blk_cfg, NULL, genItemClosure );
      CB_STATUS_NESTED_CHK(veTruePath, MVV_VERILOG, status);
      
      status = mCallback->caseGenerateTrue(InterraDesignWalkerCB::ePost, veTruePath, MVV_VERILOG);
      CB_STATUS_RMSKIP_CHK(veTruePath, MVV_VERILOG, status);
    }
  }
  
  status = mCallback->caseGenerate(InterraDesignWalkerCB::ePost, veGenerateCase, MVV_VERILOG);
  CB_STATUS_CHK(veGenerateCase, MVV_VERILOG, status);
  
  return status;
}


//! Return true if the given generate block should have a name, false if not.
/*!
 * Some veNodes for generates are for the generate conditionals (such as if and case)
 * that will have nested blocks underneath them.
 * It is those nested blocks which will have names.
 * So, this routine differentiates between those conditional generate constructs and
 * the begin/end generate blocks.
 */
bool isNameableGenerateBlock(veNode ve_node)
{
  CheetahList item_iter(veGenerateBlockGetItemList( ve_node ));

  // If the list is NULL, then it is an empty begin/end block, and it should get a name.
  if (item_iter == NULL) {
    return true;
  }

  // If the block contains more than one 1 item, then it should get a name
  if ( veListGetSize( item_iter ) > 1 ) {
    return true;
  }

  veNode ve_item = veListGetNextNode( item_iter );

  veObjType obj_type = veNodeGetObjType( ve_item );

  // If this generate block only contains a generate conditional and
  // it is implicit, it should not get a name.
  if (( obj_type == VE_GENERATE_IF || obj_type == VE_GENERATE_CASE ) &&
      ( veGenerateBlockGetIsImplicitlyUnnamed( ve_node ) == VE_TRUE )) {
    return false;
  } else {
    return true;
  }
}


InterraDesignWalkerCB::Status
InterraDesignWalker::vlogGenerateBlock(veNode ve_node, 
				       mvvNode blk_cfg,
				       const SInt32* genForIndex, 
				       VlogGenItemClosure* genItemClosure)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  // As long as this veNode is a nameable generate block, always increment
  // the block count, even if it has a name.  This is what the standard dictates.
  // A nameable generate block is a veNode which corresponds to a begin/end
  // block, and not, for instance, an if or case conditional (which has a nested
  // begin/end inside it).
  // Generate for loops just need this incremented once, as the iterations of the loop
  // are distinguished by creating an index with the name (such as genblk1[4]).  The
  // incrementing in the for loop case is handled by vlogForGenerate, so detect if we
  // are in a for loop and do not increment the counter in that case.
  bool is_nameable = isNameableGenerateBlock(ve_node);
  if (is_nameable && (genForIndex == NULL)) {
    genItemClosure->incrGenBlkCnt();
  }

  // Get the name, if there is one.
  UtString bname;
  mTicProtectedNameMgr->getVisibleName(ve_node, &bname);
  if ( bname.empty() ) {

    // An unnamed generate block should get a name, unless it contains
    // only a generate conditional in which case it doesn't get a name.
    if (is_nameable) {
      UInt32 genBlockCount = genItemClosure->getGenBlkCnt();
      
      // Make sure the name generated doesn't conflict with other names in this scope
      sFindUniqueVlogGenerateBlockName(&bname, ve_node, genBlockCount);
      
      // This callback adds this now named unnamed block to the scope, which in turn
      // adds it to the hierarchical name of whatever this block contains.
      mCallback->unnamedGenerateBlock(ve_node, bname.c_str(), MVV_VERILOG);
    }
  }

  // The LRM, in clause 12.1.3.1, states that genvars must not be
  // negative. So if genIndex is negative this means that we're coming
  // from a if or case generate, and need not consider the index at
  // all.
  // The information in the above comment is not used but is kept for
  // reference. We set it to NULL if it isn't a for-generate.
  if (genForIndex)
    bname << "[" << *genForIndex << "]";
  
  status = mCallback->generateBlock(InterraDesignWalkerCB::ePre,
				    ve_node, 
				    bname.c_str(), 
				    MVV_VERILOG, 
				    genForIndex);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  CheetahList vars(veBlockGetScopeVariableList(ve_node));
  status = processVlogScopeVarList(ve_node, vars);
  CB_STATUS_NESTED_CHK(ve_node, MVV_VERILOG, status);

  CheetahList nets(veBlockGetNetList(ve_node));
  status = processVlogGenerateBlockNetList(ve_node, nets);
  CB_STATUS_NESTED_CHK(ve_node, MVV_VERILOG, status);

  // We need to use a new VlogGenItemClosure when block nesting occurs,
  // so that the numbering will re-start.
  // Block nesting occurs when there is a begin/end block of a generate, which is nameable.
  VlogGenItemClosure newGenItemClosure;
  VlogGenItemClosure *child_closure = is_nameable ? &newGenItemClosure : genItemClosure;

  status = processVlogGenerateBlockItemList(ve_node, 
					    blk_cfg, 
					    genForIndex, 
					    child_closure); 
  CB_STATUS_NESTED_CHK(ve_node, MVV_VERILOG, status);

  status = mCallback->generateBlock(InterraDesignWalkerCB::ePost,
				    ve_node,
				    bname.c_str(),
				    MVV_VERILOG,
				    genForIndex);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogGatePrimitive(veNode ve_inst)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->gatePrimitive(InterraDesignWalkerCB::ePre, ve_inst, MVV_VERILOG);
  CB_STATUS_CHK(ve_inst, MVV_VERILOG, status);
  
  // nothing to do here.

  status = mCallback->gatePrimitive(InterraDesignWalkerCB::ePost, ve_inst, MVV_VERILOG);
  CB_STATUS_CHK(ve_inst, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::port(mvvNode node, mvvLanguageType lang)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->port(InterraDesignWalkerCB::ePre, node, lang);
  CB_STATUS_CHK(node, lang, status);

  // TBD

  status = mCallback->port(InterraDesignWalkerCB::ePost, node, lang);
  CB_STATUS_CHK(node, lang, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::net(mvvNode node, mvvLanguageType lang)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->net(InterraDesignWalkerCB::ePre, node, lang);
  CB_STATUS_CHK(node, lang, status);
  
  // TBD

  status = mCallback->net(InterraDesignWalkerCB::ePost, node, lang);
  CB_STATUS_CHK(node, lang, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::localNet(mvvNode node, mvvLanguageType lang)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->localNet(InterraDesignWalkerCB::ePre, node, lang);
  CB_STATUS_CHK(node, lang, status);
  
  // TBD

  status = mCallback->localNet(InterraDesignWalkerCB::ePost, node, lang);
  CB_STATUS_CHK(node, lang, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::alias(mvvNode node, mvvLanguageType lang)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->alias(InterraDesignWalkerCB::ePre, node, lang);
  CB_STATUS_CHK(node, lang, status);

  // TBD

  status = mCallback->alias(InterraDesignWalkerCB::ePost, node, lang);
  CB_STATUS_CHK(node, lang, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::file(mvvNode node, mvvLanguageType lang)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->file(InterraDesignWalkerCB::ePre, node, lang);
  CB_STATUS_CHK(node, lang, status);

  // TBD

  status = mCallback->file(InterraDesignWalkerCB::ePost, node, lang);
  CB_STATUS_CHK(node, lang, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogContAssign(veNode ve_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->contAssign(InterraDesignWalkerCB::ePre, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  // TBD

  status = mCallback->contAssign(InterraDesignWalkerCB::ePost, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogScopeVariable(veNode ve_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->scopeVariable(InterraDesignWalkerCB::ePre, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  // TBD

  status = mCallback->scopeVariable(InterraDesignWalkerCB::ePost, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogInitialBlock(veNode ve_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->initialBlock(InterraDesignWalkerCB::ePre, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  // TBD

  status = mCallback->initialBlock(InterraDesignWalkerCB::ePost, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogSpecifyBlock(veNode ve_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->specifyBlock(InterraDesignWalkerCB::ePre, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  // TBD

  status = mCallback->specifyBlock(InterraDesignWalkerCB::ePost, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogAlwaysBlock(veNode ve_always)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  status = mCallback->alwaysBlock(InterraDesignWalkerCB::ePre, ve_always, MVV_VERILOG);
  bool isSkip;
  CB_STATUS_SKIP_CHK(ve_always, MVV_VERILOG, status, isSkip);
  if (! isSkip) {
    veNode ve_stmt = veAlwaysGetStatement(ve_always);
    veObjType type = veNodeGetObjType(ve_stmt);
    veNode ve_block;
    if(type == VE_DELAY_OR_EVENT) {
      ve_block = veDelOrEventControlGetStatement(ve_stmt);
      type = veNodeGetObjType(ve_block);
    } else {
      ve_block = ve_stmt;
    }
    if((type == VE_SEQ_BLOCK) || (type == VE_PAR_BLOCK))
    {
      CheetahList nets(veBlockGetNetList(ve_block));
      status = processVlogLocalNetList(ve_block, nets);
      CB_STATUS_NESTED_CHK(ve_block, MVV_VERILOG, status);
    }
  }
  status = mCallback->alwaysBlock(InterraDesignWalkerCB::ePost, ve_always, MVV_VERILOG);
  CB_STATUS_CHK(ve_always, MVV_VERILOG, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::variable(mvvNode node, mvvLanguageType lang)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->variable(InterraDesignWalkerCB::ePre, node, lang);
  CB_STATUS_CHK(node, lang, status);
  
  // TBD
  
  status = mCallback->variable(InterraDesignWalkerCB::ePost, node, lang);
  CB_STATUS_CHK(node, lang, status);
  
  return status;
}


InterraDesignWalkerCB::Status
InterraDesignWalker::vlogTF(veNode ve_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  UtString name_buf;
  mTicProtectedNameMgr->getVisibleName(ve_node, &name_buf);
  status = mCallback->taskFunction(InterraDesignWalkerCB::ePre, ve_node, name_buf.c_str(), MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  // TBD
  
  status = mCallback->taskFunction(InterraDesignWalkerCB::ePost, ve_node, name_buf.c_str(), MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vlogDeclAssign(veNode ve_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->declaredAssign(InterraDesignWalkerCB::ePre, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  
  // TBD
  
  status = mCallback->declaredAssign(InterraDesignWalkerCB::ePost, ve_node, MVV_VERILOG);
  CB_STATUS_CHK(ve_node, MVV_VERILOG, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlConcSigAssign(vhNode vh_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->concSigAssign(InterraDesignWalkerCB::ePre, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  // TBD

  status = mCallback->concSigAssign(InterraDesignWalkerCB::ePost, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlConcProcess(vhNode vh_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->concProcess(InterraDesignWalkerCB::ePre, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  status = processVhdlDeclarations(vh_node);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);

  status = mCallback->concProcess(InterraDesignWalkerCB::ePost, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlConditionalSigAssign(vhNode vh_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->conditionalSigAssign(InterraDesignWalkerCB::ePre, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  // TBD
  
  status = mCallback->conditionalSigAssign(InterraDesignWalkerCB::ePost, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlSelSigAssign(vhNode vh_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->selSigAssign(InterraDesignWalkerCB::ePre, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  // TBD

  status = mCallback->selSigAssign(InterraDesignWalkerCB::ePost, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlConcProcCall(vhNode vh_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->concProcCall(InterraDesignWalkerCB::ePre, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  // TBD

  status = mCallback->concProcCall(InterraDesignWalkerCB::ePost, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlAssertStmt(vhNode vh_node)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->assertStmt(InterraDesignWalkerCB::ePre, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  // TBD

  status = mCallback->assertStmt(InterraDesignWalkerCB::ePost, vh_node, MVV_VHDL);
  CB_STATUS_CHK(vh_node, MVV_VHDL, status);
  
  return status;
}


InterraDesignWalkerCB::Status
InterraDesignWalker::processHdlComponentInstance(mvvNode component, 
                                                 mvvLanguageType callingLang, 
                                                 mvvLanguageType elabLang, 
                                                 mvvNode entityDecl, 
                                                 mvvNode oldEntity,
                                                 mvvNode newEntity,
                                                 const char* entityName,
                                                 mvvNode archDecl, 
                                                 mvvNode instance_config,
                                                 const SInt32* vectorIndex,
                                                 const UInt32* rangeOffset)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;

  UtString componentInstanceName;
  if (callingLang == MVV_VERILOG) {
    veNode ve_instance = component->castVeNode();
    UtString name_buf;
    mTicProtectedNameMgr->getVisibleName(ve_instance, &name_buf);
    componentInstanceName.assign(name_buf);
    // If a range index value is given, append "[index]" to the instance name
    if (vectorIndex)
      componentInstanceName << "[" << *vectorIndex << "]";
  }
  else if (callingLang == MVV_VHDL) {
    vhNode vh_instance = component->castVhNode();
    JaguarString jagName(vhGetName(vhGetLabelObj(vh_instance)));
    componentInstanceName.assign(jagName);
  }
  
  status = mCallback->hdlComponentInstance(InterraDesignWalkerCB::ePre, component, componentInstanceName.c_str(), entityDecl, oldEntity, newEntity, entityName, archDecl, vectorIndex, rangeOffset, elabLang, callingLang);
  CB_STATUS_CHK(component, callingLang, status);
  
  status = hdlEntity(entityDecl, archDecl, instance_config);
  CB_STATUS_NESTED_CHK(entityDecl, callingLang, status);
  
  status = mCallback->hdlComponentInstance(InterraDesignWalkerCB::ePost, component, componentInstanceName.c_str(), entityDecl, oldEntity, newEntity, entityName, archDecl, vectorIndex, rangeOffset, elabLang, callingLang);
  CB_STATUS_CHK(component, callingLang, status);

  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogPortList(veNode ve_module, CheetahList& ve_port_iter)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  if (ve_port_iter) {
    status = mCallback->portList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_port;
    while ((ve_port = veListGetNextNode(ve_port_iter))) {
      status = port(ve_port, MVV_VERILOG);
      CB_STATUS_NESTED_CHK(ve_port, MVV_VERILOG, status);
    }

    status = mCallback->portList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogLocalNetList(veNode ve_module, CheetahList& ve_net_iter)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  if (ve_net_iter) {
    status = mCallback->localNetList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_net;
    while ((ve_net = veListGetNextNode(ve_net_iter))) {
      if (veNodeGetObjType(ve_net) == VE_NETARRAY)
        status = variable(ve_net, MVV_VERILOG);
      else
        status = localNet(ve_net, MVV_VERILOG);
      CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
    }
    
    status = mCallback->localNetList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogLocalVarList(veNode ve_module, CheetahList& ve_var_iter)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  if (ve_var_iter) {
    status = mCallback->localVariableList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_var;
    while ((ve_var = veListGetNextNode(ve_var_iter)) != NULL) {
      // variables that are ports have already been processed, so we
      // only do the non ports here
      if (not veVariableGetIsPort(ve_var) ) { 
        status = variable(ve_var, MVV_VERILOG);
        CB_STATUS_NESTED_CHK(ve_var, MVV_VERILOG, status);
      }
    }

    status = mCallback->localVariableList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogScopeVarList(veNode ve_module, CheetahList& ve_hier_iter)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  if (ve_hier_iter) {
    status = mCallback->scopeVariableList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_hier;
    while ((ve_hier = veListGetNextNode(ve_hier_iter)) != NULL) {
      status = vlogScopeVariable(ve_hier);
      CB_STATUS_NESTED_CHK(ve_hier, MVV_VERILOG, status);
    }

    status = mCallback->scopeVariableList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogGenerateBlockNetList(veNode generate_block, CheetahList& ve_hier_iter)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  if (ve_hier_iter) {
    status = mCallback->generateBlockNetList(InterraDesignWalkerCB::ePre, generate_block, MVV_VERILOG);
    CB_STATUS_CHK(generate_block, MVV_VERILOG, status);

    veNode ve_net;
    while ((ve_net = veListGetNextNode(ve_hier_iter)) != NULL) {
      if (veNetGetIsImplicit(ve_net) == VE_FALSE)
	continue;
      status = net(ve_net, MVV_VERILOG);
      CB_STATUS_NESTED_CHK(ve_net, MVV_VERILOG, status);
      status = vlogDeclAssign(ve_net);
    }

    status = mCallback->generateBlockNetList(InterraDesignWalkerCB::ePost, generate_block, MVV_VERILOG);
    CB_STATUS_CHK(generate_block, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogTFList(veNode ve_module)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  CheetahList ve_tf_iter(veModuleGetTaskOrFuncList(ve_module));
  if (ve_tf_iter) {
    status = mCallback->taskFunctionList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_tf;
    while ((ve_tf = veListGetNextNode(ve_tf_iter)) != NULL) {
      status = vlogTF(ve_tf);
      CB_STATUS_NESTED_CHK(ve_tf, MVV_VERILOG, status);
    }

    status = mCallback->taskFunctionList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogInitialBlockList(veNode ve_module)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  CheetahList ve_init_iter(veModuleGetInitialBlockList(ve_module));
  if (ve_init_iter) {
    status = mCallback->initialBlockList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_init;
    while ((ve_init = veListGetNextNode(ve_init_iter)) != NULL) {
      status = vlogInitialBlock(ve_init);
      CB_STATUS_NESTED_CHK(ve_init, MVV_VERILOG, status);
    }

    status = mCallback->initialBlockList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogContAssignList(veNode ve_module)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  CheetahList ve_assign_iter(veModuleGetContAssignList(ve_module));
  if (ve_assign_iter) {
    status = mCallback->contAssignList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
    
    veNode ve_assign;
    while ((ve_assign = veListGetNextNode(ve_assign_iter))) {
      status = vlogContAssign(ve_assign);
      CB_STATUS_NESTED_CHK(ve_assign, MVV_VERILOG, status);
    }

    status = mCallback->contAssignList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogDeclAssignList(veNode ve_module)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eInvalid;
  status = mCallback->declaredAssignList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
  CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  
  status = mCallback->declaredAssignList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
  CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogAlwaysBlockList(veNode ve_module)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  
  CheetahList ve_always_iter(veModuleGetAlwaysBlockList(ve_module));
  if (ve_always_iter) {
    status = mCallback->alwaysBlockList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_always;
    while ((ve_always = veListGetNextNode(ve_always_iter))) {
      status = vlogAlwaysBlock(ve_always);
      CB_STATUS_NESTED_CHK(ve_always, MVV_VERILOG, status);
    }

    status = mCallback->alwaysBlockList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogSpecifyBlockList(veNode ve_module)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  CheetahList ve_specify_block_iter(veModuleGetSpecifyBlockList(ve_module));
  if ( ve_specify_block_iter ){
    status = mCallback->specifyBlockList(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);

    veNode ve_spec_block;
    while ((ve_spec_block = veListGetNextNode(ve_specify_block_iter))) 
    {
      status = vlogSpecifyBlock(ve_spec_block);
      CB_STATUS_NESTED_CHK(ve_spec_block, MVV_VERILOG, status);
    }

    status = mCallback->specifyBlockList(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }
  return status;
}


InterraDesignWalkerCB::Status
InterraDesignWalker::processVhdlUseClause(vhNode vh_entity)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;

  vhNode vh_use = vhGetUseClause( vh_entity );
  if (vh_use)
  {
    status = mCallback->useClause(InterraDesignWalkerCB::ePre, vh_use, MVV_VHDL);
    CB_STATUS_CHK(vh_entity, MVV_VHDL, status);

    JaguarList vh_use_iter(vhGetSelNameList( vh_use ));
    if (vh_use_iter)
    {
      vhNode useitem;
      while( (useitem = vhGetNextItem( vh_use_iter )))
      {
        status = vhdlPackage(useitem);
        CB_STATUS_NESTED_CHK(useitem, MVV_VHDL, status);
      }
    }
    status = mCallback->useClause(InterraDesignWalkerCB::ePost, vh_use, MVV_VHDL);
    CB_STATUS_CHK(vh_entity, MVV_VHDL, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVhdlPortClause(vhNode vh_entity)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  vhNode portClause = vhGetPortClause(vh_entity);
  
  if (portClause)
  {
    status = mCallback->portList(InterraDesignWalkerCB::ePre, portClause, MVV_VHDL);
    CB_STATUS_CHK(vh_entity, MVV_VHDL, status);

    JaguarList vh_port_iter(vhGetFlatSignalList(portClause));
    if (vh_port_iter) 
    {
      vhNode vh_port;
      while ((vh_port = vhGetNextItem(vh_port_iter))) 
      {
        status = port(vh_port, MVV_VHDL);
        CB_STATUS_NESTED_CHK(vh_port, MVV_VHDL, status);
      }
    }

    status = mCallback->portList(InterraDesignWalkerCB::ePost, portClause, MVV_VHDL);
    CB_STATUS_CHK(vh_entity, MVV_VHDL, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVhdlGenericClause(vhNode vh_entity)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  vhNode vh_generic_clause = vhGetGenericClause(vh_entity);
  if (NULL != vh_generic_clause) {
    status = mCallback->genericClause(InterraDesignWalkerCB::ePre, vh_generic_clause, MVV_VHDL);
    CB_STATUS_CHK(vh_entity, MVV_VHDL, status);
    
    vhNode vh_generic = 0;
    JaguarList vh_generic_list(vhGetFlatGenericList(vh_generic_clause));
    while((vh_generic = vhGetNextItem(vh_generic_list)))  {
      status = net(vh_generic, MVV_VHDL);
      CB_STATUS_NESTED_CHK(vh_generic, MVV_VHDL, status);
    }

    status = mCallback->genericClause(InterraDesignWalkerCB::ePost, vh_generic_clause, MVV_VHDL);
    CB_STATUS_CHK(vh_entity, MVV_VHDL, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVhdlDeclarations(vhNode n_stm)
{
  InterraDesignWalkerCB::Status status = mCallback->declarations(InterraDesignWalkerCB::ePre, n_stm, MVV_VHDL);
  CB_STATUS_CHK(n_stm, MVV_VHDL, status);

  JaguarList vh_decl_iter(vhGetDeclItemList(n_stm));
  if (vh_decl_iter)
  {
    vhNode vh_decl = 0;
    while((vh_decl = vhGetNextItem(vh_decl_iter))) {
      InterraDesignWalkerCB::Status loop_status =
        mCallback->declaration(InterraDesignWalkerCB::ePre, vh_decl, MVV_VHDL);
      CB_STATUS_LOOP_CHK(vh_decl, MVV_VHDL, loop_status);

      vhObjType decl_type = vhGetObjType(vh_decl);
      switch (decl_type)
      {
      case VHVARIABLE: 
        status = variable(vh_decl, MVV_VHDL);
        break;
      case VHCONSTANT:
      case VHSIGNAL:
        status = localNet(vh_decl, MVV_VHDL);
        break;
      case VHSUBPROGBODY:
        break; // We will process this subprogram during the subprogram call
      case VHCONFIGSPEC: 
        break; // vhOpenInstance() should take care this stmt
      case VHALIASDECL:
        status = alias(vh_decl, MVV_VHDL);
        break;
      case VHFILEDECL:
        status = file(vh_decl, MVV_VHDL);
        break;
      case VHSUBPROGDECL:
      case VHSUBTYPEDECL:
      case VHTYPEDECL:
      case VHCOMPONENT:
      case VHATTRBDECL:
      case VHATTRIBUTESPEC:
      case VHLIBRARYCLAUSE:
      case VHUSECLAUSE:
        break; // These would be processed during expression population
      default:
        {
          status = mCallback->unsupportedDeclaration(vh_decl, MVV_VHDL);
          CB_STATUS_RMSKIP_CHK(vh_decl, MVV_VHDL, status);
        }
        break;
      }
      
      CB_STATUS_NESTED_CHK(vh_decl, MVV_VHDL, status);

      loop_status =
        mCallback->declaration(InterraDesignWalkerCB::ePost, vh_decl, MVV_VHDL);
      CB_STATUS_LOOP_CHK(vh_decl, MVV_VHDL, loop_status);
    }
  }

  status = mCallback->declarations(InterraDesignWalkerCB::ePost, n_stm, MVV_VHDL);
  CB_STATUS_CHK(n_stm, MVV_VHDL, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVhdlBlockGuardSignal(vhNode vh_block)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  vhNode guard_signal = vhLookUpInSymTab(vh_block,"GUARD");
  if (! guard_signal)
  {
    status = mCallback->implicitGuardNullInGuardedBlock(vh_block, MVV_VHDL);
    CB_STATUS_RMSKIP_CHK(vh_block, MVV_VHDL, status);
    return InterraDesignWalkerCB::eNormal;
  }

  status = localNet(guard_signal, MVV_VHDL);
  CB_STATUS_CHK(vh_block, MVV_VHDL, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogUdpDeclarations(veNode ve_udp)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  CheetahList ve_net_iter(veUDPGetNetList(ve_udp));
  if (ve_net_iter)
  {
    status = mCallback->declarations(InterraDesignWalkerCB::ePre, ve_udp, MVV_VERILOG);
    CB_STATUS_CHK(ve_udp, MVV_VERILOG, status);
    
    status = processVlogLocalNetList(ve_udp, ve_net_iter);
    CB_STATUS_NESTED_CHK(ve_udp, MVV_VERILOG, status);
    
    status = mCallback->declarations(InterraDesignWalkerCB::ePost, ve_udp, MVV_VERILOG);
    CB_STATUS_CHK(ve_udp, MVV_VERILOG, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::processVlogModuleDeclarations(veNode ve_module)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  
  bool hasDeclaration = false;
  {
    CheetahList ve_net_iter(veModuleGetNetList(ve_module));
    if (ve_net_iter)
    {
      status = mCallback->declarations(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
      CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
      hasDeclaration = true;
    }
    status = processVlogLocalNetList(ve_module, ve_net_iter);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
  }
  
  {
    CheetahList ve_var_iter(veModuleGetVariableList(ve_module));
    if (! hasDeclaration && ve_var_iter)
    {
      status = mCallback->declarations(InterraDesignWalkerCB::ePre, ve_module, MVV_VERILOG);
      CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
      hasDeclaration = true;
    }
    
    status = processVlogLocalVarList(ve_module, ve_var_iter);
    CB_STATUS_NESTED_CHK(ve_module, MVV_VERILOG, status);
  }
  
  if (hasDeclaration)
  {
    status = mCallback->declarations(InterraDesignWalkerCB::ePost, ve_module, MVV_VERILOG);
    CB_STATUS_CHK(ve_module, MVV_VERILOG, status);
  }

  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlPackage(vhNode vh_useitem)
{
  InterraDesignWalkerCB::Status status = mCallback->package(InterraDesignWalkerCB::ePre, vh_useitem, MVV_VHDL);
  CB_STATUS_CHK(vh_useitem, MVV_VHDL, status);

  CB_ISSUE_ASSERT(vhGetObjType(vh_useitem) == VHSELECTEDNAME, vh_useitem, MVV_VHDL,
                  "Unexpected context clause object type");
  // Walk the parts of this package name
  JaguarList vh_selname_iter(vhGetExprList( static_cast<vhExpr>( vh_useitem )));
  vhNode vh_namepart = vhGetNextItem( vh_selname_iter );
  CB_ISSUE_ASSERT(vhGetObjType(vh_namepart) == VHSIMPLENAME, vh_namepart, MVV_VHDL,
                  "Unexpected selected name object in context clause");
    
  // The first name is the library. Ignore standard libs.
  JaguarString libName(vhGetSimpleName( static_cast<vhExpr>( vh_namepart )));
  // If the VHDL specifies "work" as the library, we need to know what
  // the actual logical library name for it is.
  UtString logicalLib;
  if (strcasecmp(libName, "WORK") == 0) {
    logicalLib = mLogicalWorkLib;
  } else {
    logicalLib = libName;
  }

  // The next name part is the package name
  vh_namepart = vhGetNextItem(vh_selname_iter);
  vhObjType objtype = vhGetObjType(vh_namepart);
  CB_ISSUE_ASSERT(objtype == VHSIMPLENAME, vh_namepart, MVV_VHDL,
                  "Unexpected selected name object in context clause");
  // The construct like "use libName.all" is not supported and ignored.
  if (objtype != VHALL)
  {
    JaguarString packName(vhGetSimpleName(static_cast<vhExpr>(vh_namepart)));
    vhdlPackageDecl(logicalLib.c_str(), packName);
  }

  status = mCallback->package(InterraDesignWalkerCB::ePost, vh_useitem, MVV_VHDL);
  CB_STATUS_CHK(vh_useitem, MVV_VHDL, status);
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlPackageDecl(const char* logical_lib, const char* pack_name)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  // The package declaration could be NULL if there is no package with given name
  // in the library. The name could correspond to an entity, in which case nothing
  // needs to be done here. If there's no declaration corresponding to the name in
  // the library, Jaguar will issue an error. Testcase: test/vhdl/lang_misc/bug7030.vhdl.
  vhNode vh_pack_decl = vhOpenPackDecl(logical_lib, pack_name);
  if (vh_pack_decl != NULL)
  {
    bool is_std_lib = false;
    if ( !strcasecmp( logical_lib, "STD" ) ||
         !strcasecmp( logical_lib, "IEEE" ) ||
         !strcasecmp( logical_lib, "SYNOPSYS" )) {
      is_std_lib = true;
    }
    
    status = mCallback->packageDecl(InterraDesignWalkerCB::ePre, vh_pack_decl,
                                    MVV_VHDL, logical_lib, pack_name, is_std_lib);
    CB_STATUS_CHK(vh_pack_decl, MVV_VHDL, status);

    processVhdlDeclarations(vh_pack_decl);
    
    vhNode vh_pack_body = vhOpenPackBody(logical_lib, pack_name);
    if (vh_pack_body != NULL) {
      vhdlPackageBody(logical_lib, pack_name);
    }
    
    status = mCallback->packageDecl(InterraDesignWalkerCB::ePost, vh_pack_decl,
                                    MVV_VHDL, logical_lib, pack_name, is_std_lib);
    CB_STATUS_CHK(vh_pack_decl, MVV_VHDL, status);
  }
  return status;
}

InterraDesignWalkerCB::Status
InterraDesignWalker::vhdlPackageBody(const char* logical_lib, const char* pack_name)
{
  InterraDesignWalkerCB::Status status = InterraDesignWalkerCB::eNormal;
  // The package body may not exist even though a package declaration exists.
  vhNode vh_pack_body = vhOpenPackBody(logical_lib, pack_name);
  if (vh_pack_body != NULL)
  {
    status = mCallback->packageBody(InterraDesignWalkerCB::ePre, vh_pack_body, MVV_VHDL);
    CB_STATUS_CHK(vh_pack_body, MVV_VHDL, status);
    
    processVhdlDeclarations(vh_pack_body);

    status = mCallback->packageBody(InterraDesignWalkerCB::ePost, vh_pack_body, MVV_VHDL);
    CB_STATUS_CHK(vh_pack_body, MVV_VHDL, status);
  }
  return status;
}

void InterraDesignWalker::addLocationAndType(mvvNode node, mvvLanguageType langType, UtString* str)
{
  mCallback->addLocationAndType(node, langType, str);
}
