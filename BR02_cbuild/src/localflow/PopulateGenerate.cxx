// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/VerilogPopulate.h"
#include "LFContext.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"

VerilogPopulate::ErrorCode
VerilogPopulate::generateBlock( veNode genBlock,
				const SInt32* /*genIndex*/,
				const char* blockName,
				LFContext *context)
{
  ErrorCode err_code = eSuccess;

  if (strlen(blockName) == 0)
    return err_code;

  // VE_GENERATE_BLOCKS can contain hierarchical references
  
  SourceLocator loc = locator(genBlock);
  StringAtom *name = context->getStringCache()->intern(blockName);
  
  NUNamedDeclarationScope* declaration_scope = new NUNamedDeclarationScope(name,
                                                                           context->getDeclarationScope(),
                                                                           context->getIODB(),
                                                                           context->getNetRefFactory(),
                                                                           loc);
  context->pushDeclarationScope(declaration_scope);
  
  return err_code;
}

VerilogPopulate::ErrorCode
VerilogPopulate::nestedGenerateInstance( veNode ve_item )
{
  SourceLocator loc = locator( ve_item );
  LOC_ASSERT(0, loc);    // cheetah prohibits us from getting here
  mMsgContext->NotHandledInGenerate( &loc, "A nested generate instantiation" );
  return eFatal;
}

VerilogPopulate::ErrorCode
VerilogPopulate::generateElabFail( veNode ve_item )
{
  SourceLocator loc = locator( ve_item );
  mMsgContext->LFGenerateUnsupported(&loc);
  return eFatal;
}

void VerilogPopulate::unnamedGenerateBlock(veNode ve_node, const char* blockName)
{
  if (strlen(blockName) != 0)
  {
    SourceLocator loc = locator( ve_node );
    mMsgContext->UnNamedGenBlock(&loc, blockName);
  }
}
