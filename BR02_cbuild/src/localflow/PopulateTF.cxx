// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "LFContext.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUTF.h"
#include "util/CbuildMsgContext.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"

//! Convert IN, OUT or INOUT into appropriate mode for Verilog parameter
static NUTF::CallByMode sGetParamMode (const NUNet* net)
{
  if (net->isInput ())
    return NUTF::eCallByValue;
  else if (net->isOutput ())
    return NUTF::eCallByCopyOut;
  else if (net->isBid ())
    return NUTF::eCallByCopyInOut;
  else
    NU_ASSERT (0, net);

  return NUTF::eCallByAny;
}

Populate::ErrorCode VerilogPopulate::tfCommon(NUTF* tf,
                                              veNode ve_tf,
                                              LFContext* context)
{
  ErrorCode err_code = eSuccess;

  context->pushBlockScope(tf);
  context->pushDeclarationScope(tf);

  CheetahList ve_arg_iter(veTfGetArgList(ve_tf)); // arguments to the TF
  if (ve_arg_iter) {
    veNode ve_arg;
    while ((ve_arg = VeListGetNextNode(ve_arg_iter))) {
      NUNet *net = 0;
      ErrorCode temp_err_code = tfArg(ve_arg, context, &net);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      if (net) {
	tf->addArg(net, sGetParamMode (net));
      }
    }
  }

  CheetahList ve_var_iter(veTfGetVarList(ve_tf)); // variables defined local to TF
  if (ve_var_iter) {
    veNode ve_var;
    while ((ve_var = veListGetNextNode(ve_var_iter))) {
      NUNet *net = 0;
      ErrorCode temp_err_code = tfLocal(ve_var, context, &net);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      // May be 0, for instance if we are re-declaring a port
      if (net) {
	tf->addLocal(net);
      }
    }
  }

  // Make a block to hold all the statement of the task/function,
  // so that if it gets disabled from inside, the break-processing
  // can find an NUBlock target via mBlockMap.  The NUBlock will
  // be deleted during BreakResynth.
  SourceLocator loc = locator(ve_tf);
  NUBlock* block = new NUBlock(NULL,
                               context->getBlockScope(),
                               mIODB,
                               context->getNetRefFactory(),
                               false,
                               loc);
  mBlockMap[ve_tf] = block;
  context->pushBlockScope(block);

  NUStmtList stmt_list;
  context->pushStmtList(&stmt_list);
  veNode ve_stmt = veTfGetStmt(ve_tf);
  ErrorCode temp_err_code = stmt(ve_stmt, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  context->popStmtList();

  block->addStmts(&stmt_list);
  context->popBlockScope();     // for NUBlock

  stmt_list.clear();
  stmt_list.push_back(block);
  tf->addStmts(&stmt_list);

  context->popBlockScope();     // for TF
  context->popDeclarationScope();

  return err_code;
}


Populate::ErrorCode VerilogPopulate::tf(veNode ve_tf,
                                        const char* tfName,
                                        LFContext *context,
                                        NUTask **the_tf)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_tf);

  *the_tf = 0;

  // Create the hierarchical references.
  CheetahList vars(veTfGetScopeVariableList(ve_tf));
  ErrorCode hier_err_code = scopeVariableList(vars, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(hier_err_code, &err_code ) ) {
    return hier_err_code;
  }

  // Detect recursion.  If the switches say that we don't want to
  // allow recursive tasks then later processing by MarkSweepUnusedTasks 
  // will do the elaborated analysis that detects self-recursion and mututal
  // recursion through hier-refs.
  if (context->getTFVisit(ve_tf)) {
    return eSuccess;
  }
  else {
    context->addTFVisit(ve_tf);
  }

  ErrorCode temp_err_code;

  switch (veNodeGetObjType(ve_tf)) {
  case VE_TASK:
    {
      temp_err_code = lookupTask(context->getModule(), ve_tf, the_tf);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      if (*the_tf == 0) {
	temp_err_code = task(ve_tf, tfName, context, the_tf);
        if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
      }
    }
    break;
  case VE_FUNCTION:
    {
      temp_err_code = lookupTask(context->getModule(), ve_tf, the_tf);

      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }

      if (*the_tf == 0) {
        temp_err_code = function(ve_tf, context, the_tf);
        if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
          return temp_err_code;
        }
      }
    }
    break;
  default:
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown task/function type"), &err_code);
    return err_code;
    break;
  }

  context->removeTFVisit(ve_tf);

  return err_code;
}


// this converts \a ve_function into a task
Populate::ErrorCode VerilogPopulate::function(veNode ve_function,
                                              LFContext *context,
                                              NUTask **the_task_for_function)
{
  SourceLocator loc = locator(ve_function);
  ErrorCode err_code = eSuccess;

  UtString name_buf;
  mTicProtectedNameMgr->getVisibleName(ve_function, &name_buf);


  NUModule *module = context->getModule();
  // at one time we would create a different name for the new task
  // ("$task_for_<fctname>") but now we just use the same name to
  // simplify the processing of hierarchical references.
  // (verilog does not allow a function and task to have the same name
  // so there will be no conflict)
  StringAtom *name = context->getStringCache()->intern(name_buf.c_str());

  StringAtom *origName = context->getStringCache()->intern(name_buf.c_str());
  *the_task_for_function = new NUTask(module, name, origName, context->getNetRefFactory(), loc, mIODB);
  (*the_task_for_function)->putRequestInline(context->doInlineTasks());

  // Avoid infinite recursion crash by mapping it before populating it
  module->addTask(*the_task_for_function);
  ErrorCode temp_err_code = mapTask(module, *the_task_for_function,
                                    ve_function);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Create the function result as an output in the new task.
  context->pushBlockScope(*the_task_for_function);
  context->pushDeclarationScope(*the_task_for_function);
  NUNet *function_result = NULL;   // this is the return value from the function, and will
                                // be converted to the first argument (an output)
  temp_err_code = functionResult(ve_function, context, &function_result);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  context->popBlockScope();
  context->popDeclarationScope();


  if ( function_result ){
    // turn net into an output, and add it to argument list (as first arg)
    function_result->putIsOutput(true);
    NU_ASSERT(0 == (*the_task_for_function)->getNumArgs(), *the_task_for_function);
    (*the_task_for_function)->addArg(function_result, NUTF::eCallByCopyOut);
  }

  temp_err_code = tfCommon(*the_task_for_function, ve_function, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::task(veNode ve_task,
                                          const char* tfName,
                                          LFContext *context,
                                          NUTask **the_task)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_task);

  StringAtom *name = context->getStringCache()->intern(tfName);
  StringAtom *origName = context->getStringCache()->intern(tfName);
  *the_task = new NUTask(context->getModule(), name, origName, context->getNetRefFactory(), loc, mIODB);
  (*the_task)->putRequestInline(context->doInlineTasks());

  // Put the task into the taskmap before processing the task body,
  // so that recursive tasks can be populated
  context->getModule()->addTask(*the_task);
  ErrorCode temp_err_code = mapTask(context->getModule(), *the_task, ve_task);

  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  temp_err_code = tfCommon(*the_task, ve_task, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::functionResult(veNode ve_function,
                                                    LFContext *context,
                                                    NUNet **the_net)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_function);

  *the_net = 0;

  UtString name_buf;
  mTicProtectedNameMgr->getVisibleName(ve_function, &name_buf);
  StringAtom *name = context->getStringCache()->intern(name_buf.c_str());

  // This net should be added to the task.
  NUScope * parent_scope = context->getDeclarationScope();
  LOC_ASSERT(parent_scope->getScopeType()==NUScope::eTask, loc);
  NetFlags flags = NetFlags(eTempNet|eAllocatedNet|eBlockLocalNet|eNonStaticNet);

  switch (veFunctionGetReturnType(ve_function)) {

  case SIGN_USE:
    flags = NetRedeclare(flags, eSigned);
    // fall through

  case REG_USE: // with mvv 2004.1.12.g functions without a declared
                // type/size return REG_USE as per LRM 10.3.1 so
                // handle same way as UNSET_USE
  case UNSET_USE:
    {
      veNode ve_range = veFunctionGetRange(ve_function);
      if (ve_range) {
	ConstantRange *range = 0;
	ErrorCode temp_err_code = constantRange(ve_range, &range);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
	*the_net = new NUVectorNet(name, range, flags, VectorNetFlags (0), parent_scope, loc);
      } else {
	*the_net = new NUBitNet(name, flags, parent_scope, loc);
      }
    }
    break;

  case INTEGER_USE:
    {
      ConstantRange *range = new ConstantRange(31, 0);
      flags = NetRedeclare(flags, eDMIntegerNet|eSigned);
      *the_net = new NUVectorNet(name, range, flags, VectorNetFlags (0), parent_scope, loc);
    }
    break;

  case REAL_USE:
  case REALTIME_USE:
  {
    ConstantRange *range = new ConstantRange(63, 0);
    flags = NetRedeclare(flags, eDMRealNet|eSigned);
    *the_net = new NUVectorNet(name, range, flags, VectorNetFlags (0), parent_scope, loc);
  }
    break;

  default:
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown function result type"), &err_code);
    return err_code;
    break;
  }
  ErrorCode temp_err_code = mPopulate->mapNet(context->getDeclarationScope(), ve_function, *the_net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  return err_code;
}

//! puts the width of the result of \a ve_function into \a the_width
Populate::ErrorCode VerilogPopulate::functionResultWidth(veNode ve_function,
                                                         UInt32* the_width)
{
  ErrorCode err_code = eSuccess;

  *the_width = 0;

  switch (veFunctionGetReturnType(ve_function)) {
  case SIGN_USE:
  case REG_USE: // with mvv 2004.1.12.g functions without a declared
                // type/size return REG_USE as per LRM 10.3.1 so
                // handle same way as UNSET_USE
  case UNSET_USE:
  {
    veNode ve_range = veFunctionGetRange(ve_function);
    if (ve_range) {
      // we create a temp ConstantRange so that all the necessary
      // checking will be done
      ConstantRange *range = 0;
      ErrorCode temp_err_code = constantRange(ve_range, &range);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }

      *the_width = range->getLength();
      delete range;
    } else {
      *the_width = 1;
    }
      
    break;
  }

  case INTEGER_USE:
  {
    *the_width = 32;
    break;
  }

  case REAL_USE:
  case REALTIME_USE:
  {
    *the_width = 64;
    break;
  }

  default: {
    SourceLocator loc = locator(ve_function);
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown function result type"), &err_code);
    return err_code;
    break;
  }
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::tfArg(veNode ve_arg,
                                           LFContext *context,
                                           NUNet **the_net)
{
  return var(veTfArgGetVariable(ve_arg), context, the_net);
}


Populate::ErrorCode VerilogPopulate::tfLocal(veNode ve_var,
                                             LFContext *context,
                                             NUNet **the_net)
{
  *the_net = 0;
  if (veVariableGetParentTfArg(ve_var)) {
    return eSuccess;
  } else {
    return var(ve_var, context, the_net);
  }
}
