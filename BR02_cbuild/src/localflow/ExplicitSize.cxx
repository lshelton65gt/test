// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/ExplicitSize.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUExprReplace.h"

ExplicitSize::ExplicitSize() {
}

ExplicitSize::~ExplicitSize() {
}

class ExplicitSizeMutator : public NUExprReplace {
public:
public:
  NUExpr* processExpr(NUExpr *e) {
    NUExpr* ret = NULL;
    NUExpr* rep = e->makeSizeExplicit();
    if (rep != e) {
      ret = rep;
    }
    return ret;
  }
};

// Find every expression everywhere, and replace its sizing
void ExplicitSize::mutateDesign(NUDesign* design) {
  ExplicitSizeMutator mutator;
  NUModuleList mods;
  design->getModulesBottomUp(&mods);
  for (NUModuleList::iterator p = mods.begin(), e = mods.end(); p != e; ++p) {
    NUModule* mod = *p;
    mod->replaceLeaves(mutator);
  }
}
