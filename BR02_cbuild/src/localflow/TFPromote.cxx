// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "compiler_driver/CarbonContext.h" // for CRYPT
#include "iodb/IODBNucleus.h"
#include "localflow/TFPromote.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUTFWalker.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "reduce/Flatten.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"
#include "util/UtString.h"

/*
 Move troublesome tasks to the root module.  'Troublesome' is
 defined as: a task that is hierarchically referenced, that itself
 makes hierarchical reference to other tasks.  

 Such tasks are troublesome for two reasons.  One reason is that
 it's possible for dastardly customers to create situations where
 a module contains a hierarchical task reference that resolves two
 different task-bodies in different instantiations of the module.
 That makes flow analysis through the task very difficult.  But
 I think customers probably rarely do that.  See
 test/hierref_unique/test3.v

 Another troublesome task is one that is hierarchically referenced,
 and makes non-blocking assigns.  We have to inline tasks that
 make non-blocking assigns, but it is difficult to inline hierarchically
 referenced tasks.

 A more basic issue is that we can't do accurate analysis of what
 variables the task reads & writes, which makes it hard for us to
 schedule the blocks containing the task-enables in the right order,
 among other issues.

 Note that when we pull a task 't1' from 'child' to 'root', if that
 task called local task 't2' in 'child', then 't2' must also be moved
 to the root.  This may cascade until all the tasks in 'child' have
 been moved to the root.

 We must also pull blocks that make the hierarchical references
 to troublesome tasks into the root, changing module-scoped variables
 referenced in that block into hierarchical references.

 Also note that any module-scoped variables referenced by tasks that
 are moved must be turned into hierarchical references.  Any hierarchical
 references to nets made from the task need to be absolutified.

 It may be helpful, in terms of optimization, to move nets declared
 in 'child' that are now only accessed by 'root' into 'root' so they
 are not hierarchical any longer.
*/

class HierTaskCall;

#define UNINITIALIZED_JUMP_COUNT (-1)
#define RECURSING_JUMP_COUNT (0)

// Find the root scope given a node.  It might be more efficient to
// just keep the root node in class TFPromote, but I am anticipating
// that some day we may support multiple roots, in which case we'd
// want to always promote up to the current code's root.
static STBranchNode* sFindRoot(STBranchNode* node) {
  for (STBranchNode* p = node; p != NULL; p = node->getParent()) {
    node = p;
  }
  return node;
}

//! Elaborated block (task-body, initial, or always).
class TFPromote::HierBlock {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  HierBlock(NUBase* block, STBranchNode* scope):
    mBlock(block),
    mOrigBlock(block),
    mScope(scope),
    mOrigScope(scope)
  {
    mJumpCount = UNINITIALIZED_JUMP_COUNT;
    mPromote = false;
  }

  //! dtor
  ~HierBlock();

  //! hash
  size_t hash() const {
    return ((size_t) mOrigBlock) ^ ((size_t) mOrigScope);
  }

  //! compare
  bool operator==(const HierBlock& other) const {
    return ((mOrigBlock == other.mOrigBlock) && (mScope == other.mOrigScope));
  }

  //! If this block is a task body, return it, otherwise 0
  NUTask* getTask() {
    NUTask* task = dynamic_cast<NUTask*>(mBlock);
    return task;
  }

  //! If this block is an always block, return it, otherwise 0
  NUStructuredProc* getStructuredProc() {
    NUStructuredProc* ab = dynamic_cast<NUStructuredProc*>(mBlock);
    return ab;
  }
    

  //! debug print function
  void print(UInt32 indent) {
    for (UInt32 i = 0; i < indent; ++i) {
      UtIO::cout() << " ";
    }
    UtString pathbuf, locbuf;
    mOrigScope->compose(&pathbuf);
    NUTask* task = getTask();
    if (task != NULL) {
      task->getLoc().compose(&locbuf);
      UtIO::cout() << "task " << task->getName()->str();
    }
    else {
      NUStructuredProc* ab = getStructuredProc();
      NU_ASSERT(ab, mBlock);
      ab->getLoc().compose(&locbuf);
      UtIO::cout() << "always";
    }
    UtIO::cout() << ", Path=" << pathbuf << ", " << locbuf;
    if (isPromoted()) {
      UtIO::cout() << " *PROMOTE*";
    }
    UtIO::cout() << UtIO::endl;
  }


  //! Recursively compute thie number of jumps in the call chain.
  /*! The result is memo-ized in mJumpCount, and if we want to
   *! recompute this we must first clear that member variable
   */
  SInt32 calcJumpCount();

  //! Clear the jump count and the promote flag
  void clearJumpCount() {
    mJumpCount = UNINITIALIZED_JUMP_COUNT;
    mPromote = false;
  }

  //! When walking the call graph to calculate jump count
  //! to promote, use this flag to avoid re-traversing pieces of
  //! the DAG that were already hit.  Note that once we declare
  //! a task promoted, it's mScope will permanently be set to
  //! the scope's root.  But the mPromote flag may get cleared and
  //! set multiple times as we settle out all the required promotions.
  bool isDeclaredPromoted() const {return mPromote;}

  //! Has this this block ever been promoted?
  bool isPromoted() {return mScope != mOrigScope;}

  //! Can this block be further promoted?
  bool isPromotable() {return mScope != sFindRoot(mScope);}

  //! Declare this block as promoted
  void declarePromoted() {
    NU_ASSERT(!mPromote, mBlock);
    mPromote = true;
    mScope = sFindRoot(mScope);
  }

  //! Execute the promotion, mutating nucleus as required
  void performPromotion();

  //! Record the new (promoted) version of the block nucleus
  void putReplacement(NUBase* block) {
    mBlock = block;
  }

  //! Has this block had its nucleus replaced yet?
  bool isReplaced() const {
    return mOrigBlock != mBlock;
  }

  //! Record that this block has a task-enable in it
  void rememberTaskEnable(NUTaskEnable*);

  //! Walk this block using a Design Walker
  void walk(NUDesignWalker* walker) {
    walker->putWalkTasksFromTaskEnables(false);
    NUStructuredProc* structuredProc = getStructuredProc();
    if (structuredProc != NULL) {
      walker->structuredProc(structuredProc);
    }
    else {
      NUTask* task = getTask();
      NU_ASSERT(task, mBlock);
      walker->task(task);
    }
  }

  NUBase* mBlock;                             //! the current nucleus
  NUBase* mOrigBlock;                         //! the original nucleus
  STBranchNode* mScope;                       //! the current scope
  STBranchNode* mOrigScope;                   //! the original scope
  UtVector<TFPromote::HierTaskCall*> mCalls;  //! all calls from this block
  SInt32 mJumpCount;                          //! memo-ized jump count
  bool mPromote;                              //! temp block promotion flag
};

//! Elaborated NUTaskEnable
class TFPromote::HierTaskCall {
public:
  CARBONMEM_OVERRIDES

  //! ctor
  HierTaskCall(HierBlock* hblock, NUTaskEnable* taskEnable):
    mHBlock(hblock),
    mTaskEnable(taskEnable),
    mDest(NULL)
  {
    mPromote = false;
    mJumpCount = UNINITIALIZED_JUMP_COUNT;
  }

  //! dtor
  ~HierTaskCall() {
  }

  //! debug print function
  void print(UInt32 indent) {
    for (UInt32 i = 0; i < indent; ++i) {
      UtIO::cout() << " ";
    }
    UtString locbuf;
    mTaskEnable->getLoc().compose(&locbuf);
    UtIO::cout() << "TaskEnable @" << locbuf << ": ";
    if (mDest != NULL) {
      mDest->print(0);
    }
  }

  //! recursive walk to determine maximum number of jumps in call-chain
  SInt32 calcJumpCount() {
    if (mJumpCount == UNINITIALIZED_JUMP_COUNT) {
      mJumpCount = RECURSING_JUMP_COUNT;
      mJumpCount = mDest->calcJumpCount();

      NU_ASSERT(mDest, mTaskEnable); // should be resolved
      if (mDest->mScope != mHBlock->mScope) {
        // This is a hierarchical path
        ++mJumpCount;
      }
    }
    return mJumpCount;
  }

  //! declare that this task-enable is promoted
  void declarePromoted() {
    mPromote = true;
  }

  //! has this task-enable been declared as promoted?
  bool isDeclaredPromoted() const {return mPromote;}

  //! clear out the jump-count in prepration for another call-tree walk
  void clearJumpCount() {
    mJumpCount = UNINITIALIZED_JUMP_COUNT;
    mPromote = false;
  }

  //! do we need to replace this task-enable?
  bool isReplaced() {
    bool ret = mDest->isReplaced();
    if (!ret) {
      // See test/hierref_unique/test1.v.  If the user codes a
      // local reference using a hierarchical path, we need
      // to turn it into a local path, even if the blocks are not
      // being rewritten
      if ((mHBlock->mScope == mDest->mScope) && mTaskEnable->isHierRef()) {
        ret = true;
      }
    }
    return ret;
  }

  HierBlock* mHBlock;             //! owner hierarchical block
  NUTaskEnable* mTaskEnable;      //! nucleus
  HierBlock* mDest;               //! destination hierarchical block
  SInt32 mJumpCount;              //! memo-ized jump count
  bool mPromote;                  //! temp flag for declaring promotion
}; // class TFPromote::HierTaskCall

TFPromote::HierBlock::~HierBlock() {
  for (UInt32 i = 0; i < mCalls.size(); ++i) {
    TFPromote::HierTaskCall* hcall = mCalls[i];
    delete hcall;
  }
}

SInt32 TFPromote::HierBlock::calcJumpCount() {
  if (mJumpCount == UNINITIALIZED_JUMP_COUNT) {
    UInt32 maxCalls = 0;
    for (UInt32 i = 0, n = mCalls.size(); i < n; ++i) {
      TFPromote::HierTaskCall* hcall = mCalls[i];
      UInt32 calls = hcall->calcJumpCount();
      if (calls > maxCalls) {
        maxCalls = calls;
      }
    }
    mJumpCount = maxCalls;
  }
  return mJumpCount;
}


//! design callback to find invoked tasks and the blocks that call them
class TFPromoteCallback : public NUDesignCallback
{
public:
  TFPromoteCallback(IODBNucleus* iodb, TFPromote* tfPromote) :
    mIODB(iodb),
    mTFPromote(tfPromote),
    mCurrentBlock(NULL)
  {}

  ~TFPromoteCallback() {}

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * ) { return eNormal; }

  //! Push the scope-stack based on the instance name
  Status operator() (Phase phase, NUModuleInstance* inst) {
    if (phase == ePre) {
      NU_ASSERT(!mScopeStack.empty(), inst); // should have root module on stack
      STBranchNode* parent = mScopeStack.back();
      STBranchNode* branch = mIODB->findBranch(parent, inst->getName());
      NU_ASSERT(branch, inst);
      mScopeStack.push_back(branch);
    }
    else {
      mScopeStack.pop_back();
      NU_ASSERT(!mScopeStack.empty(), inst); // should have root module on stack
    }
    return eNormal;
  }

  //! On the root module, push/pop the root module name on our scope stack.
  //! on other modules, defer to the NUModuleInstance callback
  Status operator() (Phase phase, NUModule* mod) {
    if (phase == ePre) {
      if (mScopeStack.empty()) {
        STBranchNode* root = mIODB->findBranch(NULL, mod->getName());
        NU_ASSERT(root, mod);
        mScopeStack.push_back(root);
      }
    }
    else {
      if (mScopeStack.size() == 1) {
        mScopeStack.clear();
      }
    }
    return eNormal;
  }

  Status operator() (Phase phase, NUNamedDeclarationScope* declScope) {
    // Push the declaration scope up the stack if it has instances.
    // Otherwise we don't care about it's hierarchy.
    if (declScope->hasInstances())
    {
      if (phase == ePre) {
        NU_ASSERT(!mScopeStack.empty(), declScope); // should have parent module on stack.
        STBranchNode* parent = mScopeStack.back();
        STBranchNode* branch = mIODB->findBranch(parent, declScope->getName());
        NU_ASSERT(branch, declScope);
        mScopeStack.push_back(branch);
      } else {
        mScopeStack.pop_back();
        NU_ASSERT(!mScopeStack.empty(), declScope); // should have parent module on stack.
      }
    }
    return eNormal;
  }

  Status operator() (Phase phase, NUStructuredProc* ab) {
    return processBlock(phase, ab);
  }
  Status operator() (Phase phase, NUTask* task) {
    return processBlock(phase, task);
  }

  //! Keep track of what block we are in, so that if we discover it's
  //! got a task-enable in it, that we can then reactively make a HierBlock
  //! for it
  Status processBlock(Phase phase, NUBase* block) {
    if (phase == ePre) {
      NU_ASSERT(mCurrentBlock == NULL, block);
      NU_ASSERT(!mScopeStack.empty(), block);
      mCurrentBlock = block;
    } else {
      NU_ASSERT(mCurrentBlock == block, block);
      mCurrentBlock = NULL;
    }
    return eNormal;
  }

  //! Elaborate task enables and the blocks they are invoked from
  Status operator() (Phase phase, NUTaskEnable *task_enable) {
    if (phase == ePre) {
      NU_ASSERT(mCurrentBlock, task_enable);
      NU_ASSERT(!mScopeStack.empty(), task_enable);
      STBranchNode* scope = mScopeStack.back();
      TFPromote::HierBlock* hblock =
        mTFPromote->getHierBlock(mCurrentBlock, scope);
      hblock->rememberTaskEnable(task_enable);

      // Note that we cannot resolve the task destination yet because
      // we might not have hit the block yet in the design walk.
      // TFPromote::resolveCalls fixes HierTaskCall::mDest 
    } // if
    return eSkip;               // do not recurse through calls
  } // Status operator

  IODBNucleus* mIODB;
  UtArray<STBranchNode*> mScopeStack;   //! Keep track of design hierarchy
  TFPromote* mTFPromote;
  NUBase* mCurrentBlock;
}; // class TFPromoteCallback : public NUDesignCallback

TFPromote::TFPromote(NUNetRefFactory *netref_factory,
		     MsgContext *msg_ctx,
		     AtomicCache *string_cache,
		     bool verbose,
                     IODBNucleus *iodb,
                     ArgProc* args,
                     bool promoteAllTasks) :
  mNetRefFactory(netref_factory),
  mStringCache(string_cache),
  mMsgContext(msg_ctx),
  mVerbose(verbose),
  mPromoteAllTasks(promoteAllTasks),
  mIODB( iodb ),
  mArgs(args)
{
  mHierBlockSet = new HierBlockSet;
  mHierBlockVec = new HierBlockVec;
  mTaskEnableInstanceMap = new TaskEnableInstanceMap;
  mDeleteInitialBlocks = new InitialBlockSet;
  mDeleteAlwaysBlocks = new AlwaysBlockSet;
  mDeleteTaskEnables = new TaskEnableSet;
  mTaskEnableHierCallMap = new TaskEnableHierCallMap;
  mInlineTasks = false;
}


TFPromote::~TFPromote()
{
  delete mHierBlockSet;
  for (UInt32 i = 0, n = mHierBlockVec->size(); i < n; ++i) {
    HierBlock* hblock = (*mHierBlockVec)[i];
    delete hblock;
  }
  delete mHierBlockVec;

  mTaskEnableInstanceMap->clearPointerValues();
  delete mTaskEnableInstanceMap;
  delete mDeleteInitialBlocks;
  delete mDeleteAlwaysBlocks;
  delete mDeleteTaskEnables;
  delete mTaskEnableHierCallMap;
} // TFPromote::~TFPromote

// Look up an elaborated block, creating it if it does not yet exist
TFPromote::HierBlock* TFPromote::getHierBlock(NUBase* block,
                                              STBranchNode* scope)
{
  HierBlock targetFinder(block, scope);
  HierBlock* hblock = NULL;      
  HierBlockSet::iterator p = mHierBlockSet->find(&targetFinder);

  if (p == mHierBlockSet->end()) {
    hblock = new HierBlock(block, scope);
    mHierBlockSet->insert(hblock);
    mHierBlockVec->push_back(hblock); // for ordered iteration
  }
  else {
    hblock = *p;
  }

  return hblock;
}

// 
void TFPromote::HierBlock::rememberTaskEnable(NUTaskEnable* taskEnable) {
  // HierTaskCall's ctor records itself into the owning hblock
  HierTaskCall* hcall = new TFPromote::HierTaskCall(this, taskEnable);
  mCalls.push_back(hcall);
}

//! Perform rewrite over the whole design
void TFPromote::design(NUDesign *this_design) {
  mInlineTasks = mArgs->getBoolValue(CRYPT("-inlineTasks"));
  findAllTasksAndCalls(this_design);
  resolveCalls();
  findPromotions();
  if (mVerbose) {
    print();
  }
  performPromotions();
  promoteTaskEnables();
  cleanup();
}

//! Initial design walk to find elaborated TaskEnables and their owner blocks
void TFPromote::findAllTasksAndCalls(NUDesign *this_design) {
  mIODB->fullElabParseTable();

  // Find all the instances of task enables
  TFPromoteCallback findTF(mIODB, this);
  NUDesignWalker walker(findTF, false, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.design(this_design);
}

//! Resolve the elaborated blocks for task-enable destinations
void TFPromote::resolveCalls() {
  STSymbolTable* symtab = mIODB->getParseTable();
  for (UInt32 i = 0, n = mHierBlockVec->size(); i < n; ++i) {
    HierBlock* hblock = (*mHierBlockVec)[i];

    for (UInt32 i = 0, n = hblock->mCalls.size(); i < n; ++i) {
      HierTaskCall* hcall = hblock->mCalls[i];

      NUTask* task = hcall->mTaskEnable->getTask();
      STBranchNode* target = hblock->mScope;
      NUTaskEnable* taskEnable = hcall->mTaskEnable;
      
      if (taskEnable->isHierRef()) {
        NUTaskEnableHierRef *hr = dynamic_cast<NUTaskEnableHierRef*>(taskEnable);
        NU_ASSERT( hr, taskEnable );
        NUTaskHierRef* thf = hr->getHierRef();
        const AtomArray& path = thf->getPath();
        STSymbolTableNode* taskBranch
          = NUHierRef::resolveHierRef(path, 0, symtab, target, NULL, true);

        // Find the task by name in the symbol table.  Right now this is
        // a consistency check with the task annotated in the unelaborated
        // task enable, but this could theoretically allow us to resolve to
        // resolve multiple instances of a task-enable into different task
        // bodies.
        NUBase* base = mIODB->getNucleusObject(taskBranch);
        NUTask* ntask = dynamic_cast<NUTask*>(base);
        NU_ASSERT(ntask, taskEnable);
        NU_ASSERT(task == ntask, taskEnable);
        target = taskBranch->getParent();
      }
      hcall->mDest = getHierBlock(task, target);

      // Keep track of all the instances of each unelaborated NUTaskEnable,
      // so that if we promote one of them, we've gotta promote all of them.
      HierTaskCallVec* taskCallVec = (*mTaskEnableInstanceMap)[taskEnable];
      if (taskCallVec == NULL) {
        taskCallVec = new HierTaskCallVec;
        (*mTaskEnableInstanceMap)[taskEnable] = taskCallVec;
      }
      taskCallVec->push_back(hcall);
    } // for
  } // for
} // void TFPromote::resolveCalls

//! Print the entire elaborated call-tree
void TFPromote::print() {
  for (UInt32 i = 0, n = mHierBlockVec->size(); i < n; ++i) {
    HierBlock* hblock = (*mHierBlockVec)[i];

    hblock->print(0);

    for (UInt32 i = 0, n = hblock->mCalls.size(); i < n; ++i) {
      HierTaskCall* hcall = hblock->mCalls[i];
      hcall->print(2);
    } // for
  } // for
}

//! Determine if a particular block should be promoted
void TFPromote::considerPromotingBlock(HierBlock* hblock,
                                       SInt32 maxAllowableJumps)
{
  if (mPromoteAllTasks) {
    // This is really for regression teseting to gain confidence that the
    // rewrite phase is working well by moving all tasks to the root
    // module
    maxAllowableJumps = -1;
  }
  else if (mInlineTasks && (maxAllowableJumps > 0)) {
    // If we are inlining tasks, then we can't tolerate any hierarchical
    // task calls, although there is no reason to promote local tasks
    maxAllowableJumps = 0;
  }
  if (!hblock->isDeclaredPromoted() &&
      (hblock->calcJumpCount() > maxAllowableJumps))
  {
    if (hblock->isPromotable()) {
      if (!hblock->isPromoted()) {
        mPromotionOccurred = true;
      }
      hblock->declarePromoted();
    }

    // It is *not* the case, necessarily, that all the instances of
    // this block need to be promoted.  This block may represent
    // a task that is called both hierarchically and locally.

    // But to eliminate all hierarchical references, we do need
    // to recurse through to all the calls that this block makes
    // and promote them.
    for (UInt32 i = 0, n = hblock->mCalls.size(); i < n; ++i) {
      HierTaskCall* hcall = hblock->mCalls[i];
      promoteCall(hcall);
    }
  }
}

//! Promote a task-enable
void TFPromote::promoteCall(HierTaskCall* hcall) {
  if (!hcall->isDeclaredPromoted()) {
    hcall->declarePromoted();
    considerPromotingBlock(hcall->mDest, mInlineTasks? -1: 0);
    considerPromotingBlock(hcall->mHBlock, -1); // force

    // Every instance of this NUTaskEnable must also be promoted,
    // so that we can unelaboratedly change the NUTaskEnable to
    // refer to an NUTask* in the root, and have that be correct
    // in every instance
    NUTaskEnable* taskEnable = hcall->mTaskEnable;
    HierTaskCallVec* taskCallVec =
      (*mTaskEnableInstanceMap)[taskEnable];
    for (UInt32 j = 0, nt = taskCallVec->size(); j < nt; ++j) {
      hcall = (*taskCallVec)[j];
      promoteCall(hcall);
    }
  }
}

//! Find all desirable block promotions
void TFPromote::findPromotions() {
  do {
    mPromotionOccurred = false;
    for (UInt32 i = 0, n = mHierBlockVec->size(); i < n; ++i) {
      HierBlock* hblock = (*mHierBlockVec)[i];
      if (hblock->getStructuredProc() != NULL) {
        // Only start at always-blocks, which are the the roots
        // of the task-call trees
        considerPromotingBlock(hblock, 1);
      }
    }
    clearJumpCount();
  } while (mPromotionOccurred);
}

//! Clear all the memo-ized jump counts in preparation for another
//! call-graph walk
void TFPromote::clearJumpCount() {
  for (UInt32 i = 0, n = mHierBlockVec->size(); i < n; ++i) {
    HierBlock* hblock = (*mHierBlockVec)[i];
    hblock->clearJumpCount();
    for (UInt32 i = 0, n = hblock->mCalls.size(); i < n; ++i) {
      HierTaskCall* hcall = hblock->mCalls[i];
      hcall->clearJumpCount();
    }
  }
}


//! Perform all the block promotions that were previously computed
void TFPromote::performPromotions() {
  for (UInt32 i = 0, n = mHierBlockVec->size(); i < n; ++i) {
    HierBlock* hblock = (*mHierBlockVec)[i];
    if (hblock->isPromoted()) {
      performBlockPromotion(hblock);
    }
  }
}

//! Callback to find nets referenced in blocks
class TFPromoteStructuredProcNetFinder : public NUDesignCallback {
public:
  TFPromoteStructuredProcNetFinder()
  {
  }

  void addNet(NUNet* net) {
    if (!net->isNonStatic()/* && !net->isBlockLocal() */) {
      mNets.insert(net);
    }
  }

  Status operator()(Phase phase, NUIdentRvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUIdentLvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUMemselLvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUMemselRvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase, NUBase*) {
    return eNormal;
  }

  NUNetSet mNets;
}; // class TFPromoteStructuredProcNetFinder : public NUDesignCallback

// Transform a net from the original block into the destination scope
void TFPromote::promoteNet(HierBlock* hblock, NUNet* net, CopyContext* cc) {
  // This net is coming in on mOrigScope and we need to create one
  // in mScope.  Ideally we'd see if there was a local alias of that net,
  // but that's not currently computed TFPromote time.  It could be.
  //
  // So we will need to create a hierarchical reference to the correct net.
  // First, find the correct net, which might already be a hierarchical
  // reference
  STBranchNode* netScope = hblock->mOrigScope;
  NUModule* mod = NULL;
  NUNet* newNet = NULL;
  NUNet* srcNet = net;

  AtomArray hrefBuf;

  if (net->isHierRef()) {
    NUNetHierRef *href = net->getHierRef();
    const AtomArray& atomArray = href->getPath();
    //href->getName(&netbuf);
    STSymbolTable* parseTable = mIODB->getParseTable();
    STSymbolTableNode* node
      = NUHierRef::resolveHierRef(atomArray, 0, parseTable, netScope,
                                  NULL, true);
    NU_ASSERT(node, net);    
    netScope = node->getParent();
    NU_ASSERT(netScope, net);

    NUHierRef::buildAtomArray(&hrefBuf, node);
    STBranchNode* root = sFindRoot(node->getParent());
    mod = findModule(root);

    // We need to build the new hierarchical reference in terms of the
    // resolved net, not the original hierarchical reference
    srcNet = dynamic_cast<NUNet*>(mIODB->getNucleusObject(node));
    NU_ASSERT(srcNet, net);

    // If it turns out that the net resolved into the root module,
    // then we don't need a hierarchical reference
    if (netScope == root) {
      newNet = srcNet;
    }
  }
  else {
    // Build a hierarchical reference
    mod = findModule(hblock->mScope);
    NUHierRef::buildAtomArray(&hrefBuf, hblock->mOrigScope);
    NUHierRef::buildAtomArray(&hrefBuf, net->getNameLeaf());
  }

  if (newNet == NULL) {
    newNet = srcNet->buildHierRef(mod, hrefBuf);
  }

  cc->mNetReplacements[net] = newNet;
} // void TFPromote::promoteNet

//! Find the nucleus module associated with a scope
NUModule* TFPromote::findModule(STBranchNode* scope) {
  NUBase* base = mIODB->getNucleusObject(scope);
  ST_ASSERT(base, scope);
  NUModule* mod = dynamic_cast<NUModule*>(base);
  NU_ASSERT(mod, base);
  return mod;
}

//! Perform the promotion of a particular block
void TFPromote::performBlockPromotion(HierBlock* hblock) {
  // Moves a block from mOrigScope to mScope, taking care to:
  //   - change any module-scoped net references to be
  //     hierarchical net references.  Variables that are 
  //     scoped to this task or always-block can remain
  //     as non-hierarchical references.  TBD: use hierarchical
  //     aliasing information to keep whatevever we can as local.

  //   - Resolve all hierachical references to nets.  Those that
  //     resolve to the root module can be made local.  Others can
  //     remain as fully elaborated names in the promoted module.

  //   - keep track of task-enables.  If they are to be promoted,
  //     and their destination is also to be promoted, then we
  //     now have a local task-enable, where previously we had
  //     a hierarchical one.  Otherwise, previously local task
  //     references would need to become hierarchical

  NUStructuredProc* structuredProc = hblock->getStructuredProc();
  NUTask* task = hblock->getTask();
  NUModule* dstModule = findModule(hblock->mScope);
  CopyContext cc(dstModule, mStringCache, true);

  if (structuredProc != NULL) {
    TFPromoteStructuredProcNetFinder netFinder;
    NUDesignWalker walker(netFinder, false, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.structuredProc(structuredProc);
    for (NUNetSet::UnsortedLoop p = netFinder.mNets.loopUnsorted();
         !p.atEnd(); ++p)
    {
      NUNet* net = *p;
      promoteNet(hblock, net, &cc);
    }

    NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(structuredProc);
    if (always != NULL) {
      NUAlwaysBlock* newAlways = always->clone(cc, true);
      dstModule->addAlwaysBlock(newAlways);

      // Defer the deletion of always-blocks because the owner module
      // may be multiply instantiated, and we must only delete them
      // once.
      mDeleteAlwaysBlocks->insert(always);
      hblock->putReplacement(newAlways);
    }
    else {
      NUInitialBlock* initial = dynamic_cast<NUInitialBlock*>(structuredProc);
      NU_ASSERT(initial, structuredProc);
      NUInitialBlock* newInitial = initial->clone(cc);
      dstModule->addInitialBlock(newInitial);
      mDeleteInitialBlocks->insert(initial);
      hblock->putReplacement(newInitial);
    }
  }
  else {
    NUTFWalkerCallback netFinder;
    NUDesignWalker walker(netFinder, false, false);
    walker.putWalkTasksFromTaskEnables(false);
    NU_ASSERT(task,structuredProc);
    walker.tf(task);
    for (NUNetSet::UnsortedLoop p = netFinder.mNonLocalUses.loopUnsorted();
         !p.atEnd(); ++p)
    {
      NUNet* net = *p;
      promoteNet(hblock, net, &cc);
    }
    for (NUNetSet::UnsortedLoop p = netFinder.mNonLocalDefs.loopUnsorted();
         !p.atEnd(); ++p)
    {
      NUNet* net = *p;
      promoteNet(hblock, net, &cc);
    }

    // Create a new branch-node the unelaborated symbol table inside
    // the root module.  Need to gensym the name to avoid clashes
    StringAtom* taskName = dstModule->gensym("promote", NULL, task);
    NUTask * newTask = new NUTask ( dstModule,
                                    taskName,
				    task->getOriginalName(),
                                    mNetRefFactory,
                                    task->getLoc(),
                                    mIODB );

    NUTF::clone ( newTask, *task, cc, true );
    if (task->isRequestInline()) {
      newTask->putRequestInline(true);
    }

    dstModule->addTask(newTask);
    hblock->putReplacement(newTask);
  } // else
} // void TFPromote::performBlockPromotion

//! Callback to map task-enables on a block
/*!
 * This class is used for each HierBlock to map NUTaskEnable* to
 * HierTaskCall.  The reason this is needed is that it is called
 * after the block has been cloned, so all the NUTaskEnable* have
 * been rewritten.  But we are going to count on the fact that we will
 * be traversing the cloned block and hitting task enables in the
 * exact same order as we did when we built the HierTaskCalls from
 * TFPromoteCallback.
 */
class TFPromoteTaskEnableFinderCallback : public NUDesignCallback {
public:
  TFPromoteTaskEnableFinderCallback(TFPromote::HierBlock* hb,
                                    TFPromote::TaskEnableHierCallMap* taskEnableMap)
    : mIndex(0),
      mHBlock(hb),
      mTaskEnableHierCallMap(taskEnableMap)
  {}
  ~TFPromoteTaskEnableFinderCallback() {}
  Status operator() (Phase , NUBase * ) { return eNormal; }

  Status operator() (Phase phase, NUTaskEnable *taskEna) {
    if (phase == ePre) {
      NU_ASSERT(mIndex < mHBlock->mCalls.size(), taskEna);
      TFPromote::HierTaskCall* hcall = mHBlock->mCalls[mIndex];

      // sanity check...the traversal order from NUDesignWalker needs
      // to hit the hier-refs on the original task/always-block in the
      // same order as it hits the one that gets cloned.
      NU_ASSERT(taskEna->getLoc() == hcall->mTaskEnable->getLoc(), taskEna);

      (*mTaskEnableHierCallMap)[taskEna] = hcall;
      ++mIndex;
    } // if
    return eSkip;               // do not recurse through calls
  }

  UInt32 mIndex;
  TFPromote::HierBlock* mHBlock;
  TFPromote::TaskEnableHierCallMap* mTaskEnableHierCallMap;
};

//! TaskEnableFixupCallback class
/*!
 * This class replaces task enables with other task enables.
 */
class TFPromote::TaskEnableFixupCallback :
  public FlattenTaskEnableFixupCallback
{
public:
  TaskEnableFixupCallback(TFPromote* tfpromote,
                          NUModule* mod,
                          NUNetRefFactory* nrf)
    : FlattenTaskEnableFixupCallback(mod, nrf),
      mTFPromote(tfpromote)
  {
  }

protected:
  virtual NUStmt* fixupTaskEnable(NUStmt * enable_stmt) {
    NUTaskEnable* taskEnable = dynamic_cast<NUTaskEnable*>(enable_stmt);
    NUStmt* ret = NULL;
    if (taskEnable != NULL) {
      ret = mTFPromote->rewriteTaskEnable(taskEnable);
      NU_ASSERT(ret != enable_stmt, enable_stmt);
    }
    return ret;
  }
private:
  TFPromote* mTFPromote;
};

//! OutputSysTaskCallback class
/*!
 * This class updates output system tasks with the appropriate scope so
 * that %m comes out right
 */
class OutputSysTaskCallback :  public NUDesignCallback {
public:
  OutputSysTaskCallback(STBranchNode* scope): mScope(scope) {
  }

  virtual Status operator()(Phase, NUBase*) {return eNormal;}

  Status operator()(Phase phase, NUOutputSysTask* outsys) {
    if (phase == ePre) {
      if (outsys->hasHierarchicalName()) {
        UtString buf;
        mScope->compose(&buf);
        outsys->putPromotePath(buf.c_str());
      }
    }
    return eNormal;
  }
private:
  STBranchNode* mScope;
};

NUTaskEnable* TFPromote::rewriteTaskEnable(NUTaskEnable* taskEna) {
  TaskEnableHierCallMap::iterator p = mTaskEnableHierCallMap->find(taskEna);
  NU_ASSERT(p != mTaskEnableHierCallMap->end(), taskEna);
  TFPromote::HierTaskCall* hcall = p->second;

  // sanity check...the traversal order from NUDesignWalker needs
  // to hit the hier-refs on the original task/always-block in the
  // same order as it hits the one that gets cloned.
  const SourceLocator& loc = taskEna->getLoc();
  NU_ASSERT(loc == hcall->mTaskEnable->getLoc(), taskEna);
  NUTaskEnable* replacement = NULL;
  HierBlock* hblock  = hcall->mHBlock;

  if (hcall->isReplaced() || hblock->isPromoted()) {
    CopyContext cc(0, 0);
    NUTask* newTask = hcall->mDest->getTask();
    NU_ASSERT(newTask, taskEna);
    NUTaskHierRef* href = hcall->mTaskEnable->getHierRef();
    bool isLocal = hblock->mScope == hcall->mDest->mScope;
    NUModule* rootMod = findModule(hblock->mScope);

    // We can change a local task-enable into a hierarchical one.  See
    // test/hierref_unique/mixed_children.v.  task mux needs to be
    // promoted, because it's hierarchically called, and it hierarchically
    // calls subfuncs.and2.  But it also has a local call to or2, and
    // that task-enable becomes hierarchical when we promote mux.
    if (href == NULL) {
      if (isLocal) {
        // This changes the tf-arg-array to reflect the new resolution
        taskEna->replaceTask(newTask, cc);
      }

      else {
        // Convert local task enable into a hierarchical one.  Resolution
        // fixup happens below
        AtomArray newPath;
        NUHierRef::buildAtomArray(&newPath, hcall->mDest->mScope);
        StringAtom* taskName = newTask->getName();
        replacement =
          new NUTaskEnableHierRef(newPath, taskName, rootMod,
                                  mNetRefFactory, false, loc);
        replacement->replaceTask(newTask, cc, taskEna);
        mDeleteTaskEnables->insert(taskEna);
        taskEna = replacement;
      }
    }

    // Now we have to properly change the resolution
    else if (isLocal) {
      // We are changing the type of a hierarchical TaskEnable
      // into a local TaskEnable.
      replacement = new NUTaskEnable(newTask, rootMod, mNetRefFactory,
                                     taskEna->getUsesCFNet(),
                                     loc);

      // change the tf-arg-array to reflect the new resolution
      replacement->replaceTask(newTask, cc, taskEna);
      mDeleteTaskEnables->insert(taskEna);
    }

    if (!isLocal) {
      // Changing a hierarhical task-enable to another one, we just
      // have to fix up the resolution and the tf array

      // This changes the tf-arg-array to reflect the new resolution
      if (replacement == NULL) {
        taskEna->replaceTask(newTask, cc);
      }

      // Else we are just changing the resolution task
      NUBaseVector res;
      res.push_back(newTask);
      href = taskEna->getHierRef();
      href->cleanupResolutions();
      href->replaceResolutions(res);
      newTask->addHierRef(taskEna);

      // We have to change the scope of the new hierarchical reference
      // to reflect the fact that we are referencing it from a new
      // location.  At present this new location is always the root,
      // but I think we can support, in this routine, having the
      // path for new location be any sub-path of the original location
      if (!hcall->mDest->isPromoted()) {
        AtomArray newPath;
        NUHierRef::buildAtomArray(&newPath, hcall->mDest->mScope);
        newPath.push_back(newTask->getName());
        href->putPath(newPath);
      }
    }
  }
  return replacement;
} // NUTaskEnable* TFPromote::rewriteTaskEnable

// After all tasks have been moved, we can repair the task-enables
void TFPromote::promoteTaskEnables() {
  for (UInt32 i = 0, n = mHierBlockVec->size(); i < n; ++i) {
    HierBlock* hblock = (*mHierBlockVec)[i];
    bool needBlockWalk = hblock->isPromoted();
    for (UInt32 j = 0, nc = hblock->mCalls.size();
         !needBlockWalk && (j < nc); ++j)
    {
      HierTaskCall* hcall = hblock->mCalls[j];
      needBlockWalk = hcall->isReplaced();
    }

    if (needBlockWalk) {
      // First, map all the NUTaskEnable* in the cloned block back to
      // the HierTaskCall we created
      mapTaskEnables(hblock);

      {
        NUModule* dstModule = findModule(hblock->mScope);
        TaskEnableFixupCallback cb(this, dstModule, mNetRefFactory);
        NUDesignWalker walker(cb, false, false);
        hblock->walk(&walker);
      }
      {
        OutputSysTaskCallback cb(hblock->mOrigScope);
        NUDesignWalker walker(cb, false, false);
        hblock->walk(&walker);
      }
    }        
  } // for
} // void TFPromote::promoteTaskEnables


//! When fixing up task-enables in a promoted block, we need to be
//! able to associate the original NUTaskEnable* (stored in the
//! HierTaskCall), with its promoted clone.
void TFPromote::mapTaskEnables(HierBlock* hblock) {
  mTaskEnableHierCallMap->clear();
  TFPromoteTaskEnableFinderCallback finder(hblock, mTaskEnableHierCallMap);
  NUDesignWalker walker(finder, false, false);
  hblock->walk(&walker);
}

//! Delete all original nucleus always- and initial-blocks after promotion
/*! Note that anytime an always-block or initial-block is promoted, it must
 *! be promoted in every instance.  But that is not necessarily true for
 *! tasks, where there may be both local and hierarchical calls to the same
 *! nucleus task.  So we leave it to MarkSweepUnusedTasks to clean up the
 *! dead tasks for us, rather than presuming that every promoted NUTask*
 *! must be deleted.
 */
void TFPromote::cleanup() {
  for (AlwaysBlockSet::iterator p = mDeleteAlwaysBlocks->begin(),
         e = mDeleteAlwaysBlocks->end(); p != e; ++p)
  {
    NUAlwaysBlock* always = *p;
    NUModule* mod = always->findParentModule();
    mod->removeAlwaysBlock(always);
    delete always;
  }
  mDeleteAlwaysBlocks->clear();
  for (InitialBlockSet::iterator p = mDeleteInitialBlocks->begin(),
         e = mDeleteInitialBlocks->end(); p != e; ++p)
  {
    NUInitialBlock* initial = *p;
    NUModule* mod = initial->findParentModule();
    mod->removeInitialBlock(initial);
    delete initial;
  }
  mDeleteInitialBlocks->clear();
  mDeleteTaskEnables->clearPointers();
}


/* Failures when running nightly with -promoteAllTasks

output spew diffs (harmless)
  test/bugs/bug1354
  test/bugs/bug2844
  test/bugs/bug2916
  test/bugs/bug3978 (wavequery diffs)
  test/costs
  test/flatten
  test/langcov
bug4585, %m
  test/langcov/Display
nCompare failure, bug4583
  test/cust/matrox/phoenix/carbon/cwtb/dmatop
nCompare failure, bug4588
  test/cust/matrox/phoenix/carbon/cwtb/rttop
nCompare failure, bug4589
  test/cust/matrox/phoenix/carbon/cwtb/pretop

*/


/* Richard's comments:

1. Use the mOrigBlock and mOrigScope to hash the HierBlock class so that
   the hash set remains valid after blocks have been promoted. (DONE)
2. In the TFPromoteCallback you do some work that is almost identical to
    NUInstanceWalker. It would be nice to reuse that code if possible.
    I know you have different symbol tables but you should be able to
    pass that into the constructor.
3. If the TFPromote class every gets allocated before command line
   parsing, the code to set the inlineTasks boolean won't work. (DONE)
4. It would be good to print status lines during promotion major passes.
5. Pity this couldn't run when we need to split blocks in the scheduler.
   It would help if the block has a hier ref call.
6. The code in considerPromotingBlock is a little confusing at first.
   I didn't understand why you needed to modify the input variable. I
   believe I understand it now, but it would be cleaner to set this
   in the caller if possible.
7. Also in considerPromotingBlock I don't understand this code:

    if (hblock->isPromotable()) {
      if (!hblock->isPromoted()) {
        mPromotionOccurred = true;
      }
      hblock->declarePromoted();
    }

   A comment on why it might already be promoted would help. Also, why 
   are you promoting it again if it is already promoted?

8. The TFPromoteStructuredProcNetFinder class worries me. What if a new
   expression or lvalue type is added? Can't it be written on the more
   general NULvalue and NUExpr. I know you can call getUses on any 
   NUExpr before UD is computed. But I don't know if you can call 
   getDefs() on NULvalues.

   Is the reason that you didn't have to handle NUVarSelXXX because the
   ident for those is always an expression now?

9. In TFPromoteTaskEnableFinderCallback instead of counting on the order
   of the walk, can you use a map from old to new task enables?

10. I am a little nervous about some of the promotion code. Snipets 
    like: NUAliasDataBOM....   (DONE)

There are parts of the code I just glossed over because I have forgotten
a lot about hier refs. It made sense at a superficial level but I'm not
sure I gave you good coverage. It was mostly in resolving hier refs and
moving net references to be hier refs.

*/

/* Aron's comments:

1. Test hier-ref to task that resolves locally (DONE -- test1.v)
2. In spec, clarify implication of promoting 'and2' and 'or2', so that
   -inlineTasks will actually work (DONE)
3. Construct new tasks using StringAtom constructor to avoid messing with
   new BranchNode
4. Implement inlineSingleTaskCalls
5. Implement inline-directive for a task
6. Move block-comment at top of tfpromote.cxx into .h
7. move sFindRoot into STSymbolTableNode class
8. use initializers for all member vars of HierBlock, not some of them
9. Use UtIndent in print()
10. it's mScope --> its mScope
11. HierBlock::walk --> check status & assert
12. Note that calcJumpCount breaks on recursive functions.  Probably
    should assert rather than break the stack
13. processBlock() --> comment assumption that taskEnables are not
    walked through to encounter tasks
14. Q: can tasks be declared within other tasks?
15. Document why we are keeping mHierBlockSet and mHierBlockVec rather
    than iterating over the set.
16. Check whether task-enables can elaboratedly resolve to multiple tasks
17. task calls in named blocks?  is that an issue?
18. Separate jump-count calculation from promotion
19. Comments on purpose of TFPromoteStructuredProcNetFinder::addNet.
    virtual nets?
20. dynamic_cast<NUModule*>.... -> findModule (DONE)
21. (netScope==root) check ignores block-declared nets under root module
22. The bulk of promoteNet is redundant with LocalHierRefResolver
23. Remove commented-out mVerbose section  (DONE)
24. TFPromote::findModule:: assert --> ST_ASSERT (DONE)
25. unsorted loop over netFinder.mNets may generate downstream ptr ordering
    problems
26. divide ::performBlockPromotion into smaller funcs
27. sanity check that new block no longer references nets in old module
28. aliasDB hacking unnecessary, see NUTask(StringAtom....)  (DONE)
29. sanity check that this task is no longer referenced (?)
30. Move FlattenTaskEnableFixupCallback to common location
31. Comment on fixupTaskEnable's interface
32. could rely on LocalHierRefResolve to fix up now-local enables
33. Add comment that a criteria for promotion is always that there
    will be exactly one resolution.  Otherwise we have to go up.
34. mTaskEnableHierCallMap should not be a member var.
35. TFpromote::promoteTaskEnables() -- dstModule is always the root module
    (but which root module?)
36. NUModule::removeBlock happens during delete


Aron's second review comments:

1. Simplify NUOutputSysTask::emitCode rather than making it bigger
2. Unifify the way NUTaskEnableHierRefs are constructed, always passing
   in module.
3. In TFPromote::rewriteTaskEnable, always construct a new NUTaskEnableHierRef
   rather than attempting to preserve the old one
4. Come up with non-promoting testcase proving we were dying when destructing
   tasks before the NUDesign change.  Also fix the way NetHierRefs get
   destructed
*/
