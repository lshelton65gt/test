// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*! \file

This file contains all the code necessary for composite resynthesis.
*/

#include "localflow/CompositeResynth.h"
#include "localflow/Populate.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUCompositeNetHierRef.h"
#include "util/CarbonTypes.h"
#include "util/CbuildMsgContext.h"

// Composite net resynth debug
#define DIMRESYNTH_DEBUG 0
// Everything else debug
#define COMPRESYNTH_DEBUG 0

/*! These static functions are used by both walkers in this file.  It takes
 * an expression and breaks it into parts, filling in the exprvector
 * argument.  This will allocate memory!  The optional parameter
 * numElems is used only when the expr is NULL, like for an open record
 * port association.  It tells us how many NULLs to put in the vector.

 * The expression could be an aggregate for record or single/multi dimensional
 * array of records. The records themselves could have fields that are 
 * record or single/multi dimensional array of records. The iterators allow
 * rearranging the aggregates to match the resynthesized lvalue. Here's an
 * VHDL example:
 *
 type t1 is record 
   f1 : integer;
   f2 : integer;
 end record;

 type t2 is array (Natural Range <>) of t1;

 constant sig_t2 : t2(1 downto 0) := (1 => (1, 3), others => (2, 4));

 This will be resynthesized as:

 {sig_t2.f1[1:0], sig_t2.f2[1:0]} = {1, 2, 3, 4};

 * There will be a RecordAggregateArrayIter for the rvalue aggregate.
 * The elements in the rvalue will have RecordAggregateIter.
 * The integer fields will have MiscAggregateIter.
 
 * The RecordAggregateArrayIterator will iterate over both RecordAggregateIters
 * simultaneously in one advance, resulting in the resynthesized rvalue.
    
 */
static void createRvalueVector(NUExpr* expr, UtVector<NUExpr*>* exprvector,
                               MsgContext* msgContext, const SInt32 numElems = -1);
static void createLvalueVector(NULvalue* expr, UtVector<NULvalue*>* exprvector,
                               MsgContext* msgContext, const SInt32 numElems = -1);

//! Base class for composite/non-composite aggregate expression iterators.
template <class T1, class T2>
class AggregateIter
{
public:

  AggregateIter(T1* expr, MsgContext* msgCtx):
    mExpr(expr),
    mDone(false),
    mMsgCtx(msgCtx)
  {
  }

  virtual ~AggregateIter()
  {
  }

  //! Iterate over next expression(s), make their copies and 
  //! add them to the provided expression vector
  virtual bool next(UtVector<T1*>* exprVec, const SInt32 numElems) = 0;

protected:

  //! Static helper methods.

  //! Create an iterator for given aggregate expression of appropriate type.
  static AggregateIter<T1,T2>* create(T1* expr, UtVector<T1*>* exprVec,
                                      MsgContext* msgContext, const SInt32 numElems);

  //! Create a concat for NUExpr
  static NUExpr* concatHelper(UtVector<NUExpr*>& vec, const SourceLocator& loc)
  {
    NUConcatOp* concat = new NUConcatOp(vec, 1, loc);
    concat->resize(concat->determineBitSize());
    return concat;
  }

  //! Create a concat for NULvalue
  static NULvalue* concatHelper(UtVector<NULvalue*>& vec, const SourceLocator& loc)
  {
    NUConcatLvalue* concat = new NUConcatLvalue(vec, loc);
    concat->resize();
    return concat;
  }

private:

  //! Create vector of expressions from the given aggregate composite expression.
  static void createExprVector(T1* expr, UtVector<T1*>* exprvector,
                               MsgContext *msgContext, const SInt32 numElems);

  //! Friends that provide access to implementation.
  friend void createRvalueVector(NUExpr*, UtVector<NUExpr*>*,
                                 MsgContext*, const SInt32);
  friend void createLvalueVector(NULvalue*, UtVector<NULvalue*>*,
                                 MsgContext*, const SInt32);
  
protected:

  T1*  mExpr;
  bool mDone;
  MsgContext* mMsgCtx;

};

//! Iterator for all miscellaneous expressions. These are expressions that
//! are not record or single/multi dimensional array of record expressions.
template <class T1, class T2>
class MiscAggregateIter : public AggregateIter<T1,T2>
{
public:

  MiscAggregateIter(T1* expr, MsgContext* msgCtx):
    AggregateIter<T1,T2>(expr, msgCtx)
  {
    if (dynamic_cast<T2*>(expr) != NULL) {
      NU_ASSERT("Expecting non-composite expressions." == NULL, expr);
    }
  }

  ~MiscAggregateIter()
  {
  };

  //! Makes a copy of the aggregate expression and adds it to the expression vector.
  bool next(UtVector<T1*>* exprVec, const SInt32 /*numElems*/)
  {
    if (!this->mDone)
    {
      CopyContext cc(0, 0);
      NUNet* net = NULL;
      if ((net = getIdentifier(this->mExpr)) != NULL)
      {
        NUCompositeNet *cNet = net->getCompositeNet();
        if ( cNet != NULL )
          createNetExprVector(exprVec, cNet);
        else
          exprVec->push_back(this->mExpr->copy(cc));
      }
      else
      {
        exprVec->push_back(this->mExpr->copy(cc));
      }
      this->mDone = true;
    } // if
    return this->mDone;
  }

private:

  NUNet* getIdentifier(NUExpr* expr) {
    if (expr->getType() == NUExpr::eNUIdentRvalue) {
      NUIdentRvalue* identExpr = dynamic_cast<NUIdentRvalue*>(expr);
      return identExpr->getIdent();
    }
    return NULL;
  }

  NUNet* getIdentifier(NULvalue* expr) {
    if (expr->getType() == eNUIdentLvalue) {
      NUIdentLvalue* identExpr = dynamic_cast<NUIdentLvalue*>(expr);
      return identExpr->getIdent();
    }
    return NULL;
  }

  void createNetExprVector(NUExprVector* exprVec, NUCompositeNet* net) {
    net->createExprVector(exprVec);
  }

  void createNetExprVector(NULvalueVector* exprVec, NUCompositeNet* net) {
    net->createLvalueVector(exprVec);
  }

};

//! Iterator for composite aggregate expression. Iterates over a field
//! of the record at a time.
template <class T1, class T2>
class RecordAggregateIter : public AggregateIter<T1,T2>
{
public:

  RecordAggregateIter(T2* expr, MsgContext* msgCtx):
    AggregateIter<T1,T2>(expr, msgCtx),
    mArgIdx(0),
    mSize(expr->getNumArgs()),
    mCurIter(NULL)
  {
    if (expr->isCompositeArray()) {
      NU_ASSERT("Expecting record composite aggregate" == NULL, expr);
    }
  }

  ~RecordAggregateIter()
  {
    if (mCurIter != NULL) {
      NU_ASSERT("All fields of record aggregate have not been iterated through" == NULL,
                this->mExpr);
    }
  }

  //! Makes a copy of the next field, and adds it to the expression vector.
  bool next(UtVector<T1*>* exprVec, const SInt32 numElems)
  {
    if (!this->mDone)
    {
      // Create an iterator over field.
      if (mCurIter == NULL) {
        T2* expr = dynamic_cast<T2*>(this->mExpr);
        if (expr == NULL) {
          NU_ASSERT("Unexpected type" == NULL, this->mExpr);
        }
        T1* expr_idx = expr->getArg(mArgIdx);
        mCurIter = AggregateIter<T1,T2>::create(expr_idx, exprVec, this->mMsgCtx, numElems);
      }
 
      // If mCurIter is NULL, it means we had a null expr at this argument index.
      bool done = false;
      if (mCurIter == NULL) {
        done = true;
      } else {
        // Advance field iterator.
        done = mCurIter->next(exprVec, numElems);
        // If field iterator is done, delete it.
        if (done) {
          delete mCurIter;
          mCurIter = NULL;
        }
      }

      // If field iterator is done, move to next field.
      if (done) {
        ++mArgIdx;
      }

      // If all fields are done, the iterator is done.
      if (mArgIdx == mSize) {
        this->mDone = true;
      }
    } // if
    return this->mDone;
  }

private:
  
  UInt32 mArgIdx;
  UInt32 mSize;
  AggregateIter<T1,T2>* mCurIter;
};

//! Iterator for composite array aggregate expression. Each element of
//! array has an iterator, which are all simultaneously advanced. When
//! all iterators are done ..which should be at the same time if all
//! array elements are of same type, it's done.
template <class T1, class T2>
class RecordAggregateArrayIter : public AggregateIter<T1,T2>
{
public:

  RecordAggregateArrayIter(T2* expr, MsgContext* msgCtx):
    AggregateIter<T1,T2>(expr, msgCtx),
    mSize(expr->getNumArgs())
  {
    if (!expr->isCompositeArray()) {
      NU_ASSERT("Expecting composite array aggregate" == NULL, expr);
    }
    mIters.resize(mSize);
  }

  ~RecordAggregateArrayIter()
  {
    if (mIters[0] != NULL) {
      NU_ASSERT("All elements of record array aggregate have not been iterated through." == NULL,
                this->mExpr);
    }
  }

  //! Makes a copy of the next field gotten from each array element iterator,
  //! and adds them to the expression vector in element order.
  bool next(UtVector<T1*>* exprVec, const SInt32 numElems)
  {
    if (!this->mDone)
    {
      // Create iterators, one for each array element.
      if (mIters[0] == NULL) {
        T2* expr = dynamic_cast<T2*>(this->mExpr);
        if (expr == NULL) {
          NU_ASSERT("Unexpected type" == NULL, this->mExpr);
        }
        
        for (UInt32 i = 0; i < mSize; ++i) {
          T1* expr_i = expr->getArg(i);
          if (expr_i == NULL) {
            NU_ASSERT("Composite array aggregate has null elements" == NULL, expr);
          }
          mIters[i] = AggregateIter<T1,T2>::create(expr_i, exprVec, this->mMsgCtx, numElems);
        }
      }

      // Advance iterator for every array element.
      for (UInt32 i = 0; i < mSize; ++i) {
        bool isDone = mIters[i]->next(exprVec, numElems);
        // Check to ensure that every array element is done simultaneously.
        // This should be true since each array element should be of same type.
        if (i == 0)
          this->mDone = isDone;
        if (isDone != this->mDone) {
          NU_ASSERT("Composite array aggregate element with unequal elements found" == NULL,
                    this->mExpr);
        }
      }

      // Delete iterators if every array element is done.
      if (this->mDone) {
        for (UInt32 i = 0; i < mSize; ++i) {
          delete mIters[i];
          mIters[i] = NULL;
        }
      }
    }
    return this->mDone;
  }

private:

  UInt32 mSize;
  UtVector<AggregateIter<T1,T2>*> mIters;
};

//! Create an iterator for given aggregate expression of appropriate type.
template <class T1, class T2>
AggregateIter<T1,T2>* AggregateIter<T1,T2>::create(T1* expr, UtVector<T1*>* exprVec,
                                                   MsgContext* msgContext,
                                                   const SInt32 numElems)
{
  AggregateIter<T1,T2>* aggIter = NULL;
  if (expr == NULL)
  {
    if (numElems < 0) {
      NU_ASSERT("A NULL expression vector must have a valid number of elements" == NULL,
                expr);
    }
    for (int i = 0; i < numElems; ++i)
      exprVec->push_back( NULL );    
    return NULL;
  }
  else
  {
    T2* composite_expr = dynamic_cast<T2*>(expr);
    if (composite_expr != NULL)
    {
      // Record single/multi dimensional array composite expressions get RecordAggregateArrayIter
      if (composite_expr->isCompositeArray())
        aggIter = new RecordAggregateArrayIter<T1,T2>(composite_expr, msgContext);
      else // Record composite expressions get RecordAggregateIter.
        aggIter = new RecordAggregateIter<T1,T2>(composite_expr, msgContext);
    }
    //! Everything else gets miscellaneous iterator MiscAggregateIter.
    else
    {
      aggIter = new MiscAggregateIter<T1,T2>(expr, msgContext);
    }
  }
  return aggIter;
}

template <class T1, class T2>
void AggregateIter<T1,T2>::createExprVector(T1* expr, UtVector<T1*>* exprvector,
                                            MsgContext *msgContext,
                                            const SInt32 numElems)
{
  // Create an iterator for aggregate expression of appropriate type.
  AggregateIter<T1,T2>* aggIter =
    AggregateIter<T1,T2>::create(expr, exprvector, msgContext, numElems);
  
  bool done = false;
  while (!done && (aggIter != NULL)) {
    // Add a copy of expression(s) to vector and advance the iterator.
    done = aggIter->next(exprvector, numElems);
  }

  // Delete iterator.
  delete aggIter;

  // Ensure that vector has expected number of elements.
  SInt32 numVecElems = exprvector->size();
  // If the number of elements in the expression have been specified, group the
  // exprvector components into numElems number of elements. This way,
  // if this vector assigns to a record, then it's size is exactly equal to the
  // number of record fields. If it assigns to array, it's size is exactly equal
  // to the number of array elements.
  if ((numElems != -1) && (numElems != numVecElems)) {
    if ((numVecElems % numElems) != 0) {
      NU_ASSERT("Failed to resynthesize aggregate expression." == NULL, expr);
    }
    UInt32 numPerGroup = numVecElems/numElems;
    UtVector<T1*> in_vec(*exprvector);
    exprvector->clear();
    typename UtVector<T1*>::iterator cur_itr = in_vec.begin();

    for (SInt32 idx = 0; idx < numElems; ++idx) {
      typename UtVector<T1*>::iterator next_itr = cur_itr + numPerGroup;
      UtVector<T1*> grp_vec(cur_itr, next_itr);
      cur_itr = next_itr;

      T1* grp_concat = concatHelper(grp_vec, expr->getLoc());
      exprvector->push_back(grp_concat);
    }
  }
}

void createRvalueVector(NUExpr* expr, UtVector<NUExpr*>* exprvector,
                        MsgContext* msgContext, const SInt32 numElems)
{
  AggregateIter<NUExpr,NUCompositeExpr>::createExprVector(expr, exprvector,
                                                          msgContext, numElems);
}

void createLvalueVector(NULvalue* expr, UtVector<NULvalue*>* exprvector,
                        MsgContext* msgContext, const SInt32 numElems)
{
  AggregateIter<NULvalue,NUCompositeLvalue>::createExprVector(expr, exprvector,
                                                              msgContext, numElems);
}

/*! This static data element is shared between multiple walkers.  It
 *  accumulates all NUCompositeNets during the run and deletes them all
 *  at once at the end.
 */
static NUNetSet *mNetsToDelete = NULL;
/*! This static function is used by multiple walkers in this file.  It
 * accepts a NUCompositeNet and recursively schedule deletion of the net
 * and any subnets that are also NUCompositeNets.  If called with a
 * non-compositeexprvector net, only that net is scheduled for deletion.
 */
void
scheduleDeletion( NUNet *net )
{
  NUCompositeNet *cNet = net->getCompositeNet();
  if ( cNet != NULL )
  {
    const UInt32 size = cNet->getNumFields();
    for ( UInt32 i = 0; i < size; ++i )
    {
      NUCompositeNet *compSubnet = ( cNet->getField( i ))->getCompositeNet();
      if ( compSubnet )
        scheduleDeletion( compSubnet );
    }
  }
#if COMPRESYNTH_DEBUG || DIMRESYNTH_DEBUG
  UtIO::cout() << "Scheduling net for deletion:\n";
  net->print(1,1);
#endif
  mNetsToDelete->insert( net );
}


/*! Remove a NUCompositeNet from its declaration scope and delete it.
 *
 *  This will recursively remove any nested NUCNets.  It might try to
 *  remove the same net twice, or it might not.  For example, if there's
 *  a NUCNet a that contains NUCnet b, if there is a code reference to
 *  only 'a', the recursiveness will take care of deleting a.b.
 *  However, if there's a code reference to a.b too, then a.b will be
 *  deleted twice; once because a.b has been scheduled to be deleted and
 *  the second time because a.b is recursively deleted when a is.  The
 *  code below in the CDB block ensures that we don't delete something
 *  that is no longer there.  This is necessary because
 *  NUScope::removeLocal() has an assert to ensure that we delete
 *  exactly one net.
 */
static void
deleteNets(IODBNucleus *iodb)
{
  for ( NUNetSet::iterator p = mNetsToDelete->begin(); p != mNetsToDelete->end(); ++p )
  {
    NUNet *net = *p;
    NUScope *scope = net->getScope();
#if COMPRESYNTH_DEBUG || DIMRESYNTH_DEBUG
    UtIO::cout() << "Deleting NUNet(" << net << ") "
                 << net->getName()->str() << " from scope "
                 << scope->getPrintableName() << "(" << scope << ")" << "\n";
#endif
//    if ( !net->isPort( ))
    {
#if CDB
      // In debug mode we check to ensure we're not removing something
      // that's not actually there.  So, this makes sure that it's there
      // before trying to remove the net.
      for ( NUNetLoop loop = scope->loopLocals(); !loop.atEnd(); ++loop )
      {
        if ( *loop == net )
        {
          scope->removeLocal( net, true );
          break;
        }
      }
      
      if(net->isHierRef())
      {
	NUModule *module = scope->getModule();
	for (NUNetLoop loop = module->loopNetHierRefs(); !loop.atEnd(); ++loop )
        {
          if ( *loop == net )
          {
            module->removeNetHierRef( net, true);
	    break;
	  }
	}
      }
      
#else
      
      if(net->isHierRef())
      {
	NUModule *module = scope->getModule(); 
	module->removeNetHierRef( net, true);
      }
      else
      {
        scope->removeLocal( net, true );
      }
#endif

      // Make sure that this net is removed from the list of 
      // protected observable or mutable nets before it is deleted (bug 8909)
      net->removeIsProtectedMutableNonHierref(iodb);
      net->removeIsProtectedObservableNonHierref(iodb);
    }
    delete net;
  }
}

//! \class Resynthesizes
/*! arrayed record dimensions down onto their composite elements
 */
class CompositeDimCallback : public NuToNuFn {
public:
  ~CompositeDimCallback() {}
  CompositeDimCallback(IODBNucleus* iodb, MsgContext *msgContext) :
    mIODB(iodb), mMsgContext( msgContext )
  {}

  /*! Demote the dimensions onto the leaf nodes of the composite.  You
   * still end up with a NUCompositeNet, just with no dimensions.  The
   * old child nodes are scheduled to be deleted, and new multi-dim
   * jobbers fill the now-unarrayed composite net.
   */
  virtual NUNet* operator()(NUNet *node, Phase phase) {
    if ( isPre( phase )) {
      NUCompositeNet *cNet = node->getCompositeNet();
      if (cNet && !cNet->isResynthesized())
      {
#if DIMRESYNTH_DEBUG
        UtIO::cout() << "Demoting dimensions from composite net:\n";
        cNet->print(1,1);
#endif
        ConstantRangeArray rangeArray;
        const UInt32 size = cNet->getNumDims();
        for ( UInt32 i = 0; i < size; ++i ) {
          rangeArray.push_back( const_cast<ConstantRange*>( cNet->getRange( i )));
        }
        demoteRanges( &rangeArray, cNet );
        cNet->putResynthesized( true );
#if DIMRESYNTH_DEBUG
        UtIO::cout() << "The demoted composite net:\n";
        cNet->print(1,1);
#endif
      }
    }
    return NULL;
  }

private:
  void demoteRangesArrayCompHierNet(NUCompositeNetHierRef *cNet);
  void demoteRanges( ConstantRangeArray *rangeArray, NUCompositeNet *cNet );
  void handleNetReplacement(NUNet* origNet, NUNet* replNet);

  IODBNucleus* mIODB;
  MsgContext *mMsgContext;
};



void CompositeDimCallback::demoteRanges( ConstantRangeArray *rangeArray, NUCompositeNet *cNet )
{
  const UInt32 size = cNet->getNumFields();
  const UInt32 rangeSize = rangeArray->size();

  if (rangeSize >= 1 && cNet->isHierRef())
  {
    // if it's an array of records and it's hier ref
    demoteRangesArrayCompHierNet(dynamic_cast <NUCompositeNetHierRef *> (cNet));
    return;
  }
  for ( UInt32 i = 0; i < size; ++i )
  {
    // make a new array of the current set of ranges
    ConstantRangeArray subnetRanges;
    NUNet *newNet;

    NUVectorNet *vn = NULL;
    NUMemoryNet *mn = NULL;
    NUNet *subnet = cNet->getField(i);
    if ( subnet->isCompositeNet( ))
    {
      NUCompositeNet *cn = subnet->getCompositeNet();
      const UInt32 subSize = cn->getNumDims();
      // Start with any new dimensions for the sub-cNet
      for ( UInt32 i = 0; i < subSize; ++i ) {
        subnetRanges.push_back( new ConstantRange( *cn->getRange( i )));
      }
      // Tack on the dimensions we already have
      for ( UInt32 j = 0; j < rangeSize; ++j )
        subnetRanges.push_back( new ConstantRange( *((*rangeArray)[j] )));
      demoteRanges( &subnetRanges, cn );
      // Now, take them back off and delete them
      const UInt32 allDims = subnetRanges.size();
      for ( UInt32 j = 0; j < allDims; ++j )
        delete subnetRanges[j];
      subnetRanges.clear();
    }
    else if (rangeSize == 0)
    {
      // Do nothing as there are no ranges to demote to 
      // non composite fields. Continue looking for composite fields
      // with ranges.
      continue;
    }
    else if ( subnet->isVectorNet( ))
    {
      vn = subnet->castVectorNet();
      subnetRanges.push_back( new ConstantRange( *vn->getRange( )));
    }
    else if ( subnet->isMemoryNet( ))
    {
      mn = subnet->getMemoryNet();
      const UInt32 memDims = mn->getNumDims();
      for ( UInt32 i = 0; i < memDims; ++i ) {
        subnetRanges.push_back( new ConstantRange( *mn->getRange( i )));
      }
    }
    else if ( subnet->isBitNet( ))
      ; // do nothing
    else
    {
      NU_ASSERT( "Unexpected net type in demoteRanges" == NULL, subnet );
    }

    if ( !subnet->isCompositeNet( ))
    {
      for ( UInt32 j = 0; j < rangeSize; ++j )
        subnetRanges.push_back( new ConstantRange( *((*rangeArray)[j] )));

      // We have to change the name of the old, to-be-deleted net, so it
      // doesn't collide with the new net.
      NUScope *scope = subnet->getScope();
      StringAtom *nameAtom = subnet->getName();
      StringAtom *oldName = scope->gensym( "old", *nameAtom );
      subnet->changeName( oldName );
      scheduleDeletion( subnet );

      // allocate a new net based on the value of newDims
      if ( subnetRanges.size() == 1 )
      {
        // Alloc a vector net; we must be coming from a bitnet and adding 1 dimension.
        newNet = new NUVectorNet( nameAtom, *subnetRanges[0], subnet->getFlags(),
                                  VectorNetFlags(0), scope, subnet->getLoc( ));
        delete subnetRanges[0];
      }
      else
      {
        VectorNetFlags vflags = vn?vn->getVectorNetFlags():(mn?mn->getVectorNetFlags():eNoneVectorNet);
        NetFlags flags = subnet->getFlags();
        if ( flags & eDMWireNet )
          flags = NetFlags( flags | eDMWire2DNet );
        else if ( flags & eDMRegNet )
          flags = NetFlags( flags | eDMReg2DNet );
        else
          NU_ASSERT( "Net not reg or wire in demoteRanges!", subnet );

        // Alloc a Memory Net, only first dimension is packed
        newNet = new NUMemoryNet( nameAtom, 1, &subnetRanges, flags,
                                  vflags, scope, subnet->getLoc( ));
      }
      handleNetReplacement(subnet, newNet);
      scope->addLocal( newNet );
      cNet->putField( newNet, i );
    }
  }
  // Remove the range indices; they've been demoted.
  cNet->clearRanges();
}


// This function is used for demoting the composite net for array of records and the cNet is
// hier reference.
// This is most complicated case. In this case we have to delete the previous subnets in the cNet
// and recreate the hier ref of subnets, because the resolution nets of them were removed during 
// demotion of the cNet resolution net.
void CompositeDimCallback::demoteRangesArrayCompHierNet(NUCompositeNetHierRef *cNet)
{
  NUNetHierRef *hier_ref = cNet->getHierRef();

  NUNetHierRef::NetVectorLoop loopRes = hier_ref->loopResolutions();
  NU_ASSERT(hier_ref->resolutionCount() == 1, cNet);
  
  NUCompositeNet *compNet = dynamic_cast <NUCompositeNet *> (*loopRes);
  

  const UInt32 numFields1 = cNet->getNumFields();
  const UInt32 numFields2 = compNet->getNumFields();

  NU_ASSERT(numFields1 == numFields2, cNet);
  NUModule* module = cNet->getScope()->getModule();

  // Now replace the subnets
  for(UInt32 i = 0; i < numFields2; i++)
  {
    NUNet *subnet = compNet->getField(i);
    if ( subnet->isCompositeNet( ))
    {
      NUNet *subnetHR = cNet->getField(i);
      demoteRangesArrayCompHierNet(dynamic_cast <NUCompositeNetHierRef *> (subnetHR));
    }
    else
    {
      NUNet *subnetOrig = cNet->getField(i);
      
      // We have to change the name of the old, to-be-deleted net, so it
      // doesn't collide with the new net.
      NUScope *scope = subnetOrig->getScope();
      StringAtom *nameAtom = subnetOrig->getName();
      StringAtom *oldName = scope->gensym( "old", *nameAtom );
      subnetOrig->changeName( oldName );
      scheduleDeletion( subnetOrig );
      module->removeNetHierRef(subnetOrig, false);
      
      NUNetHierRef *hierRefSubNet = subnetOrig->getHierRef();
      AtomArray pathHierName = hierRefSubNet->getPath();
      NUNet *hierRefFieldNet = subnet->buildHierRef(module, pathHierName);
      cNet->putField( hierRefFieldNet, i );
      cNet->clearRanges();

      handleNetReplacement(subnetOrig, hierRefFieldNet);
    }
  }

}

void CompositeDimCallback::handleNetReplacement(NUNet* origNet, NUNet* replNet)
{
  // When the resynthesized away net is protected mutable, the newly created
  // net ought to be marked protected mutable too. This avoids it from 
  // being resynthesized any further.
  if (origNet->isProtectedMutableNonHierref(mIODB)) {
    replNet->putIsProtectedMutableNonHierref(mIODB);
  }
  if (origNet->isProtectedObservableNonHierref(mIODB)) {
    replNet->putIsProtectedObservableNonHierref(mIODB);
  }
}

//! Resynthesize idents of composites into a concat of their elements
class CompositeIdentCallback : public NuToNuFn {
public:
  ~CompositeIdentCallback() {}

  CompositeIdentCallback( MsgContext *msgContext ) : mMsgContext( msgContext ) {}

  //! Resynthesize a NUIdentLvalue of a composite net into a concat
  virtual NULvalue* operator()(NULvalue *node, Phase phase) {
    if ( isPre( phase ))
    {
      const NUType nType = node->getType();
      if(( nType == eNUIdentLvalue || nType == eNUCompositeIdentLvalue ) &&
         node->isWholeIdentifier()) {
        NUNet *ident = node->getWholeIdentifier();
        NU_ASSERT( ident, node );
        if ( ident->isCompositeNet() )
        {
          NUCompositeNet *net = ident->getCompositeNet();
          // Make into a concat
          NULvalueVector concatVector;
          net->createLvalueVector( &concatVector );
          NUCompositeLvalue *concat = new NUCompositeLvalue( concatVector, node->getLoc() );

          scheduleDeletion( net );

#if COMPRESYNTH_DEBUG
          UtIO::cout() << "CompositeResynth old identLvalue:\n";
          node->print(1,1);
          UtIO::cout() << "CompositeResynth new concat:\n";
          concat->print(1,1);
#endif
          delete node;
          return concat;
        }
      }
      else if ( nType == eNUCompositeSelLvalue )
      {
#if COMPRESYNTH_DEBUG
        UtIO::cout() << "Resynthesizing arrayed record lvalue:\n";
        node->print(1,1);
#endif
        NUCompositeSelLvalue *csLval = dynamic_cast<NUCompositeSelLvalue*>( node );
        NUCompositeInterfaceLvalue *cLvalue = csLval->getIdentExpr();

        if (cLvalue->getType() == eNUCompositeSelLvalue)
        {
          // The node is a part select of a select. Dive down to get the identifier.
          NUCompositeSelLvalue* sel_lval = dynamic_cast<NUCompositeSelLvalue*>(cLvalue);
          cLvalue = sel_lval->getIdentExpr();
        }

        // Collect all selects from parents for demotion to field(s).
        NUExprVector selVector;
        csLval->hasSelects(&selVector);
        NULvalue* newLvalue = NULL;
        NU_ASSERT((cLvalue->getType() == eNUCompositeFieldLvalue) ||
                  (cLvalue->getType() == eNUCompositeIdentLvalue), node);

        NUNet *net = cLvalue->getNet();
        const SourceLocator loc = node->getLoc();
        
        if (net->isCompositeNet())
        {
          // This is a composite net. Demote selects to each field of
          // the composite and create vector/memory for each one of them.
          // Create a composite concat of fields, since it represents a record
          // and it helps us identify record concats for further synthesis.
          newLvalue = createCompositeLvalConcat(&selVector, net->getCompositeNet(),
                                                csLval, loc);
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Created concat for arrayed record lvalue:\n";
          newLvalue->print(1,1);
#endif
        }
        else
        {
          // This has to be a field net since the net is not composite.
          NU_ASSERT(cLvalue->getType() == eNUCompositeFieldLvalue, node);
          // Demote selects to the given field, turning the field into
          // a vector or memory. A simple concat is sufficient since
          // it represents a record field that needs no further synthesis.
          NULvalueVector concatVector;
          createFieldLvalConcatVec(&selVector, net, csLval,
                                   loc, &concatVector);

          // Create a composite concat of the composite concats we've generated
          if (concatVector.size() > 1) {
            // A composite field to multi-address memory lvalue conversion has
            // happened. Concat them.
            newLvalue = new NUConcatLvalue(concatVector, loc);
          } else {
            // A composite field to vector/memory select/part-select
            // conversion has happened.
            newLvalue = concatVector.back();
          }
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Created memsel for arrayed record lvalue:\n";
          newLvalue->print(1,1);
#endif
        }

        newLvalue->resize();
        delete node;
        return newLvalue;
      }
      else if ( nType == eNUCompositeFieldLvalue )
      {
#if COMPRESYNTH_DEBUG
        UtIO::cout() << "Resynthesizing field reference lvalue:\n";
        node->print(1,1);
#endif
        NUCompositeFieldLvalue *cField = dynamic_cast<NUCompositeFieldLvalue*>( node );
        NUExprVector selVector, copyVector;
        cField->hasSelects(&selVector);
        CopyContext cc(0,0);
        const SourceLocator loc = node->getLoc();
        const UInt32 size = selVector.size();
        for ( UInt32 i = 0; i < size; ++i )
          copyVector.push_back( selVector[i]->copy( cc ));

        NUNet *net = cField->getNet();
        NULvalue *newLvalue;
        if ((size == 0) && !net->isCompositeNet())
        {
          newLvalue = new NUIdentLvalue(net, loc);
        }
        else if ( net->isMemoryNet( ))
        {
          NUMemoryNet* mn = net->getMemoryNet();
          if ( mn->getNumDims() == copyVector.size() )
          {
            // We need a varsel of a memsel here, where the varsel expr
            // is the last element in copyVector.
            NUExpr *varsel_expr = copyVector.back();
            copyVector.pop_back();
            NULvalue *memsel = new NUMemselLvalue( net, &copyVector, loc );
            newLvalue = Populate::genVarselLvalue(net, memsel, varsel_expr, loc);
          }
          else
          {
            // Create a NUMemselLvalue
            newLvalue = new NUMemselLvalue( net, &copyVector, loc );
          }
        }
        else if ( net->isVectorNet( ))
        {
          // Create a NUVarselLvalue
          NU_ASSERT( size == 1, node );
          NUIdentLvalue *ident = new NUIdentLvalue( net, loc );
          newLvalue = Populate::genVarselLvalue(net, ident, copyVector[0], loc);
        }
        else if ( net->isCompositeNet( ))
        {
 	  newLvalue = createCompositeLvalConcat(&copyVector, net->getCompositeNet(),
                                                NULL, loc);
          // Copies of expressions in copyVector have been used.
          deleteExprVectorElements(copyVector);
        }
        else
        {
          newLvalue = NULL;
          // None of the expressions in copyVector have been used.
          deleteExprVectorElements(copyVector);
        }
        NU_ASSERT( newLvalue, node );
#if COMPRESYNTH_DEBUG
        UtIO::cout() << "Created net select for field reference lvalue:\n";
        newLvalue->print(1,1);
#endif
        delete node;
        return newLvalue;
      }
    }
    return NULL;
  }

  //! Resynthesize a NUComposite*Expr into a concat
  virtual NUExpr* operator()(NUExpr *node, Phase phase) {
    if ( isPre( phase ))
    {
      const NUExpr::Type nType = node->getType();
      if (( nType == NUExpr::eNUIdentRvalue || nType == NUExpr::eNUCompositeIdentRvalue ) &&
          node->isWholeIdentifier()) {
        NUNet *ident = node->getWholeIdentifier();
        NU_ASSERT( ident, node );
        if ( ident->isCompositeNet() )
        {
          NUCompositeNet *net = ident->getCompositeNet();
          // Make into a concat
          NUExprVector concatVector;
          net->createExprVector( &concatVector );
          NUCompositeExpr *concat = new NUCompositeExpr( concatVector, node->getLoc());
          concat->resize( concat->determineBitSize() );

          scheduleDeletion( net );

#if COMPRESYNTH_DEBUG
          UtIO::cout() << "CompositeResynth old identRvalue:\n";
          node->print(1,1);
          UtIO::cout() << "CompositeResynth new concat:\n";
          concat->print(1,1);
#endif
          delete node;
          return concat;
        }
      }
      else if ( nType == NUExpr::eNUCompositeSelExpr )
      {
#if COMPRESYNTH_DEBUG
        UtIO::cout() << "Resynthesizing arrayed record expr:\n";
        node->print(1,1);
#endif
        NUCompositeSelExpr *csExpr = dynamic_cast<NUCompositeSelExpr*>( node );
        NUCompositeInterfaceExpr *cExpr = csExpr->getIdentExpr();

        if (cExpr->getType() == NUExpr::eNUCompositeSelExpr)
        {
          // The node is a part select of a select. Dive down to get the identifier.
          NUCompositeSelExpr* sel_expr = dynamic_cast<NUCompositeSelExpr*>(cExpr);
          cExpr = sel_expr->getIdentExpr();
        }

        // Collect all selects from parents for demotion to field(s).
        NUExprVector selVector;
        csExpr->hasSelects(&selVector);
        NUExpr* newExpr = NULL;
        NU_ASSERT( (cExpr->getType() == NUExpr::eNUCompositeIdentRvalue) ||
                   (cExpr->getType() == NUExpr::eNUCompositeFieldExpr), node );

        NUNet *net = cExpr->getNet();
        const SourceLocator loc = node->getLoc();
        
        if (net->isCompositeNet())
        {
          // This is a composite net. Demote selects to each field of
          // the composite and create vector/memory for each one of them.
          // Create a composite concat of fields, since it represents a record
          // and it helps us identify record concats for further synthesis.
          newExpr = createCompositeConcatExpr(&selVector, net->getCompositeNet(),
                                              csExpr, loc);
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Created composite concat for arrayed record expr:\n";
          newExpr->print(1,1);
#endif
        }
        else
        {
          // This has to be a field expression since the net is not a composite.
          NU_ASSERT(cExpr->getType() == NUExpr::eNUCompositeFieldExpr, node);
          // Demote selects to the given field, turning the field into
          // a vector or memory. A simple concat is sufficient since
          // it represents a record field that needs no further synthesis.
          NUExprVector concatVector;
          createFieldConcatExprVec(&selVector, net, csExpr,
                                   loc, &concatVector);

          // Create a composite concat of the composite concats we've generated
          if (concatVector.size() > 1) {
            // A composite field to multi-address memory rvalue conversion has
            // happened. Concat them.
            newExpr = new NUConcatOp(concatVector, 1, node->getLoc());
          } else {
            // A composite field to vector/memory select/part-select
            // conversion has happened.
            newExpr = concatVector.back();
          }
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Created memsel for arrayed record expr:\n";
          newExpr->print(1,1);
#endif
        }

        // We created a newExpr expression, which is going to replace 
        // node expr. The sign of the new expr should match the sign of 
        // the original expr. (see bug 12902)
        if(node->isSignedResult())
        {
          // We don't populate the sign of composite expression (default is unsigned).
          // It's only populated when functions, like SZT are used.
          // If our resynthesis dropped the sign, reset it.
          newExpr->setSignedResult(node->isSignedResult());
        }

        newExpr->resize(newExpr->determineBitSize());
        delete node;
        return newExpr;
      }
      else if ( nType == NUExpr::eNUCompositeFieldExpr )
      {
#if COMPRESYNTH_DEBUG
        UtIO::cout() << "Resynthesizing field reference expr:\n";
        node->print(1,1);
#endif
        NUCompositeFieldExpr *cField = dynamic_cast<NUCompositeFieldExpr*>( node );
        NUExprVector selVector, copyVector;
        cField->hasSelects(&selVector);
        CopyContext cc(0,0);
        const UInt32 size = selVector.size();
        for ( UInt32 i = 0; i < size; ++i )
          copyVector.push_back( selVector[i]->copy( cc ));

        NUNet* net = cField->getNet();
        NUExpr *newExpr;
        const SourceLocator loc = node->getLoc();
        if ((size == 0) && !net->isCompositeNet())
        {
          newExpr = new NUIdentRvalue(net, loc);
        }
        else if ( net->isMemoryNet( ))
        {
          NUMemoryNet* mn = net->getMemoryNet();
          if ( mn->getNumDims() == copyVector.size() )
          {
            // We need a varsel of a memsel here, where the varsel expr
            // is the last element in copyVector.
            NUExpr *varsel_expr = copyVector.back();
            copyVector.pop_back();
            NUExpr *memsel = new NUMemselRvalue( net, &copyVector, loc );
            newExpr = Populate::genVarselRvalue(net, memsel, varsel_expr, loc);
          }
          else
          {
            // Create a NUMemselRvalue
            newExpr = new NUMemselRvalue( net, &copyVector, loc );
          }
        }
        else if ( net->isVectorNet( ))
        {
          // Create a NUVarselRvalue
          NU_ASSERT( size < 2, node );
	  NUIdentRvalue *ident = new NUIdentRvalue( net, loc );
	  if(size)
            newExpr = Populate::genVarselRvalue(net, ident, copyVector[0], loc);
	  else
	    newExpr = ident;

        }
        else if ( net->isBitNet() )
        {
          // Create a NUIdentRvalue.
          NU_ASSERT(size == 0, node);
          newExpr = new NUIdentRvalue(net, loc);
        }
        else if ( net->isCompositeNet( ))
        {
 	  newExpr = createCompositeConcatExpr(&copyVector, net->getCompositeNet(),
                                              NULL, loc);
          // Copies of expressions in copyVector have been used.
          deleteExprVectorElements(copyVector);
        }
        else
        {
          newExpr = NULL;
        }
        NU_ASSERT( newExpr, node );

        // We created a newExpr expression, which is going to replace 
        // node expr. The sign of the new expr should match the sign of 
        // the original expr. (see bug 12902)
        if(node->isSignedResult())
        {
          // We don't populate the sign of composite expression (default is unsigned).
          // It's only populated when functions, like SZT are used.
          // If our resynthesis dropped the sign, reset it.
          newExpr->setSignedResult(node->isSignedResult());
        }

        newExpr->resize( newExpr->determineBitSize());
#if COMPRESYNTH_DEBUG
        UtIO::cout() << "Created net select for field reference expr:\n";
        newExpr->print(1,1);
#endif
        delete node;
        return newExpr;
      }
    }
    return NULL;
  }

private:

  NUConcatLvalue* createCompositeLvalConcat(NUExprVector *selVector,
                                            NUCompositeNet *cObj,
                                            NUCompositeSelLvalue* csLval,
                                            const SourceLocator& loc)
  {
    NULvalueVector lvalues;
    createLvalConcatVec(selVector, cObj, csLval, loc, &lvalues);

    NUCompositeLvalue* newConcat = new NUCompositeLvalue(lvalues, loc);
    return newConcat;
  }

  void createLvalConcatVec(NUExprVector *selVector, NUCompositeNet *cObj,
                           NUCompositeSelLvalue* csLval, const SourceLocator& loc,
                           NULvalueVector* lvalues)
  {
    const UInt32 numFields = cObj->getNumFields();
    for ( UInt32 i = 0; i < numFields; ++i )
    {
      NUNet *subnet = cObj->getField( i );
      createFieldLvalConcatVec(selVector, subnet, csLval, loc, lvalues);
    }
  }

  void createFieldLvalConcatVec(NUExprVector *selVector, NUNet* subnet,
                                NUCompositeSelLvalue* csLval,
                                const SourceLocator& loc,
                                NULvalueVector* lvalues)
  {
    CopyContext cc(0,0);

    if (subnet->isCompositeNet())
    {
      // Composite element in a composite. Get the composite element's elements.
      NUCompositeNet *cn = subnet->getCompositeNet();
      NUConcatLvalue *subConcat = createCompositeLvalConcat(selVector, cn,
                                                            csLval, loc);
      lvalues->push_back( subConcat );
    }
    else if (subnet->isVectorNet())
    {
      // Composite element is a vector. Create a varsel with given
      // selects and range.
      NU_ASSERT(selVector->size() == 1, subnet);
      NUVarselLvalue* newVarsel = 
        createVarselLvalue(csLval, subnet->castVectorNet(),
                           (*selVector)[0]->copy(cc), loc);
      lvalues->push_back( newVarsel );
    }
    else if (subnet->isMemoryNet())
    {
      // Composite element is a memory.
      bool mem_bit_sel = false;
      if (selVector->size() == subnet->getMemoryNet()->getNumDims()) {
        mem_bit_sel = true; // This is a varsel of a memsel.
      }

      if ((csLval != NULL) && csLval->isPartSelect() && !mem_bit_sel)
      {
        const ConstantRange range = csLval->getPartSelRange();
        // This is a select for memory address range. Insert one address
        // at a time into the vector.
        NULvalueVector lvals_for_concat;
        const SInt32 incr = (range.getMsb() > range.getLsb())?-1:1;
        for (SInt32 i = range.getMsb(); range.contains(i); i += incr)
        {
          NUExprVector exprCopies;
          const UInt32 numExprs = selVector->size();
          for ( UInt32 j = 0; j < numExprs; ++j )
            exprCopies.push_back( ((*selVector)[j])->copy( cc ));
          NUExpr* varsel_expr = exprCopies.back();
          exprCopies.pop_back();
          // Construct varsel_expr + i expression.
          NUExpr *index_expr = NUConst::create (false, i, 32, loc);
          NUExpr* sel_expr = new NUBinaryOp(NUOp::eBiPlus, varsel_expr,
                                            index_expr, loc);
          exprCopies.push_back(sel_expr);
          NUMemselLvalue *newMemsel = new NUMemselLvalue(subnet, &exprCopies, loc);
          //lvalues->push_back(newMemsel);
          lvals_for_concat.push_back(newMemsel);
        }
        if (lvals_for_concat.size() == 1) {
          lvalues->push_back(*(lvals_for_concat.begin()));
        } else {
          NUConcatLvalue* concatLval = new NUConcatLvalue(lvals_for_concat, loc);
          lvalues->push_back(concatLval);
        }
      }
      else
      {
        // The select either selects one memory address (if range is NULL and
        // mem_bit_sel is false), or selects bit slice of one memory address
        // (if range is not NULL and mem_bit_sel is true), or selects a
        // bit of one memory address (if range is NULL and mem_bit_sel is true).
        NUExprVector exprCopies;
        const UInt32 numExprs = selVector->size();
        for ( UInt32 j = 0; j < numExprs; ++j )
          exprCopies.push_back( ((*selVector)[j])->copy( cc ));

        NUExpr* varsel_expr = NULL;
        if (mem_bit_sel)
        {
          varsel_expr = exprCopies.back();
          exprCopies.pop_back();
        }
        NUMemselLvalue *newMemsel = new NUMemselLvalue(subnet, &exprCopies, loc);
        if (mem_bit_sel)
        {
          NUVarselLvalue* newVarsel = 
            createVarselLvalue(csLval, subnet->getMemoryNet(), newMemsel, varsel_expr, loc);
          lvalues->push_back(newVarsel);
        }
        else
        {
          // Selects one memory address.
          lvalues->push_back(newMemsel);
        }
      }
    } // else if
    else
    {
      // This includes bitnets, since the net has already been adjusted
      NU_ASSERT( "Unexpected net type in createLvalConcat" == NULL, subnet );
    }
  }

  // Varsel of a net.
  NUVarselLvalue* createVarselLvalue(NUCompositeSelLvalue* csLval,
                                     NUVectorNet* subnet, NUExpr* selExpr,
                                     const SourceLocator& loc)
  {
    NUVarselLvalue* newVarsel = NULL;
    if ((csLval != NULL) && csLval->isPartSelect())
    {
      ConstantRange cr = csLval->getPartSelRange();
      if (csLval->isPartSelectIndexDummy())
      {
        // Part-select range is not normalized.
        ConstantRange nr = *(subnet->getDeclaredRange());
        cr.normalize(&nr, false);
        // A part-select on a net like x(1 to 3)
        newVarsel = new NUVarselLvalue(subnet, cr, loc);
        delete selExpr; // Unused dummy
      }
      else
      {
        // A part-select on a net with variable offset like x(y+1 to y+3).
        // Part-select range is already normalized.
        newVarsel = new NUVarselLvalue(subnet, selExpr, cr, loc);
      }
    }
    else
    {
      // A bit-select on a net with constant/variable index like x(y) or x(3)
      newVarsel = Populate::genVarselLvalue(subnet, selExpr, loc);
    }
    return newVarsel;
  }

  // Varsel of a memsel.
  NUVarselLvalue* createVarselLvalue(NUCompositeSelLvalue* csLval, NUMemoryNet* subnet,
                                     NULvalue* memsel, NUExpr* selExpr,
                                     const SourceLocator& loc)
  {
    NUVarselLvalue* newVarsel = NULL;
    if ((csLval != NULL) && csLval->isPartSelect())
    {
      ConstantRange cr = csLval->getPartSelRange();
      if (csLval->isPartSelectIndexDummy())
      {
        // Part-select range is not normalized.
        ConstantRange nr = *(subnet->getRange(0));
        cr.normalize(&nr, false);
        // A part-select on a memory select like x(12)(1 to 3)
        newVarsel = new NUVarselLvalue(memsel, cr, loc);
        delete selExpr; // Unused dummy
      }
      else
      {
        // A part-select on a memory select with variable offset
        // like x(12)(y+1 to y+3)
        // Part-select range is already normalized.
        newVarsel = new NUVarselLvalue(memsel, selExpr, cr, loc);
      }
    }
    else
    {
      // A bit-select on a memory select with constant/variable index
      // like x(12)(y) or x(12)(3)
      newVarsel = Populate::genVarselLvalue(subnet, memsel, selExpr, loc);
    }
    return newVarsel;
  }

  // Create a composite concat expression for given vector of select
  // expressions and composite net.
  NUCompositeExpr* createCompositeConcatExpr(NUExprVector *selVector,
                                             NUCompositeNet *cNet,
                                             NUCompositeSelExpr* csExpr,
                                             const SourceLocator& loc)
  {
    NUExprVector exprs;
    createConcatExprVec(selVector, cNet, csExpr, loc, &exprs);

    NUCompositeExpr* newConcat = new NUCompositeExpr(exprs, loc);
    newConcat->resize(newConcat->determineBitSize());
    return newConcat;
  }

  // For the given composite net and select expression vector, construct
  // a vector of varsels.
  void createConcatExprVec(NUExprVector *selVector, NUCompositeNet *cNet,
                           NUCompositeSelExpr* csExpr, const SourceLocator& loc,
                           NUExprVector* exprs)
  {
    const UInt32 numFields = cNet->getNumFields();
    for ( UInt32 i = 0; i < numFields; ++i )
    {
      NUNet *subnet = cNet->getField( i );
      createFieldConcatExprVec(selVector, subnet, csExpr, loc, exprs);
    }
  }

  void createFieldConcatExprVec(NUExprVector *selVector, NUNet* subnet,
                                NUCompositeSelExpr* csExpr, const SourceLocator& loc,
                                NUExprVector* exprs)
  {
    CopyContext cc(0,0);

    if (subnet->isCompositeNet())
    {
      // Composite element in a composite. Get the composite element's elements.
      NUCompositeNet *cn = subnet->getCompositeNet();
      NUCompositeExpr *subConcat = createCompositeConcatExpr(selVector, cn, csExpr, loc);
      exprs->push_back( subConcat );
    }
    else if (subnet->isVectorNet())
    {
      // Composite element is a vector. Create a varsel with given
      // selects and range.
      NU_ASSERT(selVector->size() == 1, subnet);
      NUVarselRvalue* newVarsel = 
        createVarselRvalue(csExpr, subnet->castVectorNet(),
                           (*selVector)[0]->copy(cc), loc);
      exprs->push_back( newVarsel );
    }
    else if (subnet->isMemoryNet())
    {
      // Composite element is a memory.
      bool mem_bit_sel = false;
      if (selVector->size() == subnet->getMemoryNet()->getNumDims()) {
        mem_bit_sel = true; // This is a varsel of a memsel.
      }

      if ((csExpr != NULL) && csExpr->isPartSelect() && !mem_bit_sel)
      {
        const ConstantRange range = csExpr->getPartSelRange();
        // This is a select for memory address range. Insert one address
        // at a time into the vector.
        NUExprVector exprs_for_concat;
        const SInt32 incr = (range.getMsb() > range.getLsb())?-1:1;
        for (SInt32 i = range.getMsb(); range.contains(i); i += incr)
        {
          NUExprVector exprCopies;
          const UInt32 numExprs = selVector->size();
          for ( UInt32 j = 0; j < numExprs; ++j )
            exprCopies.push_back( ((*selVector)[j])->copy( cc ));
          NUExpr* varsel_expr = exprCopies.back();
          exprCopies.pop_back();
          // Construct varsel_expr + i expression.
          NUExpr *index_expr = NUConst::create (false, i, 32, loc);
          NUExpr* sel_expr = new NUBinaryOp(NUOp::eBiPlus, varsel_expr,
                                            index_expr, loc);
          exprCopies.push_back(sel_expr);
          NUMemselRvalue *newMemsel = new NUMemselRvalue(subnet, &exprCopies, loc);
          newMemsel->resize(newMemsel->determineBitSize());
          exprs_for_concat.push_back(newMemsel);
          //exprs->push_back(newMemsel);
        }
        if (exprs_for_concat.size() == 1) {
          exprs->push_back(*(exprs_for_concat.begin()));
        } else {
          NUConcatOp* concatExpr = new NUConcatOp(exprs_for_concat, 1, loc);
          exprs->push_back(concatExpr);
        }
      }
      else
      {
        // The select either selects one memory address (if range is NULL and
        // mem_bit_sel is false), or selects bit slice of one memory address
        // (if range is not NULL and mem_bit_sel is true), or selects a
        // bit of one memory address (if range is NULL and mem_bit_sel is true).
        NUExprVector exprCopies;
        const UInt32 numExprs = selVector->size();
        for ( UInt32 j = 0; j < numExprs; ++j )
          exprCopies.push_back( ((*selVector)[j])->copy( cc ));

        NUExpr* varsel_expr = NULL;
        if (mem_bit_sel)
        {
          varsel_expr = exprCopies.back();
          exprCopies.pop_back();
        }
        NUMemselRvalue *newMemsel = new NUMemselRvalue(subnet, &exprCopies, loc);
        newMemsel->resize(newMemsel->determineBitSize());
        if (mem_bit_sel)
        {
          NUVarselRvalue* newVarsel = 
            createVarselRvalue(csExpr, subnet->getMemoryNet(), newMemsel, varsel_expr, loc);
          exprs->push_back( newVarsel );
        }
        else
        {
          // Selects one memory address.
          exprs->push_back( newMemsel );
        }
      }
    }
    else
    {
      // This includes bitnets, since the net has already been adjusted
      UtString msg("Unsupported object type: ");
      msg << subnet->typeStr();
      msg << " found in createConcatExpr context";
      mMsgContext->CmdLineNote(msg.c_str()); // use CmdLineNote because it seems to be the only msg that just prints text
      NU_ASSERT( ("Unexpected net type in createConcatExpr" == NULL), subnet );
    }
  }

  // Varsel of a net.
  NUVarselRvalue* createVarselRvalue(NUCompositeSelExpr* csExpr,
                                     NUVectorNet* subnet, NUExpr* selExpr,
                                     const SourceLocator& loc)
  {
    NUVarselRvalue* newVarsel = NULL;
    if ((csExpr != NULL) && csExpr->isPartSelect())
    {
      ConstantRange cr = csExpr->getPartSelRange();
      if (csExpr->isPartSelectIndexDummy())
      {
        // Part-select range is not normalized.
        ConstantRange nr = *(subnet->getDeclaredRange());
        cr.normalize(&nr, false);
        // A part-select on a net like x(1 to 3)
        newVarsel = new NUVarselRvalue(subnet, cr, loc);
        delete selExpr; // Unused dummy
      }
      else
      {
        // A part-select on a net with variable offset like x(y+1 to y+3).
        // Part-select range is already normalized.
        newVarsel = new NUVarselRvalue(subnet, selExpr, cr, loc);
      }
    }
    else
    {
      // A bit-select on a net with constant/variable index like x(y) or x(3)
      newVarsel = Populate::genVarselRvalue(subnet, selExpr, loc);
    }
    return newVarsel;
  }

  // Varsel of a memsel.
  NUVarselRvalue* createVarselRvalue(NUCompositeSelExpr* csExpr, NUMemoryNet* subnet,
                                     NUExpr* memsel, NUExpr* selExpr,
                                     const SourceLocator& loc)
  {
    NUVarselRvalue* newVarsel = NULL;
    if ((csExpr != NULL) && csExpr->isPartSelect())
    {
      ConstantRange cr = csExpr->getPartSelRange();
      if (csExpr->isPartSelectIndexDummy())
      {
        // Part-select range is not normalized.
        ConstantRange nr = *(subnet->getRange(0));
        cr.normalize(&nr, false);
        // A part-select on a memory select like x(12)(1 to 3)
        newVarsel = new NUVarselRvalue(memsel, cr, loc);
        delete selExpr; // Unused dummy
      }
      else
      {
        // A part-select on a memory select with variable offset
        // like x(12)(y+1 to y+3)
        // Part-select range is already normalized.
        newVarsel = new NUVarselRvalue(memsel, selExpr, cr, loc);
      }
    }
    else
    {
      // A bit-select on a memory select with constant/variable index
      // like x(12)(y) or x(12)(3)
      newVarsel = Populate::genVarselRvalue(subnet, memsel, selExpr, loc);
    }
    return newVarsel;
  }

  void deleteExprVectorElements(NUExprVector& selVector)
  {
    for (NUExprLoop exprLoop(selVector); !exprLoop.atEnd(); ++exprLoop) {
      NUExpr* expr = *exprLoop;
      delete expr;
    }
  }

private:
  MsgContext *mMsgContext;
};


class CompositeConcatCallback : public NuToNuFn {
public:
  CompositeConcatCallback( MsgContext *msgContext ) : mMsgContext( msgContext ) {}

  //! Resynthesize a NUIdentLvalue of a composite net into a concat
  virtual NULvalue* operator()(NULvalue *node, Phase phase) {
    if ( phase == ePre && node->getType() == eNUCompositeLvalue )
    {
      NULvalueVector concatVector;
      createLvalueVector( node, &concatVector, mMsgContext );
      NUConcatLvalue *concat = new NUConcatLvalue( concatVector, node->getLoc( ));
      NU_ASSERT( concat, node );
      concat->resize();
#if COMPRESYNTH_DEBUG
      UtIO::cout() << "CompositeResynth old compositeLvalue:\n";
      node->print(1,1);
      UtIO::cout() << "CompositeResynth new concat lvalue:\n";
      concat->print(1,1);
#endif
      delete node;
      return concat;
    }
    return NULL;
  }

  //! Resynthesize a NUIdentRvalue of a composite net into a concat
  virtual NUExpr* operator()(NUExpr *node, Phase phase) {
    if ( phase == ePre )
    {
      const NUExpr::Type exprType = node->getType();
      if ( exprType == NUExpr::eNUCompositeExpr )
      {
        NUExprVector concatVector;
        createRvalueVector( node, &concatVector, mMsgContext );
        NUConcatOp *concat = new NUConcatOp( concatVector, 1, node->getLoc( ));
        NU_ASSERT( concat, node );
        concat->resize( concat->determineBitSize() );
#if COMPRESYNTH_DEBUG
        UtIO::cout() << "CompositeResynth old compositeExpr:\n";
        node->print(1,1);
        UtIO::cout() << "CompositeResynth new concat expr:\n";
        concat->print(1,1);
#endif
        delete node;
        return concat;
      }
    }
    return NULL;
  }

private:
  MsgContext *mMsgContext;
};


/*!  The CompositeResynthCallback class is the last phase of composite
resynthesis.  All other references to a composite net should be gone by
this point.  This phase removes the net from its local scope and deletes
the NUNet.
*/
class CompositeResynthCallback : public NUDesignCallback {
public:
  CompositeResynthCallback( MsgContext *msgContext )
    : mMsgContext( msgContext )
  {}

  ~CompositeResynthCallback() {}

  //! By default, walk through everything.
  Status operator() (Phase , NUBase * ) { return eNormal; }

  Status operator()(Phase phase, NUNet* id) {
    if ( phase == ePre ) {
      NUCompositeNet *net = id->getCompositeNet();
      if ( net )
      {
        scheduleDeletion( net );
      }
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUModule* module) {
    if ( phase == ePre ) {
      // Expand the mPorts member of the module by element-blasting
      // CompositeNet ports into their fields.
      SInt32 i = 0;
      while ( i < module->getNumPorts() )
      {
        NUNet *port = module->getPortByIndex( i );
        if ( port->isCompositeNet() )
        {
          const StringAtom *recAtom = port->getName();
          NUNetVector recPorts;
          NUCompositeNet *cport = port->getCompositeNet();
          cport->createNetVector( &recPorts );
          const int recSize = recPorts.size();
          for ( int j = 0; j < recSize; ++j )
          {
            NUNet *fieldPort = recPorts[j];
            NUNet *newPort = createPortAliasNet( recAtom, fieldPort );
            recPorts[j] = newPort;
          }
          // This will fix up both mPorts and mPortIndices
          module->replacePort( cport, recPorts );
          // The original port is now unreferenced; schedule it for deletion.
          scheduleDeletion( cport );
          i += recSize;
        }
        else
        {
          ++i;
        }
      }
      NUNetList topNets;
      module->getAllTopNets( &topNets );
      for ( NUNetList::iterator p = topNets.begin(); p != topNets.end(); ++p )
      {
        NUNet *net = *p;
        if ( net->isCompositeNet() && !net->isPort( ))
        {
          module->removeLocal( net );
          scheduleDeletion( net->getCompositeNet( ));
        }
      }
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUModuleInstance *node) {
    if ( phase == ePost )
    {
      // Find record ports in the port connection vector _after_ processing the submodule.
      bool updatedPortList = false;
      NUPortConnectionVector newportconns;
      NUPortConnection *conn;
      CopyContext cc(0,0);
      for ( NUPortConnectionLoop p = node->loopPortConnections(); !p.atEnd(); ++p )
      {
	conn = *p;
        NUCompositeNet *cNet = conn->getFormal()->getCompositeNet();
        if ( cNet != NULL )
        {
          updatedPortList = true;
         // This connection has a composite net for its formal.  Expand
          // it and its actual into N portconns of the appropriate type
          // of portconn.
          NUNetVector netvector;
          cNet->createNetVector( &netvector );
          const int numRecFields = netvector.size();
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Replacing module portconn(" << conn << ") with " << numRecFields << " new connections\n";
          conn->print(1,1);
#endif
          const PortDirectionT portdir = conn->getDir();
          NUModuleInstance *inst = conn->getModuleInstance();
          NUPortConnection *newconn;
          if ( portdir == eInput )
          {
            NUPortConnectionInput *pci = dynamic_cast<NUPortConnectionInput*>( conn );
            NUExprVector exprvector;
            createRvalueVector( pci->getActual(), &exprvector, mMsgContext, numRecFields );
            for ( int i = 0; i < numRecFields; ++i )
            {
              newconn = new NUPortConnectionInput( exprvector[i], (netvector[i])->getStorage(),
                                                   conn->getLoc());
              newconn->setModuleInstance( inst );
#if COMPRESYNTH_DEBUG
              UtIO::cout() << "New module input port connection (" << newconn << ")\n";
              newconn->print(1,1);
#endif
              newportconns.push_back( newconn );
            }
          }
          else if ( portdir == eOutput )
          {
            NUPortConnectionOutput *pco = dynamic_cast<NUPortConnectionOutput*>( conn );
            NULvalueVector lvaluevector;
            createLvalueVector( pco->getActual(), &lvaluevector, mMsgContext, numRecFields );
            for ( int i = 0; i < numRecFields; ++i )
            {
              newconn = new NUPortConnectionOutput( lvaluevector[i], (netvector[i])->getStorage(),
                                                    conn->getLoc());
              newconn->setModuleInstance( inst );
#if COMPRESYNTH_DEBUG
              UtIO::cout() << "New module output port connection (" << newconn << ")\n";
              newconn->print(1,1);
#endif
              newportconns.push_back( newconn );
            }
          }
          else
          {
            NU_ASSERT( portdir == eBid, conn );
            NUPortConnectionBid *pcb = dynamic_cast<NUPortConnectionBid*>( conn );
            NULvalueVector lvaluevector;
            createLvalueVector( pcb->getActual(), &lvaluevector, mMsgContext, numRecFields );
            for ( int i = 0; i < numRecFields; ++i )
            {
              newconn = new NUPortConnectionBid( lvaluevector[i], (netvector[i])->getStorage(),
                                                 conn->getLoc());
              newconn->setModuleInstance( inst );
#if COMPRESYNTH_DEBUG
              UtIO::cout() << "New module bidi port connection (" << newconn << ")\n";
              newconn->print(1,1);
#endif
              newportconns.push_back( newconn );
            }
          }
        }
        else
        {
          NUPortConnection *conncopy = conn->copy( cc );
          conncopy->setModuleInstance( conn->getModuleInstance() );
          newportconns.push_back( conncopy );
        }
      }

      // Only update things if we really need to
      if ( updatedPortList == true )
      {
        NUPortConnectionList oldportconns;
        node->getPortConnections( oldportconns );
        for ( NUPortConnectionListIter iter = oldportconns.begin();
              iter != oldportconns.end(); ++iter )
        {
          conn = *iter;
          delete conn;
        }
        node->setPortConnections( newportconns );
      }
      else
      {
        // Ditch the copy of the portconn list; we don't need it because
        // no NUCompositeNets were found.
        for ( NUPortConnectionVectorIter iter = newportconns.begin();
              iter != newportconns.end(); ++iter )
        {
          delete (*iter);
        }
       }
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUTF* tf) {
    if ( phase == ePre ) {
      // Expand the mArgs member of the TF by element-blasting
      // CompositeNet ports into their fields.
      SInt32 i = 0;
      while ( i < tf->getNumArgs() )
      {
        NUNet *arg = tf->getArg( i );
        if ( arg->isCompositeNet() )
        {
          const StringAtom *recAtom = arg->getName();
          NUNetVector recArgs;
          NUCompositeNet *carg = arg->getCompositeNet();
          carg->createNetVector( &recArgs );
          const int recSize = recArgs.size();
          for ( int j = 0; j < recSize; ++j )
          {
            NUNet *fieldArg = recArgs[j];
            NUNet *newArg = createPortAliasNet( recAtom, fieldArg );
            newArg->putIsBlockLocal( true );
            recArgs[j] = newArg;
          }
          // This will fix up both mArgs and mArgsMode
          tf->replaceArg( carg, recArgs );
          // The original port is now unreferenced; schedule it for deletion.
          scheduleDeletion( carg );
          i += recSize;
        }
        else
        {
          ++i;
        }
      }
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUTaskEnable *node) {
    if ( phase == ePost )
    {
      // Find record ports in the port connection vector _after_ processing the submodule.
      bool updatedPortList = false;
      NUTFArgConnectionVector newportconns;
      NUTFArgConnection *conn;
      CopyContext cc(0,0);
      for ( NUTFArgConnectionLoop p = node->loopArgConnections(); !p.atEnd(); ++p )
      {
	conn = *p;
        NUCompositeNet *cNet = conn->getFormal()->getCompositeNet();
        if ( cNet != NULL )
        {
          updatedPortList = true;
         // This connection has a composite net for its formal.  Expand
          // it and its actual into N portconns of the appropriate type
          // of portconn.
          NUNetVector netvector;
          cNet->createNetVector( &netvector );
          const UInt32 numRecFields = netvector.size();
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Replacing task portconn(" << conn << ") with " << numRecFields << " new connections\n";
          conn->print(1,1);
#endif
          const PortDirectionT portdir = conn->getDir();
          NUTF *tf = conn->getTF();
          NUTFArgConnection *newconn;
          if ( portdir == eInput )
          {
            NUTFArgConnectionInput *pci = dynamic_cast<NUTFArgConnectionInput*>( conn );
            NUExprVector exprvector;
            NUExpr *actual = pci->getActual();
            if ( actual->getType() == NUExpr::eNUConcatOp )
            {
              // The actual is a single non-composite concat, probably
              // due to already having been resynthesized.  Fill
              // lvalueVector with this concat's elements.
              NUConcatOp *concat = dynamic_cast<NUConcatOp*>( actual );
              const UInt32 concatSize = concat->getNumArgs();
              for ( UInt32 i = 0; i < concatSize; ++i )
                exprvector.push_back( concat->getArg( i )->copy( cc ));
            }
            else
            {
              createRvalueVector( actual, &exprvector, mMsgContext, numRecFields );
            }
            NU_ASSERT( exprvector.size() == numRecFields, conn );
            for ( UInt32 i = 0; i < numRecFields; ++i )
            {
              newconn = new NUTFArgConnectionInput( exprvector[i], (netvector[i])->getStorage(),
                                                    tf, conn->getLoc());
              newconn->replaceTF( tf );
#if COMPRESYNTH_DEBUG
              UtIO::cout() << "New task input port connection (" << newconn << ")\n";
              newconn->print(1,1);
#endif
              newportconns.push_back( newconn );
            }
          }
          else if ( portdir == eOutput )
          {
            NUTFArgConnectionOutput *pco = dynamic_cast<NUTFArgConnectionOutput*>( conn );
            NULvalueVector lvaluevector;
            NULvalue *actual = pco->getActual();
            if ( actual->getType() == eNUConcatLvalue )
            {
              // The actual is a single non-composite concat, probably
              // due to already having been resynthesized.  Fill
              // lvalueVector with this concat's elements.
              NUConcatLvalue *concat = dynamic_cast<NUConcatLvalue*>( actual );
              const UInt32 concatSize = concat->getNumArgs();
              for ( UInt32 i = 0; i < concatSize; ++i )
                lvaluevector.push_back( concat->getArg( i )->copy( cc ));
            }
            else
            {
              createLvalueVector( actual, &lvaluevector, mMsgContext, numRecFields );
            }
            for ( UInt32 i = 0; i < numRecFields; ++i )
            {
              newconn = new NUTFArgConnectionOutput( lvaluevector[i], (netvector[i])->getStorage(),
                                                     tf, conn->getLoc());
              newconn->replaceTF( tf );
#if COMPRESYNTH_DEBUG
              UtIO::cout() << "New task output port connection (" << newconn << ")\n";
              newconn->print(1,1);
#endif
              newportconns.push_back( newconn );
            }
          }
          else
          {
            NU_ASSERT( portdir == eBid, conn );
            NUTFArgConnectionBid *pcb = dynamic_cast<NUTFArgConnectionBid*>( conn );
            NULvalueVector lvaluevector;
            NULvalue *actual = pcb->getActual();
            if ( actual->getType() == eNUConcatLvalue )
            {
              // The actual is a single non-composite concat, probably
              // due to already having been resynthesized.  Fill
              // lvalueVector with this concat's elements.
              NUConcatLvalue *concat = dynamic_cast<NUConcatLvalue*>( actual );
              const UInt32 concatSize = concat->getNumArgs();
              for ( UInt32 i = 0; i < concatSize; ++i )
                lvaluevector.push_back( concat->getArg( i )->copy( cc ));
            }
            else
            {
              createLvalueVector( actual, &lvaluevector, mMsgContext, numRecFields );
            }
            NU_ASSERT( lvaluevector.size() == numRecFields, conn );
            for ( UInt32 i = 0; i < numRecFields; ++i )
            {
              newconn = new NUTFArgConnectionBid( lvaluevector[i], (netvector[i])->getStorage(),
                                                  tf, conn->getLoc());
              newconn->replaceTF( tf );
#if COMPRESYNTH_DEBUG
              UtIO::cout() << "New task bidi port connection (" << newconn << ")\n";
              newconn->print(1,1);
#endif
              newportconns.push_back( newconn );
            }
          }
        }
        else
        {
          NUTFArgConnection *conncopy = conn->copy( cc );
          conncopy->replaceTF( conn->getTF() );
          newportconns.push_back( conncopy );
        }
      }

      // Only update things if we really need to
      if ( updatedPortList == true )
      {
        NUTFArgConnectionVector oldportconns;
        node->getArgConnections( oldportconns );
        for ( NUTFArgConnectionVectorIter iter = oldportconns.begin();
              iter != oldportconns.end(); ++iter )
        {
          conn = *iter;
          delete conn;
        }
        node->setArgConnections( newportconns );
      }
      else
      {
        // Ditch the copy of the portconn list; we don't need it because
        // no NUCompositeNets were found.
        for ( NUTFArgConnectionVectorIter iter = newportconns.begin();
              iter != newportconns.end(); ++iter )
        {
          delete (*iter);
        }
       }
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUIdentLvalue *node) {
    if ( phase == ePost )
    {
      NUNet *myNet = node->getIdent();
      STAliasedLeafNode *myLeaf = myNet->getNameLeaf();
      if ( myLeaf->hasAliases() )
      {
        STAliasedLeafNode *storage = myLeaf->getStorage();
        NUNet *storageNet = NUAliasBOM::castBOM( storage->getBOMData( ))->getNet();
        if ( myNet != storageNet )
        {
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Replacing NUIdentLvalue reference:\n";
          node->print(1,1);
#endif
          node->replaceDef( myNet, storageNet );
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Modified NUIdentLvalue reference:\n";
          node->print(1,1);
#endif          
        }
      }
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUIdentRvalue *node) {
    if ( phase == ePost )
    {
      NUNet *myNet = node->getIdent();
      STAliasedLeafNode *myLeaf = myNet->getNameLeaf();
      if ( myLeaf->hasAliases() )
      {
        STAliasedLeafNode *storage = myLeaf->getStorage();
        NUNet *storageNet = NUAliasBOM::castBOM( storage->getBOMData( ))->getNet();
        if ( myNet != storageNet )
        {
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Replacing NUIdentRvalue reference:\n";
          node->print(1,1);
#endif
          node->replace( myNet, storageNet );
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Modified NUIdentRvalue reference:\n";
          node->print(1,1);
#endif          
        }
      }
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUNamedDeclarationScope *node) {
    if ( phase == ePre )
    {
      NUNetLoop loop = node->loopLocals();
      while ( !loop.atEnd( ))
      {
        NUNet *net = (*loop);
        NUCompositeNet *cNet = net->getCompositeNet();
        if ( cNet != NULL )
        {
          ++loop; // advance the iterator before removing the net
          node->removeLocal( cNet );
#if COMPRESYNTH_DEBUG
          UtIO::cout() << "Scheduling NUCompositeNet in NUNDS "
                       << node->getName()->str() << " for deletion:\n";
          cNet->print(1,1);
#endif
          scheduleDeletion( cNet );
        }
        else
          ++loop;
      }
    }
    return eNormal;
  }

private:
  NUNet *
  createPortAliasNet( const StringAtom *recAtom, NUNet *fieldPort )
  {
    fieldPort->putIsRecordPort( true );
    fieldPort->putIsAllocated( false );
    fieldPort->putIsAliased( true );
    // Create the new net either in the module or TF scope, depending
    NUScope *declaration_scope = fieldPort->getScope();
    if ( declaration_scope->inTFHier( ))
      declaration_scope = declaration_scope->getTFScope();
    else
      declaration_scope = declaration_scope->getModule();

    NUNet *portAlias = declaration_scope->createTempNetFromImage( fieldPort, *recAtom, true );
    // createTempNetFromImage adds this as a local net; it's a port, not
    // local, so we need to immediately remove it from the local list or
    // it will be declared both as a local and as a port.
    declaration_scope->removeLocal( portAlias, false );
    // Set the net flags appropriately for both nets
    NetFlags flags = NetFlags( fieldPort->getFlags( ));
    portAlias->setFlags( flags );
    portAlias->putIsAllocated( true );
    portAlias->putIsBlockLocal( false );
    portAlias->putIsTemp( true );

    // Alias the new port to the original scoped net in the symtab.  The
    // new port is the storage node and the original is the master node.
    STAliasedLeafNode *fieldLeaf = fieldPort->getNameLeaf();
    STAliasedLeafNode *portLeaf = portAlias->getNameLeaf();
    portLeaf->linkAlias( fieldLeaf );
    portLeaf->setThisStorage();
    fieldLeaf->setThisMaster();

    return portAlias;
  }

  MsgContext *mMsgContext;
};

CompositeResynth::CompositeResynth(MsgContext *msgContext)
  : mMsgContext( msgContext )
{
}

CompositeResynth::~CompositeResynth() {
}


/*! \brief resynthesize NUCompositeNets etc. away
 *
 * Composite resynthesis is performed in three stages.  First, a
 * NUDesignWalker derivation updates all the NUCompositeNets in-place.
 * Secondly, replaceLeaves() is used to either rewrite or replace all
 * NUMemsel[LR]value nodes.  Finally, any assignments of the form mem <=
 * concat or concat <= mem are rewritten into concat <= concat.
 */
void
CompositeResynth::design(NUDesign* design) {
  mNetsToDelete = new NUNetSet;

  // Map of resynthesized nets to their replacements
  NUModuleList module_list;
  NUModuleList moduleTop_list;
  design->getAllModules( &module_list );
  design->getTopLevelModules( &moduleTop_list );

  // Resynthesize arrayed composite nets away
  CompositeDimCallback dimCallback(design->getIODB(), mMsgContext);
  // First resynthesize arrayed composite nets in the top module.
  // The signals of record type defined in a package get populated in the top module.
  // We create hier ref nets for these signals in the modules, they are used in.
  // It's important to resyntsize the nets and then the hier ref next. 
  for ( NUModuleList::iterator i = moduleTop_list.begin(); i != moduleTop_list.end(); ++i )
  {
    (*i)->replaceLeaves( dimCallback );
  }

  for ( NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i )
  {
    (*i)->replaceLeaves( dimCallback );
  }

  // The top module's ports are not covered by the resynthesis above, if
  // they are not used in continuous assignment, in another module instantiation or behavior logic.
  // Resynthesize arrayed composite nets away for ports of the top module.
  NUModuleList moduleTopList;
  design->getTopLevelModules( &moduleTopList );
  for ( NUModuleList::iterator i = moduleTopList.begin(); i != moduleTopList.end(); ++i )
  {
    NUNetList  portList;
    (*i)->getPorts(&portList);
    for(NUNetList::iterator j = portList.begin(); j != portList.end(); ++j)
    {
      dimCallback((*j), CompositeDimCallback::ePrePost);
    }
  }

  // Resynthesize the NU[Composite]Ident[LR]values that refer to
  // CompositeNets into concats
  CompositeIdentCallback identCallback( mMsgContext );
  for ( NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i )
  {
    (*i)->replaceLeaves( identCallback );
  }

  // Resynthesize all the net declarations, module ports & connections,
  // TF params and connections
  CompositeResynthCallback compositeResynthCallback( mMsgContext );
  NUDesignWalker compositewalker( compositeResynthCallback, true/*verbose*/ );
  compositewalker.putWalkTasksFromTaskEnables( false );
  compositewalker.putWalkDeclaredNets( true );
  compositewalker.design( design );

  // Resynthesize any remaining NUComposite[Lvalue|Expr] nodes into concats
  CompositeConcatCallback concatCallback( mMsgContext );
  for ( NUModuleList::iterator i = module_list.begin(); i != module_list.end(); ++i )
  {
    (*i)->replaceLeaves( concatCallback );
  }

  // We might have changed the primary port list; recompute it. This code is
  // essentially the body of LFContext::mapTopLevelPortIndices().
  NUModuleList topMods;
  design->getTopLevelModules(&topMods);
  INFO_ASSERT(topMods.size() == 1, "More than one top module exists.");
  NUModule* topMod = topMods.back();
  NUNetList portList;
  topMod->getPorts(&portList);
  topMod->getIODB()->putPrimaryPorts(portList);

  deleteNets( design->getIODB() );
  delete mNetsToDelete;
}
