// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeNet.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/HierStringName.h"
#include "util/StringAtom.h"
#include "symtab/STAliasedLeafNode.h"
#include "localflow/MemoryResynth.h"
#include "compiler_driver/CarbonContext.h"

//! Helper function to create a continuous assign; code consolidation.
NUContAssign *helperPopulateContAssign(NULvalue *lvalue,
                                       NUExpr *rvalue,
                                       const SourceLocator& loc,
                                       LFContext *context)
{
  StringAtom *block_name = 
    context->getModule()->newBlockName(context->getStringCache(), loc);
  return context->stmtFactory()->createContAssign(block_name, lvalue, rvalue, loc);
}


Populate::ErrorCode
VhdlPopulate::helperAssign( NULvalue *the_lvalue, NUExpr *the_rvalue,
                            assignType assign_type, LFContext *context,
                            const SourceLocator &loc, bool &assignCreated )
{
  ErrorCode err_code = eSuccess;
  assignCreated = false;
  the_rvalue->resize (the_rvalue->determineBitSize ());
  if ( the_lvalue->isWholeIdentifier( ))
  {
    // Check if lvalue is a full MemoryNet.
    NUMemoryNet* mem = the_lvalue->getWholeIdentifier()->getMemoryNet();
    if ( mem )
    {
      const bool isResynthesized = mem->isResynthesized();
      const UInt32 depth = isResynthesized ? mem->getDepth() : (mem->getNumDims() - mem->getFirstUnpackedDimensionIndex());
      // don't do an agg assign if the memory only has 1 row
      if ( depth > 1 )
      {
        // Now Check if rvalue is an aggregate type.
        if ( the_rvalue->getType() == NUExpr::eNUConcatOp )
        {
          NUConcatOp *concat = dynamic_cast<NUConcatOp*>( the_rvalue );
          err_code = memoryAssignByAggregate( assign_type, context, mem,
                                              the_lvalue, concat, loc );
          if ( err_code == eSuccess )
            assignCreated = true;
        }
      }
    }
  }
  else if ( the_rvalue->isWholeIdentifier( ))
  {
    // Check if rvalue is a full MemoryNet.
    NUMemoryNet* mem = the_rvalue->getWholeIdentifier()->getMemoryNet();
    if ( mem )
    {
      const bool isResynthesized = mem->isResynthesized();
      const UInt32 depth = isResynthesized ? mem->getDepth() : (mem->getNumDims() - mem->getFirstUnpackedDimensionIndex());
      // don't do an agg assign if the memory only has 1 row
      if ( depth > 1 )
      {
        // Now Check if lvalue is an aggregate type.
        if ( the_lvalue->getType() == eNUConcatLvalue )
        {
          NUConcatLvalue *concat = dynamic_cast<NUConcatLvalue*>( the_lvalue );
          err_code = memorySliceAssign( assign_type, context, concat,
                                        the_rvalue, mem, loc );
          if ( err_code == eSuccess )
            assignCreated = true;
        }
      }
    }
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::pull( vhNode vh_lvalue, vhExpr vh_rvalue, LFContext *context )
{
  ErrorCode err_code = eNotApplicable, temp_err_code;
  const SourceLocator loc = locator( vh_rvalue );

  // Get the driving value and place it into the UtString
  UtString valStr;
  switch ( vhGetObjType( vh_rvalue ))
  {
  case VHCHARLIT:
    valStr << vhGetCharVal( vh_rvalue );
    break;
  case VHSTRING:
    valStr << vhGetString( vh_rvalue );
    break;
  case VHAGGREGATE:
  {
    JaguarList eleList( vhGetEleAssocList( vh_rvalue ));
    // We only support a 1-element association list, where that
    // association is a VHOTHERS
    if ( eleList.size() != 1 )
    {
      return err_code;
    }
    vhExpr element = static_cast<vhExpr>( vhGetNextItem( eleList ));
    JaguarList choiceList( vhGetChoiceList( element ));
    if ( choiceList.size() != 1 )
    {
      return err_code;
    }
    vhNode choice = vhGetNextItem( choiceList );
    if ( vhGetObjType( choice ) != VHOTHERS )
    {
      return err_code;
    }
    // We have verified we have an others node
    vhExpr expr = vhGetExpr( element );
    if ( vhGetObjType( expr ) != VHCHARLIT )
    {
      return err_code;
    }
    // Put the character into the value string
    valStr << vhGetCharVal( expr );
    break;
  }
  default:
    return err_code;
  }
  LOC_ASSERT( valStr.size() > 0, loc );

  // Get the first char of the value string and validate it
  bool pull_up = false;
  const char val = valStr[0];
  if ( val == 'H' )
  {
    pull_up = true;
  }
  else if ( val != 'L' )
  {
    // Not an H or L? Not a pull.
    return err_code;
  }

  // Make sure the rest of the string is only the character in val
  if ( valStr.find_first_not_of( val ) != UtString::npos )
  {
    return err_code;
  }
  
  // Now we finally know we have a pull; its value is in the boolean
  // pull_up.  Next, find the net being pulled.  This code was
  // shamelessly stolen from VerilogPopulate::pull().
  err_code = eSuccess;
  NULvalue *lhs = 0;
  temp_err_code = lvalue( vh_lvalue, context, &lhs );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    return temp_err_code;
  }

  if ( not lhs->isWholeIdentifier( ))
  {
    delete lhs;
    POPULATE_FAILURE(mMsgContext->PullNotWhole( &loc ), &err_code);
    return err_code;
  }

  NUNet *net = lhs->getWholeIdentifier();
  delete lhs;

  if ( pull_up )
  {
    if ( context->checkForPullConflicts( ) && net->isPullDown( ))
    {
      POPULATE_FAILURE(mMsgContext->PullUpAndDown( net ), &err_code);
    }
    net->putIsPullUp( true );
  }
  else
  {
    if ( context->checkForPullConflicts( ) && net->isPullUp( ))
    {
      POPULATE_FAILURE(mMsgContext->PullUpAndDown( net ), &err_code);
    }
    net->putIsPullDown(true);
  }

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::concurrentSignalAssign(vhNode vh_assign,
                                     LFContext* context,
                                     NUContAssign **the_assign)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  *the_assign = 0;

  vhExpr vh_lvalue = vhGetTarget(vh_assign);
  vhNode vh_wavefrm = vhGetWaveFrm(vh_assign);
  vhNode wavefrm_ele = vhGetNodeAt(0,vhGetWaveFrmEleList(vh_wavefrm));
  vhExpr vh_rvalue = vhGetValueExpr(wavefrm_ele);

  SourceLocator loc = locator(vh_assign);
  if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_lvalue, context ))
    return recordAssign( eConcurrent, vh_lvalue, vh_rvalue, context, loc );

  // If vh_rvalue's a pullup or pulldown, annotate the net as such and
  // return, creating no assignment.  A return value of eNotApplicable
  // means that no pull was detected, and creation of an assignment
  // needs to occur.  Any other return value indicates that no
  // assignment should be created, with either a pullup noted
  // successfully or an error thrown.
  temp_err_code = pull( vh_lvalue, vh_rvalue, context );
  if ( temp_err_code != eNotApplicable )
  {
    return temp_err_code;
  }

  NULvalue *the_lvalue = 0;
  temp_err_code = lvalue(vh_lvalue, context, &the_lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  NUExpr* the_rvalue = 0;
  int lWidth = 0, numDims = 0;
  temp_err_code = getVhExprBitSize(vh_lvalue, &lWidth, &numDims, loc, context, true);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;
  temp_err_code = expr( vh_rvalue, context, lWidth, &the_rvalue );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  bool assignCreated = false;
  temp_err_code = helperAssign( the_lvalue, the_rvalue, eConcurrent,
                                context, loc, assignCreated );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  if ( !assignCreated )
  {
    the_rvalue->resize(the_rvalue->determineBitSize ());
    *the_assign = helperPopulateContAssign(the_lvalue, the_rvalue, loc, context);
  }
  return err_code;
}


/*! \brief Unset the initial value for the argument or the components
 *  thereof, if the argument is an unaliased variable
 *
 * We use the initial value field of a Jaguar VHDL variable node to
 * track its current static value, if it has one.  When the value is not
 * statically determinable we call this method to NULL out the node's
 * initial value field.  This routine deals with indexed nodes, slices,
 * selected names, and aggregates, unsetting the initialization of all
 * the relevant parts of a variable or set of variables.  Other nodes
 * are silently ignored.
 */
void
unsetVariableInitialValue( vhNode node )
{
  vhNode vh_var;
  switch ( vhGetObjType( node ))
  {
  case VHOBJECT:
    vh_var = vhGetActualObj( node );
    unsetVariableInitialValue(vh_var);
    return;
    break;
  case VHVARIABLE:
    vh_var = node;
    break;
  case VHSLICENAME:
  case VHINDNAME:
    vh_var = vhGetActualObj( vhGetPrefix( static_cast<vhExpr>( node )));
    unsetVariableInitialValue( vh_var );
    return;
    break;
  case VHSELECTEDNAME:
    vh_var = vhGetActualObj( node );
    unsetVariableInitialValue( vh_var );
    return;
  break;
  case VHAGGREGATE:
  {
    // Unset each member of an aggregate
    JaguarList aggList( vhGetEleAssocList( static_cast<vhExpr>( node )));
    while ( vhNode elem = vhGetNextItem( aggList ))
    {
      vhNode elemExpr;
      if ( vhGetObjType( elem ) == VHELEMENTASS )
      {
        elemExpr = vhGetExpr( elem );
        if ( vhGetObjType( elemExpr ) == VHOBJECT )
        {
          elemExpr = vhGetActualObj( elemExpr );
        }
      }
      else
      {
        elemExpr = elem;
      }
      unsetVariableInitialValue( elemExpr );
    }
    return;
  }
  default:
    // Nodes we want to do nothing with, like VHSIGNAL
    return;
  }

  if (vhGetObjType(vh_var) != VHVARIABLE)
  {
    UtString errMsg;
    errMsg << vhGetSourceFileName(vh_var) << ":" << vhGetLineNumber(vh_var)
           << " Attempting to unset an object that's not a variable.";
    INFO_ASSERT(vhGetObjType(vh_var) == VHVARIABLE, errMsg.c_str());
  }
  vhSetInitialValue( vh_var, NULL );
}


Populate::ErrorCode
VhdlPopulate::variableAssign( vhNode vh_assign,
                              LFContext* context,
                              NUBlockingAssign **the_assign )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  *the_assign = 0;

  vhExpr vh_lvalue = vhGetTarget( vh_assign );
  vhExpr vh_rvalue = vhGetSource( vh_assign );

  SourceLocator loc = locator( vh_assign );
  if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_lvalue, context ))
    return recordAssign( eSequentialBlocking, vh_lvalue, vh_rvalue, context, loc );

  NULvalue* the_lvalue = 0;
  temp_err_code = lvalue( vh_lvalue, context, &the_lvalue );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    UtString buf;
    mMsgContext->FailedToPopulateExpr(&loc, decompileJaguarNode(vh_lvalue, &buf));
    return temp_err_code;
  }

  NUExpr* the_rvalue = 0;
//   if ( !mRecursionStack.empty( ))
//   {
//     // This call will take any initial values as the current value of a
//     // variable or signal.  This is not dangerous since no initial values
//     // from outside the function are allowed to participate in the
//     // expression evaluation. Only the initial values of variables/signals
//     // declarated within function participate. This is needed only
//     // for recursive functions.
//     vh_rvalue = evaluateJaguarExpr( vh_rvalue );
//   }

  int lWidth = 0, numDims = 0;
  temp_err_code = getVhExprBitSize(vh_lvalue, &lWidth, &numDims, loc, context, true);

  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    // No static width for the lhs - use the worst-case width of the lvalue...
    lWidth = the_lvalue->getBitSize ();
  }
  temp_err_code = expr( vh_rvalue, context, lWidth, &the_rvalue );

  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    UtString buf;
    mMsgContext->FailedToPopulateExpr(&loc, decompileJaguarNode(vh_rvalue, &buf));
    return temp_err_code;
  }

  bool assignCreated = false;
  temp_err_code = helperAssign( the_lvalue, the_rvalue, eSequentialBlocking,
                                context, loc, assignCreated );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  if ( !assignCreated )
  {
    the_rvalue->resize (the_rvalue->determineBitSize ());
    *the_assign = context->stmtFactory()->createBlockingAssign( the_lvalue, the_rvalue, false, loc );
    err_code = eSuccess;
  }

  if ( VHOBJECT == vhGetObjType( vh_lvalue ))
  {
    vh_lvalue = static_cast<vhExpr>( vhGetActualObj( vh_lvalue ));
  }
//   // We will set this current value as initial value if and only if the
//   // lvalue is an unaliased full variable and the value is static.  If
//   // it's an unaliased variable with a nonstatic rvalue, we'll unset the
//   // initial value.  If the node is aliased it is ignored, neither set
//   // nor unset.
//   if ( not isAliasObject( vh_lvalue ))
//   {
//     if (vhGetObjType( vh_lvalue ) == VHVARIABLE &&
//         vhGetStaticType( vh_rvalue ) != VH_NOT_STATIC )
//     {
//       vhSetInitialValue( vh_lvalue, vh_rvalue );
//       context->addJaguarUninitVariable( vh_lvalue );
//     }
//     else
//     {
//       unsetVariableInitialValue( vh_lvalue );
//     }
//   }

  return err_code;
}


NUNet* VhdlPopulate::maybeCreateTempMemCopy(assignType assign_type,
                                            NUMemoryNet* mem,
                                            NULvalue* lval, NUExpr* rval,
                                            LFContext* context, 
                                            const SourceLocator& loc)
{
  NUNet* temp_mem = mem;
  if (assign_type == eSequentialBlocking)
  {
    // Check if there's an overlap between lvalue and rvalue in the assign
    // statement. If there isn't, there's no need for temporary memory.
    NUNetRefFactory* fact = context->getNetRefFactory();
    NUNetRefSet lval_defs(fact);
    lval->getDefs(&lval_defs);

    NUNetRefSet rval_uses(fact);
    rval->getUses(&rval_uses);

    if (NUNetRefSet::set_has_intersection(rval_uses, lval_defs))
    {
      // Create a block local memory net here.
      StringAtom *newNameAtom;
      NUScope *scope = context->getModule();
      newNameAtom = scope->gensym( "$mem", mem->getName()->str());
      temp_mem = scope->createTempMemoryNetFromImage(newNameAtom, mem, mem->getFlags());
      // Create a blocking assign: targetMem := mem;
      NULvalue *lval_temp = new NUIdentLvalue( temp_mem, loc );
      NUExpr *rval_temp = createIdentRvalue( mem, loc );
      NUStmt *assign_temp = context->stmtFactory()->createBlockingAssign( lval_temp, rval_temp, false, loc );
      context->addStmt( assign_temp );
    }
  } // if
  return temp_mem;
}


// A memory assign via an aggregate is accomplished by creating N memory
// word assigns.  If the assignment is a blocking assign, the memory is
// temped first and then the memory assigned from the temp.  This is to
// prevent us from reading a partially updated memory during the set of
// assigns.  If the temp memory is not needed, it will be optimized
// away.
Populate::ErrorCode
VhdlPopulate::memoryAssignByAggregate( assignType assign_type,
                                       LFContext* context,
                                       NUMemoryNet* mem,
                                       NULvalue* lvalue,
                                       NUConcatOp* concat,
                                       const SourceLocator& loc)
{
  // VHDL doesn't have a repeatable concat like Verilog
  if ( concat->getRepeatCount() != 1 )
  {
    UtString msg;
    msg << "Repeat count != 1 found in a VHDL concatenation.";
    mMsgContext->JaguarConsistency( &loc, msg.c_str( ));
    return eFatal;  
  }

  UInt32 expectedSize = 1;
  const UInt32 numDims = mem->getNumDims();
  for ( UInt32 i = 1; i < numDims; ++i )
  {
    expectedSize *= mem->getRangeSize( i );
  }

  NULvalue* lv = lvalue; 
  NUExpr* rv = concat; 


  switch ( assign_type )
  {
    case eConcurrent:
    {
      StringAtom *block_name =
        context->getModule()->newBlockName( context->getStringCache(), loc );
      NUContAssign* assign = context->stmtFactory()->createContAssign( block_name, lv, rv, loc );
      context->getModule()->addContAssign( assign );
      break;
    }
    case eSequentialNonBlocking:
    {
      NUStmt* assign = context->stmtFactory()->createNonBlockingAssign( lv, rv, false, loc );
      context->addStmt(assign);
      break;
    }
    case eSequentialBlocking:
    {
      NUStmt* assign = context->stmtFactory()->createBlockingAssign( lv, rv, false, loc );
      context->addStmt(assign);
      break;
    }
    case eInitialValue:
    {
      {
        // Indicate population of declaration initialization statement outside
        // the current scope, in the module's initial block.
        DeclInitIndicator dii(context);
        NUBlockingAssign *assign;
        assign = context->stmtFactory()->createBlockingAssign( lv, rv, false, loc );
        addStmtToInitialBlock( context, context->getModule(), assign,
                               mem, loc );
      }
      break;
    }
  }
  return eSuccess;
}

// A memory assign to a memory slice aggregate is accomplished by
// creating N memory word assigns.  If the assignment is a blocking
// assign, the memory is temped first and then the memory assigned from
// the temp.  This is to prevent us from reading a partially updated
// memory during the set of assigns.  If the temp memory is not needed,
// it will be optimized away.
Populate::ErrorCode
VhdlPopulate::memorySliceAssign( assignType assign_type,
                                 LFContext* context,
                                 NUConcatLvalue* concat,
                                 NUExpr* rvalue,
                                 NUMemoryNet* mem,
                                 const SourceLocator& loc )
{
  NUConcatLvalue* lv = concat; 
  NUExpr* rv = rvalue; 

  switch ( assign_type )
  {
    case eConcurrent:
    {
      StringAtom *block_name =
        context->getModule()->newBlockName( context->getStringCache(), loc );
      NUContAssign* assign = context->stmtFactory()->createContAssign( block_name, lv, rv, loc );
      context->getModule()->addContAssign( assign );
      break;
    }
    case eSequentialNonBlocking:
    {
      NUStmt* assign = context->stmtFactory()->createNonBlockingAssign( lv, rv, false, loc );
      context->addStmt(assign);
      break;
    }
    case eSequentialBlocking:
    {
      NUStmt* assign = context->stmtFactory()->createBlockingAssign( lv, rv, false, loc );
      context->addStmt(assign);
      break;
    }
    case eInitialValue:
    {
      {
        // Indicate population of declaration initialization statement outside
        // the current scope, in the module's initial block.
        DeclInitIndicator dii(context);
        NUBlockingAssign *assign;
        assign = context->stmtFactory()->createBlockingAssign( lv, rv, false, loc );
        addStmtToInitialBlock( context, context->getModule(), assign,
                               mem, loc );
      }
      break;
    }
  }

  return eSuccess;
}


Populate::ErrorCode
VhdlPopulate::signalAssign(vhNode vh_assign,
                           LFContext* context,
                           NUNonBlockingAssign **the_assign)
{
  ErrorCode err_code = eSuccess;
  *the_assign = 0;

  SourceLocator loc = locator(vh_assign);

  vhExpr vh_lvalue = vhGetTarget(vh_assign);
  vhNode vh_wavefrm = vhGetWaveFrm(vh_assign);
  // Get the first element of the waveform list since at the most we can
  // support this as synthesizable. The rest of the wave form elements will
  // be discarded. FYI, in case of nonblocking assignments aka VHDL signal assignment
  // user can provide a wave form instead of a single value which may look like
  // targetSignal <= value1 , value2 after 5 ns, value3 after 10 ns;
  // KLG: 01/22/2004 , simple signal assignment support
  vhNode wavefrm_ele = vhGetNodeAt(0,vhGetWaveFrmEleList(vh_wavefrm));
  vhExpr vh_rvalue = vhGetValueExpr(wavefrm_ele);

  if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_lvalue, context ))
    return recordAssign( eSequentialNonBlocking, vh_lvalue, vh_rvalue, context, loc );

  NULvalue* the_lvalue = 0;
  ErrorCode temp_err_code = lvalue(vh_lvalue, context, &the_lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  int lWidth = 0, numDims = 0;
  bool lvalue_size_not_known = false;
  temp_err_code = getVhExprBitSize(vh_lvalue, &lWidth, &numDims, loc, context, true);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    // No static width for the lhs - use the worst-case width of the lvalue...
    lWidth = the_lvalue->getBitSize ();
    lvalue_size_not_known = true;
  }

  NUExpr* the_rvalue = 0;
  temp_err_code = expr(vh_rvalue, context, lWidth, &the_rvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  bool assignCreated = false;
  UInt32 rvalue_size = the_rvalue->determineBitSize();
  the_rvalue->resize(rvalue_size);

  if (lvalue_size_not_known && !the_rvalue->sizeVaries() &&
      ((lWidth) > (SInt32) rvalue_size))
  {
    NUVarselLvalue* vlv = dynamic_cast<NUVarselLvalue*>(the_lvalue);
    NU_ASSERT(vlv, the_lvalue);
    vlv->putSize(rvalue_size);
  }

  temp_err_code = helperAssign( the_lvalue, the_rvalue, eSequentialNonBlocking,
                                context, loc, assignCreated ); // rjc, assignCreated should be a ptr, not ref
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  if ( !assignCreated )
  {
    *the_assign = context->stmtFactory()->createNonBlockingAssign(the_lvalue, the_rvalue, false, loc);
    return eSuccess;
  }

  return err_code;
}
