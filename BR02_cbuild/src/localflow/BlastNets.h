// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _BLASTNETS_H_
#define _BLASTNETS_H_

#include "util/CarbonTypes.h"

class NUDesign;
class NUModule;
class NUNetRefFactory;
class ArgProc;
class MsgContext;
class AtomicCache;
class IODBNucleus;

class SeparationStatistics;

//! BlastNets class
/*! Implements blastNet by using reduce/Separation to bit-blast
 *  selected nets.
 */
class BlastNets
{
public:
  //! constructor
  BlastNets(NUNetRefFactory* net_ref_factory, ArgProc* args, MsgContext* msg_context,
            AtomicCache* str_cache, IODBNucleus* iodb, bool verbose_fold,
            bool verbose_separation);

  //! destructor
  ~BlastNets();

  //! Walk all the modules in the given design.
  void design(NUDesign *design, SeparationStatistics* separation_stats);

  //! Walk the given module.
  void module(NUModule *module, SeparationStatistics* separation_stats);

private:
  //! Hide copy and assign constructors.
  BlastNets(const BlastNets&);
  BlastNets& operator=(const BlastNets&);

  //! Net ref factory
  NUNetRefFactory* mNetRefFactory;

  //! Arguments
  ArgProc* mArgs;

  //! Message context
  MsgContext* mMsgContext;

  //! String cache
  AtomicCache* mStrCache;
  
  //! IODB - for access to blastNet directives
  IODBNucleus* mIODB;

  //! Tells separation to blast nets and whether or not to create continuous assigns
  UInt32 mSeparationMode;

  //! Verbosity flags
  bool mVerboseFold;
  bool mVerboseSeparation;
};

#endif // _BLASTNETS_H_
