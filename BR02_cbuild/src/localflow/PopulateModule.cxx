// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "LFContext.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "symtab/STSymbolTable.h"
#include "util/UtConv.h"
#include "util/UtStack.h"
#include "util/AtomicCache.h"
#include "util/UtIOStream.h"
#include "iodb/IODBNucleus.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlTime.h"
#include "compiler_driver/CarbonContext.h"
#include "localflow/FindUses.h"
#include "compiler_driver/CheetahContext.h"


const char* gGetObjTypeStr(veObjType objType);

//! Class to maintain unique parameterized module names
/*!
  This does not keep all the information for all parameterized
  modules. This is simply a map of constructed name based on parameter
  values to unique ids. 
  This does, however, keep all the parameter names for each
  parameterized module, so Cheetah doesn't have to supply us with the
  parameter name every time through the analyzeInstanceParameters
  call.

  As a result of not keeping all the information for a given instance
  of a parameterized module, we write out a file,
  libdesign.parameters, during the analysis process. Currently,
  keeping all the information around would be horriblely memory
  consuming. We could avoid this by implementing a more savvy hashing
  strategy based on parameter values instead of strings.
*/
class VerilogPopulate::ParamModuleMap
{

public:
  //! A simple class that wraps a parameter
  class Element
  {
  public:
    Element(const char* name) : mName(name)
    {}
    
    //! Put the interpreted value of the parameter
    void putStr(const UtString& valueStr)
    {
      mValueString.assign(valueStr);
    }

    //! Get the value str
    const UtString& getValueStr() const {
      return mValueString;
    }

    //! Get the parameter name
    const UtString& getName() const {
      return mName;
    }

  private:
    UtString mValueString;
    UtString mName;
    
    // forbid
    Element();
    Element(const Element&);
    Element& operator=(const Element&);
  };

  //! Map of modules to unique instances
  /*!
    The 'unique instances' in this case are represented by a simple
    UInt32 which increments everytime a unique instance of a
    parameterized module is encountered. The map is really from
    \<unique string id\> to \<index\>. The unique string id is constructed
    by taking all the value strs of the parameters and concating them
    together. 
   */
  class ParamListObj
  {
    typedef UtArray<Element*> ElementVec;
    typedef UtHashMap<UtString, UInt32> StrToIntMap;

  public:
    ParamListObj() : mUniqueNum(0), mInit(false) {}

    ~ParamListObj()
    {
      for (ElementVec::iterator p = mElements.begin(), e = mElements.end(); 
           p != e; ++p)
        delete *p;
    }
    
    //! Have all the parameter names been initialized?
    bool isInitialized() const { return mInit; }

    //! Set whether or not this has been initialized
    void putInit(bool isInit) { mInit = isInit; }

    //! Number of parameters
    UInt32 size() const { return mElements.size(); }

    //! Get the parameter at specified index (0 - size-1)
    Element* getElement(UInt32 index) {
      // warning: unchecked. Caller must check.
      return mElements[index];
    }
    
    //! Add a parameter
    Element* addElement(const UtString& name)
    {
      Element* elem = new Element(name.c_str());
      mElements.push_back(elem);
      return elem;
    }

    //! Construct the unique id.
    /*!
      Returns true if this id has never been seen before.
    */
    bool constructSuffix(UtString* suffix)
    {
      *suffix << '_';

      bool isUnique = false;
      UtString fullAttribs;
      for (ElementVec::const_iterator p = mElements.begin(), e = mElements.end();
           p != e; ++p)
      {
        const Element* elem = *p;
        const UtString& valueStr = elem->getValueStr();
        fullAttribs.append(valueStr);
        fullAttribs << '_';
      }
      // remove trailing '_'
      INFO_ASSERT(! fullAttribs.empty(), "inconsistent string");
      fullAttribs.erase(fullAttribs.size() - 1);
      
      StrToIntMap::const_iterator q = mMap.find(fullAttribs);
      if (q != mMap.end())
      {
        UInt32 val = q->second;
        *suffix << "#" << val;
      }
      else
      {
        *suffix << "#" << mUniqueNum;
        mMap[fullAttribs] = mUniqueNum;
        ++mUniqueNum;
        isUnique = true;
      }
      
      return isUnique;
    }

  private:
    ElementVec mElements;
    StrToIntMap mMap;
    UInt32 mUniqueNum;
    bool mInit;

    ParamListObj(const ParamListObj&);
    ParamListObj& operator=(const ParamListObj&);
  };

private:
  typedef UtHashMap<UtString, ParamListObj*> StrToParamListObj;

public:
  //! Open the libdesign.parameters file and init this object
  ParamModuleMap(const char* fileRoot, MsgContext* msgContext) 
  {
    mOutFileIsInit = false;
    UtString fileName(fileRoot);
    fileName << ".parameters";
    mOutFile = new UtOBStream(fileName.c_str());
    
    if (mOutFile->bad())
      msgContext->ParamMapFileError(mOutFile->getErrmsg());
  }
  
  ~ParamModuleMap() {
    delete mOutFile;

    for (StrToParamListObj::UnsortedLoop p = mModToParamListObj.loopUnsorted(); ! p.atEnd(); ++p)
    {
      ParamListObj* pl = p.getValue();
      delete pl;
    }
  }

  //! Get the list of unique modules for the given original name.
  ParamListObj* getParamList(UtString& origName)
  {
    ParamListObj* ret = NULL;
    StrToParamListObj::iterator p = mModToParamListObj.find(origName);
    if (p != mModToParamListObj.end())
      ret = p->second;
    else
    {
      ret = new ParamListObj;
      mModToParamListObj[origName] = ret;
    }
    
    return ret;
  }

  //! Write initial comments to the file.
  /*!
    This is only written once. We don't write anything to the file if
    there are no parameterized modules. So, this function gets called
    every time we encounter a parameterized module instance.
  */
  void maybeWriteInitialComments()
  {
    if (! mOutFileIsInit)
    {
      UtOBStream& out = *mOutFile;
      
      out << "# Map of a original module name to uniquified module name and parameter values" << UtIO::endl;
      out << "#" << UtIO::endl;
      out << "# <uniquified_name> -> <original_name>" << UtIO::endl;
      out << "#      <parameter_1> = <val>" << UtIO::endl;
      out << "#      <parameter_2> = <val>" << UtIO::endl;
      out << "#      ..." << UtIO::endl;
      out << "# ..." << UtIO::endl << UtIO::endl;
      
      mOutFileIsInit = true;
    }
  }

  //! Get the output file stream
  UtOBStream& getParameterMapFile()
  {
    return *mOutFile;
  }

private:
  StrToParamListObj  mModToParamListObj;
  UtOBStream* mOutFile;
  bool mOutFileIsInit;

  ParamModuleMap();
  ParamModuleMap(const ParamModuleMap&);
  ParamModuleMap& operator=(const ParamModuleMap&);
};


StringAtom* VerilogPopulate::getOriginalName(veNode mod) {
  UtString buf;
  mTicProtectedNameMgr->getVisibleName(mod, &buf, true /* use original name*/);
  return mStrCache->intern(buf.c_str());
 
  // The tokens-file generator needs to keep track of the
  // original names of uselib-uniquified modules, and record
  // that in an aux file or a meta-comment or something.  Here
  // is where we should decode that and override the original
  // name.  That infrastructure is not built yet.
  //
  // This code needs to get the original name only if the name was not 'protected
  //   StringAtom* actualSym = mStrCache->intern(actualName);
  //   mCheetahContext->findOriginalModuleName(actualSym);
}
  

/*
 * Create a module object from the given Cheetah veNode.
 *
 * TBD: For now, if there are any parameters declared in the module, then this
 * module will not be re-used (will create a fully-populated Nucleus object each
 * time).
 */
Populate::ErrorCode VerilogPopulate::moduleOrUDP( veNode ve_module, // module or UDP
						  LFContext *context,
						  const char *uniquifiedName,
                                                  const char* unit, 
                                                  const char* precision,
						  NUModule **the_module,
                                                  bool* seenAlready )
{
  ErrorCode err_code = eSuccess;
  *the_module = 0;
  *seenAlready = false;

  bool isUDP = false;
  ErrorCode module_err_code = findIfModuleOrUDP(ve_module, &isUDP);
  if ( errorCodeSaysReturnNowWhenFailPopulate(module_err_code, &err_code ) )
  {
    return module_err_code;
  }

  SourceLocator loc = locator(ve_module);

  UtString moduleName;
  LOC_ASSERT (uniquifiedName, loc); 
  moduleName = uniquifiedName;
  
  AtomicCache* cache = context->getStringCache();
  StringAtom* moduleNameAtom = cache->intern(moduleName.c_str());
  NUModule *find_module = 0;
  if ((find_module = context->lookupModule(moduleNameAtom))) {
    context->putLastVisitedModule(find_module);
    *seenAlready = true;
    context->pushBlockScope(find_module);
    context->pushDeclarationScope(find_module);
    context->pushExtraTaskEnableContext();

    return eSuccess;
  }
  

  NUDesign * design = context->getDesign();
  STSymbolTable * aliasDB = new STSymbolTable(design->getAliasBOM(), cache);
  aliasDB->setHdlHier(new HdlVerilogPath);

  // Figure out if this is a c-model or normal module. For c-models we
  // ignore the contents of the module but we still process the
  // ports. We also don't allow top level c-models.
  //
  // Note that because Cheetah modifies the module name with uselib we
  // have to do some possible conversions.
  //  bool isCModel       = mIODB->isCModel(modName);

  // Get the timescale information from Cheetah
  unsigned st = 0;
  SInt8 timeUnit = HdlTime::parseTimeString( unit, &st );
  // apply a little arithmetic: 1,3 - report warning, 2,3 - report error
  if (st % 2) {
      mMsgContext->TimeStringRounded(&loc, unit, HdlTime::VerilogTimeUnits[(unsigned)timeUnit]); // this is a warning
  }
  if (st > 2) {
      mMsgContext->UnsupportedTimeString(&loc, unit); // this is an error
  }
  SInt8 timePrecision = HdlTime::parseTimeString( precision, &st );
  if (st % 2) {
      mMsgContext->TimeStringRounded(&loc,  precision, HdlTime::VerilogTimeUnits[(unsigned)timePrecision]); // this is a warning
  }
  if (st > 2) {
      mMsgContext->UnsupportedTimeString(&loc, precision); // this is an error
  }
  bool timescaleSpecified = ( unit != NULL );
  design->setGlobalTimePrecision( timePrecision );
  
  bool is_top_level = (not context->getModule());
  
  // If this is the top level of the design, pass the design into the
  // constructor.
  *the_module = new NUModule(design, is_top_level, moduleNameAtom, aliasDB,
                             context->getNetRefFactory(),
                             cache, loc, mIODB,
                             timeUnit, timePrecision, timescaleSpecified,
                             eSLVerilog);
  if (is_top_level) {
    context->maybeSetRootModule( *the_module );
  }

  StringAtom* modSym = getOriginalName(ve_module);
  (*the_module)->putOriginalName(modSym);
  design->addModule(*the_module);
  context->pushBlockScope(*the_module);
  context->pushDeclarationScope(*the_module);
  context->pushExtraTaskEnableContext();
  
  context->mapModule(moduleNameAtom, *the_module);

  return err_code;
}

Populate::ErrorCode VerilogPopulate::udpComponents( veNode ve_udp,
                                                    NUModule* module,
                                                    LFContext* context )
{
  ErrorCode err_code = eSuccess;

  // local nets already created

  if( veUdpGetIsSequential( ve_udp ) ) {
    ErrorCode ec = udpSequentialComponents( ve_udp, context );
    if( errorCodeSaysReturnNowOnlyWithFatal( ec, &err_code ) )
      return ec;
  } else {
    NUContAssign* the_assign = NULL;
    ErrorCode ec = udpContAssign( ve_udp, context, &the_assign );
    if( errorCodeSaysReturnNowOnlyWithFatal( ec, &err_code ) )
      return ec;
    
    if( the_assign )
      module->addContAssign(the_assign);
  }
  
  // Add an initial block to represent the UDP initial statement.
  veNode ve_init = veUDPGetInitialStmt( ve_udp );
  if( ve_init ) {
    NUInitialBlock* the_initial = NULL;
    ErrorCode ec = initialBlock(ve_init, context, &the_initial);
    if( errorCodeSaysReturnNowOnlyWithFatal( ec, &err_code ) )
      return ec;
	
    if (the_initial)
      module->addInitialBlock(the_initial);
  }
  
  SourceLocator loc = locator( ve_udp );
  if ( not loc.isTicProtected() ) {
    // never print a hint that there is a udp within a protected region
    int verbosity;
    mArg->getIntLast( CRYPT("-verboseUDP"), &verbosity );

    if( verbosity >= 1 ) {
      // Message for UDP parsing.
      mMsgContext->ParsedUDP( &loc, module->getName()->str() );
    
      if( verbosity >= 2 ) {
        // Decompile parsed UDPs for debugging.
        UtString buf;
        context->getModule()->compose(&buf, NULL,  0, true);
        UtIO::cout() << buf.c_str() << UtIO::endl;
      }
    }
  }

  return err_code;
}

// Method to create an Initial Block if not present and then add stmt to it.
void Populate::addStmtToInitialBlock(LFContext *context,
                                     NUModule *module,
                                     NUStmt* stmt,
                                     const NUBase* proxy,
                                     const SourceLocator &loc)
{
  bool inTFHierScope = context->getDeclarationScope()->inTFHier();
  NUBlock* block = NULL;
  if (!inTFHierScope)
  {
    NUModule::InitialBlockLoop p = module->loopInitialBlocks();
    if (!p.atEnd()) {
      NUInitialBlock* iblock = *p;
      block = iblock->getBlock();
    }
    else {
      NUStmtList stmts;
      NUNetRefFactory* nrf = context->getNetRefFactory();
      block = new NUBlock(&stmts, module, mIODB, nrf, false, loc);
      StringAtom* sym = module->gensym("initial_val", NULL, proxy);
      NUInitialBlock* iblock = new NUInitialBlock(sym, block, nrf, loc);
      module->addInitialBlock(iblock);
    }
  }

  if (inTFHierScope)
    context->addStmt(stmt);
  else 
    block->addStmt(stmt);
}


Populate::ErrorCode VerilogPopulate::processModuleDeclAssigns(veNode ve_module,
                                                              LFContext* context)
{
  ErrorCode err_code = eSuccess;
  
  NUModule* module = context->getModule();
  
  CheetahList ve_net_iter(veModuleGetNetList(ve_module));
  if (ve_net_iter) {
    veNode ve_net;
    while ((ve_net = VeListGetNextNode(ve_net_iter))) {
      // Find the net we've just declared
      NUNet* net;
      mPopulate->lookupNet (context->getDeclarationScope (), ve_net, &net, true);
      if (net == NULL)
        continue;
      
      // Verilog 2001 says REG, TIME, INTEGER, REAL can be initialized as if the assignment
      // was in an initial block...
      if (net->isReg ()) {
        // Initialize with an assignment in an initial block
        NUBlockingAssign* the_assign = 0;
        ErrorCode assign_err_code = declAssign(ve_net, context, &the_assign);
        if ( errorCodeSaysReturnNowOnlyWithFatal(assign_err_code, &err_code ) )
        {
          return assign_err_code;
        }

        if (the_assign)
          addStmtToInitialBlock (context, module, the_assign, 0, the_assign->getLoc ());
      } else {
        NUContAssign* the_assign = 0;
        ErrorCode assign_err_code = declAssign(ve_net, context, &the_assign);
        if ( errorCodeSaysReturnNowOnlyWithFatal(assign_err_code, &err_code ) )
        {
          return assign_err_code;
        }

        if (the_assign) {
          module->addContAssign(the_assign);
        }
      }
    }
  }

  // Get top level time, integer, real variables and process any initialization
  // expression as if it was in an initial block.
  CheetahList ve_var_iter(veModuleGetVariableList (ve_module));
  if (ve_var_iter) {
    while (veNode ve_var = VeListGetNextNode (ve_var_iter)) {
      NUBlockingAssign* the_assign = 0;
      ErrorCode assign_err_code = declAssign (ve_var, context, &the_assign);
      if (errorCodeSaysReturnNowOnlyWithFatal (assign_err_code, &err_code) )
        return assign_err_code;
      
      if (the_assign)
        addStmtToInitialBlock (context, module, the_assign, 0, the_assign->getLoc ());
    }
  }

  return err_code;
}

Populate::ErrorCode
VerilogPopulate::addSensitivityEvents(LFContext* context, NUModule* module,
                                      NUAlwaysBlock* always,
                                      const NUExprLocList& expr_loc_list,
                                      const UtString& reasons)
{
  // Get the uses for this always block. We will need it later for a
  // warning message.
  NUNetRefSet alwaysUses(mNetRefFactory);
  LFFindUsesCallback alwaysCallback(&alwaysUses);
  NUDesignWalker walker(alwaysCallback, false, false);
  walker.alwaysBlock(always);

  // Go through the sensitivity list and either find or create change
  // detects for each. We accumulate the booleans as we go. We also
  // accumulate the sensitivity uses for the warning messages.
  NUNetRefSet senseUses(mNetRefFactory);
  NUNetVector nets;
  for (NUExprLocListIter i = expr_loc_list.begin(); i != expr_loc_list.end(); ++i) {
    // Accumulate the uses
    const NUExprLocPair expr_loc = *i;
    NUExpr* expr = expr_loc.first;
    const SourceLocator& loc = expr_loc.second;
    expr->getUses(&senseUses);

    // Check if we already have a change detect for this expression
    NUNet* changeNet = context->findChangeDetectBool(expr);
    if (changeNet == NULL) {
      // Create a new change detect operator (RHS of assignment)
      NUExpr* change = new NUChangeDetect(expr);
      change->resize(1);

      // Create the LHS of an assignment
      StringAtom* atom = module->gensym("changebool");
      changeNet = module->createTempNet(atom, 1, false, loc);
      changeNet->putIsClearAtEnd(true);
      NULvalue* lhs = new NUIdentLvalue(changeNet, loc);

      // Create the assignment for the result of the change detection
      StringAtom* blockName = module->newBlockName(mStrCache, loc);
      NUContAssign* assign = new NUContAssign(blockName, lhs, change, loc);
      module->addContAssign(assign);

      // Add this to our cache
      context->addChangeDetectBool(expr, changeNet);

      // now we need to mark all of the signals involved in the
      // sensivity list as protected observable so that they will not
      // be rescoped
      for (NUNetRefSet::NetRefSetLoop l = senseUses.loop(); !l.atEnd(); ++l) {
        const NUNetRefHdl netRef = *l;
        NUNet *net = netRef->getNet();

        // mark net as protected observable
        net->putIsProtectedObservableNonHierref(mIODB);
      }
    } else {
      // Don't need this expression
      delete expr;
    }

    // Add the new/existing boolean to our list for later processing
    nets.push_back(changeNet);
  } // for

  // Gather the statements from the block in the always block. We will
  // put them in the then clause for the event detection.
  NUStmtList stmts;
  NUBlock* block = always->getBlock();
  for (NUStmtLoop l = block->loopStmts(); !l.atEnd(); ++l) {
    NUStmt* stmt = *l;
    stmts.push_back(stmt);
  }

  // Create the conditional expression for the if statement from the
  // change net bools.
  NUExpr* cond = NULL;
  NU_ASSERT(!nets.empty(), always);
  for (NUNetVectorLoop l(nets); !l.atEnd();  ++l) {
    // Create a sub conditional for this net
    NUNet* net = *l;
    NUExpr* subCond = new NUIdentRvalue(net, net->getLoc());

    // Merge the conditionals together in a big boolean OR
    if (cond == NULL) {
      // First conditional
      cond = subCond;
    } else {
      // Or the conditionals
      cond = new NUBinaryOp(NUOp::eBiLogOr, cond, subCond, net->getLoc());
    }
  }
  cond->resize(1);

  // Create the if statement with all the original block statements
  NUStmtList elseStmts;
  NUStmt* ifStmt = new NUIf(cond, stmts, elseStmts, mNetRefFactory, false,
                            always->getLoc());

  // Replace the block statements with the new ifStmt
  stmts.clear();
  stmts.push_back(ifStmt);
  block->replaceStmtList(stmts);

  // Print warnings about uses in the always block that are not in the
  // sensitivity list.
  NUNetRefSet diffs(mNetRefFactory);
  NUNetRefSet::set_difference(alwaysUses, senseUses, diffs);
  for (NUNetRefSet::SortedLoop l = diffs.loopSorted(); !l.atEnd(); ++l) {
    // Compose a name for the net ref
    const NUNetRefHdl netRef = *l;
    UtString name;
    netRef->compose(&name, NULL);

    // Print the warning
    mMsgContext->MissingSense(&(always->getLoc()), name.c_str(), reasons.c_str(),
                              name.c_str());
  }
  return eSuccess;
} // VerilogPopulate::addSensitivityEvents

Populate::ErrorCode
VerilogPopulate::addAlwaysBlock(veNode ve_always, LFContext* context,
                                NUModule* module)
{
  ErrorCode err_code = eSuccess;
  NUAlwaysBlock* the_always = 0;
  NUExprLocList* expr_loc_list = new NUExprLocList;
  ErrorCode always_err_code = alwaysBlock(ve_always, context, &the_always,
                                          expr_loc_list);
  if ( errorCodeSaysReturnNowOnlyWithFatal(always_err_code, &err_code ) ) {
    delete expr_loc_list;
    return always_err_code;
  }

  if (the_always) {
    module->addAlwaysBlock(the_always);

    if (expr_loc_list->empty()) {
      delete expr_loc_list;
    }
    else {
      // If we have a non-empty expression list, this always block
      // requires sensitivity event testing.  Do that after population
      // has mostly completed, so that design walks will work properly.
      // See test/hierref_unique/test7.v for a case that asserts out
      // due to hier-ref task resolutions being incomplete, if we were
      // to do the sensitivity fixup right here.
      SensitivityInfo si;
      si.always = the_always;
      si.exprLocList = expr_loc_list;
      si.module = module;
      context->getRequiresSensitivityEvents(&si.reasons);
      mSensitivityInfoList.push_back(si);
    }
  } else {
    if (expr_loc_list->empty()) {
      delete expr_loc_list;
    } else {
      NU_ASSERT(0, module);
    }
  }

  return err_code;
} // VerilogPopulate::addAlwaysBlock

void VerilogPopulate::sDoEvalDouble(ParamValue* paramVal, veNode expr)
{
  double valDbl = veExprEvaluateValueInDouble(expr);
  paramVal->putDbl(valDbl);
  paramVal->putSize(veExprEvaluateWidth(expr));
}

//! Local function to do value optimization (try to reduce the length of a binary string to make canonical)
/*!
 * The string coming in here should be the result of call to veExprEvaluateValue, so it is just a string
 * of values (there isn't any based number or sizing information).
 */
static void sDoValOpt(UtString& buf, UInt32& size)
{
  UtString::size_type pos = buf.find_first_not_of("0");
  if (pos != UtString::npos) {
    size -= pos;
    buf.erase(0, pos);
  }
}

Populate::ErrorCode VerilogPopulate::getParamValueString(ParamValue* paramVal,
                                                         veNode expr)
{
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(expr);

  veString valStr = NULL;

  veNode useParent;

  veObjType type = veNodeGetObjType(expr);
  paramVal->putType(type);
  UtString& buf = paramVal->getStr();

  // handle some 2001 constructs first
  switch ( type ){
  case VE_PARAMCONN:{
    expr = veParamConnGetValue(expr);
    return getParamValueString(paramVal, expr);
  }
    break;
  case VE_GENVAR:{
    CheetahStr value(veNamedObjectEvaluateValueInBinaryString(expr));
    paramVal->putValue(veGetDecimalNumberFromBinaryString(value));
    buf << paramVal->getParamVal();
    paramVal->putSize(32);
    return err_code;
  }
    break;
  case VE_LOCALPARAM: {
    expr = veLocalParamGetValue(expr);
    return getParamValueString(paramVal, expr);
  }
    break;
  default: break;
  }

  // Is the string a Verilog based number?  If not, then it is just a string of values.
  bool basedNumberString = false;

  bool useDouble = false;
  veExprValueType exprType = veExprGetValueType(expr);

  switch (exprType) {
  case LOCALLY_STATIC:
  case GLOBALLY_STATIC:
    // Note that this size is incorrect for Verilog strings, that will be fixed up below.
    paramVal->putSize(veExprEvaluateWidth(expr));
    switch(type)
    {
    case VE_CONST:
      break;
    case VE_STRING:
      valStr = veStringGetString(expr);
      break;
    case VE_PARAM:
      expr = veParamGetCurrentValue(expr);
      return getParamValueString(paramVal, expr);
      break;
    case VE_NAMEDOBJECTUSE:
      useParent = veNamedObjectUseGetParent(expr);
      switch (veNodeGetObjType(useParent))
      {
      case VE_PARAM:
        expr = veParamGetCurrentValue(useParent);
        return getParamValueString(paramVal, expr);
        break;
      case VE_PARAMCONN:
        expr = veParamConnGetValue(useParent);
        return getParamValueString(paramVal, expr);
        break;
      default:
        return getParamValueString(paramVal, useParent);
        break;
      }
      break;
    case VE_BASEDNUMBER:
      if (paramVal->doConvertBasedNumber()) {
        valStr = veBasedNumberGetBinaryValueString(expr);
      } else {
        // String will be of the form "<size>'<base><val>", and not just a raw value string.
        valStr = veExprDecompileToString(expr);
        basedNumberString = true;
      }
      break;
    case VE_FLOATNUMBER:
    case VE_EXPNUMBER:
      sDoEvalDouble(paramVal, expr);
      useDouble = true;
      break;
    default:
      // An expression which needs evaluation.
      if (veCheckExprForRealEval(expr)) {
        sDoEvalDouble(paramVal, expr);
        useDouble = true;

      } else {
        // Do nothing, the evaluation will occur below.
      }
      break;
    }
    break;
  default:
    {
      POPULATE_FAILURE(mMsgContext->NonStaticParameter(&loc), &err_code);
      return err_code;
    }
  }
  
  if (useDouble) {
    // we need to change the double to binary. This
    // is not difficult. Simply copy the double to a UInt64, then use
    // writeBinValToStr
    CarbonReal curValue = paramVal->getDblValue();
    UInt64 castValue = 0;
    CarbonValRW::cpSrcToDest(&castValue, &curValue, 2);
    // a 64 bit value cannot be greater than 64 bits + \0
    char convBuf[65];
    int convertStat = CarbonValRW::writeBinValToStr(convBuf, 65, &castValue, 64, false);
    // This will never fire unless someone changes the buffer size.
    INFO_ASSERT(convertStat != -1, "Double value conversion to binary failed.");

    buf.assign(convBuf, 64);

  } else {
    if (!valStr) {
      veBool dummy;
      valStr = veExprEvaluateValueInString(expr, &dummy, VE_BIN); // convert to a binary string so we can count the bits to find the size,
                                                                  // also use this string to get the value of the loop index bug16445
    }

    UInt32 size = strlen(valStr);
    if ((*valStr == '"') && (valStr[size - 1] == '"')) {
      // Note that the size returned by veExprEvaluateWidth is incorrect for Verilog strings,
      // so we don't check that the strlen matches veExprEvaluateWidth.

      // Handle Verilog strings
      if (type != VE_STRING) {
	POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad string in parameter"), &err_code);
	return err_code;
      }

      // Remove the leading and trailing quotation marks.
      size -= 2;
      buf.assign(valStr + 1, size);
      paramVal->putSize(size);

    } else {
      if (type == VE_STRING) {
	POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad string in parameter"), &err_code);
	return err_code;
      }

      // Put the string into the parameter.
      buf.assign(valStr, size);

      if (basedNumberString) {
        // For a based number string, cannot make the size smaller and do not try to evaluate into
        // a 32-bit integer since don't want to parse the based number for special values.
      } else {
        // For types other than Verilog strings and based numbers, the veExprEvaluateWidth size should
        // match the string length.
        INFO_ASSERT(paramVal->getSize() == size, "Parameter size doesn't match with string value length.");

        bool xzFlag = (buf.find_first_of("xz") != UtString::npos);

        // Try to evaluate to an integer value if the size is small enough and this is not an xz binary value.
        if (not xzFlag and (size <= 32)) {
          UInt32 val = veGetDecimalNumberFromBinaryString(valStr); // do not use (veExprEvaluateValue(expr)) here, it will not return the
                                                                   // correct value if expr is a part select of a generate FOR index
                                                                   // variable (bug 16445)
          paramVal->putValue(val);
        }
        if (! paramVal->doNoValOpt()) {
          sDoValOpt(buf, size);
          paramVal->putSize(size);
        }
      }
    }

  }
  
  if (valStr && (type != VE_STRING)) {
    veFreeString(valStr);
  }
  
  return err_code;
}

veNode VerilogPopulate::substituteModule(mvvNode old_mod,
                                         mvvLanguageType old_mod_lang,
					 bool is_udp,
					 UtString& new_mod_name,
					 TicProtectedNameManager* ticProtectedNameMgr)
{
  veNode cheetahRoot = veCheetahGetRoot();

  if(NULL == cheetahRoot) return NULL;
  
  veNode newMod = ticProtectedNameMgr->CheetahFindModule(new_mod_name.c_str(), cheetahRoot);
  
  if (!newMod)
  {
    return NULL;
  }
  // We found a substituted module.
  
  // Build a parameter list for the new module.  Copy any parameters that
  // match from the old module into the new list.
  if (is_udp == false) 
  {
    // If the replaced module (old module) is a VHDL entity, transform it to
    // a verilog module and use it for parameter list creation.
    veNode old_vlog_mod = NULL;
    JaguarString old_mod_file;

    if (old_mod_lang == MVV_VERILOG)
    {
      old_vlog_mod = old_mod->castVeNode();
    }
    else
    {
      mvvNode xform_mod = mvvTransformDesignUnit(old_mod);
      old_vlog_mod = xform_mod->castVeNode();
      old_mod_file = vhGetSourceFileName(old_mod->castVhNode());
    }

    // Build a map of old parameter names to it's value.
    UtMap<UtString, veNode> param_val_map;
    CheetahList param_list(veModuleGetParamList(old_vlog_mod));
    veNode old_param = NULL;
    while ((old_param = veListGetNextNode(param_list)) != NULL)
    {
      veNode param_val = veParamGetCurrentValue(old_param);
      UtString oldname_buf;
      if (old_mod_lang == MVV_VERILOG)
      {
        // The name could be protected.
        ticProtectedNameMgr->getVisibleName(old_param, &oldname_buf);
      }
      else
      {
        // Protected names not supported in VHDL yet.
        gCheetahGetName(&oldname_buf, old_param);
        // Set the file name of parameter value to old module file.
        veNodeSetFileName(param_val, old_mod_file.get_nonconst());
      }
      param_val_map[oldname_buf] = param_val;
    }

    // Copy parameters that match from old module to new module list.
    CheetahList new_param_list(vewCreateNewList());
    CheetahList npi(veModuleGetParamList(newMod));

    while (veNode newParam = veListGetNextNode(npi))
    {
      UtString newname_buf;
      ticProtectedNameMgr->getVisibleName(newParam, &newname_buf);
      UtMap<UtString, veNode>::iterator itr = param_val_map.find(newname_buf);
      veNode param_val = NULL;
      if (itr != param_val_map.end())
      {
        // Found matching old parameter. Use it's value.
        param_val = itr->second;
      }
      else
      {
        // No matching parameter found, use default new parameter value.
        param_val = veParamGetCurrentValue(newParam);
      }
      vewAppendToList(new_param_list, param_val);
    }

    // Set the parameter list for substituted module.
    veModuleSetParamValues( newMod, new_param_list );
  }
  return newMod;
}

Populate::ErrorCode 
VerilogPopulate::getParameterRange(veNode leftExpr, veNode rightExpr, ConstantRange* range)
{
  ErrorCode err_code = eSuccess;

  ParamValue leftVal;
  ParamValue rightVal;

  ErrorCode temp_err_code = getParamValueString(&leftVal, leftExpr);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  
  temp_err_code = getParamValueString(&rightVal, rightExpr);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  SourceLocator loc = locator(leftExpr);
  if (not leftVal.hasValidParamVal() or not rightVal.hasValidParamVal()) {
    POPULATE_FAILURE(mMsgContext->CannotEvaluateParameterRange(&loc), &err_code);
  } else {
    range->setMsb(leftVal.getParamVal());
    range->setLsb(rightVal.getParamVal());
  }

  return err_code;
}

StringAtom* VerilogPopulate::getInstanceName(veNode ve_inst,
                                             const SInt32 *index,
                                             LFContext *context)
{
  if ( mUseLegacyGenerateHierarchyNames ) {
    SourceLocator loc = locator(ve_inst);
    StringAtom *instName = VerilogPopulate::getInstanceMangledName(ve_inst, context, loc, mTicProtectedNameMgr);
    UtString   inst_name(instName->str());
  
    // If a range index value is given, append "[index]" to the instance name
    if (index) {
      inst_name << "[" << *index << "]";
    }
  
    StringAtom* name = context->getStringCache()->intern(inst_name.c_str());
    return name;
  } else {
    UtString suffix;
    // If a range index value is given, append "[index]" to the instance name
    if (index) {
      suffix << "[" << *index << "]";
    }
 
    UtString inst_name;
    (void) VerilogPopulate::composeInstanceName(ve_inst, context, &inst_name, mTicProtectedNameMgr, &suffix);
 
    StringAtom* name = context->getStringCache()->intern(inst_name.c_str());
    return name;
  }
}

Populate::ErrorCode VerilogPopulate::oneModuleInstancePost(veNode ve_inst,
                                                           veNode new_module,
                                                           const UInt32 *offset,
                                                           LFContext *context)
{
  SourceLocator loc = locator(ve_inst);
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  
  NUScope* parent = mUseLegacyGenerateHierarchyNames ? context->getModule() : context->getDeclarationScope();

  NUModule* the_module = context->getLastVisitedModule();

  StringAtom* name = context->getInstanceName();
  
  NUModuleInstance *the_instance = new NUModuleInstance(name, parent, the_module, context->getNetRefFactory(), loc);

  NUPortMap* portMap = context->getPortMap();
  
  int num_ports = the_module->getNumPorts();
  NUPortConnectionVector portconns(num_ports);
  
  // Initialize to 0 because not all ports may have connections.
  std::fill(portconns.begin(), portconns.end(), (NUPortConnection*)0);
  
  CheetahList ve_portconn_iter(veInstanceGetTerminalConnectionList(ve_inst));
  if (ve_portconn_iter) {
    veNode ve_portconn;
    ve_portconn = veListGetNextNode(ve_portconn_iter);
    
    NUNetSet formal_ports_processed;
    
    while ( ve_portconn ) {
      NUNet* port = 0;
      SourceLocator loc = mPopulate->locator( ve_portconn );
      
      VeNodeList portconns_for_this_port;
      temp_err_code = gatherPortconnsForSinglePort(the_module, &ve_portconn, &ve_portconn_iter, new_module, portMap, &portconns_for_this_port, &port);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
      
      // at this point ve_portconn and ve_portconn_iter are pointing
      // to an actual beyond the one(s) that will be connected to the
      // current formal port. 
      
      // Empty port if port == 0
      if (port) {
        NUPortConnection* portconn = 0;
        NU_ASSERT ((portconns_for_this_port.size()), port);
        
        if (formal_ports_processed.find(port) != formal_ports_processed.end()) {
          POPULATE_FAILURE(mMsgContext->MultiPortNet(port, "This formal port net appears in multiple and/or non-adjacent, positions in the port list."), &err_code);
          return err_code;
        }
        formal_ports_processed.insert(port);
        
        ErrorCode port_err_code = portConnection(portconns_for_this_port,
                                                 context,
                                                 port,
                                                 the_module,
                                                 offset,
                                                 loc,
                                                 &portconn);
        
        if ( errorCodeSaysReturnNowOnlyWithFatal(port_err_code, &err_code ) )
        {
          return port_err_code;
        }
        
        if (portconn) {
          portconn->setModuleInstance(the_instance);
        }
        
        int i = the_module->getPortIndex(port);
        if (not ((i >= 0) && (i < num_ports))) {
          mMsgContext->CheetahConsistency(&loc, "Out of bounds port index");
          return eFatal;
        }
        portconns[i] = portconn;
      }
      
    }    
  }

  // Create port connections for unconnected ports.
  for (int i = 0; i < num_ports; ++i) {
    if (portconns[i] == 0) {
      NUNet *port = the_module->getPortByIndex(i);
      NUPortConnection* portconn = 0;
      ErrorCode port_err_code = unconnectedPort(ve_inst, port, context, &portconn);
      if ( errorCodeSaysReturnNowOnlyWithFatal(port_err_code, &err_code ) )
      {
        return port_err_code;
      }
      portconn->setModuleInstance(the_instance);
      portconns[i] = portconn;
    }
  }
  
  the_instance->setPortConnections(portconns);

  // Add the instance to the calling module or enclosing NamedDeclarationScope
  addModuleInstance(the_instance, parent);
  
  return err_code;
}

NUOp::OpT VerilogPopulate::getOpFromGatePrimitive(veNode ve_prim, LFContext *, bool* invertResult)
{
  NUOp::OpT op = NUOp::eInvalid;
  bool invert = false;
  const char* name = NULL;

  switch (veGateInstanceGetType(ve_prim)) {
  case NAND_USE:
    invert = true;
    // fall-thru
  case AND_USE:
    op = NUOp::eBiBitAnd;
    break;

  case NOR_USE:
    invert = true;
    // fall-thru
  case OR_USE:
    op = NUOp::eBiBitOr;
    break;

  case XNOR_USE:
    invert = true;
    // fall-thru
  case XOR_USE:
    op = NUOp::eBiBitXor;
    break;

  case BUF_USE:
    op = NUOp::eUnBuf;
    break;

  case NOT_USE:
    op = NUOp::eUnBitNeg;
    break;

  case BUFIF0_USE:
  case BUFIF1_USE:
  case NOTIF0_USE:
  case NOTIF1_USE:
  case NMOS_USE:
  case RNMOS_USE:
  case PMOS_USE:
  case RPMOS_USE:
  case CMOS_USE:
  case RCMOS_USE:
    op = NUOp::eTeCond;
    break;

  case PULLDOWN_USE:
  case PULLUP_USE:
    // Should not call this routine for these.
    op = NUOp::eInvalid;
    name = "pull";
    break;

  case TRAN_USE:
    op = NUOp::eInvalid;
    name = "tran";
    break;
  case RTRAN_USE:
    op = NUOp::eInvalid;
    name = "rtran";
    break;
  case TRANIF0_USE:
    op = NUOp::eInvalid;
    name = "tranif0";
    break;
  case RTRANIF0_USE:
    op = NUOp::eInvalid;
    name = "rtranif0";
    break;
  case TRANIF1_USE:
    op = NUOp::eInvalid;
    name = "tranif1";
    break;
  case RTRANIF1_USE:
    op = NUOp::eInvalid;
    name = "rtranif1";
    break;
  case UNSET_USE:
    op = NUOp::eInvalid;
    name = "unset";
    break;
  case MODULE_USE:
    op = NUOp::eInvalid;
    name = "module";
    break;
  case PRIMITIVE_USE:
    op = NUOp::eInvalid;
    name = "primitive";
    break;
  case TASK_USE:
    op = NUOp::eInvalid;
    name = "task";
    break;
  case FUNCTION_USE:
    op = NUOp::eInvalid;
    name = "function";
    break;
  case REG_USE:
    op = NUOp::eInvalid;
    name = "reg";
    break;
  case INTEGER_USE:
    op = NUOp::eInvalid;
    name = "integer";
    break;
  case TIME_USE:
    op = NUOp::eInvalid;
    name = "time";
    break;
  case EVENT_USE:
    op = NUOp::eInvalid;
    name = "event";
    break;
  case REAL_USE:
    op = NUOp::eInvalid;
    name = "real";
    break;
  case REALTIME_USE:
    op = NUOp::eInvalid;
    name = "realtime";
    break;
  case INPUT_USE:
    op = NUOp::eInvalid;
    name = "input";
    break;
  case INOUT_USE:
    op = NUOp::eInvalid;
    name = "inout";
    break;
  case OUTPUT_USE:
    op = NUOp::eInvalid;
    name = "output";
    break;
  case WIRE_USE:
    op = NUOp::eInvalid;
    name = "wire";
    break;
  case UNARY_PLUS_USE:
    op = NUOp::eInvalid;
    name = "unary_USE";
    break;
  case UNARY_MINUS_USE:
    op = NUOp::eInvalid;
    name = "unary_USE";
    break;
  case REDUCT_AND_USE:
    op = NUOp::eInvalid;
    name = "reduct_USE";
    break;
  case REDUCT_OR_USE:
    op = NUOp::eInvalid;
    name = "reduct_USE";
    break;
  case PLUS_USE:
    op = NUOp::eInvalid;
    name = "plus";
    break;
  case MINUS_USE:
    op = NUOp::eInvalid;
    name = "minus";
    break;
  case STAR_USE:
    op = NUOp::eInvalid;
    name = "star";
    break;
  case DIV_USE:
    op = NUOp::eInvalid;
    name = "div";
    break;
  case PERCENT_USE:
    op = NUOp::eInvalid;
    name = "percent";
    break;
  case LOGICAL_EQUAL_USE:
    op = NUOp::eInvalid;
    name = "logical_USE";
    break;
  case LOGICAL_INEQUAL_USE:
    op = NUOp::eInvalid;
    name = "logical_USE";
    break;
  case CASE_EQUAL_USE:
    op = NUOp::eInvalid;
    name = "case_USE";
    break;
  case CASE_INEQUAL_USE:
    op = NUOp::eInvalid;
    name = "case_USE";
    break;
  case LOGICAL_AND_USE:
    op = NUOp::eInvalid;
    name = "logical_USE";
    break;
  case LOGICAL_OR_USE:
    op = NUOp::eInvalid;
    name = "logical_USE";
    break;
  case LT_USE:
    op = NUOp::eInvalid;
    name = "lt";
    break;
  case LT_EQUAL_USE:
    op = NUOp::eInvalid;
    name = "lt_USE";
    break;
  case GT_USE:
    op = NUOp::eInvalid;
    name = "gt";
    break;
  case GT_EQUAL_USE:
    op = NUOp::eInvalid;
    name = "gt_USE";
    break;
  case BITWISE_AND_USE:
    op = NUOp::eInvalid;
    name = "bitwise_USE";
    break;
  case BITWISE_OR_USE:
    op = NUOp::eInvalid;
    name = "bitwise_USE";
    break;
  case BITWISE_XOR_USE:
    op = NUOp::eInvalid;
    name = "bitwise_USE";
    break;
  case REDUCT_XNOR_OR_BIT_EQUIVAL_USE:
    op = NUOp::eInvalid;
    name = "reduct_OR_BIT_EQUIVAL_USE";
    break;
  case RIGHT_SHIFT_USE:
    op = NUOp::eInvalid;
    name = "right_USE";
    break;
  case LEFT_SHIFT_USE:
    op = NUOp::eInvalid;
    name = "left_USE";
    break;
  case CONDITIONAL_USE:
    op = NUOp::eInvalid;
    name = "conditional";
    break;
  case REDUCT_NAND_USE:
    op = NUOp::eInvalid;
    name = "reduct_USE";
    break;
  case REDUCT_NOR_USE:
    op = NUOp::eInvalid;
    name = "reduct_USE";
    break;
  case LOGICAL_NEGATION_USE:
    op = NUOp::eInvalid;
    name = "logical_USE";
    break;
  case BITWISE_NEGATION_USE:
    op = NUOp::eInvalid;
    name = "bitwise_USE";
    break;
  case REDUCT_XOR_USE:
    op = NUOp::eInvalid;
    name = "reduct_USE";
    break;
  case DUMMY_EOLN_USE:
    op = NUOp::eInvalid;
    name = "dummy_USE";
    break;
  case TRI_USE:
    op = NUOp::eInvalid;
    name = "tri";
    break;
  case TRI1_USE:
    op = NUOp::eInvalid;
    name = "tri1";
    break;
  case SUPPLY0_USE:
    op = NUOp::eInvalid;
    name = "supply0";
    break;
  case WAND_USE:
    op = NUOp::eInvalid;
    name = "wand";
    break;
  case TRIAND_USE:
    op = NUOp::eInvalid;
    name = "triand";
    break;
  case TRI0_USE:
    op = NUOp::eInvalid;
    name = "tri0";
    break;
  case SUPPLY1_USE:
    op = NUOp::eInvalid;
    name = "supply1";
    break;
  case WOR_USE:
    op = NUOp::eInvalid;
    name = "wor";
    break;
  case TRIOR_USE:
    op = NUOp::eInvalid;
    name = "trior";
    break;
  case TRIREG_USE:
    op = NUOp::eInvalid;
    name = "trireg";
    break;
  case POWER_USE:
    op = NUOp::eInvalid;
    name = "power";
    break;
  case ARITH_LEFT_SHIFT_USE:
    op = NUOp::eInvalid;
    name = "arith_SHIFT_USE";
    break;
  case ARITH_RIGHT_SHIFT_USE:
    op = NUOp::eInvalid;
    name = "arith_SHIFT_USE";
    break;
  case SIGN_USE:
    op = NUOp::eInvalid;
    name = "sign";
    break;
  case UWIRE_USE:
    op = NUOp::eInvalid;
    name = "uwire";
    break;
  }
  if (name != NULL)
  {
    op = NUOp::eUnBuf;
    SourceLocator loc = locator(ve_prim);
    mMsgContext->UnsupportedPrimitive(&loc, name);
  }
  
  *invertResult = invert;
  return op;
} // NUOp::OpT VerilogPopulate::getOpFromGatePrimitive


Populate::ErrorCode VerilogPopulate::pull(veNode ve_prim, LFContext *context)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_prim);

  if (not ((veGateInstanceGetType(ve_prim) == PULLDOWN_USE) or
	   (veGateInstanceGetType(ve_prim) == PULLUP_USE))) {
    mMsgContext->CheetahConsistency(&loc, "Bad pull type");
    return eFatal;
  }
  bool pull_up = (veGateInstanceGetType(ve_prim) == PULLUP_USE);

  CheetahList ve_terminal_iter(veGateInstanceGetTerminalList(ve_prim));
  if (not ve_terminal_iter) {
    mMsgContext->CheetahConsistency(&loc, "Invalid terminal list");
    return eFatal;
  }

  veNode ve_terminal = veListGetNextNode(ve_terminal_iter);
  if ((not ve_terminal) or (veListGetNextNode(ve_terminal_iter) != 0)) {
    mMsgContext->CheetahConsistency(&loc, "Invalid terminal list");
    return eFatal;
  }


  // Populate the terminal to get at the net which is being driven.
  NULvalue *lhs = 0;
  ErrorCode temp_err_code = lvalue(vePortConnGetConnectedExpr(ve_terminal),
				   context,
				   context->getModule(),
				   &lhs);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  if (not lhs->isWholeIdentifier()) {
    POPULATE_FAILURE(mMsgContext->PullNotWhole(&loc), &err_code);
    delete lhs;
    return err_code;
  }

  NUNet *net = lhs->getWholeIdentifier();
  delete lhs;

  if (pull_up) {
    if (context->checkForPullConflicts()) {
      if (net->isPullDown()) {
	POPULATE_FAILURE(mMsgContext->PullUpAndDown(net), &err_code);
      }
      if (net->isTrireg()) {
	POPULATE_FAILURE(mMsgContext->PullTrireg(net), &err_code);
      }
      if (net->isTri0()) {
	POPULATE_FAILURE(mMsgContext->PullUpTri0(net), &err_code);
      }
    }
    net->putIsPullUp(true);
  } else {
    if (context->checkForPullConflicts()) {
      if (net->isPullUp()) {
	POPULATE_FAILURE(mMsgContext->PullUpAndDown(net), &err_code);
      }
      if (net->isTrireg()) {
	POPULATE_FAILURE(mMsgContext->PullTrireg(net), &err_code);
      }
      if (net->isTri1()) {
	POPULATE_FAILURE(mMsgContext->PullDownTri1(net), &err_code);
      }
    }
    net->putIsPullDown(true);
  }

  return err_code;
}


/*!
 * Helper function to populate a continuous assign for a unary or
 * binary operator.
 */
Populate::ErrorCode VerilogPopulate::simpleGatePrimitive(veNode ve_prim,
                                                         NUOp::OpT op,
                                                         StringAtom *block_name,
                                                         UInt32 num_of_instances,
                                                         const SourceLocator& loc,
                                                         LFContext *context,
                                                         bool invert)
{
  ErrorCode err_code = eSuccess;

  CheetahList ve_terminal_iter(veGateInstanceGetTerminalList(ve_prim));
  if (not ve_terminal_iter) {
    mMsgContext->CheetahConsistency(&loc, "Invalid terminal list");
    return eFatal;
  }

  NUModule* theModule = context->getModule();

  NUExpr *rhs = 0;
  NULvalue *lhs = 0;
  NUContAssign* theAssign = 0;
  UtVector<veNode> allPortConns;
  veNode ve_terminal;
  while ((ve_terminal = veListGetNextNode(ve_terminal_iter))) {
    allPortConns.push_back(ve_terminal);
  }
  
  if ((op == NUOp::eUnBuf) || (op == NUOp::eUnBitNeg)) {
    // BUF and NOT are the only gate primitives that may have multiple
    // outputs.  They may have multiple outputs but have only 1
    // input.  We first get the input (rhs) and then we build a concat
    // of all the outputs(lhs) and create an assignment.  If there
    // were multiple outputs then we just duplicate the rhs to fill in
    // all bits. (don't get this duplication confused with the
    // duplication required for an array of instances.

    veNode ve_rhs = allPortConns.back();
    allPortConns.pop_back();

    ErrorCode rhs_err_code = expr(vePortConnGetConnectedExpr(ve_rhs), 
                                  context,
				  theModule,
				  0,
				  &rhs);

    if ( errorCodeSaysReturnNowWhenFailPopulate(rhs_err_code, &err_code ) ) {
      return rhs_err_code;
    }

    size_t numOuts = allPortConns.size();

    UInt32 op_size = rhs->getBitSize();
    if ( num_of_instances != op_size ){
      if (( op_size != 1 )) {
        // this is not an error according to most simulators, but is an error
        // in LRM  so warn the user that we are doing what most other
        // simulators do, that is use only the low bit.
        mMsgContext->IncorrectExprWidthOnPrimitiveGatePort(&loc,(allPortConns.size()+1),"input",op_size, num_of_instances);
        rhs = new NUVarselRvalue(rhs, ConstantRange(0, 0), loc);
        op_size = 1;
      }
      rhs = prepareGatePrimitiveArgument(rhs, num_of_instances, theModule, loc); // a copy of rhs for each instance
    }

    if (op != NUOp::eUnBuf) {
      rhs = new NUUnaryOp(op, rhs, loc);
    }
    rhs->resize(rhs->determineBitSize());

    NULvalueVector allLhs;
    allLhs.reserve(numOuts);
    for (size_t i = 0; i < numOuts; ++i) {
      ve_terminal = allPortConns[i];
      ErrorCode lhs_err_code = lvalue(vePortConnGetConnectedExpr(ve_terminal),
				      context,
				      theModule,
				      &lhs);
      if ( errorCodeSaysReturnNowWhenFailPopulate(lhs_err_code, &err_code ) ) {
        return lhs_err_code;
      }
      UInt32 lhs_size = lhs->getBitSize();
      if ( num_of_instances != lhs_size ){
        if (lhs_size != 1) {
	  mMsgContext->CheetahConsistency(&loc, "Bad expression size");
	  return eFatal;
	}
        lhs = prepareGatePrimitiveArgument(lhs, num_of_instances, theModule, loc);
      }

      allLhs.push_back(lhs);
    }
    
    if (allLhs.size() > 1) {
      // there were multiple outputs in lhs that are now within a
      // concat.  We may need to duplicate the RHS to handle all bits.
      NUConcatLvalue* concatLhs = new NUConcatLvalue(allLhs, loc);
      lhs = concatLhs;
      
      UInt32 lhsSize = lhs->getBitSize();
      UInt32 rhsSize = rhs->getBitSize();
      if (lhsSize != rhsSize)
      {
        // we need to repeat the rhs expression so that all the
        // outputs will get the same value.
        
        // With buf/nots, rhs must be a single bit for each instance
	if (rhsSize != num_of_instances) {
	  mMsgContext->CheetahConsistency(&loc, "Bad expression size");
	  return eFatal;
	}
        rhs = prepareGatePrimitiveArgument(rhs, lhsSize, theModule, loc); // a copy of rhs for each instance
      }
    }
    
    theAssign = new NUContAssign(block_name, lhs, rhs, loc);
    theModule->addContAssign(theAssign);
  }
  else {
    // First arg is the lhs
    UtVector<veNode>::iterator p = allPortConns.begin(); 
    ve_terminal = *p;

    ErrorCode lhs_err_code = lvalue(vePortConnGetConnectedExpr(ve_terminal),
				    context,
				    theModule,
				    &lhs);

    if ( errorCodeSaysReturnNowWhenFailPopulate(lhs_err_code, &err_code ) ) {
      return lhs_err_code;
    }

    UInt32 lhs_size = lhs->getBitSize();
    if ( num_of_instances != lhs_size ){
      if (lhs_size != 1) {
	  mMsgContext->CheetahConsistency(&loc, "Bad expression size");
	  return eFatal;
      }
      lhs = prepareGatePrimitiveArgument(lhs, num_of_instances, theModule, loc);
    }

    for (++p; p != allPortConns.end(); ++p) {
      ve_terminal = *p;
      if (rhs) {
	NUExpr *cur = 0;
	ErrorCode expr_err_code = expr(vePortConnGetConnectedExpr(ve_terminal),
				       context,
				       theModule,
				       0,
				       &cur);
        if ( errorCodeSaysReturnNowWhenFailPopulate(expr_err_code, &err_code ) ) {
          return expr_err_code;
        }
        UInt32 op_size = cur->getBitSize();
        if ( num_of_instances != op_size ){
          if (op_size != 1) {
            mMsgContext->CheetahConsistency(&loc, "Bad expression size");
            return eFatal;
          }
          cur = prepareGatePrimitiveArgument(cur, num_of_instances, theModule, loc); // a copy of rhs for each instance
        }

        rhs = new NUBinaryOp(op, rhs, cur, loc);
      } else {
	ErrorCode expr_err_code = expr(vePortConnGetConnectedExpr(ve_terminal),
				       context,
				       theModule,
				       0,
				       &rhs);
        if ( errorCodeSaysReturnNowWhenFailPopulate(expr_err_code, &err_code ) ) {
          return expr_err_code;
        }
        UInt32 op_size = rhs->getBitSize();
        if ( num_of_instances != op_size ){
          if (op_size != 1) {
            mMsgContext->CheetahConsistency(&loc, "Bad expression size");
            return eFatal;
          }
          rhs = prepareGatePrimitiveArgument(rhs, num_of_instances, theModule, loc); // a copy of rhs for each instance
        }
      }
    }

    if (not rhs) {
      return eFailPopulate;
    }

    // For unary operators, lay out the operator here, but don't create a
    // buffer operator.
    if (NUOp::isUnaryOp(op)) {
      rhs = new NUUnaryOp(op, rhs, loc);
    }
    UInt32 op_size = rhs->getBitSize();
    if ( num_of_instances != op_size ){
      if (op_size != 1) {
        mMsgContext->CheetahConsistency(&loc, "Bad expression size");
        return eFatal;
      }
      rhs = prepareGatePrimitiveArgument(rhs, num_of_instances, theModule, loc); // a copy of rhs for each instance
    }

    if (invert)
      rhs = Populate::invert (rhs);

    theAssign = new NUContAssign(block_name, lhs, rhs, loc);
    theModule->addContAssign(theAssign);
  }
  

  return err_code;
}


NUExpr* VerilogPopulate::prepareGatePrimitiveArgument(NUExpr* rhs,
                                                      UInt32 num_of_instances,
                                                      NUModule* module,
                                                      const SourceLocator& loc)
{
  UInt32 rhs_width = rhs->getBitSize();
  if ( num_of_instances > 1 ) {
    if (not (rhs->isWholeIdentifier() || rhs->isConstant())) {
      // create a temp varaible and assignement to the temp to use in place of the expression in the concat.
      StringAtom* atom = module->gensym("cse_inst_array");
      NUNet* temp_net = module->createTempNet(atom, rhs_width, false, loc);
      NULvalue* temp_lhs = new NUIdentLvalue(temp_net, loc);
      StringAtom* blockName = module->newBlockName(mStrCache, loc);
      NUContAssign* temp_assign = new NUContAssign(blockName, temp_lhs, rhs, loc);
      module->addContAssign(temp_assign);
      rhs = new NUIdentRvalue(temp_net, loc);
    }
    rhs = new NUConcatOp(rhs, num_of_instances, loc); // a copy of rhs for each instance
    rhs->resize(num_of_instances*rhs_width);
  }

  return rhs;
}

NULvalue* VerilogPopulate::prepareGatePrimitiveArgument(NULvalue* lhs,
                                                        UInt32 num_of_instances,
                                                        NUModule* /* module */,
                                                        const SourceLocator& loc)
{
  if ( num_of_instances > 1 ) {
    NULvalueVector lhsVector;
    CopyContext copy_context(NULL,NULL);
    for (UInt32 count = 0; count < num_of_instances; ++count){
      lhsVector.push_back((count==0) ? lhs : lhs->copy(copy_context));
    }
    lhs = new NUConcatLvalue(lhsVector, loc);
  }
  return lhs;
}


Populate::ErrorCode VerilogPopulate::deviceTernary(veBuiltinObjectType type,
							  UInt32 size,
							  NUExpr* driver,
							  NUExpr* enable,
							  const SourceLocator& loc,
							  LFContext *,
							  NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  UtString zval(size, 'z');
  NUConst *z_const = NUConst::createXZ(zval, false, size, loc);
  NUOp::OpT enNegOp = NUOp::eUnBitNeg;
  if ((enable->determineBitSize() == 1) ||
      (enable->determineBitSize() != driver->determineBitSize()))
    enNegOp = NUOp::eUnLogNot;
  
  switch (type) {
  case BUFIF0_USE:
  case PMOS_USE:
  case RPMOS_USE:
    *the_expr = new NUTernaryOp(NUOp::eTeCond,
				new NUUnaryOp(enNegOp, enable, loc),
				driver,
				z_const,
				loc);
    break;
  case BUFIF1_USE:
  case NMOS_USE:
  case RNMOS_USE:
    *the_expr = new NUTernaryOp(NUOp::eTeCond,
				enable,
				driver,
				z_const,
				loc);
    break;

  case NOTIF0_USE:
    *the_expr = new NUTernaryOp(NUOp::eTeCond,
				new NUUnaryOp(enNegOp, enable, loc),
				new NUUnaryOp(NUOp::eUnBitNeg, driver, loc),
				z_const,
				loc);
    break;

  case NOTIF1_USE:
    *the_expr = new NUTernaryOp(NUOp::eTeCond,
				enable,
				new NUUnaryOp(NUOp::eUnBitNeg, driver, loc),
				z_const,
				loc);
    break;

  default:
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown ternary primitive"), &err_code);
    break;
  }

  return err_code;
}

static bool sIsResistiveDevice(veBuiltinObjectType type)
{
  // This covers all cases of resistive devices, even if we do not
  // support them
  return ((type == RCMOS_USE) ||
          (type == RNMOS_USE) ||
          (type == RPMOS_USE) || 
          (type == RTRAN_USE) ||
          (type == RTRANIF0_USE) ||
          (type == RTRANIF1_USE)
          );
}


Populate::ErrorCode
VerilogPopulate::teCondGatePrimitive(veNode ve_prim,
                                     StringAtom *block_name,
                                     UInt32 num_of_instances,
                                     const SourceLocator& loc,
                                     LFContext *context)
{
  ErrorCode err_code = eSuccess;

  CheetahList ve_terminal_iter(veGateInstanceGetTerminalList(ve_prim));
  if (not ve_terminal_iter) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Invalid terminal list"), &err_code);
    return err_code;
  }

  NUModule* module = context->getModule();

  veBuiltinObjectType type = veGateInstanceGetType(ve_prim);
  
  // First arg is the lhs
  veNode ve_terminal = veListGetNextNode(ve_terminal_iter);
  NULvalue *lhs = 0;
  ErrorCode temp_err_code = lvalue(vePortConnGetConnectedExpr(ve_terminal),
				   context,
				   module,
				   &lhs);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  UInt32 size = lhs->getBitSize();

  if ( num_of_instances != size ){
    if (size != 1) {
      mMsgContext->CheetahConsistency(&loc, "Bad expression size");
      return eFatal;
    }
    lhs = prepareGatePrimitiveArgument(lhs, num_of_instances, module, loc);
    size = lhs->getBitSize();   // there are users of 'size' below
  }


  // Second arg is the driver
  ve_terminal = veListGetNextNode(ve_terminal_iter);
  NUExpr *driver = 0;
  temp_err_code = expr(vePortConnGetConnectedExpr(ve_terminal),
		       context,
		       module,
		       0,
		       &driver);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }
  UInt32 op_size = driver->getBitSize();
  if ( num_of_instances != op_size ){
    if (( op_size != 1 )) {
      // this is not an error according to most simulators, but is an
      // error according to the LRM.  Most simulators use only the low bit,.
      // so warn the user that we are doing what most other simulators do.
      mMsgContext->IncorrectExprWidthOnPrimitiveGatePort(&loc,2,"input",op_size, num_of_instances);
      driver = new NUVarselRvalue(driver, ConstantRange(0, 0), loc);
      op_size = 1;
    }
    driver = prepareGatePrimitiveArgument(driver, num_of_instances, module, loc); // a copy of rhs for each instance
  }


  // Third arg is the enable
  ve_terminal = veListGetNextNode(ve_terminal_iter);
  NUExpr *enable = 0;
  temp_err_code = expr(vePortConnGetConnectedExpr(ve_terminal),
		       context,
		       module,
		       0,
		       &enable);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }
  op_size = enable->getBitSize();
  if ( num_of_instances != op_size ){
    if (( op_size != 1 )) {
      // this is not an error according to most simulators, but is an
      // error according to the LRM.  Most simulators use only the low bit,.
      // so warn the user that we are doing what most other simulators do.
      mMsgContext->IncorrectExprWidthOnPrimitiveGatePort(&loc,3,"input",op_size, num_of_instances);
      enable = new NUVarselRvalue(enable, ConstantRange(0, 0), loc);
    }
    enable = prepareGatePrimitiveArgument(enable, num_of_instances, module, loc); // a copy of rhs for each instance
  }



  // Warn that the strength will not be reduced if this is a resistive
  // device
  if (sIsResistiveDevice(type))
    mMsgContext->NoStrengthReduceRDev(&loc);
    

  CopyContext copy_context(NULL,NULL);

  // If this is a cmos gate then there is a 4th arg, the pmos enable
  bool isCmos = ((type == CMOS_USE) || (type == RCMOS_USE));

  if (isCmos)
  {
    // make an nmos, pmos pair
    ve_terminal = veListGetNextNode(ve_terminal_iter);
    if (not ve_terminal) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Invalid terminal list"), &err_code);
      return err_code;
    }

    NUExpr* penable = 0;
    temp_err_code = expr(vePortConnGetConnectedExpr(ve_terminal),
			 context,
			 module,
			 0,
			 &penable);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    // this will get fixed by the assign
    driver->resize(driver->determineBitSize()); 
    NUExpr* rhs1 = 0;
    temp_err_code = deviceTernary(NMOS_USE, size, driver, enable, loc, context, &rhs1);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    NUExpr* rhs2 = 0;
    temp_err_code = deviceTernary(PMOS_USE, size, driver->copy(copy_context), penable, loc, context, &rhs2);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    NUContAssign* assign1 = new NUContAssign(block_name, lhs, rhs1, loc);
    
    // unfortunately, we have to create a name for the other side of
    // the pair
    block_name = module->newBlockName(context->getStringCache(), loc);
    NUContAssign* assign2 = new NUContAssign(block_name, lhs->copy(copy_context), rhs2, loc);
    module->addContAssign(assign1);
    module->addContAssign(assign2);
  }
  else {
    // create an assign for each instance
    CopyContext copy_context(NULL,NULL);
    UInt32 driver_size = driver->determineBitSize();
    UInt32 enable_size = enable->determineBitSize();
    for (UInt32 count = 0; count < num_of_instances; ++count){
      // Construct the rhs of the assign
      ConstantRange rrange (count,count);
      NUExpr* enable_bit = NULL;
      if ( enable_size == 1 ) {
        NU_ASSERT( (count == 0), enable);
        enable_bit = enable;
      } else {
        enable_bit = new NUVarselRvalue((count == 0)? enable : enable->copy(copy_context), rrange, loc);
        enable_bit->resize(1);
      }
      NUExpr* driver_bit = NULL;
      if ( driver_size == 1 ) {
        NU_ASSERT( (count == 0), driver);
        driver_bit = driver;
      } else {
        driver_bit = new NUVarselRvalue((count == 0)? driver : driver->copy(copy_context), rrange, loc);
        driver_bit->resize(1);
      }

      NUExpr* rhs = 0;
      temp_err_code = deviceTernary(type, 1, driver_bit, enable_bit, loc, context, &rhs);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      if ( count != 0 ) {
        // need a unique blockname for each copy, use provided name for the first assign
        block_name = module->newBlockName(context->getStringCache(), loc);
      }
      NULvalue* lhs_bit = new NUVarselLvalue((count == 0) ? lhs : lhs->copy(copy_context), rrange, loc);
      NUContAssign* theAssign = new NUContAssign(block_name, lhs_bit, rhs, loc);
      module->addContAssign(theAssign);
    }
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::gatePrimitive(veNode ve_prim,
							  LFContext *context)
{
  switch (veGateInstanceGetType(ve_prim)) {
  case PULLDOWN_USE:
  case PULLUP_USE:
    return pull(ve_prim, context);
    break;

  default:
    // Fallthrough
    break;
  }

  SourceLocator loc = locator(ve_prim);
  StringAtom *block_name = 0;

  // Check for strength annotation and warn if any exists
  if ((veInstanceGetStrength0(ve_prim) != UNSET_STRENGTH) ||
      (veInstanceGetStrength1(ve_prim) != UNSET_STRENGTH))
    mMsgContext->StrengthAnnotationIgnored(&loc);

// rjc temp fix
//rjc we should construct the name based on any existing generate
//index and the gate ve_prim name
//  UtString name_buf;
//  mTicProtectedNameMgr->getVisibleName(ve_prim, &name_buf);
//  if (name_buf.length() > 0) {
//    block_name = context->getStringCache()->intern(name_buf.c_str());
//  } else {
  block_name = context->getModule()->newBlockName(context->getStringCache(),
                                                    loc);
//  }

  // determine the range of instances (no range means 1 instance)
  UInt32 num_of_instances = 1;
  veNode ve_range = veInstanceGetRange(ve_prim);
  if (ve_range) {
    SInt32 inst_range_left = veExprEvaluateValue(veRangeGetLeftRange(ve_range));
    SInt32 inst_range_right = veExprEvaluateValue(veRangeGetRightRange(ve_range));
    if ( inst_range_right > inst_range_left ){
      num_of_instances = 1 + inst_range_right - inst_range_left;
    } else {
      num_of_instances = 1 + inst_range_left - inst_range_right;
    }
  }
  
  bool invert;                  // Track if op needs inversion
  NUOp::OpT op = getOpFromGatePrimitive(ve_prim, context, &invert);

  ErrorCode err_code = eSuccess;

  switch (op) {
  case NUOp::eInvalid:
    POPULATE_FAILURE(mMsgContext->UnsupportedPrimitive(&loc, gGetObjTypeStr(veNodeGetObjType(ve_prim))), &err_code);
    return err_code;
    break;
    
  case NUOp::eTeCond:
    LOC_ASSERT (!invert, loc);  // no inversion expected
    return teCondGatePrimitive(ve_prim, block_name, num_of_instances, loc, context);
    break;
  default:
    return simpleGatePrimitive(ve_prim, op, block_name, num_of_instances, loc, context, invert);
    break;
  }

  return eFatal;
}


void VerilogPopulate::createParamModuleMap(const char* fileRoot, MsgContext* msgContext)
{
  mParamModuleMap = new ParamModuleMap(fileRoot, msgContext);
}

void VerilogPopulate::destroyParamModuleMap()
{
  delete mParamModuleMap;
}

Populate::ErrorCode 
VerilogPopulate::analyzeInstanceParameters(veNode ve_module,
                                           UtString* modInstName)
{
  // No parameters on udps.
  if (veNodeGetObjType(ve_module) == VE_UDP)
    return eSuccess;

  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_module);
  veNode formalParm;
  
  CheetahList formalParmIter(veModuleGetParamList(ve_module));
  if (formalParmIter)
  {
    // We definitely have a parameterized module instance

    // We only write the initial comment to the libdesign.parameters
    // file if we have parameterized modules. 
    mParamModuleMap->maybeWriteInitialComments();

    // Get the unique modules already created from this parameterized
    // module
    ParamModuleMap::ParamListObj* paramList = mParamModuleMap->getParamList(*modInstName);
    // Used to get to each ParamListObj::Element in an initialized paramList. 
    UInt32 paramIndex = 0;

    // Used for output file indentation
    UtString indent("     ");
    // Used to express the parameter values in the output file
    UtString elemValues;

    while((formalParm = veListGetNextNode(formalParmIter)))
    {
      bool overrideToBinary = false; 
      UtString modNameAppend;

      veNode curExpr = veParamGetCurrentValue(formalParm);
      if (not curExpr) {
        POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "No expression for parameter"), &err_code);
        return err_code;
      }
      
      ParamModuleMap::Element* paramElement = NULL;

      // If this parameter list was encountered before, it has been
      // initialized and we don't have to query Cheetah for the
      // parameter name.
      if (! paramList->isInitialized())
      {
        UtString formal_name_buf;
        mTicProtectedNameMgr->getVisibleName(formalParm, &formal_name_buf);
        paramElement = paramList->addElement(formal_name_buf);
      }
      else
        paramElement = paramList->getElement(paramIndex);

      ++paramIndex;

      veNode lRange = veParamGetParamRange(formalParm);
      
      ParamValue actualParam;
      if (lRange == NULL) {
        // If no range is specified, parameter takes size of value applied,
        // so do not do any value optimization to make the parameter value canonical,
        // or else would lose the actual size.
        actualParam.putDoNoValOpt(true);
      }
      ErrorCode temp_err_code = getParamValueString(&actualParam, curExpr);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      
      UInt32 rangeSize = 0;
      ConstantRange formalRange;
      if (lRange)
      {
        veNode leftExpr = veRangeGetLeftRange(lRange);
        veNode rightExpr = veRangeGetRightRange(lRange);
        
    
        temp_err_code = getParameterRange(leftExpr, rightExpr, &formalRange);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
        rangeSize = formalRange.getLength();
       
        // Strings are handled independently.
        if (actualParam.getType() != VE_STRING)
        {
          // need to size the passed parameters
          UtString paramBinary;
          
          //size the binary strings
          {
            UtString& actualStr = actualParam.getStr();
            UtString::size_type paramSizeStr = actualStr.size();
            if (paramSizeStr < rangeSize) 
            {
              paramBinary.append(rangeSize - paramSizeStr, '0');
              paramBinary.append(actualStr);
            }
            else if (paramSizeStr > rangeSize)
            {
              paramBinary.append(actualStr, paramSizeStr - rangeSize, rangeSize);
              
              // reset the string to the new binary value since it has
              // been cut.
              actualStr.assign(paramBinary);

              // For safety, invalidate any values.
              actualParam.invalidateParamVal();
              actualParam.invalidateDblVal();

              // Whenever we cut an actual and there is a difference
              // in parameters we write out the binary value (as
              // opposed to decimal)
              overrideToBinary = true;
            }
            else
              paramBinary.assign(actualStr);
          }
        
          // Make sure the strings are consistently sized
          UtString::size_type paramBinarySize = paramBinary.size();
          if (paramBinarySize != rangeSize)
          {
            POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad parameter size"), &err_code);
            return err_code;
          }
        } // if not a string

        // must reset paramSize to context size
        actualParam.putSize(rangeSize);
      }

      /* Decide whether or not to add // <orig string> to output file
         for this parameter.
         
         We add a trailing comment if the string has to be 0 extended
         or truncated.
      */
      bool addStringComment = true;
      if (actualParam.getType() == VE_STRING)
      {
        const UtString& paramStr = actualParam.getStr();

        if (lRange == NULL)
        {
          // The parameter is not bound to a specific size
          modNameAppend << "\"" << paramStr << "\"";
          addStringComment = false;
        }
        else
        {
          // The value is bound to a bit range.
          UInt32 paramStrSize = paramStr.size();
          UInt32 strBinWidth =  paramStrSize * 8;
          UInt32 contextSize = actualParam.getSize();
          if (strBinWidth < contextSize)
          {
            // 0's will be prepended to the str value
            modNameAppend << "{" << contextSize - strBinWidth << "'b0, \"" << paramStr << "\"}";
          }
          else if (strBinWidth > contextSize)
          {
            UInt32 contextMod = contextSize % 8;
            UInt32 numCharsFit = contextSize/8;
            if (contextMod == 0)
            {
              // context size is aligned.
              const char* s = paramStr.c_str();
              s += paramStrSize - numCharsFit;
              // just truncate
              modNameAppend << "\"" << s << "\"";
            }
            else
            {
              // context size is not aligned
              CheetahStr strBinaryVal(veExprEvaluateValueInBinaryString(curExpr));
              LOC_ASSERT(strlen(strBinaryVal) == strBinWidth, loc);
              
              // need to use the binary value to fill out any value
              // holes.
              // We want to give as much information as possible to the
              // user in libdesign.parameters, so only use as much of
              // the binary value as needed.
              
              if (numCharsFit == 0)
              {
                const char* s = strBinaryVal;
                s += strBinWidth = contextSize;
                modNameAppend << contextSize << "'b" << s;
              }
              else
              {
                const char* s = strBinaryVal;
                // just get the needed binary bits
                s += strBinWidth - contextMod;
                modNameAppend << "{" << contextMod << "'b" << s << ", ";
                // Get the truncated string and append
                const char* vs = paramStr.c_str();
                vs += paramStrSize - numCharsFit;
                modNameAppend << "\"" << vs << "\"}";
              } // numChars > 0
            } // else context size is 8 bit aligned
          } // else if string > context size
          else
          {
            // The size fits perfectly
            modNameAppend << "\"" << paramStr << "\"";
            addStringComment = false;
          }
        } // if lRange
      } // if param is a string
      else
      {
        // NOT A STRING
        bool appendIt = true;
        if (! actualParam.hasValidDblVal())
          // If this is a real value, then don't print out a based
          // number in libdesign.parameters
          modNameAppend << actualParam.getSize() << "'";

        if (overrideToBinary ||
            ((actualParam.hasValidParamVal()) && ! actualParam.hasValidDblVal() &&
             (actualParam.getStr().size() >= 4)))
        {
          UtString& paramBuf = actualParam.getStr();
          // hexify the parameter value
          if (UtConv::BinaryStrToHex(&paramBuf))
            modNameAppend += 'h';
          else
            modNameAppend += 'b';
        }
        else
        {
          if (actualParam.hasValidParamVal())
          {
            modNameAppend += 'd';
            // getStr() in actualParam is the binary form.  
            // getParamVal() is the decimal form.
            modNameAppend << actualParam.getParamVal();
            appendIt = false;
          }
          else if (actualParam.hasValidDblVal())
          {
            modNameAppend << actualParam.getDblValue();
            appendIt = false;
          }
          else 
            modNameAppend += 'b';
        }
        
        if (appendIt)
          modNameAppend += actualParam.getStr();
      }

      // modNameAppend contains the interpreted value of the parameter
      // in string form. 
      paramElement->putStr(modNameAppend);
      
      elemValues << indent << paramElement->getName();
      if (lRange)
        elemValues << "[" << formalRange.getMsb() << ":" << formalRange.getLsb() << "]";
      elemValues << " = " << modNameAppend << ";";
      if (addStringComment && (actualParam.getType() == VE_STRING))
        elemValues << " // \"" << actualParam.getStr() << "\"";
      elemValues << "\n";
    } // while param && formal
    
    // This parameter list is definitely initialized at this
    // point. Future encounters of this parameterized module will be
    // more efficient.
    paramList->putInit(true);
    
    UtString modSuffix;
    if (paramList->constructSuffix(&modSuffix))
    {
      // We created a new module name. Add the mapping to
      // libdesign.parameters.
      UtOBStream& outFile = mParamModuleMap->getParameterMapFile();
      outFile << *modInstName << modSuffix << " -> " << *modInstName;
      UtString tmpLoc;
      loc.compose(&tmpLoc);
      outFile << "  (" << tmpLoc << ")" << UtIO::endl;
      outFile << elemValues << UtIO::endl;
    }

    modInstName->append(modSuffix);
    // modInstName now contains the nucleus module name.
  } // if formalIter
  
  return err_code;
} // Populate::ErrorCode VerilogPopulate::analyzeInstanceParameters


Populate::ErrorCode VerilogPopulate::scopeVariableList(CheetahList& ve_hier_iter,
                                                       LFContext *context)
{
  ErrorCode err_code = eSuccess;

  NUModule *module = context->getModule();

  // Iterate through the list of hierrefs (if any) and create the hierrefs.
  // Note that we always populate the hierref on the NUModule.
  if (ve_hier_iter) {
    veNode ve_hier;
    while ((ve_hier = VeListGetNextNode(ve_hier_iter)) != NULL) {
      NUNet *the_net = 0;
      ErrorCode hier_err_code = netHierRef(ve_hier, context, &the_net);

      if ( errorCodeSaysReturnNowOnlyWithFatal(hier_err_code, &err_code ) )
      {
        return hier_err_code;
      }

      if (the_net) {
	module->addNetHierRef(the_net);
      }
    }
  }

  return err_code;
}

//! handle any population issues from specify blocks
/*! at one time we would ignore all of the contents of a specify block
 * but the $setuphold timing check can imply a connection between the
 * reference event and the delayed reference, or the data_event and
 * the delayed data arguments.
 * 
 * currently this routine only handles this implicit connection
 * defined in a $setuphold timing check (LRM 15.5.1)
 *
 * note it is possible(and valid) that the same connection will be
 * implied by several $setuphold specifications, we create one for
 * each specification, but the excess assignments will be removed in a
 * later step.
 */
Populate::ErrorCode
VerilogPopulate::specifyBlock(veNode ve_spec_block, LFContext* context)
{
  ErrorCode err_code = eSuccess;
  CheetahList     ve_sys_time_check_iter;
  ve_sys_time_check_iter = veSpecBlockGetSysTimeCheckList(ve_spec_block);
  if (ve_sys_time_check_iter) {
    while (veNode ve_sys_time = VeListGetNextNode(ve_sys_time_check_iter)) {
      if ( VE_SYSTIMINGCHECK == veNodeGetObjType(ve_sys_time)) {
        SourceLocator loc = locator(ve_sys_time);
        veNode ve_sys_time_RefEvent;
        veNode ve_sys_time_delayedRef;
        ve_sys_time_RefEvent = veSysTimeChkGetRefEvent(ve_sys_time);
        ve_sys_time_delayedRef = veSysTimeChkGetDelayedReference(ve_sys_time);
        // only create the connection if both the clock event and the
        // delayed clcok are defined.
        if ( ve_sys_time_RefEvent && ve_sys_time_delayedRef ){
          ErrorCode temp_err_code = specifyBlockImplictConnection(ve_sys_time_RefEvent, ve_sys_time_delayedRef, context, loc);
          if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code )){
            return temp_err_code;
          }
        }
        
        veNode ve_sys_time_DataEvent;
        veNode ve_sys_time_delayedData;
        ve_sys_time_DataEvent = veSysTimeChkGetDataEvent(ve_sys_time);
        ve_sys_time_delayedData = veSysTimeChkGetDelayedData(ve_sys_time);
        // only create the connection if both the data event and the
        // delayed signal are defined.
        if ( ve_sys_time_DataEvent && ve_sys_time_delayedData ){
          ErrorCode temp_err_code = specifyBlockImplictConnection(ve_sys_time_DataEvent, ve_sys_time_delayedData, context, loc);
          if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code )){
            return temp_err_code;
          }
        }

      }
    }
  }
  return err_code;
}


//! support routine for creating implicit connections implied by $setuphold
Populate::ErrorCode
VerilogPopulate::specifyBlockImplictConnection(veNode ve_sys_time_event, veNode ve_sys_time_delay, LFContext* context, const SourceLocator& loc)
{
  ErrorCode err_code = eSuccess;
  
  veNode ve_sys_time_event_Terminal;
  ve_sys_time_event_Terminal = veTimingEventGetSpecTerminal(ve_sys_time_event);
  int rhsWidth = veExprEvaluateWidth(ve_sys_time_event_Terminal);
  int lhsWidth = veExprEvaluateWidth(ve_sys_time_delay);
  if ( lhsWidth != rhsWidth ){
    veNode ve_lhs_object = veNamedObjectUseGetParent(ve_sys_time_delay);
    UtString name_buf;
    mTicProtectedNameMgr->getVisibleName(ve_lhs_object, &name_buf);
    mMsgContext->ImplicitConnWidthMismatch( &loc, name_buf.c_str());
    return eSuccess;  // not really a success but the caller does nothing different in this case
  }

  NUModule* module = context->getModule();

  NULvalue *the_lvalue = 0;
  ErrorCode temp_err_code = lvalue(ve_sys_time_delay, context, module, &the_lvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    delete the_lvalue;
    return temp_err_code;
  }
  NUExpr* rvalue = 0;
  temp_err_code = expr(ve_sys_time_event_Terminal, context, module, rhsWidth, &rvalue);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    delete the_lvalue;
    delete rvalue;
    return temp_err_code;
  }

  NUContAssign *the_assign = helperPopulateContAssign(the_lvalue, rvalue, loc, context);
  NU_ASSERT(the_assign, rvalue);
  module->addContAssign(the_assign);

  return err_code;
}


Populate::ErrorCode
VerilogPopulate::gatherPortconnsForSinglePort(NUModule* the_module, veNode *ve_portconn, CheetahList *ve_portconn_iter,
                                              veNode new_module, NUPortMap* portMap, VeNodeList *portconns_for_this_port,
                                              NUNet** port)
{
  ErrorCode err_code = eSuccess;

  bool partial_port = false;
  bool collapse_these_ports = false; // if true then multiple ports need to be collapsed
  ConstantRange covered_range;
  NUNet* this_port = 0;

  *port = NULL;
  veNode ve_port = vePortConnGetParentPort( *ve_portconn );
  if ( ve_port == NULL ){
    // the port was not elaborated, perhaps due to substituteModule?
    // advance the iterator so that next call will work on the next portconnection
    *ve_portconn = veListGetNextNode(*ve_portconn_iter);
    return eSuccess;
  }

  ErrorCode temp_err_code = mPopulate->lookupPort( the_module, ve_port, new_module, portMap, &this_port, &partial_port, &covered_range );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    if ( this_port == NULL ){
      // there was no formal port declared for this connection, so *port
      // is null, and we advance the iterator so that next call will
      // work on the next portconnection
      *ve_portconn = veListGetNextNode(*ve_portconn_iter);
      return eSuccess;
    }
    return temp_err_code;
  }
  

  collapse_these_ports = partial_port;

  portconns_for_this_port->push_back(*ve_portconn);
  *port = this_port;

  // now we look at the next port connection(s) to see if that actual
  // should be connected to the collapsed formal, if so add the
  // portconn to portconns_for_this_port
  *ve_portconn = veListGetNextNode(*ve_portconn_iter);

  while ( *ve_portconn and partial_port and ( this_port == *port ) ) {
    ConstantRange port_range;
    // keep looking down the actuals until we find ourselves outside
    // of the current wacky port
    partial_port = false;
    ve_port = vePortConnGetParentPort( *ve_portconn );
    if ( ve_port == NULL ){
      // the port was not elaborated, perhaps due to substituteModule?
      // advance the iterator so that next call will work on the next portconnection
      break; 
    }
    temp_err_code = mPopulate->lookupPort( the_module, vePortConnGetParentPort( *ve_portconn ),
                                           new_module, portMap, &this_port, &partial_port, &port_range );
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, NULL ) ) {
      break; // break out of the loop, so we can process the ports we have seen so far
    }
    if ( partial_port and (this_port == *port) ) {
      // check to see that there is no overlap of bits
      if ( covered_range.overlaps(port_range) ){
        POPULATE_FAILURE(mMsgContext->MultiPortNet(this_port,"The same bit(s) of a net appeared more than once in the port list."), &err_code);
        return err_code;
      }
      if ( not covered_range.adjacent(port_range) ){
        POPULATE_FAILURE(mMsgContext->MultiPortNet(this_port,"Sequential bits of a net appeared in non adjacent positions in the port list."),
                         &err_code);
        return err_code;
      }
      covered_range = covered_range.combine(port_range);
      portconns_for_this_port->push_back(*ve_portconn);
      *ve_portconn = veListGetNextNode(*ve_portconn_iter);
    }
  }
  if ( collapse_these_ports and ( covered_range.getLength() != (*port)->getBitSize () ) ){
    POPULATE_FAILURE(mMsgContext->MultiPortNet(*port,"This net does not have all of its bits listed in the port list."), &err_code);
    return err_code;
  }

  return err_code;
}
      
Populate::ErrorCode
VerilogPopulate::unsupportedInstanceType(veNode ve_inst)
{
  ErrorCode err_code = eSuccess;
  UtString name_buf;
  mTicProtectedNameMgr->getVisibleName(ve_inst, &name_buf);
  SourceLocator loc = locator(ve_inst);
  POPULATE_FAILURE(mMsgContext->UnsupportedInstanceType(&loc, name_buf.c_str(), gGetObjTypeStr(veNodeGetObjType(ve_inst))), &err_code);
  return err_code;
}

Populate::ErrorCode
VerilogPopulate::topLevelMultiPortNet(NUNet* the_port)
{
  mMsgContext->TopLevelMultiPortNet(the_port);
  return eSuccess;
}
