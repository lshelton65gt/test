// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/UnelabFlow.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUControlFlowNet.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUUseDefNode.h"
#include "flow/FLNode.h"
#include "flow/FLFactory.h"
#include "util/CarbonAssert.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "util/UtDebugRecursion.h"
#include "util/StringAtom.h"

UtDEBUG_STACK(sUnelabStack)

UnelabFlow::UnelabFlow(FLNodeFactory *dfg_factory,
                       NUNetRefFactory *netref_factory,
                       MsgContext *msg_context,
                       IODBNucleus *iodb,
                       bool verbose) :
  DesignWalker(verbose),
  mFlowFactory(dfg_factory),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mIODB(iodb)
{
  mFlowStack.push(0);	// make sure we always have at least one (null) flow stack.
}
UnelabFlow::~UnelabFlow()
{
  INFO_ASSERT(mUseMapStack.empty(), "Non-empty use map stack.");
  INFO_ASSERT(mEdgeUseMapStack.empty(), "Non-empty edge use map stack.");
  INFO_ASSERT(mDefMapStack.empty(), "Non-empty def map stack.");
}


//! Add the given defs as drivers of the nets.
static void helperAddDrivers(NUNetRefFLNodeMultiMap *def_map,
                             NUScope *task_scope = 0)
{
  for (NUNetRefFLNodeMultiMap::MapLoop loop = def_map->loop();
       not loop.atEnd();
       ++loop) {
    NUNetRefHdl net_ref = (*loop).first;
    NUNet *net = net_ref->getNet();

    // Tasks are allowed to def nets outside their scope (in the module's
    // scope).  However, the task itself should not be seen as defining the
    // net, but instead the task enables will be seen as defining them in
    // the context of the enable.  So, don't connect drivers for non-task-local
    // nets.
    if ((task_scope == 0) or (net->isChildOf(task_scope))) {
      net->addContinuousDriver((*loop).second);
    }
  }
}

//! Mark a net z
static void helperMarkNetZ(NUNet* net, NUNet* /*master*/)
{
  if (net->isPrimaryBid() or net->isPrimaryOutput()) {
    net->putIsPrimaryZ(true);
  }
}

void UnelabFlow::undrivenNet(NUNet *net)
{
  // For protected mutable nets, we do not mark them as Z if they are undriven.
  // Bound nodes for protected mutable nets are created in a separate pass.
  if (net->isProtectedMutable(mIODB)) {
    return;
  }

  NUNet * master = net->getMaster();

  // Bids already have a bound node to represent the external driver, so don't
  // need a placeholder driver.
  if (not net->isBid()) {
    NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
    FLNodeBound *flow_node = mFlowFactory->createBound(net_ref, eFLBoundUndriven);
    net->addContinuousDriver(flow_node);
  }

  // Mark the net and its master as primary-Z, if appropriate
  helperMarkNetZ(net, master);
}

void UnelabFlow::fixPartiallyDriven(NUNet* net)
{
  // Check if this net is partially driven and if so, add a driver for
  // the undriven parts. Start by collecting all the drivers
  NUNetRefHdl accum_ref = mNetRefFactory->createEmptyNetRef();
  for (NUNet::DriverLoop l = net->loopContinuousDrivers(); !l.atEnd(); ++l)
  {
    FLNode* flow = *l;
    NUNetRefHdl part_ref = flow->getDefNetRef();
    accum_ref = mNetRefFactory->merge(accum_ref, part_ref);
  }

  // Check if it is partially driven
  NUNetRefHdl whole_ref = mNetRefFactory->createNetRef(net);
  NUNetRefHdl remain_ref = mNetRefFactory->subtract(whole_ref, accum_ref);
  if (!remain_ref->empty())
  {
    // Create a bound node for the undriven bits.  This should not be
    // done for bids
    if (!net->isBid())
    {  
      FLNodeBound* flow_node = mFlowFactory->createBound(remain_ref, eFLBoundUndriven);
      net->addContinuousDriver(flow_node);
    }

    // Removed this code because it was causing an asserting in the
    // Sanity pass. I will file a bug for it.
#if 0
    // Mark this as Z since it is partially Z. If this has performance
    // implications, we should re-visit this issue.
    helperMarkNetZ(net, master);
#endif
  }
}

void UnelabFlow::module(NUModule *this_module)
{
  module(this_module, true);
}


void UnelabFlow::module(NUModule *module, bool recurse)
{
  UtDEBUG(&sUnelabStack, "module", NULL, &module->getLoc(), "FLOW_SPEW")

  if (haveVisited(module)) {
    return;
  }

  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap edge_use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);
  mUseMapStack.push(&use_map);
  mEdgeUseMapStack.push(&edge_use_map);
  mDefMapStack.push(&def_map);

  for (NUTaskLoop loop = module->loopTasks(); not loop.atEnd(); ++loop) {
    tf(*loop);
  }

  NUContAssignList cont_assigns;
  module->getContAssigns(&cont_assigns);
  for (NUContAssignListIter iter = cont_assigns.begin();
       iter != cont_assigns.end();
       ++iter) {
    simpleContDriver(*iter);
  }

  NUEnabledDriverList enabled_drivers;
  module->getContEnabledDrivers(&enabled_drivers);
  for (NUEnabledDriverListIter iter = enabled_drivers.begin();
       iter != enabled_drivers.end();
       ++iter) {
    simpleContDriver(*iter);
  }

  NUTriRegInitList tri_reg_inits;
  module->getTriRegInit(&tri_reg_inits);
  for (NUTriRegInitListIter iter = tri_reg_inits.begin();
       iter != tri_reg_inits.end();
       ++iter) {
    simpleContDriver(*iter);
  }

  NUAlwaysBlockList always_blocks;
  module->getAlwaysBlocks(&always_blocks);
  for (NUAlwaysBlockListIter iter = always_blocks.begin();
       iter != always_blocks.end();
       ++iter) {
    NUAlwaysBlock* always = *iter;
    NU_ASSERT(def_map.empty(), always);
    alwaysBlock(always);
    helperAddDrivers(&def_map);
    def_map.clear();
  }

  NUInitialBlockList initial_blocks;
  module->getInitialBlocks(&initial_blocks);
  for (NUInitialBlockListIter iter = initial_blocks.begin();
       iter != initial_blocks.end();
       ++iter) {
    NUInitialBlock* initial = *iter;
    NU_ASSERT(def_map.empty(),initial);
    initialBlock(initial);
    helperAddDrivers(&def_map);
    def_map.clear();
  }

  for (NUModuleInstanceMultiLoop iter = module->loopInstances();
       not iter.atEnd();
       ++iter) {
    NUModuleInstance* instance = *iter;
    NU_ASSERT(def_map.empty(),instance);
    moduleInstance(instance, recurse);
    helperAddDrivers(&def_map);
    def_map.clear();
  }

  NUNetList all_nets;
  module->getAllNets(&all_nets);
  protectedMutableNets(all_nets);
  extNet(module, all_nets);  // this is only needed for $extnet, not $ostnet, $cfnet, etc.

  NUNetVectorLoop ports = module->loopPorts();
  portsAndArgs(ports);

  connectFanin(use_map, &FLNode::connectFaninSkipBidCycle);
  connectFanin(edge_use_map, &FLNode::connectEdgeFaninSkipBidCycle);

  rememberVisited(module);

  mUseMapStack.pop();
  mEdgeUseMapStack.pop();
  mDefMapStack.pop();

  if (mVerbose) {
    module->printFlow(0);
  }
}


void UnelabFlow::tf(NUTF *tf)
{
  UtDEBUG(&sUnelabStack, "tf", NULL, &tf->getLoc(), "FLOW_SPEW")

  if (haveVisited(tf)) {
    return;
  }

  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);
  mUseMapStack.push(&use_map);
  mDefMapStack.push(&def_map);

  {
    NUNetRefFLNodeMultiMap stmt_use_map(mNetRefFactory);
    NUNetRefFLNodeMultiMap stmt_def_map(mNetRefFactory);
    mUseMapStack.push(&stmt_use_map);
    mDefMapStack.push(&stmt_def_map);

    NUStmtLoop loop = tf->loopStmts();
    NUStmtList stmts(loop.begin(), loop.end());
    stmtList(&stmts);
    
    mUseMapStack.pop();
    mDefMapStack.pop();
    
    collect(tf,&stmt_use_map,&stmt_def_map);
  }

  mUseMapStack.pop();
  mDefMapStack.pop();

  // Tell helperAddDrivers that it is in the context of a task, do special handling
  // of non-locals.
  if (tf->getType() == eNUTask) {
    helperAddDrivers(&def_map, tf);
  } else {
    helperAddDrivers(&def_map);
  }

  NUNetVectorLoop ports = tf->loopArgs();
  portsAndArgs(ports);

  // Tell connectFanin that it is in the context of a task, do special handling
  // of non-locals.
  if (tf->getType() == eNUTask) {
    connectFanin(use_map, &FLNode::connectFaninSkipBidCycle, tf);
  } else {
    connectFanin(use_map, &FLNode::connectFaninSkipBidCycle);
  }

  rememberVisited(tf);

  if (mVerbose) {
    tf->printFlow(0);
  }
}


void UnelabFlow::initialBlock(NUInitialBlock *initial_block)
{
  UtDEBUG(&sUnelabStack, "initialBlock", NULL, &initial_block->getLoc(), "FLOW_SPEW")

  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);
  mUseMapStack.push(&use_map);
  mDefMapStack.push(&def_map);

  block(initial_block->getBlock());

  mUseMapStack.pop();
  mDefMapStack.pop();

  collect(initial_block, &use_map, &def_map);
}

void UnelabFlow::alwaysBlockSet(NUAlwaysBlockSet *blocks)
{
  UtDEBUG(&sUnelabStack, "alwaysBlockSet", NULL, NULL, "FLOW_SPEW");

  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap edge_use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);

  mUseMapStack.push(&use_map);
  mEdgeUseMapStack.push(&edge_use_map);
  mDefMapStack.push(&def_map);

  for ( NUAlwaysBlockSet::iterator iter = blocks->begin() ;
	iter != blocks->end() ;
	++iter ) {
    NUAlwaysBlock* always = *iter;
    NU_ASSERT(def_map.empty(),always);
    alwaysBlock(always);
    //helperAddDrivers(&def_map);
    def_map.clear();
  }

  connectFanin(use_map, &FLNode::connectFaninSkipBidCycle);
  connectFanin(edge_use_map, &FLNode::connectEdgeFaninSkipBidCycle);

  mUseMapStack.pop();
  mEdgeUseMapStack.pop();
  mDefMapStack.pop();
}

void UnelabFlow::alwaysBlock(NUAlwaysBlock *always_block)
{
  UtDEBUG(&sUnelabStack, "always", NULL, &always_block->getLoc(), "FLOW_SPEW");

  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);
  mUseMapStack.push(&use_map);
  mDefMapStack.push(&def_map);

  block(always_block->getBlock());

  mUseMapStack.pop();
  mDefMapStack.pop();

  NUNetRefSet edge_uses(mNetRefFactory);
  if (always_block->isSequential()) {
    always_block->getEdgeUses(&edge_uses);
  }

  collect(always_block, &use_map, &def_map, &edge_uses);
}


void UnelabFlow::block(NUBlock *block)
{
  UtDEBUG(&sUnelabStack, "block", NULL, &block->getLoc(), "FLOW_SPEW");

  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);
  mUseMapStack.push(&use_map);
  mDefMapStack.push(&def_map);

  NUStmtLoop loop = block->loopStmts();
  NUStmtList stmts(loop.begin(), loop.end());
  stmtList(&stmts);

  mUseMapStack.pop();
  mDefMapStack.pop();

  pruneTempNets( block, &use_map);

  collect(block, &use_map, &def_map);
}


void UnelabFlow::stmtList(NUStmtList *stmts)
{
  UtDEBUG(&sUnelabStack, "stmtList", NULL, NULL, "FLOW_SPEW");

  NUNetRefFLNodeMultiMap *list_use_map = mUseMapStack.top();
  NUNetRefFLNodeMultiMap *list_def_map = mDefMapStack.top();

  // Nets for which this stmt list has completely defined (thus killing any incoming def)
  NUNetRefSet killing_defs(mNetRefFactory);

  for (NUStmtListIter stmt_iter = stmts->begin();
       stmt_iter != stmts->end();
       stmt_iter++) {

    // Recurse to handle this stmt
    NUNetRefFLNodeMultiMap stmt_use_map(mNetRefFactory);
    NUNetRefFLNodeMultiMap stmt_def_map(mNetRefFactory);
    mUseMapStack.push(&stmt_use_map);
    mDefMapStack.push(&stmt_def_map);

    stmt(*stmt_iter);

    mUseMapStack.pop();
    mDefMapStack.pop();

    NUNetRefSet kills(mNetRefFactory);
    (*stmt_iter)->getBlockingKills(&kills);
    NUNetRefSet defs(mNetRefFactory);
    (*stmt_iter)->getBlockingDefs(&defs);
    oneStmtHelper(killing_defs,
                  kills,
                  defs,
                  stmt_use_map,
                  stmt_def_map,
                  list_use_map,
                  list_def_map);
    
  }
}


void UnelabFlow::oneStmtHelper(NUNetRefSet &killing_defs,
			       NUNetRefSet &stmt_kills,
			       NUNetRefSet &stmt_defs,
			       NUNetRefFLNodeMultiMap &stmt_use_map,
			       NUNetRefFLNodeMultiMap &stmt_def_map,
			       NUNetRefFLNodeMultiMap *list_use_map,
			       NUNetRefFLNodeMultiMap *list_def_map,
			       bool allow_multiple_drivers)
{
  // Try to connect flow nodes with unresolved uses to the defs so-far in this stmt
  // list.  Need to propagate the use up for any fanin which does not have a complete
  // (killing) def yet in this stmt list.
  for (NUNetRefFLNodeMultiMap::MapLoop stmt_use_loop = stmt_use_map.loop();
       not stmt_use_loop.atEnd();
       ++stmt_use_loop) {
    NUNetRefHdl use_net_ref = (*stmt_use_loop).first;
    FLNode *use_node = (*stmt_use_loop).second;
    for (NUNetRefFLNodeMultiMap::CondLoop list_def_loop = list_def_map->loop(use_net_ref, &NUNetRef::overlapsSameNet);
	 not list_def_loop.atEnd();
	 ++list_def_loop)
    {
      FLNode* node = (*list_def_loop).second;
      use_node->connectFanin(node);
    }
    NUNetRefHdl kill_net_ref = use_net_ref;
    NUNetRefSet::iterator kill_iter = killing_defs.find(use_net_ref, &NUNetRef::overlapsSameNet);
    if (kill_iter != killing_defs.end()) {
      NUNetRefHdl this_kill_set_net_ref = *kill_iter;
      kill_net_ref = mNetRefFactory->subtract(use_net_ref, this_kill_set_net_ref);
    }
    if (not kill_net_ref->empty()) {
      list_use_map->insert(kill_net_ref, use_node);
    }
  }

  // For all kills, get rid of the def in the external def map; we will be putting a
  // replacement in next.
  for (NUNetRefSetIter kill_iter = stmt_kills.begin(); kill_iter != stmt_kills.end(); ++kill_iter) {
    NUNetRefHdl kill_net_ref = *kill_iter;
    list_def_map->erase(kill_net_ref);
    killing_defs.insert(kill_net_ref);
  }

  // Place all defs into the ongoing def map.
  for (NUNetRefSetIter def_iter = stmt_defs.begin(); def_iter != stmt_defs.end(); ++def_iter) {
    NUNetRefHdl def_net_ref = *def_iter;
    NUNetRefFLNodeMultiMap::CondLoop stmt_def_loop = stmt_def_map.loop(def_net_ref, &NUNetRef::overlapsSameNet);

    // Make sure we have a def.
    NU_ASSERT(not stmt_def_loop.atEnd(),def_net_ref);

    FLNode *flnode = (*stmt_def_loop).second;
    list_def_map->insert(def_net_ref, flnode);

    ++stmt_def_loop;
    if (allow_multiple_drivers) {
      // In special circumstances (rolling up a for loop), we allow multiple defs
      // because this is called for the results of a stmtlist.
      for ( ; not stmt_def_loop.atEnd() ; ++stmt_def_loop ) {
        flnode = (*stmt_def_loop).second;	
        list_def_map->insert(def_net_ref, flnode);
      }
    } else {
      // Make sure we have only 1 def; should only create 1 flow node per def/stmt
      NU_ASSERT(stmt_def_loop.atEnd(),def_net_ref);
    }
  }
}

void UnelabFlow::stmt(NUStmt *stmt)
{
  UtDEBUG(&sUnelabStack, "stmt", NULL, &stmt->getLoc(), "FLOW_SPEW");

  // Check to make sure that non-blocking stmts no longer exist.
  NUNetRefSet non_blocking_defs(mNetRefFactory);
  stmt->getNonBlockingDefs(&non_blocking_defs);
  NU_ASSERT(non_blocking_defs.empty(),stmt);

  NUIf *if_stmt = dynamic_cast<NUIf*>(stmt);
  if (if_stmt) {
    ifStmt(if_stmt);
    return;
  }

  NUCase *case_stmt = dynamic_cast<NUCase*>(stmt);
  if (case_stmt) {
    caseStmt(case_stmt);
    return;
  }

  NUFor * for_stmt = dynamic_cast<NUFor*>(stmt);
  if (for_stmt) {
    forStmt(for_stmt);
    return;
  }

  NUBlock *block_stmt = dynamic_cast<NUBlock*>(stmt);
  if (block_stmt) {
    block(block_stmt);
    return;
  }

  NUNetRefFLNodeMultiMap *use_map = mUseMapStack.top();
  NUNetRefFLNodeMultiMap *def_map = mDefMapStack.top();

  NUNetNodeToFlowMap * flow_map = mFlowStack.top();

  // Iterate through the defs of this stmt, creating a flow node for the def.
  NUNetRefSet defs(mNetRefFactory);
  stmt->getBlockingDefs(&defs);
  for (NUNetRefSetIter def_iter = defs.begin();
       def_iter != defs.end();
       ++def_iter) {
    NUNetRefHdl def_net_ref = *def_iter;

    FLNode *flow_node = lookupFlow(def_net_ref,stmt,flow_map);
    def_map->insert(def_net_ref, flow_node);

    // Place everything used by this def into the external use map.
    NUNetRefSet uses(mNetRefFactory);
    stmt->getBlockingUses(def_net_ref, &uses);
    for (NUNetRefSetIter use_iter = uses.begin();
	 use_iter != uses.end();
	 ++use_iter) {
      NUNetRefHdl use_net_ref = *use_iter;
      use_map->insert(use_net_ref, flow_node);
    }
  }
}


FLNode * UnelabFlow::lookupFlow(const NUNetRefHdl &net_ref,
                                NUUseDefNode * stmt,
                                NUNetNodeToFlowMap * flow_map)
{
  FLNode * flow = NULL;
  if (flow_map) {
    NUNetNodeToFlowMap::iterator location = flow_map->find(NUNetNode(net_ref,stmt));
    if (location != flow_map->end()) {
      flow = location->second;
    }
  }
  if (not flow) {
    flow = mFlowFactory->create(net_ref, stmt);
    if (flow_map) {
      (*flow_map)[NUNetNode(net_ref,stmt)] = flow;
    }
  }
  return flow;
}


void UnelabFlow::ifStmt(NUIf *if_stmt)
{
  UtDEBUG(&sUnelabStack, "ifStmt", NULL, &if_stmt->getLoc(), "FLOW_SPEW");

  NUNetRefFLNodeMultiMap then_def_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap else_def_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap whole_use_map(mNetRefFactory);

  NUStmtList *stmts;

  mUseMapStack.push(&whole_use_map);

  stmts = if_stmt->getThen();
  mDefMapStack.push(&then_def_map);
  stmtList(stmts);
  mDefMapStack.pop();
  delete stmts;

  stmts = if_stmt->getElse();
  mDefMapStack.push(&else_def_map);
  stmtList(stmts);
  mDefMapStack.pop();
  delete stmts;

  mUseMapStack.pop();

  // Merge the def maps
  for (NUNetRefFLNodeMultiMap::MapLoop loop = else_def_map.loop();
       not loop.atEnd();
       ++loop) {
    NUNetRefHdl def_net_ref = (*loop).first;
    FLNode *flnode = (*loop).second;
    then_def_map.insert(def_net_ref, flnode);
  }

  collect(if_stmt, &whole_use_map, &then_def_map);
}


void UnelabFlow::caseStmt(NUCase *case_stmt)
{
  UtDEBUG(&sUnelabStack, "caseStmt", NULL, &case_stmt->getLoc(), "FLOW_SPEW")

  NUNetRefFLNodeMultiMap whole_def_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap item_def_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap whole_use_map(mNetRefFactory);

  mUseMapStack.push(&whole_use_map);

  // Iterate over all case items.
  for (NUCase::ItemLoop items_iter = case_stmt->loopItems();
       !items_iter.atEnd();
       ++items_iter)
  {
    const NUCaseItem* caseItem = *items_iter;

    // Create flow for the stmts of this item.
    NU_ASSERT(item_def_map.empty(),caseItem);
    NUStmtList* stmts = caseItem->getStmts();
    mDefMapStack.push(&item_def_map);
    stmtList(stmts);
    mDefMapStack.pop();
    delete stmts;

    // Merge this item's def map into the case def map.
    for (NUNetRefFLNodeMultiMap::MapLoop item_def_loop = item_def_map.loop();
	 not item_def_loop.atEnd();
	 ++item_def_loop) {
      NUNetRefHdl item_def_net_ref = (*item_def_loop).first;
      FLNode *item_def_node = (*item_def_loop).second;
      whole_def_map.insert(item_def_net_ref, item_def_node);
    }

    item_def_map.clear();
  }

  mUseMapStack.pop();

  collect(case_stmt, &whole_use_map, &whole_def_map);
}


void UnelabFlow::forStmt(NUFor *for_stmt)
{
  UtDEBUG(&sUnelabStack, "forStmt", NULL, &for_stmt->getLoc(), "FLOW_SPEW")

  NUNetNodeToFlowMap flow_map;
  bool using_flow_map = false;

  if (not mFlowStack.top()) {
    // Only the top-level for() is responsible for creating a flow
    // cache. All nested for() loops use the same cache. Otherwise, we
    // recreate flow upon subsequent recursion.
    mFlowStack.push(&flow_map);
    using_flow_map = true;
  }

  NUNetRefFLNodeMultiMap outer_def_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap outer_use_map(mNetRefFactory);

  mUseMapStack.push(&outer_use_map);
  mDefMapStack.push(&outer_def_map);

  // Nets for which this stmt list has completely defined (thus killing any incoming def)
  NUNetRefSet killing_defs(mNetRefFactory);

  // initial can be empty if it was never provided or if it was
  // eliminated because there was no use (even by the condition):
  // for (i = 0; keepGoing==1'b1; i = i + 1)
  NUStmtList *initial = for_stmt->getInitial(false);

  if (not initial->empty())
  {
    // 1. Process the initial stmts as first element in a stmtlist.
    NUNetRefFLNodeMultiMap stmt_use_map(mNetRefFactory);
    NUNetRefFLNodeMultiMap stmt_def_map(mNetRefFactory);
    mUseMapStack.push(&stmt_use_map);
    mDefMapStack.push(&stmt_def_map);

    stmtList(initial);

    mUseMapStack.pop();
    mDefMapStack.pop();

    NUNetRefSet kills(mNetRefFactory);
    NUNetRefSet defs(mNetRefFactory);
    for (NUStmtList::iterator iter = initial->begin(); iter != initial->end(); ++iter) {
      (*iter)->getBlockingKills(&kills);
      (*iter)->getBlockingDefs(&defs);
    }
    oneStmtHelper(killing_defs,
		  kills,
		  defs,
		  stmt_use_map,
		  stmt_def_map,
		  &outer_use_map,
		  &outer_def_map);
  }

  {
    // 2. Handle body and advance as a pseudo block, similar to a
    //    single-branch if stmt.
    NUNetRefFLNodeMultiMap body_use_map(mNetRefFactory);
    NUNetRefFLNodeMultiMap body_def_map(mNetRefFactory);
    mUseMapStack.push(&body_use_map);
    mDefMapStack.push(&body_def_map);

    NUStmtList *stmts   = for_stmt->getBody();
    NUStmtList *advance = for_stmt->getAdvance(false);

    {
      // Process stmtlist until it settles.
      int loop_cnt = 0;
      size_t old_use_map_size = 0;
      size_t new_use_map_size = 0;
      do {
	++loop_cnt;

	old_use_map_size = new_use_map_size;

	//body_def_map.clear();
	body_use_map.clear();

	NUNetRefSet body_killing_defs(mNetRefFactory);

	{
	  // 4. Process body & advance.
	  NUNetRefFLNodeMultiMap stmt_use_map(mNetRefFactory);
	  NUNetRefFLNodeMultiMap stmt_def_map(mNetRefFactory);
	  mUseMapStack.push(&stmt_use_map);
	  mDefMapStack.push(&stmt_def_map);

          // We process the body and advance at the same time so we
          // will appropriately add uses to all nets used in the
          // condition. Neglecting to add arcs between the
          // body/advance statements and the uses in the condition can
          // cause logic to become incorrectly dead.

	  stmtList(stmts);
          stmtList(advance);

	  mUseMapStack.pop();
	  mDefMapStack.pop();

	  // add the uses from the condition.
	  NUNetRefSet cond_uses(mNetRefFactory);
	  for_stmt->getCondition()->getUses(&cond_uses);
	  for (NUNetRefFLNodeMultiMap::MapLoop loop = stmt_def_map.loop();
	       not loop.atEnd();
	       ++loop) {
	    FLNode *flnode = (*loop).second;
	    for (NUNetRefSetIter use_iter = cond_uses.begin();
		 use_iter != cond_uses.end();
		 ++use_iter) {
	      NUNetRefHdl use_net_ref = *use_iter;
	      stmt_use_map.insert(use_net_ref,flnode);
	    }
	  }

	  NUNetRefSet kills(mNetRefFactory);
	  NUNetRefSet defs(mNetRefFactory);
	  for (NUStmtList::iterator iter = stmts->begin();
	       iter != stmts->end();
	       ++iter) {
	    (*iter)->getBlockingKills(&kills);
	    (*iter)->getBlockingDefs(&defs);
	  }
          for (NUStmtList::iterator iter = advance->begin();
               iter != advance->end();
               ++iter) {
            (*iter)->getBlockingKills(&kills);
            (*iter)->getBlockingDefs(&defs);
          }
	  oneStmtHelper(body_killing_defs,
			kills,
			defs,
			stmt_use_map,
			stmt_def_map,
			&body_use_map,
			&body_def_map,
			true);
	}

	new_use_map_size = body_use_map.size();
      } while (old_use_map_size != new_use_map_size);
    }

    mDefMapStack.pop(); // stmt_def_map
    mUseMapStack.pop(); // stmt_use_map

    NUNetRefSet kills(mNetRefFactory);
    NUNetRefSet defs(mNetRefFactory);
    for (NUStmtList::iterator iter = advance->begin(); iter != advance->end(); ++iter) {
      // (*iter)->getBlockingKills(&kills); // advance does not kill externally
      (*iter)->getBlockingDefs(&defs);
    }
    for (NUStmtList::iterator iter = stmts->begin(); iter != stmts->end(); ++iter) {
      // (*iter)->getBlockingKills(&kills); // the body of a for loop does not kill externally.
      (*iter)->getBlockingDefs(&defs);
    }
    oneStmtHelper(killing_defs,
		  kills,
		  defs,
		  body_use_map,
		  body_def_map,
		  &outer_use_map,
		  &outer_def_map,
		  true);

    delete initial;
    delete advance;
    delete stmts;
  }

  mDefMapStack.pop(); // outer_def_map
  mUseMapStack.pop(); // outer_use_map

  collect(for_stmt, &outer_use_map, &outer_def_map);
  if (using_flow_map) {
    mFlowStack.pop();
  }
}


void UnelabFlow::simpleContDriver(NUUseDefNode *node)
{
  UtDEBUG(&sUnelabStack, "simpleContDriver", NULL, &node->getLoc(), "FLOW_SPEW")

  NU_ASSERT(((dynamic_cast<NUContAssign*>(node) != 0) or
             (dynamic_cast<NUTriRegInit*>(node) != 0) or
             (dynamic_cast<NUEnabledDriver*>(node) != 0)), node);

  NUNetRefFLNodeMultiMap *external_use_map = mUseMapStack.top();

  // Iterate through the defs of this node, creating a flow node for the def.
  NUNetRefSet defs(mNetRefFactory);
  node->getDefs(&defs);
  for (NUNetRefSetIter def_iter = defs.begin();
       def_iter != defs.end();
       ++def_iter) {
    NUNetRefHdl def_net_ref = *def_iter;
    NUNet *def_net = def_net_ref->getNet();

    // Create a flow node to represent this def by this node.
    FLNode *flow_node = mFlowFactory->create(def_net_ref, node);
    def_net->addContinuousDriver(flow_node);

    // Place everything used by this def into the external use map.
    NUNetRefSet uses(mNetRefFactory);
    node->getUses(def_net_ref,&uses);
    for (NUNetRefSetIter use_iter = uses.begin();
	 use_iter != uses.end();
	 ++use_iter) {
      NUNetRefHdl use_net_ref = *use_iter;
      external_use_map->insert(use_net_ref, flow_node);
    }
  }
}


void UnelabFlow::moduleInstance(NUModuleInstance *inst, bool recurse)
{
  UtDEBUG(&sUnelabStack, "moduleInstance", NULL, &inst->getLoc(), "FLOW_SPEW")

  NUModule *instantiated_module = inst->getModule();

  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);
  mUseMapStack.push(&use_map);
  mDefMapStack.push(&def_map);

  if (recurse) {
    // Recurse to handle nested modules.
    module(instantiated_module);
  }

  // Handle the ports.  Note that inputs must be done before output.
  NUPortConnectionInputList iport_list;
  inst->getInputPortConnections(iport_list);
  for (NUPortConnectionInputListIter iter = iport_list.begin();
       iter != iport_list.end();
       iter++) {
    portConnectionInput(*iter);
  }

  NUPortConnectionBidList bport_list;
  inst->getBidPortConnections(bport_list);
  for (NUPortConnectionBidListIter iter = bport_list.begin();
       iter != bport_list.end();
       iter++) {
    portConnectionBidInput(*iter);
  }
  for (NUPortConnectionBidListIter iter = bport_list.begin();
       iter != bport_list.end();
       iter++) {
    portConnectionBidOutput(*iter);
  }

  NUPortConnectionOutputList oport_list;
  inst->getOutputPortConnections(oport_list);
  for (NUPortConnectionOutputListIter iter = oport_list.begin();
       iter != oport_list.end();
       iter++) {
    portConnectionOutput(*iter);
  }

  mUseMapStack.pop();
  mDefMapStack.pop();

  collect(inst, &use_map, &def_map);

  // here we alias the virtual nets so that the parent and child for
  // each are combined
  // This comes after collect because we create a module instance
  // flow node for the ExtNet, and no nested nodes.
  moduleInstanceConnectVirtNetFanin(inst, instantiated_module->getExtNet(),
                                    inst->getParentModule()->getExtNet(), def_map);

  moduleInstanceConnectVirtNetFanin(inst, instantiated_module->getOutputSysTaskNet(),
                                    inst->getParentModule()->getOutputSysTaskNet(), def_map);

  moduleInstanceConnectVirtNetFanin(inst, instantiated_module->getControlFlowNet(),
                                    inst->getParentModule()->getControlFlowNet(), def_map);
}


void UnelabFlow::portInput(NUPortConnection *portconn, NUNetRefSet *formal_defs)
{
  NU_ASSERT(((dynamic_cast<NUPortConnectionInput*>(portconn) != 0) or
             (dynamic_cast<NUPortConnectionBid*>(portconn) != 0)), portconn);

  NUNetRefFLNodeMultiMap *use_map = mUseMapStack.top();
  NUNetRefFLNodeMultiMap *def_map = mDefMapStack.top();

  // For everything that this port connection is a def-of (these defs are formals)
  for (NUNetRefSetIter def_iter = formal_defs->begin();
       def_iter != formal_defs->end();
       ++def_iter) {
    NUNetRefHdl def_net_ref = *def_iter;
    FLNode *flow_node = mFlowFactory->create(def_net_ref, portconn);
    def_map->insert(def_net_ref, flow_node);

    // For everything that this port connection uses for that def (these uses
    // are actuals)
    NUNetRefSet this_uses(mNetRefFactory);
    portconn->getUses(def_net_ref, &this_uses);
    for (NUNetRefSetIter use_iter = this_uses.begin();
	 use_iter != this_uses.end();
	 ++use_iter) {
      NUNetRefHdl use_net_ref = *use_iter;
      use_map->insert(use_net_ref, flow_node);
    }
  }
}


void UnelabFlow::portOutput(NUPortConnection *portconn, NUNetRefSet *actual_defs)
{
  NU_ASSERT(((dynamic_cast<NUPortConnectionOutput*>(portconn) != 0) or
             (dynamic_cast<NUPortConnectionBid*>(portconn) != 0)), portconn);

  NUNetRefFLNodeMultiMap *def_map = mDefMapStack.top();

  NUModuleInstance *inst = portconn->getModuleInstance();

  // For everything that this port connection is a def-of (these defs are actuals)
  for (NUNetRefSetIter def_iter = actual_defs->begin();
       def_iter != actual_defs->end();
       ++def_iter) {
    NUNetRefHdl def_net_ref = *def_iter;
    FLNode *flow_node = mFlowFactory->create(def_net_ref, portconn);
    def_map->insert(def_net_ref, flow_node);

    // For everything that this port connection uses (these uses are formals)
    NUNetRefSet this_uses(mNetRefFactory);
    portconn->getUses(def_net_ref, &this_uses);
    for (NUNetRefSetIter use_iter = this_uses.begin();
	 use_iter != this_uses.end();
	 ++use_iter) {
      NUNetRefHdl use_net_ref = *use_iter;
      NUNetRefSet formal_uses(mNetRefFactory);
      inst->getModule()->getUses(use_net_ref, &formal_uses);
      addActualUsesFromFormals(flow_node, formal_uses, *def_map);
    }
  }
}


void UnelabFlow::addActualUsesFromFormals(FLNode *flow_node,
					  NUNetRefSet &formal_uses,
					  NUNetRefFLNodeMultiMap &def_map)
{
  // For everything that this output/bid port in the instantiated module
  // uses (still in terms of formals here)
  for (NUNetRefSetIter formal_use_iter = formal_uses.begin();
       formal_use_iter != formal_uses.end();
       ++formal_use_iter) {
    NUNetRefHdl formal_use_net_ref = *formal_use_iter;

    // This will take us back to the actuals which are input to the instantiation.
    for (NUNetRefFLNodeMultiMap::CondLoop formal_def_loop = def_map.loop(formal_use_net_ref, &NUNetRef::overlapsSameNet);
	 not formal_def_loop.atEnd();
	 ++formal_def_loop) {
      flow_node->connectFanin((*formal_def_loop).second);
    }
  }
}



void UnelabFlow::moduleInstanceConnectVirtNetFanin(NUModuleInstance *child_inst,
                                                   NUNet* child_virt_net,
                                                   NUNet* parent_virt_net,
                                                   NUNetRefFLNodeMultiMap &def_map)
{
  NUModule *child_inst_module = child_inst->getModule();
  NUNetRefHdl child_virt_net_ref = mNetRefFactory->createNetRef(child_virt_net);

  // Do not create a virtual net driver for this instance if the instance does
  // not def the virtual net.
  if (not child_inst_module->queryDefs(child_virt_net_ref, &NUNetRef::overlapsSameNet, mNetRefFactory)) {
    return;
  }

  // Create the driver for the virtual net.
  NUNetRefHdl parent_virt_net_ref = mNetRefFactory->createNetRef(parent_virt_net);
  FLNode *flow_node = mFlowFactory->create(parent_virt_net_ref, child_inst);
  parent_virt_net->addContinuousDriver(flow_node);

  // Connect the fanin from the actuals.
  NUNetRefSet use_set(mNetRefFactory);
  child_inst_module->getUses(child_virt_net_ref, &use_set);
  addActualUsesFromFormals(flow_node, use_set, def_map);
}


void UnelabFlow::portConnectionInput(NUPortConnectionInput *portconn)
{
  NUNetRefSet defs(mNetRefFactory);
  portconn->getDefs(&defs);
  portInput(portconn, &defs);
}


void UnelabFlow::portConnectionBidInput(NUPortConnectionBid *portconn)
{
  NUNetRefSet formal_defs(mNetRefFactory);
  if (portconn->getFormal() != 0) {
    NUNetRefHdl formal_net_ref = mNetRefFactory->createNetRef(portconn->getFormal());
    formal_defs.insert(formal_net_ref);
  }
  portInput(portconn, &formal_defs);
}


void UnelabFlow::portConnectionBidOutput(NUPortConnectionBid *portconn)
{
  NUNetRefSet actual_defs(mNetRefFactory);
  if (portconn->getActual() != 0) {
    portconn->getActual()->getDefs(&actual_defs);
  }
  portOutput(portconn, &actual_defs);
}


void UnelabFlow::portConnectionOutput(NUPortConnectionOutput *portconn)
{
  NUNetRefSet defs(mNetRefFactory);
  portconn->getDefs(&defs);
  portOutput(portconn, &defs);
}


void UnelabFlow::portsAndArgs(NUNetVectorLoop &loop)
{
  for (; not loop.atEnd(); ++loop) {
    NUNet *net = *loop;
    if (net->isInput() or net->isBid()) {
      if (net->isBid()) {
        if (not net->isContinuouslyDriven()) {
          undrivenNet(net);
        }
      }
      NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
      FLNodeBound *flow_node = mFlowFactory->createBound(net_ref, eFLBoundPort);
      net->addContinuousDriver(flow_node);
    } else if (net->isOutput()) {
      if (not net->isContinuouslyDriven()) {
        undrivenNet(net);
      }
    }
  }
}


void UnelabFlow::protectedMutableNets(NUNetList &nets)
{
  for (NUNetList::iterator iter = nets.begin(); iter != nets.end(); ++iter) {
    NUNet *net = *iter;

    if (net->getStorage() != net) { continue; }

    // Create a bound to represent external contributions.
    NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);

    // The net has been marked mutable because of deposit or force directives.
    if (net->isProtectedMutableNonHierref(mIODB)) {
      NU_ASSERT(not net->hasContinuousDriver(eFLBoundProtect), net);
      FLNodeBound *flow_node = mFlowFactory->createBound(net_ref, eFLBoundProtect);
      net->addContinuousDriver(flow_node);
    }

    // The net has been marked mutable because of writers through hierarchical references.
    if (net->isHierRefWritten()) {
      NU_ASSERT (not net->hasContinuousDriver(eFLBoundHierRef), net);
      FLNodeBound *flow_node = mFlowFactory->createBound(net_ref, eFLBoundHierRef);
      net->addContinuousDriver(flow_node);
    }
  }
}


void UnelabFlow::extNet(NUModule *module, NUNetList &nets)
{
  NUNet* ext_net = module->getExtNet();
  NUNetRefHdl ext_net_ref = mNetRefFactory->createNetRef(ext_net);
  FLNode *ext_net_flnode = 0;

  NUNetRefFLNodeMultiMap *use_map = mUseMapStack.top();

  // Now make that fake driver depend on all protected observable nets
  // (except for special nets, including itself).
  for (NUNetList::iterator iter = nets.begin();
       iter != nets.end();
       ++iter) {
    NUNet *net = *iter;
    if (net->isProtectedObservable(mIODB) )
    {
      if ( net->isExtNet()     or
           net->isSysTaskNet() or
           net->isControlFlowNet() ) {
        // do nothing for these special nets
      }
      else {
        // if the following asserts then you probably have defined a
        // new virtual net without thinking about how it should be
        // handled here
        NU_ASSERT ( (dynamic_cast<NUVirtualNet*>(net) == 0), net );

        if (not ext_net_flnode) {
          // Create a fake driver for the ext_net_ref.  Its purpose is to collect the
          // live defs of all the protected observable nets.
          // We create this driver as needed; we do not want it if there are no
          // observables.
          ext_net_flnode = mFlowFactory->create(ext_net_ref, module);
          ext_net->addContinuousDriver(ext_net_flnode);
        }

        NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
        use_map->insert(net_ref, ext_net_flnode);
      }
    }
  }
}


bool UnelabFlow::determineDriven(NUNet *net, FLNode *flow_node)
{
  bool driven = net->isContinuouslyDriven();

  // The net may not drive the flow_node if:
  // . flow_node represents a bidirect port connection which is connected to net
  // . and, all of the other "drivers" on net are also bidirect port connections
  // . and, none of the bidirect formals are driven.
  //
  // Example:
  //module top(i1, o1);
  //   input i1;
  //   output o1;
  //   wire   w;
  //   child child (i1, o1, w);
  //endmodule
  //module child(i, o, io);
  //   input i;
  //   output o;
  //   inout  io;
  //   assign o = i & io;
  //endmodule
  //
  // Here, top.w is not driven, nor is child.io.  Note, however, that
  // child.io does drive child.o.
  //
  if (driven) {
    bool flow_is_bid_driver = false;
    bool all_undriven_bids = true;
    for (NUNet::DriverLoop loop = net->loopContinuousDrivers();
         all_undriven_bids and not loop.atEnd();
         ++loop) {
      FLNode * flow = (*loop);
      NUModuleInstance * inst = dynamic_cast<NUModuleInstance*>(flow->getUseDefNode());

      all_undriven_bids = false;
      if (inst and flow->hasNestedHier()) {
        FLNode * nested = flow->getSingleNested();
        flow_is_bid_driver = nested && (nested->getUseDefNode() == flow_node->getUseDefNode());
        if (nested) {
          NUPortConnectionBid * bid = dynamic_cast<NUPortConnectionBid*>(nested->getUseDefNode());
          if (bid) {
            NUNet * formal = bid->getFormal();
            // If a net is protected mutable, it counts as being driven.
            if (not formal->isProtectedMutable(mIODB)) {
              // Otherwise, see if the formal is driven by seeing if the instantiated module defs it.
              NUNetRefHdl formal_net_ref = mNetRefFactory->createNetRef(formal);
              NUNetRefSet uses(mNetRefFactory);
              if (not inst->getModule()->queryDefs(formal_net_ref, &NUNetRef::overlapsSameNet, mNetRefFactory)) {
                all_undriven_bids = true;
              }
            }
          }
        }
      }
    }
    driven = (not (all_undriven_bids and flow_is_bid_driver));
  }

  return driven;
}


void UnelabFlow::connectFanin(NUNetRefFLNodeMultiMap &use_map,
			      void (FLNode::* fanin_connector)(const NUNetRefHdl &net_ref,
							       NUNetRefCompareFunction fn),
                              NUScope *task_scope)
{
  for (NUNetRefFLNodeMultiMap::MapLoop use_loop = use_map.loop();
       not use_loop.atEnd();
       ++use_loop) {
    NUNetRefHdl driver_net_ref = (*use_loop).first;
    NUNet *driver = driver_net_ref->getNet();
    FLNode *flow_node = (*use_loop).second;

    // Tasks are allowed to reference nets outside their scope (in the module's
    // scope).  For them, the module-level net's drivers are not the appropriate
    // drivers to use.  The drivers that need to be used are determined by
    // the context of the task enables.  So, here, we don't connect any driver.
    bool connect_fanin = ((task_scope == 0) or driver->isChildOf(task_scope));

    // Do not let uses of sys-task or control-flow nets cross blocks. 
    if (driver->isSysTaskNet() or driver->isControlFlowNet()) {
      // NOTE: This is an inconsistency between UD and UnelabFlow. UD
      // shows an unresolved use of the sys-task (or control-flow) net
      // within an always block. This use is not visible at the always
      // block (see UD::helperTransferUD).
      
      // In UnelabFlow, we are preventing the unresolved uses from
      // causing a fanin arc between always blocks. The reason for
      // this is to keep the externa fanin for the always block flow
      // consistent with the external fanin for the nested flow of the
      // always block.
      connect_fanin = false;
    }

    if (connect_fanin) {
      // Make sure driver really drives flow_node
      bool driven = determineDriven(driver, flow_node);

      // Add bound nodes for the undriven parts of the net.
      if (not driven) {
        undrivenNet(driver);
      } else {
        fixPartiallyDriven(driver);
      }

      (flow_node->*fanin_connector)(driver_net_ref, &NUNetRef::overlapsSameNet);
    }
  }
}


void UnelabFlow::collect(NUUseDefNode *node,
                         NUNetRefFLNodeMultiMap *internal_use_map,
                         NUNetRefFLNodeMultiMap *internal_def_map,
                         NUNetRefSet *edge_uses)
{
  bool is_hier_conn = (dynamic_cast<NUModuleInstance*>(node) != 0);

  NUNetRefFLNodeMultiMap *external_use_map = mUseMapStack.top();
  NUNetRefFLNodeMultiMap *external_def_map = mDefMapStack.top();
  NUNetRefFLNodeMultiMap *external_edge_use_map = 0;
  if (edge_uses != 0) {
    external_edge_use_map = mEdgeUseMapStack.top();
  }

  NUStructuredProc *structured_proc = dynamic_cast<NUStructuredProc*>(node);
  bool is_structured_proc = ( structured_proc != NULL);

  NUNetNodeToFlowMap * flow_map = mFlowStack.top();

  // Iterate through the internal uses, place them into the external use map
  // (the use will be resolved by the node above this node).
  for (NUNetRefFLNodeMultiMap::MapLoop loop = internal_use_map->loop();
       not loop.atEnd();
       ++loop) {
    NUNetRefHdl use_net_ref = (*loop).first;

    if ( is_structured_proc && use_net_ref->getNet()->isControlFlowNet() ){
      continue;                 // do not promote any uses of the control flow nets
                                // from inside to outside of an always or initial block 
    }
    FLNode *flow_node = (*loop).second;
    external_use_map->insert(use_net_ref, flow_node);
  }

  // Iterate through the defs of this node, creating a flow node for the def.
  NUNetRefSet defs(mNetRefFactory);
  NUUseDefStmtNode *stmt_node = dynamic_cast<NUUseDefStmtNode*>(node);
  if (stmt_node != 0) {
    stmt_node->getBlockingDefs(&defs);
  } else {
    node->getDefs(&defs);
  }
  for (NUNetRefSetIter def_iter = defs.begin();
       def_iter != defs.end();
       ++def_iter) {
    NUNetRefHdl def_net_ref = *def_iter;

    // Do not create def nodes for ExtNets while collecting.
    // We only create def nodes for ExtNets for moduleInstances and modules.
    if (def_net_ref->getNet()->isExtNet() ) {
      continue;
    }
    // Collect defs for sys task nets within block nesting, but not
    // within hierarchical nesting.  Hierarchical nesting is handled
    // separately (see moduleInstanceConnectVirtNetFanin), because
    // there are not ports for sys task nets.
    if (is_hier_conn and def_net_ref->getNet()->isSysTaskNet()) {
      continue;
    }

    // handle ControlFlowNets like sysTaskNets
    if (is_hier_conn and def_net_ref->getNet()->isControlFlowNet()) {
      continue;
    }

    // Create a flow node to represent this def by this node.
    FLNode *external_flow_node = lookupFlow(def_net_ref, node,
                                            flow_map);
    external_def_map->insert(def_net_ref, external_flow_node);

    // Add all live internal defs as nested flow under the external flow node
    NUNetRefFLNodeMultiMap::CondLoop internal_def_loop = internal_def_map->loop(def_net_ref, &NUNetRef::overlapsSameNet);
    NU_ASSERT(not internal_def_loop.atEnd(), def_net_ref->getNet());     // Make sure we have a def.
    for (; not internal_def_loop.atEnd(); ++internal_def_loop) {
      if (is_hier_conn) {
        external_flow_node->addNestedHier((*internal_def_loop).second);
      } else {
        external_flow_node->addNestedBlock((*internal_def_loop).second);
      }
    }

    // Place everything used by this def into the external use
    // map. (except for control flow nets)
    // Hack around always blocks for now to get non-edge fanin...
    NUNetRefSet uses(mNetRefFactory);
    NUAlwaysBlock *always_block = dynamic_cast<NUAlwaysBlock*>(node);
    if (always_block != 0) {
      always_block->NUStructuredProc::getUses(def_net_ref, &uses);
    } else if (stmt_node != 0) {
      stmt_node->getBlockingUses(def_net_ref, &uses);
    } else {
      node->getUses(def_net_ref, &uses);
    }
    for (NUNetRefSetIter use_iter = uses.begin();
         use_iter != uses.end();
         ++use_iter) {
      NUNetRefHdl use_net_ref = *use_iter;

      if ( is_structured_proc && use_net_ref->getNet()->isControlFlowNet() ){
        continue;     // do not promote any uses of the control flow nets
                      // from inside to outside of an always or initial block 
      }

      external_use_map->insert(use_net_ref, external_flow_node);
    }

    // Add edge uses, if appropriate.
    if (edge_uses != 0) {
      NU_ASSERT(external_edge_use_map != 0, node);
      for (NUNetRefSetIter edge_use_iter = edge_uses->begin();
           edge_use_iter != edge_uses->end();
           ++edge_use_iter) {
        NUNetRefHdl edge_use_net_ref = *edge_use_iter;
        external_edge_use_map->insert(edge_use_net_ref, external_flow_node);
      }
    }
  }
}


void UnelabFlow::pruneTempNets(NUBlock *block_stmt,
                               NUNetRefFLNodeMultiMap *use_map)
{

  // pattern: iterate over use_map, identify refs to remove, put them in local_kill_set,
  //          walk local_kill_set and erase items from use_map
  NUNetRefSet local_kill_set(mNetRefFactory);
  for (NUNetRefFLNodeMultiMap::MapLoop loop = use_map->loop(); not loop.atEnd(); ++loop)
  {
    NUNetRefHdl use_net_ref = (*loop).first;
    NUNet* use_net = use_net_ref->getNet();
    if ( use_net->isTempBlockLocalNonStaticOfScope( block_stmt ) )
    {
      // kill nets that are temp/local/nonstatic/ofThisScope test/block-splitting/localvar.v
      local_kill_set.insert(use_net_ref);
    }
  }

  // removal delayed so we do not modify local list while looping the local list.
  for (NUNetRefSetIter ref_iter = local_kill_set.begin(); ref_iter != local_kill_set.end(); ++ref_iter)
  {
    use_map->erase(*ref_iter);
  }
}


void UnelabFlow::moduleUpdateExtNet(NUModule * module)
{
  for (NUModuleInstanceMultiLoop iter = module->loopInstances(); !iter.atEnd(); ++iter) {
    moduleInstanceUpdateExtNet(module, *iter);
  }

  NUNetList all_nets;
  module->getAllNets(&all_nets);
  updateExtNet(module, all_nets);
}


void UnelabFlow::moduleInstanceUpdateExtNet(NUModule * parent_module,
                                            NUModuleInstance * inst)
{
  NUNetRefFLNodeMultiMap use_map(mNetRefFactory);
  NUNetRefFLNodeMultiMap def_map(mNetRefFactory);
  mUseMapStack.push(&use_map);
  mDefMapStack.push(&def_map);

  // Handle the ports to figure out formal/actual mappings.
  NUPortConnectionInputList iport_list;
  inst->getInputPortConnections(iport_list);
  for (NUPortConnectionInputListIter iter = iport_list.begin();
       iter != iport_list.end();
       iter++) {
    portConnectionInput(*iter);
  }

  NUPortConnectionBidList bport_list;
  inst->getBidPortConnections(bport_list);
  for (NUPortConnectionBidListIter iter = bport_list.begin();
       iter != bport_list.end();
       iter++) {
    portConnectionBidInput(*iter);
  }
  for (NUPortConnectionBidListIter iter = bport_list.begin();
       iter != bport_list.end();
       iter++) {
    portConnectionBidOutput(*iter);
  }

  NUPortConnectionOutputList oport_list;
  inst->getOutputPortConnections(oport_list);
  for (NUPortConnectionOutputListIter iter = oport_list.begin();
       iter != oport_list.end();
       iter++) {
    portConnectionOutput(*iter);
  }

  moduleInstanceConnectExtNetUpdates(inst,
                                     inst->getModule()->getExtNet(),
                                     parent_module->getExtNet(),
                                     def_map);

  // cleanup the def map. these should not persist.
  for (NUNetRefFLNodeMultiMap::MapLoop loop = def_map.loop();
       not loop.atEnd();
       ++loop) {
    FLNode * actual_use_flnode = (*loop).second;
    mFlowFactory->destroy(actual_use_flnode);
  }

  mUseMapStack.pop();
  mDefMapStack.pop();
}

void UnelabFlow::moduleInstanceConnectExtNetUpdates(NUModuleInstance * child_inst,
                                                    NUNet* child_ext_net,
                                                    NUNet* parent_ext_net,
                                                    NUNetRefFLNodeMultiMap &def_map)
{
  NUModule * child_inst_module = child_inst->getModule();
  NUNetRefHdl child_ext_net_ref = mNetRefFactory->createNetRef(child_ext_net);

  // Do not create a virtual net driver for this instance if the instance does
  // not def the virtual net.
  if (not child_inst_module->queryDefs(child_ext_net_ref, &NUNetRef::overlapsSameNet, mNetRefFactory)) {
    return;
  }

  FLNode * ext_net_driver = NULL;
  // Determine if there is already a driver for this instance.
  for (NUNet::DriverLoop loop = parent_ext_net->loopContinuousDrivers();
       (not ext_net_driver) and (not loop.atEnd());
       ++loop) {
    FLNode * driver = (*loop);
    if (driver->getUseDefNode() == child_inst) {
      ext_net_driver = driver;
    }
  }

  if (ext_net_driver == NULL) {
    NUNetRefHdl parent_ext_net_ref = mNetRefFactory->createNetRef(parent_ext_net);
    ext_net_driver = mFlowFactory->create(parent_ext_net_ref, child_inst);
    parent_ext_net->addContinuousDriver(ext_net_driver);
  }

  // Get the ext-net uses from the child module.
  NUNetRefSet ext_net_uses(mNetRefFactory);
  child_inst_module->getUses(child_ext_net_ref, &ext_net_uses);

  // Convert those uses into parent-module references.

  // For everything that the child-ext-net uses (still in terms of formals here)
  for (NUNetRefSetIter use_iter = ext_net_uses.begin();
       use_iter != ext_net_uses.end();
       ++use_iter) {
    NUNetRefHdl use_net_ref = *use_iter;

    // This will take us back to the actuals which are input to the instantiation.
    for (NUNetRefFLNodeMultiMap::CondLoop formal_def_loop = def_map.loop(use_net_ref, &NUNetRef::overlapsSameNet);
	 not formal_def_loop.atEnd();
	 ++formal_def_loop) {
      FLNode * actual_use_flnode = (*formal_def_loop).second;
      NUUseDefNode * use_def = actual_use_flnode->getUseDefNode();
      
      NUNetRefSet actual_uses(mNetRefFactory);

      // get the uses of the use def node.
      use_def->getUses(actual_use_flnode->getDefNetRef(),&actual_uses);

      // for each, connect its drivers to the ext-net flnode.
      for (NUNetRefSet::iterator actual_use_iter = actual_uses.begin();
           actual_use_iter != actual_uses.end();
           ++actual_use_iter) {
        NUNetRefHdl actual_use_net_ref = (*actual_use_iter);

        // Add the drivers of the parent-module references as fanin to the ext-net driver.
        for (NUNetRef::DriverLoop driver_loop = actual_use_net_ref->loopInclusiveDrivers();
             not driver_loop.atEnd();
             ++driver_loop) {
          ext_net_driver->connectFanin((*driver_loop));
        }
      }
    }
  }
}


void UnelabFlow::updateExtNet(NUModule * module, NUNetList & nets)
{
  NUNet * parent_ext_net = module->getExtNet();

  FLNode * ext_net_driver = NULL;
  // Determine if there is already a driver for this instance.
  for (NUNet::DriverLoop loop = parent_ext_net->loopContinuousDrivers();
       (not ext_net_driver) and (not loop.atEnd());
       ++loop) {
    FLNode * driver = (*loop);
    if (driver->getUseDefNode() == module) {
      ext_net_driver = driver;
    }
  }

  // Now make that fake driver depend on all protected observable nets
  // (except for special nets, including itself).
  for (NUNetList::iterator iter = nets.begin();
       iter != nets.end();
       ++iter) {
    NUNet *net = *iter;
    if (net->isProtectedObservable(mIODB) )
    {
      if ( net->isExtNet()     or
           net->isSysTaskNet() or
           net->isControlFlowNet() ) {
        // do nothing for these special nets
      }
      else {
        // if the following asserts then you probably have defined a
        // new virtual net without thinking about how it should be
        // handled here
        NU_ASSERT ( (dynamic_cast<NUVirtualNet*>(net) == 0), net );

        if (not ext_net_driver) {
          // Create a fake driver for the ext_net_ref.  Its purpose is to collect the
          // live defs of all the protected observable nets.
          // We create this driver as needed; we do not want it if there are no
          // observables.
          NUNetRefHdl ext_net_ref = mNetRefFactory->createNetRef(parent_ext_net);
          ext_net_driver = mFlowFactory->create(ext_net_ref, module);
          parent_ext_net->addContinuousDriver(ext_net_driver);
        }

        for (NUNet::DriverLoop driver_loop = net->loopContinuousDrivers();
             not driver_loop.atEnd();
             ++driver_loop) {
          ext_net_driver->connectFanin((*driver_loop));
        }
      }
    }
  }
}
