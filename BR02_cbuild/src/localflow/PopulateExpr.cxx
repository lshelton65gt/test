// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "localflow/TicProtectedNameManager.h"
#include "LFContext.h"
#include "compiler_driver/CarbonContext.h"

#include "hdl/HdlVerilogString.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBitNet.h"
#include "util/AtomicCache.h"

#include "util/ArgProc.h"
#include "util/StringAtom.h"
#include "util/CarbonTypes.h"
#include "util/CbuildMsgContext.h"

#include <cstdlib>
#include <cctype>
#include <cmath>
#ifdef SHOW_EXPR_STRVAL
#include "util/UtIOStream.h"
#endif

NUUnaryOp* Populate::invert (NUExpr* e)
{
  NUUnaryOp* result = new NUUnaryOp ((e->determineBitSize () == 1) ? NUOp::eUnLogNot : NUOp::eUnBitNeg,
                                  e, e->getLoc ());

  result->setSignedResult (e->isSignedResult ());
  return result;
}

Populate::ErrorCode VerilogPopulate::constantRange(veNode ve_range,
                                                   ConstantRange **the_range)
{
  ErrorCode err_code = eSuccess;
  *the_range = 0;

  NUConst *msb_const = 0;
  ErrorCode temp_err_code = constant(veRangeGetLeftRange(ve_range), &msb_const);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NUConst *lsb_const = 0;
  temp_err_code = constant(veRangeGetRightRange(ve_range), &lsb_const);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  SInt32 msb = -1;
  SInt32 lsb = -1;

  bool ok = msb_const->getL(&msb);
  ok = ok and lsb_const->getL(&lsb);
  ok = ok and (veRangeGetSignType(ve_range) == UNSET_SIGN); // UNSET_SIGN means [N:M], POS_SIGN means [N+:s], NEG_SIGN means [N-:s]

  if (not ok) {
    SourceLocator loc = locator(ve_range);
    POPULATE_FAILURE(mMsgContext->UnsupportedRange(&loc, msb, lsb), &err_code);
  }
  *the_range = new ConstantRange(msb, lsb);

  delete msb_const;
  delete lsb_const;

  return err_code;
}


Populate::ErrorCode VerilogPopulate::constant(veNode ve_expr,
                                              NUConst **the_const)
{
  ErrorCode err_code = eSuccess;
  *the_const = 0;
  SourceLocator loc = locator(ve_expr);

  veString ve_string_val = NULL;

#ifdef SHOW_EXPR_STRVAL
  veNodeDecompile(ve_expr, stdout);
  UtIO::cout() << ": ";
#endif

  UInt32 bit_width = 0;
  switch (veExprGetValueType(ve_expr)) {
  case LOCALLY_STATIC:
  case GLOBALLY_STATIC:
  {
    bit_width = UInt32(veExprEvaluateWidth(ve_expr));

    veObjType type = veNodeGetObjType(ve_expr);      

    switch ( type )
    {
    case VE_CONST:
    {
      // veExprEvaluateValue only returns an int.
      SInt32 signed_val = veExprEvaluateValue(ve_expr);
      *the_const = NUConst::create (veExprIsSigned (ve_expr) == VE_TRUE,
                                    signed_val, bit_width, loc);
      break;
    }
    case VE_STRING:
    {
      // get the string as it appears in source file
      CheetahStr ve_string(veStringGetString(ve_expr));
      UtString orig_str(ve_string);
      if ( orig_str.size() <= 2 ) {
        // an empty string would create a constant of size 0 which
        // cannot be supported.  Instead we use a null char.
        // This is not quite correct but in verilog it is valid
        // because the following two assignments are the same
        // foo = "\000";
        // foo = "";  // zero fill foo because RHS is unsigned
        orig_str.clear();       // remove the surrounding quotes
        orig_str << '\0';
      } else {
        orig_str = orig_str.substr(1,orig_str.size()-2); // remove the surrounding quotes that
                                                         // cheetah includes 
      }
      // Convert string into a binary char array.  The Verilog escaped
      // characters are converted into the single char equivalent
      // during this conversion. (Cheetah does not do this for us).
      UtString binary_str;
      HdlVerilogString::convertVerilogStringToBinaryString(&orig_str, &binary_str);

      // convert binary char array into NUConst
      *the_const = NUConst::createKnownConst(binary_str, binary_str.length(), true, loc);
      break;
    }
    case VE_EXPNUMBER:
    case VE_FLOATNUMBER:
    {
      double val = veExprEvaluateValueInDouble( ve_expr );
      *the_const = NUConst::create (val, loc);
      break;
    }
    default: 
    {
      switch ( veExprGetEvaluateType( ve_expr ))
      {
      case VE_EXPREVAL_UNKNOWN:
          POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Cannot determine type of expression"),
                           &err_code);
          return err_code;
          break;
      case VE_EXPREVAL_REAL:
      {
        double val = veExprEvaluateValueInDouble( ve_expr );
        *the_const = NUConst::create( val, loc );
        break;
      }
      case VE_EXPREVAL_BINARY:
      {
        veBool flagXZ = VE_TRUE;
        ve_string_val = veExprEvaluateValueInString(ve_expr, &flagXZ, VE_BIN);
        
        UtString tmp(ve_string_val);

        if (not (bit_width == tmp.size())) {
          POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad constant bit width"), &err_code);
          return err_code;
        }

        // Cheetah's flagXZ not working for z values
        //        if (flagXZ == VE_TRUE)
        bool hasXZ = (tmp.find_first_of("xXzZ?") != UtString::npos);
        if (hasXZ)
          *the_const = NUConst::createXZ(tmp, veExprIsSigned (ve_expr) == VE_TRUE, bit_width, loc);
        else
          *the_const = NUConst::createKnownConst(tmp, bit_width, loc);
        break;
      }
      }
      break;
    }
    }

    break;
  }
  default: {
    POPULATE_FAILURE(mMsgContext->NonStaticConstant(&loc), &err_code);
    return err_code;
    break;
  }
  }
  
  if (ve_string_val) {
    veFreeString(ve_string_val);
  }

  if (not bit_width or not *the_const) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported constant"), &err_code);
    return err_code;
  }

  if (*the_const != 0) {
    // Presume that all constants are the size/sign that they purport
    // to be - otherwise things like getLL() get into trouble!
    (*the_const)->resize ((*the_const)->determineBitSize ());
    (*the_const)->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE);
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::identRvalue(veNode ve_named,
                                                 LFContext* context,
                                                 NUModule* /* module */,
                                                 NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(ve_named);

  NUNet* net = 0;
  ErrorCode temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_named, &net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // Handle constant-valued nets; put the constant into the tree.
  if (net->isSupply0()) {
    UtString val(net->getBitSize(), '0');
    *the_expr = NUConst::createKnownConst(val,  net->getBitSize(), loc);
  } else if (net->isSupply1()) {
    UtString val(net->getBitSize(), '1');
    *the_expr = NUConst::createKnownConst(val,  net->getBitSize(), loc);
  } else {
    *the_expr = new NUIdentRvalue(net, loc);
  }

  (*the_expr)->setSignedResult (veExprIsSigned (ve_named) == VE_TRUE);
  return err_code;
}


bool VerilogPopulate::sVeNodeIsParameter(veNode ve_ident)
{
  return veNodeIsA(ve_ident, VE_PARAM) || veNodeIsA(ve_ident, VE_LOCALPARAM);
}

bool VerilogPopulate::sGetParameterRange(veNode ve_ident, ConstantRange* range )
{
  bool range_updated = false;

  veNode ve_declared_range = NULL;

  if (veNodeIsA(ve_ident, VE_PARAM)) {
    ve_declared_range = veParamGetParamRange(ve_ident);
  } else if (veNodeIsA(ve_ident, VE_LOCALPARAM)) {
    ve_declared_range = veLocalParamGetRange(ve_ident);
  } else if (veNodeIsA(ve_ident, VE_DEFPARAM)) {
    ve_declared_range = veDefParamGetParamRange(ve_ident);
  } else {
    gCheetahAssert("Unsupported parameter type or non-parameter passed in.", gGetObjTypeStr(veNodeGetObjType(ve_ident)), ve_ident);
    return range_updated;
  }
  if ( ve_declared_range ) {
    veNode leftR = veRangeGetLeftRange(ve_declared_range);
    SInt64 leftVal = veConstGetDecNumber(leftR);
 
    veNode rightR = veRangeGetRightRange(ve_declared_range);
    SInt64 rightVal = veConstGetDecNumber(rightR);

    if ( 0 != rightVal ) {
      SourceLocator loc = locator(ve_ident);
      UtString msg("Unsupported parameter range: [");
      msg << leftVal << ":" << rightVal << "]. Only ranges of the form N:0 are supported.";
      mMsgContext->CheetahConsistency(&loc, msg.c_str());
      return false;
    }
    
    range->setMsb(static_cast<SInt32>(leftVal));
    range->setLsb(static_cast<SInt32>(rightVal));
    return true;
  }

  // no declared range, try to get the range from the value of the parameter
  veNode ve_param_value = NULL;

  if (veNodeIsA(ve_ident, VE_PARAM)) {
    ve_param_value = veParamGetCurrentValue(ve_ident);
  } else if (veNodeIsA(ve_ident, VE_LOCALPARAM)) {
    ve_param_value = veLocalParamGetValue(ve_ident);
  } else if (veNodeIsA(ve_ident, VE_DEFPARAM)) {
    ve_param_value = veDefParamGetParamValue(ve_ident);
  }
  if ( ve_param_value ) {
    if (veNodeGetObjType (ve_param_value) == VE_BASEDNUMBER) {
      // for based numbers the range of the param is [WIDTH-1:0]
      UInt32 width = veBasedNumberGetWidth(ve_param_value);
      range->setMsb(width-1);
      range->setLsb(0);
      return true;
    } else {
      // non-based numbers that have a value are the size of unsized numbers (we assume 31:0)
      range->setMsb(31);
      range->setLsb(0);
      return true;
    }
  }

  gCheetahAssert("Unable to determine parameter range.", gGetObjTypeStr(veNodeGetObjType(ve_ident)), ve_ident);
  return range_updated;
}

Populate::ErrorCode VerilogPopulate::bitselRvalue(veNode ve_expr,
                                                  LFContext *context,
                                                  NUModule* module,
                                                  NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(ve_expr);

  // Is this an array select or a normal bit-select?
  bool arraySelect = veNodeIsA(ve_expr, VE_ARRAYBITSELECT) == VE_TRUE;

  ErrorCode temp_err_code;
  NUExpr *bitsel_expr = 0;

  // get the ident being selected
  veNode ve_named = arraySelect ? veArrayBitSelectGetVariable(ve_expr) : veBitSelectGetVariable(ve_expr);
  veObjType type = veNodeGetObjType(ve_named);

  bool isGenvar = false;
  if ((not arraySelect) and ( VE_NAMEDOBJECTUSE == type )){
    veNode parent = veNamedObjectUseGetParent(ve_named);
    if ( veNodeGetObjType(parent) == VE_GENVAR ) {
      // here we have a bitsel of a genvar, the genvar will be a
      // constant, but the bitsel may be a variable.
      isGenvar = true;
      CheetahStr temp_val(veNamedObjectEvaluateValueInBinaryString(parent));
      UInt32 gvalue = veGetDecimalNumberFromBinaryString(temp_val);
      NUConst* genvar_const = NUConst::create(false, gvalue, 32, loc);

      temp_err_code = expr(veBitSelectGetSelectedBit(ve_expr),
                           context,
                           module,
                           0,
                           &bitsel_expr);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
      bitsel_expr->resize(bitsel_expr->determineBitSize()); // self-determined

      *the_expr = new NUVarselRvalue(genvar_const, bitsel_expr, loc);
      return err_code;
    }
  }


  veNode ve_ident = veNamedObjectUseGetParent(ve_named);
  
  if (sVeNodeIsParameter(ve_ident)) {

    NU_ASSERT(!arraySelect, module);

    temp_err_code = expr(veBitSelectGetSelectedBit(ve_expr),
                         context,
                         module,
                         0,
                         &bitsel_expr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    bitsel_expr->resize(bitsel_expr->determineBitSize()); // self-determined

    ParamValue paramVal;
    temp_err_code = getParamValueString(&paramVal, ve_named);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    const UtString& paramValStr = paramVal.getStr();
    UInt32 paramSize = paramVal.getSize();

    NUConst* identConst = NULL;
    if (paramVal.hasXZ())
      identConst = NUConst::createXZ(paramValStr, veExprIsSigned(ve_named) == VE_TRUE, paramSize, loc);
    else
      identConst = NUConst::createKnownConst(paramValStr, paramSize, loc);
    
    // constant size is self-determined
    identConst->resize(identConst->determineBitSize());

    // create the bit select
    *the_expr = new NUVarselRvalue (identConst, bitsel_expr, loc);
    (*the_expr)->setSignedResult (veExprIsSigned (ve_named) == VE_TRUE);
  }
  else {
    NUNet* net = 0;
    ErrorCode temp_err_code = mPopulate->lookupNet(context->getDeclarationScope(), ve_named, &net);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    UInt32 dimension = net->getDeclaredDimensions();

    // Cheetah overloads bit-select functionality to represent row-level memory
    // access, so a memory read will get here as a VE_BITSELECT.
    // For bit-level memory access, Cheetah uses VE_ARRAYBITSELECT, which will
    // also get us here.  The two cases can be distinguished by the arraySelect
    // boolean.  When arraySelect is true, we have to use a list of bit-selects,
    // but when it is false we have only a single bit-select expression.
    if (dimension == 2) {
      if (!arraySelect) {
        temp_err_code = getBitOrArrayBitSelExpr(veBitSelectGetSelectedBit(ve_expr),
                                                context, module, 0,
                                                loc, net->getMemoryNet(), true,
                                                &bitsel_expr);

        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }

        *the_expr = new NUMemselRvalue(net, bitsel_expr, loc);
        (*the_expr)->setSignedResult (veExprIsSigned (ve_named) == VE_TRUE); // want to believe this!
      } else {
        // this is a bit-level memory access
        CheetahList bitsel_list(veArrayBitSelectGetSelBitList(ve_expr));
        int list_size = veListGetSize(bitsel_list);
        if (list_size != 2) {
          POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Array select has unsupported dimensionality"), &err_code);
          return err_code;
        }
        veNode ve_row_select = veListGetNextNode(bitsel_list);
        veNode ve_bit_select = veListGetNextNode(bitsel_list);
        NUExpr* rowsel_expr = 0;
        NUMemoryNet* mem = net->getMemoryNet();

        temp_err_code = getBitOrArrayBitSelExpr(ve_row_select, context, module,
                                                0, loc,
                                                mem, true, &rowsel_expr);

        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }

        temp_err_code = getBitOrArrayBitSelExpr(ve_bit_select, context, module,
                                                0, loc, mem, false,
                                                &bitsel_expr);

        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }

        NUExpr* memsel_expr = new NUMemselRvalue(mem, rowsel_expr, loc);
        *the_expr = Populate::genVarselRvalue(mem, memsel_expr, bitsel_expr, loc, false);
        (*the_expr)->setSignedResult (veExprIsSigned (ve_named) == VE_TRUE);
      }
    } else if (dimension >= 3) {
      CheetahList ve_bitsel_list(veArrayBitSelectGetSelBitList(ve_expr));
      veNode ve_bit_element = NULL;
      NUMemoryNet* mem = net->getMemoryNet();
      NUExpr* rowsel_expr = 0;
      ve_bit_element = veListGetNextNode (ve_bitsel_list);

      temp_err_code = getBitOrArrayBitSelExpr(ve_bit_element, context, module,
                                              0, loc, mem, true, &rowsel_expr);

      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      NUExpr* memsel_expr = new NUMemselRvalue(mem, rowsel_expr, loc);
      if ( memsel_expr == NULL )
      {
        mMsgContext->CheetahConsistency( &loc, "Unsupported rvalue memory expression." );
        return eFatal;
      }

      NUMemselRvalue *memsel_rval = dynamic_cast<NUMemselRvalue*>(memsel_expr);

      while ((ve_bit_element = veListGetNextNode (ve_bitsel_list)) != NULL) {

        // Here, we need to know what memory range we are grabbing, whether
        // this index refers to a word/bit.  If it refers to a
        // word the index needs to be jammed into the memsel.  If it
        // refers to a bit, we need to generate a varsel.
        const UInt32 numMemDims = mem->getNumDims();
        const UInt32 numSelDims = memsel_rval->getNumDims();
        bool normalize = false;
        if (numMemDims == numSelDims + 1)
          normalize = true;

        temp_err_code = getBitOrArrayBitSelExpr(ve_bit_element, context, module,
                                                0, loc, mem, !normalize,
                                                &bitsel_expr);

        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }

        if (normalize)
        {
          // maybe sGenBitOrPartsel type function need to be called
          *the_expr = Populate::genVarselRvalue(mem, memsel_expr, bitsel_expr, loc, true);
          if (*the_expr == NULL)
          {
            ExprRangeError( mem, bitsel_expr, loc );
            delete memsel_expr;
            return eFailPopulate;
          }
        }
        else
        {
          memsel_rval->addIndexExpr( bitsel_expr );
          *the_expr = memsel_rval;
        }
      }
      (*the_expr)->setSignedResult (veExprIsSigned (ve_named) == VE_TRUE);
    } else {
      NU_ASSERT(!arraySelect, module);
      // Handle constant-valued nets; put the constant into the tree.
      if (net->isSupply0()) {
        *the_expr = NUConst::create(false, 0u, 1, loc);
      } else if (net->isSupply1()) {
        *the_expr = NUConst::create(false, 1u, 1, loc);
      } else {
        // normalize all bit references, synthesizing corrective
        // arithmetic as needed.
        temp_err_code = expr(veBitSelectGetSelectedBit(ve_expr),
                             context,
                             module,
                             0, // self-determined
                             &bitsel_expr);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
        bitsel_expr->resize(bitsel_expr->determineBitSize()); // self-determined
        // test/interra2/misc/MISC132/MISC132.v uses a bit-select
        // on a scalar, and Cheetah lets it thru.  I think codegen
        // filters this.
        *the_expr = Populate::genVarselRvalue(net, bitsel_expr, loc, false);
      }
    }
  }
  
  return err_code;
}


Populate::ErrorCode VerilogPopulate::partselRvalue(veNode ve_expr, 
                                                   LFContext *context,
                                                   NUModule* module,
                                                   NUExpr **the_expr)
{
  NUExpr* index_expr = 0;       // Holds whatever index we figure out
  NUExpr* row_expr = 0;         // If this is an array select, holds row expression for array
  NUExpr* col_expr = 0;
  ConstantRange *range = 0;     // Holds the range expression
  NUNet* net;
  SInt32 constant_value = 0;

  ErrorCode err_code = eSuccess;

  ErrorCode temp_err_code = partsel (ve_expr, context, module,
                                     &net, &constant_value, &row_expr,
                                     &col_expr,
                                     &index_expr,
                                     &range);

  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    delete row_expr;
    delete col_expr;
    delete index_expr;
    delete range;
    return temp_err_code;
  }



  // Handle constant-valued nets; put the constant into the tree.
  SourceLocator loc = locator (ve_expr);
  UInt32 bit_size = range->getLength();
  
  if ( net == NULL ) {

    if (veNodeIsA(ve_expr, VE_PARTSELECT) && sVeNodeIsParameter(veNamedObjectUseGetParent(vePartSelectGetVariable(ve_expr)))) {

      veNode ve_named = vePartSelectGetVariable(ve_expr);

      ParamValue paramVal;
      temp_err_code = getParamValueString(&paramVal, ve_named);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
	return temp_err_code;
      }
      
      const UtString& paramValStr = paramVal.getStr();
      UInt32 paramSize = paramVal.getSize();

      NUConst* identConst = NULL;
      if (paramVal.hasXZ()) {
	identConst = NUConst::createXZ(paramValStr, veExprIsSigned(ve_named) == VE_TRUE, paramSize, loc);
      } 
      else {
	identConst = NUConst::createKnownConst(paramValStr, paramSize, loc);      
      }
      // constant size is self-determined
      identConst->resize(identConst->determineBitSize());

      *the_expr = new NUVarselRvalue(identConst, index_expr, *range, loc);
    }
    else {
      // this must be a partsel of a genvar, so just use the constant_value
      NUExpr* const_expr = NUConst::create(false, constant_value, (range->getMsb())+1, loc);
      *the_expr =  new NUVarselRvalue(const_expr, index_expr, *range, loc);
    }
  } else if (net->isSupply0()) {
    *the_expr = NUConst::create(false, 0,  bit_size, loc);
  } else if (net->isSupply1()) {
    DynBitVector val (net->getBitSize());
    val.set ();                // all ones
    *the_expr = NUConst::create(false, val,  bit_size, loc);
  } else {
    if (row_expr)
    {
      row_expr->resize(row_expr->determineBitSize());
      // Limit the size of select to 32 bits.
      row_expr = limitSelectIndexExpr(net->getMemoryNet(), loc, row_expr);
      NUMemselRvalue* memsel = new NUMemselRvalue(net, row_expr, loc);
      if (col_expr) {
        col_expr->resize(col_expr->determineBitSize());
        // Limit the size of select to 32 bits.
        col_expr = limitSelectIndexExpr(net->getMemoryNet(), loc, col_expr);
        memsel->addIndexExpr(col_expr);
      }
      *the_expr = new NUVarselRvalue(memsel, index_expr, *range, loc);
    }
    else {
      *the_expr = new NUVarselRvalue(net, index_expr, *range, loc);
    }
  }
  delete range;

  if (*the_expr) {
    (*the_expr)->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE);
  }

  return err_code;
}

// Fix the signedness of operands to binary operators.  We cannot exclusively
// depend on the value returned by veExprIsSigned(), as Cheetah treats NAMEDOBJECTUSE
// leaves as if the operand were self-determined.
//
static void sFixSign (NUBinaryOp* expr) {
  NUExpr* lop = expr->getArg (0);
  NUExpr* rop = expr->getArg (1);

  switch (expr->getOp ()) {
  default:
    // Verilog rules treat the operands as being of the same signedness as the resulting
    // expression
    lop->setSignedResult (expr->isSignedResult ());
    rop->setSignedResult (expr->isSignedResult ());
    break;

  case NUOp::eBiRshift:
  case NUOp::eBiRshiftArith:
  case NUOp::eBiLshift:
  case NUOp::eBiLshiftArith:
    // RHS is self-determined (only force LHS to match)
    lop->setSignedResult (expr->isSignedResult ());
    rop->setSignedResult (false);
    break;
    

  case NUOp::eBiNeq:
  case NUOp::eBiEq:
  case NUOp::eBiTrieq:
  case NUOp::eBiTrineq:
    if (lop->isSignedResult () != rop->isSignedResult ()) {
      lop->setSignedResult (false);
      rop->setSignedResult (false);
    }
    break;

  case NUOp::eBiSLt:
  case NUOp::eBiSLte:
  case NUOp::eBiSGtr:
  case NUOp::eBiSGtre:
    // Wouldn't be signed unless both operands were signed already
    break;

  case NUOp::eBiULt:
  case NUOp::eBiULte:
  case NUOp::eBiUGtr:
  case NUOp::eBiUGtre:
    lop->setSignedResult (false);
    rop->setSignedResult (false);
    break;

  case NUOp::eBiLogAnd:
  case NUOp::eBiLogOr:
    break;

  case NUOp::eBiDExp:           // Leave signs of ** operands the way
  case NUOp::eBiExp:            //   we found 'em.
    break;
  }
}

Populate::ErrorCode VerilogPopulate::eventExpr(veNode ve_event,
                                               LFContext *context,
                                               const SourceLocator &loc,
                                               NUExpr **the_expr)
{
  // The caller supplies the location of the use of the event since
  // locator(ve_event) usually does not point to the correct line.

  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  ClockEdge edge = eClockPosedge;
  NUExpr* clkExpr = NULL;
  NUExpr* event_expr = NULL;

  ErrorCode temp_err_code = eSuccess;
  switch (veNodeGetObjType(ve_event)) {
  case VE_POSE_SCALAR_EVENT_EXPR:
    temp_err_code = expr(vePosEdgeExprGetEventExpr(ve_event),
			 context,
			 context->getModule(),
			 0,
			 &clkExpr);
    break;
  case VE_NEG_SCALAR_EVENT_EXPR:
    temp_err_code = expr(veNegEdgeExprGetEventExpr(ve_event),
			 context,
			 context->getModule(),
			 0,
			 &clkExpr);
    edge = eClockNegedge;
    break;
  default:
    // Use regular expression processing for the rest
    temp_err_code = expr(ve_event, context, context->getModule(), 0,
                         &event_expr);
    break;
  }

  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  // For non-clock event expressions, just pass the expression back
  if (clkExpr == NULL) {
    NU_ASSERT ((event_expr != NULL), context->getModule());

    // Make sure the event is at the beginning of the always block. If
    // it isn't then we ignore them. But we warn the user.
    if ((not context->isValidForEventExpr()) or
        (not context->getStmtList()->empty())) {
      // SourceLocator loc = clkExpr->getLoc();
      mMsgContext->IgnoredEvent(&loc);
      delete event_expr;
    } else {
      // It is at the beginning of the always block, return it to our
      // caller.
      *the_expr = event_expr;
    }

    return err_code;
  }

  if ((not context->isValidForEdgeExpr()) or
      (not context->getStmtList()->empty())) {
    // SourceLocator loc = clkExpr->getLoc();
    POPULATE_FAILURE(mMsgContext->UnsupportedEvent(&loc), &err_code);
    delete clkExpr;
    return err_code;
  }

  NUEdgeExpr* edge_expr;
  err_code = singleBitEdgeExpr(edge, clkExpr, context, &edge_expr);
  *the_expr = edge_expr;
  return err_code;
}

// Does the relational have signed or unsigned semantics?
static bool sIsSignedRelOp (veNode ve_expr)
{
  return veExprIsSigned (veBinaryGetLeftOp (ve_expr))
    && veExprIsSigned (veBinaryGetRightOp (ve_expr));
}

Populate::ErrorCode VerilogPopulate::binaryExpr(veNode ve_expr,
                                                LFContext *context,
                                                NUModule* module,
                                                int lWidth,
                                                NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(ve_expr);

  NUOp::OpT op = NUOp::eInvalid;
  bool invert = false;
  
  bool cheetah_is_signed = veExprIsSigned(ve_expr) == VE_TRUE;
  bool is_real =  (VE_EXPREVAL_REAL == veExprGetEvaluateType( ve_expr ));

  switch (veBinaryGetOpType(ve_expr)) {
  case PLUS_USE: op = NUOp::eBiPlus; break;
  case MINUS_USE: op = NUOp::eBiMinus; break;
  case STAR_USE: op = (cheetah_is_signed | is_real) ? NUOp::eBiSMult : NUOp::eBiUMult; break;
  case DIV_USE: op = (cheetah_is_signed | is_real) ? NUOp::eBiSDiv : NUOp::eBiUDiv; break;
  case PERCENT_USE: op = cheetah_is_signed ? NUOp::eBiSMod : NUOp::eBiUMod; break;
  case LOGICAL_EQUAL_USE: op = NUOp::eBiEq; break;
  case LOGICAL_INEQUAL_USE: op = NUOp::eBiNeq; break;
  case CASE_EQUAL_USE: op = NUOp::eBiTrieq; break;
  case CASE_INEQUAL_USE: op = NUOp::eBiTrineq; break;
  case LOGICAL_OR_USE: op = NUOp::eBiLogOr; break;
  case LOGICAL_AND_USE: op = NUOp::eBiLogAnd; break;
  case LT_USE: op = sIsSignedRelOp (ve_expr) ? NUOp::eBiSLt : NUOp::eBiULt; break;
  case GT_USE: op = sIsSignedRelOp (ve_expr) ? NUOp::eBiSGtr: NUOp::eBiUGtr; break;
  case LT_EQUAL_USE: op = sIsSignedRelOp (ve_expr) ? NUOp::eBiSLte : NUOp::eBiULte; break;
  case GT_EQUAL_USE: op = sIsSignedRelOp (ve_expr) ?NUOp::eBiSGtre : NUOp::eBiUGtre; break;
  case REDUCT_XNOR_OR_BIT_EQUIVAL_USE: op = NUOp::eBiBitXor; invert = true; break;
  case LEFT_SHIFT_USE: op = NUOp::eBiLshift; break;
  case ARITH_LEFT_SHIFT_USE: op = NUOp::eBiLshiftArith; break;
  case ARITH_RIGHT_SHIFT_USE: op = cheetah_is_signed ? NUOp::eBiRshiftArith : NUOp::eBiRshift ; break;
  case RIGHT_SHIFT_USE: op = NUOp::eBiRshift; break;
  case BITWISE_OR_USE: op = NUOp::eBiBitOr; break;
  case BITWISE_AND_USE: op = NUOp::eBiBitAnd; break;
  case BITWISE_XOR_USE: op = NUOp::eBiBitXor; break;
  case POWER_USE: op = NUOp::eBiExp; break;
  default:
  {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported binary expression"), &err_code);
    return err_code;
  }
  break;
  }

  bool is_signed_result;
  
  switch ( op ){
  case NUOp::eBiUMult:
  case NUOp::eBiUDiv:
  case NUOp::eBiUMod:
  case NUOp::eBiEq:
  case NUOp::eBiNeq:
  case NUOp::eBiTrieq:
  case NUOp::eBiTrineq:
  case NUOp::eBiLogOr:
  case NUOp::eBiLogAnd:
  case NUOp::eBiSLt:
  case NUOp::eBiULt:
  case NUOp::eBiSGtr:
  case NUOp::eBiUGtr:
  case NUOp::eBiSLte:
  case NUOp::eBiULte:
  case NUOp::eBiSGtre:
  case NUOp::eBiUGtre:
  {
    is_signed_result = false;    // LRM says these produce unsigned results
    break;
  }
  default:{
    is_signed_result = cheetah_is_signed | is_real;
    break;
  }
  }


  NUExpr *expr1 = 0;
  ErrorCode temp_err_code = expr(veBinaryGetLeftOp(ve_expr), context, module, 0, &expr1);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NUExpr *expr2 = 0;
  temp_err_code = expr(veBinaryGetRightOp(ve_expr), context, module, 0, &expr2);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  
  if (op == NUOp::eBiExp ){
    // this is the Verilog POW() operator "**") adjust the operator type and coerce operands as necessary 

    bool usingVerilog2005StylePowerOperator = mCarbonContext->usingVerilog2005StylePowerOperator();
  
    if ( usingVerilog2005StylePowerOperator ) {
      // the Verilog 2005, and SystemVerilog LRMs say that the result of POW operator is real if either operand is real
      if ( expr1->isReal () || expr2->isReal () ){
        op = NUOp::eBiDExp;
      }
    } else {
      // The Verilog 2001 LRM says POW involving signed, integer or real operands produces real result, 
      // and thus the non-self determined operands are to be coerced to real before evaluation
      if (expr1->isReal () ||
          expr2->isReal () ||
          expr1->isSignedResult () ||
          expr2->isSignedResult ()   ) {
        op = NUOp::eBiDExp;
      }
    }

    // and LRM says that the non-self determined operands should be
    // coerced to the type of the expression, the second operand of power operator is self determined so we only potentially adjust the
    // first operand here.
    if ( ( NUOp::eBiDExp == op ) && ( not (expr1->isReal()) ) ){
      expr1 = expr1->coerceOperandsToReal();
      if ( not expr1->isReal() ) {
        // found some invalid verilog. test/langcov/Real/coerce_02.v
        POPULATE_FAILURE(mMsgContext->CoercedRealOperandNotCompatible(expr1), &err_code);
        delete expr1;
        delete expr2;
        return err_code;
        // If this were valid verilog then we must resize before we
        // wrap with conversion to real so that no precision is lost.
        // expr1 = expr1->makeSizeExplicit(64);
        // expr1 = new NUUnaryOp (NUOp::eUnItoR, expr1, expr1->getLoc ());
      }
    }
  }


  if (expr1->isReal() != expr2->isReal())
  {
    // REAL ** i , i ** REAL are okay, others must be converted to reals
    // TODO this really should coerce all non-self determined operands to real
    if ( op != NUOp::eBiDExp ){
      if ( expr1->isReal() ){
        NU_ASSERT(not expr2->isReal(), expr2);
        expr2 = new NUUnaryOp (NUOp::eUnItoR, expr2, expr2->getLoc ());
      } else {
        NU_ASSERT(not expr1->isReal(), expr1);
        expr1 = new NUUnaryOp (NUOp::eUnItoR, expr1, expr1->getLoc ());
      }
    }
  }
  

  NUBinaryOp *bop = new NUBinaryOp(op, expr1, expr2, loc);
  bop->setSignedResult (is_signed_result);
  sFixSign (bop);
  
  temp_err_code = cleanupXZBinaryExpr(bop, lWidth, loc, the_expr);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  if (invert) {
    *the_expr = Populate::invert (*the_expr);
  }

  (*the_expr)->resize((*the_expr)->determineBitSize());
  return err_code;
}


Populate::ErrorCode VerilogPopulate::cleanupXZBinaryExpr(NUExpr *in_expr,
                                                         int lWidth,
                                                         const SourceLocator &loc,
                                                         NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = in_expr;

  NUBinaryOp *binary_expr = dynamic_cast<NUBinaryOp*>(in_expr);
  if (not binary_expr) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Binary expression expected"), &err_code);
    return err_code;
  }

  NUOp::OpT op = binary_expr->getOp();

  NUExpr *expr1 = binary_expr->getArg(0);
  NUExpr *expr2 = binary_expr->getArg(1);

  // analyze the expressions. If either is constant and contains
  // unknowns then this binary op must be resynthesized.
  NUConst* castExpr1 = expr1->castConst ();
  NUConst* castExpr2 = expr2->castConst ();
  
  // For non-comparison operators, we leave the expression alone.
  // Except for mod and divide, in that case change to an x if the
  // second arg is an x.
  switch (op) {
  case NUOp::eBiPlus:
  case NUOp::eBiMinus:
  case NUOp::eBiSMult:
  case NUOp::eBiUMult:
  case NUOp::eBiLshift:
  case NUOp::eBiRshift:
  case NUOp::eBiLshiftArith:
  case NUOp::eBiRshiftArith:
  case NUOp::eBiBitOr:
  case NUOp::eBiBitAnd:
  case NUOp::eBiBitXor:
    return eSuccess;
    break;

  case NUOp::eBiSDiv:
  case NUOp::eBiUDiv:
  case NUOp::eBiSMod:
  case NUOp::eBiUMod:
    if (castExpr2 && castExpr2->castConstXZ ()) {
      break;
    } else {
      return eSuccess;
    }
    break;

  default:
    // fallthrough
    break;
  }

  if ((castExpr1 && castExpr1->castConstXZ ()) || (castExpr2 && castExpr2->castConstXZ ())) {
    // oh swell. We get to resynth

    // we need the maximum size of the expression to properly
    // size the constants

    // size the expressions
    expr1->resize(expr1->determineBitSize());
    expr2->resize(expr2->determineBitSize());

    UInt32 maxSize = std::max(expr1->getBitSize(), expr2->getBitSize());
    // check against the lwidth
    maxSize = std::max(maxSize, static_cast<UInt32>(lWidth));

    UtString constValStr;
    NUConst* constVal = NULL;
    char polarity = 'm';
    UInt64 intVal = 0;

    // Next, if both are constants then take the Rvalue's
    // polarity. The polarity is used for value-creating binary ops.
    if (castExpr1 && castExpr1->drivesZ())
      polarity = 'z';
    else
      polarity = 'x';
    
    if (castExpr2 && castExpr2->drivesZ())
      polarity = 'z';
    else
      polarity = 'x';
    
    if (polarity == 'm') {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unknown constant"), &err_code);
      return err_code;
    }

    switch (op)
    {
    case NUOp::eBiSDiv:
    case NUOp::eBiUDiv:
    case NUOp::eBiSMod:
    case NUOp::eBiUMod:
      constValStr.append(maxSize, polarity);
      constVal = NUConst::createXZ(constValStr, false, maxSize, loc);
      break;

      // These are always false if either expression has unknowns
    case NUOp::eBiEq:
    case NUOp::eBiNeq:
    case NUOp::eBiLogOr:
    case NUOp::eBiLogAnd:
    case NUOp::eBiSLt:
    case NUOp::eBiSLte:
    case NUOp::eBiSGtr:
    case NUOp::eBiSGtre:
    case NUOp::eBiULt:
    case NUOp::eBiULte:
    case NUOp::eBiUGtr:
    case NUOp::eBiUGtre:
      constVal = NUConst::create(false, 0ULL, 32, loc);
      break;
      
      // === and !===, result in false and true, respectively, except
      // if both operands are constants, in which case we let this
      // fall through (not setting constVal)
    case NUOp::eBiTrieq:
    case NUOp::eBiTrineq:
      if (! (castExpr1 && castExpr2))
      {
        intVal = 0;
        if (op == NUOp::eBiTrineq)
          intVal = 1;
        constVal = NUConst::create(false, intVal, 32, loc);
      }
      break;
      
    default:
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported binary expression"), &err_code);
      return err_code;
      break;
    }
    
    if (constVal != NULL) {
      delete *the_expr;
      *the_expr = constVal;
    }
  }
  
  return err_code;
}

// Overloaded for unary operators.
static void sFixSign (NUUnaryOp* expr) {
  switch (expr->getOp ()) {
  case NUOp::eUnLogNot:
  case NUOp::eUnRedAnd:
  case NUOp::eUnRedOr:
  case NUOp::eUnRedXor:
    break;
  default:
    expr->getArg (0)->setSignedResult (expr->isSignedResult ());
  }
}


Populate::ErrorCode VerilogPopulate::unaryExpr(veNode ve_expr,
                                               LFContext *context,
                                               NUModule* module,
                                               int /* lWidth */,
                                               NUUnaryOp **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(ve_expr);

  NUOp::OpT op = NUOp::eInvalid;
  bool invert = false;

  switch (veUnaryGetOpType(ve_expr)) {
  case UNARY_PLUS_USE: op = NUOp::eUnPlus; break;
  case UNARY_MINUS_USE: op = NUOp::eUnMinus; break;
  case LOGICAL_NEGATION_USE: op = NUOp::eUnLogNot; break;
  case BITWISE_NEGATION_USE: op = NUOp::eUnBitNeg; break;
  case REDUCT_AND_USE: op = NUOp::eUnRedAnd; break;
  case REDUCT_NAND_USE: op = NUOp::eUnRedAnd; invert = true; break;
  case REDUCT_OR_USE: op = NUOp::eUnRedOr; break;
  case REDUCT_NOR_USE: op = NUOp::eUnRedOr; invert = true; break;
  case REDUCT_XOR_USE: op = NUOp::eUnRedXor; break;
  case REDUCT_XNOR_OR_BIT_EQUIVAL_USE: op = NUOp::eUnRedXor; invert = true; break;
  default:
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported unary expression"), &err_code);
    return err_code;
    break;
  }

  NUExpr *expr1 = 0;
  ErrorCode temp_err_code = expr(veUnaryGetOperand(ve_expr), context, module, 0, &expr1);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NUUnaryOp *uop = new NUUnaryOp(op, expr1, loc);
  uop->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE);

  if (invert)
    uop = Populate::invert (uop);

  sFixSign (uop);
  *the_expr = uop;

  return err_code;
}


Populate::ErrorCode VerilogPopulate::condExpr(veNode ve_expr,
                                              LFContext *context,
                                              NUModule* module,
                                              int lWidth,
                                              NUTernaryOp **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(ve_expr);

  NUExpr *expr1 = 0;
  ErrorCode temp_err_code = expr(veCondOpGetCondition(ve_expr), context, module, 0, &expr1);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  NUExpr *expr2 = 0;
  temp_err_code = expr(veCondOpGetLeftExpr(ve_expr), context, module, lWidth, &expr2);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  NUExpr *expr3 = 0;
  temp_err_code = expr(veCondOpGetRightExpr(ve_expr), context, module, lWidth, &expr3);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  temp_err_code = maybeFixConditionStmt(&expr1, loc);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  expr1 = buildCondition(expr1);

  *the_expr = new NUTernaryOp(NUOp::eTeCond, expr1, expr2, expr3, loc);
  (*the_expr)->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE);
  expr2->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE);
  expr3->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE);

  return err_code;
}

Populate::ErrorCode VerilogPopulate::exprVec(CheetahList& ve_expr_iter,
                                             LFContext* context,
                                             NUModule* module,
                                             int lWidth,
                                             NUExprVector* expr_vector)
{
  ErrorCode err_code = eSuccess;
  if (ve_expr_iter) {
    while (veNode ve_expr = VeListGetNextNode(ve_expr_iter)) {
      NUExpr *this_expr = 0;
      ErrorCode temp_err_code = expr(ve_expr, context, module,
                                     lWidth, &this_expr);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      if (this_expr) {
	this_expr->resize (this_expr->determineBitSize ());
	expr_vector->push_back (this_expr);
      }
    }
  }
  return err_code;
}

Populate::ErrorCode VerilogPopulate::concatExpr(veNode ve_expr,
                                                LFContext *context,
                                                NUModule* module,
                                                NUExpr **the_op)
{
  ErrorCode err_code = eSuccess;
  *the_op = 0;

  SourceLocator loc = locator(ve_expr);

  UInt64 repeat_count = 1;
  ErrorCode temp_err_code2 = fetchConcatRepeatCount(ve_expr, loc, &repeat_count);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code2, &err_code ) ) {
    return temp_err_code2;
  }

  if (repeat_count == 0) {
    // A repeat-count of 0 (or negative, zerod by fetchConcatRepeatCount) 
    // results in a value of 1'b0.  This is the exact same behavior
    // as Aldec, and is compatible with Finsim, which substitutes 1'bx.
    // It is also consistent with what Cheetah does when the repeat-count
    // does not come from a parameter.
    //
    // See test/concat/zero_rpt_concat4.v.
    //
    *the_op = NUConst::create(false, 0, 1, loc);
    return err_code;
  }

  NUExprVector expr_vector;
  CheetahList concats(veConcatGetConcatList(ve_expr));
  ErrorCode temp_err_code1 = exprVec(concats, context, module, 0, &expr_vector);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code1, &err_code ) ) {
    return err_code;
  }

  (*the_op) = new NUConcatOp(expr_vector, repeat_count, loc);
  (*the_op)->setSignedResult (veExprIsSigned (ve_expr) == VE_TRUE);
  return err_code;
}


Populate::ErrorCode VerilogPopulate::fetchConcatRepeatCount(veNode ve_expr,
                                                            const SourceLocator& loc, 
                                                            UInt64 * repeat_count)
{
  ErrorCode err_code = eSuccess;

  (*repeat_count) = 1;
  if (veNodeGetObjType(ve_expr) == VE_MULTIPLECONCAT) {
    veNode ve_repeat_count = veMultiConcatGetCommonExpr(ve_expr);

    NUConst * repeat_const = NULL;
    ErrorCode temp_err_code = constant(ve_repeat_count, &repeat_const);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    // FIXME: I'm not sure what to do here for values > 64 bits?? 
    repeat_const->getULL(repeat_count);

    // Guard against negative numbers in concatenations
    if (veExprIsSigned(ve_repeat_count)) {
      SInt32 signedRepeatCount = *repeat_count;
      if (signedRepeatCount < 0) {
        mMsgContext->CheetahNeedPos(&loc, "concatenation repeat count", signedRepeatCount);
        //  Commented out return statement looks strange !
        // Shouldn't it be in?
        //return eFailNoCreate;
        *repeat_count = 0;
      }
    }


    delete repeat_const;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::expr(veNode ve_expr,
                                          LFContext *context,
                                          NUModule* module,
                                          int lWidth,
                                          NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(ve_expr);

  /* Code to use when trying to figure out how Interra sizes stuff:
  UtIO::cout() << "veExprGetBitWidth = " << veExprGetBitWidth(ve_expr) << UtIO::endl;
  UtIO::cout() << "veExprEvaluateWidth = " << veExprEvaluateWidth(ve_expr) << UtIO::endl;
  veNodeBrowse(ve_expr);
  */

  // First, see if this is a constant expression.
  switch (veExprGetValueType(ve_expr)) {
  case LOCALLY_STATIC:
  case GLOBALLY_STATIC:
    return constant(ve_expr, (NUConst**)the_expr);
    break;

  default:
    break;
  }

  // Not constant, so do the normal stuff.
  switch (veNodeGetObjType(ve_expr)) {
  case VE_NAMEDOBJECTUSE:
  case VE_SCOPEVARIABLE:
    return identRvalue(ve_expr, context, module, the_expr);
    break;

  case VE_UNARY:
    return unaryExpr(ve_expr, context, module, lWidth, (NUUnaryOp**)the_expr);
    break;

  case VE_BINARY:
    return binaryExpr(ve_expr, context, module, lWidth, the_expr);
    break;

  case VE_CONDOP:
    return condExpr(ve_expr, context, module, lWidth, (NUTernaryOp**)the_expr);

  case VE_BITSELECT:
  case VE_ARRAYBITSELECT:
    return bitselRvalue(ve_expr, context, module, the_expr);
    break;

  case VE_PARTSELECT:
  case VE_ARRAYPARTSELECT:
    return partselRvalue(ve_expr, context, module, the_expr);
    break;

  case VE_CONCAT:
  case VE_MULTIPLECONCAT:
    return concatExpr(ve_expr, context, module, the_expr);
    break;

  case VE_CONST:
  case VE_BASEDNUMBER:
    // Should have been caught when we determined if this was a constant
    // or not earlier.
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported constant"), &err_code);
    return err_code;
    break;

  case VE_SYS_FUNC_CALL:
    return sysFunctionCall(ve_expr, context, module, lWidth, the_expr);

  case VE_FUNCTION_CALL:
    return functionCall(ve_expr, context, the_expr);
    break;

  default:
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported expression"), &err_code);
    return err_code;
    break;
  } // switch

  return eFailPopulate;
}

NUExpr* VerilogPopulate::limitSelectIndexExpr(NUMemoryNet* mem, SourceLocator& loc,
                                              NUExpr* selExpr)
{
  bool was_truncated = false;
  selExpr = selExpr->limitMemselIndexExpr(&was_truncated, mMsgContext, loc);

  if ( was_truncated and mem->isDeclaredWithNegativeIndex() )
  {
    mMsgContext->NegativeIndicesWithTruncation(mem);
  }

  return selExpr;
}

Populate::ErrorCode
VerilogPopulate::getBitOrArrayBitSelExpr(veNode ve_expr, LFContext* context,
                                         NUModule* module, int lWidth,
                                         SourceLocator& loc, NUMemoryNet* mem,
                                         bool limitIndex, NUExpr** bitSelExpr)
{
  ErrorCode errorCode = expr(ve_expr, context, module, lWidth, bitSelExpr);
  if (!errorCodeSaysReturnNowWhenFailPopulate(errorCode, NULL))
  {
    (*bitSelExpr)->resize((*bitSelExpr)->determineBitSize()); // self-determined

    if (limitIndex)
      *bitSelExpr = limitSelectIndexExpr(mem, loc, *bitSelExpr);
  }

  return errorCode;
}


Populate::ErrorCode VerilogPopulate::casexMask(veNode ve_expr,
                                               LFContext *context,
                                               const SourceLocator& loc,
                                               bool isCaseX,
                                               size_t maxExprSize,
                                               NUExpr **the_expr)
{
  (*the_expr) = NULL;

  // Recursively process the Cheetah expression for the case condition
  // (ve_expr). The mask is populated as masking conditions are
  // exposed.
  DynBitVector mask(maxExprSize, 0ULL);
  ErrorCode err_code = casexMaskRecurse(ve_expr, context, loc, isCaseX, 0, maxExprSize-1, &mask);

  // Determine if a mask is needed by analyzing the generated
  // constant. The mask contains a '1' for any bit in the care space
  // and a '0' for any bit in the don't care. If the mask contains a
  // single '0', we need to generate a masking constant.

  // We take the mask's complement and test for the presence of '1'.
  DynBitVector flip_mask(mask);
  flip_mask.flip();
  bool maskNeeded = (flip_mask.any()); 
  if (maskNeeded) {

    // Create the needed constant

    UInt32 bit_width = UInt32(veExprEvaluateWidth(ve_expr));
    if (not (bit_width == maxExprSize)) {
      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad case size"), &err_code);
      return err_code;
    }

    (*the_expr) = NUConst::create(false, mask, bit_width, loc);
    (*the_expr)->resize(bit_width);
  }

  return err_code;
}




Populate::ErrorCode VerilogPopulate::casexMaskRecurse(veNode ve_expr,
                                                      LFContext *context,
                                                      const SourceLocator& loc,
                                                      bool isCaseX,
                                                      size_t min_bit,
                                                      size_t max_bit,
                                                      DynBitVector * mask)
{
  ErrorCode err_code = eSuccess;

  // TBD - We don't handle anything but pure static expressions. This
  // means we need to look into concatenations, expressions of partial
  // constants and other such things.
  //
  // TBD this only handles constants up to 64 bits.
  veExprValueType ve_value_type = veExprGetValueType(ve_expr);
  if ((ve_value_type == LOCALLY_STATIC) or (ve_value_type == GLOBALLY_STATIC)) {
    size_t num_bits = (max_bit-min_bit+1);

    veBool hasXZ = VE_FALSE;

    // Create binary string representation for this constant. This
    // will contain characters in the set: [01xz?]
    veString ve_bin_val = veExprEvaluateValueInString(ve_expr, &hasXZ, VE_BIN);
    UtString valStr(ve_bin_val);
    size_t strSize = valStr.size();
     
    if (strSize < num_bits) {
      // If the size of this constant is less than the number of bits
      // needed, pad with '0'.
      valStr.insert(0, num_bits - strSize, '0');
      strSize = num_bits;
    }

    if (strSize != num_bits) {
      veFreeString(ve_bin_val);

      POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Bad case size"), &err_code);
      return err_code;
    }

    // Create a mask with 0's for the x, z, and ?'s and a 1 for 0's
    // and 1's.
    
    size_t cur_bit = max_bit;
    for (UtString::const_iterator ptr = valStr.begin(); 
	 ptr != valStr.end(); 
         ++ptr,--cur_bit)
    {
      const char cur_char = tolower(*ptr);
      if ( ((cur_char=='x') and isCaseX) or
	   (cur_char=='?') or
	   (cur_char=='z') ) {
	// This is a don't care bit, set the mask to clear this bit so
	// that it matches to 0 (which is what x's and z's are set to)

	// Do not update the mask vector.
      } else {
	// We need this bit to be compared; mask with '1'.
	mask->set(cur_bit);
      }
    }

    veFreeString(ve_bin_val);
  } else {
    // Not locally or globally static. If we have a concat, recurse
    // and take care of each sub-element. If this is not a concat, we
    // have a non-constant expression and therefore need to update our
    // entire mask.

    veObjType type = veNodeGetObjType(ve_expr);
    if ((type==VE_CONCAT) or (type==VE_MULTIPLECONCAT)) {
      UInt64 repeat_count = 1;
      ErrorCode temp_err_code = fetchConcatRepeatCount(ve_expr, loc, &repeat_count);
      if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      size_t cur_bit = max_bit;
      while(repeat_count > 0) {
	--repeat_count;
	CheetahList ve_expr_iter(veConcatGetConcatList(ve_expr));
	if (ve_expr_iter) {
	  while (veNode ve_sub_expr = VeListGetNextNode(ve_expr_iter)) {
	    UInt32 bit_width = UInt32(veExprEvaluateWidth(ve_sub_expr));
	    ErrorCode temp_err_code = casexMaskRecurse(ve_sub_expr, context, loc, isCaseX, cur_bit-bit_width+1, cur_bit, mask);
            if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
            {
              return temp_err_code;
            }
	    cur_bit -= bit_width;
	  }
	}

      }

    } else {
      // non-concat. update all bite in our range.
      for (size_t cur_bit = min_bit; cur_bit <= max_bit; ++cur_bit) {
	mask->set(cur_bit);
      }
    }

  }
  
  return err_code;
}



Populate::ErrorCode VerilogPopulate::functionCall(veNode ve_function_call,
                                                  LFContext *context,
                                                  NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;
  ErrorCode temp_err_code = eFatal;

  SourceLocator loc = locator(ve_function_call);

  veNode ve_func_named_obj = veFuncCallGetFunction(ve_function_call);
  veNode ve_function = veNamedObjectUseGetParent(ve_func_named_obj);
  UtString functionName_buf;
  mTicProtectedNameMgr->getVisibleName(ve_function, &functionName_buf);

  if (veNodeIsA(ve_func_named_obj, VE_SCOPEVARIABLE)) {
    // this is a cross-hierarchy function call, we handle this type in two
    // stages, here we create the task enable that will be used in place of
    // the function call and we save information about this new task so that
    // later (at the end of population when we can resolve the hierarchical
    // ref) we connect up the ports

    if (not mAllowHierRefs) {
      POPULATE_FAILURE(mMsgContext->OutOfScopeFunction(&loc, functionName_buf.c_str()), &err_code);
      return err_code;
    }

    // first make a task enable that we will process in two stages.
    AtomArray path;
    StringAtom *function_name = 0;
    temp_err_code = taskEnableHierRefResolve(ve_func_named_obj, context, false, &path, &function_name);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    NUTaskEnableHierRef *task_enable = new NUTaskEnableHierRef(path, function_name, context->getDeclarationScope(),context->getNetRefFactory(), false, loc);


    // create the temp variable for holding output value of task (and insert its declaration in the
    // context->getBlockScope()->createTemp{Bit,Vector}Net)
    NUNet * actual_net = 0;

    // Define a net that will hold the output value of the function, to do
    // this we need the width.  Since we do not know which function this
    // hierarchical reference will resolve to, we just use the first one we
    // can find.  Currently there is a limitation that all references must
    // resolve to the same function, so if that limitation is satisified
    // then the first we find is good enough.
    temp_err_code = addOutputVarForFunctionAsTask( ve_function, context, loc, &actual_net);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
      return temp_err_code;
    }

    // set the_expr to this new net, since that is where the result will be
    *the_expr = new NUIdentRvalue(actual_net, loc);
    (*the_expr)->setSignedResult (veExprIsSigned (ve_function_call) == VE_TRUE);

    // rjc-todo here we need to walk the argument list to process all the
    // actual arguments (saving them for use later once we figure out the
    // resolution for this function).

    // Save information needed to create the arg connections after we can resolve this function call.
    temp_err_code = addTaskHierRefToResolve(NULL, ve_function_call, task_enable, context->getDeclarationScope(), context->getBlockScope(), actual_net);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    // now put the task enable into the design.
    if ( context->isStmtStackEmpty() ){
      // we must be in a context where statements cannot be created,
      // so just save this task enable now and we will create an always block
      // for it once we get out to the module level.
      context->addExtraTaskEnable(task_enable);
    } else{
      context->addStmt(task_enable);
    }

    return err_code;
  }
  // below this point we handle non-hierarchical reference function calls

  NUTask     *the_task_for_function = NULL;
  NUModule *module = context->getModule();

        
  temp_err_code = lookupTask(module, ve_function, &the_task_for_function);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  if (the_task_for_function == 0) {
    // then define the function here  test/langcov/TaskFunction/tf7_f.v
    temp_err_code = tf(ve_function, functionName_buf.c_str(), context, &the_task_for_function);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  // first make up a task_enable statement for this function, it will
  // be put into the stmt list later (after returning from recursion
  // over args).  
  NUTaskEnable *task_enable = new NUTaskEnable(the_task_for_function,
                                               context->getModule(),
                                               context->getNetRefFactory(), false, loc);

  // make a net to hold the output of the function, add the net to the correct scope
  NUNet * actual_net = 0;
  temp_err_code = addOutputVarForFunctionAsTask( ve_function, context, loc, &actual_net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }

  // set the_expr to this new net, since that is where the result will be
  *the_expr = new NUIdentRvalue(actual_net, loc);
  (*the_expr)->setSignedResult (veExprIsSigned (ve_function_call) == VE_TRUE);

  // now handle the port connections including an extra output for this new task (the original function output)
  NUTFArgConnectionVector arg_conns;
  temp_err_code = tfArgConnections(ve_function_call, the_task_for_function, context, &arg_conns, actual_net);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }

  // finished processing the argument list, now we can insert the task enable statement
  if ( task_enable ) {
    task_enable->setArgConnections(arg_conns);

    if ( context->isStmtStackEmpty() ){
      // we must be in a context where statements cannot be created,
      // so just save this task enable now and we will create an always block
      // for it once we get out to the module level.
      context->addExtraTaskEnable(task_enable);
    } else{
      context->addStmt(task_enable);
    }
    
  }

  return err_code;
}
//! create an internal net that will hold the result for \a ve_function when it is implemented as a task,  
/*!
 * this net is added to the proper scope, and returned in \a actual_net
 */
Populate::ErrorCode VerilogPopulate::addOutputVarForFunctionAsTask( veNode ve_function,
                                                                   LFContext *context,
                                                                   const SourceLocator &loc,
                                                                   NUNet **actual_net)
{
  ErrorCode err_code = eSuccess;
  NUModule *module = context->getModule();
  // A workaround (a nasty one) for bug4288, is to declare all these temp
  // variables in the module scope
# define BUG4288 1
# if BUG4288
  NUScope* block_scope = module;
#else
  NUScope *block_scope = context->getBlockScope();
#endif
  bool is_signed = veFunctionGetIsSigned(ve_function) == VE_TRUE;
  veBuiltinObjectType ve_return_type = veFunctionGetReturnType( ve_function );
  bool is_real = ((REAL_USE == ve_return_type) || (REALTIME_USE == ve_return_type));
  UtString function_name;
  mTicProtectedNameMgr->getVisibleName(ve_function, &function_name);

  UInt32 bitsize = 0;
  ErrorCode temp_err_code = functionResultWidth(ve_function, &bitsize);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }

  StringAtom *tempname = module->gensym("$output", function_name.c_str(), NULL, false);

  // We are creating a temporary, so we place it in the block
  // scope. This causes it to persist as a function-local
  // temporary in codegen.
  NU_ASSERT((bitsize != 0), module);
  
  if (bitsize == 1 ){
    *actual_net = block_scope->createTempBitNet(tempname, is_signed,
                                                loc);
  } else {
    NetFlags net_flags = NetFlags(eTempNet);
    // no need to add eSigned to flags if return value is only signed,
    // createTempVectorNet has an argument for that.
    if ( is_real ) net_flags = NetRedeclare(net_flags, eDMRealNet|eSigned); // all real values are signed.

    ConstantRange temprange(bitsize-1,0);

    *actual_net = block_scope->createTempVectorNet(tempname, temprange, is_signed, loc, net_flags, eNoneVectorNet);
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::sysFunctionCall(veNode ve_function_call,
                                                     LFContext *context,
                                                     NUModule* module,
                                                     int lWidth,
                                                     NUExpr **the_expr)
{
  ErrorCode err_code = eSuccess;
  *the_expr = 0;

  SourceLocator loc = locator(ve_function_call);
  CheetahStr sysFunc(veSysFuncCallGetName(ve_function_call));
  bool is_converter_function = false;
  NUOp::OpT op = NUSysFunctionCall::lookupOp(sysFunc, &is_converter_function);
  NUSysRandom::Function randFunc = NUSysRandom::eInvalidRandom;
  bool isRandomFunction = NUSysRandom::lookup(sysFunc, &randFunc);

  StringAtom* orig_module_name = module->getOriginalName();
  IODBNucleus* iodb = context->getIODB();

  // first get default, then correct for any module specific directives
  bool doingOutputSysTasks = mArg->getBoolValue ("-enableOutputSysTasks");
  if ( doingOutputSysTasks ){
    doingOutputSysTasks = (not iodb->isDisableOutputSysTasks(orig_module_name) );
  } else {
    doingOutputSysTasks = (iodb->isEnableOutputSysTasks(orig_module_name) );
  }

  bool isFopenFunction = (op == NUOp::eNaFopen );

  bool isSignedFunction = (strcmp (sysFunc, "$signed") == 0);
  bool isUnsignedFunction = (strcmp (sysFunc, "$unsigned") == 0);

  if ( isFopenFunction and not doingOutputSysTasks )
  {
    op = NUOp::eInvalid;   // although this is valid syntax, we are
                           // not supporting fopen for this run/module
  }


  NUExprVector expr_vector;
  CheetahList exprList(veSysFuncCallGetExprList(ve_function_call));
  ErrorCode temp_err_code = exprVec(exprList, context, module,
                                    lWidth, &expr_vector);

  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;  // failed during attempt to create expr_vector
  }

  // If this is a random function, then we model it as a system task
  // because it modifies its args, and we don't deal well with
  // expressions that mutate their args.
  if ( isRandomFunction )
  {
    // add a system task to the current statement list.
    // (What to do with a continuous assign?)
    StringAtom* sym = module->gensym(sysFunc);
    ConstantRange r(31, 0);
    NUNet* net = module->createTempVectorNet(sym, r, false, loc);
    NULvalue* lvalue = new NUIdentLvalue(net, loc);
    NUSysRandom* sysRand = new NUSysRandom(randFunc, sym, lvalue, NULL,
                                           expr_vector, module,
                                           mNetRefFactory, false, loc);
    UtString errmsg;
    if (sysRand->isValid(&errmsg))
    {
      *the_expr = new NUIdentRvalue(net, loc);
      (*the_expr)->setSignedResult (veExprIsSigned (ve_function_call) == VE_TRUE);

      if ( context->isStmtStackEmpty() ){
        // we must be in a context where statements cannot be created,
        // so just save this sys task enable now and we will create an always block
        // for it once we get out to the module level.
        context->addExtraTaskEnable(sysRand);
      } else{
        context->addStmt(sysRand);
      }
    }
    else
    {
      POPULATE_FAILURE(mMsgContext->InvalidSysFuncArg(&loc, sysFunc, errmsg.c_str()), &err_code);
      delete sysRand;
     }
  }
  else if ( isFopenFunction and doingOutputSysTasks )
  {
    // fopen is a system function but we model it as a system task
    // because it has side effects when called, (such as opening a
    // file and creating a new file descriptor)
    StringAtom* sym = module->gensym(sysFunc);
    ConstantRange r(31, 0);   // file descriptors are always 32 bits
    NUNet* net = module->createTempVectorNet(sym, r, false, loc);
    NUFOpenSysTask* sysFOpen = new NUFOpenSysTask(sym, net, 
                                                  NULL/*return_status*/,
                                                  expr_vector,
                                                  module, mNetRefFactory, 
                                                  false,
                                                  true /*isVerilogTask*/,
                                                  false /*useInFileSystem*/,
                                                  loc);
    UtString errmsg;
    if (sysFOpen->isValid(&errmsg))
    {
      *the_expr = new NUIdentRvalue(net, loc);
      (*the_expr)->setSignedResult (veExprIsSigned (ve_function_call) == VE_TRUE);
      context->addStmt(sysFOpen);
    }
    else
    {
      POPULATE_FAILURE(mMsgContext->InvalidSysFuncArg(&loc, sysFunc, errmsg.c_str()), &err_code);
      delete sysFOpen;
    }
  }
  else if (isSignedFunction)
  {
    NUExpr *e = expr_vector[0];
    if (veExprIsSigned (ve_function_call)) {
      // If Cheetah believes that the expression is being treated as signed, then
      // the $SIGNED should remain in the tree and influence how we compute the
      // expression.  However if we did:
      //        wire [3:0] x;
      //        5'b101 + $SIGNED(x)
      // Verilog rules say that expression is UNSIGNED and 'x' should be ZERO extended
      // from 4 to 5 bits before doing the addition.
      //
      // If the expression isn't signed, then the subexpression is treated normally
      //
      e = new NUBinaryOp (NUOp::eBiVhExt, e,
                          NUConst::create (false, e->determineBitSize (),
                                           32, loc),
                          loc);
      e->setSignedResult (true);
    }
    *the_expr = e;
  }
  else if (isUnsignedFunction)
  {
    // We assume zero-extension unless otherwise encouraged.
    NUExpr *e = expr_vector[0];
    // Cheetah isn't smart enough to remove unneeded $signed() in a 
    // $unsigned($signed(x)) expression - help it out...
    if (e->isSignedResult ()) {
      if (NUBinaryOp* b = dynamic_cast<NUBinaryOp*>(e)) {
        if (b->getOp () == NUOp::eBiVhExt) {
          CopyContext cc (0,0);
          e = b->getArg (0)->copy (cc);
          delete b;
        }
      }
    }
    e->setSignedResult (false);
    *the_expr = e;
  }
  else if ( is_converter_function )
  {
    // one of $rtoi $itor $realtobits $bitstoreal
    switch ( op ) {
    case NUOp::eUnItoR:
    case NUOp::eUnRtoI:
    case NUOp::eUnRealtoBits:
    case NUOp::eUnBitstoReal:
    {
      NUExpr *expr1 = expr_vector[0];
      if ( op == NUOp::eUnBitstoReal ){
        expr1->resize(expr1->determineBitSize ());
        // require that argument be 64 bits
        UInt32 bSize = expr1->getBitSize ();
        if ( bSize > 64 ){
          // it was too large to start with
          ConstantRange r(63, 0);
          expr1 = new NUVarselRvalue(expr1, r, loc);
        } else if ( bSize < 64 ) {
          expr1 = expr1->makeSizeExplicit(64);
        }
      }
      if ( ( ( op == NUOp::eUnRtoI ) || ( op ==  NUOp::eUnRealtoBits) ) && not expr1->isReal() ){
        POPULATE_FAILURE(mMsgContext->InvalidSysFuncArg(&loc, sysFunc, "this function requires a real value argument."), &err_code);
        delete expr1;
        expr1 = NULL;
        break;
      }

      expr1 = new NUUnaryOp(op, expr1, loc);
      if ( op == NUOp::eUnRtoI ){
        // the LRM says $rtoi is defined as returning a value the size of 'integer'
        // aldec does this, nc does not, 
        // in all cases the conversion is done in 64 bits,
        // then aldec and carbon truncates to 32 bits.
        ConstantRange r(31, 0);
        expr1 = new NUVarselRvalue(expr1, r, loc);
      }
      expr1->setSignedResult( (op == NUOp::eUnItoR) or (op == NUOp::eUnRtoI) or (op == NUOp::eUnBitstoReal));
      *the_expr = expr1;
      break;
    }
    default:{
      // for now we just fall through and below we will report this
      // not supported and create a 0 
      break;
    }
    }
  }
  else if ( op != NUOp::eInvalid )
  {
    NUSysFunctionCall* sysCall = new NUSysFunctionCall(op, module->getTimeUnit(),
                                                       module->getTimePrecision(),
                                                       expr_vector, loc);
    UtString errmsg;
    if (sysCall->isValid(&errmsg))
    {
      *the_expr = sysCall;
      (*the_expr)->setSignedResult (veExprIsSigned (ve_function_call) == VE_TRUE);
      (*the_expr)->resize((*the_expr)->determineBitSize ());
    }
    else
    {
      POPULATE_FAILURE(mMsgContext->InvalidSysFuncArg(&loc, sysFunc, errmsg.c_str()), &err_code);
      delete sysCall;
    }
  }

  if ( ( err_code == eSuccess) && (*the_expr == NULL )) {
    // if we get here then this system function was not supported
    // remove expr_vector if anything was created
    for ( int index = expr_vector.size()-1; index >= 0; index--) {
      NUExpr *expr1 = expr_vector[index];
      delete expr1;
    }
    expr_vector.clear();
    // now create a dummy constant 0 to use in the place of the fuction
    UInt32 bitWidth = (lWidth > 0) ? lWidth : 32;  // since this is a dummy, 32 bits are fine for now
    *the_expr = NUConst::create (veExprIsSigned (ve_function_call) == VE_TRUE, 0u, bitWidth, loc);

    if ( isFopenFunction ) {
      mMsgContext->DisabledSysFunc(&loc, sysFunc, "-enableOutputSysTasks");
    } else {
      mMsgContext->UnsupportedSysFunc(&loc, sysFunc);
    }
    // err_code is still success because we created the dummy value of zero
  }

  return err_code;
} // Populate::ErrorCode VerilogPopulate::sysFunctionCall


Populate::ErrorCode VerilogPopulate::tfArgConnection(veNode ve_arg,
                                                     NUNet *formal_net,
                                                     NUTF *tf,
                                                     LFContext *context,
                                                     NUTFArgConnection **the_conn)
{
  ErrorCode err_code = eSuccess;
  *the_conn = 0;

  SourceLocator loc = locator(ve_arg);

  if (formal_net->isInput()) {
    NUExpr *actual_expr = 0;
    ErrorCode temp_err_code = expr(ve_arg, context, context->getModule(), 0, &actual_expr);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    actual_expr = actual_expr->makeSizeExplicit(formal_net->getBitSize());

    *the_conn = new NUTFArgConnectionInput(actual_expr, formal_net, tf, loc);

  } else if (formal_net->isOutput()) {
    NULvalue *actual_lvalue = 0;
    ErrorCode temp_err_code = lvalue(ve_arg, context, context->getModule(), &actual_lvalue);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    actual_lvalue->resize ();
    *the_conn = new NUTFArgConnectionOutput(actual_lvalue, formal_net, tf, loc);

  } else if (formal_net->isBid()) {
    NULvalue *actual_lvalue = 0;
    ErrorCode temp_err_code = lvalue(ve_arg, context, context->getModule(), &actual_lvalue);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    actual_lvalue->resize ();
    *the_conn = new NUTFArgConnectionBid(actual_lvalue, formal_net, tf, loc);

  } else {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Unsupported task/function argument"), &err_code);
    return err_code;
  }

  return err_code;
}

