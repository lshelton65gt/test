// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/Elaborate.h"
#include "LFContext.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUElabHierHelper.h"

#include "exprsynth/Expr.h"
#include "exprsynth/SymTabExpr.h"

#include "compiler_driver/CarbonDBWrite.h"

//! Same as ElaborationTransformWalker, but for back pointers
/*!
  Note that we are transforming from one factory to a different one,
  so all expressions must be transformed.
*/
class ElaborationBackPointerWalker : public CarbonTransformWalker
{
public:
  ElaborationBackPointerWalker(CbuildSymTabBOM * bomManager,
                               STSymbolTable * symtab,
                               STBranchNode * hier) :
    mBOMManager(bomManager),
    mESFactory(bomManager->getExprFactory()),
    mSymtab(symtab),
    mHier(hier)
  {}

  //! Destructor
  ~ElaborationBackPointerWalker() {}
  
  
  virtual CarbonExpr* transformConst(CarbonConst* expr) { 
    DynBitVector val;
    expr->getValue(&val);
    return mESFactory->createConst(val,
                                   expr->getSignInfo(),
                                   expr->getBitSize());
  }
  
  virtual CarbonExpr* transformConstXZ(CarbonConst* expr) { 
    DynBitVector val, drv;
    expr->getValue(&val, &drv);
    return mESFactory->createConstXZ(val, drv,
                                     expr->getSignInfo(),
                                     expr->getBitSize());
  }
  
  //! Back pointers can be partsels
  CarbonExpr* transformPartsel(CarbonExpr* expr, CarbonPartsel* partselOrig) {
    return mESFactory->createPartsel(expr,
                                     partselOrig->getRange(),
                                     partselOrig->getBitSize(),
                                     partselOrig->isSigned());
  }
  
  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformUnaryOp(CarbonExpr*, CarbonUnaryOp*) {
    INFO_ASSERT(0, "transformUnaryOp not yet implemented");
    return NULL;
  }

  //! Back pointers can be bitsels
  CarbonExpr* transformBinaryOp(CarbonExpr* expr1, CarbonExpr* expr2,
                                CarbonBinaryOp* binaryOrig) {
    return mESFactory->createBinaryOp(binaryOrig->getType(),
                                      expr1, expr2,
                                      binaryOrig->getBitSize(),
                                      binaryOrig->isSigned(), false);
  }
  
  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformTernaryOp(CarbonExpr* , CarbonExpr* , 
                                 CarbonExpr* , CarbonTernaryOp*) {
    INFO_ASSERT(0, "transformTernaryOp not yet implemented");
    return NULL;
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformEdge(CarbonExpr*, CarbonEdge*) {
    INFO_ASSERT(0, "transformEdge not yet implemented");
    return NULL;
  }

  //! Create a new concat based on transformed components.
  CarbonExpr* transformConcatOp(CarbonExprVector * exprVec, 
                                UInt32 repeatCount,
                                CarbonConcatOp * concatOrig) {
    return mESFactory->createConcatOp(exprVec, repeatCount,
                                      concatOrig->getBitSize(),
                                      concatOrig->isSigned());
  }

  //! Rewrite an identifier in terms of elaborated symbol table nodes.
  CarbonExpr* transformIdent(CarbonIdent* ident) {
    SymTabIdent * local_ident = ident->castSymTabIdent();
    CE_ASSERT(local_ident,ident);
    const STAliasedLeafNode * local_leaf = local_ident->getNode();
    CE_ASSERT(local_leaf,local_ident);
    
    // find the elaborated entry in the symbol table
    STAliasedLeafNode * elab_leaf = NUElabHierHelper::findRealLeaf(local_leaf, 
                                                                   mHier,
                                                                   mSymtab);

    // if elab_leaf is NULL, the net wasn't elaborated. So, replace
    // this ident with a constant x.
    
    CarbonExpr* retExpr = NULL;
    UInt32 bitSize = local_ident->getBitSize();
    
    if (elab_leaf)
    {
      // create an identifier based on this name.
      bool added = false;
      CarbonIdent * my_elab_ident   = new SymTabIdent(elab_leaf,
                                                      bitSize);
      ConstantRange declaredRange;
      if (local_ident->getDeclaredRange(&declaredRange))
        my_elab_ident->putDeclaredRange(&declaredRange);
      
      CarbonIdent * elab_ident = mESFactory->createIdent(my_elab_ident, added);
      if (not added) {
        delete my_elab_ident;
      }
      retExpr = elab_ident;
    }
    else {
      retExpr = mESFactory->createConstX(bitSize, ident->isSigned());
    }
    return retExpr;
  }  

private:
  CbuildSymTabBOM * mBOMManager;
  ESFactory * mESFactory;
  STSymbolTable * mSymtab;
  STBranchNode * mHier;
};
  

//! A class to translate a CarbonExpr from unelaborated to elaborated identifiers.
/*!
 * Note that we are transforming from one factory to a different one,
 * so all expressions must be transformed.
 *
 * Unelaborated reconstruction data is stored using unelaborated
 *s symbol table nodes. These nodes will have no hierarchy, unless
 * flattening is involved. If a design is partially flattened, the
 * unelaborated reconstruction data may contain identifiers with
 * relative hierarchy.
 *
 * The NUAliasBOM will contain a map between an identifier and an
 * expression. The expression defines the reconstruction data for the
 * identifier.
 *
 * An example of port-splitting reconstruction data without relative
 * hierarchy:
 *
 \verbatim
   'io' => { 'io_2_2', 'io_1_1', 'io_0_0' }
 \endverbatim
 * 
 * This reconstruction data will be elaborated as (assuming one level
 * of hierarchy):
 \verbatim
   'top.io' => { 'top.io_2_2', 'top.io_1_1', 'top.io_0_0' }
 \endverbatim
 *
 * \sa FlattenTransformWalker
 */
class ElaborationTransformWalker : public CarbonTransformWalker
{
public:
  //! Constructor
  ElaborationTransformWalker(CbuildSymTabBOM * bomManager,
                             STSymbolTable * symtab,
                             STBranchNode * hier) :
    mBOMManager(bomManager),
    mESFactory(bomManager->getExprFactory()),
    mSymtab(symtab),
    mHier(hier)
  {}

  //! Destructor
  ~ElaborationTransformWalker() {}

  virtual CarbonExpr* transformConst(CarbonConst* expr) { 
    DynBitVector val;
    expr->getValue(&val);
    return mESFactory->createConst(val,
                                   expr->getSignInfo(),
                                   expr->getBitSize());
  }
  
  virtual CarbonExpr* transformConstXZ(CarbonConst* expr) { 
    DynBitVector val, drv;
    expr->getValue(&val, &drv);
    return mESFactory->createConstXZ(val, drv,
                                     expr->getSignInfo(),
                                     expr->getBitSize());
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformPartsel(CarbonExpr* expr, CarbonPartsel* orig) {
    return mESFactory->createPartsel(expr, orig->getRange(), orig->getBitSize(), orig->isSigned());
  }
  
  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformUnaryOp(CarbonExpr*, CarbonUnaryOp*) {
    INFO_ASSERT(0, "transformUnaryOp not yet implemented");
    return NULL;
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformBinaryOp(CarbonExpr* one, CarbonExpr* two,
                                CarbonBinaryOp* orig) {
    return mESFactory->createBinaryOp(orig->getType(), one, two, 
                                      orig->getBitSize(), orig->isSigned(), false);
  }
  
  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformTernaryOp(CarbonExpr* , CarbonExpr* , 
                                 CarbonExpr* , CarbonTernaryOp*) {
    INFO_ASSERT(0, "transformTernaryOp not yet implemented");
    return NULL;
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformEdge(CarbonExpr*, CarbonEdge*) {
    INFO_ASSERT(0, "transformEdge not yet implemented");
    return NULL;
  }

  //! Create a new concat based on transformed components.
  CarbonExpr* transformConcatOp(CarbonExprVector * exprVec, 
                                UInt32 repeatCount,
                                CarbonConcatOp * concatOrig) {
    return mESFactory->createConcatOp(exprVec, repeatCount,
                                      concatOrig->getBitSize(),
                                      concatOrig->isSigned());
  }

  //! Rewrite an identifier in terms of elaborated symbol table nodes.
  CarbonExpr* transformIdent(CarbonIdent* ident) {
    SymTabIdent * local_ident = ident->castSymTabIdent();
    CE_ASSERT(local_ident,ident);
    const STAliasedLeafNode * local_leaf = local_ident->getNode();
    CE_ASSERT(local_leaf,local_ident);
    SymTabIdentBP * local_ident_bp = local_ident->castSymTabIdentBP();

    // find the elaborated entry in the symbol table
    STAliasedLeafNode * elab_leaf = NUElabHierHelper::findRealLeaf(local_leaf, 
                                                                   mHier,
                                                                   mSymtab);

    CarbonExpr* elabBP = NULL;

    if (local_ident_bp)
    {
      ElaborationBackPointerWalker bpWalk(mBOMManager, mSymtab, mHier);
      bpWalk.visitExpr(local_ident_bp->getBackPointer());
      elabBP = bpWalk.getResult();
    }
    
    // if elab_leaf is NULL, the net wasn't elaborated. So, replace
    // this ident with a constant x.
    
    CarbonExpr* retExpr = NULL;
    UInt32 bitSize = local_ident->getBitSize();
    
    if (elab_leaf)
    {
      // create an identifier based on this name.
      bool added = false;
      CarbonIdent * my_elab_ident = NULL;
      if (elabBP)
      {
        my_elab_ident   = new SymTabIdentBP(elab_leaf,
                                            bitSize,
                                            elabBP);
      }
      else
      {
        my_elab_ident   = new SymTabIdent(elab_leaf,
                                          bitSize);
        // This only needs to be done for the backpointer identifiers
        // which are SymTabIdents not SymTabIdentBPs
        ConstantRange declaredRange;
        if (local_ident->getDeclaredRange(&declaredRange))
          my_elab_ident->putDeclaredRange(&declaredRange);
      }
      
      CarbonIdent * elab_ident = mESFactory->createIdent(my_elab_ident, added);
      if (not added) {
        delete my_elab_ident;
      }
      retExpr = elab_ident;
      CbuildSymTabBOM::putIdent(elab_leaf, elab_ident);
    }
    else {
      retExpr = mESFactory->createConstX(bitSize, ident->isSigned());
    }

    return retExpr;
  }  

private:
  CbuildSymTabBOM * mBOMManager;
  ESFactory * mESFactory;
  STSymbolTable * mSymtab;
  STBranchNode * mHier;
};


void Elaborate::elabLeafReconData(STAliasedLeafNode * local_leaf, 
                                  LFContext * context)
{
  NUAliasDataBOM * local_bom = NUAliasBOM::castBOM(local_leaf->getBOMData());
  CarbonIdent * local_ident = local_bom->getIdent();
  // there is no reconstruction data for this identifier.
  if (not local_ident) {
    return;
  }

  CbuildSymTabBOM * elab_bom_manager = dynamic_cast<CbuildSymTabBOM*>(mSymtab->getFieldBOM());
  INFO_ASSERT(elab_bom_manager, "Invalid BOM type");

  ElaborationTransformWalker elabWalker(elab_bom_manager,
                                        mSymtab,
                                        context->getHier());

  // first, elaborate the identifier.
  elabWalker.visitIdent(local_ident);
  CarbonExpr * elab_ident_expr = elabWalker.getResult();
  INFO_ASSERT(elab_ident_expr, "Failed to elaborate identifier");

  SymTabIdent * elab_ident = elab_ident_expr->castSymTabIdent();
  CE_ASSERT(elab_ident, elab_ident_expr);

  // if there is a mapped unelaborated expression, elaborate that expression
  NUAliasBOM * local_bom_manager = context->getDesign()->getAliasBOM();
  CarbonExpr * local_expr = local_bom_manager->getExpr(local_ident);
  
  if (local_expr) {
    elabWalker.visitExpr(local_expr);
    CarbonExpr * elab_expr = elabWalker.getResult();
    INFO_ASSERT(elab_expr, "Failed to elaborate expression");

    elab_bom_manager->mapExpr(elab_ident, elab_expr);
  }
}
