/***************************************************************************************
  Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "PortCoercion.h"
#include "PortSplitting.h"
#include "PortAnalysis.h"

#include "util/CbuildMsgContext.h"
#include "util/SetOps.h"
#include "util/Stats.h"
#include "util/StringAtom.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAssign.h"

#include "localflow/UpdateUD.h"

#include "localflow/Elaborate.h"
#include "localflow/UnelabFlow.h"
#include "localflow/MarkReadWriteNets.h"

#include "reduce/RemoveUnelabFlow.h"
#include "reduce/ReachableAliases.h"

#include "hdl/HdlVerilogPath.h"
#include "symtab/STSymbolTable.h"
#include "compiler_driver/CarbonDBWrite.h"

#include "flow/FLIter.h"


/*!
  \file
  Implementation of port coercion package.
*/


PortCoercion::PortCoercion(AtomicCache* strCache, 
                           IODBNucleus* iodb, 
                           Stats* stats, 
                           ArgProc* args,
                           MsgContext* msgContext, 
                           NUNetRefFactory* netRefFactory,
                           SourceLocatorFactory* sourceLocatorFactory,
                           bool do_port_splitting,
                           bool verbose_port_splitting,
                           bool verbose) :
  mStrCache(strCache),
  mIODB(iodb),
  mStats(stats),
  mArgs(args),
  mMsgContext(msgContext),
  mNetRefFactory(netRefFactory),
  mSourceLocatorFactory(sourceLocatorFactory),
  mDoPortSplitting(do_port_splitting),
  mVerbosePortSplitting(verbose_port_splitting),
  mVerbose(verbose),
  mPhase(0),
  mDesign(NULL),
  mFlowFactory(NULL),
  mFlowElabFactory(NULL),
  mTmpSymTabBOM(NULL),
  mTmpSymbolTable(NULL),
  mNetElabRefFactory(NULL)
{
  mInouts  = new NUNetSet;
  mOutputs = new NUNetSet;
  mInputs  = new NUNetSet;
  mPortAnalysis = new PortAnalysis(mNetRefFactory);
}


PortCoercion::~PortCoercion()
{
  delete mPortAnalysis;
  delete mInouts;
  delete mOutputs;
  delete mInputs;
}


void PortCoercion::design(NUDesign * design, bool phaseStats)
{
  mDesign = design;

  if (phaseStats)
    mStats->pushIntervalTimer();

  lower();
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce Lower");

  // If port splitting is not enabled, skip our first pass, which
  // generates coercion candidates used by splitting.
  mPhase = mDoPortSplitting ? 1 : 2;

  do {
    if (phaseStats)
      mStats->pushIntervalTimer();

    // Create unelaborated and elaborated infrastructure.
    createFramework(phaseStats);
    if (phaseStats)
      mStats->printIntervalStatistics("Coerce Framework");

    // Traverse the design and discover ports with incorrect
    // declaration.
    discoverCoercion(phaseStats);
    if (phaseStats) 
      mStats->printIntervalStatistics("Coerce Discovery");

    if (mPhase==1) {
      // ****************************************
      // PASS 1: Discover coercion situations in preparation for port
      //         splitting.

      PortSplitting splitter(mStrCache,
                             mIODB,
                             mStats,
                             mArgs,
                             mMsgContext,
                             mNetRefFactory,
                             mSourceLocatorFactory,
                             mInouts,
                             mInputs,
                             mOutputs,
                             mVerbosePortSplitting);
      splitter.design(mDesign, phaseStats);

      if (phaseStats)
        mStats->printIntervalStatistics("Port Split");

    } else {
      INFO_ASSERT(mPhase==2, "PortCoercion has forgotten what phase it is");
      // ****************************************
      // PASS 2: Discover coercion situations as a result of port
      //         splitting; if port splitting used the coercion discovered
      //         during our first pass, inout coercion should not produce
      //         any complex bidi situations.

      // Redeclare ports as necessary; update their port connections.
      coerce();
      if (phaseStats)
        mStats->printIntervalStatistics("Coerce Update");
    }

    // Destroy any temporary infrastructure.
    destroyFramework(phaseStats);
    if (phaseStats)
      mStats->printIntervalStatistics("Coerce Cleanup");
    
    // Remove coercion candidates found in this pass.
    cleanup();
    if (phaseStats)
      mStats->printIntervalStatistics("Coerce Purge");

    if (phaseStats)
      mStats->popIntervalTimer();

    if (phaseStats)
      mStats->printIntervalStatistics("Coerce Pass");

  } while ((++mPhase) <= 2);

  if (phaseStats)
    mStats->popIntervalTimer();


}


void PortCoercion::lower()
{
  SplittingLowerCallback callback(mNetRefFactory,
                                  mArgs,
                                  mStrCache,
                                  mMsgContext,
                                  mIODB);
  NUDesignWalker walker(callback,false);
  NUDesignCallback::Status status = walker.design(mDesign);
  INFO_ASSERT(status==NUDesignCallback::eNormal, "Abnormal callback");
}


void PortCoercion::discoverCoercion(bool phaseStats)
{
  NUNetElabRefClosure closure;
  DoubleNetElabRefSet fanin;
  NUNetElabRefSet     written_ports;

  if (phaseStats)
    mStats->pushIntervalTimer();

  // 3. Traverse the design and compute connected sub-graphs and
  //    fanin/fanout relations over the set of port connections.
  traverse(&closure,&fanin,&written_ports);
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce Traverse");

  // 4. Walk the fanin/fanout relations for each connected sub-graph
  //    and determine which arcs are necessary for all writers to
  //    reach all readers.
  process(&closure,&fanin,&written_ports);
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce Process");

  if (phaseStats)
    mStats->popIntervalTimer();
}


void PortCoercion::cleanup()
{
  mInouts->clear();
  mInputs->clear();
  mOutputs->clear();
}


void PortCoercion::createFramework(bool phaseStats)
{
  if (phaseStats)
    mStats->pushIntervalTimer();

  mNetElabRefFactory = new NUNetElabRefFactory(mNetRefFactory);

  mTmpSymTabBOM   = new CbuildSymTabBOM;
  mTmpSymbolTable = new STSymbolTable(mTmpSymTabBOM, mStrCache);
  mTmpSymbolTable->setHdlHier(new HdlVerilogPath);

  mFlowFactory     = new FLNodeFactory();

  UDCallback callback(mNetRefFactory,mMsgContext,mIODB,mArgs);
  NUDesignWalker walker(callback, false);
  walker.design(mDesign);
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce UD");

  // MarkReadWriteNets needs to come after UD, as UD recomputes some of its information.
  MarkReadWriteNets marker(mIODB, mArgs, false);
  marker.design(mDesign);
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce RW");

  UnelabFlow unelaborate(mFlowFactory, 
                         mNetRefFactory, 
                         mMsgContext, 
                         mIODB, 
                         false);
  unelaborate.design(mDesign);
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce UF");

  mFlowElabFactory = new FLNodeElabFactory();
  Elaborate elaborate(mTmpSymbolTable,
                      mStrCache,
                      mSourceLocatorFactory,
                      mFlowFactory,
                      mFlowElabFactory,
                      mNetRefFactory,
                      mMsgContext,
                      mArgs,
                      mIODB);
  // Elaborate only declarations, not the flow graph. Elaborated
  // declarations allow the correct resolution of hierarchical
  // references.
  elaborate.designDecl(mDesign);
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce Elab");

  mReachables = new ReachableAliases();
  mReachables->compute(mTmpSymbolTable);
  if (phaseStats)
    mStats->printIntervalStatistics("Coerce Reach");

  if (phaseStats)
    mStats->popIntervalTimer();
}


void PortCoercion::destroyFramework(bool phaseStats)
{
  // Traverse the design and clean the UD information. We don't need
  // it and it will get re-created by the later local analysis pass.
  ClearUDCallback callback(true); // recurse
  NUDesignWalker walker(callback, false);
  walker.design(mDesign);
  if (phaseStats)
    mStats->printIntervalStatistics("ClearUD");

  // Remove the unelaborated flow
  RemoveUnelabFlow removeUnelab(mFlowFactory);
  removeUnelab.remove();
  if (phaseStats)
    mStats->printIntervalStatistics("Remove UF");

  delete mFlowElabFactory; // unpopulated.
  delete mFlowFactory;

  delete mReachables;

  NUElabBase::deleteObjects(mTmpSymbolTable);
  delete mTmpSymbolTable->getHdlHier();
  delete mTmpSymbolTable;
  delete mTmpSymTabBOM;

  delete mNetElabRefFactory;
}


//! Traverse the design and determine port connectivity
class CoercionTraverseCallback : public NUInstanceCallback
{
public:
  CoercionTraverseCallback(STSymbolTable * symtab,
                           NUNetRefFactory * netRefFactory,
                           NUNetElabRefFactory * netElabRefFactory,
                           NUNetElabRefClosure * closure,
                           DoubleNetElabRefSet * fanin,
                           NUNetElabRefSet * written_ports) :
    NUInstanceCallback(symtab),
    mNetRefFactory(netRefFactory),
    mNetElabRefFactory(netElabRefFactory),
    mClosure(closure),
    mFanin(fanin),
    mWrittenPorts(written_ports)
  {}

  ~CoercionTraverseCallback() {}

  //! Walk through modules once per instantiation.
  /*! 
   * The elaborated walk (encountering a module multiple times) is
   * controlled by NUDesignWalker.
   *
   * For the top module, initialize the hierarchy stack and remember
   * all primary ports as self-connected.
   */
  void handleModule(STBranchNode * branch, NUModule * module) {
    if (module->atTopLevel()) {
      for (NUNetVectorLoop port_loop = module->loopPorts();
           not port_loop.atEnd();
           ++port_loop) {
        NUNet * port_net = (*port_loop);
        if (port_net->is2DAnything()) {
          continue; // skip all 2-D/memory ports for now.
        }
        addTerminal(port_net, branch);
      }
    }
  }

  //! Nothing needs to be done for declaration scopes.
  void handleDeclScope(STBranchNode*, NUNamedDeclarationScope*)
  {}

  //! Walk through module instances.
  /*!
   * Determine the hierarchy as we iterate through the next module.
   * Walk all port connections and remember their connectivity. The
   * port connections aren't walked on their own because we have the
   * necessary contextual information (current and pending hierarchy)
   * while processing the module instance.
   */
  void handleInstance(STBranchNode * branch, NUModuleInstance * instance) {
    STBranchNode * parent = branch->getParent();

    for (NUPortConnectionLoop loop = instance->loopPortConnections();
         not loop.atEnd();
         ++loop) {
      NUPortConnection * connection = (*loop);
      NUNet  * formal = connection->getFormal();
      if (formal->is2DAnything()) {
        continue; // skip all 2-D/memory ports for now.
      }

      // map the formal to its elaborated equivalent.
      NUNetElab * formal_net_elab = formal->lookupElab(branch);
      NUNet * formal_storage_net = formal_net_elab->getStorageNet();
      STBranchNode * formal_storage_hier = formal_net_elab->getStorageHier();

      NUNetRefHdl formal_storage_ref = mNetRefFactory->createNetRef(formal_storage_net);

      switch(connection->getType()) {
      case eNUPortConnectionInput: {
        NUPortConnectionInput * input = dynamic_cast<NUPortConnectionInput*>(connection);
        NUExpr * actual = input->getActual();
        if (input->isSimple()) {
          connectInput(actual, parent, formal_storage_ref, formal_storage_hier);
        } else {
          if (actual) {
            markWrittenAbove(formal_storage_ref, formal_storage_hier);
          }
          addTerminal(formal_storage_net, formal_storage_hier);
        }
        break;
      }
      case eNUPortConnectionOutput: {
        NUPortConnectionOutput * output = dynamic_cast<NUPortConnectionOutput*>(connection);
        NULvalue * actual = output->getActual();
        if (actual) {
          connectOutput(actual,parent, formal_storage_ref, formal_storage_hier);
        }
        addTerminal(formal_storage_net, formal_storage_hier);
        break;
      }
      case eNUPortConnectionBid: {
        NUPortConnectionBid * bid = dynamic_cast<NUPortConnectionBid*>(connection);
        NULvalue * actual = bid->getActual();
        if (actual) {
          connectOutput(actual,parent,formal_storage_ref,formal_storage_hier);
        }
        addTerminal(formal_storage_net, formal_storage_hier);
        break;
      }
      default:
        NU_ASSERT("Unexpected port type"==0,connection);
        break;
      }
    }
  }
private:

  //! Add a terminal so the closure knows about it.
  void addTerminal(NUNet * net, STBranchNode * hier) {

    UInt32 size = net->getBitSize();
    if (size==1) {
      NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
      NUNetElabRefSet connection;
      NUNetElabRefHdl net_elab_ref = mNetElabRefFactory->createNetElabRef(hier,net_ref);
      connection.insert(net_elab_ref);
      mClosure->addSet(&connection);
    } else {
      for ( ; size > 0 ; --size ) {
        NUNetRefHdl net_ref = mNetRefFactory->createVectorNetRef(net,size-1);
        NUNetElabRefSet connection;
        NUNetElabRefHdl net_elab_ref = mNetElabRefFactory->createNetElabRef(hier,net_ref);
        connection.insert(net_elab_ref);
        mClosure->addSet(&connection);
      }
    }
  }

  //! Process information from a port-connection input.
  /*!
   * \param formal The formal of the port.
   * \param actual The actual of the port.
   * \param hier   The hierarchy on the actual side of the port.
   */
  void connectInput(const NUExpr * actual, 
                    STBranchNode * actual_hier,
                    NUNetRefHdl & formal_ref, 
                    STBranchNode * formal_hier) {

    switch(actual->getType()) {
    case NUExpr::eNUIdentRvalue:
    case NUExpr::eNUVarselRvalue: {
      NUNetRefSet uses(mNetRefFactory);
      actual->getUses(&uses);
      NU_ASSERT(uses.size()==1,actual);
      
      NUNetRefHdl actual_ref = (*(uses.begin()));

      // map the actual to its elaborated equivalent.
      NUNet * actual_net = actual_ref->getNet();
      NUNetElab * actual_net_elab = actual_net->lookupElab(actual_hier);
      NUNet * actual_storage_net = actual_net_elab->getStorageNet();
      STBranchNode * actual_storage_hier = actual_net_elab->getStorageHier();
      NUNetRefHdl actual_storage_ref = mNetRefFactory->createNetRefImage(actual_storage_net,
                                                                         actual_ref);

      connectNetRefs(actual_storage_ref, actual_storage_hier,
                     formal_ref, formal_hier);
      break;
    }
    case NUExpr::eNUConcatOp: {
      ConstantRange range;
      formal_ref->getRange(range);
      
      SInt32 msb = range.getMsb();
      
      const NUConcatOp * concat = dynamic_cast<const NUConcatOp*>(actual);
      for (UInt32 i = 0; i < concat->getNumArgs(); i++) {
        const NUExpr * child = concat->getArg(i);
        
        UInt32 size = child->getBitSize();
        
        ConstantRange sub_range(msb,msb-(size-1));
        NUNetRefHdl sub_formal_ref = mNetRefFactory->sliceNetRef(formal_ref,sub_range);
        connectInput(child, actual_hier, sub_formal_ref, formal_hier);
        msb -= size;
      }
      break;
    }
    default:
      NU_ASSERT("Unexpected expression type."==0,actual);
      break;
    }
  }

  //! Process information from a port-connection output/inout.
  /*!
   * \param formal The formal of the port.
   * \param actual The actual of the port.
   * \param hier   The hierarchy on the actual side of the port.
   * \param add_reverse Should we remember the reverse-connectivity (for inout)?
   */
  void connectOutput(NULvalue * actual, 
                     STBranchNode * actual_hier,
                     NUNetRefHdl & formal_ref, 
                     STBranchNode * formal_hier) {


    switch(actual->getType()) {
    case eNUIdentLvalue:
    case eNUVarselLvalue: {
      NUNetRefSet defs(mNetRefFactory);
      actual->getDefs(&defs);
      NU_ASSERT(defs.size()==1,actual);
      
      NUNetRefHdl actual_ref = (*(defs.begin()));

      // map the actual to its elaborated equivalent.
      NUNet * actual_net = actual_ref->getNet();
      NUNetElab * actual_net_elab = actual_net->lookupElab(actual_hier);
      NUNet * actual_storage_net = actual_net_elab->getStorageNet();
      STBranchNode * actual_storage_hier = actual_net_elab->getStorageHier();
      NUNetRefHdl actual_storage_ref = mNetRefFactory->createNetRefImage(actual_storage_net,
                                                                         actual_ref);

      connectNetRefs(actual_storage_ref, actual_storage_hier,
                     formal_ref, formal_hier);
      break;
    }
    case eNUConcatLvalue: {
      ConstantRange range;
      formal_ref->getRange(range);
      
      SInt32 msb = range.getMsb();
      
      NUConcatLvalue * concat = dynamic_cast<NUConcatLvalue*>(actual);
      for (UInt32 i = 0; i < concat->getNumArgs(); i++) {
        NULvalue * child = concat->getArg(i);
        
        UInt32 size = child->getBitSize();
        
        ConstantRange sub_range(msb,msb-(size-1));
        NUNetRefHdl sub_formal_ref = mNetRefFactory->sliceNetRef(formal_ref,sub_range);
        connectOutput(child, actual_hier, sub_formal_ref, formal_hier);
        msb -= size;
      }
      break;
    }
    default:
      NU_ASSERT("Unexpected lvalue type."==0,actual);
      break;
    }
  }

  
  void connectNetRefs(NUNetRefHdl & actual_ref,
                      STBranchNode * actual_hier,
                      NUNetRefHdl & formal_ref,
                      STBranchNode * formal_hier) {
    NU_ASSERT2(actual_ref->getNumBits()==formal_ref->getNumBits(),actual_ref,formal_ref);

    ConstantRange actual_range;
    ConstantRange formal_range;
    actual_ref->getRange(actual_range);
    formal_ref->getRange(formal_range);

    for (UInt32 i=0; i < formal_range.getLength(); ++i) {
      NUNetRefHdl sub_actual_ref = mNetRefFactory->sliceNetRef(actual_ref, actual_range.getMsb()-i);
      NUNetRefHdl sub_formal_ref = mNetRefFactory->sliceNetRef(formal_ref, formal_range.getMsb()-i);

      NUNetElabRefHdl actual_elab_ref = mNetElabRefFactory->createNetElabRef(actual_hier,
                                                                             sub_actual_ref);
      NUNetElabRefHdl formal_elab_ref = mNetElabRefFactory->createNetElabRef(formal_hier,
                                                                             sub_formal_ref);
      NUNetElabRefSet connection;
      connection.insert(actual_elab_ref);
      connection.insert(formal_elab_ref);
      mClosure->addSet(&connection);

      mFanin->insert(DoubleNetElabRef(actual_elab_ref,formal_elab_ref));
    }
      
  }

  
  //! Per-bit, add a NUNetElabRef to the mWrittenPorts set.
  /*!
   * This method is used to mark nets as written when connected to
   * complex ports.
   *
   * \param formal_ref  The unelaborated net ref.
   * \param formal_hier The elaborated hierarchy.
   */
  void markWrittenAbove(NUNetRefHdl & formal_ref,
                        STBranchNode * formal_hier) {
    ConstantRange formal_range;
    formal_ref->getRange(formal_range);

    for (UInt32 i=0; i < formal_range.getLength(); ++i) {
      NUNetRefHdl sub_formal_ref = mNetRefFactory->sliceNetRef(formal_ref, formal_range.getMsb()-i);
      NUNetElabRefHdl formal_elab_ref = mNetElabRefFactory->createNetElabRef(formal_hier,sub_formal_ref);
      mWrittenPorts->insert(formal_elab_ref);
    }
  }


  //! Unelab net-ref factory.
  NUNetRefFactory * mNetRefFactory;

  //! Elab net-ref factory.
  NUNetElabRefFactory * mNetElabRefFactory;

  //! Closure object for connections
  NUNetElabRefClosure * mClosure;

  //! Fanin relations.
  DoubleNetElabRefSet * mFanin;

  //! Formals with non-simple port connections; considered written.
  NUNetElabRefSet * mWrittenPorts;
};


void PortCoercion::traverse(NUNetElabRefClosure * closure,
                            DoubleNetElabRefSet * fanin,
                            NUNetElabRefSet     * written_ports)
{
  CoercionTraverseCallback callback(mTmpSymbolTable,
                                    mNetRefFactory,
                                    mNetElabRefFactory,
                                    closure,
                                    fanin,
                                    written_ports);
  // design walker to walk all instantiations; each module is processed > 1x.
  NUDesignWalker walker(callback,false,false);
  walker.design(mDesign);
}


void PortCoercion::process(NUNetElabRefClosure * closure,
                           DoubleNetElabRefSet * fanin,
                           const NUNetElabRefSet * written_ports)
{
  if (mVerbose) {
    printFanin(fanin);
  }

  NUNetElabRefToSetHashMap fanin_map;

  relationToConnectivity(fanin,&fanin_map);

  NUNetElabRefReadWriteData read_write_data;

  for (NUNetElabRefClosure::SetLoop loop = closure->loopSets();
       not loop.atEnd();
       ++loop) {
    NUNetElabRefSet * cluster = (*loop);
    processCluster(cluster,&fanin_map,written_ports,&read_write_data);
  }

  updatePortDirection(&read_write_data);
}


void PortCoercion::processCluster(NUNetElabRefSet * cluster,
                                  NUNetElabRefToSetHashMap * fanin_map,
                                  const NUNetElabRefSet * written_ports,
                                  NUNetElabRefReadWriteData * read_write_data)
{
  if (mVerbose) {
    printCluster(cluster);
  }

  determineReadWrite(cluster, fanin_map, written_ports, read_write_data);

  walkCluster(cluster, fanin_map, read_write_data);
}


void PortCoercion::determineReadWrite(NUNetElabRefSet * cluster,
                                      NUNetElabRefToSetHashMap * fanin_map,
                                      const NUNetElabRefSet * written_ports,
                                      NUNetElabRefReadWriteData * read_write_data)
{
  bool any_has_strong_driver = false;
  bool any_has_readers = false;
  bool any_has_writers = false;

  // Process the cluster twice. The first pass does not consider the
  // primary characteristics of nets. Once we understand whether or
  // not there are internal readers/writers, we update information
  // about the ports.

  for (NUNetElabRefSet::iterator iter = cluster->begin();
       iter != cluster->end();
       ++iter) {
    NUNetElabRefHdl point = (*iter);
    NUNet * point_net = point->getNet();

    bool written_port = (written_ports->find(point) != written_ports->end());

    bool has_strong_driver = hasStrongContDriver(point);
    any_has_strong_driver |= (has_strong_driver or written_port);

    bool has_writers = isWrittenPoint(point);
    bool has_readers = isReadPoint(point);

    // If we have a port that is not used internally (read or
    // written), consider it read. This is so coercion can have _some_
    // r/w data about the point.
    if ((not has_writers) and (not has_readers)) {
      if (not point_net->isPrimaryPort()) {
        if (fanin_map->find(point) == fanin_map->end()) {
          NU_ASSERT(point_net->isPort(),point_net);
          has_readers = true;
        }
      }
    }

    any_has_writers |= has_writers;
    any_has_readers |= has_readers;

    ReadWriteData rwData(has_readers, has_writers);
    if (written_port) {
      rwData.setAboveWriter(true);
      any_has_writers = true;
    }

    (*read_write_data)[point] = rwData;
  }
// pass 2, only process the primary ports
  for (NUNetElabRefSet::iterator iter = cluster->begin();
       iter != cluster->end();
       ++iter) {
    NUNetElabRefHdl point = (*iter);
    NUNet * point_net = point->getNet();

    if (not point_net->isPrimaryPort()) {
      continue;
    }

    ReadWriteData & rwData = (*read_write_data)[point];

    bool primary_reader = rwData.hasAboveReader();
    bool primary_writer = rwData.hasAboveWriter();

    // primaries shouldn't have above readers/writers yet.
    NU_ASSERT ((not (primary_reader or primary_writer)), point_net);

    if (isTieNet(point_net)) {
      // Cannot allow coercion of an input port to an output port. This
      // results in inconsistency between PLI/SystemC wrapper's expected
      // port direction and carbon model port direction. See bug6669.

      // Regardless of input, output, inout declaration, if the net is tied,
      // it's really an output.
//      if (not point_net->isPrimaryOutput())
//        rememberOutput(point_net);
//      primary_reader = true;
    } else if (point_net->isPrimaryOutput()) {
      primary_reader = true;
      if (any_has_readers and not any_has_strong_driver) {
        // If there are not any strong drivers, the PO needs to an
        // external value to the model.
        rememberInout(point_net);
        primary_writer = true;
      }
    } else if (point_net->isPrimaryInput()) {
      primary_writer = true;
#if 1
      // *** NOTE *** 

      // I believe that the below test is unnecessary for primary
      // inputs. Propagation of internal read/write information is
      // sufficient to determine if an input needs coercion.
      
      // If the below test is included, we will falsely attempt to
      // coerce concat situations:
      
      //   input a;
      //   wire b = ~a;
      //   sub s0 ( .out(out), .in({a,b})

      // Both 'a' and 'b' are in the same cluster, but writes should
      // not propagate from 'b' to 'a'. 

      // test/coerce/nocoerce[2,4].v test this situation.

      if (any_has_writers) {
        // If there are any writers, the PI needs to expose that value
        // to the external world.
        rememberInout(point_net);
        primary_reader = true;
      }
#endif
    } else if (point_net->isPrimaryBid()) {
      // Always consider the output contribution of an inout.
      primary_reader = true;
      // Only consider the input contribution of an inout if there is
      // no strong writer.
      if (not any_has_strong_driver) {
        primary_writer = true;
      }
    }

    rwData.setAboveReader(primary_reader);
    rwData.setAboveWriter(primary_writer);
  }
}


void PortCoercion::walkCluster(NUNetElabRefSet * cluster,
                               NUNetElabRefToSetHashMap * fanin,
                               NUNetElabRefReadWriteData * read_write_data)
{
  SInt32 iterations = 0;

  bool changed;
  do {
    changed = false;

    // Top-down.
    changed |= walkClusterTopDown(cluster, fanin, read_write_data);

    // Across siblings.
    changed |= walkClusterAcrossSiblings(cluster, fanin, read_write_data);

    // Bottom-up.
    changed |= walkClusterBottomUp(cluster, fanin, read_write_data);

    INFO_ASSERT(++iterations < 10, "Iteration limit exceeded"); // we should only take 4 iterations.
  } while (changed);
}


bool PortCoercion::walkClusterTopDown(NUNetElabRefSet * cluster,
                                      NUNetElabRefToSetHashMap * fanin,
                                      NUNetElabRefReadWriteData * read_write_data)
{
  bool changed = false;
  for (NUNetElabRefSet::iterator iter = cluster->begin();
       iter != cluster->end();
       ++iter) {
    NUNetElabRefHdl above = (*iter);

    const ReadWriteData & aboveData = (*read_write_data)[above];
    NUNetElabRefToSetHashMap::iterator location = fanin->find(above);
    if (location != fanin->end()) {
      NUNetElabRefHashSet & below_set = location->second;
      for (NUNetElabRefHashSet::iterator below_iter = below_set.begin();
           below_iter != below_set.end();
           ++below_iter) {
        NUNetElabRefHdl below = (*below_iter);

        ReadWriteData & belowData = (*read_write_data)[below];
        const ReadWriteData originalBelowData = belowData;
        belowData = belowData.addFromAbove(aboveData);

        changed |= (originalBelowData!=belowData);
      }
    }
  }
  return changed;
}


bool PortCoercion::walkClusterAcrossSiblings(NUNetElabRefSet * cluster,
                                             NUNetElabRefToSetHashMap * fanin,
                                             NUNetElabRefReadWriteData * read_write_data)
{
  bool changed = false;
  for (NUNetElabRefSet::iterator iter = cluster->begin();
       iter != cluster->end();
       ++iter) {
    NUNetElabRefHdl above = (*iter);

    const ReadWriteData & aboveData = (*read_write_data)[above];

    // If we are going to port-split, consider bits with multi-drivers
    // in a pessimistic fashion to trigger a split of this port.
    bool propagate_for_multi_writers = (mDoPortSplitting and (mPhase==1));

    // siblings have reads and writes.
    bool propagate_to_siblings = (aboveData.hasBelowWriter() and aboveData.hasBelowReader()); 
    // reads and writes would not propagate to siblings from above.
    propagate_to_siblings &= not ( (aboveData.hasLocalWriter() or aboveData.hasAboveWriter()) and
                                   (aboveData.hasLocalReader() or aboveData.hasAboveReader()) );

    if (not (propagate_for_multi_writers or propagate_to_siblings)) {
      continue;
    }

    // Determine the number of child readers/writers.
    NUNetElabRefSet readers;
    NUNetElabRefSet writers;

    NUNetElabRefToSetHashMap::iterator location = fanin->find(above);
    if (location != fanin->end()) {
      NUNetElabRefHashSet & below_set = location->second;
      for (NUNetElabRefHashSet::iterator below_iter = below_set.begin();
           below_iter != below_set.end();
           ++below_iter) {
        NUNetElabRefHdl below = (*below_iter);
        const ReadWriteData & belowData = (*read_write_data)[below];
        if (belowData.coversReader()) {
          readers.insert(below);
        }
        if (belowData.coversWriter()) {
          writers.insert(below);
        }
      }
    }

    UInt32 num_readers = readers.size();
    UInt32 num_writers = writers.size();

    if (propagate_for_multi_writers) {
      // mark multi-drivers as pessimistic. should trigger splitting.
      NUNetElabRefToSetHashMap::iterator location = fanin->find(above);
      if (location != fanin->end()) {
        NUNetElabRefHashSet & below_set = location->second;
        for (NUNetElabRefHashSet::iterator below_iter = below_set.begin();
             below_iter != below_set.end();
             ++below_iter) {
          NUNetElabRefHdl below = (*below_iter);
          ReadWriteData & belowData = (*read_write_data)[below];
          const ReadWriteData originalBelowData = belowData;

          if (belowData.coversWriter() and (num_writers > 1)) {
            belowData.setLocalReader(true);
            belowData.setAboveReader(true);
          }

          changed |= (originalBelowData!=belowData);
        }
      }
    }

    if (propagate_to_siblings) {

      // propagate read/write relationships across siblings.
      NUNetElabRefToSetHashMap::iterator location = fanin->find(above);
      if (location != fanin->end()) {
        NUNetElabRefHashSet & below_set = location->second;
        for (NUNetElabRefHashSet::iterator below_iter = below_set.begin();
             below_iter != below_set.end();
             ++below_iter) {
          NUNetElabRefHdl below = (*below_iter);
          ReadWriteData & belowData = (*read_write_data)[below];
          const ReadWriteData originalBelowData = belowData;

          // If this point covers a reader and there are other sibling
          // writers, this point needs to believe that there are
          // external writers.
          if (belowData.coversReader()) {
            if (num_writers > 1 or (num_writers==1 and not belowData.coversWriter())) {
              belowData.setAboveWriter(true);
            }
          }

          // If this point covers a writer and there are other sibling
          // readers, this point needs to believe that there are
          // external readers.
          if (belowData.coversWriter()) {
            if (num_readers > 1 or (num_readers==1 and not belowData.coversReader())) {
              belowData.setAboveReader(true);
            }
          }

          changed |= (originalBelowData!=belowData);
        }
      }
    }
  }
  return changed;
}


bool PortCoercion::walkClusterBottomUp(NUNetElabRefSet * cluster,
                                       NUNetElabRefToSetHashMap * fanin,
                                       NUNetElabRefReadWriteData * read_write_data)
{
  bool changed = false;
  for (NUNetElabRefSet::reverse_iterator iter = cluster->rbegin();
       iter != cluster->rend();
       ++iter) {
    NUNetElabRefHdl above = (*iter);

    ReadWriteData & aboveData = (*read_write_data)[above];
    const ReadWriteData originalAboveData = aboveData;
    NUNetElabRefToSetHashMap::iterator location = fanin->find(above);
    if (location != fanin->end()) {
      NUNetElabRefHashSet & below_set = location->second;
      for (NUNetElabRefHashSet::iterator below_iter = below_set.begin();
           below_iter != below_set.end();
           ++below_iter) {
        NUNetElabRefHdl below = (*below_iter);

        aboveData = aboveData.addFromBelow( (*read_write_data)[below] );
      }
    }

    changed |= (originalAboveData!=aboveData);
  }
  return changed;
}


void PortCoercion::relationToConnectivity(const DoubleNetElabRefSet * in, NUNetElabRefToSetHashMap * out) const
{
  for (DoubleNetElabRefSet::const_iterator iter = in->begin();
       iter != in->end();
       ++iter) {
    NUNetElabRefHdl a = (*iter).first;
    NUNetElabRefHdl b = (*iter).second;

    (*out)[a].insert(b);
  }
}


void PortCoercion::connectivityToRelation(const NUNetElabRefToSetHashMap * in, DoubleNetElabRefSet * out) const
{
  for (NUNetElabRefToSetHashMap::const_iterator above_iter = in->begin();
       above_iter != in->end();
       ++above_iter) {
    NUNetElabRefHdl a = (*above_iter).first;
    const NUNetElabRefHashSet & below_set = (*above_iter).second;
    for (NUNetElabRefHashSet::const_iterator below_iter = below_set.begin();
         below_iter != below_set.end();
         ++below_iter) {
      NUNetElabRefHdl b = (*below_iter);
      out->insert(DoubleNetElabRef(a,b));
    }
  }
}


void PortCoercion::updatePortDirection(const NUNetElabRefReadWriteData * read_write_data)
{
  NUNetReadWriteData unelab_read_write_data;
  determineUnelabReadWrite(read_write_data,&unelab_read_write_data);
  if (mVerbose) {
    printElabReadWriteData(read_write_data);
    printReadWriteData(&unelab_read_write_data);
  }

  for (NUNetReadWriteData::const_iterator iter = unelab_read_write_data.begin();
       iter != unelab_read_write_data.end();
       ++iter) {
    NUNet * point_net = (*iter).first;
    if (not point_net->isPort()) {
      continue;
    }

    const ReadWriteData & rwData = (*iter).second;

    bool inout = false;

    // Inout calculation for primary ports is a little more
    // complicated than for internal ports. We need to consider
    // whether or not an external reader/writer can have effect.
    if (point_net->isPrimaryPort()) {
      inout = rwData.isPrimaryInout();
    } else {
      inout = rwData.isInout();
    }

    if (inout) {
      if (not point_net->isBid()) {
        rememberInout(point_net);
      }
    } else if (rwData.isInput()) {
      if (not point_net->isInput()) {
        rememberInput(point_net);
      }
    } else if (rwData.isOutput()) {
      if (not point_net->isOutput()) {
        rememberOutput(point_net);
      }
    } else {
#if 0
      // we could use this code to locate useless ports
      if (point_net->isPort()) {
        UtString name;
        name << point_net->getScope()->getName()->str() << ".";
        point_net->compose(&name,NULL);
        UtIO::cout() << "PortNonPort: " << name.c_str() << UtIO::endl;

        INFO_ASSERT(0, "This code is ifdefed out."); // this port was declared as port but doesn't act like a port.
      }
#endif
    }
  }

  checkCoercionConsistency();
}


void PortCoercion::determineUnelabReadWrite(const NUNetElabRefReadWriteData * read_write_data,
                                            NUNetReadWriteData * unelab_read_write_data) const
{
  for (NUNetElabRefReadWriteData::const_iterator iter = read_write_data->begin();
       iter != read_write_data->end();
       ++iter) {
    NUNetElabRefHdl point = (*iter).first;
    const ReadWriteData & rwData = (*iter).second;

    NUNet * point_net = point->getNet();
    NUNetReadWriteData::iterator location = unelab_read_write_data->find(point_net);
    if (location==unelab_read_write_data->end()) {
      (*unelab_read_write_data)[point_net] = rwData;
    } else {
      (*location).second += rwData;
    }
  }
}


void PortCoercion::rememberInout(NUNet * net)
{
  NU_ASSERT(net->isInput() or net->isOutput(),net);
  mInouts->insert(net);
}


void PortCoercion::rememberInput(NUNet * net)
{
  NU_ASSERT(net->isBid() or net->isOutput(),net);
  mInputs->insert(net);
}


void PortCoercion::rememberOutput(NUNet * net)
{
  NU_ASSERT(net->isBid() or net->isInput(),net);
  mOutputs->insert(net);
}


void PortCoercion::checkCoercionConsistency() const
{
  INFO_ASSERT(not set_has_intersection(*mInouts,*mInputs), "Consistency error");
  INFO_ASSERT(not set_has_intersection(*mInouts,*mOutputs), "Consistency error");
  INFO_ASSERT(not set_has_intersection(*mInputs,*mOutputs), "Consistency error");
}


//! Callback used to update all port connections based on new port directions.
class CoerceCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  CoerceCallback(AtomicCache * str_cache,
                 NUNetSet * inouts,
                 NUNetSet * inputs,
                 NUNetSet * outputs) :
    NUDesignCallback(),
    mStrCache(str_cache),
    mInouts(inouts),
    mInputs(inputs),
    mOutputs(outputs)
  {}

  ~CoerceCallback() {};

  //! By default, skip everything.
  Status operator()(Phase, NUBase*) { return eSkip; }

  //! Walk through the design.
  Status operator()(Phase, NUDesign*) { return eNormal; }

  //! Process all module instances.
  Status operator()(Phase phase, NUModuleInstance * instance) { 
    // loop over all port connections; converting as necessary.
    if (phase==ePre) {
      bool changed = false;
      NUPortConnectionVector connections;
      for (NUPortConnectionLoop loop = instance->loopPortConnections();
           not loop.atEnd();
           ++loop) {
        NUPortConnection * connection = (*loop);
        NUPortConnection * replacement = NULL;

        NUNet * formal = connection->getFormal();

        if (not formal->is2DAnything()) { // skip 2-D/memory ports for now.
          if (mInouts->find(formal) != mInouts->end()) {
            // We have a formal that has been coerced to inout; update the port connection.
            replacement = createInout(connection);
          } else if (mInputs->find(formal) != mInputs->end()) {
            // We have a formal that has been coerced to input; update the port connection.
            replacement = createInput(connection);
          } else if (mOutputs->find(formal) != mOutputs->end()) {
            // We have a formal that has been coerced to output; update the port connection.
            replacement = createOutput(connection);
          } 
        }

        if (replacement) {
          replacement->setModuleInstance(instance);
          connections.push_back(replacement);
          changed = true;
          delete connection;
        } else {
          // unchanged.
          connections.push_back(connection);
        }
      }

      if (changed) {
        instance->setPortConnections(connections);
      }
    }
    return eNormal; 
  }

  //! Walk through modules we haven't seen before.
  Status operator()(Phase phase, NUModule * module) {
    if (phase==ePre) {
      mModuleStack.push(module);
    } else {
      mModuleStack.pop();
    }
    return eNormal;
  }

  //! Walk through intances inside this declaration scope.
  Status operator()(Phase, NUNamedDeclarationScope*) {
    return eNormal;
  }

private:

  //! Create an inout port connection from either input or output.
  NUPortConnectionBid * createInout(NUPortConnection * connection) {
    NULvalue * actual = NULL;
    switch(connection->getType()) {
    case eNUPortConnectionInput: {
      NUPortConnectionInput * input = dynamic_cast<NUPortConnectionInput*>(connection);
      actual = createActualLvalue(input);
      break;
    }
    case eNUPortConnectionOutput: {
      NUPortConnectionOutput * output = dynamic_cast<NUPortConnectionOutput*>(connection);
      actual = output->getActual();
      output->setActual(NULL);
      break;
    }
    case eNUPortConnectionBid: {
      // We should never encounter a bid connection for something that
      // is just becoming a bid.
      NU_ASSERT("Unexpected port type for coerced net."==0,connection);
      break;
    }
    default: {
      // No other port connection types.
      NU_ASSERT("Unexpected port type."==0,connection);
      break;
    }
    }
          
    NUPortConnectionBid * replacement = new NUPortConnectionBid(actual, 
                                                                connection->getFormal(), 
                                                                connection->getLoc());
    return replacement;
  }


  //! Create an inout port connection from either input or output.
  NUPortConnectionInput * createInput(NUPortConnection * connection) {
    NUExpr * actual = NULL;
    switch(connection->getType()) {
    case eNUPortConnectionInput: {
      // We should never encounter an input connection for something
      // that is just becoming an input.
      NU_ASSERT("Unexpected port type for coerced net."==0,connection);
      break;
    }
    case eNUPortConnectionOutput: {
      NUPortConnectionOutput * output = dynamic_cast<NUPortConnectionOutput*>(connection);
      NULvalue * out_actual = output->getActual();
      if (out_actual) {
        actual = out_actual->NURvalue();
        actual->resize(out_actual->getBitSize());
      }
      break;
    }
    case eNUPortConnectionBid: {
      NUPortConnectionBid * bid = dynamic_cast<NUPortConnectionBid*>(connection);
      NULvalue * out_actual = bid->getActual();
      if (out_actual) {
        actual = out_actual->NURvalue();
        actual->resize(out_actual->getBitSize());
      }
      break;
    }
    default: {
      // No other port connection types.
      NU_ASSERT("Unexpected port type."==0,connection);
      break;
    }
    }
          
    NUPortConnectionInput * replacement = new NUPortConnectionInput(actual, 
                                                                    connection->getFormal(), 
                                                                    connection->getLoc());
    return replacement;
  }


  //! Create an inout port connection from either input or output.
  NUPortConnectionOutput * createOutput(NUPortConnection * connection) {
    NULvalue * actual = NULL;
    switch(connection->getType()) {
    case eNUPortConnectionInput: {
      NUPortConnectionInput * input = dynamic_cast<NUPortConnectionInput*>(connection);
      actual = createActualLvalue(input);
      break;
    }
    case eNUPortConnectionOutput: {
      // We should never encounter an input connection for something
      // that is just becoming an input.
      NU_ASSERT("Unexpected port type for coerced net."==0,connection);
      break;
    }
    case eNUPortConnectionBid: {
      NUPortConnectionBid * bid = dynamic_cast<NUPortConnectionBid*>(connection);
      actual = bid->getActual();
      bid->setActual(NULL);
      break;
    }
    default: {
      // No other port connection types.
      NU_ASSERT("Unexpected port type."==0,connection);
      break;
    }
    }
          
    NUPortConnectionOutput * replacement = new NUPortConnectionOutput(actual, 
                                                                      connection->getFormal(), 
                                                                      connection->getLoc());
    return replacement;
  }


  //! Create an lvalue for output or input from input port connection.
  NULvalue * createActualLvalue(NUPortConnectionInput * input) {
    NUNet * formal = input->getFormal();
    NUExpr * expr = input->getActual();

    NULvalue * actual = NULL;
    if (input->isSimple()) {
      actual = expr->Lvalue(expr->getLoc());
    } else {
      // We've got something that can't be converted into
      // lvalue; make a temp. The port connection will appear
      // as a second driver for this generated temporary.
      if (expr) {
        NUModule * module = mModuleStack.top();
        StringAtom* sym = module->gensym("coerce", NULL, expr);
        NUNet * temp = module->createTempNet(sym,
                                             formal->getBitSize(),
                                             expr->isSignedResult(),
                                             input->getLoc());
        StringAtom *block_name = module->newBlockName(mStrCache,
                                                      input->getLoc());
        NULvalue *lhs = new NUIdentLvalue(temp, input->getLoc());
        NUContAssign *assign = new NUContAssign(block_name, lhs, expr, input->getLoc());
        module->addContAssign(assign);
        actual = new NUIdentLvalue(temp, input->getLoc()); 
      }else {
        actual = NULL;
      }
      input->setActual(NULL);
    }
    return actual;
  }

  //! Stack used to track the current module
  UtStack<NUModule*> mModuleStack;

  //! String cache.
  AtomicCache * mStrCache;

  //! Nets becoming inout.
  NUNetSet * mInouts;

  //! Nets becoming input.
  NUNetSet * mInputs;

  //! Nets becoming output.
  NUNetSet * mOutputs;
};


bool PortCoercion::hasStrongContDriver(const NUNetElabRefHdl & point) const
{
  bool has_strong_driver = false;
  STBranchNode * point_hier = point->getScope();

  NUNetRefHdl point_net_ref = point->getNetRef();
  NUNet * point_net = point_net_ref->getNet();

  NUNetElab * point_net_elab = point_net->lookupElab(point_hier);
  for (STAliasedLeafNode::AliasLoop aliases(point_net_elab->getSymNode());
       (not aliases.atEnd()) and (not has_strong_driver);
       ++aliases) {
    STAliasedLeafNode *alias = *aliases;
    NUNet* alias_net = NUNet::find(alias);
    if (alias_net) {
      if (true or mReachables->query(alias)) {
        NUNetRefHdl alias_net_ref = mNetRefFactory->createNetRefImage(alias_net,
                                                                      point_net_ref);
        has_strong_driver = mPortAnalysis->hasStrongContDriver(alias_net_ref);
      }
    }
  }
  return has_strong_driver;
}


bool PortCoercion::isTieNet(NUNet * net) const
{
  for (NUNet::DriverLoop loop = net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    const FLNode* driver = *loop;
    const NUUseDefNode* node = driver->getUseDefNode();
    if (node && (node->getType() == eNUContAssign)) {
      const NUContAssign* assign = dynamic_cast<const NUContAssign*>(node);
      NU_ASSERT(assign, node);
      if (assign->getStrength() >= eStrTie)
        return true;
    }
  }
  return false;
}


bool PortCoercion::isWrittenPoint(const NUNetElabRefHdl & point) const
{
  bool is_written = false;
  STBranchNode * point_hier = point->getScope();

  NUNetRefHdl point_net_ref = point->getNetRef();
  NUNet * point_net = point_net_ref->getNet();

  NUNetElab * point_net_elab = point_net->lookupElab(point_hier);
  for (STAliasedLeafNode::AliasLoop aliases(point_net_elab->getSymNode());
       (not aliases.atEnd()) and (not is_written);
       ++aliases) {
    STAliasedLeafNode *alias = *aliases;
    NUNet* alias_net = NUNet::find(alias);
    if (alias_net) {
      if (true or mReachables->query(alias)) {
        is_written = isWritten(alias_net);
      }
    }
  }
  return is_written;
}


bool PortCoercion::isWritten(const NUNet * net) const
{
  return (net->isWritten() or 
          net->isTristate() or
          net->isSupply0() or
          net->isSupply1() or
          net->isPulled() or 
          net->isProtectedMutableNonHierref(mIODB));
}


bool PortCoercion::isReadPoint(const NUNetElabRefHdl & point) const
{
  bool is_read = false;
  STBranchNode * point_hier = point->getScope();

  NUNetRefHdl point_net_ref = point->getNetRef();
  NUNet * point_net = point_net_ref->getNet();

  NUNetElab * point_net_elab = point_net->lookupElab(point_hier);
  for (STAliasedLeafNode::AliasLoop aliases(point_net_elab->getSymNode());
       (not aliases.atEnd()) and (not is_read);
       ++aliases) {
    STAliasedLeafNode *alias = *aliases;
    NUNet* alias_net = NUNet::find(alias);
    if (alias_net) {
      if (true or mReachables->query(alias)) {
        is_read = isRead(alias_net);
      }
    }
  }
  return is_read;
}


bool PortCoercion::isRead(const NUNet * net) const
{
  return (net->isRead() or
          net->isProtectedObservableNonHierref(mIODB));
}


void PortCoercion::coerce()
{
  // 1. redeclare
  redeclareInouts();
  redeclareInputs();
  redeclareOutputs();

  // 2. convert port connections.
  CoerceCallback callback(mStrCache,mInouts,mInputs,mOutputs);
  NUDesignWalker walker(callback,false);
  walker.design(mDesign);
}


void PortCoercion::redeclareInouts() const
{
  for (NUNetSet::SortedLoop loop = mInouts->loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    // output a message about this net becoming inout.
    UtString name;
    name << net->getScope()->getName()->str() << ".";
    net->compose(&name,NULL);
    const char * c_name = name.c_str();
    if (net->isOutput()) {
      if (net->isPrimary()) {
        mMsgContext->CoercePrimaryOutputToBid(&net->getLoc(),c_name);
      } else {
        mMsgContext->CoerceOutputToBid(&net->getLoc(),c_name);
      }
    } else if (net->isInput()) {
      if (net->isPrimary()) {
        mMsgContext->CoercePrimaryInputToBid(&net->getLoc(),c_name);
      } else {
        mMsgContext->CoerceInputToBid(&net->getLoc(),c_name);
      }
    } else {
      NU_ASSERT("Unexpected type for coerced net."==0,net);
    }

    // update declaration flags.
    net->putIsBid(true);
  }
}


void PortCoercion::redeclareInputs() const
{
  for (NUNetSet::SortedLoop loop = mInputs->loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    // output a message about this net becoming input.
    UtString name;
    name << net->getScope()->getName()->str() << ".";
    net->compose(&name,NULL);
    const char * c_name = name.c_str();
    if (net->isOutput()) {
      if (net->isPrimary()) {
        // In our current implementation, primary outputs can never
        // become primary inputs, as that would lose the external sink
        // on the net.

        // Should a primary output ever become primary input?
        mMsgContext->CoercePrimaryOutputToInput(&net->getLoc(),c_name);
        NU_ASSERT("Unexpected coercion."==0,net);
      } else {
        mMsgContext->CoerceOutputToInput(&net->getLoc(),c_name);
      }
    } else if (net->isBid()) {
      if (net->isPrimary()) {
        mMsgContext->CoercePrimaryBidToInput(&net->getLoc(),c_name);
      } else {
        mMsgContext->CoerceBidToInput(&net->getLoc(),c_name);
      }
    } else {
      NU_ASSERT("Unexpected type for coerced net."==0,net);
    }

    // update declaration flags.
    net->putIsInput(true);
  }
}


void PortCoercion::redeclareOutputs() const
{
  for (NUNetSet::SortedLoop loop = mOutputs->loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    // output a message about this net becoming output.
    UtString name;
    name << net->getScope()->getName()->str() << ".";
    net->compose(&name,NULL);
    const char * c_name = name.c_str();
    if (net->isInput()) {
      if (net->isPrimary()) {
        // In our current implementation, primary inputs can become
        // primary outputs if the user tied one of its aliases. This
        // is considered stronger than the external source on the net
        // due to the net declaration..

        // Should a primary input ever become primary output?
        mMsgContext->CoercePrimaryInputToOutput(&net->getLoc(),c_name);
      } else {
        mMsgContext->CoerceInputToOutput(&net->getLoc(),c_name);
      }
    } else if (net->isBid()) {
      if (net->isPrimary()) {
        mMsgContext->CoercePrimaryBidToOutput(&net->getLoc(),c_name);
      } else {
        mMsgContext->CoerceBidToOutput(&net->getLoc(),c_name);
      }
    } else {
      NU_ASSERT("Unexpected type for coerced net."==0,net);
    }

    // update declaration flags.
    net->putIsOutput(true);
  }
}


void PortCoercion::printCluster(const NUNetElabRefSet * cluster) const
{
  UtIO::cout() << "Cluster:" << UtIO::endl;
  for (NUNetElabRefSet::const_iterator iter = cluster->begin();
       iter != cluster->end();
       ++iter) {
    const NUNetElabRefHdl net_elab_ref = (*iter);
    net_elab_ref->print(2);
  }
}


void PortCoercion::printFaninMap(const NUNetElabRefToSetHashMap * fanin_map) const 
{
  DoubleNetElabRefSet fanin;
  connectivityToRelation(fanin_map,&fanin);
  printFanin(&fanin);
}


void PortCoercion::printFanin(const DoubleNetElabRefSet * fanin) const
{
  UtIO::cout() << "*** Elaborated Hierarchical Fanin (" << mPhase << ") ***" << UtIO::endl;
  for (DoubleNetElabRefSet::const_iterator iter = fanin->begin();
       iter != fanin->end();
       ++iter) {
    const NUNetElabRefHdl above = (*iter).first;
    const NUNetElabRefHdl below = (*iter).second;
    
    UtIO::cout() << "Fanin Pair:" << UtIO::endl;
    above->print(2);
    below->print(2);
  }
}


void PortCoercion::printElabReadWriteData(const NUNetElabRefReadWriteData * read_write_data) const
{
  UtIO::cout() << "*** Elaborated Read/Write Data ***" << UtIO::endl;
  for (NUNetElabRefReadWriteData::const_iterator iter = read_write_data->begin();
       iter != read_write_data->end();
       ++iter) {
    const NUNetElabRefHdl point = (*iter).first;
    const ReadWriteData & rwData = (*iter).second;
    point->print(0);
    rwData.print();
  }
}


void PortCoercion::printReadWriteData(const NUNetReadWriteData * read_write_data) const
{
  UtIO::cout() << "*** Unelaborated Read/Write Data ***" << UtIO::endl;
  for (NUNetReadWriteData::const_iterator iter = read_write_data->begin();
       iter != read_write_data->end();
       ++iter) {
    NUNet * point_net = (*iter).first;
    const ReadWriteData & rwData = (*iter).second;

    UtString name;
    name << point_net->getScope()->getName()->str() << ".";
    point_net->compose(&name,NULL);

    UtIO::cout() << name.c_str() << UtIO::endl;
    rwData.print();
  }
}

void ReadWriteData::print(int indent) const
{
  size_t len = UtIOStreamBase::limitIntArg(indent, 0, 150, 0);
  UtString prefix(len, ' ');

  UtIO::cout() << prefix << "above reader: " << mAboveReader << UtIO::endl
               << prefix << "above writer: " << mAboveWriter << UtIO::endl
               << prefix << " local reader: " << mLocalReader << UtIO::endl
               << prefix << " local writer: " << mLocalWriter << UtIO::endl
               << prefix << "  below reader: " << mBelowReader << UtIO::endl
               << prefix << "  below writer: " << mBelowWriter << UtIO::endl;
}


PortAnalysis::PortAnalysis(NUNetRefFactory * net_ref_factory) :
  mNetRefFactory(net_ref_factory)
{
}


PortAnalysis::~PortAnalysis()
{
}


bool PortAnalysis::hasStrongContDriver(const NUNetRefHdl point_hdl, bool include_ports) const
{
  NUNet * point_net = point_hdl->getNet();

  NUNetRefHdl strongly_driven_parts = mNetRefFactory->createEmptyNetRef();

  for (NUNet::DriverLoop loop = point_net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    const FLNode * driver = (*loop);
    const NUUseDefNode * node = driver->getUseDefNode();
    NUNetRefHdl def_net_ref = driver->getDefNetRef();

    // test/langcov/Generate/gclk.v takes a long time in this code if we
    // merge all the drivers of point_net and then see if they cover point_hdl.
    // Filter out those that have no intersection at all.
    if (! def_net_ref->overlapsSameNet(*point_hdl)) {
      continue;
    }
    NUNetRefHdl intersect = mNetRefFactory->intersect(def_net_ref, point_hdl);

    if (node) {
      switch(node->getType()) {
      case eNUInitialBlock:
      case eNUAlwaysBlock:
        strongly_driven_parts = mNetRefFactory->merge(strongly_driven_parts, intersect);
        break;
      case eNUContAssign: {
        const NUContAssign * assign = dynamic_cast<const NUContAssign*>(node);
        if (assign->getStrength() >= eStrDrive) {
          const NUExpr * rvalue = assign->getRvalue();
          if (not rvalue->drivesZ()) {
            strongly_driven_parts = mNetRefFactory->merge(strongly_driven_parts, intersect);
          }
        }
        break;
      }
      case eNUModuleInstance:
        if (include_ports) {
          strongly_driven_parts = mNetRefFactory->merge(strongly_driven_parts, intersect);
        }
        break;
      default:
        break; // nothing else considered strong (we don't care about port connections).
      }
    }
  }
  
  bool strong_writer = strongly_driven_parts->covers(*point_hdl);
  return strong_writer;
}


