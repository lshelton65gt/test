// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "LFContext.h"
#include "compiler_driver/CarbonContext.h"
#include "hdl/HdlVerilogPath.h"
#include "localflow/VhdlPopulate.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUSysTask.h"
#include "symtab/STSymbolTable.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/HdlFileCollector.h"

/*
 * Create a module object from the given Jaguar node of type VHENTITY
 *
 */

Populate::ErrorCode VhdlPopulate::entityDecl(vhNode vh_entity,
                                             LFContext *context,
                                             const char *uniquifiedName,
                                             NUModule **the_module,
                                             bool* seenAlready)
{
  ErrorCode err_code = eSuccess;
  *the_module = 0;
  *seenAlready = false;
  
  SourceLocator loc = locator(vh_entity);
  vhObjType type = vhGetObjType(vh_entity);
  if ( type != VHENTITY )
  {
    const char* name = vhGetName(vh_entity);
    POPULATE_FAILURE(mMsgContext->UnsupportedInstanceType(&loc, name, gGetVhNodeDescription(vh_entity)), &err_code);
    return err_code;
  }

  const char* moduleName;
  JaguarString entityName(vhGetEntityName(vh_entity));
  StringAtom *entityAtom = context->vhdlIntern( entityName );
  if (uniquifiedName)
    moduleName = uniquifiedName;
  else 
    moduleName = entityAtom->str();
  StringAtom* moduleNameAtom = context->vhdlIntern(moduleName);
  NUModule *find_module = 0;
  if ((find_module = context->lookupModule(moduleNameAtom))) 
  {
    context->putLastVisitedModule(find_module);
    *seenAlready = true;
    return eSuccess;
  }
  
  NUDesign * design = context->getDesign();
  STSymbolTable * aliasDB = new STSymbolTable(design->getAliasBOM(),
                                              context->getStringCache());
  aliasDB->setHdlHier(new HdlVerilogPath);
  //bool isCModel       = mIODB->isCModel(entityAtom->str());

  bool isTopLevel = not context->getModule();
  *the_module = new NUModule(design, isTopLevel, moduleNameAtom, aliasDB,
                             context->getNetRefFactory(), 
                             context->getStringCache(), loc, mIODB, 
                             -15, -15, false, eSLVHDL);
  if (isTopLevel) {
    context->maybeSetRootModule( *the_module );
  }

  (*the_module)->putOriginalName(entityAtom);

  design->addModule(*the_module);
  context->pushBlockScope(*the_module);
  context->pushDeclarationScope(*the_module);
  context->pushJaguarScope(vh_entity);
  context->pushExtraTaskEnableContext();

  context->mapModule(moduleNameAtom, *the_module);

  return err_code;
}

Populate::ErrorCode VhdlPopulate::emptyArchitecture(vhNode vh_arch)
{
  const SourceLocator loc = locator( vh_arch );
  UtString archname;
  archname << vhGetEntityName( vh_arch ) << "(" << vhGetArchName( vh_arch ) << ")";
  mMsgContext->EmptyArchitecture( &loc, archname.c_str() );
  // This situation is ok.
  return eSuccess;
}

bool
VhdlPopulate::isLineVariable( vhNode vh_node )
{
  const vhObjType objType = vhGetObjType( vh_node );
  if (objType == VHFORINDEX )
  {
    return false;
  }
  vhNode subtype;
  if ( objType == VHELEMENTDECL )
  {
    subtype = vhGetEleSubTypeInd( vh_node );
  }
  else
  {
    subtype = vhGetSubTypeInd( vh_node );
  }

  vhNode type = vhGetType( subtype );
  vhNode vh_typedef = vhGetTypeDef( type );
  vhNode vh_scope = vhGetScope( type );
  if ( vhGetObjType( vh_typedef ) == VHACCESSTYPE &&
       vhGetObjType( vh_scope ) == VHPACKAGEDECL &&
       !strcasecmp( vhGetTypeName( type ), "LINE" ) &&
       !strcasecmp( vhGetPackName( vh_scope ), "TEXTIO" ) &&
       !strcasecmp( vhGetLibName( vh_scope ), "STD" ))
  {
    return true;
  } 
  return false;
}

/*
 * Populate a  NUFOpenSysTask for LINE variable declaration. This would let
 * codegen to emitCode for initialization of UtIOStingStream object, which
 * represents the LINE variable.
 *
 */
Populate::ErrorCode
VhdlPopulate::initializeLineVar(LFContext *context,
                                NUVectorNet *the_net,
                                const SourceLocator &loc)
{
  NUExprVector expr_vector;
  NUModule *module = context->getModule();
  StringAtom *sym = module->gensym( "line" );

  {
    // Indicate population of declaration initialization statement outside
    // the current scope, in the module's initial block.
    DeclInitIndicator dii(context);
    // Empty 'expr_vector' with 'isVerilogTask == false' would signify 
    // initialization of LINE variable.
    NUFOpenSysTask* sysFOpen = context->stmtFactory()->createFOpenSysTask( sym, the_net, NULL, expr_vector,
                                                                           module, false,
                                                                           false /*isVerilogTask*/,
                                                                           true/*useInFileSystem*/, loc );

    // Create an initial block if none in the module & then add sysFOpen
    addStmtToInitialBlock( context, module, sysFOpen, the_net, loc );
  }
  return eSuccess;
}

// Function to process item declarations in the declarative scope
// of a given statement/object.
Populate::ErrorCode 
VhdlPopulate::processDeclarations(vhNode n_stm, LFContext *context)
{
  ErrorCode err_code = eSuccess;

  NUScope *declaration_scope = context->getDeclarationScope();
  JaguarList vh_decl_iter(vhGetDeclItemList(n_stm));
  vhNode vh_decl = 0;
  while((vh_decl = vhGetNextItem(vh_decl_iter))) {
    vhObjType decl_type = vhGetObjType(vh_decl);
    switch (decl_type)
    {
    case VHVARIABLE: 
    {
      NUNet* the_net = 0;
      ErrorCode var_err_code = var(vh_decl, context, &the_net);
      if ( errorCodeSaysReturnNowOnlyWithFatal( var_err_code, &err_code ))
      {
        return var_err_code;
      }
      if (the_net)
      {
        declaration_scope->addLocal(the_net);
      }
      break;
    }
    case VHCONSTANT:
    case VHSIGNAL:
    {
      NUNet* the_net = 0;
      ErrorCode net_err_code = local(vh_decl, context, &the_net);
      if (errorCodeSaysReturnNowOnlyWithFatal(net_err_code,&err_code))
      {
        return net_err_code;
      }
      if (the_net) 
      {
        declaration_scope->addLocal(the_net);
      }
    }
    break;
    case VHALIASDECL:
    {
      ErrorCode alias_err_code = createAndAssignSignalForAliasObject(vh_decl,
                                                                     context);
      if (errorCodeSaysReturnNowOnlyWithFatal(alias_err_code,&err_code))
      {
        return alias_err_code;
      }
      break;
    }
    case VHFILEDECL:
    {
      ErrorCode file_err_code = file(vh_decl,
                                     context);
      if (errorCodeSaysReturnNowOnlyWithFatal(file_err_code,&err_code))
      {
        return file_err_code;
      }
      break;
    }
    case VHSUBPROGBODY: // Processed during the subprogram call
    case VHCONFIGSPEC:  // Handled by elaboration
    case VHSUBPROGDECL:
    case VHSUBTYPEDECL:
    case VHTYPEDECL:
    case VHCOMPONENT:
    case VHATTRBDECL:
    case VHATTRIBUTESPEC:
    case VHLIBRARYCLAUSE:
    case VHUSECLAUSE:
      break; // These are handled elsewhere
    default: 
    {
      return unsupportedDeclaration( vh_decl );
    }
    }
  }
  return err_code;
}

bool sPackHasDefinitions(vhNode vh_pack)
{
  bool has_definitions = false;
  JaguarList vh_lst(vhGetDeclItemList(vh_pack));
  vhNode vh_node = NULL;
  while (!has_definitions && (vh_node = vhGetNextItem(vh_lst)))
  {
    vhObjType obj_typ = vhGetObjType(vh_node);
    if ((obj_typ == VHSIGNAL) || (obj_typ == VHVARIABLE)) 
       has_definitions = true;
    else if (obj_typ == VHCONSTANT)
    {
      vhNode typeDef = vhGetTypeDef(vh_node);
      if( vhGetObjType(typeDef) == VHPHYSICALTYPE)
      {
	// ignore physical types
      }
      else 
        has_definitions = true;
    }
  }
  return has_definitions;
}

// vh_pack_decl is the package declaration corresponding to the use item.
Populate::ErrorCode
VhdlPopulate::package(vhNode vh_pack_decl, LFContext* context,
                      const char* libName, const char* packName, bool* packIgnored)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator( vh_pack_decl );
  *packIgnored = true;
  
  StringAtom *packIdStr = createPackIdStr( libName, packName, context );
  NUNamedDeclarationScope *packScope = NULL;
  lookupPackage( packIdStr, &packScope, context );

  // Only create & populate the package if we haven't seen it before
  if ( packScope == NULL )
  {
    bool packHasDefinitions = sPackHasDefinitions(vh_pack_decl);
    if (!packHasDefinitions) {
      vhNode vh_pack_body = vhOpenPackBody(libName, packName);
      if (vh_pack_body != NULL) {
        packHasDefinitions = sPackHasDefinitions(vh_pack_body);
      }
    }
    // Ignore package if it's declaration or body has no signal/var/const definitions.
    if (packHasDefinitions)
    {
      temp_err_code = createPackageScope( vh_pack_decl, context, &packScope );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

      context->pushDeclarationScope( packScope );
      *packIgnored = false;
    }
  }

  // If we close the object we lose the scope pointer.
  //  vhCloseDesignUnit( vh_pack );
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::file(vhNode vh_decl, LFContext* context)
{
  // Process only if run with the switch -vhdlEnableFileIO.  We
  // treat VHDL file declarations just like file descriptors in
  // Verilog.
  
  if ( not mArg->getBoolValue( CarbonContext::scVhdlEnableFileIO ))
  {
    return eSuccess;
  }
 
  // Create a declaration population indicator. When populating
  // declarations evaluate expressions even when they're not static. In
  // declaration scope, there's no chance of variable/signal values being modified.
  // So their initial values can be used by declaration sizing/initializing.
  AggressiveEvalEnabler dpi(this);

  ErrorCode err_code = eSuccess, temp_err_code;
  NUScope *declaration_scope = context->getDeclarationScope();
  
  SourceLocator loc = locator(vh_decl);
  NUNet         *the_net = 0;
  StringAtom* name = context->vhdlIntern(vhGetName(vh_decl));
  ConstantRange *range = new ConstantRange(31, 0);
  
  NetFlags flags(eAllocatedNet);
  if (declaration_scope->getScopeType() != NUScope::eModule) {
    flags = NetFlags(flags | eBlockLocalNet);
  }
  flags = NetRedeclare(flags, eDMIntegerNet);
  the_net = new NUVectorNet( name, range, flags, VectorNetFlags (0), declaration_scope, loc );
  
  temp_err_code = mapNet(declaration_scope, vh_decl, the_net);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code) )
  {
    return temp_err_code;
  }
  if (the_net)
  {
    declaration_scope->addLocal (the_net);
  }
  // Initialize if fileName is provided.
  vhNode vh_file = vhGetFileLogicalName(vh_decl);
  if (vh_file)
  {
    NUExprVector expr_vector;
    NUExpr      *this_expr;
    NUModule    *module = context->getModule();
    StringAtom  *sym = module->gensym("file_open");

    int value = 0;
    UtString str_val;
    ErrorCode temp_err_code = getVhExprValue(static_cast<vhExpr>(vh_file),
                                             &value, &str_val);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
      return temp_err_code;

    UtString binary_str;
    UInt32 bit_width = composeBinStringFromString(str_val.c_str(), &binary_str, true);
    this_expr = NUConst::createKnownConst(binary_str, 
                                          bit_width, true, loc);
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);


    UtString mode_str;
    UInt32 mode = 0; // defaults to read mode
    vhNode open_kind_expr =   vhGetFileOpenKind(vh_decl);
    if (open_kind_expr)
    {
      mode = vhGetIdEnumLitValue(open_kind_expr);
    }
    else
    {
      vhPortType direction = vhGetFileOpenMode_87( vh_decl );
      switch ( direction )
      {
      case VH_IN:
      case VH_DEFAULT_IN:
        mode = 0;
        break;
      case VH_OUT:
        mode = 1;
        break;
      case VH_INOUT:
      case VH_BUFFER:
      case VH_LINKAGE:
      case VH_NOT_PORT:
      case VH_ERROR_PORTTYPE:
        POPULATE_FAILURE(mMsgContext->JaguarConsistency( &loc, "Unexpected VHDL-87 file mode specification" ), &err_code);
        return err_code;
        break;
      }
    }

    if (mode == 0)
    {
      // read mode
      bit_width = composeBinStringFromString("r", &mode_str);
    }
    else if (mode == 1)
    {
      // write mode
      bit_width = composeBinStringFromString("w", &mode_str);
    }
    else if (mode == 2)
    {
      // append mode
      bit_width = composeBinStringFromString("a", &mode_str);
    }
    this_expr = NUConst::createKnownConst(mode_str,
                                          bit_width, true, loc);
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);
   
    // Indicate population of declaration initialization statement outside
    // the current scope, in the module's initial block.
    {
      DeclInitIndicator dii(context);
      NUFOpenSysTask* sysFOpen = 
        context->stmtFactory()->createFOpenSysTask(sym,the_net, NULL, 
                                                   expr_vector, module,
                                                   false,
                                                   false /*isVerilogTask*/,
                                                   (mode == 0)/*useInFileSystem*/,
                                                   loc);
      // Create an initial block if none in the module & then add sysFOpen
      addStmtToInitialBlock(context, module, sysFOpen, the_net, loc);
    }
  }   
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::unsupportedDeclaration(vhNode vh_decl)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(vh_decl);
  UtString msgStr;
  msgStr << "Unsupported declarative item (" << gGetVhNodeDescription( vh_decl )
         << ") found.";
  POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, msgStr.c_str( )), &err_code);
  return err_code;
}

//! class AggressiveEvalEnabler
VhdlPopulate::AggressiveEvalEnabler::AggressiveEvalEnabler(VhdlPopulate* populator,
                                                           bool enable)
  : mPopulator(populator), mPreviousValue(false)
{
  mPreviousValue = mPopulator->mIsAggressiveEvalEna;
  mPopulator->mIsAggressiveEvalEna = enable;
}

VhdlPopulate::AggressiveEvalEnabler::~AggressiveEvalEnabler()
{
  mPopulator->mIsAggressiveEvalEna = mPreviousValue;
}

//! class DeclInitIndicator
VhdlPopulate::DeclInitIndicator::DeclInitIndicator(LFContext* context)
  : mContext(context)
{
  NUScope* decl_scope = mContext->getDeclarationScope();
  bool inTFHierScope = decl_scope->inTFHier();
  // The statements added for initializing declarations in non-task-function
  // scopes are added not in current context but in the module initial block.
  // Statement factory ought to not account for these statements in current context.
  if (!inTFHierScope) {
    mContext->stmtFactory()->outOfContextStmtsBegin();
  }
}

VhdlPopulate::DeclInitIndicator::~DeclInitIndicator()
{
  mContext->stmtFactory()->outOfContextStmtsEnd();
}

VhdlPopulate::FuncRetValCollector::FuncRetValCollector(VhdlPopulate* populator,
                                                       bool enable)
  : mPopulator(populator), mPrevFuncExprs(NULL)
{
  mPrevFuncExprs = mPopulator->mReturnExprs;
  // Supply the expression list when collection of return values is enabled. 
  // No expression list needed when disabled.
  if (enable) {
    mPopulator->mReturnExprs = &mFuncExprs;
  } else {
    mPopulator->mReturnExprs = NULL;
  }
}

VhdlPopulate::FuncRetValCollector::~FuncRetValCollector()
{
  mPopulator->mReturnExprs = mPrevFuncExprs;
}
