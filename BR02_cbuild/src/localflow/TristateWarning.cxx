// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "TristateWarning.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUEnabledDriver.h"
#include "flow/FLNode.h"
#include "util/CbuildMsgContext.h"


/*!
  \file
  Implementation of z/non-z driver conflicts.
 */


void TristateWarning::module(NUModule *this_module)
{
  NUNetList net_list;
  this_module->getAllTopNets(&net_list);

  for (NUNetListIter iter = net_list.begin(); iter != net_list.end(); ++iter) {
    detectZConflicts(*iter);
  }
}


void TristateWarning::detectZConflicts(NUNet *net)
{
  if (not net->isTriWritten()) {
    return;
  }

  for (NUNet::DriverLoop driver_loop = net->loopContinuousDrivers();
       not driver_loop.atEnd();
       ++driver_loop) {
    NUUseDefNode *driver = (*driver_loop)->getUseDefNode();

    // Skip over module instances; we will issue warnings for these, if they are
    // a problem, during aliasing.
    if ((driver != 0) and
	(dynamic_cast<NUModuleInstance*>(driver) == 0) and
	(dynamic_cast<NUEnabledDriver*>(driver) == 0)) {
      UtString buf;
      net->compose(&buf, NULL, true);   // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->NonZDriverForZNet(driver, buf.c_str());
    }
  }
}
