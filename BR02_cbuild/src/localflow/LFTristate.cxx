// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "LFTristate.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUStmtReplace.h"
#include "nucleus/NUStructuredProc.h"
#include "util/CbuildMsgContext.h"
#include "util/ConstantRange.h"
#include "util/DisjointRange.h"
#include "util/SetOps.h"

/*!
  \file
  Implementation of local tristate analysis.
 */

/* To-do list:

 - Dealing with feedback through shorted ports
 - handling z-assignements inside tasks.  Such cases exist in the
   interra suites, and probably inside customer tests.  We are
   getting wrong answers now (before & after this commit) and
   ignoring them due to xz-diff optimism.  I think the way to handle
   them is to inline them in TFRewrite.
 - Joe notes that we should deal with Z propagation within the always block
        module top(clk, out1, out2, in, ena);
          input clk, in, ena;
          output out1, out2;
          reg    out1, out2;

          always @(posedge clk) begin
            if (ena) begin
              out2 = in;
            end
            else
              out2 = 1'bz;
          end
          out1 = out2;
        endmodule
   In this case we want out1 to behave as a Z when out2 is a Z.  This
   is a related to bug4956.  I'm always trying to think of what you
   get when you make gates.  I'm ignoring for the moment that Joe has seen
   a DC spec that sez Z means 0 in an always block -- I can still see
   gates for many of these constructs.  In this one out1 and out2 are
   synonyms and could be handled with some sort of assignment aliasing.

   But if we have out1 conditionally assigned to out2 then
   it probably gets built with a MUX and in hardware, a Z would *not*
   propagate.  So we will have to think about what we really need to
   cover here.
*/


bool LFTristate::createEnabledDrivers(NUModule *this_module)
{
  NUEnabledDriverList enabled_drivers;
  NUContAssignList new_cont_assigns;
  NUContAssignList cont_assigns;
  this_module->getContAssigns(&cont_assigns);
  bool is_changed = false;
  for (NUContAssignListIter iter = cont_assigns.begin();
       iter != cont_assigns.end();
       ++iter) {
    is_changed |= contAssign(*iter, &new_cont_assigns, &enabled_drivers);
  }

  this_module->replaceContAssigns(new_cont_assigns);
  this_module->addContEnabledDrivers(&enabled_drivers);

  return is_changed;
}


bool LFTristate::resynthesizeZBlocks(NUModule *this_module) {
  bool is_changed = false;

  for (NUModule::AlwaysBlockLoop p = this_module->loopAlwaysBlocks();
       !p.atEnd(); ++p)
  {
    NUAlwaysBlock* always = *p;
    is_changed |= structuredProc(always);
  }

  for (NUModule::InitialBlockLoop p = this_module->loopInitialBlocks();
       !p.atEnd(); ++p)
  {
    NUInitialBlock* initial = *p;
    is_changed |= structuredProc(initial);
  }

  return is_changed;
}


// Chase thru an Lvalue tree to find the net we are writing into.
// This doesn't need to handle anything esoteric like a concat, as we
// can't handle tristating that unless the concat was split.
// Returns the net underneath the NUIdentRvalue, NUVarselLvalue or NUMemselLvalue.
//
static NUNet* sChaseLvalue (NULvalue* lval)
{
  if (lval->isWholeIdentifier ())
    return lval->getWholeIdentifier ();
  else if (NUVarselLvalue* v = dynamic_cast<NUVarselLvalue*>(lval))
    return sChaseLvalue (v->getLvalue ());
  else if (NUMemselLvalue* m = dynamic_cast<NUMemselLvalue*>(lval))
    return m->getIdent ();
  else
    NU_ASSERT (0, lval);        // Not happy with concat etc.

  return 0;
}

//! Process a continuous assign, breaking it out into one or more
//! enabled drivers and smaller continuous assign
bool LFTristate::contAssign(NUContAssign *assign,
                            NUContAssignList *assign_list,
                            NUEnabledDriverList *drivers_list)
{
  // Detect if a Z exists on the RHS.  If not, analysis is not needed.
  NUExpr *rhs = assign->getRvalue();
  if (not rhs->drivesZ()) {
    assign_list->push_back(assign);
    return false;               // no change made
  }

  // Make sure the LHS is a simple net, a partsel or a bitsel
  NUVarselLvalue *ps=0;
  NUNet *net;
  NULvalue *lhs = assign->getLvalue();

  if (not lhs->isWholeIdentifier()
      && not (ps = dynamic_cast<NUVarselLvalue *>(lhs))) {

    mMsgContext->UnsupportedZLhs(lhs);
    assign_list->push_back(assign);
    return false;
  }


  // Look at the underlying destination net - Memories are never
  // tristated, so exclude them up front.

  if (ps)
    net = sChaseLvalue (ps);
  else
    net = lhs->getWholeIdentifier();

  if (net && net->is2DAnything ())
  {
    mMsgContext->UnsupportedZLhs (lhs);
    assign_list->push_back (assign);
    return false;
  }

  NUNetSet defs;
  assign->getDefs(&defs);

  NUEnabledDriver *enabled_driver = 0;
  bool ok = assign->makeEnabledDriver(&enabled_driver);
  if (not ok) {
    mMsgContext->UnsupportedZExpr(assign->getRvalue());
    assign_list->push_back(assign);
    return false;
  }

  // A pure Z driver can be discarded entirely
  if (enabled_driver != 0) {
    drivers_list->push_back(enabled_driver);
  }

  // Mark the def nets as tristatable.
  NUConst* rhsK = rhs->castConst();
  bool constZ = ((rhsK != NULL) && rhsK->drivesZ());
  for (NUNetSetIter defs_iter = defs.begin();
       defs_iter != defs.end();
       ++defs_iter) {
    NUNet *def_net = *defs_iter;
    markTristateNet(def_net);
    if (constZ)
      def_net->putIsConstZ(true);
  }

  delete assign;

  return true;
}

void LFTristate::markTristateNet(NUNet* znet) {
  NU_ASSERT (not znet->is2DAnything (), znet);
  znet->putIsTriWritten(true); // Probably futile - gets recomputed
  if (znet->isPrimaryBid() or znet->isPrimaryOutput()) {
    znet->putIsPrimaryZ(true);
  }
}

//! helper class to find nets assigned to Z and collect a replacement
//! map to convert conditional expressions involving Z to if-then-else
//! statements with assign to constant Z.
class LFTristate::ZNetConvertCallback : public NUDesignCallback {
public:
  //! constructor.
  ZNetConvertCallback(NUNetSet* tristateNets, NUNetSet* usedNets,
                      MsgContext* mc, NUNetRefFactory* nrf)
    : mTristateNets(tristateNets),
      mUsedNets(usedNets),
      mMsgContext(mc),
      mNetRefFactory(nrf),
      mDoReplace(false),
      mStructuredProc(NULL)
  {}

  //! destructor
  ~ZNetConvertCallback() {}

  Status operator()(Phase phase, NUStructuredProc* proc) {
    if (phase == ePre) {
      mStructuredProc = proc;
    }
    else {
      mStructuredProc = NULL;
    }
    return eNormal;
  }

  void procAssign(NUAssign* assign, NUNetSet& assignNets) {
    NUExpr* rhs = assign->getRvalue();
    if (rhs->drivesZ()) {
      // There should be exactly one net
      NULvalue* lvalue = assign->getLvalue();
      if (assignNets.size() != 1) {
        mMsgContext->UnsupportedZLhs(lvalue);
        return;
      }
      NUNet* tristateNet = *assignNets.begin();
      if (tristateNet->is2DAnything()) {
        mMsgContext->UnsupportedZLhs(lvalue);
        return;
      }

      // We can only tolerate pure constant Z on the RHS.
      // BreakZConsts should have made this pure.
      if ( not rhs->drivesOnlyZ() ) {
        // The user might have done out=ena?in:1'bz in an
        // structuredProc block.  See if we can isolate it.  If so,
        // let's take this opportunity to make the transform
        // into an if-statement right here.
        NUExpr* enable, *driver;
        if (rhs->isolateZ(&enable, &driver)
            && (enable != NULL) && (driver != NULL))
        {
          convertToIf(assign, enable, driver);
          declareZ(tristateNet);
        }
        else {
          mMsgContext->UnsupportedZExpr(rhs);
        }
        return;
      }

      declareZ(tristateNet);
    } // if
  } // void procAssign

  Status operator()(Phase phase, NUBlockingAssign* assign) {
    if (phase == ePre) {
      NUNetSet defs;
      assign->getBlockingDefs(&defs);
      procAssign(assign, defs);
    }
    return eNormal;
  } // Status operator

  Status operator()(Phase phase, NUNonBlockingAssign* assign) {
    if (phase == ePre) {
      NUNetSet defs;
      assign->getNonBlockingDefs(&defs);
      procAssign(assign, defs);
    }
    return eNormal;
  } // Status operator

  void declareZ(NUNet* tristateNet) {
    (void) mTristateNets->insertWithCheck(tristateNet);
  }

  //! Take an assignment that's had Z-isolation performed on it, yielding
  //!         out = ena ? in : 1'bz;
  //! and create an equivalent if-statement
  //!         if (ena)
  //!           out = in;
  //!         else
  //!           out = 1'bz;
  //! as that is the general form that we have to attack when handling
  //! Z assignments in always blocks.
  void convertToIf(NUAssign* assign, NUExpr* enable, NUExpr* driver) {
    NUStmtList thenStmts, elseStmts;

    // Construct the 'then' statement, assigning lvalue to driver
    CopyContext cc(NULL, NULL);
    NULvalue* lvalue = assign->getLvalue();
    NUStmt* thenAssign = assign->create(lvalue->copy(cc), driver, NULL, NULL);
    thenStmts.push_back(thenAssign);

    // Construct the else statement (assigning lvalue to Z)
    UInt32 width = driver->getBitSize();
    DynBitVector zeros(width), ones(width);
    ones.set();
    const SourceLocator& loc = driver->getLoc();
    NUExpr* z = NUConst::createXZ(false, zeros, ones, width, loc);
    NUStmt* elseAssign = assign->create(lvalue->copy(cc), z, NULL, NULL);
    elseStmts.push_back(elseAssign);

    // Construct the if-stmt to replace the conditional expression assign
    NUIf* ifStmt = new NUIf(enable, thenStmts, elseStmts, mNetRefFactory,
                            false, loc);
    mReplacements.addReplacement(assign, ifStmt);
    mDoReplace = true;
  } // void convertToIf

  //! collect the uses (UD may not be valid at this point)
  Status operator()(Phase phase, NUIdentRvalue* rv) {
    if (phase == ePre) {
      mUsedNets->insert(rv->getIdent());
    }
    return eSkip;
  }

  Status operator()(Phase, NUBase*) {return eNormal;}

  //! Get the statement replacements accumulated to help convert 
  //! conditional assigns to Z into if-then-else
  NUStmtReplaceCallback& getReplacements() {return mReplacements;}

  //! Return true if any conditional-assigns-to-Z were found, requiring
  //! another block-walk to perform the replacements using mReplacements
  bool doReplace() const {return mDoReplace;}

private:
  NUNetSet* mTristateNets;
  NUNetSet* mUsedNets;
  MsgContext* mMsgContext;
  NUNetRefFactory* mNetRefFactory;
  bool mDoReplace;
  NUStmtReplaceCallback mReplacements;
  NUStructuredProc* mStructuredProc;
}; // class LFTristate::ZNetConvertCallback : public NUDesignCallback

//! class to find all the parts of nets that are assigned to Z
/*!
 *! It would seem natural to use an NUNetRefSet for this, but
 *! we actually don't want to merge together disjoint ranges.
 *! We need to know exactly which parts are individually assigned
 *! So we will employ a Map<NUNet*,Vector<ConstantRange>>
 */
class LFTristate::TristateDriverCallback : public NUDesignCallback {
public:
  //! constructor.
  TristateDriverCallback(const NUNetSet& tristateNets,
                         NetAssignRangeVecMap* netAssignRangeVecMap,
                         NUNetRefFactory* nrf,
                         MsgContext* msgContext)
    : mTristateNets(tristateNets),
      mNetAssignRangeVecMap(netAssignRangeVecMap),
      mNetRefFactory(nrf),
      mMsgContext(msgContext)
  {}

  //! destructor
  ~TristateDriverCallback() {}

  void procAssign(NUAssign* assign, const NUNetRefSet& defs) {
    NUExpr* rhs = assign->getRvalue();
    bool isZ = rhs->drivesZ();
    if (isZ) {
      // If it's Z, we really want it to be constant Z
      // BreakZConsts should have made this pure.
      if ( not rhs->drivesOnlyZ() ) {
        mMsgContext->UnsupportedZExpr(rhs);
        return;
      }
    }

    for (NUNetRefSet::const_iterator p = defs.begin(), e = defs.end();
         p != e; ++p)
    {
      NUNetRefHdl ref = *p;
      NUNet* assignNet = ref->getNet();
      if (mTristateNets.find(assignNet) != mTristateNets.end()) {
        // We are depending on concat-rewrite to eliminate concat-lvalues.
        // They are very tough for us to resynthesize.  Alert the user
        // if we see one.  We do this assert inside the test for finding
        // a tristate net because we don't mind if there are concat-lvalues
        // to nets that are not assigned to Z.
        if (defs.size() > 1) {
          mMsgContext->UnsupportedZLhs(assign->getLvalue());
          return;
        }

        for (NUNetRefRangeLoop q = ref->loopRanges(mNetRefFactory);
             !q.atEnd(); ++q)
        {
          const ConstantRange& range = *q;
          addNetRange(assign, assignNet, range);
        }
      }
    }
  }

  Status operator()(Phase phase, NUBlockingAssign* assign) {
    if (phase == ePre) {
      NUNetRefSet defs(mNetRefFactory);
      assign->getBlockingDefs(&defs);
      procAssign(assign, defs);
    } // if
    return eSkip;
  }
  Status operator()(Phase phase, NUNonBlockingAssign* assign) {
    if (phase == ePre) {
      NUNetRefSet defs(mNetRefFactory);
      assign->getNonBlockingDefs(&defs);
      procAssign(assign, defs);
    } // if
    return eSkip;
  }
  Status operator()(Phase, NUBase*) {
    return eNormal;
  }

private:
  void addNetRange(NUAssign* assign, NUNet* net, const ConstantRange& range) {
    AssignRangeVec& arv = (*mNetAssignRangeVecMap)[net];
    arv.push_back(AssignRange(assign, range));
  }

  const NUNetSet& mTristateNets;
  NetAssignRangeVecMap* mNetAssignRangeVecMap;
  NUNetRefFactory* mNetRefFactory;
  MsgContext* mMsgContext;
}; // class LFTristate::TristateDriverCallback : public NUDesignCallback

//! Helper class to find uses of tristateEnable nets and replace them with
//! their current drive value (which is initialized at the top of the module
class LFTristate::ReplaceTriNetUses : public NuToNuFn {
public:

  //! ctor
  ReplaceTriNetUses(const NUNetReplacementMap& netRepMap)
    : mNetRepMap(netRepMap)
  {}

  //! dtor
  ~ReplaceTriNetUses() {}

  //! user-defined method to translate an expression
  /*! Note that if this method finds a translation for the NUExpr*, it
   *! must delete the one passed in to avoid a memory leak
   */
  virtual NUExpr * operator()(NUExpr * expr, Phase phase) {
    NUExpr* ret = NULL;
    if (isPost(phase)) {
      NUIdentRvalue* rv = dynamic_cast<NUIdentRvalue*>(expr);
      if (rv != NULL) {
        NUNet* net = rv->getIdent();
        NUNetReplacementMap::const_iterator p = mNetRepMap.find(net);
        if (p != mNetRepMap.end()) {
          NUNet* rep = p->second;
          const SourceLocator& loc = rv->getLoc();
          ret = new NUIdentRvalue(rep, loc);
          delete rv;
        }
      }
    }
    return ret;
  }
  
private:
  const NUNetReplacementMap& mNetRepMap;
}; // class LFTristate::ReplaceTriNetUses : public NuToNuFn

// Process an structuredProc block, looking for nets that can be driven
// by Z, and constructing a continuous enabled driver to reflect
// the drive-status of the structuredProc block
bool LFTristate::structuredProc(NUStructuredProc *structuredProc) {
  bool workDone = false;

  // Find the tristate nets
  NUBlock* blockStmts = structuredProc->getBlock();
  NUNetSet tristateNets;
  UseBeforeKill useBeforeKill(mNetRefFactory);
  UseBeforeKill::NetToLoc usedBeforeKillMap;

  {
    NUNetSet usedNets, usedTristateNets;
    findTristateNets(structuredProc, &tristateNets, &usedNets);
    set_intersection(usedNets, tristateNets, &usedTristateNets);

    // See if any of these are used before they are killed in the block
    useBeforeKill.block(blockStmts);
    useBeforeKill.intersectNetLocs(&usedBeforeKillMap, usedTristateNets);
  }

  // Now that we know which nets are tristated, do another structuredProc-block
  // design-walk to find all the assigns (Z and non-Z) that def those
  // nets, keeping track of all the ConstantRanges being def'd by each
  // assign
  NetAssignRangeVecMap netAssignRangeVecMap;
  findNetAssignments(structuredProc, tristateNets, &netAssignRangeVecMap);

  NU_ASSERT(netAssignRangeVecMap.size() == tristateNets.size(), structuredProc);

  // Collect all the statement substitutions and replace them after
  // all the resynthesis is done, thus avoiding hitting some freed
  // memory
  NUStmtReplaceCallback replacements;

  // Collect all the net substitutions for users of the tristate nets.
  // They should be replaced with the drive value
  NUNetReplacementMap tristateToDriveVal;

  NUStmtList initializeDrive;

  // Foreach tristate net, prepare to rewrite the assigns by building
  // an NUStmtStmtMap.
  //    - Assigns to Z are replaced with a varsel assign of 0 to an enable-net.
  //    - Assigns to non-Z get replaced with an NUBlock with two statements,
  //      an varsel assign of 1 to an enable-net and the value assignment.
  //      Downstream calls to Fold will remove the extra levels of NUBlock,
  //      and PackAssigns will merge together the bits of enable nets assigned
  //      in the same StmtList.
  //
  for (NUNetSet::SortedLoop p = tristateNets.loopSorted(); !p.atEnd(); ++p) {
    NUNet* triNet = *p;
    AssignRangeVec& assignRangeVec = netAssignRangeVecMap[triNet];
    if (handleZOnly(structuredProc, assignRangeVec, &replacements)) {
      workDone = true;
    }
    else {
      DisjointRange enaRanges;
      for (UInt32 i = 0, n = assignRangeVec.size(); i < n; ++i) {
        AssignRange& ar = assignRangeVec[i];
        enaRanges.insert(ar.second.getLsb(), ar.second.getMsb());
        workDone = true;
      }

      resynthTriDrivers(structuredProc, triNet, assignRangeVec, enaRanges,
                        &replacements, &tristateToDriveVal, &initializeDrive,
                        usedBeforeKillMap);
    }
  }

  // Now that we've built the replacement map for all tristate nets,
  // fix up the structuredProc block
  {
    NUDesignWalker walker(replacements, false, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.structuredProc(structuredProc);
  }

  {
    ReplaceTriNetUses replaceTriNetUses(tristateToDriveVal);
    structuredProc->replaceLeaves(replaceTriNetUses);
  }

  for (NUStmtList::iterator p = initializeDrive.begin(),
         e = initializeDrive.end(); p != e; ++p)
  {
    NUStmt* init = *p;
    blockStmts->addStmtStart(init);
  }
  return workDone;
} // bool LFTristate::structuredProc

void LFTristate::findTristateNets(NUStructuredProc* structuredProc,
                                  NUNetSet* tristateNets,
                                  NUNetSet* usedNets)
{
  ZNetConvertCallback triFinder(tristateNets, usedNets, mMsgContext,
                                mNetRefFactory);
  NUDesignWalker walker(triFinder, false, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.structuredProc(structuredProc);

#if 0
  // Check to see if any of the tristate nets are used in the block.
  // This can be a problem in sequential always blocks which will not
  // run till they settles.  If so, report Alerts and continue on.

  // This is ifdef'd out because it causes too many problems in customer
  // testcases, and I currently think we handle this as well as can be
  // expected of a 2-state engine.
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(structuredProc);
  if ((always != NULL) && always->isSequential()) {
    NUNetSet intersection;
    set_intersection(usedNets, *tristateNets, &intersection);
    for (NUNetSet::SortedLoop p = intersection.loopSorted(); !p.atEnd(); ++p) {
      NUNet* net = *p;
      mMsgContext->LFAlwaysTriUse(net);
    }
  }
#endif

  // If we found any conditional expressions along the way and wish
  // to convert them to if/then/else then do it now
  if (triFinder.doReplace()) {
    NUDesignWalker repWalker(triFinder.getReplacements(), false, false);

    // We really should handle Z writes from inside functions.  But
    // we don't handle it properly as of 9/12/05, and the change to
    // handle Z assigns from always-blocks should not be blocked
    // by this issue.  test/interra/three_st/TRI1 is an example, which
    // today compiles properly and gives (I think) wrong answers.
    repWalker.putWalkTasksFromTaskEnables(false);
    repWalker.structuredProc(structuredProc);
  }
}

void LFTristate::findNetAssignments(NUStructuredProc* structuredProc,
                                    const NUNetSet& tristateNets,
                                    NetAssignRangeVecMap* netAssignRangeVecMap)
{
  TristateDriverCallback driverFinder(tristateNets, netAssignRangeVecMap,
                                      mNetRefFactory, mMsgContext);
  NUDesignWalker walker(driverFinder, false, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.structuredProc(structuredProc);
}

// if there is no assignment in the set that assigns
// any bits of 'enable' to 1, then
// we can suppress the generation of the enabled driver -- just
// let the temp enable net be unloaded and dead-logic will get rid
// of it.
//
// Note that there should always be a Z driver because otherwise we
// would not have identified the net as a tristate net
bool LFTristate::handleZOnly(NUStructuredProc* structuredProc,
                             const AssignRangeVec& assignRangeVec,
                             NUStmtReplaceCallback* replacements)
{
  NUScope* scope = structuredProc->getBlock();

  // if only Z is written to the net, then just declare the net
  // as a constant Z in the IODB (if it's not depositable or
  // forcible) and get rid of the blocking assignments.
  bool onlyZDrivers = true;
  for (UInt32 i = 0, n = assignRangeVec.size(); (i < n) && onlyZDrivers; ++i) {
    NUAssign* assign = assignRangeVec[i].first;
    NUExpr* rhs = assign->getRvalue();
    onlyZDrivers = rhs->drivesZ();
  }
  if (onlyZDrivers) {
    for (UInt32 i = 0, n = assignRangeVec.size(); (i < n) && onlyZDrivers; ++i) {
      NUAssign* assign = assignRangeVec[i].first;
      const SourceLocator& loc = assign->getLoc();
      NUStmtList empty;
      NUBlock* block = new NUBlock(&empty, scope, mIODB, mNetRefFactory,
                                   false, loc);
      replacements->addReplacement(assign, block);
    }
  }
  return onlyZDrivers;
} // bool LFTristate::handleZOnly

/*
 * There is a significant chunk of code in here that implements a
 * drive/enable resynthesis approach to handling always-blocks with
 * Z drivers.  It analyzes an always-block, and for each tristate
 * net, it inserts statements that maintain the drive and enable values
 * as distinct nets.
 *
 * This approach has certain benefits and certain drawbacks.  A benefit
 * is that it can handle latches and flops with tristate outputs, by
 * separating the flop/latch from enabled-driver.  This works nicely
 * with the existing scheduler, RETriInit, and codegen capabilities of
 * cbuild.
 *
 * The drawback is that the resynthesis process is hard in the presence
 * of consumers of an always-block tristate net within the always block.
 * Also, tasks are not handled well by the resynthesis approach.
 *
 * So the approach being pursued right now is much simpler -- remove
 * the Z drivers and mark the net tristate.  This means, as of today,
 * latches/flops with tristate enables don't work, but that can be
 * potentially be addressed with scheduler enhancements.
 *
 * However, in case we decide to go back to resynthesis, I don't want
 * to delete this code yet, so it's ifdef'd out.  I will delete it if
 * it looks like the scheduler approach pans out.
 */
#define SEPARATE_ENA_NET 1
#define SEPARATE_DRIVE_NET 1



void LFTristate::resynthTriDrivers(NUStructuredProc* structuredProc,
                                   NUNet* triNet, 
                                   const AssignRangeVec& assignRangeVec,
                                   const DisjointRange& enaRanges,
                                   NUStmtReplaceCallback* replacements,
                                   NUNetReplacementMap* tristateToDriveVal,
                                   NUStmtList* initializeDrive,
                                   const UseBeforeKill::NetToLoc& usedBeforeKillMap

  )
{
  // Create the drive and enable nets
  CopyContext xlateDrive(NULL, NULL, true);
  NUBlock* scope = structuredProc->getBlock();
  const SourceLocator& loc = scope->getLoc();
  NUNet* ena = NULL;

  // The temp-nets must be created as module scoped, so that if
  // the triNet isn't killed, the previous enable-bits are treated
  // as latches and retained
  NUModule* mod = scope->getModule();
  NUNet* drv = NULL;

  StringAtom* enaName = scope->gensym("ena", NULL, triNet);
  UInt32 numEnaBits = enaRanges.numSlots();

  ena = mod->createTempNet(enaName, numEnaBits, false, loc);
  drv = mod->createTempNetFromImage(triNet, "drive");

  // Walk through the assigments to this tristate and see if they are
  // blocking, non-blocking, or both.  We should only replace the uses
  // of the tristate nets when they are assigned with blocking assigns.
  // Non-blocking assigns should see the resolved value, so we can
  // leave the uses alone.
  bool hasBlockingAssign = false;
  bool hasNonBlockingAssign = false;
  for (UInt32 i = 0, n = assignRangeVec.size(); i < n; ++i) {
    const AssignRange& ar = assignRangeVec[i];
    NUAssign* assign = ar.first;
    if (dynamic_cast<NUBlockingAssign*>(assign) != NULL) {
      hasBlockingAssign = true;
    }
    else {
      NU_ASSERT(dynamic_cast<NUNonBlockingAssign*>(assign) != NULL, assign);
      hasNonBlockingAssign = true;
    }
  }

  if (hasNonBlockingAssign && hasBlockingAssign) {
    // No need to error out here because it's done in Reorder.
  }

  // If the block contains any statements that use the tristate net,
  // then Create a driver at the beginning of structuredProc to
  // initialize the drive value to the previous resolved value.  See
  // test/bidi/use_z_comb_resolve.v.  This will get 0 vs 1 mismatches
  // unless we do this.  Note that this is potententially a real cycle,
  // and potentially real trouble.
  if (hasBlockingAssign) {
    UseBeforeKill::NetToLoc::const_iterator kill_iter =
      usedBeforeKillMap.find(triNet);
    if (kill_iter != usedBeforeKillMap.end()) {
      // For flops, we can initialize the 'drive' image to the resolved
      // signal, without fear of creating a cycle
      if (structuredProc->isSequential()) {
        NULvalue* lvDrive = new NUIdentLvalue(drv, loc);
        NUExpr* prevResolved = new NUIdentRvalue(triNet, loc);
        NUStmt* stmt = new NUBlockingAssign(lvDrive, prevResolved, false, loc);
        initializeDrive->push_back(stmt);
      }

      // for combinational blocks, we can't insert that initializer from
      // the resolved value, or we will get cycles and wrong answers, and
      // we don't like either of those.  Issue an alert.
      else {
        const SourceLocator* use_before_kill_loc = kill_iter->second;
        mMsgContext->UnsupportedZRead(use_before_kill_loc);
      }
    }
  } // if

  // Translate all assigns to triNet into assigns to drv
  xlateDrive.mNetReplacements[triNet] = drv;
  if (hasBlockingAssign) {
    (*tristateToDriveVal)[triNet] = drv;
  }

  createReplacementStmts(scope, replacements, xlateDrive, ena, enaRanges,
                         assignRangeVec);

  if (! triNet->isBlockLocal()) {
    // For each bit of the enable-vector, generate an enabled driver
    UInt32 enableIndex = 0;
    NUEnabledDriverList enaDrvList;

    for (DisjointRange::SlotCLoop p = enaRanges.loopSlots(); !p.atEnd(); ++p)
    {
      DisjointRange::Slot* slot = p.getValue();
      const ConstantRange& range = slot->getRange();
      NULvalue* lval = NULL;
      NUExpr* idrv = NULL;
      UInt32 rangeSize = range.getLength();
      if (rangeSize == triNet->getBitSize()) {
        lval = new NUIdentLvalue(triNet, loc);
        idrv = new NUIdentRvalue(drv, loc);
      }
      else {
        lval = new NUVarselLvalue(triNet, range, loc);
        idrv = new NUVarselRvalue(drv, range, loc);
      }
      idrv->resize(rangeSize);
      NUExpr* iena = NULL;
      if (ena->getBitSize() == 1) {
        NU_ASSERT(enableIndex == 0, triNet);
        iena = new NUIdentRvalue(ena, loc);
      }
      else {
        iena = new NUVarselRvalue(ena,
                                  ConstantRange(enableIndex, enableIndex),
                                  loc);
      }
      iena->resize(1);

      // For sequential blocks, we must separate the tristate element from
      // the flop, like a synthesis tool would do.
      StringAtom* sym = scope->gensym("always_z_driv", NULL, triNet);
      DynBitVector val(rangeSize), drv(rangeSize);
      drv.flip();
      NUExpr* z = NUConst::createXZ(false, val, drv, rangeSize, loc);
      NUExpr* enaExpr = new NUTernaryOp(NUOp::eTeCond, iena, idrv, z, loc);
      NUContAssign* assign = new NUContAssign(sym, lval, enaExpr, loc);
      mod->addContAssign(assign);

      ++enableIndex;
    } // for
    mod->addContEnabledDrivers(&enaDrvList);
    markTristateNet(triNet);
  } // if
}


void LFTristate::createReplacementStmts(NUScope* scope,
                                        NUStmtReplaceCallback* replacements,
                                        CopyContext& xlateDrive,
                                        NUNet* ena,
                                        const DisjointRange& enaRanges,
                                        const AssignRangeVec& assignRangeVec)
{
  // Transform:
  //     reg [7:0] out;
  //     always @*
  //       if (cond) begin
  //         out[3:0] = 4'bzzzz;
  //         out[7:4] = 4'b0;
  //       end
  // into
  //      reg [7:0] $drive_out;
  //      wire [7:0] out;
  //      reg [1:0] $ena_out;  // 1->[7:4], 2->[3:0]
  //      always @(*)
  //        if (cond1) begin
  //         begin
  //           $ena_out[0] = 1'b0;  // out[3:0]=4'bzzzz
  //         end
  //         begin
  //           $drive_out[7:4] = 4'b0;
  //           $ena_out[1] = 1'b1;
  //         end
  //       end
  //       assign out[7:4] = $ena_out[1] ? $drive_out[7:4] : 4'bz;
  //       assign out[3:0] = $ena_out[0] ? $drive_out[3:0] : 4'bz;
  //

  for (UInt32 i = 0, n = assignRangeVec.size(); i < n; ++i) {
    NUAssign* assign = assignRangeVec[i].first;
    const SourceLocator& loc = assign->getLoc();
    const ConstantRange& range = assignRangeVec[i].second;
    ConstantRange enaRange;
    NU_ASSERT(enaRanges.construct(range, &enaRange), assign);

    NUExpr* rhs = assign->getRvalue();
    bool isZ = rhs->drivesZ();

    UInt32 enaWidth = enaRange.getLength();
    NULvalue* enaLvalue = ena? new NUVarselLvalue(ena, enaRange, loc): 0;

    if (isZ) {
      if (ena != NULL) {
        // Replace Z with an a blocking assign of 'ena' to 0
        NUExpr* rval = NUConst::create(false, 0, enaWidth, loc);
        NUAssign* clrEna = assign->create(enaLvalue, rval, NULL, NULL);
        replacements->addReplacement(assign, clrEna);
      }
      else {
        NUStmtList empty;
        NUBlock* block = new NUBlock(&empty, scope, mIODB, mNetRefFactory,
                                     false, loc);
        replacements->addReplacement(assign, block);
      }
    }
    else if (ena != NULL) {
      // Replace non-Z assigns with a pair of statements to set
      // the enable bits and do the assign
      DynBitVector ones(enaWidth);
      ones.set();
      NUExpr* rval = NUConst::create(false, ones, enaWidth, loc);
      NUAssign* setEna = assign->create(enaLvalue, rval, NULL, NULL);
      NUStmt* drvAssign = assign->copy(xlateDrive);
      NUStmtList stmtList;
      stmtList.push_back(setEna);
      stmtList.push_back(drvAssign);
      NUBlock* block = new NUBlock(&stmtList, scope, mIODB, mNetRefFactory,
                                   false, loc);
      replacements->addReplacement(assign, block);
    }
  }
} // void LFTristate::createReplacementStmts
