// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/DesignPopulate.h"
#include "LFContext.h"
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUModule.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"

extern bool
isUnconstrainedArray( vhNode ); // from VhPopulateExpr.cxx

Populate::ErrorCode VhdlPopulate::identLvalue(vhNode vh_named,
                                              LFContext* context,
                                              NULvalue **the_lvalue,
                                              const bool /*isAliasDecl*/)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(vh_named);

  NUNet* net = 0;
  ErrorCode temp_err_code;
  if (VHOBJECT == vhGetObjType(vh_named))
    vh_named = vhGetActualObj(vh_named);

  temp_err_code = lookupNet( context->getDeclarationScope(), vh_named,
                             context, &net );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    return temp_err_code;

  // Either this is not an alias, or after resolving alias the actual
  // turned out to be a signal or variable.
  if ( net->isCompositeNet( ))
    *the_lvalue = new NUCompositeIdentLvalue( net->getCompositeNet(), loc );
  else
    *the_lvalue = new NUIdentLvalue( net, loc );
  return err_code;
}


Populate::ErrorCode VhdlPopulate::lvalue(vhNode vh_lvalue,
                                         LFContext* context,
                                         NULvalue **the_lvalue,
                                         bool isAliasDecl)
{
  NUModule *module = context->getModule();
  *the_lvalue = 0;
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator( vh_lvalue );

  switch (vhGetObjType(vh_lvalue)) 
  {
  case VHOBJECT:
  {
    vhNode actualObj = vhGetActualObj(vh_lvalue);
    return lvalue(actualObj, context, the_lvalue, isAliasDecl);
    break;
  }
  case VHSIGNAL:
  case VHVARIABLE:
  case VHELEMENTDECL:
  case VHCONSTANT:    // a constant lvalue can happen with a VHDL alias of a constant
    return identLvalue(vh_lvalue, context, the_lvalue, isAliasDecl);
    break;  
  case VHSELECTEDNAME:
  {
    return selectedNameLvalue( vh_lvalue, context, the_lvalue, isAliasDecl );
    break;
  }
  case VHINDNAME: 
    return indexedNameLvalue(vh_lvalue, context, the_lvalue);
    break;
  case VHSLICENODE: 
  case VHFUNCNODE:
  case VHASSOCIATION:
  {
    vhNode actualObj = vhGetActual( vh_lvalue );
    if (VHOPEN != vhGetObjType(actualObj))
    {
      return lvalue(actualObj, context, the_lvalue, isAliasDecl);
      break;
    }
    // else fall thru for the handling of the OPEN nets
  }
  case VHOPEN:
  {
    vhNode vh_slice_or_type = NULL;
    if (VHOPEN == vhGetObjType(vh_lvalue))
    {
      // Case when the formal is indexed name and connected as:
      // formal(1) => OPEN
      vh_slice_or_type = vhGetExpressionType(static_cast<vhExpr>(vh_lvalue));
      if ( isUnconstrainedArray( vh_slice_or_type ))
      {
        mMsgContext->JaguarConsistency( &loc, "Unable to determine type information for Open actual parameter." );
        return eFatal;
      }
    }
    else
    {
      // Case when the formal is slice name and connected as:
      // formal(1 to 2) => OPEN . 
      vh_slice_or_type = vhGetFormal(vh_lvalue);
    }
    if ( vh_slice_or_type == NULL )
    {
      mMsgContext->JaguarConsistency( &loc, "Cannot find expression associated with open port connection" );
      return eFatal;
    }
    StringAtom *name = module->gensym( "open", "net" );
    NetFlags flags = eDMWireNet;
    NUNet *temp_net = NULL;
    ErrorCode temp_err_code = allocNet(vh_slice_or_type, name, flags,
                                       module, &temp_net, context );
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    module->addLocal(temp_net);
    SourceLocator loc = locator( vh_lvalue );
    *the_lvalue = new NUIdentLvalue(temp_net, loc);
    return temp_err_code;
  }
  break;
  case VHSLICENAME: 
    return sliceNameLvalue(vh_lvalue, context, (NULvalue**)the_lvalue);
    break;
  case VHAGGREGATE: 
    return aggregateLvalue(static_cast<vhExpr>(vh_lvalue), context,
                           (NUConcatLvalue**)the_lvalue);
    break;
  default:
  {
    SourceLocator loc = locator(vh_lvalue);
    UtString msgStr;
    msgStr << "Unexpected l-value node type (" << gGetVhNodeDescription( vh_lvalue )
           << ").";
    mMsgContext->JaguarConsistency( &loc, msgStr.c_str( ));
  }
  break;
  }

  return eFailPopulate;
}

Populate::ErrorCode
VhdlPopulate::indexedNameLvalue(vhNode vh_lvalue,
                                LFContext* context,
                                NULvalue **the_lvalue)
{
  *the_lvalue = 0;
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator( vh_lvalue );

  JaguarList indexList( vhGetExprList( static_cast<vhExpr>( vh_lvalue )));
  NUExprVector indexVector;
  vhExpr vh_index;
  NUExpr *index = NULL;
  while (( vh_index = static_cast<vhExpr>( vhGetNextItem( indexList ))))
  {
    temp_err_code = expr( vh_index, context, 0, &index );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    index->resize( index->determineBitSize( ));
    maybeAdjustIndex( vh_index, &index, loc );
    indexVector.push_back( index );
  }
  const UInt32 numIndices = indexList.size();

  NUNet *net = NULL;
//   bool isAliased;
  vhExpr vh_named = vhGetPrefix(static_cast<vhExpr>(vh_lvalue));
//   vhObjType expr_type = vhGetObjType( vh_named );
//   if ( expr_type == VHOBJECT ) {
//     vh_named = static_cast<vhExpr>( vhGetActualObj( vh_named ));
//     expr_type = vhGetObjType( vh_named );
//   }
//   if ( expr_type == VHSELECTEDNAME )
//   {
//     vhNode vh_selname_parent_obj = vhGetActualObj(vh_named);
//     isAliased = isAliasObject(vh_selname_parent_obj);
//   }
//   else
//   {
//     isAliased = isAliasObject(vh_named);
//   }

//   if (isAliased)
//   {
//     // TJM All the alias handling code assumes that numIndices < 3.
//     // This needs to be rewritten to handle multidim aliases.
//     LOC_ASSERT( numIndices < 3, loc );
//     vhNode range = NULL;
//     vhNode dummy_range = NULL;
//     temp_err_code = getVhRangeNode(vh_named, &range, &dummy_range);
//     if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
//     {
//       return temp_err_code;
//     }
//     VhdlRangeInfo aliasBounds (0,0);
//     temp_err_code = getLeftAndRightBounds(range, &aliasBounds, context );
//     if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
//     {
//       return temp_err_code;
//     }
//     VhdlRangeInfo actualBounds (0,0);
//     temp_err_code = resolveAlias( vh_named, &aliasBounds, &actualBounds,
//                                   context, &net );
//     if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
//     {
//       return temp_err_code;
//     }
//     // Map the index of the alias into the actual's range.
//     // The result is returned in the modified indexVector[0].
//     temp_err_code = resolveIndexAliasBitSel( &indexVector[0], vh_lvalue,
//                                              aliasBounds, actualBounds, loc );
//     if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
//     {
//       return temp_err_code;
//     }
//   }
//   else 
  if ( vhGetObjType( vh_named ) == VHINDNAME )
  {
    NULvalue *the_memsel = NULL;
    temp_err_code = indexedNameLvalue( static_cast<vhNode>( vh_named ), context,
                                       &the_memsel );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    if ( the_memsel->getType() == eNUCompositeSelLvalue )
    {
      // If this is a record index selector, add the indices to the
      // selector and return.  No need to traverse the range normalizing
      // code.
      NUCompositeSelLvalue *csel = dynamic_cast<NUCompositeSelLvalue*>( the_memsel );
      const UInt32 sz = indexVector.size();
      for ( UInt32 i = 0; i < sz; ++i )
        csel->addIndexExpr( indexVector[i] );
      *the_lvalue = csel;
      return temp_err_code;
    }

    NUMemselLvalue *memsel_lval = dynamic_cast<NUMemselLvalue*>( the_memsel );
    if ( memsel_lval == NULL )
    {
      mMsgContext->JaguarConsistency( &loc, "Unsupported lvalue memory expression." );
      return eFatal;
    }
    NUMemoryNet *mem = memsel_lval->getIdent()->getMemoryNet();
    NU_ASSERT( mem, memsel_lval );

    // Here, we need to know what memory range we are grabbing, whether
    // this index refers to a word or to a submemory.  If it refers to a
    // submemory, the index needs to be jammed into the memsel.  If it
    // refers to a word, we need to generate a varsel.
    const UInt32 numMemDims = mem->getNumDims();
    const UInt32 numSelDims = memsel_lval->getNumDims();

    if ( numMemDims == numSelDims + numIndices )
    {
      for ( UInt32 i = 0; i < numIndices - 1; ++i )
        memsel_lval->addIndexExpr( indexVector[i] );

      // normalize the first index for the memory's operational width range
      *the_lvalue = Populate::genVarselLvalue(mem, the_memsel, indexVector[numIndices-1],
                                              loc, !isUnrolledLoopScope());
      if (*the_lvalue == NULL)
      {
        ExprRangeError(mem, indexVector[numIndices - 1], loc);
        delete the_memsel;
        return eFailPopulate;
      }
    }
    else
    {
      LOC_ASSERT( numMemDims > numSelDims + numIndices, loc );
      for ( UInt32 i = 0; i < numIndices; ++i )
        memsel_lval->addIndexExpr( indexVector[i] );
      *the_lvalue = memsel_lval;
    }
    return eSuccess;
  }
  else if ( isRecordNet( vhGetActualObj( vh_named ), context ))
  {
    if ( mCarbonContext->isNewRecord( ))
    {
      NULvalue *prefix;
      temp_err_code = lvalue( vh_named, context, &prefix/*, isAliased */ );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
        return temp_err_code;
      }
      NUCompositeInterfaceLvalue *cPrefix = dynamic_cast<NUCompositeInterfaceLvalue*>( prefix );
      if ( cPrefix )
      {
        // Index a composite
        if ( indexVector.size() == 1 && cPrefix->getNet()->isVectorNet( ))
        {
          NUVectorNet *vn = cPrefix->getNet()->castVectorNet();
          // If this index references a vector net, it needs to be normalized
          indexVector[0] = vn->normalizeSelectExpr( indexVector[0], 0, true );
        }
        *the_lvalue = new NUCompositeSelLvalue( cPrefix, &indexVector, loc );
        return temp_err_code;
      }
      else
      {
        NU_ASSERT( prefix->getType() == eNUIdentLvalue, prefix );
        net = dynamic_cast<NUIdentLvalue*>( prefix )->getIdent();
        delete prefix;
      }
    }
    else
    {
      // Get the correct NUNet
      temp_err_code = lookupNet( context->getDeclarationScope(),
                                 vh_named, context, &net );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
      
      temp_err_code = getVhdlRecordIndicies( vh_named, &indexVector, context );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
        return temp_err_code;
    }
  }
  else
  {
    temp_err_code = lookupNet( context->getDeclarationScope(),
                               vh_named, context, &net );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
  }

  temp_err_code = indexedNameLvalueHelper( net, &indexVector, context, loc, the_lvalue );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  return err_code;
}


Populate::ErrorCode
VhdlPopulate::indexedNameLvalueHelper( NUNet *net, NUExpr *bitsel_expr,
                                       LFContext *context, const SourceLocator &loc,
                                       NULvalue **the_lvalue )
{
  NUExprVector indexVector;
  indexVector.push_back( bitsel_expr );
  return indexedNameLvalueHelper( net, &indexVector, context, loc, the_lvalue );
}


Populate::ErrorCode
VhdlPopulate::indexedNameLvalueHelper( NUNet *net, NUExprVector *indexVector,
                                       LFContext * /* context */, const SourceLocator &loc,
                                       NULvalue **the_lvalue )
{
  // An array access is just like a bit select in that we only get one entry
  // of the object.  We want to handle them differently so we differentiate 
  // them now.
  const UInt32 numIndices = indexVector->size();
  NUExpr *bitsel_expr = NULL;
  NUVectorNet *vn = NULL;
  NUMemoryNet *mn = NULL;
  NUCompositeNet *cn = NULL;
  NULvalue *baseLval = NULL;
  if (net->is2DAnything()) 
  {
    const UInt32 numDims = net->getMemoryNet()->getNumDims();
    if ( numIndices < numDims )
    {
      *the_lvalue = new NUMemselLvalue(net, indexVector, loc);
      return eSuccess;
    }
    else
    {
      // make a new vector without the last expr; these go in the memsel
      NUExprVector memselVector;
      for ( UInt32 i = 0; i < numIndices - 1; ++i )
      {
        memselVector.push_back( (*indexVector)[i] );
      }
      // We need to make a varsel with the last expr
      mn = net->getMemoryNet();
      bitsel_expr = (*indexVector)[numIndices - 1];
      baseLval = new NUMemselLvalue( mn, &memselVector, loc );
    }
  }
  else if ( net->isCompositeNet( ))
  {
    cn = net->getCompositeNet();
    NUCompositeIdentLvalue *ident = new NUCompositeIdentLvalue( cn, loc );
    *the_lvalue = new NUCompositeSelLvalue( ident, indexVector, loc );
    return eSuccess;
  }
  else
  {
    NU_ASSERT( indexVector->size() == 1, net );
    bitsel_expr = (*indexVector)[0];
    vn = net->castVectorNet();
    baseLval = new NUIdentLvalue( vn, loc );
  }

  *the_lvalue = Populate::genVarselLvalue(net, baseLval, bitsel_expr, loc,
                                          !isUnrolledLoopScope());
  if (*the_lvalue == NULL) {
    VectOrMemExprRangeError( net, bitsel_expr, loc );
    return eFailPopulate;
  }
  return eSuccess;
}

//
// Method to create NULvalue corresponding to a slice name object whose 
// width is variable. 
//
// Signal INPUT : bit_vector(H downto L );
// INPUT[MSB downto LSB] ==>  INPUT ( H 'downto' L)
//
// Signal INPUT : bit_vector(L to H );
// INPUT[MSB to LSB] ==> INPUT ( H 'downto' L),
//
// In the above, downto corresponds to the Vhdl range-deferred sizing operator eBiDownTo
//
Populate::ErrorCode
VhdlPopulate::variableWidthSliceNameLvalue(vhExpr vh_slice,
                                           LFContext *context,
                                           NUNet *net,
                                           NULvalue **the_lvalue,
                                           int /*lWidth*/)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  SourceLocator loc = locator(vh_slice);

  // In contexts where the size is self-evident, such as
  //     a(7 to 0) = b(x to y)
  // then take on faith the context size.  If x to y is anything else,
  // at runtime, a VHDL-LRM compliant simulator would give a runtime error.
  //

  vhExpr vh_named = vhGetPrefix(vh_slice);
  vhNode vh_range = vhGetDisRange(vh_slice);
  vhNode actual_range = NULL;
  temp_err_code = getVhRangeNode(vh_range, &actual_range, NULL);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  vh_range = actual_range;
  vhExpr vh_left = vhGetLtOfRange(vh_range);
  vhExpr vh_right = vhGetRtOfRange(vh_range);
  NUExpr* sliceLtBound = NULL;
  temp_err_code = expr ( vh_left, context, 0, &sliceLtBound );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  // As per IEEE std 1076.6-1999 section 8.3.2.1, array index must be 
  // of type integer or it's subtype. As per jaguar supplied STD package, 
  // INTEGER type is : TYPE integer is range -2147483648 to +2147483647;
  // which is of 32 bit, so with that STD package it can never be of more 
  // than 32 bit wide. Hence, we will set the size of the slice/net bounds
  // to 32 bit. 
  sliceLtBound->resize(32);
  NUExpr* sliceRtBound = NULL;
  temp_err_code = expr ( vh_right, context, 0, &sliceRtBound );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  sliceRtBound->resize(32);
  NULvalue* baseObj = NULL;
  temp_err_code = lvalue ( vh_named, context, &baseObj, false);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  // variable width of a whole memory is not yet supported, bug4850
  if ((baseObj->getType() == eNUIdentLvalue)&& net->isMemoryNet())
    {
      UtString buf;
      SourceLocator sliceLoc = locator(vh_slice);
      mMsgContext->VariableWidthMemory(&sliceLoc, decompileJaguarNode(vh_slice, &buf));
      return eFatal;
    }

  if (net->isCompositeNet())
  {
    UtString buf;
    SourceLocator slice_loc = locator(vh_slice);
    mMsgContext->VariableWidthRecordSlice(&slice_loc, decompileJaguarNode(vh_slice, &buf));
    return eFatal;
  }

  ConstantRange netRange;
  NUExpr* downTo = NULL;

  if (NUVectorNet* vn = dynamic_cast<NUVectorNet*>(net)) {
    netRange = *vn->getRange ();
    downTo = new NUBinaryOp (NUOp::eBiDownTo,
                             vn->normalizeSelectExpr (sliceLtBound, 0, false),
                             vn->normalizeSelectExpr (sliceRtBound, 0, false), loc);
  } else if (NUMemoryNet* mn = dynamic_cast<NUMemoryNet*>(net)) {
    netRange = *mn->getWidthRange ();
    downTo = new NUBinaryOp (NUOp::eBiDownTo,
                             mn->normalizeSelectExpr (sliceLtBound, 0, 0, false),
                             mn->normalizeSelectExpr (sliceRtBound, 0, 0, false), loc);
  } else if (NUCompositeNet* cn = net->getCompositeNet()) {
    netRange = *cn->getRange(0); // Index 0 is the innermost range.
    downTo = new NUBinaryOp (NUOp::eBiDownTo, sliceLtBound, sliceRtBound, loc);
  }

  downTo->resize (32);
  
  if (net->isCompositeNet()) {
    NUCompositeInterfaceLvalue* comp_lval = dynamic_cast<NUCompositeInterfaceLvalue*>(baseObj);
    // The lvalue better be a composite since the net is composite.
    NU_ASSERT(comp_lval, baseObj);
    NUExprVector idxVec;
    idxVec.push_back(downTo);
    *the_lvalue = new NUCompositeSelLvalue(comp_lval, &idxVec, netRange, false, loc);
  } else {
    *the_lvalue = new NUVarselLvalue (baseObj, downTo, netRange, loc);
  }

   return err_code;
}

Populate::ErrorCode VhdlPopulate::sliceNameLvalue(vhNode vh_lvalue,
                                                  LFContext *context,
                                                  NULvalue **the_lvalue)
{
  ErrorCode err_code = eSuccess, temp_err_code = eSuccess;
  *the_lvalue = 0;
  NUExpr* index_expr = NULL;
  ConstantRange range;
  NUNet* net = 0;
  UtVector<vhExpr> dimVector;
  
  int lWidth = 0;
  SourceLocator loc = locator( vh_lvalue );
  
    
  vhExpr vh_exprCheckNested = vhGetPrefix(static_cast<vhExpr>(vh_lvalue));
  if (vhGetObjType(vh_exprCheckNested) == VHSLICENAME)
  {
    UtString buf;
    POPULATE_FAILURE(mMsgContext->SliceOfSlice(&loc, decompileJaguarNode(vh_lvalue, &buf)), &err_code);
    return err_code;
  }
  
  temp_err_code = getVhExprWidth( static_cast<vhExpr>( vh_lvalue ), &lWidth,
                                  loc, context, true );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    if (temp_err_code == eFatal)
      return eFatal;
      
    // didn't get a constant width for this value; default the width for now as
    // the max-width of the object we're slicing from
    //
    temp_err_code = getVhExprWidth (vhGetPrefix (static_cast<vhExpr>(vh_lvalue)),
                                    &lWidth, loc, context, true);

    if (errorCodeSaysReturnNowWhenFailPopulate (temp_err_code, &err_code))
    {
      UtString buf;
      vhExpr vh_expr =  vhGetPrefix(static_cast<vhExpr>(vh_lvalue));
      if (vhGetObjType(vh_expr) == VHSLICENAME)
         mMsgContext->SliceOfSlice(&loc, decompileJaguarNode(vh_lvalue, &buf));
      else
         mMsgContext->FailedToDetermineRange(&loc, decompileJaguarNode(vh_lvalue, &buf));

      return temp_err_code;
    }
  }

  NUBase *base;
  bool isConstIndex = false;
  temp_err_code = partsel (static_cast<vhExpr>(vh_lvalue), context, lWidth,
                           &base, &index_expr, &range, &dimVector, &isConstIndex, true);

  // Find the net, if it's here
  net = dynamic_cast<NUNet *>( base );
  NUIdentLvalue *ident = NULL;
  if ( !net )
  {
    ident = dynamic_cast<NUIdentLvalue *>( base );
    if ( ident )
      net = ident->getIdent();
  }

  if (errorCodeSaysReturnNowWhenFailPopulate (temp_err_code, &err_code))
  {
    // try dynamic range
    temp_err_code = variableWidthSliceNameLvalue (static_cast<vhExpr>(vh_lvalue),
                                                  context, net, the_lvalue, lWidth);
    return temp_err_code;
  }

  if ( !net )
  {
    NUCompositeInterfaceLvalue *lvalue = dynamic_cast<NUCompositeInterfaceLvalue*>( base );
    NU_ASSERT( lvalue, base );

    NUExprVector idxVec;
    bool wasTruncated = false;
    temp_err_code = getIndexExprs(dimVector, loc, 0, context, &wasTruncated, &idxVec);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
      return temp_err_code;
    }
    // Add expression for last dimension's select.
    idxVec.push_back(index_expr);
    *the_lvalue = new NUCompositeSelLvalue(lvalue, &idxVec, range /*part select*/,
                                           isConstIndex, loc);
    return err_code;
  }

  if ( eSuccess != temp_err_code )
  {
    delete index_expr; 
    return eFailPopulate;
  }

  // The net is expected to be NULL if partsel returns 
  // failure status. If it returns eSuccess, the net
  // better be available ..unless there's a bug!.
  LOC_ASSERT(net != NULL, loc);

  if ( net->isMemoryNet( ))
  {
    // Delete any unnecessary ident
    delete ident;
    ident = NULL;

    NUExprVector idxVector;
    const UInt32 numIdxs = dimVector.size();
    for ( UInt32 idx = 0; idx < numIdxs; ++idx )
    {
      vhExpr vh_index = dimVector[idx];
      NUExpr *idxExpr;
      temp_err_code = expr( vh_index, context, 0, &idxExpr );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;
      idxExpr->resize( idxExpr->determineBitSize( ));
      idxVector.push_back( idxExpr );
    }
    if ( net->getMemoryNet()->getNumDims() - numIdxs == 1 )
    {
      // Create a NUMemselLValue here
      NUMemselLvalue *memLval = new NUMemselLvalue(net, &idxVector, loc );
      *the_lvalue = new NUVarselLvalue( memLval, index_expr, range, loc );
    }
    else
    {
      // This is partselect on a MemoryNet. Break it up into separate
      // MemoryNet row selects and Concat them together.
      SInt32 rowLsb = range.getLsb();
      SInt32 rowMsb = range.getMsb();
      UInt32 noOfRows = range.getLength();
      NULvalueVector lvalue_vector;
      SInt32 incr = (rowLsb > rowMsb) ? 1 : -1;
      CopyContext cc(0,0);
      for (UInt32 i = 0; i < noOfRows; i++)
      {
        // The index_expr plus the row address forms the row select.
        NUExpr* offset_expr = (NUExpr*) NUConst::create( true, rowMsb, 32, loc);
        NUExpr* addr_expr = new NUBinaryOp(NUOp::eBiPlus, index_expr->copy(cc),
                                           offset_expr, loc);
        NUExprVector pselVector;
        for ( UInt32 idx = 0; idx < numIdxs; ++idx )
        {
          if ( i == 0 )
            pselVector.push_back( idxVector[idx] ); // don't make a copy the first one
          else
            pselVector.push_back( idxVector[idx]->copy( cc ));
        }
        pselVector.push_back(addr_expr);
        NULvalue* lv = new NUMemselLvalue(net, &pselVector, loc);
        lvalue_vector.push_back(lv);
        rowMsb += incr;
      }
      *the_lvalue = new NUConcatLvalue(lvalue_vector, loc);
      delete index_expr; // We've used it's copies.
      index_expr = NULL;
    }
  }
  else
  {
    if ( ident )
      *the_lvalue = new NUVarselLvalue (ident, index_expr, range, loc);
    else
      *the_lvalue = new NUVarselLvalue (net, index_expr, range, loc);
  }
  return err_code;
}

Populate::ErrorCode VhdlPopulate::aggregateLvalue(vhExpr vh_lvalue,
                                                  LFContext *context,
                                                  NUConcatLvalue **the_lvalue)
{
  *the_lvalue = 0;
  ErrorCode err_code = eSuccess;

  if ( mCarbonContext->isNewRecord() && isRecordNet( vh_lvalue, context ))
  {
    NULvalue *retval = NULL;
    err_code = recordAggregate( vh_lvalue, context, &retval );
    NU_ASSERT( retval->getType() == eNUCompositeLvalue, retval );
    *the_lvalue = dynamic_cast<NUCompositeLvalue*>( retval );
    return err_code;
  }

  NULvalueVector lvalue_vector;
  vhExpr exprSize = vhGetExprSize(vh_lvalue);
  if (vhGetObjType(exprSize) == VHINDEXCONSTRAINT)
  {
    JaguarList rangeList(vhGetDiscRangeList(exprSize));
    exprSize = static_cast<vhExpr>(vhGetNextItem(rangeList));
  }

  VhdlRangeInfo indexBounds (0,0);
  SourceLocator loc = locator( vh_lvalue );
  LOC_ASSERT(exprSize && vhGetObjType(exprSize) == VHRANGE, loc);
  ErrorCode temp_err_code = getLeftAndRightBounds(exprSize, &indexBounds, context );
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
    return temp_err_code;

  if (VH_DOWNTO == vhGetRangeDir(exprSize))
    indexBounds.swap ();

  lvalue_vector.resize(indexBounds.getLsb (loc)+1);
  for ( int i = 0; i <= indexBounds.getLsb (loc); i++)
    lvalue_vector[i] = NULL;

  int index = indexBounds.getMsb (loc);
  JaguarList vh_lvalue_iter(vhGetEleAssocList(vh_lvalue));
  if (vh_lvalue_iter) 
  {
    vhExpr element;
    while ((element = static_cast<vhExpr>(vhGetNextItem(vh_lvalue_iter)))) 
    {
      NULvalue *cur_lvalue = 0;
      vhExpr    assoc_expr = element;
      vhObjType elem_type = vhGetObjType(element);
      switch (elem_type)
      {
      case VHELEMENTASS:
      {
        assoc_expr = vhGetExpr(element);
        JaguarList choice_l(vhGetChoiceList(element));
        vhNode upto;
        while ((upto = vhGetNextItem(choice_l)))
        {
          switch(vhGetObjType(upto))
          {
          case VHDECLIT:
          case VHBASELIT:
          case VHIDENUMLIT:
          case VHCHARLIT:
          {
            temp_err_code = getVhExprValue(static_cast<vhExpr>(upto),
                                           &index, NULL);
            if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
            {
              return temp_err_code;
            }
            temp_err_code=lvalue(assoc_expr, context, &cur_lvalue);
            if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
            {
              return temp_err_code;
            }
            LOC_ASSERT(cur_lvalue, loc);
            lvalue_vector[index] = cur_lvalue;
            break;
          }
          default:
          {
            SourceLocator loc = locator(upto);
            UtString msgStr;
            msgStr << "Unexpected choice node type (" << gGetVhNodeDescription( upto )
                   << ") in aggregate element association on RHS.";
            mMsgContext->JaguarConsistency( &loc, msgStr.c_str( ));
            return eFatal;
            break;
          }
          }
        }
        break;
      }
      default:
      {
        temp_err_code = lvalue(assoc_expr, context, &cur_lvalue);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
        LOC_ASSERT(cur_lvalue, loc);
        lvalue_vector[index] = cur_lvalue;
        index++;
        break;
      }
      }
    }
  }
  NULvalueVector actualLVector;
  for (SInt32 index=0; index <= indexBounds.getLsb (loc); index++)
  {
    if ( lvalue_vector[index] != NULL )
      actualLVector.push_back(lvalue_vector[index]);
  }

  *the_lvalue = new NUConcatLvalue(actualLVector, loc);
  return err_code;
}
