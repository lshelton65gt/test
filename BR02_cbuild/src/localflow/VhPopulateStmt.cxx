// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "LFContext.h"
#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/JaguarContext.h"
#include "compiler_driver/JaguarBrowser.h"
#include "localflow/DesignPopulate.h"
#include "reduce/Fold.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUCompositeNet.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"

//! Maximum number of iterations allowed in an unrollable loop.
//! This was set to match reduce/LoopUnroll.cxx MAX_UNROLL_ITERATIONS
//! count. If that changes for some reason, check if this should change too!.
const UInt32 VhdlPopulate::scMaxLoopUnrollIterations = 1024;
//! Returns true if the statement is not allowed in an unrollable loop.
static bool sIsValidStmtForLoopUnroll(vhNode vh_stmt);
//! Return true if the statement list has statements that are not
//! allowed in unrollable loop.
static bool sAreValidStmtsForLoopUnroll(JaguarList& stmts);

extern void unsetVariableInitialValue( vhNode node ); // defined in VhPopulateAssign.cxx

//! Class to collect nets def'ed in the lvalue being browsed.
class LvalueDefCollector : public JaguarBrowserCallback
{
public:
  LvalueDefCollector(UtSet<vhNode>* defs) 
    : mDefs(defs)
  {
  }

  Status vhvariable(Phase phase, vhNode node)
  {
    if (phase == ePre) {
      mDefs->insert(node);
    }
    return eNormal;
  }

  Status vhslicename(Phase phase, vhNode node)
  {
    if (phase == ePre) {
      // Slice bound variables are used not def'ed. Avoid adding them to def list.
      collectPrefixDefs(node);
      return eSkip;
    }
    return eNormal;
  }

  Status vhindname(Phase phase, vhNode node)
  {
    if (phase == ePre) {
      // Slice bound variables are used not def'ed. Avoid adding them to def list.
      collectPrefixDefs(node);
      return eSkip;
    }
    return eNormal;
  }

private:

  void collectPrefixDefs(vhNode node)
  {
    vhNode prefix = static_cast<vhNode>(vhGetPrefix(static_cast<vhExpr>(node)));
    LvalueDefCollector dc(mDefs);
    {
      JaguarBrowser jb(dc);
      jb.browse(prefix);
    }
  }

  UtSet<vhNode>* mDefs;
};

/*! \class JagDefCollector
  Collects all the nets that are def'ed in the statement block. These defs are
  used for example for invalidating constants propagated into loops.
*/
class JagDefCollector : public JaguarBrowserCallback
{
public:
  JagDefCollector(VhdlPopulate* populator, LFContext* context,
                      MsgContext* msgContext, bool isNewRecord,
                      bool ignoreMissing = false)
    : mPopulator(populator),
      mContext(context),
      mMsgContext(msgContext),
      mIsNewRecord(isNewRecord),
      mIgnoreMissing(ignoreMissing),
      mSuccess(true)
  {
  }

  virtual ~JagDefCollector()
  {
  }

  Status vhvarasgn(Phase phase, vhNode node)
  {
    // In pre phase, collect the def.
    if (phase == ePre) {
      vhNode lval = static_cast<vhNode>(vhGetTarget(node));
      addDef(lval);
      if (!mSuccess) {
        return eStop;
      }
    }
    return eNormal;
  }

  Status vhprocedurecall(Phase phase, vhNode node)
  {
    if (phase == ePre)
    {
      // Procedure call outputs def actuals.
      UtList<vhNode> output_list;
      {
        JaguarList actual_list;
        mPopulator->getFuncProcActualArgList(node, &actual_list,
                                             &output_list);
      }

      for (UtList<vhNode>::iterator itr = output_list.begin();
           itr != output_list.end(); ++itr)
      {
        addDef(*itr);
        if (!mSuccess) {
          return eStop;
        }
      }

      // The procedure if impure may def global nets. 
      addImpureGlobalDefs(node);
      if (!mSuccess) {
        return eStop;
      }
    }
    return eNormal;
  }

  Status vhfunccall(Phase phase, vhNode node)
  {
    if (phase == ePre)
    {
      // The function if impure may def global nets.
      addImpureGlobalDefs(node);
      if (!mSuccess) {
        return eStop;
      }
    }
    return eNormal;
  }

  bool getSuccess() const {
    return mSuccess;
  }

  NUNetSet& getDefs() {
    return mDefs;
  }

private:

  void addImpureGlobalDefs(vhNode subprog_call)
  {
    vhNode subprog_decl = vhGetMaster(subprog_call);
    if (subprog_decl != NULL)
    {
      vhPureType purity = vhGetPureType(subprog_decl);
      if (purity == VH_IMPURE)
      {
        vhNode subprog_body = vhGetSubProgBody(subprog_decl);
        if (subprog_body != NULL)
        {
          // We're looking for global defs in impure function. Ignore
          // local nets in function that we will not find. However we
          // ought to find the global nets that are def'ed.
          JagDefCollector dc(mPopulator, mContext, mMsgContext,
                                 mIsNewRecord, true /*ignore missing*/);
          {
            JaguarList stmt_list(vhGetSeqStmtList(subprog_body));
            JaguarBrowser jb(dc);
            jb.browseList(stmt_list);
          }
          mSuccess = dc.getSuccess();
          if (mSuccess) {
            mDefs.insert(dc.getDefs().begin(), dc.getDefs().end());
          }
        }
        else
        {
          // We have to have the impure function body to find global defs.
          SourceLocator loc = mPopulator->locator(subprog_call);
          LOC_ASSERT((subprog_body != NULL), loc);
        }
      }
    }
  }

  void addDef(vhNode defNode)
  {
    UtSet<vhNode> defNodes;
    LvalueDefCollector dc(&defNodes);
    {
      JaguarBrowser jb(dc);
      jb.browse(defNode);
    }

    for (UtSet<vhNode>::iterator itr = defNodes.begin();
         mSuccess && (itr != defNodes.end()); ++itr)
    {
      vhNode curDef = *itr;
      if (!mIsNewRecord && mPopulator->isRecordNet(curDef, mContext)) {
        // Constant propagation for records is not implemented yet. When implemented,
        // it would be for new record processing only. Ignore record nets in
        // old record processing mode. They're not constant propagated, so no
        // need to collect their defs to invalid them.
        continue;
      }
      NUNet* net = 0;
      Populate::ErrorCode err_code = 
        mPopulator->lookupNet(mContext->getDeclarationScope(), curDef, mContext,
                              &net, !mIgnoreMissing);
      if ((err_code != Populate::eSuccess) || (net == NULL)) {
        if (!mIgnoreMissing) {
          mSuccess = false; // lookupNet displays the error message.
        }
      } else {
        // If composite net is def'ed, all it's fields are def'ed too. This is 
        // pessimistic but safe. It can be improved later to be field specific.
        if (net->isCompositeNet()) {
          NUCompositeNet* c_net = net->getCompositeNet();
          for (UInt32 field_idx = 0; field_idx < c_net->getNumFields(); ++field_idx) {
            NUNet* field = c_net->getField(field_idx);
            mDefs.insert(field);
          }
        }
        mDefs.insert(net);
      }
    }
  }

  NUNetSet mDefs;
  VhdlPopulate* mPopulator;
  LFContext* mContext;
  MsgContext* mMsgContext;
  bool mIsNewRecord;
  bool mIgnoreMissing; // Set when looking for global defs in impure function.
  bool mSuccess;
};

/*! \class LoopAnalyzer
  Performs analysis to determine the following:
  a. Determines if the statements within the loop are the ones that allow
     unrolling to succeed. Statements like return, next, exit make loops unrollable.
     Note that this glosses over loop statements, since they're analyzed when
     time comes to populate them.
  b. Determines if unrolling is necessary for population to succeed. If there are
     functions calls which have variable bound slices as actuals, then the only
     way population can succeed is if loop is unrolled.
     Functions have to have statically known widths for input arguments for their
     population to succeed. When their actuals are variable bounds, they're
     most probably derived from loop index in some way. Unrolling helps determine
     these bounds for every iteration and thus helps successfully population
     such functions.
     Note that this is a pessimistic analysis. If this determines that unrolling
     is necessary and that unrolling is not possible ..see item a, it would
     proceed to populate without unrolling.
 */
class LoopAnalyzer : public JaguarBrowserCallback
{
public:
  LoopAnalyzer()
    : mHasValidStmtsForUnroll(true),
      mCheckSubprogActuals(0),
      mNeedsUnrolling(false)
  {
  }
  
  bool hasValidStmtsForUnroll() const {
    return mHasValidStmtsForUnroll;
  }

  bool needsUnrolling() const {
    return mNeedsUnrolling;
  }

  Status vhreturn(Phase, vhNode)
  {
    mHasValidStmtsForUnroll = false;
    return eNormal;
  }

  Status vhexit(Phase, vhNode)
  {
    mHasValidStmtsForUnroll = false;
    return eNormal;
  }

  Status vhnext(Phase, vhNode)
  {
    mHasValidStmtsForUnroll = false;
    return eNormal;
  }

  Status vhprocedurecall(Phase phase, vhNode)
  {
    return handleSubProgCall(phase);
  }

  Status vhfunccall(Phase phase, vhNode)
  {
    return handleSubProgCall(phase);
  }

  Status vhbinary(Phase phase, vhNode node)
  {
    vhExpr bin_expr = static_cast<vhExpr>(node);
    if (VhdlPopulate::isOperatorOverloadedByUser(bin_expr)) {
      return handleSubProgCall(phase);
    }
    return eNormal;
  }

  Status vhunary(Phase phase, vhNode node)
  {
    vhExpr un_expr = static_cast<vhExpr>(node);
    if (VhdlPopulate::isOperatorOverloadedByUser(un_expr)) {
      return handleSubProgCall(phase);
    }
    return eNormal;
  }

  Status vhrange(Phase phase, vhNode node)
  {
    if ((phase == ePre) && (mCheckSubprogActuals > 0) && !mNeedsUnrolling)
    {
      vhExpr left_bound = vhGetLtOfRange(node);
      vhExpr right_bound = vhGetRtOfRange(node);
      // If left and right bounds are not static, then pessimistically 
      // presume them to be loop index based and enable unrolling.
      if (!isStatic(left_bound) || !isStatic(right_bound)) {
        mNeedsUnrolling = true;
      }
    }
    return eNormal;
  }

  Status vhforindex(Phase phase, vhNode)
  {
    if ((phase == ePre) && (mCheckSubprogActuals > 0)) {
      mNeedsUnrolling = true;
    }
    return eNormal;
  }

public:

  bool isStatic(vhExpr bound) {
    vhStaticType staticness = vhGetStaticType(bound);
    if (VH_LOCALLY_STATIC == staticness || VH_GLOBALLY_STATIC == staticness) {
      return true;
    }
    return false;
  }

  Status handleSubProgCall(Phase phase)
  {
    if ((phase == ePre) && !mNeedsUnrolling) {
      ++mCheckSubprogActuals;
    } else {
      --mCheckSubprogActuals;
    }
    return eNormal;
  }

  bool mHasValidStmtsForUnroll;
  UInt32 mCheckSubprogActuals; // > 0 if iterating over subprog actuals
  bool mNeedsUnrolling;
};

/*! \class ClockDetector
 */
class ClockDetector : public NuToNuFn
{
public:
  ClockDetector()
    : mClockDetected(false)
  {
  }

  virtual NUExpr *operator()(NUExpr *expr, Phase phase)
  {
    if (phase == ePre && expr->getType() == NUExpr::eNUAttribute)
    {
      NUAttribute *attrib = dynamic_cast<NUAttribute*>( expr );
      if ((attrib->getAttrib() == NUAttribute::eEvent) ||
          (attrib->getAttrib() == NUAttribute::eStable)) {
        mClockDetected = true;
      }
    }
    return NULL;
  }
  
  bool getClockDetected() const { return mClockDetected; }

private:
  bool mClockDetected;
};

bool sExprHasClock(NUExpr* expr)
{
  ClockDetector cd;
  expr->replaceLeaves(cd);
  return cd.getClockDetected();
}

/*! \class ClockCleaner
 *  \brief Remove the clock specification from a if or elsif condition
 *  containing a clock edge.
 *
 * In order to be able to populate complex gated clock expressions we
 * need to be able to detect when a complicated expression contains a
 * clock.  Jaguar can do this for us; however, when it comes time to
 * populate we end up populating the clock level expression.  If this is
 * left in place it causes an unnecessary if check in every VHDL flop.
 * This walker gets created knowing the clock and the edge of the clock.
 * It examines all the subexpressions and when an expression like "CLK =
 * <level>" is encountered, it is replaced with a constant '1'.  It also
 * replaces NUAttribute nodes with either true or false, so that 'EVENT
 * becomes 'true', and "not 'STABLE" will become "not false".  These
 * constant expressions then get folded away later during the primary
 * Fold phase, well after population.
 */
class ClockCleaner : public NuToNuFn {
public:
  ClockCleaner( NUExpr *clock, int edge, LFContext *context )
    : mEdge( edge ), mClockSet( context->getNetRefFactory( )), mContext( context ),
      mErrorDetected( false ) {
    if ( clock )
    {
      clock->getUses( &mClockSet );
      if (mClockSet.getNumBits() != 1) {
        UtString errMsg;
        clock->getLoc().compose(&errMsg);
        errMsg << "Multiple bits detected in clock expression.";
        INFO_ASSERT(mClockSet.getNumBits() == 1, errMsg.c_str());
      }
    }
  }

  virtual NUExpr *operator()( NUExpr *expr, Phase phase ) {
    NUExpr *new_expr = NULL;
    const SourceLocator loc = expr->getLoc();
    if ( phase == ePre && expr->getType() == NUExpr::eNUAttribute )
    {
      // During the ePre phase, optimize away any 'EVENT or 'STABLE attributes
      NUAttribute *attrib = dynamic_cast<NUAttribute*>( expr );
      NUNetRefSet attribSet( mContext->getNetRefFactory( ));
      attrib->getUses( &attribSet );
      if ( attribSet == mClockSet )
      {
        NUAttribute::AttribT attrType = attrib->getAttrib();
        if ( attrType == NUAttribute::eEvent )
        {
          // Replace a 'event on the clock with TRUE
          new_expr = NUConst::create( false, 1u, 1, loc );
          delete expr;
        }
        else if ( attrType == NUAttribute::eStable )
        {
          // Replace a 'stable on the clock with FALSE
          new_expr = NUConst::create( false, 0u, 1, loc );
          delete expr;
        }
        else
        {
          // Unsupported attribute
          const char *attribStr = attrib->getAttribChar();
          mContext->getMsgContext()->UnsupportedAttribute( &loc, attribStr );
          mErrorDetected = true;
        }
      }
      else
      {
        // Attribute is not on a clock net
        const char *attribStr = attrib->getAttribChar();
        mContext->getMsgContext()->AttributeNotOnClock( &loc, attribStr );
        mErrorDetected = true;
      }
    }
    else if ( phase == ePost && expr->getType() == NUExpr::eNUBinaryOp )
    {
      // During the ePost phase, optimize away the clock level expressions
      NUOp *op = dynamic_cast<NUOp*>( expr );
      if ( op->getOp() == NUOp::eBiEq )
      {
        // If a binary op is the clock level specifier (like clk='1'),
        // replace it with TRUE (-> '1').
        NUNetRefSet exprSet( mContext->getNetRefFactory( ));
        // For a clock expression, the numBits used will be 1 from one
        // arg and 0 on the other.  The side with no bits is the level
        // of the clock.
        NUExpr *level = op->getArg(1);
        op->getArg(0)->getUses( &exprSet );
        if ( exprSet.getNumBits() == 0 ) {
          // clock is arg1, level is arg0
          op->getArg(1)->getUses( &exprSet );
          level = op->getArg(0);
        }
        
        if ( exprSet == mClockSet )
        {
          // We know we have the clock; now verify the edge.
          UInt32 value = 0;
          if (NUConst* c = level->castConst( ))
          {
            c->getUL ( &value );
          }
          if ( value == mEdge )
          {
            new_expr = NUConst::create( false, 1u, 1, loc);
            delete expr;
          }
        }
      }
    }
    return new_expr;
  }

  bool getErrorDetected() const { return mErrorDetected; }

private:
  ClockCleaner();
  ClockCleaner( const ClockCleaner& );
  ClockCleaner &operator=( const ClockCleaner& );

  UInt32 mEdge;
  NUNetRefSet mClockSet;
  LFContext *mContext;
  bool mErrorDetected;
};

// This class is used to replace the clocked net in a clock expression of type:
// vec[0]'event and vec[0] = 1
// with
// temp_vec_0'event and temp_vec_0 = 1
// where
// assign temp_vec_0 = vec[0]
// 
// If clock expression net is not a scalar bit net, but a vector bit
// like vec[0]'event, then it's replaced with a temporary net during always
// block edge expression creation, in VhdlPopulate::edgeExpr(). Replace it
// in the given expression ..typically coming from an if statement condition,
// if the expression is a clocked expression.
class ClockNetReplacer : public NuToNuFn
{
public:

  ClockNetReplacer(LFContext *context, NUExpr* clock_expr)
    : mContext(context),
      mClockExpr(clock_expr),
      mErrorDetected(false)
  {
  }

  ~ClockNetReplacer()
  {
    for (NUCExprVector::iterator itr = mMarkedForDeletion.begin();
         itr != mMarkedForDeletion.end(); ++itr)
    {
      delete *itr;
    }
  }

  virtual NUExpr* operator() (NUExpr* expr, Phase phase)
  {
    // Look for binary expression like : clk'event and clk = '1'
    const SourceLocator loc = expr->getLoc();
    if (phase == ePre && expr->getType() == NUExpr::eNUAttribute)
    {
      // During ePre phase, look for clk'event and clk'stable expressions.
      NUAttribute *attrib = dynamic_cast<NUAttribute*>(expr);
      NUExpr* clk_expr = attrib->getArg(0);
      if (*clk_expr == *mClockExpr) // Is clock used in this attrib expr?
      {
        NUAttribute::AttribT attrType = attrib->getAttrib();
        if ((attrType == NUAttribute::eEvent) ||
            (attrType == NUAttribute::eStable))
        {
          replaceClkArg(expr, 0 /*NUAttribute has only 1 arg*/, loc);
        }
        else
        {
          // Unsupported attribute
          const char *attribStr = attrib->getAttribChar();
          mContext->getMsgContext()->UnsupportedAttribute(&loc, attribStr);
          mErrorDetected = true;
        }
      }
    }
    else if (phase == ePost && expr->getType() == NUExpr::eNUBinaryOp)
    {
      // During the ePost phase, look for clock level expressions.
      // They are binary expressions with clock level specified like clk = '1'
      NUOp *op = dynamic_cast<NUOp*>(expr);
      if (op->getOp() == NUOp::eBiEq)
      {
        UInt32 arg_index = 0;
        if (op->getArg(arg_index)->isConstant()) {
          ++arg_index;
        }
        NUExpr* clk_expr = op->getArg(arg_index);
        if (*clk_expr == *mClockExpr)  // Is clock used in this level expr?
        {
          replaceClkArg(expr, arg_index, loc);
        }
      }
    }
    return NULL;
  }

  bool getErrorDetected() const { return mErrorDetected; }

private:
  ClockNetReplacer();
  ClockNetReplacer( const ClockNetReplacer& );
  ClockNetReplacer &operator=( const ClockNetReplacer& );

  void replaceClkArg(NUExpr* expr, UInt32 arg_index, const SourceLocator& loc)
  {
    NUNet* clock_net = mContext->getEdgeNet(mClockExpr);
    NU_ASSERT(clock_net != NULL, mClockExpr);
    NUExpr* new_arg = new NUIdentRvalue(clock_net, loc);
    NUExpr* old_expr = expr->putArg(arg_index, new_arg);
    mMarkedForDeletion.push_back(old_expr);
  }

  LFContext *mContext;
  NUExpr* mClockExpr;
  bool mErrorDetected;
  NUCExprVector mMarkedForDeletion;
};

Populate::ErrorCode
VhdlPopulate::maybeReplaceClockNet(NUExpr *cond_expr, LFContext *context)
{
  // Look for clock expressions on clocks which are bits of vector. They
  // need to be replaced with temporary net, so that clocked net is
  // always a scalar net.
  ErrorCode err_code = eSuccess;
  if (mCurrentProcessInfo && (mCurrentProcessInfo->mClockExpr != NULL) &&
      (mCurrentProcessInfo->mClockExpr->getType() != NUExpr::eNUIdentRvalue))
  {
    ClockNetReplacer cnr(context, mCurrentProcessInfo->mClockExpr);
    cond_expr->replaceLeaves(cnr);
    if (cnr.getErrorDetected())
      err_code = eFailPopulate;
  }
  return err_code;
}

bool VhdlPopulate::isLoop(vhNode node) {
  switch (vhGetObjType(node))
  {
    case VHFOR:
    case VHWHILE:
    case VHLOOP:
      return true;
  default:
    break;
  }
  return false;
}

StringAtom* VhdlPopulate::getSeqBlockDeclScopeName(vhNode vh_block,
                                                   LFContext* context)
{
  SourceLocator loc = locator(vh_block);
  StringAtom* name = NULL;
  JaguarString block_name( vhGetLabel( vh_block ));
  if (block_name != NULL) {
    name = context->vhdlIntern(block_name);
  } else {
    name = context->getDeclarationScope()->
      newBlockName(context->getStringCache(), loc);
  }  
  return name;
}

void VhdlPopulate::createDeclarationScope(vhNode vh_block, StringAtom* declScope,
                                          LFContext* context, const SourceLocator* loc)
{
  if (declScope == NULL) {
    declScope = getSeqBlockDeclScopeName(vh_block, context);
  }
  
  NUNamedDeclarationScope *declaration_scope;
  declaration_scope = new NUNamedDeclarationScope(declScope,
                                                  context->getDeclarationScope(),
                                                  mIODB,
                                                  mNetRefFactory,
                                                  *loc);
  context->pushDeclarationScope(declaration_scope);
}


// This helper method is called from processStmt and concProcedureCall
// to create the relevant scopes.  The various context scope stacks must
// be cleaned up by whomever calls this method.
void
VhdlPopulate::sequentialBlock(vhNode vh_block, StringAtom* declScope,
                              LFContext *context,
                              const SourceLocator *loc,
                              NUBlock **the_block)
{
  createDeclarationScope(vh_block, declScope, context, loc);
  *the_block =
    context->stmtFactory()->createBlock(context->getBlockScope(), false, *loc);

  context->pushBlockScope(*the_block);
  context->pushJaguarScope(vh_block);
}


/*
 * Function to create a dummy always block(i.e VHDL process stmt)
 * in order to process the conditional and selected signal assignment
 * statements.
 */
Populate::ErrorCode VhdlPopulate::dummyAlwaysBlock(vhNode vh_sig_assgn,
                                                   LFContext *context,
                                                   NUAlwaysBlock **the_process)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;
  *the_process = 0;
  NUStmtList stmt_list;
  context->pushStmtList(&stmt_list);

  // Now create a block inside the process stmt
  NUStmtList stmts; // Stmts within the block
  SourceLocator loc = locator(vh_sig_assgn);

  NUBlock * block = new NUBlock(&stmts,
                                context->getBlockScope(),
                                mIODB,
                                mNetRefFactory,
                                false,
                                loc);
  context->pushBlockScope(block);
  context->pushJaguarScope(vh_sig_assgn);
  context->pushStmtList(&stmts);
  NUStmtList guarded_if_then_stmts; // Statements corresponding to IF-THEN stmts
                                    // of a guarded signal assignment
  if (vhIsGuarded(vh_sig_assgn))
  {
    context->pushStmtList(&guarded_if_then_stmts);
  }
  switch (vhGetObjType(vh_sig_assgn))
  {
  case VHCONCSIGASGN:
  {
    NUNonBlockingAssign* the_stmt = 0;
    temp_err_code = signalAssign(vh_sig_assgn, context, &the_stmt);
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
    {
      return temp_err_code;
    }
    if (the_stmt)
    {
      context->addStmt(the_stmt);
    }
    break;
  }
  break;
  case VHCONDSIGASGN:
  {
    JaguarList waveFrmList(vhGetConditionalWaveFrmList(vh_sig_assgn));
    temp_err_code = conditionalSigAsgn(vh_sig_assgn,context,
                                       waveFrmList);
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
    {
      return temp_err_code;
    }
  }
  break;
  case VHSELSIGASGN: 
  {
    JaguarList waveFrmList(vhGetSelWaveFrmList(vh_sig_assgn)), emptyList;
    temp_err_code = selectedSigAsgn(vh_sig_assgn, context, 
                                    waveFrmList, NULL, emptyList);
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
    {
      return temp_err_code;
    }
  }
  break;
  default:
    NU_ASSERT(0, block);
    break;
  }
  // If it is guarded signal assignment then process this statement
  // as if it is under the 'IF (GUARD EXPR) THEN' condition.
  if (vhIsGuarded(vh_sig_assgn))
  {
    context->popStmtList(); // pop the guarded_if_then_stmts
    NUStmtList guarded_if_else_stmts; // An empty else stmt list
    NUExpr *the_guard_expr = 0;
    vhNode vh_block = vhGetScope(vh_sig_assgn);
    vhExpr vh_guard_expr = vhGetGuardExpr(vh_block);
    int dummy_edge;
    if ( detectClock( vh_guard_expr, &dummy_edge ))
    {
      // A guarded block sensitive to a clock edge is unsupported
      mMsgContext->ClockOnGuardedBlock(&loc); 
      err_code = eFatal;
    }
    temp_err_code = condExpr(vh_guard_expr, context, &the_guard_expr);
    if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
    {
      return temp_err_code;
    }

    NUIf* the_if = new NUIf(the_guard_expr, guarded_if_then_stmts,
                            guarded_if_else_stmts, mNetRefFactory, false, loc);
    context->addStmt(the_if);
  }

  block->addStmts(&stmts);
  context->popStmtList();
  context->popBlockScope();
  context->popJaguarScope();

  StringAtom * always_name = context->getModule()->newBlockName(context->getStringCache(), loc);
  *the_process = new NUAlwaysBlock(always_name, block, mNetRefFactory, loc, false);
  // Now pop the sequential stmt list 
  context->popStmtList();
  return err_code;
}

// This function is common to both VerilogPopulate and VhdlPopulate classes.
// So, we should move this function in a common base class, once we come
// up with that common base class.
Populate::ErrorCode VhdlPopulate::maybeFixConditionStmt(NUExpr** cond,
                                                        const SourceLocator& loc)
{
  ErrorCode err_code = eSuccess;
  NUOp* condAsOp = dynamic_cast<NUOp*>(*cond);
  bool doComparison = false;
  if (condAsOp)
  {
    switch (condAsOp->getOp())
    {
    case NUOp::eUnBitNeg:
    case NUOp::eUnVhdlNot:
    case NUOp::eBiPlus:                
    case NUOp::eBiMinus:               
    case NUOp::eBiSMult:                
    case NUOp::eBiSDiv:                 
    case NUOp::eBiSMod:
    case NUOp::eBiUMult:                
    case NUOp::eBiUDiv:                 
    case NUOp::eBiUMod:
    case NUOp::eBiExp:                 
    case NUOp::eBiBitAnd:              
    case NUOp::eBiBitOr:               
    case NUOp::eBiBitXor:              
    case NUOp::eBiRoR:
    case NUOp::eBiRoL:             
    case NUOp::eBiRshift:              
    case NUOp::eBiLshift:              
    case NUOp::eBiVhLshift:              
    case NUOp::eBiVhRshift:              
    case NUOp::eBiVhRshiftArith:              
    case NUOp::eBiVhLshiftArith:              
    case NUOp::eNaConcat:
      doComparison = true;
      break;
    default:
      break;
    }//switch
  }
  else if ((dynamic_cast<NUIdentRvalue*>(*cond) != NULL) ||
           (dynamic_cast<NUVarselRvalue*>(*cond) != NULL) ||
           ((*cond)->castConst() != NULL))
    doComparison = true;

  if (doComparison)
  {
    // The expression is an operator or an ident. Must  make sure the
    // eventual operator is a boolean
    UInt32 condSize = (*cond)->determineBitSize();
    NUConst* zero = NUConst::create(false, UInt64(0),  condSize, loc);
    // don't need to copy() the condition, since it isn't owned by
    // anything.
    NUExpr* superNeq = new NUBinaryOp(NUOp::eBiNeq, *cond, zero, loc);
    *cond = superNeq;
  }
  return err_code;
}


Populate::ErrorCode VhdlPopulate::sequentialStmt(vhNode vh_stmt, 
                                                 LFContext *context)
{
  ErrorCode err_code = eSuccess;
  NUStmt *the_stmt = 0;
  JaguarList emptyList;

  SourceLocator loc = locator( vh_stmt );
  vhObjType type = vhGetObjType(vh_stmt);
  switch (type) {
  case VHVARASGN:
  {
    NUBlockingAssign* asn_stmt=0;
    err_code = variableAssign(vh_stmt, context, &asn_stmt);
    the_stmt = asn_stmt;
    break;
  }
  case VHSIGASGN:
  {
    NUNonBlockingAssign* asn_stmt=0;
    err_code = signalAssign(vh_stmt, context, &asn_stmt);
    the_stmt = asn_stmt;
    break;
  }
  case VHIF:
  {
    NUBlockStmt* if_stmt=0;
    err_code = ifStmt(vh_stmt, context, &if_stmt);
    the_stmt = if_stmt;
    break;
  }
  case VHFOR:
  {
    bool loopUnrolled = true;
    ErrorCode temp_err_code = unrollLoop(vh_stmt, context, &loopUnrolled, &the_stmt);
    if (loopUnrolled) {
      // Check if loop was successfully unrolled.
      if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code))
        return temp_err_code;
      break; // Avoid fall through since the loop was unrolled
    }
    // Deliberate fall through to construct a loop statement if loop was not unrolled.
  }
  case VHLOOP:
  case VHWHILE:
  {
    JagDefCollector ldc(this, context, mMsgContext, mCarbonContext->isNewRecord());
    {
      JaguarBrowser jb(ldc);
      jb.browse(vh_stmt);
    }
    if (!ldc.getSuccess()) {
      err_code = eFailPopulate;
    } else {
      err_code = loopStmt(vh_stmt, context, ldc.getDefs(), &the_stmt);
    }
    break;
  }
  case VHRETURN:
  {
    NUBlockingAssign* asn_stmt=0;
    err_code = returnStmt(vh_stmt, context, &asn_stmt);
    the_stmt = asn_stmt;
    if ( err_code != eSuccess )
    {
      break;
    }
    if ( the_stmt ) 
    {
      context->addStmt( the_stmt );
    }

    err_code = exitStmt( vh_stmt, context, &the_stmt );
    break; 
  }
  case VHPROCEDURECALL:
    err_code = seqProcedureCall(vh_stmt, context);
    break; 
  case VHWAIT:
    // We have aleady extracted the process clock for this WAIT stmt, so
    // we don't need to do any other processing for the clock.  We don't
    // yet handle gated wait statement clocks, so we detect and error on
    // that.
  {
    vhExpr vh_cond = vhGetConditionClause( vh_stmt );
    if ( vh_cond )
    {
      // vhDetectClockInWaitStmt returns the record signal when a record
      // element is a clock, which is not useful.  Thus, we need to get
      // the clock from the process, not the condition clause.
      vhNode vh_scope = vhGetScope( vh_stmt );
      while ( vhGetObjType( vh_scope ) != VHPROCESS )
      {
        vh_scope = vhGetScope( vh_scope );
      }
      LOC_ASSERT( vh_scope, loc );
      vhNode vh_clock = vhGetProcessClockExpr( vh_scope );
      if ( vh_clock == NULL ) // work around Jaguar inconsistency/bug.
        vh_clock = vhGetProcessClock( vh_scope );

      if ( vhGetObjType( vh_clock ) == VHEXTERNAL || vhGetObjType( vh_clock ) == VHOBJECT )
        vh_clock = vhGetActualObj( vh_clock );
      NUNet *clockNet;
      if ( vh_clock )
      {
        ErrorCode temp_err_code = lookupNet( context->getDeclarationScope(), vh_clock,
                                             context, &clockNet );
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
          return temp_err_code;

        // In a wait statement in synth code a function call must be either
        // RISING_EDGE or FALLING_EDGE, so we will just accept function calls.
        if ( vhGetObjType( vh_cond ) != VHFUNCCALL )
        {
          NUExpr *condExpr;
          // condExpr will eventually be used to create a gated clock for a wait statement.
          temp_err_code = expr( vh_cond, context, 1, &condExpr );
          if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
            return temp_err_code;
          
          NUNetRefSet clockSet( mNetRefFactory ), condSet( mNetRefFactory );
          clockSet.insert( mNetRefFactory->createNetRef( clockNet ));
          condExpr->getUses( &condSet );
          if ( clockSet != condSet )
          {
            POPULATE_FAILURE(mMsgContext->ClockingError( &loc, "Gated clocks in wait statements are not yet supported." ), &err_code);
          }
          delete condExpr;
        }
      } // end if there's a clock
    } // end if there's a condition in the wait
  }
  break;
  case VHCASE: 
  {
    NUCase* case_stmt=0;
    err_code = caseStmt(vh_stmt, context, &case_stmt);
    the_stmt = case_stmt;
    break;
  }
  case VHNEXT:
  case VHEXIT:
    err_code = exitStmt(vh_stmt, context, &the_stmt);
    break;
  case VHNULL:
    break; // nothing to do for a NULL statement
  case VHASSERT:
    mMsgContext->AssertNotSupported( &loc );
    break;
  case VHREPORT:
    mMsgContext->ReportNotSupported( &loc );
    break;
  case VHSELSIGASGN:
  default:
    POPULATE_FAILURE(mMsgContext->UnExpectedObjectType(&loc,
                                                       gGetVhNodeDescription(vh_stmt),
                                                       "Unsupported statement."), &err_code);
    return err_code;
  } // switch

  if (err_code == eSuccess && the_stmt) 
  {
    context->addStmt(the_stmt);
  }
  return err_code;
}

// This function SHOULD return the bitwidth of the largest case condition value,
// but it does something entirely useless computing the array bounds of ranges...
//
int VhdlPopulate::getMaxCaseExprSize(vhNode vh_case, LFContext *context)
{
  ErrorCode err_code=eSuccess;
  vhNode vh_sel = vhGetCaseExpr(vh_case);
  SourceLocator loc = locator(vh_sel);
  int curSize = 0;
  int numDims = 0;
  getVhExprBitSize(static_cast<vhExpr>(vh_sel), &curSize,
                   &numDims,
                   loc, context,
                   false, false); // rvalue and no error reporting

  JaguarList vh_case_item_list(vhGetAlternativeList(vh_case));
  // Loop through the case items
  vhNode vh_case_item;
  while ((vh_case_item = vhGetNextItem(vh_case_item_list)) != NULL)
  {
    JaguarList vh_choice_list(vhGetChoiceList(static_cast<vhExpr>(vh_case_item)));
    vhNode vh_choice;
    while ((vh_choice = vhGetNextItem(vh_choice_list)) != NULL) 
    {
      if (vhGetObjType(vh_choice) != VHOTHERS)
      {
        SourceLocator loc = locator(vh_choice);
        int expSize = 0;
        if (vhGetObjType(vh_choice) == VHRANGE) 
        {
          // Take size of any end of the range. (eg. Left)
          vh_choice = vhGetLtOfRange(vh_choice);
          vh_choice = evaluateJaguarExpr( static_cast<vhExpr>(vh_choice));
        }  
        ErrorCode temp_err_code = getVhExprBitSize(static_cast<vhExpr>(vh_choice),
                                                 &expSize, &numDims, loc, context, false, false);
        if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        {
          return curSize;
        }
        curSize = std::max(curSize, expSize);
      }
    }
  }
  return curSize;
}

Populate::ErrorCode VhdlPopulate::caseStmt(vhNode vh_case,
                                           LFContext *context,
                                           NUCase **the_stmt)
{  
  ErrorCode err_code = eSuccess, temp_err_code;
  *the_stmt = 0;
  context->pushJaguarScope( vh_case );

  // Need the location
  const SourceLocator loc = locator(vh_case);
  
  // Get the case select expression
  vhExpr vh_sel = vhGetCaseExpr(vh_case);

  NUExpr* sel = 0;
  temp_err_code = expr(vh_sel, context, 0, &sel);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    context->popJaguarScope();
    return temp_err_code;
  }
  
  int maxExprSize = sel->determineBitSize ();
  sel->resize(maxExprSize);

  // If select is a constant, then check if select expression type is a constrained
  // integer. If it is, it needs to be resized to 32 bits ..the unconstrained integer size.
  // This way it matches with case choices which are sized to 32 bits.
  if (sel->getType() == NUExpr::eNUConstNoXZ)
  {
    vhNode sel_expr_type = vhGetExpressionType(vh_sel);
    if (isConstrainedIntegerType(sel_expr_type)) {
      sel = sel->makeSizeExplicit(32);
      maxExprSize = sel->getBitSize();
    }
  }

  *the_stmt = context->stmtFactory()->createCase(sel, NUCase::eCtypeCase, false, loc);

  bool hasDefault = false;
  // Get the case items
  JaguarList vh_case_item_list(vhGetAlternativeList(vh_case));
  vhNode vh_case_item = 0;
  while ((vh_case_item = vhGetNextItem(vh_case_item_list)) != NULL)
  {
    // Create the case item on the list
    NUCaseItem* case_item = 0;
    temp_err_code = caseItem( vh_case_item, context,
                              &hasDefault, maxExprSize, &case_item, sel );
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
    {
      delete *the_stmt;
      *the_stmt = NULL;
      context->popJaguarScope();
      return temp_err_code;
    }

    // Check for empty statement list
    if (case_item != NULL)
      (*the_stmt)->addItem(case_item);
  }

  // While we don't have inline VHDL directives, the language enforces full case-ness.
  (*the_stmt)->putUserFullCase( true );
  (*the_stmt)->putFullySpecified( false ); // initially unknown
  (*the_stmt)->putHasDefault( hasDefault );
  // Now determine the correct value for fullySpecified.
  // If case is fully specified (without the need for default) then
  // the unnecessary default will be removed.
  temp_err_code = computeFullCase(the_stmt, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }

  context->popJaguarScope();
  // Done populating case statement.
  if (!context->stmtFactory()->next(*the_stmt)) {
    err_code = eFatal; // Don't continue populating once statement factory is out of sync.
  }
  return err_code;
}

Populate::ErrorCode VhdlPopulate::caseItem(vhNode vh_case_item,
                                           LFContext* context,
                                           bool* hasDefault,
                                           size_t maxExprSize,
                                           NUCaseItem **the_item,
                                           NUExpr* caseSel)
{
  ErrorCode err_code = eSuccess;
  *the_item = 0;

  // Need the location to create the binary op below
  SourceLocator loc = locator(vh_case_item);
  bool select_sign = caseSel->isSignedResult(); 
  // Make sure this isn't an empty default item. If so, we don't have
  // to create anything.
  JaguarList vh_stmt_list(vhGetSeqStmtList(vh_case_item));
  JaguarList vh_choice_list(vhGetChoiceList(static_cast<vhExpr>(vh_case_item)));
  // TBD : check if the below mentioned restriction is req'd in VHDL?
  //  => Make sure this isn't an empty default item. If so, we don't have
  //     to create anything.
  //  if ((ve_stmt == NULL) && (ve_expr_iter == NULL))
  //    return eSuccess;

  // Get the list of match expressions
  *the_item = context->stmtFactory()->createCaseItem(loc);
  bool hasChoices = false;
  vhExpr vh_choice = 0;
  while ((vh_choice = static_cast<vhExpr>(vhGetNextItem(vh_choice_list))) 
                                                                       != NULL)
  {
    if (vhGetObjType(vh_choice) != VHOTHERS) {
      // Get the item expression/choice
      NUExpr* the_expr = 0;
      if (vhGetObjType(vh_choice) == VHRANGE) 
      {
        int ltIndex = 0;
        int rtIndex = 0;
        vhExpr lt_bound  = vhGetLtOfRange(vh_choice);
        lt_bound = evaluateJaguarExpr( lt_bound );
        vhExpr rt_bound = vhGetRtOfRange(vh_choice);
        rt_bound = evaluateJaguarExpr( rt_bound );
        UtString lt_str; // For enum_encoded string results
        UtString rt_str; // For enum_encoded string results
        ErrorCode temp_err_code = getVhExprValue( lt_bound, &ltIndex, &lt_str);
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
        {
          return temp_err_code;
        }
        temp_err_code = getVhExprValue( rt_bound, &rtIndex, &rt_str );
        if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
        {
          return temp_err_code;
        }
        int tmp = ltIndex;
        if (VH_DOWNTO == vhGetRangeDir(vh_choice))
        {
          ltIndex = rtIndex;
          rtIndex = tmp;
        }

        if ( not lt_str.empty())
        {
          // Enum_encoded string attribute present in the range, therfore 
          // blast the range into individual literals covering the range. 
          tmp = 0;
          ErrorCode temp_err_code = getVhExprWidth(lt_bound, &tmp, loc, context);
          if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ))
          {
            return temp_err_code;
          }
          UInt32 bit_width = UInt32(tmp);
          vhExpr enum_defn_expr = static_cast<vhExpr>( 
                                vhGetActualObj(vhGetExpressionType(lt_bound)));
          // create a NUCaseCondition for every element in the range
          for (tmp = ltIndex; tmp <= rtIndex; tmp++)
          {
            // Get the  enum_encoding String value from the attribute used.
            getEncodedStringFromAttrib(&lt_str, enum_defn_expr, tmp);
            the_expr = (NUExpr*)NUConst::createKnownConst(lt_str, 
                                                           
                                                          bit_width,
                                                          false/*src_is_str*/,
                                                          loc);
            the_expr->setSignedResult(select_sign);
            the_expr->resize(maxExprSize);
            // Empty mask . This was req'd for casex casez in verilog.
            NUExpr* mask = NULL;
            (*the_item)->addCondition(new NUCaseCondition(loc,the_expr,mask));
            hasChoices = true;
          } 
        }
        else
        {
          // Create a comparision expression to represent the range.
          // 'ltIndex to rtIndex' => '((sel >= ltIndex) and (sel <= rtIndex))'
          // Create expr for ltIndex
          NUExpr* ltIndexExpr = (NUExpr*) NUConst::create(select_sign, ltIndex, 
                                                          32, loc);
          // Create expr for rtIndex
          NUExpr* rtIndexExpr = (NUExpr*) NUConst::create (select_sign,rtIndex, 
                                                           32, loc);
          // Create the final comparision Expr
          CopyContext copy_context(NULL,NULL);
          NUExpr *lhs = new NUBinaryOp(select_sign ? NUOp::eBiSGtre : NUOp::eBiUGtre, 
                                       caseSel->copy(copy_context), 
                                       ltIndexExpr, loc);
          NUExpr *rhs = new NUBinaryOp(select_sign ? NUOp::eBiSLte : NUOp::eBiULte,
                                       caseSel->copy(copy_context), 
                                       rtIndexExpr, loc);
          the_expr = new NUBinaryOp(NUOp::eBiLogAnd, lhs, rhs, loc);
          the_expr->resize(1);
          NUExpr* mask = NULL;
          (*the_item)->addCondition(new NUCaseCondition(loc, the_expr, mask, 
                                                        true/*isRangeExpr*/));
          hasChoices = true;
        }
      }
      else
      {
        ErrorCode temp_err_code = expr(vh_choice,
                                       context,
                                       static_cast<int>(maxExprSize),
                                       &the_expr);
        if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
        {
          return temp_err_code;
        }
        if (not the_expr)
          continue;

        the_expr->resize(maxExprSize);
        the_expr->setSignedResult(select_sign);
        // Empty mask . This was req'd for casex casez in verilog.
        NUExpr* mask = NULL;
        (*the_item)->addCondition(new NUCaseCondition(loc, the_expr, mask));
        hasChoices = true;
      }
    }
  }
  
  // Mark case item without expression as default.
  if (!hasChoices)
    *hasDefault = true;
  
  // Loop through the case item statements,
  NUStmtList stmts;
  context->pushStmtList(&stmts);
  ErrorCode temp_err_code = sequentialStmtList( &vh_stmt_list, context );
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
  {
    return temp_err_code;
  }
  context->popStmtList();

  for (NUStmtList::iterator p = stmts.begin(); p != stmts.end(); ++p)
    (*the_item)->addStmt(*p);

  // Done populating case item.
  if (!context->stmtFactory()->next(*the_item)) {
    err_code = eFatal; // Don't continue populating once statement factory is out of sync.
  }

  return err_code;
}

Populate::ErrorCode VhdlPopulate::returnStmt(vhNode vh_return,
                                             LFContext *context,
                                             NUBlockingAssign **the_return)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  *the_return = 0;
  SourceLocator loc = locator(vh_return);
  
  // for rvalue, we get the returning expression.  Note that for
  // a return from a procedure this will be NULL, which we will
  // check later before attempting to construct an assignment statement.
  vhExpr vh_rvalue = vhGetCondition(vh_return);
  if (vh_rvalue == NULL) {
    return err_code;
  }

  // For lvalue , we need to get the NUNet created earlier for the 'function'
  // owning this 'return statement'. The vhGetScope() API call to this vh_return
  // not give the SUBPROGBODY always. If this vh_return is inside a FORLOOP, it
  // will return the VHFOR as the scope of this vh_return. We need to upscope
  // till we hit the SUBPROGBODY.
  vhNode vh_func_body = vhGetScope(vh_return);
  while (vhGetObjType(vh_func_body) != VHSUBPROGBODY)
  {
    vh_func_body = vhGetScope(vh_func_body);
  }

  // We are looking for a subprogram here, not a for-loop.  But if we
  // are in a for loop in a function, vh_func_body will be the for-loop,
  // which is not what we want.  So pop up to a subprogram body, no matter
  // how many scopes we are nested into
  while (vhGetObjType(vh_func_body) != VHSUBPROGBODY) {
    vh_func_body = vhGetScope(vh_func_body);
    if (vh_func_body == NULL) {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Cannot find subprogram scope"), &err_code);
      return err_code;
    }
  }

  if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_rvalue, context ))
  {
    temp_err_code = recordAssign( eSequentialBlocking,
                                  static_cast<vhExpr>( vhGetScope( vh_rvalue )),
                                  vh_rvalue, context, loc );
    if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      return temp_err_code;
  }
  else
  {
    NUNet* net = 0;
    temp_err_code = lookupNet( context->getDeclarationScope(), vh_func_body,
                               context, &net );
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;
    NUIdentLvalue *the_lvalue = new NUIdentLvalue(net, loc);

    NUExpr* rvalue = 0;
    UInt32 lsize = the_lvalue->getBitSize();
    temp_err_code = expr(vh_rvalue, context, lsize, &rvalue);
    if ( temp_err_code == eNotApplicable )
    {
      // The only currently known reason to hit this code is if a clock
      // edge was used in a non-clocking situation.
      int dummy_edge = -1;
      if ( vhDetectClock( vh_rvalue, &dummy_edge ) && 
           ( dummy_edge == 0 || dummy_edge == 1 ))
      {
        mMsgContext->FunctionReturningClock( &loc );
      }
    }
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    {
      delete the_lvalue;
      if ( rvalue )
        delete rvalue;
      mMsgContext->JaguarConsistency( &loc, "Unable to create internal representation for return statement." );
      return temp_err_code;
    }
    
    *the_return = context->stmtFactory()->createBlockingAssign(the_lvalue, rvalue, false, loc);
    // If the return expressions are being collected, add this one to the list.
    if (mReturnExprs != NULL) {
      NUExpr* stmt_rhs = (*the_return)->getRvalue();
      if (stmt_rhs != NULL) {
        mReturnExprs->push_back(stmt_rhs);
      }
    }
  }
 
  return err_code;
}


// Function that creates a NUOp::eBiEq expression with the select and choice
// expression of a VHDL case alternative. 
Populate::ErrorCode VhdlPopulate::caseAlternativeCond(vhExpr n_select, 
                                                      vhExpr n_choice,
                                                      LFContext *context, 
                                                      NUExpr** the_expr)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc_c = locator(n_choice);
  SourceLocator loc_s = locator(n_select);
  NUExpr* lhs = 0;
  NUExpr* rhs = 0;
  NUExpr* cond_expr;
  int lWidth = 0;
  ErrorCode temp_err_code = getVhExprWidth(n_select,&lWidth, loc_s, context);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code))
  {
    return temp_err_code;
  }
  temp_err_code = expr(n_select, context, lWidth, &lhs);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code))
  {
    return temp_err_code;
  }
  temp_err_code = getVhExprWidth(n_choice,&lWidth, loc_c, context);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code))
  {
    return temp_err_code;
  }
  temp_err_code = expr(n_choice, context, lWidth, &rhs);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code))
  {
    return temp_err_code;
  }

  cond_expr = new NUBinaryOp(NUOp::eBiEq, lhs, rhs, loc_c);
  cond_expr->setSignedResult (0);
  temp_err_code = maybeFixConditionStmt(&cond_expr, loc_c);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code))
  {
    return temp_err_code;
  }
  cond_expr->resize(1);
  *the_expr = cond_expr;
  return err_code;
}

// Function: condExpr()
// Description: This function processes the Jaguar condition expression
//              and creates a NUExpr corresponding to the jaguar cond expr.
//              After creating the NUExpr, it does certain sanity checking
//              on that to validate the created NU expr.
Populate::ErrorCode VhdlPopulate::condExpr(vhExpr vh_cond, 
                                           LFContext *context, 
                                           NUExpr** cond)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  *cond = 0;
  NUExpr* cond_expr = 0;
  SourceLocator loc = locator(vh_cond);
  temp_err_code = expr( vh_cond, context, 0, &cond_expr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    return temp_err_code;
  }
  temp_err_code = maybeFixConditionStmt(&cond_expr, loc);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    return temp_err_code;
  }

  cond_expr->resize(1);
  *cond = cond_expr;
  return err_code;
}


Populate::ErrorCode VhdlPopulate::createForStmtIndex(vhNode vh_for_index,
                                                     LFContext* context,
                                                     NUNet** for_index_net)
{
  ErrorCode err_code = eSuccess;
  // Create a variable (NUVectorNet) representing for_index (<identifier>. 
  // We use gensym to  generate unique name with the prefix "$for_index".
  *for_index_net = 0;
  ErrorCode temp_err_code = var(vh_for_index, context, for_index_net, 
                                "for_index"/*prefix*/, false /*isDeclaredVar*/);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code)) {
    return temp_err_code;
  }
  NUScope *declaration_scope = context->getDeclarationScope();
  declaration_scope->addLocal(*for_index_net); 
  return err_code;
}

// Function : createForStmtControlObjects
// Description: To create verilog style for_loop control object initializing 
//              stmt, advancing stmt and condition evaluation expression from
//              VHDL for's loop_parameter_spec (or for_index) 
//        In VHDL a 'for' loop is represented as 
//     [<loop_label> :] 
//        for <identifier> in <discrete_range> loop
//            <sequence_of_statements>
//        end loop [ <loop_label> ] 
// In the nucleus, NUFor class represents the verilog style 'for' statement, 
// where we have  1) Stmt initializing loop control variable 
//                2) A condition expression 
//            and 3) An advancing Stmt on loop control variable
// For eg.  
//    for loop_var in 4 to 9  -- vh_for_index or loop_parameter_spec 
//    loop 
//       <stmts>
//    end loop
//
// For the above VHDL stmt we create 
//   the_initial   ===> 'loop_var = 4'
//   the_condition ===> '(loop_var <= 9)'
//   the_advance   ===> ' loop_var = loop_var + 1'
// The above three objects are used to populate NUFor object.
Populate::ErrorCode
VhdlPopulate::createForStmtControlObjects( vhNode vh_for_index,
                                           LFContext *context,
                                           NUExpr           **the_condition,
                                           NUIdentLvalue** the_lvalue,
                                           NUExpr** the_advance_expr)
{

  ErrorCode err_code = eSuccess, tmp_err;
  SourceLocator loc = locator( vh_for_index );
  vhNode range = vhGetDisRange( static_cast<vhExpr>( vh_for_index ));
  vhNode vh_range = vhElaborateSizeNode( range );
  vhDirType direction = vhGetRangeDir( vh_range );
  while ( direction == VH_ERROR_DIR )
  {
    // This uses an attribute to specify the range.
    vhNode vh_attrib = vhGetRangeAttrib( vh_range );
    tmp_err = getVhRangeNode( vh_attrib, &vh_range, NULL );
    if ( errorCodeSaysReturnNowWhenFailPopulate( tmp_err, &err_code ))
    {
      return tmp_err;
    }
    direction = vhGetRangeDir( vh_range );
  }

  // Create expr for leftIndx
  NUExpr* ltIndexExpr = NULL;
  int lWidth = 0;
  vhExpr vh_left = vhGetLtOfRange(vh_range);
  LOC_ASSERT( vh_left, loc );
  tmp_err = expr( vh_left, context, lWidth, &ltIndexExpr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( tmp_err, &err_code ))
  {
    return tmp_err;
  }

  // Create expr for rightIndx
  
  NUExpr* rtIndexExpr = NULL;
  vhExpr vh_right = vhGetRtOfRange( vh_range );
  LOC_ASSERT( vh_right, loc );
  tmp_err = expr( vh_right, context, lWidth, &rtIndexExpr);
  if ( errorCodeSaysReturnNowWhenFailPopulate(tmp_err, &err_code ))
  {
    return tmp_err;
  }
  // Create a variable (NUVectorNet) representing for_index (<identifier>. 
  // We use gensym to  generate unique name with the prefix "$for_index".
  NUNet* for_index_net = 0;
  ErrorCode temp_err_code = createForStmtIndex(vh_for_index, context, &for_index_net);
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code)) {
    return temp_err_code;
  }

  // Create expr for for_index (<identifier>)
  NUExpr* forIndexExpr;
  temp_err_code = expr( static_cast<vhExpr>(vh_for_index), context, 32, &forIndexExpr );
  if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code,&err_code))
  {
    return temp_err_code;
  }

  // If the for-loop index variable is signed, then we need to do signed arithmetic
  // uniformly in the condition and advance statements.
  //
  bool signedArith = forIndexExpr->isSignedResult ();

  // Now use this for_index(<identifier>) net to create a lvalue object.
  *the_lvalue = new NUIdentLvalue(for_index_net, loc);

  NUExpr* const_one = (NUExpr*) NUConst::create(signedArith, 1, 32, loc);
  // Create a initializing statement from the loop parameter spec.
  // for eg.from  "for loop_param_var in 4 to 7" we can create a assignment
  // stmt like "loop_param = 4".
  NUBlockingAssign* the_initial = 
    context->stmtFactory()->createBlockingAssign(*the_lvalue, ltIndexExpr, false, loc);
  context->addStmt(the_initial);

  // Create a conditional expression like in verilog for_stmt from the loop 
  // parameter spec. for eg from "for loop_param_var in 4 to 7" we can create
  // a conditional expr "loop_param <= 7".  
  NUOp::OpT  op = (direction == VH_TO) ? (signedArith ? NUOp::eBiSLte: NUOp::eBiULte)
                                       : (signedArith ? NUOp::eBiSGtre : NUOp::eBiUGtre);
  *the_condition = new NUBinaryOp(op, forIndexExpr, rtIndexExpr, loc);


  // Create an Assignment stmt using the loop_param, which will represent 
  // advancement of loop_param.
  // It will be 'loop_param = loop_param [+|-] 1;' depending on the direction
  // of the loop_param range. 
  CopyContext copy_context(NULL,NULL);
  op =  (direction == VH_TO) ? NUOp::eBiPlus : NUOp::eBiMinus;
  *the_lvalue = new NUIdentLvalue(for_index_net, loc);
  *the_advance_expr = new NUBinaryOp(op, forIndexExpr->copy(copy_context),
                                     const_one, loc);
  (*the_advance_expr)->setSignedResult(signedArith);

  return err_code;
}

// Function: loopStmt()
// Description: Function to process for/while/loop statement
// FOR LOOP : In VHDL a 'for' loop is represented as
//     [<loop_label> :] 
//        for <identifier> in <discrete_range> loop
//            <sequence_of_statements>
//        end loop [ <loop_label> ] 
// In the nucleus, NUFor class represents the verilog style 'for' statement, 
// where we have  1) Stmt initializing loop control variable 
//                2) A condition expression 
//            and 3) An advancing Stmt on loop control variable
// The routine createForStmtControlObjects() creates the above three objects
// from the for_loop_parameter_spec (containing <identifier> and 
//  <discrete_range>) 
//
// NOTE: To process exit statement we will create an internal exit net whose
//       initial value will be 1. This exit net will be tied with the
//       termination condition of the for/while/loop . When we will hit exit 
//       statement, we will assign 0 to that exit net. In this way the we
//       will be able to process the exit behavior of the for/while/loop.
//

Populate::ErrorCode VhdlPopulate::loopStmt(vhNode vh_loop,
                                           LFContext *context,
                                           NUNetSet& loopDefs,
                                           NUStmt **the_stmt)
{
  SourceLocator loc = locator(vh_loop);
  ErrorCode err_code = eSuccess;
  *the_stmt = 0;
  vhObjType loop_type = vhGetObjType(vh_loop);

  // Create scope with the label of this for loop. If the for loop
  // is not labelled, then create internal string for the label of this
  // for loop.
  JaguarString vh_label(vhGetLabel(vh_loop));
  StringAtom* label = NULL;
  if (vh_label) {
    label = context->vhdlIntern(vh_label);
  }
  else {
    const char* construct = NULL;
    if (VHWHILE == loop_type)
      construct = "WHILELOOP";
    else if (VHLOOP == loop_type)
      construct = "LOOP";
    else if (VHFOR == loop_type)
      construct = "FORLOOP";
    label = context->getModule()->gensym(construct,"LABEL");
  }

# define EXTRA_SCOPE 0
# if EXTRA_SCOPE
  NUNamedDeclarationScope *for_scope = new 
                   NUNamedDeclarationScope( label, 
                                            context->getDeclarationScope(), 
                                            mIODB, mNetRefFactory,
                                            loc );
  context->pushDeclarationScope(for_scope);
# endif

  context->pushJaguarScope(vh_loop);

  ErrorCode temp_err_code = eSuccess;

  NUStmtList exitBlockStmts;
  NUBlock* exitBlock = context->stmtFactory()->createBlock(context->getBlockScope(),
                                                           false,
                                                           loc);
  context->pushBlockScope(exitBlock);
  context->pushStmtList(&exitBlockStmts);
  mapBlock(vh_loop, exitBlock);

  NUStmtList initial_stmts;
  NUExpr *condition = 0;
  NUIdentLvalue* the_lvalue = 0;
  NUExpr* the_advance_expr = 0;

  NUFor* the_for = context->stmtFactory()->createFor(condition, false, loc, loopDefs);

  switch (loop_type)
  {
    case VHWHILE:
    {
      // normally a while statement has no initial statements, but if
      // there are function call(s) within the condition then we will
      // put the generated task enable(s) in the initial_stmts list, 
      // and a copy into the advance_stmts list (below)
      context->pushStmtList(&initial_stmts);
      temp_err_code = condExpr( vhGetCondition(vh_loop), context, 
                                &condition );
      context->popStmtList();   // initial_stmts
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code )) 
      {
        context->popStmtList();   // exitBlockStmts
        context->popBlockScope(); // exitBlock
        context->popJaguarScope();
# if EXTRA_SCOPE
        context->popDeclarationScope();
# endif
        return temp_err_code;
      }
      
      break;
    }
    case VHLOOP:
    {
      // We are counting on an 'exit' statement inside the loop body, otherwise
      // we never exit.  We will count on a NUFor condition analysis in
      // src/localflow/BreakResynth.cxx to issue an error in this situation.  All
      // we need to do locally is synthesize "for (; 1; ) ..."
      condition = NUConst::create(false, 1, 1, loc);
      break;
      
    }
    case VHFOR:
    {
      vhNode loop_param_spec = vhGetParamSpec(vh_loop);
      context->pushStmtList(&initial_stmts);
      temp_err_code = createForStmtControlObjects( loop_param_spec, 
                                                   context,
                                                   &condition, 
                                                   &the_lvalue, &the_advance_expr );
      context->popStmtList(); // initial stmts
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code )) {
        context->popStmtList();   // exitBlockStmts
        context->popBlockScope(); // exitBlock
        context->popJaguarScope();
# if EXTRA_SCOPE
        context->popDeclarationScope();
# endif
        return temp_err_code;
      }
      break;
    }
    default: {
      SourceLocator loc = locator(vh_loop);
      mMsgContext->UnExpectedObjectType(&loc,
                                        gGetVhNodeDescription(vh_loop) ,
                                        " while processing for/while/loop statement.");
        context->popStmtList();   // exitBlockStmts
        context->popBlockScope(); // exitBlock
        context->popJaguarScope();
# if EXTRA_SCOPE
        context->popDeclarationScope();
# endif
      return eFailPopulate;
    }
  }

  // Set for loop condition.
  condition->resize(1);
  the_for->setCondition(condition);

  // Set loop initial statements.
  the_for->replaceInitial(initial_stmts);
  // Done populating for loop initial statements.
  if (!context->stmtFactory()->next(the_for)) {
    context->popStmtList();   // exitBlockStmts
    context->popBlockScope(); // exitBlock
    context->popJaguarScope();
# if EXTRA_SCOPE
    context->popDeclarationScope();
# endif
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }

  // Create a block for to act as a target for the NUBreak statement
  // we create if we see a 'next' statement.  This block goes inside
  // the loop, and 'owns' the 
  NUBlock* nextBlock = context->stmtFactory()->createBlock(context->getBlockScope(),
                                                           false,
                                                           loc);

  mapNextBlock(vh_loop, nextBlock);
  NUStmtList nextBlockStmts;
  context->pushBlockScope(nextBlock);
  context->pushStmtList(&nextBlockStmts);

  vhNode vh_loopstmt = vh_loop;
  if (VHLOOP != vhGetObjType(vh_loop))
    vh_loopstmt = vhGetLoopStmt(vh_loop);
  JaguarList vh_stmts(vhGetSeqStmtList(vh_loopstmt));

  // The body of the for-loop get placed uner nextBlock
  temp_err_code = sequentialStmtList( &vh_stmts, context );
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
  {
    context->popBlockScope();     // nextBlock
    context->popStmtList();       // nextBlockStmts
    context->popStmtList();       // exitBlockStmts
    context->popBlockScope();     // exitBlock
    context->popJaguarScope();
# if EXTRA_SCOPE
    context->popDeclarationScope();
# endif
    return temp_err_code;
  }

  context->popBlockScope();     // nextBlock
  context->popStmtList();       // nextBlockStmts
  // Add statements to block.
  nextBlock->addStmts(&nextBlockStmts);
  // Done populating block.
  if (!context->stmtFactory()->next(nextBlock)) {
    context->popStmtList();       // exitBlockStmts
    context->popBlockScope();     // exitBlock
    context->popJaguarScope();
# if EXTRA_SCOPE
    context->popDeclarationScope();
# endif
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }

  // Now we make a fresh statement list for the for-loop that
  // contains just the NUBlock.  We're not going to bother pushing
  // it onto the context.
  NUStmtList body_stmts;
  body_stmts.push_back(nextBlock);
  unmapNextBlock(vh_loop, nextBlock);
  // Set for loop body statements.
  the_for->replaceBody(body_stmts);
  // Done populating for body.
  if (!context->stmtFactory()->next(the_for)) {
    context->popStmtList();       // exitBlockStmts
    context->popBlockScope();     // exitBlock
    context->popJaguarScope();
# if EXTRA_SCOPE
    context->popDeclarationScope();
# endif
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }

  NUStmtList advance_stmts;
  if (loop_type == VHFOR) {
    NUBlockingAssign* adv_stmt = 
      context->stmtFactory()->createBlockingAssign(the_lvalue, the_advance_expr,
                                                   false, loc);
    advance_stmts.push_back(adv_stmt);
  } else if ((loop_type == VHWHILE) && (not initial_stmts.empty())) {
    // There's a function call within the while condition. We have some advance
    // statements to populate.
    context->pushStmtList(&advance_stmts);
    CopyContext cc(NULL, NULL);
    context->stmtFactory()->copyStmtList(initial_stmts, &advance_stmts, cc);
    context->popStmtList();
  }

  // Set for loop advance statements.
  the_for->replaceAdvance(advance_stmts);
  // Done populating for advance statements and thus the for loop.
  if (!context->stmtFactory()->next(the_for)) {
    context->popStmtList();       // exitBlockStmts
    context->popBlockScope();     // exitBlock
    context->popJaguarScope();
# if EXTRA_SCOPE
    context->popDeclarationScope();
# endif
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }

  // The only statement in the exit block is the loop itself.
  exitBlockStmts.push_back(the_for);
  context->popBlockScope();     // exitBlock
  context->popStmtList();       // exitBlockStmts
  // Set exit block statements.
  exitBlock->addStmts(&exitBlockStmts);
  // Done populating exit block statements.
  if (!context->stmtFactory()->next(exitBlock)) {
    context->popJaguarScope();
# if EXTRA_SCOPE
    context->popDeclarationScope();
# endif
    return eFatal; // Don't continue populating once statement factory is out of sync.
  }
  *the_stmt = exitBlock;

  unmapBlock(vh_loop, exitBlock);


# if EXTRA_SCOPE
  context->popDeclarationScope();
# endif
  context->popJaguarScope();
  return err_code;
}

bool sAreValidStmtsForLoopUnroll(JaguarList& stmts)
{
  bool areValidStmts = true;
  vhNode vh_stmt = NULL;
  while ((vh_stmt = vhGetNextItem(stmts)) && areValidStmts)
    areValidStmts &= sIsValidStmtForLoopUnroll(vh_stmt);
  return areValidStmts;
}

bool sIsValidStmtForLoopUnroll(vhNode vh_stmt)
{
  bool isValidStmt = true;

  vhObjType type = vhGetObjType(vh_stmt);
  switch(type)
  {
  case VHRETURN:
  case VHNEXT:
  case VHEXIT:
  case VHLOOP: // counts on an exit/return to exit.
    isValidStmt = false;
    break;
  case VHELSIF:
  case VHIF:
  {
    // Common to if and elseif.
    {
      JaguarList then_stmts(vhGetThenSeqStmtList(vh_stmt));
      isValidStmt &= sAreValidStmtsForLoopUnroll(then_stmts);
    }
    if (isValidStmt && (type == VHIF)) {
      JaguarList elseif_stmts(vhGetElsifSeqStmtList(vh_stmt));
      isValidStmt &= sAreValidStmtsForLoopUnroll(elseif_stmts);
    }
    if (isValidStmt && (type == VHIF)) {
      JaguarList else_stmts(vhGetElsSeqStmtList(vh_stmt));
      isValidStmt &= sAreValidStmtsForLoopUnroll(else_stmts);
    }
    break;
  }
  case VHCASE:
  {
    JaguarList case_item_list(vhGetAlternativeList(vh_stmt));
    vhNode vh_case_item = NULL;
    while ((vh_case_item = vhGetNextItem(case_item_list)) && isValidStmt)
    {
      JaguarList case_item_stmts(vhGetSeqStmtList(vh_case_item));
      isValidStmt &= sAreValidStmtsForLoopUnroll(case_item_stmts);
    }
    break;
  }
  case VHFOR:
  case VHWHILE:
  {
    vhNode vh_loopstmt = vhGetLoopStmt(vh_stmt);
    JaguarList loop_stmts(vhGetSeqStmtList(vh_loopstmt));
    isValidStmt &= sAreValidStmtsForLoopUnroll(loop_stmts);
    break;
  }
  default:
    break;
  }

  return isValidStmt;
}

Populate::ErrorCode
VhdlPopulate::analyzeLoopForUnroll(vhNode vh_loop, ConstantRange& cr,
                                   vhDirType* direction, LFContext* context,
                                   bool* shouldUnrollLoop)
{
  ErrorCode err_code = eSuccess;
  *shouldUnrollLoop = false;
  // A loop is unrollable if:
  // 1. It is a bounded loop, with bounds statically known.
  // 2. Loop type is VHFOR. Can't unroll VHLOOP ..unbounded loops, and VHWHILE,
  //    the conditional loops.
  // 3. And loop bounds are such that loop does not iterate more than
  //    a maximum number of iterations.
  if ((vh_loop != NULL) && (vhGetObjType(vh_loop) == VHFOR))
  {
    LoopAnalyzer la;
    {
      JaguarBrowser jb(la);
      jb.browse(vh_loop);
    }

    // Unroll loop only if user forced it or if it's needed for a successful
    // population of function calls within loops. Unrolling it all the time slows down
    // compile ..especially mpheonix perf test, since we'd be populating the same set
    // of statements for every unrolled iteration, which could be slow for large
    // loop ranges.
    if ( la.hasValidStmtsForUnroll() &&
         ( la.needsUnrolling() ||
           mArg->getBoolValue(CarbonContext::scVhdlLoopUnroll) ) )
    {
      vhNode loop_idx = vhGetParamSpec(vh_loop);
      vhNode range = vhGetDisRange(static_cast<vhExpr>(loop_idx));
      *direction = vhGetRangeDir(range);

      SourceLocator loc = locator(vh_loop);
      VhdlRangeInfo ri;
      ErrorCode temp_err_code =
        getLeftAndRightBounds(range, &ri, context, true /*reportError*/);
      if ( (temp_err_code == eSuccess) && ri.isValid() &&
           (ri.getWidth(loc) <= scMaxLoopUnrollIterations) &&
           (*direction != VH_ERROR_DIR) ) {
        cr.setLsb(ri.getLsb(loc));
        cr.setMsb(ri.getMsb(loc));
        *shouldUnrollLoop = true;
      }
    }
  }
  return err_code;
}

bool VhdlPopulate::isUnrolledLoopScope() const
{
  return (mUnrolledLoopScopeCount > 0);
}

Populate::ErrorCode VhdlPopulate::unrollLoop(vhNode vh_loop, LFContext* context,
                                             bool* loopUnrolled, NUStmt **the_stmt)
{
  // Elaborate loop, initializing the loop index
  *loopUnrolled = false;
  ErrorCode err_code = eSuccess;

  // First check if the loop is unrollable.
  ConstantRange cr;
  vhDirType direction = VH_ERROR_DIR;
  bool shouldUnrollLoop = false;
  ErrorCode temp_err_code = analyzeLoopForUnroll(vh_loop, cr, &direction, context,
                                                 &shouldUnrollLoop);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code)) {
    return temp_err_code;
  }

  if (shouldUnrollLoop)
  {
    *loopUnrolled = true; // Loop is being unrolled.
    *the_stmt = NULL;

    NUNet* loop_idx_net = 0;
    vhNode vh_loop_idx = vhGetParamSpec(vh_loop);
    ErrorCode temp_err_code = createForStmtIndex(vh_loop_idx, context, &loop_idx_net);
    if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code)) {
      return temp_err_code;
    }

    bool is_null_range = false;
    SInt32 incr = 1;
    if (direction == VH_DOWNTO) {
      incr = -1;
      if (cr.getMsb() < cr.getLsb()) {
        is_null_range = true;
      }
    } else if (direction == VH_TO) {
      if (cr.getMsb() > cr.getLsb()) {
        is_null_range = true;
      }
    } else {
      // This should never happen. We've checked for it in createForStmtIndex.
      NU_ASSERT(0, loop_idx_net);
    }

    SourceLocator loc = locator(vh_loop);
    if (is_null_range) {
      UtString slice_str, index_str;
      mMsgContext->NullRangeDetected(&loc, "The for loop has null range",
                                     decompileJaguarNode(vh_loop_idx, &slice_str),
                                     cr.format(&index_str));
    }

    NUStmtList loop_stmts;
    NUBlock* loop_blk = NULL;
    vhNode vh_loopstmt = vhGetLoopStmt(vh_loop);
    for (SInt32 loop_idx = cr.getMsb();
         !is_null_range && cr.contains(loop_idx) && (err_code == eSuccess);
         loop_idx += incr)
    {
      // Create a exit block to put unrolled loop iterations into. This allows
      // exit <loop> statements to jump out of this block.
      if (loop_blk == NULL) {
        context->pushJaguarScope(vh_loop);
        // Rolled loops don't check if a loop index based select is within valid
        // range of net being selected. Disable this error.
        if (mUnrolledLoopScopeCount == 0) {
          vhModifyMsgSeverity(446, VH_UNDISPLAY);
        }
        // Bump up unrolled loop scope count to indicate we're within that context.
        ++mUnrolledLoopScopeCount;
        loop_blk = context->stmtFactory()->createBlock(context->getBlockScope(),
                                                       false, loc);
        context->pushBlockScope(loop_blk);
        context->pushStmtList(&loop_stmts);
        mapBlock(vh_loop, loop_blk);
      }

      // Create a next block for each unrolled loop iteration. This allows
      // next <loop> statements to jump out of current iteration.
      NUBlock* next_blk = 
        context->stmtFactory()->createBlock(context->getBlockScope(), false, loc);
      mapNextBlock(vh_loop, next_blk);
      NUStmtList iter_stmts;
      context->pushBlockScope(next_blk);
      context->pushStmtList(&iter_stmts);

      // Insert a blocking assign statement assigning the current loop index
      // to loop index variable.
      {
        NULvalue* lval = new NUIdentLvalue(loop_idx_net, loc);
        UInt32 rvalSize = lval->determineBitSize();
        NUExpr*   rval = NUConst::create(false, loop_idx, rvalSize, loc);
        NUBlockingAssign* idx_asn =
          context->stmtFactory()->createBlockingAssign(lval, rval, false, loc);
        iter_stmts.push_back(idx_asn);
      }

      JaguarList vh_stmts(vhGetSeqStmtList(vh_loopstmt));
      // The statements from elaborated loop should replace for index with
      // it's current static value.
      err_code = sequentialStmtList(&vh_stmts, context);

      context->popBlockScope(); // next_blk
      context->popStmtList();   // next_blk statements
      unmapNextBlock(vh_loop, next_blk);

      next_blk->addStmts(&iter_stmts);
      if (!context->stmtFactory()->next(next_blk)) { // Done populating next_blk
        context->popBlockScope(); // loop_blk
        context->popStmtList(); // loop_stmts
        unmapBlock(vh_loop, loop_blk);
        context->popJaguarScope();
        return eFatal; // Don't continue populating once statement factory is out of sync.
      }
      loop_stmts.push_back(next_blk);
    }

    if (loop_blk != NULL) {
      context->popBlockScope();
      context->popStmtList();
      unmapBlock(vh_loop, loop_blk);
      context->popJaguarScope();
      // We're out of unrolled loop scope.
      --mUnrolledLoopScopeCount;
      // Re-enable the error message for out of range selects.
      if (mUnrolledLoopScopeCount == 0) {
        vhModifyMsgSeverity(446, VH_ERROR);
      }

      if (loop_stmts.empty()) {
        delete loop_blk;
      } else {
        loop_blk->addStmts(&loop_stmts);
        if (!context->stmtFactory()->next(loop_blk)) { // Done populating loop_blk
          delete loop_blk;
          return eFatal; // Don't continue populating once statement factory is out of sync.
        }
        *the_stmt = loop_blk;
      }
    }
  }

  return err_code;
}

// Process the 'EXIT' statement. As of now we will support EXIT stmt inside
// a for loop. We already have created an internal exit_net which will be 
// assigned FALSE corresponding to this exit stmt. If the exit stmt contains 
// condition, we will put the assignment inside a IF stmt where the if condition
// will be the condition given in the exit statement.
//
// Example:
//
// for1: for i in 0 to 100 loop
//   seq_stmt...
//   exit EXIT_COND;
//   other_seq_stmt ...
// end loop for1;
//  
// Populated as:
//
// for1_exit = 1;
// for (int i = 0; i < 100 and for1_exit; i = i + 1)
// {
//   if (for1_exit)
//     seq_stmt...
//   if (for1_exit)
//     if (EXIT_COND)
//     {
//       for1_exit = 0;
//     }
//   if (for1_exit)
//     other_seq_stmt ...
// }
//
// TBD: Exit to a loop other than the current for loop is not supported.
//
// This function has been reused for the return statement processing of
// a function. For the return statement, get the exit net of that function
// and assign 0 to it. 
// bug#3311: Incorrect handling of return statement in VHDL
// KLG, 12/02/2004
//
Populate::ErrorCode VhdlPopulate::exitStmt(vhNode vh_exit,
                                           LFContext *context,
                                           NUStmt** the_exit)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(vh_exit);
  vhObjType exit_kind = vhGetObjType(vh_exit);
  vhNode exit_scope = vhGetScope(vh_exit);
  const char* exitLabel = "";
  const char* exitKeyword = NULL;
  vhExpr vh_exit_cond = NULL;
  NUBlock* block = NULL;

  if ((VHEXIT == exit_kind) || (VHNEXT == exit_kind)) {
    exit_scope = vhGetAffectedLoop(vh_exit);
    LOC_ASSERT(exit_scope, loc);
    vh_exit_cond = vhGetCondition(vh_exit);

    if (VHEXIT == exit_kind) {
      vhNode exit_label = vhGetExitLabelObj(vh_exit);
      exitKeyword = "EXIT";
      if (exit_label != NULL) {
        exit_scope = vhGetMaster(exit_label);
        exitLabel = vhGetName(exit_label);
      }
      VhNodeBlockStackMap::iterator p = mBlockMap.find(exit_scope);
      if (p != mBlockMap.end()) {
        block = p->second->top();
      }
    }
    else {
      vhNode next_label = vhGetNextLabelObj(vh_exit);
      exitKeyword = "NEXT";
      if (next_label != NULL) {
        exit_scope = vhGetMaster(next_label);
        exitLabel = vhGetName(next_label);
      }
      VhNodeBlockMap::iterator p = mNextBlockMap.find(exit_scope);
      if (p != mNextBlockMap.end()) {
        block = p->second;
      }
    }
  } // if
  else if ( VHRETURN == exit_kind ) {
    while (vhGetObjType(exit_scope) != VHSUBPROGBODY) {
      exit_scope = vhGetScope(exit_scope);
    }
    VhNodeBlockStackMap::iterator p = mBlockMap.find(exit_scope);
    if (p != mBlockMap.end()) {
      block = p->second->top();
    }
    exitKeyword = "RETURN";
    // exitLabel = function name?
  }
  else {
    exitKeyword = gGetVhNodeDescription(vh_exit);
  }

  if (block == NULL) {
    UtString msg;
    msg << " while processing " << exitKeyword << " " << exitLabel;
    POPULATE_FAILURE(mMsgContext->UnExpectedObjectType( &loc, gGetVhNodeDescription(vh_exit),
                                                        msg.c_str()), &err_code);
    return err_code;
  }

  NUStmtList then_stmts;
  if (vh_exit_cond) {
    NUExpr *exit_cond = NULL;
    ErrorCode temp_err_code = condExpr(vh_exit_cond, context, &exit_cond);
    if (errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code))
    {
      return temp_err_code;
    }
    *the_exit = context->stmtFactory()->createIf(exit_cond, false, loc);
    
  }

  StringAtom* target = context->getStringCache()->intern(exitLabel);
  StringAtom* keyword = context->getStringCache()->intern(exitKeyword);
  NUStmt* exit_assignment = 
    context->stmtFactory()->createBreak(block, keyword, target, loc);

  if (vh_exit_cond)
  {
    // Create an IF stmt whose condition is exit_cond and which contains
    // the exit_assignment as it's statement.
    NUStmtList then_stmts;
    context->pushStmtList(&then_stmts);
    context->addStmt(exit_assignment);
    context->popStmtList();
    NUStmtList else_stmts; // Empty else
    NUIf* if_stmt = dynamic_cast<NUIf*>(*the_exit);
    if_stmt->replaceThen(then_stmts);
    context->stmtFactory()->createElse(if_stmt);  // This is needed to pop out of else block
    if (!context->stmtFactory()->next(*the_exit)) { // Done populating if-then.
      delete if_stmt;
      return eFatal;
    }
    if_stmt->replaceElse(else_stmts);
    if (!context->stmtFactory()->next(*the_exit)) { // Done populating if-else.
      delete if_stmt;
      return eFatal;
    }
  }
  else
  {
    *the_exit = exit_assignment;
  }
  return err_code;
}


Populate::ErrorCode VhdlPopulate::getIfCondExpr(LFContext *context, vhExpr vh_cond_exp, 
                                                NUExpr **cond_expr_rtn, branchToTake *br_to_take)
{
  branchToTake static_cond = eTakeBoth;
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator((vhNode) vh_cond_exp);

  NUExpr *cond_expr = 0;
  temp_err_code = resetExprHelper( vh_cond_exp, context, &cond_expr );
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;

  temp_err_code = maybeReplaceClockNet(cond_expr, context);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
    return temp_err_code;


  NUExpr* opt_expr = context->stmtFactory()->optimizeExpr(cond_expr);
  if ((opt_expr == NULL) && !sExprHasClock(cond_expr))
  {
    temp_err_code = mustNotBeAttribute(cond_expr, context, &loc);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    opt_expr = mFold->fold(cond_expr);
  }

  if (opt_expr != NULL) 
  {
    cond_expr = opt_expr;
    NUConst* const_expr = opt_expr->castConstNoXZ();
    if (const_expr != NULL)
    { 
      if (const_expr->isZero()) 
        static_cond = eTakeElseOnly;
      else 
        static_cond = eTakeThenOnly;
    }
  }

  *cond_expr_rtn = cond_expr;
  *br_to_take = static_cond;
    
  return err_code;
}

Populate::ErrorCode VhdlPopulate::ifStmtHelper(LFContext *context,
                                               vhExpr vh_cond_exp,  
                                               JaguarList* vh_seq_stmt_list,
                                               NUBlockStmt** the_block,
                                               branchToTake* br_populated)
{
  ErrorCode err_code = eSuccess, temp_err_code = eSuccess;
  SourceLocator loc = locator((vhNode) vh_cond_exp);

  branchToTake static_cond = eTakeBoth;

  if(vh_cond_exp != NULL)
  {
    NUExpr *cond_expr = 0;
    temp_err_code = getIfCondExpr(context, vh_cond_exp, &cond_expr, &static_cond);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
      return temp_err_code;

    if(static_cond == eTakeThenOnly)
    {
      NUIf* upper_if_stmt = dynamic_cast<NUIf*> (*the_block);

      NUStmtList then_stmts;
      if(upper_if_stmt != NULL)
        context->stmtFactory()->createElse(upper_if_stmt);

      NUBlock *block = context->stmtFactory()->createBlock(context->getBlockScope(), false, loc);

      context->pushStmtList( &then_stmts );
      temp_err_code = sequentialStmtList(vh_seq_stmt_list, context );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
        return temp_err_code;

      block->addStmts(&then_stmts);
      context->popStmtList();

      if(upper_if_stmt != NULL)
      {
        upper_if_stmt->addElseStmt(block);
      }

      context->stmtFactory()->next(block);

      *the_block = block;
      delete cond_expr;
    }
    else if(static_cond == eTakeElseOnly)
    {
      delete cond_expr;
    }
    else if(static_cond == eTakeBoth)
    {
      NUIf* upper_if_stmt = dynamic_cast<NUIf*> (*the_block);
      if(upper_if_stmt != NULL)
      {
        context->stmtFactory()->createElse(upper_if_stmt);
      }

      NUIf* the_if = context->stmtFactory()->createIf(cond_expr, false, loc);
      context->pushStmtList(the_if->getThenList());
      temp_err_code = sequentialStmtList(vh_seq_stmt_list, context );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
        return temp_err_code;

      context->popStmtList();

      if(upper_if_stmt != NULL)
      {
        upper_if_stmt->addElseStmt(the_if);
      }

      context->pushStmtList(the_if->getElseList());  // The pop for this is in ifStmt() function
      context->stmtFactory()->next(the_if);
      *the_block = the_if;
    }
  }
  else
  {
    if(*the_block == NULL)
    {
      static_cond = eTakeElseOnly;
      
      NUStmtList else_stmts;
      NUBlock *block = context->stmtFactory()->createBlock(context->getBlockScope(), false, loc);

      if(vh_seq_stmt_list != NULL)
      {
        context->pushStmtList( &else_stmts );
        temp_err_code = sequentialStmtList(vh_seq_stmt_list, context );
        if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
          return temp_err_code;

        block->addStmts(&else_stmts);
        context->popStmtList();
      }

      *the_block = block;
      context->stmtFactory()->next(block);
    }
    else
    {
      // We should assert here if *the_block is not of NUIf type
      NUIf* upper_if_stmt = dynamic_cast<NUIf*> (*the_block);
      NU_ASSERT(upper_if_stmt != NULL, *the_block);


      NUStmtList else_stmts;
      context->stmtFactory()->createElse(upper_if_stmt);

      if(vh_seq_stmt_list != NULL)
      {
        context->pushStmtList( &else_stmts );
        temp_err_code = sequentialStmtList(vh_seq_stmt_list, context );
        if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
          return temp_err_code;

        upper_if_stmt->addElseStmts(&else_stmts);
        context->popStmtList();
      }
    }
  }

  *br_populated = static_cond;
  return eSuccess;
}

Populate::ErrorCode VhdlPopulate::ifStmt(vhNode vh_if,
                                         LFContext *context,
                                         NUBlockStmt** the_if)
{
  ErrorCode err_code = eSuccess, temp_err_code;
  SourceLocator loc = locator(vh_if);
  context->pushJaguarScope( vh_if );
  NUIfstack if_stmt_stack;

  vhExpr if_cond = vhGetCondition(vh_if);

  JaguarList vh_then_list( vhGetThenSeqStmtList( vh_if ));
  JaguarList vh_elsifs( vhGetElsifSeqStmtList( vh_if ));
  JaguarList vh_else_list( vhGetElsSeqStmtList( vh_if ));

  NUBlockStmt* current_block = NULL;
  branchToTake branch_populated = eTakeBoth;
  temp_err_code = ifStmtHelper(context, if_cond, &vh_then_list, 
                               &current_block, &branch_populated);
  if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
    return temp_err_code;


  if(branch_populated == eTakeThenOnly)
  {
    *the_if = current_block;
    context->popJaguarScope();
    return err_code;
  }
  else if(branch_populated == eTakeBoth)
  {
    if(current_block == NULL)
    {
      context->popJaguarScope();
      return err_code;
    }
    else
    {
      NUIf* if_stmt = dynamic_cast<NUIf*> (current_block);
      NU_ASSERT(if_stmt != NULL, current_block);
      if_stmt_stack.push(if_stmt);
    }
  }


  if(vh_elsifs.empty() == false)
  {
    vhNode vh_elsif; 
    while ((vh_elsif = vhGetNextItem(vh_elsifs)) != 0)
    {
      vhExpr if_cond = vhGetCondition(vh_elsif);
      JaguarList vh_elsif_list(vhGetThenSeqStmtList(vh_elsif));

      temp_err_code = ifStmtHelper(context, if_cond, &vh_elsif_list, 
                                   &current_block, &branch_populated);
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
        return temp_err_code;

        
      if(branch_populated == eTakeThenOnly)
      {
        // We don't need to continue rest of the if statement
        if(if_stmt_stack.empty())
        {
          *the_if = current_block;
        }
        else
        {
          // Now unwind the if blocks
          NUIf* last_if_stmt = NULL;
          while( !if_stmt_stack.empty() )
          {
            last_if_stmt = if_stmt_stack.top();
            context->stmtFactory()->next(last_if_stmt);
            context->popStmtList(); // This is for getElseList() in ifStmtHelper function
            if_stmt_stack.pop();
          }
          *the_if = last_if_stmt;
        }
        context->popJaguarScope();
        return err_code;
      }
      else if(branch_populated == eTakeBoth)
      {
        NUIf* if_stmt = dynamic_cast<NUIf*> (current_block);
        NU_ASSERT(if_stmt != NULL, current_block);
        if_stmt_stack.push(if_stmt);
      }
    }
  }

  
  if(vh_else_list.empty() == false)
  {
    temp_err_code = ifStmtHelper(context, NULL, &vh_else_list, 
                                 &current_block, &branch_populated);
  }
  else
  {
    // we create empty else block to correctly manage
    // the pustNesting and popNesting of constant propagation
    temp_err_code = ifStmtHelper(context, NULL, NULL, 
                                 &current_block, &branch_populated);
  }

  if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
    return temp_err_code;


  if(if_stmt_stack.empty())
  {
    *the_if = current_block;
  }
  else
  {
    // Now unwind the if blocks
    NUIf* last_if_stmt = NULL;
    while( !if_stmt_stack.empty() )
    {
      last_if_stmt = if_stmt_stack.top();
      context->stmtFactory()->next(last_if_stmt);
      context->popStmtList(); // This is for getElseList() in ifStmtHelper function
      if_stmt_stack.pop();
    }
    *the_if = last_if_stmt;
  }

  context->popJaguarScope();
  return err_code;
}

void
VhdlPopulate::composeBinaryFromChar(UInt8 char_val, UtString *bin_str)
{
  bin_str->clear();
  UInt32 tempc = char_val;

  char result[16];

  CarbonValRW::writeBinValToStr(result, 16, &tempc, 8, false);

  *bin_str = result;
}

// This method helps in converting the char string to binary representation.
// The binary string representation is used to help store filename char*, 
// mode char* etc using NUConst() for nucleus SysTask classes.
// For ex. the character 'A' is converted to a binary string "01000001"
//         ie. 65 in binary. Since ASCII 'A' has a decimal value 65.
// NUConst() has a flag to remember that the binary string it contains was 
// actually a char string. It helps in decomposing the string when required.
// replace_null_with_zero: if true then an empty input string will
//         result in a bin_str that contains the representation of \0,
//         which will look like a null string at runtime. (default is false)
int 
VhdlPopulate::composeBinStringFromString(const char* str, UtString* bin_str, bool replace_null_with_zero)
{
  UInt32 len =  strlen(str);
  UtString  temp;
  for (int i = 0; i <= (int)len - 1; i++) {
    composeBinaryFromChar((int)str[i], &temp);
    bin_str->append(temp.c_str());
  }
  if ( replace_null_with_zero  && ( bin_str->length() == 0 ) ) {
    // An empty input string would result in a bin_str of size zero.
    // If that string is used by  emitUtil::emitConstant, then we
    // will INFO_ASSERT (see bug7535). Here we force a zero onto the string
    // which gives us a usable constant that acts as an empty
    // string at runtime.
    bin_str->append("00000000");
  }
  return bin_str->length();
}

// Since VHDL file-io subprograms implicitly define the format in which the
// values are to be written into file. We create a explicit format string 
// similar to ones in Verilog for the use of NU[Input|Output]SysTask. 
// For This we are using Verilog's string bit-pattern. This lets us re-use
// HdlVerilogString class for run-time support of filename, mode-type, and 
// format strings.
bool
VhdlPopulate::createFormatStringExpr(vhNode vh_node,
                                     const SourceLocator& loc,
                                     NUExpr **the_expr)
{
  if (vhGetObjType(vh_node) == VHOBJECT)
      vh_node = vhGetActualObj(vh_node);
  UtString bin_str;
  UInt32   bit_width;
  switch (vhGetObjType(vh_node))
  {
  case VHSIGNAL:
  case VHVARIABLE:
  case VHCONSTANT:
  {
    vhNode subtype = vhGetSubTypeInd(vh_node);
    vhNode type = vhGetType(subtype);
    vhNode basetype = vhGetAbsoluteBaseType(type);
    if (basetype)
    {
      JaguarString type_mark(vhGetName(basetype));
      if ( strcasecmp("INTEGER", type_mark) == 0)
        bit_width = composeBinStringFromString("%0d", &bin_str);
      else if (( strcasecmp("BIT_VECTOR", type_mark) == 0) ||
               ( strcasecmp("STD_ULOGIC_VECTOR", type_mark) == 0) ||
               ( strcasecmp("STD_LOGIC_VECTOR", type_mark) == 0))
        bit_width = composeBinStringFromString("%b", &bin_str);
      else if (( strcasecmp("BIT", type_mark) == 0) ||
               ( strcasecmp("STD_ULOGIC", type_mark) == 0) ||
               ( strcasecmp("STD_LOGIC", type_mark) == 0))
        bit_width = composeBinStringFromString("%b", &bin_str);
      else if ( strcasecmp("BOOLEAN", type_mark) == 0)
        bit_width = composeBinStringFromString("%b", &bin_str);
      else if ( strcasecmp("STRING", type_mark) == 0)
        bit_width = composeBinStringFromString("%s", &bin_str);
      else if ( strcasecmp("CHARACTER", type_mark) == 0)
        bit_width = composeBinStringFromString("%c", &bin_str);
      else
        return false;
    }
    else 
    {
      return false;
    }
    break;
  }
  case VHSTRING:
    bit_width = composeBinStringFromString("%s", &bin_str);
    break;
  case VHDECLIT:
    bit_width = composeBinStringFromString("%0d", &bin_str);
    break;
  case VHCHARLIT:
    bit_width = composeBinStringFromString("%c", &bin_str);
    break;
  case VHINDNAME:
  case VHSLICENAME:
  {
    // Get the base type.
    vhNode vh_prefix = vhGetPrefix(static_cast<vhExpr>(vh_node));
    return createFormatStringExpr(vh_prefix, loc, the_expr);
  }
  default:
    return false;
  }
  *the_expr = NUConst::createKnownConst(bin_str, 
                                          bit_width, true, loc);
  (*the_expr)->resize ((*the_expr)->determineBitSize ());
  return true;
}

// Check If the Procedure call is for a File-IO Procedure like FILE_OPEN,
// READ, WRITE, READLINE, WRITELINE or FILE_CLOSE.
// If So, Populate the appropriate NU*SysTask object.
// Returns eNotApplicable if vh_proc_call is not a File-IO procedure call.    
Populate::ErrorCode VhdlPopulate::fileIOProcedureCall(vhNode vh_proc_call,
                                                      LFContext *context)
{
  bool vhdlFileIOEnabled = mArg->getBoolValue(CarbonContext::scVhdlEnableFileIO);
  SourceLocator loc = locator(vh_proc_call);
  JaguarString procName(vhGetStringVal(vhGetProcedureName(vh_proc_call)));
  if (!( ( 0 == strncasecmp( procName, "FILE_OPEN", 10)) or
         ( 0 == strncasecmp( procName, "READ", 5)) or
         ( 0 == strncasecmp( procName, "WRITE", 6)) or
         ( 0 == strncasecmp( procName, "FILE_CLOSE", 11)) or
         ( 0 == strncasecmp( procName, "READLINE", 9)) or
         ( 0 == strncasecmp( procName, "HREAD", 6)) or
         ( 0 == strncasecmp( procName, "OREAD", 6)) or
         ( 0 == strncasecmp( procName, "HWRITE", 7)) or
         ( 0 == strncasecmp( procName, "OWRITE", 7)) or
         ( 0 == strncasecmp( procName, "WRITELINE", 10))))
  {
    return eNotApplicable; // Not a File-IO procedure.
  }

  // Make Sure that this is not user over-loaded procedure.
  vhNode master = vhGetMaster(vh_proc_call);
  if (master)
  {
    vhNode scope = vhGetScope(master);
    if (scope)
    {
      vhString libname = vhGetLibName(scope);
      // If the procedure is not defined in STD/IEEE then it must be
      // user defined.
      if ( strncasecmp("STD",libname,4) != 0 &&
           strncasecmp("IEEE",libname,5) != 0 )
        return eNotApplicable;  // Not a File-IO procedure.
    }
  } // else implicitly defined procedures.

  if ( not vhdlFileIOEnabled )
  {
    mMsgContext->DisabledSysTask(&loc, procName, CarbonContext::scVhdlEnableFileIO);
    return eSuccess; // It is a file-IO stmt but has been ignored.  
  }
  // else Process FILE IO procedural calls.

  ErrorCode     err_code = eSuccess;
  NUModule     *module = context->getModule();
  vhNode        element;
  JaguarList        associationList(vhGetAssocList(vh_proc_call));
  NUNet        *net = 0;
  vhNode        vh_status_node = NULL;
  bool          standardIOTerminal = false;
  ErrorCode     returnErrCode = eSuccess;

  while ((element = vhGetNextItem(associationList)) != 0)
  {
    // Get the File-object. Its either the first argument or sometimes the
    // second argument (as in case of FILE_OPEN).
    // The File object is represented by a 32-bit vectorNet.
    // The StatusNet (which is sometimes the first argument of FILE_OPEN) is 
    //     always represented by a 2-bit VectorNet.
    //     ex.
    //     FILE_OPEN( file_object, filename, READ_MODE);
    //     or
    //     FILE_OPEN(status, file_object, filename, READ_MODE);
 
    if ((vhGetObjType(element) == VHOBJECT) or 
        ( vhGetObjType(element) == VHSELECTEDNAME)) {
      element = vhGetActualObj(element);
      if (vhGetObjType(element) == VHFILEDECL ) {
        vhNode scope = vhGetScope( element );
        if ( scope != NULL )
        {
          JaguarString libname(vhGetLibName( scope ));
          if ( strncasecmp( "STD", libname, 4 ) == 0 ) {
            // This is a STD library defined file object 
            // STD.TEXTIO.OUTPUT or STD.TEXTIO.INPUT which represent
            // stdout or stdin.
            // For STD.TEXTIO.OUTPUT we will populate the constant '1'.
            // For STD.TEXTIO.INPUT  we will populate the constant '0'
            // '1' and '0' are reserved descriptors in HDLFileSystem for stdout
            // stdin respectively
            standardIOTerminal = true;
            break; // File object found.
          }
        }
      }

      ErrorCode temp_err_code = lookupNet( context->getDeclarationScope(), 
                                           element, context, &net);
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
         return temp_err_code;

      if (net->getBitSize() == 2)
      {
        // It is FILE_OPEN_STATUS variable.
        vh_status_node = element;
        net = 0;
      }
      else
        break; // File object found.
    } 
  }

  if (net == 0 && !standardIOTerminal) 
  {
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc,"File object or LINE variable not found in the argument list of this fileIO procedure."), &err_code);
    return err_code; // File object/descriptor not found
  }
  NUExprVector  expr_vector;
  NUExpr       *this_expr = 0;
  StringAtom   *sym = module->gensym(procName);

  if (0 == strncasecmp ( procName, "FILE_OPEN", 10))
  {
    NULvalue* statusLvalue = 0;
    // If status node is present, get its lvalue.
    if (vh_status_node)
    {
      if (vhGetObjType(vh_status_node) == VHOBJECT)
        vh_status_node = vhGetActualObj(vh_status_node);
      ErrorCode temp_err_code = identLvalue(vh_status_node, context, 
                                            &statusLvalue, false);
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

    }

    // Argument after the File-object is the fileName
    element = vhGetNextItem(associationList);
    if (vhGetObjType(element) == VHOBJECT)
      element = vhGetActualObj(element);

    // element is VHSTRING now which represents fileName.
    int value = 0;
    UtString str_val;
    ErrorCode temp_err_code = getVhExprValue(static_cast<vhExpr>(element),
                                             &value, &str_val);
    if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code )) {
      mMsgContext->UnsupportedNonConstantFilename(&loc);
      return temp_err_code;
    }

    UtString binary_str;
    UInt32 bit_width = composeBinStringFromString(str_val.c_str(),&binary_str, true);
    this_expr = NUConst::createKnownConst(binary_str, 
                                          bit_width, true, loc);
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);

    // Now get the file open mode. READ, WRITE or APPEND.
    element = vhGetNextItem(associationList);
    if (vhGetObjType(element) == VHOBJECT)
      element = vhGetActualObj(element);

    UtString mode_str;
    UInt32 mode = vhGetIdEnumLitValue(element);
    if (mode == 0) {
      // read mode
      bit_width = composeBinStringFromString("r", &mode_str);
    }
    else if (mode == 1)
    {
      // write mode
      bit_width = composeBinStringFromString("w", &mode_str);
    }
    else if (mode == 2)
    {
      // append mode 
      bit_width = composeBinStringFromString("a", &mode_str);
    }

    this_expr = NUConst::createKnownConst(mode_str, 
                                          bit_width, true, loc);
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);

    if ( statusLvalue )
      NU_ASSERT( statusLvalue->getType() == eNUIdentLvalue, statusLvalue );
    NUFOpenSysTask* sysFOpen;
    sysFOpen = context->stmtFactory()->createFOpenSysTask(sym, net, 
                                                          static_cast<NUIdentLvalue*>( statusLvalue ), 
                                                          expr_vector, module, false,
                                                          false /*isVerilogTask*/,
                                                          (mode == 0)/*useInFileSystem*/, loc);
    UtString errmsg;
    if (sysFOpen->isValid(&errmsg))
    {
      context->addStmt(sysFOpen);
    }

    returnErrCode = eSuccess;
  }  
  else if (0 == strncasecmp ( procName, "FILE_CLOSE", 11))
  {
    // We have the net corresponding to FILE-object/desciptor
    this_expr = createIdentRvalue(net, loc);
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);
    NUFCloseSysTask *fclose = 
      context->stmtFactory()->createFCloseSysTask(sym, expr_vector, module,
                                                  false,
                                                  false /*isVerilogTask*/,
                                                  false/*useInFileTask*/,
                                                  loc);
    context->addStmt(fclose);
    returnErrCode = eSuccess;
  }
  else if ( (0 == strncasecmp ( procName, "READ", 5)) or 
            (0 == strncasecmp ( procName, "HREAD", 6)) or
            (0 == strncasecmp ( procName, "OREAD", 6)))
  {
    if (standardIOTerminal) {
      POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc,"STD.TEXTIO.INPUT as file object not yet supported"), &err_code);
      return err_code;
    }

    // We have the net corresponding to FILE-object/desciptor
    this_expr = createIdentRvalue(net, loc);
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);   

    // Get the expr on which the value will be written to. 
    element = vhGetNextItem(associationList);

    // Create and insert the format string Expr.
    if (0 == strncasecmp ( procName, "READ", 5))
    {
      if (createFormatStringExpr(element, loc, &this_expr)) {
        expr_vector.push_back (this_expr);
      } else {
        POPULATE_FAILURE(mMsgContext->UnExpectedObjectType( &loc, gGetVhNodeDescription(element),
                                                            " while processing 2nd argument of read statement."), &err_code);
        return err_code;
      }
    }
    else
    {
      // For OREAD set the format specifier to "%o"
      // For HREAD set the format specifier to "%h"
      UtString bin_str;
      UInt32   bit_width = 0;
      if (0 == strncasecmp ( procName, "HREAD", 6))
        bit_width = composeBinStringFromString("%h", &bin_str);
      else if (0 == strncasecmp ( procName, "OREAD", 6))
        bit_width = composeBinStringFromString("%o", &bin_str);
    
      LOC_ASSERT(bit_width, loc);
      this_expr = NUConst::createKnownConst(bin_str, 
                                            bit_width, true, loc);
      this_expr->resize (this_expr->determineBitSize ());
      expr_vector.push_back (this_expr);
    }

    // Need an lvalue for element into which value will be read.
    NULvalue* the_lvalue = 0;
    ErrorCode temp_err_code = lvalue(element, context, &the_lvalue, false);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
       return temp_err_code;
    
    // Get the status argument, if present.
    element = vhGetNextItem(associationList);
    NULvalue* the_statusLvalue = 0;
    if (element)
    {
      ErrorCode temp_err_code = lvalue(element, context, &the_statusLvalue, false);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
         return temp_err_code;
    }

    NUInputSysTask *the_stmt =
      context->stmtFactory()->createInputSysTask(sym, expr_vector, the_lvalue, 
                                                 the_statusLvalue,  module,
                                                 false, false /*isVerilogTask*/,
                                                 false /*isReadLineTask*/, loc);
    context->addStmt(the_stmt);    
    returnErrCode = eSuccess;
  }
  else if ((0 == strncasecmp ( procName, "WRITE", 6)) or
           (0 == strncasecmp ( procName, "HWRITE", 7)) or
           (0 == strncasecmp ( procName, "OWRITE", 7)))                               
  {
    if (standardIOTerminal) {
      // Populate with constant value '1'. '1' is the reserved file descriptor
      // in HdlFileSystem and represents stdout.
      this_expr = (NUExpr*) NUConst::create( false, 1, 32, loc);
    }
    else
    {
      // We have the net corresponding to FILE-object/descriptor
      this_expr = createIdentRvalue(net, loc);
    }
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);

    // Get the value to be written
    element = vhGetNextItem(associationList);
    this_expr = 0;

    // Create and insert the format string Expr.
    if (0 == strncasecmp ( procName, "WRITE", 6))
    {
      if (createFormatStringExpr(element, loc, &this_expr)) {
        expr_vector.push_back (this_expr);
      } else {
        POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "WRITE procedure call is not supported for the given type_mark"), &err_code);
        return err_code;
      }
    }
    else
    {
      // For OWRITE set the format specifier to "%o"
      // For HWRITE set the format specifier to "%h"
      UtString bin_str;
      UInt32   bit_width = 0;
      if (0 == strncasecmp ( procName, "HWRITE", 7))
        bit_width = composeBinStringFromString("%h", &bin_str);
      else if (0 == strncasecmp ( procName, "OWRITE", 7))
        bit_width = composeBinStringFromString("%o", &bin_str);
    
      LOC_ASSERT(bit_width, loc);
      this_expr = NUConst::createKnownConst(bin_str, 
                                            bit_width, true, loc);
      this_expr->resize (this_expr->determineBitSize ());
      expr_vector.push_back (this_expr);
    }

    this_expr = 0;
    ErrorCode temp_err_code = expr(static_cast<vhExpr>(element), context,
                                   32, &this_expr);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ))
       return temp_err_code;

    if (this_expr) {
      this_expr->resize (this_expr->determineBitSize ());
      expr_vector.push_back (this_expr);
    }

    NUOutputSysTask *the_stmt =
      context->stmtFactory()->createOutputSysTask(sym, expr_vector, module,
                                                  true/*hasFileArg*/, false/*addNewline*/,
                                                  module->getTimeUnit(),
                                                  module->getTimePrecision(),
                                                  false, false /*isVerilogTask*/,
                                                  false /* isWriteLineTask */, false, NULL, loc);
    context->addStmt(the_stmt);

    returnErrCode = eSuccess;
  }
  else if (0 == strncasecmp ( procName, "READLINE", 9))
  {
    if (standardIOTerminal) {
      // Populate with constant value '0'. '0' is the reserved file descriptor
      // in HdlFileSystem and represents stdin.
      this_expr = (NUExpr*) NUConst::create (false, 0, 32, loc);
    }
    else
    {
      // We have the net corresponding to FILE-object/descriptor
      this_expr = createIdentRvalue(net, loc);
    }

    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);   

    // Get the line expr on which the value will be written to. 
    NUNet* line_net = 0;
    element = vhGetNextItem(associationList);
    if ((vhGetObjType(element) == VHOBJECT) &&
        (element = vhGetActualObj(element)))
    {
      ErrorCode temp_err_code = lookupNet( context->getDeclarationScope(), element,
                                           context, &line_net );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

      // Since line_net is not directly modified by readline, 
      // populate it as a rvalue.
      this_expr = createIdentRvalue(line_net, loc);
      this_expr->resize (this_expr->determineBitSize ());
      expr_vector.push_back (this_expr);
    }
    else
    {
      POPULATE_FAILURE(mMsgContext->UnExpectedObjectType( &loc, gGetVhNodeDescription(element),
                                                          " while processing 2nd argument of readline statement."), &err_code);
      return err_code;
    }
    NULvalue* lvalue = NULL; 

    NUInputSysTask *the_stmt =
      context->stmtFactory()->createInputSysTask(sym, expr_vector, lvalue, NULL/*status*/, module,
                                                 false, false /*isVerilogTask*/,
                                                 true /*isReadLineTask */, loc);
    context->addStmt(the_stmt);    
    returnErrCode = eSuccess;
  }
  else if (0 == strncasecmp ( procName, "WRITELINE", 10))
  {
     
    if (standardIOTerminal) {
      // Populate with constant value '1'. '1' is the reserved file descriptor
      // in HdlFileSystem and represents stdout.
      this_expr = (NUExpr*) NUConst::create(false, 1, 32, loc);
    }
    else
    {
      // We have the net corresponding to FILE-object/descriptor
      this_expr = createIdentRvalue(net, loc);
    }
    this_expr->resize (this_expr->determineBitSize ());
    expr_vector.push_back (this_expr);

    // Get the line variable to be written
    NUNet* line_net = 0;
    element = vhGetNextItem(associationList);
    if ((vhGetObjType(element) == VHOBJECT) &&
        (element = vhGetActualObj(element)))
    {
      ErrorCode temp_err_code = lookupNet( context->getDeclarationScope(), element,
                                           context, &line_net );
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
        return temp_err_code;

      this_expr = createIdentRvalue(line_net, loc);
      this_expr->resize (this_expr->determineBitSize ());
      expr_vector.push_back (this_expr);
    } 
    else
    {
      POPULATE_FAILURE(mMsgContext->UnExpectedObjectType( &loc, gGetVhNodeDescription(element),
                                                          " while processing 2nd argument of writeline statement."), &err_code);
      return err_code;
    }

    NUOutputSysTask *the_stmt =
      context->stmtFactory()->createOutputSysTask(sym, expr_vector, module,
                                                  true/*hasFileArg*/, false/*addNewline*/,
                                                  module->getTimeUnit(),
                                                  module->getTimePrecision(),
                                                  false, false /*isVerilogTask*/,
                                                  true /*isWriteLineTask */, false, NULL, loc);
    context->addStmt(the_stmt);

    returnErrCode = eSuccess;
  }
  else
  {
    // Currently I am not aware of any other file-IO stmt that could trigger
    // The following error.
    POPULATE_FAILURE(mMsgContext->JaguarConsistency(&loc, "Unsupported file IO statement "), &returnErrCode);
  }
  return returnErrCode;
}

bool VhdlPopulate::getFuncProcActualArgList(vhNode func_or_proc_call,
                                            JaguarList* actual_arg_list,
                                            UtList<vhNode>* output_actual_list)
{
  bool success = false;
  // Create a new list to hold actuals.
  *actual_arg_list = vhwCreateNewList(VH_TRUE /*Can be deleted*/);
  // Iterator over actuals for function/procedure call.
  JaguarList actual_iter(vhGetAssocList(func_or_proc_call));
  // The function/procedure call master provides a map between
  // function/procedure interface and initials.
  vhNode func_or_proc_master = vhGetMaster(func_or_proc_call);
  if (func_or_proc_master != NULL)
  {
    success = true;
    JaguarList formal_iter(vhGetInterfaceList(func_or_proc_master));
    const SourceLocator loc = locator(func_or_proc_call);
    vhNode vh_actual = NULL;

    while ((vh_actual = vhGetNextItem(actual_iter)) != NULL)
    {
      vhNode vh_formal = vhGetNextItem(formal_iter);
      vhNode vh_mapped_actual = vh_actual;
      if (vhGetObjType(vh_actual) == VHOPEN) // Argument actual not provided.
      {
        // Use the initial value of formal, which is the default value.
        vhExpr initial_value = vhGetInitialValue(vh_formal);
        // A missing initial value means we have a missing argument to function
        // call, and the argument has no default value. Jaguar will error out
        // before we get here, unless there's a bug.
        LOC_ASSERT(initial_value != NULL, loc);
        vh_mapped_actual = initial_value;
      }
      vhwAppendToList(*actual_arg_list, vh_mapped_actual);
      vhPortType port_type = vhGetPortType(vh_formal);
      if ( (output_actual_list != NULL) && 
           ((port_type == VH_OUT) || (port_type == VH_INOUT)) ) {
        output_actual_list->push_back(vh_mapped_actual);
      }
    }
  }
  return success;
}


Populate::ErrorCode
VhdlPopulate::seqProcedureCall(vhNode vh_proc_call, LFContext *context)
{
  // first see if this is a file IO procedure
  ErrorCode err_code = fileIOProcedureCall(vh_proc_call, context);
  if (err_code == eNotApplicable) {
    err_code = eSuccess;        // not a recognized file-io procedure, try again (below)
  } else {
    return err_code;            // Processed file IO Procedure call.
  }

  SourceLocator loc = locator(vh_proc_call);
  StringAtom* internName = createSubprogSignature(context, vh_proc_call);

  // Could not determine the name of the subprogram
  if (internName == NULL) {
    UtString subpName;
    getVhdlObjName( vhGetProcedureName( vh_proc_call ), &subpName );
    mMsgContext->CouldNotDetermineTaskName( &loc, subpName.c_str());
    return eFatal;
  }

  vhNode elaborated_subp = vhOpenSubProgram( vh_proc_call );
  if ( elaborated_subp == NULL )
  {
    UtString subpName;
    getVhdlObjName( vhGetProcedureName( vh_proc_call ), &subpName );
    POPULATE_FAILURE(mMsgContext->VhdlMissingSubprogramBody( &loc, subpName.c_str( )), &err_code);
    // No need to call vhCloseFeature()--it didn't get opened
    return err_code;
  }
  vhNode vh_proc = vhGetSubProgBody( elaborated_subp );
  vhOpenDeclarativeRegion( vh_proc );

  NUTask *task = 0;
  ErrorCode temp_err_code = lookupTask(context->getModule(), internName, &task);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
    return temp_err_code;

  StringAtom* gensymName = NULL;
  if (!task)
  {
    // Detect recursion and disallow recursive subprogram call.
    // TBD :recursion is allowed when the recursion depth is static bound.
    //     We may ask jaguar for this support under thier '-synth' mode.
    //     For now, we will disallow recursion completely.
    if (vhIsSubProgRecursive(vh_proc))
    {
      const char* subpname = vhGetSubProgName(vh_proc);
      POPULATE_FAILURE(mMsgContext->TFRecursion(&loc, subpname), &err_code);
      return err_code;
    }
    gensymName = internName;
    temp_err_code = procedure(vh_proc, gensymName, context, &task);
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
      return temp_err_code;

    temp_err_code = mapTask(context->getModule(), internName, task);
    if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
      return temp_err_code;
  }
  else
  {
    gensymName = task->getName();
  }

  // Process the arguments connected with this procedure call.  This
  // must precede the procedure call itself so that the arg exprs are
  // properly evaluated.
  NUTFArgConnectionVector arg_conns;
  JaguarList vh_arg_iter;
  getFuncProcActualArgList(vh_proc_call, &vh_arg_iter, NULL);
  temp_err_code = procArgConnections( vh_arg_iter, task, 0, context, &arg_conns );
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code,&err_code))
    return temp_err_code;

  // Make the task_enable statement for this function, set the argument
  // connections, and put it into the stmt list
  NUTaskEnable *task_enable =
    context->stmtFactory()->createTaskEnable( task, arg_conns, context->getModule(),
                                              false, loc );
  context->addStmt(task_enable);

  vhCloseFeature(); // close the declarative region
  vhCloseFeature(); // close the subprogram body
  return err_code;
}

Populate::ErrorCode
VhdlPopulate::procArgConnections( JaguarList &vh_arg_iter, NUTask *task,
                                  UInt32 num_func_outputs, LFContext *context,
                                  NUTFArgConnectionVector *arg_conns)
{
  ErrorCode err_code = eSuccess;

  NUNetVector formalArray;
  UtArray<vhNode> actualArray;
  // Populate the array of formal nets to associate to.  We begin at
  // num_func_outputs, because the args 0 to (num_func_outputs-1) are
  // function return values.  num_func_outputs will always be 0 for a
  // procedure, 1 for functions returning scalars or vectors, and >=1
  // for functions returning records or multidimensional objects.
  for ( int idx = num_func_outputs; idx < task->getNumArgs(); ++idx )
    formalArray.push_back( task->getArg( idx ));

  // Populate the array of actual expressions that associate with the
  // formals.
  while (vhNode vh_arg = vhGetNextItem(vh_arg_iter))
  {
    if ( vhGetObjType( vh_arg ) == VHOBJECT )
      vh_arg = vhGetActualObj( vh_arg );

    // Expand the r-values into record elements if it's a record
    // signal or a record aggregate
    if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_arg, context ))
      expandRecordPortActuals( vh_arg, actualArray, context );
    else
      actualArray.push_back( vh_arg );
  }

  UInt32 actualSize = actualArray.size();
  UInt32 formalSize = formalArray.size();

  // we should check here:
  // NU_ASSERT((formalArray.size() == actualArray.size()), task);
  // but the test for bug 5337 trips over it

  // Now take the formal and actual arrays and create
  // NUTFArgConnections for the associated elements.
  for( UInt32 assoc = 0; ( (assoc < formalSize ) && ( assoc < actualSize ) ); ++assoc )
  {
    NUNet *formal_net = formalArray[assoc];
    vhExpr actual_expr = static_cast<vhExpr>( actualArray[assoc] );
    NUTFArgConnection *arg_conn = NULL;
    ErrorCode temp_err_code = subProgArgConnection( actual_expr, formal_net,
                                                    task, context, &arg_conn );
    if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      return temp_err_code;

    if ( arg_conn )
      arg_conns->push_back( arg_conn );
  }

  // Deal with the initial value of any out or inout variable ports
  for( UInt32 assoc = 0; ( (assoc < formalSize ) && ( assoc < actualSize ) ); ++assoc )
  {
    NUNet *formal = formalArray[assoc];
    if ( !formal || ( formal->isOutput() || formal->isBid()))
    {
      unsetVariableInitialValue( actualArray[assoc] );
    }
  }
  return err_code;
}


// Create NUNet corresponding to alias object and then assign
// the value of the actual object into this net/var. This is required
// inorder to make the alias object visible. For each alias object declaration,
// jaguar creates a signal/variable in that scope and puts the actual object
// as the initial value of that alias. Keeping this in mind, for each object
// alias declaration, we will first find out the corresponding signal/var
// created by the jaguar. The we will create a net/var with that signal/variable
// and will create an expression with the initial value of the signal/variable.
// Finally, assign the created NU expression to the NU net created with the
// alias signal/variable. 
// Example:
// alias IN1_AL1 : std_logic_vector(7 downto 0) is IN1(7 downto 0);
// For this IN1_AL1, jaguar will internally create a signal/var depending
// on the object class of IN1 as below:
// signal IN1_AL1 : std_logic_vector(7 downto 0) := IN1(7 downto 0);
// (In the above, assumed the class of IN1 is signal).
// We will process the above as:
// IN1_AL1 <= IN1(7 downto 0);
//  
Populate::ErrorCode
VhdlPopulate::createAndAssignSignalForAliasObject( vhNode vh_alias,
                                                   LFContext *context)
{ 
  ErrorCode err_code = eSuccess, temp_err_code;
  // Create a declaration population indicator. When populating
  // declarations evaluate expressions even when they're not static. In
  // declaration scope, there's no chance of variable/signal values being modified.
  // So their initial values can be used by declaration sizing/initializing.
  AggressiveEvalEnabler aee(this);

  SourceLocator loc = locator(vh_alias);
  if ( VHALIASDECL != vhGetObjType(vh_alias))
  {
    POPULATE_FAILURE(mMsgContext->UnExpectedObjectType( &loc, gGetVhNodeDescription(vh_alias) ,
                                                        "while processing alias declaration." ), &err_code);
    return err_code;
  }
  // Look up the object (signal/variable/constant) created by Jaguar
  // for this alias decl in the Jaguar symbol table.
  vhNode vh_scope = vhGetScope(vh_alias);
  JaguarString alias_name(vhGetName(vh_alias));
  vhNode vh_aliasobj = vhLookUpInSymTab(vh_scope, alias_name);
  if ( NULL == vh_aliasobj )
  {
    POPULATE_FAILURE(mMsgContext->AliasObjectNotFound( &loc, alias_name ), &err_code);
    return err_code;
  }

  NUNet* the_net = 0;
  switch (vhGetObjType(vh_aliasobj))
  {
  case VHSIGNAL:
    temp_err_code = net(vh_aliasobj, context, &the_net);
    break;
  case VHVARIABLE:
  case VHCONSTANT:
    temp_err_code = var(vh_aliasobj, context, &the_net, NULL, false);
    break;
  default:
    // No need to process non-object aliases since the purpose of the
    // alias declaration processing is to give the visibility of the
    // alias objects.
    return eSuccess; 
    break;
  }
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ))
  {
    return temp_err_code;
  }
  if (the_net)
  {
    context->getDeclarationScope()->addLocal(the_net);
  }

  // So far we have created net and added it to the scope.  Now create a
  // lvalue for this alias net and assign the actual of this alias to
  // this lvalue.
  vhExpr vh_actual = vhGetInitialValue(vh_aliasobj);
  if (NULL == vh_actual)
  {
    POPULATE_FAILURE(mMsgContext->ActualOfAliasNotFound( &loc, alias_name), &err_code);
    return err_code;
  }

  // Handle record aliases ...
  if ( !mCarbonContext->isNewRecord() && isRecordNet( vh_aliasobj, context ))
  {
    if (vhGetObjType(vh_scope)  == VHPROCESS )
      return recordAssign( eSequentialNonBlocking, static_cast<vhExpr>(vh_aliasobj), 
                           vh_actual, context, loc );
    else
      return recordAssign( eConcurrent, static_cast<vhExpr>(vh_aliasobj), 
                           vh_actual, context, loc );
  }

//   NULvalue* the_lvalue = NULL;

//   NUModule* module = context->getModule();
//   temp_err_code = lvalue(vh_aliasobj, context, &the_lvalue, true);
//   if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
//   {
//     return temp_err_code;
//   } 
//   const int lWidth = the_lvalue->getBitSize(); 

  // Create Rvalue with the actual corresponding to this alias decl
  NUExpr* rvalue = 0;
  temp_err_code = expr(vh_actual, context, 0, &rvalue);
  if (errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code))
  {
    return temp_err_code;
  }

//   vhObjType scope_type = vhGetObjType( vh_scope );
//   NUAssign* alias_assign = NULL;
//   if ( scope_type == VHPROCESS || scope_type == VHSUBPROGBODY ) 
//   {
//     // Create blocking assignment so that the value of the RHS immediately
//     // gets assigned into the LHS( i.e. alias). This is required in order to
//     // immediately propagate the updated value of the actual onto the alias.
//     alias_assign = context->stmtFactory()->createBlockingAssign( the_lvalue, rvalue, 
//                                                                  false, loc);
//     context->addStmt((NUStmt*)alias_assign);
//   }
//   else
//   {
//     StringAtom *block_name = module->newBlockName(context->getStringCache(),
//                                                                           loc);
//     NUContAssign *the_assign = 
//       context->stmtFactory()->createContAssign(block_name, the_lvalue,
//                                                rvalue, loc);
//     module->addContAssign(the_assign);
//     alias_assign = the_assign;
//   }

  if (the_net) {
    mapVhdlAlias(context->getModule(), the_net, rvalue);
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::sequentialStmtList( JaguarList *vh_seq_stmts, LFContext *context, bool is_wait_clock )
{
  ErrorCode err_code = eSuccess, temp_err_code;
  if ( *vh_seq_stmts != NULL )
  {
    JaguarList vh_new_list, *vh_list_to_use = NULL;
    vh_new_list = vhwCreateNewList(VH_TRUE);
    if(is_wait_clock)
    {
      rearangeWaitClock(vh_seq_stmts, &vh_new_list);
      (void)vh_new_list.rewind();
      vh_list_to_use = &vh_new_list;
    }
    else
    {
      vh_list_to_use = vh_seq_stmts;
    }
      
    
    vhNode vh_stmt;
    while (( vh_stmt = vhGetNextItem( *vh_list_to_use )))
    {
      temp_err_code = sequentialStmt( vh_stmt, context );
      if ( errorCodeSaysReturnNowOnlyWithFatal( temp_err_code, &err_code ))
      {
        return temp_err_code;
      }
    }
  }
  return err_code;
}


Populate::ErrorCode
VhdlPopulate::mustNotBeAttribute(NUExpr *cond_expr, LFContext *context, SourceLocator *loc)
{
  NUAttribute *attr = dynamic_cast <NUAttribute *> (cond_expr);
  if(attr != NULL)
  {
    UtString msg;
    msg << "This type of clocking and/or reset structure is not supported.";
    context->getMsgContext()->UnsupportedSeqProcess(loc, msg.c_str());
    processCleanup( context );
    return eFatal;
  }
  return eSuccess;
}
