// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/Elaborate.h"
#include "LFContext.h"
#include "symtab/STSymbolTable.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "compiler_driver/CarbonDBWrite.h"

/*
 * Elaborate the given block in the given context; create NUNetElab object.
 */
void Elaborate::elabBlock(NUBlock *block, LFContext *context, ElabPass pass)
{
  // Elaborate locals during first pass.
  if (pass == eElabLocals) {
    for (NUNetLoop iter = block->loopLocals();
         not iter.atEnd();
         ++iter) {
      elabNet(*iter, context);
    }
  }

  for (NUBlockLoop iter = block->loopBlocks();
       not iter.atEnd();
       ++iter) {
    elabBlock(*iter, context, pass);
  }
}


/*
 * Elaborate the given named scope in the given context; create NUNetElab objects.
 */
void Elaborate::elabDeclarationScope(NUNamedDeclarationScope *scope, LFContext *context, ElabPass pass)
{
  STSymbolTable* symtab = context->getSymbolTable();
  STBranchNode* branch = 0;

  // First pass (locals), create the elaborated named scope object.
  // Second pass (hierrefs) and third pass (reconstruction), just look it up.
  switch (pass) {
  case eElabLocals: {
    // Check if the elaborated named scope already exists. This can occur when we re-elaborate
    STSymbolTableNode *node = symtab->find(context->getHier(), scope->getName());
    if (not node) {
      NUNamedDeclarationScopeElab *elab = scope->createElab(context->getHier(), symtab);
      branch = elab->getHier();
    } else {
      branch = node->castBranch();
    }
    break;
  }
  case eElabHierRefs: 
  case eElabReconData: {
    STSymbolTableNode *node = symtab->find(context->getHier(), scope->getName());
    branch = node->castBranch();
    break;
  }
  }

  NU_ASSERT(branch,scope);
  context->pushHier(branch);

  // Elaborate locals during first pass.
  if (pass == eElabLocals) {
    for (NUNetLoop iter = scope->loopLocals();
         not iter.atEnd();
         ++iter) {
      elabNet(*iter, context);
    }
  }

  // NOTE: once hierrefs are put into Verilog named blocks, will need to call elabNetHierref
  // for each hierref here.

  for (NUNamedDeclarationScopeLoop iter = scope->loopDeclarationScopes();
       not iter.atEnd();
       ++iter) {
    elabDeclarationScope(*iter, context, pass);
  }

  for (NUModuleInstanceMultiLoop iter = scope->loopDeclScopeInstances();
       !iter.atEnd(); ++iter) {
    elabModuleInstanceDecl(*iter, context, pass);
  }

  context->popHier();
}
