// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Declaration of latch cycle reduction process.
 */

#include "RemoveLatchCycles.h"

#include "localflow/UD.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"

RemoveLatchCycles::RemoveLatchCycles(NUNetRefFactory * netref_factory,
                                     MsgContext * msg_ctx,
                                     IODBNucleus * iodb,
                                     ArgProc* args) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mArgs(args)
{
}


RemoveLatchCycles::~RemoveLatchCycles()
{
}


void RemoveLatchCycles::module(NUModule * module)
{
  bool rewrote_cycle = false;
  NUContAssignList unmodified_assigns;

  UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);

  for (NUModule::ContAssignLoop loop = module->loopContAssigns();
       not loop.atEnd();
       ++loop) {
    NUContAssign * assign = (*loop);
    NUAlwaysBlock * replacement = convertCycleAssign(module,assign);
    if (replacement) {
      rewrote_cycle = true;
      module->addAlwaysBlock(replacement);
      delete assign;

      // we are not changing the module-level ud; only compute ud for
      // the created always block
      ud.structuredProc(replacement);
    } else {
      unmodified_assigns.push_back(assign);
    }
  }

  if (rewrote_cycle) {
    module->replaceContAssigns(unmodified_assigns);
  }
}


NUAlwaysBlock * RemoveLatchCycles::convertCycleAssign(NUModule * module, const NUContAssign * cassign)
{
  // only allow assigns of normal strength.
  if (cassign->getStrength()!=eStrDrive) {
    return NULL;
  }

  const NUExpr * rhs = cassign->getRvalue();

  // skip z-expressions.
  if (rhs->drivesZ()) {
    return NULL;
  }

  // only allow ternary expressions.
  if (rhs->getType()!=NUExpr::eNUTernaryOp) {
    return NULL;
  }

  const NUOp * rhs_op = dynamic_cast<const NUOp*>(rhs);
  const NUExpr * arg0 = rhs_op->getArg(0);
  const NUExpr * arg1 = rhs_op->getArg(1);
  const NUExpr * arg2 = rhs_op->getArg(2);

  // Only allow rewriting when the LHS occurs on the RHS as either the
  // then or else clause.
  const NULvalue * lhs = cassign->getLvalue();
  NUExpr * lhs_as_rhs = lhs->NURvalue();
  lhs_as_rhs->resize(lhs->getBitSize());

  bool matches_arg1 = ((*lhs_as_rhs) == (*arg1));
  bool matches_arg2 = ((*lhs_as_rhs) == (*arg2));
  bool matching = matches_arg1 or matches_arg2;
  delete lhs_as_rhs;

  if (not matching) {
    return NULL;
  }

  CopyContext cc(NULL,NULL);
  NUStmtList then_stmts;
  NUStmtList else_stmts;
  if (not matches_arg1) {
    NUBlockingAssign * assign = new NUBlockingAssign(lhs->copy(cc),
                                                     arg1->copy(cc),
                                                     false,
                                                     cassign->getLoc());
    then_stmts.push_back(assign);
  }
  if (not matches_arg2) {
    NUBlockingAssign * assign = new NUBlockingAssign(lhs->copy(cc),
                                                     arg2->copy(cc),
                                                     false,
                                                     cassign->getLoc());
    else_stmts.push_back(assign);
  }

  NUIf * condition = new NUIf(arg0->copy(cc),
                              then_stmts,
                              else_stmts,
                              mNetRefFactory,
                              false,
                              cassign->getLoc());

  NUBlock * block = new NUBlock(NULL, module, mIODB, mNetRefFactory, 
                                false, cassign->getLoc());
  block->addStmt(condition);
  
  // steal the name from the cont. assign; it is about to be deleted.
  NUAlwaysBlock * always = new NUAlwaysBlock(cassign->getName(),
                                             block,
                                             mNetRefFactory,
                                             cassign->getLoc(), false);
  return always;
}
