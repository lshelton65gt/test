// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef RESET_H_
#define RESET_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUCopyContext.h"

#include "bdd/BDD.h"

class MsgContext;
class AtomicCache;
class IODBNucleus;
class ArgProc;
class Fold;

/*!
  \file
  Declaration of asynchronous reset package.
 */

class UD;

//! Class to perform asynchronous reset detection and rewrite.
/*!
 * This class is able to perform asynchronous reset detection and rewrite
 * over a whole module or for a single always block.
 *
 * Asynchronous resets are handled by rewriting the always block into multiple
 * always blocks, one block for each reset condition, and one block for the
 * clocked condition.
 */
class Reset
{
public:
  //! Priority-order list of edge expressions with corresponding if statements,
  //! and a bool indicating whether they came from the always-block or the
  //! directive list
  typedef std::pair<NUEdgeExpr*,bool> EdgeBool;
  typedef std::pair<NUIf*,EdgeBool> IfEdge;
  typedef UtList<IfEdge> IfEdgeList;

  //! constuctor
  /*
    \param str_cache String cache to use to create new block names.
    \param msg_ctx Context to issue error messages.
    \param verbose If true, will be verbose while analysis occurs.
   */
  Reset(AtomicCache *str_cache,
	NUNetRefFactory *netref_factory,
	MsgContext *msg_ctx,
	IODBNucleus *iodb,
        ArgProc* args,
	bool verbose,
	bool enableSyncReset,
        bool enableAsyncReset,
        Fold* fold);

  //! destructor
  ~Reset();

  //! Handle resets for this module.
  void module(NUModule *module);

  //! Handle resets for this always block, populate a new list of always blocks to use.
  /*!
    \param module Module in which always block resides.
    \param block Always block to analyze.
    \param always_block_accumulator_list Updated list of always blocks for the module
    \param module_needs_new_ud if set to true then caller must recalc
           UD once the always blocks have been replaced.
    \returns true if a rewrite happened, false if no asynch resets occurred.

    new_list is always added-to, even if no rewrite occurred, and it
    may contain always blocks from previous calls to this method.
   */
  bool alwaysBlock(NUModule *module,
		   NUAlwaysBlock *block,
		   NUAlwaysBlockList *always_block_accumulator_list,
                   bool* module_needs_new_ud,
                   const NUEdgeExprSet& directiveEdges);

  //! Correlate edge expressions to corresponding statements.  Return true if successful.
  /*!
    Walk the statement list and correlate to edge expressions.  Edges which are
    correlated will be removed from the set.  The statement list will not be modified.

   \param block Always block we are processing
   \param stmts List of statements to walk.
   \param edge_set Set of edge expressions to correlate.
   \param if_edge_list If/Edge list which will be populated.
   \returns Success status.
   */
  bool stmtList(NUAlwaysBlock *block,
		NUStmtList *stmts,
		NUEdgeExprSet *edge_set,
		const NUEdgeExprSet& directive_edges,
		IfEdgeList *if_edge_list);

  //! Try to correlate an edge in the set with the given if statement.  Return true if successful.
  /*!
    If an edge is successfully correlated, it will be removed from the set.

   \param block Always block we are processing
   \param if_stmt If statement to walk.
   \param edge_set Set of edge expressions to correlate.
   \param if_edge_list If/Edge list which will be populated.
   \returns Success status.
   */
  bool ifStmt(NUAlwaysBlock *block,
	      NUIf *if_stmt,
	      NUEdgeExprSet *edge_set,
              const NUEdgeExprSet& directive_edges,
	      IfEdgeList *if_edge_list,
              NUEdgeExpr* edge,
              bool use_edge_from_always_block);

  //! Try to correlate an edge in the set to the given if-statement's
  //! conditional expression.
  /*!
   *! Return the correlated edge, if successful, 0 if not.
   *!
   *! Mutates the if-stmt by folding the condition
   */
  NUEdgeExpr *matchEdge(NUIf* if_stmt, const NUEdgeExprSet &edge_set,
                        bool allow_flip);

  // Determine if a list of statements has no uses.
  bool areUsesEmpty( NUStmtLoop sl );

private:
  //! Hide copy and assign constructors.
  Reset(const Reset&);
  Reset& operator=(const Reset&);

  /*! analyze and re-distribute the local declarations on all blocks in
   * block_list.  Nets that are def'ed in more than one block are
   * declared at the module level.  Nets that are local to a single
   * block are declared in that block.
   */
  void reDistributeLocals(NUModule *module, NUAlwaysBlockList *block_list, bool * module_needs_new_ud);


  /*!
   * Create an always block which is sensitive to the opposite edge that prev_block is
   * sensitive-to.
   * If prio_block is non-0, then make the new block have that as its priority block.
   *
   * The statement body of the if_stmt is copied.
   */
  NUAlwaysBlock *createFunkyResetBlock(NUModule *module,
				       IfEdgeList::iterator if_edge_iter,
				       IfEdgeList &if_edge_list,
				       NUAlwaysBlock *prev_block,
				       NUAlwaysBlock *prio_block,
				       NUAlwaysBlock *clock_block,
				       SourceLocator loc);

  /*!
   * Recursively construct an if-else stmt tree which covers all the resets
   * (all the resets starting at the given iter and until the end of the list).
   */
  NUStmt* createResetTree(CopyContext &copy_context,
			  IfEdgeList::iterator if_edge_iter,
			  IfEdgeList &if_edge_list);

  // Return the synchronous reset signal for an always block if one exists.
  void getSyncResetEdge( NUAlwaysBlock* always, NUEdgeExpr **sr, NUEdgeExpr **sr_bar );

  //! Find the candidate set of extra edges for nets listed in asyncReset directive
  void findDirectiveEdges(NUModule* mod, NUEdgeExprSet* directiveEdges);

  //! Clean up the directiveEdges set
  void cleanDirectiveEdges(NUEdgeExprSet* directiveEdges);

  //! String cache
  AtomicCache *mStrCache;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Message context.
  MsgContext *mMsgContext;

  //! BDD context.
  BDDContext mBDDContext;

  //! Be verbose as processing occurs?
  bool mVerbose;
  
  //! Turn off synchronous reset optimization.
  //! For debug or performance experiments.  Setting this flag should not
  //! impact functionality.
  bool mEnableSyncReset;

  //! Convert all async resets into sync resets -- used to combat DCL cycles
  bool mEnableAsyncReset;

  UD * mUD;

  //! IODB to store type intrinsic information into
  IODBNucleus *mIODB;

  //! Folder for conditions
  Fold* mFold;
};

#endif
