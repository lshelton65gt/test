// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "localflow/NucleusInterraCB.h"
#include "localflow/DesignPopulate.h"
#include "localflow/PrePopInterraCB.h"
#include "LFContext.h"
#include "nucleus/NUDesign.h"
#include "util/CbuildMsgContext.h"
#include "hdl/HdlVerilogPath.h"
#include "nucleus/NUNamedDeclarationScope.h"

#define CHK_POPULATE_ERRCODE_FATAL \
  do { \
    if (Populate::errorCodeSaysReturnNowOnlyWithFatal(mPhaseErrCode, &mErrCode)) { \
      mErrCode = Populate::eFatal; \
      return eStop; \
    } else if (mErrCode <= mPhaseErrCode) { \
      mErrCode = mPhaseErrCode; \
    } \
  } while(0) /* callers supplies semicolon */


class NUPort;
 
NucleusInterraCB::NucleusInterraCB(DesignPopulate* populator, 
                                   NUDesign* nudesign, 
                                   PrePopInterraCB* prePopCB)
  : mPopulator(populator), mPrePopInfo(prePopCB), mElabScope(NULL),
    mLFContext(NULL), mDesign(nudesign), mPackageNets(NULL)
{
  // start off pessimistic
  mErrCode = Populate::eFatal;
  mPhaseErrCode = Populate::eSuccess;

  mLFContext = mPopulator->getLFContext();
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::design(Phase phase, mvvNode /*rootMod*/, mvvNode /*architecture*/, mvvLanguageType /*langType*/)
{
  switch(phase)
  {
  case ePre:
    {
      mLFContext->initForDesignWalk();
      mLFContext->setDesign(mDesign);
      mErrCode = Populate::eSuccess;
    }
    break;
  case ePost:
    {
      switch(mErrCode)
      {
      case Populate::eSuccess:
        mDesign->addTopLevelModule(mLFContext->getRootModule());
        mPhaseErrCode = mPopulator->designFinish(mLFContext, Populate::eSuccess);
        // Perform the sanity check now that the design has been populated.
        // Why are we doing it here? There is special sanityCheckPopulatedNucleus
        // later.
        if ((mPhaseErrCode == Populate::eSuccess) &&
            !mLFContext->stmtFactory()->sanityCheckPopulatedNucleus(mDesign)) {
          mPhaseErrCode = Populate::eFailPopulate;
        }
        CHK_POPULATE_ERRCODE_FATAL;
        break;
      case Populate::eNotApplicable:
        break;
      case Populate::eFailPopulate:
        // Why do we add top level here? Should be investigated.
        mDesign->addTopLevelModule(mLFContext->getRootModule());
        break;
      case Populate::eFatal:
        break;
      }
    }
    break;
  }
  
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::hdlEntity(Phase phase, mvvNode node, mvvLanguageType langType, const char* entityName, bool* isCmodel)
{
  if (phase == ePre)
  {
    if (mElabScope == NULL)
      // at top level
      mElabScope = mPrePopInfo->findElabScope(NULL, entityName, langType);
    
    
    mLFContext->pushEdgeNetMap(); // start with a clean (empty) edge map on the way in -- avoid pollution from parent & sibling modules
    IODBNucleus* iodb = mLFContext->getIODB();
    
    switch(langType)
    {
    case MVV_VHDL:
      {
        const char* uniquifiedName = mPrePopInfo->getUniqueBlockName(mElabScope);
        
        vhNode vh_entity = node->castVhNode();

        const StringAtom *entityAtom = mLFContext->vhdlIntern( entityName );
        *isCmodel = iodb->isCModel(entityAtom->str());
        
        NUModule* the_module;
        bool seenAlready;
        mPhaseErrCode = mPopulator->mVhdlPopulate.entityDecl(vh_entity, mLFContext, uniquifiedName, &the_module, &seenAlready);
        if ( ( mPhaseErrCode == Populate::eFatal ) || seenAlready ){
          // I don't like this (executing code before the
          // CHK_POPULATE_ERRCODE_FATAL macro) but to fix this right
          // we would need to make sure that both pre and post phase are
          // called for all hdlEntities no matter what the return
          // status was for the pre phase.  This will also mean that the
          // handling of the BlockScope, DeclarationScope and
          // ExtraTaskEnableContext stacks will all need to be fixed.

          // since we return due to fatal/skip below, hdlEntity will not be called for ePost phase, so restore the previous NUEdgeNetMap now.
          mLFContext->popEdgeNetMap();
        }
        CHK_POPULATE_ERRCODE_FATAL;

        if (seenAlready) {
          return eSkip;
        }
        break;
      }
    case MVV_VERILOG:
      {
        // The walker will go to the vlog module or udp
        // we'll clean up the LFContext on the way out.

        // FIXME: Remove getOriginalName and just call intern with entityName
        const StringAtom* moduleName = mPopulator->mVlogPopulate.getOriginalName(node->castVeNode());
        *isCmodel = iodb->isCModel(moduleName->str());
      }
      break;
    case MVV_UNKNOWN_LANGUAGE:
      INFO_ASSERT(0, "Unknown language type.");
      break;
    }
    
  }
  else if (phase == ePost)
  {
    CHK_POPULATE_ERRCODE_FATAL;

    mLFContext->popEdgeNetMap(); // restore previous NUEdgeNetMap, needed in cases where we might have more expressions to process (for example while processing additional generate blocks)
    mLFContext->popBlockScope();
    mLFContext->popDeclarationScope();
    mLFContext->popExtraTaskEnableContext();
    
    switch(langType)
    {
    case MVV_VHDL:
      mLFContext->popJaguarScope();
      break;
    case MVV_VERILOG:
      break;
    case MVV_UNKNOWN_LANGUAGE:
      INFO_ASSERT(0, "Unknown language type.");
      break;
    }
  }

  CHK_POPULATE_ERRCODE_FATAL;  
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::cModel(Phase phase, mvvNode node, mvvLanguageType langType)
{
if (phase == ePost)
 {
   if (langType == MVV_VHDL)
   {
     vhNode vh_entity = node->castVhNode();
     JaguarString entityName(vhGetEntityName(vh_entity));
     const StringAtom *entityAtom = mLFContext->vhdlIntern( entityName );
     mPhaseErrCode = mPopulator->mVhdlPopulate.cModule(vh_entity, entityAtom->str(), mLFContext->getModule(), mLFContext);
   }
   else if (langType == MVV_VERILOG)
   {
     veNode ve_module = node->castVeNode();
     const StringAtom* moduleName = mPopulator->mVlogPopulate.getOriginalName(node->castVeNode());
     mPhaseErrCode = mPopulator->mVlogPopulate.cModule(ve_module, moduleName->str(), mLFContext->getModule(), mLFContext);
   }
 }
 
 CHK_POPULATE_ERRCODE_FATAL;
 return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::architecture(Phase phase, mvvNode, const char*, mvvLanguageType langType)
{
  // nothing to do in pre phase
  
  if (phase == ePost)
  {
    if (langType == MVV_VHDL)
    {
      mPhaseErrCode = mPopulator->mVhdlPopulate.extraTaskEnables(mLFContext, mLFContext->getIODB());
    }
    CHK_POPULATE_ERRCODE_FATAL;
  }
  
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::udp(Phase phase, mvvNode node, const char* unit, const char* precision, mvvLanguageType langType)
{
  Status ret = eNormal;
  if (phase == ePre)
  {
    ret = processModuleOrUDP(node, langType, unit, precision);
  }
  else if (phase == ePost)
  {
    veNode ve_udp = node->castVeNode();
    mPhaseErrCode = mPopulator->mVlogPopulate.udpComponents(ve_udp, mLFContext->getModule(), mLFContext);
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return ret;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::module(Phase phase, mvvNode node, const char* unit, const char* precision, mvvLanguageType langType)
{
  Status ret = eNormal;
  if (phase == ePre)
  {
    ret = processModuleOrUDP(node, langType, unit, precision);
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VERILOG)
      mPhaseErrCode = mPopulator->mVlogPopulate.extraTaskEnables(mLFContext, mLFContext->getIODB());
  }
  CHK_POPULATE_ERRCODE_FATAL;
  
  return ret;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::processModuleOrUDP(mvvNode node, mvvLanguageType langType,
                                     const char* unit, const char* precision)
{
  INFO_ASSERT(langType == MVV_VERILOG, "Module callback called with non-verilog language");
  
  const char* uniquifiedName = mPrePopInfo->getUniqueBlockName(mElabScope);
  veNode ve_module = node->castVeNode();
  
  NUModule* the_module;
  bool seenAlready;

  mPhaseErrCode = mPopulator->mVlogPopulate.moduleOrUDP(ve_module, mLFContext, uniquifiedName, unit, precision, &the_module, &seenAlready);

  CHK_POPULATE_ERRCODE_FATAL;

  if (seenAlready)
    return eSkip;

  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::port(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    NUNet* the_port = NULL;
    if (langType == MVV_VERILOG)
    {
      veNode ve_port = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.port(ve_port, mLFContext, &the_port);
    }
    else if (langType == MVV_VHDL)
    {
      vhNode vh_port = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.port(vh_port, mLFContext, &the_port);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
    
    // rjc here we should be getting the range of bits specified by
    // the port and keep track of those bits so that if we see the
    // same bit appear in the port list again we can report an
    // error. (add this arg to DesignPopulate::port)
    if (the_port)
    {
      NUModule* the_module = mLFContext->getModule();
      if (langType == MVV_VHDL)
        the_module->addPort(the_port);
      else if (langType == MVV_VERILOG) {
        if (the_module->getPortIndex(the_port) == -1) {
          the_module->addPort(the_port);
        } else {
          bool is_top_level = the_module == mLFContext->getRootModule();
          if ( is_top_level ){
            mPhaseErrCode = mPopulator->mVlogPopulate.topLevelMultiPortNet(the_port);
            CHK_POPULATE_ERRCODE_FATAL;
          }
        }
      }
    }
    
  } // pre
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::portList(Phase phase, mvvNode, mvvLanguageType)
{
  if (phase == ePost)
    CHK_POPULATE_ERRCODE_FATAL;
  
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::package(Phase phase, mvvNode, mvvLanguageType)
{
  if (phase == ePost)
    CHK_POPULATE_ERRCODE_FATAL;
    
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::packageDecl(Phase phase, mvvNode node, mvvLanguageType langType,
                              const char* libName, const char* packName, bool isStdLib)
{
  // Nothing to populate for the standard libraries.
  if (langType == MVV_VHDL)
  {
    if (isStdLib) {
      return eSkip; // Don't need to populate standard libraries.
    }
    vhNode pack_decl = node->castVhNode();
    if (phase == ePre)
    {
      // Creates and pushes package declaration scope onto context stack. Returns
      // pack_ignored true if package is not to be populated because it's a standard
      // library or if it has already been populated once.
      bool pack_ignored = false;
      mPhaseErrCode = 
        mPopulator->mVhdlPopulate.package(pack_decl, mLFContext, libName,
                                          packName, &pack_ignored);
      if (pack_ignored) {
        return eSkip; // No need to dive further into this package.
      }
      mPackageNets = new UtSet<StringAtom*>();
    }
    else if (phase == ePost)
    {
      mLFContext->popDeclarationScope();
      delete mPackageNets; // This package is done.
      mPackageNets = NULL;
    }
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status
NucleusInterraCB::declaration(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre && langType == MVV_VHDL && mPackageNets != NULL)
  {
    vhNode vh_net = node->castVhNode();
    StringAtom* net_atom = mLFContext->vhdlIntern(vhGetName(vh_net));
    if (mPackageNets->find(net_atom) != mPackageNets->end()) {
      // If already populated in this scope, skip it. This happens
      // when a net is declared in package declaration as well as body.
      // Example: test/vhdl/beacon6/vhdl-93.generate9.vhdl
      return eSkip;
    } else {
      // Insert the package net the first time.
      mPackageNets->insert(net_atom);
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::net(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    NUNet* the_net = NULL;
    
    if (langType == MVV_VHDL)
    {
      vhNode vh_net = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.net(vh_net, mLFContext, &the_net);
    }
    else if (langType == MVV_VERILOG)
    {
      veNode ve_net = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.net(ve_net, mLFContext, &the_net);
    }

    CHK_POPULATE_ERRCODE_FATAL;

    if (the_net)
      mLFContext->getDeclarationScope()->addLocal(the_net);
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::localNet(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    NUNet* the_net = NULL;
    if (langType == MVV_VHDL)
    {
      vhNode vh_net = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.local(vh_net, mLFContext, &the_net);
    }
    else if (langType == MVV_VERILOG)
    {
      veNode ve_net = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.local(ve_net, mLFContext, &the_net);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
    if (the_net)
      mLFContext->getDeclarationScope()->addLocal(the_net);
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::variable(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    NUNet* the_net = NULL;
    if (langType == MVV_VHDL)
    {
      vhNode vh_var = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.var(vh_var, mLFContext, &the_net);
    }
    else if (langType == MVV_VERILOG)
    {
      veNode ve_var = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.var(ve_var, mLFContext, &the_net);
    }

    CHK_POPULATE_ERRCODE_FATAL;

    if (the_net)
      mLFContext->getDeclarationScope()->addLocal(the_net);
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::alias(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_alias = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.createAndAssignSignalForAliasObject(vh_alias, mLFContext);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::file(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_file = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.file(vh_file, mLFContext);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::concSigAssign(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.maybeGuardedConcSigAssign(vh_conc_stmt, mLFContext);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::conditionalSigAssign(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.concurrentConditionalSigAssign(vh_conc_stmt, mLFContext);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::selSigAssign(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.concurrentSelSigAssign(vh_conc_stmt, mLFContext);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::concProcess(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      StringAtom* procDeclScope = mPrePopInfo->getUniqueVhdlProcessName(vh_conc_stmt);
      mPopulator->mVhdlPopulate.prePopulateProcessStmt(vh_conc_stmt, procDeclScope,
                                                       mLFContext);
      
      CHK_POPULATE_ERRCODE_FATAL;
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      NUAlwaysBlock* the_process = NULL;
      mPhaseErrCode = 
        mPopulator->mVhdlPopulate.postPopulateProcessStmt(vh_conc_stmt, mLFContext,
                                                          &the_process);
      
      CHK_POPULATE_ERRCODE_FATAL;

      if (the_process) {
        mLFContext->getModule()->addAlwaysBlock(the_process);
      }
    }
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::concProcCall(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.concProcedureCall(vh_conc_stmt, mLFContext);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::blockStmt(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      mElabScope = mPrePopInfo->findElabScope(mElabScope, blockName, MVV_VHDL);

      vhNode vh_block = node->castVhNode();
      // blockStmt pushes the declaration and the jaguar scopes
      mPhaseErrCode = mPopulator->mVhdlPopulate.blockStmt(vh_block, blockName, mLFContext);
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VHDL)
    {
      // pop the declaration and jaguar scopes
      mLFContext->popDeclarationScope();
      mLFContext->popJaguarScope();
      mElabScope = mElabScope->getParent();
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::forGenerate(Phase phase, mvvNode node, mvvNode indexVal, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      vhExpr vh_expr = (vhExpr)indexVal;

      StringAtom* forGen = NULL;
      Populate::ErrorCode tmp_err_code = mPopulator->mVhdlPopulate.calcForGenerateBlockName(vh_conc_stmt, vh_expr, mLFContext, &forGen);
      if ( tmp_err_code != Populate::eSuccess ) {
        mErrCode = tmp_err_code;
        return eStop;
      }
      
      mElabScope = mPrePopInfo->findElabScope(mElabScope, forGen->str(), MVV_VHDL);
      
      // pushes declaration and jaguar scopes
      mPhaseErrCode = mPopulator->mVhdlPopulate.forGenerate(vh_conc_stmt, forGen, mLFContext);
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VHDL)
    {
      mLFContext->popDeclarationScope();
      mLFContext->popJaguarScope();
      mElabScope = mElabScope->getParent();
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::ifGenerateTrue(Phase phase, mvvNode node, const char* blockName, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      mElabScope = mPrePopInfo->findElabScope(mElabScope, blockName, MVV_VHDL);
      
      vhNode vh_conc_stmt = node->castVhNode();
      //  pushes the declaration and the jaguar scopes
      mPhaseErrCode = mPopulator->mVhdlPopulate.ifGenerateTrue(vh_conc_stmt, blockName, mLFContext);
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VHDL)
    {
      // pop the declaration and jaguar scopes
      mLFContext->popDeclarationScope();
      mLFContext->popJaguarScope();
      mElabScope = mElabScope->getParent();
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::assertStmt(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VHDL)
    {
      vhNode vh_conc_stmt = node->castVhNode();
      mPhaseErrCode = mPopulator->mVhdlPopulate.assertStmt(vh_conc_stmt, mLFContext);
    }

    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::contAssign(Phase phase, mvvNode node, mvvLanguageType langType)
{
  NUContAssign* the_assign = 0;
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_assign = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.contAssign(ve_assign, mLFContext, &the_assign);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
    
    if (the_assign)
      mLFContext->getModule()->addContAssign(the_assign);
  }

  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::declaredAssign(Phase phase, mvvNode node, mvvLanguageType langType)
{
  NUContAssign* the_assign = 0;
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_assign = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.declAssign(ve_assign, mLFContext, &the_assign);
    }
  }

  CHK_POPULATE_ERRCODE_FATAL;

  if (the_assign)
    mLFContext->getModule()->addContAssign(the_assign);
  
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::declaredAssignList(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_module = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.processModuleDeclAssigns(ve_module, mLFContext);
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::alwaysBlock(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_always = node->castVeNode();
      NUModule* module = mLFContext->getModule();
      mPhaseErrCode = mPopulator->mVlogPopulate.addAlwaysBlock(ve_always, mLFContext, module);
    }
  }

  CHK_POPULATE_ERRCODE_FATAL;
  return eSkip;   // tell the walker to skip the contents of this always block, it was handled above
}
InterraDesignWalkerCB::Status 
NucleusInterraCB::specifyBlock(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_spec = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.specifyBlock(ve_spec, mLFContext);
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::gatePrimitive(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_inst = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.gatePrimitive(ve_inst, mLFContext);
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::initialBlock(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    NUInitialBlock* the_initial = NULL;
    
    if (langType == MVV_VERILOG)
    {
      veNode ve_initial = node->castVeNode();
      mPhaseErrCode = mPopulator->mVlogPopulate.initialBlock(ve_initial, mLFContext, &the_initial);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;

    if (the_initial)
      mLFContext->getModule()->addInitialBlock(the_initial);
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::taskFunction(Phase phase, mvvNode node, const char* tfName, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_tf = node->castVeNode();
      NUTask* the_tf = NULL;
      // tf apparently adds the NUTF to the module
      mPhaseErrCode = mPopulator->mVlogPopulate.tf(ve_tf, tfName, mLFContext, &the_tf);
    }
    
    CHK_POPULATE_ERRCODE_FATAL;
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::generateBlock(Phase phase,
				mvvNode node,
				const char* blockName,
				mvvLanguageType langType, 
				const SInt32* genForIndex)
{
  if (strlen(blockName) == 0)
    return eNormal;

  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      mElabScope = mPrePopInfo->findElabScope(mElabScope, blockName, MVV_VERILOG);

      veNode ve_gen = node->castVeNode();
      // push a declaration scope
      mPhaseErrCode = mPopulator->mVlogPopulate.generateBlock(ve_gen, genForIndex, blockName, mLFContext);
    }
  }
  else if (phase == ePost)
  {
    if (langType == MVV_VERILOG)
    {
      mLFContext->popDeclarationScope();
      mElabScope = mElabScope->getParent();
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::scopeVariable(Phase phase, mvvNode node, mvvLanguageType langType)
{
  if (phase == ePre)
  {
    if (langType == MVV_VERILOG)
    {
      veNode ve_hier = node->castVeNode();
      NUNet *the_net = 0;
      mPhaseErrCode = mPopulator->mVlogPopulate.netHierRef(ve_hier, mLFContext, &the_net);
      if (the_net) {
	mLFContext->getModule()->addNetHierRef(the_net);
      }
    }
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::hdlComponentInstanceDeclaration(Phase phase, mvvNode, mvvLanguageType)
{
  if (phase == ePost)
  {
    mLFContext->popPortMap();
  }
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::hdlComponentInstance(Phase phase, 
				       mvvNode inst,
                                       const char* instName,
                                       mvvNode instMaster,
                                       mvvNode oldEntity,
                                       mvvNode newEntity,
                                       const char* /*entityName*/,
                                       mvvNode /*arch*/,
                                       const SInt32* vectorIndex, 
                                       const UInt32* rangeOffset,
                                       mvvLanguageType instLang, 
                                       mvvLanguageType scopeLang)
{
  if (phase == ePre)
  {
    // Use the scope language type to push the instance name. The
    // instance type will be used to populate its scope.
    StringAtom* instAtom = NULL;
    if (scopeLang == MVV_VERILOG)
    {
      veNode ve_inst = inst->castVeNode();
      // pushes instance name
      // it has to get mangled because NamedDeclarationScopes cannot
      // contain instances (only mangled if mUseLegacyGenerateHierarchyNames true)
      instAtom = mPopulator->
        mVlogPopulate.getInstanceName(ve_inst,
                                      vectorIndex,
                                      mLFContext);
    }
    else if (scopeLang == MVV_VHDL)
    {
      vhNode vh_inst = inst->castVhNode();

      Populate::ErrorCode tmp_err_code = mPopulator->mVhdlPopulate.accelerateGtechComponent( vh_inst, mLFContext );
      if ( tmp_err_code == Populate::eSuccess )
      {
        return eSkip;
      }

      instAtom = mPopulator->mVhdlPopulate.getInstanceName(vh_inst, mLFContext);
    }

    // pushes instance name
    mLFContext->pushInstanceName(instAtom);
    // Set the current elaborated scope.
    mElabScope = mPrePopInfo->findElabScope(mElabScope, instName, scopeLang);
    
  } // pre
  else if (phase == ePost)
  {
    // Here is where things get a little confusing, so lets explain what the different
    // things mean:
    // inst       - The instance. Can be a module instance or a component instance.
    // instLang   - The language of the instance.
    // instMaster - The master of the instance. Can be a Verilog module or a VHDL entity.
    // scopeLang  - The language of the instMaster.
    // oldEntity  - If there has been no substitutions (i.e. newEntity == NULL), then instMaster == oldEntity
    // newEntity  - If there has been substitutions (i.e. newEntity != NULL), then instMaster == newEntity
    //
    // The complication arises because of the subsitution, which means that a VHDL entity can be replaced by
    // a Verilog module or a VHDL entity, and a Verilog module can be replaced by a another Verilog module or
    // a VHDL entity. 
    //
    // To create a new NUModuleInstance, the actuals of the instance must be mapped to the formals of the master.
    //

    if (newEntity == NULL)
    {
      if (instLang == MVV_VERILOG && scopeLang == MVV_VERILOG)
      {
	// A verilog module instantiating a verilog module with no substitutions
	mPhaseErrCode = mPopulator->mVlogPopulate.oneModuleInstancePost(inst->castVeNode(),
									(newEntity == NULL) ? NULL : newEntity->castVeNode(),
									rangeOffset,
									mLFContext);
      }
      else if (instLang == MVV_VHDL && scopeLang == MVV_VHDL)
      {
	// A VHDL entity instantiating a VHDL entity with no substitutions
	mPhaseErrCode = mPopulator->mVhdlPopulate.componentInstancePost(inst->castVhNode(),
									instMaster->castVhNode(),
									oldEntity->castVhNode(),
									mLFContext);
      }
      else if (instLang == MVV_VERILOG && scopeLang == MVV_VHDL)
      {
	// A VHDL entity instantiating a Verilog module with no substitutions
	mPhaseErrCode = mPopulator->mVlogPopulate.VlogEntity(inst->castVhNode(),
							     oldEntity,
							     NULL, // newEntity
							     mLFContext);
      }
      else if (instLang == MVV_VHDL && scopeLang == MVV_VERILOG)
      {
	// A Verilog module instantiating a VHDL entity with no substitutions
	mPhaseErrCode = mPopulator->mVhdlPopulate.VhdlModule(inst->castVeNode(),
							     instMaster->castVhNode(),
							     rangeOffset,
							     mLFContext);
      }
      else
      {
	// Should never happen!
	INFO_ASSERT(0, "Unsupported language.");
      }
    }
    else // newEntity != NULL, i.e. it was substituted
    {
      // This corresponds to all possible substitutions, i.e.
      // A VHDL entity replaced by a Verilog module or a VHDL entity or
      // A Verilog module replaced by a Verilog module or a VHDL entity.
      mPhaseErrCode = mPopulator->moduleInstance(inst,
						 oldEntity,
						 newEntity,
						 rangeOffset,
						 mLFContext);
    }

    CHK_POPULATE_ERRCODE_FATAL;

    // pop the instance name from the stack. Pushed in pre phase.
    mLFContext->popInstanceName();
    mElabScope = mElabScope->getParent();
  } // post
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::nestedGenerateInstance(mvvNode node, mvvLanguageType langType)
{
  if (langType == MVV_VERILOG)
  {
    veNode ve_gen = node->castVeNode();
    mPhaseErrCode = mPopulator->mVlogPopulate.nestedGenerateInstance(ve_gen);
  }
  else
    // fail safe
    INFO_ASSERT(0, "Nested generate instance.");
  
  CHK_POPULATE_ERRCODE_FATAL;
  
  return eNormal;
}

void NucleusInterraCB::unnamedGenerateBlock(veNode ve_node, const char* blockName, mvvLanguageType langType)
{
  if (langType == MVV_VERILOG && strlen(blockName) != 0)
    mPopulator->mVlogPopulate.unnamedGenerateBlock(ve_node, blockName);
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::emptyArchitecture(mvvNode node, const char*, mvvLanguageType langType)
{
  if (langType == MVV_VHDL)
  {
    vhNode vh_arch = node->castVhNode();
    mPhaseErrCode = mPopulator->mVhdlPopulate.emptyArchitecture( vh_arch );
  }
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::unsupportedDeclaration(mvvNode node, mvvLanguageType langType)
{
  if (langType == MVV_VHDL)
  {
    vhNode vh_decl = node->castVhNode();
    mPhaseErrCode = mPopulator->mVhdlPopulate.unsupportedDeclaration(vh_decl);
  }
  else
    // fail safe
    INFO_ASSERT(0, "Unsupported declaration.");

  CHK_POPULATE_ERRCODE_FATAL;

  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::unsupportedConcStmt(mvvNode node, mvvLanguageType langType)
{
  if (langType == MVV_VHDL)
  {
    vhNode vh_decl = node->castVhNode();
    mPhaseErrCode = mPopulator->mVhdlPopulate.unsupportedConcStmt(vh_decl);
  }
  else
    // fail safe
    INFO_ASSERT(0, "Unsupported concurrent statement");

  CHK_POPULATE_ERRCODE_FATAL;
  
  return eNormal;
}

InterraDesignWalkerCB::Status 
NucleusInterraCB::unsupportedInstanceType(mvvNode node, mvvLanguageType langType)
{
  if (langType == MVV_VERILOG)
  {
    veNode ve_inst = node->castVeNode();
    mPhaseErrCode = mPopulator->mVlogPopulate.unsupportedInstanceType(ve_inst);
  }
  else
    // fail safe
    INFO_ASSERT(0, "Unsupported instance type");

  CHK_POPULATE_ERRCODE_FATAL;
  
  return eNormal;
}

InterraDesignWalkerCB::Status  
NucleusInterraCB::implicitGuardNullInGuardedBlock(mvvNode node, mvvLanguageType lang)
{
  if (lang == MVV_VHDL)
  {
    vhNode vh_block = node->castVhNode();
    mPhaseErrCode = mPopulator->mVhdlPopulate.implicitGuardNullInGuardedBlock(vh_block);
  }

  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;  
}

InterraDesignWalkerCB::Status  
NucleusInterraCB::componentElabFail(mvvNode component, mvvLanguageType lang,
                                    int elab_status)
{
  // The Interra msg callback already handled the error report, if elab_status is 1,
  // which means elaboration failed. If elab_status is 2, which means a black box
  // was created, the error is not generated by Interra.
  if ((elab_status == 2) && (lang == MVV_VHDL))
  {
    UtString str;
    MsgContext* msgContext = mLFContext->getMsgContext();
    SourceLocator loc = mPopulator->mVhdlPopulate.locator(component->castVhNode());
    msgContext->JaguarElaborationFailure(&loc, VhdlPopulate::decompileJaguarNode(component->castVhNode(), &str));
  }
  mPhaseErrCode = Populate::eFailPopulate;
  CHK_POPULATE_ERRCODE_FATAL; // update mErrCode and fall through
  return eStop;
}

InterraDesignWalkerCB::Status  
NucleusInterraCB::generateElabFail(mvvNode node, mvvLanguageType lang)
{
  if (lang == MVV_VERILOG)
  {
    veNode ve_gen = node->castVeNode();
    mPhaseErrCode = mPopulator->mVlogPopulate.generateElabFail(ve_gen);
  }
  
  CHK_POPULATE_ERRCODE_FATAL;
  return eNormal;  
}

InterraDesignWalkerCB::Status  
NucleusInterraCB::generateElabError(mvvNode, mvvLanguageType)
{
  mPhaseErrCode = Populate::eFailPopulate;
  CHK_POPULATE_ERRCODE_FATAL;
  return eStop;  
}

mvvNode NucleusInterraCB::substituteEntity(mvvNode entityDecl, 
					   mvvLanguageType entityLanguage, 
					   const UtString& entityDeclName, 
					   mvvNode instance, 
					   bool isUDP, 
					   mvvLanguageType callingLanguage)
{
  NUModule* parent = mLFContext->getModule();
  NUPortMap* portMap = NULL;
  bool found = false;
  UtString new_mod_name;
  UtString instanceName;
  Populate::composeInstanceName(instance, mLFContext, &instanceName, mPopulator->mVlogPopulate.getTicProtectedNameManager());

  mvvNode node = getSubstituteEntity(parent->getOriginalName()->str(),
				     entityDecl,
				     entityLanguage,
				     entityDeclName,
				     instanceName,
				     isUDP,
				     callingLanguage,
				     mPopulator->mVlogPopulate.getTicProtectedNameManager(),				   
				     mLFContext->getIODB(),
				     &new_mod_name,
				     &found,
				     &portMap);
			     
  mLFContext->pushPortMap(portMap);

  if (node == NULL && found) {
    mLFContext->getMsgContext()->SubstituteModuleNotFound(new_mod_name.c_str());
  }

  return node;
}

InterraDesignWalkerCB::Status
NucleusInterraCB::vlogForeignAttribute(mvvNode module, const UtString& entityDeclName, mvvNode* ent, mvvNode* arch) 
{
  InterraDesignWalkerCB::Status status = getEntArchPairFromForeignAttr(module, ent, arch);
  
  if (status == InterraDesignWalkerCB::eStop) {
    SourceLocator loc = mPopulator->mVlogPopulate.locator(module->castVeNode());
    mLFContext->getMsgContext()->EntityOrArchitectureSpecifiedInForeignAttributeNotFound(&loc, entityDeclName.c_str());
  }
  else if (status == InterraDesignWalkerCB::eInvalid) {
    SourceLocator loc = mPopulator->mVlogPopulate.locator(module->castVeNode());
    mLFContext->getMsgContext()->IncorrectForeignAttribute(&loc, entityDeclName.c_str());
    status = InterraDesignWalkerCB::eStop;
  }

  return status;
}

// END FILE

