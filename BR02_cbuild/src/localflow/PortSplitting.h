//! -*-C++-*-

/***************************************************************************************
  Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef PORTSPLITTING_H_
#define PORTSPLITTING_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUNetRefMap.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUMappedNetRefList.h"
#include "util/UnionFind.h"
#include "util/DisjointRange.h"


/*!
  \file
  Declaration of port splitting package.
*/


class AtomicCache;
class IODBNucleus;
class Stats;
class ArgProc;
class MsgContext;
class SourceLocatorFactory;
class ESFactory;
class Fold;
class LowerPortConnections;
class PortAnalysis;

typedef UnionFind<NUNet*, NUNetSet> NUNetUnionFind;

//! Model the connectivity between a formal and its connected actuals.
/*!
 * There is a one-to-one relationship between PortConnectionData
 * objects and a NUPortConnection object. Since any module may be
 * instantiated multiple times, we may have multiple
 * PortConnectionData objects for any one port NUNet.
 *
 * The PortConnectionData object models the way a port and its
 * connections have been subdivided. Most importantly, it contains
 * lists of NUNetRefHdl for the formal and actual side of the port.
 * These NUNetRefHdl lists model the current decomposition.
 *
 * For example, if we originally had the port:
 *
 * \verbatim
 *   sub s0( .in({a,b}) );
 * \endverbatim
 *
 * The PortConnectionData (post-analysis) would contain:
 *
 * \verbatim
 *   Actuals: {     a,     b }
 *   Formals: { in[1], in[0] }
 * \endverbatim
 */
class PortConnectionData
{
public:
  //! Constructor
  /*!
   * \param netRefFactory The NUNetRef factory.
   * \param connection    The associated port connection (of any direction).
   */
  PortConnectionData(NUNetRefFactory * netRefFactory,
                     NUPortConnection * connection,
                     NUMappedNetRefList * formals);

  //! Copy constructor
  /*!
   * Create a copy of some PortConnectionData. The formal and actual
   * lists are replicated; everything else is copied.
   *
   * \param other The source PortConnectionData.
   */
  PortConnectionData(const PortConnectionData & other);

  //! Destructor
  ~PortConnectionData();

  //! Perform initial population of formal and actual lists.
  /*!
   * Traverse the NUPortConnection and determine the initial
   * decomposition of the ports. 
   *
   * The formal list is always populated with a single NUNetRef for
   * the full formal net.
   *
   * The actual list is populated based on the port connection. Full
   * identifiers become full NUNetRefs. Bit-selects and part-selects
   * become bit and part NUNetRefs; concats are processed recursively
   * until the simpler structures are discovered.
   *
   * Anything more complex than ident, bit, part, or simple concat is
   * unexpected and triggers an error. The hope is that some lowering
   * process already normalized all this away.
   */
  void populate();

  //! Integrate any splitting necessitated by higher points in the hierarchy.
  /*!
   * If any updates occurr, mark this object as modified.
   *
   * \return true if any modifications occurr.
   * \sa addFromBelow, addToTarget
   */
  bool addFromAbove(const NUNetRefHdl & above);

  //! Integrate any splitting necessitated by lower levels in the hierarchy.
  /*!
   * If any updates occurr, mark this object as modified.
   *
   * \return true if any modifications occurr.
   * \sa addFromAbove, addToTarget
   */
  bool addFromBelow(const NUNetRefHdl & below);

  //! Integrate splitting necessitated from an adjacent level of hierarchy.
  /*!
   * If any updates occurr, mark this object as modified.
   *
   * \return true if any modifications occurr.
   * \sa addFomAbove
   */
  bool addFromAdjacent(const NUNetRefHdl & adjacent);

  //! Propagate splitting requirements between formal and actual components of this port connection.
  /*!
   * If any updates occurr, mark this object as modified.
   *
   * \return true if any propagation occurr.
   * \sa propagateFromBelow, propagateFromAbove
   */
  bool propagate();

  //! Propagate splitting from actual to formal.
  /*!
   * If any updates occurr, mark this object as modified.
   *
   * \return true if any propagation occurr.
   */
  bool propagateFromAbove();

  //! Propagate splitting from formal to actual.
  /*!
   * If any updates occurr, mark this object as modified.
   *
   * \return true if any propagation occurr.
   */
  bool propagateFromBelow();

  /*!
   *! rewrite an element of a net ref list based on a splitup...
   */
  bool processIntersection(NUMappedNetRefList * target,
                           NUNetRefList::iterator* iter,
                           const NUNetRefHdl& target_net_ref,
                           const NUNetRefHdl& part);

  //! Return the associated port connection.
  NUPortConnection * getPortConnection() const { return mConnection; }

  //! Return the formal connected to our port connection
  NUNet * getFormal() const;

  //! Return the depth.
  SInt32 getDepth() const { return mDepth; }

  //! Set the depth; only one set permitted.
  void setDepth(SInt32 depth) { 
    INFO_ASSERT(mDepth==0, "PortConnectionData::setDepth() previously called");
    mDepth = depth; 
  }

  //! Iterate over the formal NUNetRefs.
  NUNetRefListLoop loopFormals() const {
    return mFormals->loopAllRefs();
  }

  //! Iterate over the actual NUNetRefs.
  NUNetRefListLoop loopActuals() const {
    return mActuals.loopAllRefs();
  }

  NUMappedNetRefList* getActuals() {return &mActuals;}

  //! Is the actual something that can be connected to a full net?
  bool isSimpleActual() const;

 //! Return if there have been any updates.
  bool isModified() const { return (mModified or (mFormals->size()>1)); }

  //! Return if we need splitting.
  bool needsSplitting() const { return (mModified or mSplittingForced); } // do not use (isModified() or mSplittingForced): see: test/port-splitting/concat3.v

  //! Force splitting of this data.
  void forceSplitting() { mSplittingForced = true; }

  //! Is there a bidi below this port connection?
  bool coversInout() const { return mCoversInout; }

  //! Update whether or not this port connection covers an inout (additive).
  void putCoversInout(bool covers) { mCoversInout |= covers; }

  //! debug print.
  void print() const;
private:

  //! Populate the formal NUMappedNetRefList from the formal part of the port connection.
  /*!
   * Since the formals list is shared with other PortConnectionData,
   * populate only if the formals list isn't already populated.
   */
  void populateFormal();

  //! Populate the actual NUMappedNetRefList from the actual (RHS) part of the port connection.
  void populateActual(const NUExpr * actual);

  //! Populate the actual NUMappedNetRefList from the actual (LHS) part of the port connection.
  void populateActual(const NULvalue * actual);

  //! Populate the actual NUMappedNetRefList from a one-element NUNetRefSet
  /*!
   * \sa populateActual
   */
  void populateActuals(NUNetRefSet * actuals);

  //! Ensure that the formal and actual lists are of like sizes.
  /*!
   * Count the number of bits in both the formal and actual NUNetRef
   * lists. Trigger an ASSERT if the two lists have a different number
   * of bits.
   *
   * The only exception to this rule is if we have a disconnected
   * port; the actual within the NUPortConnection is NULL and our
   * actual NUNetRef list will be empty.
   *
   * \sa countBits
   */
  void populationConsistency() const;

  //! Count the number of bits in a NUNetRef list.
  UInt32 countBits(const NUMappedNetRefList * parts) const;

  //! Helper method; Take the specified subdivision and apply to a NUNetRef list.
  /*!
   * Take a NUNetRef and apply its characteristics to the target list.
   *
   * For example, if we were provided the following input:
   *
   * \verbatim
   *   target = { a[4:0], b[4:0], c[4:1] }
   *   part   = b[2:1]
   * \endverbatim
   *
   * The target list would be modified to take the splitting defined
   * by 'part' into account:
   *
   * \verbatim
   *   target = { a[4:0], b[4:3], b[2:1], b[0], c[4:1] }
   * \endverbatim
   *
   * \return true if the target list is modified.
   * \sa addFromAbove, addFromBelow, addFromAdjacent
   */
  bool addToTarget(NUMappedNetRefList * target, const NUNetRefHdl & part);

  //! Helper method; Propagate subdivisions from one list to another.
  /*!
   * Make sure that the NUNetRefs in both 'source' and 'target' lists
   * line up. Only changes from source to target are propagated. 
   *
   * Reverse the arguments and re-call for propagation in the other
   * direction.
   *
   * For example, if the following were provided as input:
   *
   * \verbatim
   *   source = { a[4:0], b[0], c[2:1] }
   *   target = { x[3:2], y[5:0] }
   * \endverbatim
   *
   * The target list would be modified to take the splitting defined
   * by 'source' into account:
   *
   * \verbatim
   *   source = {         a[4:0], b[0], c[2:1] }
   *   target = { x[3:2], y[5:3], y[2], y[1:0] }
   * \endverbatim
   *
   * Note that 'target' still defines splits that are not reflected in
   * 'source' (namely {x[3:2],y[5:3]} implies that a[4:0] should be
   * subdivided).
   *
   * \return true if the target list is modified.
   * \sa propagateFromAbove, propagateFromBelow
   */
  bool propagate(const NUMappedNetRefList * source, NUMappedNetRefList * target);

  //! Print out a NUMappedNetRefList with some specified indentation.
  void printNetRefList(const NUMappedNetRefList * refs, int indent) const;

  NUNetRefFactory * mNetRefFactory;

  //! The original port connection.
  NUPortConnection * mConnection;

  // List of formal net refs (one per formal net). 
  NUMappedNetRefList * mFormals;

  // list of actual net refs
  NUMappedNetRefList mActuals;

  //! The depth of this data element in the unelaborated port-connection graph.
  SInt32 mDepth;

  //! Have any changes occured? 
  /*!
   * Modifications include any alteration to the actual and formal
   * lists. This is due to propagation to and from formal and actual,
   * and hierarchical propagation during the top-down/bottom-up
   * traversal (see PortSplitting::processCluster).
   */
  bool mModified;

  //! Is splitting forced?
  bool mSplittingForced;

  //! Does this port connection cover an inout?
  bool mCoversInout;
};

typedef UtSet<PortConnectionData*> PortConnectionDataSet;

//! One-to-one map between a port connection and its data.
typedef UtMap<NUPortConnection*,PortConnectionData*> PortConnectionDataMap;

//! Lookup PortConnectionData based on connected actuals.
/*!
 * If a port connection uses three actuals:
 *
 * \verbatim
 *   sub s0 ( .io({a[3:2], b[1:0], c[1]}) );
 * \endverbatim
 *
 * This map will contain three elements referencing the same
 * PortConnectionData:
 *
 * \verbatim
 *   { a[3:2] => PortConnectionData(io),
 *     b[1:0] => PortConnectionData(io),
 *     c[1]   => PortConnectionData(io) }
 * \endverbatim
 */
typedef NUNetRefMultiMap<PortConnectionData*> NUNetRefPortConnectionDataMap;

//! Map used to store the conversion from NUNetRef to a newly-created NUNet.
/*!
 * If a port is split into two parts: 
 *
 * \verbatim
 *   { io[3:2], io[1:0] } 
 * \endverbatim
 *
 * this map will contain the newly created net for each part: 
 *
 * \verbatim
 *   { io[3:2] => $portsplit_io_3_2,
 *     io[1:0] => $portsplit_io_1_0 }
 * \endverbatim
 *
 * We use this stored information when rewriting port connections in
 * terms of the new nets.
 */
typedef NUNetRefMultiMap<NUNet*> NUNetRefNetMap;

//! Map used to store the computed depth for some NUNet.
typedef UtMap<NUNet*,SInt32> NUNetDepthMap;

//! Track information about how many splits we performed.
class PortSplittingStatistics
{
public:
  //! Constructor.
  PortSplittingStatistics();

  //! Destructor.
  ~PortSplittingStatistics();

  //! Clear all statistics
  void clear();

  //! Increment the count of source nets.
  void source();

  //! Increment the count of target nets.
  void target();

  //! Print a summary line.
  void print() const;
private:
  //! The number of original nets we have split.
  SInt64 mSources;

  //! The number of resulting nets.
  SInt64 mTargets;
};


//! Analyze a design and split ports that prevent full-net-connections for inouts.
/*!
 *
 * I. Purpose
 *
 * Perform a design to determine unelaborated net connectivity across
 * port connections. Use this connectivity information to analyze how
 * different parts of nets are connected.
 *
 * Propagate net-subdivisions across multiple uses of those nets.
 * Perform this propagation until nets have been subdivided such that
 * no port connection refers to a partial subdivision. 
 *
 * We re-define port connections based on these subdivisions.
 *
 * \sa PortCoercion
 */
class PortSplitting
{
public:
  //! Constructor
  PortSplitting(AtomicCache* strCache, 
                IODBNucleus* iodb, 
                Stats* stats, 
                ArgProc* args,
                MsgContext* msgContext, 
                NUNetRefFactory* netRefFactory,
                SourceLocatorFactory* sourceLocatorFactory,
                NUNetSet * inouts,
                NUNetSet * inputs,
                NUNetSet * outputs,
                bool verbose);

  //! Destructor
  ~PortSplitting();

  //! Perform port splitting over the entire design.
  /*!
   * \param design     The design.
   * \param phaseStats Should we calculate intermediate performance information?
   */
  void design(NUDesign * design, bool phaseStats);

private:
  //! Process a design and locate the necessary port-splits.
  /*!
   * \param phaseStats Should we calculate intermediate performance statistics?
   */
  void discoverSplitting(bool phaseStats);

  //! Traverse the design and save information about the connectivity across port connections.
  /*!
   * \param closure Set closure defining the distinct, connected port-connection sub-graphs.
   *
   * \sa SplittingTraverseCallback
   */
  void traverse(NUNetClosure * closure);

  //! Generate a map to PortConnectionData from NUNetRef used as actual.
  void populateActualToPortConnectionData();

  //! Generate a map to PortConnectionData from NUNetRef used as formal.
  void populateFormalToPortConnectionData();

  //! Equate hierarchical references/referees in closure.
  /*!
   * Hierarchical references are included in two equivalence class structures. 
   *
   * The net closure joins the hierarchical references into
   * equivalence classes with other ports and port connections. The
   * net closure ensures that we process all related nets (connected
   * via port connection or hierarchical reference) at the same time.
   *
   * The hier-ref union collects all hierarchical references and
   * resolutions into equivalence classes. We propagate split
   * information across all members of this equivalence class in a
   * uniform manner.
   */
  void populateHierRefData(NUNetClosure * closure);

  //! Top-down propagation of primary inout connections.
  void propagatePrimaryInout();

  //! Top-down propagation of primary inout connections.
  /*!
   * \param data The data for one port connection.
   * \param primary_above Did our recursive processing reveal a primary inout above?
   */
  void propagatePrimaryInout(PortConnectionData * data, bool primary_above);

  //! Calculate depth for PortConnectionData and associated nets.
  /*!
   * Perform a depth-first traversal of the port-connection graph.
   * Leaves are assigned a depth of 1. Parents receive a depth of
   * max(depth of children) + 1.
   *
   * Nets receive the same depth as max(depth(port connections)) where
   * that net is used as an actual.
   *
   * At the same time, perform a bottom-up propagation of inout
   * coverage information.
   *
   * \sa determineDepth, determineNetDepth
   */
  void determineDepth();

  //! Calculate the depth for this point in the port-connection graph.
  /*!
   * If a depth is not already known for this PortConnectionData,
   * perform the depth-first traversal described in ::determineDepth.
   *
   * At the same time, perform a bottom-up propagation of inout
   * coverage information.
   *
   * \return The depth of this PortConnectionData element.
   * \sa determineDepth
   */
  SInt32 determineDepth(PortConnectionData * data);

  //! Determine depth for all actuals of this PortConnectionData.
  /*!
   * Nets receive the same depth as max(depth(port connections)) where
   * that net is used as an actual.
   *
   * Depth computation for PortConnectionData must occur before
   * attempting to determine the depth of its connected nets.
   *
   * \sa determineDepth
   */
  void determineNetDepth(PortConnectionData * data);

  //! Process each distinct, connected sub-graph defined by the set closure.
  /*!
   * \sa processCluster
   */
  void process(NUNetClosure * closure);

  //! Propagate port/net-splits across all members of the cluster.
  /*!
   * TBD: Describe the top-down/bottom-up traversal
   *
   * \param cluster The distinct, connected subgraph.
   * \param hier_ref_equiv_partition The hier-ref equivalence partitioning.
   */
  void processCluster(NUNetSet * cluster,
                      NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition);

  typedef UtVector<PortConnectionData*> OrderedClusterData;

  //! Order the port connections of this cluster based on the depth calculation.
  void orderClusterData(NUNetSet * cluster, OrderedClusterData * orderedData);

  //! Mark certain port connections of a cluster as forced.
  void markClusterForced(OrderedClusterData * orderedData);

  //! Propagate port/net-splits in a top-down fashion.
  /*!
   * \return true if any changes were made.
   */
  bool processClusterTopDown(OrderedClusterData * orderedData);

  //! Propagate port/net-splits in a bottom-up fashion.
  /*!
   * \return true if any changes were made.
   */
  bool processClusterBottomUp(OrderedClusterData * orderedData);

  //! Propagate port/net-splits across hierarchical references/resolution
  /*!
   * Transfer the splits for an individual hierarchical
   * reference/resolution to all members of its equivalence class.
   *
   * \param cluster Set of interconnected/related nets.
   * \param hier_ref_equiv_partition Equivalence classes of hierarchial references/resolutions.
   *
   * \sa transferHierRefSplitsFromActuals
   * \sa transferHierRefSplitsFromFormals
   */
  bool processClusterHierRefs(const NUNetSet * cluster,
                              NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition);

  //! Transfer hierarchical reference split information from port connection actuals.
  /*!
   * \param point Source for split information.
   * \param destinations Equivalence class members - split targets.
   */
  bool transferHierRefSplitsFromActuals(NUNet * point, NUNetSet * destinations);

  //! Transfer hierarchical reference split information from port connection formals.
  /*!
   * \param point Source for split information.
   * \param destinations Equivalence class members - split targets.
   */
  bool transferHierRefSplitsFromFormals(NUNet * point, NUNetSet * destinations);

  //! Apply splits to actual port connections.
  /*!
   * \param point_net_ref NetRef defining a net decomposition.

   * \param outer_loop Port connection information including NetRefs
   * which overlap with point_net_ref
   */
  bool transferHierRefSplitsToActuals(NUNetRefHdl & point_net_ref, 
                                      NUNetRefListLoop outer_loop,
                                      NUNetSet * destinations);

  //! Apply splits to formal port connections.
  bool transferHierRefSplitsToFormals(NUNetRefHdl & point_net_ref, 
                                      NUNetRefListLoop outer_loop,
                                      NUNetSet * destinations);

  //! Propagate port/net-splits across sibling connections.
  /*!
   * \return true if any changes were made.
   */
  bool processClusterLocal(const NUNetSet * cluster);

  //! Determine if any net in this set is considered an inout.
  /*!
   * \return true if any net in this set is considered an inout.
   * \sa isInout
   */
  bool containsInouts(const NUNetSet * cluster) const;

  //! Determine if this net should be considered an inout.
  /*!
   * Use information provided by the PortCoercion framework (combined
   * with the net's declared type) to determine if this net is
   * considered an inout.
   *
   * \return true if this net is considered an inout.
   */
  bool isInout(NUNet * net) const;

  //! Determine if this net should be considered an input.
  /*!
   * Use information provided by the PortCoercion framework (combined
   * with the net's declared type) to determine if this net is
   * considered an input.
   *
   * \return true if this net is considered an input.
   */
  bool isInput(NUNet * net) const;

  //! Determine if this net should be considered an output.
  /*!
   * Use information provided by the PortCoercion framework (combined
   * with the net's declared type) to determine if this net is
   * considered an output.
   *
   * \return true if this net is considered an output.
   */
  bool isOutput(NUNet * net) const;

  //! Does this net have a weak writer?
  /*!
   * Used to determine whether or not this net needs to be fully
   * connected for purposes of drive-resolution.
   */
  bool hasWeakWriter(NUNet * net) const;

  //! Perform all port-splitting and Nucleus updates.
  void split();

  //! Find all nets and net-parts which have been affected by port/net splits.
  /*!
   * \param nets The set of all nets affected by a splitting operation.
   * \param net_subdivisions The set of all net refs affected by a splitting operation.
   * \param hier_ref_equiv_partition Equivalence classes of hierarchial references/resolutions.
   *
   * \sa findModifiedNets
   */
  void findAllModifiedNets(NUNetSet * nets,
                           NUNetRefNetMap * net_subdivisions,
                           NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition);
    
  //! Remember that nets/net-parts referenced by this container have been affected by port/net splits.
  /*!
   * \param loop Iterator over decomposed net-refs.
   * \param nets The set of all decomposed nets.
   * \param net_subdivisions The set of all known decompositions.
   * \param hier_ref_equiv_partition Equivalence classes of hierarchial references/resolutions.
   */
  void findModifiedNets(NUNetRefListLoop loop,
                        NUNetSet * nets,
                        NUNetRefNetMap * net_subdivisions,
                        NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition);

  //! Remember one NetRef as a partial decomposition of its net.
  /*!
   * \param nets The set of all decomposed nets.
   * \param net_subdivisions The set of all known decompositions.
   */
  void addOneModifiedNetRef(const NUNetRefHdl & net_ref,
                            NUNetSet * nets,
                            NUNetRefNetMap * net_subdivisions);

  //! Fill in any holes in the decompositions.
  /*!
   * \param nets The set of all nets affected by a splitting operation.
   * \param net_subdivisions The set of all net refs affected by a splitting operation.
   */
  void complete(const NUNetSet * nets,
                NUNetRefNetMap * net_subdivisions);

  typedef UtMap<NUModule*,NUExprReplacementMap> ModuleToExprReplacementMap;

  //! If a net needs to be split, create replacements and remove the original from the port list.
  /*!
   * \param nets The set of all nets affected by a splitting operation.
   * \param net_subdivisions The set of all net refs affected by a splitting operation.
   * \param processed_nets The set of all nets which have been split.
   * \param expr_replacements Map from original -> concat of replacements
   * \param hier_ref_equiv_partition Equivalence classes of hierarchial references/resolutions.
   * 
   * \sa createOneReplacement
   */
  void createReplacements(NUNetSet             * nets,
                          NUNetRefNetMap       * net_subdivisions,
                          NUNetSet             * processed_nets,
                          ModuleToExprReplacementMap * all_expr_replacements,
                          NUNetUnionFind::EquivalencePartition * hier_ref_equiv_partition);

  //! Create the replacements for a modified net.
  /*!
   * \param net The net needing decomposition.
   * \param net_subdivisions The set of all net refs affected by a splitting operation (in).
   * \param net_subdivision_rewrites Map of net refs to replacement nets (out).
   * \param processed_nets The set of all nets which have been split.
   * \param expr_replacements Map from original -> concat of replacements
   * \param hier_ref_equiv_partition Equivalence classes of hierarchial references/resolutions.
   * 
   * \sa createReplacements
   */
  void createOneReplacement(NUNet * net,
                            NUNetRefNetMap       * net_subdivisions,
                            NUNetRefNetMap       * net_subdivision_rewrites,
                            NUNetSet             * processed_nets,
                            NUExprReplacementMap * expr_replacements);

  
  //! Create a replacement net from a net ref.
  /*!
   * If the net-ref represents 'io[2:1]', the replacement will be
   * named '$portsplit_io_2_1;1' 
   * 
   * If the original net is a hierarchial reference 'top.io[2:1]', the
   * replacement name will leave the path component unmodifed; the
   * replacement will be named 'top.$portsplit_io_2_1;1'.
   *
   * The net is not added to the module as a port or local; the caller
   * is expected to place the net in the proper location.
   */
  NUNet * createReplacementNet(const NUNetRefHdl & net_ref);

  //! Replace a net (either port or local) with a set of replacements.
  /*!
   * If the net is a port, the replacements become ports; the original
   * is demoted to a local (flags are updated).
   */
  void updateNet(NUNet * net, NUNetVector & replacements);

  //! Update the hierarchical references and resolutions for each split net.
  void updateHierRefs(NUNetRefNetMap * net_subdivisions);

  //! Remove the hierarchical references and resolutions for any decomposed nets.
  void disconnectHierRefs(const NUNetSet & nets);

  //! debug help: Trace that a net has been updated.
  void printNet(NUNet * net, const char * prefix=NULL) const;

  //! debug help: Print the name of every net in a cluster.
  void printCluster(const NUNetSet * cluster) const;

  //! Delete all PortConnectionData objects.
  void cleanupConnectionData();

  //! Delete all formal lists.
  void cleanupNetRefLists();

  //! Print all PortConnectionData objects
  void printConnectionData();

  //! String cache (External)
  AtomicCache * mStrCache;

  //! IODB (External)
  IODBNucleus * mIODB;

  //! Performance statistics (External)
  Stats * mStats;

  //! Command-line arguments (External)
  ArgProc * mArgs;

  //! Message context  (External)
  MsgContext * mMsgContext;

  //! Net-ref factory (External)
  NUNetRefFactory * mNetRefFactory;
  
  //! Source-locator factory (External)
  SourceLocatorFactory * mSourceLocatorFactory;

  //! Directed ports that will become inout (designated by port coercion).
  NUNetSet * mInouts;

  //! Ports that will become input (designated by port coercion).
  NUNetSet * mInputs;

  //! Ports that will become output (designated by port coercion).
  NUNetSet * mOutputs;

  //! Ports connected directly to primary inouts.
  NUNetSet * mInoutCover;

  //! Should we be verbose during processing?
  bool mVerbose;

  //! The design
  NUDesign * mDesign;

  //! Fold object used to simplify bit/part-selects.
  /*!
   * We expect that things like {a,b,c}[1] ==> b
   */
  Fold * mFold;

  //! PortAnalysis interface object.
  PortAnalysis * mPortAnalysis;

  //! Map from NUPortConnection to PortConnectionData
  PortConnectionDataMap         * mConnectionData;

  //! Map from actual NUNetRef to PortConnectionData
  NUNetRefPortConnectionDataMap * mActualToConnectionData;

  //! Map from formal NUNetRef to PortConnectionData
  NUNetRefPortConnectionDataMap * mFormalToConnectionData;

  //! Depth within the port-connection graph for all nets.
  NUNetDepthMap                 * mNetDepth;

  //! NUMappedNetRefLists for each formal; used by PortConnectionData.
  NUNetToNetRefListMap          * mNetFormalLists;

  //! Union-find of all hierarchically referenced/referring nets.
  NUNetUnionFind * mHierRefUnionFind;

  //! Keep track of splitting magnitude.
  PortSplittingStatistics mStatistics;
};


//! Class to selectively perform port lowering prior to any port-splitting.
/*!
 * If complicated port connections are encountered, lower the port.
 */
class SplittingLowerCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  //! Constructor
  /*!
   * \param netRefFactory The unelaborated netref factory.
   * \param args Argument processor.
   * \param str_cache The string cache.
   * \param msg_context The message context.
   * \param iodb IODB object.
   */
  SplittingLowerCallback(NUNetRefFactory * netRefFactory,
                         ArgProc * args,
                         AtomicCache * str_cache,
                         MsgContext * msg_context,
                         IODBNucleus * iodb);
  
  //! Destructor
  ~SplittingLowerCallback();
  
  //! By default, skip everything.
  Status operator()(Phase, NUBase *);
  
  //! Walk through the design.
  Status operator()(Phase, NUDesign *);
  
  //! Walk through modules so we can find module instances.
  /*!
   * Keep track of the current module as we recurse. The current
   * module is used when creating lowered temporaries.
   */
  Status operator()(Phase phase, NUModule * module);
  
  //! Walk through named declaration scopes so we can find module instances.
  Status operator()(Phase phase, NUNamedDeclarationScope *);

  //! Walk through module instances to find its port connections and the instantiated module.
  Status operator()(Phase /*phase*/, NUModuleInstance * /*instance*/);

  //! Process input port connections.
  /*!
   * Recursively consider lowering the expressions connected to an
   * input port connection.
   *
   * \sa maybeLowerActual
   */
  Status operator()(Phase phase, NUPortConnectionInput * connection);

  //! Process output port connections.
  /*!
   * Recursively consider lowering the lvalues connected to an output
   * port connection.
   *
   * \sa maybeLowerActual
   */
  Status operator()(Phase phase, NUPortConnectionOutput * connection);

  //! Process inout port connections.
  /*!
   * Recursively consider lowering the lvalues connected to an inout
   * port connection.
   *
   * \sa maybeLowerActual
   */
  Status operator()(Phase phase, NUPortConnectionBid * connection);

private:

  //! Recursively determine whether or not lowering is needed
  /*!
   * If lowering occurs, the provided 'actual' must be replaced with
   * the returned expression; it is now part of a continuous
   * assignment.
   *
   * \param actual Part of a port-connected expression.
   * \param bits   The number of bits the actual is expected to fill.
   * \param embedded True if this expression is part of the full port-connected expression.
   *
   * \return A new expression upon lowering; 'actual' otherwise.
   */
  NUExpr * maybeLowerActual(NUExpr * actual, UInt32 bits, bool embedded);

  //! Recursively determine whether or not lowering is needed
  /*!
   * If lowering occurs, the provided 'actual' must be replaced with
   * the returned lvalue; it is now part of a continuous assignment.
   *
   * \param actual Part of a port-connected lvalue.
   * \param bits   The number of bits the actual is expected to fill.
   *
   * \return A new lvalue upon lowering; 'actual' otherwise.
   */
  NULvalue * maybeLowerActual(NULvalue * actual, UInt32 bits);

  //! Lower an expression.
  NUExpr * lowerActual(NUExpr * actual, 
                       UInt32 bits, 
                       const SourceLocator & loc);

  NUExpr * lowerActual(NUExpr * actual, 
                       NUNet* formal_net, 
                       const SourceLocator & loc);
  //! Lower an lvalue.
  NULvalue * lowerActual(NULvalue * actual, 
                         UInt32 bits,
                         const SourceLocator & loc);

  NULvalue * lowerActual(NULvalue* actual, 
                         NUNet* formal_net,
                         const SourceLocator & loc);

  //! Stack used to track the current module
  UtStack<NUModule*> mModuleStack;

  //! Generic port-lowering interface
  LowerPortConnections * mLP;

  //! Fold/simplifications
  Fold * mFold;
};


#endif
