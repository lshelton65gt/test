// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implementation of non-blocking reorder to minimize temporaries.

  The reordering is based on building a dependency graph of statements
  in a statement list and then choosing an order which satisfies the
  dependencies but maximizes some other desirable properties that don't
  necessarily hold in the original statement order.  If all of the
  dependencies cannot be satisfied simultaneously, nets are converted
  to temporaries and the non-blocking dependencies for writes to
  those nets are dropped.

  Specifically, the dependency graph is partitioned into weakly-connected
  components and edges in the graph due to non-blocking statements are
  tracked.  Graph nodes with in-degree 0 are considered "ready" and
  are assigned a depth value.  Their edges are removed from the graph,
  making a new set of nodes ready, and those nodes are assigned to
  the next depth level, etc.  If there are nodes remaining in the graph,
  but none of them have in-degree 0, then that indicates that a
  temporary is required.  One is chosen from the set of remaining nodes
  with breakable edges, and processing resumes.
*/

#include "localflow/Reorder.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUTF.h"
#include "localflow/UD.h"
#include "util/CbuildMsgContext.h"
#include "util/GenericDigraph.h"
#include "util/GraphWCC.h"
#include "util/GraphDot.h"
#include "util/GraphQueue.h"
#include "util/GraphDepthWalker.h"
#include "compiler_driver/CarbonContext.h"  // needed for CRYPT()
#include "reduce/REUtil.h"

template <>
void NUNetRefGraphNodeMultiMap::helperTPrint(StmtGraph::Node* const & val, int /*indent*/) const {
  val->getData()->printVerilog(1,0,1);
}

//! The top-level reordering manager constructor
Reorder::Reorder(AtomicCache *str_cache,
                 NUNetRefFactory *netref_factory,
                 IODBNucleus *iodb,
                 ArgProc* args,
                 MsgContext *msg_ctx,
                 AllocAlias *alias_query,
                 bool expectAllBlocking,
                 bool groupConnected,
                 bool groupControlStatements,
                 bool dataOrderStatements) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mIODB(iodb),
  mArgs(args),
  mMsgContext(msg_ctx),
  mAliasQuery(alias_query),
  mExpectAllBlocking(expectAllBlocking),
  mGroupConnected(groupConnected),
  mGroupControlStatements(groupControlStatements),
  mDataOrderStatements(dataOrderStatements)
{
  mMustTempSet = new NUNetRefSet(mNetRefFactory);
  mVerbose = mArgs->getBoolValue(CRYPT("-verboseReorder"));
}

// Edge flag bits - represents all dependency types
typedef enum {
  NON_BLOCKING = 0x80,
  BLOCKING     = 0x00, // absence of flag
  USE_AFTER    = 0x40,
  DEF_AFTER    = 0x00, // absence of flag 
  USE          = 0x20,
  DEF          = 0x00, // absence of flag
  BROKEN       = 0x10
} EdgeFlag;

// Node flag bits 
typedef enum {
  REQUIRES_TEMP = 0x01
} NodeFlag;

//! Class records depth, etc. for each StmtGraph node
class GraphRecord
{
public:
  GraphRecord(UInt32 order, NUNetRefHdl& netRef) :
    mBreakableFanout(0), mDepth(0), mOrder(order), mNetRef(netRef), mData(0)
  {}

  UInt32      mBreakableFanout;  //!< out-degree due to non-blocking def
  SIntPtr     mDepth;            //!< assigned depth
  UInt32      mOrder;            //!< original position in statement list order
  NUNetRefHdl mNetRef;           //!< def net ref used during breaking
  SIntPtr     mData;             //!< Storage for graph queue
};

//! A class to write StmtGraphs in Dot format
class StmtGraphDotWriter : public GraphDotWriter
{
public:
  StmtGraphDotWriter(UtOStream& os) : GraphDotWriter(os) {}
  StmtGraphDotWriter(const UtString& filename) : GraphDotWriter(filename) {}

  //! Static all-in-one convenience function callable from the debugger
  static void writeGraph(StmtGraph* graph, const char* filename)
  {
    UtString fname(filename);
    StmtGraphDotWriter sgdw(fname);
    sgdw.output(graph);
  }

  static void writeGraph(StmtGraph* graph, UtOStream & os)
  {
    StmtGraphDotWriter sgdw(os);
    sgdw.output(graph);
  }

private:
  //! A node's label is the Verilog text of the statement
  virtual void writeLabel(Graph* graph, GraphNode* node)
  {
    StmtGraph* sg = (StmtGraph*) graph;
    StmtGraph::Node* n = sg->castNode(node);
    NUStmt* stmt = n->getData();
    UtString buf;
    stmt->compose(&buf,NULL);
    UtString buf2;
    StringUtil::escapeCString(buf.c_str(), &buf2, 'l');
    *mOut << "[ label = \"" << buf2 << "\" ]";
  }

  //! An edge's label describes the dependency reason encoded in the scratch field
  virtual void writeLabel(Graph* graph, GraphEdge* edge)
  {
    UtString buf;
    if (graph->anyFlagSet(edge, NON_BLOCKING))
      buf << "label = \"NONBLK\"";
    else
      buf << "label = \"BLOCK\"";
    if (graph->anyFlagSet(edge, USE_AFTER))
      buf << ", taillabel = \"U\"";
    else
      buf << ", taillabel = \"D\"";
    if (graph->anyFlagSet(edge, USE))
      buf << ", headlabel = \"U\"";
    else
      buf << ", headlabel = \"D\"";
    *mOut << "[ " << buf << " ]";
  }
};

//! Build a StmtGraph from an NUStmtList
/*! NOTE: The graph that is built has one node per statement in the list, and edges between
 *  the graph nodes represent ordering dependencies: if there is an edge a->b, then
 *  statement a must run *AFTER* statement b.  When the graph is constructed, edges
 *  are labeled (in the scratch fields) with a description of the reason for the
 *  dependency.  A crucial point to understand this algorithm is that the subgraph
 *  of blocking dependencies must be acyclic, by the nature of non-blocking semantics
 *  and the rules for constructing the graph.  It follows from this that we can 
 *  determine an execution order by breaking only the non-blocking dependencies.
 *  They are broken by creating temporary nets initialized at the beginning of the
 *  block and copied to the final value at the end of the block.
 *
 *  NOTE: if feedback set is given, then the above tome is potentially incorrect;
 *  there can potentially be cycles in the graph.
 */
StmtGraph* Reorder::createStmtGraph(const NUStmtList* stmts,
                                    NUNetRefSet *feedback,
                                    ReduceUtility *re_util)
{
  StmtGraph* graph = new StmtGraph;

  // Map of currently-active blocking defs to their graph nodes.
  AliasBucketNetRefStmtGraphMap block_def_map(mAliasQuery, mNetRefFactory);

  // Map of currently-active non-blocking defs to their graph nodes.
  AliasBucketNetRefStmtGraphMap non_block_def_map(mAliasQuery, mNetRefFactory);
  
  // Map of every def which has a non-blocking def to the nodes which make up those defs.
  NUNetRefGraphNodeMultiMap all_non_block_def_map(mNetRefFactory);

  // Map of uses which are not yet succeeded by a def of the net.
  // We keep these around so we can get WAR control dependencies.
  // Note that nets are removed from this map once they have a write, since
  // then the dependency is transitively covered through WAW.
  AliasBucketNetRefStmtGraphMap live_use_map(mAliasQuery, mNetRefFactory);

  // Map of nets in the feedback set (if given) to their initial uses.
  // This will be used at the end to populate edges from the final defs
  // to these initial uses.
  NUNetRefGraphNodeMultiMap feedback_use_map(mNetRefFactory);

  // Uses in the feedback set which have not been killed.  For every
  // statement, if a use is in this set, it will be added to the
  // feedback_use_map.
  NUNetRefSet open_feedback_uses(mNetRefFactory);
  if (feedback) {
    open_feedback_uses = *feedback;

    // Should only be doing feedback when we only have non-blocking assigns
    // (after the initial reorder pass).
    INFO_ASSERT(mExpectAllBlocking, "A data flow cycle occured in late reordering, after all non-blocking assigns have been eliminated");
  }
  
  // Map of every stmt to its graph node.
  typedef UtMap<NUStmt*, StmtGraph::Node*> StmtNodeMap;
  StmtNodeMap stmt_node_map;

  // Build the graph nodes and perform some checks on the stmt list
  UInt32 count = 0;
  for (NUStmtList::const_iterator iter = stmts->begin();
       iter != stmts->end();
       ++iter)
  {
    NUStmt *cur_stmt = *iter;
    
    // Make sure for backend processing, there are no non-blocking assignments.
    NU_ASSERT( !mExpectAllBlocking || (cur_stmt->getType() != eNUNonBlockingAssign), cur_stmt );
    
    // This stmt's node.
    StmtGraph::Node* stmt_node = new StmtGraph::Node(cur_stmt);
    graph->addNode(stmt_node);
    stmt_node_map[cur_stmt] = stmt_node;

    // Setup a GraphRecord for this node
    NUNetRefSet defs(mNetRefFactory);
    cur_stmt->getBlockingDefs(&defs);
    cur_stmt->getNonBlockingDefs(&defs);

    // If this stmt has multiple defs, we want to choose the same one to represent
    // this node each time, so we do a stableChoice() call on the net ref set.
    NUNetRefSet::const_iterator n = defs.stableChoice();
    NUNetRefHdl netRef = (n == defs.end()) ? mNetRefFactory->createEmptyNetRef() : *n;
    graph->setScratch(stmt_node, (UIntPtr) new GraphRecord(count++, netRef));    
  }

  // Add edges to the graph for all ordering dependencies
  for (NUStmtList::const_iterator iter = stmts->begin();
       iter != stmts->end();
       ++iter)
  {
    NUStmt *cur_stmt = *iter;
    StmtGraph::Node* stmt_node = stmt_node_map[cur_stmt];

    NUNetRefSet defs(mNetRefFactory);
    cur_stmt->getBlockingDefs(&defs);
    cur_stmt->getNonBlockingDefs(&defs);
    
    NUNetRefSet uses(mNetRefFactory);
    cur_stmt->getBlockingUses(&uses);
    cur_stmt->getNonBlockingUses(&uses);
    
    // All uses depend on the currently live blocking defs (RAW data dependency).
    addDependencies(graph, stmt_node, BLOCKING | USE_AFTER | DEF, uses,
                    &block_def_map, &live_use_map);
    
    // All defs must come after current live defs (WAW control dependency).
    addDependencies(graph, stmt_node, BLOCKING | DEF_AFTER | DEF, defs,
                    &block_def_map, NULL);
    addDependencies(graph, stmt_node, NON_BLOCKING | DEF_AFTER | DEF, defs,
                    &non_block_def_map, NULL);
                    
    // And, they must come after the current live reads (WAR control dependency).
    addDependencies(graph, stmt_node, BLOCKING | DEF_AFTER | USE, defs,
                    &live_use_map, NULL);
    defs.clear();
      
    // Place this stmt as currently-active defs.  If they are killing defs, then clear
    // out the other active defs.
    NUNetRefSet kills(mNetRefFactory);
    cur_stmt->getBlockingDefs(&defs);
    cur_stmt->getBlockingKills(&kills);
    processDefsAndKills(stmt_node, defs, kills,
                        &block_def_map, NULL);

    feedbackMaintenance(stmt_node, uses, kills, open_feedback_uses, feedback_use_map);

    uses.clear();
    defs.clear();
    kills.clear();

    cur_stmt->getNonBlockingDefs(&defs);
    cur_stmt->getNonBlockingKills(&kills);
    processDefsAndKills(stmt_node, defs, kills,
                        &non_block_def_map, &all_non_block_def_map);
  }

  // Object to assist with overlap detection, which is possible-alias aware.
  NetRefOverlapsQuery query(mAliasQuery);

  // Iterate through the stmts once more, for every statement which uses a net
  // which has a non-blocking def, create a dependency between the non-blocking
  // defs and the use (non-blocking control dependency).
  // The def should come after the use.  These are the breakable dependencies.
  for (StmtNodeMap::iterator iter = stmt_node_map.begin();
       iter != stmt_node_map.end();
       ++iter) {
    NUStmt *cur_stmt = iter->first;
    StmtGraph::Node* cur_node = iter->second;
    
    NUNetRefSet uses(mNetRefFactory);
    cur_stmt->getBlockingUses(&uses);
    cur_stmt->getNonBlockingUses(&uses);
    
    for (NUNetRefGraphNodeMultiMap::MapLoop loop = all_non_block_def_map.loop();
         not loop.atEnd();
         ++loop) {
      NUNetRefHdl def_net_ref = (*loop).first;
      NUNet *def_net = def_net_ref->getNet();
      StmtGraph::Node *def_node = (*loop).second;

      bool need_dependency = false;
      if (uses.find(def_net_ref, &NUNetRef::overlapsSameNet) != uses.end()) {
        need_dependency = true;
      } else {
        // Detect possible elaborated alias between the use nets and
        // the def net.  For any, add an arc.
        for (NUNetRefSet::iterator iter = uses.begin();
             not need_dependency and (iter != uses.end());
             ++iter) {
          NUNetRefHdl use_net_ref = *iter;

          if (not use_net_ref->empty()) {
            // The NetRefOverlapsQuery interface is a little bit cumbersome here,
            // but using it so that we don't replicate the logic.
            if (query(def_net, *use_net_ref) and query(*def_net_ref, *use_net_ref)) {
              need_dependency = true;
            }
          }
        }
      }

      // For loops need self-arcs because they model cyclic execution.
      if (need_dependency and
          ((def_node->getData()!=cur_node->getData()) or
           (def_node->getData()->getType()==eNUFor))) {
          StmtGraph::Edge* edge = new StmtGraph::Edge(cur_node, NULL);
          def_node->addEdge(edge);
          graph->setFlags(edge, NON_BLOCKING | DEF_AFTER | USE);
      }
    }
  }

  createFeedbackEdges(feedback_use_map, block_def_map, graph, re_util);

  removePessimisticNetRefEdges(graph);

  return graph;
}


/*!
 * Add feedback edges for nets which are keys of the feedback_use_map;
 * make their initial uses depend on their final defs.
 *
 * Also break any anti-dependence edge between the initial use and the
 * final def.  For example, if we have:
 *  b = a; // stmt1
 *  a = d; // stmt2
 * There will be an arc from stmt1 to stmt2 because uses must preceed
 * subsequent defs.  But, for the feedback nets, that def is the reaching def.
 */
void Reorder::createFeedbackEdges(NUNetRefGraphNodeMultiMap &feedback_use_map,
                                  AliasBucketNetRefStmtGraphMap &block_def_map,
                                  StmtGraph *graph,
                                  ReduceUtility *re_util)
{
  for (NUNetRefGraphNodeMultiMap::MapLoop loop = feedback_use_map.loop();
       not loop.atEnd();
       ++loop) {
    NUNetRefHdl use_ref = (*loop).first;
    StmtGraph::Node *use_node = (*loop).second;

    NUNetRefGraphNodeMultiMap* net_ref_map = block_def_map.getNetRefMap(use_ref);
    for (NUNetRefGraphNodeMultiMap::CondLoop overlap_loop = net_ref_map->loop(use_ref, &NUNetRef::overlapsSameNet);
         not overlap_loop.atEnd();
         ++overlap_loop) {
      StmtGraph::Node *def_node = (*overlap_loop).second;

      // Make sure this statement only defs nets which have one continuous driver.
      // This may be conservative in some cases.
      bool ok_stmt = true;
      NUStmt *stmt = def_node->getData();
      NUNetRefSet stmt_defs(mNetRefFactory);
      stmt->getBlockingDefs(&stmt_defs);
      for (NUNetRefSet::iterator def_iter = stmt_defs.begin();
           (def_iter != stmt_defs.end()) and ok_stmt;
           ++def_iter) {
        if (re_util->getDefCount(*def_iter) > 1) {
          ok_stmt = false;
        }
      }
      if (not ok_stmt) {
        continue;
      }

      StmtGraph::Edge *edge = new StmtGraph::Edge(def_node, 0);
      use_node->addEdge(edge);
      graph->setFlags(edge, BLOCKING | USE_AFTER | DEF);

      // Search for anti-dependence edges and remove it.  There may be several.
      UtSet<StmtGraph::Edge*> remove_set;
      for (Iter<GraphEdge*> edge_iter = graph->edges(def_node);
           not edge_iter.atEnd();
           ++edge_iter) {
        StmtGraph::Edge *this_edge = dynamic_cast<StmtGraph::Edge*>(*edge_iter);
        if ((graph->endPointOf(this_edge) == use_node) and graph->allFlagsSet(this_edge, BLOCKING | DEF_AFTER | USE)) {
          remove_set.insert(this_edge);
        }
      }
      for (UtSet<StmtGraph::Edge*>::iterator remove_iter = remove_set.begin();
           remove_iter != remove_set.end();
           ++remove_iter) {
        def_node->removeEdge(*remove_iter);
        delete *remove_iter;
      }
    }
  }
}


/*!
 * Feedback processing:
 *  1 - if this statement uses anything in the open feedback set,
 *      add this statement to the feedback_use_map
 *  2 - remove anything this statement kills from the open feedback set
 */
void Reorder::feedbackMaintenance(StmtGraph::Node* node,
                                  const NUNetRefSet &uses,
                                  const NUNetRefSet &kills,
                                  NUNetRefSet &open_feedback_uses,
                                  NUNetRefGraphNodeMultiMap &feedback_use_map)
{
    NUNetRefSet cur_feedback_uses(mNetRefFactory);
    NUNetRefSet::set_intersection(uses, open_feedback_uses, cur_feedback_uses);
    for (NUNetRefSet::iterator use_iter = cur_feedback_uses.begin();
         use_iter != cur_feedback_uses.end();
         ++use_iter) {
      feedback_use_map.insert(*use_iter, node);
    }

    NUNetRefSet::set_subtract(kills, open_feedback_uses);
}


//! Add dependencies to the graph for current net refs that intersect prior net refs
void Reorder::addDependencies(StmtGraph* graph,
                              StmtGraph::Node* stmt_node,
                              UInt32 edge_type,
                              NUNetRefSet& current_net_refs,
                              AliasBucketNetRefStmtGraphMap* prior_net_ref_map,
                              AliasBucketNetRefStmtGraphMap* current_net_ref_map)
{
  NetRefOverlapsQuery query(mAliasQuery);

  // Go through all net refs in the current set, adding edges to overlapping
  // or possible elaborated aliases of prior net refs.
  for (NUNetRefSetIter cur_iter = current_net_refs.begin();
       cur_iter != current_net_refs.end();
       ++cur_iter) {
    NUNetRefHdl cur_net_ref = *cur_iter;
    NUNet* cur_net = cur_net_ref->getNet();

    // cur_net may be NULL if there is a constant use (empty net ref)
    if (not cur_net) {
      continue;
    }

    // Iterate over the possibly-aliased prior netrefs.
    NUNetRefGraphNodeMultiMap* net_ref_map = prior_net_ref_map->getNetRefMap(cur_net_ref);
    for (NUNetRefGraphNodeMultiMap::CondMapLoop prior_loop = net_ref_map->loop(cur_net_ref, &query);
         not prior_loop.atEnd();
         ++prior_loop) {
      StmtGraph::Node* depend_node = (*prior_loop).second;
      if (depend_node != stmt_node) {
        StmtGraph::Edge* edge = new StmtGraph::Edge(depend_node, NULL);
        stmt_node->addEdge(edge);
        graph->setFlags(edge, edge_type);
      }
    }

    // Store the current net ref for future use in a prior net ref map
    if (current_net_ref_map) {
      current_net_ref_map->insert(cur_net_ref, stmt_node);
    }
  }
}

//! Update the net ref sets to reflect the defs and kills 
void Reorder::processDefsAndKills(StmtGraph::Node* stmtNode, NUNetRefSet& defs, NUNetRefSet& kills,
                                  AliasBucketNetRefStmtGraphMap* defNetRefMap,
                                  NUNetRefGraphNodeMultiMap* secondaryNetRefMap)
{
  for (NUNetRefSetIter defIter = defs.begin();
       defIter != defs.end();
       ++defIter) {
    NUNetRefHdl defNetRef = *defIter;
      
    NUNetRefSetIter killIter = kills.find(defNetRef, &NUNetRef::overlapsSameNet);
    if (killIter != kills.end()) {
      NUNetRefHdl killNetRef = *killIter;
      NUNetRefHdl intersectNetRef = mNetRefFactory->intersect(defNetRef, killNetRef);
      NU_ASSERT2(not intersectNetRef->empty(),defNetRef,killNetRef);
      defNetRefMap->erase(intersectNetRef);
    }
    defNetRefMap->insert(defNetRef, stmtNode);
    if (secondaryNetRefMap)
      secondaryNetRefMap->insert(defNetRef, stmtNode);
  }
}

//! Remove edges due to net ref pessimism
/*! Net refs, such as ones used for memories, may make it look
 *  look like there is a dependency where there is not.  This method
 *  removes edges in the dependency graph for which it can prove that
 *  there is no true dependency, despite the net ref overlap. 
 */
void Reorder::removePessimisticNetRefEdges(StmtGraph* graph)
{
  for (Iter<GraphNode*> n = graph->nodes(); !n.atEnd(); ++n)
  {
    GraphNode* node = *n;
    StmtGraph::Node* from = graph->castNode(node);
    UtList<GraphEdge*> badEdges;
    for (Iter<GraphEdge*> e = graph->edges(from); !e.atEnd(); ++e)
    {
      GraphEdge* edge = *e;
      StmtGraph::Node* to = graph->castNode(graph->endPointOf(edge));
      if (!graph->anyFlagSet(edge, USE_AFTER | USE))
      {
        // this is a def->def dependency.  we can look at the lvalue
        // (if they are assignments) to check for bit-level separation
        const NUAssign* assign_a = dynamic_cast<const NUAssign*>(from->getData());
        const NUAssign* assign_b = dynamic_cast<const NUAssign*>(to->getData());
        if (assign_a && assign_b)
        {
          const NULvalue* lv_a = assign_a->getLvalue();
          const NULvalue* lv_b = assign_b->getLvalue();
          const NUVarselLvalue* varsel_a = dynamic_cast<const NUVarselLvalue*>(lv_a);
          const NUVarselLvalue* varsel_b = dynamic_cast<const NUVarselLvalue*>(lv_b);
          if (varsel_a && varsel_b &&
              (*(varsel_a->getLvalue()) == *(varsel_b->getLvalue())) &&
              varsel_a->isConstIndex() && varsel_b->isConstIndex())
          {
            const ConstantRange* range_a = varsel_a->getRange();
            const ConstantRange* range_b = varsel_b->getRange();
            if (!range_a->overlaps(*range_b))
            {
              // this is a pessimistic dependency -- remove the edge
              badEdges.push_back(edge);
            }
          }
        }
      }
    }
    // remove any bad edges we found for this node
    for (UtList<GraphEdge*>::iterator e = badEdges.begin(); e != badEdges.end(); ++e)
    {
      StmtGraph::Edge* edge = graph->castEdge(*e);
      from->removeEdge(edge);
      delete edge;
    }
  }
}

//! A comparator base class used for sorting StmtGraph::Node*s
class StmtGraphNodeCmp
{
public:
  virtual ~StmtGraphNodeCmp() {}

  virtual bool operator()(const StmtGraph::Node* n1, const StmtGraph::Node* n2) const
  {
    int cmp = carbonPtrCompare(n1,n2);
    return (cmp < 0);
  }
};

//! A set of StmtGraph::Node*s
template <class _Cmp>
class StmtGraphNodeSet : public UtSet<StmtGraph::Node*,_Cmp>
{
public:
  StmtGraphNodeSet(_Cmp& cmp) : UtSet<StmtGraph::Node*,_Cmp>(cmp) {}
};

/*! A comparator class used for ordering StmtGraph::Node*s in the order their
 *  statements originally appeared in the list.
 */
class FileOrderCmp : public UtGraphQueue::NodeCmp
{
public:
  FileOrderCmp(StmtGraph* graph) : mGraph(graph) {}    
  bool operator()(const GraphNode* n1, const GraphNode* n2) const
  {
    GraphRecord* rec1 = (GraphRecord*) mGraph->getScratch(n1);
    GraphRecord* rec2 = (GraphRecord*) mGraph->getScratch(n2);

    return (rec1->mOrder < rec2->mOrder);
  }
private:
  StmtGraph* mGraph;
}; // class FileOrderCmp

/*! A comparator class used to order StmtGraph::Node*s in an advantageous order.
 *  We must respect the depth ordering, but within each depth we try to group
 *  defs of the same net together to help optimizations.
 */
class ReorderCmp : public UtGraphQueue::NodeCmp
{
public:
  ReorderCmp(StmtGraph* graph) : mGraph(graph) {}
  bool operator()(const GraphNode* n1, const GraphNode* n2) const
  {
    GraphRecord* rec1 = (GraphRecord*) mGraph->getScratch(n1);
    GraphRecord* rec2 = (GraphRecord*) mGraph->getScratch(n2);
    
    // Criteria: Calculated depth (largest-to-smallest)
    if (rec1->mDepth > rec2->mDepth)
      return true;
    else if (rec1->mDepth < rec2->mDepth)
      return false;
    
    // Criteria: The name of the defnet and bits referenced.
    // Bits referenced is needed by pack assigns which relies on the
    // sorted assignment order to get predictable results.
    if (!rec1->mNetRef->empty() && !rec2->mNetRef->empty())
    {
      int cmp = NUNetRef::compare(*(rec1->mNetRef), *(rec2->mNetRef), true);
      if (cmp != 0)
        return cmp < 0;

      // if net refs compare, check for assignments to bit-selects of the same
      // lvalue.  these can have the exact same defs, but we want to order them
      // based on the selected bit as well.
      const StmtGraph::Node* a = mGraph->castNode(n1);
      const StmtGraph::Node* b = mGraph->castNode(n2);
      const NUAssign* assign_a = dynamic_cast<const NUAssign*>(a->getData());
      const NUAssign* assign_b = dynamic_cast<const NUAssign*>(b->getData());
      if (assign_a && assign_b)
      {
        const NULvalue* lv_a = assign_a->getLvalue();
        const NULvalue* lv_b = assign_b->getLvalue();
        const NUVarselLvalue* varsel_a = dynamic_cast<const NUVarselLvalue*>(lv_a);
        const NUVarselLvalue* varsel_b = dynamic_cast<const NUVarselLvalue*>(lv_b);
        if (varsel_a && varsel_b &&
            (*(varsel_a->getLvalue()) == *(varsel_b->getLvalue())) &&
            varsel_a->isConstIndex() && varsel_b->isConstIndex())
        {
          const ConstantRange* range_a = varsel_a->getRange();
          const ConstantRange* range_b = varsel_b->getRange();
          cmp = (varsel_a->getConstIndex() + range_a->getLsb()) -
                (varsel_b->getConstIndex() + range_b->getLsb());
          if (cmp != 0)
            return cmp < 0;
        }
      }    
    }
    else if (!rec1->mNetRef->empty())
      return true;
    else if (!rec2->mNetRef->empty())
      return false;    
    
    // Criteria: Original statement order.
    return (rec1->mOrder < rec2->mOrder);
  }
private:
  StmtGraph* mGraph;
}; // class ReorderCmp

/* NOTE: I think this algorithm can be improved if we:
 *       1) change the breakable map to map temp'ed net refs
 *          to sets of (node,egde) pairs that can be broken
 *          by temping that net ref
 *       2) modify breakCycle() to pick the most desirable net ref
 *          (most breakable edges, preferring simple assignments,
 *           and avoiding memories).
 *       3) change the requireTemp(), etc. to work with a net ref
 *          instead of a single statement.
 */

/*! The GraphQueue class allows us to order and break the StmtGraph
 *  to produce a new, re-ordered NUStmtList.
 *
 *  It computes fanin and fanout counts for each node.  Nodes with
 *  no fanout are assigned the maximum depth -- this is an optimization
 *  to keep them together instead of scattering them intermixed with
 *  the other nodes based on their fanin.  All other nodes are removed
 *  from the graph and assigned a depth based on when their fanin reaches
 *  0.  Cyclic ordering constraints are handled by removing edges selected
 *  from the set of breakable edges until some node's fanin reaches 0.
 */
class GraphQueue : public UtGraphQueue
{
public:
  //! Constructor
  GraphQueue() : 
    mGraph(NULL), 
    mFileOrder(NULL), 
    mEdgeOrder(NULL),
    mBreakable(NULL),
    mGroupControlStatements(false)
  {
  }

  //! Initialize the queue to sequence a new graph
  void init(StmtGraph* graph, bool group_control_statements)
  {
    clear();

    // initialize for the new graph
    mGraph        = graph;
    mFileOrder    = new FileOrderCmp(mGraph); 
    mEdgeOrder    = new UtGraphQueue::EdgeCmp;
    mBreakable    = new StmtGraphNodeSet<FileOrderCmp>(*mFileOrder);
    mGroupControlStatements = group_control_statements;
    UtGraphQueue::init(mGraph, mFileOrder, mEdgeOrder);
  }

  //! Get the list of statements that require a temp
  void getTmpStmts(NUStmtList* stmtList)
  {
    for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop) {
      StmtGraph::Node* node = mGraph->castNode(*loop);
      if (mGraph->anyFlagSet(node, REQUIRES_TEMP)) {
        stmtList->push_back(node->getData());
      }
    }
  }

  //! Get the reordered NUStmtList sorted by depth, etc.
  void getList(NUStmtList* stmtList)
  {
    // build a vector of all graph nodes, then sort and convert to a list
    ReorderCmp reorder_cmp(mGraph);
    UtVector<StmtGraph::Node*> allNodes;
    for (Iter<GraphNode*> loop = mGraph->nodes(); !loop.atEnd(); ++loop) {
      StmtGraph::Node* node = mGraph->castNode(*loop);
      allNodes.push_back(node);
    }
    std::sort(allNodes.begin(), allNodes.end(), reorder_cmp);
    INFO_ASSERT(allNodes.size() == mGraph->size(), "List and graph size differ!");
    for (UtVector<StmtGraph::Node*>::iterator p = allNodes.begin();
         p != allNodes.end(); ++p) {
      StmtGraph::Node* node = *p;
      stmtList->push_back(node->getData());
    }
  }

  void clear()
  {
    mGraph = NULL;
    // delete the StmtGraphNodeSets
    if (mBreakable != NULL) {
      delete mBreakable;
      mBreakable = NULL;
    }
    if (mFileOrder != NULL) {
      delete mFileOrder;
      mFileOrder = NULL;
    }
    if (mEdgeOrder != NULL) {
      delete mEdgeOrder;
      mEdgeOrder = NULL;
    }
  }

  //! destructor - release resources
  ~GraphQueue()
  {
    clear();
  }

protected:
  void deferReadyNodes(const UtArray<GraphNode*> & ready, UtSet<GraphNode*> * deferred);

  //! Overloaded function to store data for the base class
  void putNodeData(GraphNode* node, SIntPtr data)
  {
    GraphRecord* rec = (GraphRecord*) mGraph->getScratch(node);
    rec->mData = data;
  }

  //! Overloaded function to get data for the base class
  SIntPtr getNodeData(GraphNode* node) const
  {
    GraphRecord* rec = (GraphRecord*) mGraph->getScratch(node);
    return rec->mData;
  }

  //! Overloaded function to schedule a node at a given depth
  void scheduleNode(GraphNode* node, SInt32 depth)
  {
    // Get the data for this node and mark its depth
    GraphRecord* rec = (GraphRecord*) mGraph->getScratch(node);
    rec->mDepth = depth;

    // This node should not be breakable anymore
    StmtGraph::Node* stmtNode = mGraph->castNode(node);
    mBreakable->erase(stmtNode);
  }

  //! Overload function to deal with cycles
  void breakCycle()
  {
    StmtGraph::Node* breaker = NULL;
    GraphRecord* brec = NULL;
    INFO_ASSERT(!mBreakable->empty(), "A cycle remains after all non-blocking assigns are converted");
    // choose a non-blocking statement to break the cycle
    for (StmtGraphNodeSet<FileOrderCmp>::iterator p = mBreakable->begin();
         p != mBreakable->end(); ++p) {
      StmtGraph::Node* node = *p;
      if (breaker == NULL)
      {
        breaker = node;
        brec = (GraphRecord*) mGraph->getScratch(breaker);
      }
      else
      {
        GraphRecord* nrec = (GraphRecord*) mGraph->getScratch(node);
        if (nrec->mBreakableFanout > brec->mBreakableFanout)
        {
          breaker = node;
          brec = nrec;
        }
      }
    }
    INFO_ASSERT(brec->mBreakableFanout > 0, "Chosen break node has no breakable fanout");
    INFO_ASSERT(brec->mDepth == 0, "Chosen break node has already been scheduled");

    // Now we have chosen a breaker, break the non-blocking edges
    for (Iter<GraphEdge*> i = mGraph->edges(breaker); !i.atEnd(); ++i) {
      GraphEdge* edge = *i;
      if (mGraph->allFlagsSet(edge, NON_BLOCKING | USE)) {
        breakEdge(edge);
      }
    }

    // Update data on the broken node and mark the node as requiring a
    // temp.
    brec->mBreakableFanout = 0;
    mBreakable->erase(breaker); // no longer breakable
    mGraph->setFlags(breaker, REQUIRES_TEMP);
  }

  //! Overloaded to initialize the breakable nodes
  bool initGraphEdge(GraphNode* from, GraphEdge* edge, GraphNode*)
  {
    if (mGraph->allFlagsSet(edge, NON_BLOCKING | USE)) {
      GraphRecord* rec = (GraphRecord*) mGraph->getScratch(from);
      rec->mBreakableFanout += 1;
      if (rec->mBreakableFanout == 1) {
        // First time, remember this breakable fanout node
        StmtGraph::Node* stmtNode = mGraph->castNode(from);
        mBreakable->insert(stmtNode);
      }
    }
    return true;
  }

private:
  StmtGraph*                      mGraph;        //!< The graph we are ordering
  FileOrderCmp*                   mFileOrder;    //!< The comparison instance for file order
  UtGraphQueue::EdgeCmp*          mEdgeOrder;    //!< The comparison instance for edges
  StmtGraphNodeSet<FileOrderCmp>* mBreakable;    //!< The set of graph nodes with breakable edges
  bool                  mGroupControlStatements; //!< Should the scheduling attempt to group control statements?
};


void GraphQueue::deferReadyNodes(const UtArray<GraphNode*> & ready, UtSet<GraphNode*> * deferred)
{
  if (not mGroupControlStatements) {
    return;
  }
  
  typedef UtSet<GraphNode*> GraphNodeSet;
  GraphNodeSet schedulable;
  typedef UtMap<NUNet*,GraphNodeSet,NUNetCmp> NetGraphNodeSetMap;
  NetGraphNodeSetMap control_to_nodes;

  for (UtArray<GraphNode*>::const_iterator iter=ready.begin(); 
       iter!=ready.end();
       ++iter) {
    StmtGraph::Node* node = mGraph->castNode(*iter);

    const NUStmt* stmt = node->getData();

    switch(stmt->getType()) {
    case eNUIf:
    case eNUCase:
    {
      NUExpr * control = NULL;
      if (stmt->getType()==eNUIf) {
        const NUIf * if_stmt = dynamic_cast<const NUIf*>(stmt);
        control = if_stmt->getCond();
      } else {
        const NUCase * case_stmt = dynamic_cast<const NUCase*>(stmt);
        control = case_stmt->getSelect();
      }

      // if this is a control statement, associate it with its uses.
      NUNetSet uses;
      control->getUses(&uses);
      if (uses.empty()) {
        // Control statement without uses. Something like case(1'b1).
        // Make it immediately schedulable. We could look at the
        // branch conditions.
        schedulable.insert(node);
      } else {
        for (NUNetSet::iterator niter=uses.begin();
             niter!=uses.end();
             ++niter) {
          NUNet * use = *niter;
          control_to_nodes[use].insert(node);
        }
      }
      break;
    }
    default:
    {
      // if this is a non-control statement, 
      schedulable.insert(node);
      break;
    }
    }
  }

  // if there are no schedulable nodes, find the net with the most
  // associated control statements; schedule that group of statements.
  if (schedulable.empty()) {
    UInt32  max_control_size = 0;
    NUNet * max_control_net = NULL;
    for (NetGraphNodeSetMap::iterator iter=control_to_nodes.begin();
         iter!=control_to_nodes.end();
         ++iter) {
      NUNet * control_net = iter->first;
      GraphNodeSet & nodes = iter->second;
      UInt32 control_size = nodes.size();
      if (control_size > max_control_size) {
        max_control_size = control_size;
        max_control_net  = control_net;
      }
    }
    INFO_ASSERT(max_control_net, "Could not determine high-control net.");
    schedulable = control_to_nodes[max_control_net];
  }

  INFO_ASSERT(not schedulable.empty(), "Could not determine schedulable nodes.");
  for (UtArray<GraphNode*>::const_iterator iter=ready.begin(); 
       iter!=ready.end();
       ++iter) {
    GraphNode * node = *iter;
    if (schedulable.find(node)==schedulable.end()) {
      deferred->insert(node);
    }
  }
}


//! A comparator class used to order weakly-connected components
class ComponentOrder
{
public:
  ComponentOrder(StmtGraph* graph, const NUStmtList* stmts)
    : mGraph(graph), mOrderMap(), mMinMap()
  {
    UInt32 count = 0;
    for (NUStmtList::const_iterator s = stmts->begin(); s != stmts->end(); ++s, ++count)
    {
      NUStmt* stmt = *s;
      mOrderMap[stmt] = count;
    }
  }

  //! order by size, then the original order of earliest statement in each component
  bool operator()(const GraphWCC::Component* c1, const GraphWCC::Component* c2)
  {
    if (c1 == c2)
      return false;

    // order by size first
    if (c1->size() > c2->size())
      return true;
    else if (c1->size() < c2->size())
      return false;

    // order by statement order of earliest statement
    return (computeMin(c1) < computeMin(c2));
  }
private:
  //! Compute the earliest position of any statement in the given component
  UInt32 computeMin(const GraphWCC::Component* component)
  {
    UtMap<const GraphWCC::Component*,UInt32>::iterator pos = mMinMap.find(component);
    if (pos != mMinMap.end())
      return pos->second; // use the cached data

    // Not in the cache, compute the earliest statement position
    UInt32 smallest = UtUINT32_MAX;
    for (GraphWCC::Component::NodeLoop loop = component->loopNodes(); !loop.atEnd(); ++loop)
    {
      StmtGraph::Node* node = mGraph->castNode(*loop);
      NUStmt* stmt = node->getData();
      smallest = std::min(mOrderMap[stmt], smallest);
    }
    mMinMap[component] = smallest; // store the result in the cache
    return smallest;
  }
private:
  StmtGraph*                               mGraph;    //!< The graph whose components we are ordering
  UtMap<NUStmt*,UInt32>                    mOrderMap; //!< A map from statement to original position
  UtMap<const GraphWCC::Component*,UInt32> mMinMap;   //!< A cache of earliest statement for each component
};


Reorder::~Reorder()
{
  delete mMustTempSet;
}

//! Record that a statement must be re-written with temporaries
void Reorder::requireTemp(NUStmt* stmt)
{
  // Add this stmt's defs to the must-temp set
  NUNetRefSet s(mNetRefFactory);
  stmt->getNonBlockingDefs(&s);
  for (NUNetRefSet::iterator p = s.begin(); p != s.end(); ++p)
      mMustTempSet->insert(*p);
}

//! Return a reordered statement list
void Reorder::layoutStmtList(const NUStmtList * unordered_stmts, 
                             NUStmtList * ordered_stmts,
                             StmtGraph * existing_graph)
{
  // Build a StmtGraph from the stmts
  StmtGraph* graph;
  if (existing_graph) {
    graph = existing_graph;
  } else {
    graph = createStmtGraph(unordered_stmts);
  }

  GraphWCC wcc;

  if (mGroupConnected)
  {
    // Partition the graph into weakly-connected components
    wcc.compute(graph);
  }

  if (mGroupConnected && (wcc.numComponents() > 1))
  {
    // Sort the components by size and original statement order
    UtVector<GraphWCC::Component*> components;
    for (GraphWCC::ComponentLoop loop = wcc.loopComponents(); !loop.atEnd(); ++loop)
    {
      GraphWCC::Component* component = *loop;
      components.push_back(component);
    }
    ComponentOrder compOrder(graph, unordered_stmts);
    std::sort(components.begin(), components.end(), compOrder);

    // Reorder the statements within each component
    for (UtVector<GraphWCC::Component*>::iterator c = components.begin(); c != components.end(); ++c)
    {
      GraphWCC::Component* component = *c;

      // Extract the graph for this component
      StmtGraph* subgraph = dynamic_cast<StmtGraph*>(wcc.extractComponent(component, true));

      // Append the components' nodes, ordered, to the ordered list
      order(subgraph, ordered_stmts);
      delete subgraph;
    }
  }
  else
  {
    // There is only one component, so just use the original graph
    order(graph, ordered_stmts);
  }

  freeGraph(graph);
}; // NUStmtList *Reorder::layoutStmtList

//! DataOrderNodeCmp
/*! This class is used to sort the block statement graph by data
 *  flow. A number of alternatives were tried to see if they would
 *  improve performance. They were:
 *
 *  1. Visiting the deeper graph leg first.
 *
 *  2. Visiting the shorter graph leg first.
 *
 *  3. Interleaving different parts of the DAG to make sure that the
 *     data gets written before it is read.
 *
 *  4. Variations of the above.
 *
 *  But none of these ways improved performance on the ARM/l2cc test
 *  case. The best was to sort by statement location, then original
 *  order, and finally NUUseDefNode::comapre.
 */
class DataOrderNodeCmp : public GraphSortedWalker::NodeCmp
{
public:
  //! constructor
  DataOrderNodeCmp(StmtGraph* graph) : mGraph(graph) {}

  //! ordering function
  bool operator()(const GraphNode* n1, const GraphNode* n2) const
  {
    return compare(n1, n2) < 0;
  }

private:
  //! Compare function
  int compare(const GraphNode* n1, const GraphNode* n2) const
  {
    // Test for easy case first
    if (n1 == n2) {
      return 0;
    }

    // Get the statement nodes
    const StmtGraph::Node* node1 = mGraph->castNode(n1);
    const StmtGraph::Node* node2 = mGraph->castNode(n2);
    NUStmt* stmt1 = node1->getData();
    NUStmt* stmt2 = node2->getData();

    // Sort by locator
    GraphRecord* rec1 = (GraphRecord*)mGraph->getScratch(n1);
    GraphRecord* rec2 = (GraphRecord*)mGraph->getScratch(n2);
    int cmp = 0;
    cmp = SourceLocator::compare(stmt1->getLoc(), stmt2->getLoc());

    // Sort by original order
    if (cmp == 0) {
      if (rec1->mOrder < rec2->mOrder) {
        cmp = -1;
      } else if (rec1->mOrder > rec2->mOrder) {
        cmp = 1;
      }
    }

    // Sort use def compare last
    if (cmp == 0) {
      cmp = NUUseDefNode::compare(stmt1, stmt2);
    }

    // The nodes can't be the same
    NU_ASSERT2(cmp != 0, stmt1, stmt2);
    return cmp;
  } // int compare

  //! Graph for casting to right data type
  StmtGraph* mGraph;
}; // class DataOrderNodeCmp : public class GraphSortedWalker::NodeCmp

class DataOrderEdgeCmp : public GraphSortedWalker::EdgeCmp
{
public:
  //! Constructor
  DataOrderEdgeCmp(StmtGraph* graph, DataOrderNodeCmp* nodeCmp) :
    mGraph(graph), mNodeCmp(nodeCmp) {}

  //! ordering function
  bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
  {
    // Sort by target node order
    const GraphNode* n1 = mGraph->endPointOf(e1);
    const GraphNode* n2 = mGraph->endPointOf(e2);
    return (*mNodeCmp)(n1, n2);
  }

private:
  //! Graph for casting
  StmtGraph* mGraph;

  //! Node order to aid in edge ordering
  DataOrderNodeCmp* mNodeCmp;
};

class DataOrderWalk : public GraphSortedWalker
{
public:
  //! constructor
  DataOrderWalk(StmtGraph* graph, NUStmtList* ordered_stmts) :
    mGraph(graph), mOrderedStmts(ordered_stmts)
  {}

  //! Override visit node after for DFS walk
  Command visitNodeAfter(Graph*, GraphNode* node)
  {
    NUStmt* stmt = mGraph->castNode(node)->getData();
    mOrderedStmts->push_back(stmt);
    return GW_CONTINUE;
  }

  //! Should not have cycles
  Command visitBackEdge(Graph*, GraphNode* n1, GraphEdge* edge)
  {
    // Get the target node for the assert
    GraphNode* n2 = mGraph->endPointOf(edge);

    // Assert we got a cycles
    bool cycleFound = true;
    NUStmt* stmt1 = mGraph->castNode(n1)->getData();
    NUStmt* stmt2 = mGraph->castNode(n2)->getData();
    NU_ASSERT2(!cycleFound, stmt1, stmt2);
    return GW_CONTINUE;
  }

private:
  //! Graph for casting
  StmtGraph* mGraph;

  //! Place to store ordered statements
  NUStmtList* mOrderedStmts;
}; // class DataOrderWalk : public GraphSortedWalker

void Reorder::dataOrder(StmtGraph* graph, NUStmtList* ordered_stmts)
{
  // Sort the statements by flow order
  DataOrderNodeCmp cmpNode(graph);
  DataOrderEdgeCmp cmpEdge(graph, &cmpNode);
  DataOrderWalk walker(graph, ordered_stmts);
  walker.walk(graph, &cmpNode, &cmpEdge);
}


void Reorder::freeGraph(StmtGraph *graph)
{
  // free the GraphRecord memory
  for (Iter<GraphNode*> loop = graph->nodes(); !loop.atEnd(); ++loop)
  {
    StmtGraph::Node* node = graph->castNode(*loop);
    GraphRecord* rec = (GraphRecord*) graph->getScratch(node);
    delete rec;
  }
  // delete the graph
  delete graph;
}


//! Append the ordered statements from the graph to the given list
void Reorder::order(StmtGraph* graph, NUStmtList* ordered_stmts)
{
// #define DEBUG_STATEMENT_GRAPH
#ifdef DEBUG_STATEMENT_GRAPH
  UtIO::cout() << "---start---" << UtIO::endl;
  StmtGraphDotWriter::writeGraph(graph,UtIO::cout());
  UtIO::cout() << "---end---" << UtIO::endl;
#endif

  // If the caller asked for data order then just due a DFS walk. This
  // helps gcc do better register allocation and allows data to be in
  // the cache.
  if (mDataOrderStatements) {
    dataOrder(graph, ordered_stmts);
    return;
  }

  // Populate a GraphQueue with the graph nodes
  GraphQueue graphq;
  graphq.init(graph, mGroupControlStatements);
    
  // Remove nodes from the queue, adding temporaries when required
  graphq.scheduleWalk();

  // Get the list of statements that require a temp
  NUStmtList tmpStmts;
  graphq.getTmpStmts(&tmpStmts);
  for (NUStmtList::iterator s = tmpStmts.begin(); s != tmpStmts.end(); ++s) {
    NUStmt* stmt = *s;
    requireTemp(stmt);
  }
    
  // Get the reordered list from the queue    
  NUStmtList new_list;
  graphq.getList(&new_list);
  for (NUStmtList::iterator s = new_list.begin(); s != new_list.end(); ++s) {
    NUStmt* stmt = *s;
    ordered_stmts->push_back(stmt);
  }
}


void Reorder::stmtList(const NUStmtList * unordered_stmts, 
                       NUStmtList * ordered_stmts)
{
  if (unordered_stmts->empty())
    return;
  
  layoutStmtList(unordered_stmts, ordered_stmts);

  // report on ordering changes if -verboseReorder was used
  bool changed = false;
  StatementOrderMap order;
  if (mVerbose) {
    // check if the statement order changed
    changed = orderChanged(unordered_stmts, ordered_stmts);

    // if it did change, dump the unordered list before we convert to blocking assigns
    if (changed) {
      dumpPreOrdering(unordered_stmts, order);
    }
  }

  // convert the statements to blocking assignments
  for (NUStmtList::iterator s = ordered_stmts->begin(); s != ordered_stmts->end(); ++s)
  {
    NUStmt* stmt = *s;
    serialize(stmt);
  }

  // if we are reporting changes, dump the new order after serialization
  if (mVerbose && changed) {
    dumpPostOrdering(ordered_stmts, order);
  }
} // NUStmtList *Reorder::stmtList


void Reorder::serialize(NUStmt *this_stmt)
{
  NUIf *if_stmt = dynamic_cast<NUIf*>(this_stmt);
  NUFor *for_stmt = dynamic_cast<NUFor*>(this_stmt);
  NUCase *case_stmt = dynamic_cast<NUCase*>(this_stmt);
  NUBlock *block = dynamic_cast<NUBlock*>(this_stmt);
  NUNonBlockingAssign *nonblock_assign =
    dynamic_cast<NUNonBlockingAssign*>(this_stmt);

  if (if_stmt)
    ifStmt(if_stmt);
  else if (for_stmt)
    forStmt(for_stmt);
  else if (case_stmt)
    caseStmt(case_stmt);
  else if (block)
    blockScope(block);

  // If this stmt is a non-blocking assign, then rewrite to a
  // blocking assign and remember that we need to reorder it.
  else if (nonblock_assign != 0) {
    NUBlockingAssign *new_stmt = nonblock_assign->makeBlocking();
    NU_ASSERT2(new_stmt == this_stmt, new_stmt, this_stmt); // just changes vtable
    NUNetRefSet defs(mNetRefFactory);
    new_stmt->getBlockingDefs(&defs);
    for (NUNetRefSetIter defs_iter = defs.begin();
         defs_iter != defs.end();
         ++defs_iter) {
      NUNetRefHdl net_ref = *defs_iter;
      NUNet *net = net_ref->getNet();
      mNonBlockMap.insert(AssignMultiMap::value_type(net, new_stmt));
    }
  }
}


bool Reorder::orderChanged(const NUStmtList * unordered_stmts,
                           const NUStmtList * ordered_stmts)
{
  bool changed = false;
  NUStmtList::const_iterator old_list = unordered_stmts->begin();
  NUStmtList::const_iterator new_list = ordered_stmts->begin();
  while (!changed && old_list != unordered_stmts->end() && new_list != ordered_stmts->end())
  {
    NUStmt* old_stmt = *old_list;
    NUStmt* new_stmt = *new_list;
    if (old_stmt != new_stmt)
    {
      changed = true;
    }
    else
    {
      ++old_list;
      ++new_list;
    }
  }
  return changed;
}


void Reorder::dumpPreOrdering(const NUStmtList * unordered_stmts,
                              StatementOrderMap & order)
{
  UInt32 count = 1;
  UtIO::cout() << "Reordered statement list -- original was:\n";
  for (NUStmtList::const_iterator s = unordered_stmts->begin(); s != unordered_stmts->end(); ++s)
  {
    NUStmt* stmt = *s;
    UtIO::cout() << "[" << count << "]\t";
    stmt->printVerilog(false, 0, false);
    order[stmt] = count;
    ++count;
  }
}


void Reorder::dumpPostOrdering(const NUStmtList * ordered_stmts,
                               StatementOrderMap & order)
{
  UtIO::cout() << "New statement list order is:\n";
  for (NUStmtList::const_iterator s = ordered_stmts->begin(); s != ordered_stmts->end(); ++s)
  {
    NUStmt* stmt = *s;
    UtIO::cout() << "[" << order[stmt] << "]\t";
    stmt->printVerilog(false, 0, false);
  }
}


void Reorder::dumpOrdering(const NUStmtList * unordered_stmts,
                           const NUStmtList * ordered_stmts)
{
  bool changed = false;
  StatementOrderMap order;
  if (mVerbose) {
    // check if the statement order changed
    changed = orderChanged(unordered_stmts, ordered_stmts);

    // if it did change, dump the unordered then ordered lists.
    if (changed) {
      dumpPreOrdering(unordered_stmts, order);
      dumpPostOrdering(ordered_stmts, order);
    }
  }
}


//! Detect if a net is defined by both blocking and non-blocking drivers
static void helperDetectBlockAndNonblockDrivers(NUNetRefSet *block_defs,
                                                NUNetRefSet *non_block_defs,
                                                NUNetRefFactory *netref_factory,
                                                MsgContext *msg_context,
                                                const SourceLocator& loc)
{
  if (block_defs->empty() or non_block_defs->empty()) {
    return;
  }

  NUNetRefSet intersect(netref_factory);
  NUNetRefSet::set_intersection(*block_defs, *non_block_defs, intersect);
  for (NUNetRefSetIter iter = intersect.begin(); iter != intersect.end(); ++iter) {
    NUNetRefHdl net_ref = *iter;
    // Would like to report the flow that conflicts, but we don't have a flow-graph yet
    // At least tell them what always block or task was responsible...
    msg_context->BlockAndNonblockDrivers(&loc, net_ref->getNet()->getName ()->str ());
  }
}


void Reorder::design(NUDesign *the_design)
{
  NUModuleList mods;
  the_design->getModulesBottomUp(&mods);
  for (NUModuleList::iterator iter = mods.begin();
       iter != mods.end();
       ++iter) {
    module( *iter );
  }
}


void Reorder::module(NUModule *this_module)
{
  for (NUTaskLoop loop = this_module->loopTasks(); not loop.atEnd(); ++loop) {
    task(*loop);
  }

  NUAlwaysBlockList always_blocks;
  this_module->getAlwaysBlocks(&always_blocks);
  for (NUAlwaysBlockListIter iter = always_blocks.begin();
       iter != always_blocks.end();
       ++iter) {
    structuredProc(*iter);
  }

  NUInitialBlockList initial_blocks;
  this_module->getInitialBlocks(&initial_blocks);
  for (NUInitialBlockListIter iter = initial_blocks.begin();
       iter != initial_blocks.end();
       ++iter) {
    structuredProc(*iter);
  }
}


void Reorder::task(NUTask *task)
{
  mCurrentNode = task; // Needed for re-computing use/def info

  // Create warnings about having blocking and non-blocking assigns to the
  // same variable.
  NUNetRefSet block_defs(mNetRefFactory);
  NUNetRefSet non_block_defs(mNetRefFactory);
  task->getBlockingDefs(&block_defs);
  task->getNonBlockingDefs(&non_block_defs);
  helperDetectBlockAndNonblockDrivers(&block_defs,
                                      &non_block_defs,
                                      mNetRefFactory,
                                      mMsgContext,
                                      task->getLoc ());

  // Issue a warning message when non-blocking assigns are used.  Since we
  // do not infer state for tasks or functions, these will always behave
  // differently than simulation.
  if (not non_block_defs.empty()) {
    mMsgContext->TaskNonblock(task);
  }

  // Perform the reordering, collect the non-blocking assigns which we
  // must create temps for.
  mNonBlockMap.clear();
  mMustTempSet->clear();
  blockScope(task);

  const SourceLocator& use_loc = task->getLoc();

  createTemporaries(task, use_loc);

  // Redo the UD computation if there were non-blocking defs, since there are no
  // longer any non-blocking defs.
  if (not non_block_defs.empty()) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.tf(task);
    non_block_defs.clear();
    task->getNonBlockingDefs(&non_block_defs);
    NU_ASSERT(non_block_defs.empty(),task);
  }

  mCurrentNode = 0;
}


void Reorder::structuredProc(NUStructuredProc *structured_proc)
{
  mCurrentNode = structured_proc; // Needed for re-computing use/def info
  NUBlock * block = structured_proc->getBlock();

  // Create warnings about having blocking and non-blocking assigns to the
  // same variable.
  NUNetRefSet block_defs(mNetRefFactory);
  NUNetRefSet non_block_defs(mNetRefFactory);
  block->getBlockingDefs(&block_defs);
  block->getNonBlockingDefs(&non_block_defs);
  helperDetectBlockAndNonblockDrivers(&block_defs,
                                      &non_block_defs,
                                      mNetRefFactory,
                                      mMsgContext,
                                      structured_proc->getLoc ());

  // Perform the reordering, collect the non-blocking assigns which we
  // must create temps for.
  mNonBlockMap.clear();
  mMustTempSet->clear();
  blockScope(block);

  const SourceLocator& use_loc = block->getLoc();

  createTemporaries(block, use_loc);

  // Redo the UD computation if there were non-blocking defs, since there are no
  // longer any non-blocking defs.
  if (not non_block_defs.empty()) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.structuredProc(structured_proc);
    non_block_defs.clear();
    block->getNonBlockingDefs(&non_block_defs);
    NU_ASSERT(non_block_defs.empty(),block);
  }
  mCurrentNode = NULL;
}

void Reorder::blockScope(NUBlockScope *scope)
{
  NUStmtLoop stmt_loop = scope->loopStmts();
  NUStmtList stmt_list(stmt_loop.begin(), stmt_loop.end());
  NUStmtList new_list;
  stmtList(&stmt_list,&new_list);
  scope->replaceStmtList(new_list);
}


void Reorder::ifStmt(NUIf *if_stmt)
{
  NUStmtList new_list;

  NUStmtList *stmts = if_stmt->getThen();
  stmtList(stmts,&new_list);
  if_stmt->replaceThen(new_list);
  delete stmts;

  new_list.clear();

  stmts = if_stmt->getElse();
  stmtList(stmts,&new_list);
  if_stmt->replaceElse(new_list);
  delete stmts;
}


void Reorder::forStmt(NUFor *for_stmt)
{
  NUStmtList new_list;
  NUStmtList *stmts = for_stmt->getBody();
  stmtList(stmts, &new_list);
  for_stmt->replaceBody(new_list);
  delete stmts;
}


void Reorder::caseStmt(NUCase *case_stmt)
{
  for (NUCase::ItemLoop items_iter = case_stmt->loopItems();
       !items_iter.atEnd();
       ++items_iter)
  {
    // Reorder the statements for this case item
    NUCaseItem* caseItem = *items_iter;
    NUStmtList new_list;
    NUStmtList *stmts = caseItem->getStmts();
    stmtList(stmts,&new_list);
    caseItem->replaceStmts(new_list);
    delete stmts;
  }
}


void Reorder::createTemporaries(NUBlockScope *scope, const SourceLocator& use_loc)
{
  // Only build one temp memory net per memory within a scope
  MemTempMap memTempMap;

  for (NUNetRefSet::NetRefSetLoop loop = mMustTempSet->loop();
       not loop.atEnd();
       ++loop) {
    NUNetRefHdl must_temp_net_ref = (*loop);
    NUNet * net = must_temp_net_ref->getNet();

    // 0. We know which net refs need temping, but we don't know the
    //    full set of bits for this net defined within this scope.
    NUNetRefHdl net_ref = completeDefs(must_temp_net_ref);

    // 1. Create a temporary.
    bool is_mem = false;
    bool is_complete = false;
    NUNet * temp = createTemporary(net,scope,&memTempMap,&is_mem,&is_complete);

    if (mVerbose)
    {
      if (is_mem)
        UtIO::cout() << "*** Added temporary for memory '";
      else
        UtIO::cout() << "*** Added temporary for net '";
      UtIO::cout() << net->getName()->str();
      UtIO::cout() << "' to scope '" << scope->getPrintableName() << "'\n"; 
    }

    // 2. Initialize the temporary.
    if (is_mem) {
      // Memories are fully initialized.
      NULvalue * temp_lhs = new NUIdentLvalue(temp, use_loc);
      NUExpr   * net_rhs  = new NUIdentRvalue(net, use_loc);
      NUBlockingAssign * temp_assign = new NUBlockingAssign(temp_lhs, net_rhs, false, use_loc);
      scope->addStmtStart(temp_assign);
    } else {
      // Non-memories are initialized to zero; the defined parts are then updated.
      for (NUNetRefRangeLoop range_loop = net_ref->loopRanges(mNetRefFactory);
           not range_loop.atEnd();
           ++range_loop) {
        ConstantRange range = (*range_loop);
        if (range.getLength () == net->getBitSize ())
          is_complete = true;

        NULvalue * partial_lhs = NULL;
        NUExpr   * partial_rhs = NULL;
        if (is_complete) {
          NU_ASSERT(range.getLength () == net->getBitSize (),net_ref);
          partial_lhs = new NUIdentLvalue(temp, use_loc);
          partial_rhs = new NUIdentRvalue(net, use_loc);
        } else {
          partial_lhs = new NUVarselLvalue(temp, range, use_loc);
          partial_rhs = new NUVarselRvalue(net,  range, use_loc);
        }
        NUBlockingAssign * partial_assign = new NUBlockingAssign(partial_lhs,
                                                                 partial_rhs,
                                                                 false,
                                                                 use_loc);
        scope->addStmtStart(partial_assign);
      }

      // We have to add the initialization to zero after updating the
      // defined parts because we're adding to the head of the stmt
      // list.
      NULvalue * temp_lhs  = new NUIdentLvalue(temp, use_loc);
      NUExpr *const_rhs;
      if ( temp_lhs->isReal( ))
        const_rhs = NUConst::create (0.0, use_loc);
      else
        const_rhs = NUConst::create(false, 0, 32, use_loc);
      NUBlockingAssign * temp_assign = new NUBlockingAssign(temp_lhs, const_rhs, false,
                                                            use_loc);
      scope->addStmtStart(temp_assign);
    }

    // 3. Update the net at the end of the block.
    if (is_mem) {
      // Memories are fully updated.
      NULvalue * net_lhs  = new NUIdentLvalue(net, use_loc);
      NUExpr   * temp_rhs = new NUIdentRvalue(temp, use_loc);
      NUBlockingAssign *end_assign = new NUBlockingAssign(net_lhs, temp_rhs, false, use_loc);
      scope->addStmt(end_assign);
    } else {
      // Non-memories only have the defined parts updated.
      for (NUNetRefRangeLoop range_loop = net_ref->loopRanges(mNetRefFactory);
           not range_loop.atEnd();
           ++range_loop) {
        ConstantRange range = (*range_loop);

        NULvalue * partial_lhs = NULL;
        NUExpr   * partial_rhs = NULL;
        if (is_complete) {
          NU_ASSERT(range.getLength () == net->getBitSize (),net_ref);
          partial_lhs = new NUIdentLvalue(net, use_loc);
          partial_rhs = new NUIdentRvalue(temp, use_loc);
        } else {
          partial_lhs = new NUVarselLvalue(net,  range, use_loc);
          partial_rhs = new NUVarselRvalue(temp, range, use_loc);
        }
        NUBlockingAssign * partial_assign = new NUBlockingAssign(partial_lhs,
                                                                 partial_rhs,
                                                                 false,
                                                                 use_loc);
        scope->addStmt(partial_assign);
      }
    }

    // 4. Go through all assignments to this net, making the assigns
    // to the temporary instead of the net.
    replaceNetWithTemporary(net, temp);
  } // for
} // void Reorder::createTemporaries


NUNetRefHdl Reorder::completeDefs(NUNetRefHdl & must_temp_net_ref)
{
  NUNetRefHdl net_ref = must_temp_net_ref;
  NUNet * net = must_temp_net_ref->getNet();
  NUNetRefHdl complete_net_ref = mNetRefFactory->createNetRef(net);

  std::pair<AssignMultiMap::iterator,AssignMultiMap::iterator>
    assign_range_iter = mNonBlockMap.equal_range(net);
  for (AssignMultiMap::iterator assign_iter = assign_range_iter.first;
       assign_iter != assign_range_iter.second;
       ++assign_iter) {
    NUBlockingAssign *assign = assign_iter->second;
    NUNetRefSet defs(mNetRefFactory);
    assign->getBlockingDefs(&defs);
    for (NUNetRefSet::CondLoop def_loop = defs.loop(complete_net_ref,&NUNetRef::overlapsSameNet);
         not def_loop.atEnd();
         ++def_loop) {
      net_ref = mNetRefFactory->merge(net_ref, *def_loop);
    }
  }
  return net_ref;
}


NUNet * Reorder::createTemporary(NUNet * net, 
                                 NUBlockScope *scope, 
                                 MemTempMap * memTempMap,
                                 bool * is_mem,
                                 bool * is_bit)
{
  (*is_mem) = false;
  (*is_bit) = false;

  NUNet * temp = NULL;
  const SourceLocator& decl_loc = net->getLoc();
  NUVectorNet *vector_net = dynamic_cast<NUVectorNet*>(net);
  NUMemoryNet *mem_net = net->getMemoryNet();
  StringAtom* sym = scope->gensym("reorder", NULL, net);

  if (mem_net != NULL) {
    (*is_mem) = true;
    NUTempMemoryNet* memTemp = (*memTempMap)[mem_net];
    if (memTemp == NULL) {
      // Create it static for now and have memory loop fix it
      memTemp = scope->createTempMemoryNet(sym, mem_net, false);
      (*memTempMap)[mem_net] = memTemp;
    }
    temp = memTemp;
  } else if (vector_net != NULL) {
    NetFlags net_flags = eNoneNet;
    if ( vector_net->isInteger() ) {
      if (vector_net->isSigned())
        net_flags = NetRedeclare(net_flags, eDMIntegerNet|eSigned); 
      else
        net_flags = NetRedeclare(net_flags, eDMIntegerNet);
    }
    else if ( vector_net->isReal( )) {
      net_flags = NetRedeclare( net_flags, eDMRealNet|eSigned );
    }
    else if ( vector_net->isTime() ) {
      net_flags = NetRedeclare(net_flags, eDMTimeNet);
    }
    temp = scope->createTempVectorNet(sym,
                                      *vector_net->getDeclaredRange(),
                                      vector_net->isSigned(),
                                      decl_loc, net_flags, vector_net->getVectorNetFlags ());
  } else if (net->isBitNet()) {
    (*is_bit) = true;
    temp = scope->createTempBitNet(sym, net->isSigned(), decl_loc);
  }
  NU_ASSERT(temp,net);
  return temp;
}


void Reorder::replaceNetWithTemporary(NUNet * net, NUNet * temp)
{
  std::pair<AssignMultiMap::iterator,AssignMultiMap::iterator>
    assign_range_iter = mNonBlockMap.equal_range(net);
  for (AssignMultiMap::iterator assign_iter = assign_range_iter.first;
       assign_iter != assign_range_iter.second;
       ++assign_iter) {
    NUBlockingAssign *assign = assign_iter->second;
    assign->replaceDef(net, temp);
  }
}
