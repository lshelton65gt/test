// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "localflow/TicProtectedNameManager.h"
#include "localflow/VerilogPopulate.h"
#include "LFContext.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUHierRef.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"
#include "util/AtomicCache.h"

/*
 * If a sequential block contains a Verilog named block as its child,
 * create both an NUBlock and an NUDeclarationScope for it.
 * Otherwise, just accumulate the statements it contains into the
 * on-going list (essentially flattening non-named blocks).
 */
Populate::ErrorCode VerilogPopulate::sequentialBlock(veNode ve_block,
                                                     LFContext *context,
                                                     NUBlock* currentBlock)
{
  ErrorCode err_code = eSuccess;

  SourceLocator loc = locator(ve_block);

  // Create the hierarchical references.
  CheetahList vars(veBlockGetScopeVariableList(ve_block));
  ErrorCode hier_err_code = scopeVariableList(vars, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(hier_err_code, &err_code ) ) {
    return hier_err_code;
  }

  NUNamedDeclarationScope * declaration_scope = NULL;

  UtString block_name;
  mTicProtectedNameMgr->getVisibleName(ve_block, &block_name);
  bool block_has_name = (block_name.size() > 0);

  if (block_has_name) {
    // Create a declaration scope because this is a Verilog named block.

    StringAtom *name = context->getStringCache()->intern(block_name);
    declaration_scope  = new NUNamedDeclarationScope(name,
                                                    context->getDeclarationScope(),
                                                    mIODB,
                                                    mNetRefFactory,
                                                    loc);

    context->pushDeclarationScope(declaration_scope);

    // Create the local nets
    CheetahList ve_net_iter(veBlockGetNetList(ve_block));
    if (ve_net_iter) {
      veNode ve_net;
      while ((ve_net = VeListGetNextNode(ve_net_iter))) {
	NUNet* net = 0;
        ErrorCode temp_err_code;
        if (veNodeGetObjType(ve_net) == VE_NETARRAY)
          temp_err_code = var(ve_net, context, &net);
        else
          temp_err_code = local(ve_net, context, &net);

        if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
	// May be 0, for instance if we are re-declaring a port
	if (net) {
	  declaration_scope->addLocal(net);
	}
      }
    }

    // Create the local variables
    CheetahList ve_var_iter(veBlockGetVariableList(ve_block));
    if (ve_var_iter) {
      veNode ve_var;
      while ((ve_var = VeListGetNextNode(ve_var_iter)) != NULL) {
	NUNet* net = 0;
	ErrorCode temp_err_code = var(ve_var, context, &net);
        if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
	if (net) {
	  declaration_scope->addLocal(net);
	}
      }
    }
  }

  CheetahList ve_stmt_iter(veBlockGetStmtList(ve_block));
  if (ve_stmt_iter) {
    // If the user named the block, then inject an NUBlock if there
    // was not one already built.  This is needed so that if a disable
    // is encountered, it can find its target
    NUStmtList stmts;
    bool blockify = (currentBlock == NULL) && (block_has_name);
    if (blockify) {
      context->pushStmtList(&stmts);
      currentBlock = new NUBlock(NULL,
                                 context->getBlockScope(),
                                 mIODB,
                                 context->getNetRefFactory(),
                                 false,
                                 loc);
      mBlockMap[ve_block] = currentBlock;
      context->pushBlockScope(currentBlock);
    }

    while (veNode ve_stmt = VeListGetNextNode(ve_stmt_iter)) {
      ErrorCode temp_err_code = stmt(ve_stmt, context);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
    }

    if (blockify) {
      context->popStmtList();
      currentBlock->addStmts(&stmts);
      context->addStmt(currentBlock);
      context->popBlockScope();     // for NUBlock 
    }
  }

  if (block_has_name) {
    if (declaration_scope) {
      context->popDeclarationScope();
    }
  }

  return err_code;
}


static int sGetMaxCaseExprSize(veNode ve_case)
{
  veNode ve_sel = veCaseGetCaseExpr(ve_case);  
  int curSize = veExprEvaluateWidth(ve_sel);
  
  CheetahList ve_case_item_iter(veCaseGetCaseItemList(ve_case));
  if (ve_case_item_iter != NULL)
  {
    // Loop through the case items
    veNode ve_case_item;
    while ((ve_case_item = VeListGetNextNode(ve_case_item_iter)) != NULL)
    {
      CheetahList ve_expr_iter(veCaseItemGetExprList(ve_case_item));
      if (ve_expr_iter != NULL)
      {
        veNode ve_expr;
        while ((ve_expr = VeListGetNextNode(ve_expr_iter)) != NULL) {
          int expSize = veExprEvaluateWidth(ve_expr);
          curSize = std::max(curSize, expSize);
          //cerr << "case stmt size: " << expSize << UtIO::endl;
        }
      }
    }
  }
  return curSize;
}


Populate::ErrorCode VerilogPopulate::caseStmt(veNode ve_case,
                                              LFContext *context,
                                              NUCase **the_stmt)
{
  ErrorCode err_code = eSuccess;
  *the_stmt = 0;

  // Need the location
  SourceLocator loc = locator(ve_case);

  veObjType obj_type = veNodeGetObjType(ve_case);
  NUCase::CaseType case_type = ( (obj_type == VE_CASEX) ? NUCase::eCtypeCasex : ((obj_type == VE_CASEZ) ? NUCase::eCtypeCasez : NUCase::eCtypeCase));
  int chkMaxExprSize = sGetMaxCaseExprSize(ve_case);
  //UtIO::cerr() << "Max case size: " << chkMaxExprSize << UtIO::endl;
  if (not (chkMaxExprSize > 0)) {
    POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Case expression size is not > 0"), &err_code);
    return err_code;
  }

  // Get the case select expression
  veNode ve_sel = veCaseGetCaseExpr(ve_case);

  size_t maxExprSize = static_cast<size_t>(chkMaxExprSize);

  NUExpr* sel = 0;
  ErrorCode temp_err_code = expr(ve_sel,
				 context,
				 context->getModule(),
				 chkMaxExprSize,
				 &sel);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  UInt32 naturalSelWidth = maxExprSize;
  const NUExpr::Type selType = sel->getType();
  if ( selType == NUExpr::eNUIdentRvalue or
       selType == NUExpr::eNUVarselRvalue or
       selType == NUExpr::eNUConcatOp ) {
    // if we can determine the max number of bits sel can affect, use
    // that size instead of the one based on the branch constants.
    naturalSelWidth = sel->determineBitSize();
  }
  
  // Check to see if the user specified full case directive
  bool hasFullCaseDirective = false;
  CheetahList ve_case_directives_iter(veNodeGetSynthDirectiveList(ve_case));
  if (ve_case_directives_iter != NULL)
  {
    veNode ve_synth_dir;
    veSynthDirectiveType ve_synth_dir_type;
    while ((ve_synth_dir = veListGetNextNode(ve_case_directives_iter)) != NULL)
    {
      ve_synth_dir_type = veSynthDirectiveGetDirectiveType(ve_synth_dir);
      // This used to treat parallel case like full case, but we believe
      // that to be wrong - see bug 3022.
      if ((ve_synth_dir_type == VE_FULL_CASE) ||
	  (ve_synth_dir_type == VE_BOTH_CASE))
	hasFullCaseDirective = true;
    }
  }

  sel = sel->makeSizeExplicit(maxExprSize);

  *the_stmt = new NUCase(sel, context->getNetRefFactory(), case_type, false, loc);

  // Get the case items
  CheetahList ve_case_item_iter(veCaseGetCaseItemList(ve_case));
  NUCaseItem* default_item = NULL;
  if (ve_case_item_iter != NULL)
  {
    // Loop through the case items
    veNode ve_case_item;
    while ((ve_case_item = veListGetNextNode(ve_case_item_iter)) != NULL)
    {
      // Create the case item on the list
      NUCaseItem* case_item = 0;
      bool isDefault = false;
      temp_err_code = caseItem(ve_case_item,
			       sel,
			       context,
			       veNodeGetObjType(ve_case),
			       &isDefault,
			       maxExprSize,
			       &case_item);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }

      // Store the default so that we can put it at the end of the list
      if (isDefault) {
        // Cheetah should catch these
        NU_ASSERT(default_item == NULL, *the_stmt);
        default_item = case_item;
      }
      else {
        // Check for empty statement list
        if (case_item != NULL)
          (*the_stmt)->addItem(case_item);
      }
    }

    // Now we can add the default item
    if (default_item != NULL) {
      (*the_stmt)->addItem(default_item);
      (*the_stmt)->putHasDefault(true);
    }
  }

  (*the_stmt)->eliminateUnnecessaryCaseBits(mFold);

  // Store the flags
  (*the_stmt)->putUserFullCase(hasFullCaseDirective);

  temp_err_code = computeFullCase(the_stmt, context);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
    return temp_err_code;
  }

  return err_code;
}

Populate::ErrorCode VerilogPopulate::disable(veNode ve_disable,
                                             LFContext *context,
                                             NUStmt **the_stmt)
{
  ErrorCode err_code = eSuccess;
  SourceLocator loc = locator(ve_disable);
  veNode target = veDisableGetTaskOrBlock(ve_disable);
  UtString bname;
  if ( not  mTicProtectedNameMgr->getVisibleName(target, &bname) ) {
    POPULATE_FAILURE(mMsgContext->UnsupportedBreak(&loc, "disable", gGetObjTypeStr(veNodeGetObjType(target))), &err_code);
    return err_code;
  }

  BlockMap::iterator p = mBlockMap.find(target);
  if (p == mBlockMap.end()) {
    POPULATE_FAILURE(mMsgContext->UnsupportedBreak(&loc, "disable", bname.c_str()), &err_code);
    return err_code;
  }

  NUBlock* block = p->second;
  AtomicCache* cache = context->getStringCache();
  StringAtom* blockSym = cache->intern(bname);
  StringAtom* keyword = cache->intern("disable");
  *the_stmt = new NUBreak(block, keyword, blockSym, mNetRefFactory, loc);
  return err_code;
}

Populate::ErrorCode VerilogPopulate::caseItem(veNode ve_case_item,
                                              NUExpr* /*sel*/,
                                              LFContext* context,
                                              veObjType ve_obj_type,
                                              bool* hasDefault,
                                              size_t maxExprSize,
                                              NUCaseItem **the_item)
{
  ErrorCode err_code = eSuccess;
  *the_item = 0;

  // Need the location to create the binary op below
  SourceLocator loc = locator(ve_case_item);
  
  // Make sure this isn't an empty default item. If so, we don't have
  // to create anything.
  veNode ve_stmt = veCaseItemGetStatement(ve_case_item);
  CheetahList ve_expr_iter(veCaseItemGetExprList(ve_case_item));
  if ((ve_stmt == NULL) && (ve_expr_iter == NULL))
    return eSuccess;

  // Get the list of match expressions
  *the_item = new NUCaseItem(loc);
  bool hasExprs = false;
  if (ve_expr_iter != NULL)
  {
    veNode ve_expr;
    while ((ve_expr = VeListGetNextNode(ve_expr_iter)) != NULL)
    {
      // Get the item expression
      NUExpr* the_expr = 0;
      ErrorCode temp_err_code = expr(ve_expr,
				     context,
				     context->getModule(),
				     static_cast<int>(maxExprSize),
				     &the_expr);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      if (not the_expr) {
	continue;
      }

      {
        // JDM: I think this could be optimized.  Suppose I have this:
        //   case (a[127:0])
        //   b[1:0] + c[1:0]: ...
        //   ...
        // Verilog rules will cause the addition to be done in 128
        // bits, but it's not needed.  It could be done in 3 bits and
        // *then* extended to 128 bits.  Later.
        the_expr = the_expr->makeSizeExplicit(maxExprSize);
      }

      NUExpr* mask = NULL;
      if (ve_obj_type != VE_CASE) {
	// Create the mask we need. If there are no don't cares we get
	// NULL back.
	temp_err_code = casexMask(ve_expr, context, loc, (ve_obj_type == VE_CASEX), maxExprSize, &mask);
        if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
      }
      (*the_item)->addCondition(new NUCaseCondition(loc, the_expr, mask));
      hasExprs = true;
    }
  }

  // The case item without expression is default.
  if (!hasExprs)
    *hasDefault = true;

  // Loop through the case item statements, they could be empty in
  // which case ve_stmt is NULL. The only case that should fall
  // through here is one where the exression is not NULL. We have to
  // code this to make the default case work (if there is one; I guess
  // we could check if one exists, but right now it doesn't seem worth
  // it).
  NUStmtList stmts;
  if (ve_stmt != NULL)
  {
    context->pushStmtList(&stmts);
    ErrorCode temp_err_code = stmt(ve_stmt, context);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    context->popStmtList();
    for (NUStmtList::iterator p = stmts.begin(); p != stmts.end(); ++p)
      (*the_item)->addStmt(*p);
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::eventControl(veNode ve_control,
                                                  LFContext *context, bool *isWildCard)
{
  ErrorCode err_code = eSuccess;
  SourceLocator control_loc = locator(ve_control);
  CheetahList ve_event_iter(veEventControlGetExprList(ve_control));
  if (ve_event_iter) {
    while (veNode ve_event = VeListGetNextNode(ve_event_iter)) {
      NUExpr *the_expr = 0;
      ErrorCode temp_err_code = eventExpr(ve_event, context, control_loc, &the_expr);
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      if (the_expr) {
        if (the_expr->getType() == NUExpr::eNUEdgeExpr) {
          the_expr->resize(1);
          NUEdgeExpr* edge_expr = dynamic_cast<NUEdgeExpr*>(the_expr);
          NU_ASSERT(edge_expr != NULL, the_expr);
          context->addEdgeExpr(edge_expr);
        } else {
          the_expr->resize(the_expr->determineBitSize());
          context->addEventExpr(the_expr, control_loc);
        }
      }
    }
  }
  else
  {
    // The always block uses wild card in the sensitivity list
    if(isWildCard != NULL)
      *isWildCard = true;
  }

  return err_code;
}


Populate::ErrorCode VerilogPopulate::delayOrEventControl(veNode ve_control,
                                                         LFContext *context, bool *isWildCard)
{
  switch (veNodeGetObjType(ve_control)) {
  case VE_EVENT_CONTROL:
    return eventControl(ve_control, context, isWildCard);
    break;

  default:
    // Ignore all delay constructs
    break;
  }

  return eSuccess;
}


Populate::ErrorCode VerilogPopulate::maybeFixConditionStmt(NUExpr** cond,
                                                           SourceLocator& 
                                                           /* loc*/)
{
  ErrorCode err_code = eSuccess;
#if 0
  NUOp* condAsOp = dynamic_cast<NUOp*>(*cond);
  bool maybeDoComparison = false;
  if (condAsOp)
  {
    switch (condAsOp->getOp())
    {
    case NUOp::eUnBitNeg:
    case NUOp::eBiPlus:                
    case NUOp::eBiMinus:               
    case NUOp::eBiSMult:                
    case NUOp::eBiSDiv:                 
    case NUOp::eBiSMod:                 
    case NUOp::eBiUMult:                
    case NUOp::eBiUDiv:                 
    case NUOp::eBiUMod:                 
    case NUOp::eBiBitAnd:              
    case NUOp::eBiBitOr:               
    case NUOp::eBiBitXor:              
    case NUOp::eBiRshift:              
    case NUOp::eBiLshift:
    case NUOp::eBiRshiftArith:
    case NUOp::eBiLshiftArith:
    case NUOp::eNaConcat:
    case NUOp::eZSysTime:
      maybeDoComparison = true;
      break;
    default:
      break;
    }
  }
  else if ((dynamic_cast<NUIdentRvalue*>(*cond) != NULL) ||
           (dynamic_cast<NUVarselRvalue*>(*cond) != NULL) ||
           (dynamic_cast<NUMemselRvalue*>(*cond) != NULL) ||
           ((*cond)->castConst() != NULL))
    maybeDoComparison = true;

  if (maybeDoComparison && ((*cond)->determineBitSize() != 1)) {
    // The expression is an operator or an ident. Must  make sure the
    // eventual result is a boolean.

    UInt32 condSize = (*cond)->determineBitSize();
    NUConst* zero = NUConst::create(false, UInt64(0),  condSize, loc);

    // don't need to copy() the condition, since it isn't owned by
    // anything.
    NUExpr* superNeq = new NUBinaryOp(NUOp::eBiNeq, *cond, zero, loc);
    *cond = superNeq;
  }
#else
  *cond = (*cond)->makeConditionExpr();
#endif
  return err_code;
} // Populate::ErrorCode VerilogPopulate::maybeFixConditionStmt


Populate::ErrorCode VerilogPopulate::ifStmt(veNode ve_if,
                                            LFContext *context,
                                            NUIf **the_if)
{
  ErrorCode err_code = eSuccess;
  *the_if = 0;

  SourceLocator loc = locator(ve_if);

  NUExpr *cond = 0;
  ErrorCode temp_err_code = expr(veIfGetCondition(ve_if),
				 context,
				 context->getModule(),
				 0,
				 &cond);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  temp_err_code = maybeFixConditionStmt(&cond, loc);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

#if 0
  NUOp* condAsOp = dynamic_cast<NUOp*>(cond);
  if (condAsOp)
  {
    // The expression is an operator. Must  make sure the eventual
    // operator is a boolean
    switch (condAsOp->getOp())
    {
    case NUOp::eUnBitNeg:
    case NUOp::eBiPlus:                
    case NUOp::eBiMinus:               
    case NUOp::eBiSMult:                
    case NUOp::eBiSDiv:                 
    case NUOp::eBiSMod:                 
    case NUOp::eBiUMult:                
    case NUOp::eBiUDiv:                 
    case NUOp::eBiUMod:                 
    case NUOp::eBiBitAnd:              
    case NUOp::eBiBitOr:               
    case NUOp::eBiBitXor:              
    case NUOp::eBiRshift:              
    case NUOp::eBiLshift:              
    case NUOp::eBiRshiftArith:              
    case NUOp::eBiLshiftArith:              
    case NUOp::eNaConcat:
      cond = buildCondition(cond);
      break;
    default:
      break;
    }
  }
#endif

  cond->resize(1);

  NUStmtList then_stmts;
  if (veIfGetThenPart(ve_if) != 0) {
    context->pushStmtList(&then_stmts);
    temp_err_code = stmt(veIfGetThenPart(ve_if), context);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    context->popStmtList();
  }

  NUStmtList else_stmts;
  if (veIfGetElsePart(ve_if) != 0) {
    context->pushStmtList(&else_stmts);
    temp_err_code = stmt(veIfGetElsePart(ve_if), context);
    if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
    context->popStmtList();
  }

  *the_if = new NUIf(cond, then_stmts, else_stmts, context->getNetRefFactory(), false, loc);

  return err_code;
}


Populate::ErrorCode VerilogPopulate::forStmt(veNode ve_for,
                                             LFContext *context,
                                             NUFor **the_for)
{
  SourceLocator loc = locator(ve_for);
  ErrorCode err_code = eSuccess;
  *the_for = 0;

  // handle the initial statement
  // setup a list to catch any task enables that might be created
  NUStmtList initial_stmts;
  context->pushStmtList(&initial_stmts);
  NUBlockingAssign * initial = 0;
  ErrorCode temp_err_code = blockingAssign(veForGetInitial(ve_for), context, &initial);
  context->popStmtList();
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  // now put the initial assignment at the end of the initial statements
  if ( initial ) {
    initial_stmts.push_back(initial);
  }


  // handle the condition expression, including any extra statements created by a function->task conversion
  NUExpr *condition = 0;
  NUStmtList extra_condition_stmts;
  context->pushStmtList(&extra_condition_stmts);
  temp_err_code = expr(veForGetCondition(ve_for), context, context->getModule(), 1, &condition);
  context->popStmtList();
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  condition = buildCondition(condition);

  // handle the advance statement
  NUStmtList advance_stmts;
  context->pushStmtList(&advance_stmts);
  NUBlockingAssign * advance = 0;
  temp_err_code = blockingAssign(veForGetStep(ve_for), context, &advance);
  context->popStmtList();
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  if ( advance ) {
    advance_stmts.push_back(advance);
  }

  NUStmtList body_stmts;
  if (veForGetStmtPart(ve_for) != 0) {
    context->pushStmtList(&body_stmts);
    temp_err_code = stmt(veForGetStmtPart(ve_for), context);
    context->popStmtList();
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  if ( not extra_condition_stmts.empty() ){
    // before we create the NUFor, if there are any extra_condition_stmts then
    // make a separate copy of them, and put one copy at the end of the
    // initial_stmts and the other copy at the end of the advance_stmts lists.
    // This allows us to handle function calls within the condition expression properly.
    NUStmtList copy_extra_condition_stmts;
    CopyContext copy_context( context->getDeclarationScope(), NULL) ;
    deepCopyStmtList(extra_condition_stmts, &copy_extra_condition_stmts, copy_context, NULL);
    initial_stmts.insert(initial_stmts.end(),extra_condition_stmts.begin(),extra_condition_stmts.end());
    advance_stmts.insert(advance_stmts.end(),copy_extra_condition_stmts.begin(),copy_extra_condition_stmts.end());
  }

  *the_for = new NUFor(initial_stmts, condition, advance_stmts, body_stmts, context->getNetRefFactory(), false, loc);

  return err_code;
}


// We translate while stmt into for stmt with no initial statements
// and no advance statements.
Populate::ErrorCode VerilogPopulate::whileStmt(veNode ve_while,
                                               LFContext *context,
                                               NUFor **the_while)
{
  SourceLocator loc = locator(ve_while);
  ErrorCode err_code = eSuccess;
  *the_while = 0;

  NUExpr *condition = 0;
  ErrorCode temp_err_code = expr(veWhileGetCondition(ve_while), context, context->getModule(), 0, &condition);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  UInt32 cWidth = condition->determineBitSize ();
  if (cWidth != 1)
    condition = new NUBinaryOp (NUOp::eBiNeq, condition,
                                NUConst::create ( condition->isSignedResult (),
                                                  0, cWidth,
                                                  condition->getLoc ()),
                                condition->getLoc ());
  condition->resize (1);
                                                  

  NUStmtList body_stmts;
  if (veWhileGetStatement(ve_while) != 0) {
    context->pushStmtList(&body_stmts);
    temp_err_code = stmt(veWhileGetStatement(ve_while), context);
    context->popStmtList();
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  NUStmtList initial_stmts;
  NUStmtList advance_stmts;
  *the_while = new NUFor(initial_stmts, condition, advance_stmts, body_stmts, context->getNetRefFactory(), false, loc);

  return err_code;
}


// We translate repeat stmt into for stmt and create a temporary
// for the counter.
Populate::ErrorCode VerilogPopulate::repeatStmt(veNode ve_repeat,
                                                LFContext *context,
                                                NUFor **the_repeat)
{
  SourceLocator loc = locator(ve_repeat);
  ErrorCode err_code = eSuccess;
  *the_repeat = 0;


  // Initial stmts - assign repeat count to the temporary counter.
  // Create a stmt list in case there is a function call in the
  // repeat count which gets hoisted.
  NUStmtList initial_stmts;
  context->pushStmtList(&initial_stmts);

  NUExpr *count = 0;
  ErrorCode temp_err_code = expr(veRepeatGetCondition(ve_repeat), context, context->getModule(), 0, &count);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }
  if (count->determineBitSize() > 32) {
    context->getMsgContext()->TooBigRepeatCount(&loc);
  }

  NUScope *scope = context->getBlockScope();
  StringAtom *name = scope->gensym("repeat");
  NUNet *counter_temp = scope->createTempNet(name, 32, false /* unsigned */, loc);
  NULvalue *lhs = new NUIdentLvalue(counter_temp, loc);
  NUBlockingAssign *initial_assign = new NUBlockingAssign(lhs, count, false, loc);
  context->addStmt(initial_assign);

  context->popStmtList();


  // Test: counter > 0
  NUExpr *constant_zero = NUConst::create (false, 0u, 32, loc);
  NUExpr *counter_fetch = new NUIdentRvalue(counter_temp, loc);
  NUExpr *condition = new NUBinaryOp(NUOp::eBiUGtr, counter_fetch, constant_zero, loc);
  condition->resize(1);


  // Body.
  NUStmtList body_stmts;
  if (veRepeatGetStatement(ve_repeat) != 0) {
    context->pushStmtList(&body_stmts);
    temp_err_code = stmt(veRepeatGetStatement(ve_repeat), context);
    context->popStmtList();
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }


  // Advance - decrement the counter.
  NUStmtList advance_stmts;
  NUExpr *constant_one = NUConst::create (false, 1u, 32, loc);
  counter_fetch = new NUIdentRvalue(counter_temp, loc);
  NUExpr *decrement = new NUBinaryOp(NUOp::eBiMinus, counter_fetch, constant_one, loc);
  lhs = new NUIdentLvalue(counter_temp, loc);
  NUBlockingAssign *advance_assign = new NUBlockingAssign(lhs, decrement, false, loc);
  advance_stmts.push_back(advance_assign);

  *the_repeat = new NUFor(initial_stmts, condition, advance_stmts, body_stmts, context->getNetRefFactory(), false, loc);

  return err_code;
}


/*
 * Do stmts a little bit funky; instead of returning them, we store them in
 * the passed-around context.  Do this for a few reasons:  we are squashing
 * nested blocks as we encounter them, and the event expression thing is funky.
 */
Populate::ErrorCode VerilogPopulate::stmt(veNode ve_stmt, LFContext *context,
                                          NUBlock* currentBlock, bool *isWildCard)
{
  ErrorCode err_code = eSuccess;
  NUStmt *the_stmt = 0;
  
  switch (veNodeGetObjType(ve_stmt)) {
  case VE_ASSIGN:       // needed for UDP initial blocks test/udp/UDP_DFF.v
  case VE_BLOCK_ASSIGN:
  {
    NUBlockingAssign* ass_stmt;
    err_code = blockingAssign(ve_stmt, context, &ass_stmt);
    the_stmt = ass_stmt;
    break;
  }

  case VE_NON_BLOCK_ASSIGN:
  {
    NUNonBlockingAssign* ass_stmt;
    err_code = nonBlockingAssign(ve_stmt, context, &ass_stmt);
    the_stmt = ass_stmt;
    break;
  }

  case VE_DELAY_OR_EVENT:
    {
      
      ErrorCode temp_err_code = delayOrEventControl(veDelOrEventControlGetTimingControlExpr(ve_stmt), context, isWildCard);
      if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
      {
        return temp_err_code;
      }
      if (veDelOrEventControlGetStatement(ve_stmt) != 0) {
	temp_err_code = stmt(veDelOrEventControlGetStatement(ve_stmt), context,
                             currentBlock);
        if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
        {
          return temp_err_code;
        }
      }
    }
    break;

  case VE_IF:
  {
    NUIf* if_stmt;
    err_code = ifStmt(ve_stmt, context, &if_stmt);
    the_stmt = if_stmt;
    break;
  }

  case VE_FOR:
  {
    NUFor* for_stmt;
    err_code = forStmt(ve_stmt, context, &for_stmt);
    the_stmt = for_stmt;
    break;
  }

  case VE_WHILE:
  {
    NUFor* while_stmt;

    err_code = whileStmt(ve_stmt, context, &while_stmt);
    the_stmt = while_stmt;
    break;
  }

  case VE_REPEAT:
  {
    NUFor* repeat_stmt;
    err_code = repeatStmt(ve_stmt, context, &repeat_stmt);
    the_stmt = repeat_stmt;
    break;
  }

  case VE_SEQ_BLOCK:
    err_code = sequentialBlock(ve_stmt, context, currentBlock);
    break;

  case VE_SYS_TASK_ENABLE:
  case VE_SYS_FUNC_CALL:	// Special catchall here.
    err_code = sysTask(ve_stmt, context, &the_stmt);
    break;

  case VE_TASK_ENABLE:
    err_code = taskEnable(ve_stmt, context);
    break;

  case VE_CASEX:
  case VE_CASE:
  case VE_CASEZ:
  {
    NUCase* case_stmt;
    err_code = caseStmt(ve_stmt, context, &case_stmt);
    the_stmt = case_stmt;
    break;
  }

  case VE_DISABLE:
    err_code = disable(ve_stmt, context, &the_stmt);
    break;

  case VE_FORCE:
  case VE_RELEASE: {
    SourceLocator loc = locator(ve_stmt);
    mMsgContext->UnsupportedAndIgnoredStatement(&loc, gGetObjTypeStr(veNodeGetObjType(ve_stmt)));
    break;
  }

  default:
    {
      SourceLocator loc = locator(ve_stmt);
      POPULATE_FAILURE(mMsgContext->UnsupportedStatement(&loc, gGetObjTypeStr(veNodeGetObjType(ve_stmt))), &err_code);
      return err_code;
    }
    break;
  } // switch

  if (the_stmt) {
    context->addStmt(the_stmt);
  }

  return err_code;
}


/*!
 * Traverse the cheetah hierarchical reference resolutions, make sure
 * they are all the same.  Create a StringAtom for the path and task
 * name.
 * \param looking_for_tasks if true then this call is for a task enable,
 * else the call is for a function call that was converted to a task enable.
 */
Populate::ErrorCode VerilogPopulate::taskEnableHierRefResolve(veNode ve_scopevar,
                                                              LFContext *,
                                                              bool looking_for_tasks,
                                                              AtomArray* hier_path,
                                                              StringAtom **task)
{
  ErrorCode err_code = eSuccess;
  ErrorCode temp_err_code = eSuccess;

  SourceLocator loc = locator(ve_scopevar);

  veNode resolved_task_node = 0;

  CheetahList ve_start_iter(veScopeVariableGetStartObjectList(ve_scopevar));
  if (ve_start_iter) {
    veNode ve_start_node;
    while ((ve_start_node = VeListGetNextNode(ve_start_iter)) != NULL) {
      veNode ve_obj_node = veScopeVariableGetUniqueParentObject(ve_scopevar, ve_start_node);

      if (not resolved_task_node) {
	resolved_task_node = ve_obj_node;
      } else {
	if (resolved_task_node != ve_obj_node) {
          if ( looking_for_tasks ) {
            POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical task enable resolves to different tasks in different instantiations"), &err_code);
          } else {
            POPULATE_FAILURE(mMsgContext->CheetahConsistency(&loc, "Hierarchical function call resolves to different functions in different instantiations"), &err_code);
          }
	  return err_code;
	}
      }
    }
  }

  temp_err_code = scopeVariableToPath(ve_scopevar, hier_path);
  if ( errorCodeSaysReturnNowWhenFailPopulate(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  *task = (*hier_path)[hier_path->size() - 1];

  return err_code;
} // Populate::ErrorCode VerilogPopulate::taskEnableHierRefResolve


Populate::ErrorCode VerilogPopulate::taskEnable(veNode ve_task_enable,
                                                LFContext *context)
{
  SourceLocator loc = locator(ve_task_enable);
  ErrorCode err_code = eSuccess;

  veNode ve_task_node = veTaskEnableGetTask(ve_task_enable);

  veNode ve_task = 0;

  // Detect cross-hierarchy task enables.
  if (veNodeIsA(ve_task_node, VE_SCOPEVARIABLE)) {
    if (not mAllowHierRefs) {
      UtString name_buf;
      mTicProtectedNameMgr->getVisibleName(veScopeVariableGetParentObject(ve_task_node), &name_buf);
      POPULATE_FAILURE(mMsgContext->OutOfScopeTask(&loc, name_buf.c_str()), &err_code);
      return err_code;
    }

    AtomArray path;
    StringAtom *task_name = 0;
    ErrorCode temp_err_code = taskEnableHierRefResolve(ve_task_node, context, true, &path, &task_name);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    ve_task = veScopeVariableGetParentObject(ve_task_node);
    NUTaskEnableHierRef *task_enable = new NUTaskEnableHierRef(path, task_name, 
                                                               context->getDeclarationScope(), 
                                                               context->getNetRefFactory(), 
                                                               false, 
                                                               loc);
    context->addStmt(task_enable);

    // We will create the arg connections later, after we can resolve this task.
    temp_err_code = addTaskHierRefToResolve(ve_task_enable, NULL, task_enable,
                                            context->getDeclarationScope(), context->getBlockScope(), NULL);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }

    return err_code;
  } else {
    ve_task = veNamedObjectUseGetParent(ve_task_node);
  }

  NUTask *task = 0;
  ErrorCode temp_err_code = lookupTask(context->getModule(), ve_task, &task);
  if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  if (task == 0) {
    UtString taskName_buf;
    mTicProtectedNameMgr->getVisibleName(ve_task, &taskName_buf);

    temp_err_code = tf(ve_task, taskName_buf.c_str(), context, &task);
    if ( errorCodeSaysReturnNowWhenFailPopulate( temp_err_code, &err_code ) )
    {
      return temp_err_code;
    }
  }

  NUTFArgConnectionVector arg_conns;
  temp_err_code = tfArgConnections(ve_task_enable, task, context, &arg_conns, NULL);
  if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) )
  {
    return temp_err_code;
  }

  NUModule* mod = context->getModule();
  NUTaskEnable *task_enable = new NUTaskEnable(task, mod,
                                               context->getNetRefFactory(), false, loc);
  context->addStmt(task_enable); // add the task enable stmt after
                                 // it's args were processed in case there was a
                                 // function call.

  task_enable->setArgConnections(arg_conns);

  return err_code;
}




// connect formal to actuals for function call or task enable, \a actual_net
// is the extra output of task created when a function is converted to a task.
Populate::ErrorCode VerilogPopulate::tfArgConnections(veNode ve_task_enable_or_function_call,
                                                      NUTask *task,
                                                      LFContext *context,
                                                      NUTFArgConnectionVector *arg_conns,
                                                      NUNet * actual_net)
{
  ErrorCode err_code = eSuccess;
  bool processing_task_enable = (actual_net == NULL);

  CheetahList ve_arg_iter;

  if ( processing_task_enable ) {
    ve_arg_iter = veTaskEnableGetExprList(ve_task_enable_or_function_call);
  } else {
    ve_arg_iter = veFuncCallGetExprList(ve_task_enable_or_function_call);
    //note: verilog LRM specifies that all functions have at least one input argument
  }
  if (ve_arg_iter) {
    int idx = 0;
    if ( not processing_task_enable ) {
      // this must be a function call, so add the extra output
      NUNet* formal_net = task->getArg(0); // arg 0 is the output
      NU_ASSERT (actual_net, task);
      NU_ASSERT (formal_net, task);

      // first make connection for the output of the task that was created for
      // this function
      SourceLocator loc = locator(ve_task_enable_or_function_call);

      NULvalue *actual_lvalue = new NUIdentLvalue(actual_net, loc);
      actual_lvalue->resize (); // ?
      NUTFArgConnection *arg_conn = new NUTFArgConnectionOutput(actual_lvalue, formal_net, task, loc);
      arg_conns->push_back(arg_conn);

      // now continue to connect up the arguments of this task formally known as a function
      ++idx;   // skip over the first formal argument (which is the output of the function)
    }
    while (veNode ve_arg = VeListGetNextNode(ve_arg_iter)) {
      NUNet *formal_net = task->getArg(idx);
      NUTFArgConnection *arg_conn = 0;
      ErrorCode temp_err_code = tfArgConnection(ve_arg, formal_net, task, context, &arg_conn);
      if ( errorCodeSaysReturnNowOnlyWithFatal(temp_err_code, &err_code ) ) {
        return temp_err_code;
      }
      if (arg_conn) {
        arg_conns->push_back(arg_conn);
      }
      ++idx;
    }
  }
  return err_code;
}


