#ifndef INITIALBLOCKCLEANUP_H_
#define INITIALBLOCKCLEANUP_H_

#include "nucleus/Nucleus.h"
#include "nucleus/NUNetRefSet.h"

class MsgContext;
class IODBNucleus;
class ArgProc;

/*!
  \file
  Declaration of initial block cleanup package.
 */

//! Class which removes non-constant assigns from initial blocks.
/*!
 * We only handle constant assigns in initial blocks; we do not allow
 * any other drivers to fanin to initial blocks.
 *
 * Detect when there are non-constant assigns in initial blocks, issue
 * warnings, and remove the assign.
 *
 * This analysis is performed by walking through the statements in
 * the initial blocks.  For each statement, we test that it either
 * uses constants or nets which have been def'd by constants within
 * the block.  The set of nets which have known-constant defs
 * are kept as the traversal occurs.
 *
 * This analysis can be fooled, but is pessimistic; we can
 * allow false positives, but not false negatives.
 */
class InitialBlockCleanup
{
public:
  //! constructor
  /*!
    \param netref_factory Net ref factory
    \param iodb IODB
    \param msg_ctx Context to issue error messages.
   */
  InitialBlockCleanup(NUNetRefFactory *netref_factory,
		      IODBNucleus *iodb,
                      ArgProc* args,
		      MsgContext *msg_ctx);

  //! destructor
  ~InitialBlockCleanup();

  //! Analyze this module.
  void module(NUModule *module);

private:
  //! Hide copy and assign constructors.
  InitialBlockCleanup(const InitialBlockCleanup&);
  InitialBlockCleanup& operator=(const InitialBlockCleanup&);

  //! Analyze this initial block.
  /*!
   * \param block Block to be analyzed
   * \param extra_const_net a net that should be considered a constant.
   *  can be NULL.
   *
   * \return true if any statements were removed, false otherwise.
   */
  bool initialBlock(NUInitialBlock *block, NUNet *extra_const_net);

  //! Analyze this block scope
  /*!
   \return true if any statements were removed, false otherwise.
   */
  bool blockScope(NUBlockScope *block);

  //! Analyze this statement list.
  /*!
   \return true if any statements were removed, false otherwise.
   */
  bool stmtList(NUStmtList &stmts);

  //! Analyze this statement (recursive)
  /*!
   \param remove Set to true if this statement needs to be removed, false otherwise
   \return true if any statements were removed, false otherwise.
   */
  bool stmt(NUStmt *stmt, bool &remove);

  //! Return true if the given statement should be removed.
  bool handleStmt(NUStmt *stmt);

  //! Return true if the statement uses non-constant nets
  bool stmtIsNonConstant(NUStmt *stmt);

  //! Return true if any of the net refs in the set are non-constant
  bool usesAreNonConstant(NUNetRefSet &uses);

  //! Analyze this if statement.
  /*!
   \param remove Set to true if this statement needs to be removed, false otherwise
   \return true if any statements were removed, false otherwise.
   */
  bool ifStmt(NUIf *stmt, bool &remove);

  //! Analyze this case statement.
  /*!
   \param remove Set to true if this statement needs to be removed, false otherwise
   \return true if any statements were removed, false otherwise.
   */
  bool caseStmt(NUCase *stmt, bool &remove);

  //! Analyze this for statement.
  /*!
   \param remove Set to true if this statement needs to be removed, false otherwise
   \return true if any statements were removed, false otherwise.
   */
  bool forStmt(NUFor *stmt, bool &remove);

  //! Stack of Set of known-constant nets, for statement recursion
  UtStack<NUNetRefSet> mKnownConstSetStack;

  //! Netref Factory
  NUNetRefFactory *mNetRefFactory;

  //! IODB
  IODBNucleus *mIODB;

  //! Command-line args
  ArgProc* mArgs;

  //! Message context
  MsgContext *mMsgContext;
};

#endif
