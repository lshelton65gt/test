
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  Calculates the position of the most significant 1
                      bit from the left side (closer to 0).

 -- ** Input       :  A           - data input

 -- ** Output      :  INDEX       - the index of the most significant bit
                                    starting from the left side

 -- ** Param       :  A_width     - word length of A
                      INDEX_width - word length of output INDEX
                                    (>= ceil(log[A_width])

****************************************************************************/

module DW01_prienc_carbon (A, INDEX);

parameter A_width = 32;
parameter INDEX_width = 5;

input [A_width - 1 : 0] A;

output [INDEX_width - 1 : 0] INDEX;
reg [INDEX_width - 1 : 0] INDEX;

`protect

integer i;

always @(A) begin 
    INDEX = 0;
    begin : loop
        for (i = A_width - 1; i >= 0; i = i - 1) begin
            if (A[i] == 1'b1) begin
                INDEX = i + 1;
                disable loop;
            end
        end
    end
end

`endprotect

endmodule
