
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  This modules ia a parameterized, synchronous,
                      three-port static RAM consisting of one write port
                      and two read ports.

 -- ** Input       :  clk          - clock
                      rst_n        - reset, active low
                      cs_n         - chip select, active low
                      wr_n         - write enable, active low
                      rd1_addr     - read 1 address bus
                      rd2_addr     - read 2 address bus
                      wr_addr      - write address bus
                      data_in      - input data bus

 -- ** Output      :  data_rd1_out - output data bus for read 1
                      data_rd2_out - output data bus for read 2

 -- ** Param       :  data_width - word length of data_in and data_out buses
                      depth      - number of words in the memory array
                      rst_mode   - determines the reset methodology:
                                   0 = rst_n asynchronously initializes the RAM
                                   1 = rst_n synchronously initializes the RAM

****************************************************************************/

module DW_ram_2r_w_s_dff_carbon (clk, rst_n, cs_n, wr_n, rd1_addr, rd2_addr, wr_addr, data_in, data_rd1_out, data_rd2_out);

parameter data_width = 4;
parameter depth = 8;
parameter rst_mode = 0;

`define addr_width ((depth>16) ? ((depth>64) ? ((depth>128) ? 8 : 7) : \
                   ((depth>32) ? 6 : 5)) : ((depth>4) ? \
                   ((depth>8) ? 4 : 3) : ((depth>2) ? 2 : 1)))

input [data_width - 1 : 0] data_in;
input [`addr_width - 1 : 0] rd1_addr;
input [`addr_width - 1 : 0] rd2_addr;
input [`addr_width - 1 : 0] wr_addr;
input wr_n;
input rst_n;
input cs_n;
input clk;
wire [data_width - 1 : 0] data_in;

output [data_width - 1 : 0] data_rd1_out; 
output [data_width - 1 : 0] data_rd2_out;

`protect

reg [data_width - 1 : 0] mem[depth - 1 : 0];
integer i;

assign data_rd1_out = (rd1_addr >= depth)? {data_width{1'b0}} : mem[rd1_addr];

assign data_rd2_out = (rd2_addr >= depth)? {data_width{1'b0}} : mem[rd2_addr];

wire a_rst_n;
wire s_rst_n;

assign a_rst_n = (rst_mode == 0) ? rst_n : 1'b1;
assign s_rst_n = (rst_mode == 1) ? rst_n : 1'b1;

always @ (posedge clk or negedge a_rst_n) begin 
    if(a_rst_n == 1'b0) begin
        for(i = 0; i < depth; i = i + 1) begin
            mem[i] = {data_width{1'b0}};
        end
    end else begin
        if(s_rst_n == 1'b0) begin
            for(i = 0; i < depth; i = i + 1) begin
                mem[i] = {data_width{1'b0}};
            end
        end else if ((wr_addr < depth) && ((wr_n | cs_n) !== 1'b1)) begin
                mem[wr_addr] = data_in;
        end
    end
end

`endprotect

`undef addr_width

endmodule 

