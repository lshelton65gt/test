
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  DW01_satrnd performs arithmetic, precision-handling 
                      rounding and saturation functions on its input din.

 -- ** Input       :  din     - input data
                      tc      - two's complement control (0/unsigned, 1/signed)
                      sat     - saturation enable flag
                                (0/no saturation, 1/enable saturation)  
                      rnd     - rounding enable flag
                                (0/no rounding, 1/enable rounding)

 -- ** Output      :  ov      - overflow status
                      dout    - output data

 -- ** Param       :  width   - word length of din (>= 2)
                      msb_out - dout MSB position after truncation of din MSBs
                                (width-1 >= msb_out > lsb_out)
                      lsb_out - dout LSB position after truncation of din LSBs
                                (msb_out > lsb_out >= 0)

****************************************************************************/
module DW01_satrnd_carbon (din, tc, sat, rnd, ov, dout);

parameter width = 16;
parameter msb_out = 14;
parameter lsb_out = 11;

input [width - 1 : 0] din;
input tc;
input rnd;
input sat;

output ov;
output [msb_out - lsb_out : 0] dout;
reg [msb_out - lsb_out : 0] dout;
reg ov;
   
`protect

`define max_signed (1'b1 << (msb_out-lsb_out)) - 1
`define min_signed 1'b1 << (msb_out-lsb_out)
`define max_unsigned (1'b1 << (msb_out-lsb_out + 1)) - 1

reg [msb_out - lsb_out : 0] dout_overflow;
reg [msb_out - lsb_out : 0] dout_round;
reg [msb_out : 0] din_in_LeftShiftByOne;
integer i;

always @ (din, tc, sat, rnd) begin
    ov = 0;
    dout_overflow = din[msb_out : lsb_out];
    if (msb_out < width - 1) begin
        if (tc == 1'b1) begin
            for (i = width - 1; i >= msb_out + 1 && ov != 1; i = i - 1) begin
                if (din[i] != din[msb_out]) begin
                    ov = 1;
                end
            end
            dout_overflow = (din[width - 1] == 1'b0) ? `max_signed
                : `min_signed;
        end else begin
            dout_overflow = `max_unsigned;
            for(i = width + 1; i >= msb_out + 1 && ov != 1; i = i - 1) begin
                if (din[i] == 1'b1) begin
                    ov = 1;
                end
            end
        end
    end
    if (lsb_out > 0 && rnd == 1'b1) begin
        din_in_LeftShiftByOne = din << 1;
        if ( ((tc==1'b1 && din[msb_out : lsb_out] == `max_signed) ||
            (tc==1'b0 && din[msb_out : lsb_out] == `max_unsigned)) &&
            din[lsb_out - 1] == 1'b1 && rnd == 1'b1 ) begin
            ov = 1;
        end
        dout_round = (din[lsb_out - 1] == 1'b1) ? din[msb_out : lsb_out] + 1 : 
            din[msb_out : lsb_out];
    end else begin
        dout_round = din[msb_out : lsb_out];
    end
    dout <= (sat == 1'b0) ? dout_round : (ov == 1) ? dout_overflow : dout_round;
end
  
`undef max_signed
`undef min_signed
`undef max_unsigned

`endprotect

endmodule
