
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This is a dual independent clock FIFO RAM controller.
                     It is designed to interface with a dual-port
                     synchronous RAM.
                     The RAM must have:
                       - a synchronous write port and an asynchronous
                         read port, or
                       - a synchronous write port and a synchronous read
                         port (clocks must be independent).

 -- ** Input       : clk_push        - input clock for push interface
                     clk_pop         - input clock for pop interface
                     rst_n           - reset input, active low:
                                       0 = asynchronous reset
                                       1 = synchronous reset
                     push_req_n      - FIFO push request, active low
                     pop_req_n       - FIFO pop request, active low
                     test            - active high, test input control for
                                       inserting scan test lock-up latches

 -- ** Output      : we_n            - write enable output for write port of
                                       RAM, active low
                     push_empty      - FIFO empty output flag synchronous to
                                       clk_push, active high
                     push_ae         - FIFO almost empty output flag
                                       synchronous to clk_push, active high
                                       (determined by push_ae_lvl parameter)
                     push_hf         - FIFO almost full output flag
                                       synchronous to clk_push, active high
                                       (determined by push_af_lvl parameter)
                     push_full       - FIFO full output flag synchronous to
                                       clk_push, active high
                     push_error      - FIFO push error (overrun) output flag
                                       synchronous to clk_push, active high
                     pop_empty       - FIFO empty output flag synchronous
                                       to clk_pop, active high
                     pop_ae          - FIFO almost empty output flag
                                       synchronous to clk_pop, active high
                                       (determined by pop_ae_lvl parameter)
                     pop_hf          - FIFO half full output flag
                                       synchronous to clk_pop, active high
                     pop_af          - FIFO almost full output flag
                                       synchronous to clk_pop, active high
                                       (determined by pop_af_lvl parameter)
                     pop_full        - FIFO full output flag synchronous to
                                       clk_pop, active high
                     pop_error       - FIFO pop error (underrun) output flag
                                       synchronous to clk_pop, active high
                     wr_addr         - address output to write port of RAM
                     rd_addr         - address output to read port of RAM
                     push_word_count - words in FIFO (as perceived by the
                                       push/pop interface)
                     pop_word_count  - words in FIFO (as perceived by the
                                       push/pop interface)

 -- ** Param       : depth           - number of words that can be stored
                                       in FIFO
                     push_ae_lvl     - almost empty level for the push_ae
                                       output port (the number of words in the
                                       FIFO at or below which the push_ae flag
                                       is active)
                     push_af_lvl     - almost full level for the push_af output
                                       port (the number of empty memory
                                       locations in the FIFO at which the
                                       push_af flag is active)
                     pop_ae_lvl      - almost empty level for the pop_ae
                                       output port (the number of words in the
                                       FIFO at or below which the pop_ae flag
                                       is active)
                     pop_af_lvl      - almost empty level for the pop_ae output
                                       port (the number of words in the FIFO
                                       at or below which the pop_ae flag
                                       is active)
                     err_mode        - error mode:
                                       0 = stays active until reset [latched]
                                       1 = active only as long as error
                                           condition exists [unlatched]
                     push_sync       - push flag synchronization mode:
                                       1 = single register synchronization
                                           from pop pointer
                                       2 = double register
                                       3 = triple register
                    pop_sync         - pop flag synchronization mode:
                                       1 = single register synchronization
                                           from push pointer
                                       2 = double register
                                       3 = triple register
                     rst_mode        - reset mode:
                                       0 = asynchronous reset
                                       1 = synchronous reset
                     tst_mode        - test mode:
                                       0 = test input not connected

                     
****************************************************************************/

module DW_fifoctl_s2_sf_carbon(clk_push, clk_pop, rst_n, push_req_n, pop_req_n, we_n, push_empty, push_ae, push_hf, push_af, push_full, push_error, pop_empty, pop_ae, pop_hf, pop_af, pop_full, pop_error, wr_addr, rd_addr, push_word_count, pop_word_count, test);

parameter depth = 4;
parameter push_ae_lvl = 1;
parameter push_af_lvl = 1;
parameter pop_ae_lvl = 1;
parameter pop_af_lvl = 1;
parameter err_mode = 0;
parameter push_sync = 1;
parameter pop_sync = 1;
parameter rst_mode = 0;
parameter tst_mode = 0;

`define _addr_width ((depth>65536) ? ((depth>1048576) ? ((depth>4194304) ? \
                    ((depth>8388608) ? 24 : 23) : \
                    ((depth>2097152) ? 22 : 21)) : ((depth>262144) ? \
                    ((depth>524288) ? 20 : 19) : \
                    ((depth>131072) ? 18 : 17))) : ((depth>256) ? \
                    ((depth>4096) ? ((depth>16384) ? \
                    ((depth>32768) ? 16 : 15) : ((depth>8192) ? 14 : 13)) : \
                    ((depth>1024) ? ((depth>2048) ? 12 : 11) : \
                    ((depth>512) ? 10 : 9))) : ((depth>16) ? ((depth>64) ? \
                    ((depth>128) ? 8 : 7) : ((depth>32) ? 6 : 5)) : \
                    ((depth>4) ? ((depth>8) ? 4 : 3) : ((depth>2) ? 2 : 1))))) 

`define _count_width ((depth+1<16777216) ? ((depth+1>65536) ? \
                     ((depth+1>1048576) ? ((depth+1>4194304) ? \
                     ((depth+1>8388608) ? 24 : 23) : \
                     ((depth+1>2097152) ? 22 : 21)) : \
                     ((depth+1>262144) ? ((depth+1>524288) ? 20 : 19) : \
                     ((depth+1>131072) ? 18 : 17))) : \
                     ((depth+1>256) ? ((depth+1>4096) ? ((depth+1>16384) ? \
                     ((depth+1>32768) ? 16 : 15) : \
                     ((depth+1>8192) ? 14 : 13)) : ((depth+1>1024) ? \
                     ((depth+1>2048) ? 12 : 11) : \
                     ((depth+1>512) ? 10 : 9))) : ((depth+1>16) ? \
                     ((depth+1>64) ? ((depth+1>128) ? 8 : 7) : \
                     ((depth+1>32) ? 6 : 5)) : ((depth+1>4) ? \
                     ((depth+1>8) ? 4 : 3) : ((depth+1>2) ? 2 : 1))))) : 25)

`define _real_depth (((1 << `_addr_width) == depth) ? \
                    (depth * 2) : (depth + 2 - (depth % 2)))

input clk_push, clk_pop;
input rst_n;
input push_req_n, pop_req_n;
input test;
output we_n, push_empty, push_ae, push_hf, push_af, push_full;
output push_error, pop_empty, pop_ae, pop_hf, pop_af, pop_full, pop_error;
output [`_addr_width - 1 : 0] wr_addr, rd_addr;
output [`_count_width - 1 : 0] push_word_count, pop_word_count;

wire pop_ae, pop_hf, pop_af, pop_full;
wire[`_count_width - 1 : 0] push_word_count, pop_word_count;
wire pop_empty, push_full;

reg push_error, pop_error;
wire push_af, push_hf, push_ae, push_empty;

`protect

wire a_rst_n;
wire s_rst_n;

wire next_push_error_int; 
wire next_pop_error_int; 

integer wd_count;
wire [31 : 0] next_wd_count;
integer rd_count;
wire [31 : 0] next_rd_count;
integer wr_addr_int;
wire [31 : 0] c_wr_addr_int;
reg [31 : 0] next_wr_addr_int;
integer rd_addr_int;
wire [31 : 0] c_rd_addr_int;
reg [31 : 0] next_rd_addr_int;
integer wr_addr_ltch, rd_addr_ltch;
wire [31 : 0] wr_addr_smpl;
wire [31 : 0] rd_addr_smpl;
integer wcmp_addr, rcmp_addr;

always @(pop_req_n or rd_count or rd_addr_int) begin
    if(pop_req_n == 1'b0) begin
        if(rd_count > 0) begin
            next_rd_addr_int = (rd_addr_int + 1) % `_real_depth;
        end else begin
            next_rd_addr_int = rd_addr_int;
        end
    end else begin
        next_rd_addr_int = rd_addr_int;
    end
end

always @(push_req_n or wd_count or wr_addr_int) begin
    if(push_req_n == 1'b0) begin
        if(wd_count == depth) begin
            next_wr_addr_int = wr_addr_int;
        end else begin
            next_wr_addr_int = (wr_addr_int + 1) % `_real_depth;
        end
    end else begin
        next_wr_addr_int = wr_addr_int;
    end
end

assign next_wd_count = (next_wr_addr_int < wcmp_addr) ? 
    `_real_depth - (wcmp_addr - next_wr_addr_int) :
    next_wr_addr_int - wcmp_addr;

assign next_push_error_int = ((err_mode < 1) && (push_error != 1'b0)) ? 
    push_error : ((push_req_n == 1'b0) && (wd_count == depth)) ? 1'b1 : 1'b0;

assign a_rst_n = (rst_mode == 0) ? rst_n : 1'b1;
assign s_rst_n = (rst_mode == 1) ? rst_n : 1'b1;

always @(posedge clk_push or negedge a_rst_n)
begin : clk_push_registers
    integer sync_raddr1, sync_raddr2;
    if (a_rst_n == 1'b0) begin
        wr_addr_int <= 0;
        wd_count <= 0;
        push_error <= 1'b0;
        wcmp_addr <= 0;
        sync_raddr1 <= 0;
        sync_raddr2 <= 0;
    end else if(s_rst_n == 1'b0) begin
        wr_addr_int <= 0;
        wd_count <= 0;
        push_error <= 1'b0;
        wcmp_addr <= 0;
        sync_raddr1 <= 0;
        sync_raddr2 <= 0;
    end else begin
        wr_addr_int <= next_wr_addr_int;
        wd_count <= next_wd_count;
        push_error <= next_push_error_int;
        if (push_sync == 1) begin
            wcmp_addr <= rd_addr_smpl;
        end else begin
            sync_raddr1 <= rd_addr_smpl;
            if (push_sync == 2) begin
                wcmp_addr <= sync_raddr1;
            end else begin
                sync_raddr2 <= sync_raddr1;
                wcmp_addr <= sync_raddr2;
            end
        end
    end
end

assign push_empty = (wd_count == 0)? 1'b1 : 1'b0;
assign push_ae = (wd_count <= push_ae_lvl) ? 1'b1 : 1'b0;
assign push_hf = (wd_count < (depth+1)/2) ? 1'b0 : 1'b1;
assign push_af = (wd_count < (depth-push_af_lvl)) ? 1'b0 : 1'b1;
assign push_full = (wd_count != depth) ? 1'b0 : 1'b1;
assign push_word_count = wd_count[`_count_width : 0];

assign c_wr_addr_int = (clk_push == 0) ? wr_addr_int : c_wr_addr_int; 

always @ (negedge clk_push) begin : mk_wr_addr_ltch
    wr_addr_ltch <= (clk_push == 1'b0) ? c_wr_addr_int : wr_addr_ltch;
end 

assign wr_addr_smpl = (tst_mode == 0) ? 
    wr_addr_int : (test == 1'b0) ? wr_addr_int :
    (test == 1'b1) ? wr_addr_ltch : 0;

assign c_rd_addr_int = (clk_pop == 0) ? rd_addr_int : c_rd_addr_int; 

always @ (negedge clk_pop) begin : mk_rd_addr_ltch
    rd_addr_ltch <= (clk_pop == 1'b0) ? c_rd_addr_int : rd_addr_ltch;
end 

assign rd_addr_smpl = (tst_mode == 0) ? 
    rd_addr_int : (test == 1'b0) ? rd_addr_int : 
    (test == 1'b1) ? rd_addr_ltch : 0;

assign next_rd_count = (next_rd_addr_int > rcmp_addr) ? 
    `_real_depth - (next_rd_addr_int - rcmp_addr) :
    rcmp_addr - next_rd_addr_int;

assign next_pop_error_int = ((err_mode < 1) && (pop_error != 1'b0)) ?
        pop_error : ((pop_req_n == 1'b0) && (rd_count == 0)) ?
        1'b1 : ((rd_addr_int >= 0) && (rd_count >= 0)) ? 1'b0 : 1'b0;

always @ (posedge clk_pop or negedge a_rst_n)
begin : pop_clk_registers
    integer sync_waddr1, sync_waddr2;
    if (a_rst_n == 1'b0) begin
        rd_addr_int <= 0;
        rd_count <= 0;
        pop_error <= 1'b0;
        rcmp_addr <= 0;
        sync_waddr1 <= 0;
        sync_waddr2 <= 0;
    end else if(s_rst_n == 1'b0) begin
        rd_addr_int <= 0;
        rd_count <= 0;
        pop_error <= 1'b0;
        rcmp_addr <= 0;
        sync_waddr1 <= 0;
        sync_waddr2 <= 0;
    end else begin
        rd_addr_int <= next_rd_addr_int;
        rd_count <= next_rd_count;
        pop_error <= next_pop_error_int;
        if (pop_sync == 1) begin
            rcmp_addr <= wr_addr_smpl;
        end else begin
            sync_waddr1 <= wr_addr_smpl;
            if (pop_sync == 2) begin
                rcmp_addr <= sync_waddr1;
            end  else begin
                sync_waddr2 <= sync_waddr1;
                rcmp_addr <= sync_waddr2;
            end
        end	       
    end
end 

assign pop_empty = (rd_count == 0) ? 1'b1 : 1'b0;
assign pop_ae = (rd_count <= pop_ae_lvl) ? 1'b1 : 1'b0;
assign pop_hf = (rd_count < (depth+1)/2) ? 1'b0 : 1'b1;
assign pop_af = (rd_count < (depth-pop_af_lvl)) ? 1'b0 : 1'b1;
assign pop_full = (rd_count != depth) ? 1'b0 : 1'b1;
assign pop_word_count = rd_count[`_count_width : 0];

assign wr_addr = wr_addr_int[`_addr_width - 1 : 0];
assign rd_addr = rd_addr_int[`_addr_width - 1 : 0];
assign we_n = push_full | push_req_n;

`endprotect

`undef _addr_width
`undef _count_width
`undef _real_depth

endmodule

