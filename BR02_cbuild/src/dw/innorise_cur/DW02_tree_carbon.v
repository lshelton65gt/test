
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :   This module is Wallace-tree compressor. It is used
                       for example in building the Wallace-tree adder.

 -- ** Input       :  INPUT       - input vector

 -- ** Output      :  OUT0        - partial sum
                      OUT1        - partial sum

 -- ** Param       :  num_inputs  - number of inputs
                      input_width - word length of OUT0 and OUT1

****************************************************************************/

module DW02_tree_carbon(INPUT, OUT0, OUT1);

parameter num_inputs = 4;
parameter input_width = 8;

input [num_inputs * input_width - 1 : 0] INPUT;
output [input_width - 1 : 0] OUT0, OUT1;
reg [input_width - 1 : 0] OUT0, OUT1;

`protect

always @ (INPUT) begin : begin_point
        reg [input_width - 1 : 0] carry;
        reg [input_width - 1 : 0] in [0 : num_inputs - 1];
        integer size, i, j;
        for(i = 0; i < num_inputs; i = i + 1) begin
                in[i] = INPUT[i * input_width +: input_width];
        end
        for(size = num_inputs; size > 2; size = size - (size / 3)) begin
                for ( i = 0; i < (size / 3); i = i + 1 ) begin
                        carry = (in[i * 3] & in[i * 3 + 1]) |
                                (in[i * 3 + 1] & in[i * 3 + 2]) |
                                (in[i * 3] & in[i * 3 + 2]);
                        in[i * 2] = in[i * 3] ^
                                    in[i * 3 + 1] ^
                                    in[i * 3 + 2];
                        in[i * 2 + 1] = carry << 1;
                end
                if ( (size % 3) == 1 ) begin
                        in[2 * (size / 3)] = in[3 * (size / 3)];
                end else if ( (size % 3 ) == 2 ) begin
                        in[2 * (size / 3)] = in[3 * (size / 3)];
                        in[2 * (size / 3) + 1] = in[3 * (size / 3) + 1];
                end
        end
        OUT0 <= in[0];
        if (size > 1) begin
                OUT1 <= in[1];
        end else begin
                OUT1 <= {input_width{1'b0}};
        end
end

`endprotect

endmodule
