
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :   This module represents a universal multiplexer, which 
                       outputs the selected subrange of the input.

 -- ** Input       :   A         - input data, which subrange should be selected
                       SEL       - specifies the subrange 

 -- ** Output      :   MUX       - selected subrange of the input A

 -- ** Param       :   A_width   - word length of A
                       SEL_width - word length of SEL
                       MUX_width - A((SEL+1)*MUX_width-1 downto SEL*MUX_width)

****************************************************************************/

module DW01_mux_any_carbon(A, SEL, MUX);

parameter A_width = 32;
parameter SEL_width = 2;
parameter MUX_width = 8;

input [A_width - 1 : 0] A;
input [SEL_width - 1 : 0] SEL;

output [MUX_width - 1 : 0] MUX;
reg [MUX_width - 1 : 0] MUX; 

`protect

reg [A_width - 1 : 0] temp_buffer;

always @(A or SEL) begin
    temp_buffer = A;
    MUX = temp_buffer[SEL * MUX_width +: MUX_width];
end

`endprotect

endmodule
