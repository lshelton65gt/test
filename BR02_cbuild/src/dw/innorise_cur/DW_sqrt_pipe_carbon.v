
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module is a universal stallable square root generator.
                     It computes the square root of operand a with a latency
                     of num_stages - 1 clock cycles.
                                                                          1
 -- ** Input       : clk        - input clock
                     rst_n      - reset, active low (unused if rst_mode = 0)
                     en         - load enable (used only if stall_mode = 1)
                                  0 = stall
                                  1 = load
                     a          - radicand

 -- ** Output      : root       - square root of a

 -- ** Param       : width      - word length of a
                     num_stages - number of pipeline stages
                     stall_mode - stall mode (0 = non-stallable, 1 = stallable)
                     rst_mode   - reset mode:
                                  0 = no reset
                                  1 = asynchronous reset
                                  2 = ynchronous reset
                     tc_mode    - two's complement (0 = unsigned, 1 = signed)

****************************************************************************/

`include "DW_sqrt_carbon.v"

module DW_sqrt_pipe_carbon (clk, rst_n, en, a, root);

parameter width = 32;
parameter tc_mode = 0;
parameter num_stages = 2;
parameter stall_mode = 0;
parameter rst_mode = 0;
parameter op_iso_mode = 0;

input clk;
input rst_n;
input [width - 1 : 0] a;
input en;

output [((width + 1) / 2) - 1 : 0] root;

`protect

wire a_rst_n;
wire s_rst_n;

integer j, wp;

assign a_rst_n = (rst_mode == 1)? rst_n : 1'b1;
assign s_rst_n = (rst_mode == 2)? rst_n : 1'b1;

reg [width - 1 : 0] a_reg[num_stages - 1 : 0];

always @(posedge clk or negedge a_rst_n) begin
    if(a_rst_n == 1'b0) begin
        for(j = 0; j < num_stages - 1; j = j + 1) begin
                a_reg[j] = {width{1'b0}};
        end
        wp = 0;
    end else begin
        if(s_rst_n == 1'b0) begin
           for(j = 0; j < num_stages - 1; j = j + 1) begin
                   a_reg[j] = {width{1'b0}};
           end
           wp = 0;
        end else begin
            if (stall_mode != 0 && en == 1'b0) begin
                // Do nothing, leave old values
            end else if (stall_mode == 0 || en == 1'b1) begin
                a_reg[wp] = a;
                wp = (wp + 1 == num_stages - 1) ? 0 : wp + 1;
            end else begin
                   for(j = 0; j < num_stages - 1; j = j + 1) begin
                           a_reg[j] = {width{1'b0}};
                   end
            end
        end
    end
end

DW_sqrt_carbon  
U1(.a(a_reg[wp]), .root(root));

defparam U1.width = width;
defparam U1.tc_mode = tc_mode;

`endprotect

endmodule

