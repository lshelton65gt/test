
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  This module is a universal stallable pipelined multiplier.
                      It multiplies the operands a by b to produce a product
                      with a latency of num_stages - 1 clock cycles.
                                                                          1
 -- ** Input       :  clk     - input clock
                      rst_n   - reset, active low (unused if rst_mode = 0)
                      en      - load enable (used only if stall_mode = 1)
                                0 = stall
                                1 = load
                      tc      - two's complement control:
                                0 = unsigned
                                1 = signed
                      a       - multiplier
                      b       - multiplicand

 -- ** Output      :  product - product a * b

 -- ** Param       :  a_width - word length of a
                      b_width - word length of b

****************************************************************************/

`include "DW02_mult_carbon.v"

module DW_mult_pipe_carbon(clk, rst_n, en, tc, a, b, product);

parameter a_width = 16;
parameter b_width = 16;
parameter num_stages = 2;
parameter stall_mode = 0;
parameter rst_mode = 0;

input clk;
input rst_n;
input [a_width - 1 : 0] a;
input [b_width - 1 : 0] b;
input tc;
input en;

output [a_width + b_width - 1 : 0] product;

`protect

reg [a_width - 1 : 0] a_reg[num_stages - 1 : 0];
reg [b_width - 1 : 0] b_reg[num_stages - 1 : 0];
reg tc_reg[num_stages - 1 : 0];
integer j, wp;

wire a_rst_n;
wire s_rst_n;

assign a_rst_n = (rst_mode == 1)? rst_n : 1'b1;
assign s_rst_n = (rst_mode == 2)? rst_n : 1'b1;

always @(posedge clk or negedge a_rst_n) begin
    if(a_rst_n == 1'b0) begin
        for(j = 0; j < num_stages - 1; j = j + 1) begin
            a_reg[j] = {a_width{1'b0}};
            b_reg[j] = {b_width{1'b0}};
            tc_reg[j] = 1'b0;
        end
        wp = 0;
    end else begin
        if(s_rst_n == 1'b0) begin
            for(j = 0; j < num_stages - 1; j = j + 1) begin
                a_reg[j] = {a_width{1'b0}};
                b_reg[j] = {b_width{1'b0}};
                tc_reg[j] = 1'b0;
            end
            wp = 0;
        end else begin
            if (stall_mode != 0 && en == 1'b0) begin
                // Do nothing, leave old values
            end else if (stall_mode == 0 || en == 1'b1) begin
                a_reg[wp] = a;
                b_reg[wp] = b;
                tc_reg[wp] = tc;
                wp = (wp + 1 == num_stages - 1) ? 0 : wp + 1;
            end else begin
                for(j = 0; j < num_stages - 1; j = j + 1) begin
                    a_reg[j] = {a_width{1'b0}};
                    b_reg[j] = {b_width{1'b0}};
                    tc_reg[j] = 1'b0;
                end
            end
        end
    end
end

DW02_mult_carbon
U1 (.A(a_reg[wp]),
    .B(b_reg[wp]),
    .TC(tc_reg[wp]),
    .PRODUCT(product));

defparam U1.A_width = a_width;
defparam U1.B_width = b_width;

`endprotect

endmodule

