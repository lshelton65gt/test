
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  Gray code counter.

 -- ** Input       :  clk     - clock
                      rst_n   - asynchronous reset (active low)
                      init_n  - synchronous reset (active low)
                      load_n  - enable data load to counter (active low)
                      data    - counter input data
                      cen     - count enable (active high)

 -- ** Output      :  count   - gray coded counter output
 
 -- ** Param       :  width   - word length of input and output data (>= 1).

****************************************************************************/

module DW_cntr_gray_carbon(clk, rst_n, init_n, load_n, data, cen, count);

parameter width = 32;

input clk;
input rst_n;
input init_n;
input load_n;
input [width - 1 : 0] data;
input cen;
output [width - 1 : 0] count;
reg [width - 1 : 0] count;

`protect

reg [width : 0] bin;
integer i;

always @(posedge clk or negedge rst_n)
begin 
if(rst_n == 1'b0) begin
    count = {width{1'b0}};
end else if(init_n == 1'b0) begin 
    count = {width{1'b0}};
end else begin
    if (load_n == 1'b1) begin
        if (cen == 1'b1) begin
            bin[width] = 1'b0;
            for(i = width - 1; i >= 0; i = i - 1) begin
                bin[i] = count[i] ^ bin[i + 1];
            end
            count = bin[width - 1 : 0] + 1;
            count = count ^ (count >> 1);
        end
    end else begin
        count = data;
    end
end
end

`endprotect

endmodule

