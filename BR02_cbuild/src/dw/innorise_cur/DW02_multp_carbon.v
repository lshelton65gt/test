
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  Determines the partial product of the operands a and b.

 -- ** Input       :  a         - multiplier
                      b         - multiplicand
                      tc        - two's complement (0 = signed, 1 = unsigned)

 -- ** Output      :  out0      - partial product of a * b
                      out1      - partial product of a * b

 -- ** Param       :  a_width   - word length of a
                      b_width   - word length of b
                      out_width - word length of out0 and out1

****************************************************************************/

module DW02_multp_carbon(a, b, tc, out0, out1);

parameter a_width = 15;
parameter b_width = 15;
parameter out_width = 32;

input [a_width - 1 : 0] a;
input [b_width - 1 : 0] b;
input tc;
output [out_width - 1 : 0] out0, out1;
reg [out_width - 1 : 0] out0, out1;

reg [a_width + b_width : 0] in [0 : (a_width / 2) + 1]; // carbon ignoreSynthCheck

`protect

wire [a_width + 2 : 0] apadded;
wire [a_width + b_width : 0] bpadded;

assign apadded = {tc & a[a_width - 1], tc & a[a_width - 1], a, 1'b0};
assign bpadded = {{(a_width + 1){tc & b[b_width - 1]}}, b};

reg [a_width + b_width : 0] carry;
reg [a_width + b_width + 4 : 0] temp;
reg [a_width + 2 : 0] shift_a;
reg [2 : 0] bitgroup;
integer bit_pair, size, i;

always @ (apadded or bpadded) begin 
    in[0] = {(a_width + b_width + 1){1'b0}};
    for (bit_pair = 0; bit_pair < (a_width / 2) + 1;
    bit_pair = bit_pair + 1) begin
        shift_a = (apadded >> (bit_pair * 2));
        bitgroup = shift_a[2 : 0];
        case (bitgroup)
            3'b000, 3'b111 :
                temp = {(a_width + b_width + 1){1'b0}};
            3'b001, 3'b010 : temp = bpadded;
            3'b011 : temp = bpadded << 1;
            3'b100 : temp = (~(bpadded << 1) + 1);
            3'b101, 3'b110 : temp = ~bpadded + 1;
            default : temp = {(a_width + b_width + 1){1'b0}};
        endcase
        temp = temp << (2 * bit_pair);
        in[bit_pair + 1] = temp[a_width + b_width : 0];
    end
    for(size = ((a_width / 2) + 2); size > 2; size = size - (size / 3)) begin
        for ( i = 0; i < (size / 3); i = i + 1 ) begin
            carry = (in[i * 3] & in[i * 3 + 1]) |
                (in[i * 3 + 1] & in[i * 3 + 2]) |
                (in[i * 3] & in[i * 3 + 2]);
            in[i * 2] = in[i * 3] ^
                in[i * 3 + 1] ^
                in[i * 3 + 2];
            in[i * 2 + 1] = carry << 1;
        end
        if ( (size % 3) == 1 ) begin
            in[2 * (size / 3)] = in[3 * (size / 3)];
        end else if ( (size % 3 ) == 2 ) begin
            in[2 * (size / 3)] = in[3 * (size / 3)];
            in[2 * (size / 3) + 1] = in[3 * (size / 3) + 1];
        end
    end
    out0 <= {{out_width - (a_width + b_width + 1){1'b0}}, in[0]};
    if (size > 1) begin
        out1 <= {{(out_width - a_width - b_width - 1)
            {in[0][a_width + b_width] |
                in[1][a_width + b_width]}}, in[1]};
    end else begin
        out1 <= {{(out_width - a_width - b_width - 1)
            {in[0][a_width + b_width]}},
                {(a_width + b_width + 1){1'b0}}};
    end
end

`endprotect

endmodule
