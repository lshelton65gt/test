
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : Determines the partial products resulting from the
                     multiplication of of a by a.

 -- ** Input       : a     - multiplier
                     tc    - two's complement (0 = unsigned, 1 = signed)

 -- ** Output      : out0  - partial product of a * a
                     out1  - partial product of a * a

 -- ** Param       : width - word length of a (>= 1)

****************************************************************************/

module DW_squarep_carbon(a, tc, out0, out1);

parameter width = 16;

input [width - 1 : 0] a;
input tc;
output [2 * width - 1 : 0] out0, out1;
reg [2 * width - 1 : 0] out0, out1;

`protect

always @ (a or tc)
begin : squarep_begin
    reg [2 * width - 1 : 0] in [0 : width - 1];
    reg [2 * width - 1 : 0] carry;
    reg [width - 1 : 0] abs_a;
    integer i, size;
    abs_a = ( (tc & a[width - 1]) == 1'b0 ) ? a : (~a + 1'b1);
    for ( i = 0; i < width; i = i + 1 ) begin
        if ( abs_a[i] == 1'b1 )
            in[i] = {{width{1'b0}}, abs_a} << i;
        else
            in[i] = {2*width{1'b0}};
    end
    for( size = width; size > 2; size = size - (size / 3) ) begin
        for ( i = 0; i < (size / 3); i = i + 1 ) begin
            carry = in[i * 3] & in[i * 3 + 1] |
                in[i * 3 + 1] & in[i * 3 + 2] |
                in[i * 3 + 2] & in[i * 3];
            in[i * 2] = in[i * 3] ^ in[i * 3 + 1] ^ in[i * 3 + 2];
            in[i * 2 + 1] = carry << 1;
        end
        if ( (size % 3) == 1 ) begin
            in[2 * (size / 3)] = in[3 * (size / 3)];
        end else if ( (size % 3 ) == 2 ) begin
            in[2 * (size / 3)] = in[3 * (size / 3)];
            in[2 * (size / 3) + 1] = in[3 * (size / 3) + 1];
        end
    end
    out0 = in[0];
    if (size > 1) begin
        out1 = in[1];
    end else begin
        out1 = {width*2{1'b0}};
    end
end

`endprotect

endmodule

