
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This is a universal Cyclic Redundancy Check (CRC)
                     Polynomial Generator/Checker that provides data integrity
                     on data streams for various applications.

 -- ** Input       : clk        - input clock
                     rst_n      - asynchronous reset input, active low
                     init_n     - synchronous initialization control input,
                                  active low
                     enable     - Enable control input for all operations
                                  (other than reset and initialization),
                                  active high
                     drain      - drains control input, active high
                     ld_crc_n   - synchronous CRC register load control input,
                                  active low
                     data_in    - input data
                     crc_in     - input CRC value (to be loaded into the CRC
                                  register as commanded by the ld_crc_n
                                  control input)

 -- ** Output      : draining   - indicates that the CRC register is draining
                                  (inserting the CRC into the data stream)
                     drain_done - indicates that the CRC register has
                                  finished draining
                     crc_ok     - indicates a correct residual CRC value,
                                  active high
                     data_out   - output data
                     crc_out    - provides constant monitoring of the
                                  CRC register

 -- ** Param       : data_width - width of data_in and data_out
                                  (also the number of bits per clock)
                     poly_size  - size of the CRC polynomial
                     crc_cfg    - CRC initialization and insertion
                                  configuration
                     bit_order  - bit and byte order configuration
                     poly_coef0 - polynomial coefficients 0 through 15
                     poly_coef1 - polynomial coefficients 16 through 31
                     poly_coef2 - polynomial coefficients 32 through 47
                     poly_coef3 - polynomial coefficients 48 through 63

****************************************************************************/

module DW_crc_s_carbon(clk ,rst_n ,init_n ,enable ,drain ,ld_crc_n ,data_in ,crc_in , draining ,drain_done ,crc_ok ,data_out ,crc_out);

parameter data_width = 2; 
parameter poly_size = 8; 
parameter crc_cfg = 1; 
parameter bit_order = 0; 
parameter poly_coef0 = 11; 
parameter poly_coef1 = 0; 
parameter poly_coef2 = 0; 
parameter poly_coef3 = 0;
 
input clk, rst_n, init_n, enable, drain, ld_crc_n;
input [data_width-1 : 0] data_in;
input [poly_size-1 : 0] crc_in;

wire clk, rst_n, init_n, enable, drain, ld_crc_n;
wire[data_width-1 : 0] data_in;
wire[poly_size-1 : 0] crc_in;
   
output draining, drain_done, crc_ok;
output [data_width-1 : 0] data_out;
output [poly_size-1 : 0] crc_out;

`protect
   
`define _msb_of_poly_size (poly_size-1)
`define _msb_l1_of_crc_poly (`_msb_of_poly_size-1)
`define _msb_of_data_width (data_width-1)
`define _no_of_data_segments (poly_size/data_width)

reg drain_done_int;
reg draining_status;
reg draining_status_next;
reg draining_next;
reg draining_int;
reg crc_ok_result;
reg xor_or_not_ok_reg;
reg drain_done_next;
reg crc_ok_int;
 
reg[poly_size-1 : 0] crc_result;
reg[poly_size-1 : 0] int_ok_calc_reg;
reg[poly_size-1 : 0] crc_out_int;
reg[poly_size-1 : 0] crc_out_info; 
reg[poly_size-1 : 0] crc_out_info_next;
reg[poly_size-1 : 0] crc_out_info_temp;
reg[poly_size-1 : 0] crc_out_next;
reg[poly_size-1 : 0] crc_out_temp;
reg[poly_size-1 : 0] crc_swaped_info; 
reg[poly_size-1 : 0] crc_swaped_shifted;
reg[poly_size-1 : 0] crc_ok_info;

reg[data_width-1 : 0] insert_data;
reg[data_width-1 : 0] data_out_next;
reg[data_width-1 : 0] data_out_int;

integer drain_pointer, data_pointer;
integer drain_pointer_next, data_pointer_next;
integer no_of_bytes;
integer boundry1;
integer boundry2;
integer i, j;

wire[poly_size-1 : 0] insert_crc_info;
wire[poly_size-1 : 0] cur1;
wire[poly_size-1 : 0] cur2;
wire[poly_size-1 : 0] reset_crc_reg;
wire[poly_size-1 : 0] crc_polynomial;
wire[poly_size-1 : 0] crc_xor_constant;
wire[63 : 0] con_poly_coeff;

`define swap_byte_bits(data, data_to_swap) \
    boundry1 = 0; \
    boundry2 = 0; \
    no_of_bytes = data_width/8; \
    if(bit_order == 0) begin \
        data_to_swap = data; \
    end else if(bit_order == 1) begin \
        for(i = 0; i <= `_msb_of_data_width; i = i + 1) begin \
            data_to_swap[`_msb_of_data_width-i] = data[i]; \
        end \
    end else if(bit_order == 3) begin \
        for(i = 1; i <= no_of_bytes; i = i + 1) begin \
            boundry1 = (i * 8) - 1; \
            boundry2 = (i - 1)* 8; \
            for (j = 0; j < 8; j = j + 1) begin \
                data_to_swap [(boundry2 + j)] = data[(boundry1  - j)]; \
            end \
        end \
    end else begin \
        for(i = 1; i <= no_of_bytes; i = i + 1) begin \
            boundry1 = data_width - (i * 8); \
            boundry2 = ((i - 1) * 8); \
            for(j = 0; j < 8; j = j + 1) begin \
                data_to_swap [(boundry2 + j)] = data[(boundry1 + j)]; \
            end \
        end \
    end 

`define assign_crc_result \
    `swap_byte_bits(data_in, swaped_data_in) \
    crc_result = crc_out_int; \
    for(i = 0; i < data_width; i = i + 1) begin \
        xor_or_not = swaped_data_in[`_msb_of_data_width - i] ^ crc_result[`_msb_of_poly_size]; \
        crc_result = {crc_result[`_msb_l1_of_crc_poly : 0], 1'b0}; \
        if(xor_or_not === 1'b1) begin \
            crc_result = (crc_result ^ crc_polynomial); \
        end else if(xor_or_not !== 1'b0) begin \
            crc_result = {data_width{xor_or_not}}; \
        end \
    end

always @ (posedge clk or negedge rst_n) begin : PROC_DW_crc_s_sim
    reg[data_width-1:0] swaped_data_in;
    reg xor_or_not;
    if(rst_n == 1'b1) begin
        if(draining_status === 1'b0) begin
            if((drain & ~drain_done_int) === 1'b1) begin
                draining_status_next = 1'b1;
                draining_next = 1'b1;
                drain_pointer_next = drain_pointer + 1;
                data_pointer_next = data_pointer  - 1;
                data_out_next = insert_data;
                crc_out_next = crc_out_int << data_width;
                crc_out_info_next = crc_out_info; 
                drain_done_next = drain_done_int;
            end else if((drain & ~drain_done_int) === 1'b0) begin
                `assign_crc_result
                draining_status_next = 1'b0;
                draining_next = 1'b0;
                drain_pointer_next = 0;
                data_pointer_next = `_no_of_data_segments ; 
                data_out_next = data_in ;
                crc_out_next = crc_result;
                crc_out_info_next = crc_result;
                drain_done_next = drain_done_int;
            end  
        end else begin 
            if(data_pointer == 0) begin 
                `assign_crc_result
                draining_status_next = 1'b0 ;
                draining_next = 1'b0 ;
                drain_pointer_next = 0 ;
                data_pointer_next = 0 ; 
                data_out_next = data_in ;
                crc_out_next = crc_result;
                crc_out_info_next = crc_result; 
                drain_done_next = 1'b1;
            end else begin 
                draining_status_next = 1'b1 ;
                draining_next = 1'b1 ;
                drain_pointer_next = drain_pointer + 1;
                data_pointer_next = data_pointer  - 1;
                data_out_next = insert_data ;
                crc_out_next = crc_out_int << data_width;
                crc_out_info_next = crc_out_info;
                drain_done_next = drain_done_int;
            end   
        end  
        if(ld_crc_n === 1'b0) begin
            crc_out_temp = crc_in;
            crc_out_info_temp = crc_in;
        end else begin
            crc_out_temp = crc_out_next;
            crc_out_info_temp = crc_out_info_next;
        end 
        for(i = 0; i < poly_size; i = i + 1) begin
            if(crc_out_temp[i] === 1'b0  || crc_out_temp[i] === 1'b1) begin 
                if(crc_out_temp[i] === crc_ok_info[i]) begin
                    crc_ok_result = 1'b1;
                end else begin
                    crc_ok_result = 1'b0;
                    i = poly_size;
                end 
            end else begin
                crc_ok_result = crc_out_temp[i];
                i = poly_size;
            end 
        end
        if(init_n === 1'b1) begin
            if(enable === 1'b1) begin
                draining_status <= draining_status_next;
                draining_int <= draining_next ;
                drain_pointer <= drain_pointer_next ;
                data_pointer <= data_pointer_next ;
                data_out_int <= data_out_next ;
                crc_out_int <= crc_out_temp ;
                crc_out_info <= crc_out_info_temp ;
                drain_done_int <= drain_done_next ;
                crc_ok_int <= crc_ok_result;
            end        
        end else begin 
            draining_status <= 1'b0 ;
            draining_int <= 1'b0 ;
            drain_pointer <= 0 ;
            data_pointer <= `_no_of_data_segments ;
            data_out_int <= {data_width{1'b0}} ;
            crc_out_int <= reset_crc_reg ;
            crc_out_info <= reset_crc_reg ; 
            drain_done_int <= 1'b0 ;
            crc_ok_int <= 1'b0;
        end 
    end else begin
        draining_status <= 1'b0 ;
        draining_int <= 1'b0 ;
        drain_pointer <= 0 ;
        data_pointer <= `_no_of_data_segments ;
        data_out_int <= {data_width{1'b0}} ;
        crc_out_int <= reset_crc_reg ; 
        crc_out_info <= reset_crc_reg ;  
        drain_done_int <= 1'b0 ;
        crc_ok_int <= 1'b0;   
    end
end

assign insert_crc_info = (crc_out_info ^ crc_xor_constant);
always @(insert_crc_info) begin : swap_crc_info
    reg[data_width-1:0] swap_data;
    reg [data_width-1:0] swaped_data;
    integer no_of_words;
    integer k, b1, b2;
    swaped_data = 0;
    crc_swaped_info = 0;
    no_of_words = poly_size/data_width;
    b1 = `_msb_of_poly_size + data_width;
    for(k = no_of_words; k > 0; k = k - 1) begin
        b1 = b1 - data_width;
        b2 = b1 - `_msb_of_data_width;
        swap_data[data_width - 1 : 0] = insert_crc_info[b1 -: data_width];
        `swap_byte_bits(swap_data, swaped_data)
        crc_swaped_info[b1 -: data_width] = swaped_data[data_width - 1 : 0];
    end
    crc_swaped_shifted = crc_swaped_info << (drain_pointer*data_width);
    insert_data = crc_swaped_shifted[poly_size-1:poly_size-data_width];
end

assign con_poly_coeff = {poly_coef3, poly_coef2, poly_coef1, poly_coef0};
assign crc_polynomial = con_poly_coeff[poly_size - 1 : 0];
assign reset_crc_reg = ((crc_cfg % 2 == 0) ? {poly_size{1'b0}} :
        {poly_size{1'b1}});
always @(crc_polynomial or crc_xor_constant) begin : ppp
    integer k;
    int_ok_calc_reg = crc_xor_constant; 
    for(k = 0; k < poly_size; k = k + 1) begin 
        xor_or_not_ok_reg = int_ok_calc_reg[`_msb_of_poly_size];
        int_ok_calc_reg = { int_ok_calc_reg[(`_msb_of_poly_size - 1):0], 1'b0};
        int_ok_calc_reg = (xor_or_not_ok_reg == 1'b1) ?
                (int_ok_calc_reg ^ crc_polynomial) : int_ok_calc_reg;
    end
    crc_ok_info = int_ok_calc_reg;
end

generate 
genvar t;
assign cur1[0] = 1'b1;
assign cur2[0] = 1'b0;
for(t = 1; t < poly_size; t = t + 1) begin : m
    assign cur1[t] = ~cur1[t-1];
    assign cur2[t] = ~cur2[t-1];
end
endgenerate

assign crc_xor_constant = (crc_cfg == 0 || crc_cfg == 1) ? {poly_size{1'b0}} :
			 ((crc_cfg == 6 || crc_cfg == 7)? {poly_size{1'b1}} :
			 ((crc_cfg == 2 || crc_cfg == 3) ? cur1 : cur2) );

assign crc_out = crc_out_int;
assign draining = draining_int;
assign data_out = data_out_int;
assign crc_ok = crc_ok_int;
assign drain_done = drain_done_int;

`undef _msb_of_poly_size
`undef _msb_l1_of_crc_poly
`undef _msb_of_data_width
`undef _no_of_data_segments

`endprotect

endmodule
