
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  Calculates Sum(A[i] x B[i])(i = 0..num_inputs).

 -- ** Input       :  A          - input distributed into num_inputs parts
                      B          - input distributed into num_inputs parts
                      TC         - two's complement:
                                   0 = signed
                                   1 = unsigned

 -- ** Output      :  SUM        - final summation of the input data

 -- ** Param       :  A_width    - word length of A
                      B_width    - word length of B
                      num_inputs - number of inputs
                      SUM_width  - word length of SUM

****************************************************************************/

module DW02_prod_sum_carbon (A, B, TC, SUM);

parameter A_width = 8;
parameter B_width = 8;
parameter num_inputs = 4;
parameter SUM_width = 32;

input [num_inputs * A_width - 1 : 0] A;
input [num_inputs * B_width - 1 : 0] B;
input TC;
output [SUM_width - 1 : 0] SUM;
reg [SUM_width - 1 : 0] SUM;

`protect

integer i;
reg [A_width - 1 : 0] A_int;
reg [B_width - 1 : 0] B_int;

always @(A or B or TC) begin
        SUM = 0;
        for ( i = 0; i < num_inputs; i = i + 1 ) begin
                A_int = A[i * A_width +: A_width];
                B_int = B[i * B_width +: B_width];
                if (A[(i + 1) * A_width - 1] && TC) begin
                        A_int = ~A_int + 1'b1;
                end
                if (B[(i + 1) * B_width - 1] && TC) begin
                        B_int = ~B_int + 1'b1;
                end
                if ( (A[(i + 1) * A_width - 1] ^ 
                     B[(i + 1) * B_width - 1]) && TC) begin
                        SUM = SUM + ~(A_int * B_int - 1'b1);
                end else begin
                        SUM = SUM + A_int * B_int;
                end
        end
end

`endprotect

endmodule

