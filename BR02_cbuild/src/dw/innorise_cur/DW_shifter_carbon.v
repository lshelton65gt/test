
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This is a general-purpose shifter that can be dynamically
                     programmed to operate in arithmetic or barrel shift mode.

 -- ** Input       : data_in      - input data
                     data_tc      - two’s complement control on data_in:
                                    0 = unsigned data_in
                                    1 = signed data_in
                     sh           - shift control
                     sh_tc        - two’s complement control on sh:
                                    0 = unsigned sh
                                    1 = signed sh
                     sh_mode      - arithmetic or barrel shift mode:
                                    0 = barrel shift mode
                                    1 = arithmetic shift mode
                     
 -- ** Output      : data_out     - output data
                     
 -- ** Param       : data_width   - word length of data_in and data_out
                     sh_width     - word length of sh
                     inv_mode     - logic mode:
                                    0 = normal input, 0 padding in output
                                    1 = normal input, 1 padding in output
                                    2 = inverted inputa,0 padding in output
                                    3 = inverted input, 1 padding in output

****************************************************************************/

module DW_shifter_carbon(data_in, data_tc, sh, sh_tc, sh_mode, data_out); 

parameter data_width = 32;
parameter sh_width = 4;
parameter inv_mode = 0;

input [data_width - 1 : 0] data_in;
input [sh_width - 1 : 0] sh;
input data_tc, sh_tc, sh_mode; 

output [data_width - 1 : 0] data_out;
reg [data_width - 1 : 0] data_out;

`protect

reg [sh_width - 1 : 0] sh_int;
reg data_tc_int, sh_tc_int;
reg [sh_width - 1 : 0] sh_abs;
integer i;

always @(data_in, data_tc, sh, sh_tc, sh_mode) begin
    data_tc_int = (inv_mode == 0 || inv_mode == 1) ? data_tc : ~data_tc;
    sh_int = (inv_mode == 0 || inv_mode == 1) ? sh : ~sh;
    sh_tc_int = (inv_mode == 0 || inv_mode == 1) ? sh_tc : ~sh_tc;
    if ( sh_mode == 1'b1 ) begin
        if ( sh_tc_int == 1'b0 || sh_int[sh_width - 1] == 1'b0 ) begin
            data_out = data_in << sh_int;
            if ( inv_mode == 1 || inv_mode == 3 ) begin
                for ( i = 0; i < sh_int ; i = i + 1 ) begin
                    data_out[i] = 1;
                end
            end
        end else begin
            sh_abs = ~sh_int + 1;
            data_out = data_in >> sh_abs;
            if ( data_tc_int == 1'b0 && (inv_mode == 1 || inv_mode == 3) ) begin
                for ( i = 0; i < sh_abs; i = i + 1 ) begin
                    data_out[data_width - i - 1] = 1;
                end
            end
            if ( data_tc_int == 1'b1 ) begin
                for ( i = 0; i < sh_abs; i = i + 1 ) begin
                    data_out[data_width - i - 1] = data_in[data_width - 1];
                end
            end
        end
    end else begin
        if ( sh_tc_int == 1'b0 || sh_int[sh_width - 1] == 1'b0 ) begin
            data_out = data_in << (sh_int % data_width);
            for ( i = 0; i < (sh_int % data_width); i = i + 1 ) begin
                data_out[i] = data_in[data_width - (sh_int % data_width) + i];
            end
        end else begin
            sh_abs = ~sh_int + 1;
            data_out = data_in >> sh_abs;
            for ( i = 0; i < sh_abs; i = i + 1 ) begin
                data_out[data_width - i - 1] = data_in[sh_abs - i - 1];
            end
        end
    end
end

`endprotect

endmodule

