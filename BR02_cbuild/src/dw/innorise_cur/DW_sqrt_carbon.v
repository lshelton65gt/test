
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  This module computes integer square root of input. 

 -- ** Input       :  a       - redicand

 -- ** Output      :  root    - square root of input data

 -- ** Param       :  width   - word lenght of a
                      tc_mode - two's complement control:
                                0 = unsigned
                                1 = signed

****************************************************************************/

module DW_sqrt_carbon(a, root);

parameter width = 32;
parameter tc_mode = 0;

input  [width - 1 : 0] a;
output [(width + 1) / 2 - 1 : 0] root;

reg [(width + 1) / 2 - 1 : 0] root;

`protect
        
integer i;
reg [width-1 : 0] signed_a;

always @(a) begin 
    signed_a = a;
    if ( (tc_mode == 1) && (a[width-1] == 1'b1) ) begin
        signed_a = ~a + 1'b1;
    end
    root = {((width + 1) / 2){1'b0}};
    for ( i = (width + 1) / 2 - 1; i >= 0; i = i - 1 ) begin
        root[i] = 1'b1;
        if (root * root > {1'b0,signed_a}) begin
            root[i] = 1'b0;
        end
    end
end

`endprotect

endmodule

