
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module performs addition and subtraction of
                     operands a and b as either:
                        - a single sum of width bits, or
                        - two sums (one of p1_width bits and one of
                          [width − p1_width] bits).

 -- ** Input       : a        - input data
                     b        - input data
                     ci1      - full or part1 carry input
                     ci2      - part2 carry input
                     addsub   - add/subtract select input:
                                0 = performs add
                                1 = performs subtract
                     tc       - two’s complement select (active high)
                     sat      - saturation mode select (active high)
                     avg      - average mode select (active high)
                     dplx     - duplex mode select (active high)

 -- ** Output      : sum      - output data
                     co1      - part1 carry out
                     co2      - full width or part2 carry output

 -- ** Param       : width    - word length of a, b and sum
                     p1_width - word width of part1 of duplex sdd/sub

****************************************************************************/

module DW_addsub_dx_carbon (a, b, ci1, ci2, addsub, tc, sat, avg, dplx, sum, co1, co2);

parameter width = 16;
parameter p1_width = 8;

input[width - 1 : 0] a;
input[width - 1 : 0] b;
input ci1;
input ci2;
input addsub;
input tc;
input sat;
input avg;
input dplx;

output[width - 1 : 0] sum;
output co1;
output co2;
reg co1;
reg co2;
reg[width - 1 : 0] sum;

`protect

reg[width : 0] sum_int;

`define saturation(sat, avg, addsub, tc, ci, a, b, in_width, saturation) \
    reg[in_width + 1 : 0] sum; \
    reg carry; \
    reg[in_width - 1 : 0] sat_out; \
    reg[in_width - 1 : 0] avg_out; \
    reg[in_width : 0] a_in; \
    reg[in_width : 0] b_in; \
    reg[in_width : 0] bv; \
    avg_out = 0; \
    a_in = 0; \
    b_in = 0; \
    a_in[in_width - 1 : 0] = a; \
    b_in[in_width - 1 : 0] = b; \
    a_in[in_width] = a_in[in_width - 1] & tc; \
    b_in[in_width] = b_in[in_width - 1] & tc; \
    if ( addsub === 1'b0 ) begin \
            carry = ci; \
            bv = b_in; \
    end else begin \
            bv = ~b_in; \
            carry = ~ci; \
    end \
    begin : begin_point \
            integer i; \
            for (i = 0; i <= (in_width - 1); i = i + 1) begin  \
                    sum[i] = (a_in[i] ^ bv[i]) ^ carry; \
                    carry = (a_in[i] & bv[i]) | \
                            (a_in[i] & carry ) | \
                            (carry & bv[i]); \
            end \
    end \
    sum[in_width] = (a_in[in_width] ^ bv[in_width]) ^ carry; \
    if ( addsub === 1'b0 ) begin \
            sum[in_width + 1] = carry; \
    end else begin \
            sum[in_width + 1] = ~carry; \
    end \
    carry = sum[in_width]; \
    sat_out[in_width -  1 : 0] = sum[in_width - 1 : 0]; \
    if (sat === 1'b1) begin  \
        if (tc === 1'b0) begin  \
            carry = 1'b0; \
            if (sum[in_width] === 1'b1) begin  \
                    sat_out = {in_width{~addsub}}; \
            end \
        end else if (tc === 1'b1) begin  \
            if ( sum[in_width] == 0 && sum[in_width - 1] == 1 ) begin \
                   sat_out[in_width - 2 : 0] = {(in_width - 1){1'b1}}; \
                   sat_out[in_width - 1] = 1'b0; \
            end else if ( sum[in_width] == 1 && sum[in_width - 1] == 0 ) begin \
                    sat_out[in_width - 2 : 0] = {(in_width - 1){1'b0}}; \
                    sat_out[in_width - 1] = 1'b1; \
            end \
        end  \
    end \
    if (avg === 1'b0) begin \
        avg_out = sat_out; \
    end else if (avg === 1'b1) begin  \
        if (tc === 1'b0) begin \
                carry = 1'b0; \
        end \
        if (sat === 1'b0) begin \
                avg_out[in_width - 1 : 0] = sum[in_width : 0] >> 1; \
        end else if (sat === 1'b1) begin \
                avg_out[in_width - 2 : 0] = sat_out[in_width - 1 : 0] >> 1; \
                avg_out[in_width - 1] = sat_out[in_width - 1] & tc; \
        end \
    end  \
    saturation[in_width - 1 : 0] = avg_out[in_width - 1 : 0]; \
    saturation[in_width] = carry; \

always @(a or b or ci1 or ci2 or addsub or tc or sat or avg or dplx)
begin
    if( dplx === 1'b0 ) begin 
        begin : saturation_begin1
        `saturation(sat, avg, addsub, tc, ci1, a, b, width, sum_int)
        end
        sum = sum_int[width - 1 : 0];
        co1 = 1'b0;
        co2 = sum_int[width];
    end else if(dplx === 1'b1) begin : dplx_begin
        reg [p1_width - 1 : 0] t_a1;
        reg [p1_width - 1 : 0] t_b1;
        reg [width - p1_width - 1 : 0] t_a2;
        reg [width - p1_width - 1 : 0] t_b2;
        reg [p1_width : 0] t_sum1;
        reg [width - p1_width : 0] t_sum2;
        t_a1 = a[p1_width - 1 : 0];
        t_b1 = b[p1_width - 1 : 0];
        t_a2 = a[width - 1 : p1_width];
        t_b2 = b[width - 1 : p1_width];
        begin : saturation_begin1
        `saturation(sat, avg, addsub, tc, ci1,
                    t_a1, t_b1, p1_width, t_sum1)
        end
        sum[p1_width - 1 : 0] = t_sum1[p1_width - 1 : 0];
        co1 = t_sum1[p1_width];
        begin : saturation_begin2
        `saturation(sat, avg, addsub, tc, ci2, t_a2, t_b2,
                    width - p1_width, t_sum2)
        end
        sum[width - 1 : p1_width] = t_sum2[width - p1_width - 1 : 0];
        co2 = t_sum2[width - p1_width];
    end 
end

`undef saturation

`endprotect

endmodule

