
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  This is a combinational integer divider with both 
                      quotient and remainder outputs. This component divides 
                      the dividend a by the divisor b to produce the quotient 
                      and remainder.

 -- ** Input       :  a           - dividend
                      b           - divisor

 -- ** Output      :  quotient    - quotient output (a/b)
                      remainder   - remainder / modulus
                      divide_by_0 - indicates if b == 0

 -- ** Param       :  a_width     - word length of a
                      b_width     - word length of b
                      tc_mode     - two's complement control:
                                    0 = signed
                                    1 = unsigned
                      rem_mode    - reminder output control:
                                    0 = a mod b
                                    1 = a rem b, a % b

****************************************************************************/

module DW_div_carbon (a, b, quotient, remainder, divide_by_0);

parameter a_width = 32;
parameter b_width = 32;
parameter tc_mode = 1;
parameter rem_mode = 1;

input [a_width - 1 : 0] a;
input [b_width - 1 : 0] b;
output [a_width - 1 : 0] quotient;
output [b_width - 1 : 0] remainder;
output divide_by_0;

wire [a_width - 1 : 0] a;
wire [b_width - 1 : 0] b;
reg [a_width - 1 : 0] quotient;
reg [b_width - 1 : 0] remainder;
reg divide_by_0;

`protect

`define max_uns {a_width{1'b1}}
`define min_sgn {1'b1, {a_width{1'b0}}}
`define max_sgn {1'b0, {a_width{1'b1}}}
`define ones ({(b_width % a_width) + 1{1'b1}})

reg [a_width - 1 : 0] signed_a;
reg [b_width - 1 : 0] signed_b;

always @(a or b) begin 
    if (tc_mode == 0) begin
        if(b == 0) begin
            quotient = `max_uns;
            remainder = a;
        end else begin
            quotient = a / b;
            remainder = a % b;
        end
    end else begin
        if (b == 0) begin
            quotient = (a[a_width-1] == 1'b0) ? `max_sgn >> 1 : `min_sgn >> 1;
            remainder = (b_width > a_width) && (a[a_width - 1] == 1'b1) ?
                        {`ones, a} : a;
        end else begin
            signed_a = a[a_width - 1] == 1'b1 ? ~a + 1'b1 : a;
            signed_b = b[b_width - 1] == 1'b1 ? ~b + 1'b1 : b;
            quotient = signed_a / signed_b;
            if (a[a_width - 1] != b[b_width - 1]) begin
                quotient = ~quotient + 1'b1;
            end
            remainder = signed_a % signed_b;
            if (a[a_width - 1] == 1'b1) begin
                remainder = ~remainder + 1'b1;
            end
        end
        if (rem_mode == 0) begin 
            if (b != 0 && remainder != 0 && 
                a[a_width - 1] != b[b_width - 1]) begin
                remainder = b + remainder;
            end
        end
    end
    divide_by_0 = b == {b_width{1'b0}} ? 1'b1 : 1'b0;
end

`undef max_uns
`undef min_sgn
`undef max_sgn
`undef ones

`endprotect

endmodule

