
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This is an asymmetric I/O dual independent clock FIFO RAM
                     controller. It is designed to interface with a dual-port
                     synchronous RAM.

 -- ** Input       : clk_push       - input clock for push interface
                     clk_pop        - input clock for pop interface
                     rst_n          - reset, active low
                     push_req_n     - FIFO push request, active low
                     flush_n        - flushes the partial word into
                                      memory (fills in 0's)
                                      (for data_in_width < data_out_width only)
                     pop_req_n      - FIFO pop request, active low
                     data_in        - FIFO data to push
                     rd_data        - RAM data input to FIFO controller

 -- ** Output      : we_n           - write enable output for write port of RAM,
                                      active low
                     push_empty     - FIFO empty output flag synchronous to
                                      clk_push, active high
                     push_ae        - FIFO almost empty output flag synchronous
                                      to clk_push, active high (determined by
                                      push_ae_lvl parameter)
                     push_hf        - FIFO half full output flag synchronous to
                                      clk_push, active high
                     push_af        - FIFO almost full output flag synchronous
                                      to clk_push, active high (determined by
                                      push_af_lvl parameter)
                     push_full      - FIFO’s RAM full output flag (including
                                      the input buffer of FIFO controller for
                                      data_in_width < data_out_width)
                                      synchronous to clk_push, active high
                     ram_full       - FIFO’s RAM (excluding the input buffer of
                                      FIFO controller for
                                      data_in_width < data_out_width) full
                                      output flag synchronous to clk_push,
                                      active high
                     part_wd        - partial word accumulated in the input
                                      buffer synchronous to clk_push,
                                      active high
                                      (for data_in_width < data_out_width only;
                                      otherwise, tied low)
                     push_error     - FIFO push error (overrun) output flag
                                      synchronous to clk_push, active high
                     pop_empty      - FIFO empty output flag synchronous to
                                      clk_pop, active high
                     pop_ae         - FIFO almost empty output flag synchronous
                                      to clk_pop, active high (determined by
                                      pop_ae_lvl parameter)
                     pop_hf         - FIFO half full output flag synchronous to
                                      clk_pop, active high
                     pop_af         - FIFO almost full output flag synchronous
                                      to clk_pop, active high (determined by
                                      pop_af_lvl parameter)
                     pop_full       - FIFO’s RAM full output flag (excluding
                                      the input buffer of FIFO controller for
                                      case data_in_width < data_out_width)
                                      synchronous to clk_pop, active high
                     pop_error      - FIFO pop error (underrun) output flag
                                      synchronous to clk_pop, active high
                     wr_data        - FIFO controller output data to RAM
                     wr_addr        - address output to write port of RAM
                     rd_addr        - address output to read port of RAM
                     data_out       - FIFO data to pop

 -- ** Param       : data_in_width  - width of the data-in bus
                     data_out_width - width of the data-out bus
                     depth          - number of words that can be stored
                                      in FIFO
                     push_ae_lvl    - almost empty level for the push_ae
                                      output port (the number of words in the
                                      FIFO at or below which the push_ae flag
                                      is active)
                     push_af_lvl    - almost full level for the push_af output
                                      port (the number of empty memory
                                      locations in the FIFO at which the
                                      push_af flag is active)
                     pop_ae_lvl     - almost empty level for the pop_ae output
                                      port (the number of words in the FIFO at
                                      or below which the pop_ae flag is active)
                     pop_af_lvl     - almost full level for the pop_af output
                                      port (the number of empty memory
                                      locations in the FIFO at which
                                      the pop_af flag is active)
                     err_mode       - error mode:
                                      0 = stays active until reset [latched]
                                      1 = active only as long as error
                                          condition exists [unlatched])
                     push_sync      - push flag synchronization mode:
                                      1 = single register synchronization
                                          from pop pointer
                                      2 = double register
                                      3 = triple register
                     pop_sync       - pop flag synchronization mode:
                                      1 = single register synchronization
                                          from push pointer
                                      2 = double register
                                      3 = triple register
                     rst_mode       - reset mode:
                                      0 = asynchronous reset
                                      1 = synchronous reset
                     byte_order     - order of bytes or subword within a word
                                      0 = first byte is in most significant
                                          bits position
                                      1 = first byte is in the least
                                          significant bits position

****************************************************************************/

`include "DW_fifoctl_s2_sf_carbon.v"

module DW_asymfifoctl_s2_sf_carbon (clk_push, clk_pop, rst_n, push_req_n, flush_n, pop_req_n, data_in, rd_data, we_n, push_empty, push_ae, push_hf, push_af, push_full, ram_full, part_wd, push_error, pop_empty, pop_ae, pop_hf, pop_af, pop_full, pop_error, wr_data, wr_addr, rd_addr, data_out);

parameter data_in_width = 3;
parameter data_out_width = 12;
parameter depth = 8;
parameter push_ae_lvl = 2;
parameter push_af_lvl = 2;
parameter pop_ae_lvl = 2;
parameter pop_af_lvl = 2;
parameter err_mode = 0;
parameter push_sync = 2;
parameter pop_sync = 2;
parameter rst_mode = 0;
parameter byte_order =0;

`define DW_addr_width ((depth>65536) ? ((depth>1048576) ? ((depth>4194304) ? \
	((depth>8388608) ? 24 : 23) : ((depth>2097152) ? 22 : 21)) : \
    ((depth>262144) ? ((depth>524288) ? 20 : 19) : ((depth>131072) ? \
    18 : 17))) : ((depth>256) ? ((depth>4096) ? ((depth>16384) ? \
    ((depth>32768) ? 16 : 15) : ((depth>8192) ? 14:13)) : \
    ((depth>1024) ? ((depth>2048) ? 12 : 11) : ((depth>512) ? 10 : 9))) : \
	((depth>16) ? ((depth>64) ? ((depth>128) ? 8 : 7) : \
    ((depth>32) ? 6 : 5)) : ((depth>4) ? ((depth>8) ? 4 : 3) : \
    ((depth>2) ? 2 : 1)))))

`define DW_count_width ((depth+1>65536) ? ((depth+1>16777216) ? \
	((depth+1>268435456) ? ((depth+1>536870912) ? 30 : 29) : \
    ((depth+1>67108864) ? ((depth+1>134217728) ? 28 : 27) : \
    ((depth+1>33554432) ? 26 : 25))) : ((depth+1>1048576) ? \
    ((depth+1>4194304) ? ((depth+1>8388608) ? 24 : 23) : \
	((depth+1>2097152) ? 22 : 21)) : ((depth+1>262144) ? \
    ((depth+1>524288) ? 20 : 19) : ((depth+1>131072) ? 18 : 17)))) : \
    ((depth+1>256) ? ((depth+1>4096) ? ((depth+1>16384) ? \
    ((depth+1>32768) ? 16 : 15) : ((depth+1>8192) ? 14 : 13)) : \
	((depth+1>1024) ? ((depth+1>2048) ? 12 : 11) : \
    ((depth+1>512) ? 10 : 9))) : ((depth+1>16) ? ((depth+1>64) ? \
    ((depth+1>128) ? 8 : 7) : ((depth+1>32) ? 6 : 5)) : \
	((depth+1>4) ? ((depth+1>8) ? 4 : 3) : ((depth+1>2) ? 2 : 1)))))

`define DW_K ((data_in_width>data_out_width) ? \
    (data_in_width/data_out_width) : (data_out_width/data_in_width))

`define DW_max_width ((data_in_width>data_out_width) ? \
    data_in_width : data_out_width)

`define DW_min_width ((data_in_width<data_out_width) ? \
    data_in_width : data_out_width)

input clk_push;
input clk_pop;
input rst_n;
input push_req_n;
input flush_n;
input pop_req_n;

input [data_in_width-1 : 0] data_in;
wire [data_in_width-1 : 0] data_in;

input [`DW_max_width-1 : 0] rd_data;

output we_n;
output push_empty;
output push_ae;
output push_hf;
output push_af;
output push_full;
output ram_full;
output part_wd;
output push_error;
output pop_empty;
output pop_ae;
output pop_hf;
output pop_af;
output pop_full;
output pop_error;
output [`DW_max_width-1 : 0]  wr_data;
output [`DW_addr_width-1 : 0] wr_addr;
output [`DW_addr_width-1 : 0] rd_addr;
output [data_out_width-1 : 0] data_out;

`protect

wire ram_push_n;
wire ram_pop_n;
wire flush_n_int;
wire ram_push_error, next_ram_push_error;
wire ram_pop_error, next_ram_pop_error;
wire a_rst_n;
wire s_rst_n;

reg [data_in_width-1 : 0] ubuffer;
reg [data_in_width-1 : 0] buffer [`DW_K-1 : 0];
reg [data_in_width-1 : 0] next_buffer [`DW_K-1 : 0];  
reg [`DW_min_width-1 : 0] temp_buffer;
reg [data_in_width-1 : 0] data_in_temp;


reg ram_write_en_n;
reg buff_write_en_n;
reg ram_pop_n_int;
reg push_full_int;
reg part_wd_int, next_part_wd_int;
reg buff_error, next_buff_error;
reg [`DW_max_width-1 : 0] wr_data_int;
reg [`DW_max_width-1 : 0] temp_wr_data_int;
reg push_error_int, next_push_error_int;
reg pop_error_int, next_pop_error_int;
reg [data_out_width-1 : 0] data_out_int;

wire [`DW_count_width-1 : 0] unused_pop_count;
wire [`DW_count_width-1 : 0] unused_push_count;

integer wd_buff_addr, next_wd_buff_addr;
integer rd_buff_addr, next_rd_buff_addr;
integer i;

assign flush_n_int = ((data_in_width < data_out_width) ? (flush_n) : (1'b1));
assign ram_pop_n = ((data_in_width <= data_out_width) ?
        pop_req_n : (ram_pop_n_int));
assign data_out = ((data_in_width <= data_out_width) ?
        rd_data : (data_out_int));
assign pop_error = ((data_in_width <= data_out_width) ?
        ram_pop_error : (pop_error_int));
assign ram_push_n = ((data_in_width >= data_out_width) ?
        push_req_n : ((ram_write_en_n)));  
assign wr_data = ((data_in_width >= data_out_width) ?
        data_in : (wr_data_int));
assign part_wd = ((data_in_width >= data_out_width) ?
        1'b0 : (part_wd_int));
assign push_full = ((data_in_width >= data_out_width) ?
        ram_full : (push_full_int));
assign push_error = ((data_in_width >= data_out_width) ?
        ram_push_error : (buff_error | ram_push_error));

assign a_rst_n = (rst_mode == 0) ? rst_n : 1'b1;
assign s_rst_n = (rst_mode == 1) ? rst_n : 1'b1;

always @ (push_req_n or ram_full or flush_n_int or
          data_in or wd_buff_addr or part_wd_int ) begin

    if (data_in_width < data_out_width) begin
        if (ram_full == 1'b1 && (
            ((wd_buff_addr == `DW_K-1) && (push_req_n == 1'b0)) ||
            ((wd_buff_addr > 0) && (flush_n_int == 1'b0))) ) begin
            next_buff_error = 1'b1;
        end else begin
            next_buff_error = 1'b0;
        end
        if (((wd_buff_addr == `DW_K-1) && (push_req_n == 1'b0)) ||
            ((wd_buff_addr > 0) && (flush_n_int == 1'b0))) begin
            ram_write_en_n = (ram_full == 1'b1) ? 1'b1 : 1'b0;
        end else begin
            ram_write_en_n = 1'b1;
        end
        if ((ram_full == 1'b1) && 
            (wd_buff_addr == `DW_K-1) && (push_req_n == 1'b0)) begin
            buff_write_en_n = 1'b1;
        end else begin
            buff_write_en_n = push_req_n;
        end

        if (ram_write_en_n == 1'b0 ) begin
            if (push_req_n == 1'b0 && flush_n_int == 1'b0) begin
                next_buffer[0] = data_in;
                ubuffer = {data_in_width {1'b0}};
                next_part_wd_int = 1'b1;
                for (i = 1; i < (`DW_K - 1); i = i + 1) begin
                    next_buffer[i] = {data_in_width {1'b0}};
                end		     
                next_wd_buff_addr = 1;
            end else begin
                next_wd_buff_addr = 0;
                next_part_wd_int = 1'b0;
                ubuffer = {data_in_width {1'b0}};
                if (flush_n_int == 1'b0) begin
                    ubuffer = {data_in_width {1'b0}};
                    for (i = 1; i < (`DW_K - 1); i = i + 1) begin
                        next_buffer[i] = {data_in_width {1'b0}};
                    end
                end else begin
                    ubuffer = data_in;
                    for (i = 0; i < (`DW_K - 1); i = i + 1) begin
                        next_buffer[i] = {data_in_width {1'b0}};
                    end
                end
            end
        end else begin
            if (buff_write_en_n == 1'b0) begin
                ubuffer = {data_in_width {1'b0}};
                next_part_wd_int = 1'b1;
                next_wd_buff_addr = wd_buff_addr+1;
                for (i = 0; i < wd_buff_addr; i = i+1) begin
                    next_buffer[i] = buffer[i];
                end
                for (i = wd_buff_addr+1; i < `DW_K-1; i = i+1) begin
                    next_buffer[i] = {data_in_width {1'b0}};
                end			      
                next_buffer[wd_buff_addr] = data_in;
            end
            else begin
                ubuffer = {data_in_width {1'b0}};
                next_wd_buff_addr = wd_buff_addr;
                next_part_wd_int = part_wd_int;
                for (i = 0; i <= (`DW_K - 1); i = i + 1) begin
                    next_buffer[i] = buffer[i];
                end
            end
        end
    end
end

always @ (wd_buff_addr or ubuffer or buffer[0] or ram_write_en_n) begin
    if (data_in_width < data_out_width) begin
        if ( ram_write_en_n == 1'b0 ) begin
            data_in_temp = ubuffer;
        end else begin
            data_in_temp = {data_in_width {1'b0}};
        end
        if ( byte_order == 0 ) begin
            wr_data_int = {`DW_max_width{1'b0}};
            for (i = 0; i < (`DW_K - 1); i = i + 1) begin
                temp_buffer = buffer[i];
                temp_wr_data_int = {`DW_max_width{1'b0}};
                temp_wr_data_int = temp_buffer;
                temp_wr_data_int = temp_wr_data_int << 
                        (`DW_min_width * (`DW_K-1-i));
                wr_data_int = wr_data_int | temp_wr_data_int;
            end
            for (i = 0; i <= (`DW_min_width-1); i = i + 1) begin
                wr_data_int[i] = data_in_temp [i];
            end
        end else begin
            wr_data_int = {`DW_max_width{1'b0}};
            for (i = 0 ; i < (`DW_K - 1); i = i + 1)  begin
                temp_buffer = buffer[i];
                temp_wr_data_int = {`DW_max_width{1'b0}};
                temp_wr_data_int = temp_buffer;
                temp_wr_data_int = temp_wr_data_int << (`DW_min_width * i);
                wr_data_int = wr_data_int | temp_wr_data_int;
            end
            for (i = 0; i <= (`DW_min_width-1); i = i + 1) begin
                wr_data_int[i+(`DW_min_width * (`DW_K-1))] =
                    data_in_temp[i];
            end
        end
    end
end

always @ (wd_buff_addr or ram_full) begin 
    if (data_in_width < data_out_width) begin
        if ( ram_full == 1'b1 && wd_buff_addr == (`DW_K-1) ) begin
            push_full_int = 1'b1;
        end else begin
            push_full_int = 1'b0;
        end
    end
end

always @ (rd_buff_addr or pop_req_n or pop_empty) begin 
    if (data_in_width > data_out_width) begin    
        if (rd_buff_addr == -1) begin
            ram_pop_n_int = 1'b0;
        end else if ( pop_req_n == 1'b0 &&
            pop_empty == 1'b0 &&
            rd_buff_addr == (`DW_K - 1) ) begin
            ram_pop_n_int = 1'b0;
        end else begin
            ram_pop_n_int = 1'b1;
        end
        if (pop_req_n == 1'b0 && pop_empty == 1'b0) begin
            next_rd_buff_addr = (rd_buff_addr + 1) % `DW_K;
        end else begin
            next_rd_buff_addr = (pop_req_n == 1'b1 || 
                pop_empty == 1'b1 ) ? 
                rd_buff_addr : -1;
        end
    end
end

always @ (rd_buff_addr or rd_data) begin
    if (data_in_width > data_out_width) begin
        if (rd_buff_addr > -1) begin
            if ( byte_order == 0 ) begin
                for (i = 0; i < `DW_min_width; i = i + 1)   begin
                    data_out_int[i] = rd_data[`DW_max_width -
                        `DW_min_width + i -
                        (rd_buff_addr * `DW_min_width)];
                end
            end else begin 
                for (i = 0; i < `DW_min_width; i = i + 1) begin
                    data_out_int[i] = rd_data[i+rd_buff_addr *
                        `DW_min_width];
                end
            end
        end else begin
            data_out_int = {`DW_max_width -1{1'b0}};
        end
    end
end

always @ (pop_req_n or pop_empty or pop_error_int or next_rd_buff_addr) begin
    if (data_in_width > data_out_width) begin
        if ((err_mode < 1) && (pop_error_int != 1'b0))
            next_pop_error_int = pop_error_int;
        else if (pop_req_n == 1'b0 && pop_empty == 1'b1 ) 
            next_pop_error_int = 1'b1;
        else
            next_pop_error_int = 1'b0;
    end
end

always @ (posedge clk_push or negedge a_rst_n) begin
    if (a_rst_n == 1'b0) begin
        if (data_in_width < data_out_width) begin
            wd_buff_addr <= 0;
            part_wd_int <= 0;
            buff_error <= 1'b0;
            for (i = 0; i < (`DW_K - 1); i = i + 1) begin
                buffer[i] <= {data_in_width {1'b0}};
            end
        end
    end else if (s_rst_n == 1'b0) begin
        if (data_in_width < data_out_width) begin
            wd_buff_addr <= 0;
            part_wd_int <= 0;
            buff_error <= 1'b0;
            for (i = 0; i < (`DW_K - 1); i = i + 1) begin
                buffer[i] <= {data_in_width {1'b0}};
            end
        end
    end else if (data_in_width < data_out_width) begin
        wd_buff_addr <= next_wd_buff_addr;
        part_wd_int <= next_part_wd_int;
        buff_error <= (err_mode == 0) ?
            (next_buff_error | buff_error) : 
            next_buff_error;
        for (i = 0; i < (`DW_K - 1); i = i + 1)begin
            buffer[i] <= next_buffer[i];
        end
    end
end

always @ (posedge clk_pop or negedge a_rst_n) begin    
    if (a_rst_n == 1'b0) begin
        if (data_in_width > data_out_width) begin
            rd_buff_addr <= 0;
            pop_error_int <= 1'b0;
        end
    end else if (s_rst_n == 1'b0) begin
        if (data_in_width > data_out_width) begin
            rd_buff_addr <= 0;
            pop_error_int <= 1'b0;
        end else begin
            rd_buff_addr <= next_rd_buff_addr;
            pop_error_int <= next_pop_error_int;
        end
    end
end

DW_fifoctl_s2_sf_carbon #(
    depth, 
    push_ae_lvl, 
    push_af_lvl, 
    pop_ae_lvl,
    pop_af_lvl, 
    err_mode, 
    push_sync, 
    pop_sync,
    rst_mode
)

DW_fifoctl_s2_sf_carbon (
    .clk_push(clk_push), 
    .clk_pop(clk_pop),
    .rst_n(rst_n), 
    .push_req_n(ram_push_n),
    .pop_req_n(ram_pop_n), 
    .we_n(we_n),
    .push_empty(push_empty), 
    .push_ae(push_ae),
    .push_hf(push_hf), 
    .push_af(push_af),
    .push_full(ram_full),
    .push_error(ram_push_error),
    .pop_empty(pop_empty),
    .pop_ae(pop_ae),
    .pop_hf(pop_hf), 
    .pop_af(pop_af), 
    .pop_full(pop_full),
    .pop_error(ram_pop_error), 
    .wr_addr(wr_addr), 
    .rd_addr(rd_addr),
    .push_word_count(unused_push_count),
    .pop_word_count(unused_pop_count),
    .test(1'b0)
);

`endprotect

`undef DW_addr_width
`undef DW_count_width
`undef DW_K
`undef DW_max_width
`undef DW_min_width

endmodule

