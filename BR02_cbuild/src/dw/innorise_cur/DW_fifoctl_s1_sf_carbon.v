
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  This module is a FIFO RAM controller designed to
                      interface with a dual-port synchronous RAM.

 -- ** Input       :  clk          - input clock
                      rst_n        - reset input, active low:
                                     0 = asynchronous reset
                                     1 = synchronous reset
                      push_req_n   - FIFO push request, active low
                      pop_req_n    - FIFO pop request, active low
                      diag_n       - diagnostic control, active low:
                                     for err_mode = 0, NC
                                     for other err_mode values

 -- ** Output      :  we_n         - write enable output for write port of RAM,
                                     active low
                      empty        - FIFO empty output, active low
                      almost_empty - FIFO almost empty output, asserted when
                                     FIFO level <= ae_level, active high
                      half_full    - FIFO half full output, active high
                      almost_full  - FIFO almost full output, asserted when
                                     FIFO level >= depth - af_level, active high
                      full         - FIFO full output, active high
                      error        - FIFO error output, active high
                      wr_addr      - address output to write port of RAM
                      rd_addr      - address output to read port of RAM

 -- ** Param       :  depth        - number of memory elements used in
                                     FIFO (used to size the address ports)
                      ae_level     - almost empty level (the number of words
                                     in the FIFO at or below which the
                                     almost_empty flag is active)
                      af_level     - almost full level (the number of empty
                                     memory locations in the FIFO at which
                                     the almost_full flag is active.
                      err_mode     - error mode:
                                     0 = underflow/overflow and pointer latched
                                     checking
                                     1 = underflow/overflow latched checking
                                     2 = underflow/overflow unlatched checking
                      rst_mode     - reset mode:
                                     0 = asynchronous reset
                                     1 = synchronous reset
                      
****************************************************************************/

module DW_fifoctl_s1_sf_carbon (clk, rst_n, push_req_n, pop_req_n, diag_n, we_n, empty, almost_empty, half_full, almost_full, full, error, wr_addr, rd_addr);

parameter depth = 4;
parameter ae_level = 1;
parameter af_level = 1;
parameter err_mode = 0;
parameter rst_mode = 0;

`define DW_addr_width ((depth>4096) ? ((depth>262144) ? ((depth>2097152) ? \
                      ((depth>8388608) ? 24 : ((depth> 4194304) ? 23 : 22)) : \
                      ((depth>1048576) ? 21 : ((depth>524288) ? 20 : 19))) : \
                      ((depth>32768) ? ((depth>131072) ? 18 : \
                      ((depth>65536) ? 17 : 16)) : ((depth>16384)? 15 : \
                      ((depth>8192) ? 14:13)))) : ((depth>64) ? ((depth>512) ? \
                      ((depth>2048)?12 :  ((depth>1024) ? 11 : 10)) : \
                      ((depth>256) ? 9 : ((depth>128)? 8 :7))) : ((depth>8) ? \
                      ((depth> 32) ? 6 : ((depth>16)? 5 : 4)) : \
                      ((depth>4) ? 3 : ((depth>2) ? 2 : 1)))))

input clk, rst_n, push_req_n, pop_req_n, diag_n;
output we_n, empty, almost_empty, half_full, almost_full, full, error;
output [`DW_addr_width - 1 : 0 ] wr_addr, rd_addr;

reg error;
reg [`DW_addr_width - 1 : 0 ] wr_addr;
reg [`DW_addr_width - 1 : 0 ] rd_addr;

wire a_rst_n, s_rst_n, diag_n_int, we_n;
wire empty, almost_empty, half_full, almost_full, full;

`protect

wire next_error_int;
wire [31 : 0] next_wrd_count;
wire [`DW_addr_width - 1 : 0] next_wr_addr_int;
wire [`DW_addr_width - 1 : 0] next_rd_addr_int;

integer wrd_count;

assign diag_n_int = (err_mode == 0)? diag_n : 1'b1;

assign a_rst_n = (rst_mode == 0) ? rst_n : 1'b1;
assign s_rst_n = (rst_mode == 1) ? rst_n : 1'b1;

assign next_wr_addr_int = ((push_req_n == 1'b0) &&
                          ((full == 1'b0) || (pop_req_n == 1'b0))) ? 
                          (wr_addr + 1) % depth : ((push_req_n == 1'b1) ||
                          (full == 1'b1)) ?  wr_addr : 0;

assign next_rd_addr_int = (diag_n_int == 1'b0) ? 0 :
                          ((diag_n_int == 1'b1) && (pop_req_n == 1'b0) &&
                          (empty == 1'b0)) ? (rd_addr + 1) % depth :
                          (((pop_req_n == 1'b1) || (empty == 1'b1)) &&
                          (diag_n_int == 1'b1)) ?  rd_addr : 0;

assign next_wrd_count = ((push_req_n == 1'b1) && (pop_req_n == 1'b0) &&
                        (wrd_count != 0)) ?  wrd_count - 1 : 
                        (((push_req_n == 1'b0) && (pop_req_n == 1'b0) &&
                        (wrd_count == 0)) || ((push_req_n == 1'b0) &&
                        (pop_req_n == 1'b1) && (wrd_count < depth)) ) ?
                        wrd_count + 1 : (((push_req_n == 1'b0) &&
                        (pop_req_n == 1'b0) && (wrd_count > 0)) ||
                        ((push_req_n == 1'b0) && (pop_req_n == 1'b1) &&
                        (wrd_count == depth)) || ((push_req_n == 1'b1) &&
                        (pop_req_n == 1'b0) && (wrd_count == 0)) ||
                        ((push_req_n == 1'b1) && (pop_req_n == 1'b1)) ) ?
                        wrd_count : 0;

assign next_error_int = ((err_mode < 2) && (error != 1'b0)) ? error :
                        ((err_mode == 0) && (rd_addr >= 0) && (wr_addr >= 0) &&
                        (((rd_addr == wr_addr) && (wrd_count > 0) &&
                        (wrd_count < depth))  || ((rd_addr != wr_addr) &&
                        ((wrd_count == 0) || (wrd_count == depth))) )) ?  1'b1 :
                        (wrd_count == 0)? ~pop_req_n : (wrd_count == depth) ?
                        (pop_req_n & ~push_req_n) : 1'b0;

always @ (posedge clk or negedge a_rst_n)
begin : clk_registers
    if (a_rst_n == 1'b0) begin
        wr_addr <= {`DW_addr_width{1'b0}};
        rd_addr <= {`DW_addr_width{1'b0}};
        wrd_count <= 0;
        error <= 1'b0;
    end else if(s_rst_n == 1'b0) begin
        wr_addr <= {`DW_addr_width{1'b0}};
        rd_addr <= {`DW_addr_width{1'b0}};
        wrd_count <= 0;
        error <= 1'b0;
    end else begin
        wr_addr <= next_wr_addr_int;
        rd_addr <= next_rd_addr_int;
        wrd_count <= next_wrd_count;
        error <= next_error_int;
    end
end

assign empty = (wrd_count == 0) ? 1'b1 : 1'b0;
assign almost_empty = (wrd_count > ae_level) ? 1'b0 : 1'b1;
assign half_full = (wrd_count < ((depth  +  1 )/ 2)) ? 1'b0 : 1'b1;
assign almost_full = (wrd_count < (depth  - af_level)) ? 1'b0 : 1'b1;
assign full = (wrd_count == depth) ? 1'b1 : 1'b0;

assign we_n = (push_req_n | (pop_req_n  & full));

`endprotect

`undef DW_addr_width

endmodule

