
/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2008 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description :  This module is a universal stallable pipelined divider.
                      It computes the result with a latency of
                      num_stages - 1 clock cycles.

 -- ** Input       :  clk         - input clock
                      rst_n       - reset, active low (unused if rst_mode = 0)
                      en          - load enable (used only if stall_mode = 1)
                                    0 = stall
                                    1 = load
                      a           - dividend
                      b           - divisor

 -- ** Output      :  quotient    - quotient output (a/b)
                      remainder   - remainder
                      divide_by_0 - indicates if b == 0

 -- ** Param       :  a_width     - word length of a
                      b_width     - word length of b
                      tc_mode     - two's complement control:
                                    0 = signed
                                    1 = unsigned
                      rem_mode    - reminder output control:
                                    0 = a mod b
                                    1 = a rem b, a % b
                      num_stages  - number of pipeline stages
                      stall_mode  - stall mode
                                    0 = non-stallable
                                    1 = stallable

****************************************************************************/

`include "DW_div_carbon.v"

module DW_div_pipe_carbon(clk, rst_n, en, a, b, quotient, remainder, divide_by_0);

parameter a_width = 16;
parameter b_width = 16;
parameter tc_mode = 0;
parameter rem_mode = 1;
parameter num_stages = 2;
parameter stall_mode = 0;
parameter rst_mode = 0;

input clk;
input rst_n;
input [a_width - 1 : 0] a;
input [b_width - 1 : 0] b;
input en;

output [a_width - 1 : 0] quotient;
output [b_width - 1 : 0] remainder;
output divide_by_0;

`protect

reg [a_width - 1 : 0] a_reg[num_stages - 1 : 0];
reg [b_width - 1 : 0] b_reg[num_stages - 1 : 0];
integer j, wp;

wire a_rst_n;
wire s_rst_n;

assign a_rst_n = (rst_mode == 1)? rst_n : 1'b1;
assign s_rst_n = (rst_mode == 2)? rst_n : 1'b1;

always @(posedge clk or negedge a_rst_n) begin
    if(a_rst_n == 1'b0) begin
        for(j = 0; j < num_stages - 1; j = j + 1) begin
            a_reg[j] = {a_width{1'b0}};
            b_reg[j] = {b_width{1'b0}};
        end
        wp = 0;
    end else begin
        if(s_rst_n == 1'b0) begin
            for(j = 0; j < num_stages - 1; j = j + 1) begin
                a_reg[j] = {a_width{1'b0}};
                b_reg[j] = {b_width{1'b0}};
            end
            wp = 0;
        end else begin
            if (stall_mode != 0 && en == 1'b0) begin
                // Do nothing, leave old values
            end else if (stall_mode == 0 || en == 1'b1) begin
                a_reg[wp] = a;
                b_reg[wp] = b;
                wp = (wp + 1 == num_stages - 1) ? 0 : wp + 1;
            end else begin
                for(j = 0; j < num_stages - 1; j = j + 1) begin
                    a_reg[j] = {a_width{1'b0}};
                    b_reg[j] = {b_width{1'b0}};
                end
            end
        end
    end
end

DW_div_carbon 
U1 (.a(a_reg[wp]),
    .b(b_reg[wp]),
    .quotient(quotient),
    .remainder(remainder),
    .divide_by_0(divide_by_0));

defparam U1.a_width = a_width;
defparam U1.b_width = b_width;
defparam U1.tc_mode = tc_mode;
defparam U1.rem_mode = rem_mode;

`endprotect

endmodule

