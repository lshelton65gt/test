/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW02_sum. It returns PRODUCT of A,B .

 -- ** Input       :    INPUT

 -- ** Inout       :    -

 -- ** Output      :    SUM

 -- ** Date        :    07-15-2005


****************************************************************************/




module DW02_sum_carbon (INPUT, SUM);
  parameter num_inputs=4;
  parameter input_width=32;
  input [ num_inputs*input_width- 1: 0] INPUT;
  output [ input_width- 1: 0] SUM;

`protect
  reg [ input_width- 1: 0] SUM;

  integer i;

  always @(INPUT)
    begin
       SUM = INPUT [ input_width-1 : 0 ];
       for ( i = 1; i <= num_inputs-1; i = i + 1)
	 begin
	    SUM = SUM + (INPUT >> (i*input_width));
	 end
    end
`endprotect
endmodule




