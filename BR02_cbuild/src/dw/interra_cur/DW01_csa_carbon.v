/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_csa.  It return sum and co.

 -- ** Input       :    a , b , c , ci

 -- ** Inout       :    -

 -- ** Output      :    sum , co

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-05-2005

****************************************************************************/

module DW01_csa_carbon (a,b,c,ci,carry,sum,co);

  parameter width=32;

    input  [width-1 : 0]   a,b,c;
  input                  ci;
  output [width-1 : 0]   carry,sum;
  output                 co;
`protect
  reg    [width-1 : 0]   carry,sum;
  reg                    co;
     integer               i;

always @(a or b or c or ci)
  begin
    carry[0] = c[0];
    carry[1] = (a[0]&b[0])|((a[0]^b[0])&ci);
    sum[0] = a[0]^b[0]^ci;
    for (i = 1; i <= width-2; i = i + 1) begin 
       carry[i+1] = (a[i]&b[i])|((a[i]^b[i])&c[i]);
       sum[i] = a[i]^b[i]^c[i];
    end    
    sum[width-1] = a[width-1]^b[width-1]^c[width-1];
    co = (a[width-1]&b[width-1])|((a[width-1]^b[width-1])&c[width-1]);
  end 
`endprotect
  endmodule  
