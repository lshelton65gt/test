/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_square. It returns the Square of A .

 -- ** Input       :    a

 -- ** Inout       :    -

 -- ** Output      :   square 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-22-2005


****************************************************************************/
module DW_square_carbon(a,tc,square);
parameter	width = 32;

input	[width-1:0]	a;
input	tc;
output	[width+width-1:0]	square;
`protect
reg	[width+width-1:0]	square;
reg     [width-1:0]  		abs_a ;
always@(a or tc)
begin
 abs_a = ~a + 1'b1;

 square= (tc && a[width-1]) ? ((abs_a)*(abs_a)): (a*a);
end

`endprotect
endmodule
