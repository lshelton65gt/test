/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_minmax. It calculates min/max of n-numbers.

	tc = 0  => Inputs are unsigned.
	tc = 1  => Inputs are signed.

	min_max = 0  => Output is minimum value of n-numbers.
	min_max = 1  => Output is maximum value of n-numbers.

	index reflects which number is min/max in the given set of inputs.

 -- ** Input       :    a ,tc ,min_max

 -- ** Inout       :    -

 -- ** Output      :    value ,index 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-05-2005

****************************************************************************/

module DW_minmax_carbon (a, tc, min_max, value, index);

parameter width = 32;
parameter num_inputs = 8;

`define index_width ((num_inputs>4096)? ((num_inputs>262144)? ((num_inputs>2097152)? ((num_inputs>8388608)? 24 : ((num_inputs> 4194304)? 23 : 22)) : ((num_inputs>1048576)? 21 : ((num_inputs>524288)? 20 : 19))) : ((num_inputs>32768)? ((num_inputs>131072)?  18 : ((num_inputs>65536)? 17 : 16)) : ((num_inputs>16384)? 15 : ((num_inputs>8192)? 14 : 13)))) : ((num_inputs>64)? ((num_inputs>512)?  ((num_inputs>2048)? 12 : ((num_inputs>1024)? 11 : 10)) : ((num_inputs>256)? 9 : ((num_inputs>128)? 8 : 7))) : ((num_inputs>8)? ((num_inputs> 32)? 6 : ((num_inputs>16)? 5 : 4)) : ((num_inputs>4)? 3 : ((num_inputs>2)? 2 : 1)))))

// closest multiple of 4 which is greater than or equal to num_inputs
`define ceil4_num_inputs (((num_inputs % 4) == 0)?num_inputs:(num_inputs + 4 - (num_inputs % 4)))

parameter IWidth = `index_width - 1;
   
input [num_inputs*width-1:0] a;
input tc;
input min_max;
output [width-1:0] value;
output [IWidth:0] index;

`protect

reg [width-1:0] inReg1, inReg2, inReg3, inReg4;
reg [width-1:0] tmpReg1, tmpReg2;
reg tmpIndex1, tmpIndex2;
reg ALessB, signEq,regLT;

reg [width-1:0] value;
reg [IWidth:0] index;

reg [width-1:0] partialResultMin[0:(`ceil4_num_inputs/4)-1];
reg [width-1:0] partialResultMax[0:(`ceil4_num_inputs/4)-1];

reg [1:0] partialIndexMin[0:(`ceil4_num_inputs/4)-1];
reg [1:0] partialIndexMax[0:(`ceil4_num_inputs/4)-1];

reg [width-1:0] minValue, maxValue;
reg [IWidth:0] minIndex, maxIndex;

reg [width-1:0] tmpResultMin, tmpResultMax;
reg [1:0] tmpIndexMin, tmpIndexMax;

reg [4*width-1:0] tmpReg;
integer i, j,tmp ,tmp1;

always @(min_max or minValue or minIndex or maxValue or maxIndex)
begin
	if (min_max)
	begin
		value = maxValue;
		index = maxIndex;
	end
	else
	begin
		value = minValue;
		index = minIndex;
	end
end

always @(a or tc)
begin
  for (i = 0; i < (`ceil4_num_inputs/4); i = i + 1) begin
    partialResultMin[i] = 0;
    partialResultMax[i] = 0;
    partialIndexMin[i] = 0;
    partialIndexMax[i] = 0;
  end

tmp =(`ceil4_num_inputs/4) ;
tmp1= (num_inputs % 4)  ;

	case (num_inputs )
        2:
	begin
		for(j=0; j<2*width; j=j+1)
			tmpReg[j] = a[j];
		// compare the two numbers
		if (isLessOrEqual(tmpReg[width-1:0], tmpReg[2*width-1:width], tc))
		begin
			minValue = tmpReg[width-1:0];
			minIndex = 2'b0;
			maxValue = tmpReg[2*width-1:width];
			maxIndex = 2'b1;
		end
		else
		begin
			minValue = tmpReg[2*width-1:width];
			minIndex = 2'b1;
			maxValue = tmpReg[width-1:0];
			maxIndex = 2'b0;
		end
	end
	3:
	begin
		for(j=0; j<3*width; j=j+1)
			tmpReg[j] = a[j];
		// compare the first two numbers
		if (isLessOrEqual(tmpReg[width-1:0], tmpReg[2*width-1:width], tc))
		begin
			tmpResultMin = tmpReg[width-1:0];
			tmpIndexMin = 2'b0;
			tmpResultMax = tmpReg[2*width-1:width];
			tmpIndexMax = 2'b1;
		end
		else
		begin
			tmpResultMin = tmpReg[2*width-1:width];
			tmpIndexMin = 2'b1;
			tmpResultMax = tmpReg[width-1:0];
			tmpIndexMax = 2'b0;
		end
		// compare the third number with result of first two numbers
		// for minimum...
		if (isLessOrEqual(tmpResultMin, tmpReg[3*width-1:2*width], tc))
		begin
			minValue = tmpResultMin;
			minIndex = tmpIndexMin;
		end
		else
		begin
			minValue = tmpReg[3*width-1:2*width];
			minIndex = 2'b10;
		end
		// for maximum...
		if (isLessOrEqual(tmpResultMax, tmpReg[3*width-1:2*width], tc))
		begin
			maxValue = tmpReg[3*width-1:2*width];
			maxIndex = 2'b10;
		end
		else
		begin
			maxValue = tmpResultMax;
			maxIndex = tmpIndexMax;
		end
	end
	default:
	begin
		for(i=0; i<(tmp)-1; i=i+1)
		begin
			for(j=0; j<4*width; j=j+1)
				tmpReg[j] = a[i*4*width+j];
			min_of_four(tmpReg, tc, partialResultMin[i], partialIndexMin[i]);
			max_of_four(tmpReg, tc, partialResultMax[i], partialIndexMax[i]);
		end
		case (tmp1)
	        0:
		begin
			for(j=0; j<4*width; j=j+1)
				tmpReg[j] = a[((tmp)-1)*4*width+j];
			min_of_four(tmpReg, tc, partialResultMin[((tmp)-1)], partialIndexMin[((tmp)-1)]);
			max_of_four(tmpReg, tc, partialResultMax[((tmp)-1)], partialIndexMax[((tmp)-1)]);
		end
		 1:
		begin
			for(j=0; j<width; j=j+1)
				tmpReg[j] = a[((tmp)-1)*4*width+j];
			partialResultMin[((tmp)-1)] = tmpReg[width-1:0];
			partialIndexMin[((tmp)-1)] = 2'b0;
			partialResultMax[((tmp)-1)] = tmpReg[width-1:0];
			partialIndexMax[((tmp)-1)] = 2'b0;
		end
		 2:
		begin
			for(j=0; j<2*width; j=j+1)
				tmpReg[j] = a[((tmp)-1)*4*width+j];
			// compare the two numbers
			if (isLessOrEqual(tmpReg[width-1:0], tmpReg[2*width-1:width], tc))
			begin
				partialResultMin[((tmp)-1)] = tmpReg[width-1:0];
				partialIndexMin[((tmp)-1)] = 2'b0;
				partialResultMax[((tmp)-1)] = tmpReg[2*width-1:width];
				partialIndexMax[((tmp)-1)] = 2'b1;
			end
			else
			begin
				partialResultMin[((tmp)-1)] = tmpReg[2*width-1:width];
				partialIndexMin[((tmp)-1)] = 2'b1;
				partialResultMax[((tmp)-1)] = tmpReg[width-1:0];
				partialIndexMax[((tmp)-1)] = 2'b0;
			end
		end
		 3:
		begin
			for(j=0; j<3*width; j=j+1)
				tmpReg[j] = a[((tmp)-1)*4*width+j];
			// compare the first two numbers
			if (isLessOrEqual(tmpReg[width-1:0], tmpReg[2*width-1:width], tc))
			begin
				tmpResultMin = tmpReg[width-1:0];
				tmpIndexMin = 2'b0;
				tmpResultMax = tmpReg[2*width-1:width];
				tmpIndexMax = 2'b1;
			end
			else
			begin
				tmpResultMin = tmpReg[2*width-1:width];
				tmpIndexMin = 2'b1;
				tmpResultMax = tmpReg[width-1:0];
				tmpIndexMax = 2'b0;
			end
			// compare the third number with result of first two numbers
			// for minimum...
			if (isLessOrEqual(tmpResultMin, tmpReg[3*width-1:2*width], tc))
			begin
				partialResultMin[((tmp)-1)] = tmpResultMin;
				partialIndexMin[((tmp)-1)] = tmpIndexMin;
			end
			else
			begin
				partialResultMin[((tmp)-1)] = tmpReg[3*width-1:2*width];
				partialIndexMin[((tmp)-1)] = 2'b10;
			end
			// for maximum...
			if (isLessOrEqual(tmpResultMax, tmpReg[3*width-1:2*width], tc))
			begin
				partialResultMax[((tmp)-1)] = tmpReg[3*width-1:2*width];
				partialIndexMax[((tmp)-1)] = 2'b10;
			end
			else
			begin
				partialResultMax[((tmp)-1)] = tmpResultMax;
				partialIndexMax[((tmp)-1)] = tmpIndexMax;
			end
		end
                endcase
		// minimum value calculation
		minValue = partialResultMin[0];
		minIndex = partialIndexMin[0];
		for(i=1; i<tmp; i=i+1)
		begin
		    if (isLess(partialResultMin[i], minValue, tc))
			begin
				minValue = partialResultMin[i];
				minIndex = (i << 2) + partialIndexMin[i];
			end
		end
		// maximum value calculation
		maxValue = partialResultMax[0];
		maxIndex = partialIndexMax[0];
		for(i=1; i<tmp; i=i+1)
		begin
		    if (isLessOrEqual(maxValue, partialResultMax[i], tc))
			begin
				maxValue = partialResultMax[i];
				maxIndex = (i << 2) + partialIndexMax[i];
			end
		end
	end
        endcase
end
// Determine whether first arg is Less or Equal to second arg
function isLessOrEqual;
input [width-1:0] A, B;
input tc;

reg ALessB, AEqB, signEq, regLT;
begin
	ALessB = (A < B);
	AEqB   = (A == B);
	signEq = (A[width-1] == B[width-1]);
	regLT = (!tc & ALessB) | (tc & (ALessB == signEq));
	isLessOrEqual = regLT | AEqB;
end

endfunction

// Determine whether first arg is Less to second arg
function isLess;
input [width-1:0] A, B;
input tc;

reg ALessB, signEq, regLT;
begin
	ALessB = (A < B);
	signEq = (A[width-1] == B[width-1]);
	isLess = (!tc & ALessB) | (tc & (ALessB == signEq));
end

endfunction

// Compute minimum of four numbers
task min_of_four;
input [4*width-1:0] inputReg;
input tc;
output [width-1:0] resultReg;
output [1:0] minorIndex;

reg [width-1:0] inReg1, inReg2, inReg3, inReg4;
reg [width-1:0] tmpReg1, tmpReg2;
reg tmpIndex1, tmpIndex2;
begin
	inReg1 = inputReg[width-1:0];
	inReg2 = inputReg[2*width-1:width];
	inReg3 = inputReg[3*width-1:2*width];
	inReg4 = inputReg[4*width-1:3*width];
	// compare the first two numbers
	if (isLessOrEqual(inReg1, inReg2, tc))
	begin
		tmpReg1 = inReg1;
		tmpIndex1 = 0;
	end
	else
	begin
		tmpReg1 = inReg2;
		tmpIndex1 = 1;
	end
	// compare the last two numbers
	if (isLessOrEqual(inReg3, inReg4, tc))
	begin
		tmpReg2 = inReg3;
		tmpIndex2 = 0;
	end
	else
	begin
		tmpReg2 = inReg4;
		tmpIndex2 = 1;
	end
	// compare the result of above two numbers
	if (isLessOrEqual(tmpReg1, tmpReg2, tc))
	begin
		resultReg = tmpReg1;
		minorIndex = {1'b0, tmpIndex1};
	end
	else
	begin
		resultReg = tmpReg2;
		minorIndex = {1'b1, tmpIndex2};
	end
end

endtask

// Compute maximum of four numbers
task max_of_four;
input [4*width-1:0] inputReg;
input tc;
output [width-1:0] resultReg;
output [1:0] minorIndex;

reg [width-1:0] inReg1, inReg2, inReg3, inReg4;
reg [width-1:0] tmpReg1, tmpReg2;
reg tmpIndex1, tmpIndex2;
begin
	inReg1 = inputReg[width-1:0];
	inReg2 = inputReg[2*width-1:width];
	inReg3 = inputReg[3*width-1:2*width];
	inReg4 = inputReg[4*width-1:3*width];
	// compare the first two numbers
	if (isLessOrEqual(inReg1, inReg2, tc))
	begin
		tmpReg1 = inReg2;
		tmpIndex1 = 1;
	end
	else
	begin
		tmpReg1 = inReg1;
		tmpIndex1 = 0;
	end
	// compare the last two numbers
	if (isLessOrEqual(inReg3, inReg4, tc))
	begin
		tmpReg2 = inReg4;
		tmpIndex2 = 1;
	end
	else
	begin
		tmpReg2 = inReg3;
		tmpIndex2 = 0;
	end
	// compare the result of above two numbers
	if (isLessOrEqual(tmpReg1, tmpReg2, tc))
	begin
		resultReg = tmpReg2;
		minorIndex = {1'b1, tmpIndex2};
	end
	else
	begin
		resultReg = tmpReg1;
		minorIndex = {1'b0, tmpIndex1};
	end
end

endtask

`endprotect

`undef index_width
`undef ceil4_num_inputs

endmodule
