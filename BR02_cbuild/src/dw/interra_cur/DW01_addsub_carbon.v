/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_addsub. It returns ADD and SUB of A, B and CI.when control ADD_SUB is 1 than SUB will perform else ADD .

 -- ** Input       :    A,B,CI ,ADD_SUB

 -- ** Inout       :    -

 -- ** Output      :   SUM , CO 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-15-2005


****************************************************************************/

module DW01_addsub_carbon(A, B, CI, ADD_SUB, SUM, CO);
   parameter width = 32;
   input [(width - 1):0]  A;
   input [(width - 1):0]  B;
   input 		  CI;
   input 		  ADD_SUB;
   output [(width - 1):0] SUM;
   output 		  CO;
`protect
   reg [(width - 1):0] 		SUM;
   reg 				CO;
   reg [width:0] 		tmp;
   
   always @(A or B or CI or ADD_SUB)
     begin
	if (ADD_SUB)
	  tmp = A-B-CI;
	else
          tmp = A+B+CI;
	CO = tmp[width];
	SUM = tmp[width-1:0];
     end
`endprotect
endmodule
