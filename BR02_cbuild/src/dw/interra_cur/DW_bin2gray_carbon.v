/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_bin2gray. It returns grey.

 -- ** Input       :    B

 -- ** Inout       :    -

 -- ** Output      :   G

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-15-2005


****************************************************************************/

module DW_bin2gray_carbon(B ,G);
parameter width=32;

input [width-1:0] B;
output [width-1:0] G;

`protect
assign G  = (B>>1) ^B;
`endprotect
endmodule
