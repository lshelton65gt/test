/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_dec. It returns decrementvalue of A.

 -- ** Input       :    A

 -- ** Inout       :    -

 -- ** Output      :    SUM  

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-19-2005


****************************************************************************/
module DW01_dec_carbon (A,SUM);
   parameter width=32;
   input [ width-1: 0] 	A; 
   output [ width-1: 0] SUM;  
`protect
assign  SUM=A-1;
`endprotect
endmodule
