/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_mac.  it returns MAC.

 -- ** Input       :    A , B , C , TC

 -- ** Inout       :    -

 -- ** Output      :    MAC 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-03-2005

****************************************************************************/

module DW02_mac_carbon (A, B, C, TC, MAC);
  parameter A_width=32;
  parameter B_width=32;
  output [ A_width+B_width- 1: 0] MAC;
  input [ A_width- 1: 0] A;
  input [ B_width- 1: 0] B;
  input [ A_width+B_width- 1: 0] C;
  input TC;
`protect
   reg [ A_width+B_width- 1: 0] MAC;
   reg [ A_width- 1: 0] 	a1;
   reg [ B_width- 1: 0] 	b1;
   reg [ A_width+B_width- 1: 0] c1;

   always @(TC or A or B or C)
     begin
	if ( TC === 1'b1 ) 
	  begin  // signed multiplication
           case ({A[A_width-1] ,B[B_width-1]})
           2'b00 :begin 
                       a1 =A ;
		       b1 = B;
		       c1 = a1*b1;
		       MAC = c1+C;
                  end
           2'b01: begin  
                       a1 =A ;                          
	       	       b1 = -B;
		       c1 = a1*b1;
		       MAC = -c1+C;
                  end   
           2'b10 : begin
                       a1 = -A ;            
                       b1 = B;
		       c1 = a1*b1;
		       MAC = -c1+C;
                   end
		   
           2'b11: begin
                       a1 = -A ;
                       b1 = -B;
		       c1 = a1*b1;
		       MAC = c1+C;
                   end    
            endcase
           end
         else
         MAC = A*B+C;
     end
`endprotect
endmodule
