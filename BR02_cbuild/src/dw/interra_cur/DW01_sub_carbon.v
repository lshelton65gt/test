/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_add. It returns difference of A, B and CI.

 -- ** Input       :    A,B,CI

 -- ** Inout       :    -

 -- ** Output      :    DIFF , CO 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-15-2005


****************************************************************************/

module DW01_sub_carbon(A, B, CI, DIFF, CO);
   parameter width = 32;
   input [(width - 1):0]  A;
   input [(width - 1):0]  B;
   input 		  CI;
   output [(width - 1):0] DIFF;
   output 		  CO;
`protect
   reg [(width - 1):0] 	  DIFF;
   reg 			  CO;
   reg [width:0] 	  tmp;

   always @(A or B or CI)
     begin
	tmp = A - B - CI;
	CO = tmp[width];
	DIFF = tmp[width-1:0];
     end

`endprotect
endmodule
