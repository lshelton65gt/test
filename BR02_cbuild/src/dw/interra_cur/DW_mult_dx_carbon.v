/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_mult_dx. It returns PRODUCT of A,B .

 -- ** Input       :    A,B,TC,DPLX

 -- ** Inout       :    -

 -- ** Output      :    PRODUCT

 -- ** Date        :    08-30-2005


****************************************************************************/


module DW_mult_dx_carbon (a, b, tc, dplx, product );

   parameter width = 32;
   parameter p1_width = 16;

   parameter p2_width = width-p1_width ;

   input [width-1 : 0] a;
   input [width-1 : 0] b;
   input 	       tc;
   input 	       dplx;
   output [2*width-1 : 0] product;
`protect
   wire [width-1 : 0] 	  a,a1,a2;
   wire [width-1 : 0] 	  b,b1,b2;
   wire 		  tc;
   wire 		  dplx;
   wire [2*width-1 : 0]   product;
   wire [2*width-1 : 0]   duplex_prod;
   wire [2*width-1 : 0]   simplex_prod;
wire   [width-1:0]  regAbsA,regAbsB ;
wire [p1_width-1:0]regAbsA1,regAbsB1;
wire   [p2_width-1:0]  regAbsA2,regAbsB2;

wire  outSignBit,outSignBit1,outSignBit2;



assign    regAbsA = (a[width-1] & tc)? (~a + 1'b1) : a;
assign    regAbsB = (b[width-1] & tc)? (~b + 1'b1) : b;
assign    outSignBit = (a[width-1] ^ b[width-1]) & tc;
assign    simplex_prod = outSignBit?(~(regAbsA * regAbsB) + 1'b1):(regAbsA * regAbsB);

assign a1 = a[p1_width-1 : 0] ;
assign b1 = b[p1_width-1 : 0] ;
assign a2 = a[width-1:p1_width ] ;
assign b2 = b[width-1:p1_width ] ;

assign    regAbsA1 = (a1[p1_width-1] & tc)? (~a1 + 1'b1) : a1;
assign    regAbsB1 = (b1[p1_width-1] & tc)? (~b1 + 1'b1) : b1;
assign    outSignBit1 = (a1[p1_width-1] ^ b1[p1_width-1]) & tc;
assign   duplex_prod[2*p1_width-1 : 0] = outSignBit1?(~(regAbsA1 * regAbsB1)+ 1'b1):regAbsA1 * regAbsB1;

   

assign    regAbsA2 = (a2[p2_width-1] & tc)? (~a2 + 1'b1) : a2;
assign    regAbsB2 = (b2[p2_width-1] & tc)? (~b2 + 1'b1) : b2;
assign    outSignBit2 = (a2[p2_width-1] ^ b2[p2_width-1]) & tc;
assign    duplex_prod[2*width-1 : 2*p1_width] = outSignBit2?(~ (regAbsA2 * regAbsB2)+ 1'b1):regAbsA2 *regAbsB2;


assign  product =  dplx == 1'b0 ? simplex_prod : duplex_prod ;

`endprotect
endmodule
