/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW02_prod_sum1. It calculates sum-of-product.

 -- ** Input       :    A , B ,C, TC

 -- ** Inout       :    -

 -- ** Output      :    SUM

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-29-2005

****************************************************************************/

module DW02_prod_sum1_carbon (A,B,C,TC,SUM);
  parameter A_width = 16;
  parameter B_width = 16;
  parameter SUM_width = 32;

  input [A_width-1:0] A;
  input [B_width-1:0] B;
  input [SUM_width-1:0] C;
  input TC;
  output [SUM_width-1:0] SUM;

  reg [SUM_width-1:0] prod2;  // carbon ignoreSynthCheck
   
`protect

  parameter K = A_width+B_width;
  parameter SW = (SUM_width < K) ? SUM_width : K;
   
  reg [SUM_width-1:0] temp1,SUM,temp2;
  reg [K-1:0] prod1;
  reg [A_width-1:0] abs_a;
  reg [B_width-1:0] abs_b;

  always @(A or B or C or TC) 
   begin
     abs_a = (A[A_width-1])? (~A + 1'b1) : A;
     abs_b = (B[B_width-1])? (~B + 1'b1) : B;
 
     temp1 = abs_a * abs_b;
     temp2 = ~(temp1 - 1'b1);
 
     case (TC) 
      1'b0:
       SUM = A*B +C;
     1'b1:
      begin
         prod1 =  ((A[A_width-1] ^ B[B_width-1]) && (|temp1))? temp2 : temp1; 
        case (SUM_width >= K)
        1'b1:
         begin
              prod2[K-1:0] = prod1[K-1:0];
              // BUGfix:  sign extension was incorrect. Must specify width for number of 1'b1s 
              if (SUM_width > K)
                  prod2[SUM_width-1:K] =(prod1[K-1])? {(SUM_width-K){1'b1}}: {(SUM_width-K){1'b0}};
         end
        1'b0:
              // if SUM_width > K then prod1[SUM_width-1:0] will produce a compile time warning
              //  "part select out of range".  SW is chosen as the smaller of SUM_width and K
              prod2 = prod1[SW-1:0];
         endcase
        SUM = prod2+C;
      end
      endcase
   end
`endprotect
endmodule


