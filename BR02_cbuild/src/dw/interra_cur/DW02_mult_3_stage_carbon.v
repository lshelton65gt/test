/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW02_mult_3_stage. It returns mult of  of A, B .

 -- ** Input       :    A,B,TC,CLK

 -- ** Inout       :    -

 -- ** Output      :   PRODUCT 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-01-2005


****************************************************************************/
module DW02_mult_3_stage_carbon(A,B,TC,CLK,PRODUCT);
parameter	A_width = 8;

parameter	B_width = 8;
input	[A_width-1:0]	A;
input	[B_width-1:0]	B;
input	TC,CLK;
output	[A_width+B_width-1:0]	PRODUCT;
`protect
reg	[A_width+B_width-1:0]	PRODUCT,product_piped1;
reg	[A_width+B_width-1:0]	pre_product;

reg   [A_width-1:0]  regAbsA;
reg   [B_width-1:0]  regAbsB;

reg  outSignBit;

always @ (posedge CLK)
begin
	product_piped1 <= pre_product;
	PRODUCT <= product_piped1;
end

always @(A or B or TC)
begin
    regAbsA = (A[A_width-1] & TC)? (~A + 1'b1) : A;
    regAbsB = (B[B_width-1] & TC)? (~B + 1'b1) : B;
    outSignBit = (A[A_width-1] ^ B[B_width-1]) & TC;
    pre_product = outSignBit?(~(regAbsA * regAbsB) + 1'b1):regAbsA * regAbsB;
end

`endprotect
endmodule
