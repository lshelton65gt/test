/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_prod_sum_pipe. It calculates sum-of-product.

 -- ** Input       :    CLK, RST, EN, TC, A, B

 -- ** Inout       :    -

 -- ** Output      :    SUM

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    09-07-2005

****************************************************************************/

`include "../DW02_prod_sum/DW02_prod_sum_carbon.v"
module DW_prod_sum_pipe_carbon (clk,rst_n,en,tc,a,b,sum);
   
   parameter a_width = 2;
   parameter b_width = 2;
   parameter num_inputs = 2;
   parameter sum_width = a_width + b_width + num_inputs - 1;
   parameter num_stages = 2;
   parameter stall_mode = 1;
   parameter rst_mode = 1;

   
   input clk;
   input rst_n;
   input [a_width*num_inputs-1 : 0] a;
   input [b_width*num_inputs-1 : 0] b;
   input tc;
   input en;
   
   output [sum_width-1: 0] sum;
`protect
   wire   a_rst_n;
   wire [sum_width-1: 0] sum_wire  ;
   wire [sum_width-1: 0] sum  ;
   reg [a_width*num_inputs-1 : 0] a_reg[0 : num_stages-1];
   reg [b_width*num_inputs-1 : 0] b_reg[0 : num_stages-1];
   reg tc_reg[0 : num_stages-1];
   integer i,j;

 assign a_rst_n = (rst_mode == 1)? rst_n : 1'b1;
     always @( posedge clk or negedge a_rst_n)
      begin
         

        if (a_rst_n === 1'b0) begin
         a_reg[0]  = a;
	 b_reg[0]  = b;
	 tc_reg[0] = tc;
	    for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {a_width{1'b0}};
	       b_reg[i]  = {b_width{1'b0}};
	       tc_reg[i] = 1'b0;
	    end // for (i= 1; i < num_stages-1; i++)
	 end //

         else if (rst_mode != 0 & rst_n === 1'b0) begin
         a_reg[0]  = a;
	 b_reg[0]  = b;
	 tc_reg[0] = tc;
	    for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {a_width{1'b0}};
	       b_reg[i]  = {b_width{1'b0}};
	       tc_reg[i] = 1'b0;
	    end // for (i= 1; i < num_stages-1; i++)
	 end // if (rst_mode != 0 & rst_n === 1'b0)
	 else if  (rst_mode == 0 || rst_n === 1'b1) begin
            a_reg[0]  = a;
	    b_reg[0]  = b;
	    tc_reg[0] = tc;
	     if (stall_mode != 0 && en === 1'b0) begin
	       for (i= 1; i < num_stages; i=i+1) begin
		  a_reg[num_stages-i]  = a_reg[num_stages-i];
		  b_reg[num_stages-i]  = b_reg[num_stages-i];
		  tc_reg[num_stages-i] = tc_reg[num_stages-i];
	       end 
	    end 
	    else if (stall_mode == 0 || en === 1'b1) begin
	       
	       for (i= 1; i < num_stages; i=i+1) begin
		  a_reg[num_stages-i]  = a_reg[num_stages-i-1];
		  b_reg[num_stages-i]  = b_reg[num_stages-i-1];
		  tc_reg[num_stages-i] = tc_reg[num_stages-i-1];
	       end 
	    end  
        end
      end 
     DW02_prod_sum_carbon #(a_width, b_width, num_inputs, sum_width)
      U1 (.A(a_reg[num_stages-1]),
	  .B(b_reg[num_stages-1]),
	  .TC(tc_reg[num_stages-1]),
	  .SUM(sum_wire));
assign  sum  = sum_wire ;
 `endprotect 
endmodule 
