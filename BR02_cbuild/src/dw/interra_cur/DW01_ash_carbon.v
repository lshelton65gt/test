/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////


 -- ** Description : This module contains functionality of DesignWare component DW01_ash.It is a general purpose arithmetic shifter.The result is stored in register B.
 
 -- ** Input       :    A, DATA_TC, SH, SH_TC

 -- ** Inout       :    -

 -- ** Output      :    B 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-20-2005


****************************************************************************/
module DW01_ash_carbon(A, DATA_TC, SH, SH_TC, B);

parameter A_width=8;
parameter SH_width=4;

input   [A_width-1:0]  A;
input                  DATA_TC;
input   [SH_width-1:0] SH;
input                  SH_TC;
output  [A_width-1:0]  B;

`protect
reg  [A_width-1:0]  B;

reg   [SH_width-1:0] shAbsReg;
reg   [2*A_width-1:0]  signedA;

always @(A or DATA_TC or SH or SH_TC)
begin
    shAbsReg = ~SH + 1;
	if (DATA_TC === 1'b0)
	    signedA = {{A_width{1'b0}}, A};
	else
	    signedA = {{A_width{A[A_width-1]}}, A};

    if ((SH_TC === 1'b0) || (SH[SH_width-1] === 1'b0))
	    B = A << SH;
	else
		B = signedA >> shAbsReg;
end
`endprotect
endmodule

