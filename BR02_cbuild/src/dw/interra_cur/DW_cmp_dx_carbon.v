/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_cmp_dx. It is a comparator with just six outputs. 

 -- ** Input       :     A, B, TC ,DPLX

 -- ** Inout       :    -

 -- ** Output      :     LT1, GT1, EQ1, LT2, GT2, EQ2

 -- ** Date        :    08-22-2005

 **************************************************************** */

module DW_cmp_dx_carbon(a, b, tc, dplx, lt1, eq1, gt1, lt2, eq2, gt2);

  parameter width    = 32; 
  parameter p1_width = 16; 

  input [ width- 1: 0] a, b;
  input tc;
  input dplx;
  output lt1, eq1, gt1;
  output lt2, eq2, gt2;
`protect
  reg lt1, eq1, gt1;
  integer i;
    reg       [p1_width-1:0] part1_a, part1_b;
    reg       [width-p1_width-1:0] part2_a, part2_b;

  reg lt2, eq2, gt2,ALessB,signEq,regLT,ALessB1,signEq1,regLT1,ALessB2,signEq2,regLT2,ALessB3,signEq3,regLT3;

  
  always  @(a or b or tc or dplx)
  begin 
    

    for ( i = 0; i < width-p1_width; i=i+1)
      begin
        part2_a[i] = a[i+p1_width];
        part2_b[i] = b[i+p1_width];
      end

        part1_a[p1_width-1 :0] = a[p1_width-1:0] ;
        part1_b[p1_width-1:0]  = b[p1_width-1:0] ;

     if ( dplx === 1'b0 ) begin
        ALessB = (a < b);
        signEq = (a[width-1] === b[width-1]);
	regLT = (!tc & ALessB) | (tc & (ALessB === signEq));
        if (regLT) begin
	lt2 = 1'b1;
         eq2 = 1'b0;
        gt2 = 1'b0;
      end else if ( a === b ) begin
        lt2 = 1'b0;
        eq2 = 1'b1;
        gt2 = 1'b0;
      end else begin
        lt2 = 1'b0;
        eq2 = 1'b0;
        gt2 = 1'b1;
      end 

      ALessB1 = (part1_a < part1_b);
        if (ALessB1)begin
        lt1 = 1'b1;
        eq1 = 1'b0;
        gt1 = 1'b0;
      end else if ( part1_a === part1_b ) begin
        lt1 = 1'b0;
        eq1 = 1'b1;
        gt1 = 1'b0;
      end else begin
        lt1 = 1'b0;
        eq1 = 1'b0;
        gt1 = 1'b1;
      end 
   end  
      
      else begin
       ALessB2 = (part2_a < part2_b);
	signEq2 = (part2_a[width-p1_width-1] === part2_b[width-p1_width-1]);
	regLT2 = (!tc & ALessB2) | (tc & (ALessB2 === signEq2));
        if (regLT2)begin
        lt2 = 1'b1;
        eq2 = 1'b0;
        gt2 = 1'b0;
      end else if ( part2_a === part2_b ) begin
        lt2 = 1'b0;
        eq2 = 1'b1;
        gt2 = 1'b0;
      end else begin
        lt2 = 1'b0;
        eq2 = 1'b0;
        gt2 = 1'b1;
      end    
    
         
       ALessB3 = (part1_a < part1_b);
	signEq3 = (part1_a[p1_width-1] == part1_b[p1_width-1]);
	regLT3 = (!tc & ALessB3) | (tc & (ALessB3 == signEq3));
         if (regLT3)begin
        lt1 = 1'b1;
        eq1 = 1'b0;
         gt1 = 1'b0;
          end else if ( part1_a === part1_b ) begin
        lt1 = 1'b0;
        eq1 = 1'b1;
        gt1 = 1'b0;
      end else begin
        lt1 = 1'b0;
        eq1 = 1'b0;
        gt1 = 1'b1;
       end  
    end  
  end 
`endprotect
endmodule 

