/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_gray2bin. It returns grey.

 -- ** Input       :    G

 -- ** Inout       :    -

 -- ** Output      :    B

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-15-2005


****************************************************************************/

module DW_gray2bin_carbon(G ,B);
parameter width=32;

input [width-1:0] G;
output [width-1:0] B;

`protect
reg  [width-1:0] B;
integer i;
always @(G)
begin

B[width-1]   = G[width-1] ;
for(i =width-2;i>=0;i=i-1)
B[i]=B[i+1]^G[i];
end
`endprotect
endmodule
