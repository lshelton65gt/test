/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_cmp6. It is a comparator with just six outputs. 

 -- ** Input       :     A, B, LEQ, TC

 -- ** Inout       :    -

 -- ** Output      :     LT, GT, EQ, LE, GE, NE

 -- ** Date        :    08-01-2005

 **************************************************************** */


module DW01_cmp6_carbon(A, B, TC, LT, GT, EQ, LE, GE, NE);

parameter width=32;

input   [width-1:0]  A,B;
input                TC;
output               LT;
output               GT;
output               EQ;
output               LE;
output               GE;
output               NE;
`protect
reg               LT;
reg               GT;
reg               EQ;
reg               LE;
reg               GE;
reg               NE;

reg               ALessB, AEqB, AGreaterB;
reg               signEq;


always @(A or B or TC)
begin
    ALessB = (A < B);
	AEqB   = (A == B);
	AGreaterB = !(ALessB | AEqB);
	signEq = (A[width-1] == B[width-1]);
	LT = (!TC & ALessB) | (TC & (ALessB == signEq));
	GT = (!TC & AGreaterB) | (TC & (AGreaterB == signEq));
	EQ = AEqB;
	LE = LT | EQ;
	GE = GT | EQ;
	NE = !EQ;
end	
 
`endprotect

endmodule

