/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_inc_gray. It returns increment gray value.

 -- ** Input       :    A,B,TC,CLK

 -- ** Inout       :    -

 -- ** Output      :   PRODUCT 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-01-2005


****************************************************************************/

module DW_inc_gray_carbon(a,ci,z);
		
parameter width=32;
input[width-1:0] a;
input ci;

output[width-1:0] z;

`protect
reg[width-1:0] temp1,z,temp2;
integer i;

always@(a or ci)
begin
temp1[width-1] = a[width-1];

for (i = width-2; i >= 0; i = i-1)
	   temp1[i] = a[i] ^ temp1[i+1];

	temp2 = temp1 + ci;
  	z =  temp2 ^ (temp2 >> 1);

end

`endprotect

endmodule



