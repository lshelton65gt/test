/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_cmp2. It is a comparator with just two outputs. 

 -- ** Input       :     A, B, LEQ, TC

 -- ** Inout       :    -

 -- ** Output      :    LT_LE, GE_GT

 -- ** Date        :    07-05-2005

 **************************************************************** */

module DW01_cmp2_carbon(A, B, LEQ, TC, LT_LE, GE_GT);

parameter width=32;

input [width-1: 0] A, B;
input LEQ; 
input TC; 
output LT_LE;
output GE_GT;
`protect
reg    LT_LE;
reg    GE_GT;

reg    ALessB, AEqB, signEq, regLT;

always @(A or B or LEQ or TC)
begin
    ALessB = (A < B);
    AEqB   = (A == B);
	signEq = (A[width-1] == B[width-1]);
	regLT = (!TC & ALessB) | (TC & (ALessB == signEq));
	LT_LE = regLT | (LEQ & AEqB);
    GE_GT = ~LT_LE; 
end
`endprotect
endmodule

