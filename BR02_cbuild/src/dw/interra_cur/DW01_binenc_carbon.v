/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_binenc. It returns ADDR Value of A.

 -- ** Input       :    A

 -- ** Inout       :    -

 -- ** Output      :    ADDR 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-25-2005


****************************************************************************/


module DW01_binenc_carbon (A, ADDR);
	parameter A_width = 32;
	parameter ADDR_width = 5;

	input [A_width-1:0] A;
	output [ADDR_width-1:0] ADDR;

`protect
	integer flag,i;
	reg [ADDR_width-1:0] ADDR;


	always@(A)
	begin
	ADDR = -1;
    flag = 0;
	for (i=0;(!flag)&&(i<A_width);i=i+1) 
    	if (A[i] === 1'b1) 
		begin
			 flag = 1;
        	        ADDR = i;
	    end
	end
`endprotect

endmodule


