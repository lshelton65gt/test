/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_decode.  It is  binary decoder module.

 -- ** Input       :    A

 -- ** Inout       :    -

 -- ** Output      :   B 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-05-2005

****************************************************************************/

module DW01_decode_carbon(A, B);

parameter width=5;
parameter dec_width = 1 << width;

input   [width-1:0]  A;
output  [dec_width-1:0]  B;

`protect
reg[dec_width-1 :0] B;

always@(A) 
 B = 1'b1<<A;

`endprotect
endmodule

