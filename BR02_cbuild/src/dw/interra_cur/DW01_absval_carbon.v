/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_absval.It returns absoulte value of the input.

 -- ** Input       :    A,B,CI

 -- ** Inout       :    -

 -- ** Output      :   SUM , CO 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-02-2005


****************************************************************************/

module DW01_absval_carbon(A, ABSVAL);

parameter width=32;

input   [width-1:0]  A;
output  [width-1:0]  ABSVAL;
`protect
assign ABSVAL = (A[width-1])? -A :  A ;
`endprotect
endmodule

