/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_incdec. It is an incrementer-decrementer.

 -- ** Input       :    A ,INC_DEC

 -- ** Inout       :    -

 -- ** Output      :    SUM 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-04-2005

****************************************************************************/

module DW01_incdec_carbon(A, INC_DEC, SUM);

parameter width=32;

output  [ width-1 : 0]    SUM;   
input   [ width-1 : 0]    A; 
input                     INC_DEC; 
`protect
reg  [width-1:0]  SUM;

always @(A or INC_DEC)
  SUM  	= (INC_DEC )? A - 1: A + 1;
`endprotect
endmodule

