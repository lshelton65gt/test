/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_bsh.It shifts register A in cyclic order with value in SH register. The result is stored in register B.
 
 -- ** Input       :    A,SH

 -- ** Inout       :    -

 -- ** Output      :    B 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-20-2005


****************************************************************************/

module DW01_bsh_carbon(A, SH, B);

parameter A_width  = 32;
parameter SH_width = 5;

input   [A_width-1:0]   A;
input   [SH_width-1:0]  SH;
output  [A_width-1:0]   B;
`protect
reg     [A_width-1:0]   B;
reg     [A_width-1:0] temp;

always @(A or SH)
begin
if (SH == 0)
	B = A;
else
begin

temp = A ;
B = A << (SH % A_width);
temp = A >> (A_width-(SH % A_width));
B = B + temp;  
end
end
`endprotect
endmodule
