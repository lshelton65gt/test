/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_div_pipe. It returns quatient and remainder.

 -- ** Input       :    a,b

 -- ** Inout       :    -

 -- ** Output      :    quotient,remainder,div_by_0

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-12-2005


****************************************************************************/
`include "../DW_div/DW_div_carbon.v"
module DW_div_pipe_carbon (clk,rst_n,en,a,b,quotient,remainder,divide_by_0);
   
   parameter a_width = 7;
   parameter b_width = 7;
   parameter tc_mode = 0;
   parameter rem_mode = 0;
   parameter num_stages = 2;
   parameter stall_mode = 1;
   parameter rst_mode = 1;

   input clk;
   input rst_n;
   input [a_width-1:0] a;
   input [b_width-1:0] b;
   input en;
   
   output [a_width-1:0] quotient;
   output [b_width-1:0] remainder;
   output divide_by_0;
`protect
   wire a_rst_n;
   reg [a_width-1:0] a_reg[0:num_stages-1];
   reg [b_width-1:0] b_reg[0:num_stages-1];
   integer i,j;
assign a_rst_n = (rst_mode == 1)? rst_n : 1'b1;

always @(posedge clk or negedge a_rst_n )
begin
            if (a_rst_n === 1'b0) begin
   	    a_reg[0] = a;
	    b_reg[0] = b;
	      for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {a_width{1'b0}};
	       b_reg[i]  = {b_width{1'b0}};
	      end // for (i= 1; i < num_stages-1; i++)
	    end 



        else  if (rst_mode != 0 & rst_n === 1'b0) begin
	a_reg[0] = a;
	b_reg[0] = b;

	    for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {a_width{1'b0}};
	       b_reg[i]  = {b_width{1'b0}};
	    end // for (i= 1; i < num_stages-1; i++)
	 end // if (rst_mode != 0 & rst_n === 1'b0)
	 else if  (rst_mode == 0 || rst_n === 1'b1) begin

	a_reg[0] = a;
	b_reg[0] = b;
		if (stall_mode != 0 && en === 1'b0) 
		begin
	       for (i=1;i < num_stages;i=i+1) 
		   begin
				a_reg[num_stages-i] = a_reg[num_stages-i];
				b_reg[num_stages-i] = b_reg[num_stages-i];
	       end	       
	    end
	    else if (stall_mode == 0 || en === 1'b1) 
		begin // pipeline enabled
	    	for (i= 1;i < num_stages;i=i+1) 
			begin
				a_reg[num_stages-i] = a_reg[num_stages-i-1];
		  		b_reg[num_stages-i] = b_reg[num_stages-i-1];
	        end
	    end
     end
end
   
DW_div_carbon #(a_width,b_width,tc_mode,rem_mode)
      U1 (.a(a_reg[num_stages-1]),.b(b_reg[num_stages-1]),
	  .quotient(quotient),.remainder(remainder),.divide_by_0(divide_by_0));
`endprotect
endmodule

