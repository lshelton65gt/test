/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_squarep. It returns out0,out1  .

 -- ** Input       :    a,tc

 -- ** Inout       :    -

 -- ** Output      :   out0,out1 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    09-05-2005


****************************************************************************/
module DW_squarep_carbon(a, tc, out0, out1);

parameter width = 16;

input [width-1 : 0]	a;
input			tc;
output [2*width-1 : 0]	out0, out1;
reg [2*width-1 : 0] pp_array [0 : width-1];  // carbon ignoreSynthCheck
`protect
reg    [2*width-1 : 0]	out0, out1;
reg [width-1 : 0] a_temp;
reg   [width-1 : 0]	abs_a;
reg [2*width-1 : 0] tmp_pp_sum, tmp_pp_carry;
integer indx, pp_count,tmp;

    always @ (a or tc)
	begin 

		case(tc & a[width-1])
	 1'b0:
	    abs_a = a;
	
	1'b1:
	    begin
	    if ((tc & a[width-1]) === 1'b1)
		begin
		a_temp = ~a;

		for (	indx = 0;((a_temp[indx] === 1'b1) && (indx < width)); indx = indx + 1)

		    a_temp[indx] = 1'b0;
		
		if (indx < width)
		    a_temp[indx] = ~a_temp[indx];
		
		abs_a = a_temp;
		end
	    
	    else
		abs_a = {width{1'bx}};
	    end
           endcase
	end
    

    always @ (abs_a or pp_count)
	begin 
           tmp = (pp_count % 3) ;
	    if (width > 1)
		begin
		for (indx=1 ; indx < width ; indx=indx+1)
		pp_array[indx] = (abs_a[indx] === 1'b1)?{{width{1'b0}}, abs_a} << indx :{2*width{1'b0}};
		end
	    
		pp_array[0] = (abs_a[0] === 1'b1)?{{width{1'b0}},abs_a}:{2*width{1'b0}};

	   
	       for (pp_count = width; pp_count > 2 ;pp_count = pp_count - (pp_count/3))
		begin
		for (indx=0 ; indx < (pp_count/3) ; indx=indx+1)
		    begin
		    tmp_pp_sum = pp_array[indx*3] ^ pp_array[indx*3+1] ^ pp_array[indx*3+2];
		    
		    tmp_pp_carry = pp_array[indx*3] & pp_array[indx*3+1] |
				   pp_array[indx*3+1] & pp_array[indx*3+2] |
				   pp_array[indx*3+2] & pp_array[indx*3];
		    
		    pp_array[indx*2] = tmp_pp_sum;
		    pp_array[indx*2+1] = {tmp_pp_carry[width*2-2:0], 1'b0};
		    end
		
		if ( tmp > 0 )
		    begin
		    for (indx=0 ; indx < tmp ; indx=indx+1)
		      pp_array[2 * (pp_count/3) + indx] =pp_array[3 * (pp_count/3) + indx];
		    end
		
		end
	    
	        out0 = pp_array[0];
		out1 = (pp_count == 1)?{width*2{1'b0}}:pp_array[1];
	       
	end

`endprotect
endmodule

