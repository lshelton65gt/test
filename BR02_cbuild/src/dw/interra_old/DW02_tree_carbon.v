/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW02_tree.  	It is Wallace Tree Summer.

 -- ** Input       :    INPUT

 -- ** Inout       :    -

 -- ** Output      :    OUT0 ,OUT1

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-02-2005

****************************************************************************/

module DW02_tree_carbon( INPUT, OUT0, OUT1 );

parameter num_inputs = 4;
parameter input_width = 32;

input [num_inputs*input_width-1 : 0]	INPUT;
output [input_width-1:0]		OUT0, OUT1;
`protect
reg    [input_width-1:0]		tmp_out0, tmp_out1;

always @ (INPUT) 
begin : proc 
	reg [input_width-1 : 0] input_array [0 : num_inputs-1];
	reg [input_width-1 : 0] temp_array [0 : num_inputs-1];
	reg [input_width-1 : 0] f, g;
	integer h, i, j,tmp,tmp1, p, q;
	
	for (i=0 ; i < num_inputs ; i=i+1) begin
            temp_array[i] = 0;
	    for (j=0 ; j < input_width ; j=j+1) 
			f[j] = INPUT[i*input_width+j];
	    input_array[i] = f;
	end 

	h = num_inputs;

        p = h;
	for(q=h; q > 2; q= q-1) begin
              tmp = q/3 ;
              tmp1 = q % 3 ; 

		if (q == h) begin
	    	for (i=0 ; i < (tmp) ; i = i+1) begin 
				temp_array[i*2] = input_array[i*3] ^ input_array[i*3+1] ^ input_array[i*3+2];
				g = (input_array[i*3] & input_array[i*3+1]) | (input_array[i*3+1] & input_array[i*3+2]) | (input_array[i*3] & input_array[i*3+2]);
				temp_array[i*2+1] = g << 1;
	    	end
			if ((tmp1) > 0) 
				for (i=0 ; i < (tmp1) ; i = i + 1)
		    		temp_array[2 * (tmp) + i] = input_array[3 * (tmp) + i];   	
			for (i=0 ; i < num_inputs ; i = i + 1)
				input_array[i] = temp_array[i];
        	p = q;
		end
		else if (q == (p-p/3)) begin
			for (i=0 ; i < (tmp) ; i = i+1) begin
				temp_array[i*2] = input_array[i*3] ^ input_array[i*3+1] ^ input_array[i*3+2];
				g = (input_array[i*3] & input_array[i*3+1]) | (input_array[i*3+1] & input_array[i*3+2]) | (input_array[i*3] & input_array[i*3+2]);
				temp_array[i*2+1] = g << 1;
	    	end
			if ((tmp1) > 0) 
				for (i=0 ; i < (tmp1) ; i = i + 1)
		    		temp_array[2 * (tmp) + i] = input_array[3 * (tmp) + i];
	    	for (i=0 ; i < num_inputs ; i = i + 1)
				input_array[i] = temp_array[i];
        	p = q;
		end
	end
	
	tmp_out0 <= input_array[0];

        tmp_out1 <=(h > 1)? input_array[1]:{input_width{1'b0}};
end //proc

assign OUT0 = tmp_out0;
assign OUT1 = tmp_out1;
`endprotect
endmodule
