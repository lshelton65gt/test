/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_addsub_dx. It is Duplex Adder-Subtractor. 

 -- ** Input       :     A, B, CI1,CI2,ADDSUB,TC,SAT,AVG ,DPLX

 -- ** Inout       :    -

 -- ** Output      :     SUM ,CO1,CO2

 -- ** Date        :    08-25-2005

 **************************************************************** */

module DW_addsub_dx_carbon (a, b, ci1, ci2, addsub, tc, sat, avg, dplx,
			sum, co1, co2);

  parameter		width = 8;
  parameter		p1_width = 4;

  input[width-1:0]	a;
  input[width-1:0]	b;
  input			ci1;
  input			ci2;
  input			addsub;
  input			tc;
  input			sat;
  input			avg;
  input			dplx;

  output[width-1:0]	sum;
  output		co1;
  output		co2;

`protect
  reg[width-1:0]	sum;
  reg[width+1:0]	sum_int;
  reg			co1;
  reg			co2;
reg			co11;
  reg			co21;
  reg[width-1:0]	sum1;
  reg[width:0]		sum_int0;
  reg[p1_width:0]	sum_int1;
  reg[width-p1_width:0]	sum_int2;
  integer		i;


function[width:0] saturation;

  input			sat;
  input			avg;
  input			addsub;
  input			tc;
  input			ci;
  input[width-1:0]	a;
  input[width-1:0]	b;
  input[31:0]		in_width;

begin : block_saturation

  reg[width+1:0]	sum;
  reg			carry;
  reg			sum_sign;
  reg			sum_ext_sign;
  reg[width-1:0]	sat_out;
  reg[width-1:0]	avg_out;
  reg[width:0]		final_out;
  reg[1:0]		overflow_type;
  integer		i;
  reg[width:0]		a_in;
  reg[width:0]		b_in;
  reg[width:0]		bv;
  reg			co;

	case (addsub)
 	1'b0:
	begin 
	carry = ci;

	  for (i = 0; i <= (in_width - 1); i = i + 1)
	   {carry ,sum[i]} = (a[i] + b[i]) + carry;

	sum[in_width] = (( a[in_width-1] & tc) ^(b[in_width-1] & tc)) ^ carry;
	sum[in_width+1] = carry;
	end
	1'b1: 
	begin
	for (i=0; i< in_width; i=i+1)
	  b_in[i] = b[i];
	bv = ~b_in;

	carry = ~ci;

	  for (i = 0; i <= (in_width - 1); i = i + 1)
	 {carry,sum[i]} = a[i] + bv[i] + carry;
	sum[in_width] = ((a[in_width-1] & tc ) ^ ~( b[in_width-1] & tc )) ^ carry;
	sum[in_width+1] = carry;
	end
       endcase 
	sum_sign = sum[in_width-1];
	sum_ext_sign = sum[in_width];
	carry = sum_ext_sign;

	for (i=0; i<in_width; i=i+1)
	  sat_out[i] = sum[i];

	if (sat === 1'b1)
	begin 

	  case (tc)
	 1'b0:
	  begin 
	    carry = 1'b0;

	    	case (sum_ext_sign)
	 	1'b1:
	    	begin 
  	      	for (i=0; i<in_width; i=i+1)
	        sat_out[i] = ~addsub;
	    	end 
	  	1'b0: 
	    	begin 
  	      	for (i=0; i<in_width; i=i+1)
	        sat_out[i] = sum[i];
	    	end 
           	endcase
	  end

	  1'b1: 
	  begin 

	    overflow_type = {sum_ext_sign, sum_sign};

	    	case (overflow_type)
	      	2'b00,
	      	2'b11:
	      	begin
	     	 end

	      	2'b01:
	      	begin
	        for (i=0; i<in_width-1; i=i+1)
		  sat_out[i] = 1'b1;
		sat_out[in_width-1] = 1'b0;
	      	end

	      	2'b10:
	      	begin
		for (i=0; i<in_width-1; i=i+1)
		  sat_out[i] = 1'b0;
		sat_out[in_width-1] = 1'b1;
		end

	     	endcase
	  end
         endcase

	end

	case (avg)
	1'b0:
	  avg_out = sat_out;

	1'b1:
	begin 

	  if (tc === 1'b0)
	    carry = 1'b0;
	 	case (sat)
	 	1'b0:
	 	 begin
	    	  for (i=0; i<in_width; i=i+1)
	           avg_out[i] = sum[i+1];
	  	 end

	  	1'b1:
	  	 begin
	    	 for (i=0; i<in_width-1; i=i+1)
	      	  avg_out[i] = sat_out[i+1];
	    	  avg_out[in_width-1] = sat_out[in_width-1] & tc;
	         end
                endcase
	end
        endcase  
	for (i=0; i<in_width; i=i+1)
	  final_out[i] = avg_out[i];
	final_out[in_width] = carry;

	saturation = final_out;

	end

endfunction




always @(a or b or ci1 or ci2 or addsub or tc or sat or avg or dplx)
begin

	case(dplx)
	1'b0:
	begin 
	  sum_int0 = saturation(sat, avg, addsub, tc, ci1, a, b, width);
	  sum1 = sum_int0[width-1:0];
	  co11 = 1'b0;
	  co21 = sum_int0[width];
	end

 	 1'b1:
	begin 

	  sum_int1 = saturation(sat, avg, addsub,tc, ci1, a[p1_width-1:0], b[p1_width-1:0],p1_width);
	    sum1[p1_width-1:0] = sum_int1[p1_width-1:0];
	  co11 = sum_int1[p1_width];


  

	 sum_int2 = saturation(sat,avg,addsub,tc, ci2, a[width-1:p1_width],b[width-1:p1_width],width-p1_width);
	    sum1[width-1:p1_width] = sum_int2[width-p1_width-1:0];
	  co21 = sum_int2[width-p1_width]; 
	end
        endcase

	sum_int =  {co11, co21, sum1};

	sum = sum_int[width-1:0];

	co1 = sum_int[(width + 1 )];

	co2 = sum_int[width];
end

`endprotect

endmodule
