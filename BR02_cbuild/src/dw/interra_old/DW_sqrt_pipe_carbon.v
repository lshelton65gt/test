/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_sqrt_pipe. It returns sqrt a .

 -- ** Input       :    a

 -- ** Inout       :    -

 -- ** Output      :   root 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-12-2005


****************************************************************************/

module DW_sqrt_pipe_carbon (clk,rst_n,en,a,root);
   
   parameter width = 14;
   parameter tc_mode = 0;
   parameter num_stages = 2;
   parameter stall_mode = 1;
   parameter rst_mode = 1;

   input clk;
   input rst_n;
   input [width-1 : 0] a;
   input en;
   
   output [((width+1)/2)-1 : 0] root;
`protect 
   wire a_rst_n;
   reg [width-1:0] a_reg[0:num_stages-1];
   integer i,j;

 assign a_rst_n = (rst_mode == 1)? rst_n : 1'b1;

always @(posedge clk or negedge a_rst_n)
    
begin
if (a_rst_n === 1'b0) begin
	a_reg[0]=a;

	    for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {width{1'b0}};
	    end // for (i= 1; i < num_stages-1; i++)
     end 

else if (rst_mode != 0 & rst_n === 1'b0) begin
	a_reg[0]=a;

	    for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {width{1'b0}};
	    end // for (i= 1; i < num_stages-1; i++)
     end // if (rst_mode != 0 & rst_n === 1'b0)
	 else if  (rst_mode == 0 || rst_n === 1'b1) begin
	  a_reg[0]=a;
	
	   	if (stall_mode != 0 && en === 1'b0) begin
		    for (i= 1; i < num_stages; i=i+1)begin 
	   			a_reg[num_stages-i]  = a_reg[num_stages-i];
                  end
	         end
		else if (stall_mode == 0 || en === 1'b1) begin // pipeline enabled
			for (i= 1; i < num_stages; i=i+1) begin
		   	 a_reg[num_stages-i]  = a_reg[num_stages-i-1];
                      end
                     end
	end
	   	 
end

assign root=DWF_sqrt(a_reg[num_stages-1]);

function[(width+1)/2-1:0]DWF_sqrt;
input  [width-1 : 0] a;

reg [(width+1)/2-1 : 0] root,pre_sq;
reg [width-1:0] A_temp;
integer i;
reg A_x;

begin

	if(tc_mode)
	A_temp = a[width-1]?(~a + 1'b1):a;
	else A_temp = a;
	
	A_x = ^A_temp;
    if (A_x === 1'bx) pre_sq = {(width+1)/2{1'bx}};
    else 
	begin 
		pre_sq = {(width+1)/2{1'b0}};
		for (i = (width+1)/2-1; i >= 0; i = i-1)
		begin
	    	pre_sq[i] = 1'b1;
			if (pre_sq*pre_sq > {1'b0,A_temp})
			pre_sq[i] = 1'b0;
		end
	end
	DWF_sqrt=pre_sq;
end

endfunction

`endprotect
endmodule
