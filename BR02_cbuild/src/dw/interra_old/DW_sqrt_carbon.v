/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_sqrt. It returns the Sqrt of A .

 -- ** Input       :    a

 -- ** Inout       :    -

 -- ** Output      :   square 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-29-2005


****************************************************************************/
module DW_sqrt_carbon(a, root);
  parameter width   = 32;
  parameter tc_mode = 0;

  input  [width - 1 : 0] a;
  output [(width - 1)/2 : 0] root;
`protect
  reg [(width - 1)/2 : 0] pre_sq;
  reg [(width - 1)/2 : 0] root;
  reg [width - 1:0] A_temp;
  integer i ;

always@(a or tc_mode)
begin

      A_temp = (tc_mode == 1'b1 && a[width - 1])?(~a + 1'b1):a;

	pre_sq = {(width+1)/2{1'b0}};
	for (i = (width - 1)/2; i >= 0; i = i-1)
	begin
	    	pre_sq[i] = 1'b1;
	pre_sq[i] = pre_sq*pre_sq>{1'b0,A_temp}? 1'b0 : 1'b1 ;
	end
  root = pre_sq;
end
 
`endprotect

endmodule
