/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW02_multp. It returns OUT0 and OUT1 .

 -- ** Input       :    a,b,tc

 -- ** Inout       :    -

 -- ** Output      :   out0 , out1

 
 -- ** Date        :    07-26-2005


****************************************************************************/


module DW02_multp_carbon( a, b, tc, out0, out1 );

parameter a_width = 32;
parameter b_width = 32;
parameter out_width = 66;

input [a_width-1 : 0]	a;
input [b_width-1 : 0]	b;
input			tc;
output [out_width-1:0]	out0, out1;

// if the user compiles with this `define, then the sum out0+out1 will
// be the correct product.  out0 and out1 will not match the wallace-tree
// implementation.  Also, this `define requires the -2001 switch.  The
// benefit is that it simulates significantly faster than the precise
// Wallace-tree implementation, and if accuracy is only required at the
// final multiplier output, then this is a good tradeoff.
`ifdef CARBON_FAST_PARTIAL_PRODUCT 

`protect

  assign out1 = 0;
  reg [out_width - 1:0] out0;
  always @(a or b or tc) begin
    if (tc) begin : signed_imp
      reg signed [a_width - 1:0] as;
      reg signed [b_width - 1:0] bs;
      as = a;
      bs = b;
      out0 = as * bs;
    end else begin
      out0 = a * b;
    end
  end

/*
 Here I add a few random comments so that the protected text for the FAST version
 is about the sam size as the slow version.  
 
 @pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCSlZCQldyQkJFt6AwFxPv/C6CEbfzuVW3A/UDkmvgDutFB
FgHGxcWl/Vkqn4E73HKAM0T0VCTNM3wnX+Zdux+tANCID+NfKUdn0g4uGVVlV0kliw1Xn2xiwuBm
vmIOX4fHY8unCs9xa98PD8ev3k4OyFOx3NkJhoyiWvnZlB0HBAQSrhnBwQXD+gWkkwgcLk5MrSIJ
yCxh7KcRZgKLEfXQQfGYkGnSI1AQYKHAOtHvQ8sFVfyM53KJQVHOzBX84St+dFe4zdddvq4gG288
0UJl3SWEJ/TJwRO8uqHyiVfcWNyIh6QztSCjWHUCvI9ZX6XsILO42lB6oR6NuzWJcwa5tRRwluqh
v9oLFf6QfBLHZ+ccP7ohBjsAnaxX0r+8FIS8xyBsUggEP/E+oTi+bbTf9HD4Ut6dlpYAt9EPHp9C
QxYetY1neH0x3FkKEj01Crq3YNp3RxAsRzmX7hfFD9GY8G/C9InHEQww1+JR14Z6UqUmO99p3MlI
4zRNc/MeHolSSsBqPxa0rK322M6QOEe5IgS2gaj9a9wB9FVLbgEgLRLuFw0UjA3BXaadnwJKVum/
20vFp4lpbXYFNz1sSnOIDP7RqtlrNrMKkQSiCx1Nnvr9REjjpholTQ5FEyid5JS1b28h3Bxt2dxq
@pdLzY2FyYm9uAAAAAAAAAAIAAAAAkJCUI5CQgZuQkJQ76Ax9yMvjStaE7s5tGmMANlhQMBUHpKIO
Kt5KNuX9n7afjRa6goLoy7wUAFdmQHQnZwwtMQsL9laf7ZijEEhjfSf3X+UHT7c/5q9vosk+Rn9i
XVOm7mXDJszMzG8KGeuxbmp+rE5M9KdJVQ+/1r8MUZCG8Y79wsi1+QIX1eIm3lIs2I4ae13NLtiM
dxPIVlmCXt63r8zOlK8Ozt8TO0NZZE156eClKexjbvx6kBQrDcg1ICm7qBxRY+G438agUxFwzlRV
nWQRL2Z2FABC4O2Prn5wa96YC4yyariGvtRZ2jS4loDbeEiB
 */
`endprotect

`else

`protect
parameter  xdim= a_width+b_width+1 ;
parameter npp =(a_width/2) + 2 ;
reg [xdim-1 : 0] pp_array [0 : npp-1];

reg   [xdim-1 : 0]	tmp_OUT0, tmp_OUT1;
wire  [a_width+2 : 0]	a_padded;
wire  [b_width+1 : 0]	b_padded;
wire  			a_sign, b_sign, out_sign;

assign a_sign = tc & a[a_width-1];
assign b_sign = tc & b[b_width-1];

assign a_padded = {a_sign, a_sign, a, 1'b0};
assign b_padded = {b_sign, b_sign, b};

always @ (a_padded or b_padded)
begin : proc
	reg [xdim-1 : 0] temp_pp_array [0 : npp-1];
	reg [xdim-1 : 0] next_pp_array [0 : npp-1];
	reg [xdim+3 : 0] temp_pp;
	reg [xdim-1 : 0] new_pp;
	reg [xdim-1 : 0] tmp_pp_carry;
	reg [a_width+2 : 0] temp_a_padded;
	reg [2 : 0] temp_bitgroup;
	integer bit_pair, pp_count, i,tmp,tmp1, p, q;

`ifdef CARBON_DO_NOT_CLEAR_PARTIAL_PRODUCT_TEMPS
`else
        p = 0;
        pp_count = 0;
        for (i = 0; i < npp; i = i + 1) begin
          temp_pp_array[i] = 0;
          next_pp_array[i] = 0;
        end
`endif

	temp_pp_array[0] = {xdim{1'b0}};

	for (bit_pair=0 ; bit_pair < npp-1 ; bit_pair = bit_pair+1)
	begin
	    temp_a_padded = (a_padded >> (bit_pair*2));
	    temp_bitgroup = temp_a_padded[2 : 0];	    
		if ( b_padded[b_width] == 1'b0)
		begin
			case (temp_bitgroup)
		    	3'b000, 3'b111 :
			    	temp_pp = {xdim+3{1'b0}};
		    	3'b001, 3'b010 :
			    	temp_pp = {{a_width+2{1'b0}}, b_padded, 1'b0};
		    	3'b011 :
			    	temp_pp = {{a_width+1{1'b0}}, b_padded, 2'b00};
		   	 	3'b100 :
			    	temp_pp = {{a_width+1{1'b1}}, ~b_padded, 2'b10};
		    	3'b101, 3'b110 :
			    	temp_pp = {{a_width+2{1'b1}}, ~b_padded, 1'b0};
		    	default : 
					temp_pp = {xdim+3{1'bx}};
			endcase
		end
	    else
		begin
			case (temp_bitgroup)
		    	3'b000, 3'b111 :
			    	temp_pp = {xdim+3{1'b0}};
		    	3'b001, 3'b010 :
			    	temp_pp = {{a_width+2{1'b1}}, b_padded, 1'b0};
		    	3'b011 :
			    	temp_pp = {{a_width+1{1'b1}}, b_padded, 2'b00};
		    	3'b100 :
			    	temp_pp = {{a_width+1{1'b0}}, ~b_padded, 2'b10};
		    	3'b101, 3'b110 :
			    	temp_pp = {{a_width+2{1'b0}}, ~b_padded, 1'b0};
		    	default : 
					temp_pp = {xdim+3{1'bx}};
			endcase
		end

	    temp_pp = temp_pp << (2 * bit_pair);
	    new_pp = temp_pp[xdim : 1];
	    temp_pp_array[bit_pair+1] = new_pp;

	    if ((a_padded[bit_pair*2+2] & ~(a_padded[bit_pair*2+1] & a_padded[bit_pair*2])) == 1'b1)
			temp_pp_array[0] = temp_pp_array[0] | ({{xdim-1{1'b0}}, 1'b1} << (bit_pair*2));
			
		pp_count = npp;
    end
	
	for(q=pp_count; q > 2; q= q-1) begin
      tmp = q/3 ;
      tmp1 = q % 3 ; 
		if (q == pp_count) begin
	    	for (i=0 ; i < (tmp) ; i = i+1) begin
				next_pp_array[i*2] = temp_pp_array[i*3] ^ temp_pp_array[i*3+1] ^ temp_pp_array[i*3+2];
				tmp_pp_carry = (temp_pp_array[i*3] & temp_pp_array[i*3+1]) | (temp_pp_array[i*3+1] & temp_pp_array[i*3+2]) | (temp_pp_array[i*3] & temp_pp_array[i*3+2]);
				next_pp_array[i*2+1] = tmp_pp_carry << 1;
	    	end
			if ((tmp1) > 0) 
				for (i=0 ; i < (tmp1) ; i = i + 1)
		    		next_pp_array[2 * (tmp) + i] = temp_pp_array[3 * (tmp) + i];
	    	for (i=0 ; i < npp ; i = i + 1)
				temp_pp_array[i] = next_pp_array[i];
        	p=q;
		end
		else if (q == (p-p/3)) begin
			for (i=0 ; i < (tmp) ; i = i+1) begin
				next_pp_array[i*2] = temp_pp_array[i*3] ^ temp_pp_array[i*3+1] ^ temp_pp_array[i*3+2];
				tmp_pp_carry = (temp_pp_array[i*3] & temp_pp_array[i*3+1]) | (temp_pp_array[i*3+1] & temp_pp_array[i*3+2]) | (temp_pp_array[i*3] & temp_pp_array[i*3+2]);
				next_pp_array[i*2+1] = tmp_pp_carry << 1;
	    	end
			if ((tmp1) > 0) 
				for (i=0 ; i < (tmp1) ; i = i + 1)
		    		next_pp_array[2 * (tmp) + i] = temp_pp_array[3 * (tmp) + i];
	    	for (i=0 ; i < npp ; i = i + 1)
				temp_pp_array[i] = next_pp_array[i];
        	p = q;
		end
	end

	tmp_OUT0  <= temp_pp_array[0];

	if (pp_count > 1)
	    tmp_OUT1 <= temp_pp_array[1];
	else
	    tmp_OUT1 <= {xdim{1'b0}};
end //proc 

assign out_sign = tmp_OUT0[xdim-1] | tmp_OUT1[xdim-1];
assign out0 = {{out_width-xdim{1'b0}}, tmp_OUT0};
assign out1 = {{out_width-xdim{out_sign}}, tmp_OUT1};
`endprotect

`endif
endmodule
