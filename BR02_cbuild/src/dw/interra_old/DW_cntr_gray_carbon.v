/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_cntr_gray.It returns count.

 -- ** Input       :    rst_n,init_n,cen,load_n,clk,data

 -- ** Inout       :    -

 -- ** Output      :   count 

 -- ** Date        :    08-17-2005


****************************************************************************/

module DW_cntr_gray_carbon(clk,init_n,load_n,rst_n,data,cen,count);
		
parameter width=12;

input rst_n,init_n,cen,load_n,clk;
input[width-1:0] data;
output[width-1:0] count;
`protect
reg[width-1:0] count_int,count_next;
wire [width-1:0] count;
integer i;

always@(posedge clk or negedge rst_n)
begin  
      if(rst_n ==1'b0)	
	count_int = {width{1'b0}};
      else begin
       if (init_n === 1'b0)
           begin 
	    	count_int = {width{1'b0}};
           end
           else if (init_n === 1'b1)
           begin
              if (load_n ===1'b0)
              begin
		count_int=data;
              end 
              else 
	      begin
	       for (i=0; i<width; i=i+1)
	       count_next[i]= ^(count_int>>i);
	       count_int= count_next+cen;
	       count_next=(count_int>>1)^count_int;
	       count_int = count_next;
	      end
           end 
         end
 
end
	assign count=count_int;
`endprotect
endmodule
