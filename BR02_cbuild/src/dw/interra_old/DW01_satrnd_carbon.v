/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_satrnd.  it returns OV and DOUT.

 -- ** Input       :    DIN ,TC, SAT, RND

 -- ** Inout       :    -

 -- ** Output      :    OV ,DOUT 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-31-2005

****************************************************************************/

module DW01_satrnd_carbon (din,tc,sat,rnd,ov,dout);
parameter width = 32;
parameter msb_out = width-1;
parameter lsb_out = width/2;

input [width-1:0] din;
input tc;
input rnd;
input sat;

output ov;
output [15:0] dout;
`protect
reg ov;
reg [15:0] dout;

reg [msb_out-lsb_out:0] max_signed;
reg [msb_out-lsb_out:0] min_signed;
reg [msb_out-lsb_out:0] max_unsigned;
reg [msb_out-lsb_out:0] add_lsb;
reg [width-1:0] din_in;
reg [msb_out-lsb_out:0] dout_rnd;
reg [msb_out-lsb_out:0] dout_final;
reg [msb_out-lsb_out : 0] dout_ov;
integer i;
integer out_of_range;



always @(din or tc or sat or rnd)
  begin
   	max_signed = (1'b1 << (msb_out-lsb_out)) - 1;
	min_signed = 1'b1 << (msb_out-lsb_out);
	max_unsigned = (1'b1 << (msb_out-lsb_out+1)) -1;
	add_lsb = 1;
    

    out_of_range = 0;
    dout_ov = din[msb_out:lsb_out];

    if (msb_out < width-1)  
    begin
      if (tc === 1'b1)
      begin
	  for (i = width-1;(i >= msb_out+1) && (out_of_range !== -1);i = i-1)
             begin
	      if (din[i] !== din[msb_out])
	        out_of_range = 1;
	     end 
      end

	  dout_ov = (din[width-1] === 1'b0) ? max_signed :min_signed;
    end
     else             
      begin
        dout_ov = max_unsigned;	
	for (i = width-1;((i >= msb_out+1) && (out_of_range!=-1));i=i-1)
        begin
	  if (din[i] === 1'b1)
	      out_of_range = 1;
	end 
      end 

    if (lsb_out > 0 && rnd === 1'b1)
    begin
        if (((tc===1'b1 && din[msb_out:lsb_out] === max_signed) ||(tc==1'b0 &&
    	           din[msb_out:lsb_out] === max_unsigned)) &&
	          din[lsb_out-1] === 1'b1)
	begin
	  if (rnd === 1'b1)
            out_of_range = 1;
        end
   
        dout_rnd = (din[lsb_out-1] === 1'b1)?din[msb_out:lsb_out] + add_lsb: din[msb_out:lsb_out];
    end
   else 
      dout_rnd = din[msb_out:lsb_out];

    if (sat === 1'b0)
      dout_final = dout_rnd;
    else 
        dout_final = (out_of_range === 1)?dout_ov:dout_rnd;

       {ov,dout} =(out_of_range === 1)? {1'b1,dout_final}:{1'b0,dout_final};
  end
`endprotect
endmodule
