/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_mult_pipe. It returns mult of  of A, B .

 -- ** Input       :    A,B,TC,CLK

 -- ** Inout       :    -

 -- ** Output      :   PRODUCT 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-01-2005


****************************************************************************/
module DW_mult_pipe_carbon (clk,rst_n,en,tc,a,b,product);
   
parameter a_width = 6;
parameter b_width = 6;
parameter num_stages = 2;
parameter stall_mode = 1;
parameter rst_mode = 1;

   
input clk;
input rst_n;
input [a_width-1 : 0] a;
input [b_width-1 : 0] b;
input tc;
input en;
   
output [a_width+b_width-1: 0] product;
`protect
wire a_rst_n;
reg [a_width-1 : 0] a_reg[0 : num_stages-1];
reg [b_width-1 : 0] b_reg[0 : num_stages-1];
reg                 tc_reg[0 : num_stages-1];
assign a_rst_n = (rst_mode == 1)? rst_n : 1'b1;


 integer i;
   
always @(posedge clk or negedge a_rst_n)
begin
	if (a_rst_n == 1'b0) begin
	 
	 a_reg[0]  = a;
	 b_reg[0]  = b;
	 tc_reg[0] = tc;
	 	
	    for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {a_width{1'b0}};
	       b_reg[i]  = {b_width{1'b0}};
	       tc_reg[i] = 1'b0;
	    end // for (i= 1; i < num_stages-1; i++)
	 end // if (rst_mode != 0 & rst_n === 1'b0)
         else if(rst_mode != 0 & rst_n === 1'b0) begin
             	 
	 a_reg[0]  = a;
	 b_reg[0]  = b;
	 tc_reg[0] = tc;

	    for (i= 1; i < num_stages; i=i+1) begin
	       a_reg[i]  = {a_width{1'b0}};
	       b_reg[i]  = {b_width{1'b0}};
	       tc_reg[i] = 1'b0;
	    end 
          end
	  else if  (rst_mode == 0 || rst_n === 1'b1) begin
	 a_reg[0]  = a;
	 b_reg[0]  = b;
	 tc_reg[0] = tc;
	    if (stall_mode != 0 && en === 1'b0)begin
	       for (i= 1; i < num_stages; i=i+1)   begin
			  a_reg[num_stages-i]  = a_reg[num_stages-i];
			  b_reg[num_stages-i]  = b_reg[num_stages-i];
			  tc_reg[num_stages-i] = tc_reg[num_stages-i];
	       end 	       
	    end 
	    else if (stall_mode == 0 || en === 1'b1)begin   
	       for (i= 1; i < num_stages; i=i+1)    begin
			  a_reg[num_stages-i]  = a_reg[num_stages-i-1];
			  b_reg[num_stages-i]  = b_reg[num_stages-i-1];
			  tc_reg[num_stages-i] = tc_reg[num_stages-i-1];
	       end 
	    end
         end
end

function [a_width+b_width-1:0] DWF_mult;
input[a_width:0] a;
input[b_width:0] b;
input tc;

reg	[a_width+b_width-1:0]	pre_product;
reg	[a_width-1:0]	temp_a;
reg	[b_width-1:0]	temp_b;
reg	[a_width+b_width-2:0]	long_temp1,long_temp2;

begin
	if (tc)
	begin
		temp_a = (a[a_width-1])? (~a + 1'b1) : a;
		temp_b = (b[b_width-1])? (~b + 1'b1) : b;
		long_temp1 = temp_a * temp_b;
		long_temp2 = ~(long_temp1) + 1'b1;
		pre_product =((a[a_width-1] ^ b[b_width-1])&&(|long_temp1)) ? {1'b1,long_temp2}:{1'b0,long_temp1};
	end
	else pre_product=a*b;
	DWF_mult=pre_product;
end

endfunction

assign product=DWF_mult(a_reg[num_stages-1],b_reg[num_stages-1],tc_reg[num_stages-1]); 

`endprotect

endmodule
