/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_shifter. It returns the Shifted value.

 -- ** Input       : data_in, data_tc, sh, sh_tc, sh_mode   

 -- ** Inout       :    -

 -- ** Output      :  data_out
 
 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-12-2005


****************************************************************************/
module DW_shifter_carbon(data_in, data_tc, sh, sh_tc, sh_mode, data_out); 
 
parameter data_width = 32;
parameter sh_width = 3;
parameter inv_mode = 0;
 
input  [data_width-1:0] data_in;
input  [sh_width-1:0] sh;
input data_tc, sh_tc, sh_mode; 
output [data_width-1:0] data_out;

`protect
  
reg [sh_width-1:0] sh_int;
reg data_tc_int, sh_tc_int;
reg padded_value;
reg [data_width-1:0]  data_out;  


always@(data_in or data_tc or sh or sh_tc or sh_mode)

begin
	padded_value =  (inv_mode == 0 || inv_mode == 2) ? 1'b0 : 1'b1;


	{data_tc_int, sh_tc_int, sh_int} = (inv_mode == 0 || inv_mode == 1)?{data_tc,sh_tc,sh}:{~data_tc,~sh_tc,~sh};

	 data_out=(sh_mode== 1'b1)?((sh_tc_int == 1'b0) || (sh_int[sh_width-1] == 1'b0))?shift00(data_in,sh_int,padded_value):shift01(data_in,sh_int,padded_value):((sh_tc_int == 1'b0) || (sh_int[sh_width-1] == 1'b0))?shift10(data_in,sh_int):shift11(data_in,sh_int);

end



function[data_width-1:0] shift00;
    input [data_width-1:0]	data_in;
    input [sh_width-1:0]	sh;
    input			padded_value;
    
    reg [data_width-1:0]	data_out;
    integer j;
	begin
	data_out = data_in<<sh;
	if(padded_value)
	    for(j=0;j<sh;j=j+1)
		data_out[j]=padded_value;
	
	shift00=data_out;
	end

endfunction
		 



function[data_width-1:0] shift01;

    input [data_width-1:0]	data_in;
    input [sh_width-1:0]	sh_int;
    input			padded_value;

    reg [data_width-1:0]	data_out;
    reg 			data_sign;
    reg [sh_width-1:0]		sh_abs;
    integer j;
    begin

			sh_abs = (-sh_int);
        		data_sign = data_in[data_width-1];
           		data_out = data_in >> sh_abs;
					// if padding of 1 is necessary	// sign extension of right shifted data
			if(data_tc_int)
				for ( j = 0; j < sh_abs ; j = j+1 )
		 			data_out[data_width-1-j] = data_sign;
		 		else if(padded_value)
				for ( j = 0; j < sh_abs ; j = j+1 )
					data_out[data_width-1-j] = padded_value;
	shift01=data_out;
    end

endfunction


function[data_width-1:0] shift10;

    input [data_width-1:0]	data_in;
    input [sh_width-1:0]	sh_int;
  
    reg [data_width-1:0]	data_out;
    integer j;
	begin
	data_out = data_in << (sh_int % data_width);
	     	for ( j = 0; j < (sh_int % data_width) ; j = j+1 )
	       		data_out[j] = data_in[ data_width - (sh_int % data_width) + j ];
	shift10 = data_out;
	    end 

endfunction


function[data_width-1:0] shift11;

	  input [data_width-1:0]	data_in;
    input [sh_width-1:0]	sh_int;

   
    reg [data_width-1:0]	data_out;
    reg [sh_width-1:0]		sh_abs;
    integer j;
 	begin
	        sh_abs = (-sh_int);
	        data_out = data_in >> sh_abs;
	        for ( j = 0; j < sh_abs ; j = j+1 )
	       		data_out[data_width-1-j] = data_in[sh_abs -1- j ];
	shift11 = data_out;
	end

endfunction
	
`endprotect

endmodule


