/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW_div. It returns quatient and remainder.

 -- ** Input       :    a,b

 -- ** Inout       :    -

 -- ** Output      :    quotient,remainder,divide_by_0

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-12-2005


****************************************************************************/
module DW_div_carbon(a,b,quotient,remainder,divide_by_0);
parameter a_width = 32;
parameter b_width = 32;
parameter tc_mode=0;
parameter rem_mode=1;

input [a_width-1:0]  a;
input [b_width-1:0]  b;
output               divide_by_0;
output [a_width-1:0] quotient;
output [b_width-1:0] remainder;
`protect
reg [a_width-1:0]    quot,temp_a,quotient;
reg [b_width-1:0]    rem,mod,temp_b;
reg [b_width-1:0]    rem_temp,remainder;
reg div_0,divide_by_0;

always@(a or b)
begin

if(tc_mode == 1'b0) //if unsigned mode
	if(b==0) // divide by zero error
	begin
		quot={a_width{1'b1}};
		div_0=1'b1;
		rem=a[b_width-1:0];
		mod=a[b_width-1:0];
	end
	else
	begin
		div_0=1'b0;
		quot=a/b; // calculate quot,rem,mod
		rem=a%b;
		mod=a%b;
	end
else	// if signed mode
	begin
		if(b==0) // if divide by zero error
		begin
		quot =	(a[a_width-1]==1'b0)?{1'b0, {a_width-1{1'b1}}}:{1'b1, {a_width-1{1'b0}}};
							
			div_0=1'b1;
			rem=a[b_width-1 : 0];
			mod=a[b_width-1 : 0];
		end
		else
		begin
			div_0=1'b0;
			if ((a == {1'b1, {a_width-1{1'b0}}}) && (b == {b_width{1'b1}}))
                       begin
	               quot = {1'b0, {a_width-1{1'b1}}};
	               rem = {b_width{1'b1}};
                       mod = {b_width{1'b1}};
                       end
                       else 
                     begin

                        temp_a = a[a_width - 1] ? (~a + 1'b1) : a;
			temp_b = b[b_width - 1] ? (~b + 1'b1) : b;
			quot=temp_a/temp_b;
			if(a[a_width-1] != b[b_width-1])
			quot = ~quot + 1'b1;
                       //calculate quotient
                      //  quot = (a[a_width-1] != b[b_width-1])?(~quot + 1'b1) : quot;
			rem_temp= temp_a % temp_b;
			if (a[a_width-1] == 1'b1)
	    	             rem = ~rem_temp + 1'b1;		// calculate remainder
                        else rem = rem_temp;

			if (rem_temp == {b_width{1'b0}})
			    mod = rem_temp;
	  		else 
                        begin
			    if (a[a_width-1] == 1'b0)
	    			mod = rem_temp;
	    		    else mod = ~rem_temp + 1'b1;	//calculate modulus
	    	
			    if (a[a_width-1] != b[b_width-1])	//calculate modulus if diff signs
			          mod = b + mod;
                        end
                      end
	  	end
	end
divide_by_0=div_0;

quotient=quot;
remainder=rem_mode?rem:mod;
end

//assign divide_by_0=div_0;
//assign quotient=quot;
//assign remainder=rem_mode?rem:mod;
`endprotect
endmodule



