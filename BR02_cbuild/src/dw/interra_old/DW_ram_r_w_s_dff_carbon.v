/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////


 -- ** Description : This module contains functionality of DesignWare component DW_ram_r_w_s_dff. It is Synch Write, Asynch Read RAM (Flip-Flop Based).

 -- ** Input       :    data_in, rd_addr, wr_addr, wr_n, rst_n, cs_n, clk

 -- ** Inout       :    -

 -- ** Output      :    data_out

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-05-2005


****************************************************************************/
 

module DW_ram_r_w_s_dff_carbon (clk,rst_n, cs_n, wr_n, rd_addr, wr_addr, data_in, 
			   data_out);

   parameter data_width = 4;
   parameter depth = 8;
   parameter rst_mode = 0;
   
   
   `define addr_width ((depth>16)?((depth>64)?((depth>128)?8:7):((depth>32)?6:5)):((depth>4)?((depth>8)?4:3):((depth>2)?2:1)))

   input [data_width-1:0] data_in;
   input [`addr_width-1:0] rd_addr;
   input [`addr_width-1:0] wr_addr;
   input 		   wr_n;
   input 		   rst_n;
   input 		   cs_n;
   input 		   clk;

   output [data_width-1:0] data_out;
`protect
   wire [data_width-1:0]   data_in;
   reg [depth*data_width-1:0]    next_mem;
   reg [depth*data_width-1:0]    mem;
   wire [depth*data_width-1:0]    mem_mux;   
   wire 		   a_rst_n;
   integer i, j;
   
   assign mem_mux = mem >> (rd_addr * data_width);

   assign data_out = (rd_addr >= depth)? {data_width{1'b0}} : mem_mux[data_width-1 : 0] ;
   
   assign a_rst_n = (rst_mode == 0)? rst_n : 1'b1;

 always @ (posedge clk or negedge a_rst_n) begin 
 if(a_rst_n ==1'b0)begin  
     next_mem = mem; 
    mem <= {depth*data_width{1'b0}};
  end


else if(rst_n ==1'b0)begin  
     next_mem = mem; 
    mem <= {depth*data_width{1'b0}};
  end
  else begin
   next_mem = mem;
   
     if ((cs_n | wr_n) !== 1'b1) begin
            
	 if ((wr_addr ^ wr_addr) !== {`addr_width{1'b0}}) begin
	    next_mem = {depth*data_width{1'bx}};	

	 end else begin
         
	    if ((wr_addr < depth) && ((wr_n | cs_n) !== 1'b1)) begin
	       for (i=0 ; i < data_width ; i=i+1) begin
		  j = wr_addr*data_width + i;
		  next_mem[j] = ((wr_n | cs_n) == 1'b0)? data_in[i] | 1'b0
					: mem[j];
	       end // for
	    end // if
	 end // if-else
      end // if
       mem <= next_mem;
  end
 end 
     
`endprotect
endmodule // DW_ram_r_w_s_dff
