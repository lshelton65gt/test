/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW01_prienc. It returns INDEX Value of A.

 -- ** Input       :    A

 -- ** Inout       :    -

 -- ** Output      :    INDEX 

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    07-25-2005


****************************************************************************/
module DW01_prienc_carbon (A, INDEX);
   parameter A_width = 32;
   parameter INDEX_width = 5;
   input [A_width-1:0] A;
   output [INDEX_width-1:0] INDEX;
`protect
   reg [INDEX_width-1 : 0]  INDEX;
   reg 			    flag;
   integer 		    i;
   always @(A)
     begin  
	flag = 0;
        INDEX = 0;
	for (i=A_width-1; (!flag) && i>=0; i=i-1) begin
	   if (A[i] === 1'b1) begin
              flag = 1;
              INDEX = i+1;
	   end
           else
             INDEX = 0;
	end
     end
`endprotect
endmodule
