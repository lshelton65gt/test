/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW02_prod_sum. It calculates sum-of-product.

 -- ** Input       :    A , B , TC

 -- ** Inout       :    -

 -- ** Output      :    SUM

 -- ** TestPlan    :    Section 1.1.1
 
 -- ** Date        :    08-05-2005

****************************************************************************/

module DW02_prod_sum_carbon(A, B, TC, SUM);
parameter A_width = 4;
parameter B_width = 5;
parameter num_inputs = 4;
parameter SUM_width = A_width + B_width - 1 + num_inputs ;

input [num_inputs * A_width-1:0] A;
input [num_inputs * B_width-1:0] B;
input TC;
output [SUM_width-1:0] SUM;

`protect
reg  [SUM_width-1:0]           SUM;

reg  [A_width-1:0]             aReg;
reg  [B_width-1:0]             bReg;

reg  [SUM_width-1:0]           prodTCReg;
reg  [A_width+B_width-1:0]     prodReg;
reg   [A_width-1:0]  regAbsA;
reg   [B_width-1:0]  regAbsB;
reg  [A_width+B_width-1:0]  absMultValue;
reg  outSignBit;

integer i, j;

always @(A or B or TC)
begin
    SUM = 0;
    for(i=0; i<num_inputs; i=i+1)
	begin
		for(j=0; j<A_width; j=j+1)
		    aReg[j] = A[i*A_width + j];
		for(j=0; j<B_width; j=j+1)
		    bReg[j] = B[i*B_width + j];
          case (aReg[A_width-1] & TC)
         1'b1 :
           regAbsA=  (~aReg + 1'b1);
         1'b0 :	
	   regAbsA= aReg ;
          endcase
          case (bReg[B_width-1] & TC)
        1'b1 :
           regAbsB=  (~bReg + 1'b1);
         1'b0 :	
	   regAbsB= bReg ;
          endcase



  	 absMultValue = regAbsA * regAbsB;
    		 outSignBit = (aReg[A_width-1] ^ bReg[B_width-1]) & TC;
    		prodReg  = outSignBit?(~absMultValue + 1'b1):absMultValue;

		case(TC )
                  1'b0:
		    prodTCReg = {{(SUM_width-A_width-B_width){1'b0}}, prodReg};
		  1'b1:
		    prodTCReg = {{(SUM_width-A_width-B_width){prodReg[A_width+B_width-1]}}, prodReg};
                endcase
		SUM = SUM + prodTCReg;
	//	SUM =  prodTCReg;
	end
end
`endprotect
endmodule

