/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////


 -- ** Description : This module contains functionality of DesignWare component DW01_mux_any. It returns MUX .

 -- ** Input       :    A,SEL

 -- ** Inout       :    -

 -- ** Output      :   SUM

 
 -- ** Date        :    07-26-2005


****************************************************************************/

module DW01_mux_any_carbon(A, SEL, MUX);

parameter	A_width = 32;
parameter	MUX_width = 16;
parameter	SEL_width = 4;

input	[A_width-1:0]	A;
input	[SEL_width-1:0]	SEL;
output	[MUX_width-1:0]	MUX;
`protect
reg  [MUX_width-1:0]  MUX;
reg  [MUX_width-1:0]  count;


always @(A or SEL)

      for(count=0; count<MUX_width; count=count+1)
        MUX[count] = A[SEL*MUX_width + count];





`endprotect

endmodule

