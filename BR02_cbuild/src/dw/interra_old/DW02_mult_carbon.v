/****************************************************************************
//-//////////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved.
//  Portions of this software code are licensed to Carbon Design Systems
//  and are protected by copyrights of its licensors."
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-//////////////////////////////////////////////////////////////////////////

 -- ** Description : This module contains functionality of DesignWare component DW02_mult. It returns PRODUCT of A,B .

 -- ** Input       :    A,B,TC

 -- ** Inout       :    -

 -- ** Output      :    PRODUCT

 -- ** Date        :    07-15-2005


****************************************************************************/

module DW02_mult_carbon(A, B, TC, PRODUCT);

parameter	A_width = 32;
parameter	B_width = 32;
input	[A_width-1:0]	A;
input	[B_width-1:0]	B;
input			TC;
output	[A_width+B_width-1:0]	PRODUCT;
`protect
reg  [A_width+B_width-1:0]  PRODUCT;

reg   [A_width-1:0]  regAbsA;
reg   [B_width-1:0]  regAbsB;

reg  outSignBit;

always @(A or B or TC)
begin
    regAbsA = (A[A_width-1] & TC)? (~A + 1'b1) : A;
    regAbsB = (B[B_width-1] & TC)? (~B + 1'b1) : B;
    outSignBit = (A[A_width-1] ^ B[B_width-1]) & TC;
    PRODUCT = outSignBit?(~(regAbsA * regAbsB) + 1'b1):regAbsA * regAbsB;
end
`endprotect
endmodule

