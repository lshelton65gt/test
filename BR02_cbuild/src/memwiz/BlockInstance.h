#ifndef __BLOCKINSTANCE_H__
#define __BLOCKINSTANCE_H__

#include "Scripting.h"
#include "Block.h"

class ElaboratedModel;

// File: BlockInstances, BlockInstance
// The BlockInstances collection and the BlockInstance object
//
// Class: BlockInstance
//
// The BlockInstance object
class BlockInstance : public QObject
{
  Q_OBJECT

  // Property: instanceID
  // String unique instance identifier for this instance.
  Q_PROPERTY(QString instanceID READ getInstanceID)
  
  // Property: name
  // String unique instance name for this block instance of the form  <BlockName>N where N
  // is the <instanceID> identifier.
  Q_PROPERTY(QString name READ getInstanceName)
  
  // Property: block
  // The underlying <Block> that this instance was elaborated from.
  Q_PROPERTY(QObject* block READ getBlock)
  
  // Property: ports
  // The <Port>s associated with this block (pre-elaboration)
  Q_PROPERTY(QObject* ports READ getPorts)
  
  // Property: portInstances
  // The <PortInstance>s associated with this block (post-elaboration)
  Q_PROPERTY(QObject* portInstances READ getPortInstances)


public:
  BlockInstance(ElaboratedModel* model, Block* block, QString instanceID = "");
  
  QString getInstanceName() const { return mInstanceName; }
  Block* getBlock() const { return mBlock; }

  Ports* getPorts();
  Parameters* getParameters();
  QObject* getPortInstances();

  QString getInstanceID() const { return mInstanceID; }

private:
  void copyData();

private:
  // the block we are encapsulating
  Block* mBlock;
  QString mInstanceName;

  // Parameters for this block instance copied from mBlock
  Parameters mParameters;

  // Ports for this block instance copied from mBlock
  Ports mPorts;

  // Instance Identifier
  QString mInstanceID;

  // The model we belong to
  ElaboratedModel* mModel;
};

//
// Class: BlockInstances
// Collection Class of <BlockInstance> Object
class BlockInstances : public QObject
{
  Q_OBJECT

  Q_PROPERTY(int count READ getCount)

public:
  BlockInstances()
  {
  }
  
  int getCount() const { return mBlockInstances.count(); }
  
  void addBlockInstance(BlockInstance* pi)
  {
    mBlockInstances.append(pi);
  }

  BlockInstance* getAt(int index)
  {
    return mBlockInstances[index];
  }

  void clear() { mBlockInstances.clear(); }

public slots:
  QObject* getBlockInstance(int index)
  {
    return mBlockInstances[index];
  }

private:
  QList<BlockInstance*> mBlockInstances;
};

#endif
