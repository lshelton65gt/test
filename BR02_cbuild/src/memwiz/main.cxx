//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <QtGui/QApplication>
#include "memwiz.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);


  /*QGraphicsScene scene(0, 0, 400, 300);
  QGraphicsView view(&scene);
  view.show();

  QGraphicsRectItem* rect1 = scene.addRect(QRectF(10,10,30,30));
  QGraphicsRectItem* rect2 = scene.addRect(QRectF(50,10,30,30));

  rect1->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);
  rect2->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);

  rect1->setSelected(true);
  rect2->setSelected(true);

  QGraphicsItemGroup *group = scene.createItemGroup(scene.selectedItems());
  group->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);*/

  //return a.exec();


  memwiz w;
  w.show();
  a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
  return a.exec();
}
