#ifndef __SCRIPTING_H_
#define __SCRIPTING_H_

#include <QtGui>
#include <QtXml>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>


class XmlErrorHandler
{
public:
  enum XmlErrorType { Warning, Error };

  XmlErrorHandler()
  {
    mErrors = 0;
    mWarnings = 0;
  }

  void reportProblem(XmlErrorType errType, const QDomElement& e, const QString& msg);
  bool hasErrors() const { return mErrors > 0; }
  bool numWarnings() const { return mWarnings; }
  bool numErrors() const { return mErrors; }
  const QStringList& errorMessages() const { return mMessages; }

private:
  int mWarnings;
  int mErrors;
  QStringList mMessages;
};

#endif

