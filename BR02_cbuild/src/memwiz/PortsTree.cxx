//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "PortsTree.h"
#include "WizardContext.h"
#include "ElaboratedModel.h"
#include "BlockInstance.h"
#include "Ports.h"
#include "PortInstance.h"
#include "DlgMapPortsToModule.h"


void PortsTree::applyMappings(DlgMapPortsToModule* dlg)
{ 
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);

    QString instName = item->text(1);
    QString baseName = dlg->mappedName(instName);
    item->setText(2, baseName);
  }
}

PortsTree::PortsTree(QWidget *parent)
: QTreeWidget(parent)
{
  QStringList colNames;

  colNames << "Block";
  colNames << "Port";
  colNames << "Base Name";
  colNames << "Description";

  setRootIsDecorated(false);
  setAlternatingRowColors(true);
  setSelectionMode(QAbstractItemView::SingleSelection);
  setSelectionBehavior(QAbstractItemView::SelectRows);

  setHeaderLabels(colNames);
}

PortsTree::~PortsTree()
{
}

void PortsTree::populate(WizardContext* context)
{
  clear();

  ElaboratedModel* model = context->getModel();
  PortInstances* instances = model->getUniquePorts();

  for (int i=0; i<instances->getCount(); i++)
  {
    PortInstance* portInst = instances->getAt(i);
    addItem(portInst);
  }
}

void PortsTree::addItem(PortInstance* portInst)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(this);
  item->setFlags(item->flags() | Qt::ItemIsEditable);

  BlockInstance* blockInst = portInst->getBlockInstance();

  QVariant qv = qVariantFromValue((void*)portInst);
  item->setData(0, Qt::UserRole, qv);

  item->setText(colBLOCKINST, blockInst->getInstanceName());
  item->setText(colPORT, portInst->getName());
  item->setText(colUSERPORT, "");

  int ix = -1;
  QString temp = portInst->getName();

  do // repeat until no more $(var)s
  {
    QRegExp exp1("\\$\\((\\w+)\\)");
    exp1.setPatternSyntax(QRegExp::RegExp);
    ix = exp1.indexIn(temp);
    int i2 = exp1.matchedLength();
    int nc = exp1.numCaptures();
    if (ix != -1 && nc == 1)
    {
      item->setText(colUSERPORT, exp1.cap(1));

      QString left = temp.left(ix);
      QString rest = temp.mid(ix+i2);
      temp = left + "${" + exp1.cap(1) + "}" + rest;
    }
  }
  while (ix != -1);
  
  item->setText(colDESCRIPTION, portInst->getPort()->getDescription());
} 

void PortsTree::commitData()
{
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant v = item->data(0, Qt::UserRole);
    if (!v.isNull())
    {
      PortInstance* portInst = (PortInstance*)v.value<void*>();
      QString value = item->text(colUSERPORT);
      portInst->getPort()->setBaseName(item->text(colUSERPORT));
    }
  }
}


