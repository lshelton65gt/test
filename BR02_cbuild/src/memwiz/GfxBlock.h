#ifndef __GFXBLOCK_H__
#define __GFXBLOCK_H__

#include "Graphics.h"

class Mode;
class BlockInstance;

class GfxBlock : public QObject, public QGraphicsItemGroup
{
  Q_OBJECT

public:
  GfxBlock(int x, int y, QGraphicsScene* scene, BlockInstance* blockInstance)
  {
    mX = x;
    mY = y;
    mHeight = 0;
    mWidth = 200;
    mBlockInst = blockInstance;
    calculate(scene);
  }
  int height() { return mHeight; }
  int width() { return mWidth; }
  static int textWidth(QGraphicsTextItem* txt);
  static int textHeight(QGraphicsTextItem* txt);
  void clicked();
  
  BlockInstance* getBlockInstance() { return mBlockInst; }
  QString getName() const { return mName; } 

signals:
  // if the user changes parameters which
  // causes the graphics to be re-rendered, emit this signal
  void graphicsChanged();
  void blockSelected(GfxBlock* block);

private:
  void calculate(QGraphicsScene* scene);
  virtual void mousePressEvent(QGraphicsSceneMouseEvent* ev);
  void addPorts();

private:
  BlockInstance* mBlockInst;
  int mHeight;
  int mWidth;
  int mX;
  int mY;
  QString mName;
};

class GraphicsTextItem : public QGraphicsTextItem
{
public:
  GraphicsTextItem(GfxBlock* block, QGraphicsItem* parent=0) : QGraphicsTextItem(parent)
  {
    mBlock = block;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
  }

  virtual void hoverEnterEvent( QGraphicsSceneHoverEvent* ev )
  {
    QGraphicsTextItem::hoverEnterEvent(ev);
    setCursor(Qt::PointingHandCursor);
  }

  virtual void hoverLeaveEvent( QGraphicsSceneHoverEvent* ev )
  {
    QGraphicsTextItem::hoverLeaveEvent(ev);
    unsetCursor();
  }

  virtual void mousePressEvent(QGraphicsSceneMouseEvent* ev)
  {
    qDebug() << "clicked!";
    QGraphicsTextItem::mousePressEvent(ev);
    mBlock->clicked();
  }

private:
  GfxBlock* mBlock;
};



#endif
