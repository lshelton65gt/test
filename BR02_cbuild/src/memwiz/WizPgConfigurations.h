#ifndef WIZPGCONFIGURATIONS_H
#define WIZPGCONFIGURATIONS_H

#include <QWizardPage>
#include "GfxModeSymbol.h"
#include "ui_WizPgConfigurations.h"

class GfxBlock;

class WizPgConfigurations : public QWizardPage
{
  Q_OBJECT

public:
  WizPgConfigurations(QWidget *parent = 0);
  ~WizPgConfigurations();
  int nextId() const;

protected:
  virtual void initializePage();

private slots:
  void blockSelected(GfxBlock*);

private:
  Ui::WizPgConfigurationsClass ui;
  GfxModeSymbol mSymbol;
  QGraphicsScene mScene;
};

#endif // WIZPGCONFIGURATIONS_H
