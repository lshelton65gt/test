//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "Scripting.h"

void XmlErrorHandler::reportProblem(XmlErrorType errType, const QDomElement& e, const QString& msg)
{
  switch (errType)
  {
  case Warning:
    mWarnings++;
    mMessages.append(
      QString("Warning: Line: %1: %2")
      .arg(e.lineNumber())
      .arg(msg)
      );
    break;
  case Error:
    mMessages.append(
      QString("Warning: Line: %1: %2")
      .arg(e.lineNumber())
      .arg(msg)
      );
    mErrors++;
    break;
  }
}
