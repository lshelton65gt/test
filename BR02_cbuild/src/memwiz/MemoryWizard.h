#ifndef MEMORYWIZARD_H
#define MEMORYWIZARD_H

#include <QtGui>

#include <QWizard>
#include <QWizardPage>

#include "ui_MemoryWizard.h"

class WizardTemplate;
class Template;

class MemoryWizard : public QWizard
{
  Q_OBJECT

public:
  enum { PageTemplates, PageParameters, PageModeParameters, PageConfigurations, PageUserPorts, PagePorts, PageCodeGenerate, PageFinished } ;

  MemoryWizard(QWidget *parent = 0);
  ~MemoryWizard();
  void setTemplate(WizardTemplate* templ) { mTemplate=templ; }
  WizardTemplate* getTemplate() const { return mTemplate; }

private:
  WizardTemplate* mTemplate;
  Ui::MemoryWizardClass ui;
};

#endif // MEMORYWIZARD_H
