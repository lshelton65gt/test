//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "MemoryWizard.h"
#include "WizPgPorts.h"
#include "WizardTemplate.h"
#include "WizardContext.h"
#include "Template.h"
#include "DlgAddWizardPort.h"
#include "DlgMapPortsToModule.h"

WizPgPorts::WizPgPorts(QWidget *parent)
: QWizardPage(parent)
{
  ui.setupUi(this);
  mIsCompleted = false;
}

WizPgPorts::~WizPgPorts()
{
}

void WizPgPorts::on_pushButtonMapModule_clicked()
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();
  WizardContext* context = templ->getContext();
  QString modeName = wiz->field("portMode").toString();
  Mode* mode = templ->getContext()->getTemplate()->findMode(modeName);

  DlgMapPortsToModule dlg(context, mode);
  if (dlg.exec() == QDialog::Accepted)
  {
    // get the mapped ports
    ui.treeWidgetPorts->applyMappings(&dlg);
    ui.treeWidgetPorts->commitData();
  }
}

void WizPgPorts::initializePage()
{
  mIsCompleted = true;
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();
  WizardContext* context = templ->getContext();

  QString modeName = wiz->field("portMode").toString();

  Mode* mode = templ->getContext()->getTemplate()->findMode(modeName);
  if (mode)
  {
    // Elaborate from the parameters now.
    if (!context->getModel()->elaborate(context, mode))
    {
      mIsCompleted = false;
      emit completeChanged();
    }
  }

  ui.treeWidgetPorts->populate(context);
}

bool WizPgPorts::validatePage()
{
  if (!mIsCompleted)
    return false;

  ui.treeWidgetPorts->commitData();
  return true;
}

bool WizPgPorts::isComplete() const
{
  return mIsCompleted;
}

int WizPgPorts::nextId() const
{
  if (mIsCompleted)
    return MemoryWizard::PageConfigurations;
  else
    return MemoryWizard::PageParameters;
}
