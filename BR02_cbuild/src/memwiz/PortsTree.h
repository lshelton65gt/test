#ifndef PORTSTREE_H
#define PORTSTREE_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

class Port;
class WizardContext;
class BlockInstance;
class PortInstance;
class DlgMapPortsToModule;

class PortsTree : public QTreeWidget
{
  Q_OBJECT

  enum { colBLOCKINST, colPORT, colUSERPORT, colDESCRIPTION, colLAST };
    
public:
  PortsTree(QWidget *parent);
  ~PortsTree();

  void populate(WizardContext* templ);
  void commitData();
  void applyMappings(DlgMapPortsToModule* dlg);

private:
  void addItem(PortInstance* portInst);

};

#endif // PORTSTREE_H
