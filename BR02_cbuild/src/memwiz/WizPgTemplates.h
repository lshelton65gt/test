#ifndef WIZPGTEMPLATES_H
#define WIZPGTEMPLATES_H

#include <QtGui>
#include <QWidget>
#include <QWizard>

#include "ui_WizPgTemplates.h"

class WizPgTemplates : public QWizardPage
{
  Q_OBJECT

public:
  WizPgTemplates(QWidget *parent = 0);
  ~WizPgTemplates();

  void populate();

private slots:
  void on_lineEditTemplateName_textChanged(const QString &);
  void itemSelectionChanged();

private:
  Ui::WizPgTemplatesClass ui;
  QWidget* mParent;
};

#endif // WIZPGTEMPLATES_H
