#ifndef WIZPGMODEPARAMETERS_H
#define WIZPGMODEPARAMETERS_H

#include <QWizardPage>
#include "ui_WizPgModeParameters.h"

class WizardTemplate;

class WizPgModeParameters : public QWizardPage
{
  Q_OBJECT

public:
  WizPgModeParameters(QWidget *parent);
  ~WizPgModeParameters();

protected:
  virtual void initializePage();
  int nextId() const;

private:
  Ui::WizPgModeParametersClass ui;

};

#endif // WIZPGMODEPARAMETERS_H
