#ifndef __BLOCKS_H__
#define __BLOCKS_H__

#include "Scripting.h"

class Block;
class Template;
class Mode;

// Deriving from QScriptable 

class Blocks : public QObject, protected QScriptable
{
  Q_OBJECT

  Q_PROPERTY(int count READ getCount)

public:
  int getCount() const { return mBlocks.count(); }
  void addBlock(Block* block)
  {
    mBlocks.append(block);
  }

  void removeUserDefined();
  bool deserialize(Mode* mode, Template* templ, const QDomElement& e, XmlErrorHandler* eh);

  Block* getAt(int index);
  Block* findBlock(const QString& blockName);

private:

public Q_SLOTS:
  QObject* addBlock(const QString& blockName, bool userBlock=true);
  QObject* getBlock(int index);

private:
  QList<Block*> mBlocks;
};
#endif
