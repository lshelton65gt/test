//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "MemoryWizard.h"
#include "WizPgTemplateParams.h"
#include "WizardTemplate.h"
#include "Template.h"
#include "PortConfigurations.h"
#include "PortConfiguration.h"
#include "Modes.h"
#include "Mode.h"

WizPgTemplateParams::WizPgTemplateParams(QWidget *parent)
    : QWizardPage(parent)
{
  ui.setupUi(this);

  registerField("portMode*", ui.comboBoxMode, "currentText");

  if (!connect(ui.treeWidget, SIGNAL(valuesChanged()), this, SLOT(valuesChanged())))
  {
    qDebug() << "ERROR: No such slot valuesChanged";
  }
}

WizPgTemplateParams::~WizPgTemplateParams()
{
}

bool WizPgTemplateParams::validatePage()
{
  return QWizardPage::validatePage();
}

bool WizPgTemplateParams::hasAllRequiredParameters() const
{
  bool retValue = true;
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* wizTempl = wiz->getTemplate();
  Parameters* params = wizTempl->getContext()->getTemplate()->getParameters();
  for (int i=0; i<params->getCount(); i++)
  {
    Parameter* p = params->getAt(i);
    QString validExpr = p->getValidExpression();

    if (p->getRequired() && !p->hasValueSet())
      return false;

    // Test against expression
    if (validExpr.length() > 0)
    {
      QRegExp rx(validExpr);
      QString value = p->getValue().toString();
      if (rx.indexIn(value) == -1)
        return false;
    }
  }

  return retValue;
}

// isCompleted is called during initialize and 
// also when hitting 'back'
bool WizPgTemplateParams::isComplete() const
{
  bool newValue = hasAllRequiredParameters();

  qDebug() << "isComplete: " << newValue;

  return newValue;
}


// A parameter has been edited
void WizPgTemplateParams::valuesChanged()
{
  emit completeChanged();
}

void WizPgTemplateParams::initializePage()
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();

  ui.treeWidget->populate(templ->getContext()->getTemplate()->getParameters());

  populateModes(templ);
}

void WizPgTemplateParams::populateModes(WizardTemplate* wt)
{
  Template* templ = wt->getContext()->getTemplate();
  ui.comboBoxMode->clear();

  PortConfigurations* portConfigs = templ->getPortConfigs();
  for (int i=0; i<portConfigs->getCount(); i++)
  {
    PortConfiguration* portConfig = portConfigs->getAt(i);
    Modes* modes = portConfig->getModes();
    for (int m=0; m<modes->getCount(); m++)
    {
      Mode* mode = modes->getAt(m);
      ui.comboBoxMode->addItem(mode->getName());
    }
  }
}

int WizPgTemplateParams::nextId() const
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardContext* context = wiz->getTemplate()->getContext();
  Template* templ = context->getTemplate();

  QString modeName = wiz->field("portMode").toString();
  Mode* mode = templ->findMode(modeName);

  if (mode && mode->getParameters()->getCount() > 0)
    return MemoryWizard::PageModeParameters;
  else if (templ->getPortConfigs()->getCount() > 0)
    return MemoryWizard::PageUserPorts;
  else
    return MemoryWizard::PageUserPorts;
}
