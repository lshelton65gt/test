//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "Template.h"
#include <QFileInfo>
#include "Resources.h"
#include "PortConfigurations.h"
#include "PortConfiguration.h"
#include "Modes.h"
#include "Mode.h"

Parameter* Template::findParameter(const QString& paramName)
{
  for (int i=0; i<mParameters.getCount(); i++)
  {
    Parameter* param = mParameters.getAt(i);
    if (paramName == param->getName())
      return param;
  }
  return NULL;
}

Mode* Template::findMode(const QString& modeName)
{
  PortConfigurations* portConfigs = getPortConfigs();
  for (int i=0; i<portConfigs->getCount(); i++)
  {
    PortConfiguration* portConfig = portConfigs->getAt(i);
    Modes* modes = portConfig->getModes();
    for (int m=0; m<modes->getCount(); m++)
    {
      Mode* mode = modes->getAt(m);
      if (mode->getName() == modeName)
        return mode;
    }
  }
  return NULL;
}

Parameters* Template::getParameters()
{
  return &mParameters; 
}

bool Template::read(QScriptEngine* engine, const QString& templateFile, XmlErrorHandler* eh)
{
  bool retValue = true;

  QFile file(templateFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(templateFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      QDomNode n = docElem.firstChild();

      if (docElem.hasAttribute("name"))
        setName(docElem.attribute("name"));
      
      if (docElem.hasAttribute("description"))
        setDescription(docElem.attribute("description"));

      if (docElem.hasAttribute("language"))
      {
        QString lang = docElem.attribute("language").toLower();

        if ("verilog" == lang)
          setLanguage(Verilog);
        else if ("vhdl" == lang)
          setLanguage(VHDL);
      }
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.

        //qDebug() << "tag: " << e.tagName();

        if(!e.isNull()) 
        {
          if ("Parameters" == e.tagName())
          {
            if (!mParameters.deserialize(docElem, eh))
              return false;
          }
          else if ("PortConfigurations" == e.tagName())
          {
            if (!mPortConfigurations.deserialize(this, e, eh))
              return false;
          }
          else if ("Resources" == e.tagName())
          {
            if (!mResources.deserialize(this, docElem, eh))
              return false;
          }
          else if ("Include" == e.tagName())
          {
            if (!e.hasAttribute("script"))
            {
              eh->reportProblem(XmlErrorHandler::Error, e, "Error: Include tag missing 'script' attribute");
              return false;
            }

            QString includeFile;
            QString scriptName = e.attribute("script");

            QFileInfo fi(scriptName);
            if (fi.isRelative())
              includeFile = templateFi.absolutePath() + "/" + scriptName;
            else
              includeFile = fi.absoluteFilePath();

            QFileInfo newFi(includeFile);
            if (newFi.exists())
              mIncludeScripts.append(includeFile);
            else
            {
              eh->reportProblem(XmlErrorHandler::Error, e, 
                QString("Error: Include file: %1 does not exist\n").arg(includeFile));
              return false;
            }
          }
          else if (e.tagName().length() > 0)
          {
            eh->reportProblem(XmlErrorHandler::Error, e, 
                QString("Error: Unknown Tag: %1").arg(e.tagName()));
            return false;
          }
        }
        n = n.nextSibling();
      }
    }
    else
    {
      QMessageBox::information(NULL, tr("Template Error"),
        tr("Parse error at line %1, column %2:\n%3")
        .arg(errorLine)
        .arg(errorCol)
        .arg(errStr));
      
      retValue = false;
    }
  }

  // On success, read all referenced scripts into this engine now.
  if (retValue)
  {
    foreach (QString script, getScripts())
    {
      QFile file(script);
      file.open(QIODevice::ReadOnly);
      QScriptValue result = engine->evaluate(file.readAll());
      if (engine->hasUncaughtException())
      {
        qDebug() << engine->uncaughtExceptionBacktrace();

        QString errMsg = QString("Script Error: %1 line %2\n%3")
          .arg(script)
          .arg(engine->uncaughtExceptionLineNumber())
          .arg(engine->uncaughtException().toString());    
      
        QMessageBox::information(NULL, tr("Script Error"), errMsg);
      }
    }
  }
  return retValue;
}
