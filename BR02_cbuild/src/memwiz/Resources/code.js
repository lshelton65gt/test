
function formatParameter(name, p)
{
  return "  parameter " + name + " = " + p.value + ";";
}

function byteEnabled(context)
{  
  return context.template.parameters.Bitmasked.boolValue;
}

// The Main entry point for code generation
function generateCode(context, response)
{
  response.write("module <moduleName>(");
  generatePortList(context, response);
  response.writeLine(");");
  
  generateBody(context, response);
  
  response.writeLine("endmodule");
}

// Builds up the elaborated port list
function generatePortList(context, response)
{
  var parameters = context.template.parameters;
  var ports = context.template.ports;
    
  var portSent = false;
  for (var i=0; i<parameters.Ports.intValue; i++)
  {
    for (var pi=0; pi<ports.count; pi++)
    {
        var port = ports.getPort(pi);
                
        // skip this port
        if (port.hasOnCondition() && !port.evaluateOnCondition(context))
            continue;
                    
        var expandedName = expandPort(port, "portNumber", i);
        
        if (portSent)
            response.write(",");
        response.write(expandedName);
            
        portSent = true;
    }
  }
}

function generateBody(context, response)
{
  var parameters = context.template.parameters;
  var ports = context.template.ports;

  generateParameters(context, response);

  var pNumPorts = parameters.Ports;

  response.writeLine("// ports, storage");
  for (var i=0; i<pNumPorts.intValue; i++)
  {   
    response.writeLine(sprintf("// port %d", i));
    response.writeLine(sprintf("  input %s;", expandPort(ports.Clock, "portNumber", i)));
    response.writeLine(sprintf("  input [ADDR_WIDTH-1:0] %s;", expandPort(ports.Address, "portNumber", i)));
    response.writeLine(sprintf("  input [DATA_WIDTH-1:0] %s;", expandPort(ports.DataIn, "portNumber", i)));
    response.writeLine(sprintf("  output [DATA_WIDTH-1:0] %s;", expandPort(ports.DataOut, "portNumber", i)));
    response.writeLine(sprintf("  reg [DATA_WIDTH-1:0] %s;", expandPort(ports.DataOut, "portNumber", i)));
    response.writeLine(sprintf("  input %s;", expandPort(ports.WriteEnable, "portNumber", i)));
    response.writeLine("");
  }
  
  response.writeLine("");

  // write enable
  response.writeLine("  reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];");
  
  generateImplementation(context, response);
}

function generateImplementation(context, response)
{
  var parameters = context.template.parameters;
  var ports = context.template.ports;

  var pNumPorts = parameters.Ports;
  
  response.writeLine("// Implementation");
  for (var i=0; i<pNumPorts.intValue; i++)
  {
    response.writeLine(sprintf("// port %d", i));
    response.writeLine(sprintf("  always @(posedge %s)", expandPort(ports.Clock, "portNumber", i)));
    response.writeLine(sprintf("    if (%s)", expandPort(ports.WriteEnable, "portNumber", i)));
    response.writeLine(sprintf("      mem[%s] <= %s;", 
                    expandPort(ports.Address, "portNumber", i),
                    expandPort(ports.DataIn, "portNumber", i)));
    response.writeLine(sprintf("    else"));
    response.writeLine(sprintf("      %s <= mem[%s];",
                    expandPort(ports.DataOut, "portNumber", i),
                    expandPort(ports.Address, "portNumber", i)));
    response.writeLine("");
  }
}

function generateParameters(context, response)
{
  var parameters = context.template.parameters;

  var pNumPorts = parameters.Ports;
  var pAddrWidth = parameters.AddressWidth;
  var pDataWidth = parameters.DataWidth;
  var pWords = parameters.Words;
  var pReadType = parameters.ReadType;
  
  response.writeLine(formatParameter("ADDR_WIDTH", pAddrWidth));
  response.writeLine(formatParameter("DATA_WIDTH", pDataWidth));
  response.writeLine(formatParameter("DEPTH", pWords));
  
  return true;
}