//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "PortConfigurations.h"
#include "PortConfiguration.h"
#include "Template.h"
#include "Modes.h"
#include "Mode.h"

Modes* PortConfiguration::getModes()
{
  return &mModes;
}

void PortConfiguration::addMode(Mode* mode)
{
  mModes.addMode(mode);
  QVariant v = qVariantFromValue((QObject*)mode);  
  QString name = mode->getName().replace(" ", "");
  setProperty(name.toAscii(), v); // this_toascii_ok
}

bool PortConfiguration::deserialize(Template* templ, PortConfigurations* configs, const QDomElement& parent, XmlErrorHandler* eh)
{
  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.

    if ("PortConfiguration" == e.tagName())
    {
      if (e.hasAttribute("name"))
      {
        PortConfiguration* config = new PortConfiguration();

        QString name = e.attribute("name");
        config->setName(name);

        //QString parameter = e.attribute("parameter");
        //Parameters* params = templ->getParameters();

        //for (int i=0; i<params->getCount(); i++)
        //{
        //  Parameter* p = params->getParam(i);
        //  if (p->getName() == parameter)
        //  {
        //    config->setParameter(p);
        //    break;
        //  }
        //}

        //if (config->getParameter() == NULL)
        //{
        //  eh->reportProblem(XmlErrorHandler::Error, e, 
        //    QString("Error: unable to locate parameter '%1' from PortConfiguration '%2'").arg(parameter).arg(name));
        //  return NULL;
        //}

        configs->addPortConfiguration(config);

        // Deserialize the Mode
        if (!config->mModes.deserialize(templ, e, eh))
          return false;
      }
      else
      {
        eh->reportProblem(XmlErrorHandler::Error, e, "PortConfiguration missing name and/or parameter attribute");
        return false;
      }  
    }
    n = n.nextSibling();
  }
  return true;
}
