#ifndef __PortConfigurations_H_
#define __PortConfigurations_H_

#include "Scripting.h"

class PortConfiguration;
class Template;

// File: PortConfigurations
// The Collection of <PortConfiguration> objects.
//
// Class: PortConfigurations
//
class PortConfigurations : public QObject
{
  Q_OBJECT

  // Property: count
  //
  // Access:
  // *ReadOnly*
  //  
  Q_PROPERTY(int count READ getCount)

public:
  void addPortConfiguration(PortConfiguration* config);
  int getCount() const { return mPortConfigurations.count(); }
  bool deserialize(Template* templ, const QDomElement& e, XmlErrorHandler* eh);
  PortConfiguration* getAt(int index) { return mPortConfigurations[index]; }

public slots:
  QObject* getPortConfiguration(int index);


private:
  QList<PortConfiguration*> mPortConfigurations;  
};

#endif
