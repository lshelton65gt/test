#ifndef WIZPGFINISHED_H
#define WIZPGFINISHED_H

#include <QWizardPage>
#include "ui_WizPgFinished.h"

class WizPgFinished : public QWizardPage
{
  Q_OBJECT

public:
  WizPgFinished(QWidget *parent = 0);
  ~WizPgFinished();

  virtual void initializePage();

private:
  Ui::WizPgFinishedClass ui;
};

#endif // WIZPGFINISHED_H
