#ifndef WIZPGTEMPLATEPARAMS_H
#define WIZPGTEMPLATEPARAMS_H

#include <QWizardPage>
#include "ui_WizPgTemplateParams.h"

class WizardTemplate;

class WizPgTemplateParams : public QWizardPage
{
  Q_OBJECT

public:
  WizPgTemplateParams(QWidget *parent = 0);
  ~WizPgTemplateParams();

  int nextId() const;

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const;
  void populateModes(WizardTemplate* templ);
  bool hasAllRequiredParameters() const;
 
private slots:
  void valuesChanged();

private:
  Ui::WizPgTemplateParamsClass ui;
};

#endif // WIZPGTEMPLATEPARAMS_H
