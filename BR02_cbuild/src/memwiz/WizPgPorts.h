#ifndef WizPgPorts_H
#define WizPgPorts_H

#include <QWizardPage>
#include "ui_WizPgPorts.h"

class WizPgPorts : public QWizardPage
{
  Q_OBJECT

public:
  WizPgPorts(QWidget *parent = 0);
  ~WizPgPorts();
  virtual void initializePage();
  virtual bool validatePage();

  int nextId() const;
  bool isComplete () const;

private slots:
  void on_pushButtonMapModule_clicked();

private:
  Ui::WizPgPortsClass ui;
  bool mIsCompleted;
};

#endif // WizPgPorts_H
