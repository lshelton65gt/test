//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "Mode.h"
#include "Blocks.h"
#include "Block.h"

QObject* Blocks::getBlock(int index)
{
  return mBlocks[index];
}

Block* Blocks::findBlock(const QString& blockName)
{
  foreach (Block* block, mBlocks)
  {
    if (blockName == block->getName())
      return block;
  }
  return NULL;
}

// Walk the list, find any userports and remove them
void Blocks::removeUserDefined()
{
  QMutableListIterator<Block*> i(mBlocks);
  while (i.hasNext())
  {
    Block* block = i.next();
    if (block->isUserDefined())
      i.remove();
  }
}

QObject* Blocks::addBlock(const QString& blockName, bool userBlock)
{
  Block* block = findBlock(blockName);
  if (block == NULL)
  {
    block = new Block(userBlock);
    block->setName(blockName);
    addBlock(block);
    return block;
  }
  else
  {
    context()->throwError(QScriptContext::SyntaxError, QString("Block with name '%1' already exists.").arg(blockName));
    return NULL;
  }
}

Block* Blocks::getAt(int index)
{
  return mBlocks[index];
}

bool Blocks::deserialize(Mode* mode, Template* templ, const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull())
    {
      if ("Blocks" == element.tagName())
      {
        if (!Block::deserialize(mode, templ, this, element, eh))
          return false;        
      }
    }
    n = n.nextSibling();
  }

  return true;
}
