//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "PortTree.h"
#include "Ports.h"

PortTree::PortTree(QWidget *parent)
: QTreeWidget(parent)
{
  setColumnCount(colLAST);

  QStringList colNames;

  colNames << "Port";
  colNames << "Description";

  setAlternatingRowColors(true);
  setSelectionMode(QAbstractItemView::SingleSelection);
  setSelectionBehavior(QAbstractItemView::SelectRows);
  setEditTriggers(QAbstractItemView::AllEditTriggers);
  setRootIsDecorated(false);

  setHeaderLabels(colNames);
}

void PortTree::populate(Ports* ports)
{
  clear();

  if (ports)
  {
    for (int i=0; i<ports->getCount(); i++)
    {
      Port* p = ports->getAt(i);
      addRow(p);
    }
  }
}

void PortTree::addRow(Port* p)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(this);
  item->setFlags(item->flags() | Qt::ItemIsEditable);
  item->setText(colNAME, p->getName());
  
  QVariant qv = qVariantFromValue((void*)p);
  item->setData(colNAME, Qt::UserRole, qv);

  item->setText(colDESCRIPTION, p->getDescription());  
}

PortTree::~PortTree()
{
 

}
