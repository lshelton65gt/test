#ifndef COLORIZEDEDITOR_H
#define COLORIZEDEDITOR_H

#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexervhdl.h>

class ColorizedEditor : public QsciScintilla
{
  Q_OBJECT

public:
  enum LanguageKind { Verilog, VHDL };

  ColorizedEditor(QWidget *parent);
  ~ColorizedEditor();

  void setLanguage(LanguageKind language);

private:
  LanguageKind mLanguage;
};

#endif // COLORIZEDEDITOR_H
