//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "WizPgTemplates.h"

#ifdef Q_OS_WIN
#pragma warning( disable : 4996)
#endif

#include "WizardTemplate.h"
#include "WizardContext.h"
#include "Parameters.h"
#include "Template.h"
#include "MemoryWizard.h"

#include <QDebug>

WizPgTemplates::WizPgTemplates(QWidget *parent)
    : QWizardPage(parent)
{
  mParent = parent;

  ui.setupUi(this);

  ui.treeWidget->setRootIsDecorated(false);

  ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  ui.treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

  ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
  ui.treeWidget->header()->setResizeMode(1, QHeaderView::Interactive);

  populate();

  connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));

  registerField("templateName*", ui.lineEditTemplateName);
  registerField("templatePath", new QLineEdit());
}

WizPgTemplates::~WizPgTemplates()
{

}

void WizPgTemplates::populate()
{
  QString wizDirEnv = getenv("CARBON_MEMWIZARD");

#ifdef Q_OS_WIN
  QStringList wizDirs = wizDirEnv.split(";");
#else
  QStringList wizDirs = wizDirEnv.split(":");
#endif

  foreach (QString wizDir, wizDirs)
  {
    QDir qdir(wizDir);
    
    qdir.setFilter(QDir::Files);
    QStringList filters;
    filters << "*.xml";
    qdir.setNameFilters(filters);

    QStringList files = qdir.entryList();
    foreach (QString file, files)
    {
      QFileInfo fi(wizDir + "/" + file);
      qDebug() << fi.absoluteFilePath();
      WizardTemplate* wizTemplate = new WizardTemplate(mParent);

      if (wizTemplate->readTemplate(fi.absoluteFilePath()))
      {
        Template* templ = wizTemplate->getContext()->getTemplate();
        QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
        item->setText(0, templ->getName());
        item->setText(1, templ->getDescription());
        QVariant qv = qVariantFromValue((void*)wizTemplate);
        item->setData(0, Qt::UserRole, qv);
      }
    }
  }
}

void WizPgTemplates::itemSelectionChanged()
{
  QTreeWidgetItem* item = ui.treeWidget->currentItem();

  ui.lineEditTemplateName->setText(item->text(0));

  if (item)
  {
    WizardTemplate* templ = NULL;
    QVariant v = item->data(0, Qt::UserRole);
    if (!v.isNull())
    {
      templ = (WizardTemplate*)v.value<void*>();
      MemoryWizard* wiz = (MemoryWizard*)wizard();
      wiz->setTemplate(templ);
      setField("templatePath", templ->getTemplatePath());
    }
  }
}



void WizPgTemplates::on_lineEditTemplateName_textChanged(const QString &)
{

}
