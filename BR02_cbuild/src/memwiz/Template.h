#ifndef __TEMPLATE_H__
#define __TEMPLATE_H__

#include <QtGui>
#include <QtXml>

#include "Parameters.h"
#include "Ports.h"
#include "PortConfigurations.h"
#include "PortConfiguration.h"
#include "Resources.h"

class Mode;

// File: Template
// (start code)
//   <Template description="Registered, BitMasked" name="Carbon mRnW (mRead Ports nWrite Ports) RAM" language="Verilog">
//     <Parameters>
//     ...
//     </Parameters>
//
//     <PortConfigurations>
//     ...
//     </PortConfigurations>
//
//     <Include script="mrnw.js"/>
//
//     <Resources>
//      ...
//     </Resources>
//   </Template>
//
//(end)

class Template : public QObject
{
  Q_OBJECT
  Q_ENUMS(GeneratedLanguage)

  // Property: parameters
  // A collection of <Parameter> objects.
  //
  // See Also:
  // <Parameters>
  Q_PROPERTY(QObject* parameters READ getParameters);
  // Property: portConfiguration
  //
  // See Also:
  // <PortConfiguration>
  Q_PROPERTY(QObject* portConfiguration READ getPortConfiguration);
  // Property: description
  Q_PROPERTY(QString description READ getDescription);
  // Property: name
  Q_PROPERTY(QString name READ getName);
  // Property: language
  Q_PROPERTY(GeneratedLanguage language READ getLanguage)

  // Property: resources
  Q_PROPERTY(QObject* resources READ getResources)

public:
   enum GeneratedLanguage { Verilog, VHDL };

  Template()
  {
    mBuildNames = false;
    mPortConfiguration = NULL;
  }

  PortConfiguration* getPortConfiguration() const { return mPortConfiguration; }
  Parameters* getParameters();
  PortConfigurations* getPortConfigs() { return &mPortConfigurations; }
  Mode* findMode(const QString& modeName);
  Parameter* findParameter(const QString& paramName);
  Resources* getResources() { return &mResources; }

  bool read(QScriptEngine* engine, const QString& templateFile, XmlErrorHandler* eh);

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getDescription() const { return mDescription; }
  void setDescription(const QString& newVal) { mDescription=newVal; }

  GeneratedLanguage getLanguage() const { return mLanguage; }
  void setLanguage(GeneratedLanguage newVal) { mLanguage=newVal; }

  const QStringList& getScripts() { return mIncludeScripts; }

private:
  QString mName;
  QString mDescription;
  bool mBuildNames;
  Parameters mParameters;
  PortConfigurations mPortConfigurations;
  QStringList mIncludeScripts;
  PortConfiguration* mPortConfiguration;
  GeneratedLanguage mLanguage;
  Resources mResources;
};

#endif
