//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "MemoryWizard.h"
#include "WizPgModeParameters.h"
#include "WizardTemplate.h"
#include "Template.h"
#include "Modes.h"
#include "Mode.h"

WizPgModeParameters::WizPgModeParameters(QWidget *parent)
: QWizardPage(parent)
{
  ui.setupUi(this);
}

WizPgModeParameters::~WizPgModeParameters()
{
}

void WizPgModeParameters::initializePage()
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardContext* context = wiz->getTemplate()->getContext();
  Template* templ = context->getTemplate();

  QString modeName = wiz->field("portMode").toString();

  ui.labelParameters->setText(QString("%1 Parameters").arg(modeName));
  Mode* mode = templ->findMode(modeName);

  ui.treeWidget->populate(mode->getParameters());
}

int WizPgModeParameters::nextId() const
{
  return MemoryWizard::PageUserPorts;
}
