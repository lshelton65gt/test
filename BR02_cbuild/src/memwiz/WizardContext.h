#ifndef __WIZARDCONTEXT_H
#define __WIZARDCONTEXT_H

#include <QtGui>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

#include "DlgParameters.h"
#include "Template.h"
#include "ElaboratedModel.h"

// File: Context
//
// Description:
// The toplevel context object
// This object is passed to the main entry points
class WizardContext : public QObject
{
  Q_OBJECT

  // Property: template
  // The XML <Template> used in this context
  Q_PROPERTY(QObject* template READ propGetTemplate)
  // Property: model
  // The <Model> used by this context
  Q_PROPERTY(QObject* model READ getModel)

public:
  WizardContext(QScriptEngine* engine);
  
  QString generateCode();

  Template* getTemplate() { return &mTemplate; }
  QScriptValue callScriptFunction(const QString& methodName, const QScriptValueList args);
  QScriptValue callOptionalScriptFunction(const QString& methodName, const QScriptValueList args);
  
  QScriptEngine* getScriptEngine() const { return mEngine; }

  ElaboratedModel* getModel();

private:
  void addBuiltinFunctions();

// Property getter/setters
private:
  QObject* propGetTemplate() { return &mTemplate; }

private:
  Template mTemplate;
  ElaboratedModel mModel;
  QScriptEngine* mEngine;
};

#endif
