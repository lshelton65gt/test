#ifndef __RESPONSE_H
#define __RESPONSE_H

#include <QtGui>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>

class Response : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString value READ getResponse);

public:
  QString getResponse() const
  {
    return mResult;
  }

  Response() {}

public slots:
  void clear()
  {
    mResult = "";
  }
  void write(const QString& line)
  {
    mResult += line;
  }
  void writeLine(const QString& line)
  {
    if (mIndentList.count() > 0)
      mResult += mIndentList.first();

    mResult += line + "\n";
  }

  void pushIndent(int spaces)
  {
    if (mIndentList.count() > 0)
      mIndentList.push_front(mIndentList.front() + QString().fill(' ', spaces));
    else
      mIndentList.push_front(QString().fill(' ', spaces));
  }

  void popIndent()
  {
    if (mIndentList.count() > 0)
      mIndentList.pop_front();
  }

private:
  QStringList mIndentList;
  QString mResult;
};

#endif
