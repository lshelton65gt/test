//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

//#include "memwiz.h"
#include "WizardContext.h"
#include "Response.h"
#include "DlgParameters.h"
#include "ElaboratedModel.h"
#include <QUiLoader>
#include <QtScript>


ElaboratedModel* WizardContext::getModel()
{
  return &mModel;
}

static QScriptValue expandFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  int nArgs = ctx->argumentCount();
  if (nArgs > 1)
  {
    QScriptValue portScriptObj = ctx->argument(0);
    Port* port = dynamic_cast<Port*>(portScriptObj.toQObject());
    if (port)
    {
      QString expandedValue = port->getFormat();
      for (int i=1; i<nArgs; i += 2)
      {
        QString argName = QString("^(%1)").arg(ctx->argument(i).toString());
        QString argValue = ctx->argument(i+1).toString();

        expandedValue = expandedValue.replace(argName, argValue);        
      }    

      QString baseName = port->getBaseName();
      if (baseName.length() > 0)
      {      
        QRegExp exp("(\\$\\(.*\\))");
        int ix = exp.indexIn(expandedValue);
        int nc = exp.numCaptures();
        if (ix != -1 && nc == 1)
        {
          QString varname = exp.cap(1);
          expandedValue = expandedValue.replace(varname, baseName);
        }
      }

      return QScriptValue(eng, expandedValue);
    }
    else
    {
      QScriptValue caller = ctx->activationObject();

      qDebug() << portScriptObj.toString();
      qDebug() << caller.toString();

      ctx->throwError(QScriptContext::SyntaxError, "No such port: " + portScriptObj.toString());
    }
  }

  return eng->undefinedValue();
}

static QScriptValue alertFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  // if numargs = 1
  QString title = "Carbon Rules";
  QString message;

  int nArgs = ctx->argumentCount();

  if (nArgs == 1)
    message = ctx->argument(0).toString();
  else if (nArgs == 2)
  {
    title = ctx->argument(0).toString();
    message = ctx->argument(1).toString();
  }
  
  QMessageBox::information(NULL, title, message);

  return eng->undefinedValue();
}

static QScriptValue debugFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QScriptValue arg1 = ctx->argument(0);
  
  qDebug() << "debug: " << arg1.toString();

  //if (theApp)
  //{
  //  theApp->textBrowser()->append(arg1.toString());
  //}

  return eng->undefinedValue();
}

static QScriptValue loaduiFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QScriptValue uiFileName = ctx->argument(0);
  qDebug() << "debug: " << uiFileName.toString();

  QUiLoader loader;
  QFile uiFile(uiFileName.toString());
  uiFile.open(QIODevice::ReadOnly);
  QWidget *ui = loader.load(&uiFile);
  uiFile.close();

  QScriptValue scriptUi = eng->newQObject(ui);
  ui->show();

  return scriptUi;
}


void WizardContext::addBuiltinFunctions()
{
  qDebug() << "addingBuiltinFunctions";

  QScriptValue global = mEngine->globalObject();

  QScriptValue staticPort = mEngine->newQMetaObject(&Port::staticMetaObject);
  global.setProperty("Port", staticPort);

  QScriptValue staticPorts = mEngine->newQMetaObject(&Ports::staticMetaObject);
  global.setProperty("Ports", staticPorts);

  QScriptValue expandFunc = mEngine->newFunction(expandFunction);
  if (!global.property(QLatin1String("expandPort")).isValid())
    global.setProperty(QLatin1String("expandPort"), expandFunc);

  QScriptValue alertFunc = mEngine->newFunction(alertFunction);
  if (!global.property(QLatin1String("alert")).isValid())
    global.setProperty(QLatin1String("alert"), alertFunc);

  QScriptValue debugFunc = mEngine->newFunction(debugFunction);
  if (!global.property(QLatin1String("debug")).isValid())
    global.setProperty(QLatin1String("debug"), debugFunc);


  QScriptValue loaduiFunc = mEngine->newFunction(loaduiFunction);
  if (!global.property(QLatin1String("loadui")).isValid())
    global.setProperty(QLatin1String("loadui"), loaduiFunc);


  // Import included files
  QFile fileCommon(":memwiz/Resources/common.js");
  fileCommon.open(QIODevice::ReadOnly);
  QScriptValue resultCommon = mEngine->evaluate(fileCommon.readAll());
  if (mEngine->hasUncaughtException())
  {
    QMessageBox::critical(NULL, "Script Error: common.js", 
      tr("Exception %1 on line %2")
      .arg(mEngine->uncaughtException().toString())
      .arg(mEngine->uncaughtExceptionLineNumber()));
  }
}
WizardContext::WizardContext(QScriptEngine* engine)
{
  mEngine = engine;
  addBuiltinFunctions();
}

QString WizardContext::generateCode()
{  
  Response response;
  QScriptValueList list;
  list.append(mEngine->newQObject(this));
  list.append(mEngine->newQObject(&response));

  QScriptValue pv = callScriptFunction("generateCode", list);

  return response.getResponse();
}

QScriptValue WizardContext::callOptionalScriptFunction(const QString& methodName, const QScriptValueList args)
{
  QScriptValue retValue(mEngine, true);
  
  QScriptValue func = mEngine->globalObject().property(methodName);
  if (func.isFunction())
  {
    qDebug() << "Calling Optional ScriptMethod: " << methodName;
    retValue = func.call(mEngine->globalObject(), args);
    qDebug() << "Returned From Optional ScriptMethod: " << methodName << " returnValue: " << retValue.toString();

    if (mEngine->hasUncaughtException())
    {
      QMessageBox::critical(NULL, "Script Error", 
        tr("Exception %1 on line %2")
        .arg(mEngine->uncaughtException().toString())
        .arg(mEngine->uncaughtExceptionLineNumber()));
    }
    else if (!retValue.isBoolean() || (retValue.isBoolean() && !retValue.toBoolean()))
    {
      QMessageBox::critical(NULL, "Script Error", retValue.toString());
      mEngine->currentContext()->throwError(QScriptContext::ReferenceError, retValue.toString());
    }
  }

  return retValue;
}

QScriptValue WizardContext::callScriptFunction(const QString& methodName, const QScriptValueList args)
{
  QScriptValue retValue;
  QScriptValue func = mEngine->globalObject().property(methodName);
  if (func.isFunction())
  {
    qDebug() << "Calling ScriptMethod: " << methodName;
    retValue = func.call(mEngine->globalObject(), args);
    qDebug() << "Returned From ScriptMethod: " << methodName << " returnValue: " << retValue.toString();

    if (mEngine->hasUncaughtException())
    {
      QMessageBox::critical(NULL, "Script Error", 
        tr("Exception %1 on line %2")
        .arg(mEngine->uncaughtException().toString())
        .arg(mEngine->uncaughtExceptionLineNumber()));
     }
  }
  else
  {
    QMessageBox::warning(NULL, "Bad Method", tr("No such method: %1").arg(methodName));
  }
  return retValue;
}
