//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "MemoryWizard.h"

#include "WizPgTemplates.h"
#include "WizPgTemplateParams.h"
#include "WizPgConfigurations.h"
#include "WizPgFinished.h"
#include "WizPgUserPorts.h"
#include "WizPgPorts.h"
#include "WizPgCodeGenerate.h"
#include "WizPgModeParameters.h"

MemoryWizard::MemoryWizard(QWidget *parent)
: QWizard(parent)
{
  ui.setupUi(this);

  setPage(MemoryWizard::PageTemplates, new WizPgTemplates(this));
  setPage(MemoryWizard::PageParameters, new WizPgTemplateParams(this));
  setPage(MemoryWizard::PageModeParameters, new WizPgModeParameters(this));
  setPage(MemoryWizard::PageUserPorts, new WizPgUserPorts(this));
  setPage(MemoryWizard::PagePorts, new WizPgPorts(this));
  setPage(MemoryWizard::PageConfigurations, new WizPgConfigurations(this));
  setPage(MemoryWizard::PageCodeGenerate, new WizPgCodeGenerate(this));
  setPage(MemoryWizard::PageFinished, new WizPgFinished(this));

  setStartId(MemoryWizard::PageTemplates);
}

MemoryWizard::~MemoryWizard()
{

}
