//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "ParametersTree.h"
#include "ParametersTreeDelegate.h"
#include "Template.h"

#include <QDebug>

ParametersTree::ParametersTree(QWidget *parent)
: QTreeWidget(parent)
{
  setColumnCount(colLAST);

  QStringList colNames;

  colNames << "Parameter";
  colNames << "Value";
  colNames << "Description";

  setAlternatingRowColors(true);
  setSelectionMode(QAbstractItemView::SingleSelection);
  setSelectionBehavior(QAbstractItemView::SelectItems);
  setEditTriggers(QAbstractItemView::AllEditTriggers);
  setRootIsDecorated(false);

  setHeaderLabels(colNames);

  setItemDelegate(new ParametersTreeDelegate(this, this));
}

ParametersTree::~ParametersTree()
{

}

void ParametersTree::populate(Parameters* params)
{
  clear();

  bool focusRequired = true;
  
  if (params)
  {
    QTreeWidgetItem* focusItem = NULL;

    for (int i=0; i<params->getCount(); i++)
    {
      Parameter* p = params->getAt(i);
      QTreeWidgetItem* item = addRow(p);
      if (p->isRequired() && focusRequired)
      {
        focusItem = item;
        focusRequired = false;
      }
    }

    if (focusItem)
    {
      setCurrentItem(focusItem, colVALUE);
      QTimer* tempTimer = new QTimer(this);
      tempTimer->setSingleShot(true);
      tempTimer->setInterval(10);     // milliseconds
      connect(tempTimer, SIGNAL(timeout()), this, SLOT(editSelectedItem()));
      tempTimer->start();
    }
  }
}

void ParametersTree::editSelectedItem()
{
  editItem(currentItem(), colVALUE);
}

QTreeWidgetItem* ParametersTree::addRow(Parameter* p)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(this);
  item->setFlags(item->flags() | Qt::ItemIsEditable);

  item->setText(colNAME, p->getName());
  if (p->isRequired())
  {
    QFont font = item->font(colNAME);
    font.setBold(true);
    item->setFont(colNAME, font);
  }

  item->setText(colVALUE, p->getValue().toString());

  QVariant qv = qVariantFromValue((void*)p);
  item->setData(colNAME, Qt::UserRole, qv);

  item->setText(colDESCRIPTION, p->getDescription());

  return item;
}

// Delegate
