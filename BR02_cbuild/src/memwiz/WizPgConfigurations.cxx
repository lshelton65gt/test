//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "WizPgConfigurations.h"
#include "MemoryWizard.h"
#include "WizardTemplate.h"
#include "Template.h"
#include "Mode.h"
#include "Block.h"
#include "BlockInstance.h"
#include "GfxBlock.h"

WizPgConfigurations::WizPgConfigurations(QWidget *parent)
: QWizardPage(parent)
{
  ui.setupUi(this);
  ui.graphicsView->setScene(&mScene);

  if (!connect(&mSymbol, SIGNAL(blockSelected(GfxBlock*)), this, SLOT(blockSelected(GfxBlock*))))
    qDebug() << "not such slot";
}

void WizPgConfigurations::blockSelected(GfxBlock* gfxBlock)
{
  BlockInstance* blockInstance = gfxBlock->getBlockInstance();

  ui.labelParameters->setText(QString("Parameters for: %1").arg(gfxBlock->getName()));
  ui.treeWidgetBlockParams->populate(blockInstance->getParameters());
  ui.treeWidgetPortTree->populate(blockInstance->getPorts());
}

void WizPgConfigurations::initializePage()
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();
  WizardContext* context = templ->getContext();

  QString modeName = wiz->field("portMode").toString();

  Mode* mode = templ->getContext()->getTemplate()->findMode(modeName);
  if (mode)
  {
    // Draw the symbol
    mSymbol.draw(&mScene, context->getModel());
  }
  else
    qDebug() << "Internal Error: Mode should be defined";
}

WizPgConfigurations::~WizPgConfigurations()
{

}

int WizPgConfigurations::nextId() const
{
  return MemoryWizard::PageCodeGenerate;
}
