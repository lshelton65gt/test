//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "GfxBlock.h"
#include "Block.h"
#include "BlockInstance.h"
#include "Ports.h"
#include <QDebug>

#define PORT_HEIGHT 20

void GfxBlock::mousePressEvent(QGraphicsSceneMouseEvent* ev)
{
  QGraphicsItemGroup::mousePressEvent(ev);
}

void GfxBlock::clicked()
{
  emit blockSelected(this);
}

void GfxBlock::calculate(QGraphicsScene* scene)
{
  QGraphicsRectItem* blockRect = new QGraphicsRectItem();
  
  blockRect->setPen(QPen(Qt::black));
  blockRect->setBrush(QBrush(Qt::green));
  blockRect->setZValue(0);

  scene->addItem(blockRect);

  addToGroup(blockRect);

  int numRightPorts = 0;
  int numLeftPorts = 0;
  Ports* ports = mBlockInst->getPorts();
  for (int i=0; i<ports->getCount(); i++)
  {
    Port* port = ports->getAt(i);
    if (port->isEnabled())
    {
      if (port->getLocation() == Port::GraphicsLeft)
        numLeftPorts++;
      else if (port->getLocation() == Port::GraphicsRight)
        numRightPorts++;
      else
        qDebug() << "Error: unhandled Port Location";
    }
    else
      qDebug() << "  skipping port: " << port->getName();
  }

  int numUsedPorts = 0;
  if (numLeftPorts > numRightPorts)
    numUsedPorts = numLeftPorts;
  else
    numUsedPorts = numRightPorts;

  mHeight = 2 * PORT_HEIGHT + numUsedPorts * PORT_HEIGHT;

  blockRect->setRect(0, 0, 200, mHeight);

  GraphicsTextItem* titleText = new GraphicsTextItem(this);

  titleText->setTextInteractionFlags(Qt::TextBrowserInteraction);

  if (mBlockInst->getInstanceID().length() > 0)
    mName = QString("%1 Port: %2").arg(mBlockInst->getBlock()->getName()).arg(mBlockInst->getInstanceID());
  else
    mName = mBlockInst->getBlock()->getName();

  titleText->setHtml(
    QString("<a href=\"%1\">%2</a>").arg("url").arg(mName));

  titleText->setZValue(2);
  scene->addItem(titleText);

  int tx = mX+width()/2-(textWidth(titleText)/2);
  int ty = mY+10;

  titleText->setPos(tx, ty);

  setZValue(1);

  addPorts();
}

void GfxBlock::addPorts()
{
  int leftY = PORT_HEIGHT;
  int leftX = 0;
  int rightY = PORT_HEIGHT;

  Ports* ports = mBlockInst->getPorts();
  for (int i=0; i<ports->getCount(); i++)
  {
    Port* port = ports->getAt(i);
    if (port->isEnabled())
    {
      QGraphicsTextItem* ti = new QGraphicsTextItem();
      ti->setPlainText(port->getName());
      
      ti->setZValue(1);

      int bristleDelta = textHeight(ti) / 2;
      if (port->getLocation() == Port::GraphicsLeft)
      {
        QGraphicsLineItem* bristle = new QGraphicsLineItem();
        bristle->setPen(QPen(Qt::black));

        bristle->setLine(leftX, leftY+bristleDelta, leftX-10, leftY+bristleDelta);
        addToGroup(bristle);
        ti->setPos(1+leftX, leftY);
        leftY += PORT_HEIGHT;
      }
      else if (port->getLocation() == Port::GraphicsRight)
      {
        int tw = textWidth(ti);
        int x = width() - tw;
        
        if (x < 0)
          x = 0;

        ti->setPos(x, rightY);

        QGraphicsLineItem* bristle = new QGraphicsLineItem();
        bristle->setPen(QPen(Qt::black));

        bristle->setLine(width(), rightY+bristleDelta, width()+10, rightY+bristleDelta);
        addToGroup(bristle);

        rightY += PORT_HEIGHT;
      }
      addToGroup(ti);
    }
  }
}

int GfxBlock::textWidth(QGraphicsTextItem* txt)
{
  return (int)(txt->boundingRect().width());
}

int GfxBlock::textHeight(QGraphicsTextItem* txt)
{
  return (int)(txt->boundingRect().height());
}
