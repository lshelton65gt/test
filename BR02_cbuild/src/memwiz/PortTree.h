#ifndef PORTTREE_H
#define PORTTREE_H

#include <QtGui>

class Ports;
class Port;

class PortTree : public QTreeWidget
{
  Q_OBJECT

public:
  enum Columns { colNAME, colDESCRIPTION, colLAST } ;

  PortTree(QWidget *parent);
  ~PortTree();

  void populate(Ports* ports);

private:
  void addRow(Port* port);

};

#endif // PORTTREE_H
