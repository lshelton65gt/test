//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "PortConfigurations.h"
#include "PortConfiguration.h"


QObject* PortConfigurations::getPortConfiguration(int index)
{
  return mPortConfigurations[index];
}

bool PortConfigurations::deserialize(Template* templ, const QDomElement& e, XmlErrorHandler* eh)
{
  if (!PortConfiguration::deserialize(templ, this, e, eh))
    return false;
  else
    return true;
}


void PortConfigurations::addPortConfiguration(PortConfiguration* config)
{
  mPortConfigurations.append(config);
}
