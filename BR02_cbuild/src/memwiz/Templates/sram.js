

// The Main entry point for code generation
function generateCode(context, response)
{
  response.write("module <moduleName>(");
  
  //generatePortList(context, response);
  
  response.writeLine(");");
  
  response.pushIndent(2);
  
  generateBody(context, response);
  
  response.popIndent();
  
  response.writeLine("endmodule");
}

// Write out the PortDeclarations
function generatePortDeclarations(context, response)
{
  for (var i=0; i<context.model.count; i++)
  {
    var blockInst = context.model.getInstance(i);
   
    if (blockInst.block.name == "Write")
      generateWriteDeclarations(context, response, blockInst);
    else if (blockInst.block.name == "Read")
      generateReadDeclarations(context, response, blockInst);
    else if (blockInst.block.name == "Control")
      generateControlDeclarations(context, response, blockInst);
    else
      throw("Unknown block instance: " + blockInst.block.name);    
  }
}

function generateWriteDeclarations(context, response, blockInst)
{
  var id = blockInst.instanceID;
  
  response.writeLine("");
  response.writeLine(sprintf("// Write Port %s", id));
  response.writeLine(sprintf("input %s;", expandPort(blockInst.ports.WriteClock, "instanceID", id)));
  response.writeLine(sprintf("input [ADDR_WIDTH-1:0] %s;", expandPort(blockInst.ports.WriteAddress, "instanceID", id)));
  response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", expandPort(blockInst.ports.DataIn, "instanceID", id)));
}

function generateReadDeclarations(context, response, blockInst)
{
  var id = blockInst.instanceID;
  var ports = blockInst.ports;

  response.writeLine(sprintf("// Read Port %s", id));

  if (ports.ReadAddress.enabled)
  {
    response.writeLine(sprintf("input [ADDR_WIDTH-1:0] %s;", expandPort(ports.ReadAddress, "instanceID", id)));
  }
     
  response.writeLine("");
 
//  response.writeLine(sprintf("input %s;", expandPort(blockInst.ports.WriteClock, "instanceID", id)));
//  response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", expandPort(blockInst.ports.DataIn, "instanceID", id)));

}

function generateControlDeclarations(context, response, blockInst)
{
}

function generateBody(context, response)
{
  var parameters = context.template.parameters;

  // write out the parameters
  generateParameters(context, response);

  // port declarations
  generatePortDeclarations(context, response);
  
  
  // write out the ports
  
  
//  var pNumPorts = parameters.Ports;

//  //expandPort(ports.Bad, "portNumber", 1);
//    
//  response.writeLine("// ports, storage");
//  for (var i=0; i<pNumPorts.intValue; i++)
//  {   
//    response.writeLine(sprintf("// port %d", i));
//    response.writeLine(sprintf("  input %s;", expandPort(ports.WriteClock, "writePortNumber", i)));
//    response.writeLine(sprintf("  input [ADDR_WIDTH-1:0] %s;", expandPort(ports.Address, "portNumber", i)));
//    response.writeLine(sprintf("  input [DATA_WIDTH-1:0] %s;", expandPort(ports.DataIn, "portNumber", i)));
//    response.writeLine(sprintf("  output [DATA_WIDTH-1:0] %s;", expandPort(ports.DataOut, "portNumber", i)));
//    response.writeLine(sprintf("  reg [DATA_WIDTH-1:0] %s;", expandPort(ports.DataOut, "portNumber", i)));
//    response.writeLine(sprintf("  input %s;", expandPort(ports.WriteEnable, "portNumber", i)));
//    if (parameters.Bitmasked)
//        response.writeLine(sprintf("  input [DATA_WIDTH-1:0] %s;", expandPort(ports.ByteEnable, "portNumber", i)));
//    response.writeLine("");
//  }
//  
//  if (parameters.Bitmasked)
//    response.writeLine(sprintf("  reg [DATA_WIDTH-1:0] tmp;"));
//  
//  response.writeLine("");

//  // write enable
//  response.writeLine("  reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];");
//  
//  generateImplementation(context, response);
}

function generateImplementation(context, response)
{
  var parameters = context.template.parameters;
  var ports = context.template.ports;  
  var pNumPorts = parameters.Ports;
  
  response.writeLine("// Implementation");
  for (var i=0; i<pNumPorts.intValue; i++)
  {
    response.writeLine(sprintf("// port %d", i));
    
    response.writeLine(sprintf("  always @(posedge %s)", expandPort(ports.Clock, "portNumber", i)));
    
    response.writeLine(sprintf("    if (%s)", expandPort(ports.WriteEnable, "portNumber", i)));
    response.writeLine(sprintf("    begin"));
    if (parameters.Bitmasked)
    {
	response.writeLine(sprintf("      tmp = mem[%s];", expandPort(ports.Address, "portNumber", i)));
	
	response.writeLine(sprintf("      mem[%s] <= (%s & %s) | (tmp & ~(%s));",      
	    expandPort(ports.Address, "portNumber", i),
	    expandPort(ports.DataIn, "portNumber", i),
	    expandPort(ports.ByteEnable, "portNumber", i),
	    expandPort(ports.ByteEnable, "portNumber", i)));          
    }
    else
	response.writeLine(sprintf("      mem[%s] <= %s;", 
	    expandPort(ports.Address, "portNumber", i), 
	    expandPort(ports.DataIn, "portNumber", i)));
    
    response.writeLine(sprintf("    end"));
    response.writeLine(sprintf("    else"));
    response.writeLine(sprintf("    begin"));
    response.writeLine(sprintf("      %s <= mem[%s];",
		    expandPort(ports.DataOut, "portNumber", i),
		    expandPort(ports.Address, "portNumber", i)));
    response.writeLine(sprintf("    end"));
    response.writeLine("");
  }
}

function generateParameters(context, response)
{
  var parameters = context.template.parameters;

  var pNumPorts = parameters.Ports;
  var pAddrWidth = parameters.AddressWidth;
  var pDataWidth = parameters.DataWidth;
  var pWords = parameters.Words;
  var pReadType = parameters.ReadType;
  
  response.writeLine(formatParameter("ADDR_WIDTH", pAddrWidth));
  response.writeLine(formatParameter("DATA_WIDTH", pDataWidth));
  response.writeLine(formatParameter("DEPTH", pWords));
  
  return true;
}

function formatParameter(name, p)
{
  return "parameter " + name + " = " + p.value + ";";
}

function findInstance(model, name, id)
{
  for (var i=0; i<context.model.count; i++)
  {
    var blockInst = model.getInstance(i);
    if (blockInst.block.name == name && blockInst.instanceID == id)
      return blockInst;
  }
  
  return null;
}