var ff = new Object();
ff.name = 'FF';
ff.ports = ['do','di','web','ra','wa','rclkb','wclkb','reb'];

var fr = new Object();
fr.name = 'FR';
fr.ports = ['do','di','web','ra','wa','rclkb','wclk','reb'];

var rrrr = new Object();
rrrr.name = 'RRRR';
rrrr.ports = ['doa','dob','dia','dib','raa','rab','waa','wab','rclk','wclk','reb','weba','webb'];

var interfaces = new Array(ff,fr,rrrr);
  
function definePorts(context)
{  
  var mode = context.model.mode;
  var parameters = context.template.parameters;

  debug('Defining ports for: ' + parameters.ModuleName.value);  
  
  
  var module = parameters.ModuleName;
  
  
  var modName = module.value;
  
  
  var field1Value = module.captures.Field1;
  
  
  
  var nameRegExp = /(\w+)?(B\w*RF)(\D+)?(\d+)X(\d+)(AF|AR|AAR|RR|FF|R|F|RF|FR|RRRR|RRRRRR)(\d)(\w+)?(N)?/;
 
  if (!nameRegExp.test(modName))
    throw "Invalid module name expression";
    
  var results = nameRegExp.exec(modName);
  
  var projPrefix = results[1];
  var regFile = results[2];
  var addlInfo1 = results[3];
  var numWords = results[4];
  var numBits = results[5];
  var kind = results[6];
  var colMux = results[7];
  var addlInfo2 = results[8];
  var noWriteThru = results[9];

  var foundIt = false;
  for (var i=0; i<interfaces.length; i++)
  {
    var intFace = interfaces[i];
    var name = intFace.name;
    
    if (name == kind)
    {
      var memBlock = mode.blocks.addBlock(name);
      var ports = intFace.ports;
      for (var pi=0; pi<ports.length; pi++)
        memBlock.ports.addPort(ports[pi], Ports.GraphicsLeft, "$(" + ports[pi] + ")");
      foundIt = true;
      break;
    }
  }
  
  if (!foundIt)
    throw("No interface for: " + kind);
  
  
  return true;
}


// The Main entry point for code generation
function generateCode(context, response)
{
 
  debug("name: " + context.model.blockInstances.getBlockInstance(0).name);

  writeModuleDeclaration(context, response);
    
  generateBody(context, response);   
   
  response.writeLine("endmodule");
}

function writeModuleDeclaration(context, response)
{
  var parameters = context.template.parameters;
  response.write(sprintf("module %s(", parameters.ModuleName.value));  
  generatePortList(context, response);
  response.writeLine(");");
}
// Write out the Module Port List of all the Unique Ports used
function generatePortList(context, response)
{
  var uniquePorts = context.model.uniquePortInstances;
  var sendComma = false;
  
  for (var i=0; i<uniquePorts.count; i++)
  {
    var portName = uniquePorts.getPortInstance(i).name;
    if (sendComma)
      response.write(",");
    response.write(portName);
    sendComma = true;
  }
}  

// Write out the PortDeclarations
function generatePortDeclarations(context, response)
{
  var writeBlocks = context.model.getBlockInstances("Write");
  for (var i=0; i<writeBlocks.count; i++)
  {
    var writeBlockInst = writeBlocks.getBlockInstance(i);
    generateWriteDeclarations(context, response, writeBlockInst);    
  }
  
  var readBlocks = context.model.getBlockInstances("Read");
  for (var i=0; i<readBlocks.count; i++)
  {
    var readBlockInst = readBlocks.getBlockInstance(i);
    generateReadDeclarations(context, response, readBlockInst);    
  }

  for (var i=0; i<context.model.blockInstances.count; i++)
  {
    var blockInst = context.model.blockInstances.getBlockInstance(i);
    if (blockInst.block.name == "Control")
      generateControlDeclarations(context, response, blockInst);
  }
}

function generateWriteDeclarations(context, response, blockInst)
{
  var parameters = context.model.mode.parameters;
  var id = blockInst.instanceID;
  var model = context.model;
  var writePorts = model.getPortInstances("Write", id);
    
  response.writeLine("");
  response.writeLine(sprintf("// Write Port %s", id)); 
  
  // Input port
  response.writeLine(sprintf("input %s;", writePorts.WriteEnable.name));
  
  // Are we bitmasking?
  if (parameters.BitMasking.boolValue)
    response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", writePorts.BitEnable.name));

  // Clock
  response.writeLine(sprintf("input %s;", writePorts.Clock.name));
  
  // Address
  response.writeLine(sprintf("input [ADDR_WIDTH-1:0] %s;", writePorts.Address.name));
  
  // DataIn
  response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", writePorts.DataIn.name));
}

function generateReadDeclarations(context, response, blockInst)
{
  var id = blockInst.instanceID;
  var parameters = context.model.mode.parameters; 
  var model = context.model;
  var readPorts = model.getPortInstances("Read", id);

  response.writeLine("");
  response.writeLine(sprintf("// Read Port %s", id));

  // Declare the output port
  response.writeLine(sprintf("output [DATA_WIDTH-1:0] %s;", readPorts.DataOut.name));
  
  // Registered outputs?
  if (parameters.Registered.boolValue)
    response.writeLine(sprintf("reg [DATA_WIDTH-1:0] %s;", readPorts.DataOut.name));  
}

function generateControlDeclarations(context, response, blockInst)
{
  response.writeLine("");
  response.writeLine("// The memory");
  response.writeLine("reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];");
}

function generateBody(context, response)
{
  response.pushIndent(2);

  var parameters = context.template.parameters;

  // write out the parameters
  generateParameters(context, response);

  // port declarations
  generatePortDeclarations(context, response);
  
  // write out the implementation
  generateImplementation(context, response);
  
  response.popIndent();
}

function generateImplementation(context, response)
{
  var parameters = context.template.parameters;
  var numberOfPorts = parameters.Ports.intValue;
  var model = context.model;
  
  for (var i=0; i<numberOfPorts; i++)
  {
    var instanceID = i;
    
    // Get the Write/Read Block Instances for this Instance
    var writeBlockInst = model.getBlockInstance("Write", instanceID);
    var readBlockInst = model.getBlockInstance("Read", instanceID);
    
    // Write/Read Ports of these instances
    var writePorts = writeBlockInst.portInstances;
    var readPorts = readBlockInst.portInstances;
    
    var edge = parameters.ClockEdge.value;
    
    response.writeLine("");
    response.writeLine(sprintf("// Implementation port %d", i));
    response.writeLine(sprintf("always @(%s %s)", edge, writePorts.Clock.name));
    response.pushIndent(2);
    response.writeLine(sprintf("if (%s)", writePorts.WriteEnable.name));
    response.pushIndent(2);
    
    if (model.mode.parameters.BitMasking.boolValue)
         response.writeLine(sprintf("mem[%s] <= (%s & %s) | (mem[%s] & ~%s);", 
                      writePorts.Address.name,
                      writePorts.DataIn.name,
                      writePorts.BitEnable.name,
                      writePorts.Address.name,
                      writePorts.BitEnable.name
                      ));   
    else
      response.writeLine(sprintf("mem[%s] <= %s;", writePorts.Address.name, writePorts.DataIn.name));
                      
    response.popIndent();
    
    if (context.model.mode.parameters.Registered.boolValue)
    {
      response.writeLine("else");
      response.pushIndent(2);
      response.writeLine(sprintf("%s <= mem[%s];", readPorts.DataOut.name, writePorts.Address.name));
      response.popIndent();
      response.popIndent();              
     }
     else
     {
      response.popIndent();
      response.writeLine("");
      response.writeLine(sprintf("assign %s = mem[%s];", readPorts.DataOut.name, writePorts.Address.name));
     }

  }
}

function generateParameters(context, response)
{
  var parameters = context.template.parameters;
  
  response.writeLine(formatParameter("ADDR_WIDTH", parameters.AddressWidth));
  response.writeLine(formatParameter("DATA_WIDTH", parameters.DataWidth));
  response.writeLine(formatParameter("DEPTH", parameters.Words));
  
  return true;
}

function formatParameter(name, p)
{
  return "parameter " + name + " = " + p.value + ";";
}
