

// The Main entry point for code generation
function generateCode(context, response)
{
  writeModuleDeclaration(context, response);
    
  generateBody(context, response);
   
  response.writeLine("endmodule");
  
  if (context.template.parameters.GenTest.value == "yes")
    generateTestbench (context, response);
}

function writeModuleDeclaration(context, response)
{
  var parameters = context.template.parameters;
  response.write(sprintf("module %s(", parameters.ModuleName.value));  
  generatePortList(context, response);
  response.writeLine(");");
}
// Write out the Module Port List of all the Unique Ports used
function generatePortList(context, response)
{
  var uniquePorts = context.model.uniquePortInstances;
  var sendComma = false;
  
  for (var i=0; i<uniquePorts.count; i++)
  {
    var portName = uniquePorts.getPortInstance(i).name;
    if (sendComma)
      response.write(",");
    response.write(portName);
    sendComma = true;
  }
}  

// Write out the PortDeclarations
function generatePortDeclarations(context, response)
{
  var writeBlocks = context.model.getBlockInstances("Write");
    for (var i=0; i<writeBlocks.count; i++)
  {
    var writeBlockInst = writeBlocks.getBlockInstance(i);
    generateWriteDeclarations(context, response, writeBlockInst);    
  }
  
  var readBlocks = context.model.getBlockInstances("Read");
  for (var i=0; i<readBlocks.count; i++)
  {
    var readBlockInst = readBlocks.getBlockInstance(i);
    generateReadDeclarations(context, response, readBlockInst);    
  }

  for (var i=0; i<context.model.blockInstances.count; i++)
  {
    var blockInst = context.model.blockInstances.getBlockInstance(i);
    if (blockInst.block.name == "Control")
      generateControlDeclarations(context, response, blockInst);
  }
}

function generateWriteDeclarations(context, response, blockInst)
{
  var parameters = context.model.mode.parameters;
  var id = blockInst.instanceID;
  var model = context.model;
  var writePorts = model.getPortInstances("Write", id);
    
  response.writeLine("");
  response.writeLine(sprintf("// Write Port %s", id)); 
  
  // Input port
  response.writeLine(sprintf("input %s;", writePorts.WriteEnable.name));
  
  // Are we bitmasking?
  if (parameters.Masking.value == "Bit")
    response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", writePorts.BitEnable.name));
  else if (parameters.Masking.value == "Byte")
    response.writeLine(sprintf("input [(DATA_WIDTH+7)/8-1:0] %s;", writePorts.ByteEnable.name));

  // Clock
  //response.writeLine(sprintf("input %s;", writePorts.Clock.name));
  
  // Address
  response.writeLine(sprintf("input [ADDR_WIDTH-1:0] %s;", writePorts.Address.name));
  
  // DataIn
  response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", writePorts.DataIn.name));
}

function generateReadDeclarations(context, response, blockInst)
{
  var id = blockInst.instanceID;
  var parameters = context.model.mode.parameters; 
  var model = context.model;
  var readPorts = model.getPortInstances("Read", id);

  response.writeLine("");
  response.writeLine(sprintf("// Read Port %s", id));

  response.writeLine(sprintf("output [DATA_WIDTH-1:0] %s;", readPorts.DataOut.name));
  response.writeLine(sprintf("input  [ADDR_WIDTH-1:0] %s;", readPorts.Address.name));
  response.writeLine(sprintf("reg    [DATA_WIDTH-1:0] %s;", readPorts.DataOut.name));  
}

function generateControlDeclarations(context, response, blockInst)
{
  var model = context.model;
  var controlPorts = model.getPortInstances("Control", "");

  response.writeLine(sprintf("input                   %s;", controlPorts.Clock.name));

  response.writeLine("");
  response.writeLine("// The memory");
  response.writeLine("reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];");
}

function generateBody(context, response)
{
  response.pushIndent(2);

  var parameters = context.template.parameters;

  // write out the parameters
  generateParameters(context, response);

  // port declarations
  generatePortDeclarations(context, response);
  
  // write out the implementation
  generateImplementation(context, response);
  
  response.popIndent();
}

function generateWriteImplementation(context, response)
{
  var parameters = context.template.parameters;
  var numWritePorts = parameters.WritePorts.intValue;  
  var model = context.model;
  var edge = parameters.ClockEdge.value;

  // Write Ports
  for (var i=0; i<numWritePorts; i++)
  {
    var instanceID = i;
    var writeBlockInst = model.getBlockInstance("Write", instanceID);
    var writePorts = writeBlockInst.portInstances;
    
    response.writeLine("");
    response.writeLine(sprintf("// Write port %d", i));
    response.writeLine(sprintf("if (%s)", writePorts.WriteEnable.name));
    response.pushIndent(2);
    
    if (model.mode.parameters.Masking.value == "Bit")
         response.writeLine(sprintf("mem[%s] <= (%s & %s) | (mem[%s] & ~%s);", 
                      writePorts.Address.name,
                      writePorts.DataIn.name,
                      writePorts.BitEnable.name,
                      writePorts.Address.name,
                      writePorts.BitEnable.name
                      ));   
    else
      response.writeLine(sprintf("mem[%s] = %s;", writePorts.Address.name, writePorts.DataIn.name));

    response.popIndent();                      
  }
}

function generateReadImplementation(context, response)
{
  var parameters = context.template.parameters;
  var numReadPorts = parameters.ReadPorts.intValue;
  var model = context.model;

  response.writeLine("");
  response.writeLine("// Read ports");

  // Read Ports
  for (var i=0; i<numReadPorts; i++)
  {
    var instanceID = i;
    var readBlockInst = model.getBlockInstance("Read", instanceID);
    var readPorts = readBlockInst.portInstances;

    response.writeLine(sprintf("%s <= mem[%s];", readPorts.DataOut.name, readPorts.Address.name));
  }
}

function generateImplementation(context, response)
{
  var parameters = context.template.parameters;
  var ordering = parameters.OperatingMode.value;
  var model = context.model;
  var controlBlockInst = model.getBlockInstance("Control","");
  var controlPorts = controlBlockInst.portInstances;
  var edge = parameters.ClockEdge.value;
  
    response.writeLine(sprintf("always @(%s %s) begin", edge, controlPorts.Clock.name));
    response.pushIndent(2);


  if (ordering == "ReadFirst")
  {
    generateReadImplementation(context, response);
    generateWriteImplementation(context, response);
  }
  else
  {
    generateWriteImplementation(context, response);
    generateReadImplementation(context, response);
  }
  response.popIndent();
  response.writeLine("end");
}

function generateParameters(context, response)
{
  var parameters = context.template.parameters;
  
  response.writeLine(formatParameter("ADDR_WIDTH", parameters.AddressWidth));
  response.writeLine(formatParameter("DATA_WIDTH", parameters.DataWidth));
  response.writeLine(formatParameter("DEPTH", parameters.Words));
  
  return true;
}

function formatParameter(name, p)
{
  return "parameter " + name + " = " + p.value + ";";
}


/*****************************************************************************************/

function generateTestbench(context, response)
{
  var parameters = context.template.parameters;
  response.writeLine("");
  response.writeLine("");
  response.writeLine(sprintf("module %s_tb();", parameters.ModuleName.value));  
  response.pushIndent(2);
    
  debug("generateTestDecls");
  generateTestDecls(context, response);
  debug("generateTestBody");
  generateTestBody(context, response);
  debug("generateTestTasks");
  generateTestTasks(context, response);
  debug("generateTestInstance");
  generateTestInstance(context, response);

  response.popIndent(2);
  response.writeLine("endmodule");
}

function generateTestDecls(context, response)
{
  var parameters = context.template.parameters;
  
  response.writeLine(formatParameter("ADDR_WIDTH", parameters.AddressWidth));
  response.writeLine(formatParameter("DATA_WIDTH", parameters.DataWidth));
  response.writeLine(formatParameter("DEPTH", parameters.Words));
  response.writeLine(formatParameter("CYCLE", parameters.ClockUnits));
  response.writeLine(formatParameter("NCYCLES", parameters.NumCycles));

  var writeBlocks = context.model.getBlockInstances("Write");
  for (var i=0; i<writeBlocks.count; i++)
  {
    var writeBlockInst = writeBlocks.getBlockInstance(i);
    generateTestWriteDeclarations(context, response, writeBlockInst);    
  }
  
  var readBlocks = context.model.getBlockInstances("Read");
  for (var i=0; i<readBlocks.count; i++)
  {
    var readBlockInst = readBlocks.getBlockInstance(i);
    generateTestReadDeclarations(context, response, readBlockInst);    
  }

  for (var i=0; i<context.model.blockInstances.count; i++)
  {
    var blockInst = context.model.blockInstances.getBlockInstance(i);
    if (blockInst.block.name == "Control")
      generateTestControlDeclarations(context, response, blockInst);
  }
}

function generateTestWriteDeclarations(context, response, blockInst)
{
  var parameters = context.model.mode.parameters;
  var id = blockInst.instanceID;
  var model = context.model;
  var writePorts = model.getPortInstances("Write", id);
  var readPorts = model.getPortInstances("Read", id);
    
  response.writeLine("");
  response.writeLine(sprintf("// Write Port %s", id)); 
  
  // Input port
  response.writeLine(sprintf("reg %s;", writePorts.WriteEnable.name));
  
  // Are we bitmasking?
  if (parameters.Masking.value == "Bit")
    response.writeLine(sprintf("reg [DATA_WIDTH-1:0] %s;", writePorts.BitEnable.name));
  else if (parameters.Masking.value == "Byte")
    response.writeLine(sprintf("reg [(DATA_WIDTH+7)/8-1:0] %s;", writePorts.ByteEnable.name));

  // Clock
  //response.writeLine(sprintf("input %s;", writePorts.Clock.name));
  
  // Address
  response.writeLine(sprintf("reg [ADDR_WIDTH-1:0] %s;", writePorts.Address.name));
  
  // DataIn
  response.writeLine(sprintf("reg [DATA_WIDTH-1:0] %s;", writePorts.DataIn.name));
}

function generateTestReadDeclarations(context, response, blockInst)
{
  var id = blockInst.instanceID;
  var parameters = context.model.mode.parameters; 
  var model = context.model;
  var readPorts = model.getPortInstances("Read", id);

  response.writeLine("");
  response.writeLine(sprintf("// Read Port %s", id));

  response.writeLine(sprintf("wire [DATA_WIDTH-1:0] %s;", readPorts.DataOut.name));
  response.writeLine(sprintf("reg  [ADDR_WIDTH-1:0] %s;", readPorts.Address.name));
  }

function generateTestControlDeclarations(context, response, blockInst)
{
  var model = context.model;
  var controlPorts = model.getPortInstances("Control", "");

  response.writeLine("");
  response.writeLine(sprintf("reg                   %s;", controlPorts.Clock.name));
  response.writeLine("reg                   clk_base;");
  response.writeLine("event                 sample_data;");
  response.writeLine("event                 drive_data;");
  response.writeLine("integer               error_count;");
}

function generateTestTasks(context, response)
{
// Generate clock and even generation
  var model = context.model;
  var controlBlockInst = model.getBlockInstance("Control","");
  var controlPorts = controlBlockInst.portInstances;
  var parameters = context.template.parameters;
  var edge = parameters.ClockEdge.value;
  var not = "";
  
  response.writeLine("");
  response.writeLine("always @(posedge clk_base)");
  response.pushIndent(2);
  if (edge == "negedge") {
    not = "~";
    response.writeLine ("// Memory is configured to trigger on negedge");
  }
  response.writeLine(sprintf("%s = %sclk_base;", controlPorts.Clock.name, not));
  response.popIndent();

  response.writeLine("");
  response.writeLine("// Data is driven just before the clock, and sampled just after"); 
  response.writeLine("initial begin");
  response.pushIndent(2);
  response.writeLine("repeat (NCYCLES) begin");
  response.pushIndent(2);
  response.writeLine("clk_base = 1'b0;");
  response.writeLine("#(CYCLE*3/8);");
  response.writeLine("->drive_data;");
  response.writeLine("#(CYCLE/8);");
  response.writeLine("clk_base = 1'b1;");
  response.writeLine("#(CYCLE/8);");
  response.writeLine("->sample_data;");
  response.writeLine("#(CYCLE*3/8);");
  response.popIndent();
  response.writeLine("end");
  response.popIndent();
  response.writeLine("end");

// Generate "read" and "expect" tasks
  var readBlocks = context.model.getBlockInstances("Read");
  for (var i=0; i<readBlocks.count; i++)
  {
    var readBlockInst = readBlocks.getBlockInstance(i);
    var model = context.model;
    var readPorts = model.getPortInstances("Read", i);
    response.writeLine("");
    response.writeLine(sprintf("// Do a read on read port %d", i) );
    response.writeLine(sprintf("task read%d;", i));
    response.pushIndent(2);
    response.writeLine("input [ADDR_WIDTH-1:0] address;");
    response.writeLine("");
    response.writeLine("begin");
    response.pushIndent(2);
    response.writeLine(sprintf("%s = address;", readPorts.Address.name));
    response.popIndent();
    response.writeLine("end");
    response.popIndent();
    response.writeLine("endtask");

    response.writeLine("");
    response.writeLine(sprintf("// Test the data on read port %d", i) );
    response.writeLine(sprintf("task expect%d;", i));
    response.pushIndent(2);
    response.writeLine("input [DATA_WIDTH-1:0] data;");
    response.writeLine("");
    response.writeLine("begin");
    response.pushIndent(2);
    response.writeLine(sprintf("if (data != %s) begin", readPorts.DataOut.name));
    response.pushIndent(2);
    response.writeLine(sprintf("$display (\"Mismatch on read port %d. Expected %%x, got %%x\", data, %s);",
            i, readPorts.DataOut.name));
    response.writeLine("error_count = error_count + 1;");
    response.popIndent();
    response.writeLine("end");
    response.popIndent();
    response.writeLine("end");
    response.popIndent();
    response.writeLine("endtask");
  }
  
  // "Write" tasks
  var writeBlocks = context.model.getBlockInstances("Write");
  for (var i=0; i<writeBlocks.count; i++)
  {
    var writeBlockInst = writeBlocks.getBlockInstance(i);
    var writePorts = model.getPortInstances("Write", i);
    response.writeLine("");
    response.writeLine(sprintf("// Do a write to write port %d", i) );
    response.writeLine(sprintf("task write%d;", i));
    response.pushIndent(2);
    response.writeLine("input [ADDR_WIDTH-1:0] address;");
    response.writeLine("input [DATA_WIDTH-1:0] data;");
    response.writeLine("");
    response.writeLine("begin");
    response.pushIndent(2);
    response.writeLine(sprintf("%s = address;", writePorts.Address.name));
    response.writeLine(sprintf("%s = data;", writePorts.DataIn.name));
    response.popIndent();
    response.writeLine("end");
    response.popIndent();
    response.writeLine("endtask");
  }

  // "Initialize" task  
  response.writeLine("");
  response.writeLine("// Set up infrustructure");
  response.writeLine("task initialize;");
  response.pushIndent(2);
  response.writeLine("begin");
  response.pushIndent(2);
  response.writeLine("$dumpvars;");
  response.writeLine("error_count = 0;");
  response.popIndent();
  response.writeLine("end");
  response.popIndent();
  response.writeLine("endtask");

  // "Finalize" task  
  response.writeLine("");
  response.writeLine("// Display pass/fail");
  response.writeLine("task finalize;");
  response.pushIndent(2);
  response.writeLine("begin");
  response.pushIndent(2);
  response.writeLine("if (error_count == 0) begin");
  response.pushIndent(2);
  response.writeLine("$display (\"Test passed!\");");
  response.popIndent();
  response.writeLine("end");
  response.writeLine("else begin");
  response.pushIndent(2);
  response.writeLine("$display (\"Found %0d mismatches\", error_count);");
  response.popIndent();
  response.writeLine("end");
  response.popIndent();
  response.writeLine("end");
  response.popIndent();
  response.writeLine("endtask");

  // "drive" task  
  response.writeLine("");
  response.writeLine("// Wait until 1/8 before the begining of the next clock cycle.");
  response.writeLine("//    This is when data should be driven to the model");
  response.writeLine("task drive;");
  response.pushIndent(2);
  response.writeLine("begin");
  response.pushIndent(2);
  response.writeLine("@(drive_data);");
  response.popIndent();
  response.writeLine("end");
  response.popIndent();
  response.writeLine("endtask");

  // "sample" task  
  response.writeLine("");
  response.writeLine("// Wait until 1/8 into the clock cycle.  This is when data should be sampled");
  response.writeLine("task sample;");
  response.pushIndent(2);
  response.writeLine("begin");
  response.pushIndent(2);
  response.writeLine("@(sample_data);");
  response.popIndent();
  response.writeLine("end");
  response.popIndent();
  response.writeLine("endtask");

}

function generateTestInstance(context, response)
{
  var model = context.model;
//  var parameters = context.model.mode.parameters;
  var parameters = context.template.parameters;

  response.writeLine("");
  response.writeLine("// Memory model instance");
  response.writeLine(sprintf("%s #(", parameters.ModuleName.value));
  response.pushIndent(2);
  response.writeLine(".ADDR_WIDTH (ADDR_WIDTH),");  
  response.writeLine(".DATA_WIDTH (DATA_WIDTH),");  
  response.writeLine(".DEPTH (DEPTH))");  
  response.popIndent();
  response.writeLine("mem (");  
  response.pushIndent(2);


  var writeBlocks = context.model.getBlockInstances("Write");
  for (var i=0; i<writeBlocks.count; i++)
  {
    var writeBlockInst = writeBlocks.getBlockInstance(i);
    var writePorts = model.getPortInstances("Write", i);
    response.writeLine(sprintf(".%s (%s[ADDR_WIDTH-1:0]),", writePorts.Address.name, writePorts.Address.name));
    response.writeLine(sprintf(".%s (%s[DATA_WIDTH-1:0]),", writePorts.DataIn.name, writePorts.DataIn.name));
    response.writeLine(sprintf(".%s (%s),", writePorts.WriteEnable.name, writePorts.WriteEnable.name));

    if (context.model.mode.parameters.Masking.value == "Bit")
      response.writeLine(sprintf(".%s (%s [DATA_WIDTH-1:0]),", writePorts.BitEnable.name, writePorts.BitEnable.name));
    else if (context.model.mode.parameters.Masking.value == "Byte")
      response.writeLine(sprintf(".%s (%s [DATA_WIDTH+7)/8-1:0]),", writePorts.ByteEnable.name, writePorts.ByteEnable.name));
  }
  
  var readBlocks = context.model.getBlockInstances("Read");
  for (var i=0; i<readBlocks.count; i++)
  {
    var readBlockInst = readBlocks.getBlockInstance(i);
    var readPorts = model.getPortInstances("Read", i);
    response.writeLine(sprintf(".%s (%s[ADDR_WIDTH-1:0]),", readPorts.Address.name, readPorts.Address.name));
    response.writeLine(sprintf(".%s (%s[DATA_WIDTH-1:0]),", readPorts.DataOut.name, readPorts.DataOut.name));
  }

  for (var i=0; i<context.model.blockInstances.count; i++)
  {
    var blockInst = context.model.blockInstances.getBlockInstance(i);
    if (blockInst.block.name == "Control")
    {
      var controlPorts = model.getPortInstances("Control", "");

      response.writeLine(sprintf(".%s (%s));", controlPorts.Clock.name, controlPorts.Clock.name));
    }
  }
  response.popIndent();
}

function generateTestBody(context, response)
{
  response.writeLine("");
  response.writeLine("initial begin");
  response.pushIndent(2);
  response.writeLine("initialize;");
  response.writeLine("");
  response.writeLine("drive;");
  response.writeLine("write0 (5'd3, 16'h1234);");
  response.writeLine("write1 (5'd4, 16'h2345);");
  response.writeLine("");
  response.writeLine("drive;");
  response.writeLine("write0 (5'd5, 16'h3456);");
  response.writeLine("write1 (5'd6, 16'h4567);");
  response.writeLine("");
  response.writeLine("drive;");
  response.writeLine("read0 (5'd3);");
  response.writeLine("read1 (5'd4);");
  response.writeLine("");
  response.writeLine("sample;");
  response.writeLine("expect0 (16'h1234);");
  response.writeLine("expect1 (16'h2345);");
  response.writeLine("drive;");
  response.writeLine("write0 (5'd5, 16'h3456);");
  response.writeLine("write1 (5'd6, 16'h4567);");
  response.writeLine("");
  response.writeLine("finalize;");
  response.popIndent();
  response.writeLine("end");
}
