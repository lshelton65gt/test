// This module implements the QsciLexerVerilog class.
//
// Copyright (c) 2007
// 	Riverbank Computing Limited <info@riverbankcomputing.co.uk>
// 
// This file is part of QScintilla.
// 
// This copy of QScintilla is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option) any
// later version.
// 
// QScintilla is supplied in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General Public License along with
// QScintilla; see the file LICENSE.  If not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#include "qscilexerverilog.h"

#include <qcolor.h>
#include <qfont.h>
#include <qsettings.h>


// The ctor.
QsciLexerVerilog::QsciLexerVerilog(QObject *parent, bool caseInsensitiveKeywords)
: QsciLexer(parent),
fold_atelse(false), fold_comments(false), fold_compact(true),
fold_preproc(true), style_preproc(false), nocase(caseInsensitiveKeywords)
{
  QFont f;
#if defined(Q_OS_WIN)
  f = QFont("Courier",10);
#else
  f = QFont("Bitstream Vera Sans Mono",9);
#endif
  setDefaultFont(f);
}


// The dtor.
QsciLexerVerilog::~QsciLexerVerilog()
{
}


// Returns the language name.
const char *QsciLexerVerilog::language() const
{
  return "verilog";
}


// Returns the lexer name.
const char *QsciLexerVerilog::lexer() const
{
  return ("verilog");
}


// Return the set of character sequences that can separate auto-completion
// words.
QStringList QsciLexerVerilog::autoCompletionWordSeparators() const
{
  QStringList wl;

  wl << "::" << "->" << ".";

  return wl;
}


// Return the list of keywords that can start a block.
const char *QsciLexerVerilog::blockStartKeyword(int *style) const
{
  if (style)
    *style = Keyword;

  return "begin case catch class default do else finally for if private "
    "protected public struct try union while";
}


// Return the list of characters that can start a block.
const char *QsciLexerVerilog::blockStart(int *style) const
{
  if (style)
    *style = Operator;

  return "{";
}


// Return the list of characters that can end a block.
const char *QsciLexerVerilog::blockEnd(int *style) const
{
  if (style)
    *style = Operator;

  return "}";
}


// Return the style used for braces.
int QsciLexerVerilog::braceStyle() const
{
  return Operator;
}


// Return the string of characters that comprise a word.
const char *QsciLexerVerilog::wordCharacters() const
{
  return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_#";
}


// Returns the foreground colour of the text for a style.
QColor QsciLexerVerilog::color(int style) const
{
  switch (style)
  {
  case Default:
    return QColor(0x80,0x80,0x80);

  case Comment:
  case CommentLine:
    return QColor(0x00,0x7f,0x00);

  case CommentDoc:
  case CommentLineDoc:
    return QColor(0x3f,0x70,0x3f);

  case Number:
    return QColor(0x00,0x7f,0x7f);

  case Keyword:
    return QColor(0x00,0x00,0x7f);

  case DoubleQuotedString:
  case SingleQuotedString:
    return QColor(0x7f,0x00,0x7f);

  case PreProcessor:
    return QColor(0x7f,0x7f,0x00);

  case Operator:
  case UnclosedString:
    return QColor(0x00,0x00,0x00);

  case Identifier:
    break;

  case Regex:
    return QColor(0x3f,0x7f,0x3f);

  case CommentDocKeyword:
    return QColor(0x30,0x60,0xa0);

  case CommentDocKeywordError:
    return QColor(0x80,0x40,0x20);
  }

  return QsciLexer::color(style);
}


// Returns the end-of-line fill for a style.
bool QsciLexerVerilog::eolFill(int style) const
{
  return (style == UnclosedString);
}


// Returns the font of the text for a style.
QFont QsciLexerVerilog::font(int style) const
{
  QFont f;

  int carbonFontSize = defaultFont().pointSize();

  switch (style)
  {
  case Comment:
  case CommentLine:
  case CommentDoc:
  case CommentLineDoc:
  case CommentDocKeyword:
  case CommentDocKeywordError:
#if defined(Q_OS_WIN)
    f = QFont("Comic Sans MS",carbonFontSize-1);
#else
    f = QFont("Bitstream Vera Serif",carbonFontSize);
#endif
    break;

  case Keyword:
  case Operator:
    f = QsciLexer::font(style);
    f.setBold(true);
    break;

  case DoubleQuotedString:
  case SingleQuotedString:
  case UnclosedString:
#if defined(Q_OS_WIN)
    f = QFont("Courier",carbonFontSize+1);
#else
    f = QFont("Bitstream Vera Sans Mono",carbonFontSize+1);
#endif
    break;

  default:
    f = QsciLexer::font(style);
  }

  return f;
}


// Returns the set of keywords.
const char *QsciLexerVerilog::keywords(int set) const
{
  if (set == 1)
    return
    "always assign attribute begin case casex casez deassign default " 
    "defparam disable edge else end endattribute endcase endfunction "
    "endmodule endprimitive endspecify endtable endtask event for force "
    "forever fork function highz0 highz1 if initial join large macromodule "
    "medium module negedge parameter posedge primitive pull0 pull1 release "
    "repeat rtranif1 scalared small specify specparam strength strong0 "
    "strong1 table task trior use vectored wait weak0 weak1 while";

  if (set == 2)
    return
    "input output inout wire tri tri1 supply0 wand triand tri0 "
    "supply1 wor time trireg trior reg integer real realtime genvar";

  if (set == 3)
    return
    "and nand or nor xor xnor buf bufif0 bufif1 not notif0 notif1 "
    "pulldown pullup nmos rnmos pmos rpmos cmos rcmos tran rtran "
    "tranif0 rtranif0 tranif1 rtranif1";

  if (set == 4)
    return
    "config endconfig design instance liblist use library cell "
    "generate endgenerate automatic";

  return 0;
}


// Returns the user name of a style.
QString QsciLexerVerilog::description(int style) const
{
  switch (style)
  {
  case Default:
    return tr("Default");

  case Comment:
    return tr("C comment");

  case CommentLine:
    return tr("C++ comment");

  case CommentDoc:
    return tr("JavaDoc style C comment");

  case Number:
    return tr("Number");

  case Keyword:
    return tr("Keyword");

  case DoubleQuotedString:
    return tr("Double-quoted string");

  case SingleQuotedString:
    return tr("Single-quoted string");

  case PreProcessor:
    return tr("Pre-processor block");

  case Operator:
    return tr("Operator");

  case Identifier:
    return tr("Identifier");

  case UnclosedString:
    return tr("Unclosed string");

  case CommentLineDoc:
    return tr("JavaDoc style C++ comment");

  case KeywordSet2:
    return tr("Secondary keywords and identifiers");

  case CommentDocKeyword:
    return tr("JavaDoc keyword");

  case CommentDocKeywordError:
    return tr("JavaDoc keyword error");

  case GlobalClass:
    return tr("Global classes and typedefs");
  }

  return QString();
}


// Returns the background colour of the text for a style.
QColor QsciLexerVerilog::paper(int style) const
{
  if (style == UnclosedString)
    return QColor(0xe0,0xc0,0xe0);

  return QsciLexer::paper(style);
}


// Refresh all properties.
void QsciLexerVerilog::refreshProperties()
{
  setAtElseProp();
  setCommentProp();
  setCompactProp();
  setPreprocProp();
  setStylePreprocProp();
}


// Read properties from the settings.
bool QsciLexerVerilog::readProperties(QSettings &qs,const QString &prefix)
{
  int rc = true;

  fold_atelse = qs.value(prefix + "foldatelse", false).toBool();
  fold_comments = qs.value(prefix + "foldcomments", false).toBool();
  fold_compact = qs.value(prefix + "foldcompact", true).toBool();
  fold_preproc = qs.value(prefix + "foldpreprocessor", true).toBool();
  style_preproc = qs.value(prefix + "stylepreprocessor", false).toBool();

  return rc;
}


// Write properties to the settings.
bool QsciLexerVerilog::writeProperties(QSettings &qs,const QString &prefix) const
{
  int rc = true;

  qs.setValue(prefix + "foldatelse", fold_atelse);
  qs.setValue(prefix + "foldcomments", fold_comments);
  qs.setValue(prefix + "foldcompact", fold_compact);
  qs.setValue(prefix + "foldpreprocessor", fold_preproc);
  qs.setValue(prefix + "stylepreprocessor", style_preproc);

  return rc;
}


// Return true if else can be folded.
bool QsciLexerVerilog::foldAtElse() const
{
  return fold_atelse;
}


// Set if else can be folded.
void QsciLexerVerilog::setFoldAtElse(bool fold)
{
  fold_atelse = fold;

  setAtElseProp();
}


// Set the "fold.at.else" property.
void QsciLexerVerilog::setAtElseProp()
{
  emit propertyChanged("fold.at.else",(fold_atelse ? "1" : "0"));
}


// Return true if comments can be folded.
bool QsciLexerVerilog::foldComments() const
{
  return fold_comments;
}


// Set if comments can be folded.
void QsciLexerVerilog::setFoldComments(bool fold)
{
  fold_comments = fold;

  setCommentProp();
}


// Set the "fold.comment" property.
void QsciLexerVerilog::setCommentProp()
{
  emit propertyChanged("fold.comment",(fold_comments ? "1" : "0"));
}


// Return true if folds are compact.
bool QsciLexerVerilog::foldCompact() const
{
  return fold_compact;
}


// Set if folds are compact
void QsciLexerVerilog::setFoldCompact(bool fold)
{
  fold_compact = fold;

  setCompactProp();
}


// Set the "fold.compact" property.
void QsciLexerVerilog::setCompactProp()
{
  emit propertyChanged("fold.compact",(fold_compact ? "1" : "0"));
}


// Return true if preprocessor blocks can be folded.
bool QsciLexerVerilog::foldPreprocessor() const
{
  return fold_preproc;
}


// Set if preprocessor blocks can be folded.
void QsciLexerVerilog::setFoldPreprocessor(bool fold)
{
  fold_preproc = fold;

  setPreprocProp();
}


// Set the "fold.preprocessor" property.
void QsciLexerVerilog::setPreprocProp()
{
  emit propertyChanged("fold.preprocessor",(fold_preproc ? "1" : "0"));
}


// Return true if preprocessor lines are styled.
bool QsciLexerVerilog::stylePreprocessor() const
{
  return style_preproc;
}


// Set if preprocessor lines are styled.
void QsciLexerVerilog::setStylePreprocessor(bool style)
{
  style_preproc = style;

  setStylePreprocProp();
}


// Set the "style.within.preprocessor" property.
void QsciLexerVerilog::setStylePreprocProp()
{
  emit propertyChanged("style.within.preprocessor",(style_preproc ? "1" : "0"));
}
