//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "codegen/SysIncludes.h"
#include "interp/Interp.h"
#include "codegen/carbon_priv.h"

#define FPU_OPT 1

void Interpreter::run()
{
  InterpInstruction* p = mInstructions + mScheduleEntry;
  UInt32 a32 = 0, b32 = 0;
  UInt64 a64 = 0, b64 = 0;
#ifdef INTERP_PROFILE
  UInt32 tmp = 0;
  UInt32* currentBucket = &tmp;
#endif
#ifdef CDB
  UInt32 counter = 0;
#endif

  while (true) {
#ifdef CDB
    ++counter;
#endif
#ifdef INTERP_PROFILE
    ++(*currentBucket);
#endif
#ifdef INTERP_BYTE_STREAM
    OpCode opCode = (OpCode) *p++;
#else
    UInt32 opWord = *p++;
    OpCode opCode = (OpCode) (opWord & 0xFF);
#endif
    switch (opCode)
    {
    case eInvalid:  INFO_ASSERT(0, "Invalid opcode.");

#ifdef INTERP_BYTE_STREAM
      // Macros to assemble a 32-bit integer from 4 unaligned bytes,
#     define GET_OP8 \
        register UInt32 operand = *p++

#     define GET_OP16 \
        register UInt32 operand = *p++; \
        operand |= (*p++) << 8

#     define GET_OP24 \
        register UInt32 operand = *p++; \
        operand |= (*p++) << 8; \
        operand |= (*p++) << 16

#     define GET_OP32 \
        register UInt32 operand = *p++; \
        operand |= (*p++) << 8; \
        operand |= (*p++) << 16; \
        operand |= (*p++) << 24

#     define GET_OP64 \
        register UInt64 operand = *p++; \
        operand |= ((UInt64) (*p++)) << 8; \
        operand |= ((UInt64) (*p++)) << 16; \
        operand |= ((UInt64) (*p++)) << 24; \
        operand |= ((UInt64) (*p++)) << 32; \
        operand |= ((UInt64) (*p++)) << 40; \
        operand |= ((UInt64) (*p++)) << 48; \
        operand |= ((UInt64) (*p++)) << 56
#else
      // Macros to assemble a 32-bit integer from 4 unaligned bytes,
#     define GET_OP8 \
        register UInt32 operand = opWord >> 8;

#     define GET_OP16 \
        register UInt32 operand = opWord >> 8;

#     define GET_OP24 \
        register UInt32 operand = opWord >> 8;

#     define GET_OP32 \
        register UInt32 operand = *p++

#     define GET_OP64 \
        register UInt64 operand = *((UInt64*) p); \
        p += 2
#endif

      // Fetch data pointed at by operand, which is a byte-count relative
      // to the start of a module instance
#     define FETCH(type) (* ((type *)(&mInstance[operand])))

      // Fetch data pointed at by a reference at operand, which is a
      // byte-count relative to the start of a module instance
#     define REF(type) (** ((type **)(&mInstance[operand])))

      // To operate on data we must load it from the model.  There
      // are 4 possibilities, to accomodate 1-byte, 2-byte, 4-byte,
      // and 8-byte objects, and we are interested in filling 2
      // registers

    case eFetch8A32:   {GET_OP24; a32 = FETCH(UInt8);     break;}
    case eFetch16A32:  {GET_OP24; a32 = FETCH(UInt16);    break;}
    case eFetch32A32:  {GET_OP24; a32 = FETCH(UInt32);    break;}
//    case eFetch64A32:  {GET_OP24; a32 = FETCH(UInt64);    break;}

    case eFetchRef8A32:     {GET_OP24; a32 = REF(UInt8);       break;}
    case eFetchRef16A32:    {GET_OP24; a32 = REF(UInt16);      break;}
    case eFetchRef32A32:    {GET_OP24; a32 = REF(UInt32);      break;}
//    case eFetchRef64A32:    {GET_OP24; a32 = REF(UInt64);      break;}

    case eFetch8B32:   {GET_OP24; b32 = FETCH(UInt8);     break;}
    case eFetch16B32:  {GET_OP24; b32 = FETCH(UInt16);    break;}
    case eFetch32B32:  {GET_OP24; b32 = FETCH(UInt32);    break;}
//    case eFetch64B32:  {GET_OP24; b32 = FETCH(UInt64);    break;}

    case eFetchRef8B32:     {GET_OP24; b32 = REF(UInt8);       break;}
    case eFetchRef16B32:    {GET_OP24; b32 = REF(UInt16);      break;}
    case eFetchRef32B32:    {GET_OP24; b32 = REF(UInt32);      break;}
//    case eFetchRef64B32:    {GET_OP24; b32 = REF(UInt64);      break;}

//    case eFetch8A64:   {GET_OP24; a64 = FETCH(UInt8);     break;}
//    case eFetch16A64:  {GET_OP24; a64 = FETCH(UInt16);    break;}
    case eFetch32A64:  {GET_OP24; a64 = FETCH(UInt32);    break;}
    case eFetch64A64:  {GET_OP24; a64 = FETCH(UInt64);    break;}

//    case eFetchRef8A64:     {GET_OP24; a64 = REF(UInt8);       break;}
//    case eFetchRef16A64:    {GET_OP24; a64 = REF(UInt16);      break;}
    case eFetchRef32A64:    {GET_OP24; a64 = REF(UInt32);      break;}
//    case eFetchRef64A64:    {GET_OP24; a64 = REF(UInt64);      break;}

//    case eFetch8B64:   {GET_OP24; b64 = FETCH(UInt8);     break;}
//    case eFetch16B64:  {GET_OP24; b64 = FETCH(UInt16);    break;}
    case eFetch32B64:  {GET_OP24; b64 = FETCH(UInt32);    break;}
    case eFetch64B64:  {GET_OP24; b64 = FETCH(UInt64);    break;}

//    case eFetchRef8B64:     {GET_OP24; b64 = REF(UInt8);       break;}
//    case eFetchRef16B64:    {GET_OP24; b64 = REF(UInt16);      break;}
    case eFetchRef32B64:    {GET_OP24; b64 = REF(UInt32);      break;}
//    case eFetchRef64B64:    {GET_OP24; b64 = REF(UInt64);      break;}

    case eFetchLocal32A32:  {GET_OP24; a32 = mLocal32[operand];    break;}
//    case eFetchLocal32A64:  {GET_OP24; a64 = mLocal32[operand];    break;}
    case eFetchLocal32B32:  {GET_OP24; b32 = mLocal32[operand];    break;}
//    case eFetchLocal32B64:  {GET_OP24; b64 = mLocal32[operand];    break;}

//    case eFetchLocal64A32:  {GET_OP24; a32 = mLocal64[operand];    break;}
    case eFetchLocal64A64:  {GET_OP24; a64 = mLocal64[operand];    break;}
//    case eFetchLocal64B32:  {GET_OP24; b32 = mLocal64[operand];    break;}
//    case eFetchLocal64B64:  {GET_OP24; b64 = mLocal64[operand];    break;}

      // Load constants.  Usually these wind up in the B register,
      // except for a few arithmetic operations like (1 << x)
    case eConstA32:  {GET_OP32; a32 = operand;          break;}
    case eConstA64:  {GET_OP64; a64 = operand;          break;}
    case eConstB32:  {GET_OP32; b32 = operand;          break;}
    case eConstB64:  {GET_OP64; b64 = operand;          break;}

      // We need those same operations for storing and writing
      // references, but we don't generally need to store
      // from B because our operators never write into B.

    case eStore8A32:        {GET_OP24; FETCH(UInt8)  = a32;    break;}
    case eStore16A32:       {GET_OP24; FETCH(UInt16) = a32;    break;}
    case eStore32A32:       {GET_OP24; FETCH(UInt32) = a32;    break;}
//    case eStore64A32:       {GET_OP24; FETCH(UInt64) = a32;    break;}
//    case eStore8A64:        {GET_OP24; FETCH(UInt8)  = a64;    break;}
//    case eStore16A64:       {GET_OP24; FETCH(UInt16) = a64;    break;}
//    case eStore32A64:       {GET_OP24; FETCH(UInt32) = a64;    break;}
    case eStore64A64:       {GET_OP24; FETCH(UInt64) = a64;    break;}

//    case eStoreRef8A32:     {GET_OP24; REF(UInt8)  = a32;      break;}
//    case eStoreRef16A32:    {GET_OP24; REF(UInt16) = a32;      break;}
//    case eStoreRef32A32:    {GET_OP24; REF(UInt32) = a32;      break;}
//    case eStoreRef64A32:    {GET_OP24; REF(UInt64) = a32;      break;}
//    case eStoreRef8A64:     {GET_OP24; REF(UInt8)  = a64;      break;}
//    case eStoreRef16A64:    {GET_OP24; REF(UInt16) = a64;      break;}
//    case eStoreRef32A64:    {GET_OP24; REF(UInt32) = a64;      break;}
//    case eStoreRef64A64:    {GET_OP24; REF(UInt64) = a64;      break;}

    case eStoreLocal32A32:  {GET_OP24; mLocal32[operand] = a32; break;}
//    case eStoreLocal32A64:  {GET_OP24; mLocal32[operand] = a64; break;}
//    case eStoreLocal64A32:  {GET_OP24; mLocal64[operand] = a32; break;}
    case eStoreLocal64A64:  {GET_OP24; mLocal64[operand] = a64; break;}

#   define MASK_OP32(opcode, op) \
    case opcode: { \
      GET_OP24; \
      a32 op b32; \
      a32 &= ((1 << operand) - 1); \
      break; \
    }

#   define MASK_OP64(opcode, op, breg) \
    case opcode: { \
      a64 op breg; \
      GET_OP24; \
      a64 &= ((1ULL << operand) - 1); \
      break; \
    }

    MASK_OP32(eAdd32, +=)
    MASK_OP32(eSub32, -=)
//    case eMod32:    if (b32) a32 %= b32; break; // avoid FPE 
//    MASK_OP32(eMul32, *=)
//    case eDiv32:    if (b32) a32 /= b32; break; // avoid FPE 
    MASK_OP32(eLShift32, <<=)
    case eRShift32: a32 >>= b32; break;
    case eBitOr32:  a32 |= b32;  break;
    case eBitAnd32: a32 &= b32;  break;
    case eBitXor32: a32 ^= b32;  break;
    case eLogOr32:  a32 = a32 || b32; break;
//    case eLogAnd32: a32 = a32 && b32; break;
    case eLogEq32:  a32 = a32 == b32; break;
    case eLogLt32:  a32 = a32 < b32;  break;
//    case eLogLte32: a32 = a32 <= b32; break;
    case eLogNot32: a32 = !a32; break;
    case eBitNot32: a32 = ~a32; break;
//    case eNegate32: {
//      a32 = -a32;
//      GET_OP24;
//      a32 &= ((1 << operand) - 1);
//      break;
//    }

    MASK_OP64(eAdd64, +=, b64)
    MASK_OP64(eSub64, -=, b64)
    case eMod64:    if (b64) a64 %= b64;  break; // avoid FPE
    MASK_OP64(eMul64, *=, b64)
    case eDiv64:    if (b64) a64 /= b64;  break; // avoid FPE
    MASK_OP64(eLShift64, <<=, b32)
    case eRShift64: a64 >>= b32; break;
    case eBitOr64:  a64 |= b64;  break;
    case eBitAnd64: a64 &= b64;  break;
//    case eBitXor64: a64 ^= b64;  break;
//    case eLogOr64:  a32 = a64 || b64; break;
//    case eLogAnd64: a32 = a64 && b64; break;
    case eLogEq64:  a32 = a64 == b64; break;
//    case eLogLt64:  a32 = a64 < b64;  break;
//    case eLogLte64: a32 = a64 <= b64; break;
//    case eLogNot64: a64 = !a64; break;
//    case eBitNot64: a64 = ~a64; break;
//    case eNegate64: {
//      a64 = -a64;
//      GET_OP24;
//      a64 &= ((1ULL << operand) - 1);
//      break;
//    }

    // Moving data between 32-bit and 64-bit A & B registers
    case eMoveA32A64: a64 = a32; break;
    case eMoveA32B32: b32 = a32; break;
//    case eMoveA32B64: b64 = a32; break;
    case eMoveA64A32: a32 = a64; break;
    case eMoveA64B32: b32 = a64; break;
    case eMoveA64B64: b64 = a64; break;
//    case eMoveB32B64: b64 = b32; break;
    case eMoveB32A32: a32 = b32; break;
//    case eMoveB32A64: a64 = b32; break;
//    case eMoveB64B32: b32 = b64; break;
//    case eMoveB64A32: a32 = b64; break;
//    case eMoveB64A64: a64 = b64; break;

      // To assist with concatenations, we combine two logical operations
      // into one instruction.  The concat operator assumes the value
      // you are concatting into is in reg a, and the value you are
      // shifting into it is in reg b.  The operand is the number of
      // interesting bits in reg b.
    case eConcat32: {
      GET_OP24;
      a32 = (a32 << operand) | b32;
      break;
    }
    case eConcat64: {
      GET_OP24;
      a64 = (a64 << operand) | b64;
      break;
    }

      // If a32 is 1, negate it into 0xFFFFFFFF, and use that result
      // to mask off the operand, which represents a relative jump.
      // Note that our implementation of "if" does not need an "if".
    case eIf32: {
      GET_OP24;
      p += (-(SInt32)(a32 != 0)) & operand;
      break;
    }

    case eIf64: {
      GET_OP24;
      p += (-(SInt32)(a64 != 0)) & operand;
      break;
    }

      // a change index and a storage offset for a clock, which
      // must be compatible with a UInt8.  Then it reads two
      // offsets 
    case eIfChanged: {
      GET_OP24;
      UInt32 changeIndex = operand;
      {
        GET_OP32;
        p += (((SInt32)(mChangeArray[changeIndex])) - 1) & operand;
      }
      break;
    }

      // Relative jumps are used for skipping forward to 
      // skip the losing clause in else statements, or
      // for skipping backward to iterate over a for-loop.
      // Note that the "dst" value is a constant -- it is
      // never looked up in the model.  And also note that
      // it will advance the pointer to the last statement
      // that we *don't* want to execute, because the main
      // for-loop always increments p.
    case eRelativeJump: {
      GET_OP24;
      p += operand;
      break;
    }

    case eSetInstance: {
      GET_OP24;
      mInstance = mModel + operand;
      break;
    }
    case eReturn: p = mReturnAddress; break;

      // CALL implies setting the instance.  Return does not restore it however.
    case eCall: {
      GET_OP24;
      mInstance = mModel + operand;
      {
        GET_OP32;
        mReturnAddress = p;
        p = mInstructions + operand;
      }
      break;
    }

    case ePushA32: ++mStack32; *mStack32 = a32; break;
    case ePushA64: ++mStack64; *mStack64 = a64; break;
    case ePushB32: ++mStack32; *mStack32 = b32; break;
//    case ePushB64: ++mStack64; *mStack64 = b64; break;
    case ePopA32:  a32 = *mStack32; --mStack32; break;
    case ePopA64:  a64 = *mStack64; --mStack64; break;
    case ePopB32:  b32 = *mStack32; --mStack32; break;
//    case ePopB64:  b64 = *mStack64; --mStack64; break;
      
      // index for bit-selects is always UInt32, even for 64-bit vectors,
      // and the target type is always UInt32 as well.
//    case eBitSel32: a32 = (a32 >> b32) & 1; break;
//    case eBitSel64: a32 = (a64 >> b32) & 1; break;

      // index for bit-selects is always UInt32, even for 64-bit vectors,
      // and the target type is always UInt32 as well.
    case eKBitSel32: {
      GET_OP24;
      a32 = (a32 >> operand) & 1;
      break;
    }
//    case eKBitSel64: {
//      GET_OP24;
//      a32 = (a32 >> operand) & 1;
//      break;
//    }

      // bit-assign.  The source value & destination bit is 32-bits, even if
      // the destination is 64 bits.
#   define OP_BITASSIGN(type, dstVar, dstBit, srcVar) { \
      GET_OP24; \
      type mask = ((type) 1) << dstBit; \
      if (srcVar) \
        dstVar |= mask; \
      else \
        dstVar &= ~mask; \
      break; \
    }

/*
    case eBitSet8:     OP_BITASSIGN(UInt8,  FETCH(UInt8),  a32, b32);
    case eBitSet16:    OP_BITASSIGN(UInt16, FETCH(UInt16), a32, b32);
    case eBitSet32:    OP_BITASSIGN(UInt32, FETCH(UInt32), a32, b32);
    case eBitSet64:    OP_BITASSIGN(UInt64, FETCH(UInt64), a32, b64);
    case eBitSetRef8:  OP_BITASSIGN(UInt8,  REF(UInt8),  a32, b32);
    case eBitSetRef16: OP_BITASSIGN(UInt16, REF(UInt16), a32, b32);
    case eBitSetRef32: OP_BITASSIGN(UInt32, REF(UInt32), a32, b32);
    case eBitSetRef64: OP_BITASSIGN(UInt64, REF(UInt64), a32, b64);
    case eBitSetLocal32: OP_BITASSIGN(UInt32, mLocal32[operand], a32, b32);
    case eBitSetLocal64: OP_BITASSIGN(UInt64, mLocal64[operand], a32, b64);
*/

      // For now, part-select exprs must have constant size & range.  For
      // now I will try to fit them into 12 bits each and see if I get
      // away with it!
#ifdef INTERP_BYTE_STREAM
    case ePartSel32: {
      register UInt8 startBit = *p++;
      register UInt8 numBits = *p++;
      a32 = (a32 >> startBit) & ((1UL << numBits) - 1);
      break;

    }
    case ePartSel64: {
      register UInt8 startBit = *p++;
      register UInt8 numBits = *p++;
      a64 = (a64 >> startBit) & ((1ULL << numBits) - 1);
      break;
    }

      // Concat-assign is basically like PartSel except
      // that uses data in register b to write register a
    case ePartSelBA32: {
      register UInt8 startBit = *p++;
      register UInt8 numBits = *p++;
      a32 = (b32 >> startBit) & ((1UL << numBits) - 1);
      break;

    }
    case ePartSelBA64: {
      register UInt8 startBit = *p++;
      register UInt8 numBits = *p++;
      a64 = (b64 >> startBit) & ((1ULL << numBits) - 1);
      break;
    }
#else
    case ePartSel32: {
      register UInt32 startBit = (opWord >> 8) & 0x3FF;
      register UInt32 numBits = (opWord >> 20);
      a32 = (a32 >> startBit) & ((1UL << numBits) - 1);
      break;

    }
    case ePartSel64: {
      register UInt32 startBit = (opWord >> 8) & 0x3FF;
      register UInt32 numBits = (opWord >> 20);
      a64 = (a64 >> startBit) & ((1ULL << numBits) - 1);
      break;
    }

      // Concat-assign is basically like PartSel except
      // that uses data in register b to write register a
    case ePartSelBA32: {
      register UInt32 startBit = (opWord >> 8) & 0x3FF;
      register UInt32 numBits = (opWord >> 20);
      a32 = (b32 >> startBit) & ((1UL << numBits) - 1);
      break;

    }
    case ePartSelBA64: {
      register UInt32 startBit = (opWord >> 8) & 0x3FF;
      register UInt32 numBits = (opWord >> 20);
      a64 = (b64 >> startBit) & ((1ULL << numBits) - 1);
      break;
    }
#endif
      // For now, part-select assigns must have constant size & range
#ifdef INTERP_BYTE_STREAM
#   define OP_PARTASSIGN(type, dstVar, srcReg) { \
      GET_OP24; \
      register type& dstRef = dstVar; \
      { \
       register UInt32 startBit = *p++; \
       register UInt32 numBits = *p++; \
       type mask = (((type) 1) << numBits) - 1; \
       dstRef = (dstRef & ~(mask << startBit)) | \
                ((((type) srcReg) & mask) << startBit);} \
      break; \
    }
#else
#   define OP_PARTASSIGN(type, dstVar, srcReg) { \
      GET_OP24; \
      register type& dstRef = dstVar; \
      { \
       GET_OP32; \
       register UInt32 startBit = operand & 0x3FF; \
       register UInt32 numBits = operand >> 12; \
       type mask = (((type) 1) << numBits) - 1; \
       dstRef = (dstRef & ~(mask << startBit)) | \
                ((((type) srcReg) & mask) << startBit);} \
      break; \
    }
#endif
//    case ePartSet8A32:       OP_PARTASSIGN(UInt8,  FETCH(UInt8),      a32);
//    case ePartSet16A32:      OP_PARTASSIGN(UInt16, FETCH(UInt16),     a32);
//    case ePartSet32A32:      OP_PARTASSIGN(UInt32, FETCH(UInt32),     a32);
    case ePartSet64A32:      OP_PARTASSIGN(UInt64, FETCH(UInt64),     a32);
//    case ePartSetRef8A32:    OP_PARTASSIGN(UInt8,  REF(UInt8),        a32);
//    case ePartSetRef16A32:   OP_PARTASSIGN(UInt16, REF(UInt16),       a32);
//    case ePartSetRef32A32:   OP_PARTASSIGN(UInt32, REF(UInt32),       a32);
//    case ePartSetRef64A32:   OP_PARTASSIGN(UInt64, REF(UInt64),       a32);
//    case ePartSetLocal32A32: OP_PARTASSIGN(UInt32, mLocal32[operand], a32);
//    case ePartSetLocal64A32: OP_PARTASSIGN(UInt64, mLocal64[operand], a32);

/*
    case ePartSet8A64:       OP_PARTASSIGN(UInt8,  FETCH(UInt8),      a64);
    case ePartSet16A64:      OP_PARTASSIGN(UInt16, FETCH(UInt16),     a64);
    case ePartSet32A64:      OP_PARTASSIGN(UInt32, FETCH(UInt32),     a64);
    case ePartSet64A64:      OP_PARTASSIGN(UInt64, FETCH(UInt64),     a64);
    case ePartSetRef8A64:    OP_PARTASSIGN(UInt8,  REF(UInt8),        a64);
    case ePartSetRef16A64:   OP_PARTASSIGN(UInt16, REF(UInt16),       a64);
    case ePartSetRef32A64:   OP_PARTASSIGN(UInt32, REF(UInt32),       a64);
    case ePartSetRef64A64:   OP_PARTASSIGN(UInt64, REF(UInt64),       a64);
    case ePartSetLocal32A64: OP_PARTASSIGN(UInt32, mLocal32[operand], a64);
    case ePartSetLocal64A64: OP_PARTASSIGN(UInt64, mLocal64[operand], a64);
*/

    case eReductOr32:
      a32 = a32 != 0;
      break;
    case eReductOr64:
      a32 = a64 != 0;
      break;
/*
    case eReductNor32:
      a32 = a32 == 0;
      break;
    case eReductNor64:
      a32 = a64 == 0;
      break;
    case eReductXor32:
      a32 = carbon_popcount(a32);
      break;
    case eReductXor64:
      a64 = carbon_popcount(a32);
      break;
    case eReductXnor32:
      a32 = !carbon_popcount(a32);
      break;
    case eReductXnor64:
      a32 = !carbon_popcount(a64);
      break;
*/

#if 0
    case OP_CODE(eMemSel, 0): {GET_OP24; a32 = FETCH(Memory*)->read32(a32);}
    case OP_CODE(eMemSel, 1): {GET_OP24; a64 = FETCH(Memory*)->read64(a32);}
    case OP_CODE(eMemSel, 2): {GET_OP24; a32 = FETCH(Memory*)->read32(a64);}
    case OP_CODE(eMemSel, 3): {GET_OP24; a64 = FETCH(Memory*)->read64(a64);}

    case OP_CODE(eMemSelRef, 0): {GET_OP24; a32 = REF(Memory*)->read32(a32);}
    case OP_CODE(eMemSelRef, 1): {GET_OP24; a64 = REF(Memory*)->read64(a32);}
    case OP_CODE(eMemSelRef, 2): {GET_OP24; a32 = REF(Memory*)->read32(a64);}
    case OP_CODE(eMemSelRef, 3): {GET_OP24; a64 = REF(Memory*)->read64(a64);}

#   define OP_MEMASSIGN(mem, addr, data) mem->write(addr, data); break

    case OP_CODE(eMemSet, 0): {GET_OP24; OP_MEMASSIGN(FETCH(Memory*), a32, b32);}
    case OP_CODE(eMemSet, 1): {GET_OP24; OP_MEMASSIGN(FETCH(Memory*), a32, b64);}
    case OP_CODE(eMemSet, 2): {GET_OP24; OP_MEMASSIGN(FETCH(Memory*), a64, b32);}
    case OP_CODE(eMemSet, 3): {GET_OP24; OP_MEMASSIGN(FETCH(Memory*), a64, b64);}

    case OP_CODE(eMemSetRef, 0): {GET_OP24; OP_MEMASSIGN(REF(Memory*), a32,b32);}
    case OP_CODE(eMemSetRef, 1): {GET_OP24; OP_MEMASSIGN(REF(Memory*), a32,b64);}
    case OP_CODE(eMemSetRef, 2): {GET_OP24; OP_MEMASSIGN(REF(Memory*), a64,b32);}
    case OP_CODE(eMemSetRef, 3): {GET_OP24; OP_MEMASSIGN(REF(Memory*), a64,b64);}
#endif

#ifdef INTERP_PROFILE
    case eProfile: {
      GET_OP24;
      currentBucket = &mProfileBuckets[operand];
      break;
    }
#endif

    case eEnd:
      return;

    default:
      INFO_ASSERT(0, "Unhandled op.");
    } // switch
  } // for
} // void Interpreter::run
