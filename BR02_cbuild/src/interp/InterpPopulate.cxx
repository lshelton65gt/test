//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"

#include "nucleus/NUExprFactory.h"
#include "nucleus/NUAliasDB.h"

#include "flow/FLNodeElab.h"

#include "reduce/Fold.h"

#include "schedule/Schedule.h"
#include "schedule/Event.h"
#include "schedule/ScheduleMask.h"

#include "interp/Interp.h"

#include "codegen/CGAuxInfo.h"
#include "codegen/codegen.h"

#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"

#include "util/UtIOStream.h"
#include "util/UtIndent.h"
#include "util/AtomicCache.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "hdl/HdlVerilogPath.h"

class MsgContext;
class IODBNucleus;
class ArgProc;
class NUNetRefFactory;

class InterpPopulator
{
  typedef UtHashMap<NUUseDefNode*, UInt32> BlockIntMap;
  typedef UtHashMap<NUNet*, UInt32> BlockLocalNetMap;
  typedef std::pair<SInt32, Interpreter::NetStorageMode> OffsetMode;
  typedef UtHashMap<NUNet*, OffsetMode> NetOffsets;

  enum Phase {
    eVisitBlocks,
    eCompileMultiUsedBlocks,
    eCompileSchedule
  };
public:

  //! constructor.
  InterpPopulator(NUDesign* design, STSymbolTable* symtab, SCHSchedule* sched,
                  const char* fileRoot, MsgContext* msgContext,
                  IODBNucleus* iodb, AtomicCache* atomicCache,
                  ArgProc* args, NUNetRefFactory* netRefFactory,
                  SourceLocatorFactory* sourceLocatorFactory) :
    mSchedule(sched),
    mFileRoot(fileRoot),
    mScheduleEntryPoint(0),
    mSymbolTable(symtab),
    mMsgContext(msgContext),
    mIODB(iodb),
    mAtomicCache(atomicCache),
    mArgs(args),
    mNetRefFactory(netRefFactory),
    mCurrentModuleElab(0),
    mSourceLocatorFactory(sourceLocatorFactory)
  {
    for (SInt32 i = 0; i <= (SInt32) Interpreter::eRegMax; ++i)
    {
      mRegSave[i] = false;
      mRegExprs[i] = NULL;
    }
    mMaxStack32Depth = 0;
    mMaxStack64Depth = 0;
    mMaxNumLocal32 = 0;
    mMaxNumLocal64 = 0;
    mCurrentStack32Depth = 0;
    mCurrentStack64Depth = 0;
    mFailure = false;
    mInstanceAtRoot = false;

    findUnallocatedNetOffsets();
    mFold = new Fold(netRefFactory, args, msgContext, atomicCache, iodb,
                     false,
                     eFoldUDValid | eFoldUDKiller | eFoldAggressive);
    mExprFactory = new NUExprFactory;

    SInt32 numRoots = 0;
    for (STSymbolTable::RootIter p = symtab->getRootIter(); !p.atEnd(); ++p) {
      STSymbolTableNode* node = *p;
      STBranchNode* root = node->castBranch();
      ST_ASSERT(root, node);
#ifdef INTERPRETER
      mRootOffset = gCodeGen->getModuleOffset(root);
#else
      mRootOffset = 0;
#endif
      ++numRoots;
    }
    INFO_ASSERT(numRoots == 1, "More than one design root.");
    STSymbolTable* aliasDB
      = new STSymbolTable(design->getAliasBOM(), atomicCache);
    aliasDB->setHdlHier(new HdlVerilogPath);
    SourceLocator loc = sourceLocatorFactory->create("<carbon>", 0);
    bool is_top_level = false;
    mTempModule = new NUModule(design, is_top_level,
                               atomicCache->intern("$carbon_temp"),
                               aliasDB, mNetRefFactory,
                               atomicCache, loc, iodb, 0, 0, false,
                               eSLUnknown /* not sure what to use */);
    mTempNetCount = 0;
  } // InterpPopulator

  //! destructor
  ~InterpPopulator() {
    delete mExprFactory;
    delete mFold;
    delete mTempModule;
/*
    for (UInt32 i = 0; i < mTempNets.size(); ++i)
      delete mTempNets[i];
*/
  }

  // Simplify an expression and save it locally so we don't have lifetime
  // issues.
  const NUExpr* saveExpr(NUExpr* expr, bool fold) {
    if (expr->isManaged())
      return expr;
    if (fold) {
      NUExpr* folded = mFold->fold(expr);
      return mExprFactory->insert(folded);
    }
    return mExprFactory->insert(expr);
  }

  void clearRegs() {
    for (SInt32 i = 0; i <= (SInt32) Interpreter::eRegMax; ++i)
    {
      mRegSave[i] = false;
      mRegExprs[i] = NULL;
    }
  }

  bool findNetOffset(NUNet* net, OffsetMode* om) {
    CGAuxInfo* cgaux = net->getCGOp();
    bool ret = true;
    if (net->isAllocated() && !net->isBlockLocal() && cgaux->hasOffset()) {
      om->first = cgaux->getOffset();
      om->second = Interpreter::eNetStorage;
    }
    else if (cgaux->hasOffset()) {
      om->first = cgaux->getOffset();
      om->second = Interpreter::eNetReference;
    }
    else {
      NetOffsets::iterator p = mNetOffsets.find(net);
      if (p == mNetOffsets.end())
        ret = false;
      else
        *om = p->second;
    }
    return ret;
  }

  bool isAncestorOf(STBranchNode* ancestor, STSymbolTableNode* child,
                    SInt32* offset)
  {
#ifdef INTERPRETER
    UInt32 childOffset = gCodeGen->getModuleOffset(ancestor);
#else
    UInt32 childOffset = 0;
#endif
    if (childOffset == 0)
      return false;
    for (STBranchNode* a = child->getParent(); a != NULL; a = a->getParent()) {
      if (a == ancestor)
      {
#ifdef INTERPRETER
        UInt32 parentOffset = gCodeGen->getModuleOffset(child->getParent());
        if (parentOffset != 0) {
          *offset = parentOffset - childOffset;
          ST_ASSERT(*offset >= 0, child);
          return true;
        }
#else
        *offset = 0;
        return true;
#endif
      }
    }
    return false;
  } // bool isAncestorOf

  // Find all the nets that are referenced in a module but
  // allocated in a submodule.  To calculate their offsets
  // we will need to use the alias ring to find the storage
  // of some net instance and subtract.
  void findUnallocatedNetOffsets() {
    for (STSymbolTable::NodeLoop i = mSymbolTable->getNodeLoop();
         !i.atEnd(); ++i)
    {
      STAliasedLeafNode* node = (*i)->castLeaf();
      if (node != NULL) {
        NUNet* net = NUNet::find(node);
        OffsetMode om;
        om.first = 0;
        if ((net != NULL) && !net->isDead() && !findNetOffset(net, &om)
            && !net->isBlockLocal())
        {
          STBranchNode* modScope = node->getParent();
          bool found = false;

          // First, see if the storage-node lives in a submodule of
          // net's parent-module.  If so, use that.
          STAliasedLeafNode* storage = node->getStorage();
          SInt32 modOffset;
          if (isAncestorOf(modScope, storage, &modOffset)) {
            found = findNetOffset(NUNet::find(storage), &om);
            om.first += modOffset;
          }

          // Walk the alias ring to find a reference variable for this net,
          // or some reference that we can use, in a child module
          else {
            for (STAliasedLeafNode::AliasLoop p(node); !p.atEnd(); ++p)
            {
              STAliasedLeafNode* alias = *p;
              if (isAncestorOf(modScope, alias, &modOffset) &&
                  findNetOffset(NUNet::find(alias), &om))
              {
                om.first += modOffset;
                found = true;
              }
            }
//            ASSERT(found);  fails when flattening multi-fpu
          }
          if (found)
            mNetOffsets[net] = om;
        } // if
      } // if
    } // for
  } // void findUnallocatedNetOffsets

  void populate()
  {
    UtString buf;
    buf << mFileRoot << ".code";
    UtOBStream codeFile(buf.c_str());
    if (!codeFile.is_open())
    {
      fprintf(stderr, "%s\n", codeFile.getErrmsg());
      return;
    }
    mInterpreter.putCodeFile(&codeFile);

    buf.clear();
    buf << mFileRoot << ".comment";
    UtOBStream commentFile(buf.c_str());
    if (!commentFile.is_open())
    {
      fprintf(stderr, "%s\n", commentFile.getErrmsg());
      return;
    }
    mInterpreter.putCommentFile(&commentFile);

    walkSchedule(eVisitBlocks);
    walkSchedule(eCompileMultiUsedBlocks);
    mScheduleEntryPoint = mInterpreter.getInstructionCount();
    walkSchedule(eCompileSchedule);
    mInterpreter.opcode(Interpreter::eEnd);

    // Now write some records at the end to help the interpreter
    // initialize its data structures
    codeFile << "\n";
    codeFile << UtIO::hex << UtIO::setfill('0') << UtIO::setw(8)
             << mMaxStack32Depth << "\n";
    codeFile << UtIO::hex << UtIO::setfill('0') << UtIO::setw(8)
             << mMaxStack64Depth << "\n";
    codeFile << UtIO::hex << UtIO::setfill('0') << UtIO::setw(8)
             << mMaxNumLocal32 << "\n";
    codeFile << UtIO::hex << UtIO::setfill('0') << UtIO::setw(8)
             << mMaxNumLocal64 << "\n";
    codeFile << UtIO::hex << UtIO::setfill('0') << UtIO::setw(8)
             << mScheduleEntryPoint << "\n";

    // Write the number of blocks to the code file so a bucket array
    // can be allocated, and write the block-index
    UInt32 numProfileBuckets = 0;

#ifdef INTERP_PROFILE
    numProfileBuckets = mBlockProfileIndexMap.size();
    INFO_ASSERT(mBlockProfileVector.size() == numProfileBuckets, "Consistency check failed.");
    for (UInt32 i = 0; i < numProfileBuckets; ++i) {
      UtString buf;
      NUUseDefNode* useDef = mBlockProfileVector[i];
      useDef->getLoc().compose(&buf);
      buf << "\t" << useDef->typeStr();
      StringAtom* name = useDef->getName();
      if (name != NULL)
        buf << " " << name->str();
      INFO_ASSERT(mBlockProfileIndexMap[useDef] == i, "Consistency check failed.");
      commentFile << UtIO::dec << i << ":\t" << buf << "\n";
    }
#endif

    codeFile << UtIO::hex << UtIO::setfill('0') << UtIO::setw(8)
             << numProfileBuckets << "\n";
  } // void populate

  void walkSchedule(Phase phase)
  {
    mPhase = phase;

    SCHSchedule::CombinationalLoop a;
    for (a = mSchedule->loopCombinational(eCombInput); !a.atEnd(); ++a)
    {
      SCHCombinational* input = *a;
      if (!input->empty())
        compileCombSchedule(input, "input");
    }
    for (a = mSchedule->loopCombinational(eCombAsync); !a.atEnd(); ++a)
    {
      SCHCombinational* input = *a;
      if (!input->empty())
        compileCombSchedule(input, "async");
    }

    SCHSchedule::CombinationalLoop p;
    for (p = mSchedule->loopCombinational(eCombSample); !p.atEnd(); ++p)
    {
      SCHCombinational* comb = *p;
      compileCombSchedule(comb, "sample");
    } // for

    // Sequential schedules
   SCHSequentials::SequentialLoop s;
   for (s = mSchedule->loopSequential(); !s.atEnd(); ++s) {
     SCHSequential* seq = *s;
     compileSequentialSchedule(seq);
   }

    for (p = mSchedule->loopCombinational(eCombTransition); !p.atEnd(); ++p)
    {
      SCHCombinational* comb = *p;
      compileCombSchedule(comb, "transition");
    } // for
  } // void walkSchedule

  // Compile a combinational event, which will be either one or both
  // edges of a clock.  If the SCHEvent* is NULL, that means we want
  // both edges.  The easiest way to work this, I think, is to initialize
  // register, b32 to 0.  We can then unconditionally iterate through
  // all the events, and if any of them fire, set b32 to 1.  Then we
  // can do a simple branch based on b32 to execute the combinational block
  void combEvent(const STSymbolTableNode* n, const SCHEvent* event) {
    const STAliasedLeafNode* clockNode = n->castLeaf();
    ST_ASSERT(clockNode, n);

    // First detect whether clockNode has changed.  
    const NUNetElab* clk = NUNetElab::find(clockNode);
    ST_ASSERT(clk, clockNode);

    // Make an outer guard to only run sequential schedules if
    // the clock changed
    SInt32 changeIndex = gCodeGen->getChangedIndex(clk);
    ST_ASSERT(changeIndex >= 0, clockNode);
    Interpreter::Block* clkBlock = mInterpreter.pushBlock();

    if (event == NULL) {
      // Sensitive to both edges.  Simply set b32 to 1
      mInterpreter.const32(1, Interpreter::eRegA32);
    }
    else {
      // Examine the clock to see which edge is live
      STAliasedLeafNode* storage = clk->getSymNode()->getStorage();
      SInt32 offset = CbuildSymTabBOM::getStorageOffset(storage);
      offset -= mRootOffset;
      UtString buf;
      clk->compose(&buf, NULL);
      NU_ASSERT(clk->getNet()->getBitSize() == 1, clk);
      mInterpreter.fetch(Interpreter::eRegB32, offset, 1,
                         Interpreter::eNetStorage, buf.c_str());

      // inner guard to run posedge vs negedge
      if (event->getClockEdge() == eClockNegedge)
        mInterpreter.opcode(Interpreter::eLogNot32, "negedge");
      mInterpreter.opcode(Interpreter::eLogOr32);
    }
    mInterpreter.popBlock(clkBlock);
    mInterpreter.addIfChanged(changeIndex, clkBlock);
  } // void combEvent

  void compileCombSchedule(SCHCombinational* comb, const char* label)
  {
    const SCHScheduleMask* mask = comb->getMask();
    //const SCHInputNets* inputNets = comb->getInputNets();

    if (mPhase == eCompileSchedule) {
      UtString buf(label);
      buf << " ";
      mask->compose(&buf, NULL);
      mInterpreter.blockComment(buf.c_str());

      // For now, if this schedule has any non-clock events then
      // punt the detection and just run it.
      if (mask->hasInput() || mask->hasOutput())
        compileSchedule(comb->getSchedule());
      else {
        clearExprs(Interpreter::eRegA32);

        // Initiaze b32 to 0
        mInterpreter.instance(mRootOffset);
        mInterpreter.const32(0, Interpreter::eRegA32);

        // Walk through the events in sorted order, so that we can
        // determine ahead of time if we care about a clock's value.
        // If we are sensitive to both edges then we don't, and we
        // can count on a clock's two edges being adjacent events in
        // the sorted list.
        const SCHEvent* prevEvent = NULL;
        for (SCHScheduleMask::SortedEvents p = mask->loopEventsSorted();
             !p.atEnd(); ++p)
        {
          const SCHEvent* event = *p;
          if (prevEvent != NULL) {
            if (prevEvent->getClock() == event->getClock()) {
              combEvent(prevEvent->getClock(), NULL);
              prevEvent = NULL;
            }
            else {
              combEvent(prevEvent->getClock(), prevEvent);
              prevEvent = event;
            }
          }
          else
            prevEvent = event;
        }
        if (prevEvent != NULL)
          combEvent(prevEvent->getClock(), prevEvent);

        // This processing should leave us with a32==1 if we are to
        // run the combinational schedule
        Interpreter::Block* posedge = mInterpreter.pushBlock();
        Interpreter::Block* negedge = mInterpreter.pushBlock();
        mInterpreter.popBlock(negedge);
        compileSchedule(comb->getSchedule());
        mInterpreter.popBlock(posedge);
        mInterpreter.addIf(false, posedge, negedge);
      } // else
    } // if
    else
      compileSchedule(comb->getSchedule());
  } // void compileCombSchedule

  void compileSchedule(SCHIterator i) {
    for (; !i.atEnd(); ++i) {
      FLNodeElab* flow = *i;
      compileFlow(flow);
    }
  }

  void compileSequentialSchedule(SCHSequential* seq) {
    // Use the following two bits of information to determine when to run
    // the logic
    if (mPhase == eCompileSchedule) {
      const NUNetElab* clk = seq->getEdgeNet();
      mInterpreter.instance(mRootOffset);

      // Make an outer guard to only run sequential schedules if
      // the clock changed
      SInt32 changeIndex = gCodeGen->getChangedIndex(clk);
      NU_ASSERT(changeIndex >= 0, clk);
      Interpreter::Block* clkBlock = mInterpreter.pushBlock();

      {
        STAliasedLeafNode* storage = clk->getSymNode()->getStorage();
        SInt32 offset = CbuildSymTabBOM::getStorageOffset(storage);
        offset -= mRootOffset;
        UtString buf;
        clk->compose(&buf, NULL);
        NU_ASSERT(clk->getNet()->getBitSize() == 1, clk);
        mInterpreter.fetch(Interpreter::eRegA32, offset, 1,
                           Interpreter::eNetStorage, buf.c_str());

        // inner guard to run posedge vs negedge
        Interpreter::Block* posedge = mInterpreter.pushBlock();
        Interpreter::Block* negedge = mInterpreter.pushBlock();
        if (!seq->empty(eClockNegedge))
          compileSchedule(seq->getSchedule(eClockNegedge));
        mInterpreter.popBlock(negedge);
        if (!seq->empty(eClockPosedge))
          compileSchedule(seq->getSchedule(eClockPosedge));
        mInterpreter.popBlock(posedge);
        mInterpreter.addIf(false, posedge, negedge);
      }
      mInterpreter.popBlock(clkBlock);
      mInterpreter.addIfChanged(changeIndex, clkBlock);
    } // if

    // If we are not compiling the schedule, just visit all the
    // the flow-nodes in the schedules so we can properly identify
    // the multiply called blocks
    else {
      if (!seq->empty(eClockNegedge))
        compileSchedule(seq->getSchedule(eClockNegedge));
      if (!seq->empty(eClockPosedge))
        compileSchedule(seq->getSchedule(eClockPosedge));
    }
  } // void compileSequentialSchedule

  void compileFlow(FLNodeElab* flow)
  {
    NUUseDefNode* node = flow->getUseDefNode();
    mCurrentModuleElab = NUModuleElab::find(flow->getHier());
    FLN_ELAB_ASSERT(mCurrentModuleElab, flow);

    if (mPhase == eVisitBlocks)
    {
      BlockIntMap::iterator p = mBlockCountMap.find(node);
      if (p == mBlockCountMap.end())
      {
        //mBlocks.push_Back(node);
        mBlockCountMap[node] = 1;
#ifdef INTERP_PROFILE
        mBlockProfileIndexMap[node] = mBlockProfileVector.size();
        mBlockProfileVector.push_back(node);
#endif
      }
      else
        ++p->second;
    }
    else if (mPhase == eCompileMultiUsedBlocks)
    {
      // All the blocks that are used in more than one schedule
      // are compiled up front, in order, their addresses noted,
      // and ending with a jump to Interpreter::mReturnAddress,
      // which the caller is responsible for setting before jumping
      // there.
      if (mBlockCountMap[node] > 1)
      {
        if (mBlockAddressMap.find(node) == mBlockAddressMap.end())
        {
          mBlockAddressMap[node] = mInterpreter.getInstructionCount();
          compileUseDef(node);
          mInterpreter.insertReturn();
        }
      }
    }
    else
    {
      FLN_ELAB_ASSERT(mPhase == eCompileSchedule, flow);
      UtString buf, hierBuf, locBuf, netBuf;
      node->getLoc().compose(&locBuf);
      STBranchNode* hier = flow->getHier();
      hier->compose(&hierBuf, ".");
      flow->getDefNet()->getSymNode()->compose(&netBuf, ".");

      buf << hierBuf << " " << node->typeStr()
          << " Net: " << netBuf << "  " << locBuf;
      mInterpreter.blockComment(buf.c_str());
#ifdef INTERPRETER
      UInt32 offset = gCodeGen->getModuleOffset(hier);
#else
      UInt32 offset = 0;
      FLN_ELAB_ASSERT(0, flow);
#endif
    
      // If this block was used by multi scheduled flow nodes, then
      // call it, otherwise codegen it inline.
      BlockIntMap::iterator p = mBlockAddressMap.find(node);
      if (p == mBlockAddressMap.end())
      {
        // Single-use block
        FLN_ELAB_ASSERT(mBlockCountMap[node] == 1, flow);
        mInterpreter.instance(offset);
        compileUseDef(node);
      }
      else {
        mInterpreter.call(offset, p->second);
      }
    } // else
    mCurrentModuleElab = NULL;
  } // void compileFlow

private:
  // Evaluate an ident-rvalue expr and make sure the result is
  // in either A32 or A64, depending on the size of expr.
  void compileIdent(NUNet* net, Interpreter::Register reg)
  {
    storeFetchNet(net, reg, true);
  } // void compileIdent

  void storeFetchNet(NUNet* net, Interpreter::Register reg, bool isFetch,
                     const ConstantRange* range = NULL)
  {
    Interpreter::NetStorageMode mode = Interpreter::eNetStorage;
    UInt32 offset = 0;
    UInt32 netSize = net->getBitSize();
    NU_ASSERT(netSize <= 64, net);
    bool is64Bit = netSize > 32;

    if (net->isBlockLocal() || (net->getScope() == mTempModule)) {
      BlockLocalNetMap::iterator p = mBlockLocalNetMap.find(net);
      if (p == mBlockLocalNetMap.end()) {
        // local nets should be defined before use (I think)
        //ASSERT(!isFetch);
        if (is64Bit) {
          offset = mNumLocal64++;
          mMaxNumLocal64 = std::max(mMaxNumLocal64, mNumLocal64);
        }
        else {
          offset = mNumLocal32++;
          mMaxNumLocal32 = std::max(mMaxNumLocal32, mNumLocal32);
        }
        mBlockLocalNetMap[net] = offset;
      }
      else
        offset = p->second;
      mode = Interpreter::eNetBlockLocal;
    }
    else {
      OffsetMode om;
      bool found = findNetOffset(net, &om);
      NU_ASSERT(found, net);
      offset = om.first;
      mode = om.second;
      CGAuxInfo* cgaux = net->getCGOp();
      if (!net->isAllocated() && !cgaux->isPortAllocated() &&
          (mode == Interpreter::eNetStorage))
      {
        mode = Interpreter::eNetReference;
        NU_ASSERT(0, net);
      }
    }

    UtString buf;
    net->compose(&buf, NULL);
    if (isFetch) {
      mInterpreter.fetch(reg, offset, netSize, mode, buf.c_str());
      NU_ASSERT(range == NULL, net);
    }
    else {
      if (range == NULL)
        mInterpreter.store(reg, offset, netSize, mode, buf.c_str());
      else {
        // partselect assign
        mInterpreter.partselAssign(reg, offset, netSize, mode, *range,
                                   buf.c_str());
      }      
    }
  }

  void compileUnaryOp(const NUExpr* expr, const NUExpr* a,
                      Interpreter::OpCode op,
                      Interpreter::Register destReg)
  {
    bool is64bit = a->getBitSize() > 32;
    Interpreter::Register aReg = is64bit
      ? Interpreter::eRegA64
      : Interpreter::eRegA32;
    Interpreter::Register outReg = aReg;
    if (is64bit && Interpreter::isLogical(op))
      outReg = Interpreter::eRegA32;
    RegSaver rs(this);
    if (outReg != destReg)
      rs.save(outReg);
    NU_ASSERT(!mRegSave[aReg], a);
    compileExpr(a, aReg);
    UtString buf;
    expr->compose(&buf, NULL);
    UInt32 bitSize = expr->getBitSize();
    mInterpreter.addInstruction(op, bitSize, buf.c_str(),
                                requiresMasking(op, bitSize));
    if (outReg != destReg)
      move(outReg, destReg);
//    if (requiresMasking(op, bitSize))
//      mask(bitSize);
  } // void compileUnaryOp

  void compileVarsel(const NUVarselRvalue* pr, Interpreter::Register reg)
  {
    const NUExpr* ident = pr->getIdentExpr();
    UInt32 bitSize = ident->getBitSize();
    bool is64Bit = bitSize > 32;
    NU_ASSERT(bitSize <= 64, pr);

    ConstantRange r (*pr->getRange ());
    if (not pr->isConstIndex ())
      NU_ASSERT(0, pr);               // unhandled
      
    RegSaver rs(this);
    Interpreter::OpCode op = is64Bit ? Interpreter::ePartSel64
      : Interpreter::ePartSel32;
    Interpreter::Register aReg = is64Bit? Interpreter::eRegA64: Interpreter::eRegA32;
    if (reg != aReg)
      rs.save(aReg);
    NU_ASSERT(!mRegSave[aReg], pr);
    compileExpr(ident, aReg);
    UtString buf;
    pr->compose(&buf, NULL);
    mInterpreter.partselRvalue(op, r);
    if (reg != aReg)
      move(aReg, reg);
  } // void compileVarsel

/*
  void mask(UInt32 bitSize)
  {
    if (bitSize > 32) {
      ASSERT(bitSize <= 64);
      const NUExpr* save = push(Interpreter::eRegB64);
      UInt64 mask = (1ULL << bitSize) - 1ULL;
      mInterpreter.const64(mask, Interpreter::eRegB64);
      mInterpreter.opcode(Interpreter::eBitAnd64, "mask");
      pop(Interpreter::eRegB64, save);
    }
    else
    {
      const NUExpr* save = push(Interpreter::eRegB32);
      UInt32 mask = (1UL << bitSize) - 1UL;
      mInterpreter.const32(mask, Interpreter::eRegB32);
      mInterpreter.opcode(Interpreter::eBitAnd32, "mask");
      pop(Interpreter::eRegB32, save);
    }
  }
*/

  void lshift(bool is64Bit, UInt32 shiftSize)
  {
    if (is64Bit) {
      const NUExpr* save = push(Interpreter::eRegB64);
      mInterpreter.const64(shiftSize, Interpreter::eRegB64);
      mInterpreter.opcode(Interpreter::eLShift64, "shift");
      pop(Interpreter::eRegB64, save);
    }
    else
    {
      const NUExpr* save = push(Interpreter::eRegB32);
      mInterpreter.const32(shiftSize, Interpreter::eRegB32);
      mInterpreter.opcode(Interpreter::eLShift32, "shift");
      pop(Interpreter::eRegB32, save);
    }
  }

  bool requiresMasking(Interpreter::OpCode op, UInt32 /*bitSize*/)
  {
//    if ((bitSize % 32) == 0)
//      return false;
    return Interpreter::needsMask(op);
  } // bool requiresMasking

  bool regHasExpr(Interpreter::Register reg, const NUExpr* expr) {
    INFO_ASSERT(expr, "NULL expr.");
    if (mRegExprs[reg] == NULL)
      return false;
    if (mRegExprs[reg] == expr)
      return true;

    // We also use deep-compare, which will match two NUIdentRvalue's
    // of the same net & context-size.
    return (*mRegExprs[reg] == *expr);
  }

  void move(Interpreter::Register src, Interpreter::Register dst)
  {
    INFO_ASSERT(mRegExprs[src] != NULL, "Consistency check failed.");
    mRegExprs[dst] = mRegExprs[src];
    mInterpreter.move(src, dst);
  }

  const NUExpr* push(Interpreter::Register reg) {
    if (mRegSave[reg]) {
      if (Interpreter::is64BitReg(reg))
      {
        ++mCurrentStack64Depth;
        mMaxStack64Depth = std::max(mCurrentStack64Depth, mMaxStack64Depth);
      }
      else
      {
        ++mCurrentStack32Depth;
        mMaxStack32Depth = std::max(mCurrentStack32Depth, mMaxStack32Depth);
      }

      mRegSave[reg] = false;
      mInterpreter.push(reg);
      return mRegExprs[reg];
    }
    return NULL;
  }

  void pop(Interpreter::Register reg, const NUExpr* saved) {
    INFO_ASSERT(!mRegSave[reg], "Consistency check failed.");
    if (saved != NULL)
    {
      if (Interpreter::is64BitReg(reg)) {
        --mCurrentStack64Depth;
        INFO_ASSERT(mCurrentStack64Depth >= 0, "Stack64 underflow.");
      }
      else {
        --mCurrentStack32Depth;
        INFO_ASSERT(mCurrentStack32Depth >= 0, "Stack32 underflow.");
      }
      mRegSave[reg] = true;
      mRegExprs[reg] = saved;
      mInterpreter.pop(reg);
    }
  }

  bool swap(const NUExpr** a, const NUExpr** b)
  {
    const NUExpr* tmp = *a;
    *a = *b;
    *b = tmp;
    return true;
  }

  SInt32 getDepth(const NUExpr*)
  {
    return 0;
  }

  // binary operations, as far as the interpreter is concerned, include
  // all NUBinaryOp, bit-selects, and memory-reads.  They all require
  // compilation to compute both sides of the expression and keep track
  // of registers
  void compileBinaryOp(const NUExpr* expr,
                       const NUExpr* a, const NUExpr* b,
                       Interpreter::OpCode op,
                       Interpreter::Register destReg)
  {
    bool swapped = false;
    UInt32 aBitSize = a->getBitSize();
    UInt32 bBitSize = b->getBitSize();
    UInt32 bitSize = expr->getBitSize();
    bool commutative = Interpreter::isCommutative(op);
    Interpreter::Register outReg = Interpreter::eRegA32;
    Interpreter::Register aReg = Interpreter::eRegA32;
    Interpreter::Register bReg = Interpreter::eRegB32;

    // Most operations yield results that have the same
    // size as the 'a' operand.  The exception is bit-selects
    // of 64-bit variables, and all logical boolean operations,
    // which yield 32-bit results.
    if (aBitSize > 32) {
      aReg = Interpreter::eRegA64;
      if (!Interpreter::isLogical(op))
        outReg = Interpreter::eRegA64;

      if ((op != Interpreter::eBitSel64) &&
          (op != Interpreter::eLShift64) &&
          (op != Interpreter::eRShift64))
        bReg = Interpreter::eRegB64;
    }
    if (bBitSize > 32) {
      if (!Interpreter::isLogical(op))
        outReg = Interpreter::eRegA64;
      aReg = Interpreter::eRegA64;
      bReg = Interpreter::eRegB64;
    }

    // If the register that will be written by our operand is not
    // the one desired by the caller, then save any data in the register
    // that is in the register we will temporarily write.
    RegSaver regSave(this);
    if (destReg != outReg)
      regSave.save(outReg);
    else if (destReg != aReg)
      regSave.save(aReg);
    regSave.save(bReg);

    bool computeA = true;
    bool computeB = true;

    // When the user writes:
    //    a = x + k;
    //    b = y + k;
    // after the first statement executes, we are left with the
    // net we want in regB so we can just keep it there.  
    if (regHasExpr(aReg, a))
      computeA = false;
    if (regHasExpr(bReg, b))
      computeB = false;
    if (computeA || computeB)
    {
      if (regHasExpr(aReg, b))
      {
        if (commutative)
        {
          swapped = swap(&a, &b);
          NU_ASSERT(computeA, b);
          computeA = false;
          computeB = true;
        }
        else
        {
          move(aReg, bReg);
          computeB = false;
        }
      }
      else if (regHasExpr(bReg, a))
      {
        if (commutative)
        {
          swapped = swap(&a, &b);
          NU_ASSERT(computeB, a);
          computeA = true;
          computeB = false;
        }
        else
        {
          move(bReg, aReg);
          computeA = false;
        }
      }
    }

    // If we are not computing one of our input registers, then we must
    // save it while computing the other one.
    mRegSave[aReg] = !computeA;
    mRegSave[bReg] = !computeB;

    // Compute any args that need computing, going after the
    // higher-depth expressions first, which should reduce
    // the stack.  consider
    //     x1 + (x2 + (x3 + (x4 + (x5 + x6))))
    // Here we want to always compute operand b first, which will
    // require no stack depth.  
    //
    //      Compute Operand b first      Compute Operand a first
    //      -----------------------      -----------------------
    //      load x5 into a               load x1 into a
    //      load x6 into b               push a           
    //      add                          load x2 into a
    //      load x4 into b               push a          
    //      add                          load x3 into a
    //      load x3 into b               push a          
    //      add                          load x4 into a
    //      load x2 into b               push a          
    //      add                          load x5 into a
    //      load x1 into b               load x6 into b          
    //      add                          add
    //                                   pop b
    //                                   add     
    //                                   pop b     
    //                                   add     
    //                                   pop b     
    //                                   add     
    //                                   pop b     
    //                                   add     
    //
    if (getDepth(a) < getDepth(b))
    {
      if (computeB)
      {
        compileExpr(b, bReg);
        mRegSave[bReg] = computeA;
      }
      if (computeA)
      {
        NU_ASSERT(mRegSave[bReg], b);
        compileExpr(a, bReg);
      }
    }
    else
    {
      if (computeA)
      {
        compileExpr(a, aReg);
        mRegSave[aReg] = computeB;
      }
      if (computeB)
      {
        NU_ASSERT(mRegSave[aReg], a);
        compileExpr(b, bReg);
      }
    }

    UtString buf;
    expr->compose(&buf, NULL);
    mInterpreter.addInstruction(op, bitSize, buf.c_str(),
                                requiresMasking(op, bitSize));
    mRegSave[aReg] = false;
    mRegSave[bReg] = false;

/*
    if (requiresMasking(op, bitSize))
      mask(bitSize);
*/

    mRegExprs[outReg] = expr;
    if (outReg != destReg)
      move(outReg, destReg);
  } // void compileBinaryOp

  // Compiling a conditional expression is done with exactly the same
  // code as "if", except that the blocks we compile don't have any
  // side effects except the value they leave in "reg".
  void compileCondOp(NUTernaryOp* ternop, Interpreter::Register reg) {
    NU_ASSERT(ternop->getOp() == NUOp::eTeCond, ternop);
    bool is64Bit, swap;
    compileCond(ternop->getArg(0), &is64Bit, &swap);

    // If the then/else expressions are simple variables or constants,
    // they can be loaded directly into b32, and computed as a mux
    // e.g.  cond?thenExpr:elseExpr would get rendered as
    //     condmask=((cond)==0)-1     // generates 0x00000000 or 0xFFFFFFFF
    //     (~condmask & elseExpr) | (condmask & thenExpr);
    // or, rendered as instructions for 2 registers:
    //     compute (cond==0)-1 -> b32
    //     load thenExpr into b32
    //     compile a -> a32
    //     push a32
    //     compile b -> a32
    //     pop b32 (b32 now has cond)
    //
    

    Interpreter::Block* thenBlock = mInterpreter.pushBlock();
    Interpreter::Block* elseBlock = mInterpreter.pushBlock();

    // compiling the else-block before the if block results in the
    // addresses in the comment file coming out right.  To make this
    // more robust, we would need to keep the comments in some more
    // structured form and correct the addresses as the precompiled
    // blocks get copied into the code-file.
    if (swap) {
      compileExpr(ternop->getArg(1), reg);
      mInterpreter.popBlock(elseBlock);
      compileExpr(ternop->getArg(2), reg);
    }
    else {
      compileExpr(ternop->getArg(2), reg);
      mInterpreter.popBlock(elseBlock);
      compileExpr(ternop->getArg(1), reg);
    }      
    mInterpreter.popBlock(thenBlock);

    mInterpreter.addIf(is64Bit, thenBlock, elseBlock);
  } // void compileCondOp

/*
  void concatAssign(NUConcatLvalue* concat, NUExpr* expr) {
  }
*/

  void compileConcat(NUConcatOp* concat, Interpreter::Register destReg)
  {
    UInt32 bitSize = concat->getBitSize();
    Interpreter::Register aReg = Interpreter::eRegA32;
    Interpreter::Register bReg = Interpreter::eRegB32;
    Interpreter::OpCode op = Interpreter::eConcat32;
    if (bitSize > 32) {
      aReg = Interpreter::eRegA64;
      bReg = Interpreter::eRegB64;
      op = Interpreter::eConcat64;
    }

    RegSaver regSave(this);
    if (destReg != aReg)
      regSave.save(aReg);

    // Transform {a,b,c} into (((a<<bsize)|b)<<csize)|c
    // multiply-concats will have to be handled more cleverly.
    SInt32 n = concat->getNumArgs();
    NU_ASSERT(n > 0, concat);
    NUExpr* expr = concat->getArg(0);

    // We want to collect the concatenation values in register A,
    // so start out by putting the high-order expression there.
    compileExpr(expr, aReg);

    // Now issue concat expressions, which do (a32 = (a32<<bsize) | b32)
    for (SInt32 i = 1; i < n; ++i) {
      NUExpr* e = concat->getArg(i);
      compileExpr(e, bReg);
      mInterpreter.addInstruction(op, e->getBitSize());
    }

    mRegExprs[destReg] = concat;
    if (aReg != destReg)
      move(aReg, destReg);
  } // void compileConcat

  void clearExprs(Interpreter::Register reg) {
    if ((reg == Interpreter::eRegA32) || (reg == Interpreter::eRegB32)) {
      mRegExprs[Interpreter::eRegA32] = NULL;
      mRegExprs[Interpreter::eRegB32] = NULL;
    }
    else {
      mRegExprs[Interpreter::eRegA64] = NULL;
      mRegExprs[Interpreter::eRegB64] = NULL;
    }
  }
    
#if 0
  void compileConcat(NUConcatOp* concat, Interpreter::Register reg)
  {
    SInt32 shift = 0;
    UInt32 bitSize = concat->getBitSize();
    NU_ASSERT(bitSize <= 64, concat);
    bool is64Bit = bitSize > 32;
    Interpreter::Register aReg = Interpreter::eRegA32;
    Interpreter::Register bReg = Interpreter::eRegB32;
    Interpreter::OpCode orOp = Interpreter::eBitAnd32;
    if (is64Bit) {
      aReg = Interpreter::eRegA64;
      bReg = Interpreter::eRegB64;
      orOp = Interpreter::eBitAnd64;
    }

    // First, compute and concatenate the operands
    bool cont = true;
    const NUExpr* save = NULL;
    for (UInt32 j = 0; cont; ++j) {
      NUExpr* expr = concat->getArg(j);
      UInt32 nextShift = shift + expr->getBitSize();
      cont = (j < concat->getNumArgs()) && (nextShift < bitSize);
      save = push(aReg, concat);
      compileExpr(expr, aReg);
      if (shift != 0)
      {
        lshift(is64Bit, shift);
        NU_ASSERT(save != NULL, expr);
        pop(bReg, save);
        mInterpreter.opcode(orOp, "or");
        if (cont)
          save = push(aReg);
      }
      else
      {
        NU_ASSERT(save == NULL, expr);
        NU_ASSERT(!mRegSave[aReg], expr);
        mRegSave[aReg] = true;
      }
      shift = nextShift;
    }

    // Now, replicate that operand according to the concat multiplier...later
    NU_ASSERT(concat->getRepeatCount() == 1, concat);
    if (reg != aReg)
      move(aReg, reg);
  } // void compileConcat
#endif

  Interpreter::OpCode translateOp32(NUOp::OpT op,
                                    bool* doSwap,
                                    Interpreter::OpCode* negate)
  {
    switch (op)
    {
    case NUOp::eUnBuf:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eUnPlus:     INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eUnMinus:    return Interpreter::eNegate32;
    case NUOp::eUnLogNot:   return Interpreter::eLogNot32;
    case NUOp::eUnBitNeg:   return Interpreter::eBitNot32;
    case NUOp::eUnVhdlNot:  return Interpreter::eBitNot32;
    case NUOp::eUnRedAnd:   return Interpreter::eEnd; // changed to ==
    case NUOp::eUnRedOr:    return Interpreter::eReductOr32;
    case NUOp::eUnRedXor:   return Interpreter::eReductXor32;
    case NUOp::eUnCount:    INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eUnAbs:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eUnFFZ:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eUnFFO:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eUnFLZ:
    case NUOp::eUnFLO:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eUnRound:
    case NUOp::eUnItoR:
    case NUOp::eUnRtoI:
    case NUOp::eUnRealtoBits:
    case NUOp::eUnBitstoReal:
    case NUOp::eUnChange:   INFO_ASSERT(0, "Unhandled NUOp.");

    case NUOp::eBiPlus:     return Interpreter::eAdd32; break;
    case NUOp::eBiMinus:    return Interpreter::eSub32; break;
    case NUOp::eBiSMult:     return Interpreter::eMul32; break;
    case NUOp::eBiSDiv:      return Interpreter::eDiv32; break;
    case NUOp::eBiSMod:      return Interpreter::eMod32; break;
    case NUOp::eBiUMult:     return Interpreter::eMul32; break;
    case NUOp::eBiUDiv:      return Interpreter::eDiv32; break;
    case NUOp::eBiUMod:      return Interpreter::eMod32; break;
    case NUOp::eBiEq:       return Interpreter::eLogEq32; break;
    case NUOp::eBiNeq:
      *negate = Interpreter::eLogNot32;
      return Interpreter::eLogEq32;
    case NUOp::eBiTrieq:    INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiTrineq:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiLogAnd:   return Interpreter::eLogAnd32; break;
    case NUOp::eBiLogOr:    return Interpreter::eLogOr32; break;
    case NUOp::eBiSLt:       return Interpreter::eLogLt32; break;
    case NUOp::eBiSLte:      return Interpreter::eLogLte32; break;
    case NUOp::eBiSGtr:      *doSwap = true; return Interpreter::eLogLt32;
    case NUOp::eBiSGtre:     *doSwap = true; return Interpreter::eLogLte32;
    case NUOp::eBiULt:       return Interpreter::eLogLt32; break;
    case NUOp::eBiULte:      return Interpreter::eLogLte32; break;
    case NUOp::eBiUGtr:      *doSwap = true; return Interpreter::eLogLt32;
    case NUOp::eBiUGtre:     *doSwap = true; return Interpreter::eLogLte32;
    case NUOp::eBiBitAnd:   return Interpreter::eBitAnd32; break;
    case NUOp::eBiBitOr:    return Interpreter::eBitOr32; break;
    case NUOp::eBiBitXor:   return Interpreter::eBitXor32; break;
    case NUOp::eBiRshift:   return Interpreter::eRShift32; break;
    case NUOp::eBiLshift:   return Interpreter::eLShift32; break;

    case NUOp::eTeCond:     INFO_ASSERT(0, "Unhandled NUOp."); // return Interpreter::eTernary32; break;
    case NUOp::eBiRoR:      INFO_ASSERT(0, "Unhandled NUOp."); 
    case NUOp::eBiRoL:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiExp:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiDExp:     INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiVhdlMod:  INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eNaConcat:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiVhZxt:
    case NUOp::eBiVhExt:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiVhLshift:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiVhRshift:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiVhRshiftArith:
    case NUOp::eBiVhLshiftArith:
    case NUOp::eBiRshiftArith:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiLshiftArith:   INFO_ASSERT(0, "Unhandled NUOp.");

    case NUOp::eZEndFile:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eZSysTime:   INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eZSysStime:  INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eZSysRealTime: INFO_ASSERT(0, "Unhandled NUOp.");

    case NUOp::eStart:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eNaFopen:    INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eNaLut:      INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eInvalid:    INFO_ASSERT(0, "Unhandled NUOp.");
    case NUOp::eBiDownTo:   INFO_ASSERT(0, "Unhandled NUOp.");
    } // switch
    INFO_ASSERT(0, "Unhandled NUOp.");
    return Interpreter::eEnd;
  } // Interpreter::OpCode translateOp

  Interpreter::OpCode translateOp64(NUOp::OpT op,
                                    bool* doSwap,
                                    Interpreter::OpCode* negate)
  {
    switch (op)
    {
    case NUOp::eUnBuf:      INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eUnPlus:     INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eUnMinus:    return Interpreter::eNegate64;
    case NUOp::eUnLogNot:   return Interpreter::eLogNot64;
    case NUOp::eUnBitNeg:   return Interpreter::eBitNot64;
    case NUOp::eUnVhdlNot:  return Interpreter::eBitNot64;
    case NUOp::eUnRedAnd:   return Interpreter::eEnd; // changed to ==
    case NUOp::eUnRedOr:    return Interpreter::eReductOr64;
    case NUOp::eUnRedXor:   return Interpreter::eReductXor64;
    case NUOp::eUnCount:    INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eUnAbs:      INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eUnFLZ:
    case NUOp::eUnFLO:
    case NUOp::eUnFFZ:      INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eUnFFO:      INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eUnRound:
    case NUOp::eUnItoR:
    case NUOp::eUnRtoI:
    case NUOp::eUnRealtoBits:
    case NUOp::eUnBitstoReal:
    case NUOp::eUnChange:     INFO_ASSERT(0, "Unhandled NUOp");

    case NUOp::eBiPlus:     return Interpreter::eAdd64; break;
    case NUOp::eBiMinus:    return Interpreter::eSub64; break;
    case NUOp::eBiSMult:     return Interpreter::eMul64; break;
    case NUOp::eBiSDiv:      return Interpreter::eDiv64; break;
    case NUOp::eBiSMod:      return Interpreter::eMod64; break;
    case NUOp::eBiUMult:     return Interpreter::eMul64; break;
    case NUOp::eBiUDiv:      return Interpreter::eDiv64; break;
    case NUOp::eBiUMod:      return Interpreter::eMod64; break;
    case NUOp::eBiEq:       return Interpreter::eLogEq64; break;
    case NUOp::eBiNeq:
      *negate = Interpreter::eLogNot32;
      return Interpreter::eLogEq64;
    case NUOp::eBiTrieq:    INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiTrineq:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiLogAnd:   return Interpreter::eLogAnd64; break;
    case NUOp::eBiLogOr:    return Interpreter::eLogOr64; break;
    case NUOp::eBiSLt:       return Interpreter::eLogLt64; break;
    case NUOp::eBiSLte:      return Interpreter::eLogLte64; break;
    case NUOp::eBiSGtr:      *doSwap = true; return Interpreter::eLogLt64;
    case NUOp::eBiSGtre:     *doSwap = true; return Interpreter::eLogLte64;
    case NUOp::eBiULt:       return Interpreter::eLogLt64; break;
    case NUOp::eBiULte:      return Interpreter::eLogLte64; break;
    case NUOp::eBiUGtr:      *doSwap = true; return Interpreter::eLogLt64;
    case NUOp::eBiUGtre:     *doSwap = true; return Interpreter::eLogLte64;
    case NUOp::eBiBitAnd:   return Interpreter::eBitAnd64; break;
    case NUOp::eBiBitOr:    return Interpreter::eBitOr64; break;
    case NUOp::eBiBitXor:   return Interpreter::eBitXor64; break;
    case NUOp::eBiRshift:   return Interpreter::eRShift64; break;
    case NUOp::eBiLshift:   return Interpreter::eLShift64; break;

    case NUOp::eTeCond:     INFO_ASSERT(0, "Unhandled NUOp"); // return Interpreter::eTernary64; break;

    case NUOp::eNaConcat:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eNaLut:      INFO_ASSERT(0, "Unhandled NUOp");

    case NUOp::eZEndFile:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eZSysTime:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eZSysStime:  INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eZSysRealTime: INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiRoR: INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiRoL: INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiExp: INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiDExp:INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiVhdlMod: INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eStart:      INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eNaFopen:    INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiVhZxt:
    case NUOp::eBiVhExt:    INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiVhLshift:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiVhRshift:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiVhRshiftArith:
    case NUOp::eBiVhLshiftArith:
    case NUOp::eBiRshiftArith:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiLshiftArith:   INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eInvalid:    INFO_ASSERT(0, "Unhandled NUOp");
    case NUOp::eBiDownTo:   INFO_ASSERT(0, "Unhandled NUOp");
    } // switch
    INFO_ASSERT(0, "Unhandled NUOp");
    return Interpreter::eEnd;
  } // Interpreter::OpCode translateOp

  // When compiling expressions into a register, if there is good data
  // that we need to avoid clobbering, then class RegSaver
  // helps manage the stack operations for this in a timely manner.
  class RegSaver {
    typedef std::pair<Interpreter::Register,const NUExpr*> RegExpr;
  public:
    RegSaver(InterpPopulator* populator) {
      mPopulator = populator;
    }

    void save(Interpreter::Register reg) {
      const NUExpr* save = mPopulator->push(reg);
      if (save != NULL)
        mSaved.push_back(RegExpr(reg, save));
    }

    ~RegSaver() {
      for (SInt32 i = mSaved.size() - 1; i >= 0; --i)
      {
        RegExpr& re = mSaved[i];
        mPopulator->pop(re.first, re.second);
      }
    }

/*
    void save(Interpreter::Register reg) {
      if (mReg == Interpreter::eRegB32)
        mSaved = mPopulator->push(Interpreter::eRegA32);
      else if (mReg == Interpreter::eRegB64)
        mSaved = mPopulator->push(Interpreter::eRegA64);
    }

    ~RegSaver() {
      if (mReg == Interpreter::eRegB32) {
        mPopulator->move(Interpreter::eRegA32, Interpreter::eRegB32);
        mPopulator->pop(Interpreter::eRegA32, mSaved);
      }
      else if (mReg == Interpreter::eRegB64) {
        mPopulator->move(Interpreter::eRegA64, Interpreter::eRegB64);
        mPopulator->pop(Interpreter::eRegA64, mSaved);
      }
    }
*/

    InterpPopulator* mPopulator;
    UtVector<RegExpr> mSaved;
  };
  friend class RegSaver;

  void compileExpr(const NUExpr* expr, Interpreter::Register reg)
  {
    // if we already have an equivalent expression, then we can return.
    if (regHasExpr(reg, expr))
      return;

    switch (expr->getType())
    {
    case NUExpr::eNUIdentRvalue:
      compileIdent(((NUIdentRvalue*)expr)->getIdent(), reg);
      break;
    case NUExpr::eNUTernaryOp:
      compileCondOp((NUTernaryOp*)expr, reg);
      break;
    case NUExpr::eNUBinaryOp: {
      const NUBinaryOp* bop = (const NUBinaryOp*)expr;
      const NUExpr* a = bop->getArg(0);
      const NUExpr* b = bop->getArg(1);
      UInt32 aSize = a->getBitSize();
      UInt32 bSize = b->getBitSize();
      NU_ASSERT(aSize <= 64, a);
      NU_ASSERT(bSize <= 64, b);
      bool aIs64 = aSize > 32;
      //bool bIs64 = bSize > 32;
      bool doSwap = false;
      Interpreter::OpCode negate = Interpreter::eEnd;
      NUOp::OpT op = bop->getOp();
      Interpreter::OpCode opCode = aIs64
        ? translateOp64(op, &doSwap, &negate)
        : translateOp32(op, &doSwap, &negate);
      if (doSwap)
        swap(&a, &b);

      compileBinaryOp(expr, a, b, opCode, reg);

      if (negate != Interpreter::eEnd)
        mInterpreter.opcode(negate, "inversion");

      break;
    } // case NUExpr::eNUBinaryOp:
    case NUExpr::eNUUnaryOp: {
      NUUnaryOp* uop = (NUUnaryOp*)expr;
      NUExpr* a = uop->getArg(0);
      UInt32 bitSize = a->getBitSize();
      bool is64Bit = bitSize > 32;
      NU_ASSERT(bitSize <= 64, a);
      bool doSwap = false;
      NUOp::OpT nuop = uop->getOp();
      Interpreter::OpCode negate = Interpreter::eEnd;
      Interpreter::OpCode opCode = is64Bit
        ? translateOp64(nuop, &doSwap, &negate)
        : translateOp32(nuop, &doSwap, &negate);
      NU_ASSERT(negate == Interpreter::eEnd, a);
      NU_ASSERT(!doSwap, a);

      // reduction-and gets converted into logical
      // equality.  The op-code translator uses eEnd to signify that.
      if (opCode == Interpreter::eEnd) {
        NU_ASSERT(nuop == NUOp::eUnRedAnd, uop);
        NUExpr* k = NUConst::create(false,
                                    (1ULL<<bitSize) - 1, // all 1s
                                    bitSize,
                                    uop->getLoc());
        compileBinaryOp(uop, a, saveExpr(k, false),
                        (is64Bit
                         ? Interpreter::eLogEq64
                         : Interpreter::eLogEq32),
                        reg);
        mRegExprs[is64Bit? Interpreter::eRegB64: Interpreter::eRegB32] = NULL;
      }
      else
        compileUnaryOp(expr, a, opCode, reg);
      mRegExprs[reg] = expr;
      break;
    }
    case NUExpr::eNUVarselRvalue: {
      const NUVarselRvalue* br = (const NUVarselRvalue*)expr;
      const NUExpr* ident = br->getIdentExpr();
      UInt32 bitSize = ident->getBitSize();
      bool is64Bit = bitSize > 32;
      NU_ASSERT(bitSize <= 64, br);
      if (br->isConstIndex()) {
        ConstantRange r (*br->getRange ());

        if (r.getLength () != 1) // Handle constant multi-bit access
        {
          compileVarsel (br, reg);
          break;
        }

        compileExpr(ident, is64Bit? Interpreter::eRegA64: Interpreter::eRegA32);
        mInterpreter.addInstruction((is64Bit? Interpreter::eKBitSel32:
                                     Interpreter::eKBitSel32),
                                    r.getLsb ());
      }
      else
        compileBinaryOp(expr, ident, br->getIndex(), 
                        is64Bit? Interpreter::eBitSel64: Interpreter::eBitSel32,
                        reg);
      break;
    }

    case NUExpr::eNUConstNoXZ:
    case NUExpr::eNUConstXZ: {
      NUConst* c = (NUConst*) expr;
      UInt32 bitSize = c->getBitSize();
      NU_ASSERT(bitSize <= 64, c);
      UInt64 val;
      c->getULL(&val);
      if ((reg == Interpreter::eRegA64) || (reg == Interpreter::eRegB64))
        mInterpreter.const64(val, reg);
      else {
        if (bitSize > 32)
          NU_ASSERT((val | 0xFFFFFFFF) == 0xFFFFFFFF, c);
        mInterpreter.const32((UInt32) val, reg);
      }
      break;
    }
    case NUExpr::eNUConcatOp:
      compileConcat((NUConcatOp*)expr, reg);
      break;
    default: {
      UtString buf;
      expr->compose(&buf, NULL);
      UtIO::cerr() << "Cannot handle expression " << buf << "\n";
      mFailure = true;
    }
    } // switch
    mRegExprs[reg] = expr;
  } // void compileExpr

  void compileStatement(NUStmt* stmt)
  {
    UtString buf;
    stmt->compose(&buf, NULL, 2, true);
    mInterpreter.blockComment(buf.c_str());

    NU_ASSERT(mCurrentStack32Depth == 0, stmt);
    NU_ASSERT(mCurrentStack64Depth == 0, stmt);

    // For now, just do blocking assignments
    switch (stmt->getType()) {
    case eNUIf:
      compileIf((NUIf*) stmt);
      break;
    case eNUBlockingAssign:
      compileAssign((NUBlockingAssign*)(stmt));
      break;
    case eNUCase:
      compileCase((NUCase*)(stmt));
      break;
    default: {
      UtString buf;
      stmt->compose(&buf, NULL);
      UtIO::cerr() << "Unhandled statement: " << buf << "\n";
      mFailure = true;
    }
    }
  }

  bool isZero(const NUExpr* expr) {
    const NUConst* k = expr->castConst();
    if ((k != NULL) && !k->hasXZ() && k->isZero())
      return true;
    return false;
  }

  void compileCond(const NUExpr* cond, bool* is64Bit, bool* swap) {
    // folding sometimes transforms
    //   if (cond) a; else b;    -->   if (cond == 0) b; else a;
    // or
    //   if (cond) a; else b;    -->   if (cond != 0) a; else b;
    // transform it back.  The interpreter implements !=0 itself
    // we don't need to codegen it.
    const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(cond);
    *swap = false;
    if ((bop != NULL) && isZero(bop->getArg(1))) {
      if (bop->getOp() == NUOp::eBiEq) {
        *swap = true;
        cond = bop->getArg(0);
      }
      else if (bop->getOp() == NUOp::eBiNeq) {
        cond = bop->getArg(0);
      }
    }

    UInt32 condBitSize = cond->getBitSize();
    NU_ASSERT(condBitSize <= 64, cond);
    *is64Bit = condBitSize > 32;
    Interpreter::Register aReg = *is64Bit
      ? Interpreter::eRegA64
      : Interpreter::eRegA32;
    compileExpr(cond, aReg);
  }

  void compileIf(NUIf* ifStmt) {
    bool is64Bit, swap;
    compileCond(ifStmt->getCond(), &is64Bit, &swap);

    Interpreter::Block* thenBlock = mInterpreter.pushBlock();
    Interpreter::Block* elseBlock = mInterpreter.pushBlock();

    // compiling the else-block before the if block results in the
    // addresses in the comment file coming out right.  To make this
    // more robust, we would need to keep the comments in some more
    // structured form and correct the addresses as the precompiled
    // blocks get copied into the code-file.
    if (swap) {
      compileStmtLoop(ifStmt->loopThen(), true);
      mInterpreter.popBlock(elseBlock);
      compileStmtLoop(ifStmt->loopElse(), true);
    }
    else {
      compileStmtLoop(ifStmt->loopElse(), true);
      mInterpreter.popBlock(elseBlock);
      compileStmtLoop(ifStmt->loopThen(), true);
    }
    mInterpreter.popBlock(thenBlock);

    mInterpreter.addIf(is64Bit, thenBlock, elseBlock);
  }

  // A simple expression is a constant or variable reference
  bool isSimpleExpr(const NUExpr* expr) {
    switch (expr->getType()) {
    case NUExpr::eNUIdentRvalue: return true;
    case NUExpr::eNUConstNoXZ:   return true;
    case NUExpr::eNUConstXZ:     return true;
    default:
      break;
    }
    return false;
  }

  void compileCase(NUCase* caseStmt) {
    // For now, code all case statements as an else-if tree.
    const NUExpr* sel = caseStmt->getSelect();
    if (!isSimpleExpr(sel))
      sel = tempifyExpr(sel, sel->getBitSize());
    NUCase::ItemLoop i = caseStmt->loopItems();
    compileCaseItem(sel, &i);
  }

  void compileCaseItem(const NUExpr* sel, NUCase::ItemLoop* i) {
    if (!i->atEnd()) {
      NUCaseItem* item = **i;
      ++(*i);

      // Build a big OR of all conditions
      NUExpr* expr = NULL;
      for (NUCaseItem::ConditionLoop c = item->loopConditions(); !c.atEnd();
           ++c)
      {
        NUCaseCondition* cc = *c;
        NUExpr* cond = cc->buildConditional(sel);
        cond->resize(1);
        if (expr == NULL)
          expr = cond;
        else
        {
          expr = new NUBinaryOp(NUOp::eBiBitOr, expr, cond, cond->getLoc());
          expr->resize(1);
        }
      }

      if (expr == NULL) {
        // default case?
        compileStmtLoop(item->loopStmts(), true);
      }
      else {
        bool is64Bit, swap;
        compileCond(saveExpr(expr, true), &is64Bit, &swap);

        Interpreter::Block* thenBlock = mInterpreter.pushBlock();
        Interpreter::Block* elseBlock = mInterpreter.pushBlock();

        // Recursive call to the compileItems to compile the remaining
        // case items as part of the "else" block.  Note that in
        // IF statements we emit the ELSE statements before the THEN
        // statements.
        if (swap) {
          compileStmtLoop(item->loopStmts(), true);
          mInterpreter.popBlock(elseBlock);
          compileCaseItem(sel, i);
        }
        else {
          compileCaseItem(sel, i);
          mInterpreter.popBlock(elseBlock);
          compileStmtLoop(item->loopStmts(), true);
        }

        mInterpreter.popBlock(thenBlock);

        mInterpreter.addIf(is64Bit, thenBlock, elseBlock);
      }
    } // if
  } // void compileCaseItem

  void compileAssign(NUAssign* assign)
  {
    NULvalue* lv = assign->getLvalue();
    NUConcatLvalue* cl = dynamic_cast<NUConcatLvalue*>(lv);
    if (cl != NULL) {
      concatAssign(cl, assign->getRvalue());
      return;
    }
    NUVarselLvalue* pl = dynamic_cast<NUVarselLvalue*>(lv);
    NUNet* ident = NULL;
    ConstantRange range (0,0);
    if (pl != NULL) {
      if (pl->isWholeIdentifier()) {
        ident = pl->getWholeIdentifier();
        range.setMsb (ident->getBitSize ()-1);
      }
      else if (!pl->isArraySelect())
      {
        ident = pl->getIdentNet();
        range = *pl->getRange();
      }
    }
          
    NUIdentLvalue* id = dynamic_cast<NUIdentLvalue*>(lv);
    if (id != NULL)
      ident = id->getIdent();
    if (ident == NULL)
    {
      UtString buf;
      lv->compose(&buf, NULL, 0, true);
      UtIO::cerr() << "Cannot handle complex assigns yet: " << buf << "\n";
      mFailure = true;
    }
    else {
      UInt32 bitSize = lv->getBitSize();
      NU_ASSERT(bitSize <= 64, assign);
      bool is64Bit = bitSize > 32;
      Interpreter::Register reg = is64Bit? Interpreter::eRegA64
        : Interpreter::eRegA32;
      compileExpr(assign->getRvalue(), reg);
      storeFetchNet(ident, reg, false, &range);
    }
  } // void compileAssign

  const NUExpr* tempifyExpr(const NUExpr* expr, UInt32 size) {
    UtString buf("$t");
    buf << mTempNetCount++;
    StringAtom* sym = mAtomicCache->intern(buf.c_str(), buf.size());
    const SourceLocator& loc = expr->getLoc();
    NUNet* tempNet = mTempModule->createTempNet(sym, size, 
                                                expr->isSignedResult(), loc);
    CopyContext cc(0, 0);
    NUExpr* exprCpy = expr->copy(cc);
    exprCpy->resize(size);
    NUContAssign tmpAssign(NULL, new NUIdentLvalue(tempNet, loc),
                           const_cast<NUExpr*>(saveExpr(exprCpy, true)), loc);
    compileAssign(&tmpAssign);
    NUExpr* tmpExpr = new NUIdentRvalue(tempNet, loc);
    tmpExpr->resize(size);
    return saveExpr(tmpExpr, false);
  }

  void concatAssign(NUConcatLvalue* cl, const NUExpr* expr) {
    UInt32 bitSize = cl->getBitSize();
    NU_ASSERT(bitSize <= 64, cl);
    Interpreter::Register aReg = Interpreter::eRegA32;
    Interpreter::Register bReg = Interpreter::eRegB32;
    Interpreter::OpCode op = Interpreter::ePartSelBA32;
    if (bitSize > 32) {
      aReg = Interpreter::eRegA64;
      bReg = Interpreter::eRegB64;
      op = Interpreter::ePartSelBA64;
    }
    SInt32 shift = 0;

    // There are two strategies for dealing with "{a,b} = e;"
    // If e is an identifier, and is sized exactly the same {a,b},
    // then we can transform it to "b=e&((1<<bsize)-1); a = e>>bsize;".
    // Note that this references e twice, so if e is complex this is
    // suboptimal.  Also, if e is smaller size than {a,b}, then it
    // potentially will yield wrong answers, and we need a temp.

#if 0
    const NUIdentRvalue* ident = dynamic_cast<const NUIdentRvalue*>(expr);
    CopyContext cc(NULL, NULL);
    const SourceLocator& loc = expr->getLoc();
    if ((ident != NULL) && (ident->getBitSize() == cl->getBitSize())) {
      // ident sized correctly --> don't need a temp
    }
    else {
      // Need a temp.  Create it in nucleus (but don't add it to module)
      expr = tempifyExpr(expr, cl->getBitSize());
    }
#endif
    compileExpr(expr, bReg);

    for (SInt32 i = cl->getNumArgs() - 1; i >= 0; --i) {
      NULvalue* lv = cl->getArg(i);
      UInt32 lvSize = lv->getBitSize();

#ifdef INTERP_BYTE_STREAM
      NU_ASSERT((shift & ((1 << 8) - 1)) == shift, lv);
      NU_ASSERT((lvSize & ((1 << 8) - 1)) == lvSize, lv);
      mInterpreter.opcode(op);
      mInterpreter.pushInt(shift, 1, "position");
      mInterpreter.pushInt(lvSize, 1, "numBits");
#else
      UtString buf;
      buf << "shift=" << shift << " mask={" << lvSize << "{1'b1}}";
      NU_ASSERT((shift & ((1 << 12) - 1)) == shift, lv);
      NU_ASSERT((lvSize & ((1 << 12) - 1)) == lvSize, lv);
      UInt32 shiftMask = shift | (lvSize << 12);
      mInterpreter.addInstruction(op, shiftMask, buf.c_str());
#endif
      storeFetchNet(lv->getWholeIdentifier(), aReg, false);
#if 0
      // Shift off the bits we want
      if (shift != 0) {
        eSize -= shift;
        NUConst* k = NUConst::create(false, shift, 32, loc);
        k->resize(lvSize);
        NUExpr* shiftExpr = new NUBinaryOp(NUOp::eBiRshift, e, saveExpr(k, false),
                                           loc);
        shiftExpr->resize(lvSize);
        e = saveExpr(shiftExpr, false);
      }

      // Mask off the bits we want from the expression
      if (eSize > lvSize) {
        UInt64 mask = ((1ULL) << lvSize) - 1;
        NUConst* k = NUConst::create(false, mask, lvSize, loc);
        k->resize(lvSize);
        NUExpr* maskExpr = new NUBinaryOp(NUOp::eBiBitAnd, e, saveExpr(k, false),
                                          loc);
        maskExpr->resize(lvSize);
        e = saveExpr(maskExpr, false);
      }

      NUContAssign tmpAssign(NULL, lv->copy(cc), const_cast<NUExpr*>(e), loc);
      compileAssign(&tmpAssign);
      //clearRegs();
#endif

      shift += lvSize;
    }
  }

  void compileStmtLoop(NUStmtLoop p, bool bclearRegs) {
    if (bclearRegs)
      clearRegs();
    for (; !p.atEnd(); ++p)
    {
      NUStmt* stmt = *p;
      compileStatement(stmt);
    }

    if (bclearRegs)
      clearRegs();
  }

  void compileStructuredProc(NUStructuredProc* sp)
  {
    NUBlock* block = sp->getBlock();
    compileStmtLoop(block->loopStmts(), false);
  }

  void compileUseDef(NUUseDefNode* block)
  {
    // We expect this to either be an always-block or a continuous assign.
    mBlockLocalNetMap.clear();
    mNumLocal32 = 0;
    mNumLocal64 = 0;

    UtString buf;
    block->compose(&buf, NULL, 2, true);
    mInterpreter.blockComment(buf.c_str());
#ifdef INTERP_PROFILE
    mInterpreter.addInstruction(Interpreter::eProfile,
                                mBlockProfileIndexMap[block]);
#endif

    NUStructuredProc* sp = dynamic_cast<NUStructuredProc*>(block);
    if (sp != NULL)
      compileStructuredProc(sp);
    else
    {
      NUContAssign* casn = dynamic_cast<NUContAssign*>(block);
      NU_ASSERT(casn, block);
      compileAssign(casn);
    }
  }

private:
  Interpreter mInterpreter;

  // Keep track of the expressions currently inhabiting the 4 registers.
  // Note that the presence of an expression in a register does not
  // imply that it needs to be saved.  We don't null it out because there
  // is some possibility that it will prove useful to have that value
  // in a register for a subsequent operation.
  const NUExpr* mRegExprs[Interpreter::eRegMax + 1];

  // Keep track of whether the contents of a particular register need
  // to be saved by anyone that needs temporary use
  bool mRegSave[Interpreter::eRegMax + 1];

  SCHSchedule* mSchedule;
  BlockIntMap mBlockAddressMap;
  BlockIntMap mBlockCountMap;
#ifdef INTERP_PROFILE
  BlockIntMap mBlockProfileIndexMap;
  UtArray<NUUseDefNode*> mBlockProfileVector;
#endif

  BlockLocalNetMap mBlockLocalNetMap;
  UtString mFileRoot;
  SInt32 mMaxStack64Depth;
  SInt32 mMaxStack32Depth;
  SInt32 mCurrentStack64Depth;
  SInt32 mCurrentStack32Depth;
  SInt32 mNumLocal32;
  SInt32 mNumLocal64;
  SInt32 mMaxNumLocal32;
  SInt32 mMaxNumLocal64;

  // All the code to execute multiply used blocks starts at
  // address 0, so the scheduler entry-point comes some immediately
  // after that.
  UInt32 mScheduleEntryPoint;

  STSymbolTable* mSymbolTable;
  Phase mPhase;
  NetOffsets mNetOffsets;
  bool mFailure;
  bool mInstanceAtRoot;
  NUExprFactory* mExprFactory;
  Fold* mFold;
  MsgContext* mMsgContext;
  IODBNucleus* mIODB;
  AtomicCache* mAtomicCache;
  ArgProc* mArgs;
  NUNetRefFactory* mNetRefFactory;
  SInt32 mRootOffset;
  //NUNetVector mTempNets;
  NUModuleElab* mCurrentModuleElab;
  NUModule* mTempModule;
  SourceLocatorFactory* mSourceLocatorFactory;
  SInt32 mTempNetCount;
  SInt32 mCommentIndent;
}; // class InterpPopulator : public NUDesignCallback

void Interpreter::Populate(NUDesign* design, STSymbolTable* symtab,
                           SCHSchedule* sched, const char* fileRoot,
                           MsgContext* msgContext, IODBNucleus* iodb,
                           AtomicCache* atomicCache, ArgProc* args,
                           NUNetRefFactory* netRefFactory,
                           SourceLocatorFactory* sourceLocatorFactory)
{
  InterpPopulator ip(design, symtab, sched, fileRoot, msgContext, iodb,
                     atomicCache, args, netRefFactory, sourceLocatorFactory);
  ip.populate();
}



void Interpreter::putCodeFile(UtOStream* f)
{
  mCodeFile = f;
}

void Interpreter::putCommentFile(UtOStream* f)
{
  mCommentFile = f;
}

bool Interpreter::needsMask(OpCode op)
{
  switch(op)
  {
  case eAdd32:
  case eAdd64:
  case eSub32:
  case eSub64:
  case eMul32:
  case eMul64:
  case eLShift32:
  case eLShift64:
  case eNegate32:
  case eNegate64:
    return true;
  default:
    break;
  }
  return false;
}

bool Interpreter::isCommutative(Interpreter::OpCode op)
{
  switch(op)
  {
  case eAdd32:
  case eAdd64:
  case eMul32:
  case eMul64:
  case eBitAnd32:
  case eBitAnd64:
  case eBitOr32:
  case eBitOr64:
  case eBitXor32:
  case eBitXor64:
  case eLogAnd32:
  case eLogAnd64:
  case eLogOr32:
  case eLogOr64:
  case eLogEq32:
  case eLogEq64:
    return true;
  default:
    break;
  }
  return false;
}

void Interpreter::pushInt(UInt64 val, SInt32 numBytes, const char* label) {
  if ((mCommentFile != NULL) && (label != NULL)) {
    UtString buf;
    UtOStringStream ss(&buf);
    ss << label << "(" << numBytes << ") h" << UtIO::hex
       << val << " d" << UtIO::dec << val;
    addCommentLine(buf.c_str());
  }

#if INTERP_BYTE_STREAM
  for (SInt32 i = 0; i < numBytes; ++i) {
    writeInstruction(val & 0xFF);
    val = val >> 8;
    if (i != 0)
      addCommentLine("...");
  }
  INFO_ASSERT(val == 0, "Consistency check failed.");
#else
  if (numBytes == 8) {
    mCodeFile->write((const char*) &val, 8);
    bumpInstructionCount(2);
  }
  else {
    INFO_ASSERT(numBytes == 4, "Consistency check failed.");
    writeInstruction(val);
  }
#endif
} // void Interpreter::pushInt

// Show numbers in both hex and decimal for convenience to the reader
static void sNumericComment(UtString* buf, UInt32 data) {
  char tmp[100];
  sprintf(tmp, "h%x d%d", data, data);
  *buf << tmp;
}

void Interpreter::fetch(Register reg, UInt32 offset, UInt32 netSize,
                        NetStorageMode mode,
                        const char* comment)
{
  UtString cbuf(comment);
  cbuf << " size=" << netSize;
  comment = cbuf.c_str();

  OpCode op = eEnd;
  INFO_ASSERT(netSize <= 64, comment);

  // Note that a user might be asking for an 8-bit net to be fetched
  // into a 64-bit register, so we have 32 varieties of "fetch" instructions(!)
  if (mode == eNetReference) {
    if (netSize <= 8) {
      if      (reg == eRegA64) op = eFetchRef8A64;
      else if (reg == eRegB64) op = eFetchRef8B64;
      else if (reg == eRegA32) op = eFetchRef8A32;
      else if (reg == eRegB32) op = eFetchRef8B32;
    }
    else if (netSize <= 16) {
      if      (reg == eRegA64) op = eFetchRef16A64;
      else if (reg == eRegB64) op = eFetchRef16B64;
      else if (reg == eRegA32) op = eFetchRef16A32;
      else if (reg == eRegB32) op = eFetchRef16B32;
    }
    else if (netSize <= 32) {
      if      (reg == eRegA64) op = eFetchRef32A64;
      else if (reg == eRegB64) op = eFetchRef32B64;
      else if (reg == eRegA32) op = eFetchRef32A32;
      else if (reg == eRegB32) op = eFetchRef32B32;
    }
    else {
      if      (reg == eRegA64) op = eFetchRef64A64;
      else if (reg == eRegB64) op = eFetchRef64B64;
      else if (reg == eRegA32) op = eFetchRef64A32;
      else if (reg == eRegB32) op = eFetchRef64B32;
    }
  } // if
  else if (mode == eNetStorage) {
    if (netSize <= 8) {
      if      (reg == eRegA64) op = eFetch8A64;
      else if (reg == eRegB64) op = eFetch8B64;
      else if (reg == eRegA32) op = eFetch8A32;
      else if (reg == eRegB32) op = eFetch8B32;
    }
    else if (netSize <= 16) {
      if      (reg == eRegA64) op = eFetch16A64;
      else if (reg == eRegB64) op = eFetch16B64;
      else if (reg == eRegA32) op = eFetch16A32;
      else if (reg == eRegB32) op = eFetch16B32;
    }
    else if (netSize <= 32) {
      if      (reg == eRegA64) op = eFetch32A64;
      else if (reg == eRegB64) op = eFetch32B64;
      else if (reg == eRegA32) op = eFetch32A32;
      else if (reg == eRegB32) op = eFetch32B32;
    }
    else {
      if      (reg == eRegA64) op = eFetch64A64;
      else if (reg == eRegB64) op = eFetch64B64;
      else if (reg == eRegA32) op = eFetch64A32;
      else if (reg == eRegB32) op = eFetch64B32;
    }
  } // else
  else if (mode == eNetBlockLocal) {
    if (netSize <= 32) {
      if      (reg == eRegA64) op = eFetchLocal32A64;
      else if (reg == eRegB64) op = eFetchLocal32B64;
      else if (reg == eRegA32) op = eFetchLocal32A32;
      else if (reg == eRegB32) op = eFetchLocal32B32;
    }
    else {
      if      (reg == eRegA64) op = eFetchLocal64A64;
      else if (reg == eRegB64) op = eFetchLocal64B64;
      else if (reg == eRegA32) op = eFetchLocal64A32;
      else if (reg == eRegB32) op = eFetchLocal64B32;
    }
  } // else

  addInstruction(op, offset, comment);
} // void Interpreter::fetch

void Interpreter::store(Register reg, UInt32 offset, UInt32 netSize,
                        NetStorageMode mode,
                        const char* comment)
{
  UtString cbuf(comment);
  cbuf << " size=" << netSize;
  comment = cbuf.c_str();

  OpCode op = eEnd;
  INFO_ASSERT(netSize <= 64, comment);
  INFO_ASSERT(reg != eRegB32, comment);
  INFO_ASSERT(reg != eRegB64, comment);

  // Note that a user might be asking for an 8-bit net to be fetched
  // into a 64-bit register, so we have 32 varieties of "fetch" instructions(!)
  if (mode == eNetReference) {
    if (netSize <= 8) {
      if (reg == eRegA64) op = eStoreRef8A64;
      else                op = eStoreRef8A32;
    }
    else if (netSize <= 16) {
      if (reg == eRegA64) op = eStoreRef16A64;
      else                op = eStoreRef16A32;
    }
    else if (netSize <= 32) {
      if (reg == eRegA64) op = eStoreRef32A64;
      else                op = eStoreRef32A32;
    }
    else {
      if (reg == eRegA64) op = eStoreRef64A64;
      else                op = eStoreRef64A32;
    }
  } // if
  else if (mode == eNetStorage) {
    if (netSize <= 8) {
      if (reg == eRegA64) op = eStore8A64;
      else                op = eStore8A32;
    }
    else if (netSize <= 16) {
      if (reg == eRegA64) op = eStore16A64;
      else                op = eStore16A32;
    }
    else if (netSize <= 32) {
      if (reg == eRegA64) op = eStore32A64;
      else                op = eStore32A32;
    }
    else {
      if (reg == eRegA64) op = eStore64A64;
      else                op = eStore64A32;
    }
  } // else
  else if (mode == eNetBlockLocal) {
    if (netSize <= 32) {
      if (reg == eRegA64) op = eStoreLocal32A64;
      else                op = eStoreLocal32A32;
    }
    else {
      if (reg == eRegA64) op = eStoreLocal64A64;
      else                op = eStoreLocal64A32;
    }
  } // else

  addInstruction(op, offset, comment);
} // void Interpreter::store

void Interpreter::partselAssign(Register reg, UInt32 offset, UInt32 netSize,
                                NetStorageMode mode,
                                const ConstantRange& range,
                                const char* comment)
{
  UtString cbuf;
  if (comment != NULL) {
    cbuf << comment << " [" << range.getMsb() << ":" << range.getLsb()
         << "] offset=" << offset << " netsize=" << netSize;
    comment = cbuf.c_str();
  }

  OpCode op = eEnd;
  FUNC_ASSERT(netSize <= 64, range.print());
  FUNC_ASSERT(reg != eRegB32, range.print());
  FUNC_ASSERT(reg != eRegB64, range.print());

  // Note that a user might be asking for an 8-bit net to be fetched
  // into a 64-bit register, so we have 32 varieties of "fetch" instructions(!)
  if (mode == eNetReference) {
    if (netSize <= 8) {
      if (reg == eRegA64) op = ePartSetRef8A64;
      else                op = ePartSetRef8A32;
    }
    else if (netSize <= 16) {
      if (reg == eRegA64) op = ePartSetRef16A64;
      else                op = ePartSetRef16A32;
    }
    else if (netSize <= 32) {
      if (reg == eRegA64) op = ePartSetRef32A64;
      else                op = ePartSetRef32A32;
    }
    else {
      if (reg == eRegA64) op = ePartSetRef64A64;
      else                op = ePartSetRef64A32;
    }
  } // if
  else if (mode == eNetStorage) {
    if (netSize <= 8) {
      if (reg == eRegA64) op = ePartSet8A64;
      else                op = ePartSet8A32;
    }
    else if (netSize <= 16) {
      if (reg == eRegA64) op = ePartSet16A64;
      else                op = ePartSet16A32;
    }
    else if (netSize <= 32) {
      if (reg == eRegA64) op = ePartSet32A64;
      else                op = ePartSet32A32;
    }
    else {
      if (reg == eRegA64) op = ePartSet64A64;
      else                op = ePartSet64A32;
    }
  } // else
  else if (mode == eNetBlockLocal) {
    if (netSize <= 32) {
      if (reg == eRegA64) op = ePartSetLocal32A64;
      else                op = ePartSetLocal32A32;
    }
    else {
      if (reg == eRegA64) op = ePartSetLocal64A64;
      else                op = ePartSetLocal64A32;
    }
  } // else

  addInstruction(op, offset, comment);
#ifdef INTERP_BYTE_STREAM
  pushInt(range.getLsb(), 1, "offset");
  pushInt(range.getLength(), 1, "size");
#else
  pushInt(range.getLsb() | (range.getLength() << 12), 4,
          "12bit size, 12bit offset");
#endif
} // void Interpreter::partselAssign

void Interpreter::move(Register src, Register dst)
{
  INFO_ASSERT(src != dst, "src and dst are the same register.");
  OpCode op = eEnd;
  if      ((src == eRegA32) && (dst == eRegA64)) op = eMoveA32A64;
  else if ((src == eRegA32) && (dst == eRegB32)) op = eMoveA32B32;
  else if ((src == eRegA32) && (dst == eRegB64)) op = eMoveA32B64;
  else if ((src == eRegB32) && (dst == eRegB64)) op = eMoveB32B64;
  else if ((src == eRegB32) && (dst == eRegA32)) op = eMoveB32A32;
  else if ((src == eRegB32) && (dst == eRegA64)) op = eMoveB32A64;
  else if ((src == eRegA64) && (dst == eRegA32)) op = eMoveA64A32;
  else if ((src == eRegA64) && (dst == eRegB64)) op = eMoveA64B64;
  else if ((src == eRegA64) && (dst == eRegB32)) op = eMoveA64B32;
  else if ((src == eRegB64) && (dst == eRegB32)) op = eMoveB64B32;
  else if ((src == eRegB64) && (dst == eRegA64)) op = eMoveB64A64;
  else if ((src == eRegB64) && (dst == eRegA32)) op = eMoveB64A32;
  INFO_ASSERT(op != eEnd, "Unhandled register pair.");
  opcode(op);
}

void Interpreter::push(Register reg)
{
  switch (reg) {
  case eRegA32:   opcode(ePushA32); break;
  case eRegA64:   opcode(ePushA64); break;
  case eRegB32:   opcode(ePushB32); break;
  case eRegB64:   opcode(ePushB64); break;
  }
}

void Interpreter::pop(Register reg)
{
  switch (reg) {
  case eRegA32:   opcode(ePopA32); break;
  case eRegA64:   opcode(ePopA64); break;
  case eRegB32:   opcode(ePopB32); break;
  case eRegB64:   opcode(ePopB64); break;
  }
}

void Interpreter::opcode(OpCode opCode, const char* comment)
{
  addInstruction(opCode, 0, comment, false);
} // void Interpreter::opcode

void Interpreter::const64(UInt64 k, Register dst)
{
  switch (dst) {
  case eRegA32:   INFO_ASSERT(0, "Unhandled register.");
  case eRegA64:   opcode(eConstA64); break;
  case eRegB32:   INFO_ASSERT(0, "Unhandled register.");
  case eRegB64:   opcode(eConstB64); break;
  }
  pushInt(k, 8, "const64");
}

void Interpreter::const32(UInt32 k, Register dst)
{
  switch (dst) {
  case eRegA32:   opcode(eConstA32); break;
  case eRegA64:   INFO_ASSERT(0, "Unhandled register.");
  case eRegB32:   opcode(eConstB32); break;
  case eRegB64:   INFO_ASSERT(0, "Unhandled register.");
  }
  pushInt(k, 4, "const32");
}

void Interpreter::addInstruction(OpCode x, UInt32 data, const char* comment,
                                 bool showData)
{
  if (mCommentFile != NULL)
  {
    const char* opstr = NULL;
    switch (x) {
    case eEnd: opstr = "end"; break;
#ifdef INTERP_PROFILE
    case eProfile: opstr = "profile"; break;
#endif
    case eAdd32: opstr = "add32"; break;
    case eAdd64: opstr = "add64"; break;
    case eSub32: opstr = "sub32"; break;
    case eSub64: opstr = "sub64"; break;
    case eMul32: opstr = "mul32"; break;
    case eMul64: opstr = "mul64"; break;
    case eDiv32: opstr = "div32"; break;
    case eDiv64: opstr = "div64"; break;
    case eMod32: opstr = "mod32"; break;
    case eMod64: opstr = "mod64"; break;
    case eLShift32: opstr = "lshift32"; break;
    case eLShift64: opstr = "lshift64"; break;
    case eRShift32: opstr = "rshift32"; break;
    case eRShift64: opstr = "rshift64"; break;
    case eLogAnd32: opstr = "logand32"; break;
    case eLogAnd64: opstr = "logand64"; break;
    case eLogOr32: opstr = "logor32"; break;
    case eLogOr64: opstr = "logor64"; break;
    case eLogEq32: opstr = "logeq32"; break;
    case eLogEq64: opstr = "logeq64"; break;
    case eLogLt32: opstr = "loglt32"; break;
    case eLogLt64: opstr = "loglt64"; break;
    case eLogLte32: opstr = "loglte32"; break;
    case eLogLte64: opstr = "loglte64"; break;
    case eBitAnd32: opstr = "bitand32"; break;
    case eBitAnd64: opstr = "bitand64"; break;
    case eBitOr32: opstr = "bitor32"; break;
    case eBitOr64: opstr = "bitor64"; break;
    case eBitXor32: opstr = "bitxor32"; break;
    case eBitXor64: opstr = "bitxor64"; break;
    case eBitSel32: opstr = "bitsel32"; break;
    case eBitSel64: opstr = "bitsel64"; break;
    case eKBitSel32: opstr = "kbitsel32"; break;
    case eKBitSel64: opstr = "kbitsel64"; break;
/*
    eMemSelA32D32,
    eMemSelA32D64,
    eMemSelA64D32,
    eMemSelA64D64,
    eMemSelRefA32D32,
    eMemSelRefA32D64,
    eMemSelRefA64D32,
    eMemSelRefA64D64,
*/

    case eBitSet8: opstr = "eBitSet8"; break;
    case eBitSet16: opstr = "eBitSet16"; break;
    case eBitSet32: opstr = "eBitSet32"; break;
    case eBitSet64: opstr = "eBitSet64"; break;
    case eBitSetRef8: opstr = "eBitSetRef8"; break;
    case eBitSetRef16: opstr = "eBitSetRef16"; break;
    case eBitSetRef32: opstr = "eBitSetRef32"; break;
    case eBitSetRef64: opstr = "eBitSetRef64"; break;
    case eBitSetLocal32: opstr = "eBitSetLocal32"; break;
    case eBitSetLocal64: opstr = "eBitSetLocal64"; break;

    case ePartSet8A32: opstr = "ePartSet8A32"; break;
    case ePartSet16A32: opstr = "ePartSet16A32"; break;
    case ePartSet32A32: opstr = "ePartSet32A32"; break;
    case ePartSet64A32: opstr = "ePartSet64A32"; break;
    case ePartSetRef8A32: opstr = "ePartSetRef8A32"; break;
    case ePartSetRef16A32: opstr = "ePartSetRef16A32"; break;
    case ePartSetRef32A32: opstr = "ePartSetRef32A32"; break;
    case ePartSetRef64A32: opstr = "ePartSetRef64A32"; break;
    case ePartSetLocal32A32: opstr = "ePartSetLocal32A32"; break;
    case ePartSetLocal64A32: opstr = "ePartSetLocal64A32"; break;

    case ePartSet8A64: opstr = "ePartSet8A64"; break;
    case ePartSet16A64: opstr = "ePartSet16A64"; break;
    case ePartSet32A64: opstr = "ePartSet32A64"; break;
    case ePartSet64A64: opstr = "ePartSet64A64"; break;
    case ePartSetRef8A64: opstr = "ePartSetRef8A64"; break;
    case ePartSetRef16A64: opstr = "ePartSetRef16A64"; break;
    case ePartSetRef32A64: opstr = "ePartSetRef32A64"; break;
    case ePartSetRef64A64: opstr = "ePartSetRef64A64"; break;
    case ePartSetLocal32A64: opstr = "ePartSetLocal32A64"; break;
    case ePartSetLocal64A64: opstr = "ePartSetLocal64A64"; break;

/*
    eMemSetA32D32,
    eMemSetA32D32,
    eMemSetRef,
*/

    // Unary operators have 2 fields, src & dst
    case eBitNot32: opstr = "bitnot32"; break;
    case eBitNot64: opstr = "bitnot64"; break;
    case eLogNot32: opstr = "lognot32"; break;
    case eLogNot64: opstr = "lognot64"; break;
    case eNegate32: opstr = "negate32"; break;
    case eNegate64: opstr = "negate64"; break;
    case eReductOr32: opstr = "reductor32"; break;
    case eReductOr64: opstr = "reductor64"; break;
    case eReductNor32: opstr = "reductnor32"; break;
    case eReductNor64: opstr = "reductnor64"; break;
/*
    eReductAnd32,
    eReductAnd64,
    eReductNand32,
    eReductNand64,
    eReductXor32,
    eReductXor64,
    eReductXnor32,
    eReductXnor64,
*/

    // Fetch operator for finding storage nets into registers A or B
    case eFetch8A32: opstr = "fetch8a32"; break;
    case eFetch16A32: opstr = "fetch16a32"; break;
    case eFetch32A32: opstr = "fetch32a32"; break;
    case eFetch64A32: opstr = "fetch64a32"; break;
    case eFetch8B32: opstr = "fetch8b32"; break;
    case eFetch16B32: opstr = "fetch16b32"; break;
    case eFetch32B32: opstr = "fetch32b32"; break;
    case eFetch64B32: opstr = "fetch64b32"; break;
    case eFetch8A64: opstr = "fetch8a64"; break;
    case eFetch16A64: opstr = "fetch16a64"; break;
    case eFetch32A64: opstr = "fetch32a64"; break;
    case eFetch64A64: opstr = "fetch64a64"; break;
    case eFetch8B64: opstr = "fetch8b64"; break;
    case eFetch16B64: opstr = "fetch16b64"; break;
    case eFetch32B64: opstr = "fetch32b64"; break;
    case eFetch64B64: opstr = "fetch64b64"; break;

    // Fetch operator for finding reference nets into registers A or B
    case eFetchRef8A32: opstr = "ref8a32"; break;
    case eFetchRef16A32: opstr = "ref16a32"; break;
    case eFetchRef32A32: opstr = "ref32a32"; break;
    case eFetchRef64A32: opstr = "ref64a32"; break;
    case eFetchRef8B32: opstr = "ref8b32"; break;
    case eFetchRef16B32: opstr = "ref16b32"; break;
    case eFetchRef32B32: opstr = "ref32b32"; break;
    case eFetchRef64B32: opstr = "ref64b32"; break;
    case eFetchRef8A64: opstr = "ref8a64"; break;
    case eFetchRef16A64: opstr = "ref16a64"; break;
    case eFetchRef32A64: opstr = "ref32a64"; break;
    case eFetchRef64A64: opstr = "ref64a64"; break;
    case eFetchRef8B64: opstr = "ref8b64"; break;
    case eFetchRef16B64: opstr = "ref16b64"; break;
    case eFetchRef32B64: opstr = "ref32b64"; break;
    case eFetchRef64B64: opstr = "ref64b64"; break;

    case eFetchLocal32A32: opstr = "fetchLocal32A32"; break;
    case eFetchLocal64A32: opstr = "fetchLocal64A32"; break;
    case eFetchLocal32B32: opstr = "fetchLocal32B32"; break;
    case eFetchLocal64B32: opstr = "fetchLocal64B32"; break;
    case eFetchLocal32A64: opstr = "fetchLocal32A64"; break;
    case eFetchLocal64A64: opstr = "fetchLocal64A64"; break;
    case eFetchLocal32B64: opstr = "fetchLocal32B64"; break;
    case eFetchLocal64B64: opstr = "fetchLocal64B64"; break;

    // Moving data between 32-bit and 64-bit A & B registers
    case eMoveA32A64: opstr = "movea32a64"; break;
    case eMoveA32B32: opstr = "movea32b32"; break;
    case eMoveA32B64: opstr = "movea32b64"; break;
    case eMoveA64A32: opstr = "movea64a32"; break;
    case eMoveA64B32: opstr = "movea64b32"; break;
    case eMoveA64B64: opstr = "movea64b64"; break;
    case eMoveB32B64: opstr = "moveb32b64"; break;
    case eMoveB32A32: opstr = "moveb32a32"; break;
    case eMoveB32A64: opstr = "moveb32a64"; break;
    case eMoveB64B32: opstr = "moveb64b32"; break;
    case eMoveB64A32: opstr = "moveb64a32"; break;
    case eMoveB64A64: opstr = "moveb64a64"; break;

    // Getting constants from the instruction stream
    case eConstA32: opstr = "consta32"; break;
    case eConstA64: opstr = "consta64"; break;
    case eConstB32: opstr = "constb32"; break;
    case eConstB64: opstr = "constb64"; break;

    // Storing values from register A into model storage
    case eStore8A32: opstr = "store8A32"; break;
    case eStore16A32: opstr = "store16A32"; break;
    case eStore32A32: opstr = "store32A32"; break;
    case eStore64A32: opstr = "store64A32"; break;
    case eStore8A64: opstr = "store8A64"; break;
    case eStore16A64: opstr = "store16A64"; break;
    case eStore32A64: opstr = "store32A64"; break;
    case eStore64A64: opstr = "store64A64"; break;
    case eStoreRef8A32: opstr = "storeRef8A32"; break;
    case eStoreRef16A32: opstr = "storeRef16A32"; break;
    case eStoreRef32A32: opstr = "storeRef32A32"; break;
    case eStoreRef64A32: opstr = "storeRef64A32"; break;
    case eStoreRef8A64: opstr = "storeRef8A64"; break;
    case eStoreRef16A64: opstr = "storeRef16A64"; break;
    case eStoreRef32A64: opstr = "storeRef32A64"; break;
    case eStoreRef64A64: opstr = "storeRef64A64"; break;
    case eStoreLocal32A32: opstr = "storeLocal32A32"; break;
    case eStoreLocal64A32: opstr = "storeLocal64A32"; break;
    case eStoreLocal32A64: opstr = "storeLocal32A64"; break;
    case eStoreLocal64A64: opstr = "storeLocal64A64"; break;

    // Control-flow operators, used to handle if, case, for
    case eIf32: opstr = "if32"; break;
    case eIf64: opstr = "if64"; break;
    case eIfChanged: opstr = "ifchanged"; break;
    case eRelativeJump: opstr = "relativejump"; break;
    case eReturn:       opstr = "return"; break;
    case eCall: opstr = "call"; break;

    // Stack operations
    case ePushA32: opstr = "pusha32"; break;
    case ePushA64: opstr = "pusha64"; break;
    case ePushB32: opstr = "pushb32"; break;
    case ePushB64: opstr = "pushb64"; break;
    case ePopA32: opstr = "popa32"; break;
    case ePopA64: opstr = "popa64"; break;
    case ePopB32: opstr = "popb32"; break;
    case ePopB64: opstr = "popb64"; break;

    case ePartSel32: opstr = "partsel32"; break;
    case ePartSel64: opstr = "partsel64"; break;

    case ePartSelBA32: opstr = "partselBA32"; break;
    case ePartSelBA64: opstr = "partselBA64"; break;

    case eConcat32: opstr = "concat32"; break;
    case eConcat64: opstr = "concat64"; break;

    case eSetInstance: opstr = "setinstance"; break;
    default: INFO_ASSERT (0, "Unhandled OpCode."); break;
    } // switch
    INFO_ASSERT(opstr, "Unhandled OpCode.");
    UtString buf;
    UtIndent id(&buf);
    buf << opstr << " ";
#ifndef INTERP_BYTE_STREAM
    if (showData) {
      sNumericComment(&buf, data);
      buf << " ";
    }
#endif
    if (comment != NULL) {
      id.tab(30);
      buf << "// " << comment;
    }
    addCommentLine(buf.c_str());
  } // if

  INFO_ASSERT((data & 0xFF000000) == 0, "Consistency check failed.");
#ifdef INTERP_BYTE_STREAM
  writeInstruction(x);
  if (showData)
    pushInt(data, 3, "opval");
#else
  writeInstruction((data << 8) | (UInt32) x);
#endif
} // void Interpreter::addInstruction

#ifdef INTERP_BYTE_STREAM
void Interpreter::writeInstruction(UInt8 data) {
  mCodeFile->write((const char*) &data, 1);
  bumpInstructionCount(1);
}
#else
void Interpreter::writeInstruction(UInt32 data) {
  mCodeFile->write((const char*) &data, 4);
  bumpInstructionCount(1);
}
#endif

void Interpreter::blockComment(const char* comment) {
  addCommentLine("// --------------------------------------------------------------");
  UtString buf;
  buf << "// " << comment;
  addCommentLine(buf.c_str());
}

void Interpreter::instance(UInt32 offset)
{
  addInstruction(eSetInstance, offset);
}

void Interpreter::call(UInt32 offset, UInt32 address)
{
  addInstruction(eCall, offset, "data offset");
  mCommentIndent += 2;
  pushInt(address, 4, "subr address");
  mCommentIndent -= 2;
}

void Interpreter::insertReturn() {
  opcode(eReturn);
}

void Interpreter::partselRvalue(OpCode op, const ConstantRange& range) {
#ifdef INTERP_BYTE_STREAM
  UtString comment;
  comment << "[" << range.getMsb() << ":" << range.getLsb() << "]";
  opcode(op, comment.c_str());
  FUNC_ASSERT(range.getLsb() <= range.getMsb(), range.print());
  FUNC_ASSERT(range.getLsb() < (1<<8), range.print());
  FUNC_ASSERT(range.getLength() < (1<<8), range.print());
  pushInt(range.getLsb(), 1, "startBit");
  pushInt(range.getLength(), 1, "numBits");
#else
  UtString comment;
  comment << "[" << range.getMsb() << ":" << range.getLsb() << "]";

  FUNC_ASSERT(range.getLsb() <= range.getMsb(), range.print());
  FUNC_ASSERT(range.getLsb() < (1<<12), range.print());
  FUNC_ASSERT(range.getLength() < (1<<12), range.print());
  UInt32 codedPsel = range.getLsb() | (range.getLength() << 12);
  addInstruction(op, codedPsel, comment.c_str());
#endif
}


class Interpreter::Block {
public:
  Block(UtOStream* codeFile, Block* block)
    : mCodeStream(&mCodeBuffer),
      mSaveBlock(block)
  {
    mSaveCode = codeFile;
    mSaveBlock = block;
    mInstructionCount = 0;
  }
  ~Block() {}

  UInt32 numInstructions() {
#ifdef INTERP_BYTE_STREAM
    return mCodeBuffer.size();
#else
    INFO_ASSERT((mCodeBuffer.size() & 3) == 0, "Consistency check failed.");
    return mCodeBuffer.size() / 4;
#endif
  }
  UtOStream* getSaveCodeFile() {return mSaveCode;}
  UtOStream* getCodeStream() {return &mCodeStream;}
  const char* getCodeBuffer() {return mCodeBuffer.c_str();}
  void addCommentLine(const char* str) {
    mCommentBuffer.push_back(UtString(str));
  }
  Block* getSaveBlock() {return mSaveBlock;}

  void writeComments(Interpreter* interp) {
    for (UInt32 i = 0; i < mCommentBuffer.size(); ++i)
      interp->addCommentLine(mCommentBuffer[i].c_str());
  }

  UtString mCodeBuffer;
  UtVector<UtString> mCommentBuffer;
  UtOStringStream mCodeStream;
  UtOStream* mSaveCode;
  Block* mSaveBlock;
  UInt32 mInstructionCount;
}; // class Interpreter::Block

void Interpreter::bumpInstructionCount(UInt32 increment) {
  if (mCurrentBlock == NULL)
    mInstructionCount += increment;
  else
    mCurrentBlock->mInstructionCount += increment;
}  

void Interpreter::addCommentLine(const char* commentLine) {
  UtString ibuf;
  if (mCommentIndent != 0) {
    ibuf.append(mCommentIndent, ' ');
    ibuf << commentLine;
    commentLine = ibuf.c_str();
  }

  if (mCurrentBlock == NULL) {
    if (strncmp(commentLine, "//", 2) != 0) {
      *mCommentFile << UtIO::hex << UtIO::setfill('0') << UtIO::setw(8)
                    << mCommentAddress;
      *mCommentFile << " " << commentLine << "\n";
      ++mCommentAddress;
    }
    else
      *mCommentFile << commentLine << "\n";
  }
  else
    mCurrentBlock->addCommentLine(commentLine);
}

Interpreter::Block* Interpreter::pushBlock() {
  Block* block = new Block(mCodeFile, mCurrentBlock);
  mCodeFile = block->getCodeStream();
  mCurrentBlock = block;
  return block;
}

void Interpreter::popBlock(Block* block) {
  mCodeFile = block->getSaveCodeFile();
  mCurrentBlock = block->getSaveBlock();
}

// writes the block's code, comments, and instruction count, & deletes it
void Interpreter::finalizeBlock(Block* block) {
  mCodeFile->write(block->getCodeBuffer(),
                   (sizeof(InterpInstruction))*block->numInstructions());
  bumpInstructionCount(block->mInstructionCount);
  block->writeComments(this);
  INFO_ASSERT(block->mInstructionCount == block->numInstructions(), "Consistency check failed.");
  delete block;
}

void Interpreter::addIf(bool is64Bit, Block* thenClause, Block* elseClause)
{
  OpCode opCode = is64Bit? eIf64: eIf32;

  // "if" statements look like this:
  //    <compute condition into a32, 0x00000000=Else, 0xffffffff=Then>
  //    eIf
  //    number-of-else-instructions
  //    else-instruction-1
  //    else-instruction-2
  //    else-instruction-3
  //    ....
  //    eRelativeJump
  //    number-of-then-instructions
  //    then-instruction-1
  //    then-instruction-2
  //    then-instruction-3
  //    ...

  // We know how many instructions to skip to get to the
  // then clause.  Include the 3 bytes that we'll append to
  // the "else" block to skip over the If block.
  UInt32 elseSkip = elseClause->numInstructions();
  UInt32 thenSkip = thenClause->numInstructions();
  ++elseSkip;                   // skip over else-block + eRelativeJump 
#ifdef INTERP_BYTE_STREAM
  // skip over 3 bytes of relative jump count
  elseSkip += 3;
#endif
  addInstruction(opCode, elseSkip);
  finalizeBlock(elseClause);

  // Now we know how many instructions to skip after the "else"
  // clause to skip the "then" clause.
  addInstruction(eRelativeJump, thenSkip);
  finalizeBlock(thenClause);
} // void Interpreter::addIf

void Interpreter::addIfChanged(SInt32 changedIndex, Block* clockBlock) {
  // Now we have to correct our instruction-count, which got adjusted by
  // 4 in pushBlock(), and has also been bumped up by all the instructions
  // encountered in the then- and else- blocks.
  UInt32 blockSize = clockBlock->numInstructions();

  addInstruction(eIfChanged, changedIndex);
  pushInt(blockSize, 4, "blockSize");
  finalizeBlock(clockBlock);
}

bool Interpreter::isLogical(OpCode opCode) {
  bool ret = ((opCode == Interpreter::eLogAnd32) ||
              (opCode == Interpreter::eLogAnd64) ||
              (opCode == Interpreter::eLogOr32) ||
              (opCode == Interpreter::eLogOr64) ||
              (opCode == Interpreter::eLogEq32) ||
              (opCode == Interpreter::eLogEq64) ||
              (opCode == Interpreter::eLogLt32) ||
              (opCode == Interpreter::eLogLt64) ||
              (opCode == Interpreter::eLogLte32) ||
              (opCode == Interpreter::eLogLte64) ||
              (opCode == Interpreter::eLogNot32) ||
              (opCode == Interpreter::eLogNot64) ||
              (opCode == Interpreter::eReductOr32) ||
              (opCode == Interpreter::eReductOr64) ||
              (opCode == Interpreter::eReductNor32) ||
              (opCode == Interpreter::eReductNor64) ||
              (opCode == Interpreter::eReductXor32) ||
              (opCode == Interpreter::eReductXor64) ||
              (opCode == Interpreter::eReductXnor32) ||
              (opCode == Interpreter::eReductXnor64) ||
              (opCode == Interpreter::eBitSel32) ||
              (opCode == Interpreter::eBitSel64));
  return ret;
} // bool Interpreter::isLogical
