//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "interp/Interp.h"
#include "util/UtArray.h"

#ifdef INTERP_PROFILE
#include "util/UtIOStream.h"
#include <algorithm>
#endif

Interpreter::Interpreter()
{
  mCodeFile = NULL;
  mCommentFile = NULL;
  mInstructionCount = 0;
  mLocal64 = NULL;
  mStack64 = NULL;
  mLocal32 = NULL;
  mStack32 = NULL;
  mInstructions = NULL;
  mModel = NULL;
  mChangeArray = NULL;
  mCommentAddress = 0;
  mCurrentBlock = NULL;
  mCommentIndent = 0;

#ifdef INTERP_PROFILE
  mNumProfileBuckets = 0;
  mProfileBuckets = NULL;
#endif
}

Interpreter::~Interpreter()
{
  if (mInstructions != NULL)
    delete [] mInstructions;
  if (mLocal64 != NULL)
    delete [] mLocal64;

#ifdef INTERP_PROFILE
  printProfileData();
  if (mProfileBuckets != NULL)
    delete [] mProfileBuckets;
#endif
}

void Interpreter::putModelData(UInt8* modelData, bool* changeArray) {
  mModel = modelData;
  mChangeArray = changeArray;
}

#ifdef INTERP_PROFILE
void Interpreter::printProfileData() {
  if (mNumProfileBuckets != 0) {
    // Sort the profile buckets by size
    typedef std::pair<SInt32, SInt32> IndexCount;
    UtArray<IndexCount> profileData(mNumProfileBuckets);
    for (UInt32 i = 0; i < mNumProfileBuckets; ++i) {
      profileData[i].first = mProfileBuckets[i];
      profileData[i].second = i;
    }
    std::sort(profileData.begin(), profileData.end());
    UtIO::cout() << "Index\tCount\n";
    for (UInt32 i = 0; i < mNumProfileBuckets; ++i) {
      UtIO::cout() << profileData[i].second << "\t"
                   << profileData[i].first << "\n";
    }
    UtIO::cout().flush();
  }
}
#endif
