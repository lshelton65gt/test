//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stdlib.h>
#include <algorithm>
#include <sys/times.h>
#include "util/CarbonAssert.h"
#include "util/UtArray.h"
#include "util/UtVector.h"
#include "util/MemManager.h"
#include "util/RandomValGen.h"
#include "util/UtMap.h"
#include "util/UtList.h"

class MiniMemPool
{
public:
  MiniMemPool()
  {
    mData = NULL;
    mFreeStorage = 0;
    mNextAlloc = NULL;
  }
  ~MiniMemPool() {
    for (UInt32 i = 0; i < mAllocedData.size(); ++i)
      delete [] mAllocedData[i];
  }
  void* alloc(UInt32 size)
  {
    if (size <= 65536)
    {
      size += 3;
      size &= ~0x3;
      if (size > mFreeStorage)
      {
        mData = new char[65536];
        mFreeStorage = 65536;
        mNextAlloc = mData;
        mAllocedData.push_back(mData);
      }
      mFreeStorage -= size;
      void* ret = (void*) mNextAlloc;
      mNextAlloc += size;
      return ret;
    }

    char* ret = new char[size];
    mAllocedData.push_back(ret);
    return (void*) ret;
  }

private:
  UtVector<char*> mAllocedData;
  char* mData;
  char* mNextAlloc;
  UInt32 mFreeStorage;
};

class FakeModule
{
public:
  FakeModule() {}
  ~FakeModule() {}

  Interpreter mBlock;
  UtVector<FakeModule*> mInstances;
};

class FakeModel
{
public:
  typedef std::pair<UInt32, UInt32> OffsetSize;

  FakeModel(UInt32 numInstructions, UInt32 size, MiniMemPool* mem):
    mRandom(0)
  {
    //mRandomSeed = 0;
    //srand(0);
    mPrevSize = 0;

    // For the purposes of random model generation, we will
    // generate two types of modules:  leaf modules that
    // have code-blocks and no hierarchy, and branch modules
    // that instantiate leaves & other branches.  
    
    // First, create a set of leaf modules that total between
    // 10% and 100% of the design size, in sizes ranging from
    // 1 to 1000 instructions.
    UInt32 leafInstructionsMax = mRandom(numInstructions/10, numInstructions);
    UInt32 leafInstructions = 0;
    while (leafInstructions < leafInstructionsMax)
    {
      mRandom(100, 
    }

    // Randomly allocate objects of various sizes until we
    // run out of room, making alignments work.
    UInt32 allocSize = 0, nextSize = 0;
    while ((nextSize = getNextSize()) + allocSize < size)
    {
      // Make the alignment work.  Note that
      // due to alignment requirements we may wind up overshooting
      // our intended allocation by a bit.  Doesn't matter for
      // this experiment
      if (nextSize == 2) {
        if ((allocSize & 1) != 0)
          ++allocSize;          // pad to an even byte
      } else if ((nextSize != 1) && ((allocSize & 3) != 0))
        allocSize = (allocSize + 3) & ~0x3;
      mNets.push_back(OffsetSize(nextSize, allocSize));
      allocSize += nextSize;
    }

    mStorage = (UInt8*) mem->alloc(allocSize);
    memset(mStorage, '\0', allocSize);
    mSize = allocSize;
  } // FakeModel

  ~FakeModel()
  {
    // delete [] mStorage;
  }

  UInt32 nextRandomNumber()
  {
    return mRandom(); // (UInt32) rand();
  }

  // Compute the size of a net based on semi-random sampling.
  UInt32 getNextSize()
  {
    UInt32 randNum = nextRandomNumber();
    
    // There is a currently tendency for cbuild to group nets of like
    // size together.  That may change but for now we will assume
    // that there is a 50% chance that a net's size will be the
    // same as the one that preceded it.
    if ((mPrevSize == 0) || ((randNum & 1) != 0))
    {
      // We will consider sizes of 1,2, and multiples of 4 bytes
      // where there are 4x more objects of size 1 than size 2,
      // 4x more of size 2 than size 4, etc.  With this approach
      // it's easy to get up to about size 18
      if ((randNum & (0x3 << 1)) != 0)
        mPrevSize = 1;
      else if ((randNum & (0x3 << 3)) != 0)
        mPrevSize = 2;
      else if ((randNum & (0x3 << 5)) != 0)
        mPrevSize = 4;
      else
      {
        mPrevSize = 4;
        for (SInt32 bit = 7; bit < 31; bit += 2)
        {
          mPrevSize += 4;
          if ((randNum & (0x3 << bit)) != 0)
            break;
        }
      }
    } // if
    return mPrevSize;
  } // UInt32 getNextSize

  // Compute the next state, based on an expected activity
  // ratio, which is expressed in number of changes per
  // thousand signals
  SInt32 computeNextState(UInt32 activityPerThousand)
  {
    SInt32 numChanges = 0;
    for (size_t i = 0, n = mNets.size(); i < n; ++i)
    {
      // Decide whether there should be activity on this
      // net
      UInt32 randNum = nextRandomNumber() % 1000;
      if (randNum < activityPerThousand)
      {
        UInt32 size = mNets[i].first;
        UInt32 offset = mNets[i].second;

        // Change every byte in the net based on its
        // own random number.
        bool changed = false;
        for (UInt32 i = 0; i < size; ++i)
        {
          UInt32 nextByte = nextRandomNumber() & 0xFF;
          if (mStorage[offset + i] != nextByte)
          {
            changed = true;
            mStorage[offset + i] = nextByte;
          }
        }
        if (changed)
        {
          ++numChanges;
          //fprintf(stdout, "size=%u addr=%p\n", size, &mStorage[offset]);
        }
      }
    }
    return numChanges;
  } // SInt32 computeNextState

  SInt32 numNets() const {return mNets.size();}
  OffsetSize getOffsetSize(SInt32 index) {return mNets[index];}
  void* getObjectStorage(UInt32 offset) {return (void*) (mStorage + offset);}

private:
  UInt8* mStorage;
  SInt32 mSize;
  UtVector<OffsetSize> mNets;
  //UInt32 mRandomSeed;
  RandomValue<UInt32> mRandom;
  UInt32 mPrevSize;
};

class WaveDump
{
  typedef UtArray<UInt32> OffsetList;
  typedef UtMap<UInt32,OffsetList> CollatedOffsets; // indexed by size in num chars

  typedef ValueChange<UInt32, BVOp> BitVectorVC;
  typedef UtArray<BitVectorVC*> BitVectorVCs; // indexed by num UInt32s

  typedef UtArray<ValueChangeBase*> ValueChangeVector;

public:
  WaveDump(FakeModel* fm, MiniMemPool* memPool): mModel(fm)
  {
    // Walk through the model in offset order and collate the
    // various objects of different sizes
    CollatedOffsets collatedOffsets;
    for (SInt32 i = 0; i < fm->numNets(); ++i)
    {
      FakeModel::OffsetSize os = fm->getOffsetSize(i);
      UInt32 size = os.first;
      UInt32 offset = os.second;
//      if (size >= collatedOffsets.size())
//        collatedOffsets.resize(size + 1);
      collatedOffsets[size].push_back(offset);
    }

/*
    mVC8 = NULL;
    mVC16 = NULL;
    mVC32 = NULL;
    mVC64 = NULL;
*/

    // Walk through all the sizes
    SInt32 index = 0;
//    for (UInt32 s = 0; s < collatedOffsets.size(); ++s)
    for (CollatedOffsets::iterator p = collatedOffsets.begin(),
           e = collatedOffsets.end(); p != e; ++p)
    {
      UInt32 s = p->first;
      OffsetList& offsets = p->second;
//      OffsetList& offsets = collatedOffsets[s];
      UInt32 numObjects = offsets.size();
      ValueChangeBase* vc = NULL;
      if (numObjects != 0)
      {
        switch (s)
        {
        case 1:
          vc = /*mVC8 =*/ ValueChange<UInt8>::create(index, numObjects,
                                                     ScalarOp<UInt8>(), memPool);
          break;
        case 2:
          vc = /*mVC16 =*/ ValueChange<UInt16>::create(index, numObjects,
                                                       ScalarOp<UInt16>(), memPool);
          break;
        case 4:
          vc = /*mVC32 =*/ ValueChange<UInt32>::create(index, numObjects,
                                                       ScalarOp<UInt32>(), memPool);
          break;
        case 8:
          vc = /*mVC64 =*/ ValueChange<UInt64>::create(index, numObjects,
                                                       ScalarOp<UInt64>(), memPool);
          break;
        default: {
          INFO_ASSERT((s & 3) == 0, "Invalid offset.");
          BitVectorVC* bvc = BitVectorVC::create(index, numObjects, BVOp(s / 4),
                                                 memPool);
          vc = bvc;
          //mVCbv.push_back(bvc);
          break;
        }
        } // switch
        for (OffsetList::iterator p = offsets.begin(), e = offsets.end();
             p != e; ++p)
        {
          vc->putModelPointer(index++, mModel->getObjectStorage(*p));
          // could also associate index with wave-data here
        }
        mValueChanges.push_back(vc);
      }
    }
  } // WaveDump

  ~WaveDump() {
/*
    for (size_t i = 0; i < mValueChanges.size(); ++i)
      delete mValueChanges[i];
*/
  }

  SInt32 countChanges()
  {
    SInt32 numChanges = 0;
    for (UInt32 i = 0; i < mValueChanges.size(); ++i)
    {
      numChanges += mValueChanges[i]->countChanges();
    }
    return numChanges;
  }

  FakeModel* mModel;
  ValueChangeVector mValueChanges;
/*
  ValueChange<UInt8>  *mVC8;
  ValueChange<UInt16> *mVC16;
  ValueChange<UInt32> *mVC32;
  ValueChange<UInt64> *mVC64;
  BitVectorVCs mVCbv;
*/
};

void testModel(UInt32 modelSize)
{
  UInt32 totalTime = 0;
  UInt32 totalValueChanges = 0;
  const UInt32 cIterations = 600;
  Timing timing;

  {
    MiniMemPool memPool;
    FakeModel model(modelSize, &memPool);
    CarbonMem::printStats();
    {
      WaveDump waveDump(&model, &memPool);
      CarbonMem::printStats();
      for (UInt32 i = 0; i < cIterations; ++i)
      {
        SInt32 numChanges = model.computeNextState(7); // number of changes per thousand signals
        //fprintf(stdout, "-------------------------------------------\n");
        timing.start();
        SInt32 detectChanges = waveDump.countChanges();
        timing.end();
        totalTime += timing.getUserDelta();
        totalValueChanges += detectChanges;
        INFO_ASSERT(numChanges == detectChanges, "Consistency check failed.");
      }
    }
  }
  CarbonMem::releaseBlocks();
  CarbonMem::printStats();
  fprintf(stdout, "%lu,%lu,%lu\n",
          (unsigned long) modelSize,
          (unsigned long) (totalValueChanges / cIterations),
          (unsigned long) totalTime);
} // void testModel

int main(int argc, char** argv)
{
  const UInt32 K = 1024;
  const UInt32 M = K*K;
  UInt32 modelSize = 0;

  if (argc == 2)
  {
    unsigned long ms;
    if (sscanf(argv[1], "%lu", &ms) == 1)
      modelSize = ms;
    else
    {
      fprintf(stdout, "Invalid model size: %s\n", argv[1]);
      return 1;
    }
  }

  fprintf(stdout, "Model Size,Value Changes,User Time\n");

  if (modelSize == 0)
  {
    for (modelSize = 10*K; modelSize <= 10*M; modelSize = UInt32(modelSize * 1.1))
    {
      testModel(modelSize);
    }
  }
  else
    testModel(modelSize);
  fflush(stdout);
} // int main
