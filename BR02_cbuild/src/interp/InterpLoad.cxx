//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "interp/Interp.h"
#include "util/OSWrapper.h"
#include "util/CarbonAssert.h"
#include <sys/fcntl.h>   // for O_RDONLY

bool Interpreter::loadCode(const char* filename, UtString* errmsg) {
  // The file is written as a binary byte-stream of instructions,
  // followed by 5 8-digit hex ascii numbers, separated by newlines,
  // indicating the required memory for stack and local storage, plus
  // the scheduling entry point.  This makes it easy to parse it
  // out from the end, cause we know we need to go 5*9=45 bytes
  // back from the end.

  // The strategy for reading it is to read the entire file into
  // memory in one go (because it mostly needs to be stored contiguously
  // anyway, and parse out the ascii numbers at the end.  We won't
  // bother to realloc it down to recapture usage of those 45 bytes.

  SInt64 fileSize;
  if (!OSGetFileSize(filename, &fileSize, errmsg))
    return false;

  if (fileSize > 0x7FFFFFFF) {
    *errmsg << filename << " too big\n";
    return false;
  }

  // Open the file and read the whole thing
#ifdef INTERP_BYTE_STREAM
  mInstructions = new UInt8[fileSize + 1];
#else
  mInstructions = new UInt32[(fileSize + 1)/4 + 1];
#endif
  int fd = OSSysOpen(filename, O_RDONLY | O_BINARY, 0, errmsg);
  if (fd == -1)
    return false;

  int numRead = OSSysRead(fd, (char*) mInstructions, fileSize, errmsg);
  if (numRead != fileSize)
  {
    if (numRead > 0)
      *errmsg << filename << ": expected " << fileSize << " bytes, received "
              << numRead << "\n";
    return false;
  }
  if (OSSysClose(fd, errmsg) == -1)
    return false;

  // Looking at the last lines of InterPopulator::populate, we
  // expect to see op-code eEnd, followed by 6 8-digit hex numbers
  // separated by newlines.  Let's just make sure that's what we got
  // by finding the pointer to the eEnd.
  UInt32 distanceFromEnd = 6*(8+1);
  ++distanceFromEnd;            // newline after eEnd
  distanceFromEnd += sizeof(InterpInstruction);         // eEnd instruction
  InterpInstruction* endMarker = (InterpInstruction*)
    (((char*) mInstructions) + fileSize - distanceFromEnd);
  if (*endMarker != (InterpInstruction) eEnd) {
    *errmsg << filename << ": data corruption, end not found\n";
    return false;
  }
  char* finalRecord = (char*) (endMarker + 1);
  if (*finalRecord != '\n') {
    *errmsg << filename << ": data corruption, newline not found\n";
    return false;
  }

  ++finalRecord;
  unsigned int maxStack32, maxStack64, maxLocal32, maxLocal64, schedEntry;
  unsigned int profBuckets;
  if (sscanf(finalRecord, "%x\n%x\n%x\n%x\n%x\n%x\n",
             &maxStack32, &maxStack64, &maxLocal32, &maxLocal64, &schedEntry,
             &profBuckets) != 6)
  {
    *errmsg << filename << ": data corruption, final records not parsed\n";
    return false;
  }

  // Allocate and divvy up the memory which will be used for stack and
  // local storage
  UInt32 int32sRequired = maxStack32 + maxLocal32;
  UInt32 int64sRequired = (int32sRequired+1)/2 + (maxStack64 + maxLocal64);
  if (int64sRequired != 0) {
    mLocal64 = new UInt64[int64sRequired];
    INFO_ASSERT((((UIntPtr) mLocal64) & 7) == 0, "Not aligned."); // aligned
    mStack64 = mLocal64 + maxLocal64;
    mStack32 = (UInt32*) (mStack64 + maxStack64);
    mLocal32 = mStack32 + maxStack32;

    // We start off with the stack pointers pointing *before* the first
    // element because I think it might possibly be more efficient that way.
    // No evidence though.
    --mStack32;
    --mStack64;
  }

  mScheduleEntry = schedEntry;

#ifdef INTERP_PROFILE
  if (profBuckets != 0) {
    mNumProfileBuckets = profBuckets;
    mProfileBuckets = new UInt32[profBuckets];
    for (UInt32 i = 0; i < profBuckets; ++i)
      mProfileBuckets[i] = 0;
  }
#endif

  return true;
} // bool Interpreter::load
