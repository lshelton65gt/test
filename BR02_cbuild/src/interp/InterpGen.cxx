//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "interp/Interp.h"

bool Interpreter::needsMask(OpCode op)
{
  switch (op)
  {
  case eAdd:
  case eSub:
  case eMul:
  case eDiv:
  case eMod:
    return true;
  default:
    break;
  }
  return false;
}  


UInt32 Interpreter::getSizeCode(UInt32 numBits)
{
  UInt32 sizeCode = 0;
  if (numBits > 8)
  {
    if (numBits <= 16)
      sizeCode = 1;
    else if (numBits <= 32)
      sizeCode = 2;
    else
    {
      INFO_ASSERT(numBits <= 64, "Invalid size.");
      sizeCode = 3;
    }
  }
  return sizeCode;
}

void Interpreter::recordOpCode(OpCode op, UInt32 numBits)
{
  UInt32 sizeCode = getSizeCode(numBits);
  UInt32 opWord = op;

  if (needsMask(op))
  {
    switch (sizeCode)
    {
    case 0: {
      UInt8 byteMask = (1U << numBits) - 1;
      UInt32 mask = byteMask;
      opWord |= mask << 8;
      mInstructions.push_back(opWord);
      break;
    }
    case 1: {
      UInt16 shortMask = (1U << numBits) - 1;
      UInt32 mask = shortMask;
      opWord |= mask << 16;
      mInstructions.push_back(opWord);
      break;
    }
    case 2:
      mInstructions.push_back(opWord);
      mInstructions.push_back(1 << numBits);
      break;
    case 3: {
      UInt64 u64Mask = (1ULL << numBits) - 1;
      mInstructions.push_back(u6Mask & 0xFFFFFFFF);
      mInstructions.push_back(u64Mask >> 32);
      break;
    }
    } // switch
  } // if
  else
    mInstructions.push_back(opWord);
      
  return sizeCode;
} // void Interpreter::recordOpCode

void Interpreter::addUnaryOp(OpCode op, UInt32 numBits, UInt32 dst, UInt32 op1)
{
  INFO_ASSERT(isUnary(op), "Consistency check failed.");
  recordOpCode(op, numBits);
  mInstructions.push_back(dst);
  mInstructions.push_back(op1);
}

void Interpreter::addBinaryOp(OpCode op, UInt32 numBits, UInt32 dst,
                              UInt32 op1, UInt32 op2)
{
  INFO_ASSERT(isBinary(op), "Consistency check failed.");
  recordOpCode(op, numBits);
  mInstructions.push_back(dst);
  mInstructions.push_back(op1);
  mInstructions.push_back(op2);
}

void Interpreter::addTernaryOp(OpCode op, UInt32 numBits, UInt32 dst,
                               UInt32 op1, UInt32 op2, UInt32 op3)
{
  INFO_ASSERT(isBinary(op), "Consistency check failed.");
  recordOpCode(op, numBits);
  mInstructions.push_back(dst);
  mInstructions.push_back(op1);
  mInstructions.push_back(op2);
  mInstructions.push_back(op3);
}

void Interpreter::addIf(UInt32 cond,
                        const Interpreter& thenClause,
                        const Interpreter& elseClause)
{
  mInstructions.push_back(eIf);
  mInstructions.push_back(cond);

  // If the condition is false, then we need to know how many
  // instructions to skip.  Note that when we do the skip, we
  // want to land on the last instruction that we *not* want
  // executed, because the main loop will always bump the
  // instruction pointer by one.
  UInt32 skipCount = thenClause.mInstructions.size() - 1;

  // If there is an else clause, then we will need to append
  // another two instruction word to skip over that else clause.
  if (!elseClause.mInstructions.empty())
    skipCount += 2;
  mInstructions.push_back(skipCount);

  for (SInt32 i = 0; i < thenClause.mInstructions.size(); ++i)
    mInstructions.push_back(thenClause.mInstructions[i]);

  // Now put in the code for the else clause, if there is any,
  // including the code that skips over the if clause.
  if (!elseClause.mInstructions.empty())
  {
    skipCount = elseClause.mInstructions.size() - 1;
    mInstructions.push_back(eRelativeJump);
    mInstructions.push_back(skipCount);
    for (SInt32 i = 0; i < elseClause.mInstructions.size(); ++i)
      mInstructions.push_back(elseClause.mInstructions[i]);
  }
} // void Interpreter::addIf
