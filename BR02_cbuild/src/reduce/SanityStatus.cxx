// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "SanityStatus.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"

#include "flow/FLNodeElab.h"

// Force instantiation.
template class SanityStatus<FLNodeElab>;
template class SanityStatus<FLNode>;
template class SanityStatus<NUNet>;
template class SanityStatus<NUNetElab>;

template<class NodeType>
void SanityStatus<NodeType>::addTest(const char * test_name, bool status)
{
  mFailures |= not status;
  mMessages.push_back(MessageInfo(test_name, status));
}


template<class NodeType>
void SanityStatus<NodeType>::addDetail(const UtString& detail)
{
  mMessages.push_back(MessageInfo(detail));
}


template<class NodeType>
void SanityStatus<NodeType>::print(bool verbose) const
{
  if (mFailures or verbose) {
    printHeader();

    UtOStream& out = UtIO::cout();
    out << "    Tests:" << UtIO::endl;
    
    
    for (typename MessageList::const_iterator iter = mMessages.begin();
         iter != mMessages.end();
         ++iter) {
      const MessageInfo& message = *iter;
      switch (message.mStatus) {
      case ePass:   out << "    " << message.mContents.mTest << ": PASS"; break;
      case eFail:   out << "    " << message.mContents.mTest << ": FAIL"; break;
      case eDetail: out << "        " << message.mContents.mDetail;       break;
      }
      out << "\n";
    }
  }
}
