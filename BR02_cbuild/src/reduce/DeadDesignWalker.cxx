// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/DeadDesignWalker.h"
#include "CollectBlocks.h"

#include "nucleus/NUNetSet.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUStructuredProc.h"

#include "localflow/UD.h"

#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"

/*!\file
 * Implementation of design walker which removes dead nucleus.
 */


DeadDesignWalker::DeadDesignWalker(NUNetRefFactory *netref_factory,
				   MsgContext *msg_ctx,
				   IODBNucleus *iodb,
                                   ArgProc* args,
				   DeadDesignCallback* callback,
				   bool verbose) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mArgs(args),
  mCallback(callback),
  mVerbose(verbose)
{
}


DeadDesignWalker::~DeadDesignWalker()
{
}


void DeadDesignWalker::walk(NUDesign *this_design, bool delete_instances)
{
  NucleusCallback my_callback(this, false, delete_instances);
  NUDesignWalker walker(my_callback, mVerbose);
  walker.design(this_design);
}


void DeadDesignWalker::walk(NUModule *this_module, bool delete_instances)
{
  NucleusCallback my_callback(this, true, delete_instances);
  NUDesignWalker walker(my_callback, mVerbose);
  walker.module(this_module);
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase phase,
                                              NUBase *node)
{
  // Delete nodes on the post walk; this is so we can discover any
  // nested blocks which have nets we are going to delete. If we call
  // delete on the pre walk, then we don't walk nesting.
  if (phase == ePost) {
    if (not node->getMark()) {
      mObj->markModification();
      return eDelete;
    }
  }
  return eNormal;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase */,
					      NULvalue * /* node */)
{
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase */,
					      NUExpr * /* node */)
{
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase */,
					      NUCaseItem * /* node */)
{
  return eNormal;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase */,
					      NUCaseCondition * /* node */)
{
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase */,
					      NUTFArgConnection * /* node */)
{
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase */,
					      NUPortConnection * /* node */)
{
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase */,
					      NUCModelArgConnection * /* node */)
{
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase phase,
					      NUDesign * /* design */)
{
  if (phase == ePost) {
    mObj->postWalk();
  }
  return eNormal;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase phase,
					      NUModuleInstance * instance)
{
  if (mDeleteInstances) {
    return operator()(phase, (NUBase*)instance);
  } else {
    return eNormal;
  }
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase phase,
					      NUModule *module)
{
  if (phase == ePre) {
    if (not mContinueModule) {
      return eSkip;
    }
    if (mOnlyOneModule) {
      mContinueModule = false;
      mStartModule = module;
    }
    mObj->preModule(module);
  } else {
    mObj->postModule(module);

    // If we did a module-only walk, then call the post-walk.
    if (mOnlyOneModule && (mStartModule == module)) {
      mObj->postWalk();
    }
  }
  return eNormal;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase phase,
                                              NUStructuredProc *proc)
{
  if (phase == ePost) {
    // If the block is dead, make sure this proc is dead also.
    NUBlock *block = proc->getBlock();
    if (not block->getMark()) {
      NU_ASSERT2(not proc->getMark(), proc, block);
    }

    if (not proc->getMark()) {
      mObj->markModification();
      return eDelete;
    }
  }

  return eNormal;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase phase,
					      NUBlock *block)
{
  if (phase == ePost) {
    if (not block->getMark()) {
      mObj->deadBlock(block);
      mObj->markModification();
      return eDelete;
    }
  }

  return eNormal;
}




NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase -- unused */,
					      NUTask * /* task -- unused */)
{
  // For now, do not delete tasks, because we are not walking the task enables to determine if they
  // are alive or not.
  return eSkip;
}

NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase -- unused */,
					      NUCModel * /* unused */)
{
  // For now, do not delete cmodels
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase -- unused */,
					      NUCModelPort * /* unused */)
{
  // For now, do not delete cmodel ports
  return eSkip;
}


NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase -- unused */,
					      NUCModelFn * /* unused */)
{
  // For now, do not delete cmodel functions
  return eSkip;
}

NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase -- unused */,
					      NUCModelInterface * /* unused */)
{
  // For now, do not delete cmodel interfaces
  return eSkip;
}

NUDesignCallback::Status
DeadDesignWalker::NucleusCallback::operator()(NUDesignCallback::Phase /* phase -- unused */,
					      NUNamedDeclarationScope* /* unused */)
{
  // Do not delete named declaration scopes
  return eSkip;
}


void DeadDesignWalker::markModification() 
{
  mModuleModifiedStack.top() = true;
}


void DeadDesignWalker::postWalk()
{
  // Make sure the remove net list stack is empty
  INFO_ASSERT(mRemoveNetListStack.empty(), "Items left on stack!");

  // Perform final removal of the dead nets.
  removeDeadNets();
}


void DeadDesignWalker::removeDeadNets()
{
  if (mCallback) {
    (*mCallback)(&mRemoveNetList);
  }
  for (NUNetList::iterator iter = mRemoveNetList.begin();
       iter != mRemoveNetList.end();
       ++iter) {
    NUNet *net = *iter;
    mNetRefFactory->erase(net);

    delete net;
  }
}


void DeadDesignWalker::prepareDeadNets()
{
  NUNetList *remove_list = mRemoveNetListStack.top();
  for (NUNetList::iterator iter = remove_list->begin();
       iter != remove_list->end();
       ++iter) {
    NUNet *net = *iter;
    mRemoveNetList.push_back(net);
  }
}

void DeadDesignWalker::preModule(NUModule* /*module*/)
{
  // Keep track of currently-traversed module
  mModuleModifiedStack.push(false);

  // Keep track of which nets are being removed in this module
  mRemoveNetListStack.push(new NUNetList);
}


void DeadDesignWalker::postModule(NUModule *module)
{
  bool module_modified = mModuleModifiedStack.top();

  if (module_modified) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.module(module);
  }

  // Now that UD has been recalculated, search for any block-local
  // nets which are no longer defined by their scope.
  findDeadLocals(module);

  // Delete the dead nets after clearing out the nucleus and fixing up UD.
  prepareDeadNets();

  NUNetList *remove_list = mRemoveNetListStack.top();
  delete remove_list;
  mRemoveNetListStack.pop();

  mModuleModifiedStack.pop();
}


//! Traverse Nucleus searching for referenced nets local to one module.
class DeadLocalCallback : public NUDesignCallback
{
#if ! pfGCC_2
    using NUDesignCallback::operator();
#endif

public:
  //! Constructor
  DeadLocalCallback(NUNetSet * referenced_nets) :
    NUDesignCallback(),
    mReferencedNets(referenced_nets)
  {}

  //! Destructor
  ~DeadLocalCallback() {}

  //! By default, walk through all constructs
  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eNormal; }
  
  //! Remember all nets referenced by an expression.
  Status operator()(Phase phase, NUExpr * expr) {
    if (phase==ePre) {
      expr->getUses(mReferencedNets);
    }
    // we've already learned what we can. don't recurse into this
    // construct.
    return eSkip; 
  }

  //! Remember all nets referenced by an lvalue (def and use).
  Status operator()(Phase phase, NULvalue * lvalue) {
    if (phase==ePre) {
      lvalue->getDefs(mReferencedNets);
      lvalue->getUses(mReferencedNets);
    }
    // we've already learned what we can. don't recurse into this
    // construct.
    return eSkip; 
  }
private:
  NUNetSet * mReferencedNets;
};

void DeadDesignWalker::findDeadLocals(NUModule * module)
{
  // collect all the blocks in the module.
  for (NUBlockLoop loop = module->loopBlocks();
       not loop.atEnd();
       ++loop) {
    NUBlock * block = (*loop);

    NUNetSet referenced;
    DeadLocalCallback callback(&referenced);
    NUDesignWalker walker(callback, false);
    NUDesignCallback::Status status = walker.blockStmt(block);
    NU_ASSERT(status==NUDesignCallback::eNormal,block);

    // Create a new set where all aliases are referenced also.
    // Because, if one net in an alias ring is alive, then
    // all nets in the ring are alive.
    NUNetSet alias_referenced;
    for (NUNetSet::iterator iter = referenced.begin();
         iter != referenced.end();
         ++iter) {
      NUNetList aliases;
      NUNet *net = *iter;
      net->getAliases(&aliases);
      for (NUNetList::iterator iter = aliases.begin();
           iter != aliases.end();
           ++iter) {
        alias_referenced.insert(*iter);
      }
    }

    NUBlockSet blocks;
    CollectBlocks collector(blocks);
    collector.stmt(block);

    for (NUBlockSet::iterator iter = blocks.begin();
	 iter != blocks.end();
	 ++iter) {
      NUBlock * child = (*iter);

      typedef UtHashMap<NUScope*,NUNetSet> ScopeNetSetMap;
      ScopeNetSetMap unreferenced_nets;

      for (NUNetLoop loop = child->loopLocals();
	   not loop.atEnd();
	   ++loop) {
	NUNet * net = (*loop);
	if (net->isBlockLocal() and net->isTemp() and net->isNonStatic()) {
 	  if (alias_referenced.find(net) == alias_referenced.end()) {
 	    unreferenced_nets[net->getScope()].insert(net);
 	  }
	}
      }

      // removal delayed so we do not modify local list while looping the local list.
      for (LoopMap<ScopeNetSetMap> p(unreferenced_nets); !p.atEnd(); ++p) {
        NUScope* scope = p.getKey();
        const NUNetSet& nets = p.getValue();
        scope->removeLocals(nets, false);
      
        for (Loop<const NUNetSet> q(nets); !q.atEnd(); ++q) {
          NUNet* net = *q;
          rememberDeadNet(net);
        }
      }
    }
  }
}


void DeadDesignWalker::deadBlock(NUBlock * block)
{
  // If this is dead, collect the local nets, as we delete them in a separate pass
  // at the end.
  NUNetList this_net_list;
  for (NUNetLoop loop = block->loopLocals(); not loop.atEnd(); ++loop) {
    this_net_list.push_back(*loop);
  }

  NUScope  * parent_scope  = block->getParentScope();
  NUModule * parent_module = block->getModule();
  for (NUNetList::iterator iter = this_net_list.begin();
       iter != this_net_list.end();
       ++iter) {
    NUNet *net = *iter;
    block->disconnectLocal(net);

    // The block containing this net is about to be deleted.
    // Deletion of the net is postponed until after UD recomputation
    // (otherwise, we'd have NUNetRef reference counting problems).
    //
    // Rewrite the scope so callers can still obtain a valid scope
    // pointer. Choose the containing module because there is no
    // guarantee that some other, intermediate, parent scope will
    // persist.

    net->replaceScope(block, parent_module);
    rememberDeadNet(net);
  }

  // Remove this block from the parent.
  parent_scope->removeBlock(block);
}


void DeadDesignWalker::rememberDeadNet(NUNet *net)
{
  NUNetList *remove_list = mRemoveNetListStack.top();
  remove_list->push_back(net);

  if (net->isProtectedMutable(mIODB)) {
    mMsgContext->DeadMutableNet(net);
  }
}

DeadDesignCallback::~DeadDesignCallback() {
}
