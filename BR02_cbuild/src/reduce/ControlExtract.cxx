// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/ControlExtract.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "localflow/UD.h"
#include "localflow/RedundantAssigns.h"

ControlExtract::ControlExtract(AtomicCache * str_cache,
			       NUNetRefFactory * netref_factory,
			       MsgContext * msg_context,
			       IODBNucleus * iodb,
                               ArgProc* args,
			       bool verbose,
			       ControlExtractStatistics *stats) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mStatistics(stats),
  mVerbose(verbose)
{
  mUD = new UD(mNetRefFactory, mMsgContext, iodb, args, false);
}


ControlExtract::~ControlExtract()
{
  delete mUD;
}


bool ControlExtract::module(NUModule * one)
{
  // Do not convert boolean expressions to ternaries; performing this
  // conversion too early caused performance problems with EMC/SYS.
  ControlExtractCallback callback(mNetRefFactory,mUD,mStatistics,true,false);
  NUDesignWalker walker(callback,false);
  NUDesignCallback::Status status = walker.module(one);
  NU_ASSERT(status==NUDesignCallback::eNormal, one);

  if (callback.didWork()) {
    mUD->module(one);
    mStatistics->module();
    return true;
  }
  return false;
}


bool ControlExtract::always(NUAlwaysBlock * alwaysBlock)
{
  // Do not convert boolean expressions to ternaries; performing this
  // conversion too early caused performance problems with EMC/SYS.
  ControlExtractCallback callback(mNetRefFactory,mUD,mStatistics,true,false);
  NUDesignWalker walker(callback,false);
  NUDesignCallback::Status status = walker.structuredProc(alwaysBlock);
  NU_ASSERT(status==NUDesignCallback::eNormal, alwaysBlock);

  if (callback.didWork()) {
    mStatistics->module();
    return true;
  }
  return false;
}


bool ControlExtract::mergedBlocks(NUModule * /*one*/, NUUseDefVector & blocks)
{
  bool any_did_work = false;

  for (NUUseDefVector::iterator iter = blocks.begin();
       iter != blocks.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(use_def);
    NU_ASSERT(always, use_def);

    // Allow the conversion of boolean expressions to ternaries.
    ControlExtractCallback callback(mNetRefFactory,mUD,mStatistics,false,true);
    NUDesignWalker walker(callback,false);
    NUDesignCallback::Status status = walker.alwaysBlock(always);
    NU_ASSERT(status==NUDesignCallback::eNormal, always);

    if (callback.didWork()) {
      mUD->structuredProc(always);
      any_did_work = true;
    }
  }

  if (any_did_work) {
    // We do not need to recompute module-level UD -- we have
    // already updated the modified blocks.
    mStatistics->module();
    return true;
  }
  return false;
}


ControlExtractCallback::ControlExtractCallback(NUNetRefFactory * netref_factory,
					       UD * ud,
					       ControlExtractStatistics * stats,
                                               bool remove_redundancies,
                                               bool convert_boolean_to_ternary) :
  mNetRefFactory(netref_factory),
  mUD(ud),
  mStatistics(stats),
  mRemoveRedundancies(remove_redundancies),
  mConvertBooleanToTernary(convert_boolean_to_ternary),
  mDidWork(false)
{
  mRedundantAssigns = new RedundantAssigns;
}


ControlExtractCallback::~ControlExtractCallback()
{
  delete mRedundantAssigns;
}


NUDesignCallback::Status ControlExtractCallback::operator()(Phase phase, NUBlock *stmt)
{
  if (phase==ePre) {
    NUStmtList replacements;

    bool modified = stmts(stmt->loopStmts(),
			  replacements);
    if (modified) {
      stmt->replaceStmtList(replacements);
    }
  }

  return eNormal;
}


NUDesignCallback::Status ControlExtractCallback::operator()(Phase phase, NUCase *stmt)
{
  if (phase==ePre) {
    for (NUCase::ItemLoop loop = stmt->loopItems(); 
	 not loop.atEnd() ;
	 ++loop) {
      NUCaseItem *item = (*loop);

      NUStmtList replacements;
      bool modified = stmts(item->loopStmts(),
			    replacements);
      if (modified) {
	item->replaceStmts(replacements);
      }
    }
  }
  return eNormal;
}


NUDesignCallback::Status ControlExtractCallback::operator()(Phase phase, NUIf *stmt)
{
  if (phase==ePre) {
    NUStmtList replacements;
    bool modified = stmts(stmt->loopThen(),
			  replacements);
    if (modified) {
      stmt->replaceThen(replacements);
    }

    // clear replacements before processing else stmts.
    replacements.clear();

    modified = stmts(stmt->loopElse(),
		     replacements);
    if (modified) {
      stmt->replaceElse(replacements);
    }
  }
  return eNormal;
}


NUDesignCallback::Status ControlExtractCallback::operator()(Phase phase, NUFor *stmt)
{
  if (phase==ePre) {
    NUStmtList replacements;
    bool modified = stmts(stmt->loopBody(),
			  replacements);
    if (modified) {
      stmt->replaceBody(replacements);
    }
  }
  return eNormal;
}


bool ControlExtractCallback::stmts(NUStmtLoop loop, NUStmtList & replacements)
{
  bool success = false;

  // We perform two passes over the stmtlist. Our first pass searches
  // for sequential if-then-else statements and ternary assignments.
  // It populates the 'if_stmt_replacements' list as it goes.
  NUStmtList if_stmt_replacements;
  success |= searchForAdjacentIfStmts(loop, if_stmt_replacements);

  // The second pass uses that 'if_stmt_replacements' list as its
  // starting point and searches for adjacent case statements. The
  // 'replacements' list is populated by this phase.
  success |= searchForAdjacentCaseStmts(NUStmtLoop(if_stmt_replacements), replacements);
  return success;
}


bool ControlExtractCallback::searchForAdjacentIfStmts(NUStmtLoop loop, NUStmtList & replacements)
{
  bool success = false;

  // Iterate over an ordered list of statements, finding sequential
  // statements with a shared select. 

  // The 'replacements' list needs to contain the union of all
  // unmodified statements and all generated statements. If any
  // optimizations occur, it will be used to replace the body of our
  // parent.

  for ( /*no initial*/ ; not loop.atEnd(); /*no advance*/ ) {
    NUStmt * stmt = (*loop);

    bool saw_ternary_assignment = false;

    NUExpr * rhs = getRhsSelect(stmt, &saw_ternary_assignment);
    if (not rhs) {
      replacements.push_back(stmt);
      ++loop;
      continue;
    }

    // We have found a statement with a valid select. Now, search for
    // statements immediately following this one which share its
    // select.

    NUStmtList ternary_stmts;	// stmts with shared select.
    ternary_stmts.push_back(stmt);

    SInt32 count = 1;		// keep count ourselves -- faster than LIST.size()

    NUStmtLoop inner = loop;
    for ( ++inner ; not inner.atEnd(); ++inner) {
      NUStmt * other = (*inner);
      bool is_a_ternary = false;
      NUExpr * other_rhs = getRhsSelect(other,&is_a_ternary);

      // invalid (or no) select; terminate search.
      if (not other_rhs) break;	

      // inconsistent select; terminate search.
      if ((*rhs)!=(*other_rhs)) break;

      // if we're using this, remember if we've seen a ternary.
      saw_ternary_assignment |= is_a_ternary;

      // use the select with the lowest loc. that's where we get the
      // loc for the new if() stmt.
      if (other_rhs->getLoc() < rhs->getLoc()) {
        rhs = other_rhs;
      }

      ternary_stmts.push_back(other);
      ++count;
    }

    if ((count>1) or (saw_ternary_assignment)) {
      NUIf * if_stmt = createCombinedIf(rhs, ternary_stmts);
      if (mRemoveRedundancies) {
        mRedundantAssigns->stmt(if_stmt);
      }
      mUD->stmt(if_stmt);
      replacements.push_back(if_stmt);
      deleteStmtList(ternary_stmts);

      mStatistics->ifSources(count);
      mStatistics->ifTarget();

      success = true;
      mDidWork = true;
    } else {
      replacements.push_back(stmt);
    }

    loop = inner; // update loop to the next unprocessed item.
  }
  return success;
}


bool ControlExtractCallback::searchForAdjacentCaseStmts(NUStmtLoop loop, NUStmtList & replacements)
{
  bool success = false;

  // Iterate over an ordered list of statements, finding sequential
  // case statements with identical control structure.

  // The 'replacements' list needs to contain the union of all
  // unmodified statements and all generated statements. If any
  // optimizations occur, it will be used to replace the body of our
  // parent.

  for ( /*no initial*/ ; not loop.atEnd(); /*no advance*/ ) {
    NUStmt * stmt = (*loop);

    NUCase * case_stmt = dynamic_cast<NUCase*>(stmt);
    if (not case_stmt) {
      replacements.push_back(stmt);
      ++loop;
      continue;
    }

    bool valid_control = hasValidControl(case_stmt);
    if (not valid_control) {
      replacements.push_back(stmt);
      ++loop;
      continue;
    }      

    // We have found a case statement. Now, search for other case
    // statements which immediately follow this one and share its
    // control structure.

    // Case stmts with identical control. This list does not contain
    // the first case stmt. We add the contents of everything else to
    // the first one and delete the rest.
    NUStmtList adjacent_case_stmts;

    SInt32 count = 1;		// keep count ourselves -- faster than LIST.size()

    NUStmtLoop inner = loop;
    for ( ++inner ; not inner.atEnd(); ++inner) {
      NUStmt * other = (*inner);

      NUCase * other_case_stmt = dynamic_cast<NUCase*>(other);

      // non-case; terminate search.
      if (not other_case_stmt) break;

      // invalid control; terminate search.
      bool valid_control = hasValidControl(other_case_stmt);
      if (not valid_control) break;

      // inconsistent control structure; terminate search
      bool shared_control = haveSharedControl(case_stmt, other_case_stmt);
      if (not shared_control) break;

      adjacent_case_stmts.push_back(other_case_stmt);
      ++count;
    }

    if (count>1) {
      createCombinedCase(case_stmt, adjacent_case_stmts);
      if (mRemoveRedundancies) {
        mRedundantAssigns->stmt(case_stmt);
      }
      mUD->stmt(case_stmt);
      replacements.push_back(case_stmt);
      deleteStmtList(adjacent_case_stmts);

      mStatistics->caseSources(count);
      mStatistics->caseTarget();

      success = true;
      mDidWork = true;
    } else {
      replacements.push_back(stmt);
    }

    loop = inner; // update loop to the next unprocessed item.
  }
  return success;
}


NUExpr * ControlExtractCallback::getRhsSelect(NUStmt * stmt, bool * saw_ternary_assignment)
{
  NUExpr * select = NULL;
  (*saw_ternary_assignment) = false;

  switch(stmt->getType()) {
  case eNUBlockingAssign: {
    NUBlockingAssign * assign = dynamic_cast<NUBlockingAssign*>(stmt);
    NUExpr * rhs = assign->getRvalue();
    switch(rhs->getType()) {
    case NUExpr::eNUTernaryOp: {
      NUTernaryOp * ternary = dynamic_cast<NUTernaryOp*>(rhs);
      (*saw_ternary_assignment) = true;
      select = ternary->getArg(0);
      break;
    }
    case NUExpr::eNUBinaryOp: {
      if (mConvertBooleanToTernary) {
        NUBinaryOp * op = dynamic_cast<NUBinaryOp*>(rhs);
        // Apply the following conversions in the hope that it gets
        // merged with other control:
        //
        // out = (en & data); <=> out = en ? data : 1'b0;
        // out = (nen | data); <=> out = nen ? 1'b1 : data;
        if (rhs->getBitSize()==1) {
          NUOp::OpT op_type = op->getOp();
          if (op_type==NUOp::eBiBitAnd or op_type==NUOp::eBiBitOr) {
            // FIRSTOPSELECT
            // For now, blindly assume the first arg is the control.
            // This assumption is mirrored in createCombinedIf.
            select = op->getArg(0);
          }
        }
      }
      break;
    }
    default: break;
    }
    break;
  }
  case eNUIf: {
    NUIf * if_stmt = dynamic_cast<NUIf*>(stmt);
    select = if_stmt->getCond();
    break;
  }
  default: break;
  }

  if (select) {
    // We have found a statement with a select; if this select is also
    // defined by this statement, the reaching definition is not the
    // same for subsequent statements.

    NUNetRefSet defs(mNetRefFactory);
    NUNetRefSet uses(mNetRefFactory);
    stmt->getBlockingDefs(&defs);
    select->getUses(&uses);
    
    if (NUNetRefSet::set_has_intersection(defs,uses)) {
      // if this assignment defines nets used in the select, disallow.
      select = NULL;
    }
  }

  return select;
}


NUIf * ControlExtractCallback::createCombinedIf(NUExpr * select, NUStmtList & stmts)
{
  CopyContext cc(NULL,NULL);

  NUStmtList then_stmts;
  NUStmtList else_stmts;
  bool usesCFNet = false;

  NU_ASSERT(not stmts.empty(), select);
  SourceLocator loc;
  bool first_statement = true;

  for (NUStmtList::iterator iter = stmts.begin();
       iter != stmts.end();
       ++iter) {
    NUStmt * stmt = (*iter);
    if ( first_statement ){
     // use the location of the first statement since it points back
     // to one of the real sources of the if stmt
      first_statement = false;
      loc = stmt->getLoc();
    }

    usesCFNet |= stmt->getUsesCFNet();

    switch(stmt->getType()) {
    case eNUBlockingAssign: {
      NUBlockingAssign * assign = dynamic_cast<NUBlockingAssign*>(stmt);
      NUExpr * rhs = assign->getRvalue();
      switch(rhs->getType()) {
      case NUExpr::eNUTernaryOp: {
        NUTernaryOp * ternary = dynamic_cast<NUTernaryOp*>(rhs);

        // Generate assignments for the 'then' and 'else' branches of
        // the ternary expression.
        NUAssign * then_assign = new NUBlockingAssign(assign->getLvalue()->copy(cc),
                                                      ternary->getArg(1)->copy(cc),
                                                      assign->getUsesCFNet(),
                                                      ternary->getLoc());
        NUAssign * else_assign = new NUBlockingAssign(assign->getLvalue()->copy(cc),
                                                      ternary->getArg(2)->copy(cc),
                                                      assign->getUsesCFNet(),
                                                      ternary->getLoc());
        then_stmts.push_back(then_assign);
        else_stmts.push_back(else_assign);
        break;
      }
      case NUExpr::eNUBinaryOp: {
        NU_ASSERT(mConvertBooleanToTernary, assign);
        NUBinaryOp * op = dynamic_cast<NUBinaryOp*>(rhs);
        NUOp::OpT op_type = op->getOp();
        NU_ASSERT(((op_type==NUOp::eBiBitAnd or op_type==NUOp::eBiBitOr) and rhs->getBitSize()==1), stmt);

        // FIRSTOPSELECT
        // For now, blindly assume the first arg is the control and
        // the second arg is the data. This assumption is mirrored in
        // getRhsSelect.
        NUAssign * then_assign = NULL;
        NUAssign * else_assign = NULL;
        if (op_type==NUOp::eBiBitAnd) {
          // out = (en & data); <=> out = en ? data : 1'b0;
          then_assign = new NUBlockingAssign(assign->getLvalue()->copy(cc),
                                             op->getArg(1)->copy(cc),
                                             assign->getUsesCFNet(),
                                             op->getLoc());
          else_assign = new NUBlockingAssign(assign->getLvalue()->copy(cc),
                                             NUConst::create (false, 0, 1, op->getLoc()),
                                             assign->getUsesCFNet(),
                                             op->getLoc());
        } else {
          // out = (nen | data); <=> out = nen ? 1'b1 : data;
          NU_ASSERT(op_type==NUOp::eBiBitOr, stmt);
          then_assign = new NUBlockingAssign(assign->getLvalue()->copy(cc),
                                             NUConst::create (false, 1, 1, op->getLoc()),
                                             assign->getUsesCFNet(),
                                             op->getLoc());
          else_assign = new NUBlockingAssign(assign->getLvalue()->copy(cc),
                                             op->getArg(1)->copy(cc),
                                             assign->getUsesCFNet(),
                                             op->getLoc());
        }
        NU_ASSERT((then_assign and else_assign),stmt);
        then_stmts.push_back(then_assign);
        else_stmts.push_back(else_assign);
        break;
      }
      default: NU_ASSERT(false,stmt); break;
      }
      break;
    }
    case eNUIf: {
      NUIf * if_stmt = dynamic_cast<NUIf*>(stmt);

      // If we already have an if() stmt, add its body statements to
      // our combined if() stmt.
      NUStmtList * stmts;
      stmts = if_stmt->getThen();
      then_stmts.insert(then_stmts.end(),stmts->begin(),stmts->end());
      delete stmts;

      stmts = if_stmt->getElse();
      else_stmts.insert(else_stmts.end(),stmts->begin(),stmts->end());
      delete stmts;

      // Empty the branches of the old if() stmt; its body statements
      // will belong to the combined if() stmt.
      NUStmtList empty;
      if_stmt->replaceThen(empty);
      if_stmt->replaceElse(empty);
      break;
    }
    default: NU_ASSERT(false, stmt); break;
    }
  }

  NUIf * if_stmt = new NUIf(select->copy(cc),
			    then_stmts,
			    else_stmts,
			    mNetRefFactory,
                            usesCFNet,
                            loc);
  return if_stmt;
}


bool ControlExtractCallback::hasValidControl(const NUCase * stmt) const
{
  NUNetRefSet defs(mNetRefFactory);
  NUNetRefSet uses(mNetRefFactory);
  stmt->getBlockingDefs(&defs);

  // get the uses from the selector
  NUExpr * select = stmt->getSelect();
  select->getUses(&uses);

  // get the uses from all the conditions.
  for (NUCase::ItemCLoop item_loop = stmt->loopItems();
       not item_loop.atEnd();
       ++item_loop) {
    const NUCaseItem * item = (*item_loop);
    for (NUCaseItem::ConditionCLoop cond_loop = item->loopConditions();
	 not cond_loop.atEnd();
	 ++cond_loop) {
      const NUCaseCondition * condition = (*cond_loop);
      condition->getUses(&uses);
    }
  }

  return not NUNetRefSet::set_has_intersection(defs,uses);
};


bool ControlExtractCallback::haveSharedControl(const NUCase * a, const NUCase * b) const
{
  NUExpr * a_select = a->getSelect();
  NUExpr * b_select = b->getSelect();

  // check that the selector is the same
  bool consistent = ((*a_select)==(*b_select));

  // Check that each condition within each item is the same.
  NUCase::ItemCLoop a_item_loop = a->loopItems();
  NUCase::ItemCLoop b_item_loop = b->loopItems();
  for (/*no initial*/ ; 
       consistent and (not a_item_loop.atEnd()) and (not b_item_loop.atEnd());
       ++a_item_loop,++b_item_loop) {
    const NUCaseItem * a_item = (*a_item_loop);
    const NUCaseItem * b_item = (*b_item_loop);
    NUCaseItem::ConditionCLoop a_cond_loop = a_item->loopConditions();
    NUCaseItem::ConditionCLoop b_cond_loop = b_item->loopConditions();
    for (/*no initial*/; 
	 consistent and (not a_cond_loop.atEnd()) and (not b_cond_loop.atEnd());
	 ++a_cond_loop,++b_cond_loop) {
      const NUCaseCondition * a_condition = (*a_cond_loop);
      const NUCaseCondition * b_condition = (*b_cond_loop);
      consistent = ((*a_condition)==(*b_condition));
    }
    // make sure the number of conditions was the same.
    consistent &= (a_cond_loop.atEnd() and b_cond_loop.atEnd());
  }

  // make sure that the number of items was the same.
  consistent &= (a_item_loop.atEnd() and b_item_loop.atEnd());
  return consistent;
}


void ControlExtractCallback::createCombinedCase(NUCase * a, NUStmtList & others) const
{
  for (NUStmtLoop loop(others); 
       not loop.atEnd();
       ++loop) {
    NUStmt * other = (*loop);
    NUCase * b = dynamic_cast<NUCase*>(other);
    NU_ASSERT(b, other);

    NUCase::ItemLoop a_item_loop = a->loopItems();
    NUCase::ItemLoop b_item_loop = b->loopItems();

    for (/*no initial*/ ; 
         (not a_item_loop.atEnd()) and (not b_item_loop.atEnd());
         ++a_item_loop,++b_item_loop) {
      NUCaseItem * a_item = (*a_item_loop);
      NUCaseItem * b_item = (*b_item_loop);

      // add the stmts from the other item to our target
      NUStmtList * stmts = b_item->getStmts();
      a_item->addStmts(stmts);
      delete stmts;

      // empty the old item; its body statements no belong to the
      // combined case stmt.
      NUStmtList empty;
      b_item->replaceStmts(empty);
    }

    // double-check that the number of items was the same.
    NU_ASSERT2(a_item_loop.atEnd() and b_item_loop.atEnd(), a, b);
  }
}


void ControlExtractStatistics::print() const
{
  UtIO::cout() << "ControlExtract Summary: Optimized " 
	       << mModules << " modules."
	       << UtIO::endl;
  UtIO::cout() << "    Combined " 
	       << mIfSources << " if/ternary statements into "
	       << mIfTargets << " if statements."
	       << UtIO::endl;
  UtIO::cout() << "    Combined " 
	       << mCaseSources << " case statements into "
	       << mCaseTargets << " case statements."
	       << UtIO::endl;
}
