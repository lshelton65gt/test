// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/CodeMotion.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"

#include "nucleus/NUDesignWalker.h"

#include "reduce/AllocAlias.h"
#include "reduce/StmtBlockGraph.h"

#include "util/GraphTranspose.h"

#include "iodb/IODBNucleus.h"
#include "util/SetOps.h"

#include "reduce/Fold.h"

#include "localflow/UD.h"

#include "util/UtIOStream.h"

// define if you want code motion into if control logic.
// currently, this causes btop to miscompare.
#define MOVE_INTO_IF_CONTROL_EXPRESSIONS

// define if you want code motion into if control logic.
#define MOVE_INTO_CASE_SELECT_EXPRESSIONS

CodeMotion::CodeMotion(AtomicCache * /*str_cache*/,
                       NUNetRefFactory *netref_factory,
		       MsgContext * msg_ctx,
		       IODBNucleus * iodb,
                       ArgProc* args,
                       AllocAlias * alloc_alias,
                       Fold * fold,
                       bool move_into_expressions,
                       SInt32 expr_motion_threshold,
		       bool verbose,
		       CodeMotionStatistics *stats) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mArgs(args),
  mFold(fold),
  mStatistics(stats),
  mMoveIntoExpressions(move_into_expressions),
  mExprMotionThreshold(expr_motion_threshold),
  mVerbose(verbose),
  mDebug(false)
{
  mGraphFactory = new StmtBlockGraphFactory(mNetRefFactory, alloc_alias, true);
}

CodeMotion::~CodeMotion()
{
  for (NUStmtList::iterator i=mDeleteList.begin();
       i!=mDeleteList.end();
       ++i) {
    delete *i;
  }

  delete mGraphFactory;
}


bool CodeMotion::module(NUModule * this_module)
{
  bool changed = false;

  // Create a UD object per public entry. UD keeps track of
  // encountered scopes and does not recompute that scope again. If we
  // were to keep a UD object per CodeMotion instance, we might skip
  // updating UD if called multiple times over the same module.
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);

  for (NUModule::AlwaysBlockLoop loop = this_module->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock * always = (*loop);
    bool one_changed = alwaysBlock(always);
    if (one_changed) {
      changed = true;
      // recompute UD only for changing blocks
      ud.structuredProc(always);
    }
  }
  for (NUTaskLoop loop = this_module->loopTasks();
       not loop.atEnd();
       ++loop) {
    NUTask * task = (*loop);
    bool one_changed = tf(task);
    if (one_changed) {
      changed = true;
      // recompute UD only for changing tasks
      ud.tf(task,true);
    }
  }

  if (changed) {
    mStatistics->module();
    return true;
  }
  
  return false;
}


bool CodeMotion::mergedBlocks(NUModule * /*this_module*/,
			      NUUseDefVector & blocks)
{
  bool changed = false;

  // Create a UD object per public entry. UD keeps track of
  // encountered scopes and does not recompute that scope again. If we
  // were to keep a UD object per CodeMotion instance, we might skip
  // updating UD if called multiple times over the same module.
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);

  for (NUUseDefVector::iterator iter = blocks.begin();
       iter != blocks.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(use_def);
    bool one_changed = alwaysBlock(always);
    if (one_changed) {
      changed = true;
      // recompute UD only for changing blocks
      ud.structuredProc(always);
    }
  }
  if (changed) {
    mStatistics->module();
    return true;
  }

  return false;
}


bool CodeMotion::alwaysBlock(NUAlwaysBlock * always)
{
  NUNetRefSet uplevel_uses(mNetRefFactory);
  bool changed = block(always->getBlock(), &uplevel_uses);
  return changed;
}


bool CodeMotion::tf(NUTask * task)
{
  NUNetRefSet uplevel_uses(mNetRefFactory);

  NUStmtList stmts;
  for (NUStmtLoop loop = task->loopStmts();
       not loop.atEnd();
       ++loop) {
    stmts.push_back(*loop);
  }
  bool replace_stmts = false;
  bool changed = stmtList(stmts, &uplevel_uses, replace_stmts);
  if (replace_stmts) {
    task->replaceStmtList(stmts);
  }

  return changed;
}


bool CodeMotion::block(NUBlock * block,
                       const NUNetRefSet *uplevel_uses)
{
  NUStmtList stmts;
  for (NUStmtLoop loop = block->loopStmts();
       not loop.atEnd();
       ++loop) {
    stmts.push_back(*loop);
  }
  bool replace_stmts = false;
  bool changed = stmtList(stmts, uplevel_uses, replace_stmts);
  if (replace_stmts) {
    block->replaceStmtList(stmts);
  }
  return changed;
}


bool CodeMotion::stmt(NUStmt *stmt, const NUNetRefSet *uplevel_uses)
{
  switch (stmt->getType()) {
  case eNUIf:
    return ifStmt(dynamic_cast<NUIf*>(stmt), uplevel_uses);
    break;

  case eNUCase:
    return caseStmt(dynamic_cast<NUCase*>(stmt), uplevel_uses);
    break;

  case eNUFor:
    return forStmt(dynamic_cast<NUFor*>(stmt), uplevel_uses);
    break;

  case eNUBlock:
    return block(dynamic_cast<NUBlock*>(stmt), uplevel_uses);
    break;

  default:
    return false;
    break;
  }
}


bool CodeMotion::ifStmt(NUIf *stmt, const NUNetRefSet *uplevel_uses)
{
  bool replace_stmts_then = false;
  NUStmtLoop loop = stmt->loopThen();
  NUStmtList then_stmts(loop.begin(), loop.end());
  bool work_done_then = stmtList(then_stmts, uplevel_uses, replace_stmts_then);
  if (replace_stmts_then) {
    stmt->replaceThen(then_stmts);
  }

  bool replace_stmts_else = false;
  loop = stmt->loopElse();
  NUStmtList else_stmts(loop.begin(), loop.end());
  bool work_done_else = stmtList(else_stmts, uplevel_uses, replace_stmts_else);
  if (replace_stmts_else) {
    stmt->replaceElse(else_stmts);
  }

  return (work_done_then or work_done_else);
}


bool CodeMotion::caseStmt(NUCase *stmt, const NUNetRefSet *uplevel_uses)
{
  bool work_done = false;

  for (NUCase::ItemLoop item_loop = stmt->loopItems();
       not item_loop.atEnd();
       ++item_loop) {
    bool replace_stmts = false;
    NUCaseItem *item = *item_loop;
    NUStmtLoop stmt_loop = item->loopStmts();
    NUStmtList item_stmts(stmt_loop.begin(), stmt_loop.end());
    bool this_work_done = stmtList(item_stmts, uplevel_uses, replace_stmts);
    work_done |= this_work_done;
    if (replace_stmts) {
      item->replaceStmts(item_stmts);
    }
  }

  return work_done;
}


bool CodeMotion::forStmt(NUFor *stmt, const NUNetRefSet *uplevel_uses)
{
  // Determine all of the visible uses for the advance, condition, and
  // body before processing any part of the loop. Because the advance
  // can execute before and after the body statements, we need to make
  // sure that defs for the used nets are not moved.

  // The initial statements do not contributed uses; they can only
  // satisfy the uses in the advance, condition and body.

  NUNetRefSet visible_uses(mNetRefFactory);
  visible_uses = (*uplevel_uses);

  stmt->getCondition()->getUses(&visible_uses);
  stmt->getVisibleBlockingUses(stmt->loopAdvance(), &visible_uses);
  stmt->getVisibleBlockingUses(stmt->loopBody(),    &visible_uses);

  bool replace_stmts_initial = false;
  NUStmtLoop iloop = stmt->loopInitial();
  NUStmtList initial_stmts(iloop.begin(), iloop.end());
  bool work_done_initial = stmtList(initial_stmts, &visible_uses, replace_stmts_initial);
  if (replace_stmts_initial) {
    stmt->replaceInitial(initial_stmts);
  }

  bool replace_stmts_body = false;
  NUStmtLoop bloop = stmt->loopBody();
  NUStmtList body_stmts(bloop.begin(), bloop.end());
  bool work_done_body = stmtList(body_stmts, &visible_uses, replace_stmts_body);
  if (replace_stmts_body) {
    stmt->replaceBody(body_stmts);
  }

  bool replace_stmts_advance = false;
  NUStmtLoop aloop = stmt->loopAdvance();
  NUStmtList advance_stmts(aloop.begin(), aloop.end());
  bool work_done_advance = stmtList(advance_stmts, &visible_uses, replace_stmts_advance);
  if (replace_stmts_advance) {
    stmt->replaceAdvance(advance_stmts);
  }
  
  return (work_done_initial or work_done_body or work_done_advance);
}


bool CodeMotion::stmtList(NUStmtList & stmts,
                          const NUNetRefSet *uplevel_uses,
                          bool &replace_stmts)
{
  bool changed = false;

  // Move statements into siblings without recursion.
  replace_stmts = stmtListLocal(stmts,uplevel_uses);

  changed |= replace_stmts;

  // Recursively process the statements.
  changed |= stmtListRecurse(stmts, uplevel_uses);

  return changed;
}


class CodeMotionWalker : public GraphWalker
{
public:
  CodeMotionWalker(UtList<GraphNode*> * dfs_post_order) :
    mDFSPostOrder(dfs_post_order) 
  {}

  ~CodeMotionWalker() {}

  Command visitNodeAfter(Graph * /*graph*/, GraphNode * node) {
    mDFSPostOrder->push_back(node);
    return GW_CONTINUE;
  }
  
  UtList<GraphNode*> * mDFSPostOrder;
};


bool CodeMotion::stmtListLocal(NUStmtList & stmts,
                               const NUNetRefSet * uplevel_uses)
{
  bool changed = false;

  StmtBlockGraph *graph = mGraphFactory->create(&stmts);

  // We immediately transpose this graph because we want to walk fanin
  // arcs. The StmtBlockGraph is general-purpose and contains fanout
  // arcs by default.
  UtGraphTranspose<StmtBlockGraph> transposer(graph);
  transposer.transpose();

  if (mDebug) {
    StmtBlockGraphDotWriter::writeGraphToStream(graph,UtIO::cout());
  }

  // TBD: assert that this graph is non-cyclic.
  
  UtList<GraphNode*> dfs_post_order;
  CodeMotionWalker walker(&dfs_post_order);
  walker.walk(graph);

  // Uplevel uses are trailing uses which occur at a higher level in
  // the Nucleus tree. In the following example, we will see that
  // there is a trailing use for 'a' and will not move it into the
  // inner if statement.
  //
  // if (en) begin
  //   a = c & d;
  //   if (b)
  //     e = ! a;
  // end
  // out = a;
  //
  // The unsatisfied uplevel uses are the uplevel uses which have not
  // been satisfied by a kill:
  //
  // if (en) begin
  //   a = c & d;
  //   if (b)
  //     e = ! a;
  //   a = f & g;
  // end
  // out = a;
  //
  // Here, the second assignment to 'a' allows motion of the first
  // into the if(b) statement.
  NUNetRefSet unsatisfied_uplevel_uses(mNetRefFactory);
  unsatisfied_uplevel_uses = (*uplevel_uses);

  // Encountering a kill means that preceding defs for that net can be
  // considered for motion. For example:
  //
  // a = 0;
  // out = x & y;
  // if (en) a = !out;
  // out = !a;
  // 
  // We can move the first assignment to out into the if statement
  // because it has no visibility requirements.
  NUNetRefSet local_kills(mNetRefFactory);

  UInt32 current_position = 0;
  StmtPositionMap position;
  StmtBlockGraphNodeToNodeMap destination_map;
  for (UtList<GraphNode*>::iterator i = dfs_post_order.begin();
       i != dfs_post_order.end();
       ++i) {
    StmtBlockGraph::Node* stmt_node = graph->castNode(*i);
    StmtBlockStmtData * stmt_data = stmt_node->getData();
    if (stmt_data->isStatementComponent()) {
      continue; // this is a component of an if/case. it can't be moved by itself.
    }

    // Save away the set of kills for this statement. The merging
    // operation may end up deleting this statement.
    NUStmt * stmt = stmt_data->getStmt();
    NUNetRefSet stmt_kills(mNetRefFactory);
    stmt->getBlockingKills(&stmt_kills);

    // Remember the position of this statement.
    position[stmt] = ++current_position;

    // update all arcs based on known merges.
    updateDestinations(graph,stmt_node,&destination_map);

    // look to see if we're mergable.
    changed |= mergeNodes(graph,stmt_node,local_kills,unsatisfied_uplevel_uses,
                          &position,&destination_map);

    // Remove the kills for this statement from the set of uplevel
    // uses. Once we've seen the kill, it's no longer needed
    // externally.
    NUNetRefSet::set_subtract(stmt_kills, unsatisfied_uplevel_uses);
    local_kills.insert(stmt_kills.begin(), stmt_kills.end());
  }

  if (changed) {
    // We've made changes; update the statement list to contain only
    // the unmerged statements. Preserve the original statement order.

    // First, determine the modified statements by iterating over our
    // rewrite map (which contains only rewrite rules).
    NUStmtSet  moved;
    for (StmtBlockGraphNodeToNodeMap::iterator i = destination_map.begin();
         i != destination_map.end(); 
         ++i) {
      StmtBlockGraph::Node* stmt_node = i->first;
      StmtBlockStmtData * stmt_data = stmt_node->getData();
      NUStmt * stmt = stmt_data->getStmt();
      moved.insert(stmt);
    }

    // Second, determine the unmoved statments, using the original
    // statement order.
    NUStmtList unmoved;
    for (NUStmtList::iterator i = stmts.begin();
         i != stmts.end();
         ++i) {
      NUStmt * stmt = (*i);
      if (moved.find(stmt) == moved.end()) {
        unmoved.push_back(stmt);
      }
    }

    // Update the list for return.
    stmts = unmoved;
  }

  mGraphFactory->destroy(&graph);
  return changed;
}


void CodeMotion::updateDestinations(StmtBlockGraph * graph, 
                                    StmtBlockGraph::Node * node,
                                    StmtBlockGraphNodeToNodeMap * destination_map)
{
  if (destination_map->empty()) {
    return;
  }

  // update our fanout arcs to reflect performed merges
  for (Iter<GraphEdge*> iter = graph->edges(node);
       not iter.atEnd();
       ++iter) {
    StmtBlockGraph::Edge *edge = graph->castEdge(*iter);
    StmtBlockGraph::Node *to_node = graph->castNode(graph->endPointOf(edge));
    StmtBlockGraphNodeToNodeMap::iterator location = destination_map->find(to_node);
    if (location != destination_map->end()) {
      StmtBlockGraph::Node *to_node_replacement = location->second;
      graph->mutateEndPoint(edge, to_node_replacement);
    }
  }
}


StmtBlockGraph::Node * CodeMotion::findMergableDestination(StmtBlockGraph * graph, 
                                                           StmtBlockGraph::Node * node,
                                                           StmtPositionMap * position)
{
  // We process the edge set twice. First, we check to see if all RAW
  // edges terminate at the same destination. Given that destination,
  // we check to see that no WAW and WAR dependency is invalidated by
  // using this destination.

  // With an if statement, for example, we might have a RAW edge
  // pointing to the THEN clause and a WAW edge pointing to the ELSE
  // clause. We can legally move into the THEN clause, as this would
  // be in a parallel position to the ELSE clause.
  
  // a = b & c;               if (en) begin
  // if (en) begin              a = b & c;
  //   out = a;        ==>      out = a;
  // end else begin           end else begin
  //   a = d & e;               a = d & e;
  //   out = a;                 out = a;
  // end                      end

  StmtBlockGraph::Node * destination = NULL;
  for (Iter<GraphEdge*> iter = graph->edges(node);
       not iter.atEnd();
       ++iter) {
    StmtBlockGraph::Edge *edge = graph->castEdge(*iter);
    StmtBlockGraph::Node *to_node = graph->castNode(graph->endPointOf(edge));
    StmtBlockStmtData * to_data = to_node->getData();
    if (to_data->isCompoundStatement()) {
      continue; // this is an if/case statement. consider its parts independently.
    }

    // If we ever see a RAW edge; this means that we have a consumer for the def.
    if (graph->allFlagsSet(edge, StmtBlockGraph::USE_AFTER|StmtBlockGraph::DEF)) {
      // RAW dependency.
      if (destination) {
        if (to_node != destination) {
          // multiple destinations. unmergable.
          return NULL;
        }
      } else {
        destination = to_node;
      }
    }
  }

  if (not destination) {
    return NULL;
  }

  StmtBlockStmtData * destination_data = destination->getData();
  NUStmt * destination_stmt = destination_data->getStmt();

  for (Iter<GraphEdge*> iter = graph->edges(node);
       not iter.atEnd();
       ++iter) {
    StmtBlockGraph::Edge *edge = graph->castEdge(*iter);
    StmtBlockGraph::Node *to_node = graph->castNode(graph->endPointOf(edge));
    StmtBlockStmtData * to_data = to_node->getData();
    if (to_data->isCompoundStatement()) {
      continue; // this is an if/case statement. consider its parts independently.
    }

    // Make sure that WAW and WAR edges are not violated by moving
    // into our identified position. We do not allow motion into a
    // destination which comes after any WAW/WAR point. Doing so could
    // change the value reaching subsequent reads.

    if (graph->allFlagsSet(edge, StmtBlockGraph::DEF_AFTER|StmtBlockGraph::DEF) or // WAW dependency.
        graph->allFlagsSet(edge, StmtBlockGraph::DEF_AFTER|StmtBlockGraph::USE)) { // WAR dependency.
      if (to_node != destination) {
        NUStmt * to_stmt = to_data->getStmt();
        UInt32 destination_position = (*position)[destination_stmt];
        UInt32 dependency_position  = (*position)[to_stmt];

        // Both statements should have a known position because
        // statements are processed in reverse-dependency order.
        NU_ASSERT(destination_position>0, destination_stmt);
        NU_ASSERT(dependency_position>0, to_stmt);

        if (destination_position < dependency_position) {
          // This edge references a destination node which follows a
          // WAW or WAR dependency node. We cannot move our statement
          // past its dependency.
          return NULL;
        }
      }
    } else {
      // Ignore WAW edges. If there's really an important use which follows, it should have a RAW dependency.
      INFO_ASSERT(graph->allFlagsSet(edge, StmtBlockGraph::USE_AFTER|StmtBlockGraph::DEF),  // RAW dependency.
                  "Unexpected edge type.");
    }
  }
  return destination;
}


void CodeMotion::rememberDestination(StmtBlockGraph::Node * node,
                                     StmtBlockGraph::Node * destination,
                                     StmtBlockGraphNodeToNodeMap * destination_map)
{
  destination_map->insert(StmtBlockGraphNodeToNodeMap::value_type(node,destination));

  StmtBlockStmtData * node_data = node->getData();
  if (node_data->isCompoundStatement()) {
    // If we are merging a compound statement into something else,
    // add rewrite rules for all of the related components.
    StmtBlockCompoundStmtData * compound_data = dynamic_cast<StmtBlockCompoundStmtData*>(node_data);
    NU_ASSERT(compound_data, node_data->getStmt());

    for (StmtBlockCompoundStmtData::ComponentLoop loop = compound_data->loopComponents();
         not loop.atEnd();
         ++loop) {
      StmtBlockGraph::Node * component_node = (*loop);
      destination_map->insert(StmtBlockGraphNodeToNodeMap::value_type(component_node,destination));
    }
  }
}                                    


bool CodeMotion::mergeNodes(StmtBlockGraph * graph, 
                            StmtBlockGraph::Node * node,
                            const NUNetRefSet & local_kills,
                            const NUNetRefSet & uplevel_uses,
                            StmtPositionMap * position,
                            StmtBlockGraphNodeToNodeMap * destination_map)
{
  // Find a single fanout which is legal for motion.
  StmtBlockGraph::Node * destination = findMergableDestination(graph, node, position);
  if (not destination) {
    return false;
  }

  // mergable, according to edges. need to check other qualifiers.
  StmtBlockStmtData * node_data = node->getData();
  NUStmt * candidate_stmt = node_data->getStmt();
  NUNetRefSet defs(mNetRefFactory);
  candidate_stmt->getBlockingDefs(&defs);

  // this statement defines something used by a later statement in a
  // parent scope. we can't move its computation.
  if (NUNetRefSet::set_has_intersection(defs,uplevel_uses)) {
    return false;
  }

  if (not qualifyDefsForMotion(defs, local_kills)) {
    return false;
  }

  // perform the actual merge.
  bool moved = moveInto(destination, candidate_stmt, false);

  if (moved) {
    NUNetRefSet uses(mNetRefFactory);
    candidate_stmt->getBlockingUses(&uses);

    // Update UD so we look locally at least sorta right. This is
    // necessary because we need our uses to be close to accurate (in
    // this case, a superset) for correct computation of uplevel_uses.
    StmtBlockStmtData * destination_data = destination->getData();
    NUStmt * destination_stmt = destination_data->getStmt();
    promoteUD(destination_stmt, defs, uses);

    // Remember who we merged with. We use this information to rewrite
    // incoming arcs to the current node.
    rememberDestination(node, destination, destination_map);

    return true;
  } else {
    return false;
  }
}


bool CodeMotion::stmtListRecurse(NUStmtList & stmts,
                                 const NUNetRefSet *uplevel_uses)
{
  bool changed = false;

  /* Keep track of nets which are used locally and uplevel.
   * This set can prohibit moving of stmts inside nested constructs:
   * begin
   *   z = a + b;
   *   if (sel1)
   *   begin
   *     y = c + d;
   *     if (sel2)
   *     begin
   *       x = y + e;
   *     end
   *   end
   *   v = y + f;
   * end
   *
   * We cannot move 'y = c + d;' into 'if (sel2)' because the y value
   * is used by the 'v = y + f;' statement.
   */
  NUNetRefSet local_uses(mNetRefFactory);
  local_uses = *uplevel_uses;

  for (NUStmtList::reverse_iterator iter = stmts.rbegin();
       iter != stmts.rend();
       ++iter) {
    NUStmt * candidate = (*iter);

    // Recurse to move nested stmts first (accurate local_uses information)
    changed |= stmt(candidate, &local_uses);

    // Update the local uses set; get rid of anything which this
    // statement kills, add anything which this statement uses.
    NUNetRefSet kills(mNetRefFactory);
    candidate->getBlockingKills(&kills);
    NUNetRefSet::set_subtract(kills,local_uses);

    candidate->getBlockingUses(&local_uses);
  }

  return changed;
}


bool CodeMotion::qualifyDefsForMotion(const NUNetRefSet & defs, const NUNetRefSet & kills) const
{
  bool qualified = true;
  for (NUNetRefSet::const_iterator iter = defs.begin();
       qualified and iter != defs.end();
       ++iter) {
    const NUNetRefHdl net_ref = (*iter);
    const NUNet * net = net_ref->getNet();

    // Only allow motion of non-statics. These have been proven (by
    // Rescope) to not have external uses. The isPort qualifier makes
    // sure we do not allow the movement of task/function ports.
    if (net->isNonStatic() and not net->isPort()) {
      // nop.
    } else if (net->isProtected(mIODB)) {
      // disqualify all protected nets.
      qualified = false;
    } else {
      // If we've seen a local kill for this def, we know that the
      // current statement cannot reach an external consumer.
      NUNetRefSet::const_iterator pos = kills.find(net_ref,&NUNetRef::covered);
      qualified = (pos!=kills.end());
    }
  }
  return qualified;
}


bool CodeMotion::usedBy(NUNetRefSet & net_refs, NUExpr * expr)
{
  NUNetRefSet uses(mNetRefFactory);
  expr->getUses(&uses);
  return NUNetRefSet::set_has_intersection(net_refs,uses);
}


bool CodeMotion::usedBy(NUNetRefSet & net_refs, NULvalue * lhs)
{
  NUNetRefSet uses(mNetRefFactory);
  lhs->getUses(&uses);
  return NUNetRefSet::set_has_intersection(net_refs,uses);
}


bool CodeMotion::moveInto(StmtBlockGraph::Node * destination, 
                          NUStmt * candidate,
                          bool dumped_header)
{
  StmtBlockStmtData * destination_data = destination->getData();
  NUStmt * destination_stmt = destination_data->getStmt();

  bool i_moved = false;
  if (destination_stmt->getType()==eNUIf) {
    NU_ASSERT(destination_data->isStatementComponent(), destination_stmt);
    StmtBlockIfComponentData * if_component_data = dynamic_cast<StmtBlockIfComponentData*>(destination_data);
    NU_ASSERT(if_component_data, destination_stmt);
    i_moved = moveIntoIf(if_component_data, candidate, dumped_header);
  } else if (destination_stmt->getType()==eNUCase) {
    NU_ASSERT(destination_data->isStatementComponent(), destination_stmt);
    StmtBlockCaseComponentData * case_component_data = dynamic_cast<StmtBlockCaseComponentData*>(destination_data);
    NU_ASSERT(case_component_data, destination_stmt);
    i_moved = moveIntoCase(case_component_data, candidate, dumped_header);
  } else if (destination_stmt->getType()==eNUBlock) {
    NUBlock * block_stmt = dynamic_cast<NUBlock*>(destination_stmt);
    i_moved = moveIntoBlock(block_stmt, candidate, dumped_header);
  } else if (destination_stmt->getType()==eNUBlockingAssign) {
    NUBlockingAssign * assign = dynamic_cast<NUBlockingAssign*>(destination_stmt);
    i_moved = moveIntoAssign( assign, candidate, dumped_header);
  } else {
    i_moved = false;
  }
  return i_moved;
}


//! Class to determine if there is a single consumer of a provided LHS.
/*!
  During a design walk, this callback will return eStop upon finding
  multiple occurances of the net referenced by the provided LHS.
 */
class MotionSingleIdentCallback : public NUDesignCallback
{
public:
  //! Constructor
  MotionSingleIdentCallback(NULvalue * search_lhs) :
    mSearchNet(search_lhs->getWholeIdentifier()),
    mSawIdentifier(false) 
  {
    NU_ASSERT(mSearchNet,search_lhs);
  }

  Status operator()(Phase /*phase*/, NUBase* /*node*/) {
    return eNormal;
  }

  //! Process full RHS identifiers.
  /*!
    Return eStop if this is our search-net and we have previously
    encountered this net.
   */
  Status operator()(Phase phase, NUIdentRvalue * identifier) { 
    if (phase==ePre) {
      NUNet * net = identifier->getWholeIdentifier();
      if (net == mSearchNet) {
        if (mSawIdentifier) {
          // We have seen this identifier multiple times.
          return eStop;
        }
        mSawIdentifier = true;
      }
    }
    return eNormal;
  }

  bool sawIdentifier() const { return mSawIdentifier; }

private:
  NUNet * mSearchNet;
  bool    mSawIdentifier;
};


// Counts the number of operations in the expression to compute
// the weight of expression. Binary, Unary and Ternary are all
// simplistically assumed to have the same weight of 1 operation.
class HugeExprFinderCallback : public NUDesignCallback
{
public:
  HugeExprFinderCallback(SInt32 threshold)
    : mThreshold(threshold), mWeight(0)
  {
  }

  ~HugeExprFinderCallback()
  {
  }

  Status operator()(Phase /*phase*/, NUBase* /*node*/) {
    return eNormal;
  }

  Status operator()(Phase phase, NUOp* /*op*/) {
    Status stat = eNormal;
    if (phase == ePre) {
      ++mWeight;
      if (isHuge()) {
        stat = eStop;
      }
    }
    return stat;
  }

  bool isHuge() {
    return (mWeight > mThreshold);
  }

private:
  SInt32 mThreshold;
  SInt32 mWeight;
};


//! Replace RHS occurances of a specified LHS with an arbitrary expression.
class MotionSingleIdentReplacer : public NuToNuFn
{
public:
  MotionSingleIdentReplacer(NULvalue * search_lhs, 
                            NUExpr   * replacement) :
    mSearchNet(search_lhs->getWholeIdentifier()),
    mReplacement(replacement),
    mReplaced(false)
  {
    NU_ASSERT(mSearchNet,search_lhs);
  }

  NUExpr * operator()(NUExpr * expr, Phase phase) {
    // Replace on the way out of an expression because we do not want
    // to try rewriting under our replacement. For example, assume we
    // have "x = x + 1" and are trying to replace the use of 'x' in
    // the assignment: 'y = x + z'. We CANNOT search for rewrites once
    // we have replaced 'x', as the reaching definition will have
    // changed.
    if (isPost(phase)) {
      if (expr->isWholeIdentifier() and expr->getWholeIdentifier()==mSearchNet) {
        NU_ASSERT2(not mReplaced,mSearchNet,expr);
        mReplaced = true;
        CopyContext cc(NULL,NULL);
        NUExpr * replacement = mReplacement->copy(cc);
        // Size to the thing we are replacing.
        replacement->resize(expr->getBitSize());
        replacement->setSignedResult (expr->isSignedResult ());
        delete expr;
        return replacement;
      }
    }
    return NULL;
  }

  bool replaced() const { return mReplaced; }
private:
  NUNet    * mSearchNet;
  NUExpr   * mReplacement;
  bool mReplaced;
};

bool CodeMotion::qualifyTargetExpr(NUExpr* expr)
{
  // Limit the size of candidate expressions for move to given threshold.
  // Constructing large expressions hurts gcc compile time.
  HugeExprFinderCallback callback(mExprMotionThreshold);
  NUDesignWalker walker(callback, false);
  walker.expr(expr);
  
  if (callback.isHuge()) {
    return false;
  }
  return true;
}

bool CodeMotion::qualifyCandidateAssign(NUStmt * candidate)
{
  // Make sure this is an assign.
  if (candidate->getType()!=eNUBlockingAssign) {
    return false;
  }

  NUBlockingAssign * candidate_assign = dynamic_cast<NUBlockingAssign*>(candidate);

  // Check that the candidate assignment defines an entire net.
  NULvalue * candidate_lhs = candidate_assign->getLvalue();
  if (not candidate_lhs->isWholeIdentifier()) {
    return false;
  }

  NUNet  * candidate_def = candidate_lhs->getWholeIdentifier();
  if (candidate_def->is2DAnything()) {
    // For now, disable copy propagation of memory assigns. They may
    // not reduce the cost. In the case of temporaries, we would also
    // need to worry about updating masters.
    return false;
  }

  return true;
}


NUExpr * CodeMotion::extendRHS(NULvalue * lhs, NUExpr * rhs)
{
  CopyContext cc(NULL,NULL);
  NUNet * lhs_net = lhs->getWholeIdentifier();
  NU_ASSERT(lhs_net,lhs);
  NUExpr * extended_expr = new NUBinaryOp ( lhs_net->isSigned() ? NUOp::eBiVhExt : NUOp::eBiVhZxt,
                                            rhs->copy(cc),
                                            NUConst::create(false, rhs->getBitSize(), 32, rhs->getLoc()),
                                            rhs->getLoc() );
  extended_expr->resize(rhs->getBitSize());
  return extended_expr;
}


bool CodeMotion::rewriteTargetAssign(NUAssign * target,
                                     NUStmt * candidate)
{
  if (not qualifyCandidateAssign(candidate)) {
    return false;
  }

  if (not qualifyTargetExpr(target->getRvalue())) {
    return false;
  }

  // The target expression must use the candidate def as a full
  // identifier.
  NUBlockingAssign * candidate_assign = dynamic_cast<NUBlockingAssign*>(candidate);
  NULvalue * candidate_lhs = candidate_assign->getLvalue();
  NUExpr   * candidate_rhs = candidate_assign->getRvalue();

  MotionSingleIdentCallback callback(candidate_lhs);
  NUDesignWalker walker(callback,false);
  walker.putWalkTasksFromTaskEnables(false);
  NUDesignCallback::Status status = walker.assign(target);

  if (status==NUDesignCallback::eNormal and callback.sawIdentifier()) {

    // Extend the RHS to protect it from any resizing unless there is
    // a real involved, in which case everything is already explicit
    // in the Nucleus tree.
    NUExpr * replacement_rhs = candidate_rhs;
    bool involves_real = (candidate_lhs->isReal() or candidate_rhs->isReal());
    if (not involves_real) {
      replacement_rhs = extendRHS(candidate_lhs, candidate_rhs);
    }

    // Replace a single ident RHS for this net.
    MotionSingleIdentReplacer replacer(candidate_lhs,
                                       replacement_rhs);

    NULvalue * target_lhs = target->getLvalue();
    target_lhs->replaceLeaves(replacer);
    target_lhs = mFold->fold(target_lhs);
    target->replaceLvalue(target_lhs, false);

    
    NUExpr * target_rhs = target->getRvalue();
    target_rhs = target_rhs->translate(replacer);
    target_rhs = mFold->fold(target_rhs);
    target->replaceRvalue(target_rhs);
    mFold->clearExprFactory();

    NU_ASSERT2(replacer.replaced(),target,candidate_assign);

    if (not involves_real) {
      delete replacement_rhs;
    }

    return true;
  }
  return false;
}


NUExpr * CodeMotion::rewriteTargetExpr(NUExpr * target,
                                       NUStmt * candidate)
{
  if (not qualifyCandidateAssign(candidate)) {
    return false;
  }

  if (not qualifyTargetExpr(target)) {
    return false;
  }

  // The target expression must use the candidate def as a full
  // identifier.
  NUBlockingAssign * candidate_assign = dynamic_cast<NUBlockingAssign*>(candidate);
  NULvalue * candidate_lhs = candidate_assign->getLvalue();
  NUExpr   * candidate_rhs = candidate_assign->getRvalue();

  MotionSingleIdentCallback callback(candidate_lhs);
  NUDesignWalker walker(callback,false);
  walker.putWalkTasksFromTaskEnables(false);
  NUDesignCallback::Status status = walker.expr(target);

  if (status==NUDesignCallback::eNormal and callback.sawIdentifier()) {

    // Extend the RHS to protect it from any resizing unless there is
    // a real involved, in which case everything is already explicit
    // in the Nucleus tree.
    NUExpr * replacement_rhs = candidate_rhs;
    bool involves_real = (candidate_lhs->isReal() or candidate_rhs->isReal());
    if (not involves_real) {
      replacement_rhs = extendRHS(candidate_lhs, candidate_rhs);
    }

    // Replace a single ident RHS for this net.
    MotionSingleIdentReplacer replacer(candidate_lhs,
                                       replacement_rhs);
    NUExpr * tmp;
    tmp = replacer(target, NuToNuFn::ePre);
    if (tmp) { target = tmp; }

    target->replaceLeaves(replacer);

    tmp = replacer(target, NuToNuFn::ePost);
    if (tmp) { target = tmp; }

    NU_ASSERT2(replacer.replaced(),target,candidate_assign);

    if (not involves_real) {
      delete replacement_rhs;
    }

    return mFold->fold(target);
  }
  return NULL;
}


bool CodeMotion::moveIntoAssign(NUBlockingAssign * assign, 
                                NUStmt * candidate, 
                                bool dumped_header)
{
  if (not mMoveIntoExpressions) {
    return false;
  }
  if (candidate->getType()!=eNUBlockingAssign) {
    return false;
  }

  bool did_replacement = rewriteTargetAssign(assign, candidate);

  if (did_replacement) {
    mStatistics->expression();

    if (mVerbose) {
      dumpMotion(candidate, assign->getLoc(), "assign", NULL, dumped_header);
    }

    mDeleteList.push_back(candidate);
    return true;
  } else {
    return false;
  }
}


bool CodeMotion::moveIntoIf(StmtBlockIfComponentData * if_component_data,
                            NUStmt * candidate, 
                            bool dumped_header)
{
  NUIf * if_stmt = dynamic_cast<NUIf*>(if_component_data->getStmt());

  switch(if_component_data->getComponentType()) {
  case StmtBlockIfComponentData::CONDITION: {
#ifdef MOVE_INTO_IF_CONTROL_EXPRESSIONS
    if (not mMoveIntoExpressions) {
      return false;
    }

    // used by the condition and neither of the branches.
    NUExpr * replacement_sel = rewriteTargetExpr(if_stmt->getCond(), candidate);
    if (replacement_sel) {
      // original select has already been deleted if necessary.
      if_stmt->replaceCond(replacement_sel);

      mStatistics->expression();
      if (mVerbose) {
        dumpMotion(candidate, if_stmt->getLoc(), "if", "'condition'", dumped_header);
      }

      mDeleteList.push_back(candidate);
      return true;
    } 
#endif
    return false;
    break;
  }
  case StmtBlockIfComponentData::THEN_CLAUSE: {
    mStatistics->statement();
    if (mVerbose) {
      dumpMotion(candidate, if_stmt->getLoc(), "if", "'then' branch", dumped_header);
    }

    if_stmt->addThenStmtStart(candidate);
    return true;
    break;
  }
  case StmtBlockIfComponentData::ELSE_CLAUSE: {
    mStatistics->statement();
    if (mVerbose) {
      dumpMotion(candidate, if_stmt->getLoc(), "if", "'else' branch", dumped_header);
    }

    if_stmt->addElseStmtStart(candidate);
    return true;
    break;
  }
  }
  return false; // silence GCC!
}


bool CodeMotion::moveIntoCase(StmtBlockCaseComponentData * case_component_data,
                              NUStmt * candidate,
                              bool dumped_header)
{
  NUCase     * case_stmt = dynamic_cast<NUCase*>(case_component_data->getStmt());
  NUCaseItem * case_item = case_component_data->getItem();

  switch(case_component_data->getComponentType()) {
  case StmtBlockCaseComponentData::SELECT: {
    NU_ASSERT(case_item==NULL, case_stmt);
#ifdef MOVE_INTO_CASE_SELECT_EXPRESSIONS
    if (not mMoveIntoExpressions) {
      return false;
    }
    // used by the condition and neither of the branches.
    NUExpr * replacement_sel = rewriteTargetExpr(case_stmt->getSelect(), candidate);
    if (replacement_sel) {
      // original select has already been deleted if necessary.
      case_stmt->replaceSelect(replacement_sel);

      mStatistics->expression();
      if (mVerbose) {
        dumpMotion(candidate, case_stmt->getLoc(), "case", "'select'", dumped_header);
      }

      mDeleteList.push_back(candidate);
      return true;
    } 
#endif
    return false;
    break;
  }
  case StmtBlockCaseComponentData::ITEM_CONDITION: {
    NU_ASSERT(case_item!=NULL, case_stmt);
    // TBD. Condition replacement.
    return false;
    break;
  }
  case StmtBlockCaseComponentData::ITEM_CLAUSE: {
    NU_ASSERT(case_item!=NULL, case_stmt);
    mStatistics->statement();
    if (mVerbose) {
      dumpMotion(candidate, case_stmt, case_item, dumped_header);
    }

    case_item->addStmtStart(candidate);
    return true;
    break;
  }
  }
  return false; // silence GCC!
}


bool CodeMotion::moveIntoBlock(NUBlock * block_stmt,
                               NUStmt * candidate, 
                               bool dumped_header)
{
  mStatistics->statement();
  if (mVerbose) {
    dumpMotion(candidate, block_stmt->getLoc(), "block", NULL, dumped_header);
  }

  block_stmt->addStmtStart(candidate);

  return true;
}


void CodeMotion::promoteUD(NUStmt * parent,
                           NUNetRefSet & candidate_defs,
                           NUNetRefSet & candidate_uses)
{
  // Add the use and def information to our new container.
  for (NUNetRefSet::iterator def_iter = candidate_defs.begin();
       def_iter != candidate_defs.end();
       ++def_iter) {
    NUNetRefHdl def_net_ref = (*def_iter);
    parent->addBlockingDef(def_net_ref);
    for (NUNetRefSet::iterator use_iter = candidate_uses.begin();
	 use_iter != candidate_uses.end();
	 ++use_iter) {
      NUNetRefHdl use_net_ref = (*use_iter);
      parent->addBlockingUse(def_net_ref,use_net_ref);
    }
  }
}


void CodeMotion::dumpMotionHeader(NUStmt * candidate)
{
  UtIO::cout() << "CodeMotion: Moving statement: ";
  candidate->getLoc().print();
  UtIO::cout() << UtIO::endl;
}


void CodeMotion::dumpMotion(NUStmt * candidate, NUCase * case_stmt, NUCaseItem * item, bool dumped_header)
{
  if (not dumped_header) {
    dumpMotionHeader(candidate);
  }
  UtIO::cout() << "    into branch ";
  item->getLoc().print();
  UtIO::cout() << " of case: ";
  case_stmt->getLoc().print();
  UtIO::cout() << UtIO::endl;
}


void CodeMotion::dumpMotion(NUStmt * candidate, 
                            const SourceLocator & loc,
                            const char * nucleus_type_str, 
                            const char * location, 
                            bool dumped_header)
{
  if (not dumped_header) {
    dumpMotionHeader(candidate);
  }

  UtIO::cout() << "    into ";
  if (location) {
    UtIO::cout() << location << " of ";
  }
  UtIO::cout() << nucleus_type_str << ": ";
  loc.print();
  UtIO::cout() << UtIO::endl;
}


void CodeMotionStatistics::print() const
{
  UtIO::cout() << "CodeMotion Summary: "
               << "Moved " << mStatements << " statements and "
               << "replaced " << mExpressions << " expressions "
               << "in " << mModules << " modules." 
               << UtIO::endl;
}
