// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/RemoveElabFlow.h"
#include "reduce/CleanElabFlow.h"
#include "flow/FLFactory.h"

/*!
  \file
  Implementation of flow removal package.
 */


void RemoveElabFlow::remove()
{
  ElabMarkSweep::clearMarkAllFlow(mFlowFactory);

  CleanElabFlow clean(mFlowFactory, mSymbolTable, false);
  clean.deleteDeadFlow();

  FLNodeElabFactory::FlowLoop loop = mFlowFactory->loopFlows();
  assert(loop.atEnd());
}
