// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/SanityElab.h"

#include "flow/FLFactory.h"
#include "flow/FLIter.h"
#include "flow/FLElabUseCache.h"

#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUCycle.h"
#include "symtab/STSymbolTable.h"
//#include "symtab/STBranchNode.h"

#include "SanityStatus.h"

#include "util/CbuildMsgContext.h"

//! Maintain status for elaborated flow sanity checks.
class SanityElabStatus : public SanityStatus<FLNodeElab>
{
public:
  //! Destructor
  SanityElabStatus(FLNodeElab * node) :
    SanityStatus<FLNodeElab>(node),
    mSafeFLNodeElab(false),
    mSafeFLNode(false)
  {}

  //! Constructor
  ~SanityElabStatus() {};

  //! Mark that the FLNodeElab is safe for dereferencing.
  void setSafeFLNodeElab() { mSafeFLNodeElab = true; }

  //! Mark that the FLNode is safe for dereferencing.
  void setSafeFLNode() { mSafeFLNode = true; }

  //! Print header information for the current node.
  virtual void printHeader() const;

private:
  //! Is the FLNodeElab safe for dereferencing?
  bool mSafeFLNodeElab;

  //! Is the FLNode referenced by the current FLNodeElab safe for dereferencing?
  bool mSafeFLNode;
};


void SanityElabStatus::printHeader() const
{
  UtIO::cout() << "Elaborated Sanity Check Report:" << UtIO::endl;

  // Print as much information about this flnode as is safe.

  FLNodeElab* elab_flow = getNode();
  UtIO::cout() << "    ElabFlow: " << elab_flow << UtIO::endl;
  if (mSafeFLNodeElab) {
    // It is safe to dereference the elaborated ptr.
    UtString message;
    NUNetElab * elab_net = elab_flow->getDefNet();
    elab_net ->getSymNode()->compose(&message);
    UtIO::cout() << "    ElabNet (" << elab_net << "): " << message << UtIO::endl;

    if (elab_flow->isEncapsulatedCycle())
    {
      NUCycle* cycle = elab_flow->getCycle();
      UtIO::cout() << "    NUCycle (" << cycle << "):" << UtIO::endl;
      cycle->print(false, 8);
    }
    else
    {
      STBranchNode * hier = elab_flow->getHier();
      message.clear();    
      hier->compose(&message);
      UtIO::cout() << "    Hier (" << hier << "): " << message << UtIO::endl;
    
      FLNode * unelab_flow = elab_flow->getFLNode();
      UtIO::cout() << "    UnelabFlow: " << unelab_flow << UtIO::endl;
      if (mSafeFLNode) {
        // It is safe to dereference the unelaborated ptr.
        NUNetRefHdl net_ref = unelab_flow->getDefNetRef();
        NUNet * unelab_net = net_ref->getNet();
        message.clear();
        net_ref->compose(&message);
        UtIO::cout() << "    UnelabNet (" << unelab_net << "): " << message << UtIO::endl;
        NUUseDefNode * use_def = unelab_flow->getUseDefNode();
        UtIO::cout() << "    UseDef:";
        if (use_def) {
          UtIO::cout() << UtIO::endl;
          use_def->print(false, 8);
        } else {
          UtIO::cout() << " (nil)" << UtIO::endl;
        }
      }
    }
  }
}


//! Maintain status for unelaborated net sanity checks.
class SanityElabNetStatus : public SanityStatus<NUNetElab>
{
public:
  //! Destructor
  SanityElabNetStatus(NUNetElab * node) :
    SanityStatus<NUNetElab>(node)
  {}

  //! Constructor
  ~SanityElabNetStatus() {};

protected:
  //! Print header information for the Net.
  virtual void printHeader() const;

private:
};


void SanityElabNetStatus::printHeader() const
{
  UtIO::cout() << "Elaborated Net Sanity Check Report:" << UtIO::endl;

  // Print as much information about this net as is safe.

  NUNetElab * net = getNode();

  UtIO::cout() << "    NetElab (" << net << "): ";
  net->getSymNode()->print();
}


SanityElab::SanityElab(MsgContext        * msg_context,
                       STSymbolTable     * symbol_table,
                       NUNetRefFactory   * net_ref_factory,
                       FLNodeFactory     * dfg_factory,
                       FLNodeElabFactory * dfg_elab_factory,
                       bool checkFaninOverlap,
                       bool verbose) :
  mMsgContext(msg_context),
  mSymbolTable(symbol_table),
  mNetRefFactory(net_ref_factory),
  mFlowFactory(dfg_factory),
  mFlowElabFactory(dfg_elab_factory),
  mNumErrors(0),
  mCheckOverlap(checkFaninOverlap),
  mVerbose(verbose)
{
  mFlowFactory->getFreeSet(&mFreeSet);
  mFlowElabFactory->getFreeSet(&mFreeElabSet);
  mLevelCache = new FLElabUseCache(mNetRefFactory, false);
  mEdgeCache = new FLElabUseCache(mNetRefFactory, true);
}

SanityElab::~SanityElab()
{
  delete mLevelCache;
  delete mEdgeCache;
}


void SanityElab::design(NUDesign * /*design*/)
{
  factory();
  
  nets();
  
  if (mNumErrors > 0) {
    mMsgContext->SanityElab(mNumErrors);
  }
}


void SanityElab::factory()
{
  FLNodeElab * flnode_elab = NULL;
  for ( FLNodeElabFactory::FlowLoop iter = mFlowElabFactory->loopFlows();
	iter(&flnode_elab);) {
    SanityElabStatus status(flnode_elab);
    bool success = flow(&status,flnode_elab);
    if (not success) {
      mNumErrors++;
    }
    status.print(mVerbose);
  }
}


void SanityElab::nets()
{
  for (STSymbolTable::NodeLoop i = mSymbolTable->getNodeLoop();
       not i.atEnd(); ++i) {
    STAliasedLeafNode* alias = (*i)->castLeaf();
    if (alias) {
      if (alias->getMaster() == alias) {
        NUNetElab* net_elab = NUNetElab::find(alias);
        if (net_elab) {
          SanityElabNetStatus status(net_elab);
          bool success = net(&status, net_elab);
          if (not success) {
            mNumErrors++;
          }
          status.print(mVerbose);
        }
      }
    }
  }
}


//! Add each pointer address as detail to a status object.
template <class NodeType>
static void sAddPointerDetail(SanityStatus<NodeType> * status, FLNodeElabSet * nodes)
{
  UtString detail;
  detail << "Pointers: ";
  for (FLNodeElabSet::iterator iter = nodes->begin(); 
       iter != nodes->end(); ++iter) {
    FLNodeElab * flow = (*iter);
    detail << flow << " ";
  }
  status->addDetail(detail);
}


//! Check that the set of drivers is of size 0 or 1.
static bool sSingleDriverAllowed(const char * description, SanityElabNetStatus * status, FLNodeElabSet * drivers)
{
  UInt32 count = drivers->size();
  bool drivers_ok =(count==0 or count==1);
  status->addTest(description, drivers_ok);
  if (not drivers_ok) {
    sAddPointerDetail(status, drivers);
  }
  return drivers_ok;
}


bool SanityElab::net(SanityElabNetStatus * status, NUNetElab * net_elab)
{
  bool driver_alloc_ok = true;
  bool driver_def_ok = true;

  FLNodeElabSet bad_alloc_set;
  FLNodeElabSet bad_def_set;
  FLNodeElabSet undriven_bounds;
  FLNodeElabSet hierref_bounds;
  for (NUNetElab::DriverLoop loop = net_elab->loopContinuousDrivers(); 
       not loop.atEnd(); ++loop) {
    FLNodeElab *driver = *loop;
    // Is this driver allocated by the factory?
    bool this_ok = allocFlowCheck(driver);
    if (not this_ok) {
      bad_alloc_set.insert(driver);
    }
    driver_alloc_ok &= this_ok;

    if (this_ok) {
      // Does the def match the current net?
      this_ok = (driver->getDefNet() == net_elab);
      if (not this_ok) {
        bad_def_set.insert(driver);
      }
      driver_def_ok &= this_ok;
    }

    if (this_ok) {
      // Check that there is at most one hierref and one undriven bound.
      switch(driver->getType()) {
      case eFLBoundHierRef: hierref_bounds.insert(driver); break;
      case eFLBoundUndriven: undriven_bounds.insert(driver); break;
      default: /* Other types not in need of checking */ break;
      }
    }
  }

  status->addTest("Driver Allocation", driver_alloc_ok);
  if (not driver_alloc_ok) {
    sAddPointerDetail(status, &bad_alloc_set);
  }

  status->addTest("Driver Def Check", driver_def_ok);
  if (not driver_def_ok) {
    for (FLNodeElabSet::iterator iter = bad_def_set.begin(); 
         iter != bad_def_set.end(); ++iter) {
      FLNodeElab * bad_def_flow = (*iter);
      UtString detail;
      // cannot be sure that the unelaborated flnode is valid; only print ptrs.
      NUNetElab * def_net = bad_def_flow->getDefNet();
      FLNode * flnode = bad_def_flow->getFLNode();
      detail << "Def Mismatch: FlowElab(" << bad_def_flow << "), "
             << "NetElab(" << def_net << "), "
             << "Flow(";
      if (flnode) {
        detail << flnode;
      } else {
        detail << "nil";
      }
      detail << ")";
      status->addDetail(detail);
    }
  }

  bool bounds_ok = true;
  bounds_ok &= sSingleDriverAllowed("Hierref Bounds", status, &hierref_bounds);
  bounds_ok &= sSingleDriverAllowed("Undriven Bounds", status, &undriven_bounds);

  bool ok = driver_alloc_ok and driver_def_ok and bounds_ok;
  return ok;
}


bool SanityElab::allocFlowCheck(FLNode *flnode)
{
  if (mFreeSet.find(flnode) != mFreeSet.end()) {
    return false;
  }
  return true;
}


bool SanityElab::allocFlowCheck(FLNodeElab *flnode)
{
  if (mFreeElabSet.find(flnode) != mFreeElabSet.end()) {
    return false;
  }
  return true;
}

//! check that defnet corresponds with unelab flow def net in elab hierarchy
bool SanityElab::defNetHierarchyCheck(SanityElabStatus * status, FLNodeElab* flnode)
{
  if (flnode->isEncapsulatedCycle()) {
    return true; // FLNodeElabCycle has no unelaborated flow partner
  }

  NUUseDefNode* useDef = flnode->getUseDefNode();
  if (useDef && (useDef->isPortConnInput() || useDef->isPortConnBid())) {
    return true; // Input ports are exceptions, don't check them
  }

  NUNetElab* netElab = flnode->getDefNet();
  STBranchNode* hier = flnode->getHier();

  FLNode* unelab_flow = flnode->getFLNode();
  NUNet* net = unelab_flow->getDefNet();
  
  NUNetElab* otherElab = net->lookupElab(hier,true);

  bool success = (netElab == otherElab);
  status->addTest("Net/Hierarchy Consistency", success);
  if (not success) {
    UtString details;
    details = "Elaborated Net (flow): ";
    netElab->getSymNode()->compose(&details);
    status->addDetail(details);

    details = "Flow Hierarchy: ";
    hier->compose(&details);
    status->addDetail(details);

    details = "Unelaborated Net: ";
    net->composeUnelaboratedName(&details);
    status->addDetail(details);

    details = "Elaborated Net (lookup): ";
    otherElab->getSymNode()->compose(&details);
    status->addDetail(details);

    return false;
  }
  return true;
}

//! check that fanin flow's defs overlap this flow's uses
bool SanityElab::faninOverlapCheck(FLNodeElab* flnode, FLNodeElab* fanin)
{
  return ( flnode->checkFaninOverlap(fanin, mNetRefFactory, mLevelCache) or
           flnode->checkEdgeFaninOverlap(fanin, mNetRefFactory, mEdgeCache) );
}

bool SanityElab::flow(SanityElabStatus * status, FLNodeElab * flnode_elab)
{
  bool ok = allocFlowCheck(flnode_elab);
  status->addTest("Elab Allocation", ok);
  if (not ok) {
    return false;
  }
  status->setSafeFLNodeElab();

  ok = allocFlowCheck(flnode_elab->getFLNode());
  status->addTest("Unelab Allocation", ok);
  if (not ok) {
    return false;
  }
  status->setSafeFLNode();

  ok = defNetHierarchyCheck(status,flnode_elab);
  if (not ok) {
    // status updated within test.
    return false;
  }
  
  if (dynamic_cast<FLNodeBoundElab*>(flnode_elab)) {
    return true;
  }

  NUUseDefNode* useDef = flnode_elab->getUseDefNode();
  bool isContinuousDriver = useDef && useDef->isContDriver();

  // Only visit immediate fanin.
  bool fanin_alloc_ok = true;
  bool fanin_unelab_alloc_ok = true;
  bool fanin_overlap_ok = true;

  FLNodeElabSet bad_fanin;
  FLNodeElabSet bad_fanin_unelab;
  FLNodeElabSet unnecessary_fanins;
  FLNodeElabIter *flow_iter = flnode_elab->makeFaninIter(FLIterFlags::eAll,
                                                         FLIterFlags::eAll,
                                                         FLIterFlags::eBranchAtAll);
  for (; not flow_iter->atEnd(); flow_iter->next()) {
    FLNodeElab* fanin = **flow_iter;
    bool this_ok = allocFlowCheck(fanin);
    if (not this_ok) {
      bad_fanin.insertWithCheck(fanin);
    }
    fanin_alloc_ok &= this_ok;
    if (this_ok) {
      FLNode * unelab_fanin = fanin->getFLNode();
      if (unelab_fanin) {
        this_ok = allocFlowCheck(fanin->getFLNode());
        if (not this_ok) {
          bad_fanin_unelab.insertWithCheck(fanin);
        }
        fanin_unelab_alloc_ok &= this_ok;
      }
    }

    // the overlaps testing only applies to fanins of continuous drivers
    if (this_ok && mCheckOverlap && isContinuousDriver && !flow_iter->isNestedRelationship())
    {
      bool overlaps_ok = faninOverlapCheck(flnode_elab, fanin);
      if (not overlaps_ok) {
        unnecessary_fanins.insertWithCheck(fanin);
      }
      fanin_overlap_ok &= overlaps_ok;
    }
  }
  ok &= fanin_alloc_ok;
  ok &= fanin_overlap_ok;

  status->addTest("Fanin Allocation", fanin_alloc_ok);
  if (not fanin_alloc_ok) {
    UtString detail;
    detail << "Pointers (Flow Elab): ";
    for (FLNodeElabSetIter iter = bad_fanin.begin(); iter != bad_fanin.end(); ++iter) {
      FLNodeElab * bad_fanin = (*iter);
      detail << bad_fanin << " ";
    }
    status->addDetail(detail);
  }
  status->addTest("Fanin Unelab Allocation", fanin_unelab_alloc_ok);
  if (not fanin_unelab_alloc_ok) {
    UtString detail;
    detail << "Pointers (Flow Elab[FlowUnElab]): ";
    for (FLNodeElabSetIter iter = bad_fanin_unelab.begin(); iter != bad_fanin_unelab.end(); ++iter) {
      FLNodeElab * bad_fanin = (*iter);
      FLNode * bad_unelab_fanin = bad_fanin->getFLNode();
      detail << bad_fanin << "[" << bad_unelab_fanin << "] ";
    }
    status->addDetail(detail);
  }

  status->addTest("Fanin Overlap", fanin_overlap_ok);
  if (not fanin_overlap_ok) {
    for (FLNodeElabSetIter iter = unnecessary_fanins.begin(); iter != unnecessary_fanins.end(); ++iter) {
      FLNodeElab * unnecessary_fanin = (*iter);
      UtString detail;
      detail << "Unnecessary fanin (" << unnecessary_fanin << "): ";
      unnecessary_fanin->getDefNet()->getSymNode()->compose(&detail);
      status->addDetail(detail);
      
      FLNode * fanin_unelab = unnecessary_fanin->getFLNode();
      NUNetRefHdl def_ref = fanin_unelab->getDefNetRef();

      detail = "    ";
      def_ref->compose(&detail);
      status->addDetail(detail);
    }
  }

  delete flow_iter;

  return ok;
}


