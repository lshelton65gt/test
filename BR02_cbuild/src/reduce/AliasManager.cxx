// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "AliasManager.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetSet.h"

#include "flow/FLNode.h"
#include "flow/FLIter.h"

#include "localflow/UD.h"
#include "localflow/UnelabFlow.h"

#include "reduce/REUtil.h"
#include "reduce/RemoveDeadCode.h"
#include "reduce/RemoveUnelabFlow.h"

#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/UtArray.h"

#include "util/Stats.h"
#include "util/GenericDigraph.h"
#include "util/GraphBuilder.h"
#include "util/GraphDot.h"
#include "util/GraphSCC.h"

/*!\file
 * Definition of the mechanical portion of aliasing.
 * The AliasManager keeps track of aliasing candidates and partitions
 * them into groups.  When the alias groups are finalized, it modifies
 * the Nucleus and flow to perform the aliasing.
 */



/* A PRIMER ON THE MECHANICS OF ALIASING
 *
 * When we determine that two nets are to be aliased, the first order of
 * business is to choose one to be the master (it's name is preserved) and
 * one to be the subordinate.  When multiple nets are to be aliased together,
 * we make this determination several times and eventually we have one master
 * net with the "best" name and several subordinate nets which will be
 * disconnected.
 *
 * To actually perform the aliasing, we need to transform the Nucleus
 * data structure and flow graph to reflect the aliasing.  The Nucleus
 * transformation is simple -- just replace all references to subordinate nets
 * on both the LHS and RHS with references to the master net.  Then the flow
 * can be transformed to reflect the aliasing and dead-code elimination can
 * clean-up any unreferenced Nucleus structures.  Finally, alias rings for
 * all of the aliased nets must be merged into the master's alias ring.
 *
 * The flow transformation is more involved.  We want to choose one set of
 * flow to represent the driving logic for all of the aliases, and this chosen
 * flow does not have to be the master's driving flow -- it can be flow from
 * any of the aliased nets.  Call the net driven by the chosen flow the "donor"
 * net.  The flow transformation proceeds in a number of steps:
 *
 *  1. All aliased nets are disconnected from their driving flow
 *  2. The chosen flow (and its nested flow) has its def net replaced by the 
 *     master net
 *  3. The chosen flow is added as a driver of the master net
 *  4. The fanin pointers for all flow that referenced non-chosen flow are
 *     updated to reference the non-chosen flow's fanin instead
 *
 * Step 4 requires some explanation.  We have transformed the Nucleus to
 * replace the subordinate nets with the master, but we still have to remove
 * the Nucleus nodes for the non-chosen flows.  We do that by re-writing
 * the flow graph to make them dead and then applying dead-code elimination
 * to delete the Nucleus.
 *
 * This means that we need to replace all references to the non-chosen
 * flow with references to the chosen flow, but in general the nesting
 * hierarchies will be different.  This makes it very difficult to
 * determine the ultimate corresponding flow node to replace each flow
 * node in the non-chosen hierarchies.
 *
 * Instead, we replace references to the non-chosen flows with
 * references to their fanins.  This is not the ultimately correct flow,
 * but it makes the non-chosen flow dead and preserves liveness for other
 * flows long enough for us to do dead-code elimination and then
 * recompute accurate flow based on the modified Nucleus.
 *
 * There is an obvious optimization for the case when the master net and the
 * chosen net are the same: step 2 can be eliminated entirely.
 *
 * So why not always choose the master net?  A problem arises when
 * aliasing one net which is a fanin of the other.  We want to keep
 * the chosen flow alive and allow the other flow to become dead, but
 * that does not work when the non-chosen flow is in the fanin of the
 * chosen flow.
 *
 * Consider the case when a master net M is driven from an equivalent
 * subordinate S:
 *
 *   S = ...;
 *   M = S;
 *
 * The Nucleus transformation makes this:
 *
 *   M = ...;
 *   M = M;
 *
 * And we want the first statement to stay live and allow the second statement
 * to become dead.  That means we must choose to keep the subordinate's flow.
 * See bug3656.
 *
 * The worst case is when the flow is cyclic and there is no net among the alias
 * candidates which does not have fanin from another alias candidate.  There is
 * no way to choose a donor flow that will allow all other candidates' flow to
 * become dead.  To deal with this case, we build a net->net graph representing
 * a module's flow between aliasable nets and then compute its strongly-connected
 * component graph.  By assigning component ID numbers in depth-first finishing
 * time, each net gets a "causality index" associated with it, and given two nets we
 * can tell whether they are in the same component, and if not, which one precedes
 * the other.  This lets us test the nets as they are found and maintain alias sets
 * that always have a unique causally-first flow donor net.
 */

/*!
 * Represents a group of nets that will be aliased together.
 * Within the group, one net is distinguished as a master and
 * one (possibly the same one) is distinguished as a flow donor.
 * All non-master nets are subordinates.
 *
 * AliasGroup has two modes, determined by bool mContAssignsOnly.
 *   - When that is true, mDonor will be NULL, and mDonorFlows and
 *     mNonDonorFlows will be populated
 *   - When that is false, mDonor will be non-NULL and mDonorFlows and
 *     mNonDonorFlows will be empty
 * This enables aliasing across continuous assigns to multiply driven
 * nets, by allowing a data model where there are multiple donor flows.
 *
 */
class AliasGroup
{
public:
  AliasGroup(AliasManager* manager, bool contDriversOnly)
    : mMaster(NULL), mDonor(NULL), mManager(manager),
      mContAssignsOnly(contDriversOnly)
  {}  
  bool add(NUNet* net, FLNode* aliasFlow);
  bool merge(AliasGroup* other);
  NUNet* getMaster() const { return mMaster; }
  NUNet* getDonor()  const { return mDonor; }
  NUNetSet::UnsortedLoop  loopSubordinates() { return mSubordinates.loopUnsorted(); }
  NUNetSet::UnsortedCLoop loopSubordinates() const { return mSubordinates.loopCUnsorted(); }
  bool isSubordinate(NUNet* net) { return (mSubordinates.find(net) != mSubordinates.end()); }

  FLNodeSet::UnsortedLoop loopDonorFlows() { return mDonorFlows.loopUnsorted(); }

  // Repair the flow-nodes for this groupa
  void repairFlows(NUNetRefFactory* netRefFactory);

private:
  /* pickMaster and pickDonor are functions which are used to choose
   * the distinguished master and flow donor nets from among all nets
   * in the alias group.  They are conceptually implementations of min
   * defined on the set of NUNets (+ NULL, which is greater than all
   * NUNets).
   */
  /*! In the case of pickMaster, the relation on the set of NUNets is
   *  a total order where a < b (ie. a == pickMaster(a,b)) when a is a
   *  better master choice than b.  This is a total order, so the
   *  relation is transitive and defined for all (a,b).
   */
  NUNet* pickMaster(NUNet* candidate, NUNet* current) const;

  /*! pickDonor is a strict partial order where a < b when a precedes
   *  b in a causal order.  When a and b are in the same
   *  strongly-connected componenent of the causal ordering graph,
   *  pickDonor(a,b) should return NULL.  When a and b are unrelated
   *  in the causal ordering graph (neither precedes the other and
   *  they are not in the same strongly-connected component), the
   *  ordering is defined in terms of some additional total order
   *  (in this case it's the DFS finishing time of a traversal of the
   *  causality graph).  pickDonor is a partial order, so the relation
   *  is transitive, but it is not defined for (a,b) in a causal
   *  cycle.
   */
  NUNet* pickDonor(NUNet* candidate, NUNet* current) const;

private:
  NUNet*        mMaster;       //!< The net to preserve for naming
  NUNet*        mDonor;        //!< The net whose driving flow will be used
  FLNodeSet     mDonorFlows;   //!< Flow-nodes that contribute something
  FLNodeSet     mNonDonorFlows;//!< Flow-nodes that come from aliasing assigns
  NUNetSet      mSubordinates; //!< Nets to become aliases
  AliasManager* mManager;      //!< The alias manager for this alias group
  bool          mContAssignsOnly; //!< to handle aliasing multiply driven nets, keep mDonorFlows
};

//! Add a net to this alias group (if allowed), updating donor and master
bool AliasGroup::add(NUNet* net, FLNode* aliasFlow)
{
  // choose a new flow donor for the merged group
  if (mContAssignsOnly) {
    NU_ASSERT(aliasFlow, net);
    mNonDonorFlows.insert(aliasFlow);
    mDonorFlows.insertLoop(net->loopContinuousDrivers());
    mDonorFlows.erase(aliasFlow);
  }
  else {
    NUNet* newDonor = pickDonor(net, mDonor);
    if (!newDonor)
      return false;  // donor relation undefined => aliasing failed
    mDonor = newDonor;
  }
  
  // choose master/subordinate status for this net
  NUNet* newMaster = pickMaster(net, mMaster);
  NUNet* newSubord = (net == newMaster) ? mMaster : net;
  mMaster = newMaster;
  if (newSubord && (newSubord != mMaster)) // <-- in case net == master == newMaster
    mSubordinates.insert(newSubord);

  return true;
}

//! Merge another alias group into this one, updating donor and master if succesful
bool AliasGroup::merge(AliasGroup* other)
{
  // it is assumed that the groups are disjoint
  // and both have non-NULL mMaster and mDonor
  INFO_ASSERT(mMaster != NULL, "Aliasing problem: master not found");
  INFO_ASSERT(other->mMaster != NULL, "Aliasing problem: other group master not found.");
  NU_ASSERT(other->mContAssignsOnly == mContAssignsOnly, mMaster);

  // choose a new flow donor for the merged group
  if (mContAssignsOnly) {
    mDonorFlows.insertSet(other->mDonorFlows);
    mNonDonorFlows.insertSet(other->mNonDonorFlows);
    mDonorFlows.eraseSet(mNonDonorFlows);
    NU_ASSERT(mDonor == NULL, mMaster);
    NU_ASSERT(other->mDonor == NULL, mMaster);
  }
  else {
    INFO_ASSERT(mDonor != NULL, "Aliasing problem: donor not found");
    INFO_ASSERT(other->mDonor != NULL, "Aliasing problem: other group donor not found.");
    NUNet* newDonor = pickDonor(mDonor, other->mDonor);
    if (!newDonor)
      return false;  // donor relation undefined => aliasing failed
    mDonor = newDonor;
    NU_ASSERT(mDonorFlows.empty(), mMaster);
    NU_ASSERT(mNonDonorFlows.empty(), mMaster);
    NU_ASSERT(other->mDonorFlows.empty(), mMaster);
    NU_ASSERT(other->mNonDonorFlows.empty(), mMaster);
  }

  // chose a master and update the subordinates
  NUNet* newMaster = pickMaster(mMaster, other->mMaster);
  NUNet* newSubord = (mMaster == newMaster) ? other->mMaster : mMaster;
  mMaster = newMaster;
  mSubordinates.insert(newSubord);
  mSubordinates.insert(other->mSubordinates.begin(), other->mSubordinates.end());

  return true;
}

/*! The less-than relation for masters is based on the "goodness" of the net
 *  names: ports are prefered over block-locals which are prefered over temps.
 *  In the case of a tie, NUNet::compare() is used to define a total order.
 */
NUNet* AliasGroup::pickMaster(NUNet* a, NUNet* b) const
{
  if (a == NULL)
    return b;
  if (b == NULL)
    return a;

  // Make the port win, if one is a port
  bool port1 = a->isPort();
  bool port2 = b->isPort();
  if (port1 and (not port2)) {
    return a;
  } else if ((not port1) and port2) {
    return b;
  }

  // Do not choose a temp name over a user name
  bool temp1 = a->isTemp();
  bool temp2 = b->isTemp();
  if (temp1 and (not temp2)) {
    return b;
  } else if ((not temp1) and temp2) {
    return a;
  }

  // Do not choose a block-local over a non-block-local
  bool blocklocal1 = a->isBlockLocal();
  bool blocklocal2 = b->isBlockLocal();
  if (blocklocal1 and (not blocklocal2)) {
    return b;
  } else if ((not blocklocal1) and (blocklocal2)) {
    return a;
  }

  // If you have a NUMemoryNet and a NUTempMemoryNet, choose the
  // NUMemoryNet.  This is to prevent assignment aliasing from choosing
  // a NUTempMemoryNet as the master in the alias ring.  If the temp is
  // chosen, the master (a regular NUMemoryNet) will be deleted, leaving
  // the NUTempMemoryNet's mMaster pointer pointing off at freed memory.
  if ( a->is2DAnything( ))
  {
    NU_ASSERT( b->is2DAnything(), b ); // they better both be memories
    if ( a->isMemoryNet() && !b->isMemoryNet() ) {
      return a;
    }
    else if ( !a->isMemoryNet() && b->isMemoryNet() ) {
      return b;
    }
  }


  // Use net comparison as a way of ordering the aliases.
  return (NUNet::compare(a,b) <= 0) ? a : b;
}

/*! The less-than relation for donors is based on the causality-index
 *  of the nets, but is undefined when two nets have the same causality index.
 */
NUNet* AliasGroup::pickDonor(NUNet* a, NUNet* b) const
{
  if (a == NULL)
    return b;
  if (b == NULL)
    return a;

  UInt32 id1 = mManager->getCausalityIndex(a);
  UInt32 id2 = mManager->getCausalityIndex(b);
  if (id1 < id2)
    return a;
  else if (id2 < id1)
    return b;
  else
    return NULL;
}

static void sUpdateFlowNetRefHelper(FLNode* flow, NUNet* master,
                                    NUNetRefFactory* netRefFactory)
{
  NUNetRefHdl oldRef = flow->getDefNetRef();
  NUNetRefHdl newRef = netRefFactory->createNetRefImage(master, oldRef);
  flow->replaceDefNetRef(oldRef, newRef);
                                    
  if (flow->hasNestedBlock()) {
    for (Iter<FLNode*> loop = flow->loopNested(); !loop.atEnd(); ++loop) {
      FLNode* nested = *loop;
      sUpdateFlowNetRefHelper(nested, master, netRefFactory);
    }
  }
}

void AliasGroup::repairFlows(NUNetRefFactory* netRefFactory) {
  // Remove the continuous drivers from the subordinates and the master
  for (NUNetSet::iterator p = mSubordinates.begin(), e = mSubordinates.end();
       p != e; ++p)
  {
    NUNet* subord = *p;
    subord->removeContinuousDrivers();
  }
  mMaster->removeContinuousDrivers();

  // Put all the donor flows on the master, and make the donor flows
  // point to the master
  for (FLNodeSet::iterator p = mDonorFlows.begin(), e = mDonorFlows.end();
       p != e; ++p)
  {
    FLNode* flow = *p;
    mMaster->addContinuousDriver(flow);

    sUpdateFlowNetRefHelper(flow, mMaster, netRefFactory);
  }
} // void AliasGroup::repairFlows

//! The type of graph which represetns a causal order between nets
typedef GenericDigraph<void*,NUNet*> CausalityGraph;

//! A helper class for populating a net->causality-index map by walking the component graph
class CausalityIndexMapPopulator : public GraphWalker
{
public:
  CausalityIndexMapPopulator(CausalityGraph* graph, GraphSCC& scc,
                             AliasManager::CausalityIndexMap* causalityMap)
    : mGraph(graph), mSCC(scc), mMap(causalityMap), mID(1)
  {}
  
  virtual Command visitNodeAfter(Graph* /* graph -- unused */, GraphNode* node)
  {
    // get the component
    GraphSCC::Component* comp = mSCC.getComponent(node);
    
    // assign a causality index to every node in the component
    for (GraphSCC::Component::NodeLoop loop = comp->loopNodes();
         !loop.atEnd();
         ++loop)
    {
      CausalityGraph::Node* node = mGraph->castNode(*loop);
      NUNet* net = node->getData();
      mMap->insert(AliasManager::CausalityIndexMap::value_type(net, mID));
    }

    // advance the causality index
    ++mID;

    return GW_CONTINUE;
  }

private:
  CausalityGraph*                  mGraph; //!< the underlying causality graph
  GraphSCC&                        mSCC;   //!< the SCC object that owns the component graph
  AliasManager::CausalityIndexMap* mMap;   //!< the map from net->causality-index to populate
  SInt32                           mID;    //!< the next available causality index
};

//! Prepare the manager for aliasing in the specified module
void AliasManager::prepare(NUModule* module)
{
  clear();
  mModule = module;

  // when working only with continuous assigns, we don't need the causality
  // index map to help us pick donors, since we can do that incrementally
  // by keeping track of which flow-nodes represented explicit assigns that
  // we are aliasing away, and those can be discarded
  if (! mContAssignsOnly) {
    buildCausalityIndexMap();
  }
}

//! Lookup an alias group for a given net, or return NULL is the net is not yet aliased
AliasGroup* AliasManager::lookupAliasGroup(NUNet* net)
{
  NetAliasMap::iterator pos = mNetToGroupMap.find(net);
  if (pos == mNetToGroupMap.end())
    return NULL;
  else
    return pos->second;
}

//! Build the map from net->causality-index for this module
void AliasManager::buildCausalityIndexMap()
{
  INFO_ASSERT(mModule, "buildCausalityIndexMap called without setting mModule");

  // First, build a causality graph by traversing the module's flow
  GraphBuilder<CausalityGraph> builder;
  
  typedef UtList<NUNet*> NUNetList;
  NUNetList start_list;
  mModule->getAllNets(&start_list);

#define DEBUG_SPEW 0
#if DEBUG_SPEW
  UtIO::cerr() << "buildCausalityIndexMap(" << mModule->getName()->str() << ")\n";
#endif

  // This doesn't work yet.  It produces occasional wrong answers and
  // spew diffs.  It needs to be examined, but I'm leaving this in here
  // ifdef'd out so as not to lose track of it.
  //
  // test/langcov/Stop fails when FAST_CAUSALITY_GRAPH is defined to 1, 
  // although it's just a spew diff.
#define FAST_CAUSALITY_GRAPH 0
#if FAST_CAUSALITY_GRAPH
  FLNetIter iter(&start_list,
                 FLIterFlags::eAll,
                 FLIterFlags::eNone,
                 FLIterFlags::eBranchAtNestedBlock,
                 FLIterFlags::eIterFaninPairOnce);
  for (; !iter.atEnd(); iter.next()) {
    FLNode* curFlow = iter.getCur();
    NUNet* defNet = curFlow->getDefNet();
    if (iter.isNestedRelationship())
      continue; // don't count these as edges
    NUUseDefNode* useDef = curFlow->getUseDefNode();

    // add an edge for this flow
    FLNode* fanout_node = iter.getCurParent();
    if (fanout_node != NULL) {

      NUNet* fanout_net = fanout_node->getDefNet();

      // stop at continuous drivers
      if (!useDef || useDef->isContDriver())
      {
        iter.doNotExpand();
        if (fanout_net->isFlop())
          continue; // do not add an edge
      }

      if (fanout_net != defNet) {
#if DEBUG_SPEW
        UtIO::cerr() << "  adding edge " 
                     << fanout_net->getName()->str()
                     << " -> "
                     << defNet->getName()->str() << "\n";
#endif
        builder.addEdgeIfUnique(fanout_net, defNet, NULL);
      }
    }
    else {
      builder.addNode(defNet);
    }
  } // for
#else

  for (NUNetList::iterator n = start_list.begin();
       n != start_list.end();
       ++n) {
    NUNet* net = (*n);
    builder.addNode(net);

    for (NUNet::DriverLoop loop = net->loopContinuousDrivers();
         !loop.atEnd();
         ++loop)
    {
      FLNode* flow = *loop;
      NUUseDefNode* useDef = flow->getUseDefNode();
      if (!useDef || (useDef->getType() == eNUModuleInstance))
        continue;
      bool inFlop = useDef->isSequential();

      // When starting from a flop, stop the traversal at continuous drivers
      // When starting outside of a flop, stop at sequential nodes
      FLNodeIter* iter = flow->makeFaninIter(FLIterFlags::eAll,
                                             FLIterFlags::eNone,
                                             FLIterFlags::eBranchAtNestedBlock);
      for (; !iter->atEnd(); iter->next())
      {
        FLNode* curFlow = iter->getCur();
        NUNet* defNet = curFlow->getDefNet();
        if ((defNet == net) || iter->isNestedRelationship())
          continue; // don't count these as edges
        useDef = curFlow->getUseDefNode();
        // stop at continuous drivers
        if (!useDef || useDef->isContDriver())
        {
          iter->doNotExpand();
          if (inFlop)
            continue; // do not add an edge
        }
        // add an edge for this flow

#if DEBUG_SPEW
        UtIO::cerr() << "  adding edge " 
                     << iter->getCurParent()->getDefNet()->getName()->str()
                     << " -> "
                     << defNet->getName()->str() << "\n";
#endif

        builder.addEdgeIfUnique(iter->getCurParent()->getDefNet(), defNet, NULL);
      }
      delete iter;
    }
  }
#endif

  CausalityGraph* graph = builder.getGraph();

  // Now compute the strongly-connected component graph of the net->net flow graph
  GraphSCC scc;
  Graph* compGraph = scc.buildComponentGraph(graph);

  // Walk the component graph, assigning component IDs in depth-first finishing order
  CausalityIndexMapPopulator populator(graph, scc, &mCausalityIndexMap);
  populator.walk(compGraph);

  // We are done with the graph now
  delete graph;
}

UInt32 AliasManager::getCausalityIndex(NUNet* net) const
{
  CausalityIndexMap::const_iterator pos = mCausalityIndexMap.find(net);
  NU_ASSERT(pos != mCausalityIndexMap.end(), net);
  return pos->second;
}

bool AliasManager::rememberDriver(NUNet *net,
                                  FLNode *driver,
                                  Sensitivity sense)
{
  if (mVerbose) {
    UtIO::cout() << "Driver:" << UtIO::endl;
    net->print(0,2);
    driver->print(0,2);
    UtIO::cout() << UtIO::endl;
  }

  bool seen_before = false;
  NetNodeSetMapIter driver_iter = mDrivers.find(net);
  if (driver_iter == mDrivers.end()) {
    NodeSenseSet *drivers = new NodeSenseSet();
    drivers->insert(NodeSense(driver, sense));
    mDrivers[net] = drivers;
  } else {
    NodeSenseSet *drivers = driver_iter->second;
    if (drivers->find(NodeSense(driver, sense)) != drivers->end()) {
      seen_before = true;
    } else {
      drivers->insert(NodeSense(driver, sense));
    }
  }

  return seen_before;
}

void AliasManager::rememberFanout(NUNet *net,
                                  FLNode *fanout,
                                  Sensitivity sense)
{
  if (not fanout) {
    return;
  }

  if (mVerbose) {
    UtIO::cout() << "Fanout:" << UtIO::endl;
    net->print(0,2);
    fanout->print(0,2);
    UtIO::cout() << UtIO::endl;
  }

  NetNodeSetMapIter fanout_iter = mFanouts.find(net);
  if (fanout_iter == mFanouts.end()) {
    NodeSenseSet *fanouts = new NodeSenseSet;
    fanouts->insert(NodeSense(fanout, sense));
    mFanouts[net] = fanouts;
  } else {
    NodeSenseSet *fanouts = fanout_iter->second;
    fanouts->insert(NodeSense(fanout, sense));
  }
}


bool AliasManager::rememberAlias(NUNet* net1, NUNet* net2, FLNode* aliasFlow)
{
  AliasGroup* ag1 = lookupAliasGroup(net1);
  AliasGroup* ag2 = lookupAliasGroup(net2);

  if (ag1 && !ag2)
  {
    if (!ag1->add(net2, aliasFlow))  // add net2 to net1's group
      return false;
    mNetToGroupMap[net2] = ag1;
  }
  else if (!ag1 && ag2)
  {
    if (!ag2->add(net1, aliasFlow))  // add net1 to net2's group
      return false;
    mNetToGroupMap[net1] = ag2;
  }
  else if (ag1 && ag2)
  {
    if (!ag1->merge(ag2)) // merge the two alias groups
      return false;
    mNetToGroupMap[ag2->getMaster()] = ag1;
    for (NUNetSet::UnsortedLoop loop = ag2->loopSubordinates(); !loop.atEnd(); ++loop)
    {
      NUNet* net = *loop;
      mNetToGroupMap[net] = ag1;
    }
    mAliases.erase(ag2);
    delete ag2;
  }
  else
  {
    // create a new alias group
    ag1 = new AliasGroup(this, mContAssignsOnly);
    if (!ag1->add(net1, aliasFlow) || !ag1->add(net2, aliasFlow))
    {
      delete ag1;
      return false;
    }
    mAliases.insert(ag1);
    mNetToGroupMap[net1] = ag1;
    mNetToGroupMap[net2] = ag1;
  }

  if (mVerbose) {
    UtIO::cout() << "Found alias:" << UtIO::endl;
    net1->print(false,2);
    net2->print(false,2);
    UtIO::cout() << UtIO::endl;
  }

  return true;
}


void AliasManager::sanityCheckAliases()
{
  for (AliasGroupSet::iterator iter = mAliases.begin();
       iter != mAliases.end();
       ++iter) {
    AliasGroup* ag = *iter;    
    sanityCheckAliasGroup(ag);
  }

  for (NetAliasMap::iterator iter = mNetToGroupMap.begin();
       iter != mNetToGroupMap.end();
       ++iter) {
    NUNet *net = iter->first;
    AliasGroup* ag = iter->second;
    if ((net != ag->getMaster()) && !ag->isSubordinate(net))
      NU_ASSERT("Net maps to alias group that does not contain it, sanity check failed" == 0,net);
  }
}

void AliasManager::sanityCheckAliasGroup(AliasGroup* group)
{
  NUNet* master = group->getMaster();
  INFO_ASSERT(master, "Alias group has no master, sanity check failed");

  NUNet* donor = group->getDonor();
  INFO_ASSERT(donor || mContAssignsOnly,
              "Alias group has no flow donor, sanity check failed");
    
  if (group->isSubordinate(master))
    NU_ASSERT("Alias master found as subordinate, sanity check failed" == 0,master);

  if ((donor != NULL) && (donor != master) && !group->isSubordinate(donor))
    NU_ASSERT("Alias flow donor is not master or subordinate, sanity check failed" == 0,donor);

  NetAliasMap::iterator pos = mNetToGroupMap.find(master);
  if (pos == mNetToGroupMap.end())
    NU_ASSERT("Alias master is not in net-to-group map, sanity check failed" == 0,master);
  else if (pos->second != group)
    NU_ASSERT("Alias master points to wrong alias group in net-to-group map, sanity check failed" == 0,master);

  for (NUNetSet::UnsortedLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
  {
    NUNet* subord = *loop;
    pos = mNetToGroupMap.find(subord);
    if (pos == mNetToGroupMap.end())
      NU_ASSERT("Alias subordinate is not in net-to-group map, sanity check failed" == 0,subord);
    else if (pos->second != group)
      NU_ASSERT("Alias subordinate points to wrong alias group in net-to-group map, sanity check failed" == 0,subord);
  }
}

void AliasManager::performReplacement(ReplacementMap* replacement_map)
{
  // loop over all alias sets
  for (AliasGroupSet::iterator iter = mAliases.begin();
       iter != mAliases.end();
       ++iter) {
    AliasGroup* group = *iter;

    /* Update the net flags on the master to reflect all aliased net flags */
    flagAliases(group);

    NUNet* master = group->getMaster();
    NUNet* donor = group->getDonor();

    /* Build a map from non-chosen flow nodes to their fanin sets */
    buildReplacementMap(group, replacement_map);

    /* If the master is not also the donor, then replace the
     * master's flow with the donor's flow, leaving behind any
     * enabled drivers
     */
    if (donor != master)
    {
      master->removeContinuousDrivers();
      for (NUNet::DriverLoop loop = donor->loopContinuousDrivers();
           !loop.atEnd();
           ++loop)
      {
        FLNode* chosen_flow = *loop;
        // replace def net in entire nested flow tree
        FLNodeIter* fliter = chosen_flow->makeFaninIter(FLIterFlags::eAll,
                                                        FLIterFlags::eLeafNode,
                                                        FLIterFlags::eIntoAll,
                                                        true);
        for (; !fliter->atEnd(); fliter->next())
        {
          FLNode* curFlow = **fliter;
          NUNetRefHdl old_ref = curFlow->getDefNetRef();
          NUNetRefHdl new_ref = mNetRefFactory->createNetRefImage(master, old_ref);
          curFlow->replaceDefNetRef(old_ref, new_ref);
        }
        delete fliter;
        master->addContinuousDriver(chosen_flow);
      }
    }

    /* Disconnect all subordinate nets from their driving flow */
    for (NUNetSet::UnsortedLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
    {
      NUNet* net = *loop;
      net->removeContinuousDrivers();
    }
  } // for
} // void AliasManager::performReplacement

void AliasManager::performAliasing(bool dumpAliasTable)
{
  if (mPhaseStats) {
    mStats->pushIntervalTimer();
  }
  INFO_ASSERT(mModule, "performAlising called without setting mModule");

  /* Replace subordinate nets with masters in Nucleus structures */
  NUNetReplacementMapNonConst* substMap = buildNetSubstitutionMap();
  if (substMap->empty())
  {
    delete substMap;
    if (mPhaseStats) {
      mStats->popIntervalTimer();
    }
    return;  // no aliasing to be done, just return now
  }
  SubstituteNets translator(substMap);
  bool did_fixup = mModule->replaceLeaves(translator);
  NU_ASSERT(did_fixup,mModule);

  // Update the masters of any temp memories with its aliased master.
  mModule->updateTempMemoryMasters(translator);
  delete substMap;

  if (mContAssignsOnly) {
    // Clear subord & master flows, copy all donor flows to master, fix def nets
    repairContFlows();

    // Fix the remembered fanouts so they point to the donor flows
    fixupContFanouts();
  }
  else {
    /* Rework the flow graph to make all but one set of driving flow dead */
    ReplacementMap replacement_map;
    performReplacement(&replacement_map);
    if (mPhaseStats) { mStats->printIntervalStatistics("AM:Replacement"); }

    // Make all replacements point to un-replaced flow.
    fixupReplacementMap(&replacement_map);
    if (mPhaseStats) { mStats->printIntervalStatistics("AM:Fixup"); }

    /* Fix up our fanout's fanin pointers that reference non-chosen flow */
    fixupFanouts(replacement_map);
    if (mPhaseStats) { mStats->printIntervalStatistics("AM:Fanout"); }
  }

  if (dumpAliasTable) {
    dumpAliases();
    if (mPhaseStats) { mStats->printIntervalStatistics("AM:Dump"); }
  }

  /* Merge alias rings for subordinates into their masters' rings */
  cleanupAliasedNets();
  if (mPhaseStats) { mStats->printIntervalStatistics("AM:Cleanup"); }

  /* Recompute UD */
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
  ud.module(mModule);
  if (mPhaseStats) { mStats->printIntervalStatistics("AM:UD"); }

  /* Remove unreachable Nucleus structures
   *
   * Note that RemoveDeadCode is called with delete_instances = false.
   * This change was made as part of invoking aliasing early in the compile,
   * during the local analysis phase as part of constant propagation.
   * This was needed because instances were being removed before directives
   * were processed, and directives could make an instance alive.
   * Note that this change affects all invocations of assignment aliasing, which
   * is ok to do, since assignment aliasing won't cause instances to be dead.
   */
  RemoveDeadCode cleanup(mNetRefFactory,
                         mFlowFactory,
                         mIODB,
                         mArgs,
                         mMsgContext,
                         true,    // start_at_all_nets
                         true,    // delete_dead_code
                         false,   // delete_instances
                         false);  // verbose

  cleanup.module(mModule);
  if (mPhaseStats) { mStats->printIntervalStatistics("AM:DeadCode"); }

  // TBD Blow away the local flow graph.  We really shouldn't do this.
  RemoveUnelabFlow remove(mFlowFactory);
  remove.remove();
  if (mPhaseStats) { mStats->printIntervalStatistics("AM:RemoveUnelabFlow"); }

  // TBD Recreate the local flow graph.  We really shouldn't do this.
  UnelabFlow unelab_flow(mFlowFactory, mNetRefFactory, mMsgContext, mIODB, false);
  unelab_flow.module(mModule, false);
  if (mPhaseStats) { mStats->printIntervalStatistics("AM:UnelabFlow"); }

  if (mPhaseStats) {
    mStats->popIntervalTimer();
  }
}


NUNetReplacementMapNonConst* AliasManager::buildNetSubstitutionMap() const
{
  NUNetReplacementMapNonConst* substMap = new NUNetReplacementMapNonConst;

  // map all subordinates to their master
  for (AliasGroupSet::const_iterator iter = mAliases.begin(); iter != mAliases.end(); ++iter)
  {
    AliasGroup* group = *iter;
    NUNet* master = group->getMaster();
    for (NUNetSet::UnsortedLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
    {
      NUNet *subord = *loop;
      (*substMap)[subord] = master;
    }
  }

  return substMap;
}

void AliasManager::flagAliases(AliasGroup* group)
{
  NUNet* master = group->getMaster();
  for (NUNetSet::UnsortedLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
  {
    NUNet *subord = *loop;
    master->assignAliasSetFlags(subord);
  }
}

//! Populate the map with replacements for the non-chosen flow nodes
void AliasManager::buildReplacementMap(AliasGroup* group,
                                       ReplacementMap* replacement_map)
{
  NUNet* master = group->getMaster();
  NUNet* donor = group->getDonor();
  if (master != donor)
  {
    for (NUNet::DriverLoop loop = master->loopContinuousDrivers();
         !loop.atEnd();
         ++loop)
    {
      FLNode* non_chosen_flow = *loop;
      helperRememberReplacement(non_chosen_flow, replacement_map);
    }
  }

  // Iterate over all subordinates, building the replacement map
  for (NUNetSet::UnsortedLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
  {
    NUNet* subord = *loop;
    if (subord == donor)
      continue; // don't populate the map for the donor's flow -- it won't be replaced
    for (NUNet::DriverLoop dloop = subord->loopContinuousDrivers();
         !dloop.atEnd();
         ++dloop)
    {
      FLNode* non_chosen_flow = *dloop;
      helperRememberReplacement(non_chosen_flow, replacement_map);
    }
  }
}

void AliasManager::helperRememberReplacement(FLNode* node,
                                             ReplacementMap* replacement_map)
{
  ReplacementMapIter iter = replacement_map->find(node);
  FLN_ASSERT(iter == replacement_map->end(), node);

  FLNode::Set& replacements = (*replacement_map)[node]; // populate map with empty flow Set
  replacements.clear();

  for (Iter<FLNode*> fanin_loop = node->loopFanin();
       not fanin_loop.atEnd();
       ++fanin_loop) {
    FLNode* fanin = *fanin_loop;
    replacements.insert(fanin);
  }

  // Handle nesting.
  if (node->hasNestedBlock()) {
    for (Iter<FLNode*> nested_loop = node->loopNested();
         not nested_loop.atEnd();
         ++nested_loop) {
      FLNode* nested = *nested_loop;
      helperRememberReplacement(nested, replacement_map);
    }
  }
}


void AliasManager::fixupReplacementMap(ReplacementMap* replacement_map)
{
  ReplacementMap fixed_replacement_map;
  for (ReplacementMapIter iter = replacement_map->begin();
       iter != replacement_map->end();
       ++iter) {
    FLNode* flnode = iter->first;
    FLNode::Set& original_replacements = iter->second;
    FLNode::Set& new_replacements = fixed_replacement_map[flnode];
    FLNode::Set processed;
    for (FLNode::Set::iterator replacement_iter = original_replacements.begin();
         replacement_iter != original_replacements.end();
         ++replacement_iter) {
      FLNode* replacement = (*replacement_iter);
      fixupOneReplacement(replacement,
                          *replacement_map,
                          &new_replacements,
                          &processed);
    }
  }
  
  replacement_map->clear();
  replacement_map->insert(fixed_replacement_map.begin(),
                          fixed_replacement_map.end());
}

void AliasManager::fixupOneReplacement(FLNode* replacement,
                                       ReplacementMap& replacement_map,
                                       FLNode::Set* new_replacements,
                                       FLNode::Set* processed)
{
  if (processed->find(replacement) != processed->end()) {
    return;
  }
  processed->insert(replacement);

  ReplacementMapIter location = replacement_map.find(replacement);
  if (location != replacement_map.end()) {
    FLNode::Set& original_replacements = location->second;
    for (FLNode::Set::iterator iter = original_replacements.begin();
         iter != original_replacements.end();
         ++iter) {
      FLNode* original_replacement = (*iter);
      fixupOneReplacement(original_replacement,
                          replacement_map,
                          new_replacements,
                          processed);
    }    
  } else {
    new_replacements->insert(replacement);
  }
}

void AliasManager::fixupFanouts(const ReplacementMap &replacement_map)
{
  NUNetSet covered_nets;
  FLNodeSet covered_flow;

  for (ReplacementMapCIter iter = replacement_map.begin();
       iter != replacement_map.end();
       ++iter)
  {
    FLNode* flow = iter->first;
    NUNet* net = flow->getDefNet();
    if (covered_nets.find(net) == covered_nets.end()) {
      covered_nets.insert(net);
      fixupFanout(net, covered_flow, replacement_map);
    }
  }
}

void AliasManager::fixupFanout(NUNet* net,
                               FLNodeSet & covered_flow,
                               const ReplacementMap& replacement_map)
{
  NetNodeSetMapIter iter = mFanouts.find(net);
  if (iter == mFanouts.end()) return;

  NodeSenseSet *fanouts = iter->second;
  for (NodeSenseSet::iterator fanout_iter = fanouts->begin();
       fanout_iter != fanouts->end();
       ++fanout_iter) {
    FLNode *fanout = fanout_iter->first;
    if (covered_flow.find(fanout) == covered_flow.end()) {
      covered_flow.insert(fanout);
      fanout->replaceFaninsWithMap(replacement_map);
    }
  }
}

// Fix the remembered fanouts so they point to the donor flows.  This version
// only works for continuous drivers, but handles multiply driven nets
void AliasManager::repairContFlows() {
  // loop over all alias sets
  for (AliasGroupSet::iterator iter = mAliases.begin();
       iter != mAliases.end();
       ++iter) {
    AliasGroup* group = *iter;

    /* Update the net flags on the master to reflect all aliased net flags */
    flagAliases(group);

    group->repairFlows(mNetRefFactory);
  }
}

// Fix the remembered fanouts so they point to the donor flows.  This version
// only works for continuous drivers, but handles multiply driven nets
void AliasManager::fixupContFanouts() {
  // For every fanout we've remembered...
  for (NetNodeSetMapIter p = mFanouts.begin(); p != mFanouts.end(); ++p) {
    NUNet* net = p->first;
    NetAliasMap::iterator q = mNetToGroupMap.find(net);

    // If we've replaced the net, fix the fanout's fanin pointers
    if (q != mNetToGroupMap.end()) {
      AliasGroup* group = q->second;
      NU_ASSERT(mContAssignsOnly, net);
      NodeSenseSet* nodeSenseSet = p->second;

      for (NodeSenseSet::iterator n = nodeSenseSet->begin(),
             e = nodeSenseSet->end(); n != e; ++n)
      {
        FLNode* fanout = n->first;
        Sensitivity sense = n->second;

        // Collect all the old fanin flow-nodes that def this net
        FLNodeList oldDrivers;
        for (FLNode::FaninLoop f = fanout->loopFanin(); !f.atEnd(); ++f) {
          FLNode* fanin = *f;
          if (fanin->getDefNet() == net) {
            oldDrivers.push_back(fanin);
          }
        }

        // Replace with the new flows for the group
        if (not oldDrivers.empty()) {
          fanout->replaceFaninsWithLoop(oldDrivers, group->loopDonorFlows());
        }
        else {
          FLNode::FaninLoop flowLoop = FLNode::FaninLoop::create(group->loopDonorFlows());
          if (sense == eSensLevel) {
            fanout->connectFaninFromLoop(flowLoop);
          }
          else {
            FLN_ASSERT(sense == eSensEdge, fanout);
            fanout->connectEdgeFaninFromLoop(flowLoop);
          }
        }
      }
    }
  }
} // void AliasManager::fixupContFanout


void AliasManager::dumpAliases()
{
  // First, create an ordered set of alias masters
  typedef UtSet<NUNet*,NUNetCmp> NetOrderedSet;
  NetOrderedSet sorted_masters;
  for (AliasGroupSet::iterator iter = mAliases.begin(); iter != mAliases.end(); ++iter) {
    AliasGroup* group = *iter;
    sorted_masters.insert(group->getMaster());
  }

  // Go through the masters, print out the master/subord info
  for (NetOrderedSet::iterator iter = sorted_masters.begin();
       iter != sorted_masters.end();
       ++iter) {
    NUNet* master = *iter;
    UtString s;
    s += master->getScope()->getPrintableName();
    s += ".";
    master->compose(&s, NULL);
    UtIO::cout() << "Master:  " << s << UtIO::endl;

    AliasGroup* group = mNetToGroupMap[master];
    NU_ASSERT(group != NULL,master);
    NetOrderedSet sorted_subords;
    for (NUNetSet::UnsortedLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
    {
      sorted_subords.insert(*loop);
    }
    for (NetOrderedSet::iterator subord_iter = sorted_subords.begin();
         subord_iter != sorted_subords.end();
         ++subord_iter) {
      NUNet* subord = *subord_iter;
      s.clear();
      NUScope * scope = subord->getScope();
      s += scope->getPrintableName();
      s += ".";
      subord->compose(&s, NULL);
      UtIO::cout() << "  Subord:  " << s << UtIO::endl;
    }
  }
}

void AliasManager::cleanupAliasedNets()
{
  for (AliasGroupSet::iterator iter = mAliases.begin(); iter != mAliases.end(); ++iter) {
    AliasGroup* group = *iter;
    cleanupSubords(group);
  }
}

void AliasManager::cleanupSubords(AliasGroup* group)
{
  NUNet* master = group->getMaster();
  for (NUNetSet::UnsortedLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
  {
    NUNet* subord = *loop;

    // Do not alias in temporaries, because this will screw up block splitting later.
    // Basically, block splitting assumes that a temporary is deletable, but here
    // we could alias a port or other important signal to a temporary.
    //
    // However, the temporary may be aliased to other names which we want to keep.
    // So, in that case, we transfer the aliases of subord to master.
    if (not subord->isNonStatic()) {
      master->alias(subord);

      // clear the allocation bit from the subordinate.
      subord->putIsAllocated(false);
    } else {
      // dead code elimination will later remove these locals.
      master->transferAliases(subord);
    }
  }
}

void AliasManager::clear()
{
  mModule = NULL;

  for (AliasGroupSet::iterator iter = mAliases.begin(); iter != mAliases.end(); ++iter) {
    delete *iter;
  }
  mAliases.clear();

  mNetToGroupMap.clear();

  mCausalityIndexMap.clear();

  for (NetNodeSetMapIter iter = mFanouts.begin(); iter != mFanouts.end(); ++iter) {
    delete iter->second;
  }
  mFanouts.clear();

  for (NetNodeSetMapIter iter = mDrivers.begin(); iter != mDrivers.end(); ++iter) {
    delete iter->second;
  }
  mDrivers.clear();
}

bool AliasManager::queryAliases(NUNet* net, NUNet::QueryFunction fn) const
{
  NetAliasMap::const_iterator pos = mNetToGroupMap.find(net);
  if (pos == mNetToGroupMap.end())
  {
    // not aliased -- just check the net
    return (net->*fn)();
  }

  // the net is aliased -- check the master and all subordinates
  const AliasGroup* group = pos->second;
  if ((group->getMaster()->*fn)())
    return true;
  
  for (NUNetSet::UnsortedCLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
  {
    NUNet* subord = *loop;
    if ((subord->*fn)())
      return true;
  }

  return false;
}

NUNet* AliasManager::getCurrentMaster(NUNet * net) const
{
  NetAliasMap::const_iterator pos = mNetToGroupMap.find(net);
  if (pos == mNetToGroupMap.end())
    return net;

  const AliasGroup* group = pos->second;
  return group->getMaster();
}


void AliasManager::getCurrentAliases(NUNet * net, NUNetSet * nets) const
{
  NetAliasMap::const_iterator pos = mNetToGroupMap.find(net);
  if (pos == mNetToGroupMap.end()) {
    // not aliased -- just add the net
    nets->insert(net);
    return;
  }

  // the net is aliased -- add the master and all subordinates
  const AliasGroup* group = pos->second;
  nets->insert(group->getMaster());
  
  for (NUNetSet::UnsortedCLoop loop = group->loopSubordinates(); !loop.atEnd(); ++loop)
  {
    NUNet* subord = *loop;
    nets->insert(subord);
  }
}
