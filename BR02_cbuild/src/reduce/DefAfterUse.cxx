// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "DefAfterUse.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUUseDefNode.h"
#include "util/SetOps.h"

/*!
  \file
  Finds places in block where nets are re-defined after being used
 */

DefAfterUse::DefAfterUse() 
{
  mRedefAfterUse = new NUNetSet;
  mDefAfterUse   = new NUNetSet;
}


DefAfterUse::~DefAfterUse() 
{
  delete mRedefAfterUse;
  delete mDefAfterUse;
}


bool DefAfterUse::redefinedAfterUse(NUNet* net) 
{
  return (mRedefAfterUse->find(net) != mRedefAfterUse->end());
}


bool DefAfterUse::definedAfterUse(NUNet* net) 
{
  return (mDefAfterUse->find(net) != mDefAfterUse->end());
}


void DefAfterUse::module(NUModule* module) 
{
  for (NUModule::AlwaysBlockLoop p = module->loopAlwaysBlocks();
       !p.atEnd(); ++p) {
    alwaysBlock(*p);
  }
}


void DefAfterUse::clear() 
{
  mRedefAfterUse->clear();
  mDefAfterUse->clear();
}


void DefAfterUse::alwaysBlock(NUAlwaysBlock * always)
{
  NUNetSet previousDefs;
  NUNetSet previousUses;
  block(always->getBlock(), &previousDefs, &previousUses);
}


void DefAfterUse::stmt(NUStmt * stmt, NUNetSet * previousDefs, NUNetSet * previousUses)
{
  switch (stmt->getType()) {
  case eNUCase:
    caseStmt((NUCase*) stmt, previousDefs, previousUses);
    break;
  case eNUIf:
    ifStmt((NUIf*) stmt, previousDefs, previousUses);
    break;
  case eNUFor:
    forStmt((NUFor*) stmt, previousDefs, previousUses);
    break;
  case eNUBlock:
    block((NUBlock*) stmt, previousDefs, previousUses);
    break;

    // These cases for which we will do nothing in this routine are
    // explicitly left in here, rather than using a default, so that we
    // if someone adds a new type then he will get a compile warning
    // and be given the opportunity to consider whether this new statement
    // type must be explicity handled here.
  case eNUAlwaysBlock:
  case eNUAssign:
  case eNUBlockingAssign:
  case eNUCModelCall:
  case eNUCModel:
  case eNUCModelArgConnectionInput:
  case eNUCModelArgConnectionOutput:
  case eNUCModelArgConnectionBid:
  case eNUConcatLvalue:
  case eNUContAssign:
  case eNUControlSysTask:
  case eNUCycle:
  case eNUEnabledDriver:
  case eNUIdentLvalue:
  case eNUInitialBlock:
  case eNULvalue:
  case eNUMemselLvalue:
  case eNUModule:
  case eNUModuleInstance:
  case eNUNonBlockingAssign:
  case eNUOutputSysTask:
  case eNUInputSysTask:
  case eNUFOpenSysTask:
  case eNUFCloseSysTask:
  case eNUFFlushSysTask:
  case eNUPortConnectionBid:
  case eNUPortConnectionInput:
  case eNUPortConnectionOutput:
  case eNUStructuredProc:
  case eNUTF:
  case eNUTFArgConnectionBid:
  case eNUTFArgConnectionInput:
  case eNUTFArgConnectionOutput:
  case eNUTFElab:
  case eNUTask:
  case eNUTaskEnable:
  case eNUTriRegInit:
  case eNUPartselIndexLvalue:
  case eNUReadmemX:
  case eNUSysRandom:
  case eNUVarselLvalue:
  case eNUBlockingEnabledDriver:
  case eNUContEnabledDriver:
  case eNUNamedDeclarationScope:
  case eNUBreak:
  case eNUCompositeLvalue:
  case eNUCompositeIdentLvalue:
  case eNUCompositeSelLvalue:
  case eNUCompositeFieldLvalue: {
    // simple (non-compound) leaf-statement.
    NUNetSet defs;
    stmt->getBlockingDefs(&defs);
    stmt->getBlockingUses(previousUses);
    stmt->getNonBlockingUses(previousUses);

    // An alternate implementation:
    //     set_intersection(defs, *previousUses, *mDefAfterUse);
    // 
    //     NUNetSet redefs;
    //     set_intersection(*previousDefs, defs, &redefs);
    //     set_intersection(redefs, *previousUses, *mRedefAfterUse);
    // 
    // The chosen implementation avoids one set_intersection operation.

    NUNetSet my_def_after_use;
    set_intersection(defs, *previousUses, &my_def_after_use);
    mDefAfterUse->insert(my_def_after_use.begin(), my_def_after_use.end());

    set_intersection(my_def_after_use, *previousDefs, mRedefAfterUse);

    previousDefs->insert(defs.begin(), defs.end());
    break;
  }
  } // end switch
}


void DefAfterUse::block(NUBlock * stmt, NUNetSet * previousDefs, NUNetSet * previousUses)
{
  stmtList(stmt->loopStmts(), previousDefs, previousUses);
}


void DefAfterUse::ifStmt(NUIf * stmt, NUNetSet * previousDefs, NUNetSet * previousUses)
{
  NUNetSet conditionUses;
  stmt->getCond()->getUses(&conditionUses);

  // Process the then and else clauses independently.
  NUNetSet thenPreviousDefs(*previousDefs);
  NUNetSet thenPreviousUses(*previousUses);
  thenPreviousUses.insert(conditionUses.begin(), conditionUses.end());
  stmtList(stmt->loopThen(), previousDefs, previousUses);

  NUNetSet elsePreviousDefs(*previousDefs);
  NUNetSet elsePreviousUses(*previousUses);
  elsePreviousUses.insert(conditionUses.begin(), conditionUses.end());
  stmtList(stmt->loopElse(), previousDefs, previousUses);

  // Update needs to be after the else sets have been initialized
  // because we don't want to have defs/uses from the then clause
  // trickle into the else clause processing.
  previousDefs->insert(thenPreviousDefs.begin(), thenPreviousDefs.end());
  previousUses->insert(thenPreviousUses.begin(), thenPreviousUses.end());

  previousDefs->insert(elsePreviousDefs.begin(), elsePreviousDefs.end());
  previousUses->insert(elsePreviousUses.begin(), elsePreviousUses.end());
}


void DefAfterUse::caseStmt(NUCase * stmt, NUNetSet * previousDefs, NUNetSet * previousUses)
{
  NUNetSet selectUses;
  stmt->getSelect()->getUses(&selectUses);

  // Store away the updates to defs and uses; update as we exit.
  NUNetSet observedDefs;
  NUNetSet observedUses;

  // Process each case item independently.
  for (NUCase::ItemLoop items = stmt->loopItems();
       not items.atEnd();
       ++items) {
    NUCaseItem* item = *items;

    NUNetSet itemDefs(*previousDefs);
    NUNetSet itemUses(*previousUses);
    itemUses.insert(selectUses.begin(),selectUses.end());

    // Collect the nets used in the item trigger, adding them to those
    // used by the case selector.
    for (NUCaseItem::ConditionLoop conditions = item->loopConditions();
         not conditions.atEnd(); ++conditions) {
      NUCaseCondition* condition = *conditions;
      condition->getExpr()->getUses(&itemUses);
      NUExpr* mask = condition->getMask();
      if (mask != NULL) {
        mask->getUses(&itemUses);
      }
    }
    
    stmtList(item->loopStmts(), &itemDefs, &itemUses);

    observedDefs.insert(itemDefs.begin(), itemDefs.end());
    observedUses.insert(itemUses.begin(), itemUses.end());
  }

  // Update our state with all the processed defs/uses.
  previousDefs->insert(observedDefs.begin(), observedDefs.end());
  previousUses->insert(observedUses.begin(), observedUses.end());
  // Add the select uses just in case there were no items.
  previousUses->insert(selectUses.begin(), selectUses.end());
}


void DefAfterUse::forStmt(NUFor * stmt, NUNetSet * previousDefs, NUNetSet * previousUses)
{
  // Examine the initial block without seeing the uses for the condition
  stmtList(stmt->loopInitial(), previousDefs, previousUses);

  NUNetSet conditionUses;
  stmt->getCondition()->getUses(previousUses);

  // Consider this case:
  // for (i = 0; i < 10; i = i + 1) begin
  //   b = a;
  //   c = b;
  // end
  //
  // Is 'b' redefined after it is used? Yes. We just need to process
  // the advance and body twice to make that determination.
  for (int i = 0; i < 2; ++i) {
    stmtList(stmt->loopBody(), previousDefs, previousUses);
    stmtList(stmt->loopAdvance(), previousDefs, previousUses);
  }
}


void DefAfterUse::stmtList(NUStmtLoop loop, NUNetSet * previousDefs, NUNetSet * previousUses)
{
  for ( /*no initial*/; not loop.atEnd(); ++loop) {
    stmt(*loop, previousDefs, previousUses);
  }
}

                       
