// -*-C++-*-    $Revision: 1.3 $
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/Nucleus.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "util/UtIOStream.h"
#include "reduce/Inference.h"
#include "reduce/CommonConcat.h"
#include "reduce/OccurrenceLogger.h"
#include "reduce/InferenceLogger.h"

//! set options on the log
void InferenceLogger::set (const UInt32 options)
{
  if (mLog != NULL) {
    mLog->set (options);
  }
}

// An operator is defined for each (key, argument, ...) combination used in the
// code.

void InferenceLogger::operator () (const UInt32 key, const UInt32 n)
{
  if (mLog != NULL) {
    mLog->arg (n);
    mLog->log (NULL, 0, key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUAssign *assign)
{
  if (mLog != NULL) {
    mLog->arg (assign);
    mLog->log (assign->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUAssign *a, const NULvalue *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUAssign *a, const UInt32 b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUAssign *a, const UInt32 b, const UInt32 c)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->arg (c);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUExpr *expr)
{
  if (mLog != NULL) {
    mLog->arg (expr);
    mLog->log (expr->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUNet *a, const NUExpr *b, const NUExpr *c)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    if (c != NULL) {
      mLog->arg (c);
    }
    mLog->log (b->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUNet *a, const NULvalue *b, const NULvalue *c)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    if (c != NULL) {
      mLog->arg (c);
    }
    mLog->log (b->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUNet *a, const NUNet *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const NUModule *b, const ConcatAnalysis::NetMap::Edge *c, const NUNet *d, const NUNet *e)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->arg (c);
    mLog->arg (d);
    mLog->arg (e);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const UInt32 b, const UInt32 c)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->arg (c);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const UInt32 b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const UInt32 b, const ConcatAnalysis::NetMap *c)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->arg (c);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const NUModule *b, const ConcatAnalysis::NetMap::Edge *c, const OccurrenceLogger::Visible *d, const OccurrenceLogger::Visible *e)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->arg (c);
    mLog->arg (d);
    mLog->arg (e);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const ConcatAnalysis::NetMap::Node *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (b->getNet ()->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const ConcatAnalysis::NetMap *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModule *a, const NUNet *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const SourceLocator &loc,
  const NUModule *a, const NUNet *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (loc, key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUAssign *a, const ConcatAnalysis::NetMap *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUAssign *a, const NUAssign *b, const UInt32 c, const ConcatAnalysis::NetMap *d)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->arg (c);
    mLog->arg (d);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const UInt32 a, const NUModule *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (b->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModuleInstance *a)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModuleInstance *a, const UInt32 b, const UInt32 c)
{
  if (mLog != NULL) {
    mLog->arg (b);
    mLog->arg (c);
    mLog->arg (a);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModuleInstance *a, const UInt32 b)
{
  if (mLog != NULL) {
    mLog->arg (b);
    mLog->arg (a, true);
    mLog->log (a->getLoc (), key);
  }
}

void InferenceLogger::operator () (const UInt32 key, const NUModuleInstance *a, const NUPortConnection *b)
{
  if (mLog != NULL) {
    mLog->arg (a);
    mLog->arg (b);
    mLog->log (a->getLoc (), key);
  }
}

// An output method must be explicitly specialised for each argument types used
// in the logs.

template <>
SInt32 OccurrenceLogger::ArgT<const NUAssign *>::write  (char *s, const UInt32 size) const
{
  // The log file already includes the source file and line number so pull
  // this method pulls apart the assign and prints it without source
  // location information. Just using the compose method also appends the
  // location as a comment.
  const NULvalue *lvalue = mValue->getLvalue ();
  const NUExpr *rvalue = mValue->getRvalue ();
  UtString b0, b1;
  lvalue->compose (&b0, NULL, 0, true);
  rvalue->compose (&b1, NULL);
  UInt32 n;
  if (mValue->getType () == eNUContAssign) {
    n = snprintf (s, size, ": assign %s = %s", b0.c_str (), b1.c_str ());
  } else {
    n = snprintf (s, size, ": %s <= %s", b0.c_str (), b1.c_str ());
  }
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const NUExpr *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b, NULL);
  UInt32 n = snprintf (s, size, ": %s", b.c_str ());
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const NUUseDefNode *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b, NULL);
  UInt32 n = snprintf (s, size, ": %s", b.c_str ());
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const NULvalue *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b, NULL, 0, true);
  UInt32 n = snprintf (s, size, ": %s", b.c_str ());
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const NUNet *>::write  (char *s, const UInt32 size) const
{
  UInt32 n = snprintf (s, size, ": %s", mValue->getName ()->str ());
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const NUModule *>::write  (char *s, const UInt32 size) const
{
  UInt32 n = snprintf (s, size, ": module %s", mValue->getName ()->str ());
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const ConcatAnalysis::NetMap *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b, ConcatAnalysis::NetMap::cLINE|2);
  UInt32 n = snprintf (s, size, ":\n%s", b.c_str ());
  return n < size ? n : - n;
}

template<>
SInt32 OccurrenceLogger::ArgT<const ConcatAnalysis::NetMap::Node *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b);
  UInt32 n = snprintf (s, size, ": %s", b.c_str ());
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const NUModuleInstance *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  UInt32 n = 0;
  n += snprintf (s + n, size - n, ": %s", mValue->getParent ()->getName ()->str ());
  if (mBrief) {
    // just the instance name
    n += snprintf (s + n, size - n, ".%s", mValue->getName ()->str ());
  } else {
    mValue->compose (&b, NULL, 0, false);
    n += snprintf (s + n, size - n, " instance %s", b.c_str ()); 
 }
  return n < size ? n : - n;
}

template <>
SInt32 OccurrenceLogger::ArgT<const NUPortConnection *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b, NULL, 0, false);
  UInt32 n = snprintf (s, size, ": %s", b.c_str ());
  return n < size ? n : - n;
}

template<>
SInt32 OccurrenceLogger::ArgT<const ConcatAnalysis::NetMap::Edge *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b);
  UInt32 n = snprintf (s, size, ": %s", b.c_str ());
  return n < size ? n : - n;
}


