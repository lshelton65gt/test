// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
 * elaborated analysis to assign allocations
 */

#include "reduce/REAlloc.h"
#include "symtab/STSymbolTable.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STBranchNode.h"
#include "nucleus/NUNet.h"
#include "localflow/MarkReadWriteNets.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIndent.h"
#include <algorithm>
#include "flow/FLIter.h"
#include "flow/FLNodeElab.h"
#include "nucleus/NUNetSet.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUModule.h"
#include "reduce/ReachableAliases.h"
#include "REHierRefCovered.h"

// 0: Use "is written" as a primary sort criteria; depth is secondary.
// 1: Use "depth" as primary sort criteria; "is written" is secondary.
#define ALLOCATE_DEEP_BEFORE_WRITE 1

REAlloc::REAlloc(STSymbolTable* symtab,
                 IODBNucleus* iodb,
                 ArgProc* args,
                 MsgContext *msg_context,
                 bool dump,
                 Stats* stats,
                 FLNodeElabFactory *flow_elab_factory,
                 FLNodeFactory *flow_factory,
                 ReachableAliases *reachables)
  : mMsgContext(msg_context),
    mIODB(iodb),
    mArgs(args),
    mSymbolTable(symtab),
    mDump(dump),
    mStats(stats),
    mFlowElabFactory(flow_elab_factory),
    mFlowFactory(flow_factory),
    mReachableAliases(reachables)
{
  mLiveNetElabs = NULL;
  mDisqualifiedNets = new NUNetSet;
  mRequiresTriInit = new NUNetElabSet;
}

REAlloc::~REAlloc()
{
  if (mLiveNetElabs != NULL)
    delete mLiveNetElabs;
  delete mDisqualifiedNets;
  delete mRequiresTriInit;
}

static void sClearAlloc(NUNet* net)
{
  if (net->isAllocated())
    net->putIsAllocated(false);
}

// Note that this ignores pullups, as they are always instantiated by RETriAlias
// wherever we put the storage, so we don't need to consider them in deciding
// where to allocate.
static bool sNetWrites(NUNet* net, bool considerPrimaryPorts)
{
  bool ret = false;
  if (net->isWritten() or
      net->isTriWritten() or
      net->isPulled() or
      net->isDepositable() or
      net->isForcible() or 
      net->isHierRefWritten()) {
    ret = true;
  } else if (considerPrimaryPorts) {
    if (net->isPrimaryInput() or net->isPrimaryBid()) {
      ret = true;
    }
  }
  return ret;
}

// This is a helper function for iterating over the symbol table, when
// we are only interested in STAliasedLeafNodes, not STBranchNodes, and
// we only want the master in the alias ring.
static STAliasedLeafNode* sGetMasterLeaf(STSymbolTable::NodeLoop& i)
{
  STAliasedLeafNode* node = (*i)->castLeaf();
  if (node != NULL)
  {
    if (node->getMaster() != node)
      node = NULL;              // only start with master nodes
//    else if (node->getAlias() == node)
//      node = NULL;              // only interested in nets with aliases
  }
  return node;
}

// Remember which nets need tristate initialization. Allocation points are 
// computed in a different manner for these nets. Tristate initialization 
// expects to place an initial driver at the allocation location. 
// Therefore, allocation cannot occur at read-only aliases.
// 
// The tristate information is cached before the allocator begins changing 
// net flags.
void REAlloc::recordTriInitNets() {
  for (STSymbolTable::NodeLoop i = mSymbolTable->getNodeLoop();
       !i.atEnd(); ++i)
  {
    STAliasedLeafNode* node = sGetMasterLeaf(i);
    if (node != NULL)
    {
      NUNetElab* net_elab = NUNetElab::find(node);
      if (mReachableAliases->requiresTriInit(net_elab)) {
        mRequiresTriInit->insert(net_elab);
      }
    }
  }
}


bool REAlloc::design(NUDesign* design)
{
  recordTriInitNets();

  // Reset net r/w flags to ignore simple port connections
  {
    MarkReadWriteNets rw(mIODB, mArgs, false);
    rw.design(design);
  }

  // Find all the net elabs.
  findNetElabs(design);

  // Disqualify certain nets as allocation candidates. There are
  // currently two reasons for disqualification:
  // 
  // 1. Any NUNet that appears in a live alias ring more than once due to
  //    multiple instantiations is immediately disqualified.
  //
  // 2. Any NUNet covered by a hierarchical reference port connection
  //    is disqualified.
  bool success = findDisqualifiedNets(design);
  if (not success) {
    return false;
  }

  // first pass, pick candidate allocation
  greedyPickAlloc();

  // second pass, move storage up hierarchy till only one storage node per ring
  moveAllocsUp();

  // Reset net r/w flags to pay attention to simple port connections
  {
    MarkReadWriteNets rw(mIODB, mArgs, true);
    rw.design(design);
  }

  // third pass, propagate bidirect flags up
  if (mMoveUpSettled)
  {
    propagateFlags(design);
    sanityCheck();
  }

  if (mDump)
    dump();

  return mMoveUpSettled;
} // bool REAlloc::design

// Find the net-elabs in the design.
void REAlloc::findNetElabs(NUDesign* design)
{
  // We perform allocation over all net elabs and not just the live
  // ones. This ensures that codegen can emit legal references to all
  // nets, even if they are dead.

  NU_ASSERT(mLiveNetElabs == NULL, design);
  NUNetElabSet liveElabs;

  for (STSymbolTable::NodeLoop i = mSymbolTable->getNodeLoop();
       not i.atEnd(); ++i) {
    STAliasedLeafNode* node = sGetMasterLeaf(i);
    if (node != NULL) {
      NUNetElab* netElab = NUNetElab::find(node);
      ST_ASSERT(netElab,node);
      if (not netElab->getNet()->isVirtualNet()) {
        liveElabs.insert(netElab);
      }
    }
  }

  // Get the protected mutable/observable nunetelabs (just to be sure)
  mIODB->appendProtectedNetElabs(&liveElabs, design);

  // Now get them in canonical sorted order and store them
  mLiveNetElabs = new NUNetElabVector(liveElabs.size());
  SInt32 i = 0;
  NU_ASSERT((mLiveNetElabs->size() == liveElabs.size()), design);
  for (NUNetElabSet::SortedLoop p = liveElabs.loopSorted(); !p.atEnd(); ++p)
    (*mLiveNetElabs)[i++] = *p;
} // void REAlloc::findLiveNetElabs


bool REAlloc::findDisqualifiedNets(NUDesign * design)
{
  disqualifyRepeatedUnelaboratedNets();
  bool success = disqualifyHierRefCoverage(design);
  return success;
}


void REAlloc::disqualifyRepeatedUnelaboratedNets()
{
  NUNetSet netsInRing;
  for (UInt32 i = 0; i < mLiveNetElabs->size(); ++i)
  {
    netsInRing.clear();
    NUNetElab* masterNet = (*mLiveNetElabs)[i];
    STAliasedLeafNode* masterNode = masterNet->getSymNode();
    for (STAliasedLeafNode::AliasLoop p(masterNode); !p.atEnd(); ++p)
    {
      STAliasedLeafNode* alias = *p;
      if (mReachableAliases->query(alias))
      {
        NUNet* aliasNet = NUNet::find(alias);
        if (netsInRing.find(aliasNet) == netsInRing.end())
          netsInRing.insert(aliasNet);
        else
          mDisqualifiedNets->insert(aliasNet);
      }
    }
  }
}


bool REAlloc::disqualifyHierRefCoverage(NUDesign * design)
{
  REHierRefCovered hier_ref_covered(design,mSymbolTable,mMsgContext);
  return hier_ref_covered.discover(mDisqualifiedNets);
}


// Pick the deepest level net at which a write occurs, and assign it
// the allocation.  This algorithm can produce alias-rings with more
// than one allocation, but we can fix it in a later pass.  Undriven
// nets are allocated at the master node.
void REAlloc::pickBestNodeInRing(STAliasedLeafNode* masterNode,
                                 bool pickHighest)
{
  STAliasedLeafNode* storage = NULL;
  SInt32 storageDepth = 0;
  bool storageWrites = false;

  NUNetElab* masterNet = NUNetElab::find(masterNode);
  bool isComplexNet =
    masterNet->queryAliases(&NUNet::isForcible) ||
    //masterNet->queryAliases(&NUNet::isPrimaryZ) ||
    (mRequiresTriInit->find(masterNet) != mRequiresTriInit->end());

  pickHighest |= masterNet->queryAliases(&NUNet::isPrimaryZ);

  for (STAliasedLeafNode::AliasLoop p(masterNode); !p.atEnd(); ++p)
  {
    STAliasedLeafNode* alias = *p;
    NUNet* aliasNet = NUNet::find(alias);
/*
    if (user has specified "-tristate z")
      isComplexNet |= masterNet->queryAliases(&NUNet::isTriWritten);
*/

    bool reachable_alias = mReachableAliases->query(alias);
    bool qualified_alias = not isDisqualifiedNet(aliasNet);

    if (reachable_alias or aliasNet->isHierRef()) {
      // We are in the process of computing the allocation for all
      // reachable nets. Clear all allocation bits as we go; the
      // proper allocation is assigned at the end. For hierarchical
      // aliases, clear all allocation bits - they can never be the
      // storage point.
      sClearAlloc(aliasNet);
    }

    if (reachable_alias and qualified_alias) {
      SInt32 aliasDepth = NUScope::determineModuleDepth(alias);
      bool aliasWrites =  sNetWrites(aliasNet, true);
      if (pickHighest)
      {
        if ((storage == NULL) || (aliasDepth < storageDepth))
        {
          storageDepth = aliasDepth;
          storage = alias;
        }
        else if (aliasDepth == storageDepth) {
          // If we find two qualified nets at the same level of
          // hierarchy, pick the alphabetically earliest one.
          if (HierName::compare(alias, storage) < 0)
          {
            storage = alias;
            storageDepth = aliasDepth;
            storageWrites = aliasWrites;
          }
        }
      }
      else
      {
        // Look for the "best" net to receive the allocation. If the
        // net is complex, then we want to find the lowest level
        // writer.
        if (!isComplexNet || aliasWrites)
        {
#if ALLOCATE_DEEP_BEFORE_WRITE
          // If the net is simple, then we want to find the lowest
          // level net, independent of whether it is read or written.
          // 
          // If we find two qualified nets at the same level of
          // hierarchy, pick a writer over a non-writer, and after
          // that, pick the alphabetically earliest one.
          if ((storage == NULL) ||
              (aliasDepth > storageDepth) ||                // 1: go deep
              ((aliasDepth == storageDepth) &&
               ((aliasWrites && !storageWrites) ||          // 2: prefer writers
                ((aliasWrites == storageWrites) &&
                 (HierName::compare(alias, storage) < 0)))))
          {
            storage = alias;
            storageDepth = aliasDepth;
            storageWrites = aliasWrites;
          }
#else
          // Alternate allocation strategy. Choose write locations
          // over unwritten, even when the unwritten is lower in the
          // hierarchy.
          //
          // If both the current storage choice and this alias are
          // written, prefer deep aliases over shallow ones. As a
          // final tie-breaker, perform a lexical comparison.
          bool aliasWins = (storage==NULL);
          if (storageWrites) {
            if (aliasWrites) {
              // They both write; prefer depth.
              if (aliasDepth==storageDepth) {
                // Same depth; sort by name.
                aliasWins |= (HierName::compare(alias, storage) < 0);
              } else {
                aliasWins |= (aliasDepth > storageDepth);
              }
            } else {
              // Storage the only writer. No change.
            }
          } else {
            if (aliasWrites) {
              // Alias the only writer. It wins.
              aliasWins = true;
            } else {
              // Neither is a writer; prefer depth.
              if (aliasDepth==storageDepth) {
                aliasWins |= (HierName::compare(alias, storage) < 0);
              } else {
                aliasWins |= (aliasDepth > storageDepth);
              }
            }
          }

          if (aliasWins) {
            storage = alias;
            storageDepth = aliasDepth;
            storageWrites = aliasWrites;
          }
#endif
        }
      } // if
    } // if
  } // for

  if (storage == NULL)
  {
    if (mReachableAliases->query(masterNode) &&
        !isDisqualifiedNet(NUNet::find(masterNode)))
    {
      storage = masterNode;
    }
    else
    {
      // The "best name" in the ring turned out to be a name that's
      // been flattened away, so pick the highest legal one instead.
      ST_ASSERT(!pickHighest,masterNode);
      pickBestNodeInRing(masterNode, true);
    }      
  }
  if (storage != NULL)
  {
    storage->setThisStorage();
    NUNet* storageNet = NUNet::find(storage);
    storageNet->putIsAllocated(true);
  }
} // void REAlloc::pickBestNodeInRing

void REAlloc::greedyPickAlloc()
{
  for (UInt32 i = 0; i < mLiveNetElabs->size(); ++i)
  {
    NUNetElab* masterNet = (*mLiveNetElabs)[i];
    STAliasedLeafNode* masterNode = masterNet->getSymNode();

    // primary BIDs must be allocated at top level.  BIDs must be
    // at the top level to matching results on some fairly simple testcases
    // (comment this out & run allsmall to figure which ones -- I've forgotten)
    // and primary inputs are needed for CWTB sanity
    bool isPrimaryInput = masterNet->queryAliases(&NUNet::isPrimaryBid);
    // || masterNet->queryAliases(&NUNet::isPrimaryInput));
    bool pickHighest = isPrimaryInput;

    pickBestNodeInRing(masterNode, pickHighest);
  } // for
} // void REAlloc::greedyPickAlloc

void REAlloc::moveAllocsUp()
{
  // second pass:  detect more than one allocation, move the allocation up one
  // level in the hierarchy.  That algorithm has to eventually converge on
  // hitting the top level of the design.  When there is a tie the alphabetically
  // earliest qualified alias-node wins.  Could also have picked the causally
  // earliest.  Not sure it matters.
  mMoveUpSettled = false;
  const SInt32 maxIters = 500;
  for (SInt32 i = 0; (i < maxIters) && !mMoveUpSettled; ++i)
  {
    mMoveUpSettled = true;
    for (UInt32 j = 0; j < mLiveNetElabs->size(); ++j)
    {
      NUNetElab* masterNet = (*mLiveNetElabs)[j];
      STAliasedLeafNode* masterNode = masterNet->getSymNode();
      // Look for multiple allocations.  When we find one, move the allocation
      // up one level in the hierarchy and rescan the design
      STAliasedLeafNode* storage = masterNode->getStorage();
      NU_ASSERT(storage,masterNet);
      if (storage != NULL)    // $extnet has no storage
      {
        for (STAliasedLeafNode::AliasLoop p(masterNode); !p.atEnd(); ++p)
        {
          STAliasedLeafNode* alias = *p;
          if (mReachableAliases->query(alias))
          {
            NUNet* aliasNet = NUNet::find(alias);
            bool storageAndAllocationConsistent =
              (aliasNet->isAllocated() == (alias == storage));
            if (! storageAndAllocationConsistent)
            {
              // Because REAlias uses a design fanin walk, it does not
              // cover all the nodes.  This can leave disconnected
              // subgraphs.  Warn on these.  Error for multiple
              // allocations.  We should have killed all these.
              if (i == maxIters - 1)
                reportError(masterNode, aliasNet->isAllocated());
              else
              {
                pickBestNodeInRing(masterNode, true);
                mMoveUpSettled = false;
                break;
              }
            } // if
          } // if
        } // for
      } // if
    } // for
  } // for
} // void REAlloc::moveAllocsUp

void REAlloc::reportError(STAliasedLeafNode* masterNode, bool dup)
{
  NUNetElab* netElab = NUNetElab::find(masterNode);
  if (dup)
  {
    mMsgContext->REDupAlloc(netElab);
    mMoveUpSettled = false;
  }
  else
    mMsgContext->RENoAlloc(netElab);
  for (STAliasedLeafNode::AliasLoop p(masterNode);
       !p.atEnd(); ++p)
  {
    STAliasedLeafNode* alias = *p;
    if (alias != netElab->getSymNode())
      mMsgContext->REAllocCont(alias);
  }
}

// This routine is currently not used.   It may be used in the future
// for a more aggressive allocation algorithm where if the deepest
// allocation fails we try to move the allocation up in the hierarchy.
// This was found on some designs to have bad convergence properties
// in the current implementation, and so moveAllocsUp(), when it
// finds an allocation conflict in its most aggressive attemp, jumps
// immediately to a known legal allocations for all alias rings in
// violation.
#if 0
void REAlloc::moveStorageUpHierarchy(STAliasedLeafNode* node,
                                     STAliasedLeafNode* alias)
{
  STAliasedLeafNode* storage = node->getStorage();
  SInt32 desiredDepth = NUScope::determineModuleDepth(storage) - 1;
  NUNet* oldStorageNet = NUNet::find(storage);
  NUNet* oldAliasNet = NUNet::find(alias);

  for (storage = NULL; (storage == NULL) && (desiredDepth > 1); --desiredDepth)
  {
    for (STAliasedLeafNode::AliasLoop p(node); !p.atEnd(); ++p)
    {
      alias = *p;
      if (mReachableAliases->query(alias))
      {
        SInt32 alias_depth = NUScope::determineModuleDepth(alias);
        if ((alias_depth == desiredDepth) &&
            ((storage == NULL) ||
             (HierName::compare(alias, storage) < 0)))
          storage = alias;
      }
      alias = alias->getAlias();
    }
  }
  if (storage == NULL)
  {
    // There were no eligible aliases at a higher level, so we have to
    // pick an alias at some level.  Probably whatever the current
    // storage is set to is OK
    storage = node->getStorage();
  }
  sClearAlloc(oldStorageNet);

  storage->setThisStorage();
  sClearAlloc(oldAliasNet);
  NUNet* storageNet = NUNet::find(storage);
  storageNet->putIsAllocated(true);
}
#endif

// Any NUNet* that appears twice in one ring is disqualified from
// owning the storage allocation.  We may disqualify NUNets for 
// other reasons in the future.
bool REAlloc::isDisqualifiedNet(NUNet* net)
{
  bool isDisqualified = false;
  if (net->isHierRef())
    isDisqualified = true;
  else if (mDisqualifiedNets->find(net) != mDisqualifiedNets->end())
    isDisqualified = true;
  
  return isDisqualified;
}

//! Callback class for walker to discover nets that should be marked primary Z
class PrimaryZFinder : public NUDesignCallback
{
public:
  //! constructor.
  /*!
    \param module_recurse  If false, will only visit 1 module
   */
  PrimaryZFinder() :
    NUDesignCallback()
  {
    mIsModified = false;
  }

  //! destructor
  ~PrimaryZFinder() {}

  //! By default, visit all nodes
  Status operator()(Phase, NUBase*) { return eNormal;}

/*
  //! Check if we should recurse into submodules
  Status operator()(Phase phase, NUModule*)
  {
    if (phase == ePre)
      return eSkip;
    return eNormal;
  }
*/

  Status operator()(Phase phase, NUPortConnectionOutput *con)
  {
    if (phase == ePre)
      markOutCon(con->getFormal(), con->getActual());
    return eSkip;
  }

  Status operator()(Phase phase, NUPortConnectionBid *con)
  {
    if (phase == ePre)
      markOutCon(con->getFormal(), con->getActual());
    return eSkip;
  }

  Status operator()(Phase phase, NUPortConnectionInput *con)
  {
    if (phase == ePre)
      markCon(con->getFormal(), con->getActual()->getWholeIdentifier());
    return eSkip;
  }

  bool isModified() const {return mIsModified;}
  void clearModified() {mIsModified = false;}

private:
  //! Hide copy and assign constructors.
  PrimaryZFinder(const PrimaryZFinder&);
  PrimaryZFinder& operator=(const PrimaryZFinder&);

  void markOutCon(NUNet* formal, NULvalue* lvalue)
  {
    // This must be called after port lowering
    NUNet* actual = lvalue->getWholeIdentifier();
    markCon(formal, actual);
  }

  void markCon(NUNet* formal, NUNet* actual)
  {
    // Propagate primary-Z down into writers
    if (actual->isPrimaryZ())
    {
      if (!formal->isPrimaryZ() && sNetWrites(formal, false))
      {
        formal->putIsPrimaryZ(true);
        mIsModified = true;
      }
    }
    else if (formal->isPrimaryZ())
    {
      // always propgate primary-Z up, even if actual does not write
      actual->putIsPrimaryZ(true);
      mIsModified = true;
    }
  }

  bool mIsModified;
};

// propagate primary-Z flags up the hierarchy from the storage point
//
// Joe's comments:
//
//   We should always be able to find a solution for this, we
//   shouldn't have to use an iterative solution with an artificial iteration
//   cap.  For instance, maybe we should construct a directed graph of the
//   port connections (port coercion already has code to do something like this).
//
// This is probably true, and not very difficult, although it involves
// allocating a chunk of memory and in practice this iterative algorithm
// does converge very quickly.
//
//   Also, this really is not an allocation issue, perhaps we should move
//   this algorithm to its own class.
//
// This is definitely true.  This should be somewhere else.  REPropagate?
// It does need the ReachableAliases infrastructure, however.  We need
// to determine those prior to REAlias to avoid assigning storage to NUNets
// that are flattened away, and therefore not represented in the module
// as it gets realized as a C++ class.  We see those in the alias ring
// because we keep all the names for ports of flattened submodules.
//
void REAlloc::propagateFlags(NUDesign* design)
{
  NUModuleList mods;
  design->getModulesBottomUp(&mods);

  // Clear then set the flags
  PrimaryZFinder zFinder;
  NUDesignWalker zWalker(zFinder, false);

  // Walk top-down by traversing the bottom-up list backwards
  const SInt32 maxIters = 100;

  // Re-traverse until there is nothing more to do.
  bool propagateSettled = false;
  for (SInt32 i = 0; (i < maxIters) && !propagateSettled; ++i)
  {
    propagateSettled = true;

    // Propagate primary-Zs unconditionally up, and down if the
    // lower-level module writes to the net.
    zFinder.clearModified();
    for (NUModuleList::reverse_iterator p = mods.rbegin(); p != mods.rend(); ++p)
    {
      NUModule* mod = *p;
      NUDesignCallback::Status status = zWalker.module(mod);
      NU_ASSERT(status == NUDesignCallback::eNormal,mod);
    }
    if (zFinder.isModified())
      propagateSettled = false;

    // Ensure every elaborated storage-net is marked as primary-Z if
    // anyone in its alias ring is marked primary-Z.
    for (STSymbolTable::NodeLoop i = mSymbolTable->getNodeLoop();
         !i.atEnd(); ++i)
    {
      STAliasedLeafNode* node = sGetMasterLeaf(i);
      if (node != NULL)
      {
        STAliasedLeafNode* storage = node->getStorage();
        NUNet* storageNet = NUNet::find(storage);
        if (!storageNet->isPrimaryZ())
        {
          for (STAliasedLeafNode::AliasLoop p(node); !p.atEnd(); ++p)
          {
            STAliasedLeafNode* alias = *p;
            if (mReachableAliases->query(alias))
            {
              NUNet* aliasNet = NUNet::find(alias);
              if (aliasNet->isPrimaryZ())
              {
                storageNet->putIsPrimaryZ(true);
                propagateSettled = false;
                break;
              }
            }
          }
        }
      }
    } // for
  } // for

  NU_ASSERT(propagateSettled, design);
} // void REAlloc::propagateFlags

struct NetElabCmp {
  bool operator()(const NUNetElab* n1, const NUNetElab* n2) {
    return *n1 < *n2;
  }
};

// Verify that:
//   - there is exactly one allocated NUNet* per alias ring
//   - it corresponds to the alias ring's storage-node 
void REAlloc::sanityCheck()
{
  for (STSymbolTable::NodeLoop i = mSymbolTable->getNodeLoop();
       !i.atEnd(); ++i)
  {
    STAliasedLeafNode* node = sGetMasterLeaf(i);
    if (node != NULL)
    {
      STAliasedLeafNode* storage = node->getInternalStorage();
      for (STAliasedLeafNode::AliasLoop p(node); !p.atEnd(); ++p)
      {
        STAliasedLeafNode* alias = *p;
        NUNet* aliasNet = NUNet::find(alias);
        if (mReachableAliases->query(alias))  // skip $extnet
        {
          bool storageAndAllocationConsistent =
            (aliasNet->isAllocated() == (alias == storage));
          if (!storageAndAllocationConsistent)
          {
            // It is OK for a locally dead alias of a live net
            // to not have its storage properly allocated.
            NUNetElab* netElab = NUNetElab::find(node);
            bool isLive = std::binary_search(mLiveNetElabs->begin(),
                                             mLiveNetElabs->end(),
                                             netElab,
                                             NetElabCmp());
            NU_ASSERT(!isLive,netElab);
            ST_ASSERT(storage == NULL,node);
/*
            if (aliasNet->isAllocated())
            {
              // the storage node must be elaboratedly dead.  Move the storage
              NUNet* storageNet = NUNet::find(storage);
              assert(storageNet->isDead() || !storageNet->isAllocated() ||  // this_assert_OK
                     aliasNet->isDead() || (aliasNet == storageNet));
              // alias->setThisStorage();
            }
*/
          }
        }
      }
    }
  }

  // to enable the fix for bug3085 in codegen/CodeSchedule.cxx, we need to know
  // that NUNetElabs that are reachable in a flow walk have non-null storage
  for (UInt32 i = 0; i < mLiveNetElabs->size(); ++i)
  {
    NUNetElab* masterNet = (*mLiveNetElabs)[i];
    STAliasedLeafNode* node = masterNet->getSymNode();
    NU_ASSERT(node->getInternalStorage() != NULL, masterNet);
  }

} // void REAlloc::sanityCheck

struct STALNCmp {
  bool operator()(const STAliasedLeafNode* n1, const STAliasedLeafNode* n2) {
    return *n1 < *n2;
  }
};

void REAlloc::printRing(STAliasedLeafNode* master)
{
  UtOStream& out = UtIO::cout();
  UtString buf;
  master->getStorage()->compose(&buf);
  out << buf << "\n";
  UtVector<STAliasedLeafNode*> aliases;
  for (STAliasedLeafNode::AliasLoop p(master); !p.atEnd(); ++p)
  {
    aliases.push_back(*p);
  }
  std::sort(aliases.begin(), aliases.end(), STALNCmp());
  for (UInt32 j = 0; j < aliases.size(); ++j)
  {
    STAliasedLeafNode* alias = aliases[j];
    buf.clear();
    UtIndent indent(&buf);
    if (!mReachableAliases->query(alias))
      buf << "*";
    indent.tab(2);
    alias->compose(&buf);
    NUNet* net = NUNet::find(alias);
    buf << " ";
    indent.tab(45);
    NUModule* mod = net->getScope()->getModule();
    buf << mod->getName()->str() << ".";
    net->getNameLeaf()->compose(&buf);
    net->composeDeclarePrefix(&buf);
    buf << " ";
    indent.tab(70);
    net->composeFlags(&buf);
    indent.newline();
    out << buf;
  }
  out << UtIO::endl;
} // void REAlloc::printRing

// Dump all the alias rings
void REAlloc::dump()
{
  UtString buf;
  UtOStream& out = UtIO::cout();
  for (UInt32 i = 0; i < mLiveNetElabs->size(); ++i)
  {
    NUNetElab* masterNet = (*mLiveNetElabs)[i];
    STAliasedLeafNode* master = masterNet->getSymNode();
    out << "Ring " << i << " storage: ";
    printRing(master);
  } // for
}
