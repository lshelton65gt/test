// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "CollectBlocks.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCase.h"

void CollectBlocks::stmt(NUStmt * one)
{
  NUIf *if_stmt = dynamic_cast<NUIf*>(one);
  if (if_stmt) {
    ifStmt(if_stmt);
    return;
  }

  NUCase *case_stmt = dynamic_cast<NUCase*>(one);
  if (case_stmt != NULL)
  {
    caseStmt(case_stmt);
    return;
  }

  NUBlock *block_stmt = dynamic_cast<NUBlock*>(one);
  if (block_stmt) {
    blockStmt(block_stmt);
    return;
  }

  NUFor * for_stmt = dynamic_cast<NUFor*>(one);
  if (for_stmt) {
    forStmt(for_stmt);
    return;
  }
}

void CollectBlocks::blockStmt(NUBlock * one)
{
  mBlocks.insert(one);
  for (NUStmtLoop loop = one->loopStmts();
       not loop.atEnd();
       ++loop) {
    stmt(*loop);
  }
}

void CollectBlocks::ifStmt(NUIf * one)
{
  for (NUStmtLoop loop = one->loopThen();
       not loop.atEnd();
       ++loop) {
    stmt(*loop);
  }
  for (NUStmtLoop loop = one->loopElse();
       not loop.atEnd();
       ++loop) {
    stmt(*loop);
  }
}

void CollectBlocks::caseStmt(NUCase * one)
{
  for (NUCase::ItemLoop item_loop = one->loopItems();
       not item_loop.atEnd();
       ++item_loop) {
    NUCaseItem* item = (*item_loop);
    for (NUStmtLoop loop = item->loopStmts();
	 not loop.atEnd();
	 ++loop) {
      stmt(*loop);
    }
  }
}

void CollectBlocks::forStmt(NUFor * one)
{
  for (NUStmtLoop loop = one->loopBody();
       not loop.atEnd();
       ++loop) {
    stmt(*loop);
  }
}
