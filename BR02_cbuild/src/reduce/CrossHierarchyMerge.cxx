//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "reduce/CrossHierarchyMerge.h"
#include "nucleus/NUUseDefNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "symtab/STBranchNode.h"
#include "util/UtIOStream.h"


//! UD Node elaborated at some point in the hierarchy
/*!
 * This is needed because the analysis needs to ensure
 * that all elaborations of a UD node behave the same
 * for it to be mergeable.
 *
 * NOTE: this is similar to scheduling's Block data structure.
 * If we ever do anything with this code, we should consider
 * merging the ideas and making it available publicly.
 */
class ElabUDNode
{
public:
  //! constructor
  ElabUDNode(NUUseDefNode *node, STBranchNode *hier) :
    mNode(node),
    mHier(hier)
  {}

  //! copy constructor
  ElabUDNode(const ElabUDNode& other) :
    mNode(other.mNode),
    mHier(other.mHier)
  {}

  //! assignment operator
  ElabUDNode& operator=(const ElabUDNode &other)
  {
    if (&other != this) {
      this->mNode = other.mNode;
      this->mHier = other.mHier;
    }
    return *this;
  }

  //! destructor
  ~ElabUDNode() {}

  //! Comparison operator, so that ElabUDNode can be used in STL containers.
  bool operator<(const ElabUDNode& other) const
  {
    SInt64 diff = other.mNode - mNode;
    if (diff == 0) {
      diff = other.mHier - mHier;
    }
    return (diff < 0);
  }

  //! Return the UD node
  NUUseDefNode *getNode() const { return mNode; }

  //! Return the elaboration hierarchy
  STBranchNode *getHier() const { return mHier; }

  //! Equality operator
  bool operator==(const ElabUDNode &other)
  {
    return ((other.mNode == mNode) and (other.mHier == mHier));
  }

private:
  //! The UD node
  NUUseDefNode *mNode;

  //! The elaboration hierarchy
  STBranchNode *mHier;
};


//! Compute fanin and fanout for the ElabUDNode and NUUseDefNodes
/*!
 * This information is computed over the design, and then used to
 * determine which nodes are mergeable.
 */
class ComputeFanout
{
public:
  //! constructor
  ComputeFanout() {}

  //! destructor
  ~ComputeFanout() {}

  //! Compute UD fanin/fanout for the design
  void design(NUDesign *design, STSymbolTable *symtab);

  //! Return the number of fanouts of the given node.
  size_t numFanouts(NUUseDefNode *node);

  //! Return true if the UD node should be excluded because it has varied fanouts
  /*!
   * A node will be disqualified if it does not fan out to the same UD node
   * for all elaborations.
   */
  bool isDisqualified(NUUseDefNode *node) const;

private:
  //! Elaborated UD graph
  typedef UtMultiMap<ElabUDNode,ElabUDNode> ElabUDFaninMap;
  ElabUDFaninMap mElabUDFaninMap;  // (key fans-in to value)

  //! Set of elaborated UD nodes;  all of the ElabUDNodes there are
  typedef UtSet<ElabUDNode> ElabUDSet;
  ElabUDSet mElabUDNodeSet;

  //! Map of an unelaborated UD node to all its fanin/fanout
  typedef UtMap<NUUseDefNode*,NUUseDefSet> UDFanMap;
  UDFanMap mUDFanouts;
  UDFanMap mUDFanins;

  //! UD nodes that cannot be merged because they have inconsistent fanout.
  NUUseDefSet mDisqualified;
};


void ComputeFanout::design(NUDesign *design,
                           STSymbolTable *symtab)
{
  // Compute the ElabUDNode & unelab UDNode graphs
  FLDesignElabIter iter(design,
                        symtab,
                        FLIterFlags::eIOOnly,             // start IO's
                        FLIterFlags::eAll,                // visit all
                        FLIterFlags::eNone,               // stop at nothing
                        FLIterFlags::eOverAll,            // do not go into nesting
                        FLIterFlags::eIterFaninPairOnce); // visit all fanin/fanout pairs once
  for (; not iter.atEnd(); ++iter) {
    FLNodeElab *cur = iter.getCur();
    FLNodeElab *fanout = iter.getCurParent();

        // If no fanout, its a start node.  Need a fanout to consider mergeables.
    if (not fanout) {
      continue;
    }

    NUUseDefNode *cur_ud = cur->getUseDefNode();
    NUUseDefNode *fanout_ud = fanout->getUseDefNode();
    STBranchNode *cur_hier = cur->getHier();
    STBranchNode *fanout_hier = fanout->getHier();

    ElabUDNode cur_elab_ud(cur_ud, cur_hier);
    ElabUDNode fanout_elab_ud(fanout_ud, fanout_hier);

    // Add the ElabUDNodes to the set of all ElabUDNodes
    mElabUDNodeSet.insert(cur_elab_ud);
    mElabUDNodeSet.insert(fanout_elab_ud);

    // Remember elaborated fanin
    mElabUDFaninMap.insert(ElabUDFaninMap::value_type(fanout_elab_ud, cur_elab_ud));

    // Remember unelaborated fanin/fanout
    mUDFanins[fanout_ud].insert(cur_ud);
    mUDFanouts[cur_ud].insert(fanout_ud);
  }

  // Figure out which UD nodes do not always fanout to the same UD node.
  // Disqualify them.
  for (ElabUDSet::iterator iter = mElabUDNodeSet.begin();
       iter != mElabUDNodeSet.end();
       ++iter) {
    ElabUDNode cur = *iter;

    // First, get the complete set of unelaborated fanin for this node.
    // Note: this set is the set of all possible unelaborated fanins for
    // this unelaborated UD node.
    NUUseDefSet &fanins = mUDFanins[cur.getNode()];
    NUUseDefSet unelab_fanin_set(fanins);

    // Now, remove the unelaborated nodes which are seen as fanin in this elaboration.
    std::pair<ElabUDFaninMap::iterator,ElabUDFaninMap::iterator> range_iter = mElabUDFaninMap.equal_range(cur);
    for (ElabUDFaninMap::iterator fanout_iter = range_iter.first;
         fanout_iter != range_iter.second;
         ++fanout_iter) {
      unelab_fanin_set.erase((*fanout_iter).second.getNode());
    }

    // Anything left over is disqualified.
    mDisqualified.insert(unelab_fanin_set.begin(), unelab_fanin_set.end());
  }
}


size_t ComputeFanout::numFanouts(NUUseDefNode *node)
{
  return mUDFanouts[node].size();
}


bool ComputeFanout::isDisqualified(NUUseDefNode *cur) const
{
  return (mDisqualified.find(cur) != mDisqualified.end());
}


CrossHierarchyMerge::CrossHierarchyMerge()
{
}


CrossHierarchyMerge::~CrossHierarchyMerge()
{
}


void CrossHierarchyMerge::design(NUDesign *design, STSymbolTable *symtab)
{
  // Compute fanin/fanout of both elaborated flow nodes and unelaborated UD nodes.
  ComputeFanout fanout_info;
  fanout_info.design(design, symtab);

  //! Map of mergeable nodes:  node -> fanout node
  //! Sorted to make printing consistent
  typedef UtMap<NUUseDefNode*,NUUseDefNode*,NUUseDefNodeCmp> UDMap;
  UDMap mUDMergeMap;

  // Traverse design, looking for single-fanout cross hierarchy situations.
  FLDesignElabIter iter(design,
                        symtab,
                        FLIterFlags::eIOOnly,             // start IO's
                        FLIterFlags::eAll,                // visit all
                        FLIterFlags::eNone,               // stop at nothing
                        FLIterFlags::eOverAll,            // do not go into nesting
                        FLIterFlags::eIterFaninPairOnce); // visit all fanin/fanout pairs once
  for (; not iter.atEnd(); ++iter) {
    FLNodeElab *cur = iter.getCur();
    FLNodeElab *fanout = iter.getCurParent();
    
    // If no fanout, its a start node.  Need a fanout to consider mergeables.
    if (not fanout) {
      continue;
    }

    // Disregard bound nodes.
    if (iter.getCurFlags() & FLIterFlags::eBound) {
      continue;
    }

    // Weed out:
    // . no initial drivers
    // . no enabled tristate drivers
    // . no weak drivers
    // . cur must be combinational (allow comb->comb and comb->seq merges)
    //   also, do not consider cycle nodes
    // . cur must have 1 fanout both elaboratedly and unelaboratedly
    NUUseDefNode *cur_ud_node = cur->getUseDefNode();
    NUUseDefNode *fanout_ud_node = fanout->getUseDefNode();
    if (cur_ud_node->isInitial() or fanout_ud_node->isInitial()) {
      continue;
    }
    if ((cur_ud_node->getType() == eNUContEnabledDriver) or
        (fanout_ud_node->getType() == eNUContEnabledDriver)) {
      continue;
    }
    if ((cur_ud_node->getStrength() != eStrDrive) or
        (fanout_ud_node->getStrength() != eStrDrive)) {
      continue;
    }
    if (cur_ud_node->isSequential() or cur_ud_node->isCycle()) {
      continue;
    }
    if (fanout_info.numFanouts(cur_ud_node) != 1) {
      continue;
    }
    if (fanout_info.isDisqualified(cur_ud_node)) {
      continue;
    }

    // Only allow parent/child hierarchical relationships for merging.
    STBranchNode *cur_hier = cur->getHier();
    STBranchNode *fanout_hier = fanout->getHier();
    if ((cur_hier->getParent() != fanout_hier) and
        (fanout_hier->getParent() != cur_hier)) {
      continue;
    }

    // Can merge.
    NU_ASSERT2(((mUDMergeMap.find(cur_ud_node) == mUDMergeMap.end()) or
                (mUDMergeMap[cur_ud_node] == fanout_ud_node)), cur_ud_node, fanout_ud_node);
    mUDMergeMap[cur_ud_node] = fanout_ud_node;
  }

  // Now print out statistics.
  UtIO::cout() << "XHIER  Cross Hierarchy Merge Statistics:" << UtIO::endl;
  UtIO::cout() << "XHIER    Number of mergeable pairs = " << mUDMergeMap.size() << UtIO::endl;
  for (UDMap::iterator iter = mUDMergeMap.begin();
       iter != mUDMergeMap.end();
       ++iter) {
    NUUseDefNode *node = (*iter).first;
    NUUseDefNode *fanout = (*iter).second;
    UtString buf;
    buf << "XHIER    ";
    SourceLocator loc = node->getLoc();
    loc.compose(&buf);
    buf << " ";
    buf << node->getName()->str();
    buf << "  --into-->  ";
    loc = fanout->getLoc();
    loc.compose(&buf);
    buf << " ";
    buf << fanout->getName()->str();
    UtIO::cout() << buf;
    UtIO::cout() << UtIO::endl;
  }
}
