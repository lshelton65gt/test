// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "LocalPropagation.h"
#include "reduce/LocalCopyProp.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"

#include "localflow/UD.h"

#include "reduce/Fold.h"


class LocalCopyPropagationCallback : public LocalPropagationCallback
{
public:
  //! Constructor.
  LocalCopyPropagationCallback(IODBNucleus* iodb, Fold* fold, 
                               LocalPropagationStatistics * statistics) :
    LocalPropagationCallback(iodb,fold,statistics,true)
  {}

  //! Destructor.
  ~LocalCopyPropagationCallback() {}

protected:
  //! Analyze an assignment and enroll if applicable.
  void enrollAssign(NUAssign * assign);

  //! Copy propagation does nothing special for if selectors. Nop.
  void enterIfBranch(NUIf * /*ifStmt*/, bool /*selector*/) {}

private:
  //! Check that it is valid to propagate from an RHS expression to a LHS net.
  bool checkExpression(const NUNet * lnet, const NUExpr * rvalue, bool net_checks, bool * needs_extension) const;

  //! Check that it is valid to propagate between two nets.
  bool checkNets(const NUNet * a, const NUNet * b) const;
};


RELocalCopyProp::RELocalCopyProp(IODBNucleus* iodbNucleus, 
                                 UD* ud,
                                 Fold* fold, 
                                 LocalPropagationStatistics * statistics,
                                 bool verbose) :
  mVerbose(verbose), 
  mIODBNucleus(iodbNucleus), 
  mUD(ud), 
  mFold(fold),
  mStatistics(statistics)
{}


RELocalCopyProp::~RELocalCopyProp() {}


void RELocalCopyProp::module(NUModule* module)
{
  // Create a design walker to walk the always blocks and optimize
  // them.
  LocalCopyPropagationCallback callback(mIODBNucleus, mFold, mStatistics);
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);

  bool module_changed = false;

  NUUseDefVector optimizedBlocks;

  // Walk the tasks and process them with an empty context. No
  // constants can flow into tasks.
  for (NUTaskLoop l = module->loopTasks(); !l.atEnd(); ++l) {
    NUTask* task = *l;
    walker.task(task);
    if (callback.getChanged()) {
      module_changed = true;
      optimizedBlocks.push_back(task);
    }
  }

  // Get an ordering of UseDef nodes which allows cascading propagation.
  NUUseDefVector depth_order;
  LocalPropagationBuilder::getDepthOrder(module,&depth_order);

  callback.pushContext();       // A new context for the module.

  for (NUUseDefVector::iterator i=depth_order.begin(); i!=depth_order.end(); ++i) {
    NUUseDefNode * node = *i;
    switch(node->getType()) {
    case eNUContAssign: 
    {
      NUContAssign * assign = dynamic_cast<NUContAssign*>(node);
      walker.contAssign(assign);
      if (callback.getChanged()) {
        module_changed = true;
        optimizedBlocks.push_back(assign);
      }
      break;
    }
    case eNUAlwaysBlock:
    {
      NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(node);
      // Propagate the expressions in the always block. We pre clear
      // the changed flag from the previous always block.
      walker.alwaysBlock(always);

      // If it was changed, save the always block for folding
      if (callback.getChanged()) {
        module_changed = true;
        optimizedBlocks.push_back(always);
      }

      // TBD: Propagate out of always blocks (not sequential!)
    }
    default:
      // only handle continuous assigns and always blocks.
      break;
    }
  }

  callback.popContext();        // All done with the module context.

  if (module_changed) {
    mStatistics->module();
  }

  // Sort the modified blocks so we get consistent folding and fold
  // them
  std::sort(optimizedBlocks.begin(), optimizedBlocks.end(), NUUseDefNodeCmp());
  typedef Loop<NUUseDefVector> UseDefLoop;
  for (UseDefLoop l(optimizedBlocks); !l.atEnd(); ++l) {
    NUUseDefNode * node = *l;
    switch(node->getType()) {
    case eNUContAssign: {
      NUContAssign* assign = dynamic_cast<NUContAssign*>(node);
      mFold->stmt(assign);
      break;
    }
    case eNUAlwaysBlock: {
      NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(node);
      mFold->structuredProc(always);
      break;
    }
    case eNUTask: {
      NUTask* task = dynamic_cast<NUTask*>(node);
      mFold->tf(task);
      break;
    }
    default: {
      NU_ASSERT(0,node);
      break;
    }
    }
  }

  // If a UD pointer was provided, then recalculate it now
  if (mUD != NULL) {
    mUD->module(module);
  }

  // If we are being verbose, print the optimized blocks
  if (mVerbose && !optimizedBlocks.empty()) {
    // Sort the blocks so we get canonical output

    // Print the verilog for all optimized blocks
    UtIO::cout() << "\nOptimized nodes for module "
                 << module->getName()->str() << ":\n\n";
    for (UseDefLoop l(optimizedBlocks); !l.atEnd(); ++l) {
      NUUseDefNode* useDef = *l;
      useDef->printVerilog(true, 0, true);
    }
  }
} // void RELocalCopyProp::module


void RELocalCopyProp::alwaysBlock(NUAlwaysBlock * always)
{
  // Create a design walker to walk the always blocks and optimize
  // them.
  LocalCopyPropagationCallback callback(mIODBNucleus, mFold, mStatistics);
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);

  // There is no inheirited context; only propagate internal to the block.
  callback.pushContext();       // A new context for the always block.
  walker.alwaysBlock(always);
  callback.popContext();        // Remove the context.
  bool changed = callback.getChanged();

  // Assigns have been propagated, fold the block again
  mFold->structuredProc(always);

  // If a UD pointer was provided, then recalculate it now
  if (mUD != NULL) {
    mUD->structuredProc(always);
  }

  if (mVerbose and changed) {
    // Print the verilog for all optimized blocks
    UtIO::cout() << "\nOptimized block in module "
                 << always->getBlock()->getModule()->getName()->str() << ":\n\n";
    always->printVerilog(true, 0, true);
  }
}


void LocalCopyPropagationCallback::enrollAssign(NUAssign * assign)
{
  // Add this assign to our constant map whether it is a constant or not
  NULvalue * lvalue = assign->getLvalue();
  NUExpr   * rvalue = assign->getRvalue();

  NUNet * lnet = NULL;

  bool valid = false;
  if (lvalue->isWholeIdentifier()) {
    lnet = lvalue->getWholeIdentifier();
    // Really, this should check protected-mutable, but clock-collapse
    // directives break (see test/clock-collapse/bug1635)
    valid = not lnet->isProtected(getIODB());
    if (valid) {
      // Cases like Bug3703 contain memsels which are flagged as full
      // memory references. We don't know how to rewrite this sort of
      // construct.
      valid = (lvalue->getType()==eNUIdentLvalue);
    }
    if (valid) {
      // Don't propagate from continuous assigns which define
      // multiply-driven nets. This does not matter within always
      // blocks because the local definition overrides any external
      // driver.
      valid = not (lnet->isMultiplyDriven() and assign->getType()==eNUContAssign);
    }
    if (valid) {
      // For now, disable copy propagation of memory assigns. They are
      // not zero-cost. In the case of temporaries, we would also need
      // to worry about updating masters.
      valid = (not lnet->is2DAnything());
    }
    // check signing of LHS net compared to expression.
    // TBD: Can we use this to say that extension is needed?
    if (valid) {
      valid = (lnet->isSigned() == rvalue->isSignedResult());
    }
    if (valid) {
      // Don't move overlapping uses past this def, as it represents a
      // different value than the one coming out of this def.
      NUNetSet uses;
      rvalue->getUses(&uses);
      valid = (uses.find(lnet)==uses.end());
    }
    if (valid) {
      // Check that size matches. We check against the RHS determined bit
      // size because we do not want to perform a replacement where we can
      // become sized differently than the LHS identifier.
      valid = (lnet->getBitSize() == rvalue->determineBitSize());
    }
    if (valid) {
      // So far, so good. Check the potential cost increase of
      // propagating this expression.
      bool needs_extension = false;
      valid = checkExpression(lnet,rvalue,true,&needs_extension);

      if (valid) {
        if (needs_extension) {
          CopyContext cc(NULL,NULL);
          // If this expression needs to be protected from extending,
          // wrap it with an extend operator before storing in our
          // cache.
          NUExpr * stored_expr = new NUBinaryOp ( lnet->isSigned() ? NUOp::eBiVhExt : NUOp::eBiVhZxt,
                                                  rvalue->copy(cc),
                                                  NUConst::create(false, rvalue->getBitSize(), 32, rvalue->getLoc()),
                                                  rvalue->getLoc() );
          stored_expr->resize(rvalue->getBitSize());
          addNet(lnet, stored_expr);
          delete stored_expr;
        } else {
          addNet(lnet, rvalue);
        }
      }
    }
  }
}


bool LocalCopyPropagationCallback::checkExpression(const NUNet * lnet, const NUExpr * rvalue, bool net_checks, bool * needs_extension) const
{
  bool valid = false;

  switch (rvalue->getType()) {
    // Blind propagation of memory selects appears to cause a 5%
    // runtime slowdown in Mindspeed/m27480.
    //case NUExpr::eNUMemselRvalue:
  case NUExpr::eNUVarselRvalue: {
    // Not a substantial cost increase. Allow through if the
    // identifier is full and the expression is constant.

    // The backend may decide to CSE these references to reduce the
    // number of selects.
    const NUVarselRvalue * rvarsel = dynamic_cast<const NUVarselRvalue*>(rvalue);
    valid = rvarsel->isSimpleConstantVarsel();
    break;
  }
  case NUExpr::eNUIdentRvalue: {
    // not a substantial cost increase. allow through.
    if (net_checks) {
      const NUNet * rnet = rvalue->getWholeIdentifier();
      valid = checkNets(lnet,rnet);
    } else {
      valid = true;
    }
    break;
  }
  case NUExpr::eNUUnaryOp: {
    const NUUnaryOp * unary = dynamic_cast<const NUUnaryOp*>(rvalue);
    switch (unary->getOp()) {
    case NUOp::eUnLogNot: {
      if (lnet->getBitSize()==1) {
        bool dummy;
        valid = checkExpression(lnet,unary->getArg(0),net_checks,&dummy);
        (*needs_extension) = false;
      }
      break;
    }
    case NUOp::eUnMinus:
    case NUOp::eUnBitNeg: {
      bool dummy = false;
      valid = checkExpression(lnet,unary->getArg(0),net_checks,&dummy);
      // Force explicit sizing before this expression is replaced.
      (*needs_extension) = true;
      break;
    }
    default: {
      valid = false;
      break;
    }
    } // switch
    break;
  } // case NUExpr::eNUUnaryOp:

  case NUExpr::eNUBinaryOp: {
    const NUBinaryOp * binary = dynamic_cast<const NUBinaryOp*>(rvalue);
    switch (binary->getOp()) {
    case NUOp::eBiVhZxt: 
    case NUOp::eBiVhExt: {
      // The first argument is the expression being extended. The
      // second is its size. When we are extending, we are generally
      // converting between nets of different sizes. Checking net
      // dimension is unnecessary.
      bool dummy = false;
      valid = checkExpression(lnet,binary->getArg(0),false,&dummy);
      (*needs_extension) = false;
      break;
    }
    default: {
      valid = false;
      break;
    }
    }    
    break;
  } // case NUExpr::eNUBinaryOp:

  case NUExpr::eNUConcatOp:
  {
    // Added RHS concats as valid copy propagation expressions. This
    // is to get back some performance lost on test/assign/vector-04.v
    // due to new sizing. New sizing turns off concat rewrite on
    // concats with constants if the concat is less than 32-bits.
    //
    // This change isn't necessarily the best way to solve this
    // problem. The issue is that local copy prop is making decisions
    // on what to propagate only by looking at the expression to
    // propagate. But really, it would be better if the decision was
    // made based on how the expression was used. So far example if we
    // have:
    //
    //    block
    //      a = { 4'hc, trap, 27'h0 };
    //      word[0] = a[0];
    //      word[1] = a[1];
    //      ...
    //    end
    //
    // Then the bit select of any complex concat will usually result
    // in something much simpler. So it makes sense to copy propagate
    // no matter how complex the concat.
    //
    // For now the following code allows the vector mux case to be
    // implemented more efficiently.
    const NUConcatOp* concat = dynamic_cast<const NUConcatOp*>(rvalue);
    if (concat->getRepeatCount() > 1) {
      // Not sure about repeat counts yet
      valid = false;
    } else {
      int numIdents = 0;
      valid = true;
      for (UInt32 i = 0; i < concat->getNumArgs() && valid; ++i) {
        const NUExpr* subExpr = concat->getArg(i);
        if (!subExpr->castConst()) {
          // Only allow identifiers or idents. We don't want concats
          // of concats yet
          bool dummy;
          if ((subExpr->isWholeIdentifier() ||
               (subExpr->getType() == NUExpr::eNUVarselRvalue)) &&
              checkExpression(lnet, subExpr, false, &dummy)) {
            ++numIdents;
          } else {
            // Give up
            valid = false;
          }
        }
      }
      valid &= (numIdents < 2);
    }
    break;
  } // case NUExpr::eNUConcatOp:

  default: {
    valid = false;
    break;
  }
  } // switch

  return valid;
} // bool LocalCopyPropagationCallback::checkExpression


bool LocalCopyPropagationCallback::checkNets(const NUNet * a, const NUNet * b) const
{
  if ((a->isBitNet() or a->isVectorNet()) and
      (b->isBitNet() or b->isVectorNet())) {
    return (a->getBitSize() == b->getBitSize());
  } else if (a->isMemoryNet() and b->isMemoryNet()) {
    const NUMemoryNet * aMem = dynamic_cast<const NUMemoryNet*>(a);
    const NUMemoryNet * bMem = dynamic_cast<const NUMemoryNet*>(b);
    const ConstantRange * aWidth = aMem->getWidthRange();
    const ConstantRange * bWidth = bMem->getWidthRange();
    const ConstantRange * aDepth = aMem->getDepthRange();
    const ConstantRange * bDepth = bMem->getDepthRange();
    return ((*aWidth)==(*bWidth) and (*aDepth)==(*bDepth));
  } else {
    return false;
  }
}
