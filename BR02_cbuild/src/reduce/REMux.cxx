// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/REMux.h"

#include "nucleus/NUModule.h"

#include "bdd/BDD.h"
#include "localflow/UD.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"

#include "nucleus/NUNetSet.h"

#include "util/SetOps.h"
#include "util/UtString.h"
#include "util/UtString.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"

REMux::REMux(NUNetRefFactory *netref_factory,
	     MsgContext* msg_context,
	     AtomicCache* str_cache,
	     IODBNucleus* iodb,
             ArgProc* args,
	     UInt32 min_bit_size,
	     UInt32 min_term_count,
	     bool processTernaryOps,
	     bool updateUD,
	     bool verbose):
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mStrCache(str_cache),
  mIODB(iodb),
  mArgs(args),
  mMinBitSize(min_bit_size),
  mMinTermCount(min_term_count),
  mProcessTernaryOps(processTernaryOps),
  mUpdateUD(updateUD),
  mVerbose(verbose)
{
  mBDDContext = new BDDContext;
}


REMux::~REMux()
{
  delete mBDDContext;
}


void REMux::module(NUModule* mod)
{
  bool changed = false;

  NU_ASSERT(mTemporaryExprs.empty(), mod);

  // Loop over all the continuous assigns, replacing the ones that look
  // like MUXes with a list of if statements.  We'll need to transform
  // the continuous assign into a combinational always block, so we'll
  // have to mutate the continuous assigns.
  NUContAssignList keep;
  NUContAssignList kill;
  NUStmtList stmts;
  for (NUModule::ContAssignLoop loop = mod->loopContAssigns(); 
       not loop.atEnd();
       ++loop) {
    NUContAssign* assign = (*loop);
    stmts.clear();

    bool success = xformMuxAssign(assign, stmts);
    if (success) {
      kill.push_back(assign);
      const SourceLocator& loc = assign->getLoc();
      NUBlock* block = new NUBlock(&stmts, mod, 
                                   mIODB, mNetRefFactory, 
                                   false, loc);

      StringAtom * block_name = mod->newBlockName(mStrCache, loc);
      NUAlwaysBlock* ab = new NUAlwaysBlock(block_name, block, mNetRefFactory, loc, false);
      mod->addAlwaysBlock(ab);

      changed = true;
    } else {
      keep.push_back(assign);
    }
  }
  if (not kill.empty()) {
    mod->replaceContAssigns(keep);
    for (NUContAssignList::iterator iter = kill.begin(); 
	 iter != kill.end(); 
	 ++iter) {
      delete (*iter);
    }
  }

  // Now loop over all the combinational always blocks, and mutate any
  // statements that are mux assigns.
  for (NUModule::AlwaysBlockLoop loop = mod->loopAlwaysBlocks();
       not loop.atEnd(); 
       ++loop) {
    NUAlwaysBlock * always = (*loop);
    changed |= reduce(always->getBlock());
  }

  // Do the initial blocks also
  for (NUModule::InitialBlockLoop loop = mod->loopInitialBlocks ();
       not loop.atEnd(); 
       ++loop) {
    NUInitialBlock * initial = (*loop);
    changed |= reduce(initial->getBlock());
  }
	 
  // Same for tasks
  for(NUTaskLoop loop = mod->loopTasks ();
      not loop.atEnd(); 
      ++loop) {
    changed |= reduce(*loop);
  }

  if (changed and mUpdateUD) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.module(mod);
  }

  deleteExprList(mTemporaryExprs);
} // void REMux::reduceModule


bool REMux::reduce(NUTF * tf)
{
  NUStmtList replacements;
  NUStmtList kills;
  bool changed = reduce(tf->loopStmts(),
			replacements,
			kills);
  if (not kills.empty()) {
    tf->replaceStmtList(replacements);
    deleteStmtList(kills);
  }
  return changed;
}


bool REMux::reduce(NUBlock * block)
{
  NUStmtList replacements;
  NUStmtList kills;
  bool changed = reduce(block->loopStmts(),
			replacements,
			kills);
  if (not kills.empty()) {
    block->replaceStmtList(replacements);
    deleteStmtList(kills);
  }
  return changed;
}


bool REMux::reduce(NUIf * stmt)
{
  NUStmtList replacements;
  NUStmtList kills;
  bool changed = false;

  changed |= reduce(stmt->loopThen(),
		    replacements,
		    kills);
  if (not kills.empty()) {
    stmt->replaceThen(replacements);
    deleteStmtList(kills);
  }

  replacements.clear();
  kills.clear();

  changed |= reduce(stmt->loopElse(),
		    replacements,
		    kills);
  if (not kills.empty()) {
    stmt->replaceElse(replacements);
    deleteStmtList(kills);
  }

  return changed;
}


bool REMux::reduce(NUCase * stmt)
{
  bool changed = false;
  for (NUCase::ItemLoop loop = stmt->loopItems(); 
       not loop.atEnd() ;
       ++loop) {
    NUCaseItem *item = (*loop);

    NUStmtList replacements;
    NUStmtList kills;
    changed |= reduce(item->loopStmts(),
		      replacements,
		      kills);
    if (not kills.empty()) {
      item->replaceStmts(replacements);
      deleteStmtList(kills);
    }
  }
  return changed;
}


bool REMux::reduce(NUFor * stmt)
{
  NUStmtList replacements;
  NUStmtList kills;
  bool changed = reduce(stmt->loopBody(),
			replacements,
			kills);
  if (not kills.empty()) {
    stmt->replaceBody(replacements);
    deleteStmtList(kills);
  }
  return changed;
}


bool REMux::reduce(NUStmt * stmt, 
		   NUStmtList & replacements,
		   NUStmtList & kills)
{
  const NUBlockingAssign* assign = dynamic_cast<const NUBlockingAssign*>(stmt);
  if (assign) {
    bool i_changed = xformMuxAssign(assign, replacements);
    if (i_changed) {
      kills.push_back(stmt);
    } else {
      replacements.push_back(stmt);
    }
    return i_changed;
  }

  replacements.push_back(stmt);

  NUIf * ifstmt = dynamic_cast<NUIf*>(stmt);
  if (ifstmt) {
    return reduce(ifstmt);
  }

  NUCase * casestmt = dynamic_cast<NUCase*>(stmt);
  if (casestmt) {
    return reduce(casestmt);
  }

  NUFor * forstmt = dynamic_cast<NUFor*>(stmt);
  if (forstmt) {
    return reduce(forstmt);
  }

  NUBlock * block = dynamic_cast<NUBlock*>(stmt);
  if (block) {
    return reduce(block);
  }

  return false;
}

bool REMux::reduce(NUStmtLoop loop, 
		   NUStmtList & replacements,
		   NUStmtList & kills)
{
  bool changed = false;

  for ( /*no initial*/ ;
       not loop.atEnd(); 
       ++loop) {
    NUStmt* stmt = (*loop);

    changed |= reduce(stmt, replacements, kills);
  }

  return changed;
}


// Transform
//    assign var = ({N{e1}} & a1) | ({N{e2}} & a2) | ({N{e3}} & a3) ...
// to
//    var = 0;   
//    if (e1) var |= a1;
//    if (e2) var |= a2;
//    if (e3) var |= a3;
//
// if we can prove (using BDDs that e1, e2, e3 are mutually exclusive,
// then we can avoid the |= and just use simple assignment and write
// an if-then-else structure without an initialization (but with the final
// else being an assignment of zero
//
// If we can prove that the expressions cover ALL the possible cases,
// (and they're of the form (x == k), we can generate a switch statement)

NUExpr* REMux::isTeCond (NUExpr* enaExpr)
{
  NUExpr* enable = NULL;

  if (NUTernaryOp* condExp = dynamic_cast<NUTernaryOp*>(enaExpr)) {
    // Check for ((enab ? -1:0) & val) as the mux leg
    NUConst* thenClause;
    NUConst* elseClause;
    if ((condExp != NULL) &&
	(condExp->getOp() == NUOp::eTeCond) &&
	((thenClause = condExp->getArg(1)->castConst()) != NULL) &&
	((elseClause = condExp->getArg(2)->castConst()) != NULL)) {
      if (thenClause->isOnes () && elseClause->isZero ()) {
	enable = condExp->getArg(0);
      } else if (thenClause->isZero () && elseClause->isOnes ()) {
	// check for swapped ops and return complemented enable!
	enable = generateNot(condExp->getArg(0));
      }
    }
  }
  return enable;
}


// Test to see if an expression is a muliply concat,
// and that the width matches what's passed in.  Actually
// that width can be greater, but it can't be smaller, or
// unless we mask off the expression, which we could do,
// if that seemed to benefit us.
NUExpr* REMux::isMultiplyConcat(NUExpr* enaExpr, NUExpr* expr)
{
  NUExpr* enable = NULL;
  
  if (NUConcatOp* op = dynamic_cast<NUConcatOp*>(enaExpr))
    {
      UInt32 width = expr->getBitSize();

      // {width{enable}} -- extract enable
      if ((op->getRepeatCount() >= width) && (op->getNumArgs() == 1))
	enable = op->getArg(0);
      else if (op->getNumArgs () == 1)
	enable = isTeCond (op->getArg (0));
    }
  else
    // Check for ((enab ? -1:0) & val) as the mux leg
    enable = isTeCond (enaExpr);

  return enable;
} // NUExpr* isMultiplyConcat

// examine a term from an OR expression, which might be
// a nested OR expression, or it might be an AND between
// a multiply-concatenated expression and another expression.
// Anything else is not going to pass.
bool REMux::checkTerm(NUExpr* exp, TermVector* terms)
{
  bool ret = false;
  NUBinaryOp  * binary_op  = dynamic_cast<NUBinaryOp*>(exp);
  NUTernaryOp * ternary_op = dynamic_cast<NUTernaryOp*>(exp);
  if (binary_op) {
    NUExpr* x = binary_op->getArg(0);
    NUExpr* y = binary_op->getArg(1);
    NUExpr* enable;
    switch (binary_op->getOp()) {
    case NUOp::eBiBitOr:  // (a&b | (c&d | e&f))
      ret = (checkTerm(x, terms) && checkTerm(y, terms));
      break;
    case NUOp::eBiBitAnd: // {127{ena}} & v[126:0]
      			  // (ena?{127{1'b1}}:0) & v[126:0]
      if ((enable = isMultiplyConcat(x, y)) != NULL) {
	terms->push_back(Term(enable, y));
	ret = true;
      } else if ((enable = isMultiplyConcat(y, x)) != NULL) {
	terms->push_back(Term(enable, x));
	ret = true;
      }
      break;
    case NUOp::eBiUMult:         // a[i] * XVEC => (a[i] ? XVEC : 0)
      if (x->effectiveBitSize () == 1)
        terms->push_back (Term (x, y));
      else if (y->effectiveBitSize () == 1)
        terms->push_back (Term (y, x));
      else
        break;
      ret = true;
      break;

    default:
      break;
    }
  } else if (mProcessTernaryOps and ternary_op) {
    // matching: ena?(a&b):{128{1'b0}}
    NUExpr * enable = ternary_op->getArg(0);
    NUExpr * x      = ternary_op->getArg(1);
    NUExpr * y      = ternary_op->getArg(2);

    NUConst * x_const = x->castConst();
    NUConst * y_const = y->castConst();
    
    if (x_const and x_const->isZero()) {
      // this is the then. need to invert the enable.
      NUExpr * not_enable = generateNot(enable);
      terms->push_back(Term(not_enable, y));
      ret = true;
    } else if (y_const and y_const->isZero()) {
      terms->push_back(Term(enable, x));
      ret = true;
    }
  }
  return ret;
} // bool checkTerm
        
#ifdef CDB
static const char* sCheckFile = NULL;
static UInt32 sCheckLine = 0;
#endif

// Are the enables for each term mutually exclusive, e.g.
// for all i!=j, (E(i)&E(j)) == 0.
//
bool REMux::isExclusive(const TermVector& terms)
{
  if (terms.size () == 1)
    return true;                // degenerately true for singleton

  for(TermVector::const_iterator t = terms.begin ();
      t != terms.end ();)
  {
    const NUExpr *e = t->first;
    
    BDD p1 = e->bdd (mBDDContext, 0);
    if (not p1.isValid ())
      return false;
    
    for(TermVector::const_iterator r = ++t; r != terms.end (); ++r)
    {
      const NUExpr *e = r->first;
      BDD p2 = e->bdd (mBDDContext, 0);
      if (not p1.isValid ())
        return false;
      
      BDD bdd = mBDDContext->opAnd (p1, p2);
      
      if (not bdd.isValid () || bdd != mBDDContext->val0 ())
        return false;
    }
  }
  return true;
}

// Do the selectors cover ALL possible cases?
bool REMux::isComplete(const TermVector& terms)
{
  BDD allTerms = mBDDContext->val0 ();
  
  for(TermVector::const_iterator t = terms.begin ();
      t != terms.end (); ++t)
  {
    const NUExpr *e = t->first;
    
    BDD p1 = e->bdd (mBDDContext, 0);
    if (not p1.isValid ())
      return false;
    
    allTerms = mBDDContext->opOr (allTerms, p1);
    
    if (not allTerms.isValid ())
      return false;
  }
  
  // All terms are covered if we evaluate to 1
  return allTerms == mBDDContext->val1 ();
} // bool REMux::isComplete

bool REMux::xformMuxAssign(const NUAssign* old, NUStmtList & replacements)
{
  bool ret = false;
  TermVector terms;
  const SourceLocator& loc = old->getLoc();
  bool usesCFNet = old->getUsesCFNet();

#ifdef CDB
  if ((sCheckFile != NULL) && (strcmp(loc.getFile(), sCheckFile) == 0) &&
      (loc.getLine() == sCheckLine))
  {
    UtIO::cout() << "checking " << sCheckFile << ", line " << sCheckLine << UtIO::endl;
  }
#endif

  bool def_use_overlap = hasDefUseOverlap(old);
  if (def_use_overlap) {
    return false;
  }

  NUExpr* rvalue = old->getRvalue();
  if (rvalue->drivesZ()) {
    return false;
  }

  if (checkTerm(rvalue, &terms))
  {
    if ( (rvalue->getBitSize() <= mMinBitSize) and
	 (terms.size()         <= mMinTermCount) ) {
      return false;
    }

    bool exclusive = isExclusive(terms);
    bool complete = isComplete(terms);

    NULvalue* lvalue = old->getLvalue();
    UInt32 width = lvalue->determineBitSize();
    NUExpr* lvalueExpr = lvalue->NURvalue();
    NU_ASSERT (lvalueExpr, lvalue);

    lvalueExpr->resize(width);

    if (mVerbose) {
      UtString loc_str;
      loc.compose(&loc_str);

      UtIO::cout() << loc_str.c_str() << ": " 
		   << width << "-bit " 
		   << terms.size() << "-term MUX converted to if statements." 
		   << UtIO::endl;
    }

    ret = true;

    CopyContext copy_context(NULL,NULL);
    NUStmtList thenList, elseList;

    if (!complete || !exclusive)
    {
      NUConst* zero = NUConst::create(rvalue->isSignedResult (), 0,
                                      lvalue->getBitSize(), loc);
      // Initialize the output to 0 unless it's exclusive...
      NUBlockingAssign* initAssign = new NUBlockingAssign(lvalue->copy(copy_context), zero, usesCFNet, loc);
      if (!exclusive)
        replacements.push_back(initAssign);
      else
        elseList.push_back (initAssign);
    }

    // Based on some experimental evidence in "utiltest miscperf", we
    // have confirmed that ?: is superior to if/then/else under
    // some circumstances, and not under others.  Here is what we've learned,
    // by timing about 4 billion iterations of each.
    //
    //     Linux ?:   Linux if/then/else    Solaris ?:     Solaris if/then/else
    //      
    //  8  
    // 16
    // 32
    // 64  2842       2783                  8933           8317
    //
    // 

    // Make a sequence of assignment statements to assign each term
    // if the enable is active
    NUIf* ifStmt = 0;

    // Walk in reverse order to get user written order of mux in
    // hopes that most frequent cases written first.
    for (TermVector::reverse_iterator i = terms.rbegin (); i != terms.rend (); ++i)
    {
      NUExpr* enable = i->first;
      NUExpr* value;

      if (exclusive)
	value = i->second->copy(copy_context);
      else
	value = new NUBinaryOp (NUOp::eBiBitOr,
				lvalueExpr->copy(copy_context),
				i->second->copy (copy_context), loc);

      value = new NUVarselRvalue (value, ConstantRange (width-1, 0), loc);
      value->resize (width);
      value->setSignedResult (rvalue->isSignedResult ());

      NUBlockingAssign *assign = new NUBlockingAssign(lvalue->copy(copy_context), value, usesCFNet, loc);

      if (complete && exclusive && (i == terms.rbegin ()) &&
	  (terms.size() > 1))
      {
	/*
	 * Make the last conditional into an unconditional because preceding
	 * tests excluded all other possibilities.
	 */
	elseList.clear ();
	elseList.push_back (assign);
      }
      else
      {
	// Just build if-then; or if-then-else sequences
	thenList.push_back(assign);

        // Force the condition into a one-bit expression by comparing to zero.
        NUExpr * new_enable = new NUBinaryOp(NUOp::eBiNeq,
                                             enable->copy(copy_context),
                                             NUConst::create(false,0,1,loc),
                                             loc);
        new_enable->resize(1);
	ifStmt = new NUIf(new_enable, thenList, elseList, mNetRefFactory, usesCFNet, loc);

	thenList.clear();
	elseList.clear();

	if (exclusive)
	  elseList.push_back (ifStmt);
	else
	  replacements.push_back(ifStmt);
      }
    }

    if (exclusive && ifStmt)
      replacements.push_back (ifStmt);

    delete lvalueExpr;
  }
  return ret;
}


bool REMux::hasDefUseOverlap(const NUAssign * assign)
{
  NUNetSet defs;
  NUNetSet uses;
  getDefs(assign,&defs);
  getUses(assign,&uses);

  return set_has_intersection(defs,uses);
}


void REMux::getUses(const NUAssign* assign, NUNetSet* uses)
{
  if (assign->useBlockingMethods())
    assign->getBlockingUses(uses);
  else
    assign->getUses(uses);
}


void REMux::getDefs(const NUAssign* assign, NUNetSet* defs)
{
  if (assign->useBlockingMethods())
    assign->getBlockingDefs(defs);
  else
    assign->getDefs(defs);
}


void REMux::deleteExprList(NUExprList & exprs)
{
  for (NUExprList::iterator iter = exprs.begin(); 
       iter != exprs.end(); 
       ++iter) {
    delete (*iter);
  }
  exprs.clear();
}


NUExpr * REMux::generateNot(NUExpr * enable)
{
  CopyContext copy_context(NULL,NULL);
  NUExpr * not_enable = new NUUnaryOp (NUOp::eUnLogNot,
				       enable->copy(copy_context), 
				       enable->getLoc());
  not_enable->resize(1);

  // remember that we have to delete this.
  mTemporaryExprs.push_back(not_enable);

  return not_enable;
}
