// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/Inference.h"
#include "reduce/REUtil.h"
#include "reduce/Fold.h"
#include "reduce/OccurrenceLogger.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUNetRef.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"
#include "reduce/CommonConcat.h"

#include "vectorise/Collapser.h"

Inference::Inference(Fold* fold_ctx,
		     NUNetRefFactory * net_ref_factory,
                     MsgContext *msg_context,
                     AtomicCache *str_cache,
                     IODBNucleus *iodb,
                     ArgProc *args,
                     Params params,
		     InferenceStatistics *stats) :
  mFoldContext (fold_ctx),
  mNetRefFactory(net_ref_factory),
  mMsgContext (msg_context),
  mStrCache (str_cache),
  mIODB(iodb),
  mArgs (args),
  mStatistics(stats),
  mVerbose(params.isVerboseInference ()),  
  mVerboseCollapse(params.isVerboseCollapse ()),
  mVerboseVectorMetrics(params.mOptions & eVerboseVectorMetrics),
  mParams (params)
{
}


Inference::~Inference()
{
  INFO_ASSERT(mScopes.empty(), "Stack consistency error.");
}


/*! 
  Discover Nucleus elements local to a single module which contain
  statement lists.

  Apply vectorization to each statement list, updating the containing
  element if necessary.
*/
class InferenceDiscoveryCallback : public NUDesignCallback
{
public:

  //! Constructor.
  /*!
    \param inference An Inference object, responsible for all
    vectorization.
   */
  InferenceDiscoveryCallback(Inference * inference) :
    NUDesignCallback(),
    mInference(inference),
    mCollapsed(false),
    mDoCommonConcat (inference->getParams ().doCommonConcat ()),
    mDoConcatNetElimination (inference->getParams ().doConcatNetElimination ()),
    mCommonConcat (NULL)
  {}

  //! Destructor.
  ~InferenceDiscoveryCallback() {}

  /*!
    NUBase handler: by default, do not walk through anything. 
    All statement-containing elements will be walked.
  */
  Status operator()(Phase /*phase*/, NUBase * /*obj*/) { return eSkip; }

  Status operator()(Phase, NUNamedDeclarationScope*) { return eNormal; }

  /*!
    NUModule handler: Locally process continuous assignments and allow
    the walk to continue.

    Scopes are pushed/popped into the Inference context as we
    enter/exit a module.
   */
  Status operator()(Phase phase, NUModule * module);

  /*!
    NUTF handler: Locally process the owned statement list and allow
    the walk to continue.

    The mCollapsed status variable is set to true if the local
    statement list was updated.

    Scopes are pushed/popped into the Inference context as we
    enter/exit a tf.
   */
  Status operator()(Phase phase, NUTF * tf);

  /*!
    NUStructuredProc handler: Allow the walk to continue; always and
    initial blocks cover statement-containing blocks.
   */
  Status operator()(Phase /*phase*/, NUStructuredProc * /*proc*/) { return eNormal; }

  /*!
    Generic NUStmt handler: Do not walk through generic statement types. 
   */
  Status operator()(Phase /*phase*/, NUStmt * /*stmt*/) { return eSkip; }
  
  /*!
    NUBlock handler: Locally process the owned statement list and
    allow the walk to continue.

    The mCollapsed status variable is set to true if the local
    statement list was updated.

    Scopes are pushed/popped into the Inference context as we
    enter/exit a block.
   */
  Status operator()(Phase phase, NUBlock * stmt);

  /*! 
    NUCase handler: Walk through to discover sub-elements.

    The NUCaseItem owns the statement lists within the NUCase; we do
    not locally process any statements in this callback.
  */
  Status operator()(Phase /*phase*/, NUCase * /*stmt*/) { return eNormal; }

  /*!
    NUCaseItem handler: Locally process the owned statement list and
    allow the walk to continue.

    The mCollapsed status variable is set to true if the local
    statement list was updated.
   */
  Status operator()(Phase phase, NUCaseItem * item);

  /*!
    NUIf handler: Locally process the owned statement lists for both
    'then' and 'else' clauses then allow the walk to continue.

    The mCollapsed status variable is set to true if the local
    statement list was updated.
   */
  Status operator()(Phase phase, NUIf * stmt);

  /*!
    NUFor handler: Locally process the owned statement list and
    allow the walk to continue.

    The mCollapsed status variable is set to true if the local
    statement list was updated.
   */
  Status operator()(Phase phase, NUFor * stmt);

  //! Did any updates occur?
  bool didCollapse() const { return mCollapsed; }

private:

  //! Inference object; responsible for collapsing a stmtlist.
  Inference * mInference;

  //! Did any updates occur?
  bool mCollapsed;

  bool mDoCommonConcat;
  bool mDoConcatNetElimination;
  CommonConcat *mCommonConcat;
};


NUDesignCallback::Status InferenceDiscoveryCallback::operator()(Phase phase, NUModule * module)
{
  if (phase == NUDesignCallback::ePre) {
    mInference->pushScope(module);
    
    // Locally identify vector operations over continuous assigns.

    // Other constructs contained within the module are discovered and
    // processed by the design walker.

    if (mDoCommonConcat) {
      mCommonConcat = new CommonConcat (module, mInference->getMsgContext (), mInference->getIODB (), 
                                        mInference->getParams (), NULL);
      mCommonConcat->winnowProtectedNets (module);
    }
    bool collapsed = mInference->collapseContAssigns(module, mCommonConcat);
    mCollapsed |= collapsed;
  } else {
    // Concat analysis testing
    if (mCommonConcat != NULL && mDoConcatNetElimination) {
      // look for any local net occurrence that can be replaced by bitsels from
      // common-concat temporaries
      mCollapsed |= mCommonConcat->eliminateLocalNets (module);
    }
    if (mCommonConcat != NULL && mInference->getParams () [Inference::eAlwaysWideCommonConcat]) {
      // do aggressive common concat processing for the always blocks in the module
      mCommonConcat->always (module);
    }
    if (mCommonConcat != NULL) {
      // apply the discovered right-side common concats
      mCollapsed |= mCommonConcat->rightSideReplace (module);
      // replace the defining occurrences of applied common concats with an
      // assignment to the concat temporary
      mCollapsed |= mCommonConcat->leftSideReplace (module);
      delete mCommonConcat;
      mCommonConcat = NULL;
    }
    mInference->popScope();
  }
  return eNormal;
}


NUDesignCallback::Status InferenceDiscoveryCallback::operator()(Phase phase, NUTF * tf)
{
  if (phase == NUDesignCallback::ePre) {
    mInference->pushScope(tf);

    NUStmtList * stmts = tf->getStmts();
    bool collapsed = mInference->collapseStmts( *stmts, mCommonConcat );
    if ( collapsed ) {
      tf->replaceStmtList(*stmts);
      mCollapsed = true;
    }
    delete stmts;

  } else {
    mInference->popScope();
  }
  return eNormal;
}


NUDesignCallback::Status InferenceDiscoveryCallback::operator()(Phase phase, NUBlock * block)
{
  if (phase == NUDesignCallback::ePre) {
    mInference->pushScope(block);

    NUStmtList * stmts = block->getStmts();
    bool collapsed = mInference->collapseStmts( *stmts, mCommonConcat );
    if ( collapsed ) {
      block->replaceStmtList(*stmts);
      mCollapsed = true;
    }
    delete stmts;

  } else {
    mInference->popScope();
  }
  return eNormal;
}


NUDesignCallback::Status InferenceDiscoveryCallback::operator()(Phase phase, NUCaseItem * case_item)
{
  if (phase == NUDesignCallback::ePre) {
    NUStmtList *stmts = case_item->getStmts();
    bool success = mInference->collapseStmts ( *stmts, mCommonConcat );
    if ( success ) {
      case_item->replaceStmts ( *stmts );
      mCollapsed = true;
    }
    delete stmts;
  }
  return eNormal;
}


NUDesignCallback::Status InferenceDiscoveryCallback::operator()(Phase phase, NUIf * if_stmt)
{
  if (phase == NUDesignCallback::ePre) {

    {
      NUStmtList * stmts = if_stmt->getThen();
      bool success = mInference->collapseStmts ( *stmts, mCommonConcat );
      if ( success ) {
        if_stmt->replaceThen( *stmts );
        mCollapsed = true;
      }
      delete stmts;
    }
    {
      NUStmtList *stmts = if_stmt->getElse();
      bool success = mInference->collapseStmts ( *stmts, mCommonConcat );
      if ( success ) {
        if_stmt->replaceElse( *stmts );
        mCollapsed = true;;
      }
      delete stmts;
    }

  }
  return eNormal;
}


NUDesignCallback::Status InferenceDiscoveryCallback::operator()(Phase phase, NUFor * for_stmt)
{
  if (phase == NUDesignCallback::ePre) {
    NUStmtList * stmts = for_stmt->getBody();
    bool success = mInference->collapseStmts ( *stmts, mCommonConcat );
    if ( success ) {
      for_stmt->replaceBody ( *stmts );
      mCollapsed = true;
    }
    delete stmts;
  }
  return eNormal;
}


bool Inference::module(NUModule * this_module)
{
  InferenceDiscoveryCallback callback(this);
  NUDesignWalker walker(callback,false);
  NUDesignCallback::Status status = walker.module(this_module);
  NU_ASSERT(status==NUDesignCallback::eNormal, this_module);

  bool collapsed = callback.didCollapse();

  // experimental module-wide common-concat
  if (mParams [eModuleWideCommonConcat]) {
    CommonConcat common_concat (this_module, getMsgContext (), getIODB (), getParams (), NULL);
    collapsed |= common_concat.module (this_module);
  }

  if (collapsed) {
    mStatistics->module ();
  }
  return collapsed;
}


bool Inference::always(NUAlwaysBlock * always) 
{
  NUModule * mod = always->getBlock()->getModule();

  // Manually push/pop the scope here to ensure that we have a valid
  // scope before we process any of the blocks.
  pushScope(mod);

  InferenceDiscoveryCallback callback(this);
  NUDesignWalker walker(callback,false);
  NUDesignCallback::Status status = walker.alwaysBlock(always);
  NU_ASSERT(status==NUDesignCallback::eNormal, always);
  bool collapsed = callback.didCollapse();

  popScope();

  return collapsed;
}


bool Inference::mergedBlocks(NUModule * mod,
			     NUUseDefVector & blocks)
{
  // Manually push/pop the scope here to ensure that we have a valid
  // scope before we process any of the blocks.
  pushScope(mod);

  // mergedBlocks is called from RETransform::optimizeMergedFlows
  // after Elaboration. Set the flag to disable case inferencing, or
  // NUNet::lookupElab will complain. 
  INFO_ASSERT(not mParams.doCaseCollapsing(), "Case inferencing consistency error.");

  InferenceDiscoveryCallback callback(this);
  NUDesignWalker walker(callback,false);
  for (NUUseDefVector::iterator iter = blocks.begin();
       iter != blocks.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(use_def);
    NU_ASSERT(always!=NULL, use_def);

    NUDesignCallback::Status status = walker.alwaysBlock(always);
    NU_ASSERT(status==NUDesignCallback::eNormal, always);
  }

  bool collapsed = callback.didCollapse();
  if (collapsed) {
    mStatistics->module();
  }

  popScope();

  return collapsed;
}


bool Inference::stmts(NUScope * scope,
		      NUStmtList & stmtlist,
                      bool recurse)
{
  // Manually push/pop the scope here to ensure that we have a valid
  // scope before we process any of the statements.
  pushScope(scope);

  bool collapsed = collapseStmts(stmtlist, NULL);

  if (recurse) {
    InferenceDiscoveryCallback callback(this);

    NUDesignWalker walker(callback,false);
    for (NUStmtList::iterator iter = stmtlist.begin();
         iter != stmtlist.end();
         ++iter) {
      NUStmt * stmt = (*iter);
      NUDesignCallback::Status status = walker.stmt(stmt);
      NU_ASSERT(status==NUDesignCallback::eNormal, stmt);
    }

    collapsed |= callback.didCollapse();
  }

  popScope();

  return collapsed;
}


bool Inference::collapseContAssigns(NUModule * this_module, CommonConcat *concats)
{
  NUContAssignList cont_assigns;
  this_module->getContAssigns(&cont_assigns);

  if (cont_assigns.empty ()) {
    return false;                       // uninteresting...
  }

  // copy cont. assigns into a vector so we can use a custom sort method.
  NUContAssignVector cont_vector;
  for (NUContAssignList::iterator it = cont_assigns.begin (); it != cont_assigns.end (); it++) {
    cont_vector.push_back (*it);
  }

  LessThanUseDef lt (mNetRefFactory);
  std::sort (cont_vector.begin (), cont_vector.end (), lt);

  // copy stmts into a stmtlist so it looks like all other stmtlists.
  NUStmtList cont_stmts;
  for (NUContAssignVector::iterator it = cont_vector.begin(); it != cont_vector.end (); it++) {
    cont_stmts.push_back (*it);
  }

  bool collapsed = false;

  collapsed |= collapseStmts ( cont_stmts, concats );

  if (concats == NULL) {
    // module wide common concat elimination is suppressed
  } else if (!concats->leftSideDiscover (cont_stmts)) {
    // did not find any left-side concats in the continuous assigns
  } else {
    // found left-side concats so apply common concat elimination to the
    // continuous assigns
    concats->rightSideDiscover (cont_stmts);
    // force writting back of the sorted statements into the module so that
    // if any concats are later used by application to the procedure
    // assignments or in net elimination, then leftSideReplace finds the
    // inducing statements in the correct order.
    collapsed = true;
    // local variable elimination is done on the entire module just prior to
    // deleting the module-wide common concat instance (in the post branch of
    // the InferenceDiscoveryCallback operator for NUModule).
  }

  if ( collapsed ) {
    // convert the cont_stmts back into cont_assigns.
    cont_assigns.clear();
    for ( NUStmtList::iterator iter = cont_stmts.begin() ;
	  iter != cont_stmts.end() ;
	  ++iter ) {
      NUStmt * stmt = (*iter);
      NUContAssign * assign = dynamic_cast<NUContAssign *>(stmt);
      NU_ASSERT(assign, stmt);
      cont_assigns.push_back(assign);
    }
    // replace the module assign list with the collapsed cont_assigns.
    this_module->replaceContAssigns(cont_assigns);
  }
  return collapsed;
}


bool Inference::collapseStmts (NUStmtList &stmts, CommonConcat *concats)
{
  bool retval = false;                  // nothing collapsed yet
  NUStmtList::iterator it = stmts.begin ();
  NUType type = eNUAssign;
  while (it != stmts.end ()) {
    NUStmt *stmt = *it;
    // collapse the case statement if appropriate.
    if (mParams.doCaseCollapsing()) {
      NUCase *case_stmt = dynamic_cast <NUCase *> (stmt);
      if (case_stmt) {
        NUAssign *new_assign = NULL;
        NUAssign *sel_assign = NULL;
        if (collapseCaseToAssign (case_stmt, &new_assign, &sel_assign, concats)) {
	  NU_ASSERT (new_assign, case_stmt);
	  NU_ASSERT (sel_assign, case_stmt);
          // The selector assignment must precede the assignment which replaces
          // the case stmt.
          stmts.insert (it, sel_assign);
          (*it) = new_assign;
          delete case_stmt;
          retval = true;
          stmt = *it;
        }
      }
    }

    NUAssign *first_assign = dynamic_cast <NUAssign *> (stmt);
    NUStmtList::iterator first = it;    // start of assignment chain
    int number_matched = 0;
    NUAssign *last_assign = NULL;
    it++;                               // past the first assignment
    if (first_assign == NULL) {
      continue;                         // not an assignment statement
    }
    Collapser collapser (mParams, getScope(), mIODB, mMsgContext, first_assign, concats);
    NUStmtList::iterator inner = it;
    while (inner != stmts.end ()) {
      NUAssign *next_assign = dynamic_cast <NUAssign *> (*inner);
      if (next_assign == NULL) {
        // the next statement in the statement list is not an assignment
        break;
      } else if (!collapser.acrete (next_assign)) {
        // the assignment does not match
        type = next_assign->getType ();
        break;
      } else {
        // this statement is collapsible against the current vector pattern
        // matcher context
        number_matched++;
        inner++;
        last_assign = next_assign;
        type = next_assign->getType ();
      }
    }
    // Diagnostic output
    if (number_matched <= 0 || !mVerbose) {
      // no diagnostics
    } else if (collapser.collapsedIntoConcat ()) {
      // Diagnostics for a concat-inducing collapse. The endpoints are the
      // first and last lvalues rather than an index, so just dumping the
      // assigment list is adequate here.
      dumpAssignList (collapser.getSliced (), first, inner);
    } else if (collapser.isConstIndex ()) {
      // diagnostics for constant range end points
      dumpAssignList (collapser.getSliced (), first, inner);
      dumpAssignmentEndpoints (collapser.getSliced (), first_assign, last_assign,
        collapser.getFirstMatchedRange (), collapser.getLastMatchedRange ());
    } else {
      // diagnostics for indexed range end points
      dumpAssignList (collapser.getSliced (), first, inner);
      dump (UtIO::cout (), first_assign);
      dump (UtIO::cout (), last_assign);
      dumpAssignmentEndpoints (collapser.getSliced (), first_assign, last_assign,
        collapser.getFirstMatchedIndex (), collapser.getLastMatchedIndex ());
    }
    // fall out of the inner loop when the end of a chain of vectorisable
    // statements is reached
    NUStmt *replacement = NULL;
    if (number_matched < 1) {
      it = inner;                       // just past last collapsed assignment
    } else if (!collapser.createCollapsed (&replacement) || replacement == NULL ) {
      // a sequence of collapsable statements was identified but the vector
      // matcher was unable to construct the replacement
#ifdef CDB
      UtIO::cerr () << "INTERNAL: failed to collapse equivalent assignment sequence\n";
      dump (UtIO::cerr (), first, number_matched);
      collapser.print ();
#endif
      replacement = NULL;
      it = inner;                       // keep going from where we stopped
      if (mVerboseCollapse) {
        UtIO::cout () << "Failed to collapse " << (number_matched + 1) << " assignments\n";
        dump (UtIO::cout (), first, number_matched + 1);
      }
    } else if (!mParams [eNoFoldCollapsedVector]
      && (replacement = mFoldContext->stmt (replacement)) == NULL) {
      // Folding of the replacement statement just squashed it into
      // nothing... this happens to one branch of an if statement in bug1580...
      // Use -noFoldCollapsedVector when debugging vectorisation issues to more
      // easily see any damage wrought by collapser bugs.
      if (mVerboseCollapse) {
        UtIO::cout () << "Matched " << (number_matched + 1) << " assignments\n";
        dump (UtIO::cout (), first, number_matched + 1);
        UtIO::cout () << "Replacement folded into oblivion" << UtIO::endl;
      }

      // Just throw away the statements
      for (NUStmtList::iterator del = first; del != inner; ++del) {
        mStatistics->incoming ();
        delete *del;
      }
      stmts.erase (first, inner);
      it = inner;
      retval = true;
    } else if (!collapser.allowCollapse (dynamic_cast <const NUAssign *> (replacement))) {
      // The collapser has rejected the vectorisation, most likely by deciding
      // that the replacement is more expensive than the set of collapsed
      // statements.
      if (mVerboseCollapse) {
        UtIO::cout () << "Matched " << (number_matched + 1) << " assignments:\n";
        dump (UtIO::cout (), first, number_matched + 1);
        UtIO::cout () << "Rejected replacement:" << UtIO::endl;
        dump (UtIO::cout (), replacement);
      }
      if (mVerboseCollapse || mVerbose || mVerboseVectorMetrics) {
        UtIO::cout () << "Replacement rejected: ";
        collapser.dumpComplexity (UtIO::cout (), dynamic_cast <const NUAssign *> (replacement));
      }
      delete replacement;
      // We did not make the replacement so reset the outer iterator to the
      // second matched statement in the collapsible sequence.
      it = inner;
    } else {
      // Replaced the sequence with a real statement
      if (mVerboseCollapse) {
        UtIO::cout () << "Matched " << (number_matched + 1) << " assignments:\n";
        dump (UtIO::cout (), first, number_matched + 1);
        UtIO::cout () << "Replaced with:" << UtIO::endl;
        dump (UtIO::cout (), replacement);
      }
      // we have a valid replacement. throw away the stmts which were composed
      // to create the replacement.
      NUStmtList::iterator replacement_iter;
      replacement_iter = stmts.insert (inner, replacement);

      // delete the stmts we are replacing.
      for (NUStmtList::iterator del = first; del != replacement_iter; ++del) {
        mStatistics->incoming ();       // count the number of collapsed stmts.
        delete (*del);
      }
      mStatistics->outgoing();          // count the number of generated stmts.
      stmts.erase (first, replacement_iter);
      it = replacement_iter;
      retval = true;                    // indicate that we did replace something

      // diagnostic noise...
      if (mVerbose) {
        UtIO::cout() << "Assignment Replaced." << UtIO::endl;
      }
      if (mVerboseCollapse) {
        replacement->pr ();
      }
      if (mVerboseVectorMetrics) {
        collapser.dumpComplexity (UtIO::cout (), dynamic_cast <const NUAssign *> (replacement));
      }
    }
  }

  if (concats != NULL) {
    // apply global common concats
    retval |= concats->rightSideDiscover (stmts);
  }

  // Apply local common concat elimination
  CommonConcat local_concats (getScope (), mMsgContext, mIODB, mParams, concats);
  if (!mParams.doCommonConcat ()) {
    // common concat elimination is suppressed
  } else if  (concats != NULL && concats->isLocal (getScope ())) {
    // this is the collapse of the continuous statements so we have inherited
    // the global common concat instance for the module
  } else if (!local_concats.leftSideDiscover (stmts)) {
    // no left-side concats found in the list of statements
  } else if (!local_concats.rightSideDiscover (stmts)) {
    // no matches to left-side concats found in the list of statements
  } else if (type != eNUContAssign && mParams.doConcatNetElimination ()) {
    // do local net elimination and replace the common left-side concats
    local_concats.eliminateLocalNets (stmts);
    local_concats.rightSideReplace (&stmts);
    local_concats.leftSideReplace (stmts);
    retval = true;
  } else {
    // just replace the common left-side concats
    local_concats.leftSideReplace (stmts);
    retval = true;
  }
  return retval;
}

UInt32 Inference::computeIndexForNet(const NUNet * net, SInt32 offset) const
{
  const ConstantRange * range = NULL;
  if (net->isVectorNet()) {
    const NUVectorNet * vector_net = dynamic_cast<const NUVectorNet*>(net);
    range = vector_net->getDeclaredRange();
  } else if (net->isMemoryNet()) {
    const NUMemoryNet * memory_net = net->getMemoryNet();
    range = memory_net->getDeclaredWidthRange();
  } else { 
    // others not expected.
    NU_ASSERT(0, net);
  }
  return range->index(offset);
}

void Inference::dumpAssignmentEndpoints(const NULvalue * sliced,
    const NUAssign * first, 
    const NUAssign * last,
    const ConstantRange &first_range,
    const ConstantRange &last_range) const
{
  UtIO::cout() << "Isomorphic assignment endpoints" << ":" << UtIO::endl;
  UtIO::cout() << "    ";
  if (first_range.getLsb () < last_range.getLsb ()) {
    dumpEndpoint (sliced, last, last_range);
    UtIO::cout() << "    ";
    dumpEndpoint (sliced, first, first_range);
  } else {
    dumpEndpoint (sliced, first, first_range);
    UtIO::cout() << "    ";
    dumpEndpoint (sliced, last, last_range);
  }
}

void Inference::dumpAssignmentEndpoints(const NULvalue * sliced,
    const NUAssign * first, 
    const NUAssign * last,
    const NUExpr *first_index,
    const NUExpr *last_index) const
{
  UtIO::cout() << "Isomorphic assignment endpoints" << ":" << UtIO::endl;
  UtIO::cout() << "    ";
  dumpEndpoint (sliced, first, first_index);
  UtIO::cout() << "    ";
  dumpEndpoint (sliced, last, last_index);
}

void Inference::dumpEndpoint ( const NULvalue * sliced, const NUAssign * assign, 
  const ConstantRange &range) const
{
  const SourceLocator& loc = assign->getLoc();
  UtString printable_loc;
  loc.compose(&printable_loc);
  
  NUNet* net = NULL;
  UtString name;
  const NUIdentLvalue* ident = dynamic_cast<const NUIdentLvalue*>(sliced);
  if (ident)
  {
    net = ident->getIdent();
    name << net->getName()->str();
  }
  else
  {
    const NUMemselLvalue* memsel = dynamic_cast<const NUMemselLvalue*>(sliced);
    NU_ASSERT(memsel, sliced);
    net = memsel->getIdent();
    memsel->compose(&name, NULL, 0, false);    
  }

  if (range.getLength () == 1) {
    UtIO::cout() << printable_loc 
      << ": " << name << "[" << computeIndexForNet(net, range.getMsb ()) << "]" << UtIO::endl;
  } else {
    UtIO::cout() << printable_loc 
      << ": " << name << "[" 
      << computeIndexForNet(net, range.getMsb ()) << ":"
      << computeIndexForNet(net, range.getLsb ()) 
      << "]" << UtIO::endl;
  }
}

void Inference::dumpEndpoint ( const NULvalue * sliced, const NUAssign * assign, 
  const NUExpr *index) const
{
  const SourceLocator& loc = assign->getLoc();
  UtString printable_loc;
  loc.compose(&printable_loc);
  
  NUNet* net = NULL;
  UtString name;
  const NUIdentLvalue* ident = dynamic_cast<const NUIdentLvalue*>(sliced);
  if (ident)
  {
    net = ident->getIdent();
    name << net->getName()->str();
  }
  else
  {
    const NUMemselLvalue* memsel = dynamic_cast<const NUMemselLvalue*>(sliced);
    NU_ASSERT(memsel, sliced);
    net = memsel->getIdent();
    memsel->compose(&name, NULL, 0, false);    
  }

  UtString index_str;
  index->compose (&index_str, NULL, 0);
  UtIO::cout() << printable_loc << ": " << name << "[" << index_str << "]" << UtIO::endl;
}

void Inference::dumpEndpoint ( const NULvalue * sliced, const NUAssign * assign, SInt32 offset ) const
{
  const SourceLocator& loc = assign->getLoc();
  UtString printable_loc;
  loc.compose(&printable_loc);
  
  NUNet* net = NULL;
  UtString name;
  const NUIdentLvalue* ident = dynamic_cast<const NUIdentLvalue*>(sliced);
  if (ident)
  {
    net = ident->getIdent();
    name << net->getName()->str();
  }
  else
  {
    const NUMemselLvalue* memsel = dynamic_cast<const NUMemselLvalue*>(sliced);
    NU_ASSERT(memsel, sliced);
    net = memsel->getIdent();
    memsel->compose(&name, NULL, 0, false);    
  }

  UtIO::cout() << printable_loc 
               << ": " 
               << name
               << "[" << computeIndexForNet(net,offset) << "]"
               << UtIO::endl;
}

void Inference::dumpAssignList(const NULvalue *sliced,
  const NUStmtList::iterator begin, const NUStmtList::iterator end) const
{
  UtString name;
  const NUIdentLvalue* ident = dynamic_cast<const NUIdentLvalue*>(sliced);
  if (ident) {
    NUNet* net = ident->getIdent();
    name << net->getName()->str();
  } else {
    const NUMemselLvalue* memsel = dynamic_cast<const NUMemselLvalue*>(sliced);
    NU_ASSERT(memsel, sliced);
    memsel->compose(&name, NULL, 0, false);    
  }
  UtIO::cout() << "Isomorphic Assignments" << ": " << name << UtIO::endl;
  for ( NUStmtList::const_iterator iter = begin; iter != end ; ++iter ) {
    dumpStmt(*iter); 
  }
}

void Inference::dumpAssignList(const NULvalue * sliced,
                               const NUAssignList & equiv_assigns) const
{
  UtString name;
  const NUIdentLvalue* ident = dynamic_cast<const NUIdentLvalue*>(sliced);
  if (ident)
  {
    NUNet* net = ident->getIdent();
    name << net->getName()->str();
  }
  else
  {
    const NUMemselLvalue* memsel = dynamic_cast<const NUMemselLvalue*>(sliced);
    NU_ASSERT(memsel, sliced);
    memsel->compose(&name, NULL, 0, false);    
  }

  UtIO::cout() << "Isomorphic Assignments"
               << ": "
               << name
               << UtIO::endl;

  for ( NUAssignList::const_iterator iter = equiv_assigns.begin() ;
	iter != equiv_assigns.end() ;
	++iter ) {
    const NUAssign * assign = (*iter);
    dumpStmt(assign);
  }
}

void Inference::dumpStmt(const NUStmt * stmt) const
{
  const SourceLocator& loc = stmt->getLoc();
  UtString printable_loc;
  loc.compose(&printable_loc);
  UtIO::cout() << "    " << printable_loc << UtIO::endl;
}

void InferenceStatistics::print() const
{
  UtIO::cout() << "Inference Summary: "  << UtIO::endl;
  UtIO::cout() << "    " << mModules << " modules affected" << UtIO::endl;
  UtIO::cout() << "    " << mIncoming << " assignments collapsed into " 
	       << mOutgoing << " assignments" << UtIO::endl;
  UtIO::cout() << "    " << mCaseIncoming << " assignments in " 
	       << mCaseOutgoing << " case statements collapsed into "
               << mCaseOutgoing*2 << " assignments" << UtIO::endl;
}


bool Inference::collapseCaseToAssign (NUCase *case_stmt, NUAssign **new_stmt, NUAssign **sel_assign,
  CommonConcat *concats)
{
  // qualify the case statement for vectorisation
  if (!case_stmt->isUserFullCase () && !case_stmt->isFullySpecified()) {
    // the case statement has to be fully specified.
    return false;
  } else if (case_stmt->hasDefault()) {
    // the case statement must not have a default item
    return false;
  }
  NUCase::ItemLoop it = case_stmt->loopItems ();
  NUCaseItem *first_item = *it;
  Collapser collapser (mParams, getScope(), mIODB, mMsgContext, first_item, concats);
  ++it;
  bool matched = true;
  UInt32 n_items = 1;                   // counting the first item
  while (!it.atEnd () && matched) {
    NUCaseItem *item = *it;
    if (!collapser.acrete (item)) {
      // failed to match on this item
      return false;
    }
    n_items++;
    ++it;
  }

  // OK, the matcher churned through all the case items and believes that the
  // case statement may be replaced by a single assignment.
  if (collapser.createCollapsed (case_stmt, new_stmt, sel_assign)
    && *new_stmt != NULL && *sel_assign != NULL) {
    // constructed the replacement statements adequately
  } else if (n_items == 1 && mVerbose) {
    // Screw case - we try to collapse a single-item user full_case case
    // statement into an equivalent pair of statements. However, it is possible
    // that a statement cannot be constructed. For example, if there is an
    // out-of-range constant vector index. In this case we want to output
    // appropriate verbose diagnostics and then just fail the collapse.
    UtIO::cout () << "Cannot collapse singleton case statement\n";
    dump (UtIO::cout (), case_stmt);
    return false;
  } else if (n_items) {
    // Same deal... no diagnostics...
    return false;
  } else {
    // !^%$!%^!... it couldn't make the replacement assignment... something bad
    // has happened...
#ifdef CDB
      UtIO::cerr () << "INTERNAL: failed to collapse equivalent case items\n";
      dump (UtIO::cerr (), case_stmt);
      collapser.print ();
#endif
    return false;
  }

  if (mVerboseCollapse) {
    UtIO::cout () << "Collapsed case statement\n";
    dump (UtIO::cout (), case_stmt);
    dump (UtIO::cout (), *sel_assign);
    dump (UtIO::cout (), *new_stmt);
  }

  if ( mVerbose ) {
    // count the number of case stmts replaced.
    mStatistics->caseIncoming (n_items);
    mStatistics->caseOutgoing (); 
    UtIO::cout() << "Case statement Replaced." << UtIO::endl;
    dumpStmt (case_stmt);
  }

  return true;
}

// These methods are used for dumping lists of collapser-equivalent items and
// the collapsed replacement. 

/*static*/ void Inference::dump (UtOStream &f, const NULvalue *lvalue)
{
  UtString buffer1;
  lvalue->compose (&buffer1, NULL, 0, true);
  f << buffer1 << UtIO::endl;
}

/*static*/ void Inference::dump (UtOStream &f, const NUStmt *stmt, const UInt32 options)
{
  const UInt32 indent = options & eIndentMask;
  if (stmt == NULL) {
    UtString indent (indent, ' ');
    f << indent << "NULL" << UtIO::endl;
    return;
  } else if (stmt == (NUStmt *) 0xdeadbeef) {
    UtString indent (indent, ' ');
    f << indent << "0xDEADBEEF" << UtIO::endl;
    return;
  }
  UtString buffer1;
  stmt->compose (&buffer1, NULL, indent, true);
  f << buffer1;
  if (options & eDumpBlockingDefs) {
    NUNetSet defs;
    stmt->getBlockingDefs (&defs);
    NUNetSet::iterator it;
    int n = 0;
    for (it = defs.begin (); it != defs.end (); ++it) {
      if (n++ == 0) {
        f << "Blocking defs:";
      }
      f << " " << (*it)->getName ()->str ();
    }
    if (n > 0) {
      f << UtIO::endl;
    }
  }
  if (options & eDumpBlockingKills) {
    NUNetSet defs;
    stmt->getBlockingKills (&defs);
    NUNetSet::iterator it;
    int n = 0;
    for (it = defs.begin (); it != defs.end (); ++it) {
      if (n++ == 0) {
        f << "Blocking kills:";
      }
      f << " " << (*it)->getName ()->str ();
    }
    if (n > 0) {
      f << UtIO::endl;
    }
  }
}

/*static*/ void Inference::dump (UtOStream &f, const NUAssignList &list, const UInt32 options)
{
  NUAssignList::const_iterator it = list.begin ();
  UInt32 position = 0;
  while (it != list.end ()) {
    if (options & ePosition) {
      f << UtIO::Width(3) << UtIO::right << position++ << ": ";
    }
    dump (f, *it, options);
    it++;
  }
}

/*static*/ void Inference::dump (UtOStream &f, NUStmtList::const_iterator it, const UInt32 N)
{
  UInt32 n = 0;
  while (n++ < N) {
    dump (f, *it);
    it++;
  }
}

/*static*/ void Inference::dump (UtOStream &f, NUStmtList::iterator it, const UInt32 N)
{
  UInt32 n = 0;
  while (n++ < N) {
    dump (f, *it);
    it++;
  }
}

/*static*/ void Inference::dump (UtOStream &f, const NUStmtList &stmts, const UInt32 options)
{
  NUStmtList::const_iterator it = stmts.begin ();
  UInt32 position = 0;
  while (it != stmts.end ()) {
    if (options & ePosition) {
      char b [64];
      snprintf (b, sizeof (b), "%3d: ", position++);
      f << b;
    }
    dump (f, *it, options);
    it++;
  }
}

/*static*/ void Inference::dump (UtOStream &s, const NUModule *module, const UInt32 options)
{
  s << "Module " << module->getName ()->str () << UtIO::endl;
  if (options & eDumpBlocks) {
    for (NUBlockCLoop it0 = module->loopBlocks (); !it0.atEnd (); ++it0) {
      NUBlock *block = *it0;
      for (NUStmtLoop it1 = block->loopStmts (); !it1.atEnd (); ++it1) {
        dump (s, *it1, options);
      }
    }
  }
  if (options & eDumpContAssigns) {
    NUContAssignList cont_assigns;
    module->getContAssigns (&cont_assigns);
    dump (s, cont_assigns, options);
  }
  if (options & eDumpContAssignOrder) {
    NUContAssignList assigns;
    module->getContAssigns (&assigns);
    NUContAssignList::const_iterator it;
    s << "[";
    for (it = assigns.begin (); it != assigns.end (); it++) {
      NUContAssign *assign = *it;
      if (it == assigns.begin ()) {
        s << assign->getLoc ().getLine ();
      } else {
        s << "," << assign->getLoc ().getLine ();
      }
    }
    s << "]" << UtIO::endl;
  }
}

/*static*/ void Inference::dump (UtOStream &f, const NUContAssignList &stmts, const UInt32 options)
{
  NUContAssignList::const_iterator it = stmts.begin ();
  UInt32 N = (options & eCountMask) >> 8;
  if (N == 0) {
    N = stmts.size ();
  }
  UInt32 position = 0;
  while (it != stmts.end () && N > 0) {
    if (options & ePosition) {
      char b [64];
      snprintf (b, sizeof (b), "%3d: ", position++);
      f << b;
    }
    dump (f, *it, options);
    it++;
    N--;
  }
}

/*static*/ void Inference::dump (UtOStream &f, const NUContAssignVector &stmts, const UInt32 options)
{
  NUContAssignVector::const_iterator it = stmts.begin ();
  UInt32 N = (options & eCountMask) >> 8;
  if (N == 0) {
    N = stmts.size ();
  }
  UInt32 position = 0;
  while (it != stmts.end () && N > 0) {
    if (options & ePosition) {
      char b [64];
      snprintf (b, sizeof (b), "%3d: ", position++);
      f << b;
    }
    dump (f, *it, options);
    it++;
    N--;
  }
}

// Constructor that initialises options from the command line
Inference::Params::Params (const ArgProc *args, MsgContext *msg_context, const UInt32 options) : 
  mMsgContext (msg_context), mOptions (options), mCollapserThreshold (0)
{
  mOptions |= 
      (args->getBoolValue(CRYPT("-verboseInference")) ? eVerboseInference : 0)
    | (args->getBoolValue(CRYPT("-verboseCollapse")) ?  eVerboseCollapse  : 0)
    | (args->getBoolValue(CRYPT("-verboseVectorMetrics")) ?  eVerboseVectorMetrics  : 0)
    | (args->getBoolValue(CRYPT("-noFoldCollapsedVector")) ?  eNoFoldCollapsedVector  : 0)
    | (args->getBoolValue(CRYPT("-verboseAlerts")) ? eVerboseAlerts :0);
  if (ArgProc::eKnown == args->getIntFirst (CRYPT("-collapseThreshold"), &mCollapserThreshold)
    && mCollapserThreshold > 0) {
    mOptions |= eEnableMetrics;
  }
  // Common concat options
  CommonConcat::parseOptions (args, &mOptions, mMsgContext);
}

#ifdef CDB
void Inference::Params::pr () const
{
  char buffer [1024];
  UtIO::cout () << cvt (buffer, sizeof (buffer)) << UtIO::endl;
}

const char *Inference::Params::cvt (char *s, const unsigned int size) const
{
  unsigned int n = 0;
  s [n] = '\0';
#define PROBE(_x_) \
  if ((mOptions & Inference::e##_x_) == 0) {   \
  } else if (n == 0) { \
    n += snprintf (s + n, size - n, #_x_); \
  } else { \
    n += snprintf (s + n, size - n, "|" #_x_); \
  }
  PROBE (VerboseInference);
  PROBE (VerboseCollapse);
  PROBE (EnableMetrics);
  PROBE (VerboseVectorMetrics);
  PROBE (VerboseCommonConcat);
  PROBE (LeftConcatCollapse);
  PROBE (RightConcatCollapse);
  PROBE (CommonConcat);
  PROBE (EliminateConcatNets);
#undef PROBE
  return s;
}
#endif


