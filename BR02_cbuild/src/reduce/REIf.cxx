// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*! \file 
  Transform IF THEN ELSE
*/

#include "reduce/REIf.h"
#include "localflow/UD.h"

#include "util/UtString.h"
#include "util/UtString.h"
#include "util/UtVector.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"

#include "nucleus/NUNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUTaskEnable.h"

#include "bdd/BDD.h"

#include <algorithm>

REIf::REIf(NUNetRefFactory *netref_factory,
           MsgContext* msgContext,
           AtomicCache* cache,
	   IODBNucleus* iodb,
           ArgProc* args,
           bool verbose):
  mNetRefFactory(netref_factory),
  mMsgContext(msgContext),
  mAtomicCache(cache),
  mIODB(iodb),
  mArgs(args),
  mVerbose (verbose) {}

REIf::~REIf() {}


//! Walk the blocks that could contain if-then-else stmts in module \a mod
void REIf::reduceModule(NUModule* mod)
{
  Callback callback(this);
  NUDesignWalker walker(callback, mVerbose);
  walker.module(mod);
  // may need to redo UD
  if (callback.statementsModified())
  {
    UD ud (mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.module(mod);
  }
} // void REIf::reduceModule


void REIf::reduceStatements(NUStmtLoop stmtLoop, NUStmtList& newStmts,
                            NUStmtList& killStmts)
{
  for (/* no initial */; !stmtLoop.atEnd(); ++stmtLoop)
  {
    NUStmt* stmt = *stmtLoop;
    NUIf* ifstmt = dynamic_cast<NUIf*>(stmt);
    if ((ifstmt != NULL) &&
        xformcase(ifstmt, &newStmts))
      killStmts.push_back(stmt);
    else
      newStmts.push_back(stmt);
  }
} // void REIf::reduceStatements

// Transform
//	if (x==k1)
//	   stmt1
//	else if (x == k2)
//	   stmt2
//	...
//	else
//	   stmtk
//
// to
//	switch (x) {
//	  case k1: stmt1; break;
//	  case k2: stmt2; break;
//	  ...
//	  default: stmtk; break;
//	}
//


// If we steal the statements from the then- and else-
// blocks, we need to remove them from the NUIf or the statements
// will be deleted twice.
//
static void sScrubIf (NUIf* ifstmt)
{
  NUStmtList emptyList;

  // The THEN has gone into the case actions
  ifstmt->replaceThen (emptyList);

  // Now if ELSE is an if statement, recurse...
  NUStmtList *elseStmts = ifstmt->getElse ();
  NUIf *elif;
  if ((elseStmts->size () == 1)
      && (elif = dynamic_cast<NUIf*> (*(elseStmts->begin ()))))
    sScrubIf (elif);
  else
    ifstmt->replaceElse (emptyList);    

  delete elseStmts;
}

// See if the conditional expression is equivalent to a series of case indices.
// Called with the conditional expression \a cond (assumed to be of the form "sel == 3"
// Returns an updated set of case indices \a cases, and initializes or tests against any
// existing \foundSelector that represents the non-constant expression we are comparing
// against.
//
// Returns: true if this was a good selector expression, false if we need to abandon processing.
//      Sets the \a reverse flag if the conditional expression was such that the "else-clause" is
//      where the action we are trying to extract. Consider "if (a) ; else ACTION_FOR_ZERO;"
//
bool REIf::findSelector (const NUExpr *cond, CaseIndices& cases, const NUExpr** foundSelector, bool* reverse )
{
  *reverse = false; // Assume THEN clause is the interesting one to follow for next condition.

  // Now look at the expression
  switch (cond->getType ())
    {
    default:
      return false;		// not well formed.

    case NUExpr::eNUVarselRvalue:
    case NUExpr::eNUIdentRvalue:
    case NUExpr::eNUMemselRvalue:
      // Treat "if (v)" like "if (v != 0)", so check if we have a zero case already
      // because the "else" is going to be the "zero" case.
      {
        if (*foundSelector) {
          if (not cond->equal (**foundSelector))
            return false;
        } else {
          *foundSelector = cond;
        }

        // Check if we already have a 'zero case'
	if (cases.find (SInt32 (0)) != cases.end ())
	  {
            UtString eString;
            cond->compose (&eString, NULL);
	    mMsgContext->DuplicateIfCondition (&(cond->getLoc ()), eString.c_str (), 0);
	  }
	else
	  cases.insert (0);
        *reverse = true;        // Do the ELSE clause on a zero
        return true;
      }

    case NUExpr::eNUUnaryOp:
      // Look for (!v), equivalent to (v==0)
      {
        const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(cond);
        if (uop->getOp () != NUOp::eUnLogNot)
          return false;

        // check that the operand is valid as the selector
        const NUExpr* candSelector = uop->getArg (0);
        if (*foundSelector) {
          if (not candSelector->equal (**foundSelector))
            return false;
        } else {
          *foundSelector = candSelector;
        }
            
        if (cases.find (SInt32 (0)) != cases.end ())
        {
          UtString eString;
          uop->compose (&eString, NULL);
          mMsgContext->DuplicateIfCondition (&(cond->getLoc ()), eString.c_str (), 0);
        } else {
          cases.insert (0);
        }
        return true;
      }

    case NUExpr::eNUBinaryOp:
      // Either (e == c) or else (e==c1) || (e==c2)
      {
	const NUBinaryOp * bop = dynamic_cast<const NUBinaryOp*>(cond);

	if (bop->getOp () == NUOp::eBiEq
	    && (bop->getArg (1)->getType () == NUExpr::eNUConstNoXZ)) {
          // (expr == CONST)
          const NUConst *c = bop->getArg(1)->castConst();
          const NUExpr* candSelector = bop->getArg (0);
          if (*foundSelector) {
            if (not candSelector->equal (**foundSelector))
              return false;
          } else {
            *foundSelector = candSelector;
          }

          SInt32 val;
          if (not c->getL (&val))
            // Can't represent in 32 signed bits
            return false;

          if (cases.find (val) != cases.end ())
          {
            UtString eString;
            candSelector->compose (&eString, NULL);
            mMsgContext->DuplicateIfCondition (&(bop->getLoc ()), eString.c_str (), val);
          } else {
            cases.insert (val);
          }

          return true;
        } else if ((bop->getOp () == NUOp::eBiLogOr) || (bop->getOp () == NUOp::eBiBitOr)) {
          bool dummy1=false, dummy2=false;
          if (findSelector(bop->getArg (0), cases, foundSelector, &dummy1) &&
              findSelector(bop->getArg (1), cases, foundSelector, &dummy2))
          {
            if (dummy1 || dummy2)
              return false;   // messy case not handled

            return true;
          }
        }
      }
      break;
    }
  return false;
}

// Look thru the actions list to see if any of the existing CaseIndices
// contain elements on the indices list for the case-action we're about to add.
//
struct FindActionOverLap{
  // Constructor saves vector with possible duplicates
  FindActionOverLap (REIf::CaseIndices* p, MsgContext* mctx,
		     const SourceLocator& loc,
		     const NUExpr* selector)
    : mIndices (p), mMsgContext (mctx), mLoc (&loc), mSel (selector){}

  // Search a single caseAction looking for matching indices
  // remove any duplicates
  void operator()(REIf::CaseAction& action)
  {
    REIf::CaseIndices* actionIndices = action.first;
    REIf::CaseIndices intersect;

    std::set_intersection (actionIndices->begin (), actionIndices->end (),
			   mIndices->begin (), mIndices->end (),
			   std::inserter (intersect, intersect.begin ()));

    if (intersect.empty ())
      return;

    // Complain about duplicates
    UtString selString;
    mSel->compose (&selString, NULL);
    for(REIf::CaseIndices::const_iterator i=intersect.begin ();
	i != intersect.end (); ++i)
      mMsgContext->DuplicateIfCondition (mLoc,
                                         selString.c_str (),
                                         *i);
  }

  // Remember the list of candidate indices
  REIf::CaseIndices *mIndices;
  MsgContext* mMsgContext;
  const SourceLocator* mLoc;
  const NUExpr* mSel;
};

void
REIf::checkDuplicateIndices (const NUExpr* selector, CaseActionList& actions, CaseIndices *indices, const SourceLocator& loc)
{
  FindActionOverLap matcher (indices, mMsgContext, loc, selector);
  for_each(actions.begin (), actions.end (), matcher);

}
/*!
 * Examine \a ifstmt to see that it compares selector \a sel.  If well
 * formed, add the conditions and the actions to their respective
 * lists.  On a successful return, \a stmts is updated to contain the
 * "else" clause that would be the default case action.
 */
bool
REIf::walkIf (NUStmtList* stmts, const NUExpr* sel, CaseActionList& actions,
		     NUStmtList &defActions)
{
  
  if (stmts->size () != 1)
    // This isn't an else-if, so it would have to be the default
    {
      std::copy (stmts->begin (), stmts->end (), back_inserter (defActions));
      delete stmts;
      return true;
    }

  NUIf *ifstmt = dynamic_cast<NUIf *>(*(stmts->begin ()));

  if (not ifstmt)
    // It's not part of an else-if chain, so treat it as the "default:" action
    {
      std::copy (stmts->begin (), stmts->end (), back_inserter (defActions));
      delete stmts;
      return true;
    }

  // We're thru with the passed in statement list
  delete stmts;

  bool reversed = false;
  CaseIndices *indices = new CaseIndices;
  const NUExpr* foundSel = sel;
  if (findSelector (ifstmt->getCond (), *indices, &foundSel, &reversed) &&
      (sel == foundSel))
    {
      // If there are action statements associated with this
      // index, they won't execute because some other action
      // already used this index.  But something like:
      //   if (a) S1 else if (!a) S2;
      // is transformed to
      //   if (a) S1 else if (a) else S2;
      //
      NUStmtCLoop thenLoop = reversed ? ((const NUIf*)ifstmt)->loopElse () 
                                      : ((const NUIf*)ifstmt)->loopThen ();
      if (!thenLoop.atEnd ())
	// Check the indices list to see if any are already specified
	// by an earlier condition and warn about that
	checkDuplicateIndices (foundSel, actions, indices, ifstmt->getLoc ());

      // Add the indices and action stmts to the candidate actions.
      actions.push_back (std::pair<CaseIndices*, NUStmtList *>(indices,
                                                               reversed ? ifstmt->getElse () : ifstmt->getThen ()));
      
      return walkIf ((reversed ? ifstmt->getThen () : ifstmt->getElse ()), sel, actions, defActions);
    }

  // Not a well-formed candidate for becoming a case-action
  delete indices;
  return false;
}

// Ideally we ought to factor in the density, because that impacts the defaults
static UInt32 sCountCases (REIf::CaseActionList &actions)
{
  UInt32 count = 0;

  for (REIf::CaseActionList::const_iterator i = actions.begin ();
       i != actions.end (); ++i)
    {
      count += (*i).first->size ();
    }

  return count;
}


bool REIf::xformcase(NUIf* old, NUStmtList* newStmts)
{
  bool ret = false;
  const SourceLocator& loc = old->getLoc();

  // Find what the case selector should be
  CaseIndices *cases = new CaseIndices;

  const NUExpr* sel = NULL;     // Start with no matching selector yet found
  bool reversed = false;

  if (! findSelector (old->getCond (), *cases, &sel, &reversed)
    || !sel
    || sel->getBitSize () > 32)
    // Not looking like a case...
    {
      delete cases;
      return ret;
    }

  CaseActionList actions;
  actions.push_back (std::pair<CaseIndices*, NUStmtList*> (cases, reversed ? old->getElse () : old->getThen ()));

  NUStmtList *elseRoot = reversed ? old->getThen () : old->getElse ();
  NUStmtList defActions;

  if (walkIf (elseRoot, sel, actions, defActions) && sCountCases (actions) > 2)
    {
      // We successfully transformed the whole if-then-else if chain.
      // Check for PartselRvalue vs. IdentRvalue
      CopyContext cc (0,0);
      NUExpr *caseSel = sel->copy (cc);
      UInt32 cmpSize = caseSel->getBitSize ();

      NUCase *theCase = new NUCase (caseSel, mNetRefFactory, NUCase::eCtypeCase,  old->getUsesCFNet(), loc);

      // Construct a vector of case actions
      for(CaseActionList::iterator i = actions.begin ();
	  i != actions.end (); ++i)
	{
	  CaseIndices* cIndices = (*i).first;

	  if (cIndices->empty ())
	    // No indices associated with this action (must have been
	    // duplicates of an earlier case action)...
	    {
	      // The actions associated with this case-action will never
	      // be coded, so they need to be freed now.
	      for(NUStmtList::iterator stmt = (*i).second->begin ();
		  stmt != (*i).second->end (); ++stmt)
		{
		  delete *stmt;
		}
	      (*i).second->clear ();
	      continue;
	    }
	  // Create a new item for this group of related actions
	  NUCaseItem* item = new NUCaseItem (loc);

	  for (CaseIndices::const_iterator c = cIndices->begin ();
	       c != cIndices->end ();
	       ++c)
	    {
	      NUConst* cv = NUConst::create ((*c < 0), *c, cmpSize, loc);

              NUCaseCondition *ccond = new NUCaseCondition (loc, cv);
              item->addCondition (ccond);
	    }

	  // Add the statements associated with this action
	  item->replaceStmts (* ((*i).second));

	  // Now add this case item to the case statement
	  theCase->addItem (item);
	}

      // Check if this is fully populated
      UInt32 indexcount=0;
      for (NUCase::ItemLoop item = theCase->loopItems ();
	   !item.atEnd ();
	   ++item)
	for(NUCaseItem::ConditionLoop conds = (*item)->loopConditions ();
	    !conds.atEnd ();
	    ++conds)
	  indexcount++;
   
      theCase->putFullySpecified ((1u << cmpSize) == indexcount);

      // if the elseRoot is non-empty and we didn't fully specify
      // all values of the selector, add a default.
      if (not defActions.empty () && not theCase->isFullySpecified ()) {
	NUCaseItem * defItem = new NUCaseItem (loc);
	defItem->replaceStmts (defActions);

	theCase->addItem (defItem);
	theCase->putHasDefault (true);
	defActions.clear();
      }
      
      if (mVerbose)
	{
	  UtIO::cout () << "Transformed if :" << UtIO::endl;
	  old->print (true, 8);
	  UtIO::cout () << "into case :" << UtIO::endl;
	  theCase->print (true, 8);
	}

      // Lastly remove the statements from the if-then-else that we put
      // into the case.
      sScrubIf (old);

      // if defActions is populated, we didn't use its contents. cleanup.
      for (NUStmtList::iterator iter = defActions.begin();
	   iter != defActions.end();
	   ++iter) {
	NUStmt * unneeded = (*iter);
	delete unneeded;
      }
      defActions.clear();


      newStmts->push_back (theCase);

      ret = true;
    }

  // Lastly clean out the case map too
  for(CaseActionList::iterator i = actions.begin (); i != actions.end (); ++i)
    {
      delete (*i).first;	// free old vector of indices
      delete (*i).second;	// Free old NUStmtList
    }

  return ret;
}

REIf::Callback::Callback(REIf* reif) :
  mREIf(reif),
  mStmtsModified(false)
{}

REIf::Callback::~Callback()
{}

NUDesignCallback::Status
REIf::Callback::operator()(Phase phase, NUBlock* block)
{
  // In post phase, we're going bottom up, when we're not going to find
  // any remaining conversions.
  if (phase == ePost)
    return eNormal;

  // Call our reduce statements function to redo the statements. If it succeeds
  // then process the data.
  NUStmtList newStmts;
  NUStmtList killStmts;
  mREIf->reduceStatements(block->loopStmts(), newStmts, killStmts);

  if (!killStmts.empty())
  {
    // The block statement list is stale, update it
    block->replaceStmtList(newStmts);
    mStmtsModified = true;
    deleteStmtList(killStmts);
  }
  return eNormal;
}

NUDesignCallback::Status
REIf::Callback::operator()(Phase phase, NUIf* ifStmt)
{
  // In post phase, we're going bottom up, when we're not going to find
  // any remaining conversions.
  if (phase == ePost)
    return eNormal;

  // Call our rewrite function to redo the then statements. If it
  // succeeds then process the data.
  NUStmtList newStmts;
  NUStmtList killStmts;
  mREIf->reduceStatements(ifStmt->loopThen(), newStmts, killStmts);
  if (!killStmts.empty())
  {
    // The if-then statement list is stale, update it
    ifStmt->replaceThen(newStmts);
    mStmtsModified = true;
    deleteStmtList(killStmts);
  }

  // Call our rewrite function to redo the else statements. If it
  // succeeds then process the data.
  newStmts.clear();
  killStmts.clear();
  mREIf->reduceStatements(ifStmt->loopElse(), newStmts, killStmts);
  if (!killStmts.empty())
  {
    // The if-then statement list is stale, update it
    ifStmt->replaceElse(newStmts);
    mStmtsModified = true;
    deleteStmtList(killStmts);
  }
  return eNormal;
}

NUDesignCallback::Status
REIf::Callback::operator()(Phase phase, NUCaseItem* caseItem)
{
  // In post phase, we're going bottom up, when we're not going to find
  // any remaining conversions.
  if (phase == ePost)
    return eNormal;

  // Call our rewrite function to redo the statements. If it succeeds
  // then process the data.
  NUStmtList newStmts;
  NUStmtList killStmts;
  mREIf->reduceStatements(caseItem->loopStmts(), newStmts, killStmts);
  if (!killStmts.empty())
  {
    // The caseItem statement list is stale, update it
    caseItem->replaceStmts(newStmts);
    mStmtsModified = true;
    deleteStmtList(killStmts);
  }
  return eNormal;
}

NUDesignCallback::Status
REIf::Callback::operator()(Phase, NUTaskEnable* enable)
{
  if (enable->isHierRef()) {
    return eSkip;
  } else {
    return eNormal;
  }
}

NUDesignCallback::Status
REIf::Callback::operator()(Phase phase, NUTF* tf)
{
  // In post phase, we're going bottom up, when we're not going to find
  // any remaining conversions.
  if (phase == ePost)
    return eNormal;

  // Call our rewrite function to redo the statements. If it succeeds
  // then process the data.
  NUStmtList newStmts;
  NUStmtList killStmts;
  mREIf->reduceStatements(tf->loopStmts(), newStmts, killStmts);
  if (!killStmts.empty())
  {
    // The caseItem statement list is stale, update it
    tf->replaceStmtList(newStmts);
    mStmtsModified = true;
    deleteStmtList(killStmts);
  }
  return eNormal;
}
