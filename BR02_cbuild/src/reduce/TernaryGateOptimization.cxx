// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/TernaryGateOptimization.h"
#include "reduce/ControlExtract.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"

#include "bdd/BDD.h"

TernaryGateOptimization::TernaryGateOptimization(AtomicCache * str_cache,
                                                 NUNetRefFactory * netref_factory,
                                                 MsgContext * msg_context,
                                                 IODBNucleus * iodb,
                                                 ArgProc * args,
                                                 bool verbose,
                                                 ControlExtractStatistics * control_extract_statistics) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mIODB(iodb),
  mArgs(args),
  mVerbose(verbose),
  mControlExtractStatistics(control_extract_statistics)
{
  mBDDContext = new BDDContext;
  mBDDContext->putExpressionCaching(true);  // For BDD-to-NUExpr construction.
  mBDDContext->openWide();                  // Expand the BDD thresholds.
  mBDDContext->putReduceExpressions(false); // Do not reduce during conversion.
}

TernaryGateOptimization::~TernaryGateOptimization()
{
  delete mBDDContext;
}


void TernaryGateOptimization::mergedBlocks(NUModule * /*module*/, NUUseDefVector & blocks)
{
  for (NUUseDefVector::iterator iter = blocks.begin();
       iter != blocks.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    NUAlwaysBlock * always_block = dynamic_cast<NUAlwaysBlock*>(use_def);
    NU_ASSERT(always_block!=NULL,use_def);
    always(always_block);
  }
}


void TernaryGateOptimization::determineContextForAssigns(NUAlwaysBlock * always,
                                                         NUStmtToBDDEnrollment & enrollment)
{
  NUBlock * block = always->getBlock();

  NUNetRefSet context_uses(mNetRefFactory);
  for (NUStmtLoop loop = block->loopStmts();
       not loop.atEnd(); 
       ++loop) {
    NUStmt * stmt = *loop;

    if (stmt->getType() != eNUBlockingAssign) {
      // Non-assignment. Restart context and skip.
      context_uses.clear();
      enrollment[stmt] = eDoNotEnroll;
      continue;
    } 

    NUBlockingAssign * assign = dynamic_cast<NUBlockingAssign*>(stmt);
    if (assign->getLvalue()->getBitSize() != 1) {
      // Multi-bit write. Restart context and skip.
      context_uses.clear();
      enrollment[stmt] = eDoNotEnroll;
      continue;
    }

    NUNetRefSet stmt_uses(mNetRefFactory);
    assign->getBlockingUses(&stmt_uses);

    UInt32 stmt_bits = stmt_uses.getNumBits();
    if (stmt_bits < 4) { 
      // SWAG: Statements referencing too few bits do not deserve to be BDDized.
      // Restart context and skip.
      context_uses.clear();
      enrollment[stmt] = eDoNotEnroll;
      continue;
    }

    if (context_uses.empty()) {
      // First statement worth enrolling.
      enrollment[stmt] = eEnrollNewContext;
      context_uses.insert(stmt_uses.begin(),stmt_uses.end());
    } else {
      // On subsequent statements with the same context, check that
      // there is use overlap.
      NUNetRefSet shared(mNetRefFactory);

      // shared == set intersection.
      NUNetRefSet::set_intersection(context_uses,stmt_uses,shared);

      UInt32 shared_bits  = shared.getNumBits();
      UInt32 context_bits = context_uses.getNumBits();

      // SWAG: Allow if the number of shared bits is at least half of the
      // context uses and three quarters of the number of stmt uses.
      bool sufficient_overlap = ( ((2 * shared_bits) > (context_bits)) and
                                  ((4 * shared_bits) > (3 * stmt_bits)) );
      if (sufficient_overlap) {
        enrollment[stmt] = eEnrollSameContext;
        context_uses.insert(stmt_uses.begin(),stmt_uses.end());
      } else {
        // Subsequent assigns are too different. Restart context, but
        // allow processing.
        enrollment[stmt] = eEnrollNewContext;
        context_uses.clear();
        context_uses.insert(stmt_uses.begin(),stmt_uses.end());
      }
    }
  }
}


void TernaryGateOptimization::eliminateSmallAssignContexts(NUAlwaysBlock * always,
                                                           NUStmtToBDDEnrollment & enrollment)
{
  NUBlock * block = always->getBlock();

  // Disqualify all contexts which do not have more than 2 statements.
  for (NUStmtLoop loop = block->loopStmts();
       not loop.atEnd(); 
       ++loop) {
    NUStmt * stmt = *loop;
    NUStmtToBDDEnrollment::iterator location = enrollment.find(stmt);
    NU_ASSERT(location != enrollment.end(),stmt);
    
    BDDEnrollmentEnum enroll = location->second;
    if (enroll == eEnrollNewContext) {
      // We've got a statement worth enrolling. Count the number in
      // this context.
      UInt32 ctx_stmts = 0;
      NUStmtLoop ctx_loop = loop; // Copy our iterator to walk forward.
      for (++ctx_loop; not ctx_loop.atEnd(); ++ctx_loop) {
        NUStmt * ctx_stmt = *ctx_loop;
        NUStmtToBDDEnrollment::iterator ctx_location = enrollment.find(ctx_stmt);
        NU_ASSERT(ctx_location != enrollment.end(),ctx_stmt);

        BDDEnrollmentEnum ctx_enroll = ctx_location->second;
        if (ctx_enroll == eEnrollSameContext) {
          ++ctx_stmts;
        } else {
          break;
        }
      }

      // Only proceed if there are more than 2 statements in this context. 
      if (ctx_stmts <= 2) {
        // Two or fewer statements in this context. Mark everything in
        // this context as eDoNotEnroll.
        location->second = eDoNotEnroll;
        NUStmtLoop ctx_loop = loop;
        for (++ctx_loop; not ctx_loop.atEnd(); ++ctx_loop) {
          NUStmt * ctx_stmt = *ctx_loop; // Copy our iterator to walk forward.
          NUStmtToBDDEnrollment::iterator ctx_location = enrollment.find(ctx_stmt);
          NU_ASSERT(ctx_location != enrollment.end(),ctx_stmt);

          BDDEnrollmentEnum ctx_enroll = ctx_location->second;
          if (ctx_enroll == eEnrollSameContext) {
            ctx_location->second = eDoNotEnroll;
          } else {
            break;
          }
        }
      }
    }
  }
}


bool TernaryGateOptimization::always(NUAlwaysBlock * always)
{
  bool did_work = false;

  NUStmtToBDDEnrollment enrollment;

  determineContextForAssigns(always, enrollment);

  eliminateSmallAssignContexts(always, enrollment);

  NUExprToBDD expr_to_bdd; 

  NUBlock * block = always->getBlock();
  for (NUStmtLoop loop = block->loopStmts();
       not loop.atEnd(); 
       ++loop) {
    NUStmt * stmt = *loop;
    NUStmtToBDDEnrollment::iterator location = enrollment.find(stmt);
    NU_ASSERT(location != enrollment.end(),stmt);

    BDDEnrollmentEnum enroll = location->second;
    if (enroll == eDoNotEnroll) {
      // Not included. Restart context and skip.
      mBDDContext->clear();
      continue;
    } else if (enroll == eEnrollNewContext) {
      // New context.
      mBDDContext->clear();
    }

    NU_ASSERT(stmt->getType() == eNUBlockingAssign, stmt);

    NUBlockingAssign * assign = dynamic_cast<NUBlockingAssign*>(stmt);

    NUExpr * rhs = assign->getRvalue();
    NUExpr * replacementRhs = makeTernaryExpr(rhs,&expr_to_bdd);
    if (replacementRhs) {
      // Rewrite was successful. Update assign.
      assign->replaceRvalue(replacementRhs);
      delete rhs;
      
      did_work = true;
    }
  }

  expr_to_bdd.clear();
  mBDDContext->clear();

  return did_work;
}


void TernaryGateOptimization::module(NUModule * one_module)
{
  // Only modify requested modules.
  if (not one_module->isTernaryGateOptimization()) {
    return;
  }

  // Only process modules which contain only continuous assignments.
  if (not onlyContAssignDrivers(one_module)) {
    return;
  }

  // Only allow single-bit continuous assignments.
  if (not simpleContAssigns(one_module)) {
    return;
  }

  NetAssignMap net_assigns;
  segmentContAssigns(one_module, &net_assigns);

  CopyContext cc(NULL,NULL);

  ControlExtract extractor(mStrCache,
                           mNetRefFactory,
                           mMsgContext,
                           mIODB,
                           mArgs,
                           true,
                           mControlExtractStatistics);

  // Keep track of assigns we have replaced.
  NUContAssignList changed;

  // Create one always block per net.
  // Future: Create one always block per net per unique fanin set (use union find over net-ref bits).
  for (NetAssignMap::iterator iter=net_assigns.begin();
       iter != net_assigns.end();
       ++iter) {
    NUContAssignList & assigns = iter->second;
    // Disqualify if only one candidate statement.
    // Disqualify if defs overlap.
    // Disqualify if any use overlaps with any def.
    // If any single assign in a group fails, the entire group fails.
    if (not consistentAssigns(assigns)) {
      continue;
    }

    // Keep bdds around to persist variable orderings across expressions.
    // TBD: is this really the right way? should we clear the BDD context between nets?
    NUExprToBDD expr_to_bdd; 

    // Create the new always block as needed.
    NUAlwaysBlock * always = NULL; 

    for (NUContAssignList::iterator iter=assigns.begin();
         iter != assigns.end();
         ++iter) {
      NUContAssign * assign = (*iter);

      NUBlockingAssign * replacement = makeTernaryAssign(assign,&expr_to_bdd);
      if (replacement==NULL) {
        continue;
      }
    
      if (always==NULL) {
        // Create a new always block to contain our BDD-massaged statements.
        NUBlock * block = new NUBlock(NULL, one_module, mIODB, mNetRefFactory, false, one_module->getLoc());
        always = new NUAlwaysBlock(one_module->newBlockName(mStrCache, one_module->getLoc()),
                                   block, mNetRefFactory, one_module->getLoc(), false);
        one_module->addAlwaysBlock(always);
      }

      always->getBlock()->addStmt(replacement);

      changed.push_back(assign);
    }

    mBDDContext->clear();

    if (always) {
      // Combine the control structures for the generated ?: assigns.
      extractor.always(always);
    }
  }

  // Remove the replaced continuous assigns.
  for (NUContAssignList::iterator iter = changed.begin();
       iter != changed.end();
       ++iter) {
    NUContAssign * assign = (*iter);
    one_module->removeContAssign(assign);
    delete assign;
  }
}


NUExpr * TernaryGateOptimization::makeTernaryExpr(const NUExpr * rhs, NUExprToBDD * expr_to_bdd)
{
  // Does the RHS contain wide operators? Bit-blasting these is usually sub-optimal.
  if (wideExpressions(rhs)) {
    return NULL;
  }

  BDD bdd = rhs->bdd(mBDDContext);
  if (not bdd.isValid()) {
    return NULL;
  }
  // Save the BDD for consistent orderings across expressions.
  (*expr_to_bdd)[rhs] = bdd;
  const NUExpr * bddRhs = mBDDContext->bddToExpr(bdd, rhs->getLoc());
  if (bddRhs==NULL) {
    return NULL;
  }

  // Will this expresssion explode when converted back to a non-factory expression?
  if (blowsup(bddRhs)) {
    return NULL;
  }

  // We're good, amigo.
  CopyContext cc(NULL,NULL);
  NUExpr * replacementRhs = bddRhs->copy(cc);
  return replacementRhs;
}

NUBlockingAssign * TernaryGateOptimization::makeTernaryAssign(const NUAssign * assign, NUExprToBDD * expr_to_bdd)
{
  const NUExpr   * rhs = assign->getRvalue();
  NUExpr * replacementRhs = makeTernaryExpr(rhs,expr_to_bdd);
  if (replacementRhs == NULL) {
    return NULL;
  }

  CopyContext cc(NULL,NULL);
  const NULvalue * lhs = assign->getLvalue();
  NULvalue * replacementLhs = lhs->copy(cc);
  NUBlockingAssign * replacement = new NUBlockingAssign(replacementLhs, replacementRhs, 
                                                        false, assign->getLoc());
  return replacement;
}


bool TernaryGateOptimization::onlyContAssignDrivers(const NUModule * mod) const
{
  // return true if there are continuous assigns and NO other continuous driver.
  bool only_cont_assigns = ( (not mod->loopContAssigns().atEnd()) and
                             mod->loopContEnabledDrivers().atEnd() and
                             mod->loopTriRegInit().atEnd() and
                             mod->loopAlwaysBlocks().atEnd() and
                             mod->loopInitialBlocks().atEnd() and 
                             mod->loopInstances().atEnd() );
  return only_cont_assigns;
}


bool TernaryGateOptimization::simpleContAssigns(const NUModule * mod) const
{
  for (NUModule::ContAssignCLoop loop = mod->loopContAssigns();
       not loop.atEnd();
       ++loop) {
    const NUContAssign * assign = (*loop);

    if (assign->getStrength() != eStrDrive) {
      return false;
    }

    const NULvalue * lhs = assign->getLvalue();
    if (lhs->getType()==eNUConcatLvalue or lhs->getBitSize()!=1) {
      return false;
    }

    const NUExpr   * rhs = assign->getRvalue();
    if (rhs->drivesZ()) {
      return false;
    }
  }
  return true;
}

void TernaryGateOptimization::segmentContAssigns(NUModule * mod, NetAssignMap * net_assigns)
{
  for (NUModule::ContAssignLoop loop = mod->loopContAssigns();
       not loop.atEnd();
       ++loop) {
    NUContAssign * assign = (*loop);

    NUNetSet defs;
    assign->getDefs(&defs);

    NUNetSet::iterator iter=defs.begin();
    NU_ASSERT(iter!=defs.end(), assign);
    NUNet * def = (*iter);
    ++iter;
    NU_ASSERT(iter==defs.end(), assign);

    (*net_assigns)[def].push_back(assign);
  }
}


bool TernaryGateOptimization::consistentAssigns(const NUContAssignList & assigns)
{
  UInt32 processed_assigns = 0;

  NUNetRefSet seen_defs(mNetRefFactory);
  NUNetRefSet seen_uses(mNetRefFactory);
  for (NUContAssignList::const_iterator iter=assigns.begin();
       iter != assigns.end();
       ++iter) {
    const NUContAssign * assign = (*iter);
    ++processed_assigns;

    NUNetRefSet defs(mNetRefFactory);

    assign->getDefs(&defs);
    assign->getUses(&seen_uses);

    if (NUNetRefSet::set_has_intersection(defs,seen_uses)) {
      // defs overlap uses. potential cycle between assigns. avoid
      // this group of assigns.
      return false;
    }

    if (NUNetRefSet::set_has_intersection(defs,seen_defs)) {
      // multiple defs to same netref; avoid this group of assigns.
      return false;
    }
    seen_defs.insert(defs.begin(),defs.end());
  }
  return (processed_assigns > 1);
}


/*!
  Determine the number of expression nodes contained under an entry
  expression.
*/
class NodeCounter : public NUDesignCallback
{
public:
  NodeCounter() :
    NUDesignCallback(),
    mUniqueNodes(0),
    mNodes(0)
  {}

  ~NodeCounter() {}
  
  //! By default, walk through everything.
  Status operator()(Phase, NUBase * ) { return eNormal; }
  
  //! Count nodes during expression handling.
  Status operator()(Phase phase, NUExpr * expr);

  //! The number of unique expression nodes.
  UInt32 mUniqueNodes;

  //! The number of expression nodes in a tree representation.
  UInt32 mNodes;
private:

  typedef UtMap<const NUExpr*,UInt32> ExprNodeCount;

  /*!
    Number of sub-nodes below a given expression.

    We remember the number of sub-nodes to avoid re-traversing the
    same subexpression each time it is encountered.
   */
  ExprNodeCount mExprNodeCount;
};


NUDesignCallback::Status NodeCounter::operator()(Phase phase, NUExpr * expr)
{
  // On the way down the tree (ePre), we have one of two situations:

  // 1. We have not seen this expression before. Here, we save the
  //    current number of tree-nodes encountered. We will subtract
  //    this starting count from the tree-node count in the post step.
  //    The sub-tree is walked normally.

  // 2. We have seen this expression before. The information in
  //    mExprNodeCount is accurate; add this count to the ongoing
  //    tree-node count. The sub-tree is skipped.
  //
  //    Skipping the sub-tree means we do not need to match this
  //    scenario in our ePost processing.

  // When exiting the recursion (ePost), we only have one situation:

  // 1. We are exiting the recursion corresponding to the first visit
  //    of this node. Subtract the saved tree-node count from the
  //    current tree-node count. The result is the tree-node count for
  //    this expression.

  if (phase==ePre) {
    ExprNodeCount::iterator location = mExprNodeCount.find(expr);
    if (location == mExprNodeCount.end()) {
      mExprNodeCount[expr] = mNodes; // remember the starting point.
      ++mNodes;
      ++mUniqueNodes;
    } else {
      mNodes += location->second;
      return eSkip; // we already know the size of this expression.
    }
  } else {
    mExprNodeCount[expr] = mNodes - mExprNodeCount[expr];
  }
  return eNormal;
}


bool TernaryGateOptimization::blowsup(const NUExpr * bddExpr)
{
  NodeCounter counter;
  NUDesignWalker walker(counter,false);
  walker.expr(const_cast<NUExpr*>(bddExpr));

  // Exclude anything which causes more than a factor of 100 increase in size.
  return (counter.mNodes > (100*counter.mUniqueNodes));
}


//! Stop the walk if any wide expressions encountered.
class WideExpressionDiscovery : public NUDesignCallback
{
public:
  //! Constructor.
  WideExpressionDiscovery() : 
    NUDesignCallback(),
    mSizeLimit(2)
  {}

  //! Destructor
  ~WideExpressionDiscovery () {}

  //! By default, walk through everything.
  Status operator()(Phase, NUBase * ) { return eNormal; }

  //! Process all expression types except var- and mem-sel; abort walk if large size.
  Status operator()(Phase, NUExpr * expr) {
    if (expr->getBitSize() > mSizeLimit) {
      return eStop;
    } else {
      return eNormal;
    }
  }

  //! Specialize varsel handling to prevent walking of contained identifier.
  Status operator()(Phase, NUVarselRvalue * varsel) {
    if (varsel->getBitSize() > mSizeLimit) {
      return eStop;
    } else {
      return eSkip; // do not walk over identifiers under varsels.
    }
  }

  //! Specialize memsel handling to prevent walking of contained identifier.
  Status operator()(Phase, NUMemselRvalue * memsel) {
    if (memsel->getBitSize() > mSizeLimit) {
      return eStop;
    } else {
      return eSkip; // do not walk over identifiers under memsels.
    }
  }

private:
  // The widest allowed expression.
  UInt32 mSizeLimit;
};


bool TernaryGateOptimization::wideExpressions(const NUExpr * expr)
{
  WideExpressionDiscovery findWide;
  NUDesignWalker walker(findWide,false);
  NUDesignCallback::Status status = walker.expr(const_cast<NUExpr*>(expr));
  return (status!=NUDesignCallback::eNormal);
}
