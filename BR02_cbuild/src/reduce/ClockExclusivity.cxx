// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"

#include "flow/Flow.h"
#include "flow/FLNode.h"
#include "flow/FLFactory.h"

#include "bdd/BDD.h"

#include "reduce/Fold.h"

#include "exprsynth/ExprFactory.h"
#include "exprsynth/ESPopulateExpr.h"
#include "exprsynth/ExprResynth.h"
#include "exprsynth/ExprCallbacks.h"
#include "flow/FlowClassifier.h"

#include "ClockExclusivity.h"

REClockExclusivity::REClockExclusivity(FLNodeFactory* flowFactory,
                                       NUNetRefFactory* netRefFactory,
                                       AtomicCache* strCache,
                                       MsgContext* msgContext, IODBNucleus* iodb,
                                       ArgProc* args) :
  mFlowFactory(flowFactory), mNetRefFactory(netRefFactory), mModule(NULL)
{
  mClockGateFlopCache = new ClockGateFlopCache;
  mClockGateLatchCache = new ClockGateLatchCache;
  mBDDContext = new BDDContext;
  mDeviceEnables = new DeviceEnables;
  mDevices = new FLNodeSet;
  mClocks = new NUNetRefSet(netRefFactory);
  ESFactory* esFactory = new ESFactory;
  mPopulateExpr = new ESPopulateExpr(esFactory, netRefFactory);
  mExprResynth = new ExprResynth(ExprResynth::eStopAtScalar,
				 mPopulateExpr,
				 netRefFactory,
				 args,
				 msgContext,
				 strCache,
				 iodb);
  mExprFactory = new NUExprFactory;
  mExprResynth->putExprFactory(mExprFactory);
  mFold = new Fold(netRefFactory, args, msgContext, strCache, iodb, false,
                   eFoldUDKiller);
}

//! Class to hold information about a sequential clock
class REClockExclusivity::ClockGateInfo
{
public:
  //! constructor for initial info
  ClockGateInfo() :
    mValid(false), mClkExpr(NULL), mEnables(NULL)
  {}

  //! destructor
  ~ClockGateInfo() { delete mEnables; }

  //! Add valid gate info
  void addValidGateInfo(const NUExpr* clkExpr, const NUNetRefHdl& rootClkRef, 
                        const FLNodeVector& enables)
  {
    INFO_ASSERT(!mValid, "Cannot add gate info to a populated class");
    mValid = true;
    mRootClkRef = rootClkRef;
    mClkExpr = clkExpr;
    mEnables = new FLNodeVector(enables);
  }

  //! Get whether this is a valid clock gating value
  bool isValid(void) const { return mValid; }

  //! Get the new condition
  const NUExpr* getClockExpr(void) const
  {
    INFO_ASSERT(mValid, "Grabbing new clock on invalid gating condition");
    return mClkExpr;
  }

  //! Get the root clock from the expression tree
  const NUNetRefHdl& getRootClock(void) const { return mRootClkRef; }

  //! Iterate over the enables for the gated clock
  FLNodeVectorCLoop loopEnables(void) const
  {
    INFO_ASSERT(mValid, "Grabbing gate enable on invalid gating condition");
    return CLoop<FLNodeVector>(*mEnables);
  }

private:
  //! The valid boolean
  bool mValid;

  //! The gated clock expression
  const NUExpr* mClkExpr;

  //! The root clock found when walking the clock expression
  NUNetRefHdl mRootClkRef;

  //! The set of enable conditions (flops and latches)
  FLNodeVector* mEnables;
}; // class REClockExclusivity::ClockGateInfo

REClockExclusivity::~REClockExclusivity()
{
  for (LoopMap<ClockGateFlopCache> l(*mClockGateFlopCache); !l.atEnd(); ++l) {
    ClockGateInfo* clockGateInfo = l.getValue();
    delete clockGateInfo;
  }
  for (LoopMap<ClockGateLatchCache> l(*mClockGateLatchCache); !l.atEnd(); ++l) {
    ClockGateInfo* clockGateInfo = l.getValue();
    delete clockGateInfo;
  }
  delete mClockGateFlopCache;
  delete mClockGateLatchCache;
  delete mDeviceEnables;
  delete mDevices;
  delete mBDDContext;
  delete mClocks;
  delete mExprResynth;
  delete mPopulateExpr->getFactory();
  delete mPopulateExpr;
  delete mFold;
  delete mExprFactory;
}

void REClockExclusivity::init(NUModule* mod, FLNodeVector* flops)
{
  // Set up the currently processed module, this is not recallable
  NU_ASSERT(mModule == NULL, mod);
  mModule = mod;

  // Visit all unelaborated flow in this module. We then look for
  // latches, flops and clocks.
  NUUseDefSet covered;
  for (FLNodeFactory::iterator i = mFlowFactory->begin();
       i != mFlowFactory->end();
       ++i) {
    // Check if this is a flop or latch. It must be a continous driver
    // for that to be true.
    FLNode* flow = *i;
    NUUseDefNode* useDef = flow->getUseDefNode();
    if ((useDef != NULL) && useDef->isContDriver()) {
      if (useDef->isSequential()) {
        // Flop, get the clock net and condition under which the flop
        // is active
        NUNetRefHdl clkNetRef;
        bool invert;
        const NUExpr* cond = getFlopClock(flow, &clkNetRef, &invert);

        // Add the clock and device
        mClocks->insert(clkNetRef);
        addDevice(flow, cond, clkNetRef, invert);
        if (flops != NULL) {
          flops->push_back(flow);
        }

      } else if (flow->getDefNet()->isLatch()) {
        // Get the enable and treat it like a clock. If we can't get
        // the latch enable then we don't treat it like a
        // clock. Remember that the latch marking is pessimistic.
        NUNetRefHdl clkNetRef;
        bool invert;
        const NUExpr* cond = getLatchEnable(flow, &clkNetRef, &invert);
        if (cond != NULL) {
          if (invert) {
            cond = invertClockExpr(cond);
          }

          addDevice(flow, cond, clkNetRef, invert);
          mClocks->insert(clkNetRef);
        }
      }
    } // if
  } // ++i)
} // void REClockExclusivity::findDesignComponents

bool REClockExclusivity::isValidDevice(FLNode* flow)
{
  DeviceEnables::iterator pos = mDeviceEnables->find(flow);
  return (pos != mDeviceEnables->end());
}

const NUExpr*
REClockExclusivity::getDeviceEnable(FLNode* flow, NUNetRefHdl* clkNetRef,
                                    bool* invert)
{
  Enable enable = getDeviceEnable(flow);
  if (clkNetRef != NULL) {
    *clkNetRef = enable.getClockNetRef();
  }
  if (invert != NULL) {
    *invert = enable.getInvert();
  }
  return enable.getExpr();
}

const NUExpr*
REClockExclusivity::getDeviceEnableExpr(FLNode* flow, FLNodeVector* enables,
                                        NUNetRefHdl* rootClkRef)
{
  // Use the right routine to do this, flops and latches work differently
  const NUExpr* enExpr;
  NUUseDefNode* useDef = flow->getUseDefNode();
  if (useDef->isSequential()) {
    enExpr = getFlopClockExpr(flow, enables, rootClkRef);
  } else {
    enExpr = getLatchEnableExpr(flow, enables, rootClkRef);
  }

  // Sort the enables so we get stable results
  if (enables != NULL) {
    std::sort(enables->begin(), enables->end(), FLNodeCmp());
  }
  return enExpr;
}

const NUExpr* 
REClockExclusivity::getFlopClock(FLNode* flow, NUNetRefHdl* clkNetRef,
                                 bool* invert)
{
  // Get the edge expression
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  NU_ASSERT(always != NULL, useDef);
  NUEdgeExpr* edgeExpr = always->getEdgeExpr();

  // Get the clock net if they need it
  if (clkNetRef != NULL) {
    *clkNetRef = mNetRefFactory->createNetRef(edgeExpr->getNet());
  }

  // Create a condition, if it is posedge then the existing expression
  // is fine. Otherwise, invert it.
  CopyContext cntxt(0,0);
  const NUExpr* cond = mExprFactory->insert(edgeExpr->getExpr()->copy(cntxt));
  if (edgeExpr->getEdge() == eClockNegedge) {
    cond = invertClockExpr(cond);
    *invert = true;
  } else {
    *invert = false;
  }
  return cond;
}

FLNode* REClockExclusivity::findIfFlow(FLNode* flow)
{
  FLNode* ifFlow = NULL;
  FLNode* nested = flow;
  while ((nested != NULL) && (ifFlow == NULL) &&
         ((nested = nested->getSingleNested()) != NULL)) {
    switch (nested->getUseDefNode()->getType()) {
      case eNUBlock:
        // This is a valid nesting
        break;

      case eNUIf:
        // Found the if statement, we will stop
        ifFlow = nested;
        break;

      default:
        // Everything else causes us to give up
        nested = NULL;
        break;
    }
  }
  if (ifFlow == NULL) {
    return NULL;
  }
  return ifFlow;
} // FLNode* REClockExclusivity::findIfFlow

const NUExpr*
REClockExclusivity::getLatchEnable(FLNode* flow, NUNetRefHdl* clkNetRef,
                                   bool* invert)
{
  // Get the if statement flow node
  FLNode* ifFlow = findIfFlow(flow);
  if (ifFlow == NULL) {
    return NULL;
  }

  // If we found the if statement, Make sure that at least one of the
  // then or else is empty and that we have only one def for the latch
  // net.
  FLNodeVector thenFlows;
  FLIfClassifier<FLNode> ifClassifier(ifFlow);
  for (FLIfClassifier<FLNode>::FlowLoop l = ifClassifier.loopThen(ifFlow);
       !l.atEnd(); ++l) {
    FLNode* thenFlow = *l;
    thenFlows.push_back(thenFlow);
  }
  if (thenFlows.size() > 1) {
    return NULL;
  }

  // Same with else flows
  FLNodeVector elseFlows;
  for (FLIfClassifier<FLNode>::FlowLoop l = ifClassifier.loopElse(ifFlow);
       !l.atEnd(); ++l) {
    FLNode* elseFlow = *l;
    elseFlows.push_back(elseFlow);
  }
  if (elseFlows.size() > 1) {
    return NULL;
  }

  // If both then and else def the net, then give up
  if (!thenFlows.empty() && !elseFlows.empty()) {
    return NULL;
  }

  // Grab the if condition and get the net uses. There has to be a
  // single scalar use.
  NUIf* ifStmt = dynamic_cast<NUIf*>(ifFlow->getUseDefNode());
  FLN_ASSERT(ifStmt != NULL, ifFlow);
  NUExpr* cond = ifStmt->getCond();
  NUNetRefSet uses(mNetRefFactory);
  cond->getUses(&uses);
  if (uses.empty() || (uses.size() > 1)) {
    return NULL;
  }
  NUNetRefHdl netRef = *(uses.begin());
  if (netRef->getNumBits() != 1) {
    // Only handle single bit clocks
    return NULL;
  }

  // If they asked for the clock net return it
  if (clkNetRef != NULL) {
    *clkNetRef = netRef;
  }

  // Make a copy of the condition and return whether it should be inverted
  CopyContext cntxt(0,0);
  NUExpr* newCond = cond->copy(cntxt);
  *invert = thenFlows.empty();
  return mExprFactory->insert(newCond);
} // REClockExclusivity::getLatchEnable

void 
REClockExclusivity::addDevice(FLNode* flow, const NUExpr* cond,
                              const NUNetRefHdl& clkNetRef, bool invert)
{
  // We can't have visited the same flow twice, we do a factory walk
  FLN_ASSERT(mDeviceEnables->find(flow) == mDeviceEnables->end(), flow);

  // Add it to the map and set
  Enable enable(cond, clkNetRef, invert);
  mDeviceEnables->insert(DeviceEnables::value_type(flow, enable));
  mDevices->insert(flow);
}

void
REClockExclusivity::replaceDeviceEnable(FLNode* flow, const NUExpr* cond,
                                        const NUNetRefHdl& clkNetRef, bool invert)
{
  // Make a copy of the condition and put it in our factory
  CopyContext cntxt(0,0);
  const NUExpr* newCond = mExprFactory->insert(cond->copy(cntxt));

  // Find the location
  DeviceEnables::iterator pos = mDeviceEnables->find(flow);
  FLN_ASSERT(pos != mDeviceEnables->end(), flow);

  // Replace it
  pos->second = Enable(newCond, clkNetRef, invert);
}

REClockExclusivity::Enable REClockExclusivity::getDeviceEnable(FLNode* flow)
{
  DeviceEnables::iterator pos = mDeviceEnables->find(flow);
  FLN_ASSERT(pos != mDeviceEnables->end(), flow);
  return pos->second;
}

class SynthCallback : public ESCallback
{
public:
  //! constructor
  SynthCallback(ESPopulateExpr* populateExpr, const FLNodeSet& devices,
                const NUNetRefSet& clocks, FLNode* flow, NUNet* net) : 
    ESCallback(populateExpr), mDevices(devices), mClocks(clocks),
    mStartFlow(flow), mStartNet(net)
  {}

  //! Function to test whether we should stop or not at this flow node
  bool stop(FLNode* flow, bool contDriver)
  {
    // Stop at devices; even if it is the start flow
    NUNetRefHdl netRef = flow->getDefNetRef();
    if (mDevices.find(flow) != mDevices.end()) {
      return true;
    }

    // If it is what we started with or a nested flow, continue
    if ((flow == mStartFlow) || !contDriver) {
      return false;
    }

    return false;
  }

private:
  //! Hide copy constructor
  SynthCallback(const SynthCallback&);

  //! Hide assign constructor
  SynthCallback& operator=(const SynthCallback&);

  //! The flops in the design
  const FLNodeSet& mDevices;

  //! The clocks in the design
  const NUNetRefSet& mClocks;

  //! The flow that started this walk
  FLNode* mStartFlow;

  //! The clock net we started with (for when we process if conditions)
  NUNet* mStartNet;
}; // class SynthCallback : public ESCallback

const NUExpr*
REClockExclusivity::getClockNetExpr(const NUNetRefHdl& clkNetRef)
{
  // Get the clock gating information for this net
  ClockGateInfo* clkGateInfo = computeClockGateInfo(clkNetRef);

  // Return the clock expression if one was found
  if (clkGateInfo->isValid()) {
    return clkGateInfo->getClockExpr();
  } else {
    return NULL;
  }
}

const NUExpr* REClockExclusivity::invertClockExpr(const NUExpr* clkExpr)
{
  const NUExpr* subExpr;
  if (clkExpr->isManaged()) {
    subExpr = clkExpr;
  } else {
    CopyContext cntxt(0,0);
    subExpr = mExprFactory->insert(clkExpr->copy(cntxt));
  }
  NUExpr* cond = new NUUnaryOp(NUOp::eUnBitNeg, subExpr, clkExpr->getLoc());
  cond->resize(1);
  return mExprFactory->insert(cond);
}

const NUExpr*
REClockExclusivity::getFlopClockExpr(FLNode* flop, FLNodeVector* enables,
                                     NUNetRefHdl* rootClkRef)
{
  // Get the device enable information
  Enable enable = getDeviceEnable(flop);
  NUNetRefHdl clkNetRef = enable.getClockNetRef();
  bool invert = enable.getInvert();

  // Get the clock gating information for this net
  ClockGateInfo* clkGateInfo = computeClockGateInfo(clkNetRef);

  // Check if we couldn't create an expression
  if (!clkGateInfo->isValid()) {
    return NULL;
  }

  // If they asked for the root clock, return it
  if (rootClkRef != NULL) {
    *rootClkRef = clkGateInfo->getRootClock();
  }

  // We have valid data, return it
  if (enables != NULL) {
    for (FLNodeVectorCLoop l = clkGateInfo->loopEnables(); !l.atEnd(); ++l) {
      FLNode* flow = *l;
      enables->push_back(flow);
    }
  }

  // Return the new clock expression, if the original expression was
  // inverted, we have to invert this.
  const NUExpr* retExpr = clkGateInfo->getClockExpr();
  if (invert) {
    retExpr = invertClockExpr(retExpr);
  }
  return retExpr;
} // REClockExclusivity::getFlopClockExpr

const NUExpr*
REClockExclusivity::getLatchEnableExpr(FLNode* flow, FLNodeVector* enables,
                                       NUNetRefHdl* rootClkRef)
{
  // Get the device enable information
  Enable enable = getDeviceEnable(flow);
  NUNetRefHdl clkNetRef = enable.getClockNetRef();
  const NUExpr* cond = enable.getExpr();
  bool invert = enable.getInvert();

  // Check if this is in the cache yet
  ClockGateInfo* clkGateInfo;
  ClockGateLatchCache::iterator pos = mClockGateLatchCache->find(cond);
  if (pos != mClockGateLatchCache->end()) {
    clkGateInfo = pos->second;

  } else {
    // Start with an invalid clock gate info
    clkGateInfo = new ClockGateInfo;

    // Find the if statement flow node
    FLNode* ifFlow = findIfFlow(flow);
    if (ifFlow != NULL) {
      // Create an expression for the if condition
      NUNet* clkNet = clkNetRef->getNet();
      SynthCallback callback(mPopulateExpr, *mDevices, *mClocks, flow, clkNet);
      ESExprHndl hndl = mPopulateExpr->createIfCondition(ifFlow, &callback);

      // Convert the expression to clock gate information
      processExpression(clkNetRef, clkNetRef, hndl.getExpr(), clkGateInfo);
    } // if

    // Add it to the cache and return it
    ClockGateLatchCache::value_type val(cond, clkGateInfo);
    mClockGateLatchCache->insert(val);
  } // } else
    
  // Check if we couldn't create an expression
  if (!clkGateInfo->isValid()) {
    return NULL;
  }

  // We have valid data, return it
  if (enables != NULL) {
    for (FLNodeVectorCLoop l = clkGateInfo->loopEnables(); !l.atEnd(); ++l) {
      FLNode* flow = *l;
      enables->push_back(flow);
    }
  }

  // If they asked for the root clock, return it
  if (rootClkRef != NULL) {
    *rootClkRef = clkGateInfo->getRootClock();
  }

  // Return the new clock expression, if the original expression was
  // inverted, we have to invert this.
  const NUExpr* retExpr = clkGateInfo->getClockExpr();
  if (invert) {
    retExpr = invertClockExpr(retExpr);
  }
  return retExpr;
}

ConstantRange
REClockExclusivity::normalizeBitSel(const NUNetRefHdl& netRef,
                                    const NUNetRefHdl& subNetRef)
{
  // The outer net ref must cover the sub net ref and the sub net ref
  // must be a single bit. Also, the sizes of the underlying nets must
  // match. Otherwise the code below doesn't work.
  NU_ASSERT(netRef->covers(*subNetRef), netRef->getNet());
  NU_ASSERT(subNetRef->getNumBits() == 1, subNetRef->getNet());
  NU_ASSERT2(netRef->getNet()->getBitSize() == subNetRef->getNet()->getBitSize(),
             netRef->getNet(), subNetRef->getNet());

  // Get the usage mask for the two net refs to find the offset of the
  // bit select into the netRef. The netRef may not be contiguous or
  // start at 0.
  DynBitVector mask;
  DynBitVector subMask;
  netRef->getUsageMask(&mask);
  subNetRef->getUsageMask(&subMask);
  
  // Walk the two masks until we find the offset of the first
  // overlaping bit. This means we skip holes in netRef so that we
  // find the offset of the subNetRef into the netRef.
  bool found = false;
  SInt32 offset = 0;
  for (UInt32 i = 0; (i < mask.size() && !found); ++i) {
    if (mask.test(i)) {
      // The fuller netref references this bit
      if (subMask.test(i)) {
        // We found the overlap
        found = true;
      } else {
        // not yet found, move to the next offset
        offset++;
      }
    }
  }

  // We should have found a valid offset or else something is really
  // wrong
  NU_ASSERT(found, netRef->getNet());
  return ConstantRange(offset, offset);
} // REClockExclusivity::normalizeBitSel

void
REClockExclusivity::processExpression(const NUNetRefHdl& exprNetRef,
                                      const NUNetRefHdl& clkNetRef,
                                      CarbonExpr* carbonExpr,
                                      ClockGateInfo* clkGateInfo)
{
  // The carbon expression should be valid or there is nothing to do.
  if (carbonExpr == NULL) {
    return;
  }

  // Convert the Carbon Expression to a Nucleus expression (to do
  // BDD operations)
  NUNet* clkNet = clkNetRef->getNet();
  const SourceLocator& loc = clkNet->getLoc();
  const NUExpr* expr = mExprResynth->exprToNucleus(carbonExpr, mModule, loc);
  if (exprNetRef->getNumBits() != 1) {
    // The clock is a bit select. We need to add that to the
    // expression for it to be valid
    ConstantRange range = normalizeBitSel(exprNetRef, clkNetRef);
    NUExpr* newExpr = mFold->createPartsel(expr, range, loc);
    expr = mExprFactory->insert(newExpr);
  }

  // Gather the clock tree root clock and enables
  FLNodeVector devicesFound;
  NUNetRefHdl rootClkRef;
  gatherClocksAndEnables(expr, &rootClkRef, &devicesFound);

  // Add the clock gate information
  clkGateInfo->addValidGateInfo(expr, rootClkRef, devicesFound);
} // REClockExclusivity::processExpression

void
REClockExclusivity::gatherClocksAndEnables(const NUExpr* expr, 
                                           NUNetRefHdl* rootClkRef,
                                           FLNodeVector* devicesFound)
{
  // Use the expression uses to gather the leafs of the expression. We
  // then try to match the leafs up with clocks or enables.
  NUNetRefSet uses(mNetRefFactory);
  expr->getUses(&uses);
  bool clkValid = true;
  bool enablesValid = true;
  for (NUNetRefSet::iterator i = uses.begin(); i != uses.end() && clkValid; ++i) {
    // Check if it is a clock or an enable
    const NUNetRefHdl& use = *i;
    if (mClocks->find(use, &NUNetRef::overlapsSameNet) != mClocks->end()) {
      // It is a clock, if this is the first one we found it is valid.
      // If we found more than one clock or this is a multi-bit use,
      // give up.
      if (use->getNumBits() > 1) {
        // Multi-bit use. We might have a situation like: (clk[1] &
        // (en & clk[0])) and are not currently differentiating
        // between clock and non-clock portions [bug6410].
        clkValid = false;
        *rootClkRef = NUNetRefHdl();        
      } else if (!rootClkRef->isValid()) {
        // First time
        *rootClkRef = use;
      } else if (use != *rootClkRef) {
        clkValid = false;
        *rootClkRef = NUNetRefHdl();
      }
    } else {
      // It must be an enable, but ignore initial blocks etc
      for (NUNetRef::DriverLoop l = use->loopInclusiveDrivers();
           !l.atEnd() && enablesValid; ++l) {
        FLNode* flow = *l;
        if (isActiveDriver(flow)) {
          if (mDevices->find(flow) != mDevices->end()) {
            devicesFound->push_back(flow);
          } else {
            // Not sure what we found, have to give up
            enablesValid = false;
            devicesFound->clear();
          }
        }
      }
    }
  } // for
} // REClockExclusivity::gatherClocksAndEnables

bool REClockExclusivity::isActiveDriver(FLNode* flow)
{
  // Ignore initial blocks
  NUUseDefNode* useDef = flow->getUseDefNode();
  if ((useDef != NULL) && useDef->isInitial()) {
    return false;
  }

  // Ignore pull drivers
  if ((useDef != NULL) && (useDef->getStrength() == eStrPull)) {
    return false;
  }

  // Everything else must be ok
  return true;
}

bool
REClockExclusivity::exclusiveExprs(const NUExpr* expr1, const NUExpr* expr2)
{
  // Make sure we can build bdds
  if (!expr1->hasBdd(mBDDContext) || !expr2->hasBdd(mBDDContext)) {
    return false;
  }

  // Create BDD's for the two expressions
  BDD bdd1 = expr1->bdd(mBDDContext);
  BDD bdd2 = expr2->bdd(mBDDContext);

  // Check if ANDing the two together results in 0. That means they
  // both can't be high at the same time.
  BDD result = mBDDContext->opAnd(bdd1, bdd2);
  return result == mBDDContext->val0();
}

REClockExclusivity::ClockGateInfo*
REClockExclusivity::computeClockGateInfo(const NUNetRefHdl& clkNetRef)
{
  // Check if this is in the cache yet
  ClockGateInfo* clkGateInfo;
  ClockGateFlopCache::iterator pos = mClockGateFlopCache->find(clkNetRef);
  if (pos != mClockGateFlopCache->end()) {
    clkGateInfo = pos->second;

  } else {
    // Gather the drivers for this clk net
    FLNodeVector flows;
    for (NUNetRef::DriverLoop l = clkNetRef->loopInclusiveDrivers();
         !l.atEnd(); ++l) {
      FLNode* flow = *l;
      if (isActiveDriver(flow)) {
        flows.push_back(flow);
      }
    }

    // Make sure there is only one driver for the clock
    clkGateInfo = new ClockGateInfo;
    if (flows.size() == 1) {
      // Get the driver an create an expression for it that goes back to
      // latch, flops and clocks. Note that this is a local walk
      // only. If multiple different input ports are connected to the
      // same net, this will not find it.
      FLNode* flow = flows.back();
      SynthCallback callback(mPopulateExpr, *mDevices, *mClocks, flow, NULL);
      ESExprHndl hndl = mPopulateExpr->create(flow, &callback);

      // Convert the expression to clock gate information. This is the
      // same code for flops and latches.
      NUNetRefHdl flowNetRef = flow->getDefNetRef();
      processExpression(flowNetRef, clkNetRef, hndl.getExpr(), clkGateInfo);
    }

    // Add it to the cache and return it
    ClockGateFlopCache::value_type val(clkNetRef, clkGateInfo);
    mClockGateFlopCache->insert(val);
  } // } else

  return clkGateInfo;
} // REClockExclusivity::computeClockGateInfo

bool REClockExclusivity::getLatchDataExpr(FLNode* flow, 
                                          const NUExpr** dataExpr,
                                          const NUExpr** latchOut)
{
  // Make sure it is a latch. We may still not analyze it correctly
  // below, but it at least has to be marked as a latch.
  if (!flow->getDefNet()->isLatch()) {
    return false;
  }

  // Get the device enable information
  Enable enable = getDeviceEnable(flow);

  // Find the statement flow node from within the if statement
  // flow. Use that to create the data expression.
  FLNode* ifFlow = findIfFlow(flow);
  FLNode* stmtFlow = ifFlow->getSingleNested();
  bool found = false;
  if (stmtFlow != NULL) {
    // We have a single statement in the if. Create an expression back
    // to continuous drivers.
    ContinuousDriverSynthCallback callback(mPopulateExpr, stmtFlow);
    ESExprHndl hndl = mPopulateExpr->create(stmtFlow, &callback);

    // Convert the Carbon Expression to a Nucleus expression (to do
    // BDD operations)
    NUUseDefNode* useDef = stmtFlow->getUseDefNode();
    const SourceLocator& loc = useDef->getLoc();
    *dataExpr = mExprResynth->exprToNucleus(hndl.getExpr(), mModule, loc);

    // Get the latch output expression
    NUBlockingAssign* assign = dynamic_cast<NUBlockingAssign*>(useDef);
    if (assign != NULL) {
      NUExpr* latchExpr = assign->getLvalue()->NURvalue();
      latchExpr->resize (assign->getLvalue ()->getBitSize ());
      *latchOut = mExprFactory->insert(latchExpr);
      found = true;
    }
  }

  return found;
} // const NUExpr* REClockExclusivity::getLatchDataExpr

    
