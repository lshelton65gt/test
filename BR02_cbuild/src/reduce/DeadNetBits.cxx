// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Optmiziation to remove and dead net bits
*/

#include "util/Loop.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUSysTask.h"

#include "flow/Flow.h"
#include "flow/FLNode.h"
#include "flow/FLIter.h"

#include "reduce/RewriteUtil.h"
#include "reduce/DeadNetBits.h"
#include "reduce/Fold.h"

REDeadNetBits::REDeadNetBits(NUNetRefFactory* netRefFactory,
                             IODBNucleus* iodb,
                             AtomicCache* strCache,
                             ArgProc* args,
                             MsgContext* msgContext) :
  mNetRefFactory(netRefFactory), mIODB(iodb)
{
  mFold = new Fold(netRefFactory, args, msgContext, strCache, iodb, false,
                   eFoldUDKiller|eFoldNoNetConstants);
}

REDeadNetBits::~REDeadNetBits()
{
  delete mFold;
}

bool REDeadNetBits::module(NUModule* module)
{
  // Find all used bits in this module. We do this by doing a flow
  // walk and recording all uses. This starts with all module outputs
  // including hier ref read nets.
  NUNetRefSet usedBits(mNetRefFactory);
  findUsedBits(module, &usedBits);

  // Optimize the nets that are not read. We take all nets and
  // subtract them from the used nets.
  return removeDeadBits(module, usedBits);
}

void REDeadNetBits::findUsedBits(NUModule* module, NUNetRefSet* usedBits)
{
  // Start by adding all starting nets to the used bits set. They
  // aren't necessarily in a usedef node' used set.
  NUNetList startNets;
  module->getFaninStartNets(&startNets, mIODB);
  for (Loop<NUNetList> l(startNets); !l.atEnd(); ++l) {
    NUNet* net = *l;
    NUNetRefHdl netRef = mNetRefFactory->createNetRef(net);
    usedBits->insert(netRef);
  }

  // Also walk all port connection bids/outputs and add any
  // actuals. This is because port lowering bids afterwards can cause
  // port splitting issues. Also, there is no benefit to breaking up
  // outputs because they formal cannot be broken up. We would port
  // lower them anyway.
  for (NUModuleInstanceMultiLoop l = module->loopInstances(); !l.atEnd(); ++l) {
    NUModuleInstance* instance = *l;
    NUPortConnectionLoop p;
    for (p = instance->loopPortConnections(); !p.atEnd(); ++p) {
      NUPortConnection* port = *p;
      NUType type = port->getType();
      if (type == eNUPortConnectionOutput) {
        NUPortConnectionOutput* output;
        output = dynamic_cast<NUPortConnectionOutput*>(port);
        NULvalue* lvalue = output->getActual();
        lvalue->getDefs(usedBits);

      } else if (type == eNUPortConnectionBid) {
        NUPortConnectionBid* bid = dynamic_cast<NUPortConnectionBid*>(port);
        NULvalue* lvalue = bid->getActual();
        lvalue->getDefs(usedBits);
      }
    }
  }

  // System task outputs (or functions converted to tasks) may have
  // dead bits. But codegen doesn't allow a LHS concat, so add the
  // task outputs to the used set. Also add any nets used in a dynamic
  // index write.
  //
  // All qualifications that require a nucleus walk should be done
  // here. All net based qualifications can be done in the
  // removeDeadBits code.
  addInvalidOptNets(module, usedBits);

  // Walk the design adding all uses
  FLNetIter iter(&startNets,
                 FLIterFlags::eAll,                 // visit all nets
                 FLIterFlags::eNone,                // stop at no nets
                 FLIterFlags::eOverAll,             // Ignore nesting
                 FLIterFlags::eIterNodeOnce);       // visit node once
  for (; !iter.atEnd(); ++iter) {
    FLNode* flow = *iter;
    NUUseDefNode* useDef = flow->getUseDefNode();
    if (useDef != NULL) {
      const NUNetRefHdl& defNetRef = flow->getDefNetRef();
      useDef->getUses(defNetRef, usedBits);
    }
  }
} // void REDeadNetBits::findUsedBits

//! Design walker class to find all nets that cannot be optimized.
/*! All invalid nets get added as used to the net ref set. These
 *  currently include:
 *
 *  - outputs of system tasks
 *  - nets that are written with dynamic index
 */
class FindInvalidOptNets : public NUDesignCallback
{
public:
  //! constructor
  FindInvalidOptNets(NUNetRefSet* netRefs) : mNetRefs(netRefs) {}

  //! Overload the base class as a catch all
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Don't walk into sub instances
  Status operator()(Phase, NUModuleInstance*) { return eSkip; }

  //! Overload the system task callback to find outputs
  Status operator()(Phase phase, NUSysTask* sysTask)
  {
    if (phase == ePost) {
      sysTask->getBlockingDefs(mNetRefs);
    }
    return eNormal;
  }

  //! Overload the NUVarselLvalue to find dynamic index writes
  Status operator()(Phase phase, NUVarselLvalue* varselLvalue)
  {
    if (phase == ePost) {
      if (!varselLvalue->isConstIndex()) {
        varselLvalue->getDefs(mNetRefs);
      }
    }
    return eNormal;
  }

private:
  //! Place to put sys task defs
  NUNetRefSet* mNetRefs;
}; // class FindInvalidOptNets : public NUDesignCallback

void REDeadNetBits::addInvalidOptNets(NUModule* module, NUNetRefSet* usedBits)
{
  FindInvalidOptNets find(usedBits);
  NUDesignWalker walker(find, false, true);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(module);
}

//! Class to sort lvalues by range (they should not overlap)
/*! This sort should put them in concat order so it uses !lessThan in
 *  the ConstantRange class.
 */
class SortLvalues
{
public:
  //! constructor
  SortLvalues(NUNet* net) : mNet(net) {}

  //! Less than operator on two constant ranges
  bool operator()(const ConstantRange& range1, const ConstantRange& range2)
  {
    // Assert using the passed in net. They may not overlap
    NU_ASSERT(!range1.overlaps(range2), mNet);
    return !range1.lessThan(range2);
  }

private:
  //! For asserts
  NUNet* mNet;
};

bool REDeadNetBits::canOptimize(NUNet* net)
{
  // The following cannot be optimized:
  //
  // Cannot create a slice of a memory
  //
  // Port formals cannot be modified
  //
  // Depositable nets can't be optimized. Observables should not get
  // to this point but there may be other types of protected
  // observable nets that might leak through.
  //
  // It is not clear how to maintain the sign bit information after
  // breaking up the net. So it is better not to do this.
  return (!net->is2DAnything() && !net->isPort() && !net->isProtected(mIODB) &&
          !net->isSigned());
}

bool REDeadNetBits::removeDeadBits(NUModule* module, const NUNetRefSet& usedBits)
{
  // Get all the module net refs
  NUNetList allNets;
  NUNetRefSet netRefSet(mNetRefFactory);
  module->getAllNets(&allNets);
  for (Loop<NUNetList> l(allNets); !l.atEnd(); ++l) {
    NUNet* net = *l;
    NUNetRefHdl netRef = mNetRefFactory->createNetRef(net);
    netRefSet.insert(netRef);
  }

  // Subtract the two sets in place. netRefSet is now the dead bits
  NUNetRefSet::set_subtract(usedBits, netRefSet);

  // Optimized the dead bits by creating module level temps for the
  // bits we care about. We do this by creating a map from nets to a
  // new NULvalue for those nets and using the rewrite utils. Note
  // that we don't create the rvalue because, RewriteUtils does that
  // translation for us.
  NULvalueReplacementMap lvalueReplacements;
  for (NUNetRefSet::SortedLoop l = netRefSet.loopSorted(); !l.atEnd(); ++l) {
    // Only look at partial dead nets, the rest don't need to be
    // optimized. Also ignore memories and ports. The former because
    // it doesn't make sense to optimize columns and the latter
    // because it doesn't make sense to optimize module inputs (bids,
    // outputs couldn't be optimized anyway).
    const NUNetRefHdl& deadNetRef = *l;
    NUNet* net = deadNetRef->getNet();
    if (!deadNetRef->all() && canOptimize(net)) {
      createDeadBitsReplacements(deadNetRef, &lvalueReplacements);

      // Also mark the net unwavable because some bits will be dead
      net->putIsInaccurate(true);
    } // if
  } // for

  // Only modify the design if there is work to do
  if (!lvalueReplacements.empty()) {
    // Modify the design
    NUNetReplacementMap dummyNetReplacements;
    NUExprReplacementMap dummyExprReplacements;
    NUTFReplacementMap dummyTFReplacements;
    NUCModelReplacementMap dummyCModelReplacements;
    NUAlwaysBlockReplacementMap dummyAlwaysReplacements;
    RewriteLeaves translator(dummyNetReplacements, lvalueReplacements,
                             dummyExprReplacements, dummyTFReplacements,
                             dummyCModelReplacements, dummyAlwaysReplacements,
                             mFold,
                             true /* Do post walk replacement */ );
    module->replaceLeaves(translator);

    // Clean up - note the return value, changes were made
    deleteDeadBitsReplacements(lvalueReplacements);
    return true;
  }

  // No changes made
  return false;
} // void REDeadNetBits::removeDeadBits

void
REDeadNetBits::createDeadBitsReplacements(const NUNetRefHdl& deadNetRef,
                                          NULvalueReplacementMap* lvalReplacements)
{
  // Needed to create temp nets below
  NUNet* net = deadNetRef->getNet();
  NUScope* scope = net->getScope();

  // Create the individual dead lvalues for each range. Each range is
  // its own temp net.
  typedef UtMap<ConstantRange, NULvalue*, SortLvalues> LvalueParts;
  SortLvalues cmp(net);
  LvalueParts lvalueParts(cmp);
  const SourceLocator& loc = net->getLoc();
  for (NUNetRefRangeLoop r = deadNetRef->loopRanges(mNetRefFactory);
       !r.atEnd(); ++r) {
    // Create the new temp
    const ConstantRange& range = *r;
    NUNet* tempNet;
    UInt32 size = range.getLength();
    tempNet = scope->createTempSubRangeNetFromImage(net, size, "deadbits");

    // Add the lvalue to the sorted map so we can 
    NULvalue* lvalue = new NUIdentLvalue(tempNet, loc);
    lvalueParts.insert(LvalueParts::value_type(range, lvalue));
  }

  // Create the individual live lvalues for each range
  NUNetRefHdl allNetRef = mNetRefFactory->createNetRef(net);
  NUNetRefHdl liveNetRef = mNetRefFactory->subtract(allNetRef, deadNetRef);
  for (NUNetRefRangeLoop r = liveNetRef->loopRanges(mNetRefFactory);
       !r.atEnd(); ++r) {
    // Create the new temp
    const ConstantRange& range = *r;
    NUNet* tempNet;
    UInt32 size = range.getLength();
    tempNet = scope->createTempSubRangeNetFromImage(net, size, "livebits");

    // Add the lvalue to the sorted map so we can 
    NULvalue* lvalue = new NUIdentLvalue(tempNet, loc);
    lvalueParts.insert(LvalueParts::value_type(range, lvalue));
  }

  // Create a vector of lvalues from the sorted map
  NULvalueVector lvalues;
  for (LoopMap<LvalueParts> p(lvalueParts); !p.atEnd(); ++p) {
    NULvalue* lvalue = p.getValue();
    lvalues.push_back(lvalue);
  }

  // Add the lvalue concat to the map
  NULvalue* lvalue = new NUConcatLvalue(lvalues, loc);
  lvalReplacements->insert(NULvalueReplacementMap::value_type(net, lvalue));
} // REDeadNetBits::createDeadBitsReplacements

void
REDeadNetBits::deleteDeadBitsReplacements(const NULvalueReplacementMap& lvalues)
{
  for (NULvalueReplacementMap::const_iterator l = lvalues.begin();
       l != lvalues.end(); ++l) {
    NULvalue* lvalue = l->second;
    delete lvalue;
  }
}
