// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Simple constant folding pass.  Statement-oriented constant-folding
 */
#include "FoldI.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUSysTask.h"
#include "reduce/Congruent.h"
#include "CollectBlocks.h"
#include "nucleus/NUExprFactory.h"
#include <algorithm>     // for std::max

/*!
 * Remove deleted statements on \a LIST from some class object \a STMT
 * using \a removeStmt - a member function of C.  Clear the list when
 * finished.
 */
template <class C> static void
sRemoveStmts (C* stmt, void (C::*removeStmt) (NUStmt*, NUStmt *),
              NUStmtVector* list)
{
  for(NUStmtVector::iterator i = list->begin (); i != list->end (); ++i)
    (stmt->*removeStmt) ((*i), 0);

  list->clear ();
}

// Check for existance of blocks that need to be preserved
// because they may have UD information pointing to them.
// 
bool
sHasNastyBlocks (NUStmt *stmt)
{
  NUBlockSet blocks;

  CollectBlocks collector (blocks);
  collector.stmt (stmt);

  // Check the contained blocks for any net declarations;
  for (NUBlockSet::const_iterator i = blocks.begin ();
       i != blocks.end ();
       ++i)
  {
    NUNetLoop declaredNets ((*i)->loopLocals ());

    if(not declaredNets.atEnd ())
      return true;
  }

  return false;
}

bool
sHasNastyBlocks (NUStmtLoop stmts)
{
  while (not stmts.atEnd ())
  {
    if (sHasNastyBlocks (*stmts))
      return true;
  
    ++stmts;
  }

  return false;
}

// Helper functions for statement folding.  Given a list of
// statements, return a copy of either an anonymously block or
// the single statement in the list.  WARNING: SHALLOW COPIES!!!  You
// must be careful to empty-list the containing if-then-else or other
// statement-list containing object.
NUStmt*
FoldI::processStmtList (NUStmtLoop stmts, const SourceLocator& loc)
{
  NUStmtList s (stmts.begin (), stmts.end ());
  NUStmt *result;

  if (s.size () == 1) {
    // Just replace by a single statement
    result = *s.begin ();
  } else {
    // Construct a statement block to replace the if-then-else
    NUBlockScope *context = getContext (); // Containing scope

    NUBlock *block = new NUBlock (&s,
                                  context,
                                  mIODB,
                                  getFactory(),
                                  false,
                                  loc);

    // Any blocks in the statement list S must be reparented by the
    // newly contained block.
    block->adoptChildren ();
    result = block;
  }

  return result;
}

// Exchange the then- and else-stmts as we invert the conditional test
static void
sSwapIf (NUIf *ifstmt)
{
  NUStmtList *thenList = ifstmt->getThen ();
  NUStmtList *elseList = ifstmt->getElse ();
  ifstmt->replaceThen (*elseList);
  ifstmt->replaceElse (*thenList);
  delete thenList;              // not_managed_expr
  delete elseList;              // not_managed_expr
}


bool 
FoldI::removeSubBlocks(NUUseDefNode* /*useDef*/, NUStmtLoop loop,
                       NUStmtList* replacements)
{
  bool found_sub_block = false;
  NUBlockScope * parent_bs = NULL;
  for ( /*no initial*/ ; not loop.atEnd(); ++loop) {
    NUStmt * stmt = (*loop);
    NUBlock *block;
    if (stmt->getType() != eNUBlock) {
      // not a block statement
      replacements->push_back(stmt);
    } else if ((block = dynamic_cast<NUBlock*> (stmt)) == NULL || block->isSeparated ()) {
      // This subblock is tagged for code generation as a separate function so
      // leave it alone.
      replacements->push_back(stmt);
    } else {

      NUScope * parent_scope = block->getParentScope();
      NU_ASSERT(parent_scope->getScopeType()==NUScope::eTask or 
		parent_scope->getScopeType()==NUScope::eBlock, stmt);
      parent_bs = dynamic_cast<NUBlockScope*>(parent_scope);

      for (NUStmtLoop blockStmts = block->loopStmts();
           not blockStmts.atEnd();
           ++blockStmts) {
        replacements->push_back(*blockStmts);
      }

      // we can't iterate over the locals and remove them at the same
      // time.. first gather them up.
      NUNetList nets;
      for (NUNetLoop blockLocals = block->loopLocals();
           not blockLocals.atEnd();
           ++blockLocals) {
        nets.push_back((*blockLocals));
      }

      // clear out the stmt list. it's been moved to our replacement list.
      NUStmtList empty;
      block->replaceStmtList(empty);

      // Clear UD information that might be associated with this block.
      block->clearUseDef();

      // now, update their scope information.
      for (NUNetList::iterator iter = nets.begin();
           iter != nets.end();
           ++iter) {
        NUNet * net = (*iter);
        block->removeLocal(net,false);
        net->replaceScope(block, parent_bs);
        parent_bs->addLocal(net);
      }

      // There could be sub-blocks in the replacement statement list.
      // They need to be reparented to parent of the removed sub-block.
      parent_bs->adoptChildren(replacements);

      finish(block,NULL);

      found_sub_block = true;
    }
  }
  return found_sub_block;
}


//! Helper function for sReduceIfBranches
/*!
 * Move a copy of the given stmt out of the if, and just before it.
 * Create a new block stmt to hold the statements.
 * Return the new block stmt.
 */
static NUStmt* sHoistStmt(NUIf *if_stmt,
                          NUStmt *stmt,
                          void (NUIf::* remove_fn)(NUStmt*, NUStmt*),
                          NUScope *scope,
                          IODBNucleus *iodb,
                          NUNetRefFactory *factory)
{
  NUBlock *new_block = new NUBlock(0, scope, iodb, factory, false, if_stmt->getLoc());
  CopyContext copy_ctx(scope, 0);

  // Remove the stmt we want hoisted from the if, and create a copy
  // for the newly created block.
  NUStmt *hoist_copy = stmt->copy(copy_ctx);
  new_block->addStmt(hoist_copy);
  (if_stmt->*remove_fn)(stmt, 0);
  delete stmt;                  // not_managed_expr

  new_block->addStmt(if_stmt->copy(copy_ctx));
  new_block->adoptChildren();
  return new_block;
}


/*!
 * Reduce branching by converting:
 *   if (foo)
 *     a = b;
 *   else
 *     a = 0;
 * Into:
 *   a = 0;
 *   if (foo)
 *     a = b;
 */
static NUStmt* sReduceIfBranches(NUIf *if_stmt,
                                 NUNetRefFactory *factory,
                                 IODBNucleus *iodb,
                                 NUScope *scope)
{
  // Limiting scope of this optimization for now to:
  //  . just one stmt in both then and else branches
  //  . the stmt is an assign to the same lhs
  //  . one of the stmts is a constant RHS
  //
  // This can be expanded in the future to do more, if it is useful.

  // Get the stmt from the then branch.
  NUStmtLoop stmt_loop = if_stmt->loopThen();
  if (stmt_loop.atEnd()) {
    return 0;
  }
  NUStmt *then_stmt = *stmt_loop;
  ++stmt_loop;
  if (not stmt_loop.atEnd()) {
    return 0;
  }
  if (then_stmt->getType() != eNUBlockingAssign) {
    return 0;
  }

  // Get the stmt from the else branch.
  stmt_loop = if_stmt->loopElse();
  if (stmt_loop.atEnd()) {
    return 0;
  }
  NUStmt *else_stmt = *stmt_loop;
  ++stmt_loop;
  if (not stmt_loop.atEnd()) {
    return 0;
  }
  if (else_stmt->getType() != eNUBlockingAssign) {
    return 0;
  }

  // Make sure both assigns are to the same lhs.
  NUAssign *then_assign = dynamic_cast<NUAssign*>(then_stmt);
  NU_ASSERT(then_assign, then_stmt);
  NUAssign *else_assign = dynamic_cast<NUAssign*>(else_stmt);
  NU_ASSERT(else_assign, else_stmt);

  if (not (*(then_assign->getLvalue()) == *(else_assign->getLvalue()))) {
    return 0;
  }

  // Make sure there is no overlap between the assignment defs and
  // the if stmt conditional uses or the assign stmt uses.
  NUNetRefSet uses(factory);
  NUNetRefSet assign_defs(factory);
  if_stmt->getCond()->getUses(&uses);
  then_stmt->getBlockingUses(&uses);
  else_stmt->getBlockingUses(&uses);
  then_assign->getBlockingDefs(&assign_defs);
  if (NUNetRefSet::set_has_intersection(uses, assign_defs)) {
    return 0;
  }

  // Determine which assign is constant, if either is.
  if (then_assign->getRvalue()->castConst()) {
    return sHoistStmt(if_stmt,
                      then_stmt,
                      &NUIf::removeThenStmt,
                      scope,
                      iodb,
                      factory);
  } else if (else_assign->getRvalue()->castConst()) {
    return sHoistStmt(if_stmt,
                      else_stmt,
                      &NUIf::removeElseStmt,
                      scope,
                      iodb,
                      factory);
  } else {
    return 0;
  }
}

// When replacing an if-then-else by just the then-clause or else-clause,
// do the necessary cleanup here, including wrapping the stmt-list
// in a block and making sure that the unused parts of the if-stmt get
// garbage collected.
//
// Note that we use pointer-to-member functions to do the heavy lifting here.
//
// 
NUStmt* FoldI::ifHelper (NUIf* ifstmt,
                          pLoopFun looper, pReplaceFun replacer)
{
  // Pick the statements we are going to keep and put them into a block
  NUStmt* result = processStmtList ((ifstmt->*looper) (), ifstmt->getLoc ());

  result = finish (ifstmt, result);

  // Now replace them in the obsolete if-stmt by an empty list
  NUStmtList emptyList;
  (ifstmt->*replacer) (emptyList);

  return result;
}


NUStmt* 
FoldI::foldIfStmt(NUIf* ifstmt)
{
  if (isUDKillable () && not isPreservedReset ()
      && !(isUDValid () && sHasNastyBlocks (ifstmt)))
    // Look for if-then-else-if... that looks like a findbit sequence.
    // Do this *before* we recursively fold statements, or we'll
    // get confused about find-leading-bit operations and not grab the
    // whole thing
    //
    {
      NUStmt *result = ifthenFindBit (ifstmt);

      //
      // NU_ASSERT (result, ifstmt);
      //
      // if we proved that this case-statement is dead, then it's OK to have
      // ifThenFindBit return NULL.  This happens in
      //     test/cust/amcc/3700/carbon/cwtb/np3700_core : alu_odd_even.v:309
      // The select is found to be a constant by Fold, but was not eliminated
      // by local constant propagation (yet).  The constant (-1) mismatches
      // all the branches, so we reduce the case-statement to NULL.

      if (result != ifstmt)
        return result;
    }

  NUStmtVector toDelete;

  stmtList (ifstmt->loopThen (), &toDelete);
  sRemoveStmts (ifstmt, &NUIf::removeThenStmt, &toDelete);

  stmtList (ifstmt->loopElse (), &toDelete);
  sRemoveStmts (ifstmt, &NUIf::removeElseStmt, &toDelete);

  SourceLocator loc = ifstmt->getLoc();

  NUStmtList flattened_result;
  if (removeSubBlocks(ifstmt, ifstmt->loopThen(),&flattened_result)) {
    ifstmt->replaceThen(flattened_result);
  }
  flattened_result.clear();
  if (removeSubBlocks(ifstmt, ifstmt->loopElse(),&flattened_result)) {
    ifstmt->replaceElse(flattened_result);
  }
  flattened_result.clear();
  
  // Look for "if (e);"
  if (ifstmt->loopThen ().atEnd () && ifstmt->loopElse ().atEnd ())
  {
    finish (ifstmt, NULL);
    return NULL;
  }

  // Try to fold: if(e) S else S;
  // But, because reset analysis would be confused, don't do this til AFTER
  // we have the UD information and have finished the reset reductions.
  const NUIf* constIfStmt = ifstmt;
  if (isUDKillable ()
      && not isPreservedReset ()
      && not mNoFoldIfCase
      && not sHasNastyBlocks (ifstmt->loopThen ())
      && ObjListEqual<NUStmtCLoop> (constIfStmt->loopThen (),
                                    constIfStmt->loopElse ()))
  {
    NUStmt *result = ifHelper (ifstmt,
                                &NUIf::loopThen,
                                &NUIf::replaceThen);

    return result;
  }
  
  // Fold the test and the then- and else- statements
  ifstmt->replaceLeaves (*this);
  
  // Does it evaluate to TRUE or FALSE at compile time?

  NUExpr* ifCond = ifstmt->getCond();
  NUConst *c = ctce (ifCond);
  if (isUDKillable () && c)
    // condition evaluates to constant, so we can discard untaken branch
    // as long as we're willing to rerun UD information.
  {
    NUStmtList emptyList;
    NUStmt *result = ifstmt;
    
    if (isConstantX (c))
    {
#if 0
      // we're testing 'x', so pick whichever leg is cheaper
      NUCost thenCost, elseCost;
      NUCostContext costContext;
      NUStmtList *stmts = ifstmt->getThen ();
      costContext.calcStmtList (*stmts, &thenCost);
      delete stmts;             // not_managed_expr
      stmts = ifstmt->getElse ();
      costContext.calcStmtList (*stmts, &elseCost);
      delete stmts;             // not_managed_expr

      // Try and delete the cheaper - unless it declares nets, then
      // delete the other one...
      //
      if (thenCost.mInstructionsRun < elseCost.mInstructionsRun
          && not (isUDValid () && sHasNastyBlocks (ifstmt->loopElse ())))
      {
        result = ifHelper (ifstmt, &NUIf::loopThen, &NUIf::replaceThen);
      }
      else if (not (isUDValid () && sHasNastyBlocks (ifstmt->loopThen ())))
      {
        result = ifHelper (ifstmt, &NUIf::loopElse, &NUIf::replaceElse);
      }
#else
      // Delete the 'then' unless it declares nets, then delete the
      // else
      if (not (isUDValid () && sHasNastyBlocks (ifstmt->loopThen ())))
        result = ifHelper (ifstmt, &NUIf::loopElse, &NUIf::replaceElse);
      else if (not (isUDValid () && sHasNastyBlocks (ifstmt->loopElse ())))
        result = ifHelper (ifstmt, &NUIf::loopThen, &NUIf::replaceThen);
#endif
    }

    // It's okay to delete blocks if we haven't computed Flow
    // yet OR if there are no nets declared within the blocks.

    else if (not c->isZero ()
        && not (isUDValid () && sHasNastyBlocks (ifstmt->loopElse ())))
    {
      result = ifHelper (ifstmt, &NUIf::loopThen, &NUIf::replaceThen);
    }
    else if (c->isZero ()
         && not (isUDValid () && sHasNastyBlocks (ifstmt->loopThen ())))
    {
      result = ifHelper (ifstmt, &NUIf::loopElse, &NUIf::replaceElse);
    }

    return result;
  }	
  
  
  // Don't swap the legs of if-then-else until AFTER UD and
  // reset detection have been done.  Reset only understands
  // very simple situations and can't invert the edge and test
  // code
  if (testFoldFlags (eFoldPreserveReset))
    return ifstmt;
  
  // Not a constant.  Is it a negated relational comparison?
  switch (ifCond->getType ())
  {
  default:
    break;
    
  case NUExpr::eNUUnaryOp:
  case NUExpr::eNUBinaryOp:
  {
    NUOp * cond = dynamic_cast<NUOp*>(ifCond);
    NU_ASSERT(cond, ifCond);
    if (cond->getOp () == NUOp::eUnLogNot)
      // if (!e1)
    {
      // bug3539 - operand is multiple bits, test for '==0'
      // the example:
      // reg [7:0] id;
      // if (!id)  == > if (id==0)
      NUExpr * subExpr = cond->getArg (0);
      NUExpr *result;
      if (subExpr->getBitSize() != 1) {
	NUExpr *zero = NUConst::create (false, 0, subExpr->getBitSize (), 
                                        cond->getLoc () );
	result = new NUBinaryOp (NUOp::eBiEq, subExpr->copy (getCC ()), zero, cond->getLoc ());
      }
      else {
	sSwapIf (ifstmt);
	result = subExpr->copy (getCC ());
      }
      const NUExpr* managedCond = mExprCache->insert(cond);
      ifstmt->replaceCond(finish (managedCond, result)->copy(mCC));
      return ifstmt;
    }
    else if (cond->getOp () == NUOp::eBiNeq)
    {
      sSwapIf (ifstmt);
      NUExpr *result =
        new NUBinaryOp (NUOp::eBiEq, cond->getArg (0)->copy (getCC ()),
                        cond->getArg (1)->copy (getCC ()),
                        cond->getLoc ());

      const NUExpr* managedCond = mExprCache->insert(cond);
      ifstmt->replaceCond(finish (managedCond, result)->copy(mCC));
      return ifstmt;
    }
    break;
  }
  }

  // Try to combine if-then-else if sequences

  // Consider these, where S1==S2
  //
  //    if (a) S1 else if (b) S2; else S3       => if (a||b) S1; else S3;
  //
  //    if (a) S1 else if (b) S3; else S2;        => if (a || !b) S1; else S3
  //
  // Could we also do:
  //    if (a) if (b) S1; else S3; else S2; => if(!a) S2; else if(b) S1; else S3 => if (!a ||b) S1; else S3

  if (not ifstmt->emptyElse ())
  {
    NUStmtLoop elseStmts = ifstmt->loopElse ();
    NUStmt* stmtx = *elseStmts;

    ++elseStmts;

    // Check that this IF-stmt has only ONE statement in it's "else clause"
    if (stmtx && elseStmts.atEnd ())
    {
      if (stmtx->getType () == eNUIf)
      {
        // And it must be an IF-THEN-ELSE also...
        NUIf* innerIf = dynamic_cast<NUIf*>(stmtx);

        bool matchThen = ObjListEqual<NUStmtLoop> (ifstmt->loopThen (), innerIf->loopThen ()),
          matchElse = ObjListEqual<NUStmtLoop> (ifstmt->loopThen (), innerIf->loopElse ());

        if (matchThen || matchElse)
        {
          // Build new condition
          NUExpr *newCond = innerIf->getCond ()->copy (getCC ());
          if (matchElse)
            newCond = new NUUnaryOp (NUOp::eUnLogNot, newCond, newCond->getLoc ());

          newCond = new NUBinaryOp (NUOp::eBiLogOr, ifCond->copy (getCC ()), newCond,
                                      ifCond->getLoc ());
          newCond->resize (1);

          NUStmtList *thenList = ifstmt->getThen (),
            *elseList = matchElse ? innerIf->getThen () : innerIf->getElse ();

          NUStmt* newIf = new NUIf (newCond, *thenList, *elseList,
                                    getFactory (), ifstmt->getUsesCFNet (), ifstmt->getLoc ());

          delete thenList;      // not_managed_expr
          delete elseList;      // not_managed_expr

          // Tell world we've done the optimization
          newIf = finish (ifstmt, newIf);

          // Now clean up the old stmt so we don't have doubly-allocated statements
          NUStmtList emptyList;
          if (matchElse)
            innerIf->replaceThen (emptyList);
          else
            innerIf->replaceElse (emptyList);

          ifstmt->replaceThen (emptyList);
          return newIf;
        }
      }
    }
  }

  // Try to reduce branches caused by if stmts.
  // Only do this under the aggressive mode (right before codegen),
  // so that code motion, etc, is not foiled.
  if (isAggressive()) {
    NUStmt *stmt = sReduceIfBranches(ifstmt, getFactory(), mIODB, getContext());
    if (stmt) {
      return finish(ifstmt, stmt);
    }
  }

  // Give back what we started with...
  return ifstmt;
}


NUStmt*
FoldI::foldAssign(NUAssign* assign)
{
  SourceLocator loc = assign->getLoc();

  NUStmt* result = assign;
  if (assign->getType()==eNUContAssign) {
    mProcessingContinuousAssign = assign;
  }

  NUExpr* rval = assign->getRvalue();
  NULvalue* lval = assign->getLvalue ();

  NULvalue* foldedLvalue = fold(lval);
  if ((foldedLvalue != NULL) && (lval != foldedLvalue)) {
    assign->replaceLvalue(foldedLvalue, false);
    lval = foldedLvalue;
  }


  UInt32 lhs_size = lval->getBitSize();
  NU_ASSERT(lhs_size == lval->determineBitSize(), assign);
  UInt32 use_size = rval->getBitSize();
  if ( not lval->isReal() ){
    use_size = lhs_size;
  }
  NUExpr* foldedRvalue = foldResizeExpr(rval, use_size);
  if (foldedRvalue != NULL) {
    NU_ASSERT(foldedRvalue != rval, foldedRvalue);
    assign->replaceRvalue(foldedRvalue);
    rval = foldedRvalue;
  }
  

  // Look for LHS[H:-L] = RHS; ==> LHS[H:0] = RHS >> L
  //
  if (NUVarselLvalue* dest = dynamic_cast<NUVarselLvalue*>(lval)) {
    const ConstantRange &r = *dest->getRange ();
    if (dest->isConstIndex () && r.getLsb () < 0 && r.getMsb () >= 0) {
      lval = new NUVarselLvalue (dest->getLvalue ()->copy(mCC),
                                 dest->getIndex()->copy(mCC),
                                 ConstantRange (r.getMsb (), 0),
                                 loc);

      // the shift amount width is self determined so we use 32 here
      NUConst* shift_amount = NUConst::create (false, -r.getLsb (), 32, loc );
      rval = new NUBinaryOp (NUOp::eBiRshift, rval->copy(mCC), shift_amount, loc );
      // we need at least enough bits to fill the in range LHS but since this
      // is a right shift the RHS must not shrink either, use the max
      // of those widths
      rval->resize (std::max((UInt32)(r.getMsb () + 1), (UInt32)rval->determineBitSize ()));
      if (mProcessingContinuousAssign) {
        NUContAssign* cassign = dynamic_cast<NUContAssign*>(assign);
        result = new NUContAssign (cassign->getName (), lval, rval,
                                   loc, cassign->getStrength ());
      } else {
        result = new NUBlockingAssign (lval, rval, assign->getUsesCFNet (), loc);
      }
      result = finish (assign, result);
      mProcessingContinuousAssign = NULL;
      return result;
    }
  }

  if (isPreservedReset () || not isUDKillable ()
    || (isNoStmtDelete () && mProcessingContinuousAssign))
    // Only do these when it won't confuse things.  Must wait
    // til after reset recognition is done and can't do in schedule
    // (among other places!).  Also, if we've said not to delete statements
    // and we're doing continuous assigns (which have direct flow!)
    ;
  else if (isPrimary (rval))
  {
    // Look for A = A, using looser equality so that signed
    // expressions are considered as equivalent. Avoid non-blocking
    // assign statements like A <= A (Test: test/fold/bug7300.v)
    if (assign->getType() != eNUNonBlockingAssign)
    {
      NUExpr* equivLval = lval->NURvalue ();
      equivLval->resize (equivLval->determineBitSize ());
      if (equivLval->equal (*rval))
      {
        cleanupExpr (equivLval);
        finish (assign,0);
        mProcessingContinuousAssign = NULL;
        return NULL;
      }
      cleanupExpr (equivLval);
    }
  }
  // Look for {A,B}[H:L] where the selected range doesn't overlap with
  // the size of the concat!
  else if (lval->getType () == eNUVarselLvalue)
  {
    NUVarselLvalue * varsel = dynamic_cast<NUVarselLvalue*>(lval);
    if (varsel->isConstIndex ())
    {
      ConstantRange r (*varsel->getRange ());
      SInt32 lsb = r.getLsb ();
      if (lsb >= SInt32 (varsel->getLvalue ()->getBitSize ()))
      {
        finish (assign, 0);
        mProcessingContinuousAssign = NULL;
        return NULL;   // Can delete this assignment it has no results
      } else if (lsb < 0) {
        // x[5:-2] = e   => x[5:0] = e[2+:6]
        const SourceLocator& loc = lval->getLoc ();
        r.setLsb (0);
        lval = new NUVarselLvalue (varsel->getLvalue ()->copy (getCC ()), r, loc);
        
        r.adjust (-lsb);

        NUExpr* expr = createPartsel (rval, r, loc);

        if (mProcessingContinuousAssign) {
          NUContAssign* cassign = dynamic_cast<NUContAssign*>(assign);
          result = new NUContAssign (cassign->getName (), lval, rval,
                                     cassign->getLoc (), cassign->getStrength ());

        } else if (assign->getType () == eNUBlockingAssign) {
          result = new NUBlockingAssign (lval, expr, assign->getUsesCFNet (), assign->getLoc ());
        } else {
          result = new NUNonBlockingAssign (lval, expr, assign->getUsesCFNet (), assign->getLoc ());
        }
        result = finish (assign, result);
      }
    }
  }
  // Look for {A,B} = {A,B}; Avoid {A,B} <= {A,B} (Test: test/fold/bug7300_concat.v)
  else if ((assign->getType() != eNUNonBlockingAssign) && 
           (lval->getType () == eNUConcatLvalue) &&
           (rval->getType () == NUExpr::eNUConcatOp))
  {
    NUExpr *newRv = lval->NURvalue ();
    newRv->resize (rval->getBitSize ());
    if (newRv->equal (*rval))
    {
      cleanupExpr (newRv);
      finish (assign,0);
      mProcessingContinuousAssign = NULL;
      return NULL;
    }
    cleanupExpr (newRv);
  }

  // UInt32 lsize = lval->getBitSize ();
  // I'd like to assert == here, but v[31:0] = !a will
  // fail, because operator unary ! rejects resize requests and
  // always gives its bit size as 1.  Probably that's broken and
  // we should allow it to resize up.
  //
  // In addition, the VHDL sign-extension operator ignores resizes AND it can
  // start out larger, in situations like:
  //     abit = eBIVhExt(avector64, 1);
  // NU_ASSERT(lsize >= rval->getBitSize(), assign);
  
  mProcessingContinuousAssign = NULL;
  return result;
}


NUStmt*
FoldI::foldFor(NUFor* forStmt)
{
  forStmt->replaceLeaves (*this);

  if (isAggressive () && isUDKillable ())
  {
    NUExpr *cond = forStmt->getCondition ();
    if (NUConst* c = ctce (cond)) {
      if (not c->isZero ()) {
        // This has become an infinite loop - that's probably a problem, so warn about it
        // But only on the aggressive pass, or we end up complaining 5-6 times
        //
        // This warning is now in BreakResynth.cxx, where it will be
        // executed exactly once.
        // mMsgContext->REInfiniteLoop (&(forStmt->getLoc ()));
      }
      else if (not sHasNastyBlocks (forStmt->loopBody ()) && not sHasNastyBlocks (forStmt->loopAdvance ()))
      {
        //  replacing forstmt by just the initial statements
        // 
        NUStmt* result = finish (forStmt, processStmtList (forStmt->loopInitial (),
                                                           forStmt->getLoc ()));
        NUStmtList emptyList;
        forStmt->replaceInitial (emptyList);
        return result;
      }
    }
  }

  NUStmtVector toDelete;
  stmtList (forStmt->loopBody (), &toDelete);
  sRemoveStmts (forStmt, &NUFor::removeBodyStmt, &toDelete);

  NUStmtList flattened_result;
  if (removeSubBlocks(forStmt, forStmt->loopBody(),&flattened_result)) {
    forStmt->replaceBody(flattened_result);
  }

  toDelete.clear ();
  stmtList (forStmt->loopInitial (), &toDelete);
  sRemoveStmts (forStmt, &NUFor::removeInitialStmt, &toDelete);
  flattened_result.clear ();
  if (removeSubBlocks (forStmt, forStmt->loopInitial (), &flattened_result)) {
    forStmt->replaceInitial (flattened_result);
  }

  toDelete.clear ();
  stmtList (forStmt->loopAdvance (), &toDelete);
  sRemoveStmts (forStmt, &NUFor::removeAdvanceStmt, &toDelete);
  flattened_result.clear ();
  if (removeSubBlocks (forStmt, forStmt->loopAdvance (), &flattened_result)) {
    forStmt->replaceAdvance (flattened_result);
  }


  return forStmt;
}


// Compare the case selector against a group of conditions and return a comparison 
// result of:
//	eUnknown : can't determine relationship
//	eFalse : false - never equal
//	eTrue : true  - compare as equal
//

FoldI::CompareState FoldI::evalCaseIndex (NUCaseItem::ConditionLoop conditions,
                                          const NUExpr* selector)
{
  CompareState result = eFalse;	// worst case assumption

  for (; !conditions.atEnd (); ++ conditions)
  {
    const NUCaseCondition* cond = *conditions;
    bool condval;
    
    if (not checkConditionMask (cond, 0)) {
      // This is a CASEX or CASEZ - don't make any early decisions
      return eUnknown;
    }

    const NUExpr* right = cond->getExpr ();
    DynBitVector rv;
    DynBitVector* rvp = NULL;

    if (const NUConst* rc = right->castConst ()) {
      rc->getSignedValue (&rv);
      rvp = &rv;
    }
    
    if (evalRelop (NULL, &condval, NUOp::eBiEq, selector, right, rvp)) {
      // We can evaluate this condition
      if (condval)
        return eTrue; 	// At least one case-condition will match, quit now
      // Still having false conditions or unknowns.
    } else {
      // We can't decide if this condition matches
      result = eUnknown;
    }
  }

  // At this point, if none of the conditions match the selector AND we were able
  // to evaluate them all, then the 'result' is still eFalse.  If we encountered
  // things we couldn't figure out we are eUnknown.  Regardless, return our 
  return result;
}

/*!
 * Check that this mask is ok; if there is a mask, that the
 * mask is constant and the number of dont care bits in the
 * mask is less than limit.
 */
bool FoldI::checkConditionMask(const NUCaseCondition *cond, UInt32 limit) const
{
  NUExpr *mask = cond->getMask();
  if (not mask) {
    return true;
  }

  NUConst *constant = ctce (mask);
  if (not constant) {
    return false;
  }

  DynBitVector v;
  constant->getSignedValue(&v);
  return ((v.size() - v.count()) <= limit);
}


//! Expand don't cares in the given case item.  Return false if we should
//! remove this case item completely from the set.
bool FoldI::caseItemExpandDontCare(NUCaseItem *caseItem, ConditionSet *conditionSet)
{
  typedef UtVector<DynBitVector> VectOfDynBit;
  NUCaseItem::ConditionVector replacements;

  NUCaseItem::ConditionLoop conds = caseItem->loopConditions();
  if (conds.atEnd ())
    // this is already the default
    return false;
    
  for (; !conds.atEnd(); ++conds) {
    NUCaseCondition *cond = *conds;

    NUConst *const_test = cond->getExpr()->castConst();
    NU_ASSERT(const_test, cond);

    DynBitVector test_val;
    const_test->getSignedValue(&test_val);

    UInt32 size = test_val.size();

    NUExpr *mask = cond->getMask();
    if (not mask) {
      if (conditionSet->find (test_val) == conditionSet->end ()) {
        // Not already found
        replacements.push_back(cond);
        conditionSet->insert (test_val);
      }
      else
        // already handled by earlier action
        delete cond;            // not_managed_expr
      continue;
    }

    NUConst *const_mask = ctce (mask);
    NU_ASSERT(const_mask, mask);
    DynBitVector mask_val;
    const_mask->getSignedValue(&mask_val);

    // Expand the mask into all possible combinations.
    VectOfDynBit expanded_masks;
    DynBitVector start(test_val);
    expanded_masks.push_back(start);
    for (unsigned int i = 0; i < mask_val.size(); ++i) {
      if (not mask_val.test(i)) {
        // The vector has the 0 values already, so append a copy with 1s.
        VectOfDynBit ones;
        for (VectOfDynBit::iterator iter = expanded_masks.begin();
             iter != expanded_masks.end();
             ++iter) {
          DynBitVector new_vect(*iter);
          new_vect.set(i);
          ones.push_back(new_vect);
        }
        for (VectOfDynBit::iterator iter = ones.begin();
             iter != ones.end();
	     ++iter) {
          expanded_masks.push_back(*iter);
        }
      }
    }

    // Go through the expansion and create new case conditions.
    SourceLocator loc = cond->getLoc();
    for (VectOfDynBit::iterator iter = expanded_masks.begin();
         iter != expanded_masks.end();
         ++iter) {

      if (conditionSet->find (*iter) != conditionSet->end ())
        // Already in the case - don't add another copy!
        continue;

      conditionSet->insert (*iter);
      NUExpr *expr = NUConst::create(false, *iter, size, loc);
      expr->resize(const_test->getBitSize());
      NUCaseCondition *new_cond = new NUCaseCondition(loc, expr);
      replacements.push_back(new_cond);
    }

    // No longer need the original.
    delete cond;                // not_managed_expr
  }

  caseItem->replaceConditions(replacements);

  // Are all the conditions redundant to those previously encountered
  return  (replacements.empty ());
}


//! Try to get rid of don't cares by expanding out
/*!
 * Having don't cares causes us to generate if-then-else statements
 * instead of switch statements.  So, try to get rid of don't cares.
 *
 * Only get rid of don't cares when the resultant case statement would
 * be emittable as a switch.
 */
void FoldI::caseExpandDontCare(NUCase* theCase)
{
  if (not theCase->canEmitSwitch (true))
    // Not likely to generate a C switch statement, so forget
    // removing don't cares
    return;

  // Make sure that all the case conditions are constant, that
  // there are don't cares and that the size of the don't cares
  // doesn't exceed our limit.
  UInt32 dont_care_bit_limit = 5;
  bool has_mask = false;

  for (NUCase::ItemLoop items = theCase->loopItems();
       !items.atEnd();
       ++items) {
    NUCaseItem* caseItem = *items;
    for (NUCaseItem::ConditionLoop conds = caseItem->loopConditions();
         !conds.atEnd();
         ++conds) {
      NUExpr *mask = (*conds)->getMask();
      has_mask |= (mask != 0);
      if (not checkConditionMask(*conds, dont_care_bit_limit))
        return;
    }
  }

  if (not has_mask)
    return;

  // Expand all don't care case conditions, without duplicating any existing
  // conditions

  ConditionSet conditionSet;
  NUCaseItemList redundantItems;

  for (NUCase::ItemLoop items = theCase->loopItems();
       !items.atEnd();
       ++items) {
    NUCaseItem* caseItem = *items;
    if (caseItemExpandDontCare(caseItem, &conditionSet))
      // this item has become redundant, because all its conditions
      // were found in earlier caseItems.
      redundantItems.push_back (caseItem);
  }

  // Clean up any redundant case items
  for(NUCaseItemList::iterator i = redundantItems.begin ();
      i != redundantItems.end ();
      ++i)
  {
    theCase->removeItem (*i);
    delete *i;                  // not_managed_expr
  }
}


void 
FoldI::caseItemPropagateDontCareImplications(NUCase * caseStmt,
                                             NUCaseItem * item,
                                             const DynBitVector & implicant_value,
                                             const DynBitVector & implicant_mask)
{
  NUExpr * select = caseStmt->getSelect();
  UInt32 select_size = select->getBitSize();

  DynBitVector inverted_implicant_mask(select_size);
  inverted_implicant_mask = ~implicant_mask;

  DynBitVector overspecified_value(select_size);
  overspecified_value = implicant_value & inverted_implicant_mask;

  // There is no reason to stop propagating don't care implications to
  // subsequent conditions just because the current condition cannot
  // receive an update.
  // 
  // For example:
  // \code
  // casex(sel):
  // 3'b1xx: out = a;
  // other:  out = b;
  // 3'b001: out = c;
  // default: out = d;
  // 
  // We cannot update the second condition because it's non-constant.
  // We can, however, propagate the implied don't care from the first
  // condition to the third, resulting in:
  // 
  // casex(sel):
  // 3'b1xx: out = a;
  // other:  out = b;
  // 3'bx01: out = c;
  // default: out = d;

  for (NUCaseItem::ConditionLoop conds = item->loopConditions();
       not conds.atEnd();
       ++conds) {
    NUCaseCondition * condition = (*conds);

    NUExpr * expr = condition->getExpr();
    if (expr->getBitSize()!=select_size) {
      // Different sizes. Unhandled.
      continue;
    }

    NUConst * const_expr = expr->castConst();
    if (const_expr == NULL) {
      // Non-constant condition. Unhandled.
      continue;
    }
    
    // Check that the specified value matches the incoming value. If
    // it doesn't, this condition can never fire. Updating the mask on
    // this condition could incorrectly allow it to trigger.
    DynBitVector my_value(select_size);
    const_expr->getSignedValue(&my_value);
    my_value &= inverted_implicant_mask;

    NUExpr * mask = condition->getMask();
    if (mask == NULL) {
      if (my_value != overspecified_value) {
        // Condition can never fire.

      } else {
        NUConst * propagated_mask = NUConst::create(false, implicant_mask, 
                                                    select_size, condition->getLoc());
        condition->replaceMask(propagated_mask);
      }
    } else {
      if (mask->getBitSize()!=select_size) {
        // Different sizes. Unhandled.
        continue;
      }

      NUConst * mask_const = mask->castConst();
      NU_ASSERT(mask_const, expr);

      DynBitVector initial_mask_value(select_size);
      mask_const->getSignedValue(&initial_mask_value);

      my_value &= initial_mask_value;
      if (my_value != overspecified_value) {
        // Condition can never fire.

      } else {
        DynBitVector propagated_mask_value(select_size);
        propagated_mask_value = initial_mask_value & implicant_mask;
          
        if (propagated_mask_value != initial_mask_value) {
          // If we have discovered new bits of don't care space,
          // rewrite the mask.

          NUConst * propagated_mask = NUConst::create(false, propagated_mask_value, 
                                                      select_size, mask->getLoc());
          condition->replaceMask(propagated_mask);
          cleanupExpr (mask);
        }
      }
    }
  }
}


bool 
FoldI::caseItemDiscoverDontCareImplications(NUCase * caseStmt,
                                            NUCaseItem * item,
                                            DynBitVector & implicant_value,
                                            DynBitVector & implicant_mask)
{
  NUExpr * select = caseStmt->getSelect();
  UInt32 select_size = select->getBitSize();

  for (NUCaseItem::ConditionLoop conds = item->loopConditions();
       not conds.atEnd();
       ++conds) {
    NUCaseCondition * condition = (*conds);

    NUExpr * expr = condition->getExpr();
    if (expr->getBitSize()!=select_size) {
      return false; // Different sizes. Unhandled.
    }

    NUConst * const_expr = expr->castConst();
    if (const_expr == NULL) {
      return false; // Non-constant condition. Unhandled.
    }

    NUExpr * mask = condition->getMask();
    if (mask == NULL) {
      return false; // Unmasked condition. The implications stop here.
    } 
      
    if (mask->getBitSize()!=select_size) {
      return false; // Different sizes. Unhandled.
    }

    NUConst * mask_const = mask->castConst();
    NU_ASSERT(mask_const, expr);

    DynBitVector my_implicant_mask(select_size);
    mask_const->getSignedValue(&my_implicant_mask);

    if (my_implicant_mask.count() == 1) {
      // One bit of care space.

      // Remember the value which no longer needs specifying.
      DynBitVector my_value(select_size);
      const_expr->getSignedValue(&my_value);
      my_value.flip();
      implicant_value |= (my_value & my_implicant_mask);

      // The implicant_mask contains the bits which have not yet been covered.
      my_implicant_mask.flip();
      implicant_mask &= my_implicant_mask;
    } else {
      // Multiple bits of new care space. No further implications.
      return false;
    }
  }
  return true;
}


void
FoldI::casePropagateDontCareImplications(NUCase * caseStmt)
{
  NUExpr * select = caseStmt->getSelect();
  UInt32 select_size = select->getBitSize();

  // Computed mask implications.
  DynBitVector implicant_mask(select_size);
  implicant_mask.set();

  // The implicant_value is important when determining if the current
  // condition is overspecified. The implicant_mask can only be
  // applied if the masked value for the current condition matches the
  // provided implicant value. If it does not match, the condition can
  // never fire and should not be updated.
  DynBitVector implicant_value(select_size);

  bool discover_new_implicants = true;

  for (NUCase::ItemLoop items = caseStmt->loopItems();
       not items.atEnd();
       ++items) {
    NUCaseItem * item = (*items);

    // Propagate any implicants from prior items to this one.
    if (not implicant_mask.all()) {
      caseItemPropagateDontCareImplications(caseStmt, item, 
                                            implicant_value,
                                            implicant_mask);
    }

    // Determine new implicants and remember for subsequent items.
    if (discover_new_implicants) {
      bool ok = caseItemDiscoverDontCareImplications(caseStmt, item, 
                                                     implicant_value,
                                                     implicant_mask);
      discover_new_implicants &= ok;
    }
  }
}


/*
 * Helper function for caseIntoLut.
 *
 * Test case item:
 *  . the conditions are all constants, without X or Z
 *
 * Return true if the item passes the tests, false otherwise.
 *
 * The table will be populated with the RHS value to copy for each
 * condition under this item.
 * Set is_signed to true if any of the constants are signed.
 */
bool FoldI::caseIntoLutTestItemConditions(NUCaseItem *item,
                                           NUConst *rhs,
                                           NUConstVector &table,
                                           bool &is_signed)
{
  // For the default, iterate through the table and populate any 0 entries.
  if (item->isDefault()) {
    for (UInt32 idx = 0; idx < table.size(); ++idx) {
      if (table[idx] == 0) {
        is_signed |= rhs->isSignedResult();
        table[idx] = dynamic_cast<NUConst*>(rhs);
      }
    }

    return true;
  }

  for (NUCaseItem::ConditionLoop cond_loop = item->loopConditions();
       not cond_loop.atEnd();
       ++cond_loop) {
    NUCaseCondition *cond = *cond_loop;

    // Don't allow masked comparisons (don't cares).
    if (cond->getMask()) {
      return false;
    }

    NUExpr *expr = cond->getExpr();
    if (expr->getType() != NUExpr::eNUConstNoXZ) {
      return false;
    }

    NUConst *const_expr = ctce (expr);
    NU_ASSERT(const_expr, expr);

    UInt32 idx = 0;
    if (not const_expr->getUL(&idx)) {
      return false;
    }

    // If this is already populated, don't re-populate.
    if (table[idx] == 0) {
      is_signed |= rhs->isSignedResult();
      table[idx] = dynamic_cast<NUConst*>(rhs);
    }
  }

  return true;
}


/*
 * Helper function for caseIntoLut.
 *
 * Test the case item:
 *  . it has one statement which is an assign
 *  . if common_lhs is non-0, that the lhs of the assign is equivalent to
 *    common_lhs
 *  . the rhs is a constant
 *
 * Return true if the item passes the tests, false otherwise.
 * Update common_lhs if it is 0 and the item otherwise passes the tests.
 * Update max_rhs_size to be the maximum of the passed-in value and the
 * rhs size.
 */
static bool sCaseIntoLutTestItemStmt(NUCaseItem *item,
                                     NUConst **rhs,
                                     NULvalue** common_lhs,
                                     UInt32 &max_rhs_size)
{
  // Test for single statement.
  NUStmtLoop stmt_loop = item->loopStmts();
  if (stmt_loop.atEnd()) {
    return false;
  }

  NUStmt *stmt = *stmt_loop;
  ++stmt_loop;
  if (not stmt_loop.atEnd()) {
    return false;
  }

  // Test for assign.
  if (stmt->getType() != eNUBlockingAssign) {
    return false;
  }

  NUAssign *assign = dynamic_cast<NUAssign*>(stmt);
  NU_ASSERT(assign, stmt);

  // Test the rhs to be constant.
  NUExpr *this_rhs = assign->getRvalue();
  if (this_rhs->getType() == NUExpr::eNUConstNoXZ) {
    *rhs = dynamic_cast<NUConst*>(this_rhs);
    NU_ASSERT(*rhs, this_rhs);
    max_rhs_size = std::max(max_rhs_size, (*rhs)->getBitSize());
  } else {
    return false;
  }

  // If there is a common lhs already, test that this lhs is equivalent.
  // Otherwise, set the common lhs to this lhs.
  NULvalue *this_lhs = assign->getLvalue();
  if (*common_lhs) {
    return (**common_lhs == *this_lhs);
  } else {
    *common_lhs = this_lhs;
  }

  return true;
}


/*
 * Attempt to transform a case statment into a lookup table.
 * In order to do this transformation, the case statement must:
 *  . either be fully specified or have a default
 *  . have constant conditions
 *  . have a single assign in all case items
 *  . have the same LHS in all assigns
 *  . the RHS of all assigns must be constant
 *
 * If successful, will return an assign stmt which uses the Lut.
 * If not successful, will return 0.
 */
NUStmt* FoldI::caseIntoLut(NUCase* case_stmt)
{
  NUExpr *case_selector = case_stmt->getSelect();
  UInt32 case_selector_size = case_selector->getBitSize();

  // Set a size limit of 8 bits of selector.  This will result in a table
  // which has at most 256 (2^8) table entries.
  if (case_selector_size > 8) {
    return 0;
  }

  // For now, disqualify signed selects.  We could implement this by
  // converting to unsigned, if there is enough demand.  Currently,
  // this causes a gcc warning bug 3519.
  if (case_selector->isSignedResult()) {
    return 0;
  }

  UInt32 num_table_entries = (1 << case_selector_size);

  // Initialize the table to null pointers.
  // This table will be pointers to the original constants.  If we are
  // successful, these constants will need to be copied so the Lut can own them.
  NUConstVector table(num_table_entries, 0);

  bool transform_ok = true;
  bool is_signed = false;
  NULvalue *common_lhs = 0;
  UInt32 rhs_size = 0;

  // Iterate through the case statement items.  As we go, build the table
  // of constants.
  for (NUCase::ItemLoop item_loop = case_stmt->loopItems();
       not item_loop.atEnd() and transform_ok;
       ++item_loop) {
    NUCaseItem *item = *item_loop;

    NUConst *rhs = 0;
    if (not sCaseIntoLutTestItemStmt(item, &rhs, &common_lhs, rhs_size)) {
      transform_ok = false;
      break;
    }

    if (not caseIntoLutTestItemConditions(item, rhs, table, is_signed)) {
      transform_ok = false;
      break;
    }
  }

  // If any table entries are not populated, then fail.
  if (transform_ok) {
    for (UInt32 idx = 0; idx < table.size(); ++idx) {
      if (table[idx] == 0) {
        transform_ok = false;
        break;
      }
    }
  }

  // If we were successful in populating the table, create the Lut and the
  // assign to the lhs.
  // Otherwise, return 0.
  if (transform_ok) {
    CopyContext copy_ctx(0, 0);

    // Copy the constants so the Lut can own them.
    NUConstVector table_copies(num_table_entries, 0);
    UInt32 idx = 0;
    for (NUConstVector::iterator iter = table.begin();
         iter != table.end();
         ++iter, ++idx) {
      NUConst *new_const = dynamic_cast<NUConst*>((*iter)->copy(copy_ctx));
      new_const->resize(rhs_size);
      // Luts must be 'precise', so don't allow the constants we
      // place in the table to be signed values.  That would end up giving
      // them precision that extended to machine-unit sizes (based on 
      // whether the LUT is stored as an array of char, short, etc.
      // If they really are signed 32 bit numbers, we'll populate the
      // table with the right bit pattern anyway.
      new_const->setSignedResult (false);
      table_copies[idx] = new_const;
    }

    NULut *lut = new NULut(case_selector->copy(copy_ctx),
                           table_copies,
                           case_stmt->getLoc());
    lut->setSignedResult(is_signed);
    NUBlockingAssign *assign = new NUBlockingAssign(common_lhs->copy(copy_ctx),
                                                    lut,
                                                    false,
                                                    case_stmt->getLoc());
    return assign;
  }

  return 0;
}


NUStmt * 
FoldI::caseOnlyDontCare(NUCase * caseStmt)
{
  UInt32 select_size = caseStmt->getSelect()->getBitSize();

  // If the first item contains a condition with all don't-care,
  // convert the case statement into an if-then-else based on the
  // selector.

  NUCaseItem * conditional_item = NULL;

  // Find the don't care item. If a care item occurs before the don't
  // care item, abort the analysis.
  for (NUCase::ItemLoop items = caseStmt->loopItems();
       not items.atEnd();
       ++items) {
    NUCaseItem * item = (*items);

    bool has_only_dont_care = false;
    for (NUCaseItem::ConditionLoop conds = item->loopConditions();
         not conds.atEnd();
         ++conds) {
      NUCaseCondition * condition = (*conds);

      NUExpr * expr = condition->getExpr();
      if (expr->getBitSize()!=select_size) {
        // Different sizes. Unhandled.
        return NULL;
      }

      NUConst * const_expr = expr->castConst();
      if (const_expr == NULL) {
        // Non-constant condition. Unhandled.
        return NULL;
      }

      NUExpr * mask = condition->getMask();
      if (mask != NULL) {
        if (mask->getBitSize()!=select_size) {
          // Different sizes. Unhandled.
          return NULL;
        }

        NUConst * mask_const = mask->castConst();
        NU_ASSERT(mask_const != NULL, condition);
        DynBitVector mask_value;
        mask_const->getSignedValue(&mask_value);
        if (mask_value.none()) {
          has_only_dont_care = true;
          break;
        }
      }
    }
    if (has_only_dont_care) {
      conditional_item = item;
      break;
    } else {
      return NULL;
    }
  } 

  // Could not find the don't care item.
  if (conditional_item == NULL) {
    return NULL;
  }

  NUStmt * result = processStmtList (conditional_item->loopStmts (),
                                    caseStmt->getLoc ());

  result = finish (caseStmt, result);

  NUStmtList empty;
  conditional_item->replaceStmts (empty);
  return result;
}


bool 
FoldI::caseFindOneCareRange(NUCase * caseStmt,
                            NUCaseItem ** conditional_item,
                            NUCaseItem ** default_item,
                            NUExpr     ** conditional_value,
                            ConstantRange * care_range)
{
  (*conditional_item)  = NULL;
  (*default_item)      = NULL;
  (*conditional_value) = NULL;

  UInt32 select_size = caseStmt->getSelect()->getBitSize();

  for (NUCase::ItemLoop items = caseStmt->loopItems();
       not items.atEnd();
       ++items) {
    NUCaseItem * item = (*items);

    if (item->isDefault()) {
      if ((*default_item)==NULL) {
        (*default_item) = item;
      } else {
        return false;
      }
    } else {
      if ((*conditional_item)==NULL) {
        (*conditional_item) = item;
      } else {
        return false;
      }

      NUCaseItem::ConditionLoop conds = item->loopConditions();
      if (conds.atEnd()) { return false; } // no conditions.

      NUCaseCondition * condition = (*conds);
      ++conds;
      if (not conds.atEnd()) { return false; } // not a single condition

      NUExpr * expr = condition->getExpr();
      if (expr->getBitSize()!=select_size) {
        // Different sizes. Unhandled.
        return false;
      }

      NUConst * const_expr = expr->castConst();
      if (const_expr == NULL) {
        // Non-constant condition. Unhandled.
        return false;
      }

      NUExpr * mask = condition->getMask();
      if (mask == NULL) {
        // Care condition.
        return false;
      } 

      if (mask->getBitSize()!=select_size) {
        // Different sizes. Unhandled.
        return false;
      }

      NUConst * mask_const = mask->castConst();
      NU_ASSERT(mask_const != NULL, condition);
      DynBitVector mask_value;
      mask_const->getSignedValue(&mask_value);
      if (mask_value.all()) {
        // All bits matter.
        return false;
      } else if (mask_value.none()) {
        // No bits matter.
        return false;
      }

      UInt32 start;
      UInt32 size;
      mask_value.getContiguousRange(&start,&size);

      ConstantRange constant_range(start+size-1,start);
      mask_value.setRange(start,size,0);

      if (mask_value.any()) {
        // More than one range.
        return false;
      }

      (*conditional_value) = expr;
      (*care_range) = constant_range;
    }
  } 

  if ((*conditional_item)==NULL) {
    // we processed the case statement without finding any conditional items.
    return false;
  } else {
    return true;
  }
}


NUStmt * 
FoldI::caseOneCareRange(NUCase * caseStmt)
{
  NUCaseItem * conditional_item = NULL;
  NUCaseItem * default_item     = NULL;
  NUExpr     * conditional_value = NULL;
  ConstantRange care_range;

  bool success = caseFindOneCareRange(caseStmt,
                                      &conditional_item,
                                      &default_item,
                                      &conditional_value,
                                      &care_range);
  if (not success) {
    return NULL;
  }

  // If this is a case statement with a single care range, eliminate
  // all don't care from the condition.
  // case(sel)                if (sel[1]) 
  // 3'x1x:   out = a;  ==>     out = a;
  // default: out = b;        else
  // endcase                    out = b;

  NUExpr * select = caseStmt->getSelect();
  NUExpr * care_select = createPartsel(select, care_range,
                                       select->getLoc());
  NUExpr * care_value  = createPartsel(conditional_value, care_range,
                                       conditional_value->getLoc());
  NUExpr * care_condition = new NUBinaryOp(NUOp::eBiEq,
                                           care_select, care_value, 
                                           caseStmt->getLoc());
  care_condition->resize(1);

  NUIf * care_if = new NUIf(care_condition,
                            mFactory,
                            caseStmt->getUsesCFNet(),
                            caseStmt->getLoc());

  NUStmtList * conditional_stmts = conditional_item->getStmts();
  care_if->addThenStmts(conditional_stmts);
  delete conditional_stmts;     // not_managed_expr

  if (default_item) {
    NUStmtList * default_stmts = default_item->getStmts();
    care_if->addElseStmts(default_stmts);
    delete default_stmts;       // not_managed_expr
  }
  
  NUStmt * result = finish(caseStmt, care_if);

  NUStmtList empty;
  conditional_item->replaceStmts(empty);
  if (default_item) {
    default_item->replaceStmts(empty);
  }

  return result;
}


bool FoldI::caseFindIsolatedMasks(NUCase * caseStmt, 
                                  DynBitVector * all_masks,
                                  NUCaseItem ** conditional_item,
                                  NUCaseItem ** default_item)
{
  (*conditional_item) = NULL;
  (*default_item) = NULL;

  UInt32 select_size = caseStmt->getSelect()->getBitSize();

  all_masks->resize(select_size);
  all_masks->set();

  for (NUCase::ItemLoop items = caseStmt->loopItems();
       not items.atEnd();
       ++items) {
    NUCaseItem * item = (*items);

    if (item->isDefault()) {
      if ((*default_item)==NULL) {
        (*default_item) = item;
      } else {
        return false;
      }
    } else {
      if ((*conditional_item)==NULL) {
        (*conditional_item) = item;
      } else {
        return false;
      }

      for (NUCaseItem::ConditionLoop conds = item->loopConditions();
           not conds.atEnd();
           ++conds) {
        NUCaseCondition * condition = (*conds);

        NUExpr * expr = condition->getExpr();
        if (expr->getBitSize()!=select_size) {
          // Different sizes. Unhandled.
          return false;
        }

        NUConst * const_expr = expr->castConst();
        if (const_expr == NULL) {
          // Non-constant condition. Unhandled.
          return false;
        }

        NUExpr * mask = condition->getMask();
        if (mask == NULL) {
          // NULL mask, meaning every bit is meaningful. Covered by the
          // initial state of all_masks.
        } else {
          if (mask->getBitSize()!=select_size) {
            // Different sizes. Unhandled.
            return false;
          }

          NUConst * mask_const = mask->castConst();
          NU_ASSERT(mask_const != NULL, condition);
          DynBitVector mask_value;
          mask_const->getSignedValue(&mask_value);
          (*all_masks) &= mask_value;
        }
      }
    }
  }

  if ((*conditional_item)==NULL) {
    // We processed the case statement without finding any conditional items.
    return false;
  } else {
    return true;
  }
}


//! Map a constant to a number of case conditions.
/*!
  Used to determine which case conditions have identical masked values.
 */
typedef UtMap<DynBitVector,NUCaseConditionVector> ValueConditionMap;


/*! 
  Group case conditions with identical masked values into one map
  entry corresponding to that maked value.
*/
void sFindDistinctMaskedConstants(NUCaseItem * item, SInt32 start, SInt32 size,
                                  ValueConditionMap * value_condition_map)
{
  // The contiguous mask is combined with the value of each condition
  // to determine a constant portion of that condition.
  DynBitVector contiguous_mask;
  contiguous_mask.setRange(start,size,1);

  for (NUCaseItem::ConditionLoop conds = item->loopConditions();
       not conds.atEnd();
       ++conds) {
    NUCaseCondition * condition = (*conds);
    NUExpr * expr = condition->getExpr();
    NUConst * expr_const = expr->castConst();
    NU_ASSERT(expr_const, condition);
    DynBitVector expr_value;
    expr_const->getSignedValue(&expr_value);
    expr_value &= contiguous_mask;
    (*value_condition_map)[expr_value].push_back(condition);
  }
}


NUCaseCondition * 
FoldI::partselCaseCondition(NUExpr * expr, NUExpr * mask,
                            ConstantRange & range,
                            const SourceLocator & loc)
{
  NUExpr * partial_expr = createPartsel(expr, range,
                                        expr->getLoc());
  NUExpr * partial_mask = NULL;
  if (mask) {
    partial_mask = createPartsel(mask, range,
                                 mask->getLoc());
  }
  NUCaseCondition * partial_condition = new NUCaseCondition(loc,
                                                            partial_expr, 
                                                            partial_mask);
  return partial_condition;
}


//! Helper routine wrapping a deep stmtlist copy.
void sAddItemStmts(NUCaseItem * item, NUStmtList * stmts, CopyContext & cc)
{
  NUStmtList tmp;
  deepCopyStmtList(*stmts, &tmp, cc);
  item->addStmts(&tmp);
}


void FoldI::processOneMaskedConstant(NUCase * caseStmt,
                                     NUCaseItem * conditional_item,
                                     NUCaseItem * default_item,
                                     ConstantRange constant_range,
                                     ConstantRange variable_range,
                                     NUCase * outer_case,
                                     NUCaseConditionVector * conditions)
{
  NUExpr * select = caseStmt->getSelect();

  NUStmtList * conditional_stmts = conditional_item->getStmts();
  NUStmtList * default_stmts = default_item ? default_item->getStmts() : NULL;

  NUCaseItem * outer_item = NULL;
  NUCaseItem * inner_item = NULL;

  for (NUCaseConditionVector::iterator conds = conditions->begin();
       conds != conditions->end();
       ++conds) {
    NUCaseCondition * condition = (*conds);
    NUExpr * expr = condition->getExpr();
    NUExpr * mask = condition->getMask();

    if (outer_item == NULL) {
      // The first item for this masked constant; create a
      // condition for the outer case stmt.
      outer_item = new NUCaseItem(conditional_item->getLoc());
      outer_case->addItem(outer_item);

      // This condition corresponds to the constant portion of the
      // original condition.
      NUCaseCondition * outer_condition = partselCaseCondition(expr, mask, 
                                                               constant_range, 
                                                               condition->getLoc());
      outer_item->addCondition(outer_condition);
    }

    if (inner_item == NULL) {
      // The first item for this masked constant; create the inner
      // case stmt, default and conditional item, and populate the
      // stmts. The conditions for this item are added for each
      // processed original NUCaseCondition.
      NUExpr * inner_select = createPartsel(select, variable_range,
                                            select->getLoc());
      NUCase * inner_case = new NUCase(inner_select,
                                       mFactory,
                                       caseStmt->getCaseType(),
                                       caseStmt->getUsesCFNet(),
                                       caseStmt->getLoc());
      outer_item->addStmt(inner_case);

      inner_item = new NUCaseItem(conditional_item->getLoc());
      inner_case->addItem(inner_item);

      // copy all statements from the condition.
      sAddItemStmts(inner_item, conditional_stmts, getCC());

      if (default_stmts) {
        NUCaseItem * inner_default = new NUCaseItem(default_item->getLoc());
        inner_case->addItem(inner_default);
        inner_case->putHasDefault(true);

        // copy all statements from the default.
        sAddItemStmts(inner_default, default_stmts, getCC());
      }
    }

    // This condition corresponds to the variable portion of the
    // original condition.
    NUCaseCondition * inner_condition = partselCaseCondition(expr, mask,
                                                             variable_range,
                                                             condition->getLoc());
    inner_item->addCondition(inner_condition);

  }

  if (default_stmts) {
    delete default_stmts;       // not_managed_expr
  }
  delete conditional_stmts;     // not_managed_expr
}

NUCase * 
FoldI::caseIsolateMasks(NUCase * caseStmt)
{
  DynBitVector all_masks(caseStmt->getSelect()->getBitSize());
  all_masks.set();

  NUCaseItem * conditional_item = NULL;
  NUCaseItem * default_item = NULL;

  bool success = caseFindIsolatedMasks(caseStmt, &all_masks,
                                       &conditional_item, &default_item);
  if (not success) {
    return NULL;
  }

  if (all_masks.any() and not all_masks.all()) {
    // Operate on the the first contiguous range. Other ranges will be
    // handled when this method is called on the replacement case
    // statements.
    UInt32 start;
    UInt32 size;
    all_masks.getContiguousRange(&start,&size);

    // Allow only lsb or msb-aligned ranges. This allows simple
    // part-selects. If the range were in the middle, we would need to
    // introduce concats as selectors.
    if (not (start == 0 or (start+size)==all_masks.size())) {
      return NULL;
    }

    // The constant range corresponds to the _FIRST_ contiguous range
    // over the conditions that never contains any variable (x or z)
    // values.
    ConstantRange constant_range(start+size-1,start);
    // The variable range contains all bits not in the constant range.
    ConstantRange variable_range;
    if (start==0) {
      variable_range = ConstantRange(all_masks.size()-1,start+size);
    } else {
      variable_range = ConstantRange(start-1,0);
    }

    NUExpr * select = caseStmt->getSelect();
    NUExpr * outer_select = createPartsel(select, constant_range,
                                          select->getLoc());
    outer_select->resize (constant_range.getLength ());
    NUCase * outer_case = new NUCase(outer_select,
                                     mFactory,
                                     caseStmt->getCaseType(),
                                     caseStmt->getUsesCFNet(),
                                     caseStmt->getLoc());

    // If the masked portion of two conditions is the same, we need to
    // place them in the same branch of the generated case stmt. For
    // example:
    //
    // case(sel)
    // 11xxx,
    // 00x1x, 
    // 00x0x: out = a;
    // default: out = b;
    // endcase
    //
    // becomes (needing further simplification):
    // 
    // case(sel[4:3])
    // 11: case(sel[2:0])
    //     xxx: out = a;
    //     default: out = b;
    //     endcase
    // 00: case(sel[2:0])
    //     x1x, x0x: out = a;
    //     default:  out = b;
    // default: out = b;
    // endcase

    ValueConditionMap value_condition_map;
    sFindDistinctMaskedConstants(conditional_item, start, size, 
                                 &value_condition_map);

    // Create a condition in the replacement case stmt for each unique
    // masked portion of the constant condition. Within that
    // condition, create a new case statement with conditions which
    // match the variable portion of the original condition.

    for (ValueConditionMap::iterator iter = value_condition_map.begin();
         iter != value_condition_map.end();
         ++iter) {
      NUCaseConditionVector & conditions = iter->second;

      processOneMaskedConstant(caseStmt, conditional_item, default_item,
                               constant_range, variable_range, 
                               outer_case, &conditions);
    }

    if (default_item) {
      NUStmtList * default_stmts = default_item->getStmts();

      NUCaseItem * outer_default = new NUCaseItem(default_item->getLoc());
      outer_case->addItem(outer_default);
      outer_case->putHasDefault(true);

      // copy over the statements from the default.
      sAddItemStmts(outer_default, default_stmts, getCC());

      delete default_stmts;     // not_managed_expr
    }

    return outer_case;
  }

  return NULL;
}


void FoldI::caseEquivFold(NUCase * caseStmt)
{
  // Do not process case statements with more than the specified number of items
  SInt32 threshold;
  mArgs->getIntLast(CRYPT("-caseEquivFoldThreshold"), &threshold);
  SInt32 count = 0;

  bool requiresAdjacency = false;

  for (NUCase::ItemLoop i = caseStmt->loopItems (); 
       !i.atEnd (); 
       ++i, ++count)
  {
    NUCaseItem* caseItem = *i;

    // You can't re-arrange items in a casex/casez -- priority matters.
    if (count >= threshold) {
      return;
    }

    for (NUCaseItem::ConditionLoop j = caseItem->loopConditions();
         !requiresAdjacency && !j.atEnd(); ++j)
    {
      NUCaseCondition* cond = *j;
      if ((cond->getMask() != NULL) ||
          (cond->getExpr()->castConstNoXZ() == NULL))
      {
        requiresAdjacency = true;
      }
    }
  }

  // Can we fold away one or more of the case actions because they
  // have equivalent statements?
  NUCaseItem* newDefault = NULL;

  NUCaseItemList newItems;
  bool changed = false;
  Congruent congruent(mIODB, false, 0, NULL, mMsgContext, false);

  UtHashMap<NUStmtList*,NUCaseItem*> stmtListItemMap;

  // If this is a case-x, then we can only combine adjacent items,
  // otherwise we risk violating the priority order.
  NUStmtList* prevStmtList = NULL;

  for (NUCase::ItemLoop i = caseStmt->loopItems (); !i.atEnd (); ++i)
  {
    NUCaseItem* caseItem = *i;
    NUStmtList* stmts = caseItem->getStmts();

    bool combine = false;
    NUStmtList* canonicalStmts = congruent.insertStmtList(stmts);
    if (stmts != canonicalStmts) {
      combine = true;

      // Do an adjacency check if required
      if (requiresAdjacency && (prevStmtList != canonicalStmts)) {
        combine = false;
      }
    }

    if (combine) {
      changed = true;

      NUCaseItem* canonicalCaseItem = stmtListItemMap[canonicalStmts];
      print(canonicalCaseItem, caseItem);

      if (caseItem->isDefault()) {
        canonicalCaseItem->clearConditions(true); // delete and forget
        NU_ASSERT(canonicalCaseItem->isDefault(), caseStmt);
        NU_ASSERT(newDefault == NULL, caseStmt);
        newDefault = canonicalCaseItem;
      }
      else if (! canonicalCaseItem->isDefault()) {
        // Move all the conditions from this one to the default.
        for (NUCaseItem::ConditionLoop j = caseItem->loopConditions();
             !j.atEnd(); ++j)
        {
          NUCaseCondition* cond = *j;
          canonicalCaseItem->addCondition(cond);
        }
        caseItem->clearConditions(false); // don't delete, just forget
      }
      delete caseItem;          // not_managed_expr
      delete stmts;             // not_managed_expr
    } // if
    else {
      newItems.push_back(caseItem);
      stmtListItemMap[stmts] = caseItem;
    }
    prevStmtList = canonicalStmts;
  } // for
  if (changed) {
    caseStmt->replaceItems(newItems);
  }
  stmtListItemMap.clearPointerKeys(); // free the StmtLists from getStmtList()

  // If the default item was moved, we need to put it back at the end
  // because some code that uses NUCase objects assumes the default will
  // be the last case item.
  if (newDefault) {
    caseStmt->removeItem(newDefault); // remove from the middle
    caseStmt->addItem(newDefault);    // add at the end
  }
} // void FoldI::caseEquivFold


NUStmt*
FoldI::foldCase(NUCase* caseStmt)
{
  NU_ASSERT(caseStmt->isConsistent(), caseStmt);
  caseStmt->replaceLeaves (*this);
  
  NUStmtList emptyList;
  
  // Recurse on the statements within the case before doing any
  // folding at this level. (this is bottom-up-optimization)
  for (NUCase::ItemLoop i = caseStmt->loopItems (); !i.atEnd (); ++i) {
    NUCaseItem* caseItem =*i;
    
    NUStmtVector toDelete;
    stmtList (caseItem->loopStmts (), &toDelete);
    sRemoveStmts (caseItem, &NUCaseItem::removeStmt, &toDelete);
    
    NUStmtList flattened_result;
    if (removeSubBlocks(caseStmt, caseItem->loopStmts(),&flattened_result)) {
      caseItem->replaceStmts(flattened_result);
    }
  }  
  
  if (mNoFoldIfCase)
    // Don't collapse then/else or identical case actions
    return caseStmt;
  
  {
    NUStmt * care = caseOnlyDontCare(caseStmt);
    if (care) {
      return care; // finish already called.
    }
  }

  {
    NUStmt * care = caseOneCareRange(caseStmt);
    if (care) {
      return care; // finish already called.
    }
  }

  {
    NUCase * isolated = caseIsolateMasks(caseStmt);
    if (isolated) {
      return finish(caseStmt, isolated);
    }
  }

  caseExpandDontCare(caseStmt);

  casePropagateDontCareImplications(caseStmt);


  // Does this case stmt contain any nested blocks with declared nets within?
  // We can't eliminate them without screwing up netrefs.
  //
  bool nastyBlocks = isUDValid () && sHasNastyBlocks (caseStmt);

  // Check to see if the case-selector can be matched to a specific
  // action
  //
  NUCaseItem *default_item = 0;
  bool nonConstant = false;
  for (NUCase::ItemLoop i = caseStmt->loopItems (); not nonConstant and not i.atEnd (); ++i)
  {
    NUCaseItem* caseItem =*i;
    
    NUCaseItem::ConditionLoop conditions = caseItem->loopConditions();
    
    // Keep the default for later
    if (conditions.atEnd()) {
      default_item = caseItem;
    }
    
    switch (evalCaseIndex (conditions, caseStmt->getSelect()))
    {
    case eUnknown:
      // Expressions prevent further search for equal case-index
      nonConstant = true;
      break;
      
    case eFalse:
      // constant indices, but no match yet
      break;
      
    case eTrue:
      // Bingo!
      if (nastyBlocks)
        // But can't fold away due to UD information
        break;
      
      NUStmt *result = processStmtList (caseItem->loopStmts (),
                                        caseStmt->getLoc ());
      result = finish (caseStmt, result);
      caseItem->replaceStmts (emptyList);
      return result;
    }
  }

  // If there is a default, and all of the matches failed, then the stmt reduces to the default.
  if (not nonConstant && !nastyBlocks) {
    if (default_item != NULL) {
      NUStmt *result = processStmtList (default_item->loopStmts (),
                                        caseStmt->getLoc ());
      result = finish (caseStmt, result);
      default_item->replaceStmts (emptyList);
      return result;
    }

    // Constant-select, no funny blocks, no default.
    // The case statement disappears.
    return finish(caseStmt, 0);
  }
  

  if (not isUDKillable ())
    return caseStmt;

  // Try to transform the case stmt into a look up table.
  // Do this after sCaseExpandDontCare because this transformation depends
  // on having constant conditions instead of don't cares.
  //
  // Only do this when folding is being aggressive - this impacts cycle breaking
  // which special-cases case/if stmts but doesn't handle simple expressions.
  if (!mNoFoldCaseIntoLut && isAggressive()) {
    NUStmt *lut = caseIntoLut(caseStmt);
    if (lut) {
      caseStmt->clearUseDef();
      return finish(caseStmt, lut);
    }
  }
  
  if (isCaseEquivFold ()) {
    caseEquivFold(caseStmt);
  }

  NUCase::ItemLoop caseItems = caseStmt->loopItems ();
  if (caseItems.atEnd ()) {
    // Case is completely empty - no actions => delete it completely
    return finish (caseStmt, 0);
  }
  
  // Is there exactly ONE NUCaseItem and this is a fully-populated
  // case, or that one item is the default, then we don't need the case-dispatch any more
  NUCaseItem *firstItem = *caseItems;
  NUCaseItem::ConditionLoop firstConditions = firstItem->loopConditions ();
  NUCase::ItemLoop nextItem = caseItems;
  ++nextItem;
  if (nextItem.atEnd () && (caseStmt->isFullySpecified ()
                            || caseStmt->hasDefault ()
                            || caseStmt->isUserFullCase ()
                            || firstConditions.atEnd ()) // only item is a 'default' case
      
    )
  {
    NUStmt *result = processStmtList ((*caseItems)->loopStmts (),
                                      caseStmt->getLoc ());
    result = finish (caseStmt, result);
    (*caseItems)->replaceStmts (emptyList);
    return result;
  }
  
  if (caseStmt->isFullySpecified() and not caseStmt->hasDefault() && isAggressive ())
    // Remove the trailing conditions from a fully-specified case statement
    // and make them the "default".  This produces a smaller switch table and
    // fewer tests by making the "remaining choices" be matched by default.
    // Put this transform under aggressive mode so it is guaranteed to occur after inferencing.
  {
    NUCase::ItemRLoop rloop = caseStmt->loopItemsReverse();
    // if this assert fires, we have a case statement without any items.
    NU_ASSERT(not rloop.atEnd(), caseStmt);
    NUCaseItem * last_item = (*rloop);
    // if this assert fires, we have a case statement not marked as
    // having a default, but an item without conditions (a default).
    NU_ASSERT(last_item->hasConditions(), last_item); 
    print(last_item);
    
    for (NUCaseItem::ConditionLoop condLoop = last_item->loopConditions();
         not condLoop.atEnd();
         ++condLoop) {
      delete (*condLoop);       // not_managed_expr
    }
    NUCaseItem::ConditionVector empty_conditions;
    last_item->replaceConditions(empty_conditions);
    
    // by using the default the fully-specified property must be false.
    caseStmt->putFullySpecified(false);
    caseStmt->putHasDefault(true);
  }
  
  if (caseStmt->hasDefault ())
    // It could be one combined action and a default with the same value
  {
    ++nextItem;
    if (nextItem.atEnd ()) {
      nextItem = caseItems;
      ++nextItem;
      const NUCaseItem* constA = *caseItems;
      const NUCaseItem* constB = *nextItem;
      
      if (ObjListEqual<NUStmtCLoop>(constA->loopStmts (), constB->loopStmts ())
          && !nastyBlocks)
      {
        NUStmt *result = processStmtList ((*caseItems)->loopStmts (),
                                          caseStmt->getLoc ());
        result = finish (caseStmt, result);
        (*caseItems)->replaceStmts (emptyList);
        return result;
      }
    }
  }
  
  
  // Can we convert it to FFO | FFZ | FLO | FLZ ?
  if (isUDKillable () && not isPreservedReset () && not nastyBlocks)
  {
    NUStmt *result = casexFind (caseStmt);
    NU_ASSERT (result, caseStmt);
    if (result != caseStmt)
      return result;
  }
  
  return caseStmt;
} // NUStmt* foldCase


NUBlockScope* FoldI::getContext () const {
  return mContextStack->top ();
}

void FoldI::push (NUBlockScope* scope) {
  mContextStack->push (scope);
  mCC = CopyContext (scope, mStrCache);
}

void FoldI::pop () {
  mContextStack->pop ();
  if (mContextStack->empty ())
    mCC = CopyContext (0,0);
  else
    mCC = CopyContext (mContextStack->top(), mStrCache);
}

typedef UtVector<NUNamedDeclarationScope*> NUNamedDeclarationScopeVec;

// Recursively walk a named scope hierarchy, removing empty scopes as we go
// returns TRUE if scope is empty
bool
FoldI::foldScope (NUNamedDeclarationScope* scope, bool recurse)
{
  NUNamedDeclarationScopeVec emptyList;

  // First visit and process all subscopes
  for(NUNamedDeclarationScopeLoop s = scope->loopDeclarationScopes ();
      !s.atEnd ();
      ++s)
  {
    if (foldScope (*s))
      emptyList.push_back (*s);
  }
                   
  // dispose of pending scopes
  for(NUNamedDeclarationScopeVec::iterator i = emptyList.begin ();
      i != emptyList.end ();
      ++i)
  {
    scope->removeDeclarationScope (*i);
    delete *i;     // not_managed_expr
  }

  // Visit any submodules that are instantiated here and fold them.
  for (NUModuleInstanceMultiLoop iter = scope->loopDeclScopeInstances();
       !iter.atEnd(); ++iter) {
    moduleInstance(*iter, recurse);
  }

  // If scope has instances, it's not empty.
  if (scope->hasInstances()) {
    return false;
  }

  // Check if this one is now empty
  NUNetLoop locals = scope->loopLocals ();
  if (not locals.atEnd ())
    return false;                     // contains nets

  if (not scope->loopDeclarationScopes ().atEnd ())
    // We could promote these, but it would screw up naming hierarchies
    return false;                     // contains other scopes

  return true;
}

NUConst* FoldI::getEdgeNetConst(NUEdgeExpr* ee) {
  NUConst* k = ee->getExpr()->castConst();
  if (k == NULL) {
    NUNet* net = ee->getNet();
    if (net != NULL) {
      NUExpr* e = findConstantValue(net);
      if (e != NULL) {
        return e->castConst();
      }
    }
  }
  return NULL;
}

bool FoldI::isEdgeAlwaysInactive(NUEdgeExpr* ee, NUAlwaysBlock* blk) {
  NUConst* k = getEdgeNetConst(ee);
  if (k != NULL) {
    // Any constant kills a clock block, but only non-asserted constants
    // kill reset blocks.
    NUAlwaysBlock* clk = blk->getClockBlock();
    bool is_clock_block = (clk == NULL) || (clk == blk);
    if (is_clock_block ||
        (k->isZero() && (ee->getEdge() == eClockPosedge)) ||
        (!k->isZero() && (ee->getEdge() == eClockNegedge)))
    {
      return true;
    }
  }
  return false;
} // bool FoldI::isEdgeAlwaysInactive

bool FoldI::isEdgeAlwaysActive(NUEdgeExpr* ee, NUAlwaysBlock* blk) {
  // If a higher priority block exists then this edge cannot be always
  // active.
  if (blk->getPriorityBlock() != NULL) {
    return false;
  }

  NUConst* k = getEdgeNetConst(ee);
  if (k != NULL) {
    // No constant livens a clock block, and only asserted constants
    // liven reset blocks.
    NUAlwaysBlock* clk = blk->getClockBlock();
    bool is_clock_block = (clk == NULL) || (clk == blk);
    if (!is_clock_block &&
        ((!k->isZero() && (ee->getEdge() == eClockPosedge)) || (k->isZero() && (ee->getEdge() == eClockNegedge))) )
    {
      return true;
    }
  }
  return false;
}

void FoldI::clearBlock(NUAlwaysBlock* blk) {
  // Remove all the statements in the always block, folding them to
  // nothing
  NUBlock* block = blk->getBlock();
  for (NUStmtLoop stmts(block->loopStmts()); !stmts.atEnd(); ++stmts) {
    NUStmt* stmt = *stmts;
    finish(stmt, NULL);
  }
  NUStmtList empty;
  block->replaceStmtList(empty);
}

NUAlwaysBlock* FoldI::removeEdge(NUAlwaysBlock* blk) {
  // Also remove the edge from the always block and remove it from the
  // priority chain
  NUAlwaysBlock* clock_block = blk->removeFromPriorityChain();

  NUEdgeExpr* ee = blk->getEdgeExpr();
  if (ee != NULL) {
    delete ee;   // not_managed_expr    use finish?  expr version would SEGV on NULL
    NUEdgeExprList empty_edges;
    blk->replaceEdgeExprs(&empty_edges);
    NU_ASSERT(!blk->isSequential(), blk);
  }
  return clock_block;
}

void FoldI::sequentialGroup(NUAlwaysBlock* clk_blk) {
  // Process sequential blocks from highest to lowest priority.  This routine
  // will only be called on the highest priority block, which should
  // be processed first.  But let's figure out all the blocks that should
  // be called, and process them from highest to lowest priority
  //
  // The reason to go in priority order is that if we prove a high priority
  // block is always-inactive, then we remove it from the chain and the next
  // lower priority block can be proved always-active.  E.g.
  //
  //   assign rst = 1'b0;
  //   assign pre = 1'b1;
  //   always @(posedge clk or posedge pre or posedge rst) begin
  //     if      (rst) q = 0;
  //     else if (pre) q = 1;
  //     else          q = d;
  //   end
  //
  // Here the 'pre' block is always-active, but you can't tell that until
  // you kill the 'rst' block.  This ordering enables the transformation of this
  // structure to always q = 1.
  // 
  NUAlwaysBlockVector chain, referers;
  NUAlwaysBlockSet chain_set;
  bool make_combinational = false;
  clk_blk->getClockBlockReferers(&referers);

  for (NUAlwaysBlock* b = clk_blk; b != NULL; b = b->getPriorityBlock()) {
    chain.push_back(b);
    chain_set.insert(b);
  }

  // Walk the chain from high to low priority
  SInt32 highest_active_block_index = chain.size() - 1;
  for (SInt32 i = highest_active_block_index; i >= 0; --i) {
    NUAlwaysBlock* blk = chain[i];
    NUEdgeExpr* ee = blk->getEdgeExpr();

    // If edge-condition means this block cannot be run, remove the statements,
    // but only remove the edge-expr for async-reset blocks.  
    if (isEdgeAlwaysInactive(ee, blk)) {
      // Do not remove the edge condition for clock-blocks with reset-blocks
      // because, by Carbon's priority-chain semantics, that would change
      // the next lowest-priority async-reset block to a clock block.  The
      // clock-block has i==0. 
      bool remove_edge = (i > 0);
      if (i == highest_active_block_index) {
        --highest_active_block_index;
        remove_edge = true;
      }

      clearBlock(blk);
      if (remove_edge) {
        clk_blk = removeEdge(blk);
      }
    }

    // If edge condition means this block is *always* run, remove
    // the contents of all lower-priority blocks
    else if (isEdgeAlwaysActive(ee, blk)) {
      // If this proves that the highest-priority block is always
      // active, then we've gone combinational.  Napalm the whole
      // priority queue.  This block is all that will remain
      make_combinational = (i == highest_active_block_index);

      // Clear all lower priority blocks
      for (SInt32 j = i - 1; j >= 0; --j) {
        NUAlwaysBlock* lower_pri_blk = chain[j];
        clearBlock(lower_pri_blk);
        if ((j > 0) || make_combinational) {
          clk_blk = removeEdge(lower_pri_blk);
        }
      }

      // Even thought this block is always active, we leave the condition
      // in if there is an active higher-priority block.
      if (make_combinational) {
        removeEdge(blk);
      }
      break;
    }
  } // for

  bool clear_referers = make_combinational || (clk_blk == NULL) ||
    (clk_blk->getPriorityBlock() == NULL);

  // Walk through all funky reset blocks (referers that are not in the chain)
  // and clear them if they are empty, or if we have no further need for funky
  // reset blocks.  Note that this optimization will be harder to implement
  // once we re-organize the async-reset chains to have each block be independent.
  for (UInt32 i = 0; i < referers.size(); ++i) {
    NUAlwaysBlock* referer = referers[i];
    if (chain_set.find(referer) == chain_set.end()) {
      bool clear_this_referer = clear_referers
        || referer->getBlock()->loopStmts().atEnd();
      if (! clear_this_referer) {
        // if this funky reset block is always on, then that means
        // we are checking the deassertion of a reset block that
        // is always off, so we can eliminate that.
        NUEdgeExpr* ee = referer->getEdgeExpr();
        NUConst* k = getEdgeNetConst(ee);
        clear_this_referer = ((k != NULL) &&
                              ((!k->isZero() && (ee->getEdge() == eClockPosedge)) ||
                               (k->isZero() && (ee->getEdge() == eClockNegedge))));
      }
      if (clear_this_referer) {
        clearBlock(referer);
        removeEdge(referer);
      }
    }
  }
} // void FoldI::sequentialGroup

void FoldI::module(NUModule *module, bool recurse)
{
  if (haveVisited(module)) {
    return;
  }

  // Remember changes encountered in this module or in any module instantiated
  // *by* this module.
  bool was_changed = false;
  clearChanged ();

  // Visit any submodules that are instantiated here and fold them.
  for (NUModuleInstanceMultiLoop iter = module->loopModuleInstances();
       !iter.atEnd(); ++iter) {
    moduleInstance(*iter, recurse);
  }
  
  was_changed |= isChanged ();

  // Now track any changes occuring within THIS module
  clearChanged ();

  // Visit named scopes and scrub them of empty nesting
  NUNamedDeclarationScopeVec emptyScopes;
  for(NUNamedDeclarationScopeLoop scopes = module->loopDeclarationScopes ();
      not scopes.atEnd ();
      ++scopes)
  {
    NUNamedDeclarationScope *s = *scopes;
    if (foldScope (s, recurse))
      emptyScopes.push_back (s);
  }

  for(NUNamedDeclarationScopeVec::iterator s = emptyScopes.begin ();
      s != emptyScopes.end ();
      ++s)
  {
    module->removeDeclarationScope (*s);
    delete *s;     // not_managed_expr
  }


  for (NUTaskLoop loop = module->loopTasks(); not loop.atEnd(); ++loop) {
    tf(*loop);
  }

  was_changed |= isChanged ();  // Record any folds to tasks or functions


  // Since continous assigns might propagate constant values, iterate over
  // them til they stabilize.
  do {
    clearChanged ();
    NUContAssignList cont_assigns;
    module->getContAssigns(&cont_assigns);
    for (NUContAssignListIter iter = cont_assigns.begin();
	 iter != cont_assigns.end();
	 ++iter) {
      NUContAssign* assign = *iter;
      NUStmt* repl = foldAssign (assign);
      if (repl != static_cast<NUStmt*>(assign))
        module->removeContAssign (assign, repl ? dynamic_cast<NUContAssign*>(repl): 0);
    }
    
    was_changed |= isChanged ();
  } while (isChanged ());

  NUEnabledDriverList enabled_drivers;
  module->getContEnabledDrivers(&enabled_drivers);
  for (NUEnabledDriverListIter iter = enabled_drivers.begin();
       iter != enabled_drivers.end();
       ++iter) {
    NUEnabledDriver* driver = *iter;
    NUStmt* stmt = enabledDriver(driver);
    if (stmt == NULL)
      module->removeContEnabledDriver (driver, NULL);
    else if (stmt != driver) {
      switch (stmt->getType ()) {
      case eNUContAssign:
        module->addContAssign (dynamic_cast<NUContAssign*>(stmt));
        module->removeContEnabledDriver (driver, NULL);
        break;
      case eNUEnabledDriver:
        module->removeContEnabledDriver (driver,
                                         dynamic_cast<NUEnabledDriver*>(stmt));
        break;
      default:
        NU_ASSERT2 (false, driver, stmt);
      }
    }
  }

  // Invalidate expressions, this will prevent copy propagation from occurring
  // mistakenly into always blocks through expression cache pollution.
  clearExprCache();

  NUTriRegInitList tri_reg_inits;
  module->getTriRegInit(&tri_reg_inits);
  for (NUTriRegInitListIter iter = tri_reg_inits.begin();
       iter != tri_reg_inits.end();
       ++iter) {
    triRegInit(*iter);
  }

  NUAlwaysBlockList always_blocks;
  module->getAlwaysBlocks(&always_blocks);
  for (NUAlwaysBlockList::iterator iter = always_blocks.begin();
       iter != always_blocks.end(); ++iter)
  {
    NUAlwaysBlock* blk = *iter;
    if (blk->isSequential() && !isNoStmtDelete() && isUDKillable() &&
        (blk->getClockBlock() == NULL)) // always start from clock-block
    {
      sequentialGroup(blk);
    }
    structuredProc(blk);
  } // for

  NUInitialBlockList initial_blocks;
  module->getInitialBlocks(&initial_blocks);
  for (NUInitialBlockListIter iter = initial_blocks.begin();
       iter != initial_blocks.end();
       ++iter) {
    structuredProc(*iter);
  }

  was_changed |= isChanged ();

  if (was_changed && testFoldFlags (eFoldComputeUD))
  {
    mChanged = true;            // Set so our instantiator is warned about changes
    UD ud (mFactory, mMsgContext, mIODB, mArgs, mVerbose);
    ud.module (module);
  }

  // Allow mChanged flag to *stay* set so that instantiating module can
  // record that it needs to redo UD also.
  //
  rememberVisited(module);

  clearExprCache();
}

void FoldI::tf(NUTF *tf)
{
  if (haveVisited(tf)) {
    return;
  }

  push (tf);
  NUStmtVector toDelete;
  stmtList(tf->loopStmts (), &toDelete);
  sRemoveStmts (tf, &NUTF::removeStmt, &toDelete);

  NUStmtList flattened_result;
  if (removeSubBlocks(tf, tf->loopStmts(),&flattened_result)) {
    tf->replaceStmtList(flattened_result);
  }

  pop ();
  
  rememberVisited(tf);
}


void FoldI::moduleInstance(NUModuleInstance *inst, bool recurse)
{
  NUModule *instantiated_module = inst->getModule();
  if (recurse) {
    module(instantiated_module, true);
  }

  // If we fold input port expressions to constant values, we can't pass the
  // constructor parameters because emitModule doesn't handle them....
  UInt32 saveFlags = mFlags;
  putFlags(mFlags | eFoldNoNetConstants);
  for (NUPortConnectionLoop p(inst->loopPortConnections ()); !p.atEnd(); ++p) {
    NUPortConnection* port = *p;
    port->replaceLeaves (*this);
  }
  putFlags(saveFlags);
}

NUStmt* FoldI::enabledDriver(NUEnabledDriver *driver)
{
  driver->replaceLeaves (*this);

  if (isNoStmtDelete ())
    return driver;
#if 0
  // This causes crashes when a NUEnabledDriver participates in a cycle
  // and we replace it after schedule has finished.  Codegen has equivalent
  // code that either avoids coding or unconditionally codes an assignment
  // so, disabling this code doesn't impact performance by much...
  //
  if (NUConst* c = ctce (driver->getEnable ()))
    if (c->isZero ()) {
      // if the enable evaluates to FALSE, we will never execute the
      // strength setting or data value modification - so just remove the
      // statement entirely
      finish (driver, NULL);
      return NULL;
    } else if (c->isOnes ()) {
      // completely driven, replace by a normal assignment or cont assign.
      NULvalue *lv = driver->getLvalue ()->copy (getCC ());
      NUExpr   *rv = driver->getDriver ();
      const SourceLocator& loc = driver->getLoc ();

      if (driver->getType () == eNUContEnabledDriver) {
        NUContEnabledDriver *d = dynamic_cast<NUContEnabledDriver*>(driver);
        NUStmt* result = new NUContAssign (d->getName (), lv, rv, loc,
                                           d->getStrength ());
        return finish (driver, result);
      } else {
        return finish (driver,
                       new NUBlockingAssign (lv, rv,
                                             driver->getUsesCFNet (), loc));
      }
    }
#endif

  return driver;
}



void FoldI::triRegInit(NUTriRegInit *regInit)
{
  regInit->replaceLeaves (*this);
}


void FoldI::structuredProc(NUStructuredProc *proc)
{
  NUBlock * block = proc->getBlock();
  foldBlock(block);
  clearExprCache();
}

// Returns NULL if statement eliminated
// Returns STMT (or new statement)
NUStmt*
FoldI::foldStmt(NUStmt* stmt)
{
  if (not stmt)
    return stmt;

  // I think DesignWalker should do this for me somehow...this
  // code was lifted from there but it wasn't obvious how
  // to take advantage of it.

  if (NUAssign *this_assign = dynamic_cast<NUAssign*>(stmt))
    return foldAssign(this_assign);

  if (NUBlock *block = dynamic_cast<NUBlock*>(stmt)) {
    foldBlock(block);
    return block;
  }

  if (NUIf *if_stmt = dynamic_cast<NUIf*>(stmt))
    return foldIfStmt(if_stmt);

  if (NUCase *case_stmt = dynamic_cast<NUCase*>(stmt))
  {
    NUStmt* result = foldCase(case_stmt);
    NUCase* case_result = dynamic_cast<NUCase*>(result);
    if (case_result)
      NU_ASSERT(case_result->isConsistent(), case_result);
    return result;
  }

  if (NUFor * for_stmt = dynamic_cast<NUFor*>(stmt))
    return foldFor(for_stmt);

  if (NUTaskEnable *enable_stmt = dynamic_cast<NUTaskEnable*>(stmt))
    return taskEnable (enable_stmt);

  if (NUEnabledDriver *driver = dynamic_cast<NUEnabledDriver*>(stmt))
    return enabledDriver (driver);

  if (NUSysTask* task = dynamic_cast<NUSysTask*>(stmt))
    return sysTask (task);

  // We don't have any special handling for this statement.
  // So, just do a replaceLeaves on the subexpressions to fold them
  stmt->replaceLeaves (*this);
  
  return stmt;
} // NUStmt* foldStmt

void FoldI::stmtList (NUStmtLoop stmts, NUStmtVector* toDelete)
{
  for (; !stmts.atEnd (); ++stmts)
  {
    NUStmt* newStmt = foldStmt (*stmts);
    if (newStmt)
      // THIS IS SOMEWHAT TOUCHY.  It ASSUMES that we can do an inplace replacement of
      // a statement by just overwriting what WAS on the list.  It assumes that the StmtLoop
      // is a writeable iterator and that it doesn't invalidate the iterator to deposit thru it.
      //
      *stmts = newStmt;
    else
      // Deleted the statement, put the statement on a list for later cleanup by our caller
      // because THAT would invalidate the iterator to delete out-from-under the loop!
      toDelete->push_back (*stmts);
  }
}

void FoldI::foldBlock(NUBlock *block)
{
  push (block);
  NUStmtVector toDelete;
  stmtList(block->loopStmts (), &toDelete);
  sRemoveStmts (block, &NUBlock::removeStmt, &toDelete);
  
  NUStmtList flattened_result;
  if (removeSubBlocks(block, block->loopStmts(),&flattened_result)) {
    block->replaceStmtList(flattened_result);
  }

  pop ();
}

NUSysTask* FoldI::sysTask (NUSysTask* task)
{
  task->replaceLeaves (*this);

  // No transformations yet for a whole sys function
  return task;
}

NUTaskEnable* FoldI::taskEnable(NUTaskEnable *task_enable)
{
  int idx = 0;
  for (NUTFArgConnectionLoop loop = task_enable->loopArgConnections();
       not loop.atEnd();
       ++loop, ++idx) {

    // Fold the target or expression
    (*loop)->replaceLeaves (*this);

    switch ((*loop)->getDir()) {
    case eInput:
    {
      NUTFArgConnectionInput *iconn = dynamic_cast<NUTFArgConnectionInput*>(*loop);
      // Try to partselect the smallest piece of the source as we need
      // for the destination
      UInt32 argWidth = iconn->getFormal ()->getBitSize ();
      NUExpr* actual = iconn->getActual ();
      NUExpr* foldedActual = foldResizeExpr(actual, argWidth);
      if (foldedActual != NULL) {
        iconn->setActual(foldedActual);
      }
    } // 
    break;

    case eOutput:
      break;

    case eBid:
      break;

    default:
      NU_ASSERT2("Unknown Direction for Port Connection" == 0, 
		 task_enable, (*loop));
      break;
    }
  }

  return task_enable;
}
