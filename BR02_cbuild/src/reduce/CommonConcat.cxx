// -*-C++-*-    $Revision: 1.8 $
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <string.h>
#include "compiler_driver/CarbonContext.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUAlwaysBlock.h"
#include "reduce/Inference.h"
#include "reduce/OccurrenceLogger.h"
#include "reduce/CommonConcat.h"
#include "reduce/InferenceLogger.h"
#include "util/MsgContext.h"
#include "util/UtIOStream.h"

// Enable execution of experimental concat analysis code by setting

// Module-wide processing builds and applies concat analysis to modules at a
// time.
#define ENABLE_MODULE_WIDE_COMMON_CONCAT 0

// Always-block-wide processing builds and applies concat analysis to entire
// always blocks at a time, rather than by the embedded statement list.
#define ENABLE_ALWAYS_BLOCK_WIDE_COMMON_CONCAT 0

#define TWO_PASS 1

/*static*/ UInt32 CommonConcat::sNNets = 0;

static InferenceLogger Log;

CommonConcat::CommonConcat (NUScope *scope, MsgContext *msg_context, const IODBNucleus *iodb, 
  const Inference::Params &params, const CommonConcat *parent) :
  mVerbose (params.isVerboseCommonConcat ()),
  mDoLocalNetElimination (params.doConcatNetElimination ()),
  mProtected (), mParent (parent), mScope (scope), mMsgContext (msg_context), mIODB (iodb),
  mParams (params), mDescriptors (), mLhsOccurrences (*this), mReplacements (),
  mAppliedToStatements (false), mCount ()
{
}

/*virtual*/ CommonConcat::~CommonConcat ()
{
  for (Concat::Map::iterator it = mLhsOccurrences.begin (); it != mLhsOccurrences.end (); it++) {
    delete it->second;
  }
  if (mParent != NULL) {
    mParent->mCount.merge (mCount);
  }
  // Output all the verbose stuff... remember that this output is used for
  // regression gold files so changes to the text will require some regolding.
  if (!mVerbose) {
    // quiet...
  } else if (mParent != NULL) {
    // this is a nested common-concat
  } else if (getCount (cREPLACE_RVALUE) == 0) {
    // nothing was replaced so no vebosity
  } else if (getCount (cREPLACE_RVALUE) == 1 && getCount (cREPLACE_LVALUE) == 1) {
    UtIO::cout () << "Replaced 1 instance of 1 common concat." << UtIO::endl;
  } else if (getCount (cREPLACE_RVALUE) == 1) {
    UtIO::cout () << "Replaced 1 instance of "
      << getCount (cREPLACE_LVALUE) << " common concats." << UtIO::endl;
  } else {
    UtIO::cout () << "Replaced " << getCount (cREPLACE_RVALUE) << " instances of "
      << getCount (cREPLACE_LVALUE) << " common concats." << UtIO::endl;
  }
  // module-wide statistics reporting
  NUModule *module;
  if (!mVerbose) {
    // quiet
  } else if (mParent != NULL) {
    // this is not the module-wide CommonConcat instances so propagate
    // statistics to the parent
  } else if (mScope == NULL || (module = dynamic_cast <NUModule *> (mScope)) == NULL) {
    // it is not a module anyway
  } else {
    // this is the top-level CommonConcat for the module so report the
    // module-wide information.
    statistics (UtIO::cout (), module);
  }
}

//! Parse the options word from the command line
/*static*/ void CommonConcat::parseOptions (const ArgProc *args, UInt32 *options, MsgContext *msg_context)
{
  *options |= (args->getBoolValue(CRYPT("-verboseCommonConcat")) ?  Inference::eVerboseCommonConcat  : 0);
  ArgProc::StrIter it;
  if (args->getStrIter (CRYPT ("-commonConcat"), &it) == ArgProc::eKnown) {
    while (!it.atEnd ()) {
      if (!strcmp (*it, "config")) {
#if ENABLE_MODULE_WIDE_COMMON_CONCAT
        UtIO::cout () << "Module-wide common concat is enabled." << UtIO::endl;
#else
        UtIO::cout () << "Module-wide common concat is disabled." << UtIO::endl;
#endif
#if ENABLE_ALWAYS_BLOCK_WIDE_COMMON_CONCAT
        UtIO::cout () << "Always-block-wide common concat is enabled." << UtIO::endl;
#else
        UtIO::cout () << "Always-block-wide common concat is disabled." << UtIO::endl;
#endif
#if ENABLE_MODULE_WIDE_COMMON_CONCAT
      } else if (!strcasecmp (*it, "modulewide")) {
        *options |= Inference::eModuleWideCommonConcat;
#endif
#if ENABLE_ALWAYS_BLOCK_WIDE_COMMON_CONCAT
      } else if (!strcasecmp (*it, "alwayswide")) {
        *options |= Inference::eAlwaysWideCommonConcat;
#endif
      } else {
        msg_context->RECommonConcatBadOption (*it);
      }
      ++it;
    }
  }
  UInt32 disabled = (args->getBoolValue (CRYPT ("-noCommonConcat")) ? Inference::eCommonConcat : 0)
    | (args->getBoolValue (CRYPT ("-noConcatCollapse")) ? (Inference::eLeftConcatCollapse|Inference::eRightConcatCollapse) : 0)
    | (args->getBoolValue (CRYPT ("-noConcatNetElimination")) ? Inference::eEliminateConcatNets : 0)
    | (args->getBoolValue (CRYPT ("-noProcConcatNetElimination")) ? Inference::eEliminateProcConcatNets : 0)
    ;
  *options &= ~disabled;
}

//
// Left-side discovery pass
//

// The left-side discovery pass looks for left-side concats in a list of
// statements. Output is the mLhsOccurrences map from nets to
// CommonConcat::Concat instances.

//! Look for left-side concats in a list of statements.
bool CommonConcat::leftSideDiscover (NUStmtList &list)
{
  // all the eliminable concats by walking into the NUConcatLvalue instances.
  UInt32 position = 0;                  // ordinal in the statement list
  UInt32 n = 0;                         // number of eliminable concats found
  NUStmtList::const_iterator it;
  for (it = list.begin (); it != list.end (); it++) {
    NUStmt *stmt = *it;
    switch (stmt->getType ()) {
    case eNUContAssign:
      {
        NUContAssign *assign = dynamic_cast <NUContAssign *> (stmt);
        NU_ASSERT (assign != NULL, stmt);
        NUConcatLvalue *lconcat = dynamic_cast <NUConcatLvalue *> (assign->getLvalue ());
        if (lconcat == NULL) {
          // this is not a lhs concat
        } else if (assign->getRvalue ()->drivesZ () && !canBeEliminated (assign->getLvalue ())) {
          // tristates are not propagated across assignment so anything with a
          // Z cannot be replaced by a common concat temporary unless the net
          // is eliminated and globally replaced by a bitsel/partsel of the
          // common concat temporary.
          Log (CommonConcat::cLVALUE_CONCAT_REJECTED, assign);
        } else if (addConcat (Concat::cUNSPECIFIED, 0, lconcat, assign)) {
          n++;
        }
      }
      break;
    case eNUNonBlockingAssign:
    case eNUBlockingAssign:
      {
        killConcats (stmt, position);
        NUAssign *assign = dynamic_cast <NUAssign *> (stmt);
        NU_ASSERT (assign != NULL, stmt);
        NUConcatLvalue *lconcat = dynamic_cast <NUConcatLvalue *> (assign->getLvalue ());
        if (lconcat == NULL) {
          // this is not a lhs concat
        } else if (assign->getRvalue ()->drivesZ ()) {
          // tristates are not propagated across assignment so anything with a
          // Z cannot be eliminated
        } else if (addConcat (Concat::cUNSPECIFIED, position, lconcat, assign)) {
          n++;
        }
      }
      break;
    default:
      killConcats (stmt, position);
      break;
    }
    position++;
  }
  return n > 0;                         // return iff any concats were found
}

//! Add a left side concat to the concat occurrence map.
bool CommonConcat::addConcat (const Concat::Flavour flavour, const UInt32 rank,
                              NUConcatLvalue *concat, NUAssign *assign)
{
  // build a list of lvalues in the concat
  Log (cLVALUE_CONCAT, assign);
  NUNetList nets;
  NUNetSet uses;
  assign->getRvalue ()->getUses (&uses);
  NULvalueLoop loop (concat->loopLvalues ());
  while (!loop.atEnd ()) {
    const NULvalue *lvalue = *loop;
    switch (lvalue->getType ()) {
    case eNUIdentLvalue:
      {
        NUNet *net = dynamic_cast <const NUIdentLvalue *> (lvalue)->getIdent ();
        if (isProtected (net) || (net->isTristate () && !canBeEliminated (net))) {
          // tristates are not propagated across assignment so anything with a
          // Z cannot be eliminated unless it is a local net and all
          // occurrences will be replaced by the common-concat temporary.
          return false;
        } else if (uses.find (net) != uses.end ()) {
          return false;
        } else {
          nets.push_back (net);
        }
      }
      break;
    default:
      // this is an item type that cannot be recognised in an eliminable concat
      return false;
      break;
    }
    ++loop;
  }
  if (nets.empty ()) {
    return false;                       // degenerate concat
  }
  Concat *descriptor = new Concat (*this, flavour, rank, concat, assign, nets);
  mLhsOccurrences.add (descriptor);
  // Need to keep a list of the descriptors in the order that they were found
  // in the statement list. 
  mDescriptors.push_back (descriptor);
  if (assign->getType () != eNUContAssign) {
    // The common concat must is killed by any subsequent assignments to any of
    // the nets on the lhs. So, we add all the nets to the killing maps.
    NUNetSet defs;
    assign->getBlockingDefs (&defs);
    NUNetSet::iterator it;
    for (it = defs.begin (); it != defs.end (); ++it) {
      mKilling.insert (std::pair <NUNet *, Concat *> (*it, descriptor));
    }
  }
  return true;
}

//! Kill any concats containing nets defined by a statement
void CommonConcat::killConcats (NUStmt *stmt, const UInt32 position)
{
  NU_ASSERT (stmt->getType () != eNUContAssign, stmt);
  Concat::AssignKiller killer (mKilling, position);
  NUDesignWalker walk (killer, false);
  walk.stmt (stmt);
}

/*virtual*/ NUDesignCallback::Status CommonConcat::Concat::AssignKiller::operator () 
  (Phase, NUIdentLvalue *ident)
{
  NUNet *net = ident->getIdent ();
  Concat::Map::iterator killed = mKilling.find (net);
  while (killed != mKilling.end ()) {
    killed->second->killAt (mPosition);
    killed++;
  }
  return eNormal;
}

//
// Right-side discovery pass
//

// Right-side discovery looks for occurrences of left-side concats (in the
// mLhsOccurrences map) in expressions. Output is in the mReplacements map from
// expressions to concats.

//! Seek left-side concats in the expressions embedded in a list of statements
bool CommonConcat::rightSideDiscover (NUStmtList &list)
{ 
  if (mLhsOccurrences.empty ()) {
    return false;                       // no left-hand concats discovered
  } else {
    RightSide rhs_discovery (*this, mLhsOccurrences, mVerbose);
    return rhs_discovery (&list);
  }
}

//! Traverse a statement list calling replaceLeaves for each statement.
bool CommonConcat::RightSide::operator () (NUStmtList *list)
{
  NUStmtList::iterator it;
  mPosition = 0;
  bool retval = false;
  UInt32 lineno = 0;
  for (it = list->begin (); it != list->end (); it++) {
    mStmt = *it;
    mType = mStmt->getType ();
    // The expr walker needs to know the position in the statement list of 
    // enclosing procedural statements in order to test for liveness of
    // concats. For continuous assigns, the position of the defining assign is
    // given as 0, the position of everthing is given as 1, so they are always
    // assumed to be live.
    mPosition = mType == eNUContAssign ? 1 : lineno++;
    // The expr walker needs to know the strength of the enclosing continuous
    // assign. Replacement of concats must be senstive to the strength of the
    // assignment that induced the concat. Ignored for procedural assigns.
    mStrength = mType == eNUContAssign ? 
      dynamic_cast <NUContAssign *> (mStmt)->getStrength () : (Strength) 0;
#if TWO_PASS
    mFoundReplacement = false;
    mStmt->replaceLeaves (*this);
    retval |= mFoundReplacement;
#else
    retval |= mStmt->replaceLeaves (*this);
#endif
  }
  return retval;                        // return iff any substitutions happened
}

//! Look for NUConcatRvalue instances that match a left-side concat.
/*virtual*/ NUExpr *CommonConcat::RightSide::operator () (NUExpr *expr, Phase phase)
{
  NUConcatOp *concat;
  Concat *descr;
  NUExpr *replacement;
  UInt32 width;
  if (phase != ePre) {
    return NULL;
  } else if (expr->getType () != NUExpr::eNUConcatOp) {
    replacement = NULL;
  } else if ((concat = dynamic_cast <NUConcatOp *> (expr)) == NULL) {
    NU_ASSERT (concat != NULL, expr);   // bang!
    replacement = NULL;                 // eliminate gcc warning
  } else if (!mMap.match (concat, mPosition, &descr, &width, mType, mStrength)) {
    // does not match any concat
    replacement = NULL;
  } else {
#if TWO_PASS
    descr->foundRightSideOccurrence (expr, width);
    mFoundReplacement = true;
    replacement = NULL;
#else
    // OK, this concat matches a lhs concat
    NUNet *net = mCommonConcat.getReplacementNet (descr);
    if (net->getBitSize () == width) {
      // matched the entire lvalue concat
      replacement = new NUIdentRvalue (net, expr->getLoc ());
    } else if (net->getBitSize () > width) {
      // matched a prefix of the lvalue concat
      UInt32 offset = net->getBitSize () - width;
      ConstantRange range (net->getBitSize () - 1, offset);
      replacement = new NUVarselRvalue (net, range, expr->getLoc ());
    } else {
      NU_ASSERT (net->getBitSize () >= width, expr);
      replacement = NULL;               // crush a gcc warning
    }
    replacement->resize (width);
    // descr->protect (mCommonConcat);
    if (mVerbose && descr->getCount (cREPLACE_RVALUE) == mLowCount) {
      UtIO::cout () << "Common-concat substitutions:" << UtIO::endl;
    }
    descr->count (cREPLACE_RVALUE);
    Log (CommonConcat::cREPLACED_RVALUE_CONCAT, net, expr, replacement);
    if (mVerbose) {
      UtString b0, b1, b2;
      expr->compose (&b0, NULL);
      replacement->compose (&b1, NULL);
      expr->getLoc ().compose (&b2);
      UtIO::cout () << "  replaced " << b0 << " with " << b1 << ": " << b2 << UtIO::endl;
    }
#endif
  }
  if (replacement != NULL) {
    delete expr;
  }
  return replacement;
}

//! Note the occurrence of this concat in a right-side concat.
void CommonConcat::foundRightSideOccurrence (NUExpr *expr, Concat *descr, const UInt32 width)
{
  mReplacements.insert (expr, descr, width);
}

//
// Local net replacement and elimination pass
//

// Local net replacement looks for instances of nets that are subsumed by a
// concat temporary net and replaces those net occurrence with a bitsel or
// partsel of the concat temporary.

//! Look for occurrences of nets as actual parameters and protect them from CommonConcat.
class CommonConcatActualSeeker : public NUDesignCallback {
public:
  CommonConcatActualSeeker (CommonConcat &context) : mCommonConcat (context) {}

  //! Base operator does nothing.
  virtual Status operator()(Phase, NUBase *) { return eNormal; }

  //! Skip sub modules
  virtual Status operator()(Phase, NUModuleInstance*) { return eSkip; }

  //! Protect the actuals in a port connection.
  virtual Status operator()(Phase phase, NUPortConnection *node) 
  {
    if (phase == ePre) {
      NUNetSet actuals;
      node->getActuals (&actuals);
      protect (actuals, node);
    }
    return eNormal;
  }

  //! Protect the actuals in a task or function input connection.
  virtual Status operator()(Phase phase, NUTFArgConnectionInput *node) 
  { 
    if (phase == ePre) {
      NUExpr *actual = node->getActual ();
      NUNetSet actuals;
      switch (actual->getType ()) {
      case NUExpr::eNUVarselRvalue:
        {
          // get the use nets in the ident expression
          NUVarselRvalue *rvalue = dynamic_cast <NUVarselRvalue *> (actual);
          NU_ASSERT (rvalue != NULL && rvalue->getIdentExpr () != NULL, node);
          rvalue->getIdentExpr ()->getUses (&actuals);
        }
        break;
        // Note that common concat does not make concats out of memory nets so
        // no specialisation for NUMemselRvalue is required.
      default:
        // get the use nets of the entire expression
        actual->getUses (&actuals);
        break;
      }
      protect (actuals, node);
    }
    return eNormal;
  }
  
  //! Protect the actuals in a task or function output connection.
  virtual Status operator()(Phase phase, NUTFArgConnection *node) 
  { 
    if (phase == ePre) {
      NUNetSet actuals;
      node->getActuals (&actuals);
      protect (actuals, node);
    }
    return eNormal;
  }
  
  virtual Status operator()(Phase phase, NUCModelArgConnectionInput* node)
  {
    if (phase == ePre) {
      NUExpr *actual = node->getActual ();
      NUNetSet actuals;
      switch (actual->getType ()) {
      case NUExpr::eNUVarselRvalue:
        {
          // get the use nets in the ident expression
          NUVarselRvalue *rvalue = dynamic_cast <NUVarselRvalue *> (actual);
          NU_ASSERT (rvalue != NULL && rvalue->getIdentExpr () != NULL, node);
          rvalue->getIdentExpr ()->getUses (&actuals);
        }
        break;
        // Note that common concat does not make concats out of memory nets so
        // no specialisation for NUMemselRvalue is required.
      default:
        // get the use nets of the entire expression
        actual->getUses (&actuals);
        break;
      }
      protect (actuals, node);
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUCModelArgConnectionOutput* node)
  { 
    if (phase == ePre) {
      NU_ASSERT (node->getDir () == eOutput || node->getDir () == eBid, node);
      NUNetSet actuals;
      node->getDefs (&actuals);
      protect (actuals, node);
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUCModelArgConnectionBid* node)
  { 
    if (phase == ePre) {
      NU_ASSERT (node->getDir () == eOutput || node->getDir () == eBid, node);
      NUNetSet actuals;
      node->getDefs (&actuals);
      protect (actuals, node);
    }
    return eNormal;
  }

private:

  CommonConcat &mCommonConcat;          //!< enclosing context

  //! Protect all the nets in set.
  /*! \note The node parameter is used only for diagnostic information in
   *   CommonConcat::protectActual.
   */
  void protect (NUNetSet &nets, NUUseDefNode *node)
  {
    NUNetSet::iterator it;
    for (it = nets.begin (); it != nets.end (); ++it) {
      mCommonConcat.protectActual (*it, node);
    }
  }

  //! Protect all the nets in a set.
  /*! \note The node parameter is used only for diagnostic information in
   *   CommonConcat::protectActual.
   */
  void protect (NUNetSet &nets, NUCModelArgConnection *node)
  {
    NUNetSet::iterator it;
    for (it = nets.begin (); it != nets.end (); ++it) {
      mCommonConcat.protectActual (*it, node);
    }
  }

};

//! Walk a module looking for nets that must not be touched by common concat
void CommonConcat::winnowProtectedNets (NUModule *module)
{
  CommonConcatActualSeeker seek_actuals (*this);
  NUDesignWalker walk (seek_actuals, false);
  walk.module (module);
}

//! Eliminate any local nets in a statement list that were part of an applied common concat.
bool CommonConcat::eliminateLocalNets (NUStmtList &list)
{
  if (list.empty ()) {
    return false;                       // the boring case...
  }
  NU_ASSERT (mDoLocalNetElimination, *list.begin ());
  NUStmtList::iterator it;
  // construct the leaf rewriter
  Concat::Eliminator eliminator (*this);
  mLhsOccurrences.build (&eliminator);
  if (eliminator.getSize () == 0) {
    // no candidates for elimination so no point bothering with the traversal
  } else {
    // blow away the nets
    UInt32 n = 0;
    for (it = list.begin (); it != list.end (); it++) {
      eliminator.setLine (n++);
      (*it)->replaceLeaves (eliminator);
    }
  }
  if (!mVerbose) {
    // quiet...
  } else if (eliminator.getSize () == 0) {
    // no nets to replaces
  } else if (eliminator.getCount () == 0) {
    // none replaced
  } else {
    UtIO::cout () << "Concat-eliminated " << eliminator.getCount () << " instances of "
      << eliminator.getSize () << " local nets" << UtIO::endl;
    eliminator.pr (UtIO::cout (), false);
  }
  return eliminator.getCount () > 0;
}

//! Eliminate any local nets in a module that were part of an applied common concat.
bool CommonConcat::eliminateLocalNets (NUModule *module)
{
  NU_ASSERT (mDoLocalNetElimination, module);
  NU_ASSERT (module == mScope, module);
  // walk the module checking for nets that are used as actual parameters
  CommonConcatActualSeeker seek_actuals (*this);
  NUDesignWalker walk (seek_actuals, false);
  walk.module (module);
  // construct the leaf rewriter
  Concat::Eliminator eliminator (*this);
  mLhsOccurrences.build (&eliminator);
  if (eliminator.getSize () == 0) {
    // no candidates for elimination so no point bothering with the traversal
  } else {
    // blow away the nets
    module->replaceLeaves (eliminator);
  }
  if (!mVerbose) {
    // quiet...
  } else if (eliminator.getSize () == 0) {
    // no nets to replaces
  } else if (eliminator.getCount () == 0) {
    // none replaced
  } else {
    UtIO::cout () << "Concat-eliminated " << eliminator.getCount () << " instances of "
      << eliminator.getSize () << " local nets" << UtIO::endl;
    eliminator.pr (UtIO::cout (), true);
  }
  return eliminator.getCount () > 0;
}

//
// Right-side replacement pass
//

// Replace qualifying occurrences of left-side concats that are repeated in
// expressions with the concat temporary net.

//! \class CommonConcatRightReplacement

/*! Use replaceLeaves to apply a concat replacement map to right-side concat
 *  occurrences,
 */

class CommonConcatRightReplacement : public NuToNuFn {
public:

  CommonConcatRightReplacement (CommonConcat &concat, 
                                CommonConcat::Concat::ReplacementMap &map, 
                                bool verbose) : 
    mCommonConcat (concat), 
    mMap (map), 
    mVerbose (verbose),
    mCount (0)
  {}

  //! Traverse a statement list calling replaceLeaves for each statement.
  bool operator () (NUStmtList *list)
  {
    NUStmtList::iterator it;
    bool retval = false;
    for (it = list->begin (); it != list->end (); it++) {
      NUStmt *stmt = *it;
      retval |= stmt->replaceLeaves (*this);
    }
    return retval;                        // return iff any substitutions happened
  }

  bool operator () (NUModule *module)
  {
    return module->replaceLeaves (*this);
  }

private:

  CommonConcat &mCommonConcat;          //!< enclosing common concat
  CommonConcat::Concat::ReplacementMap &mMap;
                                        //!< the map of expressions to replace
  const bool mVerbose;                  //!< be noisy or quiet
  UInt32 mCount;

  //! Replace NUConcatRvalue instances that match a left-side concat.
  virtual NUExpr *operator () (NUExpr *expr, Phase)
  {
    CommonConcat::Concat::ReplacementMap::iterator it;
    if ((it = mMap.find (expr)) == mMap.end ()) {
      return NULL;                      // not a replacement
    } else if (it->second.mConcat->isOrphan () && !mCommonConcat.canBeEliminated (expr)) {
      // do not make the replacement
      if (mVerbose) {
        UtString b0;
        expr->compose (&b0, NULL);
        UtIO::cout () << "  orphan common-concat suppressed: " << b0 << UtIO::endl;
        if (mCommonConcat.canBeReplaced (expr)) {
          UtIO::cout () << "  **** canBeReplaced: " << b0 << UtIO::endl;
        }
      }
      Log (CommonConcat::cORPHAN_RIGHT_SIDE_IGNORED, expr);
      return NULL;
    } else {
      CommonConcat::Concat *descr = it->second.mConcat;
      UInt32 width = it->second.mWidth;
      NUExpr *replacement;
      NUNet *net = NULL;
      // OK, this concat matches a lhs concat
      net = mCommonConcat.getReplacementNet (descr);
      if (net->getBitSize () == width) {
        // matched the entire lvalue concat
        replacement = new NUIdentRvalue (net, expr->getLoc ());
      } else if (net->getBitSize () > width) {
        // matched a prefix of the lvalue concat
        UInt32 offset = net->getBitSize () - width;
        ConstantRange range (net->getBitSize () - 1, offset);
        replacement = new NUVarselRvalue (net, range, expr->getLoc ());
      } else {
        NU_ASSERT (net->getBitSize () >= width, expr);
        replacement = NULL;               // crush a gcc warning
      }
      replacement->resize (width);
      // descr->protect (mCommonConcat);
      if (mVerbose && mCount == 0) {
        UtIO::cout () << "Common-concat substitutions:" << UtIO::endl;
      }
      descr->count (CommonConcat::cREPLACE_RVALUE);
      Log (CommonConcat::cREPLACED_RVALUE_CONCAT, net, expr, replacement);
      if (mVerbose) {
        UtString b0, b1, b2;
        expr->compose (&b0, NULL);
        replacement->compose (&b1, NULL);
        expr->getLoc ().compose (&b2);
        UtIO::cout () << "  replaced " << b0 << " with " << b1 << ": " << b2 << UtIO::endl;
      }
      delete expr;
      mCount++;
      return replacement;
    }
  }

};

bool CommonConcat::rightSideReplace (NUModule *module)
{
  CommonConcatRightReplacement replacement (*this, mReplacements, mVerbose);
  return replacement (module);
}

bool CommonConcat::rightSideReplace (NUStmtList *list)
{
  CommonConcatRightReplacement replacement (*this, mReplacements, mVerbose);
  return replacement (list);
}

//
// Left-side replacement pass
//


UInt32 CommonConcat::leftSideReplace (NUModule *module)
{
  NUContAssignList cont_assigns;
  module->getContAssigns (&cont_assigns);
  NUStmtList cont_stmts;
  for (NUContAssignList::iterator it0 = cont_assigns.begin(); it0 != cont_assigns.end(); ++it0) {
    NUStmt *stmt = (*it0);
    cont_stmts.push_back (stmt);
  }
  UInt32 retval = leftSideReplace (cont_stmts);
  // convert the cont_stmts back into cont_assigns.
  cont_assigns.clear();
  for (NUStmtList::iterator it1 = cont_stmts.begin(); it1 != cont_stmts.end(); ++it1) {
    NUStmt *stmt = (*it1);
    NUContAssign *assign = dynamic_cast<NUContAssign *> (stmt);
    NU_ASSERT (assign, stmt);
    cont_assigns.push_back (assign);
  }
  // replace the module assign list with the collapsed cont_assigns.
  module->replaceContAssigns (cont_assigns);
  return retval;
}

//! Replace the left-side of any applied common concats
UInt32 CommonConcat::leftSideReplace (NUStmtList &list)
{
  Concat::List::const_iterator descr_it;
  NUStmtList::iterator stmt_it = list.begin ();
  UInt32 n_replaced = 0;                // number replaced by this call
  for (descr_it = mDescriptors.begin (); descr_it != mDescriptors.end (); descr_it++) {
    Concat *descr = *descr_it;
    if (!descr->isEliminated ()) {
      // The concat encapsulated in the descriptor was not used in an
      // expression so just skip past.
      NU_ASSERT (descr->getCount (cREPLACE_RVALUE) == 0, *list.begin ());
      continue;
#if TWO_PASS
    } else if (descr->isOrphan ()) {
      continue;
#endif
    }
    // Replace the lvalue of the assignment with an assignment to the
    // elimination net.
    NUAssign *assign = descr->getAssign ();
    NULvalue *old_lvalue = assign->getLvalue ();
    NUIdentLvalue *new_lvalue = new NUIdentLvalue (descr->getReplacementNet (NULL),
      old_lvalue->getLoc ());
    // Do not delete the old lvalue here because it will be used in a
    // constructed assignment.
    assign->replaceLvalue (new_lvalue, /*delete_old = */ false);
    NU_ASSERT (descr->getLvalue () == old_lvalue, assign);
    if (mVerbose && n_replaced == 0) {
      UtIO::cout () << "Common-concat net assignments:" << UtIO::endl;
    }
    if (mVerbose) {
      UtString b0;
      assign->compose (&b0, NULL);
      UtIO::cout () << "  " << b0;
    }
    n_replaced++;
    Log (cREPLACED_LVALUE_CONCAT, assign, old_lvalue);
    // Now keep looking through the statement list for the initial
    // assignment. The descriptor list is constructed, in order, as the
    // statement list is sequentially traversed so we should always find the
    // appropriate posintion in the list by just advancing the statement
    // iterator. If the statement does not appear, then either this method has
    // been passed the wrong list, or the list has been screwed about with!
    // Either is not a good thing for common concat elimination.
    while (stmt_it != list.end () && descr->getAssign () != *stmt_it) {
      stmt_it++;
    }
    NU_ASSERT (stmt_it != list.end (), descr->getAssign ());
    NUStmtList new_assign;
    if (!descr->buildDefiningAssign (&new_assign)) {
      // this is bad... this is really bad... just give up... construction of
      // the new assignment statements failed
      NU_ASSERT (false, assign);
    }
    stmt_it++;
    list.insert (stmt_it, new_assign.begin (), new_assign.end ());
  }
  count (cREPLACE_LVALUE, n_replaced);  // track total number of replacements
  return n_replaced;
}

//
// Concat transformation qualifying predicates
//

//! Can an lvalue be safely eliminated for common-concat replacement.
bool CommonConcat::canBeEliminated (const NULvalue *lvalue, const UInt32 qualifiers) const
{
  NUNetSet def_nets;
  lvalue->getDefs (&def_nets);
  NUNetSet::iterator it;
  for (it = def_nets.begin (); it != def_nets.end (); ++it) {
    if (!canBeEliminated (*it, qualifiers)) {
      return false;
    }
  }
  return true;
}

//! Can the net encapsulated by an expression be eliminated.
bool CommonConcat::canBeEliminated (const NUExpr *expr, const UInt32 qualifiers) const
{
  const NUIdentRvalue *ident;
  const NUVarselRvalue *varsel;
  if ((ident = dynamic_cast <const NUIdentRvalue *> (expr)) != NULL) {
    return canBeEliminated (ident->getIdent (), qualifiers);
  } else if ((varsel = dynamic_cast <const NUVarselRvalue *> (expr)) != NULL) {
    return canBeEliminated (varsel->getIdentNet (), qualifiers);
  } else {
    return false;
  }
}

//! Can a net be safely eliminated for common-concat replacement.
bool CommonConcat::canBeEliminated (const NUNet *net, const UInt32 /*qualifiers*/) const
{
  return net != NULL
    && net->getScope () == mScope
    && !net->isPullUp () && !net->isPullDown ()
    && !net->isEdgeTrigger ()           // edge triggers must be scalar nets
    && !net->isProtected (mIODB)        // cannot be protected
    && !net->isPort ()                  // or a port
    && !net->isPrimaryPort ()
    && !net->isHierRef ()
    && !isProtected (net)
    ;
}

//! Can an expression be replaced by a partition or bit of a common concat.
bool CommonConcat::canBeReplaced (const NUExpr *expr, const UInt32 qualifiers) const
{
  const NUIdentRvalue *ident;
  const NUVarselRvalue *varsel;
  if ((ident = dynamic_cast <const NUIdentRvalue *> (expr)) != NULL) {
    return canBeReplaced (ident->getIdent (), qualifiers);
  } else if ((varsel = dynamic_cast <const NUVarselRvalue *> (expr)) != NULL) {
    return canBeReplaced (varsel->getIdentNet (), qualifiers);
  } else {
    return false;
  }
}

//! Can a net be safely eliminated for replacement by a common-concat temporary reference
bool CommonConcat::canBeReplaced (const NUNet *net, const UInt32 qualifiers) const
{
  return mDoLocalNetElimination
    && !net->isTristate ()              // tristates can be eliminated but not merely replaced
    && !net->isEdgeTrigger ()           // edge triggers must be scalar nets
    && !net->isProtected (mIODB)        // cannot be protected
    && (!net->isPort () || (qualifiers & cALLOW_PORT)) // or a port
    && !net->isPrimaryPort ()
    && !net->isHierRef ()
    && !net->isPullUp () && !net->isPullDown ()
    && !isProtected (net)
    ;
}

// Nucleus objects are marked as protected by adding an entry to mProtected. 

//! Mark all the lvalues in a concat as protected from common concat elimination
void CommonConcat::protect (const NUConcatLvalue *concat)
{
  NULvalueCLoop loop (concat->loopLvalues ());
  while (!loop.atEnd ()) {
    const NULvalue *lvalue = *loop;
    if (mProtected.find (lvalue) == mProtected.end ()) {
      mProtected.insert (lvalue);
    }
    ++loop;
  }
}

//! Mark an lvalue as protected from common concat elimination
void CommonConcat::protect (const NULvalue *lvalue)
{
  if (mProtected.find (lvalue) == mProtected.end ()) {
    mProtected.insert (lvalue);
  }
}

//! Mark an expression as protected from common concat elimination
void CommonConcat::protect (const NUExpr *expr)
{
  if (mProtected.find (expr) == mProtected.end ()) {
    mProtected.insert (expr);
  }
}

//! Mark a net as protected against replacement or elimination.
void CommonConcat::protectActual (const NUNet *net, const NUUseDefNode * /*node*/)
{
  if (mProtected.find (net) == mProtected.end ()) {
#if 0
    // Adds a little too much noise to the log file, but it can be usefull
    // during debugging... if issues arise with replacement of nets that should
    // have been protected then enable this code, and the corresponding code in
    // the other variant of protectActual and run it again.
    log (cPROTECT_ACTUAL, net, node);
#endif
    mProtected.insert (net);
  }
}

//! Mark a net as protected against replacement or elimination.
void CommonConcat::protectActual (const NUNet *net, const NUCModelArgConnection * /*node*/)
{
  if (mProtected.find (net) == mProtected.end ()) {
#if 0
    // adds a little too much noise to the log file, but it can be usefull
    // during debugging
    log (cPROTECT_ACTUAL, net, node);
#endif
    mProtected.insert (net);
  }
}

//! Mark a net as protected against replacement or elimination.
void CommonConcat::protect (const NUNet *net)
{
  if (mProtected.find (net) == mProtected.end ()) {
    mProtected.insert (net);
  }
}

//
// Diagnostics and reporting
//

//! output the verbose statistics for a module
void CommonConcat::statistics (UtOStream &s, const NUModule *module) const
{
  if (mCount.isNonZero ()) {
    s << "Common-concat for " << module->getName ()->str () << ":" << UtIO::endl;
    mCount.statistics (s, 2|Count::cBRIEF);
  }
}

//! dump the statistics with a comment
void CommonConcat::statistics (UtOStream &s, const char *fmt, ...) const
{
  char buffer [256];
  va_list ap;
  va_start (ap, fmt);
  int n = vsnprintf (buffer, sizeof (buffer), fmt, ap);
  va_end (ap);
  if (buffer [n-1] == '\n') {
    buffer [n-1] = '\0';
  }
  if (mCount.isNonZero ()) {
    s << buffer << ": no concat substitutions" << UtIO::endl;
  } else {
    s << buffer << UtIO::endl;
    mCount.statistics (s, 2|Count::cBRIEF);
  }
}

//! Print a human readable representation of the common concat instance to stdout
void CommonConcat::pr (bool deep) const
{
  pr (UtIO::cout (), deep);
}

//! Print a human readable representation of the common concat instance to a stream
void CommonConcat::pr (UtOStream &s, bool deep) const
{
  if (mLhsOccurrences.empty ()) {
    s << "No eliminable concats" << UtIO::endl;
  } else {
    s << "******************** CommonConcat ********************" << UtIO::endl;
    s << "number of eliminable concats: " << mLhsOccurrences.size () << UtIO::endl;
    if (!mProtected.empty ()) {
      UtSet <const NUBase *>::const_iterator it;
      s << "number of protected items: " << mProtected.size () << ":" << UtIO::endl;
      for (it = mProtected.begin (); it != mProtected.end (); it++) {
        const NUBase *item = *it;
        const NULvalue *lvalue;
        const NUNet *net;
        if ((net = dynamic_cast <const NUNet *> (item)) != NULL) {
          s << "protected net:    " << net->getName ()->str () << UtIO::endl;
        } else if ((lvalue = dynamic_cast <const NULvalue *> (item)) != NULL) {
          UtString b;
          lvalue->compose (&b, NULL, 9, true);
          s << "protected lvalue: " << b << UtIO::endl;
        } else {
          char b [64];
          sprintf (b, "0x%08lx", (unsigned long) item);
          s << "protected item:   " << b << UtIO::endl;
        }
      }
    }
    dumpConcatList (s, deep);
  }
}

//
// CommonConcat::Concat implementation
//

void CommonConcat::Concat::foundRightSideOccurrence (NUExpr *expr, const UInt32 width)
{
  mNRightSides++;
  mCommonConcat.foundRightSideOccurrence (expr, this, width);
  NUConcatOp *concat = dynamic_cast <NUConcatOp *> (expr);
  NU_ASSERT (concat != NULL, expr);
  UInt32 size = 0;
  UInt32 i = 0;
  while (size < width) {
    // protect these expressions from local net replacement
    size += concat->getArg (i)->getBitSize ();
    mCommonConcat.protect (concat->getArg (i));
    i++;
  }
}

//! Initialise an eliminator functor with this concat.
void CommonConcat::Concat::build (Eliminator *eliminator)
{
  NUNetList::reverse_iterator it;       // iterate from lsb to msb
  SInt32 lsb = 0;                       // track the position of the net in the concat
  for (it = mNets.rbegin (); it != mNets.rend (); it++) {
    NUNet *net = *it;
    const UInt32 size = net->getBitSize ();
    if (mCommonConcat.canBeEliminated (net)) {
      // HACK ALERT - do not always need to create this net, and this is a
      // weird way to get the scope
      (void) mCommonConcat.getReplacementNet (this);
      NU_ASSERT (mEliminator != NULL, mLvalue);
      NU_ASSERT (!mCommonConcat.isProtected (net), net);
      mRemoved.insert (net);
      eliminator->define (this, net, mEliminator, ConstantRange (lsb + size - 1, lsb));
    } else if (mCommonConcat.canBeReplaced (net)) {
      // HACK ALERT - do not always need to create this net, and this is a
      // weird way to get the scope
      (void) mCommonConcat.getReplacementNet (this);
      NU_ASSERT (mEliminator != NULL, mLvalue);
      eliminator->define (this, net, mEliminator, ConstantRange (lsb + size - 1, lsb));
    }
    lsb += size;
  }
}

//! Add an entry for each applied concat to a local net eliminator.
void CommonConcat::Concat::Occurrences::build (Eliminator *eliminator)
{
  iterator it;
  for (it = begin (); it != end (); it++) {
    Concat *concat = it->second;
#if TWO_PASS
    if (concat->getNumRightSides () == 0) {
      // No right-side occurrences of this left-side concat were found so do
      // not eliminate the net.
    } else {
      concat->build (eliminator);
    }
#else
    if (concat->getCount (cREPLACE_RVALUE) == 0) {
      // No right-side occurrences of this left-side concat were found so do
      // not eliminate the net.
    } else {
      concat->build (eliminator);
    }
#endif
  }
}

CommonConcat::Concat::Eliminator::~Eliminator ()
{
  Map::const_iterator it;
  for (it = mMap.begin (); it != mMap.end (); it++) {
    struct Replacement replacement = it->second;
    replacement.mConcat->count (cREPLACE_LOCAL_NET, replacement.mCount);
  }
}

//! Define an eliminable net.
void CommonConcat::Concat::Eliminator::define (Concat *concat, 
  const NUNet *net, NUNet *replacement, ConstantRange range)
{
  mMap.insert (std::pair <const NUNet *, struct Replacement> (net, Replacement (concat, replacement, range)));
}

//! Match NUIdentRvalue or NUVarselRvalues against the eliminable nets.
/*virtual*/ NUExpr *CommonConcat::Concat::Eliminator::operator () (NUExpr *expr, Phase)
{
  switch (expr->getType ()) {
  case NUOp::eNUIdentRvalue:
    {
      NUNet *net = dynamic_cast <NUIdentRvalue *> (expr)->getIdent ();
      NU_ASSERT (net != NULL, expr);
      Map::iterator it;
      NU_ASSERT (net != NULL, expr);
      if (mCommonConcat.isProtected (expr)) {
        // leave it alone
        return NULL;
      } else if ((it = mMap.find (net)) == mMap.end ()) {
        // this is not a local net that can be eliminated by the common concat
        // replacement
        return NULL;
      } else {
        // blow it away...
        it->second.mConcat->foundReplacement ();
        NUExpr *replacement = new NUVarselRvalue (it->second.mNet, it->second.mRange,
                                                  expr->getLoc ());
        replacement->resize (it->second.mRange.getLength ());
        mNReplaced++;                   // count it...
        it->second.mCount++;
        Log (CommonConcat::cREPLACED_LOCAL_NET_RVALUE, net, expr, replacement);
        delete expr;
        return replacement;
      }
    }
    break;
  case NUOp::eNUVarselRvalue:
    {
      NUVarselRvalue *varsel = dynamic_cast <NUVarselRvalue *> (expr);
      NUNet *net = varsel->getIdentNet (false);
      Map::iterator it;
      if (mCommonConcat.isProtected (expr)) {
        // leave it alone
        return NULL;
      } else if (net == NULL || (it = mMap.find (net)) == mMap.end ()) {
        // this is not a local net that can be eliminated by the common concat
        // replacement
        return NULL;
      } else if (varsel->isConstIndex ()) {
        // grab the range
        const ConstantRange *range = varsel->getRange ();
        NU_ASSERT (range->getLength () <= it->second.mRange.getLength (), varsel);
        SInt32 offset = it->second.mRange.getLsb ();
        ConstantRange new_range (range->getMsb () + offset, range->getLsb () + offset);
        NUExpr *replacement = new NUVarselRvalue (it->second.mNet, new_range, expr->getLoc ());
        replacement->resize (range->getLength ());
        mNReplaced++;                   // count it
        it->second.mCount++;
        Log (CommonConcat::cREPLACED_LOCAL_NET_RVALUE, net, expr, replacement);
        delete expr;
        return replacement;
      } else {
        // grab the range and the ident expression
        const ConstantRange *range = varsel->getRange ();
        CopyContext ctx (NULL, NULL);
        // need to copy the index because the original nucleus will get blown
        // away when expr is deleted
        NUExpr *index = varsel->getIndex ()->copy (ctx);
        NU_ASSERT (range->getLength () <= it->second.mRange.getLength (), varsel);
        SInt32 offset = it->second.mRange.getLsb ();
        ConstantRange new_range (range->getMsb () + offset, range->getLsb () + offset);
        NUExpr *replacement = new NUVarselRvalue (it->second.mNet, index, new_range, expr->getLoc ());
        replacement->resize (range->getLength ());
        mNReplaced++;                   // count it
        it->second.mCount++;
        Log (CommonConcat::cREPLACED_LOCAL_NET_RVALUE, net, expr, replacement);
        delete expr;
        return replacement;
      }
    }
    break;
  default:
    return NULL;                        // don't touch
    break;
  }
}

//! Match NUIdentLvalue or NUVarselLvalue against the eliminablenets.
/*virtual*/ NULvalue *CommonConcat::Concat::Eliminator::operator () (NULvalue *lvalue, Phase)
{
  if (mCommonConcat.isProtected (lvalue)) {
    return NULL;
  }
  switch (lvalue->getType ()) {
  case eNUIdentLvalue:
    {
      NUNet *net = dynamic_cast <NUIdentLvalue *> (lvalue)->getIdent ();
      NU_ASSERT (net != NULL, lvalue);
      Map::iterator it;
      NU_ASSERT (net != NULL, lvalue);
      if ((it = mMap.find (net)) == mMap.end ()) {
        // this is not a local net that can be eliminated by the common concat
        // replacement
        return NULL;
      } else {
        // blow it away...
        NULvalue *replacement = new NUVarselLvalue (it->second.mNet, it->second.mRange,
          lvalue->getLoc ());
        mNReplaced++;                   // count it...
        it->second.mCount++;
        it->second.mConcat->foundReplacement ();
        Log (CommonConcat::cREPLACED_LOCAL_NET_LVALUE, net, lvalue, replacement);
        delete lvalue;
        return replacement;
      }
    }
    break;
  case eNUVarselLvalue:
    {
      NUVarselLvalue *varsel = dynamic_cast <NUVarselLvalue *> (lvalue);
      NUNet *net = varsel->getIdentNet (false);
      Map::iterator it;
      if (net == NULL || (it = mMap.find (net)) == mMap.end ()) {
        // this is not a local net that can be eliminated by the common concat
        // replacement
        return NULL;
      } else if (varsel->isConstIndex ()) {
        // grab the range
        const ConstantRange *range = varsel->getRange ();
        NU_ASSERT (range->getLength () <= it->second.mRange.getLength (), varsel);
        SInt32 offset = it->second.mRange.getLsb ();
        ConstantRange new_range (range->getMsb () + offset, range->getLsb () + offset);
        NULvalue *replacement = new NUVarselLvalue (it->second.mNet, new_range, lvalue->getLoc ());
        mNReplaced++;                   // count it
        it->second.mCount++;
        it->second.mConcat->foundReplacement ();
        Log (CommonConcat::cREPLACED_LOCAL_NET_LVALUE, net, lvalue, replacement);
        delete lvalue;
        return replacement;
      } else {
        // grab the range and the ident expression
        const ConstantRange *range = varsel->getRange ();
        CopyContext ctx (NULL, NULL);
        // need to copy the index because the original nucleus will get blown
        // away when expr is deleted
        NUExpr *index = varsel->getIndex ()->copy (ctx);
        NU_ASSERT (range->getLength () <= it->second.mRange.getLength (), varsel);
        SInt32 offset = it->second.mRange.getLsb ();
        ConstantRange new_range (range->getMsb () + offset, range->getLsb () + offset);
        NULvalue *replacement = new NUVarselLvalue (it->second.mNet, index, new_range, lvalue->getLoc ());
        mNReplaced++;                   // count it
        it->second.mCount++;
        it->second.mConcat->foundReplacement ();
        Log (CommonConcat::cREPLACED_LOCAL_NET_LVALUE, net, lvalue, replacement);
        delete lvalue;
        return replacement;
      }
    }
    break;
  default:
    return NULL;                        // don't touch
    break;
  }
}

//! Diagnostics.
void CommonConcat::Concat::Eliminator::pr (UtOStream &s, bool deep) const
{
  Map::const_iterator it;
  for (it = mMap.begin (); it != mMap.end (); it++) {
    UtString b0, b1;
    switch (it->second.mCount) {
    case 0:
      if (deep) {
        it->first->getLoc ().compose (&b1);
        s << "  No occurrences of " << it->first->getName ()->str () << " with "
          << it->second.mNet->getName ()->str () << it->second.mRange.format (&b0) << ": "
          << b1
          << UtIO::endl;
      }
      break;
    case 1:
      it->first->getLoc ().compose (&b1);
      s << "  1 occurrence of " << it->first->getName ()->str () << " with "
        << it->second.mNet->getName ()->str () << it->second.mRange.format (&b0) << ": "
        << b1
        << UtIO::endl;
      break;
    default:
      it->first->getLoc ().compose (&b1);
      s << "  " << it->second.mCount << " occurrences of " << it->first->getName ()->str () << " with "
        << it->second.mNet->getName ()->str () << it->second.mRange.format (&b0) << ": "
        << b1
        << UtIO::endl;
    }
  }
}

//! Encapsulate a left-side concat found by CommonConcat::discover.
CommonConcat::Concat::Concat (CommonConcat &concat, const Flavour flavour,
                              const UInt32 rank, NUConcatLvalue *lvalue, NUAssign *assign, NUNetList &nets) :
  mCommonConcat (concat),
  mFlavour (flavour),
  mRank (rank), 
  mKilled (0xFFFFFFFF),                 // initialise this to never be killed
  mType (assign->getType ()), 
  mStrength (mType == eNUContAssign ? dynamic_cast <NUContAssign *> (assign)->getStrength () : (Strength) 0),
  mLvalue (lvalue), mAssign (assign), mNets (nets), 
  mEliminator (NULL), mRemoved (),
  mNRightSides (0), mNNetReplacements (0)
{
  NU_ASSERT (mType == eNUContAssign 
    || mType == eNUNonBlockingAssign 
    || mType == eNUBlockingAssign,
    assign);
  NU_ASSERT (mAssign->getLvalue () == lvalue, assign);
  NU_ASSERT (mNets.size () == lvalue->getNumArgs (), lvalue);
}

/*static*/ const char *CommonConcat::Concat::cvt (const Flavour x)
{
  switch (x) {
#define CONCAT_FLAVOUR(_tag_, _descr_) case c##_tag_: return #_tag_;
#include "reduce/CommonConcat.def"
  default: return "###error###";
  }
}

CommonConcat::Concat::~Concat ()
{
  // an orphan right-side is a common concat that replaced only a single
  // right-side concat
  if (mCount [cREPLACE_RVALUE] != 1) {
    // not an orphan
  } else if (mCount [cREPLACE_LOCAL_NET] == 0) {
    // a bad orphan is an orphan that did not induce any local net replacements
    mCount.count (cBAD_ORPHAN_RVALUE_CONCAT);
    Log (CommonConcat::cBAD_ORPHAN_RIGHT_SIDE, mAssign, mCount [cREPLACE_LOCAL_NET]);
  } else {
    mCount.count (cORPHAN_RVALUE_CONCAT);
    Log (CommonConcat::cORPHAN_RIGHT_SIDE, mAssign);
  }
  // add the count for this descriptor to the global count for this concat
  mCommonConcat.mCount.merge (mCount);
}

//! Return the common-concat temporary net used to replace this concat
NUNet *CommonConcat::Concat::getReplacementNet (NUScope *scope)
{
  if (mEliminator != NULL) {
    // We have already constructed the elimination net for this concat instance
    // so just return the already created net.
    return mEliminator;
  } else {
    // work out the width of the net
    NU_ASSERT (scope != NULL, mLvalue);
    NUNetList::iterator it;
    UInt32 size = 0;
    for (it = mNets.begin (); it != mNets.end (); it++) {
      size += (*it)->getBitSize ();
    }
    // create the net
    char stem [64];
    sprintf (stem, "CommonConcat_%d", sNNets++);
    StringAtom* sym = scope->gensym (stem /*, NULL, mLvalue */ );
    mEliminator = scope->createTempNet (sym, size, false, mLvalue->getLoc());
    mCommonConcat.protect (mEliminator);
    if (mType == eNUContAssign) {
      mEliminator->setFlags (NetRedeclare (mEliminator->getFlags (), eDMWireNet));
    } else {
      mEliminator->setFlags (NetRedeclare (mEliminator->getFlags (), eDMRegNet));
    }
    return mEliminator;                 // return the new net.
  }
}

//! Build the defining assignment for the common concat.
bool CommonConcat::Concat::buildDefiningAssign (NUStmtList *assign)
{
  NUNet *net = getReplacementNet ();
  switch (mAssign->getType ()) {
  case eNUContAssign:
    if (mRemoved.size () == 0) {
      // none of the nets in the concat have been eliminated so just construct
      // a single assignment
      NUAssign *new_assign = new NUContAssign (net->getName (),
          mLvalue, new NUIdentRvalue (net, mAssign->getLoc ()),
        mAssign->getLoc (), mAssign->getStrength ());
      assign->push_back (new_assign);
      Log (CommonConcat::cBUILD_ASSIGN, new_assign);
      return true;
    } else {
      // some of the nets have been eliminated so create a separate assignment
      // for each non-eliminated part of the common concat
      NUNetList::reverse_iterator it;
      SInt32 lsb = 0;
      for (it = mNets.rbegin (); it != mNets.rend (); it++) {
        NUNet *net = *it;
        UInt32 size = net->getBitSize ();
        if (mRemoved.find (net) != mRemoved.end ()) {
          Log (CommonConcat::cELIMINATED, net, mLvalue);
        } else {
          NULvalue *lvalue = new NUIdentLvalue (net, mAssign->getLoc ());
          ConstantRange range (lsb + size - 1, lsb);
          NUExpr *rvalue = new NUVarselRvalue (mEliminator, range, mAssign->getLoc ());
          NUAssign *new_assign = new NUContAssign (net->getName (),
            lvalue, rvalue, mAssign->getLoc (), mAssign->getStrength ());
          assign->push_back (new_assign);
          Log (CommonConcat::cBUILD_ASSIGN, new_assign, range.getMsb (), range.getLsb ());
        }
        lsb += size;
      }
      // the original lvalue is no longer used so blow it away
      delete mLvalue;
      return true;
    }
    break;
  case eNUBlockingAssign:
    if (mRemoved.size () == 0) {
      NUAssign *new_assign = new NUBlockingAssign (mLvalue, new NUIdentRvalue (net, mAssign->getLoc ()),
        false, mAssign->getLoc ());
      assign->push_back (new_assign);
      Log (CommonConcat::cBUILD_ASSIGN, new_assign);
      return true;
    } else {
      // some of the nets have been eliminated so create a separate assignment
      // for each non-eliminated part of the common concat
      NUNetList::reverse_iterator it;
      SInt32 lsb = 0;
      for (it = mNets.rbegin (); it != mNets.rend (); it++) {
        NUNet *net = *it;
        UInt32 size = net->getBitSize ();
        if (mRemoved.find (net) != mRemoved.end ()) {
          Log (CommonConcat::cELIMINATED, net, mLvalue);
        } else {
          NULvalue *lvalue = new NUIdentLvalue (net, mAssign->getLoc ());
          ConstantRange range (lsb + size - 1, lsb);
          NUExpr *rvalue = new NUVarselRvalue (mEliminator, range, mAssign->getLoc ());
          NUAssign *new_assign = new NUBlockingAssign (lvalue, rvalue, false, mAssign->getLoc ());
          assign->push_back (new_assign);
          Log (CommonConcat::cBUILD_ASSIGN, new_assign, range.getMsb (), range.getLsb ());
        }
        lsb += size;
      }
      // the original lvalue is no longer used so blow it away
      delete mLvalue;
      return true;
    }
    break;
  case eNUNonBlockingAssign:
    if (mRemoved.size () == 0) {
      // have not implemented elimination for anything but continuous assigns yet.
      NU_ASSERT (mRemoved.size () == 0, mAssign);
      NUAssign *new_assign = new NUNonBlockingAssign (mLvalue, new NUIdentRvalue (net, mAssign->getLoc ()),
        false, mAssign->getLoc ());
      assign->push_back (new_assign);
      Log (CommonConcat::cBUILD_ASSIGN, new_assign);
      return true;
    } else {
      // some of the nets have been eliminated so create a separate assignment
      // for each non-eliminated part of the common concat
      NUNetList::reverse_iterator it;
      SInt32 lsb = 0;
      for (it = mNets.rbegin (); it != mNets.rend (); it++) {
        NUNet *net = *it;
        UInt32 size = net->getBitSize ();
        if (mRemoved.find (net) != mRemoved.end ()) {
          Log (CommonConcat::cELIMINATED, net, mLvalue);
        } else {
          NULvalue *lvalue = new NUIdentLvalue (net, mAssign->getLoc ());
          ConstantRange range (lsb + size - 1, lsb);
          NUExpr *rvalue = new NUVarselRvalue (mEliminator, range, mAssign->getLoc ());
          NUAssign *new_assign = new NUNonBlockingAssign (lvalue, rvalue, false, mAssign->getLoc ());
          assign->push_back (new_assign);
          Log (CommonConcat::cBUILD_ASSIGN, new_assign, range.getMsb (), range.getLsb ());
        }
        lsb += size;
      }
      // the original lvalue is no longer used so blow it away
      delete mLvalue;
      return true;
    }
    break;
  default:
    NU_ASSERT (false, mAssign);
    return false;
    break;
  }
}

//! Match an expression against a segment of a concat.
bool CommonConcat::Concat::Matcher::operator () (const NUExpr *expr, UInt32 *cumulative_width)
{
  const NUIdentRvalue *rvalue;
  if (expr->drivesZ ()) {
    // tristates are not propagated across assignment so anything with a Z
    // cannot be eliminated
    return false;
  } else if (!mMatched) {
    return false;                       // already failed
  } else if ((rvalue = dynamic_cast <const NUIdentRvalue *> (expr)) == NULL) {
    mMatched = false;
    return false;
  } else if (rvalue->getIdent () != *mIt) {
    mMatched = false;
    return false;
  } else {
    *cumulative_width += rvalue->getIdent ()->getBitSize ();
    mIt++;
    return true;
  }
}

//! print a human readable representation of the common concat instance to stdout
void CommonConcat::Concat::pr (bool deep) const
{
  pr (UtIO::cout (), deep);
}

//! print a human readable representation of the common concat instance to a stream
void CommonConcat::Concat::pr (UtOStream &s, bool deep) const
{
  if (deep) {
    UtString buffer;
    mLvalue->compose (&buffer, NULL, 2, false);
    s << buffer << " = ";
  }
  if (mNets.size () == 0) {
    s << "{}" << UtIO::endl;
  } else {
    NUNetList::const_iterator it = mNets.begin ();
    s << "{" << (*it)->getName ()->str ();
    it++;
    while (it != mNets.end ()) {
      s << ", " << (*it)->getName ()->str ();
      it++;
    }
    s << "}" << UtIO::endl;
  }
}

//! print a human readable representation of a list of concat instances.
void CommonConcat::Concat::List::pr (UtOStream &s, bool deep) const
{
  const_iterator it;
  for (it = begin (); it != end (); it++) {
    (*it)->pr (s, deep);
  }
}

//! print a human readable representation of a map of instances.
void CommonConcat::Concat::Map::pr (UtOStream &s, bool) const
{
  const_iterator it;
  for (it = begin (); it != end (); it++) {
    Concat *descr = it->second;
    descr->pr (s, false);
  }
}

void CommonConcat::Concat::Occurrences::pr (UtOStream &s, bool deep) const
{
  if (mMaxDegree > 1) {
    s << "Concat matcher maximum degree is " << mMaxDegree << UtIO::endl;
  }
  Concat::Map::pr (s, deep);
}

//! Searchable map of all Concat instances.

CommonConcat::Concat::Occurrences::Occurrences (CommonConcat &x) : Map (), mCommonConcat (x), mMaxDegree (0) 
{}

//! Add a concat to the map.
void CommonConcat::Concat::Occurrences::add (Concat *concat)
{
  NU_ASSERT (concat->getLength () > 0, concat->getLvalue ());
  NUNet *leader = concat->getLeader ();
  insert (std::pair <NUNet *, Concat *> (leader, concat));
  if (count (leader) > mMaxDegree) {
    mMaxDegree = count (leader);
  }
}

//! Search for a concat match.
bool CommonConcat::Concat::Occurrences::match (const NUConcatOp *concat, const UInt32 position,
  Concat **descr, UInt32 *width, const NUType flavour, const Strength strength) const
{
  if (concat->getRepeatCount () != 1) {
    // TODO - handle non-trivial repeat count
    return false;
  }
  const UInt32 arity = concat->getNumArgs ();
  if (arity < 2) {
    return false;                       // will not match anything interesting
  }
  const NUExpr *leader = concat->getArg (0);
  if (leader->getType () != NUOp::eNUIdentRvalue) {
    return false;                       // wrong form for eliminable concat
  }
  // it is a simple ident, so find the set of possible matches
  NUNet *net = dynamic_cast <const NUIdentRvalue *> (leader)->getIdent ();
  NU_ASSERT (net != NULL, concat);
  // More than one path with the same leader net may have been found so we
  // iterate accross the possible matches looking for a complete match.
  //
  // TODO - this search should be made more efficient
  //
  const_iterator paths = find (net);
  *descr = NULL;
  while (paths != end () && *descr == NULL) {
    Concat *candidate = paths->second;
    if (candidate->getType () == eNUContAssign && flavour != eNUContAssign) {
      // allow matching of continuous assign left-sides to any right sides
    } else if (candidate->getType () != flavour || candidate->getStrength () != strength) {
      paths++; continue;
    } else if (candidate->getLength () > arity) {
      paths++; continue;
    } else if (!candidate->isLive (position)) {
      // The common concat assignment is killed before this position
      paths++; continue;
    }
    Concat::Matcher matcher (candidate);
    UInt32 n = 0;
    *width = 0;
    while (n < arity && matcher (concat->getArg (n), width)) {
      n++;
    }
    if (n == arity) {
      *descr = candidate;
    }
    paths++;
  }
  return *descr != NULL;
}

void CommonConcat::dumpConcatList (UtOStream &s, bool deep) const
{
  switch (mLhsOccurrences.size ()) {
  case 0:
    s << "No concats found" << UtIO::endl;
    break;
  case 1:
    s << "1 concat found" << UtIO::endl;
    break;
  default:
    s << mLhsOccurrences.size () << " concats found" << UtIO::endl;
    break;
  }
  Concat::Map::const_iterator it;
  if (deep) {
    for (it = mLhsOccurrences.begin (); it != mLhsOccurrences.end (); it++) {
      it->second->pr (s, deep);
    }
  }
}

//! Counting manager for CommonConcat.
CommonConcat::Count::Count () : mNonZero (false)
{
  memset (mCount, 0, sizeof (mCount));
}

//! Map a counter tag to a short identifier
/*static*/ const char *CommonConcat::Count::getLabel (const UInt32 x)
{
  switch (x) {
#define CONCAT_COUNTER(_tag_, _label_, _comment_) case CommonConcat::c##_tag_: return _label_;
#include "reduce/CommonConcat.def"
  default: return "###error###";
  }
}

//! Map a counter tag to a longer description
/*static*/ const char *CommonConcat::Count::getDescription (const UInt32 x)
{
  switch (x) {
#define CONCAT_COUNTER(_tag_, _label_, _comment_) case CommonConcat::c##_tag_: return _comment_;
#include "reduce/CommonConcat.def"
  default: return "###error###";
  }
}

//! Diagnostic dump
void CommonConcat::Count::pr () const
{
  statistics (UtIO::cout (), 0);
}

//! Output statistics from the counter
void CommonConcat::Count::statistics (UtOStream &s, const UInt32 options) const
{
  if (!mNonZero) {
    // output nothing
  } else if (options & cBRIEF) {
    // output a compressed format
    UtString line;
    for (UInt32 i = 0; i < cNCounters; i++) {
      if (mCount [i] == 0) {
        continue;                       // zero count, no output
      }
      UtString entry;
      entry << getLabel (i) << "=" << mCount [i];
      if (line.size () + entry.size () >= 80) {
        s << line << "," << UtIO::endl;
        line.clear ();
      }
      if (line.size () == 0) {
        line.append (options & cINDENT_MASK, ' ');
      } else {
        line.append (", ");
      }
      line << entry;
    }
    if (line.size () > 0) {
      s << line << UtIO::endl;
    }
  } else {
    // output a longer multi-line format
    UtString indent (options & cINDENT_MASK, ' ');
    for (UInt32 i = 0; i < cNCounters; i++) {
      if (mCount [i] > 0) {
        s << indent << getDescription (i) << ":\t" << mCount [i] << UtIO::endl;
      }
    }
  }
}

//! Merge a counter with another counter.
void CommonConcat::Count::merge (const Count &other)
{
  for (UInt32 i = 0; i < cNCounters; i++) {
    mCount [i] += other.mCount [i];
  }
  mNonZero |= other.mNonZero;
}

//! Log file manager for CommonConcat.
/*! Provides logging of common concat instances. */
class CommonConcatLogger : public OccurrenceLogger {
public:
  CommonConcatLogger (MsgContext &msg_context, const char *root, const UInt32 flags) : 
    OccurrenceLogger (msg_context, root, "concat", flags) {}
  virtual ~CommonConcatLogger () {}
  virtual const char *keyToName (const UInt32 n) const
  {
    switch (n) {
#define CONCAT_LOG(_tag_, _descr_) case CommonConcat::c##_tag_: return #_tag_;
#include "reduce/CommonConcat.def"
    default: return "###error###";
    }
  }
};


/*! While an cbuild run may have many CommonConcat instances, a single log file
 *  is written to capture all common concat events.
 */

/*static*/ OccurrenceLogger *CommonConcat::sLog = NULL;

//! Open the concat log file building the filename from root
/*static*/ void CommonConcat::openOccurrenceLog (MsgContext &msg_context, const char *root)
{
  if (sLog == NULL) {
    sLog = new CommonConcatLogger (msg_context, root, OccurrenceLogger::cSUMMARY);
    Log.bind (sLog);
  }
}

//! Close the concat log file
/*static*/ void CommonConcat::closeOccurrenceLog ()
{
  if (sLog != NULL) {
    sLog->summarise ();
    sLog->summarise (UtIO::cout ());
    Log.bind ();
    delete sLog;
    sLog = NULL;
  }
}

/******************** experimental code ********************/

// The rest of this is code under construction that  cannot be executed unless
// ENABLE_MODULE_WIDE_COMMON_CONCAT or ENABLE_ALWAYS_BLOCK_WIDE_COMMON_CONCAT
// are defined at compile time.

// FYI: I'm looking at doing more aggressive concat analysis, at both block
// granularity and entire module granularity. This is just the infrastructure
// upon which more common-concat instances will be hung.

#if !ENABLE_MODULE_WIDE_COMMON_CONCAT

//! Module-wide common concat
bool CommonConcat::module (NUModule *module)
{
  NU_ASSERT (false, module);
  return false;
}

#else

class ModuleWideDiscovery : public NUDesignCallback {
public:

  ModuleWideDiscovery (CommonConcat &common_concat, NUModule *module) :
    mCommonConcat (common_concat), mModule (module), mScopes (),
    mContAssign (NULL), mAssign (NULL)
  {}

  virtual ~ModuleWideDiscovery () 
  {
    // if this fails then the traversal was screwed up... somehow...
    NU_ASSERT (mScopes.empty (), mModule);
  }

private:

  CommonConcat &mCommonConcat;          //!< the governing common-concat
  NUModule *mModule;                    //!< the victim
  NUScopeStack mScopes;                 //!< current scope stack
  NUContAssign *mContAssign;            //!< current cont assign
  NUAssign *mAssign;                    //!< current procedural assign

  //! Push a scope onto the stack.
  void pushScope (NUScope * scope) { mScopes.push (scope); }

  //! Pop a scope off the stack.
  void popScope () { mScopes.pop (); }

  //! Get the top of the stack.
  NUScope * getScope () const { return mScopes.top (); }

  //! Base operator does nothing
  virtual Status operator () (Phase, NUBase *) { return eNormal; }

  //! Skip sub modules
  virtual Status operator()(Phase, NUModuleInstance*) { return eSkip; }

  virtual Status operator () (Phase phase, NUModule *node)
  {
    switch (phase) {
    case ePre:
      NU_ASSERT (mModule == node && mScopes.empty (), node);
      pushScope (node);
      break;
    case ePost:
      NU_ASSERT (mModule == node && getScope () == node, node);
      popScope ();
      break;
    default:
      NU_ASSERT (false, node);
      break;
    }
    return eNormal;
  }

  virtual Status operator () (Phase phase, NUContAssign *node)
  {
    switch (phase) {
    case ePre:
      NU_ASSERT (mAssign == NULL && mContAssign == NULL, node);
      mContAssign = node;
      break;
    case ePost:
      NU_ASSERT (mAssign == NULL && mContAssign == node, node);
      mContAssign = NULL;
      break;
    default:
      NU_ASSERT (false, node);
      break;
    }
    return eNormal;
  }

  virtual Status operator () (Phase phase, NUAssign *node)
  {
    switch (phase) {
    case ePre:
      NU_ASSERT (mAssign == NULL && mContAssign == NULL, node);
      mAssign = node;
      break;
    case ePost:
      NU_ASSERT (mAssign == node && mContAssign == NULL, node);
      mAssign = NULL;
      break;
    default:
      NU_ASSERT (false, node);
      break;
    }
    return eNormal;
  }

  virtual Status operator () (Phase phase, NUBlock *node)
  {
    switch (phase) {
    case ePre:
      pushScope (node);
      break;
    case ePost:
      NU_ASSERT (getScope () == node, node);
      popScope ();
      break;
    default:
      NU_ASSERT (false, node);
      break;
    }
    return eNormal;
  }

  virtual Status operator () (Phase phase, NUConcatLvalue * /*node*/)
  {
    if (phase != ePre) {
      return eNormal;                   // only looking in pre...
    } else {
      // this is a left-side concat
      
      return eNormal;
    }
  }

  virtual Status operator () (Phase phase, NULvalue *node)
  {
    if (phase != ePre) {
      return eNormal;                   // only looking in pre
    } else if (node->getType () == eNUConcatLvalue) {
      // should have been handled by the virtual operator () method for NUConcatLvalue
      NU_ASSERT (node->getType () != eNUConcatLvalue, node);
      return eNormal;
    } else {
      return eNormal;
    }
  }

  virtual Status operator () (Phase phase, NUExpr *node)
  {
    if (phase != ePre) {
      return eNormal;                   // only looking in pre
    } else if (node->getType () != NUExpr::eNUConcatOp) {
      return eNormal;                   // only looking for concat
    } else {
      // this is a right side concat
      return eNormal;
    }
    return eNormal;
  }

};

//! Module-wide common concat
bool CommonConcat::module (NUModule *module)
{
  NU_ASSERT (mScope == module, module);
  ModuleWideDiscovery finder (*this, module);
  NUDesignWalker discovery (finder, false);
  discovery.module (module);
  return false;
}

#endif

// Always-block-wide processing can only be executed in
// ENABLE_ALWAYS_BLOCK_WIDE_COMMON_CONCAT is set at compile time.

#if !ENABLE_ALWAYS_BLOCK_WIDE_COMMON_CONCAT

//! Always-block-wide common-concat application
bool CommonConcat::always (NUModule *module)
{
  NU_ASSERT (false, module);
  return false;
}

bool CommonConcat::always (NUAlwaysBlock *block)
{
  NU_ASSERT (false, block);
  return false;
}

#else

//! Always-block-wide common-concat application
bool CommonConcat::always (NUModule *module)
{
  bool retval = false;
  if (mVerbose) {
    UtIO::cout () << "Always-block common-concat processing in " << module->getName ()->str ()
      << UtIO::endl;
  }
  for (NUModule::AlwaysBlockLoop loop = module->loopAlwaysBlocks (); !loop.atEnd (); ++loop) {
    CommonConcat nested (module, mMsgContext, mIODB, mParams, this);
    retval |= nested.always (*loop);
  }
  return retval;
}

bool CommonConcat::always (NUAlwaysBlock *block)
{
  NU_ASSERT (mParent != NULL, block);
  // Does not do much interesting just yet...
  return false;
}

#endif
