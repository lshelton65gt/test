// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/RemoveUnelabFlow.h"
#include "reduce/CleanUnelabFlow.h"
#include "flow/FLFactory.h"

/*!
  \file
  Implementation of flow removal package.
 */


void RemoveUnelabFlow::remove()
{
  UnelabMarkSweep::clearMarkAllFlow(mFlowFactory);

  CleanUnelabFlow clean(mFlowFactory, 0, true, false);
  clean.deleteDeadFlow();

  FLNodeFactory::iterator iter = mFlowFactory->begin();
  assert(iter == mFlowFactory->end());
}
