// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/REAlias.h"
#include "flow/FLIter.h"
#include "flow/FLNodeElab.h"
#include "flow/FLFactory.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "symtab/STSymbolTable.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "util/Stats.h"
#include "util/MemManager.h"
#include "util/SetClosure.h"


/*!
  \file
  Generate aliasing.
*/


//! Helper function to print a master
void sPrintMaster(NUNetElab* master, bool allocated=false)
{
  UtString s;
  master->compose(&s, NULL);    // (we have no testcase in almostall that checks this line)
  UtIO::cout() << "Master:  " << s;
  if (allocated) {
    UtIO::cout() << "  (allocated)";
  }
  UtIO::cout() << "\n";
}


//! Helper function to print a subordinate
void sPrintSubord(NUNetElab* subord, bool allocated=false)
{
  UtString s;
  subord->compose(&s, NULL);      // (we have no testcase in almostall that checks this line)
  UtIO::cout() << "Subordinate:  " << s;
  if (allocated) {
    UtIO::cout() << "  (allocated)";
  }
  UtIO::cout() << "\n";
}


void REAlias::fixupFanout(NUNetElab *net,
			  const FLNodeElabList& old_driver_list,
			  NUNetElab::DriverLoop new_drivers)
{
  NetNodeElabSetMapIter iter = mFanouts.find(net);
  if (iter == mFanouts.end()) return;

  FLNodeElabSet & one_net_fanouts = iter->second;
  fixupOneNetFanouts(net, one_net_fanouts, old_driver_list, new_drivers);
}

void REAlias::fixupOneNetFanouts(NUNetElab *net,
                                 FLNodeElabSet & one_net_fanouts,
                                 const FLNodeElabList& old_driver_list,
                                 NUNetElab::DriverLoop new_drivers)
{                                   
  for (FLNodeElabSet::iterator iter = one_net_fanouts.begin();
       iter != one_net_fanouts.end();
       ++iter) {
    FLNodeElab * fanout = (*iter);

    // Don't do this for flow nodes which are going to be eliminated.
    bool ignore = false;
    NetNodeElabMapRange elim_driver_range = mElimDrivers.equal_range(fanout->getDefNet());
    for (NetNodeElabMapIter elim_driver_iter = elim_driver_range.first;
         not ignore and (elim_driver_iter != elim_driver_range.second);
         ++elim_driver_iter) {
      if (elim_driver_iter->second == fanout) {
        ignore = true;
      }
    }

    if (ignore) {
      continue;
    }

    // Figure out which drivers we are actually going to use,
    // based on the UD information, and only hook those drivers up.
    NUNetRefHdl used_bits = fanout->findUsedBits(mNetRefFactory, net);
    FLNodeElabList replacements;
    if (not used_bits->empty()) {
      for (NUNetElab::DriverLoop::iterator driver_iter = new_drivers.begin();
           driver_iter != new_drivers.end();
           ++driver_iter) {
        FLNodeElab *driver = *driver_iter;
        NUNetRefHdl def_net_ref = driver->getDefNetRef(mNetRefFactory);
        if (def_net_ref->overlapsBits(*used_bits)) {
          replacements.push_back(driver);
        }
      }
    }
    fanout->replaceFaninsWithList(old_driver_list, replacements);
  }
}


void REAlias::processElimDrivers(NUNetElab *master_net,
				 NetElabSet *subord_nets,
				 FLNodeElabSet &old_driver_set)
{
  // Eliminate the master driver(s) if necessary.
  NetNodeElabMapRange driver_range = mElimDrivers.equal_range(master_net);
  if (driver_range.first != driver_range.second) {
    for (NetNodeElabMapIter driver_iter = driver_range.first;
	 driver_iter != driver_range.second;
	 ++driver_iter)
    {
      FLNodeElab* driver = driver_iter->second;
      master_net->removeContinuousDriver(driver);
      old_driver_set.insert(driver);
    }
  }

  // Go through the subordinates, eliminating any of their drivers as necessary.
  for (NetElabSetIter subord_iter = subord_nets->begin();
       subord_iter != subord_nets->end();
       ++subord_iter) {
    NUNetElab *subord_net = *subord_iter;
    driver_range = mElimDrivers.equal_range(subord_net);
    if (driver_range.first != driver_range.second) {
      for (NetNodeElabMapIter driver_iter = driver_range.first;
	   driver_iter != driver_range.second;
	   ++driver_iter)
      {
        FLNodeElab* driver = driver_iter->second;
	subord_net->removeContinuousDriver(driver);
	old_driver_set.insert(driver);
      }
    }
  }
}


void REAlias::processBoundDrivers(NUNetElab *master_net,
                                  NetElabSet *subord_nets,
                                  FLNodeElabSet &old_driver_set)
{
  FLNodeElab * first_bound = NULL;

  // If this group of aliases has multiple bound nodes, only retain the first.

  // Go through the drivers for the master; remove all bound nodes but the first.
  NetNodeElabSetMapIter master_driver_iter = mDrivers.find(master_net);
  if (master_driver_iter != mDrivers.end()) {
    FLNodeElabSet & one_net_fanouts = master_driver_iter->second;
    for (FLNodeElabSet::iterator net_fanout_iter = one_net_fanouts.begin();
         net_fanout_iter != one_net_fanouts.end();
         ++net_fanout_iter) {
      FLNodeElab * driver = (*net_fanout_iter);
      if (driver->isBoundNode()) {
        // Undriven bounds and protected bounds, and primary port bounds should persist.
        switch(driver->getType()) {
        case eFLBoundProtect: break; // Leave all protected bounds.
        case eFLBoundPort:    break; // There should only be one port, but leave it alone.
        case eFLBoundUndriven: {
          if (first_bound and (first_bound != driver)) {
            master_net->removeContinuousDriver(driver);
            old_driver_set.insert(driver);
          } else {
            first_bound = driver;
          }
          break;
        }
        default:
          // Don't expect any other bound types.
          FLN_ELAB_ASSERT(false,driver);
          break;
        }
      }
    }
  }

  // Go through the drivers for all subords; remove all bound nodes
  // but the first. If the master had a bound node, all subord bound
  // nodes will be eliminated.
  for (NetElabSetIter subord_iter = subord_nets->begin();
       subord_iter != subord_nets->end();
       ++subord_iter) {
    NUNetElab *subord_net = *subord_iter;
    NetNodeElabSetMapIter subord_driver_iter = mDrivers.find(subord_net);
    if (subord_driver_iter != mDrivers.end()) {
      FLNodeElabSet & subord_fanouts = subord_driver_iter->second;
      for (FLNodeElabSet::iterator subord_fanout_iter = subord_fanouts.begin();
           subord_fanout_iter != subord_fanouts.end();
           ++subord_fanout_iter) {
        FLNodeElab * driver = (*subord_fanout_iter);
        if (driver->isBoundNode()) {
          // Undriven bounds and protected bounds, and primary port bounds should persist.
          switch(driver->getType()) {
          case eFLBoundProtect: break; // Leave all protected bounds.
          case eFLBoundPort:    break; // There should only be one port, but leave it alone.
          case eFLBoundUndriven: {
            if (first_bound and (first_bound != driver)) {
              subord_net->removeContinuousDriver(driver);
              old_driver_set.insert(driver);
            } else {
              first_bound = driver;
            }
            break;
          }
          default:
            // Don't expect any other bound types.
            FLN_ELAB_ASSERT(false,driver);
            break;
          }
        }
      }
    }
  }
}


void REAlias::processDrivers(NUNetElab *master_net,
			     NetElabSet *subord_nets)
{
  // Any remaining drivers are drivers of the master.
  for (NetElabSetIter subord_iter = subord_nets->begin();
       subord_iter != subord_nets->end();
       ++subord_iter) {
    NUNetElab *subord_net = *subord_iter;

    // Change all non-eliminated drivers to be drivers to the master.
    NetNodeElabSetMapIter all_driver_iter = mDrivers.find(subord_net);
    if (all_driver_iter != mDrivers.end()) {
      FLNodeElabSet & driver_fanouts = all_driver_iter->second;
      for (FLNodeElabSet::iterator driver_fanout_iter = driver_fanouts.begin();
           driver_fanout_iter != driver_fanouts.end();
           ++driver_fanout_iter) {
        // Potentially, may have encountered this through walks of different
        // sensitivity.  So, do not change the driver if we have already changed it
        // (bug 128).
        FLNodeElab *this_driver = (*driver_fanout_iter);
        if (this_driver->getDefNet() != master_net) {
          this_driver->replaceDefNet(subord_net, master_net);
        }
      }
    }

    if (subord_net->isContinuouslyDriven()) {
      for (NUNetElab::DriverLoop drivers = subord_net->loopContinuousDrivers();
	   not drivers.atEnd();
	   ++drivers) {
	master_net->addContinuousDriver(*drivers);
      }
    }
  }
}


//! Return true if the given net is driven from within the design, but don't count ignore_driver.
bool helperIsNetInternallyDriven(NUNetElab *net, FLNodeElab *ignore_driver=0)
{
  UInt32 n = 0;

  for (NUNetElab::DriverLoop loop = net->loopContinuousDrivers(); not loop.atEnd(); ++loop) {
    FLNodeElab *driver = *loop;
    if (driver != ignore_driver) {
      FLNodeBoundElab *bound = dynamic_cast<FLNodeBoundElab*>(driver);
      if (not (bound && (bound->getDefNet() == net))) {
	++n;
      }
    }
  }

  return (n > 0);
}


void REAlias::flagAliases(NUNetElab *master_net,
			  NetElabSet *subord_nets,
			  bool &is_primary_tristate)
{
  NUNet *unelab_master = master_net->getStorageNet();

  // Do one pass to pick up flags for the aliases.
  bool any_is_driven_z = (unelab_master->isTristate() || unelab_master->isZ())
    && helperIsNetInternallyDriven(master_net);
  bool any_is_driven = helperIsNetInternallyDriven(master_net);
  bool any_is_primary_port = unelab_master->isPrimaryPort();
  bool any_is_primary_bid = unelab_master->isPrimaryBid();
  bool any_is_primary_output = unelab_master->isPrimaryOutput();
  bool any_is_pullup = unelab_master->isPullUp();
  bool any_is_pulldown = unelab_master->isPullDown();
  // TBD bool any_is_wor = unelab_master->isWor();
  for (NetElabSetIter subord_iter = subord_nets->begin();
       subord_iter != subord_nets->end();
       ++subord_iter) {
    NUNetElab *subord_net = *subord_iter;
    NUNet *unelab_subord = subord_net->getStorageNet();
    any_is_primary_port |= unelab_subord->isPrimaryPort();
    any_is_primary_bid |= unelab_subord->isPrimaryBid();
    any_is_primary_output |= unelab_subord->isPrimaryOutput();
    any_is_driven_z |= (unelab_subord->isZ() || unelab_subord->isTristate())
      && helperIsNetInternallyDriven(subord_net);
    any_is_driven |= helperIsNetInternallyDriven(subord_net);
    any_is_pullup |= unelab_subord->isPullUp();
    any_is_pulldown |= unelab_subord->isPullDown();
    //  TBD  any_is_wor |= unelab_subord->isWor();
  }

  // Do a second pass to issue warnings.
  //
  // Note that when we check to issue warnings, always check that the net
  // itself doesn't cause the violation; if it does, it would have already
  // been flagged during nucleus population.
  for (NetElabSetIter subord_iter = subord_nets->begin();
       subord_iter != subord_nets->end();
       ++subord_iter) {
    NUNetElab *subord_net = *subord_iter;
    NUNet *unelab_subord = subord_net->getStorageNet();

    // Detect and warn for the case where a primary port gets aliased to a trireg.
    if (any_is_primary_port and not unelab_subord->isPrimaryPort() and unelab_subord->isTrireg()) {
      UtString buf;
      master_net->compose(&buf, NULL, true);   // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->TriregAliasForPortNet(subord_net, buf.c_str());
    } else if (unelab_subord->isPrimaryPort() and unelab_master->isTrireg()) {
      UtString buf;
      subord_net->compose(&buf, NULL, true);    // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->TriregAliasForPortNet(master_net, buf.c_str());
    }

    // The following section of code detects pullup/down problems.
    // 1 - pullup and pulldown on the same net
    if ((any_is_pullup and not unelab_subord->isPullUp() and unelab_subord->isPullDown()) or
	(any_is_pulldown and not unelab_subord->isPullDown() and unelab_subord->isPullUp())) {
      UtString buf;
      master_net->compose(&buf, NULL, true);  // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullUpAndDown(subord_net, buf.c_str());
    } else if ((unelab_master->isPullUp() and not unelab_master->isPullDown() and unelab_subord->isPullUp()) or
	       (unelab_master->isPullDown() and not unelab_master->isPullUp() and unelab_subord->isPullDown())) {
      UtString buf;
      subord_net->compose(&buf, NULL, true);   // include root in name  (we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullUpAndDown(master_net, buf.c_str());
    }
    // 2 - pullup/pulldown on trireg
    if ((any_is_pullup or any_is_pulldown) and
	not unelab_subord->isPullUp() and
	not unelab_subord->isPullDown() and
	unelab_subord->isTrireg()) {
      UtString buf;
      master_net->compose(&buf, NULL, true);   // include root in name we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullTrireg(subord_net, buf.c_str());
    } else if (not unelab_master->isPullUp() and
	       not unelab_master->isPullDown() and
	       unelab_master->isTrireg() and
	       (unelab_subord->isPullUp() or unelab_subord->isPullDown())) {
      UtString buf;
      subord_net->compose(&buf, NULL, true);   // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullTrireg(master_net, buf.c_str());
    }
    // 3 - Pullup on a tri0
    if (any_is_pullup and not unelab_subord->isPullUp() and unelab_subord->isTri0()) {
      UtString buf;
      master_net->compose(&buf, NULL, true);   // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullUpTri0(subord_net, buf.c_str());
    } else if (not unelab_master->isPullUp() and unelab_master->isTri0() and unelab_subord->isPullUp()) {
      UtString buf;
      subord_net->compose(&buf, NULL, true);    // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullUpTri0(master_net, buf.c_str());
    }
    // 3 - Pulldown on a tri1
    if (any_is_pulldown and not unelab_subord->isPullDown() and unelab_subord->isTri1()) {
      UtString buf;
      master_net->compose(&buf, NULL, true);   // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullDownTri1(subord_net, buf.c_str());
    } else if (not unelab_master->isPullDown() and unelab_master->isTri1() and unelab_subord->isPullDown()) {
      UtString buf;
      subord_net->compose(&buf, NULL, true);    // include root in name (we have no testcase in almostall that checks this line)
      mMsgContext->AliasPullDownTri1(master_net, buf.c_str());
    }
  }

  if ((any_is_driven_z or not any_is_driven) and
      (any_is_primary_bid or any_is_primary_output))
  {
    is_primary_tristate = true;
  } else {
    is_primary_tristate = false;
  }
}


void REAlias::sanityCheckAliases()
{
  for (NetElabSetMapConstIter iter = mAliases.begin();
       iter != mAliases.end();
       ++iter) {
    NUNetElab *master = iter->first;
    const NetElabSet &subords = iter->second;
    sanityCheckMaster(master);
    sanityCheckSubordSet(master, &subords);
    if (mDoAllocation) {
      sanityCheckAllocation(master, &subords);
    }
  }

  for (NetElabMapConstIter iter = mSubords.begin(); iter != mSubords.end(); ++iter) {
    NUNetElab *subord = iter->first;
    NUNetElab *master = iter->second;
    NetElabSetMapConstIter alias_iter = mAliases.find(master);
    if (alias_iter == mAliases.end()) {
      mMsgContext->MasterAlone(master);
      mBadAllocation = true;
    }
    else {
      const NetElabSet &aliases = alias_iter->second;
      if (aliases.find(subord) == aliases.end()) {
        mMsgContext->SubordAlone(subord);
        mBadAllocation = true;
      }
    }
  }
}


void REAlias::sanityCheckMaster(NUNetElab *master)
{
  NetElabMapConstIter iter = mSubords.find(master);
  if (iter != mSubords.end()) {
    mMsgContext->MasterIsSubord(master);
    mBadAllocation = true;
  }
}


void REAlias::sanityCheckSubordSet(NUNetElab *master, const NetElabSet *subord_nets)
{
  for (NetElabSetIter iter = subord_nets->begin();
       iter != subord_nets->end();
       ++iter) {
    NUNetElab *subord = *iter;
    NetElabSetMapConstIter alias_iter = mAliases.find(subord);
    if (alias_iter != mAliases.end()) {
      mMsgContext->MasterIsSubord(subord);
      mBadAllocation = true;
    }
    NetElabMapConstIter find_iter = mSubords.find(subord);
    if ((find_iter == mSubords.end()) or (find_iter->second != master)) {
      mMsgContext->MasterSubordCheck();
      mBadAllocation = true;
      sPrintMaster(master);
      sPrintSubord(subord);
    }
  }
}


void REAlias::sanityCheckAllocation(NUNetElab *master, const NetElabSet *subord_nets)
{
  bool allocated = master->getStorageNet()->isAllocated();
  bool more_than_one_allocation = false;
  for (NetElabSetIter iter = subord_nets->begin();
       iter != subord_nets->end();
       ++iter) {
    NUNetElab *subord = *iter;
    if (allocated and subord->getStorageNet()->isAllocated()) {
      more_than_one_allocation = true;
    }
    allocated |= subord->getStorageNet()->isAllocated();
  }

  if (more_than_one_allocation) {
    mMsgContext->MoreThanOneAllocation();
    mBadAllocation = true;
    sPrintMaster(master, master->getStorageNet()->isAllocated());
    for (NetElabSetIter iter = subord_nets->begin();
	 iter != subord_nets->end();
	 ++iter) {
      NUNetElab *subord = *iter;
      sPrintSubord(subord, subord->getStorageNet()->isAllocated());
    }
  }

  if (not allocated) {
    // there may have been something in the alias ring before aliasing occurred.
    NUNet * storage_net = master->getStorageNet();
    allocated |= storage_net->isAllocated();

    if (not allocated) {
      mMsgContext->NoAllocation();
      mBadAllocation = true;
      sPrintMaster(master);
      for (NetElabSetIter iter = subord_nets->begin();
	   iter != subord_nets->end();
	   ++iter) {
	NUNetElab *subord = *iter;
	sPrintSubord(subord);
      }
    }
  }
}


void REAlias::createAliases(NUNetElab *master_net,
			    NetElabSet *subord_nets)
{
  if (mVerbose) {
    UtIO::cout() << "Creating aliases to master:" << UtIO::endl;
    master_net->print(false,2);
    UtIO::cout() << "Subordinate Nets:" << UtIO::endl;
    for (NetElabSetIter subord_iter = subord_nets->begin();
	 subord_iter != subord_nets->end();
	 ++subord_iter) {
      NUNetElab *subord_net = *subord_iter;
      subord_net->print(false,2);
    }
    UtIO::cout() << UtIO::endl;
  }

  // This old driver set is used to fixup the fanout.
  FLNodeElabSet old_driver_set;

  processElimDrivers(master_net, subord_nets, old_driver_set);

  processBoundDrivers(master_net, subord_nets, old_driver_set);

  // Issue any warnings or errors about flag conflicts.  Must be after drivers
  // have been eliminated, but before drivers are changed from subords to master.
  bool is_primary_tristate;
  flagAliases(master_net, subord_nets, is_primary_tristate);

  processDrivers(master_net, subord_nets);

  // Set master flags, do this before aliasing so the flags will propagate
  // to any node that writes them.
  master_net->setIsMaster();
  if (is_primary_tristate) {
    master_net->getStorageNet()->putIsPrimaryZ(true);
  }

  // Handle master allocation.
  if (master_net->getStorageNet()->isAllocated()) {
    // If the master does not already have an appointed storage node,
    // mark it as the storage element of its alias ring.
    STAliasedLeafNode * master_node = master_net->getSymNode();
    if (not master_node->getInternalStorage()) {
      master_node->setThisStorage();
    }
  }

  // remember the old storage location. if it changes, we need to 
  // clear allocation.
  NUNet * old_storage_net = master_net->getStorageNet();

  FLNodeElabList old_driver_list;
  old_driver_list.assign(old_driver_set.begin(),
                         old_driver_set.end());

  // Do the final pass over the subordinates:
  // . Fixup the fanout.  The master now fans-out to everything the subordinates
  //   fanned-out to.  Also have to fix the master fanout, since it is possible
  //   that the master driver has changed.
  // . Alias the nets.
  // . Delete the subordinate elaborated nets.
  fixupFanout(master_net, old_driver_list, master_net->loopContinuousDrivers());
  for (NetElabSetIter subord_iter = subord_nets->begin();
       subord_iter != subord_nets->end();
       ++subord_iter) {
    NUNetElab *subord_net = *subord_iter;
    fixupFanout(subord_net, old_driver_list, master_net->loopContinuousDrivers());
    subord_net->alias(master_net);
    delete subord_net;
  }

  // Make sure all the aliases have the same storage.
  if (mDoAllocation)
  {
    STAliasedLeafNode * master_node  = master_net->getSymNode();
    STAliasedLeafNode * storage_node = master_node->getInternalStorage();
    ST_ASSERT(storage_node, master_node);
    storage_node->setThisStorage();

    NUNet* storageNet = master_net->getStorageNet();
    if (storageNet != old_storage_net) {
      old_storage_net->putIsAllocated(false);
    }

    // Make sure the storage-net is marked as tristate if appropriate
    if (is_primary_tristate) {
      storageNet->putIsPrimaryZ(true);
    }
  }

  // Finally, delete all old drivers.
  if (!mKeepDeadFlows)
  {
    for (FLNodeElabListIter list_iter = old_driver_list.begin();
         list_iter != old_driver_list.end();
         ++list_iter) {
      FLNodeElab *old_driver = *list_iter;
      mFlowFactory->destroy(old_driver);
    }
  }
}


void REAlias::sanityCheckSTAllocation()
{
  return; // this sanity check does the wrong thing in the presence of dead logic.
  bool badness = false;
  for (STSymbolTable::NodeLoop i = mSymtab->getNodeLoop();
       not i.atEnd(); 
       ++i) {
    STSymbolTableNode* node = *i;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf) {
      NUBase * base = CbuildSymTabBOM::getNucleusObj(leaf);
      NUNet * net = dynamic_cast<NUNet*>(base);
      if (not net) {
        NUNetElab * net_elab = dynamic_cast<NUNetElab*>(base);
        if (net_elab) {
          net = net_elab->getNet();
        }
      }
      if (net) {
        if (leaf->getInternalStorage() == leaf) {
          // if this leaf is marked for storage, ensure that the net has been marked as allocated.
          // ASSERT(net->isAllocated());
          
          if (not net->isAllocated()) {
            UtIO::cout() << "Symbol table entry marked for storage but net not allocated." << UtIO::endl;
            leaf->print();
            leaf->printAliasStorage();
            badness = true;
          }
        }
      }
    }
  }
  
  if (badness) {
    INFO_ASSERT(0, "Storage/allocation check failed.");
  }
}


void REAlias::walkAliases()
{
  sanityCheckAliases();

  // It's my theory that this point right here is peak cbuild memory usage
  if (CarbonMem::isMemDebugOn()) {
    CarbonMem::checkpoint("carbon_peak_memory");
  }

  for (NetElabSetMapIter iter = mAliases.begin();
       iter != mAliases.end();
       ++iter) {
    NUNetElab *master = iter->first;
    NetElabSet &subords = iter->second;
    createAliases(master, &subords);
  }
  sanityCheckSTAllocation();

  // walkDesign doesn't encounter dead FLNodeElabs. make sure they are
  // updated with respect to our aliasing decisions.

  FLNodeElab* flnodeelab = NULL;
  for (FLNodeElabFactory::FlowLoop p = mFlowFactory->loopFlows(); p(&flnodeelab);)
  {
    // the net elabs may have been deleted by createAliases. just use
    // ptr comparisons.    
    NetElabMap::iterator s = mSubords.find(flnodeelab->getDefNet());
    if (s != mSubords.end()) {
      NUNetElab *master = s->second;
      if (master != flnodeelab->getDefNet()) {
        flnodeelab->replaceDefNet(flnodeelab->getDefNet(),
                                  master);
      }
    }
  }
  
  if (mBadAllocation) {
    mMsgContext->AllocationProblems();
  }
}


void REAlias::mergeAliasSets(NUNet *net,
			     NUNetSet &new_alias_set,
			     NUNetSetMap &net_alias_map,
			     SetOfNetSet &delete_sets)
{
  NUNetSetMapIter alias_map_iter = net_alias_map.find(net);
  if (alias_map_iter != net_alias_map.end()) {
    NUNetSet *existing_set = alias_map_iter->second;
    // Optimization -- don't merge the sets more than once
    if (delete_sets.find(existing_set) == delete_sets.end()) {
      new_alias_set.insert(existing_set->begin(), existing_set->end());
      delete_sets.insert(existing_set);
    }
  }
}


//! Walk all port connections, propagate z flags around.
class DeadPortCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  DeadPortCallback() : mWorkDone(false) {}
  ~DeadPortCallback() {}

  //! Did we fixup anything on the walk?
  bool getWorkDone() const { return mWorkDone; }

  //! Just interested in port connections
  Status operator()(Phase /* phase */, NUBase * /* node */) { return eSkip; }
  Status operator()(Phase /* phase */, NUDesign * /* node */) { return eNormal; }
  Status operator()(Phase /* phase */, NUModuleInstance * /* node */) { return eNormal; }
  Status operator()(Phase /* phase */, NUModule * /* node */) { return eNormal; }
  Status operator()(Phase /* phase */, NUNamedDeclarationScope * /* node */) { return eNormal; }

  Status operator()(Phase phase, NUPortConnectionInput *node)
  {
    if (phase == ePre) {
      if (not node->isUnconnectedActual()) {
	NUNet *formal = node->getFormal();
	NUNet *actual = node->getActual()->getWholeIdentifier();
	processNets(formal, actual);
      }
    }
    return eSkip;
  }

  Status operator()(Phase phase, NUPortConnectionOutput *node)
  {
    if (phase == ePre) {
      if (not node->isUnconnectedActual()) {
	NUNet *formal = node->getFormal();
	NUNet *actual = node->getActual()->getWholeIdentifier();
	processNets(formal, actual);
      }
    }
    return eSkip;
  }

  Status operator()(Phase phase, NUPortConnectionBid *node)
  {
    if (phase == ePre) {
      if (not node->isUnconnectedActual()) {
	NUNet *formal = node->getFormal();
	NUNet *actual = node->getActual()->getWholeIdentifier();
	processNets(formal, actual);
      }
    }
    return eSkip;
  }

private:
  void processNets(NUNet *formal, NUNet *actual)
  {
    // Propagate tri-written flag up hierarchy only
    if (formal->isTriWritten() and not actual->isTriWritten()) {
      mWorkDone = true;
      actual->putIsTriWritten(true);
    }

    // Primary-Z flag flows up unconditionally
    if (formal->isPrimaryZ() and not actual->isPrimaryZ()) {
      mWorkDone = true;
      actual->putIsPrimaryZ(true);
    }

    // Primary-Z flag flows down only if the submodule writes.
    else if (not formal->isPrimaryZ() and actual->isPrimaryZ()
             and (formal->isTriWritten() or
                  formal->isWritten()))
    {
      mWorkDone = true;
      formal->putIsPrimaryZ(true);
    }
  }

  bool mWorkDone;
};


void REAlias::deadPortFixup()
{
  bool work_done = false;
  do {
    DeadPortCallback callback;
    NUDesignWalker walker(callback, false);
    walker.design(mDesign);
    work_done = callback.getWorkDone();
  } while (work_done);
}


std::pair<NUNetElab*, NUNetElab*>
REAlias::determineMasterAlias(NUNetElab *causal_first, NUNetElab *causal_second)
{
  STBranchNode *first_hier = causal_first->getHier();
  STBranchNode *second_hier = causal_second->getHier();

  // Prefer primary ports to non-primary ports.
  bool isprim1 = causal_first->getNet()->isPrimaryPort();
  bool isprim2 = causal_second->getNet()->isPrimaryPort();
  if (isprim1 && !isprim2)
    return std::pair<NUNetElab*, NUNetElab*>(causal_first, causal_second);
  else if (isprim2 && !isprim1)
    return std::pair<NUNetElab*, NUNetElab*>(causal_second, causal_first);

  // First, do not ever choose a temp name over a user name
  bool temp1 = causal_first->getNet()->isTemp();
  bool temp2 = causal_second->getNet()->isTemp();
  if (temp1 && !temp2)
    return std::pair<NUNetElab*, NUNetElab*>(causal_second, causal_first);
  else if (temp2 && !temp1)
    return std::pair<NUNetElab*, NUNetElab*>(causal_first, causal_second);

  // Second, hierarchy determines aliasing
  int cmp = HierName::compare(first_hier, second_hier);
  if (cmp < 0) {
    return std::pair<NUNetElab*, NUNetElab*>(causal_first, causal_second);
  } else if (cmp > 0) {
    return std::pair<NUNetElab*, NUNetElab*>(causal_second, causal_first);
  }

  // Third, causality determines aliasing
  return std::pair<NUNetElab*, NUNetElab*>(causal_first, causal_second);
}


void REAlias::mergeAlias(NUNetElab *master_alias,
			 NUNetElab *subord_alias)
{
  // link the master and subordinate.
  mAliases[master_alias].insert(subord_alias);
  mSubords[subord_alias] = master_alias;

  NetElabSetMapIter subord_iter = mAliases.find(subord_alias);
  if (subord_iter == mAliases.end()) {
    return;
  }

  // if the subordinate has any aliases, update their master.
  NetElabSet &aliases = subord_iter->second;
  for (NetElabSetIter iter = aliases.begin();
       iter != aliases.end();
       ++iter) {
    NUNetElab *subord = *iter;
    mSubords[subord] = master_alias;
  }

  // add all existing subordinates to the master.
  mAliases[master_alias].insert(aliases.begin(), aliases.end());

  // the subordinate is no lnoger a master.
  mAliases.erase(subord_iter);
}


void REAlias::pairwiseAllocation(NUNetElab *net_being_allocated,
				 NUNetElab *net_allocated_to)
{
  // Don't create any circular references in the map
  if (net_being_allocated != net_allocated_to) {
    mAllocated[net_being_allocated] = net_allocated_to;
  }
}


void REAlias::rememberAllocation(NUNetElab *causal_first,
				 NUNetElab *causal_second)
{
  NUNet *causal_first_unelab = causal_first->getStorageNet();
  NUNet *causal_second_unelab = causal_second->getStorageNet();

  if (causal_second_unelab->isMultiplyDriven()) {
    // If a net is multiply driven, that is where we do the allocation.

    // Force the feeding net to never be allocated.
    mCannotAllocate.insert(causal_first_unelab);

    // Force all nets feeding-in to not be allocated.
    NUNetElab *feeding_net = causal_first;
    bool done = false;
    while (not done) {
      NetElabMapIter iter = mAllocated.find(feeding_net);
      if (iter != mAllocated.end()) {
	feeding_net = iter->second;
	if (feeding_net == causal_second) {
	  done = true;
	} else {
	  mCannotAllocate.insert(feeding_net->getStorageNet());
	}
      } else {
	done = true;
      }
    }

    pairwiseAllocation(causal_first, causal_second);

  } else {
    // If the causal_second net is already allocated to something else, then
    // use that (this will prevent two nets in the alias ring from being
    // allocated).
    // Otherwise, allocate to the causal_first.
    NetElabMapIter iter = mAllocated.find(causal_second);
    if (iter != mAllocated.end()) {
      // For bids, we will see the pair two times.  Make sure we don't allocate
      // something to itself.
      if (iter->second != causal_first) {
	pairwiseAllocation(causal_first, iter->second);
      }
    } else {
      pairwiseAllocation(causal_second, causal_first);
    }
  }
}


void REAlias::performAllocation()
{
  for (NetElabMapIter iter = mAllocated.begin();
       iter != mAllocated.end();
       ++iter) {
    NUNetElab *key = iter->first;
    NUNetElab *value = iter->second;

    if (mCannotAllocate.find(value->getStorageNet()) == mCannotAllocate.end()) {
      // Allocation is handled by the net mapped to (either it is allocated or
      // it is mapped to something which is allocated).
      key->getStorageNet()->putIsAllocated(false);

    } else {
      // If the cannot-allocate net is allocated to something else, then we
      // will use that for allocation.
      NetElabMapIter iter = mAllocated.find(value);
      if (iter != mAllocated.end()) {
	key->getStorageNet()->putIsAllocated(false);
      }
    }
  }

  for (NUNetSetIter iter = mCannotAllocate.begin();
       iter != mCannotAllocate.end();
       ++iter) {
    NUNet* net = *iter;
    net->putIsAllocated(false);
  }
}

void REAlias::rememberAlias(NUNetElab *in_causal_first,
                            NUNetElab *in_causal_second,
                            FLNodeElab *driver,
                            bool enforceCausalFirst)
{
  NUNetElab *causal_first = in_causal_first;
  NUNetElab *causal_second = in_causal_second;
  NUNetElab* master = causal_first;
  NUNetElab* subord = causal_second;
  std::pair <NUNetElab*, NUNetElab*> alias_order;
  
  // For bid port connections, will see the formal and actual 2 times (since the
  // logic flows both ways), so force the causality to be formal as causal first.
  NUUseDefNode* useDef = driver->getUseDefNode();
  if ((useDef != NULL) && useDef->isPortConnBid()) {
    NUPortConnectionBid *portconn = dynamic_cast<NUPortConnectionBid*>(useDef);
    NU_ASSERT(portconn, useDef);
    if (portconn->getFormal() == causal_second->getStorageNet()) {
      causal_first = in_causal_second;
      causal_second = in_causal_first;
    }
  }

  if (mVerbose) {
    UtIO::cout() << "Found alias:" << UtIO::endl;
    causal_first->print(false,2);
    causal_second->print(false,2);
    UtIO::cout() << UtIO::endl;
  }

  if (!enforceCausalFirst)
  {
    alias_order = determineMasterAlias(causal_first, causal_second);
    master = alias_order.first;
    subord = alias_order.second;
  }

  if (mVerbose) {
    UtIO::cout() << "Ordering of alias is:" << UtIO::endl;
    master->print(false,2);
    subord->print(false,2);
    UtIO::cout() << UtIO::endl;
  }

  if (mDoAllocation)
    rememberAllocation(causal_first, causal_second);

  // Issue warnings for z-conflicts
  if (subord->getNet()->isTriWritten() and master->getNet()->isWritten()) {
    if (helperIsNetInternallyDriven(master, driver) &&
        helperIsNetInternallyDriven(subord, driver))
    {
      UtString buf;
      master->getNet()->compose(&buf, master->getHier() );
      mTriMismatches.insert(TriMismatch(subord->getSymNode(), buf));
      //mMsgContext->NonZAliasForZNet(subord, buf.c_str());
    }
  } else if (master->getNet()->isTriWritten() and subord->getNet()->isWritten()) {
    if (helperIsNetInternallyDriven(subord, driver) && helperIsNetInternallyDriven(master, driver)) {
      UtString buf;
      subord->getNet()->compose(&buf, subord->getHier());
      mTriMismatches.insert(TriMismatch(master->getSymNode(), buf));
      //mMsgContext->NonZAliasForZNet(master, buf.c_str());
    }
  }

  // Propagate isTriWritten flag up hierarchy only, not down
  SInt32 master_depth = NUScope::determineModuleDepth(master->getHier());
  SInt32 subord_depth = NUScope::determineModuleDepth(subord->getHier());
  if (master_depth < subord_depth)
  {
    // Subord is lower level 
    if (subord->getNet()->isTriWritten()) {
      master->getNet()->putIsTriWritten(true);
    }
  } else {
    // Master is lower level 
    if (master->getNet()->isTriWritten()) {
      subord->getNet()->putIsTriWritten(true);
    }
  }

  rememberAliasHelper(master, subord, causal_first);

  // Remember the elim driver, if there is one.
  mElimDrivers.insert(NetNodeElabMap::value_type(in_causal_second, driver));

  // Remove this elim driver as a driver to be fixed up later.
  NetNodeElabSetMapIter all_driver_iter = mDrivers.find(in_causal_second);
  if (all_driver_iter != mDrivers.end()) {
    FLNodeElabSet & one_fanouts = all_driver_iter->second;
    FLNodeElabSet::iterator fanout_iter = one_fanouts.find(driver);
    if (fanout_iter != one_fanouts.end()) {
      one_fanouts.erase(fanout_iter);
    }
  }
} // void REAlias::rememberAliasDriver

void REAlias::rememberAliasHelper(NUNetElab* master, NUNetElab* subord,
                                  NUNetElab* causal_first)
{
  NetElabMapIter master_subord_iter = mSubords.find(master);
  NetElabMapIter subord_subord_iter = mSubords.find(subord);

  // Aliasing is transitive, handle the cases to find the winning master.
  NUNetElab *real_master = 0;

  // If master is a subordinate, then this subordinate becomes a subordinate
  // of that.
  if (master_subord_iter != mSubords.end()) {
    real_master = master_subord_iter->second;

  // Otherwise the master is the real master.
  } else {
    real_master = master;
  }

  // If this subordinate is a subordinate of something else, need to pick a
  // master.  Merge any existing aliases of the masters.
  if (subord_subord_iter != mSubords.end()) {
    NUNetElab *subord_master = subord_subord_iter->second;
    if (real_master != subord_master) {
      std::pair <NUNetElab*, NUNetElab*> alias_order;

      // Preserve causality
      if (subord == causal_first) {
        alias_order = determineMasterAlias(subord_master, real_master);
      } else {
        alias_order = determineMasterAlias(real_master, subord_master);
      }

      real_master = alias_order.first;
      NUNetElab *old_master = alias_order.second;
      mergeAlias(real_master, old_master);
    }
  }

  NU_ASSERT2(real_master != 0, master, subord);

  if (mVerbose && (master != real_master)) {
    UtIO::cout() << "Real master is:" << UtIO::endl;
    real_master->print(false,2);
    UtIO::cout() << UtIO::endl;
  }

  // Remember the alias information
  mergeAlias(real_master, subord);

  // Fixup the subordinate information.
  mSubords.erase(real_master);
} // void REAlias::rememberAliasHelper

void REAlias::reportTriMismatches()
{
  // Having gathered up all the tristate mismatch errors, print them
  // in sorted order
  for (SortedTriMismatchSet::iterator p = mTriMismatches.begin();
       p != mTriMismatches.end(); ++p)
  {
    TriMismatch tm = *p;
    mMsgContext->NonZAliasForZNet(tm.first, tm.second.c_str());
  }
}

void REAlias::rememberFanout(NUNetElab *net, FLNodeElab *fanout)
{
  if (fanout == 0) return;

  if (mVerbose) {
    UtIO::cout() << "Fanout:" << UtIO::endl;
    net->print(0,2);
    fanout->print(0,2);
    UtIO::cout() << UtIO::endl;
  }

  addNetFlow(mFanouts,net,fanout);
}


bool REAlias::addNetFlow(NetNodeElabSetMap & net_flow,
                         NUNetElab * net,
                         FLNodeElab * fanout)
{
  bool seen_before = false;

  FLNodeElabSet & one_net_fanouts = net_flow[net];

  seen_before = (one_net_fanouts.find(fanout) != one_net_fanouts.end());

  if (not seen_before) {
    one_net_fanouts.insert(fanout);
  }
  return seen_before;
}


bool REAlias::rememberDriver(NUNetElab *net, FLNodeElab *driver)
{
  if (mVerbose) {
    UtIO::cout() << "Driver:" << UtIO::endl;
    net->print(0,2);
    driver->print(0,2);
    UtIO::cout() << UtIO::endl;
  }

  // If we've already seen this driver, and its something we are gonna
  // eliminate, just return right away.
  NetNodeElabMapRange elim_driver_range = mElimDrivers.equal_range(net);
  if (elim_driver_range.first != elim_driver_range.second) {
    for (NetNodeElabMapIter elim_driver_iter = elim_driver_range.first;
	 elim_driver_iter != elim_driver_range.second;
	 ++elim_driver_iter) {
      if (elim_driver_iter->second == driver) {
	return true;
      }
    }
  }

  bool seen_before = addNetFlow(mDrivers,net,driver);
  return seen_before;
}


REAlias::Predicate::Predicate()
{
}

REAlias::Predicate::~Predicate()
{
}

class HierarchyPredicate: public REAlias::Predicate
{
public:
  HierarchyPredicate() {
    mFlow = NULL;
  }

  NUNetElab* findDriverNet(FLNodeElab* flow)
  {
    NUNetElab *driver_net = flow->getDefNet();
    if (driver_net == 0)
      return 0;

    mFlow = flow;
    return driver_net;
  }

  NUNetElab* findMaster()
  {
    NUUseDefNode *node = mFlow->getUseDefNode();
    if (node == 0) {
      return 0;
    }

    if (node->isPortConn()) {
      // Make sure the port connection can be aliased
      NUPortConnection *port_conn = dynamic_cast<NUPortConnection*>(node);
      NU_ASSERT(port_conn,node);
      if (not port_conn->isAliasable()) {
        return 0;
      }

      // Find the alias
      Iter<FLNodeElab*> l = mFlow->loopFanin();
      if (not l.atEnd()) {
        FLNodeElab* flnode = *l;
        return flnode->getDefNet();
      } else {
        // The port is either undriven or unconnected on the driving end.
        // Test for unconnected.
        if (port_conn->isUnconnectedActual()) {
          return 0;
        }
        
        // It is undriven, find the elaborated net which is
        // undriven. This is the opposite net of what mFlow
        // represents. So if mFlow represents the formal, then lookup
        // the actual. Otherwise lookup the formal.
        NUNet* net = mFlow->getDefNet()->getStorageNet();
        STBranchNode* hier = mFlow->getDefNet()->getStorageHier();
        NUNet *formal = port_conn->getFormal();
        NUNet *actual = port_conn->getActualNet();
        if (net == actual) {
          // The formal drives the actual. This is either an output or
          // bid port
          NUModuleInstance *instance = port_conn->getModuleInstance();
          NUModuleElab *module_elab = instance->lookupElab(hier);
          return formal->lookupElab(module_elab->getHier());
          
        } else if (net == formal) {
          // The actual drives the formal. This is either an input or
          // bid port.
          return actual->lookupElab(hier->getParent());
          
        } else {
          NU_ASSERT("Net is neither formal nor actual"==NULL, net);
        }
        return 0;
      }
    }
    return 0;
  } // NUNetElab* findMaster

  FLIterFlags::TraversalT getVisitFlags()
  {
    return FLIterFlags::eAll;  // Visit everything
  }

  FLIterFlags::NestingT getNestingFlags()
  {
    return FLIterFlags::eBranchAtAll;
  }

  bool enforceCausalFirst() const {return false;}

private:
  FLNodeElab* mFlow;
}; // class HierarchyPredicate: public REAlias::Predicate

void REAlias::walkDesign(bool createAliases)
{
  Predicate* p = mPredicate;
  HierarchyPredicate hp;
  if (p == 0)
    p = &hp;
  bool enforceCausalFirst = p->enforceCausalFirst();
  FLIterFlags::StartT startFlags =
    FLIterFlags::StartT(FLIterFlags::eModuleLocals |
                        FLIterFlags::eClockMasters);

  // The new allocator needs to have all the design traversed, not
  // just what's live from a primary-outputs fanin traversal.
  if (! mDoAllocation)
    startFlags = FLIterFlags::StartT(FLIterFlags::eAllElaboratedNets |
                                     startFlags);
  for (FLDesignElabIter iter(mDesign,
			     mSymtab,
                             startFlags,
			     p->getVisitFlags(),
			     FLIterFlags::eNone, // Stop at nothing
			     p->getNestingFlags(),
			     FLIterFlags::eIterFaninPairOnce);
       not iter.atEnd();
       iter.next())
  {
    FLNodeElab *driver = *iter;
    FLNodeElab *fanout = iter.getCurParent();
    NUNetElab* driver_net = p->findDriverNet(driver);

    if (driver_net != 0)
    {
      // If we are not going to create aliases, we don't need to
      // remember the fanouts or drivers
      bool seen_before;
      if (createAliases) {
        // Remember the fanout and driver, since it is possible this net will be
        // aliased to something, and will need to fix those up.
        rememberFanout(driver_net, fanout);

        // If we've seen this driver before, then don't do the rest.
        seen_before = rememberDriver(driver_net, driver);
      } else {
        NUNetElab* fanin_net = p->findMaster();
        seen_before = (fanin_net != 0) && isAlias(fanin_net, driver_net);
      }

      // Only add the alias if we haven't seen them before
      if (!seen_before) {
        NUNetElab* fanin_net = p->findMaster();
        if (fanin_net != 0) 
          rememberAlias(fanin_net, driver_net, driver,
                        enforceCausalFirst);
      }
    } // if
  } // for
} // void REAlias::walkDesign

void REAlias::aliasDeadPort(NUNetElab* master, NUNetElab* alias) {
  NetElabMap::iterator alias_iter = mSubords.find(alias);
  bool aliasHasMaster = (alias_iter != mSubords.end());
  NetElabMap::iterator master_iter = mSubords.find(master);
  bool masterHasMaster = (master_iter != mSubords.end());

  bool aliasKnown = (aliasHasMaster ||
                     (mAliases.find(alias) != mAliases.end()));
  bool masterKnown = (masterHasMaster ||
                      (mAliases.find(master) != mAliases.end()));
#if 1
  if (aliasHasMaster)
    alias = alias_iter->second;
  if (masterHasMaster)
    master = master_iter->second;

  if (alias != master) {
    // Under some conditions, the causality implied by the args
    // to this routine is not consistent with what we need to 
    // get aliasing to work.  For example, we are fixing the
    // alias across a dead output port.
    if (aliasKnown && !masterKnown) {
      rememberAliasHelper(alias, master, NULL);
    }
    else {
      rememberAliasHelper(master, alias, NULL);
    }
  }
#else
    if (aliasKnown && (!masterKnown /* || (masterHasMaster && !aliasHasMaster) */)) {
      NUNetElab* swapTmp = alias;
      alias = master;
      master = swapTmp;
    }

    rememberAliasHelper(master, alias, NULL);

  if (aliasKnown && masterKnown) {
    // See if we are merging two dead rings
    if (masterHasMaster) {
      NUNetElab* masterMaster = master_iter->second;
      NUNetElab* aliasMaster = aliasHasMaster? alias_iter->second: 0;
      if ((masterMaster != alias) && (aliasMaster != masterMaster))
      {
        rememberAliasHelper(master, alias, NULL);
      }
    }
    else if (aliasHasMaster) {
      NUNetElab* aliasMaster = aliasHasMaster? alias_iter->second: 0;
      if (aliasMaster != master) {
        rememberAliasHelper(aliasMaster, master, NULL);
      }
    }
    else {
      rememberAliasHelper(master, alias, NULL);
    }
  }
  else if (aliasKnown && !masterKnown) {
    rememberAliasHelper(alias, master, NULL);
  }
  else {
    rememberAliasHelper(master, alias, NULL);
  }
#endif
} // void REAlias::aliasDeadPort

// Correct allocation depends on complete hierarchical aliasing, which
// is not currently provided by REAlias.  REAlias skips locally dead nets, 
// even if they are connected hierarchical to live nets.  See 
// test/alloc/deadhier.v for an example.
void REAlias::fixMissedAliases(STBranchNode* parent) {
  for (SInt32 i = 0, n = parent->numChildren(); i < n; ++i) {
    STSymbolTableNode* child = parent->getChild(i);
    if (child != NULL) {
      STBranchNode* childBranch = child->castBranch();
      if (childBranch != NULL) {
        NUModuleElab* moduleElab = NUModuleElab::find(childBranch);
        if (moduleElab != NULL) {
          NUModuleInstance* inst = moduleElab->getModuleInstance();
          if (inst != NULL) {
            for (NUPortConnectionLoop q = inst->loopPortConnections();
                 !q.atEnd(); ++q) {
              NUPortConnection* con = *q;
              if (con->isAliasable()) {
                NUNet* formalNet = con->getFormal();
                NU_ASSERT(formalNet,con);
                NUNetElab* formal = formalNet->lookupElab(childBranch);
                NU_ASSERT(formal,con);
                NUNet* actualNet = con->getActualNet();
                NU_ASSERT(actualNet,con);
                NUNetElab* actual = actualNet->lookupElab(parent);
                NU_ASSERT(actual,con);

                // If these two net-elabs are not alisaed together,
                // then that means they were not encountered during
                // the flow-walk in REAlias.  Alias them now.
                switch (con->getType()) {
                  case eNUPortConnectionBid:
                    aliasDeadPort(formal, actual);
                    aliasDeadPort(actual, formal);
                    break;
                  case eNUPortConnectionOutput:
                    aliasDeadPort(formal, actual);
                    break;
                  case eNUPortConnectionInput:
                    aliasDeadPort(actual, formal);
                    break;
                  default:
                    NU_ASSERT("Unexpected port connection type"==NULL,con);
                }
              } // if
            } // for
            fixMissedAliases(childBranch);
          } // if
        } else {
          // Look for instances inside named declaration scopes.
          NUNamedDeclarationScopeElab* declScopeElab = 
            NUNamedDeclarationScopeElab::find(childBranch);
          if (declScopeElab != NULL) {
            fixMissedAliases(childBranch);
          }
        }
      } // if
    } // if
  } // for
} // void REAlias::fixMissedAliases

void REAlias::walkSymtab() {
  for (STSymbolTable::RootIter i = mSymtab->getRootIter(); !i.atEnd(); ++i) {
    STSymbolTableNode* node = *i;
    STBranchNode* root = node->castBranch();
    if (root != NULL) {
      fixMissedAliases(root);
    }
  }
}

void REAlias::dumpAliasStats()
{
//#define DUMP_ALIAS_STATS
#ifdef DUMP_ALIAS_STATS
  NUNetElabSet storedNetElabs;
#define HIST_SIZE 100
  // Walk the alias rings and populate the histogram
  int ringSizes[HIST_SIZE+1];
  memset(ringSizes, 0, sizeof(ringSizes));
  for (NetElabSetMapIter i = mAliases.begin(); i != mAliases.end(); ++i) {
    // Count the number of stored net elabs as keys in this map
    NUNetElab* netElab = i->first;
    storedNetElabs.insert(netElab);

    // Populate the histogram of alias ring sizes
    NetElabSet& netElabSet = i->second;
    int size = netElabSet.size();
    if (size > HIST_SIZE)
      size = HIST_SIZE;
    ++ringSizes[size];
  }
    
  // Walk the aliases and populate the stored net elabs
  for (NetElabMapIter i = mSubords.begin(); i != mSubords.end(); ++i) {
    NUNetElab* netElab = i->first;
    storedNetElabs.insert(netElab);
  }

  // Print the data
  UtIO::cout() << "There are " << storedNetElabs.size() << " stored keys\n";
  UtIO::cout() << "There are " << mAliases.size() << " master keys\n";
  UtIO::cout() << "There are " << mSubords.size() << " alias keys\n";
  UtIO::cout() << "The alias ring histogram is:\n";
  for (int i = 0; i < HIST_SIZE; ++i) {
    if (ringSizes[i] > 0) {
      UtIO::cout() << "   " << i << " => " << ringSizes[i] << "\n";
    }
  }
  UtIO::cout() << "  >" << HIST_SIZE-1 << " => " << ringSizes[HIST_SIZE]
               << "\n";
  storedNetElabs.clear();
#endif
} // static void sDumpAliasStats

void REAlias::findAliases(Stats* stats, bool phase_stats, bool createAliases)
{
  walkDesign(createAliases);
  walkSymtab();                 // alias dead ports
  dumpAliasStats();             // See the routine to enable this

  if (phase_stats) {
    stats->printIntervalStatistics("WalkDesign");
  }
}


void REAlias::aliasDesign(Stats *stats, bool phase_stats)
{
  if (phase_stats) {
    stats->pushIntervalTimer();
  }

  findAliases(stats, phase_stats, true);

  if (mDoAllocation)
  {
    performAllocation();
    if (phase_stats) {
      stats->printIntervalStatistics("Allocation");
    }
  }

  walkAliases();

  if (phase_stats) {
    stats->printIntervalStatistics("WalkAliases");
  }

  deadPortFixup();

  if (phase_stats) {
    stats->printIntervalStatistics("DeadPortFixup");
    stats->popIntervalTimer();
  }

  reportTriMismatches();
}


REAlias::~REAlias()
{
}

void REAlias::getAliases(NUNetElab* netElab, NUNetElabVector* netElabs)
{
  // Get the master net if this isn't it
  NUNetElab* master = getMaster(netElab);
  netElabs->push_back(master);

  // Gather the aliases if it has any
  NetElabSetMap::iterator posAlias = mAliases.find(master);
  if (posAlias != mAliases.end()) {
    NetElabSet& aliases = posAlias->second;
    for (NetElabSetLoop l = aliases.loopUnsorted(); !l.atEnd(); ++l) {
      NUNetElab* alias = *l;
      netElabs->push_back(alias);
    }
  }
}

NUNetElab* REAlias::getMaster(NUNetElab* netElab)
{
  NUNetElab* master = netElab;
  NetElabMap::iterator posSub = mSubords.find(netElab);
  if (posSub != mSubords.end()) {
    master = posSub->second;
  }
  return master;
}

bool REAlias::isAlias(NUNetElab* netElab1, NUNetElab* netElab2)
{
  // They are aliases if they have the same master
  return getMaster(netElab1) == getMaster(netElab2);
}

//! Class to walk all design port connections to find potential aliases
/*! This class walks the entire design and for each port connection it
 *  adds an alias between the elaborated actuals and elaborated
 *  formals.
 *
 *  This method for finding port connections is preferred because a
 *  flow walk does not work before port coercion.
 *
 *  This class is a callback for the NUDesignWalker. Note that for it
 *  to work correctly, the NUDesignWalker rememberVisited flag should
 *  be false so that we visit all port connection instances.
 */
class FindPortConnections : public NUInstanceCallback
{
public:
  //! constructor
  FindPortConnections(REAlias* alias, STSymbolTable* symTab) :
    NUInstanceCallback(symTab), mAlias(alias) {}

  //! handle modules (nothing to be done for top modules)
  void handleModule(STBranchNode*, NUModule*) { return; }

  //! handle declaration scopes
  void handleDeclScope(STBranchNode*, NUNamedDeclarationScope*) { return; }

  //! handle instances (find all the port connections for the instance)
  void handleInstance(STBranchNode* branch, NUModuleInstance* instance);

private:
  //! The alias class to apply all port connection aliases
  REAlias* mAlias;
};

void 
FindPortConnections::handleInstance(STBranchNode* branch,
                                    NUModuleInstance* instance)
{
  for (NUPortConnectionLoop q = instance->loopPortConnections(); !q.atEnd(); ++q) {
    // Get the formal elaborated net
    NUPortConnection* port = *q;
    NUNet* formalNet = port->getFormal();
    NU_ASSERT(formalNet != NULL, port);
    NUNetElab* formal = formalNet->lookupElab(branch);
    NU_ASSERT(formal != NULL, port);

    // Get the actuals. Note that for NUPortConnectionInput this will
    // also get the index into a bit of part select as a potential
    // alias. That is not a bug but a potential optimization
    // breaker. The code to get the actuals that could be aliases is
    // more complex and I'm not sure it is warranted here.
    NUNetSet actuals;
    port->getActuals(&actuals);

    // Elaborated the actuals and alias them to the formals
    STBranchNode* parent = branch->getParent();
    for (NUNetSetIter i = actuals.begin(); i != actuals.end(); ++i) {
      // Make sure we don't have an unconnected port. In that case the
      // actual net would be the same as the formal net.
      NUNet* actualNet = *i;
      if (actualNet != formalNet) {
        NUNetElab* actual = actualNet->lookupElab(parent);
        NU_ASSERT(actual != NULL, port);

        // Apply the alias
        mAlias->aliasDeadPort(formal, actual);
      }
    }
  }
}


void REAlias::findPessimisticAliases()
{
  // Walk the design finding all port connections. We assume that all
  // elaborated nets across the port connections are aliases for this
  // pessimistic aliasing. We should use this information for
  // optimizing early in the compile. We do this with a design walk
  // because port directions may not be correct and we have to be
  // pessimistic.
  FindPortConnections callback(this, mSymtab);
  NUDesignWalker walker(callback, false, false);
  walker.design(mDesign);
}
