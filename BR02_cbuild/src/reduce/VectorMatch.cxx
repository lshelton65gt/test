// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


// Regular expression includes -- need to come before VectorMatch.h
extern "C" {
#include <sys/types.h>
#include "rxspencer/regex.h"
}

#include "reduce/VectorMatch.h"
#include "reduce/RewriteUtil.h"

#include "nucleus/NUDesignWalker.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUAliasDB.h"

#include "iodb/IODBNucleus.h"

#include "symtab/STSymbolTable.h"

#include "util/ArgProc.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/AtomicCache.h"
#include "util/UtWildcard.h"

#ifndef DEBUG_CDS_REGEX
// Set this to 1 if you want to dump all regex matches
#define DEBUG_CDS_REGEX 0
#endif

VectorMatch::VectorMatch(AtomicCache * str_cache,
			 NUNetRefFactory * netref_factory,
			 MsgContext * msg_context,
			 IODBNucleus * iodb,
			 ArgProc * args,
			 bool verbose,
			 bool portVerbose,
			 VectorMatchStatistics *stats) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mIODB(iodb),
  mArgs(args),
  mStatistics(stats),
  mVerbose(verbose),
  mPortVerbose(portVerbose)
{
}

VectorMatch::~VectorMatch()
{
}

void VectorMatch::module(NUModule * one)
{
  VectorMatchModule one_module(one,
			       mStrCache,
			       mNetRefFactory,
			       mIODB,
			       mStatistics,
			       mVerbose,
			       mPortVerbose);
  bool reconstructed_vectors = one_module.reconstruct();

  if (reconstructed_vectors) {
    mStatistics->module();
  }
}


bool VectorMatchModule::reconstruct()
{
  bool success = false;

  const int num_patterns = 5;
#if REGEX_GROUP_BUG
  const RE patterns[num_patterns] = { 
    // first $temp ruleset.
    RE("^(\\$flatten_\\$?[a-zA-Z0-9_]*[a-zA-Z_;]+)([0-9]+_[a-zA-Z]+[a-zA-Z0-9_]*);[0-9]+$",
       "^([0-9]+)(_[a-zA-Z]+[a-zA-Z0-9_]*)$"),
    // second $temp ruleset.
    RE("^(\\$flatten_\\$?[a-zA-Z_;]+)([0-9]+_[a-zA-Z]+[a-zA-Z0-9_]*);[0-9]+$",
       "^([0-9]+)(_[a-zA-Z]+[a-zA-Z0-9_]*)$"),
    // User-name ordinal ruleset
    // Support names like:
    //    foo_1st, foo2nd, foo_3rd_bar
    // This ruleset has a more strict remainder rule than initial rule.
    RE("^([a-zA-Z_]+)([0-9]+[a-zA-Z_]*)$",
       "^([0-9]*1st|[0-9]*2nd|[0-9]*3rd|[0-9]*[0-9]th)($|_[a-zA-Z_]*$)"),
    // user-name ruleset.
    RE("^([a-zA-Z0-9_]+_|[a-zA-Z0-9]*[a-zA-Z]+)([0-9]+[a-zA-Z_]*)$",
      "^([0-9]+)([a-zA-Z_]*)$"),
    // rule for tool-generated flattened vector names:
    //   reg \x.y.a[0] , \x.y.a[1] , \x.y.a[2] ;
    //   reg \x.y.b[0] , \x.y.b[1] , \x.y.b[2] ;
    // This rule matches Verilog escaped names that finish with [N].
    RE ("^(\\\\[^[]*\\[)([0-9]+\\] +)$", "([0-9]*)(\\] +)$")
  };
#else // REGEX_GROUP_BUG
  const RE patterns[num_patterns] = { 
    // first $temp ruleset.
    RE("^(\\$flatten_\\$?[a-zA-Z0-9_]*[a-zA-Z_;]+)([0-9]+)(_[a-zA-Z]+[a-zA-Z0-9_]*);[0-9]+$"),
    // second $temp ruleset.
    RE("^(\\$flatten_\\$?[a-zA-Z_;]+)([0-9]+)(_[a-zA-Z]+[a-zA-Z0-9_]*);[0-9]+$"),
    // User-name ordinal ruleset
    // Support names like:
    //    foo_1st, foo2nd, foo_3rd_bar
    RE("^([a-zA-Z_]+)([0-9]+|[0-9]*1st|[0-9]*2nd|[0-9]*3rd|[0-9]*[0-9]th)($|_[a-zA-Z_]*$)"),
    // user-name ruleset.
    RE("^([a-zA-Z0-9_]+_|[a-zA-Z0-9]*[a-zA-Z]+)([0-9]+)([a-zA-Z_]*)$")
    // This rule matches Verilog escaped names that finish with [N].
    RE ("^(\\\\[^[]*\\[)([0-9]+)(\\] +)$")
  };
#endif // REGEX_GROUP_BUG

  // Search ports -- just informational.
  if (mPortVerbose) {
    findPortCandidates(patterns[3]);
    printPortCandidates();
    cleanup();
  }

  // two iterations: one for larger vectors, the second for everything.
  for (int i=0; i<2; ++i) {
    SInt32 size_limit;
    if (i==0) size_limit = 4;
    else      size_limit = 1;

    // Search locals.
    for (int j=0; j<num_patterns; ++j) {
      // Search.
      findCandidates(patterns[j]);

      // Create.
      success |= createVectors(size_limit);

      if (mVerbose) {
	printCandidates(true);
      }

      // Create maps from old to new.
      setupReplacements();

      // Replace.
      useVectors();

      // Cleanup.
      cleanup();
    }
  }

  return success;
}


bool VectorMatchModule::findCandidates(const RE & patterns)
{
  bool success = false;
  for (NUNetLoop loop = mModule->loopLocals();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    // Make sure it matches any vectors names the user specified
    bool match = true;
    if (mIODB->hasVectorMatches()) {
      match = false;
      for (IODBNucleus::WildcardsLoop l = mIODB->loopVectorMatches();
           !l.atEnd() && !match;
           ++l) {
        UtWildcard* wildcard = *l;
        match = wildcard->isMatch(net->getName()->str());
      }
    }
    if (match) {
      success |= findCandidates(net, patterns);
    }
  }
  return success;
}


bool VectorMatchModule::findPortCandidates(const RE & patterns)
{
  bool success = false;
  for (NUNetVectorLoop loop = mModule->loopPorts();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    success |= findCandidates(net, patterns);
  }
  return success;
}


bool VectorMatchModule::findCandidates(NUNet * net, const RE & patterns)
{
  if (not qualify(net)) {
    return false;
  }

#if REGEX_GROUP_BUG
  return findNameMatchCandidates(net, patterns.re_vector, patterns.re_remainder);
#else // REGEX_GROUP_BUG
  return findNameMatchCandidates(net, patterns.re_vector);
#endif // REGEX_GROUP_BUG
}

static bool sMatchToString(UtString* str, const char* cstr, REMatch* matches,
                           int i)
{
  if ((matches[i].rm_so == -1) || (matches[i].rm_so > matches[i].rm_eo))
    return false;
  size_t sz = matches[i].rm_eo - matches[i].rm_so;
  str->assign(cstr, matches[i].rm_so, sz);
  return true;
}

#if REGEX_GROUP_BUG
bool VectorMatchModule::findNameMatchCandidates(NUNet * net,
                                                const char * re_vector,
                                                const char * re_remainder)
{
  bool success = false;
  StringAtom * name = net->getName();
  const char * cname = name->str();

  // TBD: don't recompile the RE each time.

  const int max_matches = 10;
  int compile_status;

  regex_t rec_vector;
  compile_status = regcomp(&rec_vector, re_vector, REG_EXTENDED);
  ASSERT(compile_status==0);

  regex_t rec_remainder;
  compile_status = regcomp(&rec_remainder, re_remainder, REG_EXTENDED);
  ASSERT(compile_status==0);

  REMatch matches[max_matches];
  int search_status;
  search_status = regexec(&rec_vector, cname, max_matches, matches, 0);
  if (search_status==0) {
#if DEBUG_CDS_REGEX
    printMatches(cname,max_matches,matches);
#endif
    
    // The 0th match is the full string. Ignore it.
    success = true;

    UtString vectnamestart, vectrest, vectindex, vectnameend, vectname;

    // The 1st match is the first part of the name.
    bool match_copied = sMatchToString(&vectnamestart, cname, matches, 1);
    NU_ASSERT(match_copied,net);

    // The 2nd match is the rest of the name..
    match_copied = sMatchToString(&vectrest, cname, matches, 2);
    NU_ASSERT(match_copied,net);

    const char * rest_cname = vectrest.c_str();
    REMatch rmatches[max_matches];
    search_status = regexec(&rec_remainder, rest_cname, max_matches, rmatches, 0); 

    if (search_status==0) {
#if DEBUG_CDS_REGEX
      printMatches(rest_cname,max_matches,rmatches);
#endif

      // The 0th match is the full string. Ignore it.

      // The 1st match is the index.
      match_copied = sMatchToString(&vectindex, rest_cname, rmatches, 1);
      NU_ASSERT(match_copied,net);

      // Parse out the index, ignoring the ordinal suffix (ie 12th becomes 12)
      const char * index_cname = vectindex.c_str();
      char * end_index_ptr = NULL; // trash; updated by OSStrToU32
      SInt32 index = OSStrToU32(index_cname, &end_index_ptr, 10, NULL);
      bool is_number = (end_index_ptr != index_cname);
      NU_ASSERT(is_number,net); // our regexp should avoid all non-numbers/ordinals.

      // The 2nd match is the second part of the name.
      match_copied = sMatchToString(&vectnameend, rest_cname, rmatches, 2);
      NU_ASSERT(match_copied,net);

      vectname << vectnamestart << vectnameend;

      const char * vect_cname = vectname.c_str();

      // disallow vector match over gate-style names.
      //    n1012, net1023, n_10123.
      bool gate_net_name = ( (strcasecmp(vect_cname, "n") == 0) or
                             (strcasecmp(vect_cname, "n_") == 0) or
                             (strcasecmp(vect_cname, "net") == 0) );
      if (gate_net_name) {
        if (mVerbose) {
          //UtIO::cout() << "gate name: " << vectname << UtIO::endl;
        }
      } else {
        VectorCandidate * candidate = getCandidate(vectname);
        candidate->insert(index,net);
      }
    } else {
      // anything besides a NOMATCH is an error.
      NU_ASSERT(search_status==REG_NOMATCH,net);
    }
  } else {
    // anything besides a NOMATCH is an error.
    NU_ASSERT(search_status==REG_NOMATCH,net);
  }

  regfree(&rec_vector);
  regfree(&rec_remainder);

  return success;
}
#else // REGEX_GROUP_BUG
bool VectorMatchModule::findNameMatchCandidates(NUNet * net, const char * re_vector)
{
  bool success = false;
  StringAtom * name = net->getName();
  const char * cname = name->str();

  // TBD: don't recompile the RE each time.

  const int max_matches = 3;
  regex_t rec_vector;
  int compile_status = regcomp(&rec_vector, re_vector, REG_EXTENDED);
  ASSERT(compile_status==0);

  REMatch matches[max_matches];
  int search_status = regexec(&rec_vector,
			      cname,
			      max_matches,
			      matches, 0);
  if (search_status==0) {
#if DEBUG_CDS_REGEX
    printMatches(cname,max_matches,matches);
#endif

    // The 0th match is the full string. Ignore it.
    success = true;

    UtString vectnamestart, vectindex, vectnameend, vectname;

    // The 1st match is the first part of the name.
    bool match_copied = sMatchToString(&vectnamestart, cname, matches, 1);
    NU_ASSERT(match_copied,net);

    // The 2nd match is the index.
    match_copied = sMatchToString(&vectindex, cname, matches, 2);
    NU_ASSERT(match_copied,net);

    SInt32 index;
    bool is_number = StringUtil::parseNumber(vectindex.c_str(), &index);
    NU_ASSERT(is_number,net); // our regexp should avoid all non-numbers.

    // The 3rd match is the rest part of the name.
    match_copied = sMatchToString(&vectnameend, cname, matches, 3);
    NU_ASSERT(match_copied,net);

    vectname << vectnamestart << vectnameend;

    UtString gate_net_name("net");
    UtString gate_n_name("n");

    // disallow vector match over gate-style names.
    //    n1012, net1023. 
    if ( (vectname == gate_net_name) or
         (vectname == gate_n_name) ) {
      if (mVerbose) {
        //UtIO::cout() << "gate name: " << vectname << UtIO::endl;
      }
    } else if ( (vectnameend == gate_net_name) or
                (vectnameend == gate_n_name) ) {
      if (mVerbose) {
        //UtIO::cout() << "gate name: " << vectnameend << UtIO::endl;
      }
    } else {
      VectorCandidate * candidate = getCandidate(vectname);
      candidate->insert(index,net);
    }

  } else {
    // anything besides a NOMATCH is an error.
    NU_ASSERT(search_status==REG_NOMATCH,net);
  }

  regfree(&rec_vector);

  return success;
}
#endif // REGEX_GROUP_BUG


bool VectorMatchModule::qualify(const NUNet * net)
{
  NUDesign* design = mIODB->getDesign();
  NUAliasBOM* alias_bom = design->getAliasBOM();
  CarbonIdent* netIdent = net->getIdent();
  return (
	  // user declared as 2 or 3d 
	  net->isBitNet() and
	  // would be nice to eventually process clocks.
	  (not net->isEdgeTrigger()) and 
	  // user has made mention of this net in a directives file.
	  (not net->isProtected(mIODB)) and
          // If this net has been expressioned don't mess with itsx
          (not (netIdent && (alias_bom->getExpr(netIdent) != NULL))) and
          (not net->isUndriven ())      // workaround for bug6846
	  );
}


bool VectorMatchModule::createVectors(SInt32 size_limit)
{
  bool success = false;
  for (CandidateMap::iterator iter = mCandidates.begin();
       iter != mCandidates.end();
       ++iter) {
    VectorCandidate * candidate = (*iter).second;
    bool qualified = candidate->qualify(size_limit);
    if (qualified and not candidate->isPort()) {
      createVector(candidate);
      mVectors.insert(candidate);
      success = true;
    }
  }
  return success;
}


void VectorMatchModule::createVector(VectorCandidate * candidate)
{
  std::pair<SInt32,SInt32> bounds = candidate->getBounds();
  StringAtom * name = mStrCache->intern(candidate->getName().c_str());

  VectorCandidate::iterator first = candidate->begin();
  ASSERT(first!=candidate->end());
  NUNet * first_net = (*first).second;
  const SourceLocator & loc = first_net->getLoc();
  NUNet * net = new NUVectorNet(name,
				new ConstantRange(bounds.second, bounds.first),
				NetFlags(first_net->getFlags()|eTempNet),
                                VectorNetFlags (0),
				mModule,
				loc);
  mModule->addLocal(net);

  candidate->setNet(net);
}


void VectorMatchModule::setupReplacements()
{
  for (CandidateSet::iterator iter = mVectors.begin();
       iter != mVectors.end();
       ++iter) {
    VectorCandidate * candidate = (*iter);
    setupReplacements(candidate);
  }
}


void VectorMatchModule::setupReplacements(VectorCandidate * candidate)
{
  NUNet * repl_net = candidate->getNet();
  for (VectorCandidate::iterator iter = candidate->begin();
       iter != candidate->end();
       ++iter) {
    SInt32 index = (*iter).first;
    NUNet * orig_net = (*iter).second;

    // Having matched a5,a6,a7 as vector, we will declare it as
    // a[7:5], but use offsets 0,1,2, respectively, for bitselects
    // into that vector.
    const SourceLocator& loc = repl_net->getLoc();
    NUVectorNet* vn = dynamic_cast<NUVectorNet*>(repl_net);
    NU_ASSERT(vn,repl_net);
    UInt32 offset = vn->getDeclaredRange()->offsetBounded(index);
    ConstantRange range (offset, offset);
    NULvalue * lvalue = new NUVarselLvalue(repl_net, range, loc);
    lvalue->resize();

    NUExpr * rvalue = new NUVarselRvalue(repl_net, range, loc);
    rvalue->resize(1);

    mLvalueReplacements[orig_net] = lvalue;
    mRvalueReplacements[orig_net] = rvalue;
    mStatistics->in();
  }
  mStatistics->out();
}


void VectorMatchModule::useVectors()
{
  NUNetReplacementMap     dummy_net_replacements;
  NUTFReplacementMap      dummy_tf_replacements;
  NUCModelReplacementMap  dummy_cmodel_replacements;
  NUAlwaysBlockReplacementMap      dummy_always_replacements;
  RewriteLeaves translator(dummy_net_replacements,
                           mLvalueReplacements,
                           mRvalueReplacements,
                           dummy_tf_replacements,
                           dummy_cmodel_replacements,
                           dummy_always_replacements,
                           NULL);
  mModule->replaceLeaves(translator);
}


void VectorMatchModule::cleanup()
{
  // delete the nets that we have replaced.
  for (CandidateSet::iterator iter = mVectors.begin();
       iter != mVectors.end();
       ++iter) {
    VectorCandidate * candidate = (*iter);
    cleanup(candidate);
  }
  
  // delete all candidate structures
  for (CandidateMap::iterator iter = mCandidates.begin();
       iter != mCandidates.end();
       ++iter) {
    VectorCandidate * candidate = (*iter).second;
    delete candidate;
  }

  // delete temporary lvalue and rvalue structures.
  for (NULvalueReplacementMap::iterator iter = mLvalueReplacements.begin();
       iter != mLvalueReplacements.end();
       ++iter) {
    delete (*iter).second;
  }
  for (NUExprReplacementMap::iterator iter = mRvalueReplacements.begin();
       iter != mRvalueReplacements.end();
       ++iter) {
    delete (*iter).second;
  }

  // Reset all local context.
  mVectors.clear();
  mCandidates.clear();
  mLegalNames.clear();
  mLvalueReplacements.clear();
  mRvalueReplacements.clear();
}


void VectorMatchModule::cleanup(VectorCandidate * candidate)
{
  for (VectorCandidate::iterator iter = candidate->begin();
       iter != candidate->end();
       ++iter) {
    NUNet * orig_net = (*iter).second;
    NUScope * scope = orig_net->getScope();
    scope->removeLocal(orig_net);
    delete orig_net;
  }
}


VectorCandidate * VectorMatchModule::getCandidate(UtString & name)
{
  CandidateMap::iterator location = mCandidates.find(name);

  VectorCandidate * candidate = NULL;
  if (location != mCandidates.end()) {
    candidate = (*location).second;
  } else {
    UtString legal_name = getLegalName(name);
    candidate = new VectorCandidate(legal_name);
    mCandidates.insert(CandidateMap::value_type(name, candidate));
    mLegalNames.insert(legal_name);
  }
  ASSERT(candidate);
  return candidate;
}


UtString VectorMatchModule::getLegalName(UtString & name)
{
  UtString candidate_name = name;
  StringAtom * sname = mStrCache->intern(candidate_name.c_str());
  STSymbolTable * aliasDB = mModule->getAliasDB();
  while ( (aliasDB->find(NULL,sname) != NULL) or
	  (mLegalNames.find(candidate_name) != mLegalNames.end()) ) {
    candidate_name += "_";
    sname = mStrCache->intern(candidate_name.c_str());
  }
  return candidate_name;
}


std::pair<SInt32,SInt32> VectorCandidate::getBounds() const
{
  const_iterator iter = begin();
  SInt32 min = (*iter).first;
  SInt32 max = (*iter).first;
    
  for (++iter; iter != end(); ++iter) {
    min = std::min((*iter).first,min);
    max = std::max((*iter).first,max);
  }

  return std::pair<SInt32,SInt32>(min,max);
}


void VectorMatchModule::printMatches(const char * cname, int max_matches, REMatch * matches) {
  UtIO::cout() << "Input string: " << cname << UtIO::endl;
  for (int i=0; i<max_matches; ++i) {
    if (matches[i].rm_so==-1) break;
    UtString one_match;
    bool match_copied = sMatchToString(&one_match, cname, matches, i);
    ASSERT(match_copied);    
    UtIO::cout() << "Match " << i << ": " << one_match << UtIO::endl;
  }
}


void VectorMatchModule::printCandidates(bool filter) const
{
  bool printed_header = false;
  for (CandidateMap::const_iterator iter = mCandidates.begin();
       iter != mCandidates.end();
       ++iter) {
    const VectorCandidate * candidate = (*iter).second;
    if ((not filter) or candidate->getNet()) {
      if (not printed_header) {
	UtIO::cout() << "Vector candidates for module: " << mModule->getName()->str() << UtIO::endl;
	printed_header = true;
      }
      candidate->print();
    }
  }
}


void VectorMatchModule::printPortCandidates() const
{
  bool printed_header = false;
  for (CandidateMap::const_iterator iter = mCandidates.begin();
       iter != mCandidates.end();
       ++iter) {
    const VectorCandidate * candidate = (*iter).second;
    if (candidate->qualify(1) and candidate->isPort()) {
      if (not printed_header) {
	UtIO::cout() << "Vector port candidates for module: " << mModule->getName()->str() << UtIO::endl;
	printed_header = true;
      }
      candidate->print();
    }
  }
}


void VectorCandidate::insert(SInt32 index, NUNet * net) 
{
  mPort |= net->isPort();
  mIndices.insert(value_type(index,net)); 
}


bool VectorCandidate::qualify(SInt32 size_limit) const
{
  SInt32 the_size = size();
  if (the_size <= size_limit) {
    return false;
  }

  std::pair<SInt32,SInt32> bounds = getBounds();  

  SInt32 length = bounds.second - bounds.first + 1;

  if (length != the_size) {
    // we have an incompletely specified vector; make sure we cover at
    // least half the declared range.

    return ( (2*the_size) >= length );
  } else {
    // TBD: check for consistent net flags, etc.
    return true;
  }
}


void VectorCandidate::print() const
{
  UtIO::cout() << "Candidate: " << getName() << " [";
  for (const_iterator iter = begin(); iter != end(); ++iter) {
    SInt32 index = (*iter).first;
    UtIO::cout() << " " << index;
  }
  UtIO::cout() << " ]" << UtIO::endl;
  for (const_iterator iter = begin(); iter != end(); ++iter) {
    const NUNet * net = (*iter).second;
    UtIO::cout() << "    " << net->getName()->str() << UtIO::endl;
  }

  if (getNet()) {
    UtIO::cout() << "Replacement: " << getNet()->getName()->str() << UtIO::endl;
  }
}


void VectorMatchStatistics::print() const
{
  UtIO::cout() << "Vector Match Summary: "
	       << mBits << " single-bit registers were transformed into "
	       << mVectors << " vectors in "
	       << mModules << " modules." << UtIO::endl;
}
