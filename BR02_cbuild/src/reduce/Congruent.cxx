// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
  Implemented Congruence testing for modules.  Any two modules are congruent
  if they differ only in values of constants.  Those constants can then be
  factored out as input ports, and all the module-instances for those modules
  can get the appropriate values passed into the ports.

  There are several disqualifications, which will stop congruence:
        - tasks & functions
        - differences in ConstantRanges in part-selects (LHS & RHS)
        - any directives on a module

  General strategy for Congruence:

    - use NUDesignWalker to render a module and its contents as a linear
      vector of tokens.  The vector is owned by a CongruentModule.  The
      tokens are tagged unions.  NUConst* is a choice in that union,
      but the values of the constants are not considered in the compare
      method for CongruentObject.  The constant values are
      needed later on to compute the port-values for instances once
      the congruent modules are factored out.  
    - A UtHashSet of CongruentModules is kept, relying on the hash & 
      operator== methods defined by CongruentModule.
    - All design modules are kept as CongruentModules, traversing
      the hierarchy bottom-up.
    - NUModuleInstances are compared taking into account congruence that
      has already been computed, so that congruence cascades up the
      hierarchy.
    - A map is kept from NUConst* to port NUNet* to change the 'master' module   
      with replace-leaves
    - a vector is kept with the NUNet*,NUConst* pairs needed to add 
      module connections to each instance
    - an assertion strategy is used so that if new Nucleus objects are
      added to the design walker, that we never "fall back" to the
      major species underneath NUBase*.  I don't think this can be done
      at compile-time with our architecture, but it should be fine to
      do it with an assertion, because you won't get too far in cbuild
      without hitting the assertion.   The only vulnerability is if someone
      adds a new subclass or field to an existing nucleus object, and
      Congruence needs to know about it, there will be no compile-time
      or run-time catch.  I don't know how to fix this right now.
    - I am not filtering on modules that came from the same original
      source via parameter uniquification.  I could do that, but and that
      would be a lot more conservative.  But this actually did find quite
      a bit of replication in our existing test suite.  Maybe this will
      have a general performance benefit.  I haven't checked yet.

Notes:

  There was an attempt to implementi the congruence of a[5:3] against
  a[4:2].  Before I got too far, Matt determined that if we made those
  congruent by turning them into dynamic part-selects we would block
  some vectorization opportunities at Intel, and it would be a net loss.

  That congruence is difficult anyway because 5:3 and 4:2 are two
  ConstantRanges.  There are no NUConsts involved, and the whole infrastructure
  built up here is based on NUConst.  That can be changed, but so far there
  is no motivation.


  We have solved the potential issue illustrated in
  test/congruent/rom*.v, which is that two modules may be congruent,
  but the cost turning the mismatching constants into input ports is
  worse than the cost of having the otherwise congruent modules
  represented separately.  The current solution is to compute use cost
  estimates for the modules, and compare the module cost to the number
  of mismatching constants.  Based on some threshold which was arrived
  at based on a single testcase, we abstain from doing any congruency at all.

  A better approach is to divide the congruence set into sub-groups that
  have acceptable numbers of mismatching constants.  NYI.

  I've noticed another issue which needs to be addressed, which is that the
  token for StmtList needs to record the index of the StmtList's usedef
  node and use that in comparisons.  I haven't come up with a testcase that
  proves we get the wrong answer yet, but it might exist.


Pending review comments from Aron:

1. What will NUTaskLevels do if there are recursive tasks? Just document.

2. What protects the congruency map from being dirtied with task data 
when processing modules and vice-versa? Maybe there should be 
TaskCongruency and ModuleCongruency classes which share common 
infrastructure.

3. In compareNet, you should disqualify the root of the leaf when within 
a task -- there may still be different nets in named declaration scopes. 
Something like:

task t1();
   begin
     begin : foo
       reg bar;
       bar = in;
       out = bar;
     end
   end
endtask

task t2();
   begin
     begin : baz
       reg bar;
       bar = in;
       out = bar;
     end
   end
endtask

4. You're passing parent tasks around various places. Can't you just 
net->isTFNet() and scope->inTFHier()? You could even walk back the scope 
set to get the containing task.

5. Your task references are not reflected in comments -- you still refer 
only to modules in a number of places. One example is within:
  int compare(const CongruentObject& other, bool factorConstants) const

6. Add some asserts at replace time to make sure you're not attempting 
to replace a hierref task or rewrite a hierref task enable.

7. You're not using congruent/flattask.v

8. I would not have put the congruence call in flattenModule. I would 
have put it in lowerModule and flattenAnalyzeModule.


*/

#include "compiler_driver/CarbonContext.h" // for CRYPT
#include "iodb/IODBNucleus.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUInstanceLevels.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTaskLevels.h"
#include "reduce/Congruent.h"
#include "util/UtIOStream.h"
#include "util/UtPair.h"

#define CASE_COND_TAG     1000000
#define CASE_ITEM_TAG     1000001
#define CMODEL_ARG_COND   1000002

// Should fix & use the one from CarbonTypes.h, carbonPtrCompare.  It
// is defined to only take pointers, which is not necessary.
template <typename T>
static int sCompare(T i1, T i2) {
  return (i1 < i2)? -1: (i1 > i2)? 1: 0;
}

//! CongruentNucleus class
/*! This class represents a canonical nucleus object.
 */
class CongruentObject {
public:
  //! ctor -- exactly one of 'mods' and 'stmtList' should be non-null
  CongruentObject(NUModule* mod, NUStmtList* stmtList, NUTask* task,
                  const NUExpr* expr,
                  const Congruent::NucleusObjMap* modModMap,
                  UInt32 index, bool inNetDirective, UInt32 numInstructions):
    mCopyContext(NULL,NULL),
    mNucleusObjMap(modModMap),
    mIndex(index),
    mInNetDirective(inNetDirective),
    mInInitialBlock(false),
    mNumInstructions(numInstructions)
  {
    if (expr != NULL) {
      INFO_ASSERT((mod == NULL) && (stmtList == NULL) && (task == NULL), "mutex");
      mObject.mExpr = expr;
      mType = eExpression;
      mObject.mExpr->compose(&mName, NULL);
      mHasHierRefs = false;
    }
    else if (mod != NULL) {
      INFO_ASSERT((stmtList == NULL) && (task == NULL), "mutex");
      mObject.mModule = mod;
      mType = eModule;
      mName = mod->getName()->str();
      mHasHierRefs = mod->hasHierRefTasks();
    }
    else if (task != NULL) {
      INFO_ASSERT(stmtList == NULL, "mutex");
      mObject.mTask = task;
      mType = eTaskBody;
      mName = task->getName()->str();
      mHasHierRefs = task->hasHierRef();
    }
    else {
      INFO_ASSERT(stmtList != NULL, "mutex");
      mObject.mStmtList = stmtList;
      mType = eStmtList;
      mHasHierRefs = false;

      // stmtLists don't really have names, so just use the index number
      if (stmtList->empty()) {
        mName << "(empty)";
      }
      else {
        NUStmt* stmt = *(stmtList->begin());
        stmt->getLoc().compose(&mName);
      }
    }
    mMaster = this;
  } // CongruentObject

  //! dtore
  ~CongruentObject() {
    // Delete the mapped constants, since the copy is owned by
    // the CongruentObject
    for (size_t i = 0; i < mPortToConstVec.size(); ++i) {
      NUExpr* k = mPortToConstVec[i].second;
      delete k;
    }
  }

  enum Type {eModule, eStmtList, eTaskBody, eExpression};

  bool canCompareNetByPosition(const NUNet* net) {
    return canCompareNetByPosition(net, mType);
  }

  static bool canCompareNetByPosition(const NUNet* net, Type objType) {
    switch (objType) {
    case eModule:
      // For modules, any net that is not visible in waveforms
      // can be compared by its order of appearance, rather than
      // its name.  Non-static nets cannot have waveforms, so their
      // name is not relevant.  Static nets that have temp names
      // are also not visible.
      return net->isTemp() || net->isNonStatic();
    case eTaskBody:
      // For tasks, any net that is declared in the task's scope can
      // be compared by position.  Note that if we ever make a change
      // to allow visibility into task-nets, then we will have to
      // change this.
      return net->isTFNet();
    case eStmtList:
      // For stmt lists, the nets must match exactly
      return false;
    case eExpression:
      // For exprs, the nets can always be compared
      return true;
    }
    INFO_ASSERT(0, "not reached");
    return false;
  }

  typedef UtPair<NUNet*,NUExpr*> NetExprPair;
  typedef UtVector<NetExprPair> PortToConstVec;
  typedef UtHashMap<NUBase*,SInt32> BaseIndexMap;

  //! type to represent a nucleus token, which is a serialization of a nucleus tree
  enum TokenType {eLHSNet, eRHSNet,
                  eLHSTempNet, eRHSTempNet,
                  eInstance, eConst, eInitConst, eExpr,
                  eUseDef, eOp, eExprType, eNumArgs, eConcat, eNext, eInteger, eCModel,
                  eTask, eEdge, eStringReal, eDepth, eSymbol,
                  //eVarselRvalue, ePartselIndexRvalue, eVarselLvalue
                  eRange
  };

  //! Nucleus Token that can easily be compared for congruence
  struct Token {

    Token(NUNet* net, TokenType type): mType(type) {
      mData.net = net;
    }
    Token(NUConst* k, TokenType type): mType(type) {
      mData.constant = k;
    }
    Token(NUExpr* k): mType(eExpr) {
      mData.expr = k;
    }
    Token(SInt32 integer, TokenType type): mType(type) {
      mData.integer = integer;
    }
    Token(NUBase* base, TokenType type): mType(type) {
      mData.nucleus = base;
    }
    Token(NUModuleInstance* inst): mType(eInstance) {
      mData.instance = inst;
    }
    Token(const ConstantRange* range): mType(eRange) {
      mData.range = range;
    }
    Token(NUCModelCall* cmodel): mType(eCModel) {
      mData.cmodel = cmodel;
    }
    Token(NUTask* task): mType(eTask) {
      mData.task = task;
    }
    Token(ClockEdge edge): mType(eEdge) {
      mData.integer = (SInt32) edge;
    }
    Token(StringAtom* sym): mType(eSymbol) {
      mData.symbol = sym;
    }
      
    //! Get the module for an instance taking into account any mappings
    //! we may have discovered thus far.  Note that we don't mutate the
    //! nucleus until after we have examined all the modules, so we have
    //! to do ModuleInstance equivalence taking into account the proposed
    //! mapping.  This approach depends on traversing the module bottom-up
    //! in Congruent::design, below.
    static NUModule* getMod(const NUModuleInstance* a,
                            const Congruent::NucleusObjMap* modModMap)
    {
      NUModule* mod = a->getModule();
      Congruent::NucleusObjMap::const_iterator p = modModMap->find(mod);
      if (p != modModMap->end()) {
        CongruentObject* cm = p->second;
        mod = cm->getMaster()->getModule();
      }
      return mod;
    }

    //! Compare one module instance to another for congruence
    /*! Note that the port-conections of instances don't need to
     *! be explicitly traversed here because they will be reached
     *! by NUDesignWalker.  But let's make sure the module & instance
     *! name are the same
     */
    static int compareInstance(const NUModuleInstance* a,
                               const NUModuleInstance* b,
                               const Congruent::NucleusObjMap* modModMap)
    {
      int cmp = sCompare(getMod(a, modModMap), getMod(b, modModMap));
      if (cmp == 0) {
        cmp = strcmp(a->getName()->str(), b->getName()->str());
      }
      return cmp;
    }

    //! compare two nets from two different modules to see if they are congruent
    /*! For the moment, nets must have the same name to be congruent, which
     *! is the easiest way to ensure that debug information is not lost to
     *! congruency.  We could also allow net names to be different, if we
     *! keep a name-map foreach substituted NUModulesInstance, which could
     *! be used to correct the elaborated symbol table at the back end of
     *! the compilation.  But in that case, we would need to keep some sort
     *! of net correlation index
     */
    static int compareNet(const NUNet* n1, const NUNet* n2,
                          CongruentObject::Type objType)
    {
      int cmp = sCompare(n1->getFlags(), n2->getFlags());
      if (cmp == 0) {
        cmp = compareNetAliases(n1, n2, objType);

        if (cmp == 0) {
          const NUMemoryNet* m1 = n1->getMemoryNet();
          const NUMemoryNet* m2 = n2->getMemoryNet();
          cmp = sCompare(m1 != NULL, m2 != NULL);
          if (cmp == 0) {
            if (m1 != NULL) {
              const ConstantRange* wr1 = m1->getDeclaredWidthRange();
              const ConstantRange* wr2 = m2->getDeclaredWidthRange();
              cmp = wr1->compare(*wr2);
              if (cmp == 0) {
                cmp = m1->getDepthRange()->compare(*m2->getDepthRange());
              }
            }
            else {
              cmp = sCompare(n1->isVectorNet(), n2->isVectorNet());
              if ((cmp == 0) && n1->isVectorNet()) {
                NUVectorNet* vn1 = (NUVectorNet*) n1;
                NUVectorNet* vn2 = (NUVectorNet*) n2;
                const ConstantRange* r1 = vn1->getDeclaredRange();
                const ConstantRange* r2 = vn2->getDeclaredRange();
                cmp = r1->compare(*r2);
              }
            }
          }
        }
      }
      return cmp;
    } // static int compareNetHelper

    // Get all the aliases for a net, sorted by name
    static void getNetNameVector(const NUNet* net, STAliasedLeafNodeVector* v,
                                 CongruentObject::Type objType)
    {
      for (STAliasedLeafNode::AliasLoop p(net->getNameLeaf()); !p.atEnd(); ++p)
      {
        STAliasedLeafNode* alias = *p;
        NUNet* aliasNet = NUNet::findUnelab(alias);

        // Exclude aliases that don't matter
        //
        // When a block-local net doesn't match by name, there are situations
        // when it's still acceptable to declare it congruent.  This is
        // acceptable for block-locals because we will not provide visibility
        // by name to such nets.
        //
        // Also note that in addNet, we have already established the
        // one-to-one matching of nets by order of appearance in
        // whatever Nucleus block we are looking at.
        if(aliasNet != NULL)
        {
          if (!canCompareNetByPosition(aliasNet, objType)) 
            v->push_back(alias);
        }
      }
      std::sort(v->begin(), v->end(), HierNameCmp());
    }

    static int compareNetAliases(const NUNet* n1, const NUNet* n2,
                                 CongruentObject::Type objType)
    {
      STAliasedLeafNodeVector v1, v2;
      getNetNameVector(n1, &v1, objType);
      getNetNameVector(n2, &v2, objType);
      int cmp = sCompare(v1.size(), v2.size());

      for (UInt32 i = 0; (cmp == 0) && (i < v1.size()); ++i) {
        // Cannot use NUNet::compare because it compares the scope ptr
        // and the source locator, which will always be different, even
        // for otherwise congruent nets in different modules.
        cmp = HierName::compare(v1[i], v2[i]);
      } // for
      return cmp;
    } // static int compareNetAliases

    //! compare this token to another in order to determine congruence.
    /*! A return value of 0 means the tokens are considered
     *! congruent.  Constants that differ in value but have the same
     *! size and signedness are considered congruent -- they can be
     *! factored out and the guts of the modules can be shared.
     */
    int compare(const Token& other,
                const Congruent::NucleusObjMap* modModMap,
                bool factorConstants,
                CongruentObject::Type type)
      const
    {
      int cmp = sCompare(mType, other.mType);
      if (cmp == 0) {
        switch (mType) {
        case eConst:
          if (factorConstants) {
            // We don't differentiate between constants because they
            // will be factored out, but we should make
            // sure they are the same type, size, and signedness
            cmp = sCompare(mData.constant->getBitSize(),
                              other.mData.constant->getBitSize());
            if (cmp == 0) {
              cmp = sCompare(mData.constant->isSignedResult(),
                                other.mData.constant->isSignedResult());
            }
            break;
          }

          // fall thru to expression-compare if we are not factoring
          // out constants
        case eInitConst:
          // Constants in initial blocks cannot be factored out as
          // module parameters, because Carbon cannot currently handle
          // non-constant values in an initial block.  Sigh.
          // fall thru
        case eStringReal:
          // bug 4140 -- cbuild cannot handle format strings as parameters,
          // so don't create that situation from otherwise acceptable
          // verilog code
          cmp = NUExpr::compare(mData.constant, other.mData.constant,
                                false, false, true);
          break;
        break;
        case eLHSNet:
        case eRHSNet:
          cmp = compareNet(mData.net, other.mData.net, type);
          break;
        case eInstance:
          cmp = compareInstance(mData.instance, other.mData.instance,
                                modModMap);
          break;
        case eExpr:
          cmp = sCompare(mData.expr->getType(),
                            other.mData.expr->getType());
          if (cmp == 0) {
            cmp = sCompare(mData.expr->getBitSize(),
                              other.mData.expr->getBitSize());
            if (cmp == 0) {
              cmp = sCompare(mData.expr->isSignedResult(),
                                other.mData.expr->isSignedResult());
            }
          }
          break;
        case eOp:
        case eExprType:
        case eNumArgs:
        case eConcat:
        case eUseDef:
        case eInteger:
        case eDepth:
        case eEdge:
        case eLHSTempNet:
        case eRHSTempNet:
          cmp = sCompare(mData.integer, other.mData.integer);
            break;
        case eNext:
          // we really just want to compare the type.  Ignore.
          break;
        case eRange:
          cmp = mData.range->compare(*other.mData.range);
          break;
        case eTask:
          cmp = strcmp(mData.task->getName()->str(),
                       other.mData.task->getName()->str());
          break;
        case eCModel:
          // makes it impossible to converge any modules with cmodel calls
          cmp = sCompare(mData.cmodel, other.mData.cmodel);
          break;
        case eSymbol:
          cmp = sCompare(mData.symbol, other.mData.symbol);
          break;
        }
      } // if
      return cmp;
    } // int compare

    //! print a token for debugging
    void print(const CongruentObject*) const;

    //! is this token a constant?
    bool isConstant() const { return mType == eConst; }

    //! assuming this token is a constant, return it
    NUConst* getConstant() const {
      INFO_ASSERT(mType == eConst,
                  "Congruent::getConstant called on non-constant");
      return mData.constant;
    }

  private:
    TokenType mType;
    union {
      NUNet* net;
      NUBase* nucleus;
      NUModuleInstance* instance;
      SInt32 integer;
      NUConst* constant;
      NUTask* task;
      NUExpr* expr;
      NUCModelCall* cmodel;
      const ConstantRange* range;
      StringAtom* symbol;
    } mData;
    //UInt32 width;               // these must always match
  };
    
  //! This comparator is used only for sorting, so use the index
  bool operator<(const CongruentObject& cm) {
    return mIndex < cm.mIndex;
  }

  //! compare this congruent module to another one, assuming returning 0 if
  //! they can be converged, extracting out mismatching constants and turning
  //! them into input ports.
  int compare(const CongruentObject& other, bool factorConstants) const {
    int cmp = 0;

    if (this != &other) {       // short-circuit compare to self
      INFO_ASSERT(other.mType == mType, "type consistency");

      // If either of these modules is mentioned in a directive
      // then order them by their index, ensuring that they will
      // not be determined equivalent
      if (((mType == eModule) || (mType == eTaskBody)) &&
          (mInNetDirective || other.mInNetDirective ||
           mHasHierRefs || other.mHasHierRefs ||
           ((mType == eModule) &&
            !mObject.mModule->areFlagsCongruent(other.mObject.mModule))))
      {
        cmp = sCompare(mIndex, other.mIndex);
      }
      else {
        cmp = sCompare(mTokenVector.size(), other.mTokenVector.size());
        for (size_t i = 0; (cmp == 0) && (i < mTokenVector.size()); ++i) {
          cmp = compareTokens(i, &other, factorConstants);
        }
      }
    }
    return cmp;
  }

  int compareTokens(UInt32 tokenIndex, const CongruentObject* co,
                    bool factorConstants)
    const
  {
    const Token& tok = mTokenVector[tokenIndex];
    const Token& otherTok = co->mTokenVector[tokenIndex];
    return tok.compare(otherTok, mNucleusObjMap, factorConstants, mType);
  }

  const char* getName() const {return mName.c_str();}
  NUModule* getModule() const {return mObject.mModule;}
  NUTask* getTask() const {return mObject.mTask;}
  NUStmtList* getStmtList() const {return mObject.mStmtList;}
  const NUExpr* getExpr() const {return mObject.mExpr;}

  void addNetRHS(NUNet* net) {
    mHasHierRefs = mHasHierRefs || net->isHierRef() || net->hasHierRef();
    mTokenVector.push_back(Token(net, eRHSNet));
  }

  void addNetLHS(NUNet* net) {
    mHasHierRefs = mHasHierRefs || net->isHierRef() || net->hasHierRef();
    mTokenVector.push_back(Token(net, eLHSNet));
  }

  void addModuleInstance(NUModuleInstance* inst) {
    mTokenVector.push_back(Token(inst));
  }

  void addConst(NUConst* k) {
    if (mInInitialBlock) {
      mTokenVector.push_back(Token(k, eInitConst));
    }
    else if (k->isDefinedByString() || k->isReal()) {
      mTokenVector.push_back(Token(k, eStringReal));
    }
    else {
      mTokenVector.push_back(Token(k, eConst));
    }
  }

  void addExpr(NUExpr* expr) {
    mTokenVector.push_back(Token(expr));
  }

  void addRange(const ConstantRange* range) {
    mTokenVector.push_back(Token(range));
  }

  void addTask(NUTask* task) {
    mHasHierRefs = mHasHierRefs || task->hasHierRef();
    mTokenVector.push_back(Token(task));
  }

  void addEdge(ClockEdge edge) {
    mTokenVector.push_back(Token(edge));
  }

  void addName(StringAtom* sym) {
    mTokenVector.push_back(Token(sym));
  }

  void addCModel(NUCModelCall* ccall) {
    mTokenVector.push_back(Token(ccall));
  }

  void addNumArgs(SInt32 numArgs) {
    mTokenVector.push_back(Token(numArgs, eNumArgs));
  }

  void addUseDef(NUType type) {
    mTokenVector.push_back(Token((SInt32) type, eUseDef));
  }

  void addIntegerToken(SInt32 code, TokenType type) {
    mTokenVector.push_back(Token(code, type));
  }

  // Adding a token associated with a nucleus object.  We can't compare
  // the nucleus object by pointer to one in a congruent module, but
  // we should encounter arbitrary nucleus objects in the same order.
  // This is particularly aimed at StmtList, where we are trying to
  // sure that the statemnt-list we are about to traverse into is
  // associated with the appropriate container.
  void addNucleusObject(NUBase* base, TokenType type, UInt32 nucleusType) {
    BaseIndexMap::iterator p = mBaseIndexMap.find(base);
    SInt32 index = 0;
    if (p == mBaseIndexMap.end()) {
      index = mBaseIndexMap.size();
      mBaseIndexMap[base] = index;
#ifdef CDB
      mIndexBaseMap.push_back(base);
#endif
    }
    else {
      index = p->second;
    }
    mTokenVector.push_back(Token(index, type));
    mTokenVector.push_back(Token(nucleusType, eInteger));
  }

  void addDeclScope(NUNamedDeclarationScope* declScope, SInt32 depth) {
    StringAtom* dsName = declScope->getName();
    mTokenVector.push_back(Token(dsName));
    mTokenVector.push_back(Token(depth, eDepth));
  }

  typedef UtVector<size_t> IntVec;
  typedef UtHashSet<size_t> IntSet;

  //! Remove an element from a vector by moving the last element.
  //! Warning: does maintain order
  static void vec_remove_elt(IntVec* v, size_t index) {
    size_t newSize = v->size() - 1;
    if (index != newSize) {
      (*v)[index] = (*v)[newSize];
    }
    v->resize(newSize);
  }

  struct Xlate: public NuToNuFn {
    Xlate(const NUExprNetMap& constToPort)
      : mConstToPortMap(constToPort),
        mCopyContext(NULL, NULL)
    {
    }

    virtual NUExpr * operator()(NUExpr* expr, Phase phase)
    {
      NUExpr* ret = NULL;
      if (!isPre(phase))
      {
        NUExprNetMap::const_iterator p = mConstToPortMap.find(expr);
        if (p != mConstToPortMap.end()) {
          NUNet* port = p->second;
          ret = new NUIdentRvalue(port, expr->getLoc());
          ret->resize(port->getBitSize());
          delete expr;
        }
      }
      return ret;
    }

    const NUExprNetMap& mConstToPortMap;
    CopyContext mCopyContext;
  };

  //! Determine which constants differ between a group of otherwise congruent models
  /*! This routine is called twice.  The first time is once to test how
   *! many constants need to be factored out.  After that the equivalent-sets
   *! may be adjusted to minimize excessive factoring.  The second time,
   *! mutate==true and the mutation is performed on the constants and the
   *! module port-lists
   */
  UInt32 factorConstants(bool mutate, Congruent::ObjVec* equivMods) {
    if (equivMods == NULL) {
      equivMods = &mEquivMods;
    }
    if (equivMods->empty()) {
      return 0;
    }
    IntVec convertConstants;
    IntSet prunedConstants;

    if (mType != eModule) {
      // Cannot mutate statement lists or tasks at this time
      mutate = false;
    }

    bool checkNonConsts = mutate;
    findNonMatchingConstants(&convertConstants, equivMods, checkNonConsts);

    UInt32 numDistinctConstants = 0;

    for (size_t i = 0; i < convertConstants.size(); ++i) {
      IntVec commonConstants;
      size_t constIndex = convertConstants[i];

      // If this constant was consistently used across every
      // module in the equivalence group, and has already been
      // covered with a port-addition, we don't need to add another
      // port for it
      if (prunedConstants.find(constIndex) != prunedConstants.end()) {
        continue;
      }

      ++numDistinctConstants;

      NUConst* k = mTokenVector[constIndex].getConstant();
      collectMatchingConstants(k, convertConstants, i, &commonConstants);

      // Now see if those constants are also the same in all the other modules
      for (size_t j = 0; j < equivMods->size(); ++j) {
        (*equivMods)[j]->removeMismatchingConstants(convertConstants, i,
                                                    &commonConstants);
      }

      // consistent matching constants can be removed from convertContants
      // array as they will share a port
      for (size_t j = 0; j < commonConstants.size(); ++j) {
        size_t constIndex = commonConstants[j];
        prunedConstants.insert(constIndex);
      }

      // For uniformity, add the first occurence of this constant to
      // commonConstants.  This simplifies the call to updateConstToPortMap
      commonConstants.push_back(convertConstants[i]);

      if (mutate) {
        // Add the port to this module and set up the mapping function for
        // the eventual nucleus walk to change NUConst* refs to NUNet* refs
        // to get the value that will now arrive via input port
        NUNet* port = addPort(k);
        updateConstToPortMap(commonConstants, port);

        // Collect the port<->constant-value map for each version of the
        // module, including this one
        collectPortToConstVec(port, constIndex);
        for (size_t j = 0; j < equivMods->size(); ++j) {
          (*equivMods)[j]->collectPortToConstVec(port, constIndex);
        }
      }
    }

    // Now that we've figured out the const-to-portnet map, do the
    // substitution with replaceLeaves.
    if (mutate) {
      Xlate xlate(mConstToPortMap);
      mObject.mModule->replaceLeaves(xlate);
    }

    return numDistinctConstants;
  }

  //! Find the non-matching constants in a congruence group
  void findNonMatchingConstants(IntVec* convertConstants,
                                Congruent::ObjVec* equivMods,
                                bool checkNonConsts)
  {
    size_t numMods = equivMods->size();
    NU_ASSERT(numMods > 0, mObject.mModule);

    // Find the differences in constants between all the modules in vec,
    // and store their token-vector indices in convertConstants
    for (size_t i = 0; i < mTokenVector.size(); ++i) {
      const Token& tok = mTokenVector[i];
      bool isConst = tok.isConstant();
      NUConst* k = isConst? tok.getConstant(): 0;
      bool anyDifferent = false;

      for (size_t j = 0; j < numMods; ++j) {
        CongruentObject* cm = (*equivMods)[j];
        const Token& modTok = cm->mTokenVector[i];

        if (isConst) {
          if (!anyDifferent) {
            NUConst* modK = modTok.getConstant();
            if (NUExpr::compare(k, modK, false, false, true) != 0) {
              anyDifferent = true;
              convertConstants->push_back(i);
            }
          }
        }
        else if (checkNonConsts) {
          // If it's not constant, it has to match
          NU_ASSERT(compareTokens(i, cm, false) == 0, cm->mObject.mModule);
        }
      }
    } // for
  } // void findNonMatchingConstants

  void collectMatchingConstants(NUConst* k, const IntVec& convertConstants,
                                size_t index, IntVec* commonConstants)
  {
    // Now we have the indices of all the NUConst* objects found by the
    // design walker which need to be converted.  We will want to
    // coalesce some of these, e.g. if the parameter X is used 100
    // times in a module, then we only need to add one port for these.
    //
    // Find all the other differentiated constants that are the same
    // as this one in this module
    //
    // This routine is optimistic because it looks for redundancy in
    // the constants that we need to convert within must one of
    // the congruent modules.  This gets us a starting set.
    // removeMismatchingConstants removes the optimism by examining
    // the rest of the modules
    for (size_t c = index + 1; c < convertConstants.size(); ++c) {
      size_t ccIndex = convertConstants[c];
      NUConst* otherK = mTokenVector[ccIndex].getConstant();
      if (NUExpr::compare(k, otherK, false, false, true) == 0) {
        commonConstants->push_back(ccIndex);
      }
    }
  }

  void removeMismatchingConstants(const IntVec& convertConstants,
                                  size_t index, IntVec* commonConstants)
    const
  {
    // Now we have the indices of all the NUConst* objects found by the
    // design walker which need to be converted.  We will want to
    // coalesce some of these, e.g. if the parameter X is used 100
    // times in a module, then we only need to add one port for these.
    size_t constIndex = convertConstants[index];
    NUConst* k = mTokenVector[constIndex].getConstant();

    // Find all the other constants that are the same as this one in this module
    for (size_t c = 0; c < commonConstants->size(); ) {
      size_t ccIndex = (*commonConstants)[c];
      NUConst* otherK = mTokenVector[ccIndex].getConstant();
      if (NUExpr::compare(k, otherK, false, false, true) != 0) {
        vec_remove_elt(commonConstants, c);
      }
      else {
        ++c;
      }
    }
  }

  //! add a port to the current module and return it
  NUNet* addPort(NUConst* k) {
    StringAtom* name = mObject.mModule->gensym("param");
    ConstantRange* range = new ConstantRange(k->getBitSize() - 1, 0);
    NetFlags flags = NetFlags(eInputNet | eAllocatedNet);
    if (k->isSignedResult ())
      flags = NetFlags (flags | eSigned); // Replacing signed integers by signed net..

    NUNet* net = new NUVectorNet(name, range, flags, VectorNetFlags (0),
                                 mObject.mModule, k->getLoc());
    mObject.mModule->addPort(net);
    return net;
  }

  // set up the mapping function for the eventual nucleus walk to
  // change NUConst* refs to NUNet* refs to get the value that will
  // now arrive via input port
  void updateConstToPortMap(const IntVec& commonConstants, NUNet* port)
  {
    for (size_t i = 0; i < commonConstants.size(); ++i) {
      size_t constIndex = commonConstants[i];
      NUConst* k = mTokenVector[constIndex].getConstant();
      mConstToPortMap[k] = port;
    }
  }


  // Collect the port<->constant-value map for each version of the
  // module, including this one
  void collectPortToConstVec(NUNet* port, size_t constIndex)
  {
    NUConst* k = mTokenVector[constIndex].getConstant();
    mPortToConstVec.push_back(NetExprPair(port, k->copy(mCopyContext)));
  }

  void addEquiv(CongruentObject* cm) {
    cm->mMaster = this;
    mEquivMods.push_back(cm);
  }

  CongruentObject* getMaster() const {return mMaster;}

  // Replace any instances in this module, based on congruence that was found
  // in lower-level modules
  bool replaceModInstances() {
    bool workDone = false;
    for (NUModuleInstanceMultiLoop q = mObject.mModule->loopInstances();
         !q.atEnd(); ++q)
    {
      NUModuleInstance* inst = *q;
      NUModule* mod = inst->getModule();
      Congruent::NucleusObjMap::const_iterator p = mNucleusObjMap->find(mod);

      // By the time this runs, every module should have a CongruentObject
      NU_ASSERT(p != mNucleusObjMap->end(), inst);
      CongruentObject* cm = p->second;
      NU_ASSERT(cm->getModule() == mod, inst);
      NUModule* replacement = cm->getMaster()->getModule();

      if (mod != replacement) {
        // This is not the master, replace it
        inst->putModule(replacement);
        workDone = true;
      }

      // For every port that we've mapped to a constant for the target
      // module, pass in the correct value
      for (size_t i = 0; i < cm->mPortToConstVec.size(); ++i) {
        NUNet* port = cm->mPortToConstVec[i].first;
        NUExpr* k = cm->mPortToConstVec[i].second;
        k = k->copy(mCopyContext);
        const SourceLocator& loc = k->getLoc();
        NUPortConnection* c = new NUPortConnectionInput(k, port, loc);
        inst->addConnection(c);
        c->setModuleInstance(inst);
        workDone = true;
      }
    }

    // take care of all of a master's equivalences
    if (mMaster == this) {
      for (UInt32 i = 0, n = mEquivMods.size(); i < n; ++i) {
        CongruentObject* equiv = mEquivMods[i];
        workDone |= equiv->replaceModInstances();
      }
    }
    return workDone;
  } // bool replaceModInstances

  class TaskReplacementCallback : public NUDesignCallback {
  public:
    //! constructor.
    TaskReplacementCallback(const Congruent::NucleusObjMap* nucleusObjMap,
                            bool verbose, UtOStream* infoFile)
      : mNucleusObjMap(nucleusObjMap),
        mWorkDone(false),
        mVerbose(verbose),
        mInfoFile(infoFile)
    {}

    //! destructor
    ~TaskReplacementCallback() {}

    Status operator()(Phase phase, NUTaskEnable* taskEnable) {
      if ((phase == ePre) && !taskEnable->isHierRef()) {
        NUTask* task = taskEnable->getTask();
        Congruent::NucleusObjMap::const_iterator p =
          mNucleusObjMap->find(task);
        if (p != mNucleusObjMap->end()) {
          CongruentObject* rep = p->second;
          NU_ASSERT(rep->getType() == eTaskBody, taskEnable);
          if (rep != rep->mMaster) {
            NUTask* newTask = rep->mMaster->getTask();
            NU_ASSERT(newTask != task, task);
            CopyContext cc(NULL, NULL);
            taskEnable->replaceTask(newTask, cc);
            if (mVerbose) {
              rep->print(UtIO::cout());
            }
            if (mInfoFile != NULL) {
              rep->print(*mInfoFile);
            }
            mWorkDone = true;
          }
        }
      }
      return eSkip;
    }
    Status operator()(Phase, NUModuleInstance*) {
      return eSkip;
    }
    Status operator()(Phase, NUBase*) {
      return eNormal;
    }

    bool workDone() const {return mWorkDone;}
  private:
    const Congruent::NucleusObjMap* mNucleusObjMap;
    bool mWorkDone;
    bool mVerbose;
    UtOStream* mInfoFile;
  }; // class TaskReplacementCallback : public NUDesignCallback

  bool replaceTaskInstances(bool verbose, UtOStream* infoFile) {
    TaskReplacementCallback trc(mNucleusObjMap, verbose, infoFile);
    NUDesignWalker walker(trc, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.task(mObject.mTask);
    return trc.workDone();
  }

  void print(UtOStream&) const;
  void printTokens() const;
  void computeTokenStream();

  void putInInitialBlock(bool flag) {
    mInInitialBlock = flag;
  }

  Congruent::ObjVec mEquivMods;
  CongruentObject* mMaster;

  //! How many instructions does this module cost out to?
  UInt32 getNumInstructions() const {return mNumInstructions;}

  //! Disqualify an object from congruence because it's got hier refs
  void putHasHierRefs(bool flag) {mHasHierRefs = flag;}

  //! Get the type of this object
  Type getType() const {return mType;}

  friend struct Token;          // needed for gcc 2.95.3

private:
  typedef UtVector<Token> TokenVector;
  TokenVector mTokenVector;

  // Exactly 1 of these three will be non-null
  union {
    NUModule* mModule;
    NUStmtList* mStmtList;
    NUTask* mTask;
    const NUExpr* mExpr;
  } mObject;
  Type mType;

  NUExprNetMap mConstToPortMap;
  PortToConstVec mPortToConstVec;
  CopyContext mCopyContext;
  const Congruent::NucleusObjMap* mNucleusObjMap;
  UInt32 mIndex;
  bool mInNetDirective;
  bool mHasHierRefs;
  bool mInInitialBlock;
  UInt32 mNumInstructions;
  BaseIndexMap mBaseIndexMap;
#ifdef CDB
  UtArray<NUBase*> mIndexBaseMap;
#endif
  UtString mName;
}; // class CongruentObject

void CongruentObject::Token::print(const CongruentObject*
#ifdef CDB
                                   obj
#endif
  ) const
{
  UtString buf;
  switch (mType) {
  case eConst:
  case eInitConst:
  case eStringReal:
    mData.constant->compose(&buf, NULL);
    UtIO::cout() << "  NUConst " << buf;
    break;
  case eLHSNet:
  case eRHSNet:
    mData.net->compose(&buf, NULL);
    UtIO::cout() << "  NUNet " << buf;
    break;
  case eInstance:
    mData.instance->compose(&buf, NULL, 0, true);
    UtIO::cout() << "  NUModuleInstance " << buf;
    break;
  case eExpr: {
    UtString buf;
    mData.expr->compose(&buf, NULL);
    UtIO::cout() << "  Expr " << buf;
    break;
  }
  case eOp:
    UtIO::cout() << "  Op " << mData.integer;
    break;
  case eExprType:
    UtIO::cout() << "  ExprType " << mData.integer;
    break;
  case eNumArgs:
    UtIO::cout() << "  NumArgs " << mData.integer;
    break;
  case eUseDef:
    UtIO::cout() << "  UseDef " << mData.integer;
    break;
  case eDepth:
    UtIO::cout() << "  Depth " << mData.integer;
    break;
  case eEdge:
    UtIO::cout() << "  Edge " << mData.integer;
    break;
  case eInteger:
    UtIO::cout() << "  Integer " << mData.integer;
    break;
  case eNext:
    UtIO::cout() << "  Next 0x" << ((UIntPtr) mData.nucleus);
    break;
  case eLHSTempNet:
  case eRHSTempNet: {
    UtString buf;
#ifdef CDB
    NUBase* base = obj->mIndexBaseMap[mData.integer];
    NUNet* net = dynamic_cast<NUNet*>(base);
    NU_ASSERT(net, base);
    net->compose(&buf);
#else
    buf << "Nucleus Index " << mData.integer;
#endif
    if (mType == eLHSTempNet) {
      UtIO::cout() << "  Temp LHS Net " << buf;
    }
    else {
      UtIO::cout() << "  Temp RHS Net " << buf;
    }
    break;
  }
  case eRange:
    UtIO::cout() << "  Range [" << mData.range->getMsb()
                 << ":" << mData.range->getLsb();
    break;
  case eTask:
    UtIO::cout() << "  task " << mData.task->getName()->str();
    break;
  case eCModel:
    UtIO::cout() << "  cmodel " << mData.cmodel->getName()->str();
    break;
  case eSymbol:
    UtIO::cout() << "  symbol " << mData.symbol->str();
    break;
  case eConcat:
    UtIO::cout() << "  concat " << mData.integer;
    break;
  }
  UtIO::cout() << UtIO::endl;
}

void CongruentObject::printTokens() const {
  UtIO::cout() << getName() << "\n";
  for (size_t i = 0; i < mTokenVector.size(); ++i) {
    const Token& tok = mTokenVector[i];
    tok.print(this);
  }
}

void CongruentObject::print(UtOStream& out) const {
  const char* typeStr = NULL;
  switch (mType) {
  case eModule: typeStr = ""; break;
  case eStmtList: typeStr = "StmtList "; break;
  case eTaskBody: typeStr = "Task "; break;
  case eExpression: typeStr = "Expr "; break;
  }

  if (mMaster == this) {
    if (mEquivMods.empty()) {
      return;           // don't print anything for modules with no congruence
    }
    out << "Master " << typeStr << getName();
    out << " instruction-cost: " << mNumInstructions;
  }
  else {
    out << "Congruent " << typeStr << getName() << " (master = "
        << mMaster->getName() << ")";
  }
  if (mType == eModule) {
    NUModule* mod = mObject.mModule;
    if (mod->getOriginalName() != mod->getName()) {
      out << " (originally " << mod->getOriginalName()->str() << ")";
    }

    const SourceLocator& loc = mod->getLoc();
    UtString buf;
    loc.compose(&buf);
    out << " " << buf << "\n";
    for (size_t i = 0; i < mPortToConstVec.size(); ++i) {
      NUNet* port = mPortToConstVec[i].first;
      NUExpr* k = mPortToConstVec[i].second;
      buf.clear();
      k->compose(&buf, NULL);
      out << "    " << port->getName()->str() << "\t" << buf
          << "\tsize " << k->getBitSize() << "\n";
    }
  }
  else if (mType == eTaskBody) {
    const SourceLocator& loc = mObject.mTask->getLoc();
    UtString buf;
    loc.compose(&buf);
    out << " " << buf;
  }

  out << "\n";
  for (size_t i = 0; i < mEquivMods.size(); ++i) {
    mEquivMods[i]->print(out);
  }
} // void CongruentObject::print

//! Callback class for walker to record a unique stream of
//! nucleus objects and other tokens, for easy diffing with
//! another such stream.  For the moment we will only but
//! objects derived from NUBase in the stream.
class CongruentCallback : public NUDesignCallback
{
public:
  //! constructor.
  /*!
    \param module_recurse  If false, will only visit 1 module
   */
  CongruentCallback(CongruentObject* cm, NUTask* currentTask = NULL):
    mDepth(0),
    mOut(UtIO::cout()),
    mCongruentObject(cm),
    mModule(NULL),
    mCurrentTask(currentTask)
  {
    mHasNesting = false;
  }

  //! destructor
  ~CongruentCallback() {}

  Status operator()(Phase phase, NUIdentRvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent(), CongruentObject::eRHSTempNet);
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUVarselRvalue* id) {
    if (phase == ePre) {
      mCongruentObject->addRange(id->getRange());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUIdentLvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent(), CongruentObject::eLHSTempNet);
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUVarselLvalue* id) {
    if (phase == ePre) {
      mCongruentObject->addRange(id->getRange());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUModuleInstance* inst) {
    if (phase == ePre) {
      mCongruentObject->addModuleInstance(inst);
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUModule* mod) {
    if (phase == ePre) {
      if (mModule == NULL) {
        mModule = mod;
      }
      else {
        return eSkip;           // do not recurse into modules
      }
    }
    else {
      mModule = NULL;
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUConst* k) {
    if (phase == ePre) {
      mCongruentObject->addConst(k);
    }
    return eNormal;
  }

  Status stmtList(Phase phase, NUBase* base) {
    if (phase == ePre) {
      UInt32 typeTag = 0;
      NUUseDefNode* ud = dynamic_cast<NUUseDefNode*>(base);
      if (ud != NULL) {
        typeTag = (UInt32) ud->getType();
      }
      mCongruentObject->addNucleusObject(base, CongruentObject::eNext,
                                         typeTag);
    }
    return eNormal;
  }

  // not called at this point
  Status constantRange(Phase phase, NUBase*, const ConstantRange& range) {
    if (phase == ePre) {
      mCongruentObject->addRange(&range);
    }
    return eNormal;
  }

  // Expressions & use-defs are marked for their type
  Status operator()(Phase phase, NUExpr* expr) {
    // pre & post, keep track of the object ID
    mCongruentObject->addNucleusObject(expr, CongruentObject::eExprType,
                                       expr->getType());

    if (phase == ePre) {
      mCongruentObject->addExpr(expr);
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUOp* op) {
    // pre & post, keep track of the object ID
    mCongruentObject->addNucleusObject(op, CongruentObject::eOp,
                                       op->getOp());

    // Pre-only, make sure operators are same size, sign, concats are
    // same repeat-count
    if (phase == ePre) {
      mCongruentObject->addNumArgs(op->getNumArgs());
      mCongruentObject->addExpr(op);
      NUConcatOp* concat = dynamic_cast<NUConcatOp*>(op);
      if (concat != NULL) {
        mCongruentObject->addIntegerToken(concat->getRepeatCount(),
                                          CongruentObject::eConcat);
      }
    }
    return eNormal;
  }
  Status operator()(Phase, NUStmt* stmt) {
    // Do this both pre & post
    mCongruentObject->addNucleusObject(stmt, CongruentObject::eNext,
                                       stmt->getType());
    return eNormal;
  }

  Status operator()(Phase phase, NUReadmemX* readmem) {
    if (phase == ePre) {
      mCongruentObject->addName(readmem->getFileName());
      UInt64 startAddr = readmem->getStartAddress();
      UInt64 endAddr = readmem->getEndAddress();
      mCongruentObject->addIntegerToken(startAddr & 0xFFFFFFFF,
                                        CongruentObject::eInteger);
      mCongruentObject->addIntegerToken(startAddr >> 32,
                                        CongruentObject::eInteger);
      mCongruentObject->addIntegerToken(endAddr & 0xFFFFFFFF,
                                        CongruentObject::eInteger);
      mCongruentObject->addIntegerToken(endAddr >> 32,
                                        CongruentObject::eInteger);
      mCongruentObject->addIntegerToken(readmem->getHexFormat(),
                                        CongruentObject::eInteger);
      mCongruentObject->addIntegerToken(readmem->getEndSpecified(),
                                        CongruentObject::eInteger);
    }
    return eNormal;
  }



  Status operator()(Phase phase, NUInitialBlock* ib) {
    if (phase == ePre) {
      mCongruentObject->addUseDef(ib->getType());
      mCongruentObject->putInInitialBlock(true);
    }
    else {
      mCongruentObject->putInInitialBlock(false);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUCaseCondition*) {
    if (phase == ePre) {
      mCongruentObject->addIntegerToken(CASE_COND_TAG, CongruentObject::eInteger);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUCaseItem*) {
    if (phase == ePre) {
      mCongruentObject->addIntegerToken(CASE_ITEM_TAG, CongruentObject::eInteger);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUCModelArgConnection*) {
    if (phase == ePre) {
      mCongruentObject->addIntegerToken(CMODEL_ARG_COND, CongruentObject::eInteger);
    }
    return eNormal;
  }

  // CModel-calls prevent congruence, because the c-models somehow have
  // access to the original parameters.  Rather than do something out-of-band
  // like we did for hier-refs. just recording the cmodel call by pointers
  // and diffing that in the compare routine is enough
  Status operator()(Phase phase, NUCModelCall* cmc) {
    if (phase == ePre) {
      mCongruentObject->addCModel(cmc);
    }
    return eNormal;
  }

  Status operator()(Phase /*phase*/, NUCModelPort*) {
    return eSkip;
  }

  Status operator()(Phase /*phase*/, NUCModel*) {
    return eSkip;
  }

  Status operator()(Phase /*phase*/, NUCModelInterface*) {
    return eSkip;
  }

  Status operator()(Phase /*phase*/, NUCModelFn*) {
    return eSkip;
  }


  Status operator()(Phase phase, NUTaskEnable* te) {
    if (phase == ePre) {
      if (te->isHierRef()) {
        mCongruentObject->putHasHierRefs(true); // disqualify it from congruence
      }
      else {
        mCongruentObject->addTask(te->getTask());
      }
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUEdgeExpr* edge) {
    if (phase == ePre) {
      mCongruentObject->addEdge(edge->getEdge());
    }
    return eNormal;
  }

  // Put in explicit assertions for nucleus species reached by the NUDesignWalker
  // that we have not explicitly handled
  Status operator()(Phase, NUBase* base) {
    NU_ASSERT("base not handled" == NULL, base);
    return eNormal;
  }

  // Handle use-def nodes by default, but let's consider carefully
  // which ones we need to do something special with, by putting
  // all the ones we can handle generically specifically in the
  // switch
  Status operator()(Phase, NUUseDefNode* ud) {
    NUType type = ud->getType();
    switch (type) {
    case eNUMemselLvalue:
    case eNUVarselLvalue:
    case eNUConcatLvalue:
    case eNUAlwaysBlock:
    case eNUPortConnectionInput:
    case eNUPortConnectionOutput:
    case eNUPortConnectionBid:
    case eNUTFArgConnectionInput:
    case eNUTFArgConnectionOutput:
    case eNUTFArgConnectionBid:
    case eNUContEnabledDriver:
    case eNUBlockingEnabledDriver:
      // Do this both pre & post
      mCongruentObject->addNucleusObject(ud, CongruentObject::eNext, type);
      break;
    default:
      NU_ASSERT("usedef not handled" == NULL, ud);
      return eNormal;
    }
    return eNormal;
  }

  Status operator()(Phase, NUUseNode* use) {
    NU_ASSERT("use not handled" == NULL, use);
    return eNormal;
  }

  Status operator()(Phase phase, NUTask* task) {
    if (phase == ePre) {
      if (task == mCurrentTask) {
        // Visit all the task formals to avoid false congruence in
        // test/congruent/congfalsefunc.v
        for (NUNetVectorLoop p = task->loopArgs(); !p.atEnd(); ++p) {
          NUNet* net = *p;
          if (net->isInput()) {
            addNet(net, CongruentObject::eRHSTempNet);
          }
          else {
            addNet(net, CongruentObject::eLHSTempNet);
          }
        }
      }
      else {
        mCongruentObject->addTask(task);
      }
    }
    return eNormal;
  }

  Status operator()(Phase, NUDesign* design) {
    NU_ASSERT("design not handled" == NULL, design);
    return eNormal;
  }

  void addNet(NUNet* net, CongruentObject::TokenType type) {
    // In modules and tasks, temp nets don't have to have the
    // same names.  compareNets knows this and will not compare
    // the names.  But use the nucleus object index map to
    // make sure we encounter the temps in the same order
    if (mCongruentObject->canCompareNetByPosition(net)) {
      mCongruentObject->addNucleusObject(net, type, net->getFlags());
    }

    if (type == CongruentObject::eRHSTempNet) {
      mCongruentObject->addNetRHS(net);
    }
    else {
      mCongruentObject->addNetLHS(net);
    }
  }

  Status operator()(Phase phase, NUNet* net) {
    if (phase == ePre) {
      addNet(net, CongruentObject::eRHSTempNet);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUNamedDeclarationScope* ds) {
    if (mCongruentObject->getType() != CongruentObject::eTaskBody) {
      if (phase == ePre) {
        mCongruentObject->addDeclScope(ds, mDepth);
        ++mDepth;
      }
      else {
        --mDepth;
      }
    }
    return eNormal;
  }

#if 0
  Status operator()(Phase phase, NUBase* base) {
    if (phase == ePre) {
      if (mLastPhase == ePost) {
        // We are now at the boundary of (e.g.) then-list vs else-list
        if (mHasNesting) {
          //mStream.push_back(NULL);
          mOut << "----\n";
        }
        mHasNesting = false;
      }
      else {
        mHasNesting = true;
      }
      mOut << mDepth << " ";
      ++mDepth;
      mOut << base->typeStr() << " " << (void*) base << UtIO::endl;
      //mStream.push_back(base);
    }
    else {
      --mDepth;
      mHasNesting = false;
    }
    mLastPhase = phase;
    return eNormal;
  }
#endif

private:
  //! Hide copy and assign constructors.
  CongruentCallback(const CongruentCallback&);
  CongruentCallback& operator=(const CongruentCallback&);

  SInt32 mDepth;
  bool mHasNesting;
#if 0
  Phase mLastPhase;
#endif
  UtOStream& mOut;
  CongruentObject* mCongruentObject;
  NUModule* mModule;
  NUTask* mCurrentTask;
};


void CongruentObject::computeTokenStream() {
  mTokenVector.clear();
  NUTask* currentTask = (mType == eTaskBody)? mObject.mTask: 0;
  CongruentCallback congruentCallback(this, currentTask);
  NUDesignWalker congruentWalker(congruentCallback, false);
  if (mType != eTaskBody) {
    // We don't care about declaration scopes in tasks when comparing
    // tasks for congruence, since none of those nets will be visible by
    // name anyway
    congruentWalker.putWalkDeclaredNets(true);
  }
  congruentWalker.putWalkTasksFromTaskEnables(false);
  switch (mType) {
  case eModule:
    congruentWalker.module(mObject.mModule);
    break;
  case eTaskBody:
    congruentWalker.task(mObject.mTask);
    break;
  case eExpression:
    congruentWalker.expr(const_cast<NUExpr*>(mObject.mExpr));
    break;
  case eStmtList:
    for (NUStmtList::iterator p = mObject.mStmtList->begin(),
           e = mObject.mStmtList->end(); p != e; ++p)
    {
      NUStmt* stmt = *p;
      congruentWalker.stmt(stmt);
    }
    break;
  }
} // void CongruentObject::computeTokenStream

#if 0
// How many ways to choose n items out of total;
static UInt32 sChoose(UInt32 total, UInt32 n) {
  // Do this with 64-bit arithmetic & make sure we don't overflow
  UInt64 numer = 1;
  UInt32 denom = 1;

  // 6 choose 4 == 6 choose 2, but the latter is easier to compute
  UInt32 total_minus_n = total - n;
  if (total_minus_n < n) {
    n = total_minus_n;
  }

  for (UInt32 i = 1; i <= n; ++i) {
    numer *= (total - i + 1);
    denom *= i;
  }

  UInt64 choose = numer / denom;
  INFO_ASSERT(denom * choose == numer, "sChoose arithmetic error");
  INFO_ASSERT(choose <= 0xffffffff, "sChoose arithmetic error");
  return (UInt32) choose;
} // static UInt32 sChoose
#endif

// Let's say I have 4 modules that are all congruent, given some integer
// factoring.  For every given pair of that 4, too many constants
// may need to be moved into ports to justify the i-stream savings.
//
// Say these modules have single assignments:
//
//    m1:  assign out = (in1 == 0) && (in2 == 1) && (in3 == 2) && (in3 == 3)
//    m2:  assign out = (in1 == 0) && (in2 == 1) && (in3 == 4) && (in3 == 5)
//    m3:  assign out = (in1 == 2) && (in2 == 3) && (in3 == 6) && (in3 == 7)
//    m4:  assign out = (in1 == 2) && (in2 == 3) && (in3 == 8) && (in3 == 9)
//
// In this case, grouping (m1,m2),(m3,m4) means each group only needs to
// have two constants factored out.  If we group (m1,m2,m3,m4) together,
// then all 4 constants must be factored out.  Depending on the module
// cost, it may be wortwhile doing the congruenced for 2 constants but
// not 4.
//
// This routine figures out, based on some DRE heuristics, a proposed
// grouping.
void Congruent::findBestCongruenceGroups(ObjVec* newCanonicalMods,
                                         CongruentObject* cm)
{
  UInt32 nmods = cm->mEquivMods.size() + 1;

  // Don't bother if this module has no equivalents
  if (nmods == 1) {
    newCanonicalMods->push_back(cm); // still the master
  }
  else {
    // Get the number of constants we'd have to create for this
    // congruence group.  If it's small enough, relative to the module
    // cost, then we can let the maximal congruence-group stand
    UInt32 numConstants = cm->factorConstants(false, NULL);

    // If there are no constants that need to be factored out, then
    // we can let the whole group stand without even computing its cost
    if (numConstants == 0) {
      newCanonicalMods->push_back(cm); // still the master
    }
    else {
      UInt32 numInstructions = cm->getNumInstructions();
      if (mParamCostFactor*numConstants >= numInstructions) {
        //optimizeCongruenceGroups(newCanonicalMods, cm);  NYI

        // Break apart the group -- everyone is the master
        newCanonicalMods->push_back(cm);

        UInt32 numSubords = cm->mEquivMods.size();

        UtString buf;
        buf  << "Congruency discarded for master "
             << cm->getName() << "\n"
             << "    number of subordinates: " << numSubords << "\n"
             << "    mismatching constants: " << numConstants << "\n"
             << "    -congruencyParamCostLimit " << mParamCostFactor << "\n"
             << "    module instruction cost " << numInstructions << "\n";
        if (mVerbose) {
          UtIO::cout() << buf;
        }
        if (mInfoFile != NULL) {
          *mInfoFile << buf;
        }

        for (UInt32 i = 0; i < numSubords; ++i) {
          CongruentObject* equiv = cm->mEquivMods[i];
          newCanonicalMods->push_back(equiv);
          equiv->mMaster = equiv;
          buf.clear();
          buf << "  discarded subordinate " << equiv->getName() << "\n";
          if (mVerbose) {
            UtIO::cout() << buf;
          }
          if (mInfoFile != NULL) {
            *mInfoFile << buf;
          }
        }
        cm->mEquivMods.clear();
      }
      else {
        // Cost OK -- still the master
        newCanonicalMods->push_back(cm); // still the master
      }        
    }
  } // else
} // void Congruent::findBestCongruenceGroups

#if 0
void Congruent::optimizeCongruenceGroups(ObjVec* ,
                                         CongruentObject* )
{
  // this cannot be more than 32 due to use of UInt32 as bitset
#     define MAX_MOD_COMBOS 5  // but it really must be much smaller.

  // Look at every 2^N possible combination of congruent modules to
  // see if they make the grade, as long as the number of
  // equivalent modules is small enough.  I don't want to this
  // n^2 calculation to blow up.  2^4 seems like a decent number
  // of combinations to try.
  if (nmods < MAX_MOD_COMBOS) {
    // Make a temp vec to make the search cleaner, and allocate
    // another one to use in place of cm->mEquivMods

    ObjVec equiv, testEquiv;
    equiv.push_back(cm);
    for (UInt32 i = 0; i < nmods - 1; ++i) {
      equiv.push_back(cm->mEquivMods[i]);
    }

    UInt32 candidateMask = 0;

    // We are picking out of 2^n set but we are not going
    // to count in binary to enumerate them.  It's better
    // to find the biggest possible sets.  So start by
    // eliminating 1 equivalent (nmods times).  If that
    // doesn't work, start eliminating 2 of them...
    // repeat until we've removed all the possiblities.

    // In this loop, note that nmods may be reduced in the middle,
    // and numToElminate adjusted as well
    for (UInt32 numToEliminate = 1; numToEliminate < nmods - 1; ++numToEliminate) {
      UInt32 eliminateMask = 0;
      UInt32 firstChoiceAvail = 0;

      // Figure out each element we want to eliminate
      for (UInt32 kill = 0; kill < numToEliminate; ++kill) {
        // Grab the first bit available
        for (UInt32 choice = firstChoiceAvail; choice < nmods; ++choice) {
          UInt32 mask = (1 << choice);
          bool avail = (bitSet & mask) == 0;
          if (avail) {
            testEquiv.push_back(equiv[choice]);
            if (choice == firstChoiceAvail) {
              ++firstChoiceAvail;
            }
            bitSet |= mask;
            break;
          }
        }
      }

      // Now we have a can

      // Use a binary bit-pattern walk to figure out which ones to eliminate
      UInt32 trials = sChoose(nmods, numToEliminate);
      // If I want to eliminate 1 module, there are nmods-choose-1 = nmods choices.  
      // If I want to eliminate 2 modules, there are nmods-choose-2
    }

    for (UInt32 i = 0; i < nmods; ++i) {
      CongruentObject* one = equiv[i];
      for (UInt32 j = i + 1; j < nmods; ++j) {
        CongruentObject* two = equiv[j];

        testEquiv[0] = two;
        numConstants = one->factorConstants(false, &testEquiv);

        if (CONST_TO_COST_RATIO*numConstants < cost.mInstructions) {
          newCanonicalMods->push_back(cm);
        }
      }
    }
  } // else if
  else {
    // Too many constants and too many combinations to try.  Don't
    // make any congruence for this set
    newCanonicalMods->push_back(cm);
    for (UInt32 i = 0; i < cm->mEquivMods.size(); ++i) {
      CongruentObject* equiv = cm->mEquivMods[i];
      newCanonicalMods->push_back(equiv);
      equiv->mMaster = equiv;
    }
    cm->mEquivMods.clear();
  }
} // void Congruent::optimizeCongruenceGroups
#endif


//! compute congruence on an entire design
bool Congruent::designModules(NUDesign *this_design) {
  NUInstanceLevels levels;
  levels.design(this_design);

  bool workDone = false;
  for (UInt32 i = 0, n = levels.numInstanceLevels(); i < n; ++i) {
    workDone |= computeCanonicalMods(levels, i);
    ObjVec  newCanonicalMods;
    for (UInt32 i = 0; i < mCanonicalObjVec->size(); ++i) {
      findBestCongruenceGroups(&newCanonicalMods, (*mCanonicalObjVec)[i]);
    }
    *mCanonicalObjVec = newCanonicalMods;

    convergeCanonicalMods();
  }
  return workDone;
} // bool Congruent::design

//! compute congruence for a module, folding together equivalent tasks
bool Congruent::moduleTasks(NUModule* mod) {
  INFO_ASSERT(!mFactorConstants, "Cannot factor constants for tasks yet");
  NUTaskLevels levels;
  levels.module(mod);
  bool workDone = false;

  for (UInt32 i = 0, n = levels.numTaskLevels(); i < n; ++i) {
    workDone |= computeCanonicalTasks(levels, i);
  }

  // Replace task-enables at the module level
  CongruentObject::TaskReplacementCallback trc(mNucleusObjMap, mVerbose,
                                               mInfoFile);
  NUDesignWalker walker(trc, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(mod);
  workDone |= trc.workDone();
  return workDone;
}

bool Congruent::computeCanonicalMods(const NUInstanceLevels& levels, UInt32 level) {
  mCanonicalObjVec->clear();
  mCongruentSet->clear();

  bool workDone = false;

  for (NUInstanceLevels::ModuleLevelLoop p = levels.loopLevel(level);
       !p.atEnd(); ++p)
  {
    NUModule* mod = *p;

    // This is very pessimistic.  Any directive on a net in a module keeps that module from being marked as congruent with any other.
    // If two modules have exactly the same net directives then they could still be considered for congruence testing but this code is not
    // that smart.  
    bool inNetDirective = mIODB->isModuleSpecifiedInNetDirective(mod) || mod->hasAsyncResetNets();

    UInt32 moduleIndex = mNucleusObjMap->size();

    // Compute the cost of the module in question.  We assume (& do
    // not check) that the cost of each congruent module is the same
    NUCost cost;
    mCostContext->calcModule(mod, &cost, false);
    CongruentObject* cm = new CongruentObject(mod,
                                              NULL, // stmtList
                                              NULL, // task
                                              NULL, // expr
                                              mNucleusObjMap, moduleIndex,
                                              inNetDirective,
                                              cost.mInstructions);
    mAllObjVec->push_back(cm);
    (*mNucleusObjMap)[mod] = cm;

    // As we go up the hierarchy from leaves to root, we must replace
    // instances from lower-level congruence, before we factor out
    // constants, otherwise the constants don't bubble up properly.
    // see test/congruent/twolevel.v
    workDone |= cm->replaceModInstances();
    cm->computeTokenStream();
    CongruentSet::iterator q = mCongruentSet->find(cm);
    if (q != mCongruentSet->end()) {
      CongruentObject* master = *q;
      master->addEquiv(cm);
    }
    else {
      mCongruentSet->insert(cm);
      mCanonicalObjVec->push_back(cm);
    }
  }
  return workDone;
} // bool Congruent::computeCanonicalMods

bool Congruent::computeCanonicalTasks(const NUTaskLevels& levels, UInt32 level)
{
  mCanonicalObjVec->clear();
  mCongruentSet->clear();
  bool workDone = false;

  for (NUTaskLevels::TaskLevelLoop p = levels.loopLevel(level);
       !p.atEnd(); ++p)
  {
    NUTask* task = *p;

    UInt32 taskIndex = mNucleusObjMap->size();

    NUCost cost;
    task->calcCost(&cost, mCostContext);
    bool inNetDirective = false; // do something for this for tasks?
    CongruentObject* cm = new CongruentObject(NULL, // mod
                                              NULL, // stmtList
                                              task,
                                              NULL, // expr
                                              mNucleusObjMap,
                                              taskIndex,
                                              inNetDirective,
                                              cost.mInstructions);
    mAllObjVec->push_back(cm);
    (*mNucleusObjMap)[task] = cm;

    // As we go up the hierarchy from leaves to root, we must replace
    // instances from lower-level congruence, before we factor out
    // constants, otherwise the constants don't bubble up properly.
    // see test/congruent/twolevel.v
    workDone |= cm->replaceTaskInstances(mVerbose, mInfoFile);
    cm->computeTokenStream();
    CongruentSet::iterator q = mCongruentSet->find(cm);
    if (q != mCongruentSet->end()) {
      CongruentObject* master = *q;
      master->addEquiv(cm);
    }
    else {
      mCongruentSet->insert(cm);
      mCanonicalObjVec->push_back(cm);
    }
  }
  return workDone;
} // bool Congruent::computeCanonicalTasks

void Congruent::convergeCanonicalMods() {
  for (UInt32 i = 0, n = mCanonicalObjVec->size(); i < n; ++i) {
    CongruentObject* cm = (*mCanonicalObjVec)[i];

    cm->factorConstants(true, NULL);
    if (mVerbose) {
      cm->print(UtIO::cout());
    }
    if (mInfoFile != NULL) {
      cm->print(*mInfoFile);
    }
  }
}

Congruent::Congruent(IODBNucleus* iodb, bool verbose, UInt32 paramCostFactor,
                     const char* congruenceFilename,
                     MsgContext* msgContext, bool factorConstants)
  : mIODB(iodb),
    mVerbose(verbose),
    mFactorConstants(factorConstants),
    mParamCostFactor(paramCostFactor),
    mMsgContext(msgContext)
{
  mNucleusObjMap = new NucleusObjMap;
  mCanonicalObjVec = new ObjVec;
  mAllObjVec = new ObjVec;
  mCostContext = new NUCostContext;

  CongruentCompare congruentCompare(mFactorConstants);
  mCongruentSet = new CongruentSet(congruentCompare);

  if (congruenceFilename == NULL) {
    mInfoFile = NULL;
  }
  else {
    mInfoFile = new UtOBStream(congruenceFilename, "a");
    if (!mInfoFile->is_open()) {
      mMsgContext->VlogCannotOpenFile(congruenceFilename, mInfoFile->getErrmsg());
      delete mInfoFile;
      mInfoFile = NULL;
    }
  }
}

Congruent::~Congruent() {
  clear();
  delete mCongruentSet;
  delete mNucleusObjMap;
  delete mAllObjVec;
  delete mCanonicalObjVec;
  delete mCostContext;
  if (mInfoFile != NULL) {
    if (!mInfoFile->close()) {
      mMsgContext->VlogCannotOpenFile(mInfoFile->getFilename(),
                                      mInfoFile->getErrmsg());
    }
    delete mInfoFile;
  }
}

void Congruent::clear() {
  mCongruentSet->clear();
  for (UInt32 i = 0, n = mAllObjVec->size(); i < n; ++i) {
    CongruentObject* cm = (*mAllObjVec)[i];
    delete cm;
  }
  mNucleusObjMap->clear();
  mAllObjVec->clear();
  mCanonicalObjVec->clear();
  mCostContext->clear();
}

NUStmtList* Congruent::insertStmtList(NUStmtList* stmtList) {
  UInt32 index = mCongruentSet->size();
  CongruentObject* co =  new CongruentObject(NULL,  // module
                                             stmtList,
                                             NULL,  // task
                                             NULL,  // expr
                                             mNucleusObjMap,
                                             index,
                                             false, // module is in net directive
                                             0);    // numUinstructions
  co->computeTokenStream();
  CongruentSet::iterator p = mCongruentSet->find(co);
  if (p != mCongruentSet->end()) {
    delete co;
    co = *p;
  }
  else {
    mCongruentSet->insert(co);
    mAllObjVec->push_back(co);
  }
  return co->getStmtList();
}

const NUExpr* Congruent::insertExpr(const NUExpr* expr) {
  UInt32 index = mCongruentSet->size();
  CongruentObject* co =  new CongruentObject(NULL,  // module
                                             NULL,  // stmtList
                                             NULL,  // task
                                             expr,
                                             mNucleusObjMap,
                                             index,
                                             false, // module is in net directive
                                             0);    // numUinstructions
  co->computeTokenStream();
  CongruentSet::iterator p = mCongruentSet->find(co);
  if (p != mCongruentSet->end()) {
    delete co;
    co = *p;
  }
  else {
    mCongruentSet->insert(co);
    mAllObjVec->push_back(co);
  }
  return co->getExpr();
}

bool Congruent::CongruentCompare::operator()(const CongruentObject* c1,
                                             const CongruentObject* c2)
  const
{
  return c1->compare(*c2, mFactorConstants) < 0;
}
