// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2011 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/AllocAlias.h"

#include "util/UtSet.h"
#include "util/UtIOStream.h"
#include "util/Loop.h"
#include "util/LoopMap.h"
#include "util/GenericDigraph.h"
#include "util/GraphBuilder.h"
#include "util/GraphWCC.h"

#include "nucleus/NUNet.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUDesign.h"

#include "compiler_driver/CarbonDBWrite.h"


bool AllocAlias::query(NUNet* net) const
{
  return mNetAliases->hasEquivalence(net);
}

bool AllocAlias::query(NUNet *net1, NUNet *net2) const
{
  // Ask the union find for the representative net for each. If they
  // are the same, they are equivalent.
  return getRepresentativeNet(net1) == getRepresentativeNet(net2);
}

int AllocAlias::compare(NUNet* net1, NUNet* net2) const
{
  // Get the representative net for each and compare the two
  NUNet* rep_net1 = mNetAliases->find(net1);
  NUNet* rep_net2 = mNetAliases->find(net2);
  ptrdiff_t diff = NUNet::compare(rep_net1, rep_net2);
  if (diff < 0) {
    return -1;
  } else if (diff > 0) {
    return 1;
  } else {
    return 0;
  }
}

void AllocAlias::equate(const NUNetSet& nets)
{
  mNetAliases->equate(nets);
}

void AllocAlias::add(NUNet* net)
{
  mNetAliases->add(net);
}

void AllocAlias::getRangeVectorForNet(NUNet* net, ConstantRangeVector& crv)
{
  if ( (getPortStatus() != eMatchedPortSizes) ||
       net->isBitNet() || net->isSysTaskNet() ||
       net->isVHDLLineNet() || net->isVirtualNet() )
    crv.push_back(ConstantRange(0,0));
  else
  {
    NUTempMemoryNet* tmpMem = dynamic_cast<NUTempMemoryNet*>(net);
    if (tmpMem != NULL)
    {
      crv.push_back(*(tmpMem->getWidthRange()));
      crv.push_back(*(tmpMem->getDepthRange()));
    }
    else if (net->isVectorNet())
    {
      const NUVectorNet* vn = net->castVectorNet();
      crv.push_back(*(vn->getRange()));
    }
    else if (net->isMemoryNet())
    {
      const NUMemoryNet* memNet = net->getMemoryNet();
      SInt32 numDims = memNet->getNumDims();
      for (SInt32 dim = 0; dim < numDims; ++dim)
        crv.push_back(*(memNet->getRange(dim)));
    }
    else if (net->isCompositeNet())
    {
      const NUCompositeNet* compNet = net->getCompositeNet();
      SInt32 numDims = compNet->getNumDims();
      for (SInt32 dim = 0; dim < numDims; ++dim)
        crv.push_back(*(compNet->getRange(dim)));
    }
    else
    {
      // Not yet supported.
      NU_ASSERT(0, net);
    }
  }
}

/*!
 * This design walk traverses all scopes, adding nets in the scopes to
 * the closure.  This is necessary because not all nets are added to the
 * symbol table.  In particular, block-local nets created by clock aliasing
 * are not there.
 */
class ScopeWalkerCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif
public:
  ScopeWalkerCallback(ElabAllocAlias* elabAllocAlias) :
    mElabAllocAlias(elabAllocAlias)
  {}
  ~ScopeWalkerCallback() {}

  Status operator()(Phase /* phase */, NUBase * /* node */)
  {
    return eSkip;
  }

  Status operator()(Phase /* phase */, NUDesign * /* node */)
  {
    return eNormal;
  }

  Status operator()(Phase phase, NUModule *module)
  {
    if (phase == ePre) {
      walkScope(module);
    }
    return eNormal;
  }

  Status operator()(Phase /* phase */, NUTF *tf)
  {
    walkScope(tf);
    return eSkip;
  }

  Status operator()(Phase /* phase */, NUModuleInstance * /* inst */)
  {
    return eNormal;
  }

  Status operator()(Phase /* phase */, NUNamedDeclarationScope * /* inst */)
  {
    return eNormal;
  }

private:
  void walkScope(NUScope *scope)
  {
    if (scope->getScopeType() == NUScope::eModule) {
      NUModule *module = dynamic_cast<NUModule*>(scope);
      NU_ASSERT(module, scope->getBase());
      for (NUNetVectorLoop net_loop = module->loopPorts();
	   not net_loop.atEnd();
	   ++net_loop) {
        NUNet* net = *net_loop;
        mElabAllocAlias->add(net);
      }
    }

    for (NUNetLoop net_loop = scope->loopLocals();
	 not net_loop.atEnd();
	 ++net_loop) {
        NUNet* net = *net_loop;
        mElabAllocAlias->add(net);
    }

    for (NUBlockLoop block_loop = scope->loopBlocks();
	 not block_loop.atEnd();
	 ++block_loop) {
      walkScope((*block_loop));
    }
    for (NUNamedDeclarationScopeLoop scope_loop = scope->loopDeclarationScopes();
	 not scope_loop.atEnd();
	 ++scope_loop) {
      walkScope((*scope_loop));
    }
  }

  ElabAllocAlias* mElabAllocAlias;
}; // class ScopeWalkerCallback : public NUDesignCallback

UnelabAllocAlias::UnelabAllocAlias(NUModule *module, PortStatus port_status) :
  AllocAlias(port_status),
  mModule(module)
{
  // Compute the actuals for analysis
  computeActuals();

  // If we are matching port sizes, break up all nets by
  // size. Otherwise they all get put into bucket 0.
  NetBuckets net_buckets;
  gatherAliasableNets(&net_buckets);

  // Analyze each bucket as a separate set of nets
  for (LoopMap<NetBuckets> l(net_buckets); !l.atEnd(); ++l) {
    const NUNetSet& nets = l.getValue();
    equate(nets);
  }
}

void UnelabAllocAlias::gatherAliasableNets(NetBuckets* net_buckets)
{
  // Walk all the buckets and gather the pertinent nets
  for (NUModule::NetLoop l = mModule->loopNets(); !l.atEnd(); ++l) {
    // Test if this net is aliasable, hier refs, hier refed,
    // non-primary ports, and actuals to sub modules.
    NUNet* net = *l;
    if (net->isHierRef() || net->hasHierRef() ||
        (net->isPort() && !net->isPrimary()) ||
        (mActuals.find(net) != mActuals.end()))
    {
      // Get the range vector for net in each dimension and use it
      // as a key for bucket. If we're not matching port sizes, use one
      // bucket with constant range (0:0). When matching port sizes,
      // this ensures that nets with unequal dimensions don't get aliased.
      ConstantRangeVector crv;
      getRangeVectorForNet(net, crv);
      NUNetSet& nets = (*net_buckets)[crv];
      nets.insert(net);
    }
  }
} // void UnelabAllocAlias::gatherAliasableNets

//! Callback class collect actuals from the instances of a module.
/*!
 * This class only needs to walk module instances to get at the
 * port connections.  From the port connections it just adds the
 * actual nets to the set given at construction.
 */
class ActualCallback : public NUDesignCallback
{
public:
  ActualCallback(NUModule *module, NUNetSet *actuals) :
    NUDesignCallback(),
    mModule(module),
    mActuals(actuals)
  {}

  ~ActualCallback() {}

  Status operator()(Phase, NUBase*) { return eSkip; }

  Status operator()(Phase, NUNamedDeclarationScope*) { return eNormal; }

  //! Only consider the given module, do not traverse sub-modules.
  Status operator()(Phase, NUModule* mod) { return (mod == mModule) ? eNormal : eSkip; }

  Status operator()(Phase, NUModuleInstance*) { return eNormal; }

  Status operator()(Phase, NUPortConnectionInput* conn)
  {
    if (not conn->isUnconnectedActual()) {
      conn->getActual()->getUses(mActuals);
    }
    return eSkip;
  }

  Status operator()(Phase, NUPortConnectionOutput* conn)
  {
    if (not conn->isUnconnectedActual()) {
      conn->getActual()->getDefs(mActuals);
    }
    return eSkip;
  }

  Status operator()(Phase, NUPortConnectionBid* conn)
  {
    if (not conn->isUnconnectedActual()) {
      conn->getActual()->getDefs(mActuals);
    }
    return eSkip;
  }

private:
  NUModule *mModule;
  NUNetSet *mActuals;
};

void UnelabAllocAlias::computeActuals()
{
  // This can occur before port lowering so need to be prepared to
  // see more complicated things than whole nets as actuals.
  //
  // Note that this means that if an expression occurs in the
  // actual (say for an input), every net in the expression
  // will be considered as connected to the actual.  After port
  // lowering, only the temporary created would be considered:
  // Before port lowering:
  //   wire a, b;
  //   sub sub (.i(a + b), ...);  // both a and b are considered
  // After port lowering:
  //   wire a, b, $temp;
  //   assign $temp = a + b;
  //   sub sub (.i($temp), ...);  // only $temp is considered
  //
  ActualCallback callback(mModule, &mActuals);
  NUDesignWalker walker(callback, false);
  walker.module(mModule);
}

ElabAllocAlias::ElabAllocAlias(PortStatus port_status) :
  AllocAlias(port_status)
{
}

void ElabAllocAlias::compute(NUDesign* design, STSymbolTable* symtab)
{
  // Walk the elaborated symbol table to find possible aliases in the
  // same scope.
  gatherAliases(symtab);

  // Walk the design adding all other nets in their own set
  ScopeWalkerCallback scope_callback(this);
  NUDesignWalker scope_walker(scope_callback, false);
  scope_walker.design(design);
}

void ElabAllocAlias::gatherAliases(STSymbolTable* symtab)
{
  for (STSymbolTable::NodeLoop iter = symtab->getNodeLoop();
       not iter.atEnd();
       ++iter) {
    STSymbolTableNode* node = *iter;
    STAliasedLeafNode* leaf = node->castLeaf();

    // Only walk alias rings when see a master, otherwise are n-squared
    if (leaf and (leaf->getMaster() == leaf)) {
      compute(leaf);
    }
  }
}

void ElabAllocAlias::compute(STAliasedLeafNode* leaf)
{
  // Walk the aliases and sort the nets by scope. Only two nets in the
  // same scope can be unelaboratedly aliased.
  typedef UtMap<NUScope*, NUNetSet> ScopeAliases;
  ScopeAliases scopeAliases;
  STAliasedLeafNode* alias_leaf = leaf;
  do {
    // Insert the current leaf. But be careful because some nodes may
    // not have an NUNet associated with it. I believe this occurs
    // when they get optimized away.
    NUNet* net = NUNet::find(alias_leaf);
    if (net != NULL) {
      NUScope* scope = net->getScope();
      scopeAliases[scope].insert(net);
    }

    // Look for the next one
    alias_leaf = alias_leaf->getAlias();
  } while (alias_leaf != leaf);

  // Walk the scopes and add them to the union find set
  for (LoopMap<ScopeAliases> l(scopeAliases); !l.atEnd(); ++l) {
    const NUNetSet& nets = l.getValue();
    equate(nets);
  }
}

//! Abstraction for a port connection graph
typedef GenericDigraph<void*, NUNet*> UnelabPortConnectionGraph;

//! class BuildUnelabPortConnectionGraph
/*! This class walks all the port connections and creates a graph from
 *  actuals to formals. The resulting roots of the graph can be used
 *  to find all possible alias rings.
 */
class BuildUnelabPortConnectionGraph : public NUDesignCallback
{
public:
  //! Constructor
  BuildUnelabPortConnectionGraph() {}

  //! Destructor
  ~BuildUnelabPortConnectionGraph() {}

  //! Get the created unelaborated port connection graph
  UnelabPortConnectionGraph* getGraph() { return mBuilder.getGraph(); }

private:
  //! Override the base callback to catch uninteresting objects
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Catch all the port connections and add them to the graph
  Status operator()(Phase phase, NUPortConnection* connection);

  //! Catch all top modules and add hier refs to the graph
  Status operator()(Phase phase, NUModule* module)
  {
    if (phase == ePre) {
      handleModule(module);
    }
    return eNormal;
  }

  //! Catch all lower level modules and add hier refs to the graph
  Status operator()(Phase phase, NUModuleInstance* instance)
  {
    if (phase == ePre) {
      handleModule(instance->getModule());
    }
    return eNormal;
  }

  //! Helper function to add module hier refs to the graph
  void handleModule(NUModule* module);

  //! Helper function to deal with actuals that are hier refs (they add edges)
  void addHierRefEdges(NUNet* net);

  //! Helper function to add a graph edge
  void buildEdge(NUNet* fromNet, NUNet* toNet);

  //! Graph builder for connection graph
  GraphBuilder<UnelabPortConnectionGraph> mBuilder;
}; // class BuildUnelabPortConnectionGraph

BuildUnelabPortConnectionGraph::Status
BuildUnelabPortConnectionGraph::operator()(Phase phase,
                                           NUPortConnection* connection)
{
  if (phase == ePre) {
    // Get the actuals and formal. The actual can be NULL, in that
    // case ignore this connection. It can also be an expression, in
    // which case we assume all the nets in the expression can be an
    // alias (in case the expression optimizes down to a simple net
    // expression)
    NUNetSet actuals;
    connection->getActuals(&actuals);
    for (Loop<NUNetSet> l(actuals); !l.atEnd(); ++l) {
      NUNet* actual = *l;
      NUNet* formal = connection->getFormal();

      // Note, if the actual is the same as the formal it means we
      // have the *other* direction bid port connection. Ignore them.
      if (actual != formal) {
        buildEdge(actual, formal);
      } else {
        NU_ASSERT(connection->getDir() == eBid, connection);
      }

      // Add any hier ref edges if the actual is a hier ref
      addHierRefEdges(actual);
    }
  }
  return eNormal;
}

void
BuildUnelabPortConnectionGraph::handleModule(NUModule* module)
{
  // Walk all the hier refs for this module. Only locals can be hier
  // refs, formals can't be.
  for (NUNetLoop l = module->loopNetHierRefs(); !l.atEnd(); ++l) {
    NUNet* net = *l;
    addHierRefEdges(net);
  }
}

void BuildUnelabPortConnectionGraph::addHierRefEdges(NUNet* net)
{
  if (not net->isHierRef()) {
    return;
  }

  // Walk the resolutions and add edges
  NUNetHierRef* netHierRef = net->getHierRef();
  for (NUNetHierRef::NetVectorLoop n = netHierRef->loopResolutions();
       !n.atEnd(); ++n) {
    // Create an edge in both directions. We want to introduce pessimism
    NUNet* resNet = *n;
    buildEdge(resNet, net);
    buildEdge(net, resNet);
  }
}

void
BuildUnelabPortConnectionGraph::buildEdge(NUNet* fromNet, NUNet* toNet)
{
  // Create the nodes
  mBuilder.addNode(fromNet, 0);
  mBuilder.addNode(toNet, 0);

  // And the edge
  mBuilder.addEdgeIfUnique(fromNet, toNet, NULL, 0);
}

bool DesignAllocAlias::SizeAndScopeCompare::operator()(const SizeAndScope& sas1,
                                                       const SizeAndScope& sas2) const
{
  ConstantRangeVectorCompare crvc;
  SInt32 compVal = crvc.compare(sas1.first, sas2.first);

  // Pointer compare for NUScope*.
  if (compVal == 0)
    compVal = sas1.second - sas2.second;

  return (compVal < 0);
}

DesignAllocAlias::DesignAllocAlias(NUDesign* design, PortStatus port_status) :
  AllocAlias(port_status),
  mDesign(design)
{
  // Walk the design creating a graph
  BuildUnelabPortConnectionGraph callback;
  NUDesignWalker walker(callback, false /* verbose */, false /* rememberVisit */);
  walker.design(mDesign);


  // Use the graph to find all aliases, using weakly connected components.
  UnelabPortConnectionGraph* graph = callback.getGraph();
  GraphWCC wcc;
  wcc.compute(graph);

  // For each component, gather the aliases and mark them
  for (GraphWCC::ComponentLoop l = wcc.loopComponents(); !l.atEnd(); ++l) {
    // Gather all the nets in this component
    GraphWCC::Component* comp = *l;
    NUNetVector aliases;
    for (GraphWCC::Component::NodeLoop n = comp->loopNodes(); !n.atEnd(); ++n) {
      GraphNode* graphNode = *n;
      NUNet* net = graph->castNode(graphNode)->getData();
      aliases.push_back(net);
    }

    // Mark possible aliases by scope
    processAliases(aliases);
  }

  // All done with the graph
  delete graph;
} // DesignAllocAlias::DesignAllocAlias

void DesignAllocAlias::processAliases(const NUNetVector& aliases)
{
  // Break up all the nets by size and scope
  ScopeAliases scopeAliases;
  for (CLoop<const NUNetVector> l(aliases); !l.atEnd(); ++l) {
    NUNet* net = *l;
    // Get the range vector for net in each dimension and use it
    // as a key for bucket. If we're not matching port sizes, use one
    // bucket with constant range (0:0). When matching port sizes,
    // this ensures that nets with unequal dimensions don't get aliased.
    ConstantRangeVector crv;
    getRangeVectorForNet(net, crv);
    NUScope* scope = net->getScope();
    SizeAndScope sizeAndScope(crv, scope);
    scopeAliases[sizeAndScope].insert(net);
  }

  // Now walk the sets and equate them
  for (LoopMap<ScopeAliases> l(scopeAliases); !l.atEnd(); ++l) {
    const NUNetSet& nets = l.getValue();
    equate(nets);
  }
}

//! Constructor for ProtectedAlias
/*! Iterates over all the nets in the design and notes the representative of
 *  the alias ring for any protected nets
 */

ProtectedAlias::ProtectedAlias (IODBNucleus *iodb, NUDesign *design) :
  DesignAllocAlias (design, AllocAlias::eMismatchedPortSizes)
{
  // look at each module in the design
  NUDesign::ModuleLoop loop = design->loopAllModules ();
  while (!loop.atEnd ()) {
    NUModule *module = *loop;
    NUNetList nets;
    // look at each net in the module
    module->getAllNets (&nets);
    for (NUNetList::iterator it = nets.begin (); it != nets.end (); it++) {
      NUNet *net = *it;
      if (!net->isProtected (iodb)) {
        // net is fair game
      } else {
        // add the representative to the set of protected nets
        protect (net);
      }
    }
    ++loop;
  }
}

//! construct a human-readable representation of the list of protected representative nets
void ProtectedAlias::compose (UtString *s, const UInt32 flags) const
{
  const UInt32 indent = flags & 0xFF;
  UtString prefix (indent, ' ');
  UInt32 start = s->size ();
  *s << prefix;
  if (mProtect.empty ()) {
    *s << "<empty>";
  } else {
    for (NUNetSet::const_iterator it = mProtect.begin (); it != mProtect.end (); ++it) {
      const char *name = (*it)->getName ()->str ();
      if (it != mProtect.begin ()) {
        *s << ",";
      }
      if (start + strlen (name) >= 80) {
        *s << UtIO::endl;
        start = s->size ();
        *s << prefix;
      }
      *s << name;
    }
  }
}
