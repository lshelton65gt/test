// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/Rescope.h"
#include "reduce/REUtil.h"
#include "reduce/RewriteCheck.h"
#include "reduce/Fold.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"

#include "nucleus/NUNetSet.h"

#include "flow/FLNode.h"

#include "iodb/IODBNucleus.h"
#include "localflow/UD.h"
#include "util/StringAtom.h"

#include "util/UtIOStream.h"

#include <algorithm>

Rescope::Rescope(AtomicCache *str_cache,
		 NUNetRefFactory *netref_factory,
		 MsgContext *msg_ctx,
		 IODBNucleus * iodb,
                 ArgProc* args,
                 SInt32 memory_limit,
		 bool verbose,
		 RescopeStatistics *stats) :
  mUtility(NULL),
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mArgs(args),
  mStatistics(stats),
  mMemoryLimit(memory_limit),
  mVerbose(verbose)
{
}


Rescope::~Rescope() 
{ 
  INFO_ASSERT(!mUtility, "consistency check error");
}


void Rescope::module(NUModule * this_module)
{
  NU_ASSERT(!mUtility, this_module);
  mUtility = new ReduceUtility(this_module,
			       mNetRefFactory);
  mUtility->update();

  bool nets_were_rescoped = rescopeAllBlocks(this_module);

  nets_were_rescoped |= rescopeTFs(this_module);

  delete mUtility;
  mUtility=NULL;

  if ( nets_were_rescoped ) {
    mStatistics->module();

    // If this module has changed, redo UD (after processing submodules)
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.module(this_module);
  }    

}


void Rescope::mergedBlocks(NUModule * this_module,
			   NUUseDefVector & blocks,
			   NUNetSet * rescoped_nets)
{
  NU_ASSERT(!mUtility, this_module);
  mUtility = new ReduceUtility(this_module,
			       mNetRefFactory);
  mUtility->update();

  bool nets_were_rescoped = false;
  for (NUUseDefVector::iterator iter = blocks.begin();
       iter != blocks.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(use_def);
    NU_ASSERT(always!=NULL,use_def);

    nets_were_rescoped |= rescopeMergedBlock(always, rescoped_nets);

    nets_were_rescoped |= rescopeUnwavableMergedBlock(always);
  }

  delete mUtility;
  mUtility=NULL;

  if (nets_were_rescoped) {
    mStatistics->module();
  }

}


bool Rescope::rescopeAllBlocks(NUModule * this_module)
{
  // traverse this module looking for net expected to persist between
  // blocks/modules/cont assigns.

  bool nets_were_rescoped = false;

  // traverse this module and identify/create block-local nets.
  NUStructuredProcList procs;
  this_module->getStructuredProcs(&procs);
  for (NUStructuredProcList::iterator iter = procs.begin();
       iter != procs.end();
       ++iter) {
    NUStructuredProc * proc = (*iter);
    nets_were_rescoped |= rescope(proc);
  }

  return nets_were_rescoped;
}


bool Rescope::rescope(NUStructuredProc * proc)
{    
  bool found_locals = false;

  NUNetSet pending_rescope;

  NUBlock *block = proc->getBlock();

  // Consider all block-defs as local candidates. Any module-level
  // use indicates either a feedback or cross-block dependency arc;
  // invalidating the net as a candidate.
  NUNetSet net_set;
  block->getBlockingDefs(&net_set);

  for (NUNetSet::SortedLoop loop = net_set.loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    // Tells us if a net can be block-local in scope
    bool potential_local = qualified(net);

    // Make sure that a candidate memory is not used in an ident. We
    // don't rewrite full-memory assigns, only memsels.
    if (potential_local) {
      if (net->is2DAnything()) {
	// TBD: do this ident walk once per net.
	RescopeMemoryIdentSearch callback(net);
	NUDesignWalker walker(callback,false);
        callback.putWalker(&walker);
	NUDesignCallback::Status status = walker.structuredProc(proc);
	potential_local = (status == NUDesignCallback::eNormal);
      }
    }

    // We've got something which is local in scope.
    if ( potential_local ) {
      // Remember it for later application.
      pending_rescope.insert(net);

      // Report that we found a local.
      if ( mVerbose ) {
	printRescope("Rescope", proc, net, not found_locals);
      }

      // Update summary information.
      mStatistics->net();
      if (not found_locals) {
	mStatistics->block();
      }
      found_locals = true;
    }
  }

  // make sure that found_locals==false IFF pending_rescope.empty()
  NU_ASSERT( found_locals xor pending_rescope.empty(), proc );

  if (found_locals) {
    demote(block, &pending_rescope);
  }

  return found_locals;
}


bool Rescope::rescopeTFs(NUModule * this_module)
{
  // traverse this module looking for net expected to persist between
  // blocks/modules/cont assigns.

  bool nets_were_rescoped = false;

  // traverse this module and identify/create block-local nets.
  for (NUTaskLoop loop = this_module->loopTasks();
       not loop.atEnd();
       ++loop) {
    NUTask * task = (*loop);
    nets_were_rescoped |= rescope(task);
  }

  if (nets_were_rescoped) {
    mStatistics->module();
  }

  return nets_were_rescoped;
}


bool Rescope::rescope(NUTask * task)
{    
  bool found_locals = false;

  NUNetSet pending_rescope;

  // Consider all block-defs as local candidates. Any module-level
  // use indicates either a feedback or cross-block dependency arc;
  // invalidating the net as a candidate.
  NUNetSet net_set;
  task->getBlockingDefs(&net_set);

  for (NUNetSet::SortedLoop loop = net_set.loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    // Tells us if a net can be block-local in scope
    bool potential_local = qualifiedTFNet(net);

    // Make sure that a candidate memory is not used in an ident. We
    // don't rewrite full-memory assigns, only memsels.
    if (potential_local) {
      if (net->is2DAnything()) {
	// TBD: do this ident walk once per net.
	RescopeMemoryIdentSearch callback(net);
	NUDesignWalker walker(callback,false);
        callback.putWalker(&walker);
	NUDesignCallback::Status status = walker.task(task);
	potential_local = (status == NUDesignCallback::eNormal);
      }
    }

    // We've got something which is local in scope.
    if ( potential_local ) {
      // Remember it for later application.
      pending_rescope.insert(net);

      // Report that we found a local.
      if ( mVerbose ) {
	printRescope("Rescope", task, net, not found_locals);
      }

      // Update summary information.
      mStatistics->net();
      if (not found_locals) {
	mStatistics->task();
      }
      found_locals = true;
    }
  }

  // make sure that found_locals==false IFF pending_rescope.empty()
  NU_ASSERT( found_locals xor pending_rescope.empty(), task );

  if (found_locals) {
    demote(task, &pending_rescope);
  }

  return found_locals;
}


bool Rescope::qualifiedNetRef(NUNetRefHdl & net_ref) const
{
  NUNet * net = net_ref->getNet();

  // if an input or output, cannot be local
  if ( net->isInput() or
       net->isOutput() or
       net->isBid() ) {
    return false;
  }

  // if this is already a local net, don't do all this crazy stuff!
  if (net->isNonStatic()) {
    return false;
  }

  // don't allow edge triggers or memories (during the scheduler).
  if (net->isEdgeTrigger()) return false;
  if (net->is2DAnything()) return false;

  // if the user has specified this net in a directives file, it can't be local.
  if ( net->isProtected(mIODB) ) {
    return false;
  }

  // if its used in any block (including this one), it can't be local.
  if (mUtility->hasModuleUse(net_ref)) {
    return false;
  }

  // nets appearing in always blocks which also appear in initial
  // blocks can't be rescoped into the always block -- if they are,
  // the initial state of the rescoped net could be incorrect.
  if (mUtility->inInitialBlock(net_ref)) {
    return false;
  }
      
  // nets appearing in always blocks which also get def'ed in tasks
  // can't be rescoped into the always block -- if they are,
  // the task won't write to the same net.
  if (mUtility->inTask(net_ref)) {
    return false;
  }

  return true;
}


bool Rescope::qualified(NUNet * net) const
{
  // if an input or output, cannot be local
  if ( net->isInput() or
       net->isOutput() or
       net->isBid() ) {
    return false;
  }

  // if this is already a local net, don't do all this crazy stuff!
  if (net->isNonStatic()) {
    return false;
  }
      
  // if its used in any block (including this one), it can't be local.
  if (mUtility->hasModuleUse(net)) {
    return false;
  }

  // nets appearing in always blocks which also appear in initial
  // blocks can't be rescoped into the always block -- if they are,
  // the initial state of the rescoped net could be incorrect.
  if (mUtility->inInitialBlock(net)) {
    return false;
  }
      
  // nets appearing in always blocks which also get def'ed in tasks
  // can't be rescoped into the always block -- if they are,
  // the task won't write to the same net.
  if (mUtility->inTask(net)) {
    return false;
  }
      
  // if the user has specified this net in a directives file, it can't be local.
  if ( net->isProtected(mIODB) ) {
    return false;
  }

  // watch out for big memories.
  if (net->is2DAnything()) {
    // reg [2:0] mem [1000:0];
    // always 
    //     for (i=0;i<=1;i=i+1) 
    //       mem[i] = in;

    // Rescoping will translate this into:

    // always begin
    //   reg [2:0] $rescope_mem_addr_0;
    //   reg [2:0] $rescope_mem_addr_1;
    //   ...
    //   for (i=0;i<=1;i=i+1) begin
    //     case(i) :
    //     0: $rescope_mem_addr_0 = in;
    //     1: $rescope_mem_addr_1 = in;
    //     ...
    //     endcase
    //   end
    // end

    // We've gone from having one net (the memory) to 1000. In the
    // customer example, the size of the memory was 65k. We need to
    // perform UD computations for all generated nets. We need to
    // generate flow for all generated nets.

    // Depth is a multiplier for the cost of UD (remember -- we
    // cache/collect UD information for block-stmts).
    NUMemoryNet * mem = dynamic_cast<NUMemoryNet*>(net);
    if (((SInt32)mem->getDepth()) > mMemoryLimit) {
      return false;
    }
  }

  return true;
}


bool Rescope::qualifiedTFNet(NUNet * net) const
{
  // if an input or output, cannot be local
  if ( net->isInput() or
       net->isOutput() or
       net->isBid() ) {
    return false;
  }

  // If this net does not belong to the TF, it's not ours to touch. 
  //
  // In the future, we may want to rescope module-scoped nets into the
  // task if they act like a task-tmp:
  // \code
  // reg tmp;
  // task doit;
  //   input a,b,c;
  //   output o;
  //   begin
  //   tmp = a ^ b;
  //   o = tmp & c;
  //   end
  // endtask
  // \endcode
  // Here, 'tmp' can be rescoped, provided it is not referenced elsewhere.
  if (not net->isTFNet()) {
    return false;
  }

  // watch out for big memories.
  if (net->is2DAnything()) {
    // reg [2:0] mem [1000:0];
    // always 
    //     for (i=0;i<=1;i=i+1) 
    //       mem[i] = in;

    // Rescoping will translate this into:

    // always begin
    //   reg [2:0] $rescope_mem_addr_0;
    //   reg [2:0] $rescope_mem_addr_1;
    //   ...
    //   for (i=0;i<=1;i=i+1) begin
    //     case(i) :
    //     0: $rescope_mem_addr_0 = in;
    //     1: $rescope_mem_addr_1 = in;
    //     ...
    //     endcase
    //   end
    // end

    // We've gone from having one net (the memory) to 1000. In the
    // customer example, the size of the memory was 65k. We need to
    // perform UD computations for all generated nets. We need to
    // generate flow for all generated nets.

    // Depth is a multiplier for the cost of UD (remember -- we
    // cache/collect UD information for block-stmts).
    NUMemoryNet * mem = dynamic_cast<NUMemoryNet*>(net);
    if (mem) {
      return (((SInt32)mem->getDepth()) <= mMemoryLimit);
    } else {
      // temp memory?
      return false;
    }
  }

  // only allow rescoping of task-owned memories; when we code
  // generate tasks, all other types are already non-statics.
  return false;;
}


template<class DeclarationType>
void Rescope::demote(DeclarationType * scope, const NUNetSet * pending_rescope)
{
  RescopeTranslator translator;

  // 1. Create block-local replacements for each rescope candidate.
  //    For memories, create a temporary for each address.
  for (NUNetSet::SortedLoop iter = pending_rescope->loopSorted();
       !iter.atEnd();
       ++iter) {
    demote(scope,(*iter),&translator);
  }
  
  // 2. Rewrite dynamic memory references as static.
  rewriteDynamicMemories(scope,pending_rescope);

  // 3. Rewrite the always block in terms of the block-local
  //    replacements
  scope->replaceLeaves(translator);

  // 4. Consistency-check - the block should no longer reference any
  //    of the original candidates.
  RewriteCheck consistency(*pending_rescope);
  scope->replaceLeaves(consistency);
}


StringAtom * Rescope::generateNameForNetRef(const char * prefix,
                                            NUBlock * block,
                                            NUNetRefHdl & net_ref)
{
  StringAtom * name = NULL;
  NUNet * net = net_ref->getNet();
  if (net_ref->all()) {
    name = block->gensym(prefix,net->getName()->str());
  } else {
    ConstantRange range;
    bool success = net_ref->getRange(range);
    NU_ASSERT(success,net_ref);

    UtString ref_name;
    ref_name << net->getName()->str();
    ref_name << "[" << range.getMsb();
    if (range.getMsb() != range.getLsb()) {
      ref_name << ":" << range.getLsb();
    }
    ref_name << "]";
    name = block->gensym(prefix,ref_name.c_str());
  }
  return name;
}

void Rescope::demoteNetRefs(NUBlock * block, const NUNetRefSet * pending_rescope)
{
  RescopeTranslator translator;

  for (NUNetRefSet::const_iterator iter = pending_rescope->begin();
       iter != pending_rescope->end();
       ++iter) {
    NUNetRefHdl def_net_ref = (*iter);
    NUNet * net = def_net_ref->getNet();

    NUExpr * replacement = NULL;
    if (net->isVectorNet()) {
      NUVectorNet * vnet = dynamic_cast<NUVectorNet*>(net);

      NUExprVector parts;
      SInt32 last_bit_processed = -1;
      for (NUNetRefRangeLoop loop = def_net_ref->loopRanges(mNetRefFactory);
           not loop.atEnd();
           ++loop) {
        NUNetRefHdl net_ref = loop.getCurrentNetRef();

        ConstantRange range;
        bool success = net_ref->getRange(range);
        NU_ASSERT(success,net_ref);

        if (range.getLsb() > (last_bit_processed+1)) {
          // there's a hole that this net ref doesn't cover.
          ConstantRange hole(range.getLsb()-1,last_bit_processed+1);
          NUExpr * hole_expr = new NUVarselRvalue(vnet,hole,vnet->getLoc());
          hole_expr->resize(hole.getLength());
          parts.push_back(hole_expr);
        }

        StringAtom * name = generateNameForNetRef("rescopepart",block,net_ref);
        NUNet * partial_net = block->createTempNet(name, range.getLength(), 
                                                   false, vnet->getLoc());
        NUExpr * partial_expr = new NUIdentRvalue(partial_net,vnet->getLoc());
        partial_expr->resize(partial_net->getBitSize());
        parts.push_back(partial_expr);

        last_bit_processed = range.getMsb();
      }
    
      const ConstantRange * net_range = vnet->getRange();
      if (net_range->getMsb() != last_bit_processed) {
        // the net ref doesn't cover the full range of the net.
        ConstantRange hole(net_range->getMsb(),last_bit_processed+1);
        NUExpr * hole_expr = new NUVarselRvalue(vnet,hole,vnet->getLoc());
        hole_expr->resize(hole.getLength());
        parts.push_back(hole_expr);
      }

      std::reverse(parts.begin(), parts.end());
      replacement = new NUConcatOp(parts, 1, vnet->getLoc());
      replacement->resize(net->getBitSize());
    } else {
      // bit net
      NU_ASSERT(net->isBitNet(),def_net_ref);
      NU_ASSERT(def_net_ref->all(),def_net_ref);

      // tbd: better name.
      StringAtom * name = generateNameForNetRef("rescopepart",block,def_net_ref);
      NUNet * partial_net = block->createTempBitNet(name,false,net->getLoc());
      replacement = new NUIdentRvalue(partial_net,net->getLoc());
      replacement->resize(1);
    }

    NU_ASSERT(replacement,def_net_ref);
    translator.add(net,replacement);

  }

  block->replaceLeaves(translator);
}


NetFlags Rescope::determineFlags(const NUNet * net) const
{
  NetFlags net_flags = NetFlags(eTempNet|eAllocatedNet|eBlockLocalNet|eNonStaticNet);
  // preserve storage information.
  if ( net->isReg() ) {
    net_flags = NetRedeclare(net_flags, eDMRegNet);
  } else if ( net->isWire() ) {
    net_flags = NetRedeclare(net_flags, eDMWireNet);
  }
  return net_flags;
}


template<class DeclarationType>
void Rescope::demote(DeclarationType * scope, 
                     NUNet * original,
                     RescopeTranslator * translator)
{
  // 1. make a local copy of the net
  //    * mark with the eNonStaticNet flag
  //    * scope the net into the block

  NetFlags net_flags(determineFlags(original));
   
  if ( original->is2DAnything() ) {
    // ASSERT("This assertion tells us which testcases are good 'temp memory' cases." == NULL);
    NUMemoryNet *mem_net = dynamic_cast<NUMemoryNet*>(original);

    UtString head_string;
    head_string << "rescope_" << mem_net->getName()->str();

    const ConstantRange *depth = mem_net->getDepthRange();

    for (SInt32 i = depth->rightmost(); i<=depth->leftmost(); ++i) {
      UtString name_string;

      // if 'i' is negative, add 'n' to the name.
      name_string << (i<0 ? "n" : "") << std::abs(i);

      StringAtom *name = scope->gensym(head_string.c_str(),
                                       name_string.c_str());

      NUNet * replacement = new NUVectorNet(name,
					    new ConstantRange(*mem_net->getWidthRange()),
					    net_flags,
                                            mem_net->getVectorNetFlags (),
					    scope,
					    mem_net->getLoc());
      scope->addLocal(replacement);
      translator->add(mem_net,i,replacement);
    }
  }
  else
  {
    // leave the original name as part of the temp name for simpler debugging.
    StringAtom *name = scope->gensym("rescope", original->getName()->str());

    NUNet * replacement = NULL;
    NUVectorNet *vector_net = dynamic_cast<NUVectorNet*>(original);
    if ( vector_net )
    {
      // preserve vector characteristics
      if ( vector_net->isInteger() ) {
	net_flags = NetRedeclare(net_flags, eDMIntegerNet); 
      }
      else if ( vector_net->isTime() ) {
	net_flags = NetRedeclare(net_flags, eDMTimeNet);
      }
      else if ( vector_net->isReal( )) {
        net_flags = NetRedeclare( net_flags, eDMRealNet );
      }
      if ( vector_net->isSigned() )
        net_flags = NetFlags(net_flags | eSigned);

      replacement = new NUVectorNet(name, 
				    new ConstantRange ( *vector_net->getRange() ), 
				    net_flags, 
                                    vector_net->getVectorNetFlags (),
				    scope, 
				    original->getLoc());
      scope->addLocal(replacement);
    }
    else if ( original->isBitNet() ) 
    {
      replacement = new NUBitNet(name, net_flags, scope, original->getLoc());
      scope->addLocal(replacement);
    }
    NU_ASSERT ( replacement!=NULL, original );

    translator->add(original,replacement);
  }
}


/* Rewrite rules:
 *
 * 1. Dynamic read.
 *      out = mem[sel];
 *    Becomes:
 *      case(sel)
 *      0: out = mem[0];
 *      ...
 *      endcase
 *
 * 2. Dynamic write
 *      mem[sel] = val;
 *    Becomes:
 *      case(sel) // full-case
 *      0: mem[0] = val;
 *      ...
 *      endcase
 *
 * 3. Dynamic read in selector.
 *      if (mem[sel]) begin
 *        stmts...
 *      end
 *    Becomes:
 *      begin // block
 *        reg tmp;
 *        case(sel) // full-case
 *        0: tmp=mem[0];
 *        ...
 *        endcase
 *        if (tmp) begin
 *          stmts...
 *        end
 *      end
 *
 * 4. Dynamic def and use.
 *      mem[a] = mem[b];
 *    Becomes:
 *      begin // block
 *        reg tmp;
 *        case(sel)
 *        0: tmp = mem[0];
 *        ...
 *        endcase
 *        case(sel) // full-case
 *        0: mem[0] = tmp;
 *        ...
 *        endcase
 *      end
 */
void Rescope::rewriteDynamicMemories(NUBlockScope * block, const NUNetSet * pending_rescope)
{
  RescopeDynamicMemoryHelper helper(mNetRefFactory, 
                                    pending_rescope);
  helper.blockScope(block);
}


static bool sReallyMultiplyDriven(const NUNet * net)
{
  NUUseDefNode * seen_use_def = NULL;
  for (NUNet::ConstDriverLoop loop = net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    FLNode * driver = (*loop);
    NUUseDefNode * use_def = driver->getUseDefNode();
    if (seen_use_def) {
      if (use_def and (seen_use_def != use_def)) {
        return true;
      }
    } else {
      seen_use_def = use_def;
    }
  }
  return false;
}


bool Rescope::rescopeMergedBlock(NUAlwaysBlock * always,
                                 NUNetSet * rescoped_nets)
{
  NUBlock *block = always->getBlock();

  bool found_locals = false;

  NUNetSet pending_rescope;

  NUNetSet net_set;
  block->getBlockingDefs(&net_set);

  for (NUNetSet::SortedLoop loop = net_set.loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);

    bool potential_local = qualified(net);

    if (potential_local) {
      potential_local = not sReallyMultiplyDriven(net);
    }

    if (potential_local) {
      NU_ASSERT(not net->isDoubleBuffered(), net);
    }
      
    if (potential_local) {
      potential_local = (mUtility->getDefCount(net) == 1);
    }

    if (potential_local) {
      // Clock aliasing mixed with block merging expects clocks
      // without adequate to persist. This is the workaround.
      // Removing this test will break:
      //     test/schedule/derivedclocks15.v
      potential_local = (not net->isEdgeTrigger());
    }

    if (potential_local) {
      // TBD: Can memories become local after block-merging?
      potential_local = (not net->is2DAnything());
    }

    if (potential_local) {
      pending_rescope.insert(net);

      if (mVerbose) {
        printRescope("Rescope[Merge]", always, net, not found_locals);
      }

      mStatistics->net();
      if (not found_locals) {
        mStatistics->block();
      }

      found_locals = true;
    }
  }

  // Make sure that found_locals==false IFF pending_rescope.empty()
  NU_ASSERT( found_locals xor pending_rescope.empty(), always );

  if (found_locals) {
    demote(always->getBlock(), &pending_rescope);

    rescoped_nets->insert(pending_rescope.begin(), pending_rescope.end());

    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.structuredProc(always);
  }

  return found_locals;
}


bool Rescope::rescopeUnwavableMergedBlock(NUAlwaysBlock * always)
{
  bool found_locals = false;

  NUBlock *block = always->getBlock();

  NUNetRefSet pending_rescope(mNetRefFactory);

  // Get the set of defs and uses for processing
  NUNetRefSet defs(mNetRefFactory);
  NUNetRefSet uses(mNetRefFactory);
  block->getBlockingDefs(&defs);
  always->getUses(&uses);

  // Precompute the set of bits outside the defined portion that
  // intersect with uses. This is done to avoid an N*M algorithm in
  // the for loop below.
  NUNetRefSet undefined_parts(mNetRefFactory);
  for (NUNetRefSet::iterator i = defs.begin(); i != defs.end(); ++i) {
    const NUNetRefHdl& def_net_ref = *i;
    NUNet* def_net = def_net_ref->getNet();
    NUNetRefHdl complete_net_ref = mNetRefFactory->createNetRef(def_net);
    NUNetRefHdl undefined_part  = mNetRefFactory->subtract(complete_net_ref,
                                                           def_net_ref);
    undefined_parts.insert(undefined_part);
  }
  NUNetRefSet invalid_defs(mNetRefFactory);
  NUNetRefSet::set_intersection(undefined_parts, uses, invalid_defs);

  // Qualify the net refs
  for (NUNetRefSet::iterator iter = defs.begin();
       iter != defs.end();
       ++iter) {
    NUNetRefHdl def_net_ref = (*iter);
    NUNet * def_net = def_net_ref->getNet();

    // If there are uses of bits outside this defined portion, do not
    // rescope. We might not be able to cleanly rewrite. This is were
    // the above invalid_defs comes in handy.
    if (!invalid_defs.findNet(def_net)->empty()) {
      continue;
    }

    // Allow rescoping only if all locally-defined parts are legal. We
    // could relax this to evaluate each partial net-ref
    // independently, but would need to update the net-ref owned by
    // FLNodes. Today, treating each net-ref independently triggers
    // the REUnexpectedUD Alert in SplitFlow.
    
    // The following customer tests broke:
    // test/cust/stargen/kestrel/carbon/cwtb/vcore
    // test/cust/stargen/kestrel/carbon/cwtb/ktop
    // test/cust/stargen/kestrel/carbon/cwtb/kcore
    // test/cust/matrox/sunex/carbon/cwtb/demtop

    bool potential_local = true;
    for (NUNetRefRangeLoop loop = def_net_ref->loopRanges(mNetRefFactory);
         potential_local and not loop.atEnd();
         ++loop) {
      NUNetRefHdl partial_def_net_ref = loop.getCurrentNetRef();
      potential_local = qualifiedNetRef(partial_def_net_ref);
    }

    // rewrite each part as a separate temporary.
    if (potential_local) {
      for (NUNetRefRangeLoop loop = def_net_ref->loopRanges(mNetRefFactory);
           not loop.atEnd();
           ++loop) {
        NUNetRefHdl partial_def_net_ref = loop.getCurrentNetRef();
        pending_rescope.insert(partial_def_net_ref);

        if (mVerbose) {
          printPartialRescope("Rescope[Merge:Partial]", always, partial_def_net_ref, not found_locals);
        }

        if (partial_def_net_ref->all()) {
          mStatistics->net();
        } else {
          mStatistics->net_ref();
        }
        if (not found_locals) {
          mStatistics->block();
        }

        found_locals = true;
      }
    }
  }

  // Make sure that found_locals==false IFF pending_rescope.empty()
  NU_ASSERT( found_locals xor pending_rescope.empty(), always );

  if (found_locals) {
    demoteNetRefs(always->getBlock(), &pending_rescope);

    Fold fold (mNetRefFactory, 
               mArgs,
               mMsgContext, 
               mStrCache,
               mIODB,
               false,
               0);
    fold.structuredProc(always);

    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.structuredProc(always);
  }

  return found_locals;
}


void Rescope::lowerScope(NUNet * net, NUBlock * block)
{
  NUScope * scope = net->getScope();

  STAliasedLeafNode * master = net->getMasterNameLeaf();
  STAliasedLeafNode * alias = master;
  do {
    NUAliasDataBOM * bom = NUAliasBOM::castBOM(alias->getBOMData());
    if (bom->hasValidNet())
    {
      NUNet * alias_net = bom->getNet();
      NUScope * alias_scope = alias_net->getScope();
      if (alias_scope==scope) {
        // remove the net from its declaration scope, but we still have live references...
        scope->removeLocal(alias_net,false);
        alias_net->replaceScope(scope,block);

        // update flags before adding to the block.
        NetFlags flags = NetFlags(alias_net->getFlags()|eBlockLocalNet|eNonStaticNet|eTempNet);
        alias_net->setFlags(flags);
        block->addLocal(alias_net);
      }
    }
  } while ((alias = alias->getAlias()) != master);
}


NUDesignCallback::Status RescopeMemoryIdentSearch::operator()(Phase, NUIdentRvalue * node)
{
  NUNet * net = node->getIdent();
  if (net==mNet) {
    return eStop;
  } else {
    return eNormal;
  }
}


NUDesignCallback::Status RescopeMemoryIdentSearch::operator()(Phase, NUIdentLvalue * node)
{
  NUNet * net = node->getIdent();
  if (net==mNet) {
    return eStop;
  } else {
    return eNormal;
  }
}

// As of 8/5/05, Memsel*value walkers hit the ident exprs, which is
// not what Rescope wants.  So override those callbacks to just
// recurse into the index expressions.  This might be done more simply
// by filtering memory-ntest in the Ident*value callbacks above, but
// that doesn't work for test/bugs/bug3640, which has a VHDL whole
// memory assign, which does *not* want to be rescoped, and the
// in Ident*value callbacks don't know where they are called from.
NUDesignCallback::Status RescopeMemoryIdentSearch::operator()(Phase phase, NUMemselRvalue * node)
{
  if (phase == ePre) {
    mWalker->expr(node->getIndex(0));
  }
  return NUDesignCallback::eSkip;
}


NUDesignCallback::Status RescopeMemoryIdentSearch::operator()(Phase phase, NUMemselLvalue * node)
{
  if (phase == ePre) {
    mWalker->expr(node->getIndex(0));
  }
  return NUDesignCallback::eSkip;
}

void RescopeMemoryIdentSearch::putWalker(NUDesignWalker* walker) {
  mWalker = walker;
}


bool RescopeDynamicMemoryHelper::stmt(NUStmt * stmt, NUStmtList * replacements) 
{
  bool changed = false;
  switch(stmt->getType()) {
  case eNUBlock: {
    // blocks do not cause a change to their parent StmtList.
    blockScope(dynamic_cast<NUBlock*>(stmt));
    replacements->push_back(stmt);
    break;
  }
  case eNUIf: {
    changed = ifStmt(dynamic_cast<NUIf*>(stmt), replacements);
    break;
  }
  case eNUCase: {
    changed = caseStmt(dynamic_cast<NUCase*>(stmt), replacements);
    break;
  }
  case eNUFor: {
    changed = forStmt(dynamic_cast<NUFor*>(stmt), replacements);
    break;
  }
  case eNUBlockingAssign: {
    changed = assignStmt(dynamic_cast<NUBlockingAssign*>(stmt), replacements);
    break;
  }
  default: {
    RescopeDynamicMemoryDiscoveryCallback callback(mRescopedNets);
    NUDesignWalker walker(callback,false);
    walker.stmt(stmt);

    if (callback.any()) {
      decompose(stmt, replacements);
      changed = true;
    } else {
      replacements->push_back(stmt);
    }
    break;
  }
  }
  return changed;
}


bool RescopeDynamicMemoryHelper::blockScope(NUBlockScope * block)
{
  // keep track of our current containing scope.

  // make sure our parent scope is the current top of stack.
  if (not mScopes.empty()) {
    NUScope * top = mScopes.top();
    if (top != block->getParentScope()) {
      NU_ASSERT(0,(NUBase*)block);
    }
  }
  mScopes.push(block);
  
  // Process and update our stmtlist.
  NUStmtList replacements;
  bool changed = processStmtLoop(block->loopStmts(),
                                 &replacements);
  if (changed) {
    block->replaceStmtList(replacements);
  }

  // Leaving this scope
  mScopes.pop();

  return false;
}

 
bool RescopeDynamicMemoryHelper::caseStmt(NUCase * stmt, 
                                          NUStmtList * replacements)
{
  // Process and update our stmtlists.
  for (NUCase::ItemLoop iter = stmt->loopItems();
       !iter.atEnd();
       ++iter) {
    NUCaseItem* item = (*iter);

    NUStmtList item_replacements;
    bool changed = processStmtLoop(item->loopStmts(),
                                   &item_replacements);
    if (changed) {
      item->replaceStmts(item_replacements);
    }
  }

  // Process references in conditions
  RescopeDynamicMemoryDiscoveryCallback callback(mRescopedNets);
  NUDesignWalker walker(callback,false);

  walker.expr(stmt->getSelect());

  for (NUCase::ItemLoop iter = stmt->loopItems();
       !iter.atEnd();
       ++iter) {
    NUCaseItem* item = (*iter);
    for (NUCaseItem::ConditionLoop loop = item->loopConditions();
	 not loop.atEnd();
	 ++loop) {
      NUCaseCondition * condition = (*loop);
      walker.expr(condition->getExpr());
    }
  }

  if (callback.any()) {
    decompose(stmt, replacements);
    return true;
  } else {
    replacements->push_back(stmt);
    return false;
  }
}


bool RescopeDynamicMemoryHelper::ifStmt(NUIf * stmt, 
                                        NUStmtList * replacements)
{
  // Process and update our stmtlists.
  NUStmtList clause_replacements;
  bool changed;

  // Then clause.
  changed = processStmtLoop(stmt->loopThen(),
                            &clause_replacements);
  if (changed) {
    stmt->replaceThen(clause_replacements);
  }
  clause_replacements.clear();

  // Else clause.
  changed = processStmtLoop(stmt->loopElse(),
                            &clause_replacements);
  if (changed) {
    stmt->replaceElse(clause_replacements);
  }
  clause_replacements.clear();

  // Process references in condition.
  RescopeDynamicMemoryDiscoveryCallback callback(mRescopedNets);
  NUDesignWalker walker(callback,false);
  walker.expr(stmt->getCond());

  if (callback.any()) {
    decompose(stmt, replacements);
    return true;
  } else {
    replacements->push_back(stmt);
    return false;
  }
}


bool RescopeDynamicMemoryHelper::forStmt(NUFor * stmt,
                                         NUStmtList * replacements)
{
  bool changed;
  // Process and update our stmtlist.
  NUStmtList body_replacements;
  changed = processStmtLoop(stmt->loopBody(), &body_replacements);
  if (changed) {
    stmt->replaceBody(body_replacements);
  }

  // Process references in declaration.
  NUStmtList initial_replacements;
  changed = processStmtLoop(stmt->loopInitial(), &initial_replacements);
  if (changed) {
    stmt->replaceInitial(initial_replacements);
  }
  NUStmtList advance_replacements;
  changed = processStmtLoop(stmt->loopAdvance(), &advance_replacements);
  if (changed) {
    stmt->replaceAdvance(advance_replacements);
  }


  RescopeDynamicMemoryDiscoveryCallback callback(mRescopedNets);
  NUDesignWalker walker(callback,false);
  walker.expr(stmt->getCondition());

  if (callback.any()) {
    decompose(stmt, replacements);
    return true;
  } else {
    replacements->push_back(stmt);
    return false;
  }
}


bool RescopeDynamicMemoryHelper::assignStmt(NUBlockingAssign * stmt,
                                            NUStmtList * replacements)
{
  RescopeDynamicMemoryDiscoveryCallback callback(mRescopedNets);
  NUDesignWalker walker(callback,false);
  walker.blockingAssign(stmt);

  if (not callback.any()) {
    replacements->push_back(stmt);
    return false;
  }

  NUExprSet   memsel_rvalues;
  NULvalueSet memsel_lvalues;

  callback.getRvalues(&memsel_rvalues);
  callback.getLvalues(&memsel_lvalues);

  SInt32 count = (memsel_rvalues.size() + memsel_lvalues.size());
  if (count > 1) {
    decompose(stmt, replacements);

  } else if (not memsel_rvalues.empty()) {
    NU_ASSERT(memsel_rvalues.size()==1,stmt);
    NUExpr * memsel = (*memsel_rvalues.begin());

    processSingleMemsel<NUExpr,NUMemselRvalue>
      (memsel,stmt,replacements);

  } else if (not memsel_lvalues.empty()) {
    NU_ASSERT(memsel_lvalues.size()==1,stmt);
    NULvalue * memsel = (*memsel_lvalues.begin());

    processSingleMemsel<NULvalue,NUMemselLvalue>
      (memsel,stmt,replacements);
    
  } else {
    NU_ASSERT(0, stmt); // unreachable.
  }
  
  return true;
}


bool RescopeDynamicMemoryHelper::processStmtLoop(NUStmtLoop loop,
                                                 NUStmtList * replacements)
{
  bool any_changed = false;
  bool one_changed = false;
  one_changed = processStmtLoopWorker(loop,replacements);
  any_changed |= one_changed;
  while (one_changed) {
    NUStmtList local_replacements;
    one_changed = processStmtLoopWorker(NUStmtLoop(*replacements),
                                        &local_replacements);
    any_changed |= one_changed;
    replacements->assign(local_replacements.begin(),
                         local_replacements.end());
  }
  return any_changed;
}


bool RescopeDynamicMemoryHelper::processStmtLoopWorker(NUStmtLoop loop,
                                                       NUStmtList * replacements)
{
  bool changed = false;
  for ( /*no-initial*/; not loop.atEnd(); ++loop) {
    changed |= stmt(*loop,replacements);
  }
  return changed;
}


template<class SuperMemselType,class MemselType>
void RescopeDynamicMemoryHelper::processSingleMemsel(SuperMemselType * super_memsel, 
                                                     NUStmt * node,
                                                     NUStmtList * replacements)
{
  MemselType * memsel = dynamic_cast<MemselType*>(super_memsel);
  NU_ASSERT(memsel,super_memsel);

  NUExpr * address = memsel->getIndex(0);

  NUMemoryNet * memory_net = dynamic_cast<NUMemoryNet*>(memsel->getIdent());
  NU_ASSERT(memory_net,memsel);

  CopyContext copy_context(NULL,NULL);

  NUCase * case_stmt = new NUCase (address->copy(copy_context),
				   mNetRefFactory,
                                   NUCase::eCtypeCase,
                                   false,
				   node->getLoc());
  case_stmt->putFullySpecified(true);

  RescopeDynamicMemoryRewriter rewriter(false);
  const ConstantRange * depth =  memory_net->getDepthRange();

  bool address_is_signed = address->isSignedResult ();
  UInt32 address_width = address->getBitSize();
  if ( address_width > 32 ) address_width = 32; // limit address to maximum of 32 bits
  for (SInt32 i = depth->rightmost(); i<=depth->leftmost(); ++i) {

    // create the new, static memsel.
    NUExpr * index = NUConst::create (address_is_signed, (SInt64)i, address_width, node->getLoc());

    MemselType * static_memsel = new MemselType(memory_net,
						index->copy (copy_context),
						node->getLoc());

    // rewrite a copy of the stmt using the static memsel.
    rewriter.add(memsel,static_memsel);
    NUStmt * stmt = node->copy(copy_context);
    bool changed = stmt->replaceLeaves(rewriter);
    NU_ASSERT(changed,stmt);
    rewriter.clear();

    // create a case item.
    NUCaseItem * item = new NUCaseItem(node->getLoc());

    // create a case condition.
    NUCaseCondition * cond = new NUCaseCondition(node->getLoc(),
						 index);

    item->addCondition(cond);
    item->addStmt(stmt);

    case_stmt->addItem(item);
  }
  
  replacements->push_back(case_stmt);
  delete node;
}


void RescopeDynamicMemoryHelper::decompose(NUStmt * node, NUStmtList * replacements)
{
  // Decompose a statement containing multiple dynamic references
  // into a series of statements containing at most one dynamic
  // reference.

  NUScope * scope = mScopes.top();

  RescopeDynamicMemoryRewriter rewriter(true);
  RescopeDynamicMemorySeparationCallback callback(scope,&rewriter);
  NUDesignWalker walker(callback,false);

  switch (node->getType()) {
  case eNUIf: {
    NUIf * if_stmt = dynamic_cast<NUIf*>(node);
    walker.expr(if_stmt->getCond());
    break;
  }
  case eNUCase: {
    NUCase * case_stmt = dynamic_cast<NUCase*>(node);
    walker.expr(case_stmt->getSelect());
    for (NUCase::ItemLoop iter = case_stmt->loopItems();
	 !iter.atEnd();
	 ++iter) {
      NUCaseItem* item = (*iter);
      for (NUCaseItem::ConditionLoop loop = item->loopConditions();
	   not loop.atEnd();
	   ++loop) {
	NUCaseCondition * condition = (*loop);
	walker.expr(condition->getExpr());
      }
    }
    break;
  }
  case eNUFor: {
    NUFor * for_stmt = dynamic_cast<NUFor*>(node);
    walker.expr(for_stmt->getCondition()); // initial and adavance are
                                           // now handled like body
    break;
  }

  case eNUBlockingAssign:
  case eNUTaskEnable:
  case eNUCModelCall:
  case eNUBlockingEnabledDriver:
  case eNUControlSysTask:
  case eNUOutputSysTask:
  case eNUInputSysTask:
  case eNUFOpenSysTask:
  case eNUFCloseSysTask:
  case eNUFFlushSysTask: {
    walker.stmt(node);
    break;
  }

  default: {
    NU_ASSERT("Unexpected statement type when trying to rescope a memory."==NULL, node);
    break;
  }
  }

  callback.getPreStmts(replacements);

  node->replaceLeaves(rewriter);
  replacements->push_back(node);

  callback.getPostStmts(replacements);
}


//! Determine if a memselect index expression is constant and within the depth range for the specified memory.
static bool sIsConstantMemselInBounds(const NUNet * net, const NUExpr * index)
{
  NU_ASSERT(net->is2DAnything(),net);
  const NUMemoryNet * memory = net->getMemoryNet();

  const NUConst* constant = index->castConstNoXZ ();
  if (constant == NULL) {
    return false;
  }

  // Bug 5836: Do not allow rescoping when the index expression requires more than 32 bits.
  if (constant->getBitSize() > 32) {
    return false;
  }

  SInt32 index_value = 0;
  bool success = false;
  if ( constant->isSignedResult() ){
    success = constant->getL(&index_value);
  } else {
    // we cannot use constant->getL here because it will return false
    // (indicating a failure) when the constant value is not signed but
    // the MSB is set (suggesting a negative value).  Instead we use
    // getUL and then allow C to convert the unsigned bitpattern into
    // a signed pattern.  In the case where the MSB is not set then
    // the copy will work fine, when MSB is set then the src/dest must
    // be the same size to get the proper conversion.
    UInt32 unsigned_index_value = 0;
    // unsigned_index_value and index_value must be same number of
    // bits for the following assignment conversion from unsigned to
    // signed to work.
    NU_ASSERT(sizeof(unsigned_index_value) == sizeof(index_value), index);
    success = constant->getUL(&unsigned_index_value);
    index_value = unsigned_index_value;
  }
  NU_ASSERT(success,index);
  const ConstantRange * depthRange = memory->getDepthRange();
  return depthRange->contains(index_value);
}


NUDesignCallback::Status RescopeDynamicMemoryDiscoveryCallback::operator()(Phase, NUMemselRvalue * memsel)
{
  // if non-constant index or index out-of-range, remember this memsel.
  NUNet * net = memsel->getIdent();
  if (mRescopedNets->find(net)!=mRescopedNets->end()) {
    if (not sIsConstantMemselInBounds(net, memsel->getIndex(0))) {
      mDynamicMemselRvalues.insert(memsel);
    }
  }
  return eNormal;
}


NUDesignCallback::Status RescopeDynamicMemoryDiscoveryCallback::operator()(Phase, NUMemselLvalue * memsel)
{
  // if non-constant index or index out-of-range, remember this memsel.
  NUNet * net = memsel->getIdent();
  if (mRescopedNets->find(net)!=mRescopedNets->end()) {
    if (not sIsConstantMemselInBounds(net, memsel->getIndex(0))) {
      mDynamicMemselLvalues.insert(memsel);
    }
  }
  return eNormal;
}


void RescopeDynamicMemoryDiscoveryCallback::getRvalues(NUExprSet * rvalues)
{ 
  rvalues->insert(mDynamicMemselRvalues.begin(),
                  mDynamicMemselRvalues.end());
}


void RescopeDynamicMemoryDiscoveryCallback::getLvalues(NULvalueSet * lvalues) 
{
  lvalues->insert(mDynamicMemselLvalues.begin(),
                  mDynamicMemselLvalues.end());
}


void RescopeDynamicMemoryDiscoveryCallback::clear()
{ 
  mDynamicMemselRvalues.clear(); 
  mDynamicMemselLvalues.clear(); 
}


bool RescopeDynamicMemoryDiscoveryCallback::any()
{ 
  return not (mDynamicMemselRvalues.empty() and mDynamicMemselLvalues.empty()); 
}


void RescopeDynamicMemorySeparationCallback::hoistExpression(NUMemselRvalue * expr)
{
  StringAtom* sym = mScope->gensym("rescope", NULL, expr);
  NUNet * net = mScope->createTempNet(sym,
                                      expr->determineBitSize(),
                                      expr->isSignedResult(),
                                      expr->getLoc());

  CopyContext copy_context(NULL,NULL);

  NULvalue * lvalue = new NUIdentLvalue(net,expr->getLoc());
  NUAssign * assign = new NUBlockingAssign(lvalue,expr->copy(copy_context), false, expr->getLoc());
  assign->resize();
  
  // add assign to block.
  mPreStmts.push_back(assign);

  NUExpr * hoisted = new NUIdentRvalue(net,expr->getLoc());
  hoisted->resize(expr->getBitSize());

  // replace memsel with rvalue.
  mRewriter->add(expr,hoisted);
}


void RescopeDynamicMemorySeparationCallback::hoistLvalue(NUMemselLvalue * lvalue)
{
  StringAtom* sym = mScope->gensym("rescope", NULL, lvalue);
  NUNet * net = mScope->createTempNet(sym,
                                      lvalue->getBitSize(),
                                      (lvalue->isWholeIdentifier() &&
                                      lvalue->getWholeIdentifier()->isSigned()),
                                      lvalue->getLoc());

  CopyContext copy_context(NULL,NULL);

  NUExpr * rvalue = new NUIdentRvalue(net,lvalue->getLoc());
  NUAssign * assign = new NUBlockingAssign(lvalue->copy(copy_context),rvalue,false,lvalue->getLoc());
  assign->resize();

  // add assign to block.
  mPostStmts.push_back(assign);

  NULvalue * hoisted = new NUIdentLvalue(net,lvalue->getLoc());
  hoisted->resize();

  // replace memsel with lvalue.
  mRewriter->add(lvalue,hoisted);
}


NUDesignCallback::Status RescopeDynamicMemorySeparationCallback::operator()(Phase phase, NUMemselRvalue * memsel)
{
  if (phase==ePre) 
    return eNormal;

  // Constant reference -- pass.
  if (sIsConstantMemselInBounds(memsel->getIdent(), memsel->getIndex(0))) {
    return eNormal;
  }

  memsel->replaceLeaves(*mRewriter);

  hoistExpression(memsel);

  return eSkip;
}

NUDesignCallback::Status RescopeDynamicMemorySeparationCallback::operator()(Phase phase, NUMemselLvalue * memsel)
{
  if (phase==ePre) 
    return eNormal;

  // Constant reference -- pass.
  if (sIsConstantMemselInBounds(memsel->getIdent(), memsel->getIndex(0))) {
    return eNormal;
  }

  memsel->replaceLeaves(*mRewriter);

  hoistLvalue(memsel);

  return eNormal;
}


void RescopeDynamicMemorySeparationCallback::getPreStmts(NUStmtList * stmts) 
{
  stmts->insert(stmts->end(),mPreStmts.begin(),mPreStmts.end());
}


void RescopeDynamicMemorySeparationCallback::getPostStmts(NUStmtList * stmts) 
{
  stmts->insert(stmts->end(),mPostStmts.begin(),mPostStmts.end());
}


NUExpr * RescopeDynamicMemoryRewriter::operator()(NUExpr * node, Phase phase) 
{
  if (not NuToNuFn::isPre(phase)) {
    return NULL;
  }

  NUExpr * replacement = NULL;
  NUMemselRvalue * memsel = dynamic_cast<NUMemselRvalue*>(node);
  if (memsel) {
    if (mPtrComparison) {
      NUExprExprMap::iterator location = mExprReplacements.find(memsel);
      if (location != mExprReplacements.end()) {
        replacement = (*location).second;
      }
    } else {
      for (NUExprExprMap::iterator iter = mExprReplacements.begin();
           (not replacement) and iter != mExprReplacements.end();
           ++iter) {
        const NUExpr * old_expr = (*iter).first;
        const NUMemselRvalue * old_memsel = dynamic_cast<const NUMemselRvalue*>(old_expr);
        NU_ASSERT(old_memsel, old_expr);
        if (old_memsel->getIdent()==memsel->getIdent() and
            old_memsel->getIndex(0)->equal (*memsel->getIndex(0))) {
          replacement = (*iter).second;
          replacement->resize(old_memsel->getBitSize());
        }
      }
    }
  }
  if (replacement)
    delete node;
  return replacement;
}

NULvalue * RescopeDynamicMemoryRewriter::operator()(NULvalue * node, Phase phase) 
{
  if (not NuToNuFn::isPre(phase)) {
    return NULL;
  }

  NULvalue * replacement = NULL;
  NUMemselLvalue * memsel = dynamic_cast<NUMemselLvalue*>(node);
  if (memsel) {
    if (mPtrComparison) {
      NULvalueLvalueMap::iterator location = mLvalueReplacements.find(memsel);
      if (location != mLvalueReplacements.end()) {
        replacement = (*location).second;
      }
    } else {
      for (NULvalueLvalueMap::iterator iter = mLvalueReplacements.begin();
           (not replacement) and iter != mLvalueReplacements.end();
           ++iter) {
        const NULvalue * old_expr = (*iter).first;
        const NUMemselLvalue * old_memsel = dynamic_cast<const NUMemselLvalue*>(old_expr);
        NU_ASSERT(old_memsel, old_expr);
        if (old_memsel->getIdent()==memsel->getIdent() and
            old_memsel->getIndex(0)->equal (*memsel->getIndex(0))) {
          replacement = (*iter).second;
          replacement->resize();
        }
      }
    }
  }
  if (replacement)
    delete node;
  return replacement;
}


void RescopeDynamicMemoryRewriter::add(NUMemselRvalue * old_expr, NUExpr * new_expr) 
{
  mExprReplacements[old_expr] = new_expr;
}


void RescopeDynamicMemoryRewriter::add(NUExprExprMap & expr_replacements) 
{
  mExprReplacements.insert(expr_replacements.begin(),expr_replacements.end());
}


void RescopeDynamicMemoryRewriter::add(NUMemselLvalue * old_lvalue, NULvalue * new_lvalue) 
{
  mLvalueReplacements[old_lvalue] = new_lvalue;
}


void RescopeDynamicMemoryRewriter::add(NULvalueLvalueMap & lvalue_replacements) 
{
  mLvalueReplacements.insert(lvalue_replacements.begin(),lvalue_replacements.end());
}


void RescopeDynamicMemoryRewriter::clear() 
{ 
  mExprReplacements.clear(); 
  mLvalueReplacements.clear(); 
}


RescopeTranslator::RescopeTranslator ()
{
}


RescopeTranslator::~RescopeTranslator ()
{
  for (NUExprReplacementMap::iterator i=mExprReplacements.begin();
       i!=mExprReplacements.end();
       ++i) {
    delete i->second;
  }
}


NUNet * RescopeTranslator::operator()(NUNet * net, Phase phase) 
{
  if (isPre(phase)) {
    NUNetReplacementMap::iterator location = mNetReplacements.find(net);
    if ( location != mNetReplacements.end() ) {
      NUNet * replacement = NULL;
      replacement = location->second;
      return replacement;
    }
  }
  return NULL;
}


NULvalue * RescopeTranslator::operator()(NULvalue * lvalue, Phase phase) 
{ 
  if (isPost(phase)) {
    NUIdentLvalue * ident = dynamic_cast<NUIdentLvalue*>(lvalue);
    if (ident) {
      NUNet * net = ident->getIdent();
      NUExprReplacementMap::iterator location = mExprReplacements.find(net);
      if ( location != mExprReplacements.end() ) {
        NUExpr * concat = location->second;
        NULvalue * replacement = concat->Lvalue(lvalue->getLoc());
        delete lvalue;
        return replacement;
      }
    }
  }

  if (isPre(phase)) {
    NUMemselLvalue * memsel = dynamic_cast<NUMemselLvalue*>(lvalue);
    if (not memsel) {
      return NULL; 
    }

    NUNet * address_net = getAddressNet(memsel->getIdent(),
                                        memsel->getIndex(0));
    if (address_net) {
      NULvalue * replacement = new NUIdentLvalue(address_net,
                                                 memsel->getLoc());
      replacement->resize();
      delete memsel;
      return replacement;
    } 
  }
  return NULL;
}


NUExpr * RescopeTranslator::operator()(NUExpr * expr, Phase phase)  
{ 
  if (isPost(phase)) {
    NUIdentRvalue * ident = dynamic_cast<NUIdentRvalue*>(expr);
    if (ident) {
      NUNet * net = ident->getIdent();
      NUExprReplacementMap::iterator location = mExprReplacements.find(net);
      if ( location != mExprReplacements.end() ) {
        NUExpr * concat = location->second;
        CopyContext cc(NULL,NULL);
        NUExpr * replacement = concat->copy(cc);
        replacement->resize (ident->getBitSize ());
        replacement->setSignedResult (ident->isSignedResult ());
        delete ident;
        return replacement;
      }
    }
  }

  if (isPre(phase)) {
    NUMemselRvalue * memsel = dynamic_cast<NUMemselRvalue*>(expr);
    if (not memsel) {
      return NULL;
    }

    NUNet * address_net = getAddressNet(memsel->getIdent(),
                                        memsel->getIndex(0));
    if (address_net) {
      NUExpr * replacement = new NUIdentRvalue(address_net,
                                               memsel->getLoc());
      replacement->resize(memsel->getBitSize());
      replacement->setSignedResult (memsel->isSignedResult ());
      delete memsel;
      return replacement;
    } 
  }
  return NULL;
}


NUNet * RescopeTranslator::getAddressNet(const NUNet * memory_net, const NUExpr * address_expr) const
{
  if (not sIsConstantMemselInBounds(memory_net, address_expr)) {
    return NULL;
  }

  const NUConst * address_const = address_expr->castConstNoXZ ();
  NU_ASSERT(address_const, address_expr);

  // We should never find a memory with an address that needs more than 32 bits.
  NU_ASSERT(address_const->getBitSize() <= 32, address_const);

  SInt32 address = 0;
  address_const->getL(&address);

  return getAddressNet(memory_net,address);
}


NUNet * RescopeTranslator::getAddressNet(const NUNet * memory_net, SInt32 address) const
{
  NUNet * address_net = NULL;
  MemoryReplacementMap::const_iterator memory_location = mMemoryReplacements.find(memory_net);
  if (memory_location!=mMemoryReplacements.end()) {
    const AddressMap & address_map = (*memory_location).second;
    AddressMap::const_iterator address_location = address_map.find(address);
    NU_ASSERT(address_location!=address_map.end(),memory_net);
    address_net = (*address_location).second;
  }
  return address_net;
}


void RescopeTranslator::add(const NUNet * original, NUNet * replacement)
{
  mNetReplacements[original] = replacement;
}


void RescopeTranslator::add(const NUNet * original, NUExpr * replacement)
{
  mExprReplacements[original] = replacement;
}


void RescopeTranslator::add(const NUMemoryNet * original, SInt32 address, NUNet * replacement)
{
  mMemoryReplacements[original][address] = replacement;
}


void Rescope::printHeader(const char * label,
                          const NUStructuredProc * proc) const
{
    const NUModule * module = proc->findParentModule();
    UtIO::cout() << label << ": Module: " << module->getName()->str();
    UtIO::cout() << " Block: " << proc->getName()->str() << " ";
    proc->getLoc().print();
    UtIO::cout() << " has locals:" << UtIO::endl;
}


void Rescope::printPartialRescope(const char * label,
                                  const NUStructuredProc * proc,
                                  const NUNetRefHdl & net_ref,
                                  bool do_header) const
{
  if (do_header) { 
    // first local found in this block. print header.
    printHeader(label, proc);
  }
  const NUNet * net = net_ref->getNet();
  UtIO::cout() << "    " << net->getName()->str();

  if (not net_ref->all()) {
    ConstantRange range;
    bool success = net_ref->getRange(range);
    NU_ASSERT(success,net_ref);

    if (net->isVectorNet()) {
      const NUVectorNet * vnet = dynamic_cast<const NUVectorNet*>(net);
      const ConstantRange * declaration_range = vnet->getDeclaredRange();
      range.denormalize(declaration_range);
    } else {
      NU_ASSERT(net->is2DAnything(),net_ref);
    }

    UtIO::cout() << " [" << range.getMsb();
    if (range.getMsb()!=range.getLsb()) {
      UtIO::cout() << ":" << range.getLsb();
    }
    UtIO::cout() << "]";
  }

  if (net->is2DAnything()) {
    UtIO::cout() << " [memory]";
  }
  UtIO::cout() << UtIO::endl;
}


void Rescope::printRescope(const char * label,
                           const NUStructuredProc * proc,
                           const NUNet * net,
                           bool do_header) const
{
  if (do_header) { 
    // first local found in this block. print header.
    printHeader(label, proc);
  }
  UtIO::cout() << "    " << net->getName()->str();
  if (net->is2DAnything()) {
    UtIO::cout() << " [memory]";
  }
  UtIO::cout() << UtIO::endl;
}


void Rescope::printRescope(const char * label,
                           const NUTask * task,
                           const NUNet * net,
                           bool do_header) const
{
  if (do_header) { 
    // first local found in this block. print header.
    const NUModule * module = task->getModule();
    UtIO::cout() << label << ": Module: " << module->getName()->str();
    UtIO::cout() << " task: ";
    task->getNameBranch()->print();
    UtIO::cout() << " ";
    task->getLoc().print();
    UtIO::cout() << " has locals:" << UtIO::endl;
  }
  UtIO::cout() << "    " << net->getName()->str();
  if (net->is2DAnything()) {
    UtIO::cout() << " [memory]";
  }
  UtIO::cout() << UtIO::endl;
}


void RescopeStatistics::print() const
{
  UtIO::cout() << "Rescope Summary: "
	       << mNets << " nets and "
               << mNetRefs << " partial nets rescoped into "
	       << mBlocks << " blocks and "
               << mTasks << " tasks in " 
	       << mModules << " modules." << UtIO::endl;
}
