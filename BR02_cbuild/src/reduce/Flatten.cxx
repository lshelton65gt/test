// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/Flatten.h"
#include "reduce/RewriteUtil.h"
#include "reduce/Fold.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUControlFlowNet.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUSysTask.h"

#include "util/ArgProc.h"
#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"
#include "util/AtomicCache.h"

#include "iodb/IODBNucleus.h"

#include "symtab/STSymbolTable.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STBranchNode.h"

#include "localflow/MarkReadWriteNets.h"

#include "exprsynth/Expr.h"
#include "exprsynth/SymTabExpr.h"

#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"

#include "compiler_driver/CarbonContext.h" // for CRYPT

/*! Only clone locals which have the storage.
 * Nets which do not have the storage have been optimized
 * away in the design, and will be cloned as part of their
 * alias ring.
 */
static bool sNetDoClone(NUNet *net)
{
  return (net->getStorage() == net);
}


class LeafConsistency : public NuToNuFn
{
public:
  LeafConsistency(NUModule * parent,
                  NUModuleInstance * instance) :
    mParent(parent),
    mInstance(instance),
    mChild(instance->getModule())
  {}

  virtual NUNet * operator()(NUNet * net, Phase phase) {
    if (not isPre(phase))
      return NULL;

    NUModule * net_module = net->getScope()->getModule();
    if (net_module == mChild) {
      UtIO::cout() << "Encountered a net rewriting error when flattening." << UtIO::endl;
      UtIO::cout() << "    Parent:   " << mParent->getName()->str() << UtIO::endl;
      UtIO::cout() << "    Instance: " << mInstance->getName()->str() << UtIO::endl;
      UtIO::cout() << "    Net:      " << net->getName()->str() << UtIO::endl;
      NU_ASSERT(net_module!=mChild, net_module);
    }
    return NULL;
  }
  virtual NUTF * operator()(NUTF * tf, Phase) {
    NUModule * tf_module = tf->getModule();
    if (tf_module == mChild) {
      UtIO::cout() << "Encountered a tf rewriting error when flattening." << UtIO::endl;
      UtIO::cout() << "    Parent:   " << mParent->getName()->str() << UtIO::endl;
      UtIO::cout() << "    Instance: " << mInstance->getName()->str() << UtIO::endl;
      UtIO::cout() << "    TF:       " << tf->getName()->str() << UtIO::endl;
      NU_ASSERT(tf_module!=mChild, tf_module);
    }
    return NULL;
  }
  virtual NUCModel * operator()(NUCModel * , Phase) {
    // no assertion checks yet
    return NULL;
  }
  virtual NULvalue * operator()(NULvalue * /*lvalue*/, Phase) {
    // no assertion checks.. deferred to net check.
    return NULL;
  }
  virtual NUExpr * operator()(NUExpr * /*rvalue*/, Phase) {
    // no assertion checks.. deferred to net check.
    return NULL;
  }

private:
  //! The target module for flattening.
  NUModule * mParent;
  //! The child module instance being flattened into the parent.
  NUModuleInstance * mInstance;
  //! The child module referenced by the instance.
  NUModule * mChild;
};


/*!
 * Class to traverse a module, finding nets which should not be scalarized.
 * A net should not be scalarized if it is not advantageous; if they are used in
 * vector operations, or if they are used in dynamic bit selects.
 */
class FindNoScalarizeNetsCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  FindNoScalarizeNetsCallback(NUModule *module, NUNetSet &nets) :
    mModule(module),
    mNoScalarizeNets(nets)
  {}
  ~FindNoScalarizeNetsCallback() {}

  Status operator()(Phase /* phase */, NUBase * /* node */)
  {
    return eNormal;
  }

  Status operator()(Phase /* phase */, NUModule *node)
  {
    if (node == mModule) {
      return eNormal;
    } else {
      return eSkip;
    }
  }

  Status operator()(Phase phase, NUPortConnectionInput *node)
  {
    if (phase == ePre) {
      NUExpr *actual = node->getActual();
      if (actual) {
        actual->getUses(&mNoScalarizeNets);
        return eSkip;
      } else {
        return eNormal;
      }
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUPortConnectionOutput *node)
  {
    if (phase == ePre) {
      NULvalue *actual = node->getActual();
      if (actual) {
        actual->getDefs(&mNoScalarizeNets);
        return eSkip;
      } else {
        return eNormal;
      }
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUPortConnectionBid *node)
  {
    if (phase == ePre) {
      NULvalue *actual = node->getActual();
      if (actual) {
        actual->getDefs(&mNoScalarizeNets);
        return eSkip;
      } else {
        return eNormal;
      }
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUVarselRvalue *node)
  {
    if (phase == ePre) {
      if (not node->isConstIndex ()) {
        NUExpr* ident = node->getIdentExpr();
        ident->getUses(&mNoScalarizeNets);
        return eSkip;
      } else {
        return eNormal;
      }
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUMemselRvalue *node)
  {
    if (phase == ePre) {
      NU_ASSERT( node->isResynthesized(), node );
      NUExpr *idx = node->getIndex(0);
      if ((idx->getType() != NUExpr::eNUConstXZ) and (idx->getType() != NUExpr::eNUConstNoXZ)) {
        mNoScalarizeNets.insert(node->getIdent());
        return eSkip;
      } else {
        return eNormal;
      }
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUVarselLvalue *node)
  {
    if (phase == ePre) {
      if (not node->isConstIndex ()) {
        NULvalue* lvalue = node->getLvalue();
        lvalue->getDefs(&mNoScalarizeNets);
        return eSkip;
      } else {
        return eNormal;
      }
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase phase, NUMemselLvalue *node)
  {
    if (phase == ePre) {
      NU_ASSERT( node->isResynthesized(), node );
      NUExpr *idx = node->getIndex(0);
      if ((idx->getType() != NUExpr::eNUConstXZ) and (idx->getType() != NUExpr::eNUConstNoXZ)) {
        mNoScalarizeNets.insert(node->getIdent());
        return eSkip;
      } else {
        return eNormal;
      }
    } else {
      return eNormal;
    }
  }

  Status operator()(Phase, NUOp *)
  {
    // Normal operators shouldn't force port lowering. We'll replace
    // the formal with the actual (even if it's a concat). This allows
    // other optimizations (concat rewrite, etc) more freedom in
    // choosing how the design should be re-written.
    return eNormal;
  }

private:
  NUModule *mModule;
  NUNetSet &mNoScalarizeNets;
};


Flatten::Flatten(NUDesign * design,
                 AtomicCache *str_cache,
                 NUNetRefFactory *netref_factory,
                 MsgContext *msg_ctx,
                 IODBNucleus * iodb,
                 ArgProc* args,
                 NUCostContext *design_cost_ctx,
                 UtOStream * trace_file,
                 SInt32 inline_limit,
                 bool flatten_only_leaves,
                 bool flatten_single_instances,
                 bool flatten_hier_ref_tasks,
                 bool early_flattening_pass,
                 bool debugging,
                 bool verbose,
                 UInt32 tiny_threshold,
                 UInt32 parent_size_limit,
                 FlattenStatistics *stats) :
  mDesign(design),
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mArgs(args),
  mDesignCostCtx(design_cost_ctx),
  mTraceFile(trace_file),
  mInlineLimit(inline_limit),
  mStatistics(stats),
  mFlattenOnlyLeaves(flatten_only_leaves),
  mFlattenSingleInstances(flatten_single_instances),
  mFlattenHierRefTasks(flatten_hier_ref_tasks),
  mEarlyFlatteningPass(early_flattening_pass),
  mDebugging(debugging),
  mVerbose(verbose),
  mTinyThreshold(tiny_threshold),
  mParentSizeLimit(parent_size_limit)
{
  mCosts = new NUCostContext;
  bool verbose_fold = args->getBoolValue(CRYPT("-verboseFold"));
  mFold = new Fold(netref_factory, args, msg_ctx, str_cache, iodb,
                   verbose_fold, eFoldBottomUp | eFoldKeepExprs);
}

Flatten::~Flatten()
{
  delete mFold;
  delete mCosts;
}


bool Flatten::module(NUModule * this_module, FlattenAnalyzer::Mode flattenMode)
{
  mCosts->calcModule(this_module, 0, false);

  // Perform the flattening operation on this module
  bool flattened = false;

  FlattenAnalyzer analyzer(mMsgContext, mDesignCostCtx, mCosts, this_module, flattenMode,
                           mInlineLimit, mFlattenOnlyLeaves, mFlattenSingleInstances, mFlattenHierRefTasks,
                           mTinyThreshold, mParentSizeLimit);

  NUModuleInstanceList instances;
  
  // create a copy of the instance list before beginning the
  // flattening process. this lets us remove instances as we go 
  // (and afterwards recalculate module cost)
  for ( NUModuleInstanceMultiLoop loop = this_module->loopInstances() ;
	!loop.atEnd() ;
	++loop ) {
    instances.push_back(*loop);
  }

  // queue up instances to remove after resolution
  NUModuleInstanceSet removed_instances;

  // if we delete instances, some elements in this list may point to
  // freed memory.
  NUCost* module_cost = mCosts->findCost(this_module);
  for ( NUModuleInstanceList::iterator iter = instances.begin();
	iter != instances.end();
	++iter ) {
    NUModuleInstance * one_instance = (*iter);
    NUModule * submodule = one_instance->getModule();

    FlattenAnalyzer::FlattenSize   size;
    FlattenAnalyzer::Status status = analyzer.qualified(submodule, &size);
    if (status == FlattenAnalyzer::eStatusSuccess) {
      FlattenModuleInstance instance(mDesign,
                                     this_module, 
                                     one_instance, 
                                     mStrCache, 
                                     mNetRefFactory, 
                                     mIODB, 
                                     mMsgContext,
                                     mFold,
                                     mEarlyFlatteningPass,
                                     mDebugging);
      status = instance.flatten();
    } else if (status == FlattenAnalyzer::eStatusError) {
      break; // an error occurred; allow no further processing.
    }

    // Dump once for the log file and once (conditionally) for the screen.
    dumpFlattening(*mTraceFile, this_module, one_instance, status, &size);
    if (mVerbose) {
      dumpFlattening(UtIO::cout(), this_module, one_instance, status, &size);
    }
    
    if (status==FlattenAnalyzer::eStatusSuccess) {
      flattened = true;

      mStatistics->child();
      
      // Recalculate the cost of this module. It is not cheap to
      // recompute the costs again because it becomes and N*N
      // operation. It is cheaper to add the sub module costs to the
      // total costs.
      NUCost* sub_cost = mCosts->findCost(submodule);
      module_cost->addCost(*sub_cost);
      removed_instances.insert(one_instance);
    }
  }

  // If any instances have been removed, then recompute the module instance
  // list and delete them.
  if (! removed_instances.empty()) {

    // Try to resolve hierrefs which have been exposed by flattening.
    //
    // We do this here instead of during the clone phase because it is easier.
    // By delaying until now, the local symbol table has been set up correctly,
    // so determining resolution is easy (we just need to do a symbol table
    // lookup).  Note that prior to May, 07, this was done in the loop above,
    // over all instances, and was n^2.  This causes a major compile bottleneck
    // in test/bugs/bug7283, using the e2th_s_05.f command file.
    FlattenResolveHierRefs resolver(this_module, mNetRefFactory, mFold);
    resolver.resolve();

    NUModuleInstanceList replacements;

    for ( NUModuleInstanceList::iterator iter = instances.begin();
          iter != instances.end();
          ++iter ) {
      NUModuleInstance * one_instance = (*iter);
      NUModuleInstanceSet::iterator p = removed_instances.find(one_instance);
      if (p == removed_instances.end()) {
        // not in the removed set, put it in the replacements
        replacements.push_back(one_instance);
      }
      else {
        // in the removed set.  delete it and prune the removal set
        delete one_instance;
        removed_instances.erase(p);
      }
    }

    // all the removed instances should now be erased from the set
    NU_ASSERT(removed_instances.empty(), this_module);
    this_module->replaceModuleAndDeclScopeInstances(replacements);
    // Some declaration scopes would have nothing left in them after 
    // instances inside them have been flattened into parent module.
    // Delete them and remove them from NUAliasBOM's right here. If 
    // this is not done, Fold will delete them and the freed pointers
    // will persist in the NUAliasBOM.

    // Get a list of such declaration scopes.
    NUNamedDeclarationScopeList emptyDeclScopes;
    this_module->getEmptyDeclScopes(emptyDeclScopes);
    // Remove them from the NUAliasBOM if they exist and delete them.
    for (NUNamedDeclarationScopeList::iterator itr = emptyDeclScopes.begin();
         itr != emptyDeclScopes.end(); ++itr) {
      NUNamedDeclarationScope* declScope = *itr;
      STBranchNode* node = 
        FlattenHierHelper::findBranchForDeclScope(declScope, this_module->getAliasDB());
      if (node != NULL) {
        NUAliasDataBOM* bom = NUAliasBOM::castBOM(node->getBOMData());
        if (bom) {
          bom->setScope(NULL);
        }
      }
      // Declaration scope removes itself from the parents list of declaration
      // scope in it's destructor.
      delete declScope;
    }
  } // if


  if (flattened) {
    mStatistics->parent();
  }

  // we used to recompute UD. we are now prior to UD, so don't do it.
  // However, we do need to recompute read/write information.
  MarkReadWriteNets marker(mIODB, mArgs, false);
  marker.module(this_module);

  return flattened;
}

void Flatten::dumpFlattening(UtOStream & out,
                             NUModule * this_module,
                             NUModuleInstance * instance, 
                             FlattenAnalyzer::Status status, 
                             FlattenAnalyzer::FlattenSize * size)
{
  NUModule * submodule = instance->getModule();
  out << "Flatten: ";

  const SourceLocator& loc = instance->getLoc();
  if ( loc.isTicProtected() ) {
    out << " instance <protected>";
  } else {
    out << "{" << loc.getFile() << ":" << loc.getLine() << "}";
    out << " instance " << instance->getName()->str();
  }

  out << " of module ";
  if ( submodule->getLoc().isTicProtected() ) {
    out << "<protected>";
  } else {
    out << submodule->getName()->str();
  }

  out << " into module ";
  if ( this_module->getLoc().isTicProtected() ) {
    out << "<protected>";
  } else {
    out << this_module->getName()->str();
  }
  
  if (status==FlattenAnalyzer::eStatusSuccess) {
    out << " (SUCCESS)" << UtIO::endl;
  } else {
    out << " (FAILURE)" << UtIO::endl;
  }

  out << "  --> ";
  switch(status) {
  case FlattenAnalyzer::eStatusSuccess:          out << "Successfully flattened.";                         break;
  case FlattenAnalyzer::eStatusForced: NU_ASSERT(0,instance); break; // unexpected exposure of internal state.
  case FlattenAnalyzer::eStatusLargeCandidates : out << "Both the parent and sub modules were too large."; break;
  case FlattenAnalyzer::eStatusParentGrowth :    out << "The parent module grew too large.";               break;
  case FlattenAnalyzer::eStatusNonLeaf:          out << "The sub module is not a leaf.";                   break;
  case FlattenAnalyzer::eStatusUnknownPort:      out << "Instance has an unhandled expression type as its port."; break;
  case FlattenAnalyzer::eStatusPortNoScalarize:  out << "Concat actual and formal is non-scalarizable.";   break;
  case FlattenAnalyzer::eStatusDirective:        out << "User directive disallows flattening.";            break;
  case FlattenAnalyzer::eStatusHierRefTasks:     out << "The submodule contains tasks with hierarchical references."; break;
  case FlattenAnalyzer::eStatusError:            out << "An error occurred.";                              break;
  case FlattenAnalyzer::eStatusUnknown:          out << "Unknown qualification failure.";                  break;
  }

  if (size) {
    out << " [parent=" << size->parent << " child=" << size->child << " comb=" << size->combined << "]";
  }
  out << UtIO::endl;
}

FlattenModuleInstance::FlattenModuleInstance(NUDesign * design,
                                             NUModule * parent,
                                             NUModuleInstance * instance,
                                             AtomicCache * string_cache,
                                             NUNetRefFactory *netref_factory,
                                             IODBNucleus * iodb,
                                             MsgContext * msg_context,
                                             Fold * fold,
                                             bool early_flattening_pass,
                                             bool debugging) :
  mDesign(design),
  mModule(parent),
  mInstance(instance),
  mStrCache(string_cache),
  mNetRefFactory(netref_factory),
  mIODB(iodb),
  mMsgContext(msg_context),
  mFold(fold),
  mEarlyFlatteningPass(early_flattening_pass),
  mDebugging(debugging)
{
  mSubmodule = instance->getModule();

  mHierHelper = new FlattenHierHelper(mModule, mInstance);
}


FlattenModuleInstance::~FlattenModuleInstance()
{
  delete mHierHelper;
}


FlattenAnalyzer::Status FlattenModuleInstance::flatten()
{
  // from instance, copy cont. assigns, blocks, functions, submodules, etc.
  // create module-scoped nets for all submodule-scoped nets
  FlattenAnalyzer::Status status = cloneModule();

  if (status == FlattenAnalyzer::eStatusSuccess) {
    // replace all references in the copied constructs
    rewriteConstructs();

    // make sure we have no lingering references to the submodule we
    // are inlining.
    inlineConsistency();

    // add all copied constructs to this module
    promoteConstructs();
  }

  return status;
}

FlattenAnalyzer::Status FlattenModuleInstance::cloneModule()
{
  // Populate a set of nets which should not be scalarized.  This will be used to
  // determine which complex ports we should allow and which should stop flattening.
  NUNetSet no_scalarize_nets;
  {
    FindNoScalarizeNetsCallback callback(mSubmodule, no_scalarize_nets);
    NUDesignWalker walker(callback, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.module(mSubmodule);
  }

  FlattenAnalyzer::Status status = addPortConnections(no_scalarize_nets);

  if (status != FlattenAnalyzer::eStatusSuccess) {
    destroyLocals();
    return status;
  }

  cloneLocals();

  cloneConstructs();

  return status;
}


void FlattenModuleInstance::cloneConstructs()
{
  // these rewrites need to be cached -- they're called by:
  // 1. themselves
  // 2. other constructs: assigns, instances, always, initials
  // we pass that along in the copy context, which tells
  // the copier how to name mangle.

  CopyContext copy_context(mModule,mStrCache,true,"flatlocal");

  addMemoriesToRewriteMap(copy_context);

  cloneTasks(copy_context);
    
  cloneInstances(copy_context);
    
  cloneAssigns(copy_context);
  cloneAlways(copy_context);
  cloneInitials(copy_context);

  cloneCModels(copy_context);
  cloneTriRegInits(copy_context);
  cloneEnabledDrivers(copy_context);

  // Copy the discovered locals back into our parent rewriter.
  mNetReplacements.insert(copy_context.mNetReplacements.begin(),
                          copy_context.mNetReplacements.end());
}


void FlattenModuleInstance::addMemoriesToRewriteMap(CopyContext & copy_context)
{
  // Add 2D nets to the copy context used while cloning the Nucleus
  // structures. We do this up-front because the memories need to be
  // known before we create copies of NUTempMemoryNet (declared within
  // NUBlock).

  // We do not add all nets to allow the correct TieNet rewriting.
  // \sa rewriteConstructs
  // \sa addInputPortConnection
  // \sa addOutputPortConnection
  for (NUNetReplacementMap::iterator iter = mNetReplacements.begin();
       iter != mNetReplacements.end();
       ++iter) {
    const NUNet * below = iter->first;
    NUNet * above = iter->second;

    if (below->is2DAnything()) {
      NU_ASSERT2(above->is2DAnything(),above,below);
      copy_context.mNetReplacements[below] = above;
    }
  }

  extractMemoriesFromMap<NULvalueReplacementMap>(copy_context, mLvalueReplacements);
  extractMemoriesFromMap<NUExprReplacementMap>(copy_context, mRvalueReplacements);
}


template<typename MapType> 
void FlattenModuleInstance::extractMemoriesFromMap(CopyContext & copy_context,
                                                   MapType & replacement_map)
{
  for (typename MapType::iterator iter = replacement_map.begin();
       iter != replacement_map.end();
       ++iter) {
    typename MapType::key_type    below = iter->first;
    typename MapType::mapped_type above = iter->second;
    INFO_ASSERT(above, "Replacement map entry is NULL");
    if (below->is2DAnything()) {
      NU_ASSERT2(above->isWholeIdentifier(),above,below);
      NUNet * above_net = above->getWholeIdentifier();
      copy_context.mNetReplacements[below] = above_net;
    }
  }
}

FlattenFixupSysTasks::Status
FlattenFixupSysTasks::operator()(Phase phase, NUOutputSysTask* sysTask)
{
  if (phase == ePre) {
    if (sysTask->hasHierarchicalName()) {
      mHierHelper->branchForInstanceOutSysTask(sysTask);
    }
  }

  return eNormal;
}

void FlattenModuleInstance::rewriteConstructs()
{
  // We need to mark $displays and their variants that have %m in a
  // format string with the original instance they came from. We do
  // this fixup as part of the tie net fixup below. This is needed for
  // tasks, always, and initials which can contain displays.
  FlattenFixupSysTasks fixupSysTasks(mHierHelper);

  // First, rewrite any tie-nets. Here, we want to replace any uses of
  // ports connected to tied nets, but not definitions of these nets.
  // See Bug 3012 for further details and an example.
  {
    NUNetReplacementMap         empty_net_replacements;
    NUTFReplacementMap          empty_tf_replacements;
    NUCModelReplacementMap      empty_cmodel_replacements;
    NUAlwaysBlockReplacementMap empty_always_replacements;
    REReplaceRvalues translator(empty_net_replacements,
                                mTieNetLvalueRHSReplacements,
                                mTieNetRvalueRHSReplacements,
                                empty_tf_replacements,
                                empty_cmodel_replacements,
                                empty_always_replacements);

    rewriteTasks(translator, &fixupSysTasks);
    rewriteAssigns(translator);
    rewriteInstances(translator);
    rewriteAlways(translator, &fixupSysTasks);
    rewriteInitials(translator, &fixupSysTasks);
    rewriteTriRegInits(translator);
  }

  // Now, rewrite all other constructs. Definitions of ports connected
  // to tied nets will be replaced by a new temporary, not the tied
  // net itself. This is because we want readers to see the tied
  // value, not any locally written value. Again, see Bug 3012 for
  // details and an example.
  {
    RewriteLeaves translator(mNetReplacements,
                             mLvalueReplacements,
                             mRvalueReplacements,
                             mTFReplacements,
                             mCModelReplacements,
                             mAlwaysReplacements,
                             mFold);
    rewriteTasks(translator, NULL);
    rewriteAssigns(translator);
    rewriteInstances(translator);
    rewriteAlways(translator, NULL);
    rewriteInitials(translator, NULL);
    rewriteTriRegInits(translator);
  }

}


void FlattenModuleInstance::cloneOneLocal(NUNet * local)
{
  if (not sNetDoClone(local)) {
    return;
  }

  // Make sure we do not see dataflow tokens here; we clone
  // all types below.
  NU_ASSERT(not local->isVirtualNet(),local);

  // Make sure we do not see hierrefs here; they are cloned independently.
  NU_ASSERT(not local->isHierRef(),local);

  NUNet * replacement = clone(local, mModule, mNetReplacements);
  // add to our list of locals
  mLocalNetList.push_back ( replacement );

  // keep a map so we can walk the other constructs and perform
  // in-place replacement.
  mNetReplacements[local] = replacement;

  rememberAlias(replacement,local);
}


void FlattenModuleInstance::cloneScopeLocals(NUScope * scope)
{
  for (NUNetLoop loop = scope->loopLocals();
       not loop.atEnd();
       ++loop) {
    cloneOneLocal(*loop);
  }

  for (NUNamedDeclarationScopeLoop loop = scope->loopDeclarationScopes();
       not loop.atEnd();
       ++loop) {
    cloneScopeLocals(*loop);
  }
}


void FlattenModuleInstance::cloneLocals ()
{
  cloneScopeLocals(mSubmodule);

  // Handle hierarchical references.
  cloneHierRefs();

  // Alias the data and control flow nets (tokens) in the child with
  // the nets for the parent.
  rememberAlias(mModule->getExtNet(), mInstance->getModule()->getExtNet());
  // alias the to be expanded OutputSysTask net
  rememberAlias(mModule->getOutputSysTaskNet(), mInstance->getModule()->getOutputSysTaskNet());

  rememberAlias(mModule->getControlFlowNet(), mInstance->getModule()->getControlFlowNet());
}

void FlattenModuleInstance::cloneHierRefs()
{
  for (NUNetLoop loop = mSubmodule->loopNetHierRefs();
       not loop.atEnd();
       ++loop) {
    cloneHierRef(*loop);
  }
}

NUNet* FlattenModuleInstance::findMatchingHierref(NUNet *net)
{
  NUNet *parent_net = 0;

  // Do a pairwise match against the parent's hierrefs.  
  findMatchingHierrefFromLoop(mModule->loopNetHierRefs(), &parent_net, net, true);

  // Do a pairwise match against other hierrefs from this flattening
  // attempt. This second search is necessary to resolve the following
  // style of hierrefs together upon flattening into 'parent':
  //     parent.sub1.sub2.sub3.net
  //     sub1.sub2.sub3.net
  // 
  // In the child module (sub1), we do not have enough information to
  // consider these the same hierref. When both are flattened into
  // 'parent', we can determine that they are the same hierref.
  //
  // Unfortunately, we have not yet populated the resolutions for
  // these new hierrefs. As a result, we cannot check their
  // consistency with matches.
  findMatchingHierrefFromLoop(NUNetLoop(mLocalNetHierRefList), &parent_net, net, false);

  return parent_net;
}


void FlattenModuleInstance::findMatchingHierrefFromLoop(NUNetLoop loop, NUNet ** parent_net, NUNet * net, bool do_consistency_check)
{
  // Yes, this is n-squared, but the number of hierrefs is usually
  // very small. If this is a performance problem, use a map.

  NUNetHierRef *net_hier_ref = net->getHierRef();

  for ( /*no init*/; not loop.atEnd(); ++loop) {
    NUNet *cur_net = *loop;
    NUNetHierRef *cur_net_hier_ref = cur_net->getHierRef();

    if (cur_net_hier_ref->sameResolutionAsChild(net_hier_ref, mInstance, do_consistency_check)) {
      // If already found a matching resolution, give an error, something
      // has gone wrong.
      if ((*parent_net) != 0) {
        UtString name1;
        UtString name2;
        UtString name3;
        net->compose(&name1, 0);
        (*parent_net)->compose(&name2, 0);
        cur_net->compose(&name3, 0);
        mMsgContext->AmbiguousFlattenedResolution(&mInstance->getLoc(), name1.c_str(), name2.c_str(), name3.c_str());
      } else {
        (*parent_net) = cur_net;
      }
    }
  }
}


NUNet* FlattenModuleInstance::findLocalResolution(NUNet *net)
{
  NUNetHierRef *net_hier_ref = net->getHierRef();

  // Localize the reference to the parent.
  AtomArray atomPath;
  net_hier_ref->localizeNameForParent(mModule, mInstance->getName(),
                                      &atomPath);

  STSymbolTableNode* node = mModule->resolveHierRefLocally(atomPath, 0);
  if (node != NULL)
    return NUNet::findStorageUnelab(node);
  else
    return NULL;
}

void FlattenModuleInstance::cloneHierRef(NUNet *net)
{
  NUNetHierRef *net_hier_ref = net->getHierRef();

  // First, see if this hierref resolves to a net in the parent.
  NUNet *parent_net = findLocalResolution(net);

  // If no match was found, see if there is already a matching hierref in the parent to use.
  if (not parent_net) {
    parent_net = findMatchingHierref(net);
  }

  // If no match was found, create a local hierarchical reference to cover it.
  if (not parent_net) {
    AtomArray new_net_name;
    net_hier_ref->localizeNameForParent(mModule, mInstance->getName(),
                                        &new_net_name);

    // assert early if we've create a duplicate
#ifdef CDB
    //    NU_ASSERT(!mModule->hasNetName(new_net_name), net);
#endif

    // Create the appropriate type of net.
    NUBitNetHierRef *bit_net_hier_ref = dynamic_cast<NUBitNetHierRef*>(net);
    NUVectorNetHierRef *vector_net_hier_ref = dynamic_cast<NUVectorNetHierRef*>(net);
    NUMemoryNetHierRef *memory_net_hier_ref = dynamic_cast<NUMemoryNetHierRef*>(net);
    if (bit_net_hier_ref) {
      parent_net = new NUBitNetHierRef(new_net_name,
                                       net->getFlags(),
                                       mModule,
                                       net->getLoc());
    } else if (vector_net_hier_ref) {
      parent_net = new NUVectorNetHierRef(new_net_name,
                                          new ConstantRange(*vector_net_hier_ref->getDeclaredRange()),
                                          net->getFlags(),
                                          vector_net_hier_ref->getVectorNetFlags (),
                                          mModule,
                                          net->getLoc());
    } else if (memory_net_hier_ref) {
      NUMemoryNetHierRef *mem = new NUMemoryNetHierRef(new_net_name,
                                          new ConstantRange(*memory_net_hier_ref->getWidthRange()),
                                          new ConstantRange(*memory_net_hier_ref->getDepthRange()),
                                          net->getFlags(),
                                          memory_net_hier_ref->getVectorNetFlags (),
                                          mModule,
                                          memory_net_hier_ref->isResynthesized(),
                                          net->getLoc());
      mem->copyRanges(memory_net_hier_ref);
      parent_net = mem;
    } else {
      NU_ASSERT("Bad hierarchical net reference type" == 0, net);
    }

    // Add to the list of new hier-ref nets for this module.
    mLocalNetHierRefList.push_back(parent_net);
  }

  // Set up the mappings so flattening will replace the child's net with the
  // parent's net correctly.
  mNetReplacements[net] = parent_net;
  rememberAlias(parent_net, net);
}

void FlattenModuleInstance::cloneInstances ( CopyContext & copy_context )
{
  for ( NUModuleInstanceMultiLoop loop = mSubmodule->loopInstances() ;
	!loop.atEnd();
	++loop ) {
    NUModuleInstance * instance = (*loop);
    mInstanceList.push_back ( clone(instance,copy_context) );
  }
}

void FlattenModuleInstance::cloneAssigns ( CopyContext & copy_context )
{
  for ( NUModule::ContAssignLoop loop = mSubmodule->loopContAssigns() ;
	!loop.atEnd() ;
	++loop ) {
    const NUContAssign * assign = (*loop);
    NUStmt * stmt = assign->copy(copy_context);
    NUContAssign * replacement = dynamic_cast<NUContAssign*>(stmt);
    mContAssignList.push_back ( replacement );
  }
}

void FlattenModuleInstance::cloneAlways ( CopyContext & copy_context )
{
  for ( NUModule::AlwaysBlockLoop loop = mSubmodule->loopAlwaysBlocks() ;
	!loop.atEnd() ;
	++loop ) {
    const NUAlwaysBlock * always = (*loop);
    NUAlwaysBlock *new_always = clone(always,copy_context);
    mAlwaysBlockList.push_back ( new_always);
    mAlwaysReplacements[always] = new_always;
  }
}

void FlattenModuleInstance::cloneInitials ( CopyContext & copy_context )
{
  for ( NUModule::InitialBlockLoop loop = mSubmodule->loopInitialBlocks() ;
	!loop.atEnd() ;
	++loop ) {
    const NUInitialBlock * initial = (*loop);
    mInitialBlockList.push_back ( clone(initial,copy_context) );
  }
}

void FlattenModuleInstance::cloneTasks ( CopyContext & copy_context )
{
  for ( NUTaskLoop loop = mSubmodule->loopTasks() ;
	!loop.atEnd();
	++loop ) {
    NUTask * task = (*loop);
    NUTask * replacement = clone(task,copy_context);
    mTaskVector.push_back ( replacement );
    mTFReplacements[task] = replacement;

    if (task->hasHierRef()) {
      cloneTaskHierRefs(task, replacement);
    }
  }
}

void FlattenModuleInstance::cloneCModels ( CopyContext & copy_context )
{
  for ( NUCModelLoop loop = mSubmodule->loopCModels();
	!loop.atEnd();
	++loop) {
    NUCModel * cmodel = (*loop);
    NUCModel * replacement = clone(cmodel, copy_context);
    mCModelVector.push_back ( replacement );
    mCModelReplacements[cmodel] = replacement;
  }
}

void FlattenModuleInstance::cloneTriRegInits ( CopyContext & copy_context )
{
  for ( NUModule::TriRegInitLoop loop = mSubmodule->loopTriRegInit();
	!loop.atEnd();
	++loop) {
    NUTriRegInit * init = (*loop);
    NUTriRegInit * replacement = clone(init, copy_context);
    mTriRegInitList.push_back(replacement);
  }
}

void FlattenModuleInstance::cloneEnabledDrivers ( CopyContext & copy_context )
{
  NUModule::ContEnabledDriverLoop loop;
  for (loop = mSubmodule->loopContEnabledDrivers();
       !loop.atEnd();
       ++loop) {
    NUEnabledDriver * init = (*loop);
    NUContAssign * replacement = clone(init, copy_context);
    mContAssignList.push_back(replacement);
  }
}

NUNet * FlattenModuleInstance::clone ( NUNet * original,
				       NUScope * scope,
				       NUNetReplacementMap & net_replacements,
				       bool preserve_true_name )
{
  StringAtom * name = original->getName();
  NetFlags flags(original->getFlags());

  if (not preserve_true_name) {
    name = scope->gensym("flatten", mInstance->getName()->str(), original);

    // Mark up-scope nets as temps -- we always have an alternate name.
    flags = NetFlags(flags|eTempNet);

    // Remove block-local flag -- the replacement is module-local.
    flags = NetFlags(flags&~eBlockLocalNet);
  }
  return original->clone(scope, net_replacements, name, flags);
}

NUModuleInstance * FlattenModuleInstance::clone ( const NUModuleInstance * original,
						  CopyContext & copy_context ) 
{
  UtString str;
  str << mInstance->getName()->str() << "_" << original->getName()->str();
  const char* cstr = str.c_str();
  StringAtom *name = mStrCache->intern(cstr, str.size());

  NUModuleInstance * cloned = new NUModuleInstance( name,
						    mModule,
						    original->getModule(),
						    mNetRefFactory,
						    original->getLoc() );
  NUPortConnectionList connections;
  original->getPortConnections(connections);

  NUPortConnectionVector convec;
  for ( NUPortConnectionList::iterator iter = connections.begin();
	iter != connections.end() ;
	++iter ) {
    NUPortConnection * connection = (*iter);
    NUPortConnection * conn_copy = connection->copy(copy_context);
    conn_copy->setModuleInstance( cloned );
    convec.push_back( conn_copy );
  }
  cloned->setPortConnections(convec);
  return cloned;
}

NUAlwaysBlock * FlattenModuleInstance::clone ( const NUAlwaysBlock * original,
					       CopyContext & copy_context )
{
  return original->clone(copy_context, true);
}


NUInitialBlock * FlattenModuleInstance::clone ( const NUInitialBlock * original,
						CopyContext & copy_context )
{
  return original->clone(copy_context);
}

NUTask * FlattenModuleInstance::clone ( NUTask * original,
					CopyContext & copy_context )
{
  STBranchNode * name_branch = mHierHelper->branchForInstanceTF(original);
                                                                     
  NUTask * task = new NUTask ( mModule,
			       name_branch,
			       mNetRefFactory,
			       original->getLoc(),
                               mIODB );

  NUAliasDataBOM * bom = NUAliasBOM::castBOM(name_branch->getBOMData());
  bom->setScope(task);

  NUTF::clone ( task, *original, copy_context, false );
  return task;
}


void FlattenModuleInstance::cloneTaskHierRefs(NUTask * original, NUTask * replacement)
{
  NU_ASSERT(original->hasHierRef(),original);

  for (NUTaskEnableLoop loop = original->loopHierRefTaskEnables();
       not loop.atEnd();
       ++loop) {
    NUTaskEnable * enable = (*loop);
    NU_ASSERT(enable->isHierRef(), enable);

    replacement->addHierRef(enable);
    enable->getHierRef()->addResolution(replacement);
  }
}


NUCModel * FlattenModuleInstance::clone(NUCModel * original,
                                        CopyContext &)
{
  NUCModelInterface* cmodelInterface = original->getCModelInterface();
  UInt32 variant = original->getVariant();

  // Record what the original instance name is for this c-model so
  // that it can be passed to the user's create function. This name is
  // a function of this instance and any originial instance
  // information in the instance to be cloned.
  const UtString* origName = original->getOrigName();
  UtString subName;
  subName << mInstance->getName()->str();
  if (origName != NULL)
    subName << "." << *origName;

  NUCModel * cmodel;
  cmodel =  new NUCModel(cmodelInterface, mModule, variant, &subName);

  for (NUCModel::ParamLoop loop = original->loopParams();
       not loop.atEnd();
       ++loop) {
    NUCModel::Param * param = (*loop);
    cmodel->addParam(*param);
  }
  return cmodel;
}

NUTriRegInit* FlattenModuleInstance::clone(NUTriRegInit *original,
					   CopyContext &copy_context)
{
  StringAtom *name = copy_context.mScope->newBlockName(copy_context.mStrCache,
						       original->getLoc());
  NUTriRegInit *init =
    new NUTriRegInit(name,
		     original->getLvalue()->copy(copy_context),
		     original->getLoc());
  return init;
}

NUContAssign* FlattenModuleInstance::clone(NUEnabledDriver *original,
					   CopyContext &copy_context)
{
  StringAtom *name = copy_context.mScope->newBlockName(copy_context.mStrCache,
						       original->getLoc());

  CopyContext ctx(0,0);
  UInt32 size = original->getLvalue()->getBitSize();
  UtString z_string(size, 'z');
  NUExpr *z_const = NUConst::createXZ(z_string,
                                      false,
                                      size,
                                      original->getLoc());
  NUExpr *rhs = new NUTernaryOp(NUOp::eTeCond,
				original->getEnable()->copy(ctx),
				original->getDriver()->copy(ctx),
				z_const,
				original->getLoc());
  NUContAssign *assign =
    new NUContAssign(name,
		     original->getLvalue()->copy(ctx),
		     rhs,
		     original->getLoc(),
		     original->getStrength());
  return assign;
}


FlattenAnalyzer::Status FlattenModuleInstance::addPortConnections(NUNetSet &no_scalarize_nets)
{
  FlattenAnalyzer::Status status = FlattenAnalyzer::eStatusSuccess;
  {
    NUPortConnectionInputList inputs;
    mInstance->getInputPortConnections(inputs);

    for ( NUPortConnectionInputList::iterator iter = inputs.begin();
	  (status == FlattenAnalyzer::eStatusSuccess) && iter != inputs.end();
	  ++iter ) {
      NUPortConnectionInput * input = (*iter);
      NUExpr * actual = input->getActual();
      NUNet  * formal = input->getFormal();

      status = addInputPortConnection (actual, formal, no_scalarize_nets);
    }
  }

  {
    NUPortConnectionOutputList outputs;
    mInstance->getOutputPortConnections(outputs);
    for ( NUPortConnectionOutputList::iterator iter = outputs.begin();
	  (status == FlattenAnalyzer::eStatusSuccess) && iter != outputs.end();
	  ++iter ) {
      NUPortConnectionOutput * output = (*iter);
      NUNet *    formal = output->getFormal();
      NULvalue * actual = output->getActual();

      status = addOutputPortConnection (actual, formal, no_scalarize_nets);
    }
  }

  {
    NUPortConnectionBidList bids;
    mInstance->getBidPortConnections(bids);
    for ( NUPortConnectionBidList::iterator iter = bids.begin();
	  (status == FlattenAnalyzer::eStatusSuccess) && iter != bids.end();
	  ++iter ) {
      NUPortConnectionBid * bid = (*iter);
      NUNet *    formal = bid->getFormal();
      NULvalue * actual = bid->getActual();

      status = addOutputPortConnection (actual, formal, no_scalarize_nets);
    }
  }

  return status;
}


FlattenAnalyzer::Status FlattenModuleInstance::qualifyConcatInputPortConnection(NUConcatOp *concat,
                                                                                NUNet *formal,
                                                                                NUNetSet &no_scalarize_nets)
{
  // Make sure formal is scalarizable
  if (no_scalarize_nets.find(formal) != no_scalarize_nets.end()) {
    return FlattenAnalyzer::eStatusPortNoScalarize;
  }

  // NOTE: if you change the qualification in this routine, you may also need to
  // change promoteNetFlagsConcatRvalue.

  if (concat->getRepeatCount() > 1) {
    return FlattenAnalyzer::eStatusUnknownPort;
  }

  // Make sure concat is simple enough.  For now, just handle concats
  // which are made of simple rvalues.
  FlattenAnalyzer::Status status = FlattenAnalyzer::eStatusSuccess;
  for (NUExprLoop loop = concat->loopExprs();
       (not loop.atEnd()) and (status == FlattenAnalyzer::eStatusSuccess);
       ++loop) {
    NUExpr *expr = *loop;
    switch (expr->getType()) {
    case NUExpr::eNUIdentRvalue:
    case NUExpr::eNUConstNoXZ:
    case NUExpr::eNUConstXZ:
      break;

    case NUExpr::eNUVarselRvalue: 
    {
      const NUVarselRvalue *bitsel = dynamic_cast<const NUVarselRvalue*>(expr);
      if (not bitsel->isConstIndex ()) {
        status = FlattenAnalyzer::eStatusUnknownPort;
      }
    }
   break;

    default:
      status = FlattenAnalyzer::eStatusUnknownPort;
      break;
    }
  }

  return status;
}


FlattenAnalyzer::Status FlattenModuleInstance::qualifyConcatOutputPortConnection(NUConcatLvalue *concat,
                                                                                 NUNet *formal,
                                                                                 NUNetSet &no_scalarize_nets)
{
  // Make sure formal is scalarizable
  if (no_scalarize_nets.find(formal) != no_scalarize_nets.end()) {
    return FlattenAnalyzer::eStatusPortNoScalarize;
  }

  // Make sure concat is simple enough.  For now, just handle concats
  // which are made of simple lvalues.
  FlattenAnalyzer::Status status = FlattenAnalyzer::eStatusSuccess;
  for (NULvalueLoop loop = concat->loopLvalues();
       (not loop.atEnd()) and (status == FlattenAnalyzer::eStatusSuccess);
       ++loop) {
    NULvalue *lvalue = *loop;
    switch (lvalue->getType()) {
    case eNUIdentLvalue:
      break;

    case eNUVarselLvalue: 
    {
      NUVarselLvalue *bitsel = dynamic_cast<NUVarselLvalue*>(lvalue);
      if (not bitsel->isConstIndex ()) {
        status = FlattenAnalyzer::eStatusUnknownPort;
      }
    }
    break;

    default:
      status = FlattenAnalyzer::eStatusUnknownPort;
      break;
    }
  }

  return status;
}


FlattenAnalyzer::Status FlattenModuleInstance::addInputPortConnection(NUExpr * actual,
									   NUNet * formal,
									   NUNetSet &no_scalarize_nets)
{
  FlattenAnalyzer::Status status = FlattenAnalyzer::eStatusUnknown;
  if (actual) {

    bool dimensions_match = checkDimensions(formal,actual);

    status = FlattenAnalyzer::eStatusSuccess;

    NUNet * actual_net = NULL;

    const NUIdentRvalue  * ident = dynamic_cast<const NUIdentRvalue*>(actual);
    const NUVarselRvalue * bit   = dynamic_cast<const NUVarselRvalue*>(actual);
    const NUMemselRvalue  * mem  = dynamic_cast<const NUMemselRvalue*>(actual);
    NUConcatOp * concat  = dynamic_cast<NUConcatOp*>(actual);
    const NUConst * constant = dynamic_cast<const NUConst*>(actual);
    const NUUnaryOp * unary = dynamic_cast<NUUnaryOp*>(actual);

    if (ident) {
      actual_net = ident->getIdent();
    } else if (bit) {
      if (!bit->isArraySelect())
        actual_net = bit->getIdentNet();
      // otherwise, we lower
    } else if (mem) {
      // pass. handle this when we lower.
    } else if (concat) {
      status = qualifyConcatInputPortConnection(concat, formal, no_scalarize_nets);
    } else if (constant) {
      // nothing else to do -- see 'lower_port' code below.
    } else if (unary) {
      bool bit_flip = unary->isIdentifierBitFlip();
      // Not a simple ~vec or !bit port.
      if (not bit_flip) {
        status = FlattenAnalyzer::eStatusUnknownPort;
      }
    } else {
      // ignore other expr types.
      status = FlattenAnalyzer::eStatusUnknownPort;
    }

    if ((status==FlattenAnalyzer::eStatusSuccess)) {
      bool lower_port = false;
      bool use_actual = true;
      bool generate_assignment = false;
      
      if ((not dimensions_match) or (mem) or (bit && bit->isArraySelect())) {
        lower_port = true;
        use_actual = false;
        generate_assignment = true;
      } else if (actual_net and actual_net->isInteger()) {
        // Insert an unsigned temporary between an integer and its
        // uses. This preserves the semantics of passing the integer
        // through a module boundary (just in case someone else
        // tries to infer signedness based on what's within the
        // expression).
        lower_port = true;
        use_actual = false;
        generate_assignment = true;
      } else if (actual_net and isTiedNet(actual_net)) {
        lower_port = true;
        use_actual = false;

        // Since this tied net is connected to an input, we want to
        // create an assignment to the lowered temporary. That way, it
        // will be properly initialized and the formal will have the
        // correct value.
        generate_assignment = true;

        // Remember that this formal was connected to a tied net. We
        // will replace readers with a reference to the tie net and
        // writers with a reference to the lowered temporary.
        mTieNetRvalueRHSReplacements[formal] = actual;

      } else if ((not actual_net) or (not actual->isWholeIdentifier())) {

        if (formal->isEdgeTrigger()) {
          // 1. If the input is tied to a constant and the input is
          //    used as an edge trigger, don't let the constant be
          //    directly replaced in the edge expr list. Instead,
          //    create a module-scoped temporary and assign the
          //    constant to this temporary.

          // 2. If the input is tied to a complex expression and this
          //    input is used as an edge trigger, don't let the
          //    expression be directly replaced in the edge expr list.
          //    Instead, create a module-scoped temporart. Clock
          //    aliasing & codegen expect NUIdentRvalue within
          //    NUEdgeExpr. [bug 1234]

          lower_port = true;
          use_actual = false;
          generate_assignment = true;

        } else if (mDebugging or formal->isProtected(mIODB)) {
          // Preserve all the names we can.
          lower_port = true;
          use_actual = false;
          generate_assignment = true;
        } else {
          // Create a lowered name, but leave it disconnected. This
          // makes sure that user names exist even thought they may
          // have been optimized out of the model.
          lower_port = true;
          use_actual = true;
          generate_assignment = false;
        }
      }

      if (mEarlyFlatteningPass and not use_actual) {
        // When early flattening, do not allow port lowering.
        return FlattenAnalyzer::eStatusUnknownPort;
      }
      
      if (lower_port) {
        NUNet * lowered_net = clone(formal, mModule,
                                    mNetReplacements);
        lowered_net->setFlags( NetFlags(lowered_net->getFlags() & ~ePortMask ) );
        mLocalNetList.push_back ( lowered_net );
        promoteNetFlags(formal,lowered_net);
        
        if (generate_assignment) {
          CopyContext copy_context(NULL,NULL);
          NUContAssign * assign = new NUContAssign(mModule->newBlockName(mStrCache,formal->getLoc()),
                                                   new NUIdentLvalue(lowered_net,
                                                                     formal->getLoc()),
                                                   actual->copy(copy_context),
                                                   formal->getLoc(),
                                                   eStrDrive);
          mContAssignList.push_back(assign);
        }
        
        if (not use_actual) {
          // keep a map so we can walk the other constructs and perform
          // in-place replacement.
          mNetReplacements[formal] = lowered_net;
        }
        
        rememberAlias(lowered_net,formal);
      } 
      
      if (use_actual) {
        if (actual_net) {
          if (actual->isWholeIdentifier()) {
            rememberAlias(actual_net,formal);
          }
          promoteNetFlags(formal,actual_net);
        } else if (concat) {
          promoteNetFlagsConcatRvalue(concat, formal);
        } else {
          // actual_net is not valid here only if we have a constant
          NU_ASSERT(constant or unary, actual);
        }
        
        mRvalueReplacements[formal] = actual;
      }
    }
    
  } else {
    // we've got a disconnected input; we need to create a new
    // up-level net.
    NUNet * actual_net = clone(formal, mModule,
                               mNetReplacements);
    actual_net->setFlags( NetFlags( actual_net->getFlags() & ~ePortMask ) );
    mLocalNetList.push_back ( actual_net );
    promoteNetFlags(formal,actual_net);
    
    // keep a map so we can walk the other constructs and perform
    // in-place replacement.
    mNetReplacements[formal] = actual_net;
    
    rememberAlias(actual_net,formal);
    
    status = FlattenAnalyzer::eStatusSuccess;
  }
  return status;
}

FlattenAnalyzer::Status FlattenModuleInstance::addOutputPortConnection(NULvalue * actual,
									    NUNet * formal,
									    NUNetSet &no_scalarize_nets)
{
  FlattenAnalyzer::Status status = FlattenAnalyzer::eStatusUnknown;
  if (actual) {

    bool dimensions_match = checkDimensions(formal,actual);

    status = FlattenAnalyzer::eStatusSuccess;

    NUNet * actual_net = NULL;

    NUIdentLvalue * ident = dynamic_cast<NUIdentLvalue*>(actual);
    NUVarselLvalue * bit = dynamic_cast<NUVarselLvalue*>(actual);
    NUMemselLvalue * mem = dynamic_cast<NUMemselLvalue*>(actual);
    NUConcatLvalue * concat  = dynamic_cast<NUConcatLvalue*>(actual);

    if (ident) { 
      actual_net = ident->getIdent();
    } else if (bit) {
      if (!bit->isArraySelect())
        actual_net = bit->getIdentNet();
      // otherwise we lower
    } else if (mem) {
      // pass. handle this when we lower.
    } else if (concat) {
      status = qualifyConcatOutputPortConnection(concat, formal, no_scalarize_nets);
    } else {
      status = FlattenAnalyzer::eStatusUnknownPort;
    }

    if (status==FlattenAnalyzer::eStatusSuccess) {

      bool lower_port = false;
      bool use_actual = true;
      bool generate_write_assignment = false;
      bool generate_read_assignment = false;

      if ((not dimensions_match) or (mem)) {
	lower_port = true;
	use_actual = false;
	generate_write_assignment = true;

      } else if (actual_net and isTiedNet(actual_net)) {
        lower_port = true;
        use_actual = false;

        // We do not want to create an assignment to the tie net. If
        // the tie net is connected to an inout, we do want to assign
        // the tie net to the inout.
        generate_write_assignment = false;
        generate_read_assignment = formal->isBid();

        // Remember that this formal was connected to a tied net. We
        // will replace readers with a reference to the tie net and
        // writers with a reference to the lowered temporary.
        mTieNetLvalueRHSReplacements[formal] = actual;

      } else if ((not actual_net) or (not actual->isWholeIdentifier())) {

        if (formal->isEdgeTrigger()) {
          // If the output is tied to an incomplete identifier and it is
          // used as a clock within the submodule, force a lowering
          // operation so we don't create edge expressions with
          // non-identifiers. [bug 2953]

          lower_port = true;
          use_actual = false;
          generate_write_assignment = true;

        } else if (formal->isProtected(mIODB)) {
          if (formal->isProtectedMutable(mIODB)) {
            if (formal->isBid()) {
              // Lowering an incomplete bidirect has the potential (bad)
              // side-effect of eliminating depositable bidirects. Port
              // coercion would solve all of these issues (assuming it
              // runs before flattening).

              // Generate an error message. Port coercion is called for.
              mMsgContext->ComplexBid(actual);
            } else {
              // We have a mutable output. Insert an intermediate
              // temporary, aliased to the flattened formal.

              lower_port = true;
              use_actual = false;
              generate_write_assignment = true;
            }
          } else {
            // The user has only requested observability. We can
            // accomplish this through a read assignment (the
            // flattened sub-module will use the actual, not the
            // temporary). This avoids any complex bidi situations.

            lower_port = true;
            use_actual = true;
            generate_read_assignment = true;
          }
        } else if (mDebugging) {
          if (formal->isBid()) {
            // If we have a bidirect, generate the temporary and read
            // assignment to allow visibility, but do not use that
            // temporary in the flattened sub-module. If we were to
            // generate a (strong) write assignment, it could
            // overwrite the values from other drivers.

            // Port coercion would avoid this issue (assuming it runs
            // before flattening).

            lower_port = true;
            use_actual = true;
            generate_read_assignment = true;

          } else {
            // Preserve all the names we can.
            lower_port = true;
            use_actual = false;
            generate_write_assignment = true;
          }
	} else {
          // Create a lowered name, but leave it disconnected. This
          // makes sure that user names exist even thought they may
          // have been optimized out of the model.
          lower_port = true;
          use_actual = true;
        }
      }

      if (mEarlyFlatteningPass and not use_actual) {
        // When early flattening, do not allow port lowering.
        return FlattenAnalyzer::eStatusUnknownPort;
      }
      
      if (lower_port) {
	NUNet * lowered_net = clone(formal, mModule,
				    mNetReplacements);
	lowered_net->setFlags( NetFlags( lowered_net->getFlags() & ~ePortMask ) );
	mLocalNetList.push_back ( lowered_net );
	promoteNetFlags(formal,lowered_net);

	if (generate_write_assignment) {
	  CopyContext copy_context(NULL,NULL);
	  NUContAssign * assign = new NUContAssign(mModule->newBlockName(mStrCache,formal->getLoc()),
						   actual->copy(copy_context),
						   new NUIdentRvalue(lowered_net,
								     formal->getLoc()),
						   formal->getLoc(),
						   eStrDrive);
	  mContAssignList.push_back(assign);
        }

        if (generate_read_assignment) {
          NUContAssign * assign = new NUContAssign(mModule->newBlockName(mStrCache,formal->getLoc()),
                                                   new NUIdentLvalue(lowered_net,
                                                                     formal->getLoc()),
                                                   actual->NURvalue(),
                                                   formal->getLoc(),
                                                   eStrDrive);
          mContAssignList.push_back(assign);
	}

	if (not use_actual) {
	  // keep a map so we can walk the other constructs and perform
	  // in-place replacement.
	  mNetReplacements[formal] = lowered_net;
	}

	rememberAlias(lowered_net,formal);
      }

      if (use_actual) {
	if (actual_net) {
	  if (actual->isWholeIdentifier()) {
	    rememberAlias(actual_net,formal);
	  }

	  promoteNetFlags(formal,actual_net);
	} else {
	  // actual_net is not valid only if we have a concat
	  // ASSERT(concat); -- in VHDL you can have a NUVarselLvalue here
	}
	mLvalueReplacements[formal] = actual;
      }
    }
  } else {
    // we've got a disconnected output/bid. create a new up-level net.
    NUNet * actual_net = clone(formal, mModule,
			       mNetReplacements);
    actual_net->setFlags( NetFlags( actual_net->getFlags() & ~ePortMask ) );
    mLocalNetList.push_back ( actual_net );
    promoteNetFlags(formal,actual_net);

    // keep a map so we can walk the other constructs and perform
    // in-place replacement.
    mNetReplacements[formal] = actual_net;

    rememberAlias(actual_net,formal);

    status = FlattenAnalyzer::eStatusSuccess;
  }
  return status;
}


bool FlattenModuleInstance::checkDimensions(const NUNet * formal, const NULvalue * actual)
{
  UInt32 formal_size = formal->getBitSize();
  UInt32 actual_size = actual->getBitSize();

  bool ok = (formal_size == actual_size);
  return ok;
}


bool FlattenModuleInstance::checkDimensions(const NUNet * formal, const NUExpr * actual)
{
  UInt32 formal_size = formal->getBitSize();
  UInt32 actual_size = actual->getBitSize();
  
  bool ok = (formal_size == actual_size);
  return ok;
}


void FlattenModuleInstance::promoteNetFlags(NUNet * under, NUNet * net)
{
  // mask out the flags we do not want to inheirit
  // Note we do not promote Z; this will be recomputed locally, it may
  // be different due to the flattening.
  NetFlags under_flags = under->getFlags();
  NetFlags masked_flags = NetFlags(under_flags &
                                   ~(ePortMask|eTempNet|eBlockLocalNet|eNonStaticNet
                                     |eDeclareMask|eTriWritten|ePrimaryZ));

  // if the submodule net has defined storage requirements, clear any implicit markings.
  NetFlags over_flags = net->getFlags();

  // add them to our uplevel net.
  NetFlags complete = NetFlags(over_flags|masked_flags);
  net->setFlags(complete);

  // Promote tri/pull declarations.
  if (under->isTrireg()) {
    net->putIsTrireg(true);
  }
  if (under->isTri1()) {
    net->putIsTri1(true);
  }
  if (under->isTri0()) {
    net->putIsTri0(true);
  }
  if (under->isPullUp()) {
    net->putIsPullUp(true);
  }
  if (under->isPullDown()) {
    net->putIsPullDown(true);
  }
  if (under->isWand()) {
    net->putIsWand(true);
  }
  if (under->isWor()) {
    net->putIsWor(true);
  }
  if (under->isTriand()) {
    net->putIsTriand(true);
  }
  if (under->isTrior()) {
    net->putIsTrior(true);
  }
}


void FlattenModuleInstance::promoteNetFlagsConcatRvalue(NUConcatOp *concat,
							NUNet *formal)
{
  NUNetSet nets;

  // NOTE: if dynamic bit selects ever are allowed in qualifyConcatInputPortConnection,
  // this becomes wrong.
  concat->getUses(&nets);

  for (NUNetSetIter iter = nets.begin(); iter != nets.end(); ++iter) {
    promoteNetFlags(formal, *iter);
  }
}


void FlattenModuleInstance::promoteNetFlagsConcatLvalue(NUConcatLvalue *concat,
							NUNet *formal)
{
  NUNetSet nets;
  concat->getDefs(&nets);

  for (NUNetSetIter iter = nets.begin(); iter != nets.end(); ++iter) {
    promoteNetFlags(formal, *iter);
  }
}


void FlattenModuleInstance::rememberAlias(NUNet * first, NUNet * second)
{
  mPendingAliases.insert(MasterToSlave::value_type(first,second));
}


void FlattenModuleInstance::promoteAliases()
{
  for (MasterToSlave::iterator iter = mPendingAliases.begin();
       iter != mPendingAliases.end();
       ++iter) {
    alias(iter->first,iter->second);
  }
}


void FlattenModuleInstance::alias(NUNet * first, NUNet * second)
{
  STAliasedLeafNode * first_leaf = first->getNameLeaf();

  // The second net is either in the child module being flattened
  // or in the parent module.
  // If it is in the child module, create  aleaf node for it in
  // the AliasDB of the parent module.
  STAliasedLeafNode * second_leaf = 0;
  if (second->getScope()->getModule() != first->getScope()->getModule()) {
    second_leaf = mHierHelper->leafForInstanceNet(second);
  } else {
    second_leaf = second->getNameLeaf();
  }

  // Use masters to check for temp nets; they should represent the best names.
  NUNet * first_master_net = first->getMaster();
  NUNet * second_master_net = second->getMaster();

  bool first_port  = first_master_net->isPrimaryPort();

  bool first_temp  = first_master_net->isTemp();
  bool second_temp = second_master_net->isTemp();

  // Prefer primary ports to non-ports
  if (first_port) {
    // if first is a primary port, it wins.
    first_leaf->linkAlias(second_leaf);
    // We do not check if the second is a port because
    // it is in the submodule -- it doesn't matter.
  } else if (first_temp and not second_temp) {
    // first is a temp and second is not; second wins.
    second_leaf->linkAlias(first_leaf);
  } else {
    // if neither are temp, second is temp, or both are temp, respect
    // call order: first wins.
    first_leaf->linkAlias(second_leaf);
  }

  // mark the up-level net for storage.
  if (first_leaf->getInternalStorage() != first_leaf)
    first_leaf->setThisStorage();

  fixupResolutions(first,second);
}

void FlattenModuleInstance::fixupResolutions(NUNet * first, NUNet * second)
{
  // If the replaced is a net which is referenced hierarchically, make
  // those references to point to the replacement.
  if (second->hasHierRef()) {
    // fixupResolution() may possibly modify the hier ref container in "second",
    // so first copy the container locally.
    NUNetVectorLoop loop = second->loopHierRefs();
    NUNetVector second_refs(loop.begin(), loop.end());
    for (NUNetVector::iterator iter = second_refs.begin();
         iter != second_refs.end();
         ++iter) {
      NUNet *referring_net = *iter;
      NUNetHierRef *referring_hier_ref = referring_net->getHierRef();
      bool use_new = referring_hier_ref->fixupResolution(second,
                                                         mInstance);
      if (use_new) {
        if (first->isHierRef()) {
          // If the hierarchical reference 'referring_net' can
          // potentially refer to another hierarchical reference
          // 'first', add all the resolutions from 'first' to
          // 'referring_net'. We will later attempt to minimize the
          // number of hierarchical references in
          // FlattenResolveHierRefs.
          NUNetHierRef * first_hier_ref = first->getHierRef();
          for (NUNetHierRef::NetVectorLoop loop = first_hier_ref->loopResolutions();
               not loop.atEnd();
               ++loop) {
            NUNet * resolution = *loop;
            referring_hier_ref->addResolution(resolution);
            resolution->addHierRef(referring_net);
          }
        } else {
          // If 'first' is not a hierarchical reference, connect it as
          // a new resolution.
          referring_hier_ref->addResolution(first);
          first->addHierRef(referring_net);
        }
      }
    }
  }

  // Set up the resolutions for the replacement.
  // Two cases:
  // 1) Replacement is locally resolved; use the computed resolution.
  // 2) Otherwise, propagate the resolutions from the replaced to the replacement.
  if (first->isHierRef() and second->isHierRef()) {
    NUNetHierRef *first_hier_ref = first->getHierRef();
    NUNetHierRef *second_hier_ref = second->getHierRef();
    NUNet *resolution = 0;
    if (first_hier_ref->isLocallyRelative(0, &resolution)) {
      first_hier_ref->addResolution(resolution);
      resolution->addHierRef(first);
    } else {
      for (NUNetHierRef::NetVectorLoop loop = second_hier_ref->loopResolutions();
           not loop.atEnd();
           ++loop) {
        NUNet * possible_resolution = (*loop);
        first_hier_ref->addResolution(possible_resolution);
        possible_resolution->addHierRef(first);
      }
    }
  }
}


FlattenHierHelper::FlattenHierHelper(NUModule * parent,
                                     NUModuleInstance * instance) :
  mModule(parent),
  mInstance(instance)
{
  mSubmodule = instance->getModule();
}


FlattenHierHelper::~FlattenHierHelper()
{
}


STAliasedLeafNode * FlattenHierHelper::leafForInstanceNet(NUNet * lower)
{
  STBranchNode * instance_branch = branchForInstance();

  STAliasedLeafNode * lower_master_leaf = lower->getNameLeaf()->getMaster();

  STAliasedLeafNode * instance_master_leaf = cloneLeafWithRoot(lower_master_leaf,instance_branch);
  for (STAliasedLeafNode * alias_submodule_leaf = lower_master_leaf->getAlias();
       alias_submodule_leaf != lower_master_leaf;
       alias_submodule_leaf = alias_submodule_leaf->getAlias()) {
    STAliasedLeafNode * alias_instance_leaf = cloneLeafWithRoot(alias_submodule_leaf,instance_branch);
    instance_master_leaf->linkAlias(alias_instance_leaf);
  }
  instance_master_leaf->setThisMaster(); // make sure master persists.
  return instance_master_leaf;
}


STBranchNode * FlattenHierHelper::branchForInstanceTF(NUTF * original)
{
  STBranchNode * original_branch = original->getNameBranch();
  STBranchNode * lowered_branch = branchForInstance(original_branch);

  // The NUScope object is incorrect; it points to the original task,
  // not the flattened version. NULL the NUScope object out, expecting
  // that our caller will update after T/F creation.
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(lowered_branch->getBOMData());
  bom->setScope(NULL);

  return lowered_branch;
}

void
FlattenHierHelper::branchForInstanceOutSysTask(NUOutputSysTask* outSysTask)
{
  STBranchNode* original = outSysTask->getNameBranch();
  STBranchNode* update = branchForInstance(original);
  outSysTask->putNameBranch(update);
}


STBranchNode*
FlattenHierHelper::branchForInstance(STBranchNode* original_branch)
{
  STBranchNode* instance_branch = branchForInstance();
  STBranchNode* lowered_branch;
  lowered_branch = cloneBranchWithRoot(original_branch, instance_branch);
  return lowered_branch;
}


STBranchNode * FlattenHierHelper::branchForInstance()
{
  STBranchNode * instance_branch = NULL;
  STSymbolTable * aliasDB = mModule->getAliasDB();
  STBranchNode* root = branchForInstanceScope(mInstance->getParent());
  STSymbolTableNode * instance_node = aliasDB->find(root, mInstance->getName());
  if (instance_node) {
    instance_branch = instance_node->castBranch();
    ST_ASSERT(instance_branch,instance_node);
  } else {
    instance_branch = aliasDB->createBranch(mInstance->getName(),root,mInstance->getSymtabIndex());
    NU_ASSERT(instance_branch,mInstance);
    NUAliasDataBOM * bom = NUAliasBOM::castBOM(instance_branch->getBOMData());
    bom->setScope(mSubmodule);
    bom->setIndex(mInstance->getSymtabIndex()); // steal the symtab index away from the instance (we're removing it)
  }

  return instance_branch;
}

STBranchNode* FlattenHierHelper::findBranchForDeclScope(NUNamedDeclarationScope* declScope,
                                                        STSymbolTable* symtab)
{
  STBranchNode* parentBranch = NULL;
  NUScope* parentScope = declScope->getParentScope();
  if (parentScope->getScopeType() == NUScope::eNamedDeclarationScope) {
    NUNamedDeclarationScope* parentDeclScope =
      dynamic_cast<NUNamedDeclarationScope*>(parentScope);
    parentBranch = findBranchForDeclScope(parentDeclScope, symtab);
  }
  STSymbolTableNode* declScopeNode = symtab->find(parentBranch, declScope->getName());
  if (declScopeNode) {
    return declScopeNode->castBranch();
  }
  return NULL;
}

STBranchNode* FlattenHierHelper::branchForInstanceScope(NUScope* scope)
{
  STBranchNode* declScopeBranch = NULL;
  if (scope->getScopeType() == NUScope::eModule) {
    return NULL;
  }

  NU_ASSERT(scope->getScopeType() == NUScope::eNamedDeclarationScope, mInstance);
  STBranchNode* root = branchForInstanceScope(scope->getParentScope());
  NUNamedDeclarationScope* declScope = 
    dynamic_cast<NUNamedDeclarationScope*>(scope);

  STSymbolTable * aliasDB = mModule->getAliasDB();
  STSymbolTableNode* declScopeNode = aliasDB->find(root, declScope->getName());
  if (declScopeNode) {
    declScopeBranch = declScopeNode->castBranch();
  } else {
    declScopeBranch = aliasDB->createBranch(declScope->getName(), root,
                                            declScope->getSymtabIndex());
    NUAliasDataBOM * bom = NUAliasBOM::castBOM(declScopeBranch->getBOMData());
    bom->setScope(declScope);
    bom->setIndex(declScope->getSymtabIndex());
  }
  NU_ASSERT(declScopeBranch, declScope);
  return declScopeBranch;
}

STAliasedLeafNode * FlattenHierHelper::createLeafWithRoot(const STAliasedLeafNode * source, STBranchNode * root)
{
  STSymbolTable * aliasDB = mModule->getAliasDB();
  STBranchNode * parent = cloneBranchWithRoot(source->getParent(),root);

  STAliasedLeafNode * leaf = NULL;
  STSymbolTableNode * existing = aliasDB->find(parent,source->strObject());
  if (existing) {
    leaf = existing->castLeaf();
    ST_ASSERT(leaf,existing);
  } else {
    NUAliasDataBOM * source_bom = NUAliasBOM::castBOM(source->getBOMData());
    ST_ASSERT(source_bom->hasValidNet(), source);
    leaf = aliasDB->createLeaf(source->strObject(),parent,source_bom->getIndex());
    ST_ASSERT(leaf,source);
  }
  return leaf;
}


STAliasedLeafNode * FlattenHierHelper::cloneLeafWithRoot(const STAliasedLeafNode * source, STBranchNode * root)
{
  STAliasedLeafNode * leaf = createLeafWithRoot(source, root);

  // make sure the BOM data is consistent.
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(leaf->getBOMData());
  NUAliasDataBOM * source_bom = NUAliasBOM::castBOM(source->getBOMData());
  ST_ASSERT(source_bom->hasValidNet(), source);
  bom->setNet(source_bom->getNet());
  bom->setIndex(source_bom->getIndex());

  // translate the reconstruction data, if we have any.
  cloneReconData(source,root);
  return leaf;
}

//! An object to transform a replaced net to the flattened hierarchy.
/*!
  Called from FlattenTransformWalker
*/
class BackPointerWalker : public CarbonTransformWalker
{
public:
  CARBONMEM_OVERRIDES
  
  //! constructor
  BackPointerWalker(NUAliasBOM * bomManager,
                    FlattenHierHelper * hierHelper,
                    STBranchNode * hier)
    :mBOMManager(bomManager),
     mESFactory(bomManager->getExprFactory()),
     mHierHelper(hierHelper),
     mHier(hier)
  {}
 
  //! virtual destructor
  virtual ~BackPointerWalker() {}
     
  //! Back pointers can be partsels
  CarbonExpr* transformPartsel(CarbonExpr* expr, CarbonPartsel* partselOrig) {
    return mESFactory->createPartsel(expr,
                                     partselOrig->getRange(),
                                     expr->getBitSize(),
                                     expr->isSigned());
  }
     
  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformUnaryOp(CarbonExpr*, CarbonUnaryOp*) {
    INFO_ASSERT(0,"transformUnaryOp is not yet implemented");
    return NULL;
  }
 
  //! Back pointers can be bit sels
  CarbonExpr* transformBinaryOp(CarbonExpr* expr1, CarbonExpr* expr2,
                                CarbonBinaryOp* binaryOrig) {
    CE_ASSERT(binaryOrig->getType() == CarbonExpr::eBiBitSel, binaryOrig);
    return mESFactory->createBinaryOp(binaryOrig->getType(),
                                      expr1, expr2,
                                      binaryOrig->getBitSize(),
                                      binaryOrig->isSigned(), false);
  }
 
  //! NYI. Back pointers can only be partsels, bitsels, idents
  CarbonExpr* transformTernaryOp(CarbonExpr* , CarbonExpr* , 
                                 CarbonExpr* , CarbonTernaryOp*) {
    INFO_ASSERT(0,"transformTernaryOp is not yet implemented");
    return NULL;
  }
 
  //! NYI. Back pointers can only be partsels, bitsels, idents
  CarbonExpr* transformEdge(CarbonExpr*, CarbonEdge*) {
    INFO_ASSERT(0,"transformEdge is not yet implemented");
    return NULL;
  }
 
  //! NYI. Back pointers can only be partsels, bitsels, idents
  CarbonExpr* transformConcatOp(CarbonExprVector * exprVec, 
                                UInt32 repeatCount,
                                CarbonConcatOp * concatOrig) {
    return mESFactory->createConcatOp(exprVec, repeatCount,
                                      concatOrig->getBitSize(),
                                      concatOrig->isSigned());
  }
 
  //! Rewrite an identifier in terms of parent-module symbol table nodes.
  CarbonExpr* transformIdent(CarbonIdent* ident) {
    SymTabIdent * local_ident = ident->castSymTabIdent();
    CE_ASSERT(local_ident, ident);
    const STAliasedLeafNode * local_leaf = local_ident->getNode();
    CE_ASSERT(local_leaf,local_ident);
 
 
    // find the flattened entry in the symbol table.
    STAliasedLeafNode * flat_leaf = mHierHelper->createLeafWithRoot(local_leaf, mHier);
 
    // create an identifier based on this name.
    bool added = false;
    CarbonIdent * my_flat_ident   = new SymTabIdent(flat_leaf,
                                                    local_ident->getBitSize());
    ConstantRange declaredRange;
    if (local_ident->getDeclaredRange(&declaredRange))
      my_flat_ident->putDeclaredRange(&declaredRange);
    
    CarbonIdent * flat_ident = mESFactory->createIdent(my_flat_ident, added);
    if (not added) {
      delete my_flat_ident;
    }
    
    return flat_ident;
  }  
 
private:
  //! The unelaborated BOM manager.
  NUAliasBOM * mBOMManager;
 
  //! The reconstruction expression factory
  ESFactory * mESFactory;
 
  //! Helper interface for translating between sub-module and parent-module symbol table entries.
  FlattenHierHelper * mHierHelper;
 
  //! The root within the parent module for all transformations (corresponds to the submodule instance name).
  STBranchNode * mHier;
  
};

//! A class to translate a CarbonExpr between nodes in two unelaborated symbol tables.
/*!
 *
 * If reconstruction data contains the following relationship:
 *
 \verbatim
 'io' => { 'io_2_2', 'io_1_1', 'io_0_0' }
 \endverbatim
 * 
 * This reconstruction data will be rewritten during flattening in
 * terms of parent-module symbol table nodes:
 *
 \verbatim
 'sub.io' => { 'sub.io_2_2', 'sub.io_1_1', 'sub.io_0_0' }
 \endverbatim
 *
 * \sa ElaborationTransformWalker
 */
class FlattenTransformWalker : public CarbonTransformWalker
{
public:
  //! Constructor
  FlattenTransformWalker(NUAliasBOM * bomManager,
                         FlattenHierHelper * hierHelper,
                         STBranchNode * hier) :
    mBOMManager(bomManager),
    mESFactory(bomManager->getExprFactory()),
    mHierHelper(hierHelper),
    mHier(hier)
  {}

  //! Destructor
  ~FlattenTransformWalker() {}

  CarbonExpr* transformConst(CarbonConst* expr)
  {
    SInt64 val;
    expr->getLL(&val);
    return mESFactory->createConst(val,
                                   expr->getBitSize(), 
                                   expr->isSigned());
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformPartsel(CarbonExpr* expr, CarbonPartsel* partselOrig) {
    return mESFactory->createPartsel(expr,
                                     partselOrig->getRange(),
                                     expr->getBitSize(),
                                     expr->isSigned());
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformUnaryOp(CarbonExpr*, CarbonUnaryOp*) {
    INFO_ASSERT(0,"transformUnaryOp is not yet implemented");
    return NULL;
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformBinaryOp(CarbonExpr* one , CarbonExpr* two, 
                                CarbonBinaryOp* orig) {
    
    return mESFactory->createBinaryOp(orig->getType(), one, two, 
                                      orig->getBitSize(), orig->isSigned(), false);
    
  }
  
  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformTernaryOp(CarbonExpr* , CarbonExpr* , 
                                 CarbonExpr* , CarbonTernaryOp*) {
    INFO_ASSERT(0,"transformTernaryOp is not yet implemented");
    return NULL;
  }

  //! NYI. Reconstruction data currently includes concats and idents.
  CarbonExpr* transformEdge(CarbonExpr*, CarbonEdge*) {
    INFO_ASSERT(0,"transformEdge is not yet implemented");
    return NULL;
  }

  //! Create a new concat based on transformed components.
  CarbonExpr* transformConcatOp(CarbonExprVector * exprVec, 
                                UInt32 repeatCount,
                                CarbonConcatOp * concatOrig) {
    return mESFactory->createConcatOp(exprVec, repeatCount,
                                      concatOrig->getBitSize(),
                                      concatOrig->isSigned());
  }

  //! Rewrite an identifier in terms of parent-module symbol table nodes.
  CarbonExpr* transformIdent(CarbonIdent* ident) {
    SymTabIdent * local_ident = ident->castSymTabIdent();
    CE_ASSERT(local_ident,ident);
    const STAliasedLeafNode * local_leaf = local_ident->getNode();
    CE_ASSERT(local_leaf,local_ident);
    SymTabIdentBP * local_ident_bp = local_ident->castSymTabIdentBP();

    // find the flattened entry in the symbol table.
    STAliasedLeafNode * flat_leaf = mHierHelper->createLeafWithRoot(local_leaf, mHier);

    CarbonExpr* flattenedBP = NULL;

    if (local_ident_bp)
    {
      BackPointerWalker bpWalker(mBOMManager, mHierHelper, mHier);
      bpWalker.visitExpr(local_ident_bp->getBackPointer());
      flattenedBP = bpWalker.getResult();
    }

    // create an identifier based on this name.
    bool added = false;
    CarbonIdent * my_flat_ident = NULL;
    if (flattenedBP)
    {
      my_flat_ident   = new SymTabIdentBP(flat_leaf,
                                          local_ident->getBitSize(),
                                          flattenedBP);
    }
    else
    {
      my_flat_ident   = new SymTabIdent(flat_leaf,
                                        local_ident->getBitSize());

      // This only needs to be done for the backpointer identifiers
      // which are SymTabIdents not SymTabIdentBPs
      ConstantRange declaredRange;
      if (local_ident->getDeclaredRange(&declaredRange))
        my_flat_ident->putDeclaredRange(&declaredRange);
    }

    CarbonIdent * flat_ident = mESFactory->createIdent(my_flat_ident, added);
    if (not added) {
      delete my_flat_ident;
    }
    return flat_ident;
  }  

private:

  //! The unelaborated BOM manager.
  NUAliasBOM * mBOMManager;

  //! The reconstruction expression factory
  ESFactory * mESFactory;

  //! Helper interface for translating between sub-module and parent-module symbol table entries.
  FlattenHierHelper * mHierHelper;

  //! The root within the parent module for all transformations (corresponds to the submodule instance name).
  STBranchNode * mHier;
};


void FlattenHierHelper::cloneReconData(const STAliasedLeafNode * source_leaf, STBranchNode * root)
{
  NUAliasDataBOM * source_bom = NUAliasBOM::castBOM(source_leaf->getBOMData());
  CarbonIdent * source_ident = source_bom->getIdent();
  if (not source_ident) {
    return;
  }

  STSymbolTable * aliasDB = mModule->getAliasDB();
  NUAliasBOM * bom_manager = dynamic_cast<NUAliasBOM*>(aliasDB->getFieldBOM());
  NU_ASSERT(bom_manager,mModule);
  FlattenTransformWalker flattenExprWalker(bom_manager,this,root);

  // first, rewrite the identifier in terms of the flattened hierarchy.
  flattenExprWalker.visitIdent(source_ident);
  CarbonExpr * target_ident_expr = flattenExprWalker.getResult();
  ST_ASSERT(target_ident_expr,source_leaf);

  SymTabIdent * target_ident = target_ident_expr->castSymTabIdent();
  CE_ASSERT(target_ident,target_ident_expr);

  const STAliasedLeafNode * target_leaf = target_ident->getNode();
  NUAliasDataBOM* target_bom = NUAliasBOM::castBOM(target_leaf->getBOMData());

  // Save the identifier using hierarchy.
  target_bom->putIdent(target_ident);

  // if there is a mapped expression, translate that expression to
  // the hierarchical equivalent.
  CarbonExpr * source_expr = bom_manager->getExpr(source_ident);
  if (source_expr) {
    flattenExprWalker.visitExpr(source_expr);
    CarbonExpr * target_expr = flattenExprWalker.getResult();
    INFO_ASSERT(target_expr, "Failed to translate flattened expression");

    bom_manager->mapExpr(target_ident, target_expr);
  }
}


STBranchNode * FlattenHierHelper::cloneBranchWithRoot(const STBranchNode * source, STBranchNode * root)
{
  if (source) {
    STBranchNode * parent = cloneBranchWithRoot(source->getParent(),root);
    STSymbolTable * aliasDB = mModule->getAliasDB();

    STBranchNode * branch = NULL;
    STSymbolTableNode * existing = aliasDB->find(parent,source->strObject());
    if (existing) {
      branch = existing->castBranch();
      ST_ASSERT(branch,existing);
    } else {
      NUAliasDataBOM * source_bom = NUAliasBOM::castBOM(source->getBOMData());
      branch = aliasDB->createBranch(source->strObject(),parent,source_bom->getIndex());
      ST_ASSERT(branch,source);

      // make sure the BOM data is consistent.
      NUAliasDataBOM * bom = NUAliasBOM::castBOM(branch->getBOMData());
      bom->setScope(source_bom->getScope());
      bom->setIndex(source_bom->getIndex());
    }

    return branch;
  } else {
    return root;
  }
}


void FlattenModuleInstance::rewriteTasks(RewriteLeaves & translator,
                                         FlattenFixupSysTasks* fixupSysTasks)
{
  for ( NUTaskVector::iterator iter = mTaskVector.begin() ;
	iter!=mTaskVector.end() ;
	++iter ) {
    NUTask * task = (*iter);
    task->replaceLeaves(translator);

    if (fixupSysTasks != NULL) {
      NUDesignWalker walker(*fixupSysTasks, false);
      walker.putWalkTasksFromTaskEnables(false);
      walker.task(task);
    }
  }
}

void FlattenModuleInstance::rewriteAssigns(RewriteLeaves & translator)
{
  for ( NUContAssignList::iterator iter = mContAssignList.begin() ;
	iter!=mContAssignList.end() ;
	++iter ) {
    NUContAssign * assign = (*iter);
    assign->replaceLeaves(translator);
  }
}

void FlattenModuleInstance::rewriteInstances(RewriteLeaves & translator)
{
  for ( NUModuleInstanceList::iterator iter = mInstanceList.begin() ;
	iter!=mInstanceList.end() ;
	++iter ) {
    NUModuleInstance * instance = (*iter);
    instance->replaceLeaves(translator);
  }
}

void FlattenModuleInstance::rewriteAlways(RewriteLeaves & translator,
                                          FlattenFixupSysTasks* fixupSysTasks)
{
  for ( NUAlwaysBlockList::iterator iter = mAlwaysBlockList.begin() ;
	iter!=mAlwaysBlockList.end() ;
	++iter ) {
    NUAlwaysBlock * always = (*iter);
    always->replaceLeaves(translator);
    
    // This is another location wear the sync reset may be replaced with
    // a constant.  Remove the constant to prevent downstream difficulties
    // when processing the sync reset.
    always->removeConstantSyncReset();

    if (fixupSysTasks != NULL) {
      NUDesignWalker walker(*fixupSysTasks, false);
      walker.putWalkTasksFromTaskEnables(false);
      walker.alwaysBlock(always);
    }
  }
}

void 
FlattenModuleInstance::rewriteInitials(RewriteLeaves & translator,
                                       FlattenFixupSysTasks* fixupSysTasks)
{
  for ( NUInitialBlockList::iterator iter = mInitialBlockList.begin() ;
	iter!=mInitialBlockList.end() ;
	++iter ) {
    NUInitialBlock * initial = (*iter);
    initial->replaceLeaves(translator);

    if (fixupSysTasks != NULL) {
      NUDesignWalker walker(*fixupSysTasks, false);
      walker.putWalkTasksFromTaskEnables(false);
      walker.initialBlock(initial);
    }
  }
}

void FlattenModuleInstance::rewriteTriRegInits(RewriteLeaves & translator)
{
  for ( NUTriRegInitList::iterator iter = mTriRegInitList.begin() ;
	iter!=mTriRegInitList.end() ;
	++iter ) {
    NUTriRegInit * initial = (*iter);
    initial->replaceLeaves(translator);
  }
}

void FlattenModuleInstance::inlineConsistency()
{
  // review all constructs we are about to promote and make sure that
  // nothing has a scope pointing into our submodule.

  LeafConsistency leafChecker(mModule,mInstance);

  localConsistency();
  taskConsistency();
  instanceConsistency(leafChecker);
  assignConsistency(leafChecker);
  alwaysConsistency(leafChecker);
  initialConsistency(leafChecker);
}


void FlattenModuleInstance::localConsistency()
{
  for (NUNetList::iterator iter = mLocalNetList.begin();
       iter != mLocalNetList.end();
       ++iter) {
    NUNet * net = (*iter);
    NU_ASSERT( net->getScope() != mSubmodule, net );
  }
  for (NUNetList::iterator iter = mLocalNetHierRefList.begin();
       iter != mLocalNetHierRefList.end();
       ++iter) {
    NUNet * net = (*iter);
    NU_ASSERT( net->getScope() != mSubmodule, net );
  }
}


void FlattenModuleInstance::taskConsistency()
{
  for ( NUTaskVector::iterator iter = mTaskVector.begin() ;
	iter != mTaskVector.end() ;
	++iter ) {
    NUTask * task = (*iter);
    // we probably should double-check the internals of tasks.
    NU_ASSERT(task->getParentModule() != mSubmodule, task);
  }
}


void FlattenModuleInstance::instanceConsistency(LeafConsistency & consistency)
{
  for (NUModuleInstanceList::iterator iter = mInstanceList.begin();
       iter != mInstanceList.end();
       ++iter) {
    NUModuleInstance * instance = (*iter);
    NU_ASSERT(instance->getParent() != mSubmodule, instance);
    instance->replaceLeaves(consistency);
  }
}


void FlattenModuleInstance::assignConsistency(LeafConsistency & consistency)
{
  for (NUContAssignList::iterator iter = mContAssignList.begin();
       iter != mContAssignList.end();
       ++iter) {
    NUContAssign * assign = (*iter);
    assign->replaceLeaves(consistency);
  }
}


void FlattenModuleInstance::alwaysConsistency(LeafConsistency & consistency)
{
  for ( NUAlwaysBlockList::iterator iter = mAlwaysBlockList.begin() ;
	iter!=mAlwaysBlockList.end() ;
	++iter ) {
    NUAlwaysBlock * always = (*iter);
    NU_ASSERT(always->getBlock()->getParentModule() != mSubmodule, always);
    always->replaceLeaves(consistency);
  }
}


void FlattenModuleInstance::initialConsistency(LeafConsistency & consistency)
{
  for (NUInitialBlockList::iterator iter = mInitialBlockList.begin();
       iter != mInitialBlockList.end();
       ++iter ) {
    NUInitialBlock * initial = (*iter);
    NU_ASSERT(initial->getBlock()->getParentModule() != mSubmodule, initial);
    initial->replaceLeaves(consistency);
  }
}


void FlattenModuleInstance::promoteConstructs()
{
  promoteLocals();
  promoteAliases();
  promoteTasks();
  promoteInstances();
  promoteAssigns();
  promoteAlways();
  promoteInitials();
  promoteCModels();
  promoteTriRegInits();
}


void FlattenModuleInstance::destroyLocals()
{
  // Before we delete any nets, clear out the Fold context's expression
  // factory, which may contain IdentRvalues that point to those nets,
  // causing subsequent users of that factory to crash doing lookups
  if (! mLocalNetList.empty() || ! mLocalNetHierRefList.empty()) {
    mFold->clearExprFactory();
  }

  for (NUNetList::iterator iter = mLocalNetList.begin();
       iter != mLocalNetList.end();
       ++iter) {
    NUNet * net = (*iter);
    mNetRefFactory->erase(net);
    delete net;
  }
  for (NUNetList::iterator iter = mLocalNetHierRefList.begin();
       iter != mLocalNetHierRefList.end();
       ++iter) {
    NUNet * net = (*iter);
    mNetRefFactory->erase(net);
    delete net;
  }
}

void FlattenModuleInstance::promoteLocals()
{
  for (NUNetList::iterator iter = mLocalNetList.begin();
       iter != mLocalNetList.end();
       ++iter) {
    NUNet *net = *iter;
    NU_ASSERT(not net->isHierRef(),net);
    mModule->addLocal((*iter));
  }
  for (NUNetList::iterator iter = mLocalNetHierRefList.begin();
       iter != mLocalNetHierRefList.end();
       ++iter) {
    NUNet *net = *iter;
    NU_ASSERT(net->isHierRef(),net);
    mModule->addNetHierRef(net);
  }
}

void FlattenModuleInstance::promoteTasks()
{
  for ( NUTaskVector::iterator iter = mTaskVector.begin() ;
	iter != mTaskVector.end() ;
	++iter ) {
    mModule->addTask((*iter));
  }
}

void FlattenModuleInstance::promoteInstances()
{
  for (NUModuleInstanceList::iterator iter = mInstanceList.begin();
       iter != mInstanceList.end();
       ++iter) {
    {
      // this check was added when  the enableLegacyGenerateHierarchyNames was added, if that switch
      // is used then this assert cannot fire because parent is always a module, if the alternative
      // switch -disableLegacyGenerateHierarchyNames is used then this assert is still valid and if it
      // fires then it protects the caller from trying to use this promoteInstances when it has not
      // been properly implemented
      NUModuleInstance* inst = (*iter);
      NUScope* parent = inst->getParent();
      NUModule* parent_module = inst->getParentModule();
      NU_ASSERT((parent == parent_module),inst); // this code does not yet handle instances declared within namedDeclarationScopes
    }
    mModule->addModuleInstance((*iter));
  }
}

void FlattenModuleInstance::promoteAssigns()
{
  for (NUContAssignList::iterator iter = mContAssignList.begin();
       iter != mContAssignList.end();
       ++iter) {
    mModule->addContAssign((*iter));
  }
}

void FlattenModuleInstance::promoteAlways()
{
  for (NUAlwaysBlockList::iterator iter = mAlwaysBlockList.begin();
       iter != mAlwaysBlockList.end();
       ++iter) {
    mModule->addAlwaysBlock((*iter));
  }
}

void FlattenModuleInstance::promoteInitials()
{
  for (NUInitialBlockList::iterator iter = mInitialBlockList.begin();
       iter != mInitialBlockList.end();
       ++iter ) {
    mModule->addInitialBlock((*iter));
  }
}

void FlattenModuleInstance::promoteCModels()
{
  for ( NUCModelVectorIter iter = mCModelVector.begin();
	iter != mCModelVector.end();
	++iter ) {
    mModule->addCModel((*iter));
  }
}

void FlattenModuleInstance::promoteTriRegInits()
{
  for ( NUTriRegInitListIter iter = mTriRegInitList.begin();
	iter != mTriRegInitList.end();
	++iter ) {
    mModule->addTriRegInit((*iter));
  }
}

bool FlattenModuleInstance::isTiedNet(NUNet* net) const
{
  const NUNetSet * tie_net_temps = mDesign->getTieNetTemps();
  return (tie_net_temps->find(net) != tie_net_temps->end());
}


FlattenResolveHierRefs::FlattenResolveHierRefs(NUModule *module, 
                                               NUNetRefFactory * netref_factory,
                                               Fold* fold) :
  mModule(module),
  mNetRefFactory(netref_factory),
  mFold(fold)
{
}


FlattenResolveHierRefs::~FlattenResolveHierRefs()
{
}


void FlattenResolveHierRefs::resolve()
{
  resolveNets();

  resolveTasks();
}


// Follow chains of hierarchical references all the way to the end.
// This is potentially n^2 in the length of the hierarchical reference
// chains, but that typically should not be a very large n.  You can
// manufacture such a case by expanding test/hierref_port/bug4483b.v,
// and that can be solved by passing the net_replacements map into this
// function for short-circuiting.
static NUNet* sGetResolvedNet(NUModule* mod, NUNet* net, NUNetSet* obsolete) {
  while (net->isHierRef()) {
    NUNetHierRef* hier_ref = net->getHierRef();
    const AtomArray path = hier_ref->getPath();
    STSymbolTableNode* node = mod->resolveHierRefLocally(path, 0);
    if (node) {
      NUNet *resolution = NUNet::findStorageUnelab(node);
      ST_ASSERT(resolution != NULL, node);
      obsolete->insert(net);
      resolution->transferAliases(net);
      net = resolution;
    }
    else {
      // the net was not fully locally resolvable
      break;
    }
  }
  return net;
}

void FlattenResolveHierRefs::resolveNets()
{
  NUNetReplacementMap net_replacements;

  NUNetSet obsolete_hierrefs;

  // Collect the resolvable hierrefs into a map.
  for (NUNetLoop loop = mModule->loopNetHierRefs(); not loop.atEnd(); ++loop) {
    NUNet *net = *loop;
    NUNet* resolution = sGetResolvedNet(mModule, net, &obsolete_hierrefs);
    if (net != resolution) {
      net_replacements[net] = resolution;
    }
  }

  if (not net_replacements.empty()) {
    // We only use the net replacement functionality.  Create a bunch of empty
    // maps for the other types of replacements.
    NULvalueReplacementMap lvalue_replacements;
    NUExprReplacementMap rvalue_replacements;
    NUTFReplacementMap tf_replacements;
    NUCModelReplacementMap cmodel_replacements;
    NUAlwaysBlockReplacementMap always_replacements;

    // Call the rewriter on the module to do the replacements.
    RewriteLeaves translator(net_replacements,
                             lvalue_replacements,
                             rvalue_replacements,
                             tf_replacements,
                             cmodel_replacements,
                             always_replacements,
                             NULL);
    mModule->replaceLeaves(translator);

    // Update the temp-memory masters based on hierref rewrites.
    mModule->updateTempMemoryMasters(translator);
  }

  // Before we delete any nets, clear out the Fold context's expression
  // factory, which may contain IdentRvalues that point to those nets,
  // causing subsequent users of that factory to crash doing lookups
  if (! obsolete_hierrefs.empty()) {
    mFold->clearExprFactory();
  }

  for (NUNetSet::iterator iter = obsolete_hierrefs.begin();
       iter != obsolete_hierrefs.end();
       ++iter) {
    NUNet * hier_ref = *iter;
    mModule->removeNetHierRef(hier_ref);
    delete hier_ref;
  }
}


void FlattenResolveHierRefs::resolveTasks()
{
  // 1. Update any hierarchical task enables locally contained within this module.
  FlattenTaskEnableFixupCallback callback(mModule, mNetRefFactory);
  NUDesignWalker walker(callback,false);
  walker.putWalkTasksFromTaskEnables(false);
  NUDesignCallback::Status status = walker.module(mModule);
  NU_ASSERT(status==NUDesignCallback::eNormal,mModule);

  // 2. Update any remaining references to our locally defined tasks.
  for ( NUTaskLoop loop = mModule->loopTasks() ;
	!loop.atEnd();
	++loop ) {
    NUTask * task = (*loop);
    if (task->hasHierRef()) {

      // Create a copy of the referrers to this task. Our inner loop
      // will be removing references and resolutions; we don't want to
      // mutate while looping.

      NUTaskEnableVector referrers;
      for (NUTaskEnableLoop enable_loop = task->loopHierRefTaskEnables();
           not enable_loop.atEnd();
           ++enable_loop) {
        NUTaskEnable * enable = (*enable_loop);
        NU_ASSERT(enable->isHierRef(), enable);
        referrers.push_back(enable);
      }

      for (NUTaskEnableVector::iterator enable_iter = referrers.begin();
           enable_iter != referrers.end();
           ++enable_iter) {
        NUTaskEnable * enable = (*enable_iter);

        NUTaskEnableHierRef * enable_hier_ref = dynamic_cast<NUTaskEnableHierRef*>(enable);
        NUTaskHierRef * hier_ref = enable_hier_ref->getHierRef();

        NUTask * resolved_task = NULL;
        bool relative = hier_ref->isLocallyRelative(NULL,&resolved_task);
        if (not relative) {
          continue;
        }

        NU_ASSERT(resolved_task,enable);

        // If we remove the existing representative resolution, we
        // have to update the arg connections to reference the new
        // resolution.

        NUTask * old_representative_resolution = enable_hier_ref->getRepresentativeResolution();
        if (old_representative_resolution != resolved_task) {
          CopyContext cc(NULL, NULL);
          enable->replaceTask(resolved_task, cc);
        }

        // Create a copy of the resolutions for this task enable. We
        // are marking them for removal and don't want to mutate while
        // looping.

        NUTaskVector remove_resolutions;
        for (NUTaskHierRef::TaskVectorLoop ref_loop = hier_ref->loopResolutions();
             not ref_loop.atEnd();
             ++ref_loop) {
          NUTask * resolution = (*ref_loop);
          if (resolution != resolved_task) {
            remove_resolutions.push_back(resolution);
          }
        }

        for (NUTaskVector::iterator ref_iter = remove_resolutions.begin();
             ref_iter != remove_resolutions.end();
             ++ref_iter) {
          NUTask * stale_resolution = (*ref_iter);
          stale_resolution->removeHierRef(enable);
          enable->getHierRef()->removeResolution(stale_resolution);
        }
      }

    }
  }
}


FlattenTaskEnableFixupCallback::FlattenTaskEnableFixupCallback(NUModule * the_module,
                                                               NUNetRefFactory * netref_factory) :
  mNetRefFactory(netref_factory),
  mModule(the_module)
{
}


FlattenTaskEnableFixupCallback::~FlattenTaskEnableFixupCallback()
{
}


NUDesignCallback::Status FlattenTaskEnableFixupCallback::operator()(Phase phase, NUTF * stmt)
{
  if (phase==ePre) {
    NUStmtList replacements;

    bool modified = stmts(stmt->loopStmts(),
			  replacements);
    if (modified) {
      stmt->replaceStmtList(replacements);
    }
  }

  return eNormal;
}


NUDesignCallback::Status FlattenTaskEnableFixupCallback::operator()(Phase phase, NUBlock * stmt)
{
  if (phase==ePre) {
    NUStmtList replacements;

    bool modified = stmts(stmt->loopStmts(),
			  replacements);
    if (modified) {
      stmt->replaceStmtList(replacements);
    }
  }

  return eNormal;
}



NUDesignCallback::Status FlattenTaskEnableFixupCallback::operator()(Phase phase, NUCase * stmt)
{
  if (phase==ePre) {
    for (NUCase::ItemLoop loop = stmt->loopItems(); 
	 not loop.atEnd() ;
	 ++loop) {
      NUCaseItem *item = (*loop);

      NUStmtList replacements;
      bool modified = stmts(item->loopStmts(),
			    replacements);
      if (modified) {
	item->replaceStmts(replacements);
      }
    }
  }
  return eNormal;
}


NUDesignCallback::Status FlattenTaskEnableFixupCallback::operator()(Phase phase, NUIf * stmt)
{
  if (phase==ePre) {
    NUStmtList replacements;
    bool modified = stmts(stmt->loopThen(),
			  replacements);
    if (modified) {
      stmt->replaceThen(replacements);
    }

    // clear replacements before processing else stmts.
    replacements.clear();

    modified = stmts(stmt->loopElse(),
		     replacements);
    if (modified) {
      stmt->replaceElse(replacements);
    }
  }
  return eNormal;
}


NUDesignCallback::Status FlattenTaskEnableFixupCallback::operator()(Phase phase, NUFor * stmt)
{
  if (phase==ePre) {
    NUStmtList replacements;
    bool modified = stmts(stmt->loopBody(),
			  replacements);
    if (modified) {
      stmt->replaceBody(replacements);
    }
  }
  return eNormal;
}


bool FlattenTaskEnableFixupCallback::stmts(NUStmtLoop loop, NUStmtList & replacements)
{
  bool success = false;
  for ( /*no initial*/ ; not loop.atEnd(); ++loop ) {
    NUStmt * stmt = (*loop);
    NUStmt * replacement = fixupTaskEnable(stmt);
    if (replacement) {
      replacements.push_back(replacement);
      success = true;
    } else {
      replacements.push_back(stmt);
    }
  }
  return success;
}


NUStmt * FlattenTaskEnableFixupCallback::fixupTaskEnable(NUStmt * enable_stmt)
{
  if (enable_stmt->getType() != eNUTaskEnable) {
    return NULL;
  }

  NUTaskEnable * enable = dynamic_cast<NUTaskEnable*>(enable_stmt);
  if (not enable->isHierRef()) {
    return NULL;
  }

  NUTaskEnable * replacement = NULL;

  NUTaskHierRef * hier_ref = enable->getHierRef();
  STSymbolTableNode* node = mModule->resolveHierRefLocally(hier_ref->getPath(), 0);
  if (node) {
    STBranchNode * branch = node->castBranch();
    ST_ASSERT(branch,node);
    NUAliasDataBOM * bom = NUAliasBOM::castBOM(branch->getBOMData());
    NUScope * scope = bom->getScope();
    NUTask * task = dynamic_cast<NUTask*>(scope);
    NU_ASSERT(task,enable_stmt);

    replacement = new NUTaskEnable(task, scope->getModule(),
                                   mNetRefFactory, enable->getUsesCFNet(),
                                   enable->getLoc());
    CopyContext cc(NULL, NULL);
    replacement->replaceTask(task, cc, enable);

    delete enable;
  }  

  return replacement;
}


void FlattenStatistics::print(UtOStream & out) const
{
  out << "Flattening Summary: " 
      << mChildren << " instances flattened into " 
      << mParents << " parent modules." << UtIO::endl;
}


FlattenQualifyPort::FlattenQualifyPort(NUModule *submodule) :
  mSubmodule(submodule),
  mNoScalarizeNets(new NUNetSet())
{
  FindNoScalarizeNetsCallback callback(mSubmodule, *mNoScalarizeNets);
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(mSubmodule);
}


FlattenQualifyPort::~FlattenQualifyPort()
{
  delete mNoScalarizeNets;
}


bool FlattenQualifyPort::qualify(NUExpr *actual, NUNet *formal) const
{
  if (not actual) {
    // flattening can handle disconnects.
    return true;
  }

  switch(actual->getType()) {
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUVarselRvalue:
  case NUExpr::eNUMemselRvalue:
  case NUExpr::eNUConstNoXZ:
  case NUExpr::eNUConstXZ:
    // flattening can handle these things.
    return true;
    break;

  case NUExpr::eNUConcatOp:
    // flattening can handle these if the formal is scalarizable and the
    // concats are easy.
  {
    NUConcatOp *concat = dynamic_cast<NUConcatOp*>(actual);
    return (FlattenModuleInstance::qualifyConcatInputPortConnection(concat, formal, *mNoScalarizeNets) == FlattenAnalyzer::eStatusSuccess);
  }
  break;

  case NUExpr::eNUUnaryOp:
  {
    // Flattening can handle ~vec if vec is a full vector. Flattened
    // (~vec)[sel] references should get folded into ~(vec[sel]).
    NUUnaryOp * unary = dynamic_cast<NUUnaryOp*>(actual);

    // A simple ~vec or !bit port.
    bool bit_flip = unary->isIdentifierBitFlip();
    return bit_flip;
  }
  break;

  default:
    return false;
    break;
  }
}


bool FlattenQualifyPort::qualify(NULvalue *actual, NUNet *formal) const
{
  if (not actual) {
    // flattening can handle disconnects.
    return true;
  }

  switch(actual->getType()) {
  case eNUIdentLvalue:
  case eNUMemselLvalue:
  case eNUVarselLvalue:
    // flattening can handle these things.
    return true;
    break;

  case eNUConcatLvalue:
    // flattening can handle these if the formal is scalarizable and the
    // concats are easy.
  {
    NUConcatLvalue *concat = dynamic_cast<NUConcatLvalue*>(actual);
    return (FlattenModuleInstance::qualifyConcatOutputPortConnection(concat, formal, *mNoScalarizeNets) == FlattenAnalyzer::eStatusSuccess);
  }
  break;

  default:
    return false;
    break;
  }
}
