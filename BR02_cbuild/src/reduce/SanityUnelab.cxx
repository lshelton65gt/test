// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/SanityUnelab.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUTF.h"
#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "flow/FLFactory.h"
#include "util/UtIOStream.h"
#include "util/OSWrapper.h"

#include "SanityStatus.h"

#include "util/CbuildMsgContext.h"

/*!
  \file
  Implementation of unelaborated flow sanity check package.
 */

//! Maintain status for unelaborated flow sanity checks.
class SanityUnelabStatus : public SanityStatus<FLNode>
{
public:
  //! Destructor
  SanityUnelabStatus(FLNode * node) :
    SanityStatus<FLNode>(node),
    mSafeFLNode(false),
    mSafeNet(false),
    mSafeUseDefNode(false)
  {}

  //! Constructor
  ~SanityUnelabStatus() {};

  //! Mark that the FLNode is safe for dereferencing.
  void setSafeFLNode() { mSafeFLNode = true; }

  //! Mark that the NUNet referenced by the FLNode is safe for dereferencing.
  void setSafeNet() { mSafeNet = true; }

  //! Mark that the UseDefNode referenced by the FLNode is safe for dereferencing.
  void setSafeUseDefNode() { mSafeUseDefNode = true; }

protected:
  //! Print header information for the FLNode.
  virtual void printHeader() const;

private:
  //! Is the FLNode safe for dereferencing?
  bool mSafeFLNode;

  //! Is the Net referenced by the FLNode is safe for dereferencing?
  bool mSafeNet;

  //! Is the UseDefNode referenced by the FLNode is safe for dereferencing?
  bool mSafeUseDefNode;
};


void SanityUnelabStatus::printHeader() const
{
  UtIO::cout() << "Unelaborated Sanity Check Report:" << UtIO::endl;

  // Print as much information about this flnode as is safe.

  FLNode * unelab = getNode();
  UtIO::cout() << "    UnelabFlow: " << unelab << UtIO::endl;
  if (mSafeFLNode) {
    // It is safe to dereference the unelaborated ptr.
    UtString message;
    NUNetRefHdl net_ref = unelab->getDefNetRef();
    NUNet * unelab_net = net_ref->getNet();
    net_ref->compose(&message);
    UtIO::cout() << "    UnelabNet (" << unelab_net << "): " << message << UtIO::endl;
    NUUseDefNode * use_def = unelab->getUseDefNode();
    UtIO::cout() << "    UseDef:";
    if (use_def) {
      if (mSafeUseDefNode) {
        UtIO::cout() << UtIO::endl;
        use_def->print(false, 8);
      } else {
        // Unsafe to dereference NUUseDefNode. Print the ptr.
        UtIO::cout() << " " << use_def << UtIO::endl;
      }
    } else {
      UtIO::cout() << " (nil)" << UtIO::endl;
    }
  }
}


//! Maintain status for unelaborated net sanity checks.
class SanityUnelabNetStatus : public SanityStatus<NUNet>
{
public:
  //! Destructor
  SanityUnelabNetStatus(NUNet * node) :
    SanityStatus<NUNet>(node)
  {}

  //! Constructor
  ~SanityUnelabNetStatus() {};

protected:
  //! Print header information for the Net.
  virtual void printHeader() const;

private:
};


void SanityUnelabNetStatus::printHeader() const
{
  UtIO::cout() << "Unelaborated Net Sanity Check Report:" << UtIO::endl;

  // Print as much information about this net as is safe.

  NUNet * net = getNode();

  UtIO::cout() << "    Net (" << net << "): ";
  UtString message;
  net->composeUnelaboratedName(&message);
  UtIO::cout() << message << UtIO::endl;
}


SanityUnelab::SanityUnelab(MsgContext * msg_context,
                           NUNetRefFactory * netref_factory,
                           FLNodeFactory *dfg_factory,
                           bool checkFaninOverlap,
                           bool verbose) :
  DesignWalker(verbose),
  mMsgContext(msg_context),
  mNetRefFactory(netref_factory),
  mFlowFactory(dfg_factory),
  mNumErrors(0),
  mCheckOverlap(checkFaninOverlap)
{
  mFlowFactory->getFreeSet(&mFreeSet);
}


void SanityUnelab::design(NUDesign *design)
{
  // check the driver sets for all nets
  // remember all nets
  DesignWalker::design(design);

  // remember all use def nodes
  SanityNucleusCallback nucleus_callback(this);
  NUDesignWalker nucleus_marker(nucleus_callback, false);
  NUDesignCallback::Status status = nucleus_marker.design(design,true);
  ASSERT(status == NUDesignCallback::eNormal);

  // check the fanin sets for all flow
  // check that the net is valid
  // check that the usedef is valid
  factory();

  if (mNumErrors > 0) {
    mMsgContext->SanityUnelab(mNumErrors);
  }
}


NUDesignCallback::Status
SanityUnelab::SanityNucleusCallback::operator()(NUDesignCallback::Phase phase, NUBase *node)
{
  if (phase == ePre) {
    mObj->mark(node);
  }
  return eNormal;
}


void SanityUnelab::mark(NUBase* node)
{
  mValidUseDefs.insert(node);
}


void SanityUnelab::module(NUModule *this_module)
{
  if (haveVisited(this_module)) {
    return;
  }

  scopeNets(this_module);
  
  for (NUTaskLoop iter = this_module->loopTasks();
       not iter.atEnd();
       ++iter) {
    scopeNets(*iter);
  }

  for (NUModuleInstanceMultiLoop iter = this_module->loopInstances();
       not iter.atEnd();
       ++iter) {
    NUModuleInstance *inst = *iter;
    module(inst->getModule());
  }

  rememberVisited(this_module);
}


void SanityUnelab::scopeNets(NUScope * scope)
{
  NUNetList start_list;
  scope->getAllNets(&start_list);

  for (NUNetListIter iter = start_list.begin(); iter != start_list.end(); ++iter) {
    NUNet * one_net = (*iter);
    mValidNets.insert(one_net);

    SanityUnelabNetStatus status(one_net);
    bool success = net(&status, one_net);
    if (not success) {
      mNumErrors++;
    }
    status.print(mVerbose);
  }
}


bool SanityUnelab::net(SanityUnelabNetStatus * status, NUNet *this_net)
{
  bool driver_alloc_ok = true;
  bool driver_def_ok = true;

  FLNodeSet bad_alloc_set;
  FLNodeSet bad_def_set;
  for (NUNet::DriverLoop loop = this_net->loopContinuousDrivers(); not loop.atEnd(); ++loop) {
    FLNode *driver = *loop;
    bool this_ok = allocFlowCheck(driver);
    if (not this_ok) {
      bad_alloc_set.insert(driver);
    }
    driver_alloc_ok &= this_ok;

    if (this_ok) {
      this_ok = (driver->getDefNet() == this_net);
      if (not this_ok) {
        bad_def_set.insert(driver);
      }
      driver_def_ok &= this_ok;
    }
  }

  status->addTest("Driver Allocation", driver_alloc_ok);
  if (not driver_alloc_ok) {
    UtString detail;
    detail << "Pointers: ";
    for (FLNodeSetIter iter = bad_alloc_set.begin(); iter != bad_alloc_set.end(); ++iter) {
      FLNode * bad_fanin = (*iter);
      detail << bad_fanin << " ";
    }
    status->addDetail(detail);
  }

  status->addTest("Driver Def Check", driver_def_ok);
  if (not driver_def_ok) {
    for (FLNodeSetIter iter = bad_def_set.begin(); iter != bad_def_set.end(); ++iter) {
      FLNode * bad_def_flow = (*iter);
      UtString detail;
      // cannot be sure that net/usedef of bad_def_flow is valid; only print ptrs.
      NUNet * def_net = bad_def_flow->getDefNet();
      NUUseDefNode * use_def = bad_def_flow->getUseDefNode();
      detail << "Def Mismatch: Flow(" << bad_def_flow << "), "
             << "Net(" << def_net << "), "
             << "UseDef(";
      if (use_def) {
        detail << use_def;
      } else {
        detail << "nil";
      }
      detail << ")";
      status->addDetail(detail);
    }
  }

  bool ok = driver_alloc_ok && driver_def_ok;
  return ok;
}


void SanityUnelab::factory()
{
  for (FLNodeFactory::iterator iter = mFlowFactory->begin();
       iter != mFlowFactory->end();
       ++iter) {
    FLNode *flnode = *iter;
    SanityUnelabStatus status(flnode);
    bool success = flow(&status,flnode);
    if (not success) {
      mNumErrors++;
    }
    status.print(mVerbose);
  }
}


bool SanityUnelab::allocFlowCheck(FLNode *flnode)
{
  if (mFreeSet.find(flnode) != mFreeSet.end()) {
    return false;
  }
  return true;
}


//! check that fanin flow's defs overlap this flow's uses
bool SanityUnelab::faninOverlapCheck(FLNode* flnode, 
                                     const FLNodeSet & bad_fanin,
                                     FLNodeSet & unnecessary_fanin)
{
  NUUseDefNode* useDef = flnode->getUseDefNode();
  if (!useDef)
    return true; // bound node. not allowed to have fanin.

  // port connections can have uses in a different hierarchy -- don't check their fanin
  NUType type = useDef->getType();
  if ((type == eNUPortConnectionInput) ||
      (type == eNUPortConnectionOutput) ||
      (type == eNUPortConnectionBid))
    return true;

  // $extnet can have fanin to protected-observable nets even though they are not in its uses
  // it is also used for hierrefs in ways that are inconsistent with UD
  // $ostnet for a module instance can have fanin directly to port connections
  // (it has no nested port connection of its own)
  // $cfnet is being excluded on the basis of guilt-by-association
  if (flnode->getDefNet()->isVirtualNet())
    return true;

  NUNetRefSet uses_for_def(mNetRefFactory);

  // get all fanin for this def.
  if (useDef->useBlockingMethods()) {
    NUUseDefStmtNode* stmt = dynamic_cast<NUUseDefStmtNode*>(useDef);
    stmt->getBlockingUses(flnode->getDefNetRef(),
                          &uses_for_def);
    stmt->getNonBlockingUses(flnode->getDefNetRef(),
                             &uses_for_def);
  } else {
    useDef->getUses(flnode->getDefNetRef(),
                    &uses_for_def);
  }

  bool fanin_ok = true;

  // check if the fanin's def overlaps our use
  // the overlaps testing only applies to fanins of continuous drivers
  FLNodeIter *flow_iter = flnode->makeFaninIter(FLIterFlags::eAll,
                                                FLIterFlags::eAll,
                                                FLIterFlags::eBranchAtAll);
  for (; not flow_iter->atEnd(); flow_iter->next()) {
    FLNode* fanin = **flow_iter;

    if (flow_iter->isNestedRelationship()) {
      continue;
    }

    bool this_ok = (bad_fanin.find(fanin)==bad_fanin.end());

    if (this_ok) {
      NUNetRefHdl fanin_def = fanin->getDefNetRef();
      bool overlaps_ok = (uses_for_def.find(fanin_def,&NUNetRef::overlapsSameNet) != uses_for_def.end());
      if (not overlaps_ok) {
        unnecessary_fanin.insert(fanin);
      }
      fanin_ok &= overlaps_ok;
    }    
  }
  delete flow_iter;

  return fanin_ok;
}

bool SanityUnelab::flow(SanityUnelabStatus * status, FLNode *flnode)
{
  bool ok = allocFlowCheck(flnode);
  status->addTest("Flow Allocation", ok);
  if (not ok) {
    return false;
  }

  status->setSafeFLNode();

  if (dynamic_cast<FLNodeBound*>(flnode)) {
    return true;
  }

  NUUseDefNode * use_def = flnode->getUseDefNode();
  bool ud_ok = (mValidUseDefs.find(use_def) != mValidUseDefs.end());
  status->addTest("UseDef Allocation", ud_ok);
  if (ud_ok) {
    status->setSafeUseDefNode();
  }
  ok &= ud_ok;

  NUNet * net = flnode->getDefNet();
  bool net_ok = (mValidNets.find(net) != mValidNets.end());
  status->addTest("Net Allocation", net_ok);
  if (net_ok) {
    status->setSafeNet();
  }
  ok &= net_ok;

  bool isContinuousDriver = use_def && use_def->isContDriver();

  // TBD: This test does not currently distinguish between edge and level fanin.

  // Only visit immediate fanin.
  bool fanin_alloc_ok = true;

  FLNodeSet bad_set;
  FLNodeIter *flow_iter = flnode->makeFaninIter(FLIterFlags::eAll,
                                                FLIterFlags::eAll,
                                                FLIterFlags::eBranchAtAll);
  for (; not flow_iter->atEnd(); flow_iter->next()) {
    FLNode* fanin = **flow_iter;
    bool this_ok = allocFlowCheck(fanin);
    if (not this_ok) {
      bad_set.insert(fanin);
    }
    fanin_alloc_ok &= this_ok;
  }
  delete flow_iter;

  bool fanin_overlap_ok = true;
  FLNodeSet unnecessary_fanins;
  if (mCheckOverlap && isContinuousDriver) {
    fanin_overlap_ok = faninOverlapCheck(flnode, bad_set, unnecessary_fanins);
  }

  ok &= fanin_alloc_ok;
  ok &= fanin_overlap_ok;

  status->addTest("Fanin Allocation", fanin_alloc_ok);
  if (not fanin_alloc_ok) {
    UtString detail;
    detail << "Pointers: ";
    for (FLNodeSetIter iter = bad_set.begin(); iter != bad_set.end(); ++iter) {
      FLNode * bad_fanin = (*iter);
      detail << bad_fanin << " ";
    }
    status->addDetail(detail);
  }

  status->addTest("Fanin Overlap", fanin_overlap_ok);
  if (not fanin_overlap_ok) {
    for (FLNodeSetIter iter = unnecessary_fanins.begin();
         iter != unnecessary_fanins.end();
         ++iter) {
      FLNode * unnecessary_fanin = (*iter);
      UtString detail;
      detail << "Unnecessary fanin (" << unnecessary_fanin << "): ";
      status->addDetail(detail);

      NUNetRefHdl def_ref = unnecessary_fanin->getDefNetRef();
      detail = "    ";
      def_ref->compose(&detail);
      status->addDetail(detail);
    }
  }
  return ok;
}
