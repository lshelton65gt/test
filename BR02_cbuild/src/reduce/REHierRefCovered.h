// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef REHIERREFCOVERED_H_
#define REHIERREFCOVERED_H_

#include "reduce/Reduce.h"
#include "nucleus/Nucleus.h"

/*!
  \file 
  Declaration of class used to discover nets covered by hierrefs in the port-connection graph.
 */

class STSymbolTable;
class MsgContext;

//! 
/*!
 * An elaborated alias ring consists of nets joined in a number of
 * ways:
 *
 * 1. Functional aliases.
 * 2. Flattened hierarchical aliases.
 * 3. Hierarchical aliases.
 * 4. Hierarchical references.
 *
 * Functional aliases begin as an unelaborated equivalence and leave
 * only one alias in the Nucleus tree as representative. The other,
 * subordinate aliases are considered unreachable during allocation.
 *
 * Flattened hierarchical aliases connect additional names to a
 * physical net. Only the top-most of these aliases is considered
 * reachable during allocation.
 *
 * Hierarchical aliases are found by analyzing the port connections.
 * The elaborated names connected through this port connection tree
 * are considered equivalent.
 *
 * Hierarchical references combine multiple hierarchical alias trees
 * into one alias group (we implement this group with singly-linked
 * ring).
 *
 * As a result of hierarchical aliases and hierarchical references, an
 * alias ring is a forest of port connection trees. For example:
 *
 \code
   module top();
     connector connector();
     elsewhere elsewhere();
   endmodule
   module connector();
     wire data;
     writer w ( .out(data) );
     reader r ( .in(data) );
   endmodule
   module elsewhere();
     monitor mon( .data(connector.data) );
   endmodule
 \endcode
 *
 * This example contains one alias ring with the following nets:
 *     top.connector.data
 *     top.connector.w.out
 *     top.connector.r.in
 *     top.elsewhere."connector.data" (hierref)
 *     top.elsewhere.mon.data
 *
 * The port connection graph for this ring contains the following two trees:
 *
 \code
                top.connector.data
                 /               \
               |_                 _|
      top.connector.w.out  top.connector.r.in
 
          top.elsewhere."connector.data"
                        |
                        V
              top.elsewhere.mon.data
 \endcode
 *
 * If we include an arc representing the resolution of the
 * hierarchical reference, we get the graph:
 * 
 \code
                top.connector.data <---------- top.elsewhere."connector.data"
                 /               \                           |
               |_                 _|                         V
      top.connector.w.out  top.connector.r.in      top.elsewhere.mon.data    
 
          top.elsewhere."connector.data"
                        |
              top.elsewhere.mon.data    
 \endcode
 *
 * A cyclic hierarchical reference is considered an illegal reference.
 * We check for cycles and trigger an error if they occur. The
 * simplest example of a cyclic hierarchical reference is the
 * following:
 *
 \code
   sub sub ( .data ( sub.data ) );
 \endcode
 *
 * In order for this cyclic check to be accurate, the port connection
 * graph must be in terms of elaborated names. An unelaborated port
 * connection graph will contain false cycles, such as the following
 * example:
 *
 \code
   sub s0 ( .data ( data_in ) );
   sub s1 ( .data ( s0.data ) );
 \endcode
 *
 * Unelaboratedly, the hierarchical reference "s0.data" resolves to
 * some instance of the unelaborated net sub.data. Unless we know
 * precisely which instance is the resolution, this appears as a
 * cycle. Using an elaborated port connection graph avoids this
 * problem.
 *
 * Claim: Consider the port-connection & hierarchical resolution
 * graph. If this graph is acyclic, there must be at least one
 * port-connection tree rooted at a non-hierref.
 *
 * Proof: Assume that an acyclic graph is rooted only at hierarchical
 * references. Choose one tree of the alias ring and label it T1. The
 * hierarchical reference at the root of this tree is HR1. Label the
 * tree containing the resolution of HR1 T2. Continue this
 * construction until all trees in the alias ring have been included.
 * Tree TN has hierarchical reference HRN, but it cannot resolve to
 * any of the trees already in the construction without causing a
 * cycle. Therefore, TN cannot be a hierarchical reference.
 *
 * Any HRi resolving to multiple trees can be handled in a similar
 * fashion. Assume that HRi resolves to two trees and they have been
 * labelled Ti+1 and Ti+2. The hierrefs HRi+1 and HRi+2 cannot resolve
 * to any previous tree without causing a cycle. Therefore, they must
 * resolve to each other. HRi+1 can resolve to Ti+2, but the
 * introduction of a HRi+2 to Ti+1 resolution introduces a cycle. This
 * argument extends to HRi resolving to M different trees.
 * 
 * All alias rings must contain a port-connection tree rooted at a
 * non-hierarchical reference. The REHierRefCovered class identifies
 * all nets which are covered by hierarchical references. The REAlloc
 * allocator disqualifies these hierref-covered nets as valid
 * allocation locations.
 */
class REHierRefCovered
{
public:
  //! Constructor
  REHierRefCovered(NUDesign * design,
                   STSymbolTable * symbol_table,
                   MsgContext * msg_context);

  //! Destructor
  ~REHierRefCovered();

  //! Determine the set of all nets covered by a hierarchical reference port connection.
  /*!
   * \return false if cyclic hierrefs are discovered, true otherwise.
   */
  bool discover(NUNetSet * covered_by_hier_ref);

  /*!
   * Edge flags for the port-connection graph. Take care that the
   * values do not use the first two bits (scratch pad for
   * GraphWalker).
   */
  enum PortConnectionEdgeFlags {
    eEdgePortConnection        = 0x4, //!< An edge caused by a port connection.
    eEdgeHierarchicalReference = 0x8, //!< An edge caused by a hierarchical referrer.
    eEdgeAllFlags = (eEdgePortConnection|eEdgeHierarchicalReference)
  };

  /*!
   * Node flags for the port-connection graph. Take care that the
   * values do not use the first two bits (scratch pad for
   * GraphWalker).
   */
  enum PortConnectionNodeFlags {
    eNodeHierRefCovered = 0x4, //!< A node covered by a hierarchical reference. 
    eNodeUncovered      = 0x8, //!< A node not known to be covered by a hierarchical reference.
    eNodeAllFlags = (eNodeHierRefCovered|eNodeUncovered)
  };

private:
  //! Hide copy and assign constructors.
  REHierRefCovered(const REHierRefCovered&);
  REHierRefCovered& operator=(const REHierRefCovered&);

  //! The design.
  NUDesign * mDesign;

  //! The symbol table.
  STSymbolTable * mSymbolTable;

  //! The message context.
  MsgContext * mMsgContext;
};

#endif // REHIERREFCOVERED_H_
