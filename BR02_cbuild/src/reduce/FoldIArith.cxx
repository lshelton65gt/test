// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Algebraic simplifications of things like
 *     (((idx_int + 31) - idx_int) + 1)  --->   32
 * This form is easily produced by localflow/DowntoResynth.cxx
 * Simple constant folding pass.  Walk the design folding all expression
 * trees into simpler forms.
 */

#include "FoldI.h"
#include "nucleus/NUExprFactory.h"

/*
 * Strategy:
 *
 * Recursively collect the terms from the binary operator.  As long
 * as we are working with the same size args, collect them in a vector.
 *
 * For now, we will look only at + and -.  When we see a - term, we will
 * put it in a separate vector.
 *
 * When we encounter a term that is not a + or -, we consider that a leaf.
 *
 * If we encounter a unary -, we can put it's arg into the opposite bucket.
 *
 * All the expressions we encounter will be put into a set so that
 * recursive collections will not re-traverse the same expressions.
 */


void FoldI::collectTerms(const NUExpr* expr, ExprBag* terms,
                         UInt32 size, SInt32 count)
{
  bool is_terminal = true;

  // Make sure the sizes are consistent.  They should be, NUExpr::resize
  // will ensure that for + and -.  
  if (expr->getBitSize() == size) {
      
    const NUOp* op = dynamic_cast<const NUOp*>(expr);
    if (op != NULL) {
      NUOp::OpT opcode = op->getOp();

      switch (opcode) {
      case NUOp::eBiPlus:
        collectTerms(op->getArg(0), terms, size, count);
        collectTerms(op->getArg(1), terms, size, count);
        is_terminal = false;
        break;
      case NUOp::eBiMinus:
        collectTerms(op->getArg(0), terms, size, count);
        collectTerms(op->getArg(1), terms, size, -count);
        is_terminal = false;
        break;
      case NUOp::eUnPlus:
        collectTerms(op->getArg(0), terms, size, count);
        is_terminal = false;
        break;
      case NUOp::eUnMinus:
        collectTerms(op->getArg(0), terms, size, -count);
        is_terminal = false;
        break;
      case NUOp::eBiUMult: {
        const NUConst* k = op->getArg(0)->castConst();
        const NUExpr* arg = op->getArg(1);
        if (k == NULL) {
          k = arg->castConst();
          arg = op->getArg(0);
        }
        SInt32 multiplier;
        if ((k != NULL) && k->getL(&multiplier)) {
          collectTerms(arg, terms, size, count * multiplier);
          is_terminal = false;
        }
      }
        break;
      default:
        break;
      } // switch
    }
  } // else
    
  if (is_terminal) {
    (*terms)[expr] += count;
  }
} // void FoldI::collectTerms

void FoldI::accumConstants(ExprBag* bag, DynBitVector* accum,
                           CarbonReal* raccum) 
{
  // Accumulate any constants in the + and - column and remove them
  for (ExprBag::iterator p = bag->begin(), e = bag->end(); p != e; ++p) {
    const NUExpr* expr = p->first;
    const NUConst* k = expr->castConst();
    if ((k != NULL) && !k->hasXZ()) {
      SInt32 num = p->second;
      CarbonReal rval;
      bool is_real = k->getCarbonReal(&rval);
      DynBitVector val;
      k->getSignedValue(&val);
      if (is_real) {
        *raccum += rval * num;
      }
      else {
        DynBitVector dnum(32);
        bool neg = false;
        if (num < 0) {
          neg = true;
          num = -num;
        }
        dnum = num;
        val.integerMult(dnum);
        if (neg) {
          *accum -= val;
        }
        else {
          *accum += val;
        }
      }
      bag->erase(p);
    }
  }
} // void FoldI::accumConstants

const NUExpr* FoldI::arithmeticAdd(const NUExpr* e1, const NUExpr* e2) {
  if (e1 == NULL) {
    return e2;
  }
  if (e2 == NULL) {
    return e1;
  }
  UInt32 sz = std::max(e1->getBitSize(), e2->getBitSize());
  return binaryOp(NUOp::eBiPlus, e1, e2, e1->getLoc(), sz);
}

const NUExpr* FoldI::accumExprs(ExprBag* bag, SInt32 mult) {
  const NUExpr* result = NULL;
  NUCExprVector v;
  for (ExprBag::iterator p = bag->begin(), e = bag->end(); p != e; ++p) {
    const NUExpr* expr = p->first;
    SInt32 num = p->second * mult;
    if (num > 0) {
      v.push_back(expr);
    }
  }
  std::sort(v.begin(), v.end(), AssocOrder(NUOp::eBiPlus));
  for (UInt32 i = 0; i < v.size(); ++i) {
    const NUExpr* expr = v[i];
    SInt32 num = (*bag)[expr] * mult;
    bag->erase(expr);
    if (num > 0) {
      // I'm not sure whether it's better to write 3*x or (x+(x+x))
      // in terms of codegen.  But on the theory that a smaller
      // representation is better for the compile process, I'm
      // going to go with 3*x

      if (num < 3) {
        for (SInt32 i = 0; i < num; ++i) {
          result = arithmeticAdd(result, expr);
        }
      }
      else {
        const SourceLocator& loc = expr->getLoc();
        const NUExpr* k = constant(false, num, expr->getBitSize(), loc);
        NUOp::OpT op = expr->isSignedResult() ? NUOp::eBiUMult : NUOp::eBiSMult;
        const NUExpr* k_expr = binaryOp(op, expr, k, loc);
        result = arithmeticAdd(result, k_expr);
      }
    }
  }
  return result;
} // const NUExpr* FoldI::accumExprs

const NUExpr* FoldI::combineArithmetic(const NUBinaryOp* bop) {
  ExprBag terms;

  UInt32 size = bop->getBitSize();
  bool sign = bop->isSignedResult();
  collectTerms(bop, &terms, size, 1);
  const SourceLocator& loc = bop->getLoc();

  // Collect all the constants into one term
  DynBitVector accum(size);
  CarbonReal raccum = 0;
  accumConstants(&terms, &accum, &raccum);

  // Collect the remaining expressions and sort them so we
  // get canonical expressions coming out of this transformation
  const NUExpr* result = accumExprs(&terms, 1);
  const NUExpr* minusExpr = accumExprs(&terms, -1);

  if (raccum != 0.0) {
    const NUExpr* constExpr = constant(raccum, loc, sign);
    result = arithmeticAdd(result, constExpr);
  }

  if (accum.any() || ((result == NULL) && (minusExpr == NULL))) {
    // If we have found a negative number, and there is a term
    // besides the integer constant (a variable or a real), then
    // don't add a negative number.  Subtract a positive number.
/*
    if ((result != NULL) && accum.test(size - 1)) {
      accum.negate();
      const NUExpr* constExpr = constant(sign, accum, size, loc);
      minusExpr = arithmeticAdd(minusExpr, constExpr);
    }
    else
*/
    {
      const NUExpr* constExpr = constant(sign, accum, size, loc);
      result = arithmeticAdd(result, constExpr);
    }
  }

  if (minusExpr != NULL) {
    if (result == NULL) {
      result = unaryOp(NUOp::eUnMinus, minusExpr, loc, size);
    }
    else {
      result = binaryOp(NUOp::eBiMinus, result, minusExpr, loc, size);
    }
  }

  if (result != bop) {
    NU_ASSERT(result->determineBitSize() == size, result);
    result = mExprCache->changeSign(result, sign);
  }

  return result;
} // const NUExpr* FoldI::combineArithmetic
