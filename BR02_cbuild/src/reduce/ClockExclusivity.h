// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _CLOCKEXCLUSIVITY_H_
#define _CLOCKEXCLUSIVITY_H_

class NUNetRefFactory;
class AtomicCache;
class MsgContext;
class IODBNucleus;
class ArgProc;
class ESPopulateExpr;
class ExprResynth;
class NUExprFactory;
class NUExpr;
class FLNode;
class NUNet;
class NUModule;
class CarbonExpr;
class Fold;

//! Class to perform exclusivity operations of latches and flops
/*! This class contains utility functions to get the enable condition
 *  for latches, and the edge condition for flops. It uses expression
 *  synthesis to get expressions that can be tested for exclusivity.
 *  It also provides a function to test exclusivity
 */
class REClockExclusivity
{
public:
  //! Constructor
  REClockExclusivity(FLNodeFactory* flowFactory, NUNetRefFactory* netRefFactory,
                     AtomicCache* strCache, MsgContext* msgContext,
                     IODBNucleus* iodb, ArgProc* args);
                     
  //! Destructor
  ~REClockExclusivity();

  //! Initialize the data for the analysis for the given module
  /*! Returns a vector of flop flow nodes if it is non-NULL
   */
  void init(NUModule* mod, FLNodeVector* flops);

  //! Returns true if this device for which enable information is available
  /*! All the variants of getting enable expressions for devices below
   *  only work correctly on valid devices. Call this routine before
   *  calling them if there is any doubt.
   *
   *  This is mostly important for latches because this class only
   *  considers latches for which it can compute the enable
   *  expression. And currently complex if statement structures or
   *  case statements are not supported.
   */
  bool isValidDevice(FLNode* flow);

  //! Function to get the flop/latch enable
  /*! The invert return boolean indicates whether the clkNet is
   *  inverted or not, if one is provided. The return expression is
   *  properly inverted.
   */
  const NUExpr* getDeviceEnable(FLNode* flow, NUNetRefHdl* clkNetRef,
                                bool* invert = NULL);

  //! Function to get the enable off the latch if statement 
  const NUExpr* getLatchEnable(FLNode* flow, NUNetRefHdl* clkNetRef, bool* invert);

  //! Function to get the clock expression off the always block
  const NUExpr* getFlopClock(FLNode* flow, NUNetRefHdl* clkNetRef, bool* invert);

  //! Function to get an expression for the clock tree of the flop/latch enable
  /*! If the enables vector is provided, it is populated with all the
   *  flops and latches from the leafs of the clock tree.
   */
  const NUExpr* getDeviceEnableExpr(FLNode* flow, FLNodeVector* enables,
                                    NUNetRefHdl* rootClk);

  //! Get an expression for a given clock net
  const NUExpr* getClockNetExpr(const NUNetRefHdl& clkNetRef);

  //! Create an inverted expression from a given clock expression (in factory)
  const NUExpr* invertClockExpr(const NUExpr* clkExpr);

  //! Function to determine the exclusivity of two clock/enable expressions
  bool exclusiveExprs(const NUExpr* expr1, const NUExpr* expr2);

  //! Update the device enable for a transformed flop
  void replaceDeviceEnable(FLNode* flow, const NUExpr* cond, 
                           const NUNetRefHdl& clkNetRef, bool invert);

  // Get the d pin for a latch if possible (for simple latches)
  bool getLatchDataExpr(FLNode* flow, const NUExpr** dataExpr, 
                        const NUExpr** latchOut);

private:
  //! Add a flop or latch to our module components
  void addDevice(FLNode* flow, const NUExpr* cond, const NUNetRefHdl& clkNetRef,
                 bool invert);

  //! Abstraction to keep track of an enable condition and clock edge
  class Enable
  {
  public:
    //! empty constructor
    Enable() : mExpr(NULL), mClkNetRef(NULL), mInvert(false) {}

    //! Enable constructor
    Enable(const NUExpr* expr, const NUNetRefHdl& clkNetRef, bool invert) :
      mExpr(expr), mClkNetRef(clkNetRef), mInvert(invert)
    {}

    //! Get the enable expression
    const NUExpr* getExpr(void) const { return mExpr; }

    //! Get the clock net for the enable
    const NUNetRefHdl& getClockNetRef(void) const { return mClkNetRef; }

    //! Get whether the device inverts the expression (negedge flop)
    bool getInvert() const { return mInvert; }

  private:
    //! The enable expression
    const NUExpr* mExpr;

    //! The clock net in the original device
    NUNetRefHdl mClkNetRef;

    //! True if the original expression was a negedge
    bool mInvert;
  }; // class Enable
    
  //! Function to get the clock tree expression for a flop
  /*! If the enables vector is provided, it is populated with all flops and
   *  latches at the leafs of the clock tree.
   */
  const NUExpr* getFlopClockExpr(FLNode* flop, FLNodeVector* enables,
                                 NUNetRefHdl* rootClkRef);

  //! Function to get the clock tree expression for a latch
  /*! If the enables vector is provided, it is populated with all flops and
   *  latches at the leafs of the clock tree.
   */
  const NUExpr* getLatchEnableExpr(FLNode* flow, FLNodeVector* enables,
                                   NUNetRefHdl* rootClkRef);

  //! Forward Class definition
  class ClockGateInfo;

  //! Helper function to compute an expression for a clock net.
  /*! This function uses the ClockGateInfo cache to reduce processing.
   */
  ClockGateInfo* computeClockGateInfo(const NUNetRefHdl& clkNetRef);

  //! Convert a Carbon expression clock tree into clock gating information
  void processExpression(const NUNetRefHdl& exprNetRef, 
                         const NUNetRefHdl& clkNetRef, CarbonExpr* hndl,
                         ClockGateInfo* clkGateInfo);

  //! Walk a gated clock expression and gather the clock and enables
  /*! If this fails to find the data, the clock will be an invalid net
   *  ref.
   */
  void gatherClocksAndEnables(const NUExpr* expr, 
                              NUNetRefHdl* rootClkRef,
                              FLNodeVector* devicesFound);

  //! Utility function to make sure a flow node is something we want to analyze
  /*! This allows us to prune pull drivers and initial blocks which
   *  do not affect the logic value for enables.
   */
  bool isActiveDriver(FLNode* flow);

  //! Get the cached enable expression and net for a module flop or latch
  Enable getDeviceEnable(FLNode* flow);

  //! Find the nested flow for the if statement of a latch
  FLNode* findIfFlow(FLNode* flow);

  //! Utility function to find the offset of a bitsel net ref into a net ref
  ConstantRange normalizeBitSel(const NUNetRefHdl& netRef,
                                const NUNetRefHdl& subNetRef);

  //! Abstraction to keep track of all latches/flops and their enables/clocks
  typedef UtMap<FLNode*, Enable> DeviceEnables;

  //! Storage for all the flops and latches in this module
  DeviceEnables* mDeviceEnables;

  //! Abstraction for a cache of clock gate information for flops
  typedef UtMap<NUNetRefHdl, ClockGateInfo*> ClockGateFlopCache;

  //! Space for the flop clock gate cache
  ClockGateFlopCache* mClockGateFlopCache;

  //! Abstraction for a cache of latch enable information
  typedef UtMap<const NUExpr*, ClockGateInfo*> ClockGateLatchCache;

  //! Space for the latch clock gate cache
  ClockGateLatchCache* mClockGateLatchCache;

  //! Storage for all the devices in the module
  FLNodeSet* mDevices;

  //! Storage for all the clocks in this module
  NUNetRefSet* mClocks;

  //! Context to create BDDs
  BDDContext* mBDDContext;
  
  //! Logic population interface (NUUseDefNode/NUExpr --> CarbonExpr).
  ESPopulateExpr* mPopulateExpr;

  //! Logic resynthesis interface (CarbonExpr --> NUExpr).
  ExprResynth* mExprResynth;

  //! Expression factory - used by resynthesis to create managed expressions.
  NUExprFactory* mExprFactory;

  //! Flow factory to find drivers of nets
  FLNodeFactory* mFlowFactory;

  //! Net ref factory to create clock net refs
  NUNetRefFactory* mNetRefFactory;

  //! Fold class to simplify expressions
  Fold* mFold;

  //! The module being processed
  NUModule* mModule;
}; // class REClockExclusivity

#endif // _CLOCKEXCLUSIVITY_H_
