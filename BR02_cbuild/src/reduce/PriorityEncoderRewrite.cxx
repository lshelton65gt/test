// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "nucleus/NUExpr.h"
#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUBlock.h"
#include "bdd/BDD.h"
#include "localflow/UD.h"
#include "localflow/Reorder.h"

#include "reduce/PriorityEncoderRewrite.h"

PriorityEncoderRewrite::PriorityEncoderRewrite(EncoderStatistics* encoderStats, 
                                               NUNetRefFactory* netRefFactory,
                                               MsgContext* msgContext, 
                                               AtomicCache * strCache,
                                               IODBNucleus* iodb, 
                                               ArgProc* args, 
                                               AllocAlias* alloc_alias,
                                               bool verbose) :
  mEncoderStats(encoderStats),
  mNetRefFactory(netRefFactory),
  mUD(new UD(netRefFactory, msgContext, iodb, args, false)),
  mReorder(new Reorder(strCache, netRefFactory, iodb, args, msgContext, alloc_alias, true, true, false, false)),
  mVerbose(verbose)
{}

PriorityEncoderRewrite::~PriorityEncoderRewrite()
{
  delete mUD;
  delete mReorder;
}

bool PriorityEncoderRewrite::module(NUModule* module)
{
  Callback callback(this);
  NUDesignWalker walker(callback, mVerbose);
  walker.module(module);
  if (callback.statementsModified()) {
    mUD->module(module);
    mEncoderStats->module();
    return true;
  } else {
    return false;
  }
}

SInt64 PriorityEncoderRewrite::rewrite(NUBase * parent,
                                       NUStmtLoop unprocessedStmtsLoop,
				       const SourceLocator& loc,
				       const char* msg,
				       NUStmtList& newStmts)
{
  SInt64 stmtsModified = 0;
  NUStmtList peStmts;
  {
    // Use reordering to order our statements into weakly-connected components.
    NUStmtList unordered_stmts(unprocessedStmtsLoop.begin(),
                               unprocessedStmtsLoop.end());
    NUStmtList ordered_stmts;

    // Future: Walk the statement graph directly rather than the ordered 
    // statement list.
    mReorder->layoutStmtList(&unordered_stmts,&ordered_stmts);
    if (mVerbose) {
      // Dump the reordering, if there was any. This will only print if
      // run with both -verboseRewritePriorityEncoders and -verboseReorder.
      mReorder->dumpOrdering(&unordered_stmts, &ordered_stmts);
    }

    stmtsModified += rewriteMatchingDefIfs(ordered_stmts, peStmts, parent, loc, msg);
  }

  // We may be able to chain if statements that we failed to prioritize 
  // into if-else statements if their conditions are mutually exclusive. 
  // This chain would get converted to case statement in following stages.
  stmtsModified += rewriteMutexCondIfs(peStmts, newStmts);

  // If we didn't modify the statements we can clear the list passed
  // in by our caller
  if (stmtsModified == 0)
    newStmts.clear();

  return stmtsModified;
}

PriorityEncoderRewrite::Callback::Callback(PriorityEncoderRewrite* rewrite) :
  mPriorityEncoderRewrite(rewrite),
  mStmtsModified(false)
{}

PriorityEncoderRewrite::Callback::~Callback()
{}

NUDesignCallback::Status
PriorityEncoderRewrite::Callback::operator()(Phase phase, NUBlock* block)
{
  // If this is the pre phase, keep walking
  if (phase == ePre)
    return eNormal;

  // Call our rewrite function to redo the statements. If it succeeds
  // then process the data.
  NUStmtList newStmts;
  SInt64 numStmts;
  numStmts = mPriorityEncoderRewrite->rewrite(block,
                                              block->loopStmts(),
					      block->getLoc(),
					      " if statements in block",
					      newStmts);
  if (numStmts > 0)
  {
    // The block statement list is stale, update it
    block->replaceStmtList(newStmts);
    mStmtsModified = true;
  }
  return eNormal;
}

NUDesignCallback::Status
PriorityEncoderRewrite::Callback::operator()(Phase phase, NUIf* ifStmt)
{
  // If this is the pre phase, keep walking
  if (phase == ePre)
    return eNormal;

  // Call our rewrite function to redo the then statements. If it
  // succeeds then process the data.
  NUStmtList newStmts;
  SInt64 numStmts;
  numStmts = mPriorityEncoderRewrite->rewrite(ifStmt,
                                              ifStmt->loopThen(),
					      ifStmt->getLoc(),
					      " if statments in then clause",
					      newStmts);
  if (numStmts > 0)
  {
    // The block statement list is stale, update it
    ifStmt->replaceThen(newStmts);
    mStmtsModified = true;
  }

  // Call our rewrite function to redo the else statements. If it
  // succeeds then process the data.
  newStmts.clear();
  numStmts = mPriorityEncoderRewrite->rewrite(ifStmt,
                                              ifStmt->loopElse(),
					      ifStmt->getLoc(),
					      " if statments in else clause",
					      newStmts);
  if (numStmts > 0)
  {
    // The block statement list is stale, update it
    ifStmt->replaceElse(newStmts);
    mStmtsModified = true;
  }
  return eNormal;
}

NUDesignCallback::Status
PriorityEncoderRewrite::Callback::operator()(Phase phase, NUCaseItem* caseItem)
{
  // If this is the pre phase, keep walking
  if (phase == ePre)
    return eNormal;

  // Call our rewrite function to redo the statements. If it succeeds
  // then process the data.
  NUStmtList newStmts;
  SInt64 numStmts;
  numStmts = mPriorityEncoderRewrite->rewrite(caseItem,
                                              caseItem->loopStmts(),
					      caseItem->getLoc(),
					      " if statements in case item",
					      newStmts);
  if (numStmts > 0)
  {
    // The block statement list is stale, update it
    caseItem->replaceStmts(newStmts);
    mStmtsModified = true;
  }
  return eNormal;
}

NUDesignCallback::Status
PriorityEncoderRewrite::Callback::operator()(Phase, NUTaskEnable* enable)
{
  if (enable->isHierRef()) {
    return eSkip;
  } else {
    return eNormal;
  }
}

NUDesignCallback::Status
PriorityEncoderRewrite::Callback::operator()(Phase phase, NUTF* tf)
{
  // If this is the pre phase, keep walking
  if (phase == ePre)
    return eNormal;

  // Call our rewrite function to redo the statements. If it succeeds
  // then process the data.
  NUStmtList newStmts;
  SInt64 numStmts;
  numStmts = mPriorityEncoderRewrite->rewrite(tf,
                                              tf->loopStmts(), tf->getLoc(),
					      " if statments in task/function",
					      newStmts);
  if (numStmts > 0)
  {
    // The function/taks statement list is stale, update it
    tf->replaceStmtList(newStmts);
    mStmtsModified = true;
  }
  return eNormal;
}

NUIf*
PriorityEncoderRewrite::validIf(NUStmt* stmt, 
                                NUNetRefSet& uses,
                                NUNetRefSet& blockingDefs,
				NUNetRefSet& nonBlockingDefs)
{
  // Make sure it is an if statement
  NUIf* ifStmt = NULL;
  if (stmt->getType() == eNUIf)
  {
    // Make sure either the then or else is empty
    ifStmt = dynamic_cast<NUIf*>(stmt);
    NU_ASSERT(ifStmt,stmt);
    if (!ifStmt->emptyThen() && !ifStmt->emptyElse())
      ifStmt = NULL;
    else
    {
      // Gather the def information
      ifStmt->getBlockingDefs(&blockingDefs);
      ifStmt->getNonBlockingDefs(&nonBlockingDefs);

      // Make sure the blocking defs are not also used. We don't care
      // about non-blocking defs since they cannot affect later
      // statements (we don't allow mixed blocking and non-blocking
      // statements or this would not work!!!)
      ifStmt->getBlockingUses(&uses);
      ifStmt->getNonBlockingUses(&uses);

      if (NUNetRefSet::set_has_intersection(blockingDefs, uses)) {
	ifStmt = NULL;
      }

      // Make sure the sub-statements of the if kill the defs. This is
      // a little pessemistic but it avoids a bug where some of the
      // bits of the defs are conditionally killed.
      if (ifStmt != NULL)
      {
	NUNetRefSet blockingKills(mNetRefFactory);
	NUNetRefSet nonBlockingKills(mNetRefFactory);
	gatherKills(ifStmt->loopThen(), blockingKills, nonBlockingKills);
	gatherKills(ifStmt->loopElse(), blockingKills, nonBlockingKills);
	if ((blockingDefs != blockingKills) ||
	    (nonBlockingDefs != nonBlockingKills))
	  ifStmt = NULL;
      }
    }
  } // if
  return ifStmt;
} // PriorityEncoderRewrite:validIf


bool PriorityEncoderRewrite::unrelatedStatement(NUStmt * stmt,
                                                NUNetRefSet &uses,
                                                NUNetRefSet &blockingDefs, 
                                                NUNetRefSet &nonBlockingDefs)
{
  NUNetRefSet thisUses(mNetRefFactory);
  NUNetRefSet thisBlockingDefs(mNetRefFactory);
  NUNetRefSet thisNonBlockingDefs(mNetRefFactory);

  stmt->getBlockingUses(&thisUses);
  stmt->getNonBlockingUses(&thisUses);

  // The discovered if statements define bits used by this statement.
  // As a result, this statement cannot be ordered before the if
  // statements (RAW dependency).
  if (NUNetRefSet::set_has_intersection(blockingDefs, thisUses)) {
    return false;
  }

  stmt->getBlockingDefs(&thisBlockingDefs);

  // The discovered if statements define bits also defined by this
  // statement. As a result, this statement cannot be ordered before
  // the if statements (WAW dependency).
  if (NUNetRefSet::set_has_intersection(blockingDefs, thisBlockingDefs)) {
    return false;
  }

  // The discovered if statements use bits defined by this statement.
  // As a result, this statement cannot be ordered before the if
  // statements (WAR dependency).
  if (NUNetRefSet::set_has_intersection(uses, thisBlockingDefs)) {
    return false;
  }
  
  stmt->getNonBlockingDefs(&thisNonBlockingDefs);

  // The discovered if statements define bits also defined by this
  // statement. As a result, this statement cannot be ordered before
  // the if statements (WAW dependency).
  if (NUNetRefSet::set_has_intersection(nonBlockingDefs, thisNonBlockingDefs)) {
    return false;
  }

  // We don't care about WAR dependencies for non-blocking defs.

  return true;
}


NUStmt*
PriorityEncoderRewrite::convert(IfStmts& ifStmts)
{
  // Go through the statements in order and create one if statement
  // from the if statements. The first statement will have the lowest
  // priority and the last statement will have the highest priority.
  //
  // I've chosen not to mess with the condition so this algorithm puts
  // the current statement into the empty clause of the higher
  // priority statement.

  // The first statement is used as is
  IfStmtsIter i = ifStmts.begin();
  if (i==ifStmts.end()) {
    return NULL; // caller needs to handle NULL return.
  }
  NUStmt* stmt = *i++;

  // Iterator over the remaining statements, adding to the existing
  // statement
  for (; i != ifStmts.end(); ++i)
  {
    // Find the clause to insert it in (the empty one)
    NUIf* ifStmt = *i;
    if (ifStmt->emptyThen()) {
      ifStmt->addThenStmt(stmt);
    } else {
      NU_ASSERT(ifStmt->emptyElse(), ifStmt);
      ifStmt->addElseStmt(stmt);
    }

    // Update our statement to point to the higher priority if
    stmt = ifStmt;
  }

  // stmt now replaces the input if statements
  return stmt;
}

void
PriorityEncoderRewrite::gatherKills(NUStmtLoop stmtsLoop,
				    NUNetRefSet& blockingKills,
				    NUNetRefSet& nonBlockingKills)
{
  for (; !stmtsLoop.atEnd(); ++stmtsLoop)
  {
    NUStmt* stmt = *stmtsLoop;
    stmt->getBlockingKills(&blockingKills);
    stmt->getNonBlockingKills(&nonBlockingKills);
  }
}

// Chain if statements into a priority encoded chain of if-else statements.
SInt64 PriorityEncoderRewrite::rewriteMatchingDefIfs(NUStmtList& inputStmts,
                                                     NUStmtList& outputStmts,
                                                     NUBase * parent,
                                                     const SourceLocator& loc,
                                                     const char* msg)
{
  SInt64 stmtsModified = 0;
  // Visit the statements looking for groups of if statements in a row
  // that have no then or else clause
  NUNetRefSet uses(mNetRefFactory);
  NUNetRefSet blockingDefs(mNetRefFactory);
  NUNetRefSet nonBlockingDefs(mNetRefFactory);
  IfStmts ifStmts;

  NUStmtLoop stmtsLoop(inputStmts);
  while (!stmtsLoop.atEnd())
  {
    // Check if it is a valid if statement with a missing then or else
    // clause. It must also, not use any part of the defs (for
    // blocking assigns)
    NUStmt* stmt = *stmtsLoop;
    ++stmtsLoop;

    NUNetRefSet thisUses(mNetRefFactory);
    NUNetRefSet thisBlockingDefs(mNetRefFactory);
    NUNetRefSet thisNonBlockingDefs(mNetRefFactory);
    NUIf* ifStmt = validIf(stmt, thisUses, thisBlockingDefs, thisNonBlockingDefs);
    
    // Check if there is an existing list for this if statement
    bool match = false;
    if (!ifStmts.empty())
    {
      // If we have an if statement and the vector of if stmts is not
      // empty, see if the defs are the same.
      if ((ifStmt != NULL) && (thisBlockingDefs == blockingDefs) &&
          (thisNonBlockingDefs == nonBlockingDefs))
      {
        // We can add this to the set of if statements to process
        ifStmts.push_back(ifStmt);
        uses.insert(thisUses.begin(), thisUses.end());
        match = true;
      }

      if (stmt->getType()!=eNUIf) {
        // Reordering has grouped statements into weakly connected
        // components, but that does not mean that every statement
        // depends on every other statement. See testcase
        // test/priority-encoder/top9.v
        if (unrelatedStatement(stmt, uses, blockingDefs, nonBlockingDefs)) {
          // Not really a match, but a statement which should not
          // cause our matching to terminate.
          match = true;
        }
      }

      // Process the list if this if statement doesn't match or we are
      // at the end of the stmt list. We can only do work if there is
      // at least two if statements
      if (!match || stmtsLoop.atEnd())
      {
        SInt64 numStmts = ifStmts.size();
        if (numStmts > 1)
        {
          // We have enough if statements to process
          stmtsModified += numStmts;
          NUStmt* newStmt = convert(ifStmts);
          NU_ASSERT(newStmt,parent);
          outputStmts.push_back(newStmt);

          // update information for the user
          mEncoderStats->ifStmt(numStmts);
          if (mVerbose)
          {
            UtString locStr;
            loc.compose(&locStr);
            UtIO::cout() << locStr.c_str() << ": Rewrote " << numStmts
                         << msg << "\n";
          }

        }
        else
        {
          // Can't do anything about this stmt, push it onto the list
          NUStmt* thisStmt = ifStmts.front();
          outputStmts.push_back(thisStmt);
        }

        // Done with the list
        ifStmts.clear();
      }
    }

    // Check if we should start a new list
    if ((ifStmt != NULL) && ifStmts.empty() && !stmtsLoop.atEnd())
    {
      // First if statement found, start recording
      ifStmts.push_back(ifStmt);
      uses = thisUses;
      blockingDefs = thisBlockingDefs;
      nonBlockingDefs = thisNonBlockingDefs;
    }

    // At this point, if ifStmt is NULL, this statement should stay as is
    if ((ifStmt == NULL) || (stmtsLoop.atEnd() && !match))
      outputStmts.push_back(stmt);
  } // while

    // If we have a stmt in the if statements list, it belongs at the
    // end of the new statements
  if (!ifStmts.empty())
  {
    NU_ASSERT(ifStmts.size() == 1, parent);
    outputStmts.push_back(ifStmts.front());
  }

  return stmtsModified;
}

// Check if we have an ordered series of if statements with same uses and
// mutually exclusive conditions. Chain them into if-else statements. There
// could be multiple such groups in the if statements list.
SInt64 PriorityEncoderRewrite::rewriteMutexCondIfs(NUStmtList& inputStmts,
                                                   NUStmtList& outputStmts)
{
  if (inputStmts.size() <= 1)
  {
    outputStmts = inputStmts;
    return 0;
  }

  NUStmtLoop stmtsLoop(inputStmts);
  SInt64 stmtsModified = 0;
  IfStmts mutexIfs;
  BDDContext ctx;
  BDDList mutexIfBdds;
    
  while (!stmtsLoop.atEnd())
  {
    NUStmt* stmt = *stmtsLoop;
    ++stmtsLoop;
    bool isMutexIf = false;
    if (stmt->getType() == eNUIf)
    {
      NUIf* ifStmt = dynamic_cast<NUIf*>(stmt);
      NU_ASSERT(ifStmt, stmt);

      // Pick cases where else is empty and then is not.
      if (!ifStmt->emptyThen() && ifStmt->emptyElse())
      {
        // If this if statement is mutex to all if's in the mutexIfs list
        // or the list is empty, append it to the list.
        isMutexIf = maybeAddMutexIf(ifStmt, mutexIfs, ctx, mutexIfBdds);
      }
    }
      
    // If not a mutex if statement, process the accumulated mutex if list.
    if (!isMutexIf || stmtsLoop.atEnd())
    {
      // Process pending mutex if's into if-else chain.
      SInt32 numIfs = mutexIfs.size();
      if (numIfs > 1)
      {
        NUStmt* newStmt = convert(mutexIfs);
        outputStmts.push_back(newStmt);
        stmtsModified += numIfs;
        mEncoderStats->ifStmt(numIfs);
      }
      // Remove the if statement that we couldn't find an adjacent mutex if for.
      else if (numIfs == 1)
      {
        NUStmt* prevStmt = mutexIfs.back();
        outputStmts.push_back(prevStmt);
      }
        
      // We've taken care of all accumulated if statements.
      if (numIfs > 0)
      {
        mutexIfs.clear();
        mutexIfBdds.clear();
        ctx.clear();
      }

      // Add the non mutexif statement to the new statements list
      if (!isMutexIf)
        outputStmts.push_back(stmt);
    }
  } // while

  if ((stmtsModified > 0) && mVerbose)
  {
    // After priority encoder and mutex chaining
    {
      UtIO::cout() << "After chaining mutex condition ifs: " << UtIO::endl;
      NUStmtLoop verbStmtsLoop(outputStmts);
      while (!verbStmtsLoop.atEnd())
      {
        // Check if it is a valid if statement with a missing then or else clause.
        NUStmt* stmt = *verbStmtsLoop;
        ++verbStmtsLoop;
        stmt->printVerilog();
      }
    }
  }

  return stmtsModified;
} // SInt64 PriorityEncoderRewrite::rewriteMutexCondIfs
      
bool PriorityEncoderRewrite::maybeAddMutexIf(NUIf* ifStmt, IfStmts& mutexIfs,
                                             BDDContext& ctx,
                                             BDDList& mutexIfBdds)
{
  NUExpr* newCond  = ifStmt->getCond();

  // If the new if statement condition doesn't have a bdd, forget about it.
  if (!newCond->hasBdd(&ctx))
    return false;

  // If the new if statement condition bdd is invalid, forget about it.
  BDD newBdd = newCond->bdd(&ctx);
  if (!newBdd.isValid())
    return false;

  NUNetRefSet newUses(mNetRefFactory);
  newCond->getUses(&newUses);
  NUNetRefSet blockingDefs(mNetRefFactory);
  ifStmt->getBlockingDefs(&blockingDefs);

  // If the blocking defs are used in the condition, then the mutex 
  // computation cannot be done statically. The non-blocking defs can be
  // ignored since they cannot affect later statements.
  if (NUNetRefSet::set_has_intersection(blockingDefs, newUses))
    return false;

  // If there are no accumulated if's, add this one readily.
  if (mutexIfs.size() == 0)
  {
    mutexIfs.push_back(ifStmt);
    mutexIfBdds.push_back(newBdd);
    return true;
  }

  IfStmtsIter ifIter = mutexIfs.begin();
  NUExpr* prevCond = (*ifIter)->getCond();
  NUNetRefSet prevUses(mNetRefFactory);
  prevCond->getUses(&prevUses);

  // If the nets the new if uses and one of the accumulated if uses are
  // the not the same, this is not mutex.
  if (newUses != prevUses)
    return false;

  // If the new if condition is not mutually exclusive to all the accumulated
  // if statement conditions, don't add this if.
  bool isMutexIf = true;
  BDDLoop bddLoop(mutexIfBdds);
  for (/* no initial */; isMutexIf && !bddLoop.atEnd(); ++bddLoop)
  {
    BDD andedBdd = ctx.opAnd(*bddLoop, newBdd);
    if (andedBdd != ctx.val0())
      isMutexIf = false;
  }

  if (isMutexIf)
  {
    mutexIfs.push_back(ifStmt);
    mutexIfBdds.push_back(newBdd);
  }

  return isMutexIf;
} // bool PriorityEncoderRewrite::maybeAddMutexIf

void EncoderStatistics::print() const
{
  UtIO::cout() << "Encoder Summary: "
	       << mIfStmts << " chained if statements in "
	       << mModules << " modules." << UtIO::endl;
}
