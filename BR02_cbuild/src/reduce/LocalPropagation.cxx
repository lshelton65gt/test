// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "util/SetOps.h"

#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NULvalue.h"

#include "flow/FLNode.h"

#include "reduce/Fold.h"
#include "reduce/LocalPropagationBase.h"

#include "LocalPropagation.h"

LocalPropagationCallback::LocalPropagationCallback(IODBNucleus* iodbNucleus, 
                                                   Fold* fold,
                                                   LocalPropagationStatistics * statistics,
                                                   bool storing_non_constants) :
  mLPBase(fold, statistics, storing_non_constants),
  mIODBNucleus(iodbNucleus) 
{
}

LocalPropagationCallback::~LocalPropagationCallback()
{
}

LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUStmt* stmt)
{
  if (phase == ePost) {
    applyNonPropagatedDefs(stmt);
  }
  return eNormal;
}

LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUFor* forStmt)
{
  if (phase == ePre) {

    // Clear out any loop-defs from the context before we begin. We do
    // not want to propagage any external definitions for anything
    // also defined by the loop.
    applyNonPropagatedDefs(forStmt);

    NUDesignWalker walker(*this, false);
    walker.putWalkTasksFromTaskEnables(false);

    // Walk each clause of the loop with its own context. This
    // prevents anything defined within the loop from propagating out
    // of the loop.

    pushContext();
    for (NUStmtLoop l = forStmt->loopInitial(); !l.atEnd(); ++l) {
      NUStmt* stmt = *l;
      walker.stmt(stmt);
    }
    popContext();

    pushContext();
    for (NUStmtLoop l = forStmt->loopBody(); !l.atEnd(); ++l) {
      NUStmt* stmt = *l;
      walker.stmt(stmt);
    }
    popContext();

    pushContext();
    for (NUStmtLoop l = forStmt->loopAdvance(); !l.atEnd(); ++l) {
      NUStmt* stmt = *l;
      walker.stmt(stmt);
    }
    popContext();
  }

  // Don't re-process the loop.
  return eSkip;
}

LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUIf* ifStmt)
{
  // Walk the then and else clauses separately. Each with its own set
  // of constant nets.
  if (phase == ePre) {
    // Walk the condition so that we propagate constants into it
    NUExpr* expr = ifStmt->getCond();
    NUExpr* newExpr = optimizeExpr(expr);
    if (newExpr != NULL) {
      ifStmt->replaceCond(newExpr);
    }

    NUDesignWalker walker(*this, false);
    walker.putWalkTasksFromTaskEnables(false);

    expr = ifStmt->getCond();
    NUConst * constExpr = expr->castConst();
    // Do not assume X/Z constants force the if statement to evaluate
    // one way or another.
    if (constExpr and not constExpr->hasXZ()) {
      // The if condition is a constant, meaning it will evaluate to
      // true or false. If true, we walk only the then clause. If
      // false, we walk only the else clause. In both cases, we do not
      // need to worry about pushing a constant context; the current
      // context is sufficient.
      if (constExpr->isZero()) {
        // False evaluation. Walk only the else clause.
        for (NUStmtLoop l = ifStmt->loopElse(); !l.atEnd(); ++l) {
          NUStmt* stmt = *l;
          walker.stmt(stmt);
        }
      } else {
        // True evaluation. Walk only the then clause.
        for (NUStmtLoop l = ifStmt->loopThen(); !l.atEnd(); ++l) {
          NUStmt* stmt = *l;
          walker.stmt(stmt);
        }
      }
    } else {
      // Start a new nesting context for the if so we can gather the
      // then and else assignments and apply them to the parent context
      pushNesting();

      // Walk the then with its own context. We can sometimes treat the
      // if condition as a constant expression.
      NUDesignWalker walker(*this, false);
      walker.putWalkTasksFromTaskEnables(false);
      pushContext();
      enterIfBranch(ifStmt, true);
      for (NUStmtLoop l = ifStmt->loopThen(); !l.atEnd(); ++l) {
        NUStmt* stmt = *l;
        walker.stmt(stmt);
      }
      popContext();

      // And now the else
      pushContext();
      enterIfBranch(ifStmt, false);
      for (NUStmtLoop l = ifStmt->loopElse(); !l.atEnd(); ++l) {
        NUStmt* stmt = *l;
        walker.stmt(stmt);
      }
      popContext();

      // Apply the nested constant nets
      popNesting();
    } // } else
  } // if

  // Don't process the if statements any more. They were done above
  return eSkip;
} // LocalPropagationCallback::operator
    
LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUCase* caseStmt)
{
  // Walk all the case items separately, each with its own set of
  // constant nets. If this is not a full case, we create an empty
  // context to indicate that case.
  if (phase == ePre) {
    // Walk the select condition so that we propagate constants into it
    NUExpr* expr = caseStmt->getSelect();
    NUExpr* newExpr = optimizeExpr(expr);
    if (newExpr != NULL) {
      caseStmt->replaceSelect(newExpr);
    }

    // Start a new nesting context for the if so we can gather the
    // then and else assignments and apply them to the parent context
    pushNesting();

    // If this is not a full case, do a push and pop context to
    // indicate that the nets are not defined in some branches.
    NUDesignWalker walker(*this, false);
    walker.putWalkTasksFromTaskEnables(false);
    if (!caseStmt->isFullySpecified() && !caseStmt->isUserFullCase() &&
        !caseStmt->hasDefault()) {
      pushContext();
      popContext();
    }

    // Walk the case items and creating a nested context for each
    for (NUCase::ItemLoop l = caseStmt->loopItems(); !l.atEnd(); ++l) {
      NUCaseItem* item = *l;
      pushContext();
      walker.caseItem(item);
      popContext();
    }

    // Apply the nested constant nets to the parent context
    popNesting();
  } // if

  // Don't process the case statements any more. They were done above
  return eSkip;
}

LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUAssign* assign)
{
  if (phase == ePre) {
    // Optimize the rhs
    NUExpr* expr = assign->getRvalue();
    NUExpr* newExpr = optimizeExpr(expr);
    if (newExpr != NULL) {
      assign->replaceRvalue(newExpr);
    }
  } else {
    // clear any defs that need disqualifying
    (*this)(NUDesignCallback::ePost, (NUStmt*)assign);
    // add defs from this assignment.
    enrollAssign(assign);
  }
  return eNormal;
}

LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUVarselLvalue* lval)
{
  if (phase == ePre) {
    NUExpr* expr = lval->getIndex();
    NUExpr* newExpr = optimizeExpr(expr);
    if (newExpr != NULL) {
      lval->replaceIndex(newExpr);
    }
  }

  // Don't want to skip because the ident may be a memsellvalue
  return eNormal;
}

LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUMemselLvalue* lval)
{
  if (phase == ePre) {
    NUExpr* expr = lval->getIndex(0);
    NUExpr* newExpr = optimizeExpr(expr);
    if (newExpr != NULL) {
      lval->replaceIndex(0, newExpr);
    }
  }

  // We can probably skip at this point, but I'm not sure it is worth
  // it
  return eNormal;
}

LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUCaseCondition* cond)
{
  // Don't optimize masked expressions; not sure about the ramifications
  if ((phase == ePre) && (cond->getMask() == NULL)) {
    NUExpr* expr = cond->getExpr();
    NUExpr* newExpr = optimizeExpr(expr);
    if (newExpr != NULL) {
      cond->replaceExpr(newExpr);
    }
  }

  // No more optimizations are possible
  return eSkip;
}


LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUStructuredProc* proc)
{
  if (phase == ePre) {
    // Push a nesting so we can promote out of this context.
    if (proc->getType()==eNUAlwaysBlock and not proc->isSequential()) {
      pushNesting();
    }
    pushContext();
    clearChanged();
  } else {
    popContext();
    // Pop the nesting to promote out of this context.
    if (proc->getType()==eNUAlwaysBlock and not proc->isSequential()) {
      popNesting(false);
    }
    if (mChanged) { 
      getStatistics()->alwaysBlock(); 
    }
  }
  return eNormal;
}


LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUTask *)
{
  if (phase == ePre) {
    pushContext();
    clearChanged();
  } else {
    popContext();
    if (mChanged) { 
      getStatistics()->task(); 
    }
  }
  return eNormal;
}


LocalPropagationCallback::Status
LocalPropagationCallback::operator()(Phase phase, NUContAssign * assign)
{
  if (phase == ePre) {
    clearChanged();
  }

  Status status = (*this)(phase, (NUAssign*)assign);

  if (phase == ePost) {
    if (mChanged) { 
      getStatistics()->contAssign(); 
    }
  }
  return status;
}


void LocalPropagationCallback::pushContext()
{
  mLPBase.pushContext();
}


void LocalPropagationCallback::popContext()
{
  mLPBase.popContext();
}


void LocalPropagationCallback::addNet(NUNet* net, NUExpr * incoming_expr)
{
  mLPBase.addNet(net, incoming_expr);
} // void LocalPropagationCallback::addNet


void LocalPropagationCallback::pushNesting()
{
  mLPBase.pushNesting();
}

void LocalPropagationCallback::popNesting(bool preserve_non_statics)
{
  mLPBase.popNesting(preserve_non_statics);
} // void LocalPropagationCallback::popNesting
  
NUExpr * LocalPropagationCallback::optimizeExpr(NUExpr* expr)
{
  return mLPBase.optimizeExpr(expr, &mChanged);
} // LocalPropagationCallback::optimizeExpr

void LocalPropagationCallback::applyNonPropagatedDefs(NUStmt* stmt)
{
  NUNetSet defs;
  if (stmt->useBlockingMethods()) {
    stmt->getBlockingDefs(&defs);
  } else {
    stmt->getDefs(&defs);
  }
  mLPBase.applyNonPropagatedDefs(defs);
}

LocalPropagationStatistics* LocalPropagationCallback::getStatistics() const
{
  return mLPBase.getStatistics();
}

void LocalPropagationStatistics::print(const char * optimization_name) const
{
  UtIO::cout() << optimization_name << " Summary: Optimized " 
               << mModules      << " modules."
               << UtIO::endl;
  UtIO::cout() << "    Optimized " 
               << mReplacements << " expressions in " 
               << mTasks        << " tasks, "
               << mContAssigns  << " continuous assignments and " 
               << mAlwaysBlocks << " always blocks." << UtIO::endl;
}


class ContinuousDriverDFSWalker : public GraphSortedWalker
{
public:
  ContinuousDriverDFSWalker(NUUseDefVector * drivers) :
    mDrivers(drivers) 
  {}

  ~ContinuousDriverDFSWalker() {}

  Command visitNodeAfter(Graph * graph, GraphNode * node) {
    LocalPropagationBuilder::ContinuousDriverGraph * cdGraph = dynamic_cast<LocalPropagationBuilder::ContinuousDriverGraph*>(graph);
    LocalPropagationBuilder::ContinuousDriverGraph::Node * cdNode = cdGraph->castNode(node);
    NUUseDefNode * driver = cdNode->getData();
    mDrivers->push_back(driver);
    return GW_CONTINUE;
  }
  
private:
  NUUseDefVector * mDrivers;
};


class ContinuousDriverDFSWalkerNodeCmp : public GraphSortedWalker::NodeCmp
{
public:
  ContinuousDriverDFSWalkerNodeCmp(LocalPropagationBuilder::ContinuousDriverGraph * cdGraph) :
    mGraph(cdGraph)
  {}

  //! ordering function
  bool operator()(const GraphNode* n1, const GraphNode* n2) const {
    return compare(n1, n2) < 0;
  }

private:
  //! Compare function
  int compare(const GraphNode* n1, const GraphNode* n2) const {
    // Test for easy case first
    if (n1 == n2) {
      return 0;
    }

    const LocalPropagationBuilder::ContinuousDriverGraph::Node * cdNode1 = mGraph->castNode(n1);
    const LocalPropagationBuilder::ContinuousDriverGraph::Node * cdNode2 = mGraph->castNode(n2);
    NUUseDefNode * driver1 = cdNode1->getData();
    NUUseDefNode * driver2 = cdNode2->getData();
    
    return NUUseDefNode::compare(driver1, driver2);
  }

  LocalPropagationBuilder::ContinuousDriverGraph * mGraph;
};


class ContinuousDriverDFSWalkerEdgeCmp : public GraphSortedWalker::EdgeCmp
{
public:
  ContinuousDriverDFSWalkerEdgeCmp(LocalPropagationBuilder::ContinuousDriverGraph * cdGraph,
                                   ContinuousDriverDFSWalkerNodeCmp * nodeCmp) :
    mGraph(cdGraph),
    mNodeCmp(nodeCmp)
  {}

  //! ordering function
  bool operator()(const GraphEdge* e1, const GraphEdge* e2) const {
    // Sort by target node order
    const GraphNode* n1 = mGraph->endPointOf(e1);
    const GraphNode* n2 = mGraph->endPointOf(e2);
    return (*mNodeCmp)(n1, n2);
  }
private:
  LocalPropagationBuilder::ContinuousDriverGraph * mGraph;
  ContinuousDriverDFSWalkerNodeCmp * mNodeCmp;
};


void LocalPropagationBuilder::getDepthOrder(NUModule * module, NUUseDefVector * depth_order)
{
  ContinuousDriverGraph * graph = buildContinuousDriverGraph(module);

  ContinuousDriverDFSWalkerNodeCmp node_cmp(graph);
  ContinuousDriverDFSWalkerEdgeCmp edge_cmp(graph,&node_cmp);
  ContinuousDriverDFSWalker graph_walker(depth_order);
  graph_walker.walk(graph,&node_cmp,&edge_cmp);

  delete graph;
}


LocalPropagationBuilder::ContinuousDriverGraph * LocalPropagationBuilder::buildContinuousDriverGraph(NUModule * module)
{
  GraphBuilder<ContinuousDriverGraph> builder;

  NUNetList allNets;
  module->getAllNonTFNets(&allNets);
  
  for (NUNetList::iterator i=allNets.begin(); i!=allNets.end(); ++i) {
    NUNet * net = *i;
    for (NUNet::DriverLoop l=net->loopContinuousDrivers(); !l.atEnd(); ++l) {
      FLNode * flow = *l;
      NUUseDefNode * driver = flow->getUseDefNode();
      if (driver and (driver->getType() == eNUContAssign or
                      driver->getType() == eNUAlwaysBlock)) {
        builder.addNode(driver);
        for (FLNode::FaninLoop floop=flow->loopFanin(); !floop.atEnd(); ++floop) {
          FLNode * fanin = *floop;
          NUUseDefNode * fanin_driver = fanin->getUseDefNode();
          // Don't add edges to a fanin sequential block. We aren't
          // propagating out of sequential blocks, so this dependency
          // is unnecessary.
          if (fanin_driver and not fanin_driver->isSequential()) {
            builder.addNode(fanin_driver);
            builder.addEdgeIfUnique(driver, fanin_driver, NULL);
          }
        }
        // Don't add arcs for edge-fanin. We aren't propagating into
        // edge expressions; these dependencies are unnecessary.
      }
    }
  }
  return builder.getGraph();
}


