// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/CleanUnelabFlow.h"
#include "reduce/UnelabMarkSweep.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "flow/FLFactory.h"
#include "iodb/IODBNucleus.h"
#include "util/UtIOStream.h"
#include "util/OSWrapper.h"

/*!
  \file
  Implementation of unelaborated flow cleanup package.
 */


void CleanUnelabFlow::design(NUDesign *this_design)
{
  UnelabMarkSweep marker(mFlowFactory, mIODB, mStartAtAllNets);
  marker.mark(this_design);

  deleteDeadFlow();
}


void CleanUnelabFlow::deleteDeadFlow()
{
  if (mVerbose) {
    UtIO::cout() << "CleanUnelabFlow found the following dead flow:" << UtIO::endl;
  }

  mDeadFlowFound = false;
  Callback callback(this);

  UnelabMarkSweep marker(mFlowFactory, mIODB, mStartAtAllNets);
  marker.sweep(callback);

  if (mVerbose and not mDeadFlowFound) {
    UtIO::cout() << "  No unelaborated dead flow was found." << UtIO::endl;
  }
}


void CleanUnelabFlow::removeFlow(FLNode *flnode, bool is_dead)
{
  if (not is_dead) {
    return;
  }

  if (not flnode->getMark()) {
    if (mVerbose) {
      flnode->print(0,2);
    }
    flnode->getDefNet()->removeContinuousDriver(flnode);
    mFlowFactory->destroy(flnode);
    mDeadFlowFound = true;
  }
}
