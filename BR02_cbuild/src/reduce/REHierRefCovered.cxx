// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "REHierRefCovered.h"

#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUNet.h"

#include "symtab/STBranchNode.h"

#include "util/GenericDigraph.h"
#include "util/GraphBuilder.h"
#include "util/GraphSCC.h"
#include "util/GraphDumper.h"

#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"

/*!
  \file 
  Implementation of class used to discover nets covered by hierrefs in the port-connection graph.
 */

REHierRefCovered::REHierRefCovered(NUDesign * design, 
                                   STSymbolTable * symbol_table,
                                   MsgContext * msg_context) :
  mDesign(design),
  mSymbolTable(symbol_table),
  mMsgContext(msg_context)
{
}


REHierRefCovered::~REHierRefCovered()
{
}


//! Digraph with port connections as edge-data and nets as node-data.
/*!
 * In order for this cyclic check to be accurate, the port connection
 * graph must be in terms of elaborated names. An unelaborated port
 * connection graph will contain false cycles
 */
typedef GenericDigraph<NUPortConnection*,STAliasedLeafNode*> PortConnectionGraph;


//! Walk Nucleus and build a graph from encountered port connections.
class BuildPortConnectionGraph : public NUInstanceCallback
{
public:
  //! Constructor
  BuildPortConnectionGraph(STSymbolTable * symbol_table) :
    NUInstanceCallback(symbol_table)
  {}

  //! Destructor
  ~BuildPortConnectionGraph() {}

  Status operator()(Phase,NUBase*) { return eNormal; }

  //! Create graph nodes for the actual and formal and an arc between the two.
  /*!
   * Is either the formal or the actual has hierarchial referrers,
   * edges to those referrers are also added. These hierarchical
   * reference edges are used to detect cycles in the alias graph.
   *
   * Hierarchical reference edges are marked with
   * eEdgeHierarchicalReference, while port connection edges are
   * marked with eEdgePortConnection.
   *
   * \sa HierRefCycleDumper
   */
  Status operator()(Phase phase, NUPortConnection* connection);

  //! Return the constructed graph.
  /*!
   * Memory management of the graph is the responsibility of the caller.
   */
  PortConnectionGraph * getGraph() { return mBuilder.getGraph(); }

  void handleModule(STBranchNode *, NUModule *) {}
  void handleInstance(STBranchNode *, NUModuleInstance *) {}
  void handleDeclScope(STBranchNode *, NUNamedDeclarationScope *) {}

private:
  //! Add eEdgeHierarchicalReference-marked edges for the given net.
  void addHierRefEdges(NUNet * net, STAliasedLeafNode * leaf);

  //! The graph builder.
  GraphBuilder<PortConnectionGraph> mBuilder;
};


//! Subclass of generic root finder - do not consider eEdgeHierarchicalReference edges in the root analysis.
/*!
 * This class finds the roots of the port connection graph. The roots
 * are used to determine which parts of the port connection graph are
 * covered by hierarchical references.
 * 
 * Hierarchical reference edges are not considered during the root
 * analysis.
 */
class PortConnectionRootFinder : public GraphRootFinder
{
public:
  //! Constructor.
  PortConnectionRootFinder() : GraphRootFinder() {}

  //! Destructor.
  ~PortConnectionRootFinder() {}

  //! Disallow hierref edges from being analyzed during root discovery.
  bool processEdge(Graph * g, GraphEdge * edge) {
    REHierRefCovered::PortConnectionEdgeFlags flags = 
      (REHierRefCovered::PortConnectionEdgeFlags)g->getFlags(edge);
    return (flags&REHierRefCovered::eEdgePortConnection);
  }
private:
};


/*!
 * Walk the port-connection edges of a graph and mark all encountered
 * nodes as covered by hierarchical reference port connections. This
 * walker is used when a root of the port connection graph is a
 * hierarchical reference. All nodes in the port connection tree for
 * that hierarchical reference root will be marked as covered by a
 * hierarchical reference.
 *
 * We cannot distinguish between tree, back and cross edges in our
 * walk because the walk considers both eEdgePortConnection and
 * eEdgeHierarchicalReference edges. 
 * 
 * When marking nodes as covered by hier-refs, we really want to
 * process only eEdgePortConnection but didn't want to construct a
 * second graph with only these edges.
 */
class HierRefPortConnectionWalker : public GraphWalker
{
public:
  //! Constructor
  HierRefPortConnectionWalker() {}

  //! Destructor
  ~HierRefPortConnectionWalker() {}

  //! Mark a node as covered by a hierarchical reference.
  /*!
   * If a node is already marked, do not allow re-traversal.
   */
  Command visitNodeBefore(Graph * graph, GraphNode * node);

  //! Allow walking over all port-connection edges.
  Command visitTreeEdge(Graph * graph, GraphNode * node, GraphEdge * edge);

  //! Allow walking over all port-connection edges.
  Command visitCrossEdge(Graph * graph, GraphNode * node, GraphEdge * edge);

  //! Allow walking over all port-connection edges.
  Command visitBackEdge(Graph * graph, GraphNode * node, GraphEdge * edge);
};


//! Print out any cyclic hierarchical references.
class HierRefCycleDumper : public UtGraphDumper
{
public:
  //! Constructor.
  HierRefCycleDumper(MsgContext * msg_context, UtOStream& out) :
    UtGraphDumper(out),
    mMsgContext(msg_context)
  {}

  //! Determine if a graph has any cycles and dump them.
  /*!
   * \return true if any cycles were found.
   */
  bool dumpCycles(PortConnectionGraph * graph);

private:
  //! Overloaded function to compose a string for an edge
  void edgeString(Graph*, GraphNode*, GraphEdge*, UtString* str);

  //! Overloaded function to compose a string for a node
  void nodeString(Graph*, GraphNode*, UtString* str);

  //! Message context.
  MsgContext * mMsgContext;
};


bool REHierRefCovered::discover(NUNetSet * covered_by_hier_ref)
{
  // Build the port connection graph. Each net (formal and actual) is
  // referenced by a node in this graph. Edges correspond to the
  // presence of a port connection or a hierarchical reference. The
  // two types of edges can be distinguished by a mark of either
  // eEdgePortConnection or eEdgeHierarchicalReference.
  BuildPortConnectionGraph callback(mSymbolTable);
  NUDesignWalker design_walker(callback, false, false);
  design_walker.design(mDesign);

  PortConnectionGraph * graph = callback.getGraph();

  // Process all the strongly-connected components of the graph. If
  // any component is cyclic, we have an unallocatable situation.
  HierRefCycleDumper dumper(mMsgContext, UtIO::cout());
  bool success = not dumper.dumpCycles(graph);
  if (not success) {
    return false; // no legal allocation.
  }

  // Find all the roots in the graph. Roots are discovered by
  // considering only port-connection arcs and not arcs created
  // through hierarchical reference resolutions.
  UtSet<GraphNode*> roots;
  PortConnectionRootFinder root_finder;
  root_finder.findRoots(graph, &roots);

  // Reset the graph before any walks.
  HierRefPortConnectionWalker hier_ref_marker;
  hier_ref_marker.reset(graph);

  // Walk the roots of the graph. If the root is a hierarchical
  // reference, mark all covered nodes as present in the port
  // connection tree of a hierarchical reference.
  for (UtSet<GraphNode*>::iterator iter = roots.begin();
       iter != roots.end();
       ++iter) {
    PortConnectionGraph::Node* root_node = graph->castNode(*iter);
    STAliasedLeafNode * root_leaf = root_node->getData();
    NUNet * root_net = NUNet::find(root_leaf);

    if (root_net->isHierRef()) {
      // walk the graph covered by root_node and mark as covered by hierref.
      hier_ref_marker.walkFrom(graph,root_node);
    }
  }

  // Walk all nodes in the graph. If a node has been marked as covered
  // by a hierarchical reference, add it to the set provided by the
  // caller.
  for (Iter<GraphNode*> iter = graph->nodes();
       not iter.atEnd();
       ++iter) {
    PortConnectionGraph::Node* node = graph->castNode(*iter);
    REHierRefCovered::PortConnectionNodeFlags flags = 
      (REHierRefCovered::PortConnectionNodeFlags)graph->getFlags(node);
    if (flags&REHierRefCovered::eNodeHierRefCovered) {
      STAliasedLeafNode * covered_leaf = node->getData();
      NUNet * covered_net = NUNet::find(covered_leaf);
      covered_by_hier_ref->insert(covered_net);
    }
  }

  delete graph;

  return true;
}


GraphWalker::Command HierRefPortConnectionWalker::visitNodeBefore(Graph * graph, GraphNode * node)
{
  REHierRefCovered::PortConnectionNodeFlags flags = 
    (REHierRefCovered::PortConnectionNodeFlags)graph->getFlags(node);
  if (flags&REHierRefCovered::eNodeHierRefCovered) {
    // Do not re-walk already marked subgraphs.
    return GW_SKIP;
  } else {
    // Mark this unmarked node and continue.
    graph->setFlags(node,REHierRefCovered::eNodeHierRefCovered);
    return GW_CONTINUE;
  }
}


GraphWalker::Command HierRefPortConnectionWalker::visitTreeEdge(Graph * graph, GraphNode *, GraphEdge * edge)
{
  REHierRefCovered::PortConnectionEdgeFlags flags = 
    (REHierRefCovered::PortConnectionEdgeFlags)graph->getFlags(edge);
  if (flags&REHierRefCovered::eEdgePortConnection) {
    return GW_CONTINUE;
  } else {
    return GW_SKIP;
  }
}


GraphWalker::Command HierRefPortConnectionWalker::visitCrossEdge(Graph * graph, GraphNode *, GraphEdge * edge)
{
  REHierRefCovered::PortConnectionEdgeFlags flags = 
    (REHierRefCovered::PortConnectionEdgeFlags)graph->getFlags(edge);
  if (flags&REHierRefCovered::eEdgePortConnection) {
    return GW_CONTINUE;
  } else {
    return GW_SKIP;
  }
}


GraphWalker::Command HierRefPortConnectionWalker::visitBackEdge(Graph * graph, GraphNode *, GraphEdge * edge)
{
  REHierRefCovered::PortConnectionEdgeFlags flags = 
    (REHierRefCovered::PortConnectionEdgeFlags)graph->getFlags(edge);
  if (flags&REHierRefCovered::eEdgePortConnection) {
    return GW_CONTINUE;
  } else {
    return GW_SKIP;
  }
}

NUDesignCallback::Status BuildPortConnectionGraph::operator()(Phase phase, NUPortConnection * connection)
{
  if (phase==ePre) {
    NUNet * actual = connection->getActualNet();
    NUNet * formal = connection->getFormal();

    STBranchNode * formal_hierarchy = getCurrentNode();
    STBranchNode * actual_hierarchy = formal_hierarchy->getParent();

    STAliasedLeafNode * actual_leaf = actual->lookupElabSymNode(actual_hierarchy);
    STAliasedLeafNode * formal_leaf = formal->lookupElabSymNode(formal_hierarchy);

    // Make sure the graph has nodes for both the actual and the formal.
    mBuilder.addNode(actual_leaf,REHierRefCovered::eNodeUncovered);
    mBuilder.addNode(formal_leaf,REHierRefCovered::eNodeUncovered);

    // Add an edge from the actual to the formal.
    mBuilder.addEdgeIfUnique(actual_leaf,formal_leaf,connection,REHierRefCovered::eEdgePortConnection);

    // Add an edge from each hierarchical reference actual to its resolution.
    // These edges are used to detect hierarchical reference cycles.
    // Formals cannot be hierarchical references
    addHierRefEdges(actual,actual_leaf);
  }
  return eNormal;
}


void BuildPortConnectionGraph::addHierRefEdges(NUNet * net, STAliasedLeafNode * leaf)
{
  if (not net->isHierRef()) {
    return;
  }

  NUHierRef* hier_ref = net->getHierRef();
  NU_ASSERT(hier_ref, net);
  STSymbolTableNode * resolved_node = NUHierRef::resolveHierRef(hier_ref->getPath(), 0,
                                                                getSymtab(), leaf->getParent());
  NU_ASSERT(resolved_node,net);
  STAliasedLeafNode * resolved_leaf = resolved_node->castLeaf();

  // Make sure the graph has a node for this resolution.
  mBuilder.addNode(resolved_leaf);

  // Add an edge from the resolution to its referrer.
  mBuilder.addEdgeIfUnique(resolved_leaf,leaf,NULL,REHierRefCovered::eEdgeHierarchicalReference);
}


//! Sort nodes in the port-connection graph.
/*!
 * Sort port connection nodes by their data, the NUNets. The sort is
 * required by the graph dumper to produce stable output.
 */
class PortConnectionNodeOrder : public GraphSortedWalker::NodeCmp
{
public:
  PortConnectionNodeOrder(Graph * g) { mGraph = dynamic_cast<PortConnectionGraph*>(g); }
  bool operator()(const GraphNode* n1, const GraphNode* n2) const {
    const PortConnectionGraph::Node * pcn1 = mGraph->castNode(n1);
    const PortConnectionGraph::Node * pcn2 = mGraph->castNode(n2);
    STAliasedLeafNode * leaf1 = pcn1->getData();
    STAliasedLeafNode * leaf2 = pcn2->getData();
    return (HierName::compare(leaf1,leaf2) < 0);
  }
private:
  PortConnectionGraph * mGraph;
};


//! Sort edges in the port-connection graph.
/*!
 * Sort port connection edges by the data of their end points, the
 * NUNets. The sort is required by the graph dumper to produce stable
 * output.
 */
class PortConnectionEdgeOrder : public GraphSortedWalker::EdgeCmp
{
public:
  PortConnectionEdgeOrder(Graph* g) { mGraph = dynamic_cast<PortConnectionGraph*>(g); }
  bool operator()(const GraphEdge* e1, const GraphEdge* e2) const
  {
    GraphNode * n1 = mGraph->endPointOf(e1);
    GraphNode * n2 = mGraph->endPointOf(e2);

    PortConnectionGraph::Node * pcn1 = mGraph->castNode(n1);
    PortConnectionGraph::Node * pcn2 = mGraph->castNode(n2);
    STAliasedLeafNode * leaf1 = pcn1->getData();
    STAliasedLeafNode * leaf2 = pcn2->getData();
    return (HierName::compare(leaf1,leaf2) < 0);
  }
private:
  PortConnectionGraph * mGraph;
};


bool HierRefCycleDumper::dumpCycles(PortConnectionGraph * graph)
{
  bool has_cycles = false;

  PortConnectionNodeOrder node_order(graph);
  PortConnectionEdgeOrder edge_order(graph);

  GraphSCC scc;
  scc.compute(graph);
  for (GraphSCC::ComponentLoop loop = scc.loopCyclicComponents(); 
       not loop.atEnd(); ++loop) {
      GraphSCC::Component* component = *loop;
      Graph * cyclic_graph = scc.extractComponent(component);
      mMsgContext->RECyclicHierRefs();
      dump(cyclic_graph, &node_order, &edge_order, "-");
      delete cyclic_graph;
      has_cycles = true;
  }

  return has_cycles;
}


void HierRefCycleDumper::edgeString(Graph* graph, GraphNode* /*node*/, GraphEdge* edge, UtString* str)
{
  PortConnectionGraph * pcGraph = dynamic_cast<PortConnectionGraph*>(graph);
  PortConnectionGraph::Edge * pcEdge = pcGraph->castEdge(edge);

  REHierRefCovered::PortConnectionEdgeFlags flags = 
    (REHierRefCovered::PortConnectionEdgeFlags)pcGraph->getFlags(pcEdge);
  if (flags&REHierRefCovered::eEdgePortConnection) {
    (*str) = "PORT";
  } else {
    (*str) = "HREF";
  }
}


void HierRefCycleDumper::nodeString(Graph* graph, GraphNode* node, UtString* buf)
{
  PortConnectionGraph * pcGraph = dynamic_cast<PortConnectionGraph*>(graph);
  PortConnectionGraph::Node * pcNode = pcGraph->castNode(node);
  STAliasedLeafNode * leaf = pcNode->getData();
  (*buf) << " : ";
  leaf->compose(buf);
}
