// -*-C++-*-    $Revision: 1.5 $
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/Nucleus.h"
#include "reduce/Inference.h"
#include "reduce/CommonConcat.h"
#include "reduce/OccurrenceLogger.h"
#include "util/ArgProc.h"
#include "util/MsgContext.h"
#include "util/OSWrapper.h"
#include "util/SourceLocator.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"
#include "vectorise/PortVectorisation.h"
#if pfMSVC
#include <io.h>
#else
#include <unistd.h>
#endif

//! Wrapper opens the common-concat log if -logCommonConcat is specified
OccurrenceLogger::Wrapper::Wrapper (MsgContext &msg_context, const ArgProc &args, const char *root)
{
  if (args.getBoolValue ("-logCommonConcat")) {
    CommonConcat::openOccurrenceLog (msg_context, root);
  }
  if (args.getBoolValue ("-logPortVec")) {
    PortVectoriser::openOccurrenceLog (msg_context, root, "portvec");
  }
}

//! Close any open logs
OccurrenceLogger::Wrapper::~Wrapper ()
{
  CommonConcat::closeOccurrenceLog ();
  PortVectoriser::closeOccurrenceLog ();
}

OccurrenceLogger::OccurrenceLogger (MsgContext &msg_context, const char *root, const char *ident, 
  const UInt32 flags, const char *dirname) :
  mMsgContext (msg_context), mFlags (flags), mIdent (ident), 
  mFD (-1), mLength (0), mNArgs (0), mTotalCount (0)
{
  mSize = BUFFER_SIZE;
  mLineBuffer = new char [BUFFER_SIZE];
  mFilename [0] = '\0';
  openlog (root, ident, dirname);
  for (UInt32 n = 0; n < cMAXKEYS; n++) {
    mCount [n] = 0;
  }
}

/*virtual*/ OccurrenceLogger::~OccurrenceLogger ()
{
  flush ();
  closelog ();
  delete mLineBuffer;
  while (mNArgs > 0) {
    delete mArg [--mNArgs];
  }
  if (mTotalCount > 0 || !(mFlags & cACTIVE)) {
    // do not delete
  } else {
    UtString reason;
    UtIO::cout () << "Deleting " << mFilename << " (no entries)" << UtIO::endl;
    OSUnlink (mFilename, &reason);
  }
}

//! Open the log file.
void OccurrenceLogger::openlog (const char *root, const char *ident, const char *dirname)
{
  UtString reason;
  if (!makeFilename (root, ident, dirname)) {
    mFlags &= ~cACTIVE;
  } else if ((mFD = OSSysOpen (mFilename, O_CREAT|O_WRONLY|O_TRUNC, 0644, &reason)) < 0) {
    UtString errBuf;
    fail ("unable to create log file: %s", OSGetLastErrmsg(&errBuf));
  } else {
    mFlags |= cACTIVE;
    stampLog ();
    if (mFlags & cVERBOSE) {
      UtIO::cout () << "Opened " << mIdent << " occurrence log at " << mFilename
        << UtIO::endl;
    }
  }
}

//! Close the logfile.
void OccurrenceLogger::closelog ()
{
  if (mFD < 0) {
    // nothing open
  } else if (::close (mFD) < 0) {
    mFD = -1;                           // set before calling fail
    UtString errBuf;
    fail ("unable to close log file: %s", OSGetLastErrmsg(&errBuf));
  }
  mFlags &= ~cACTIVE;
  mFD = -1;
}

//! Form the filename from the root, ident and optional directory.
bool OccurrenceLogger::makeFilename (const char *root, const char *ident, const char *dirname)
{
  if (dirname == NULL && root == NULL) {
    snprintf (mFilename, sizeof (mFilename), "%s", ident);
  } else if (dirname == NULL) {
    snprintf (mFilename, sizeof (mFilename), "%s.%s.log", root, ident);
  } else {
    snprintf (mFilename, sizeof (mFilename), "%s/%s.%s.log", dirname, root, ident);
  }
  return true;
}

//! Put a timestamp on the log.
void OccurrenceLogger::stampLog ()
{
  int n;
  time_t current_time;
  struct tm *tm;
  if (time (&current_time) < 0 
    || (tm = localtime (&current_time)) == NULL
    || (n = strftime (mLineBuffer + mLength, mSize - mLength, "%c\n", tm)) == 0) {
    UtString errBuf;
    fail ("log file timestamp failed: %s", OSGetLastErrmsg(&errBuf));
  } else {
    mLength += n;
  }
  flush ();
}

void OccurrenceLogger::writebuffer (const int fd)
{
  UInt32 n = 0;
  while (n < mLength) {
    int ret = ::write (fd, mLineBuffer + n, mLength - n);
    if (ret < 0) {
      // write failed
      UtString errBuf;
      fail ("error writing to logfile: %s", OSGetLastErrmsg(&errBuf));
      break;
    } else {
      n += ret;
    }
  }
}

//! Flush the buffer to the logfile.
void OccurrenceLogger::flush ()
{
  if (mFlags & cTEE) {
    writebuffer (1);                    // tee to standard output
  }
  if (mFlags & cACTIVE) {
    writebuffer (mFD);                  // to the log file
  }
  mLength = 0;
}

//! Grow the buffer
bool OccurrenceLogger::grow (const UInt32 delta)
{
  // flush the old buffer and free the storage
  flush ();
  delete mLineBuffer;
  mLength = 0;
  // compute a new size... add the requested extra amount and multiply
  mSize = (UInt32) ((mSize + delta + 1) * 1.4);
  if ((mLineBuffer = new char [mSize]) == NULL) {
    fail ("unable to grow buffer");
    mSize = 0;
    return false;
  }
  return true;
}

//! Write an error message and close the logfile.
void OccurrenceLogger::fail (const char *fmt, ...)
{
  char buffer [256];
  va_list ap;
  va_start (ap, fmt);
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
  va_end (ap);
  mMsgContext.REOccurrenceLogError (mIdent, buffer);
  if (mFD >= 0) {
    closelog ();
  }
}

#if 0
//! Write bytes to the logfile.
void OccurrenceLogger::write (const char *buffer, const unsigned int len)
{
  unsigned int n = 0;
  while (n < len) {
    if (mLength >= mSize) {
      flush ();
    }
    ASSERT (mLength < sizeof (mBuffer));
    int space_in_buffer = sizeof (mBuffer) - mLength;
    int bytes_left_to_copy = len - n;
    int bytes_to_copy = bytes_left_to_copy <= space_in_buffer ? bytes_left_to_copy : space_in_buffer;
    ASSERT (mLength + bytes_to_copy <= sizeof (mBuffer));
    ASSERT (n + bytes_to_copy <= len);
    memcpy (mBuffer + mLength, buffer + n, bytes_to_copy);
    n += bytes_to_copy;
    mLength += bytes_to_copy;
  }
}
#endif


//! Write an entry to the log.
void OccurrenceLogger::log (const char *file, const unsigned int line, const UInt32 key)
{
  ASSERT (mLength == 0 && mSize > 0);
  if (!(mFlags & cACTIVE)) {
    return;
  }
  if (file != NULL) {
    mLength += snprintf (mLineBuffer + mLength, mSize - mLength, "%s: %d: ", file, line);
  }
  mLength += snprintf (mLineBuffer + mLength, mSize - mLength, "%s", keyToName (key));
  if (mLength >= mSize) {
    flush ();
  }
  for (UInt32 i = 0; i < mNArgs; i++) {
    SInt32 retval = mArg [i]->write (mLineBuffer + mLength, mSize - mLength - 1);
    // the -1 leaves space for the trailing \n
    while (retval < 0) {
      grow (-retval);
      retval = mArg [i]->write (mLineBuffer + mLength, mSize - mLength - 1);
    }
    delete mArg [i];
    mLength += retval;
  }
  mLineBuffer [mLength++] = '\n';
  flush ();
  mNArgs = 0;
}

//! Write an entry to the log.
void OccurrenceLogger::log (const SourceLocator &loc, const UInt32 key)
{
  if (!(mFlags & cACTIVE)) {
    return;
  }
  log (loc.getFile (), loc.getLine (), key);
}

//! Output a summary of the number of entries per tag
void OccurrenceLogger::summarise (UtOStream &s) const
{
  if ((mFlags & cSUMMARY) && mTotalCount > 0) {
    for (UInt32 n = 0; n < cMAXKEYS; n++) {
      if (mCount [n] == 0) {
        // output nothing
      } else if (mCount [n] == 1) {
        s << keyToName (n) << ": 1 occurrence" << UtIO::endl;
      } else {
        s << keyToName (n) << ": " << mCount [n] << " occurrences" << UtIO::endl;
      }
    }
  }
}

//! Output a summary of the number of entries per tag
void OccurrenceLogger::summarise ()
{
  flush ();
  if ((mFlags & cSUMMARY) && mTotalCount > 0) {
    for (UInt32 n = 0; n < cMAXKEYS; n++) {
      char buffer [256];
      int len;
      if (mCount [n] == 0) {
        continue;
      } else if (mCount [n] == 1) {
        len = sprintf (buffer, "%s: 1 occurrence\n", keyToName (n));
      } else {
        len = sprintf (buffer, "%s: %d occurrences\n", keyToName (n), mCount [n]);
      }
      if (::write (mFD, buffer, len) < 0) {
        UtString errBuf;
        fail ("error writing to logfile: %s", OSGetLastErrmsg(&errBuf));
        return;
      }
    }
  }
}

//! Specialisation for UInt32 args
template <>
/*virtual*/ SInt32 OccurrenceLogger::ArgT<UInt32>::write (char *s, const UInt32 size) const
{
  UInt32 n = snprintf (s, size, ": %d", mValue);
  if (n >= size) {
    // did not fit intop the buffer
    return -(n + 1);
  } else {
    return n;
  }
}

template<>
/*virtual*/ SInt32 OccurrenceLogger::ArgT<const OccurrenceLogger::Visible *>::write  (char *s, const UInt32 size) const
{
  UtString b;
  mValue->compose (&b, 0);
  UInt32 n = snprintf (s, size, ": %s", b.c_str ());
  if (n >= size) {
    // did not fit into the buffer
    return -(n + 1);
  } else {
    return n;
  }
}
