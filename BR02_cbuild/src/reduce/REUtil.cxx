// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/REUtil.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUTF.h"

#if !pfICC && !defined(__COVERITY__)
// force instantiation
template class NUNetRefMultiMap<NUUseDefSet*>;
#endif

template <>
NUUseDefSet * NUNetRefMultiMap<NUUseDefSet*>::nullT() const {
  return NULL;
}

template <>
void NUNetRefMultiMap<NUUseDefSet*>::helperTPrint(NUUseDefSet * const & v, int indent) const {
  for (NUUseDefSet::iterator iter = v->begin(); iter != v->end(); ++iter) {
    (*iter)->print(false,indent);
  }
}

void ReduceUtility::update()
{
  clear();

  getModuleUses();
  getModuleDefs();
  
  mValid = true;
}

void ReduceUtility::getModuleUses()
{
  // 1. sub-modules
  for (NUModuleInstanceMultiLoop loop = mModule->loopInstances();
       !loop.atEnd();
       ++loop) {
    NUModuleInstance * instance = (*loop);
    instance->getUses(&mModuleUses);

    // 1a. Make sure all sub-module inputs are marked. They
    //     won't appear in the UD if disconnected, but we need to
    //     preserve them (they're still passed around).
    NUPortConnectionInputList iport_list;
    instance->getInputPortConnections(iport_list);
    for (NUPortConnectionInputListIter iter = iport_list.begin();
       iter != iport_list.end();
       iter++) {
      NUPortConnectionInput * input = (*iter);
      
      NUExpr * actual = input->getActual();
      if (actual) {
        actual->getUses(&mModuleUses);
      }
    }
  }

  // 2. cont. assigns
  for (NUModule::ContAssignLoop loop = mModule->loopContAssigns();
       !loop.atEnd();
       ++loop) {
    (*loop)->getUses(&mModuleUses);
  }

  // 3. this module itself (potentially overkill)
  // mModule->getUses(&mModuleUses);

  // 4. always blocks
  for (NUModule::AlwaysBlockLoop loop = mModule->loopAlwaysBlocks();
       !loop.atEnd();
       ++loop) {
    (*loop)->getUses(&mModuleUses);
  }

  // 5. initial blocks
  for (NUModule::InitialBlockLoop loop = mModule->loopInitialBlocks();
       !loop.atEnd();
       ++loop) {
    (*loop)->getUses(&mModuleUses);
  }

  // 6. enabled drivers (necessary for late calls)
  for (NUModule::ContEnabledDriverLoop loop =mModule->loopContEnabledDrivers();
       ! loop.atEnd();
       ++loop) {
    (*loop)->getUses(&mModuleUses);
  }

  // 7. TriRegInit (necessary for late calls)
  for (NUModule::TriRegInitLoop loop = mModule->loopTriRegInit() ;
       ! loop.atEnd();
       ++loop) {
    (*loop)->getUses(&mModuleUses);
  }

  // 8. Tasks (necessary for late calls)
  for (NUTaskLoop loop = mModule->loopTasks() ;
       ! loop.atEnd();
       ++loop) {
    (*loop)->getUses(&mModuleUses);
  }
}

void ReduceUtility::getModuleDefs()
{
  NUNetRefSet worker(mNetRefFactory);
  // 1. sub-modules
  for (NUModuleInstanceMultiLoop loop = mModule->loopInstances();
       !loop.atEnd();
       ++loop) {
    (*loop)->getDefs(&worker);
    addDefs((*loop),worker);
    mModuleDefs.insert(worker.begin(),worker.end());
    worker.clear();
  }

  // 2. cont. assigns
  for (NUModule::ContAssignLoop loop = mModule->loopContAssigns();
       !loop.atEnd();
       ++loop) {
    (*loop)->getDefs(&worker);
    addDefs((*loop),worker);
    mModuleDefs.insert(worker.begin(),worker.end());
    worker.clear();
  }

  // 3. this module itself (potentially overkill)
  // mModule->getDefs(&mModuleDefs);

  // 4. always blocks
  for (NUModule::AlwaysBlockLoop loop = mModule->loopAlwaysBlocks();
       !loop.atEnd();
       ++loop) {
    (*loop)->getDefs(&worker);
    addDefs((*loop),worker);
    mModuleDefs.insert(worker.begin(),worker.end());
    worker.clear();
  }

  // 5. initial blocks
  for (NUModule::InitialBlockLoop loop = mModule->loopInitialBlocks();
       !loop.atEnd();
       ++loop) {
    (*loop)->getDefs(&worker);
    addDefs((*loop),worker);
    mInitialBlockDefs.insert(worker.begin(),worker.end());
    worker.clear();
  }
  mModuleDefs.insert(mInitialBlockDefs.begin(),
		     mInitialBlockDefs.end());

  // 6. enabled drivers (necessary for late calls)
  for (NUModule::ContEnabledDriverLoop loop =mModule->loopContEnabledDrivers();
       ! loop.atEnd();
       ++loop) {
    (*loop)->getDefs(&worker);
    addDefs((*loop),worker);
    mModuleDefs.insert(worker.begin(),worker.end());
    worker.clear();
  }

  // 7. TriRegInit (necessary for late calls)
  for (NUModule::TriRegInitLoop loop = mModule->loopTriRegInit() ;
       ! loop.atEnd();
       ++loop) {
    (*loop)->getDefs(&worker);
    addDefs((*loop),worker);
    mModuleDefs.insert(worker.begin(),worker.end());
    worker.clear();
  }

  // 8. Tasks (necessary for late calls)
  for (NUTaskLoop loop = mModule->loopTasks() ;
       ! loop.atEnd();
       ++loop) {
    (*loop)->getDefs(&worker);
    addDefs((*loop),worker);
    mTaskDefs.insert(worker.begin(),worker.end());
    worker.clear();
  }
  mModuleDefs.insert(mTaskDefs.begin(), mTaskDefs.end());
}


void ReduceUtility::addDefs(NUUseDefNode * node, NUNetRefSet & net_ref_set)
{
  for (NUNetRefSet::iterator iter = net_ref_set.begin();
       iter != net_ref_set.end();
       ++iter) {
    NUNetRefHdl net_ref = (*iter);
    NUNetRefToNodeSet::CondLoop loop = mNetRefDefs.loop(net_ref,&NUNetRef::operator==);
    NUUseDefSet * use_def_set = NULL;
    if (not loop.atEnd()) {
      use_def_set = (*loop).second;
    } else {
      use_def_set = new NUUseDefSet();
      mNetRefDefs.insert(net_ref,use_def_set);
    }
    use_def_set->insert(node);
  }
}


void ReduceUtility::clear()
{
  mModuleUses.clear();
  mModuleDefs.clear();
  mInitialBlockDefs.clear();
  mTaskDefs.clear();

  for (NUNetRefToNodeSet::MapLoop loop = mNetRefDefs.loop();
       not loop.atEnd();
       ++loop) {
    NUUseDefSet * use_def_set = (*loop).second;
    delete use_def_set;
  }
  mNetRefDefs.clear();
  
  mValid = false;
}

bool ReduceUtility::inInitialBlock(const NUNet * net) const {
  const NUNetRefHdl net_ref = mNetRefFactory->createNetRef(const_cast<NUNet*>(net));
  return inInitialBlock(net_ref);
}

bool ReduceUtility::inInitialBlock(const NUNetRefHdl & net_ref) const {
  NU_ASSERT(mValid,net_ref);
  return (mInitialBlockDefs.find(net_ref,&NUNetRef::overlapsSameNet) != mInitialBlockDefs.end());
}


bool ReduceUtility::inTask(const NUNet * net) const {
  const NUNetRefHdl net_ref = mNetRefFactory->createNetRef(const_cast<NUNet*>(net));
  return inTask(net_ref);
}

bool ReduceUtility::inTask(const NUNetRefHdl & net_ref) const {
  NU_ASSERT(mValid,net_ref);
  return (mTaskDefs.find(net_ref,&NUNetRef::overlapsSameNet) != mTaskDefs.end());
}


bool ReduceUtility::hasModuleUse(const NUNet * net) const {
  const NUNetRefHdl net_ref = mNetRefFactory->createNetRef(const_cast<NUNet*>(net));
  return hasModuleUse(net_ref);
}

bool ReduceUtility::hasModuleUse(const NUNetRefHdl & net_ref) const {
  NU_ASSERT(mValid,net_ref);
  return (mModuleUses.find(net_ref,&NUNetRef::overlapsSameNet) != mModuleUses.end());
}


bool ReduceUtility::hasModuleDef(const NUNet * net) const {
  const NUNetRefHdl net_ref = mNetRefFactory->createNetRef(const_cast<NUNet*>(net));
  return hasModuleDef(net_ref);
}

bool ReduceUtility::hasModuleDef(const NUNetRefHdl & net_ref) const {
  NU_ASSERT(mValid,net_ref);
  return (mModuleDefs.find(net_ref,&NUNetRef::overlapsSameNet) != mModuleDefs.end());
}


SInt32 ReduceUtility::getDefCount(const NUNet * net) const {
  const NUNetRefHdl net_ref = mNetRefFactory->createNetRef(const_cast<NUNet*>(net));
  return getDefCount(net_ref);
}

SInt32 ReduceUtility::getDefCount(const NUNetRefHdl &  net_ref) const {
  NU_ASSERT(mValid,net_ref);
  SInt32 size = 0;
  for (NUNetRefToNodeSet::CondLoop loop = mNetRefDefs.loop(net_ref,&NUNetRef::overlapsSameNet);
       not loop.atEnd();
       ++loop) {
    NUUseDefSet * use_def_set = (*loop).second;
    size += use_def_set->size();
  }
  return size;
}


void ReduceUtility::getDefs(const NUNet * net, NUUseDefSet & use_def_set) const {
  const NUNetRefHdl net_ref = mNetRefFactory->createNetRef(const_cast<NUNet*>(net));
  getDefs(net_ref,use_def_set);
}

void ReduceUtility::getDefs(const NUNetRefHdl & net_ref, NUUseDefSet & user_use_def_set) const
{
  for (NUNetRefToNodeSet::CondLoop loop = mNetRefDefs.loop(net_ref,&NUNetRef::overlapsSameNet);
       not loop.atEnd();
       ++loop) {
    NUUseDefSet * use_def_set = (*loop).second;
    user_use_def_set.insert(use_def_set->begin(),use_def_set->end());
  }
}

bool LessThanUseDef::operator()(const NUUseDefNode *a, const NUUseDefNode *b) const
{
  NUNetSet a_net_defs;
  NUNetSet b_net_defs;

  NU_ASSERT (a != NULL, b);
  NU_ASSERT (a != (NUUseDefNode *) 0xdeadbeef, b);
  NU_ASSERT (b != NULL, a);
  NU_ASSERT (b != (NUUseDefNode *) 0xdeadbeef, a);

  if (a==b)
    return false;

  if (a->useBlockingMethods())
  {
    const NUUseDefStmtNode* stmt = dynamic_cast<const NUUseDefStmtNode*>(a);
    stmt->getBlockingDefs(&a_net_defs);
    stmt->getNonBlockingDefs(&a_net_defs);
  }
  else
  {
    a->getDefs(&a_net_defs);
  }

  if (b->useBlockingMethods())
  {
    const NUUseDefStmtNode* stmt = dynamic_cast<const NUUseDefStmtNode*>(b);
    stmt->getBlockingDefs(&b_net_defs);
    stmt->getNonBlockingDefs(&b_net_defs);
  }
  else
  {
    b->getDefs(&b_net_defs);
  }

  // we compare by net first because netrefs are ordered by ptr order.
  // we can order nets by name.
  SInt32 cmp = (a_net_defs.size() - b_net_defs.size());
  if (cmp==0) {
    // sort nets so we don't rely on ptr order.
    NUNetOrderedSet a_ordered_defs;
    NUNetOrderedSet b_ordered_defs;
    a_ordered_defs.insert(a_net_defs.begin(),a_net_defs.end());
    b_ordered_defs.insert(b_net_defs.begin(),b_net_defs.end());

    for (NUNetOrderedSet::const_iterator iter_a = a_ordered_defs.begin(), iter_b = b_ordered_defs.begin();
	 (cmp==0) and iter_a != a_ordered_defs.end() and iter_b != b_ordered_defs.end();
	 ++iter_a, ++iter_b) {
      const NUNet * a_def = (*iter_a);
      const NUNet * b_def = (*iter_b);

      cmp = NUNet::compare(a_def,b_def);
    }
  }

  if (cmp==0) {
    // if nets compare, compare using netref.
    NUNetRefSet a_defs(mNetRefFactory);
    NUNetRefSet b_defs(mNetRefFactory);

    if (a->useBlockingMethods())
    {
      const NUUseDefStmtNode* stmt = dynamic_cast<const NUUseDefStmtNode*>(a);
      stmt->getBlockingDefs(&a_defs);
      stmt->getNonBlockingDefs(&a_defs);
    }
    else
    {
      a->getDefs(&a_defs);
    }

    if (b->useBlockingMethods())
    {
      const NUUseDefStmtNode* stmt = dynamic_cast<const NUUseDefStmtNode*>(b);
      stmt->getBlockingDefs(&b_defs);
      stmt->getNonBlockingDefs(&b_defs);
    }
    else
    {
      b->getDefs(&b_defs);
    }

    cmp = (a_defs.size() - b_defs.size());
    if (cmp == 0) {
      for (NUNetRefSet::const_iterator iter_a = a_defs.begin(), iter_b = b_defs.begin();
	   (cmp == 0) and iter_a != a_defs.end() and iter_b != b_defs.end();
	   ++iter_a, ++iter_b) {
        const NUNetRefHdl a_def = (*iter_a);
        const NUNetRefHdl b_def = (*iter_b);

        if (a_def != b_def)
        {
          if (a_def < b_def)
            cmp = -1;
          else
            cmp = 1;
        }
      }
    }
  }

  if (cmp==0) {
    // if net refs compare, check for assignments to bit-selects of the same
    // lvalue.  these can have the exact same defs, but we want to order them
    // based on the selected bit as well.
    const NUAssign* assign_a = dynamic_cast<const NUAssign*>(a);
    const NUAssign* assign_b = dynamic_cast<const NUAssign*>(b);
    if (assign_a && assign_b)
    {
      const NULvalue* lv_a = assign_a->getLvalue();
      const NULvalue* lv_b = assign_b->getLvalue();
      const NUVarselLvalue* varsel_a = dynamic_cast<const NUVarselLvalue*>(lv_a);
      const NUVarselLvalue* varsel_b = dynamic_cast<const NUVarselLvalue*>(lv_b);
      if (varsel_a && varsel_b &&
          (*(varsel_a->getLvalue()) == *(varsel_b->getLvalue())) &&
          varsel_a->isConstIndex() && varsel_b->isConstIndex())
      {
        const ConstantRange* range_a = varsel_a->getRange();
        const ConstantRange* range_b = varsel_b->getRange();
        cmp = (varsel_a->getConstIndex() + range_a->getLsb()) -
              (varsel_b->getConstIndex() + range_b->getLsb());
      }
    }    
  }

  return (cmp<0);
}

bool LessThanAssign::operator () (const NUUseDefNode *a, const NUUseDefNode *b) const
{

  if (a == b) {
    // the same guy...
    return false;
  }
  // first check that both are assignment statements
  const NUAssign *a_assign = dynamic_cast <const NUAssign *> (a);
  const NUAssign *b_assign = dynamic_cast <const NUAssign *> (b);
  if (a_assign == NULL && b_assign == NULL) {
    // neither is an assignment so just pass to the superclass for comparison
    return LessThanUseDef::operator () (a, b);
  } else if (a_assign == NULL) {
    // a is not an assign, but b is an assign, define a to be not less than b
    return false;
  } else if (b_assign == NULL) {
    // a is an assign, but b is not an assign, define a to be less than b
    return true;
  }
  // both NUUseDefNode instances are assignments, check the types of the left side
  if (!a_assign->getLvalue ()->isWholeIdentifier () 
    && !b_assign->getLvalue ()->isWholeIdentifier ()) {
    // neither assigns to a whole identifiers so use the superclass 
    return LessThanUseDef::operator () (a, b);
  } else if (!a_assign->getLvalue ()->isWholeIdentifier ()) {
    // a is not a whole identifier assignment, b is a whole identifier
    // assignment, define a to be not less than b
    return false;
  } else if (!b_assign->getLvalue ()->isWholeIdentifier ()) {
    // a is a whole identifier assignment, b is a not whole identifier
    // assignment, define a to be less than b
    return true;
  }
  // both statements are assignments to whole identifiers, so first compare
  // using expression comparison
  SInt32 cmp; 
  if ((cmp = a_assign->getRvalue ()->compare (*b_assign->getRvalue (), false, false, false)) != 0) {
    // Right sides compared as different
  } else {
    // Right sides came up equal so just do the left-side nets. The left sides
    // are both whole identifiers, so getWholeIdentifier does the right thing.
    cmp = NUNet::compare (
      a_assign->getLvalue ()->getWholeIdentifier (),
      b_assign->getLvalue ()->getWholeIdentifier ());
  }
  return (cmp < 0);
}
