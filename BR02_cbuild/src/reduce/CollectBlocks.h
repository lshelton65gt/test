// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _COLLECT_BLOCKS_H_
#define _COLLECT_BLOCKS_H_

#include "nucleus/Nucleus.h"

/*!
  \file
  Declaration of block collection package.
 */

//! Utility class to walk a Nucleus sub-structure and return the set of embedded blocks.
/*!
  The NUBlock lists attached to some structures (modules, other
  blocks) are not processed. These lists do not necessarily contain
  all embedded blocks.

  For example, a block may contain another block. If the parent block
  does not define a scope, the child block will have the scope of the
  parent as its scope. We want all embedded blocks -- not simply the
  ones reachable as subscopes.
 */
class CollectBlocks {
public:
  //! Constructor.
  /*!
    \param blocks Discovered blocks will be added to this set.
  */
  CollectBlocks(NUBlockSet & blocks) :
    mBlocks(blocks) {}

  //! Destructor
  ~CollectBlocks() {}

  //! Start the traversal with this statement.
  void stmt(NUStmt * one);
private:
  void blockStmt(NUBlock * one);
  void ifStmt(NUIf * one);
  void forStmt(NUFor * one);
  void caseStmt(NUCase * one);

  NUBlockSet & mBlocks;
};

#endif // _COLLECT_BLOCKS_H_

