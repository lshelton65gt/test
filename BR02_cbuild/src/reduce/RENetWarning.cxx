/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*
 * elaborated analysis to accurately report undriven net warnings
 */

#include "reduce/RENetWarning.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetElabRef.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/Nucleus.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "util/UtHashMap.h"
#include "util/CbuildMsgContext.h"
#include "util/Zstream.h"
#include "iodb/IODBNucleus.h"
#include "reduce/ReachableAliases.h"
#include "reduce/REAlias.h"
#include "reduce/Fold.h"
#include "reduce/RewriteUtil.h"
#include "symtab/STAliasedLeafNode.h"
#include "util/ArgProc.h"
#include "compiler_driver/CarbonContext.h" // for CRYPT
#include "compiler_driver/CarbonDBWrite.h"

#include <algorithm>

static const char* scUndrivenWave = NULL;
static const char* scDeadWave = NULL;


static bool sParseWaveBit(const char* bit, Carbon4StateVal* val) {
  if ((bit[0] == '\0') || (bit[1] != '\0')) {
    return false;               // want length 1
  }
  switch (*bit) {
  case '0':
    *val = eValue0;
    return true;
  case '1':
    *val = eValue1;
    return true;
  case 'x':
  case 'X':
    *val = eValueX;
    return true;
  case 'z':
  case 'Z':
    *val = eValueZ;
    return true;
  default:
    break;
  }
  return false;
} // static bool sParseWaveBit

RENetWarning::RENetWarning(NUNetRefFactory *netref_factory,
                           STSymbolTable* symtab,
                           IODBNucleus* iodb,
                           MsgContext *msg_context,
                           ReachableAliases* reachableAliases,
                           REAlias* alias,
                           NUDesign* design,
                           ArgProc* args,
                           FLNodeElabFactory* flNodeElabFactory)
  : mNetRefFactory(netref_factory),
    mMsgContext(msg_context),
    mIODB(iodb),
    mSymbolTable(symtab),
    mReachableAliases(reachableAliases),
    mAlias(alias),
    mDesign(design),
    mFLNodeElabFactory(flNodeElabFactory),
    mArgs(args)
{
  mReadBits = new NUNetElabRefSet2(netref_factory);
  mWrittenBits = new NUNetElabRefSet2(netref_factory);
  mWeakBits = new NUNetElabRefSet2(netref_factory);
  mFullTimeBits = new NUNetElabRefSet2(netref_factory);
  mEnabledBits = new NUNetElabRefSet2(netref_factory);

  Carbon4StateVal deadBitWave = eValueZ;
  Carbon4StateVal undrivenBitWave = eValueX;

  const char* deadBit, *undrivenBit;
  args->getStrValue(scUndrivenWave, &undrivenBit);
  if (!sParseWaveBit(undrivenBit, &undrivenBitWave)) {
    mMsgContext->InvalidWaveBit(undrivenBit, scUndrivenWave);
  }
  args->getStrValue(scDeadWave, &deadBit);
  if (!sParseWaveBit(deadBit, &deadBitWave)) {
    mMsgContext->InvalidWaveBit(deadBit, scDeadWave);
  }

  mIODB->putDeadBitWave(deadBitWave);
  mIODB->putUndrivenBitWave(undrivenBitWave);
  mProtectedMutableCache = new NetBoolMap;
  mProtectedObservableCache = new NetBoolMap;
  mPrimaryOutputCache = new NetBoolMap;
} // RENetWarning::RENetWarning

RENetWarning::~RENetWarning()
{
  delete mReadBits;
  delete mWrittenBits;
  delete mWeakBits;
  delete mEnabledBits;
  delete mFullTimeBits;
  delete mProtectedMutableCache;
  delete mProtectedObservableCache;
  delete mPrimaryOutputCache;
}

void RENetWarning::printWarnings(UInt32 warningFlags) {
  if ((warningFlags & eWarnUndriven) != 0) {
    printUndrivenWarnings();
    printUndrivenLocalOuts();      // these are missed in elab undriven walk,
                                   // e.g. test/directives/cheetah.v
  }
  if ((warningFlags & eWarnDead) != 0) {
    computeDeadBits();
  }
}

//! Structure to assist factoring out prep code that happens in both
//! design-walks in RENetWarning (walkDesign() and walkDesignZ())
struct RENetWarningIter {
  RENetWarningIter(REAlias* alias, NUDesign* design, STSymbolTable* symtab,
                   RENetWarning* netWarn, NUNetRefFactory* netRefFactory,
                   FLIterFlags::NestingT nesting,
                   FLIterFlags::IterationT iterFlags)
    : mAlias(alias),
      mIter(design, symtab,
            FLIterFlags::eClockMasters,
            FLIterFlags::eAll,  // Visit everything
            FLIterFlags::eNone, // Stop at nothing
            nesting,
            iterFlags),
      mRENetWarning(netWarn),
      mNetRefFactory(netRefFactory)
  {
    if (!atEnd()) {
      setup();
      advanceToNextValid();
    }
  }

  void advanceToNextValid() {
    while (!mValid) {
      ++mIter;
      if (atEnd()) {
        return;
      }
      setup();
    }
  }

  void operator++() {
    mValid = false;
    advanceToNextValid();
  }
  bool atEnd() const {return mIter.atEnd();}
  
  void setup() {
    // Mark the driving bits as such
    mDrive = *mIter;
    mDriveNet = mDrive->getDefNet();
    mFanout = mIter.getCurParent();

    // should be resolved by now
    FLN_ELAB_ASSERT((mDrive->getType() != eFLBoundHierRef), mDrive);

    if (mDriveNet != NULL) {
      // Find the master net for this net and apply all drivers to
      // that. We will fix up the subordinate nets later.
      mMasterNet = mDriveNet;
      if (mAlias != NULL) {
        mMasterNet = mAlias->getMaster(mDriveNet);
      }
      NUNet* net = mMasterNet->getNet();

      // Figure out if this is a real driver. It isn't if we have an
      // undriven bound node or a port connection that is represented
      // as an alias.
      // By setting the checkForConstant to false, a input port driven
      // actually having an actual expression will be considered driven,
      // even if the actual itself is undriven or an out of range bit
      // or part select. The important thing is that there is an actual
      // expression. See bug 12540 for more details.
      mRENetWarning->isValidDriver(mDrive, mDriveNet, &mDriven, &mDriverFound, false /* checkForConstant */);

      // Create a net ref for the master
      mMasterRef = mDrive->getDefNetRef(mNetRefFactory);
      if (mMasterNet != mDriveNet) {
        mMasterRef = mNetRefFactory->createNetRefImage(net, mMasterRef);
      }

      // NOTE -- this if-statement should not be necessary.  It is only
      // necessary because the flow is wrong when the user deposits
      // to a submodule input, and there is no driver above.  The scheduler
      // checks IODB's set, so RENetWarning should as well.  At some point
      // the flow should be made correct and the scheduler and RENetWarning
      // should be changed to trust it.  This is logged in bug3061.  Another
      // related issue is that port coercion fails for unconnected inputs,
      // and that is logged in bug3062.
      if (!mDriven && mRENetWarning->isProtectedMutable(mMasterNet)) {
        mDriven = true;
      }
      mValid = true;
    } // if
    else {
      mValid = false;
    }
  } // void setup

  void doNotExpand() {mIter.doNotExpand();}

  REAlias* mAlias;
  FLDesignElabIter mIter;
  RENetWarning* mRENetWarning;
  NUNetRefFactory* mNetRefFactory;

  bool mDriven;
  bool mDriverFound;
  bool mValid;
  FLNodeElab* mDrive;
  NUNetElab* mDriveNet;
  NUNetElab* mMasterNet;
  NUNetRefHdl mMasterRef;
  FLNodeElab* mFanout;
};

void RENetWarning::walkDesign()
{
  // Do an elaborated walk, noting all readers and writers to all the
  // nets in a bit-accurate fashion.  We will then iterate over the
  // nets and print warnings for any bits that are read but not
  // written.

  for (RENetWarningIter iter(mAlias, mDesign, mSymbolTable, this,
                             mNetRefFactory, FLIterFlags::eBranchAtAll,
                             FLIterFlags::eIterFaninPairOnce);
       not iter.atEnd();
       ++iter)
  {
    if (iter.mDriven) {
      NUUseDefNode* ud = iter.mDrive->getUseDefNode();
      bool weak = false;
      if ((ud != NULL) && (ud->getStrength() < eStrDrive))
        weak = true;

/*
*      If we wanted to report "weakly driven" warnings on
*      primary ports that have no other drivers other than
*      pulls, then we can uncomment this block.  I (Josh)
*      don't think those warnings make sense because the 
*      outside world may be driving strongly.
*
*        else if ((drive->getType() == eFLBoundPort) &&
*                 (masterNet->queryAliases(&NUNet::isPrimaryBid)))
*          weak = true;
*/
      if (weak)
        mWeakBits->insert(iter.mMasterNet, iter.mMasterRef);
      else
        mWrittenBits->insert(iter.mMasterNet, iter.mMasterRef);
    } // if

    NUNet* net = iter.mMasterNet->getNet();

    // Account for pullups, pulldowns, tri1, tri0, but only
    // if we haven't already written all the bits.
    if ((!iter.mDriven || !iter.mMasterRef->all()) &&
        mReachableAliases->queryAliases(iter.mMasterNet, &NUNet::isPulled))
    {
      mWeakBits->insert(iter.mMasterNet, mNetRefFactory->createNetRef(net));
    }

    // If this flow-node has no fanout, then it is a primary output
    // or observable net, and we are going to need all the bits, not
    // just the ones being driven
    NUNetRefHdl useRef = iter.mMasterRef;
    if (iter.mFanout == NULL) {
      useRef = mNetRefFactory->createNetRef(net); // use all the bits
    }

    // If there is fanout or this net is marked observable then mark
    // the bits as read.  Actually, this does assert in
    // langcov/for12.v on net top.a, and so make it an if statement
    if ((iter.mFanout != NULL) || // has live fanout in design
        isPrimaryOutput(iter.mMasterNet) ||
        isProtectedObservable(iter.mMasterNet))
    {
      // In UnelabFlow::fixPartiallyDriven, all the undriven bits of
      // a vector are merged into a single FLBoundNode, including
      // those bits that are never read.  We want to calculate the
      // bits that are actually read.
      if (iter.mDriverFound && !iter.mDriven && (iter.mFanout != NULL) &&
          ((mAlias == NULL) ||
           !mAlias->isAlias(iter.mFanout->getDefNet(), iter.mMasterNet)))
      {
        NUNetRefHdl usedBits = iter.mFanout->findUsedBits(mNetRefFactory,
                                                          iter.mDriveNet);
        if (iter.mDriveNet != iter.mMasterNet) {
          usedBits = mNetRefFactory->createNetRefImage(net, usedBits);
        }
        useRef = mNetRefFactory->intersect(iter.mMasterRef, usedBits);
      }

      mReadBits->insert(iter.mMasterNet, useRef);
    }
  } // for
} // void RENetWarning::walkDesign

//! Define a FLNodeElab set where the comparator will show
//! two different FLNodeElabs that print the same (same net,
//! same usedef location) as being the same, and will only admit
//! one.  This is used to reduce redundant message printing.
bool RENetWarning::FLNodeElabPrintCmp::operator()(FLNodeElab* f1,
                                                  FLNodeElab* f2) const
{
  int cmp = 0;
  NUUseDefNode* u1 = f1->getUseDefNode();
  NUUseDefNode* u2 = f2->getUseDefNode();
  if (u1 != u2) {
    if (u1 == NULL) {
      cmp = -1;
    }
    else if (u2 == NULL) {
      cmp = 1;
    }
    else {
      cmp = SourceLocator::compare(u1->getLoc(), u2->getLoc());
    }
  }
  if (cmp == 0) {
    cmp = NUNetElab::compare(f1->getDefNet(), f2->getDefNet());
    if (cmp == 0) {
      NUNetRefHdl r1 = f1->getFLNode()->getDefNetRef();
      NUNetRefHdl r2 = f2->getFLNode()->getDefNetRef();
      cmp = NUNetRef::compare(*r1, *r2, false);
    }
  }
  return cmp < 0;
}

//! Determine which bits that are killed by the given flow, return true if
//! any were killed.  localKill is filled in with the netref for the local net
//! driven by the given flow, not the master net.
bool RENetWarning::findKilledBits(FLNodeElab* flow, NUNetRefHdl* localKill,
                                  NUNetRefHdl* globalKill)
{
  NUUseDefNode* ud = flow->getUseDefNode();

  if ((ud == NULL) || ud->isInitial()) {
    // Ignore port contributions and initial blocks for
    // z conflict warnings
    return false;
  }

  // We only look at killed nets.  A net that is not killed may
  // arise from Z-drivers in always blocks, which LFTristate will
  // eliminate (marking the net Z, but leaving no other hints).
  // The resultant graph is indistinguishable from a latch structure:
  //   BEFORE: always @(a or b) if (a) out = b; else out = 1'bz;
  //   AFTER:  always @(a or b) if (a) out = b;
  // Thus we cannot infer anything about Z conflicts from the
  // flow-node for this always-block.  It may be Z, it may be
  // strongly driven, but there is no way to print an Alert
  // without excessive pessimism.  Excessive pessimism printing
  // Alerts slows deployment without adding much value.  
  NUNetRefSet killSet(mNetRefFactory);

  // Walk into the block from the always block, which is where we
  // will find the appropriate kill information
  NUStructuredProc* structProc = dynamic_cast<NUStructuredProc*>(ud);
  if (structProc != NULL) {
    ud = structProc->getBlock();
  }

  NUUseDefStmtNode* udStmt = dynamic_cast<NUUseDefStmtNode*>(ud);
  if ((udStmt == NULL) || !ud->useBlockingMethods()) {
    // We assuming continous drivers kill their defs, but
    // we'll check to see which bits are Z below
    ud->getDefs(&killSet);
  }
  else {
    udStmt->getBlockingKills(&killSet);
  }
  FLNode* localFlow = flow->getFLNode();
  NUNet* localNet = localFlow->getDefNet();
  NUNetRefHdl killNetRef = killSet.findNet(localNet);
  NUNetRefHdl flowRef = localFlow->getDefNetRef();
  *localKill = mNetRefFactory->intersect(killNetRef, flowRef);
  bool hasBits = !((*localKill)->empty());
  if (hasBits) {
    NUNetElab* netElab = flow->getDefNet();
    NUNet* net = netElab->getNet();
    *globalKill = mNetRefFactory->createNetRefImage(net, *localKill);
  }
  return hasBits;
} // bool RENetWarning::findKilledBits

bool RENetWarning::intersects(NUNetElab* netElab,
                              const NUNetRefHdl& netRef,
                              NUNetElabRefSet2* netElabRefSet)
{
  NUNetRefHdl drv = netElabRefSet->getNetRef(netElab);
  NUNetRefHdl xsect = mNetRefFactory->intersect(drv, netRef);
  return ! xsect->empty();
}

// Another design walk, finding z and non-z drivers to nets.  This
// is to be separate because it is a non-nested traversal.
// 
// We do this walk twice.  In the first one we discover the nets with
// conflicts, and in the second one we map out the flows to those nets
// so we can report conflicts that include all the drivers.  We only
// enlarge the map on flows driving nets that are in conflict.
void RENetWarning::walkDesignZ() {
  NUNetElabSet tieNets;

  // First pass -- discover nets with conflicts
  for (RENetWarningIter iter(mAlias, mDesign, mSymbolTable, this,
                             mNetRefFactory, FLIterFlags::eOverAll,
                             FLIterFlags::eIterNodeOnce);
       not iter.atEnd();
       ++iter)
  {
    NUNetRefHdl localKill, flowKillRef;
    if (iter.mDriven && findKilledBits(iter.mDrive, &localKill, &flowKillRef))
    {
      NUNet* masterNet = iter.mMasterNet->getNet();
      FLNode* localFlow = iter.mDrive->getFLNode();
      NUNet* localNet = localFlow->getDefNet();
      NU_ASSERT(masterNet->getBitSize() == localNet->getBitSize(), masterNet);

      // If a net is driven by an enabled driver, and also
      // by a non-enabled driver, then we need to warn.  E.g.
      //    assign a = ena ? in : 1'bz;
      //    assign a = b | c;
      // this is a potential drive conflict and requires a warning.
      // The first assignment causes 'a' to be entered into mEnabledBits,
      // the second causes it to get put into mFullTimeBits
      //
      // always-blocks are a little tricky.  There are two cases
      // where always-blocks drive Z.  One is where ContEnabledDrivers
      // get merged into always-blocks and become BlockingEnableDrivers.
      // In that case we will nest into the enabled driver and find it
      // via the normal iterator.
      //
      // The other case is when an explicit Z driver in an always block
      // is eliminated during LFTristate (new as of Sept 05).  The net
      // is marked as Z, and the local net is also put into a set in the
      // NUStructuredProc (base class of NUAlwaysBlock) to indicate this
      // always-block only conditionally drives that net.
      //
      // To handle these two cases we allow discovery of enabled drivers
      // at the always block level, but we don't assume...
      if (testConflict(iter.mDrive, flowKillRef, &tieNets)) {
        (void) mConflictFlowMap[iter.mMasterNet]; // insert an empty set
      }
    } // if
  } // for

  // Second pass -- map out all the flows driving conflicted nets
  for (RENetWarningIter iter(mAlias, mDesign, mSymbolTable, this,
                             mNetRefFactory, FLIterFlags::eOverAll,
                             FLIterFlags::eIterNodeOnce);
       not iter.atEnd();
       ++iter)
  {
    NUNetRefHdl localKill, globalKill;
    if (iter.mDriven && findKilledBits(iter.mDrive, &localKill, &globalKill)) {
      ElabNetRefFlowSetMap::iterator q =
        mConflictFlowMap.find(iter.mMasterNet);
      if (q != mConflictFlowMap.end()) {
        // Eliminate all tied nets from the map
        if (tieNets.find(iter.mMasterNet) != tieNets.end()) {
          mConflictFlowMap.erase(q);
        }

        // See if this flow flow actually drives conflicted bits.
        // Consider this scenario:
        //    flow1: drives Z     to bit[0]
        //    flow2: drives non-Z to bit[1]
        //    flow3: drives Z     to bits[1:0]
        // flow1 is not conflicted, just flow2 & flow3
        else if (testConflict(iter.mDrive, globalKill, NULL)) {
          FLNodeElabPrintSet& flows = q->second;
          flows.insert(iter.mDrive);
        }
      }
    }
  } // for
} // void RENetWarning::walkDesignZ

bool RENetWarning::testConflict(FLNodeElab* flow, const NUNetRefHdl& killRef,
                                NUNetElabSet* tieNets)
{
  FLNode* localFlow = flow->getFLNode();
  NUNet* localNet = localFlow->getDefNet();
  NUNetElab* netElab = flow->getDefNet();
  NUUseDefNode* ud = flow->getUseDefNode();
  NUContAssign* contAssign = dynamic_cast<NUContAssign*>(ud);
  bool conflict_detected = false;

  if ((contAssign != NULL) && (contAssign->getStrength() == eStrTie)) {
    // Ignore z-drivers on tie-nets.  In the first pass, we might
    // find a tie-net that should override a conflicted driver issue.
    // But in the second pass, we should never hit a tied net
    NU_ASSERT(tieNets, netElab);
    tieNets->insert(netElab);
  }

  else if (ud->drivesZ(localNet)) {
    // The compiler will not discard tristate drivers with elaboratedly
    // constant enables, but we know those drivers will never fire, so
    // we can ignore them for the purposes of being z-written.
    //
    // Note that if the enable were constant in every instant, then
    // constant propagation would figure that out, replace the enable-net
    // with a constant, and tristate simplification would remove
    // the always-off NUContEnabledDriver.  But test/zprop/non_z2.v
    // shows a case where we need to look at the elaborated constants
    // in order to avoid a spurious alert (bug 5478).
    bool discard_driver = testEnableConstant0(ud, flow->getHier());

    if (! discard_driver) {
      conflict_detected = intersects(netElab, killRef, mFullTimeBits);
      mEnabledBits->insert(netElab, killRef);
    }
  }

  // exclude primary BIDs
  else if ((flow->getType() != eFLBoundPort) &&
           (flow->getType() != eFLBoundProtect)) // bug3677
  {
    conflict_detected =
      intersects(netElab, killRef, mEnabledBits) ||
      intersects(netElab, killRef, mFullTimeBits);
    mFullTimeBits->insert(netElab, killRef);
  }
  return conflict_detected;
} // bool RENetWarning::testConflict

class RENetConstReplace : public NuToNuFn {
public:
  RENetConstReplace() {}
  ~RENetConstReplace() {}

  void replace(NUNet* net, const DynBitVector val) {
    mNetToK[net] = val;
  }

  virtual NUExpr* operator()(NUExpr* expr, Phase phase) {
    NUExpr* rep = NULL;
    if (phase == ePre) {
      NUIdentRvalue* id = dynamic_cast<NUIdentRvalue*>(expr);
      if (id != NULL) {
        NUNet* net = id->getIdent();
        NetToK::iterator p = mNetToK.find(net);
        if (p != mNetToK.end()) {
          const DynBitVector& v = p->second;
          UInt32 netSize = net->getBitSize();
          NU_ASSERT(netSize == v.size(), net);
          const SourceLocator& loc = net->getLoc();
          rep = NUConst::create(net->isSigned(), v, netSize, loc);
          delete expr;
        }
      }
    }
    return rep;
  }

  bool hasReplacements() const {return !mNetToK.empty();}

private:
  typedef UtHashMap<NUNet*,DynBitVector> NetToK;
  NetToK mNetToK;
};

bool RENetWarning::testEnableConstant0(NUUseDefNode* ud, STBranchNode* hier) {
  bool isZero = false;

  // test/zprop/non_z2.v -- see if the enable-net is a constant 0
  NUContEnabledDriver* enadriv = dynamic_cast<NUContEnabledDriver*>(ud);
  if (enadriv != NULL) {
    NUExpr* ena = enadriv->getEnable();
    NUNetSet uses;
    ena->getUses(&uses);

    // See if any of the nets used in the enable expression are globally
    // constant.  If so, then map the constant into 
    DynBitVector val;
    RENetConstReplace constReplace;
    NUNet* net = NULL;

    for (NUNetSet::iterator p = uses.begin(), e = uses.end(); p != e; ++p) {
      net = *p;
      NUNetElab* netElab = net->lookupElab(hier);
      if (mIODB->isConstant01(netElab->getSymNode(), &val)) {
        constReplace.replace(net, val);
      }
    }

    // We are going to do an expression substitution and then a fold, but
    // only on a copy of the expression.  This is a global investigation,
    // not a local mutation.
    if (constReplace.hasReplacements()) {
      CopyContext cc(NULL, NULL);
      ena = ena->copy(cc);
      ena = ena->translate(constReplace);
      Fold fold (mNetRefFactory, mArgs, mMsgContext, mIODB->getAtomicCache(),
                 mIODB,
                 false,         // verbose fold
                 eFoldAggressive);
      ena = fold.fold(ena);
      NUConst* k = ena->castConst();
      isZero = (k != NULL) && k->isZero();
      delete ena;
    }
  }
  return isZero;
} // bool RENetWarning::testEnableConstant0

void 
RENetWarning::isValidDriver(FLNodeElab* drive, NUNetElab* driveNet,
                            bool* driven, bool* driverFound, bool checkForConstant)
{
  *driven = drive->getType() != eFLBoundUndriven;
  *driverFound = true;
  NUUseDefNode* ud = drive->getUseDefNode();
  if (*driven && (ud != NULL) && (ud->isPortConn())) {
    // Check the fanins to see if we have any non alias fanins
    *driverFound = false;
    FLNodeElab::AllFaninLoop l;
    for (l = drive->loopAllFanin(); !l.atEnd() && !*driverFound; ++l) {
      FLNodeElab* fanin = *l;
      NUNetElab* faninNet = fanin->getDefNet();
      if ((mAlias == NULL) || !mAlias->isAlias(driveNet, faninNet)) {
        *driverFound = true;
      }
    }

    // If there are no nets acting as drivers, check if this is
    // an input port.  If the actual is NULL, then the port has a
    // driver, but it is not being driven. If the actual is not
    // NULL, and checkForConstant is true, check to see if the
    // actual is a constant. If it is, then the port is considered
    // driven. However, if checkForConstant is false, then any
    // actual expression is considered as being the driver of the
    // input port, even if that actual expression is undriven itself.
    // Check bug 12540 for more details and a testcase.
    if (!*driverFound && ud->isPortConnInput()) {
      NUPortConnectionInput* input;
      input = dynamic_cast<NUPortConnectionInput*>(ud);
      NU_ASSERT(input != NULL, ud);
      NUExpr* actual = input->getActual();
      if (actual == NULL) {
        *driverFound = true;
        *driven = false;
      } else if (checkForConstant) {
	if (actual->castConst() != NULL) {
	  *driverFound = true;
	}
      } else {
        *driverFound = true;
      }
    }

    // If there are no valid fanins, then don't count this as a
    // driver
    if (!*driverFound) {
      *driven = false;
    }
  } // if
} // RENetWarning::isValidDriver

void RENetWarning::printUndrivenWarnings() {
  // Loop over all the bits we care about and print those that are not written
  for (NUNetElabRefSet2::RefLoop p = mReadBits->loopRefs(); !p.atEnd(); ++p)
  {
    NUNetElab* netElab = p.getKey();
    NUNetRefHdl load = p.getValue();
    NUNetRefHdl notStrong = mWrittenBits->getUncovered(netElab, load);
    if (!notStrong->empty())
    {
      // The code to report warnings is complex because it
      // deals with potentially disjoint fragments of a net.
      // So use a vector of net-refs to reprsent the undriven
      // and weakly driven bits.  Index 0 is undriven bits,
      // Index 1 is weakly driven bits.   Also share the reporting
      // code between memories and vectors, using bit/row slice/range
      // nomenclature as appropriate

      NUNetRefHdl undriven = mWeakBits->getUncovered(netElab, notStrong);
      StringAtom* netName = netElab->getNet()->getName();
      bool isTempSymbol = NUScope::isGensym(netName);
      if (!undriven->empty()) {
        if (! isTempSymbol) {
          printRangeWarning(netElab, undriven, eUndriven);
        }

        if (! netElab->getNet()->is2DAnything()) {
          DynBitVector undrivenBits;
          undriven->getUsageMask(&undrivenBits);
          mIODB->rememberConstBits(netElab->getStorageSymNode(), undrivenBits,
                                   mIODB->getUndrivenBitWave());
        }
      }

      if (! isTempSymbol) {
        NUNetRefHdl weak = mNetRefFactory->subtract(notStrong, undriven);
        if (!weak->empty()) {
          printRangeWarning(netElab, weak, eWeaklyDriven);
        }
      }
    }
  }
} // void RENetWarning::printUndrivenWarnings

// Having figured out all the nets that have both Z and non-Z drivers,
// print out all the bits with driver conflicts
void RENetWarning::printZConflictWarnings() {
  for (ElabNetRefFlowSetMap::SortedLoop p = mConflictFlowMap.loopSorted();
       !p.atEnd(); ++p)
  {
    NUNetElab* netElab = p.getKey();
    FLNodeElabPrintSet& printSet = p.getValue();
    NU_ASSERT(!printSet.empty(), netElab);
    NUNetRefHdl enabledBits = mEnabledBits->getNetRef(netElab);
    NUNetRefHdl fullTimeBits = mFullTimeBits->getNetRef(netElab);
    
    NU_ASSERT(!fullTimeBits->empty(), netElab);

    // Loop over all the enabled bits and see which ones are also driven
    // full-time
    NUNetRefHdl conflict = mNetRefFactory->intersect(enabledBits,
                                                     fullTimeBits);

    if (! conflict->empty()) {
      printRangeWarning(netElab, conflict, eDrivenBothZAndNonZ);
      FLNodeElabPrintSet& printSet = mConflictFlowMap[netElab];
      for (FLNodeElabPrintSet::iterator p = printSet.begin(),
             e = printSet.end(); p != e; ++p)
      {
        FLNodeElab* flow = *p;
        NUNet* localNet = flow->getFLNode()->getDefNet();
        NUUseDefNode* ud = flow->getUseDefNode();
        if ((ud != NULL) && ud->drivesZ(localNet)) {
          mMsgContext->REZDriverConflict(flow);
        }
        else {
          mMsgContext->RENonZDriverConflict(flow);

          // Recurse through the fanin and see if there are any
          // Z drivers that have lost their Z-state through an
          // assign, bit-select, etc.
          ZLossMap zlossMap;
          walkZLoss(flow, true, &zlossMap);
        }
      }
    } // if

    // If a net got inserted into mConflictFlowMap and there were no enabled
    // bits, then there must have been some multiply full-strength driven
    // bits.  We won't be able to figure out which bits they were from
    // the map, but we need to detect whether there was a Z-loss on any
    // of the fanins. 
    else {
      // If there is a full-time driver conflict keep track of
      // all the drivers.  We want to warn on:
      //     v[1:0] = ena1 ? in1[1:0] : 2'bz;
      //     w[1:0] = ena2 ? in2[1:0] : 2'bz;
      //     out    = v[0];
      //     out    = w[0];
      // where out has two non-Z drivers and no Z drivers.  But
      // we don't want to warn on
      //     out    = a & b;
      //     out    = a & b;
      // because they may just be redundant drivers used for
      // electrical reasons.
      //
      // So first we have to walk the fanin to detect which we have found.
      bool hasZLoss = false;
      FLNodeElabVector conflictedFlows;

      for (FLNodeElabPrintSet::iterator p = printSet.begin(),
             e = printSet.end(); p != e; ++p)
      {
        FLNodeElab* flow = *p;
        NUNet* localNet = flow->getFLNode()->getDefNet();
        NUUseDefNode* ud = flow->getUseDefNode();
        FLN_ASSERT(ud != NULL, flow);

        // Note that there may be unconflicted Z drivers on net[0], and conflicting
        // Z drivers on net[1].  See test/bugs/bug257/bid_hier.v, which probably meant
        // to have "32'bz" but has "1'bz" used in a context where 32 Zs are required,
        // creating conflicts on out[31:1].  But we don't need to report anything
        // regarding out[0]
        if (! ud->drivesZ(localNet)) {
          ZLossMap zlossMap;
          hasZLoss = walkZLoss(flow, false, &zlossMap); // don't print it yet

          // Find the bits that are driven in conflict
          NUNetRefHdl drivenBits, localKill;
          if (findKilledBits(flow, &localKill, &drivenBits)) {
            conflictedFlows.push_back(flow);
            NUNetRefHdl conflict_bits = 
              mNetRefFactory->intersect(drivenBits, fullTimeBits);
            conflict = mNetRefFactory->merge(conflict_bits, conflict);
          }
        }
      }

      // If any of the drivers has a lost Z, then print alerts.
      if (hasZLoss) {
        NU_ASSERT(!conflict->empty(), netElab);
        printRangeWarning(netElab, conflict, eMultipleStrongDrivers);
        for (UInt32 i = 0, n = conflictedFlows.size(); i < n; ++i) {
          FLNodeElab* flow = conflictedFlows[i];
          mMsgContext->RENonZDriverConflict(flow);
          ZLossMap zlossMap;
          (void) walkZLoss(flow, true, &zlossMap);
        }
      }
    } // else
  } // for
} // void RENetWarning::printZConflictWarnings

// Having determined that a Z/Non-Z conflict exists, let's
// explore the source of the Non-Z drivers and see if we can
// find some Z drivers in the fanin cone that have lost their
// Z-state through some operation that in Verilog semantics
// preserves Z, but in Carbon does not (yet)
bool RENetWarning::walkZLoss(FLNodeElab* flow, bool printIt, ZLossMap* zlossMap) {
  ZLossMap::iterator zloss = zlossMap->find(flow);
  if (zloss != zlossMap->end()) {
    return zloss->second;
  }
  (*zlossMap)[flow] = false;


  FLNode* localFlow = flow->getFLNode();
  NUNet* localDestNet = localFlow->getDefNet();
  NUUseDefNode* ud = flow->getUseDefNode();
  bool driverLosesZ = false;

  FLNodeElabVector zLossVec;

  // Suppose we have "net=a & b".  That does not preserve Z in event sim,
  // so we don't need to worry about losing them in Carbon.  Check
  // each fanin net
  for (Fanin faninLoop(mFLNodeElabFactory, flow, eFlowOverCycles);
       !faninLoop.atEnd(); ++faninLoop)
  {
    FLNodeElab* fanin = *faninLoop;
    FLNode* localFaninFlow = fanin->getFLNode();
    NUNet* localSrcNet = localFaninFlow->getDefNet();

    if ((ud != NULL) && ud->shouldPreserveZ(localSrcNet, localDestNet) &&
        !ud->drivesZ(localDestNet))
    {
      zLossVec.push_back(fanin);
    }
  }
  
  if (printIt) {
    std::sort(zLossVec.begin(), zLossVec.end(), FLNodeElabCmp());
  }
  for (UInt32 i = 0, n = zLossVec.size(); i < n; ++i) {
    FLNodeElab* fanin = zLossVec[i];
    NUNetElab* faninNet = fanin->getDefNet();

    // If got here it might have been net=sel?a:b, and so if a or b
    // are Z, then we need to preserve the Z across the assign.  So
    // if we are not doing this we need to warn

    if (mEnabledBits->getNetRef(faninNet)->empty()) {
      // 'a' might not be an enabled driver, but it might be
      // assigned from one, so rercurse
      driverLosesZ |= walkZLoss(fanin, printIt, zlossMap);
    }
    else {
      // Report the conflict on the fanin that drives the net
      if (printIt) {
        mMsgContext->REZLossage(fanin);
      }
      driverLosesZ = true;
    }
  }

  (*zlossMap)[flow] = driverLosesZ;
  return driverLosesZ;
} // bool RENetWarning::walkZLoss

void RENetWarning::printRangeWarning(NUNetElab* netElab,
                                     const NUNetRefHdl& undriven,
                                     Warning warning)
{
  NUVectorNet* vn = netElab->getNet()->castVectorNet();
  int msgIndex = (vn == NULL)? 1: 0;

  static const char* bitMsgs[] = {"Bit", "Row"};
  static const char* sliceMsgs[] = {"Slice", "Range"};

  if (undriven->all()) {
    printSliceWarning(netElab, warning, "Net");
  }
  else {
    // Print error messages in contiguous ranges
    for (NUNetRefRangeLoop r = undriven->loopRanges(mNetRefFactory);
         !r.atEnd(); ++r)
    {
      ConstantRange range = *r;

      if (vn != NULL) {     // vectors are normalized, memories are not
        range.denormalize(vn->getDeclaredRange());
      }
      UtString buf;
      if (range.getMsb() == range.getLsb()) {
        buf << bitMsgs[msgIndex] << " [" << range.getMsb() << "]";
      }
      else {
        buf << sliceMsgs[msgIndex] << " [" << range.getMsb()
            << ":" << range.getLsb() << "]";
      }
      printSliceWarning(netElab, warning, buf.c_str());
    }
  } // else if
} // void RENetWarning::printRangeWarning

void RENetWarning::printSliceWarning(NUNetElab* netElab, Warning warning,
                                     const char* slice)
{
  switch (warning) {
  case eUndriven:
    mMsgContext->RENetUndriven(netElab, slice);
    break;
  case eWeaklyDriven:
    mMsgContext->RENetWeaklyDriven(netElab, slice);
    break;
  case eDrivenBothZAndNonZ:
    mMsgContext->RENetDrivenBothZAndNonZ(netElab, slice);
    break;
  case eMultipleStrongDrivers:
    mMsgContext->RENetMultipleStrongDrivers(netElab, slice);
    break;
  case eDead:
    mMsgContext->RENetDead(netElab, slice);
    break;
  }
}

// Compute the bits of live vector nets that have no live drivers and no loads.
// We don't necessarily need to warn about them (maybe we will) but we do need
// to mark them in the IODB file so the shell can display those bits correctly.
void RENetWarning::computeDeadBits()
{
  // Loop over all the bits we care about and print those that are not written
  for (NUNetElabRefSet2::RefLoop p = mReadBits->loopRefs(); !p.atEnd(); ++p)
  {
    NUNetElab* netElab = p.getKey();

    // In schedule/MarkDesign.cxx, there is a call to
    // setSCHSignature(leaf,NULL) which removes nets from the
    // waveform.  Don't bother reporting dead bits on nets that are
    // missing entirely from the waveform.  This affects
    // test/block-splitting/part_select.v.
    //
    // Note that memories are never in waveforms, but the dead-row warnings
    // are cool -- let's show them anyway.
    //
    // Also note, that if we mark something inaccurate during
    // optimizations, it won't be waveformed, so don't clutter the log
    // output.
    NUNet* net = netElab->getNet();
    bool isMem = net->is2DAnything();
    bool showDeadWarning = isMem;
    if (!showDeadWarning) {
      STAliasedLeafNode* leaf = netElab->getSymNode();
      const SCHSignature* sig = CbuildSymTabBOM::getSCHSignature(leaf);
      showDeadWarning = (sig != NULL);
    }
    if (showDeadWarning && !net->isInaccurate()) {
      NUNetRefHdl load = p.getValue();
      NUNetRefHdl allBits = mNetRefFactory->createNetRef(net);

      // Compute the bits that have no fanout
      NUNetRefHdl deadBits = mNetRefFactory->subtract(allBits, load);

      // Compute the bits that are written 
      NUNetRefHdl strongBits = mWrittenBits->getNetRef(netElab);
      NUNetRefHdl weakBits = mWeakBits->getNetRef(netElab);
      NUNetRefHdl writtenBits = mNetRefFactory->merge(strongBits, weakBits);

      // Compute the bits that have no live driver, and thus must be marked X
      // in waveforms.
      NUNetRefHdl xBits = mNetRefFactory->subtract(deadBits, writtenBits);
      if (!xBits->empty()) {
        DynBitVector xMask;
        xBits->getUsageMask(&xMask);
        StringAtom* netName = net->getName();
        bool isTempSymbol = NUScope::isGensym(netName);
        if (! isTempSymbol) {
          printRangeWarning(netElab, xBits, eDead);
        }

        // Note, we are marking temp const bits because temps can end
        // up in CarbonExprs for visible nets. We want to mark the
        // unwavable bits.
        if (! isMem) {
          mIODB->rememberConstBits(netElab->getStorageSymNode(), xMask,
                                   mIODB->getDeadBitWave());
        }
      }
    } // if
  } // for
} // void RENetWarning::computeDeadBits

void RENetWarning::printUndrivenLocalOuts() {
  // Undriven local outputs are missed if they are connected to a
  // driven net at a higher level.  They are still funky and dubious
  // and we should still warn about them, although perhaps only
  // if they are not also warned about elaboratedly.  For now, warn
  // about undriven outputs even if they are also warned about
  // elaboratedly, at least if there is some live elaborated reader
  // somewhere
  //
  // NOTE: use getModulesBottomUp instead of loopAllModules, because
  // the latter can loop "dead" (always flattened) modules, which is bad.
  NUModuleList modules;
  mDesign->getModulesBottomUp(&modules);
  for (NUModuleList::iterator module_iter = modules.begin();
       module_iter != modules.end();
       ++module_iter) {
    const NUModule* mod = *module_iter;
    for (NUNetFilterCLoop p = mod->loopOutputPorts(); !p.atEnd(); ++p) {
      const NUNet* outport = *p;
      if (outport->isUndriven() &&
          !outport->isPrimaryOutput() && // these are covered by net warnings
          !outport->isPulled() &&
          !outport->isDead()) {
        mMsgContext->REUndrivenOutPort(outport);
      }
    }
  }
} // void RENetWarning::printUndrivenLocalOuts

// Consider test/constprop/partial_const.v.  There is a live
// wire ("temp") with one constant bit.  It gets rescoped into
// the always-block but constant propagation finds the constant
// bit and annotates it.  The Shell does not know how to deal
// with partial constants on dead nets, so make all the bits we
// don't know about be "X"
void RENetWarning::fixPartialDeadConstants() {
  for (IODB::NameValueLoop p = mIODB->loopConstNets(); !p.atEnd(); ++p) {
    const STSymbolTableNode* symNode = p.getKey();
    const STAliasedLeafNode* node = symNode->castLeaf();
    ST_ASSERT(node,symNode);
    const DynBitVector* value = p.getValue();

    NUNet* net = NUNet::find(node);
    if (net != NULL) {
      NUNetElab* net_elab = NUNetElab::find(node);
      bool dead = true;
      if (net_elab != NULL) {
        // If there are bits that are completely uncovered, and
        // are not marked constant, then mark them X.
        NUNetRefHdl liveBits = mReadBits->getNetRef(net_elab);
        liveBits = mNetRefFactory->merge(liveBits,
                                         mWrittenBits->getNetRef(net_elab));
        liveBits = mNetRefFactory->merge(liveBits,
                                         mWeakBits->getNetRef(net_elab));
        dead = liveBits->empty();
      }

      if (dead) {
        // Figure out which bits are not marked constant, and mark them live.
        // Note that the low-order bits of the DynBitVector in the IODB is
        // the mask of what's been identified as constant already
        UInt32 size = net->getBitSize();
        DynBitVector xmask(size, *value);
        xmask.flip ();

        mIODB->declareConstBits(node, xmask, eValueX);
      }
    } // if
  }
} // void RENetWarning::fixPartialDeadConstants

void RENetWarning::getAliases(NUNetElab* netElab, NUNetElabVector* netElabs)
  const
{
  if (mAlias != NULL) {
    mAlias->getAliases(netElab, netElabs);
  }
  else {
    netElabs->push_back(netElab);
  }
}

bool RENetWarning::queryProtectedMutable(NUNetElab* netElab) const {
  return netElab->getNet()->isProtectedMutable(mIODB) ||
    mIODB->isCollapseSubordinate(netElab);
}

bool RENetWarning::queryProtectedObservable(NUNetElab* netElab) const {
  return netElab->getNet()->isProtectedObservable(mIODB);
}

bool RENetWarning::queryPrimaryOutput(NUNetElab* netElab) const {
  return netElab->isPrimaryOutput();
}

bool RENetWarning::checkNetFlag(NUNetElab* netElab,
                                NetBoolMap* cache,
                                NetQueryFunction queryFn)
  const
{
  NetBoolMap::iterator p = cache->find(netElab);
  if (p != cache->end()) {
    return p->second;
  }

  // Get all the elaborated nets that are aliased together
  NUNetElabVector netElabs;
  getAliases(netElab, &netElabs);

  // Walk the aliases looking for mutable points. These can be
  // protected mutable or part of a collapse clock directive.
  bool flag = false;
  for (NUNetElabVectorLoop l(netElabs); !l.atEnd() && !flag; ++l) {
    NUNetElab* curNetElab = *l;
    if ((this->*queryFn)(curNetElab)) {
      flag = true;
    }
  }

  // memoize the result so we don't have to do the protected-mutable
  // search again
  for (NUNetElabVectorLoop l(netElabs); !l.atEnd(); ++l) {
    NUNetElab* curNetElab = *l;
    NU_ASSERT(cache->find(curNetElab) == cache->end(), curNetElab);
    (*cache)[curNetElab] = flag;
  }

  return flag;
} // bool RENetWarning::checkNetFlag

bool RENetWarning::isProtectedObservable(NUNetElab* netElab) const {
  return checkNetFlag(netElab, mProtectedObservableCache,
                      &RENetWarning::queryProtectedObservable);
}

//! Determine if any of the local nets in this netelab's alias ring
//! have been marked as protected-mutable
bool RENetWarning::isProtectedMutable(NUNetElab* netElab) const {
  return checkNetFlag(netElab, mProtectedMutableCache,
                      &RENetWarning::queryProtectedMutable);
}

bool RENetWarning::isPrimaryOutput(NUNetElab* netElab) const {
  return checkNetFlag(netElab, mPrimaryOutputCache,
                      &RENetWarning::queryPrimaryOutput);
}

void RENetWarning::addCommandlineArgs(ArgProc * arg)
{
  // Create switches to enable/disable portions of global optimizations
  scUndrivenWave = CRYPT ("-undrivenWave");
  scDeadWave = CRYPT ("-deadWave");

  arg->addString(scUndrivenWave,
                 CRYPT("Waveform value to show (01XZ) for bits that are not driven in the design"),
                 "Z", false, false, CarbonContext::ePassCarbon);
  arg->addString(scDeadWave,
                 CRYPT("Waveform value to show (01XZ) for bits that cannot affect design outputs"),
                 "X", false, false, CarbonContext::ePassCarbon);
}

void RENetWarning::putREAlias(REAlias* alias)
{
  mAlias = alias;
 }

// Another design walk, dumping all net drivers to a file, in the form 
//
// hier.path.to.net [bits] hier.path.to.driver file:line comment
//
// The MSB and LSB will be in terms of the net's declaration bounds,
// not the normalized ones used inside cbuild.
void RENetWarning::dumpDrivers(ZostreamDB* file) {
  for (RENetWarningIter iter(mAlias, mDesign, mSymbolTable, this,
                             mNetRefFactory, FLIterFlags::eOverAll,
                             FLIterFlags::eIterNodeOnce);
       not iter.atEnd();
       ++iter)
  {
    FLNodeElab* flow = iter.mDrive;
    NUUseDefNode* node = flow->getUseDefNode();
    if (node != NULL) {         // ignore undriven and PI
      dumpDriver(file, flow, 0);
    }
  } // for
  *file << 'Z';                 // eof marker
} // void RENetWarning::dumpDrivers

void RENetWarning::dumpDriver(ZostreamDB* file, FLNodeElab* flow, UInt32 depth)
{
  // Recurse into nesting as required
  FLNodeElabSet nestedFlows;
  ++depth;
  for (FLNodeElabLoop p = flow->loopNested(); !p.atEnd(); ++p) {
    FLNodeElab* nested = *p;
    dumpDriver(file, nested, depth);
    nestedFlows.insert(nested);
  }
  --depth;
  STBranchNode* driverPath = flow->getHier();
  STSymbolTableNode* netPath = flow->getDefNet()->getSymNode();

  NUUseDefNode* node = flow->getUseDefNode();

#if 0
  // If this is a port-connection, then rather than dumping the driver
  // (which might be in the wrong direction anyway), just note the aliasing
  // of the net and fanin
  if (dynamic_cast<NUPortConnection*>(node) != NULL) {
    for (FLNodeElabLoop p = flow->loopFanin(); !p.atEnd(); ++p) {
      FLNodeElab* faninFlow = *p;
      STSymbolTableNode* fanin = faninFlow->getDefNet()->getSymNode();
    
    return;
  }
#endif

  const SourceLocator loc = node->getLoc();
  const char* filename = loc.getFile();
  const char* type = node->typeStr();
  if (strncmp(type, "NU", 2) == 0) {
    type += 2;              // some are prefixed with NU, some not
  }

  // Pre-write the filename and hierarachy info, if needed
  writeString(file, filename);
  writeString(file, type);
  writeHierarchy(file, driverPath);
  writeHierarchy(file, netPath);

  STSymbolTableNodeSet faninNets;
  for (FLNodeElabLoop p = flow->loopFanin(); !p.atEnd(); ++p) {
    FLNodeElab* faninFlow = *p;
    STSymbolTableNode* fanin = faninFlow->getDefNet()->getSymNode();
    writeHierarchy(file, fanin);
    faninNets.insert(fanin);
  }

  UtString netBits;
  NUNetRefHdl ref = flow->getDefNetRef(mNetRefFactory);
  ref->printBits(&netBits, 0, true);
  if (netBits.empty()) {
    netBits = "[*]";
  }

  *file << 'D' << loc.getLine() << netBits << depth;
  file->writePointer(filename);
  file->writePointer(driverPath);
  file->writePointer(netPath);
  file->writePointer(type);
  file->writePointerContainer(faninNets);
  file->writePointerContainer(nestedFlows);
  file->mapPtr(flow);
} // void RENetWarning::dumpDriver

void RENetWarning::writeString(ZostreamDB* db, const char* str) {
  if (! db->isMapped(str)) {
    *db << 'S' << str;
    db->mapPtr(str);
  }
}

void RENetWarning::writeHierarchy(ZostreamDB* db, STSymbolTableNode* node) {
  if (! db->isMapped(node)) {
    STBranchNode* parent = node->getParent();
    if (parent != NULL) {
      writeHierarchy(db, parent);
    }
    const char* str = node->strObject()->str();
    writeString(db, str);
    *db << 'P';
    db->writePointer(parent);
    db->writePointer(str);
    db->mapPtr(node);
  }
}
