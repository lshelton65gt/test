// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUTF.h"

#include "symtab/STSymbolTable.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "reduce/CleanTaskResolutions.h"

#include "hdl/HdlVerilogPath.h"

//! Abstraction to keep track of live task resolutions per task enable
typedef UtMap<NUTaskEnable*, NUTaskSet> LiveTasks;

//! Class to walk all the hier refs
class TaskHierRefWalkCallback : public NUInstanceCallback
{
public:
  //! constructor
  TaskHierRefWalkCallback(STSymbolTable* symtab) :
    NUInstanceCallback(symtab) {}

  //! destructor
  ~TaskHierRefWalkCallback() {}

protected:
  //! Function to be called on every module instance
  virtual void handleModule(STBranchNode* branch, NUModule* mod)
  {
    // Find all the hier ref task enables and call the function to
    // process them.
    NUTaskEnableVector taskEnables;
    mod->getTaskEnableHierRefs(&taskEnables);
    for (NUTaskEnableCLoop l(taskEnables); !l.atEnd(); ++l)
    {
      NUTaskEnable* enable = *l;
      handleHierTaskEnable(enable, branch);
    }
  }

  //! No hierarchically referenced task enables expected in decl scopes.
  virtual void handleDeclScope(STBranchNode*, NUNamedDeclarationScope*) {}

  virtual void handleInstance(STBranchNode*,NUModuleInstance*) {}

protected:
  //! Virtual abstraction function to work on a hier ref task enable
  virtual void handleHierTaskEnable(NUTaskEnable* enable,
                                    STBranchNode* branch) = 0;

private:
  //! Hide copy and assign constructors.
  TaskHierRefWalkCallback(const TaskHierRefWalkCallback&);
  TaskHierRefWalkCallback& operator=(const TaskHierRefWalkCallback&);
}; // class TaskHierRefWalkCallback : public NUDesignWalker

//! Class to mark all elaboratedly referenced tasks as live
class FindLiveTaskResolutions : public TaskHierRefWalkCallback
{
public:
  //! constructor
  FindLiveTaskResolutions(STSymbolTable* symTab, LiveTasks* liveTasks) :
    TaskHierRefWalkCallback(symTab), mLiveTasks(liveTasks)
  {}

  //! destructor
  ~FindLiveTaskResolutions() {}

protected:
  //! Function to mark live tasks
  virtual void handleHierTaskEnable(NUTaskEnable* enable, STBranchNode* branch)
  {
    // Resolve this hier ref instance
    NUTaskHierRef* taskHierRef = enable->getHierRef();
    const AtomArray& taskPath = taskHierRef->getPath();
    STSymbolTableNode* taskNode;
    taskNode = NUHierRef::resolveHierRef(taskPath, 0, getSymtab(), branch);
    if (taskNode == NULL)
    {
      HdlVerilogPath pather;
      UtString branchName, taskName;
      pather.compPathHier(branch, &branchName);
      taskHierRef->getName(&taskName, pather);
      UtIO::cout() << "resolveHierRef FAILURE: taskPath = " << taskName
                   << ", branch = " << branchName << UtIO::endl;
    }

    NU_ASSERT(taskNode != NULL, enable);

    // Convert the symbol table resolution to an NUTask
    STBranchNode* taskBranch = taskNode->castBranch();
    NUTFElab* tfElab = NUTFElab::lookup(taskBranch);
    NUTask* task = dynamic_cast<NUTask*>(tfElab->getTF());

    // While we are here, fixup the NUTFArgConnections
    NUTFArgConnectionLoop l;
    for (l = enable->loopArgConnections(); !l.atEnd(); ++l)
    {
      NUTFArgConnection* arg = *l;
      if (arg->getTF() != task)
        arg->replaceTF(task);
    }

    // Record this in our live set
    (*mLiveTasks)[enable].insert(task);
  } // virtual void handleHierTaskEnable

private:
  //! Hide copy and assign constructors.
  FindLiveTaskResolutions(const FindLiveTaskResolutions&);
  FindLiveTaskResolutions& operator=(const FindLiveTaskResolutions&);

  // Local data
  LiveTasks* mLiveTasks;
}; // class FindLiveTaskResolutions : public hierRefWalkCallback

//! Class to clean up dead resolutions
class RemoveDeadTaskResolutions : public TaskHierRefWalkCallback
{
public:
  //! constructor
  RemoveDeadTaskResolutions(STSymbolTable* symTab, const LiveTasks& liveTasks)
    : TaskHierRefWalkCallback(symTab), mLiveTasks(liveTasks)
  {}

  //! destructor
  ~RemoveDeadTaskResolutions() {}

protected:
  //! Function to process a given heir ref (delete dead tasks)
  virtual void handleHierTaskEnable(NUTaskEnable* enable, STBranchNode*)
  {
    // Get the live tasks for this enable
    LiveTasks::const_iterator pos = mLiveTasks.find(enable);
    NU_ASSERT(pos != mLiveTasks.end(), enable);
    const NUTaskSet& tasks = pos->second;

    // Visit all the resolutions and find the live ones
    NUTaskHierRef* taskHierRef = enable->getHierRef();
    NUTaskHierRef::TaskVectorLoop l;
    NUBaseVector newTasks;
    bool deadTaskFound = false;
    for (l = taskHierRef->loopResolutions(); !l.atEnd(); ++l)
    {
      NUTask* task = *l;
      if (tasks.find(task) == tasks.end())
      {
        // This is a dead resolution
        deadTaskFound = true;
        task->removeHierRef(enable);
      }
      else
        newTasks.push_back(task);
    }

    // If we found any dead tasks, replace the resolutions
    if (deadTaskFound)
    {
      NU_ASSERT(!newTasks.empty(), enable);
      taskHierRef->replaceResolutions(newTasks);
    }
  } // virtual void handleHierTaskEnable

private:
  //! Hide copy and assign constructors.
  RemoveDeadTaskResolutions(const RemoveDeadTaskResolutions&);
  RemoveDeadTaskResolutions& operator=(const RemoveDeadTaskResolutions&);

  // Local data
  const LiveTasks& mLiveTasks;
}; // class RemoveDeadTaskResolutions : public TaskHierRefWalkCallback

void CleanTaskResolutions::design(NUDesign* design, STSymbolTable* symTab)
{
  // Walk the design to mark the live tasks
  LiveTasks liveTasks;
  FindLiveTaskResolutions liveCallback(symTab, &liveTasks);
  NUDesignWalker liveWalker(liveCallback, false, false);
  liveWalker.design(design);

  // Walk the design to remove dead resolutions
  RemoveDeadTaskResolutions removeDeadCallback(symTab, liveTasks);
  NUDesignWalker removeDeadWalker(removeDeadCallback, false, false);
  removeDeadWalker.design(design);
}

