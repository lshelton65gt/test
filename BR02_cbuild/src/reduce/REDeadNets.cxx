// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/REDeadNets.h"
#include "nucleus/NUDesignWalker.h"
#include "flow/FLIter.h"
#include "iodb/IODBNucleus.h"

/*!
  \file 
  Implementation of class used to clear/set the dead flag on nets.
 */

REDeadNets::REDeadNets(STSymbolTable * symbol_table,
                       FLNodeElabFactory * flow_elab_factory,
                       IODBNucleus * iodb) :
  mSymbolTable(symbol_table),
  mFlowElabFactory(flow_elab_factory),
  mIODB(iodb)
{}


void REDeadNets::design(NUDesign * the_design)
{
  markDead(the_design);
  markLive(the_design);
}


//! Callback class for walker to mark all nets dead.
class MarkNetsDeadCallback : public NUDesignCallback
{
public:
  //! Constructor
  MarkNetsDeadCallback() :
    NUDesignCallback()
  {}

  //! Destructor
  ~MarkNetsDeadCallback() {}

  //! Walk through all Nucleus elements.
  Status operator()(Phase, NUBase*) { return eNormal;}

  //! When the walk encounters a module, grab all nets and mark them dead.
  Status operator()(Phase phase, NUModule* module) {
    if (phase == ePre) {
      NUNetList nets;
      module->getAllNonTFNets(&nets);
      for (NUNetList::iterator iter = nets.begin(); iter != nets.end(); ++iter) {
        NUNet* net = (*iter);
        net->putIsDead(true);
      }
    }
    return eNormal;
  }
};


void REDeadNets::markDead(NUDesign * the_design)
{
  MarkNetsDeadCallback callback;
  NUDesignWalker walker(callback, false);
  walker.design(the_design);
}


void REDeadNets::markLive(NUDesign * the_design)
{
  NUNetElabSet liveElabs;
  for (FLDesignElabIter iter(the_design,
 			     mSymbolTable,
 			     FLIterFlags::eClockMasters,
 			     FLIterFlags::eAll,  // Visit everything
 			     FLIterFlags::eNone, // Stop at nothing
 			     FLIterFlags::eBranchAtAll,
 			     FLIterFlags::eIterFaninPairOnce);
       not iter.atEnd();
       iter.next())
  {
    // Mark the driving bits as such
    FLNodeElab* driver = *iter;
    NUNetElab* net = driver->getDefNet();
    if (net != NULL) {
      liveElabs.insert(net);
    }
  }
  
  // Get the protected mutable/observable nunetelabs
  //
  // Ideally, this call is unnecessary. We tried removing it and had a
  // number of nightly tests crash (usually in backend).
  mIODB->appendProtectedNetElabs(&liveElabs, the_design);

  // Mark all aliases of the live elaborated nets as live.
  for (NUNetElabSet::iterator iter = liveElabs.begin();
       iter != liveElabs.end();
       ++iter) {
    NUNetElab * net_elab = (*iter);
    STAliasedLeafNode * master_leaf = net_elab->getSymNode();
    for (STAliasedLeafNode::AliasLoop loop(master_leaf); 
         not loop.atEnd(); 
         ++loop) {
      STAliasedLeafNode* alias = (*loop);
      NUNet* alias_net = NUNet::find(alias);
      alias_net->putIsDead(false);
    }
  }
}
