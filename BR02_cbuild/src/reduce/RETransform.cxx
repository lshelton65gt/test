// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "reduce/RETransform.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUDesignWalkerCallbacks.h"

#include "nucleus/NUNetSet.h"
#include "nucleus/NUBitNet.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "flow/FLFactory.h"
#include "flow/FLElabUseCache.h"

#include "localflow/UD.h"
#include "localflow/Reorder.h"
#include "localflow/TFRewrite.h"

#include "reduce/AllocAlias.h"
#include "reduce/CodeMotion.h"
#include "reduce/ControlExtract.h"
#include "reduce/Fold.h"
#include "reduce/Inference.h"
#include "reduce/Rescope.h"
#include "reduce/SplitFlow.h"
#include "reduce/RewriteUtil.h"
#include "reduce/LocalConstProp.h"
#include "reduce/LocalCopyProp.h"
#include "reduce/Ternary.h"
#include "reduce/TernaryGateOptimization.h"

#include "symtab/STSymbolTable.h"


#include "util/ArgProc.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"

#include "compiler_driver/CarbonContext.h"
#include "compiler_driver/CarbonDBWrite.h"

RETransform::RETransform(STSymbolTable *symtab, AtomicCache *str_cache,
                         SourceLocatorFactory *loc_factory,
                         FLNodeFactory *flow_factory,
                         FLNodeElabFactory *flow_elab_factory,
                         NUNetRefFactory *netref_factory,
                         MsgContext *msg_context,
                         IODBNucleus *iodb,
                         ArgProc *args) :
  mSymtab(symtab), 
  mStrCache(str_cache), 
  mLocFactory(loc_factory),
  mFlowFactory(flow_factory),
  mFlowElabFactory(flow_elab_factory),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mIODB(iodb),
  mArgs(args)
{
  mBufferAndFlow = new BufferAndFlow;
  mBufferedInputs = new BufferedInputs;
  mModNetDelayBlocks = new ModNetDelayBlocks;
  mTempNets = new TempNets;
  mRepairBlocks = new NUAlwaysBlockSet;
  mOldToNewAlwaysBlocks = new OldToNewAlwaysBlocks;
  mTempClkExprs = new TempClkExprs;
  mClkConstReplacements = new ClkConstReplacements;
}

RETransform::~RETransform()
{
  delete mBufferAndFlow;
  delete mBufferedInputs;
  delete mModNetDelayBlocks;
  INFO_ASSERT(mTempNets->empty(),
              "Inconsistency found in buffer insertion code");
  delete mTempNets;
  NU_ASSERT(mRepairBlocks->empty(), *(mRepairBlocks->begin()));
  delete mRepairBlocks;
  delete mOldToNewAlwaysBlocks;
  delete mTempClkExprs;
  delete mClkConstReplacements;
} // RETransform::~RETransform

void
RETransform::addBuffer(FLNodeElabSet& outFlows, NUNetRefSet& inNetRefs,
                       bool replaceEdges)
{
  // Go through the data provided and get the usedef for this block
  NUUseDefNode* useDef = NULL;
  FLNodeElabSet* outFlowCopy = new FLNodeElabSet;
  for (FLNodeElabSetIter f = outFlows.begin(); f != outFlows.end(); ++f)
  {
    // Get the usedef
    FLNodeElab* flow = *f;
    if (useDef == NULL)
      useDef = flow->getUseDefNode();
    else
      NU_ASSERT2(useDef == flow->getUseDefNode(), useDef, flow->getUseDefNode());

    // Copy this flow over to our private copy. See below for why
    outFlowCopy->insert(flow);
  }

  // Call the local flow routines to create the new set of blocks
  // fix up the use/def information.
  NUAlwaysBlockVector alwaysBlocks;
  addBufferBlock(useDef, inNetRefs, &alwaysBlocks, replaceEdges);

  // Store this information for post processing. We don't process UD
  // immediately because we have to run UD on the module. We update
  // the flow after UD. So we store the new always block and the
  // output flows that are affected by this change.
  bool flowsInserted = false;
  NUAlwaysBlockVector::iterator i;
  for (i = alwaysBlocks.begin(); i != alwaysBlocks.end(); ++i)
  {
    NUAlwaysBlock* always = *i;
    BufferAndFlow::iterator pos = mBufferAndFlow->find(always);
    if (pos == mBufferAndFlow->end())
    {
      NU_ASSERT(!flowsInserted, always);
      flowsInserted = true;
      mBufferAndFlow->insert( BufferAndFlowValue(always, outFlowCopy) );
    }
    else
    {
      FLNodeElabSet* otherFlows = pos->second;
      otherFlows->insert(outFlowCopy->begin(), outFlowCopy->end());
    }
  }
  if (!flowsInserted)
    delete outFlowCopy;

} // RETransform::addBuffer

void
RETransform::fixupBufferFlow(NUDesign* design, NewFlows* newFlows,
                             FLNodeElabSet* deletedElabFlows,
                             ClockAndDriver* clockAndDriver,
                             Stats* stats)
{
  // Make sure we have work to do. If we don't exiting early saves on
  // some empty iteration below.
  if (mBufferAndFlow->empty())
    return;

  // Find all the affect modules
  NUModuleSet modules;
  for (BufferAndFlowLoop l(*mBufferAndFlow); !l.atEnd(); ++l) {
    NUAlwaysBlock* always = l.getKey();
    NUModule* module = always->findParentModule();
    modules.insert(module);
  }

  // Find all the instances of the modules
  ModuleHierarchies moduleHierarchies;
  findModuleHierarchies(modules, &moduleHierarchies);
  
  // Run UD on all the modules
  UD ud (mNetRefFactory, mMsgContext, mIODB, mArgs, false);
  for (NUModuleSetIter m = modules.begin(); m != modules.end(); ++m) {
    NUModule* module = *m;
    ud.module(module);
  }

  // Verify that all uses of the input nets were removed now that UD
  // is up to date.
  for (BufferedInputsLoop l(*mBufferedInputs); !l.atEnd(); ++l) {
    NUAlwaysBlock* always = l.getKey();
    NUNetRefSet* inNetRefs = l.getValue();
    verifyBufferedNets(always, *inNetRefs);
  }

  // Elaborate the nets, need to do this as a separate pass to
  // creating flow; it is possible that some of the elaborated flow is
  // dead, so doing a separate pass ensures we see it.
  //
  // We need to do this after UD is done above
  elaborateTempNets(moduleHierarchies, NULL);

  // Now that UD is done, we can create the flow infrastructure. We
  // have to update the flow for the output flow and create new flow
  // for the new buffer block.
  for (BufferAndFlowLoop l(*mBufferAndFlow); !l.atEnd(); ++l) {
    NUAlwaysBlock* always = l.getKey();
    FLNodeElabSet* flows = l.getValue();

    NUBlock* block = always->getBlock();
    for (NUStmtLoop s = block->loopStmts(); !s.atEnd(); ++s)
    {
      NUStmt* stmt = *s;
      NUBlockingAssign* assign = dynamic_cast<NUBlockingAssign*>(stmt);
      NUNetRefSet netRefs(mNetRefFactory);
      assign->getBlockingDefs(&netRefs);

      // We could have used a vector and a covered-set and iterate
      // over the vector here.  That would be faster.
      for (NUNetRefSet::SortedLoop n = netRefs.loopSorted(); !n.atEnd(); ++n) {
        // Create the new unelaborated flow node
        const NUNetRefHdl& dstNetRef = *n;
        FLNode* unelabFlow;
        unelabFlow = createUnelaboratedData(flows, always, stmt, dstNetRef);
        
        // Create the elaborated data.
        // Add the new elab flow to the old elab flow.  Delete the old elab
        // flow later.
        NUNet* dstNet = dstNetRef->getNet();
        createElaboratedData(flows, always, dstNet, unelabFlow, newFlows);
      }
    }
  } 
  
  // Create names for all inserted buffers
  typedef UtMap<FLNodeElab*, UtString> BufferNames;
  BufferNames bufferNames;
  for (NewFlows::iterator i = newFlows->begin(); i != newFlows->end(); ++i) {
    FLNodeElab* flowElab = *i;
    UtString& name = bufferNames[flowElab];
    flowElab->compose(&name, flowElab->getHier(), 0, false);
  }
  
  // Repair any unelaborated and nested flow
  repairFlow(design, deletedElabFlows, clockAndDriver, stats);

  // The above repair pass might have deleted new flows. clean those
  // up We no longer assert about these because we now pre-assert if
  // any of the buffer insertions did not work correctly. We could
  // additionally assert here but that means the customer has to
  // change two asserts to get around this problem.
  NewFlows cleanedNewFlows;
  bool buffersDeleted = false;
  for (NewFlows::iterator i = newFlows->begin(); i != newFlows->end(); ++i) {
    FLNodeElab* flowElab = *i;
    if (deletedElabFlows->find(flowElab) == deletedElabFlows->end()) {
      cleanedNewFlows.insert(flowElab);
    } else {
      buffersDeleted = true;
    }
  }
  if (buffersDeleted) {
    newFlows->swap(cleanedNewFlows);
  }

  // Done with the memory, delete it
  for (BufferAndFlowLoop l(*mBufferAndFlow); !l.atEnd(); ++l) {
    FLNodeElabSet* outFlows = l.getValue();
    delete outFlows;

  }
  mBufferAndFlow->clear();
  for (ModNetDelayBlocksLoop l = loopModNetDelayBlocks(); !l.atEnd(); ++l)
  {
    NetDelayBlocks* netDelayBlocks = l.getValue();
    delete netDelayBlocks;
  }
  mModNetDelayBlocks->clear();
  for (BufferedInputsLoop l(*mBufferedInputs); !l.atEnd(); ++l) {
    NUNetRefSet* inNetRefs = l.getValue();
    delete inNetRefs;
  }
  mBufferedInputs->clear();
} // RETransform::fixupBufferFlow

void RETransform::elaborateTempNets(ModuleHierarchies& moduleHierarchies,
                                    NUNetElabSet* newElabNets)
{
  for (TempNetsLoop l = loopTempNets(); !l.atEnd(); ++l) {
    NUScope* scope = l.getKey();
    NUNetVector* nets = l.getValue();
    elaborateNets(scope, nets, moduleHierarchies, newElabNets);
  }
  clearTempNets();
}

FLNode*
RETransform::createUnelaboratedData(FLNodeElabSet* outFlows,
                                    NUAlwaysBlock* always,
                                    NUStmt* stmt,
                                    const NUNetRefHdl& dstNetRef)
{
  // Create the new flow nodes
  FLNode* unelabFlow = mFlowFactory->create(dstNetRef, always);
  NUBlock* block = always->getBlock();
  FLNode* nestedFlow = mFlowFactory->create(dstNetRef, block);
  unelabFlow->addNestedBlock(nestedFlow);
  FLNode* stmtFlow = mFlowFactory->create(dstNetRef, stmt);
  nestedFlow->addNestedBlock(stmtFlow);

  // Update the nets drivers
  NUNet* dstNet = dstNetRef->getNet();
  dstNet->addContinuousDriver(unelabFlow);

  // Fix up the fanin for this flow node
  NUNetSet flowInNets;
  always->getUses(dstNet, &flowInNets);
  NU_ASSERT2(flowInNets.size() == 1,always,dstNet);
  NUNet* srcNet = *flowInNets.begin();
  for (NUNet::DriverLoop d = srcNet->loopContinuousDrivers(); !d.atEnd(); ++d)
  {
    // Connect the external flow
    FLNode* fanin = *d;
    unelabFlow->connectFanin(fanin);

    // Connect the nested flow
    nestedFlow->connectFanin(fanin);
    stmtFlow->connectFanin(fanin);
  }

  // Gather the output flow nodes that need their fanin fixed up.
  FLNodeSet nodeSet;
  for (FLNodeElabSetIter f = outFlows->begin(); f != outFlows->end(); ++f)
  {
    FLNodeElab* flow = *f;
    nodeSet.insert(flow->getFLNode());
  }

  // Now fix up the fanin for the output unelaborated flows
  for (FLNodeSetIter f = nodeSet.begin(); f != nodeSet.end(); ++f)
  {
    FLNode* outFlow = *f;
    while (outFlow != NULL)
    {
      // We do not remove the original fanin because there may still
      // be remaining fanin.
      outFlow->connectFanin(unelabFlow);
      if (outFlow->hasNestedBlock())
      {
        outFlow = outFlow->getSingleNested();
      }
      else
        outFlow = NULL;
    }
  }
  return unelabFlow;
}

void
RETransform::createElaboratedData(FLNodeElabSet* outFlows,
				  NUAlwaysBlock* ,
				  NUNet* dstNet, FLNode* unelabFlow,
				  NewFlows* newFlows)
{
  // Create the elaborated net and flow node
  typedef UtMap<STBranchNode*, FLNodeElab*> FlowCache;
  FlowCache flowCache;
  for (FLNodeElabSetIter f = outFlows->begin(); f != outFlows->end(); ++f)
  {
    // Get the hierarchy we need to create it in. Make sure this
    // output flow reads the delayed net.
    //
    // Note, normally you would expect to only have to delay level
    // sensitive input nets. But with -enableSyncReset code the sync
    // resets are really a level fanin but they are in the edge fanin
    // set. This is probably ok because this couldn't happen to a real
    // edge fanin. See test/synreset/flopped.v for an example.
    FLNodeElab* outFlow = *f;
    FLNode* flnode = outFlow->getFLNode();
    if (flnode->isFanin(unelabFlow, eSensLevelOrEdge)) {
      // Create the new elaborated flow node if we haven't before
      STBranchNode* hier = outFlow->getHier();
      FLNodeElab* newFlow;
      FlowCache::iterator pos = flowCache.find(hier);
      if (pos == flowCache.end())
      {
        // Create the elaborated flow node
        NUNetElab* netElab = dstNet->lookupElab(hier);
        newFlow = mFlowElabFactory->create(unelabFlow, netElab, hier);

        // Create the nested elaborated flow
        FLNode* nestedFlow = unelabFlow;
        FLNodeElab* parentElabFlow = newFlow;
        while (nestedFlow->hasNestedBlock())
        {
          nestedFlow = nestedFlow->getSingleNested();
          FLNodeElab* nestedElabFlow;
          nestedElabFlow = mFlowElabFactory->create(nestedFlow, netElab, hier);
          parentElabFlow->addNestedBlock(nestedElabFlow);
          parentElabFlow = nestedElabFlow;
        }

        // Set the nets drivers
        netElab->addContinuousDriver(newFlow);

        // Add this flow node to our cache
        flowCache[hier] = newFlow;
      }
      else
        // The elaborated data already exists
        newFlow = pos->second;

      // Add the new flow to our callers set
      newFlows->insert(newFlow);
    } // if
  } // for
} // RETransform::createElaboratedData


void RETransform::updateFlowMaps(FLNodeElabVectors & nodeVectors)
{
  for (FLNodeElabVectorsIter n = nodeVectors.begin(); n != nodeVectors.end(); ++n) {
    FLNodeElabVector& fv = (*n);
    for (FLNodeElabVector::iterator p = fv.begin(); p != fv.end(); ++p) {
      FLNodeElab * flow_elab = (*p);
      FLNode * flow = flow_elab->getFLNode();
      mMergedFlow.insert(flow);
      mMergedElabToFlow[flow].insert(flow_elab);
    }
  }
}


void
RETransform::mergeFlows(FLNodeElabVectors& nodeVectors)
{
  // Remember these flow for cleanup.
  updateFlowMaps(nodeVectors);

  // Grab the first flow node from each vector to get the use def. The
  // vectors are in order of how they should be merged.  information.
  NUUseDefVector useDefVector;
  UtSet<NUUseDefNode*> useDefSet;
  for (FLNodeElabVectorsIter n = nodeVectors.begin(); n != nodeVectors.end(); ++n)
  {
    // Get the use def for this vector
    FLNodeElabVector& fv = *n;
    FLNodeElab* flow = *(fv.begin());
    NUUseDefNode* useDef = flow->getUseDefNode();
    FLN_ELAB_ASSERT(useDef != NULL,flow);

    // Insert it into the vector. We use the set to make sure we don't
    // insert a usedef in more than once.
    NU_ASSERT(useDefSet.find(useDef) == useDefSet.end(), useDef);
    useDefSet.insert(useDef);
    useDefVector.push_back(useDef);
  }

  // Create a new use def node which is merged between two of them
  NUUseDefNode* newUD = mergeUseDefNodes( useDefVector );
  
  // Update the pointers for all the flow nodes
  for (FLNodeElabVectorsIter n = nodeVectors.begin(); n != nodeVectors.end(); ++n)
  {
    FLNodeElabVector& fv = *n;
    FLNodeElabVector::iterator p;
    const NUUseDefNode* oldUseDef = NULL;
    for (p = fv.begin(); p != fv.end(); ++p)
    {
      // Make sure all the flow nodes in a vector used to have the
      // same use def node
      FLNodeElab* flow = *p;
      NU_ASSERT2(((oldUseDef == NULL) || (flow->getUseDefNode() == oldUseDef)), oldUseDef, flow->getUseDefNode());

      // Update the use def node to the new block
      flow->getFLNode()->setUseDefNode(newUD);
    }
  }

  // Update the fanin of the nodes. Any flow to an internal node
  // should be modified to flow to the outer node. We start with the
  // first vector which doesn't need any updating. We then traverse
  // the other vectors looking for the found nodes in the fanin.
  FLNodeElabSet blockNodes;
  for (FLNodeElabVectorsIter n = nodeVectors.begin(); n != nodeVectors.end(); ++n)
  {
    // Update the fanin for this set of flow nodes if we have some
    // internal flow nodes in our set.
    FLNodeElabVector& fv = *n;
    if (!blockNodes.empty())
    {
      // Must not be the first time through
      for (FLNodeElabVectorIter p = fv.begin(); p != fv.end(); ++p)
      {
        // Find the set of fanin that is internal
        FLNodeElab* flow = *p;
        FLNodeElabSet faninFixupSet;
        for (Fanin f(NULL, flow, eFlowIntoCycles); !f.atEnd(); ++f)
        {
          FLNodeElab* fanin = *f;
          if (blockNodes.find(fanin) != blockNodes.end())
            faninFixupSet.insert(fanin);
        }
        
        // Fixup the fanin
        FLNodeElabSetIter i;
        for (i = faninFixupSet.begin(); i != faninFixupSet.end(); ++i)
        {
          FLNodeElab* fanin = *i;
          flow->replaceFaninWithFanin(fanin);
        }
      }
    } // if
    
    // Update the set of flow nodes in this block
    for (FLNodeElabVectorIter p = fv.begin(); p != fv.end(); ++p)
    {
      FLNodeElab* flow = *p;
      blockNodes.insert(flow);
    }
  } // for
}

void
RETransform::addBufferBlock(NUUseDefNode* useDef, NUNetRefSet& netRefSet,
                            NUAlwaysBlockVector* alwaysBlocks,
                            bool replaceEdges)
{
  // Get the location for the new blocks created
  const SourceLocator& loc = useDef->getLoc();

  // Get the parent module to put these blocks into
  NUModule* module = const_cast<NUModule*>(useDef->findParentModule());

  // Get the set of defs for this always block
  NUNetSet defNets;
  useDef->getDefs(&defNets);

  // Get the always block, we shouldn't be adding delays to anything
  // else.
  NU_ASSERT(useDef->getType() == eNUAlwaysBlock, useDef);
  NUAlwaysBlock* origAlways = dynamic_cast<NUAlwaysBlock*>(useDef);

  // Rewrite any tasks that write to module nets. In the future we
  // should only rewrite these if the task reads/writes the buffered
  // net. We don't do this if the -multiLevelHierTasks switch is on
  // because the UD information is not valid in the tasks. This
  // means inlining a task could make an elaboratedly dead net live.
  if (!mArgs->getBoolValue(CRYPT("-multiLevelHierTasks"))) {
    TFRewrite tf_rewrite(mNetRefFactory,
                         mMsgContext,
                         mStrCache,
                         false,
                         false,
                         false,
                         mIODB,
                         mArgs);
    tf_rewrite.alwaysBlock(origAlways);
  }

  // Go through the list of nets, creating statements
  NUStmtList stmtList;
  typedef UtMap<NUNetRefHdl, NUNet*> OldToNewNet;
  OldToNewNet oldToNewNet;
  for (NUNetRefSet::SortedLoop n = netRefSet.loopSorted(); !n.atEnd(); ++n) {
    // Check if we have already delayed this net, if not create the
    // delays, otherwise, use the existing delayed net.
    const NUNetRefHdl& netRef = *n;
    BlockNetPair blockNetPair = findNetDelayBlock(module, netRef);
    NUAlwaysBlock* always = blockNetPair.first;
    NUNet* newNet;
    if (always == NULL) {
      // Create the delayed net for the assigns
      newNet = createTempNet(module, netRef->getNet(), "cds_delay");
      oldToNewNet.insert(OldToNewNet::value_type(netRef, newNet));

      // Create assignments for this delayed net.
      createDelays(newNet, netRef, loc, &stmtList);
    } else {
      // Already exists
      alwaysBlocks->push_back(always);
      newNet = blockNetPair.second;
    }
    
    // Modify the original block to use the new net
    replaceInputNet(module, origAlways, netRef, newNet, replaceEdges);
  }

  // Create a new always block to contain the statements if we have
  // new assigns.
  if (!stmtList.empty()) {
    // Create the always block and return it to our caller
    NUAlwaysBlock* newAlways = createAlways(module, stmtList, loc);
    alwaysBlocks->push_back(newAlways);
    mRepairBlocks->insert(newAlways);

    // Remember the always for each net ref where we created an always
    // block.
    typedef LoopMap<OldToNewNet> OldToNewNetLoop;
    for (OldToNewNetLoop l(oldToNewNet); !l.atEnd(); ++l) {
      const NUNetRefHdl& netRef = l.getKey();
      NUNet* newNet = l.getValue();
      addNetDelayBlock(module, netRef, newAlways, newNet);
    }
  }

  // Remember the buffered inputs to verify that we handled them
  // correctly
  BufferedInputs::iterator pos = mBufferedInputs->find(origAlways);
  NUNetRefSet* inNetsCopy = NULL;
  if (pos == mBufferedInputs->end()) {
    inNetsCopy = new NUNetRefSet(mNetRefFactory);
    mBufferedInputs->insert(BufferedInputsValue(origAlways, inNetsCopy));
  } else {
    inNetsCopy = pos->second;
  }
  inNetsCopy->insert(netRefSet.begin(), netRefSet.end());
} // RETransform::addBufferBlock

void 
RETransform::createDelays(NUNet* dstNet, const NUNetRefHdl& srcNetRef,
                          const SourceLocator& loc, NUStmtList* stmtList)
{
  // Create lvalues for this net ref to create an always block that
  // defines all the delay bits.
  typedef UtList<NULvalue*> Lvalues;
  Lvalues lvalues;
  srcNetRef->createLvalues(mNetRefFactory, loc, &lvalues);
  NUNet* srcNet = srcNetRef->getNet();

  // Loop over the lvalues and possibly create always blocks for every
  // one.
  for (Loop<Lvalues> l(lvalues); !l.atEnd(); ++l) {
    // Create the assign statement and add it to the end of the
    // statement list.
    NULvalue* lvalue = *l;
    NUExpr* rvalue = lvalue->NURvalue();
    lvalue->replaceDef(srcNet, dstNet);
    createAssign(lvalue, rvalue, loc, stmtList);
  }
} // RETransform::createDelays


//! class REUpdateTempNetCallback
/*! This class rewrites statements in an always block that def a net
 *  that is going to be buffered. This is because any local uses of
 *  that net should used the delayed portions for any words/bits
 *  defined outside the block and use the new value for any portions
 *  defined inside the block.
 *
 *  This is accomplished by converting a statement that looks as
 *  follows:
 *
 *  \verbatim
 *    net[<range>] = <expr>;
 *
 *  to the following set of statements:
 *
 *    begin
 *      reg [size(<expr>)-1:0] $dlylcl_net;
 *      $dlylcl_net = <expr>;
 *      net[<range.] = $dlylcl_net;
 *      $dlytmp_net = $dlylcl_net;
 *    end
 *
 *  \endverbatim
 *
 *  Where $dlytmp_net has been initialized to be a copy of net at the
 *  begining of the always block. Since all uses of net will be
 *  converted to using dlytmp_net, it will get the new/delayed value
 *  of net as appropriate.
 *
 *  Note that we additionally create a temporary net for the
 *  expression of the original assignment. This is because dlytmp_net
 *  should not use net. It weakens the test for valid transformations
 *  done by verifyBufferedNets().
 *
 *  Also note that this is only necessary if \<range\> contains dynamic
 *  portions. If it were a constant bit/part select this would not be
 *  a problem. For an example look at test/bugs/bug3120/bug5.v
 */
class REUpdateTempNetCallback : public NUDesignCallback
{
public:
  //! constructor
  REUpdateTempNetCallback(NUModule* module, NUNet* net, NUNet* tempNet,
                          IODBNucleus* iodb, NUNetRefFactory* netRefFactory) :
    mNet(net), mTempNet(tempNet), mIODB(iodb), mNetRefFactory(netRefFactory)
  {
    mScopes.push(module);
    mNetRef = netRefFactory->createNetRef(net);
  }

  //! destructor
  ~REUpdateTempNetCallback() {}

  //! Catch all
  Status operator()(Phase, NUBase*)
  {
    return eNormal;
  }

  //! Catch all tasks, we should not modify them
  Status operator()(Phase, NUTask*)
  {
    return eSkip;
  }

  //! Catch all the changes in block scope
  Status operator()(Phase phase, NUBlock* block)
  {
    if (phase == ePre) {
      // Update our current scope
      mScopes.push(block);
    } else {
      // Out of the scope, remove it
      NU_ASSERT(block == mScopes.top(), block);
      mScopes.pop();
    }
    return eNormal;
  }

  //! Catch all statements that might assign the net
  Status operator()(Phase phase, NUStmt* stmt);

  //! Function to return the replacement
  NUStmt* replacement(NUStmt *orig)
  {
    UtMap<NUStmt*,NUStmt*>::iterator pos = mStmtReplacements.find(orig);
    if (pos != mStmtReplacements.end()) {
      NUStmt *stmt = pos->second;

      // Erase this map element just in case someone else gets the
      // memory used by 'orig' on a subsequent allocation.
      mStmtReplacements.erase(pos);

      return stmt;
    } else {
      return NULL;
    }
  }

  //! Abstraction for scope/net pair
  typedef std::pair<NUScope*, NUNet*> ScopeNetPair;

  //! Temp nets created by this process
  typedef UtSet<ScopeNetPair> NewTempNets;

  //! Iterator over new temp nets (with their scope)
  typedef Loop<NewTempNets> NewTempNetsLoop;

  //! Return an iterator over the new temp nets
  NewTempNetsLoop loopTempNets() { return NewTempNetsLoop(mNewTemptNets); }

private:
  //! Helper function to process leaf statements
  /*! If this statement def's the net, it will be replaced with a
   *  block that assigns both the temp net and the net.
   */
  Status handleStmt(NUStmt* stmt);

  // The information used to start this process
  NUNet* mNet;
  NUNet* mTempNet;
  NUNetRefHdl mNetRef;

  // A stack of scopes so we create the NUBlock in the right scope
  NUScopeStack mScopes;

  //! Keep track of new temp nets created
  NewTempNets mNewTemptNets;

  // Helper classes
  IODBNucleus* mIODB;
  NUNetRefFactory* mNetRefFactory;

  // Space to store replacements as we add blocks of statements to
  // replace single blocking assigns.
  UtMap<NUStmt*,NUStmt*> mStmtReplacements;
}; // class REUpdateTempNetCallback : public NUDesignCallback

NUDesignCallback::Status
REUpdateTempNetCallback::operator()(Phase phase, NUStmt* stmt)
{
  // Only work in the post phase. 
  if (phase == ePre) {
    return eNormal;
  }

  // We can have two types of statements. One is a control statement
  // like case, if etc. In that case we just recurse to lower
  // statements. The other are statements that can define nets. In
  // those cases we have to process the statement and replace any defs
  // we care about.
  //
  // If a new statement type is added, we have to add to this case
  // statement.
  NUDesignCallback::Status status = NUDesignCallback::eNormal;
  switch(stmt->getType()) {
    case eNUBlock:
    case eNUCase:
    case eNUIf:
    case eNUFor:
      // These are just control statements and can be ignored
      break;

    case eNUBlockingAssign:
    case eNUCModelCall:
    case eNUControlSysTask:
    case eNUInputSysTask:
    case eNUOutputSysTask:
    case eNUFOpenSysTask:
    case eNUFCloseSysTask:
    case eNUFFlushSysTask:
    case eNUTaskEnable:
    case eNUReadmemX:
    case eNUSysRandom:
    {
      // Handle defs in these statements
      status = handleStmt(stmt);
      break;
    }

    case eNUAlwaysBlock:
    case eNUAssign:
    case eNUVarselLvalue:
    case eNUBreak:
    case eNUCModel:
    case eNUCModelArgConnectionInput:
    case eNUCModelArgConnectionOutput:
    case eNUCModelArgConnectionBid:
    case eNUConcatLvalue:
    case eNUCycle:
    case eNUEnabledDriver:
    case eNUIdentLvalue:
    case eNUInitialBlock:
    case eNULvalue:
    case eNUMemselLvalue:
    case eNUModule:
    case eNUModuleInstance:
    case eNUNamedDeclarationScope:
    case eNUNonBlockingAssign:
    case eNUPortConnectionBid:
    case eNUPortConnectionInput:
    case eNUPortConnectionOutput:
    case eNUStructuredProc:
    case eNUTF:
    case eNUTFArgConnectionBid:
    case eNUTFArgConnectionInput:
    case eNUTFArgConnectionOutput:
    case eNUTFElab:
    case eNUTask:
    case eNUPartselIndexLvalue:
    case eNUCompositeLvalue:
    case eNUCompositeIdentLvalue:
    case eNUCompositeSelLvalue:
    case eNUCompositeFieldLvalue:
    {
      // Should not get these. Some because they aren't statements and
      // others because they are intermediate classes that should not
      // exist in our structures (eg. NUAssign).
      bool unexpectedStmtType = true;
      NU_ASSERT(!unexpectedStmtType, stmt);
      break;
    }

    case eNUBlockingEnabledDriver:
    {
      // We shouldn't be getting enabled drivers in flops which is
      // where we add buffers. If that changes, we have to support
      // this code. Not sure how though because we need z propagation.
      bool unexpectedEnabledDriver = true;
      NU_ASSERT(!unexpectedEnabledDriver, stmt);
      break;
    }
      
    case eNUContAssign:
    case eNUContEnabledDriver:
    case eNUTriRegInit:
    {
      // We don't expect these because we don't add buffers to continous assigns.
      bool unexpectedContAssign = true;
      NU_ASSERT(!unexpectedContAssign, stmt);
      break;
    }

  } // switch

  return status;
} // REUpdateTempNetCallback::operator

NUDesignCallback::Status
REUpdateTempNetCallback::handleStmt(NUStmt* stmt)
{
  // Make sure we haven't worked on this statement yet.
  NU_ASSERT(mStmtReplacements.find(stmt) == mStmtReplacements.end(), stmt);

  // Check if this assign defs the net we care about
  bool defsNet = stmt->queryBlockingDefs(mNetRef, &NUNetRef::overlapsSameNet,
                                         mNetRefFactory);
  if (defsNet) {
    // Gather the lvalues that this statement defines. This must be
    // done first because, we may have a task that defines this net
    // with -multiLevelHierTasks thrown. If that is the case, we will
    // get 0 defs. This means we should not do this transformation.
    NULvalueVector lvalues;
    NUGatherLvaluesCallback callback(&lvalues, mNetRef, mNetRefFactory);
    NUDesignWalker walker(callback, false);
    walker.putWalkTasksFromTaskEnables(false);
    walker.stmt(stmt);
    if (!lvalues.empty()) {
      // Make a new block to store the multiple statements and local
      // declaration
      const SourceLocator& loc = stmt->getLoc();
      NUScope* scope = mScopes.top();
      NUBlock* block = new NUBlock(NULL, scope, mIODB, mNetRefFactory, false, loc);

      // Create a temp to store the rvalue for this assign
      NUNet* net = mNetRef->getNet();
      NUNet* tmpNet = block->createTempNetFromImage(net, "dlylcl", true);
      mNewTemptNets.insert(ScopeNetPair(block, tmpNet));

      // Make a copy of the statement and replace the def net with the
      // temp net
      CopyContext context(block,0);
      NUStmt* newStmt = stmt->copy(context);
      newStmt->replaceDef(net, tmpNet);
      block->addStmt(newStmt);

      // Copy the result to the original def net and the temp net so
      // that it is up to date.
      NUStmtList stmtLst;
      for (NULvalueLoop l(lvalues); !l.atEnd(); ++l) {
        // First the original net
        NULvalue* lvalue = *l;
        NULvalue* newLvalue = lvalue->copy(context);
        NUExpr* rvalue = newLvalue->NURvalue();
        rvalue->replace(net, tmpNet);
        RETransform::createAssign(newLvalue, rvalue, loc, &stmtLst);

        // Now the temp net
        newLvalue = lvalue->copy(context);
        newLvalue->replaceDef(net, mTempNet);
        rvalue = rvalue->copy(context);
        RETransform::createAssign(newLvalue, rvalue, loc, &stmtLst);
      }
      
      // Add the statements to new block which is put in our replacement
      // map. This will replace the existing statement with this block.
      for (NUStmtLoop l(stmtLst); !l.atEnd(); ++l) {
        NUStmt* stmt = *l;
        block->addStmt(stmt);
      }
      mStmtReplacements[stmt] = block;
      return eDelete;
    } // if
  } // if

  return eNormal;
} // REUpdateTempNetCallback::handleBlockingAssign

void
RETransform::replaceInputNet(NUModule* module, NUAlwaysBlock* always,
                             const NUNetRefHdl& netRef, NUNet* newNet,
                             bool replaceEdges)
{
  // Check if any part of the to-be-buffered net is def'ed. If so, we
  // do the more complex buffer insertion.
  NUNetSet defs;
  always->getDefs(&defs);
  NUNet* net = netRef->getNet();
  if (defs.find(net) != defs.end()) {
    // Need to fix up the define. We do the following:
    //
    // 1. Create a temp for the net and assign it from the new net
    //    passed into this function (but only the bits/words that are
    //    used in the block so we don't create new live data).
    //
    // 2. Replace all uses of the net with the temp net (must be done
    //    before we update the temp net because it creates a valid use
    //    of the net).
    //
    // 3. Updated the temp net whenever the net is written.
    //
    // 4. Record any temp nets created by the above process for fixups
    //
    // 5. Update the block so that the first statement is the initial
    //    temp net assign created in step 1.

    // 1a. Create the temp net 
    NUBlock* block = always->getBlock();
    NUNet* tempNet = createTempNet(block, net, "dlytmp");

    // 1b. Figure out the uses of the input net. We only want to
    // initialize those. We get the uses from the block and them
    // create the net ref image in terms of the new (delayed) net.
    NUNetRefSet uses(mNetRefFactory);
    block->getBlockingUses(netRef, &uses);
    NUNetRefHdl usesNetRef = uses.findNet(net);
    NUNetRefHdl newNetRef = mNetRefFactory->createNetRefImage(newNet, netRef);

    // 1c. Create the initializing assigns. Insert these statements
    // before step 3 because they must be preassigned.
    NUStmtList stmts;
    const SourceLocator& loc = always->getLoc();
    createDelays(tempNet, newNetRef, loc, &stmts);

    // 2. Replace all uses of the net with the tempNet
    replaceNetUses(always, net, tempNet, replaceEdges);

    // 3. Updated the temp net whenever the net is written.
    REUpdateTempNetCallback callback(module, net, tempNet,
                                     mIODB, mNetRefFactory);
    NUDesignWalker walker(callback, false, false);
    walker.alwaysBlock(always);

    // 4. Record any temp nets created by REUpdateTempNetCallback
    for (REUpdateTempNetCallback::NewTempNetsLoop l = callback.loopTempNets();
         !l.atEnd(); ++l) {
      const REUpdateTempNetCallback::ScopeNetPair& pair = *l;
      addTempNet(pair.first, pair.second);
    }

    // 5. Get the statements for the block. They go after the temp
    // variable assign. We will replace the block statements at the
    // end.
    for (NUStmtLoop l = block->loopStmts(); !l.atEnd(); ++l) {
      NUStmt* stmt = *l;
      stmts.push_back(stmt);
    }
    block->replaceStmtList(stmts);
  } else {
    // Replace the uses of the net there are no defs
    NUNet* net = netRef->getNet();
    replaceNetUses(always, net, newNet, replaceEdges);
  }

  // This always block needs its flow repaired
  mRepairBlocks->insert(always);
}

void
RETransform::replaceNetUses(NUAlwaysBlock* always, NUNet* net, NUNet* newNet,
                            bool replaceEdges)
{
  // Replace the uses of the net
  NUNetReplacementMap netReplacements;
  NULvalueReplacementMap dummyLvalueReplacements;
  NUExprReplacementMap dummyRvalueReplacements;
  NUTFReplacementMap dummyTFReplacements;
  NUCModelReplacementMap dummyCmodelReplacements;
  NUAlwaysBlockReplacementMap dummyAlwaysReplacements;
  netReplacements.insert(NUNetReplacementMap::value_type(net, newNet));
  REReplaceRvalues translator(netReplacements, dummyLvalueReplacements,
                              dummyRvalueReplacements, dummyTFReplacements,
                              dummyCmodelReplacements,
                              dummyAlwaysReplacements);
  if (replaceEdges) {
    always->replaceLeaves(translator);
  } else {
    always->getBlock()->replaceLeaves(translator);
  }

  // If the new net is a memory net, it could be used in a temp memory
  // nets constructor. Visit the local nets for this.
  if (newNet->is2DAnything()) {
    // We need the net as a memory
    NUMemoryNet* memNet = dynamic_cast<NUMemoryNet*>(newNet);
    NU_ASSERT(memNet != NULL, newNet);

    // Grab all the nets in this block. We don't need the clock since
    // it can't be a memory so we don't have to ask for the nets of
    // the always block.
    NUNetSet covered;
    NUNetList nets;
    always->getBlock()->getAllNets(&nets);
    for (NUNetCLoop l(nets); !l.atEnd(); ++l) {
      NUNet* blockNet = *l;
      if (blockNet->is2DAnything() && (covered.find(blockNet) == covered.end())) {
        covered.insert(blockNet);
        NUTempMemoryNet* tempNet = dynamic_cast<NUTempMemoryNet*>(blockNet);
        if ((tempNet != NULL) && (tempNet->getMaster() == net)) {
          tempNet->putMaster(memNet);
        }
      }
    }
  } // if
} // RETransform::replaceNetUses

NUNet*
RETransform::createTempNet(NUScope* scope, NUNet* net, const char* prefix)
{
  // Gather information about the input net
  StringAtom* newName = scope->gensym(prefix, NULL, net);

  // Create the new net
  NUNet* newNet = NULL;
  const SourceLocator& netLoc = net->getLoc();
  if (net->isMemoryNet()) {
    NUMemoryNet* mem = net->getMemoryNet();
    newNet = scope->createTempMemoryNetFromImage(newName, mem, mem->getFlags ());
  } else if (net->getBitSize() == 1) {
    newNet = scope->createTempBitNet(newName, net->isSigned(), netLoc);
  } else {
    NUVectorNet* vecNet = dynamic_cast<NUVectorNet*>(net);
    const ConstantRange* range = vecNet->getRange();
    newNet = scope->createTempVectorNet(newName, *range, vecNet->isSigned (), netLoc, NetFlags (0),
                                        vecNet->getVectorNetFlags ());
  }

  // Remember it for elaboration
  addTempNet(scope, newNet);
  return newNet;
}

void
RETransform::moveStmts(NUBlock* block, NUUseDefNode* ud)
{
  // Try a structure proc use def
  NUStructuredProc* sp = dynamic_cast<NUStructuredProc*>(ud);
  if (sp != NULL) {
    // Get the block and move its contents over. This includes local
    // register declarations and nested statements.
    NUBlock* thisBlock = sp->getBlock();

    // Just move all the contents over
    thisBlock->moveStmts(block);
    // clear the use-def information on the block.
    thisBlock->clearUseDef();

    // clear the use-def information on the structured proc -- this
    // allows for net deletion if things become dead.
    sp->clearUseDef();

  } else if (ud->getType() == eNUContAssign) {
    // It is a continous assign, convert it to an assign statement
    const NUContAssign* ca = dynamic_cast<const NUContAssign*>(ud);
    NU_ASSERT(ca,ud);
    block->addStmt(ca->copyBlocking());

  } else if (ud->getType() == eNUContEnabledDriver) {
    // It is a continous enabled driver, convert it to an enabled
    // driver statement.
    const NUContEnabledDriver* ced;
    ced = dynamic_cast<const NUContEnabledDriver*>(ud);
    NU_ASSERT(ced,ud);
    block->addStmt(ced->copyBlocking());

  } else {
    NU_ASSERT("Can only merge structured procs, cont enabled drivers and cont assigns" == NULL,ud);
  }
} // RETransform::moveStmts

NUUseDefNode*
RETransform::mergeUseDefNodes(NUUseDefVector& useDefVector)
{
  // get the module for the use def nodes and make sure they are all
  // the same.
  NUModule* module = NULL;
  NUUseDefNode* firstUseDef = NULL;
  for (NUUseDefVectorIter u = useDefVector.begin(); u != useDefVector.end();
       ++u)
  {
    NUUseDefNode* useDef = *u;
    if (module == NULL)
    {
      firstUseDef = useDef;
      module = const_cast<NUModule*>(useDef->findParentModule());
      NU_ASSERT(module != NULL,useDef);
    }
    else
      NU_ASSERT2(module == useDef->findParentModule(), module, useDef->findParentModule());
  }

  // Use the first block's location for the new block
  const SourceLocator& loc = firstUseDef->getLoc();

  // Create the new block
  NUBlock* newBlock = new NUBlock(0, module, 
                                  mIODB, mNetRefFactory, 
                                  false, loc);

  // Make a name for this new block
  StringAtom* blockName = module->newBlockName(mStrCache, loc);

  // Create a structure proc around the block and add it to the module
  NUAlwaysBlock* always = new NUAlwaysBlock(blockName, newBlock, mNetRefFactory, loc, false);
  module->addAlwaysBlock(always);

  // Add the statement list from the blocks in order, remember any
  // sequential use defs for fixup below
  NUUseDefNode* seqUseDef = NULL;
  for (NUUseDefVectorIter u = useDefVector.begin(); u != useDefVector.end();
       ++u)
  {
    NUUseDefNode* useDef = *u;
    moveStmts(newBlock, useDef);

    // save old to new.
    mDeletedUseDefs.insert(NUUseDefOldToNew::value_type(useDef,always));

    // We remember one of the sequentials for fixups. If we are
    // merging sequentials together, any of them is fine. But if we
    // are merging combos into sequentials we want to make sure we get
    // the sequential use def for the fixups. Today we merge only
    // combo fanins, but if that changes, it is good to have this code
    // be flexible.
    if (useDef->isSequential()) {
      seqUseDef = useDef;
    }
  }

  // Reparent any contained blocks.
  newBlock->adoptChildren();

  // Add the edge expressions, and clock/prio pointers if this is
  // sequential
  if (seqUseDef != NULL) {    
    // Copy the edge list from one of the merged blocks into the new block.
    // Doesn't matter which one since the canonical clocks are marked as
    // observable.
    CopyContext cc(0,0);
    NUAlwaysBlock * old_always = dynamic_cast<NUAlwaysBlock*>(seqUseDef);
    NU_ASSERT( old_always, seqUseDef );
    for (NUAlwaysBlock::EdgeExprLoop e = old_always->loopEdgeExprs();
         not e.atEnd(); ++e) {
      NUEdgeExpr* edgeExpr = *e;
      NUEdgeExpr* newEdgeExpr = edgeExpr->copyEdgeExpr(cc);
      always->addEdgeExpr(newEdgeExpr);
    }

    // Copy the clock and priority block pointers. They may point to
    // blocks that will go away. That is ok, we fix them up before
    // deleting the old (merged) blocks.
    NUAlwaysBlock* clockBlock = old_always->getClockBlock();
    always->setClockBlock(clockBlock);
    NUAlwaysBlock* priorityBlock = old_always->getPriorityBlock();
    always->setPriorityBlock(priorityBlock);
  }

  // Remember that we created these blocks.
  mCreatedUseDefs.insert(always);

  // Return the new block
  return always;
}

void
RETransform::elaborateNets(NUScope* scope, NUNetVector* newNets,
                           ModuleHierarchies& moduleHierarchies,
                           NUNetElabSet* newElabNets)
{
  // Find all elaborations of module
  NUModule* module = scope->getModule();
  ModuleHierarchies::iterator pos = moduleHierarchies.find(module);
  NU_ASSERT(pos != moduleHierarchies.end(), module);
  InstanceSet& instances = pos->second;

  // Elaborate all the nets in all the hierarchies
  for (NUNetVectorLoop l(*newNets); !l.atEnd(); ++l) {
    NUNet* net = *l;
    for (InstanceSetIter i = instances.begin(); i != instances.end(); ++i) {
      STBranchNode* node = *i;
      net->createElab(node, mSymtab);
      if (newElabNets != NULL) {
        NUNetElab* netElab = net->lookupElab(node);
        newElabNets->insert(netElab);
      }
    }
  }
}

void RETransform::cleanUpMergedFlows()
{
  UD ud (mNetRefFactory, mMsgContext, mIODB, mArgs, false);

  // Create a map from modules to a set of deleted usedefs
  typedef UtMap<NUModule*, NUUseDefVector> ModuleDeletedUseDefs;
  ModuleDeletedUseDefs moduleDeletedUseDefs;
  for (NUUseDefOldToNew::iterator iter = mDeletedUseDefs.begin(); 
       iter != mDeletedUseDefs.end(); 
       ++iter) {
    NUUseDefNode* useDef = (*iter).first;
    NUModule* module = const_cast<NUModule*>(useDef->findParentModule());
    moduleDeletedUseDefs[module].push_back(useDef);
  }

  // For each module, create a hash set of the use defs and then go
  // through the lists to delete them from the module. Also update the
  // clock/priority blocks.
  typedef UtHashSet<NUUseDefNode*> DeletedUseDefs;
  DeletedUseDefs deletedUseDefs;
  ModuleDeletedUseDefs::iterator p;
  for (p = moduleDeletedUseDefs.begin(); p != moduleDeletedUseDefs.end(); ++p)
  {
    NUModule * module = p->first;

    // Fill the hash set and check if we found a sequential
    bool sequentialFound = false;
    NUUseDefVector& useDefs = p->second;
    for (NUUseDefVectorIter i = useDefs.begin(); i != useDefs.end(); ++i)
    {
      NUUseDefNode* useDef = *i;
      deletedUseDefs.insert(useDef);
      if (useDef->isSequential()) {
        sequentialFound = true;
        NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
        NU_ASSERT(always != NULL, useDef);
        always->setPriorityBlock(NULL);
        always->setClockBlock(NULL);
      }
    }

    // Go through the module const assign lists to delete them
    NUContAssignList contAssigns;
    NUContAssignList newContAssigns;
    module->getContAssigns(&contAssigns);
    NUContAssignList::iterator c;
    for (c = contAssigns.begin(); c != contAssigns.end(); ++c)
    {
      NUContAssign* assign = *c;
      if (deletedUseDefs.find(assign) == deletedUseDefs.end())
	newContAssigns.push_back(assign);
    }
    module->replaceContAssigns(newContAssigns);

    // Go through the list of always blocks and delete them
    NUAlwaysBlockList alwaysBlocks;
    NUAlwaysBlockList newAlwaysBlocks;
    UtHashSet<NUBlock*> deletedBlocks;
    module->getAlwaysBlocks(&alwaysBlocks);
    NUAlwaysBlockList::iterator a;
    for (a = alwaysBlocks.begin(); a != alwaysBlocks.end(); ++a)
    {
      NUAlwaysBlock* always = *a;
      if (deletedUseDefs.find(always) == deletedUseDefs.end())
	newAlwaysBlocks.push_back(always);
      else
      {
	// Store the block for later deletion
	NUBlock* block = always->getBlock();
	deletedBlocks.insert(block);

	// We may have created and then destroyed an always block.
	mCreatedUseDefs.erase(always);
      }
    }
    module->replaceAlwaysBlocks(newAlwaysBlocks);

    // Go through the list of initial blocks and delete them
    NUInitialBlockList initialBlocks;
    NUInitialBlockList newInitialBlocks;
    module->getInitialBlocks(&initialBlocks);
    for (NUInitialBlockList::iterator iter = initialBlocks.begin();
	 iter != initialBlocks.end();
	 ++iter) {
      NUInitialBlock* initial = (*iter);
      if (deletedUseDefs.find(initial) == deletedUseDefs.end())
	newInitialBlocks.push_back(initial);
      else
      {
	// Store the block for later deletion
	NUBlock* block = initial->getBlock();
	deletedBlocks.insert(block);

	// We may have created and then destroyed an initial block.
	mCreatedUseDefs.erase(initial);
      }
    }
    module->replaceInitialBlocks(newInitialBlocks);

    // Go through the list of enabled drivers and remove deleted ones
    NUEnabledDriverList newContEnabledDrivers;
    NUModule::ContEnabledDriverLoop l;
    for (l = module->loopContEnabledDrivers(); !l.atEnd(); ++l) {
      NUEnabledDriver* contEnabledDriver = *l;
      if (deletedUseDefs.find(contEnabledDriver) == deletedUseDefs.end()) {
        newContEnabledDrivers.push_back(contEnabledDriver);
      }
    }
    module->replaceContEnabledDrivers(newContEnabledDrivers);

    // ACA: IS THIS BLOCK CLEANUP NECESSARY???

    // Go through the block list to remove the deleted ones
    NUBlockList blocks;
    NUBlockList newBlocks;
    module->getBlocks(&blocks);
    NUBlockList::iterator n;
    for (n = blocks.begin(); n != blocks.end(); ++n)
    {
      NUBlock * block = *n;
      if (deletedBlocks.find(block) == deletedBlocks.end())
	newBlocks.push_back(block);
    }
    module->replaceBlocks(newBlocks);

    // Update the clock/priority block pointers
    if (sequentialFound) {
      updateClockPriorityPointers(module);
    }

    // Empty the hash set for the next module
    deletedUseDefs.clear();

    ud.module(module);

  } // for
}

void RETransform::updateClockPriorityPointers(NUModule* module)
{
  // Loop over all the always blocks in this module
  for (NUModule::AlwaysBlockLoop l = module->loopAlwaysBlocks(); !l.atEnd(); ++l) {
    NUAlwaysBlock* always = *l;
    NUAlwaysBlock* clockBlock = findNewBlock(always->getClockBlock());
    if (clockBlock != NULL) {
      always->setClockBlock(clockBlock);
    }
    NUAlwaysBlock* priorityBlock = findNewBlock(always->getPriorityBlock());
    if (priorityBlock != NULL) {
      always->setPriorityBlock(priorityBlock);
    }
  }
}

NUAlwaysBlock* RETransform::findNewBlock(NUAlwaysBlock* oldAlways)
{
  // Merged blocks can be remerged so make sure to iterate until we
  // get the final block.
  NUUseDefNode* useDef = oldAlways;
  NUUseDefNode* prevUseDef = NULL;
  while (useDef != prevUseDef) {
    prevUseDef = useDef;
    NUUseDefOldToNew::iterator pos = mDeletedUseDefs.find(useDef);
    if (pos != mDeletedUseDefs.end()) {
      useDef = pos->second;
    }
  }

  // If we found something, cast it
  if (useDef != oldAlways) {
    NUAlwaysBlock* newAlways = dynamic_cast<NUAlwaysBlock*>(useDef);
    NU_ASSERT(newAlways != NULL, useDef);
    return newAlways;
  } else {
    return NULL;
  }
}

void RETransform::discoverBoundary(FLNodeSet & boundary_flow)
{
  // Loop over the flow factory -- there may be unelab flow which is
  // not reachable through elab flow.
  for ( FLNodeFactory::iterator fliter = mFlowFactory->begin();
	fliter != mFlowFactory->end();
	++fliter ) {
    FLNode * flow = (*fliter);
    NUUseDefNode * useDef = flow->getUseDefNode();

    NUUseDefNode * replacement = useDef;
    NUUseDefOldToNew::iterator iter;

    // This use def may have been replaced a number of times. Find the
    // final replacement.
    while ( (iter=mDeletedUseDefs.find(replacement)) != mDeletedUseDefs.end() ) {
      replacement = (*iter).second;
    }

    if (useDef != replacement) {
      boundary_flow.insert(flow);
      flow->setUseDefNode(replacement);
    }
  }

}


void RETransform::repairMergedFlow(NUDesign * design, ClockAndDriver* clock_and_driver, FLNodeElabSet* deleted_flow_elabs, Stats* stats)
{
  if (not mDeletedUseDefs.empty() or
      not mCreatedUseDefs.empty()) {
    NUAlwaysBlockSet replacement_blocks;
    for (NUUseDefSet::iterator iter = mCreatedUseDefs.begin(); 
	 iter != mCreatedUseDefs.end(); 
	 ++iter) {
      NUUseDefNode* useDef = (*iter);
      NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(useDef);
      NU_ASSERT(always,useDef);
      replacement_blocks.insert(always);
    }
  
    FLNodeSet boundary_flow;
    boundary_flow.insert(mMergedFlow.begin(),
			 mMergedFlow.end());
    discoverBoundary(boundary_flow);

    SplitFlow splitFlow(mSymtab,
                        mStrCache,
                        mLocFactory,
                        mFlowFactory,
                        mFlowElabFactory,
                        mNetRefFactory,
                        mMsgContext,
                        mIODB,
                        mArgs);
    splitFlow.repair(design, 
                     replacement_blocks,
                     boundary_flow,
                     mMergedElabToFlow,
                     deleted_flow_elabs,
                     clock_and_driver,
                     true,
                     stats);
  }

  for (NUUseDefOldToNew::iterator iter = mDeletedUseDefs.begin(); 
       iter != mDeletedUseDefs.end(); 
       ++iter) {
    NUUseDefNode* useDef = (*iter).first;
    delete useDef;
  }

  mDeletedUseDefs.clear();
  mCreatedUseDefs.clear();
  mMergedFlow.clear();
  mMergedElabToFlow.clear();
}

void 
RETransform::repairFlow(NUDesign* design, FLNodeElabSet* deletedElabFlow,
                        ClockAndDriver* clockAndDriver, Stats* stats)
{
  repairBlockFlows(design, mRepairBlocks, deletedElabFlow, clockAndDriver, stats);
  
  // All done with the memory
  mRepairBlocks->clear();
}

void
RETransform::repairBlockFlows(NUDesign* design,
                              NUAlwaysBlockSet* blockSet,
                              FLNodeElabSet* deletedElabFlow,
                              ClockAndDriver* clockAndDriver,
                              Stats* stats)
{
  if (blockSet->empty())
    return;

  // Create a searchable usedef set so we don't have to convert
  // usedef's to always blocks. That is because the reason we are
  // doing repairs is that some usedefs got deleted and we can't
  // dynamic cast those.
  NUUseDefSet useDefSet;  
  useDefSet.insert(blockSet->begin(), blockSet->end());

  // Find the affected elaborated and unelaborated flows
  FLNodeSet repairFlows;
  FLNodeConstElabMap unelabToElabMap;
  FLNodeElab* flowElab;
  for (FLNodeElabFactory::FlowLoop i = mFlowElabFactory->loopFlows();
       i(&flowElab);) {
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    if ((useDef != NULL) && (useDefSet.find(useDef) != useDefSet.end())) {
      FLNode* flow = flowElab->getFLNode();
      repairFlows.insert(flow);
      FLNodeElabSet& elabFlows = unelabToElabMap[flow];
      elabFlows.insert(flowElab);
    }
  }
  
  // Create the boundary flow
  FLNodeSet boundaryFlow;
  boundaryFlow.insert(repairFlows.begin(), repairFlows.end());
  for (FLNodeFactory::iterator i = mFlowFactory->begin();
       i != mFlowFactory->end();
       ++i) {
    // Update our boundary flow
    FLNode * flow = *i;
    NUUseDefNode* useDef = flow->getUseDefNode();
    if (useDefSet.find(useDef) != useDefSet.end()) {
      boundaryFlow.insert(flow);
    }
  }
  
  // Repair the flow
  SplitFlow splitFlow(mSymtab,
                      mStrCache,
                      mLocFactory,
                      mFlowFactory,
                      mFlowElabFactory,
                      mNetRefFactory,
                      mMsgContext,
                      mIODB,
                      mArgs);
  splitFlow.repair(design, 
                   *blockSet,
                   boundaryFlow,
                   unelabToElabMap,
                   deletedElabFlow,
                   clockAndDriver,
                   true,
                   stats);
} // RETransform::repairBlockFlows


void RETransform::optimizeMergedFlows(NUDesign *design, bool verbose)
{
  if (mCreatedUseDefs.empty()) {
    return; // nothing to do.
  }

  bool do_rescope   = (not mArgs->getBoolValue(CRYPT("-noRescope")));
  bool do_inference = (not mArgs->getBoolValue(CRYPT("-noVectorInference")));
  bool do_motion    = (not mArgs->getBoolValue(CRYPT("-noCodeMotion")));
  bool do_expression_motion = (mArgs->getBoolValue(CRYPT("-expressionMotion")));
  bool do_extract   = (not mArgs->getBoolValue(CRYPT("-noExtractControl")));
  bool do_ternary_creation = (not mArgs->getBoolValue(CRYPT("-noTernaryCreation")));
  bool do_ternary_gate = (not (mArgs->getBoolValue(CRYPT("-noTernaryGateOptimization"))));
  bool do_copy_prop  = (not mArgs->getBoolValue(CRYPT("-noCopyPropagation")));
  bool do_const_prop = true;

  bool debugging = mArgs->getBoolValue("-g");
  if (debugging) {
    // these opts have visibility effects. disable under -g.
    do_rescope = false;
    do_motion = false;
  }

  if (not (do_rescope or do_motion or do_inference or do_extract or do_copy_prop or do_const_prop)) {
    return;
  }

  bool verbose_rescope   = verbose or mArgs->getBoolValue(CRYPT("-verboseRescope"));
  bool verbose_inference = verbose or mArgs->getBoolValue(CRYPT("-verboseInference"));
  bool verbose_motion    = verbose or mArgs->getBoolValue(CRYPT("-verboseCodeMotion"));
  bool verbose_extract   = verbose or mArgs->getBoolValue(CRYPT("-verboseExtractControl"));
  bool verbose_ternary_creation = mArgs->getBoolValue(CRYPT("-verboseTernaryCreation"));
  bool verbose_ternary_gate = true;
  bool verbose_copy_prop  = verbose or mArgs->getBoolValue(CRYPT("-verboseCopyPropagation"));
  SInt32 verboseConstProp = 0;
  mArgs->getIntLast(CRYPT("-verboseConstantPropagation"), &verboseConstProp);
  bool verbose_const_prop = verbose or (verboseConstProp >= 2);
  bool verbose_prop       = verbose or mArgs->getBoolValue(CRYPT("-verboseLocalPropagation"));

  SInt32 memory_limit = 0;
  ArgProc::OptionStateT arg_status;
  arg_status = mArgs->getIntLast(CRYPT("-rescopeMemoryLimit"),
                                &memory_limit);
  INFO_ASSERT(arg_status==ArgProc::eKnown,
              "Problem parsing switch -rescopeMemoryLimit");

  // Determine which module contains each use def
  ModuleCreatedUseDefs moduleCreatedUseDefs;
  groupBlocksByModule(moduleCreatedUseDefs);

  ElabAllocAlias alloc_alias(AllocAlias::eMatchedPortSizes);
  alloc_alias.compute(design, mSymtab);

  Fold fold (mNetRefFactory, 
	     mArgs,
	     mMsgContext, 
	     mStrCache,
	     mIODB,
	     mArgs->getBoolValue(CRYPT("-verboseFold")), 
             eFoldUDValid);     // Don't set eFoldUDKiller - it would confuse Schedule
  InferenceStatistics inference_stats;
  Inference::Params params (mArgs, mMsgContext);
  Inference inference(&fold, 
    mNetRefFactory, mMsgContext, mStrCache,
    mIODB, mArgs, params, &inference_stats);
  RescopeStatistics rescope_stats;
  Rescope rescope(mStrCache,
		  mNetRefFactory,
		  mMsgContext,
		  mIODB,
                  mArgs,
                  memory_limit,
		  verbose_rescope,
		  &rescope_stats);

  SInt32 expr_motion_threshold = CodeMotion::cDefExprMotionThreshold;
  mArgs->getIntLast(CRYPT("-expressionMotionThreshold"), &expr_motion_threshold);

  CodeMotionStatistics code_motion_stats;
  CodeMotion mover(mStrCache,
                   mNetRefFactory,
		   mMsgContext,
		   mIODB,
                   mArgs,
                   &alloc_alias,
                   &fold,
                   do_expression_motion,
                   expr_motion_threshold,
		   verbose_motion,
		   &code_motion_stats);

  ControlExtractStatistics control_extract_stats;
  ControlExtract extract(mStrCache,
			 mNetRefFactory,
			 mMsgContext,
			 mIODB,
                         mArgs,
			 verbose_extract,
			 &control_extract_stats);

  TernaryGateOptimization ternary_gate(mStrCache, 
                                       mNetRefFactory, 
                                       mMsgContext, 
                                       mIODB, 
                                       mArgs, 
                                       verbose_ternary_gate,
                                       &control_extract_stats);

  TernaryStatistics ternary_statistics;
  Ternary ternary_creation(mStrCache,
                           mNetRefFactory,
                           mMsgContext,
                           mIODB,
                           mArgs,
                           false, // allow non-bitsel conditions
                           true,  // allow balanced conditionals.
                           false, // allow non-bitsel definitions.
                           true,  // fold results.
                           verbose_ternary_creation,
                           &ternary_statistics,
                           &inference_stats);

  Reorder reorder(mStrCache, mNetRefFactory, mIODB, mArgs,
                  mMsgContext, &alloc_alias, true, false, true, false);

  UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs,
        mArgs->getBoolValue(CRYPT("-verboseUD")));

  LocalPropagationStatistics const_prop_statistics;
  RELocalConstProp const_prop(mIODB, &ud, &fold, 
                              &const_prop_statistics, 
                              verbose_const_prop,
                              false); // do not use flow order
  LocalPropagationStatistics copy_prop_statistics;
  RELocalCopyProp copy_prop(mIODB, &ud, &fold, 
                            &copy_prop_statistics,
                            verbose_copy_prop);

  NUNetSet rescoped_nets;
  for (ModuleCreatedUseDefs::iterator iter = moduleCreatedUseDefs.begin();
       iter !=  moduleCreatedUseDefs.end();
       ++iter) {
    NUModule * module = iter->first;
    NUUseDefVector & useDefs = iter->second;

    // Sort the blocks so we get stable output
    std::sort(useDefs.begin(), useDefs.end(), NUUseDefNodeCmp());

    if (do_inference) {
      // Inference does not update UD; should be unnecessary.
      inference.mergedBlocks(module, useDefs);
    }

    if (do_const_prop or do_copy_prop) {
      for (NUUseDefVector::iterator ud_iter = useDefs.begin();
           ud_iter != useDefs.end();
           ++ud_iter) {
        NUUseDefNode * udNode = (*ud_iter);
        NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(udNode);
        NU_ASSERT(always,udNode);
        if (do_const_prop) { const_prop.alwaysBlock(always); }
        if (do_copy_prop)  { copy_prop.alwaysBlock(always); }
      }
    }

    if (do_rescope) {
      rescope.mergedBlocks(module, useDefs, &rescoped_nets);
    }
    
    if (do_ternary_gate) {
      // TernaryGateOp does not update UD; should be unnecessary.
      ternary_gate.mergedBlocks(module, useDefs);
    }

    UInt32 iterations = 0;
    UInt32 max_iterations = 20;

    // First, iteratively apply CodeMotion and ControlExtract. This
    // combines control structures and moves statements into these
    // control structures without affecting the incoming order. 

    // Code motion can make extration possibilities apparant and vice
    // versa, thus the need for iteration.
  
    // Block merging often merges things in dependency order; this
    // makes it likely that related constructs will be adjacent,
    // improving the success of ControlExtract.

    bool do_another_pass = false;
    do {
      do_another_pass = false;
      ++iterations;

      if (do_motion) {
        do_another_pass |= mover.mergedBlocks(module, useDefs);
      }

      if (do_extract) {
        do_another_pass |= extract.mergedBlocks(module, useDefs);
      }

    } while (do_another_pass and (iterations < max_iterations));

    if (do_ternary_creation) {
      // Ternary creation may run UD on the blocks that are needed.
      ternary_creation.mergedBlocks(module, useDefs);
    }

    // The second round of iterations incooperates a Reorder step.
    // Reordering exposes optimization opportunities which were not
    // visible in the incoming Nucleus (for ControlExtract, in
    // particular).

    do {
      do_another_pass = false;
      ++iterations;

      if (do_motion) {
        do_another_pass |= mover.mergedBlocks(module, useDefs);
      }

      if (do_extract) {
        for (NUUseDefVector::iterator ud_iter = useDefs.begin();
             ud_iter != useDefs.end();
             ++ud_iter) {
          NUUseDefNode * udNode = (*ud_iter);
          NUStructuredProc * proc = dynamic_cast<NUStructuredProc*>(udNode);
          NU_ASSERT(proc,udNode);
          reorder.structuredProc(proc);
        }
      }

      // code motion can make extration possibilities apparant and vice versa.
      if (do_extract) {
        do_another_pass |= extract.mergedBlocks(module, useDefs);
      }

    } while (do_another_pass and (iterations < max_iterations));

#if 0
    // ACA: This seemed like a good idea, but prevents Fold from
    // applying the following transform during backend:
    // if (sel) out = 0;    ==>  out = 0;
    // else     out = x&y;       if (!sel) out = x & y;

    if (do_ternary_creation) {
      // Ternary creation does not update UD; should be unnecessary.
      ternary_creation.mergedBlocks(module, useDefs);
    }
#endif

    if (do_const_prop or do_copy_prop) {
      for (NUUseDefVector::iterator ud_iter = useDefs.begin();
           ud_iter != useDefs.end();
           ++ud_iter) {
        NUUseDefNode * udNode = (*ud_iter);
        NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(udNode);
        NU_ASSERT(always,udNode);
        if (do_const_prop) { const_prop.alwaysBlock(always); }
        if (do_copy_prop)  { copy_prop.alwaysBlock(always); }
      }
    }

    // Reordering, code motion and extraction make inferencing
    // possibilities apparant, but not vice-versa.
    if (do_inference) {
      // Inference does not update UD; should be unnecessary.
      inference.mergedBlocks(module, useDefs);
    }
  }

  if (do_inference and verbose_inference) {
    inference_stats.print();
  }

  if (do_rescope and verbose_rescope) {
    rescope_stats.print();
  }

  if (do_ternary_creation and verbose_ternary_creation) {
    ternary_statistics.print();
  }

  if (do_extract and verbose_extract) {
    control_extract_stats.print();
  }

  if (do_motion and verbose_motion) {
    code_motion_stats.print();
  }

  if (do_const_prop and verbose_prop) {
    const_prop_statistics.print("Local Constant Propagation");
  }

  if (do_copy_prop and verbose_prop) {
    copy_prop_statistics.print("Local Copy Propagation");
  }

  if (not rescoped_nets.empty()) {
    // Make sure the symtab no longer has storage/signature for nets
    // we have rescoped (converted into tmps).
    cleanupSymtabForNets(rescoped_nets);
  }
}


void RETransform::groupBlocksByModule(ModuleCreatedUseDefs & moduleCreatedUseDefs)
{
  // group use-defs by their containing module so we can perform a
  // number of optimizations a module at a time.

  for (NUUseDefSet::iterator iter = mCreatedUseDefs.begin(); 
       iter != mCreatedUseDefs.end(); 
       ++iter)
  {
    NUUseDefNode* useDef = (*iter);
    NUModule* module = const_cast<NUModule*>(useDef->findParentModule());
    moduleCreatedUseDefs[module].push_back(useDef);
  }
}


void RETransform::cleanupSymtabForNets(NUNetSet & rescoped_nets)
{
  CbuildSymTabBOM::BaseSet base_rescoped_nets;
  for (NUNetSet::iterator iter = rescoped_nets.begin();
       iter != rescoped_nets.end();
       ++iter) {
    base_rescoped_nets.insert(*iter);
  }

  CbuildSymTabBOM::LeafNodeSet masters;
  CbuildSymTabBOM::getMasters(&base_rescoped_nets,
			      &masters,
			      mSymtab);

  // clear the symtab storage and signatures for the rescoped nets.
  for (CbuildSymTabBOM::LeafNodeSet::iterator iter = masters.begin();
       iter != masters.end();
       ++iter) {
    STAliasedLeafNode * master = (*iter);
    // remove any scheduling information that may be in the alias ring
    // This has to be done before the clearStorage call.
    CbuildSymTabBOM::setSCHSignature(master, NULL);
    // clear storage for all aliases.
    master->clearStorage();
  }
}


RETransform::NetDelayBlocks*
RETransform::getNetDelayBlocks(NUModule* module)
{
  NetDelayBlocks* netDelayBlocks;
  ModNetDelayBlocks::iterator pos = mModNetDelayBlocks->find(module);
  if (pos == mModNetDelayBlocks->end())
  {
    netDelayBlocks = new NetDelayBlocks;
    mModNetDelayBlocks->insert(ModNetDelayBlocksValue(module,
						      netDelayBlocks));
  }
  else
    netDelayBlocks = pos->second;
  return netDelayBlocks;
}

void
RETransform::addNetDelayBlock(NUModule* module, const NUNetRefHdl& netRef,
			      NUAlwaysBlock* always, NUNet* delayNet)
{
  NetDelayBlocks* netDelayBlocks = getNetDelayBlocks(module);
  NUNet* net = netRef->getNet();
  NU_ASSERT2((netDelayBlocks->find(netRef) == netDelayBlocks->end()),net,module);
  BlockNetPair blockNetPair(always, delayNet);
  netDelayBlocks->insert(NetDelayBlocks::value_type(netRef, blockNetPair));
}

RETransform::BlockNetPair
RETransform::findNetDelayBlock(NUModule* module, const NUNetRefHdl& netRef)
{
  NetDelayBlocks* netDelayBlocks = getNetDelayBlocks(module);
  NetDelayBlocks::iterator pos = netDelayBlocks->find(netRef);
  if (pos != netDelayBlocks->end())
  {
    BlockNetPair blockNetPair = pos->second;
    return blockNetPair;
  }
  else
    return BlockNetPair(NULL,NULL);
}

RETransform::ModNetDelayBlocksLoop RETransform::loopModNetDelayBlocks()
{
  return ModNetDelayBlocksLoop(*mModNetDelayBlocks);
}

bool 
RETransform::CompareFLNodeElabs::operator()(const FLNodeElab* f1,
					    const FLNodeElab* f2) const
{
  return FLNodeElab::compare(f1, f2) < 0;
}


NUNetElab* RETransform::addResetBuffer( NUNetElab *netElab, FLNodeElab** returnElabFlow  )
{
  // Put the buffer at the storageNet location.  Other hierarchy may be dead from flattening.
  // fix for test/cust/mindspeed/m27480/carbon/cwtb/m27480.
  NUNet* net = netElab->getStorageNet();
  STBranchNode* hier = netElab->getStorageHier();
  const SourceLocator& loc = net->getLoc();
  NUModule* module = net->getScope()->getModule();
  // This caching does not yet hold the elaborated info, so it can not
  // be used for sync reset yet.
  //
  // Check the cache to see if we already have a buffer for this net.
  //BlockNetPair blockNetPair = findNetDelayBlock( module, net );
  //NUAlwaysBlock* cachedAlways = blockNetPair.first;
  //if( cachedAlways && 0 ) {
  //  // This net is already buffered.
  //  NUNet* cachedNet = blockNetPair.second;
  //  NUNetElab* cachedNetElab = cachedNet->lookupElab( hier );
  //  (*returnElabFlow) = cachedNetElab->getContinuousDriver();
  //  return cachedNetElab;
  //}

  // Go through the list of nets, creating statements
  NUStmtList stmtList;

  // Create the name for the new net
  StringAtom* newName = module->gensym("cds_sr_delay", NULL, net);

  // Create the new net
  NetFlags flags = NetFlags(eDMRegNet|eAllocatedNet|eTempNet);
  // For now we can assume sync reset nets are single bit.
  NU_ASSERT( net->getBitSize() == 1, net );
  NUNet* newNet = new NUBitNet( newName, flags, module, loc );
  module->addLocal(newNet);

  // Create the assign statement in an always block
  NUExpr* rvalue = new NUIdentRvalue( net, loc );
  NUBlockingAssign* assign = createAssign(newNet, rvalue, loc, &stmtList);
  NUAlwaysBlock* always = createAlways(module, stmtList, loc);
  
  // Run UD on all the modules
  UD ud( mNetRefFactory, mMsgContext, mIODB, mArgs, false );
  // Only need to call ud for the new always block I think. -Dylan
  //ud.module(module);
  ud.structuredProc( always );

  // We need to do this after UD is done above
  // Elaborate the delayed net.  Must be elaborated in all instances of
  // this module.
  for (STSymbolTable::NodeLoop i = mSymtab->getNodeLoop(); !i.atEnd(); ++i)
  {
    STSymbolTableNode *node = *i;
    NUElabBase *base = NUElabBase::find( node );
    if (base) {
      NUModuleElab *elab = dynamic_cast<NUModuleElab*>(base);
      if (elab && (module == elab->getModule()) ) {
        newNet->createElab( node->castBranch(), mSymtab );
      }
    }
  }

  // Now that UD is done, we can create the flow.
  FLNodeElabSet emptyFlowSet;
  NUNetRefHdl netRef = mNetRefFactory->createNetRef(newNet);
  FLNode* newFlow = createUnelaboratedData(&emptyFlowSet, always, assign, netRef);
  
  // Create the elaborated flow node
  NUNetElab* newNetElab = newNet->lookupElab( hier );
  FLNodeElab*  newElabFlow = mFlowElabFactory->create( newFlow, newNetElab, hier );

  // Create the nested elaborated flow
  FLNode* nestedFlow = newFlow;
  FLNodeElab* parentElabFlow = newElabFlow;
  while( nestedFlow->hasNestedBlock() ) {
    nestedFlow = nestedFlow->getSingleNested();
    FLNodeElab* nestedElabFlow;
    nestedElabFlow = mFlowElabFactory->create( nestedFlow, newNetElab, hier );
    parentElabFlow->addNestedBlock( nestedElabFlow );
    parentElabFlow = nestedElabFlow;
  }

  newNetElab->addContinuousDriver( newElabFlow );
  
  // Add fanin to the new elab flow.
  NUNetElab::DriverLoop d;
  for( d = netElab->loopContinuousDrivers(); !d.atEnd(); ++d ) {
    FLNodeElab* fanin = *d;
    newElabFlow->connectFanin( fanin );
    FLNodeElab* nestedFlow = newElabFlow;
    while( nestedFlow->hasNestedBlock() ) {
      nestedFlow = nestedFlow->getSingleNested();
      nestedFlow->connectFanin( fanin );
    }
  }
  (*returnElabFlow) = newElabFlow;
  return newNetElab;
} 

void RETransform::addTempNet(NUScope* scope, NUNet* tempNet)
{
  // Make space to put the net in if we haven't already
  NUNetVector* tempNets = NULL;
  TempNets::iterator pos = mTempNets->find(scope);
  if (pos == mTempNets->end()) {
    tempNets = new NUNetVector;
    mTempNets->insert(TempNets::value_type(scope, tempNets));
  } else {
    tempNets = pos->second;
  }

  // Add the temp net
  tempNets->push_back(tempNet);
}

RETransform::TempNetsLoop RETransform::loopTempNets()
{
  return TempNetsLoop(*mTempNets);
}

void RETransform::clearTempNets()
{
  for (TempNetsLoop l = loopTempNets(); !l.atEnd(); ++l) {
    NUNetVector* nets = l.getValue();
    delete nets;
  }
  mTempNets->clear();
}

void
RETransform::recordLatchClock(NUAlwaysBlock* latch, NUExpr* clk, NUExpr* origClk,
                              bool inverted)
{
  // Make sure the clock expression is made up of continously driven
  // nets. We can have block local temps from a different always block
  NUNetSet uses;
  clk->getUses(&uses);
  for (NUNetSetIter i = uses.begin(); i != uses.end(); ++i) {
    NUNet* net = *i;
    NU_ASSERT(!net->isTempBlockLocalNonStatic(), latch);
  }

  // If this clock is a whole identifier there is nothing to do
  if (clk->isWholeIdentifier()) {
    return;
  }

  // Check if we created this before
  NUModule* module = latch->findParentModule();
  NUExpr* clkExpr = createTempClk(module, clk, latch);

  // If this was inverted, we need to replace it inverted
  if (inverted) {
    CopyContext cntxt(module, 0, false);
    clkExpr = clkExpr->copy(cntxt);
    clkExpr = new NUUnaryOp(NUOp::eUnLogNot, clkExpr, clkExpr->getLoc());
    clkExpr->resize(1);
  }

  // Walk the block and replace the clocks. All the clock
  // expressions in an always blocks are duplicates of the edge
  // expression. So if we just compared pointers, the comparison
  // would fail. This uses a deep compare hash map to find the
  // clocks and replace them.
  NUExprExprDeepHashMap clockMap;
  clockMap.insert(NUExprExprDeepHashMap::value_type(origClk, clkExpr));
  REReplaceDeepExprExpr translator(clockMap);
  latch->getBlock()->replaceLeaves(translator);

  // If we created an inverted expression, delete it now
  if (inverted) {
    delete clkExpr;
  }
}

NUAlwaysBlock*
RETransform::convertLatchToFlop(NUAlwaysBlock* latch, ClockEdge edge,
                                NUExpr* clk, NUAlwaysBlock* priority)
{
  // Make a copy of the nested block statements
  NUModule* module = latch->findParentModule();
  CopyContext cntxt(module, 0, true);
  NUStmt* stmt = latch->getBlock()->copy(cntxt);
  NUBlock* newBlock = dynamic_cast<NUBlock*>(stmt);
  NU_ASSERT(newBlock != NULL, stmt);
  module->addBlock(newBlock);

  // Create the new always block and attach the block
  const SourceLocator& loc = latch->getLoc();
  StringAtom* alwaysName = module->newBlockName(mStrCache, loc);
  NUAlwaysBlock* newAlways = new NUAlwaysBlock(alwaysName, newBlock,
                                               mNetRefFactory, loc, false);
  module->addAlwaysBlock(newAlways);

  // Set the priority block pointer provided by the caller. We will
  // fixup the clock block pointers later. We have to do this before
  // the code below, because we need to get the priority clocks to add
  // to our replacement maps.
  if (priority != NULL) {
    newAlways->setPriorityBlock(priority);
  }

  // Make sure the edge expression is an ident. If not, we need to
  // find the created one.
  NUExpr* clkExpr;
  if (!clk->isWholeIdentifier()) {
    // We must have created this before
    NUExpr* rvalue = getTempClk(module, clk);
    clkExpr = rvalue->copy(cntxt);
  } else {
    // It is a valid ident expression, use it
    clkExpr = clk->copy(cntxt);
  }

  // Create a clock expression for this flop and attach it
  NUEdgeExpr* edgeExpr = new NUEdgeExpr(edge, clkExpr, loc);
  newAlways->addEdgeExpr(edgeExpr);

  // Figure out the constant replacements for clocks and any priority
  // blocks for this flop. This code counts on the fact that
  // recordLatchClocks has modified the original always blocks to use
  // any newly created temp clocks. The benefit is the clock constant
  // replacement is simpler. It can just use the edge expression from
  // the clock or priority block.
  findClockValues(newAlways);

  // Save the new always block for this flop description so that we
  // can do the follow on fixups. See fixupNewFlops().
  addNewAlways(latch, newAlways);
  return newAlways;
} // RETransform::convertLatchToFlop

void RETransform::findClockValues(NUAlwaysBlock* newAlways)
{
  // get the constant for this clock
  addConstExpr(newAlways, newAlways, false);

  // Add the constants for all priority blocks
  for (NUAlwaysBlock* priority = newAlways->getPriorityBlock();
       priority != NULL;
       priority = priority->getPriorityBlock()) {
    addConstExpr(newAlways, priority, true);
  }
}

void 
RETransform::addConstExpr(NUAlwaysBlock* newAlways, NUAlwaysBlock* clkAlways,
                          bool invert)
{
  // Get a copy of the edge expression
  NUEdgeExpr* edgeExpr = clkAlways->getEdgeExpr();
  NUExpr* clkExpr = edgeExpr->getExpr();
  CopyContext cntxt(NULL, NULL);
  NUExpr* clkExprCopy = clkExpr->copy(cntxt);

  // Create a constant for the clock
  ClockEdge edge = edgeExpr->getEdge();
  NUExpr* constExpr = NULL;
  const SourceLocator& loc = clkExprCopy->getLoc();
  if (((edge == eClockPosedge) && !invert) || 
      ((edge == eClockNegedge) && invert)) {
    constExpr = NUConst::create(false, 1,  1, loc);
  } else {
    constExpr = NUConst::create(false, 0,  1, loc);
  }
  constExpr->resize(clkExprCopy->getBitSize());

  // Find the replacement map for this new always block
  ClkConstReplacements::iterator pos2 = mClkConstReplacements->find(newAlways);
  NUExprExprDeepHashMap* replacementMap;
  if (pos2 == mClkConstReplacements->end()) {
    replacementMap = new NUExprExprDeepHashMap;
    ClkConstReplacements::value_type value(newAlways, replacementMap);
    mClkConstReplacements->insert(value);
  } else {
    replacementMap = pos2->second;
  }

  // Insert it into our replacement map. There is only one case where
  // it can already be there. It happens when two different clock
  // expressions fold down to the same constant and get replace with
  // the same temp net (random tests found this). In that case we make
  // sure that the replacement value is identical or we will have a
  // problem.
  NUExprExprDeepHashMap::iterator pos3 = replacementMap->find(clkExprCopy);
  if (pos3 != replacementMap->end()) {
    // Make sure it is the same value
    NUExpr* otherConstExpr = pos3->second;
    NU_ASSERT(*constExpr == *otherConstExpr, clkExprCopy);
  } else {
    // Haven't seen it before, add it
    NUExprExprDeepHashMap::value_type clkValue(clkExprCopy, constExpr);
    replacementMap->insert(clkValue);
  }
} // RETransform::addConstExpr

void
RETransform::addNewAlways(NUAlwaysBlock* oldAlways, NUAlwaysBlock* newAlways)
{
  NUAlwaysBlockVector& newAlwaysBlocks = (*mOldToNewAlwaysBlocks)[oldAlways];
  newAlwaysBlocks.push_back(newAlways);
  mRepairBlocks->insert(newAlways);
}


void RETransform::fixupNewFlops(NUDesign* design, Stats* stats)
{
  // The prioirity pointers should have been set during the conversion
  // process. We now need to pick a clock block. Note that because the
  // process can create a fully populated tree of blocks, there can be
  // more than one good candidate. We pick the deepest candidate. If
  // there are multiple candidates at the deepest depth, we pick one.
  //
  // TBD. I'm not sure we can currently handle multiple clock
  // blocks. I think there are some passes like constant propagation
  // that may not work correctly. We hope to get rid of clock/priority
  // block pointers making this fixup unecessary.
  connectClockPointers();

  // Replace the clocks with the appropriate constant values and then
  // optimize the blocks.
  replaceConstantClocks();

  // Create and repair flow and elaborated flow for all the always blocks
  repairAlwaysFlow(design, stats);
}

void RETransform::connectClockPointers()
{
  // Walk the the priority pointers and figure out the root of every
  // tree and the depth for every node in the tree. We are going to
  // pick one of the deepest nodes for every tree as the clock block.
  for (OldToNewAlwaysBlocksLoop l(*mOldToNewAlwaysBlocks); !l.atEnd(); ++l) {
    // Compute the root and depth for this tree
    DepthRootMap depthRootMap;
    const NUAlwaysBlockVector& alwaysBlocks = l.getValue();
    for (NUAlwaysBlockVectorCLoop a(alwaysBlocks); !a.atEnd(); ++a) {
      NUAlwaysBlock* always = *a;
      if (always->isSequential()) {
        determineBlockDepth(always, &depthRootMap);
      }
    }

    // Organize the trees by root and pick a block to be the clock block
    DepthRootMap rootElemMap;
    for (DepthRootMapLoop l(depthRootMap); !l.atEnd(); ++l) {
      // Get the root, depth, and element of the tree
      NUAlwaysBlock* always = l.getKey();
      DepthBlockPair depthRootPair = l.getValue();
      int depth = depthRootPair.first;
      NUAlwaysBlock* rootAlways = depthRootPair.second;

      // For this root, check if we have a higher depth
      DepthBlockPair elemDepthPair = rootElemMap[rootAlways];
      if (depth > elemDepthPair.first) {
        // The new item is better
        rootElemMap[rootAlways] = DepthBlockPair(depth, always);

      } else if (depth == elemDepthPair.first) {
        // They are the same, we want to make it canonical so compare
        // the clocks.
        //
        // TBD - I should probably assert here once the latch analysis
        // prunes these out. (commit from Jeff)
        NUAlwaysBlock* clkBlock = elemDepthPair.second;
        NUEdgeExpr* clkEdgeExpr = clkBlock->getEdgeExpr();
        NUEdgeExpr* otherEdgeExpr = always->getEdgeExpr();
        ptrdiff_t cmp = otherEdgeExpr->compare(*clkEdgeExpr,false,true,true);
        if (cmp < 0) {
          // switch clock blocks
          elemDepthPair.second = always;
          rootElemMap[rootAlways] = elemDepthPair;
        }
      }
    } // for


    // For every root, use the picked clock block
    for (DepthRootMapLoop l(depthRootMap); !l.atEnd(); ++l) {
      // Get this always block and its root
      NUAlwaysBlock* always = l.getKey();
      DepthBlockPair depthRootPair = l.getValue();
      NUAlwaysBlock* rootAlways = depthRootPair.second;

      // Get the clock block we choose for this root
      depthRootPair = rootElemMap[rootAlways];
      NUAlwaysBlock* clkBlock = depthRootPair.second;

      // If we don't have the clock block, set its clock block
      // pointer. This occurs if there are no async reset blocks.
      if (always != clkBlock) {
        always->setClockBlock(clkBlock);
      }
    }
  } // for
} // void RETransform::connectPointers

RETransform::DepthBlockPair
RETransform::determineBlockDepth(NUAlwaysBlock* always,
                                 DepthRootMap* depthRootMap)
{
  // Check if we have been here before
  DepthBlockPair depthRootPair;
  DepthRootMap::iterator pos = depthRootMap->find(always);
  if (pos != depthRootMap->end()) {
    // Return and incremented depth with the same root
    depthRootPair = pos->second;
    ++depthRootPair.first;

  } else {
    // Visit the priority tree to find the root
    NUAlwaysBlock* priority = always->getPriorityBlock();
    if (priority == NULL) {
      // This is the root
      depthRootPair = DepthBlockPair(1, always);

    } else {
      // Recurse looking for the root
      depthRootPair = determineBlockDepth(priority, depthRootMap);
      ++depthRootPair.first;
    }

    // Save the information in the cache
    depthRootMap->insert(DepthRootMap::value_type(always, depthRootPair));
  }

  return depthRootPair;
} // RETransform::determineBlockDepth

void RETransform::replaceConstantClocks()
{
  // Helper classes to optimize the blocks
  UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
  bool verbose = mArgs->getBoolValue(CRYPT("-verboseFold"));
  Fold fold(mNetRefFactory, mArgs, mMsgContext, mStrCache, mIODB, verbose,
            eFoldUDKiller);
  LocalPropagationStatistics propagation_statistics;
  RELocalConstProp constProp(mIODB, NULL, &fold, 
                             &propagation_statistics,
                             false, // not verbose
                             false); // do not use flow order

  // Walk and optimize each block individually. We can optimize each
  // block by using its clock condition and all its priority
  // conditions so they can be done independently.
  for (OldToNewAlwaysBlocksLoop l(*mOldToNewAlwaysBlocks); !l.atEnd(); ++l) {
    // Compute the root and depth for this tree
    DepthRootMap depthRootMap;
    const NUAlwaysBlockVector& alwaysBlocks = l.getValue();
    for (NUAlwaysBlockVectorCLoop a(alwaysBlocks); !a.atEnd(); ++a) {
      NUAlwaysBlock* always = *a;
      if (always->isSequential()) {
        // Find the constants for this always block
        NUExprExprDeepHashMap* clockMap;
        ClkConstReplacements::iterator pos = mClkConstReplacements->find(always);
        NU_ASSERT(pos != mClkConstReplacements->end(), always);
        clockMap = pos->second;

        // Walk the block and replace the clocks. All the clock
        // expressions in an always blocks are duplicates of the edge
        // expression. So if we just compared pointers, the comparison
        // would fail. This uses a deep compare hash map to find the
        // clocks and replace them.
        REReplaceDeepExprExpr translator(*clockMap);
        always->getBlock()->replaceLeaves(translator);

        // Recompute the UD and fold the block. We need UD up to date
        // for fold to work correctly. This is the first time UD has
        // been called on this block.
        ud.structuredProc(always);
        constProp.alwaysBlock(always);
      }

      // Make sure UD is up to date for all the blocks. For the
      // sequential blocks some clocks went away. For the
      // combinational blocks we are computing it for the first time.
      ud.structuredProc(always);
    }
  }

  // We are done with the clock maps, delete them now.
  deleteClkConstReplacementMap();
  deleteTempClkExprs();
} // void RETransform::replaceConstantClocks

void RETransform::deleteClkConstReplacementMap()
{
  // Get the created expressions. We can't delete them immediately
  // because it messes up hash maps.
  NUExprVector exprs;
  for (ClkConstReplacements::iterator i = mClkConstReplacements->begin();
       i != mClkConstReplacements->end(); ++i) {
    NUExprExprDeepHashMap* replacementMap = i->second;
    for (NUExprExprDeepHashMap::iterator e = replacementMap->begin();
         e != replacementMap->end(); ++e) {
      NUExpr* clkExpr = e->first;
      NUExpr* constExpr = e->second;
      exprs.push_back(clkExpr);
      delete constExpr;
    }
    delete replacementMap;
  }

  // Delete the clock expressions
  for (NUExprVectorIter i = exprs.begin(); i != exprs.end(); ++i) {
    NUExpr* expr = *i;
    delete expr;
  }
  mClkConstReplacements->clear();
} // void RETransform::deleteClkMaps

void RETransform::repairAlwaysFlow(NUDesign* design, Stats* stats)
{
  // To use the general repairFlow routine we have to fixup
  // unelaborated an elaborated flow somewhat. We have to make all new
  // always blocks reachable. But the fanin pointers don't have to be
  // correct. Start by creating the unelaborated always flow. This
  // reuses the existing flow for the first new always block.
  NewFlowsMap newFlowsMap;
  createUnelaboratedAlwaysFlow(&newFlowsMap);

  // Create elaborated versions of the temporary clock nets. We need
  // to create these for all instances even if the latch might be dead
  // in some instances. So we do a design walk to find all instances.
  elaboratedClockNets();

  // Create the elaborated always flow. This reuses the existing
  // elaborated flow for the first new always block. We only create
  // elaborated flow where it existed before.
  createElaboratedAlwaysFlow(newFlowsMap);

  // Now repair all the nested flow and fanin pointers within the
  // modified always blocks. Note this will also delete any always
  // blocks that are no longer referenced (the latch always blocks).
  FLNodeElabSet dummy;
  repairFlow(design, &dummy, NULL, stats);
}

void RETransform::createUnelaboratedAlwaysFlow(NewFlowsMap* newFlowsMap)
{
  // Find all the unelaborated flow that points to a latch we are
  // going to delete. There can be more than one unelaborated flow per
  // latch because always blocks can have more than one def.
  UInt32 latchCount = 0;
  typedef UtMap<NUAlwaysBlockVector*, FLNodeVector> LatchFlow;
  LatchFlow latchFlow;
  for (FLNodeFactory::iterator i = mFlowFactory->begin();
       i != mFlowFactory->end();
       ++i) {
    FLNode* flow = *i;
    NUUseDefNode* useDef = flow->getUseDefNode();
    NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
    if (always != NULL) {
      OldToNewAlwaysBlocks::iterator pos = mOldToNewAlwaysBlocks->find(always);
      if (pos != mOldToNewAlwaysBlocks->end()) {
        // Found one, store it (make sure we get every one)
        NUAlwaysBlockVector& newAlwaysBlocks = pos->second;
        FLNodeVector& flows = latchFlow[&newAlwaysBlocks];
        if (flows.empty()) {
          ++latchCount;
        }
        flows.push_back(flow);
      }
    }
  }

  // From this point on we traverse the loop of flow nodes that
  // matched latch always blocks. So there is a chance that we didn't
  // find all always blocks. This assert verifies that we found as
  // many always blocks as there are in our old to new map.
  INFO_ASSERT(latchCount == mOldToNewAlwaysBlocks->size(),
              "Inconsistency found when repairing latches converted to flops");

  // In this loop, we want to assign flows for each new always block.
  // We want to reuse existing flows when possible, but once they are
  // used up, we must create new flows.
  //
  // We also have combinational blocks in the latch to always
  // map. These are blocks to create temporary clocks since we can't
  // handle anything but an ident expression for clocks. We create
  // flow from the defs for those blocks.
  //
  // The newFlowsMap is a map from pre-existing unelaborated flows to
  // newly created unelaborated flows at the same level of the
  // hierarchy.  Given an elaborated flow, we can lookup its
  // unelaborated flow in the newFlowsMap to find a set of all newly
  // created unelaborated flows for which we need to create an
  // elaborated version, in the hierarchy of the elaborated flow we
  // started from.
  //
  // Since the new tempclk blocks do not have pre-existing
  // unelaborated flows, their new flow is added to the newFlowsMap
  // entry for each of the flows in the block.  This is overkill, but
  // it ensures that every hierarchy in which some def of the block is
  // elaboratedly live will get elaborated flow for the tempclk block
  // (We don't have elaborated liveness information so we choose to be
  // pessimistic and make the elaboration code guard against
  // duplicates.)
  typedef LoopMap<LatchFlow> LatchFlowLoop;
  for (LatchFlowLoop l(latchFlow); !l.atEnd(); ++l) {
    // Walk the always blocks, matching flows to the sequential blocks.
    NUAlwaysBlockVector* alwaysBlocks = l.getKey();
    const FLNodeVector& flows = l.getValue();

    // Process all of the defs of the flows
    for (FLNodeVectorCLoop f(flows); !f.atEnd(); ++f) {
      FLNode* flow = *f;
      NUNetRefHdl flowDefNetRef = flow->getDefNetRef();

      // Assign flow to each always block that overlaps this def.
      // The flow can only be used once, after that we create new
      // flows.  And each flow must be used by some always block.
      // Note that blocks that contain elaboratedly dead portions
      // should have been rejected before reaching here, so that
      // all flows will have corresponding new blocks.
      bool flowAvailable = true;
      for (NUAlwaysBlockVectorLoop a(*alwaysBlocks); !a.atEnd(); ++a) {
        NUAlwaysBlock* always = *a;
      
        NUNetRefSet defs(mNetRefFactory);
        always->getDefs(&defs);

        if (always->isSequential())
        {
          for (NUNetRefSet::NetRefSetLoop d = defs.loop(); !d.atEnd(); ++d) {
            const NUNetRefHdl& alwaysDefNetRef = *d;
            
            if (alwaysDefNetRef->overlapsSameNet(*flowDefNetRef))
            {
              // This always block should have a flow assigned to it for this def
              if (flowAvailable) {
                // Reuse the existing flow
                flow->replaceDefNetRef(flowDefNetRef,alwaysDefNetRef);
                flow->setUseDefNode(always);              
                flowAvailable = false;
              } else {
                // There was no available flow covering this def, so
                // create a new flow for it.
                FLNode* newFlow = mFlowFactory->create(alwaysDefNetRef, always);
                alwaysDefNetRef->getNet()->addContinuousDriver(newFlow);
                
                // Remember this so we can create elaborated flow
                FLNodeVector& newFlows = (*newFlowsMap)[flow];
                newFlows.push_back(newFlow);
              }
            }
          } // for (always block defs)
        }
      } // for (always blocks)
    } // for (flows)

    // The set of always blocks created may also include combinational blocks
    // that def tmpclk nets.  For these, we should always create a new flow.
    // There is no existing elaborated flow for these, so we add it to the
    // newFlowsMap entry for each existing unelaborated flow for the original
    // always block (to make sure that every block with any elaboratedly live
    // def will see it).
    for (NUAlwaysBlockVectorLoop a(*alwaysBlocks); !a.atEnd(); ++a) {
      NUAlwaysBlock* always = *a;
      
      if (!always->isSequential())
      {
        NUNetRefSet defs(mNetRefFactory);
        always->getDefs(&defs);

        for (NUNetRefSet::NetRefSetLoop d = defs.loop(); !d.atEnd(); ++d) {
          const NUNetRefHdl& alwaysDefNetRef = *d;
          NUNet* net = alwaysDefNetRef->getNet();
          NU_ASSERT(!net->isTempBlockLocalNonStatic(), always);
          FLNode* newFlow = mFlowFactory->create(alwaysDefNetRef, always);
          net->addContinuousDriver(newFlow);

          for (FLNodeVectorCLoop f(flows); !f.atEnd(); ++f) {
            FLNode* flow = *f;
            FLNodeVector& newFlows = (*newFlowsMap)[flow];
            newFlows.push_back(newFlow);
          }
        } // for (always block defs)
      }
    } // for (always blocks)
  } // for (always block vector -> flow vector map)

  // Walk the factory again and any unelaborated flow that points to
  // the fixed up flow, must also point to the new unelaborated
  // flow. This makes it reachable so that the flow repair pass finds
  // them all. Note that we artificially add the combinational blocks
  // has fanin. It does not matter that those edges are wrong because
  // they will be deleted by flow repair. Again, we are just keeping
  // the code simple to make the new unelaborated flow live.
  for (FLNodeFactory::iterator i = mFlowFactory->begin();
       i != mFlowFactory->end();
       ++i) {
    // Add new flow to this nodes fanin if necessary
    FLNode* flow = *i;

    Iter<FLNode*> loop = flow->loopFanin();
    connectNewFanin(flow, loop, *newFlowsMap,
                    &FLNode::connectFanin, 
                    &FLNode::clearFanin,
                    &FLNode::checkFaninOverlap);

    loop = flow->loopEdgeFanin();
    connectNewFanin(flow, loop, *newFlowsMap,
                    &FLNode::connectEdgeFanin, 
                    &FLNode::clearEdgeFanin,
                    &FLNode::checkEdgeFaninOverlap);
  }
} // void RETransform::createUnelaboratedAlwaysFlow

void
RETransform::connectNewFanin(FLNode* flow,
                             Iter<FLNode*> loop,
                             const NewFlowsMap& newFlowsMap,
                             void (FLNode::* connector)(FLNode*),
                             void (FLNode::* clear_fanin)(),
                             bool (FLNode::* check_overlap)(const FLNode *, NUNetRefFactory*))
{
  FLNodeVector replacements;
  bool replacement_needed = false;

  NewFlowsMap::const_iterator map_end = newFlowsMap.end();

  for (; not loop.atEnd(); ++loop) {
    FLNode* fanin = (*loop);
    NewFlowsMap::const_iterator location = newFlowsMap.find(fanin);
    if (location == map_end) {
      replacements.push_back(fanin);
    } else {
      // Connect all the new fanin. The old fanin is still a candidate
      // fanin, but is not in its own replacement set, so we handle
      // that independently.
      if (fanin->getUseDefNode()->isSequential()) {
        bool fanin_overlaps = (flow->*check_overlap)(fanin,mNetRefFactory);
        if (fanin_overlaps) {
          replacements.push_back(fanin);
          replacement_needed = true;
        }
      }
      const FLNodeVector& newFlows = location->second;
      for (FLNodeVectorCLoop f(newFlows); !f.atEnd(); ++f) {
        FLNode* newFanin = *f;
        // Checking isSequential on the new fanin is an easy way of
        // avoiding connecting the flow associated with the blocks for
        // generated clocks.
        if (newFanin->getUseDefNode()->isSequential()) {
          bool fanin_overlaps = (flow->*check_overlap)(newFanin,mNetRefFactory);
          if (fanin_overlaps) {
            replacements.push_back(newFanin);
            replacement_needed = true;
          }
        }
      }
    }
  }

  if (replacement_needed) {
    (flow->*clear_fanin)();
    for (FLNodeVector::iterator iter = replacements.begin();
         iter != replacements.end();
         ++iter) {
      FLNode * newFanin = (*iter);
      (flow->*connector)(newFanin);
    }
  }
}

void RETransform::elaboratedClockNets()
{
  // Gather all the affected modules
  NUModuleSet modules;
  for (OldToNewAlwaysBlocksLoop l(*mOldToNewAlwaysBlocks); !l.atEnd(); ++l) {
    NUAlwaysBlock* always = l.getKey();
    NUModule* module = always->findParentModule();
    modules.insert(module);
  }

  // Find all the possible hierarchies that are affect
  ModuleHierarchies moduleHierarchies;
  findModuleHierarchies(modules, &moduleHierarchies);

  // Elaborate the temp nets
  NUNetElabSet newElabNets;
  elaborateTempNets(moduleHierarchies, &newElabNets);

  // Create elaborated flow for all the new clock nets. We do this
  // here because it is easier to make sure all of them get created
  // for all possible hierarchies. (See bug4703).
  //
  // If any of this elaborations are dead, they should be deleted by
  // SplitFlow::repair.
  for (NUNetElabLoop l(newElabNets); !l.atEnd(); ++l) {
    // Gather the hierarchy and unelaborated net
    NUNetElab* netElab = *l;
    NUNet* net = netElab->getNet();
    STBranchNode* hier = netElab->getHier();

    // Get the unelaborated flow from the net
    for (NUNet::DriverLoop d = net->loopContinuousDrivers(); !d.atEnd(); ++d) {
      FLNode* flow = *d;
      FLNodeElab* flowElab = mFlowElabFactory->create(flow, netElab, hier);
      netElab->addContinuousDriver(flowElab);
    }
  }
}

void
RETransform::createElaboratedAlwaysFlow(NewFlowsMap& newFlowsMap)
{
  // Walk the elaborated flow factory to find all the elaborated flow
  // that points to unelaborated flow which was fixed up. The fixed up
  // flow is provided by the caller. We need these to create the new
  // elaborated flow and to have elaborated flow that points to fixed
  // up elaborated flow also point to the new elaborated flow.
  //
  // Note that the newFlowsMap only has entries if we created new
  // unelaborated flow. This is fine because we only have to create
  // elaborated flow if we created unelaborated flow.
  typedef UtMap<FLNode*, FLNodeElabVector> FixedElabFlow;
  FixedElabFlow fixedElabFlow;
  FLNodeElab* flowElab = NULL;
  for (FLNodeElabFactory::FlowLoop l = mFlowElabFactory->loopFlows();
       l(&flowElab);) {
    FLNode* flow = flowElab->getFLNode();
    NewFlowsMap::iterator pos = newFlowsMap.find(flow);
    if (pos != newFlowsMap.end()) {
      // Found one, remember it so we can create new elaborated flow
      // correctly.
      FLNodeElabVector& elabFlows = fixedElabFlow[flow];
      elabFlows.push_back(flowElab);
    }
  }

  // Create new elaborated flow for the new always blocks that don't
  // have them.
  //
  // Note that the newFlowsMap can have flows for tempclk blocks
  // duplicated in multiple entries, so we want to make sure we only
  // create flows for them once per hierarchy.
  NewElabFlowsMap newElabFlowsMap;
  typedef LoopMap<NewFlowsMap> NewFlowsMapLoop;
  for (NewFlowsMapLoop l(newFlowsMap); !l.atEnd(); ++l) {
    // Find the elaborated flows for this fixed unelaborated flow. The
    // only reason we wouldn't find one is if it's dead.
    FLNode* flow = l.getKey();
    FixedElabFlow::iterator pos = fixedElabFlow.find(flow);
    if (pos != fixedElabFlow.end()) {
      // Not dead, keep going
      FLNodeElabVector elabFlows = pos->second;
      for (FLNodeElabVectorLoop e(elabFlows); !e.atEnd(); ++e) {
        // Create the new elaborated flow for all the created unelaborated flow
        FLNodeElab* flowElab = *e;
        STBranchNode* hier = flowElab->getHier();
        const FLNodeVector& newFlows = l.getValue();
        for (FLNodeVectorCLoop f(newFlows); !f.atEnd(); ++f) {
          // Lookup the elaborated net for this def. It should have been
          // created already.
          FLNode* newFlow = *f;
          NUNet* net = newFlow->getDefNet();
          NUNetElab* netElab = net->lookupElab(hier);
          bool isTempClock = !newFlow->getUseDefNode()->isSequential();

          // If this is a tempclk that we have already given a driver to, do
          // not create additional flow.
          if (isTempClock) {
            FLN_ASSERT(netElab->isContinuouslyDriven(), newFlow);
            continue;
          }

          // Create the new elaborated flow
          FLNodeElab* newElabFlow;
          newElabFlow = mFlowElabFactory->create(newFlow, netElab, hier);
          netElab->addContinuousDriver(newElabFlow);

          // Remember this for elaborated flow fixup
          FLNodeElabVector& newElabFlows = newElabFlowsMap[flowElab];
          newElabFlows.push_back(newElabFlow);
        } // for
      } // for
    } // if
  } // for

  // Walk the factory again and any elaborated flow that points to the
  // fixed up elaborated flow must also point to the new elaborated
  // flow. This makes the new elaborated flow live.
  FLElabUseCache levelCache(mNetRefFactory, false);
  FLElabUseCache edgeCache(mNetRefFactory, true);
  for (FLNodeElabFactory::FlowLoop l = mFlowElabFactory->loopFlows();
       l(&flowElab);) {
    FLNodeElabLoop loop = flowElab->loopFanin();
    connectNewFanin(flowElab, loop, newElabFlowsMap, &levelCache,
                    &FLNodeElab::connectFanin,
                    &FLNodeElab::clearFanin,
                    &FLNodeElab::checkFaninOverlap);
    loop = flowElab->loopEdgeFanin();
    connectNewFanin(flowElab, loop, newElabFlowsMap, &edgeCache,
                    &FLNodeElab::connectEdgeFanin,
                    &FLNodeElab::clearEdgeFanin,
                    &FLNodeElab::checkEdgeFaninOverlap);
  }
} // RETransform::createElaboratedAlwaysFlow

void
RETransform::connectNewFanin(FLNodeElab* flowElab,
                             FLNodeElabLoop loop,
                             NewElabFlowsMap& newElabFlowsMap,
                             FLElabUseCache* cache,
                             void (FLNodeElab::* connector)(FLNodeElab*),
                             void (FLNodeElab::* clear_fanin)(),
                             bool (FLNodeElab::* check_overlap)(const FLNodeElab *, NUNetRefFactory*, FLElabUseCache*))
{
  FLNodeElabVector replacements;
  bool replacement_needed = false;

  NewElabFlowsMap::const_iterator map_end = newElabFlowsMap.end();

  for (; !loop.atEnd(); ++loop) {
    FLNodeElab* fanin = (*loop);
    NewElabFlowsMap::const_iterator location = newElabFlowsMap.find(fanin);
    if (location == map_end) {
      replacements.push_back(fanin);
    } else {
      // Connect all the new fanin. The old fanin is still a candidate
      // fanin, but is not in its own replacement set, so we handle
      // that independently.
      if (fanin->getUseDefNode()->isSequential()) {
        bool fanin_overlaps = (flowElab->*check_overlap)(fanin,mNetRefFactory,cache);
        if (fanin_overlaps) {
          replacements.push_back(fanin);
          replacement_needed = true;
        }
      }
      // Connect all the new fanin
      const FLNodeElabVector& newFlows = location->second;
      for (FLNodeElabVectorCLoop f(newFlows); !f.atEnd(); ++f) {
        FLNodeElab* newFanin = *f;
        // Checking isSequential on the new fanin is an easy way of
        // avoiding connecting the flow associated with the blocks for
        // generated clocks.
        if (newFanin->getUseDefNode()->isSequential()) {
          bool fanin_overlaps = (flowElab->*check_overlap)(newFanin,mNetRefFactory,cache);
          if (fanin_overlaps) {
            replacements.push_back(newFanin);
            replacement_needed = true;
          }
        }
      }
    }
  }

  if (replacement_needed) {
    (flowElab->*clear_fanin)();
    for (FLNodeElabVector::iterator iter = replacements.begin();
         iter != replacements.end();
         ++iter) {
      FLNodeElab * newFanin = (*iter);
      (flowElab->*connector)(newFanin);
    }
  }
}


NUBlockingAssign*
RETransform::createAssign(NUNet* defNet, NUExpr* rvalue,
                          const SourceLocator& loc, NUStmtList* stmtList)
{
  NULvalue* lvalue = new NUIdentLvalue(defNet, loc);
  return createAssign(lvalue, rvalue, loc, stmtList);
}

NUBlockingAssign*
RETransform::createAssign(NULvalue* lvalue, NUExpr* rvalue,
                          const SourceLocator& loc, NUStmtList* stmtList)
{
  NUBlockingAssign* assign = new NUBlockingAssign(lvalue, rvalue, false, loc);
  stmtList->push_back(assign);
  return assign;
}

NUAlwaysBlock*
RETransform::createAlways(NUModule* module, NUStmtList& stmtList,
                          const SourceLocator& loc)
{
  NUBlock* block = new NUBlock(&stmtList, module, mIODB, mNetRefFactory, 
                               false, loc);
  StringAtom* name = module->newBlockName(mStrCache, loc);
  NUAlwaysBlock* always = new NUAlwaysBlock(name, block, mNetRefFactory, loc, false);
  module->addAlwaysBlock(always);
  mRepairBlocks->insert(always);
  return always;
}

  
void
RETransform::findModuleHierarchies(NUModuleSet& moduleSet,
                                   ModuleHierarchies* moduleHierarchies)
{
  // Find all the elaborated modules in the symbol table
  for (STSymbolTable::NodeLoop i = mSymtab->getNodeLoop(); !i.atEnd(); ++i) {
    STSymbolTableNode *node = *i;
    NUElabBase *base = NUElabBase::find(node);
    if (base) {
      NUModuleElab *elab = dynamic_cast<NUModuleElab*>(base);
      if (elab != NULL) {
        // Make sure this is one of the modules we are interested in
	NUModule* module = elab->getModule();
        NUModuleSet::iterator pos = moduleSet.find(module);
	if (pos != moduleSet.end()) {
          // We want this instance, add the hierarchy
	  InstanceSet& instanceSet = (*moduleHierarchies)[module];
	  instanceSet.insert(node->castBranch());
	}
      }
    }
  }
} // RETransform::findModuleHierarchies

void 
RETransform::verifyBufferedNets(NUAlwaysBlock* always,
                                const NUNetRefSet& inNetRefs)
{
  // Find any input nets that are still referenced
  NUNetRefSet uses(mNetRefFactory);
  always->getBlock()->getBlockingUses(&uses);

  // Print a message about all nets that are still used
  for (NUNetRefSetIter i = inNetRefs.begin(); i != inNetRefs.end(); ++i) {
    const NUNetRefHdl& netRef = *i;
    if (uses.find(netRef, &NUNetRef::overlapsSameNet) != uses.end()) {
      NUNet* net = netRef->getNet();
      mMsgContext->REFailedBuffer(&always->getLoc(), net->getName()->str());
    }
  }
}

NUExpr*
RETransform::createTempClk(NUModule* module, NUExpr* clk, NUAlwaysBlock* latch)
{
  // Create a cache for this module if we haven't already
  TempClkExprs::iterator pos1 = mTempClkExprs->find(module);
  NUExprExprDeepHashMap* moduleTempClkExprs;
  if (pos1 == mTempClkExprs->end()) {
    moduleTempClkExprs = new NUExprExprDeepHashMap;
    mTempClkExprs->insert(TempClkExprs::value_type(module, moduleTempClkExprs));
  } else {
    moduleTempClkExprs = pos1->second;
  }

  // Create the temp clock if it hasn't been created already
  NUExpr* clkExpr = NULL;
  NUExprExprDeepHashMap::iterator pos2 = moduleTempClkExprs->find(clk);
  if (pos2 == moduleTempClkExprs->end()) {
    // Create a temp net from the expression so that we can map it
    // back to the original HDL.
    UtString buf;
    clk->compose(&buf, NULL, true);
    StringUtil::makeIdent(&buf);
    StringAtom* name = module->gensym(buf.c_str());
    const SourceLocator& loc = latch->getLoc();
    NUNet* clkNet = module->createTempBitNet(name, false, loc);
    addTempNet(module, clkNet);

    // Add an assignment that creates a clock. We create it as an
    // always block because the code to fix things up only works on
    // always blocks. It also simplifies things because we don't
    // have to deal with the different constructs.
    NUStmtList stmtList;
    CopyContext cntxt(module, 0, false);
    NUExpr* rvalue = clk->copy(cntxt);
    createAssign(clkNet, rvalue, loc, &stmtList);
    NUAlwaysBlock* always = createAlways(module, stmtList, loc);

    // Save the new always block in the map from latches to new
    // always blocks. Note that all the clock blocks are
    // combinational. That is how we tell them apart from
    // sequential.
    //
    // Does it make more sense to have two maps?
    addNewAlways(latch, always);

    // create the clock expression and remember it so we reuse them
    clkExpr = new NUIdentRvalue(clkNet, loc);
    moduleTempClkExprs->insert(NUExprExprDeepHashMap::value_type(clk, clkExpr));
  } else {
    // We created one before. Use a copy of that
    clkExpr = pos2->second;
  }

  return clkExpr;
} // RETransform::createTempClk

NUExpr* RETransform::getTempClk(NUModule* module, NUExpr* clk)
{
  // Get the cache for this module; it must exist
  TempClkExprs::iterator pos1 = mTempClkExprs->find(module);
  NU_ASSERT(pos1 != mTempClkExprs->end(), clk);
  NUExprExprDeepHashMap* moduleTempClkExprs = pos1->second;

  // Get the temp clock; it must exist
  NUExprExprDeepHashMap::iterator pos2 = moduleTempClkExprs->find(clk);
  NU_ASSERT(pos2 != moduleTempClkExprs->end(), clk);
  NUExpr* clkExpr = pos2->second;
  return clkExpr;
}

void RETransform::deleteTempClkExprs()
{
  for (LoopMap<TempClkExprs> l(*mTempClkExprs); !l.atEnd(); ++l) {
    NUExprExprDeepHashMap* moduleTempClkExprs = l.getValue();
    for (LoopMap<NUExprExprDeepHashMap> e(*moduleTempClkExprs); !e.atEnd(); ++e) {
      NUExpr* expr = e.getValue();
      delete expr;
    }
    delete moduleTempClkExprs;
  }
  mTempClkExprs->clear();
}
