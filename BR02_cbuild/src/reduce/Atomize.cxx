// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/Atomize.h"
#include "localflow/UD.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetClosure.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "util/SetOps.h"
#include "reduce/AllocAlias.h"

Atomize::Atomize(AtomicCache * str_cache,
                 NUNetRefFactory * net_ref_factory,
                 IODBNucleus * iodb,
                 ArgProc* args,
                 MsgContext * msg_ctx,
                 AllocAlias * alloc_alias,
                 bool verbose,
                 AtomizeStatistics * stats,
                 bool update_ud) :
  mStrCache(str_cache),
  mNetRefFactory(net_ref_factory),
  mIODB(iodb),
  mArgs(args),
  mMsgContext(msg_ctx),
  mAllocAlias(alloc_alias),
  mVerbose(verbose),
  mStatistics(stats),
  mUpdateUD(update_ud)
{
}

Atomize::~Atomize()
{
}

bool Atomize::module(NUModule * this_module)
{
  bool changed = false;
  changed |= atomizeBlocks(this_module);
  changed |= convertBlocks(this_module);
  if (changed) {
    mStatistics->module();
    if (mUpdateUD) {
      UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
      ud.module(this_module);
    }
  }
  return changed;
}

bool Atomize::atomizeBlocks(NUModule * this_module)
{
  NUNetSet possible_aliases;
  populatePossibleAliasSet(this_module, &possible_aliases);

  bool changed = false;
  NUAlwaysBlockVector resulting_always_blocks;
  for (NUModule::AlwaysBlockLoop loop = this_module->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock * always = (*loop);
    NUAlwaysBlockList one_block_replacements;
    bool replaced = atomizeBlock(always,&one_block_replacements,&possible_aliases);
    if (replaced) {
      // this always block has been replaced.
      for (NUAlwaysBlockList::iterator p = one_block_replacements.begin(),
             e = one_block_replacements.end(); p != e; ++p)
      {
        resulting_always_blocks.push_back(*p);
      }
      delete always;
      changed = true;
    } else {
      // the always block is unaffected by atomization. add it to our
      // list so we're able to update the full always block set.
      resulting_always_blocks.push_back(always);
    }
  }

  if (changed) {
    std::sort(resulting_always_blocks.begin(),
              resulting_always_blocks.end(),
              CmpAlwaysBlocks());
    NUAlwaysBlockList abl;

    // gcc 2.95 doesn't like the call to abl.insert here:
    //    ....stl_iterator.h:325: warning: comparison of unsigned expression >= 0 is always true
    //abl.insert(abl.end(),
    //           resulting_always_blocks.begin(),
    //           resulting_always_blocks.end());
    for (UInt32 i = 0, n = resulting_always_blocks.size(); i < n; ++i) {
      abl.push_back(resulting_always_blocks[i]);
    }
    this_module->replaceAlwaysBlocks(abl);
  }
  return changed;
}


bool Atomize::convertBlocks(NUModule * this_module)
{
  bool changed = false;
  NUAlwaysBlockList resulting_always_blocks;
  for (NUModule::AlwaysBlockLoop loop = this_module->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock * always = (*loop);
    NUContAssign * replacement = NULL;
    bool replaced = convertBlock(always,&replacement);
    if (replaced) {
      // the always block needs to be replaced. 
      NU_ASSERT(replacement, always);
      this_module->addContAssign(replacement);
      delete always;
      changed = true;
    } else {
      // the always block is unaffected by atomization. add it to our
      // list so we're able to update the full always block set.
      resulting_always_blocks.push_back(always);
    }
  }

  if (changed) {
    this_module->replaceAlwaysBlocks(resulting_always_blocks);
  }
  return changed;
}


bool Atomize::atomizeBlock(NUAlwaysBlock * always,
                           NUAlwaysBlockList * replacements,
                           NUNetSet * possible_aliases)
{
  bool valid = validAtomizationBlock(always);
  if (not valid) {
    return false;
  }

  StmtPositionMap position_map;
  StmtDefMap      def_map;

  NUNetClosure closure;

  // Seed the closure set with possible elaborated aliases.
  closure.addSet(possible_aliases);

  bool success = buildStmtDependencyInfo(always,
                                         position_map,
                                         def_map,
                                         &closure,
                                         possible_aliases);
  if (not success) {
    return false;
  }

  // do not create anything new if the closure only produces one set.
  if (closure.size()==1) {
    return false;
  }

  createIndependentBlocks(always,
                          position_map,
                          def_map,
                          &closure,
                          replacements);

  return true;
}


bool Atomize::validAtomizationBlock(NUAlwaysBlock * always)
{
  // skip async-reset generated blocks.
  if (always->getPriorityBlock() or
      always->getClockBlock())
  {
    return false;
  }

  NUBlock * block = always->getBlock();

  // skip blocks with locals.
  NUNetLoop nloop = block->loopLocals();
  if (not nloop.atEnd()) {
    return false;
  }

  return true;
}


bool Atomize::buildStmtDependencyInfo(NUAlwaysBlock   * always,
                                      StmtPositionMap & position_map,
                                      StmtDefMap      & def_map,
                                      NUNetClosure    * closure,
                                      NUNetSet        * possible_aliases)
{
  NUBlock * block = always->getBlock();

  // Keep track of whether we have seen possible elaborated alias defs so we
  // only insert the possible aliases set once.
  bool seen_possible_alias_def = false;

  SInt32 position = 0;
  NUNetSet local_defs;
  for (NUStmtLoop loop = block->loopStmts();
       not loop.atEnd();
       ++loop) {
    NUStmt * stmt = (*loop);
    NUNetSet defs;
    NUNetSet uses;

    // remember our position.
    position_map[stmt] = ++position;

    stmt->getBlockingDefs(&defs);
    stmt->getNonBlockingDefs(&defs);

    // we don't know how to handle a statement without any defs.
    if (defs.empty()) {
      return false;
    }

    // associate the defs with this statement
    for (NUNetSet::iterator iter = defs.begin();
         iter != defs.end();
         ++iter) {
      NUNet * def = (*iter);
      def_map.insert(StmtDefMap::value_type(def,stmt));
    }

    stmt->getBlockingUses(&uses);
    stmt->getNonBlockingUses(&uses);

    NUNetSet local_ties;

    // Only put the use nets which have been locally def'd into the full set.
    // Then, if there are free variables coming into the always block, we
    // can still break up the uses of those free variables.
    set_intersection(local_defs, uses, &local_ties);

    // add the defs as part of our local dependencies.
    local_ties.insert(defs.begin(), defs.end());

    // make our defs and local uses connected through the closure.
    closure->addSet(&local_ties);

    // build up the set of local defs as we proceed.
    local_defs.insert(defs.begin(), defs.end());

    // If any of these defs are possible elaborated aliases, then
    // add all the possible elaborated aliases into the defs.
    // Only need to do this once.
    if (not seen_possible_alias_def and set_has_intersection(local_defs, *possible_aliases)) {
      seen_possible_alias_def = true;
      local_defs.insert(possible_aliases->begin(), possible_aliases->end());
    }
  }

  // Invalid for atomization if the block doesn't contain any statements.
  return (position > 0);
}


void Atomize::createIndependentBlocks(NUAlwaysBlock     * always,
                                      StmtPositionMap   & position_map,
                                      StmtDefMap        & def_map,
                                      NUNetClosure      * closure,
                                      NUAlwaysBlockList * replacements)
{
  NUBlock  * block  = always->getBlock();
  NUModule * module = block->getModule();

  // for each group of connected nets...
  for (NUNetClosure::SetLoop loop = closure->loopSets();
       not loop.atEnd();
       ++loop) {
    NUNetSet * connected_nets = (*loop);

    NUStmtSet connected_stmts;

    // find the statements connected with those nets...
    for (NUNetSet::iterator iter = connected_nets->begin();
         iter != connected_nets->end();
         ++iter) {
      NUNet * net = (*iter);
      StmtDefMapRange range = def_map.equal_range(net);
      for (StmtDefMap::iterator stmt_iter = range.first;
           stmt_iter != range.second;
           ++stmt_iter) {
        NUStmt * stmt = (*stmt_iter).second;
        connected_stmts.insert(stmt);
      }
    }

    // It is possible that there are no statements for this
    // set of nets, since we added the set of possibly elaborated
    // alias nets in when we created the closure.
    if (connected_stmts.empty()) {
      continue;
    }

    // sort according to the original order..
    NUStmtVector connected_vec;
    for (NUStmtSet::UnsortedLoop p = connected_stmts.loopUnsorted();
         !p.atEnd(); ++p)
    {
      connected_vec.push_back(*p);
    }
    StmtPositionCmp cmp(position_map);
    std::sort(connected_vec.begin(),
              connected_vec.end(),
              cmp);

    NUStmtList stmts;
    stmts.assign(connected_vec.begin(),
                 connected_vec.end());

    // Take the LOC from the first statement. this avoids N blocks all
    // with the same source locator.
    const SourceLocator & loc = (*(stmts.begin()))->getLoc();

    // New block/always block.
    NUBlock * block = new NUBlock(&stmts,
                                  module,
                                  mIODB,
                                  mNetRefFactory,
                                  false,
                                  loc);
    StringAtom * name = module->newBlockName(mStrCache, loc);

    bool isWildCard = always->isWildCard();
    NUAlwaysBlock * partial = new NUAlwaysBlock(name,block,mNetRefFactory,loc, isWildCard, always->getAlwaysType());

    // re-parent nested blocks.
    block->adoptChildren();

    CopyContext cc(NULL,NULL);
    for (NUAlwaysBlock::EdgeExprLoop edge_loop = always->loopEdgeExprs();
         not edge_loop.atEnd();
         ++edge_loop) {
      NUExpr * edge = (*edge_loop);
      NUEdgeExpr * edge_copy = dynamic_cast<NUEdgeExpr*>(edge->copy(cc));
      partial->addEdgeExpr(edge_copy);
    }

    replacements->push_back(partial);
    mStatistics->target();
  }

  // all the statements have moved to other blocks.
  NUStmtList empty;
  block->replaceStmtList(empty);

  mStatistics->source();
}


bool Atomize::convertBlock(NUAlwaysBlock * always,
                           NUContAssign ** replacement)
{
  (*replacement) = NULL;

  bool valid = validConversionBlock(always);
  if (not valid) {
    return false;
  }
  NUBlock * block = always->getBlock();
  NUStmtLoop loop = block->loopStmts();
  if (loop.atEnd()) {
    return false; // no stmts.
  }

  NUStmt * stmt = (*loop);
  ++loop;
  if (not loop.atEnd()) {
    return false; // multiple stmts.
  }

  NUAssign * assign = dynamic_cast<NUAssign *>(stmt);
  if (not assign) {
    return false; // not an assignment!
  }

  CopyContext cc(NULL,NULL);
  NULvalue * lhs = assign->getLvalue()->copy(cc);
  NUExpr   * rhs = assign->getRvalue()->copy(cc);
  
  NUModule * module = block->getModule();

  StringAtom *name = module->newBlockName(mStrCache,
                                               assign->getLoc());
  (*replacement) = new NUContAssign(name,
                                    lhs,rhs,
                                    assign->getLoc());

  mStatistics->assign();

  return true;
}


bool Atomize::validConversionBlock(NUAlwaysBlock * always)
{
  // skip all sequential blocks.
  if (always->isSequential()) {
    return false;
  }

  NUBlock * block = always->getBlock();

  // skip blocks with locals.
  NUNetLoop nloop = block->loopLocals();
  if (not nloop.atEnd()) {
    return false;
  }
  return true;
}


void Atomize::populatePossibleAliasSet(NUModule * this_module,
                                       NUNetSet * possible_aliases)
{
  for (NUModule::NetLoop loop = this_module->loopNets();
       not loop.atEnd();
       ++loop) {
    NUNet *net = *loop;
    if (mAllocAlias->query(net)) {
      possible_aliases->insert(net);
    }
  }
}


StmtPositionCmp::StmtPositionCmp(StmtPositionMap & position_map):
  mPositionMap(position_map)
{}


bool StmtPositionCmp::operator()(const NUStmt * s1, const NUStmt * s2)
{
  SInt32 cmp = mPositionMap[s1] - mPositionMap[s2];
  return cmp < 0;
}


AtomizeStatistics::AtomizeStatistics()
{
  clear();
}


AtomizeStatistics::~AtomizeStatistics()
{
}


void AtomizeStatistics::module() 
{
  ++mModules;
}


void AtomizeStatistics::source() 
{
  ++mAtomizeSources;
}


void AtomizeStatistics::target() 
{
  ++mAtomizeTargets;
}


void AtomizeStatistics::assign() 
{
  ++mAssigns;
}


void AtomizeStatistics::clear()
{
  mModules = 0;
  mAtomizeSources = 0;
  mAtomizeTargets = 0;
  mAssigns = 0;
}


void AtomizeStatistics::print() const
{
  UtIO::cout() << "Atomization Summary: " 
               << UtIO::endl;
  UtIO::cout() << "    Atomized " << mModules
               << " modules."
               << UtIO::endl;
  UtIO::cout() << "    Separated " << mAtomizeSources 
               << " blocks into "  << mAtomizeTargets << " parts." 
               << UtIO::endl;
  UtIO::cout() << "    Converted " << mAssigns 
               << " single-stmt blocks into cont assigns." 
               << UtIO::endl;
}
