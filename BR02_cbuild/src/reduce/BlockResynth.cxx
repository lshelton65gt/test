// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file 
  Implementation of the block resynthesis package.
 */

/*
  Small test issues:

  1. infrastructure/obfuscate cost diffs (regold)
  2. func_layout -- regold
  3. cycle report diffs
  4. costs - regold
  5. stats - regold

    CARBON_CBUILD_ARGS=-blockResynthesis
    CARBON_NO_CBUILD_DIFF=1

test/assert                harmless spew
test/bugs/bug1819          cycle spew order
test/bugs/bug300           wrong answers, custom tb
test/bugs/bug3110          bvoConcat 0/1 sim diff
test/bugs/bug3587          timeout?
test/bugs/bug3687          z/non-z conflict
test/bugs/bug3978          wavequery diffs.  ok?
test/bugs/bug4284          verilog spew diff
test/bugs/bug5985          nCompare
test/clock-aliasing        randcrash sim diff
test/clock-collapse        bug1635 sim diff
test/constprop             tienet17 sim diffs
test/directives            schedule mask spew diffs
test/langcov/Stop          spew diffs
test/vhdl/beacon12         correct, eliminated clocks, changed vectors
test/cust/amcc/3450/citop  x/0 sim diffs
test/simple/testdriver          reorder6.v sim diffs

test/cust/amd/pogo/pogo-TD
test/cust/fujitsu/device/carbon/cwtb/rtl069
test/cust/matrox/phoenix/carbon/cwtb/demtop
test/cust/matrox/phoenix/carbon/cwtb/ps_top
test/cust/matrox/phoenix/carbon/cwtb/sysclktop
test/cust/matrox/sunex/carbon/cwtb/dmatop
test/cust/matrox/sunex/carbon/cwtb/hst_phy
test/cust/s3/columbia/carbon/cwtb/tb_c3d
test/cust/s3/metro/carbon/cwtb/VS
test/cust/toshiba/gfx/carbon/cwtb/gffifo
test/flatten
test/fold
test/func_layout
test/interra/misc2
test/langcov/verilogCase
test/mixed-lang/beacon_VHDL_top_p_ns
test/ondemand
test/protected-source
test/replay
test/rescope
test/sample
test/schedule/schedule6
test/schedule/schedule9
test/separatedblocks
test/shell
test/vhdl/beacon1
test/vhdl/beacon7
test/vhdl/lang_memory
test/vhdl/lang_predef_func

*/


#include "reduce/BlockResynth.h"

#include "localflow/UD.h"
#include "localflow/UpdateUD.h"
#include "localflow/UnelabFlow.h"
#include "reduce/RemoveUnelabFlow.h"
#include "reduce/AllocAlias.h"

#include "exprsynth/ExprFactory.h"
#include "exprsynth/ExprCallbacks.h"
#include "exprsynth/ExprResynth.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUUseDefNode.h"

typedef std::pair<FLNode*,const NUExpr*> NetExprPair;
typedef UtVector<NetExprPair> NetExprPairVector;


BlockResynth::BlockResynth(NUNetRefFactory *netref_factory,
                           FLNodeFactory *dfg_factory,
                           ArgProc* args,
                           MsgContext *msg_ctx,
                           IODBNucleus *iodb,
                           AtomicCache *str_cache,
                           bool verbose,
                           bool force,
                           AllocAlias* alloc_alias) :
  mNetRefFactory(netref_factory),
  mFlowFactory(dfg_factory),
  mArgs(args),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mStrCache(str_cache),
  mVerbose(verbose),
  mForce(force),
  mAllocAlias(alloc_alias)
{
  ESFactory* esFactory = new ESFactory;
  mPopulateExpr = new ESPopulateExpr(esFactory, mNetRefFactory);
  mExprResynth = new ExprResynth(ExprResynth::eStopAtScalar,
				 mPopulateExpr,
				 mNetRefFactory,
				 args,
				 mMsgContext,
				 mStrCache,
				 mIODB);
  mExprFactory = new NUExprFactory;
  mExprResynth->putExprFactory(mExprFactory);
  mCostContext = new NUCostContext;
}


BlockResynth::~BlockResynth()
{
  delete mExprResynth;
  delete mPopulateExpr->getFactory();
  delete mPopulateExpr;
  delete mExprFactory;
  delete mCostContext;
}


bool BlockResynth::module(NUModule * one_module)
{
  // Create the local flow graph.
  UnelabFlow unelab_flow(mFlowFactory, mNetRefFactory, mMsgContext, mIODB, false);
  unelab_flow.module(one_module, false);
  NUUseDefList pending_deletion;

  bool assign_modified = contAssigns(one_module, &pending_deletion);

  bool always_modified = alwaysBlocks(one_module, &pending_deletion);

  bool modified = assign_modified or always_modified;
  if (modified) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.module(one_module);
  }

  // Blow away the local flow graph.
  RemoveUnelabFlow remove(mFlowFactory);
  remove.remove();

  // Blow away UD cache
  if (modified) {
    ClearUDCallback clearUseDef(false);
    NUDesignWalker clearUDWalker(clearUseDef, false);
    clearUDWalker.module(one_module);
  }

  // remove always blocks and statements
  for (NUUseDefList::iterator iter = pending_deletion.begin();
       iter != pending_deletion.end();
       ++iter) {
    delete (*iter);
  }

  return modified;
}


/*!
 * Walk an expression and create a continuous assignment for each
 * unique subexpression. For expression 'E', greate a new net
 * '$resynth_E' and initialize the net with a continuous assign:
 *
 \code
     assign $resynth_E = E;
 \endcode
 *
 * If an expression contains any subexpressions, the subexpressions
 * are rewritten first and used when rewriting the parent. The
 * expression 'A & B' would become:
 *
 \code
     assign $resynth_A = A;
     assign $resynth_B = B;
     assign $resynth_A_&_B = $resynth_A & $resynth_B;
 \endcode
 *
 * An interface is provided so callers can determine the generated net
 * for a given expression
 */
class SubExprCallback : public NUDesignCallback
{
public:
  //! Constructor.
  /*!
   * \param str_cache The string cache - used for generating names for
   *            the new continuous assignments.
   *
   * \param assigns   List of generated continuous assignments.
   */
  SubExprCallback(AtomicCache * str_cache,
                  NUScope * scope,
                  NUContAssignList * assigns,
                  NUCostContext* costContext,
                  NUCost* cost);

  //! Destructor.
  ~SubExprCallback();

  //! We should never encounter non-expressions.
  Status operator()(Phase, NUBase *);

  //! Generic callback for unhandled expressions.
  Status operator()(Phase phase, NUExpr* expr);

  //! Constants.
  Status operator()(Phase phase, NUConst * expr);

  //! Full-net identifier. 
  /*!
   * TBD: Do not create assignments for these expressions. Map
   * directly to the contained net. This would reduce assignment
   * aliases.
   */
  Status operator()(Phase phase, NUIdentRvalue * expr);

  //! Partial-net identifier.
  Status operator()(Phase phase, NUVarselRvalue * expr);

  //! memory row identifier.
  Status operator()(Phase phase, NUMemselRvalue * expr);

  //! Operator.
  Status operator()(Phase phase, NUOp * expr);

  //! Which net has been assigned to this expression?
  NUNet * getNet(const NUExpr * expr) const;

  //! Generate an RHS identifier associated with the replacement net for this expression.
  NUExpr * getNetExpr(const NUExpr * expr);

private:
  //! Determine if an expression has already been encountered.
  /*!
   * \param expr The managed expression in question.
   *
   * \return true if 'expr' already has an associated replacement net.
   */
  Status processPreExpr(const NUExpr * expr) const;

  //! Remember the relation between a managed expression and the replacement net.
  void putNet(const NUExpr * expr, NUNet * net);

  //! Generate a replacement net and LHS for an expression.
  /*!
   * \param expr The managed expression being replaced.
   * \param rhs  The resulting (unmanaged) expression that has been recursively collapsed.
   *
   * \return An LHS containing the replacement net for 'expr'.
   */
  NULvalue * createLhs(const NUExpr * expr, NUExpr * rhs);

  //! Create a continuous assignment from an LHS and RHS.
  void createAssign(NULvalue * lhs, NUExpr * rhs);

  //! Wrapper around replacement net and assignment creation.
  /*!
   * Use this method when the given expression is a leaf in the
   * expression -- constant or an identifier (partial or full).
   *
   * The expression replacement is formed by completely copying the
   * original -- there are no subexpressions which need replacing. The
   * replacement net and LHS are created, then the continuous
   * assignment.
   */
  void createCopyAssign(const NUExpr * expr);

  //! The string cache.
  AtomicCache * mStrCache;
  
  //! Scope for net creation (usually a module).
  NUScope * mScope;
  
  //! Map between managed expressions and replacement nets.
  NUExprNetMap mExprNetMap;
  
  //! List of generated continuous assignments.
  NUContAssignList * mAssigns;

  //! Cost context
  NUCostContext* mCostContext;

  //! Running cost of continuous assigns
  NUCost* mCost;
};


bool BlockResynth::alwaysBlocks(NUModule * one_module, NUUseDefList * pending_deletion)
{
  bool modified = false;
  NUAlwaysBlockList replacements;
  for (NUModule::AlwaysBlockLoop loop = one_module->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock * always = (*loop);
    NUAlwaysBlockList one_replacements;
    bool one_modified = alwaysBlock(one_module, always, &one_replacements,
                                    pending_deletion);
    if (one_modified) {
      replacements.push_back(always);
      modified = true;
    } else {
      replacements.push_back(always);
    }
  }

  if (modified) {
    one_module->replaceAlwaysBlocks(replacements);
  }
  return modified;
}


static bool sIsValidExpr(ESExprHndl& expr_handle, FLNode* driver) {
  CarbonExpr* cexpr = expr_handle.getExpr();
  bool ret = false;
  if (cexpr != NULL) {
    ret = true;

    // Ensure that we haven't gotten back expression 'a' for def-net 'a'
    CarbonIdent* id = cexpr->castIdent();
    if (id != NULL) {
      NUNet* defnet = driver->getDefNet();
      FLNode* flow = id->getFlow();
      NU_ASSERT(flow, driver->getUseDefNode());

      // Ensure we were able to expand past the identifier. Expression
      // creation appeared successful, but in truth was incomplete.
      ret = flow->getDefNet() != defnet;
    }
  }
  return ret;
}


class ResynthValidExpr : public NUDesignCallback
{
public:
  //! We should never encounter non-expressions.
  Status operator()(Phase, NUBase*) {
    return eNormal;
  }

  //! Most expressions are OK
  Status operator()(Phase phase, NUExpr* expr) {
    if (phase == ePre) {
      if (checkCovered(expr)) {
        return eSkip;
      }

      // Check Z-driving at every level, because bug3522 has a testcase
      // where we have
      //
      //   a = b | (c ? d : 1'bz)
      //
      // which does not drive Z at the top level, but drives Z inside.
      // we can probably resynthesize and get answers which are not worse
      // than the answers we get when we don't resynthesize.  But we will
      // wind up asserting because we'll assign a variable to the ?: sub-expr
      // and that will drive Z.  So nip this issue in the bud and stay out
      // of the expressions with embedded Zs.

      if (expr->drivesZ()) {
        return eStop;
      }
    }
    return eNormal;
  }

  //! Change expressions are not OK
  Status operator()(Phase phase, NUOp * expr) {
    if (phase == ePre) {
      if (checkCovered(expr)) {
        return eSkip;
      }

      if (expr->getOp() == NUOp::eUnChange) {
        return eStop;
      }
      if (expr->sizeVaries()) {
        return eStop;               // test/vhdl/lang_predef_func/bug5666
      }
    }
    return eNormal;
  }
private:
  bool checkCovered(NUExpr* expr) {
    return !mCovered.insertWithCheck(expr);
  }

  NUCExprHashSet mCovered;
};

static bool sIsValidResynthExpr(const NUExpr* expr) {
  // z-driving expressions are not OK
  if (expr->drivesZ()) {
    return false;
  }

  // change-detect operators are not OK
  ResynthValidExpr cb;
  NUDesignWalker walker(cb, false);
  if (walker.expr(const_cast<NUExpr*>(expr)) == NUDesignCallback::eStop) {
    return false;
  }

  return true;
}


class BlockResynthSynthCallback : public ContinuousDriverSynthCallback {
public:
  CARBONMEM_OVERRIDES

  BlockResynthSynthCallback(ESPopulateExpr * populateExpr, FLNode * start_flow)
    : ContinuousDriverSynthCallback(populateExpr, start_flow),
      mExprCount(0)
  {
  }

  virtual bool stop(FLNode * flow, bool isContDriver) {
    // avoid creating huge exprs in resynthesis, until many of 
    // our n^2 problems are resolved
    if (mExprCount > 2000) {
      return true;
    }
    ++mExprCount;
    return ContinuousDriverSynthCallback::stop(flow, isContDriver);
  }

private:
  UInt32 mExprCount;
};

bool BlockResynth::contAssigns(NUModule * one_module, NUUseDefList * pending_deletion)
{
  bool modified = false;

  NUContAssignList assign_replacements;
  NetExprPairVector net_expr_vector;

  for (NUModule::ContAssignLoop loop = one_module->loopContAssigns();
       not loop.atEnd();
       ++loop) {
    NUContAssign * assign = (*loop);
    NUContAssignList one_replacements;

    bool one_modified = false;

    NUExpr* rval = assign->getRvalue();
    if (sIsValidResynthExpr(rval)) {
      NUNetRefSet defs(mNetRefFactory);
      assign->getDefs(&defs);
      NU_ASSERT(!defs.empty(), assign);
      NUNetRefHdl def_ref = *(defs.begin());
      NUNet* def = def_ref->getNet();

      if ((defs.size()==1) && qualify(def)) {

        // get the uses, and don't resynthesize any continuous assign that
        // is a self-cycle
        NUNetRefSet uses(mNetRefFactory);
        rval->getUses(&uses);

        FLNode * driver = getDrivingNode(def,assign);
        if (driver && ! NUNetRefSet::set_has_intersection(uses, defs)) {
          BlockResynthSynthCallback callback(mPopulateExpr, driver);
          ESExprHndl expr_handle = mPopulateExpr->create(driver, &callback);
          if (sIsValidExpr(expr_handle, driver)) {

            NUNet* net = driver->getDefNet();
            if (!net->isWiredNet() && !net->isTri() && !net->isForcible() &&
                !net->isTrireg() && !net->isDepositable() && !net->isDefault0() &&
                !net->isDefault1())
            {
              const SourceLocator & loc = net->getLoc();
              const NUExpr * factory_expr = mExprResynth->exprToNucleus(expr_handle.getExpr(), one_module, loc);
              if (sIsValidResynthExpr(factory_expr)) {
                one_modified = true;
                net_expr_vector.push_back(NetExprPair(driver,factory_expr));
              }
            }
          }
        }
      }
    }

    if (one_modified) {
      pending_deletion->push_back(assign);
      modified = true;
    } else {
      assign_replacements.push_back(assign);
    }
  }

  NUCost cost;
  SubExprCallback callback(mStrCache, one_module, &assign_replacements,
                           mCostContext, &cost);
  for (NetExprPairVector::iterator iter = net_expr_vector.begin();
       iter != net_expr_vector.end();
       ++iter) {
    NetExprPair net_expr = (*iter);
    FLNode* driver         = net_expr.first;
    NUNet * net            = driver->getDefNet();
    NUUseDefNode* ud       = driver->getUseDefNode();
    NU_ASSERT(ud, net);
    NUContAssign* assign   = dynamic_cast<NUContAssign*>(ud);
    NU_ASSERT(assign, ud);

    if (mVerbose) {
      print(net,"continuous assign");
    }

    const NUExpr * factory_expr = net_expr.second;

    NUDesignWalker walker(callback,false);
    NUDesignCallback::Status status = walker.expr(const_cast<NUExpr*>(factory_expr));
    NU_ASSERT(status == NUDesignCallback::eNormal,factory_expr);

    CopyContext cc (NULL,NULL);
    NULvalue * lhs = constructLvalue(driver);
    NUExpr   * rhs = callback.getNetExpr(factory_expr);

    StringAtom * sym = one_module->newBlockName(mStrCache,lhs->getLoc());
    assign = new NUContAssign(sym,lhs,rhs,lhs->getLoc());
    assign_replacements.push_back(assign);
  }

  if (modified) {
    one_module->replaceContAssigns(assign_replacements);
  }

  return modified;
}

NULvalue* BlockResynth::constructLvalue(FLNode* flow) {
  NUNetRefHdl defs = flow->getDefNetRef();
  NUNet* net = defs->getNet();
  const SourceLocator& loc = flow->getUseDefNode()->getLoc();

  if (defs->all()) {
    return new NUIdentLvalue(net, loc);
  }

  NULvalueVector lvalVec;
  for (NUNetRefRangeLoop p = defs->loopRanges(mNetRefFactory); !p.atEnd(); ++p)
  {
    const ConstantRange& range = *p;
    lvalVec.push_back(new NUVarselLvalue(net, range, loc));
  }

  if (lvalVec.size() > 1)
    return new NUConcatLvalue(lvalVec, loc);
  FLN_ASSERT((lvalVec.size() == 1), flow);
  return lvalVec[0];
} // NULvalue* BlockResynth::constructLvalue

bool BlockResynth::alwaysBlock(NUModule * one_module, NUAlwaysBlock * always,
                               NUAlwaysBlockList * /*replacements*/,
                               NUUseDefList* pending_deletion)
{
  bool acceptable = qualify(always);
  if (not acceptable) {
    return false;
  }

  NUNetSet defs;
  always->getDefs(&defs);

  NetExprPairVector net_expr_vector;
  const SourceLocator & loc = always->getLoc();

  for (NUStmtLoop loop(always->getBlock()->loopStmts());
       not loop.atEnd();
       ++loop)
  {
    NUStmt* stmt = *loop;
    NUNetSet stmt_defs;
    stmt->getBlockingDefs(&stmt_defs);

    // should we be concerned about processing the defs of a particular
    // statement in the correct order?  Probably!  This needs to be
    // revisited in a different commit.  The situation I'm concerned
    // about is where the order of defing of a net differs between two
    // branches of an if-statement.  But I haven't convinced myself
    // whether this is problematic for resynthesis or not.
    for (NUNetSet::SortedLoop p(stmt_defs.loopSorted()); !p.atEnd(); ++p) {
      NUNet* def = *p;

      // If this stmt is the first to def this particular net, then
      // process it now.
      NUNetSet::iterator d = defs.find(def);
      if (d != defs.end()) {
        defs.erase(d);

        if (def->isBlockLocal() && def->isNonStatic()) {
          continue;
        }

        acceptable = qualify(def);
        if (not acceptable) {
          return false;
        }

        FLNode * driver = getDrivingNode(def,always);
        if (not driver) {
          return false;
        }

        // I don't think we can correctly resynth an always-block that partially
        // drives a net, sad to say.
        if (not driver->getDefNetRef()->all()) {
          return false;
        }

        BlockResynthSynthCallback callback(mPopulateExpr, driver);
        ESExprHndl expr_handle = mPopulateExpr->create(driver, &callback);
        if (not sIsValidExpr(expr_handle, driver)) {
          // If NULL, we have an invalid expr.
          return false;
        }

        const NUExpr * factory_expr = mExprResynth->exprToNucleus(expr_handle.getExpr(), one_module, loc);
        if (! sIsValidResynthExpr(factory_expr)) {
          return false;
        }
        net_expr_vector.push_back(NetExprPair(driver,factory_expr));
      } // if
    } // for
  } // for

  // We must have hit all the always-block defs.  If not, punt.  Probably we
  // can assert, even.
  if (! defs.empty()) {
    return false;
  }

  NUStmtList stmts;
  NUContAssignList cont_assigns;
  NUCost cost;
  SubExprCallback callback(mStrCache, one_module, &cont_assigns,
                           mCostContext, &cost);
  for (NetExprPairVector::iterator iter = net_expr_vector.begin();
       iter != net_expr_vector.end();
       ++iter) {
    NetExprPair net_expr = (*iter);
    FLNode * driver        = net_expr.first;
    NUNet * net            = driver->getDefNet();

    if (mVerbose) {
      print(net, always->isSequential() ? "sequential block" : "combinational block");
    }

    const NUExpr * factory_expr = net_expr.second;

    NUDesignWalker walker(callback,false);
    NUDesignCallback::Status status = walker.expr(const_cast<NUExpr*>(factory_expr));
    NU_ASSERT(status == NUDesignCallback::eNormal,factory_expr);

    NUExpr   * rhs = callback.getNetExpr(factory_expr);
    NULvalue * lhs = constructLvalue(driver);

    NUBlockingAssign * assign = new NUBlockingAssign(lhs,rhs,false,lhs->getLoc());
    assign->calcCost(&cost, mCostContext);
    stmts.push_back(assign);
  }

  NUCost origCost;
  always->calcCost(&origCost, mCostContext);

  // Temporarily ignore the costing because we want to flush out bugs on tiny
  // testcases.
  if (mForce || (origCost.mInstructionsRun > cost.mInstructionsRun)) {
    for (NUStmtLoop loop = always->getBlock()->loopStmts();
         not loop.atEnd();
         ++loop)
    {
      NUStmt* stmt = *loop;
      pending_deletion->push_back(stmt);
    }
    always->getBlock()->replaceStmtList(stmts);
    for (NUContAssignList::iterator iter = cont_assigns.begin();
         iter != cont_assigns.end();
         ++iter)
    {
      NUContAssign* assign = *iter;
      one_module->addContAssign(assign);
    }
  }
  else {
    for (NUStmtList::iterator p = stmts.begin(), e = stmts.end(); p != e; ++p) {
      NUStmt* stmt = *p;
      delete stmt;
    }
    for (NUContAssignList::iterator iter = cont_assigns.begin();
         iter != cont_assigns.end();
         ++iter)
    {
      NUAssign* assign = *iter;
      delete assign;
    }
  }

  return true;
}

bool BlockResynth::qualify(NUAlwaysBlock * /*always*/)
{
  bool acceptable = true;
  return acceptable;
}


bool BlockResynth::qualify(NUNet* net) 
{
  if (net->is2DAnything()) {
    return false;
  }

  // If we are going to temp whole variables, then I don't think it
  // matters whether two nets are aliased together.  Then we are
  // relying on assignment aliasing to get rid of the extra temps.
  //
  // Right now my guess is that it's better to create the temps than
  // it is to exclude externally aliased nets from participating in
  // block resynthesis at all.  However, if I set this varaiable to
  // 0, then test/simpleSched/deposit1/deposit3.v fails with block
  // resynthesis forced.
#define AVOID_TEMPS_FOR_WHOLE_NETS 1
#if AVOID_TEMPS_FOR_WHOLE_NETS
  if (mAllocAlias->query(net)) {
    return false;               // test/reorder/hieralias3.v
  }
#endif

  if (net->isProtectedMutable(mIODB)) {
    return false;               // test/simpleSched/clkAsData/top3.v (delayedClock)
  }

#if 0
  bool acceptable = (not net->isMultiplyDriven());

  if (acceptable) {
    FLNode * driver = net->getContinuousDriver();
    acceptable = driver->getDefNetRef()->all();
  }

  return acceptable;
#else
  return true;
#endif
}

FLNode* BlockResynth::getDrivingNode(NUNet* net, NUUseDefNode * node)
{
  FLNode * driver = NULL;
  for (NUNet::DriverLoop loop = net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop)
  {
    FLNode * flow = (*loop);
    if (flow->getUseDefNode()==node) {
      if (driver) {
        return NULL;
      }

      // If this is a vector node then be sure we are driving the whole
      // thing.
      if (net->getBitSize() > 1) {
        // If this is from an always block then get the statement node
        // and look at what it kills

        if (node->getType() == eNUAlwaysBlock) {
          NUUseDefStmtNode* stmt = NULL;
          while ((stmt == NULL) && flow->hasNestedBlock()) {
            flow = flow->getSingleNested();
            NU_ASSERT(flow,node);
            stmt = dynamic_cast<NUUseDefStmtNode*>(flow->getUseDefNode());
          }
          NU_ASSERT(stmt, node);

          // Ensure that this node kills the whole net.  For example, in
          // test/arraysel/simple.v,
          //    state[sel]=~in[sel]
          // defs all of 'state' but does not kill it
          NUNetRefSet kills(mNetRefFactory);
          stmt->getBlockingKills(&kills);

          // Adding all the def'd bits of this flow-node into a copy of the
          // kills shouldn't enlarge the set.  Otherwise we know we didn't
          // kill the whole net.
          NUNetRefSet killCopy(kills);
          killCopy.insert(flow->getDefNetRef());
          if (killCopy != kills) {
            return NULL;
          }
        } // if
        else {
          // Otherwise this has to be a continuous assign.  Note that
          // we cannot find the kills for a continuous assign so
          // we need to look more carefully to ensure this is a killing
          // assign, which means we need either a straight identifier
          // or an NUVarselLvalue with a constant index.  Dynamic
          // part/bitselect writes def all bits but don't kill any of
          // them.
          NUContAssign* ca = dynamic_cast<NUContAssign*>(node);
          NU_ASSERT(ca, node);
          NULvalue* lv = ca->getLvalue();
          if (dynamic_cast<NUIdentLvalue*>(lv) == NULL) {
            NUVarselLvalue* var = dynamic_cast<NUVarselLvalue*>(lv);
            if ((var == NULL) || !var->isConstIndex()) {
              // concat-lvalue or dynamic select.  We should be
              // able to handle concat-lvalues but we'd need to
              // do some further work to ensure that we are
              // not writing to a dynamic select inside a concat.
              return NULL;
            }
          }
        }
      } // if

      driver = flow;
    } // if
  } // for
  return driver;
} // FLNode* BlockResynth::getDrivingNode


void BlockResynth::print(NUNet * net, const char * driver_type)
{
  net->getLoc().print();
  UtIO::cout() << " Resynthesized " << driver_type 
               << " driver of " << net->getName()->str();
  UtIO::cout() << UtIO::endl;
}


SubExprCallback::SubExprCallback(AtomicCache * str_cache,
                                 NUScope * scope,
                                 NUContAssignList * assigns,
                                 NUCostContext* costContext,
                                 NUCost* cost) :
  mStrCache(str_cache),
  mScope(scope),
  mAssigns(assigns),
  mCostContext(costContext),
  mCost(cost)
{
}


SubExprCallback::~SubExprCallback()
{
}


NUDesignCallback::Status SubExprCallback::operator()(Phase, NUBase * node)
{
  NU_ASSERT("SubExprCallback should never be invoked on an NUBase"==NULL,node);
  return eStop;
}


NUDesignCallback::Status SubExprCallback::operator()(Phase /*phase*/, NUExpr * node)
{
  NU_ASSERT("SubExprCallback should never be invoked on an NUExpr"==NULL,node);
  return eStop;
}


NUDesignCallback::Status SubExprCallback::operator()(Phase phase, NUConst * expr)
{
  if (phase==ePre) {
    return processPreExpr(expr);
  }

  createCopyAssign(expr);
  return eNormal;
}


NUDesignCallback::Status SubExprCallback::operator()(Phase phase, NUIdentRvalue * expr)
{
  if (phase==ePre) {
    return processPreExpr(expr);
  }

  createCopyAssign(expr);
  return eNormal;
}


NUDesignCallback::Status SubExprCallback::operator()(Phase /*phase*/, NUVarselRvalue * expr)
{
  NUDesignCallback::Status status = processPreExpr(expr);
  if (status == eNormal) {
    createCopyAssign(expr);
  }
  return eSkip; // don't recursively walk through the identifier.
}

NUDesignCallback::Status SubExprCallback::operator()(Phase phase, NUMemselRvalue * expr)
{
  if (phase==ePre) {
    return processPreExpr(expr);
  }

  createCopyAssign(expr);
  return eNormal;
}



NUDesignCallback::Status SubExprCallback::operator()(Phase phase, NUOp * expr)
{
  if (phase==ePre) {
    return processPreExpr(expr);
  }

  NU_ASSERT(not getNet(expr),expr);
  NUExpr * rhs = NULL;
  NUExpr::Type exprType = expr->getType();
  switch (exprType) {
  case NUExpr::eNUUnaryOp: {
    NUExpr * arg0 = getNetExpr(expr->getArg(0));

    rhs = new NUUnaryOp(expr->getOp(),
                        arg0,
                        expr->getLoc());
    break;
  }
  case NUExpr::eNUBinaryOp: {
    NUExpr * arg0 = getNetExpr(expr->getArg(0));
    NUExpr * arg1 = getNetExpr(expr->getArg(1));

    rhs = new NUBinaryOp(expr->getOp(),
                         arg0, arg1,
                         expr->getLoc());
    break;
  }
  case NUExpr::eNUTernaryOp: {
    NUExpr * arg0 = getNetExpr(expr->getArg(0));
    NUExpr * arg1 = getNetExpr(expr->getArg(1));
    NUExpr * arg2 = getNetExpr(expr->getArg(2));

    rhs = new NUTernaryOp(expr->getOp(),
                          arg0, arg1, arg2,
                          expr->getLoc());
    break;
  }
  case NUExpr::eNUConcatOp: {
    NUConcatOp * concat = static_cast<NUConcatOp*>(expr);
    NUExprVector new_args(expr->getNumArgs());
    for (UInt32 i = 0; i < concat->getNumArgs(); ++i) {
      new_args[i] = getNetExpr(concat->getArg(i));
    }
    rhs = new NUConcatOp(new_args, concat->getRepeatCount(), concat->getLoc());
    break;
  }
  default:
    expr->print(true,0);
    NU_ASSERT("Unhandled expr-type."==NULL,expr);
    break;
  } // switch
  NU_ASSERT(rhs!=NULL,expr);

  rhs->resize(expr->getBitSize());
  rhs->setSignedResult(expr->isSignedResult());
  NULvalue * lhs = createLhs(expr,rhs);
  createAssign(lhs,rhs);
  return eNormal;
}


NUDesignCallback::Status SubExprCallback::processPreExpr(const NUExpr * expr) const
{
  NUNet * net = getNet(expr);
  if (net) {
    return eSkip;
  } else {
    return eNormal;
  }
}


NUNet * SubExprCallback::getNet(const NUExpr * expr) const
{
  NUExprNetMap::const_iterator location = mExprNetMap.find(expr);
  if (location != mExprNetMap.end()) {
    return (*location).second;
  } else {
    return NULL;
  }
}


void SubExprCallback::putNet(const NUExpr * expr, NUNet * net)
{
  mExprNetMap[expr] = net;
}


// Do not create temps and assigns for constants or idents.  Note that
// this is not just an optimization, it's also a functional
// necessesity.  We can't substitute a temp variable for constants
// with 'z' in them.  Tristate analysis will not follow that flow.
// And in general there appears to be no benefit to replacing
// constants with variables -- CSE won't do much and there is already
// a common constant pool implemented in codegen.  So it is simplest
// just to keep all constants as they are.
static bool sNeedTempForExpr(const NUExpr* expr) {
  // do not temp constants
  if (expr->castConst() != NULL) {
    return false;
  }

#if 0 && AVOID_TEMPS_FOR_WHOLE_NETS
  if (expr->isWholeIdentifier()) {
    NUNet* net = expr->getWholeIdentifier();
    if ((expr->isSignedResult() == net->isSigned()) &&
        (expr->getBitSize() == net->getBitSize()))
    {
      return false;
    }
  }
#endif

  return true;
}

NUExpr * SubExprCallback::getNetExpr(const NUExpr * expr)
{
  NUExpr * rhs = NULL;

  if (sNeedTempForExpr(expr)) {
    NUNet * net = getNet(expr);
    NU_ASSERT(net!=NULL,expr);
    rhs = new NUIdentRvalue(net, expr->getLoc());
  }
  else {
    CopyContext cc (NULL,NULL);
    NU_ASSERT(getNet(expr) == NULL, expr);
    rhs = expr->copy(cc);
  }
  rhs->resize(expr->getBitSize());
  return rhs;
}


NULvalue * SubExprCallback::createLhs(const NUExpr * expr,
                                      NUExpr * rhs)
{
  NU_ASSERT(not getNet(expr),expr);
  // avoid using the expression in the resynth name.  It makes
  // the names too verbose in the context of resynthesis.
  StringAtom* sym = mScope->gensym("resynth");
  NU_ASSERT(expr->castConst() == NULL, expr);
  NUNet * net = mScope->createTempNet(sym,
                                      rhs->getBitSize(),
                                      rhs->isSignedResult(),
                                      rhs->getLoc());
  net->setFlags(NetFlags(net->getFlags() | eDMWireNet));
  putNet(expr,net); // save relationship.

  NULvalue * lhs = new NUIdentLvalue(net, expr->getLoc());
  return lhs;
}


void SubExprCallback::createAssign(NULvalue * lhs, NUExpr * rhs)
{
  NU_ASSERT(sIsValidResynthExpr(rhs), rhs);
  StringAtom * sym = mScope->newBlockName(mStrCache,lhs->getLoc());
  NUContAssign * assign = new NUContAssign(sym,lhs,rhs,lhs->getLoc());

  // walk thru rhs's uses & validate they are not non-static

  mAssigns->push_back(assign);
  assign->calcCost(mCost, mCostContext);
}

void SubExprCallback::createCopyAssign(const NUExpr * expr)
{
  if (sNeedTempForExpr(expr)) {
    CopyContext cc (NULL,NULL);
    NUExpr * rhs = expr->copy(cc);
    rhs->resize(expr->getBitSize());
    NULvalue * lhs = createLhs(expr, rhs);
    createAssign(lhs,rhs);
  }
}


