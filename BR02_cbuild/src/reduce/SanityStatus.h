// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef SANITY_STATUS_H_
#define SANITY_STATUS_H_

#include "util/UtList.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"

template <class NodeType>
class SanityStatus
{
public:
  //! Constructor
  /*!
    \param node The current node getting sanity-checked.
   */
  SanityStatus(NodeType * node) :
    mNode(node),
    mFailures(false)
  {}

  virtual ~SanityStatus() {};

  //! Add pass/fail information for one specific test.
  /*!
    \param name   The name of a single test.
    \param status True if the test passed.
   */
  virtual void addTest(const char * name, bool status);

  //! Add a single-line string of detail to the set of cached messages.
  virtual void addDetail(const UtString & detail);

  //! Print status details.
  virtual void print(bool verbose) const;

protected:
  //! Print header for status msg.
  virtual void printHeader() const = 0;

  //! Return the current node.
  NodeType * getNode() const { return mNode; }

private:
  //! The current node under test.
  NodeType * mNode;

  enum MsgStatus { ePass,
                   eFail,
                   eDetail };   // detail messages 'own' their strings

  //! Class to represent a sanity message.
  /*! Pass/Fail messages comprise a static string and a pass/fail enum.
   *! detail messages are only issued when there is a problem, and involve
   *! an allocated copy of composed string.
   */
  struct MessageInfo {
    //! ctor for pass/fail messages
    MessageInfo(const char* test, bool status) {
      mStatus = status? ePass: eFail;
      mContents.mTest = test;
    }

    //! ctor for detail messages
    MessageInfo(const UtString& detail) {
      mStatus = eDetail;
      mContents.mDetail = StringUtil::dup(detail.c_str(), detail.size()+1);
    }

    //! dtor
    ~MessageInfo() {
      if (mStatus == eDetail) {
        StringUtil::free(mContents.mDetail);
      }
    }

    //! copy ctor
    MessageInfo(const MessageInfo& src) {
      mStatus = src.mStatus;
      if (mStatus == eDetail) {
        mContents.mDetail = StringUtil::dup(src.mContents.mDetail);
      }
      else {
        mContents.mTest = src.mContents.mTest;
      }
    }

    //! assign op
    MessageInfo& operator=(const MessageInfo& src) {
      if (&src != this) {
        if (mStatus == eDetail) {
          StringUtil::free(mContents.mDetail);
        }
        mStatus = src.mStatus;
        if (mStatus == eDetail) {
          mContents.mDetail = StringUtil::dup(src.mContents.mDetail);
        }
        else {
          mContents.mTest = src.mContents.mTest;
        }
      }
      return *this;
    }

    union {
      const char* mTest;
      char* mDetail;
    } mContents;
    MsgStatus mStatus;
  };

  //! Messages for each test; includes failure details.
  typedef UtList<MessageInfo> MessageList;
  MessageList mMessages;

  //! Did any test fail?
  bool mFailures;
};

#endif
