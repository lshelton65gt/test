// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 *
 * Implement algebraic associativity for operators that have the
 * associative property.
 *
 */

#include "FoldI.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUExprFactory.h"

#include <functional>

//! Examine \a e, adding terms to the flattened vector \a v
bool FoldI::group (NUOp::OpT opcode, const NUExpr* e, NUCExprVector*v)
{
  if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(e)) {
    NUOp::OpT bopcode = bop->getOp ();
    if (bopcode == opcode)
    {
      return group (opcode, bop->getArg (0), v)
        && group (opcode, bop->getArg (1), v);
    }
  }

  // Not a term that's got associative sub-expressions
  v->push_back (e);
  return true;
}

// A sorting functor with the properties that:
//  For expressions e,e1,e2,  constant c1,c2, and unary operator op (-,~, etc) or 
//  binary op of the form "e == c1"
//  FORALL(e): e < c
//  FORALL(e, e1<e2): e1 < op(e1) < e2
//  
//! Check that operand \a e is a -EXP or ~EXP that folds against EXP
//! or a reduction operator that's compatible with the rest of the
//! operators.
// Returns ptrdiff_t so that we can use in AssocOrder::compare()
ptrdiff_t FoldI::AssocOrder::isRelated (const NUUnaryOp* e) const {
  if (not e)
    return 0;

  NUOp::OpT op = e->getOp ();
  return ((op == NUOp::eUnBitNeg)
          || (op == NUOp::eUnVhdlNot)
          || (op == NUOp::eUnRedAnd && mOp == NUOp::eBiBitAnd)
          || (op == NUOp::eUnRedOr && mOp == NUOp::eBiBitOr)
          || (mNegAndNot && op == NUOp::eUnMinus)) ? 1 : 0;
}


// Three-way compare
ptrdiff_t FoldI::AssocOrder::compare (const NUExpr* a, const NUExpr* b) const
{
  // First check for constants vs non constants
  const NUConst* ca = a->castConstNoXZ ();
  const NUConst* cb = b->castConstNoXZ ();

  if (ca && not cb)
    return 1;               // A > B
  else if (not ca && cb)
    return -1;              // A < B
  else if (ca && cb)
    // order constants by size if all else equal
    return a->compare (*b, false, false, true); // canonical order imposed by NUExpr::compare

  // Next check for XZ constants
  const NUConst* cxa = a->castConst ();
  const NUConst* cxb = b->castConst ();

  if (cxa && not cxb)
    return 1;               // A > B
  else if (not cxa && cxb)
    return -1;              // A < B
  else if (cxa && cxb)
    return a->compare (*b, false, false, true); // canonical order imposed by NUExpr::compare

  // No constant terms involved
  const NUUnaryOp* ua = dynamic_cast<const NUUnaryOp*>(a);
  const NUUnaryOp* ub = dynamic_cast<const NUUnaryOp*>(b);

  ptrdiff_t diff;
  if (ua && (not ub) && hasComplement ())
  {
    // make -a sort next to a
    diff =  ua->getArg (0)->compare (*b, false, false, true); // order (-a) < (b)) as (a) < (b)
    if (diff == 0)
      return 1;              // Make -e come JUST after e
    else
      return diff;
  }
  else if ((not ua) && ub && hasComplement ())
  {
    diff = a->compare (*(ub->getArg (0)), false, false, true); // a < (-b) => a < b
    if (diff == 0)
      return -1;
    else
      return diff;
  }
  else if (ua && ub && hasComplement ())
  {
    // order by operand and then operator
    diff = compare(ua->getArg (0), ub->getArg (0)); // order -a < -b as a < b
    if (diff == 0)
    {
      // Put the more related operand closer to the front.  E.g. if we have a + !a + (-a),
      // then (-a) is related, while !a is unrelated, so we should end up with a sort
      // order of a + (-a) + !a
      //
      diff = isRelated (ub) - isRelated (ua);
      if (diff == 0)
        return (ub->getOp () - ua->getOp ()); // order -a < ~a ....
    }
    return diff;
  }

  // No Unary terms - just compare the primaries
  return a->compare (*b, false, false, true);
}


bool FoldI::DenseOrder::operator() (const NUExpr*a, const NUExpr*b) const {

  const NUVarselRvalue *va = FoldI::stripVarsel (a, mOp);
  if (!va)
    return false;             // sort non-partsels up high

  const NUVarselRvalue *vb = FoldI::stripVarsel (b, mOp);
  if (!vb)
    return true;

  return AssocOrder::operator()(va, vb);
}

/*!
 * Is the operator \a opcode the algebraic opposite of the operator in
 * \a e - (e.g. for eBiPlus, is e's operator eUnMinus?
 */
static bool sComplementOp (NUOp::OpT opcode, const NUExpr* e,
                           NUOp::OpT* comp=0)
{
  const NUUnaryOp *u = dynamic_cast<const NUUnaryOp *>(e);
  if (not u)
    return false;

  NUOp::OpT other = u->getOp ();

  switch(other) {
  case NUOp::eUnMinus:
    if (opcode == NUOp::eBiPlus) {
      if (comp) *comp = other;
      return true;
    }
    return false;

  case NUOp::eUnBitNeg:
  case NUOp::eUnVhdlNot:
    // anything but a*a has a complementValue
    if ( opcode != NUOp::eBiUMult
      && opcode != NUOp::eBiSMult)
    {
      if (comp) *comp = other;
      return true;
    }
    return false;

  default:
    return false;
  }
}

/*!
 *  Return the constant equivalent for a + -a, a ^ ~a, a & ~a, a | ~a, etc.
 * \a opcode - operator being associated over
 * \a comp - complementary operator (e.g. for +, a unary minus, etc.)
 * \a e - expression that has a complement present
 * \a size - size expected for constant values (the getBitSize for whole associative expression.
 *
 */
const NUExpr* FoldI::complementValue (NUOp::OpT opcode, NUOp::OpT comp,
                                      const NUExpr*e, UInt32 size)
{
  bool sign = e->isSignedResult ();

  switch (opcode) {
  case NUOp::eBiPlus:
    if (comp == NUOp::eUnMinus)
      return genFalse (e->getLoc (), size, sign); // a + -a => 0
    else
      return genOnes (e->getLoc (), size, sign); // a + ~a => -1

  case NUOp::eBiBitAnd:
  case NUOp::eBiLogAnd:
    return genFalse (e->getLoc (), size, sign); // a & ~a => 0
    
  case NUOp::eBiBitXor:
    if (comp == opcode)
      return genFalse (e->getLoc (), size, sign); // a ^ a => 0
    // fall-thru

  case NUOp::eBiBitOr:
  case NUOp::eBiLogOr:
    return genOnes (e->getLoc (), size, sign); // a ^ ~a => -1, a | ~a => -1

  default:
    NU_ASSERT ("Unexpected opcode" == 0, e);
    return NULL; // return e, but that violates const-ness
  }
}

// Test that this is (a) + (-a) or (-a) + (a), or (a) ^ (a)
bool FoldI::isComplement (NUOp::OpT opcode, const NUExpr* a, const NUExpr* b,
                          NUOp::OpT* comp)
{
  if (const NUUnaryOp *ua = dynamic_cast<const NUUnaryOp*>(a))
    return (AssocOrder (opcode).compare (ua->getArg (0), b) == 0
            && sComplementOp (opcode, a, comp)); // (-b) + b
  else if (const NUUnaryOp *ub = dynamic_cast<const NUUnaryOp*>(b))
    return (AssocOrder (opcode).compare (a,ub->getArg (0)) == 0
            && sComplementOp (opcode, b, comp)); // a + (-a)
  else if (opcode == NUOp::eBiBitXor
           && a->equal (*b))
  {
    if (comp)
      *comp = opcode;
    return true;
  }

  return false;
}

// Look for related relational expressions and return collapsed term
// if result pointer provided.
//
//  e.g.   (a < b) | (a > b) => (a != b)
//
// Looking for relational expressions \arg A and \arg B, connected by operator \arg OPCODE.
//
// If \arg RESULT points to a NUExpr*, then return a new NUExpr that is equivalent to
// the optimized result.
//
// Returns FALSE if no optimization is possible, or TRUE if the expression <a> <opcode> <b>
// could be simplified.
bool FoldI::isCollapsibleRelop (NUOp::OpT opcode, const NUExpr* a,
                                const NUExpr * b, const NUExpr**result)
{
  const NUBinaryOp* bina = dynamic_cast<const NUBinaryOp*>(a);
  const NUBinaryOp* binb = dynamic_cast<const NUBinaryOp*>(b);

  if (not bina or not binb)
    return false;               // Not both binary operators.

  NUOp::OpT opa = bina->getOp (),
    opb = binb->getOp ();

  if (not FoldI::isRelationalOp (opa) or not FoldI::isRelationalOp (opb))
    return false;               // Not relationals

  if (*(bina->getArg (0)) != *(binb->getArg (0))
      || *(bina->getArg (1)) != *(binb->getArg (1)))
    return false;               // Not testing same operands in both terms

  if (opa > opb)
    std::swap (opa, opb);       // Simplify the search space

  CopyContext cc (0,0);

  // Combine a and b into a single compare using relational operator opc
#define MERGE(opc) do {if (result) *result = binaryOp(NUOp::opc, \
         bina->getArg(0), bina->getArg(1), bina->getLoc());} while(0); \
         return true

  // Result of the compare is always true
#define ISTRUE do {if (result) *result = FoldI::genOnes(a->getLoc(),1, false);} while(0); \
        return true

  // Result of the compare is always false
#define ISFALSE do {if (result) *result = FoldI::genFalse(a->getLoc()); } while(0); \
        return true

  switch (opcode) {
  case NUOp::eBiBitOr:
  case NUOp::eBiLogOr:
    switch (opa) {
    case NUOp::eBiEq:
      switch (opb) {
      case NUOp::eBiNeq:   ISTRUE;
      case NUOp::eBiSLt:   MERGE (eBiSLte);
      case NUOp::eBiSLte:  MERGE (eBiSLte);
      case NUOp::eBiSGtr:  MERGE (eBiSGtre);
      case NUOp::eBiSGtre: MERGE (eBiSGtre);
      case NUOp::eBiULt:   MERGE (eBiULte);
      case NUOp::eBiULte:  MERGE (eBiULte);
      case NUOp::eBiUGtr:  MERGE (eBiUGtre);
      case NUOp::eBiUGtre: MERGE(eBiUGtre);
      default:		   return false;
      }

    case NUOp::eBiNeq:
      switch (opb) {
      case NUOp::eBiSLt:   MERGE (eBiNeq);
      case NUOp::eBiSLte:  ISTRUE;
      case NUOp::eBiSGtr:  MERGE (eBiNeq);
      case NUOp::eBiSGtre: ISTRUE;
      case NUOp::eBiULt:   MERGE (eBiNeq);
      case NUOp::eBiULte:  ISTRUE;
      case NUOp::eBiUGtr:  MERGE (eBiNeq);
      case NUOp::eBiUGtre: ISTRUE;
      default:	           return false;
      }

    case NUOp::eBiSLt:
      switch (opb) {
      case NUOp::eBiSLte:  MERGE (eBiSLte);
      case NUOp::eBiSGtr:  MERGE (eBiNeq);
      case NUOp::eBiSGtre: ISTRUE;
      default:		   return false;
      }

    case NUOp::eBiSLte:
      switch(opb) {
      case NUOp::eBiSGtr:  ISTRUE;
      case NUOp::eBiSGtre: ISTRUE;
      default:		   return false;
      }

    case NUOp::eBiSGtr:
      switch (opb) {
      case NUOp::eBiSGtre: MERGE (eBiSGtre);
      default:             return false;
      }

    case NUOp::eBiSGtre:
      return false;

    case NUOp::eBiULt:
      switch (opb) {
      case NUOp::eBiULte:  MERGE (eBiULte);
      case NUOp::eBiUGtr:  MERGE (eBiNeq);
      case NUOp::eBiUGtre: ISTRUE;
      default:		   return false;
      }

    case NUOp::eBiULte:
      switch (opb) {
      case NUOp::eBiUGtr:  ISTRUE;
      case NUOp::eBiUGtre: ISTRUE;
      default:		   return false;
      }

    case NUOp::eBiUGtr:
      switch (opb) {
      case NUOp::eBiUGtre: MERGE (eBiUGtre);
      default: 		   return false;
      }

    case NUOp::eBiUGtre:
      return false;

    default:
      return false;
    }
    return false;


  case NUOp::eBiBitAnd:
  case NUOp::eBiLogAnd:
    switch (opa) {
    case NUOp::eBiEq:
      switch (opb) {
      case NUOp::eBiNeq:   ISFALSE;
      case NUOp::eBiSLt:   ISFALSE;
      case NUOp::eBiSLte:  MERGE (eBiEq);
      case NUOp::eBiSGtr:  ISFALSE;
      case NUOp::eBiSGtre: MERGE (eBiEq);
      case NUOp::eBiULt:   ISFALSE;
      case NUOp::eBiULte:  MERGE (eBiEq);
      case NUOp::eBiUGtr:  ISFALSE;
      case NUOp::eBiUGtre: MERGE(eBiEq);
      default:		   return false;
      }

    case NUOp::eBiNeq:
      switch (opb) {
      case NUOp::eBiSLt:   MERGE (eBiSLt);
      case NUOp::eBiSLte:  MERGE (eBiSLt);
      case NUOp::eBiSGtr:  MERGE (eBiSGtr);
      case NUOp::eBiSGtre: MERGE (eBiSGtr);
      case NUOp::eBiULt:   MERGE (eBiULt);
      case NUOp::eBiULte:  MERGE (eBiULt);
      case NUOp::eBiUGtr:  MERGE (eBiUGtr);
      case NUOp::eBiUGtre: MERGE (eBiUGtr);
      default:		   return false;
      }

    case NUOp::eBiSLt:
      switch (opb) {
      case NUOp::eBiSLte:  MERGE (eBiSLt);
      case NUOp::eBiSGtr:  ISFALSE;
      case NUOp::eBiSGtre: ISFALSE;
      default:		   return false;
      }

    case NUOp::eBiSLte:
      switch(opb) {
      case NUOp::eBiSGtr:  ISFALSE;
      case NUOp::eBiSGtre: MERGE (eBiEq);
      default:		   return false;
      }

    case NUOp::eBiSGtr:
      switch (opb) {
      case NUOp::eBiSGtre: MERGE (eBiSGtr);
      default:             return false;
      }

    case NUOp::eBiSGtre:
      return false;

    case NUOp::eBiULt:
      switch (opb) {
      case NUOp::eBiULte:  MERGE (eBiULt);
      case NUOp::eBiUGtr:  ISFALSE;
      case NUOp::eBiUGtre: ISFALSE;
      default:		   return false;
      }

    case NUOp::eBiULte:
      switch (opb) {
      case NUOp::eBiUGtr:  ISFALSE;
      case NUOp::eBiUGtre: MERGE (eBiEq);
      default:		   return false;
      }

    case NUOp::eBiUGtr:
      switch (opb) {
      case NUOp::eBiUGtre: MERGE (eBiUGtr);
      default: 		   return false;
      }

    case NUOp::eBiUGtre:
      return false;
      
    default:
      return false;
    }

    return false;
    break;

  default:
    return false;
  }

  return false;
#undef ISTRUE
#undef ISFALSE
#undef MERGE
}


bool FoldI::scalarValue(NUOp::OpT oldOp, const NUExpr* e)
{
  // Be careful about the context that terms appear in.  We don't
  // want to confuse multibit operations with bit-reduction operations.
  switch(oldOp) {
  case NUOp::eBiPlus:         // Can do POPCOUNT
    if (getCachedEffectiveBitSize(e) != 1)
      return false;
    break;

  case NUOp::eBiBitXor:
  case NUOp::eBiBitAnd:
  case NUOp::eBiBitOr:
  case NUOp::eBiLogAnd:
    // if we are doing bitwise operations, we are ONLY happy with single bit reductions
    // e.g. a[4:2] ^ a[1:0]  is very different from ^a[4:0]
    if (e->getBitSize () != 1)
      return false;
    break;

  case NUOp::eBiLogOr:
    // no constraints on field width because we only care if ANY of the bits are set
    break;

  default:
    return false;
  }

  // Look for ~&varsel which we don't handle
  if (const NUUnaryOp *uop = dynamic_cast<const NUUnaryOp*>(e)) {
    if (FoldI::isComplement (uop)) {
      if ((uop = dynamic_cast<const NUUnaryOp*>(uop->getArg (0)))) {
        return false;         // ~ &varsel  -- don't handle this
      } else {
        return true;          // ~ varsel
      }
    }
  }

  return true;
} // bool FoldI::scalarValue

bool FoldI::sameVarselIdent(NUOp::OpT redOp, const NUVarselRvalue* varsel, const NUExpr* e) const
{
  const NUVarselRvalue *v = FoldI::stripVarsel(e, redOp);
  if (not v
      || not v->getIndex()->equal(*(varsel->getIndex()))
      || not v->getIdentExpr()->equal((*varsel->getIdentExpr()))) {
    // Not at a knowable offset from canonical net
    return false;
  }
  return true;
}

// Return underlying varsel (or NULL) for a varsel, reduction of a varsel, 
// comparison of varsel to constant or a complemented varsel
//
const NUVarselRvalue* FoldI::stripVarsel (const NUExpr* expr, NUOp::OpT matchOp)
{
  switch (expr->getType ()) {
  case NUExpr::eNUVarselRvalue:
    return dynamic_cast<const NUVarselRvalue*>(expr);

  case NUExpr::eNUUnaryOp:
  {
    const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(expr);
    if (isComplement (uop))
      expr = uop->getArg (0);
    else if (uop->getOp () == matchOp)
      expr = uop->getArg (0);
    else
      return 0;
    break;
  }

  case NUExpr::eNUBinaryOp:
  {
    const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(expr);
    // Think of x[5:0] != 0  => |x[5:0]
    //          x[5:0] == 63 => &x[5:0]
    //          (x[5:0] == 2) && (x[10:6] == 1) => (x[10:0] == 0x42) 
    NUOp::OpT opcode = bop->getOp ();
    const NUConst* c = bop->getArg (1)->castConst ();

    if (not c)
      return 0;                 // Nope
    else if ((opcode == NUOp::eBiNeq && c->isZero ()
              && matchOp == NUOp::eUnRedOr)
             || (opcode == NUOp::eBiEq
                 && matchOp == NUOp::eUnRedAnd))
      expr = bop->getArg (0);
    else
      return 0;
    break;
  }
  case NUExpr::eNUConcatOp:
  {
    const NUConcatOp * cop = dynamic_cast<const NUConcatOp*>(expr);
    if ( (cop->getRepeatCount()==1) and (cop->getNumArgs() == 2) ) {
      const NUConst* carg1 = cop->getArg(0)->castConst ();
      if ( carg1 and carg1->isZero() ){
        expr = cop->getArg(1);
      } else {
        return 0;
      }
    } else {
      return 0;
    }
    break;
  }



  default:
    return 0;
  }

  // Check that what we end up with really is a NUVarsel...
  if (expr && expr->getType () == NUExpr::eNUVarselRvalue)
    return dynamic_cast<const NUVarselRvalue*>(expr);
  else
    return 0;
}

/*!
 * Does \a terms represent a dense set of fields that can be converted into a
 * reduction operation?  The iterator bounds a sequence of terms being operated on
 * by operator \a opcode.
 *
 * The terms can involve MULTIPLE primary nets, constants, etc. and we want to
 * collapse appropriate subsequences, viz:
 *
 *      a[0] & a[1] & b[0] & b[1] & c
 *
 * should fold down to
 *
 *      &a[1:0] & &b[1:0] & c
 */
const NUExpr*
FoldI::denseSelection (const NUExpr* rootExpr, NUOp::OpT opcode,
                       NUCExprVector::iterator beg, NUCExprVector::iterator end)
{
  // Figure out the reduction operator to be used to optimize the
  // expression.
  bool andBits = false;
  NUOp::OpT redOp;
  switch (opcode) {
  case NUOp::eBiBitAnd:
  case NUOp::eBiLogAnd:
    andBits = true;
    redOp = NUOp::eUnRedAnd;
    break;

  case NUOp::eBiBitOr:
  case NUOp::eBiLogOr:
    redOp = NUOp::eUnRedOr;
    break;

  case NUOp::eBiBitXor:
    redOp = NUOp::eUnRedXor;
    break;

  case NUOp::eBiPlus:
    redOp = NUOp::eUnCount;
    break;

  default:
    return 0;
  }


  // Because we are going to allow messy things like:
  //    a[0] || a[2:1]!=0 || a[3]
  //
  // the naive sort algorithm doesn't handle that.  Reorder the terms so that
  // the partselects have moved to the front of the vector
  //
  // This also sorts the set of expressions into groups such that all
  // expressions with the same ident are next to each other.
  //
  std::stable_sort (beg, end, DenseOrder (redOp));


  NUCExprVector collapsed;       // Terms we've collapsed go here
  NUCExprVector preserved;       // Terms we've been unable to collapse go here
  NUCExprVector optExprs;        // Place to grab expressions that we can optimize

  // We set this to true so that we can lazily copy the remaining items
  // to the 'collapsed' vector if it is needed, following the discovery
  // of a non-partselect in the following loop
  bool copy_to_collapsed = false;
  NUCExprVector::iterator denseBegin = beg;

  // Search thru the terms, finding subsequences of related terms and collapsing them
  // into more efficient representation.
  while (denseBegin != end) {
    // Candidate first term in the sub-sequence
    const NUExpr* firstExpr = *denseBegin;
    const NUVarselRvalue* firstVarsel = stripVarsel(firstExpr, redOp);    
    if (not firstVarsel) {
      copy_to_collapsed = true;
      break;
    }
      
    // Start by adding the first expression to the to be optimized
    // vector if it is a valid scalar.
    optExprs.clear();
    if (scalarValue(opcode, firstExpr)) {
      // Optimizable
      optExprs.push_back(firstExpr);
    } else {
      // Not optimizable but continue in case there are others that
      // are in the same group
      preserved.push_back(firstExpr);
    }

    // Walk the remaining elements and gather varsel expressions to
    // optimize. The array is in sorted order such that expressions
    // with the same ident are next to each other. So we just have to
    // walk until we hit one that isn't the same ident and validate
    // that they are scalars.
    NUCExprVector::iterator denseEnd = denseBegin + 1;
    bool cont = true;
    while ((denseEnd != end) && cont) {
      const NUExpr* nextExpr = *denseEnd;
      cont = sameVarselIdent(redOp, firstVarsel, nextExpr);
      if (cont) {
        // We still have the same ident but it may not be optimizable
        if (scalarValue(opcode, nextExpr)) {
          // It can be optimized
          optExprs.push_back(nextExpr);
        } else {
          // Same ident but not optimizable
          preserved.push_back(nextExpr);
        }

        // This expression is the same ident so keep looking for the
        // end of the group.
        ++denseEnd;
      }
    }

    // Possibly optimize the expressions. We only do this if we have
    // at least 2 expressions to combine into a reduction operation.
    if (optExprs.size() > 1) {
      const NUExpr* collapsedTerm;
      collapsedTerm = mergeDenseSelection(rootExpr, redOp, andBits, optExprs.begin(), optExprs.end());
      if (collapsedTerm) {
        // The range was collapsed into a single term, yeah!
        collapsed.push_back (collapsedTerm);

      } else {
        // Failed to merge this subsequence, just copy it into the preserved
        // container and wait to see if we manage to actually collapse some other
        // sub-sequences.
        std::copy (optExprs.begin(), optExprs.end(), std::back_inserter (preserved));
      }

    } else if (!optExprs.empty()) {
      // Singleton, copy it to the preserved set.
      std::copy (optExprs.begin(), optExprs.end(), std::back_inserter (preserved));
    }
    
    // skip to end of the match sequence (aka beginning of NEXT candidate group
    // of related expressions
    denseBegin = denseEnd;
  }

  if (collapsed.empty ())
    // We didn't succeed in collapsing ANYTHING, so this attempt to optimize
    // was a failure.  Return an indication that nothing changed.
    return NULL;

  // Make a copy of each of the preserved terms for the new expression
  for(NUCExprVector::iterator p = preserved.begin (); p != preserved.end (); ++p)
    collapsed.push_back (copyExpr(*p));

  if (copy_to_collapsed) {
    // remaining terms aren't partselects and can't be collapsed.  Copy what's
    // left to our result and stop searching for more dense terms.
    for (NUCExprVector::iterator p = denseBegin; p != end; ++p) {
      collapsed.push_back (copyExpr(*p));
    }      
  }

  // reinflate the remaining terms using the opcode
  //
  NUCExprVector::iterator cbeg = collapsed.begin ();
  const NUExpr* result = manage(*cbeg, false);
  const SourceLocator& loc = result->getLoc ();
  UInt32 size = result->determineBitSize();
  for ( ++cbeg; cbeg != collapsed.end (); ++cbeg) {
    const NUExpr* expr = manage(*cbeg, false);
    result = binaryOp (opcode, result, expr, loc, size);
  }
  return result;
} // FoldI::denseSelection


// Given a group of related terms, construct a more efficent evaluation
const NUExpr*
FoldI::mergeDenseSelection (const NUExpr* rootExpr, NUOp::OpT redOp, bool andBits, 
                            NUCExprVector::iterator beg, NUCExprVector::iterator end)
{
  //
  // check if it's all partselects that can be collapsed
  // 

  const NUVarselRvalue* first = stripVarsel (*(beg), redOp);

  UInt32 termCount = (end - beg);

  // Look for things like:
  //  a[0] & !a[1] & a[4]    =>  (a & 0x13) ^ 0x11 == 0
  //  a[0] | a[1] | !a[2]    =>  (a[2:0]) ^ 4         != 0
  //  a[0] + a[1] + a[2]     => popcount(a[2:0])

  const ConstantRange& firstRange (*(first->getRange ()));
  SInt32 firstBitOff = firstRange.getLsb ();

  // Because we could have something like:
  //            &x[0+:3] &x[3+:4]  & x[4]     (redundant term)
  // we have to search thru all the terms to find the maximum
  SInt32 lastBitOff = firstRange.getMsb (); // at least this big...
  ConstantRange denseRange (lastBitOff, firstBitOff);

  // Find the range of bits we're looking at and make certain
  // we can reduce correctly.  Consider that (x[0] + x[0]) != POPCOUNT(x[0]),
  // (x[2] | !x[2]) != (|((x & 4) ^ 4)), (x[2] | x[2]) == (|((x & 4))),
  // (x[2] & !x[2]) != (&((x & 4) ^ 0xffffffff)), (x[2] & x[2]) == (&((x & 4) ^ 0xfffffffb)),
  // In general this code can't handle and/or operations if the range of operands
  // overlap and if any of the overlapping operands are complemented. - bug6622.
  
  const NUVarselRvalue*last = NULL;
  NUCExprVector::const_iterator second = beg;
  bool hasComplOperand = isComplement (dynamic_cast<const NUUnaryOp*>(*second));
  ++second;
  hasComplOperand |= isComplement (dynamic_cast<const NUUnaryOp*>(*second));

  for(NUCExprVector::const_iterator i = second; i!=end; ++i)
  {
    last = stripVarsel (*i, redOp);
    const ConstantRange& nextRange (*(last->getRange ()));
    if ( ( redOp == NUOp::eUnCount || redOp == NUOp::eUnRedXor ||
           ( hasComplOperand && 
             ((redOp == NUOp::eUnRedOr) || (redOp == NUOp::eUnRedAnd)) ) )
         && denseRange.overlaps (nextRange) )
      return 0;
    
    lastBitOff = std::max (lastBitOff, nextRange.getMsb ());
    denseRange.setMsb (lastBitOff);
  }

  UInt32 width = lastBitOff + 1 - firstBitOff;

  // Before we finish schedule, if we create partselects with masked
  // gaps, that fools UD into thinking there are uses of the masked
  // bits.  So only do sparse fields when we're allowed to be
  // aggressive.
  //

  if ( width != termCount ) {
    // Not a dense range of bits
    if (isAggressive ()) {
      // maximize aligned word operations by reducing the lowerbound to an aligned word bound
      // and the upper-bound to the last bit in the MSB referenced bit
      firstBitOff &= ~0x1f;     // round-down to nearest word
      lastBitOff |= 7;          // round-up to nearest byte.
      width = lastBitOff + 1 - firstBitOff;

      // bug 6748: Don't create huge bit-vector masks to replace two bit-tests.
      // More specifically, for 64-bit nets, we can allow spans up to 64.
      // For all other nets, we only allow spans of just a single 32-bit
      // integer.  E.g. bv256[31] & bv256[33] is not a good candidate to
      // transform into a mask.
      UInt32 ident_width = last->getIdentExpr()->getBitSize();
      if (ident_width <= 64) {
        if (width > 64) {
          // Probably can't reach this statement
          return 0;
        }
      }
      else {
        // We have a 32-bit value or a bitvector.  Either way, we don't
        // want to span UInt32s.  Based on the POGO perf tests, it appears
        // that acessing adjacent UInt32s is OK -- we construct a UInt64
        // from a BV in the generated code.  That is probably a good idea
        // on little-endian machines.  I'm dubious about that idea on
        // big-endian machines, where the words would have to be shuffled
        // to create a UInt64 from a UInt32-array. 
        SInt32 wordOne = firstBitOff / 32;
        SInt32 wordTwo = lastBitOff / 32;
        if (std::abs(wordOne - wordTwo) > 1) {
          return 0;
        }
      }

      // Note that we let bv[31]&bv[32] get combined into &bv[32:31].  I
      // looked at the generated C++ for this and it might be OK to leave
      // taht as is:
      // get_out3() = ((((((((m_bv._M_w[0])) >> 31) +
      //                  (((m_bv._M_w[1])) << 1)))
      //                &CarbonUInt2(0x3U))
      //               == (CarbonUInt2)MASK(CarbonUInt32,2)));
      // The reason we will let this be combined is that width==termCount
      // so we won't get into this code for that expression.  This
      // is tested in test/fold/big_relop.v
    } else {
      // when UD and bit-level flow matters, we don't want to do this sort of bit-mashing
      return 0;
    }
  } // if
      
  ConstantRange piece (lastBitOff, firstBitOff);
  const SourceLocator &loc = first->getLoc ();

  DynBitVector complementMask (width);
  DynBitVector ignoreMask (width);
  ignoreMask.set ();            // assume we have NO bits usable in the range


  for(NUCExprVector::const_iterator i = beg; i != end; ++i)
  {
    // strip any complement operators, record them in the complementMask
    const NUExpr* aBit = *i;
    const NUVarselRvalue *bitsel = stripVarsel (aBit, redOp);
    
    const ConstantRange &newPiece = *(bitsel->getRange ());
    NU_ASSERT (piece.contains (newPiece), aBit);

    ignoreMask.setRange (newPiece.getLsb () - firstBitOff,
                         newPiece.getLength (), 0);

    // Remember if this bitfield was complemented or compared against some random constant
    if (aBit != bitsel) {
      if (isComplement (dynamic_cast<const NUUnaryOp*>(aBit)))
        complementMask.setRange (newPiece.getLsb () - firstBitOff,
                                 newPiece.getLength (),
                                 1);
      else if (const NUBinaryOp* aBop = dynamic_cast<const NUBinaryOp*>(aBit))
        if (aBop->getOp () == NUOp::eBiEq) {
          const NUConst* c = aBop->getArg (1)->castConst ();
          // For x[p:s] == c, we want to complement the zero bits and directly compare the rest
          DynBitVector eqValue (newPiece.getLength ());
          c->getSignedValue (&eqValue);
          eqValue.flip ();      // invert to get bits we must complement for reduction
          complementMask.lpartsel (newPiece.getLsb () - firstBitOff,
                                   newPiece.getLength ())
            = eqValue;
        }
    }
  }


  // If the ignoreMask is all zeroes, then all the bits in the range
  // were referenced and we don't need it.  Otherwise mask out any set
  // bits because they don't participate in the reduction operation.
  //
  // If we're AND-ing bits, we force them set so that the
  // reduction-and correctly ignores them too.  If we were OR-ing
  // bits, then leave them cleared out.
  //
  // Create a partselect from first:last and then walk thru the terms
  // figuring out which bits are complemented and which are ignored
  // and build the mask
  //
  const NUExpr* partsel = varsel(first->getIdentExpr (), first->getIndex (),
                                 piece, loc, width);

  const NUExpr* result = partsel;
  if (ignoreMask.any ()) {
    if (andBits)
      complementMask |= ignoreMask; // turn on the ignored bits in the complement

    ignoreMask.flip ();         // change to and-able mask

    result = binaryOp (NUOp::eBiBitAnd, partsel,
                       constant (false, ignoreMask, width, loc), loc,
                       width);
  }
    
  // if the complement mask is non-zero, we need to apply it
  if (complementMask.any ())
    result = binaryOp (NUOp::eBiBitXor, result,
                       constant (false, complementMask, width, loc), loc);

  result = unaryOp (redOp, result, result->getLoc (), rootExpr->getBitSize ());
  result = mExprCache->makeSizeExplicit(result);

#if 0
  // Special dump code..
  UtString buf ("DenseSelection :");
  for (NUCExprVector::const_iterator i = beg; i != end; ++i){
    (*i)->compose (&buf, NULL);
    buf << "\t\t";
  }
  buf << "\nBecomes:\t";
  result->compose (&buf, NULL);
  UtIO::cout () << buf << "\n\n";
#endif
  
  return result;
}

// Check if both arguments are suitable for concat-repeatcount factoring and
// return coerced pointers to suitable operands.
//
static bool sIsParallelConcat (const NUExpr* a, const NUExpr* b,
                               const NUConcatOp** rca=0, const NUConcatOp** rcb=0)
{
  if (a->getType () != NUExpr::eNUConcatOp
      || b->getType () != NUExpr::eNUConcatOp)
    return false;

  const NUConcatOp *ca = dynamic_cast<const NUConcatOp*>(a);
  const NUConcatOp *cb = dynamic_cast<const NUConcatOp*>(b);

  UInt32 count = ca->getRepeatCount ();

  if (count == 1 || count != cb->getRepeatCount ()
      || ca->getNumArgs () != 1 || cb->getNumArgs () != 1)
    return false;

  if (rca)
    *rca = ca;
  if (rcb)
    *rcb = cb;
  return true;
}

// Change {K{a}} op {K{b}} => {K{{a} op {b}}}
const NUExpr* FoldI::factorParallelConcat (const NUConcatOp* ca,
                                           const NUConcatOp*cb,
                                           NUOp::OpT opcode)
{
  UInt32 k = ca->getRepeatCount ();

  NUCExprVector v (1);
  v[0] = copyExpr (ca->getArg (0));
  const NUExpr* newa = concatOp (v, 1, ca->getLoc (), 0); // self-determined size OK

  v[0] = copyExpr (cb->getArg (0));
  const NUExpr* newb = concatOp (v, 1, cb->getLoc (), 0); // self-determined size OK

  v[0] = binaryOp (opcode, newa, newb, ca->getLoc ());
  return concatOp (v, k, ca->getLoc (), ca->getBitSize());
}

// Are the terms all candidates for conversion to bitwise logical operators?
static bool sIsLogicalToBitwise (NUOp::OpT* opcode, NUCExprVector& terms)
{
  NUCost cost;
  NUCostContext ctx;

  NUOp::OpT newOp = *opcode;
  if (newOp == NUOp::eBiLogAnd)
    newOp = NUOp::eBiBitAnd;
  else if (newOp == NUOp::eBiLogOr)
    newOp = NUOp::eBiBitOr;
  else
    return false;

  for (NUCExprVector::const_iterator i = terms.begin (); i!=terms.end (); ++i) {
    if ((*i)->getBitSize () != 1 || (*i)->isSignedResult ())
      // Need logical operator to force boolean result, can't do as bitwise
      // converting from && to & is only valid if the operands are
      // forced to be unsigned (because they might be resized later)
      // (test/langcov/Signed/binary_F_K_10_1_1_1.v)
      return false;

    (*i)->calcCost(&cost, &ctx);

    // Cost of 8 instructions chosen by experimentation by Joe.  But 
    // after transforming regardless of cost, running performance
    // tests shows that we are on average 3% faster across ALL the tests and only one test
    // was more than 4% worse, while three tests were 9 to 21% better!
    // Boosted to 64 to avoid pathological cases. [APL]
    //
    if (cost.mInstructions > 64) {
      // converting from && to & is only valid if the operands are
      // forced to be unsigned (because they might be resized later)
      // (test/langcov/Signed/binary_F_K_10_1_1_1.v)
      return false;
    }
  }

  *opcode = newOp;
  return true;
}

/*
 * Convert an expression involving an associative operator into canonical form,
 * such that:
 * (1) All terms are in a canonical sorted order, with unary minus and unary bitwise negate
 *     ignored so that a and -a sort adjacent
 * (2) All terms of the form (a + -a) have been removed
 * (3) There is no more than one NUConstNoXZ term.  [Note that X's and Z's are
 *     not collapsed by this transformation
 *
 */

const NUExpr*
FoldI::reassociate (const NUBinaryOp *e)
{
  if (not ignoreXZ ()           // Don't reassociate expressions with 'x' prematurely
      || isBottomUp ()          // Don't collapse from bottom up
      || mAssocOrderSet->find (e) != mAssocOrderSet->end () // We already messed with this
    )
    return 0;
      
  mAssocOrderSet->insert(e);
  NUOp::OpT opcode = e->getOp ();

  if (not isAssociative (opcode))
    return 0;

  NUCExprVector terms; // A vector of terms joined by an associative operator

  // Collect all the terms rooted at this point
  if (not group (opcode, e, &terms)
      || (terms.size () < 2))
    // terms are not compatible
    return 0;


  // If the terms are already in canonical order and we don't see any
  // simplifications, then we don't need to reassociate this tree.
  //
  AssocOrder cmp (opcode);      // A sorting object for this associative group
  bool unOrdered = false;       // Need to transform if out-of-order

  for(NUCExprVector::iterator i = terms.begin ();
      not unOrdered && (i != terms.end ());
      ++i)
  {
    NUCExprVector::iterator next = i;
    ++next;
    if (next == terms.end ())
      break;


    if (cmp.compare(*i, *next) > 0)
      unOrdered = true;         // need to reorder, so we are going to transform
  }

  // Now we want to sort the structure and determine if sorting
  // changed the order of the terms?  (if true, then we know we want
  // to transform the tree into a canonical form.)
  //

  if (unOrdered)
  {
    // Sort the tree into canonical order - we will emit a new tree in
    // canonical order.
    std::stable_sort (terms.begin (), terms.end (), cmp);
  }

  // Otherwise, we may still want to transform the tree if we find
  // MULTIPLE constant terms or we find (a) and (-a) in the list and
  // optimize them.
  //
  bool collapsible = false;

  for (NUCExprVector::iterator i = terms.begin ();
       not collapsible && (i != terms.end ());
       ++i)
  {
    NUCExprVector::iterator next = i;
    ++next;
    if (next == terms.end ())
      break;

    collapsible |= isComplement (opcode, *i, *next) // (a) + (-a)
      || isCollapsibleRelop (opcode, *i, *next) // (x < 0) | (x == 0)
      || (isBitwiseOp (opcode) && 
          ((*i)->equal (**next)			// (a) & (a)
           || sIsParallelConcat (*i, *next)))   // {k{a}} & {k{b}}
      || (ctce(*i) && ctce(*next)); // two constant terms.
  }

  // If we were doing && or ||, this is a convenient time to transform all the terms
  // at once without introducing a cascade of copying
  bool doLogicalToBitwise = sIsLogicalToBitwise (&opcode, terms);

  if (not collapsible and not unOrdered) {
    // No re-associations - try merging
    const NUExpr* result = denseSelection (e, opcode, terms.begin (), terms.end ());
    if (result)
      return result;
    else if (not doLogicalToBitwise)
      return NULL;
  }

  // We're either reordering or we found expressions that could be
  // simplified, or we want to change logical relationals to bitwise.
  //
  // Now run thru the vector building a NEW expression tree
  // to replace the existing one.
  //

  // Start at the last terms - if there are any constants, this is
  // where they've all sorted to.  Collect and collapse them into a
  // single constant value.
  //
  UInt32 computeSize = e->getBitSize ();
  NUCExprVector::reverse_iterator i = terms.rbegin ();
  const NUExpr* constantTerm = NULL;
  const NUExpr* firstTerm = (*i);
  const NUExpr* result = NULL;
  SourceLocator loc = firstTerm->getLoc();

  if (ctce (firstTerm)) {
    ++i;                        // slurp the term
    result = firstTerm;

    // Collapse adjacent constants via construction and folding
    for(; i!= terms.rend () && ctce(*i); ++i) {
      const NUExpr* expr = *i;
      result = binaryOp(opcode, copyExpr(expr), result, loc, computeSize);
      foldExprTree(&result);
      mAssocOrderSet->insert(result);
      NU_ASSERT (result->castConst (), result);
    }

    if (i == terms.rend ())
      return manage(result, false);      // All folded away to single constant

    constantTerm = result;      // Put constant term aside for later
  }

  // We've reduced the number of constant terms to at most one, and
  // left the iterator i pointing to the rightmost element that we still need to
  // process.
  result = *i;
  ++i;
  // Now combine the last term with the 'result', handling the  permutations
  // so that we convert
  //     ....  a + (-a) + result => 0 + result
  //     ....  b + (-a) + a => b + 0
  //     ....  b + a + result = (b + (a + result)
  //
  const NUExpr* rterm = NULL;
  
  while(i != terms.rend ())
  {
    const NUConcatOp* ca = NULL, *cb = NULL;
    const NUExpr* term = *i;
    ++i;
    bool skip = false;

    NUOp::OpT compOp;           // Complement operator (- or ~)
    if (isComplement (opcode, term, result, &compOp)) { // result + - result => 0
      cleanupExpr (result);
      rterm = complementValue (opcode, compOp, term, computeSize);
    }
    else if (i != terms.rend () && isComplement (opcode, *i, term, &compOp))
    {
      // combine (a + -a) + result => 0 + result
      rterm = complementValue (opcode, compOp, term, computeSize);
      ++i; 
                    
      rterm = binaryOp (opcode, result, rterm, loc);
    }
    else if ((opcode == NUOp::eBiBitAnd || opcode == NUOp::eBiBitOr
               || opcode == NUOp::eBiLogAnd || opcode == NUOp::eBiLogOr)
             && (term->equal (*result)
                 || (i != terms.rend () && term->equal (**i))))
      // a | a => a, a & a => a
      // just ignore term, it will go away
    {
      skip = true;
    }
    else if ((opcode == NUOp::eBiBitXor) && term->equal (*result)) {
      //  a ^ a
      // replace everything to date with a zero
      cleanupExpr (result);
      rterm = genFalse (e->getLoc (), computeSize, term->isSignedResult ());
    } else if (i != terms.rend () && opcode == NUOp::eBiBitXor
               && term->equal (**i))
    {
      // a ^ a ^ result => skip TWO operands
      ++i;
      skip = true;
    }
    else if (i != terms.rend () && isCollapsibleRelop (opcode, term, *i, &rterm)) {
      // rela & relb => relc
      // rela | relb => relc
      ++i;                      // skip next term and use collapsed term
      rterm = binaryOp (opcode, rterm, result, loc);
    } else if (isCollapsibleRelop (opcode, result, term, &rterm)) {
      // Just skip this term and replace result by new term
      cleanupExpr (result);
    } else if (isBitwiseOp (opcode) && sIsParallelConcat (term, result, &ca, &cb)) {
      // {k{term}} | {k{result}}
      rterm = factorParallelConcat (ca, cb, opcode);
      cleanupExpr(result);
    } else if (i != terms.rend () && isBitwiseOp (opcode)
             && sIsParallelConcat (term, *i, &ca, &cb)) {
      // {k{a}} | {k{b}} => {k{{a}|{b}}}, when a and b are single terms
      rterm = factorParallelConcat (ca, cb, opcode);
      rterm = binaryOp (opcode, rterm, result, loc);
      ++i;                      // skip 2nd operand
    } else {
      // combine (a + result)
      rterm = binaryOp (opcode, term, result, loc);
    }
    if (!skip) {
      rterm = mExprCache->resize(rterm, computeSize);
      result = manage(rterm, true);
    }
  } // while

  // Lastly combine 'result' with 'constantTerm' at the top so we can
  // easily manipulate expressions like (a+b+c)+5

  if (constantTerm) {
    result = binaryOp (opcode, result, constantTerm, loc, computeSize);
  }

  // At this point, we've re-associated all the terms, leaving things
  // in canonical order with collapsed constants and collapsed
  // complementary terms.

  return manage(result, false);
} // FoldI::reassociate

bool
FoldI::isGoodDistributiveQC (const NUTernaryOp* cond, NUOp::OpT opcode, const NUExpr* expr,
                             const NUExpr**tTerm, const NUExpr**fTerm)
{
  const NUConst *b = ctce (cond->getArg (1));
  const NUConst *c = ctce (cond->getArg (2));
  const NUConst *d = ctce (expr);

  if (const NUTernaryOp* te = dynamic_cast<const NUTernaryOp*>(expr)) {
    // a?b:c <op> a?d:e
    if (te->getOp () == NUOp::eTeCond)
      if (cond->getArg (0)->equal (*(te->getArg (0)))) {
        *tTerm = te->getArg (1);
        *fTerm = te->getArg (2);
        return true;
      }
  }

  if ((b && c && d)      // All constants - trivially safe
      // (a?0:1) op d
      || (b && b->isZero () && c && c->isOnes ()) // Simple masks
      // (a?1:0) op d
      || (b && b->isOnes () && c && c->isZero ()) //  "      "
      // (a?b:c) < d, and folding we are comparing against a constant
      //        we can fold things like: wire [3:0] r;
      //                                 ... (e?r:x) < 16
      //        because r<16 is always true.  This comes up in
      //        elaborating components quite frequently....
      //
      || (isAggressive () && isRelationalOp (opcode) &&
          (d || (b && c))) // Only for non-ExprSynthesis folds
    )
  {
    NUCostContext costCtx;
    NUCost cost;
    expr->calcCost (&cost, &costCtx);
    if (cost.mInstructions > 64
        && not ((opcode == NUOp::eBiBitAnd || opcode == NUOp::eBiBitOr)
                && ((b && b->isZero ()) || (c && c->isZero ()))))
      // Don't try distributing expensive things unless we're doing &| with
      // one leg of zero.
      return false;

    *tTerm = expr;
    *fTerm = expr;
    return true;                           // (b op d) or (c op d) will fold away.
  } else {
    return false;
  }
}


//  (a ? b:c) op d	=> (a ? (b op d) : (c op d))
// note that if (a ? b:c) is signed then  transfor is => (a ? ($signed(b) op d) : ($signed(c) op d))
const NUExpr*
FoldI::distributeQC (const NUTernaryOp* cond, NUOp::OpT opcode, const NUExpr* rop,
                     const SourceLocator& loc)
{
  //  But, this isn't always a win;
  //
  // In particular, avoid folding things like
  //   (a?b:c) | (d?e:f) | (g?h:i) ....
  // because that has exponential growth.  But (a?b:c) | (a?d:e) is a good idea
  // 
  const NUExpr* tExpr, *fExpr;

  if (not isGoodDistributiveQC (cond, opcode, rop, &tExpr, &fExpr))
    return NULL;

  bool signed_ops = cond->isSignedResult ();

  const NUExpr *lt = copyExpr (cond->getArg (1), specifySign(signed_ops));
  const NUExpr *rt = copyExpr (tExpr);

  const NUExpr* newlop = binaryOp (opcode, lt, rt, loc);

  lt = copyExpr (cond->getArg (2), specifySign(signed_ops));
  rt = copyExpr (fExpr);

  const NUExpr* newrop = binaryOp (opcode, lt, rt, loc);
  foldExprTree(&newrop);
  const NUExpr *newcond = cond->getArg (0);
  foldExprTree(&newlop);
  return ternaryOp (newcond, newlop, newrop, loc);
}

// Overloaded - d <op> (a?b:c)  => a?(d<op>b) : (d<op>c);
// note that if (a?b:c) is signed then the transform is => a?(d<op>$signed(b)) : (d<op>$signed(c)); // bug8876/bug12130
const NUExpr* FoldI::distributeQC (const NUExpr* lop, NUOp::OpT opcode,
                                   const NUTernaryOp* cond,
                                   const SourceLocator& loc)
{
  const NUExpr* tExpr, *fExpr;
  if (not isGoodDistributiveQC (cond, opcode, lop, &tExpr, &fExpr))
    return NULL;

  // if the cond expression is signed then during this distribution
  // the signed property must also be distributed
  bool signed_ops = cond->isSignedResult ();

  const NUExpr *lt = copyExpr (tExpr);
  const NUExpr *rt = copyExpr (cond->getArg (1), specifySign(signed_ops));

  const NUExpr* newlop = binaryOp (opcode, lt, rt, loc);

  lt = copyExpr (fExpr);
  rt = copyExpr (cond->getArg (2), specifySign(signed_ops));

  const NUExpr* newrop = binaryOp (opcode, lt, rt, loc);
  const NUExpr *newcond = copyExpr (cond->getArg (0));

  foldExprTree(&newlop);
  foldExprTree(&newrop);
  return ternaryOp (newcond, newlop, newrop, loc);
}
