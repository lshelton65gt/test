/***************************************************************************************
  Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


/*!
  \file
  TBD
*/

#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"

#include "iodb/IODBNucleus.h"
#include "iodb/IODBDesignData.h"
#include "iodb/IODBUserTypes.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUExpr.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"

#include "reduce/TieNets.h"
#include "reduce/RewriteUtil.h"

RETieNets::RETieNets(NUDesign* design, AtomicCache* strCache,
                     IODBNucleus* iodbNucleus, MsgContext* msgContext,
                     Fold* fold, bool verbose, bool tying, NUNetSet* tieNetTemps) :
  mVerbose(verbose),
  mTying(tying), 
  mDesign(design), 
  mStrCache(strCache),
  mIODBNucleus(iodbNucleus), 
  mMsgContext(msgContext), 
  mFold(fold), 
  mTieNetTemps(tieNetTemps)
{
  mConstModuleNets = new ConstModuleNets;
}

RETieNets::~RETieNets()
{
  ASSERT(mConstModuleNets->empty());
  delete mConstModuleNets;
}

void RETieNets::processDirectives(void)
{
  // Iterate over all the tie nets creating a map for all modules
  for (IODB::NameValueLoop l = mIODBNucleus->loopTieNets(); !l.atEnd(); ++l) {
    // get the module and net for this tie
    const STSymbolTableNode* node = l.getKey();
    NUBase* base = mIODBNucleus->getNucleusObject(node);
    NUNet* net = dynamic_cast<NUNet*>(base);
    NU_ASSERT(net != NULL, base);
    NUModule* module = net->getScope()->getModule();

    // Add this tie to the set. If it already exists it should have
    // the same value
    const DynBitVector* value = l.getValue();
    addModuleNet(module, net, NetValue(value, node));
  } // for
} // void RETieNets::processDirectives

void
RETieNets::addModuleNet(NUModule* mod, NUNet* net, const NetValue& netValue)
{
  // Allocate space for this module if we haven't already
  NetValues* netValues;
  ConstModuleNets::iterator pos = mConstModuleNets->find(mod);
  if (pos == mConstModuleNets->end())
  {
    netValues = new NetValues;
    mConstModuleNets->insert(ConstModuleNets::value_type(mod, netValues));
  }
  else
    netValues = pos->second;

  // Add this new constants, it could be added before because we do an
  // elaborated walk to find these, but it should be the same value.
  NetValues::iterator pos2 = netValues->find(net);
  if (pos2 == netValues->end()) {
    netValues->insert(NetValues::value_type(net, netValue));
  } else {
    // Test for a conflict. If the conflict comes from a tieNet
    // directive then print an alert, otherwise assert. We should not
    // be processing a mix of tieNet directives and constant
    // propagation ties.
    const NetValue& storedNetValue = pos2->second;
    const DynBitVector* storedValue = storedNetValue.getConstant();
    const DynBitVector* value = netValue.getConstant();
    if (*value != *storedValue) {
      const STSymbolTableNode* node1 = storedNetValue.getSourceNode();
      const STSymbolTableNode* node2 = netValue.getSourceNode();
      if (node1 != NULL) {
        // It is a tie net
        NU_ASSERT(node2 != NULL, net);
        UtString buf1, buf2;
        const char* val1 = createValueString(net, storedValue, &buf1);
        const char* val2 = createValueString(net, netValue.getConstant(), &buf2);
        mMsgContext->RETieConflict(net, *node1, val1, *node2, val2);
      } else {
        // Constant propagation problem
        NU_ASSERT(netValue.getConstant() == storedValue, net);
      }
    }
  } // } else
} // RETieNets::addModuleNet

void RETieNets::optimize(bool tieStrength)
{
  // Gather all module instances so that we can lookup the parent for
  // a tied port connection.
  NUDesign::ModuleInstances moduleInstances;
  mDesign->findModuleInstances(&moduleInstances);

  // Visit the modules bottom up. This allows any ties to output/bid
  // port connections to propagated up. It adds a tie net to the
  // parent modules
  NUModuleList modules;
  mDesign->getModulesBottomUp(&modules);
  for (Loop<NUModuleList> l(modules); !l.atEnd(); ++l) {
    // Get the set of ties for this module if there are any
    NUModule* module = *l;
    ConstModuleNets::iterator pos = mConstModuleNets->find(module);
    if (pos != mConstModuleNets->end()) {
      // If verbosity is on print the optimized nets. We do this now so
      // that the net flags are still correct.
      NetValues* netValues = pos->second;
      if (mVerbose) {
        printOptimizedNets(netValues);
      }

      // Apply the constants to any module ports and the parent
      // modules for outputs/bids
      typedef UtVector <NUModuleInstance*> InstanceVector; 
      InstanceVector instanceVector;

      NUDesign::InstancesLoop i;
      for (i = loopInstances(&moduleInstances, module); !i.atEnd(); ++i) {
        NUModuleInstance* moduleInstance = *i;
        instanceVector.push_back(moduleInstance);
      }

      std::sort(instanceVector.begin(), instanceVector.end(), NUModuleInstanceCmp());

      for (Loop<InstanceVector> s(instanceVector); !s.atEnd(); ++s) {
        NUModuleInstance* moduleInstance = *s;
        optimizeInstancePorts(moduleInstance, netValues, tieStrength);
      }

      // Walk the constant module nets and optimize them for this
      // module. All LHS's are replaced with a temp, a cont assign is
      // added with the constant, and all RHS's are replaced with the
      // constant.
      optimizeModuleNets(module, netValues, tieStrength);
    }
  } // for

  // All done with the module instances memory
  for (NUDesign::ModuleInstancesLoop m(moduleInstances); !m.atEnd(); ++m) {
    NUDesign::Instances* instances = m.getValue();
    delete instances;
  }

  // All done with the const nets memory
  for (ConstModuleNetsLoop m(*mConstModuleNets); !m.atEnd(); ++m) {
    NetValues* netValues = m.getValue();
    delete netValues;
  }
  mConstModuleNets->clear();
}

void
RETieNets::optimizeInstancePorts(NUModuleInstance* moduleInstance,
                                 NetValues* netValues,
                                 bool tieStrength)
{
  // Gather all the non-constant output ports so that we can replace
  // the full set with this partial set.
  NUPortConnectionVector ports;
  NUPortConnectionVector freePorts;
  NUPortConnectionLoop p;
  for (p = moduleInstance->loopPortConnections(); !p.atEnd(); ++p) {
    NUPortConnection* port = *p;
    NUNet* net = port->getFormal();
    if (netValues->find(net) == netValues->end())
      ports.push_back(port);
    else
      freePorts.push_back(port);
  }

  // Add ties to the actual of the parent module of all constant
  // output ports if they are whole nets. Otherwise, just add a
  // constant assignment at tie strength.
  NUModule* parentModule = moduleInstance->getParentModule();
  NUModuleInstance::PortConnectionOutputLoop o;
  for (o = moduleInstance->loopOutputPortConnections(); !o.atEnd(); ++o) {
    NUPortConnectionOutput* port = *o;
    NUNet* net = port->getFormal();
    NULvalue* lvalue = port->getActual();
    // Make sure the port is not unconnected
    if (lvalue != NULL) {
      optimizePort(parentModule, net, lvalue, netValues, tieStrength);
    } else if (tieStrength) {
      // There is no actual, the tie goes nowhere
      const char* instName = moduleInstance->getName()->str();
      mMsgContext->RETieNetIgnored(net, instName);
    }
  }

  // Add ties to the actual of for bid ports
  NUModuleInstance::PortConnectionBidLoop b;
  for (b = moduleInstance->loopBidPortConnections(); !b.atEnd(); ++b) {
    NUPortConnectionBid* port = *b;
    NUNet* net = port->getFormal();
    NULvalue* lvalue = port->getActual();
    // Make sure the port is not unconnected
    if (lvalue != NULL) {
      optimizePort(parentModule, net, lvalue, netValues, tieStrength);
    } else {
      // There is no actual, the tie goes nowhere
      const char* instName = moduleInstance->getName()->str();
      mMsgContext->RETieNetIgnored(net, instName);
    }
  }

  // Update the port connections
  moduleInstance->setPortConnections(ports);

  // Don't need the constant port connections anymore
  for (p = NUPortConnectionLoop(freePorts); !p.atEnd(); ++p) {
    NUPortConnection* port = *p;
    delete port;
  }
} // RETieNets::optimizeInstancePorts

void RETieNets::optimizePort(NUModule* parentModule, NUNet* formal,
                             NULvalue* lvalue, NetValues* netValues,
                             bool tieStrength)
{
  NetValues::iterator pos = netValues->find(formal);
  if (pos != netValues->end()) {
    const NetValue& netValue = pos->second;
    if (tieStrength && lvalue->isWholeIdentifier()) {
      // The tie continues up the stack
      NUNet* actualNet = lvalue->getWholeIdentifier();
      addModuleNet(parentModule, actualNet, netValue);

    } else {
      // The best we can do is add an assignment to the parent
      CopyContext cntxt(0,0);
      NULvalue* newLvalue = lvalue->copy(cntxt);
      const DynBitVector* value = netValue.getConstant();
      createConstContAssign(formal, newLvalue, parentModule, value, tieStrength);
    }
  }
} // void RETieNets::optimizePort

void
RETieNets::optimizeModuleNets(NUModule* module, NetValues* netValues,
                              bool tieStrength)
{
  // Replace all the LHSs for any constant nets
  replaceLvalues(module, netValues, tieStrength);

  // Replace all right-hand sides with the constant
  replaceRvalues(module, netValues);

  // Optimize all ports (input and output) if it is not a top level
  removePorts(module, netValues);

  // Add continous assigns. We do this after the above lvalue replacement
  // replacement so that our continous assigns don't get clobbered.
  addContAssigns(module, netValues, tieStrength);
} // RETieNets::optimizeModulePorts


//! Design-walker callback to avoid problem constructs
/*! 
 * When re-writing nets found to be constant, do not process
 * edge expressions. See bug2009 for details
 *
 * This callback is used when performing both LHS and RHS
 * replacements.
 *
 * Any new problem constructs can be intercepted here. But be aware
 * that tie nets is used both from the directives processing and
 * constant propagation. So make sure that disabling an optimization
 * applies to both cases.
 */
class RETieNetAvoidProblemConstructs : public NUDesignCallback
{
public:
  RETieNetAvoidProblemConstructs(NuToNuFn & translator) :
    mTranslator(translator)
  {}

  ~RETieNetAvoidProblemConstructs() {}

  //! Everything should have been caught by other operator() methods; assert as consistency check.
  Status operator()(Phase /*phase*/, NUBase* node) {
    NU_ASSERT("NUBase method should never be invoked"==NULL, node);
    return eNormal;
  }

  Status operator()(Phase /*phase*/, NUNamedDeclarationScope*) {
    return eSkip;
  }

  Status operator()(Phase /*phase*/, NUCModel*) {
    return eSkip;
  }

  //! Walk through modules.
  Status operator()(Phase /*phase*/, NUModule * /*module*/) { return eNormal; }

  //! Call the translator on all non-modules.
  /*! 
   * Don't walk through, as ::replaceLeaves has recursively processed
   * everything underneath.
   */
  Status operator()(Phase phase, NUUseDefNode * node) { 
    if (phase==ePre) {
      node->replaceLeaves(mTranslator);
    }
    return eSkip;
  }

  //! Edge expressions need to be processed independently because they aren't yet covered during an always block walk.
  /*!
   * Bug 2009 tracks this issue.
   */
  Status operator()(Phase phase, NUEdgeExpr * edge) {
    if (phase==ePre) {
      edge->replaceLeaves(mTranslator);
    }
    return eSkip;
  }

private:
  //! replaceLeaves translator.
  NuToNuFn & mTranslator;
};

void RETieNets::replaceLvalues(NUModule* module, NetValues* netValues,
                               bool tieStrength)
{
  // Create a net to net map to replace all LHS references to these
  // nets with the temp. We also create continous assigns so the temp
  // value gets that constant assigned. This helps keep the aliased
  // nets correct.
  NUNetReplacementMap netReplMap;
  for (NetValuesLoop l(*netValues); !l.atEnd(); ++l)
  {
    // Create the temp net
    NUNet* net = l.getKey();
    NUNet* tempNet = module->createTempNetFromImage(net, "TieNet");
    netReplMap.insert(NUNetReplacementMap::value_type(net, tempNet));

    // Remember this temp if requested
    if (mTieNetTemps != NULL) {
      mTieNetTemps->insert(tempNet);
    }

    // Assign to the temp net
    const NetValue& netValue = l.getValue();
    const DynBitVector* value = netValue.getConstant();
    NULvalue* lvalue = new NUIdentLvalue(tempNet, net->getLoc());
    createConstContAssign(tempNet, lvalue, module, value, tieStrength);
  }

  // Visit the module replacing all LHS
  NULvalueReplacementMap lvalueReplMap;
  NUExprReplacementMap exprReplMap;
  NUTFReplacementMap tfReplMap;
  NUCModelReplacementMap cmodelReplMap;
  NUAlwaysBlockReplacementMap alwaysReplMap;
  REReplaceLvalues translator(netReplMap, lvalueReplMap, exprReplMap,
                              tfReplMap, cmodelReplMap, alwaysReplMap,
                              mFold);

  RETieNetAvoidProblemConstructs callback(translator);
  NUDesignWalker walker(callback, false);
  NUDesignCallback::Status status = walker.module(module);
  NU_ASSERT(status == NUDesignCallback::eNormal,module);

} // void RETieNets::optimize



void RETieNets::replaceRvalues(NUModule* module, NetValues* netValues)
{
  // Remember the input ports so that we can replace all uses
  NUExprReplacementMap exprReplMap;
  for (NUNetVectorLoop p = module->loopPorts(); !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    NetValues::iterator pos = netValues->find(net);
    if ((pos != netValues->end()) && net->isInput())
    {
      // If the net is not used in an edge expression keep track
      // of the information to do a replace in the entire module.
      //
      // We skip edge expressions because... well, let's just say you
      // don't want to go there.
      if (!net->isEdgeTrigger())
      {
        // Store this net and value for replacement in the module
        const NetValue& netValue = pos->second;
        const DynBitVector* value = netValue.getConstant();
        const SourceLocator& loc = net->getLoc();
        UInt32 size = net->getBitSize();
        bool isSigned = net->isSigned();
        NUExpr* expr = NUConst::create(isSigned, *value, size, loc);
        NU_ASSERT(exprReplMap.find(net) == exprReplMap.end(),net);
        exprReplMap.insert(NUExprReplacementMap::value_type(net, expr));
      }
    }
  }

  // Replace all uses of this net with the constant expression
  NUNetReplacementMap netReplMap;
  NULvalueReplacementMap lvalueReplMap;
  NUTFReplacementMap tfReplMap;
  NUCModelReplacementMap cmodelReplMap;
  NUAlwaysBlockReplacementMap alwaysReplMap;
  REReplaceRvalues translator(netReplMap, lvalueReplMap, exprReplMap,
                              tfReplMap, cmodelReplMap, alwaysReplMap,
                              mFold);

  RETieNetAvoidProblemConstructs callback(translator);
  NUDesignWalker walker(callback, false);
  NUDesignCallback::Status status = walker.module(module);
  NU_ASSERT(status == NUDesignCallback::eNormal,module);
  
  // If the syncReset has been replaced with a constant, remove it
  // from the edgelist.
  for( NUModule::AlwaysBlockLoop ab = module->loopAlwaysBlocks(); 
       not ab.atEnd();  ++ab ) {
    (*ab)->removeConstantSyncReset();
  }


  // Delete the memory we allocated
  for (NUExprReplacementMap::iterator i = exprReplMap.begin();
       i != exprReplMap.end();
       ++i)
  {
    NUExpr* value = i->second;
    delete value;
  }
} // void RETieNets::replaceRHS


void RETieNets::removePorts(NUModule* module, NetValues* netValues)
{
  NUNetVector ports;
  NUNetVector non_ports;
  for (NUNetVectorLoop n = module->loopPorts(); !n.atEnd(); ++n)
  {
    NUNet* net = *n;
    NetValues::iterator pos = netValues->find(net);
    if ((pos == netValues->end()) || net->isPrimaryPort()) {
      // Don't optimize this
      ports.push_back(net);
    } else {
      non_ports.push_back(net);
    }
  }

  // The ::replacePorts call will call NUScope::removeNetName for each
  // removed port.
  module->replacePorts(ports);

  // Because the ::replacePorts call removes references to port names,
  // we need to call NUModule::localizePort (which re-adds names via
  // NUScope::addNetName) as a separate step.
  for (NUNetVector::iterator iter = non_ports.begin();
       iter != non_ports.end();
       ++iter) {
    NUNet * net = (*iter);
    // Update the flags and make it a local. A later pass will clean
    // this up correctly.
    module->localizePort(net);
  }

}

void RETieNets::addContAssigns(NUModule* module, NetValues* netValues,
                               bool tieStrength)
{
  // Add continous assignments for all the nets.
  for (NetValuesLoop l(*netValues); !l.atEnd(); ++l) {
    NUNet* net = l.getKey();
    const NetValue& netValue = l.getValue();
    const DynBitVector* value = netValue.getConstant();
    NULvalue* lvalue = new NUIdentLvalue(net, net->getLoc());
    createConstContAssign(net, lvalue, module, value, tieStrength);
  }
}

void
RETieNets::createConstContAssign(NUNet* net, NULvalue* lvalue,
                                 NUModule* parentModule,
                                 const DynBitVector* value,
                                 bool tieStrength)
{
  const SourceLocator& loc = net->getLoc();
  StringAtom* name = parentModule->newBlockName(mStrCache, loc);
  NUExpr* expr = NUConst::create (lvalue->isWholeIdentifier () ? net->isSigned () : false, *value, lvalue->getBitSize(), loc);
  Strength str = eStrDrive;
  if (tieStrength)
    str = eStrTie;
  NUContAssign* assign = new NUContAssign(name, lvalue, expr, loc, str);
  parentModule->addContAssign(assign);
}

const char* RETieNets::createValueString(NUNet* net, const DynBitVector* val,
                                         UtString* buf)
{
  DynBitVector value(net->getBitSize(), *val);
  value.format(buf, eCarbonHex);
  return buf->c_str();
}

void RETieNets::printOptimizedNets(NetValues* netValues)
{
  // Visit the nets
  ASSERT(mVerbose);
  for (NetValuesLoop n(*netValues); !n.atEnd(); ++n) {
    // Print the location and type of optimization
    NUNet* net = n.getKey();

    // print the constant (make sure it is the right size)
    const NetValue& netValue = n.getValue();
    UtString buf;
    const char * value_str = createValueString(net, netValue.getConstant(), &buf);

    // Are we optimizing or tying?
    if (mTying) {
      // Find the net type (net, output port, bid port, input port)
      if (net->isOutput())
        mMsgContext->RETieNetTiedOutput(net, value_str);
      else if (net->isInput())
        mMsgContext->RETieNetTiedInput(net, value_str);
      else if (net->isBid())
        mMsgContext->RETieNetTiedBid(net, value_str);
      else if (net->isReg())
        mMsgContext->RETieNetTiedReg(net, value_str);
      else 
        mMsgContext->RETieNetTiedWire(net, value_str);
    } else {
      // Find the net type (net, output port, input port)
      if (net->isOutput())
        mMsgContext->RETieNetOptimizedOutput(net, value_str);
      else if (net->isInput())
        mMsgContext->RETieNetOptimizedInput(net, value_str);
      else if (net->isBid())
        mMsgContext->RETieNetOptimizedBid(net, value_str);
      else if (net->isReg())
        mMsgContext->RETieNetOptimizedReg(net, value_str);
      else 
        mMsgContext->RETieNetOptimizedWire(net, value_str);
    }
  } // for
} // void RETieNets::printOptimizedNets

NUDesign::InstancesLoop
RETieNets::loopInstances(NUDesign::ModuleInstances* moduleInstances,
                         NUModule* module)
{
  NUDesign::ModuleInstances::iterator pos = moduleInstances->find(module);
  NU_ASSERT(pos != moduleInstances->end(),module);
  NUDesign::Instances* instances = pos->second;
  return NUDesign::InstancesLoop(*instances);
}  

void RETieNets::findTienetIssues(void)
{
  // Walk the design looking for all elaborated nets
  NUNetElabSet covered;
  STSymbolTable::NodeLoop p;
  for (p = mDesign->getSymbolTable()->getNodeLoop(); !p.atEnd(); ++p) {
    STSymbolTableNode* node = *p;
    STAliasedLeafNode* leaf = node->castLeaf();
    if (leaf != NULL) {
      NUNetElab* netElab = NUNetElab::find(leaf);
      if ((netElab != NULL) && !netElab->getNet()->isDead() &&
          (covered.find(netElab) == covered.end())) {
        // Mark it covered because he may hit it multiple times due to
        // aliasing
        covered.insert(netElab);

        // Check if this net had a tie net in its aliases
        NUNetVector aliasNets;
        netElab->getAliasNets(&aliasNets, true);
        bool tieNetFound = false;
        for (NUNetVectorLoop l(aliasNets); !l.atEnd() && !tieNetFound; ++l) {
          NUNet* net = *l;
          tieNetFound = mTieNetTemps->find(net) != mTieNetTemps->end();
        }

        // If a tie net was found, re-visit the alias net drivers
        // looking for other problematic drivers.
        if (tieNetFound) {
          for (NUNetVectorLoop l(aliasNets); !l.atEnd(); ++l) {
            NUNet* net = *l;
            if (mTieNetTemps->find(net) == mTieNetTemps->end()) {
              analyzeOtherDriver(net);
            }
          }
        }
      } // if
    } // if
  } // for
} // void RETieNets::findTienetIssues

void RETieNets::analyzeOtherDriver(NUNet* net)
{
  // Iterate over all the drivers of this net
  for (NUNet::DriverLoop l = net->loopContinuousDrivers(); !l.atEnd(); ++l) {
    // Check if this is an always block and if so get its
    // statements. Otherwise, this is not a problem driver.
    FLNode* flnode = *l;
    NUUseDefNode* useDef = flnode->getUseDefNode();
    NUBlock* block = NULL;
    if ((useDef != NULL) && (useDef->getType() == eNUAlwaysBlock)) {
      NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
      NU_ASSERT(always,useDef);
      block = always->getBlock();
    }
    if (block != NULL) {
      // Get the net for this tied elaborated net
      NUNet* tiedNet = flnode->getDefNet();

      // Loop over the statements in this block to see if any of them use
      // the tied net.
      for (NUStmtLoop l = block->loopStmts(); !l.atEnd(); ++l) {
        NUStmt* stmt = *l;
        NUNetSet uses;
        stmt->getBlockingUses(&uses);
        if (uses.find(tiedNet) != uses.end()) {
          NUModule* module = tiedNet->getScope()->getModule();
          UtString name;
          name << *(module->getName()) << "." << *(tiedNet->getName());
          mMsgContext->RETieIneffective(&stmt->getLoc(), name.c_str());
        }
      }
    }
  } // for
} // void RETieNets::analyzeOtherDriver

