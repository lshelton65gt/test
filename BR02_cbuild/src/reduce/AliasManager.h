// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#ifndef _ALIASMANAGER_H_
#define _ALIASMANAGER_H_

#include "util/OSWrapper.h"
#include "nucleus/Nucleus.h"
#include "flow/FLNode.h"
#include "util/CarbonTypes.h"
#include "util/UtSet.h"
#include "util/UtList.h"
#include "util/UtMap.h"
#include "util/UtArray.h"

/*!\file
 * Declaration of alias manager.
 */

class NUNetRefFactory;
class FLNodeFactory;
class MsgContext;
class IODBNucleus;
class ArgProc;
class AliasGroup;
class CausalityIndexMapPopulator;
class Stats;

//! AliasManager class
/*!
 * Class to manage unelaborated aliases.
 */
class AliasManager
{
public:
  //! constructor
  AliasManager(NUNetRefFactory* netRefFactory,
               FLNodeFactory*   flowFactory,
               IODBNucleus*     IODB,
               ArgProc*         args,
               MsgContext*      msgContext,
               Stats *          stats,
               bool             phase_stats,
               bool             verbose,
               bool             contAssignsOnly)
    : mNetRefFactory(netRefFactory),
      mFlowFactory(flowFactory),
      mIODB(IODB),
      mArgs(args),
      mMsgContext(msgContext),
      mStats(stats),
      mPhaseStats(phase_stats),
      mVerbose(verbose),
      mContAssignsOnly(contAssignsOnly)
  {}

  //! destructor
  ~AliasManager() { clear(); }

  //! Prepare the manager for aliasing in the specified module
  void prepare(NUModule* module);

  //! Get the causality index for the specified net (valid between prepare() and clear())
  UInt32 getCausalityIndex(NUNet* net) const;

  //! Remember the pair of nets as an alias.  aliasDriver is NULL for functional aliases
  bool rememberAlias(NUNet *net1, NUNet* net2, FLNode* aliasDriver);

  //! Remember the fanout for a net
  void rememberFanout(NUNet *net, FLNode *fanout, Sensitivity sense);

  //! Remember the driver for a net
  bool rememberDriver(NUNet *net, FLNode *driver, Sensitivity sense);

  //! Sanity check the alias maps
  void sanityCheckAliases();

  //! Perform the Nucleus and Flow modifications for the scheduled aliases
  void performAliasing(bool dumpAliases);

  //! Clear out the state of this manager
  void clear();

  //! Are we verbose?
  bool isVerbose() const  { return mVerbose;}

  //! Query over a set of pending aliases.
  bool queryAliases(NUNet *net, NUNet::QueryFunction fn) const;

  //! Return the current master alias for the given net.
  NUNet* getCurrentMaster(NUNet * net) const;

  //! Add the current set of pending aliases for the given net to the given set.
  void getCurrentAliases(NUNet * net, NUNetSet * nets) const;

private:
  //! Hide copy and assign constructors.
  AliasManager(const AliasManager&);
  AliasManager& operator=(const AliasManager&);

  //! Update temp-memory masters based on aliasing.
  void updateTempMemoryMasters(NuToNuFn & translator);

  //! Convenience typedef for mapping flow to flow set
  typedef UtMap<FLNode*,FLNode::Set> ReplacementMap;
  typedef ReplacementMap::iterator ReplacementMapIter;
  typedef ReplacementMap::const_iterator ReplacementMapCIter;

  //! Get the AliasGroup for the net or NULL is it is not in one
  AliasGroup* lookupAliasGroup(NUNet* net);

  friend class CausalityIndexMapPopulator;

  //! Populate the map of net to causality-index for this module
  void buildCausalityIndexMap();

  //! Sanity check the given alias group
  void sanityCheckAliasGroup(AliasGroup* group);

  //! Build a map for replacing Nucleus references to subordinates with masters
  NUNetReplacementMapNonConst* buildNetSubstitutionMap() const;

  //! Propagate flags from subords to master
  /*!
   * The assumption is that the flags have already been checked for sanity, so we
   * just do propagation now.
   */
  void flagAliases(AliasGroup* group);

  //! Populate the map with corresponding replacements for non-chosen flow
  void buildReplacementMap(AliasGroup* group, ReplacementMap* replacement_map);

  //! Helper to populate node replacement map
  void helperRememberReplacement(FLNode *node, ReplacementMap* replacement_map);

  //! Perform the actual replacement.
  void performReplacement(ReplacementMap* replacement_map);

  //! Rewrite a replacement map.
  /*!
    If a FLNode A is being eliminated and its fanin FLNode B is also
    being eliminated, do not allow B as a replacement for A. The
    replacements for A should instead contain the replacements for B.
    
    This processing ensures that no FLNode marked for replacement is
    in the replacement set for another FLNode.
   */
  void fixupReplacementMap(ReplacementMap* replacement_map);

  //! Rewrite the replacement map entries for one FLNode.
  /*!
    If a FLNode A is being eliminated and its fanin FLNode B is also
    being eliminated, do not allow B as a replacement for A. The
    replacements for A should instead contain the replacements for B.
    
    This processing ensures that no FLNode marked for replacement is
    in the replacement set for another FLNode.
   */
  void fixupOneReplacement(FLNode* replacement,
                           ReplacementMap& replacement_map,
                           FLNode::Set* new_replacements,
                           FLNode::Set* processed);

  //! Fixup fanout of aliased nets; replace old drivers with the new ones
  void fixupFanouts(const ReplacementMap& replacement_map);

  //! Fixup fanouts for the given net
  void fixupFanout(NUNet* net,
                   FLNodeSet & covered_flow,
                   const ReplacementMap& replacement_map);

  //! Dump out master/alias relationships
  void dumpAliases();

  //! Cleanup net references which have been aliased.
  void cleanupAliasedNets();

  //! Alias subordinate nets to master and remove subordinate nets
  void cleanupSubords(AliasGroup* group);

  //! Repair driving flow-nodes in continuous-assign mode, handling multiply
  //! driven nets
  void repairContFlows();

  //! Fixup fanouts for continuous drivers, allowing for multiply driven nets
  void fixupContFanouts();

private:
  NUNetRefFactory* mNetRefFactory;
  FLNodeFactory*   mFlowFactory;
  IODBNucleus*     mIODB;
  ArgProc*         mArgs;
  MsgContext*      mMsgContext;

  //! The current module being processed
  NUModule* mModule;

  //! The set of all AliasGroups
  typedef UtSet<AliasGroup*> AliasGroupSet;
  AliasGroupSet mAliases;

  //! A map from every aliased net to its AliasGroup
  typedef UtMap<NUNet*,AliasGroup*> NetAliasMap;
  NetAliasMap mNetToGroupMap;

  //! Keep track of a node and how we got there
  typedef std::pair<FLNode*,Sensitivity> NodeSense;

  //! Set of nodes and how we got there
  typedef UtSet<NodeSense> NodeSenseSet;

  //! Convenience typedef for mapping net to set of flow.
  typedef UtMap<NUNet*,NodeSenseSet*> NetNodeSetMap;
  typedef NetNodeSetMap::iterator NetNodeSetMapIter;

  //! Map of nets which are going to be aliased to their fanouts.
  NetNodeSetMap mFanouts;

  //! Map of nets which are going to be aliased to their drivers.
  NetNodeSetMap mDrivers;

  //! A map from nets to causality index (SCC component numbers in DFS order)
  /*! Two nets that map to the same causality index are in a cycle together.
   *  Two nets that map to different causality indices are not in a cycle together
   *  and the one with the smaller index should be chosen as the flow donor.
   */
  typedef UtMap<NUNet*,UInt32> CausalityIndexMap;
  CausalityIndexMap mCausalityIndexMap;

  //! Performance statistics object.
  Stats * mStats;

  //! Are we computing performance statistics?
  bool mPhaseStats;

  //! Be verbose?
  bool mVerbose;

  //! Is this alias manager processing continuous assigns only?
  /*! If so, then the replacement map strategy is a lot simpler,
   *! and AliasGroups can retain their own flow-donor set, which
   *! enables the handling of aliasing nets with multiple drivers
   */
  bool mContAssignsOnly;
};

#endif // _ALIASMANAGER_H_
