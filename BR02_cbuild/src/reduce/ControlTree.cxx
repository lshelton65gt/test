// -*-C++-*-
/******************************************************************************
 Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!\file
 * An implementation of a ControlTree data structure.
 */

#include <algorithm>
#include <vector>

#include "reduce/ControlTree.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FlowClassifier.h"

#include "schedule/BDDSource.h"

//! Build the address for a node
ControlTreeNodeAddress::ControlTreeNodeAddress(ControlTree* node)
{
  // Build a stack of branch directions, then build the address from the root
  UtStack<bool> dirStack;
  mNumBranches = 0;
  ControlTree* parent = node->getParent();
  while (parent)
  {
    ++mNumBranches;
    dirStack.push(parent->getTrueBranch() == node);
    node = parent;
    parent = node->getParent();
  }

  UInt32 word = 0;
  UInt32 idx = 0;
  while (!dirStack.empty())
  {
    bool dir = dirStack.top();
    dirStack.pop();
    if (dir)
      word |= (0x1 << idx);
    idx = (idx + 1) % 32;
    if (idx == 0)
    {
      mRoute.push_back(word);
      word = 0;
    }
  }
  if (idx != 0)
    mRoute.push_back(word);
}

//! Get the node in a tree that corresponds to this address, or NULL
ControlTree* ControlTreeNodeAddress::lookup(ControlTree* tree) const
{
  UtList<UInt32>::const_iterator iter = mRoute.begin();
  UInt32 idx = 0;
  UInt32 word = 0;
  for (UInt32 n = mNumBranches; (tree != NULL) && (n > 0); --n)
  {
    INFO_ASSERT(iter != mRoute.end(), "Address data is truncated");
    if (tree->isLeaf())
      return NULL;
    if (idx == 0)
      word = *iter;
    else
      word = word >> 1;

    if (word & 0x1)
      tree = tree->getTrueBranch();
    else
      tree = tree->getFalseBranch();
    
    idx = (idx + 1) % 32;
    if (idx == 0)
      ++iter;
  }

  return tree;
}

//! Truncate the address to len branches
void ControlTreeNodeAddress::truncate(UInt32 len)
{
  if (len >= mNumBranches)
    return;

  UInt32 listLen = (len + 31) / 32;  
  if (listLen < ((mNumBranches + 31) / 32))
    mRoute.resize(listLen);
  
  UInt32 newIdx = len % 32;
  if (newIdx != 0)
  {
    UtList<UInt32>::reverse_iterator p = mRoute.rbegin();
    *p &= (1 << newIdx) - 1;
  }

  mNumBranches = len;
}


//! Print an address to stdout
void ControlTreeNodeAddress::print() const
{
  UtList<UInt32>::const_iterator iter = mRoute.begin();
  UInt32 idx = 0;
  UInt32 word = 0;
  for (UInt32 n = mNumBranches; n > 0; --n)
  {
    INFO_ASSERT(iter != mRoute.end(), "Address data is truncated");
    if (idx == 0)
      word = *iter;
    else
      word = word >> 1;

    if (word & 0x1)
      UtIO::cout() << "T";
    else
      UtIO::cout() << "F";
    
    idx = (idx + 1) % 32;
    if (idx == 0)
      ++iter;
  }
}

//! Test for equality
bool ControlTreeNodeAddress::operator==(const ControlTreeNodeAddress& other) const
{
  if (mNumBranches != other.mNumBranches)
    return false;

  UtList<UInt32>::const_iterator l1 = mRoute.begin();
  UtList<UInt32>::const_iterator l2 = other.mRoute.begin();
  while ((l1 != mRoute.end()) && (l2 != other.mRoute.end()))
  {
    // Note: the last word is zero-padded 
    if (*(l1++) != *(l2++))
      return false;
  }

  return true;
}

//! Comparison operator
bool ControlTreeNodeAddress::operator<(const ControlTreeNodeAddress& other) const
{
  if (mNumBranches < other.mNumBranches)
    return true;
  else if (mNumBranches > other.mNumBranches)
    return false;

  UtList<UInt32>::const_iterator l1 = mRoute.begin();
  UtList<UInt32>::const_iterator l2 = other.mRoute.begin();
  while ((l1 != mRoute.end()) && (l2 != other.mRoute.end()))
  {
    UInt32 word1 = *(l1++);
    UInt32 word2 = *(l2++);
    // Note: the last word is zero-padded 
    if (word1 < word2)
      return true;
    else if (word1 > word2)
      return false;
  }

  return false;
}

//! Get the condition expression from a branch node if-statement
NUExpr* ControlTree::getIfCondExpr() const
{
  INFO_ASSERT(isBranch(), "Cannot get condition expression for non-branch node");
  NUUseDefNode* useDef = getBranchFlow()->getUseDefNode();
  NUIf* ifStmt = dynamic_cast<NUIf*>(useDef);
  NU_ASSERT(ifStmt,useDef);
  return ifStmt->getCond();
}

//! Make a deep copy of this control tree
ControlTree* ControlTree::clone() const
{
  if (isLeaf())
    return new ControlTree(mFlowSet.begin(),mFlowSet.end());
  
  ControlTree* trueLeg = mTrue ? mTrue->clone() : NULL;
  ControlTree* falseLeg = mFalse ? mFalse->clone() : NULL;
  return new ControlTree(getBranchFlow(), trueLeg, falseLeg);
}

//! Test for equality
bool ControlTree::operator==(const ControlTree& other) const
{
  if ((mTrue  && !other.mTrue)  || (!mTrue  && other.mTrue) ||
      (mFalse && !other.mFalse) || (!mFalse && other.mFalse))
    return false;

  if (!(mFlowSet == other.mFlowSet))
    return false;

  if (mTrue && ((*mTrue) != (*other.mTrue)))
    return false;

  if (mFalse && ((*mFalse) != (*other.mFalse)))
    return false;

  return true;
}

//! Test for equal control structures (leaf expressions can differ)
bool ControlTree::sameControlStructure(ControlTree* other) const
{
  if (other == NULL)
    return false;

  if (isLeaf() && other->isLeaf())
    return true;

  if (!isBranch() || !other->isBranch())
    return false;

  // branch flow nodes should be for the same use-def node
  if (getBranchUseDef() != other->getBranchUseDef())
    return false;
  
  if ((mTrue && !mTrue->sameControlStructure(other->mTrue)) ||
      (!mTrue && other->mTrue))
    return false;

  if ((mFalse && !mFalse->sameControlStructure(other->mFalse)) ||
      (!mFalse && other->mFalse))
    return false;

  return true;
}

//! Delete the subtree at this node and remove it from its parent
/*! Note: ONLY CALL THIS IF THE TREE IS ON THE HEAP */
void ControlTree::prune()
{
  if (!isRoot())
  {
    if (mParent->mTrue == this)
      mParent->mTrue = NULL;
    if (mParent->mFalse == this)
      mParent->mFalse = NULL;
  }
  // delete ourselves!
  delete this;
}

//! Prune use of the specified net from the tree
/*! Note: ONLY CALL THIS IF THE TREE IS ON THE HEAP */
bool ControlTree::pruneUse(NUNet* net)
{
  if (isBranch())
  {
    if (mTrue)
      mTrue->pruneUse(net);
    if (mFalse)
      mFalse->pruneUse(net);
    if (!mTrue && !mFalse) // branch is now empty!
    {
      prune();
      return false;
    }
  }
  else // is leaf
  {
    bool canPrune = true;
    for (FLNodeElabSet::iterator iter = mFlowSet.begin();
         iter != mFlowSet.end();
         ++iter)
    {
      FLNodeElab* flow = *iter;
      NUAssign* assign = dynamic_cast<NUAssign*>(flow->getUseDefNode());
      if (assign)
      {
        NUExpr* rhs = assign->getRvalue();
        if (rhs->getType() == NUExpr::eNUIdentRvalue)
        {
          NUIdentRvalue* ident = dynamic_cast<NUIdentRvalue*>(rhs);
          NU_ASSERT(ident,rhs);
          if (ident->getIdent() == net) // matches pruning target net
            continue;
        }
      }
      canPrune = false;
    }

    if (canPrune)
    {
      prune();
      return false;
    }
  }

  return true;
}

//! Merge another control tree into this one (this one has priority)
void ControlTree::merge(ControlTree* other)
{
  if (isLeaf() || (other == NULL))
    return;

  // Handle same condition by merging sub-branches
  if (other->isBranch() && (other->getBranchFlow() == getBranchFlow()))
  {
    if (mTrue)
      mTrue->merge(other->mTrue);
    else if (other->mTrue)
    {
      mTrue = other->mTrue->clone();
      mTrue->mParent = this;
    }
    if (mFalse)
      mFalse->merge(other->mFalse);
    else if (other->mFalse)
    {
      mFalse = other->mFalse->clone();
      mFalse->mParent = this;
    }
    return;
  }

  // Handle different conditions by pushing other down tree
  if (mTrue)
    mTrue->merge(other);
  else
  {
    mTrue = other->clone();
    mTrue->mParent = this;
  }

  if (mFalse)
    mFalse->merge(other);
  else
  {
    mFalse = other->clone();
    mFalse->mParent = this;
  }
}

//! Replace this node with the given replacement tree
void ControlTree::replace(ControlTree* replacement)
{
  // if no replacement, prune this tree
  if (!replacement)
  {
    prune();
    return;
  }

  // copy info from replacement root
  ControlTree* oldTrueBranch = mTrue;
  ControlTree* oldFalseBranch = mFalse;
  mFlowSet.swap(replacement->mFlowSet);
  mTrue = replacement->mTrue;
  if (mTrue) mTrue->mParent = this;
  mFalse = replacement->mFalse;
  if (mFalse) mFalse->mParent = this;

  // disconnect replacement root and delete it
  replacement->mParent = NULL;
  replacement->mTrue = NULL;
  replacement->mFalse = NULL;
  delete replacement;

  // delete old subtrees
  if (oldTrueBranch != replacement)
    delete oldTrueBranch;
  if ((oldFalseBranch != replacement) && (oldFalseBranch != oldTrueBranch))
    delete oldFalseBranch;
}

//! Reduce the control tree if there are any redundant branches
ControlTree* ControlTree::reduce(BDDSource* bddSource)
{
  BDD trueBDD = bddSource->trueBDD();
  return reduceHelper(bddSource, trueBDD);
}

//! Replace or prune this node if it's redundant according to the condition
ControlTree* ControlTree::reduceHelper(BDDSource* bddSource, BDD& condition)
{
  if (isLeaf())
    return this;

  // Eliminate a branch which is implied by the given condition
  BDD test = bddSource->bdd(getBranchFlow());
  BDD notTest = bddSource->opNot(test);

  // If the condition implies test, replace this node with mTrue
  if (bddSource->implies(condition,test))
  {
    if (mTrue) {
      replace(mTrue->clone());
      return reduceHelper(bddSource,condition);
    } else {
      prune();
      return NULL;
    }
  }
  // If the condition implies !test, replace this node with mFalse
  else if (bddSource->implies(condition,notTest))
  {
    if (mFalse) {
      replace(mFalse->clone());
      return reduceHelper(bddSource,condition);
    } else {
      prune();
      return NULL;
    }
  }

  // recurse onto subtrees with a restricted condition
  if (mTrue)
  {
    BDD restricted = bddSource->opAnd(condition,test);
    mTrue->reduceHelper(bddSource, restricted);
  }

  if (mFalse)
  {
    BDD restricted = bddSource->opAnd(condition,notTest);
    mFalse->reduceHelper(bddSource, restricted);
  }

  // Eliminate a branch where both legs are the same
  if (mTrue && mFalse && (*mTrue == *mFalse))
  {
    replace(mTrue->clone());
    // We have already reduced the entire subtree; no need to re-recurse.
    return this;
  }

  if (isLeaf()) {
    prune();
    return NULL;
  } else {
    return this;
  }
}

//! Convert a branch node into a leaf node
void ControlTree::makeLeaf()
{
  delete mTrue;
  if (mFalse != mTrue)
    delete mFalse;
  mTrue = NULL;
  mFalse = NULL;
}

//! Build a BDD representing the condition from the root to this node
BDD ControlTree::getReadCondition(BDDSource* bddSource) const
{
  BDD condition = bddSource->trueBDD();
  bool invert = (mParent != NULL) && (this == mParent->mFalse);
  ControlTree* node = mParent;
  while (node)
  {
    if (invert)
      condition = bddSource->opAnd(condition, bddSource->opNot(bddSource->bdd(node->getBranchFlow())));
    else
      condition = bddSource->opAnd(condition, bddSource->bdd(node->getBranchFlow()));
    invert = node->mParent && (node == node->mParent->mFalse);
    node = node->mParent;
  }
  return condition;
}

//! Build a BDD representing the condition from this node to its leaves
BDD ControlTree::getDrivingCondition(BDDSource* bddSource) const
{
  if (isLeaf())
    return bddSource->trueBDD();

  BDD condition = bddSource->bdd(getBranchFlow());
  BDD result = bddSource->falseBDD();
  if (mTrue)
    result = bddSource->opAnd(condition,
                              mTrue->getDrivingCondition(bddSource));
  if (mFalse)
    result = bddSource->opOr(result,
                             bddSource->opAnd(bddSource->opNot(condition),
                                              mFalse->getDrivingCondition(bddSource)));
  return result;
}

//! Return true if the control tree has a single spine
bool ControlTree::isSingleSpine() const
{
  return (singleSpineHelper() < 2);
}

//! Helper method for isSingleSpine()
int ControlTree::singleSpineHelper() const
{
  /* The logic behind this is that we are trying to detect when
   * a control tree splits and has a branch with two non-trivial
   * sub-trees.  In order to be single-spine the branches of a
   * control tree must all have no more than 1 non-leaf sub-tree.
   *
   * We use 0 to mean a leaf, 1 to mean a single-spine sub-tree
   * and 2 to mean a non-single-spine sub-tree.
   */
  if (isLeaf())
    return 0;

  // Neither sub-tree is allowed to be non-single-spine
  int trueVal  = mTrue  ? mTrue->singleSpineHelper()  : 0;
  if (trueVal == 2)
    return 2;
  int falseVal = mFalse ? mFalse->singleSpineHelper() : 0;
  if (falseVal == 2)
    return 2;

  // Only one sub-tree is allowed to be non-leaf
  if ((trueVal == 1) && (falseVal == 1))
    return 2;
  else
    return 1;
}

//! Return true if this tree's leaf drivers have only continuous flow fanin
bool ControlTree::isContinuouslyDriven() const
{
  if (isBranch())
  {
    // A branch is continuously driven if both legs are
    return (((mTrue == NULL) || mTrue->isContinuouslyDriven()) &&
            ((mFalse == NULL) || mFalse->isContinuouslyDriven()));
  }

  // A leaf is continuously driven if all of its fanin are continuous drivers
    
  for (FLNodeElabSet::const_iterator iter = mFlowSet.begin(); iter != mFlowSet.end(); ++iter)
  {
    FLNodeElab* flow = *iter;
    for (FLNodeElabLoop loop = flow->loopFanin(); !loop.atEnd(); ++loop)
    {
      FLNodeElab* fanin = *loop;
      NUUseDefNode* useDef = fanin->getUseDefNode();
      if (useDef && !useDef->isContDriver())
        return false;
    }
  }

  return true;
}

//! Return true if this tree is fully populated (every branch has both true and false legs)
bool ControlTree::isFullyPopulated() const
{
  if (isLeaf())
    return true;
  
  return (isCompleteBranch() && mTrue->isFullyPopulated() && mFalse->isFullyPopulated());
}

//! Return the node with the given flow, or NULL if none in this tree have it
ControlTree* ControlTree::findNodeWithFlow(FLNodeElab* flow)
{
  if (mFlowSet.find(flow) != mFlowSet.end())
    return this;

  ControlTree* match = NULL;
  if (mTrue)
    match = mTrue->findNodeWithFlow(flow);
  
  if (!match && mFalse)
    match = mFalse->findNodeWithFlow(flow);

  return match;
}

//! Print a text representation of the control tree
void ControlTree::print(int indent) const
{
  UtString buf;
  compose(&buf, indent);
  UtIO::cout() << buf;
}

//! Compose a text representation of the control tree
void ControlTree::compose(UtString* buf, int indent) const
{
  // For a leaf, just print the expression
  if (isLeaf())
  {
    for (FLNodeElabSet::const_iterator iter = mFlowSet.begin(); iter != mFlowSet.end(); ++iter)
    {
      FLNodeElab* flow = *iter;
      NUUseDefNode* useDef = flow->getUseDefNode();
      useDef->compose(buf,NULL,indent);
    }
    return;
  }

  // For a branch, print the conditional and true and false legs
  NUExpr* cond = getIfCondExpr();
  if (mTrue)
  {
    // Print true branch
    buf->append(indent, ' ');
    buf->append("if ");
    cond->compose(buf, NULL);
    buf->append(" {\n");
    mTrue->compose(buf, indent+2);
    if (mFalse)
    {
      buf->append(indent, ' ');
      buf->append("} else {\n");
      mFalse->compose(buf, indent+2);
    }
    buf->append(indent, ' ');
    buf->append("}\n");
  }
  else if (mFalse)
  {
    // Only false branch, so invert condition
    buf->append(indent, ' ');
    buf->append("if !");
    cond->compose(buf, NULL);
    buf->append(" {\n");
    mFalse->compose(buf, indent+2);
    buf->append(indent, ' ');
    buf->append("}\n");
  }
}

//! Write out the ControlTree in dot format
void ControlTree::write(const char* filename) const
{
  UtOFStream out(filename);
  
  out << "digraph \"ControlTree\" {\n";

  writeHelper(out);

  out << "}\n";
}

void ControlTree::writeHelper(UtOStream& out) const
{
  if (!isRoot())
  {
    mParent->writeName(out);
    out << " -> ";
  }

  writeName(out);
  out << "\n";

  if (mTrue)
    mTrue->writeHelper(out);
  if (mFalse)
    mFalse->writeHelper(out);
}

void ControlTree::writeName(UtOStream& out) const
{
  UtString buf;
  if (isLeaf())
  {
    bool first = true;
    for (FLNodeElabSet::const_iterator iter = mFlowSet.begin(); iter != mFlowSet.end(); ++iter)
    {
      FLNodeElab* flow = *iter;
      NUUseDefNode* useDef = flow->getUseDefNode();
      if (!first)
        buf.append("\\n");
      else
        first = false;
      useDef->compose(&buf,NULL);
    }
  }
  else
  {
    NUExpr* cond = getIfCondExpr();
    cond->compose(&buf, NULL);
  }
  out << '"' << buf << '"';
}

bool ControlTree::forAllLeaves(ControlTreeCallback* callback)
{
  if (isLeaf())
    return callback->visit(this);
  else
  {
    if (mTrue && !mTrue->forAllLeaves(callback))
      return false;
    return (!mFalse || mFalse->forAllLeaves(callback));
  }
}

bool ControlTree::forAllBranches(ControlTreeCallback* callback)
{
  if (isLeaf())
    return true;

  if (!callback->visit(this))
    return false;

  if (mTrue && !mTrue->forAllBranches(callback))
    return false;
  return (!mFalse || mFalse->forAllBranches(callback));
}

bool ControlTree::forAllNodes(ControlTreeCallback* callback)
{
  if (!callback->visit(this))
    return false;

  if (mTrue && !mTrue->forAllNodes(callback))
    return false;
  return (!mFalse || mFalse->forAllNodes(callback));
}

bool ControlTree::consistent()
{
  if (isLeaf()) 
  {
    // No leaf-specific tests yet.
    return true;
  } 
  else 
  {
    // Check that we are the parent of both branchs.
    if ((mTrue && (mTrue->mParent != this)) or
        (mFalse && (mFalse->mParent != this))) 
    {
      return false;
    }
    // Branches have only one flow.
    if (mFlowSet.size() != 1) 
    {
      return false;
    }
    return true;
  }
}

//! Static utility method for building a ControlTree from an elaborated flow node
ControlTree*
ControlTree::controlTreeFromFlow(FLNodeElab* flow, NucleusOrderMap& orderMap,
                                 NUNetRefFactory* netRefFactory)
{
  Result result = buildTreeForSingleFlow(flow, orderMap, netRefFactory);
  return result.getTree();
}

ControlTree::Result
ControlTree::buildTreeForSingleFlow(FLNodeElab* flow, NucleusOrderMap& orderMap,
                                    NUNetRefFactory* netRefFactory)
{
  NUUseDefNode* useDef = flow->getUseDefNode();

  // Bound nodes are simply leaves that always load
  if (!useDef) {
    // We assume a bound node kills the entire net
    return Result(new ControlTree(flow), true);
  }

  // Construct the control tree based on the type of usedef node
  switch (useDef->getType())
  {
    case eNUAlwaysBlock:
    {
      NU_ASSERT(!useDef->isSequential(), useDef);

      // For an always block, recurse into the nested block
      FLNodeElab* nested = flow->getSingleNested();
      FLN_ELAB_ASSERT(nested,flow);
      return buildTreeForSingleFlow(nested, orderMap, netRefFactory);
    }
    break;
    case eNUBlock:
    {
      // There are two cases we need to handle
      //   1. a block in which all nested flows def the same netref, where
      //      we want to build individual control trees and then merge
      //      them in their source order
      //   2. all other cases, in which we want to build a control tree
      //      leaf from the block itself.
      FLNodeElabSet flows;
      for (FLNodeElabLoop loop = flow->loopNested(); !loop.atEnd(); ++loop)
        flows.insert(*loop);
      return buildTreeForMultipleFlows(flow, flows, orderMap, netRefFactory);
    }
    break;
    case eNUIf:
    {
      // If this is a conditional statement, build trees for the then and else clauses
      bool kills = true;
      FLNodeElabSet flows;
      FLIfClassifier<FLNodeElab> ifClassifier(flow);
      for (FLIfClassifier<FLNodeElab>::FlowLoop loop = ifClassifier.loopThen(flow);
           !loop.atEnd();
           ++loop)
      {
        flows.insert(*loop);
      }
      Result result = buildTreeForMultipleFlows(flow, flows, orderMap,
                                                netRefFactory);
      ControlTree* thenTree = result.getTree();
      kills &= result.allAssignsKill();

      flows.clear();
      for (FLIfClassifier<FLNodeElab>::FlowLoop loop = ifClassifier.loopElse(flow);
           !loop.atEnd();
           ++loop)
      {
        flows.insert(*loop);
      }
      result = buildTreeForMultipleFlows(flow, flows, orderMap, netRefFactory);
      ControlTree* elseTree = result.getTree();
      kills &= result.allAssignsKill();

      ControlTree* tree = new ControlTree(flow, thenTree, elseTree);
      return Result(tree, kills);
    }
    break;
    case eNUBlockingAssign:
    {
      // If this is a simple assignment, walk through it, otherwise stop here
      NUAssign* assign = dynamic_cast<NUAssign*>(useDef);
      NU_ASSERT(assign,useDef);

      // Check if this assignment doesn't kills the defs. If so, we
      // return false so that the upper layers can throw out cases
      // where we can't create a valid control tree. See
      // buildTreeForMultipleFlows for details.
      const NUNetRefHdl& defNetRef = flow->getFLNode()->getDefNetRef();
      bool kills = assign->queryBlockingKills(defNetRef, &NUNetRef::covered, 
                                              netRefFactory);

      NUExpr* rhs = assign->getRvalue();
      NUIdentRvalue* ident = dynamic_cast<NUIdentRvalue*>(rhs);
      if (!ident) {
        // not a simple assign
        return Result(new ControlTree(flow), kills);
      }
      
      // Make sure we don't have any dynamic addressing on the LHS.
      // This code is here to avoid the FLExprFaninClassifier code
      // that tries to add more control conditions for things like
      // ternaries. We can't do that if there are uses on the LHS
      // because they won't be accounted for in the exclusivity test
      // unless they are represented in the control tree.
      NULvalue* lhs = assign->getLvalue();
      NUNetSet lhs_uses;
      lhs->getUses(&lhs_uses);
      if (not lhs_uses.empty()) {
        // LHS has uses; not a simple assign.
        return Result(new ControlTree(flow), kills);
      }

      // Make sure the rhs supplies all required bits. We should
      // document why this is here. It was added by Jeff with a big
      // commit.
      NUNet* net = ident->getIdent();
      if (net->getBitSize() != lhs->getBitSize()) {
         // not properly sized
        return Result(new ControlTree(flow), kills);
      }

      // Build control trees for each fanin due to the RHS
      bool stopHere = false;
      FLNodeElabSet flows;
      FLExprFaninClassifier<FLNodeElab> classifier(flow, rhs);
      for (FLExprFaninClassifier<FLNodeElab>::FlowLoop loop = classifier.loopExprFanin();
           !loop.atEnd();
           ++loop)
      {
        FLNodeElab* fanin = *loop;
        flows.insert(fanin);

        NUUseDefNode* faninUseDef = fanin->getUseDefNode();
        // If the rhs is a bound node or continuous driver, stop here
        if (!faninUseDef || faninUseDef->isContDriver())
          stopHere = true;
        else
        {
          // If the rhs does not def a full net, stop here
          NUNetRefHdl netRef = fanin->getFLNode()->getDefNetRef();
          if (!(*netRef).all())
            stopHere = true;
        }
      }
      if (stopHere) {
        return Result(new ControlTree(flow), kills);
      } else {
        Result result = buildTreeForMultipleFlows(flow, flows, orderMap,
                                                  netRefFactory);
        kills &= result.allAssignsKill();
        return Result(result.getTree(), kills);
      }
    }
    break;

    default:
    {
      // Anything else is disqualified. But lets try to get the kill
      // information for some of these correct
      bool kills = allLeafAssignmentsKill(flow, netRefFactory);
      return Result(NULL, kills);
    }
  }
  return Result(NULL, false);
}

//! Static private utility function for merging multiple control trees
ControlTree::Result
ControlTree::buildTreeForMultipleFlows(FLNodeElab* parent, FLNodeElabSet& flows,
                                       NucleusOrderMap& orderMap,
                                       NUNetRefFactory* netRefFactory)
{
  bool makeLeaf = false;
  bool kills = true;
  int flowCount = 0;
  NUNetRefHdl defNetRef;
  ControlTree* tree = NULL;
  OrderedTreeVector trees;

  // Create the control trees for each flow first. Under some
  // conditions we give up and decide to create a leaf node for these
  // sets of flows. They are:
  //
  // 1. Building the sub-control tree fails.
  //
  // 2. The various flows def different bits of the net. This breaks
  //    the control tree merging code.
  //
  // 3. Any of the sub-control trees contain assigns that don't kill
  //    the net. This invalidates the def net ref test in item 2.
  for (Loop<FLNodeElabSet> l(flows); !l.atEnd(); ++l) {
    FLNodeElab* nested = *l;
    ++flowCount;
    if (flowCount > 1) {
      // Not the first flow, validate the net refs (item 2 above)
      NUNetRefHdl thisNetRef = nested->getFLNode()->getDefNetRef();
      makeLeaf = (*thisNetRef != *defNetRef);

    } else {
      // First flow, store the net ref for future checking
      defNetRef = nested->getFLNode()->getDefNetRef();
    }

    if (!makeLeaf) {
      // Create the tree for this sub flow
      Result result = buildTreeForSingleFlow(nested, orderMap, netRefFactory);
      tree = result.getTree();
      kills &= result.allAssignsKill();

      // If we failed to create the tree (item 1 above) or if one of
      // these flows does not kill in all assignments, then give up.
      if (!tree) {
        makeLeaf = true;
      } else if (!kills && (flowCount > 1)) {
        makeLeaf = true;
        delete tree;
      } else {
        // So far so good, add it to the set of trees to merge
        UInt32 order = orderMap[nested->getUseDefNode()];
        NU_ASSERT(order != 0, nested->getUseDefNode());
        trees.push_back(OrderedTree(order,tree));
      }    
    }
  }

  // If we gave up, create a leaf and destroy the created trees
  if (makeLeaf) {
    ControlTree* leaf = NULL;

    // if the flow node is a block, use it -- otherwise build a multi-flow leaf
    NUBlock* block = dynamic_cast<NUBlock*>(parent->getUseDefNode());
    if (block)
      leaf = new ControlTree(parent);
    else
      leaf = new ControlTree(flows.begin(), flows.end());

    // delete the control trees and return the leaf
    for (OrderedTreeVector::iterator iter = trees.begin();
         iter != trees.end();
         ++iter) {
      OrderedTree& pair = *iter;
      delete pair.second;
    }
    
    return Result(leaf, kills);
  }

  // Otherwise, sort the trees into order and merge them to generate a
  // result tree
  std::sort(trees.begin(), trees.end());
  tree = NULL;
  for (OrderedTreeVector::iterator iter = trees.begin();
       iter != trees.end();
       ++iter) {
    OrderedTree& pair = *iter;
    if (tree == NULL) {
      tree = pair.second;
    } else {
      pair.second->merge(tree);
      delete tree;
      tree = pair.second;
    }
  }

  // At this point we merged the tree so we better have kills a the
  // leaf assignments or only a single flow node
  FLN_ELAB_ASSERT(kills || (flowCount == 1), parent);
  return Result(tree, kills);
}

//! Less than operator for sorting (stability of output)
int ControlTree::compare(const ControlTree* ct1, const ControlTree* ct2)
{
  // Make sure they are the same time
  if (ct1->isBranch() && !ct2->isBranch()) {
    return -1;
  } else if (!ct1->isBranch() && ct2->isBranch()) {
    return 1;
  }

  // For branches we compare the sub control trees
  if (ct1->isBranch()) {
    // Compare by the branch flows first
    FLNodeElab* flowElab1 = ct1->getBranchFlow();
    FLNodeElab* flowElab2 = ct2->getBranchFlow();
    ptrdiff_t cmp1 = FLNodeElab::compare(flowElab1, flowElab2);
    int cmp = 0;
    if (cmp1 < 0) {
      cmp = -1;
    } else if (cmp1 > 0) {
      cmp = 1;
    }

    // Check the then branch next
    if (cmp == 0) {
      if ((ct1->mTrue != NULL) && (ct2->mTrue != NULL)) {
        cmp = *ct1->mTrue < *ct2->mTrue;
      } else if (ct1->mTrue != NULL) {
        cmp = -1;
      } else if (ct2->mTrue != NULL) {
        cmp = 1;
      }
    }

    // Check the else branch next
    if (cmp == 0) {
      if ((ct1->mFalse != NULL) && (ct2->mFalse != NULL)) {
        cmp = *ct1->mFalse < *ct2->mFalse;
      } else if (ct1->mFalse != NULL) {
        cmp = -1;
      } else if (ct2->mFalse != NULL) {
        cmp = 1;
      }
    }

    return cmp;
  } else {
    // For leafs, get a representative elaborated flow for each leaf
    FLNodeElabVector nodes1;
    FLNodeElabVector nodes2;
    for (FLNodeElabSet::const_iterator i = ct1->beginLeafFlows();
         i != ct1->endLeafFlows(); ++i) {
      FLNodeElab* flowElab = *i;
      nodes1.push_back(flowElab);
    }
    for (FLNodeElabSet::const_iterator i = ct2->beginLeafFlows();
         i != ct2->endLeafFlows(); ++i) {
      FLNodeElab* flowElab = *i;
      nodes2.push_back(flowElab);
    }
    std::sort(nodes1.begin(), nodes1.end(), FLNodeElabCmp());
    std::sort(nodes2.begin(), nodes2.end(), FLNodeElabCmp());

    // Pick the first elaborated flow and sort them
    FLNodeElab* flowElab1 = *nodes1.begin();
    FLNodeElab* flowElab2 = *nodes2.begin();
    ptrdiff_t cmp1 = FLNodeElab::compare(flowElab1, flowElab2);
    if (cmp1 < 0) {
      return -1;
    } else if (cmp1 > 0) {
      return 1;
    } else {
      return 0;
    }
  }
} // int ControlTree::compare

bool
ControlTree::allLeafAssignmentsKill(FLNodeElab* flowElab, 
                                    NUNetRefFactory* netRefFactory)
{
  // Iterate down and if we find any leaf assignments that don't kill,
  // return. We are looking for any leaf statements that introduce UD
  // pessimism.
  //
  // If this has nested flow we get the kill information from
  // below. Otherwise, we get it from here.
  bool kills = true;
  if (flowElab->hasNestedBlock()) {
    // Check the neeted flow
    for (FLNodeElabLoop l = flowElab->loopNested(); !l.atEnd() && kills; ++l) {
      FLNodeElab* nested = *l;
      kills = ControlTree::allLeafAssignmentsKill(nested, netRefFactory);
    }

  } else {
    // This is a leaf node. It must be a statement. If not, it should
    // be accounted for here.
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    NUUseDefStmtNode* stmtNode = dynamic_cast<NUUseDefStmtNode*>(useDef);
    FLN_ELAB_ASSERT(stmtNode != NULL, flowElab);

    // Make sure all that is def'ed is killed. Otherwise we have pessimism.
    const NUNetRefHdl& defNetRef = flowElab->getFLNode()->getDefNetRef();
    kills = stmtNode->queryBlockingKills(defNetRef, &NUNetRef::covered, 
                                         netRefFactory);
  }

  return kills;
}
