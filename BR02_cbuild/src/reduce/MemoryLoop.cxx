// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/MemoryLoop.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUAssign.h"
#include "util/UtHashSet.h"

class NUFor;

REMemoryLoop::REMemoryLoop (MsgContext* msg)
  : mMsgContext (msg), mLoopDepth (0) {}

REMemoryLoop::~REMemoryLoop () {}

/*!
 * Track assignments to memories to detect writes inside for-loops and
 * to count approximate number of write-ports required for StateUpdate
 * temp memories.  This pass comes AFTER unrolling so that we accurately count
 * writes and don't mark things as requiring loops that end up being linearized.
 */

class MemoryWriteAnalysis : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  MemoryWriteAnalysis (bool verbose): 
    mLoopDepth (0),
    mWalker(NULL),
    mLoopTaskWalker(NULL),
    mVerbose(verbose)
  {
    mTempMems = new TempMems;
  }

  ~MemoryWriteAnalysis () {

    INFO_ASSERT(mLoopDepth == 0,
                "Inconsistency found while analyzing memory writes");
    delete mTempMems;
  }

  void limitStaticPortSize() {
    for (TempMems::SortedLoop p = mAllTempMems.loopSorted(); !p.atEnd(); ++p) {
      NUTempMemoryNet* tmem = *p;

      // We should probably make this parameterized.  But we have
      // an numPorts^2 issue with StaticTempMemories starting with
      // our Verilog 2001 support.  We use a hash-table now for
      // DynTempMemories,  So once we get over a certain port threshold,
      // we want to implement TempMemories as dynamic.
      if (tmem->getNumWritePorts() >= 8) {
        tmem->putIsDynamic();
      }

      if (mVerbose) {
        UtString netbuf;
        tmem->composeUnelaboratedName(&netbuf);
        UtIO::cout() << "Temp Memory " << netbuf << ", ports="
                     << tmem->getNumWritePorts() << " dynamic="
                     << tmem->isDynamic() << "\n";
      }
    }
  } // void limitStaticPortSize

  Status operator()(Phase, NUBase*) { return eNormal;}

  Status operator()(Phase phase, NUFor*) {
    if (phase == ePre)
      ++mLoopDepth;
    else
      --mLoopDepth;
    return eNormal;
  }

  //! Walk the declared nets to find the temp memories
  Status operator()(Phase phase, NUNet* net)
  {
    if (phase == ePre) {
      NUTempMemoryNet* tmem = dynamic_cast<NUTempMemoryNet*>(net);
      if (tmem != NULL) {
        mTempMems->insert(tmem);
        mAllTempMems.insert(tmem);
      }
    }
    return eNormal;
  }

  //! Clear the temp mems at the end of every structured proc
  Status operator()(Phase phase, NUStructuredProc*)
  {
    if (phase == ePost) {
      mTempMems->clear();
    }
    return eNormal;
  }

  //! We have to walk inside the tasks to see if they write memories.
  /*! The design walker only visits the task enable but not the task
   *  body. So we have to intercept a call and force it look into the
   *  task.
   */
  Status operator()(Phase phase, NUTaskEnable* taskEnable) {
    if (phase == ePost)
      return eNormal;

    NU_ASSERT(mWalker and mLoopTaskWalker, taskEnable);

    // Check if we have a many resolutions hier ref. We treat this
    // like we came from a for loop.
    if (taskEnable->isHierRef()) {
      NUTaskHierRef* taskHierRef = taskEnable->getHierRef();
      if (taskHierRef->resolutionCount() != 1) {
        NUTaskHierRef::TaskVectorLoop l;
        ++mLoopDepth;
        for (l = taskHierRef->loopResolutions(); !l.atEnd(); ++l) {
          NUTask* task = *l;
          mLoopTaskWalker->task(task);
        }
        --mLoopDepth;
        return eNormal;
      }
    }

    // Normal task or we have one resolution
    NUTask* task = taskEnable->getTask();
    if (mLoopDepth) {
      mLoopTaskWalker->task(task);
    } else {
      mWalker->task(task);
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUAssign* assign)
  {
    // For temporary memories, skip the assignment that initializes it
    // and the one that copies the data back. We can tell if it is the
    // init because it is the first assignment to the temp memory and
    // the rhs is the whole master memory. And we can tell it is the
    // copy back because the assignment is from the temp mem into the
    // original memory.
    if (phase == ePre) {
      // Check for temp mem init
      NUTempMemoryNet* tmem;
      if (assign->isTempMemInit(&tmem) && toggleUninitializedTempMem(tmem)) {
        return eSkip;
      }
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUIdentLvalue* lvalue) {
    if (phase == ePost)
      return eNormal;

    NUNet* net = lvalue->getWholeIdentifier ();

    if (net->is2DAnything ()) {
      if (NUMemoryNet* mem = dynamic_cast<NUMemoryNet*>(net))
        mem->incrementWritePorts(mem->getDepth ());
      else if (NUTempMemoryNet *tmem = dynamic_cast<NUTempMemoryNet*>(net))
        tmem->incrementWritePorts(tmem->getDepth ());
    }
    return eNormal;
  }

  Status operator()(Phase phase, NUMemselLvalue* memval) {
    if (phase == ePost)
      return eNormal;

    NUMemoryNet* mem = dynamic_cast<NUMemoryNet*>(memval->getIdent ());
    if (mem != NULL) 
    {
      if (mLoopDepth)
	mem->putWrittenInForLoop (true);
      else
	mem->incrementWritePorts (1);
    } 
    else 
    {
      NUTempMemoryNet* tmem = dynamic_cast<NUTempMemoryNet*>(memval->getIdent());
      NU_ASSERT(tmem, memval);
      if (mLoopDepth)
      {
        tmem->putIsDynamic();
        // Must mark the master memory as dynamic because we won't know how
        // many write ports it needs.
        tmem->getMaster ()->putWrittenInForLoop (true);
      }
      else
        tmem->incrementWritePorts (1);
    }

    // Do not walk into the identifier (it's an NUIdentLValue and should not
    // trigger a write-port update).

    // Do not walk into the index expression (it's an NUExpr and is
    // not a write).
    return eSkip;
  }


  void setWalker(NUDesignWalker * walker) {
    INFO_ASSERT(mWalker == NULL,
                "Should not modify the walker for memory write analysis");
    mWalker = walker;
  }

  void setLoopTaskWalker(NUDesignWalker * walker) {
    INFO_ASSERT(mLoopTaskWalker == NULL,
                "Should not modify task walker for memory write analysis");
    mLoopTaskWalker = walker;
  }
    
private:
  // Hide copy constructors and assignment
  MemoryWriteAnalysis (const MemoryWriteAnalysis&);
  MemoryWriteAnalysis& operator=(const MemoryWriteAnalysis&);

  // Count loop depth
  UInt32 mLoopDepth;

  //! Walker for normal constructs and non-hierref tasks.
  NUDesignWalker * mWalker;

  //! Abstraction to keep track of temp nets
  typedef UtHashSet<NUTempMemoryNet*> TempMems;

  //! Keep a set of temp mems per always block
  TempMems* mTempMems;

  //! Keep track of all the design tempmemories for verbose spew
  TempMems mAllTempMems;

  //! Check if a temp memory has not been initialized yet
  /*! All temp memories should be initialized once. So we only want to
   *  detect it once. Therefore, if we find it, remove it from the set
   *
   *  JDM: the semantic of this routins is that it's a toggle, so
   *  I changed its name
   */
  bool toggleUninitializedTempMem(NUTempMemoryNet* tmem)
  {
    TempMems::iterator pos = mTempMems->find(tmem);
    if (pos != mTempMems->end()) {
      mTempMems->erase(pos);
      return true;
    } else {
      return false;
    }
  }

  //! Walker for hierref tasks.
  NUDesignWalker * mLoopTaskWalker;

  //! do verbose spew?
  bool mVerbose;
};


void
REMemoryLoop::design(NUDesign * the_design, bool verbose)
{
  MemoryWriteAnalysis visitor(verbose);

  NUDesignWalker loop_walker (visitor, false);
  NUDesignWalker walker (visitor, false);

  visitor.setWalker(&walker);
  visitor.setLoopTaskWalker(&loop_walker);

  walker.putWalkDeclaredNets(true);
  walker.design (the_design);
  visitor.limitStaticPortSize();
}
