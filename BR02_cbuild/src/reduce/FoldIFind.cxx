// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file 
  Simple constant folding pass.  FindFirst/FindLast pattern
  recognition.  The functions here generally try to convert various
  Nucleus trees into either a single findbit operation, or into a case
  statement indexed by the findfirst.  Case-statement bit inferencing
  is expected to further improve case statements if they are simple
  enough.
 */
#include "FoldI.h"

enum SpecialMatches
{
  eAllOnesCase = -1,        // "case" or "if" has test for all ones
  eAllZerosCase = -2};      // tests for all zeros...

//! Track deducing the type of find we're doing.
// 
// Note that we depend on the enum FindType allowing bitwise-Or to identify
// invalid directions (e.g. a value of 3 would imply rejecting this pattern
// as a find candidate.
//
inline
void sSetFindDirection (FoldI::FindType* fp, bool cond, FoldI::FindType t,
                        FoldI::FindType f=FoldI::eNone)
{
  *fp = FoldI::FindType (*fp | (cond ? t : f));
}

//! Walk case filling in vector of <pattern,action> values
/*!
 * Examine \a caseStmt to see if it can be turned into a FFO, FFZ,
 * FLO, FLZ operation Fill in the ConditionVector \a v with
 * the case condition and actions in the case.
 *
 * \Returns true if the case stmt is properly formed and false otherwise.
 */
bool
FoldI::walkCase (const NUCase* caseStmt, CondActionVector& v,
                 bool* findingOnes, FindType* finding) const
{
  bool firstCase = true;
  bool testedBit = false;             // looking for set or clear?

  for (NUCase::ItemCLoop items = caseStmt->loopItems ();
       !items.atEnd ();
       ++items)
  {
    SInt32 ithIndex;            // the bit we're looking at

    const NUCaseItem* caseItem = *items;
    if (caseItem->isDefault ())
    {
      // If we don't have a pattern for no findable bits (all zeros or
      // all ones), then we can make this default into our find-fails
      // action
      ithIndex = (testedBit)? eAllZerosCase : eAllOnesCase;
    }
    else
    {
      UInt32 condCount = 0;
      for(NUCaseItem::ConditionCLoop cond = caseItem->loopConditions ();
          !cond.atEnd ();
          ++cond)
      {
        ++condCount;

        // Check that the condition is either a single bit test or a test
        // for ALL ones (findingzeroes) or ALL zeros (findingones)
        bool ithTest;
        if (not evalCondition (*cond, &ithIndex, &ithTest, finding))
          return false;

        if (firstCase)
        {
          firstCase = false;
          *findingOnes = testedBit = ithTest;
        }
        else if (ithTest != testedBit)
          return false;
      }
      if (condCount != 1)
        return false;          // No luck
    }

    // Remember this index:action pair
    v.push_back (CondActionPair (ithIndex, caseItem->loopStmts ()));
  }

  if (v.empty ())
    return false;

  return true;
}

/*!
 * Given a single case-condition value, examine it to see if it of the form to be part
 * of a bit-finding operation.
 *
 * Returns: true|false indicating condition is properly formed.
 *          modifies \a indexValue to be the bit number being tested (0..N-1)
 *                      or the special values for eAllZerosCase and eAllOnesCase
 *          modifies \a testedBit to indicate if we are matching a 'b1 or 'b0
 *          modifies \a finding to indicate the deduced FindType
 */
bool
FoldI::evalCondition ( const NUCaseCondition* p,
                       SInt32* indexValue,
                       bool* testedBit, FindType* finding) const
{

  DynBitVector cond, mask, condx0, condx1;
  const NUConst *e = p->getExpr ()->castConst ();
  if (e)
    e->getSignedValue (&cond);
  else
    return false;

  if (NUExpr *maskExpr = p->getMask ())
  {
    e = maskExpr->castConst ();
    if (e)
      e->getSignedValue (&mask);
    else
      mask.set ();
  }
  else
    mask.set ();

  mask.resize (cond.size ());
  condx0 = cond;                // Build version of COND mask with x's as zero (for find-one)
  condx0 &= mask;               // clears the x's
  condx1 = cond;                // Inverted for find-zero
  condx1.flip ();
  condx1 &= mask;               // mirror image of cond bit mask....

  UInt32 testedBits = mask.count ();
  UInt32 condx1Count = condx1.count ();
  UInt32 condx0Count = condx0.count ();
  UInt32 condCount = cond.count ();
  if (testedBits == mask.size ())
  {
    // No 'X's in the pattern to match
    if (cond == 0)
    {
      *indexValue = eAllZerosCase;
      *testedBit = true;           // finding ones if we default on zeros
    }
    else if (condCount == cond.size ())
    {
      *indexValue = eAllOnesCase;
      *testedBit = false;       // finding zeros if we default on ones
    }
    else if (condCount == 1
             && (cond.test (testedBits-1) || cond.test (0)))
    {
      // Must be a border bit - either 10000 or 00001
      *indexValue = (cond.test (0) ? (cond.size ()-1) : 0);
      *testedBit = true;
      sSetFindDirection (finding, cond.test (0), eFindLeading, eFindFirst);
    }
    else if (condCount == (testedBits-1)
             && (!cond.test (testedBits-1) || !cond.test (0)))
    {
      // Must be border bit - either 01111 or 11110
      *indexValue = (cond.test (0) ? 0 : (cond.size ()-1));
      *testedBit = false;
      sSetFindDirection (finding, cond.test (0), eFindFirst, eFindLeading);
    }
    else
      return false;             // not well formed for case/if => findbit
  }
  else if (testedBits == 1)
  {
    // e.g either xxx1xx or xxxx0xx
    *indexValue = mask.findFirstOne ();
    *testedBit = cond.test (*indexValue);
  }
  else if (condx0Count == 1 && condx1Count == 1)
  {
    // disambiguate among 8'b10xxxxxx, 8'b01xxxxxx, 8'bxxxxxx10, 8'bxxxxxx01
    UInt32 border = 0;
    if (mask.test (0)) {
      // low bits are NOT x
      border = mask.findLastOne ();
      sSetFindDirection (finding, true, eFindFirst);
    }  else {
      // low bits are x
      border = mask.findFirstOne ();
      sSetFindDirection (finding, true, eFindLeading);
    }

    *indexValue = border;
    *testedBit = cond.test (border);

  } else if (condx0Count == 1) {
    *indexValue = condx0.findFirstOne ();
    *testedBit = true;

    sSetFindDirection (finding, mask.test (0), eFindFirst, eFindLeading);
  }
  else if (condx1Count == 1)
  {
    *indexValue = condx1.findFirstOne ();
    *testedBit = false;
    sSetFindDirection (finding, mask.test (0), eFindFirst, eFindLeading);
  }
  else
    // Not well formed casex constant pattern
    return false;

  return true;
}

/*!
 * Construct a new case statement using the \a selector to apply a bit-finding operator to,
 * the \a condAct to provide the index:action pairs, and the \a findFirst and \a findingOnes
 * flags to control what kind of a bit-finding operation we generate.  \a repl is the statement
 * we are replacing (which might not be a case statement)
 *
 * \Returns a new case statement suitable for case-inferencing to optimize into a single
 * assignment of the form: dest = FFO(selector);
 */
NUStmt*
FoldI::constructFindBitCase (NUExpr *selector, CondActionVector& condAct,
                             bool findFirst, bool findingOnes,
                             ConstantRange & wholeRange,
                             NUStmt* repl)
{
  const SourceLocator& loc (repl->getLoc ());

  NUOp::OpT opcode = findFirst ? (findingOnes ? NUOp::eUnFFO : NUOp::eUnFFZ)
    			       : (findingOnes ? NUOp::eUnFLO : NUOp::eUnFLZ);

  NUExpr *bitfind = new NUUnaryOp (opcode, selector, loc);

  // Construct the case statement
  NUCase* theCase =
    new NUCase (bitfind, getFactory (), NUCase::eCtypeCase, repl->getUsesCFNet (), loc);

  // Convert the worklist into the case conditions and actions.
  for(CondActionVector::iterator i = condAct.begin ();
      i != condAct.end ();
      ++i)
  {
    SInt32 index = (*i).first;
    if (index == eAllOnesCase
      || index == eAllZerosCase)
    {
      index = -1;               // findbit returns -1 for failure
      theCase->putFullySpecified (true);
    }
    else
      // Adjust if-ranges so that bit range is corrected
      index -= wholeRange.getLsb ();

    NUStmtCLoop& actions ((*i).second);

    NUExpr* caseIndex = NUConst::create (false, index, 
                                         LONG_BIT, loc);
    NUCaseCondition *cond = new NUCaseCondition (loc, caseIndex);

    NUCaseItem* newItem = new NUCaseItem (loc);
    while (not actions.atEnd ())
    {
      newItem->addStmt ((*actions)->copy (getCC ()));
      ++actions;
    }

    theCase->addItem (newItem);

    newItem->addCondition (cond);
  }

  return theCase;
}


// Look for a casex where the selector patterns look like a findfirst   
// or findlast
//
// Just transform casex(sel) into case(FFO(sel))
//
// Returns caseStmt or simplified equivalent

NUStmt*
FoldI::casexFind (NUCase* caseStmt)
{
  // First check that there are caseselector.getBitSize() case items
  // (and possibly a "default") and that each non-default action has only
  // a single conditional test
  //
  const NUExpr * selector = caseStmt->getSelect ();
  SInt32 caseWidth = selector->getBitSize ();

  CondActionVector condVec;
  bool findingOnes = false;     // finding ones or zeros?
  FindType finding = eNone;     // No idea if FF* or FL* yet.

  if (not walkCase (caseStmt, condVec, &findingOnes, &finding))
    // Check that this is well-formed for conversion to find bit operation
    return caseStmt;
  
  // Insure that we have the right number of case-items.  Should be
  // one-per-bit of the width of the selector plus an optional no-bits
  // pattern
  UInt32 numCases = condVec.size ();
  if (numCases < 4              // Not enough to make it interesting?
      || numCases > UInt32(caseWidth + 1)
      || numCases < UInt32 (caseWidth))
    return caseStmt;

  // Look at the conditions to see if we are find-first or find-last
  // and if we're looking for zeros or ones...

  CondActionVector::const_iterator firstAction = condVec.begin ();
  SInt32 initialIndex = (*firstAction++).first;
  SInt32 expectedIndex = initialIndex;
  SInt32 deltaIndex = initialIndex - (*firstAction).first;

  if (std::abs (deltaIndex) != 1)
    return caseStmt;            // non adjacent and monotonic...

  sSetFindDirection (&finding, (expectedIndex == 0), eFindFirst, eFindLeading);

  if (finding != eFindFirst && finding != eFindLeading)
    // conflicting deductions
    return caseStmt;

  // Remember if we found a findbit default (either all ones for
  // findzero or all zeros for findone)
  bool foundDefault = false;

  // Now check that vector is appropriately increasing or decreasing
  for (CondActionVector::const_iterator i = condVec.begin ();
       i!=condVec.end ();
       ++i)
  {
    
    // get the ith element and see if it matches our predictions
    // concerning direction and pattern to be matched.
    SInt32 ithIndex = i->first;

    // Look to see if we've got a all-ones mask while finding zeros) or a
    // all-zeros mask while finding ones - THIS is a potential fail case for
    // the Find bit...
    //
    if (foundDefault)
      // Can't have already found a default - it MUST be the last
      // matching pattern
      return caseStmt;

    if (ithIndex == eAllOnesCase || ithIndex == eAllZerosCase)
    {
      foundDefault = true;
      continue;                 // Default wil be LAST entry
    }

    if (ithIndex != expectedIndex)
      // not the expected adjacent bit
      return caseStmt;

    expectedIndex -= deltaIndex;
  }

  // If we reach here, we completely matched for the find pattern.
  // Now build the correct one
  ConstantRange wholeRange ((condVec.size () - (foundDefault)) - 1, 0);

  NUExpr* newSelector = new NUVarselRvalue (selector->copy(getCC()), wholeRange, caseStmt->getLoc());
  newSelector->resize (wholeRange.getLength ());


  NUStmt *result = constructFindBitCase (newSelector, condVec, finding == eFindFirst, findingOnes, wholeRange, caseStmt);

  return finish (caseStmt, result);
}

// Methods to recognize patterns in if-then-else
//
// Look for
//      if (a[0])
//        x = 0;
//      else if (a[1])
//        x=1;
//      else if (a[2])
//        x=2;
//      else if (a==0)
//        x = -1

const NUExpr* FoldI::getBitTested (const NUExpr* cond, bool findingOnes) const
{
  if (findingOnes) {

    if (cond->getType () == NUExpr::eNUVarselRvalue)
    {
      // Only allow primary form
      const NUVarselRvalue *v = dynamic_cast<const NUVarselRvalue*>(cond);
      if (v->getRange ()->getLength () != 1)
        return NULL;
      return cond;
    }
    else
      // TODO: Handle (x[k] == 1) and (x & 1<<k) == 1
      return NULL;
  }

  // Looking for zeros.  Allow x[k] == 0 or !x[k]
  if (const NUBinaryOp *bop = dynamic_cast<const NUBinaryOp*>(cond))
  {
    if (bop->getOp () == NUOp::eBiEq)
    {
      if (const NUConst *c = ctce (bop->getArg (1)))
        if (c->isZero ())
          return getBitTested (bop->getArg (0), false);

      return NULL;
    } 
    else if (bop->getOp () == NUOp::eBiNeq)
    {
      if (const NUConst *c = ctce (bop->getArg (1)))
        if (c->isOnes ())
          return getBitTested (bop->getArg (0), false);
      return NULL;
    }
  } else if (const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(cond))
  {
    if (uop->getOp () == NUOp::eUnLogNot)
      return getBitTested (uop->getArg (0), findingOnes);
  }

  return NULL;
}

// Return success if the \a ifStmt appears to be a find-bit pattern, searching for
// a \a findingOnes.
// We need to see the first two conditionals before we can predict if this is
// a find-leading or find-trailing operation, so we pass in \a lastBitSeen to
// indicate the previous bit tested, and we return \a lastIndex indicating what
// bit ended the sequence, \a findFirst is updated to indicate whether we're doing a
// FF or FL, and \a workList contains the index:action pair
// 
bool
FoldI::walkIf (const NUIf* ifStmt, bool findingOnes, bool* findFirst,
               NUNetRefHdl& lastBitSeen, SInt32 *lastIndex,
               CondActionVector& workList) const
{
  // First find if we are testing a single bit.
  const NUExpr* cond = getBitTested (ifStmt->getCond (), findingOnes);

  if (not cond)
    return false;

  // Now get the UD information for this bit
  NUNetRefSet uses (getFactory ());
  cond->getUses (&uses);

  NUNetRefSet::const_iterator aUse = uses.begin ();
  NU_ASSERT(!uses.empty(), ifStmt);
  NUNetRefHdl thisBit = *aUse;
  ConstantRange expectedRange;
  lastBitSeen->getRange (expectedRange);

  ConstantRange thisRange;
  thisBit->getRange (thisRange);

  if (workList.size () == 1)
  {
    // Now we can predict what direction we're going in
    if (*lastIndex == (thisRange.getLsb () - 1))
      *findFirst = true;
    else if (*lastIndex == (thisRange.getLsb () + 1))
      *findFirst = false;
    else
      // Not adjacent - no further interest
      return false;
  }

  if (workList.empty ())
  {
    // No prediction possible
  }
  else
  {
    // Check that our predictions are correct
    expectedRange.adjust (*findFirst ? 1 : -1);

    if (not thisBit->adjacent (*lastBitSeen) // not adjacent
        || thisRange != expectedRange) // wrong bit
      return false;
  }

  // We are testing the properly adjacent bit to whatever
  // we last tested. Remember what it was.
  *lastIndex = expectedRange.getLsb ();

  // Remember this case:action pair
  workList.push_back (CondActionPair (*lastIndex, ifStmt->loopThen ()));

  // Now check for further chain members..

  if (ifStmt->emptyElse ())
    // No more work, no default, quit while we're ahead...
    // If we've only pushed ONE CondActionPair onto the list, we
    // don't have a direction prediction AND it's not a very interesting
    // candidate for FFO
    return workList.size () > 1;

  NUStmtCLoop elseStmts = ifStmt->loopElse ();
  
  const NUStmt* stmt = *elseStmts;
  
  if (stmt->getType () == eNUIf)
  {
    ++elseStmts;
    if (not elseStmts.atEnd ())
      return false;             // can't go any further...

    // Keep looking for additional bit testing
    const NUIf* ifElse = dynamic_cast<const NUIf*>(stmt);
    if (ifElse == NULL) {
      return false;
    }
    return walkIf (ifElse, findingOnes, findFirst, thisBit, lastIndex, workList);
  }

  // We have an else-statement block that's not an "if" - this is
  // going to be our "no bits set" default action
  workList.push_back (CondActionPair (findingOnes ? eAllZerosCase : eAllOnesCase,
                                      elseStmts));
  return true;
}

// Determine the potential size of a FFO/FFZ/FLO/FLZ and the direction.
// Returns:
//    ZERO if not in the form of a bit-finder.
//    -1   if an invalid pattern found (like bits were examined out of order)
//    <num> the width of the FF* or FL*
//
//  \a *findFirst is set to -1 if FL* pattern recognized or +1 for FF* pattern
//  \a **other is set to the last constant found to be checked by the caller
SInt32 
FoldI::walkQCTree (const NUVarselRvalue* lastBit, // previous bit being tests X[k]
                   const NUExpr* exprTree,  // remainining unexamined ?: tree
                   bool onesFlag,           // is this a FFO|FLO 
                   SInt32 lastVal,	    // integer value of match
                   int* findFirst,          // returned indicator FL*=-1, FF*=+1
                   const NUExpr **other)    // last term (the no bit found value...)
{
  if (exprTree->getType () != NUExpr::eNUTernaryOp) {
    return 0;
  }

  const NUTernaryOp* cond = dynamic_cast<const NUTernaryOp*>(exprTree);
  if (cond->getArg (0)->getType () != NUExpr::eNUVarselRvalue)
    // Not a BIT test
    return -1;

  const NUVarselRvalue* nextBit = dynamic_cast<const NUVarselRvalue*>(cond->getArg (0));

  const ConstantRange &lastRange (*(lastBit->getRange ()));
  const ConstantRange &nextRange (*(nextBit->getRange ()));

  // Confirm that we are stepping through the bits one by one and in a consistent direction

  if (!(*(nextBit->getIdentExpr ()) == *(lastBit->getIdentExpr ())
        && *(nextBit->getIndex ()) == *(lastBit->getIndex ())
        && nextRange.adjacent (lastRange))) {
    return -1;     // Bit being tested is NOT adjacent.
  }

  // Compute the order we're visiting bits, where
  // next < last => -1, next > last => 1
  SInt32 currentDirection = nextRange.compare (lastRange);

  if ( 0 == *findFirst ){
    *findFirst = currentDirection; // No direction identified yet, save now for checking in all future (recursive) uses
  }

  if (*findFirst != currentDirection) {
      return -1;                // we must always work through the bits in the same direction
  }


  SInt32 width = 0;
  // Now check that the value that is returned is also being incremented by 1 and always in the same direction

  const NUExpr* const_expr     = (onesFlag ? cond->getArg (1) : cond->getArg (2));
  const NUExpr* remaining_expr = (onesFlag ? cond->getArg (2) : cond->getArg (1));


  const NUConst *c = ctce (const_expr);
  if (!c) {
    return -1;                  // not a constant, so this is invalid
  }
  SInt32 v;
  if (!c->getL (&v)) {
    return -1;                  // unable to get constant value
  }

  SInt32 delta = v - lastVal;

  if (std::abs (delta) != 1) {
    return -1;                 // non-uniform, the constants must change by 1(or -1) for each term
  }
  if ( currentDirection != delta ){
    return -1;                  // found that the direction marching through the bits does not match the direction that the values change
  }
  

  const NUConst* remaining_const = ctce (remaining_expr);
  if (remaining_const) {
    *other = remaining_expr;    // pass constant back to top level of recursion for checking of this value
    return 1;                   // recursion has finished
  } else {
    width = walkQCTree (nextBit, remaining_expr, onesFlag, v, findFirst, other);
  }

  if (width < 0) {
    return -1;                  // recognition via recursion failed
  } else {
    return width + 1;
  }
}

NUStmt*
FoldI::ifthenFindBit (NUIf* ifstmt)
{
  if (not isUDValid () || not isUDKillable ())
    return ifstmt;

  NUExpr * cond = ifstmt->getCond ();

  // Use UD to see if the first condition involves a SINGLE net and one bit.
  //
  NUNetRefSet uses (getFactory ());
  cond->getUses (&uses);
  NUNetRefSet::const_iterator use = uses.begin ();
  if (use == uses.end ())
    return ifstmt;                // No UD available, we're out-of-luck

  NUNetRefHdl firstBit = *use;
  NUNet* firstNet = firstBit->getNet ();
  ++use;
  if (firstNet == NULL          // Can't find the object via UD
      || use != uses.end ())        // more than one net in the condition
    return ifstmt;

  ConstantRange wholeRange;
  firstBit->getRange (wholeRange);
  if (wholeRange.getLength () != 1) // More than one bit being tested
    return ifstmt;
  SInt32 firstIndex = wholeRange.getLsb ();

  bool findingOnes = isPrimary (cond);

  // Now recurse to discover if this is really well formed.  The
  // walker has to determine what direction we're moving in, so it
  // returns both the firstBit flag (true|false) and the firstIndex

  CondActionVector workList;
  SInt32 lastIndex = firstIndex;
  bool findFirst = false;

  if (not walkIf (ifstmt, findingOnes, &findFirst, firstBit,
                  &lastIndex, workList))
    // not well formed at all
    return ifstmt;

  if (findFirst)
    wholeRange.setMsb (lastIndex);
  else
    wholeRange.setLsb (lastIndex);
  
  // Check that there is enough of a sequence here to make the bit-finding
  // code worthwhile - we better have at least 4 bits or the overhead probably
  // doesn't justify the complexity.
  if (wholeRange.getLength () < 4)
    return ifstmt;

  {
    // THIS IS A TEMP WORKAROUND FOR BUG8257
    if (firstNet->isMemoryNet ()) {
      // If the net is a memory net we should map this into a selection on a
      // NUMemSelRvalue.  Currently this is not supported.  
      return ifstmt;
    }
  }

  // Now convert the if-then-else into an equivalent case statement

  NUExpr* selector
    = new NUVarselRvalue (firstNet, wholeRange, ifstmt->getLoc ());
  selector->resize (wholeRange.getLength ());

  NUStmt* result
    = constructFindBitCase (selector, workList, findFirst, findingOnes,
                            wholeRange,
                            ifstmt);

  return finish (ifstmt, result);
}


