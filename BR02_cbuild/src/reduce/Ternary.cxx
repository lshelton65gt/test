// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "localflow/UD.h"

#include "reduce/Ternary.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"

#include "reduce/Inference.h"
#include "reduce/Fold.h"

#include "util/ArgProc.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

Ternary::Ternary(AtomicCache * str_cache,
		 NUNetRefFactory * netref_factory,
		 MsgContext * msg_context,
		 IODBNucleus * iodb,
		 ArgProc * args,
                 bool only_bit_conditions,
                 bool only_balanced_conditions,
                 bool only_bit_definitions,
                 bool fold_results,
		 bool verbose,
		 TernaryStatistics *stats,
		 InferenceStatistics *inference_stats) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mArgs(args),
  mStatistics(stats),
  mOnlyBitConditions(only_bit_conditions),
  mOnlyBalancedConditions(only_balanced_conditions),
  mOnlyBitDefinitions(only_bit_definitions),
  mFoldResults(fold_results),
  mVerbose(verbose)
{
  mFold = new Fold(mNetRefFactory, args, mMsgContext, mStrCache, iodb,
                   mArgs->getBoolValue(CRYPT("-verboseFold")),
                   (mFoldResults ? (eFoldUDValid|eFoldUDKiller) : eFoldUDKiller));

  Inference::Params params (mArgs, mMsgContext);
  mInference = new Inference(mFold, mNetRefFactory, mMsgContext, mStrCache, iodb,
                             mArgs, params, inference_stats);
  mUD = new UD(mNetRefFactory, mMsgContext, iodb, mArgs,
               mArgs->getBoolValue(CRYPT("-verboseUD")));
}

Ternary::~Ternary()
{
  delete mFold;
  delete mInference;
  delete mUD;
}


void Ternary::design(NUDesign *the_design)
{
  NUModuleList mods;
  the_design->getModulesBottomUp(&mods);
  for (NUModuleList::iterator iter = mods.begin();
       iter != mods.end();
       ++iter) {
    module( *iter );
  }
}


void Ternary::module(NUModule * one)
{
  bool success = false;
  TernaryBlock xlate(mNetRefFactory,
                     mFold,
		     mStatistics,
		     mOnlyBitConditions,
                     mOnlyBalancedConditions,
                     mOnlyBitDefinitions,
                     mFoldResults,
		     mVerbose);

  for (NUModule::AlwaysBlockLoop loop = one->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    // Convert to ternaries, recompute UD if necessary
    NUAlwaysBlock * always = (*loop);
    xlate.reset();
    xlate.translate(always->getBlock());
    if (xlate.getRecomputeUD()) {
      mUD->structuredProc(always);
      xlate.putRecomputeUD(false);
    }

    // If changes occured, there may be inference opportunities
    bool this_changed = xlate.success();
    if (this_changed) {
      success = true;
      mInference->always(always);
    }
  }

  if (success) {
    mStatistics->module();
  }
}


bool Ternary::stmts(NUStmtList & stmts)
{
  NUStmtList replacements;
  TernaryBlock xlate(mNetRefFactory,
                     mFold,
		     mStatistics,
		     mOnlyBitConditions,
                     mOnlyBalancedConditions,
                     mOnlyBitDefinitions,
                     mFoldResults,
		     mVerbose);
  bool success = xlate.translate(NUStmtLoop(stmts), replacements);
  if (success) {
    stmts.clear();
    stmts.insert(stmts.end(),replacements.begin(),replacements.end());
  }
  return success;
}


bool Ternary::mergedBlocks(NUModule * /*mod*/, NUUseDefVector & blocks)
{
  bool success = false;
  TernaryBlock xlate(mNetRefFactory,
                     mFold,
		     mStatistics,
		     mOnlyBitConditions,
                     mOnlyBalancedConditions,
                     mOnlyBitDefinitions,
                     mFoldResults,
		     mVerbose);
  for (NUUseDefVector::iterator iter = blocks.begin();
       iter != blocks.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(use_def);
    NU_ASSERT(always!=NULL, use_def);
    xlate.reset();
    xlate.translate(always->getBlock());
    success |= xlate.success();
    if (xlate.getRecomputeUD()) {
      mUD->structuredProc(always);
      xlate.putRecomputeUD(false);
    }
  }

  if (success) {
    mStatistics->module();
  }
  return success;
}


NUStmt * TernaryBlock::translate(NUStmt * stmt) 
{
  // handle the stmt types we want to recurse into; for others, return NULL.

  NUBlock *oneBlock = dynamic_cast<NUBlock*>(stmt);
  if (oneBlock) {
    return translate(oneBlock);
  }

  NUIf *oneIf = dynamic_cast<NUIf*>(stmt);
  if (oneIf) {
    return translate(oneIf);
  }

  NUCase *oneCase = dynamic_cast<NUCase*>(stmt);
  if (oneCase) {
    return translate(oneCase);
  }

  NUFor *oneFor = dynamic_cast<NUFor*>(stmt);
  if (oneFor) {
    return translate(oneFor);
  }

  return NULL;
}

NUStmt * TernaryBlock::translate(NUBlock * block) {
  NUStmtList stmts;
  bool success = translate(block->loopStmts(), stmts);
  if (success) {
    block->replaceStmtList(stmts);
  }
  return NULL;
}

NUStmt * TernaryBlock::translate(NUIf * stmt)
{
  // 1: walk the if stmt and perform the replacement search.
  TernaryTranslateCallback xl8tr(mNetRefFactory,
                                 mOnlyBitConditions,
                                 mOnlyBalancedConditions,
                                 mOnlyBitDefinitions);
  NUDesignWalker walker(xl8tr,false);
  NUDesignCallback::Status status = walker.ifStmt(stmt);

  if (status==NUDesignCallback::eNormal) {
    NUStmt * replacement = xl8tr.getAssign(stmt->getLoc());
    NU_ASSERT(replacement, stmt);
    mStatistics->ternary();
    if (mFoldResults) {
      mFold->clearChanged();
      replacement = mFold->stmt(replacement);
      putRecomputeUD(mFold->isChanged());
    }
    return replacement;
  }

  // 2: if that failed, try looking for matches at lower levels.
  NUStmtList stmts;
  bool result;
  result = translate(stmt->loopThen(),
		     stmts);
  if (result) {
    stmt->replaceThen(stmts);
  }
  stmts.clear();

  result = translate(stmt->loopElse(),
		     stmts);
  if (result) {
    stmt->replaceElse(stmts);
  }
  stmts.clear();
  return NULL;
}


NUStmt * TernaryBlock::translate(NUCase * stmt)
{
  // 1: walk the if stmt and perform the replacement search.
  TernaryTranslateCallback xl8tr(mNetRefFactory,
                                 mOnlyBitConditions,
                                 mOnlyBalancedConditions,
                                 mOnlyBitDefinitions);
  NUDesignWalker walker(xl8tr,false);
  NUDesignCallback::Status status = walker.caseStmt(stmt);

  if (status==NUDesignCallback::eNormal) {
    NUStmt * replacement = xl8tr.getAssign(stmt->getLoc());
    NU_ASSERT(replacement, stmt);
    mStatistics->ternary();
    if (mFoldResults) {
      mFold->clearChanged();
      replacement = mFold->stmt(replacement);
      putRecomputeUD(mFold->isChanged());
    }
    return replacement;
  }

  // 2: if that failed, try looking for matches at lower levels.
  for (NUCase::ItemLoop loop = stmt->loopItems(); 
       not loop.atEnd() ;
       ++loop) {
    NUCaseItem *item = (*loop);

    NUStmtList stmts;
    bool result = translate(item->loopStmts(),
			    stmts);
    if (result) {
      item->replaceStmts(stmts);
    }
  }
  return NULL;
}


NUStmt * TernaryBlock::translate(NUFor * stmt) 
{
  NUStmtList stmts;
  bool result = translate(stmt->loopBody(),
			  stmts);
  if (result) {
    stmt->replaceBody(stmts);
  }
  return NULL;
}


bool TernaryBlock::translate(NUStmtLoop loop,
			     NUStmtList & stmts)
{
  bool success = false;
  for (; not loop.atEnd();
       ++loop) {
    NUStmt * stmt = (*loop);
    NUStmt * repl = translate(stmt);
    if (repl) {
      stmts.push_back(repl);
      delete stmt;
      success = true;
    } else {
      stmts.push_back(stmt);
    }
  }
  mSuccess |= success;
  return success;
}


NUDesignCallback::Status TernaryTranslateCallback::operator()(Phase phase, NUBlockingAssign *assign) 
{
  if (mAssignmentType==eNonBlocking) {
    // inconsistent assignment type.
    return eStop;
  } else {
    mAssignmentType = eBlocking;
    return operator()(phase,(NUAssign*)assign);
  }      
}


NUDesignCallback::Status TernaryTranslateCallback::operator()(Phase phase, NUNonBlockingAssign *assign) 
{
  if (mAssignmentType==eBlocking) {
    // inconsistent assignment type.
    return eStop;
  } else {
    mAssignmentType = eNonBlocking;
    return operator()(phase,(NUAssign*)assign);
  }      
}


NUDesignCallback::Status TernaryTranslateCallback::operator()(Phase phase, NUAssign *assign) 
{
  if (phase==ePre) {

    bool valid = false;

    // Check for a valid def size.
    if (mDefNetRef->empty()) {
      // find the defnetref and check size constraints
      valid = setDefNetRef(assign);
    } else {
      // check for consitency
      valid = checkDefNetRef(assign);
    }

    if (not valid) {
      return eStop;
    }

    CopyContext copy_context(NULL,NULL);
    NULvalue * lvalue = assign->getLvalue();
    if (mLvalue == NULL) {
      mLvalue = lvalue->copy(copy_context);
    } else {
      // compare the current LHS with the first encountered LHS.
      if (not ((*lvalue) == (*mLvalue))) {
        return eStop;
      }
    }

    NUExpr * rvalue = assign->getRvalue();
    mExprs.push_back(rvalue->copy(copy_context));
  }
  return eNormal;
}


NUDesignCallback::Status TernaryTranslateCallback::operator()(Phase phase, NUIf *if_stmt) 
{
  if (phase==ePre) {
    if (not validIf(if_stmt)) {
      return eStop;
    }
  } else {
    NU_ASSERT(phase==ePost, if_stmt);
    NU_ASSERT(mLvalue, if_stmt);

    NUExpr * then_expr;
    NUExpr * else_expr;
    if (if_stmt->loopThen().atEnd()) {
      // then branch contains no stmts.

      if (mAssignmentType==eNonBlocking) {
        // don't create hold conditions when we've got non-blocking assigns.
        return eStop;
      }

      NU_ASSERT(not mExprs.empty(), if_stmt);
      else_expr = mExprs.back();
      NU_ASSERT(else_expr, if_stmt);
      mExprs.pop_back();
      then_expr = createRvalue(mLvalue,else_expr->getLoc());
    } else if (if_stmt->loopElse().atEnd()) {
      // else branch contains no stmts.

      if (mAssignmentType==eNonBlocking) {
        // don't create hold conditions when we've got non-blocking assigns.
        return eStop;
      }

      NU_ASSERT(not mExprs.empty(), if_stmt);
      then_expr = mExprs.back();
      NU_ASSERT(then_expr, if_stmt);
      mExprs.pop_back();
      else_expr = createRvalue(mLvalue,then_expr->getLoc());
    } else {
      NU_ASSERT(mExprs.size() >= 2, if_stmt);
      else_expr = mExprs.back();
      NU_ASSERT(else_expr, if_stmt);
      mExprs.pop_back();
      then_expr = mExprs.back();
      NU_ASSERT(then_expr, if_stmt);
      mExprs.pop_back();
    }

    CopyContext copy_context(NULL,NULL);
    NUExpr * old_condition = if_stmt->getCond();
    NUExpr * condition = old_condition->copy(copy_context);
    NUExpr * ternary = new NUTernaryOp(NUOp::eTeCond,
				       condition,
				       then_expr,
				       else_expr,
				       if_stmt->getLoc());

    mExprs.push_back(ternary);
  }
  return eNormal;
}


NUDesignCallback::Status TernaryTranslateCallback::operator()(Phase phase, NUCase *case_stmt) 
{
  if (phase==ePre) {
    if (not validCase(case_stmt)) {
      return eStop;
    }
  } else {
    NU_ASSERT(mExprs.size() >= 2, case_stmt);

    NUExpr * second_expr = mExprs.back();
    NU_ASSERT(second_expr, case_stmt);
    mExprs.pop_back();
    NUExpr * first_expr = mExprs.back();
    NU_ASSERT(first_expr, case_stmt);
    mExprs.pop_back();

    CopyContext copy_context(NULL,NULL);

    NUExpr * old_condition = case_stmt->getSelect();

    NUCaseItem * item = (*case_stmt->loopItems());

    NUExpr * condition = item->buildConditional(old_condition);
    NUExpr * ternary = new NUTernaryOp(NUOp::eTeCond,
				       condition,
				       first_expr,
				       second_expr,
				       case_stmt->getLoc());
    
    mExprs.push_back(ternary);
  }
  return eNormal;
}

NUAssign * TernaryTranslateCallback::getAssign (const SourceLocator & loc) 
{
  if (not consistent()) {
    return NULL; // callers must check for NULL and assert.
  }
  NULvalue * lvalue = popLvalue();
  NUExpr * rvalue = popRvalue();
  NUAssign * assign;
  if (mAssignmentType==eBlocking) {
    assign = new NUBlockingAssign(lvalue, rvalue, false, loc);
  } else {
    assign = new NUNonBlockingAssign(lvalue, rvalue, false, loc);
  }
  return assign;
}


bool TernaryTranslateCallback::setDefNetRef(NUAssign* stmt) 
{
  NUNetRefSet defs(mNetRefFactory);
  stmt->getBlockingDefs(&defs);
  stmt->getNonBlockingDefs(&defs);

  bool valid = (defs.size()==1);

  if (valid) {
    NUNetRefHdl def = (*(defs.begin()));

    NUNet * net = def->getNet();

    if (mOnlyBitDefinitions) {
      // make sure its a vector.
      valid = (net->isVectorNet());

      if (valid) {
        // this is a one-bit netref which does not cover the entire net
        // (ie. not a NUBitNetRef).
        valid = ((def->getNumBits()==1) and
                 (not def->all()));
      }
    }

    if (valid) {
      mDefNetRef = def;
    }
  }

  return valid;
}


bool TernaryTranslateCallback::checkDefNetRef(NUStmt* stmt) const 
{
  NUNetRefSet defs(mNetRefFactory);
  stmt->getBlockingDefs(&defs);
  stmt->getNonBlockingDefs(&defs);

  bool valid = (defs.size()==1);
    
  if (valid) {
    valid = ((*mDefNetRef) == (**(defs.begin())));
  }
  return valid;
}


bool TernaryTranslateCallback::validIf(NUIf * if_stmt) 
{
  bool valid = true;

  // Check for a valid conditional
  if (mOnlyBitConditions and not mCheckedIfCondition) {
    mCheckedIfCondition = true; // only check this for the first stmt.
    NUExpr * cond = if_stmt->getCond();
    NUNetRefSet uses(mNetRefFactory);
    cond->getUses(&uses);
    valid = false; // invalid unless we find a bitsel.
    for (NUNetRefSet::iterator iter = uses.begin();
	 (not valid) and iter != uses.end();
	 ++iter) {
      NUNetRefHdl use = (*iter);
      // this is a one-bit netref which does not cover the entire
      // net (ie. not a NUBitNetRef).
      valid = ((use->getNumBits()==1) and
	       (not use->all()));
    }
  }

  // Check that branch size == 1.
  if (valid) {
    // allow either 0 or 1 stmt in the then clause.
    NUStmtLoop loop = if_stmt->loopThen();
    if (mOnlyBalancedConditions) {
      valid = (not loop.atEnd());
    }

    if (valid) {
      if (not loop.atEnd()) {
	++loop;
	valid = loop.atEnd();
      }
    }
  }
  if (valid) {
    // allow either 0 or 1 stmt in the else clause.
    NUStmtLoop loop = if_stmt->loopElse();
    if (mOnlyBalancedConditions) {
      valid = (not loop.atEnd());
    }

    if (valid) {
      if (not loop.atEnd()) {
	++loop;
	valid = loop.atEnd();
      }
    }
  }

  return valid;
}


bool TernaryTranslateCallback::validCase(NUCase * case_stmt) 
{
  bool valid = true;

  valid = (case_stmt->isFullySpecified() or case_stmt->hasDefault());

  // Check for a valid conditional
  if (valid and mOnlyBitConditions and not mCheckedIfCondition) {
    mCheckedIfCondition = true; // only check this for the first stmt.
    NUExpr * cond = case_stmt->getSelect();
    NUNetRefSet uses(mNetRefFactory);
    cond->getUses(&uses);
    valid = false; // invalid unless we find a bitsel.
    for (NUNetRefSet::iterator iter = uses.begin();
	 (not valid) and iter != uses.end();
	 ++iter) {
      NUNetRefHdl use = (*iter);
      // this is a one-bit netref which does not cover the entire
      // net (ie. not a NUBitNetRef).
      valid = ((use->getNumBits()==1) and
	       (not use->all()));
    }
  }

  // Check that there are two branches.
  // Check that each branch size == 1.
  // Check that each branch has only one condition.
  // Check that each condition is constant.
  SInt32 branch_count = 0;
  for (NUCase::ItemLoop loop = case_stmt->loopItems();
       valid and not loop.atEnd();
       ++loop) {
    // check that we see only two branches.
    valid = (++branch_count <= 2);
    if (valid) {
      NUCaseItem * item = (*loop);

      // allow 1 stmt in each item.
      NUStmtLoop stmt_loop = item->loopStmts();
      valid = (not stmt_loop.atEnd());

      if (valid) {
	++stmt_loop;
	valid = stmt_loop.atEnd();
      }

      // check for only one constant condition.
      SInt32 condition_count = 0;
      for (NUCaseItem::ConditionLoop cond_loop = item->loopConditions();
	   valid and not cond_loop.atEnd();
	   ++cond_loop) {
        valid = (++condition_count <= 1);
        if (valid) {
          NUCaseCondition * condition = (*cond_loop);
          NUExpr * expr = condition->getExpr();
          NUExpr * mask = condition->getMask();
          valid = (expr->castConstNoXZ () and
                   not mask);
        }
      }
    }
  }
  // Do the check to ensure there are exactly two branches.
  valid = valid && (branch_count == 2);

  return valid;
}


NUExpr * TernaryTranslateCallback::createRvalue(NULvalue * lvalue, const SourceLocator &) const 
{
  // Ignores the locator passed in?
  NUExpr * bit_rvalue = lvalue->NURvalue ();
  return bit_rvalue;
}


void TernaryTranslateCallback::cleanup() 
{
  mDefNetRef = mNetRefFactory->createEmptyNetRef();
  for (NUExprVector::iterator iter = mExprs.begin();
       iter != mExprs.end();
       ++iter) {
    NUExpr * expr = (*iter);
    delete expr;
  }
  mExprs.clear(); // clear so we are multi-call safe.
    
  if (mLvalue) {
    delete mLvalue;
    mLvalue = NULL;
  }
    
  mCheckedIfCondition = false;
  mAssignmentType = eUnknown;
}


void TernaryStatistics::print() const
{
  UtIO::cout() << "TernaryGen Summary: "
	       << mTernaryStmts << " if-then-else stmts converted into ternary stmts in " << mModules << " modules." << UtIO::endl;
}
