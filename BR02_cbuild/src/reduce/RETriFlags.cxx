// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/RETriFlags.h"
#include "reduce/ReachableAliases.h"

#include "nucleus/NUNetClosure.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNet.h"

#include "symtab/STSymbolTable.h"

#include "util/Stats.h"

RETriFlags::RETriFlags(STSymbolTable * symbol_table,
                       ReachableAliases * reachables) :
  mSymbolTable(symbol_table),
  mReachableAliases(reachables)
{
}


RETriFlags::~RETriFlags()
{
}


void RETriFlags::getAliasNets(NUNetElab *master, NUNetSet *aliases)
{
  STAliasedLeafNode *node = master->getSymNode();
  for (STAliasedLeafNode::AliasLoop loop(node);
       not loop.atEnd();
       ++loop) {
    STAliasedLeafNode *alias = *loop;
    if (mReachableAliases->query(alias)) {
      NUNet *net = NUNet::find(alias);
      aliases->insert(net);
    }
  }
}


void RETriFlags::collectUnelaboratedAliases(NUNetClosure* alias_sets)
{
  for (STSymbolTable::NodeLoop i = mSymbolTable->getNodeLoop();
       not i.atEnd(); ++i) {
    STSymbolTableNode * node = (*i);
    STAliasedLeafNode * leaf = node->castLeaf();
    if (leaf != NULL) {
      STAliasedLeafNode * master_leaf = leaf->getMaster();
      if (master_leaf == leaf) {
        NUNetElab* master = NUNetElab::find(master_leaf);
        ST_ASSERT(master, master_leaf);

        NUNetSet aliases;

        // Only use reachable aliases.  This prevents unnecessary flag
        // propagation.
        getAliasNets(master, &aliases);

        alias_sets->addSet(&aliases);
      }
    }
  }
}


void RETriFlags::propagate(Stats *stats, bool phase_stats)
{
  if (phase_stats) {
    stats->pushIntervalTimer();
  }

  NUNetClosure alias_sets;
  collectUnelaboratedAliases(&alias_sets);

  if (phase_stats) {
    stats->printIntervalStatistics("Collect");
  }

  // Propagate the flags. Currently, we only propagate primary-z flag.
  for (NUNetClosure::SetLoop set_iter = alias_sets.loopSets();
       !set_iter.atEnd();
       ++set_iter) {
    NUNetSet *aliases = *set_iter;
    bool is_primary_z = false;
    for (NUNetSetIter alias_iter = aliases->begin();
	 alias_iter != aliases->end();
	 ++alias_iter) {
      NUNet* net = *alias_iter;
      is_primary_z |= net->isPrimaryZ();
      // Optimization; stop after we find it.
      if (is_primary_z) {
	break;
      }
    }
    if (is_primary_z) {
      for (NUNetSetIter alias_iter = aliases->begin();
           alias_iter != aliases->end();
           ++alias_iter)
      {
        NUNet* net = *alias_iter;

        if (net->isWritten() || net->isTriWritten())
        {
          // codegen can connect Tristate<T> nets into
          // submodules with T ports, as long as they
          // will not be written inside the submodule
          net->putIsPrimaryZ(true);
        }
      }
    }
  }

  if (phase_stats) {
    stats->printIntervalStatistics("Propagate");
    stats->popIntervalTimer();
  }
}


