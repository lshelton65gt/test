// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/LoopUnroll.h"
#include "reduce/RewriteUtil.h"

#include "reduce/Ternary.h"
#include "reduce/Inference.h"
#include "reduce/Fold.h"

#include "localflow/UD.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUDesignWalkerCallbacks.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"

#include "nucleus/NUCost.h"
#include "nucleus/NUNetSet.h"

#include "util/ArgProc.h"
#include "util/SetOps.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

//! The localFlow/VhPopulateStmt.cxx uses the same value for VHDL
//! loop unroll. If this changes for some reason, check if VHDL
//! loop unroll maximum iterations should change too!.
#define MAX_UNROLL_ITERATIONS   1024

// This size is used when computing the unrolling size of a for loop
// and all its nested for loops. The value 16 * a single for loop
// unroll size is just a guess at a good number. More work is needed
// to determine if there is a better limit for run-time speed. This
// limit was picked to make the test case
// test/langcov/Memories/largeIndex_3.v compile faster.
#define MAX_UNROLL_SIZE         (MAX_UNROLL_ITERATIONS * 16)

LoopUnroll::LoopUnroll(AtomicCache * str_cache,
                       NUNetRefFactory * netref_factory,
                       MsgContext * msg_context,
                       IODBNucleus * iodb,
                       ArgProc * args,
                       bool force,
                       bool verbose,
                       LoopUnrollStatistics *stats,
                       InferenceStatistics *inference_stats,
                       TernaryStatistics *ternary_stats) :
  mStatistics(stats),
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mArgs(args),
  mForceUnroll(force),
  mVerbose(verbose),
  mUnrolled(false),
  mOptimized(false),
  mAllowDistribution(true),
  mAllowExtraction(true),
  mAllowReplication(true),
  mForceUDUpdate(false),
  mLoopDepth(0)
{
  mUD = new UD(mNetRefFactory, mMsgContext, iodb, args, false);
  mFold = new Fold(mNetRefFactory, args, mMsgContext, mStrCache, iodb,
                   mArgs->getBoolValue(CRYPT("-verboseFold")),
                   eFoldUDValid | eFoldUDKiller | eFoldKeepExprs);

  Inference::Params params (mArgs, mMsgContext, Inference::eRightConcatCollapse);
  mInference = new Inference(mFold, mNetRefFactory, mMsgContext, mStrCache, iodb,
                             mArgs, params, inference_stats);
  mTernary = new Ternary(mStrCache, mNetRefFactory, 
                         mMsgContext, iodb, mArgs, 
                         false, // allow non-bit conditions.
                         false, // allow unbalanced conditionals.
                         true,  // only allow bit definitions.
                         false, // do not fold results.
                         mArgs->getBoolValue(CRYPT("-verboseTernaryCreation")), 
                         ternary_stats, 
                         inference_stats);
  mCosts = new NUCostContext;
  mScopes = new UtVector<NUBlockScope*>;
  mReplicator  = new LoopReplicator(mStrCache, mNetRefFactory, mUD, mVerbose);
  mDistributor = new LoopDistributor(mStrCache, mNetRefFactory, mUD, mVerbose);
  mExtractor   = new InvariantExtractor(mStrCache, mNetRefFactory, mUD, mScopes, mVerbose);
}

LoopUnroll::~LoopUnroll()
{
  delete mExtractor;
  delete mDistributor;
  delete mReplicator;
  delete mScopes;
  delete mCosts;
  delete mTernary;
  delete mInference;
  delete mFold;
  delete mUD;
}


bool LoopUnroll::module(NUModule * one_module)
{
  mCosts->calcModule(one_module, 0, false);

  // Make task enables as expensive as the tasks they call. We may
  // later need to inline these tasks, which can lead to enormous
  // code-bloat if loops containing these enables have been unrolled.
  mCosts->putTaskEnablesHaveTaskCost(true);

  // initialize per-module state variables.
  mUnrolled  = false;
  mOptimized = false;
  mForceUDUpdate = false;

  for (NUTaskLoop p = one_module->loopTasksLevelized(); !p.atEnd(); ++p) {
    NUTask * task = (*p);
    if (qualify(task)) {
      optimize(task, mDeleteStmts);
      unroll(task, mDeleteStmts);

      // recalculate the cost of the task after unrolling its loops.
      mCosts->recalcTF(task);
    }
    NU_ASSERT(mScopes->empty(),task);
  }

  for (NUModule::AlwaysBlockLoop loop = one_module->loopAlwaysBlocks(); not loop.atEnd(); ++loop) {
    NUAlwaysBlock * always = (*loop);

    if (qualify(always->getBlock())) {
      optimize(always->getBlock(), mDeleteStmts);
      unroll(always->getBlock(), mDeleteStmts);
    }
    NU_ASSERT(mScopes->empty(),always);
  }

  for (NUModule::InitialBlockLoop loop = one_module->loopInitialBlocks(); not loop.atEnd(); ++loop) {
    NUInitialBlock *initial = *loop;
    if (qualify(initial->getBlock())) {
      optimize(initial->getBlock(), mDeleteStmts);
      unroll(initial->getBlock(), mDeleteStmts);
    }
    NU_ASSERT(mScopes->empty(),initial);
  }

  if (mUnrolled) {
    mStatistics->module();
  }

  if (mUnrolled or mForceUDUpdate or mOptimized) {
    // Recompute UD either because we unrolled a loop, optimized a
    // loop, or because Fold rewrote something under a loop.

    // Optimization recomputes UD for the parts of the tree that it
    // modifies, but if defs are eliminated, UD needs to be corrected
    // above the modified part. See test/loops/empty.v
    mUD->module(one_module);
  }

  if (mUnrolled or mOptimized) {
    deleteStmtList(mDeleteStmts);
  } else {
    NU_ASSERT(mDeleteStmts.empty(),one_module);
  }

  return mUnrolled;
}

bool LoopUnroll::always(NUAlwaysBlock* one_always)
{
  // initialize per-module state variables.
  mUnrolled  = false;
  mOptimized = false;
  mForceUDUpdate = false;

  if (qualify(one_always->getBlock())) {
    optimize(one_always->getBlock(), mDeleteStmts);
    unroll(one_always->getBlock(), mDeleteStmts);
  }
  NU_ASSERT(mScopes->empty(),one_always);

  if (mUnrolled) {
    mStatistics->module();
  }

  if (mUnrolled or mForceUDUpdate or mOptimized) {
    // Recompute UD either because we unrolled a loop, optimized a
    // loop, or because Fold rewrote something under a loop.

    // Optimization recomputes UD for the parts of the tree that it
    // modifies, but if defs are eliminated, UD needs to be corrected
    // above the modified part. See test/loops/empty.v
    mUD->structuredProc(one_always);
  }

  if (mUnrolled or mOptimized) {
    deleteStmtList(mDeleteStmts);
  } else {
    NU_ASSERT(mDeleteStmts.empty(), one_always);
  }

  return mUnrolled;
}

//! Discover for() loops where the body re-defines the index variable.
class BodyDefinesIndexCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  BodyDefinesIndexCallback() {}
  ~BodyDefinesIndexCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eNormal; }
  Status operator()(Phase /*phase*/, NUFor * node) {
    NUStmtList * initial_statements = node->getInitial(false);

    // Possible for there to be no initial statement, due to while or repeat.
    if (initial_statements->empty()) {
      delete initial_statements;
      return eStop;
    }

    NUStmt * initial_stmt = initial_statements->back();  // get last statement in list
    delete initial_statements;
    NUBlockingAssign * initial  = dynamic_cast<NUBlockingAssign*>(initial_stmt);
    if (not initial) {
      return eStop;
    }
    NUIdentLvalue * lvalue = dynamic_cast<NUIdentLvalue*>(initial->getLvalue());
    if (not lvalue) {
      return eStop;
    }
    
    NUNet * index_net = lvalue->getIdent();

    for (NUStmtLoop loop = node->loopBody();
	 not loop.atEnd();
	 ++loop) {
      NUStmt * stmt = (*loop);
      NUNetSet defs;
      stmt->getBlockingDefs(&defs);
      stmt->getNonBlockingDefs(&defs);
      if (defs.find(index_net) != defs.end()) {
	return eStop;
      }
    }
    return eNormal;
  }
};


bool LoopUnroll::qualify(NUUseDefNode * scope)
{
  BodyDefinesIndexCallback locator;
  NUDesignWalker walker(locator,false);

  NUDesignCallback::Status status;
  NUBlock * block = dynamic_cast<NUBlock*>(scope);
  if (block) {
    status = walker.blockStmt(block);
  } else {
    NUTF * tf = dynamic_cast<NUTF*>(scope);
    NU_ASSERT(tf,scope);
    status = walker.tf(tf);
  }
  return (status==NUDesignCallback::eNormal);
}


bool LoopUnroll::optimize(NUTF * tf, NUStmtList & delete_stmts)
{
  NUStmtList replacements;

  mScopes->push_back(tf);

  bool optimized = optimize(tf->loopStmts(),
			    replacements,
			    delete_stmts);
  if (optimized) {
    tf->replaceStmtList(replacements);
  }

  mScopes->pop_back();

  return false;
}


bool LoopUnroll::unroll(NUTF * tf, NUStmtList & delete_stmts)
{
  NUStmtList replacements;

  mScopes->push_back(tf);

  bool unrolled = unroll(tf->loopStmts(),
			 replacements,
			 delete_stmts);
  if (unrolled) {
    tf->replaceStmtList(replacements);
  }

  mScopes->pop_back();

  return false;
}


bool LoopUnroll::unroll(NUStmt * stmt, 
                        NUStmtList & replacements,
                        NUStmtList & delete_stmts) 
{
  // handle the stmt types we want to recurse into; for others, return false.
  NUBlock *oneBlock = dynamic_cast<NUBlock*>(stmt);
  if (oneBlock) {
    bool status = unroll(oneBlock,delete_stmts);
    NU_ASSERT(status==false,stmt);
    return status;
  }

  NUIf *oneIf = dynamic_cast<NUIf*>(stmt);
  if (oneIf) {
    bool status = unroll(oneIf,delete_stmts);
    NU_ASSERT(status==false,stmt);
    return status;
  }

  NUCase *oneCase = dynamic_cast<NUCase*>(stmt);
  if (oneCase) {
    bool status = unroll(oneCase,delete_stmts);
    NU_ASSERT(status==false,stmt);
    return status;
  }

  NUFor *oneFor = dynamic_cast<NUFor*>(stmt);
  if (oneFor) {
    // for loop is the only one which can return true.
    return doubleUnroll(oneFor, replacements,delete_stmts);
  }

  // other stmt type. return false.
  return false;
}


bool LoopUnroll::optimize(NUStmt * stmt, 
				NUStmtList & delete_stmts) 
{
  // handle the stmt types we want to recurse into; for others, return false.
  NUBlock *oneBlock = dynamic_cast<NUBlock*>(stmt);
  if (oneBlock) {
    bool status = optimize(oneBlock,delete_stmts);
    NU_ASSERT(status==false,stmt);
    return status;
  }

  NUIf *oneIf = dynamic_cast<NUIf*>(stmt);
  if (oneIf) {
    bool status = optimize(oneIf,delete_stmts);
    NU_ASSERT(status==false,stmt);
    return status;
  }

  NUCase *oneCase = dynamic_cast<NUCase*>(stmt);
  if (oneCase) {
    bool status = optimize(oneCase,delete_stmts);
    NU_ASSERT(status==false,stmt);
    return status;
  }

  NUFor *oneFor = dynamic_cast<NUFor*>(stmt);
  if (oneFor) {
    bool status = optimize(oneFor, delete_stmts);
    NU_ASSERT(status==false,stmt);
    return status;
  }

  // other stmt type. return false.
  return false;
}


bool LoopUnroll::unroll(NUBlock * block, NUStmtList & delete_stmts)
{
  NUStmtList replacements;

  mScopes->push_back(block);

  bool unrolled = unroll(block->loopStmts(),
			 replacements,
			 delete_stmts);
  if (unrolled) {
    block->replaceStmtList(replacements);
  }

  mScopes->pop_back();

  return false;
}


bool LoopUnroll::optimize(NUBlock * block, NUStmtList & delete_stmts)
{
  NUStmtList replacements;

  mScopes->push_back(block);

  bool optimized = optimize(block->loopStmts(),
			    replacements,
			    delete_stmts);
  if (optimized) {
    block->replaceStmtList(replacements);
  }

  mScopes->pop_back();

  return false;
}


bool LoopUnroll::unroll(NUIf * stmt,
			      NUStmtList & delete_stmts)
{
  NUStmtList replacements;
  bool unrolled;

  unrolled = unroll(stmt->loopThen(),
		    replacements,
		    delete_stmts);
  if (unrolled) {
    stmt->replaceThen(replacements);
  }
  replacements.clear();

  unrolled = unroll(stmt->loopElse(),
		    replacements,
		    delete_stmts);
  if (unrolled) {
    stmt->replaceElse(replacements);
  }
  replacements.clear();

  return false;
}


bool LoopUnroll::optimize(NUIf * stmt,
				NUStmtList & delete_stmts)
{
  NUStmtList replacements;
  bool optimized;

  optimized = optimize(stmt->loopThen(),
		       replacements,
		       delete_stmts);
  if (optimized) {
    stmt->replaceThen(replacements);
  }
  replacements.clear();

  optimized = optimize(stmt->loopElse(),
		       replacements,
		       delete_stmts);
  if (optimized) {
    stmt->replaceElse(replacements);
  }
  replacements.clear();

  return false;
}


bool LoopUnroll::unroll(NUCase * stmt,
			      NUStmtList & delete_stmts)
{
  for (NUCase::ItemLoop loop = stmt->loopItems(); 
       not loop.atEnd() ;
       ++loop) {
    NUCaseItem *item = (*loop);

    NUStmtList replacements;
    bool unrolled = unroll(item->loopStmts(),
			   replacements,
			   delete_stmts);
    if (unrolled) {
      item->replaceStmts(replacements);
    }
  }
  return false;
}


bool LoopUnroll::optimize(NUCase * stmt,
                          NUStmtList & delete_stmts)
{
  for (NUCase::ItemLoop loop = stmt->loopItems(); 
       not loop.atEnd() ;
       ++loop) {
    NUCaseItem *item = (*loop);

    NUStmtList replacements;
    bool optimized = optimize(item->loopStmts(),
			      replacements,
			      delete_stmts);
    if (optimized) {
      item->replaceStmts(replacements);
    }
  }
  return false;
}


bool LoopUnroll::optimize(NUFor * stmt,
                          NUStmtList & delete_stmts)
{
  NUStmtList replacements;
  bool optimized;

  optimized = optimize(stmt->loopBody(),
		       replacements,
		       delete_stmts);
  if (optimized) {
    stmt->replaceBody(replacements);
  }
  replacements.clear();

  return false;
}


bool LoopUnroll::doubleUnroll(NUFor * stmt, 
                              NUStmtList & replacements,
                              NUStmtList & delete_stmts)
{
  NU_ASSERT(mLoopDepth >= 0, stmt);
  ++mLoopDepth;

  // First, try and recurse and process any sub-elements of the for().
  // We may not unroll this parent loop, but we still want to try
  NUStmtList body_replacements;
  bool replace = unroll(stmt->loopBody(), body_replacements, delete_stmts);
  if (replace) {
    stmt->replaceBody(body_replacements);
  }
  // We have updated the body of the loop, meaning the replacements
  // list does not need populating.
  //
  // We need to return a 'true' status only if the following unroll
  // call requires it.

  // Now, try unrolling the loop itself.
  bool unrolled = unroll(stmt, replacements, delete_stmts);

  --mLoopDepth;
  NU_ASSERT(mLoopDepth >= 0, stmt);
  if (mLoopDepth == 0) {
    mFold->clearExprFactory();
  }

  return unrolled;
}


bool LoopUnroll::unroll(NUFor * stmt, 
                        NUStmtList & replacements,
                        NUStmtList & delete_stmts)
{
  // Try and reduce the size of the for() loop before unrolling.
  mFold->pushScope(mScopes->back()); 
  mFold->clearChanged();
  NUStmt * folded = mFold->stmt(stmt);
  // remember if folding rewrote anything.
  mForceUDUpdate |= mFold->isChanged();
  NU_ASSERT2(folded==stmt,folded,stmt);
  mFold->popScope();

  // TBD: qualify this for loop as a valid unroll candidate.
  // 1. blocks bad.

  NUStmtList * initial_stmts = stmt->getInitial(false);
  SInt32 initial_size = initial_stmts->size();
  NUStmt * initial_stmt = NULL;
  if ( initial_size  == 1 ){
    NUStmtListIter iter = initial_stmts->begin();    // prefetch first and only stmt
    initial_stmt = (*iter);
  }
  delete initial_stmts;
  if ( initial_size != 1) {
    // either not statements, or more than 1 stmt
    // (RHS must have been a function call)
    // not yet handled
    return false;
  }
  NUBlockingAssign * initial   = dynamic_cast<NUBlockingAssign*>(initial_stmt);
  NU_ASSERT(initial,initial_stmt);

  NUExpr * condition = stmt->getCondition();

  NUStmtList * advance_stmts = stmt->getAdvance(false);
  SInt32 advance_size = advance_stmts->size();
  NUStmt *advance_stmt = NULL;
  if ( advance_size == 1 ){
    NUStmtListIter iter = advance_stmts->begin();    // prefetch first and only stmt
    advance_stmt = (*iter);
  }
  delete advance_stmts;
  if ( advance_size != 1 ){
    // if more than 1 stmt, then RHS must have been a function call,
    // we do not handle this situation yet.  If zero, then must be an
    // infinite loop, and we don't handle that either.
    return false;
  }
  NUBlockingAssign * step = dynamic_cast<NUBlockingAssign*>(advance_stmt);
  NU_ASSERT (step, advance_stmt);
  
  
  NUExpr   * rvalue = initial->getRvalue();
  NUIdentLvalue * lvalue = dynamic_cast<NUIdentLvalue*>(initial->getLvalue());
  NU_ASSERT(lvalue, initial);
  NUNet * index_net = lvalue->getIdent();

  NUExpr * step_rvalue = step->getRvalue();
  NUIdentLvalue * step_lvalue = dynamic_cast<NUIdentLvalue*>(step->getLvalue());
  NU_ASSERT(step_lvalue, step);
  NUNet * step_net = lvalue->getIdent();

  // avoid loops like: for(i=0;i<8;j=j+1)
  if (step_net != index_net) {
    return false;
  }

  // avoid conditions which don't mention the index.
  if (not checkUses(condition,index_net)) {
    return false;
  }

  // avoid step fn which don't use the index.
  if (not checkUses(step_rvalue,index_net)) {
    return false;
  }

  // non-constant initial.
  if (not rvalue->castConstNoXZ ()) {
    return false;
  }

  // Estimate whether the unrolling will help or not. This avoids
  // compile performance problems.
  if (!estimateBeneficial(stmt)) {
    return false;
  }

  CopyContext copy_context(NULL,NULL);
  NUStmtList unrolled;

  UInt32 iterations = 0;

  bool use_signed = index_net->isInteger();
  if (use_signed) {
    updateSignedness(rvalue);
    updateSignedness(condition);
    updateSignedness(step_rvalue);
  }

  bool successful_unroll = true;

  NUExpr * current = rvalue->copy(copy_context);

  {
    UInt32 net_size = index_net->getBitSize();
    current = current->makeSizeExplicit(net_size);

    while (successful_unroll and test(condition, index_net, current)) {
      NUStmtList body;

      // copy the body and replace the index variable with its current value
      copy(stmt->loopBody(), body, index_net, current);

      // append the unrolled stmts to our set of replacements.
      unrolled.insert(unrolled.end(), body.begin(), body.end());

      if ((++iterations) > MAX_UNROLL_ITERATIONS) {
        successful_unroll = false; // avoid unrolling extremely large loops.
      }
      current = advance(step_rvalue, index_net, current);
      current = current->makeSizeExplicit(net_size);
    }
  }

  if (not successful_unroll) {
    if (mVerbose) {
      dumpIterationFailure(stmt->getLoc());
    }
    delete current;
    deleteStmtList(unrolled);
    return false;
  }

  if (mVerbose) {
    UtIO::cout() << "Unrolling loop: ";
    stmt->getLoc().print();
    UtIO::cout() << " required " << iterations << " iterations." << UtIO::endl;
  }

  // add the final value for the index -- in case someone else needs it.
  unrolled.push_back(new NUBlockingAssign(lvalue->copy(copy_context),
					  current->copy(copy_context),
                                          initial->getUsesCFNet(),
					  initial->getLoc()));
  delete current;

  // now recurse and process any sub-elements of this for() [they're now in unrolled]
  NUStmtList my_delete_stmts;
  unroll(NUStmtLoop(unrolled), replacements, my_delete_stmts); // ignore return.

  // Try and generate ternary stmts from if-then-else stmts.
  mTernary->stmts(replacements); // ignore return value.

  // Try and optimize the stmts.
  mInference->stmts(mScopes->back(), replacements); // ignore return value.

  bool keep_unroll = advantageous(stmt, replacements, iterations);

  if (not keep_unroll) {
    deleteStmtList(replacements);
    deleteStmtList(my_delete_stmts);

    return false;
  } else {
    // At this point, we really know that the unrolling is official.
    mStatistics->loop();
    mUnrolled = true;

    delete_stmts.insert(delete_stmts.end(),
			my_delete_stmts.begin(),
			my_delete_stmts.end());
  }

  return true;
}


void LoopUnroll::dumpIterationFailure(const SourceLocator & loc, const char * estimated) 
{
  UtIO::cout() << "Unrolling loop: ";
  loc.print();
  UtIO::cout() << " required more than " << MAX_UNROLL_ITERATIONS;
  if (estimated) {
    UtIO:: cout() << " " << estimated;
  }
  UtIO::cout() << " iterations." << UtIO::endl;
  UtIO::cout() << "    Discarding: High iteration count." << UtIO::endl;
}


void LoopUnroll::dumpEstimatedIterationFailure(const SourceLocator & loc, SInt32 estimated_loop_count) 
{
  UtString estimated;
  estimated << "(estimated: " << estimated_loop_count << ")";
  dumpIterationFailure(loc,estimated.c_str());
}

void
LoopUnroll::dumpEstimatedUnrollSizeFailure(const SourceLocator& loc, SInt32 size)
{
  UtIO::cout() << "Unrolling loop: ";
  loc.print();
  UtIO::cout() << " estimated unroll size more than " << MAX_UNROLL_SIZE;
  UtIO::cout() << " (estimated unroll size: " << size << ").\n";
  UtIO::cout() << "    Discarding: High estimated unroll size.\n";
}


bool LoopUnroll::checkUses(NUExpr * fn, NUNet * net) const
{
  NUNetSet uses;
  fn->getUses(&uses);
  if (uses.size()==1) {
    NUNet * use = (*(uses.begin()));
    return (use==net);
  }
  return false;
}

class SignedUpdateCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif
  
public:
  SignedUpdateCallback() {}
  ~SignedUpdateCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eNormal; }
  Status operator()(Phase /*phase*/, NUExpr * fn) { 
    fn->setSignedResult(true);
    return eNormal;
  }
};

void LoopUnroll::updateSignedness(NUExpr * fn)
{
  return; // NOP for now.

  SignedUpdateCallback updater;
  NUDesignWalker walker(updater,false);
  walker.expr(fn); // ignore return value.
}


bool LoopUnroll::test(NUExpr * original_condition, 
			    NUNet  * net,
			    NUExpr * current)
{
  bool should_loop_continue = true;
  NUExpr * condition = evaluate(original_condition, net, current);
  if (const NUConst * const_condition = condition->castConstNoXZ ())
    should_loop_continue = not const_condition->isZero ();
  else if (condition->castConstXZ ())
    should_loop_continue = false;

  delete condition;
  return should_loop_continue;
}


NUExpr * LoopUnroll::advance(NUExpr * original_target,
				   NUNet * net,
				   NUExpr * current)
{
  NUExpr * target = evaluate(original_target, net, current);
  delete current;
  return target;
}


NUExpr * LoopUnroll::evaluate(NUExpr * original_target,
                              NUNet  * net,
                              NUExpr * current)
{
  CopyContext copy_context(NULL,NULL);
  NUExpr * target = original_target->copy(copy_context);

  NUNetReplacementMap     dummy_net_replacements;
  NUTFReplacementMap      dummy_tf_replacements;
  NUCModelReplacementMap      dummy_cmodel_replacements;
  NUAlwaysBlockReplacementMap      dummy_always_replacements;
  NULvalueReplacementMap  dummy_lvalue_replacements;
  NUExprReplacementMap    rvalue_replacements;
  rvalue_replacements[net] = current;
  RewriteLeaves translator(dummy_net_replacements,
                           dummy_lvalue_replacements,
                           rvalue_replacements,
                           dummy_tf_replacements,
                           dummy_cmodel_replacements,
                           dummy_always_replacements,
                           mFold);
  NUExpr * repl = translator(target, NuToNuFn::ePre);
  if (repl) {
    target = repl;
  }
  target->replaceLeaves(translator);
  repl = translator(target, NuToNuFn::ePost);
  if (repl) {
    target = repl;
  }
  
  repl = mFold->fold (target);
  if (repl) {
    target = repl;
  }

  return target;
}


void LoopUnroll::copy(NUStmtLoop loop,
                      NUStmtList & target,
                      NUNet * net,
                      NUExpr * current)
{
  NUNetReplacementMap     dummy_net_replacements;
  NUTFReplacementMap      dummy_tf_replacements;
  NULvalueReplacementMap  dummy_lvalue_replacements;
  NUExprReplacementMap    rvalue_replacements;
  NUCModelReplacementMap  dummy_cmodel_replacements;
  NUAlwaysBlockReplacementMap      dummy_always_replacements;
  rvalue_replacements[net] = current;
  RewriteLeaves translator(dummy_net_replacements,
                           dummy_lvalue_replacements,
                           rvalue_replacements,
                           dummy_tf_replacements,
                           dummy_cmodel_replacements,
                           dummy_always_replacements,
                           mFold);

  CopyContext copy_context(mScopes->back(),mStrCache,true);

  // make the folder aware of our parent block -- in case it
  // needs to create new blocks.
  mFold->pushScope(mScopes->back()); 
  for (; not loop.atEnd();
       ++loop) {
    NUStmt * orig_stmt = (*loop);
    NUStmt * stmt = orig_stmt->copy(copy_context);

    stmt->replaceLeaves(translator);
    stmt = mFold->stmt(stmt);
    if (stmt)
      target.push_back(stmt);
  }
  mFold->popScope();
}


bool LoopUnroll::unroll(NUStmtLoop loop,
                        NUStmtList & replacements,
                        NUStmtList & delete_stmts)
{
  bool changed = false;
  for (;
       not loop.atEnd();
       ++loop) {
    NUStmt * stmt = (*loop);
    NUStmtList one_replacement;
    bool i_changed = unroll(stmt, one_replacement, delete_stmts);
    if (i_changed) {
      replacements.insert(replacements.end(),
			  one_replacement.begin(),
			  one_replacement.end());
      delete_stmts.push_back(stmt);
      changed = true;
    } else {
      replacements.push_back(stmt);
    }
  }
  return changed;
}


bool LoopUnroll::optimize(NUStmtLoop loop,
                          NUStmtList & replacements,
                          NUStmtList & delete_stmts)
{
  NUStmtList distribution;
  bool changed = distribute(loop,distribution,delete_stmts);

  NUStmtList extraction;
  changed |= extract(NUStmtLoop(distribution),extraction,delete_stmts);
  distribution.clear();

  NUStmtList replication;
  changed |= replicate(NUStmtLoop(extraction),replication);
  extraction.clear();

  bool i_changed;
  do {
    for (loop = NUStmtLoop(replication);
	 not loop.atEnd();
	 ++loop) {
      NUStmt * stmt = (*loop);
      bool status = optimize(stmt, delete_stmts);
      NU_ASSERT(status==false,stmt);
    }

    i_changed = distribute(NUStmtLoop(replication),distribution,delete_stmts);
    replication.clear();

    i_changed |= extract(NUStmtLoop(distribution),extraction,delete_stmts);
    distribution.clear();

    i_changed |= replicate(NUStmtLoop(extraction),replication);
    extraction.clear();

    changed |= i_changed;
  } while (i_changed);

  replacements.insert(replacements.end(),
		      replication.begin(),
		      replication.end());

  mOptimized |= changed;

  return changed;
}


bool LoopUnroll::replicate(NUStmtLoop loop,
                           NUStmtList & replacements)
{
  bool changed = false;
  for (; not loop.atEnd();
       ++loop) {
    NUStmt * stmt = (*loop);

    if (mAllowReplication) {
      NUFor * for_stmt = dynamic_cast<NUFor*>(stmt);
      if (for_stmt) {
        changed |= mReplicator->replicate(for_stmt);
      }
    }

    replacements.push_back(stmt);
  }
  return changed;
}


bool LoopUnroll::distribute(NUStmtLoop loop,
                            NUStmtList & replacements,
                            NUStmtList & delete_stmts)
{
  bool changed = false;
  for (; not loop.atEnd();
       ++loop) {
    NUStmt * stmt = (*loop);
    
    bool i_changed = false;
    NUStmtList distribution;

    if (mAllowDistribution) {
      NUFor * for_stmt = dynamic_cast<NUFor*>(stmt);
      if (for_stmt) {
        i_changed = mDistributor->distribute(for_stmt,distribution);
      }
    }

    if (i_changed) {
      replacements.insert(replacements.end(),
			  distribution.begin(),
			  distribution.end());
      delete_stmts.push_back(stmt);
      changed = true;
    } else {
      replacements.push_back(stmt);
    }
  }
  return changed;
}


bool LoopUnroll::extract(NUStmtLoop loop,
                         NUStmtList & replacements,
                         NUStmtList & delete_stmts)
{
  bool changed = false;
  for (; not loop.atEnd();
       ++loop) {
    NUStmt * stmt = (*loop);
    
    bool i_changed = false;
    NUStmtList extraction;

    if (mAllowExtraction) {
      NUFor * for_stmt = dynamic_cast<NUFor*>(stmt);
      if (for_stmt) {
        i_changed = mExtractor->extract((NUBlockStmt*)for_stmt,extraction);
      }
    }

    if (i_changed) {
      replacements.insert(replacements.end(),
			  extraction.begin(),
			  extraction.end());
      delete_stmts.push_back(stmt);
      changed = true;
    } else {
      replacements.push_back(stmt);
    }
  }
  return changed;
}


static SInt32 controlCost(NUCost * cost) {
  return (cost->mBlocks + 
	  cost->mFors + 
	  cost->mIfs + 
	  cost->mCases);
}

bool LoopUnroll::advantageous(NUFor * stmt,
                              NUStmtList & replacements,
                              int iterations)
{
  // if the cost of the unrolled loop is too high, throw out the unroll.
  NUCost for_cost;
  NUCost unrolled_cost;
  stmt->calcCost(&for_cost,mCosts);
  mCosts->calcStmtList(replacements, &unrolled_cost);

  // the number of high-cost sub-structures.
  SInt32 for_control_count = controlCost(&for_cost);
  SInt32 unrolled_control_count = controlCost(&unrolled_cost);
  SInt32 unrolled_assign_count = unrolled_cost.mBlockingAssigns + unrolled_cost.mNonBlockingAssigns;

  // We want to keep unrollings that reduce the number of branches
  bool reduced_branching = (unrolled_control_count <= (3 * iterations * for_control_count / 4));
  
  // We want to unroll loops if the loop body is too short to fill the pipeline
  bool loop_body_is_short = (for_cost.mInstructions <= 30);

  // We want to unroll if the number of iterations is small
  bool few_iterations = (iterations <= 4);

  // Try to estimate the running time of the two loops based on some guesses.
  // We want to unroll if we think the cost of the unrolled loop will be lower.
  // The cache miss penalty models the cost associated with fetching istream
  // for the unrolled loop instead of re-executing the same istream in the
  // looping case.  The misprediction penalty models the branch misprediciton
  // caused by the fact that branches share the same address but execute differently
  // on each iteration of the loop, causing branch mispredicts -- the unrolled loop
  // gives each executed branch its own address so there are no branch prediction
  // conflicts.
  double cache_miss_penalty = 0.5; // SWAG of amortized penalty per instruction of running not-in-cache
  double mispredict_penalty = 1.0; // SWAG of amortized penalty per branch of misprediction
  // Charge the first iteration of the loop with cache miss penalties, and all iterations
  // with mispredicted branches
  UInt32 for_estimate = (UInt32) (for_cost.mInstructionsRun * (1.0 + (cache_miss_penalty / iterations)) +
                                  (for_control_count * iterations * mispredict_penalty));
  // Charge the entire unrolled istream with cache miss penalties but no mispredicted branch penalties
  UInt32 unrolled_estimate = (UInt32) (unrolled_cost.mInstructionsRun * (1.0 + cache_miss_penalty));

  // Make sure that we are not putting too much pressure on the I-cache
  // The bug 4843 testcase shows that small loops can cause dramatic instruction growth.  It 
  // contains a for loop with a small number of iterations (64), unrolling this loop will increase
  // the instruction cost by almost a factor of 100.

  bool blows_up_icache = false;
  if (iterations > 256) {
    blows_up_icache = ((unrolled_cost.mInstructions > 1500) and
                       (unrolled_cost.mInstructions > (for_cost.mInstructions * 3)));
  } else {
    blows_up_icache = ((unrolled_cost.mInstructions > 5000) and
                       (unrolled_cost.mInstructions > (for_cost.mInstructions * 20)));
  }

  // OC48 is very sensitive to these tests. If the m_cam_enc_mmc.v
  // for() loop is unrolled, cbuild will not be happy (ie. looong
  // compile times). This testcase has been extracted as bug1502.
  // NOTE: this does not appear to be true anymore: bug1502 builds fine with -forceUnroll

  // Don't allow the unrolled number of control stmts to get too large.
  // The UD cost for the resulting block will be high.
  bool bad_for_UD = (unrolled_control_count > 500);

  // Likewise, the testcase for Bug 860 cannot be unrolled. Some
  // versions of GCC are not able to compile the resulting code in a
  // reasonable amount of time.
  // NOTE: this does not appear to be true for gcc3.3.3: bug860 builds fine with -forceUnroll

  // Don't allow the number of assignments to grow too large.
  // GCC may not be able to compile the resulting block.
  bool bad_for_gcc = (unrolled_assign_count > 1000);

  bool keep_results = ((reduced_branching or loop_body_is_short or
                        few_iterations or (unrolled_estimate <= for_estimate))
                       and not (blows_up_icache or bad_for_UD or bad_for_gcc));

  bool wasForced = false;
  if (mForceUnroll && !keep_results)
  {
    keep_results = true;
    wasForced = true;
    mStatistics->forced();
  }

  if (mVerbose) {
    UtIO::cout() << "    "
                 << (keep_results ? 
                     "Keeping" :
                     "Discarding: High costs")
                 << ": [asn: "
                 << unrolled_assign_count 
                 << ", inst: " 
                 << for_cost.mInstructions << " / " 
                 << unrolled_cost.mInstructions
                 << ", run: " 
                 << for_cost.mInstructionsRun << " / " 
                 << unrolled_cost.mInstructionsRun
                 << ", ctrl: "
                 << for_control_count << " / " 
                 << unrolled_control_count 
                 << "]";
    if (wasForced)
      UtIO::cout() << " (forced)";
    UtIO::cout() << UtIO::endl;
  }
  return keep_results;
}



bool LoopReplicator::replicate(NUFor * for_stmt)
{
  NUStmtList replacements;
  NUStmtList delete_stmts;

  bool changed = false;
  for (NUStmtLoop loop = for_stmt->loopBody();
       not loop.atEnd();
       ++loop) {
    NUStmt * body_stmt = (*loop);
    NUStmtList my_replacements;
    bool i_changed = replicate(body_stmt,my_replacements);
    if (i_changed) {
      replacements.insert(replacements.end(),
			  my_replacements.begin(),
			  my_replacements.end());
      delete_stmts.push_back(body_stmt);
      changed = true;
    } else {
      replacements.push_back(body_stmt);
    }
  }
  
  if (changed) {
    for_stmt->replaceBody(replacements);
    // If we've replicated statements, we need to recompute UD locally
    // so other loop optimizations will see accurate UD.
    mUD->stmt(for_stmt);
  }
  for (NUStmtList::iterator iter = delete_stmts.begin();
       iter != delete_stmts.end();
       ++iter) {
    delete (*iter);
  }
  return changed;
}


bool LoopReplicator::replicate(NUStmt * stmt, NUStmtList & my_replacements)
{
  NUNetRefSet def_set(mNetRefFactory);
  stmt->getBlockingDefs(&def_set);
  stmt->getNonBlockingDefs(&def_set);

  if (qualify(stmt,def_set)) {
    if (mVerbose) {
      UtIO::cout() << "Replicated body: ";
      stmt->getLoc().print();
      UtIO::cout() << " " << def_set.size() << " times." << UtIO::endl;
    }
    CopyContext copy_context(NULL,NULL);
    for (NUNetRefSet::iterator iter = def_set.begin();
	 iter != def_set.end();
	 ++iter) {
      NUNetRefHdl def_net_ref = (*iter);
      NUStmt * stmt_copy = stmt->copy(copy_context);
      reduce(stmt_copy,def_net_ref);
      my_replacements.push_back(stmt_copy);
    }
    return true;
  }

  return false;
}

class ReplicationCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  ReplicationCallback(NUNetRefFactory * netref_factory,
		      NUNetRefSet & def_set) :
    mNetRefFactory(netref_factory),
    mDefSet(def_set),
    mPartialDefinition(false)
  {}
  ~ReplicationCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eStop; }
  Status operator()(Phase /*phase*/, NUExpr * /*node*/) { return eSkip; }
  Status operator()(Phase /*phase*/, NULvalue * /*node*/) { return eSkip; }
  Status operator()(Phase /*phase*/, NUAssign * node) {
    NUNetRefSet defs(mNetRefFactory);
    node->getBlockingDefs(&defs);
    node->getNonBlockingDefs(&defs);

    if (defs.size()!=1) {
      return eStop;
    }

    // check that we've got a partial net definition. this is
    // necessary so unrolling does not produce duplicate if()
    // structures which will be optimized by the priority encoder.
    NULvalue * lvalue = node->getLvalue();
    if (lvalue->getType()==eNUVarselLvalue or
	lvalue->getType()==eNUMemselLvalue) {
      mPartialDefinition=true;
    }

    return qualify(node);
  }
  Status operator()(Phase /*phase*/, NUIf * node) {
    return qualify(node);
  }
  bool hasPartialDefinition() const { return mPartialDefinition; }
private:
  Status qualify(NUStmt * stmt) {
    Status status = NUDesignCallback::eNormal;
    for (NUNetRefSet::iterator iter = mDefSet.begin();
	 status == NUDesignCallback::eNormal and iter != mDefSet.end();
	 ++iter) {
      const NUNetRefHdl & def_net_ref = (*iter);
      NUNetRefSet uses(mNetRefFactory);
      stmt->getBlockingUses(def_net_ref, &uses);
      stmt->getNonBlockingUses(def_net_ref, &uses);

      if (NUNetRefSet::set_has_intersection(mDefSet,uses)) {
	status = NUDesignCallback::eStop;
      }
    }
    return status;
  }
  NUNetRefFactory * mNetRefFactory;
  NUNetRefSet & mDefSet;
  bool mPartialDefinition;
};


bool LoopReplicator::qualify(NUStmt * stmt, NUNetRefSet & def_set)
{
  if (not dynamic_cast<NUIf*>(stmt)) {
    return false;
  }

  NUNetRefSet defs(mNetRefFactory);
  stmt->getBlockingDefs(&defs);
  stmt->getNonBlockingDefs(&defs);

  // if there is only one def, then splitting makes no sense.
  if (defs.size()<=1) {
    return false;
  }

  ReplicationCallback callback(mNetRefFactory, def_set);
  NUDesignWalker walker(callback,false);
  
  NUDesignCallback::Status status = walker.stmt(stmt);

  if (not callback.hasPartialDefinition()) {
    return false;
  }

  return (status==NUDesignCallback::eNormal);
}


class ReductionCallback : public NUDesignCallback
{
#if ! pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  ReductionCallback(NUNetRefFactory * netref_factory,
		    NUNetRefHdl & def_net_ref) :
    mNetRefFactory(netref_factory),
    mDefNetRef(def_net_ref) 
  {}
  ~ReductionCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eStop; }
  Status operator()(Phase /*phase*/, NUExpr * /*node*/) { return eSkip; }
  Status operator()(Phase /*phase*/, NULvalue * /*node*/) { return eSkip; }
  Status operator()(Phase /*phase*/, NUAssign * node) {
    if ((not node->queryBlockingDefs(mDefNetRef, &NUNetRef::overlapsSameNet, mNetRefFactory)) and
	(not node->queryNonBlockingDefs(mDefNetRef, &NUNetRef::overlapsSameNet, mNetRefFactory))) {
      return eDelete;
    } else {
      return eSkip;
    }
  }
  Status operator()(Phase /*phase*/, NUIf * /*node*/) {
    return eNormal;
  }
private:
  NUNetRefFactory * mNetRefFactory;
  NUNetRefHdl & mDefNetRef;
};


void LoopReplicator::reduce(NUStmt * stmt, NUNetRefHdl & def_net_ref)
{
  ReductionCallback callback(mNetRefFactory, def_net_ref);
  NUDesignWalker walker(callback,false);
  
  NUDesignCallback::Status status = walker.stmt(stmt);
  NU_ASSERT(status==NUDesignCallback::eNormal, stmt);
}


bool LoopDistributor::distribute(NUFor * for_stmt, NUStmtList & results) {
  if (qualify(for_stmt)) {
    NUStmtList * body = for_stmt->getBody();
    for (NUStmtList::iterator iter = body->begin();
	 iter != body->end();
	 ++iter) {
      NUStmt * body_stmt = (*iter);

      NUFor * for_distribute = copy(for_stmt, body_stmt);

      // update the UD for each distributed part. People depend on us!
      mUD->stmt(for_distribute);
      results.push_back(for_distribute);
    }

    if (mVerbose) {
      UtIO::cout() << "Distributed loop: ";
      for_stmt->getLoc().print();
      UtIO::cout() << " into " << body->size() << " parts." << UtIO::endl;
    }

    // Give the for() loop an empty body -- when someone deletes the
    // original for() stmt, we don't want to have its body deleted.
    body->clear();
    for_stmt->replaceBody(*body);
    delete body;
    return true;
  } else {
    return false;
  }
}


NUFor * LoopDistributor::copy(NUFor * for_stmt, NUStmt * body_stmt)
{
  NUStmtList body_stmts;
  body_stmts.push_back(body_stmt);
  LoopFactory loop_factory(mStrCache, mNetRefFactory);
  return loop_factory(for_stmt, &body_stmts);
}


bool LoopDistributor::qualify(const NUFor * for_stmt) const 
{
  UInt32 body_stmts_cnt = 0;

  NUNetSet body_defs;
  NUNetSet body_blocking_defs;
  NUNetSet uses;

  for (NUStmtCLoop loop = for_stmt->loopBody();
       not loop.atEnd();
       ++loop, ++body_stmts_cnt) {
    const NUStmt * stmt = (*loop);

    NUNetSet blocking_defs;
    NUNetSet non_blocking_defs;
    stmt->getNonBlockingDefs(&non_blocking_defs);
    stmt->getBlockingDefs(&blocking_defs);

    // If multiple statements define (blocking or nonblocking) the
    // same net, do not allow them to be distributed apart. (Bug 5109)
    if (set_has_intersection(body_defs,non_blocking_defs) or
        set_has_intersection(body_defs,blocking_defs)) {
      return false;
    }

    body_blocking_defs.insert(blocking_defs.begin(),blocking_defs.end());
    body_defs.insert(blocking_defs.begin(),blocking_defs.end());
    body_defs.insert(non_blocking_defs.begin(),non_blocking_defs.end());

    stmt->getBlockingUses(&uses);
    stmt->getNonBlockingUses(&uses);
  }

  // Distribution is valid only with a multi-statement body.
  if (body_stmts_cnt <= 1) {
    return false;
  }

  // Intersection between non-blocking defs and uses cannot
  // invalidate loop distribution, as they don't take effect until
  // the end of the containing block. Non-blocking uses can
  // invalidate loop distribution if they use something that has
  // been defined in a blocking fashion.
  // 
  // Pessimistic qualification: If the loop uses nets it defines,
  // abort. This needs to be refined; there are many safe
  // distribution scenarios, eg:
  // 
  // for (...)
  //   x[i] = a[i];
  //   y[i] = x[i] & b[i];
  if (set_has_intersection(body_blocking_defs,uses)) {
    return false;
  }

  // Now, consider the elements in the loop header.
  uses.clear();

  // If the loop body defines any nets used in the condition, we
  // cannot perform distribution. The body could be trying to
  // perform an early-abort of the loop:
  // 
  // ena = 1;
  // for (i=0; ena & i < 10; i = i + 1) begin
  //   x[i] = y[i];
  //   if (z[i]) ena = 0;
  // end
  for_stmt->getCondition()->getUses(&uses);

  // Further, if the loop initial or advance defs or uses any nets
  // defined by the body, we cannot perform distribution. If we were
  // to distribute, one (or more) of the distributions might not
  // iterate the correct number of times:
  //
  // for (i=0; i<10; i=i+adv) begin
  //   x[i] = y[i];
  //   if (skip[i]) advance = 2;
  //   else         advance = 1;
  // end
  for (NUStmtCLoop loop = for_stmt->loopInitial();
       not loop.atEnd();
       ++loop) {
    const NUStmt * stmt = *loop;
    stmt->getBlockingDefs(&uses);
    stmt->getNonBlockingDefs(&uses);
    stmt->getBlockingUses(&uses);
    stmt->getNonBlockingUses(&uses);
  }
  for (NUStmtCLoop loop = for_stmt->loopAdvance();
       not loop.atEnd();
       ++loop) {
    const NUStmt * stmt = *loop;
    stmt->getBlockingDefs(&uses);
    stmt->getNonBlockingDefs(&uses);
    stmt->getBlockingUses(&uses);
    stmt->getNonBlockingUses(&uses);
  }

  // If the body defines any nets referenced in the loop header,
  // disqualify distribution.
  return not set_has_intersection(body_defs,uses);
}


bool InvariantExtractor::extract(NUBlockStmt * stmt, NUStmtList & results)
{
  NUFor * for_stmt = dynamic_cast<NUFor*>(stmt);
  if (for_stmt) {
    return extract(for_stmt, results);
  }
  return false;
}


bool InvariantExtractor::extract(NUFor * for_stmt, NUStmtList & results)
{
  // First attempt to break up any multi-condition if statements
  // inside the for loop into multiple if statements. If we put the
  // invariant conditions in the first if and the variant conditions
  // in the nested if, then extraction code below will be more
  // effective.
  splitIfAndStatements(for_stmt);

  splitIfOrStatements(for_stmt);
  
  // reverse variant and invariant if statements.
  flipIfStatements(for_stmt);

  // Extract any invariant statements possible
  if (qualify(for_stmt)) {
    NUStmtList * body_stmts = for_stmt->getBody();

    NUStmtList::iterator iter = body_stmts->begin();
    NUStmt * body_stmt = (*iter);
  
    NUCase * case_stmt = dynamic_cast<NUCase*>(body_stmt);
    NUIf * if_stmt = dynamic_cast<NUIf*>(body_stmt);

    if (case_stmt) {
      extract(case_stmt, for_stmt);
      if (mVerbose)
	UtIO::cout() << "Extracted case: ";
    } else if (if_stmt) {
      extract(if_stmt, for_stmt);
      if (mVerbose)
	UtIO::cout() << "Extracted if: ";
    } else {
      NU_ASSERT("Unexpected statement type."==0, body_stmt);
    }

    if (mVerbose) {
      body_stmt->getLoc().print();
      UtIO::cout() << " out of for loop: ";
      for_stmt->getLoc().print();
      UtIO::cout() << UtIO::endl;
    }

    // update the UD for each extracted stmt. People depend on us!
    mUD->stmt(body_stmt);
    results.push_back(body_stmt);

    // Give the for() loop an empty body -- when someone deletes the
    // original for() stmt, we don't want to have its body deleted.
    body_stmts->clear();
    for_stmt->replaceBody(*body_stmts);
    delete body_stmts;
    return true;
  } else {
    return false;
  }
}


bool InvariantExtractor::qualify(const NUFor * for_stmt) const 
{
  NUStmtList * body_stmts = for_stmt->getBody();
  bool qualified = (body_stmts->size() == 1);

  if (qualified) {
    NUStmtList::iterator iter = body_stmts->begin();
    NUStmt * body_stmt = (*iter);
  
    NUCase * case_stmt = dynamic_cast<NUCase*>(body_stmt);
    NUIf * if_stmt = dynamic_cast<NUIf*>(body_stmt);

    qualified = (case_stmt or if_stmt);
    if (qualified) {
      NUNetSet loop_defs;
      NUNetSet cond_uses;
      for_stmt->getBlockingDefs(&loop_defs);
      
      NUExpr * condition = NULL;
      if (case_stmt) {
	condition = case_stmt->getSelect();
	for (NUCase::ItemLoop item_loop = case_stmt->loopItems();
	     not item_loop.atEnd();
	     ++item_loop) {
	  NUCaseItem * item = (*item_loop);
	  for (NUCaseItem::ConditionLoop cond_loop = item->loopConditions();
	       not cond_loop.atEnd();
	       ++cond_loop) {
	    NUCaseCondition * cond = (*cond_loop);
	    cond->getUses(&cond_uses);
	  }
	}
      } else if (if_stmt) {
	condition = if_stmt->getCond();
      }
      NU_ASSERT(condition,body_stmt);
      
      // the if/case condition cannot use anything defined by the for() loop.
      qualified = isInvariant(condition,&loop_defs,&cond_uses);
    }
  }

  delete body_stmts;
  return qualified;
}


void InvariantExtractor::extract(NUCase * case_stmt, NUBlockStmt * block_stmt)
{
  NUStmtList * branch_stmts;
  NUStmtList body_stmts;

  for (NUCase::ItemLoop loop = case_stmt->loopItems(); 
       not loop.atEnd() ;
       ++loop) {
    NUCaseItem *item = (*loop);

    branch_stmts = item->getStmts();
    body_stmts.push_back(copy(block_stmt, branch_stmts));
    item->replaceStmts(body_stmts);
    body_stmts.clear();
    delete branch_stmts;
  }
}


void InvariantExtractor::extract(NUIf * if_stmt, NUBlockStmt * block_stmt)
{
  NUStmtList * branch_stmts;
  NUStmtList body_stmts;

  branch_stmts = if_stmt->getThen();
  if (not branch_stmts->empty()) {
    body_stmts.push_back(copy(block_stmt, branch_stmts));
  }
  if_stmt->replaceThen(body_stmts);
  body_stmts.clear();
  delete branch_stmts;

  branch_stmts = if_stmt->getElse();
  if (not branch_stmts->empty()) {
    body_stmts.push_back(copy(block_stmt, branch_stmts));
  }
  if_stmt->replaceElse(body_stmts);
  body_stmts.clear();
  delete branch_stmts;
}


NUBlockStmt * InvariantExtractor::copy(NUBlockStmt * block_stmt, NUStmtList * body_stmts)
{
  NUFor * for_stmt = dynamic_cast<NUFor*>(block_stmt);
  if (for_stmt) {
    return copy(for_stmt, body_stmts);
  }
  NU_ASSERT("Unexpected statement type."==0,block_stmt); // nyi - other types.
  return NULL;
}


NUFor * InvariantExtractor::copy(NUFor * for_stmt, NUStmtList * body_stmts)
{
  LoopFactory loop_factory(mStrCache, mNetRefFactory);
  return loop_factory(for_stmt, body_stmts);
}

void InvariantExtractor::splitIfAndStatements(NUFor* for_stmt)
{
  // We can only handle splitting if statements if there is only one
  // if statement in the for body.
  NUIf * if_stmt = getSingleIf(for_stmt->loopBody());
  if (not if_stmt) {
    return;
  }

  NUNetSet defs;
  for_stmt->getBlockingDefs(&defs);

  // Analyze the if statement conditional
  NUExpr* cond = if_stmt->getCond();
  NUExpr* invariant_cond;
  NUExpr* variant_cond;

  bool split_success = splitCondition(cond, defs, 
                                      NUOp::eBiBitOr, NUOp::eBiLogAnd, NUOp::eBiBitAnd, 
                                      invariant_cond, variant_cond);
  if (split_success) {
    // Make a new if statement for the variant conditional
    NUStmtList* then_stmts = if_stmt->getThen();
    NUStmtList* else_stmts = if_stmt->getElse();
    NUIf* new_if = new NUIf(variant_cond, *then_stmts, *else_stmts,
                            mNetRefFactory,
                            if_stmt->getUsesCFNet(),
                            if_stmt->getLoc());

    // Modify the current if statement to hold the new if
    // statement and be based on the invariant conditional instead
    // of the full conditional.
    NUStmtList sub_stmts;
    sub_stmts.push_back(new_if);

    bool has_then_stmts = not then_stmts->empty();
    bool has_else_stmts = not else_stmts->empty();

    if (has_then_stmts and not has_else_stmts) {
      // If the 'else' branch is empty, we do not need to duplicate
      // code in the 'then' clause:
      //
      //     if (VARIANT && INVARIANT) begin
      //         THEN_CLAUSE
      //     end
      //
      // becomes:
      //
      //     if (INVARIANT) begin
      //         if (VARIANT) begin
      //             THEN_CLAUSE
      //         end
      //     end
      //
      if_stmt->replaceThen(sub_stmts);
    } else {
      // If the 'then' branch is empty, we need to duplicate code in
      // the 'else' clause:
      //
      //     if (VARIANT && INVARIANT) begin
      //     end else begin
      //         ELSE_CLAUSE
      //     end
      //
      // becomes:
      // 
      //     if (INVARIANT) begin
      //         if (VARIANT) begin
      //         end else begin
      //             ELSE_CLAUSE
      //         end
      //     end else begin
      //         ELSE_CLAUSE (copy)
      //     end
      //

      // If both branches are populated:
      //
      //     if (VARIANT && INVARIANT) begin
      //         THEN_CLAUSE
      //     end else begin
      //         ELSE_CLAUSE
      //     end
      //
      // becomes:
      // 
      //     if (INVARIANT) begin
      //         if (VARIANT) begin
      //             THEN_CLAUSE
      //         end else begin
      //             ELSE_CLAUSE
      //         end
      //     end else begin
      //         ELSE_CLAUSE (copy)
      //     end
      //
      if_stmt->replaceThen(sub_stmts);
      sub_stmts.clear();
      if_stmt->replaceElse(sub_stmts);
      CopyContext copy_context(mScopes->back(),mStrCache,true);
      for (NUStmtList::iterator iter = else_stmts->begin();
           iter != else_stmts->end();
           ++iter) {
        if_stmt->addElseStmt((*iter)->copy(copy_context));
      }
    }
    if_stmt->replaceCond(invariant_cond);

    // Update the UD for the if statement because other for loop
    // operations count on it being accurate.
    mUD->stmt(if_stmt);

    // Clean up
    delete cond;
    delete then_stmts;
    delete else_stmts;

    // Print information if requested by the user
    if (mVerbose) {
      UtIO::cout() << "Split if condition to AND expressions: ";
      if_stmt->getLoc().print();
      UtIO::cout() << " inside for loop: ";
      for_stmt->getLoc().print();
      UtIO::cout() << "\n";
    }
  } // if
}


void InvariantExtractor::splitIfOrStatements(NUFor* for_stmt)
{
  // We can only handle splitting if statements if there is only one
  // if statement in the for body.
  NUIf * if_stmt = getSingleIf(for_stmt->loopBody());
  if (not if_stmt) {
    return;
  }

  NUNetSet defs;
  for_stmt->getBlockingDefs(&defs);

  // Analyze the if statement conditional
  NUExpr* cond = if_stmt->getCond();
  NUExpr* invariant_cond;
  NUExpr* variant_cond;

  bool split_success = splitCondition(cond, defs, 
                                      NUOp::eBiBitAnd, NUOp::eBiLogOr, NUOp::eBiBitOr, 
                                      invariant_cond, variant_cond);
  if (split_success) {
    // Make a new if statement for the variant conditional
    NUStmtList* then_stmts = if_stmt->getThen();
    NUStmtList* else_stmts = if_stmt->getElse();
    NUIf* new_if = new NUIf(variant_cond, *then_stmts, *else_stmts,
                            mNetRefFactory,
                            if_stmt->getUsesCFNet(),
                            if_stmt->getLoc());

    // Modify the current if statement to hold the new if
    // statement and be based on the invariant conditional instead
    // of the full conditional.
    NUStmtList sub_stmts;
    sub_stmts.push_back(new_if);

    bool has_then_stmts = not then_stmts->empty();
    bool has_else_stmts = not else_stmts->empty();

    if (has_else_stmts and not has_then_stmts) {
      // If the 'then' branch is empty, we do not need to duplicate
      // code in the 'else' clause:
      //
      //     if (VARIANT || INVARIANT) begin
      //     end else begin
      //         ELSE_CLAUSE
      //     end
      //
      // becomes:
      // 
      //     if (INVARIANT) begin
      //     end else begin
      //         if (VARIANT) begin
      //         end else begin
      //             ELSE_CLAUSE
      //         end
      //     end
      //
      if_stmt->replaceElse(sub_stmts);
    } else {
      // If the 'else' branch is empty, we need to duplicate code in
      // the 'then' clause:
      //
      //     if (VARIANT || INVARIANT) begin
      //         THEN_CLAUSE
      //     end
      //
      // becomes:
      //
      //     if (INVARIANT) begin
      //         THEN_CLAUSE
      //     end else begin
      //         if (VARIANT) begin
      //             THEN_CLAUSE (copy)
      //         end
      //     end
      //

      // If both branches are populated:
      //
      //     if (VARIANT || INVARIANT) begin
      //         THEN_CLAUSE
      //     end else begin
      //         ELSE_CLAUSE
      //     end
      //
      // becomes:
      // 
      //     if (INVARIANT) begin
      //         THEN_CLAUSE (copy)
      //     end else begin
      //         if (VARIANT) begin
      //             THEN_CLAUSE
      //         end else begin
      //             ELSE_CLAUSE
      //         end
      //     end
      //
      if_stmt->replaceElse(sub_stmts);
      sub_stmts.clear();
      if_stmt->replaceThen(sub_stmts);
      CopyContext copy_context(mScopes->back(),mStrCache,true);
      for (NUStmtList::iterator iter = then_stmts->begin();
           iter != then_stmts->end();
           ++iter) {
        if_stmt->addThenStmt((*iter)->copy(copy_context));
      }
    }
    if_stmt->replaceCond(invariant_cond);

    // Update the UD for the if statement because other for loop
    // operations count on it being accurate.
    mUD->stmt(if_stmt);

    // Clean up
    delete cond;
    delete then_stmts;
    delete else_stmts;

    // Print information if requested by the user
    if (mVerbose) {
      UtIO::cout() << "Split if condition to OR-expressions: ";
      if_stmt->getLoc().print();
      UtIO::cout() << " inside for loop: ";
      for_stmt->getLoc().print();
      UtIO::cout() << "\n";
    }
  } // if
}


void InvariantExtractor::flipIfStatements(NUFor* for_stmt)
{
  // We can only handle splitting if statements if there is only one
  // if statement in the for body.
  NUIf * if_stmt = getSingleIf(for_stmt->loopBody());
  if (not if_stmt) {
    return;
  }

  NUNetSet defs;
  for_stmt->getBlockingDefs(&defs);

  NUExpr * variant_cond = if_stmt->getCond();
  if (isInvariant(variant_cond, &defs)) {
    // The outer if stmt is already an invariant control.
    return; 
  }

  NUIf * then_if = getSingleIf(if_stmt->loopThen());
  NUIf * else_if = getSingleIf(if_stmt->loopElse());

  bool then_if_invariant = false;
  bool else_if_invariant = false;
  if (then_if) {
    then_if_invariant = (isInvariant(then_if->getCond(), &defs));
  }
  if (else_if) {
    else_if_invariant = (isInvariant(else_if->getCond(), &defs));
  }

  bool balanced = false;
  if (then_if_invariant and else_if_invariant) {
    NU_ASSERT(then_if and else_if,for_stmt);
    NUExpr * then_cond = then_if->getCond();
    NUExpr * else_cond = else_if->getCond();
    if ((*then_cond) == (*else_cond)) {
      balanced = true;
      flipIfBalancedStatements(for_stmt, if_stmt, then_if, else_if);
    }
  }

  if (not balanced) {
    if (then_if_invariant) {
      NU_ASSERT(then_if,for_stmt);
      flipIfThenStatements(for_stmt, if_stmt, then_if);
    } else if (else_if_invariant) {
      NU_ASSERT(else_if,for_stmt);
      flipIfElseStatements(for_stmt, if_stmt, else_if);
    }
  }
}


void InvariantExtractor::flipIfBalancedStatements(NUFor * for_stmt,
                                                  NUIf * if_stmt,
                                                  NUIf * then_if,
                                                  NUIf * else_if)
{
  NUExpr * variant_cond   = if_stmt->getCond();
  NUExpr * invariant_cond = then_if->getCond();

  NUStmtList * then_if_then = then_if->getThen();
  NUStmtList * then_if_else = then_if->getElse();
  NUStmtList * else_if_then = else_if->getThen();
  NUStmtList * else_if_else = else_if->getElse();

  NUStmtList empty;
  CopyContext copy_context(mScopes->back(),mStrCache,true);

  NUIf * new_then_if = new NUIf(variant_cond->copy(copy_context),
                                *then_if_then, *else_if_then,
                                mNetRefFactory,
                                if_stmt->getUsesCFNet(),
                                if_stmt->getLoc());

  NUIf * new_else_if = new NUIf(variant_cond->copy(copy_context),
                                *then_if_else, *else_if_else,
                                mNetRefFactory,
                                if_stmt->getUsesCFNet(),
                                if_stmt->getLoc());

  NUIf * new_outer_if = new NUIf(invariant_cond->copy(copy_context),
                                 empty, empty,
                                 mNetRefFactory,
                                 if_stmt->getUsesCFNet(),
                                 then_if->getLoc());
  new_outer_if->addThenStmt(new_then_if);
  new_outer_if->addElseStmt(new_else_if);

  for_stmt->replaceBody(empty);
  for_stmt->addBodyStmt(new_outer_if);

  mUD->stmt(new_outer_if);

  // Print information if requested by the user
  if (mVerbose) {
    UtIO::cout() << "Flipped variant if and invariant if-then and if-else statements: ";
    if_stmt->getLoc().print();
    UtIO::cout() << " inside for loop: ";
    for_stmt->getLoc().print();
    UtIO::cout() << "\n";
  }

  // the stmts are now owned by the new ifs; clear bodies before deletion.
  then_if->replaceThen(empty);
  then_if->replaceElse(empty);
  else_if->replaceThen(empty);
  else_if->replaceElse(empty);

  delete if_stmt; // only need to delete the old outer if.
  delete then_if_then;
  delete then_if_else;
  delete else_if_then;
  delete else_if_else;

}


void InvariantExtractor::flipIfThenStatements(NUFor * for_stmt,
                                              NUIf * if_stmt,
                                              NUIf * then_if)
{
  NUExpr * variant_cond   = if_stmt->getCond();

  NUStmtList * outer_else = if_stmt->getElse();
  NUStmtList * inner_then = then_if->getThen();
  NUStmtList * inner_else = then_if->getElse();

  NUStmtList empty;
  CopyContext copy_context(mScopes->back(),mStrCache,true);

  NUIf * new_if = new NUIf(variant_cond->copy(copy_context),
                           *inner_else, empty,
                           mNetRefFactory,
                           if_stmt->getUsesCFNet(),
                           if_stmt->getLoc());
  for (NUStmtList::iterator iter = outer_else->begin();
       iter != outer_else->end();
       ++iter) {
    new_if->addElseStmt((*iter)->copy(copy_context));
  }

  // move the invariant out.
  for_stmt->replaceBody(empty);
  for_stmt->addBodyStmt(then_if);

  // invariant now contains the old, outer if-stmt.
  then_if->replaceThen(empty);
  then_if->addThenStmt(if_stmt);
  then_if->replaceElse(empty);
  then_if->addElseStmt(new_if);

  // update the variant clauses.
  if_stmt->replaceThen(*inner_then);
  if_stmt->replaceElse(*outer_else);

  mUD->stmt(then_if);

  // Print information if requested by the user
  if (mVerbose) {
    UtIO::cout() << "Flipped variant if and invariant if-then statements: ";
    if_stmt->getLoc().print();
    UtIO::cout() << " inside for loop: ";
    for_stmt->getLoc().print();
    UtIO::cout() << "\n";
  }

  delete outer_else;
  delete inner_then;
  delete inner_else;
}


void InvariantExtractor::flipIfElseStatements(NUFor * for_stmt,
                                              NUIf * if_stmt,
                                              NUIf * else_if)
{
  NUExpr * variant_cond   = if_stmt->getCond();

  NUStmtList * outer_then = if_stmt->getThen();
  NUStmtList * inner_then = else_if->getThen();
  NUStmtList * inner_else = else_if->getElse();

  NUStmtList empty;
  CopyContext copy_context(mScopes->back(),mStrCache,true);

  NUIf * new_if = new NUIf(variant_cond->copy(copy_context),
                           empty, *inner_else,
                           mNetRefFactory,
                           if_stmt->getUsesCFNet(),
                           if_stmt->getLoc());
  for (NUStmtList::iterator iter = outer_then->begin();
       iter != outer_then->end();
       ++iter) {
    new_if->addThenStmt((*iter)->copy(copy_context));
  }

  // move the invariant out.
  for_stmt->replaceBody(empty);
  for_stmt->addBodyStmt(else_if);

  // invariant now contains the old, outer if-stmt.
  else_if->replaceThen(empty);
  else_if->addThenStmt(if_stmt);
  else_if->replaceElse(empty);
  else_if->addElseStmt(new_if);

  // update the variant clauses.
  if_stmt->replaceThen(*outer_then);
  if_stmt->replaceElse(*inner_then);

  mUD->stmt(else_if);

  // Print information if requested by the user
  if (mVerbose) {
    UtIO::cout() << "Flipped variant if and invariant if-else statements: ";
    if_stmt->getLoc().print();
    UtIO::cout() << " inside for loop: ";
    for_stmt->getLoc().print();
    UtIO::cout() << "\n";
  }

  delete outer_then;
  delete inner_then;
  delete inner_else;
}

NUIf * InvariantExtractor::getSingleIf(NUStmtLoop loop)
{
  if (loop.atEnd()) {
    return NULL; // empty body.
  }

  NUStmt * stmt = (*loop);
  ++loop;
  if (not loop.atEnd()) {
    return NULL; // more than one stmt in body.
  }

  // Process the statement, if it is an if we could create a new
  // statement, otherwise, we leave it alone.
  if (stmt->getType() == eNUIf) {
    return dynamic_cast<NUIf*>(stmt);
  } else {
    return NULL;
  }
}


bool InvariantExtractor::isInvariant(NUExpr * expr, NUNetSet * defs, NUNetSet * other_uses) const
{
  NUNetSet uses;
  expr->getUses(&uses);

  if (other_uses) {
    uses.insert(other_uses->begin(),other_uses->end());
  }

  return not set_has_intersection(*defs,uses);
}


bool InvariantExtractor::splitCondition(NUExpr * condition, NUNetSet& defs,
                                        NUOp::OpT eq_op_type,
                                        NUOp::OpT log_op_type,
                                        NUOp::OpT bit_op_type,
					NUExpr *& invariantCondition,
					NUExpr *& variantCondition)
{
  bool splitFound = false;
  if (condition->getType() == NUExpr::eNUBinaryOp) {
    NUBinaryOp* op = dynamic_cast<NUBinaryOp*>(condition);

    NUOp::OpT op_type = op->getOp();
    if ((op_type == NUOp::eBiEq) or (op_type == NUOp::eBiNeq)) {
      // Looking for ((a | b | c) == 0)
      // Looking for ((a & b & c) != 0)

      NUOp::OpT cmp_op_type = (op_type==NUOp::eBiEq) ? eq_op_type : bit_op_type;

      bool success = ( isConstantZero(op->getArg(1)) and
		       qualifyConditions(op->getArg(0), cmp_op_type) );
      if (success) {
	// Break up the conditions
	NUExprList invariantConditions;
	NUExprList variantConditions;
	splitBinaryConditions(op->getArg(0), cmp_op_type, defs, 
			      invariantConditions, variantConditions);
	success = ( (not invariantConditions.empty()) and
		    (not variantConditions.empty()) );
	if (success) {
	  // Create the new conditions
	  invariantCondition = createEqOp(invariantConditions, cmp_op_type, op);
	  variantCondition   = createEqOp(variantConditions, cmp_op_type, op);
	  splitFound = true;
	}
      }
    } else if (op_type==bit_op_type || op_type==log_op_type) {
      // Looking for (a & b)
      // Looking for (a && b)

      bool success = qualifyConditions(op,op_type);
      if (success) {
	// Break up the conditions
	NUExprList invariantConditions;
	NUExprList variantConditions;

	splitBinaryConditions(op, op_type, defs, 
			      invariantConditions, variantConditions);
	success = ( (not invariantConditions.empty()) and
		    (not variantConditions.empty()) );
	if (success) {
	  // Create the new conditions
	  invariantCondition = createOp(invariantConditions,op_type,op);
	  variantCondition   = createOp(variantConditions,op_type,op);
	  splitFound = true;
	}
      }
    }
  } 
  return splitFound;
}

bool InvariantExtractor::qualifyConditions(NUExpr* condition, NUOp::OpT op_type)
{
  // Recursive routine to see if we can split up this conditional into
  // multiple pieces. This is only true if we have a bitwise and of
  // multiple single bit values.
  //
  // This routine recurses down if we hit a bitwise and operation. It
  // tests the bit size otherwise.
  bool qualified = false;
  bool recurse = false;

  if (condition->getType() == NUExpr::eNUBinaryOp) {
    NUBinaryOp* op = dynamic_cast<NUBinaryOp*>(condition);
    if (op->getOp() == op_type) {
      recurse = true;
      qualified = qualifyConditions(op->getArg(0),op_type);
      if (qualified) {
	qualified = qualifyConditions(op->getArg(1),op_type);
      }
    }
  }

  // If we didn't recurse, test the bit size
  if (not recurse) {
    qualified = (condition->getBitSize() == 1);
  }
  return qualified;
}

void InvariantExtractor::splitBinaryConditions(NUExpr * condition, NUOp::OpT op_type,
					       NUNetSet& defs,
					       NUExprList & invariantConditions,
					       NUExprList & variantConditions)
{
  // Recursive routine to split things up. If this is an & condition,
  // keep traversing. Otherwise, test whether this condition is
  // variant or invariant
  bool recurse = false;

  if (condition->getType() == NUExpr::eNUBinaryOp) {
    NUBinaryOp* op = dynamic_cast<NUBinaryOp*>(condition);
    if (op->getOp() == op_type) {
      splitBinaryConditions(op->getArg(0), op_type, defs, 
			    invariantConditions, variantConditions);
      splitBinaryConditions(op->getArg(1), op_type, defs, 
			    invariantConditions, variantConditions);
      recurse = true;
    }
  }

  // If we didn't recurse, put this condition in the variant or invariant list
  if (not recurse) {
    NUNetSet uses;
    condition->getUses(&uses);
    
    if (not set_has_intersection(defs, uses)) {
      invariantConditions.push_back(condition);
    } else {
      variantConditions.push_back(condition);
    }
  }
}


NUExpr * InvariantExtractor::createBinaryOp(NUExprList & conditions, 
					    NUOp::OpT op_type,
					    const SourceLocator & loc)
{
  CopyContext copy_context(NULL,NULL);

  // Create the chain of binary operators. 
  // Note: This creates a linear chain, not a binary tree.
  NUExprListIter iter = conditions.begin();
  NUExpr * source = (*iter);
  NUExpr* result = (*iter)->copy(copy_context);
  result->resize(source->getBitSize());
  for (++iter; 
       iter != conditions.end(); 
       ++iter) {
    NUExpr * source = (*iter);
    NUExpr * condition = source->copy(copy_context);
    condition->resize(source->getBitSize());
    result = new NUBinaryOp(op_type, result, condition, loc);
  }
  return result;
}


NUExpr * InvariantExtractor::createEqOp(NUExprList& conditions, 
                                        NUOp::OpT op_type,
                                        NUBinaryOp* old_condition)
{
  NUExpr * condition = createBinaryOp(conditions,op_type,old_condition->getLoc());

  // Compare it to the same constant.
  CopyContext copy_context(NULL,NULL);
  NUExpr * old_value = old_condition->getArg(1);
  NUExpr * value = old_value->copy(copy_context);
  value->resize(old_value->getBitSize());

  // Generate the comparison.
  NUOp::OpT cmp_op_type = old_condition->getOp();
  NUExpr * comparison = new NUBinaryOp(cmp_op_type, condition, value, old_condition->getLoc());
  comparison->resize(old_condition->getBitSize());
  return comparison;
}


NUExpr * InvariantExtractor::createOp(NUExprList & conditions,
                                      NUOp::OpT op_type,
                                      NUBinaryOp * old_condition)
{
  NUExpr * condition = createBinaryOp(conditions,op_type,old_condition->getLoc());
  condition->resize(old_condition->getBitSize());
  return condition;
}


bool InvariantExtractor::isConstantZero(NUExpr * expr)
{
  if ((expr->getType() == NUExpr::eNUConstNoXZ) and
      (expr->getBitSize() == 1)) {
    NUConst* constExpr = expr->castConst ();
    return constExpr->isZero ();
  } else {
    return false;
  }
}
    

NUFor * LoopFactory::operator()(NUFor * for_stmt, NUStmtList * body_stmts)
{
  CopyContext copy_context(NULL,NULL);
  const NUStmtList * initial_copy = for_stmt->getInitial(true); // this is a copy of list and statements
  NUExpr           * condition = for_stmt->getCondition();
  const NUStmtList * advance_copy = for_stmt->getAdvance(true); // this is a copy of list and statements

  NU_ASSERT(initial_copy and advance_copy, for_stmt);

  NUExpr * condition_copy = condition->copy(copy_context);

  NUFor * for_copy = new NUFor((*initial_copy),
			       condition_copy,
			       (*advance_copy),
			       (*body_stmts),
			       mNetRefFactory,
                               for_stmt->getUsesCFNet(),
			       for_stmt->getLoc());

  // now delete only the lists, the statement copies were put into the NUFor
  delete initial_copy;
  delete advance_copy;
  return for_copy;
}

void LoopUnrollStatistics::print() const
{
  UtIO::cout() << "LoopUnroll Summary: "
               << mLoops << " loops were unrolled in " << mModules << " modules";
  if (mForced > 0)
    UtIO::cout() << " (" << mForced << " by force)";
  UtIO::cout() << "." << UtIO::endl;
}

//! Class to compute the nested cost of unrolling a loop
/*! The idea is to size all the nested for loops to figure out the
 *  total number of unrolls.
 */
class REUnrollSize : public NUDesignCallback
{
public:
  //! constructor
  REUnrollSize() : mUnrollSize(0) {}

  //! Destructor
  ~REUnrollSize()
  {
    INFO_ASSERT(mUnrollSizes.empty(),
                "Stack nesting problem in unroll computation");
  }

  //! Get the unroll size
  SInt32 getSize(void) const { return mUnrollSize; }

  //! Override the base to catch everything we don't care about
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Override the for statement type to compute the total unroll size
  Status operator()(Phase phase, NUFor* forStmt)
  {
    // In the pre phase, push a new nesting, in the post phase, pop
    // the nesting
    if (phase == ePre) {
      pushNesting();
    } else {
      // Compute the iterations of the for loop
      SInt32 loopCount;
      bool valid = forStmt->estimateLoopCount(&loopCount);
      if (!valid) {
        // Not sure what to do yet. For now use a constant 1 so we
        // don't change the current behavior. In the future we should
        // make the forStmt estimator better so it handles as many
        // cases as loop unrolling does.
        loopCount = 1;
      }

      // Pop the nesting
      popNesting(loopCount);
    }
    return eNormal;
  }

private:
  //! Function to push a new statement cost for a new For nesting
  void pushNesting()
  {
    mUnrollSizes.push_back(mUnrollSize);
    mUnrollSize = 0;
  }

  //! Function to pop a statement size and add it to the unroll size
  void popNesting(SInt32 forIterations)
  {
    // Get the previous nestings sizes off the stack
    SInt32 unrollSize = mUnrollSizes.back();
    mUnrollSizes.pop_back();

    // Add it to the current unroll size. At this point unrollSize is
    // the current count for the parent of this for loop, mUnrollSize
    // is the size for any nested for loops, and forIterations is the
    // number of iterations for this for loop.
    if (mUnrollSize == 0) {
      // No nested for loops so count this as a nesting value of 1.
      mUnrollSize = unrollSize + forIterations;
    } else {
      // There are nested for loops. Multiply that unroll size by the
      // iterations for this for loop and add it to the parent for
      // loops size.
      mUnrollSize = unrollSize + (mUnrollSize * forIterations);
    }
  }

  //! Stack of unroll sizes, each entry is for a different for nesting
  typedef UtVector<SInt32> UnrollSizes;
  UnrollSizes mUnrollSizes;

  //! The unroll size for the current nesting
  SInt32 mUnrollSize;
}; // class REUnrollCost : public NUDesignCallback

bool LoopUnroll::estimateBeneficial(NUFor* stmt)
{
  // Validate that we are within the number of iteration threshold.
  SInt32 estimated_loop_count = 0;
  bool estimate_valid = stmt->estimateLoopCount(&estimated_loop_count);
  if (estimate_valid) {
    if (estimated_loop_count > MAX_UNROLL_ITERATIONS) {
      if (mVerbose) {
        dumpEstimatedIterationFailure(stmt->getLoc(),estimated_loop_count);
      }
      return false;
    }
  }

  // Walk the statments and make sure there are some optimizable
  // statements. See the callback class for details
  REUnrollSize unrollSize;
  NUDesignWalker walker(unrollSize, false /* verbose */);
  walker.forStmt(stmt);
  if (unrollSize.getSize() > (MAX_UNROLL_SIZE)) {
    if (mVerbose) {
      dumpEstimatedUnrollSizeFailure(stmt->getLoc(), unrollSize.getSize());
    }
    return false;
  }

  return true;
} // void LoopUnroll::estimateBeneficial

  
