// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "compiler_driver/CarbonContext.h"
#include "flow/FLFactory.h"
#include "nucleus/NUDesign.h"
#include "reduce/CleanElabFlow.h"
#include "reduce/CleanTaskResolutions.h"
#include "reduce/CrossHierarchyMerge.h"
#include "reduce/DeadNets.h"
#include "reduce/ReachableAliases.h"
#include "reduce/Reduce.h"
#include "reduce/REAlias.h"
#include "reduce/REAlloc.h"
#include "reduce/REDeadNets.h"
#include "reduce/RETriInit.h"
#include "reduce/RETriFlags.h"
#include "reduce/RemoveDeadCode.h"
#include "reduce/SanityElab.h"
#include "reduce/SanityUnelab.h"
#include "reduce/TieNets.h"
#include "reduce/RENetWarning.h"
#include "symtab/STSymbolTable.h"
#include "util/Stats.h"

Reduce::Reduce(Stats* stats, NUDesign* design,
               bool verbose_cleanup,
               bool verbose_sanity,
               bool dump_x_hier,
               bool verbose_tri_init,
               bool dump_allocation,
               int internal_assert_test,
               FLNodeElabFactory* flow_elab_factory,
               FLNodeFactory* flow_factory,
               ReachableAliases* reachable_aliases,
               TristateModeT tristate_mode)
  : mStats(stats),
    mDesign(design),
    mVerboseCleanup(verbose_cleanup),
    mVerboseSanity(verbose_sanity),
    mDumpXHier(dump_x_hier),
    mVerboseTriInit(verbose_tri_init),
    mDumpAllocation(dump_allocation),
    mInternalAssertTest(internal_assert_test),
    mFlowElabFactory(flow_elab_factory),
    mFlowFactory(flow_factory),
    mReachableAliases(reachable_aliases),
    mTristateMode(tristate_mode)
{
}

// This routine should run after aliasing the guarantee that there are
// only elaborated flow nodes in the design.
static void testAssert(int testNum, FLNodeElabFactory* factory)
{
  // Get an iterator to choose an elaborated flow
  FLNodeElabFactory::FlowLoop l = factory->loopFlows();
  FLNodeElab* flowElab = NULL;

  // Handle the various possible failures in a FLNodeElab access
  switch (testNum) {
    case 1:
      // test the null elaborated flow case
      FLN_ELAB_ASSERT(false, flowElab);
      break;

    case 2:
      // Test with a valid flnode elab with a NULL use def node
      while ((flowElab == NULL) && l(&flowElab)) {
        if (flowElab->getUseDefNode() != NULL) {
          // Keep looking
          flowElab = NULL;
        }
      }

      // Make sure we got an elaborated flow
      FLN_ELAB_ASSERT((flowElab != NULL), flowElab);

      // This is the assert we are regression testing
      FLN_ELAB_ASSERT(false, flowElab);
      break;

    case 3:
      // Test with a valid flnode elab with a valid use def node
      while ((flowElab == NULL) && l(&flowElab)) {
        if (flowElab->getUseDefNode() == NULL) {
          // Keep looking
          flowElab = NULL;
        }
      }

      // Make sure we got an elaborated flow
      FLN_ELAB_ASSERT((flowElab != NULL), flowElab);

      // This is the assert we are regression testing
      FLN_ELAB_ASSERT(false, flowElab);
      break;
  } // switch
} // static void testAssert

// This is brought out as a separate static function for easy isolation
// with kcachegrind
extern "C" void performAliasing(NUDesign* design,
                                STSymbolTable* symtab,
                                FLNodeElabFactory* flowFactory,
                                MsgContext* msgContext,
                                NUNetRefFactory *netref_factory,
                                Stats* stats,
                                SInt32 aliasing_verbosity,
                                bool flowBasedAllocator)
{
  REAlias preAlias(design, symtab, flowFactory, msgContext, netref_factory,
                   (aliasing_verbosity == 1),
                   NULL,      // predicate
                   flowBasedAllocator);
  preAlias.aliasDesign(stats, true);
  CarbonMem::releaseBlocks();
}

bool Reduce::runHierAlias() {
  bool success = true;

  STSymbolTable* symbol_table = mDesign->getSymbolTable();
  MsgContext* msg_context = mDesign->getMsgContext();
  IODBNucleus* iodb = mDesign->getIODB();
  ArgProc* args = mDesign->getArgProc();
  NUNetRefFactory* net_ref_factory = mDesign->getNetRefFactory();

  mStats->pushIntervalTimer();

  // Compute set of reachable symtab nodes.
  // Must come before hierarchical aliasing but after elaboration.
  mReachableAliases->compute(symbol_table);

  // We are only going to run the new allocator if asked to by the user,
  // but we will allocate one independently, because it looks like its
  // propagateFlags logic helps EMC M9 be compilable
  REAlloc* alloc = NULL;
  alloc = new REAlloc(symbol_table, iodb, args, msg_context,
                      mDumpAllocation, mStats,
                      mFlowElabFactory, mFlowFactory, mReachableAliases);

  // unelaborated sanity check.
  {
    SanityUnelab sanity(msg_context, net_ref_factory, mFlowFactory, true, mVerboseSanity);
    sanity.design(mDesign);
  }

  // elaborated sanity check.
  {
    SanityElab sanity(msg_context, symbol_table, net_ref_factory, mFlowFactory, mFlowElabFactory, true, mVerboseSanity);
    sanity.design(mDesign);
  }

  // Hierarchical (port-based) aliasing
  {
    SInt32 aliasing_verbosity;
    ArgProc::OptionStateT arg_status = args->getIntLast(CRYPT("-verboseAliasing"), &aliasing_verbosity);
    INFO_ASSERT(arg_status==ArgProc::eKnown, "aliasing swicthes not properly initialized");
    performAliasing(mDesign, symbol_table, mFlowElabFactory, msg_context, net_ref_factory, mStats,
                   aliasing_verbosity, false);
  }

  mStats->printIntervalStatistics(CRYPT("Alias"));

  // Resolve and mark depositable nets after hierarchical aliasing.
  iodb->resolveDepositable((CbuildSymTabBOM*) symbol_table->getFieldBOM(),
                           mReachableAliases);
  // OnDemand excluded nets as well
  iodb->resolveOnDemandExcluded();

  // unelaborated sanity check.
  {
    SanityUnelab sanity(msg_context, net_ref_factory, mFlowFactory, true, mVerboseSanity);
    sanity.design(mDesign);
  }

  // elaborated sanity check.
  {
    SanityElab sanity(msg_context, symbol_table, net_ref_factory, mFlowFactory, mFlowElabFactory, true, mVerboseSanity);
    sanity.design(mDesign);
  }

  success = alloc->design(mDesign);
  mStats->printIntervalStatistics(CRYPT("Allocation"));
  delete alloc;

  if (mInternalAssertTest > 0) {
    testAssert(mInternalAssertTest, mFlowElabFactory);
  }

  mStats->popIntervalTimer();

  return success;
} // bool Reduce::runHierAlias

bool Reduce::runReduce() {
  mStats->pushIntervalTimer();

  STSymbolTable* symbol_table = mDesign->getSymbolTable();
  AtomicCache* atomic_cache = symbol_table->getAtomicCache();
  MsgContext* msg_context = mDesign->getMsgContext();
  IODBNucleus* iodb = mDesign->getIODB();
  ArgProc* args = mDesign->getArgProc();
  NUNetRefFactory* net_ref_factory = mDesign->getNetRefFactory();
  SourceLocatorFactory* source_locator_factory = mDesign->getSourceLocatorFactory();

  bool mVerboseSanity = args->getBoolValue(CRYPT("-verboseSanity"));

  // Cleanup after allocation
  {
    CleanElabFlow cleanup(mFlowElabFactory, symbol_table, mVerboseCleanup);
    cleanup.design(mDesign);
  }
  mStats->printIntervalStatistics(CRYPT("DesElabCleanup"));

  /*
    This -multiLevelHierTasks if statement is NASTY FIX for Neteffect,
    so Dylan can use -multiLevelHierTasks with the PCIX transactor.
    
    THIS IS NOT CHECKED INTO THE MAINLINE

    THIS IF SHOULD BE REMOVED AND CleanTaskResolutions SHOULD ALWAYS
    RUN!!!!!!!
    - MS
  */
  if (! args->getBoolValue(CRYPT("-multiLevelHierTasks")))
  {
    // Remove any dead task resolutsion
    CleanTaskResolutions cleanTaskResolutions;
    cleanTaskResolutions.design(mDesign, symbol_table);
    mStats->printIntervalStatistics(CRYPT("HierResCleanup"));
  }

  // track deleted nets so their elaborated (and symtab) versions can be sanitized.
  DeadNetCallback dead_net_callback(symbol_table);

  // Perform a dead code pass so that we get rid of unnecessary unelaborated flow.
  {
    RemoveDeadCode cleanup(net_ref_factory, mFlowFactory,
                           iodb, args, msg_context, 
                           false, // only start at important nets.
                           true,  // always delete dead code.
                           false, // do not delete module instances
                           mVerboseCleanup);
    cleanup.design(mDesign,&dead_net_callback);
  }
  mStats->printIntervalStatistics(CRYPT("RemoveDeadCode"));

  // Unelaborated sanity check
  {
    SanityUnelab sanity(msg_context, net_ref_factory, mFlowFactory, true, mVerboseSanity);
    sanity.design(mDesign);
  }
  mStats->printIntervalStatistics(CRYPT("SanityUnelab"));

  // Elaborated sanity check
  {
    SanityElab sanity(msg_context, symbol_table, net_ref_factory, mFlowFactory, mFlowElabFactory, true, mVerboseSanity);
    sanity.design(mDesign);
  }
  mStats->printIntervalStatistics(CRYPT("SanityElab"));

  // Warn about any ineffective tie net directives
  {
    NUNetSet* tieNetTemps = mDesign->getTieNetTemps();
    RETieNets tieNets(mDesign, atomic_cache, iodb, msg_context, NULL,
                      true, true, tieNetTemps);
    tieNets.findTienetIssues();
  }

  // Determine cross-hierarchy merge statistics
  if (mDumpXHier) {
    CrossHierarchyMerge x_hier_merge;
    x_hier_merge.design(mDesign, symbol_table);
    mStats->printIntervalStatistics(CRYPT("X-Hier Merge"));
  }

  // Analyze Z propagation issues.  This must be done after LFTristate,
  // which is needed to analyze Z drivers in always blocks, but it's
  // preferable to do it before tristate initialization, which will
  // create synthetic drivers to initialize tristate nets
  {
    RENetWarning netWarn(net_ref_factory, symbol_table, iodb,
                         msg_context, mReachableAliases,
                         (REAlias*) 0, // aliasing already done
                         mDesign, args, mFlowElabFactory);

    // Warn on Z propagation limitations.  Thi
    netWarn.walkDesignZ();
    netWarn.printZConflictWarnings();
#if 0
    UtString driversFile;
    driversFile << getFileRoot() << ".drivers";
    ZOSTREAMDB(drivers, driversFile.c_str());
    bool ok = drivers.is_open();
    if (ok) {
      netWarn.dumpDrivers(&drivers);
      ok = drivers.close();
    }
    if (!ok) {
      fprintf(stdout, "%s\n", drivers.getErrmsg());
    }
#endif
  }

  // Tristate-initialization
  /*
    Tristate initialization in addition to initializing tristates also
    recalculates the read/write flags for the design. This is needed
    in order for resolveForce() to properly see hierrefs initializers
    as written. See bug 4453, test/hierref_port/bug4453.v
  */
  {
    RETriInit triInit(mDesign,
		      symbol_table,
                      atomic_cache,
		      mFlowFactory,
		      mFlowElabFactory,
		      net_ref_factory,
		      msg_context,
		      iodb,
                      args,
                      source_locator_factory,
                      mReachableAliases,
		      mTristateMode,
		      mVerboseTriInit);
    triInit.triInitDesign(mStats, true);
  }
  mStats->printIntervalStatistics(CRYPT("TriInit"));

  {
    RETriFlags tristate_flags(symbol_table,
                              mReachableAliases);
    tristate_flags.propagate(mStats, true);
  }
  mStats->printIntervalStatistics("TriFlagPropagate");

  {
    REDeadNets dead_nets(symbol_table,
                         mFlowElabFactory,
                         iodb);
    dead_nets.design(mDesign);
  }
  mStats->printIntervalStatistics(CRYPT("DeadNets"));

  /* resolveForce must run AFTER tristate initialization so that
     hierref initializers will properly be marked as written.
     See bug 4453, test/hierref_port/bug4453.v
  */
  iodb->resolveForce();
  mStats->printIntervalStatistics(CRYPT("IODBResolveForce"));
  return true;
} // bool Reduce::run
