// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _LOCALPROPAGATION_H_
#define _LOCALPROPAGATION_H_

#include "nucleus/NUDesignWalker.h"
#include "util/GraphBuilder.h"

#include "reduce/LocalPropagationStatistics.h"
#include "reduce/LocalPropagationBase.h"

class Fold;

/*!
  Common framework for stack-based propagation approaches.

  As a design walker traverses Nucleus, this callback maintains a
  stack of contexts which include the currently available
  propagations.

  All subclasses need to implement the following methods:
      enrollAssign
      enterIfBranch

  All tasks must be walked with an empty context. It is currently
  considered unsafe to propagate expressions from elsewhere in the
  module into tasks. This is because a task may be called from
  multiple contexts. The current implementation does not determine
  which external definitions are safe for propagation into a task.
 */
class LocalPropagationCallback : public NUDesignCallback
{
public:
  //! Constructor
  /*!
    iodbNucleus           - The IODB object.
    fold                  - A Fold object used for expression simplification.
    statistics            - Statistics object for noting successful replacements.
    storing_non_constants - Must be true if the stored propagations may be non-constant.
   */
  LocalPropagationCallback(IODBNucleus* iodbNucleus, 
                           Fold* fold, 
                           LocalPropagationStatistics * statistics,
                           bool storing_non_constants);

  //! Destructor
  ~LocalPropagationCallback();

  //! Pushes a new context onto the stack.
  void pushContext();

  //! Removes the constant context from the top of the stack.
  /*! 
    If the current context is within a nesting, the propagation
    expressions are saved aside so they can be applied to the parent
    context when we leave the nested object.
   */
  void popContext();

  //! Add a propagation expression to the current context.
  /*! 

    The provided expression is added to the top propagation stack. A
    copy of the expression is saved; the original expression may be
    deleted if needed.

    If the provided expression is NULL, any cached expression is
    cleared.
   */
  void addNet(NUNet* net, NUExpr * expr);

  //! Get the changed flag.
  /*!
    The changed status is reset upon entry of each always block, task,
    or continuous assignment. The changed status will be true if any
    expression had an expression propagated into it.
   */
  bool getChanged() const { return mChanged; }

  //! Catch all.
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! All unhandled statements clear the defs from the cache
  Status operator()(Phase phase, NUStmt* stmt);

  //! Process blocks.
  /*!
    Blocks are simply levels of scoping. The do not define nets by
    themselves, therefore they do not affect the propagation cache.
   */
  Status operator()(Phase, NUBlock*) { return eNormal; }

  //! For statements need to pre clear the defs
  Status operator()(Phase phase, NUFor* forStmt);

  //! If statement, start a new nested traversal
  Status operator()(Phase phase, NUIf* ifStmt);

  //! Case statement, start a new nested traversal
  Status operator()(Phase phase, NUCase* caseStmt);

  //! Assign statements, look for constants
  Status operator()(Phase phase, NUAssign* assign);

  //! Optimize variable select lvalues. The select may be constant
  Status operator()(Phase phase, NUVarselLvalue* lvalue);

  //! Optimize variable select lvalues. The select may be constant
  Status operator()(Phase phase, NUMemselLvalue* lvalue);

  //! Optimize case item expressions
  Status operator()(Phase phase, NUCaseCondition* cond);

  //! Optimize continuous assignments.
  Status operator()(Phase phase, NUContAssign * assign);

  //! Optimize tasks.
  Status operator()(Phase phase, NUTask * task);

  //! Optimize an always block
  Status operator()(Phase phase, NUStructuredProc* proc);

protected:

  //! Analyze an assignment and conditionally add a propagation.
  /*!
    Subclasses are responsible for implementing enrollment into the
    propagation cache. The provided assignment must be analyzed for a
    valid propagation opportunity. 

    If a propagation is discovered, perform a call to ::addNet() with
    the appropriate net and expression.
   */
  virtual void enrollAssign(NUAssign * assign) = 0;

  //! Analyze an if statement and conditionally add a propagation.
  /*!
    Subclasses are responsible for implementing enrollment into the
    propagation cache from if statements. The provided if statement
    may be analyzed in an attempt to infer propagation values for
    different branches of an if statement.

    If a propagation is discovered, perform a call to ::addNet() with
    the appropriate net and expression.
   */
  virtual void enterIfBranch(NUIf * ifStmt, bool selector) = 0;

  //! Function to optimize an expression in the nucleus tree.
  /*!
    Given the current contents of the propagation cache, the provided
    expression has all propagation opportunities replaced. The result
    is simplified upon replacement.

    If the result is non-NULL, an optimization has been performed.
   */
  NUExpr* optimizeExpr(NUExpr* expr);

  //! Return the IODB object.
  IODBNucleus * getIODB() const { return mIODBNucleus; }

  //! Return the Fold object.
  Fold * getFold() const { return mLPBase.getFold(); }

  //! Clear the changed flag, we are starting a new block
  void clearChanged() { mChanged = false; }

private:
  void cleanupExprUseMap(NUExpr** pexpr);
  void populateExprUseMap(NUExpr** pexpr);

  //! Hide copy and assign constructors.
  LocalPropagationCallback(const LocalPropagationCallback&);
  LocalPropagationCallback& operator=(const LocalPropagationCallback&);

  // Types and data for a stack of propagation contexts.
  typedef UtHashMap<NUNet*, NUExpr*> NetValues;
  typedef LoopMap<NetValues> NetValuesLoop;
  typedef UtVector<NetValues*> NetValuesVector;
  typedef Loop<NetValuesVector> NetValuesVectorLoop;
  typedef RLoop<NetValuesVector> NetValuesVectorRLoop;
  NetValuesVector* mNetValues;

  // Types and data for a stack of nestings. Each nesting contains a
  // vector of constant caches. Each cache represents a different
  // leg of the nested construct. For example for if statements
  // there are two contexts, one for the then and one for the else.
  typedef UtVector<NetValuesVector*> NestedNetValues;
  NestedNetValues * mNestedNetValues;

  //! Note entry into a nested context.
  void pushNesting();

  //! Note exit from a nested context.
  /*!
    If the preserve_non_statics flag is false, do not promote
    non-static nets to the parent context.
   */
  void popNesting(bool preserve_non_statics=true);

  //! Clear the propagation cache for all defs in the given statement.
  void applyNonPropagatedDefs(NUStmt* stmt);

  //! Get the local propagation statistics
  LocalPropagationStatistics* getStatistics() const;

  //! Flag that indicates whether the current block was optimized.
  bool mChanged;

  //! Base class with common routines 
  LocalPropagationBase mLPBase;

  //! IODB.
  IODBNucleus* mIODBNucleus;

}; // class LocalPropagationCallback : public NUDesignCallback


//! Class to assist in creating a dependency graph of continuous drivers.
class LocalPropagationBuilder
{
public:
  typedef GenericDigraph<void*,NUUseDefNode*> ContinuousDriverGraph;

  static void getDepthOrder(NUModule* module, NUUseDefVector * depth_order);

private:
  static ContinuousDriverGraph * buildContinuousDriverGraph(NUModule * module);
};

#endif
