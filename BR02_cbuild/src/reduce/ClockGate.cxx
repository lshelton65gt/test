// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implements a transformation to reduce the number of clocks in the design.
*/

#include "bdd/BDD.h"

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAssign.h"

#include "flow/FLFactory.h"
#include "flow/FLNode.h"
#include "flow/FlowClassifier.h"

#include "exprsynth/Expr.h"
#include "exprsynth/ExprFactory.h"
#include "exprsynth/ESPopulateExpr.h"
#include "exprsynth/ExprResynth.h"

#include "localflow/UD.h"

#include "reduce/RewriteUtil.h"
#include "reduce/ClockGate.h"
#include "reduce/Fold.h"

#include "ClockExclusivity.h"


REClockGate::REClockGate(FLNodeFactory* flowFactory,
                         NUNetRefFactory* netRefFactory, AtomicCache* strCache,
                         MsgContext* msgContext, IODBNucleus* iodb,
                         ArgProc* args, UD* ud, REClockGateStats* stats,
                         bool verbose) :
  mNetRefFactory(netRefFactory), mUD(ud), mStats(stats), mVerbose(verbose)
{
  mClockExclusivity = new REClockExclusivity(flowFactory, mNetRefFactory,
                                             strCache, msgContext, iodb, args);
  mFold = new Fold(netRefFactory, args, msgContext, strCache, iodb, false,
                   eFoldUDKiller);
}

REClockGate::~REClockGate()
{
  delete mClockExclusivity;
  delete mFold;
}

void REClockGate::module(NUModule* mod)
{
  // Init the exclusivity class and find all the flops in the design
  FLNodeVector flops;
  mClockExclusivity->init(mod, &flops);

  // Walk all the flops looking for candiates for the transformation
  std::sort(flops.begin(), flops.end(), FLNodeCmp());
  transformFlops(flops);
}

void REClockGate::transformFlops(const FLNodeVector& flops)
{
  // Walk the flops analyzing their clocks
  NUUseDefSet covered;
  for (FLNodeVectorCLoop l(flops); !l.atEnd(); ++l) {
    // Count the number of flop blocks
    mStats->incrFlops();

    // Only process each use def once
    FLNode* flow = *l;
    NUUseDefNode* useDef = flow->getUseDefNode();
    if (covered.find(useDef) == covered.end()) {
      covered.insert(useDef);

      // Transform it if we find a gate clock where this flop is
      // exclusive with the enable device.
      ClockEdge newEdge = eClockPosedge;
      NUNetRefHdl newClkRef;
      NUExprVector enableExprs;
      if (isValidTransform(flow, &newEdge, &newClkRef, &enableExprs)) {
        transformFlop(flow, newClkRef, newEdge, enableExprs);

        // Must update UD so that flow can be recreated correctly
        NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
        mUD->structuredProc(always);
      }
    }
  }
} // void REClockGate::transformFlops

bool 
REClockGate::isValidTransform(FLNode* flow, ClockEdge* newEdge,
                              NUNetRefHdl* newClkRef, NUExprVector* enableExprs)
{
  // Get the clock expression for this flop if possible
  const NUExpr* flopClkExpr;
  FLNodeVector enables;
  NUNetRefHdl clkRef;
  flopClkExpr = mClockExclusivity->getDeviceEnableExpr(flow, &enables, &clkRef);
  if (flopClkExpr == NULL) {
    mStats->incrInvalidClk();
    printInvalidClock(flow, NULL, "failed expression creation");
    return false;
  }

  // If we didn't find any enables or a root clock in the clock tree,
  // then give up, we can't move the enable into the flop if there are
  // none.
  if (!clkRef.isValid()) {
    mStats->incrInvalidRootClk();
    printInvalidClock(flow, flopClkExpr, "missing root clock");
    return false;
  }
  if (enables.empty()) {
    mStats->incrInvalidEnables();
    printInvalidClock(flow, flopClkExpr, "missing enable");
    return false;
  }

  // Get the original clock net
  NUNetRefHdl netRef;
  mClockExclusivity->getDeviceEnable(flow, &netRef);

  // If the gated clock expression is not exclusive with all the
  // enable device clocks, then give up.
  for (FLNodeVectorCLoop l(enables); !l.atEnd(); ++l) {
    // Get the latches' actual enable and test it's exclusivity with
    // the flop clock expression.
    FLNode* enFlow = *l;
    const NUExpr* latEnable = mClockExclusivity->getDeviceEnable(enFlow, NULL);
    if (!mClockExclusivity->exclusiveExprs(flopClkExpr, latEnable)) {
      // The raw enable is not exclusive, try going back to its
      // clock tree leafs
      const NUExpr* latEnableExpr;
      latEnableExpr = mClockExclusivity->getDeviceEnableExpr(enFlow, NULL, NULL);
      if ((latEnableExpr != NULL) && 
          ((latEnable == latEnableExpr) ||
           !mClockExclusivity->exclusiveExprs(flopClkExpr, latEnableExpr))) {
        // This is not exclusive, then give up
        mStats->incrNonExcl();
        printNonExcl(flow, flopClkExpr, enFlow, latEnable);
        if (latEnable != latEnableExpr) {
          printNonExcl(flow, flopClkExpr, enFlow, latEnableExpr);
        }
        return false;
      }
    }
  }

  // Return the new edge and that we succesfully found a valid
  // transformation. We want the only value of the root clock that
  // would cause the flop to load. Note tht this routine may fail
  // because the logic is too complex to find the right edge. This
  // happens when the enable clock expression is always 0. See
  // test/gatedclks/clkgate12.v and test/gatedclks/clkgate13.v
  ClockEdge edge;
  if (!findNewClockEdge(flopClkExpr, clkRef, &edge)) {
    mStats->incrInvalidClkEdge();
    printInvalidClock(flow, flopClkExpr, "New clock edge indistinguishable");
    return false;
  }

  // Create the enable expression for the downstream flop.
  createFlopEnableExpr(flopClkExpr, clkRef, edge, enables, enableExprs);
  // Return the information
  *newEdge = edge;
  *newClkRef = clkRef;
  return true;
} // REClockGate::isValidTransform

class ReplaceExpr : public NuToNuFn
{
public:
  //! constructor
  ReplaceExpr(const NUExpr* origExpr, NUExpr* newExpr) :
    mOrigExpr(origExpr), mNewExpr(newExpr)
  {}

  //! Overload the expr to expr call back to do the work
  NUExpr* operator()(NUExpr* expr, Phase phase)
  {
    if ((phase == ePost) && (*expr == *mOrigExpr)) {
      delete expr;
      CopyContext cntxt(0,0);
      return mNewExpr->copy(cntxt);
    } else {
      return NULL;
    }
  }

private:
  const NUExpr* mOrigExpr;
  NUExpr* mNewExpr;
}; // class ReplaceExpr : public NuToNuFn

NUExpr*
REClockGate::replaceClockWithConst(const NUExpr* expr, const NUNetRefHdl& clkRef,
                                   UInt64 clkVal)
{
  // Create an expression to match the net ref and the constant for
  // the clock
  const SourceLocator& loc = expr->getLoc();
  NUExpr* clkExpr = createClkExpr(clkRef, loc);
  NUExpr* constExpr = NUConst::create(false, clkVal, 1, loc);

  // Replace the clock with the appropriate constant
  CopyContext cntxt(0, 0);
  NUExpr* newExpr = expr->copy(cntxt);
  ReplaceExpr translator(clkExpr, constExpr);
  newExpr = newExpr->translate(translator);
  delete clkExpr;
  delete constExpr;

  // Fold the expression and then clear the cache. We may delete some
  // of these expression so the fold expression cache can cause us to
  // not fold correctly.
  newExpr = mFold->fold(newExpr);
  return newExpr;
}

// Find under which value of the root clock the flop must be
// triggered. This is done by replacing the clock with a constant 1/0
// and testing the resulting expression. Whichever one results in a
// something other than constant 0, must be the right edge (1 =>
// posedge, 0 => negedge)
bool
REClockGate::findNewClockEdge(const NUExpr* clkExpr, const NUNetRefHdl& clkRef,
                              ClockEdge* edge)
{
  // Create the two expressions and figure out which is constant 0
  NUExpr* expr0 = replaceClockWithConst(clkExpr, clkRef, 0);
  NUExpr* expr1 = replaceClockWithConst(clkExpr, clkRef, 1);
  NUConst* constExpr0 = expr0->castConstNoXZ();
  NUConst* constExpr1 = expr1->castConstNoXZ();
  bool isZero0 = (constExpr0 != NULL) && constExpr0->isZero();
  bool isZero1 = (constExpr1 != NULL) && constExpr1->isZero();

  // Return the opposite of the edge of the one that is constant 0. If
  // we don't have exactly one expression with constant 0, give up.
  bool found = false;
  if (isZero0 && !isZero1) {
    *edge = eClockPosedge;
    found = true;
  } else if (isZero1 && !isZero0) {
    *edge = eClockNegedge;
    found = true;
  }

  // All done with the memory
  delete expr0;
  delete expr1;
  return found;
}

ClockEdge REClockGate::invert(ClockEdge edge)
{
  if (edge == eClockNegedge) {
    return eClockPosedge;
  } else if (edge == eClockPosedge) {
    return eClockNegedge;
  } else {
    INFO_ASSERT(false, "Expected clock edge: should be posedge or negedge");
    return eClockPosedge;
  }
}

void
REClockGate::transformFlop(FLNode* flow, const NUNetRefHdl& newClkRef,
                           ClockEdge newEdge, const NUExprVector& enExprs)
{
  // Get the always block for this flop
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  NU_ASSERT(always != NULL, useDef);
  NU_ASSERT(always->isSequential(), useDef);

  // Get the original clock net for printing
  NUNetRefHdl clkRef;
  mClockExclusivity->getDeviceEnable(flow, &clkRef);

  // Create an assignment for the clock if the newClkRef is not a
  // scalar
  NUNet* newClk = newClkRef->getNet();
  if (!newClk->isBitNet()) {
    // Need to create a bit net
    NUModule* module = newClk->getScope()->getModule();
    const SourceLocator& loc = newClk->getLoc();
    NUExpr* clkExpr = createClkExpr(newClkRef, loc);
    StringAtom* newName = module->gensym("$clk", NULL, newClk);
    newClk = module->createTempBitNet(newName, false, loc);
    NULvalue* lvalue = new NUIdentLvalue(newClk, loc);
    NUContAssign* assign = new NUContAssign(newName, lvalue, clkExpr, loc);
    module->addContAssign(assign);
  }

  // Create the new edge expression and replace the existing one
  NU_ASSERT(newClk->isBitNet(), newClk);
  NUEdgeExpr* oldEdgeExpr = always->getEdgeExpr();
  SourceLocator loc = oldEdgeExpr->getLoc();
  NUExpr* expr = new NUIdentRvalue(newClk, loc);
  expr->resize(1);
  NUEdgeExpr* edgeExpr = new NUEdgeExpr(newEdge, expr, loc);
  always->removeEdgeExpr(oldEdgeExpr);
  always->addEdgeExpr(edgeExpr);
  delete oldEdgeExpr;
  newClk->putIsEdgeTrigger(true);

  // Add the enable if statement in the block
  NUBlock* block = always->getBlock();
  NUStmtList stmts;
  for (NUStmtLoop l = block->loopStmts(); !l.atEnd(); ++l) {
    NUStmt* stmt = *l;
    stmts.push_back(stmt);
  }
  NUStmtList blockStmts;
  createIfStatements(enExprs, stmts, loc, &blockStmts);
  block->replaceStmtList(blockStmts);

  // Update the device enable data for this flop
  bool invert = (newEdge == eClockNegedge);
  mClockExclusivity->replaceDeviceEnable(flow, expr, newClkRef, invert);

  // Tell the user about it
  mStats->incrTransformedFlops();
  printTransformedFlop(flow, clkRef, newClk, enExprs);
} // REClockGate::transformFlop

void REClockGateStats::print(void) const
{
  int rejectedFlops = (mNumInvalidClk + mNumInvalidRootClks +
                       mNumInvalidEnables + mNumInvalidClkEdges +
                       mNumNonExcl + mNumConflEnables);
  if ((mNumTransformedFlops > 0) || (rejectedFlops > 0)) {
    UtIO::cout() << "Transformed " << mNumTransformedFlops
                 << " gated clock flops out of "
                 << mNumFlops << " flops in the design.\n"
                 << "Rejected " << rejectedFlops
                 << " flops due to:\n"
                 << "   invalid clock:         " << mNumInvalidClk << "\n"
                 << "   invalid root clock:    " << mNumInvalidRootClks << "\n"
                 << "   invalid enables:       " << mNumInvalidEnables << "\n"
                 << "   invalid clock edge:    " << mNumInvalidClkEdges << "\n"
                 << "   non-exclusive enables: " << mNumNonExcl << "\n"
                 << "   conflicting enables:   " << mNumConflEnables << "\n";
  }
}

void
REClockGate::printTransformedFlop(FLNode* flow, const NUNetRefHdl& clkRef,
                                  NUNet* newClk, const NUExprVector& enExprs) const
{
  // Only print the information if the user requested it
  if (!mVerbose) {
    return;
  }

  // Location of the always block
  flow->getUseDefNode()->getLoc().print();

  // Get a name for the flop net
  UtString buf1;
  flow->getDefNet()->composeUnelaboratedName(&buf1);

  // Print the transformed flop
  UtIO::cout() << ": transformed flop `" << buf1
               << "' to include enable(s):\n";

  // Get the names of the old and new clock
  UtString buf2;
  buf1.clear();
  clkRef->composeUnelaboratedName(&buf1);
  newClk->composeUnelaboratedName(&buf2);
  UtIO::cout() << "Gated clock: " << buf1 
               << ", new clock: " << buf2
               << ", enable expressions:\n";
  for (CLoop<const NUExprVector> l(enExprs); !l.atEnd(); ++l) {
    NUExpr* enExpr = *l;
    UtIO::cout() << "  ";
    enExpr->printVerilog(1);
  }
} // void REClockGate::printTransformedFlop

void
REClockGate::printNonExcl(FLNode* flopFlow, const NUExpr* clkExpr,
                          FLNode* enFlow, const NUExpr* enableClkExpr) const
{
  // Only print the information if the user requested it
  if (!mVerbose) {
    return;
  }

  // Print the start (common) of the failed flop transformation
  printFailedTransform(flopFlow);

  // Print the reason and the flops clock expression
  UtIO::cout() << "found non-exclusive enable device's clock.\n";
  UtIO::cout() << "Flop Clock Expr: ";
  clkExpr->printVerilog(0);

  // Print the enable device and its clock tree's expression.
  UtString buf;
  enFlow->getDefNet()->composeUnelaboratedName(&buf);
  UtIO::cout() << "\nEnable: " << buf;
  UtIO::cout() << "\nEnable Clock Expr: ";
  enableClkExpr->printVerilog(1);
}

void
REClockGate::printInvalidClock(FLNode* flopFlow, const NUExpr* flopClkExpr,
                               const char* reason) const
{
  // Only print the information if the user requested it
  if (!mVerbose) {
    return;
  }

  // Print the start (common) of the failed flop transformation
  printFailedTransform(flopFlow);

  // Print the reason and the flops clock expression
  UtIO::cout() << "found an invalid clock expression due to: " << reason << "\n";
  if (flopClkExpr != NULL) {
    UtIO::cout() << "Clock expression: ";
    flopClkExpr->printVerilog(1);
  } else {
    UtIO::cout() << "Invalid clock expression.\n";
  }
}

void REClockGate::printFailedTransform(FLNode* flopFlow) const
{
  // Print the location of the flop
  flopFlow->getUseDefNode()->getLoc().print();

  // Get a name for the flop net and print it
  UtString buf;
  flopFlow->getDefNet()->composeUnelaboratedName(&buf);
  UtIO::cout() << "(" << buf << ")";

  // Print the flops clock expression
  UtIO::cout() << ": Failed to transform flop because: ";
}

void
REClockGate::createFlopEnableExpr(const NUExpr* flopClkExpr,
                                  const NUNetRefHdl& clkRef, ClockEdge edge,
                                  const FLNodeVector& enables,
                                  NUExprVector* enExprs)
{
  // make a copy of the expression for processing
  CopyContext cntxt(0,0);
  NUExpr* enExpr = flopClkExpr->copy(cntxt);

  // If it is possible, replace the latch output of the enable(s) with
  // the latch input. This is because we are moving it into the flop
  // which synchronizes the data anyway. So the latch doesn't do
  // anyting. And by removing the latch we have less logic.
  //
  // We do this currently under the simple scenario where there is one
  // and only one enable.
  for (FLNodeVectorCLoop l(enables); !l.atEnd(); ++l) {
    FLNode* enable = *l;
    const NUExpr* latchDataExpr;
    const NUExpr* latchOut;
    if (mClockExclusivity->getLatchDataExpr(enable, &latchDataExpr, &latchOut)) {
      // Replace the latch output with the latch input
      CopyContext cntxt(0, 0);
      NUExpr* out = latchOut->copy(cntxt);
      NUExpr* data = latchDataExpr->copy(cntxt);
      NUExprExprDeepHashMap replacements;
      replacements.insert(NUExprExprDeepHashMap::value_type(out, data));
      REReplaceDeepExprExpr translator(replacements);
      enExpr = enExpr->translate(translator);

      // The replacements are done with copies so clean up the
      // expressions
      delete out;
      delete data;
    }
  }

  // Break up the enable expression into a vector of enable
  // expressions. This vector is an ordered list of enables. Each
  // entry needs a separate if statement created for it.
  //
  // This is done because the gated clock expression can walk through
  // multiple nested flops and creating nested if statements results
  // in the best control extract behavior.
  //
  // Note that we replace any clock expressions with a constant
  UInt64 clkVal = (edge == eClockNegedge) ? 0 : 1;
  createEnableExprs(enExpr, clkRef, clkVal, enExprs);

  // All done with the expression copy. Copies of the sub expressions
  // were put in enExprs.
  delete enExpr;
} // REClockGate::createFlopEnableExpr

NUExpr*
REClockGate::createClkExpr(const NUNetRefHdl& clkRef, const SourceLocator& loc)
{
  NUNet* clkNet = clkRef->getNet();
  NUExpr* clkExpr = NULL;
  if (clkNet->isBitNet()) {
    clkExpr = new NUIdentRvalue(clkNet, loc);
  } else {
    NU_ASSERT(clkRef->getNumBits() == 1, clkNet);
    ConstantRange range;
    bool valid = clkRef->getRange(range);
    NU_ASSERT(valid, clkNet);
    clkExpr = new NUVarselRvalue(clkNet, range, loc);
  }
  clkExpr->resize(1);
  return clkExpr;
}

void
REClockGate::createEnableExprs(const NUExpr* clkExpr,
                               const NUNetRefHdl& clkRef, UInt64 clkVal, 
                               NUExprVector* enExprs)
{
  // Recursively create enable expressions as long as we have an AND
  // operation. The vector is populated by depth.
  EnableExprMap enableExprMap;
  createEnableExprsRecurse(clkExpr, &enableExprMap, 0);

  // Walk the map and create enable expressions in depth order
  CopyContext cntxt(0,0);
  for (LoopMap<EnableExprMap> l(enableExprMap); !l.atEnd(); ++l) {
    const NUCExprVector& enables = l.getValue();
    for (CLoop<NUCExprVector> e(enables); !e.atEnd(); ++e) {
      // Make copy of the expression so we can alter it
      const NUExpr* origEnExpr = *e;
      NUExpr* enExpr = origEnExpr->copy(cntxt);

      // Remove the clock if it exists
      if (enExpr->queryUses(clkRef, &NUNetRef::overlapsSameNet, mNetRefFactory)) {
        NUExpr* newExpr = replaceClockWithConst(enExpr, clkRef, clkVal);
        if (newExpr != enExpr) {
          delete enExpr;
          enExpr = newExpr;
        }
      }


      // Check if this is a constant 1. This can happen for the clock
      // condition or an enable that is always on. If it is, just
      // remove it from the list of enables.
      //
      // It is possible that the enable is constant 0. In that case
      // the flop should be optimized away. But rather than do it
      // here, lets let local constant propagation do it. So keep the
      // constant 0 enable.
      NUConst* constExpr = enExpr->castConst();
      if ((constExpr != NULL) && constExpr->isOnes()) {
        delete enExpr;
      } else {
        enExprs->push_back(enExpr);
      }
    }
  }
}

void
REClockGate::createEnableExprsRecurse(const NUExpr* expr, 
                                      EnableExprMap* enableExprMap,
                                      int depth)
{
  // As long as this is an AND gate, keep recursing. Note that we
  // expect to only have AND's because the expression was created with
  // expression synthesis and then folded. In the future we may want
  // to use BDDs to guarantee that we find the most AND conditions
  // possible to improve run-time performance.
  const NUBinaryOp* binOp = dynamic_cast<const NUBinaryOp*>(expr);
  if ((binOp != NULL) && (binOp->getOp() == NUOp::eBiBitAnd)) {
    // Recurse down the two legs
    const NUExpr* expr0 = binOp->getArg(0);
    const NUExpr* expr1 = binOp->getArg(1);
    createEnableExprsRecurse(expr0, enableExprMap, depth + 1);
    createEnableExprsRecurse(expr1, enableExprMap, depth + 1);

  } else {
    // Add this to the map
    (*enableExprMap)[depth].push_back(expr);
  }
}

void
REClockGate::createIfStatements(const NUExprVector& enExprs,
                                const NUStmtList& stmts,
                                const SourceLocator& loc,
                                NUStmtList* blockStmts)
{
  // Loop over the enables in order and create if statements. We want
  // the last enable to be the outermost if statement.
  NUStmtList thenStmts(stmts);
  NUStmtList elseStmts;
  NUStmt* stmt = NULL;
  for (CLoop<const NUExprVector> l(enExprs); !l.atEnd(); ++l) {
    // Create this if
    NUExpr* enExpr = *l;
    stmt = new NUIf(enExpr, thenStmts, elseStmts, mNetRefFactory, false, loc);

    // Add the if statement to the thenStmts in case there are more
    // enable expressions.
    thenStmts.clear();
    thenStmts.push_back(stmt);
  }

  // The thenStmts will contain the final list of statements. Note
  // that we could have no enable expressions at all. This occurs when
  // the RHS of the latch is a constant 1. The code that creates the
  // enable vectors prunes out constant 1 enables.
  *blockStmts = thenStmts;
} // REClockGate::createIfStatements

