// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/Split.h"
#include "reduce/SplitFlow.h"
#include "reduce/DeadNets.h"

#include "localflow/UpdateUD.h"
#include "localflow/TFRewrite.h"

#include "nucleus/NUDesign.h"
#include "nucleus/NUTFWalker.h"
#include "nucleus/NUAlwaysWalker.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUAssign.h"

#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "flow/FLNodeElab.h"
#include "flow/FLFactory.h"

#include "iodb/IODBNucleus.h"

#include "util/SetOps.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/ArgProc.h"

#include "compiler_driver/CarbonContext.h" // needed for CRYPT macro
#include "util/CbuildMsgContext.h"

Split::Split(STSymbolTable *symtab,
             AtomicCache * str_cache,
             SourceLocatorFactory *loc_factory,
             FLNodeFactory * flow_factory,
             FLNodeElabFactory * flow_elab_factory,
             NUNetRefFactory *netref_factory,
             MsgContext * msg_ctx,
             IODBNucleus * iodb,
             ArgProc *args) :
  mSymtab(symtab),
  mStrCache(str_cache),
  mLocFactory(loc_factory),
  mFlowFactory(flow_factory),
  mFlowElabFactory(flow_elab_factory),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mArgs(args),
  mMultiLevelTasks(false)
{}


Split::Status
Split::blocks ( NUDesign * design,
                ClockAndDriver * clock_and_driver,
                BlockToSplitInstructionsMap & block_map, 
                FLNodeConstElabMap & flow_to_elab,
                FLNodeElabSet * split_elab_flow,
                FLNodeElabSet * deleted_elab_flow,
                bool allowTaskInlining,                
                bool verbose,
                Stats* stats)
{
  if (block_map.empty()) {
    return eSplitNone;
  }
  
  // We have to compute this after construction because parsing hasn't
  // happened at construction time.
  mMultiLevelTasks = mArgs->getBoolValue( CRYPT("-multiLevelHierTasks") );

  // Status for all the splits
  Status splitStatus = eSplitNone;

  // Augment the boundary with blocks which are unreachable through an
  // elaborated walk.
  discoverFullBoundary(block_map, flow_to_elab);

  // The set of original always blocks that have been split.
  NUAlwaysBlockSet split_blocks;

  // The set of replacement always blocks.
  NUAlwaysBlockSet split_replacements;

  for ( BlockToSplitInstructionsMap::iterator iter = block_map.begin() ;
        iter != block_map.end() ;
        ++iter ) {
    SplitStatus status = eSuccess;

    NUUseDefNode   * use_def      = iter->first;
    SplitInstructions * instructions = iter->second;
    NU_ASSERT2(use_def == instructions->getUseDef(), use_def, instructions->getUseDef());

    // Check that the block does not contain constructs which prevent splitting.
    status = checkUseDef(use_def, allowTaskInlining);

    if (status==eSuccess) {
      // Make sure that the groups all come from the same pieces of hierarchy.
      status = checkPartitionHierarchy(instructions, flow_to_elab);
    }

    if (status==eSuccess) {
      status = one_block(instructions, &split_blocks, &split_replacements);
      if ( status==eSuccess ) {
        // If we successfully split this block, add all its elab flows
        // into the success set.
        rememberSplitElabFlow(instructions, flow_to_elab, split_elab_flow);
        splitStatus = eSplitChanged;
      }
    }

    if ( verbose ) {
      instructions->print(status);
    }
  }

  if ( splitStatus == eSplitChanged ) {
    // If anything changed, update UD and Flow.
    repair(design, 
           clock_and_driver,
           &split_blocks,
           &split_replacements,
           block_map,
           flow_to_elab,
           deleted_elab_flow,
           stats);

    // Delete the elaborations for any temporary nets contained in the
    // (about to be deleted) split blocks.
    deleteElaboratedNets(&split_blocks);
    
    // Delete the blocks which have been successfully split. This
    // happens after the repair because flow may have ptrs to these
    // blocks/nets which will be purged by a dead code/flow pass.
    destroySplitBlocks(&split_blocks);

    // We may have deleted flow that was split. Cleanup.
    set_subtract(*deleted_elab_flow, split_elab_flow);
  }

  return splitStatus;
} // Split::blocks


void Split::discoverFullBoundary(BlockToSplitInstructionsMap & block_map,
                                 FLNodeConstElabMap & flow_to_elab)
{
  FLNodeSet connectivity_flow;  // Set of added flow across all instructions.
  for ( FLNodeFactory::iterator iter = mFlowFactory->begin();
	iter != mFlowFactory->end();
	++iter ) {
    FLNode * flow = (*iter);
    if (dynamic_cast<FLNodeBound*>(flow)) {
      continue;
    }

    NUUseDefNode * use_def = flow->getUseDefNode();
    NU_ASSERT(use_def,flow->getUseDefNode());

    if (not use_def->isContDriver()) {
      continue;
    }

    NUNet * net = flow->getDefNet();
    // Block-local temps should not have continuous flow.
    // Unfortunately, we cannot add this assert until bug 6618 is
    // fixed (module-level temps are improperly marked block-local
    // non-static).
    // NU_ASSERT(not net->isTempBlockLocalNonStatic(),net);
    // For now, just disqualify block-local and non-static nets.
    if (net->isBlockLocal() and net->isNonStatic()) {
      continue;
    }

    BlockToSplitInstructionsMap::iterator location = block_map.find(use_def);
    if (location != block_map.end()) {
      SplitInstructions * instructions = location->second;
      if (not instructions->isPartitioned(flow)) {
        instructions->addConnectivityFlow(flow);
        connectivity_flow.insert(flow);
      }
    }
  }

  FLNodeElab * flow_elab;
  for ( FLNodeElabFactory::FlowLoop iter = mFlowElabFactory->loopFlows();
        iter(&flow_elab);) {
    FLNode * flow = flow_elab->getFLNode();
    if (connectivity_flow.find(flow) != connectivity_flow.end()) {
      flow_to_elab[flow].insert(flow_elab);
    }
  }
}


void Split::repair(NUDesign * design,
                   ClockAndDriver * clock_and_driver,
                   NUAlwaysBlockSet * split_blocks,
                   NUAlwaysBlockSet * split_replacements,
                   BlockToSplitInstructionsMap & block_map,
                   FLNodeConstElabMap & flow_to_elab,
                   FLNodeElabSet * deleted_elab_flow,
                   Stats* stats)
{
  FLNodeSet boundary_flow;
  NUModuleSet affected_modules;

  for (NUAlwaysBlockSet::iterator iter = split_blocks->begin();
       iter != split_blocks->end();
       ++iter) {
    NUAlwaysBlock * always = (*iter);

    // Remember the containing module for UD update.
    affected_modules.insert(always->findParentModule());

    // The boundary flow associated with the original block (we no
    // longer care about partitioning).
    SplitInstructions * instructions = block_map[ always ];
    instructions->getFlowFromAllGroups(&boundary_flow);
  }

  // Update UD before repairing the flow graphs, which depend on
  // accurate UD information.
  // 
  // TBD: Can we get away updating UD only on the modified blocks?
  //
  // TBD: If we _really_ want accurate UD for all modules, we should
  // process the affected modules using a bottom-up instantiation
  // order.
  UD ud ( mNetRefFactory, mMsgContext, mIODB, mArgs, false );
  for ( NUModuleSet::iterator moditer = affected_modules.begin() ;
        moditer != affected_modules.end() ;
        ++moditer ) {
    NUModule * mod = (*moditer);
    ud.module(mod);
  }

  SplitFlow splitFlow(mSymtab,
		      mStrCache,
		      mLocFactory,
		      mFlowFactory,
		      mFlowElabFactory,
		      mNetRefFactory,
		      mMsgContext,
		      mIODB,
		      mArgs);
  splitFlow.repair(design,
                   *split_replacements,
                   boundary_flow,
                   flow_to_elab,
                   deleted_elab_flow,
                   clock_and_driver,
                   true,
                   stats);
}


SplitStatus Split::checkUseDef(NUUseDefNode * use_def, bool allowTaskInlining)
{
  SplitStatus status = eSuccess;

  NUAlwaysBlock * always_block = dynamic_cast<NUAlwaysBlock*>(use_def);
  if (always_block) {
    if (always_block->isCModelCall()) {
      // We don't handle always blocks with c-model calls.
      status = eCModelAlways;
    } else if (always_block->getPriorityBlock() or
               always_block->getClockBlock()) {
      // disallow always blocks with clock/priority components.
      status = eAsyncReset;
    } else if (!allowTaskInlining and wouldRequireTaskInlining(always_block)) {
      // Disallow task inlining if the caller requested it
      status = eTaskInlining;
    } else {
      // Disallow blocks containing hierarchical task references.
      status = checkAlwaysForTaskHierRefs(always_block);
    }
  } else {
    // We do not handle constructs other than always blocks (ie. cont
    // assigns).
    status = eNonAlwaysBlock;
  }

  return status;
}


SplitStatus Split::checkAlwaysForTaskHierRefs(NUAlwaysBlock * always_block)
{
  SplitStatus status = eSuccess;
  if(not mMultiLevelTasks ) {
    // Do not disqualify because of tasks under -multiLevelHierTasks.
    // See bug 2916.
    NUHierRefTaskDiscoveryCallback callback;
    NUDesignWalker walker(callback,false);
    callback.setWalker(&walker);
    NUDesignCallback::Status walk_status = walker.structuredProc(always_block);
    if (walk_status != NUDesignCallback::eNormal) {
      status = eHierRefTask;
    }
  }
  return status;
}


SplitStatus Split::one_block(SplitInstructions * instructions,
                             NUAlwaysBlockSet * split_blocks,
                             NUAlwaysBlockSet * split_replacements)
{
  NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(instructions->getUseDef());
  NU_ASSERT(always,instructions->getUseDef());

  NUBlock * block = always->getBlock();
  NUModule * module = block->getParentModule();

  NUNetSet all_partition_nets;
  SplitStatus status = checkPartition(instructions, &all_partition_nets);
  if (status != eSuccess)
    return status;  // return early on failure, before copying the block

  // The splitting is broken up into two stages.  The first stage
  // identifies all of the statements we want to copy into each new
  // block.  The second stage uses that information to perform the
  // Nucleus transformations and flow fixups.  The first stage
  // operates on a copy of the block (it may modify it by inlining tasks).
  // This allows us to check properties of the final partition before
  // making any permanent modifications to the design.
  FlowSetToStmtGroupMap partition;

  // Set up to operate on a copy of the always block
  bool modified = false;
  CopyContext cc(module, mStrCache, true, "tmp");
  NUAlwaysBlock* always_copy = always->clone(cc, true);
  {
    UD ud ( mNetRefFactory, mMsgContext, mIODB, mArgs, false );
    ud.structuredProc(always_copy); // compute defs for copied block
  }

  // Stage 1: get the final partition -- Note: can modify block even on failure
  // This is a test partition on the copied always block.
  if (status == eSuccess)
  {
    instructions->setUseDef(always_copy);
    status = get_final_partition(instructions, all_partition_nets, &partition, &modified);
    instructions->setUseDef(always);
  }

  // Check the partition before proceeding.  If it is OK, then do the real partitioning
  // of the statements in the original always block.
  if (status == eSuccess) {
    status = checkFinalPartition(partition, module);
    if (status == eSuccess) {
      bool second_modified = false;
      partition.clear();
      status = get_final_partition(instructions, all_partition_nets, &partition, &second_modified);
      INFO_ASSERT(status == eSuccess, "Second partitioning failed after first succeeded");
      INFO_ASSERT(second_modified == modified, "First and second partitionings made different modifications");
    }
  }
  delete always_copy;
  
  // Stage 2: modify the Nucleus and flow
  if (status == eSuccess) {
    status = statement_partition(module, always, partition, split_blocks, split_replacements);
  }
  
  return status;
}

SplitStatus Split::get_final_partition(SplitInstructions * instructions,
                                       const NUNetSet& all_partition_nets,
                                       FlowSetToStmtGroupMap * partition,
                                       bool * modified)
{
  NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(instructions->getUseDef());
  NU_ASSERT(always,instructions->getUseDef());

  // Remove any tasks with non-local references. Rewriting will be
  // incorrect, otherwise.
  *modified |= inlineTasksWithNonLocals(always);
  
  NUNetSet all_block_nets;
  always->getDefs(&all_block_nets);
  
  BlockSplitSets   * block_splits = instructions->getGroups();
  for ( BlockSplitSets::iterator iter = block_splits->begin() ;
        iter != block_splits->end() ;
        ++iter ) {
    FLNodeOrderedSet * flow_set = (*iter);
    
    add_to_final_partition ( always, all_block_nets, all_partition_nets,
                             *flow_set, partition);
  }
  
  FLNodeOrderedSet * connectivity = instructions->getConnectivityGroup();
  if (not connectivity->empty()) {
    add_to_final_partition ( always, all_block_nets, all_partition_nets,
                             *connectivity, partition);
  }

  return eSuccess;
}

SplitStatus Split::checkFinalPartition(FlowSetToStmtGroupMap & partition,
                                       NUModule* module)
{
  // Check that the final partition does not duplicate any statement
  // that defs the OST net.  This could mean that we would end up executing
  // two copies of a statement with side-effects, which would be wrong.
  // For example, if the statement is a file read, the file position will
  // advance too quickly.
  NUNet* ostNet = module->getOutputSysTaskNetAsNUNet();
  NUNetRefHdl ostNetRef = mNetRefFactory->createNetRef(ostNet);
  NUStmtSet previousSideEffectStmts;
  for (FlowSetToStmtGroupMap::iterator iter = partition.begin();
       iter != partition.end();
       ++iter) {
    NUStmtList& stmts = iter->second;
    for (NUStmtLoop l(stmts); !l.atEnd(); ++l) {
      NUStmt* stmt = *l;
      if (stmt->queryBlockingDefs(ostNetRef, &NUNetRef::overlapsSameNet, mNetRefFactory))
      {
        // This statement has side-effects
        NUStmtSet::iterator pos = previousSideEffectStmts.find(stmt);
        if (pos != previousSideEffectStmts.end())
          return eDuplicateSideEffects; // return failure
        previousSideEffectStmts.insert(stmt);
      }
    }
  }
  return eSuccess;
}

SplitStatus Split::statement_partition(NUModule * module,
                                       NUAlwaysBlock * always,
                                       FlowSetToStmtGroupMap & partition,
                                       NUAlwaysBlockSet * split_blocks,
                                       NUAlwaysBlockSet * split_replacements)
{
  for (FlowSetToStmtGroupMap::iterator iter = partition.begin();
       iter != partition.end();
       ++iter) {
    FLNodeOrderedSet* flow_set = iter->first;
    NUStmtList& stmts = iter->second;

    one_group ( module, always, stmts, *flow_set, split_replacements );
  }

  split_blocks->insert(always);
  module->removeAlwaysBlock(always);

  return eSuccess;
}

//! Note: this routine populates the all_partition_nets set
SplitStatus Split::checkPartition(SplitInstructions * instructions,
                                  NUNetSet * all_partition_nets)
{
  // do any of the groups contain flow for local nets? if so, reject.
  // see Beacon PROC3.v test for local which holds state, appears to be
  // a split candidate -- splitting will not help for locals.
  
  // Collect all nets from all groups and check that they are
  // valid (ie. no block locals and no memories).
  BlockSplitSets   * block_splits = instructions->getGroups();
  for ( BlockSplitSets::iterator iter = block_splits->begin() ;
	iter != block_splits->end() ;
	++iter ) {
    FLNodeOrderedSet * flow_set = (*iter);
    getGroupNets(*flow_set, all_partition_nets);
  }
  SplitStatus status = checkPartitionNets(*all_partition_nets);

  if (status==eSuccess) {
    // The connectivity partition gets checked for validity, but its
    // nets are not added to all_partition_nets. This prevents
    // dependencies other partitions from depending on the connectivity
    // partition.
    FLNodeOrderedSet * connectivity = instructions->getConnectivityGroup();
    if (not connectivity->empty()) {
      NUNetSet dummy_partition_nets;
      getGroupNets(*connectivity, &dummy_partition_nets);
      status = checkPartitionNets(dummy_partition_nets);
    }
  }
  
  if (status==eSuccess) {
    // Check that the there are no unpartitioned memories.
    status = checkNonPartitionNets(instructions->getUseDef(),*all_partition_nets);
  }
  return status;
}

SplitStatus Split::checkNonPartitionNets(NUUseDefNode * node,
                                         const NUNetSet & all_partition_nets)
{
  NUNetRefSet defs(mNetRefFactory);
  node->getDefs(&defs);

  for (NUNetRefSet::iterator iter = defs.begin();
       iter != defs.end();
       ++iter) {
    NUNetRefHdl def_net_ref = (*iter);
    NUNet * def_net = def_net_ref->getNet();
    
    if (def_net->is2DAnything()) {
      // determine if there is a memory outside a partition.
      if (all_partition_nets.find(def_net) == all_partition_nets.end()) {
        return eNonPartitionNetInvalid;
      }
    }
  }
  return eSuccess;
}


void Split::add_to_final_partition ( NUAlwaysBlock * always,
                                     const NUNetSet & all_block_nets,
                                     const NUNetSet & all_partition_nets,
                                     FLNodeOrderedSet & flow_set,
                                     FlowSetToStmtGroupMap * partition )
{
  // nets in this group
  NUNetSet group_nets;
  getGroupNets(flow_set, &group_nets);

  NUBlock * block = always->getBlock();
  NUStmtLoop stmt_loop = block->loopStmts();
  NUStmtList stmts(stmt_loop.begin(), stmt_loop.end());

  // stmts with no group dependents
  NUStmtSet independent_stmts;

  isolate_dependencies ( all_block_nets,
                         all_partition_nets,
                         group_nets,
                         stmts, 
                         &independent_stmts,
                         always->isSequential() );

  // subtract the statements we have marked as independent
  // and add the resulting list as a new stmt list at the end
  // of the partition.
  NUStmtList& partition_stmts = (*partition)[&flow_set];
  subtract_independents(stmts, independent_stmts, &partition_stmts);
  NU_ASSERT(partition_stmts.size() > 0, always);
}

void Split::one_group ( NUModule * module,
                        NUAlwaysBlock * always,
                        const NUStmtList & group_stmts,
                        FLNodeOrderedSet & flow_set,
                        NUAlwaysBlockSet * split_replacements )
{
  // nets in this group
  NUNetSet group_nets;
  getGroupNets(flow_set, &group_nets);

  NUAlwaysBlock * new_always = create ( always, 
                                        group_nets, 
                                        group_stmts );
  split_replacements->insert(new_always);
  
  module->addAlwaysBlock(new_always);
  
  fixup_flow ( new_always, flow_set );
}

void Split::isolate_dependencies ( const NUNetSet & all_block_nets,
                                   const NUNetSet & all_partition_nets,
                                   const NUNetSet & group_nets,
                                   NUStmtList & stmts,
                                   NUStmtSet * independent_stmts,
                                   bool is_sequential)
{
  // This routine determines which statements we need to keep in order
  // to correctly compute the given group. It traverses the
  // statements in reverse order, keeping defs for the group until
  // it sees a kill, at which point it no longer needs to keep that
  // net. It also needs to keep defs for outside the group when it
  // encounters a use which does not see the continuous reaching def
  // from the other group.

  // Description of sets used by this method:
  // ----------------------------------------
  //
  // all_block_nets      - All defs in this group of statements. This
  //                       set is fixed - it doesn't change.
  // all_partition_nets  - Nets computed by all partitions. This set is fixed.
  // group_nets          - All nets that need to be computed by this group
  //                       of statements. Subset (not necessarily proper) 
  //                       of all_block_nets. This set is fixed.
  // initially_needed    - Possibly growing set of nets that need to be
  //                       computed by this group of statements. Starts
  //                       out as 'group_nets', and grows as new requirements
  //                       are found.
  // needed              - This set determines which statements are kept.
  //                       It is initiazed with 'initially_needed', and
  //                       grows/shrinks based on new uses/kills.
  // outside             - This is the set of nets that are computed 
  //                       outside this block. Some of these values
  //                       can be relied on in this block. Others
  //                       cannot, determined by the set of defs and
  //                       uses. This set is fixed during loop processing.
  // oustside_wrong      - This is the set of outside nets whose
  //                       values cannot be relied on to compute
  //                       the needed nets. This set is changing
  //                       on each pass through the loop.
  // outside_correct     - Complement of outside_wrong.
  // needed_by_this_stmt - This is the set of nets whose values are
  //                       needed to compute the defs of this
  //                       statement. Recomputed on each iteration
  //                       through the loop.

  NUStmtSet all_stmts;
  NUStmtSet dependent_stmts;

  // Nets in groups other than this one.
  NUNetSet outside;
  set_difference(all_partition_nets, group_nets, &outside);

  // Set of nets that are needed should we restart our dependency search.
  NUNetSet initially_needed;
  initially_needed = group_nets;

  // Pre-compute defs to add to the 'initially_needed' set. Statements
  // with more than one def (e.g. if statements) will not be split.
  // So if one def is needed, all defs are needed.
  NUNetSet accumDefs;
  for (NUStmtList::iterator iter = stmts.begin(); iter != stmts.end(); ++iter) {
    NUStmt* stmt = *iter;
    NUNetSet defs;
    stmt->getBlockingDefs(&defs);
    if (set_has_intersection(initially_needed, defs)) {
      accumDefs.insertSet(defs);
    }
  }

  // Defs accumulated by the above loop that
  // are in the 'outside' set will have to be recomputed by this group of
  // statements - the value computed outside cannot be relied upon.
  NUNetSet newNeeded;
  set_intersection(accumDefs, outside, &newNeeded);
  initially_needed.insertSet(newNeeded);

  // Guarantee that nets in 'newNeeded' are not mistaken as
  // correctly computed 'outside'.
  set_subtract(newNeeded, &outside);
  
  // Initially, we need only nets in our partition (plus those from
  // the 'outside' set computed above).As we encounter
  // kills, nets are removed from our need set. As we encounter uses,
  // nets are added.
  NUNetSet needed;
  needed = initially_needed;

  // Initially, nets in other groups expose the correct value to
  // this group. As soon as a def is encountered, prior uses read
  // an incorrect value. This set tracks the fact that the reaching
  // def will not be the used one.
  NUNetSet outside_wrong;

  for (NUStmtList::reverse_iterator iter = stmts.rbegin(), next;
       iter != stmts.rend();
       iter = next) {
    NUStmt * stmt = (*iter);
    next = iter;
    ++next;

    NUNetSet defs;
    NUNetSet kills;
    NUNetSet uses;

    stmt->getBlockingDefs(&defs);
    stmt->getBlockingUses(&uses);
    stmt->getBlockingKills(&kills);

    all_stmts.insert(stmt);
    
    bool restarted = false;

    if (set_has_intersection(needed, defs)) {
      // This statement has defs that we need. Mark it dependent.
      dependent_stmts.insert(stmt);
      
      // Killed nets are no longer needed by the group.
      set_subtract(kills, &needed);

      // Determine the new needs of this statement.
      NUNetSet needed_by_this_stmt;

      {
        NUNetSet all_needed_by_this_stmt;
        
        if (is_sequential) {
          // Sequential blocks need all uses from needed stmts.
          all_needed_by_this_stmt.insertSet(uses);
        } else {
          // Determine which outside defs will have the correct value
          NUNetSet outside_correct;
          set_difference(outside, outside_wrong, &outside_correct);
          
          // Make no assumptions about order of definition; every local
          // def is immediately considered incorrect.
          //
          // Note that this is pessemistic in that it considers all
          // defs, even ones that occur before uses as incorrect. This
          // is because we don't walk into statements. We think this
          // is ok, because we do dead code removal later anyway.
          set_subtract(defs, &outside_correct);

          // Add necessary uses.
          set_difference(uses, outside_correct, &all_needed_by_this_stmt);
        }
        
        // It is not necessary to include needs that are not defed in this block
        set_intersection(all_needed_by_this_stmt, all_block_nets, &needed_by_this_stmt);
      }

      // Any time we have a new need, make sure we include all defs of
      // that need. This is done so that subsequent local reads are
      // guaranteed to see the correct value. The way to ensure that
      // we get everything is to restart our analysis after updating
      // our initial needs.

      NUNetSet new_initial_needs;
      set_difference(needed_by_this_stmt, initially_needed, &new_initial_needs);
      
      if (not new_initial_needs.empty()) {
        // RESTART.
        restarted = true;
        next = stmts.rbegin();
        initially_needed.insertSet(new_initial_needs);
        needed = initially_needed;
        outside_wrong.clear();
      } else {
        // If we have no new needs, just remember these needs locally.
        needed.insertSet(needed_by_this_stmt);
      }
    }

    if (!restarted)
    {
      // Any def to a net in another group means that the
      // reachable value is not what is read.
      set_intersection(defs, outside, &outside_wrong);
    }

  }

  // Everything that has not been marked as dependent is an independent statement.
  set_difference(all_stmts,dependent_stmts,independent_stmts);
}

NUAlwaysBlock * Split::create ( NUAlwaysBlock * always,
                                const NUNetSet & group_nets,
                                const NUStmtList & group_stmts )
{

  NUBlock * block = always->getBlock();
  NUScope * parent_scope = block->getParentScope();
  NUBlock * new_block = new NUBlock( NULL,
                                     block->getParentScope(),
                                     mIODB,
                                     mNetRefFactory,
                                     block->getUsesCFNet(),
                                     block->getLoc() );

  StringAtom * new_block_name = parent_scope->newBlockName( mStrCache,
                                                            always->getLoc() );
  NUAlwaysBlock * new_always = new NUAlwaysBlock ( new_block_name,
                                                   new_block,
                                                   mNetRefFactory,
                                                   always->getLoc(), 
                                                   always->isWildCard(),
                                                   always->getAlwaysType());
  
  CopyContext copy_context(new_block, mStrCache);

  // duplicate the stmts -- other blocks may use parts
  NUStmtList stmtlist;
  deepCopyStmtList( group_stmts, &stmtlist, copy_context ); 

  // give our stmts to the block
  new_block->replaceStmtList(stmtlist);

  // Create temp nets and replace all defined nets not present in this group.
  tmpify_nets(block, new_block, group_nets);

  NUEdgeExprList expr_list;
  always->getEdgeExprList(&expr_list);
  for ( NUEdgeExprListIter expr_iter = expr_list.begin();
        expr_iter != expr_list.end();
        ++expr_iter ) {
    NUEdgeExpr * edge_expr = (*expr_iter);
    NUEdgeExpr * new_edge_expr = dynamic_cast<NUEdgeExpr*>(edge_expr->copy(copy_context));
    NU_ASSERT(new_edge_expr, edge_expr);
    new_always->addEdgeExpr(new_edge_expr);
  }
  return new_always;
}

// This function is responsible for tmpifying all nets that are
// not outputs for this split of the original block. This will
// include dead defs and as well as defs that belong to other
// splits.
//
// Note that isolate_dependencies has removed statements that don't
// def any of the group_nets. But it leaves behind some statements
// that def both nets in the group_nets and outside the
// group_nets. This is where tmping comes in.
//
// This process has to to do two things: convert all def'ed nets
// outside this split to a temp version and initialize the temp with
// any external contributors.
void Split::tmpify_nets( const NUBlock * old_block,
                         NUBlock * new_block,
                         const NUNetSet & group_nets)
{
  NUNetSet def_set;

  NUNetRefSet use_ref_set(mNetRefFactory);
  NUNetRefSet kill_ref_set(mNetRefFactory);

  const SourceLocator & loc = new_block->getLoc();

  // Recompute the UD for the new block so we can use it to compute
  // the external uses below. This is more accurate than the old
  // block's UD because any references of nets for statements that are
  // not longer in this block are treated as external. See below.
  //
  // We used to use the old block's ud with a new to old statement
  // map. But that failed for various reasons. Using UD::block() got
  // us the right answer since it was designed to do this type of
  // work.
  {
    UD ud ( mNetRefFactory, mMsgContext, mIODB, mArgs, false );
    ud.block(new_block); // compute defs for copied block
  }

  // Get all the defs for this split. This will be used to figure out
  // what to temp.
  for ( NUStmtLoop loop = new_block->loopStmts() ;
        !loop.atEnd();
        ++loop) {
    // Assumption: All non-blocking defs have gone away by now.
    NUStmt *stmt = (*loop);
    stmt->getBlockingDefs(&def_set);
  }

  // Get the external uses for the defs we care about. We do that from
  // the new block so that any statements that got pruned by
  // isolate_dependencies will be treated as external def's. That way
  // they show up when we call getBlocking uses on the new block.
  //
  // Also note that we used to get this from the individual
  // statements. But we were essentially replicating what UD already
  // knows how to do (deal with local kills etc). It is better to
  // recompute UD above and then use it here.
  for (Loop<const NUNetSet> l(group_nets); !l.atEnd(); ++l) {
    NUNet* net = *l;
    NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
    new_block->getBlockingUses(net_ref, &use_ref_set);
  }

  // Compute the set of nets we have to temp
  NUNetSet difference;
  set_difference ( def_set, group_nets, &difference );

  // Create replacements for all locally declared nets. If they are
  // unneeded, the dead code pass will eliminate them.
  NUNetList locals;
  old_block->getAllNets(&locals);
  difference.insert(locals.begin(),locals.end());

  // Tmpify all the nets and initialize them with the external uses
  // which is stored in use_ref_set.
  for ( NUNetSet::iterator iter = difference.begin() ;
        iter != difference.end() ;
        ++iter ) {
    NUNet * original = (*iter);
    if (original->isSysTaskNet() or original->isControlFlowNet()) {
      continue;   //  SystemTaskNets & ControlFlowNets don't need processing here
    }
    
    NUNet * replacement = createReplacement ( new_block, original );
    new_block->replace(original, replacement);

    if ( original->isNonStatic() ) {
      // Control-flow statements can cause non-statics to unexpectedly
      // end up in use sets when there isn't a visible killing def.
      // See bugs 3441 and 3345 for examples.
      continue;
    }

    // initialize the replacement to the incoming value of the original.
    NUNetRefHdl full_net_ref = mNetRefFactory->createNetRef(original);
    NUNetRefSet::iterator location = use_ref_set.find(full_net_ref,&NUNetRef::overlapsSameNet);
    if (location != use_ref_set.end()) {
      NUNetRefHdl use_net_ref = (*location);
      UtList<NULvalue*> lvalues;
      use_net_ref->createLvalues(mNetRefFactory, loc, &lvalues);
      for (UtList<NULvalue*>::iterator l = lvalues.begin(); l != lvalues.end(); ++l)
      {
        NULvalue* lhs = *l; // in terms of original net
        NUExpr* rhs = lhs->NURvalue(); 
        lhs->replaceDef(original,replacement); // switch to replacement net
        NUBlockingAssign * assign = new NUBlockingAssign(lhs,rhs,new_block->getUsesCFNet(),loc);
        new_block->addStmtStart(assign);
      }
    }
  }

  // Tmpifying nets makes the UD invalid. Clear it
  new_block->clearUseDef();
}

NUNet * Split::createReplacement ( NUBlock * block, NUNet * original )
{
  NUNet * replacement = NULL;

  if (original->isVectorNet()) {
    NUVectorNet * vector_net = dynamic_cast<NUVectorNet*>(original);
    NetFlags net_flags = eNoneNet;
    if ( vector_net->isInteger() ) {
      if (vector_net->isSigned())
        net_flags = NetRedeclare(net_flags, eDMIntegerNet|eSigned);
      else
        net_flags = NetRedeclare(net_flags, eDMIntegerNet); 
    }
    if ( vector_net->isTime() ) {
      net_flags = NetRedeclare(net_flags, eDMTimeNet);
    }
    StringAtom* sym = block->gensym("split", NULL, original);
    replacement = block->createTempVectorNet ( sym, *vector_net->getRange(),
					       vector_net->isSigned(), 
                                               original->getLoc(), net_flags,
                                               vector_net->getVectorNetFlags ());
  } else if (original->isBitNet()) {
    StringAtom* sym = block->gensym("split", NULL, original);
    replacement = block->createTempBitNet ( sym, original->isSigned(),
                                            original->getLoc());
  } else if (original->is2DAnything()) {
    // If this is a block local temp, we just want to create a block
    // local temp and not create a temp memory that references the
    // original memory net. That memory net is going to get deleted.
    // 
    // This special handling of memories keeps us from calling
    // NUScope::createTempNetFromImage.
    if (original->isTempBlockLocalNonStatic()) {
      NUMemoryNet* mem = original->getMemoryNet();
      StringAtom* sym = block->gensym("split", NULL, original);
      replacement = block->createTempMemoryNetFromImage(sym, mem,
                                                        mem->getFlags());
    } else {
      replacement = block->createTempNetFromImage(original, "split");
    }
  } else {
    NU_ASSERT("Original net has unknown type" == NULL, original);
  }

  NU_ASSERT(replacement,original);
  return replacement;
}


void Split::getGroupNets(const FLNodeOrderedSet & flow_set,
                         NUNetSet * net_set)
{
  for ( FLNodeOrderedSet::const_iterator iter = flow_set.begin() ;
        iter != flow_set.end() ;
        ++iter ) {
    FLNode * flow = (*iter);
    NUNet  * net  = flow->getDefNet();
    net_set->insert(net);
  }
}


SplitStatus Split::checkPartitionNets(const NUNetSet & net_set)
{
  for (NUNetSet::const_iterator iter = net_set.begin();
       iter != net_set.end();
       ++iter) {
    NUNet * net = (*iter);
    if (net->isBlockLocal() and net->isNonStatic()) {
      return ePartitionNetInvalid;
    } else if (net->isForcible()) {
      return eForcibleNetDefined;
    } else if (net->isControlFlowNet()) {
      return eControlFlowPresent;
    }
  }
  return eSuccess;
}


void Split::fixup_flow ( NUAlwaysBlock * new_always, 
                         FLNodeOrderedSet & flow_set )
{
  for ( FLNodeOrderedSet::const_iterator iter = flow_set.begin();
        iter != flow_set.end() ;
        ++iter ) {
    FLNode * flow = (*iter);
    flow->setUseDefNode ( new_always );
  }
}

void Split::subtract_independents ( NUStmtList & stmts, 
                                    NUStmtSet  & independent_stmts,
                                    NUStmtList * group_stmts )
{
  for ( NUStmtList::iterator iter = stmts.begin() ;
	iter != stmts.end() ;
	++iter ) {
    NUStmt * stmt = (*iter);
    if ( independent_stmts.find(stmt) == independent_stmts.end() ) {
      group_stmts->push_back ( stmt );
    }
  }
}

void Split::deleteElaboratedNets(NUAlwaysBlockSet * split_blocks)
{
  NUNetList net_list;
  for (NUAlwaysBlockSet::iterator iter = split_blocks->begin();
       iter != split_blocks->end();
       ++iter) {
    NUAlwaysBlock * always = (*iter);
    NUBlock * block = always->getBlock();
    block->getAllNets(&net_list);
  }

  // cleanup the elaborated nets.
  DeadNetCallback dead_nets(mSymtab);
  dead_nets(&net_list);
}


void Split::destroySplitBlocks(NUAlwaysBlockSet * split_blocks)
{
  for (NUAlwaysBlockSet::iterator iter = split_blocks->begin();
       iter != split_blocks->end();
       ++iter) {
    NUAlwaysBlock * always = (*iter);
    delete always;
  }
}


void Split::rememberSplitElabFlow(SplitInstructions * instructions,
                                  FLNodeConstElabMap & flow_to_elab,
                                  FLNodeElabSet * split_elab_flow)
{
  BlockSplitSets * block_splits = instructions->getGroups();
  for ( BlockSplitSets::iterator iter = block_splits->begin() ;
	iter != block_splits->end() ;
	++iter ) {
    FLNodeOrderedSet * flow_set = (*iter);
    rememberGroupElabFlow(*flow_set,
                        flow_to_elab,
                        split_elab_flow);
  }
#if 0
  // Don't add the connectivity elaborated flow to the split_elab_flow
  // set. The scheduler didn't give them to us -- don't give them
  // back!
  
  // If we do, the scheduler will improperly think that we created new
  // FLNodeElabs -- we haven't.

  FLNodeOrderedSet * connectivity = instructions->getConnectivityGroup();
  rememberGroupElabFlow(*connectivity,
                      flow_to_elab,
                      split_elab_flow);
#endif
}

void Split::rememberGroupElabFlow(FLNodeOrderedSet & flow_set,
                                FLNodeConstElabMap & flow_to_elab,
                                FLNodeElabSet * split_elab_flow)
{
  for ( FLNodeOrderedSet::iterator iter = flow_set.begin() ;
        iter != flow_set.end();
        ++iter ) {
    FLNode * flow = (*iter);
    FLNodeElabSet& elab_set = flow_to_elab[flow];
    split_elab_flow->insert( elab_set.begin(),elab_set.end() );
  }
}


SplitStatus Split::checkPartitionHierarchy(SplitInstructions * instructions,
                                           FLNodeConstElabMap & flow_to_elab)
{
  NUUseDefNode * use_def = instructions->getUseDef();
  NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(use_def);
  if (always->isSequential()) {
    // splitting can only change the liveness for outputs of
    // combinational nodes.
    return eSuccess;
  }

  BlockSplitSets * block_splits = instructions->getGroups();
  FLNodeOrderedSet * connectivity = instructions->getConnectivityGroup();

  // 1. Determine the hierarchy for each instance of the FLNodes in
  //    this partition.

  FlowToHierarchyMap flow_to_hierarchy;
  for ( BlockSplitSets::iterator iter = block_splits->begin() ;
        iter != block_splits->end() ;
        ++iter ) {
    FLNodeOrderedSet * flow_set = (*iter);
    getHierarchy(*flow_set, &flow_to_hierarchy, flow_to_elab);
  }
  getHierarchy(*connectivity, &flow_to_hierarchy, flow_to_elab);

  // 2. Collect this as the set of all hierarchies for all
  //    instantiations.

  HierarchySet representative_hierarchy;
  for (FlowToHierarchyMap::iterator iter = flow_to_hierarchy.begin();
       iter != flow_to_hierarchy.end();
       ++iter) {
    HierarchySet & one_flow_hierarchy = (*iter).second;
    representative_hierarchy.insert(one_flow_hierarchy.begin(),
                                    one_flow_hierarchy.end());
  }

  // 3. Determine which FLNodes are not completely instantiated.

  FLNodeSet incomplete_flow;
  // Check that this is consistent across all groups in the partition.
  for ( BlockSplitSets::iterator iter = block_splits->begin() ;
        iter != block_splits->end() ;
        ++iter ) {
    FLNodeOrderedSet * flow_set = (*iter);
    checkHierarchy(*flow_set, representative_hierarchy, 
                   flow_to_hierarchy, &incomplete_flow);
  }
  checkHierarchy(*connectivity, representative_hierarchy, 
                 flow_to_hierarchy, &incomplete_flow);

  // 4. If there are some,, determine if there is any cross-group
  //    flow (ie. will we create any new live FLNodeElab nodes as a
  //    consequence of these split instructions).
  bool consistent = true;
  if (not incomplete_flow.empty()) {
    // 4A. Determine the set of nets used internally by each group.
    // 4B. Are any of our incompletely elaborated flow used?
    for ( BlockSplitSets::iterator iter = block_splits->begin() ;
          consistent and iter != block_splits->end() ;
          ++iter ) {
      FLNodeOrderedSet * flow_set = (*iter);
      consistent = checkInternalDependencies(*flow_set,incomplete_flow);
    }
    if (consistent) {
      consistent = checkInternalDependencies(*connectivity,incomplete_flow);
    }
  }

  return (consistent ? eSuccess : eHierMismatch);
}


void Split::getHierarchy(FLNodeOrderedSet & flow_set,
                         FlowToHierarchyMap * flow_to_hierarchy,
                         FLNodeConstElabMap & flow_to_elab)
{
  for ( FLNodeOrderedSet::iterator iter = flow_set.begin() ;
        iter != flow_set.end();
        ++iter ) {
    FLNode * flow = (*iter);
    HierarchySet& hierarchy_set = (*flow_to_hierarchy)[flow];
    getHierarchy(flow_to_elab[flow], &hierarchy_set);
  }
}

void Split::getHierarchy(FLNodeElabSet & elab_set,
                         HierarchySet * hierarchy)
{
  for ( FLNodeElabSet::iterator elabiter = elab_set.begin();
        elabiter != elab_set.end() ;
        ++elabiter ) {
    FLNodeElab * flow_elab = (*elabiter);
    hierarchy->insert(flow_elab->getHier());
  }
}


void Split::checkHierarchy(FLNodeOrderedSet & flow_set,
                           HierarchySet & representative_hierarchy,
                           FlowToHierarchyMap & flow_to_hierarchy,
                           FLNodeSet * incomplete_flow)
{
  for ( FLNodeOrderedSet::iterator iter = flow_set.begin() ;
        iter != flow_set.end();
        ++iter ) {
    FLNode * flow = (*iter);
    HierarchySet & hierarchy = flow_to_hierarchy[flow];

    // pay attention to things elaborated at least once.
    if (hierarchy.empty()) continue;

    bool consistent = (representative_hierarchy == hierarchy);
    if (not consistent) {
      incomplete_flow->insert(flow);
    }
  }
}


void Split::computeInternalDependencies(FLNodeOrderedSet & flow_set,
                                        NUNetSet * internal_dependencies)
{
  FLNodeSet unordered_flow_set;
  unordered_flow_set.insert(flow_set.begin(),flow_set.end());
  
  FLSetIter iter(&unordered_flow_set, 
                 FLIterFlags::eAll,
                 FLIterFlags::eBound,
                 FLIterFlags::eBranchAtNestedBlock,
                 true,
                 FLIterFlags::eIterNodeOnce);
  for (; not iter.atEnd(); ++iter) {
    FLNode * flow = (*iter);
    NUNet * net = flow->getDefNet();
    internal_dependencies->insert(net);
    
    NUUseDefNode * use_def = flow->getUseDefNode();
    if (use_def) {
      if (use_def->isContDriver()) {
        if (unordered_flow_set.find(flow)==unordered_flow_set.end()) {
          iter.doNotExpand();
        }
      }
    }
  }
}


bool Split::checkInternalDependencies(FLNodeOrderedSet & flow_set,
                                      FLNodeSet & incomplete_flow)
{
  NUNetSet internal_dependencies;
  computeInternalDependencies(flow_set,&internal_dependencies);

  bool consistent = true;
  for (FLNodeSet::iterator incomplete_iter = incomplete_flow.begin();
       consistent and incomplete_iter != incomplete_flow.end();
       ++incomplete_iter) {
    FLNode * flow = (*incomplete_iter);
    NUNet * net = flow->getDefNet();
    // don't check things in this group.
    if (flow_set.find(flow)==flow_set.end()) {
      // stop as soon as we find one cross-dependency.
      consistent = (internal_dependencies.find(net)==internal_dependencies.end());
    }
  }
  return consistent;
}

//! A class that queries always block task enables to see if inlining is required
class InlineQueryCallback : public NUDesignCallback
{
public:
  InlineQueryCallback() : mMustInline(false) {}

  //! By default, walk through everything.
  Status operator() (Phase, NUBase *) { return eNormal; }

  //! When a task enable is encountered, check if it will be inlined
  Status operator() (Phase phase, NUTaskEnable *task_enable)
  {
    if ((phase == ePre) && !mMustInline && !task_enable->isHierRef()) {
      NUTFWalkerCallback callback;
      NUDesignWalker walker(callback, false);
      callback.setWalker(&walker);
      walker.tf(task_enable->getTask());
      mMustInline = callback.mMustInline ||
                    !callback.mNonLocalUses.empty() ||
                    !callback.mNonLocalDefs.empty();
    }
    return eNormal;
  }

public:
  bool mMustInline;
};

bool Split::wouldRequireTaskInlining(NUAlwaysBlock* always)
{
  InlineQueryCallback callback;
  NUDesignWalker walker(callback, false);
  walker.alwaysBlock(always);

  return callback.mMustInline;
}

bool Split::inlineTasksWithNonLocals(NUAlwaysBlock * always)
{
  // Don't inline for tasks under -multiLevelHierTasks See bug 2916.
  if (mMultiLevelTasks) {
    return false;
  }
 
  TFRewrite tf_rewrite(mNetRefFactory,
                       mMsgContext,
                       mStrCache,
                       false,
                       false,
                       false,
                       mIODB,
                       mArgs);
  bool did_work = tf_rewrite.alwaysBlock(always);
  if (did_work) {
    UD ud ( mNetRefFactory, mMsgContext, mIODB, mArgs, false );
    ud.structuredProc(always);
  }
  return did_work;
}


void Split::print(BlockToSplitInstructionsMap * block_map)
{
  for ( BlockToSplitInstructionsMap::iterator iter = block_map->begin() ;
	iter != block_map->end() ;
	++iter ) {
    SplitInstructions * instructions = iter->second;
    instructions->print(eUnknown);
  }
}

void Split::print(const NUNetSet & s)
{
  for (Loop<const NUNetSet> l(s); !l.atEnd(); ++l) {
    NUNet* net = *l;
    StringAtom* sa = net->getName();
    UtIO::cout() << "DEBUG: NET " << sa->str() << UtIO::endl;
  }
}

SplitInstructions::SplitInstructions(NUUseDefNode * use_def) :
  mUseDef(use_def)
{
}


SplitInstructions::~SplitInstructions()
{
  for (BlockSplitSets::iterator iter = mGroups.begin(); 
       iter != mGroups.end(); 
       ++iter) {
    FLNodeOrderedSet* group = (*iter);
    delete group;
  }
  mGroups.clear();
}


NUUseDefNode * SplitInstructions::getUseDef() const
{ 
  return mUseDef; 
}

void SplitInstructions::setUseDef(NUUseDefNode* useDef)
{
  mUseDef = useDef;
}

void SplitInstructions::addGroup(FLNodeOrderedSet * group) 
{
  mGroups.insert(group);
}


BlockSplitSets * SplitInstructions::getGroups() 
{ 
  return &mGroups; 
}


bool SplitInstructions::isPartitioned(FLNode * flow) const 
{
  bool partitioned = false;
  for (BlockSplitSets::const_iterator iter = mGroups.begin();
       (not partitioned) and (iter != mGroups.end());
       ++iter) {
    const FLNodeOrderedSet * flow_set = (*iter);
    FLNodeOrderedSet::const_iterator location = flow_set->find(flow);
    partitioned = (location != flow_set->end());
  }
  return partitioned;
}


void SplitInstructions::getFlowFromAllGroups(FLNodeSet * output) const
{
  for (BlockSplitSets::const_iterator iter = mGroups.begin();
       iter != mGroups.end();
       ++iter) {
    const FLNodeOrderedSet * flow_set = (*iter);
    output->insert(flow_set->begin(),flow_set->end());
  }
  output->insert(mConnectivityGroup.begin(),mConnectivityGroup.end());
}


void SplitInstructions::addConnectivityFlow(FLNode * flow) 
{
  FLN_ASSERT(not isPartitioned(flow), flow); // eventually remove this.
  NU_ASSERT2(flow->getUseDefNode() == mUseDef, flow->getUseDefNode(), mUseDef);
  mConnectivityGroup.insert(flow);
}


FLNodeOrderedSet * SplitInstructions::getConnectivityGroup() 
{ 
  return &mConnectivityGroup;
}


void SplitInstructions::print (SplitStatus status)
{
  const SourceLocator& loc = mUseDef->getLoc();
  UtIO::cout() << loc.getFile() << ":" << loc.getLine() << ": ";
  if (status==eSuccess) {
    UtIO::cout() << "Successfully split:";
  } else {
    UtIO::cout() << "Could not split: ";
    switch(status) {
    case eNonAlwaysBlock:         UtIO::cout() << "Split requested of non-always."; break;
    case eCModelAlways:           UtIO::cout() << "Split requested of CModel always"; break;
    case eHierMismatch:           UtIO::cout() << "Hierarchy mismatch across partitions"; break;
    case eNonPartitionNetInvalid: UtIO::cout() << "Invalid non-partitioned net."; break;
    case ePartitionNetInvalid:    UtIO::cout() << "Invalid partitioned net."; break;
    case eControlFlowPresent:     UtIO::cout() << "The block contains a $stop or $finish."; break;
    case eForcibleNetDefined:     UtIO::cout() << "The block defines a forcible net."; break;
    case eHierRefTask:            UtIO::cout() << "Hierarchical task reference."; break;
    case eAsyncReset:             UtIO::cout() << "Sequential block with async reset."; break;
    case eTaskInlining:           UtIO::cout() << "Splitting would require inlining a task."; break;
    case eDuplicateSideEffects:   UtIO::cout() << "Splitting would require duplicating a statement with side effects."; break;
    case eUnknown:                // fallthrough
    default:                      UtIO::cout() << "Unknown split failure."; break;
    }
  }
  UtIO::cout() << UtIO::endl;
  int index = 0;
  BlockSplitSets * groups = getGroups();
  for ( BlockSplitSets::iterator iter = groups->begin() ;
	iter != groups->end() ;
	++iter ) {
    FLNodeOrderedSet * flow_set = (*iter);
    UtIO::cout() << "    Group " << ++index << ":" << UtIO::endl;
    printGroup(flow_set);
  }
  
  FLNodeOrderedSet * connectivity = getConnectivityGroup();
  if (not connectivity->empty()) {
    UtIO::cout() << "    Group " << ++index << " (connectivity):" << UtIO::endl;
    printGroup(connectivity);
  }
}

void SplitInstructions::printGroup(FLNodeOrderedSet * group) const
{
  // since we're operating on a FLNodeOrderedSet, do we still need to convert to an NUNetOrderedSet?
  NUNetOrderedSet stable;
  for ( FLNodeOrderedSet::iterator iter = group->begin();
        iter != group->end() ;
        ++iter ) {
    FLNode * flow = (*iter);
    stable.insert(flow->getDefNet());
  }
  for ( NUNetOrderedSet::iterator iter = stable.begin() ;
        iter != stable.end();
        ++iter ) {
    NUNet* net = (*iter);
    UtString long_name;
    net->getNameLeaf()->compose(&long_name);
    
    UtIO::cout() << "        "
                 << long_name.c_str()
                 << (net->is2DAnything() ? " (memory) " : "")
                 << UtIO::endl;
  }
}


bool FLNodeSetCmp::operator()(const FLNodeOrderedSet* set1, const FLNodeOrderedSet * set2) const
{
  SInt32 size1 = set1->size();
  SInt32 size2 = set2->size();
  SInt32 cmp = size1-size2;
  // same size -- compare the set contents until there is a difference.
  for (FLNodeOrderedSet::const_iterator i1 = set1->begin(), i2 = set2->begin();
       cmp==0 and i1 != set1->end() and i2 != set2->end();
       ++i1, ++i2) {
    const FLNode * f1 = (*i1);
    const FLNode * f2 = (*i2);
    cmp = FLNode::compare(f1,f2);
  }
  return cmp < 0;
}
