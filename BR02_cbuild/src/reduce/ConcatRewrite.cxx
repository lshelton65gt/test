// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCase.h"

#include "localflow/UD.h"

#include "util/UtIOStream.h"
#include "util/DynBitVector.h"

#include "reduce/Fold.h"
#include "reduce/AllocAlias.h"
#include "reduce/ConcatRewrite.h"

#include "util/GenericDigraph.h"
#include "util/GraphBuilder.h"
#include "util/GraphSCC.h"

#if !pfICC && !defined(__COVERITY__)
// force instantiation
template class NUNetRefMultiMap<ConcatRewrite::AssignComponents*>;
#endif

template <>
ConcatRewrite::AssignComponents * NUNetRefMultiMap<ConcatRewrite::AssignComponents*>::nullT() const {
  return NULL;
}

template <>
void NUNetRefMultiMap<ConcatRewrite::AssignComponents*>::helperTPrint(ConcatRewrite::AssignComponents* const & v, int indent) const {
  v->getLhs()->print(true,indent);
  v->getRhs()->print(true,indent);
}


ConcatRewrite::ConcatRewrite(NUNetRefFactory *netref_factory,
			     MsgContext *msg_context,
			     AtomicCache *str_cache,
			     IODBNucleus *iodb,
                             ArgProc * args,
			     ConcatStatistics *stats,
                             AllocAlias* alias_query,
			     bool updateUD) :
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mStrCache(str_cache),
  mIODB(iodb),
  mArgs(args),
  mStats(stats),
  mUpdateUD(updateUD),
  mAliasQuery(alias_query)
{
  mFold = new Fold(mNetRefFactory, 
                   args, 
                   mMsgContext, 
                   mStrCache, 
                   mIODB,
                   false,
                   eFoldUDKiller);
  mScope = NULL;
}


ConcatRewrite::~ConcatRewrite()
{
  delete mFold;
}


void ConcatRewrite::module(NUModule *this_module)
{
  bool work_done = false;

  NU_ASSERT(mScope == NULL, this_module);
  mScope = this_module;

  work_done |= contAssigns(this_module);
  work_done |= structuredProcs(this_module);
  work_done |= tfs(this_module);

  NU_ASSERT(mScope == this_module, this_module);
  mScope = NULL;

  if (work_done) {
    mStats->module();
    if (mUpdateUD) {
      UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
      ud.module(this_module);
    }
  }
}


bool ConcatRewrite::tfs(NUModule *module)
{
  bool work_done = false;

  for (NUTaskLoop loop = module->loopTasks();
       not loop.atEnd();
       ++loop) {
    work_done |= blockScope(*loop);
  }

  return work_done;
}


bool ConcatRewrite::contAssigns(NUModule *module)
{
  bool work_done = false;

  NUContAssignList cont_assigns;

  for (NUModule::ContAssignLoop loop = module->loopContAssigns();
       not loop.atEnd();
       ++loop) {
    NUContAssign * assign = (*loop);
    NUContAssignList replacements;
    bool did_rewrite = rewriteContAssign(module, assign, replacements);
    if (did_rewrite) {
      cont_assigns.insert(cont_assigns.end(), replacements.begin(), replacements.end());
      delete assign;
      work_done = true;
    } else {
      cont_assigns.push_back(assign);
    }
  }

  if (work_done) {
    module->replaceContAssigns(cont_assigns);
  }

  return work_done;
}


NUExpr * ConcatRewrite::findRealRvalue(NUExpr * rhs)
{
  // if we have a simple wrapper concat, recurse through it until we
  // encounter a real expression.
  if (rhs->getType() == NUExpr::eNUConcatOp) {
    NUConcatOp * concat = dynamic_cast<NUConcatOp*>(rhs);
    if (concat->getRepeatCount()==1 and
	concat->getNumArgs()==1) {
      return findRealRvalue(concat->getArg(0));
    }
  }
  return rhs;
}


NULvalue * ConcatRewrite::findRealLvalue(NULvalue * lhs)
{
  if (lhs->getType() == eNUConcatLvalue) {
    NUConcatLvalue * concat = dynamic_cast<NUConcatLvalue*>(lhs);
    if (concat->getNumArgs()==1) {
      return findRealLvalue(concat->getArg(0));
    }
  }
  return lhs;
}


bool ConcatRewrite::rewriteContAssign(NUModule *module,
				      NUContAssign *assign,
				      NUContAssignList & replacements)
{
  AssignComponentsList new_parts_list;
  createNewParts(assign, false, new_parts_list);
  if (new_parts_list.empty()) {
    return false;
  }

  bool success = checkNewParts(new_parts_list);
  if (success) {
    createNewContAssigns(module, assign, new_parts_list, replacements);
    return true;
  } else {
    cleanupNewParts(new_parts_list);
    return false;
  }
}


#define ORDER_CONCAT_ASSIGNS 1
bool ConcatRewrite::rewriteAssign(NUAssign *assign, bool looping_context,
                                  NUStmtList &replacements)
{
  AssignComponentsList new_parts_list;
  bool needs_overlap_processing = createNewParts(assign, looping_context,
                                                 new_parts_list);
  if (new_parts_list.empty()) {
    return false;
  }

  bool success = checkNewParts(new_parts_list);
  if (success and (needs_overlap_processing)) {
    success = noNewPartsOverlap(new_parts_list);
#if ORDER_CONCAT_ASSIGNS
    if (not success) {
      // Order the new parts to preserve the incoming cyclic
      // dependencies. The new_parts_list is reordered in-place.
      success = orderNewParts(new_parts_list);
    }
#endif
  }
  if (success) {
    createNewAssigns(assign, new_parts_list, replacements);
    return true;
  } else {
    cleanupNewParts(new_parts_list);
    return false;
  }
}


class DiscoverComplexPartsels : public NUDesignCallback
{
public:
  DiscoverComplexPartsels() : NUDesignCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*base*/) { return eNormal; }
  Status operator()(Phase /*phase*/, NUVarselLvalue * lhs) {
    NULvalue * identifier = lhs->getLvalue();
    if (identifier->isWholeIdentifier() or
        identifier->getType()==eNUMemselLvalue) {
      return eNormal;
    } else {
      return eStop;
    }
  }
  Status operator()(Phase /*phase*/, NUVarselRvalue * rhs) {
    NUExpr * identifier = rhs->getIdentExpr();
    if (identifier->isWholeIdentifier() or
        identifier->getType()==NUExpr::eNUMemselRvalue) {
      return eNormal;
    } else {
      return eStop;
    }
  }
private:
};


bool ConcatRewrite::checkNewParts(AssignComponentsList & new_parts_list)
{
  for (AssignComponentsList::iterator iter = new_parts_list.begin();
       iter != new_parts_list.end();
       ++iter) {
    AssignComponents assign_parts = *iter;
    NULvalue *lhs = assign_parts.getLhs();
    NUExpr *rhs = assign_parts.getRhs();

    // return false if there any complex varselects on the rhs or lhs.

    DiscoverComplexPartsels callback;
    NUDesignWalker walker(callback,false);
    bool valid = (walker.lvalue(lhs)==NUDesignCallback::eNormal &&
                  walker.expr(rhs)==NUDesignCallback::eNormal);
    if (not valid) {
      return false;
    }
  }
  return true;
}


class ConcatPartGraphWalker : public GraphWalker
{
public:
  ConcatPartGraphWalker(ConcatRewrite::AssignComponentsList * parts_list) :
    mPartsList(parts_list) 
  {}

  ~ConcatPartGraphWalker() {}

  Command visitNodeAfter(Graph * graph, GraphNode * node) {
    ConcatRewrite::ConcatPartGraph * cpGraph = dynamic_cast<ConcatRewrite::ConcatPartGraph*>(graph);
    ConcatRewrite::ConcatPartGraph::Node * cpNode = cpGraph->castNode(node);
    ConcatRewrite::AssignComponents * assign_parts = cpNode->getData();
    mPartsList->push_back(*assign_parts);
    return GW_CONTINUE;
  }
  
  ConcatRewrite::AssignComponentsList * mPartsList;
};


bool ConcatRewrite::orderNewParts(AssignComponentsList & parts_list)
{
  GraphBuilder<ConcatPartGraph> builder;
  ConcatPartDefs def_to_assign(mNetRefFactory);

  // create a graph. return false if there are any cyclic components.

  for (AssignComponentsList::iterator iter = parts_list.begin();
       iter != parts_list.end();
       ++iter) {
    AssignComponents & assign_parts = *iter;
    NULvalue * lhs = assign_parts.getLhs();

    NUNetRefSet defs(mNetRefFactory);
    lhs->getDefs(&defs);

    builder.addNode(&assign_parts);

    for (NUNetRefSet::iterator riter = defs.begin();
         riter != defs.end();
         ++riter) {
      NUNetRefHdl net_ref = (*riter);
      for (NUNetRefRangeLoop loop = net_ref->loopRanges(mNetRefFactory);
           not loop.atEnd();
           ++loop) {
        NUNetRefHdl partial_net_ref = loop.getCurrentNetRef();
        def_to_assign.insert(partial_net_ref,&assign_parts);
      }
    }
  }

  NetRefOverlapsQuery query(mAliasQuery);
  for (AssignComponentsList::iterator iter = parts_list.begin();
       iter != parts_list.end();
       ++iter) {
    AssignComponents & using_assign_parts = *iter;
    NULvalue * lhs = using_assign_parts.getLhs();
    NUExpr   * rhs = using_assign_parts.getRhs();

    NUNetRefSet uses(mNetRefFactory);
    lhs->getUses(&uses);
    rhs->getUses(&uses);

    for (NUNetRefSet::iterator riter = uses.begin();
         riter != uses.end();
         ++riter) {
      NUNetRefHdl net_ref = (*riter);

      for (ConcatPartDefs::CondMapLoop loop = def_to_assign.loop(net_ref, &query);
           not loop.atEnd();
           ++loop) {
        AssignComponents * defining_assign_parts = (*loop).second;

        // Avoid adding self-edges.
        if (defining_assign_parts != &using_assign_parts) {
          // add edge from the defining assign to the using assign
          // because we want to schedule the def before the use.
          builder.addEdgeIfUnique(defining_assign_parts, &using_assign_parts, NULL);
        }
      }
    }
  }

  ConcatPartGraph * graph = builder.getGraph();

  GraphSCC scc;
  scc.compute(graph);
  if (scc.hasCycle()) {
    delete graph;
    return false;
  }

  AssignComponentsList replacement_parts;
  ConcatPartGraphWalker walker(&replacement_parts);
  walker.walk(graph);

  delete graph;

  parts_list = replacement_parts;

  // 1. record all defs (net_ref->node). create nodes.
  // 2. create edges based on uses.
  // 3. check for cycles using GraphSCC (see REHierRefCovered)
  // 4. if no cycles, lay-out with a dfs walk (see new motion).
  return true;
}

bool ConcatRewrite::noNewPartsOverlap(AssignComponentsList & new_parts_list)
{
  // temporary -- check for LHS/RHS overlap.
  NUNetRefSet defs(mNetRefFactory);
  NUNetRefSet uses(mNetRefFactory);

  for (AssignComponentsList::iterator iter = new_parts_list.begin();
       iter != new_parts_list.end();
       ++iter) {
    AssignComponents assign_parts = *iter;
    NULvalue *lhs = assign_parts.getLhs();
    NUExpr *rhs = assign_parts.getRhs();

    lhs->getDefs(&defs);
    rhs->getUses(&uses);
  }

  // Use the alloc alias class to test the net refs. This is N*N but
  // hopefully N is small because we are dealing with assignments. It
  // would be nice to make this N*log(N) at some point but it is hard
  // because before aliasing, we don't know what bits of what net are
  // connected.
  bool overlap = false;
  NetRefOverlapsQuery query(mAliasQuery);
  for (NUNetRefSet::NetRefSetLoop d = defs.loop(); !d.atEnd() && !overlap; ++d) {
    const NUNetRefHdl& defNetRef = *d;
    NUNet* defNet = defNetRef->getNet();
    for (NUNetRefSet::NetRefSetLoop u = uses.loop(); !u.atEnd() && !overlap; ++u) {
      // The NetRefOverlapsQuery interface is a little bit cumbersome
      // here, but using it so that we don't replicate the logic.
      const NUNetRefHdl& useNetRef = *u;
      if (query(defNet, *useNetRef) and query(*defNetRef, *useNetRef)) {
        overlap = true;
      }
    }
  }
  return not overlap;
}


void ConcatRewrite::createNewContAssigns(NUModule * module,
                                         NUContAssign * assign,
                                         AssignComponentsList & new_parts_list,
                                         NUContAssignList & replacements)
{
  for (AssignComponentsList::iterator iter = new_parts_list.begin();
       iter != new_parts_list.end();
       ++iter) {
    AssignComponents assign_parts = *iter;
    NULvalue *lhs = assign_parts.getLhs();
    NUExpr *rhs = assign_parts.getRhs();

    StringAtom *name = module->newBlockName(mStrCache, assign->getLoc());
    NUContAssign *new_assign = new NUContAssign(name,
						lhs,
						rhs,
						assign->getLoc(),
						assign->getStrength());
    replacements.push_back(new_assign);
  }

  mStats->contAssign();
}


void ConcatRewrite::createNewAssigns(NUAssign * assign, 
                                     AssignComponentsList & new_parts_list,
                                     NUStmtList & replacements)
{
  switch (assign->getType()) {
  case eNUBlockingAssign:    mStats->blockingAssign();    break;
  case eNUNonBlockingAssign: mStats->nonblockingAssign(); break;
  default:
    NU_ASSERT(0,assign);
    break;
  }

  for (AssignComponentsList::iterator iter = new_parts_list.begin();
       iter != new_parts_list.end();
       ++iter) {
    AssignComponents assign_parts = *iter;
    NULvalue *lhs = assign_parts.getLhs();
    NUExpr *rhs = assign_parts.getRhs();

    NUAssign *new_assign = 0;

    switch (assign_parts.getAssignType()) {
    case eNUBlockingAssign:
      new_assign = new NUBlockingAssign(lhs, rhs, assign->getUsesCFNet(), assign->getLoc());
      break;
    case eNUNonBlockingAssign:
      new_assign = new NUNonBlockingAssign(lhs, rhs, assign->getUsesCFNet(), assign->getLoc());
      break;
    default:
      NU_ASSERT(0,assign);
      break;
    }

    replacements.push_back(new_assign);
  }
}


void ConcatRewrite::cleanupNewParts(AssignComponentsList & new_parts_list)
{
  for (AssignComponentsList::iterator iter = new_parts_list.begin();
       iter != new_parts_list.end();
       ++iter) {
    AssignComponents assign_parts = *iter;
    NULvalue *lhs = assign_parts.getLhs();
    NUExpr *rhs = assign_parts.getRhs();
    delete lhs;
    delete rhs;
  }
  new_parts_list.clear();
}


bool ConcatRewrite::structuredProcs(NUModule *module)
{
  bool work_done = false;

  for (NUModule::InitialBlockLoop loop = module->loopInitialBlocks();
       not loop.atEnd();
       ++loop) {
    NUInitialBlock *initial = *loop;
    work_done |= structuredProc(initial);
  }

  for (NUModule::AlwaysBlockLoop loop = module->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock *always = *loop;
    work_done |= structuredProc(always);
  }

  return work_done;
}


bool ConcatRewrite::structuredProc(NUStructuredProc * proc)
{
  return blockScope(proc->getBlock());
}


bool ConcatRewrite::blockScope(NUBlockScope *block)
{
  bool work_done = false;

  NUScope* saveScope = mScope;
  mScope = block;
  NUStmtLoop loop = block->loopStmts();
  NUStmtList stmts(loop.begin(), loop.end());
  work_done = stmtList(stmts, false);

  if (work_done) {
    block->replaceStmtList(stmts);
  }
  mScope = saveScope;

  return work_done;
}


bool ConcatRewrite::stmtList(NUStmtList &stmts, bool looping_context)
{
  bool work_done = false;

  for (NUStmtList::iterator iter = stmts.begin();
       iter != stmts.end();
       /*no advance*/) {
    NUStmt * current_stmt = (*iter);
    NUStmtList replacements;
    work_done |= stmt(current_stmt, looping_context, replacements);
    if (not replacements.empty()) {
      stmts.insert(iter, replacements.begin(), replacements.end());
      iter = stmts.erase(iter);
      delete current_stmt;
    } else { 
      ++iter;
    }
  }

  return work_done;
}


bool ConcatRewrite::stmt(NUStmt *stmt, bool looping_context,
			 NUStmtList & replacements)
{
  bool work_done = false;

  switch (stmt->getType()) {
  case eNUBlockingAssign:
  case eNUNonBlockingAssign:
    work_done = rewriteAssign(dynamic_cast<NUAssign*>(stmt), looping_context,
                              replacements);
    break;

  case eNUBlock:
    work_done = blockScope(dynamic_cast<NUBlock*>(stmt));
    break;

  case eNUIf:
    work_done = ifStmt(dynamic_cast<NUIf*>(stmt));
    break;

  case eNUCase:
    work_done = caseStmt(dynamic_cast<NUCase*>(stmt));
    break;

  case eNUFor:
    work_done = forStmt(dynamic_cast<NUFor*>(stmt));
    break;

  default:
    break;
  }

  return work_done;
}


bool ConcatRewrite::ifStmt(NUIf *stmt)
{
  NUStmtLoop loop = stmt->loopThen();
  NUStmtList then_stmts(loop.begin(), loop.end());
  bool work_done_then = stmtList(then_stmts, false);
  if (work_done_then) {
    stmt->replaceThen(then_stmts);
  }

  loop = stmt->loopElse();
  NUStmtList else_stmts(loop.begin(), loop.end());
  bool work_done_else = stmtList(else_stmts, false);
  if (work_done_else) {
    stmt->replaceElse(else_stmts);
  }

  return (work_done_then or work_done_else);
}


bool ConcatRewrite::caseStmt(NUCase *stmt)
{
  bool work_done = false;

  for (NUCase::ItemLoop item_loop = stmt->loopItems();
       not item_loop.atEnd();
       ++item_loop) {
    NUCaseItem *item = *item_loop;
    NUStmtLoop stmt_loop = item->loopStmts();
    NUStmtList item_stmts(stmt_loop.begin(), stmt_loop.end());
    bool this_work_done = stmtList(item_stmts, false);
    work_done |= this_work_done;
    if (this_work_done) {
      item->replaceStmts(item_stmts);
    }
  }

  return work_done;
}


bool ConcatRewrite::forStmt(NUFor *stmt)
{
  NUStmtLoop loop = stmt->loopBody();
  NUStmtList stmts(loop.begin(), loop.end());
  bool work_done = stmtList(stmts, true);
  if (work_done) {
    stmt->replaceBody(stmts);
  }

  return work_done;
}


//! Discover if any variable-sized expressions exist. 
/*!
  Variable-sized expressions prevent concat rewriting because the
  sub-portions of the concat cannot be statically determined.
*/
class DiscoverVariableSizedExprs : public NUDesignCallback
{
public:
  DiscoverVariableSizedExprs() : NUDesignCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*base*/) { return eNormal; }
  Status operator()(Phase /*phase*/, NUExpr * expr) {
    if (expr->sizeVaries()) {
      return eStop;
    } else {
      return eNormal;
    }
  }
private:
};


bool ConcatRewrite::createNewParts(NUAssign *assign,
                                   bool looping_context,
                                   AssignComponentsList &new_parts_list)
{
  // Call Fold on top-level LHS concats to eliminate any concat nesting.
  CopyContext ctx(0,0);
  NULvalue * original_lvalue = assign->getLvalue();
  NULvalue * lvalue = mFold->fold(original_lvalue);
  if (lvalue != original_lvalue) {
    assign->replaceLvalue(lvalue,false);
  }
  
  if ( lvalue->getType() == eNUVarselLvalue ){
    NUVarselLvalue *varsel = dynamic_cast<NUVarselLvalue*>(lvalue);
    if ( varsel->sizeVaries() ) {
      return false;       // the size of the lhs may be a pessimistic value 
                          // (the full size of the net), so for now skip the 
                          // concat rewrite and wait for the indices to be
                          // resolved. test/vhdl/lang_misc/dyn_slice7 
    }
  }

  NUExpr   * rvalue = findRealRvalue(assign->getRvalue());

  DiscoverVariableSizedExprs callback;
  NUDesignWalker walker(callback,false);
  bool valid = (walker.assign(assign)==NUDesignCallback::eNormal);
  if (not valid) {
    return true;
  }

  NUType assign_type = assign->getType();

  if (lvalue->getType() == eNUConcatLvalue) {
    createNewPartsConcatLvalue(assign_type, lvalue, rvalue, new_parts_list);
    createNewPartsConcatRvalues(new_parts_list);
  } else if (rvalue->getType() == NUExpr::eNUConcatOp) {
    createNewPartsConcatRvalue(assign_type, lvalue, rvalue, new_parts_list);
  } else if (lvalue->getType() == eNUVarselLvalue) {
    // TBD: what about this? should this be handled somewhere else?
    if (assign_type != eNUContAssign) {
      createNewPartsVarselLvalue(assign_type, lvalue, rvalue, looping_context,
                                 new_parts_list);
      createNewPartsConcatLvalues(new_parts_list);
      createNewPartsConcatRvalues(new_parts_list);
      return false;
    }
  } else {
    // Nothing obvious to rewrite.
  }
  return true;
}


void ConcatRewrite::createNewPartsConcatLvalues(AssignComponentsList &new_parts_list)
{
  for (AssignComponentsList::iterator iter = new_parts_list.begin();
       iter != new_parts_list.end();
       /*no advance*/) {
    AssignComponents assign_parts = *iter;
    NULvalue *lhs = assign_parts.getLhs();
    NUExpr *rhs = assign_parts.getRhs();
    NUType assign_type = assign_parts.getAssignType();

    if (lhs->getType()==eNUConcatLvalue) {
      AssignComponentsList sub_parts_list;
      bool success = createNewPartsConcatLvalue(assign_type, lhs, rhs, sub_parts_list);
      if (success) {
        new_parts_list.insert(iter, sub_parts_list.begin(), sub_parts_list.end());
        delete lhs;
        delete rhs;
        iter = new_parts_list.erase(iter);
      } else {
        ++iter;
      }
    } else {
      ++iter;
    }
  }
}


bool ConcatRewrite::createNewPartsConcatLvalue(NUType assign_type, NULvalue * lhs, NUExpr * original_rhs, 
                                               AssignComponentsList &new_parts_list)
{
  // Do not subdivide memsels.
  if (original_rhs->getType() == NUExpr::eNUMemselRvalue) {
    return false;
  }

  NUConcatLvalue *lhs_concat = dynamic_cast<NUConcatLvalue*>(findRealLvalue(lhs));
  NU_ASSERT(lhs_concat,lhs);
  
  CopyContext ctx(0,0);
  NUExpr * rhs = original_rhs->copy(ctx);
  rhs = rhs->makeSizeExplicit();

  // cur_part tracks which absolute piece of the rhs we are currently after.
  ConstantRange cur_part(-1,-1);

  // Loop through all lhs parts, create new lhs, rhs pairs
  UInt32 nargs = lhs_concat->getNumArgs();
  for (SInt32 idx = nargs - 1; idx >= 0; --idx) {
    NULvalue *cur_lvalue = lhs_concat->getArg((UInt32)idx);
    NULvalue *new_lvalue = cur_lvalue->copy(ctx);

    UInt32 part_size = cur_lvalue->getBitSize();
    cur_part = ConstantRange(cur_part.getMsb() + part_size, cur_part.getMsb() + 1);

    NUExpr * new_rvalue = mFold->createPartsel (rhs, cur_part, rhs->getLoc());

    AssignComponents assign_parts(new_lvalue, new_rvalue, assign_type);
    new_parts_list.push_back(assign_parts);
  }

  delete rhs;
  return true;
}


void ConcatRewrite::createNewPartsConcatRvalues(AssignComponentsList &new_parts_list)
{
  for (AssignComponentsList::iterator iter = new_parts_list.begin();
       iter != new_parts_list.end();
       /*no advance*/) {
    AssignComponents assign_parts = *iter;
    NULvalue *lhs = assign_parts.getLhs();
    NUExpr *rhs = assign_parts.getRhs();
    NUType assign_type = assign_parts.getAssignType();

    if (rhs->getType()==NUExpr::eNUConcatOp) {
      AssignComponentsList sub_parts_list;
      bool success = createNewPartsConcatRvalue(assign_type, lhs, rhs, sub_parts_list);
      if (success) {
        new_parts_list.insert(iter, sub_parts_list.begin(), sub_parts_list.end());
        delete lhs;
        delete rhs;
        iter = new_parts_list.erase(iter);
      } else {
        ++iter;
      }
    } else {
      ++iter;
    }
  }
}


bool ConcatRewrite::createNewPartsConcatRvalue(NUType assign_type, NULvalue * lhs, NUExpr * rhs,
                                               AssignComponentsList &new_parts_list)
{
  // Do not subdivide memsels.
  if (lhs->getType() == eNUMemselLvalue) {
    return false;
  }

  NUConcatOp *rhs_concat = dynamic_cast<NUConcatOp*>(findRealRvalue(rhs));

  NU_ASSERT(rhs_concat,rhs);
#if 0
  // Fold is unable to eliminate the occasional single-element concat.
  // For this reason, the real rvalue may not be a concat.
  if (not rhs_concat) {
    return false;
  }
#endif

  CopyContext ctx(0,0);

  UInt32 nargs  = rhs_concat->getNumArgs();
  SInt32 nrepeats = rhs_concat->getRepeatCount();

  // Do not rewrite concats of the form {N{x}} where 'x' is a
  // single-bit expression. Fold does a better job at simplifying this
  // than a concat-rewrite. Fold constructs: 
  //     x ? {N{1'b1}} : {N{1'b0}}.
  if ((nrepeats > 1) and (nargs==1)) {
    NUExpr * arg = rhs_concat->getArg(0);
    if (arg->getBitSize()==1) {
      return false;
    }
  }

  // Cannot handle a variable-sized expression.
  if (rhs_concat->sizeVaries()) {
    return false;
  }

  {
    // do not rewrite concats if the lhs and rhs can fit in 32 bits and
    // part of the RHS is a constant, codegen can handle this better
    // with integers
    if ( ( rhs->getBitSize() <= 32) && ( lhs->getBitSize() <= 32 ) ){
      for (SInt32 idx = nargs - 1; (idx >= 0); --idx) {
        NUExpr *cur_rvalue = rhs_concat->getArg((UInt32)idx);
        if ( cur_rvalue->castConst() ){
          return false;
        }
      }
    }
  }
 
  // cur_part tracks which absolute piece of the lhs we are currently after.
  ConstantRange cur_part(-1,-1);

  // Loop through all rhs parts, create new lhs, rhs pairs
  for (SInt32 repeat = nrepeats;
       repeat > 0;
       --repeat) {
    for (SInt32 idx = nargs - 1; idx >= 0; --idx) {
      NUExpr *cur_rvalue = rhs_concat->getArg((UInt32)idx);

      UInt32 part_size = cur_rvalue->getBitSize();
      cur_part = ConstantRange(cur_part.getMsb() + part_size, cur_part.getMsb() + 1);
      // getAbsoluteIndexedLvalueCopy will return NULL if the
      // requested part is out-of-range. In this situation, we are
      // computing more RHS bits than the LHS needs and can ignore
      // that portion of the RHS.
      NULvalue *new_lvalue = getAbsoluteIndexedLvalueCopy(lhs, cur_part);
      if (new_lvalue)
      {
        NUExpr *new_rvalue = cur_rvalue->copy(ctx);
        AssignComponents assign_parts(new_lvalue, new_rvalue, assign_type);
        new_parts_list.push_back(assign_parts);
      }
    }
  }

  // If the LHS was not completely covered, assign LHS to zero.
  // Note: This only works if we are working with an unsigned RHS.
  UInt32 cur_msb = cur_part.getMsb();
  if ((cur_msb+1) < lhs->getBitSize()) {
    cur_part = ConstantRange(lhs->getBitSize()-1, cur_msb + 1);
    NULvalue *new_lvalue = getAbsoluteIndexedLvalueCopy(lhs, cur_part);
    if (new_lvalue) {
      NUExpr *new_rvalue = NUConst::create( false, 0, 8, lhs->getLoc());
      AssignComponents assign_parts(new_lvalue, new_rvalue, assign_type);
      new_parts_list.push_back(assign_parts);
    }
  }

  return true;
} // void ConcatRewrite::createNewPartsConcatRvalue


NULvalue *ConcatRewrite::getAbsoluteIndexedLvalueCopy(NULvalue *lhs, const ConstantRange &wanted_part)
{
  if (lhs->isWholeIdentifier()) {
    NUNet *net = lhs->getWholeIdentifier();
    if (net->isBitNet()) {
      if (wanted_part.getLsb()==0) {
        return new NUIdentLvalue(net, lhs->getLoc());
      } else {
        return NULL;
      }
    } else {
      NU_ASSERT(net->isVectorNet(),lhs);
      NUVectorNet *vnet = dynamic_cast<NUVectorNet*>(net);
      const ConstantRange &net_range = *(vnet->getRange());
      ConstantRange new_range(net_range.index(wanted_part.getMsb(), false),
                              net_range.index(wanted_part.getLsb(), false));
      if (net_range.overlaps(new_range)) {
        return new NUVarselLvalue(net, net_range.overlap(new_range), lhs->getLoc());
      } else {
        return NULL;
      }
    }
  } else if (lhs->getType() == eNUVarselLvalue) {
    CopyContext cc (0,0);
    NUVarselLvalue *partsel = dynamic_cast<NUVarselLvalue*>(lhs);
    const ConstantRange &part_range = *(partsel->getRange());
    ConstantRange new_range(part_range.index(wanted_part.getMsb(), false),
                            part_range.index(wanted_part.getLsb(), false));
    if (part_range.overlaps(new_range)) {
      return new NUVarselLvalue(partsel->getLvalue()->copy(cc),
                                partsel->getIndex()->copy (cc),
                                part_range.overlap(new_range), lhs->getLoc());
    } else {
      return NULL;
    }
  } else {
    NU_ASSERT("invalid LHS during concat rewrite" == NULL,lhs);
    return NULL;
  }
}


void ConcatRewrite::createNewPartsVarselLvalue(NUType assign_type, NULvalue * lhs,
                                               NUExpr * rhs, bool looping_context,
                                               AssignComponentsList &new_parts_list)
{
  // test/port-splitting/hiersplit3.v
  NUVarselLvalue* varselLval = dynamic_cast<NUVarselLvalue*>(lhs);
  NU_ASSERT(varselLval, lhs);

  // If the lvalue is a select on a concat, then it's conversion process ..described
  // later in this method, creates temporaries and statements which blockingly
  // def as well as use them. If the assign statement is non-blocking, we have a mix
  // of blocking/non-blocking statements which could all be inside a loop!. This conversion
  // is incorrect. To avoid this issue, the conversion is disabled for blocking assign
  // statements inside looping context with concat lvalues. 
  // For more details, see test/vhdl/clocking/trailingreset26.vhdl.
  // Here's an example:
  //
  // // Although this is illegal in Verilog, VHDL converted Verilog could generate this
  // for (i = 0; i < 4; i = i + 1)
  //  {vec1[3], vec2[2:0]}[i] <= in[i];
  //
  // // After concat rewrite it would look like:
  // for (i = 0; i < 4; i = i + 1) begin
  //   temp[3] = vec1[3];
  //   temp[2:0] = vec2[2:0];
  //   temp[i] = in[i];
  //   vec1[3] <= temp[3];
  //   vec2[2:0] <= temp[2:0];
  // end


  NULvalue* ident = varselLval->getLvalue();
  if ( (ident->getType() != eNUConcatLvalue) ||
       ((assign_type == eNUNonBlockingAssign) && looping_context) ) {
    return; // nothing to do.
  }

  NUConcatLvalue* concat = dynamic_cast<NUConcatLvalue*>(ident);
  NU_ASSERT(concat, lhs);

  // We need to convert this:
  //    {sub.$portsplit_out_19_18,
  //     sub.$portsplit_out_17_0}[{1'b0,sub.a}] = 1'b1;
  // to this:
  //    s1: temp = {sub.$portsplit_out_19_18, sub.$portsplit_out_17_0};
  //    s2: temp[{1'b0,sub.a}] = 1'b1;
  //    s3: {sub.$portsplit_out_19_18, sub.$portsplit_out_17_0} = temp;
  // and then we know that concat-rewrite will re-process the
  // statement list so that we wind up replacing that last one with
  //    sub.$portsplit_out_19_18 = temp[19:18];
  //    sub.$portsplit_out_17_0} = temp[17:0];
  StringAtom* sym = mScope->getModule()->gensym("concat");
  ConstantRange range(concat->getBitSize() - 1, 0);
  const SourceLocator& loc = concat->getLoc();
  NUNet* tempNet = mScope->getModule()->createTempVectorNet(sym, range, false, loc);
  NULvalue* tempLval1 = new NUIdentLvalue(tempNet, loc);

  // Make a concat-rvalue from the concat-lvalue.
  NUExpr* concatRval = concat->NURvalue();
  concatRval->resize(concat->getBitSize());
  AssignComponents s1(tempLval1, concatRval, eNUBlockingAssign);

  // Make S2 using the existing RHS
  CopyContext cc(0, 0);
  NUExpr* varIndex = varselLval->getIndex()->copy(cc);
  ConstantRange varRange = *varselLval->getRange();
  const SourceLocator& vloc = varselLval->getLoc();
  varselLval = new NUVarselLvalue(tempNet, varIndex, varRange, vloc);

  NUExpr* s2Rhs = rhs->copy(cc);
  s2Rhs->resize(rhs->getBitSize());
  AssignComponents s2(varselLval, s2Rhs, eNUBlockingAssign);

  // Make S3
  NUExpr* tempRval = new NUIdentRvalue(tempNet, loc);
  tempRval->resize(tempNet->getBitSize());
  AssignComponents s3(concat->copy(cc), tempRval, assign_type);

  new_parts_list.push_back(s1);
  new_parts_list.push_back(s2);
  new_parts_list.push_back(s3);
} // void ConcatRewrite::createNewPartsVarselLvalue

void ConcatStatistics::print() const
{
  UtIO::cout() << "Concat Rewrite Summary: " 
	       << mBlockingAssigns << " blocking assigns rewritten, "
	       << mNonblockingAssigns << " non-blocking assigns rewritten, "
	       << mContAssigns << " continuous assigns rewritten, affecting "
	       << mModules  << " modules." << UtIO::endl;
}
