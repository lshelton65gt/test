// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/ControlMerge.h"

#include "reduce/Inference.h"
#include "reduce/Fold.h"
#include "reduce/REUtil.h"

#include "localflow/UD.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUTaskEnable.h"

#include "util/ArgProc.h"
#include "util/UtIOStream.h"
#include "compiler_driver/CarbonContext.h"

ControlMerge::ControlMerge(AtomicCache * str_cache,
			   NUNetRefFactory * netref_factory,
			   MsgContext * msg_context,
			   IODBNucleus * iodb,
			   ArgProc * args,
			   bool verbose,
			   ControlMergeStatistics *stats,
			   InferenceStatistics *inference_stats) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_context),
  mArgs(args),
  mStatistics(stats),
  mVerbose(verbose)
{
  mUD = new UD(mNetRefFactory, mMsgContext, iodb, args, false);
  mFold = new Fold(mNetRefFactory, args, mMsgContext, mStrCache, iodb,
                   mArgs->getBoolValue(CRYPT("-verboseFold")),
                   /* eFoldUDValid |*/ eFoldUDKiller);

  Inference::Params params (mArgs, msg_context);
  mInference = new Inference(mFold, mNetRefFactory, msg_context, str_cache, 
    iodb, mArgs, params, inference_stats);
}


ControlMerge::~ControlMerge()
{
  delete mUD;
  delete mFold;
  delete mInference;
}


void ControlMerge::module(NUModule * one)
{
  ControlMergeModule one_module(one,
				mNetRefFactory,
				mStatistics,
				mVerbose);
  bool blocks_were_merged = one_module.merge();

  if (blocks_were_merged) {
    mStatistics->module();

    // vectorize if possible.
    mInference->module(one);

    // recompute UD.
    mUD->module(one);
  }
}


ControlMergeModule::ControlMergeModule(NUModule* module,
				       NUNetRefFactory * netref_factory,
				       ControlMergeStatistics * statistics,
				       bool verbose) :
  mModule(module),
  mNetRefFactory(netref_factory),
  mStatistics(statistics),
  mVerbose(verbose)
{
  mUtility = new ReduceUtility(module,
			       mNetRefFactory);
}


ControlMergeModule::~ControlMergeModule()
{
  delete mUtility;
}


bool ControlMergeModule::merge()
{
  mUtility->update();

  findCandidates();

  filterCandidates();

  filterIsoChunks();

  bool merged = mergeIsoChunks();

  mUtility->clear();

  return merged;
}


void ControlMergeModule::findCandidates()
{
  NUAlwaysBlockList always_blocks;

  mModule->getAlwaysBlocks(&always_blocks);
  for (NUAlwaysBlockList::iterator iter = always_blocks.begin();
       iter != always_blocks.end();
       ++iter) {
    NUAlwaysBlock * always = (*iter);
    validate(always);
  }
}


void ControlMergeModule::validate(NUAlwaysBlock * always)
{
  // 1) always block defines one net.
  NUNetRefSet defs(mNetRefFactory);
  always->getDefs(&defs);
  bool valid = (defs.size()==1);

  if (valid) {
    // 2) block contains one statement.
    const NUBlock * block = always->getBlock();
    NUStmtCLoop loop = block->loopStmts();
    NUStmtList block_stmts(loop.begin(),loop.end());
    valid = (block_stmts.size()==1);

    if (valid) {
      // 3) that one statement is one of:
      //    a) blocking assign
      //    b) branching stmt (case or if)
      const NUStmt * stmt = (*(block_stmts.begin()));
      const NUBlockingAssign * assign = dynamic_cast<const NUBlockingAssign *>(stmt);
      const NUCase * case_stmt = dynamic_cast<const NUCase *>(stmt);
      const NUIf * if_stmt = dynamic_cast<const NUIf *>(stmt);
      valid = (assign or case_stmt or if_stmt);
    }
  }

  if (valid) {
    NUNetRefHdl def_net_ref = (*(defs.begin()));

    // this netref cannot be multiply defined.
    valid = (mUtility->getDefCount(def_net_ref) == 1);

    if (valid) {
      valid = checkDefConsistency(always,def_net_ref);
    }

    if (valid) {
      NUNet * def_net = def_net_ref->getNet();

      // we should never consider block-local nets.
      valid = (not def_net->isBlockLocal());

      if (valid) {
	mValidNetDrivers[def_net].push_back(always);
      }
    }
  }
}


class CheckAlwaysDefNetRefCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  CheckAlwaysDefNetRefCallback(NUNetRefFactory * netref_factory,
			       NUNetRefHdl & def_net_ref) :
    mNetRefFactory(netref_factory),
    mDefNetRef(def_net_ref),
    mConsistent(true)
  {}
  
  // just concerned with stmts.
  Status operator()(Phase /* phase */, NUBase * /* node */) { return eNormal; }
  Status operator()(Phase phase, NUStmt *stmt)
  {
    if (phase != ePre) {
      return eNormal; // only call in pre.
    }
    NUNetRefSet defs(mNetRefFactory);
    stmt->getBlockingDefs(&defs);
    
    if (defs.size()==1) {
      NUNetRefHdl def_net_ref = (*(defs.begin()));
      mConsistent = (def_net_ref==mDefNetRef);
    } else {
      mConsistent = false;
    }
    return mConsistent ? eNormal : eStop;
  }

  bool consistent() const { return mConsistent; }

private:
  NUNetRefFactory * mNetRefFactory;
  NUNetRefHdl & mDefNetRef;
  bool mConsistent;
};


bool ControlMergeModule::checkDefConsistency(NUAlwaysBlock * always,
					     NUNetRefHdl & def_net_ref)
{
  CheckAlwaysDefNetRefCallback checker(mNetRefFactory,def_net_ref);
  NUDesignWalker walker(checker,false);
  walker.alwaysBlock(always);
  return checker.consistent();
}


void ControlMergeModule::filterCandidates()
{
  for (ValidNetDrivers::iterator iter = mValidNetDrivers.begin();
       iter != mValidNetDrivers.end();
       ++iter) {
    NUAlwaysBlockVector & blocks = iter->second;

    filterCandidates(blocks);
  }
}


void ControlMergeModule::filterCandidates(NUAlwaysBlockVector & blocks)
{
  // 1. initialize chunk
  Chunk everything(mNetRefFactory);
  everything.extend(blocks);

  // 2. sort chunk in order of def_net_ref.
  LessThanUseDef ltUD(mNetRefFactory);
  sort(everything.begin(), everything.end(), ltUD);

  // Make sure sequential blocks do not have dependencies to earlier
  // blocks in a chunk.
  NUNetRefSet collected_defs(mNetRefFactory);

  // 3. create chunks based on adjacent def_net_ref
  Chunk chunk(mNetRefFactory);
  NUNetRefHdl previous = mNetRefFactory->createEmptyNetRef();
  for (NUAlwaysBlockVector::iterator iter = everything.begin();
       iter != everything.end();
       ++iter) {
    NUAlwaysBlock * always = (*iter);
    NUNetRefSet defs(mNetRefFactory);
    always->getDefs(&defs);
    NU_ASSERT(defs.size()==1, always);

    NUNetRefHdl current = (*(defs.begin()));
    bool valid_to_merge = previous->adjacent(*current);

    if (valid_to_merge and always->isSequential()) {
      // Don't merge dependent, sequential blocks together.
      NUNetRefSet uses(mNetRefFactory);
      always->getUses(&uses);

      valid_to_merge = not NUNetRefSet::set_has_intersection(collected_defs,uses);
    }

    if (not valid_to_merge) {
      if (chunk.size()>1) {
	mChunks.push_back(chunk);
      }
      collected_defs.clear();
      chunk.clear();
    }

    chunk.push_back(always);
    if (always->isSequential()) {
      collected_defs.insert(defs.begin(),defs.end());
    }
    previous = current;
  }
  if (chunk.size()>1) {
    mChunks.push_back(chunk);
  }
  chunk.clear();
}


void ControlMergeModule::filterIsoChunks()
{
  for (Chunks::iterator iter = mChunks.begin();
       iter != mChunks.end();
       ++iter) {
    Chunk chunk = (*iter);
    if (chunk.size()<=1) {
      continue; // too small.
    }

    filterIsoChunks(chunk);
  }  
}


void ControlMergeModule::filterIsoChunks(Chunk & chunk)
{
  NUAlwaysBlock * previous = NULL;
  Chunk isoChunk(mNetRefFactory);

  for (Chunk::iterator iter = chunk.begin();
       iter != chunk.end();
       ++iter) {
    NUAlwaysBlock * current = (*iter);
    if (previous) {
      if (not iso(previous, current)) {
	if (isoChunk.size()>1) {
	  mIsoChunks.push_back(isoChunk);
	}
	isoChunk.clear();
      }
    }

    isoChunk.push_back(current);
    previous = current;
  }

  if (isoChunk.size()>1) {
    mIsoChunks.push_back(isoChunk);
  }
  isoChunk.clear();
}


bool ControlMergeModule::iso(NUAlwaysBlock * a,
			     NUAlwaysBlock * b)
{
  // TBD: verify edge expression equivalence.
  bool result = true;
  NUAlwaysBlock::EdgeExprLoop aLoop = a->loopEdgeExprs();
  NUAlwaysBlock::EdgeExprLoop bLoop = b->loopEdgeExprs();
  for ( ; result and ((not aLoop.atEnd()) and
		      (not bLoop.atEnd()));
	++aLoop,++bLoop) {
    NUEdgeExpr * aExpr = (*aLoop);
    NUEdgeExpr * bExpr = (*bLoop);

    result = ((*aExpr)==(*bExpr));
  }

  result &= (aLoop.atEnd() and bLoop.atEnd());
  
  if (result) {
    result = iso(a->getBlock(), b->getBlock());
  }

  return result;
}

bool ControlMergeModule::iso(NUStmt *a,
			     NUStmt *b)
{
  NUBlock *aBlock = dynamic_cast<NUBlock*>(a);
  NUBlock *bBlock = dynamic_cast<NUBlock*>(b);
  if (aBlock and bBlock) {
    return iso(aBlock,bBlock);
  } else if (aBlock or bBlock) {
    return false;
  }

  NUIf *aIf = dynamic_cast<NUIf*>(a);
  NUIf *bIf = dynamic_cast<NUIf*>(b);
  if (aIf and bIf) {
    return iso(aIf,bIf);
  } else if (aIf or bIf) {
    return false;
  }

  NUCase *aCase = dynamic_cast<NUCase*>(a);
  NUCase *bCase = dynamic_cast<NUCase*>(b);
  if (aCase and bCase) {
    return iso(aCase,bCase);
  } else if (aCase or bCase) {
    return false;
  }

  // other statement types are seen as isomorphic if they line up. we
  // do not perform subcompares on their structure.

  NUBlockingAssign *aAss = dynamic_cast<NUBlockingAssign*>(a);
  NUBlockingAssign *bAss = dynamic_cast<NUBlockingAssign*>(b);
  if (aAss and bAss) {
    return true;
  } else if (aAss or bAss) {
    return false;
  }
  
  NUFor *aFor = dynamic_cast<NUFor*>(a);
  NUFor *bFor = dynamic_cast<NUFor*>(b);
  if (aFor and bFor) {
    return true;
  } else if (aFor or bFor) {
    return false;
  }

  NUTaskEnable *aTE = dynamic_cast<NUTaskEnable*>(a);
  NUTaskEnable *bTE = dynamic_cast<NUTaskEnable*>(b);
  if (aTE and bTE) {
    return true;
  } else if (aTE or bTE) {
    return false;
  }
  // Should we support an other types?
  return false;
}

bool ControlMergeModule::iso(NUBlock *a,
			     NUBlock *b)
{
  return iso(a->loopStmts(),
	     b->loopStmts());
}

bool ControlMergeModule::iso(NUIf *a,
			     NUIf *b)
{
  bool result;
  NUExpr * aCond = a->getCond();
  NUExpr * bCond = b->getCond();

  result = ((*aCond)==(*bCond));

  if (result) {
    result = iso(a->loopThen(),
		 b->loopThen());
  }

  if (result) {
    result = iso(a->loopElse(),
		 b->loopElse());
  }
  return result;
}

static bool
isoConditionHelper (NUCaseItem::ConditionLoop aLoop,
                    NUCaseItem::ConditionLoop bLoop)
{
  bool result = true;
  for (; result and ((not aLoop.atEnd ()) and (not bLoop.atEnd ())); 
       ++aLoop,++bLoop) {
    result &= ((*aLoop) == (*bLoop));
  }

  result &= (aLoop.atEnd () & bLoop.atEnd ());
  return result;
}

bool ControlMergeModule::iso(NUCase *a,
			     NUCase *b)
{
  bool result;
  NUExpr * aSel = a->getSelect();
  NUExpr * bSel = b->getSelect();

  result = ((*aSel)==(*bSel));

  NUCase::ItemLoop aLoop = a->loopItems();
  NUCase::ItemLoop bLoop = b->loopItems();
  for (; result and ((not aLoop.atEnd()) and
		     (not bLoop.atEnd()));
       ++aLoop,++bLoop) {
    NUCaseItem *aItem = (*aLoop);
    NUCaseItem *bItem = (*bLoop);

    result &= isoConditionHelper (aItem->loopConditions (), bItem->loopConditions ())
           && iso(aItem->loopStmts(), bItem->loopStmts());
  }

  result &= (aLoop.atEnd() and bLoop.atEnd());
  return result;
}


bool ControlMergeModule::iso(NUStmtLoop aLoop,
			     NUStmtLoop bLoop)
{
  bool result = true;
  for ( ; result and ((not aLoop.atEnd()) and
		      (not bLoop.atEnd())) ;
	++aLoop,++bLoop) {
    NUStmt *a = (*aLoop);
    NUStmt *b = (*bLoop);
    result &= iso(a,b);
  }
  result &= (aLoop.atEnd() and bLoop.atEnd());
  return result;
}


bool ControlMergeModule::mergeIsoChunks()
{
  for (Chunks::iterator iter = mIsoChunks.begin();
       iter != mIsoChunks.end();
       ++iter) {
    Chunk chunk = (*iter);
    NU_ASSERT(chunk.size()>1, mModule);

    bool success = mergeIsoChunk(chunk);
    if (success) {
      mMergedChunks.push_back(chunk);
    }
  }

  return (not mMergedChunks.empty());
}


bool ControlMergeModule::mergeIsoChunk(Chunk & chunk)
{
  NU_ASSERT(chunk.size()>1, mModule);

  NUAlwaysBlock * always = chunk.collate();
  if (always) {
    // the one returned should be saved.
    mStatistics->target();
    for (Chunk::iterator iter = chunk.begin();
	 iter != chunk.end();
	 ++iter) {
      NUAlwaysBlock * old = (*iter);
      mStatistics->source(); // 'always' is counted as source and target.
      if (always != old) {
        NUAlwaysBlock *current_block = old->getClockBlock ();
        if (current_block == NULL) {
          current_block = old;
        }
        NUAlwaysBlock* old_clock_block = current_block;

        while (current_block) {
          NUAlwaysBlock *next_block = current_block->getPriorityBlock ();
          if (next_block == old) {
            current_block->setPriorityBlock (always);
          }
          if (current_block->getClockBlock () == old) {
            current_block->setClockBlock (always);
          }
          current_block = next_block;
        }
	mModule->removeAlwaysBlock(old);
	// ACA: I believe this is unneeded.
        // mModule->removeBlock(old->getBlock());

        // The old_clock_block now has all kinds of stray pointers
        // in its referrers set and mPrioBlock.  Make sure all the
        // referrers now refer to the new clock block, and clear out
        // the priority block pointer from the old clock block.  This
        // is needed to allow NUModule::sanityCheckAlwaysBlockPrioChain
        // to pass.
        NUAlwaysBlockVector referers;
        old_clock_block->getClockBlockReferers(&referers);
        NUAlwaysBlock* new_clock_block = always->getClockBlock();
        if (new_clock_block == NULL) {
          new_clock_block = always;
        }

        for (UInt32 i = 0; i < referers.size(); ++i) {
          NUAlwaysBlock* referer = referers[i];
          NUAlwaysBlock* referer_clk_blk = referer->getClockBlock();
          if (referer_clk_blk != new_clock_block) {
            NU_ASSERT(referer_clk_blk == old_clock_block, referer);
            referer->setClockBlock(new_clock_block);
          }
          old_clock_block->removeClockBlockReferer(referer);
        }

        old_clock_block->setPriorityBlock(NULL);
	delete old;
      }
    }
    return true;
  }
  return false;
}


NUAlwaysBlock * Chunk::collate()
{
  iterator iter = begin();
  NUAlwaysBlock * first = (*iter);
  for (++iter ; iter != end() ; ++iter) {
    NUAlwaysBlock * second = (*iter);
    NUAlwaysBlock * result = collate(first,second);
    NU_ASSERT2(result==first, first, result);
  }
  return first;
}


NUAlwaysBlock * Chunk::collate(NUAlwaysBlock * a, NUAlwaysBlock * b)
{
  collate(a->getBlock(),
	  b->getBlock());
  return a;
}


NUStmt * Chunk::collate(NUStmt *a, NUStmt *b)
{
  NUBlock *aBlock = dynamic_cast<NUBlock*>(a);
  if (aBlock) {
    NUBlock *bBlock = dynamic_cast<NUBlock*>(b);
    NU_ASSERT(bBlock, b);
    return collate(aBlock,bBlock);
  }

  NUIf *aIf = dynamic_cast<NUIf*>(a);
  if (aIf) {
    NUIf *bIf = dynamic_cast<NUIf*>(b);
    NU_ASSERT(bIf, b);
    return collate(aIf,bIf);
  }

  NUCase *aCase = dynamic_cast<NUCase*>(a);
  if (aCase) {
    NUCase *bCase = dynamic_cast<NUCase*>(b);
    NU_ASSERT(bCase, b);
    return collate(aCase,bCase);
  }

  return NULL;
}


NUBlock * Chunk::collate(NUBlock *a, NUBlock *b)
{
  NUStmtList savStmts;
  NUStmtList delStmts;

  collate(a->loopStmts(),
	  b->loopStmts(),
	  savStmts,
	  delStmts);

  // 'a' gets the statements from 'b' we want to preserve. it already
  // has its own statements.
  a->addStmts(&savStmts);

  // 'b' gets the statements from 'b' we didn't use -- they'll need deleting.
  b->replaceStmtList(delStmts); 

  return a;
}


NUIf * Chunk::collate(NUIf *a, NUIf *b)
{
  NUStmtList savStmts;
  NUStmtList delStmts;

  collate(a->loopThen(),
	  b->loopThen(),
	  savStmts,
	  delStmts);

  // 'a' gets the statements from 'b' we want to preserve. it already
  // has its own statements.
  a->addThenStmts(&savStmts);

  // 'b' gets the statements from 'b' we didn't use -- they'll need deleting.
  b->replaceThen(delStmts); 

  savStmts.clear();
  delStmts.clear();

  collate(a->loopElse(),
	  b->loopElse(),
	  savStmts,
	  delStmts);

  // 'a' gets the statements from 'b' we want to preserve. it already
  // has its own statements.
  a->addElseStmts(&savStmts);

  // 'b' gets the statements from 'b' we didn't use -- they'll need deleting.
  b->replaceElse(delStmts); 

  return a;
}


NUCase * Chunk::collate(NUCase *a, NUCase *b)
{
  for (NUCase::ItemLoop aLoop = a->loopItems(),
	 bLoop = b->loopItems();
       ((not aLoop.atEnd()) and
	(not bLoop.atEnd()));
       ++aLoop,++bLoop) {
    NUCaseItem *aItem = (*aLoop);
    NUCaseItem *bItem = (*bLoop);

    NUStmtList savStmts;
    NUStmtList delStmts;

    collate(aItem->loopStmts(),
	    bItem->loopStmts(),
	    savStmts,
	    delStmts);

    // 'a' gets the combined set of statements.
    aItem->addStmts(&savStmts);

    // 'b' gets the statements we didn't use -- they'll need deleting.
    bItem->replaceStmts(delStmts);
  }

  return a;
}

void Chunk::collate(NUStmtLoop aLoop,
		    NUStmtLoop bLoop,
		    NUStmtList &savStmts,
		    NUStmtList &delStmts)
{
  for ( ; ((not aLoop.atEnd()) and
	   (not bLoop.atEnd()));
	++aLoop,++bLoop) {
    NUStmt * aStmt = (*aLoop);
    NUStmt * bStmt = (*bLoop);

    NUStmt * result = collate(aStmt,bStmt);
    if (result) {
      NU_ASSERT2(aStmt==result, aStmt, result);
      delStmts.push_back(bStmt);
    } else {
      savStmts.push_back(bStmt);
    }
  }

  assert(bLoop.atEnd());                // this_assert_OK
}


void ControlMergeModule::printChunks(Chunks * chunks) const
{
  for (Chunks::const_iterator iter = chunks->begin();
       iter != chunks->end();
       ++iter) {
    const Chunk chunk = (*iter);
    chunk.print();
  }
}


void Chunk::print() const
{
  UtIO::cout() << "Chunk (size=" << size() << "):" << UtIO::endl;
  for (const_iterator iter = begin();
       iter != end();
       ++iter) {
    const NUAlwaysBlock * always = (*iter);
    UtIO::cout() << "    AlwaysBlock ";
    always->getLoc().print();
    NUNetRefSet defs(mNetRefFactory);
    always->getDefs(&defs);
    for (NUNetRefSet::iterator nr_iter = defs.begin();
	 nr_iter != defs.end();
	 ++nr_iter) {
      NUNetRefHdl def_net_ref = (*nr_iter);
      def_net_ref->print(8);
    }
  }
}


void ControlMergeStatistics::print() const
{
  UtIO::cout() << "ControlMerge Summary: "
	       << mSources << " blocks merged into "
	       << mTargets << " target always blocks in "
	       << mModules << " modules." << UtIO::endl;
}
