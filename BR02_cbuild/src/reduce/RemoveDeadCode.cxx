// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/RemoveDeadCode.h"
#include "reduce/CleanUnelabFlow.h"
#include "reduce/DeadDesignWalker.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUModule.h"
#include "flow/FLNode.h"
#include "iodb/IODBNucleus.h"
#include "util/UtIOStream.h"
#include "util/OSWrapper.h"

/*!
  \file
  Implementation of dead code removal package.
 */


void RemoveDeadCode::design(NUDesign *this_design, DeadDesignCallback * callback)
{
  UnelabMarkSweep marker(mFlowFactory, mIODB, mStartAtAllNets);
  marker.mark(this_design);

  if (mDeleteDeadCode) {
    markLiveNucleus(this_design);
  }

  // Clean up the unelaborated flow before delete the nucleus,
  // because the nucleus deletion may get rid of some nets.
  CleanUnelabFlow clean(mFlowFactory, mIODB, mStartAtAllNets, mVerbose);
  clean.deleteDeadFlow();

  if (mDeleteDeadCode) {
    deleteDeadNucleus(this_design, callback);
  }
}


void RemoveDeadCode::module(NUModule *this_module, DeadDesignCallback *callback)
{
  UnelabMarkSweep marker(mFlowFactory, mIODB, mStartAtAllNets);
  marker.mark(this_module, false);

  if (mDeleteDeadCode) {
    markLiveNucleus(this_module);
  }

  // Clean up the unelaborated flow before delete the nucleus,
  // because the nucleus deletion may get rid of some nets.
  CleanUnelabFlow clean(mFlowFactory, mIODB, mStartAtAllNets, mVerbose);
  clean.deleteDeadFlow();

  if (mDeleteDeadCode) {
    deleteDeadNucleus(this_module, callback);
  }
}


void RemoveDeadCode::markLiveNucleus(NUDesign *this_design)
{
  // First, clear out the mark bit.
  NucleusTouchCallback nucleus_callback(this);
  NUDesignWalker nucleus_marker(nucleus_callback, false);
  NUDesignCallback::Status status = nucleus_marker.design(this_design);
  INFO_ASSERT(status == NUDesignCallback::eNormal, "Marker walk exited abnormally");

  // Now, mark alive nucleus.
  UnelabMarkSweep marker(mFlowFactory, mIODB, mStartAtAllNets);
  FlowCallback sweep_callback(this);
  marker.sweep(sweep_callback);
}


void RemoveDeadCode::markLiveNucleus(NUModule *this_module)
{
  // First, clear out the mark bit.
  NucleusTouchCallback nucleus_callback(this, true);
  NUDesignWalker nucleus_marker(nucleus_callback, false);
  NUDesignCallback::Status status = nucleus_marker.module(this_module);
  NU_ASSERT(status == NUDesignCallback::eNormal,this_module);

  // Now, mark alive nucleus.
  UnelabMarkSweep marker(mFlowFactory, mIODB, mStartAtAllNets);
  FlowCallback sweep_callback(this);
  marker.sweep(sweep_callback);
}


void RemoveDeadCode::deleteDeadNucleus(NUDesign *this_design, DeadDesignCallback * callback)
{
  DeadDesignWalker walker(mNetRefFactory, mMsgContext, mIODB, mArgs, callback, mVerbose);
  walker.walk(this_design, mDeleteInstances);
}


void RemoveDeadCode::deleteDeadNucleus(NUModule *this_module, DeadDesignCallback * callback)
{
  DeadDesignWalker walker(mNetRefFactory, mMsgContext, mIODB, mArgs, callback, mVerbose);
  walker.walk(this_module, mDeleteInstances);
}


void RemoveDeadCode::sweepCallback(FLNode *flnode, bool is_dead)
{
  if (is_dead) {
    return;
  }

  NUUseDefNode *node = flnode->getUseDefNode();

  if (node == 0) {
    return;
  }

  node->putMark(true);

  // If this is an always block part of a clock/priority tree, the
  // entire tree should be marked.
  NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(node);
  if (always) {
    // if this always block is in a clock/priority tree, mark all of
    // them at once.
    NUAlwaysBlock * clockBlock = always->getClockBlock(); // the clock block knows about the entire tree.
    if (not clockBlock) {
      clockBlock = always; // a clock block has a NULL clock block.
    }

    // The clockBlock will not be encountered during our referer walk.
    clockBlock->putMark(true);
    clockBlock->getBlock()->putMark(true);

    NUAlwaysBlockVector priority_tree; 
    clockBlock->getClockBlockReferers(&priority_tree);
    // if this is not a clock block, priority_tree will be empty.
    for (NUAlwaysBlockVector::iterator iter = priority_tree.begin();
	 iter != priority_tree.end();
	 ++iter) {
      NUAlwaysBlock * priority = (*iter);
      priority->putMark(true);
      priority->getBlock()->putMark(true);
    }
  }

}


NUDesignCallback::Status
RemoveDeadCode::NucleusTouchCallback::operator()(NUDesignCallback::Phase phase, NUBase *node)
{
  if (phase == ePre) {
    node->putMark(false);
  }
  return eNormal;
}


NUDesignCallback::Status
RemoveDeadCode::NucleusTouchCallback::operator()(NUDesignCallback::Phase phase, NUModule *node)
{
  if (phase == ePre) {
    if (not mContinueModule) {
      return eSkip;
    }
    if (mOnlyOneModule) {
      mContinueModule = false;
    }
    node->putMark(false);
  }
  return eNormal;
}
