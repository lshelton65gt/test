// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/UnelabMerge.h"
#include "reduce/REUtil.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUCopyContext.h"
#include "nucleus/NUEnabledDriver.h"

#include "localflow/UD.h"
#include "util/StringAtom.h"
#include "util/SetOps.h"
#include "iodb/IODBNucleus.h"

#include "util/UtIOStream.h"
#include "util/UtOrder.h"

#if !pfICC && !defined(__COVERITY__)
// force instantiation
template class NUNetRefMultiMap<UnelabFanout*>;
#endif

template <>
UnelabFanout * NUNetRefMultiMap<UnelabFanout*>::nullT() const {
  return 0;
}

template <>
void NUNetRefMultiMap<UnelabFanout*>::helperTPrint(UnelabFanout * const & v, int /*indent*/) const {
  v->print();
}

#if !pfICC && !defined(__COVERITY__)
// force instantiation
template class NUNetRefMultiMap<NUUseDefNode*, HashPointer<NUUseDefNode*, 2>,
                                NUNetCmp>;
#endif

template <>
NUUseDefNode * NUNetRefNodeMap::nullT() const {
  return 0;
}

template <>
void NUNetRefNodeMap::helperTPrint(NUUseDefNode * const & v, int indent) const
{
  v->print(false,indent);
}

UnelabMerge::UnelabMerge(AtomicCache * string_cache,
                         NUNetRefFactory *netref_factory,
                         MsgContext * msg_ctx,
                         IODBNucleus * iodb,
                         ArgProc* args,
                         bool verbose,
                         UnelabMergeStatistics *stats) :
  mStrCache(string_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mStatistics(stats),
  mVerbose(verbose)
{
  mUD = new UD(mNetRefFactory, mMsgContext, iodb, args, false);
}


UnelabMerge::~UnelabMerge() 
{ 
  delete mUD;
}


void UnelabMerge::module(NUModule * this_module)
{
  bool any_changed = false;
  bool changed = false;
  UnelabMergeStatistics overall_stats;
  UnelabMergeStatistics stats;
  do {
    stats.iteration();
    UnelabModuleMerge one_module(this_module,
				 mStrCache,
				 mNetRefFactory,
				 mMsgContext,
				 mIODB,
				 &stats,
				 mVerbose);
    changed = one_module.merge(UnelabModuleMerge::eFanoutMerge);
    any_changed |= changed;
  } while (changed);
  if (mVerbose) stats.print("Fanout Merge");
  overall_stats.add(stats);
  stats.clear();

  do {
    stats.iteration();
    UnelabModuleMerge one_module(this_module,
				 mStrCache,
				 mNetRefFactory,
				 mMsgContext,
				 mIODB,
				 &stats,
				 mVerbose);
    changed = one_module.merge(UnelabModuleMerge::eFanoutOrCreateMerge);
    any_changed |= changed;
  } while (changed);
  if (mVerbose) stats.print("Fanout/Create Merge");
  overall_stats.add(stats);
  stats.clear();

  do {
    stats.iteration();
    UnelabModuleMerge one_module(this_module,
				 mStrCache,
				 mNetRefFactory,
				 mMsgContext,
				 mIODB,
				 &stats,
				 mVerbose);
    changed = one_module.merge(UnelabModuleMerge::eAnyMerge);
    any_changed |= changed;
  } while (changed);
  if (mVerbose) stats.print("Fanout/Create/Fanin Merge");
  overall_stats.add(stats);
  stats.clear();

  if (mVerbose) overall_stats.print("Module");
  mStatistics->add(overall_stats);
  overall_stats.clear();

  if (any_changed) {
    mUD->module(this_module);
  }
}


UnelabModuleMerge::UnelabModuleMerge(NUModule * this_module,
                                     AtomicCache * string_cache,
                                     NUNetRefFactory *netref_factory,
                                     MsgContext * msg_ctx,
                                     IODBNucleus * iodb,
                                     UnelabMergeStatistics * stats,
                                     bool verbose) :
  mModule(this_module),
  mStrCache(string_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mStatistics(stats),
  mVerbose(verbose),
  mFanoutMap(netref_factory),
  mStartingPoints(netref_factory)
{
  mUtility = new ReduceUtility(this_module,
			       mNetRefFactory);
}


UnelabModuleMerge::~UnelabModuleMerge() 
{
  delete mUtility;
}


bool UnelabModuleMerge::merge(ChainMergeType merge_type)
{
  mMergeType = merge_type;
  INFO_ASSERT(mMergeType & (eFanoutMerge|eFaninMerge|eCreateMerge), "Invalid merge type.");

  mUtility->update();

  // 1. Build up all fanout relationships
  buildFanout();

  // 2. Gather chains of continuous assigns (this can be extended to
  //    trees). These chains are terminated by an always block. If the
  //    chain is not naturally (ie. in the Verilog) terminated by an
  //    always block, one will be created.
  findChains();

  // 3. All chained continuous assignments are added to the front of
  //    this always block.
  bool changed = mergeChains();

  // remove the constructs we promoted into always blocks.
  update(); 

  // clear our internal data structures.
  clearChains();
  clearFanout();

  mUtility->clear();

  mMergeType = eUnknownMerge;
  return changed;
}


void UnelabModuleMerge::buildFanout()
{
  // 1. fanin of cont. assigns.
  for (NUModule::ContAssignLoop loop = mModule->loopContAssigns();
       not loop.atEnd();
       ++loop) {
    NUContAssign * assign = (*loop);
    NUNetRefSet uses(mNetRefFactory);
    assign->getUses(&uses);
    addFanout(assign,uses);
  }

  // 2. fanin of always blocks.
  for (NUModule::AlwaysBlockLoop loop = mModule->loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock * always = (*loop);
    NUNetRefSet uses(mNetRefFactory);
    always->getUses(&uses);
    addFanout(always,uses);
  }

  // 3. submodules.
  for (NUModuleInstanceMultiLoop loop = mModule->loopInstances();
       not loop.atEnd();
       ++loop) {
    NUModuleInstance * instance = (*loop);
    NUNetRefSet uses(mNetRefFactory);
    instance->getUses(&uses);
    addFanout(instance,uses);
  }

  // 4. initial blocks.
  for (NUModule::InitialBlockLoop loop = mModule->loopInitialBlocks();
       !loop.atEnd();
       ++loop) {
    NUInitialBlock * initial = (*loop);
    NUNetRefSet uses(mNetRefFactory);
    initial->getUses(&uses);
    addFanout(initial,uses);
  }

  // 5. fanout of cont enabled drivers
  for (NUModule::ContEnabledDriverLoop loop = mModule->loopContEnabledDrivers();
       !loop.atEnd();
       ++loop) {
    NUEnabledDriver* enabled = (*loop);
    NUNetRefSet uses(mNetRefFactory);
    enabled->getUses(&uses);
    addFanout(enabled,uses);
  }
}


void UnelabModuleMerge::addFanout(NUUseDefNode * use_def, NUNetRefSet & uses)
{
  for (NUNetRefSet::iterator iter = uses.begin();
       iter != uses.end();
       ++iter) {
    NUNetRefHdl net_ref = (*iter);
    addFanout(net_ref,use_def);
  }
}


void UnelabModuleMerge::addFanout(const NUNetRefHdl & net_ref, NUUseDefNode * use_def)
{
  if (net_ref->empty()) return;

  UnelabFanout * net_fanout = getFanout(net_ref);
  if (not net_fanout) {
    net_fanout = new UnelabFanout(net_ref);
    mFanoutMap.insert(net_ref,net_fanout);
  }
  net_fanout->addFanout(use_def);
}


UnelabFanout * UnelabModuleMerge::getFanout(const NUNetRefHdl & net_ref)
{
  UnelabFanout * net_fanout = NULL;

  NUNetRefFanout::CondLoop loop = mFanoutMap.loop(net_ref,&NUNetRef::operator==);
  if (not loop.atEnd()) {
    net_fanout = (*loop).second;
    // Make sure we only have one fanout structure for this netref.
    ++loop; NU_ASSERT(loop.atEnd(), net_ref);
  }
  return net_fanout;
}


void UnelabModuleMerge::getFanout(const NUNetRefHdl & net_ref, NUUseDefSet & fanout)
{
  for (NUNetRefFanout::CondLoop loop = mFanoutMap.loop(net_ref,&NUNetRef::overlapsSameNet);
       not loop.atEnd();
       ++loop) {
    UnelabFanout * net_ref_fanout = (*loop).second;
    net_ref_fanout->getFanout(fanout);
  }
}


void UnelabModuleMerge::clearFanout()
{
  for (NUNetRefFanout::MapLoop loop = mFanoutMap.loop();
       not loop.atEnd();
       ++loop) {
    UnelabFanout * net_fanout = (*loop).second;
    delete net_fanout;
  }
  mFanoutMap.clear();
}


void UnelabModuleMerge::printFanout()
{
  for (NUNetRefFanout::MapLoop loop = mFanoutMap.loop();
       not loop.atEnd();
       ++loop) {
    UnelabFanout * net_fanout = (*loop).second;
    net_fanout->print();
  }
}

void UnelabModuleMerge::findChains()
{
  // 1. Generate starting points.
  //    starting points = nets defined by continuous assigns with
  //                      fanout size of 1.

  findChainCandidates();

  buildChains(&NUNet::isOutput);

  buildChains(NULL);
}


void UnelabModuleMerge::buildChains(NUNet::QueryFunction queryFn)
{
  // 2. While there are still starting points, build chains with the
  //    following technique:
  //    a. Fetch a starting point.
  //    b. Gather fanout chain.
  //    c. From fanout chain, gather fanin tree.
  //    d. Remove elements from starting set.
  //    e. Add "chain" as mergable.

  while (not mStartingPoints.empty()) {

    NUNetRefHdl starting_net_ref;
    NUUseDefNode * starting_node;

    if (queryFn) {
      NUNetRefNodeMap::CondNetLoop loop = mStartingPoints.loop(queryFn);
      if (loop.atEnd()) break; // our filter found nothing. we're done.
      starting_net_ref = (*loop).first;
      starting_node = (*loop).second;
    } else {
      NUNetRefNodeMap::MapLoop loop = mStartingPoints.loop();
      starting_net_ref = (*loop).first;
      starting_node = (*loop).second;
    }
    mStartingPoints.erase(starting_net_ref,&NUNetRef::overlapsSameNet);

    Chain chain;
    chain.push_back(starting_node);

    gatherFanoutChain(starting_net_ref,chain);
    gatherFaninChain(chain);

    mChains.push_back(chain);
  }
}


void UnelabModuleMerge::findChainCandidates()
{
  INFO_ASSERT(mStartingPoints.empty(), "Consistency check error.");

  // Nets we have disallowed as starting points.
  NUNetRefSet invalid(mNetRefFactory);

  for (NUModule::ContAssignLoop loop = mModule->loopContAssigns();
       !loop.atEnd();
       ++loop) {
    NUContAssign * assign = (*loop);

    NUNetRefSet defs(mNetRefFactory);
    assign->getDefs(&defs);

    // Only allow continuous assigns defining a single net.
    if (defs.size() != 1) {
      for (NUNetRefSet::iterator iter = defs.begin();
	   iter != defs.end();
	   ++iter) {
	invalid.insert(*iter); // remember these nets as invalid.
      }
      continue;
    }

    NUNetRefHdl net_ref = (*(defs.begin()));
    NUNet * net = net_ref->getNet();

    // If the RHS drives Z, disqualify.
    NUExpr * rvalue = assign->getRvalue();
    if (rvalue->drivesZ()) {
      continue;
    }

    // invalidate "special" nets which need further analysis.
    if (net->isTrireg() or net->isTri0() or net->isTri1() or 
	net->isTriand() or net->isTrior() or
	net->isWand() or net->isWor() or 
	net->isPullUp() or net->isPullDown()) {
      continue;
    }

    // invalidate all bids -- we can't know their driving
    // characteristics.
    if (net->isBid()) {
      continue;
    }

    // if RHS is change detect then don't include in chain
    NUChangeDetect* changeDetect = dynamic_cast<NUChangeDetect*>(rvalue);
    if (changeDetect != NULL) {
      continue;
    }

    // Don't allow nets we have previously seen and discarded
    if (invalid.find(net_ref,&NUNetRef::overlapsSameNet) != invalid.end()) {
      continue;
    }

    // Don't merge assigments that are candidates for static members.
    // Perform a direct check of the RValue;
    // NUContAssign::isConstantAssign makes use of the DFG, which
    // doesn't exist yet..
    if (assign->getLvalue()->isWholeIdentifier() and
        assign->getRvalue()->castConst() != NULL)
      continue;

    // Only allow nets with a single driver. This will probably
    // disallow cont. assignments of different bits.
    if (mUtility->getDefCount(net_ref) > 1) {
      continue;
    }

    // double-check that we haven't already seen this net.
    NUNetRefNodeMap::CondLoop condloop = mStartingPoints.loop(net_ref,&NUNetRef::overlapsSameNet);
    if (not condloop.atEnd()) {
      invalid.insert(net_ref);
      mStartingPoints.erase(net_ref,&NUNetRef::overlapsSameNet);
    } else {
      mStartingPoints.insert(net_ref,assign);
    }
  }
}


void UnelabModuleMerge::gatherFanoutChain(const NUNetRefHdl & starting_net_ref,
					  Chain & chain)
{
  const NUNetRefHdl empty_net_ref = mNetRefFactory->createEmptyNetRef();
  NUNetRefHdl chain_net_ref = starting_net_ref;
  do {
    NUNet * chain_net = chain_net_ref->getNet();
    if ( chain_net->isInput() or
	 chain_net->isOutput() or
	 chain_net->isBid() or
         chain_net->isProtectedMutable(mIODB) or
         chain_net->isProtectedObservable(mIODB)) {
      break; // IO fanin are not allowed mid-chain.
    }

    NUUseDefSet fanout;
    getFanout(chain_net_ref,fanout);
    chain_net_ref = empty_net_ref;

    if (fanout.size() == 1) {
      NUUseDefNode * fanout_use_def = (*(fanout.begin()));
      if (dynamic_cast<NUContAssign*>(fanout_use_def)) {
	NUNetRefSet defs(mNetRefFactory);
	fanout_use_def->getDefs(&defs);
	if (defs.size() == 1) {
	  NUNetRefHdl net_ref = (*(defs.begin()));
	  NUNetRefNodeMap::CondLoop loop = mStartingPoints.loop(net_ref,&NUNetRef::overlapsSameNet);
	  if (not loop.atEnd()) {
	    chain_net_ref = (*loop).first;
	    NUUseDefNode * chain_node = (*loop).second;
	    chain.push_back(chain_node);

	    // Make sure we had only one starting point for this netref.
	    ++loop; NU_ASSERT(loop.atEnd(),net_ref);

	    mStartingPoints.erase(net_ref);
	  }
	}
      }
    }
  } while(not chain_net_ref->empty());
}


void UnelabModuleMerge::gatherFaninChain(Chain & chain)
{
  // Iterate over the chain in reverse order. Add fanin nodes to the
  // front of the chain.
  for (Chain::reverse_iterator iter = chain.rbegin();
       iter != chain.rend();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    
    NUNetRefSet uses(mNetRefFactory);
    use_def->getUses(&uses);
    for (NUNetRefSet::SortedLoop net_iter = uses.loopSorted();
	 !net_iter.atEnd(); ++net_iter) {
      NUNetRefHdl net_ref = (*net_iter);
      if (net_ref->empty()) continue;

      NUNet * net = net_ref->getNet();
      if ( net->isInput() or
	   net->isOutput() or
	   net->isBid() or
           net->isProtectedMutable(mIODB) or
           net->isProtectedObservable(mIODB)) {
	continue; // IO fanin are not allowed mid-chain.
      }
      NUUseDefSet fanout;
      getFanout(net_ref,fanout);
      
      if (fanout.size()==1) { 
	// The fanout for this one netref is one. Now we need to make
	// sure that the fanout of all bits defined by the same use
	// def as this netref also have fanout of one.

	// We want to avoid collapsing:
	//   a = b | c;
	//   x = a[0];
	//   y = a[1];
	// Our greedy algorithm will want to merge the def of _a_ with
	// both _x_ and _y_, but the def of _a_ has fanout to two places!

	// We want to collapse:
	//   a[0] = x;
	//   a[1] = y;
	//   out = a;
	// Although _a_ is defined in multiple places, each bit is
	// defined uniquely and they each have a single fanout.

	for (NUNetRefNodeMap::CondLoop loop = mStartingPoints.loop(net_ref,&NUNetRef::overlapsSameNet);
	     not loop.atEnd();
	     ++loop) {
	  NUNetRefHdl chain_net_ref = (*loop).first;
	  getFanout(chain_net_ref,fanout);
	}

	if (fanout.size()==1) {
	  for (NUNetRefNodeMap::CondLoop loop = mStartingPoints.loop(net_ref,&NUNetRef::overlapsSameNet);
	       not loop.atEnd();
	       ++loop) {
	    NUUseDefNode * chain_node = (*loop).second;
	    chain.push_front(chain_node);
	  }
	  mStartingPoints.erase(net_ref);
	}
      }
    }
  }
}


void UnelabModuleMerge::clearChains()
{
  mChains.clear();
}


void UnelabModuleMerge::printChains()
{
  for (Chains::iterator iter = mChains.begin();
       iter != mChains.end();
       ++iter) {
    printChain(*iter,eUnknownMerge);
  }
}

void UnelabModuleMerge::printChain(Chain & chain, ChainMergeType merge_type)
{
  UtIO::cout() << "Chain (size=" << chain.size() << "): Merged into ";
  switch(merge_type) {
  case eFanoutMerge:  UtIO::cout() << "fanout";  break;
  case eFaninMerge:   UtIO::cout() << "fanin";   break;
  case eCreateMerge:  UtIO::cout() << "created"; break;
  case eUnknownMerge: UtIO::cout() << "unknown"; break;
  default: INFO_ASSERT(0,"Invalid merge type.");
  }
  UtIO::cout() << " always block." << UtIO::endl;
  for (Chain::iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);

    UtIO::cout() << "    ";
    use_def->getLoc().print();
    NUNetSet defs;
    use_def->getDefs(&defs);
    for (NUNetSet::SortedLoop net_iter = defs.loopSorted();
         !net_iter.atEnd(); ++net_iter)
    {
      NUNet * net = (*net_iter);
      UtIO::cout() << " " << net->getName()->str();
    }

    UtIO::cout() << " <-";
    NUNetSet uses;
    use_def->getUses(&uses);
    for (NUNetSet::SortedLoop net_iter = uses.loopSorted();
	 !net_iter.atEnd(); ++net_iter)
    {
      NUNet * net = (*net_iter);
      UtIO::cout() << " " << net->getName()->str();
    }
    UtIO::cout() << UtIO::endl;
    // (*iter)->print(true,4);
  }
}


bool UnelabModuleMerge::mergeChains()
{
  bool changed = false;
  for (Chains::iterator iter = mChains.begin();
       iter != mChains.end();
       ++iter) {
    Chain chain = (*iter);
    
    bool creates_cycle = createsCycle(chain,NULL);
    if (creates_cycle) {
      continue; // This chain is self-cyclic. Do not merge.
    }

    ChainMergeType merge_type = eUnknownMerge;
    NUAlwaysBlock * always = NULL;

    bool is_dangerous   = isDangerousChain(chain);
    bool incomplete_def = hasIncompleteDef(chain);
    bool no_sequential  = (is_dangerous or incomplete_def);

    if (mMergeType & eFanoutMerge) {
      merge_type = eFanoutMerge;
      always = findFanoutAlways(chain,no_sequential);
    }

    if (not always) {
      if (mMergeType & eFaninMerge) {
	merge_type = eFaninMerge;
	always = findFaninAlways(chain,no_sequential);
      }
    }

    if (not always and chain.size() > 1) {
      if ((mMergeType & eCreateMerge) or no_sequential) {
	// If this chain does not terminate with an always block, we
	// create a new always block for chains larger than 1 element.

	merge_type = eCreateMerge;

	// block name based on the last (causal) cont. assign in our chain.
	const SourceLocator & loc = (*(chain.rbegin()))->getLoc();
	StringAtom * block_name = mModule->newBlockName(mStrCache, loc);
	NUBlock * block = new NUBlock(NULL,
                                      mModule,
                                      mIODB,
                                      mNetRefFactory,
                                      false,
                                      loc);

	always = new NUAlwaysBlock(block_name,block,mNetRefFactory,loc, false);
	mModule->addAlwaysBlock(always);
      }
    }

    // 1-element chains which don't terminate with an always block are
    // left alone.
    if (always) {
      if (mVerbose) {
	printChain(chain,merge_type);
      }

      addChainToBlock(always,chain,merge_type);
      markChainForRemoval(chain);
      changed = true;
    }
  }
  return changed;
}


NUAlwaysBlock * UnelabModuleMerge::findFanoutAlways(Chain & chain, bool no_sequential)
{
  // Determine if the chain terminates at an always block.

  NUNetRefSet defs(mNetRefFactory);
  // The chain models data dependencies; only
  // consider the last chain element (chain is in causal order).
  NUUseDefNode * use_def = (*(chain.rbegin()));
  use_def->getDefs(&defs);
  NU_ASSERT(defs.size()==1,use_def);

  NUAlwaysBlock * always = NULL;

  if (defs.size()==1) {
    NUNetRefHdl net_ref = (*(defs.begin()));
    NUUseDefSet fanout_nodes;
    getFanout(net_ref,fanout_nodes);

    if (fanout_nodes.size()==1) {
      NUUseDefNode * fanout = (*(fanout_nodes.begin()));
      // This _might_ be an always block.
      always = dynamic_cast<NUAlwaysBlock*>(fanout);
    }
  }

  if (always) {
    if (definesProtectedMutableNet(chain)) {
      // If there are known external drivers, don't merge into an always block.
      always = NULL;
    }
  }

  if (always) {
    if (always->isSequential()) {
      if (no_sequential) {
        always = NULL;
      } else if (alwaysDefinesMemory(always)) {
        // Avoid sequential always blocks defining memories.
        always = NULL;
      }
    } else {
      if (createsCycle(chain,always)) {
        // don't create a combinational self-cycle
	always = NULL;
      }
    }
  }

  if (always) {
    // these nets are now updated sequentially; during other clock
    // edges, they could be inaccurate.
    if (always->isSequential()) {
      markChainInaccurate(chain);
    }
  }

  return always;
}


NUAlwaysBlock * UnelabModuleMerge::findFaninAlways(Chain & chain, bool no_sequential)
{
  // Determine if the chain is rooted at an always block.

  bool valid_always = true;
  bool has_input_bid_fanin = false;
  bool has_output_fanin = false;
  NUAlwaysBlock * always = NULL;

  // Collect all the use defs driving this chain. If there's just
  // one, yay!
  NUUseDefSet drivers;
  for (Chain::iterator iter = chain.begin();
       valid_always and iter != chain.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);

    NUNetRefSet uses(mNetRefFactory);
    use_def->getUses(&uses);

    for (NUNetRefSet::iterator net_iter = uses.begin();
	 valid_always and net_iter != uses.end();
	 ++net_iter) {
      NUNetRefHdl net_ref = (*net_iter);
      if (net_ref->empty()) continue;

      NUNet * net = net_ref->getNet();

      NUUseDefSet my_drivers;
      mUtility->getDefs(net_ref,my_drivers);

      drivers.insert(my_drivers.begin(),my_drivers.end());

      // Remember if any ports/edges fan into this chain.
      has_output_fanin |= (net->isOutput() or net->isBid());
      has_input_bid_fanin |= (net->isInput() or net->isBid());

      // Don't merge protected mutable nets because they may have a deposit or
      // force net on it.
      if (net->isProtectedMutable(mIODB))
	has_input_bid_fanin = true;

      // Don't merge protected observable nets because they have
      // external reads; don't change their timing.
      if (net->isProtectedObservable(mIODB))
	has_output_fanin = true;

      // Don't merge ports with their driving blocks -- it could
      // change their timing.
      if ((net->isPort()) and not my_drivers.empty() ) {
	valid_always = false;
      }
    }
  }

  if (valid_always and drivers.size()==1) {
    NUUseDefNode * fanin = (*(drivers.begin()));
    // This _might_ be an always block.
    always = dynamic_cast<NUAlwaysBlock*>(fanin);
  }

  if (always) {
    if (has_output_fanin) {
      // If we _use_ output fanin, don't merge into _any_ driving block.
      // There may be more drivers externally.
      always = NULL;
    } else if (definesProtectedMutableNet(chain)) {
      // If there are known external drivers, don't merge into an
      // always block. We don't want to retime something that's
      // forced. Users get grumpy.
      always = NULL;
    } else if (createsCycle(chain,always)) {
      // don't create a self-cycle
      always = NULL;
    }
  }

  if (always) {
    if (always->isSequential()) {
      if (has_input_bid_fanin) {
        // If we _use_ input/bid fanin, don't merge into sequential blocks.
        always = NULL;
      } else if (no_sequential) {
        // bug 4777 - do not move definitions of observable nets into
        // sequential blocks.
        // bug 5712 - do not move incomplete defs into sequential blocks.
        always = NULL;
      } else if (usesEdgeNet(chain)) {
        // If we _use_ clocks, don't merge into sequential blocks.
        always = NULL;
      } else if (alwaysDefinesMemory(always)) {
        // Avoid sequential always blocks defining memories.
        always = NULL;
      }
    }
  }

  return always;
}


bool UnelabModuleMerge::hasIncompleteDef(const Chain & chain) const
{
  for (Chain::const_iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    const NUUseDefNode * use_def = (*iter);
    const NUContAssign * assign  = dynamic_cast<const NUContAssign*>(use_def);

    NUNetRefSet defs(mNetRefFactory);
    NUNetRefSet complete_defs(mNetRefFactory);
    assign->getDefs(&defs);
    assign->getCompleteDefs(&complete_defs);

    // Only allow continuous assigns which completely define their
    // net-ref. This disqualifies moving dynamic-selecting writes into
    // sequential blocks (bug5712).
    //     assign d[i] = in;
    //     always @(posedge clk) q=d;
    if (defs != complete_defs) {
      return true;
    }
  }
  return false;
}


bool UnelabModuleMerge::alwaysDefinesMemory(NUAlwaysBlock * always)
{
  // Avoid merging into sequential always blocks which def
  // memories as this could cause StateUpdate problems (see
  // bug 513)
  NUNetSet always_defs;
  always->getDefs(&always_defs);
  for (NUNetSet::iterator def_iter=always_defs.begin();
       def_iter != always_defs.end();
       ++def_iter) {
    NUNet* net = *def_iter;
    if (net->is2DAnything()) {
      return true;
    }
  }
  return false;
}


bool UnelabModuleMerge::isDangerousChain(const Chain & chain) const
{
  return (definesOutputNet(chain) or 
          definesProtectedObservableNet(chain) or
          definesEdgeNet(chain));
}


bool UnelabModuleMerge::definesOutputNet(const Chain & chain) const
{
  for (Chain::const_iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    const NUUseDefNode * use_def = (*iter);

    NUNetRefSet defs(mNetRefFactory);
    use_def->getDefs(&defs);
    NU_ASSERT(defs.size()==1,use_def);

    const NUNetRefHdl net_ref = (*(defs.begin()));
    const NUNet * net = net_ref->getNet();

    // Don't merge outputs into sequential always blocks -- this
    // changes the timing of the output.
    if (net->isOutput() or
	net->isBid()) {
      return true;
    }
  }
  return false;
}


bool UnelabModuleMerge::definesProtectedObservableNet(const Chain & chain) const
{
  for (Chain::const_iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    const NUUseDefNode * use_def = (*iter);

    NUNetRefSet defs(mNetRefFactory);
    use_def->getDefs(&defs);
    NU_ASSERT(defs.size()==1,use_def);

    const NUNetRefHdl net_ref = (*(defs.begin()));
    const NUNet * net = net_ref->getNet();

    // Don't merge observable nets into sequential always blocks --
    // this changes the timing of the net.
    if (net->isProtectedObservable(mIODB)) {
      return true;
    }
  }
  return false;
}


bool UnelabModuleMerge::definesProtectedMutableNet(const Chain & chain) const
{
  for (Chain::const_iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    const NUUseDefNode * use_def = (*iter);

    NUNetRefSet defs(mNetRefFactory);
    use_def->getDefs(&defs);
    NU_ASSERT(defs.size()==1,use_def);

    const NUNetRefHdl net_ref = (*(defs.begin()));
    const NUNet * net = net_ref->getNet();

    // Don't merge depositable nets into fanout always blocks; a read
    // value may be inaccurate.
    if (net->isProtectedMutable(mIODB)) {
      return true;
    }
  }
  return false;
}


bool UnelabModuleMerge::definesEdgeNet(const Chain & chain) const
{
  for (Chain::const_iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    const NUUseDefNode * use_def = (*iter);

    NUNetRefSet defs(mNetRefFactory);
    use_def->getDefs(&defs);
    NU_ASSERT(defs.size()==1,use_def);

    const NUNetRefHdl net_ref = (*(defs.begin()));
    const NUNet * net = net_ref->getNet();

    if (net->isEdgeTrigger()) {
      return true;
    }
  }
  return false;
}


bool UnelabModuleMerge::usesEdgeNet(const Chain & chain) const
{
  for (Chain::const_iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    const NUUseDefNode * use_def = (*iter);

    NUNetRefSet uses(mNetRefFactory);
    use_def->getUses(&uses);

    for (NUNetRefSet::const_iterator net_iter = uses.begin();
	 net_iter != uses.end();
	 ++net_iter) {
      const NUNetRefHdl net_ref = (*net_iter);
      if (net_ref->empty()) continue;

      const NUNet * net = net_ref->getNet();
      if (net->isEdgeTrigger()) {
        return true;
      }
    }
  }
  return false;
}


bool UnelabModuleMerge::createsCycle(Chain & chain, NUAlwaysBlock * always) 
{
  NUNetRefSet chain_defs(mNetRefFactory);
  NUNetRefSet chain_uses(mNetRefFactory);
  bool creates_cycle = false;
  for (Chain::reverse_iterator iter = chain.rbegin();
       (not creates_cycle) and iter != chain.rend();
       ++iter) {
    NUUseDefNode * use_def = (*iter);

    NUNetRefSet my_uses(mNetRefFactory);
    use_def->getUses(&my_uses);

    // A cycle is when this member uses something that has already
    // been defined by the chain.

    creates_cycle = NUNetRefSet::set_has_intersection(my_uses, chain_defs);

    use_def->getDefs(&chain_defs);
    chain_uses.insert(my_uses.begin(),my_uses.end());
  }
 
  if (not creates_cycle and always) {
    NUNetRefSet always_defs(mNetRefFactory);
    NUNetRefSet always_uses(mNetRefFactory);
    always->getDefs(&always_defs);
    always->getUses(&always_uses);

    NUNetRefSet def_intersection(mNetRefFactory);
    NUNetRefSet use_intersection(mNetRefFactory);

    // A cycle is when the chain defines something the block uses _and_
    // vice-versa.
    creates_cycle = (NUNetRefSet::set_has_intersection(chain_uses, always_defs) and
                     NUNetRefSet::set_has_intersection(chain_defs, always_uses));
  }

  if (not creates_cycle and not (always and always->isSequential())) {
    NUNetRefSet external_uses(mNetRefFactory);
    NUNetRefSet::set_difference(chain_uses, chain_defs, external_uses);

    for (NUNetRefSet::iterator iter = external_uses.begin();
	 (not creates_cycle) and iter != external_uses.end();
	 ++iter) {
      NUNetRefHdl external_use_ref = (*iter);

      NUNetRefSet my_uses(mNetRefFactory);

      NUUseDefSet my_drivers;
      mUtility->getDefs(external_use_ref,my_drivers);
      for (NUUseDefSet::iterator ud_iter = my_drivers.begin();
	   ud_iter != my_drivers.end();
	   ++ud_iter) {
	NUUseDefNode * ud = (*ud_iter);
	NUAlwaysBlock * external_always = dynamic_cast<NUAlwaysBlock*>(ud);
	if ((not external_always) or (not external_always->isSequential())) {
	  ud->getUses(external_use_ref,&my_uses);
	}
      }

      creates_cycle = NUNetRefSet::set_has_intersection(chain_defs, my_uses);
    }
  }
  return creates_cycle;
}


void UnelabModuleMerge::markChainInaccurate(Chain & chain)
{
  // when adding nets to a sequential block, they are no longer
  // accurate for VCD dumping.
  for (Chain::iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    NUNetSet defs;
    use_def->getDefs(&defs);
    for (NUNetSet::iterator each = defs.begin();
	 each != defs.end();
	 ++each) {
      NUNet * net = (*each);
      net->putIsInaccurate(true);
    }
  }
}


void UnelabModuleMerge::addChainToBlock(NUAlwaysBlock * always,
					Chain & chain,
					ChainMergeType merge_type)
{
  promoteUD(chain,always);
  NUBlock * block = always->getBlock();
  switch(merge_type){

  case eFanoutMerge:
    {
      for (Chain::reverse_iterator iter = chain.rbegin();
	   iter != chain.rend();
	   ++iter) {
	block->addStmtStart(transformAssign((*iter)));
      }
    }
    break;

  case eCreateMerge: 
  case eFaninMerge: 
    {
      for (Chain::iterator iter = chain.begin();
	   iter != chain.end();
	   ++iter) {
	block->addStmt(transformAssign((*iter)));
      }
    }
    break;
  default:
    NU_ASSERT(0,always);
  }
  mStatistics->mark(chain.size());
}


void UnelabModuleMerge::promoteUD(Chain & chain, NUAlwaysBlock * always)
{
  for (Chain::iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    promoteUD((*iter),always);
  }
}


void UnelabModuleMerge::promoteUD(NUUseDefNode * node, NUAlwaysBlock * always)
{
  // add the use and def information to our new container.
  NUNetRefSet use_net_refs(mNetRefFactory);
  NUNetRefSet def_net_refs(mNetRefFactory);

  node->getDefs(&def_net_refs);
  node->getUses(&use_net_refs);

  for (NUNetRefSet::iterator def_iter = def_net_refs.begin();
       def_iter != def_net_refs.end();
       ++def_iter) {
    NUNetRefHdl def_net_ref = (*def_iter);
    always->addDef(def_net_ref);
    for (NUNetRefSet::iterator use_iter = use_net_refs.begin();
	 use_iter != use_net_refs.end();
	 ++use_iter) {
      NUNetRefHdl use_net_ref = (*use_iter);
      always->addUse(def_net_ref,use_net_ref);
    }
  }
}


NUBlockingAssign * UnelabModuleMerge::transformAssign(NUUseDefNode * use_def)
{
  NUContAssign * cont_assign = dynamic_cast<NUContAssign*>(use_def);
  NU_ASSERT(cont_assign,use_def);

  CopyContext copy_context(NULL,NULL);
  NULvalue * lvalue = cont_assign->getLvalue()->copy(copy_context);
  NUExpr * rvalue = cont_assign->getRvalue()->copy(copy_context);
  NUBlockingAssign * assign = new NUBlockingAssign (lvalue,
						    rvalue,
                                                    false,
						    cont_assign->getLoc());
  return assign;
}


void UnelabModuleMerge::markChainForRemoval(Chain & chain)
{
  for (Chain::iterator iter = chain.begin();
       iter != chain.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    mRemovedUseDefs.insert(use_def);
  }
}


void UnelabModuleMerge::update()
{
  if (! mRemovedUseDefs.empty()) {
    NUContAssignList assigns;

    for (NUModule::ContAssignLoop p(mModule->loopContAssigns()); !p.atEnd(); ++p) {
      NUContAssign* assign = *p;
      if (mRemovedUseDefs.find(assign) == mRemovedUseDefs.end()) {
        assigns.push_back(assign);
      }
      else {
        delete assign;
      }
    }
    mModule->replaceContAssigns(assigns);
  }
}


void UnelabMergeStatistics::mark(SInt32 size)
{
  ++mHistogram[size];
}


void UnelabMergeStatistics::add(UnelabMergeStatistics & other)
{
  mIterations += other.mIterations;
  for(Histogram::iterator iter = other.mHistogram.begin();
      iter != other.mHistogram.end();
      ++iter) {
    mHistogram[(*iter).first] += (*iter).second;
  }
}


void UnelabMergeStatistics::clear()
{
  mIterations = 0;
  mHistogram.clear();
}


void UnelabMergeStatistics::print(const char * label) const
{
  if (mHistogram.size()==0) {
    return;
  }
  UtIO::cout() << "UnelabMerge: Statistics: " << label << " (" << mIterations << " iterations)" << UtIO::endl;
  UtIO::cout() << "Size:  ";
  for(Histogram::const_iterator iter = mHistogram.begin();
      iter != mHistogram.end();
      ++iter) {
    UtIO::cout().setwidth(8);
    UtIO::cout() << (*iter).first << " ";
  }
  UtIO::cout() << UtIO::endl;
  UtIO::cout() << "Count: ";
  for(Histogram::const_iterator iter = mHistogram.begin();
      iter != mHistogram.end();
      ++iter) {
    UtIO::cout().setwidth(8);
    UtIO::cout() << (*iter).second << " ";
  }
  UtIO::cout() << UtIO::endl;
}


void UnelabFanout::print()
{
  UtIO::cout() << "Net: ";
  mNetRef->print(0);
  UtIO::cout() << "Fanout: " << UtIO::endl;
  for (NUUseDefSet::iterator iter = mFanout.begin();
       iter != mFanout.end();
       ++iter) {
    NUUseDefNode * use_def = (*iter);
    use_def->print(false,4);
  }
}

bool UnelabFanout::operator<(const UnelabFanout& that) const
{
  // compare nets by name cause this is for sorting
  int cmp = NUNetRef::compare(*mNetRef, *that.mNetRef, true);
  if (cmp == 0)
  {
    // The nets are equal.  Better compare the fanout sets.
    cmp = mFanout.size() - that.mFanout.size();
    if (cmp == 0)
    {
      // the sets are of equal size.  Iterate through them and compare
      NUUseDefSet::const_iterator p1 = mFanout.begin();
      NUUseDefSet::const_iterator e1 = mFanout.end();
      NUUseDefSet::const_iterator p2 = that.mFanout.begin();
      // already know the sets of are equal size, only need to check 1 end
      for (; (cmp == 0) && (p1 != e1); ++p1, ++p2)
      {
        const NUUseDefNode* ud1 = *p1;
        const NUUseDefNode* ud2 = *p2;
        cmp = NUUseDefNode::compare(ud1, ud2);
      }
      if (cmp == 0)
        cmp = carbonPtrCompare(this, &that);
    }
  }
  return cmp < 0;
} // bool UnelabFanout::operator<
