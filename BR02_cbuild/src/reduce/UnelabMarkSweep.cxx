// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/UnelabMarkSweep.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "flow/FLFactory.h"
#include "iodb/IODBNucleus.h"

/*!
  \file
  Implementation of unelaborated flow mark/sweep package.
 */


void UnelabMarkSweep::mark(NUDesign *this_design)
{
  clearMarkAllFlow(mFlowFactory);
  design(this_design);
}


void UnelabMarkSweep::mark(NUModule *this_module, bool recurse)
{
  clearMarkAllFlow(mFlowFactory);
  module(this_module, recurse);
}


void UnelabMarkSweep::clearMarkAllFlow(FLNodeFactory * flow_factory)
{
  for (FLNodeFactory::iterator iter = flow_factory->begin();
       iter != flow_factory->end();
       ++iter) {
    FLNode *flnode = *iter;
    flnode->putMark(false);
  }
}


void UnelabMarkSweep::sweep(SweepCallback &callback)
{
  for (FLNodeFactory::iterator iter = mFlowFactory->begin();
       iter != mFlowFactory->end();
       ++iter) {
    FLNode *flnode = *iter;
    callback(flnode, not flnode->getMark());
  }
}


void UnelabMarkSweep::module(NUModule *this_module)
{
  module(this_module, true);
}


void UnelabMarkSweep::module(NUModule *this_module, bool recurse)
{
  if (haveVisited(this_module)) {
    return;
  }

  markModuleFlow(this_module);

  if (recurse) {
    for (NUModuleInstanceMultiLoop iter = this_module->loopInstances();
	 not iter.atEnd();
	 ++iter) {
      NUModuleInstance *inst = *iter;
      module(inst->getModule());
    }
  }

  rememberVisited(this_module);
}


void UnelabMarkSweep::markModuleFlow(NUModule *module)
{
  NUNetList start_list;
  if (mStartAtAllNets) {
    module->getAllNets(&start_list);
  } else {
    module->getFaninStartNets(&start_list, mIODB);
  }

  markNetFlow(&start_list);
}


//! Print the bit numbers of the bits which are set in vect.
#define DEBUG_MARK_SWEEP 0
#if DEBUG_MARK_SWEEP
static void sPrintBits(const DynBitVector &vect)
{
  UtString buf;
  if (vect.count() != vect.size()) {
    buf << "[";
    vect.composeBits(&buf);
    buf << "]";
  }
}

static void printFlow(FLNode* flow) {
  NUNetRefHdl ref = flow->getDefNetRef();
  DynBitVector bv;
  ref->getUsageMask(&bv);
  UtIO::cout() << ref->getNet()->getName()->str();
  sPrintBits(bv);
  
  NUUseDefNode* ud = flow->getUseDefNode();
  if (ud != NULL) {
    UtIO::cout() << " " << ud->typeStr() << " ";
    UtString buf;
    ud->getLoc().compose(&buf);
    UtIO::cout() << buf;
  }
}
#endif

void UnelabMarkSweep::markNetFlow(NUNetList * start_list)
{
  FLNetIter flow_iter(start_list,
		      FLIterFlags::eAll,            // visit all nets
		      FLIterFlags::eNone,           // stop at no nets
		      FLIterFlags::eBranchAtAll,    // branch at all nesting
		      FLIterFlags::eIterNodeOnce);  // visit each node only once

  for (; not flow_iter.atEnd(); ++flow_iter) {
    FLNode *flnode = *flow_iter;
    flnode->putMark(true);
#if DEBUG_MARK_SWEEP
    FLNode* fanout = flow_iter.getCurParent();
    UtIO::cout() << flnode << " ";
    if (fanout == NULL)
      UtIO::cout() << "(OUT) <- ";
    else {
      printFlow(fanout);
      UtIO::cout() << " <- ";
    }
    printFlow(flnode);
    UtIO::cout() << "\n";
#endif
  }
}
