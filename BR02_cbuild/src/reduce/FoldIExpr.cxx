// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Simple constant folding pass.  Walk the design folding all expression
 * trees into simpler forms.
 */

#include <numeric>
#include <functional>
#include <cmath>
#include "FoldI.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUSysFunctionCall.h"

#include "codegen/carbon_priv.h"
#include "util/CarbonRunTime.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "reduce/Congruent.h"

// When folding certain expressions, we try to fold the pieces and if they all succeed, we
// can fold the expression.  If any of the pieces *fail* to fold, we have to undo the folding
// operations  [See FoldI::foldPartsel() for an example].
//
void FoldI::cleanupExpr (const NUExpr* e)
{
  if (e && !e->isManaged ())
    delete const_cast<NUExpr*>(e);                   // not_managed_expr
}

// Is this a ~ expression?
//
bool
FoldI::isComplement (const NUOp *u)
{
  return (u != NULL)
    && (u->getOp () == NUOp::eUnBitNeg
        || u->getOp () == NUOp::eUnVhdlNot
        || (u->getOp () == NUOp::eUnLogNot && u->getBitSize () == 1
            && u->getArg (0)->getBitSize () == 1)
      );
}

// Exchange the relational operators when we swap operands
static NUOp::OpT sReverseOp (NUOp::OpT opcode)
{
  switch (opcode)
    {
    case NUOp::eBiTrieq:
    case NUOp::eBiTrineq:
    case NUOp::eBiNeq:
    case NUOp::eBiEq:	return opcode;
    case NUOp::eBiSLt:	return NUOp::eBiSGtr;
    case NUOp::eBiSGtr:	return NUOp::eBiSLt;
    case NUOp::eBiSGtre:return NUOp::eBiSLte;
    case NUOp::eBiSLte:	return NUOp::eBiSGtre;
    case NUOp::eBiULt:	return NUOp::eBiUGtr;
    case NUOp::eBiUGtr:	return NUOp::eBiULt;
    case NUOp::eBiUGtre:return NUOp::eBiULte;
    case NUOp::eBiULte:	return NUOp::eBiUGtre;
    default:		return opcode;
    }
}

// Exchange the relational operators when we invert the operation,
// e.g.  !(a==0) => (a!=0),     !(a>b) => (a <= b)
static NUOp::OpT sInverseOp (NUOp::OpT opcode)
{
  switch (opcode)
    {
    case NUOp::eBiTrieq:return NUOp::eBiTrineq;
    case NUOp::eBiTrineq:return NUOp::eBiTrieq;
    case NUOp::eBiNeq:	return NUOp::eBiEq;
    case NUOp::eBiEq:	return NUOp::eBiNeq;
    case NUOp::eBiSLt:	return NUOp::eBiSGtre;
    case NUOp::eBiSGtr:	return NUOp::eBiSLte;
    case NUOp::eBiSGtre:return NUOp::eBiSLt;
    case NUOp::eBiSLte:	return NUOp::eBiSGtr;
    case NUOp::eBiULt:	return NUOp::eBiUGtre;
    case NUOp::eBiUGtr:	return NUOp::eBiULte;
    case NUOp::eBiUGtre:return NUOp::eBiULt;
    case NUOp::eBiULte:	return NUOp::eBiUGtr;
    default: INFO_ASSERT (0, "Unsupported relational operator");
    }

  return opcode;
}

static bool sIsRshift (NUOp::OpT opcode)
{
  switch (opcode) {
  case NUOp::eBiRshift:
  case NUOp::eBiRshiftArith:
  case NUOp::eBiVhRshift:
  case NUOp::eBiVhRshiftArith:
    return true;
  default:
    return false;
  }
}

static bool sIsLshift (NUOp::OpT opcode)
{
  switch (opcode) {
  case NUOp::eBiLshift:
  case NUOp::eBiLshiftArith:
  case NUOp::eBiVhLshift:
  case NUOp::eBiVhLshiftArith:
    return true;
  default:
    return false;
  }
}

// Return the width of the bitmask that \a value represents.  If it's not
// a solid right-aligned mask, then return zero.
//
static UInt32
sBitMask (const DynBitVector& value)
{
  // count all the bits and then check that it's a continuous
  UInt32 count = value.count ();

  if (count == value.size ())
    // All ones, don't waste time looking any harder...
    return count;

  DynBitVector rest (value.size (), value);

  rest >>= count;

  return (rest == 0) ? count : 0;
}

//! is \a value a mask of at least \a k ones?
static bool
sAllOnes (const DynBitVector& value, UInt32 k)
{
  if (k > value.size ())
    return false;		// zero extending, so not all ones!
  return value.isRangeOnes (0,k);
}

//! is \a value a mask of \a k zeros?
static bool
sAllZeroes (const DynBitVector& value, size_t k)
{
  k = std::min (value.size (), k); // Don't have to look for extra zeros
  return value.isRangeZero (0,k);
}

// Helper function to extend an expression with explicit zeros or sign copies.
// This is different from NUExpr::makeSizeExplicit() in that it only affects the top
// level expression.  For example, consider the expression:
//         a+b   ;; size=8, context=32
// sPadExpr yields
//      {24'b0, a+b}
// while makeSizeExplicit yields
//      {24'b0, a} + {24'b0, b}
//
// In particular, you call sPadExpr when you don't want to change the self-determined
// size of the underlying expression terms, just change the SIZE of the whole expression
// to preserve the original self-determined size.
//
const NUExpr* FoldI::padExpr(const NUExpr* c, UInt32 totWidth) {
  NUCExprVector pieces;
  UInt32 csize = c->determineBitSize ();
  const SourceLocator& loc = c->getLoc ();

  if (csize > totWidth) {
    // More bits than we need, partselect the ones we need.
    return varsel(c, ConstantRange (totWidth-1, 0), loc);
  } else if (csize == totWidth)
    // Exactly right
    return c;

  // Given smaller self-determined size than we need enforced.  Prefix with
  // copies of sign or with zero.
  if (c->isSignedResult ()) {
    CopyContext cc(NULL, NULL);
    ConstantRange r (csize-1, csize-1);
    const NUExpr* sign = varsel(c, r, loc);
    pieces.push_back (sign);

    // This should use SXT, rather than replicating references to 'sign' expr
    UInt32 sign_size = totWidth - csize;
    sign = concatOp(pieces, sign_size, loc, sign_size);

    pieces[0] = sign;           // Reusing pieces array for { k{c[msb]}, c}
  } else {
    const NUExpr* zero = FoldI::genFalse (c->getLoc (), totWidth - csize, false);
    pieces.push_back(zero);
  }

  c = mExprCache->resize(c, csize);
  pieces.push_back (c);
  c = concatOp(pieces, 1, c->getLoc (), totWidth);
  return c;
} // const NUExpr* FoldI::padExpr


// Is this an expression that is guaranteed to have some high-bits set to ones?
bool FoldI::wideningTwiddle (const NUExpr *e)
{
  const NUOp *v = dynamic_cast<const NUOp *>(e);

  // In new sizing, we would transform:
  //   (2 < ~a8) --> (2 < ~{24'b0,a8})
  //             --> (2 < {~24'b0,~a8})
  //             --> (2 < {24'b111111111111111111111111,~a8})
  //
  // This occurs in test/fold/bug1504.v, for o[12]
  const NUConcatOp* cat = dynamic_cast<const NUConcatOp*>(e);
  if (cat != NULL) {
    const NUConst* k = cat->getArg(0)->castConst();
    if (k != NULL) {
      DynBitVector val;
      k->getSignedValue(&val);
      if (val.test(val.size() - 1)) {
        return true;
      }
    }
  }

  if (!v || v->getOp () != NUOp::eUnBitNeg)
    return false;

  if (v->getBitSize () > getCachedEffectiveBitSize(v->getArg(0)))
    return true;

  return false;
}

// expressions like (bool ^ const32) are always non-zero
bool FoldI::alwaysNonZero (const NUExpr* e)
{
  // If it's a constant and it is not zero, then we win.  Get the easy ones!
  const NUConst* k = e->castConst();
  if ((k != NULL) && !k->isZero()) {
    return true;
  }

  if (e->isSignedResult ())
    return false;
  const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(e);
  if (not bop)
    return wideningTwiddle (e);

  const NUConst *c = bop->getArg (1)->castConst ();
  
  NUOp::OpT opc = bop->getOp ();

  UInt32 ropWidth = getCachedEffectiveBitSize(bop->getArg (1));
  UInt32 lopWidth = getCachedEffectiveBitSize(bop->getArg (0));

  switch (opc) {
  case NUOp::eBiBitXor:
    return ((wideningTwiddle (bop->getArg (0))
             && (lopWidth > ropWidth)) // some high bits will always be set....
            || (c && (not c->isZero ())
                && lopWidth < ropWidth)); // the constant has more bits set than the RHS

  case NUOp::eBiBitOr:
    return c && not c->isZero (); //  guaranteed non-zero with constant non-zero argument

  case NUOp::eBiPlus:
    // Hard to do this one because carry-propagation can zero value.
    // to guarantee, need to note that there are zeros (carry-stops) in
    // bits beyond the getCachedEffectiveBitSize of the lop.

  default:
    return false;
  }
}

const NUExpr*
FoldI::genOnes (const SourceLocator& loc, UInt32 width, bool sign)
{
  DynBitVector v (width);
  v.set ();
  return constant(sign, v, width, loc);
}

const NUExpr*
FoldI::genTrue (const SourceLocator& loc, UInt32 width, bool sign)
{
  return constant(sign, 1, width, loc);
}

const NUExpr*
FoldI::genFalse (const SourceLocator& loc, UInt32 width, bool sign)
{
  return constant(sign, 0, width, loc);
}

bool
FoldI::isPrimary (const NUExpr* e)
{
  switch (e->getType ()) {
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUIdentRvalueElab:
  case NUExpr::eNUVarselRvalue:
  case NUExpr::eNUMemselRvalue:
  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ:
    return true;
  default:
    return false;
  }
}

#if 0
// Is e an expression with a boolean value
static bool
sIsBoolean (const NUExpr* e)
{
  if (const NUOp* opr = dynamic_cast<const NUOp*>(e))
    switch (opr->getOp ()) {
    case NUOp::eBiTrieq:
    case NUOp::eBiTrineq:
    case NUOp::eBiNeq:
    case NUOp::eBiEq:
    case NUOp::eBiSLt:
    case NUOp::eBiSLte:
    case NUOp::eBiSGtr:
    case NUOp::eBiSGtre:
    case NUOp::eBiULt:
    case NUOp::eBiULte:
    case NUOp::eBiUGtr:
    case NUOp::eBiUGtre:
    case NUOp::eBiLogAnd:
    case NUOp::eBiLogOr:
    case NUOp::eUnRedOr:
    case NUOp::eUnRedAnd:
    case NUOp::eUnRedXor:
    case NUOp::eUnRedXnor:
    case NUOp::eUnLogNot:
      return true;
    default:
      return false;
    }

  return false;
}

/*!
 * Given a partselect, return the real bounds of the "object" we're
 * selecting from.  This handles partselects of an expression as well
 * as simple partselects of VectorNets.
 *
 * Inputs
 *	\a ps the partselect we're examining
 * Outputs
 *	\return ConstantRange that describes the whole object and is suitable
 *	as an argument to ConstantRange::normalize()
 */

static ConstantRange
sGetPartselBounds (const NUVarselRvalue* ps)
{
  const NUExpr* psident = ps->getIdentExpr ();

  if (psident->isWholeIdentifier ())
    // x[a:b]
    {
      const NUVectorNet* vn = dynamic_cast<const NUVectorNet*>(psident->getWholeIdentifier ());
      NU_ASSERT (vn, psident);
      return *vn->getRange ();
    }
  else if (const NUVarselRvalue* subps = dynamic_cast<const NUVarselRvalue*>( psident))
    // x[a:b][c:d]
    return *subps->getRange ();
  else
    return ConstantRange (psident->getBitSize () - 1, 0);
}
#endif

/*!
 * Given an indexing expression - if it's of the form
 *    e +/- c   , where c != 0.
 * return the expression 'e' and the constant 'c'
 * otherwise return false (indicating no constant term.)
 */
bool FoldI::getConstantOffset (const NUExpr* index, const NUExpr** term,
                               const NUConst**offset, bool insideVarsel)
{
  if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(index)) {
    if (bop->getOp () == NUOp::eBiPlus) {
      const NUExpr * arg0 = bop->getArg(0);
      const NUExpr * arg1 = bop->getArg(1);
      if (const NUConst* cOffset = arg1->castConst ()) {
        if (not cOffset->isZero ()) {
          if ((not insideVarsel) or 
              // Indexing operations are restricted to 32-bits.
              (index->getBitSize() == LONG_BIT) or
              // This constant produces fewer bits than our context
              // size and will not produce overflow [bug 6478].
              (index->getBitSize() >
               std::max(arg0->determineBitSize(),
                        getCachedEffectiveBitSize(arg1))))
          {
            if (term)
              *term = arg0;
            if (offset)
              *offset = cOffset;
            return true;
          }
        }
      }
    }
  }

  // No appropriate expression found
  return false;
}

// Check that operand is a non-trivial relational operation
// bit identities are not good choices...
bool FoldI::isGoodBoolRelOp (const NUExpr* expr)
{
  if (getCachedEffectiveBitSize(expr) != 1)
    // Boolean expressions can be resized, so use effective number of bits
    return false;

  if (const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(expr))
    // Accept '!(x<5)' as a good candidate
    return FoldI::isComplement (uop) && isGoodBoolRelOp (uop->getArg (0));

  
  const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(expr);
  if (not bop)
    return false;

  NUOp::OpT opcode = bop->getOp ();
  if (not FoldI::isRelationalOp (opcode)
      || ((opcode == NUOp::eBiEq
           || opcode == NUOp::eBiNeq
           || opcode == NUOp::eBiTrieq
           || opcode == NUOp::eBiTrineq)
          && bop->getArg (0)->getBitSize () == 1
          && bop->getArg (1)->getBitSize () == 1))
    return false;

  return true;
}

/*!
 * \verbatim 
 Given an expression BOOLRES <op> EXPR, it might be more useful to
 compute as
      BOOLRES ? 1 <op> EXPR : 0 <op> EXPR
 This will be especially true of EXPR is constant because we can
 apply distributivity further up the expression tree as well.
 Don't do this if the cost of the EXPR term is large, as we're
 unlikely to recoup it.
 \endverbatim 
 */
const NUExpr* FoldI::reduceBoolOpExpr (const NUBinaryOp* bop) {
  NUOp::OpT opcode = bop->getOp ();
  if (bop->getBitSize () == 1
      || opcode == NUOp::eBiVhExt // Don't break up SXT/ZXT nodes!
      || opcode == NUOp::eBiVhZxt)
    return NULL;

  const SourceLocator& loc = bop->getLoc ();
  const NUExpr*lop = bop->getArg (0);
  const NUExpr*rop = bop->getArg (1);

  if (const NUConst* rc = ctce (rop))
  {
    DynBitVector rv;
    rc->getSignedValue (&rv);

    // Check some e <op> mask cases
    UInt32 lsize = getCachedEffectiveBitSize(lop);

    switch (opcode) {
    case NUOp::eBiBitOr:
      if (rv.partsel (0, lsize).all ())    // constant term has all bits set that
        return copyExpr (rop);  	   // could possibly matter
      break;
    case NUOp::eBiBitAnd:
      if (not rv.partsel (0, lsize).any()) // constant term is all zeros in bits that
        return genFalse (loc); 		   // could possibly matter
      break;
    default:
      break;
    }
  }
  
  // Don't let & get past this point, or we could oscillate converting ?: to and from
  // & trees.
  if (opcode == NUOp::eBiBitAnd)
    return NULL;

  NUCost cost;
  NUCostContext costCtx;

  if (isGoodBoolRelOp (lop)) {
    rop->calcCost (&cost, &costCtx);
    if (cost.mInstructions < 8) {
      return ternaryOp(copyExpr (lop),
                              binaryOp(opcode,
                                              genTrue (loc, 1, false),
                                              copyExpr (rop), loc),
                              binaryOp(opcode,
                                              genFalse (loc, 1, false),
                                              copyExpr (rop), loc),
                              loc);
    }
  }

  if (isGoodBoolRelOp (rop)) {
    lop->calcCost (&cost, &costCtx);
    if (cost.mInstructions < 8) {
      return ternaryOp(copyExpr (rop),
                              binaryOp(opcode,
                                              copyExpr (lop),
                                              genTrue (loc, 1, false), loc),
                              binaryOp(opcode,
                                              copyExpr (lop),
                                              genFalse (loc, 1, false), loc),
                              loc);
    }
  }

  return NULL;
} // NUExpr* FoldI::reduceBoolOpExpr

// Protect an index expression by forcing a +0 onto it to replace
// the non-zero constant we removed.
//
// TODO: If the index expression's self-determined and effective sizes are
// as large as the context-size, then we don't need to add zero.
//
// Similarly, we can fold away a zero term if the object size isn't affected by this.
// c.f.  (byte + byte + 9'b0)  => zero can't be folded away because (byte+byte) is self-determined
// as zero (and the effective bitsize is 9 bits...)
//
const NUExpr* FoldI::protectIndexExpr (const NUExpr* index)
{
  UInt32 width = index->getBitSize ();
  bool sign = index->isSignedResult ();
  const NUExpr* zero = FoldI::genFalse (index->getLoc (), width, sign);
  index = binaryOp(NUOp::eBiPlus, index, zero,index->getLoc (), width,
                   specifySign(sign));
  return index;
}

/*!
 * Get the constant associated with this net, optionally collapsing
 * XZ constants into their masked values (e.g. ignoring the X's.)
 */
NUConst*
FoldI::ctce (NUExpr* e) const
{
  if (!e)
    return 0;

  if (ignoreXZ () or not e->drivesZ ())
    return e->castConst ();
  else 
    return e->castConstNoXZ();
}

/*!
 * Get the constant associated with this net, optionally collapsing
 * XZ constants into their masked values (e.g. ignoring the X's.)
 */
const NUConst* FoldI::ctce (const NUExpr* e) const
{
  if (!e)
    return 0;

  if (ignoreXZ () or not e->drivesZ ())
    return e->castConst ();
  else
    return e->castConstNoXZ ();
}

//! Is this tree a simple N-ary PLUS, XOR, AND, or OR of single bit field selects?
static bool sIsOpOver (NUOp::OpT opcode, NUCExprVector& operands,
                       const NUExpr *tree)
{
  switch (tree->getType ()) {
  case NUExpr::eNUBinaryOp:
  {
    const NUBinaryOp* exp = dynamic_cast<const NUBinaryOp*>(tree);
    if (exp->getOp () != opcode)
      return false;

    return sIsOpOver (opcode, operands, exp->getArg (0))
      & sIsOpOver (opcode, operands, exp->getArg (1));
  }
  break;

  case NUExpr::eNUVarselRvalue:
  {
    const NUVarselRvalue* ps = dynamic_cast<const NUVarselRvalue*>(tree);

    if (ps->getRange ()->getLength () != 1 || not ps->isConstIndex ())
      return false;

    operands.push_back (ps);
    return true;
  }

  default:
    return false;
  }
}

// Determine if parallel bit operations are likely to be a win.
// Only expect +,^,|,&
static bool sReductionFeasible (NUOp::OpT opcode, NUCExprVector& operands)
{

  // The same bit could have been present multiple times.  For
  // AND or OR operators, this isn't a problem.  But for XOR, we
  // must eliminate an even number of repeats or we get the wrong
  // answer, and for PLUS, we have to reject the reduction outright....
  //
  typedef UtSet<const NUExpr*, BitRefOrder> OpSetT;
  OpSetT opSet;
  for (NUCExprVector::const_iterator i = operands.begin ();
       i != operands.end ();
       ++i)
  {
    const NUExpr* operand = *i;

    if (const NUVarselRvalue* varsel =
        dynamic_cast<const NUVarselRvalue*>(operand))
    {
      // reject any varsel which is not of a whole identifier
      if (!varsel->getIdentExpr()->isWholeIdentifier())
        return false;
    }

    OpSetT::iterator p = opSet.find(operand);
    if (p == opSet.end ())
      // Not already in the set of unique operands
      opSet.insert (*i);
    else if (opcode == NUOp::eBiPlus)
    {
      // Can't handle redundant terms in plus! Reject this out-of-hand.
      operands.clear ();
      return false;
    }
    else if (opcode == NUOp::eBiBitXor)
      opSet.erase (p);          // Even # of terms cancels
  }

  // Take the non-redundant operands out of the set
  operands.clear ();
  std::copy (opSet.begin (), opSet.end (), std::back_inserter (operands));

  UInt32 count = operands.size ();
 
  return (count >= 2);
}

// TRUE iff mask is dense right-aligned field of 1s
static bool sIsIntMask (UInt32 k)
{
  if (k == 0)
    return false;

  // Dense mask iff (k+1) == 2**N.
  ++k;

  return (k & -k) == k;
}

// Return position of leftmost 1 in mask
static UInt32 sMaskWidth (UInt32 k)
{
  UInt32 i;
  if (k==0)
    return 0;

  for(i=0; k!=0; k>>=1, ++i) ;
  return i;
}


// Construct a reduction operator of the selected bits
// v[0]^v[1]^...v[k] => popcount((v.M_w[0] & mask[0])^(v.M_w[1]& mask[1])...)&1
// v[0]+v[1]+...v[k] => popcount(...)
// v[0]&v[1]^...v[k] => (v&mask) == mask
// v[0]|v[1]|...v[k] => (v&mask) != 0
//
const NUExpr* FoldI::genReduction (NUOp::OpT newOp, const NUExpr* identExpr,
                                   NUNet *net, DynBitVector *mask, bool *noR)
{
  NUVectorNet *vn = dynamic_cast<NUVectorNet*>(net);
  const NUExpr *p = NULL;

  ConstantRange vnPhysBounds = *(vn->getRange ());
  vnPhysBounds.normalize (vn->getRange ());
  bool allAligned = true;
  // Do 32 bits at a time...
  for(UInt32 offset = 0;
      offset < mask->size ();
      offset += LONG_BIT, *mask >>= LONG_BIT)
    {
      UInt32 maskWordVal = mask->value ();

      if (maskWordVal == 0)	// No bits needed from this chunk?
	continue;

      bool noBitAnd = sIsIntMask (maskWordVal);
      allAligned = allAligned && (maskWordVal == 1);
      UInt32 fieldWidth = noBitAnd ? sMaskWidth (maskWordVal) : LONG_BIT;

      // Construct a [j:k] range for the current word
      ConstantRange r (offset + fieldWidth - 1, offset);
      r = r.overlap (vnPhysBounds);
      r.denormalize (vn->getRange ());

      CopyContext cc (NULL,NULL);
      const NUExpr* newp = varsel(copyExpr (identExpr), r, net->getLoc (),
                                  r.getLength ());

      if (not noBitAnd)
	// Not a dense aligned mask of bits for this chunk, mask it off.
	{
	  const NUConst *c = constant(false, maskWordVal, r.getLength (), net->getLoc ());
	  newp = binaryOp(NUOp::eBiBitAnd, newp, c, net->getLoc ());
	}

      if (newOp == NUOp::eUnRedAnd)
	// all selected bits must be set, compare against fieldmask
	{
	  const NUConst *c = constant(false, maskWordVal, r.getLength (), net->getLoc ());
	  newp = binaryOp(NUOp::eBiEq, newp, c, newp->getLoc ());
	}
      else if (newOp == NUOp::eUnCount)
	// popcount this word
	newp = unaryOp(newOp, newp, newp->getLoc ());

      if (p)
	switch (newOp)
	  {
	  case NUOp::eUnRedAnd:
            p = binaryOp(NUOp::eBiLogAnd, newp, p, p->getLoc (), 1);
	    break;

	  case NUOp::eUnRedXor:
	    p = binaryOp(NUOp::eBiBitXor, p, newp, p->getLoc (), LONG_BIT);
	    break;

	  case NUOp::eUnCount:
	    p = binaryOp(NUOp::eBiPlus, p, newp, p->getLoc (), LONG_BIT);
	    break;

	  case NUOp::eUnRedOr:
	    p = binaryOp(NUOp::eBiBitOr, p, newp, p->getLoc (), LONG_BIT);
	    break;

	  default:
	    NU_ASSERT ("Unknown ReduceOp" == 0, p);
	  }
      else
	{
	  p = mExprCache->resize(newp, newp->determineBitSize ());
	}
    }
  *noR = allAligned;
  *mask = 0;	// Reset the mask
  return p;
}


//! Return the number of non-zero words in the given bitvector.
static UInt32 sCountNonZeroWords (const DynBitVector &bv)
{
  UInt32 count = 0;
  const UInt32 *array = bv.getUIntArray();
  for (UInt32 idx = 0; idx < bv.numWords(); ++idx) {
    if (array[idx] != 0) {
      ++count;
    }
  }
  return count;
}


const NUExpr* FoldI::reduceOpOver(const NUBinaryOp *tree) {
  NUCExprVector operands;	// compatible operands

  NUOp::OpT oldOp = tree->getOp ();

  if (oldOp != NUOp::eBiBitXor
      && oldOp != NUOp::eBiPlus
      && oldOp != NUOp::eBiBitAnd
      && oldOp != NUOp::eBiBitOr)
    return 0;			// No optimization possible.

  // Check that this is a complete subtree of the same operator.

  if (sIsOpOver (oldOp, operands, tree)
      && sReductionFeasible (oldOp, operands))
    // transform
    {
      // Which reduction operator we end up with?
      NUOp::OpT newOp = NUOp::eUnRedXor; // silence gcc on sol7
    
      switch (tree->getOp ())
      {
        case NUOp::eBiPlus: newOp = NUOp::eUnCount; break;
        case NUOp::eBiBitXor: newOp = NUOp::eUnRedXor; break;
        case NUOp::eBiBitAnd: newOp = NUOp::eUnRedAnd; break;
        case NUOp::eBiBitOr:  newOp = NUOp::eUnRedOr;  break;
        default: NU_ASSERT ("Unknown ReduceOp" == 0, tree); break;
      }

      NUCExprVector partials;

      NUNet *v  = 0;
      const NUExpr* identExpr = NULL;
      DynBitVector mask (1);	// zero-width causes troubles.

      bool all_aligned = false;

      // Keep track if we have reduced the number of word accessors.
      // If we have, then we will keep the result.  Otherwise, we will
      // get endless recursion by interacting with other folds.
      bool reduced_word_accessors = false;
      UInt32 num_accessors = 0;

      for(NUCExprVector::iterator i = operands.begin ();
          i != operands.end ();
          ++i, ++num_accessors)
        // build a constant mask
      {
        const NUVarselRvalue* ps = dynamic_cast<const NUVarselRvalue*>(*i);
        NU_ASSERT (ps->isConstIndex (), ps);
        const NUExpr* nextIdentExpr = ps->getIdentExpr();

        if ((identExpr == NULL) || (*nextIdentExpr != *identExpr))
          // Finish the current reduction and start a new one.
        {
          bool no_red = false;
          if (v) {		// This isn't the first one...
            reduced_word_accessors |= (num_accessors > sCountNonZeroWords(mask));
            partials.push_back (genReduction (newOp, identExpr, v, &mask, &no_red));
            // Initialize
            if (partials.size () == 1 ) {
              all_aligned = no_red;
            }
            else {
              all_aligned = all_aligned && no_red;
            }
          }
          mask = 0;
          num_accessors = 0;

          v = ps->getIdent ();
          identExpr = nextIdentExpr;
        }
	  
        // Convert bitreference to mask entry
        // Make it large enough to address this bit
        ConstantRange range = *ps->getRange ();
        UInt32 new_size = range.getLsb () + range.getLength ();
        if (new_size > mask.size()) {
          mask.resize (new_size);
        }
        mask.set (range.getLsb (), range.getLength ());
      } // for

      // The last match didn't get put into the partials list
      bool no_red = false;
      reduced_word_accessors |= (num_accessors > sCountNonZeroWords(mask));
      partials.push_back (genReduction (newOp, identExpr, v, &mask, &no_red));

      // Initialize
      if (partials.size () == 1 ) {
        all_aligned = no_red;
      }
      else {
        all_aligned = all_aligned && no_red;
      }

      // Now take the partials and construct the reduction over each piece
      // XOR can xor the pieces in parallel
      // AND must do a logical and of the pieces
      // PLUS must sum the partial popcounts
      // OR  can or the pieces in parallel
      while (partials.size () > 1)
      {
        const NUExpr* lop = partials.back (); partials.pop_back ();
        const NUExpr* rop = partials.back (); partials.pop_back ();
        
        lop = binaryOp(oldOp, lop, rop, lop->getLoc ());
        partials.push_back (lop);

        // Do not try to resize here, because the result->resize
        // call below does all that is needed.  In particular,
        // resize this partial combination to rop's size
        // is wrong!  Consider this case from test/cust/amcc/3450/gmacenv,
        // dw_crc_check.v:38:
        //    (crc[31:0] & 32'h41000000) ^ (in[7:0] & 8'h82)
        // resize lop to rop's size turns that 32-bit constant
        // to 0, and gives wrong answers.  The right thing
        // at this point would be to call lop->resize(lop->determineBitSize())
        // but that's superfluous because the red-xor resize will do that
        // below when result->resize(tree->getBitSize()) is called.
      }
      
      const NUExpr* result = partials.back ();

      if (newOp == NUOp::eUnRedXor
          || newOp == NUOp::eUnRedOr)
        result = unaryOp(newOp, result, result->getLoc ());
      else if (newOp == NUOp::eUnCount)
        // Restrict the size of the sum to be the same as the size of the
        // serial tree we started with.
        result = padExpr (result, tree->getBitSize ());

      result = mExprCache->resize(result, tree->getBitSize ());

      if (!all_aligned && reduced_word_accessors) {
#if 0
        // Special dump code..
        UtString buf ("\nReduction\t:");
        tree->compose (&buf, NULL);
        buf << "\t\t";
        buf << "\nBecomes:\t";
        result->compose (&buf, NULL);
        UtIO::cout () << buf << "\n\n";
#endif
        return mExprCache->makeSizeExplicit(result);
      } else {
        FoldI::cleanupExpr (result);
        return 0;
      }
    }
  return 0;
}

// Simplify  e1 & mask(k) into a partselect if possible - should do constants as well!
const NUExpr*
FoldI::reducePartsel (const NUExpr *e, UInt32 width)
{
  if (width == 0)
    // degenerate case
    return genFalse (e->getLoc ());

  switch (e->getType ())
    {
    case NUExpr::eNUVarselRvalue:
      // X[h:l] & mask
      {
	const NUVarselRvalue *ps = dynamic_cast<const NUVarselRvalue*>(e);

	ConstantRange r(*(ps->getRange ())); // bounds of this reference
	
	if (r.getLength () <= width)
	  // physical object is smaller than mask, just return a copy
	  return e;
	else
	  // Do a smaller partselect
	  {
	    // Change range to size bounded by mask
	    r.setMsb (r.getLsb () + width - 1);

	    const NUExpr *result =
	      varsel(ps->getIdentExpr(), ps->getIndex (), r, ps->getLoc (),
                     r.getLength ());
	    return result;
          }
      }
      break;

    case NUExpr::eNUMemselRvalue: {
      const NUExpr* ret = varsel(e, ConstantRange (width-1,0), e->getLoc (),
                                 width);
      return ret;
    }

    case NUExpr::eNUIdentRvalue:
      {
	const NUIdentRvalue *rv = dynamic_cast<const NUIdentRvalue*>(e);

	NUNet *net = rv->getIdent ();

	if (net->getBitSize () <= width)
	  return copyExpr(e);
	
	ConstantRange r(width - 1, 0);

	NUVectorNet *v = dynamic_cast<NUVectorNet*>(rv->getIdent ());
	r.denormalize (v->getRange ());

	const NUExpr *result = varsel(rv, r, e->getLoc (), r.getLength ());
        return result;
      }
      break;

    case NUExpr::eNUTernaryOp:
      return foldPartsel (e, 0, ConstantRange (width-1,0), e->getLoc (), false);

    case NUExpr::eNUConcatOp:
    case NUExpr::eNUBinaryOp:
    case NUExpr::eNUUnaryOp:
      return foldPartsel (e, 0, ConstantRange (width-1,0), e->getLoc (), false);

    default:
      return 0;      // don't fold these we drop necessary truncations
    }
}


// Simplify ?: (Question-Colon) expressions.
//  Don't CALL FINISH - just return non-NULL on success
const NUExpr*
FoldI::reduceQC (const NUBinaryOp *e)
{
  const NUExpr *lop = e->getArg (0);
  const NUExpr *rop = e->getArg (1);

  if (lop->getBitSize() != rop->getBitSize())
    return 0;

  const NUTernaryOp *cond;
  bool rightOp = false;		// Assume left op is ?: 
  if ((cond = dynamic_cast<const NUTernaryOp*>(lop))
      && (cond->getOp () == NUOp::eTeCond))
    ;
  else if ((cond = dynamic_cast<const NUTernaryOp*>(rop))
	   && (cond->getOp () == NUOp::eTeCond))
    {
      rop = lop;
      rightOp = true;
    }
  else
    return 0;

  const SourceLocator& loc = e->getLoc ();

  // a && (a ? b:c) => a && b
  // a || (a ? b:c) => a || c
  // a ^  (a ? b:c) => a ? (!b):c
  NUOp::OpT opcode = e->getOp ();
  switch (opcode) {
  case NUOp::eBiBitAnd:
    if (e->getBitSize () != 1)
      break;
    // fall-thru
  case NUOp::eBiLogAnd:
    if ((*rop) == *(cond->getArg (0)))
    {
      return binaryOp(opcode, rop, cond->getArg (1), loc, rop->getBitSize ());
    }
    break;
      
  case NUOp::eBiBitOr:
    if (e->getBitSize () != 1)
      break;
    // fall-thru

  case NUOp::eBiLogOr:
    if ((*rop) == *(cond->getArg (0))) {
      return binaryOp(opcode, rop, cond->getArg (2), loc, rop->getBitSize ());
    }
    break;

  case NUOp::eBiBitXor:
    if ((e->getBitSize () == 1) && ((*rop) == *(cond->getArg (0)))) {
      const NUExpr* result = unaryOp(NUOp::eUnLogNot, cond->getArg(1), loc, 1);
      result = ternaryOp(cond->getArg(0), result, cond->getArg(2), loc, 1);
      return result;
    }
    break;

  default:
    break;
  }
    
  // Try distributing the other term into the ?:
  const NUExpr* result = NULL;
  if (rightOp) {
    result = distributeQC (rop, opcode, cond, loc);

    if (!result && isCommutative (opcode))
      result = distributeQC (cond, opcode, rop, loc);
  } else {
    result = distributeQC (cond, opcode, rop, loc);
    if (!result && isCommutative (opcode))
      result = distributeQC (rop, opcode, cond, loc);
  }

  return result;
}

// Simplify ?: (Question-Colon) expressions like
//  - (e ? a : b)
//
const NUExpr*
FoldI::reduceQC (const NUUnaryOp *e)
{
  const NUExpr *uop = e->getArg (0);

  const NUTernaryOp *cond;
  if ((cond = dynamic_cast<const NUTernaryOp*>(uop))
      && (cond->getOp () == NUOp::eTeCond))
    ;
  else
    return 0;

  const NUOp* xa;

  NUOp::OpT opcode = e->getOp ();

  if (ctce (cond->getArg (1)) || ctce (cond->getArg (2)))
    ;                    // One or more constants to take the operator
  else if ((xa = dynamic_cast<const NUOp*>(cond)) != 0
           && xa->getOp () == opcode) // operators might cancel
    ;
  else if (isReductionOp (opcode)       // avoids a temporary, always a winner
           || e->getBitSize () < cond->getBitSize ()) // otherwise reducing
    ;
  else
    return 0;                   // No reason to favor distributing

  const SourceLocator &loc = e->getLoc ();
  UInt32 eSize = e->getBitSize ();

  const NUExpr *lop = unaryOp(opcode, copyExpr (cond->getArg (1)), loc, eSize);
  foldExprTree(&lop);
  const NUExpr *rop = unaryOp(opcode, copyExpr (cond->getArg (2)), loc, eSize);
  foldExprTree(&rop);
  return ternaryOp(cond->getArg(0), lop, rop, loc, eSize);
}

// Convert:  (~a) & (~b) => ~(a|b)
//           (~a) | (~b) => ~(a&b)
//           (!a) && (!b) => !(a || b)
//           (!a) || (!b) => !(a && b)
//
// WORRY ABOUT (!a) ^ (!b) => (a ^ b), not true for vector a,b!
const NUExpr*
FoldI::DeMorgan (const NUBinaryOp* bop)
{
  NUOp::OpT code = bop->getOp ();

  // Watch out for something like !X ^ ~X
  switch (code) {
  case NUOp::eBiBitXor:
    {
      bool sameSizeExprs = bop->getArg (0)->getBitSize () == bop->getArg (1)->getBitSize ();

      const NUOp* lop = dynamic_cast<const NUOp*>(bop->getArg (0));
      const NUOp* rop = dynamic_cast<const NUOp*>(bop->getArg (1));

      if (isComplement (lop) && sameSizeExprs)
      {
        if (isComplement (rop) && sameSizeExprs)
        {
          // (~a) ^ (~b) => a^b

          return binaryOp(NUOp::eBiBitXor, lop->getArg(0), rop->getArg (0),
                          bop->getLoc ());
        }
        else if ((*bop->getArg (1)) == (*lop->getArg (0)))
          // (~a) ^ (a) => 1
          return genOnes (bop->getLoc (), bop->getBitSize ());
        else
        {
          // (~a) ^ (b) => ~(a^b)
          const NUExpr* result = binaryOp(NUOp::eBiBitXor,
                                          lop->getArg (0), bop->getArg (1),
                                          bop->getLoc ());
          result = unaryOp(lop->getOp (), result, result->getLoc ());
          return result;
        }
      }
      else if (isComplement (rop) && sameSizeExprs)
      {
        if ((*rop->getArg (0)) == (*bop->getArg (0)))
          // (a ^ (~a)) => 1
          return genOnes (bop->getLoc (), bop->getBitSize ());
        else
        {
          // (a ^ (~b)) => ~(a ^ b)
          const NUExpr* result = binaryOp(NUOp::eBiBitXor, bop->getArg (0),
                                          rop->getArg (0), bop->getLoc ());
          result = unaryOp(rop->getOp (), result, result->getLoc ());
          return result;
        }
      }
          
      return NULL;
    }

  case NUOp::eBiBitAnd:
  case NUOp::eBiBitOr:
    // BITWISE DeMorgan's
    {
      const NUOp* lop = dynamic_cast<const NUOp*>(bop->getArg (0));
      const NUOp* rop = dynamic_cast<const NUOp*>(bop->getArg (1));

      if (not lop or not rop)
        return 0;

      if ((lop->getOp () == NUOp::eUnBitNeg)
          && (rop->getOp () == NUOp::eUnBitNeg))
      {
        const NUExpr* result =
          binaryOp((code==NUOp::eBiBitAnd) ? NUOp::eBiBitOr : NUOp::eBiBitAnd,
                   lop->getArg (0), rop->getArg (0), bop->getLoc ());
        result = unaryOp(NUOp::eUnBitNeg, result, result->getLoc ());
        return result;
      }

      if ( ((lop->getOp () == NUOp::eUnVhdlNot) && (rop->getOp () == NUOp::eUnVhdlNot))
           || ((lop->getOp () == NUOp::eUnLogNot && lop->getArg (0)->getBitSize () == 1)
               && (rop->getOp () == NUOp::eUnLogNot && rop->getArg (0)->getBitSize () == 1)))
      {
        const NUExpr* result =
          binaryOp((code==NUOp::eBiBitAnd) ? NUOp::eBiBitOr : NUOp::eBiBitAnd,
                   lop->getArg (0), rop->getArg (0), bop->getLoc ());
        result = unaryOp(lop->getOp (), result, result->getLoc ());
        return result;
      }

      // No luck
      return NULL;
    }

  case NUOp::eBiLogAnd:
  case NUOp::eBiLogOr:
    // LOGICAL DeMorgan's  (!a) && (!b) => !(a || b)
    {
      const NUOp* lop = dynamic_cast<const NUOp*>(bop->getArg (0));
      const NUOp* rop = dynamic_cast<const NUOp*>(bop->getArg (1));

      if (not lop or not rop)
        return 0;

      if ((lop->getOp () == NUOp::eUnLogNot)
          && (rop->getOp () == NUOp::eUnLogNot))
      {
        const NUExpr* result =
          binaryOp((code==NUOp::eBiLogAnd) ? NUOp::eBiLogOr : NUOp::eBiLogAnd,
                   lop->getArg (0), rop->getArg (0), bop->getLoc ());
        result = unaryOp(NUOp::eUnLogNot, result, result->getLoc ());
        return result;
      }
      return NULL;
    }

  default:
    return NULL;
  }
}

// Appropriately complement a negated term - we DON'T COPY THE ARGUMENT, IT'S ALREADY
// a copy!!!!
const NUExpr* FoldI::genInverted(const NUExpr*e)
{
  if (FoldI::isConstantX (e->castConstXZ ()))
    // If this was an 'x', don't bother inverting it, it should stay an 'x'
    return e;

  UInt32 width = e->getBitSize ();
  NUOp::OpT op = (width == 1) ? NUOp::eUnLogNot : NUOp::eUnBitNeg;
  return unaryOp(op, e, e->getLoc(), width, specifySign(e->isSignedResult ()));
}

const NUExpr* FoldI::genComplement (const NUExpr* e)
{
  const NUExpr* ret = NULL;
  bool copy = true;
  const SourceLocator& loc = e->getLoc();
  const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(e);
  const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(e);

  if ((uop != NULL) && isComplement (uop)) {
    ret = uop->getArg(0);
  } else if (const NUConst* cop = e->castConstNoXZ ()) {
    DynBitVector v;
    cop->getSignedValue (&v);
    v.flip ();
    ret = constant(cop->isSignedResult (), v, v.size (), loc);
    copy = false;
  } else if (isConstantX (e->castConstXZ ())) {
    // ! 1'bx => 1'bx
    ret = const_cast<NUExpr*>(e);
  } else if ((bop != NULL) &&
             (bop->getBitSize () == 1 && isRelationalOp (bop->getOp ())))
  {
    ret = binaryOp(sInverseOp (bop->getOp ()),
                   bop->getArg (0),
                   bop->getArg (1),
                   loc, e->getBitSize (), specifySign(e->isSignedResult ()));
    copy = false;
  }
  else {
    // Nothing clever happened...
    ret = genInverted (copyExpr (e));
    copy = false;
  }
  return foldAndManage (ret, copy);
} // const NUExpr* FoldI::genComplement

/*!
 * For a complemented expression \arg !\<bop\> or ~\<bop\>, try to
 * transform \<bop\> into an equivalent expression where the complement
 * is already generated.  For example
 *
 *      !( A && !B)  => (!A || B)
 *      !( A ^ !B) => (A ^ B)
 *      !(!A || !B) => (A && B)
 */
const NUExpr*
FoldI::negatedDeMorgan (const NUExpr* e)
{
  const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(e);
  if (!bop)
    return NULL;

  NUOp::OpT op = bop->getOp ();
  if (! isBitwiseOp (op))
    return NULL;

  // There are several ways we can win here.  If we have an inverted operand,
  // then we can apply DeMorgan's theorem to remove the outer inverter.
  //
  // If we have a relational expression, then we can invert the conditional
  // test and achieve the same end
  //

  UInt32 i;
  for (i = 0; i <2; ++i) {
    const NUExpr* e = bop->getArg (i);

    const NUOp* term = dynamic_cast<const NUOp*>(e);
    if (not term)
      continue;

    if (isComplement (term)) {
      break;
    } else if (term->castConstNoXZ ()) {
      break;
    } else if (term->getBitSize () == 1 && isRelationalOp (term->getOp ())) {
      break;
    }
  }

  if (i == 2)
    return NULL;                // No luck.

  const SourceLocator& loc = bop->getLoc ();
  // We have at least one complemented or complementable term already.

  // To prevent a recursive crash with FoldI::DeMorgan(), we need to collapse
  // any  nested inversions.
  //
  if (op == NUOp::eBiBitXor) {
    // not(a ^ not(b)) => a ^ b
    const NUExpr* comp = genComplement (bop->getArg (i));
    return binaryOp(op, comp, bop->getArg ((i+1)%2), loc);
  } else {
    // Complement BOTH operands and exchange the operator also
    if (op == NUOp::eBiBitAnd)
      op = NUOp::eBiBitOr;
    else
      op = NUOp::eBiBitAnd;

    const NUExpr* lop = genComplement (bop->getArg (0));
    const NUExpr* rop = genComplement (bop->getArg (1));
      
    return binaryOp(op, lop, rop, loc);
  }
}


// Make a logical (1-bit) value out of a multi-bit value by
// comparing it to an appropriately sized '0' (if necessary)
//
// If the operation we're folding doesn't produce a logical result, then
// just return the operand, or if the operand is only 1 bit long anyway,
// then don't bother generating a comparison either.
//
const NUExpr* FoldI::genLogical (const NUExpr* expr, NUOp::OpT op) {
  UInt32 sz = expr->determineBitSize();

  if (sz == 1
       || (! isLogical (op)
           && ! isReductionOp (op)
           && ! isRelationalOp (op))) {
    return copyExpr(expr);
  }
  const SourceLocator& loc = expr->getLoc();
  const NUExpr* zero = genFalse(loc, sz, expr->isSignedResult());
  return binaryOp(NUOp::eBiNeq, expr, zero, loc, 1);
}

// Check that this partsel is safe to convert into a masked value
static bool sMaskablePartsel (const NUVarselRvalue* v)
{
  if (not v or not v->isConstIndex ())
    return false;

  const ConstantRange &r (*(v->getRange ()));

  // Whatever you do, don't cross a 64 bit boundary
  UInt32 lb = r.getLsb () & - LONG_BIT;     // round down to word boundary
  UInt32 ub = r.getMsb () | (LONG_BIT - 1); // and to upper edge of word bound

  if ((ub + 1 - lb)  > LLONG_BIT)
    return false;               // can't cross 64 bit boundary

  return true;
}

// Check for a maskable sign-extend.
// If we started with an expression (in Verilog) such as:
//
//   $signed(a[15:8]) < $signed(b[15:8])
//
// we want to allow these operands to be optimized.  The nucleus population looks like
//
//        eBiLt ( eBiVhExt(a[15:8], 8), eBiVhExt(b[15:8], 8))
//
// This function is called with one of the eBiVhExt nodes and will decide if we can
// ignore the EXT and convert the underlying partsel into a masked operand.
//
//
static bool sMaskableSignExtend (const NUBinaryOp* v)
{
  if (not v)
    return false;

  switch (v->getOp ()) {
  case NUOp::eBiVhExt:
  case NUOp::eBiVhZxt:
  {
    UInt32 width;
    const NUConst* c =  v->getArg (1)->castConst ();
    if (c && c->getUL (&width))
      return (width == v->getArg (0)->getBitSize ())
        && sMaskablePartsel (dynamic_cast<const NUVarselRvalue*>(v->getArg (0)));
    return false;
  }
  default:
    return false;
  }  
}

// Build a word-aligned part-select from the provided pieces
const NUExpr* FoldI::genWordAccess (const NUExpr* base, ConstantRange range)
{
  range.setLsb (range.getLsb () & -LONG_BIT); // Round down to word boundary

  // Compute best size for comparison
  UInt32 width = range.getLength ();
  if (width <= CHAR_BIT)
    width = CHAR_BIT;
  else if (width <= SHORT_BIT)
    width = SHORT_BIT;
  else if (width <= LONG_BIT)
    width = LONG_BIT;
  else
    width = LLONG_BIT;

  range.setMsb (range.getLsb () + width - 1); // round up to boundary

  return varsel(base, range, base->getLoc ());
}

// Simplify X1[H:L] relop X2.  Only do this when aggressively folding
// as it screws up bit-level usage information.
// an example: (reg a[6:0];)
//   a[3:2] != 1'b11  ==> (a & 7'b0001100) != 7'b0001100
const NUExpr*
FoldI::reducePartselCompares (const NUBinaryOp* op)
{
  if (not isAggressive ())
    // Also don't do this for non-aggressive passes, as we will ping-pong with
    // the binary op folder that creates partsels compared to shifted constants.
    return NULL;

  const NUExpr* lop = op->getArg (0);

  if (const NUBinaryOp* lbop = dynamic_cast<const NUBinaryOp*>(lop)) {
    if (!sMaskableSignExtend (lbop))
      return NULL;
    lop = lbop->getArg (0);
  }

  const NUVarselRvalue* x1 = dynamic_cast<const NUVarselRvalue*>(lop);
  if (!x1 || !sMaskablePartsel (x1)
    || x1->getType () != NUExpr::eNUVarselRvalue) {
    return NULL;
  }

  const ConstantRange &r1 (* (x1->getRange ()));

  UInt32 wordOff = r1.getLsb () % LONG_BIT;
  if (wordOff == 0)
    // CodeGen does zero-aligned for free or we already reduced a bitvector
    // fetch into a word-aligned access.
    return NULL;

  // What's the width of the aligned mask for this operand.
  UInt32 width = r1.getLength () + wordOff;
  NU_ASSERT (width <= LLONG_BIT, op);

  // Construct dense mask of interesting bits 
  UInt64 mask = (UtUINT64_MAX >> (LLONG_BIT - r1.getLength ())) << wordOff;
  NUOp::OpT opcode = op->getOp ();
  bool isSigned = isSignedOp (opcode);

  SInt32 msb = r1.getMsb () % LONG_BIT;
  if (isSigned && msb != 31 && msb != 15 && msb != 7)
    // resulting compare wouldn't have sign-bits in correct alignment for a physical
    // sized compare instruction.
    return NULL;
    
  // Now check X2
  const NUExpr* result;
  const NUExpr* rop = op->getArg (1);

  if (const NUBinaryOp* rbop = dynamic_cast<const NUBinaryOp*>(rop)) {
    if (! sMaskableSignExtend (rbop))
      return NULL;
    rop = rbop->getArg (0);     // this is untested code
  }

  if ( ( op->getArg(0)->isSignedResult () )||
       ( op->getArg(1)->isSignedResult () )  )
  {
    // at one time the thinking was that it was safe to do the
    // following transform with signed operands that were not at the
    // most significant position of the word.  Bug 12130 shows this is
    // wrong. see a simple testcase in test/langcov/Signed/bug12130.v
    // and  (test/vhdl/signed/bug8453_01.vhdl)

    // The idea was that it would be ok here to just shift the value
    // up to an point where it aligns with the left op (filling with
    // zeros).  HOWEVER this is only safe if the MSB of the shifted
    // value is the MSB bit of the physical size.
    // Othewise when we make it word size below (genWordAccess), it will 
    // be sign extended and ops like < == and > will not do what is 
    // expected (test/vhdl/signed/bug8453_01.vhdl).
    // NOTE This optimization is currently disabled whenever either
    // operand is signed. This is overally restrictive and could be
    // relaxed by checking for the matching of the physical size MSB
    // and the shifted MSB.

    return NULL;
  }
  if (const NUVarselRvalue* x2 = dynamic_cast<const NUVarselRvalue*>(rop)) {
    const ConstantRange r2 (*(x2->getRange ()));
    if ( !sMaskablePartsel (x2)
         || x2->getType () != NUExpr::eNUVarselRvalue
         || ((r2.getLsb () % LONG_BIT) != (SInt32)wordOff) // incompatible alignment?
         || (r2.getLength () != r1.getLength ()))
      // Badly formed
      return NULL;

    result = binaryOp(NUOp::eBiBitAnd, genWordAccess (copyExpr (x2->getIdentExpr ()), r2),
                             constant(false, mask, width, x2->getLoc ()),
                             x2->getLoc (), 0, specifySign(isSigned));

  } else if (const NUConst* c2 = op->getArg (1)->castConst ()) {
    DynBitVector value;
    c2->getSignedValue (&value);
    value.resize (value.size () + wordOff);
    value <<= wordOff;
    result = constant(c2->isSignedResult (), value, value.size (), c2->getLoc ());
  } else {
    // not a candidate
    return NULL;
  }


  const NUExpr* newX1 = binaryOp(NUOp::eBiBitAnd, genWordAccess (x1->getIdentExpr (), r1),
                                 constant(false, mask, width, x1->getLoc ()),
                                 x1->getLoc (), 0, specifySign(isSigned));

  result = binaryOp(opcode, newX1, result, op->getLoc ());
  return result;
}
  
// Convert logical &&, || operators into bitwise, to reduce
// amount of branching GCC emits.  Also collapse situations where
// we know one of the operands is guaranteed to be non-zero
//
// The || => | is only valid for one-bit operands.
// Only done for operations with one-bit inputs.
const NUExpr*
FoldI::reduceLogical (const NUBinaryOp *op) {
  NUOp::OpT old_op = op->getOp();

  const NUExpr *arg0 = op->getArg(0);
  const NUExpr *arg1 = op->getArg(1);

  switch (old_op) {
  case NUOp::eBiLogAnd:
    if (alwaysNonZero (arg0)) {
      if (alwaysNonZero (arg1)) {
        return genTrue(op->getLoc(), op->getBitSize(), op->isSignedResult());
      }
      else {
        return genLogical (arg1);
      }
    }
    else if (alwaysNonZero (arg1)) {
      return genLogical (arg0);
    }

    break;

  case NUOp::eBiLogOr:
    if (alwaysNonZero (op->getArg (0))
        || alwaysNonZero (op->getArg (1)))
      return genTrue (op->getLoc (), op->getBitSize (), op->isSignedResult ());

    break;
  default:
    return NULL;
  }

  return NULL;
}

const NUExpr*
FoldI::reduceSingleBitArithmetic (const NUBinaryOp *op) {
  if (op->getBitSize() != 1) {
    return NULL;
  }

  const NUExpr *arg0 = op->getArg(0);
  const NUExpr *arg1 = op->getArg(1);

  if (arg0->getBitSize() != 1) {
    return NULL;
  }
  if (arg1->getBitSize() != 1) {
    return NULL;
  }

  NUOp::OpT old_op = op->getOp();
  NUOp::OpT new_op = old_op;
  switch (old_op) {
  case NUOp::eBiPlus:
  case NUOp::eBiMinus:
    new_op = NUOp::eBiBitXor;
    break;
  default:
    break;
  }
  if (old_op != new_op) {
    return binaryOp(new_op, arg0, arg1, op->getLoc());
  }
  return NULL;
} // FoldI::reduceSingleBitArithmetic

// evaluate a shiftcount expression - if the shift amount is always
// greater or equal to the bitwidth, then return true indicating that
// all significant bits will be shifted away.
//
bool FoldI::shiftTooMuch (const NUBinaryOp* shift)
{
  const NUExpr *lop = shift->getArg (0),
    *rop = shift->getArg (1);

  NUOp::OpT opcode = shift->getOp ();

  if (const NUConst* c = ctce (rop)) {
    DynBitVector sc;            // Verilog shifts always treat constant as unsigned
    c->getSignedValue (&sc);

    switch (opcode) {
    case NUOp::eBiLshift:
    case NUOp::eBiLshiftArith:
      if (sc >= shift->getBitSize ()
        || sc >= lop->getBitSize ())
        return true;    // no significant bits are left for the result
      break;

    case NUOp::eBiRshiftArith: {
      // If the expression is positive (e.g. has leading zeros), and we
      // arithmetically right-shift it, then it might shift away all significance.
      UInt32 lop_eff_size = getCachedEffectiveBitSize(lop);
      if ((lop->getBitSize () > lop_eff_size) && (sc >= lop_eff_size))
        return true;
      break;
    }
        
    case NUOp::eBiRshift:
      if (sc >= getCachedEffectiveBitSize(lop)
          || sc >= lop->getBitSize ())
        return true;              // Shifted all the source bits away...
      break;

    case NUOp::eBiVhRshift:
    case NUOp::eBiVhLshift:
      // These have to deal with negative shift counts.

    case NUOp::eBiVhRshiftArith:
    case NUOp::eBiVhLshiftArith:
      // These do bit-copying from the shift-in side.
      return false;

    default:
      NU_ASSERT (0=="Unknown Shift Operator", shift);     // Unknown shift operator
      break;
    }

    return false;
  }

  if (opcode == NUOp::eBiVhRshift
      || opcode == NUOp::eBiVhLshift
      || opcode == NUOp::eBiVhRshiftArith
      || opcode == NUOp::eBiVhLshiftArith)
    return false;

  // How much could we possibly be shifting by?  This is only important
  // for non-constant shift amounts in VERILOG.  VHDL behavior is more complex.
  //
  DynBitVector valueWidth (32, lop->getBitSize ());

  bool result;
  if (evalRelop (NULL, &result, NUOp::eBiUGtre, rop, NULL, &valueWidth))
    // evaluates at compile time...
    return result;

  return false;
}

// Compute precise sum with room for carry
const NUExpr* FoldI::preciseSum(const NUExpr* e1, const NUExpr* e2,
                                const SourceLocator& loc)
{
  // Number of bits needed to represent the sum accurately
  UInt32 csize = std::max (getCachedEffectiveBitSize(e1),
                           getCachedEffectiveBitSize(e2)) + 1;
  const NUExpr* sum = binaryOp(NUOp::eBiPlus, copyExpr (e1), copyExpr (e2), loc);
  UInt32 asize = sum->getBitSize (); // will be the 'determineBitSize' value
  if (asize <= csize) {
    sum = binaryOp(NUOp::eBiPlus, sum, FoldI::genFalse (loc, csize, false),
                   loc, csize);
  }
  return sum;
}


// reduce a << operation
const NUExpr*
FoldI::reduceLshift (const NUBinaryOp* bop)
{
  UInt32 width = bop->getBitSize ();
  const SourceLocator& loc = bop->getLoc ();
  NUOp::OpT opc = bop->getOp ();

  // Special case:  0 << K => 0
  const NUExpr* lop = bop->getArg (0);

  const NUConst *c = ctce (lop);      // Handle xz eventually also.
  if ( c && c->isZero ())
    return genFalse (loc, width);
  else if (c && c->isOnes () && opc == NUOp::eBiVhLshiftArith)
    return genOnes (loc, width);

  // Try converting VHDL logical shift into simpler form that we can
  // optimize.
  const NUExpr* result;

  SInt32 shiftCount;
  c = ctce (bop->getArg (1));
  if (c && c->getL (&shiftCount) && opc == NUOp::eBiVhLshift) {

    if (shiftCount < 0) {
      result = binaryOp(NUOp::eBiRshift, lop,
                        constant(false, -shiftCount, 32, loc), loc,
                        width);
    } else {
      result = mExprCache->changeSign(c, false);
      result = binaryOp(NUOp::eBiLshift, lop, result, loc, width);
    }
    return result;
  }


  if (shiftTooMuch (bop))
    return genFalse (loc, width);


  // All other cases depend on shift count being constant
  //

  c = ctce (bop->getArg (1));
  if (not c)
    return NULL;

  // Check for (a<<b) << c  => a << (b+c)
  // This is only safe if doing VERILOG SHIFTS
  if (opc == NUOp::eBiLshift || opc == NUOp::eBiLshiftArith)
    if (const NUBinaryOp *lbop = dynamic_cast<const NUBinaryOp*>(lop))
      if (lbop->getOp () == opc && lbop->getBitSize () == width) {
        const NUExpr *shift = preciseSum (lbop->getArg (1), bop->getArg (1), loc);

        result = binaryOp(opc, lbop->getArg (0), shift, loc, width);
        return result;
      }

  if (c->isZero ())
    // X << 0 => X
    return copyExpr(lop);

  if (opc != NUOp::eBiLshift && opc != NUOp::eBiLshiftArith)
    return NULL;                // exclude VHDL shifts from further consideration

  UInt64 count;
  c->getULL (&count);

  if (count >= width)
    // Shifted away more bits than exist in the result
    return  genFalse (loc, width);

  // Check for
  //   (SCALAR << K)
  // and convert into
  //  SCALAR ? 1<<K : 0
  // but only if we are working with bit-vectors.  Otherwise, just leave
  // it as a shift
  if ((getCachedEffectiveBitSize(lop) == 1) && (width > LLONG_BIT)) {
    // Bingo!
    DynBitVector v (width);	// holds 1<<k
    v.set (count);		// {1'b1, count'b0}

    result = ternaryOp(lop, constant(false, v, width, loc),
                       genFalse (loc, width), loc, width);
    return result;
  }

  // Check for and expression that might reduce to a constant after shifting, e.g.:
  //            {x,32'h12345678} << k, such that all the non-constant bits disappear
  //
  result = NULL;
  ConstantRange excess ((width - count)-1, 0); // Range of possibly non-zero bits left
  const NUExpr* piece = foldPartsel (lop, NULL,  excess, loc, false);
  if (not piece)
    return NULL;              // 

  // Make sure we reduce concat's with a singleton const
  const NUExpr* foldedPiece = foldAndManage(piece, false);
  if (const NUConst *c = foldedPiece->castConst ()) {
    // just recreate with count zeros tacked on.
    DynBitVector v;
    c->getSignedValue (&v);
    v.resize (width);
    v <<= count;
    result = constant(bop->isSignedResult (), v, width, loc);
  }

  if (! result && ! bop->isSignedResult () && width > LLONG_BIT) {
    // Convert BitVector left-shifts into a concat of a partselect; being careful to maintain size.
    // This allows us to stripmine.
    UInt32 catExtra = width - count;

    NUCExprVector v;

    ConstantRange r (catExtra - 1, 0);
    if (catExtra > lop->getBitSize ()) {
      r.setMsb (lop->getBitSize () - 1);
      catExtra -= lop->getBitSize ();
    } else {
      catExtra = 0;
    }
    
    if (catExtra)
      v.push_back (genFalse (loc, catExtra));
    v.push_back (varsel(lop, r, loc));
    v.push_back (genFalse (loc, count)); // left-shift produces low-order zeros
    result = concatOp(v, 1, loc, bop->getBitSize());
  }
  return result;
}

// reduce a >> operation
const NUExpr*
FoldI::reduceRshift (const NUBinaryOp* bop)
{
  NUOp::OpT opc = bop->getOp ();
  UInt32 width = bop->getBitSize (); // width of the result expression
  const SourceLocator& loc = bop->getLoc ();
  const NUExpr* lop = bop->getArg (0);
  const NUConst* cVal = ctce (lop);

  if (cVal && cVal->isZero ())
    // 0 >> x   =>   0
    return genFalse (loc, width);
  else if (cVal && cVal->isOnes () &&
           (opc == NUOp::eBiVhRshiftArith || opc == NUOp::eBiRshiftArith))
    // -1 >>> x => -1
    // -1 sra x => -1
    return genOnes (loc, width);

  if (shiftTooMuch (bop))
    return genFalse (loc, width);

  const NUConst *c = ctce (bop->getArg (1));
  if (not c)
    return NULL;

  if (c->isZero ())
    // a >> 0 => a
    return copyExpr(lop);

  // Check for (a >> b) >> c  => a >> (b+c)
  const NUExpr* result;
  if (const NUBinaryOp *lbop = dynamic_cast<const NUBinaryOp*>(lop))
    if (lbop->getOp () == opc && lbop->getBitSize () == width
      && (opc == NUOp::eBiRshift || opc == NUOp::eBiRshiftArith)) {
      // compute a larger sum because the values are unsigned...
      const NUExpr *shift = preciseSum (lbop->getArg (1), bop->getArg (1), loc);
        
      result = binaryOp(opc, lbop->getArg (0), shift, loc, width);
      return result;
    }

  // Try converting VHDL logical shift into simpler form that we can
  // optimize.

  SInt32 shiftCount;
  if (c->getL (&shiftCount)) {
    switch (opc) {
    case NUOp::eBiVhRshift:
      if (shiftCount < 0) {
        result =
          binaryOp(NUOp::eBiLshift, copyExpr (lop),
                          constant(false, -shiftCount,
                                           32,
                                           loc),
                          loc, width);
      } else {
        result = mExprCache->changeSign(c, false);
        result = binaryOp(NUOp::eBiRshift, copyExpr (lop), result, loc, width);
      }
      return result;

    case NUOp::eBiVhRshiftArith:
      if (shiftCount > 0) {
        const NUExpr* shiftAmt = mExprCache->changeSign(c, false);
        result = mExprCache->changeSign(lop, true);
        result = binaryOp(NUOp::eBiRshiftArith, result, shiftAmt, loc, width);
        return result;
      }
      return NULL;
      
    case NUOp::eBiRshift:
      break;

    case NUOp::eBiRshiftArith:
    {
      // arithmetic right shift might end up with just the sign bit
      UInt32 shiftValueWidth = lop->getBitSize ();
      if (UInt32 (shiftCount) >= shiftValueWidth) {
        lop = mExprCache->changeSign(lop, true);
        result = binaryOp(NUOp::eBiSLt, lop,
                                 constant(true, 0, 1, loc), loc);
        result = ternaryOp(result, genOnes (loc, width, true),
                                  genFalse (loc, width, true), loc);
        return result;
      }
    }
    // fall-thru

    default:
      // Exclude cases with no further shift optimization
      return NULL;

    }
  }

  // ONLY VERILOG rightshift reaches past here.
  
  UInt64 count;
  c->getULL (&count);

  if (count >= getCachedEffectiveBitSize(lop)
      && not lop->isSignedResult ())
    // right shifting away all signficant bits?
    return genFalse (loc, width);

  // [BUG5436] Beware of things like:
  //     DEST33 = (SIGNED32 >> 4);    -- should be {4'b0000,SIGNED32[31],SIGNED32[31:4]}
  // We would transform that into
  //     DEST33 = $SIGNED(SIGNED32[31:4])  -- wrong {5{SIGNED32[31]}, SIGNED32[31:0]}
  // We could make
  //     DEST33 = (SIGNED32 SXT 33)[32:4]
  // but that's uglier than leaving the shift alone!
  //
  if (lop->isSignedResult () && width >= lop->determineBitSize ())
    return NULL;

  switch (lop->getType ())
    {
    default:
      break;

    case NUExpr::eNUVarselRvalue:
      {
	const NUVarselRvalue *ps = dynamic_cast<const NUVarselRvalue*>(lop);
        if (not ps->isConstIndex ())
          break;

	ConstantRange r = *ps->getRange ();

        r.setLsb (r.getLsb () + count);

	// Create new partsel that excludes count bits
        const NUExpr *id = copyExpr (ps->getIdentExpr ());
	const NUExpr* result = NULL;
        result = varsel(id, r, ps->getLoc (), r.getLength());
        result = mExprCache->makeSizeExplicit(result, width);

	return result;
      }

    case NUExpr::eNUIdentRvalue:
      {
	const NUIdentRvalue *id = dynamic_cast<const NUIdentRvalue*>(lop);
	NUNet *net = id->getIdent ();

	if (NUVectorNet * vn = dynamic_cast<NUVectorNet*>(net))
	  {
	    const ConstantRange &vrange = *(vn->getRange ());

	    UInt32 vecWidth = vrange.getLength ();

	    ConstantRange r (vecWidth-1, count);
	    const NUExpr* result = varsel(id, r, loc, r.getLength());
            result = mExprCache->makeSizeExplicit(result);
	    return result;
	  }
	else
	  return NULL;
	
      }
    case NUExpr::eNUConcatOp:
    {
      // {a,b,c} >> k   =>   {a,b,c}[w-k,k]
      const NUConcatOp *concat = dynamic_cast<const NUConcatOp*>(lop);
      SInt32 msb = getCachedEffectiveBitSize(concat) - 1; // What's possible MSB

      if (msb < (SInt32)count)
        // Shifted away all the interesting bits...
        return genFalse (loc, width,
                          bop->isSignedResult ());

      const NUExpr* result = foldConcatPartsel (concat, msb, count, loc);
      if (result) {
        result = mExprCache->makeSizeExplicit(result, width);
        return result;
      }
    }
    break;

    }

  if (! bop->isSignedResult () && width > LLONG_BIT) {
    // Convert BitVector right-shifts into a concat of a partselect;
    result = padExpr (varsel(copyExpr (lop),
                                           ConstantRange (lop->getBitSize () - 1, count),
                                           loc),
                       width);
    return result;
  }

  return NULL;
}

// True if expression is never negative
bool FoldI::termIsPositive (const NUExpr* e)
{
  if (not e->isSignedResult ())
    return true;

  if (getCachedEffectiveBitSize(e) < e->getBitSize ())
    // It may be a signed expression, but it doesn't have enough
    // significant bits to become negative
    return true;

  if (const NUIdentRvalue* rv = dynamic_cast<const NUIdentRvalue*>(e))
    return rv->getIdent ()->isUnsignedSubrange ();
  
  return false;
}

static bool sComputeRelOp (const DynBitVector &lv, const DynBitVector &rv, NUOp::OpT op,
                           const SourceLocator& loc)
{
  bool signResult = NUOp::isSignedOperator (op);

  if (signResult) {
    switch (op)
    {
    case NUOp::eBiTrieq:
    case NUOp::eBiEq:   return lv == rv;
    case NUOp::eBiTrineq:
    case NUOp::eBiNeq:  return lv != rv;
    case NUOp::eBiSLt:  return lv.signedCompare(rv) < 0;
    case NUOp::eBiSGtr: return lv.signedCompare(rv) > 0;
    case NUOp::eBiSLte: return lv.signedCompare(rv) <= 0;
    case NUOp::eBiSGtre:return lv.signedCompare(rv) >= 0;
    default:	LOC_ASSERT ("Unknown Rel Op" == 0, loc);
    }
  }
  else {
    switch (op)
    {
    case NUOp::eBiULt:  return lv < rv;
    case NUOp::eBiUGtr: return rv < lv;;
    case NUOp::eBiULte: return !(rv < lv);
    case NUOp::eBiUGtre:return !(lv < rv);
    case NUOp::eBiTrieq:
    case NUOp::eBiEq:   return lv == rv;
    case NUOp::eBiTrineq:
    case NUOp::eBiNeq:  return lv != rv;
    default:	LOC_ASSERT ("Unknown Rel Op" == 0, loc);
    }
  }

  return false;
} // static bool sComputeRelOp

// Return both the result of the comparison AND whether it's valid.
// Handle both signed and unsigned relationals and situations with
// partial information, including null values for e or right.
bool FoldI::evalRelop(const NUBinaryOp* e, bool* result,
                      NUOp::OpT op, const NUExpr* left, const NUExpr* right,
                      const DynBitVector* rightVal)
{
  bool ret = false;

  if (left && right && (*left == *right))
    // Degenerate A relop A...
    switch (op)
    {
    case NUOp::eBiEq:
    case NUOp::eBiTrieq:
    case NUOp::eBiSLte:
    case NUOp::eBiULte:
    case NUOp::eBiSGtre:
    case NUOp::eBiUGtre:
      *result = true;
      return true;

    case NUOp::eBiSLt:
    case NUOp::eBiULt:
    case NUOp::eBiSGtr:
    case NUOp::eBiUGtr:
    case NUOp::eBiNeq:
    case NUOp::eBiTrineq:
      *result = false;
      return true;

    default:
      break;
    }

  // Try for simple constant compares..

  if (left and rightVal) {
    const NUConst* lc = ctce (left);
    if (lc) {
      DynBitVector leftVal;
      lc->getSignedValue (&leftVal);
      *result = sComputeRelOp (leftVal, *rightVal, op, left->getLoc ());
      return true;
    }
  }

  if (e && mBDDPrescan->isBDDPromising(e)) {
    NU_ASSERT(e->isManaged(), e);
    BDD bdd = mBDDContext->bdd(e);

    if (bdd == mBDDContext->val1 ())
    {
      *result = true;
      return true;
    }
    else if (bdd == mBDDContext->val0 ())
    {
      *result = false;
      return true;
    }
  }

  if (rightVal) {
    // How precise is the rhs?
    size_t rsize = rightVal->findLastOne () + 1;

    size_t size = getCachedEffectiveBitSize(left); // ditto, the left...

    // leftIsHighOnes is true if we know that the MSBs of the left are always set,
    // and there are fewer signficant bits on the right
    //
    bool leftIsHighOnes = (size > rsize) && wideningTwiddle (left);
    bool leftIsNonZero = alwaysNonZero (left);
    bool leftIsPositive = termIsPositive (left);
    bool rightIsNegative = rightVal->test (rightVal->size ()-1); // MSB of right set
    bool rightIsZero = not rightVal->any ();

    if (left->isSignedResult ()) {
      // Only compare against signed things..
      switch (op) {
      case NUOp::eBiEq:
      case NUOp::eBiTrieq:
      case NUOp::eBiULte:
      case NUOp::eBiUGtre:
      case NUOp::eBiULt:
      case NUOp::eBiUGtr:
      case NUOp::eBiNeq:
      case NUOp::eBiTrineq:
      default:
        break;                    // no clue

      case NUOp::eBiSLte:
        if (leftIsPositive && rightIsNegative) {
          *result = false;
          return true;
        } else if (leftIsPositive && leftIsNonZero
                   && rightIsZero) {
          *result = false;
          return true;
        } else if (leftIsPositive && !rightIsNegative && size <= rsize
                   && (rightVal->partsel (0,size).all ()
                       || rightVal->partsel (size, rsize-size).any ())) {
          // eg.  SINT11 <= UINT32
          *result = true;
          return true;
        }
        break;

      case NUOp::eBiSGtre:
        if (leftIsPositive && (rightIsNegative || rightIsZero)){
          *result = true;
          return true;
        } else if (leftIsPositive && !rightIsNegative && size < rsize
                   && rightVal->partsel (size, rsize-size).any ()) {
          *result = false;
          return true;
        }
        break;

      case NUOp::eBiSLt:
        if (leftIsPositive && (rightIsNegative || rightIsZero)) {
          *result = false;
          return true;
        } else if (leftIsPositive && !rightIsNegative && size < rsize
                   && rightVal->partsel (size,rsize-size).any ()) {
          *result = true;
          return true;
        }
        break;

      case NUOp::eBiSGtr:
        if (leftIsPositive && rightIsNegative) {
          *result = true;
          return true;
        } else if (leftIsPositive && ! rightIsNegative && size <= rsize
                   && (rightVal->partsel (0,size).all ()
                       || rightVal->partsel (size, rsize-size).any ())) {
          *result = false;
          return true;
        }
        break;
      }
    } else {
      // No help from BDDs, but Agere has "sel[1:0]<4" which is
      // always true.  Verilog 2001 says that for comparisons the
      // width of the bits is the larger of the left and right side,
      // so we have to be careful about using the self-determined size
      // when we could get a wrong answer.  The 'getCachedEffectiveBitSize()'
      // looks at the underlying expression to determine how many bits might
      // possibly exist
      //
      DynBitVector maxValPlus1(size + 1);       // 3 bits
      maxValPlus1.set(size);                    // maxValPlus1 == 4

      switch (op) {
      case NUOp::eBiSLt:
        //  VHDL will generate signed tests of positive subranges
        if (rightIsZero) {      // U < 0  always false
          *result = false;
          return true;
        }
        break;

      case NUOp::eBiULt:                 // sel < 4
         if (*rightVal >= maxValPlus1) {
          *result = true;
          return true;
        } else if (leftIsHighOnes) {  // {-1,sel} < rightVal
          *result = false;
          return true;
        }
        break;

      case NUOp::eBiULte:                // sel <= 3
        if (*rightVal >= (maxValPlus1 - 1)) {
          *result = true;
          return true;
         } else if (leftIsHighOnes) {  // {-1,sel} <= rightVal
          *result = false;
          return true;
        } else if (leftIsNonZero && rightIsZero) {
          *result = false;
          return true;
        }
        break;

      case NUOp::eBiUGtr:                // sel > 3
        if (*rightVal >= (maxValPlus1 - 1)) {
          *result = false;
          return true;
        } else if (leftIsHighOnes) { // {-1,sel} > 3
          *result = true;
          return true;
         } else if (leftIsNonZero && rightIsZero) {
          *result = true;
          return true;
        }
         break;

      case NUOp::eBiSGtre:
        if (rightIsZero) {
          *result = true;
          return true;
        }
        break;

      case NUOp::eBiUGtre:               // sel >= 4
        if (*rightVal >= maxValPlus1) {
          *result = false;
          return true;
         } else if (leftIsHighOnes) {  // {-1,sel} >= 4
          *result = true;
          return true;
        } else if (leftIsNonZero && rightIsZero) {
          *result = true;
          return true;
        }   
        break;

      case NUOp::eBiTrieq:
      case NUOp::eBiEq:		// Check for LHS unsigned and RHS with significant bits...
        if ((*rightVal >= maxValPlus1)
            || leftIsHighOnes) {    // {-1,sel} == 4
          *result = false;
          return true;
        } else if (leftIsNonZero && rightIsZero) {
          *result = false;
          return true;
        }
        break;

      case NUOp::eBiTrineq:
      case NUOp::eBiNeq:
        if ((*rightVal >= maxValPlus1)
            || leftIsHighOnes) {   // {-1,sel} != 4
          *result = true;
          return true;
        } else if (leftIsNonZero && rightIsZero) {
          *result = true;
          return true;
        }
        break;

      default:
        break;
      } // switch
    }
  }

  if (right) {
    if (wideningTwiddle (left) &&
        (getCachedEffectiveBitSize(right) < left->getBitSize ()))
    {
      // Check for things that look like
      //           ~(x) relop e
      // where we know that ~x is guaranteed to have the MSBs set and 
      // e is positive, thus forcing a sign mismatch
      ret = true;
    } else if (wideningTwiddle (right) &&
               (getCachedEffectiveBitSize(left) < right->getBitSize ()))
    {
      std::swap (left,right);
      op = sReverseOp (op);
      ret = true;
    } else if (const NUConst* xzval = right->castConstXZ ()) {
      // We have a constant mask
      DynBitVector drive, value;
      if (not ignoreXZ ()) {
        xzval->getValueDrive (&value, &drive, false);
        if (drive != 0) {
          // Contains X's or Z's.  A relational involving an 'x' is always false.  We won't
          // encounter this in a CASEX, it's represented by a constant with a separate mask.
          *result = false;      // comparison are always false
          return true;          // and we know the relationships....
        }
      }
    }

    // Trivial evaluation comparing 1 to 0
    switch(op) {
    case NUOp::eBiEq:
    case NUOp::eBiSLt:	
    case NUOp::eBiULt:
    case NUOp::eBiSLte:
    case NUOp::eBiULte:
    case NUOp::eBiTrieq: *result = false; break;

    case NUOp::eBiNeq:
    case NUOp::eBiSGtr:
    case NUOp::eBiUGtr:
    case NUOp::eBiSGtre:
    case NUOp::eBiUGtre:
    case NUOp::eBiTrineq: *result = true; break;
    default: NU_ASSERT("Unknown Rel Op" == 0, left);
    }
  }

  return ret;
} // bool FoldI::evalRelop

// WARNING:  Don't try to eliminate a concat with a single operand
//	reg [1:0] v;
//	reg x;
//	v = {~x};
// is not the same as
//	v = ~x;
// ~x is either 10 or 11, while {~x} is either 00 or 01.
//

// Compare two expressions and see if they can be collapsed into a single
// constant
class CollapsibleConstant
{
public:
  CollapsibleConstant(){};

  //! Binary predicate for adjacent_find()
  bool operator() (const NUExpr *a, const NUExpr *b)
  {
    int ta = a->getType ();
    int tb = b->getType ();

    // Check for constants
    if( (ta == NUExpr::eNUConstXZ) || (ta == NUExpr::eNUConstNoXZ) )
      if( (tb == NUExpr::eNUConstXZ) || (tb == NUExpr::eNUConstNoXZ) )
        return true;

    return false;
  }
};

// Given an expression that is a constant, return the 0,1,X,Z string rep
static UtString sToString (const NUExpr*a)
{
  UtString stringRep;
  DynBitVector v, xy;

  const NUConst* aConst = a->castConst();
  if (! aConst)
    NU_ASSERT (aConst, a);

  if (aConst->hasXZ ())
    aConst->getValueDrive (&v, &xy);
  else
    {
      aConst->getSignedValue (&v);
      xy.resize (v.size ());
      xy=0;
    }

  // Convert to string rep
  CarbonValRW::writeBin4ToStr (&stringRep, &v, &xy);

  return stringRep;
}

// Combine two strings
const NUExpr* FoldI::collapseStrings(const NUExpr *a, const NUExpr *b)
{
  UtString resultString = sToString (a) + sToString (b);
  UInt32 size = a->getBitSize () + b->getBitSize ();
  // And convert to an appropriate constant representation.
  NUExpr *c = NUConst::createConstFromStr (resultString, 
					   size, a->getLoc ());
  c->resize (size);

  return manage(c, false);
}

// Try to combine a whole vector of constant terms...
const NUExpr* FoldI::fastCollapse (const NUConcatOp * cat)
{
  UInt32 endPos = cat->determineBitSize ();
  DynBitVector v (endPos);
  
  for (NUExprCLoop i = cat->loopExprs (); !i.atEnd (); ++i) {
    const NUConst* c = (*i)->castConst ();
    if (not c || c->hasXZ ())
      return 0;                 // some terms not constant.

    UInt32 width = c->getBitSize ();
    endPos -= width;

    DynBitVector value;
    c->getSignedValue (&value);
    v.lpartsel (endPos, width) = value;
  }

  UInt32 initWidth = v.size () - endPos;
  // If there was a repeat count then duplicate the high bits [v.size()-1 : endPos]
  // N-1 times.
  for (SInt32 repeatCount = cat->getRepeatCount ()-1; repeatCount > 0; --repeatCount) {
    v.lpartsel (endPos - initWidth, initWidth) = v.partsel (endPos, initWidth);
    endPos -= initWidth;
  }

  return constant(cat->isSignedResult (), v, cat->determineBitSize (),
                          cat->getLoc ());
}

// Look for {a==3, a==2, a==1, a==0}
bool
FoldI::reduceConcatSwitch (const NUConcatOp* cat, const NUExpr** value) {
  const NUExpr* testVal = 0;
  for(UInt32 i = 0, f = cat->getNumArgs () - 1; i < cat->getNumArgs (); ++i,--f)
  {
    const NUExpr *e = cat->getArg (i);
    if (e->getType () != NUExpr::eNUBinaryOp)
      return false;

    const NUBinaryOp * bop = dynamic_cast<const NUBinaryOp*>(e);
    const NUExpr *lv = bop->getArg (0);
    const NUConst *rv = ctce (bop->getArg (1));

    UInt32 cmpval;
    if (bop->getOp () != NUOp::eBiEq
        || (testVal !=0 && (*testVal != *lv)) // Not all same LHS
        || not rv               	      // nonconstant RHS
        || not rv->getUL (&cmpval) 	      // or not 32 bits or less
        || cmpval != f)         	      // or not the expected value
      return false;

    testVal = lv;               // Remember match
  }

  // If we reach here, all the comparisons were properly formed
  *value = testVal;
  return true;
}

	
//! Recognize various fold special cases for concatenations
const NUExpr*
FoldI::reduceConcat (const NUConcatOp *cat)
{
  // TODO:
  // Look for { 8'b0, x[23:0] } => x[23:0] & mask(24) => x[23:0]
  //          { 8'hff, x[23:0] } => x | (mask(8)<<24)
  //          { x[31:8], 8'b0 } => x & ~mask(8)
  //          { x[31:8], 8'hff } => x | mask(8)
  // Assumes x.getbitsize() == concat.getbitsize().  Must be careful
  // about bit-level UD and concat resizing issues.
  //

  // Look for pattern like
  //    wire [1:0] a;
  //    ...
  //    x = {a==3, a==2, a==1, a==0};
  // and convert to
  //    x = 1<<a;

  const NUExpr* switchExpr = 0;
  if (reduceConcatSwitch (cat, &switchExpr))
  {
    const NUExpr * result = genTrue (cat->getLoc (), cat->getBitSize (), false);
    switchExpr = mExprCache->changeSign(switchExpr, false);
    result = binaryOp(NUOp::eBiLshift, result, switchExpr, cat->getLoc ());

    // retain the repeat-count

    return finish (cat, result);
  }

  // No changes...
  return 0;
}

struct ExpressionAutoFree {
  ExpressionAutoFree(NUCExprHashSet& es): mExprSet(es) {}
  ~ExpressionAutoFree() {
    // free up any locally allocated objects
    std::for_each (mExprSet.begin (), mExprSet.end (), FoldI::cleanupExpr);
  }

  NUCExprHashSet& mExprSet;
};


static bool sWantRepeatCountLUT(UInt32 arg0Size, UInt32 repeatCount) {
  if (arg0Size > 8) {
    // do not create lookup tables deeper than 256
    return false;
  }

  // Intuitively, if I have
  //    1000{a[7:0]}
  // then it's clearly a win.  I have to make a 256x8000 lookup table, which
  // costs some runtime data space, but I avoid a buttload of code to out
  // that repeat-concat at runtime.  But if I am only repeating by a factor
  // of 2:
  //    2{a[7:0]}
  // then the 256x16 lookup table seems like a lot of clutter, compared with
  // the relatively innocuous cost of one shift & OR.
  //
  // I'm not really sure what the right heuristic is.  I'm going to pick
  // a minimum repeat-count of 4 for now.  I'm not sure how the two factors
  // should play together
  if (repeatCount < 4) {
    return false;
  }

  return true;
} // static bool sWantRepeatCountLUT


const NUExpr* FoldI::foldAndManage(const NUExpr* expr, bool copy) {
  expr = mExprCache->insert(const_cast<NUExpr*>(expr), copy);
  (void) foldExprTree(&expr);
  return expr;
}

const NUExpr* FoldI::manage(const NUExpr* expr, bool copy) {
  expr = mExprCache->insert(const_cast<NUExpr*>(expr), copy);
  return expr;
}

/*!
 * For a vector \a v of expressions, make a copy of any expression that's
 * in the set \a ownedExprs.
 */
void
FoldI::copyUnchangedTerms (NUCExprVector* v, NUCExprHashSet* ownedExprs)
{
  // copy any unchanged args
  for (NUCExprVector::iterator k = v->begin (); k != v->end (); ++k)
  {
    const NUExpr* e = *k;
    NUCExprHashSet::iterator p = ownedExprs->find(e);
    if (p != ownedExprs->end())
      ownedExprs->erase(p);
    else
      *k = manage(e, true);
  }
}

struct ExprCompare : public std::binary_function<NUExpr*,NUExpr*,bool> {
  bool operator() (const NUExpr* a, const NUExpr* b) const { return *a == *b; }
};

// Are there at least \a count remaining members of the sequence [i:bound]
static bool sIsSafeSequence (NUCExprVector::iterator i, NUCExprVector::iterator bound, int count)
{
  
  if (count > 0)
    for (; count > 0; --count,++i) {
      if (i == bound)
        return false;
    }

  return true;
}

/*!
 * Find repeated patterns of expressions within the vector v and replace them with
 * repeat-count concats.  Update \a ownedExprs to indicate that expressions within
 * the vector have already been copied
 */
bool
FoldI::factorConcatPatterns (NUCExprVector* v, NUCExprHashSet* ownedExprs, const SourceLocator& loc)
{
  bool changed = false;         // Assume nothing was factorable

  bool shrunk;                  // Remember changes on this iteration

  // Look for subsequences of v, of length 1 .. v->size()/2 to see
  // if there are duplicates that can be factored out.
  //
  do {
    shrunk = false;

    // Start with longest possible sequence length (1/2 of the total concat
    // and work our way down to sequences of length one.
    for (UInt32 sequenceLength = v->size ()/2; sequenceLength >= 1; --sequenceLength)
    {
      // Check every possible sequence beginning
      for (NUCExprVector::iterator seq = v->begin (); seq != (v->end () - sequenceLength); ++seq)
      {
        UInt32 repeatCount = 1; // How many copies of this sequence have we found

        // Check for as many repetitions of [seq..seq+sequenceLength] as we
        // have room for
        NUCExprVector::iterator seqend = seq + sequenceLength;
        NUCExprVector::iterator matchend = seqend;        
        for(int i = (v->end () - seqend)/sequenceLength; i > 0; --i)
        {
          if (std::equal (seq, seqend, matchend, ExprCompare ())) {
            // Count the equal adjacent sequences and remember how large the
            // match is
            ++repeatCount;
            matchend = matchend + sequenceLength;
          } else {
            // This sequence is not repeated any more
            break;
          }
        }

        if (repeatCount > 1) {
          // one or more repeats - make a copy of the basic sequence, set its
          // repeat count and remove all the copies from the vector.
          NUCExprVector group;
          std::copy (seq, seqend, std::back_inserter (group));
          
          // Now recursively shrink The repeated pattern
          factorConcatPatterns (&group, ownedExprs, loc);

          // Whatever's left is unique terms, copy them into a new concat
          //
          copyUnchangedTerms (&group, ownedExprs);

          const NUExpr *e = concatOp(group, repeatCount, loc, 0); // self-determined OK
          *seq++ = e;

          ownedExprs->insert (e); // Mark this as being a copy already

          v->erase (seq, matchend); // Remove the things that were repeats
          changed = shrunk = true;
          goto tryagain;        // Our iterators are invalidated.
        }
      }  // every possible sequence beginning
    } // every length of sequence

  tryagain:
    ;
  } while (shrunk);

  return changed;
}

const NUExpr* FoldI::foldConcatOp(const NUConcatOp *cat) {
  UInt32 nargs = cat->getNumArgs();
  const SourceLocator& loc = cat->getLoc();
  UInt32 repeatCount = cat->getRepeatCount ();
  UInt32 catSize = cat->getBitSize();
  const NUExpr* ret = NULL;

  // find any bottom-up folds first
  {
    bool ret = false;
    NUCExprVector v;
    for (UInt32 i = 0; i < nargs; ++i) {
      const NUExpr* arg = cat->getArg(i);
      ret |= foldExprTree(&arg);
      v.push_back(arg);
    }
    if (ret) {
      return finish(cat, concatOp(v, repeatCount, loc, catSize));
    }
  }

  if (nargs == 0)
    // Empty {} is the same as a zero.
    return finish (cat, genFalse (loc, catSize));

  // Special case all constant member concats
  if (const NUExpr* result = fastCollapse (cat))
    return finish (cat, result);

  NUCExprVector v(nargs);
  UInt32 concatSize = cat->determineBitSize ();

  // If the concat contains a dynamic-ranged partsel - VHDL  x(255 downto j)
  // we have to avoid various truncations because we don't really know how large
  // the varsel will be.
  //
  bool variableSlice = cat->sizeVaries ();

  // Copy the args by pointer into a temp array.  Do not copy by value
  // because if the concat cannot be folded, we don't want to pay the
  // price of doing the copy & delete.  In test/constprop/crc.v, we
  // have an example where we compute a very large expression by
  // following flow, and then fold it.  The expression must be represented
  // by a graph with the aid of NUExprFactory -- it won't fit in memory
  // as a tree.  The graph is folded on construction, and there should
  // be no folding after the fact, and it's critical not to pay for these
  // copies that will not be used, and must be freed.

  for (UInt32 i = 0, vin=0; i < nargs; ++i)
  {
    const NUExpr* arg = cat->getArg(i);

    // But, if the arg is a nested concat, then we do want to flatten the
    // sub-concat.
    //
    if (const NUConcatOp* sub = dynamic_cast<const NUConcatOp*> (arg)) {
      if (sub->getRepeatCount () == 1) {
        v.resize (v.size () + sub->getNumArgs () - 1);
        for (UInt32 j = 0; j < sub->getNumArgs (); ++j) {
          v[vin] =  sub->getArg (j);
          ++vin;
        }

        // we've appended the expansion of the concat, don't
        // push it twice...
        continue;
      }
    }

    v[vin] = arg;
    ++vin;
  }

  UInt32 concatBaseSize = concatSize / repeatCount;;

  // When we do decide we need to reconstruct a concat arg, keep track
  // of which ones we've created so we don't copy them.
  NUCExprHashSet ownedExprs;
  ExpressionAutoFree exprAutoFree(ownedExprs);

  // Collapse constants slowly
  bool combinedConstant = false;
  bool combinedArgs = false;
  while (true)
  {
    NUCExprVector::iterator i = std::adjacent_find (v.begin (), v.end (),
                                                    CollapsibleConstant());
    
    if (i == v.end ())
      break;
    
    // Found two adjacent constants. Collapse them and keep going...
    
    // combine i and i+1 into a single constant
    NUCExprVector::iterator j = i;
    ++j;
    const NUExpr* arg_i = *i;
    const NUExpr* arg_j = *j;
    const NUExpr *c = collapseStrings (arg_i, arg_j);
    ownedExprs.insert(c);
    *i = c;		// Replace with combined constant
    v.erase (j);	// remove empty hole from list
    combinedConstant = true;
  }
  
  // Catenate collapsible expressions
  for (NUCExprVector::iterator i = v.begin ();
       i != v.end ();
       )                        // No incrementer!!!
  {
    if( (i+1) == v.end() )      // Nothing follows our current position
      break;                    // We're done

    
    // Can we combine the current element in the concat with the one that follows
    const NUExpr *e1 = *i;
    const NUExpr *e2 = *(i+1);
    NUExpr *combined = e1->catenate(e2, getCC());

    if(combined)
      // Yes
    {
      v.erase( i+1 );           // Erase the one we combined
      *(i) = combined;
      ownedExprs.insert (combined);
      combinedArgs = true;
    }
    else if (const NUConcatOp* c1 = dynamic_cast<const NUConcatOp*>(e1))
    {
      // Try looking for 
      //        {{K{e1,e2,..}},e1,e2,...} => {K+1{e1,e2,..}}

      NUExprCLoop eBounds (c1->loopExprs ());
      NUCExprVector::iterator matchFrom = i+1;
      UInt32 count = c1->getRepeatCount ();
      UInt32 c1Length = c1->getNumArgs ();
      if (count == 1)
      {
        ++i;                    // Advance to next candidate
        continue;
      }

      if (sIsSafeSequence (matchFrom, v.end (), c1Length) // room enough in sequence
          && std::equal (eBounds.begin (), eBounds.end (), // {e} matches with suffix?
                         matchFrom, ExprCompare ()))
      {
        // {K{e},e}
        v.erase (matchFrom, matchFrom + c1Length); // erase suffix
        NUConcatOp* newCat = c1->copyConcatOp(mCC);
        newCat->putRepeatCount (count + 1);
        newCat->resize (newCat->determineBitSize ());
        *i = manage(newCat, false);
        ownedExprs.insert (newCat);
        continue;               // Keep looking from this new object
      }
      else if (sIsSafeSequence (v.begin (), i, c1Length) // enought room in front
               && std::equal (eBounds.begin (), eBounds.end (), // {e} matches with prefix?
                              i - c1Length,
                              ExprCompare ()))
      {
        // e, {K{e}}
        matchFrom = (i-c1Length);
        NUConcatOp* newCat = c1->copyConcatOp(mCC);
        newCat->putRepeatCount (count+1);
        newCat->resize (newCat->determineBitSize ());
        *(i) = manage(newCat, false);
        ownedExprs.insert (newCat);
        v.erase (matchFrom, i); // Erase prefix
        i = v.begin ();         // Invalidated iterators, let's start over...
        continue;
      }
      else
      {
        ++i;                    // Advance to next element
        continue;
      }
    }
    else
    {
      ++i;                      // Advance to NEXT element
      continue;
    }
  }

  SInt32 excessBits = variableSlice ? 0 : (concatSize - catSize);
  UInt32 removeFromHead = 0;
  bool resizedArg = false, unrolled = false;

  // If there are too many bits, and there is a repeatCount, then
  // we need to unroll the concat to peal off the MSBs until we
  // get to the right size.
  if ((excessBits > 0) && (repeatCount > 1)) {
    // We don't have to unroll the concat all the
    // way.  E.g. if I have {100{a[1:0],b[1:0]}}, which is
    // naturally 400 bits.  Suppose that expression has a context
    // size of 393 -- we need to transform that to
    //    {a[1:0],b[1:0],a[1:0],b[1:0],98{a[1:0],b[1:0]}}
    // which is exactly the same, but can be more easily shortened to
    //    {b[0],98{a[1:0],b[1:0]}}
    // to fit into 393 bits.
    //
    // In this case there would be excessBits=7, and concatBaseSize==4
    // We need to unroll 2 iterations, which we get with this formula:
    UInt32 unrollCount = (excessBits + concatBaseSize - 1) / concatBaseSize;
    NU_ASSERT(unrollCount <= repeatCount, cat);
    UInt32 remainingRepeatCount = repeatCount - unrollCount;
    unrolled = true;

    // If we are left with a small repeat count then we might as
    // well unroll the whole thing.
    UInt32 origSize = v.size();
    if (remainingRepeatCount <= 1) {
      UInt32 newSize = repeatCount * origSize;
      v.resize(newSize);
      for (UInt32 i = origSize; i < newSize; ++i) {
        UInt32 origIndex = i % origSize;
        v[i] = copyExpr(v[origIndex]);
        ownedExprs.insert(v[i]);
      }
      repeatCount = 1;
    } else {
      // First create the vector for the remaining repeat-count
      NUCExprVector v2(origSize);
      for (UInt32 i = 0; i < origSize; ++i) {
        v2[i] = v[i];
      }

      const NUExpr* c2 = concatOp(v2, remainingRepeatCount, loc,
                                  concatBaseSize * remainingRepeatCount);

      // Now increase 'v' to accomodate the extra unrolled copies,
      // plus the new concat we just built.
      UInt32 newSize = unrollCount * origSize;
      v.resize(newSize + 1);    // accomodate the new concat
      for (UInt32 i = origSize; i < newSize; ++i) {
        UInt32 origIndex = i % origSize;
        v[i] = copyExpr(v[origIndex]);
        ownedExprs.insert(v[i]);
      }
      v[newSize] = c2;
      ownedExprs.insert(c2);
      repeatCount = 1;
    }
    resizedArg = true;
  } // if excessBits > 0

  for (size_t i = 0; (i < v.size()) && (excessBits > 0); ++i)
  {
    // Remove bits from the concatenation til the sizes are compatible
    const NUExpr *e = v[i];
    
    if (e->getBitSize () <= (UInt32) excessBits)
      // too small or just right?
    {
      excessBits -= e->getBitSize ();
      ++removeFromHead;
    }
    else
    {
      // this cat-member is larger than the excess, so trim it via partselect
      UInt32 trimmedWidth = e->getBitSize () - excessBits;
      ConstantRange r (trimmedWidth - 1, 0);
      e = createPartsel (e, r, cat->getLoc ());
      ownedExprs.insert(e);
      v[i] = e;
      resizedArg = true;
      break;
    }
  }
  if (removeFromHead != 0)
    v.erase(v.begin(), v.begin() + removeFromHead);

  const NUExpr* arg0 = cat->getArg(0);
  UInt32 arg0Size = arg0->getBitSize();
  const NUExpr* result = NULL;
  bool foldedArg = false;

  if (not ownedExprs.empty ())
    // We modified one-or-more components of this concat.  See if
    // we can further simplify, as we might have created { vec[hi:lo] }
    for (NUCExprVector::iterator i = v.begin (); i != v.end (); ++i)
    {
      if (ownedExprs.find (*i) != ownedExprs.end ())
      {
        // This was something we created.
        const NUExpr* old = *i;
        const NUExpr* expr = foldAndManage(old, false);
        if (old != expr) {
          foldedArg = true;

          // We succeeded in folding it; erase the existing
          ownedExprs.erase (old);
          ownedExprs.insert (expr);
          *i = expr;
        }
      }
    }

  if ((v.size () != cat->getNumArgs()) || resizedArg || combinedConstant ||
      combinedArgs || foldedArg)
  {
    factorConcatPatterns (&v, &ownedExprs, loc);
    // copy any unchanged args
    copyUnchangedTerms (&v, &ownedExprs);
    result = concatOp(v, repeatCount, loc, catSize);
  }

  // Look to see if we have all constants and combine them...
  else if (cat->getNumArgs() == 1 
           && (cat->getArg (0)->getType () == NUExpr::eNUConstNoXZ
               || cat->getArg(0)->getType () == NUExpr::eNUConstXZ))
  {
    // Now apply any repeat factor
    UtString resultString;
    UtString stringRep (sToString (cat->getArg(0)));
    
    for (UInt32 rpt = repeatCount; rpt > 0; --rpt)
      resultString += stringRep;
    
    // And convert to an appropriate constant representation.
    NUConst* k = NUConst::createConstFromStr(resultString, catSize, loc);
    k->resize(catSize);
    result = manage(k, false);
  }
  
  // Look for:  {128'{exp}}.   If exp is 1-bit wide, then
  // I create a conditional expression.  If exp is up to
  // 8 bits wide then I'll create a lookup table, but I
  // can only do that if I know what module to add the
  // lookup memory in, and the initial block to populate it.
  else if ((cat->getNumArgs () == 1)
           && (repeatCount > 1)
           && (repeatCount <= DYNBITVECTOR_MAX_NUM_BITS)
           && ((arg0Size == 1) ||
               (isAggressive() &&
                (sWantRepeatCountLUT(arg0Size, repeatCount)))))
  {
    if (arg0Size == 1) {
      // Always favor "exp ? {128{1'b1}} : {128{1'b0}}", because ?: distributivity
      // will further reduce more complex sequences.
      result = ternaryOp( copyExpr(arg0),
                               genOnes (loc, repeatCount, arg0->isSignedResult ()),
                               genFalse (loc, repeatCount, arg0->isSignedResult ()),
                               loc);
    } else {
      // {99{i[1:0]}} --> lookup_table[i[1:0]]
      // where lookup_table can be a simple constant memory that would have to
      // be declared & initialized in the module
      result = createRepeatCountLUT(cat, arg0, repeatCount, arg0Size, loc);

    }
  } else if (repeatCount > 1
             && concatSize > 1
             && concatSize <= LLONG_BIT // Always for 64 or fewer bit result
             && isAggressive ()) {
    //
    // Generalized repeat via multiply with mask.  Measured as significantly
    // faster for concats between 2..64 bits wide. 
    //
    //  {K{~a[1:0]}}  == magic * {~a[1:0]}
    //
    // Don't do this fold until we have had a chance to merge adjacent similar-sized terms,
    // so we take (assuming a,b are two bits each)
    //    (2{a} | 2{b}) => (2{a|b}) => 5 * (a|b)
    //
    DynBitVector mask (concatSize);
    for(UInt32 i=0; i<repeatCount; ++i)
      mask.set (i*concatBaseSize);

    factorConcatPatterns (&v, &ownedExprs, loc);
    copyUnchangedTerms (&v, &ownedExprs);
    result = binaryOp(NUOp::eBiUMult,
                             concatOp(v, 1, loc, concatSize),
                             constant(false, mask, concatSize, loc),
                             loc);
  }
  
  // Collapse concat with only one element that's already the same size
  // (avoid the {~x} problem for scalar net x....) or that will not
  // produce excess bits if verilog's natural widening rules are applied.
  //
  else if (cat->getNumArgs () == 1
           && not variableSlice
           && repeatCount == 1
           && (getCachedEffectiveBitSize(arg0) <= arg0->determineBitSize ()
               || (catSize <= cat->determineBitSize ())
               || (arg0->isWholeIdentifier () and not arg0->isSignedResult ())))
  {
    const NUExpr *cand = mExprCache->resize(arg0, catSize);
    if (getCachedEffectiveBitSize(cand) <= getCachedEffectiveBitSize(arg0)) {
      result = cand;
    }
  }

  else if (not variableSlice && (ret = reduceConcat (cat)))
    return ret;

  // look for :
  // { (sel ? a : b), (sel ? c : d), (sel ? e : f) }
  // and
  // { (sel ? a : b), c, (sel ? d : e), f }
  // and
  // { (sel1 ? a : b), (sel2 ? c : d), (sel1 ? e : f), (sel2 ? g : h) }
  else if (cat->getNumArgs() > 1) {

    UInt32 saw_this_sel = 0;
    const NUExpr * sel = NULL;
    NUCExprVector then_terms;
    NUCExprVector else_terms;

    then_terms.reserve (cat->getNumArgs ());
    else_terms.reserve (cat->getNumArgs ());

    for (UInt32 i = 0 ; i < cat->getNumArgs() ; ++i ) {
      const NUExpr * arg = cat->getArg(i);
      if (arg->getType()==NUExpr::eNUTernaryOp) {
	const NUTernaryOp * ternary = dynamic_cast<const NUTernaryOp*>(arg);
	const NUExpr * my_sel = ternary->getArg(0);
	if ((not sel) or (sel->equal (*my_sel))) {
	  sel = my_sel;
	  ++saw_this_sel;
	  then_terms.push_back(ternary->getArg(1));
	  else_terms.push_back(ternary->getArg(2));
	} else {
	  // ternary with different selector -- push the full ternary to both branches.
	  then_terms.push_back(arg);
	  else_terms.push_back(arg);
	}
      } else {
	// not a ternary -- push the arg to both branches.
	then_terms.push_back(arg);
	else_terms.push_back(arg);
      }
    }

    // If we only saw one ternary with this selector, do not fold.
    // If the select is only seen once, we double the number of
    // concats in this expression and only eliminate one test. The
    // pathological case is:

    // { (sel[0]?a:b), (sel[1]?c:d), (sel[2]?e:f) ... }

    // Here, we will create 2^N concats (N==number of selectors).
    // Make sure that at least half the elements in the concat involve the
    // selector and there's more than one.
    bool valid = (not then_terms.empty()) and (sel)
      and (saw_this_sel >= (then_terms.size ()/2))
      and (saw_this_sel > 1);

    if (valid) {
      UInt32 internal_size = 0;
      for (NUCExprVector::iterator iter = then_terms.begin();
	   iter != then_terms.end();
	   ++iter) {
	internal_size += (*iter)->getBitSize();
      }

      // functor-ize NUExpr::copy()
      const NUExpr* then_cat = concatOp(then_terms, 1, loc, concatSize);
      const NUExpr* else_cat = concatOp(else_terms, 1, loc, concatSize);

      result = ternaryOp(sel, then_cat, else_cat, loc, internal_size);
      if (repeatCount > 1) {
	NUCExprVector rpt;
	rpt.push_back(result);
	result = concatOp(rpt, repeatCount, loc, concatSize);
      }
    }
  } else {
    // Check to see if there is EXACTLY ONE operand and it's a concat too..

    if (const NUConcatOp* arg = dynamic_cast<const NUConcatOp*>(cat->getArg (0))) {
    //   {K{{M{t1,t2,...}}}} => {K*M{t1,t2,...}}
      repeatCount *= arg->getRepeatCount ();
      NUConcatOp* new_concat = arg->copyConcatOp(mCC);
      new_concat->putRepeatCount (repeatCount);
      result = manage(new_concat, false);
    }
  }

  
  if (result != NULL) {
    result = mExprCache->resize(result, catSize);

    // compare so that if the folding did not change anything, we don't
    // get an infinite recursion crash
    NU_ASSERT (cat->compare(*result, false, false, true) != 0, cat);

    // Beware of any bug which involves shuffling aimlessly by treating 50k
    // deep fold as probably an attempt at an infinite loop
    static int depth = 0;
    ++depth;
    NU_ASSERT(depth < 50000, cat);

    // Allow concat folding to actually reduce the self-determined size
    // of the concat.  This will only happen when we have a context size that
    // is SMALLER than the self-determined size (consider passing a concat as a
    // task input parameter where the formal is smaller than the concat.)
    //
    ret = finish(cat, result, true, false);  // resize, no size assert
    --depth;
  } else if (factorConcatPatterns (&v, &ownedExprs, loc)) {
    // No other optimization had an effect, but the factoring transform
    // found something to simplify.
    copyUnchangedTerms (&v, &ownedExprs);
    
    result =  concatOp(v, repeatCount, loc, concatSize);
    ret = finish (cat, result);
  }
    
  
  return ret;
} // const NUExpr* FoldI::foldConcatOp

const NUExpr* FoldI::foldLut(const NULut* lut) {
  // Eventually we should really fold LUTs in the event that some
  // of the select-bits have become constant, so it can be a smaller
  // LUT.  In the meantime, just do the bottom-up folds.


  // We don't really need to walk down all the N arguments since all
  // but the first one of them are constants.  However we will fold
  // the constant ones here so that they get entered into the touched
  // map (mExprMap) so that a later consistency check will see that
  // they were touched.
  UInt32 numArgs = lut->getNumArgs();
  
  for (UInt32 i = 1; i < numArgs; ++i) { // arg 0 is done below
    const NUExpr* expr = lut->getArg(i);
    bool status = foldExprTree(&expr);
    NU_ASSERT((status == false), expr); // fold should not be able to improve on a constant
  }

  const NUExpr* sel = lut->getArg(0); // now handle arg 0, the non-constant one
  if (foldExprTree(&sel)) {
    NUCConstVector v;
    for (UInt32 i = 1; i < numArgs; ++i) {
      const NUExpr* expr = lut->getArg(i);
      const NUConst* k = expr->castConst();
      NU_ASSERT(k, expr);
      NU_ASSERT(k->isManaged(), expr);
      v.push_back(k);
    }
    return finish(lut, lutOp(sel, v, lut->getLoc(),
                             lut->getBitSize(),
                             specifySign(lut->isSignedResult())));
  }
  return NULL;
}

const NUExpr* FoldI::foldMemsel(const NUMemselRvalue* ms) {
  const NUExpr* id = ms->getIdentExpr();
  bool changed = foldExprTree(&id);
  NUExprVector v;
  for (UInt32 i = 0; i < ms->getNumDims(); ++i) {
    const NUExpr* idx = ms->getIndex(i);
    changed |= foldExprTree(&idx);
    v.push_back(const_cast<NUExpr*>(idx));
  }
  if (changed) {
    return finish(ms, memsel(id, v, ms->getLoc(), ms->getBitSize(), ms->isResynthesized()));
  }
  return NULL;
}

const NUExpr* FoldI::foldSysFuncCall(const NUSysFunctionCall *sys_call) {
  bool changed = false;
  NUExprVector v;
  for (UInt32 i = 0; i < sys_call->getNumArgs(); i++) {
    const NUExpr* e = sys_call->getArg(i);
    changed |= foldExprTree(&e);
    v.push_back(const_cast<NUExpr*>(e));
  }
  if (changed) {
    const NUExpr* nsys =
      sysFuncCall(sys_call->getOp(), sys_call->getTimeUnit(),
                  sys_call->getTimePrecision(), v, sys_call->getLoc(),
                  sys_call->getBitSize(),
                  specifySign(sys_call->isSignedResult()));
    return finish(sys_call, nsys);
  }
  return NULL;
}

// Given an expression in a 1-arg repeat-concat, generate a lookup
// table to accelerate the expression.  This capitalizes on the LUT
// implementation that was made for converting case-statements with
// all constants on the RHS of all assigns.
const NUExpr* FoldI::createRepeatCountLUT(const NUExpr* origExpr,
                                          const NUExpr* expr,
                                          UInt32 repeatCount, UInt32 arg0Size,
                                          const SourceLocator& loc)
{
  UInt32 width = repeatCount * arg0Size;
  NU_ASSERT(arg0Size < 32, expr); // validated already by sWantRepeatCountLUT
  UInt32 depth = 1 << arg0Size;
  DynBitVector bv(width);

  // Construct the table of constants.  Consider 8{x[1:0]}.
  // The table should be:
  //    00: 0000000000000000
  //    01: 0101010101010101
  //    10: 1010101010101010
  //    11: 1111111111111111
  // the NULut constructor expects an NUConstVector so we
  // build DynBitVectors and convert them into unsigned constants.
  NUCConstVector table;
  for (UInt32 repeatExpr = 0; repeatExpr < depth; ++repeatExpr) {
    // Evaluate the repeat-concat 
    bv.reset();
    for (UInt32 j = 0; j < repeatCount; ++j) {
      for (UInt32 k = 0; k < arg0Size; ++k) {
        if ((repeatExpr & (1 << k)) != 0) {
          bv.set(j * arg0Size + k);
        }
      }
    }
    const NUConst* val = constant(false, bv, width, loc);
    table.push_back(val);
  }
  
  return lutOp(expr, table, loc, width,
               specifySign(origExpr->isSignedResult()));
} // const NUExpr* FoldI::createRepeatCountLUT

//! Is the constant \a c an X value?
bool
FoldI::isConstantX (const NUConst* c)
{
  if (not c or not c->hasXZ ())
    return false;

  DynBitVector val, drive;
  c->getValueDrive (&val, &drive);

  // All X requires value of ones and drive of ones
  return (val.count () == val.size ()
          && drive.count () == drive.size ());
}


// construct (e0 || e1) ? x : y
const NUExpr*
FoldI::genQCHelper (const NUExpr* e0, const NUExpr* e1, const NUExpr* x,
                    const NUExpr *y, const SourceLocator& loc)
{
  const NUExpr* orCond = binaryOp(NUOp::eBiLogOr, e0, e1, loc, 1);
  const NUExpr* orCondFolded = foldAndManage(orCond, false);
  const NUExpr* result = ternaryOp(orCondFolded, x, y, loc, x->getBitSize ());
  return result;
}

//
// Careful: Verilog allows wide selectors (not a boolean-value), ala:
//
//      wire [31:0] ena, [31:0] x, [31:0] y;
//      y = (~ena) ? x : 31'bz;
//
const NUExpr* FoldI::foldTernaryOp(const NUTernaryOp *te) {
  if (te->getOp () != NUOp::eTeCond)
    // We only know about one ternary operator...
    return 0;

  // Before bottom-up folding, look for FFO/FFZ patterns
  if (!isBottomUp()) {
    const NUExpr* repl = qcFindBit(te);
    if (repl != NULL) {
      return repl;
    }
  }

  const NUExpr *cond = te->getArg (0);
  const NUExpr *thenClause = te->getArg (1);
  const NUExpr *elseClause = te->getArg (2);
  const SourceLocator& loc = te->getLoc ();

  // find any bottom-up folds first
  {
    bool ret = foldExprTree(&cond);
    ret |= foldExprTree(&thenClause);
    ret |= foldExprTree(&elseClause);
    if (ret) {
      return finish(te, ternaryOp(cond, thenClause, elseClause, loc));
    }
  }

  // When the condition is an 'x', evaluate as false
  // if either leg of ?: is an X, replace by the other leg
  const NUExpr* activeLeg = NULL;
  if (isConstantX (cond->castConstXZ ()))
    activeLeg = elseClause;
  else if (isConstantX (elseClause->castConstXZ ()))
    activeLeg = thenClause;
  else if (isConstantX (thenClause->castConstXZ ()))
    activeLeg = elseClause;
  else if (const NUConst* c = ctce (cond))
    activeLeg = c->isZero () ? elseClause : thenClause;

  if (activeLeg) {
    return finish (te, mExprCache->makeSizeExplicit(activeLeg));
  }

  // Check for equivalent operators in then and else clauses.  Note that
  // because of potential widening, we use NUExpr::operator==(), not 
  // NUExpr::equal().

  const NUExpr* e1 = te->getArg(1);
  const NUExpr* e2 = te->getArg(2);
  if ((*e1) == (*e2))
    return finish(te, copyExpr(e1));

  // Check for silly special cases
  //    e?1:0 => e!=0
  //    e?0:1 => !e
  const NUConst* ce1 = ctce (e1);
  const NUConst* ce2 = ctce (e2);

  if (te->getBitSize () == 1 && ce1 && ce2)
  {
    if (ce1->isOnes () && ce2->isZero ()) {
      return finish (te, genLogical(cond));
    } else if (ce1->isZero () && ce2->isOnes ()) {
      // (expr) ? 1'b0 : 1'b1 => !(expr)
      return finish (te, unaryOp(NUOp::eUnLogNot, copyExpr (cond), loc));
    }
  }

  // Not constant

  const NUConst *cr = te->getArg(2)->castConst();
  const NUConst *cl = te->getArg(1)->castConst();

  if (not ignoreXZ ()
      and ((cr and cr->drivesZ ())
           or (cl and cl->drivesZ ())))
    // Don't try and mess with Z's if Fold constructor doesn't allow
    ;
  else if (getCachedEffectiveBitSize(te) <= 1 &&
           getCachedEffectiveBitSize(cond) <= 1)
  {
    const NUExpr* result = 0;
    switch (cond->getType ())
    {
    case NUExpr::eNUIdentRvalue:
    case NUExpr::eNUVarselRvalue:
    case NUExpr::eNUBinaryOp:
    case NUExpr::eNUUnaryOp:
      if (cr && cr->isZero ())
      {
        // (a ? b:0)  => a&b
        result = binaryOp(NUOp::eBiBitAnd,
                                 copyExpr(cond),
                                 copyExpr(thenClause),
                                 loc);
      }
      else if (cl && cl->isZero ())
      {
        // (a ? 0:b)  => (!a)&b
        const NUExpr* cond_ = copyExpr(genComplement(cond));
        const NUExpr* sizedCond = padExpr(cond_, elseClause->getBitSize());
        const NUExpr* mngCond = foldAndManage(sizedCond, false);
        result = binaryOp(NUOp::eBiBitAnd, mngCond, elseClause, loc);
      }
      else if (cr && cr->isOnes ())
      {
        // (a ? b:1)  => (!a) | b
        result = binaryOp(NUOp::eBiBitOr,
                                 genComplement (cond),
                                 thenClause,
                                 loc);
      }
      else if (cl && cl->isOnes ())
      {
        // (a ? 1:b)  => (a | b)
        result = binaryOp(NUOp::eBiBitOr,
                                 copyExpr(cond),
                                 copyExpr(elseClause),
                                 loc);
      }
      break;
      
    default:
      break;
    }

    if (result) {
      result = padExpr (result, te->determineBitSize ());
      return finish (te, result);
    }
  }


  
  // Look for:   e0 ? v : e1 ? v : x => (e0 || e1) ? v : x   (A)
  //             e0 ? v : e1 ? x : v => (e0 || !e1) ? v : x  (B)
  //             e0 ? e1 ? v : x : v => (!e0 || e1) ? v : x  (C)
  //             e0 ? e1 ? x : v : v => (!e0 || !e1)? v : x  (D)
  if (const NUTernaryOp* leftTernary =
      dynamic_cast<const NUTernaryOp*>(thenClause)) {
    if (leftTernary->getOp () == NUOp::eTeCond && cond->getBitSize () == 1) {
      if (*(leftTernary->getArg (1)) == *(elseClause)) // (C)
        return finish (te,
                       genQCHelper (genComplement (cond),
                                    leftTernary->getArg (0),
                                    elseClause,
                                    leftTernary->getArg (2), loc));
      else if (*leftTernary->getArg (2) == *(elseClause)
        && leftTernary->getArg (0)->getBitSize () == 1) // (D)
        return finish (te,
                       genQCHelper (genComplement (cond),
                                    genComplement (leftTernary->getArg (0)),
                                    elseClause,
                                    leftTernary->getArg (1), loc));
    }

  } else if (const NUTernaryOp* rightTernary =
             dynamic_cast<const NUTernaryOp*>(elseClause))
  {
    if (rightTernary->getOp () == NUOp::eTeCond) {
      if (*(rightTernary->getArg (1)) == *(thenClause)) // (A)
        return finish (te, genQCHelper (cond,
                                        rightTernary->getArg (0),
                                        thenClause,
                                        rightTernary->getArg (2), loc));
      else if (*rightTernary->getArg (2) == *(thenClause)
        && rightTernary->getArg (0)->getBitSize () == 1) // (B)
        return finish (te, genQCHelper (cond,
                                        genComplement (rightTernary->getArg (0)),
                                        thenClause,
                                        rightTernary->getArg (1), loc));
    }
  }


  // Don't do any of the following optimizations too early
  if ( isPreservedReset ())
    return NULL;

  // replace != in conditional test by == and swap operands
  // (unless we're comparing to an 'x', in which case leave the
  // sense of the test as we found it, when we get to folding the
  // relational, we'll decide it's always false.
  switch (cond->getType ())
    {
    case NUExpr::eNUBinaryOp:
      {
	const NUBinaryOp* bcond = dynamic_cast<const NUBinaryOp*>(cond);
	if (bcond->getOp () == NUOp::eBiNeq
            && !isConstantX (bcond->getArg (0)->castConstXZ ())
            && !isConstantX (bcond->getArg (1)->castConstXZ ()))
	  {
            const NUExpr* ncond = binaryOp(NUOp::eBiEq,
                                           bcond->getArg (0),
                                           bcond->getArg (1),
                                           bcond->getLoc (), 1);
	    const NUExpr* result = ternaryOp(foldAndManage(ncond, false),
                                             elseClause, thenClause,
                                             loc);
	    return finish (te, result);
	  }
      }
      break;

    case NUExpr::eNUUnaryOp:
      {
	const NUUnaryOp* ucond = dynamic_cast<const NUUnaryOp*>(cond);
	if (ucond->getOp () == NUOp::eUnLogNot
          && ! isConstantX (ucond->getArg (0)->castConstXZ ()))
	  {
	    const NUExpr* result = ternaryOp(ucond->getArg (0),
                                             elseClause, thenClause, loc);
	    return finish (te, result);
	  }
      }
      break;

    default:
      break;
    }
  
  // assign out1 = sel? in: ~in;  -> ~(sel ^ in)
  // assign out2 = sel? ~in: in;   -> (sel ^ in)
  //
  // Note that we prefer "~(sel ^ in)" to "(sel ^ ~in)" because
  // having the inversion as the outer operator allows us to find it
  // more easily if the result of that is in turn inverted:
  //    ~(~(sel ^ in)) == sel ^ in
  // is easy, but
  //    ~(sel ^ ~in) == sel ^ in
  // is less obvious
  const NUExpr* a0 = te->getArg(0);
  const NUExpr* a1 = te->getArg(1);
  const NUExpr* a2 = te->getArg(2);
  if ((a0->getBitSize() == 1) &&
      (a1->getBitSize() == 1) &&
      (a2->getBitSize() == 1))
  {
    const NUUnaryOp* uop1 = dynamic_cast<const NUUnaryOp*>(a1);
    const NUUnaryOp* uop2 = dynamic_cast<const NUUnaryOp*>(a2);
    const NUExpr* xorOp1 = NULL;
    bool invert = false;

    // assign out1 = sel? in: ~in;  -> ~(sel ^ in)
    if (isComplement(uop2) && (*uop2->getArg(0) == *a1))
    {
      xorOp1 = a1;
      invert = true;
    }

    // assign out2 = sel? ~in: in;   -> (sel ^ in)
    else if (isComplement(uop1) && (*uop1->getArg(0) == *a2))
    {
      xorOp1 = a2;
    }
      
    if (xorOp1 != NULL)
    {
      const NUExpr* result = binaryOp(NUOp::eBiBitXor,
                                      copyExpr(a0), copyExpr(xorOp1),
                                      te->getLoc(), 1);
      if (invert)
        result = genInverted (result);

      return finish (te, result);
    }
  } // if

  return NULL;
} // FoldI::fold


const NUExpr*
FoldI::reduceRealExpr (const NUBinaryOp* bop)
{
  const NUConst *ac = ctce (bop->getArg (0)),
                *bc = ctce (bop->getArg (1));

  // We could do some trivial folds like x*1.0 or y+0.0
  // without breaking semantics of real numbers, but this isn't
  // likely to occur in real programs, so for now just require
  // both operands to be constant.
  if (not ac or not bc)
    return NULL;
  
  CarbonReal a,b;
  if (not ac->getCarbonReal (&a) || not bc->getCarbonReal (&b))
    return NULL;

  const SourceLocator& loc = bop->getLoc ();

  bool logresult;
  switch (bop->getOp ()) {
  case NUOp::eBiPlus:	a += b; break;
  case NUOp::eBiMinus:	a -= b; break;
  case NUOp::eBiSMult:	a *= b; break;
  case NUOp::eBiSDiv: {
    if (b == 0.0)
      a = 0.0;                  // undefined treat 
    else
      a /= b;
    break;
  }
  case NUOp::eBiDExp:	a = std::pow (a,b); break;

  case NUOp::eBiSLt:     logresult = a < b; goto logic;
  case NUOp::eBiSLte:	logresult = a <= b; goto logic;
  case NUOp::eBiSGtr:	logresult = a > b; goto logic;
  case NUOp::eBiSGtre:	logresult = a >= b; goto logic;
  case NUOp::eBiEq:	logresult = a == b; goto logic;
  case NUOp::eBiNeq:	logresult = a != b; goto logic;
  case NUOp::eBiLogAnd:	logresult = (a!=0.0) && (b!=0.0); goto logic;
  case NUOp::eBiLogOr:	logresult = (a!=0.0) || (b!=0.0); goto logic;
  default: NU_ASSERT (0, bop);
  }

  // a contains the real value result
  return manage(NUConst::create(a, loc), false);

 logic:
  // expression results in a boolean valued result
  return logresult ? FoldI::genTrue (loc) : FoldI::genFalse (loc);
}

// Convert an expression of the form ([0..10] == i)
const NUExpr*
FoldI::expandDownTo (const NUBinaryOp* downTo, const NUExpr* e, NUOp::OpT op)
{
  NU_ASSERT (op == NUOp::eBiEq || op == NUOp::eBiNeq, downTo);

  bool sign = downTo->isSignedResult ();

  const NUExpr *termHigh = binaryOp(sign ? NUOp::eBiSLte : NUOp::eBiULte,
                                    e, downTo->getArg (0), downTo->getLoc ());
  const NUExpr *termLow = binaryOp(sign ? NUOp::eBiSGtre : NUOp::eBiUGtre,
                                   e, downTo->getArg (1), downTo->getLoc ());
  const NUExpr* ret = binaryOp(NUOp::eBiLogAnd, termHigh, termLow,
                               downTo->getLoc ());
  if (op == NUOp::eBiNeq)
    ret = unaryOp(NUOp::eUnLogNot, e, e->getLoc ());

  return ret;
}

// Match patterns that look like 
//      e1 <op1> (e1 <op2> b)
//
// Interesting cases are:
//
//    a  &  (a|b) => a
//    a  |  (a&b) => a
//    a  |  (a^b) => a | b
//    a  &  (a^b) => a & ~b
//    a  ^  (a|b) => ~a & b
//    a  ^  (a&b) => a & ~b
//
const NUExpr*
FoldI::reduceBoolIdent (NUOp::OpT op1, NUOp::OpT op2,
                        const NUExpr* e1, const NUExpr* e2, const NUExpr* root)
{
  const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(e2);
  if (not bop || bop->getOp () != op2)
    return NULL;

  const NUExpr* aterm = e1;
  const NUExpr* bterm;

  if ((*aterm) == *(bop->getArg (0)))
    bterm = bop->getArg (1);
  else if ((*aterm) == *(bop->getArg (1)))
    bterm = bop->getArg (0);
  else
    return NULL;                // no matches

  const NUExpr* result = NULL;
  switch (op1) {
  case NUOp::eBiBitAnd:
    if (op2 == NUOp::eBiBitOr)
      result = aterm;
    else
      result = binaryOp(op1, aterm, genComplement (bterm), e2->getLoc());
    break;

  case NUOp::eBiBitOr:
    if (op2 == NUOp::eBiBitAnd)
      result = aterm;
    else
      result = binaryOp(op1, aterm, bterm, e2->getLoc ());
    break;

  case NUOp::eBiBitXor:
    if (op2 == NUOp::eBiBitOr)
      result = binaryOp(NUOp::eBiBitAnd, genComplement (aterm),
                               bterm, e2->getLoc ());
    else
      result = binaryOp(NUOp::eBiBitAnd, aterm,
                               genComplement (bterm),
                               e2->getLoc ());
    break;
                             
  default:
    NU_ASSERT (false, e1);
  }

  // We could change the size of the computation, and must protect against that
  if (result && result->determineBitSize () != root->determineBitSize ())
  {
    result = mExprCache->resizeSign(result, root->getBitSize(), e1->isSignedResult());
    result = mExprCache->makeSizeExplicit(result);
  }

  return result;
}

// create a 64 bit value that is a sign extended version of \a val (where val is \a size bits)
SInt64 sExtendConstant(UInt32 size, UInt64 val)
{
  INFO_ASSERT( ((0 < size) && (size <=64)), "sExtendConstant: the argument named size has a value that is too large.");
  UInt32 sign_bit_index = size - 1;
  SInt64 ret_val;
  UInt64 sign_bit_mask = 1ULL << sign_bit_index;
  bool val_is_negative = val & sign_bit_mask;
  if ( val_is_negative && (size < 64) ) {
    UInt64 value_mask = sign_bit_mask - 1;
    UInt64 sign_extend_mask = ~ value_mask;
    ret_val = val | sign_extend_mask;
  } else {
    ret_val = (SInt64) val;
  }
  return ret_val;
}

const NUExpr* FoldI::foldBinaryOp(const NUBinaryOp *bop)
{
  UInt32 bop_size = bop->getBitSize();
  bool bop_sign = bop->isSignedResult ();
  NUOp::OpT op = bop->getOp ();


  // Before bottom-up folding, look for associativity and FFO/FFZ patterns
  if (!isBottomUp()) {
    const NUExpr* r = reassociate(bop);
    if (r != NULL) {
      // Expr-to-LUT on test/lut/bit_hack6.v yields this
      // expression: ((~4'h0) & 4'h0), which re-associates
      // to itself.  Al should figure out why that is, but
      // until he does, I need to be sure re-associate did
      // something useful, otherwise 'finish' will assert.
      if (r != bop) {
        return finish (bop, r);
      }
    }
    if (isAggressive ()) {
      if (const NUExpr* result = reduceOpOver (bop))
        return finish (bop, result);
    }

    // Arithmetic simplification is functionally correct across the test suite,
    // but does bad things to strip-mining and vectorization in 
    //    test/codegen/bug5506.v  -- size diff -- stripming
    //    test/assign/vectorize.v -- cbuild diff -- vectorization.  I think the
    //        testcase in test/assign is vector-04.v.
    // So for now it is only called on request for now, from
    // localflow/DowntoResynth.cc
    if ((mFlags & eFoldSimplifyArithmetic) != 0) {
      r = combineArithmetic(bop);
      if (r != bop) {
        return finish(bop, r);
      }
    }
  }

  const NUExpr *e1 = bop->getArg (0);
  const NUExpr *e2 = bop->getArg (1);
  const SourceLocator loc = bop->getLoc ();

  // find any bottom-up folds first
  {
    bool ret = foldExprTree(&e1);

    // In general, due to bug6688, we can't let arithmetic
    // folding disturb patterns looked for by vectorization.
    // However, it is likely in VHDL designs, following copy
    // propagation, to find (1<<((size + 32) - size)), and
    // it's highly desirable to fold that to 1<<32.  So let's
    // turn on arithmetic folding for e2 if we are in a <<
    // See test/vhdl/lang_misc/dyn_slice9.vhd with -newSizing.
    UInt32 save_flags = mFlags;
    if (op == NUOp::eBiLshift) {
      mFlags |= eFoldSimplifyArithmetic;
    }
    ret |= foldExprTree(&e2);
    mFlags = save_flags;
    if (ret) {
      return finish(bop, binaryOp(bop->getOp(), e1, e2, loc, bop_size));
    }
  }

  // Check for bit operations that can be reduced to reduction operators
  // Don't do this until after schedule, so that it doesn't obfuscate
  // UD information.  Consider:
  // 	a[3] = (a[1] | a[2])	=>  a[3] = (a & 3) != 0;
  // but UD thinks that there's a dependency between the LHS and RHS and
  // schedule will double-call the containing block.
  //
  // This also interferes with vector inferencing.
  //

  if (e1->isReal ())
    if (e2->isReal ())
      if (const NUExpr* result = reduceRealExpr (bop)) // try folding R <op> R
        return finish (bop, result);
      else
        return NULL;
    else
      return NULL;
  else if (e2->isReal ())
    return NULL;
  
  // No real operands present...

  if (*e1 == *e2)		// Equivalent operands on each side...
    {
      if (isIdempotent (op))
	// x op x == x
      {
        const NUExpr* result = genLogical (e1, op);
	return finish (bop, result);
      }
      else if (op == NUOp::eBiBitXor)
        // a ^ a => 0
        return finish (bop, genFalse (loc, bop_size));

      else if (zeroReduction (op))
	// x op x == 0
	return finish (bop, genFalse (loc, bop_size));

      else if (isRelationalOp (op))
	{
	  switch(op) {
	  case NUOp::eBiEq:
	  case NUOp::eBiSLte:
          case NUOp::eBiULte:
	  case NUOp::eBiSGtre:
          case NUOp::eBiUGtre:
	    return finish (bop, genTrue (loc, bop_size));

	  case NUOp::eBiSLt:
          case NUOp::eBiULt:
	  case NUOp::eBiSGtr:
          case NUOp::eBiUGtr:
	  case NUOp::eBiNeq:
	    return finish (bop, genFalse (loc, bop_size));

	  default:
	    break;
	  }
	}
    }

  // now check for zeroReduction: if a is unsigned then ({n'b0,a} op a) => 0
  if (zeroReduction (op)){
    if ( e1->getType() == NUExpr::eNUConcatOp ) {
      const NUConcatOp * cop = dynamic_cast<const NUConcatOp*>(e1);
      if ( (cop->getRepeatCount()==1) and
           (cop->getNumArgs() == 2) and
           (getCachedEffectiveBitSize(e1) == getCachedEffectiveBitSize(e2)) and
           not e2->isSignedResult() and
           (cop->getArg(1) == e2 ) ) {
	// {n'b0,x} op x == 0
	return finish (bop, genFalse (loc, bop_size));
      }
    }
  }
  

  // Canonicalize the tree if possible (with a constant as right op)
  bool swapped = false;
  if (not ctce(e2) && ctce(e1) && (isCommutative (op) || isRelationalOp (op)))
  {
    std::swap (e1, e2);
    op = sReverseOp (op);
    swapped = true;
  }

  // Check for constant operands.
  const NUConst *lp = ctce (e1);
  const NUConst *rp = ctce (e2);

  const NUExpr* shrunkBop = maybeShrinkOperands(e1, e2, op, bop_size, loc);
  if ( shrunkBop != NULL ) {
#if 0
    UtString oldBuf, newBuf, locBuf;
    bop->compose(&oldBuf, NULL);
    shrunkBop->compose(&newBuf, NULL);
    loc.compose(&locBuf);
    UtIO::cout() << "** Reducing constant Exp=" << bop_size
                 << " Leff=" << getCachedEffectiveBitSize(e1)
                 << " Reff=" << getCachedEffectiveBitSize(rp)
                 << " Lbs=" << e1->getBitSize()
                 << " Rbs=" << rp->getBitSize()
                 << " Lds=" << e1Size
                 << " : " << oldBuf
                 << " -> " << newBuf
                 << " @ " << locBuf
                 << "\n";
    UtIO::cout().flush();
#endif

    return finish(bop, shrunkBop, true);
  }



  // Check for BIT==(1|0) or BIT!=(1|0) and simplify
  if (!rp                       // RHS not a constant
      || (not ignoreXZ () and rp->drivesZ ()) // ZX are still being tracked
      || e1->getBitSize () != 1)
    ;                           // Don't even think about it!
  else
    {
      DynBitVector rvalue;
      rp->getSignedValue (&rvalue);
	
      bool isBool = (rvalue == 0 || rvalue == 1);

      switch (op) {
      case NUOp::eBiEq:
	if (not isBool)
	  // (BIT == 2) => FALSE
	  return finish (bop, genFalse (rp->getLoc ()));

	// a == 0 => !a
	// a == 1 => a
	{
	  const NUExpr* result = e1;
	  if (rp->isZero ())
	    // a == 0
	    result = unaryOp(NUOp::eUnLogNot, result, result->getLoc (), 1);
          else {
            result = mExprCache->resize(result, 1);
          }
	  return finish (bop, result);
	}

      case NUOp::eBiNeq:
	if (not isBool)
	  // (BIT != 2)  => TRUE
	  return finish (bop, genTrue (rp->getLoc ()));

	// a != 0 => a
	// a != 1 => !a
	{
	  const NUExpr* result = e1;
	  if (not rp->isZero ())
	    // a != 1
	    result = unaryOp(NUOp::eUnLogNot, result, result->getLoc (), 1);
          else {
            result = mExprCache->resize(result, 1);
          }
	  return finish (bop, result);
	}

      default:
	break;
      }
    }

  // Check for relational operators that can be swapped if we convert
  // the relational test (e.g. "0 < a" becomes "a > 0"

  // Get the potential constant values
  DynBitVector rv (e2->getBitSize ());
  if (rp)
    rp->getSignedValue (&rv);

  DynBitVector lv (e1->getBitSize ());
  if (lp)
    lp->getSignedValue (&lv);

  // Checks for random folds
  bool condTest;
  if (isRelationalOp (op))
  {
    // Look for partsel compares that can be simplified
    if (const NUExpr* result = reducePartselCompares (bop))
      return finish (bop, result);

    // First check for invertable unary operators like -,~ applied to relational terms.
    // You'd like to do things like -X > 5 => X < -5; but this actually requires computing
    // the constant to a higher precision to deal with overflows; consider the pathology of
    //   X > -32768 where X is 16 bits.  This is true for all X, except when X is 0x8000.
    // But the inverse X < 32768 in 16 bits looks like X<-32768 which is FALSE for all X
    //
    if (not isSignedOp (op) and not isUnsignedOp (op)) { // ==, != , ===, !==
      const NUUnaryOp* u1 = dynamic_cast<const NUUnaryOp*>(e1);
      const NUUnaryOp* u2 = dynamic_cast<const NUUnaryOp*>(e2);
      NUOp::OpT candOp = u1 ? u1->getOp () : NUOp::eInvalid;

      if (u1 && (candOp == NUOp::eUnMinus || candOp == NUOp::eUnBitNeg)) {
        if (rp) {
          // -E <relop> CONST  => E <relop> -CONST
          const NUExpr * neg_const = unaryOp(candOp, copyExpr (e2), loc,
                                             e2->getBitSize());
          const NUExpr* result = binaryOp(op,
                                           copyExpr(u1->getArg (0)),
                                           copyExpr(foldAndManage(neg_const, false)),
                                           loc);
          return finish (bop, result);
        } else if (u2 && u2->getOp () == candOp) {
          // -E <relop> -F  => E <relop> F
          const NUExpr* result = binaryOp(sReverseOp (op),
                                           copyExpr (u1->getArg (0)), copyExpr (u2->getArg (0)),
                                           loc);
          return finish (bop, result);
        }
      }
    }

    DynBitVector* rightVal = (rp && (ignoreXZ () || not rp->drivesZ()))? &rv: 0;
    if (evalRelop (bop, &condTest, op, e1, e2, rightVal))
    {
      const NUExpr* result = condTest ? genTrue (loc, bop_size, false)
        : genFalse (loc, bop_size, false);

      return finish (bop, result);
    } else if (ignoreXZ ()) {
      if (op == NUOp::eBiTrieq)
        return finish (bop, binaryOp(NUOp::eBiEq,
                                            copyExpr (e1),
                                            copyExpr (e2),
                                            loc, bop_size));
      else if (op == NUOp::eBiTrineq)
        return finish (bop, binaryOp(NUOp::eBiNeq,
                                            copyExpr (e1),
                                            copyExpr (e2),
                                            loc, bop_size));
    }

    // look for compares to constants that can be simplified
    //      ~a == 5 => a == ~5
    //     a+5 == 6 => a == 6-5
    //     5 - a = b => 5 == a+b, and if 'b' is constant we end up with 5-b == a
    // note that because of canonicalized expressions we don't have to test for a-const,
    // or for const + a.
    //
    const NUUnaryOp *uop = dynamic_cast<const NUUnaryOp*>(e1);
    const NUExpr* result;
    const NUConst* offset;

    if (!lp && rp && (op == NUOp::eBiEq || op == NUOp::eBiNeq)) {
      // look for ~a == 5, but don't be fooled by "~0 != 0"
      // or by (1 == (5 - 6))
      //
      UInt32 termSize = e1->getBitSize ();
      if (uop && isComplement (uop) && ! uop->getArg (0)->castConst ()) {
        const NUExpr* n2 = unaryOp(uop->getOp(), e2, e2->getLoc ());
        const NUExpr* n1 = copyExpr (uop->getArg (0));
        n1 = mExprCache->changeSign(n1, bop->isSignedResult ());
        n2 = mExprCache->changeSign(n2, bop->isSignedResult ());
        return finish (bop, binaryOp(op, n1, n2, loc, bop_size));
      } else if (getConstantOffset (e1, &result, &offset, false)) {
        const NUExpr* e2offset = binaryOp(NUOp::eBiMinus, e2, offset,
                                          e2->getLoc (), e2->getBitSize(),
                                          specifySign(bop->isSignedResult()));
        e2 = foldAndManage(e2offset, false);
        result = binaryOp(op, foldAndManage(result, false), e2, loc);
        return finish (bop, result);
      } else if (const NUBinaryOp* inverseCandidate =
                 dynamic_cast<const NUBinaryOp*>(e1)) {
        if (inverseCandidate->getOp () == NUOp::eBiMinus) {
          // looking at (a-b) == const, convert to a == b + const, and if we're
          // lucky, and 'a' is constant, we'll end up with b == (a-const)
          if (rp->isZero ())
            e2 = inverseCandidate->getArg (1);
          else {
            const NUExpr* n2 = binaryOp(NUOp::eBiPlus,
                                        inverseCandidate->getArg (1),
                                        e2, loc, termSize,
                                        specifySign(bop->isSignedResult()));
            e2 = foldAndManage(n2, false);
          }
          result = binaryOp(op, inverseCandidate->getArg(0), e2, loc);
          return finish (bop, result);
        }
      } else if (e1->getType() == NUExpr::eNUConcatOp) {
        // Try {a,b,c} == constant
        const NUConcatOp * cop = dynamic_cast<const NUConcatOp*>(e1);
        // 1. Repeat counts can be folded, but they're more cumbersome.
        // 2. Make sure that the concat is at least as wide as our constant.
        if ( (cop->getRepeatCount()==1) and
             (e1->determineBitSize() >= e2->getBitSize()) ) {
          ConstantRange range(-1,-1);
          result = NULL;
          for (SInt32 idx = cop->getNumArgs()-1; idx >= 0; --idx) {
            const NUExpr * arg = cop->getArg(idx);
            range = ConstantRange(range.getMsb()+arg->getBitSize(),range.getMsb()+1);
            const NUExpr * part = binaryOp(op, copyExpr(arg),
                                           foldPartsel(rp, 0, range, rp->getLoc()),
                                           rp->getLoc());
            if (result) {
              // {a,b}==0 <==> a==0 && b==0
              // {a,b}!=0 <==> a!=0 || b!=0
              NUOp::OpT combine_op = (op==NUOp::eBiEq) ? NUOp::eBiLogAnd : NUOp::eBiLogOr;
              result = binaryOp(combine_op, result, part, bop->getLoc());
            } else {
              result = part;
            }
          }
          NU_ASSERT(result,bop);
          return finish(bop,result);
        }
      }
      // TODO:
      // check for (a & mask) == 0 and for sparse multibit masks,
      // replace by |(a&mask) == 1'b0.  The reduction or is a candidate for
      // breaking into piecewise words either in strip-mining or in
      // genReduction.  [cf. BUG3776]
    }

    // Check for  a == (10 downto 0) and the like...  This MUST come before
    // distributing ops over downto...
    if (const NUBinaryOp* lbop = dynamic_cast<const NUBinaryOp*>(e1)) {
      if (lbop->getOp () == NUOp::eBiDownTo)
        return finish (bop, expandDownTo (lbop, e2, op));
    } 
    if (const NUBinaryOp* rbop = dynamic_cast<const NUBinaryOp*>(e2)) {
      if (rbop->getOp () == NUOp::eBiDownTo)
        return finish (bop, expandDownTo (rbop, e1, sReverseOp (op)));
    }

    // Test for foldable operations against zero...
    if (rightVal && *rightVal == 0)
      switch (op) {
      case NUOp::eBiULt:
        // X < 0 => false for all unsigned X
        return finish (bop, genFalse (loc, 1, false));
        
      case NUOp::eBiUGtre:
        //  x >= 0 => true
        return finish (bop, genTrue (loc, 1, false));

      case NUOp::eBiULte:
        // X <= 0 => X==0
        return finish (bop, binaryOp(NUOp::eBiEq, e1, e2, loc, bop_size));

      case NUOp::eBiUGtr:
        // X > 0 => X != 0
        return finish (bop, binaryOp(NUOp::eBiNeq, e1, e2, loc, bop_size));

      default:
        break;
      }
  }

  // Check for logical operation with a concat of constants (a128&{96'b0,b32}),
  // and strip-mine the expression, distributing the binary operation to
  // each 32-bit word.  When there are constants that span the whole 32 bit
  // word then this is a nice win because the operation can be precomputed
  // for that word.
  if ((bop_size > 64) && isBitwiseOp(op) && !bop_sign &&
      ((dynamic_cast<const NUConcatOp*>(e1) != NULL) ||
       (dynamic_cast<const NUConcatOp*>(e2) != NULL)))
  {
    if (const NUExpr* result = stripMineConcat(bop)) {
      return finish(bop, result);
    }
  }

  // Check for DeMorgan's Law
  if (const NUExpr* result = DeMorgan (bop))
    return finish (bop, result);

  // Simplify a&1=a, a|0=a
  if (const NUExpr* result = reduceLogical (bop))
    return finish (bop, result);

  // Try simplifying bool <op> expr
  if (const NUExpr* result = reduceBoolOpExpr (bop))
    return finish (bop, result);

  // Look to convert  (e?1:0) op X into appropriately simpler form
  if (const NUExpr* result = reduceQC (bop))
    return finish (bop, result);

  // Look to convert a1+b1 into a1^b1
  if (const NUExpr* result = reduceSingleBitArithmetic(bop))
    return finish (bop, result);

  // Look for shift opportunities
  if (! isAggressive()) {
    // don't do this in 'aggressive' mode, as that is counter-productive
    // from what BETransform is doing.  In particular, with this verilog:
    //
    //    out = {in[0],in[1],in[2],in[3]
    //
    // BETransform makes this, which seems reasonable to me, even though that's
    // the same thing that codegen would eventually do:
    //
    //   (((((in << 3) & 4'h8) | ((in << 1) & 4'h4)) | ((in >> 1) & 4'h2)) |
    //    ((in >> 3) & 4'h1))
    //
    // but then this code, if allowed to run in aggressive mode, looks for
    // these shift opportunities and turns them back into bit-selects or
    // part-selects, e.g. surgically folding (in >> 1) to
    // {1'b0,in[3:1]}.  Not a good day out!
    if (sIsLshift (op)){
      if (const NUExpr* result = reduceLshift (bop))
        return finish (bop, result);
    } else if (sIsRshift (op)){
      if (const NUExpr* result = reduceRshift (bop))
        return finish (bop, result);
    }
  }

  // Check for possible relational operator merges.
  const NUExpr* mergedRelop;
  if (isCollapsibleRelop (op, e1, e2, &mergedRelop))
    return finish (bop, mergedRelop);


  // Check for a LOP that's a eBiDownTo, and distribute this op
  // over that...
  const NUBinaryOp* lbop = dynamic_cast<const NUBinaryOp*>(e1);
  if (lbop && lbop->getOp () == NUOp::eBiDownTo)
  {
    const NUExpr* result = binaryOp(NUOp::eBiDownTo,
                                     binaryOp(op,
                                                     copyExpr(lbop->getArg (0)),
                                                     copyExpr(e2),
                                                     loc),
                                     binaryOp(op,
                                                     copyExpr(lbop->getArg (1)),
                                                     copyExpr(e2),
                                                     loc),
                                     loc);
    return finish (bop, result);
  }

  // Some sign-extend optimizations
  if (op == NUOp::eBiVhExt && rp && (rv == 1)) {
    // SXT(e,1) => (e[0] ? -1 : 0), for large bitvectors
    const NUExpr* e1_0 = varsel(copyExpr(e1), ConstantRange (0,0), e1->getLoc ());
    if (bop_size > LONG_BIT) {
      return finish (bop, ternaryOp(e1_0,
                                    genOnes (loc, bop_size, true),
                                    genFalse (loc, bop_size, true), loc, bop_size));
    } else if (bop_size > 1) { // for small vectors, 0-e[0]
      return finish (bop, binaryOp(NUOp::eBiMinus,
                                   genFalse(loc, bop_size, true),
                                   e1_0,
                                   loc, bop_size));
    } else { // for scalars, just that one bit is all we need
      return finish(bop, e1_0);
    }
  } else if (rp && (op == NUOp::eBiVhZxt || op == NUOp::eBiVhExt)) {
    // The ZXT or EXT may be a no-op
    if (rv == e1->getBitSize () && rv == e1->determineBitSize () && rv >= bop_size) {
      // The expression is being extended from its self-determined
      // most-significant bit, and that is at least as large as the
      // contextual size required.  [BUG6071]
      const NUExpr* result = copyExpr(e1);
      return finish (bop, result);
    } else if (op == NUOp::eBiVhZxt && isAggressive ()
               && not e1->isSignedResult ()
               // Added to cure bug 6295.
               && e1->getBitSize()==e1->determineBitSize()
               && rv >= getCachedEffectiveBitSize(e1)) {
      // zero extending from a position that's guaranteed to be zeros.  But
      // don't do this too early, as reducePartselCompare can handle ZXT,
      // but can't handle concat with leading zeros.
      return finish (bop, padExpr (copyExpr(e1), bop_size));
    }
  }

  // Some folds with 1'bx
  if (isConstantX (rp))
    switch (op) {
    case NUOp::eBiEq:
    case NUOp::eBiNeq:
    case NUOp::eBiSLt:                  //!< Binary <
    case NUOp::eBiSLte:                 //!< Binary <=
    case NUOp::eBiSGtr:                 //!< Binary >
    case NUOp::eBiSGtre:                //!< Binary >=
    case NUOp::eBiULt:                  //!< Binary <
    case NUOp::eBiULte:                 //!< Binary <=
    case NUOp::eBiUGtr:                 //!< Binary >
    case NUOp::eBiUGtre:                //!< Binary >=
    case NUOp::eBiBitAnd: return finish (bop, genFalse (loc, bop_size));

    case NUOp::eBiBitOr: return finish (bop, genTrue (loc, bop_size));

    case NUOp::eBiBitXor:       // a  ^ 1'bx => 1'bx
    default:                    // a  + 1'bx => 1'bx, etc.
      return finish (bop, copyExpr(rp));
    }
  else if (isConstantX (lp)) {
    // 1'bx - expr => 1'bx
    // 1'bx / expr => 1'bx
    return finish (bop, copyExpr(lp));
  }


  // Check for multiplier used as MUX.  Assume that:
  // 1/ MUX is better than multiply for small values (1-32 bits) because of CMOVE instructions
  // 2/ MULTIPLY is better than MUX for intermediates (33-64 bits)
  // 3/ MUX is better than multiply for bitvectors.

  UInt32 eSize = bop_size;
  if ((op == NUOp::eBiUMult || op == NUOp::eBiSMult)
      && (eSize <= LONG_BIT || eSize > LLONG_BIT)) {
    if (getCachedEffectiveBitSize(e1) == 1 && not lp)
      return finish (bop, ternaryOp(genLogical (e1), e2,
                                    genFalse (loc,
                                              e2->getBitSize (),
                                              e2->isSignedResult ()),
                                    loc, bop_size));
    else if (getCachedEffectiveBitSize(e2) == 1 && not rp)
      return finish (bop, ternaryOp(genLogical (e2), e1,
                                    genFalse (loc,
                                              e1->getBitSize (),
                                              e1->isSignedResult ()),
                                    loc, bop_size));
  } else if (op == NUOp::eBiSMult && rp && rp->isOnes ()) {
    // e * -1 => -e
    return finish (bop, unaryOp(NUOp::eUnMinus, copyExpr(e1), loc, bop_size));
  }

  // Try doing boolean identities

  if (op == NUOp::eBiBitOr) {
    const NUExpr* result;
    if ((result = reduceBoolIdent (op, NUOp::eBiBitAnd, e1, e2, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitAnd, e2, e1, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitXor, e1, e2, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitXor, e2, e1, bop))
      )
      return finish (bop, result);
  } else if (op == NUOp::eBiBitAnd) {
    const NUExpr* result;
    if ((result = reduceBoolIdent (op, NUOp::eBiBitOr, e1, e2, bop))
        ||(result = reduceBoolIdent (op, NUOp::eBiBitOr, e2, e1, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitXor, e1, e2, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitXor, e2, e1, bop))
      )
      return finish (bop, result);
  } else if (op == NUOp::eBiBitXor) {
    const NUExpr* result;
    if ((result = reduceBoolIdent (op, NUOp::eBiBitOr, e1, e2, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitOr, e2, e1, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitAnd, e1, e2, bop))
        || (result = reduceBoolIdent (op, NUOp::eBiBitAnd, e2, e1, bop)))
      return finish (bop, result);
  }

  if (not lp and not rp)
    // Neither operand is a constant, we don't have anything clever to
    // do beyond this point
    return NULL;

  // BEYOND HERE, we know that any constants are not pure X, and that
  // since we don't really do 4-state representations at runtime, we can
  // treat the X as anything we want...

  // Check for commutative identity - we don't care about LOP except
  // that we need to be sure this operation isn't forcing a wider
  // result.  Consider that:
  //     a + b + 0
  // in Verilog requires that the expression be evaluated at least
  // to 32 bits of accuracy regardless of how small 'a' and 'b' are.
  //
  UInt32 e1DetSize = e1->determineBitSize ();
  bool widening = (e1DetSize < e2->determineBitSize ());
  
  if (rp && not rp->hasXZ () && not widening
      && not bop->isReal ()     		// Don't be fooled if real!
      && ((isZeroIdentity (op) && rv == 0) 	// viz E1 | 0
	  || (isOneIdentity (op) && rv == 1) 	//     E1 * 1
          || (isMinusIdentity (op)
	      && sAllOnes (rv, e1DetSize)) // E1 & -1
        )
    )
  {
    const NUExpr* result = genLogical(e1, op);
    return finish (bop, result);
  }

  // Special checks
  if ((op == NUOp::eBiBitOr && rp && rp->isOnes ())
      || (op == NUOp::eBiLogOr && rp && !rp->isZero ())
      )
    // A|(~0) => ~0
    {
      const NUExpr* result = genOnes (loc, bop_size, bop_sign);

      return finish (bop, result);
    }
  else if ((op == NUOp::eBiBitAnd
	    || (op == NUOp::eBiSMult)
            || (op == NUOp::eBiUMult)
	    || (op == NUOp::eBiLogAnd))
	   && rp && rp->isZero ())
    // A&0 => 0
    {
      const NUExpr* result = copyExpr(rp);

      return finish (bop, result);
    }
  else if ((   (op == NUOp::eBiSDiv)
            || (op == NUOp::eBiSMod)
            || (op == NUOp::eBiUDiv)
            || (op == NUOp::eBiUMod))
           && rp
           && rp->isZero ())
  {
    // Treat A % 0 or A / 0 as 'x'.
    DynBitVector ones (bop_size);
    ones.set ();                 // all ones
    return finish (bop,
                   constantXZ (false, ones, ones,
                               bop_size, loc));
  }
  else if ((op == NUOp::eBiPlus) && rp && rv == 1
	   && e1->getType () == NUExpr::eNUUnaryOp)
    {
      // Look for  ~e + 1 => -e
      // Being careful that we're not widening the operands.
      if (const NUUnaryOp *uop = dynamic_cast<const NUUnaryOp*>(e1))
	if ((uop->getOp () == NUOp::eUnBitNeg
             || uop->getOp () == NUOp::eUnVhdlNot)
            && (uop->getBitSize () >= bop_size))
        {
          const NUExpr* e = uop->getArg(0);
          if (bop_size > e->determineBitSize ()) {
            e = mExprCache->makeSizeExplicit(e, bop_size);
          }
            
          const NUExpr* result = unaryOp(NUOp::eUnMinus,
                                          e,
                                          uop->getLoc ());
          return finish (bop, result);
        }
    }
  else if (op == NUOp::eBiUDiv && rp && rv.count () == 1)
  {
    // X / 2**k => X >> k
    const NUExpr* k = constant(false, rv.findFirstOne (), 32, loc);
    const NUExpr* x = copyExpr (e1);
    if (widening)
      // quotient size was affected by divisor;  this isn't true for shifts, so
      // wrap in ZXT to preserve accuracy.
      x = binaryOp(NUOp::eBiVhZxt, x,
                   constant(false, x->getBitSize (), 32, loc), loc);
    const NUExpr* result = binaryOp(NUOp::eBiRshift, x, k, loc); 
    return finish (bop, result);
  }
  else if ((op == NUOp::eBiUMod || op == NUOp::eBiSMod || op == NUOp::eBiVhdlMod)
           && rp && rv == 1)
    // Anything (signed or unsigned) mod 1 is zero
    return finish (bop, genFalse (loc, bop_size));
  else if ((op == NUOp::eBiUMod
            || (op == NUOp::eBiVhdlMod && not e1->isSignedResult ()))
           && rp && rv.count () == 1)
  {
    // UExpr % 2**k ==> UExp & mask(k)
    UInt32 maskWidth = rv.findFirstOne ();
    return finish (bop,
                   binaryOp(NUOp::eBiBitAnd, copyExpr (e1), genOnes (loc, maskWidth, false), loc));
  }
  else if ((op == NUOp::eBiSMod || op == NUOp::eBiVhdlMod) && bop_size == 1
           && rp && rv.count () == 1)
    // (exp mod 2**k) but returning ONLY one bit, so the sign doesn't matter
  {
    return finish (bop, binaryOp(NUOp::eBiBitAnd, e1, genTrue (loc, 1),
                                 loc, bop_size));
  }
  else if ((op == NUOp::eBiBitAnd) && not bop_sign
	   && rp && sAllZeroes (rv, getCachedEffectiveBitSize(e1)))
    // bit & 64 => 0
    return finish (bop, genFalse (loc, bop_size));
  else if ((op == NUOp::eBiBitAnd) && rp && not lp && not bop_sign) {
    UInt32 maskWidth = sBitMask (rv);
    const NUExpr* result;

    // Be careful about X&255 => X[7:0]; that could impact resize() calculations.
    // Only do the conversion if the mask is precisely the right size AND it's
    // no larger than the thing we'd be partselecting from.  Then this mask can't
    // be altering the size of something else. [BUG5392]
    if (maskWidth
        && maskWidth == rp->determineBitSize ()
        && maskWidth <= e1->determineBitSize ()
        && (result = reducePartsel (e1, maskWidth)))
    {
      return finish (bop, result);
    }
  }
  // Only safe for boolean.  Otherwise, this optimization ends up
  // ignoring any extensions (see bug 3649).
  else if ((op == NUOp::eBiBitXor) && not bop_sign
           && (e1->determineBitSize() == 1)
           && !e1->isSignedResult ()
	   && rp && (rv == 1))
    // bit ^ 1 = !bit
  {
    const NUExpr* result = copyExpr(e1);
    result = unaryOp(NUOp::eUnLogNot, result, loc);
    result = padExpr (result, bop_size);
    return finish (bop, result);
  }
  else if (((op == NUOp::eBiRoR) || (op == NUOp::eBiRoL))
           && rp && (rv == e1->getBitSize()) )
    // K rotate k.width => k
  {
    const NUExpr* result = copyExpr(e1);
    return finish (bop, result);
  }
  else if (op == NUOp::eBiMinus && rp && !lp
           && (rv.size () <= LLONG_BIT))
  {
    // a - c => a + (-c), exposing associativity opportunities for things
    // smaller than bitvectors
    rv.negate ();

    const NUConst* c = constant(true, rv, rv.size (), e2->getLoc ());

    return finish (bop, binaryOp(NUOp::eBiPlus, copyExpr(e1), c, loc,
                                 bop_size));
  } else if (op == NUOp::eBiExp) {
    if ((lp && lv == 1)         // 1**K => 1 for integral k
        || (lp && lp->isZero ())) // 0**K => 0
      return finish (bop, copyExpr(e1));
    else if (rp) {
      if (rp->isZero ())        // x**0 -> 1
        return finish (bop, genTrue (loc, bop_size));
      else if (rv == 1)         // x**1 -> x
        return finish (bop, copyExpr(e1));
    } else  if (lp && lv.count () == 1) {
      // (2**k) ** exp => 2 ** (k*exp) => (1) << (k*exp)
      const NUExpr *kpow2  = constant(false, lv.findFirstOne (), LONG_BIT,
                                      e1->getLoc ());
      const NUExpr *shiftBy = binaryOp(NUOp::eBiUMult, e2, kpow2, e2->getLoc (),
                                       LONG_BIT);
      const NUExpr* result = binaryOp(NUOp::eBiLshift,
                                      genTrue (bop->getLoc (), e1->getBitSize (), false),
                                      shiftBy, bop->getLoc ());
      return finish (bop, result);
    }
  }

  // Check for mask folds
  // Cases of interest include:
  //
  //  (VAR & 0xff0) == 0x3c0    ->  VAR[4+:8] == 0x3c
  //  (VAR & 0xf0f) == 0x30c    ->  VAR[0+:12] == 0x30c
  //  (VAR & 0xf0f) == 0x3cc)   ->  false
  //  ((-VAR[0]) & 16) == 16    ->  VAR[0] == 1   (Hard to do...)
  if ((op == NUOp::eBiEq || op == NUOp::eBiNeq) and rp) {
    const NUOp * subop = dynamic_cast<const NUOp*>(e1);
    if (subop!=NULL and subop->getOp() == NUOp::eBiBitAnd) {
      const NUExpr * a1 = subop->getArg(0);
      const NUExpr * a2 = subop->getArg(1);
      const NUConst * c1 = ctce(a1);
      const NUConst * c2 = ctce(a2);

      if (c1 or c2) {
        if (c1 and not c2) {
          std::swap(c1,c2);
          std::swap(a1,a2);
        }

        DynBitVector avalue;    // The potential field mask
        c2->getSignedValue(&avalue);
        ConstantRange maskRange (avalue.findLastOne(), avalue.findFirstOne ());

        // Remember if mask was dense
        bool fullMask = (avalue.count () == maskRange.getLength ());

        // Look at RHS value
        DynBitVector rvalue;
        rp->getSignedValue(&rvalue);

        avalue &= rvalue;
        if (avalue != rvalue) {
          // We would be masking off bits that we are expecting to compare against
          // e.g. (a & 2) == 1 => false
          //      (a & 2) != 1 => true
          return finish (bop, (op==NUOp::eBiEq) ? genFalse (loc, 1, false)
                         			: genTrue (loc, 1, false));

        } else if (fullMask && not isAggressive () ) {
          // See of we can transform the expression, but don't undo transforms
          // produced by reducePartselCompare which only runs on aggressive passes
          const NUExpr * part1 = foldPartsel(a1, 0, maskRange, a1->getLoc());
          const NUExpr * part2 = foldPartsel(rp, 0, maskRange, rp->getLoc());
          if (part1 && part2) {
            const NUExpr* result = binaryOp(op, part1, part2, loc);
            return finish(bop, result);
          }
          cleanupExpr (part1);
          cleanupExpr (part2);
        } else {
          // Not a full mask - something like (VAR & 128'h1040000) == 0x1040000
          // Slightly better would be (VAR[2+:7]&0x41 == 0x41 
          // but we need complex analysis to decide - so for now ignore these
          // cases.
          ;
        }
      }
    }
  }

  // For now, just perform CONST OP CONST cases

  if (not (lp && rp))
  {
    if (swapped)
      return finish (bop,
                     binaryOp(op, copyExpr (e1), copyExpr (e2), loc));

    return NULL;
  }

  if (op == NUOp::eBiDExp) {
    // Produces a REAL valued result from integral inputs.
    SInt64 sMant, sExp;
    if ( not lp->getLL (&sMant)
         || not rp->getLL (&sExp))
      return NULL;            // Can't represent mant**exp in 64 bit values
      
    CarbonReal mant = (CarbonReal) sMant,
      exp = (CarbonReal) sExp;

    mant = std::pow (mant, exp);
    const NUConst* realConst = constant(mant, loc, bop_sign);
    return finish (bop, realConst, bop_sign);
  }

  DynBitVector result (bop_size,lv);

  // Make sure the result is the appropriate size.  The assignment operator for
  // bitvectors may have resized to the incorrect size.
  result.resize(bop_size);

  switch(op)
    {
    default:
      return NULL;

    case NUOp::eBiVhdlMod:
    {
      // Handle normal VHDL cases involving signed operands or small values
      UInt32 xsiz = lp->getBitSize ();
      SInt32 sl, sr;
      UInt32 ul, ur;
      char select=0;

      if (e1->isSignedResult ()) {
        select=2;
        lp->getL (&sl);
      } else {
        lp->getUL (&ul);
      }

      if (e2->isSignedResult ()) {
        select += 1;
        rp->getL (&sr);
      } else {
        rp->getUL (&ur);
      }

      switch (select)
      {
      case 0: result = carbon_vhmod (ul, ur, xsiz); break;
      case 1: result = carbon_vhmod (ul, sr, xsiz); break;
      case 2: result = carbon_vhmod (sl, ur, xsiz); break;
      case 3: result = carbon_vhmod (sl, sr, xsiz); break;
      }
      break;
    }

    case NUOp::eBiVhZxt:        // zero-extend operator
    case NUOp::eBiVhExt:        // sign-extend operator
    {
      SInt32 extendWidth;
      rp->getL (&extendWidth);  // Anticipated of the result

      SInt32 actWidth = result.size ();
      SInt32 srcWidth = lv.size ();

      UInt32 signBit = srcWidth - 1; // Assumed sign position

      // Can't handle widening expression to size LARGER than context
      // of result - so make it as wide as we will use.

      extendWidth = std::min (actWidth, extendWidth);

      if (extendWidth <= 0) {
        result = 0;             // No significant bits
        break;
      }

      // If result width narrower than source expression, then
      // truncation is effected by just copying the value
      if (srcWidth >= extendWidth) {
        if (extendWidth == actWidth)
          break;                  // truncated or just copying
        else
          // Either from MSB of source or implied sign of result
          signBit = extendWidth-1;
      }

      // Sign-extend if MSB set AND opcode is EXT, otherwise zero-extend
      bool sign = result.test (signBit) && (op == NUOp::eBiVhExt);

      // When widening an expression where we have to figure out how to widen
      // the source in order to reach the extend-width (which in turn may be
      // smaller than the actual width, we use the isSignedResult() on the
      // sub-expression
      if (extendWidth > srcWidth)
        sign &= e1->isSignedResult ();

      result.setRange (signBit+1, actWidth - (signBit+1), sign);
      break;
    }

    case NUOp::eBiPlus:                //!< Binary +
      result += rv;
      break;
      
    case NUOp::eBiMinus:               //!< Binary -
      result -= rv;
      break;

    case NUOp::eBiUMult:                //!< Binary *
      result.multiply(lv, rv);
      break;

    case NUOp::eBiSMult:                //!< Binary *
      result.signedMultiply(lv, rv);
      break;

    case NUOp::eBiUDiv:                 //!< Binary /
      if (rv.count () == 1)
      {
        //  A / 2^k => A >> k (for unsigned)
        result >>= rv.findFirstOne ();
      }
      else if (lv.size () <= LLONG_BIT && rv.size () <= LLONG_BIT)
      {
        // Unsigned divide
        UInt64 lint = lv.llvalue ();
        UInt64 rint = rv.llvalue ();
        if (rint == 0)
          result = 0;           // Avoid zero-divide
        else
          result = lint / rint;
      } else {
        return NULL;
      }
      break;

    case NUOp::eBiSDiv:                 //!< Binary /
      // Divide by 0 --> yield 0
      if (! rv.any()) {
        result = 0;
      }

      // small integers -- use native support
      else if ((lv.size() <= LLONG_BIT) && (rv.size() <= LLONG_BIT)) {
        SInt64 lint = sExtendConstant(lv.size(), lv.llvalue ());
        SInt64 rint = sExtendConstant(rv.size(), rv.llvalue ());
        lint /= rint;
        result = lint;
        result.resize (bop_size);
        if (lint < 0 && result.size () > LLONG_BIT)
          result.setRange (LLONG_BIT, result.size () - LLONG_BIT, 1);
      }

      // Wide signed integer division by non-power-of-2.  punt.
      else if (rv.count() != 1) {
        return NULL;
      }

      else {
        bool neg = false;
        if (lv.test(lv.size() - 1)) {
          neg = !neg;
          lv.negate();
        }
        if (rv.test(rv.size() - 1)) {
          neg = !neg;
          rv.negate();
        }
        result = lv;
        result >>= rv.findFirstOne();
        if (neg) {
          result.negate();
        }
      }
      break;

    case NUOp::eBiSMod:                 //!< Binary %
    case NUOp::eBiUMod:
      // Don't handle arbitrary precision!
      if (rv.count () == 1)
      {
        // X %= 2**k
        rv -= 1;
        result &= rv;
      }
      else if (lv.size () <= LLONG_BIT && rv.size () <= LLONG_BIT)
      {
        UInt64 lint = lv.llvalue ();
        result = lint % rv.llvalue ();
      }
      else
      {
        return NULL;
      }
      break;

    case NUOp::eBiEq:                  //!< Binary ==
    case NUOp::eBiTrieq:               //!< Binary ===
      result = (lv == rv);
      break;

    case NUOp::eBiNeq:                 //!< Binary !=
    case NUOp::eBiTrineq:              //!< Binary !==
      result = (lv != rv);
      break;

    case NUOp::eBiLogAnd:              //!< Binary Logical &&
      result = lv.any () && rv.any ();
      break;

    case NUOp::eBiLogOr:               //!< Binary Logical ||
      result = lv.any () || rv.any ();
      break;

    case NUOp::eBiSLt:                  //!< Binary <
    case NUOp::eBiSLte:                 //!< Binary <=
    case NUOp::eBiSGtr:                 //!< Binary >
    case NUOp::eBiSGtre:                //!< Binary >=
    case NUOp::eBiULt:                  //!< Binary <
    case NUOp::eBiULte:                 //!< Binary <=
    case NUOp::eBiUGtr:                 //!< Binary >
    case NUOp::eBiUGtre:                //!< Binary >=
      NU_ASSERT(lv.size() == rv.size(), bop);
      result = sComputeRelOp (lv, rv, op, loc);
      break;

    case NUOp::eBiBitAnd:              //!< Binary Bitwise &
      result &= rv;
      break;

    case NUOp::eBiBitOr:               //!< Binary Bitwise |
      result |= rv;
      break;

    case NUOp::eBiBitXor:             //!< Binary Bitwise ^
      result ^= rv;
      break;
      
    case NUOp::eBiRshift:              //!< Binary >>
      if (rv > 65535)
        lv = 0;
      else
        lv >>= rv.value ();
      result = lv;
      break;


    case NUOp::eBiRshiftArith:  //!< Arithmetic >>
    {
      bool sign = lv.test (lv.size ()-1);

      if (rv >= lv.size ())
        lv.setRange (0, lv.size (), sign);
      else {
        lv >>= rv.value ();
        UInt32 signExt = e1->getBitSize () - rv.value ();
        if (sign)
          lv.setRange (rv.value (), signExt, 1);
      }
      result = lv;
      break;
    }
    
    case NUOp::eBiLshift:              //!< Binary <<
    case NUOp::eBiLshiftArith:         //!< Binary <<<
      // Verilog LRM treats these identically
      if (rv >= lv.size ())
        result = 0;
      else
        result <<= rv.value ();
      break;

    case NUOp::eBiVhRshiftArith:
    case NUOp::eBiVhLshiftArith:
      return NULL;                 // Not handled yet
        
    case NUOp::eBiExp:
    {
      UInt64 lint = lv.llvalue ();
      UInt64 prod = lint;
      UInt32 rint = rv.value ();

      if (rint == 0)
        result = 1;
      else {
        for(; rint > 1; --rint)
          prod *= lint;
        result = prod;
      }
      break;
    }

    }

  // we've folded the expression into an integral type value 'result'

  const NUConst *c = constant(bop_sign, result, bop_size, loc);

  return finish (bop, c);
}

// take concat OP concat and distrbute OP over the 32-bit words.
const NUExpr* FoldI::stripMineConcat(const NUExpr* expr) {
  const SourceLocator& loc = expr->getLoc();
  UInt32 esize = expr->getBitSize();
  UInt32 numWords = (esize + 31) / 32;
  NUCExprVector v;

  for (UInt32 i = 0; i < numWords; ++i) {
    UInt32 lsb = 32*i;
    UInt32 msb = lsb + 31;
    msb = std::min(esize - 1, msb);
    ConstantRange range(msb, lsb);
    NUExpr* subexpr = createPartsel(expr, range, loc);
    NU_ASSERT(subexpr->determineBitSize() == range.getLength(), subexpr);
    const NUVarselRvalue* varsel = dynamic_cast<const NUVarselRvalue*>(subexpr);
    if ((varsel != NULL) && !varsel->getIdentExpr()->isWholeIdentifier()) {
      // If we had to do (a+b)[63:32] then let's call it a total loss, even
      // if other terms reduced to variables or constants
      delete subexpr;           // not_managed_expr
      return NULL;
    }
    v.push_back(foldAndManage(subexpr, false));
  }

  std::reverse(v.begin(), v.end());
  const NUExpr* cat = concatOp(v, 1, loc, esize,
                               specifySign(expr->isSignedResult()));
  NU_ASSERT(cat->determineBitSize() == esize, cat);
  (void) foldExprTree(&cat);
  NU_ASSERT(cat->determineBitSize() == esize, cat);
  if (cat == expr) {
    return NULL;                // back to where we started
  }

  return cat;
} // const NUExpr* FoldI::stripMineConcat

/*!
 * Given a set of expressions \arg loop, produce the expanded form, applying the \arg unop
 * to each term, and connecting the terms by \arg binop.
 */
const NUExpr*
FoldI::expandList (NUExprCLoop loop, NUOp::OpT unop, NUOp::OpT binop, const SourceLocator& loc)
{
  if (loop.atEnd ())
    return NULL;

  const NUExpr* result = copyExpr (*loop);
  if (unop != NUOp::eStart)
    result = unaryOp(unop, result, result->getLoc ());

  for(++loop; !loop.atEnd (); ++loop) {
    const NUExpr *term = copyExpr (*loop);
    if (unop != NUOp::eStart)
      term = unaryOp(unop, term, term->getLoc ());

    result = binaryOp(binop, result, term, loc);
  }

  return result;
}

/*
 * Given operation \arg opcode, and a concatenation rvalus \arg cat,
 * return an expression that computes the reduction without constructing
 * the concat.
 */

const NUExpr*
FoldI::reduceOpConcat (NUOp::OpT opcode, const NUConcatOp* cat)
{
  const NUExpr* result = NULL;
  NUOp::OpT unop=opcode, binop;
  const SourceLocator& loc(cat->getLoc ());
  NUExprCLoop loop (cat->loopExprs ());

  switch (opcode) {
  case NUOp::eUnLogNot:
    // !{a,b} => !a & !b
    binop = NUOp::eBiBitAnd;
    break;

  case NUOp::eUnRedAnd:
    // &{a,b} => &a & &b
    binop = NUOp::eBiBitAnd;
    break;

  case NUOp::eUnRedOr:
    // |{a,b} => |a | |b
    binop = NUOp::eBiBitOr;
    break;

  case NUOp::eUnCount:
    binop = NUOp::eBiPlus;
    break;

  case NUOp:: eUnRedXor:
    // ^{a,b} => ^a ^ ^b
    binop = NUOp::eBiBitXor;
    break;

  default:
    // Unhandled unary operators.
    return NULL;
  }

  // Common cases !,|,&
  result = expandList (loop, unop, binop, loc);

  // If we reduced it, return the new expression
  return result;
}

const NUExpr* FoldI::foldUnaryOp(const NUUnaryOp *uop)
{
  const NUExpr *arg = uop->getArg (0);
  const SourceLocator loc = uop->getLoc ();
  NUOp::OpT op = uop->getOp ();
  UInt32 uop_size = uop->getBitSize();
  UInt32 uop_sign = uop->isSignedResult();

  // find any bottom-up folds first
  if (foldExprTree(&arg)) {
    const NUExpr* new_uop =
      unaryOp(op, arg, loc, uop_size,
              specifySign(uop->isSignedResult()));
    return finish(uop, new_uop);
  }

  const NUExpr* result;

  if (const NUExpr* result = reduceQC (uop))
    // Convert <op> (e?1:0) into appropriately simpler form
    return finish (uop, result);

  if ((op == NUOp::eUnBitNeg || op == NUOp::eUnVhdlNot) &&
      uop_size == 1 && arg->getBitSize () == 1)
    // canonicalize ~e and !e for one-bit values.
    return finish (uop, genComplement (arg));

  // at this point we might want to call a unaryDemorgan routine that
  // would convert ~(a & ~b) => ~a | b (test/exprsynth/assign4)

  // It's useful to know if the argument to this operator is
  // a bitwise complement..
  const NUUnaryOp* sub = dynamic_cast<const NUUnaryOp*>(arg);

  switch(op)
  {
  case NUOp::eUnBuf:
  case NUOp::eUnPlus:
    // If this changes sign, we have to keep it!
    if (uop->isSignedResult () != arg->isSignedResult ())
      break;
    result = copyExpr(arg);
    return finish (uop, result);
    

  case NUOp::eUnBitNeg:
  case NUOp::eUnVhdlNot:
    if (sub)
    {
      switch (sub->getOp ()) {
      case NUOp::eUnBitNeg:
      case NUOp::eUnVhdlNot:
        if (sub->getBitSize () == uop_size)
        {
          result = copyExpr(sub->getArg (0));
          return finish (uop, result);
        }
        break;
        
      case NUOp::eUnLogNot:
        if (uop_size == 1)
          // ~!e => (e != 0)
        {
          result = genLogical(sub->getArg (0));
          return finish (uop, result);
        }
        break;
      default:
        break;
      }
    } else if ((result = negatedDeMorgan (arg))) {
      return finish (uop, result);
    } else if ((result = distributeNotConcat(arg))){
      // distribute ~ over a concatenation that's more than 64 bits succeeded
      return finish (uop, result);
    }

    break;

  case NUOp::eUnLogNot:
    if (const NUBinaryOp *e = dynamic_cast<const NUBinaryOp *>(arg))
    {
      if (isRelationalOp (e->getOp ()))
        // ! (a == b) => (a != b)
      {
        result = binaryOp(sInverseOp (e->getOp ()),
                                 copyExpr(e->getArg(0)),
                                 copyExpr(e->getArg(1)),
                                 loc);
        return finish (uop, result);
      }
      else if (alwaysNonZero (e))
      {
        // look for (e | const) or (e ^ const) such that
        // expression is guaranteed to be non-zero
        {
          result = genFalse (loc, uop_size, false);
          return finish (uop, result);
        }
      }
      else if (e->getBitSize () > 1)
      {
        // !(expr) => (expr == 0), hoping for (X-5) == 0  => X == 5
        result = binaryOp(NUOp::eBiEq, copyExpr(e),
                                 genFalse (loc,
                                           e->getBitSize (),
                                           e->isSignedResult ()), loc);
        return finish (uop, result);
      }
      else if (isBitwiseOp (e->getOp ()) && e->getBitSize ()==1 && (result = negatedDeMorgan (e)))
        return finish (uop, result);
    } else if (sub)
    {
      // !!(e) => e (for scalar e.  Vector e becomes (e!=0)
      if (sub->getOp () == NUOp::eUnLogNot) // !!(e))
      {
        result = genLogical (sub->getArg(0));
        return finish (uop, result);
      }
      else if (isComplement (sub)) 	      // !~(e)  => e==-1
      {
        const SourceLocator& loc = sub->getLoc ();
        result = copyExpr(sub->getArg(0));

        result = binaryOp(NUOp::eBiEq, result,
                                 genOnes (loc, result->determineBitSize ()),
                                 loc);
        return finish (uop, result);
      }
    } else if (const NUConcatOp* cat = dynamic_cast<const NUConcatOp*>(arg)) {
      if ((result = reduceOpConcat (NUOp::eUnLogNot, cat)))
        return finish (uop, result);
    }
    break;

  case NUOp::eUnMinus:
    if (uop_size == 1)
      // -X[i] => X[i], but ONLY if the expression size is 1 bit.
      // otherwise we would have a carry propagation and we get the
      // wrong answer
    {
      getMsgContext ()->NegationNoOp (&loc);
      result = copyExpr(arg);
      return finish (uop, result);
    }

    if (const NUBinaryOp *e = dynamic_cast<const NUBinaryOp*>(arg))
      if (e->getOp () == NUOp::eBiMinus)
      {
        // Look for:
        //  - (a - b) => b-a
        //  
        result = binaryOp(NUOp::eBiMinus,
                                 copyExpr(e->getArg(1)),
                                 copyExpr(e->getArg(0)),
                                 loc);
        return finish (uop, result);
      }
    break;

  
  case NUOp::eUnAbs:
    if (sub)
      if (sub->getOp () == NUOp::eUnAbs // abs(abs(X)) => abs(X)
           && sub->getBitSize () == uop_size)
      {
        result = copyExpr(sub);
        return finish (uop, result);
      }
    break;

  case NUOp::eUnRedAnd:
    // If we know that the operand has a reduced getCachedEffectiveBitSize(),
    // we can fold this
    if (getCachedEffectiveBitSize(arg) < arg->getBitSize ())
      return finish ( uop, genFalse (loc, 1));
    else if (isComplement (sub)) {
      // &(~x) => x==0
      result = copyExpr(sub->getArg(0));
      result = binaryOp(NUOp::eBiEq, result,
                               genFalse (loc, result->getBitSize ()),
                               loc);
      return finish (uop, result);
    } else if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(arg)) {
      if (bop->getOp () == NUOp::eBiBitXor
          && ctce (bop->getArg(1)))
        // & (e ^ c)     =>   e == ~c
        return finish (uop, binaryOp(NUOp::eBiEq,
                                            bop->getArg(0),
                                            genComplement (bop->getArg(1)),
                                            loc));
      else if (bop->getOp () == NUOp::eBiBitAnd) {
        NUCExprVector v;
        if (group (NUOp::eBiBitAnd, bop, &v)
            && v.end () == std::find_if (v.begin (), v.end (),
                                      std::not1 (std::ptr_fun (FoldI::isPrimary)))) {
          // Reductions of primaries want to be expanded so that field adjacency optimizations
          // can be applied
          //    & (a[0+:8] & a[8+:8])  => (&a[0+:8] & &a[8+:8]) => &a[0+:16]
          // This can only be true if ALL the bits are set, btw...
          result = binaryOp(NUOp::eBiBitAnd,
                                   unaryOp(NUOp::eUnRedAnd, copyExpr (bop->getArg(0)), loc),
                                   unaryOp(NUOp::eUnRedAnd, copyExpr (bop->getArg(1)), loc),
                                   loc);
          return finish (uop, result);
        }
      }
    }
    
    goto red_common;

  case NUOp::eUnRedOr:
    if (isComplement (sub)) {
      // |(~x) => x != -1
      result = copyExpr(sub->getArg(0));
      result = binaryOp(NUOp::eBiNeq, result,
                               genOnes (loc, result->getBitSize (),
                                        result->isSignedResult ()),
                               loc);
      return finish (uop, result);
    } else if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(arg)) {
      if (bop->getOp () == NUOp::eBiBitOr) {
        NUCExprVector v;
        if (group (NUOp::eBiBitOr, bop, &v)
            && v.end () == std::find_if (v.begin (), v.end (),
                                      std::not1 (std::ptr_fun (FoldI::isPrimary)))) {
          // | (a | b)  => (|a | |b)
          result = binaryOp(NUOp::eBiBitOr,
                                   unaryOp(NUOp::eUnRedOr, copyExpr (bop->getArg(0)), loc),
                                   unaryOp(NUOp::eUnRedOr, copyExpr (bop->getArg(1)), loc),
                                   loc);
          return finish (uop, result);
        }
      }
    }
    goto red_common;

  case NUOp::eUnCount:
    // count(~X) => sizeof(X) - count(X)
    if (isComplement (sub)) {
      result = constant(false, sub->getBitSize (), 
                                uop_size, loc);
      result = binaryOp(NUOp::eBiMinus, result, copyExpr(sub->getArg(0)),
                               loc);
      return finish (uop, result);
    }
    goto red_common;

  case NUOp::eUnRedXor:

  red_common:
    {
      const NUExpr* redExpr = arg;

      // Consider reductions of concat's with repeat counts...
      const NUConcatOp* cat = dynamic_cast<const NUConcatOp*>(redExpr);
      if (cat && cat->getRepeatCount () > 1) {
        UInt32 rpt = cat->getRepeatCount ();

        switch (op) {
        case NUOp::eUnRedXor: {
          // ^{K{e}} => K even => 0, K odd => ^{e}
          if (rpt % 2) {
            NUConcatOp* newCat = cat->copyConcatOp(mCC);
            newCat->putRepeatCount (1);
            result = unaryOp(op, manage(newCat, false), loc);
          } else {
            result = genFalse (loc);
          }
          return finish (uop, result);
        }
        case NUOp::eUnRedAnd:
        case NUOp::eUnRedOr: {
          // &{K{e}} => &{e}
          NUConcatOp* newCat = cat->copyConcatOp(mCC);
          newCat->putRepeatCount (1);
          result = unaryOp(op, manage(newCat, false), loc);
          return finish (uop, result);
        }
        case NUOp::eUnCount: {
          // POPCOUNT({K{e]}) => K * POPCOUNT({e})
          NUConcatOp* newCat = cat->copyConcatOp(mCC);
          newCat->putRepeatCount (1);
          result = binaryOp(NUOp::eBiUMult,
                            unaryOp(op, manage(newCat, false), loc),
                            constant(false, rpt, uop_size, loc), loc);
          return finish (uop, result);
        }
        default:
          break;
        }
      }

      // Check for generalized <op>{concat} where it's always better not to build the concat
      if (cat) {
        result = reduceOpConcat (op, cat);
        if (result)
          return finish (uop, result);
      }
        

      if (redExpr->getBitSize () == 1
          && isPrimary (redExpr))
      {
        // Reduction on a single one-bit operand is silly....BUT, reduction
        // prevents a resize from being propagated down - and that means that
        // something like:
        //    (& (~ |x)) + Vector
        // would get the wrong results. So only allowed when arg is a primary
        // &X[i] => (X[i]&1)
        result = copyExpr (redExpr);
        return finish (uop, result);
      }
      else
      {
        // Check for a reduction with more generated bits than
        // effective bits; we've already dealt with & and ~& where the
        // extra zeros alter the result, so the remaining cases of
        // |,^,~|, ~^ and POPCOUNT are independent of the extra width.

        UInt32 effSize = getCachedEffectiveBitSize(redExpr);

        // Heuristically, we want to reduce to nearest multiple of 8/16/32
        if (effSize == 1)
          ;                       // This will simplify further
        else if (effSize <= 8)
          effSize = 8;
        else if (effSize <= 16)
          effSize = 16;
        else if (effSize <= 32)
          effSize = 32;
        else if (effSize <= 64)
          effSize = 64;
        else
          // Must be a bitvector size
          effSize = (effSize+31) & -32; // roundup to a word size
          ;

        if (effSize > 0 && effSize < redExpr->getBitSize ()) {
          ConstantRange r (effSize-1, 0);
          const NUExpr* ps = manage(createPartsel (redExpr, r, loc), false);
          result = unaryOp(op, ps, loc);
          result = padExpr (result, uop_size);
          return finish (uop, result);
        }
      }
    }
    break;

  case NUOp::eUnFFZ:
  case NUOp::eUnFFO:
  case NUOp::eUnFLZ:
  case NUOp::eUnFLO:
    if (isComplement (sub)) {
      op = ( op == NUOp::eUnFFZ ? NUOp::eUnFFO
           : op == NUOp::eUnFFO ? NUOp::eUnFFZ
           : op == NUOp::eUnFLZ ? NUOp::eUnFLO
           :                      NUOp::eUnFLZ);
      result = unaryOp(op, copyExpr(sub->getArg(0)), loc);
      return finish (uop, result);
    }
    break;

  case NUOp::eUnRound:
  {
    // This is not really the correct fix to the problem.  But the test 
    //  test/langcov/Signed/exp_display.v fails because we don't properly
    // maintain the isReal attribute when simplifying constant real
    // arithmetic.  But for now, make that testcase not crash by optimizing
    // round(int) to int
    if (!arg->isReal()) {
      return finish(uop, arg);
    }
  }
    break;

  case NUOp::eUnChange:
    // Think of some optimizations?
    break;

  case NUOp::eUnRealtoBits:
  case NUOp::eUnBitstoReal:
  case NUOp::eUnItoR:
  case NUOp::eUnRtoI:
    break;

  default:
    // Bad operator?
    NU_ASSERT ("Unknown Operator" == 0, uop);
  }

  if (isConstantX (arg->castConst ()))
    // (~1'bx)  (or possibly (^ 4'bxxxx) - just collapse to 'x'
    return finish (uop, copyExpr(arg));

  // Try normal constants
  const NUConst* c = ctce (arg);
  const NUExpr* ret = NULL;
  if (not c)
    return NULL;


  if (c->isReal ()) {

    CarbonReal d;
    if(not c->getCarbonReal (&d))
      return NULL;

    switch (op) {
    case NUOp::eUnMinus: {
      ret = constant(-d, loc, uop_sign);
      break;
    }
    case NUOp::eUnLogNot: {
      ret = (d == 0.0) ? genTrue (loc) : genFalse (loc);
      break;
    }
    case NUOp::eUnPlus:  {
      ret = constant(d, loc, uop_sign);
      break;
    }
    case NUOp::eUnRound: {
      ret = constant(uop->isSignedResult (), carbon_round_real (d),
                             uop_size, loc);
      break;
    }
    case NUOp::eUnRtoI: {
      if ( uop->isSignedResult() ) {
        SInt64 truncated = (SInt64)d;
        ret = constant(true, truncated, uop_size, loc);
      } else {
        UInt64 truncated = (UInt64)d;
        ret = constant(false, truncated, uop_size, loc);
      }
      break;
    } 
    case NUOp::eUnRealtoBits: {
      UInt64 bits = carbon_real_to_bits(d);
      ret = constant(false, bits, 64, loc);
      break;
    }

    case NUOp::eUnItoR:
    case NUOp::eUnBitstoReal: {
      NU_ASSERT(false, uop); // this should be impossible since c is real
      break;
    }

    default:		 NU_ASSERT (0, uop); // no other legal unary operators
    }
  } else {

    DynBitVector cv (c->getBitSize ());
    c->getSignedValue (&cv);

    DynBitVector rv (uop_size);

    switch (op)
      {
      case NUOp::eUnMinus:               //!< Unary -
        rv-=cv;
        break;

      case NUOp::eUnLogNot:              //!< Unary Logical !
        rv = not cv.any ();
        break;

      case NUOp::eUnBitNeg:              //!< Unary Bitwise ~
      case NUOp::eUnVhdlNot:             //!< Unary VHDL Bitwise not
        rv = cv;
        rv.flip ();
        break;

      case NUOp::eUnRedAnd:              //!< Unary Reduction &
        rv = (cv.count () == c->getBitSize ());
        break;

      case NUOp::eUnRedOr:               //!< Unary Reduction |
        rv = cv.any ();
        break;

      case NUOp::eUnRedXor:              //!< Unary Reduction ^
        rv = cv.count () & 1;
        break;


      case NUOp::eUnFFZ:
        cv.flip ();
        // fall-thru
      case NUOp::eUnFFO:
        if (not cv.any ())
          rv.flip ();
        else
          rv = cv.findFirstOne ();
        break;

      case NUOp::eUnFLZ:
        cv.flip ();
        // fall-thru

      case NUOp::eUnFLO:
        if (not cv.any ())
          rv.flip ();
        else
          rv = cv.findLastOne ();
        break;

      case NUOp::eUnCount:		//!< Population Count
        rv = cv.count ();
        break;

      default:
        return NULL;
      }

    // rv describes the result
    ret = constant(uop->isSignedResult (), rv, uop_size, loc);
  }

  return finish (uop, ret);
}

// distribute a ~ operation over a concatenation
const NUExpr* FoldI::distributeNotConcat(const NUExpr* expr) {
  const NUConcatOp* cat = dynamic_cast<const NUConcatOp*>(expr);
  if (cat == NULL) {
    return NULL;
  }

  // Consider three scenarios.
  // 1. ~{a128[127:96],96'hffffffffffffffffffffffff} --> {~a128[127:96],96'b0}
  // 2. ~{a2,b2,c2,26'b0} --> {~a2,~b2,~c2,26'b1}
  // 3. ~({16'h0000,(~a48)} --> {16'hffff,a48}

  // 1. This fold is good for a couple of reasons because it's very wide.
  //    It is almost never good to have to do ~ on a wide bitvector, as it
  //    will have to construct a wide bitvector on the stack.  Or it might
  //    be strip-mined but then any interior folds will not occur.
  // 2. This fold is *bad* because now we have the three shift-ORs rather
  //    than just one, but the two-bit ~ requires extra masking, and there
  //    are three of them rather than one.
  // 3. This fold is good because, even though it was <= 64 bits, the ~
  //    operation disappeared because it canceled an existing ~ or was
  //    immediately applied to a constant.

  // Now consider this variation on 3:
  // 4.    ~{16'h0000,(~a16),b32} --> {16'hffff,a16,~b32}
  // this is probably a win.  The 64-bit ~ is probably OK for
  // codegen, but there is one less ~ after the fold.  A second variation:
  // 5.    ~{16'h0000,a16,b32} --> {16'hffff,~a16,~b32}
  // is probably a wash.  There is one less ~ before folding, but it's
  // a 64-bit ~ which might cost the same as a 16-bit ~ plus a 32-bit ~.
  //
  // These five testcases are all hit in test/fold/strip_bv_expr.v, 
  // out4,out5,out6,out7,out8.

  // You almost have to cost it out to know for sure, but costing is
  // probably not accurate enough to make this determination, and there
  // is always the complexity risk of n^2 issues doing expression costing.
  // So let's just use some heuristics.
  //
  // For wide bitvectors, it's probably good to just distribute the
  // ~ if there is just one single subexpression win.  This admits case #1.
  //
  // For size 64 and less, it's a win if we got a fold out of at least
  // as many sub-expressions as when no subexpr folds occurred.  This admits
  // case 3 & 4, and rejects cases 2 & 5.
  UInt32 numWins = 0;
  UInt32 numLosses = 0;

  NUCExprVector v;
  for (UInt32 i = 0; i < cat->getNumArgs(); ++i) {
    const NUExpr* arg = cat->getArg(i);

    // See if applying the NOT to the arg results in a reduction
    const NUExpr* narg = unaryOp(NUOp::eUnBitNeg, arg, arg->getLoc(),
                                 arg->getBitSize(), specifySign(false));

    // ignore the return value of foldExprTree because we don't consider
    // changing ~foo to !foo a win for the purposes of deciding whether
    // to do this optimization.  Instead, look to see if this is still
    // a unary ~ or !.
    (void) foldExprTree(&narg);

    const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(narg);
    if ((uop != NULL) && ((uop->getOp() == NUOp::eUnBitNeg) ||
                          (uop->getOp() == NUOp::eUnLogNot) ||
                          (uop->getOp() == NUOp::eUnVhdlNot)))
    {
      ++numLosses;
    }
    else {
      ++numWins;
    }
    v.push_back(narg);
  }
  UInt32 size = expr->getBitSize();
  if (((size > LLONG_BIT) && (numWins != 0)) ||
      ((size <= LLONG_BIT) && (numWins >= numLosses)))
  {
    const NUExpr* ncat = concatOp(v, cat->getRepeatCount(), cat->getLoc(),
                                  size, specifySign(cat->isSignedResult()));
    (void) foldExprTree(&ncat);
    return ncat;
  }
  return NULL;
} // const NUExpr* FoldI::distributeNotConcat

// Return the constant value associated with a net
NUExpr *
FoldI::findConstantValue (NUNet *n)
{

  // Don't propagate a constant into a memory.
  if (n->is2DAnything()) {
    return 0;
  }

  // Be sure there is only one driver, or if there are multiple drivers, only
  // one at tie-strength.
  UInt32 num_drivers = 0;
  UInt32 num_tie_drivers = 0;
  FLNode* val = NULL;
  FLNode* tie_val = NULL;
  for (NUNet::DriverLoop p(n->loopContinuousDrivers()); !p.atEnd(); ++p) {
    val = *p;
    NUUseDefNode* node = val->getUseDefNode();

    if ((node != NULL) && (node->getStrength() == eStrTie)) {
      tie_val = val;
      ++num_tie_drivers;
      if (num_tie_drivers == 2) {
        return 0;               // tie conflict.  (detect redundancy?)
      }
    }
    else {
      // It doesn't matter how many non-tie drivers there are, as long
      // as there is a tie driver then we have a valid constant.   Does this
      // create an n^2 scenario?  Perhaps we should keep track of the
      // number of tie-drivers on the NUNet itself so we can exit immediately
      // when finding a multiply driven net with no Tie.
      ++num_drivers;
    }
  }
  // Comment from Al:

  // Only one driver.  If the driver's a constant expression,
  // we should substitute.  I suspect that with constant folding, it's
  // possible that ALL the drivers reduce to the same value and we could
  // still substitute.  But that would be pretty unusual...
  if ((num_tie_drivers == 0) && (num_drivers != 1)) {
    return 0;
  }
  if (tie_val != NULL) {
    val = tie_val;
  }

  NUUseDefNode *def = val->getUseDefNode ();

  NUAssign *as = dynamic_cast<NUAssign*>(def);
  if (not as)
    return 0;

  NUNetRefHdl net_ref = mFactory->createNetRef(n);
  NUNetRefHdl def_ref = val->getDefNetRef();
  if ((*def_ref)!=(*net_ref))
    return 0;

  NUExpr* expr = as->getRvalue();
  if (expr->castConstNoXZ() == NULL)
    // Not a constant value - return failure.
    return 0;

  //UInt32 esize = expr->getBitSize();
  //UInt32 eoffset = 0;

  NULvalue* lv = as->getLvalue();
  NUConcatLvalue* clv = dynamic_cast<NUConcatLvalue*>(lv);
  if (clv != NULL) {
    // find the piece of the concat that def's the net we want,
    // and apply that offset/mask to the rvalue.  But for now,
    // let's just punt.  You can get here to see an example by
    // running test/cust/amd/pogo/pogo with -newSizing, finding:
    //   {a5,b1} = six_bit_const;
    // we were previously mating a5 with six_bit_const, which is
    // wrong.
    return 0;
  }

  return expr;
}

const NUExpr*
FoldI::foldDownToPartsel (const NUVarselRvalue* partsel,
                          const NUBinaryOp* downto)
{
  const NUExpr* result = NULL;

  // Does the downto range evaluate to a constant width?
  const NUExpr* msbExpr = binaryOp(NUOp::eBiMinus,
                                   downto->getArg(0),
                                   downto->getArg(1),
                                   downto->getLoc (),
                                   downto->getBitSize ());
  const NUExpr* foldedMSBExpr = foldAndManage(msbExpr, false);
  if (const NUConst* cWidthMinusOne = foldedMSBExpr->castConst ())
  {
    SInt32 width;
    ConstantRange r (*(partsel->getRange ()));
    cWidthMinusOne->getL (&width);
    ++width;

    if (width <= 0)
      result = genFalse (partsel->getLoc (), r.getLength ());
    else
      {
        if (width < (SInt32)r.getLength ())
          r.setMsb (r.getLsb () + width - 1);

        // Now we have a constant width beginning at downto....
        result = varsel(partsel->getIdentExpr(),
                        downto->getArg(1),
                        r, partsel->getLoc ());
      }
    if (result)
      result = mExprCache->changeSign(result, r.getLength ());
  }
 
  return result;
} // FoldI::foldDownToPartsel

const NUExpr* FoldI::foldVarsel(const NUVarselRvalue *partsel) {
  const SourceLocator& loc = partsel->getLoc ();
  const NUExpr* identExpr = partsel->getIdentExpr();
  const NUExpr* indexExpr = partsel->getIndex();
  const ConstantRange& range = *partsel->getRange();
  
  // find any bottom-up folds first
  {
    bool ret = foldExprTree(&identExpr);
    ret |= foldExprTree(&indexExpr);
    if (ret) {
      return finish(partsel,
                    varsel(identExpr, indexExpr, range, loc,
                           partsel->getBitSize(),
                           specifySign(partsel->isSignedResult()),
                           partsel->isIndexInBounds()));
    }
  }

  NU_ASSERT (partsel->getType () == NUExpr::eNUVarselRvalue, partsel);
  const NUExpr *subtree;
  const NUExpr *folded = NULL;
  const NUBinaryOp *indexBop = dynamic_cast<const NUBinaryOp*>(indexExpr);
  const NUConst *k;
  SInt32 offset;

  if (partsel->getBitSize () < range.getLength ())
  {
    // Reduce the range of the partsel to the size needed
    ConstantRange r (range);
    r.setMsb ( r.getLsb () + partsel->getBitSize () - 1);
    folded = varsel(copyExpr (identExpr),
                                 copyExpr (indexExpr),
                                 r, loc);
  }
  else if (indexBop && indexBop->getOp () == NUOp::eBiDownTo) {
    // VHDL downto expression - check for constant width conversion
    folded = foldDownToPartsel (partsel, indexBop);
  }
  else if ((folded = foldPartsel(identExpr, indexExpr, range, loc, true))) {
    ;
  }
  else if (not partsel->isConstIndex () 
           && getConstantOffset (indexExpr, &subtree, &k)
           && k->getL (&offset))
    // Non-constant indexing expression, but of the form "<exp> +/- <const32>"
  {
    ConstantRange r (range);
    r.adjust (offset);
    const NUExpr* pi = protectIndexExpr (copyExpr (subtree));
    const NUExpr* protectedIdx = foldAndManage(pi, false);
    folded = varsel(identExpr, protectedIdx, r, loc,
                    partsel->getBitSize(),
                    specifySign(partsel->isSignedResult()),
                    partsel->isIndexInBounds());
  }
  else if (range.getLength () == 1)
  {
    // Some simplifications for 1-bit case...
    folded = foldBitsel (identExpr,
                         indexExpr,
                         range.getLsb (),
                         loc,
                         true); // started as a partsel
  }

  const NUExpr* ret = NULL;
  if (folded != NULL)
  {
    folded = padExpr (folded, partsel->getBitSize ());
    if (partsel != folded) {
      ret = finish(partsel, folded);
    }
  }
  return ret;
} // const NUExpr* FoldI::foldVarsel

NUExpr* FoldI::createPartsel(const NUExpr* ident, const ConstantRange& ps,
                             const SourceLocator& loc)

{
  ident = manage(ident, true);
  const NUExpr* partsel = foldPartsel(ident, 0, ps, loc);
  if (partsel == NULL)
    partsel = varsel(copyExpr(ident), ps, loc);
  return partsel->copy(mCC);
}

// Copy an NUExpr so it can be owned by a new NUExpr.  But only do
// this if the NUExpr is not managed by an expression factory.  This
// is needed so that we can share the bitsel and partsel folding code
// between the existing Fold infrastructure and ExprResynth, which wants
// to created pre-folded expressions that contain pre-folded
// factory-owned expressions, and put them directly into the factory.
// if sign is not eDefaultSign then the returned expression will have
// the signedness of 'sign'
const NUExpr* FoldI::copyExpr(const NUExpr* expr, SpecifySign sign )
{
  bool is_managed = expr->isManaged();
  bool adjust_sign = ( (sign != eDefaultSign) && (specifySign(expr->isSignedResult()) != sign));
  bool need_copy = !is_managed || adjust_sign;
  
  if (need_copy) {
    NUExpr *work_expr = expr->copy(mCC);
    if ( adjust_sign ) {
      work_expr->setSignedResult(sign==eSigned);
    }
    if ( is_managed ) {
      expr = manage(work_expr, false);
    }
  }
  return const_cast<NUExpr*>(expr);
}

static NUVectorNet* sGetVectorNet(const NUExpr* expr)
{
  NUVectorNet* vn = NULL;
  const NUIdentRvalue* id = dynamic_cast<const NUIdentRvalue*>(expr);
  if (id != NULL)
    vn = dynamic_cast<NUVectorNet*>(id->getIdent());
  return vn;
}

// Is the expression NULL or a CONST (0)
bool FoldI::isConstantIndexExpr (const NUExpr* expr)
{
  if (! expr)
    return true;
  const NUConst* cexpr = expr->castConst ();
  if (! cexpr)
    return false;
  return cexpr->isZero ();
}

// Create a (possibly) simplified expression for a partselect or return
// NULL if no legal partselect node can be constructed, -OR- if we would
// simply return the same partsel we started with
//
const NUExpr* FoldI::foldPartsel(const NUExpr* ident,
                                 const NUExpr* indexExpr,
                                 const ConstantRange& ps,
                                 const SourceLocator& loc,
                                 bool isPartsel)
{
  
  bool constIndexExpr = isConstantIndexExpr (indexExpr);

  // Simplest `optimization for a new range is
  // ident[ident->getCachedEffectiveBitSize()-1:0] But ONLY if ident is a
  // simple net AND it's unsigned!
  //
  if (constIndexExpr
      && (ps.getLsb () == 0)
      && ((ident->getBitSize() == ps.getLength())
          || ((getCachedEffectiveBitSize(ident) <= ps.getLength ()) 
              && ((ident->isWholeIdentifier()) &&
                  !ident->getWholeIdentifier()->isSigned()))))
  {
    NUExpr* result = ident->copy(mCC);
    // part-selects of signed expressions become unsigned
    result->setSignedResult(false);
    result = result->makeSizeExplicit(ps.getLength());
    return manage(result, false);
  }

  const NUExpr* result = NULL;
  ConstantRange r = ps;         // Refinable copy of the range

  // Check for degenerate cases
  //
  if (!constIndexExpr)
    // No simple folds involving complex index expression
    ;
  else if (r.getMsb () < 0)
    return genFalse (loc, ps.getLength (), false);
  else if (r.getLsb () < 0) {
    //
    // We really want to work with normalized (e.g. positive unsigned partselects.  So, convert
    // an expression like  x[5-:10] into {x[5:0], 4'b0}.
    //
    UInt32 shift = - r.getLsb ();
    r.setLsb (0);
    NUCExprVector pieces;
    pieces.push_back (manage(createPartsel (ident, r, loc), false));
    pieces.push_back (genFalse (loc, shift, false));
    result = concatOp(pieces, 1, loc, ps.getLength ());
    return result;
  } else if (r.getLsb () > r.getMsb ())
    // invalid bounds imply X's, and zero is just fine..
    return genFalse (loc, ident->getBitSize ());


  // Non-trivial folding of partselects

  switch (ident->getType ())
  {

  case NUExpr::eNUConstXZ:
  {
    if (not constIndexExpr) {
      if (isPartsel)
        break;
      return varsel(copyExpr (ident), copyExpr (indexExpr), r, loc);
    }

    // Get the value & drive, shift & resize so it's just the ones we want
    const NUConst* c = ident->castConst ();
    DynBitVector val, drive;
    c->getValueDrive(&val, &drive);

    val >>= r.getLsb();
    val.resize(r.getLength());
    
    drive >>= r.getLsb();
    drive.resize(r.getLength());

    if (drive.any())
      result = constantXZ(false, val, drive, r.getLength(), loc);
    else
      result = constant(false, val, r.getLength(), loc);
  }
  break;

  case NUExpr::eNUConstNoXZ:
  {
    const NUConst* c = ctce (ident);
    if (c && c->isZero ())
      return genFalse (loc, r.getLength ());
    else if (c && c->isOnes () && mNOOOB)
      return genOnes (loc,r.getLength ());

    if (not constIndexExpr) {
      if (isPartsel)
        break;
      return varsel(copyExpr (ident), copyExpr (indexExpr), r, loc);
    }


    // Get the value & drive, shift & resize so it's just the ones we want
    DynBitVector val;
    c->getSignedValue(&val);
      
    val >>= r.getLsb();
    val.resize(r.getLength());
      
    result = constant(false, val, r.getLength(), loc);
    break;
  }

  case NUExpr::eNUIdentRvalueElab:
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUMemselRvalue:
  {
    if (not constIndexExpr) {
      if (not isPartsel) {
        result = varsel(copyExpr (ident), copyExpr (indexExpr), r, loc);
      }
      break;
    }

    const ConstantRange* vecRange = NULL;
    if (NUVectorNet *vn = sGetVectorNet (ident)) {
      vecRange = vn->getRange();
    }
    else if (const NUMemselRvalue *mn = dynamic_cast<const NUMemselRvalue*>(ident)) {
      NUMemoryNet* mnet = mn->getIdent()->getMemoryNet();
      NU_ASSERT(mnet, ident);
      vecRange = mnet->getWidthRange();
    }

    if (vecRange != NULL) {
      if (ps == (*vecRange) && not ident->isSignedResult ())
        // Accessing the whole vector and no sign  extension to worry about.
        result = copyExpr(ident);
      else if (not vecRange->overlaps (ps)) // Completely out-of-range
        result = genFalse (loc, ps.getLength ());
      else
      {
        r = vecRange->overlap (ps);

        if (!isPartsel || (r != ps))
        {
          // we're not just trying to fold an existing partsel into the same
          // partsel...
          result = varsel(copyExpr(ident), r, loc, r.getLength ());
        }
      }
    }
    else if (ps.getLsb () == 0)
      // if the range includes zero, then return the scalar, else return 0
      result = copyExpr (ident);
    else
      result = genFalse (loc, ps.getLength ());
    break;
  }

  case NUExpr::eNUVarselRvalue:
  {
    // check for net[a:b][c:d], transforming to net[b+c:b+d].  b+c <= a.
    const NUVarselRvalue* psident = dynamic_cast<const NUVarselRvalue*>(ident);

    ConstantRange innerPs ( *psident->getRange ());

    // looking at net[i+:w][h:l] => net[i+l+:h-l+1]

    if (constIndexExpr) {
      if (r.getLsb () >= SInt32 (innerPs.getLength ())) { // LSB outside size of object
        // Consider something like  $SIGNED(a[0])[10+:5].  We need 5 copies
        // of the sign bit.
        if (psident->isSignedResult ()) {
          if (isPartsel)
            // already reduced this as far as we can; don't fall into recursive trap
            return NULL;

          result = varsel(psident,
                          ConstantRange (innerPs.getMsb (), innerPs.getMsb ()),
                          loc, 1);
          result = padExpr(result, r.getLength ());
          
        } else {
          result = genFalse (loc, r.getLength ());
        }
        break;
      } else if (r.getMsb () < 0) {// MSB out-of range below
        result = genFalse (loc, r.getLength ());
        break;
      }
    }

    // field access is at least partially in bounds
      
    UInt32 innerLength = std::min (innerPs.getLength () - r.getLsb (), r.getLength ());
    innerPs.setLsb (innerPs.getLsb () + r.getLsb ());
    innerPs.setMsb (innerPs.getLsb () + innerLength-1);

    // pick subrange of original object.
    const NUExpr* newIndex = copyExpr (psident->getIndex ());
    if (not constIndexExpr) {
      newIndex = binaryOp(NUOp::eBiPlus, copyExpr (indexExpr), newIndex, loc);
    }
    result = varsel(copyExpr (psident->getIdentExpr ()), newIndex,
                                 innerPs, loc);
    break;
  }

  // check for {w, x, y, z}[2:1], transform it to {x, y}.
  // Be sure to deal with the more general form:
  //      {v[0], w[3:0], x[3:0], y[3:0], z[0]}[10:3] = {w[1:0], x[3:0], y[3:2]}
  //
  case NUExpr::eNUConcatOp:
  {
    if (not constIndexExpr) {
      if (not isPartsel) {
        result = varsel(copyExpr (ident), copyExpr (indexExpr), r, loc);
      }
      break;
    }

    const NUConcatOp* concat = dynamic_cast<const NUConcatOp*>(ident);
    result = foldConcatPartsel(concat, ps.getMsb(), ps.getLsb(), loc);
    break;
  }

  case NUExpr::eNUTernaryOp:
  {
    const NUTernaryOp* condExpr = dynamic_cast<const NUTernaryOp*>(ident);

    // consider the expression for myclk given this verilog
    //
    //   wire clk_vec[1:0] = sel ? {clk1, clk0}: {~clk1, ~clk0};
    //   wire myclk = clk_vec[0];
    //
    // Expression synthesis constructs this:
    //
    //   wire myclk = (sel ? {clk1, clk0}: {~clk1, ~clk0})[0];
    //
    // Now we want to simplify this to:
    //
    //   wire myclk = sel? clk0: ~clk0;
    //
    // for which we can construct a rational BDD
    
    if (condExpr->getOp() == NUOp::eTeCond)
    {
      // distribute the bitselect over both sides of the cond-expr
      const NUExpr* cond = condExpr->getArg(0);
      const NUExpr* thenExpr = condExpr->getArg(1);
      const NUExpr* elseExpr = condExpr->getArg(2);

      thenExpr = foldPartsel (thenExpr, indexExpr, r,
                              thenExpr->getLoc());
      elseExpr = foldPartsel (elseExpr, indexExpr, r,
                              elseExpr->getLoc());

      if (thenExpr && elseExpr)
      {
        result = ternaryOp(cond, thenExpr, elseExpr, condExpr->getLoc(),
                           r.getLength ());
      }
      else
      {
        cleanupExpr (thenExpr);
        cleanupExpr (elseExpr);
      }
    } // if

    break;
  }

  case NUExpr::eNUBinaryOp:
  {
    const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(ident);
    const NUConst*c;

    NUOp::OpT op = bop->getOp();
    if (isBitwiseOp(op)         // Any subset of bits is fine
      || (ps.getLsb () == 0     // Can handle rightmost bits of +/-
	  && constIndexExpr     // Optimizing shouldn't be done on a non-const index (bug 7825)
          && (op == NUOp::eBiPlus || op == NUOp::eBiMinus))) {
      result = foldBinaryOpPartsel(bop, indexExpr, ps);
    } else if ((op == NUOp::eBiVhExt || op == NUOp::eBiVhZxt) && ((c = ctce (bop->getArg(1))))) {
      // partselect of ZXT or EXT is easy if this is just a size preserving extend.
      UInt32 width;
      if (not c->getUL (&width))
        break;                  // give up on this.

      const NUExpr* arg0 = bop->getArg(0);
      UInt32 arg0size = arg0->getBitSize();
      if (width >= arg0size) {
        ConstantRange exprRange (arg0size - 1, 0);

        if (exprRange.contains (ps))
          // SXT(e1, 32)[7:0] or ZXT(e1, 32)[7:0]
          result = foldPartsel (bop->getArg(0), indexExpr, ps, loc, false);
        else if (not bop->getArg(0)->isSignedResult ()
                 && op == NUOp::eBiVhZxt) {
          // ZXT(u1, 16)[31:0]
          result = foldPartsel (bop->getArg(0), indexExpr, ps, loc, false);
        }

        // make sure we retain full precision
        if (result)
          result = mExprCache->makeSizeExplicit(result);
      }
    }  else if ((op == NUOp::eBiLshift || op == NUOp::eBiLshiftArith) && constIndexExpr) {
      const NUConst* countExpr = bop->getArg(1)->castConst ();
      if (not countExpr)
        break;                  // no opportunities here

//rjc the following may be needed to prohibit the use of resize to reduce the size of an expression      
//rjc      if ( ps.getLength () < bop->getBitSize() ) {
//rjc        break;                  // nothing more to do, the partselect is required
//rjc     }
      
      SInt64 count;
      if (not countExpr->getLL (&count))
        break;

      // (expr << count)[Msb:Lsb] is equivalent to {expr, count'b0}[Msb:Lsb]
      //
      const NUExpr* shiftValue = bop->getArg(0);

      if (count > r.getMsb ()) {
        // shifting past all interesting bits
        result = genFalse (loc, r.getLength ());
      } else if (count > r.getLsb ()
        && (r.getLsb () || not isPartsel)) {
        // Some of the bits are still going to be zeros.  Shift just enough.
        // But don't do this unless the LSB is non-zero (which will change
        // the shift amount) or we didn't start with a partsel (otherwise we
        // end up returning a copy of what we're trying to fold and that's
        // cause for an assertion in finish().
        result = copyExpr (shiftValue);
        result = padExpr (result, r.getLength ());

        count -= r.getLsb ();   // Don't need to shift bits we'd ignore anyway
        r.adjust (-r.getLsb ()); 

        if  (count) {            // Still need some shifted?
          result = binaryOp(op, result,
                                 constant(false, count, 32, loc), loc);
        }
        result = varsel(result, r, loc);
      } else if (count <= r.getLsb ()){
        // We are selecting bits all ABOVE where we shifted in any zeros
        r.adjust (-count);      // skip past the zeroes

        // Reduce the size of the shifted value by the number of bits that the
        // shift would chop off.
        ConstantRange baseRange (shiftValue->getBitSize () - (count + 1), 0);
        result = varsel(copyExpr (shiftValue), baseRange, loc);

        // Now chop off just the bits we care about
        result = varsel(result, r, loc);
      }
    } else if (op == NUOp::eBiRshift) {
      // (a>>b)[x:y] --> a[(y+b) +: (x-y+1)] 
      // This is slightly simpler if 'b' is a constant, since 'y' is
      // definitely a constant.
      const NUExpr* a = bop->getArg(0);
      const NUExpr* b = bop->getArg(1);
      const NUConst* kb = b->castConst();
      SInt32 kv;
      UInt32 width = ps.getLength ();
      bool gotConst;
      if (kb && (gotConst = kb->getL (&kv))) {
        kv += ps.getLsb();
        ConstantRange newPs(width - 1 + kv, kv);
        result = foldPartsel(a, indexExpr, newPs, bop->getLoc(), false);
      } else {
        // Reject if the shift could be out-of-range.  We generate guard
        // code and return zero, but with the partselect you get X's not zeros.
        // We COULD do the conversion if we inserted a guard - but we can't tell if we
        // already inserted a guard, so we'd end up double-guarding if we converted, AND
        // that can't be very smart...
        UInt32 shiftWidth = getCachedEffectiveBitSize(b);
        if (shiftWidth > 16u    // 2**shiftWidth is larger than largest bitvector
            || a->getBitSize () < (1u<<shiftWidth)) // could shift away whole vector
          break;

	// To avoid a simulation mismatch (bug 7901), don't create a varsel if
	// the index of the shift is not constant and the expression is, an
	// add, a subtract or a left shift, operations that could potentially
	// generate "dirty" bits.
	if (a->getType() == NUExpr::eNUBinaryOp &&
	    NULL == kb) {
	  const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(a);
	  NUOp::OpT op = bop->getOp();	  
	  if (op == NUOp::eBiPlus || op == NUOp::eBiMinus || op == NUOp::eBiLshift) 
	    break;
	}

        // Verilog treats the shift count as UNSIGNED, so we have to treat
        // b as an unsigned value!
        // 
        const NUExpr* y_plus_b = NULL;
        if (ps.getLsb() != 0) {
          const NUConst* y = constant(false, ps.getLsb(), 32, bop->getLoc());
          b = copyExpr (b);
          b = mExprCache->changeSign(b, false);
          y_plus_b = binaryOp(NUOp::eBiPlus, b, y, bop->getLoc());
        } else {
          y_plus_b = copyExpr (b);
        }
        if (not constIndexExpr) {
          y_plus_b = binaryOp(NUOp::eBiPlus, y_plus_b, copyExpr (indexExpr), loc);
          // adding two unsigned number might cause overflow unless we increase the size of the result (bug 15222)
          y_plus_b = mExprCache->resize(y_plus_b, (1+ y_plus_b->getBitSize()));
        } else {
          y_plus_b = mExprCache->resize(y_plus_b);
        }
        
        ConstantRange newPs(width - 1, 0);
        result = varsel(copyExpr (a), y_plus_b, newPs, bop->getLoc());
      }
    } else if (op == NUOp::eBiPlus) {
      result = blastAdd(bop, ps);
    }

    break;
  } // case NUExpr::eNUBinaryOp:

  case NUExpr::eNUUnaryOp:
  {
    const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(ident);

    if (isBitwiseOp(uop->getOp()))
      result = foldUnaryOpPartsel(uop, indexExpr, ps);
    else if (constIndexExpr && (isReductionOp (uop->getOp ()) || isComplement (uop)))
      // could we treat non-const index as "!(index) & reductionExpr"??
    {
      if (ps.getLsb () == 0 || uop->isSignedResult ())
        result = copyExpr (uop);
      else
        result = genFalse (uop->getLoc (), ps.getLength ());
    }

    break;
  }

  
  default:
    // No particular optimizations left here....
    // distribute the partsel to whatever this is unless we were
    // starting with a partsel.
    if (not isPartsel) {
      if (indexExpr)
        result = varsel(copyExpr (ident), copyExpr (indexExpr), r, loc);
      else
        result = varsel(copyExpr (ident), r, loc);
    }
    break;
  }

  if (result) {
    UInt32 newSize = std::max (ps.getLength (), result->determineBitSize ());
    result = mExprCache->resize(result, newSize);
  }

  return result;
} // NUExpr* FoldI::foldPartsel

const NUExpr* FoldI::blastAddGetOperand(const NUBinaryOp* bop, UInt32 bit,
                                  UInt32 index)
{
  const NUExpr* a = bop->getArg(index);
  UInt32 asize = a->determineBitSize();
  if (asize <= bit) {
    a = NULL;
  }
  else if ((asize > 1) || (bit == 1)) {
    a = manage(createPartsel(a, ConstantRange(bit, bit), bop->getLoc()), false);
  }
  else {
    a = copyExpr(a);
  }
  return a;
}

const NUExpr* FoldI::blastAddComputeOp(const NUBinaryOp* bop, UInt32 bit,
                                 NUOp::OpT op)
{
  const NUExpr* a = blastAddGetOperand(bop, bit, 0);
  const NUExpr* b = blastAddGetOperand(bop, bit, 1);
  if (a == NULL) {
    return b;
  }
  else if (b == NULL) {
    return a;
  }
  const NUExpr* result = binaryOp(op, a, b, bop->getLoc(), 1);
  return result;
}

const NUExpr* FoldI::blastAdd(const NUBinaryOp* bop, const ConstantRange& ps) {
  UInt32 len = ps.getLength();
  UInt32 lsb = ps.getLsb();

  // Don't compute multi-bit results.
  if (len != 1) {
    return NULL;
  }

  // Avoid dealing with sign-extension
  if (bop->isSignedResult() &&
      ((bop->getArg(0)->getBitSize() <= lsb) ||
       (bop->getArg(1)->getBitSize() <= lsb)))
  {
    return NULL;
  }

  const SourceLocator& loc = bop->getLoc();

  // If a sum1 bit is required, then create it
  const NUExpr* result = NULL;
  switch (lsb) {
  case 0:
    result = blastAddComputeOp(bop, 0, NUOp::eBiBitXor);
    break;
  case 1: {
    const NUExpr* carry = blastAddComputeOp(bop, 0, NUOp::eBiBitAnd);
    const NUExpr* sum1 = blastAddComputeOp(bop, 1, NUOp::eBiBitXor);

    // sum1 can be NULL when operations are mis-sized.
    // See test/fold/blast-add2.v
    if (sum1 == NULL) {
      result = carry;
    }
    else {
      result = binaryOp(NUOp::eBiBitXor, carry, sum1, loc, 1);
    }
    break;
  }
  default:
    return NULL;
    break;
  }

  result = mExprCache->resize(result, 1);
  return result;
} // NUExpr* FoldI::blastAdd

// While we normally want to avoid constant bitselects, this is used in selecting
// from a concat.
//
const NUExpr* FoldI::foldPartselBitsel(const NUVarselRvalue* ps, SInt32 bit,
                                       const SourceLocator& loc)
{
  const ConstantRange *r = ps->getRange();
  bit += r->getLsb();
  
  if (ps->isConstIndex ()) {
    if (not r->contains (bit))
      return genFalse (loc);
  }

  ConstantRange range (bit, bit);

  const NUExpr* ret = varsel(ps->getIdentExpr(), ps->getIndex (), range, loc);
  return ret;
}

// check for {w, x, y, z}[2:1], transform it to {x, y}.
// Be sure to deal with the more general form:
//      {v[0], w[3:0], x[3:0], y[3:0], z[0]}[10:3] = {w[1:0], x[3:0], y[3:2]}
//
// If the concat contains arbitrary expressions, we will end up with an illegal partsel. 
// These must be eliminated or the fold can't be permitted to 'stand'.
//
const NUExpr* FoldI::foldConcatPartsel(const NUConcatOp* concat,
                                 SInt32 msb, SInt32 lsb,
                                 const SourceLocator& loc)
{
  UInt32 resultWidth = msb + 1 - lsb; // How wide do we NEED from the partsel

  if (lsb >= SInt32 (concat->determineBitSize ())
    || msb < 0)
    // Completely out of range - just return a zero of the requested partsel size
    return genFalse (loc, resultWidth, false);

  if (concat->sizeVaries ())
    return NULL;

  NUCExprVector v;

  // Something like:
  //	{a[9:0],b[9:0],c[9:0],d[9:0],e[9:0]}[34:15]
  // xform into
  //    {b[4:0],c[9:0],d[9:5]}
  //
  // Note: Unlike NUConcatLvalues, we have to worry about repeat-counts

  SInt32 lowPos = 0;            // How far thru the input have we come

  for (SInt32 repeatCount = concat->getRepeatCount ();
       (repeatCount > 0) && (lsb <= msb);
       --repeatCount)
    for (SInt32 n = concat->getNumArgs () - 1;
         (n >= 0) && (lsb <= msb);
         --n)
    {
      const NUExpr* piece = concat->getArg(n);
      SInt32 highPos = piece->getBitSize () + lowPos - 1;  // upper bound represented by piece

      if (highPos >= lsb)       // We're in range of bits we want
      {
        ConstantRange r (std::min (highPos, msb), lsb);
        r.adjust (-lowPos);
        if (const NUExpr *rv = foldPartsel (piece, 0, r, loc, false)) {
          // Add to list (in reverse order)
          v.push_back (padExpr(rv, r.getLength()));
        }
        else
        {
          // Unable to partselect this concat member - abandon after cleanup
          // of previously folded pieces.
          std::for_each (v.begin (), v.end (), FoldI::cleanupExpr);
          return NULL;
        }
        lsb += r.getLength ();
      }

      if (msb <= highPos)
        break;                  // extracted all the bits we need...

      lowPos = highPos+1; // Advance to where next concat member will start
    }

  // v now has the pieces that participate in the partselect of the concat,
  // There better be some - we have already dealt with the out-of-bounds case
  //
  NU_ASSERT (!v.empty (), concat);

  std::reverse (v.begin (), v.end ());
  const NUExpr* folded = concatOp(v, 1, loc, resultWidth);

  return folded;
}

// Create a pre-folded bit-select expression.  It is assumed that the
// resulting partselect will be legal (or that it will be folded away into
// a legal expression.
NUExpr* FoldI::createBitsel(const NUExpr* expr, const NUExpr* sel,
                            const SourceLocator& loc)
{
  expr = manage(expr, true);
  sel = manage(sel, true);
  const NUExpr* bitsel = foldBitsel(expr, sel, 0, loc);
  if (bitsel == NULL)
  {
    ConstantRange r (0,0);

    if (const NUConst *c = ctce (sel))
    {
      SInt32 off;
      c->getL (&off);
      r.adjust (off);

      bitsel = varsel(copyExpr (expr), r, loc);
    }
    else
    {
      bitsel = varsel(copyExpr(expr), copyExpr(sel), r, loc);
    }
  }
  bitsel = mExprCache->resize(bitsel, 1);
  return bitsel->copy(mCC);
}

const NUExpr* FoldI::foldBinaryOpPartsel(const NUBinaryOp* bop, const NUExpr* index, const ConstantRange& range)
{
  const NUExpr* a = foldPartsel(bop->getArg(0), index, range, bop->getArg(0)->getLoc());
  const NUExpr* b = foldPartsel(bop->getArg(1), index, range, bop->getArg(1)->getLoc());
  const NUExpr* result = NULL;
  if (a && b)
  {
    result = binaryOp(bop->getOp(), a, b, bop->getLoc(), range.getLength ());
  }
  else
  {
    cleanupExpr (a);
    cleanupExpr (b);
  }
  return result;
}

const NUExpr* FoldI::foldUnaryOpPartsel(const NUUnaryOp* uop, const NUExpr* index, const ConstantRange& range)
{
  const NUExpr* a = foldPartsel(uop->getArg(0), index, range, uop->getArg(0)->getLoc());
  const NUExpr* result = NULL;
  if (a) {
    NUOp::OpT opcode = uop->getOp ();
    result = unaryOp(opcode, a, uop->getLoc(), range.getLength ());
  }
  return result;
}

// optimize "(3 ^ {crc.data_in[6],crc.data_in[7]})[0]" to ~crc.data_in[7]
const NUExpr* FoldI::foldBinaryOpBitsel(const NUBinaryOp* bop, const NUConst* bit)
{
  const NUExpr* a = foldBitsel(bop->getArg(0), bit, 0, bop->getArg(0)->getLoc());
  const NUExpr* b = foldBitsel(bop->getArg(1), bit, 0, bop->getArg(1)->getLoc());
  const NUExpr* result = NULL;
  if (a && b)
  {
    result = binaryOp(bop->getOp(), a, b, bop->getLoc(), 1);
  }
  else
  {
    cleanupExpr (a);
    cleanupExpr (b);
  }
  return result;
}

// optimize "(~{crc.data_in[7],crc.data_in[7]})[0]" to ~crc.data_in[7]
const NUExpr* FoldI::foldUnaryOpBitsel(const NUUnaryOp* uop, const NUConst* bit)
{
  const NUExpr* a = foldBitsel(uop->getArg(0), bit, 0, uop->getArg(0)->getLoc());
  const NUExpr* result = NULL;
  if (a) {
    NUOp::OpT opcode = uop->getOp ();
    result = unaryOp(opcode, a, uop->getLoc(), 1);
  }
  return result;
}

//! helper for FoldI::foldBitsel
const NUExpr* FoldI::foldBitselConstIndex(const NUExpr* ident, const NUConst* const_index, SInt32 off,
                                    const SourceLocator& loc, bool isBitsel)
{
  const NUExpr* folded = NULL;


  // If accessing a scalar net, we don't care what we indexed - either it's zero, or we're OOB
  // and reading the bit we've got is just fine.
  if (ident->determineBitSize() == 1)
  {
    folded = copyExpr(ident);
  }
  else
  {
    SInt32 bit;
    bool smallIndex = const_index->getL(&bit);
    bit += off;

    SInt32 width = ident->getBitSize ();
    if (bit < 0 || bit >= width)
      return genFalse (loc, 1, false); // out-of-bounds access!

    switch (ident->getType ())
    {
    case NUExpr::eNUIdentRvalueElab:
    case NUExpr::eNUIdentRvalue:
      if (isBitsel)
        // Don't recreate existing node...
        break;

      folded = varsel(ident, ConstantRange (bit, bit), loc, 1);
      break;

    case NUExpr::eNUConcatOp:
      if (smallIndex)
        {
          const NUConcatOp* concat = dynamic_cast<const NUConcatOp*>(ident);
          folded = foldConcatPartsel(concat, bit, bit, loc);
        }
      break;
          
    case NUExpr::eNUVarselRvalue:
      if (smallIndex)
        {
          const NUVarselRvalue* ps = dynamic_cast<const NUVarselRvalue*>(ident);
          folded = foldPartselBitsel(ps, bit, loc);
        }
      break;

    case NUExpr::eNUConstXZ:
      if (smallIndex)
        {
          const NUConst* kident = ident->castConst();
          
          DynBitVector kidentVal, kidentDrv;
          kident->getValueDrive(&kidentVal, &kidentDrv);
          bool val = false;
          bool drv = false;
          if (bit < SInt32 (kidentVal.size()))
          {
            val = kidentVal.test(bit);
            drv = kidentDrv.test(bit);
          }
          folded = constantXZ(false, val, drv, 1, const_index->getLoc());
        }
      break;

    case NUExpr::eNUConstNoXZ:
      // optimize 0000001f[00000001] == 1 (pogo)
      if (smallIndex)
        {
          const NUConst* kident = ident->castConst();

          DynBitVector kidentVal;
          kident->getSignedValue(&kidentVal);
          folded = constant(false, (bit >= SInt32 (kidentVal.size ())) ? 0 : kidentVal.test(bit),
                                   1, const_index->getLoc());
        }
      break;

    case NUExpr::eNUBinaryOp:
      {
        const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(ident);

        if (isBitwiseOp(bop->getOp()))
          folded = foldBinaryOpBitsel(bop, const_index);
      }
      break;

    case NUExpr::eNUUnaryOp:
      {
        const NUUnaryOp* uop = dynamic_cast<const NUUnaryOp*>(ident);

        if (isBitwiseOp(uop->getOp()))
          folded = foldUnaryOpBitsel(uop, const_index);
        else if (isReductionOp (uop->getOp ()))
        {
          if (bit == 0 || uop->isSignedResult ())
            folded = copyExpr (uop); // e.g. (&VEC)[0] == (&VEC)
          else
            folded = genFalse (uop->getLoc (), 1);
        }
      }
      break;

    case NUExpr::eNUTernaryOp:
      // consider the expression for myclk given this verilog
      //
      //   wire clk_vec[1:0] = sel ? {clk1, clk0}: {~clk1, ~clk0};
      //   wire myclk = clk_vec[0];
      //
      // Expression synthesis constructs this:
      //
      //   wire myclk = (sel ? {clk1, clk0}: {~clk1, ~clk0})[0];
      //
      // Now we want to simplify this to:
      //
      //   wire myclk = sel? clk0: ~clk0;
      //
      // for which we can construct a rational BDD
      {
        const NUTernaryOp* condExpr = dynamic_cast<const NUTernaryOp*>(ident);

        if (condExpr->getOp() == NUOp::eTeCond)
        {
          // distribute the bitselect over both sides of the cond-expr
          const NUExpr* cond = condExpr->getArg(0);
          const NUExpr* thenExpr = condExpr->getArg(1);
          const NUExpr* elseExpr = condExpr->getArg(2);

          ConstantRange range (bit, bit);
          
          thenExpr = foldPartsel (thenExpr,
                                  0,
                                  range,
                                  thenExpr->getLoc());
          elseExpr = foldPartsel (elseExpr,
                                  0,
                                  range,
                                  elseExpr->getLoc());
          if (thenExpr && elseExpr)
          {
            folded = ternaryOp(cond, thenExpr, elseExpr, condExpr->getLoc(),
                               range.getLength ());
          }
          else
          {
            cleanupExpr (thenExpr);
            cleanupExpr (elseExpr);
          }
        } // if
      }
      break;

    default:
      break;
    } // switch
  }
  return folded;
}

//! helper for FoldI::foldBitsel
const NUExpr* FoldI::foldBitselConstObject(const NUConst* cIdent, const NUExpr* index, SInt32 off,
                                     const SourceLocator& loc, bool isBitselConst)
{
  const NUExpr* folded = NULL;

  // Special case for a constant object...
  if (cIdent->isZero ()) {
    // 0[k] is always '0' for any k
    return genFalse (loc);
  }

  // If we aren't bounds-checking OR the index value is guaranteed to be
  // inside the range, then look for a subrange of all zero or all one
  UInt32 indexWidth = getCachedEffectiveBitSize(index);
  if (mNOOOB) {
    // when not bounds-checking, only have to handle 65k bit widths
    indexWidth = std::min (indexWidth, 16u);
  }

  UInt32 indexLimit = 0;
  bool indexLimitValid = false;
  if (indexWidth <= 16U) {      // arbitrary limit
    indexLimit = 1 << indexWidth;
    indexLimitValid = true;
  }

  if (not isBitselConst &&
      (!indexLimitValid || (indexLimit > cIdent->getBitSize ())))
  {
    // Can't optimize non-constant index
    const ConstantRange range (cIdent->getBitSize ()-1, 0);
    const NUExpr *newIndex;

    if (off) {
      newIndex = binaryOp(NUOp::eBiPlus, copyExpr (index),
                              constant(false, off,  32, loc),
                          loc);
    } else
      newIndex = copyExpr (index);

    folded = varsel(copyExpr (cIdent), newIndex, loc, range.getLength ());
    return folded;
  }

  DynBitVector identValue;
  cIdent->getSignedValue (&identValue);
  if (indexLimitValid && (indexLimit < identValue.size())) {
    identValue.resize(indexLimit);
  }

  // How many bits in the constant are set?
  UInt32 setBits = identValue.count ();

  if (setBits == 0) {
    // All possible bits are zero
    return genFalse (loc);
  } else if (indexLimitValid && (setBits == indexLimit)) {
    // All possible bits are set
    return genTrue (loc);
  } else if (not isBitselConst) {
    // Must change into NUBitselConstRvalue

    const NUExpr* newIndex;

    if (off) {
      newIndex = binaryOp(NUOp::eBiPlus, copyExpr (index),
                          constant(false, off,  32, loc),
                          loc);
    } else
      newIndex = copyExpr (index);

    folded = varsel(copyExpr (cIdent), newIndex, loc, 1);
    return folded;
  }

  return folded;
}

//! helper for FoldI::foldBitsel
const NUExpr* FoldI::foldBitselIfAllObjBitsSame(const NUExpr* ident, const NUExpr* index, SInt32 /*off*/, const SourceLocator& loc)
{
  const NUExpr* folded = NULL;
  // we are looking for the situation where all possible bit selections are an identical expression
  // Assume that the 'off' doesn't materially affect the index expression - it's just been
  // separated out by the normalizing of NUVarsels

  // first check to see that the index can only address bits from
  // within the ident expression, anything outside should be an x.
  UInt32 indexWidth = getCachedEffectiveBitSize(index);
  UInt32 indexLimit = 1 << indexWidth;

  UInt32 identWidth = ident->determineBitSize();
  
  if ( indexLimit > identWidth ){
    // index could address more bits than available in ident, so some
    // could be x, while others have a value
    return NULL;
  }

  // we know of two special cases where this routine would be
  // applicable, look for them first.

  // look for special case 1: (cond ? allones : allzeros)[index]
  const NUTernaryOp* condExpr = dynamic_cast<const NUTernaryOp*>(ident);
  if ( condExpr ){
    // check that one sub-operand is all ones, and the other all
    // zeros, (or the opposite, so keep track of polarity)
    const NUExpr* cond = condExpr->getArg(0);
    const NUExpr* thenExpr = condExpr->getArg(1);
    const NUExpr* elseExpr = condExpr->getArg(2);
    if ( ( thenExpr->determineBitSize() >= indexLimit )  and
         ( elseExpr->determineBitSize() >= indexLimit ) ) {
      const NUConst* thenConst = thenExpr->castConst();
      if ( thenConst ) {
        const NUConst* elseConst = elseExpr->castConst();
        if ( elseConst ){
          if (thenConst->isOnes() and elseConst->isZero()) {
            // found the situation where result is simply the condition
            folded = copyExpr (cond);
            return folded;
          } else if (thenConst->isZero() and elseConst->isOnes()) {
            const NUExpr* temp = copyExpr (cond);
            folded = unaryOp(NUOp::eUnLogNot, temp, loc);
            return folded;
          }
        }
      }
    }
  }

  // There is a potential second special case that could be considered
  // here: 
  // special case 2: {repeat{cond}}[index]  // where repeat >= indexLimit
  // but I do not think that it will ever be found because concats are
  // converted into 'if' stmts, and then later they are converted to
  // ternary statements (and thus would be handled by the code
  // above).  I do not know of a way to get a concat operation when
  // this method is called.
  // 
  // If a testcase is found in the future, in order to perform the
  // conversion:    {repeat{cond}}[index]  --> cond
  // the following must be true:
  //    ident must be a NUConcatOp
  //    cond must be a single bit (so that all bits are identical),
  //    repeat must be >= the number of bits addressable by index


  // otherwise an unrecognized special case.
  // here is where we would enumerate each bit, building an expression
  // for each and checking to see that they are all identical.
  // for now this expansion is not implemented

  return folded;
}



// Fold a bit-select, or return NULL if it cannot be folded.
// \a ident and \a index are the base expression and the bit index
// values. \a isBitsel indicates if this was an
// NUVarselRvalue originally, and we don't want to get fooled
// into folding into an identical object.
//
const NUExpr* FoldI::foldBitsel(const NUExpr* ident, const NUExpr* index, SInt32 off,
                          const SourceLocator& loc, bool isBitsel)
{
  const NUExpr* folded = NULL;
  const NUConst *const_index = ctce (index);

  
  if (const_index ){
    folded = foldBitselConstIndex(ident, const_index, off, loc, isBitsel);
  } else {
    // not a constant index

    if (const NUConst *cident = ctce (ident))
    {
      // Special case for variable index with a constant object...
      folded = foldBitselConstObject(cident, index, off, loc, isBitsel);
    } else {
      // variable index, variable object, there are still some possibilities 

      // first look for special case where all bits of ident are same
      if ((folded = foldBitselIfAllObjBitsSame(ident, index, off, loc)))
        ;
      else if (ident->getType () != NUExpr::eNUVarselRvalue)
        ;
      else if (const NUVarselRvalue *rv = dynamic_cast<const NUVarselRvalue *>(ident))
      {
        // (x[10:0])[i] => x[i] (if i is out of [10:0] range then 'x' can be anything.)

        const NUExpr* newIndex = index;
        if (!rv->isConstIndex ())
          // x[j+:k][i] => x[j+i]
          newIndex = binaryOp(NUOp::eBiPlus, newIndex, rv->getIndex(), loc);
        else if (SInt32 lbound = rv->getRange ()->getLsb ())
          // x[H:L][i] => x[L+i]
          newIndex = binaryOp(NUOp::eBiPlus, newIndex,
                              constant(false, lbound, 32, loc),
                              loc);

        if (off == 0) {
          newIndex = mExprCache->resize(newIndex, 32);
          folded = manage(createBitsel(rv->getIdentExpr (), newIndex, loc), false);
        } else {
          NU_ASSERT(!mNoFinish, rv);
          folded = 
            varsel(rv->getIdentExpr(), newIndex, ConstantRange (off, off),
                   loc, 1);
          folded = copyExpr(foldAndManage(folded, false));
        }
      }
    }
  }

  if (folded) {
    folded = mExprCache->resize(folded, 1);
  }
  return folded;
} // NUExpr* FoldI::foldBitsel

NUExpr*
FoldI::getNetDriverValue (NUNet* net, UInt32 size)
{
  if (NUExpr* result = findConstantValue (net))
  {
    // Looking for a boolean result
    if (size == 1 && result->getBitSize () != 1)
    {
      NUConst *c = ctce (result); // must be a constant

      result = NUConst::create (false, not c->isZero (), 1, result->getLoc ());
    }
    else
    {
      result = result->copy(mCC);
    }
    result->resize (size);
    return result;
  }

  return 0;
}

class FoldNetConstSubCallback : public NuToNuFn {
public:
  FoldNetConstSubCallback(FoldI* foldi) : mFoldI(foldi) {}
  
  NUExpr* netToExpr(NUNet* net, Phase) {
    NUExpr* result = mFoldI->getNetDriverValue (net, net->getBitSize ());
    return result;
  }

  NUExpr* operator()(NUExpr *, Phase) {
    return NULL;
  }

private:
  FoldI* mFoldI;
};

const NUExpr* FoldI::foldIdent(const NUIdentRvalue *iv) {
  if (not iv->getIdent()->is2DAnything() &&
      ((mFlags & eFoldNoNetConstants) == 0))
  {
    // Look for a single driver
    if (NUExpr* result = getNetDriverValue (iv->getIdent (), iv->getBitSize ()))
      return finish (iv, manage(result, false));
  }
  return NULL;
}

// NUExprReplace replaceLeaves callback for expressions.  We actually
// don't want to use replaceLeaves as a mechanism to manipulate the
// guts of expressions, because we need to use Factories to avoid n^2
// behavior in large exprssions, and factories don't work well with
// replaceLeaves.  So we must keep track of our entry into an
// expression sub-tree and call our own recursive-descent folder as we
// are leaving the tree.
NUExpr* FoldI::processExpr(NUExpr *e) {
  NUExpr* repl = NULL;
  NUExpr* rep = e;

  // First, substitute any constant nets
  if ((mFlags & eFoldNoNetConstants) == 0) {
    FoldNetConstSubCallback replaceNets(this);
    rep = e->translate(replaceNets);
  }

  // Finally, apply recursive expression folding
  repl = foldExpr(rep);
  if (repl == e) {
    repl = NULL;
  }
  return repl;
}

bool FoldI::foldDispatch(const NUExpr** e) {
  NU_ASSERT((*e)->isManaged(), *e);

  const NUExpr* repl = NULL;
  switch ((*e)->getType ()) {
  default:
    break;

  case NUExpr::eNUBinaryOp:
    repl = foldBinaryOp(dynamic_cast<const NUBinaryOp*>(*e));
    break;
  case NUExpr::eNULut:
    repl = foldLut(dynamic_cast<const NULut*>(*e));
    break;
  case NUExpr::eNUMemselRvalue:
    repl = foldMemsel(dynamic_cast<const NUMemselRvalue*>(*e));
    break;
  case NUExpr::eNUSysFunctionCall:
    repl = foldSysFuncCall(dynamic_cast<const NUSysFunctionCall*>(*e));
    break;
  case NUExpr::eNUConcatOp:
    repl = foldConcatOp(dynamic_cast<const NUConcatOp*>(*e));
    break;
  case NUExpr::eNUUnaryOp:
    repl = foldUnaryOp(dynamic_cast<const NUUnaryOp*>(*e));
    break;
  case NUExpr::eNUTernaryOp:
    repl = foldTernaryOp(dynamic_cast<const NUTernaryOp*>(*e));
    break;
  case NUExpr::eNUIdentRvalue:
    repl = foldIdent(dynamic_cast<const NUIdentRvalue*>(*e));
    break;
  case NUExpr::eNUVarselRvalue:
    repl = foldVarsel(dynamic_cast<const NUVarselRvalue*>(*e));
    break;
  case NUExpr::eNUConstNoXZ:
  case NUExpr::eNUConstXZ:
    repl = foldConst((*e)->castConst());
    break;
  } // switch

  bool ret = false;
  if (repl != NULL) {
    NU_ASSERT(repl->isManaged(), repl);
    ret = *e != repl;
    *e = repl;
  }

  return ret;
} // const NUExpr* FoldI::foldDispatch

static ptrdiff_t sCompareExprCost(const NUExpr* e1, const NUCost& c1,
                                  const NUExpr* e2, const NUCost& c2)
{
  // The most important criteria is the number of instructions
  ptrdiff_t cmp = c1.mInstructionsRun - c2.mInstructionsRun;
  if (cmp == 0) {
    // The second most is the number of gates estimated
    cmp = c1.mAsicGates - c2.mAsicGates;

    // Next, use the lexical comparison for the expressions, taking
    // into account source-locators.
    if (cmp == 0) {
      cmp = e1->compare(*e2,
                        true,   // compare locators
                        false,  // do not compare pointers
                        true);  // do compare sizes of constants

      // Finally, use pointer-comparison as a last resort
      if (cmp == 0) {
        cmp = carbonPtrCompare(e1, e2);
      }
    }
  }
  return cmp;
} // static ptrdiff_t sCompareExpr

// Fold an expression by examining the tree with brute force code
bool FoldI::foldExprTree(const NUExpr **e) {
  NU_ASSERT((*e)->isManaged(), *e);

  bool was_changed = mChanged;

  const NUExpr* orig = *e;
  NUExprExprHashMap::iterator p = mExprMap->find(orig);
  if (p != mExprMap->end()) {
    // This expression was folded before, use those results.
    const NUExpr* folded = p->second;
    *e = folded;
  } else {
    // Iterate until foldDispatch can't find any more, or
    // starts cycling back on itself.  Then try BDDs, and pick the best.

    // test/fold/bigor.v shows a case where folding oscillates between several
    // different expr forms (in aggressive mode only).
    //   fold(a)==b, fold(b)==c, fold(c)==d, fold(d)==a
    // To settle this deterministically, we need to keep a set of equivalent
    // forms, and in the event of a cycle, we stop folding and pick the lowest
    // cost or alphabetically earliest form as the winner.
    NUCExprHashSet equiv;
    equiv.insert(orig);

    // We keep folding as long as the dispatcher finds more folds, and we
    // are not cycling back to one we already encountered.
    bool cyclic = false;
    while (!cyclic && foldDispatch(e)) {
      cyclic = !equiv.insertWithCheck(*e);
    }

    // If we hit a cycle, then pick the best choice via costing.  Note that
    // cyclic folds are probably a bug.  We have one fold that thinks it's
    // good to go A->B, and anothe that thinks it's better to go B->A.  It
    // happens, but not often.  One case where it does is
    //   test/cust/s3/metro/carbon/cwtb/DZ_MEU
    // where an expression cycles:
    //   ($flatten_dz_daddr_i_d_bpp | ($cds_delay_entry_data;2[16] | $cds_delay_entry_data;2[44]))
    //   ($cds_delay_entry_data;2[16] | ($cds_delay_entry_data;2[44] | $flatten_dz_daddr_i_d_bpp))
    //   ((|($cds_delay_entry_data;2[47:0] & 48'h100000010000)) | $flatten_dz_daddr_i_d_bpp)
    //   ($flatten_dz_daddr_i_d_bpp | (|($cds_delay_entry_data;2[47:0] & 48'h100000010000)))
    // I think the issue is that BDD-folding blasts apart the 48-bit mask
    // that was created by reassociate, and Costing thinks the blasted form
    // is better.  We should fix costing in this case, but we should also
    // not crash or perform erratically when this occurs.
    if (cyclic) {
      // Pick the best Fold, not necessarily the one we finalized on
      bool first = true;
      NUCost bestCost, exprCost;
      for (NUCExprHashSet::iterator p = equiv.begin(), last = equiv.end();
           p != last; ++p)
      {
        const NUExpr* expr = *p;
        if (first) {
          mCostContext->calcExpr(expr, &bestCost);
          *e = expr;
          first = false;
        }
        else {
          exprCost.clear();
          mCostContext->calcExpr(expr, &exprCost);
          if (sCompareExprCost(expr, exprCost, *e, bestCost) < 0) {
            bestCost = exprCost;
            *e = expr;
          }
        }
      }
    }
      
    // Go to the BDD package and see if it's got a better answer
    // which don't look great for costing, but are actually OK.  (?)
    if (!mBDDRecursionBlocker && ((*e)->getBitSize() == 1) &&
        mFoldBDD &&
        mBDDPrescan->isBDDPromising(*e))
    {
      if ( mTraceBDDFold != 0){
        mBDDPrescan->print(*e, ((mTraceBDDFold & 0x2)!= 0));
      }
      const NUExpr* bddExpr = foldExprBdd(*e);
      if (bddExpr != NULL) {
        *e = bddExpr;
        (void) equiv.insertWithCheck(bddExpr);
      }
    }

    // If we found a top-down fold, then we may not have visited
    // all the sub-nodes, and we may still find fruit there.  So
    // do a round of bottom-up folding and see if that improves things
    // relative to the current winner.
    if (cyclic && !isBottomUp()) {
      mFlags |= eFoldBottomUp;
      // We keep folding as long as the dispatcher finds more folds, and we
      // are not cycling back to one we already encountered.
      NUCExprHashSet bottomUpCache;
      while (foldDispatch(e) && bottomUpCache.insertWithCheck(*e)) {
      }
      equiv.insertSet(bottomUpCache);
      mFlags &= ~eFoldBottomUp;
    }

    // Map all the forms to the winner
    for (NUCExprHashSet::iterator p = equiv.begin(), last = equiv.end();
         p != last; ++p)
    {
      const NUExpr* expr = *p;
      (*mExprMap)[expr] = *e;
    }
  }

  bool this_changed = *e != orig;
  mChanged = this_changed | was_changed;

  return this_changed;
} // bool FoldI::foldExprTree

// Fold an expression by converting to a BDD and back to an expression,
// and cost-comparing.
const NUExpr* FoldI::foldExprBdd(const NUExpr* e) {
  const NUExpr* repl = NULL;

  // See if we've already transformed this expression, in which case we do
  // not need to go to the engine
  bool changed = false;

  // This code is probably a good idea, but it causes us to run out
  // of memory in test/cust/emc/bookman/carbon/cwtb/BOOKMAN, because
  // it's generating very large expressions, perhaps through copy-prop,
  // and the congruent objects take a lot of space.  This deserves
  // further investigation.  One possibility is that the CongruentObject
  // code should avoid retaining its serialized form, and instead should
  // regenerate it on demand.
#define AVOID_FOLDS_THRU_CONGRUENCE 0
#if AVOID_FOLDS_THRU_CONGRUENCE
  // Before bothering to compute a BDD on an expression, see if similar
  // expressions have yielded anything interesting for us in the past
  const NUExpr* canonicalExpr = mCongruent->insertExpr(e);

  if (canonicalExpr != e) {
    // We are asking for BDD of a&b|c, and we've already computed a BDD
    // for d&e|f.  If we didn't get anything out of a&b|c, then don't
    // bother trying for d&e|f.
    if (mExprBDDSimplifies->find(canonicalExpr) == mExprBDDSimplifies->end())
    {
      return NULL;
    }
  }
#endif

  mBDDContext->putMakeIdentsForInvalidBDDs(true);
  BDD bdd = mBDDContext->bdd(e); // punts on big expressions

  if (bdd.isValid()) {
    const NUExpr* bddExpr = mBDDContext->bddToExpr(bdd, e->getLoc());
    if ((bddExpr != NULL) && (bddExpr != e)) {
      // while mBDDContext->bdd punts on big expressions, an xor
      // tree will create a proportionally sized bdd graph, but
      // rendered as a tree may be very large.  Fortunately costing
      // is clever and will catch this for us before we explode it out
      // for folding
      NUCost origCost, bddGraphCost;
      mCostContext->calcExpr(e, &origCost);
      mCostContext->calcExpr(bddExpr, &bddGraphCost);

      // Allow the BDD graph to be 10x as large as the original
      // cost before exploding it.  We may get it back during tree folding.
      //
      // test/bugs/bug5905/bytemerge.v:6 BDD-reduces to this:
      //
      // word[0] | (word[1] | (word[2] | (word[3] | (word[4] |
      // (word[5] | (word[6] | (word[7] | (word[8] | (word[9] |
      // (word[10] | (word[11] | (word[12] | (word[13] | (word[14] |
      // (word[15] | (word[16] | (word[17] | (word[18] | (word[19] | 
      // (word[20] | (word[21] | (word[22] | (word[23] | (word[24] |
      // (word[25] | (word[26] | (word[27] | (word[28] | (word[29] | 
      // (word[30] | word[31])))))))))))))))))))))))))))))))
      //
      // This has mInstructionsRun=127, whereas the original line
      // has mInstructionsRun=16.  But if we allow Fold to run then
      // it reduces to word!=32'b0.  This is why I allow the bdd-cost
      // to be as much as 10x the original expr cost so that we can
      // allow Fold to run.  If it's unlimited, though, BDDs on
      // very unfriendly expressions like (a[31:0]==b[31:0]) will
      // blow up and tests will not complete.
      if ((bddGraphCost.mInstructionsRun <= 10*origCost.mInstructionsRun) &&
          (bddGraphCost.mAsicGates <= 10*origCost.mAsicGates))
      {
        //bool saveTraceFold = mTraceFold;
        //bool wasChanged = mChanged;
        //mTraceFold = false;
        mBDDRecursionBlocker = true;
        (void) foldExprTree(&bddExpr);
        mBDDRecursionBlocker = false;
        if (bddExpr != e) {
          //mChanged = wasChanged;
          //mTraceFold = saveTraceFold;
          NU_ASSERT(bddExpr, e);

          NUCost bddCost;
          mCostContext->calcExpr(bddExpr, &bddCost);

          // The gate-cost comparison here is not intuitive.  The testcase
          // test/assign/always-constant-big.v presents this code with a
          // choice of
          //
          //     orig:   condvar & complex_expr1 | ~condvar & complex_expr1
          // vs
          //     bdd:    a ? complex_expr1 : complex_expr2
          //
          // where the gate-cost for those is equal, bu tthe runInstruction
          // cost for the bdd version is much better than the original.
          // But we need to pick the original form because it seems to work
          // much better for the vector inferencing code.  Until we figure
          // this out we are artificially going to require an improvement
          // in gate-count AND run-instruction cost before allowing the bdd
          // expression to win.  Another way to handle this for that particular
          // test case is to make the loop unrolling code pass a flag down to
          // the Fold to disable BDD transformations.
          if ((bddCost.mInstructionsRun >= origCost.mInstructionsRun) ||
              (bddCost.mAsicGates >= origCost.mAsicGates))
          {
            // The BDD did not improve things.  Check Sum Of Products (NYI)
            //cleanupExpr (bddExpr);
          }
          else {
#if AVOID_FOLDS_THRU_CONGRUENCE
            mExprBDDSimplifies->insert(canonicalExpr);
#endif
            changed = true;
            repl = finish(e, copyExpr(bddExpr), true);
          }
        } // if
      } // if
    } // if
  } // if
  mBDDContext->putMakeIdentsForInvalidBDDs(false);

  return repl;
} // const NUExpr* FoldI::foldExprBdd

class VerifyExprsVisited : public NUDesignCallback {
public:
  VerifyExprsVisited(NUExprExprHashMap* exprMap)
    : mExprMap(exprMap)
  {
  }

  Status operator()(Phase, NUBase*) {return eNormal;}

  Status operator()(Phase phase, NUExpr* expr) {
    if (phase == ePre) {
      NU_ASSERT(mExprMap->find(expr) != mExprMap->end(), expr);
    }
    return eNormal;
  }
  
private:
  NUExprExprHashMap* mExprMap;
};
  
const NUExpr* FoldI::foldFactoryExpr(const NUExpr* expr) {
  if (mFoldExprDepth == 0) {
    clearBDDContext();
  }
  ++mFoldExprDepth;
  (void) foldExprTree(&expr);
  --mFoldExprDepth;
  return expr;
}

NUExpr* FoldI::foldExpr(NUExpr* expr) {
  const NUExpr* folded = manage(expr, true);

  // Be paranoid and clear the BDD context both before and
  // after diving into a top-level expresson.  Without this
  // clearing before diving in, we exceed the 65536 # BDD
  // entries in test/cust/emc/bookman/carbon/cwtb/BOOKMAN.
  if (mFoldExprDepth == 0) {
    clearBDDContext();
  }

  ++mFoldExprDepth;

  if (foldExprTree(&folded)) {
    // Make sure that the new expression is allocated before the old expression
    // is freed. Otherwise, the storage for the new expression *can* have the
    // same address as the old expression. This will cause problems in
    // sReplaceExprUnlessConstant in NUExpr.cxx. It determines that a
    // replacement has been made by comparing the address of the original with
    // the address of the replacement. If a replacement has been made, but the
    // replacement has the same address as the original, then
    // sReplaceExprUnlessConstant gets a little confused. This behavior was
    // observed for test/bugs/bug3522 when CARBON_MEM_DISABLE=1.
    NUExpr *tmp = expr;
    expr = folded->copy(mCC);
    cleanupExpr(tmp);
  }

  --mFoldExprDepth;

  if (mFoldExprDepth == 0) {

    // the following "if 0" block is related to bug9062, see that bug for details
    
#if 0    
    {
      // this code should be enabled once enough nucleus fold methods
      // have been implemented.  Currently it is known that
      // FoldI::compositeSelExpr is missing.
 
      // Ensure that every sub-expression has been visited, because the 
      // Fold code is recursing through the expression without using
      // replaceLeaves or NUDesignWalker.
      VerifyExprsVisited verify(mExprMap);
      NUDesignWalker walker(verify, false);
      walker.expr(const_cast<NUExpr*>(folded));
    }
#endif 
    
    // Clear the BDD context when we pop out of expression-recursion,
    // so that we don't run out of indices.
    clearBDDContext();
  }

  return expr;
} // NUExpr* FoldI::foldExpr

NUExpr* FoldI::foldResizeExpr(NUExpr* expr, UInt32 bitSize) {
  NUExpr* orig = expr;
  expr = foldExpr(expr);
  UInt32 rhs_size = expr->getBitSize();

  if (bitSize < rhs_size && not expr->isReal ()) {
    // The right-hand side is too big.  Shrink it down. 
    // Note that if this assignment has already been folded, the
    // part select may have been converted to a mask, in which
    // case its determineBitSize will still be large.  Don't
    // re-mask it.
    ConstantRange range(bitSize - 1, 0);
    NUExpr* rv = createPartsel(expr, range, expr->getLoc());
    cleanupExpr(expr);

    // This may have been a signed expression; if we partselect to
    // reduce it to fewer bits, we don't need any sign-extension
    // indicator, and in fact for BitVectors, that may cause overload
    // mismatches.
    rv->setSignedResult (false);

    // Re-fold this.  This should really be done in createPartsel,
    // but the last masking only happens in fold(NUVarselRvalue*).
    rv = foldExpr(rv);
    NU_ASSERT(!rv->isSignedResult(), rv);

    if (rv->determineBitSize() > bitSize) {
      // Still more bits than we want
      const NUExpr* bits = varsel(rv, ConstantRange (bitSize-1, 0), rv->getLoc(),
                                  bitSize);
      rv = bits->copy(mCC);
    }
    expr = rv;
  } // if

  if (orig == expr) {
    expr = NULL;
  }
  return expr;
} // NUExpr* FoldI::foldResizeExpr

const NUExpr* FoldI::foldConst(const NUConst* c)
{
  if (c->isSignedResult () || c->hasXZ() || c->isDefinedByString())
    return NULL;		// Not interested in these yet.

  // Reduce constants to their natural size.
  // We cannot reduce to the smallest number of significant bits that can represent the value,
  // because this would change the width of concats containing constants, etc.
  DynBitVector val;
  c->getSignedValue (&val);

  DynBitVector truncval (val);
  truncval.resize (c->determineBitSize ());
  if (truncval != val)
  {
    const NUExpr* result = constant(false, truncval, truncval.size (), c->getLoc ());
    return finish (c, result);
  }

  return NULL;
}

const NUExpr* FoldI::qcFindBit(const NUTernaryOp* qc) {
  // There are 4 interesting forms
  // FFO => x[0] ? 0 : X[1] ? 1 : X[2] ? 2 : -1
  // FLO => x[2] ? 2 : X[1] ? 1 : X[0] ? 0 : -1
  // FFZ => x[0] ? X[1] ? X[2] ? -1 : 2 : 1 : 0
  // FLZ => x[2] ? X[1] ? X[0] ? -1 : 0 : 1 : 2

  if (qc->getBitSize () > 32)
    return NULL;

  const NUVarselRvalue* cond = dynamic_cast<const NUVarselRvalue*>(qc->getArg (0));
  if (! cond)
    return NULL;

  const NUExpr* tb = qc->getArg (1);
  const NUExpr* fb = qc->getArg (2);
  int findFirst = 0;            // Doing FF(1) vs FL(-1) vs don't know(0)
  UInt32 width = 1;             // Width of FF* or FL* (starts with one bit)
  const NUConst* tconst = ctce (tb);
  const NUConst* fconst = ctce (fb);
  SInt32 lastVal,firstVal;
  NUOp::OpT opcode=NUOp::eStart;
  const NUExpr* other = NULL;         // The not-found value...

  if (tconst && fb->getType () == NUExpr::eNUTernaryOp) {
    // Potential FIND ONES
    tconst->getL (&lastVal);
    firstVal = lastVal;
    width += walkQCTree (cond, fb, true, lastVal, &findFirst, &other);
    if (findFirst > 0) {
      opcode = NUOp::eUnFFO;
    } else {
      opcode = NUOp::eUnFLO;
      lastVal -= (width-1);         // delta factor
    }
  } else if (fconst && tb->getType () == NUExpr::eNUTernaryOp) {
    // Potential FIND ZEROES
    fconst->getL (&lastVal);
    firstVal = lastVal;
    width += walkQCTree (cond, tb, false, lastVal, &findFirst, &other);
    if (findFirst > 0) {
      opcode = NUOp::eUnFFZ;
    } else {
      opcode = NUOp::eUnFLZ;
      lastVal -= (width-1);
    }
  } else
    return 0;

  if (other == 0)
    return 0;

  if (const NUConst* otherConst = other->castConst ()) {
    // The value when no bit is found in the correct state needs to be all ones to the
    // precision of the value.  The find functions return a 32-bit -1, so if we store 16 bits
    // then that works fine.
    DynBitVector val;
    otherConst->getSignedValue (&val);
    // note second arg to DynBitVector::partsel is the size not the MSB
    if (not (val.partsel(0, (qc->getBitSize())).all ()))
      return 0;
  }
    
  // reject sequences shorter than 4 bits, the overhead exceeds any possible value.
  // Also quit if we haven't figured out if this is a FF* or FL*
  if (width < 4 || findFirst == 0)
    return 0;               // no luck

  // Lastly check to see if there's a delta-factor in the returned value..
  
  SInt32 delta = 0;
  if (findFirst > 0)            // starting at bit zero... for FF*
    delta = firstVal;
  else if (findFirst < 0)       // starting at bit width-1 for FL*
    delta  = firstVal + 1 - width;

  // We can't handle deltas yet because the -1 returned on not found 
  // We should change ?: forms to something that looks like:
  // 
  //          case FFO(expr)
  //          0: temp = delta;
  //          1: temp = delta+1;
  //          2: temp = delta+2;
  //    ...
  //          default:
  //             temp = other
  //    ... // in ?: expression, substitute 'temp' for the whole ?:
  // but we can't insert a statement in front of an expression unless we make
  // FoldI retain the current block and statement being iterated thru and only
  // allow this fold when those data values are valid.
  if (delta)
    return 0;


  ConstantRange r = *(cond->getRange ());

  if (findFirst > 0) {
    r.setMsb (r.getLsb () + width - 1);
  } else if (findFirst < 0) {
    r.setLsb (r.getMsb () +1 - width);
  }

  const NUExpr* v = varsel(cond->getIdentExpr (), cond->getIndex (),
                             r, cond->getLoc());
  const NUExpr* result = unaryOp(opcode, v, qc->getLoc ());

  if (delta) {
    // Coverity says this line of code is dead, which it is.  But
    // I think Al intended the "if (delta) return 0;" code above
    // as a temporary bug avoidance tactic, and this code might
    // become live again later on if Al works on this further.
    result = binaryOp(NUOp::eBiPlus, result,
                      constant(true, delta, 32, qc->getLoc ()),
                      qc->getLoc ());
  }

  UInt32 findWidth = result->determineBitSize ();
  UInt32 qcWidth = qc->getBitSize ();
  if (findWidth > qcWidth) {
    result = varsel(result, ConstantRange(qcWidth-1, 0), result->getLoc());
  }

  result = mExprCache->makeSizeExplicit(result, qc->getBitSize());

  return finish (qc, result);
} // const NUExpr* FoldI::qcFindBit


const NUExpr* FoldI::maybeShrinkOperands(const NUExpr *e1, const NUExpr *e2, const NUOp::OpT op, UInt32 bop_size, const SourceLocator& loc )
{
  const NUConst *lp = ctce (e1);
  const NUConst *rp = ctce (e2);

  // If rp is bigger than it needs to be, make it smaller.  E.g.
  // If the user writes (a[0]==0) that populates as (a[0]==32'b0)
  // but 'rp' could be resized down
  UInt32 e1Size = e1->determineBitSize();
  UInt32 e1EffectiveSize = getCachedEffectiveBitSize(e1);
  if ( e1EffectiveSize >= bop_size ){
    e1Size = e1EffectiveSize;
  }
  if ((lp == NULL) &&
      (rp != NULL) &&
      ((op == NUOp::eBiEq) || (op == NUOp::eBiNeq) || (op == NUOp::eBiTrieq) || (op == NUOp::eBiTrineq) ||
       ((op == NUOp::eBiUGtr) || (op == NUOp::eBiULt))) &&
      (getCachedEffectiveBitSize(rp) <= e1Size) &&
      (rp->getBitSize() > e1Size) &&
      (e1Size >= getCachedEffectiveBitSize(e1)) && // see test/fold/compare_add.v 
      (!rp->hasXZ()))
  {
    DynBitVector val;
    rp->getSignedValue(&val);
    bool e1Signed = e1->isSignedResult();
    e1Signed = false;         // shrinking the constant makes it unsigned
    val.resize(e1Size);
    const NUExpr* k = constant(e1Signed, val, e1Size, loc);
    NUExpr* e1copy = e1->copy(mCC);
    e1copy = e1copy->makeSizeExplicit(e1Size);
    e1copy->setSignedResult(false); // shrinking the expression // makes it unsigned
    e1 = manage(e1copy, false);
    return  binaryOp(op, e1, k, loc);
  }
  return NULL;
}

UInt32 FoldI::getCachedEffectiveBitSize(const NUExpr* expr) {
  return expr->effectiveBitSize(mEffectiveBitSizeCache); // effective_bitsize_ok
}
