// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "LocalPropagation.h"
#include "reduce/LocalConstProp.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"

#include "localflow/UD.h"

#include "reduce/Fold.h"

class LocalConstantPropagationCallback : public LocalPropagationCallback
{
public:
  //! Constructor.
  LocalConstantPropagationCallback(IODBNucleus* iodb, Fold* fold, 
                                   LocalPropagationStatistics * statistics) :
    LocalPropagationCallback(iodb,fold,statistics,false)
  {}

  //! Destructor.
  ~LocalConstantPropagationCallback() {}

protected:

  //! Analyze an assignment and conditionally add a propagation.
  void enrollAssign(NUAssign * assign);

  //! Analyze an if statement and conditionally add a propagation.
  /*!
    If we have a simple condition, try and infer the assumed value for
    either the then or the else branch.
    
    selector - If true, enroll inferred constants for the then branch.
               If false, enroll inferred constants for the else branch.
   */
  void enterIfBranch(NUIf * ifStmt, bool selector);

private:
  NUExpr * checkNetValue(NUNet * net, NUExpr * expr);

  //! Function to get/optimize an assigns rhs into a constant
  /*! Some expressions might simplify to a constant such as
   *  ~(1'b1). By running fold on the assign expression, we can
   *  actually propagate more constants.
   */
  NUConst* getConstantExpr(NUAssign* assign);

  //! Function to add constant nets based on knowing the value of the expr
  /*! This function can be used for if statements. The nets in the
   *  condition expression can sometimes be determined to be a
   *  constant. Today this just handles limited if conditions and no
   *  case conditions. This can be expanded in the future.
   */
  void addConstExprNets(NUExpr* expr, bool invert);

  //! Abstraction to figure out the net value that makes an expression true
  typedef std::pair<NUNet*, bool> NetValue;

  //! Helper function to find a net value that will make an expression true
  NetValue findNetTrueValue(NUExpr* expr);

};


RELocalConstProp::RELocalConstProp(IODBNucleus* iodbNucleus, 
                                   UD* ud,
                                   Fold* fold, 
                                   LocalPropagationStatistics * statistics,
                                   bool verbose,
                                   bool use_flow_order) :
  mVerbose(verbose),
  mUseFlowOrder(use_flow_order),
  mIODBNucleus(iodbNucleus), 
  mUD(ud), 
  mFold(fold),
  mStatistics(statistics)
{}

RELocalConstProp::~RELocalConstProp() {}

void RELocalConstProp::module(NUModule* module)
{
  // Create a design walker to walk the always blocks and optimize
  // them.
  LocalConstantPropagationCallback callback(mIODBNucleus, mFold, mStatistics);
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);

  bool module_changed = false;

  // Walk the tasks and process them with an empty context. No
  // constants can flow into tasks.
  for (NUTaskLoop l = module->loopTasks(); !l.atEnd(); ++l) {
    NUTask* task = *l;
    walker.task(task);
    if (callback.getChanged()) {
      module_changed = true;
    }
  }

  // Add all constant assigns to the context
  callback.pushContext();       // A new context for the module.

  NUAlwaysBlockVector optimizedBlocks;

  if (mUseFlowOrder) {
    // Get an ordering of UseDef nodes which allows cascading propagation.
    NUUseDefVector depth_order;
    LocalPropagationBuilder::getDepthOrder(module,&depth_order);

    for (NUUseDefVector::iterator i=depth_order.begin(); i!=depth_order.end(); ++i) {
      NUUseDefNode * node = *i;
      switch(node->getType()) {
      case eNUContAssign: {
        NUContAssign * assign = dynamic_cast<NUContAssign*>(node);
        walker.contAssign(assign);
        if (callback.getChanged()) {
          module_changed = true;
        }
        break;
      }
      case eNUAlwaysBlock: {
        NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(node);
        walker.alwaysBlock(always);

        // If it was changed, save the always block for folding
        if (callback.getChanged()) {
          module_changed = true;
          optimizedBlocks.push_back(always);
        }
        break;
      }
      default:
        // only handle continuous assigns and always blocks.
        break;
      }
    }
  } // if
  else {
    for (NUModule::ContAssignLoop l = module->loopContAssigns(); !l.atEnd(); ++l) {
      NUContAssign* assign = *l;
      walker.contAssign(assign);
      if (callback.getChanged()) {
        module_changed = true;
      }
    }

    // Walk the always blocks and blocks propagating constants.
    for (NUModule::AlwaysBlockLoop l = module->loopAlwaysBlocks();
         !l.atEnd(); ++l) {
      // Propagate the constants in the always block. We pre clear the
      // changed flag from the previous always block.
      NUAlwaysBlock* always = *l;
      walker.alwaysBlock(always);

      // If it was changed, save the always block for folding
      if (callback.getChanged()) {
        module_changed = true;
        optimizedBlocks.push_back(always);
      }
    }
  } // else

  // All done with the module context
  callback.popContext();

  if (module_changed) {
    mStatistics->module();
  }

  // Sort the modified blocks so we get consistent folding and fold
  // them
  std::sort(optimizedBlocks.begin(), optimizedBlocks.end(), NUUseDefNodeCmp());
  typedef Loop<NUAlwaysBlockVector> AlwaysLoop;
  for (AlwaysLoop l(optimizedBlocks); !l.atEnd(); ++l) {
    NUAlwaysBlock* always = *l;
    mFold->structuredProc(always);
  }

  // If a UD pointer was provided, then recalculate it now
  if (mUD != NULL) {
    mUD->module(module);
  }

  // If we are being verbose, print the optimized blocks
  if (mVerbose && !optimizedBlocks.empty()) {
    // Sort the blocks so we get canonical output

    // Print the verilog for all optimized blocks
    UtIO::cout() << "\nOptimized blocks for module "
                 << module->getName()->str() << ":\n\n";
    for (AlwaysLoop l(optimizedBlocks); !l.atEnd(); ++l) {
      NUUseDefNode* useDef = *l;
      useDef->printVerilog(true, 0, true);
    }
  }
} // void RELocalConstProp::module

void RELocalConstProp::alwaysBlock(NUAlwaysBlock* always)
{
  // Create a design walker to walk the always blocks and optimize
  // them.
  LocalConstantPropagationCallback callback(mIODBNucleus, mFold, mStatistics);
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);

  // There is no inheirited context; only propagate internal to the block.
  callback.pushContext();       // A new context for the always block.
  walker.alwaysBlock(always);
  callback.popContext();        // Remove the context.
  bool changed = callback.getChanged();

  // Constant assigns have been propagated, fold the block again
  mFold->structuredProc(always);

  // If a UD pointer was provided, then recalculate it now
  if (mUD != NULL) {
    mUD->structuredProc(always);
  }

  if (mVerbose and changed) {
    // Print the verilog for all optimized blocks
    UtIO::cout() << "\nOptimized block in module "
                 << always->getBlock()->getModule()->getName()->str() << ":\n\n";
    always->printVerilog(true, 0, true);
  }
}
  
void LocalConstantPropagationCallback::enrollAssign(NUAssign * assign)
{
  // Add this assign to our constant map whether it is a constant or not
  NULvalue* lvalue = assign->getLvalue();
  if (lvalue->isWholeIdentifier()) {
    // We can handle this here.
    NUNet* net = lvalue->getWholeIdentifier();
    // Avoid propagating constants from pull assigns if there are
    // other drivers.  There are two scenarios. If this is a strong
    // driver then we don't care if it is multiply driven because we
    // prefer to optimize a strong constant driver over anything. The
    // second case is it is a pull driver. In that case it can't be
    // multiply driven because a pull constant driver cannot override
    // a strong driver.
    if ((assign->getStrength() >= eStrDrive) or (not net->isMultiplyDriven())) {
      NUConst* constExpr = getConstantExpr(assign);
      NUExpr * netValue = checkNetValue(net,constExpr);
      addNet(net, netValue);
      if (netValue) {
        delete netValue;
      }
    }
  }
}


NUConst* LocalConstantPropagationCallback::getConstantExpr(NUAssign* assign)
{
  // See if the rhs is a constant as. If so, return it
  NUExpr* expr = assign->getRvalue();
  NUConst* constExpr = expr->castConst();
  if (constExpr) {
    return constExpr;
  } 

  // Check if we can fold this to a constant. We need to make a copy
  // of the expression because we don't want to simplify expressions,
  // just propagate constants. When this code simplified expressions
  // it would cause downstream optimizations to fail (eg case inference)
  CopyContext cc(0, 0);
  NUExpr* copyExpr = expr->copy(cc);
  NUExpr* newExpr = getFold()->fold(copyExpr);
  constExpr = newExpr->castConst();

  // If we folded it into a constant we can also fixup the assign
  // otherwise we give up and return NULL.
  if (constExpr == NULL) {
    // No success, give up on this being a constant
    delete newExpr;

  } else {
    // it is a constant, optimize the assign now too
    assign->replaceRvalue(newExpr);
    delete expr;
  }

  return constExpr;
} // NUConst* LocalConstantPropagationCallback::getConstantExpr


NUExpr * LocalConstantPropagationCallback::checkNetValue(NUNet * net, NUExpr * expr)
{
  NUConst * constExpr = NULL;
  if (expr) {
    constExpr = expr->castConst();
  }

  // make sure this is a valid constant. This seems to be the best
  // choke point to figure this out because it is called block from
  // this callback and the RELocalConstProp::module() function.
  if (net->isProtectedMutable(getIODB()) || net->is2DAnything()) {
    // We can't optimize protected mutable points. Also, if the entire
    // memory is a constant, we could do more, but rewriting the code
    // is messsy so we are giving up for now.
    constExpr = NULL;
  }

  // We are currently not sure of the semantics of propagating x/z. So
  // if the constant has x/z, lets not propagate it for now.
  if ((constExpr != NULL) && constExpr->hasXZ()) {
    // If it's driven by Z and also by something else, we should
    // not do net-expr replacements with Z.  Probably the Z should
    // be ignored but I think that's an enhancement to BreakNetConsts.
    if (net->isMultiplyDriven()) {
      constExpr = NULL;
    }
  }

  // Make a copy of the constant expression for our map. We don't want
  // to count on the memory staying allocated through
  // optimizations. Since we are making a copy, we can also fixup the
  // size and sign to match the nets sign and size.
  if (constExpr != NULL) {
    // Check for VHDL integer subranges
    if (net->isIntegerSubrange ()) {
      constExpr = constExpr->rebuild(net->isSignedSubrange (), 32);
    } else {
      constExpr = constExpr->rebuild(net->isSigned (), net->getBitSize ());
    }
  }
  return constExpr;
}


void LocalConstantPropagationCallback::addConstExprNets(NUExpr* expr, bool invert)
{
  // Make a copy of the expression and fold it. This gets rid of any
  // (clk != 0) type expressions and simplifies them to just clk. Not
  // sure why we do that.
  CopyContext cc(0, 0);
  NUExpr* condExpr = expr->copy(cc);
  NUExpr* newExpr = getFold()->fold(condExpr);
  if (newExpr == NULL) {
    newExpr = condExpr;
  }

  // Recursively walk the expression trying to find a possible
  // constant value
  NetValue netValue = findNetTrueValue(newExpr);
  NUNet* net = netValue.first;
  bool isOne;
  if (invert) {
    isOne = !netValue.second;
  } else {
    isOne = netValue.second;
  }

  // If we have a valid net, mark it as a constant based on isOne.
  if (net != NULL) {
    NUConst* constVal = NULL;
    if (isOne) {
      constVal = NUConst::create(net->isSigned(), 1, 1, expr->getLoc());
    } else {
      constVal = NUConst::create(net->isSigned(), 0, 1, expr->getLoc());
    }
    addNet(net, constVal);
    delete constVal;
  }

  // All done with the copied or folded expression
  delete newExpr;
} // void LocalConstantPropagationCallback::addConstExprNets

LocalConstantPropagationCallback::NetValue LocalConstantPropagationCallback::findNetTrueValue(NUExpr* expr)
{
  // Today we only support an ident rvalue or inversion of a scalar
  // net. In the future we could support more conditions. For example
  // "if (a & b)" implies that a and b are 1 in the then clause.
  //
  // We also don't suport constant net refs yet
  NUNet* net = NULL;
  bool isOne = false;
  switch (expr->getType()) {
    case NUExpr::eNUIdentRvalue:
    {
      // If we have a scalar, then it is true when the net is one.
      net = expr->getWholeIdentifier();
      if (!net->isBitNet()) {
        net = NULL;
      }
      isOne = true;
      break;
    }

    case NUExpr::eNUUnaryOp:
    {
      // test for logical not or bit neg. If so, we can test the sub
      // expression and invert it.
      NUUnaryOp* unary = dynamic_cast<NUUnaryOp*>(expr);
      NU_ASSERT(unary != NULL, expr);
      if ((unary->getOp() == NUOp::eUnLogNot) || 
          (unary->getOp() == NUOp::eUnBitNeg)) {
        // Make sure we are inverting an ident rvalue
        NUExpr* subExpr = unary->getArg(0);
        NetValue netValue = findNetTrueValue(subExpr);
        net = netValue.first;
        isOne = not netValue.second;
      }
      break;
    }

    case NUExpr::eNUBinaryOp:
    {
      // Test for one-bit comparisons like:
      //    x==1'b1, y!=1'b0, z===1'b1, w!==1'b0
      NUBinaryOp * binary = dynamic_cast<NUBinaryOp*>(expr);
      if (binary->getOp() == NUOp::eBiEq  or 
          binary->getOp() == NUOp::eBiNeq or 
          binary->getOp() == NUOp::eBiTrieq or
          binary->getOp() == NUOp::eBiTrineq) {
        NUExpr * arg0 = binary->getArg(0);
        NUExpr * arg1 = binary->getArg(1);
        if (arg0->getBitSize()==1 and arg1->getBitSize()==1) {
          NUConst * arg1const = arg1->castConstNoXZ();
          if (arg1const) {
            NetValue arg0value = findNetTrueValue(arg0);
            net = arg0value.first;
            // If the constant argument is zero, we need to flip the
            // assumed value of our identifier.
            if (arg1const->isZero()) {
              isOne = not arg0value.second;
            } else {
              isOne = arg0value.second;
            }
            // If the operator is a negative comparator, we again need
            // to flip the assumed value of our identifier.
            if (binary->getOp() == NUOp::eBiNeq or
                binary->getOp() == NUOp::eBiTrineq) {
              isOne = not isOne;
            }
          }
        }
      }
      break;
    }

    default:
      break;
  }

  // Return the net and whether it is one or not
  return NetValue(net, isOne);
} // LocalConstantPropagationCallback::findNetTrueValue


void LocalConstantPropagationCallback::enterIfBranch(NUIf * ifStmt, bool selector)
{
  addConstExprNets(ifStmt->getCond(), (not selector) /* invert */);
}


