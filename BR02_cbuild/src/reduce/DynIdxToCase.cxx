// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/DynIdxToCase.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUNetRef.h"
#include "util/CarbonTypeUtil.h"
#include "reduce/RewriteUtil.h"
#include "localflow/UD.h"
#include "reduce/Fold.h"


DynIdxToCase::DynIdxToCase(NUNetRefFactory *factory,
                           MsgContext *msg_context,
                           IODBNucleus *iodb,
                           ArgProc *args,
                           AtomicCache *str_cache,
                           UInt32 size_limit,
                           bool verbose) :
  mNetRefFactory(factory),
  mMsgContext(msg_context),
  mIODB(iodb),
  mArgs(args),
  mFold(new Fold(factory, args, msg_context, str_cache, iodb, false, 0)),
  mSizeLimit(size_limit),
  mVerbose(verbose)
{
}


DynIdxToCase::~DynIdxToCase()
{
  delete mFold;
}


//! DynIdxRewriterCallback class
/*!
 * Callback to perform the dynamic index to case conversion inside a module.
 *
 * Only one module is traversed.
 *
 * Dynamic indices on the RHS of continuous assigns are not rewritten, only
 * assigns within procedural blocks are rewritten.
 */
class DynIdxRewriterCallback : public NUDesignCallback
{
public:
  //! constructor
  DynIdxRewriterCallback(UInt32 size_limit,
                         NUNetRefFactory *factory,
                         ArgProc *args,
                         MsgContext *msg_context,
                         IODBNucleus *iodb,
                         Fold *fold,
                         bool verbose,
                         bool *changed) :
    mSizeLimit(size_limit),
    mNetRefFactory(factory),
    mArgs(args),
    mMsgContext(msg_context),
    mIODB(iodb),
    mFold(fold),
    mVerbose(verbose),
    mChanged(changed)
  {}

  //! destructor
  ~DynIdxRewriterCallback() {}

  //! Visit all nucleus objects.
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Do not traverse into submodules, do this module only.
  Status operator()(Phase, NUModuleInstance*) { return eSkip; }

  //! Attempt to xform simple procedural assigns with dyn idx on rhs.
  /*!
   * Limited to the cases:
   *  . vector being indexed
   *  . dynamic index
   *  . index expression values are under a certain size threshold
   *
   * These limits are just out of convenience.  This is what was necessary
   * to get the Agere performance back.  It is possible that xforming in
   * more situations may be a win, also xforming LHS dyn indexes may be
   * a win, these ideas need to be validated.
   */
  Status operator()(Phase phase, NUBlockingAssign* assign)
  {
    // Only xform when the assign is first encountered.
    if (phase == ePost) {
      return eNormal;
    }

    // Only xform rhs dynamic indices.
    NUExpr *rhs = assign->getRvalue();
    if (rhs->getType() != NUExpr::eNUVarselRvalue) {
      return eNormal;
    }

    NUVarselRvalue *rvalue = dynamic_cast<NUVarselRvalue*>(rhs);
    NU_ASSERT(rvalue, assign);

    // Do not xform complex varsel situations (varsel of varsel, etc).
    if (not rvalue->getIdentExpr()->isWholeIdentifier()) {
      return eNormal;
    }

    // Only xform dynamic vector indexing (ie memory indexing is not xformed).
    NUNet *ident = rvalue->getIdent();
    NUVectorNet *vect_ident = ident->castVectorNet();
    if (not vect_ident) {
      return eNormal;
    }

    // Do not xform constant indexing.
    if (rvalue->isConstIndex()) {
      return eNormal;
    }

    // Isolate 1 ident from index expression.
    // If more than 1 ident, do not xform.
    NUNetSet uses;
    NUExpr *idx = rvalue->getIndex();
    idx->getUses(&uses);
    if (uses.size() != 1) {
      return eNormal;
    }
    NUNet *idx_var = *(uses.begin());

    // Get bit size from index ident size, and make sure it is under the
    // size limit.
    UInt32 bit_size = idx_var->getBitSize();
    if (bit_size > mSizeLimit) {
      return eNormal;
    }

    // Do not xform memory nets which are used as index variables.
    // The rewriter code doesn't handle this correctly; it assumes the
    // whole memory can be replaced by a constant.
    if (idx_var->isMemoryNet()) {
      return eNormal;
    }

    // Do not xform signed index variables.
    // Verilog signed nets have isSigned() true.
    // For VHDL, allow unsigned integer subranges, even though isSigned()
    // is true, it can never hold a negative value.
    if (idx_var->isSigned() and not idx_var->isUnsignedSubrange()) {
      return eNormal;
    }

    const SourceLocator &loc = assign->getLoc();
    bool uses_cfnet = assign->getUsesCFNet();
    CopyContext copy_ctx(0, 0);

    // Create case statement object and set full case and fully specified flags,
    // since we will enumerate all possible values of the index variable.
    NUExpr *selector = new NUIdentRvalue(idx_var, loc);

    // The NUIdentRvalue constructor sets isSignedResult() when idx_var is a
    // VHDL unsigned integer subrange, but we are using it in an unsigned context.
    selector->setSignedResult(false);

    NUCase *case_stmt = new NUCase(selector, mNetRefFactory, NUCase::eCtypeCase, uses_cfnet, loc);
    case_stmt->putUserFullCase(true);
    case_stmt->putFullySpecified(true);

    // Enumerate all possible values of the index variable, create case
    // condition and item for each value.
    for (UInt32 idx_value = 0; idx_value < (UInt32(1) << bit_size); idx_value++) {

      NUConst *cur_value = NUConst::createKnownIntConst(idx_value, bit_size, false, loc);

      // Copy the assign to put into the case item.
      NUStmt *new_assign = assign->copy(copy_ctx);

      // Replace index variable with the current enumerated value in the copied assign.
      {
        NUNetReplacementMap dummy1;
        NULvalueReplacementMap dummy2;
        NUExprReplacementMap replacement;
        NUTFReplacementMap dummy3;
        NUCModelReplacementMap dummy4;
        NUAlwaysBlockReplacementMap dummy5;

        replacement[idx_var] = cur_value;
        REReplaceRvalues rewriter(dummy1, dummy2, replacement, dummy3, dummy4, dummy5);
        new_assign->replaceLeaves(rewriter);
      }

      // Simplify the assign if possible.
      new_assign = mFold->stmt(new_assign);

      // Populate case condition and item and place into the case statement.
      NUCaseCondition *cond = new NUCaseCondition(loc, cur_value);
      NUCaseItem *item = new NUCaseItem(loc);
      item->addCondition(cond);
      item->addStmt(new_assign);
      case_stmt->addItem(item);
    }

    // Remember the case statement for when the design walker calls us back.
    mReplacementMap[assign] = case_stmt;

    *mChanged = true;

    if (mVerbose) {
      UtIO::cout() << "Converted to Case Statement:  ";
      assign->printVerilog();
      case_stmt->printVerilog();
    }

    return eDelete;
  }

  /*!
   * If the given statement was an assign that is being rewritten to a case
   * statement, return the case statement.  Otherwise return 0.
   */
  NUStmt *replacement(NUStmt *stmt)
  {
    NUStmtStmtMap::iterator iter = mReplacementMap.find(stmt);
    if (iter != mReplacementMap.end()) {
      return iter->second;
    } else {
      return 0;
    }
  }

private:
  //! Bit size limit of the indexing variable.  0 disables any xforms.
  UInt32 mSizeLimit;

  //! Net ref factory
  NUNetRefFactory *mNetRefFactory;

  //! Arg processor
  ArgProc *mArgs;

  //! Message context
  MsgContext *mMsgContext;

  //! IO DB
  IODBNucleus *mIODB;

  //! Fold object used to simplify generated expressions.
  Fold *mFold;

  //! If true, dump out xforms as they occur.
  bool mVerbose;

  //! Set to true if any statements were xformed, so that we can recompute UD.
  bool *mChanged;

  /*!
   * Map of xformed assigns to case statements.  This is needed due to the
   * way the design walker handles statement deletion through callbacks.
   */
  NUStmtStmtMap mReplacementMap;
};


void DynIdxToCase::module(NUModule *module)
{
  bool changed = false;

  // Perform rewrite on this module.
  DynIdxRewriterCallback callback(mSizeLimit, mNetRefFactory, mArgs, mMsgContext, mIODB, mFold, mVerbose, &changed);
  NUDesignWalker walker(callback, false);
  walker.module(module);

  // Recompute UD if any statements were modified.
  if (changed) {
    UD ud(mNetRefFactory, mMsgContext, mIODB, mArgs, false);
    ud.module(module);
  }
}
