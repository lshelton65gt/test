// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUAssign.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUElabHierHelper.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLFactory.h"
#include "flow/FLIter.h"

#include "symtab/STSymbolTable.h"

#include "util/UtIOStream.h"
#include "util/Stats.h"
#include "util/CbuildMsgContext.h"
#include "util/ArgProc.h"

#include "iodb/IODBNucleus.h"

#include "localflow/MarkReadWriteNets.h"
#include "localflow/UnelabFlow.h"
#include "localflow/Elaborate.h"

#include "reduce/ReachableAliases.h"
#include "reduce/RemoveElabFlow.h"
#include "reduce/RemoveUnelabFlow.h"
#include "reduce/RemoveDeadCode.h"
#include "reduce/CleanElabFlow.h"
#include "reduce/SanityUnelab.h"
#include "reduce/SanityElab.h"
#include "reduce/DeadNets.h"
#include "reduce/RETriInit.h"


void RETriInit::walkDesign()
{
  for (FLDesignElabIter iter(mDesign,
			     mSymtab,
			     FLIterFlags::eClockMasters,
			     FLIterFlags::eAll,  // Visit everything
			     FLIterFlags::eNone, // Stop at nothing
			     FLIterFlags::eBranchAtAll,
			     FLIterFlags::eIterFaninPairOnce);
       not iter.atEnd();
       iter.next()) {
    // We don't care about nesting fanout since it is not a real
    // fanout. Otherwise remember the fanout.
    if (iter.isNestedRelationship()) {
      continue;
    }

    FLNodeElab *driver = *iter;
    NUNetElab* driver_net = driver->getDefNet();
    if (driver_net==NULL) {
      continue;
    }

    if (mNonTriInit.find(driver_net)!=mNonTriInit.end()) {
      continue;
    }

    bool requires_tri_init = mReachableAliases->requiresTriInit(driver_net);
    if (requires_tri_init) {
      FLNodeElab *fanout = iter.getCurParent();
      Sensitivity sense = iter.getSensitivity();
      rememberFanout(driver_net, fanout, sense);
    } else {
      mNonTriInit.insert(driver_net);
    }
  }
} // void RETriInit::walkDesign


void RETriInit::markNonTriInit()
{
  for (NUNetElabSet::iterator iter = mNonTriInit.begin();
       iter != mNonTriInit.end();
       ++iter) {
    NUNetElab * net_elab = (*iter);
    // Mark all non-tristate nets as a tri-init conflict. As a result,
    // tristate initializers on shared aliases must be coded as
    // hierarchical references.
    updateAliasesWithValue(net_elab, eTriInitConflict);
  }
}


void RETriInit::checkNonTriInit()
{
  // Check that there are no unelaborated initial drivers for any
  // elaborated nets that should not have an initial driver
  for (NUNetElabSet::iterator iter = mNonTriInit.begin();
       iter != mNonTriInit.end();
       ++iter) {
    NUNetElab * net_elab = (*iter);

    for (STAliasedLeafNode::AliasLoop loop(net_elab->getSymNode());
         not loop.atEnd();
         ++loop) {
      STAliasedLeafNode * alias = (*loop);
      if (mReachableAliases->query(alias)) {
        NUNet * alias_net = NUNet::find(alias);
        if (alias_net==NULL) { continue; }

        // If we find an unelaborated initial driver assert
        InitDriverMap::iterator location = mInitDriverMap.find(alias_net);
        if (location != mInitDriverMap.end()) {
          // We found one, testing against NULL will always fail
          NUUseDefNode* init_driver = location->second;
          NU_ASSERT2(init_driver == NULL, net_elab, alias_net);
        }
      }
    }
  }
}


void RETriInit::checkInitialDriverConsistency()
{
  // Create a set of the intial drivers created, to find all
  // elaborated flow for it.
  NUUseDefSet initDrivers;
  for (LoopMap<InitDriverMap> l(mInitDriverMap); !l.atEnd(); ++l) {
    NUUseDefNode* useDef = l.getValue();
    initDrivers.insert(useDef);
  }

  // Walk the elab flow factory and create a map from elaborated net
  // to the init driver elaborated flows. We can then do a consistency
  // check one elaborated net at a time.
  typedef UtMap<NUNetElab*, FLNodeElabVector> ElabInitDrivers;
  ElabInitDrivers elabInitDrivers;
  FLNodeElab* flowElab;
  for (FLNodeElabFactory::FlowLoop l = mFlowElabFactory->loopFlows();
       l(&flowElab);) {
    NUUseDefNode* useDef = flowElab->getUseDefNode();
    if ((useDef != NULL) && initDrivers.count(useDef) > 0) {
      // Found one, add it to the map
      NUNetElab* netElab = flowElab->getDefNet();
      elabInitDrivers[netElab].push_back(flowElab);
    }
  }

  // Now walk the elaborated init drivers and check the consistency
  for (LoopMap<ElabInitDrivers> l(elabInitDrivers); !l.atEnd(); ++l) {
    const FLNodeElabVector& elabFlows = l.getValue();
    if (not checkSingleInitialDriverConsistency(elabFlows)) {
      NUNetElab* netElab = l.getKey();
      mMsgContext->REInconsistentInitialDrivers(netElab);
    }
  }
} // void RETriInit::checkInitialDriverConsistency
      

bool
RETriInit::checkSingleInitialDriverConsistency(const FLNodeElabVector& drivers)
{
  TriInitValue tri_init_value = eTriInitUnknown;

  for (CLoop<FLNodeElabVector> l(drivers); !l.atEnd(); ++l) {
    FLNodeElab * initial_driver = (*l);
    NUUseDefNode * initial_use_def = initial_driver->getUseDefNode();

    TriInitValue my_init_value = eTriInitUnknown;
    if (initial_use_def->getType() == eNUContAssign) {
      NUContAssign * initial_assign = dynamic_cast<NUContAssign*>(initial_use_def);

      NU_ASSERT(initial_assign->getStrength()==eStrPull, initial_assign);

      NUConst * initial_value = initial_assign->getRvalue()->castConst();
      NU_ASSERT(initial_value != NULL, initial_assign);

      NU_ASSERT(initial_value->isZero() or initial_value->isOnes(), initial_assign);
      my_init_value = (initial_value->isZero() ? eTriInitZero : eTriInitOne);
    } else {
      FLN_ASSERT(initial_use_def->getType() == eNUTriRegInit, initial_driver);
      my_init_value = eTriInitTrireg;
    }

    if (tri_init_value == eTriInitUnknown) {
      tri_init_value = my_init_value;
    } else if (tri_init_value != my_init_value) {
      return false;
    }
  }
  return true;
}


void RETriInit::dumpMultipleInitialDrivers(NUNetElab * net_elab, 
                                           FLNodeElabVector * initial_drivers)
{
  if (initial_drivers->size() > 1) {
    UtIO::cout() << "Multiple initial drivers for net:" << UtIO::endl;
    net_elab->print(false,2);
    for (FLNodeElabVector::iterator init_iter = initial_drivers->begin();
         init_iter != initial_drivers->end();
         ++init_iter) {
      FLNodeElab * initial_driver = (*init_iter);
      initial_driver->print(false,4);
    }
  }
}

NUUseDefNode* RETriInit::makeInitConstant(NUModule *module,
					  NUNet *net,
					  const UtString &val,
					  const SourceLocator &loc)
{
  NUConst *oldconstant = NUConst::createKnownConst(val,  net->getBitSize(), loc);
  NUConst *constant;
  if( net->isSigned() && not oldconstant->isSignedResult() ) {
    constant = oldconstant->rebuild(true, net->getBitSize());
    delete oldconstant;
    } else {
    constant = oldconstant;
  }
  StringAtom *block_name = module->newBlockName(mSymtab->getAtomicCache(), loc);
  NUContAssign *assign =
    new NUContAssign(block_name, new NUIdentLvalue(net, loc), constant, loc, eStrPull);
  module->addContAssign(assign);
  return assign;
}


NUUseDefNode* RETriInit::makeInit1(NUModule *module,
				   NUNet *net,
				   const SourceLocator &loc)
{
  UtString val(net->getBitSize(), '1');
  return makeInitConstant(module, net, val, loc);
}


NUUseDefNode* RETriInit::makeInit0(NUModule *module,
				   NUNet *net,
				   const SourceLocator &loc)
{
  UtString val(net->getBitSize(), '0');
  return makeInitConstant(module, net, val, loc);
}


NUUseDefNode* RETriInit::makeInitTrireg(NUModule *module,
					NUNet *net,
					const SourceLocator &loc)
{
  NUTriRegInit *init = new NUTriRegInit(module->newBlockName(mSymtab->getAtomicCache(), loc),
					new NUIdentLvalue(net, loc),
					loc);
  module->addTriRegInit(init);
  return init;
}


RETriInit::TriInitValue RETriInit::determineInitValue(NUNetElab * net_elab)
{
  if (net_elab->queryAliases(&NUNet::isTrireg)) {
    if (mInitMode == eTristateModeZ) {
      return eTriInitTrireg;
    }
  } else if (net_elab->queryAliases(&NUNet::isWiredAnd) or
    net_elab->queryAliases(&NUNet::isTri1) or
    net_elab->queryAliases(&NUNet::isPullUp)) {
    return eTriInitOne;
  } else if (net_elab->queryAliases(&NUNet::isWiredOr) or
    net_elab->queryAliases(&NUNet::isTri0) or
    net_elab->queryAliases(&NUNet::isPullDown)) {
    return eTriInitZero;
  } else if (net_elab->queryAliases(&NUNet::isReg) or
             net_elab->queryAliases(&NUNet::isWire) or
             net_elab->queryAliases(&NUNet::isTri) or
             net_elab->queryAliases(&NUNet::isInteger) or
             net_elab->queryAliases(&NUNet::isTime) or
             net_elab->queryAliases(&NUNet::isReal)) {
    switch (mInitMode) {
      case eTristateModeZ:
      case eTristateModeX:
      case eTristateMode0:
        return eTriInitZero;
        break;
      case eTristateMode1:
        return eTriInitOne;
        break;
      default:
        NU_ASSERT("Invalid init mode"==NULL,net_elab);
        break;
    }
    // For memories, we have already warned about being uninitialized,
    // leave it at that (bug 660).
  } else if (not net_elab->queryAliases(&NUNet::is2DAnything)) {
    mMsgContext->UnknownInitType(net_elab);
  }
  return eTriInitUnknown;
}


void RETriInit::makeInit(NUNet * net_for_init,
                         TriInitValue tri_init_value)
{
  // If we have already created an initial driver for this
  // unelaborated net, don't create another.
  if (mInitDriverMap.find(net_for_init) != mInitDriverMap.end()) {
    if (mVerbose) {
      UtIO::cout() << "Reused unelaborated initial driver on net:" << UtIO::endl;
      net_for_init->print(false,2);
    }
    return;
  }

  NUUseDefNode *ud_node = 0;
  const SourceLocator& loc = net_for_init->getLoc();
  NUModule *module = net_for_init->getScope()->getModule();

  switch (tri_init_value) {
  case eTriInitTrireg: ud_node = makeInitTrireg(module, net_for_init, loc); break;
  case eTriInitZero:   ud_node = makeInit0(module, net_for_init, loc);      break;
  case eTriInitOne:    ud_node = makeInit1(module, net_for_init, loc);      break;
  default: break;
  }

  if (ud_node == 0) {
    return;
  }

  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net_for_init);

  if (mVerbose) {
    UtIO::cout() << "Added unelaborated initial driver to net:" << UtIO::endl;
    net_for_init->print(false,2);
    ud_node->print(false,2);
  }

  mInitDriverMap[net_for_init] = ud_node;
}


static bool sIsWritten(NUNet * net, IODBNucleus * mIODB)
{
  if (net->isWritten() or
      net->isTriWritten() or
      net->isSupply0() or
      net->isSupply1() or
      net->isPulled() or
      net->isProtectedMutableNonHierref(mIODB) or
      net->isHierRefWritten()) {
    return true;
  } else if (net->isPrimaryInput() or net->isPrimaryBid()) {
    return true;
  }
  return false;
}

STAliasedLeafNode * RETriInit::determineInitLocation(NUNetElab * net_elab)
{
  if (net_elab->isPrimaryBid() or net_elab->isPrimaryInput()) {
    // If we have a primary input or inout, the highest writer is the
    // primary port. There is no need to analyze the alias ring. We
    // should encounter primary inputs needing initial drivers only
    // when port coercion is disabled.
    return net_elab->getSymNode();
  }

  STAliasedLeafNode * written_alias = NULL;
  STAliasedLeafNode * highest_alias = NULL;
  STAliasedLeafNode * output_alias  = NULL;

  SInt32 written_depth = UtSINT32_MAX;
  SInt32 highest_depth = UtSINT32_MAX;
  SInt32 output_depth  = -1;

  // Sort the members of the alias ring before we start to avoid
  // re-comparing the same names (highest_alias, written_alias, and
  // output_alias are all determined independently).
  STAliasedLeafNodeVector aliases;
  for (STAliasedLeafNode::AliasLoop loop(net_elab->getSymNode());
       not loop.atEnd();
       ++loop) {
    aliases.push_back(*loop);
  }
  std::sort(aliases.begin(),aliases.end(),HierNameCmp());

  // Walk the alias ring and determine the highest alias, the highest
  // written alias, and the lowest "output" alias. These three
  // candidates are then considered below as locations for the initial
  // driver.
  for (STAliasedLeafNodeVector::iterator iter = aliases.begin();
       iter != aliases.end();
       ++iter) {
    STAliasedLeafNode * alias = const_cast<STAliasedLeafNode*>(*iter);
    if (mReachableAliases->query(alias)) {
      NUNet * alias_net = NUNet::find(alias);
      if (alias_net) {
        SInt32 alias_depth = NUScope::determineModuleDepth(alias);
        // Track the alias at the highest point in the hierarchy. If
        // this is a pure input alias ring, we want to place the
        // initial driver at the root of the tree (causally earliest).
        if (alias_depth < highest_depth) {
          highest_alias = alias;
          highest_depth = alias_depth;
        }
        // Track the written alias at the highest point in the
        // hierarchy. Port coercion guarantees that the write will
        // reach all reads. We place it at the highest written alias
        // to limit the number of initial drivers in the elaborated
        // design.
        if (sIsWritten(alias_net, mIODB)) {
          if (alias_depth < written_depth) {
            written_alias = alias;
            written_depth = alias_depth;
          }
        }
        // Track the "output" alias at the lowest point in the
        // hierarchy. Port coercion saw a write here and guaranteed
        // that that write would reach all reads. If we no longer see
        // a write in this ring (and it's not pure input), this is a
        // good fallback which maintains an accurate flow graph.
        if (alias_net->isOutput() or alias_net->isBid()) {
          if (alias_depth > output_depth) {
            output_alias = alias;
            output_depth = alias_depth;
          }
        }
      }
    }
  }

  // When choosing a location for the initial driver, prefer first the
  // location of a writer. The highest writer is used in an attempt to
  // avoid placing initial drivers in highly instantiated submodules.
  //
  // Port coercion will have manipulated port directions so existing
  // writers can reach all readers. If there is no writer, place the
  // initial driver at the lowest port with output behavior.
  //
  // If there is no writer and no net has output behavior, choose the
  // highest point. Port coercion should have manipulated the port
  // directions of undriven ports into inputs -- this means that
  // placing the initial driver at the highest point allows it to
  // reach all readers.
  STAliasedLeafNode * alias_for_init = NULL;
  if (written_alias != NULL) {
    alias_for_init = written_alias;
  } else if (output_alias != NULL) {
    alias_for_init = output_alias;
  } else {
    alias_for_init = highest_alias;
  }

  NU_ASSERT(alias_for_init != NULL, net_elab);

  return alias_for_init;
}


/*!
 * Go through all nets which need initial drivers and make them, populate
 * the mInitDriverMap.
 */
void RETriInit::makeAllInit()
{
  // First collect all elaborated nets which need initial drivers into a
  // sorted set.  A sorted set is used so that the compiler-generated block
  // names are stable enough to be used in textual diffs of the compiler output.
  NUNetElabOrderedSet sorted_set;
  for (NetNodeElabSetMapIter fanout_iter = mFanouts.begin();
       fanout_iter != mFanouts.end();
       ++fanout_iter) {
    NUNetElab *net_elab = fanout_iter->first;
    sorted_set.insert(net_elab);
  }

  // Determine the initial values for all the elaborated nets. Mark
  // the reachable, unelaborated nets in its alias ring as getting
  // that initial value.

  // If an unelaborated net has already been assigned a different
  // initial value, mark that net as having an initial driver
  // conflict. We may alter the initial driver location if there is a
  // conflict.
  for (NUNetElabOrderedSet::iterator iter = sorted_set.begin();
       iter != sorted_set.end();
       ++iter) {
    NUNetElab *net_elab = *iter;
    TriInitValue tri_init_value = determineInitValue(net_elab);

    updateAliasesWithValue(net_elab, tri_init_value);
  }


  // Now iterate through the set, create unelaborated drivers.
  for (NUNetElabOrderedSet::iterator iter = sorted_set.begin();
       iter != sorted_set.end();
       ++iter) {
    NUNetElab *net_elab = *iter;

    // Determine an appropriate location for an initial driver.
    STAliasedLeafNode * alias_for_init = determineInitLocation(net_elab);
    NUNet * net_for_init = NUNet::find(alias_for_init);
    TriInitValue tri_init_elab_value = mInitialDriverElabValue[net_elab];
    TriInitValue tri_init_value = mInitialDriverValue[net_for_init];

    if (mVerbose) {
      UtString buf;
      net_elab->compose(&buf, NULL);
      UtIO::cout() << "Creating initial driver for elaborated net:" << UtIO::endl;
      UtIO::cout() << "  " << buf << UtIO::endl;
      UtIO::cout() << "Initialization location:" << UtIO::endl;
      UtIO::cout() << "  "; alias_for_init->print();
    }    

    if (tri_init_value == eTriInitConflict) {
      // The location for the initial driver is in multiple alias
      // rings requesting conflicting values. Our solution to this is
      // to create a hierarchical reference with the proper
      // initializer.

      // Create the hierarchical reference.
      NUNet * hier_ref_net_for_init = createHierRefForInit(net_elab, alias_for_init);

      if (mVerbose) {
        UtIO::cout() << "Using hierarchical reference:" << UtIO::endl;
        hier_ref_net_for_init->print(false,2);
      }

      // Create the unelaborated initial driver on the hierarchical
      // reference to the initialization location.
      makeInit(hier_ref_net_for_init, tri_init_elab_value);

    } else {
      NU_ASSERT2(tri_init_elab_value==tri_init_value,net_elab,net_for_init);
      makeInit(net_for_init, tri_init_value);
    }

    if (mVerbose) {UtIO::cout() << UtIO::endl;}
  }

}


NUNet * RETriInit::createHierRefForInit(NUNetElab * net_elab, STAliasedLeafNode * alias_for_init)
{
  NUNet * net_for_init = NUNet::find(alias_for_init);

  // Create a hierarchical bound for the net requiring initialization.
  NUNetRefHdl net_for_init_ref = mNetRefFactory->createNetRef(net_for_init);
  FLNode * hier_bound = mFlowFactory->createBound(net_for_init_ref, eFLBoundHierRef);
  net_for_init->addContinuousDriver(hier_bound);

  // Connect the initial location to the UD for the ext net. This
  // keeps it unelaboratedly alive for its hierarchical reference.
  NUModule * init_module = net_for_init->getScope()->getModule();
  NUNet * ext_net = init_module->getExtNet();
  NUNetRefHdl ext_net_ref = mNetRefFactory->createNetRef(ext_net);
  init_module->addDef(ext_net_ref);
  init_module->addUse(ext_net_ref, net_for_init_ref);

  // Add unelaborated flow for the ext net, if needed.
  FLNode * ext_net_driver = NULL;
  for (NUNet::DriverLoop loop = ext_net->loopContinuousDrivers();
       (not ext_net_driver) and (not loop.atEnd());
       ++loop) {
    FLNode * driver = (*loop);
    if (driver->getUseDefNode() == init_module) {
      ext_net_driver = driver;
    }
  }
  if (not ext_net_driver) {
    ext_net_driver = mFlowFactory->create(ext_net_ref, init_module);
    ext_net->addContinuousDriver(ext_net_driver);
  }
  // Connect the hierarchial bound as fanin to the ext net. This
  // makes it appear unelaboratedly live to the dead flow walks.
  ext_net_driver->connectFanin(hier_bound);


  // The name for this hierarchical reference is the full path to
  // the initialization location.
  AtomArray hier_ref_path;
  NUHierRef::buildAtomArray(&hier_ref_path, alias_for_init);

  // This hierarchical references goes to the top of the design.
  NUModule * top_module = *mDesign->loopTopLevelModules();

  // Find a pre-existing or create a hierarchical reference to the
  // initial location.
  NUNet * hier_ref_net_for_init = top_module->findNetHierRef(hier_ref_path);
  if (not hier_ref_net_for_init) {
    NetFlags flags = NetFlags(net_for_init->getDeclarationFlags() | eAllocatedNet);
    hier_ref_net_for_init = net_for_init->createHierRef(hier_ref_path, top_module, flags, net_for_init->getLoc());
    net_for_init->addHierRef(hier_ref_net_for_init);
    hier_ref_net_for_init->getHierRef()->addResolution(net_for_init);
    top_module->addNetHierRef(hier_ref_net_for_init);

    // If this hierarchical reference did not exist, we need to
    // elaborate it. We also immediately alias it to the current
    // elaborated net. Since the alias_for_init is an alias of
    // net_elab and hier_ref_net_elab is a hierarchical reference
    // to alias_for_init, hier_ref_net_elab is also an alias of
    // net_elab.
    STAliasedLeafNode * storage = net_elab->getStorageSymNode();
    STBranchNode * top_hier = top_module->lookupElab(mSymtab)->getHier();
    hier_ref_net_for_init->createElab(top_hier, mSymtab);
    NUNetElab * hier_ref_net_elab = hier_ref_net_for_init->lookupElab(top_hier);
    hier_ref_net_elab->alias(net_elab);
    delete hier_ref_net_elab;
    storage->setThisStorage();
  }
  return hier_ref_net_for_init;
}


void RETriInit::updateAliasesWithValue(NUNetElab * net_elab, TriInitValue tri_init_value)
{
  mInitialDriverElabValue[net_elab] = tri_init_value;

  for (STAliasedLeafNode::AliasLoop loop(net_elab->getSymNode());
       not loop.atEnd();
       ++loop) {
    STAliasedLeafNode * alias = (*loop);
    if (mReachableAliases->query(alias)) {
      NUNet * alias_net = NUNet::find(alias);
      if (alias_net==NULL) { continue; }

      InitialDriverValueMap::iterator location = mInitialDriverValue.find(alias_net);
      if (location != mInitialDriverValue.end()) {
        TriInitValue current_init_value = location->second;
        // If this net has already been marked with a different value
        // and hasn't already been identified as a conflict, then mark
        // it as a conflict.
        if ((current_init_value != eTriInitConflict) and 
            (current_init_value != tri_init_value)) {
          mInitialDriverValue[alias_net] = eTriInitConflict;
        }
      } else {
        // First time we've encountered this net -- save its initial value.
        mInitialDriverValue[alias_net] = tri_init_value;
      }
    }
  }    
}


void RETriInit::triInitDesign(Stats *stats, bool phase_stats)
{
  UtStatsInterval statsInt(phase_stats, stats);

  // During tri-init discovery, we want to include the contribution
  // from port connections in the read/write flags. Determining
  // whether or not a net requires initialization depends on
  // read/write propagation across ports.
  walkDesign();
  statsInt.printIntervalStatistics("WalkDesign");

  markNonTriInit();
  statsInt.printIntervalStatistics("MarkNonTriInit");

  // During tri-init creation, we do not include the read/write
  // contribution from port connections. We need to place initial
  // drivers with true readers/writers in order to maintain an
  // accurate unelaborated flow graph. See ::determineInitLocation for
  // further details.

  // Reset net r/w flags to ignore simple port connections
  /* 
     Note this must happen before force
     resolution. IODBNucleus::resolveForce() only marks written
     aliases of an unelaborated force net as forcible.
  */
  {
    MarkReadWriteNets rw(mIODB, mArgs, false);
    rw.design(mDesign);
  }
  statsInt.printIntervalStatistics("PreReadWrite");

  makeAllInit();
  statsInt.printIntervalStatistics("MakeAllInit");

  // Reset net r/w flags to pay attention to simple port connections
  {
    MarkReadWriteNets rw(mIODB, mArgs, true);
    rw.design(mDesign);
  }
  statsInt.printIntervalStatistics("PostReadWrite");

  // Remove all flow, we will recreate it
  {
    RemoveUnelabFlow remove_unelab(mFlowFactory);
    RemoveElabFlow remove_elab(mFlowElabFactory, mSymtab);
    remove_unelab.remove();
    remove_elab.remove();
  }
  statsInt.printIntervalStatistics("RemFlow");

  // Recompute unelaborated flow
  bool verboseFlow    = mArgs->getBoolValue("-verboseFlow"); 
  bool verboseCleanup = mArgs->getBoolValue("-verboseCleanup");
  {
    UnelabFlow unelab_flow(mFlowFactory, mNetRefFactory, mMsgContext, mIODB,
                           verboseFlow);
    unelab_flow.design(mDesign);
  }
  statsInt.printIntervalStatistics("Flow");

  {
    DeadNetCallback dead_net_callback(mSymtab);
    RemoveDeadCode cleanup(mNetRefFactory, mFlowFactory,
                           mIODB, mArgs, mMsgContext, 
                           false, // only start at important nets.
                           true,  // always delete dead code.
                           false, // do not delete module instances
                           verboseCleanup);
    cleanup.design(mDesign,&dead_net_callback);
  }
  statsInt.printIntervalStatistics("RemoveDeadCode");

  // Recompute elaborated flow
  {
    Elaborate elab_flow(mSymtab, mAtomicCache, mSourceLocatorFactory,
                        mFlowFactory, mFlowElabFactory, mNetRefFactory,
                        mMsgContext, mArgs, mIODB);
    elab_flow.designFlow(mDesign, false /* do not elab port flow */ );
  }
  statsInt.printIntervalStatistics("ElabFlow");

  // Cleanup after re-elaboration
  {
    CleanElabFlow cleanup(mFlowElabFactory, mSymtab, verboseCleanup);
    cleanup.design(mDesign);
  }
  statsInt.printIntervalStatistics("DesElabCleanup");

  // Sanity check the tri-inits
  checkNonTriInit();
  checkInitialDriverConsistency();
  statsInt.printIntervalStatistics("CheckTriInit");

  // Unelaborated sanity check
  bool verboseSanity = mArgs->getBoolValue("-verboseSanity");
  {
    SanityUnelab sanity(mMsgContext, mNetRefFactory, mFlowFactory, true,
                        verboseSanity);
    sanity.design(mDesign);
  }
  statsInt.printIntervalStatistics("SanityUnelab");

  // Elaborated sanity check
  {
    SanityElab sanity(mMsgContext, mSymtab, mNetRefFactory, mFlowFactory,
                      mFlowElabFactory, true, verboseSanity);
    sanity.design(mDesign);
  }
  statsInt.printIntervalStatistics("SanityElab");
}


void RETriInit::rememberFanout(NUNetElab *net,
			       FLNodeElab *fanout,
			       Sensitivity sense)
{
  if (mVerbose) {
    UtIO::cout() << "Fanout:" << UtIO::endl;
    net->print(0,2);
    if (fanout) {
      fanout->print(0,2);
    } else {
      UtIO::cout() << "  0" << UtIO::endl;
    }
    UtIO::cout() << UtIO::endl;
  }

  NetNodeElabSetMapIter fanout_iter = mFanouts.find(net);
  if (fanout_iter == mFanouts.end()) {
    FlowSenseSet *fanouts = new FlowSenseSet;
    fanouts->insert(FlowSense(fanout, sense));
    mFanouts[net] = fanouts;
  } else {
    FlowSenseSet *fanouts = fanout_iter->second;
    fanouts->insert(FlowSense(fanout, sense));
  }
}


RETriInit::~RETriInit()
{
  for (NetNodeElabSetMapIter iter = mFanouts.begin(); iter != mFanouts.end(); ++iter) {
    FlowSenseSet *this_set = iter->second;
    delete this_set;
  }
}
