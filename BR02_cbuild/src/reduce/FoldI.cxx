// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Simple constant folding pass.  Housekeeping and infrastructure code for Folding.
 */

#include "FoldI.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUSysFunctionCall.h"
#include "reduce/Congruent.h"

// And a table of properties (sized from 0..eInvalid)
// The properties are represented by a bit that indicates if the
// operator is
// (C)	Commutative
// (0)  Has a zero-identity
// (1)  has a one-identity, viz X <op> 1 == X
// (M)  has a minus-identity, X<op> -1 == X
// (R)  is a relational operator.
// (I)  X <op> X == X
// (Z)  X <op> X == 0
// (B)  bit-wise (e.g. ^, &, 
// (L)  logical (e.g. &&, ||, !)
// (A)  associative
// (S)  signed operator
// (U)  unsigned operator
//
// Last entry in this table is nonsensical, so that we can
// check later to make sure that someone has not added
// a NUOp without adding a line to this table.
// [I tried to make this a static member of FoldI, but linker was
//  really unhappy with that]
//
const OpProperty gOpProperties[NUOp::eInvalid+1] = {
  //		
  //          	       C 0 1 M R I Z B L A S U
  /*0*/               {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnBuf*/	      {0,0,0,0,0,1,0,1,0,0,0,0},
  /*eUnPlus*/ 	      {0,0,0,0,0,1,0,0,0,0,0,0},
  /*eUnMinus*/	      {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnLogNot*/       {0,0,0,0,0,0,0,0,1,0,0,0},
  /*eUnBitNeg*/       {0,0,0,0,0,0,0,1,0,0,0,0},   // Careful here!
  /*eUnVhdlNot*/      {0,0,0,0,0,0,0,1,0,0,0,0},
  /*eUnRedAnd*/       {0,0,0,1,0,0,0,0,0,0,0,0},
  /*eUnRedOr*/        {0,1,0,0,0,0,0,0,0,0,0,0},
  /*eUnRedXor*/       {0,1,0,0,0,0,0,0,0,0,0,0},
  /*eUnCount*/        {0,1,1,0,0,0,0,0,0,0,0,0},
  /*eUnAbs*/          {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnFFZ*/          {0,1,0,0,0,0,0,0,0,0,0,0},
  /*eUnFFO*/          {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnFLZ*/          {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnFLO*/          {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnRound*/        {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnItoR*/         {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnRtoI*/         {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnRealtoBits*/   {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnBitstoReal*/   {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eUnChange*/       {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eBiPlus*/	      {1,1,0,0,0,0,0,0,0,1,0,0},  // don't fold (a+b+0), need carry
  /*eBiMinus*/	      {0,1,0,0,0,0,1,0,0,0,0,0},  // don't fold (a+b-0)
  /*eBiSMult*/ 	      {1,0,1,0,0,0,0,0,0,1,1,0},
  /*eBiUMult*/ 	      {1,0,1,0,0,0,0,0,0,1,0,1},
  /*eBiSDiv*/  	      {0,0,1,0,0,0,0,0,0,0,1,0},
  /*eBiUDiv*/  	      {0,0,1,0,0,0,0,0,0,0,0,1},
  /*eBiSMod*/	      {0,0,0,0,0,0,1,0,0,0,1,0},
  /*eBiUMod*/	      {0,0,0,0,0,0,1,0,0,0,0,1},
  /*eBiVhdlMod*/      {0,0,0,0,0,0,1,0,0,0,0,0},
  /*eBiEq*/   	      {1,0,0,0,1,0,0,0,0,0,0,0},
  /*eBiNeq*/  	      {1,0,0,0,1,0,1,0,0,0,0,0},
  /*eBiTrieq*/	      {1,0,0,0,1,0,0,0,0,0,0,0},
  /*eBiTrineq*/	      {1,0,0,0,1,0,0,0,0,0,0,0},
  /*eBiLogAnd*/	      {1,0,1,0,0,1,0,0,1,1,0,0},
  /*eBiLogOr*/	      {1,1,0,0,0,1,0,0,1,1,0,0},
  /*eBiSLt*/	      {0,0,0,0,1,0,1,0,0,0,1,0},
  /*eBiSLte*/	      {0,0,0,0,1,0,0,0,0,0,1,0},
  /*eBiSGtr*/	      {0,0,0,0,1,0,1,0,0,0,1,0},
  /*eBiSGtre*/	      {0,0,0,0,1,0,0,0,0,0,1,0},
  /*eBiULt*/	      {0,0,0,0,1,0,1,0,0,0,0,1},
  /*eBiULte*/	      {0,0,0,0,1,0,0,0,0,0,0,1},
  /*eBiUGtr*/	      {0,0,0,0,1,0,1,0,0,0,0,1},
  /*eBiUGtre*/	      {0,0,0,0,1,0,0,0,0,0,0,1},
  /*eBiBitAnd*/	      {1,0,0,1,0,1,0,1,0,1,0,0},
  /*eBiBitOr*/	      {1,1,0,0,0,1,0,1,0,1,0,0},
  /*eBiBitXor*/	      {1,1,0,0,0,0,1,1,0,1,0,0},
  /*eBiRoR*/          {0,1,0,0,0,0,0,0,0,0,0,0},
  /*eBiRoL*/          {0,1,0,0,0,0,0,0,0,0,0,0},
  /*eBiExp*/          {0,0,1,0,0,0,0,0,0,0,0,0},
  /*eBiDExp*/         {0,0,1,0,0,0,0,0,0,0,0,0},
  /*eBiVhExt*/	      {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eBiVhZxt*/	      {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eBiVhLshift*/     {0,1,0,0,0,0,0,0,0,0,0,0},
  /*eBiVhRshift*/     {0,1,0,0,0,0,0,0,0,0,0,0},
  /*eBiVhLshiftArith*/{0,1,0,0,0,0,0,0,0,0,0,0},
  /*eBiVhRshiftArith*/{0,1,0,0,0,0,0,0,0,0,0,0},
  /*eBiRshiftArith*/  {0,1,0,0,0,0,0,0,0,0,1,0},
  /*eBiLshiftArith*/  {0,1,0,0,0,0,0,0,0,0,1,0},
  /*eBiRshift*/	      {0,1,0,0,0,0,0,0,0,0,0,1},
  /*eBiLshift*/	      {0,1,0,0,0,0,0,0,0,0,0,1},
  /*eBiDownTo*/       {0,0,0,0,0,0,0,0,0,0,0,0},	// VHDL vector range
  /*eTeCond*/	      {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eNaConcat*/       {0,0,0,0,0,0,0,0,0,0,0,0},              //!< N-ary {}
  /*eNaFopen*/        {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eNaLut*/          {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eZEndFile*/       {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eZSysTime*/       {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eZSysSTime*/      {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eZSysRealTime*/   {0,0,0,0,0,0,0,0,0,0,0,0},
  /*eInvalid*/        {1,1,1,1,1,1,1,1,0,0,0,0}     // nonsense entry as guard
};

// Trace buffer for changes
struct FoldTraceObject: public UtString {
  SourceLocator mLoc;

  FoldTraceObject (SourceLocator loc)
    : UtString (), mLoc (loc) {}

  ~FoldTraceObject () {}

  bool operator ()(const FoldTraceObject& a, const FoldTraceObject& b) const
  {
    return (a.mLoc < b.mLoc);
  }

  bool operator< (const FoldTraceObject& b) const
  {
    if (mLoc < b.mLoc)
    {
      return true;
    }
    else if (b.mLoc < mLoc)
    {
      return false;
    }
    else
    {
      // must be equal, use the string itself for comparison
      return ( static_cast<UtString>(*this)   <   static_cast<UtString>(b) );
    }
  }
};


FoldI::FoldI(NUNetRefFactory *netref_factory, ArgProc *args,
             MsgContext *msg_ctx, AtomicCache *cache, IODBNucleus *iodb,
             bool verbose,
             UInt32 flags) :
  DesignWalker(verbose),
  mNoFinish (0),
  mFactory (netref_factory),
  mArgs (args),
  mMsgContext(msg_ctx),
  mContextStack (new UtStack<NUBlockScope*>),
  mStrCache (cache),
  mIODB (iodb),
  mChanged (false),
  mFlags (flags),
  mCC(0, 0),
  mTraceFold (mArgs->getBoolValue(CRYPT("-traceFold"))),
  mTraceBDDFold (0),
  mNoFoldIfCase(mArgs->getBoolValue(CRYPT("-nofoldIfCase"))),
  mNoFoldCaseIntoLut(mArgs->getBoolValue(CRYPT("-nofoldCaseIntoLut"))),
  mNOOOB (mArgs->getBoolValue (CRYPT ("-no-OOB"))),
  mFoldBDD(mArgs->getBoolValue(CRYPT("-foldBDD"))),
  mSharingExprFactory(false),
  mProcessingContinuousAssign(NULL),
  mChangedList (new ChangedList),
  mExprMap (new NUExprExprHashMap),
  mNetToExprVec (new NetToExprVec),
  mExprCache (new NUExprFactory),
  mAssocOrderSet (new NUCExprHashSet),
  mBDDRecursionBlocker (false),
  mFoldExprDepth(0)
{
  // if the following assert fails then most likely forgot to add a
  // line to the OpProperty array.  It will happen EVERYTIME fold runs, so
  // if we had STATIC_ASSERT, that would be preferable - wait for C++ 2009...
  INFO_ASSERT ( gOpProperties[NUOp::eInvalid].guarded (),
                "fold operator property table missing entries." );

  initBDDContext();
  mCostContext = new NUCostContext;
  mCongruent = new Congruent(mIODB, false, 0, NULL, mMsgContext, false);
  mExprBDDSimplifies = new NUCExprHashSet;
  mBDDPrescan = new BDDPrescan(netref_factory);
  mEffectiveBitSizeCache = new NUExpr::ExprIntMap;
}

void FoldI::initBDDContext() {
  mBDDContext = new BDDContext;
  mBDDContext->putExpressionFactory(mExprCache);
  mBDDContext->putExpressionCaching(true);
  mBDDContext->putIsConservative(true);
  if (isBDDMux()) {
    mBDDContext->putCreateMuxes(true);
  }
}

void FoldI::putFlags(UInt32 flags) {
  if (flags != mFlags) {
    mFlags = flags;
    clearExprCache();
  }
}

FoldI::~FoldI ()
{
  if (mTraceFold)
    dumpTrace ();

  // Cleanup the change list
  for(ChangedList::iterator i=mChangedList->begin (); i!= mChangedList->end (); ++i)
    delete (*i);

  delete mExprMap;
  if (! mSharingExprFactory) {
    delete mExprCache;
  }
  delete mNetToExprVec;
  delete mAssocOrderSet;

  // Toss any deferred garbage collections
  for (size_t i = 0; i < mPending.size(); ++i)
    delete mPending[i];

  delete mChangedList;
  delete mContextStack;
  delete mBDDContext;
  delete mCostContext;
  delete mCongruent;
  delete mExprBDDSimplifies;
  delete mBDDPrescan;
  delete mEffectiveBitSizeCache;
}


// Private class to compare changelists
struct ChangedListCompare{
  bool operator()(const FoldTraceObject* a, const FoldTraceObject* b) const {
    return (*a) < (*b);
  }
}; 

NULvalue*
FoldI::finish (NULvalue* oldval, NULvalue* newval)
{
  NU_ASSERT (mNoFinish == 0, oldval);

  // Defend against a transform that doesn't actually do anything.  If we transformed
  // an expression into an identical expression, then we are about to get stuck into
  // a recursive fold loop and crash badly.
  //
  NU_ASSERT ( !((*oldval) == (*newval)), oldval);

  print (oldval, newval);
  mPending.push_back (oldval);
  return newval;
}

const NUExpr* FoldI::finish(const NUExpr* oldval, const NUExpr *newval,
                            bool resize, bool /* checkSelfSize */)
{
  NU_ASSERT(oldval->isManaged(), oldval);

  // UD is valid going in, then avoiding adding refs.  I want to leave this
  // code in place, but it needs to be off now because we fail due to pessimistic
  // varsel use-set calculation.
  //
  // Example:  u[7:4][v[1:0]] folds to u[EXPR=(v[1:0] + 0) RANGE=4:4]
  // 
  // The effective bit size of the EXPR should be 2.  But at the moment
  // it's 3, but it should be 2 because +0 cannot wrap).  So the bits
  // referencable should go from lsb=4 to msb=4+(1<<2)-1=7.  If
  // NUVarselRvalue::getUses made that determination then
  // test/cust/deshaw/midrange/cwtb would not fail this code below.  We'd
  // have to fix both NUBinaryOp::effectiveBitSize() on +, and
  // NUVarselRvalue::getUses to look for this case.
#if 0
  if (((mFlags & eFoldUDValid) != 0) &&
      ((mFlags & eFoldAggressive) == 0))
       //((mFlags & eFoldUDKiller) == 0) &&
       //((mFlags & eFoldUDKiller) == 0) &&
    //((mFlags & eFoldComputeUD) == 0))
  {
    NUNetRefSet oldUses(mFactory), newUses(mFactory);
    oldval->getUses(&oldUses);
    newval->getUses(&newUses);
    NUNetRefSet::set_subtract(oldUses, newUses);

    // If, after having subtraced out the old uses, there are still new uses, then
    // we are in trouble.
    if (!newUses.empty()) {
      NU_ASSERT2(newUses.size() == 1, oldval, newval);
      NU_ASSERT2((*newUses.begin())->empty(), oldval, newval);
    }
  }
#endif

  // First check that we aren't trying to call finish in situations
  // where we must not delete the old tree because this is a
  // conditional fold that will only be performed if other folds are
  // performed also.
  //
  NU_ASSERT (mNoFinish == 0, oldval);

  // Make sure newval is managed & appropriately sized & signed
  UInt32 oldContextSize = oldval->getBitSize ();
  bool oldSign = oldval->isSignedResult();
  if (!newval->isManaged()) {
    if (resize) {
      (const_cast<NUExpr*>(newval))->resize(oldContextSize);
    }
    newval = manage(newval, false);
  }

  if (resize) {
    newval = mExprCache->resizeSign(newval, oldContextSize, oldSign);
  }
  else {
    newval = mExprCache->changeSign(newval, oldSign);
  }

  // Defend against a transform that doesn't actually do anything.  If we transformed
  // an expression into an identical expression, then we are about to get stuck into
  // a recursive fold loop and crash badly.
  //
  // Two expressions are DIFFERENT if they have different bitwidths too.
  // Since oldval and newval are managed, we can compare with ==
  NU_ASSERT(oldval != newval, oldval);

  print (oldval, newval);
  return newval;
} // const NUExpr* FoldI::finish

// Stmts are shallow-copied, so after calling finish, you must replace the
// oldval's statements by an empty-list.
//
NUStmt*
FoldI::finish (NUStmt* oldval, NUStmt* newval)
{
  NU_ASSERT (mNoFinish == 0, oldval);
  print (oldval,newval);
  mPending.push_back (oldval);
  if (newval)
    return foldStmt (newval);
  else
    return 0;
}

NUUseDefNode*
FoldI::finish (NUUseDefNode* oldval, NUUseDefNode* newval)
{
  NU_ASSERT (mNoFinish == 0, oldval);
  print (oldval,newval);
  mPending.push_back (oldval);
  return newval;
}

void
FoldI::dumpTrace (void)
{
  // First sort the list by sourcelocator
  std::stable_sort (mChangedList->begin (), mChangedList->end (),
		    ChangedListCompare ());

  // Then print it
  for (ChangedList::iterator i = mChangedList->begin ();
       i !=mChangedList->end ();
       ++i)
    {
      FoldTraceObject *aChange = *i;
      UtIO::cout () << "Folded :";
      aChange->mLoc.print ();
      UtIO::cout () << UtIO::endl
		    << "       :" << *aChange << UtIO::endl;
      
    }
}

//! Called when an expression is folded
void FoldI::print (const NUExpr *oldval, const NUExpr *newval)
{
  if (!mBDDRecursionBlocker) {
    mChanged = true;		// Mark changes
    if (mVerbose) {
      UtString buf ("Folded :");
      oldval->compose (&buf, NULL);
      buf << "\n       :";
      newval->compose (&buf, NULL);
      buf << "\n";
      UtIO::cout () << buf;
    }
    if (mTraceFold) {
      FoldTraceObject *aChange = new FoldTraceObject (oldval->getLoc ());
      oldval->compose (aChange, NULL);    // should this use scope?
      *aChange += "\n       :";
      newval->compose (aChange, NULL);    // should this use scope?
      mChangedList->push_back (aChange);
    }
  }
}

void FoldI::print (const NUUseDefNode *oldval, const NUUseDefNode *newval)
{
  mChanged = true;		// Mark changes

  if (mVerbose)
    {
      UtIO::cout() << "Folded :";
      UtString buf;
      oldval->compose (&buf, NULL, 0);
      UtIO::cout() << buf << "       :";
      buf.erase ();
      if (newval)
        newval->compose (&buf, NULL, 0);
      buf << "\n";
      UtIO::cout () << buf;
    }
  if (mTraceFold)
    {
      FoldTraceObject *aChange = new FoldTraceObject (oldval->getLoc ());
      oldval->compose (static_cast<UtString*>(aChange), NULL /* scope */,  8);
      if ((*aChange)[aChange->length ()-1] != '\n')
        *aChange += "\n";
      *aChange += "       :";
      if (newval)
        newval->compose (static_cast<UtString*>(aChange), NULL /* scope */,  8);
      if ((*aChange)[aChange->length ()-1] != '\n')
        *aChange += "\n";

      mChangedList->push_back (aChange);
    }
}

void FoldI::print(const NUCaseItem* item1, const NUCaseItem* item2)
{
  mChanged = true;		// Mark changes
  if (mVerbose)
    {
      UtString buf ("Folded (identical case item) :\n");
      item1->compose (&buf, NULL, 8, true);
      UtIO::cout() << buf << "\n       :\n";
      buf.erase ();
      item2->compose (&buf, NULL, 8, true);
      UtIO::cout () << buf << UtIO::endl;
    }
  if (mTraceFold)
    {
      FoldTraceObject *aChange = new FoldTraceObject (item1->getLoc ());
      *aChange += "(identical case item):\n";
      item1->compose (aChange, NULL, 8, true);
      *aChange += "       :\n";
      item2->compose (aChange, NULL, 8, true);
      mChangedList->push_back (aChange);
    }
}


void FoldI::print(const NUCaseItem* item)
{
  mChanged = true;		// Mark changes
  if (mVerbose)
    {
      UtString buf ("Folded (overspecified case; removed conditions) :\n");
      item->compose (&buf, NULL, 8, true);
      UtIO::cout() << buf << UtIO::endl;
    }
  if (mTraceFold)
    {
      FoldTraceObject *aChange = new FoldTraceObject (item->getLoc ());
      *aChange += "(overspecified case; removed conditions):\n";
      item->compose (aChange, NULL, 8, true);
      *aChange += "\n";
      mChangedList->push_back (aChange);
    }
}


bool FoldI::isReductionOp (NUOp::OpT opcode)
{
  return ((opcode >= NUOp::eUnRedAnd)
            && (opcode <= NUOp::eUnRedXor))
    || (opcode == NUOp::eUnLogNot);
}

void FoldI::clearBDDContext() {
  // TO avoid the BDD package getting really slow as it learns a lot
  // of BDDs, reinitialize it after it grows to 10k.
  if (mBDDContext->numEntries() > 10000) {
    delete mBDDContext;       // not_managed_expr
    initBDDContext();
  }

  // Otherwise, we need to clear the maps so that each time we visit
  // an expression we introduce the variables to the BDD package according
  // to expression traversal order.  It turns out this order is important,
  // as shown by test/constprop/bidi1.v, where a random order may
  // transform expression
  //    (top.S1.a ? (top.S1.b & top.S1.in) : 1)
  // into the BDD
  //    top.S1.in ? (top.S1.b | (!top.S1.a)) : (!top.S1.a)
  // That occurs if the BDD package "learns" about top.S1.a before
  // top.S1.in.  So expression traversal order is desirable. 
  else
  {
    mBDDContext->clear();
  }
} // void FoldI::clearBDDContext

void FoldI::clearExprCache() {
  mExprCache->clear();
  mExprMap->clear();
  mNetToExprVec->clear();
  mExprBDDSimplifies->clear();
  mCongruent->clear();
  clearBDDContext();
  mCostContext->clear();
  mBDDPrescan->clear();
  mEffectiveBitSizeCache->clear();
  cleanup();
}

void FoldI::shareExprCache(NUExprFactory* expr_factory) {
  if (! mSharingExprFactory) {
    INFO_ASSERT(mExprCache->empty(),
                "Cannot share expr cache if Fold has already populated its own");
    delete mExprCache;        // not_managed_expr
  }
  mExprCache = expr_factory;
  delete mBDDContext;       // not_managed_expr
  initBDDContext();
  mSharingExprFactory = true;
}

bool FoldI::putVerbose(bool flag) {
  bool ret = mVerbose;
  mVerbose = flag;
  return ret;
}

bool FoldI::putTraceFold(bool flag) {
  bool ret = mTraceFold;
  mTraceFold = flag;
  return ret;
}

UInt32 FoldI::putTraceBDDFold(UInt32 verboseFlags) {
  UInt32 ret = mTraceBDDFold;
  mTraceBDDFold = verboseFlags;
  return ret;
}

const NUExpr* FoldI::enroll(NUExpr* e, UInt32 sz, SpecifySign sign_spec) {
  NU_ASSERT(!e->isManaged(), e);
  
  if (sz == 0) {
    sz = e->determineBitSize();
  }

  {
    //NUExpr* cmp = e->makeSizeExplicit(sz);
    //NU_ASSERT(cmp == e, e);
    //
    //  test/buffer-insertion/bug5072.v has a case where a mis-sized
    //  port-connection makes it to constant propagation, where an
    //  attempt to is made to take an 18-bit partselect of a smaller
    //  bit-sized concat.  I'd like to assert here and solve this in
    //  population, but for the moment let's just 0-extend this right
    //  here.  I'm concerned that there might be complex port-splitting
    //  ramifications if I do the explicit sizes on the port-connections
    //  upstream of constant propagation
    // 
    e = e->makeSizeExplicit(sz);
  }

  if (sign_spec != eDefaultSign) {
    e->setSignedResult(sign_spec == eSigned);
  }
  return manage(e, false);
}

const NUConst* FoldI::constant(bool is_signed, const DynBitVector& v, UInt32 width,
                               const SourceLocator& loc)
{
  NUConst *result = NUConst::create (is_signed, v, width, loc);
  return (const NUConst*) manage(result, false);
}

const NUConst* FoldI::constant(CarbonReal value, const SourceLocator& loc,
                               bool is_signed)
{
  NUConst *result = NUConst::create (value, loc);
  result->putSignedResult(is_signed);
  return (const NUConst*) manage(result, false);
}

const NUConst* FoldI::constantXZ(bool is_signed, const DynBitVector& val,
                                 const DynBitVector& drive, UInt32 width,
                                 const SourceLocator& loc)
{
  NUConst *result = NUConst::createXZ (is_signed, val, drive, width, loc);
  return (const NUConst*) manage(result, false);
}

const NUConst* FoldI::constantXZ(bool is_signed, bool val, bool drive, UInt32 width,
                                 const SourceLocator& loc)
{
  NUConst *result = NUConst::createXZ (is_signed, val, drive, width, loc);
  return (const NUConst*) manage(result, false);
}

const NUConst* FoldI::constant(bool is_signed, UInt64 val, UInt32 width,
                               const SourceLocator& loc)
{
  NUConst *result = NUConst::create (is_signed, val, width, loc);
  return (const NUConst*) manage(result, false);
}

const NUExpr* FoldI::binaryOp(NUOp::OpT op, const NUExpr* e1,
                              const NUExpr* e2, const SourceLocator& loc,
                              UInt32 bit_size, SpecifySign sign_spec)
{
  NU_ASSERT(e1->isManaged(), e1);
  NU_ASSERT(e2->isManaged(), e2);
  NUBinaryOp* bop = new NUBinaryOp(op, e1, e2, loc);
  return enroll(bop, bit_size, sign_spec);
}

const NUExpr* FoldI::unaryOp(NUOp::OpT op, const NUExpr* e1,
                             const SourceLocator& loc,
                             UInt32 bit_size, SpecifySign sign_spec)
{
  NU_ASSERT(e1->isManaged(), e1);
  //e1 = manage(e1, true);
  NUUnaryOp* uop = (op == NUOp::eUnChange)
    ? new NUChangeDetect(e1)
    : new NUUnaryOp(op, e1, loc);
  return enroll(uop, bit_size, sign_spec);
}

const NUExpr* FoldI::ternaryOp(const NUExpr* e1, const NUExpr* e2,
                               const NUExpr* e3, const SourceLocator& loc,
                               UInt32 bit_size, SpecifySign sign_spec)
{
  NU_ASSERT(e1->isManaged(), e1);
  NU_ASSERT(e2->isManaged(), e2);
  NU_ASSERT(e3->isManaged(), e3);
  NUTernaryOp* top = new NUTernaryOp(NUOp::eTeCond, e1, e2, e3, loc);
  return enroll(top, bit_size, sign_spec);
}

const NUExpr* FoldI::concatOp(const NUCExprVector& pieces, UInt32 repeatCount,
                              const SourceLocator& loc, UInt32 bit_size,
                              SpecifySign sign_spec)
{
  for (UInt32 i = 0; i < pieces.size(); ++i) {
    NU_ASSERT(pieces[i]->isManaged(), pieces[i]);
  }
  NUConcatOp* cop = new NUConcatOp(pieces, repeatCount, loc);
  return enroll(cop, bit_size, sign_spec);
}

const NUExpr* FoldI::memsel(const NUExpr* id,
                            NUExprVector& pieces,
                            const SourceLocator& loc,
                            UInt32 bit_size,
                            bool is_resynthesized,
                            SpecifySign sign_spec)
{
  NUMemselRvalue* newMemsel =
    new NUMemselRvalue(const_cast<NUExpr*>(id), &pieces, loc);
  if (is_resynthesized) {
    newMemsel->putResynthesized(true);
  }
  return enroll(newMemsel, bit_size, sign_spec);
}

const NUExpr* FoldI::lutOp(const NUExpr* sel, const NUCConstVector& pieces,
                           const SourceLocator& loc, UInt32 bit_size,
                           SpecifySign sign_spec)
{
  NU_ASSERT(sel->isManaged(), sel);
  for (UInt32 i = 0; i < pieces.size(); ++i) {
    NU_ASSERT(pieces[i]->isManaged(), pieces[i]);
  }
  NULut* rep = new NULut(sel, pieces, loc);
  return enroll(rep, bit_size, sign_spec);
}

const NUExpr* FoldI::sysFuncCall(NUOp::OpT op, SInt8 timeUnit, SInt8 timePrecision,
                                 const NUExprVector& v, const SourceLocator& loc,
                                 UInt32 bit_size, SpecifySign sign_spec)
{
  NUSysFunctionCall* nsys = new NUSysFunctionCall(op, timeUnit,
                                                  timePrecision, v, loc);
  return enroll(nsys, bit_size, sign_spec);
}

const NUExpr* FoldI::varsel(const NUExpr* id, const ConstantRange& range,
                            const SourceLocator& loc, UInt32 bit_size,
                            SpecifySign sign_spec)
{
  NU_ASSERT(id->isManaged(), id);
  NUVarselRvalue* vs = new NUVarselRvalue(id, range, loc);
  return enroll(vs, bit_size, sign_spec);
}

const NUExpr* FoldI::varsel(const NUExpr* id, const NUExpr* select,
                            const ConstantRange& range,
                            const SourceLocator& loc, UInt32 bit_size,
                            SpecifySign sign_spec, bool isIndexInBounds)
{
  NU_ASSERT(id->isManaged(), id);
  NU_ASSERT(select->isManaged(), select);
  NUVarselRvalue* vs = new NUVarselRvalue(id, select, range, loc);
  if (isIndexInBounds) {
    vs->putIndexInBounds(true);
  }
  return enroll(vs, bit_size, sign_spec);
}

const NUExpr* FoldI::varsel(const NUExpr* id, const NUExpr* select,
                            const SourceLocator& loc, UInt32 bit_size,
                            SpecifySign sign_spec)
{
  NU_ASSERT(id->isManaged(), id);
  NU_ASSERT(select->isManaged(), select);
  NUVarselRvalue* vs = new NUVarselRvalue(id, select, loc);
  return enroll(vs, bit_size, sign_spec);
}

