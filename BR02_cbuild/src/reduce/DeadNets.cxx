// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/DeadNets.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUNet.h"

#include "compiler_driver/CarbonDBWrite.h"

void DeadNetCallback::operator()(NUNetList * dead_nets)
{
  if (dead_nets->empty()) {
    return;
  }

  CbuildSymTabBOM::BaseSet nets_pending_deletion;
  for ( NUNetList::iterator iter = dead_nets->begin();
	iter != dead_nets->end();
	++iter ) {
    NUNet * net = (*iter);
    nets_pending_deletion.insert(net);
  }

  CbuildSymTabBOM::ElabBaseSet netelabs_pending_deletion;
  CbuildSymTabBOM::getElabs(&nets_pending_deletion,
			    &netelabs_pending_deletion,
			    mSymtab);
  for (CbuildSymTabBOM::ElabBaseSet::iterator iter = netelabs_pending_deletion.begin();
       iter != netelabs_pending_deletion.end();
       ++iter) {
    NUElabBase* elab = (*iter);
    NUNetElab * netelab = dynamic_cast<NUNetElab*>(elab);
    NU_ASSERT(netelab,elab);

    // if we are deleting either the storage or master net, we need to
    // delete the entire alias ring. otherwise, null out only the net
    // ptrs which are being deleted.
    NUNet * master_net  = netelab->getNet();
    NUNet * storage_net = netelab->getStorageNet();

    if ((nets_pending_deletion.find(master_net)  != nets_pending_deletion.end()) or
	(nets_pending_deletion.find(storage_net) != nets_pending_deletion.end()) ) {
      delete netelab;
    } else {
      STAliasedLeafNode * master_leaf = netelab->getSymNode();
      STAliasedLeafNode * alias_leaf = master_leaf;
      while ( (alias_leaf = alias_leaf->getAlias()) != master_leaf ) {
	NUBase * base = CbuildSymTabBOM::getNucleusObj(alias_leaf);
	NUNet * alias_net = dynamic_cast<NUNet*>(base);
	NU_ASSERT(alias_net,base); // everything except the master should be NUNet*.

	if (nets_pending_deletion.find(alias_net) != nets_pending_deletion.end()) {
          CbuildSymTabBOM::putNucleusObj(alias_leaf, NULL);
	}

      }

    }
  }
}
