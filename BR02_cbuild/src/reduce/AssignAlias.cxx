// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/AssignAlias.h"
#include "reduce/REUtil.h"
#include "AliasManager.h"
#include "DefAfterUse.h"

#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUDesignWalkerCallbacks.h"

#include "flow/FLFactory.h"
#include "flow/FLIter.h"

#include "iodb/IODBNucleus.h"

#include "exprsynth/ExprCallbacks.h"
#include "exprsynth/ExprFactory.h"

#include "util/Stats.h"
#include "util/SetOps.h"

/*!\file
 * Definition of assignment and functional aliasing analysis.
 * AssignAlias is the analysis portion of the aliasing code --
 * the mechanics of aliasing are handled by the AliasManager
 * class.
 */

//! Functional expression cache
/*!
  Maintain a relation from expression to net. If two nets expand to
  the same expression, they are considered as potential aliases.
 */
class FunctionCache
{
public:
  //! Constructor
  FunctionCache() {}

  //! Destructor
  ~FunctionCache() {}

  //! Have we already encountered this net during processing?
  bool queryProcessed(NUNet * net) {
    return (mProcessed.find(net)!=mProcessed.end());
  }

  //! Mark this net as already encountered during processing.
  void addProcessed(NUNet * net) {
    NU_ASSERT(not queryProcessed(net), net); // double-check for now.
    mProcessed.insert(net);
  }

  //! Have we already encountered this expression?
  NUNet * query(ESExprHndl expr_handle) {
    INFO_ASSERT(expr_handle.getExpr()!=NULL,
                "query called before expr_handle populated");
    ExprNetMap::iterator location = mExprNetMap.find(expr_handle);
    if (location != mExprNetMap.end()) {
      NUNet * net = location->second;
      return net;
    } else {
      return NULL;
    }
  }

  //! Remember that an expression is associated with a net.
  void add(ESExprHndl expr_handle, NUNet * net) {
    NU_ASSERT(query(expr_handle)==NULL,net); // double-check for now.
    mExprNetMap.insert(ExprNetMap::value_type(expr_handle,net));
  }

private:

  //! Map from expression to net.
  typedef UtMap<ESExprHndl,NUNet*> ExprNetMap;

  //! Map from expression to net.
  ExprNetMap mExprNetMap;

  //! Set of nets encountered during functional aliasing.
  NUNetSet   mProcessed;
};

AssignAlias::AssignAlias(NUNetRefFactory *netref_factory,
                         FLNodeFactory *dfg_factory,
                         IODBNucleus *iodb,
                         ArgProc* args,
                         MsgContext *msg_ctx,
                         Stats * stats,
                         bool phase_stats,
                         bool find_functional_aliases,
                         bool cont_assigns_only,
                         SInt32 verbosity,
                         AssignAliasStatistics * statistics) :
  mAliasManager(new AliasManager(netref_factory,
                                 dfg_factory,
                                 iodb,
                                 args,
                                 msg_ctx,
                                 stats,
                                 phase_stats,
                                 (verbosity == 1),
                                 cont_assigns_only)),
  mNetRefFactory(netref_factory),
  mIODB(iodb),
  mStats(stats),
  mPhaseStats(phase_stats),
  mFindFunctionalAliases(find_functional_aliases),
  mContAssignsOnly(cont_assigns_only),
  mVerbosity(verbosity),
  mStatistics(statistics)
{
  mDefAfterUse = new DefAfterUse;
}


AssignAlias::~AssignAlias()
{
  delete mAliasManager;
  delete mDefAfterUse;
}


void AssignAlias::module(NUModule *this_module)
{
  if (mPhaseStats) {
    mStats->pushIntervalTimer();
  }

  mAliasManager->prepare(this_module);
  if (mPhaseStats) {
    mStats->printIntervalStatistics("AA:MgrPrepare");
  }

  mDefAfterUse->module(this_module);
  if (mPhaseStats) {
    mStats->printIntervalStatistics("AA:DUPrepare");
  }

  findAliases(this_module);
  if (mPhaseStats) {
    mStats->printIntervalStatistics("AA:Discovery");
  }

  mAliasManager->sanityCheckAliases();
  if (mPhaseStats) {
    mStats->printIntervalStatistics("AA:Sanity");
  }

  mAliasManager->performAliasing(mVerbosity > 0);
  if (mPhaseStats) {
    mStats->printIntervalStatistics("AA:Perform");
  }

  mAliasManager->clear();
  if (mPhaseStats) {
    mStats->printIntervalStatistics("AA:MgrCleanup");
  }

  mDefAfterUse->clear();
  if (mPhaseStats) {
    mStats->printIntervalStatistics("AA:DUCleanup");
  }

  if (mPhaseStats) {
    mStats->popIntervalTimer();
  }
}

void AssignAlias::findAliases(NUModule *this_module)
{
  ESFactory* exprFactory = new ESFactory;
  mPopulateExpr = new ESPopulateExpr(exprFactory, mNetRefFactory);
  mFunctionCache = new FunctionCache;

  NUNetList start_list;
  this_module->getAllNets(&start_list);

  NUNetSet start_nets;
  start_nets.insert(start_list.begin(),start_list.end());
  start_list.clear();
  
  // Note that a FLNetIter could have been used which takes the whole
  // start_list.  However, that seems to be a little slow for large
  // designs (such as ATI), since it is keeping a map of all arcs
  // to make sure we don't get stuck in loops.
  // So, instead, we walk the fanin for each net separately.
  for (NUNetSet::SortedLoop loop = start_nets.loopSorted();
       not loop.atEnd();
       ++loop) {
    NUNet * net = (*loop);
    walkNetFanin(net);
  }

  delete mFunctionCache;

  delete mPopulateExpr->getFactory();
  delete mPopulateExpr;
}

static bool sDrivenOnlyContinuously(NUNet* net) {
  for (NUNet::DriverLoop p = net->loopContinuousDrivers(); !p.atEnd(); ++p) {
    FLNode* driver = *p;
    NUUseDefNode* use_def = driver->getUseDefNode();
    if (use_def == NULL) {
      return false;
    }

    NUContAssign* assign = dynamic_cast<NUContAssign*>(use_def);

    // Initial drivers are problematic -- see test/zprop/rslatch_init.v
    if ((assign == NULL) &&
        (dynamic_cast<NUEnabledDriver*>(use_def) == NULL) &&
        (dynamic_cast<NUAlwaysBlock*>(use_def) == NULL))
    {
      return false;
    }

    // bug 6848, do not assignment alias a tied net, because a
    // tie should not flow backward across an assign
    if ((assign != NULL) && (assign->getStrength() == eStrTie)) {
      return false;
    }
  }
  return true;
}


//! Return true if any fanin net is a module input.
static bool sDriverHasInputFanin(FLNode * driver)
{
  for (FLNode::AllFaninLoop loop = driver->loopAllFanin();
       not loop.atEnd();
       ++loop) {
    FLNode * fanin = *loop;
    NUNet * fanin_net = fanin->getDefNet();
    if (fanin_net->isInput()) {
      return true;
    }
  }
  return false;
}


/*!
 * Return true if the net has more than 1 non-bound driver,
 * false otherwise.
 */
static bool sMultipleDriversIgnoreBounds(NUNet *net)
{
  UInt32 count = 0;
  for (NUNet::DriverLoop loop = net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    if (not (*loop)->isBoundNode()) {
      count++;
    }
    if (count > 1) {
      return true;
    }
  }
  return false;
}


void AssignAlias::walkNetFanin(NUNet *walk_net)
{
  FLNetIter flow_iter(walk_net,
                      FLIterFlags::eAll,                 // visit all nets
                      FLIterFlags::eNone,                // stop at no nets
                      FLIterFlags::eBranchAtAll,         // visit the whole design, so can see fanout
                      FLIterFlags::eIterFaninPairOnce);  // visit each arc once

  for (; not flow_iter.atEnd(); ++flow_iter) {
    FLNode *driver = flow_iter.getCur();
    FLNode *fanout = flow_iter.getCurParent();
    Sensitivity sense = flow_iter.getSensitivity();
    NUUseDefNode* use_def = driver->getUseDefNode();

    NUNet *driven_net = driver->getDefNet();

    if (not driven_net) {
      continue;
    }
    
    // Remember fanout, but do not count nesting as fanout
    if (fanout and (not fanout->isNested(driver))) {
      mAliasManager->rememberFanout(driven_net, fanout, sense);
    }
    else if (mAliasManager->isVerbose()){
      if (fanout) {
        UtIO::cout() << "No Fanout reported for driver of nesting:" << UtIO::endl;
      } else {
        UtIO::cout() << "No Fanout for this driver:" << UtIO::endl;
      }
    }

    // Remember the drivers for everything
    bool seen_before = mAliasManager->rememberDriver(driven_net, driver, sense);
    if (seen_before) {
      // If we've seen this driver before, then don't do the rest.
      flow_iter.doNotExpand();
      continue;
    }

    if (mContAssignsOnly) {
      // always-blocks are too hard to deal with for multiply driven nets
      if (dynamic_cast<NUContAssign*>(use_def) == NULL) {
        continue;
      }
      if (not sDrivenOnlyContinuously(driven_net)) {
        continue;
      }
      // Don't allow x = in aliasing if x has other drivers. bug 5766.
      if (driven_net->isMultiplyDriven() and sDriverHasInputFanin(driver)) {
        continue;
      }
    }
    else {
      // Multiply driven nets are too hard for functional aliasing.

      // Do not count bounds as drivers, this will allow a net which has
      // bound drivers to be aliased to its fanin.
      if (sMultipleDriversIgnoreBounds(driven_net)) {
        continue;
      }
    }

    // Do not process bound nodes.
    if (driver->isBoundNode()) {
      continue;
    }

    // Only process continuous drivers.
    if (not use_def->isContDriver()) {
      continue;
    }

    // add simple assignment aliases
    if (addFaninAlias(driver))
      mStatistics->assign();

    // add functional aliases
    if (mFindFunctionalAliases && addFunctionalAlias(driver))
      mStatistics->functional();
  }
}

bool AssignAlias::addFaninAlias(FLNode * driver)
{
  NUNet * driven_net = driver->getDefNet();

  // Make sure the driver is simple enough
  NUNet *fanin_net = testDriver(driver);
  if (not fanin_net) {
    return false;
  }

  return addAlias(fanin_net, driven_net, driver);
}

bool AssignAlias::invalidLvaluesRecurse(FLNode* flow) const
{
  // Either recurse until we find leaf level flow or analyze the flow
  // if this is a leaf level flow.
  bool found = false;
  if (flow->hasNestedBlock()) {
    // Recurse until we get to leaf level flow or we find a statment
    // that has a dynamic bit select.
    for (FLNodeLoop l = flow->loopNested(); !l.atEnd() && !found; ++l) {
      FLNode* subFlow = *l;
      found = invalidLvaluesRecurse(subFlow);
    }

  } else {
    // This is a leaf level flow, it must be a statement.
    NUUseDefNode* useDef = flow->getUseDefNode();
    NUStmt* stmt = dynamic_cast<NUStmt*>(useDef);
    if (stmt != NULL) {
      NUFindDynamicLvalues findInvalidLvalues;
      NUDesignWalker walker(findInvalidLvalues, false);
      if (walker.stmt(stmt) == NUDesignCallback::eStop) {
        found = true;
      }
    } else {
      // Must be something else, give up (module)
      found = false;
    }
  }

  return found;
} // bool AssignAlias::invalidLvaluesRecurse

bool AssignAlias::addFunctionalAlias(FLNode * driver)
{
  if (driver->isBoundNode()) {
    return false;
  }

  NUUseDefNode * driving_node = driver->getUseDefNode();
  if (not driving_node->isContDriver()) {
    return false;
  }
  
  // Skip initial blocks; intitial blocks are not the same as
  // continuous drivers.
  if (driving_node->isInitial()) {
    return false;
  }

  // Skip any drivers that have a dynamic lvalue
  if (invalidLvaluesRecurse(driver)) {
    return false;
  }

  // Don't alias constant assigns. The problem is some of them
  // represent z values (undriven nets) and others represent true
  // constants. If we alias them we can make a driven net look
  // undriven. We don't think there is a benefit to aliasing
  // constants.
  NUUseDefNode* useDef = driver->getUseDefNode();
  if ((useDef != NULL) && (useDef->getType() == eNUContAssign)) {
    NUContAssign* assign = dynamic_cast<NUContAssign*>(useDef);
    NU_ASSERT(assign != NULL, useDef);
    if (assign->getRvalue()->castConst() != NULL) {
      return false;
    }
  }

  NUNetRefHdl driven_net_ref = driver->getDefNetRef();
  if (not driven_net_ref->all()) {
    return false;
  }

  NUNet * driven_net = driver->getDefNet();

  if (driving_node->getType()==eNUAlwaysBlock) {
    NUAlwaysBlock * always = dynamic_cast<NUAlwaysBlock*>(driving_node);

    // avoid async clock/reset blocks for now.
    if (driving_node->isSequential()) {
      if (always->getClockBlock() or always->getPriorityBlock()) {
        return false;
      }
    }

    // avoid multi-definition situations.
    bool single_definition = testSingleDefinition(driven_net, always);
    if (not single_definition) {
      return false;
    }

    // Avoid sequential nets with reads. 
    //
    // always @(posedge clk) begin
    //   t1 = a & b;
    //   o1 = t1 & c;
    // end
    //
    // always @(posedge clk) begin
    //   t2 = a & b;
    //   o2 = t2 & d;
    // end
    //
    // It is unsafe to alias t1 and t2 because the reads in o1 and o2
    // cannot be delayed.
    bool read_of_sequential = testSequentialRead(driven_net, always);
    if (read_of_sequential) {
      return false;
    }
  }

  // Only process each net once in our quest for functional aliases.
  if (mFunctionCache->queryProcessed(driven_net)) {
    return false;
  }
  mFunctionCache->addProcessed(driven_net);

  // Create the expression for this node.
  ContinuousDriverSynthCallback callback(mPopulateExpr, driver);
  ESExprHndl expr_handle = mPopulateExpr->createWithEdges(driver, &callback);

  if (not expr_handle.getExpr()) {
    // If NULL, we have an invalid expr.
    return false;
  }

  // Create the identity expression for this node.
  IdentitySynthCallback identity_callback(mPopulateExpr);
  ESExprHndl identity_expr_handle = mPopulateExpr->createWithEdges(driver, &identity_callback);
  if (not identity_expr_handle.getExpr()) {
    // If NULL, we have an invalid expr.
    return false;
  }

  if (expr_handle.getExpr()==identity_expr_handle.getExpr()) {
    // We were not able to expand past the identifier. Expression
    // creation appeared successful, but in truth was incomplete.
    return false;
  }

  NUNet * cached_net = mFunctionCache->query(expr_handle);
  if (not cached_net) {
    // Cache it if it's not already available.
    mFunctionCache->add(expr_handle, driven_net);
    return false;
  }

  // Otherwise, we have a potential alias.
  return addAlias(cached_net, driven_net, NULL);
}

bool AssignAlias::addAlias(NUNet * fanin_net, 
                           NUNet * driven_net,
                           FLNode * driver)
{
  // Determine if the nets are suitable
  if (not testNetPair(fanin_net, driven_net)) {
    return false;
  }

  /*
   * Make sure that any blocks which either of the nets are def'd do not use the
   * other net.  This is a conservative analysis, but the issue we want to avoid is
   * where the wrong def of a net reaches a use due to aliasing:
   *   always
   *   begin
   *    if (sel)
   *	    begin
   *	       a = c;
   *	       e = b & f;
   *	    end
   *    else
   *      begin
   *	       a = d;
   *	       e = b & g;
   *	    end
   *   end
   *   always b = a;
   * Here, we cannot simply alias 'a' and 'b' together and substitute one for the other,
   * because then 'e' would see the wrong value.
   */
  if (not testUseDefRelationship(fanin_net, driven_net, 0) or
      not testUseDefRelationship(driven_net, fanin_net, driver)) {
    return false;
  }

  // test/alias/resynth.v -- avoid substituting for nets that are def'd,
  // used, and then re-def'd.  The re-def'd definition is not a good
  // candidate for substitution into the first use.  SSA would be nice.
  if (mDefAfterUse->redefinedAfterUse(fanin_net) or
      mDefAfterUse->redefinedAfterUse(driven_net))
  {
    return false;
  }

  return mAliasManager->rememberAlias(fanin_net, driven_net, driver);
}

NUNet* AssignAlias::testAssign(NUAssign *assign)
{
  NULvalue *lvalue = assign->getLvalue();
  if (not lvalue->isWholeIdentifier()) {
    return 0;
  }

  NUExpr *expr = assign->getRvalue();
  if (not expr->isWholeIdentifier()) {
    return 0;
  }

  return expr->getWholeIdentifier();
}


NUNet* AssignAlias::testAlways(NUAlwaysBlock *always, FLNode *driver)
{
  // We allow two different cases.
  //
  // Case 1:
  // The driver inside the always block is an assign, and the drivers of
  // the rhs net are within the always block:
  // always ...
  //   begin
  //     if (sel) b = c; else b = d;
  //     a = b;
  //   end
  //
  // Here, we can alias a and b, as long as the only driver for b is within
  // the always block.
  // It does not matter if the always block is sequential or combinational.
  //
  //
  // Case 2:
  // The driver inside the always block is an assign, and the drivers of
  // the rhs net are all outside the always block:
  // always ...
  //   begin
  //     a = b;
  //   end
  // assign b = sel ? c : d;
  //
  // For this case, the always block must be combinational.
  //

  // Drill down through block nesting
  FLNode* nested_driver = driver;
  NUUseDefNode* nested_driving_node = nested_driver->getUseDefNode();
  while (nested_driver->hasNestedBlock() and (nested_driver->numNestedFlows() == 1) and
         ((nested_driving_node->getType() == eNUBlock) or
          (nested_driving_node->getType() == eNUAlwaysBlock))) {
    nested_driver = nested_driver->getSingleNested();
    nested_driving_node = nested_driver->getUseDefNode();
  }

  NUAssign *assign = dynamic_cast<NUAssign*>(nested_driving_node);
  if (not assign) {
    return 0;
  }
  NUNet *fanin_net = testAssign(assign);
  if (not fanin_net) {
    return 0;
  }

  // Check that the driven net is only defined once within this always
  // block. We want to avoid aliasing in situations like:
  //   always @(..) begin
  //     a = (in1 & in2);
  //     out1 = (a & out2);
  //     a = b;
  //   end
  // Here, aliasing 'a' and 'b' will result in:
  //   always @(..) begin
  //     b = (in1 & in2);
  //     out1 = (b & out2);
  //   end
  // The value of 'b' has changed.
  bool single_definition = testSingleDefinition(driver->getDefNet(), always);
  if (not single_definition) {
    return 0;
  }

  // Now see if this is one of the valid cases we will handle.
  // If there are no drivers of fanin net, then we can alias.
  NUNet::DriverLoop loop = fanin_net->loopContinuousDrivers();
  if (loop.atEnd()) {
    return fanin_net;
  }

  // If there is more than one driver, it must be Case 2 for us to alias.
  loop = fanin_net->loopContinuousDrivers();
  ++loop;
  if (not loop.atEnd()) {
    return testValidCombAlways(fanin_net, always);
  }

  // Otherwise, there is only one driver.
  // If the driver is this always block, its Case 1.  Otherwise its Case 2.
  FLNode * fanin_driver = fanin_net->getContinuousDriver();
  NUUseDefNode *fanin_node = fanin_driver->getUseDefNode();
  if (fanin_node == always) {
    return testValidSingleAlways(fanin_net, fanin_driver, nested_driver);
  } else {
    return testValidCombAlways(fanin_net, always);
  }
}


NUNet *AssignAlias::testValidCombAlways(NUNet *fanin_net, NUAlwaysBlock *always)
{
  if (always->isSequential()) {
    return 0;
  }

  for (NUNet::DriverLoop loop = fanin_net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    NUUseDefNode *node = (*loop)->getUseDefNode();
    if (node == always) {
      return 0;
    }
  }

  return fanin_net;
}


//! Design walk callback
class SingleDefinitionCallback : public NUDesignCallback
{
public:
  SingleDefinitionCallback(NUNetRefFactory * factory) :
    mNetRefFactory(factory)
  { }

  ~SingleDefinitionCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eSkip; }

  //! Walk through declaration scopes. There could be always blocks inside them.
  Status operator()(Phase /*phase*/, NUNamedDeclarationScope * /*node*/) { return eNormal; }

  //! Walk through always blocks.
  Status operator()(Phase /*phase*/, NUStructuredProc * /*node*/) { return eNormal; }

  //! Walk through blocks; different from all other stmt types.
  Status operator()(Phase /*phase*/, NUBlock * /*node*/) { return eNormal; }

  //! Search for definitions of our def net; stop the walk if multiple definitions are found.
  Status operator()(Phase phase, NUStmt * node) {
    if (phase!=ePre) {
      return eNormal; // only walk each block once.
    }

    NUNetSet defs;
    node->getBlockingDefs(&defs);

    NUNetSet my_multiple_definitions;
    set_intersection(defs,mSingleDefinitions,&my_multiple_definitions);
    set_intersection(defs,mMultipleDefinitions,&my_multiple_definitions);

    NUNetSet remaining_single_definitions;
    mSingleDefinitions.insert(defs.begin(),defs.end());
    set_difference(mSingleDefinitions,my_multiple_definitions,&remaining_single_definitions);

    mSingleDefinitions = remaining_single_definitions;

    return eSkip;
  }

private:
  NUNetRefFactory * mNetRefFactory;

  //! Set of nets with multiple definitions in the current context.
  NUNetSet mMultipleDefinitions;

public:
  //! Set of nets with single definitions in the current context.
  NUNetSet mSingleDefinitions;
};


bool AssignAlias::testSingleDefinition(NUNet * net, NUAlwaysBlock * always)
{
  DefinitionMap::iterator location = mSingleDefinitionMap.find(always);
  if (location == mSingleDefinitionMap.end()) {
    SingleDefinitionCallback callback(mNetRefFactory);
    NUDesignWalker walker(callback, false);
    NUDesignCallback::Status status = walker.alwaysBlock(always);
    NU_ASSERT(status==NUDesignCallback::eNormal,always);

    NetSet & single_definitions = mSingleDefinitionMap[always];
    for (NUNetSet::iterator iter = callback.mSingleDefinitions.begin();
         iter != callback.mSingleDefinitions.end();
         ++iter) {
      single_definitions.insert(*iter);
    }

    return (single_definitions.find(net) != single_definitions.end());
  } else {
    NetSet & single_definitions = location->second;
    return (single_definitions.find(net) != single_definitions.end());
  }
}


NUNet *AssignAlias::testValidSingleAlways(NUNet *fanin_net,
                                          FLNode *always_driver,
                                          FLNode *assign_driver)
{
  FLNodeSet internal_always_drivers;

  // handle simple block nesting
  FLNode * nested_driver = always_driver;
  NUUseDefNode * nested_driving_node = nested_driver->getUseDefNode();
  while (nested_driver->hasNestedBlock() and
         (nested_driver->numNestedFlows() == 1) and
         ((nested_driving_node->getType() == eNUBlock) or
          (nested_driving_node->getType() == eNUAlwaysBlock))) {
    nested_driver = nested_driver->getSingleNested();
    nested_driving_node = nested_driver->getUseDefNode();
  }

  if (nested_driving_node->getType() == eNUBlock) {
    // Live drivers are nested under nested_driver
    for (FLNodeLoop loop = nested_driver->loopNested();
         not loop.atEnd();
         ++loop) {
      FLNode* nested = *loop;
      NU_ASSERT(nested->getDefNet() == fanin_net, nested->getDefNet());
      internal_always_drivers.insert(nested);
    }
  } else {
    // nested_driver is the internal driver
    internal_always_drivers.insert(nested_driver);
  }  

  FLNodeSet assign_fanin;
  for (FLNodeLoop loop = assign_driver->loopFanin();
       not loop.atEnd();
       ++loop) {
    NUNet* driver_fanin = (*loop)->getDefNet();
    if ( driver_fanin->isControlFlowNet() ){
      return 0; // for now we cannot alias this pair a $stop/$finish
                // is in the way.  This function needs to be rewritten
                // to support aliasing when $stop/$finish exist
    }
    NU_ASSERT(driver_fanin == fanin_net, driver_fanin);
    assign_fanin.insert(*loop);
  }

  if (assign_fanin == internal_always_drivers) {
    return fanin_net;
  } else {
    return 0;
  }

  return 0; // work around bug in gcc-2.95 (see bug3993)
}


NUNet* AssignAlias::testDriver(FLNode *driver)
{
  NUUseDefNode *node = driver->getUseDefNode();
  if (not node) {
    return 0;
  }

  NUContAssign *assign = dynamic_cast<NUContAssign*>(node);
  if (assign) {
    return testAssign(assign);
  }

  NUAlwaysBlock *always = dynamic_cast<NUAlwaysBlock*>(node);
  if (always) {
    return testAlways(always, driver);
  }

  return 0;
}


bool AssignAlias::testNetPair(NUNet *driver_net, NUNet *fanout_net)
{
  // Do not alias if these nets are already aliased together
  if (mAliasManager->getCurrentMaster(driver_net) == mAliasManager->getCurrentMaster(fanout_net)) {
    return false;
  }

  if (not testNetFlags(driver_net, fanout_net)) {
    return false;
  }

  if (not testScopeConsistency(driver_net, fanout_net)) {
    return false;
  }

  if (driver_net->is2DAnything() and fanout_net->is2DAnything()) {
    NUMemoryNet * driver_mem_net = driver_net->getMemoryNet();
    NUMemoryNet * fanout_mem_net = fanout_net->getMemoryNet();

    // If we have memories, alias only if their ranges are exact. 
    // This may need relaxing in the future.
    return ( ((*driver_mem_net->getWidthRange())==(*fanout_mem_net->getWidthRange())) and
             ((*driver_mem_net->getDepthRange())==(*fanout_mem_net->getDepthRange())) );
  }
  else if (driver_net->is2DAnything() or fanout_net->is2DAnything()) {
    return false; // memory and non-memory candidates.
  } else if ((driver_net->isBitNet() != fanout_net->isBitNet()) or
             (driver_net->isVectorNet() != fanout_net->isVectorNet())) {
    // Do not alias vectors and scalars; out of range issues.  bug 5940
    return false;
  } else {
    // Make sure the nets have the same size.
    return (driver_net->getBitSize() == fanout_net->getBitSize());
  }
}


// This should probably be a deeper analysis -- see test/alias/resynth.v
// If a real transitive closure is done then the pessimistic DefAfterUse
// analysis can be eliminated.
bool AssignAlias::testUseDefRelationship(NUNet *net1, NUNet *net2, FLNode *exclude_driver)
{
  NUNetSet net1_aliases;
  NUNetSet net2_aliases;

  // Check for illegal use/def relationships between all pending
  // aliases for both nets. This helps prevent aliasing in situations like:
  //
  // assign s1_data_hold = s1_data;
  // assign s2_data_hold = s2_data;
  // always @(posedge clk) begin
  //     s1_data = data;
  //     s2_data = s1_data_hold;
  //     s3_data = s2_data_hold;
  // end
  // assign s2_out = s2_data;
  // assign s3_out = s3_data;
  //
  // Here, we can legally alias the following pairs:
  //     (s2_out, s2_data), (s3_out, s3_data)
  //
  // If s2_data_hold is also aliased with s2_data, we end up
  // propagating information from s1_data_hold to s3_out in one cycle,
  // which is incorrect. 

  mAliasManager->getCurrentAliases(net1,&net1_aliases);
  mAliasManager->getCurrentAliases(net2,&net2_aliases);

  // Set used to avoid re-checking the same use-def node over and over.
  NUUseDefSet checked_nodes;

  for (NUNetSet::iterator iter1 = net1_aliases.begin();
       iter1 != net1_aliases.end();
       ++iter1) {
    NUNet * alias1 = *iter1;

    for (NUNet::DriverLoop loop = alias1->loopContinuousDrivers();
         not loop.atEnd();
         ++loop) {
      FLNode *driver = *loop;
      if (driver == exclude_driver) {
        continue;
      }
      NUUseDefNode *node = driver->getUseDefNode();
      if (not node) {
        continue;
      }

      // Check each use-def node only once.
      if (checked_nodes.find(node)!=checked_nodes.end()) {
        continue;
      }
      checked_nodes.insert(node);

      // Get all the uses from the block and check in bulk. This is
      // better than re-processing the internal NUNetRefMultiMap over
      // and over, though it does mean constructing a temporary set.
      NUNetSet uses;
      node->getUses(&uses);
      if (set_has_intersection(uses,net2_aliases)) {
        return false;
      }
    }

  }

  return true;
}


bool AssignAlias::testNetFlags(NUNet *net1, NUNet *net2)
{
  // For now, do not alias any sort of deposit or observable together
  // We do not need to check pending aliases as we do not allow _any_
  // protected nets through.
  if (net1->isProtected(mIODB) or net2->isProtected(mIODB)) {
    return false;
  }

  // For now, do not alias ports together
  bool port1 = mAliasManager->queryAliases(net1, &NUNet::isPort);
  bool port2 = mAliasManager->queryAliases(net2, &NUNet::isPort);
  if (port1 and port2) {
    return false;
  }

  // For now, don't substitute things with different signs
  if (net1->isSigned () != net2->isSigned ())
    return false;

  // In the new sizing methodology, we seem to have trouble with
  // an assignment alias in test/maxsim/xtor_clocks, on
  // s_grant_slave and t_grant_slave, one of which is an
  // unsigned subrange, and the other is a signed subrange.  This
  // leads to a codegen error passing a UInt32& to a
  // function that expects an SInt32&.
  if (((net1->isSignedSubrange() != net2->isSignedSubrange()) ||
       (net1->isUnsignedSubrange() != net2->isUnsignedSubrange())))
  {
    return false;
  }

  // Make sure that the default values do not conflict
  bool net1_trireg = mAliasManager->queryAliases(net1, &NUNet::isTrireg);
  bool net1_default_0 = ( mAliasManager->queryAliases(net1, &NUNet::isDefault0) );
  bool net1_default_1 = ( mAliasManager->queryAliases(net1, &NUNet::isDefault1) );

  bool net2_trireg = mAliasManager->queryAliases(net2, &NUNet::isTrireg);
  bool net2_default_0 = ( mAliasManager->queryAliases(net2, &NUNet::isDefault0) );
  bool net2_default_1 = ( mAliasManager->queryAliases(net2, &NUNet::isDefault1) );

  if (// default mismatch
      (net1_default_0 and net2_default_1) or
      // default mismatch
      (net2_default_0 and net1_default_1) or
      // net1 is trireg and net2 has default
      (net1_trireg and (net2_default_1 or net2_default_0)) or
      // net2 is trireg and net1 has default
      (net2_trireg and (net1_default_1 or net1_default_0)) or
      // trireg declaration does not match.
      (net1_trireg != net2_trireg)) {
    return false;
  }

  // A pull on net1(fanin) that does not exist on net2(fanout) will cause a
  // simulation mismatch, so don't do the alias -- let RENetWarning issue the
  // alert for Z conflicts
  if (mContAssignsOnly) {
    if ((net1_default_0 and not net2_default_0) or
        (net1_default_1 and not net2_default_1))
    {
      return false;
    }

    // For now be very pessimistic with wand and wor
    bool net1_wand =  ( mAliasManager->queryAliases(net1, &NUNet::isWand) );
    bool net2_wand =  ( mAliasManager->queryAliases(net2, &NUNet::isWand) );
    bool net1_wor =   ( mAliasManager->queryAliases(net1, &NUNet::isWor) );
    bool net2_wor =   ( mAliasManager->queryAliases(net2, &NUNet::isWor) );

    if ((net1_wand != net2_wand) or (net1_wor != net2_wor)) {
      return false;
    }
  }

  // any aliased temporaries must have the same scope.

  return true;
}

bool AssignAlias::testScopeConsistency(NUNet * net1, NUNet * net2)
{
  // base our tests off the masters for each net.
  NUNet * master1 = mAliasManager->getCurrentMaster(net1);
  NUNet * master2 = mAliasManager->getCurrentMaster(net2);

  NUScope * scope1 = master1->getScope();
  NUScope * scope2 = master2->getScope();

  if ( (scope1->getScopeType() != NUScope::eModule) and
       (scope2->getScopeType() != NUScope::eModule) and
       (scope1 != scope2) ) {
    return false;
  }

  return true;
}


//! Walk all the statements within always blocks, looking for any stmt which uses any part the def_net. 
class SequentialReadCallback : public NUDesignCallback
{
public:
  SequentialReadCallback(NUNetRefFactory * factory) :
    mNetRefFactory(factory),
    mSequentialReads(factory)
  {}

  ~SequentialReadCallback() {}

  Status operator()(Phase /*phase*/, NUBase * /*node*/) { return eSkip; }

  //! There could be always blocks inside named declaration scopes.
  Status operator()(Phase, NUNamedDeclarationScope*) { return eNormal; }

  //! Walk through always blocks.
  Status operator()(Phase /*phase*/, NUStructuredProc * /*node*/) { return eNormal; }

  //! Walk through blocks.
  Status operator()(Phase /*phase*/, NUBlock * /*node*/) { return eNormal; }

  //! Search for uses of our def net; stop the walk uses are found.
  Status operator()(Phase phase, NUStmt * node) {
    if (phase!=ePre) {
      return eNormal; // only walk each block once.
    }

    node->getBlockingUses(&mSequentialReads);
    return eNormal;
  }

private:
  NUNetRefFactory * mNetRefFactory;

public:
  NUNetRefSet mSequentialReads;
};


bool AssignAlias::testSequentialRead(NUNet * net, NUAlwaysBlock * always) 
{
  DefinitionMap::iterator location = mSequentialReadMap.find(always);
  if (location == mSequentialReadMap.end()) {
    SequentialReadCallback callback(mNetRefFactory);
    NUDesignWalker walker(callback, false);
    NUDesignCallback::Status status = walker.alwaysBlock(always);
    NU_ASSERT(status==NUDesignCallback::eNormal,always);

    NetSet & sequential_reads = mSequentialReadMap[always];
    for (NUNetRefSet::iterator iter = callback.mSequentialReads.begin();
         iter != callback.mSequentialReads.end();
         ++iter) {
      NUNetRefHdl net_ref = (*iter);
      sequential_reads.insert(net_ref->getNet());
    }
    return (sequential_reads.find(net) != sequential_reads.end());
  } else {
    NetSet & sequential_reads = location->second;
    return (sequential_reads.find(net) != sequential_reads.end());
  }
}


void AssignAliasStatistics::print() const 
{
  UtIO::cout() << "AssignAlias summary:" << UtIO::endl;
  UtIO::cout() << "    Assign aliases:     " << mAssignAliases << UtIO::endl;
  UtIO::cout() << "    Functional aliases: " << mFunctionalAliases << UtIO::endl;
  UtIO::cout() << "    Total:              " << (mAssignAliases+mFunctionalAliases) << UtIO::endl;
}
