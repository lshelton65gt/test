// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/MarkSweepUnusedTasks.h"
#include "util/UtHashSet.h"
#include "symtab/STSymbolTableNode.h"

#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUHierRef.h"
#include "util/CbuildMsgContext.h"

//! Remember all called tasks.
class MarkSweepUnusedTasks::MarkCallback : public NUDesignCallback
{
public:
  MarkCallback(MsgContext * msg_ctx,
               TaskSet* usedTasks,
               TaskSet* activeTasks,
               NUTaskEnableVector* activeStack,
               bool allow_recursion) :
   mMsgContext(msg_ctx),
   mUsedTasks(usedTasks),
   mActiveTasks(activeTasks),
   mActiveTaskEnableStack(activeStack),
   mAllowRecursion(allow_recursion)
  {
    mTaskEnableDepth = 0;
    mRecursionFound = false;
  }

  virtual ~MarkCallback() {
    INFO_ASSERT(mTaskEnableDepth == 0, "Task recursion imbalance");
  }

  Status operator() (Phase /* phase */, NUBase * /* node */)
  {
    return eNormal;
  }

  /* \brief for each task_enable, save the info about the task being enabled
   */
  virtual Status operator()(Phase phase, NUTaskEnable *task_enable)
  {
    Status status;
    if (phase == ePre)
    {
      status = visitTaskEnablePre(task_enable);
    }
    else {
      status = visitTaskEnablePost(task_enable);
    }
    return status;
  }

  //! Don't process tasks directly from the module -- only through the task enables
  virtual Status operator()(Phase, NUTask*) {
    return (mTaskEnableDepth == 0)? eSkip: eNormal;
  }

  bool recursiveTaskChainFound() const {return mRecursionFound;}

private:
  //! reports a recursion of task enables
  void reportTaskRecursion( NUTaskEnable* task_enable )
  {
    NUTask* task = task_enable->getTask();
    if (! mAllowRecursion) {
      mMsgContext->RERecursiveTaskCalls(task_enable,
                                        mActiveTaskEnableStack->size());
    }

    bool first = true;
    SInt32 stackSize = mActiveTaskEnableStack->size();
    SInt32 cycleSize = 1;
    for ( int i = stackSize-1; i > 0; --i) {
      NUTaskEnable* chain_task_enable = (*mActiveTaskEnableStack)[i-1];
      NUTask* chain_task = chain_task_enable->getTask();
      chain_task->putRecursive(true);
      cycleSize++;
      if ( not first && ( chain_task == task )) break;
      first=false;
    }
    first = true;
    for ( int i = stackSize; i > (stackSize-cycleSize); --i) {
      NUTaskEnable* chain_task_enable = (*mActiveTaskEnableStack)[i-1];
      NUTask* chain_task = chain_task_enable->getTask();
      chain_task->putRecursive(true);
      if (! mAllowRecursion) {
        mMsgContext->RERecursiveTaskEnable(chain_task_enable,
                                           chain_task->getName()->str(),
                                           stackSize-i+1, cycleSize);
      }
      if ( not first && ( chain_task == task )) break;
      first = false;
    }
  }

  /* \brief store information as we start to process a task_enable
   * 
   * log information about the task(s) linked to the current
   * task enable.  mActiveTaskEnableStack and mActiveTasks are
   * respectively a stack and a set of the tasks that are currently
   * enabled.  mUsedTasks is a set of all the tasks encountered during
   * the design traversal.
   * 
   */
  Status visitTaskEnablePre(NUTaskEnable* task_enable )
  {
    ++mTaskEnableDepth;
    mActiveTaskEnableStack->push_back(task_enable);
    if (task_enable->isHierRef()) {
      NUTaskEnableHierRef * enable_hier_ref = dynamic_cast<NUTaskEnableHierRef*>(task_enable);
      // loop all resolutions in case one or more of them causes a
      // recursive loop -- we report all.
      NUTaskHierRef* hier_ref = enable_hier_ref->getHierRef();
      bool found_a_recursive_loop = false;
      NUTaskVector tasks_that_caused_recursion;
      for (NUTaskHierRef::TaskVectorLoop tv = hier_ref->loopResolutions(); ! tv.atEnd(); ++tv)
      {
        NUTask* task = *tv;
        Status temp_status = logAnEnabledTask(task);
        if ( temp_status == eSkip ){
          // this task caused a cycle, it has been reported
          found_a_recursive_loop = true;
          tasks_that_caused_recursion.push_back(task);
        }
      }
      if ( found_a_recursive_loop ){
        mRecursionFound=true;

        // terminate recursion here, we will return eSkip.
        // Before returning we call post processing on this task_enable 
        // because design walker will not do this for us.
        visitTaskEnablePost( task_enable );

        // re-insert in the mActiveTasks set, the task(s) that caused
        // the recursive loops. Each was in the mActiveTasks set and in
        // two  places in the stack. The call to visitTaskEnablePost
        // removed each from mActiveTasks set and from the top of the stack.
        // But since each of the tasks still exists in the stack
        // somewhere, it needs to be added back to mActiveTasks.
        
        for (NUTaskVectorIter tv = tasks_that_caused_recursion.begin();
             tv != tasks_that_caused_recursion.end(); ++tv)
        {
          NUTask* task = *tv;
          mActiveTasks->insert(task);
        }
        return eSkip; 
      }
             
    } else {
      NUTask* task = task_enable->getTask();

      Status status = logAnEnabledTask( task );
      if ( status == eSkip ){
        // a recursion was found, we will return eSkip so call post
        // processing on this task enable before returning because the
        // design walker will not do this for us.
        visitTaskEnablePost( task_enable );

        // re-insert in the mActiveTasks set, the task that caused
        // the recursive loops. It was in the mActiveTasks set and in
        // two  places in the stack. The call to visitTaskEnablePost
        // removed it from mActiveTasks set and from the top of the stack.
        // But since the task still exists in the stack
        // somewhere, it needs to be added back to mActiveTasks.
        mActiveTasks->insert(task);
      }
      return status;
    }
    return eNormal;
  }

  //! add task to the tracking sets, returns eSkip if a recursion is found, else eNormal.
  Status logAnEnabledTask(NUTask* task)
  {
    mUsedTasks->insert(task);
    if ( not mActiveTasks->insertWithCheck(task) ){
      NUTaskEnable* task_enable = mActiveTaskEnableStack->back();
      reportTaskRecursion( task_enable );
      mRecursionFound=true;
      // Terminate recursion here because recursive task enables are not currently supported
      return eSkip;
    }
    return eNormal;
  }

  Status visitTaskEnablePost(NUTaskEnable* task_enable )
  {
    NU_ASSERT(mTaskEnableDepth > 0, task_enable);
    --mTaskEnableDepth;

    NU_ASSERT(mActiveTaskEnableStack->size(), task_enable);
    mActiveTaskEnableStack->pop_back();

    if (task_enable->isHierRef()) {
      NUTaskEnableHierRef * enable_hier_ref = dynamic_cast<NUTaskEnableHierRef*>(task_enable);
      // loop all resolutions and remove all from the activeTasks
      NUTaskHierRef* hier_ref = enable_hier_ref->getHierRef();
      for (NUTaskHierRef::TaskVectorLoop tv = hier_ref->loopResolutions(); 
           ! tv.atEnd(); ++tv)
      {
        NUTask* task = *tv;
        mActiveTasks->erase(task);
      }
             
    } else {
      NUTask* task = task_enable->getTask();
      mActiveTasks->erase(task);
    }
    return eNormal;
  }

  MsgContext * mMsgContext;
  TaskSet* mUsedTasks;
  TaskSet* mActiveTasks;        // a hash set of the active tasks
  NUTaskEnableVector* mActiveTaskEnableStack; // a stack to keep track of the ordering of task enables in the active set
  UInt32 mTaskEnableDepth;
  bool mRecursionFound;
  bool mAllowRecursion;
};

//! Eliminate all tasks not discovered during the mark pass.
class MarkSweepUnusedTasks::SweepCallback : public NUDesignCallback
{
public:
  SweepCallback(MsgContext * msg_ctx,
                TaskSet* usedTasks,
                bool report_removals,
                bool allow_recursion
                ) :
    mMsgContext(msg_ctx),
    mUsedTasks(usedTasks),
    mReportRemovals(report_removals),
    mAllowRecursion(allow_recursion),
    mRemoved(false)
  {
  }

  virtual ~SweepCallback() {}

  Status operator() (Phase /* phase */, NUBase * /* node */)
  {
    return eNormal;
  }

  //! Walk all the tasks in a module and eliminate the uncalled ones.
  virtual Status operator()(Phase phase, NUModule *module)
  {
    if (phase == ePre)
    {
      NUTaskVector deadTasks;
      for (NUTaskLoop t = module->loopTasks();
           ! t.atEnd(); ++t)
      {
        NUTask* task = *t;
        if (mUsedTasks->find(task) == mUsedTasks->end())
          deadTasks.push_back(task);
      }

      for (UInt32 i = 0, n = deadTasks.size(); i < n; ++i) {
        NUTask* task = deadTasks[i];
        const SourceLocator& loc = task->getLoc();
        if (mReportRemovals) {
          const char* name = task->getName()->str();
          mMsgContext->REUnusedTask(&loc, name);
        }

        module->removeTask(task);
        mRemoved = true;
        delete task;
      }
    }

    return eNormal;
  }

  bool wereTasksRemoved() const {return mRemoved;}

private:
  MsgContext * mMsgContext;
  TaskSet* mUsedTasks;
  bool mReportRemovals;
  bool mAllowRecursion;
  bool mRemoved;
};

MarkSweepUnusedTasks::MarkSweepUnusedTasks(MsgContext* msgContext,
                                           bool verbose,
                                           bool allow_recursion) :
  mMsgContext(msgContext),
  mReportRemovals(verbose),
  mAllowRecursion(allow_recursion)
{
  mUsedTasks = new TaskSet;
  mActiveTasks = new TaskSet;
  mActiveTaskEnableStack = new NUTaskEnableVector;
}

MarkSweepUnusedTasks::~MarkSweepUnusedTasks()
{
  delete mActiveTaskEnableStack;
  delete mActiveTasks;
  delete mUsedTasks;
}

bool MarkSweepUnusedTasks::walkAndRemove(NUDesign* design, bool* recursion_found)
{
  if ( mark(design) ) {
    // early exit if recursion found
    *recursion_found = true;
    return false;
  }
  return sweep(design);
}

bool MarkSweepUnusedTasks::mark(NUDesign* design)
{
  MarkCallback mcb(mMsgContext, mUsedTasks, mActiveTasks,
                   mActiveTaskEnableStack, mAllowRecursion);
  NUDesignWalker walker(mcb, false, false);
  walker.design(design);
  return false; // mcb.recursiveTaskChainFound();
}

bool MarkSweepUnusedTasks::sweep(NUDesign* design)
{
  SweepCallback sweep_callback(mMsgContext, mUsedTasks, mReportRemovals,
                               mAllowRecursion);
  NUDesignWalker walker(sweep_callback, false, false);
  walker.design(design);
  return sweep_callback.wereTasksRemoved();
}

