// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/SplitFlow.h"
#include "reduce/SplitFlowFactory.h"

#include "localflow/UnelabFlow.h"
#include "localflow/Elaborate.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUInstanceWalker.h"

#include "util/ArgProc.h"
#include "util/Stats.h"

#include "reduce/RemoveDeadCode.h"

#include "reduce/CleanElabFlow.h"
#include "reduce/SanityUnelab.h"
#include "reduce/SanityElab.h"
#include "reduce/DeadNets.h"

#include "compiler_driver/CarbonContext.h"


void SplitFlow::repair(NUDesign * design,
                       NUAlwaysBlockSet & replacement_blocks,
                       FLNodeSet & boundary_flow,
                       FLNodeConstElabMap & flow_to_elab,
                       FLNodeElabSet* deleted_flow_elabs,
                       ClockAndDriver * clock_and_driver,
                       bool create_declarations,
                       Stats* stats)
{
  UtStatsInterval statsInt(true, stats);
  mDesign = design;

  mCreateDeclarations = create_declarations;

  completeBoundary(replacement_blocks,boundary_flow);
  statsInt.printIntervalStatistics("SR Bound");

  completeElabFlowMap(boundary_flow,flow_to_elab);
  statsInt.printIntervalStatistics("SR ElabFlow");

  findMultipleDrivers(boundary_flow,
                      flow_to_elab);
  statsInt.printIntervalStatistics("SR MultDrive");

  fixupFlow(replacement_blocks,
            boundary_flow,
            flow_to_elab,
            clock_and_driver);
  statsInt.printIntervalStatistics("SR Fix");

  cleanupFlow(design, deleted_flow_elabs, stats);
  statsInt.printIntervalStatistics("SR Clean");

  sanityChecks(design, stats);
  statsInt.printIntervalStatistics("SR Sanity");

  // GC on net-ref factory.
  mNetRefFactory->cleanup();

  // Release memory.
  CarbonMem::releaseBlocks();
  statsInt.printIntervalStatistics("SR Release");
}


void SplitFlow::cleanupFlow(NUDesign * design, FLNodeElabSet * deleted_flow_elabs,
                            Stats* stats)
{
  static bool verboseCleanup = mArgs->getBoolValue(CRYPT("-verboseCleanup"));
  UtStatsInterval statsInt(true, stats);
  
  // remove dead elab flow.
  CleanElabFlow cleanup(mFlowElabFactory,
                        mSymtab,
                        verboseCleanup);
  cleanup.design(design,deleted_flow_elabs);
  statsInt.printIntervalStatistics("SRC FlowElab");

  // remove any dead code from affected modules.
  DeadNetCallback dead_net_callback(mSymtab);
  RemoveDeadCode unelabCleanup(mNetRefFactory, mFlowFactory, mIODB, mArgs,
                               mMsgContext, 
                               false, // only start at important nets.
                               true,  // always delete dead code.
                               false, // do not delete module instances
                               verboseCleanup);
  unelabCleanup.design(design, &dead_net_callback);
  statsInt.printIntervalStatistics("SRC Code");
}

void SplitFlow::sanityChecks(NUDesign * design, Stats* stats)
{
  static bool verboseSanity = mArgs->getBoolValue(CRYPT("-verboseSanity"));
  UtStatsInterval statsInt(true, stats);

  // unelaborated sanity check.
  {
    // do not perform the UD/Flow consistency check, because the flow
    // can become inconsistent with the UD after block merging -- see bug3959
    SanityUnelab sanity(mMsgContext, mNetRefFactory, mFlowFactory, 
                        false, verboseSanity);
    sanity.design(design);
    statsInt.printIntervalStatistics("SRS Unelab");
  }

  // elaborated sanity check.
  {
    // do not perform the UD/Flow consistency check, because the flow
    // can become inconsistent with the UD after block merging -- see bug3959
    SanityElab sanity(mMsgContext, mSymtab, mNetRefFactory, mFlowFactory, mFlowElabFactory, false, verboseSanity);
    sanity.design(design);
    statsInt.printIntervalStatistics("SRS Elab");
  }
} // void SplitFlow::sanityChecks


void SplitFlow::completeBoundary(NUAlwaysBlockSet & blocks,
                                 FLNodeSet & boundary)
{
  NUUseDefSet ud_blocks;
  // create a use-def set so we don't have to determine the type/cast
  // every use-def referenced by the factory (some may have already
  // been deleted).
  ud_blocks.insert(blocks.begin(),blocks.end());
  for ( FLNodeFactory::iterator iter = mFlowFactory->begin();
	iter != mFlowFactory->end();
	++iter ) {
    FLNode * flow = (*iter);
    NUUseDefNode * use_def = flow->getUseDefNode();
    if (ud_blocks.find(use_def) != ud_blocks.end()) {
      boundary.insert(flow);
    }
  }
}


void SplitFlow::completeElabFlowMap(FLNodeSet & boundary,
                                    FLNodeConstElabMap & flow_to_elab)
{
  FLNodeElab * flow_elab;
  for ( FLNodeElabFactory::FlowLoop iter = mFlowElabFactory->loopFlows();
	iter(&flow_elab);) {
    FLNode * flow = flow_elab->getFLNode();
    if (boundary.find(flow) != boundary.end()) {
      flow_to_elab[flow].insert(flow_elab);
    }
  }
}


void SplitFlow::findMultipleDrivers(FLNodeSet & flow_set,
                                    FLNodeConstElabMap & flow_to_elab)
{
  // Handle situations (like block merging) where the incoming flow
  // graph has multiple nodes driving the same net from a single
  // continuous usedef node.
  //
  // Rewrite the unelaborated graph to have one driver per
  // (net,usedef) pair. Rewrite the elaborated graph to have one
  // driver per (net,hier,usedef) triple.
  // 
  // 1. Choose one unelaborated flow per (net,usedef) pair as
  //    representative.
  //
  // 2. Augment the representative unelaborated flow with netref
  //    information from all other drivers sharing that (net,usedef).
  // 
  // 3. Choose one elaborated flow per (net,hier,usedef) triple as
  //    representative. 
  //
  // 4. Update the representative elaborated flow to reference the
  //    corresponding representative unelaborated flow. 
  //
  //    The incoming unelaborated flows will not always be live in the
  //    same instances. For this reason, we cannot blindly choose as
  //    elaborated representatives all the elaborated flow associated
  //    with the representative unelaborated flow. (bug3858)
  //
  // 5. Rewrite arcs involving non-representative flow; disconnect the
  //    losing flow as drivers.

  MultiDriverFlowLookup flow_lookup;

  MultiDriverFlowElabLookup flow_elab_lookup;

  for (FLNodeSet::iterator iter = flow_set.begin();
       iter != flow_set.end();
       ++iter) {
    FLNode * flow = (*iter);
    NUNet * net = flow->getDefNet();
    NUUseDefNode * use_def = flow->getUseDefNode();

    FLNode * representative_flow = flow;

    // ****************************************
    // Process unelaborated multi-drivers.

    MultiDriverNetNode net_node(net,use_def);
    MultiDriverFlowLookup::iterator location = flow_lookup.find(net_node);
    if (location == flow_lookup.end()) {
      // This is the first unelaborated flow for this (net,node) pair.
      // Save it as our representative unelaborated flow.
      flow_lookup.insert(MultiDriverFlowLookup::value_type(net_node,flow));
    } else {
      // We have previously seen an unelaborated flow for this (net,node) pair.
      representative_flow = location->second;

      // Augment the net-ref for the representative with our netref --
      // it now represents the driver for both parts.
      NUNetRefHdl representative_net_ref = representative_flow->getDefNetRef();
      NUNetRefHdl net_ref = flow->getDefNetRef();
      
      NUNetRefHdl merged_net_ref = mNetRefFactory->merge(net_ref,representative_net_ref);
      representative_flow->replaceDefNetRef(representative_net_ref,merged_net_ref);

      // Remember the rewriting relationship.
      mFlowTranslate[flow] = representative_flow;

      // Remember all fanin of non-representative.
      FLNode::FaninLoop fanin_loop = flow->loopFanin();
      representative_flow->connectFaninFromLoop(fanin_loop);
      fanin_loop = flow->loopEdgeFanin();
      representative_flow->connectEdgeFaninFromLoop(fanin_loop);

      // Remove this unelaborated flow node as an active driver.
      net->removeContinuousDriver(flow);
    }


    // ****************************************
    // Process elaborated multi-drivers.
    FLNodeElabSet & elab_set = flow_to_elab[ flow ];
    for ( FLNodeElabSet::iterator elabiter = elab_set.begin();
          elabiter != elab_set.end() ;
          ++elabiter ) {
      FLNodeElab * flow_elab = (*elabiter);
      STBranchNode * hier = flow_elab->getHier();

      MultiDriverNetHierNode net_hier_node(net,hier,use_def);
      MultiDriverFlowElabLookup::iterator elab_location = flow_elab_lookup.find(net_hier_node);
      if (elab_location == flow_elab_lookup.end()) {
        // This is the first elaborated flow for this (net,hier,node) triple.
        if (flow != representative_flow){ 
          // Make sure it references our representative unelaborated flow.
          flow_elab->replaceFLNode(flow, representative_flow);
          // Add it to the flow_to_elab for our representative.
          flow_to_elab[ representative_flow ].insert(flow_elab);
        }
        // Save it as our representative elaborated flow.
        flow_elab_lookup.insert(MultiDriverFlowElabLookup::value_type(net_hier_node,
                                                                      flow_elab));
      } else {
        // We have previously seen an elaborated flow for this (net,hier,node) triple.
        FLNodeElab * representative_flow_elab = elab_location->second;

        // Remember the rewriting relationship.
        mFlowElabTranslate[flow_elab] = representative_flow_elab;

        // Remember all fanin of non-representative.
        FLNodeElabLoop fanin_loop = flow_elab->loopFanin();
        representative_flow_elab->connectFaninFromLoop(fanin_loop);
        fanin_loop = flow_elab->loopEdgeFanin();
        representative_flow_elab->connectEdgeFaninFromLoop(fanin_loop);

        // Remove this elaborated flow node as an active driver.
        NUNetElab * net_elab = flow_elab->getDefNet();
        net_elab->removeContinuousDriver(flow_elab);
      }
    }
  }

  // There may be internal defs which were not continuously live prior
  // to the current transformation. As a result, they are not
  // represented by continuous drivers. Find these internal defs and
  // augment the def-net-refs for the boundary flow.
  //
  // See the declaration of addInternalDefsToFlow for more details.
  addInternalDefsToFlow(flow_set);

  // Eliminate the nodes which now have replacements from our boundary.
  removeMultiDriversFromBoundary(flow_set);

  // now, update the fanin of the flow we are about to process.
  updateFaninForMultiDrivers();
}


void SplitFlow::addInternalDefsToFlow(FLNodeSet & flow_set)
{
  // First, create a map from use-def to driving flow. We will process
  // all the flow associated with a given use-def at the same time.
  // This reduces the number of times we have to call getDefs().
  UtMap<NUUseDefNode*,FLNodeSet> use_def_flows;
  for (FLNodeSet::iterator iter = flow_set.begin();
       iter != flow_set.end();
       ++iter) {
    FLNode * flow = *iter;
    NUUseDefNode * node = flow->getUseDefNode();
    use_def_flows[node].insert(flow);
  }

  // For each use-def, update its flow with complete def information.
  for (UtMap<NUUseDefNode*,FLNodeSet>::iterator iter = use_def_flows.begin();
       iter != use_def_flows.end();
       ++iter) {
    NUUseDefNode * node = iter->first;
    FLNodeSet & flows = iter->second;

    NUNetRefSet defs(mNetRefFactory);
    node->getDefs(&defs);
    for (FLNodeSet::iterator fiter = flows.begin();
         fiter != flows.end();
         ++fiter) {
      FLNode * flow = *fiter;
      NUNetRefHdl net_ref = flow->getDefNetRef();
      NUNet * net = net_ref->getNet();
      NUNetRefHdl complete = mNetRefFactory->createNetRef(net);
      NUNetRefSet::iterator matching = defs.find(complete,&NUNetRef::overlapsSameNet);
      if (matching != defs.end()) {
        NUNetRefHdl defined = *matching;
        if (net_ref != defined) {
          // We expect only a superset of definitions.
          //
          // Bug 6466 documents a situations which triggers this
          // alert. This alert implies that there is some def (or
          // partial def) which has been eliminated from the current
          // block. Usually, block-merging has exposed some
          // optimization which eliminates some statements.
          //
          // This clause does not completely catch all situations.
          // Because we only check when there is a matching def, we
          // avoid displaying an alert when all defined bits have been
          // eliminated.
          //
          // We tried to catch the eliminated defs (below), but that
          // often fires for very understandable reasons (all
          // documented in bug 6466).
#ifdef CDB
          if (not defined->covers(*net_ref)) {
            UtString buf1, buf2;
            defined->compose(&buf1, NULL);
            net_ref->compose(&buf2, NULL);
            const SourceLocator& loc = flow->getUseDefNode()->getLoc();
            mMsgContext->REUnexpectedUD(&loc, buf1.c_str(), buf2.c_str());
          }
#endif
          flow->replaceDefNetRef(net_ref,defined);
        }
      } else {
        // No def which matches the net-ref predicted by the flow.
        // This is similar to the above alert, but checks for
        // complete, rather than partial def elimination.
        // 
        // This alert fires _very_ often. See comment #6 in bug 6466
        // for details about important and unimportant reasons this
        // alert could fire.
        //
        // UtString buf;
        // net_ref->compose(&buf, NULL);
        // const SourceLocator& loc = flow->getUseDefNode()->getLoc();
        // mMsgContext->REMissingUD(&loc, buf.c_str());
      }
    }
  }
}


void SplitFlow::removeMultiDriversFromBoundary(FLNodeSet & flow_set)
{
  for (FLNodeToFLNodeMap::iterator iter = mFlowTranslate.begin();
       iter != mFlowTranslate.end();
       ++iter) {
    FLNode * flow = iter->first;
    FLNodeSet::iterator location = flow_set.find(flow);
    flow_set.erase(location);
  }
}


void SplitFlow::updateFaninForMultiDrivers()
{
  if (not mFlowTranslate.empty()) {
    for ( FLNodeFactory::iterator iter = mFlowFactory->begin();
          iter != mFlowFactory->end();
          ++iter ) {
      FLNode * flow = (*iter);
      updateFanin(flow);
    }
  }

  if (not mFlowElabTranslate.empty()) {
    FLNodeElab * flow_elab = NULL;
    for ( FLNodeElabFactory::FlowLoop iter = mFlowElabFactory->loopFlows();
          iter(&flow_elab);) {
      updateElabFanin(flow_elab);
    }
  }
}


void SplitFlow::updateFanin(FLNode * flow)
{
  if (mFlowTranslate.empty()) {
    return;
  }

  if (dynamic_cast<FLNodeBound*>(flow)) {
    return;
  }

  FLNodeSet new_fanin;
  for (Iter<FLNode*> loop = flow->loopFanin();
       not loop.atEnd();
       ++loop) {
    FLNode * fanin = (*loop);
    FLNodeToFLNodeMap::iterator location = mFlowTranslate.find(fanin);
    if (location != mFlowTranslate.end()) {
      fanin = location->second;
    }
    new_fanin.insert(fanin);
  }
  flow->clearFanin();
  for (FLNodeSet::iterator iter = new_fanin.begin();
       iter != new_fanin.end();
       ++iter) {
    FLNode * fanin = (*iter);
    flow->connectFanin(fanin);
  }

  new_fanin.clear();
  for (Iter<FLNode*> loop = flow->loopEdgeFanin();
       not loop.atEnd();
       ++loop) {
    FLNode * fanin = (*loop);
    FLNodeToFLNodeMap::iterator location = mFlowTranslate.find(fanin);
    if (location != mFlowTranslate.end()) {
      fanin = location->second;
    }
    new_fanin.insert(fanin);
  }
  flow->clearEdgeFanin();
  for (FLNodeSet::iterator iter = new_fanin.begin();
       iter != new_fanin.end();
       ++iter) {
    FLNode * fanin = (*iter);
    flow->connectEdgeFanin(fanin);
  }
}


void SplitFlow::updateElabFanin(FLNodeElab * flow)
{
  if (mFlowTranslate.empty()) {
    return;
  }

  if (dynamic_cast<FLNodeBoundElab*>(flow)) {
    return;
  }

  if (flow->isEncapsulatedCycle())
  {
    // This is an FLNodeElabCycle, so its fanin is cached
    // in the NUCycle structure.  We just have to mark the
    // cache stale, so that it will be recomputed the next
    // time it is accessed.  Since the leaf nodes' fanin will
    // be updated in this routine, the correct fanin will be
    // computed when the cache is updated.
    NUCycle* cycle = flow->getCycle();
    cycle->invalidateFaninCache();
    return;
  }

  FLNodeElabSet new_fanin;
  for (Iter<FLNodeElab*> loop = flow->loopFanin();
       not loop.atEnd();
       ++loop) {
    FLNodeElab * fanin = (*loop);
    FLNodeElabToFLNodeElabMap::iterator location = mFlowElabTranslate.find(fanin);
    if (location != mFlowElabTranslate.end()) {
      fanin = location->second;
    }
    new_fanin.insert(fanin);
  }
  flow->clearFanin();
  for (FLNodeElabSet::iterator iter = new_fanin.begin();
       iter != new_fanin.end();
       ++iter) {
    FLNodeElab * fanin = (*iter);
    flow->connectFanin(fanin);
  }

  new_fanin.clear();
  for (Iter<FLNodeElab*> loop = flow->loopEdgeFanin();
       not loop.atEnd();
       ++loop) {
    FLNodeElab * fanin = (*loop);
    FLNodeElabToFLNodeElabMap::iterator location = mFlowElabTranslate.find(fanin);
    if (location != mFlowElabTranslate.end()) {
      fanin = location->second;
    }
    new_fanin.insert(fanin);
  }
  flow->clearEdgeFanin();
  for (FLNodeElabSet::iterator iter = new_fanin.begin();
       iter != new_fanin.end();
       ++iter) {
    FLNodeElab * fanin = (*iter);
    flow->connectEdgeFanin(fanin);
  }
}


void SplitFlow::fixupFlow(NUAlwaysBlockSet & replacement_blocks,
                          FLNodeSet & boundary_flow,
                          FLNodeConstElabMap & flow_to_elab,
                          ClockAndDriver * clock_and_driver)
{
  NUNetNodeToFlowMap       flow_lookup;
  FlowNetHierToFlowElabMap flow_elab_lookup;

  SplitFlowFactory     split_flow_factory(mFlowFactory, flow_lookup);
  SplitFlowElabFactory split_flow_elab_factory(mFlowElabFactory, flow_elab_lookup);

  setupFlowLookup(flow_lookup, boundary_flow);
  setupFlowElabLookup(flow_elab_lookup, boundary_flow, flow_to_elab);

  // create a per-module set of always blocks.
  ModuleCreatedAlways module_created_always;
  mapAlwaysToModule(replacement_blocks, module_created_always);

  doUnelaboration(&split_flow_factory,
		  module_created_always);

  doElaboration(&split_flow_factory,
		&split_flow_elab_factory,
                module_created_always,
		boundary_flow,
		flow_to_elab,
                clock_and_driver);

  // The fanin caches on cycles must be invalidated here, which will
  // trigger the fanin to be recomputed from the flow in the cycle
  // the next time it is needed.
  FLNodeElab* cycle_flow;
  for (FLNodeElabFactory::CycleLoop iter = mFlowElabFactory->loopCycleFlows();
       iter(&cycle_flow);) {
    NUCycle* cycle = cycle_flow->getCycle();
    FLN_ELAB_ASSERT(cycle, cycle_flow);
    cycle->invalidateFaninCache();
  }
}


void SplitFlow::mapAlwaysToModule(NUAlwaysBlockSet & replacement_blocks,
                                  ModuleCreatedAlways & module_created_always)
{
  for (NUAlwaysBlockSet::iterator iter = replacement_blocks.begin();
       iter != replacement_blocks.end();
       ++iter) {
    NUAlwaysBlock * always = (*iter);
    NUModule* module = const_cast<NUModule*>(always->findParentModule());
    module_created_always[module].insert(always);
  }
}


void SplitFlow::setupDriverLookup(NUNetNodeToFlowMap & flow_lookup,
                                  NUNet * net)
{
  for (NUNet::DriverLoop loop = net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    FLNode * flow = (*loop);
    NUNetNode net_node(flow->getDefNetRef(),flow->getUseDefNode());
    flow_lookup[net_node] = flow;
  }
}


void SplitFlow::setupDriverLookup(FlowNetHierToFlowElabMap & flow_lookup,
                                  NUNetElab * net)
{
  for (NUNetElab::DriverLoop loop = net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    FLNodeElab * flow = (*loop);
    FlowNetHier flow_net_hier(flow->getFLNode(),flow->getDefNet(),flow->getHier());
    flow_lookup[flow_net_hier] = flow;
  }
}


void SplitFlow::setupFlowLookup(NUNetNodeToFlowMap & flow_lookup,
                                FLNodeSet & boundary_flow)
{
  for ( FLNodeSet::iterator iter = boundary_flow.begin() ;
	iter != boundary_flow.end() ;
	++iter ) {
    FLNode * flow = (*iter);

    setupOneFlowLookup(flow_lookup, flow);
  }
}


void SplitFlow::setupOneFlowLookup(NUNetNodeToFlowMap & flow_lookup,
                                   FLNode * flow)
{
  NUUseDefNode * split_use_def = flow->getUseDefNode();
  NUAlwaysBlock * always_block = dynamic_cast<NUAlwaysBlock*>(split_use_def);
  NU_ASSERT(always_block, split_use_def);

  // add each of our fanin to our flow_lookup set -- don't allow
  // recreation of this flow.
  for (Iter<FLNode*> loop = flow->loopFanin() ;
       not loop.atEnd();
       ++loop) {
    FLNode * fanin = (*loop);
    setupDriverLookup(flow_lookup, fanin->getDefNet());
  }
  for ( Iter<FLNode*> loop = flow->loopEdgeFanin() ;
        !loop.atEnd();
        ++loop ) {
    FLNode * fanin = (*loop);
    setupDriverLookup(flow_lookup, fanin->getDefNet());
  }

  // Destroy any nested constructs. Do this before calling
  // UnelabFlow, which updates the nesting.
  prepareNestedFlowForDestruction ( flow );

  flow->clearEdgeFanin();
  flow->clearFanin();

  // Save away which net/use_def are associated with this flow.
  setupDriverLookup(flow_lookup, flow->getDefNet());
}


void SplitFlow::setupFlowElabLookup(FlowNetHierToFlowElabMap & flow_elab_lookup,
                                    FLNodeSet & boundary_flow,
                                    FLNodeConstElabMap & flow_to_elab)
{
  for ( FLNodeSet::iterator iter = boundary_flow.begin() ;
	iter != boundary_flow.end() ;
	++iter ) {
    FLNode * flow = (*iter);

    FLNodeElabSet & elab_set = flow_to_elab[ flow ];
    for ( FLNodeElabSet::iterator elabiter = elab_set.begin();
	  elabiter != elab_set.end() ;
	  ++elabiter ) {
      FLNodeElab * flow_elab = (*elabiter);

      setupOneFlowElabLookup(flow_elab_lookup, flow_elab);
    }
  }
}


void SplitFlow::setupOneFlowElabLookup(FlowNetHierToFlowElabMap & flow_elab_lookup,
                                       FLNodeElab * flow_elab)
{
  for ( Iter<FLNodeElab*> loop = flow_elab->loopFanin() ;
        !loop.atEnd() ;
        ++loop ) {
    FLNodeElab * fanin = (*loop);
    setupDriverLookup(flow_elab_lookup, fanin->getDefNet());
  }
  for ( Iter<FLNodeElab*> loop = flow_elab->loopEdgeFanin() ;
        !loop.atEnd() ;
        ++loop ) {
    FLNodeElab * fanin = (*loop);
    setupDriverLookup(flow_elab_lookup, fanin->getDefNet());
  }

  // Destroy any nested constructs. Do this before calling
  // UnelabFlow, which updates the nesting.
  prepareNestedFlowForDestruction ( flow_elab );

  flow_elab->clearEdgeFanin();
  flow_elab->clearFanin();

  setupDriverLookup(flow_elab_lookup, flow_elab->getDefNet());
}


void SplitFlow::doUnelaboration(SplitFlowFactory * split_flow_factory,
                                ModuleCreatedAlways & module_created_always)
{
  // fixup the unelab flow graph for this element; using the
  // cached live flow where possible. We accomplish this by using
  // a special derived flow factory.
  UnelabFlow unelabFlow( split_flow_factory,
			 mNetRefFactory,
			 mMsgContext,
			 mIODB,
			 false);

  // per-module, recompute unelab flow.
  for (ModuleCreatedAlways::iterator iter = module_created_always.begin();
       iter !=  module_created_always.end();
       ++iter) {
    NUAlwaysBlockSet & alwaysBlocks = iter->second;
    unelabFlow.alwaysBlockSet(&alwaysBlocks);
  }
}


void SplitFlow::doElaboration(SplitFlowFactory * split_flow_factory,
			      SplitFlowElabFactory * split_flow_elab_factory,
                              ModuleCreatedAlways & module_created_always,
			      FLNodeSet & boundary_flow,
			      FLNodeConstElabMap & flow_to_elab,
                              ClockAndDriver * clock_and_driver)
{
  Elaborate elaborate(mSymtab,
		      mStrCache,
		      mLocFactory,
		      split_flow_factory,
		      split_flow_elab_factory,
		      mNetRefFactory,
		      mMsgContext,
		      mArgs,
                      mIODB);

  // Set of all hierarchy needing partial re-elaboration.
  HierSet hier_set;

  // Map from module to the unelaborated flow contained within that module.
  ModuleToFlow     module_to_flow;

  populateElaborationData(boundary_flow,
                          flow_to_elab,
                          hier_set,
                          module_to_flow);

  findAllModuleHierarchy(module_to_flow,
                         hier_set);

  // Mark the unelaborated limits of our re-elaboration walk.
  FLNodeSet stoppers;
  markStoppingPoints(boundary_flow,stoppers);

  // For each hierarchy element, perform re-elaboration of the blocks
  // within that hierarchy.
  FLNodeElabSet covered;
  for (HierSet::iterator iter = hier_set.begin();
       iter != hier_set.end();
       ++iter) {
    STBranchNode * hier = (*iter);

    NUModule * module_for_hierarchy = NUModule::lookup(hier);
    ST_ASSERT(module_for_hierarchy,hier);

    // Elaboratedly declare nets within the new blocks.
    NUAlwaysBlockSet & blocks_for_hierarchy = module_created_always[module_for_hierarchy];
    setupElaboration(&elaborate,hier,blocks_for_hierarchy);

    // Our starting flow set is the full set of modified flow within
    // the current module, even if that flow is dead in some
    // elaborations.
    FLNodeSet & starting_flow = module_to_flow[module_for_hierarchy];

    // The set of starting nets is the full set of nets declared
    // within the modified blocks.
    NUNetList starting_nets;
    getStartingNets(blocks_for_hierarchy,starting_nets);

    // Elaborate the flow graph.
    elaborate.elabScopeFlow( hier, starting_flow, starting_nets, &stoppers, clock_and_driver, false, &covered );
  }

}


class FindInstantiatedModuleHierarchyCallback : public NUInstanceCallback
{
public:
  FindInstantiatedModuleHierarchyCallback(STSymbolTable * symtab,
                                          const SplitFlow::ModuleToFlow & module_to_flow,
                                          SplitFlow::HierSet & hier_set) :
    NUInstanceCallback(symtab),
    mModuleToFlow(module_to_flow),
    mHierSet(hier_set) {}

  ~FindInstantiatedModuleHierarchyCallback() {}

protected:
  void handleModule(STBranchNode * branch, NUModule * module) {
    SplitFlow::ModuleToFlow::const_iterator location = mModuleToFlow.find(module);
    if (location != mModuleToFlow.end()) {
      mHierSet.insert(branch);
    }
  }

  void handleDeclScope(STBranchNode*,NUNamedDeclarationScope*) {}
  void handleInstance(STBranchNode * /*branch*/, NUModuleInstance * /*instance*/) {}
private:
  const SplitFlow::ModuleToFlow & mModuleToFlow;
  SplitFlow::HierSet & mHierSet;
};


void SplitFlow::findAllModuleHierarchy(const ModuleToFlow & module_to_flow,
                                       HierSet & hier_set)
{
  FindInstantiatedModuleHierarchyCallback callback(mSymtab,
                                                   module_to_flow,
                                                   hier_set);
  NUDesignWalker walker(callback,false,false);
  walker.design(mDesign);
}


void SplitFlow::getStartingNets(NUAlwaysBlockSet & blocks_for_hierarchy,
                                NUNetList & starting_nets)
{
  NUNetSet nets;
  ASSERT(starting_nets.empty());
  for (NUAlwaysBlockSet::iterator iter = blocks_for_hierarchy.begin();
       iter != blocks_for_hierarchy.end();
       ++iter) {
    NUAlwaysBlock * always = (*iter);
    always->getBlock()->getAllNets(&starting_nets);
    nets.insert(starting_nets.begin(),starting_nets.end());
    starting_nets.clear();
  }

  for ( NUNetSet::iterator iter = nets.begin();
        iter != nets.end();
        ++iter ) {
    NUNet * net = (*iter);
    starting_nets.push_back(net);
  }
}


void SplitFlow::populateElaborationData(FLNodeSet & boundary_flow,
                                        FLNodeConstElabMap & flow_to_elab,
                                        HierSet & hier_set,
                                        ModuleToFlow & module_to_flow)
{
  for ( FLNodeSet::iterator iter = boundary_flow.begin() ;
	iter != boundary_flow.end();
	++iter ) {
    FLNode * flow = (*iter);
    FLNodeElabSet & elab_set = flow_to_elab[ flow ];
    populateElaborationDataElabFlowSet(elab_set, hier_set, module_to_flow);
  }
}


void
SplitFlow::populateElaborationDataElabFlowSet(FLNodeElabSet& elab_set,
                                              HierSet& hier_set,
                                              ModuleToFlow& module_to_flow)
{
  for ( FLNodeElabSet::iterator elabiter = elab_set.begin();
        elabiter != elab_set.end() ;
        ++elabiter ) {
    FLNodeElab * flow_elab = (*elabiter);
    STBranchNode * hier = flow_elab->getHier();

    // remember hierarchy
    hier_set.insert(hier);

    // associate the flow with the module it will be elaborated into.
    NUModule * module_for_branch = NUModule::lookup(hier);
    ST_ASSERT(module_for_branch,hier);
    FLNode* flow = flow_elab->getFLNode();
    module_to_flow[module_for_branch].insert(flow);
  }
}


void SplitFlow::setupElaboration(Elaborate * elaborate,
				 STBranchNode * hier,
                                 NUAlwaysBlockSet & blocks_for_hierarchy)
{
  if (mCreateDeclarations) {
    for ( NUAlwaysBlockSet::iterator iter = blocks_for_hierarchy.begin();
	  iter != blocks_for_hierarchy.end();
	  ++iter ) {
      NUAlwaysBlock * always = (*iter);
      elaborate->elabScopeDecl(hier,always->getBlock());
    }
  }
}


void SplitFlow::markStoppingDrivers(NUNet * net,
				    FLNodeSet & starting,
				    FLNodeSet & stoppers)
{
  for (NUNet::DriverLoop loop = net->loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    FLNode * flow = (*loop);
    if ( starting.find(flow) == starting.end() ) {
      stoppers.insert(flow);
    }
  }  
}


void SplitFlow::markStoppingPoints(FLNodeSet & starting,
				   FLNodeSet & stoppers)
{
  for (FLNodeSet::iterator iter = starting.begin();
       iter != starting.end();
       ++iter) {
    FLNode * flow = (*iter);

    markStoppingDrivers(flow->getDefNet(),
			starting,
			stoppers);

    for (Iter<FLNode*> loop = flow->loopFanin() ;
	 not loop.atEnd() ;
	 ++loop) {
      FLNode * fanin = (*loop);
      markStoppingDrivers(fanin->getDefNet(),
			  starting,
			  stoppers);
    }
    for (Iter<FLNode*> loop = flow->loopEdgeFanin() ;
	 not loop.atEnd() ;
	 ++loop) {
      FLNode * fanin = (*loop);
      markStoppingDrivers(fanin->getDefNet(),
			  starting,
			  stoppers);
    }
  }
}


void SplitFlow::prepareNestedFlowForDestruction(FLNodeElab * flow)
{
  if ( flow->hasNestedBlock() ) {
    // Clear prior nesting information.
    flow->clearNestedBlock();
  }
}


void SplitFlow::prepareNestedFlowForDestruction(FLNode * flow)
{
  if ( flow->hasNestedBlock() ) {
    // Clear prior nesting information.
    flow->clearNestedBlock();
  }
}


