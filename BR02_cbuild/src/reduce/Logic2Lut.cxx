// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implementation of transform from general NUExpr to NULut (lookup table)

  Examines all expressions in the unelaborated Nucleus tree.  For those
  with a sufficiently small number of fanin bits (limit 8), we exhaustively
  simulate the entire input space, and record the results in a truth table.
  If the evaluation of the LUT, including the concat of the input bits,
  is less than the cost of the original expression, then we substitute.

  For now, we only do this substitution at the back end, because further
  simplifications will not be possible once we have computed the LUT.  At
  some point, we could add Folds to simplify LUTs with some constant input
  bits into smaller LUTs.
*/
  

//
// test/common-concat/common-concat-09.v has a case where we'd
// want to copy-propagate the whole always-block into one LUT.  Perhaps
// we want to steal some infrastructure from BlockResynth and do LUT
// substitution on the results of the expression synthesis.  Note that
// LUTs and block resynthesis do not go well together, becuase block
// resynthesis atomizes expresssions with lots of temp variables, and
// LUTs are at their best when they can compute complex functions in
// one shot.
//
// test/lut/wide_vec.v is a case where pessimistic
// NUExpr::getUses(NUNetRefSet*) causes us to think that we are using
// all the bits of B when run after aggressive folding.  We should
// make an aggressive version of getUses that gives more accurate bit
// information, although Joe does not like that idea.  I need to discuss
// that more with him.


#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUExprFactory.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUUseDefNode.h"
#include "reduce/Fold.h"
#include "reduce/Logic2Lut.h"

Logic2Lut::Logic2Lut(NUNetRefFactory *netref_factory,
                     Fold* fold,
                     FLNodeFactory *dfg_factory,
                     ArgProc* args,
                     MsgContext *msg_ctx,
                     IODBNucleus *iodb,
                     AtomicCache *str_cache,
                     bool verbose,
                     bool force) :
  mNetRefFactory(netref_factory),
  mFlowFactory(dfg_factory),
  mArgs(args),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mStrCache(str_cache),
  mVerbose(verbose),
  mForce(force),
  mFold(fold)
{
  mCostContext = new NUCostContext;
  mTotalCostWin = 0;
  mNumLuts = 0;
}

Logic2Lut::~Logic2Lut()
{
  delete mCostContext;
}

 
//! replaceLeaves helper class for conversion from logic to LUTs
class Logic2LutExprCallback : public NuToNuFn
{
public:
  //! Constructor.
  Logic2LutExprCallback(Logic2Lut* l2l) : mLogic2Lut(l2l) {}

  //! Destructor.
  ~Logic2LutExprCallback() {}

  //! Generic callback for unhandled expressions.
  NUExpr* operator()(NUExpr* expr, Phase phase) {
    if (phase == ePre) {
      NUExpr* rep = mLogic2Lut->expr(expr);
      if (rep != NULL) {
        delete expr;
        return rep;
      }
    }
    return NULL;
  }
private:
  Logic2Lut* mLogic2Lut;
}; // class Logic2LutExprCallback : public NuToNuFn

NUExpr* Logic2Lut::expr(NUExpr* expr) {
  if (expr->isWholeIdentifier() || (dynamic_cast<NULut*>(expr) != NULL)) {
    return NULL;
  }

  // test/vhdl/signed/add28.vhdl fails due to some issue with signed BVs
  if ((expr->getBitSize() > 64) && expr->isSignedResult()) {
    return NULL;
  }

  NUExpr* ret = NULL;
  NUNetRefSet bits(mNetRefFactory);
  expr->getUses(&bits);
  if (bits.empty()) {
    // Must be a constant
    return NULL;
  }

  NUCost exprCost;
  expr->calcCost(&exprCost);
  if (exprCost.mInstructionsRun < 4) {
    // Too simple
    return NULL;
  }

  NetRefVector v;
  const SourceLocator& loc = expr->getLoc();
  if (!createConcatVector(bits, &v) || v.empty()) {
    return NULL;                // bail on effectively empty expressions
  }
  sortConcatVector(&v);
  NUConcatOp* concat = buildConcat(v, expr);
  UInt32 concatSize = concat->determineBitSize();
  CopyContext cc(NULL, NULL);

  // Make an optimize LUT select expression via aggressive folding.  Use
  // that for costing as well.  But keep the original concat because it's
  // convenient to get the original expressions from that when computing
  // the LUT table values
  concat->resize(concatSize);
  NUExpr* select = mFold->fold(concat->copy(cc), eFoldAggressive);

  // Do not fold the concat because it probably won't find any folds
  // -- we are constructing it optimially, I think.  And we want to
  // retain the correlation with the vector 'v', so we can build
  // expressions via substitutions
  // concat = mFold->fold(concat); // probably won't find any folds

  NUCost selectCost;
  select->calcCost(&selectCost, mCostContext);
  UtString b1, b2;
  expr->compose(&b1, NULL);
  select->compose(&b2, NULL);
  const char* decision = "loss";

  // I think we may be underestimating the cost of a LUT because it
  // can be big and have cache disturbance.  So I'm going to try compensating
  // by derating the original expression cost by 33%.
  //SInt32 derated_expr_cost = (2*exprCost.mInstructionsRun) / 3;
  SInt32 derated_expr_cost = exprCost.mInstructionsRun - 2;

# define MAX_COUNT 6   // max size 64 element table
  if ((concatSize <= MAX_COUNT) &&
      ((mForce && (*concat != *expr))
       || (selectCost.mInstructionsRun < (derated_expr_cost - 1))))
  {
    NUExpr* lut = buildLut(concat, select, expr, concatSize);

    // Now that we have the whole LUT, cost that out to see if we
    // still have a win.
    if (lut != NULL) {
      NUCost lutCost;
      lut->calcCost(&lutCost);
      if (mForce || (lutCost.mInstructionsRun < derated_expr_cost)) {
        ret = lut;
        decision = "win";
        mTotalCostWin += exprCost.mInstructionsRun;
        mTotalCostWin -= lutCost.mInstructionsRun;
        ++mNumLuts;
      }
      else {
        delete lut;
      }
    }
    else {
      delete select;
    }
  }
  else {
    delete select;
  }
  if (mVerbose) {
    UtString locbuf;
    loc.compose(&locbuf);
    UtIO::cout() << locbuf << ": LUT " << decision << ": expr="
                 << b1 << " cost=" << exprCost.mInstructionsRun
                 << " select=" << b2 << " count=" << concatSize << " cost="
                 << selectCost.mInstructionsRun << "\n";
  }
  delete concat;
  return ret;
} // NUExpr* Logic2Lut::expr

bool Logic2Lut::createConcatVector(const NUNetRefSet& bits, NetRefVector* v) {
  // Create an expression that gets the bits in the netref set concated
  // together
  UInt32 totalBits = 0;
  for (NUNetRefSet::SortedLoop p = bits.loopSorted(); !p.atEnd(); ++p) {
    NUNetRefHdl ref = *p;
    NUNet* net = ref->getNet();
    if (net == NULL) {
      continue;           // out-of-bounds mem refs can yield an empty net ref
    }
    if (net->is2DAnything()) {
      return false;
    }
    if (ref->all()) {
      v->push_back(ref);
      totalBits += net->getBitSize();
    }
    else {
      for (NUNetRefRangeLoop r = ref->loopRanges(mNetRefFactory);
           !r.atEnd(); ++r)
      {
        const ConstantRange& range = *r;
        v->push_back(mNetRefFactory->createVectorNetRef(net, range));
        totalBits += range.getLength();
      }
    }
    if (totalBits > 8) {
      return false;
    }
  }
  return true;
} // bool Logic2Lut::createConcatVector

NUConcatOp* Logic2Lut::buildConcat(const NetRefVector& v, NUExpr* expr) {
  const SourceLocator& loc = expr->getLoc();
  NUExprVector exprs;
  bool needConstantFill = false;
  SInt32 bitOffset = 0;
  for (UInt32 i = 0; i < v.size(); ++i) {
    NUNetRefHdl ref = v[i];
    NUNet* net = ref->getNet();
    if (ref->empty()) {
      needConstantFill = true;
    }
    else if (ref->all()) {
      NU_ASSERT(!needConstantFill, net);
      exprs.push_back(new NUIdentRvalue(net, loc));
      bitOffset += net->getBitSize();
    }
    else {
      ConstantRange range;
      ref->getFirstContiguousRange(range);
      if (needConstantFill) {
        NU_ASSERT(range.getLsb() > bitOffset, net);
        UInt32 fillSize = range.getLsb() - bitOffset;
        bitOffset = range.getLsb();
        NUConst* filler = NUConst::create(false, 0, fillSize, loc);
        exprs.push_back(filler);
      }
      exprs.push_back(new NUVarselRvalue(net, range, loc));
      bitOffset += range.getLength();
      needConstantFill = false;
    }
  } // for

  // Reverse the order of exprs -- we have been building up our array
  // LSB first, now we want it LSB last for the concat.
  std::reverse(exprs.begin(), exprs.end());

  NUConcatOp* concat = new NUConcatOp(exprs, 1, expr->getLoc());
  concat->resize(concat->determineBitSize());
  return concat;
} // NUConcatOp* Logic2Lut::buildConcat

struct CompareNetRefLsb {
  bool operator()(const NUNetRefHdl& h1, const NUNetRefHdl& h2) const {
    ConstantRange r1, r2;
    h1->getFirstContiguousRange(r1);
    h2->getFirstContiguousRange(r2);
    return r1.getLsb() < r2.getLsb();
  }
};

// the concatenation vector is arbitrary in order.  Re-order it to minimize
// the bit-shifting.  
//
// For example, if we have {a[0],b[3],c[4],d[6]}, it would be
// better to move a[0] into the 0th position and b[3] into the 3rd
// position, so that they can be ORed in without any shifting.  For
// now, do this in greedy order.
void Logic2Lut::sortConcatVector(NetRefVector* v) {
  // Walk through all of the source netrefs and try to place them
  // where they want to be, moving the refs that overlap placed bits
  // into holes as a second pass.  We'll do this greedily.  Note that
  // we will never have more than 32 bits because that would result in
  // a table > 2^32 long.
  
  UInt32 bitMask = 0;
  SInt32 highestBit = 0;
  SInt32 numPlacedBits = 0;
  SInt32 numUnplacedBits = 0;
  NetRefVector placed, unplaced;
  UtHashMap<SInt32,NUNetRefHdl> forcedPositionMap;

  // Collect all the slices that will fit without shifting
  for (UInt32 i = 0; i < v->size(); ++i) {
    NUNetRefHdl ref = (*v)[i];
    ConstantRange range;
    ref->getFirstContiguousRange(range);

    // If this range covers bit index > 7 then it will make the table
    // too large.  Don't place it.
    bool was_placed = false;
    if (range.getMsb() < 8) {
      // If it fits, put it in.
      UInt32 refMask = ((1 << range.getLength()) - 1) << range.getLsb();
      if ((refMask & bitMask) == 0) {
        // It fits here.  Grab it.
        bitMask |= refMask;
        placed.push_back(ref);
        if (highestBit < range.getMsb()) {
          highestBit = range.getMsb();
        }
        numPlacedBits += range.getLength();
        was_placed = true;
      }
    }

    // Otherwise, we will have to put it some place that will require
    // a shift.  Keep track of those.
    if (!was_placed) {
      unplaced.push_back(ref);
      numUnplacedBits += range.getLength();
    }
  } // for

  SInt32 totalBits = numUnplacedBits + numPlacedBits;
  SInt32 maxbit = std::max(highestBit + 1, totalBits);

  // Now try to fill all the unplaced bits wherever they will fit.
  for (UInt32 i = 0; i < unplaced.size(); ++i) {
    NUNetRefHdl ref = unplaced[i];
    ConstantRange range;
    ref->getFirstContiguousRange(range);

    bool found_fit = false;
    for (SInt32 bit = 0; bit < maxbit; ++bit) {
      UInt32 refMask = ((1 << range.getLength()) - 1) << bit;
      if ((refMask & bitMask) == 0) {
        bitMask |= refMask;     // claim these bits
        forcedPositionMap[bit] = ref;
        found_fit = true;
        SInt32 highBit = bit + range.getMsb();
        if (highestBit < highBit) {
          highestBit = highBit;
        }
        break;
      }
    }
    if (!found_fit) {
      // Could not find a way to fit this netref.  Punt on the sort entirely!
      return;
    }
  }

  // How many bits of filler do we need?
  UInt32 holes = highestBit + 1 - totalBits;
  if (holes > 2) {
    return;                     // punt.  too many holes.
  }

  // The placed references are all non-overlapping -- this was guaranteed
  // by our use of bitMask.  But they are unsorted.  We need them sorted
  // so we can find the holes for putting the unplaced bits
  std::sort(placed.begin(), placed.end(), CompareNetRefLsb());

  // If we made it here, then we have consed up a good packing for this
  // concat to minimize the number of shifts needed.  Rewrite the NUExpr
  // vector with the ordered refs.
  SInt32 bitOffset = 0;
  v->clear();
  UtHashMap<SInt32,NUNetRefHdl>::SortedLoop u(forcedPositionMap);
  for (UInt32 p = 0; (p < placed.size()) || !u.atEnd(); ) {
    ConstantRange range;
    if (!u.atEnd() && (u.getKey() == bitOffset)) {
      NUNetRefHdl ref = u.getValue();
      ref->getFirstContiguousRange(range);
      v->push_back(ref);
      ++u;
    }
    else {
      INFO_ASSERT(p < placed.size(),"ref sorting insanity 1");
      NUNetRefHdl ref = placed[p];
      ref->getFirstContiguousRange(range);
      INFO_ASSERT(range.getLsb() >= bitOffset, "ref sorting insanity 2");
      if (range.getLsb() > bitOffset) {
        // Need to fill a hole with a zeros.  Mark that in the vector with 
        // an empty net ref
        v->push_back(mNetRefFactory->createEmptyNetRef());
        bitOffset = range.getLsb();
      }
      v->push_back(ref);
      ++p;
    }
    bitOffset += range.getLength();
  } // for
} // void Logic2Lut::sortConcatVector

bool Logic2Lut::design(NUDesign* design) {
  Logic2LutExprCallback callback(this);

  NUModuleList mods;
  design->getModulesBottomUp(&mods);
  for (NUModuleList::iterator p = mods.begin(), e = mods.end(); p != e; ++p) {
    NUModule* mod = *p;
    mod->replaceLeaves(callback);
  }

  if (mVerbose) {
    UtIO::cout() << mNumLuts
                 << " LUTS created, total runInstruction cost win=" << mTotalCostWin
                 << "\n";
  }

  return true;
}

class ConstCreationCallback : public NuToNuFn
{
public:
  //! Constructor.
  ConstCreationCallback(const Logic2Lut::NetConstantMap& ncmap,
                        const SourceLocator& loc)
    : mNetConstantMap(ncmap),
      mLoc(loc)
  {}

  NUExpr* operator()(NUExpr* expr, Phase phase) {
    if ((phase == ePre) && expr->isWholeIdentifier()) {
      NUNet* net = expr->getWholeIdentifier();
      Logic2Lut::NetConstantMap::const_iterator p = mNetConstantMap.find(net);
      if (p != mNetConstantMap.end()) {
        const DynBitVector& bitvec = p->second;
        UInt32 bitSize = expr->getBitSize(); // net->getBitSize();
        bool sign = expr->isSignedResult();
        NUConst* k = NUConst::create(sign, bitvec, bitSize, mLoc);
        delete expr;
        return k;
      }
    }
    return NULL;
  }

private:
  const Logic2Lut::NetConstantMap& mNetConstantMap;
  SourceLocator mLoc;
}; // class ConstCreationCallback : public NuToNuFn


NUExpr* Logic2Lut::buildLut(NUConcatOp* concat, NUExpr* select,
                            NUExpr* expr, UInt32 numBits)
{
  // "Simulate" the expression with all possible constants.
  // Do this by creating a copy of the expression and then
  // substituting the desired constant-pattern for all the
  // appropriate bits.

  NUConstVector table;

  const SourceLocator& loc = expr->getLoc();
  UInt32 esize = expr->getBitSize();
  bool esign = expr->isSignedResult();

  // Note that the total number of bits that can be in the LUT selector
  // has to be pretty small (8) so the LUT doesn't explode in space.
  // However, the nets being substituted may be quite wide, so it's
  // probably best to do this with DynBitVectors.

  UInt32 numEntries = 1 << numBits;
  NetConstantMap netConstantMap;

  for (UInt32 bitPattern = 0; bitPattern < numEntries; ++bitPattern) {
    UInt32 offset = 0;

    // clear out all the bitvectors
    for (NetConstantMap::iterator p = netConstantMap.begin(),
           e = netConstantMap.end(); p != e; ++p)
    {
      DynBitVector& bv = p->second;
      bv.reset();
    }

    // Loop the concat from LSB to LSB
    for (SInt32 e = concat->getNumArgs() - 1; e >= 0; --e) {
      NUExpr* selExpr = concat->getArg(e);
      NUIdentRvalue* id = dynamic_cast<NUIdentRvalue*>(selExpr);
      NUVarselRvalue* vs = dynamic_cast<NUVarselRvalue*>(selExpr);
      NUConst* k = selExpr->castConst();
      NU_ASSERT((vs != NULL) || (id != NULL) || (k != NULL), selExpr);
      NUNet* net = NULL;
      UInt32 selExprWidth = selExpr->getBitSize();

      if (k != NULL) {
        offset += selExprWidth;
        continue;
      }

      if (id != NULL) {
        net = id->getIdent();
      }
      else {
        net = vs->getIdent();
      }

      UInt32 netPatternMask = (1 << selExprWidth) - 1;

      // First time through the bitPattern loop, resize the bitvectors
      // to be the correct size
      UInt32 netSize = net->getBitSize();
      DynBitVector& netBitPattern = netConstantMap[net];
      if (bitPattern == 0) {
        netBitPattern.resize(netSize);
      }

      // For simple nets, just set the bitvector to the desired pattern
      if (id != NULL) {
        netBitPattern = (bitPattern >> offset) & netPatternMask;
      }
      
      // For var-selects, we have to move the pattern bits into the position
      // referenced by the select-range, potentially merging with other
      // disjoint var-selects on the same net (though that not happen till
      // bug 6137 is fixed)
      else {
        DynBitVector pattern(netSize);
        pattern = (bitPattern >> offset) & netPatternMask;

        // If we are referencing only a few bits of the net for
        // this part of the concat, then shift the value over
        // so we land this pattern in the interesting bits
        const ConstantRange* range = vs->getRange();

        // note range lsb may be > 64, so DBV is needed
        pattern <<= range->getLsb();

        // Also, we need to OR this bit-pattern into any existing
        // bit-pattern that has been established for the net.  We
        // are relying on the fact that the map will initialize
        // new entries to 0
        netBitPattern |= pattern;
      }

      offset += selExprWidth;
    } // for
    NU_ASSERT(offset == concat->getBitSize(), concat);

    ConstCreationCallback ccb(netConstantMap, loc);
    CopyContext cc(NULL, NULL);
    NUExpr* copy = expr->copy(cc);
    copy->replaceLeaves(ccb);

    // Do the Folding bottom-up.  This is to avoid a Fold crash in
    // FoldIAssociate.cxx which shows up if we try to run test/cust/mips/m74k
    // with -enableLogic2LUT.  The bug is in FoldI::reassociate which is
    // not expecting to see a lot of CONSTANT[bit] forms in expressions.
    // Al thought he had fixed it but this 74k testcase proves he did not
    // complete that work.  As I (Josh) am working on extensive changes to
    // Folding anyway, I want to avoid having this issue block us if we want
    // to try LUTs at a customer site.
    copy = mFold->fold(copy, eFoldBottomUp);

    NUConst* k = copy->castConst();
    if (k == NULL) {
      // We get here for test/vhdl/lang_pragma/operators_carbon.vhd, due
      // to our failure to fold ROL and ROR over constants.  Also
      // small integer division by non-power-of-2 in operators2.
      // 3 downto 3 does not fold away to a constant either in
      // lang_flowctrl/part_select_14.vhdl

      if (mVerbose) {
        UtIO::cout() << "INTERNAL WARNING: cannot reduce expr to constant:\n";
        UtString buf;
        copy->compose(&buf, NULL);
        UtIO::cout() << "  " << buf.c_str() << "\n";
      }
      delete copy;

      // After replacing all the used nets with constants, the
      // expression still did not fold to a constant, sadly.
      // Let's find out why.  Free any constants created so far
      // and print a warning
      for (UInt32 i = 0; i < bitPattern; ++i) {
        k = table[i];
        delete k;
      }
      return NULL;
    }
    else {
      table.push_back(k);
    }
  }
  NUExpr* lut = new NULut(select, table, loc);
  lut->resize(esize);
  lut->setSignedResult(esign);
  return lut;
} // void Logic2Lut::buildLut

#if 0
NUExpr* Logic2Lut::optimizePartSel(NUExpr*) {
  // optimize a[6:4]<<4 to a&(((1<<3)-1)<<4)
  // the 3 in 1<<3 comes from the length of the constant range [6:4]
  //
  // We are only doing this in aggressive mode because we are changing the
  // used bits of a to encompass the whole net.
  //
  // This was initially put into Fold but Al raised some troubling issues
  // with regard to changing the determineBitSize of an expression, even
  // in 'aggressive' mode.  But in the context of Logic2LUT we know we
  // are only going to be ORing the result of this part-select into other
  // non-overlapping bits, and will not incorporate this expression directly
  // into (say) a +.
  if ((width <= LLONG_BIT) && (opc == NUOp::eBiLshift) &&
      (count <= 0xffffffff))
  {
    NUVarselRvalue* vs = dynamic_cast<NUVarselRvalue*>(lop);
    if (vs != NULL) {
      NUConst* idx = vs->getIndex()->castConst();
      const ConstantRange* range = vs->getRange();
      UInt32 len = range->getLength();
      if ((idx != NULL) && idx->isZero() && (range->getLsb() == (SInt32) count)
          && (range->getLsb() + len <= 32))
      {
        UInt64 mask = ((1 << len) - 1) << count;
        NUExpr* id = vs->getIdentExpr();
        UInt32 idSize = id->getBitSize();
        id = id->copy(mCC);
        id->resize(idSize);

        UInt32 constSize = idSize;
        if (width > idSize) {
          constSize = width;
        }
        NUConst* kmask = NUConst::create(false, mask, constSize, loc);
        result = new NUBinaryOp(NUOp::eBiBitAnd, kmask, id, loc);
        UInt32 naturalSize = result->determineBitSize();
        result->resize(width);
        if (naturalSize != width) {
          // Upsizing the constant should have made it so we are only
          // dealing with shrinking the result via part-select
          NU_ASSERT(width < naturalSize, result);
          result = result->NUExpr::makeSizeExplicit();
        }
        return result;
      }
    }
  } // if
} // void Logic2Lut::optimizePartSel

// If no other optimizations, transform concats with lucky bits
// into more efficient OR expressions.  E.g.
//    {a[2],b[1],c[0]} --> a&3'b100 | b&3'b010 | c&3'b001
// Logic2Lut contrives to make such concats when possible.  Codegen
// doesn't render them particularly efficiently (nor should it).
// So at the last moment, transform them to something that can be
// rendered in fewer instructions.
//
// Also consider this case:
//    {a[3],b[2],c[0]} --> (a&3'b1000 | b&4'b0100)>>1 | c&3'b001
// We can do this in one less shift than the "natural" way.
//
// Note -- this code produces simulation mismatches, due to
// dirty-bit polution when arithmetic operations such as +
// exist as concat members.  The explicit masking needs to be
// put in.  I'll leave that for another commit.
NUExpr* FoldI::genConcatTerms(NUConcatOp* cat) {
  // First make a histogram of every expression in the concatenation,
  // and what bit-position it wants to wind up in.  Then we'll figure
  // out how to get them there.
  UInt32 currentOffset = 0;

  //typedef std::pair<SInt32,SInt32> TargetPosition;
  //UtHashMap<NUExpr*, TargetPosition> exprToTargetPosition;

  NUExpr* out = NULL;

  const SourceLocator& loc = cat->getLoc();
  UInt32 catSize = cat->getBitSize();

  for (SInt32 i = cat->getNumArgs() - 1; i >= 0; --i) {
    NUExpr* e = cat->getArg(i);
    UInt32 exprSize = e->getBitSize();
    e = e->copy(mCC);
    e->resize(exprSize);
    e = e->NUExpr::makeSizeExplicit();

#if 0
    NUVarselRvalue* vs = dynamic_cast<NUVarselRvalue*>(e);
    SInt32 start = 0;
    if (vs != NULL) {
      NUConst* idxConst = vs->getIndex()->castConst();
      if ((idxConst != NULL) && idxConst->isZero()) {
        start = vs->getRange()->getLsb();
      }
      else {
        vs = NULL;
      }
    }
#endif

    if (currentOffset != 0) {
      NUConst* k = NUConst::create(false, currentOffset, 32, loc);
      e = new NUBinaryOp(NUOp::eBiLshift, e, k, loc);
      // sizing issues?
    }

    if (out == NULL) {
      out = e;
    }
    else {
      out = new NUBinaryOp(NUOp::eBiBitOr, out, e, loc);
    }

    // could also look for explicit shift(mask(expr)), or mask(shift(expr))
    //exprToTargetPosition[e] = TargetPosition(currentOffset, start);
    currentOffset += exprSize;
  }

  out->resize(catSize);
  out = out->NUExpr::makeSizeExplicit();

  // If, in order to generate the correct size results, we wound up
  // producing a concat to 0-extend, we may wind up with an infinite
  // recursion.  Cut that here.
  if (out->compare(*cat, false, false, true) == 0) {
    delete out;                 // not_managed_expr
    out = NULL;
  }
  else {
    out = finish(cat, out);
  }

  return out;

  // Find the first shift that's required.  Note that we might have
  // {d[7],a[5],b[4],c[5]}, in which case the first shift we want to do
  // is >>3, which, gets both a and b into the correct bit position.
  // Our final result will be:
  //     (((a&(1<<5)) | (b&(1<<4)) | (c&(1<<5)) | (d&(1<<7))) >> 3)
  //
} // NUExpr* FoldI::genConcatTerms
#endif
