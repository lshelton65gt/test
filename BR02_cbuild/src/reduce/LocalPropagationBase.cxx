// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "util/SetOps.h"

#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"

#include "reduce/Fold.h"
#include "reduce/RewriteUtil.h"
#include "reduce/LocalPropagationBase.h"

LocalPropagationBase::LocalPropagationBase(Fold* fold, 
                                           LocalPropagationStatistics * statistics,
                                           bool storing_non_constants,
                                           bool dont_check_prop_vecs) :
  mFold(fold),
  mStatistics(statistics),
  mStoringNonConstants(storing_non_constants),
  mDontCheckPropVecs(dont_check_prop_vecs)
{
  mNetValues = new NetValuesVector;
  mNestedNetValues = new NestedNetValues;
}

LocalPropagationBase::~LocalPropagationBase()
{
  INFO_ASSERT(mDontCheckPropVecs || (mNetValues->empty() && mNestedNetValues->empty()),
              "LocalPropagationBase vectors not empty");
  delete mNetValues;
  delete mNestedNetValues;
}

void LocalPropagationBase::pushContext()
{
  NetValues * netValues = new NetValues;
  mNetValues->push_back(netValues);
}

void LocalPropagationBase::popContext()
{
  // Remove the top of the stack
  NetValues * netValues = mNetValues->back();
  mNetValues->pop_back();

  // If we are in a nested context, move the constNets to the nested
  // area for application to the parent context when we leave the
  // nested object.
  if (mNestedNetValues->empty()) {
    cleanupNetValues(netValues, true, true);
  } else {
    NetValuesVector* constNetsVector = mNestedNetValues->back();
    constNetsVector->push_back(netValues);
    cleanupNetValues(netValues, false, true);
  }
}

void LocalPropagationBase::cleanupExprUseMap(NUExpr** pexpr) {
  NUNetSet uses;
  (*pexpr)->getUses(&uses);
  for (NUNetSet::iterator p = uses.begin(), e = uses.end(); p != e; ++p) {
    NUNet* use = *p;
    ExprPtrSet& expr_ptr_set = mUseMap[use];
    int removed = expr_ptr_set.erase(pexpr);
    NU_ASSERT(removed == 1, use);
    if (expr_ptr_set.empty()) {
      // Erase empty entries to avoid temporary memory bloat
      mUseMap.erase(use);
    }
  }
}

void LocalPropagationBase::populateExprUseMap(NUExpr** pexpr) {
  if (*pexpr != NULL) {
    NUNetSet uses;
    (*pexpr)->getUses(&uses);
    for (NUNetSet::iterator p = uses.begin(), e = uses.end(); p != e; ++p) {
      NUNet* use = *p;
      ExprPtrSet& expr_ptr_set = mUseMap[use];
      expr_ptr_set.insert(pexpr);
    }
  }
}

void LocalPropagationBase::addNet(NUNet* net, NUExpr * incoming_expr)
{
  NUExpr * expr = NULL;
  if (incoming_expr) {
    CopyContext cc(0,0);
    // make our own copy, as the original may not persist long enough.
    expr = incoming_expr->copy(cc); 
  }

  // Add it to our map. If there was a previous constant, clean up
  NetValues * netValues = mNetValues->back();
  NUExpr** pexpr = &(*netValues)[net];
  if (*pexpr != NULL) {
    cleanupExprUseMap(pexpr);
    delete *pexpr;
  }
  *pexpr = expr;
  populateExprUseMap(pexpr);
} // void LocalPropagationBase::addNet


NUExpr * LocalPropagationBase::getNetValue(NUNet* net) const
{
  // Search from the current context to the first context. As soon as
  // we find an entry we stop.
  bool entryFound = false;
  NUExpr * expr = NULL;
  for (NetValuesVectorRLoop l(*mNetValues); !l.atEnd() && !entryFound; ++l) {
    NetValues * netValues = *l;
    NetValues::iterator pos = netValues->find(net);
    if (pos != netValues->end()) {
      entryFound = true;
      expr = pos->second;
    }
  }
  return expr;
}


void LocalPropagationBase::pushNesting()
{
  NetValuesVector* constNetsVector = new NetValuesVector;
  mNestedNetValues->push_back(constNetsVector);
}

void LocalPropagationBase::popNesting(bool preserve_non_statics)
{
  // Pop the nested data
  NetValuesVector* constNetsVector = mNestedNetValues->back();
  mNestedNetValues->pop_back();

  // Gather all the nets from the nested const nets. Just grab the
  // first constExpr we find for all nets. A later pass will make sure
  // all nested vectors have the same value.
  NetValues mergedNetValues;
  for (NetValuesVectorLoop l(*constNetsVector); !l.atEnd(); ++l) {
    NetValues * netValues = *l;
    for (NetValuesLoop n(*netValues); !n.atEnd(); ++n) {
      NUNet  * net = n.getKey();
      NUExpr * expr = n.getValue();
      mergedNetValues.insert(NetValues::value_type(net, expr));
    }
  }

  // Make sure all the const nets have the same value
  for (NetValuesVectorLoop l(*constNetsVector); !l.atEnd(); ++l) {
    NetValues * netValues = *l;
    for (NetValuesLoop n(mergedNetValues); !n.atEnd(); ++n) {
      // Get the currently merged net and value
      NUNet  * net = n.getKey();
      NUExpr * expr = n.getValue();

      // Make sure the constant values are the same. If the merged
      // value is already NULL, this net is not a constant net.
      if (expr != NULL) {
        // Get this nested nets value. If that doesn't exist, use the
        // parent scopes value.
        NUExpr * otherExpr;
        NetValues::iterator pos = netValues->find(net);
        if (pos != netValues->end()) {
          otherExpr = pos->second;
        } else {
          otherExpr = getNetValue(net);
        }
        
        // Make sure they are the same and non NULL
        if ((otherExpr == NULL) || (*expr != *otherExpr)) {
          // They are not the same, NULL out the existing entry
          mergedNetValues[net] = NULL;
        } else if (not preserve_non_statics) {
          if (net->isTempBlockLocalNonStatic() or net->isMultiplyDriven()) {
            // This is a non-static local and we are leaving its
            // scope. Or we have a multiply driven net and cannot
            // propagate one value into a more general scope.
            mergedNetValues[net] = NULL;
          } else {
            if (mStoringNonConstants) {
              NUNetSet uses;
              expr->getUses(&uses);
              bool has_temp_block_local_non_static = false;
              for (NUNetSet::iterator iter = uses.begin(); 
                   not has_temp_block_local_non_static and (iter != uses.end());
                   ++iter) {
                has_temp_block_local_non_static = (*iter)->isTempBlockLocalNonStatic();
              }
              if (has_temp_block_local_non_static) {
                // This expression uses a non-static local.
                mergedNetValues[net] = NULL;
              }
            }            
          }
        }
      }
    } // for
  }

  // Apply the nets to the current context
  for (NetValuesLoop n(mergedNetValues); !n.atEnd(); ++n) {
    NUNet  * net = n.getKey();
    NUExpr * expr = n.getValue();
    addNet(net, expr);
  }

  // All done with the nested const nets memory
  for (NetValuesVectorLoop l(*constNetsVector); !l.atEnd(); ++l) {
    NetValues * netValues = *l;
    cleanupNetValues(netValues, true, false);
  }
  delete constNetsVector;
} // void LocalPropagationBase::popNesting
  

class LocalPropagationRewriteLeaves : public RewriteLeaves
{
public:
  //! Constructor
  LocalPropagationRewriteLeaves(NUExprReplacementMap & exprReplacements,
                                LocalPropagationStatistics * statistics,
                                Fold * fold) :
    RewriteLeaves(mDummyNetReplacements, mDummyLvalueReplacements,
                  exprReplacements, mDummyTFReplacements,
                  mDummyCmodelReplacements,
                  mDummyAlwaysReplacements, fold),
    mExprReplacements(exprReplacements),
    mStatistics(statistics)
  {}

  //! Destructor
  ~LocalPropagationRewriteLeaves() {}

  //! Hijack the expression rewriter to see if it's being transformed.
  virtual NUExpr * operator()(NUExpr * rvalue, Phase phase);

private:
  //! Expression replacement rules.
  NUExprReplacementMap &       mExprReplacements;

  //! Statistics object.
  LocalPropagationStatistics * mStatistics;

  //! Dummy maps so we can initialize RewriteLeaves.
  NUNetReplacementMap         mDummyNetReplacements;
  NULvalueReplacementMap      mDummyLvalueReplacements;
  NUTFReplacementMap          mDummyTFReplacements;
  NUCModelReplacementMap      mDummyCmodelReplacements;
  NUAlwaysBlockReplacementMap mDummyAlwaysReplacements;
};


NUExpr* LocalPropagationRewriteLeaves::operator()(NUExpr * rvalue, Phase phase)
{
  NUExpr * replacement = RewriteLeaves::operator()(rvalue, phase);
  if (replacement) {
    mStatistics->replacement();
  }
  return replacement;
}


NUExpr * LocalPropagationBase::optimizeExpr(NUExpr* expr, bool* expr_changed,
                                            NUNetSet* replacedNets)
{
  // Get the uses for this node. We only create a replacement map if
  // some of the uses are constants.
  NUExprReplacementMap exprReplacements;
  NUNetSet uses;
  expr->getUses(&uses);
  for (NUNetSetIter i = uses.begin(); i != uses.end(); ++i) {
    NUNet* net = *i;
    NUExpr * replacement = getNetValue(net);
    if (replacement != NULL) {
      // Add it to the replacement map
      exprReplacements[net] = replacement;
      if (replacedNets != NULL) {
        replacedNets->insert(net);
      }
    }
  }

  if (!exprReplacements.empty()) {
    LocalPropagationRewriteLeaves translator(exprReplacements, 
                                             mStatistics,
                                             NULL);
    bool this_expr_changed = false;

    NUExpr * optimized = expr;
    NUExpr * replacement = translator(optimized, NuToNuFn::ePre);
    if (replacement) {
      this_expr_changed = true;
      optimized = replacement;
    }
    this_expr_changed |= optimized->replaceLeaves(translator);
    replacement = translator(optimized, NuToNuFn::ePost);
    if (replacement) {
      this_expr_changed = true;
      optimized = replacement;
    }

    // Our uses say that we should have had replacements. Why weren't there any?
    NU_ASSERT(this_expr_changed, expr);

    if (this_expr_changed) {
      // Delay folding until the entire expression has been processed.
      // This allows for accurate rewrite counts and avoids repeatedly
      // recursing over the same expression tree.
      if (expr_changed != NULL) {
        *expr_changed = true;
      }
      optimized = mFold->fold(optimized);
      if (optimized != expr) {
        return optimized;
      }
    }
  }

  return NULL;
} // LocalPropagationBase::optimizeExpr


void LocalPropagationBase::applyNonPropagatedDefs(NUNetSet& defs)
{
  disqualifyDefs(defs);
  // We disqualify all expressions using these defs because the
  // reaching definition is now different than the cached expressions
  // require.
  disqualifyUses(defs);
}

void LocalPropagationBase::disqualifyDefs(NUNetSet & defs)
{
  // invalidate previous propagation values for each def.
  for (NUNetSetIter i = defs.begin(); i != defs.end(); ++i) {
    NUNet* net = *i;
    addNet(net, NULL);
  }
}

void LocalPropagationBase::disqualifyUses(NUNetSet & defs)
{
  if (mStoringNonConstants) {

    // define a consistency check to ensure that the new map-based approach
    // for this compute-intensive operation is producing precisely the same
    // set of deletions as the old one, by computing both and comparing the
    // results.  I will leave this checked in and #if'd out for now.  Later
    // as confidence is gained this could be removed.

#define DQ_CHECK 0
#if DQ_CHECK
    NUCExprHashSet exprs;
    // TBD: this is expensive for now. make faster.
    for (NetValuesVector::iterator i=mNetValues->begin(); i!=mNetValues->end(); ++i) {
      NetValues * netValues = *i;
      for (NetValues::iterator j = netValues->begin(); j!=netValues->end(); ++j) {
        NUExpr * value = j->second;
        if (value) {
          NUNetSet uses;
          value->getUses(&uses);
          if (set_has_intersection(uses,defs)) {
            exprs.insert(value);
          }
        }
      }
    }
#endif

    for (NUNetSet::iterator p = defs.begin(), e = defs.end(); p != e; ++p) {
      NUNet* def = *p;
      UseMap::iterator p2 = mUseMap.find(def);
      if (p2 != mUseMap.end()) {
        ExprPtrSet& expr_ptr_set = p2->second;
        for (ExprPtrSet::iterator q = expr_ptr_set.begin(); q != expr_ptr_set.end(); ++q) {
          NUExpr** pexpr = *q;
          if (*pexpr != NULL) {
#if DQ_CHECK
            size_t removed = exprs.erase(*pexpr);
            NU_ASSERT(removed == 1, *pexpr);
#endif
            delete *pexpr;
            *pexpr = NULL;
          }
        }
        mUseMap.erase(p2);
      }
    }
#if DQ_CHECK
    INFO_ASSERT(exprs.empty(), "disqualifyUses consistency problem");
#endif
  }
} // void LocalPropagationBase::disqualifyUses

void LocalPropagationBase::cleanupNetValues(NetValues * netValues,
                                            bool delete_expr,
                                            bool clean_use_map)
{
  for (NetValues::iterator p = netValues->begin(), e = netValues->end();
       p != e; ++p)
  {
    NUExpr** pexpr = &p->second;
    if (*pexpr != NULL) {
      if (clean_use_map) {
        cleanupExprUseMap(pexpr);
      }
      if (delete_expr) {
        delete *pexpr;
      }
    }
  }
  if (delete_expr) {
    delete netValues;
  }
}

