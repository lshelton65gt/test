// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/FlattenAnalyzer.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCost.h"

#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"


FlattenAnalyzer::FlattenAnalyzer(MsgContext * msg_context,
                                 NUCostContext * design_costs,
                                 NUCostContext * costs,
                                 NUModule * module,
                                 Mode flatten_mode,
                                 SInt32 inline_limit,
                                 bool flatten_only_leaves,
                                 bool flatten_single_instances,
                                 bool flatten_hier_ref_tasks,
                                 UInt32 tiny_threshold,
                                 UInt32 parent_size_limit) :
  mMsgContext(msg_context),
  mDesignCostCtx(design_costs),
  mModuleCostCtx(costs),
  mModule(module),
  mFlattenMode(flatten_mode),
  mInlineLimit(inline_limit),
  mFlattenOnlyLeaves(flatten_only_leaves),
  mFlattenSingleInstances(flatten_single_instances),
  mFlattenHierRefTasks(flatten_hier_ref_tasks),
  mTinyThreshold(tiny_threshold),
  mParentSizeLimit(parent_size_limit)
{
  analyze();
}


FlattenAnalyzer::~FlattenAnalyzer()
{
}


void FlattenAnalyzer::analyze()
{
  NUModuleToModuleInstanceMap module_to_instance;
  for (NUModuleInstanceMultiLoop loop = mModule->loopInstances();
       not loop.atEnd();
       ++loop) {
    NUModuleInstance * instance = (*loop);
    NUModule * submodule = instance->getModule();
    module_to_instance[submodule].insert(instance);
  }  

  ModuleSizeHistogram size_histogram;

  // Analyze the local characteristics for each module once.
  for (NUModuleToModuleInstanceMap::iterator iter = module_to_instance.begin();
       iter != module_to_instance.end();
       ++iter) {
    NUModule * submodule = iter->first;
    
    Status status = analyze(submodule);
    mSubmoduleStatus[submodule] = status;

    if (status == eStatusSuccess || status == eStatusForced) {
      SInt32 submodule_complexity = computeComplexity(submodule);
      size_histogram[submodule_complexity].insert(submodule);
    }
  }

  SInt32 current_complexity = computeComplexity(mModule);

  // Analyze all instantiations of a given module at the same time. We
  // will flatten all local instances of a module or none at all.
  for (ModuleSizeHistogram::iterator iter = size_histogram.begin();
       iter != size_histogram.end();
       ++iter) {
    SInt32 submodule_complexity = iter->first;
    NUModuleSet & modules = iter->second;
    for (NUModuleSet::iterator miter = modules.begin();
         miter != modules.end();
         ++miter) {
      NUModule * module = (*miter);

      Status current_status = mSubmoduleStatus[module];

      SInt32 instances = module_to_instance[module].size();
      SInt32 instance_complexity = instances * submodule_complexity;

      // SWAG: Do not allow the complexity of the current module to
      // grow past mParentSizeLimit (default 10000). AMCC/Savannah is
      // particularly sensitive to single-module bloat caused by
      // thousands of small module instantiations.  [aron 06-Sep-05]
      //
      // On the other hand, some modules are sufficiently tiny
      // (mTinyThreshold, default 10) that it's a much lesser burden
      // to inline them then to leave in the instances.  Buffers are
      // an obvious example, which turn into continuous assigns and
      // are generally aliased, inducing 0 cost.  However, I set the
      // default at 10 and measured performance across all tests,
      // including AMCC Savannah.  Performance tests are very unstable,
      // but they seem to confirm, at least that Savannah is OK, and
      // there may be an improvement in atop or not.
      // 
      // Some of the concerns Aron may have had were around compile-time,
      // but I've been whittling away at some n^2 issues with other commits,
      // that may have given him pause.

      bool extreme_growth = (submodule_complexity > mTinyThreshold) && // 10
        ((instance_complexity + current_complexity) > mParentSizeLimit); // 10000
      if (extreme_growth and current_status == eStatusSuccess) {
        // allowing all instances of this module is too costly.
        mSubmoduleStatus[module] = eStatusParentGrowth;
      } else {
        // all instances of this module are allowed.
        current_complexity += instance_complexity;
      }
    }
  }
}


FlattenAnalyzer::Status FlattenAnalyzer::analyze(NUModule * submodule) 
{
  Mode submoduleFlattenMode = determineFlattenMode(submodule,mMsgContext,mFlattenMode);
  if (submoduleFlattenMode==eModeUnknown) {
    // error while processing directive.
    return eStatusError;
  }

  bool contains_hier_ref_tasks = submodule->hasHierRefTasks();
  if ((not mFlattenHierRefTasks) and contains_hier_ref_tasks) {
    // tasks with hierarchical references.
    return eStatusHierRefTasks;
  }

  bool submodule_is_leaf = not submodule->hasInstances();
  if (mFlattenOnlyLeaves and not submodule_is_leaf) {
    // If only flattening leaves, disallow modules with instantiated submodules.
    return eStatusNonLeaf;
  }

  if (mFlattenMode==eModeForce or 
      mFlattenMode==eModeForceContents or
      submoduleFlattenMode==eModeForce) {
    // Forced flattening via directive of all instances of this module.
    return eStatusForced;
  }

  if (mFlattenMode==eModeDisable or
      submoduleFlattenMode==eModeDisable) {
    // Disallowed through directive.
    return eStatusDirective;  
  }

  NUCost * submodule_design_cost = mDesignCostCtx->findCost(submodule);
  NU_ASSERT(submodule_design_cost,submodule);

  SInt32 submodule_complexity = computeComplexity(submodule);

  if (mFlattenOnlyLeaves) {

    if (submodule_complexity <= mInlineLimit) {
      // Module smaller than threshold. Allow.
      return eStatusSuccess;
    }

    if (mFlattenSingleInstances) {
      // If this module isn't strictly within size thresholds, the
      // single-instance switch controls its flattening.
      
      SInt32 submodule_unelab_instances = submodule_design_cost->mUnelabInstances;
      SInt32 submodule_inlined_complexity = (submodule_unelab_instances *
                                             submodule_complexity);

#ifdef SMALL_THRESHOLD_IMPROVEMENTS
      // Relating modules with small instance counts to the current
      // threshold appears to restrict flattening too much if a low
      // threshold is used. See the SiCortex MIPS core prior to mixed
      // block merging.
      SInt32 small_instance_inline_limit = std::max(mInlineLimit, 25);
#else
      SInt32 small_instance_inline_limit = mInlineLimit;
#endif

      // SWAG Default allowed: 4*25=100.
      if (submodule_inlined_complexity <= (4 * small_instance_inline_limit)) {
        // If there are modules with a small number of instances, allow
        // flattening as long as the combined complexity is not some
        // multiple of the inline limit. 

        // With a default threshold of 25, this allows 2 instances of
        // complexity 50, 3 instances of complexity 33, etc.

        // Relatively small module or small module with few instances.
        return eStatusSuccess;
      }

      bool submodule_single_unelab_instance = (submodule_unelab_instances == 1);
      if (submodule_single_unelab_instance) {
#ifdef SMALL_THRESHOLD_IMPROVEMENTS
        // Blindly inlining singly-instanced modules may benefit small
        // designs (the SiCortex MIPS core prior to mixed block
        // merging, for example).
        return eStatusSuccess;
#endif

        // SWAG Default allowed: 20*25=500
        if (submodule_complexity < (20 * small_instance_inline_limit)) {
          // Allow almost all singly-instantiated leaf modules to be
          // flattened. Large ones may cause substantial increases in
          // compile times. Don't blindly flatten those unless the
          // threshold requests it.

          // Given a default threshold of 25, we chose a multiple of 20
          // because it allowed most of the singly-instanced modules.
          // Avoiding modules with a complexity greater than 500
          // improved compile and runtime performance.
          return eStatusSuccess;
        }

      }
    }

    return eStatusLargeCandidates;

  } else {

    // ************************************************************
    // ************************************************************
    // NOTE: Thresholds for branch flattening need to be
    // re-determined. The single-instance and other flattening
    // functionality has not been incoorporated into branch
    // flattening.
    // ************************************************************
    // ************************************************************

    // We are flattening leaves and branches. Check both the parent and child sizes.
    SInt32 parent_complexity = computeComplexity(mModule);
    if ( submodule_complexity > mInlineLimit and
         parent_complexity    > mInlineLimit ) {
      // This submodule is too large to be inlined into this parent.
      return eStatusLargeCandidates;
    }

    return eStatusSuccess;
  }
}


SInt32 FlattenAnalyzer::computeComplexity(const NUModule * module) const
{
  // this is self-cost, not deep cost.
  NUCost * cost = mModuleCostCtx->findCost(module);

  NU_ASSERT(cost,module);

  // measure a submodule's complexity based on the number of
  // assignment stmts.

  SInt32 total_assign_cost = (cost->mContAssigns + 
                              cost->mBlockingAssigns + 
                              cost->mNonBlockingAssigns);

  // The complexity of a module is estimated by the number of
  // assignments expected to execute. We derive the execution
  // estimation from the run-instr/instr ratio.

  // The run-instr/instr ratio helps identify modules with a high
  // number of branches. It gives a way of characterizing module
  // "shrinkage". The presence of control structures implies a high
  // probability of post-flattening optimization. 

  // The run-inst/instr ratio is used to reduce the assignment cost
  // based on that optimization probability. Use the bare assignment
  // cost if the instruction cost is zero or if the (run-instr/instr)
  // ratio is greater than one.
  if ((cost->mInstructions > 0) and (cost->mInstructions > cost->mInstructionsRun)) {
    if (cost->mInstructions > (2*cost->mInstructionsRun)) {
      // limit the reduction to 50%
      return (total_assign_cost / 2);
    } else {
      return (total_assign_cost * cost->mInstructionsRun) / cost->mInstructions;
    }
  } else {
    return total_assign_cost;
  }
}


FlattenAnalyzer::Mode FlattenAnalyzer::determineFlattenMode(const NUModule * module, MsgContext * msgContext, Mode currentMode)
{
  Mode nextMode = eModeUnknown;
  const char* moduleName = module->getName()->str();

  if (module->isFlattenModule()) {
    nextMode = eModeForce;
  } else if (module->isFlattenContents()) {
    if (currentMode == eModeForce) {
      // warn about nop.
      if (msgContext)
        msgContext->FlattenModuleContentsUnderModule(moduleName);
      nextMode = currentMode;
    } else {
      nextMode = eModeForceContents;
    }
  } else if (module->isFlattenAllow()) {
    if (currentMode == eModeForce) {
      // warn about nop.
      if (msgContext) msgContext->FlattenAllowUnderModule(moduleName);
      nextMode = currentMode;
    } else if (currentMode == eModeForceContents) {
      // warn about nop.
      if (msgContext) msgContext->FlattenAllowUnderModuleContents(moduleName);
      nextMode = currentMode;
    } else {
      nextMode = eModeEnable;
    }
  } else if (module->isFlattenDisallow()) {
    if (currentMode == eModeForce) {
      // error about inconsistent directives. leave nextMode unknown.
      if (msgContext) msgContext->FlattenDisallowUnderModule(moduleName);
    } else if (currentMode == eModeForceContents) {
      // error about inconsistent directives. leave nextMode unknown.
      if (msgContext) msgContext->FlattenDisallowUnderModuleContents(moduleName);
    } else {
      nextMode = eModeDisable;
    }
  } else {
    // this module was not marked.
    switch(currentMode) {
    case eModeEnable:
    case eModeDisable:
    case eModeForce:
      nextMode = currentMode;
      break;
    case eModeForceContents:
      nextMode = eModeForce;
      break;
    case eModeUnknown:
      nextMode = eModeEnable;
    }
  }

  return nextMode;
}


FlattenAnalyzer::Status FlattenAnalyzer::qualified ( const NUModule * submodule, FlattenAnalyzer::FlattenSize * size ) const
{
  size->child = computeComplexity(submodule);
  size->parent = computeComplexity(mModule);
  size->combined = size->child + size->parent;

  ModuleStatusMap::const_iterator location = mSubmoduleStatus.find(submodule);
  NU_ASSERT(location != mSubmoduleStatus.end(), submodule);
  Status reason = location->second;
  if (reason == eStatusForced) {
    return eStatusSuccess; // Forced is an internal state. Callers see it as an allowed flatten.
  } else {
    return reason;
  }
}
