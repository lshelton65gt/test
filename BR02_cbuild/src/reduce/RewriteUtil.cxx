// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/RewriteUtil.h"

#include "nucleus/NUNet.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"

#include "reduce/Fold.h"

/*!
  \file
  Implementation of rewrite utility package.
*/


NUNet * RewriteLeaves::operator()(NUNet * net, Phase phase)
{
  if (not isPre(phase))
    return 0;

  return getReplacementNet(net);
}


NUTF * RewriteLeaves::operator()(NUTF * tf, Phase phase)
{
  if (not isPre(phase))
    return 0;

  NUTF * replacement = NULL;

  NUTFReplacementMap::iterator location = mTFReplacements.find(tf);
  if ( location != mTFReplacements.end() ) {
    replacement = location->second;
  }
  return replacement;
}


NUAlwaysBlock * RewriteLeaves::operator()(NUAlwaysBlock * always, Phase phase)
{
  if (not isPre(phase))
    return 0;

  NUAlwaysBlock * replacement = NULL;

  NUAlwaysBlockReplacementMap::iterator location = mAlwaysReplacements.find(always);
  if ( location != mAlwaysReplacements.end() ) {
    replacement = location->second;
  }
  return replacement;
}


NUCModel * RewriteLeaves::operator()(NUCModel * cmodel, Phase phase)
{
  if (not isPre(phase))
    return 0;

  NUCModel * replacement = NULL;

  NUCModelReplacementMap::iterator location;
  location = mCModelReplacements.find(cmodel);
  if ( location != mCModelReplacements.end() ) {
    replacement = location->second;
  }
  return replacement;
}

NULvalue * RewriteLeaves::operator()(NULvalue * lvalue, Phase phase)
{
  NULvalue * replacement = NULL;

  if ((isPre(phase) && !mPostReplacement) || (isPost(phase) && mPostReplacement)) {
    /* transform on the way down */
    if (const NUIdentLvalue * ident = dynamic_cast<NUIdentLvalue*>(lvalue))
      replacement = makeIdentLvalue(ident);
    else if (dynamic_cast<NUVarselLvalue*>(lvalue))
      // Varsels are not leaves, so keep replacement == NULL
      ;
    else if (dynamic_cast<NUMemselLvalue*>(lvalue))
      // Memsels are not leaves
      ;
    else if (dynamic_cast<NUConcatLvalue*>(lvalue))
      ;
    else
      NU_ASSERT(0, lvalue);

    if (replacement) {
      delete lvalue;
    }
  }

  if (isPost(phase) && mFold)
  {
    /* fold on the way back up */
    NULvalue* target = replacement ? replacement : lvalue;
    NULvalue* folded = mFold->fold(target);
    if (folded != target)
      replacement = folded;
  }

  return replacement;
}


NUExpr * RewriteLeaves::operator()(NUExpr * rvalue, Phase phase)
{
  NUExpr* replacement = NULL;
 
  if ((isPre(phase) && !mPostReplacement) || (isPost(phase) && mPostReplacement)) {
    /* transform on the way down */
    const NUIdentRvalue  * ident = dynamic_cast<const NUIdentRvalue*>(rvalue);

    if (ident) {
      replacement = makeIdentRvalue(ident);
    }

    if (replacement)
    {
      UInt32 rSize = rvalue->getBitSize ();
      replacement = replacement->makeSizeExplicit(rSize);
      replacement->setSignedResult (rvalue->isSignedResult ());
      if (!rvalue->isManaged())
        delete rvalue;
    }
  }

  if (isPost(phase) && mFold)
  {
    /* fold on the way back up */
    NUExpr* target = replacement ? replacement : rvalue;
    NUExpr* folded = mFold->fold(target);
    if (folded != target)
      replacement = folded;
  }

  return replacement;
}


NULvalue * RewriteLeaves::makeIdentLvalue(const NUIdentLvalue * ident)
{
  NULvalue * replacement = makeIdentLvalueFromLvalue(ident);
  if (not replacement) {
    replacement = makeIdentLvalueFromRvalue(ident);
  }
  if (not replacement) {
    replacement = makeIdentLvalueFromNet(ident);
  }
  return replacement;
}


NUExpr * RewriteLeaves::makeIdentRvalue(const NUIdentRvalue * ident)
{
  NUExpr * replacement = makeIdentRvalueFromLvalue(ident);
  if (not replacement) {
    replacement = makeIdentRvalueFromRvalue(ident);
  }
  if (not replacement) {
    replacement = makeIdentRvalueFromNet(ident);
  }
  return replacement;
}

/*
 * The makeIdentXvalueFromYvalue() functions are all pretty much identical.  It looks like
 * makeIdentRvalueFromRvalue() *could* be greatly simplified
 */
NULvalue * RewriteLeaves::makeIdentLvalueFromLvalue(const NUIdentLvalue * ident)
{
  NULvalue * replacement = NULL;
    
  const NUNet * net = ident->getIdent();
  const NULvalue * repl = getReplacementLvalue(net);
  if (repl) {
    CopyContext copy_context(NULL,NULL);
    replacement = repl->copy (copy_context);
  }
  return replacement;
}


NUExpr * RewriteLeaves::makeIdentRvalueFromLvalue(const NUIdentRvalue * ident)
{
  NUExpr * replacement = NULL;

  const NUNet * net = ident->getIdent();
  const NULvalue * repl = getReplacementLvalue(net);
  if (repl) {
    replacement = repl->NURvalue ();
  }
  return replacement;
}


NULvalue * RewriteLeaves::makeIdentLvalueFromRvalue(const NUIdentLvalue * ident)
{
  NULvalue * replacement = NULL;

  const NUNet * net = ident->getIdent();
  const NUExpr * repl = getReplacementRvalue(net);
  if (repl) {
    replacement = repl->Lvalue (repl->getLoc ());
  }
  return replacement;
}


NUExpr * RewriteLeaves::makeIdentRvalueFromRvalue(const NUIdentRvalue * ident)
{
  NUExpr * replacement = NULL;

  const NUNet * net = ident->getIdent();
  const NUExpr * repl = getReplacementRvalue(net);
  if (repl) {
    CopyContext copy_context (0,0);
    replacement = repl->copy (copy_context);
  }
  return replacement;
}

NULvalue * RewriteLeaves::makeIdentLvalueFromNet(const NUIdentLvalue * ident)
{
  NULvalue * replacement = NULL;
  const NUNet * net = ident->getIdent();
  NU_ASSERT(net != NULL, ident);
  const SourceLocator & loc = ident->getLoc();
  NUNet * replacement_net = getReplacementNet(net);
  if (replacement_net) {
    replacement = new NUIdentLvalue(replacement_net, loc);
  }
  return replacement;
}


NUExpr * RewriteLeaves::makeIdentRvalueFromNet(const NUIdentRvalue * ident)
{
  NUExpr * replacement = NULL;
  const NUNet * net = ident->getIdent();
  NU_ASSERT(net != NULL, ident);
  const SourceLocator & loc = ident->getLoc();
  NUNet * replacement_net = getReplacementNet(net);
  if (replacement_net) {
    replacement = new NUIdentRvalue(replacement_net, loc);
  }
  return replacement;
}

const NULvalue * RewriteLeaves::getReplacementLvalue(const NUNet * net)
{
  INFO_ASSERT(net, "bad RewriteLeaves::getReplacementLvalue call");
  const NULvalue * replacement = NULL;
  NULvalueReplacementMap::const_iterator location = mLvalueReplacements.find(net);
  if (location != mLvalueReplacements.end()) {
    replacement = location->second;
  }
  return replacement;
}


const NUExpr * RewriteLeaves::getReplacementRvalue(const NUNet * net)
{
  INFO_ASSERT(net, "bad RewriteLeaves::getReplacementRvalue call");
  const NUExpr * replacement = NULL;
  NUExprReplacementMap::const_iterator location = mRvalueReplacements.find(net);
  if (location != mRvalueReplacements.end()) {
    replacement = location->second;
  }
  return replacement;
}


NUNet * RewriteLeaves::getReplacementNet(const NUNet * net)
{
  INFO_ASSERT(net, "bad RewriteLeaves::getReplacementNet call");
  NUNet * replacement = NULL;
  NUNetReplacementMap::iterator location = mNetReplacements.find(net);
  if ( location != mNetReplacements.end() ) {
    replacement = location->second;
    NU_ASSERT(net->getBitSize() == replacement->getBitSize(), net);
  }
  return replacement;
}

NUExpr* REReplaceDeepExprExpr::operator()(NUExpr* expr, Phase phase)
{
  NUExpr* replacement = NULL;
  if (phase == ePost) {
    NUExprExprDeepHashMap::const_iterator pos = mExprMap.find(expr);
    if (pos != mExprMap.end()) {
      replacement = pos->second;
      CopyContext cntxt(0,0);
      replacement = replacement->copy(cntxt);
    }
  }

  // If we are going to replace this expression, the old one is no
  // longer needed; delete it.
  if (replacement != NULL) {
    delete expr;
  }
  return replacement;
}

NULvalue* REReplaceDeep::operator()(NULvalue* lvalue, Phase phase)
{
  NULvalue* replacement = NULL;
  if (phase == ePost) {
    NULvalLvalDeepHashMap::const_iterator pos = mLvalMap.find(lvalue);
    if (pos != mLvalMap.end()) {
      replacement = pos->second;
      CopyContext cntxt(0,0);
      replacement = replacement->copy(cntxt);
    }
  }

  // If we are going to replace this lvalue, the old one is no
  // longer needed; delete it.
  if (replacement != NULL) {
    delete lvalue;
  }
  return replacement;
}
