// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Lvalue simplification pass.  Walk the design folding all lvalue
 * trees into simpler forms.
 */

#include "FoldI.h"

NULvalue*
FoldI::foldDownToPartsel (NUVarselLvalue* partsel, const NUBinaryOp* downto)
{
  NULvalue* result = NULL;

  // Does the downto range evaluate to a constant width?
  const NUExpr* msbExpr = binaryOp(NUOp::eBiMinus,
                                   manage(downto->getArg(0), true),
                                   manage(downto->getArg(1), true),
                                   downto->getLoc (),
                                   downto->getBitSize ());
  const NUExpr* foldedMSBExpr = foldAndManage(msbExpr, false);
  if (const NUConst* cWidthMinusOne = foldedMSBExpr->castConst ())
  {
    UInt32 msb;
    cWidthMinusOne->getUL (&msb);

    // Now we have a constant width beginning at downto....
    result = new NUVarselLvalue (partsel->getLvalue ()->copy(mCC),
                                 downto->getArg(1)->copy(mCC),
                                 ConstantRange (msb, 0),
                                 partsel->getLoc ());
  }
  else
  {
    // If the binary expression created to determine if width is constant
    // folds, but not to a constant, the changed flag remains set. Unset it.
    // Testcase: test/vhdl/lang_vslice/vec_slice.vhdl
    mChanged = false;
  }
  return result;
} // FoldI::foldDownToPartsel

NULvalue*
FoldI::fold (NUConcatLvalue* concat)
{
  NULvalue* folded = NULL;

  // A concat of a single item can be replaced by the item itself
  if (concat->getNumArgs() == 1)
  {
    folded = concat->getArg(0)->copy(mCC);
  }
  else
  {
    // fold {a[3],a[2:1],a[0]} ==> a[3:0]
    // fold {a,{b,{c,d}}}      ==> {a,b,c,d}

    NULvalue* arg = NULL;
    NULvalueSet ownedExprs;

    bool modified = false;

    NULvalueVector newVec;
    UInt32 concatLen = concat->getNumArgs ();
    for (UInt32 n = 0,skip=0; n < concatLen; ++n)
    {
      arg = concat->getArg(n);
      if (skip)                 // We merged previous with this.
      {
        skip = 0;
        continue;
      }

      NUConcatLvalue* sub_concat = dynamic_cast<NUConcatLvalue*>(arg);
      if (sub_concat) {
        // Collapse a nested concat into its parent.
        UInt32 sub_concatLen = sub_concat->getNumArgs ();
        for (UInt32 sub_n = 0; sub_n < sub_concatLen; ++sub_n) {
          NULvalue * sub_arg = sub_concat->getArg(sub_n);
          newVec.push_back (sub_arg);
        }
        modified = true;
        continue;
      }

      if (n == concatLen-1)     // We're on the very last entry
      {
        newVec.push_back (arg);
        break;
      }

      NUVarselLvalue* varsel = dynamic_cast<NUVarselLvalue*>(arg);
      if (varsel == NULL)
      {
        newVec.push_back (arg);
        continue;
      }

      NUVarselLvalue* neighbor = dynamic_cast<NUVarselLvalue*>(concat->getArg(n+1));
      if (neighbor == NULL)
      {
        newVec.push_back (arg);
        continue;
      }

      NULvalue *merged = varsel->catenate (neighbor, mCC);

      if (merged) {
        modified = true;
        ownedExprs.insert (merged);

        newVec.push_back (merged);
        skip = 1;
      } else {
        newVec.push_back (arg);
      }
    }

    if (modified) {
      // Copy any arguments that we didn't fold
      for(NULvalueVector::iterator i = newVec.begin (); i!= newVec.end (); ++i) {
        if (ownedExprs.find (*i) == ownedExprs.end ()) {
          // We didn't create it, so we have to make a new copy
          *i = (*i)->copy(mCC);
        }
      }
      folded = new NUConcatLvalue (newVec, concat->getLoc ());
    }
  }

  if (folded != NULL)
  {
    folded->resize();
    folded = finish(concat, folded);
  }

  return folded;
}

/* Fix for bug1391. Simplify LHS part selects for vectorization to work. */
NULvalue*
FoldI::fold (NUVarselLvalue *varsel)
{

  NUBinaryOp* indexExpr = dynamic_cast<NUBinaryOp*>(varsel->getIndex ());
  const NUExpr* term = NULL;
  const NUConst* offset = NULL;
  SInt32 offsetS;
  UInt32 offsetU;
  NULvalue* folded = foldPartselLvalue(varsel->getLvalue(),
                                       varsel->getIndex (),
                                       *varsel->getRange(),
                                       varsel->getLoc(), true);

  if (folded)
  {
    ;
  } else if (!varsel->isConstIndex ()
             && getConstantOffset (indexExpr, &term, &offset)
             && offset->getL (&offsetS))
  {
    ConstantRange r (*varsel->getRange ());
    r.adjust (offsetS);
    const NUExpr* protectedIdx = protectIndexExpr (manage(term, true));
    NUVarselLvalue* foldedVarsel =
      new NUVarselLvalue (varsel->getLvalue ()->copy(mCC),
                          protectedIdx->copy(mCC), r,
                          varsel->getLoc ());
    foldedVarsel->putIndexInBounds(varsel->isIndexInBounds());
    folded = foldedVarsel;
  } else if (term != NULL && offset != NULL 
    && offset->getUL (&offsetU)
    && (offsetU & 0x70000000)) {
    // Looks like it was a negative number and getL did not want to return it
    // as a negative number. All index computations are performed as 32-bit,
    // twos complemenet arithmetic, so we can turn this into a subtraction.
    ConstantRange r (*varsel->getRange ());
    SInt32 negative = (UInt32) offsetU;
    r.adjust (negative);
    const NUExpr* protectedIdx = protectIndexExpr(manage(term, true));
    folded = new NUVarselLvalue (varsel->getLvalue ()->copy(mCC),
                                 protectedIdx->copy(mCC), r,
                                 varsel->getLoc ());
  } else if ( varsel->sizeVaries() )
  {
    folded = foldDownToPartsel (varsel, indexExpr);
    if (folded == varsel)
      folded = NULL;
  }

  if (folded != NULL)
  {
    folded->resize();
    finish(varsel, folded);
  }

  return folded;
}

/*! Possibly return a folded expression equivalent to an NUVarselLvalue
 *  with the given ident and range.
 */
NULvalue* FoldI::foldPartselLvalue(const NULvalue* ident,
                                   const NUExpr* index,
                                   const ConstantRange& ps,
                                   const SourceLocator& loc,
                                   bool isPartsel)
{
  NULvalue* result = NULL;

  // The partselect has a constant index if NULL was passed or the index is a constant
  bool constantIndex = isConstantIndexExpr (index);

  if (const NUIdentLvalue*  id = dynamic_cast<const NUIdentLvalue*>(ident))
  {
    if (NUVectorNet* vn = dynamic_cast<NUVectorNet*>(id->getIdent()))
    {
      const ConstantRange& vecRange(*vn->getRange());
      
      if (not constantIndex)
        // non-constant indexing expression
        ;
      else if (ps == vecRange)
        // Accessing the whole vector
        result = id->copy(mCC);
      else if (not vecRange.overlaps(ps)) // Completely out-of-range
      {
        // Do nothing -- we can actually remove this entire statement,
        // but there is no way to indicate that to the upper layers,
        // and it is unsafe to do anything locally unless we are sure
        // that we are being called through an upper level function
        // that expects it.
        // We just ignore the fold opportunity here, it's handled elsewhere
      }
      else if (ps.getLsb () >= 0) // don't fall into x[2:-4] trap, can't reduce here!
      {
        // Create a new varsel lvalue with a reduced range
        const ConstantRange r = vecRange.overlap(ps);
        if (not isPartsel || r != ps)
        {
          result = new NUVarselLvalue (id->copy(mCC), r, loc);
          result->resize();
        }
      }
    }
    else if (ps.getLsb() == 0 && ps.getLength() >= 1)
    {
      // This a select of a scalar, and the range includes 0
      result = id->copy(mCC);
    }
    else
    {
      // This is a select of a scalar, and the range doesn't include 0
      // We just ignore the fold opporunity here, it's handled elsewhere
    }
  }
  else if (const NUMemselLvalue* memsel = dynamic_cast<const NUMemselLvalue*>(ident))
  {
    NUMemoryNet* mem = memsel->getIdent()->getMemoryNet();
    NU_ASSERT(mem, memsel);

    const ConstantRange& widthRange(*mem->getWidthRange());

    if (not constantIndex)
      ;
    else if (ps == widthRange)
      // Accessing the whole row
      result = memsel->copy(mCC);
    else if (not widthRange.overlaps(ps)) // Completely out-of-range
    {
      // We just ignore the fold opporunity here, it's handled elsewhere
    }
    else
    {
      const ConstantRange r = widthRange.overlap(ps);
      
      if (!isPartsel || (r != ps))
      {
        // Create a new varsel lvalue with a reduced range
        result = new NUVarselLvalue (memsel->copy(mCC), r, loc);
        result->resize();
      }
    }
  }
  else if (const NUVarselLvalue* varsel = dynamic_cast<const NUVarselLvalue*>(ident))
  {
    if (not varsel->isConstIndex () || not constantIndex)
    {
      // looking at a reference with one or more non-constant index terms in the varsel
      // 
      //       net[H:L][i+:W]    => net[i + L + W.lsb +: W.length]
      //       net[i+:W][H:L]    => net[i + L + W.lsb +: H+1-L]
      //       net[i+:W1][j+:W2] => net[i + j + W1.lsb + W2.lsb +: W2.length]
      //
      // [remember that index expression could have a constant term that's actually stored
      //  in the ConstantRange associated with the Varsel.]
      // 
      // But folding this suppresses some bounds checking, so don't do it unless
      // we are sure there are no OOB references.
      //
      // TODO: If we were smart enough to know that the index expression is guaranteed
      //       to be in-bounds we could be more aggressive in collapsing these.

      if (not mNOOOB)
        return NULL;            // have to be careful about non-constant indices

      ConstantRange baseRange = *(varsel->getRange ());
      NUExpr *newIndex = NUConst::create(false, ps.getLsb () + baseRange.getLsb (),
                                         LONG_BIT, loc);

      if (varsel->isConstIndex ())
        newIndex = new NUBinaryOp(NUOp::eBiPlus, // new_nucleus_ok
                                  varsel->getIndex()->copy(mCC),
                                  newIndex, loc);

      if (not constantIndex)
        newIndex = new NUBinaryOp(NUOp::eBiPlus, // new_nucleus_ok
                                  index->copy(mCC), newIndex, loc);

      ConstantRange r (ps.getLength () - 1, 0);
      return new NUVarselLvalue (varsel->getLvalue()->copy(mCC), newIndex, r, loc);

    }

    // Fold net[a:b][c:d] into net[b+c:b+d].  b+c <= a.
    ConstantRange innerRange(*varsel->getRange());
    ConstantRange outerRange(ps);
    outerRange.adjust(innerRange.getLsb());
    
    if (innerRange.overlaps(outerRange))
    {
      innerRange = innerRange.overlap (outerRange);
      if (innerRange !=  *(varsel->getRange ()))
        // Not just producing the same partsel
        result = foldPartselLvalue(varsel->getLvalue(), 0, innerRange, loc, false);
      else 
        result = varsel->copy(mCC);
    }
  }
  else if (const NUConcatLvalue* concat = dynamic_cast<const NUConcatLvalue*>(ident))
  {
    if (constantIndex)
      result = foldConcatPartselLvalue(concat, ps.getMsb(), ps.getLsb(), loc);
  }
  else
  {
    // no optimization performed
  }

  return result;
}  // NUExpr* FoldI::foldPartselLvalue

// check for {w, x, y, z}[2:1], transform it to {x, y}.
// Be sure to deal with the more general form:
//      {v[0], w[3:0], x[3:0], y[3:0], z[0]}[10:3] = {w[1:0], x[3:0], y[3:2]}
//
// It's easier to use SInt32 for the dimensions so we can decrement our counter
// past lsb and test it.
//
// If the concat contains arbitrary expressions, we will end up with an illegal partsel. 
// These must be eliminated or the fold can't be permitted to 'stand'.
//
NULvalue* FoldI::foldConcatPartselLvalue(const NUConcatLvalue* concat,
                                         SInt32 msb, SInt32 lsb,
                                         const SourceLocator& loc)
{
  NULvalueVector v;

  NULvalue* folded = NULL;      // Assume no luck folding.

  // Something like:
  //   {a,b,c}[H:L] = v;
  // xform into
  //  {a[H-b.width-c.width, L+b.width,c.width], b[H-c.width:L+c.width], c[H:L]
  // where we elide any concat operand that doesn't overlap with the fundemental
  // width of the partsel-ed thing.

  SInt32 lowPos = 0;

  for (SInt32 n = concat->getNumArgs () - 1; n >= 0; --n)
  {
    NULvalue* piece = concat->getArg(n);
    SInt32 highPos = piece->getBitSize () + lowPos - 1;  // upper bound represented by piece

    if (lsb <= highPos)
    {
      // We want this one in our new range

      ConstantRange r (std::min (highPos, msb), lsb);
      r.adjust (-lowPos);       // Shift relative to current aggregate memeber
      NULvalue *lv = foldPartselLvalue (piece, 0, r, loc, false);

      if (not lv)
        // no more contributions (if any)
        break;

      v.push_back (lv);         // In reverse order

      lsb += r.getLength ();    // advance where remaining lsb's come from
    }

    if (msb <= highPos)
      // extracted all the bits we need...
      break;

    lowPos = highPos+1;          // Advance to where next concat member will start
  }
  // v now has the pieces that participate in the partselect of the concat,
  // an empty vector would indicate that the partselect was completely out-of-range
  // of the vector and need to suppress the whole assignment - that's done
  // by FoldI::foldAssign()
  //
  if (not v.empty ())
  {
    std::reverse (v.begin (), v.end ()); // Put into forward order
    folded = new NUConcatLvalue (v, loc);
    folded->resize ();
  }

  return folded;
}

// Connection to the outside world.  Can be called to transform a single
// LHS.  Always returns a valid LHS
//
NULvalue*
FoldI::fold (NULvalue *lhs)
{
  // Simplify the sub-lhs tree
  lhs->replaceLeaves (*this);

  // Dispatch to the private type-dependent converters to fold this
  // particular expression, and keep folding while a transform works
  // (non-zero implies we changed something, keep trying)
  while (NULvalue *result = operator()(lhs, ePrePost))
    lhs = result;

  return lhs;
}

NULvalue*
FoldI::operator()(NULvalue *e, Phase)
{

  switch (e->getType ())
  {
  default:
    return NULL;

  case eNUConcatLvalue:
    return fold (dynamic_cast<NUConcatLvalue*>(e));

  case eNUVarselLvalue:
    return fold (dynamic_cast<NUVarselLvalue*>(e));
  }
}

