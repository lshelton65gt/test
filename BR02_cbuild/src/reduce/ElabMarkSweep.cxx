// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/ElabMarkSweep.h"
#include "nucleus/NUDesign.h"

#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "flow/FLFactory.h"
#include "iodb/IODBNucleus.h"

#include "nucleus/NUCycle.h"

/*!
  \file
  Implementation of elaborated flow mark/sweep package.
*/

void ElabMarkSweep::mark(NUDesign *this_design)
{
  clearMarkAllFlow(mFlowFactory);
  markDesignFlow(this_design);
}


void ElabMarkSweep::clearMarkAllFlow(FLNodeElabFactory * flow_factory)
{
  FLNodeElab* flnode = NULL;
  for (FLNodeElabFactory::FlowLoop p = flow_factory->loopFlows(); p(&flnode);)
    flnode->putIsReachable(false);
}


void ElabMarkSweep::sweep(SweepCallback &callback)
{
  FLNodeElab* flnode = NULL;
  for (FLNodeElabFactory::FlowLoop p = mFlowFactory->loopFlows(); p(&flnode);)
  {
    callback(flnode, not flnode->isReachable());
  }
}


void ElabMarkSweep::markDesignFlow(NUDesign *design)
{
  UtSet<NUCycle*> coveredCycles;

  FLDesignElabIter flow_iter(design,
			     mSymtab,
			     FLIterFlags::eClockMasters,   // no locals, include clock masters
			     FLIterFlags::eAll,            // visit all nets
			     FLIterFlags::eNone,           // stop at no nets
			     FLIterFlags::eBranchAtAll,    // branch at all nesting
			     FLIterFlags::eIterNodeOnce);  // visit each node only once

  for (; not flow_iter.atEnd(); ++flow_iter) {
    FLNodeElab *flnode = *flow_iter;
    flnode->putIsReachable(true);

    // if this node is in a cycle, mark all nodes in the cycle live
    if (mFlowFactory->isInCycle(flnode))
    {
      flnode = mFlowFactory->getAcyclicNode(flnode);
      flnode->putIsReachable(true); // mark the FLNodeElabCycle live, too
      NUCycle* cycle = flnode->getCycle();
      FLN_ELAB_ASSERT(cycle,flnode);
      if (coveredCycles.find(cycle) == coveredCycles.end())
      {
        FLNodeElab* cycleNode;
        for (NUCycle::FlowLoop p = cycle->loopFlows(); p(&cycleNode);)
          cycleNode->putIsReachable(true);
        coveredCycles.insert(cycle);
      }
    }
  }
}


