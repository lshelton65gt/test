// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/CleanElabFlow.h"
#include "reduce/ElabMarkSweep.h"
#include "flow/FLNodeElab.h"
#include "flow/FLNode.h"
#include "flow/FLIter.h"
#include "flow/FLFactory.h"

#include "util/UtIOStream.h"
#include "util/OSWrapper.h"

/*!
  \file
  Implementation of elaborated flow cleanup package.
 */


void CleanElabFlow::design(NUDesign *design, FLNodeElabSet* deleted_flow_elabs,
                           bool deleteFlows)
{
  ElabMarkSweep marker(mFlowFactory, mSymtab);
  marker.mark(design);

  mDeleteFlows = deleteFlows;
  deleteDeadFlow(deleted_flow_elabs);
}


void CleanElabFlow::deleteDeadFlow(FLNodeElabSet* deleted_flow_elabs)
{
  if (mVerbose) {
    UtIO::cout() << "CleanElabFlow found the following dead flow:" << UtIO::endl;
  }

  mDeadFlowFound = false;
  mDeleteFlows = true;
  Callback callback(this,deleted_flow_elabs);

  ElabMarkSweep marker(mFlowFactory, mSymtab);
  marker.sweep(callback);

  if (mVerbose and not mDeadFlowFound) {
    UtIO::cout() << "  No elaborated dead flow was found." << UtIO::endl;
  }
}


void CleanElabFlow::removeFlow(FLNodeElab *flnode, 
                               FLNodeElabSet *deleted_flow_elabs,
                               bool is_dead)
{
  if (not is_dead) {
    return;
  }

  if (not flnode->isReachable()) {
    if (mVerbose) {
      flnode->print(0,2);
    }
    flnode->getDefNet()->removeContinuousDriver(flnode);
    if (mDeleteFlows)
      mFlowFactory->destroy(flnode);
    if (deleted_flow_elabs) {
      deleted_flow_elabs->insert(flnode);
    }
    mDeadFlowFound = true;
  }
}

