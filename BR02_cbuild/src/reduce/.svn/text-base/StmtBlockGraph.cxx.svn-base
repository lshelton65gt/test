// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/StmtBlockGraph.h"

#include "nucleus/NUCase.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"

#include "util/UtStringUtil.h"

template <>
void NUNetRefMultiMap<StmtBlockGraph::Node*>::helperTPrint(StmtBlockGraph::Node* const & val, int indent) const {
  val->getData()->getStmt()->printVerilog(1,indent,1);
}


StmtBlockIfData::StmtBlockIfData(NUIf * if_stmt) :
  StmtBlockCompoundStmtData(if_stmt)
{}

StmtBlockCaseData::StmtBlockCaseData(NUCase * case_stmt) :
  StmtBlockCompoundStmtData(case_stmt)
{}

StmtBlockGraph * StmtBlockGraphFactory::create(const NUStmtList * stmts)
{
  StmtBlockGraph* graph = new StmtBlockGraph;

  // Map of currently-active blocking defs to their graph nodes.
  AliasBucketNetRefStmtBlockGraphMap block_def_map(mAliasQuery, mNetRefFactory);

  // Map of currently-active non-blocking defs to their graph nodes.
  AliasBucketNetRefStmtBlockGraphMap non_block_def_map(mAliasQuery, mNetRefFactory);
  
  // Map of every def which has a non-blocking def to the nodes which make up those defs.
  NUNetRefStmtBlockGraphNodeMultiMap all_non_block_def_map(mNetRefFactory);

  // Map of uses which are not yet succeeded by a def of the net.
  // We keep these around so we can get WAR control dependencies.
  // Note that nets are removed from this map once they have a write, since
  // then the dependency is transitively covered through WAW.
  AliasBucketNetRefStmtBlockGraphMap live_use_map(mAliasQuery, mNetRefFactory);

  // Map of every stmt to its graph node.
  typedef UtMap<NUStmt*, StmtBlockGraph::Node*> StmtNodeMap;
  StmtNodeMap stmt_node_map;

  // Build the graph nodes and perform some checks on the stmt list
  for (NUStmtList::const_iterator iter = stmts->begin();
       iter != stmts->end();
       ++iter)
  {
    NUStmt *cur_stmt = *iter;
    
    // Make sure for backend processing, there are no non-blocking assignments.
    NU_ASSERT( !mExpectAllBlocking || (cur_stmt->getType() != eNUNonBlockingAssign), cur_stmt );
    
    // This stmt's node.
    StmtBlockGraph::Node* stmt_node = createNodeAndComponents(graph, cur_stmt);
    stmt_node_map[cur_stmt] = stmt_node;
  }

  // Add edges to the graph for all ordering dependencies
  for (NUStmtList::const_iterator iter = stmts->begin();
       iter != stmts->end();
       ++iter)
  {
    NUStmt *cur_stmt = *iter;
    StmtBlockGraph::Node* stmt_node = stmt_node_map[cur_stmt];

    updateDependencies(graph, stmt_node, &block_def_map, &non_block_def_map, &live_use_map);
      
    // Place this stmt as currently-active defs.  If they are killing defs, then clear
    // out the other active defs.
    NUNetRefSet defs(mNetRefFactory);
    NUNetRefSet kills(mNetRefFactory);
    cur_stmt->getBlockingDefs(&defs);
    cur_stmt->getBlockingKills(&kills);
    processDefsAndKills(stmt_node, defs, kills,
                        &block_def_map, NULL);
    defs.clear();
    kills.clear();

    cur_stmt->getNonBlockingDefs(&defs);
    cur_stmt->getNonBlockingKills(&kills);
    processDefsAndKills(stmt_node, defs, kills,
                        &non_block_def_map, &all_non_block_def_map);
  }

  // Object to assist with overlap detection, which is possible-alias aware.
  NetRefOverlapsQuery query(mAliasQuery);

  // Iterate through the stmts once more, for every statement which uses a net
  // which has a non-blocking def, create a dependency between the non-blocking
  // defs and the use (non-blocking control dependency).

  // The def should come after the use. These arcs can create cyclic
  // situations which require breaking.
  for (StmtNodeMap::iterator iter = stmt_node_map.begin();
       iter != stmt_node_map.end();
       ++iter) {
    NUStmt *cur_stmt = iter->first;
    StmtBlockGraph::Node* cur_node = iter->second;
    
    NUNetRefSet uses(mNetRefFactory);
    cur_stmt->getBlockingUses(&uses);
    cur_stmt->getNonBlockingUses(&uses);
    
    for (NUNetRefStmtBlockGraphNodeMultiMap::MapLoop loop = all_non_block_def_map.loop();
         not loop.atEnd();
         ++loop) {
      NUNetRefHdl def_net_ref = (*loop).first;
      NUNet *def_net = def_net_ref->getNet();
      StmtBlockGraph::Node *def_node = (*loop).second;

      bool need_dependency = false;
      if (uses.find(def_net_ref, &NUNetRef::overlapsSameNet) != uses.end()) {
        need_dependency = true;
      } else {
        // Detect possible elaborated alias between the use nets and
        // the def net.  For any, add an arc.
        for (NUNetRefSet::iterator iter = uses.begin();
             not need_dependency and (iter != uses.end());
             ++iter) {
          NUNetRefHdl use_net_ref = *iter;

          if (not use_net_ref->empty()) {
            // The NetRefOverlapsQuery interface is a little bit cumbersome here,
            // but using it so that we don't replicate the logic.
            if (query(def_net, *use_net_ref) and query(*def_net_ref, *use_net_ref)) {
              need_dependency = true;
            }
          }
        }
      }

      NUStmt * def_stmt = def_node->getData()->getStmt();
      NUStmt * cur_stmt = cur_node->getData()->getStmt();
      // For loops need self-arcs because they model cyclic execution.
      if (need_dependency and
          ((def_stmt!=cur_stmt) or (def_stmt->getType()==eNUFor))) {
        StmtBlockGraph::Edge* edge = new StmtBlockGraph::Edge(cur_node, NULL);
        def_node->addEdge(edge);
        graph->setFlags(edge, StmtBlockGraph::NON_BLOCKING | StmtBlockGraph::DEF_AFTER | StmtBlockGraph::USE);
      }
    }
  }

  removePessimisticNetRefEdges(graph);

  return graph;
}


void StmtBlockGraphFactory::destroy(StmtBlockGraph ** pgraph)
{
  for (Iter<GraphNode*> loop = (*pgraph)->nodes(); !loop.atEnd(); ++loop)
  {
    StmtBlockGraph::Node* node = (*pgraph)->castNode(*loop);
    StmtBlockStmtData * data = node->getData();
    delete data;
  }
  delete (*pgraph);
  (*pgraph) = NULL;
}

void StmtBlockGraphFactory::addDependencies(StmtBlockGraph* graph,
                                            StmtBlockGraph::Node* stmt_node,
                                            UInt32 edge_type,
                                            NUNetRefSet& current_net_refs,
                                            AliasBucketNetRefStmtBlockGraphMap* prior_net_ref_map,
                                            AliasBucketNetRefStmtBlockGraphMap* current_net_ref_map)
{
  NetRefOverlapsQuery query(mAliasQuery);

  // Go through all net refs in the current set, adding edges to overlapping
  // or possible elaborated aliases of prior net refs.
  for (NUNetRefSetIter cur_iter = current_net_refs.begin();
       cur_iter != current_net_refs.end();
       ++cur_iter) {
    NUNetRefHdl cur_net_ref = *cur_iter;
    NUNet* cur_net = cur_net_ref->getNet();

    // cur_net may be NULL if there is a constant use (empty net ref)
    if (not cur_net) {
      continue;
    }

    // Iterate over the possibly-aliased prior netrefs.
    NUNetRefStmtBlockGraphNodeMultiMap* net_ref_map = prior_net_ref_map->getNetRefMap(cur_net_ref);
    for (NUNetRefStmtBlockGraphNodeMultiMap::CondMapLoop prior_loop = net_ref_map->loop(cur_net_ref, &query);
         not prior_loop.atEnd();
         ++prior_loop) {
      StmtBlockGraph::Node* depend_node = (*prior_loop).second;
      if (depend_node != stmt_node) {
        StmtBlockGraph::Edge* edge = new StmtBlockGraph::Edge(depend_node, NULL);
        stmt_node->addEdge(edge);
        graph->setFlags(edge, edge_type);
      }
    }

    // Store the current net ref for future use in a prior net ref map
    if (current_net_ref_map) {
      current_net_ref_map->insert(cur_net_ref, stmt_node);
    }
  }
}


void StmtBlockGraphFactory::processDefsAndKills(StmtBlockGraph::Node* stmtNode, NUNetRefSet& defs, NUNetRefSet& kills,
                                                AliasBucketNetRefStmtBlockGraphMap* defNetRefMap,
                                                NUNetRefStmtBlockGraphNodeMultiMap* secondaryNetRefMap)
{
  for (NUNetRefSetIter defIter = defs.begin();
       defIter != defs.end();
       ++defIter) {
    NUNetRefHdl defNetRef = *defIter;
      
    NUNetRefSetIter killIter = kills.find(defNetRef, &NUNetRef::overlapsSameNet);
    if (killIter != kills.end()) {
      NUNetRefHdl killNetRef = *killIter;
      NUNetRefHdl intersectNetRef = mNetRefFactory->intersect(defNetRef, killNetRef);
      NU_ASSERT2(not intersectNetRef->empty(),defNetRef,killNetRef);
      defNetRefMap->erase(intersectNetRef);
    }
    defNetRefMap->insert(defNetRef, stmtNode);
    if (secondaryNetRefMap)
      secondaryNetRefMap->insert(defNetRef, stmtNode);
  }
}


void StmtBlockGraphFactory::removePessimisticNetRefEdges(StmtBlockGraph* graph)
{
  for (Iter<GraphNode*> n = graph->nodes(); !n.atEnd(); ++n)
  {
    GraphNode* node = *n;
    StmtBlockGraph::Node* from = graph->castNode(node);
    UtList<GraphEdge*> badEdges;
    for (Iter<GraphEdge*> e = graph->edges(from); !e.atEnd(); ++e)
    {
      GraphEdge* edge = *e;
      StmtBlockGraph::Node* to = graph->castNode(graph->endPointOf(edge));
      if (!graph->anyFlagSet(edge, StmtBlockGraph::USE_AFTER | StmtBlockGraph::USE))
      {
        // this is a def->def dependency.  we can look at the lvalue
        // (if they are assignments) to check for bit-level separation
        const NUAssign* assign_a = dynamic_cast<const NUAssign*>(from->getData());
        const NUAssign* assign_b = dynamic_cast<const NUAssign*>(to->getData());
        if (assign_a && assign_b)
        {
          const NULvalue* lv_a = assign_a->getLvalue();
          const NULvalue* lv_b = assign_b->getLvalue();
          const NUVarselLvalue* varsel_a = dynamic_cast<const NUVarselLvalue*>(lv_a);
          const NUVarselLvalue* varsel_b = dynamic_cast<const NUVarselLvalue*>(lv_b);
          if (varsel_a && varsel_b &&
              (*(varsel_a->getLvalue()) == *(varsel_b->getLvalue())) &&
              varsel_a->isConstIndex() && varsel_b->isConstIndex())
          {
            const ConstantRange* range_a = varsel_a->getRange();
            const ConstantRange* range_b = varsel_b->getRange();
            if (!range_a->overlaps(*range_b))
            {
              // this is a pessimistic dependency -- remove the edge
              badEdges.push_back(edge);
            }
          }
        }
      }
    }
    // remove any bad edges we found for this node
    for (UtList<GraphEdge*>::iterator e = badEdges.begin(); e != badEdges.end(); ++e)
    {
      StmtBlockGraph::Edge* edge = graph->castEdge(*e);
      from->removeEdge(edge);
      delete edge;
    }
  }
}


StmtBlockGraph::Node * StmtBlockGraphFactory::createNodeWithData(StmtBlockGraph * graph,
                                                                 StmtBlockStmtData * node_data)
{
  StmtBlockGraph::Node * node = new StmtBlockGraph::Node(node_data);

  // Add the node to the graph.
  graph->addNode(node);

  return node;
}


void StmtBlockGraphFactory::createIfComponentNode(StmtBlockGraph * graph,
                                                  StmtBlockGraph::Node* stmt_node,
                                                  StmtBlockIfData *stmt_data,
                                                  StmtBlockIfComponentData::ComponentType component_type)
{
  StmtBlockIfComponentData * component_data = new StmtBlockIfComponentData(stmt_node,component_type);
  StmtBlockGraph::Node * component_node = createNodeWithData(graph,component_data);

  // Make sure the if statement knows about its components.
  stmt_data->addComponent(component_node);
}


void StmtBlockGraphFactory::createCaseComponentNode(StmtBlockGraph * graph,
                                                    StmtBlockGraph::Node* stmt_node,
                                                    StmtBlockCaseData *stmt_data,
                                                    NUCaseItem * item,
                                                    StmtBlockCaseComponentData::ComponentType component_type)
{
  StmtBlockCaseComponentData * component_data = new StmtBlockCaseComponentData(stmt_node,item,component_type);
  StmtBlockGraph::Node * component_node = createNodeWithData(graph,component_data);

  // Make sure the if statement knows about its components.
  stmt_data->addComponent(component_node);
}


StmtBlockGraph::Node* StmtBlockGraphFactory::createNodeAndComponents(StmtBlockGraph * graph, 
                                                                     NUStmt * stmt)
{
  StmtBlockGraph::Node* stmt_node = NULL;

  // Create the node(s) associated with a particular statement. For if
  // and case statements, we create graph nodes for individual
  // components (selects, stmt-lists, etc) as well as a node
  // representing the entire statement.

  if (stmt->getType()==eNUIf) {
    // Add a node for the complete if statement.
    NUIf * if_stmt = dynamic_cast<NUIf *>(stmt);
    StmtBlockIfData * stmt_data = new StmtBlockIfData(if_stmt);
    stmt_node = createNodeWithData(graph,stmt_data);

    // Add specific components for the if, then, and else components of this if statement.
    createIfComponentNode(graph, stmt_node, stmt_data, StmtBlockIfComponentData::CONDITION);
    createIfComponentNode(graph, stmt_node, stmt_data, StmtBlockIfComponentData::THEN_CLAUSE);
    createIfComponentNode(graph, stmt_node, stmt_data, StmtBlockIfComponentData::ELSE_CLAUSE);

  } else if (stmt->getType()==eNUCase) {
    // Add a node for the complete case statement.
    NUCase * case_stmt = dynamic_cast<NUCase *>(stmt);
    StmtBlockCaseData * stmt_data = new StmtBlockCaseData(case_stmt);
    stmt_node = createNodeWithData(graph,stmt_data);

    // Add a specific component for the selector.
    createCaseComponentNode(graph, stmt_node, stmt_data, NULL, StmtBlockCaseComponentData::SELECT);

    // Add specific components for each case item of this case statement.
    for (NUCase::ItemLoop item_loop = case_stmt->loopItems();
         not item_loop.atEnd();
         ++item_loop) {
      NUCaseItem *item = *item_loop;
      createCaseComponentNode(graph, stmt_node, stmt_data, item, StmtBlockCaseComponentData::ITEM_CONDITION);
      createCaseComponentNode(graph, stmt_node, stmt_data, item, StmtBlockCaseComponentData::ITEM_CLAUSE);
    }

  } else {
    // Generic statement data.
    StmtBlockStmtData * stmt_data = new StmtBlockStmtData(stmt);
    stmt_node = createNodeWithData(graph,stmt_data);
  }

  return stmt_node;
}


void StmtBlockGraphFactory::updateDependencies(StmtBlockGraph * graph,
                                               StmtBlockGraph::Node* stmt_node,
                                               AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                               AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                               AliasBucketNetRefStmtBlockGraphMap* live_use_map)
{
  StmtBlockStmtData * stmt_data = stmt_node->getData();
  NUStmt            * stmt  = stmt_data->getStmt();

  NUNetRefSet defs(mNetRefFactory);
  stmt->getBlockingDefs(&defs);
  stmt->getNonBlockingDefs(&defs);
    
  NUNetRefSet uses(mNetRefFactory);
  stmt->getBlockingUses(&uses);
  stmt->getNonBlockingUses(&uses);
    
  updateDependenciesForNode(graph, stmt_node, defs, uses, 
                            block_def_map, non_block_def_map,
                            live_use_map);
  defs.clear();
  uses.clear();

  // Add specialized arcs for compound statements.
  if (stmt->getType()==eNUIf) {
    updateDependenciesForIfComponents(graph, stmt_node,
                                      block_def_map, non_block_def_map,
                                      live_use_map);
  } else if (stmt->getType()==eNUCase) {
    updateDependenciesForCaseComponents(graph, stmt_node,
                                        block_def_map, non_block_def_map,
                                        live_use_map);
  }
}


void StmtBlockGraphFactory::updateDependenciesForIfComponents(StmtBlockGraph * graph,
                                                              StmtBlockGraph::Node* stmt_node,
                                                              AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                                              AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                                              AliasBucketNetRefStmtBlockGraphMap* live_use_map)
{
  StmtBlockStmtData * stmt_data = stmt_node->getData();
  NUStmt            * stmt  = stmt_data->getStmt();
  StmtBlockIfData * if_data = dynamic_cast<StmtBlockIfData*>(stmt_data);
  NUIf * if_stmt = dynamic_cast<NUIf*>(stmt);
  NU_ASSERT(if_stmt and if_data, stmt);
  for (StmtBlockCompoundStmtData::ComponentLoop loop = if_data->loopComponents();
       not loop.atEnd();
       ++loop) {
    StmtBlockGraph::Node * component_node = (*loop);
    StmtBlockStmtData * generic_data = component_node->getData();
    StmtBlockIfComponentData * component_data = dynamic_cast<StmtBlockIfComponentData*>(generic_data);
    NU_ASSERT(component_data, generic_data->getStmt());

    switch(component_data->getComponentType()) {
    case StmtBlockIfComponentData::CONDITION: {
      NUNetRefSet cond_uses(mNetRefFactory);
      if_stmt->getCond()->getUses(&cond_uses);
      // Conditions only have a RAW dependencies.
      addDependencies(graph, component_node, StmtBlockGraph::BLOCKING | StmtBlockGraph::USE_AFTER | StmtBlockGraph::DEF, cond_uses,
                      block_def_map, live_use_map);
      break;
    }
    case StmtBlockIfComponentData::THEN_CLAUSE: {
      updateDependenciesForClause(graph, component_node, if_stmt->loopThen(),
                                  block_def_map,
                                  non_block_def_map,
                                  live_use_map);
      break;
    }
    case StmtBlockIfComponentData::ELSE_CLAUSE: {
      updateDependenciesForClause(graph, component_node, if_stmt->loopElse(),
                                  block_def_map,
                                  non_block_def_map,
                                  live_use_map);
      break;
    }
    }
  }
}


void StmtBlockGraphFactory::updateDependenciesForCaseComponents(StmtBlockGraph * graph,
                                                                StmtBlockGraph::Node* stmt_node,
                                                                AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                                                AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                                                AliasBucketNetRefStmtBlockGraphMap* live_use_map)
{
  StmtBlockStmtData * stmt_data = stmt_node->getData();
  NUStmt            * stmt  = stmt_data->getStmt();
  StmtBlockCaseData * case_data = dynamic_cast<StmtBlockCaseData*>(stmt_data);
  NUCase * case_stmt = dynamic_cast<NUCase*>(stmt);
  NU_ASSERT(case_stmt and case_data, stmt);

  for (StmtBlockCompoundStmtData::ComponentLoop loop = case_data->loopComponents();
       not loop.atEnd();
       ++loop) {
    StmtBlockGraph::Node * component_node = (*loop);
    StmtBlockStmtData * generic_data = component_node->getData();
    StmtBlockCaseComponentData * component_data = dynamic_cast<StmtBlockCaseComponentData*>(generic_data);
    NU_ASSERT(component_data, generic_data->getStmt());

    NUCaseItem * item = component_data->getItem();

    switch(component_data->getComponentType()) {
    case StmtBlockCaseComponentData::SELECT: {
      NUNetRefSet sel_uses(mNetRefFactory);
      case_stmt->getSelect()->getUses(&sel_uses);
      // Selects only have a RAW dependencies.
      addDependencies(graph, component_node, StmtBlockGraph::BLOCKING | StmtBlockGraph::USE_AFTER | StmtBlockGraph::DEF, sel_uses,
                      block_def_map, live_use_map);
      break;
    }
    case StmtBlockCaseComponentData::ITEM_CONDITION: {
      NU_ASSERT(item, case_stmt);
      NUNetRefSet cond_uses(mNetRefFactory);
      for (NUCaseItem::ConditionLoop cond_loop = item->loopConditions();
           not cond_loop.atEnd();
           ++cond_loop) {
        NUCaseCondition * condition = *cond_loop;
        condition->getUses(&cond_uses);
      }
      // Conditions only have a RAW dependencies.
      addDependencies(graph, component_node, StmtBlockGraph::BLOCKING | StmtBlockGraph::USE_AFTER | StmtBlockGraph::DEF, cond_uses,
                      block_def_map, live_use_map);
      break;
    }
    case StmtBlockCaseComponentData::ITEM_CLAUSE: {
      NU_ASSERT(item, case_stmt);
      updateDependenciesForClause(graph, component_node, item->loopStmts(),
                                  block_def_map,
                                  non_block_def_map,
                                  live_use_map);
      break;
    }
    }
  }
}


void StmtBlockGraphFactory::updateDependenciesForClause(StmtBlockGraph * graph,
                                                        StmtBlockGraph::Node* component_node,
                                                        NUStmtLoop loop,
                                                        AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                                        AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                                        AliasBucketNetRefStmtBlockGraphMap* live_use_map)
{
  NUNetRefSet defs(mNetRefFactory);
  NUNetRefSet uses(mNetRefFactory);

  calculateDefsAndUsesForClause(loop, &defs, &uses);

  updateDependenciesForNode(graph,
                            component_node,
                            defs, uses,
                            block_def_map,
                            non_block_def_map,
                            live_use_map);
}


void StmtBlockGraphFactory::updateDependenciesForNode(StmtBlockGraph * graph,
                                                      StmtBlockGraph::Node* stmt_node,
                                                      NUNetRefSet & defs,
                                                      NUNetRefSet & uses,
                                                      AliasBucketNetRefStmtBlockGraphMap* block_def_map,
                                                      AliasBucketNetRefStmtBlockGraphMap* non_block_def_map,
                                                      AliasBucketNetRefStmtBlockGraphMap* live_use_map)
{
  // All uses depend on the currently live blocking defs (RAW data dependency).
  addDependencies(graph, stmt_node, StmtBlockGraph::BLOCKING | StmtBlockGraph::USE_AFTER | StmtBlockGraph::DEF, uses,
                  block_def_map, live_use_map);
    
  // All defs must come after current live defs (WAW control dependency).
  addDependencies(graph, stmt_node, StmtBlockGraph::BLOCKING | StmtBlockGraph::DEF_AFTER | StmtBlockGraph::DEF, defs,
                  block_def_map, NULL);
  addDependencies(graph, stmt_node, StmtBlockGraph::NON_BLOCKING | StmtBlockGraph::DEF_AFTER | StmtBlockGraph::DEF, defs,
                  non_block_def_map, NULL);
                    
  // And, they must come after the current live reads (WAR control dependency).
  addDependencies(graph, stmt_node, StmtBlockGraph::BLOCKING | StmtBlockGraph::DEF_AFTER | StmtBlockGraph::USE, defs,
                  live_use_map, NULL);
}


void StmtBlockGraphFactory::calculateDefsAndUsesForClause(NUStmtLoop loop, 
                                                          NUNetRefSet * defs,
                                                          NUNetRefSet * uses)
{
  NUNetRefSet killed(mNetRefFactory);
  for ( ; not loop.atEnd(); ++loop) {
    NUStmt * stmt = (*loop);
    NUNetRefSet local_uses(mNetRefFactory);
    stmt->getBlockingUses(&local_uses);
    stmt->getNonBlockingUses(&local_uses);

    // subtract the set of netrefs that have been killed by this loop.
    NUNetRefSet::set_difference(local_uses,killed,*uses);

    stmt->getBlockingDefs(defs);
    stmt->getNonBlockingDefs(defs);

    stmt->getBlockingKills(&killed);
  }
}


void StmtBlockGraphDotWriter::printNode(Graph* g, GraphNode* node)
{
  StmtBlockGraph* sg = (StmtBlockGraph*) g;
  StmtBlockGraph::Node* n = sg->castNode(node);
  StmtBlockStmtData * data = n->getData();

  // Compound nodes trigger the creation of a subgraph cluster, which
  // causes all the compound statement's components to be grouped
  // together in the graph rendering.
  if (data->isCompoundStatement()) {
    // We're more than just a node! We have parts!
    *mOut << "subgraph cluster_" << node << " {";

    NUStmt* stmt = data->getStmt();
    UtString buf;
    stmt->compose(&buf,NULL);
    UtString buf2;
    StringUtil::escapeCString(buf.c_str(), &buf2, 'l');
    *mOut << "label = \"" << buf2 << "\";\n";
    *mOut << "color = blue;\n"
          << "node [style=filled];\n";

    StmtBlockCompoundStmtData * compound_data = dynamic_cast<StmtBlockCompoundStmtData*>(data);
    for (StmtBlockCompoundStmtData::ComponentLoop loop = compound_data->loopComponents();
         not loop.atEnd();
         ++loop) {
      StmtBlockGraph::Node * component_node = (*loop);

      *mOut << "\"" << component_node << "\";\n";
    }
    *mOut << "}\n";
  }

  // Every node gets a normal entry, as well.
  GraphDotWriter::printNode(g,node);
}

void StmtBlockGraphDotWriter::writeLabel(Graph* graph, GraphNode* node)
{
  StmtBlockGraph* sg = (StmtBlockGraph*) graph;
  StmtBlockGraph::Node* n = sg->castNode(node);
  StmtBlockStmtData * data = n->getData();

  // The label for a component is the type of that component.
  if (data->isStatementComponent()) {
    StmtBlockStmtComponentData * component_data = dynamic_cast<StmtBlockStmtComponentData*>(data);
    NU_ASSERT(component_data, data->getStmt());

    *mOut << "[ label = \"" << component_data->getComponentTypeString() << "\";\n";
  } else {
    // Otherwise, the label is the Verilog text of the statement.
    NUStmt* stmt = data->getStmt();
    UtString buf;
    stmt->compose(&buf,NULL);
    UtString buf2;
    StringUtil::escapeCString(buf.c_str(), &buf2, 'l');
    *mOut << "[ label = \"" << buf2 << "\" ]";
  }
}

void StmtBlockGraphDotWriter::writeLabel(Graph* graph, GraphEdge* edge)
{
  UtString buf;
  if (graph->anyFlagSet(edge, StmtBlockGraph::NON_BLOCKING))
    buf << "label = \"NONBLK\"";
  else
    buf << "label = \"BLOCK\"";
  if (graph->anyFlagSet(edge, StmtBlockGraph::USE_AFTER))
    buf << ", taillabel = \"U\"";
  else
    buf << ", taillabel = \"D\"";
  if (graph->anyFlagSet(edge, StmtBlockGraph::USE))
    buf << ", headlabel = \"U\"";
  else
    buf << ", headlabel = \"D\"";
  *mOut << "[ " << buf << " ]";
}
