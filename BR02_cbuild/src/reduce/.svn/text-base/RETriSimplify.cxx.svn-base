// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "flow/FLNode.h"

#include "nucleus/NUAssign.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"

#include "reduce/Fold.h"
#include "reduce/RETriSimplify.h"

#include "util/CbuildMsgContext.h"
#include "util/UtList.h"
#include "util/UtHashSet.h"

/*!
  \file
  Implementation of tristate optimization pass
 */

RETriSimplify::RETriSimplify(Fold* fold, bool verbose, MsgContext* mc,
                             TristateModeT tristateMode, IODBNucleus* iodb):
  mVerbose(verbose),
  mTristateMode(tristateMode),
  mFold(fold),
  mMsgContext(mc),
  mIODB(iodb)
{
  mNewDrivers = new NUEnabledDriverList;
  mKillDrivers = new NUEnabledDriverSet;
  mCostContext = new NUCostContext;
  mModule = NULL;
}

RETriSimplify::~RETriSimplify() {
  delete mNewDrivers;
  delete mKillDrivers;
  delete mCostContext;
}

static bool sIsConstant(NUExpr* expr, bool* isTrue) {
  NUConst* k = expr->castConst();
  *isTrue = false;
  
  if ((k != NULL) && !k->hasXZ()) {
    *isTrue = !k->isZero();
    return true;
  }
  return false;
}

bool RETriSimplify::module(NUModule* mod) {
  bool workDone = false;
  mNewDrivers->clear();
  mKillDrivers->clear();
  mModule = mod;

  for (NUModule::NetLoop p = mod->loopNets(); !p.atEnd(); ++p) {
    NUNet* net = *p;
    if (net->isMultiplyDriven()) {
      workDone |= processNet(net);
    } // if
  } // for

  for (NUModule::ContEnabledDriverLoop p = mod->loopContEnabledDrivers();
       !p.atEnd(); ++p)
  {
    NUEnabledDriver* tri = *p;
    workDone |= processEnabledDriver(tri);
  }
  if (workDone) {
    mod->replaceContEnabledDrivers(*mNewDrivers);
  }

  mModule = NULL;
  return workDone;
}

bool RETriSimplify::processEnabledDriver(NUEnabledDriver* tri) {
  bool workDone = false;
  NUEnabledDriverSet::iterator not_found = mKillDrivers->end();
  if (mKillDrivers->find(tri) != not_found) {
    delete tri;
  }
  else {
    // Before replacing the enabled driver, see if its
    // enable is constant 1, and if so, just make it a
    // continuous assign
    NUExpr* ena = tri->getEnable();
    bool isTrue;
    if (sIsConstant(ena, &isTrue)) {
      // Change enabled drivers with constant 1 enables into assigns
      if (isTrue) {
        CopyContext cc(NULL, NULL);
        NUExpr* drv = tri->getDriver()->copy(cc);
        NULvalue* lval = tri->getLvalue()->copy(cc);
        StringAtom* sym = mModule->gensym("trireduce", NULL, lval);
        const SourceLocator& loc = tri->getLoc();
        Strength strength = tri->getStrength();
        NUContAssign* ca = new NUContAssign(sym, lval, drv, loc, strength);
        mModule->addContAssign(ca);
      }

      // Change enabled drivers with constant 0 assigns into nothing
      workDone = true;
      if (mVerbose) {
       mMsgContext->RETriElim(&tri->getLoc());
      }
      delete tri;
    }

    else {
      mNewDrivers->push_back(tri);
    }
  }
  return workDone;
} // bool RETriSimplify::processEnabledDriver

bool RETriSimplify::processNet(NUNet* net) {
  NUEnabledDriverList enabled_drivers;
  Strength strength = eStrPull;
  NULvalue* lval = NULL;
  bool ret = false;

  if (qualifyNet(net, &enabled_drivers, &lval, &strength)) {
    ret = optimizeNet(net, enabled_drivers, lval, strength);
  }
  return ret;
}

// Determine whether a net is qualified to be tristate-simplified,
// and gather up the list of enabled drivers while we are figuring
// it out
bool RETriSimplify::qualifyNet(NUNet* net,
                               NUEnabledDriverList* enabled_drivers,
                               NULvalue** lval,
                               Strength* strength)
{
  bool isMultiplyDriven = false;
  NUContEnabledDriver* tri = NULL;

  for (NUNet::DriverLoop p = net->loopContinuousDrivers();
       !p.atEnd(); ++p)
  {
    FLNode* flow = *p;
    if (flow->getType() == eFLBoundHierRef) {
      continue;
    }
    NUUseDefNode* node = flow->getUseDefNode();
    if ((node == NULL) ||
        ((tri = dynamic_cast<NUContEnabledDriver*>(node)) == NULL))
    {
      // This net is driven by something other than an enabled driver
      // --> reject
      return false;
    }
    else if (*lval == NULL) {
      // This is the first lval we have found, so establish its
      // parameters as the benchmark to compare to the others
      *lval = tri->getLvalue();
      *strength = tri->getStrength();
    }
    else if (!((**lval == *tri->getLvalue()) &&
               (*strength == tri->getStrength())))
    {
      // This lvalue/strength does not match all the other drivers --> reject
      return false;
    }
    else {
      isMultiplyDriven = true;
    }
    enabled_drivers->push_back(tri);
  } // for
  return isMultiplyDriven;
} // bool RETriSimplify::qualifyNet

bool RETriSimplify::optimizeNet(NUNet* net,
                                const NUEnabledDriverList& enabled_drivers,
                                NULvalue* lval,
                                Strength strength)
{
  // Accumulate the enables and drives together, Fold, and compare costs
  NUCost origCost, newCost;
  NUExpr* ena = NULL, *drv = NULL;
  collectEnablesAndDrives(enabled_drivers, &ena, &drv, &origCost,
                          lval->getBitSize());
  StringAtom* name = net->getScope()->gensym("enadriv", NULL, lval);
  CopyContext cc(NULL, NULL);
  lval = lval->copy(cc);
  NUContEnabledDriver* newEd = NULL;
  NUContAssign* contAssign = NULL;
  const SourceLocator& loc = drv->getLoc();

  if (canEliminateTristate(ena)) {
    delete ena;
    contAssign = new NUContAssign(name, lval, drv, loc, strength);
    contAssign->calcCost(&newCost, mCostContext);
  }
  else {
    newEd = new NUContEnabledDriver(name, lval, ena, drv,
                                    strength, false, loc);
    newEd->calcCost(&newCost, mCostContext);
  }

  // If the combined driver is a measurable win, then mutate the
  // design and return true, indicating that work was done.
  if (newCost.mInstructionsRun < origCost.mInstructionsRun) {
    // Kill the old drivers and add the new driver
    mKillDrivers->insert(enabled_drivers.begin(), enabled_drivers.end());
    if (newEd != NULL) {
      mNewDrivers->push_back(newEd);
      repairFlow(net, newEd);
    }
    else {
      mModule->addContAssign(contAssign);
      repairFlow(net, contAssign);
    }

    return true;
  } // if
  else if (newEd != NULL) {
    // Otherwise, we've made things worse somehow, kill the new
    // driver and leave well enough alone
    delete newEd;
  }
  else {
    delete contAssign;
  }
  return false;
} // bool RETriSimplify::optimizeNet

// Having changed the drivers for a net, repair the continuously
// driving flow-nodes for that net.  Note that we reject any net
// this is driven by anything other than an NUEnabledDriver*, so
// we don't have to worry about nested flow
void RETriSimplify::repairFlow(NUNet* net, NUUseDefNode* replacementNode) {
  // Fix the flow nodes for all the net drivers to point to the
  // optimized usedef node
  for (NUNet::DriverLoop p = net->loopContinuousDrivers(); !p.atEnd(); ++p) {
    FLNode* flow = *p;
    if (flow->getType() != eFLBoundHierRef) {
      if (mVerbose) {
        NUUseDefNode* oldUD = flow->getUseDefNode();
        NU_ASSERT(oldUD->getType() == eNUContEnabledDriver, oldUD);
        const SourceLocator& oldLoc = oldUD->getLoc();
        mMsgContext->RETriReduce(&oldLoc);
      }
      flow->setUseDefNode(replacementNode);
    }
  }
}

void RETriSimplify::collectEnablesAndDrives(const NUEnabledDriverList& enabled_drivers,
                                            NUExpr** ena, NUExpr** drv, NUCost* origCost,
                                            UInt32 bitSize)
{
  CopyContext cc(NULL, NULL);

  // Walk through the enabled drivers and collect all the enables
  for (NUEnabledDriverList::const_iterator p = enabled_drivers.begin(),
         e = enabled_drivers.end(); p != e; ++p)
  {
    NUEnabledDriver* ed = *p;
    ed->calcCost(origCost, mCostContext);
    NUExpr* edEna = ed->getEnable()->copy(cc);
    NUExpr* edDrv = ed->getDriver()->copy(cc);
    if (*ena == NULL) {
      *ena = edEna;
      *drv = edDrv;
    }
    else {
      *ena = new NUBinaryOp(NUOp::eBiBitOr, edEna, *ena, edEna->getLoc());
      edEna = edEna->copy(cc);
      *drv = new NUTernaryOp(NUOp::eTeCond, edEna, edDrv, *drv,
                             edDrv->getLoc());
    }
  }
  (*ena)->resize(1);
  *ena = mFold->fold(*ena);
  (*drv)->resize(bitSize);
  *drv = mFold->fold(*drv);
} // void RETriSimplify::collectEnablesAndDrives

// Based on this enable, is it acceptable to eliminate the tristate
// altogether, and replace with a continuous assign?
bool RETriSimplify::canEliminateTristate(NUExpr* ena)
{
  // If the enables for this net guarantee that it will be driven, then
  // we can eliminate the tristate altogether, as we will never generate
  // a Z.
  bool isTrue;
  if (sIsConstant(ena, &isTrue) && isTrue) {
    return true;
  }

  return false;
}
