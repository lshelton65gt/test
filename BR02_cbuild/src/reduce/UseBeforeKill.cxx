// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "reduce/UseBeforeKill.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUStmt.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUTF.h"

/*!
  \file
  Finds places in block where nets are used before being killed, ie.,
  used uninitialized.
*/

UseBeforeKill::UseBeforeKill(NUNetRefFactory* factory) 
  : mNetRefFactory(factory)
{
  mUseBeforeKillNetRefs = new NUNetRefSet(mNetRefFactory);
  mKillNetRefs = new NUNetRefSet(mNetRefFactory);
  mUBKNets = new NetToLoc;
}


UseBeforeKill::~UseBeforeKill() {
  delete mUseBeforeKillNetRefs;
  delete mKillNetRefs;
  delete mUBKNets;
}

static void sAppendRefs(NUNetRefSet* dstSet, const NUNetRefSet& srcSet)
{
  dstSet->insert(srcSet.begin(), srcSet.end());
}


void UseBeforeKill::stmtList(NUStmtLoop stmts, NUNetRefSet* killNetRefs) {
  NUNetRefSet usedNets(mNetRefFactory);
  
  for (; !stmts.atEnd(); ++stmts) {
    NUStmt* stmt = *stmts;
    
    usedNets.clear();
    
    switch (stmt->getType()) {
    case eNUCase:
      caseStmt((NUCase*) stmt, killNetRefs);
      break;
    case eNUIf:
      ifStmt((NUIf*) stmt, killNetRefs);
      break;
    case eNUFor:
      forStmt((NUFor*) stmt, killNetRefs);
      break;
    case eNUBlock:
      blockStmt((NUBlock*) stmt, killNetRefs);
      break;

      // These cases for which we will do nothing in this routine are
      // explicitly left in here, rather than using a default, so that we
      // if someone adds a new type then he will get a compile warning
      // and be given the opportunity to consider whether this new statement
      // type must be explicity handled here.
    case eNUAlwaysBlock:
    case eNUAssign:
    case eNUBlockingAssign:
    case eNUCModelCall:
    case eNUCModel:
    case eNUCModelArgConnectionInput:
    case eNUCModelArgConnectionOutput:
    case eNUCModelArgConnectionBid:
    case eNUConcatLvalue:
    case eNUContAssign:
    case eNUControlSysTask:
    case eNUCycle:
    case eNUEnabledDriver:
    case eNUIdentLvalue:
    case eNUInitialBlock:
    case eNULvalue:
    case eNUMemselLvalue:
    case eNUModule:
    case eNUModuleInstance:
    case eNUNonBlockingAssign:
    case eNUOutputSysTask:
    case eNUInputSysTask:
    case eNUFOpenSysTask:
    case eNUFCloseSysTask:
    case eNUFFlushSysTask:
    case eNUPortConnectionBid:
    case eNUPortConnectionInput:
    case eNUPortConnectionOutput:
    case eNUStructuredProc:
    case eNUTF:
    case eNUTFArgConnectionBid:
    case eNUTFArgConnectionInput:
    case eNUTFArgConnectionOutput:
    case eNUTFElab:
    case eNUTask:
    case eNUTaskEnable:
    case eNUTriRegInit:
    case eNUPartselIndexLvalue:
    case eNUReadmemX:
    case eNUSysRandom:
    case eNUVarselLvalue:
    case eNUBlockingEnabledDriver:
    case eNUContEnabledDriver:
    case eNUNamedDeclarationScope:
    case eNUBreak:
    case eNUCompositeLvalue:
    case eNUCompositeIdentLvalue:
    case eNUCompositeSelLvalue:
    case eNUCompositeFieldLvalue:
      stmt->getBlockingUses(&usedNets);
      stmt->getNonBlockingUses(&usedNets);
      checkUses(&usedNets, *killNetRefs, stmt->getLoc());
      updateKillRefs(stmt, killNetRefs);
      break;
    } // switch
  } // for
} // void UseBeforeKill::stmtList

void UseBeforeKill::caseStmt(NUCase* caseStmt, NUNetRefSet* collectKills) {
  NUNetRefSet condUses(mNetRefFactory);
  caseStmt->getSelect()->getUses(&condUses);
  checkUses(&condUses, *collectKills, caseStmt->getLoc());

  // set used for trigger checks. Triggers don't kill, so this set
  // won't change.
  NUNetRefSet caseKills(*collectKills);

  for (NUCase::ItemLoop items_iter = caseStmt->loopItems();
       !items_iter.atEnd();
       ++items_iter)
  {
    NUCaseItem* caseItem = *items_iter;

    // Collect the nets used in the item trigger, adding them to those used by
    // the case selector.
    for (NUCaseItem::ConditionLoop p = caseItem->loopConditions();
         !p.atEnd(); ++p)
    {
      NUCaseCondition* cond = *p;
      const SourceLocator& loc = cond->getLoc();
      condUses.clear();
      cond->getExpr()->getUses(&condUses);
      checkUses(&condUses, caseKills, loc);
      
      NUExpr* mask = cond->getMask();
      if (mask != NULL) {
        condUses.clear();
        mask->getUses(&condUses);
        checkUses(&condUses, caseKills, loc);
      }
    }

    // Each case statement independently kills
    NUNetRefSet caseItemKills(*collectKills);    
    stmtList(caseItem->loopStmts(), &caseItemKills);
  }

  updateKillRefs(caseStmt, collectKills);
}

void UseBeforeKill::ifStmt(NUIf* ifStmt, NUNetRefSet* collectKills) {
  NUNetRefSet condUses(mNetRefFactory);
  ifStmt->getCond()->getUses(&condUses);
  checkUses(&condUses, *collectKills, ifStmt->getLoc());

  {
    NUNetRefSet ifKills(*collectKills);
    stmtList(ifStmt->loopThen(), &ifKills);
  }

  {
    NUNetRefSet ifKills(*collectKills);
    stmtList(ifStmt->loopElse(), &ifKills);
  }
  
  updateKillRefs(ifStmt, collectKills);
}

void UseBeforeKill::checkUses(NUNetRefSet* usedNetRefs, 
                              const NUNetRefSet& collectKills,
                              const SourceLocator& curLoc)
{
  if (! usedNetRefs->empty())
  {
    // Run through the uses, intersect the kills, masking them out,
    // leaving only un-killed uses
    NUNetRefSet tmpUsed(mNetRefFactory);
    NUNetRefSet::set_difference(*usedNetRefs, collectKills, tmpUsed);
    if (! tmpUsed.empty())
    {
      sAppendRefs(mUseBeforeKillNetRefs, tmpUsed);
      addUninitReadLoc(tmpUsed, curLoc);
    }
  }
}

void UseBeforeKill::addUninitReadLoc(const NUNetRefSet& ubkNets, 
                                     const SourceLocator& loc)
{
  for (NUNetRefSetIter p = ubkNets.begin(), e = ubkNets.end(); p != e;
       ++p)
  {
    const NUNetRefHdl& srcHdl = *p;
    NUNet* net = srcHdl->getNet();
    // only keep the first occurrence of a bad read
    if (mUBKNets->find(net) == mUBKNets->end())
      (*mUBKNets)[net] = &loc;
  }
}

void UseBeforeKill::updateKillRefs(NUStmt* stmt, NUNetRefSet* killRefs)
{
  if (killRefs)
  {
    NUNetRefSet tmpKillNets(mNetRefFactory);
    stmt->getBlockingKills(&tmpKillNets);
    sAppendRefs(killRefs, tmpKillNets);
  }
}

void UseBeforeKill::blockStmt(NUBlock* block, NUNetRefSet* collectKills) {
  stmtList(block->loopStmts(), collectKills);
  updateKillRefs(block, collectKills);
}

void UseBeforeKill::forStmt(NUFor* forStmt, NUNetRefSet* collectKills) {
  NUNetRefSet killRefs(*collectKills);
  
  // Check the initial block for uninitialized read
  stmtList(forStmt->loopInitial(), &killRefs);
  
  // Check the condition for uninitialized read.
  NUNetRefSet condUses(mNetRefFactory);
  forStmt->getCondition()->getUses(&condUses);
  checkUses(&condUses, killRefs, forStmt->getLoc());

  /*
    Technically, the advance statement could reference or kill an
    output, but it must be checked after the body statement. We only
    need to loop the body and the advance statement once to see if an
    uninitialized read happens.
  */
  stmtList(forStmt->loopBody(), &killRefs);
  stmtList(forStmt->loopAdvance(), &killRefs);
  
  updateKillRefs(forStmt, collectKills);
}

void UseBeforeKill::clear() {
  mUseBeforeKillNetRefs->clear();
  mKillNetRefs->clear();
}


void UseBeforeKill::addFullyKilled(NUNet* killedNet)
{
  NUNetRefHdl hdl = mNetRefFactory->createNetRef(killedNet);
  mKillNetRefs->insert(hdl);
}

bool UseBeforeKill::hasUBKNets() const
{
  return ! mUBKNets->empty();
}

void UseBeforeKill::intersectNetLocs(NetToLoc* result, const NUNetSet& queryNets) const
{
  for (NUNetSet::SortedLoop p = queryNets.loopSorted(); ! p.atEnd(); ++p)
  {
    NUNet* net = *p;
    NetToLoc::iterator q = mUBKNets->find(net);
    if (q != mUBKNets->end())
    {
      const SourceLocator* loc = q->second;
      (*result)[net] = loc;
    }
  }
}

// entry points
void UseBeforeKill::tf(NUTF* tf) {
  // only output nets can be read uninitialized inside a task
  for (NUNetVectorLoop tfArgLoop = tf->loopArgs();
       ! tfArgLoop.atEnd(); ++tfArgLoop)
  {
    NUNet* tfParam = *tfArgLoop;
    bool addToSet = false;
    int argIndex = tf->getArgIndex (tfParam);
    NU_ASSERT (argIndex >= 0, tf);
    NUTF::CallByMode mode = tf->getArgMode (argIndex);
    switch (mode) {
    case NUTF::eCallByCopyOut:
      break;
      
    case NUTF::eCallByAny: 
    case NUTF::eCallByReference: // input
    case NUTF::eCallByValue: // copy in
    case NUTF::eCallByCopyInOut: // copy in/out
      addToSet = true;
      break;
    }
    
    if (addToSet)
      addFullyKilled(tfParam);
  }
  
  stmtList(tf->loopStmts(), mKillNetRefs);
}

void UseBeforeKill::block(NUBlock* block) {
  stmtList(block->loopStmts(), mKillNetRefs);
}
