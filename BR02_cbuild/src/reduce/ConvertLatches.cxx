// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "util/CbuildMsgContext.h"
#include "util/UtStringUtil.h"

#include "nucleus/Nucleus.h"
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NULvalue.h"

#include "flow/FLFactory.h"
#include "flow/FlowClassifier.h"

#include "localflow/UD.h"

#include "reduce/ConvertLatches.h"

#include "bdd/BDD.h"

#include "ClockExclusivity.h"

REConvertLatches::REConvertLatches(FLNodeFactory* flowFactory,
                                   NUNetRefFactory* netRefFactory,
                                   AtomicCache* strCache,
                                   MsgContext* msgContext,
                                   IODBNucleus* iodb,
                                   UD* ud,
                                   ArgProc* args,
                                   bool convert,
                                   bool verbose) :
  mFlowFactory(flowFactory), mNetRefFactory(netRefFactory), mStrCache(strCache),
  mMsgContext(msgContext), mIODB(iodb), mUD(ud), mConvert(convert),
  mVerbose(verbose), mConvertCount(0)
{
  mClockExclusivity = new REClockExclusivity(mFlowFactory, mNetRefFactory,
                                             mStrCache, mMsgContext, mIODB, args);
}

REConvertLatches::~REConvertLatches()
{
  delete mClockExclusivity;
}


void REConvertLatches::module(NUModule* mod)
{
  // Initialize the exclusivity data
  mClockExclusivity->init(mod, NULL);

  // Process all the assigns in the module looking for possible gated
  // clocks. We can't start from flops because the gating logic may be
  // in its own module.
  for (NUModule::ContAssignLoop l = mod->loopContAssigns(); !l.atEnd(); ++l) {
    NUContAssign* assign = *l;
    processAssign(assign);
  }

#ifdef DEBUG_CONV
  if (mConvertCount > 0) {
    // Create the file we want to write to
    UtString cwd;
    UtString fileName;
    OSGetCurrentDir(&cwd);
    char* testDir = strstr(cwd.c_str(), "/test/");
    if (testDir != NULL) {
      char* testName = testDir + 6;
      for (char* p = testName; *p != '\0'; ++p) {
        if (*p == '/') {
          *p = '_';
        }
      }
      fileName << testName << "_";
    }

    // Make the filename unique by combining hostname and pid.  All
    // other attempts may fall short.
    UtString pathName, pid, host;
    OSGetHostname(&host);
    OSGetPidStr(&pid);
    fileName << host << "_" << pid;
    OSConstructFilePath(&pathName, "$HOME/convlatch/", fileName.c_str());


    // Open it for appending
    FILE* file = OSFOpen(pathName.c_str(), "w+", NULL);
    UtOFileStream stream(file);
    stream << mod->getName()->str() << " converted: " << mConvertCount << "\n";
    fclose(file);
  } // if
#endif
}

class ReplaceNet : public NuToNuFn
{
public:
  //! constructor
  ReplaceNet(NUNet* net, NUNet* newNet) : mNet(net), mNewNet(newNet) {}

  //! Overload the net to net function to translate the def
  NUNet* operator()(NUNet* net, Phase phase)
  {
    if (isPost(phase) && (net == mNet)) {
      return mNewNet;
    } else {
      return NULL;
    }
  }

private:
  NUNet* mNet;
  NUNet* mNewNet;
} // class ReplaceNet : public NuToNuFn
;

void REConvertLatches::processAssign(NUContAssign* assign)
{
  // Make sure the assignment is a valid assign fed by a latch
  ClkLatchUses clkLatchUses;
  NUNetRefHdl assignNetRef;
  bool isOR;
  if (!isValidAssign(assign, &assignNetRef, &clkLatchUses, &isOR)) {
    return;
  }

  // Look for a candidate
  FLNode* latchFlow = NULL;
  findCandidateLatch(assignNetRef, clkLatchUses, isOR, &latchFlow);

  if (latchFlow != NULL) {
    // Print the results if we found a candidate
    if (mVerbose) {
      const char* action = mConvert ? "has been" : "could be";
      const SourceLocator& loc = latchFlow->getUseDefNode()->getLoc();
      NUNet* net = latchFlow->getDefNet();
      mMsgContext->ClkGateLatch(&loc, net->getName()->str(), action);
    }

    // Convert the latch and update the assign
    if (mConvert) {
      NUNet* newNet = convertLatch(latchFlow);
      ReplaceNet translator(latchFlow->getDefNet(), newNet);
      assign->replaceLeaves(translator);
      ++mConvertCount;
    }
  }
}

bool
REConvertLatches::isValidAssign(NUContAssign* contAssign,
                                NUNetRefHdl* assignNetRef,
                                ClkLatchUses* clkLatchUses,
                                bool* isOR)
{
  // Make sure the lhs is a scalar net
  NUNetRefSet defs(mNetRefFactory);
  contAssign->getDefs(&defs);
  if (defs.size() != 1) {
    return false;
  }
  const NUNetRefHdl& netRef = *(defs.begin());
  if (netRef->getNumBits() != 1) {
    return false;
  }
  *assignNetRef = netRef;

  // Make sure the rhs is a binary AND/OR (logical AND/OR is supported
  // because we are dealing with scalars). Also the caller cares if
  // this is an AND or OR for its exclusivity analysis.
  //
  // Note that this test for AND/OR would not work if we allowed
  // expressions like ~(clk & en) or ~(clk | en) through.
  NUExpr* expr = contAssign->getRvalue();
  if (expr->getType() != NUExpr::eNUBinaryOp) {
    return false;
  }
  NUBinaryOp* binaryOp = dynamic_cast<NUBinaryOp*>(expr);
  NU_ASSERT(binaryOp != NULL, expr);
  NUOp::OpT opType = binaryOp->getOp();
  if ((opType == NUOp::eBiBitAnd) || (opType == NUOp::eBiLogAnd)) {
    *isOR = false;
  } else if ((opType == NUOp::eBiBitOr) || (opType == NUOp::eBiLogOr)) {
    *isOR = true;
  } else {
    // Not a match, give up
    return false;
  }

  // Make sure the two legs of the binary and contain only scalar nets
  NUNetRefHdl netRef1;
  NUExpr* expr1 = binaryOp->getArg(0);
  if (!singleScalarUse(expr1, &netRef1)) {
    return false;
  }
  NUNetRefHdl netRef2;
  NUExpr* expr2 = binaryOp->getArg(1);
  if (!singleScalarUse(expr2, &netRef2)) {
    return false;
  }

  // Make sure at least one leg is a latch that isn't observable outside
  bool latchFound = false;
  if (isOptimizableLatch(netRef1)) {
    clkLatchUses->push_back(ClkLatchUsesPair(expr2, netRef1));
    latchFound = true;
  }
  if (isOptimizableLatch(netRef2)) {
    clkLatchUses->push_back(ClkLatchUsesPair(expr1, netRef2));
    latchFound = true;
  }
  return latchFound;
} // REConvertLatches::isValidAssign

bool REConvertLatches::singleScalarUse(NUExpr* expr, NUNetRefHdl* netRef)
{
  // Make sure there is one use
  NUNetRefSet uses(mNetRefFactory);
  expr->getUses(&uses);
  if (uses.size() != 1) {
    return false;
  }

  // Make sure the use is a scalar
  *netRef = *(uses.begin());
  if ((*netRef)->getNumBits() != 1) {
    return false;
  }

  return true;
}

bool REConvertLatches::isOptimizableLatch(const NUNetRefHdl& netRef) const
{
  NUNet* net = netRef->getNet();
  return (net->isLatch() && !net->isOutput() && !net->isBid() &&
          !net->isProtected(mIODB));
}

void
REConvertLatches::findCandidateLatch(const NUNetRefHdl& assignNetRef,
                                     const ClkLatchUses& clkLatchUses,
                                     bool isOR,
                                     FLNode** latchFlow)
{
  // Pick the first candidate that matches. This appears to be ok
  // because both candidates can't match. This is because one of the
  // legs of the AND has to be the function of enable for the latch
  // and other leg has to be the output of the latch. Those conditions
  // cannot both be true if both legs of the AND are valid candidates.
  for (ClkLatchUsesLoop l(clkLatchUses); !l.atEnd() && (*latchFlow == NULL); ++l) {
    // Get the latch use and find its driver(s)
    int flowCount = 0;
    FLNode* fanin = NULL;
    ClkLatchUsesPair pair = *l;
    const NUNetRefHdl& netRef = pair.second;
    for (NUNetRef::DriverLoop f = netRef->loopInclusiveDrivers();
         !f.atEnd(); ++f) {
      // Ignore inital blocks, they don't affect the conversion
      FLNode* thisFanin = *f;
      if (!isInitialBlock(thisFanin)) {
        fanin = thisFanin;
        ++flowCount;
      }
    }

    // Make sure we have at most one driver and there is only one def
    // and that the latch enable is something that can be analyzed.
    bool invert;
    if ((flowCount == 1) && singleDef(fanin, netRef) &&
        mClockExclusivity->isValidDevice(fanin) &&
        (mClockExclusivity->getLatchEnable(fanin, NULL, &invert) != NULL)) {
      // Get the latch enable from the if statement
      NUNetRefHdl clkRef;
      const NUExpr* cond;
      cond = mClockExclusivity->getDeviceEnable(fanin, &clkRef);

      // Get the clock expression for the assign. If it has an OR,
      // invert it so that we are testing that it is exclusive when
      // the clk is 0.
      const NUExpr* clkExpr = pair.first;
      if (isOR) {
        clkExpr = mClockExclusivity->invertClockExpr(clkExpr);
      }

      // If we got a condition, check if the clk expression and latch
      // condition are exclusive
      if ((cond != NULL) && mClockExclusivity->exclusiveExprs(clkExpr, cond)) {
        // Yup, we have what we need to convert the latch
        *latchFlow = fanin;
      } else {
        // Try using the latch enable clock tree
        const NUExpr* cond2;
        cond2 = mClockExclusivity->getDeviceEnableExpr(fanin, NULL, NULL);
        if (cond2 != NULL) {
          if (mClockExclusivity->exclusiveExprs(clkExpr, cond2)) {
            // We can transform it, but use the original condition since it is
            // simpler and re-uses existing code.
            *latchFlow = fanin;
          } else if (assignNetRef->all()) {
            // Try creating a fuller expression for the
            // assignment. Again, if the expression has an OR in it then
            // we reverse it because we can transform when the clk == 0.
            const NUExpr* fullClkExpr;
            fullClkExpr = mClockExclusivity->getClockNetExpr(assignNetRef);
            if (fullClkExpr != NULL) {
              if (isOR) {
                fullClkExpr = mClockExclusivity->invertClockExpr(fullClkExpr);
              }

              // Test for exclusivity
              if (mClockExclusivity->exclusiveExprs(fullClkExpr, cond2)) {
                *latchFlow = fanin;
              }
            }
          }
        }
      }
    } // if
  } // for
} // REConvertLatches::findCandidateLatch

bool REConvertLatches::isInitialBlock(FLNode* flow) const
{
  NUUseDefNode* useDef = flow->getUseDefNode();
  return (useDef != NULL) && useDef->isInitial();
}

bool
REConvertLatches::singleDef(FLNode* flow, const NUNetRefHdl& netRef)
{
  // It must be an always block
  NUUseDefNode* useDef = flow->getUseDefNode();
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(useDef);
  if (always == NULL) {
    return false;
  }

  // Make sure the always block only defs the bits we care about
  NUNetRefSet defs(mNetRefFactory);
  always->getDefs(&defs);
  if (defs.size() != 1) {
    return false;
  }
  const NUNetRefHdl& defNetRef = *(defs.begin());
  if (netRef != defNetRef) {
    return false;
  }

  return true;
} // REConvertLatches::singleDef

//! replaceLeaves translator to fixup the latch that was converted
class LatchFixup : public ReplaceNet
{
public:
  //! constructor
  LatchFixup(const NUExpr* clk, NUExpr* constExpr, NUNet* def, NUNet* newDef) :
    ReplaceNet(def, newDef), mClk(clk), mConstExpr(constExpr)
  {}
  ~LatchFixup () { delete mConstExpr; }

  //! Overload the expr to expr call back to do the work
  NUExpr* operator()(NUExpr* expr, Phase phase)
  {
    if ((phase == ePost) && (*expr == *mClk)) {
      delete expr;
      CopyContext cc (0,0);
      return mConstExpr->copy (cc);
    } else {
      return NULL;
    }
  }

private:
  const NUExpr* mClk;
  NUExpr* mConstExpr;
}; // class LatchClockConst : public NuToNuFn

NUNet* REConvertLatches::convertLatch(FLNode* flow)
{
  // Get the module for all this work and a location to put it in
  NUAlwaysBlock* always = dynamic_cast<NUAlwaysBlock*>(flow->getUseDefNode());
  FLN_ASSERT(always != NULL, flow);
  const SourceLocator& loc = always->getLoc();
  NUModule* module = always->findParentModule();

  // Figure out the clock edge. Note that we create an edge that
  // indicates when the latch closes. This is because we want to
  // capture the value that the latch would have captured if the data
  // input was changing while the latch was open.
  //
  // We don't care that the flop doesn't wiggle when the latch was
  // open because the and gate says that nothing is looking at its
  // value.
  bool invert;
  NUNetRefHdl clkRef;
  const NUExpr* enable = mClockExclusivity->getLatchEnable(flow, &clkRef, &invert);
  ClockEdge edge = invert ? eClockPosedge : eClockNegedge;
  FLN_ASSERT(enable != NULL, flow);

  // Create a clock, it must be a whole identifier
  CopyContext cntxt(0,0);
  NUExpr* clk;
  if (enable->isWholeIdentifier()) {
    clk = enable->copy(cntxt);
    NUNet* clkNet = clk->getWholeIdentifier();
    clkNet->putIsEdgeTrigger(true);
  } else {
    // Not a whole identifier, we must make it one, create the rhs
    NUExpr* expr = enable->copy(cntxt);

    // Create a temp net from the expression so that we can map it
    // back to the original HDL.
    UtString buf;
    enable->compose(&buf, NULL, true);
    StringUtil::makeIdent(&buf);
    StringAtom* name = module->gensym(buf.c_str());
    NUNet* clkNet = module->createTempBitNet(name, false, loc);
    clkNet->putIsEdgeTrigger(true);
    
    // Create the assign and add it to the module
    NULvalue* lvalue = new NUIdentLvalue(clkNet, loc);
    StringAtom* blockName = module->newBlockName(mStrCache, loc);
    NUContAssign* assign = new NUContAssign(blockName, lvalue, expr, loc);
    module->addContAssign(assign);

    // Create the clock expression
    clk = new NUIdentRvalue(clkNet, loc);
  } // } else

  // Make a copy of the always block to create the flop
  CopyContext alwaysCntxt(module, mStrCache, true, "lat$");
  NUAlwaysBlock* newAlways = always->clone(alwaysCntxt, true);

  // Convert the always block to a flop
  NUEdgeExpr* edgeExpr = new NUEdgeExpr(edge, clk, always->getLoc());
  newAlways->addEdgeExpr(edgeExpr);

  // Create a new temp net for the latch output
  NUNet* def = flow->getDefNet();
  NUNet* newDef = module->createTempNetFromImage(def, "lat$");

  // Replace the if condition with a constant based on the invert flag
  // and the def net with the new def.
  NUExpr* constExpr = NUConst::create (false, not invert, 1, loc);
  LatchFixup translator(enable, constExpr, def, newDef);
  newAlways->getBlock()->replaceLeaves(translator);

  // Add the always block to the module and compute UD on it
  module->addAlwaysBlock(newAlways);
  mUD->structuredProc(newAlways);

  // Return the new net so the assign can be modified
  return newDef;
} // void REConvertLatches::convertLatch

