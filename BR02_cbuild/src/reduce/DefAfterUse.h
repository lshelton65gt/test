// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __DEF_AFTER_USE_H_
#define __DEF_AFTER_USE_H_

#include "nucleus/Nucleus.h"

//! class to help determine whether nets are redefined after being used
class DefAfterUse {
public:
  DefAfterUse();
  ~DefAfterUse();

  //! Process a module
  void module(NUModule*);

  //! Clear all the net information
  void clear();

  //! Is a net redefined after being used?
  bool redefinedAfterUse(NUNet*);

  //! Is a net defined after being used?
  bool definedAfterUse(NUNet*);

private:
  //! Process an always block
  void alwaysBlock(NUAlwaysBlock*);

  //! Process a statement list
  void stmtList(NUStmtLoop loop, NUNetSet * previousDefs, NUNetSet * previousUses);

  //! Dispatch method for various types of statements.
  void stmt(NUStmt * stmt, NUNetSet * previousDefs, NUNetSet * previousUses);

  //! Process a case statement
  void caseStmt(NUCase * stmt, NUNetSet * previousDefs, NUNetSet * previousUses);

  //! Process an if statement
  void ifStmt(NUIf * stmt, NUNetSet * previousDefs, NUNetSet * previousUses);

  //! Process a for statement
  void forStmt(NUFor * stmt, NUNetSet * previousDefs, NUNetSet * previousUses);

  //! Process a block
  void block(NUBlock * stmt, NUNetSet * previousDefs, NUNetSet * previousUses);

  NUNetSet * mRedefAfterUse;
  NUNetSet * mDefAfterUse;
};  

#endif
