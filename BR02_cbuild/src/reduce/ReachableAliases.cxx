// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "reduce/ReachableAliases.h"
#include "nucleus/NUNet.h"
#include "symtab/STSymbolTable.h"


ReachableAliases::ReachableAliases()
{
}


ReachableAliases::~ReachableAliases()
{
}


void ReachableAliases::compute(STSymbolTable *symtab)
{
  for (STSymbolTable::NodeLoop iter = symtab->getNodeLoop();
       !iter.atEnd();
       ++iter) {
    STAliasedLeafNode* node = (*iter)->castLeaf();

    if ((node != NULL) && (node->getMaster() == node)) {
      STAliasedLeafNode* storage = node->getStorage();
      ST_ASSERT(storage,node);
      mReachables.insert(storage);
    }
  }
}


bool ReachableAliases::query(STAliasedLeafNode *node) const
{
  STALNSet::const_iterator iter = mReachables.find(node);
  return (iter != mReachables.end());
}


bool ReachableAliases::queryAliases(NUNetElab *node,
                                    NUNet::QueryFunction fn) const
{
  return queryAliases(node->getSymNode(), fn);
}


bool ReachableAliases::queryAliases(STAliasedLeafNode *node,
                                    NUNet::QueryFunction fn) const
{
  bool found = false;
  for (STAliasedLeafNode::AliasLoop loop(node);
       not loop.atEnd() and not found;
       ++loop) {
    STAliasedLeafNode *cur_node = *loop;
    if (query(cur_node)) {
      NUNet *net = NUNet::find(cur_node);
      ST_ASSERT(net,cur_node);
      found = (net->*fn)();
    }
  }

  return found;
}


void ReachableAliases::rememberDepositable(NUNetElab *net_elab)
{
  mDepositables.insert(net_elab);
}


bool ReachableAliases::queryDepositable(NUNetElab *net_elab) const
{
  return (mDepositables.find(net_elab) != mDepositables.end());
}


bool ReachableAliases::requiresTriInit(NUNetElab *net_elab) const
{
  if (not isAttachedToPull(net_elab))
    return false;
  if (not isReadOrWritten(net_elab))
    return false;
  if (isDepositOrInput(net_elab))
    return false;
  return true;
}


bool ReachableAliases::isAttachedToPull(NUNetElab *net_elab) const
{
  // We use if/elseif structure below rather than a big OR
  // for easier single-step debugging.
  bool ret = false;
  if (queryAliases(net_elab, &NUNet::isUndriven))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isTristate) and
           (queryAliases(net_elab, &NUNet::isPrimaryBid) or
            queryAliases(net_elab, &NUNet::isPrimaryOutput)))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isTriWritten))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isWiredNet))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isPulled))
    ret = true;
  return ret;
}


bool ReachableAliases::isReadOrWritten(NUNetElab *net_elab) const
{
  // We use if/elseif structure below rather than a big OR
  // for easier single-step debugging.
  bool ret = false;
  if (queryAliases(net_elab, &NUNet::isRead))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isWritten))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isTriWritten))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isConstZ))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isPrimaryZ))
    ret = true;
  else if (queryAliases(net_elab, &NUNet::isPulled))
    ret = true;
  return ret;
}


bool ReachableAliases::isDepositOrInput(NUNetElab *net_elab) const
{
  bool ret = false;
  if (not queryAliases(net_elab, &NUNet::isPulled) and
      queryDepositable(net_elab))
    ret = true;
  if (queryAliases(net_elab, &NUNet::isPrimaryInput))
    ret = true;
  return ret;
}
