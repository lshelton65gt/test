// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "reduce/Separation.h"
#include "reduce/RewriteUtil.h"
#include "reduce/RewriteCheck.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NULvalue.h"

#include "iodb/IODBNucleus.h"

//#define SEPARATION_RECONSTRUCTION
#ifdef SEPARATION_RECONSTRUCTION
// Enable this define to include reconstruction data associated with
// separated vectors. This is not enabled by default because cwtb does
// not yet make use of the information (bug 2592, among others).
#include "nucleus/NUAliasDB.h"
#endif

#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"

#if !pfICC && !defined(__COVERITY__)
// force instantiation
template class NUNetRefMultiMap<SInt32>;
#endif

template <>
SInt32 NUNetRefMultiMap<SInt32>::nullT() const {
  return 0;
}

template <>
void NUNetRefMultiMap<SInt32>::helperTPrint(SInt32 const & /*val*/, int /*indent*/) const {
  // nop
}

Separation::Separation(UInt32            threshold,
                       AtomicCache     * str_cache,
                       NUNetRefFactory * netref_factory,
                       MsgContext      * msg_ctx,
                       IODBNucleus     * iodb,
                       Fold            * fold,
                       UInt32            separation_mode,
                       bool              verbose,
                       SeparationStatistics * stats) :
  mStrCache(str_cache),
  mNetRefFactory(netref_factory),
  mMsgContext(msg_ctx),
  mIODB(iodb),
  mFold(fold),
  mStatistics(stats),
  mSeparationMode(separation_mode),
  mVerbose(verbose),
  mSizeThreshold(threshold)
{
  mUnionFind = new NUNetRefUnionFind;
  mCache     = new NUNetRefCntMap(netref_factory);
}


Separation::~Separation()
{
  delete mUnionFind;
  delete mCache;
}


void Separation::module(NUModule * this_module)
{
  // Characterize how nets are referenced, combining overlapping references.
  if (mSeparationMode & eSeparateVectors)
  {
    SeparationUseCallback callback(mNetRefFactory, mIODB, 
                                   this_module, mUnionFind);
    NUDesignWalker walker(callback,false);
    callback.setWalker(&walker);
    NUDesignCallback::Status status;
    status = walker.module(this_module);
    NU_ASSERT(status == NUDesignCallback::eNormal, this_module);
  }

  // Convert from union-find to net-ref map.
  populateCache();

  // Handle the blastNet directive by replacing/adding bit-blasted netrefs
  NUModule* blastModule = NULL;
  if ((mSeparationMode & eBlastNets) && blast(this_module))
    blastModule = this_module;

  // Qualify all nets; remember the valid ones.
  NUNetSet nets;
  qualify(blastModule, nets);

  if (not nets.empty()) {
    // Add net_refs to fill the holes.
    complete(nets);

    // For each net, create temporaries unless that net is protected.
    // Replace old new with references to generated temporaries.
    // Create continuous assigns for each replaced net.
    separate(this_module, nets);
  
    // Track modification.
    mStatistics->module();	// increment module count.
  }
}


void Separation::populateCache()
{
  NUNetRefUnionFind::EquivalencePartition * partition = mUnionFind->getPartition();
  for (NUNetRefUnionFind::EquivalencePartition::iterator iter = partition->begin();
       iter != partition->end();
       ++iter) {
    // all the bits in our equivalence class.
    NUNetRefUnionFind::EquivalenceSet & equiv_class = iter->second;
    // converting to a NUNetRefSet will combine all the different
    // pieces of each net.
    NUNetRefSet net_refs(mNetRefFactory);
    for (UtSet<NUNetRefHdl>::iterator n = equiv_class.begin();
         n != equiv_class.end();
         ++n) {
      net_refs.insert(*n);
    }

    // add each independent chunk to the cache .
    addToCache(net_refs);
  }
  delete partition;
}


void Separation::addToCache(NUNetRefSet & net_refs)
{
  for (NUNetRefSet::iterator iter = net_refs.begin();
       iter != net_refs.end();
       ++iter) {
    NUNetRefHdl net_ref = (*iter);

    // Consistency check: If the UnionFind worked, nothing should overlap.
    NUNetRefCntMap::CondLoop loop = mCache->loop(net_ref,&NUNetRef::overlapsSameNet);
    NU_ASSERT(loop.atEnd(), net_ref);

    for (NUNetRefRangeLoop loop = net_ref->loopRanges(mNetRefFactory);
         not loop.atEnd(); 
         ++loop) {
      mCache->insert(loop.getCurrentNetRef(), 0);
    }
  }
}


bool Separation::blast(NUModule* this_module)
{
  bool anyBlasted = false;

  // loop over all nets for this module, testing for blasted nets
  NUNetList allNets;
  this_module->getAllTopNets(&allNets);
  this_module->getAllNestedNets(&allNets);
  for (NUNetList::iterator l = allNets.begin(); l != allNets.end(); ++l)
  {
    NUNet* net = *l;
    if (mIODB->isBlastedNet(net))
    {
      // this is a blasted net
      if (net->isVectorNet())
      {
        anyBlasted = true;

        // if the net is in the cache, remove all refs for it
        mCache->erase(net);

        // add a single ref for each bit of the net to the cache
        // Note: NUNetRefFactory::createVectorNetRef is based on declared range
        NUVectorNet* vn = dynamic_cast<NUVectorNet*>(net);
        const ConstantRange* r = vn->getDeclaredRange();
        for (SInt32 idx = r->rightmost(); idx <= r->leftmost(); ++idx)
        {
          NUNetRefHdl net_ref = mNetRefFactory->createVectorNetRef(net,idx);
          mCache->insert(net_ref, 0);
        }
      }
      else
      {
        // the blastNet directive is used on a non-vector
        mMsgContext->REBlastedNonVector(net);
      }
    }
  }

  return anyBlasted;
}

void Separation::qualify(NUModule* module, NUNetSet & nets) const
{
  NUNetSet covered;
  for (NUNetRefCntMap::MapLoop loop = mCache->loop();
       not loop.atEnd();
       ++loop) {
    const NUNetRefHdl net_ref = (*loop).first;

    // if this net ref is for the whole net, skip it
    if (net_ref->all())
      continue;

    // if this net has already been tested, don't re-qualify it
    // this is not for performance -- the qualify() test is fast
    // but we only want to qualify each net once, so that we don't
    // write warnings multiple times for the same net
    NUNet * net = net_ref->getNet();
    if (covered.find(net) != covered.end())
      continue;
    covered.insert(net);

    // this is the first sighting of this net, qualify it and possibly add it
    if (qualify(module, net))
      nets.insert(net);
  }
}


bool Separation::qualify(NUModule* module, const NUNet * net) const
{
  // only do lookup if we are given a module with blasted nets
  bool isBlasted = module && mIODB->isBlastedNet(net);

  if (net->isPort())
  {
    if (isBlasted)
      mMsgContext->REBlastedPort(net);
    return false;
  }

  // If this is a blast net and we are doing separation, don't blast
  // this net again. It was already done. Doing it again just blasts
  // the net left behind in case it was marked observable.
  if (((mSeparationMode & eBlastNets) == 0) && mIODB->isBlastedNet(net)) {
    return false;
  }

  // Check if it is depositable, then we can't berak it up.
  if (net->isProtectedMutable(mIODB)) {
    if (isBlasted) {
      mMsgContext->REBlastedProtected(net);
    }
    return false;
  }

  if (!net->isVectorNet())
  {
    return false;
  }

  if ((net->getBitSize() <= mSizeThreshold) && !isBlasted)
  {
    return false;
  }

  if (net->getBitSize() > UtSINT16_MAX) { 
    return false;
  }

  return true;
}


void Separation::complete(NUNetSet & nets)
{
  for (NUNetSet::iterator iter = nets.begin();
       iter != nets.end();
       ++iter) {
    NUNet * net = (*iter);
    
    NUNetRefHdl covered_net_ref = mNetRefFactory->createEmptyNetRef();
    NUNetRefHdl full_net_ref    = mNetRefFactory->createNetRef(net);

    for (NUNetRefCntMap::CondLoop loop = mCache->loop(full_net_ref,&NUNetRef::overlapsSameNet);
	 not loop.atEnd();
	 ++loop) {
      NUNetRefHdl net_ref = (*loop).first;

      // keep track of the parts which have replacements
      covered_net_ref = mNetRefFactory->merge(covered_net_ref,net_ref);
    }

    NUNetRefHdl missing_net_ref = mNetRefFactory->subtract(full_net_ref,
                                                           covered_net_ref);
    for (NUNetRefRangeLoop loop = missing_net_ref->loopRanges(mNetRefFactory);
         not loop.atEnd(); 
         ++loop) {
      mCache->insert(loop.getCurrentNetRef(), 0);
    }
  }
} // void Separation::complete


void Separation::separate(NUModule * this_module, NUNetSet & nets)
{
  NUExprReplacementMap expr_replacements;

#ifdef SEPARATION_RECONSTRUCTION
  STSymbolTable * alias_db = this_module->getAliasDB();
  NUAliasBOM * alias_bom = dynamic_cast<NUAliasBOM*>(alias_db->getFieldBOM());
  NU_ASSERT(alias_bom,this_module);
  ESFactory * expr_factory = alias_bom->getExprFactory();
#endif

  for (NUNetSet::SortedLoop iter = nets.loopSorted(); !iter.atEnd(); ++iter) {
    NUNet * net = (*iter);

    NUExprVector components;
#ifdef SEPARATION_RECONSTRUCTION
    CarbonExprVector symtab_components;
#endif

    mStatistics->net();		// increment net count.

    if (mVerbose)
    {
      UtString netName;
      net->composeUnelaboratedName(&netName);
      if ((mSeparationMode & eBlastNets) && mIODB->isBlastedNet(net))
        UtIO::cout() << "Bit-blasted vector: ";
      else
        UtIO::cout() << "Separated vector: ";
      UtIO::cout() << netName << UtIO::endl;
    }

    NUNetRefHdl covered_net_ref = mNetRefFactory->createEmptyNetRef();
    NUNetRefHdl full_net_ref = mNetRefFactory->createNetRef(net);

    NUScope *scope = net->getScope();
    
    for (NUNetRefCntMap::CondLoop loop = mCache->loop(full_net_ref,&NUNetRef::overlapsSameNet);
	 not loop.atEnd();
	 ++loop) {
      NUNetRefHdl net_ref = (*loop).first;

      mStatistics->part();      // increment part count.

      ConstantRange range(0,0);
      bool success = net_ref->getRange(range);
      NU_ASSERT(success, net_ref);

      UtString name_string;
      name_string << net->getName()->str() << "_"
                  << range.getMsb() << "_" << range.getLsb();
      StringAtom* name = scope->gensym("slice", name_string.c_str(), NULL,
                                       true); // already unique

      NetFlags net_flags(NetFlags(net->getFlags()|eTempNet));
      NUNet* replacement = scope->createTempVectorNet(name, range, net->isSigned(),
                                                      net->getLoc(), net_flags, 
                                                      net->getVectorNetFlags(),
                                                      net->getName());
      if (mVerbose)
	UtIO::cout() << "  Replacement: " << replacement->getName()->str() << UtIO::endl;

      // keep track of the parts which have replacements
      covered_net_ref = mNetRefFactory->merge(covered_net_ref,net_ref);

      NUIdentRvalue * ident = new NUIdentRvalue(replacement,
						replacement->getLoc());
      ident->resize(replacement->getBitSize());
      components.push_back(ident);

#ifdef SEPARATION_RECONSTRUCTION 
      CarbonIdent * symtab_ident = alias_bom->createCarbonIdentBP(replacement, &net_ref);
      symtab_components.push_back(symtab_ident);
#endif
    }

    // Create the expression replacement.
    std::reverse(components.begin(),
		 components.end());
    NUConcatOp * concat = new NUConcatOp(components, 1,
					 net->getLoc());
    concat->resize(net->getBitSize());
    expr_replacements[net] = concat;

#ifdef SEPARATION_RECONSTRUCTION
    // Create the symtab mapping for the original net.
    std::reverse(symtab_components.begin(),
                 symtab_components.end());
    CarbonExpr * symtab_concat = expr_factory->createConcatOp(&symtab_components,
                                                              1, net->getBitSize(),
                                                              false);
 
    CarbonIdent * symtab_ident = alias_bom->createCarbonIdent(net);
    NU_ASSERT(alias_bom->getExpr(symtab_ident)==NULL,net);
    alias_bom->mapExpr(symtab_ident,
                       symtab_concat);
#endif

    NU_ASSERT (covered_net_ref->all(), covered_net_ref);
  }


  {
    NUNetReplacementMap     dummy_net_replacements;
    NUTFReplacementMap      dummy_tf_replacements;
    NUCModelReplacementMap      dummy_cmodel_replacements;
    NUAlwaysBlockReplacementMap      dummy_always_replacements;
    NULvalueReplacementMap  dummy_lvalue_replacements;
    RewriteLeaves translator(dummy_net_replacements,
                             dummy_lvalue_replacements,
                             expr_replacements,
                             dummy_tf_replacements,
                             dummy_cmodel_replacements,
                             dummy_always_replacements,
                             mFold);
    
    this_module->replaceLeaves(translator);
  }

  // consistency check before we add continuous assigns -- we should
  // no longer refer to any of the candidate nets.
  RewriteCheck consistency(nets);
  this_module->replaceLeaves(consistency);

  for (NUNetSet::iterator iter = nets.begin();
       iter != nets.end();
       ++iter) {
    NUNet * net     = (*iter);
    NUExpr * rvalue = expr_replacements[net];

    bool create_assign = (mSeparationMode & eCreateContAssigns);
    if (net->isNonStatic()) {
      // do not create continuous assigns for non-statics.
      create_assign = false;
    }
    if (create_assign) {
      const SourceLocator & loc = net->getLoc();
      NUIdentLvalue * lvalue = new NUIdentLvalue(net, loc);
      StringAtom * name = this_module->newBlockName(mStrCache, loc);
      NUContAssign * assign = new NUContAssign (name,
                                                lvalue,
                                                rvalue,
                                                loc);
      this_module->addContAssign(assign);
    } else {
      delete rvalue;
    }
  }
}


SeparationUseCallback::SeparationUseCallback(NUNetRefFactory * netref_factory,
					     IODBNucleus * iodb,
                                             NUModule * module,
                                             NUNetRefUnionFind * union_find) :
  mNetRefFactory(netref_factory),
  mIODB(iodb),
  mModule(module),
  mUnionFind(union_find)
{}


SeparationUseCallback::~SeparationUseCallback()
{
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase /*phase*/, NUModule * module)
{
  return (module==mModule) ? eNormal : eSkip;
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUPortConnectionOutput * output)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }
  NULvalue * actual = output->getActual();
  if (actual)
    mWalker->lvalue(actual);
  return eNormal;
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUPortConnectionInput * input)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }
  NUExpr * actual = input->getActual();
  if (actual)
    (void) operator()(phase,actual);
  return eNormal;
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUPortConnectionBid * bid)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }
  NULvalue * actual = bid->getActual();
  if (actual)
    mWalker->lvalue(actual);
  return eNormal;
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUEnabledDriver * driver)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }
  mWalker->lvalue(driver->getLvalue());
  mWalker->expr(driver->getEnable());
  mWalker->expr(driver->getDriver());
  return eNormal;
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUTriRegInit * tri_init)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }
  mWalker->lvalue(tri_init->getLvalue());
  return eNormal;
}


NUNetRefHdl SeparationUseCallback::prepareLhs(NULvalue * lhs)
{
  NUNetRefSet defs(mNetRefFactory);
  lhs->getDefs(&defs);
  NU_ASSERT(not defs.empty(), lhs);
  NUNetRefSet::iterator defs_iter = defs.begin();
  NUNetRefHdl def_net_ref = (*defs_iter);
  ++defs_iter; 
  NU_ASSERT(defs_iter==defs.end(), lhs);
  NU_ASSERT(not def_net_ref->empty(), lhs);
  return def_net_ref;
}


NUNetRefHdl SeparationUseCallback::prepareRhs(NUExpr * rhs)
{
  NUNetRefSet uses(mNetRefFactory);
  rhs->getUses(&uses);
  NU_ASSERT(not uses.empty(), rhs);
  NUNetRefSet::iterator uses_iter = uses.begin();
  NUNetRefHdl use_net_ref = (*uses_iter);
  ++uses_iter;
  NU_ASSERT(uses_iter==uses.end(), rhs);
  NU_ASSERT(not use_net_ref->empty(), rhs);
  return use_net_ref;
}


void SeparationUseCallback::equateBits(const ConstantRange range, const NUNetRefHdl & a, const NUNetRefHdl & b)
{
  for (UInt32 i=0; i < range.getLength(); ++i) {
    NUNetRefHdl a_bit_ref = mNetRefFactory->sliceNetRef(a, range.getMsb()-i);
    NUNetRefHdl b_bit_ref = mNetRefFactory->sliceNetRef(b, range.getMsb()-i);
    NU_ASSERT(not a_bit_ref->empty(), a);
    NU_ASSERT(not b_bit_ref->empty(), b);
    mUnionFind->equate(a_bit_ref, b_bit_ref);
  }
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUAssign *assign)
{
  if (phase != NUDesignCallback::ePre) {
    return eNormal;
  }

  NUExpr *rhs = assign->getRvalue();
  NULvalue *lhs = assign->getLvalue();

  // Do not walk assigns to whole nets which have constant RHS, there is no reason to disqualify them.
  if ((rhs->getType() == NUExpr::eNUConstNoXZ) || (rhs->getType() == NUExpr::eNUConstXZ)) {
    if (lhs->isWholeIdentifier()) {
      return eSkip; // identifier = constant; don't allow the constant assign to impact separation.
    } else if (lhs->getType()==eNUVarselLvalue) {
      NUVarselLvalue * varsel = dynamic_cast<NUVarselLvalue*>(lhs);
      if (varsel->getLvalue()->isWholeIdentifier() and varsel->isConstIndex()) {
        return eSkip; // vector[constant] = constant; don't allow the constant assign to impact separation.
      } else {
        return eNormal; // vector[dynamic] = constant; allow normal lhs processing.
      }
    } else {
      return eNormal; // complex identifier (concat?); allow normal lhs processing.
    }
  } else if (lhs->isWholeIdentifier()) {

    NUNetRefHdl def_net_ref = prepareLhs(lhs);
    NUNet * def_net = def_net_ref->getNet();

    if (not qualify(def_net)) { return eNormal; }

    ConstantRange def_range;
    def_net_ref->getRange(def_range);

    if (rhs->getType() == NUExpr::eNUTernaryOp) {
      // Handle X = sel ? Y : Z by bit-equating X, Y, and Z.

      // TBD: Handle easily bit-equated expression forms. Assuming
      // everything is a full identifier, we may want to bit-equate
      // some of the following forms:
      //
      // Arbitrary nesting of ternary operators (selects ignored and
      // walked in isolation):
      // X = sel ? (sel2 ? V1 : V2) : (sel3 ? V3 : V4);
      //
      // Bitwise operators:
      // X = V1 & V2;
      //
      // Concats:
      // X = {V1, V2} & V3;
      //
      // The bitwise determination is similar to the analysis
      // performed by BitNetRefCallback in BreakNetCycles. This
      // callback would need to process selects differently; the
      // select uses cannot be merged across all bits for this
      // Separation application.

      NUTernaryOp * ternary = dynamic_cast<NUTernaryOp*>(rhs);

      NUExpr * select = ternary->getArg(0);
      NUExpr * arg1   = ternary->getArg(1);
      NUExpr * arg2   = ternary->getArg(2);

      if (arg1->isWholeIdentifier() and arg2->isWholeIdentifier()) {
        // handle X = en ? Y : Z;
        NUNetRefHdl arg1_net_ref = prepareRhs(arg1);
        NUNetRefHdl arg2_net_ref = prepareRhs(arg2);
        NUNet * arg1_net = arg1_net_ref->getNet();
        NUNet * arg2_net = arg2_net_ref->getNet();

        if (not qualify(arg1_net)) { return eNormal; }
        if (not qualify(arg2_net)) { return eNormal; }

        ConstantRange arg1_range;
        ConstantRange arg2_range;
        arg1_net_ref->getRange(arg1_range);
        arg2_net_ref->getRange(arg2_range);
        if ((def_range != arg1_range) or (def_range != arg2_range)) {
          return eNormal;
        }

        // manually walk the select.
        mWalker->expr(select);

        // bit-connect the args and the lhs.
        if (def_net != arg1_net) {
          equateBits(def_range, def_net_ref, arg1_net_ref);
        }
        if (def_net != arg2_net) {
          equateBits(def_range, def_net_ref, arg2_net_ref);
        }

        return eSkip;
      } else if ((arg1->isWholeIdentifier() and arg2->castConst()) or
                 (arg1->castConst() and arg2->isWholeIdentifier())) {
        // handle X = en ? Y : const; and X = en ? const : Z;
        if (arg2->isWholeIdentifier()) {
          std::swap(arg1,arg2);
        }

        NUNetRefHdl arg1_net_ref = prepareRhs(arg1);
        NUNet * arg1_net = arg1_net_ref->getNet();

        if (not qualify(arg1_net)) { return eNormal; }

        ConstantRange arg1_range;
        arg1_net_ref->getRange(arg1_range);
        if (def_range != arg1_range) {
          return eNormal;
        }

        // manually walk the select.
        mWalker->expr(select);

        // bit-connect the non-constant arg and the lhs.
        if (def_net != arg1_net) {
          equateBits(def_range, def_net_ref, arg1_net_ref);
        }
        return eSkip;
      }

    } else if (rhs->isWholeIdentifier()) {
      // Handle X = Y by bit-equating X, Y, and Z.

      NUNetRefHdl use_net_ref = prepareRhs(rhs);
      NUNet * use_net = use_net_ref->getNet();

      if (def_net==use_net) {
        return eSkip; // hold assign should not effect separation.
      }

      if (not qualify(use_net)) { return eNormal; }

      ConstantRange use_range;
      use_net_ref->getRange(use_range);
      if (def_range != use_range) {
        return eNormal;
      }

      equateBits(def_range, def_net_ref, use_net_ref);

      return eSkip;
    }
  }
  return eNormal;
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUContAssign *assign)
{
  return operator()(phase, (NUAssign*)assign);
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUOp * op)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }

  switch ( op->getOp() )
  {
  case NUOp::eBiEq:
  case NUOp::eBiNeq:
  case NUOp::eBiTrieq:
  case NUOp::eBiTrineq:
  {
    NUExpr * first  = op->getArg(0);
    NUExpr * second = op->getArg(1);

    if ( (first->getType() == NUExpr::eNUIdentRvalue and second->castConst()) xor
         (second->getType()== NUExpr::eNUIdentRvalue and first->castConst()) ) {
      return eSkip; // identifier==const. don't allow the constant comparison to affect separation.
    }
    // fall-thru
  }
  default:
    // If not skipped, fall back to default expression handler.
    return operator()(phase,(NUExpr*)op);
  }
}


NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUExpr * expr)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }

  bool walk_sub_exprs = true;

  switch ( expr->getType() )
  {
  case NUExpr::eNUVarselRvalue:
  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ:
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUIdentRvalueElab:
  case NUExpr::eNULut:
  case NUExpr::eNUNullExpr: {
    // don't walk anything under this point.
    walk_sub_exprs = false; 

    // expression types we evaluate here.
    NUNetRefSet uses(mNetRefFactory);
    expr->getUses(&uses);
    remember(uses);
    break;
  }

  case NUExpr::eNUAttribute:
  case NUExpr::eNUEdgeExpr:
  case NUExpr::eNUSysFunctionCall:
  case NUExpr::eNUMemselRvalue:
  case NUExpr::eNUUnaryOp:
  case NUExpr::eNUBinaryOp:
  case NUExpr::eNUTernaryOp:
  case NUExpr::eNUConcatOp:
  case NUExpr::eNUCompositeExpr:
  case NUExpr::eNUCompositeIdentRvalue:
  case NUExpr::eNUCompositeSelExpr:
  case NUExpr::eNUCompositeFieldExpr:
  {
    // expression types we pass through and evaluate later.
    break;
  }
    
    // do not put a default here, that way if someone derives a new
    // class  from NUExpr they will see that they need to handle it here.
  }
  return (walk_sub_exprs ? eNormal : eSkip);
}
  

NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NUVarselLvalue * lvalue)
{
  if (phase!=NUDesignCallback::ePre) {
    return eSkip;
  }

  NUNetRefSet defs(mNetRefFactory);
  lvalue->getDefs(&defs);
  remember(defs);

  // Manually visit the indexing expression
  mWalker->expr (lvalue->getIndex ());

  return eSkip;
}

NUDesignCallback::Status SeparationUseCallback::operator()(Phase phase, NULvalue * lvalue)
{
  if (phase!=NUDesignCallback::ePre) {
    return eNormal;
  }

  // lvalue types we evaluate here.
  NUIdentLvalue   * ident   = dynamic_cast<NUIdentLvalue*>(lvalue);

  if (ident) {
    NUNetRefSet defs(mNetRefFactory);
    lvalue->getDefs(&defs);
    remember(defs);
  } else {
    // lvalue types we pass through and evaluate later.
    NUConcatLvalue  * concat  = dynamic_cast<NUConcatLvalue*>(lvalue);
    NUMemselLvalue  * memsel  = dynamic_cast<NUMemselLvalue*>(lvalue);

    NU_ASSERT (concat or memsel, lvalue);

    // what is a NUPartselIndexLvalue?
    // NUPartselIndexLvalue * partsel_index = dynamic_cast<NUPartselIndexLvalue*>(lvalue);
  }

  return eNormal;
}

void SeparationUseCallback::remember(NUNetRefSet & net_refs)
{
  NUNetRefUnionFind::EquivalenceSet bit_set;
  for (NUNetRefSet::iterator iter = net_refs.begin();
       iter != net_refs.end();
       ++iter) {
    NUNetRefHdl net_ref = (*iter);
    if (net_ref->empty()) {
      continue;
    }
    NUNet * net = net_ref->getNet();
    if (not qualify(net)) {
      continue;
    }
    for (NUNetRefRangeLoop loop = net_ref->loopRanges(mNetRefFactory);
         not loop.atEnd(); 
         ++loop) {
      NUNetRefHdl partial_net_ref = loop.getCurrentNetRef();
      ConstantRange range;
      partial_net_ref->getRange(range);
      for (UInt32 i=0; i < range.getLength(); ++i) {
        NUNetRefHdl bit_net_ref = mNetRefFactory->sliceNetRef(partial_net_ref, range.getMsb()-i);
        bit_set.insert(bit_net_ref);
      }
    }
  }
  mUnionFind->equate(bit_set);
}


bool SeparationUseCallback::qualify(NUNet * net)
{
  return ((not net->isPort()) and
          (net->isVectorNet()) and
          (net->getBitSize() < UtSINT16_MAX) and
          (not net->isProtected(mIODB)));
}


void SeparationStatistics::print() const 
{
  UtIO::cout() << "Separation Summary: "
	       << mNets << " nets separated into "
	       << mParts << " parts in "
	       << mModules << " modules." << UtIO::endl;
}
