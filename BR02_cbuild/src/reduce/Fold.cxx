// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Simple constant folding pass.  Walk the design folding all expression
 * trees into simpler forms.
 */

#include "reduce/Fold.h"
#include "FoldI.h"

NUExpr* Fold::createPartsel(const NUExpr* ident, const ConstantRange& ps,
                            const SourceLocator& loc)
{
  NUExpr * r = NULL;
  {
    FoldNoFinish dummy(mFold);
    r = mFold->createPartsel (ident, ps, loc);
  }
  return fold(r);
}


// External entry point
NUExpr* Fold::createBitsel(const NUExpr* expr, const NUExpr* sel,
                           const SourceLocator& loc)
{
  NUExpr * r = NULL;
  {
    FoldNoFinish dummy(mFold);
    r = mFold->createBitsel(expr, sel, loc);
  }
  return fold(r);
}


//----------------------------------------------------------------------------------------------
// Public interface to the folding system

Fold::Fold(NUNetRefFactory *netref_factory,
           ArgProc* args,
           MsgContext *msg_ctx,
           AtomicCache *cache,
	   IODBNucleus *iodb,
           bool verbose,
           UInt32 flags
           )
{
  mFold = new FoldI(netref_factory,
                    args,
                    msg_ctx,
                    cache,
		    iodb,
                    verbose,
                    flags);
}

Fold::~Fold()
{
  delete mFold;
}

NUExpr* Fold::fold(NUExpr* expr, UInt32 flagsOn, UInt32 flagsOff)
{
  UInt32 savedFlags = mFold->getFlags ();
  mFold->putFlags ((savedFlags | flagsOn) & ~flagsOff);

  NUExpr* r = mFold->foldExpr(expr);
  mFold->cleanup ();
  mFold->putFlags (savedFlags);
  return r;
}

const NUExpr* Fold::foldFactoryExpr(const NUExpr* expr, UInt32 flagsOn,
                                    UInt32 flagsOff)
{
  UInt32 savedFlags = mFold->getFlags ();
  mFold->putFlags ((savedFlags | flagsOn) & ~flagsOff);

  const NUExpr* r = mFold->foldFactoryExpr(expr);
  mFold->cleanup ();
  mFold->putFlags (savedFlags);
  return r;
}

void Fold::shareExprFactory(NUExprFactory* ef) {
  mFold->shareExprCache(ef);
}

NULvalue* Fold::fold(NULvalue* lhs, UInt32 flagsOn, UInt32 flagsOff)
{
  UInt32 savedFlags = mFold->getFlags ();
  mFold->putFlags ((savedFlags | flagsOn) & ~flagsOff);
  NULvalue *r = mFold->fold(lhs);
  mFold->cleanup ();
  mFold->putFlags (savedFlags);
  return r;
}

void Fold::clearExprFactory()
{
  mFold->clearExprCache();
}
   

//  Constant fold the module
/*!
 * Iterate through all the different top-level drivers which exist
 * in a module.  For each driver, visit the statements and fold expression.
 *
 * Recurse to handle submodules if recurse is true.
 */
void Fold::module(NUModule *mod, bool recurse)
{
  mFold->module(mod, recurse);
  mFold->cleanup ();
}

//  Constant fold the block
void Fold::block(NUBlock *blk) {
  mFold->foldBlock(blk);
  mFold->cleanup();
}

//  Walk a design
void Fold::design(NUDesign *design, UInt32 flagsOn, UInt32 flagsOff)
{
  UInt32 savedFlags = mFold->getFlags ();
  mFold->putFlags ((savedFlags | flagsOn) & ~flagsOff);

  mFold->design(design);
  mFold->putFlags (savedFlags);
  mFold->cleanup ();
}

// Fold a structured proc
void Fold::structuredProc (NUStructuredProc* proc)
{
  mFold->structuredProc (proc);
  mFold->cleanup ();
}

// Fold a task
void Fold::tf (NUTF* tf)
{
  mFold->tf (tf);
  mFold->cleanup ();
}

//  Fold a statement
NUStmt* Fold::stmt(NUStmt* s, UInt32 flagsOn, UInt32 flagsOff)
{
  UInt32 savedFlags = mFold->getFlags ();
  mFold->putFlags ((savedFlags | flagsOn) & ~flagsOff);

  s = mFold->foldStmt(s);
  mFold->cleanup ();

  mFold->putFlags (savedFlags);
  return s;
}

//  Test
bool Fold::isChanged () const
{
  return mFold->isChanged();
}

//  Reset
void Fold::clearChanged()
{
  mFold->clearChanged();
}

//  Push the scope  (Visible to make it possible to fold less than a design...)
void Fold::pushScope(NUBlockScope* scope)
{
  mFold->push(scope);
}

//  pop a scope
void Fold::popScope()
{
  mFold->pop();
}

bool Fold::putVerbose(bool flag) {
  return mFold->putVerbose(flag);
}

bool Fold::putTraceFold(bool flag) {
  return mFold->putTraceFold(flag);
}

UInt32 Fold::putTraceBDDFold(UInt32 verboseFlags) {
  return mFold->putTraceBDDFold(verboseFlags);
}
