#!/usr/bin/perl

#*****************************************************************************
# Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
#*****************************************************************************

# This will parse a memory file list and generate wrappers for the Carbon memory core.
#
# To run this:
# perl parse_mem_table < memory_table_name  > new_memory_file_name
#
# Note: this is a temporary solution.  This will eventually be refitted into the remodeling wizard.
#

# We care about: name, dsize (address bit width), num words, mux(?), write mask, ports.  Others affect extra pins.

# Version 1: initial coding
# Version 2: Added byte enables

use POSIX;

#$numArgs = $#ARGV + 1;
#foreach $argnum (0 .. $ARGV) {
#    print "ARGV($argnum)\n";
#}



$RAMLISTCFG = qr/
^
(\S+)  # prefix
\s+
(\d+)  # data bits
\s+
(\d+)  # w_size
\s+
(rise|fall)   # clk edge
\s+
\w+\s+
\w+\s+
(\d)   # mux
\s+
\w+\s+  #wl_banks
\w+\s+  #bl_banks
\w+\s+  #core
\w+\s+  #comp_type
\w+\s+  #acc
\w+\s+  #banks
\w+\s+  #str
\w+\s+  #core_dev
\w+\s+  #core_fs
\w+\s+  #peri_dev
\w+\s+  #peri_fs
(\w+)   #writable
\s+
(\d+)  # ports [Total][Read][write][clocks]
\s+    
\w+\s+ #impl
\w+    #bist
/x;



$RAMDPLISTCFG = qr/
^
(\S+)  # prefix
\s+
(\d+)  # data bits
\s+
(\d+)  # w_size
\s+
(rise|fall)   # clk edge
\s+
\w+\s+
\w+\s+
(\d)   # mux
\s+
\w+\s+  #wl_banks
\w+\s+  #bl_banks
\w+\s+  #core
\w+\s+  #comp_type
\w+\s+  #str
\w+\s+  #core_dev
\w+\s+  #core_fs
\w+\s+  #peri_dev
\w+\s+  #peri_fs
(\w+)   #writable
\s+
(\d+)  # ports [Total][Read][write][clocks]
\s+    
\w+\s+ #impl
\w+    #bist
/x;

$ROMCFG = qr/
^
(\S+)  # prefix
\s+
(\d+)  # data bits
\s+
(\d+)  # w_size
\s+
(r|f)   # clk edge
\s+
\w+     #code
\s+
(\d+)   # mux
\s+
\w+\s+  #wl_banks
\w+\s+  #bl_banks
\w+\s+  #comp_type
\w+\s+  #str
\w+\s+  #core_dev
\w+\s+  #core_hs
\w+\s+  #peri_dev
\w+\s+  #peri_fs
\w+     #diff
\s+
\w+     #tech
/x;





foreach (<>) {
  if (/$RAMLISTCFG/ || /$RAMDPLISTCFG/ || /$ROMCFG/ ) {
    #print "Match happened\n";
    $prefix = $1;
    $width = $2;
    $depth = $3;
    $edge = $4;
    $mux = $5;
    #fake out writer
    if (/$ROMCFG/) {
      $writable = "wd";
      $ports = "2111";;
    }
    else {
      $writable = $6;
      $ports = $7;
    }

    #print "prefix = $prefix ";
    #print "width = $width ";
    #print "depth = $depth ";
    #print "edge = $edge ";
    #print "mux = $mux ";
    #print "writable = $writable  ";
    #print "ports = $ports \n";
    $addr_width = POSIX::ceil(log($depth)/log(2));
    $edgeValue = "1'b1";
    if ($edge eq "f" or $edge eq "fall") {
        $edgeValue = "1'b0";
    }

    $byte_to_bit_code = "";	# code to convert byte enable to bit enable, if needed
    $be_instance = "{DATA_WIDTH{1'b1}}"; # What gets passed into core

    if ($ports == 2111) {
      $read_addr_name = "addra";
      $write_addr_name = "addrb";
      $data_out_name = "douta";
      $data_in_name = "dinb";
      $cs_read_name = "csa_n";
      $cs_write_name = "csb_n";
      $portnames = << "END";
                         $read_addr_name,
                         $write_addr_name,
                         $data_out_name,
                         $data_in_name,
                         $cs_read_name,
                         $cs_write_name,
END
      $portdecls = << "END";
   input [ADDR_WIDTH-1:0] $read_addr_name ;
   output [DATA_WIDTH-1:0]$data_out_name ;
   
   input [ADDR_WIDTH-1:0]  $write_addr_name ;
   input [DATA_WIDTH-1:0]  $data_in_name ;

   input 		   $cs_read_name ;
   input 		   $cs_write_name ;
END
    }
    else {
      # Single port
      $read_addr_name = "addr";
      # print "read_addr_name = $read_addr_name";
      $write_addr_name = "addr";
      # print "write_addr_name = $write_addr_name";
      $data_in_name = "din";
      # print "data_in_name = $data_in_name";
      $data_out_name = "dout";
      # print "data_out_name = $data_out_name";
      $cs_read_name = "cs_n";
      # print "read_name = $cs_read_name";
      $cs_write_name = "we_n";
      # print "write_name = $cs_write_name";
      $write_addr_name = "addr";
      # print "write_addr_name = $write_addr_name";
      # Byte enable?
      if ($writable eq "by") {
	$data_enable_name = "wby_n";
	$be_instance = "temp_bit_enable";
      }
      # Bit enable
      elsif ($writable eq "bt") {
	$data_enable_name = "wbt_n";
	$be_instance = "~wby_n";
      }
      # Neither
      else {
	$data_enable_name = "";
      }
      $portnames = << "END";
                         $read_addr_name,
                         $data_out_name,
                         $data_in_name,
                         $cs_read_name,
                         $cs_write_name,
END
      $portdecls = << "END";
   input [ADDR_WIDTH-1:0] $read_addr_name ;
   output [DATA_WIDTH-1:0]$data_out_name ;
   
   input [DATA_WIDTH-1:0]  $data_in_name ;

   input 		   $cs_read_name ;
   input 		   $cs_write_name ;
END
      # Byte enable?
      if ($writable ne "wd") {
	$portnames .= << "END";
                         $data_enable_name,
END
	# Yes, width is number of bytes
	if ($writable eq "by") {
	  $portdecls .= << "END";
   input [BYTES-1:0]  $data_enable_name ;
END
	  $byte_to_bit_code = << "END";
   integer i;
   # print " number 4";
   reg [DATA_WIDTH-1:0]  temp_bit_enable ;

   always @($data_enable_name) begin
     for (i = 0; i < BYTES; i = i + 1) begin
       // Generate bit enable from byte enables
       temp_bit_enable = (temp_bit_enable << 8) | {8 {~${data_enable_name}[BYTES-i-1]}};
     end
   end
END
	}
	else {
	  # No, but enable, width is number of bits
	  $portdecls .= << "END";
   input [DATA_WIDTH-1:0]  $data_enable_name ;
END
	}
      }
    }				# end of single port
    print "// ${prefix}${depth}x${width} : $writable, $ports, $edge, $mux\n";
    print "module ${prefix}${depth}x${width} (\n";
    print <<"END";
			 sin,
			 sout,	
			 clk,
$portnames
			 shift_n,
			 slp_n,
			 scan_n
			 );
   // Parameter declarations
   parameter DEPTH = $depth;
   parameter ADDR_WIDTH = $addr_width;
   parameter DATA_WIDTH = $width;
   parameter BYTES = DATA_WIDTH/8;
   parameter RISING = $edgeValue;
END
   if (/$ROMCFG/) {
       print << "END";
   parameter OPERATION_RAM = 1'b0;
END
   }
   print << "END";

   input sin ;
   output sout ;
   input  clk ;

$portdecls
   input 		   shift_n ;
   input 		   slp_n ;
   input 		   scan_n ;

$byte_to_bit_code

   carbon_ram_core #(ADDR_WIDTH, DATA_WIDTH, DEPTH) mem_core
     (
      // Outputs
      .CM_DO				($data_out_name),
      // Inputs
      .CM_WE				(~$cs_write_name),
      .CM_BE				($be_instance),
      .CM_WADR				($write_addr_name),
      .CM_DI				($data_in_name),
      .CM_RADR				($read_addr_name),
      .CM_CLK				(clk));

   // Deal with unused inputs
   always @(posedge clk) begin
      if (!shift_n || !slp_n || !scan_n) begin
	 \$write ("ASSERTION at time %0t: ", \$stime);
	 if (!shift_n)
	   \$write ("shift_n is %0d, expected 1 ", shift_n);
	 if (!slp_n)
	   \$write ("slp_n is %0d, expected 1 ", slp_n);
	 if (!scan_n)
	   \$write ("scan_n is %0d, expected 1 ", scan_n);
      end
   end


endmodule
END
  }
  elsif (/$ROMCFG/) {
    # print("// ***rom ");
  }
else {
    # print "other";
 }
}
#!/usr/bin/perl

#*****************************************************************************
# Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
#*****************************************************************************

# This will parse a memory file list and generate wrappers for the Carbon memory core.
#
# To run this:
# perl parse_mem_table < memory_table_name  > new_memory_file_name
#
# Note: this is a temporary solution.  This will eventually be refitted into the remodeling wizard.
#

# We care about: name, dsize (address bit width), num words, mux(?), write mask, ports.  Others affect extra pins.

# Version 1: initial coding
# Version 2: Added byte enables

use POSIX;

#$numArgs = $#ARGV + 1;
#foreach $argnum (0 .. $ARGV) {
#    print "ARGV($argnum)\n";
#}



$RAMLISTCFG = qr/
^
(\S+)  # prefix
\s+
(\d+)  # data bits
\s+
(\d+)  # w_size
\s+
(rise|fall)   # clk edge
\s+
\w+\s+
\w+\s+
(\d)   # mux
\s+
\w+\s+  #wl_banks
\w+\s+  #bl_banks
\w+\s+  #core
\w+\s+  #comp_type
\w+\s+  #acc
\w+\s+  #banks
\w+\s+  #str
\w+\s+  #core_dev
\w+\s+  #core_fs
\w+\s+  #peri_dev
\w+\s+  #peri_fs
(\w+)   #writable
\s+
(\d+)  # ports [Total][Read][write][clocks]
\s+    
\w+\s+ #impl
\w+    #bist
/x;



$RAMDPLISTCFG = qr/
^
(\S+)  # prefix
\s+
(\d+)  # data bits
\s+
(\d+)  # w_size
\s+
(rise|fall)   # clk edge
\s+
\w+\s+
\w+\s+
(\d)   # mux
\s+
\w+\s+  #wl_banks
\w+\s+  #bl_banks
\w+\s+  #core
\w+\s+  #comp_type
\w+\s+  #str
\w+\s+  #core_dev
\w+\s+  #core_fs
\w+\s+  #peri_dev
\w+\s+  #peri_fs
(\w+)   #writable
\s+
(\d+)  # ports [Total][Read][write][clocks]
\s+    
\w+\s+ #impl
\w+    #bist
/x;

$ROMCFG = qr/
^
(\S+)  # prefix
\s+
(\d+)  # data bits
\s+
(\d+)  # w_size
\s+
(r|f)   # clk edge
\s+
\w+     #code
\s+
(\d+)   # mux
\s+
\w+\s+  #wl_banks
\w+\s+  #bl_banks
\w+\s+  #comp_type
\w+\s+  #str
\w+\s+  #core_dev
\w+\s+  #core_hs
\w+\s+  #peri_dev
\w+\s+  #peri_fs
\w+     #diff
\s+
\w+     #tech
/x;





foreach (<>) {
  if (/$RAMLISTCFG/ || /$RAMDPLISTCFG/ || /$ROMCFG/ ) {
    #print "Match happened\n";
    $prefix = $1;
    $width = $2;
    $depth = $3;
    $edge = $4;
    $mux = $5;
    #fake out writer
    if (/$ROMCFG/) {
      $writable = "wd";
      $ports = "2111";;
    }
    else {
      $writable = $6;
      $ports = $7;
    }

    #print "prefix = $prefix ";
    #print "width = $width ";
    #print "depth = $depth ";
    #print "edge = $edge ";
    #print "mux = $mux ";
    #print "writable = $writable  ";
    #print "ports = $ports \n";
    $addr_width = POSIX::ceil(log($depth)/log(2));
    $edgeValue = "1'b1";
    if ($edge eq "f" or $edge eq "fall") {
        $edgeValue = "1'b0";
    }

    $byte_to_bit_code = "";	# code to convert byte enable to bit enable, if needed
    $be_instance = "{DATA_WIDTH{1'b1}}"; # What gets passed into core

    if ($ports == 2111) {
      $read_addr_name = "addra";
      $write_addr_name = "addrb";
      $data_out_name = "douta";
      $data_in_name = "dinb";
      $cs_read_name = "csa_n";
      $cs_write_name = "csb_n";
      $portnames = << "END";
                         $read_addr_name,
                         $write_addr_name,
                         $data_out_name,
                         $data_in_name,
                         $cs_read_name,
                         $cs_write_name,
END
      $portdecls = << "END";
   input [ADDR_WIDTH-1:0] $read_addr_name ;
   output [DATA_WIDTH-1:0]$data_out_name ;
   
   input [ADDR_WIDTH-1:0]  $write_addr_name ;
   input [DATA_WIDTH-1:0]  $data_in_name ;

   input 		   $cs_read_name ;
   input 		   $cs_write_name ;
END
    }
    else {
      # Single port
      $read_addr_name = "addr";
      # print "read_addr_name = $read_addr_name";
      $write_addr_name = "addr";
      # print "write_addr_name = $write_addr_name";
      $data_in_name = "din";
      # print "data_in_name = $data_in_name";
      $data_out_name = "dout";
      # print "data_out_name = $data_out_name";
      $cs_read_name = "cs_n";
      # print "read_name = $cs_read_name";
      $cs_write_name = "we_n";
      # print "write_name = $cs_write_name";
      $write_addr_name = "addr";
      # print "write_addr_name = $write_addr_name";
      # Byte enable?
      if ($writable eq "by") {
	$data_enable_name = "wby_n";
	$be_instance = "temp_bit_enable";
      }
      # Bit enable
      elsif ($writable eq "bt") {
	$data_enable_name = "wbt_n";
	$be_instance = "~wby_n";
      }
      # Neither
      else {
	$data_enable_name = "";
      }
      $portnames = << "END";
                         $read_addr_name,
                         $data_out_name,
                         $data_in_name,
                         $cs_read_name,
                         $cs_write_name,
END
      $portdecls = << "END";
   input [ADDR_WIDTH-1:0] $read_addr_name ;
   output [DATA_WIDTH-1:0]$data_out_name ;
   
   input [DATA_WIDTH-1:0]  $data_in_name ;

   input 		   $cs_read_name ;
   input 		   $cs_write_name ;
END
      # Byte enable?
      if ($writable ne "wd") {
	$portnames .= << "END";
                         $data_enable_name,
END
	# Yes, width is number of bytes
	if ($writable eq "by") {
	  $portdecls .= << "END";
   input [BYTES-1:0]  $data_enable_name ;
END
	  $byte_to_bit_code = << "END";
   integer i;
   # print " number 4";
   reg [DATA_WIDTH-1:0]  temp_bit_enable ;

   always @($data_enable_name) begin
     for (i = 0; i < BYTES; i = i + 1) begin
       // Generate bit enable from byte enables
       temp_bit_enable = (temp_bit_enable << 8) | {8 {~${data_enable_name}[BYTES-i-1]}};
     end
   end
END
	}
	else {
	  # No, but enable, width is number of bits
	  $portdecls .= << "END";
   input [DATA_WIDTH-1:0]  $data_enable_name ;
END
	}
      }
    }				# end of single port
    print "// ${prefix}${depth}x${width} : $writable, $ports, $edge, $mux\n";
    print "module ${prefix}${depth}x${width} (\n";
    print <<"END";
			 sin,
			 sout,	
			 clk,
$portnames
			 shift_n,
			 slp_n,
			 scan_n
			 );
   // Parameter declarations
   parameter DEPTH = $depth;
   parameter ADDR_WIDTH = $addr_width;
   parameter DATA_WIDTH = $width;
   parameter BYTES = DATA_WIDTH/8;
   parameter RISING = $edgeValue;
END
   if (/$ROMCFG/) {
       print << "END";
   parameter OPERATION_RAM = 1'b0;
END
   }
   print << "END";

   input sin ;
   output sout ;
   input  clk ;

$portdecls
   input 		   shift_n ;
   input 		   slp_n ;
   input 		   scan_n ;

$byte_to_bit_code

   carbon_ram_core #(ADDR_WIDTH, DATA_WIDTH, DEPTH) mem_core
     (
      // Outputs
      .CM_DO				($data_out_name),
      // Inputs
      .CM_WE				(~$cs_write_name),
      .CM_BE				($be_instance),
      .CM_WADR				($write_addr_name),
      .CM_DI				($data_in_name),
      .CM_RADR				($read_addr_name),
      .CM_CLK				(clk));

   // Deal with unused inputs
   always @(posedge clk) begin
      if (!shift_n || !slp_n || !scan_n) begin
	 \$write ("ASSERTION at time %0t: ", \$stime);
	 if (!shift_n)
	   \$write ("shift_n is %0d, expected 1 ", shift_n);
	 if (!slp_n)
	   \$write ("slp_n is %0d, expected 1 ", slp_n);
	 if (!scan_n)
	   \$write ("scan_n is %0d, expected 1 ", scan_n);
      end
   end


endmodule
END
  }
  elsif (/$ROMCFG/) {
    # print("// ***rom ");
  }
else {
    # print "other";
 }
}
