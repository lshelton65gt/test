/****************************************************************************
 * Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
 * 
 * THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 * DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 * THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 * APPEARS IN ALL COPIES OF THIS SOFTWARE.
 ****************************************************************************/

/*
 * This is a generic N-read, N-write single-clock synchronous memory core.
 * This is NOT intended to be synthesizable.  It IS intended to
 * be Carbonizable.
 */

module carbon_ram_core(
		       CM_CLK,	// Clock in
		       CM_DI,	// Data in
		       CM_DO,	// Data out
		       CM_RADR,	// Read address
		       CM_WADR,	// Write address
		       CM_WE,	// Write enable (true/false)
		       CM_BE	// Bit enable (width of data in)
		       );
   // These parameters are overridden by wrapper
   // Wrapper should make ADDR_WIDTH = ceil(log2(DEPTH))
   parameter ADDR_WIDTH = 32;
   parameter DATA_WIDTH = 32;
   parameter DEPTH = 64;
   // Under construction (N read, M write)
   parameter NREAD_PORTS = 1;
   parameter NWRITE_PORTS = 1;
   parameter WRITE_BEFORE_READ = 0;// Add bypass
   parameter RISING = 1'b1;

  // Write Port
  input CM_WE;
  input [NWRITE_PORTS*ADDR_WIDTH-1:0] CM_WADR;// Write address
  input [NWRITE_PORTS*DATA_WIDTH-1:0] CM_DI;  // Data in
  input [NWRITE_PORTS*DATA_WIDTH-1:0] CM_BE;  // Bit enable
  
  // Read Port
  output [NREAD_PORTS*DATA_WIDTH-1:0] CM_DO;  // Data out
  input  [NREAD_PORTS*ADDR_WIDTH-1:0] CM_RADR;// Read address

  input                   CM_CLK;

  reg    [NREAD_PORTS*DATA_WIDTH-1:0] CM_DO;
  
  // The memory
  reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];
  
 generate
 if(RISING ==1'b1) begin
  always @(posedge CM_CLK) begin
      // Read port
     if (WRITE_BEFORE_READ && (CM_RADR == CM_WADR))
	// If the addresses are the same, just do a bypass
       CM_DO <= CM_DI;
     else
       // Read port
       CM_DO <= mem[CM_RADR];
    
     // Write port
     if (CM_WE)
       // Bit enable on means take the data from the input.  Else, recycle the memory.
       // Handle byte enables in the wrapper
       mem[CM_WADR] = (CM_DI & CM_BE) | (mem[CM_WADR] & ~CM_BE);
     end
 end
else begin    
    always @(negedge CM_CLK)  begin
              // Read port
     if (WRITE_BEFORE_READ && (CM_RADR == CM_WADR))
	// If the addresses are the same, just do a bypass
       CM_DO <= CM_DI;
     else
       // Read port
       CM_DO <= mem[CM_RADR];
    
     // Write port
     if (CM_WE)
       // Bit enable on means take the data from the input.  Else, recycle the memory.
       // Handle byte enables in the wrapper
       mem[CM_WADR] = (CM_DI & CM_BE) | (mem[CM_WADR] & ~CM_BE);
     end
end        
endgenerate
  
endmodule
