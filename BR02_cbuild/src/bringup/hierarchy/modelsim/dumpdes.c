/*
 * Copyright 1991-2008 Mentor Graphics Corporation
 *
 * All Rights Reserved.
 *
 * THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY INFORMATION WHICH IS THE PROPERTY OF 
 * MENTOR GRAPHICS CORPORATION OR ITS LICENSORS AND IS SUBJECT TO LICENSE TERMS.
 *
 * Prints the signal hierarchy to the transcript.
 *
 * The entry point is dump_design_init().
 */

#include <stdio.h>
#include "mti.h"

static FILE *f;
#define mti_PrintFormatted(ARGS...) fprintf(f, ARGS)

#ifdef NOTUSED
static void dump_type( mtiTypeIdT type )
{
    char **enum_vals;
    long   left, right;
    long   i;
    int    kind;

    kind = mti_GetTypeKind(type);
    switch ( kind ) {
      case MTI_TYPE_ARRAY:
        left  = mti_TickLeft(type);
        right = mti_TickRight(type);
        mti_PrintFormatted( "Array(%ld to %ld) of ", left, right );
        dump_type( mti_GetArrayElementType( type ) );
        break;
      case MTI_TYPE_ENUM:
        mti_PrintFormatted( "(" );
        enum_vals = mti_GetEnumValues( type );
        right = mti_TickRight( type );
        for ( i = 0; i <= right; i++ ) {
            mti_PrintFormatted( enum_vals[i] );
            if ( i != right ) {
                mti_PrintFormatted( ", " );
            }
        }
        mti_PrintFormatted( ")" );
        break;
      case MTI_TYPE_SCALAR:
      case MTI_TYPE_INTEGER:
        mti_PrintFormatted( "Integer" );
        break;
      case MTI_TYPE_REAL:
        mti_PrintFormatted( "Real" );
        break;
      case MTI_TYPE_TIME:
        mti_PrintFormatted( "Time" );
        break;
      default:
        mti_PrintFormatted( "Unknown type: %d", kind );
        break;
    }
}

static void dump_signal( mtiSignalIdT sig, int margin )
{
    mti_PrintFormatted( "%*sSignal %s : ", margin, " ",
                       mti_GetSignalName( sig ) );
    switch ( mti_GetSignalMode( sig ) ) {
      case MTI_INTERNAL:
        break;
      case MTI_DIR_IN:
        mti_PrintFormatted( "IN " );
        break;
      case MTI_DIR_OUT:
        mti_PrintFormatted( "OUT " );
        break;
      case MTI_DIR_INOUT:
        mti_PrintFormatted( "INOUT " );
        break;
      default:
        mti_PrintFormatted( "?MODE? " );
        break;
    }
    dump_type( mti_GetSignalType( sig ) );
    mti_PrintFormatted( "\n" );
}

static void dump_variable( mtiVariableIdT var, int margin )
{


    mti_PrintFormatted( "%*sVariable %s : ", margin, " ",
                       mti_GetVarName( var ) );

    mti_PrintFormatted( "%*sTypeNum %d : ", margin, " ",
                       mti_GetVarType( var ) );

    mti_PrintFormatted( "%*sImage %s\n", margin, " ",
                       mti_GetVarImageById( var ) );

}


static void dump_process( mtiProcessIdT proc, int margin )
{
    mtiVariableIdT var;

    mti_PrintFormatted( "%*sProcess %s\n ", margin, " ",
                       mti_GetProcessName( proc ) );
    margin += 2;

    for ( var = mti_FirstVar( proc ); var; var = mti_NextVar() ) {
      dump_variable( var, margin );
    }
}
#endif
void printGenericList( mtiInterfaceListT * generic_list, int free_it, int margin )
{
  mtiInterfaceListT * glp;
  mtiInterfaceListT * glp_next;

  if (!generic_list)
    return;
  margin += 2;
  mti_PrintFormatted( "%*s<Parameters>\n", margin, " ");
  margin += 2;
  for ( glp = generic_list; glp; glp = glp_next ) {

    mti_PrintFormatted( "%*s<Parameter name=\"%s\" value=\"", margin, " ", glp->name );
    //    mti_PrintFormatted( " %s =", glp->name );
    switch ( mti_GetTypeKind( glp->type ) ) {
    case MTI_TYPE_ENUM:
    case MTI_TYPE_PHYSICAL:
    case MTI_TYPE_SCALAR:
      mti_PrintFormatted( "%d", glp->u.generic_value );
      break;
    case MTI_TYPE_REAL:
      mti_PrintFormatted( "%g", glp->u.generic_value_real );
      break;
    case MTI_TYPE_TIME:
      mti_PrintFormatted( "[%d,%d]",
			  MTI_TIME64_HI32(glp->u.generic_value_time),
			  MTI_TIME64_LO32(glp->u.generic_value_time) );
      break;
    case MTI_TYPE_ARRAY:
      {
	int i;
	mtiInt32T num_elems = mti_TickLength( glp->type );
	mtiTypeIdT elem_type = mti_GetArrayElementType( glp->type );
	switch ( mti_GetTypeKind( elem_type ) ) {
	case MTI_TYPE_PHYSICAL:
	case MTI_TYPE_SCALAR:
	  {
	    mtiInt32T * val = glp->u.generic_array_value;
	    for ( i = 0; i < num_elems; i++ ) {
	      mti_PrintFormatted( " %d", val[i] );
	    }
	  }
	  break;
	case MTI_TYPE_ARRAY:
	  mti_PrintFormatted( " ARRAY of ARRAYs" );
	  break;
	case MTI_TYPE_RECORD:
	  mti_PrintFormatted( " ARRAY of RECORDs" );
	  break;
	case MTI_TYPE_ENUM:
	  {
	    char ** enum_values = mti_GetEnumValues( elem_type );
	    char * array_val = glp->u.generic_array_value;
	    for ( i = 0; i < num_elems; i++ ) {
	      mti_PrintFormatted( " %s",
				  enum_values[array_val[i]] );
	    }
	  }
	  break;
	case MTI_TYPE_REAL:
	  {
	    double * val = glp->u.generic_array_value;
	    for ( i = 0; i < num_elems; i++ ) {
	      mti_PrintFormatted( " %g", val[i] );
	    }
	  }
	  break;
	case MTI_TYPE_TIME:
	  {
	    mtiTime64T * val = glp->u.generic_array_value;
	    for ( i = 0; i < num_elems; i++ ) {
	      mti_PrintFormatted( " [%d,%d]",
				  MTI_TIME64_HI32(val[i]),
				  MTI_TIME64_LO32(val[i]) );
	    }
	  }
	  break;
	default:
	  break;
	}
	mti_PrintFormatted( "" );
      }
      break;
    default:
      mti_PrintFormatted( "\"" );
      break;
    }
    mti_PrintFormatted( "\">\n" );
    glp_next = glp->nxt;
    if ( free_it ) {
      mti_Free( glp );
    }
  }
  margin -= 2;
  mti_PrintFormatted( "%*s</Parameters>\n", margin, " ");
}

static void dump_region( mtiRegionIdT region, int margin )
{
    mtiSignalIdT sig;
    mtiProcessIdT proc;
    mtiInterfaceListT * generic_list;

    if ( region ) {
        margin += 2;

        mti_PrintFormatted( "%*s<Instance id=\"%s\" interface=\"%s\" library=\"%s\" architecture=\"%s\" locator=\"%s\">\n",
			    margin, " ",
			    mti_GetRegionName( region ),
			    mti_GetPrimaryName( region ),
			    mti_GetLibraryName( region ),
			    mti_GetSecondaryName( region ),
			    mti_GetRegionSourceName( region )
			    );
	/*
        mti_PrintFormatted( "%*sRegion: %s\n", margin, " ",
                           mti_GetRegionName( region ) );
        mti_PrintFormatted( "%*sLibrary: %s\n", margin, " ",
                           mti_GetLibraryName( region ) );
        mti_PrintFormatted( "%*sPrimary: %s\n", margin, " ",
                           mti_GetPrimaryName( region ) );
        mti_PrintFormatted( "%*sFull: %s\n", margin, " ",
                           mti_GetRegionFullName( region ) );
        mti_PrintFormatted( "%*sSource: %s\n", margin, " ",
                           mti_GetRegionSourceName( region ) );
        mti_PrintFormatted( "%*sSecondary: %s\n", margin, " ",
                           mti_GetSecondaryName( region ) );
	*/
	/* Get the generics for this region */
	generic_list = mti_GetGenericList( region );
	printGenericList( generic_list, 1, margin );

        //margin += 2;
        //mti_PrintFormatted( "%*sPush Level\n", margin, " ");//; " ",
#ifdef NOTUSED
        /* Print the signals in this region */
        for ( sig = mti_FirstSignal( region ); sig; sig = mti_NextSignal() ) {
            dump_signal( sig, margin );
        }

        for ( proc = mti_FirstProcess( region ); proc; proc = mti_NextProcess() ) {
            dump_process( proc, margin );
        }
#endif
        /* Do the lower level regions */
        for ( region = mti_FirstLowerRegion( region ); region;
             region = mti_NextRegion( region ) ) {
            dump_region( region, margin );
        }

	mti_PrintFormatted( "%*s</Instance>\n", margin, " ");
	//        mti_PrintFormatted( "%*sPop Level\n", margin, " ");//; " ",
	  //                           mti_GetSecondaryName( region ) );
    }
}

static void dump_design( void * param )
{
  f = fopen ("hierarchy.log", "w");
  fprintf (f, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
  fprintf (f, "<DesignHierarchy>\n");

    dump_region( mti_GetTopRegion(), 0 );
    fprintf (f, "</DesignHierarchy>\n");
    fclose(f);
}

/* Initialization Function */
void dump_design_init(
  mtiRegionIdT       region,
  mtiInterfaceListT *generics,
  mtiInterfaceListT *ports,
  char              *param
)
{
    mti_AddLoadDoneCB( dump_design, NULL );
}
