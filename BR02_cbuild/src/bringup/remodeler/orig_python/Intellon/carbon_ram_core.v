/****************************************************************************
 * Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
 * 
 * THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 * DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 * THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 * APPEARS IN ALL COPIES OF THIS SOFTWARE.
 ****************************************************************************/

/*
 * This is a generic 1-read, 1-write single-clock synchronous memory core.
 * This is NOT intended to be synthesizable.  It IS intended to
 * be Carbonizable.  Also, it is inteneted to be instantiated from an
 * auto-generated wrapper, and not directly from a design.
 * 
 * If more ports are needed, instantiate this twice or more.  All instances will
 * access the same copy of the actual memory array.
 * 
 * Note that in the case of multiple ports, contention detection, if implemented,
 * would need to be done in the wrapper.
 */

module carbon_ram_core(
		       CM_CLK,	// Clock in
		       CM_DI,	// Data in
		       CM_DO,	// Data out
		       CM_RADR,	// Read address
		       CM_WADR,	// Write address
		       CM_WE,	// Write enable (true/false)
		       CM_BE	// Bit enable (width of data in)
		       );
   // These parameters are overridden by wrapper
   // Wrapper should make ADDR_WIDTH = ceil(log2(DEPTH))
   parameter ADDR_WIDTH = 32;
   parameter DATA_WIDTH = 32;
   parameter DEPTH = 64;
   parameter WRITE_BEFORE_READ = 0;// Add bypass

  // Write Port
  input CM_WE;
  input [ADDR_WIDTH-1:0] CM_WADR;// Write address
  input [DATA_WIDTH-1:0] CM_DI;  // Data in
  input [DATA_WIDTH-1:0] CM_BE;  // Bit enable
  
  // Read Port
  output [DATA_WIDTH-1:0] CM_DO;  // Data out
  input  [ADDR_WIDTH-1:0] CM_RADR;// Read address

  input                   CM_CLK;

  reg    [DATA_WIDTH-1:0] CM_DO;
  
  always @(posedge CM_CLK) begin
     
     // Read port
     if (WRITE_BEFORE_READ && (CM_RADR == CM_WADR))
	// If the addresses are the same, just do a bypass
       CM_DO <= CM_DI;
     else
       // Read port
       CM_DO <= cmem.array[CM_RADR];
    
     // Write port
     if (CM_WE)
       // Bit enable on means take the data from the input.  Else, recycle the memory.
       // Handle byte enables in the wrapper
       cmem.array[CM_WADR] = (CM_DI & CM_BE) | (cmem.array[CM_WADR] & ~CM_BE);
     task_call;
  end

endmodule

// This is instantiated from the wrapper, so that multiple ports will
// access the same array
module carbon_memory_array();
   parameter DATA_WIDTH = 32;
   parameter DEPTH = 64;
  // The memory
  reg [DATA_WIDTH-1:0] array[0:DEPTH-1];

endmodule
