#******************************************************************************
# Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
# OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
# DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
# OF CARBON DESIGN SYSTEMS, INC.
#******************************************************************************


# The class writes a wrapper around the generic carbon_sync_mem_core.v model.
# This is a synchronous memory with one or two ports, where each can have its own clock.

# Wrapper generation parameters:
#  parameters["WriteEnable"]: "BitEnable", "ByteEnable", "WordEnable" (no port needed)

# Verilog parameter overrides:
#   Python variable          Verilog param    Description
#   parameters["AddrWidth"]  ADDR_WIDTH   width of the address line in bits
#   parameters["DataWidth"]  DATA_WIDTH   width of the data line in bits
#   parameters["Depth"]      DEPTH        number of memory entries:
#                                                      ceil (log2 (ADDR_WIDTH))
#   parameters["WriteBeforeRead"]WRITE_BEFORE_READ
#                                         1: if read and write addresses are the same, add a bypass
#                                         2: Use seperate clocks, so no bypass
#   parameters["IgnoreParams"]            Number of module parameters to pad

# The port types are:
#  Python variable       Width       Dir'n  Description
#  ports[n]["ReadAddrName"] ADDR_WIDTH  input  Name of the read address port
#  ports[n]["WriteAddrName"]ADDR_WIDTH  input  Name of write address port, if two ports
#  ports[n]["DataOutName"]  DATA_WIDTH  input  Name of data read port
#  ports[n]["DataInName"]   DATA_WIDTH  output Name of data write port
#  ports[n]["DataEnable"]   BYTE or DATA_WIDTH  input  if set, allow data to pass into memory
#  ports[n]["CsReadName"]   1           input  Enable reads
#  ports[n]["CsWriteName"]  1           input  Enable writes
#  ports[n]["Clk"]          1           input  Clock
#  ports[n]["ExtraAsserts"]             input  List of ports not used, but asserted
#  ports[n]["ExtraAssertsPolarity"]            Polarity of assertion
#  ports[n]["ExtraAssertsExpect"]              Expected value of assertion
#  ports[n]["ExtraInputs"]              input  List of ignored inputs...
#  ports[n]["ExtraInputsWidth"]                ...and their widths
#  ports[n]["ExtraOutputs"]             output List of unused outputs...
#  ports[n]["ExtraOutputsWidth"]               ...and their widths (tied to 0)

import string

class CarbonMemSync:
    def __init__(self, name, parameters, ports):
        self.__mName = name
        self.__mParameters = parameters
        self.__mPorts = ports

    # This is the main entrypoint
    def emitWrapper(self):
        # Depending on the parameters, collect the various parts of the wrapper
#        partsDict = self.makeParts(self.__mPorts[0])
        wrapperString = self.getWrapperTemplate()
        wrapperTemplate = string.Template(wrapperString);

        # Maybe some day it will be easier to merge dictionaries
        # Alternatives that are messy:
        # d = dict(self.__mParameters.items() + self.__mPorts.items() + partsDict.items() )
        # d = reduce(lambda x, y: dict(x, **y), [self.__mParameters, self.__mPorts, partsDict] )
        # This one is arguably cleanest and simplest:
        d = {}
        d.update(self.__mParameters)
#        for i in xrange(len(self.__mPorts)):
#            d.update(self.__mPorts[i])
        d.update(ModuleName = self.__mName)

        # Do this is two passes because this first pass copies in more template-ish stuff;
        # The second pass fills in that template
        wrapperPass1 = string.Template(wrapperString).safe_substitute(d)
        wrapperCode = string.Template(wrapperPass1).safe_substitute(d)
        return wrapperCode

    def makeParts(self, ports):
        portlist = ""
        portdecl = ""
        toBitCode = ""


        # Default word enable instance name (i.e. no masking at all)
        enableInstance = "{DATA_WIDTH{1'b1}}"

        # For ROMs, don't supply a data in or write enable
        if ports.get("DataInName", "") != "":
            portlist += '''\
                         $DataInName,
'''

            portdecl += '''\
   input [DATA_WIDTH-1:0]  $DataInName;
'''

            # In the dual-port case, add the write port address
            if ports.get("WriteAddrName", "") != "":
                portlist += '''\
                         $WriteAddrName,
'''
                portdecl += '''\
   input [ADDR_WIDTH-1:0]  $WriteAddrName;
'''

            if ports.get("CsWriteName", "") != "":
                portlist += '''\
                         $CsWriteName,
'''

                portdecl += '''\
   input 		   $CsWriteName;
'''

            # If there are byte enables, add them
            if ports.get("DataEnableName", "") != "":
                portlist += '''\
                         $DataEnableName,
'''

                # Add bit/byte enable ports
                enableInstance = "write_enable_bits"
                portdecl += '''\
   input [(DATA_WIDTH+$DataEnableSize-1)/$DataEnableSize-1:0]  $DataEnableName;
'''

                toBitCode = '''\
   integer i;
   reg [DATA_WIDTH-1:0]  write_enable_bits;

   always @($DataEnableName) begin
     write_enable_bits =
       (~{
'''

                we_size = int(ports["DataEnableSize"])
                toBitCode += "          "
                j = 0
                for i in reversed(xrange(int(self.__mParameters["DataWidth"]))):
                    toBitCode += "$DataEnableName[%d]," % (i/we_size)
                    j += 1
                    if j > 7:
                        toBitCode += "\n          "
                        j = 0
                    
                toBitCode = toBitCode.rstrip(", \n") + "\n"# remove trailing comma
                toBitCode += '''\
         });
   end
'''
        # Some dual-ported memories don't have a data out port on that port
        if ports.get("DataOutName", "") != "":
            portlist += '''\
                         $DataOutName,
'''
            portdecl += '''\
   output [DATA_WIDTH-1:0]$DataOutName;
   reg    [DATA_WIDTH-1:0]$DataOutName;
'''

        if ports.get("CsReadName", "") != "":
            portlist += '''\
                         $CsReadName,
'''
            portdecl += '''\
   input                  $CsReadName;
'''
        # Fill in the common ports for all cases
        portlist += '''\
                         $ReadAddrName,
                         $Clk,
'''


        portdecl += '''\
   input [ADDR_WIDTH-1:0] $ReadAddrName;
   input 		  $Clk;
'''


        # Fill in the extra ports

        for p in ports.get("ExtraInputs", []) + ports.get("ExtraAsserts", []):
            portlist += '''\
                         %s,
''' % p[0]

            portdecl += '''\
   input %s                %s;
''' % (p[1], p[0])

        for p in [x for x in ports.get("ExtraOutputs", []) if x != ""]:
            portlist += '''\
                         %s,
''' % p[0]

            portdecl += '''\
   output %s               %s;
''' % (p[1], p[0])

#        portlist = portlist.rstrip(", \n") # Remove trailing commas, etc
        return { 'portList'  : string.Template(portlist).substitute(ports),
                 'portDecl' : string.Template(portdecl).substitute(ports),
                 'toBitCode' : string.Template(toBitCode).substitute(ports),
                 'enableInstance' : enableInstance }

# ******************************************************************************

    def getWrapperTemplate(self):
        ignoreParams = ""
        if self.__mParameters.has_key("IgnoreParams"):
            for i in range(self.__mParameters["IgnoreParams"]):
                ignoreParams += '''\
   parameter C_IGNORE%d = 0;
''' % i

        bodyCode = ""
        partsDict = {}
        for i in xrange (len(self.__mPorts)):
            partsDicta = self.makeParts(self.__mPorts[i])
            # concat the keys
            for key in partsDicta.keys():
                partsDict[key] = partsDict.get(key, "") + partsDicta[key]
            bodyCode += self.makePerPortBody(self.__mPorts[i], i)

        code = '''\
module $ModuleName (
%(portList)s
			 );
   // Parameter declarations
%(ignore_params)s
   parameter DEPTH = $Depth;
   parameter ADDR_WIDTH = $AddrWidth;
   parameter DATA_WIDTH = $DataWidth;
   parameter BYTES = DATA_WIDTH/8;
   parameter WRITE_BEFORE_READ = 0;  // Implement this later

%(portDecl)s
  reg [DATA_WIDTH-1:0] memory[0:DEPTH-1];
%(toBitCode)s
%(body_code)s
/*
   carbon_memory_array #(DATA_WIDTH, DEPTH) cmem();
*/
endmodule
''' % dict (partsDict,  ignore_params = ignoreParams,
        body_code = bodyCode)

        return code


    def makeAssertion (self, ports):
        code = ""
        if not ports.has_key("ExtraAsserts") or len(ports["ExtraAsserts"]) == 0:
            return code

        code += '''\
   // Deal with unused inputs
   always @(posedge %(clk)s) begin
''' % {'clk' : ports["Clk"]}

        code += '     if ('
#        alist = ports["ExtraAsserts"]
#        apolarity = ports["ExtraAssertsPolarity"]
#        aexpect = ports["ExtraAssertsExpect"]
        for a in ports.get("ExtraAsserts", []):
            code += '%s != %s || ' %( a[0], a[2] )
        code = code[0:-4]               # Remove the last " ||"
        code += ''') begin
           $$write ("ASSERTION at time %%0t: ", $$stime);
'''
        for a in ports.get("ExtraAsserts", []):
            code += '''\
	 if (%(name)s != %(value)s)
           $$write ("%(name)s is %%0d, expected %(value)s ", %(name)s);
''' % { 'name' : a[0],
        'value' : a[2]}

        code += '''\
      end
   end
'''
        return code


    def makeWriteCode(self, ports):
        if ports.get("DataInName", "") == "":
            code = ""
        else:
            code = '''\
     // Write port
     if (%(cs_write_name)s)
       // Bit enable on means take the data from the input.  Else, recycle the memory.
       // Handle byte enables in the wrapper
       memory[%(read_or_write_addr_name)s] = ($DataInName & %(write_enable_bits)s) | (memory[%(read_or_write_addr_name)s] & ~%(write_enable_bits)s);
''' % { 'read_or_write_addr_name' : (ports.get("WriteAddrName", "") != "") and \
          "$WriteAddrName" or "$ReadAddrName",
        'write_enable_bits' : ((ports.get("DataEnableName", "") != "" and
                               ports.get("DataInName", "") != "") and
                               "write_enable_bits" or "{DATA_WIDTH{1'b1}}"),
#        "cs_write_name" : ((ports.get("CsWriteName", "") != "" and 
#                           ports.get("DataInName", "") != "") and
#                           "!$CsWriteName" or "1'b0") }
        # If there is no data in, then there is no write port,
        # so set the write enable to 1'b0.
        # Otherwise, if there is a write enable signal, use it,
        #   otherwise set it to 1'b1 and use the byte enable
        "cs_write_name" : (
            ports.get("DataInName", "") == "") and \
            "1'b0" or \
            (ports.get("CsWriteName", "") != "" and \
             "!$CsWriteName" or "1'b1") }
        return code

    def makeReadCode(self, ports):
        if ports.get("DataOutName", "") == "":
            code = ""
        elif ports.get("DataInName", "") != "":
            code = '''\
     // Read port
     if (WRITE_BEFORE_READ && ($ReadAddrName == %(read_or_write_addr_name)s))
	// If the addresses are the same, just do a bypass
       $DataOutName <= $DataInName;
     else
       $DataOutName <= memory[$ReadAddrName];
''' % { 'read_or_write_addr_name' : (ports.get("WriteAddrName", "") != "") and \
          "$WriteAddrName" or "$ReadAddrName"}
        else:
            code = '''\
     $DataOutName <= memory[$ReadAddrName];
'''
        return code

    def makePerPortBody(self, ports, iteration):
        code = '''\
/*
   carbon_ram_core #(ADDR_WIDTH, DATA_WIDTH, DEPTH) carbon_ram_core_%(iteration)d
     (
      // Outputs
      .CM_DO				($DataOutName),
      // Inputs
      .CM_WE				(%(cs_write_name)s),
      .CM_BE				(%(write_enable_bits)s),
      .CM_WADR				(%(read_or_write_addr_name)s),
      .CM_DI				($DataInName),
      .CM_RADR				($ReadAddrName),
      .CM_CLK				($Clk));
*/
  always @(posedge $Clk) begin

%(read_code)s

%(write_code)s
  end

%(assertion_code)s
''' % { 'assertion_code' : self.makeAssertion(ports),
        'read_code' : self.makeReadCode(ports),
        'write_code' : self.makeWriteCode(ports),
        'read_or_write_addr_name' : (ports.get("WriteAddrName", "") != "") and \
          "$WriteAddrName" or "$ReadAddrName",
        'write_enable_bits' : ((ports.get("DataEnableName", "") != "" and
                               ports.get("DataInName", "") != "") and
                               "write_enable_bits" or "{DATA_WIDTH{1'b1}}"),
        "cs_write_name" : ((ports.get("CsWriteName", "") != "" and 
                           ports.get("DataInName", "") != "") and
                           "!$CsWriteName" or "1'b0"),
        'iteration' : iteration }

        
        return string.Template(code).substitute(ports)
