# This is the QC-specific front-end to the memory wizard.  Eventually this should be a text file
# or some sort of configuration file

# Memory names are in the form:
#  qcsram1111_1rbyng00_32x64_4_hnrf_2_19_core
#        1111
#        2111
# 2 or 1 ports
#               by
#               bt
#               wd
# Byte/bit/word enables
#                      32x64
# depth by width

# For now, this is all we care about

import sys
import re
import math
import CarbonMemSync

def run(argv):

    # Set the defaults
    parameters = {}
    ports = []

    parameters["WriteEnable"] = "WordEnable"
    parameters["CoreInstName"] = "mem"

    # Arbitrary default value
    parameters["AddrWidth"] = 6
    parameters["DataWidth"] = 32
    parameters["Depth"] = 128

    # Assume one port
    parameters["WriteBeforeRead"] = 0

    # All of the instances have 2 module parameters.  Ignore them.
    parameters["IgnoreParams"] = 2

    # Default to the single-port case
    ports.append({})
    ports[0]["ReadAddrName"] = "i_addr"
    ports[0]["WriteAddrName"] = ""
    ports[0]["DataOutName"] = "i_dout"
    ports[0]["DataInName"] = "i_din"
    ports[0]["DataEnableName"] = ""
    ports[0]["DataEnableSize"] = 0
    ports[0]["CsReadName"] = "i_cs_n"
    ports[0]["CsWriteName"] = "i_we_n"
    ports[0]["Clk"] = "i_clk"
    ports[0]["ExtraAsserts"] = ["i_shift_n", "i_slp_n", "i_scan_n"]
    ports[0]["ExtraAssertsPolarity"] = ["!", "!", "!"]
    ports[0]["ExtraAssertsExpect"] = ["1", "1", "1"]
    ports[0]["ExtraInputs"] = ["i_sin"]
    ports[0]["ExtraInputsWidth"]= [1]
    ports[0]["ExtraOutputs"] = ["i_sout"]
    ports[0]["ExtraOutputsWidth"] = [1]


    # Open the file
    modulelistfile = open(argv[1], 'r')

    for txt in modulelistfile:
        re_name = 'module\s+(\S+)'
        rg = re.compile(re_name)
        name_match = rg.search(txt)
        module_name = name_match.group(1)

        # Break down the file name pattern

        # URL that generated this code:
        # http://txt2re.com/index-python.php3?s=qcsram1111_1rbyng00_32x64_4_hnrf_2_19_core&7&-4&27&-76&-77&16&-101&17&45&-52&-40&-35&-32&-14&-78&25&-79&-9&-80&-24&-81&-15&-82&-8&23


        re1='(qcsram)'	# Word 1
        re2='(\\d+)'	# Integer Number 1
        re3='(_)'	# Any Single Character 1
        re4='(\\d+)'	# Integer Number 2
        re5='(r)'	# Any Single Word Character (Not Whitespace) 1
        re6='([a-z])'	# Any Single Word Character (Not Whitespace) 2
        re7='([a-z])'	# Any Single Word Character (Not Whitespace) 3
        re8='(n)'	# Any Single Word Character (Not Whitespace) 4
        re9='(g)'	# Any Single Word Character (Not Whitespace) 5
        re10='(00)'	# Integer Number 3
        re11='(_)'	# Any Single Character 2
        re12='(\\d+)'	# Integer Number 4
        re13='(x)'	# Any Single Character 3
        re14='(\\d+)'	# Integer Number 5
        re15='(_)'	# Any Single Character 4
        re16='(\\d+)'	# Integer Number 6
        re17='(_)'	# Any Single Character 5
        re18='(hnrf)'	# Word 2
        re19='(_)'	# Any Single Character 6
        re20='(2)'	# Integer Number 7
        re21='(_)'	# Any Single Character 7
        re22='(19)'	# Integer Number 8
        re23='(_)'	# Any Single Character 8
        re24='(core)'	# Word 3

        rg = re.compile(re1+re2+re3+re4+re5+re6+re7+re8+re9+re10+re11+re12+re13+re14+re15+re16+re17+re18+re19+re20+re21+re22+re23+re24,re.IGNORECASE|re.DOTALL)
        m = rg.search(txt)
        if m:
            word1=m.group(1)
            int1=m.group(2)
            c1=m.group(3)
            int2=m.group(4)
            w1=m.group(5)
            w2=m.group(6)
            w3=m.group(7)
            w4=m.group(8)
            w5=m.group(9)
            int3=m.group(10)
            c2=m.group(11)
            int4=m.group(12)
            c3=m.group(13)
            int5=m.group(14)
            c4=m.group(15)
            int6=m.group(16)
            c5=m.group(17)
            word2=m.group(18)
            c6=m.group(19)
            int7=m.group(20)
            c7=m.group(21)
            int8=m.group(22)
            c8=m.group(23)
            word3=m.group(24)
            print "//("+word1+")"+"("+int1+")"+"("+c1+")"+"("+int2+")"+"("+w1+")"+"("+w2+")"+"("+w3+")"+"("+w4+")"+"("+w5+")"+"("+int3+")"+"("+c2+")"+"("+int4+")"+"("+c3+")"+"("+int5+")"+"("+c4+")"+"("+int6+")"+"("+c5+")"+"("+word2+")"+"("+c6+")"+"("+int7+")"+"("+c7+")"+"("+int8+")"+"("+c8+")"+"("+word3+")"
            #name = word1+int1+c1+int2+w1+w2+w3+w4+w5+int3+c2+int4+c3+int5+c4+")"+"("+int6+")"+"("+c5+")"+"("+word2+")"+"("+c6+")"+"("+int7+")"+"("+c7+")"+"("+int8+")"+"("+c8+")"+"("+word3+")"

            rwports = int1
            enables = w2+w3

            depth = int4
            width = int5
            addrBits = int (math.ceil (math.log(float(depth), 2)))
            parameters["AddrWidth"] = addrBits
            parameters["DataWidth"] = width
            parameters["Depth"] = depth

            if rwports == 2111:
                parameters["ReadPorts"] = 0

                ports[0]["ReadAddrName"] = "i_addra"
                ports[0]["WriteAddrName"] = "i_addrb"
                ports[0]["DataOutName"] = "i_douta"
                ports[0]["DataInName"] = "i_dinb"
                ports[0]["DataEnableName"] = "notused"
                ports[0]["CsReadName"] = "i_csa_n"
                ports[0]["CsWriteName"] = "i_csb_n"

            # Byte enables
            if enables == "by":
                ports[0]["DataEnableName"] = "i_wby_n"
                ports[0]["DataEnableSize"] = 8

            # Bit enables
            elif enables == "bt":
                ports[0]["DataEnableName"] = "i_wbt_n"
                ports[0]["DataEnableSize"] = 1


            wrapper = CarbonMemSync.CarbonMemSync(module_name, parameters, ports)
            code = wrapper.emitWrapper()

            print code

run(sys.argv)

