
set libList [::mtiLibrary::LibraryList]
echo "*******************"
set timestamp [clock format [clock seconds]]
set chan [open AETest.cmd w]
puts $chan "#Created on $timestamp"
foreach l [::mtiLibrary::LibraryList] {
  set libName [lindex $l 0]
  puts $chan "-vhdllib  $libName" 
  set srcList [vdir -lib $libName -prop src]
  #echo " srclist: $srcList"
  variable i 0;
  foreach s $srcList {
    if {$i == 4} {
      set i  0;
      puts $chan "$s"
    } else {
      incr i
    }
  }
}
close $chan





