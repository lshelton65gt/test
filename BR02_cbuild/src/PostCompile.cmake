# Rules for things that get built after compilation.
# These are related to cbuild (specifically backend stuff), so it's not needed with MSVC
if(${MSVC})
  return()
endif()

# Skip these when building for Windows, since they should have been created already in the Linux build.
if(NOT(${WIN32}))
  # crypt_messages - always extract these, and rebuild carbon_cfg.db
  set(cryptMessages ${CMAKE_BINARY_DIR}/crypt_messages)
  add_custom_target(ExtractCryptMessages
                     COMMAND ${CARBON_SANDBOX}/scripts/cryptograbber.pl ${CMAKE_SOURCE_DIR} ${cryptMessages})
  
  # carbon_cfg.db
  set(carbonCfgDB ${CARBON_HOME}/lib/carbon_cfg.db)
  add_custom_command(OUTPUT ${carbonCfgDB}
                     COMMAND ccrypt ${cryptMessages} ${carbonCfgDB}
                     DEPENDS ccrypt ${cryptMessages})
endif()

# carbon.*.ia
set(carbonIA ${CARBON_HOME}/lib/carbon.${CONFIGURED_COMPILER}.${ARCH}.${CARBON_BIN}.ia)

# Read the .bom files to get the list of headers
set(carbonIAHeaders "")
file(STRINGS ${CMAKE_SOURCE_DIR}/includes.bom bom)
foreach(f ${bom})
  set(carbonIAHeaders ${carbonIAHeaders} ${CMAKE_SOURCE_DIR}/inc/${f})
endforeach(f)
file(STRINGS ${CMAKE_SOURCE_DIR}/genincludes.bom bom)
foreach(f ${bom})
  set(carbonIAHeaders ${carbonIAHeaders} ${CARBON_OBJ}/${f})
endforeach(f)
# The working directory here is actually ${CARBON_OBJ}, but on Windows
# that's a different drive letter than the current cmake dir.  Cmake
# implements WORKING_DIRECTORY by doing a 'cd', which doesn't work on
# Windows if you're changing drives.  ${CMAKE_BINARY_DIR}/.. is
# equivalent.
add_custom_command(OUTPUT ${carbonIA}
                   COMMAND speedcc -build-db ${CMAKE_SOURCE_DIR}/includes.bom ${carbonIA} ${CMAKE_SOURCE_DIR}/genincludes.bom
                   DEPENDS PrepCarbonHome speedcc ${carbonIAHeaders} ${CMAKE_SOURCE_DIR}/includes.bom ${CMAKE_SOURCE_DIR}/genincludes.bom
                   WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/..
                   )


# Create a custom target so these all get built.
if(NOT(${WIN32}))
  add_custom_target(PostCompileTarget ALL
                    DEPENDS
                    PrepCarbonHome
                    ExtractCryptMessages
                    ${carbonCfgDB}
                    ${carbonIA}
                    )
else()
  add_custom_target(PostCompileTarget ALL
                    DEPENDS
                    ${carbonIA}
                    )
endif()
