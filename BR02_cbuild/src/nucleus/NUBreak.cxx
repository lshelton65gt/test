// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUBreak.h"
#include "nucleus/NUBlock.h"
#include "util/UtIndent.h"

NUBreak::NUBreak(NUBlock* block, StringAtom* keyword, StringAtom* name,
                 NUNetRefFactory* nrf, const SourceLocator& loc) :
  NUBlockStmt(nrf, false, loc),
  mTarget(block),
  mTargetName(name),
  mKeyword(keyword)
{
  block->putIsBreakTarget( true );
}

NUBreak::~NUBreak() {
}

//! Change this node to define new_net instead of old_net.
void NUBreak::replaceDef(NUNet *, NUNet* ) {
}

//! Replace all references to old_net with new_net.
bool NUBreak::replace(NUNet * , NUNet * ) {
  return false;
}

//! Return a copy of this statement; recurses to copy nested statements.
NUBreak* NUBreak::copy(CopyContext& cc) const {
  return new NUBreak( cc.lookup( mTarget ), mKeyword, mTargetName,
                      mNetRefFactory, mLoc );
}

//! Is this statement identical to another
bool NUBreak::operator==(const NUStmt& stmt) const {
  const NUBreak* brk = dynamic_cast<const NUBreak*>(&stmt);
  return (brk != NULL) && (mTarget == brk->mTarget);
}

//! Replace all net references based on a translation functor.
/*!
  Returns true if any lower net was successfully replaced.
 */
bool NUBreak::replaceLeaves(NuToNuFn &) {
  return false;
}

//! Code Generator
CGContext_t NUBreak::emitCode (CGContext_t) const {
  NU_ASSERT(0, this);
  return 0;
}

//! print human friendly
void NUBreak::compose(UtString* buf, const STBranchNode*, int indent_arg, bool)
  const
{
  UtIndent indent(buf);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  buf->append(numspaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  // Note that we hard-code 'disable' here rather than the keyword,
  // because we are generating verilog.
  *buf << "disable " << mTargetName->str() << "; // " << mKeyword->str();
  // try to find a useful type
  if ( mTarget->getType() == eNUBlock ){
    *buf << " from NUBlock at ";
    mTarget->getSourceLocation().compose(buf);
    NUScope* parent = mTarget->getParentScope();
    while ( parent && (! parent->isNamed()) && ( parent->getScopeType() != NUScope::eBlock )){
      parent = parent->getParentScope();
    }
    if ( parent ) {
      *buf << " probably from " << parent->getScopeTypeStr() << " at ";
      parent->getSourceLocation().compose(buf);
    }
  } else {
    *buf << " from " << mTarget->typeStr() << " ";
    mTarget->getSourceLocation().compose(buf);
  }
  
  indent.tabToLocColumn();
  *buf << " // ";
  mLoc.compose(buf);
  indent.newline();
}

//! Dump myself to cout
void NUBreak::print(bool, int indent_arg) const {
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "NUBreak(" << this << ") : " << mKeyword->str() << " "
               << mTargetName->str() << UtIO::endl;
}

//! Return class name
const char* NUBreak::typeStr() const {
  return "NUBreak";
}

//! calculate costs, add to NUCost arg
void NUBreak::calcCost(NUCost*, NUCostContext*) const {
}

//! Get the type of this use def
NUType NUBreak::getType() const {
  return eNUBreak;
}

