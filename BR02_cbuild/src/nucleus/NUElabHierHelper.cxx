// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUElabHierHelper.h"

#include "nucleus/NUAliasDB.h"

#include "nucleus/NUModule.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUTF.h"

#include "compiler_driver/CarbonDBWrite.h"

STBranchNode * NUElabHierHelper::findUnelabBranchForScope(NUModule * module, NUScope * scope)
{
  STBranchNode * branch = NULL;
  if (scope!=module) {
    NUTF * tf = dynamic_cast<NUTF*>(scope);
    if (tf) {
      branch = tf->getNameBranch();
    } else {
      STBranchNode * parent = findUnelabBranchForScope(module,scope->getParentScope());
      if (scope->emitInSymtab()) {
        SInt32 index = scope->getSymtabIndex();

        // A NULL parent means that this scope is top-level within the
        // module -- a name-based lookup is necessary. Use index-based
        // lookups when the parent is known.

        STSymbolTableNode * existing = NULL;
        if (parent) {
          if (parent->hasChild(index)) {
            existing = parent->getChild(index);
          }
        } else {
          existing = module->getAliasDB()->find(parent,scope->getName());
        }

        if (existing) {
          branch = existing->castBranch();
          ST_ASSERT (branch, existing);
        } else {
          branch = module->getAliasDB()->createBranch(scope->getName(),parent,index);
          if (not branch) {
            if (parent)
              ST_ASSERT(0, parent);
            else
              NU_ASSERT (0, module);
          }

          NUAliasDataBOM * bom = NUAliasBOM::castBOM(branch->getBOMData());
          bom->setScope(scope);
          bom->setIndex(index);
        }
      } else {
        branch = parent;
      }
    }
  }
  return branch;
}


const STBranchNode * NUElabHierHelper::findModuleInHier(const STBranchNode *hier)
{
  // technically hier could be null
  NUModuleElab *module_elab = NULL;
  if (hier) {
    module_elab = NUModuleElab::lookup(hier);
  }
  if (module_elab != 0) {
    return hier;
  } else {
    return findModuleInHier(hier->getParent());
  }
}


const STBranchNode * NUElabHierHelper::resolveElabHierForScope(const NUScope *scope, const STBranchNode *hier)
{
  switch (scope->getScopeType()) {
  case NUScope::eModule:
  case NUScope::eTask:
    return hier;
    break;

  case NUScope::eNamedDeclarationScope: {
    const NUScope * parent_scope = scope->getParentScope();

    // Named scopes are in the symtab, rooted off its parent.
    const STBranchNode * parent_hier = resolveElabHierForScope(parent_scope, hier);
    return scope->lookupElabScope(parent_hier)->getHier();

    break;
  }

  case NUScope::eBlock: {
    const NUScope * parent_scope = scope->getParentScope();

    // Blocks are not in the symtab; hierarchy comes from the parent.
    return resolveElabHierForScope(parent_scope, hier);

    break;
  }
  default:
    break;
  }

  INFO_ASSERT (0, "Unknown scope type enum");

  return NULL;
}


const STBranchNode * NUElabHierHelper::resolveElabHierForTF(const STBranchNode * unelab_parent, 
                                                            const STBranchNode * module_hier)
{
  const STBranchNode * elab_parent = NULL;

  if (unelab_parent) {
    const STBranchNode * elab_grandparent = resolveElabHierForTF(unelab_parent->getParent(), module_hier);
    const NUAliasDataBOM * unelab_bom = NUAliasBOM::castBOM(unelab_parent->getBOMData());
    SInt32 index = unelab_bom->getIndex();
    const STSymbolTableNode * node = elab_grandparent->getChild(index);
    elab_parent = node->castBranch();
    ST_ASSERT(elab_parent, unelab_parent);
  } else {
    elab_parent = module_hier;
    INFO_ASSERT (elab_parent, "hier-ref with no elaboration parent");
  }

  return elab_parent;
}


const STBranchNode *NUElabHierHelper::resolveElabHierForNet(const NUNet *net, const STBranchNode *hier)
{
  // Walk the provided hierarchy back until we find a containing
  // module. This handles the case where we are given hierarchy
  // referencing a block or tf.
  NUModuleElab * module_elab = NULL;
  do {
    module_elab = NUModuleElab::lookup(hier);
    if (module_elab==NULL) {
      hier = hier->getParent();
    }
  } while (module_elab == NULL);

  const NUScope * scope = net->getScope();
  return resolveElabHierForScope(scope, hier);
}


STBranchNode * NUElabHierHelper::findModuleRoot(STBranchNode * branch)
{
  INFO_ASSERT(branch, "no branch");

  NUBase * symtab_scope = CbuildSymTabBOM::getNucleusObj(branch);
  
  NUModuleElab * module_elab = dynamic_cast<NUModuleElab*>(symtab_scope);

  STBranchNode * root = NULL;
  if (module_elab) {
    root = branch;
  } else {
    root = findModuleRoot(branch->getParent());
  }
  
  ST_ASSERT(root, branch);
  return root;
}


STBranchNode * NUElabHierHelper::findRealHier(const STBranchNode * name_node, STBranchNode * root, STSymbolTable * symtab)
{
  if (not name_node) {
    return findModuleRoot(root);
  }

  NUAliasDataBOM * name_bom = NUAliasBOM::castBOM(name_node->getBOMData());
  NUScope * name_scope = name_bom->getScope();
  SInt32 symtab_index = name_bom->getIndex();
  ST_ASSERT(symtab_index!=-1, name_node);

  NUBase * symtab_scope = CbuildSymTabBOM::getNucleusObj(root);
  
  NUScopeElab * scope_elab = dynamic_cast<NUScopeElab*>(symtab_scope);
  if (scope_elab) {
    if (name_scope == scope_elab->getScope()) {
      return root;
    }
  }
  
  root = findRealHier(name_node->getParent(),root,symtab);
  STSymbolTableNode* node = symtab->find(root,name_node->strObject());
  STBranchNode * branch = NULL;
  if (node)
    branch = node->castBranch();
  if (not branch) {
    // If there is not already a branch node in the elaborated symtab, create one.
    if ((not name_scope) or name_scope->emitInSymtab()) {
      // Create a branch only if there is not one associated with the
      // branch in the alias table (a flattened scope) or if it is
      // emitInSymtab (user-named block, function, task, etc.).
      branch = symtab->createBranch(name_node->strObject(),root,symtab_index);

      NUScopeElab * name_scope_elab = NULL;
      NUModule * module = dynamic_cast<NUModule*>(name_scope);
      NUTF * tf = dynamic_cast<NUTF*>(name_scope);
      NUNamedDeclarationScope * declaration_scope = dynamic_cast<NUNamedDeclarationScope*>(name_scope);
      if (module) {
	name_scope_elab = new NUModuleElab ( module, NULL, branch );
      } else if (tf) {
	name_scope_elab = new NUTFElab ( tf, branch );
      } else if (declaration_scope) {
	name_scope_elab = new NUNamedDeclarationScopeElab ( declaration_scope, branch );
      }
      // Note that name_scope can be NULL for named declaration scopes whose
      // all instances have been flattened into parent module and it has no
      // local nets. Such empty scopes are deleted.
      CbuildSymTabBOM::putNucleusObj(branch, name_scope_elab);
    } else {
      // We don't have a precomputed branch and our scope does not
      // define a branch. Continue with our parent.
      branch = root;
    }
  }
  ST_ASSERT(branch, name_node);
  return branch;
}


STAliasedLeafNode * NUElabHierHelper::findRealLeaf(const STAliasedLeafNode * name_leaf, STBranchNode * root, STSymbolTable * symtab)
{
  STBranchNode * real_hier = NUElabHierHelper::findRealHier(name_leaf->getParent(),
                                                            root,symtab);
  STSymbolTableNode * real_node = symtab->find(real_hier, name_leaf->strObject());
  if (real_node) {
    return real_node->castLeaf();
  } else {
    return NULL;
  }
}
