// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUTaskLevels.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"

NUTaskLevels::NUTaskLevels() {
  mLevelBuckets = NULL;
  mTaskLevelMap = NULL;
}

NUTaskLevels::~NUTaskLevels() {
  delete mTaskLevelMap;
  for (UInt32 i = 0, n = mLevelBuckets->size(); i < n; ++i) {
    NUTaskList* tasks = (*mLevelBuckets)[i];
    delete tasks;
  }
  delete mLevelBuckets;
}

class NUTaskLevelsCallback : public NUDesignCallback
{
public:
  //! constructor.
  NUTaskLevelsCallback() {}

  //! destructor
  ~NUTaskLevelsCallback() {}

  Status operator()(Phase phase, NUTaskEnable* id) {
    if ((phase == ePre) && !id->isHierRef()) {
      mTaskEnables.push_back(id);
    }
    return eSkip;
  }
  Status operator()(Phase, NUModuleInstance*) {
    return eSkip;
  }
  Status operator()(Phase, NUBase*) {
    return eNormal;
  }

  NUTaskEnableVector mTaskEnables;
}; // class NUTaskLevelsCallback : public NUDesignCallback

//! Helper routine to populate the entire instance-depth bucket map
void NUTaskLevels::module(const NUModule* mod) {
  mLevelBuckets = new TaskBuckets;
  mTaskLevelMap = new TaskLevelMap;

  // The algorithm has to work bottom-up so that we see modules definitions
  // before we see module instantiations.  In particular, it should do all the
  // leaves, then all the modules that have 1 level of children, then 2 levels,
  // etc.  This is not the same as sorting by depth from the root, as a leaf
  // module may appear directly underneath the root (depth=1) and also
  // 5 levels below that.  We want to consider the bottom-up depth.

  // First, use DesignWalker to hit all the task-enables
  NUTaskLevelsCallback cb;
  NUDesignWalker walker(cb, false, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(const_cast<NUModule*>(mod));

  for (UInt32 i = 0, n = cb.mTaskEnables.size(); i < n; ++i) {
    NUTaskEnable* enable = cb.mTaskEnables[i];
    getTaskLevel(enable->getTask());
  }
}

//! how many levels of task hierarchy are there in the design?
UInt32 NUTaskLevels::numTaskLevels() const {
  return mLevelBuckets->size();
}

//! Get the height of a task in the task-hierarchy tree (distinct from depth)
UInt32 NUTaskLevels::getTaskLevel(NUTask* task) const {
  TaskLevelMap::iterator p = mTaskLevelMap->find(task);
  if (p != mTaskLevelMap->end()) {
    return p->second;
  }

  // preload the map with 0 to avoid infinite recursion.  We will
  // correct it below
  UInt32 myLevel = 0;
  (*mTaskLevelMap)[task] = myLevel;

  // First, use DesignWalker to hit all the task-enables
  NUTaskLevelsCallback cb;
  NUDesignWalker walker(cb, false, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.task(task);

  for (UInt32 i = 0, n = cb.mTaskEnables.size(); i < n; ++i) {
    NUTaskEnable* enable = cb.mTaskEnables[i];
    UInt32 childLevel = getTaskLevel(enable->getTask());
    myLevel = std::max(myLevel, childLevel + 1);
  }

  (*mTaskLevelMap)[task] = myLevel;

  while (mLevelBuckets->size() <= myLevel) {
    mLevelBuckets->push_back(new NUTaskList);
  }

  NUTaskList* bucketList = (*mLevelBuckets)[myLevel];
  bucketList->push_back(task);

  return myLevel;
} // UInt32 NUTaskLevels::getTaskLevel

//! loop over the tasks at a particular level (must be < numInstanceLevels())
NUTaskLevels::TaskLevelLoop NUTaskLevels::loopLevel(UInt32 level) const {
  INFO_ASSERT(level < numTaskLevels(), "level out of bounds");

  NUTaskList* tasks = (*mLevelBuckets)[level];

  return TaskLevelLoop(*tasks);
}
