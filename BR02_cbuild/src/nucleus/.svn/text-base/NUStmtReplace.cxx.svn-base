// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*/

#include "nucleus/NUStmtReplace.h"

NUStmtReplaceCallback::NUStmtReplaceCallback() {
  mReplacements = new NUStmtStmtMap;
}

NUStmtReplaceCallback::~NUStmtReplaceCallback() {
  delete mReplacements;
}

NUDesignCallback::Status NUStmtReplaceCallback::operator()(Phase, NUBase*) {
 return eNormal;
}

//! Do not go into submodules; just do this module.
NUDesignCallback::Status NUStmtReplaceCallback::operator()(Phase, NUModuleInstance*) {
  return eSkip;
}

//! Callback to walk task enables, return eDelete if we want to replace it.
NUDesignCallback::Status NUStmtReplaceCallback::operator()(Phase phase,
                                                           NUStmt* stmt)
{
  if (phase == ePre) {
    NUStmt* rep = replacement(stmt); // virtual is overridden in TFPromote
    if (rep != NULL) {
      return eDelete;
    }
  }
  return eNormal;
}

//! Callback to give a replacement statement for the given statement.
NUStmt* NUStmtReplaceCallback::replacement(NUStmt *stmt) {
  NUStmtStmtMap::iterator iter = mReplacements->find(stmt);
  if (iter != mReplacements->end()) {
    return (*iter).second;
  }

  return 0;
}

//! Add a replacement to the internal map
void NUStmtReplaceCallback::addReplacement(NUStmt* old, NUStmt* newStmt) {
  (*mReplacements)[old] = newStmt;
}
