// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Routines used to manipulate Nucleus structures as graphs.
*/

#include "nucleus/NucleusGraph.h"

#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphDot.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "symtab/STSymbolTable.h"

//! private helper routine that recursively extends the module graph
void buildModuleGraphRecursive(ModuleGraph* g, STSymbolTable* symtab,
                               ModuleGraph::Node* parent, STBranchNode* branch, NUModuleInstance* instance)
{
  // get the symbol table node for this module
  STSymbolTableNode* node = symtab->find(branch, instance->getName());
  ST_ASSERT(node != NULL, branch);
  STBranchNode* child = node->castBranch();
  
  // add the module to the graph, using the STBranchNode* as the node data
  ModuleGraph::Node* n = new ModuleGraph::Node(child);
  g->addNode(n);

  // if we have a parent, add an edge from it to this node
  if (parent != NULL)
    parent->addEdge(new ModuleGraph::Edge(n,NULL));

  // recurse into all sub-modules
  NUModule* module = instance->getModule();
  for (NUModuleInstanceMultiLoop l = module->loopInstances(); !l.atEnd(); ++l)
  {
    NUModuleInstance* sub = *l;
    buildModuleGraphRecursive(g, symtab, n, child, sub);
  }
}

//! Extract a graph of the elaborated module hierarchy from a design
ModuleGraph* extractModuleGraph(NUDesign* design, STSymbolTable* symtab)
{
  ModuleGraph* g = new ModuleGraph();

  // iterate over all of the top modules
  for (NUDesign::ModuleLoop l = design->loopTopLevelModules(); !l.atEnd(); ++l)
  {
    NUModule* module = *l;

    /* there is no top-level NUModuleInstance, so handle the tops here and
     * do the others recursively.
     */
    STSymbolTableNode* node = symtab->find(NULL, module->getName());
    NU_ASSERT(node != NULL, module);
    STBranchNode* top = node->castBranch();
    ModuleGraph::Node* n = new ModuleGraph::Node(top);
    g->addNode(n);

    // recurse into all sub-modules
    for (NUModuleInstanceMultiLoop l = module->loopInstances(); !l.atEnd(); ++l)
    {
      NUModuleInstance* sub = *l;
      buildModuleGraphRecursive(g, symtab, n, top, sub);
    }
  }

  return g;
}

void dumpModuleGraph(NUDesign* design, STSymbolTable* symtab,
                     const UtString& fileName, const UtString& graphName)
{
  ModuleGraph* moduleGraph = extractModuleGraph(design, symtab);
  ModuleGraphDotWriter mgdw(fileName);
  mgdw.output(moduleGraph, graphName.c_str());
  delete moduleGraph;
}

//! label each node with its scope name from the symbol table
void ModuleGraphDotWriter::writeLabel(Graph* g, GraphNode* node)
{
  ModuleGraph* mg = dynamic_cast<ModuleGraph*>(g);
  ModuleGraph::Node* n = mg->castNode(node);
  STBranchNode* branch = n->getData();
  *mOut << "[ label=\"" << branch->str() << "\" ]";
}

//! label each edge with the module name
void ModuleGraphDotWriter::writeLabel(Graph* g, GraphEdge* edge)
{
  ModuleGraph* mg = dynamic_cast<ModuleGraph*>(g);
  ModuleGraph::Node* n = mg->castNode(g->endPointOf(edge));
  STBranchNode* branch = n->getData();
  NUModule* module = NUModule::lookup(branch);
  if (module != NULL)
    *mOut << "[ label=\"" << *(module->getName()) << "\" ]";
}

