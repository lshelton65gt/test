// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUTF.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUTaskEnable.h"
#include "symtab/STSymbolTable.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "util/HierStringName.h"
#include "nucleus/NUNetSet.h"
#include "iodb/IODBNucleus.h"

#include "nucleus/NUElabHierHelper.h"
#include "nucleus/NUAliasDB.h"

/*!
  \file
  Implementation of nucleus task and function classes.
*/


NUTF::NUTF(NUModule *parent,
	   STBranchNode * name_branch,
	   NUNetRefFactory *netref_factory,
	   const SourceLocator& loc,
           IODBNucleus *iodb) :
  NUBlockingMaps(netref_factory),
  NUBlockScope(iodb, eHTTaskFunc),
  mNameBranch(name_branch),
  mLoc(loc)
{
  commonInit(parent);
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(mNameBranch->getBOMData());
  mMySymtabIndex = bom->getIndex();

  UtString composed_name;
  mNameBranch->compose(&composed_name);
  mRelativeName = mIODB->getAtomicCache()->intern(composed_name.c_str());
}


NUTF::NUTF(NUModule *parent,
	   StringAtom *name,
	   StringAtom *originalName,
	   NUNetRefFactory *netref_factory,
	   const SourceLocator& loc,
           IODBNucleus *iodb) :
  NUBlockingMaps(netref_factory),
  NUBlockScope(iodb, eHTTaskFunc),
  mLoc(loc),
  mMySymtabIndex(parent->reserveSymtabIndex())
{
  commonInit(parent);
  mNameBranch = mParent->getAliasDB()->createBranch(name,NULL,mMySymtabIndex);

  mOriginalName =  originalName;

  NUAliasDataBOM * bom = NUAliasBOM::castBOM(mNameBranch->getBOMData());
  bom->setScope(this);
  bom->setIndex(mMySymtabIndex);

  UtString composed_name;
  mNameBranch->compose(&composed_name);
  mRelativeName = mIODB->getAtomicCache()->intern(composed_name.c_str());
}

// use commonInit in favor of ctor initializer syntax
// to have one central place where new member variables get
// inititialized
void NUTF::commonInit(NUModule* parent) {
  mParent = parent;
  mHdlParent = NULL;
  mCurSymtabIndex = 0;
  mRequestInline = false;
  mRecursive = false;
}

NUTF::~NUTF()
{
  removeBody();

  while (not mDeclarationScopeList.empty()) {
    // deleting the scope removes it from the list.
    NUNamedDeclarationScope * scope = (*mDeclarationScopeList.begin());
    delete scope;
  }

  for (NUNetLoop loop = loopLocals(); not loop.atEnd(); ++loop) {
    mNetRefFactory->erase(*loop);
    delete *loop;
  }

  for (NUNetVectorLoop loop = loopArgs(); not loop.atEnd(); ++loop) {
    mNetRefFactory->erase(*loop);
    delete *loop;
  }
}

void NUTF::removeBody() {
  clearUseDef();

  for (NUStmtLoop loop = loopStmts(); not loop.atEnd(); ++loop) {
    delete *loop;
  }
  mStmts.clear();
}

SInt32 NUTF::getSymtabIndex() const 
{ 
  return mMySymtabIndex; 
}

SInt32 NUTF::reserveSymtabIndex() 
{
  return mCurSymtabIndex++; 
}

StringAtom * NUTF::getName() const
{
  return mRelativeName;
}

StringAtom* NUTF::getOriginalName() const
{
  return mOriginalName;
}

STBranchNode * NUTF::getNameBranch() const
{
  return mNameBranch;
}

void NUTF::addLocal(NUNet *net)
{
  mLocalNetList.push_back(net);
  mIODB->addTypeIntrinsic( net );
}

void NUTF::removeLocal(NUNet* net, bool remove_references)
{
  mLocalNetList.remove(net);
  if (remove_references) {
    mNetRefFactory->erase(net);
  }
}

void NUTF::removeLocals(const NUNetSet& nets, bool remove_references) {
  NUScope::removeNetsHelper(nets, remove_references, &mLocalNetList);
}

void NUTF::addBlock(NUBlock * block)
{
  mBlockList.push_back(block);
}


void NUTF::addDeclarationScope(NUNamedDeclarationScope * scope)
{
  mDeclarationScopeList.push_back(scope);
}


void NUTF::removeBlock(NUBlock * block)
{
  mBlockList.remove(block);
}


void NUTF::removeDeclarationScope(NUNamedDeclarationScope * scope)
{
  mDeclarationScopeList.remove(scope);
}


NUBlockCLoop NUTF::loopBlocks() const 
{ 
  return NUBlockCLoop(mBlockList); 
}


NUBlockLoop NUTF::loopBlocks() 
{
  return NUBlockLoop(mBlockList); 
}


NUNamedDeclarationScopeCLoop NUTF::loopDeclarationScopes() const 
{ 
  return NUNamedDeclarationScopeCLoop(mDeclarationScopeList); 
}


NUNamedDeclarationScopeLoop NUTF::loopDeclarationScopes() 
{
  return NUNamedDeclarationScopeLoop(mDeclarationScopeList); 
}


void NUTF::addArg(NUNet *net, CallByMode mode)
{
  NU_ASSERT (mArgs.size () == mArgsMode.size (), this);
  mArgs.push_back(net);
  mArgsMode.push_back (mode);
}

// addArgFirst is a more expensive version of addArg, but it puts the net/mode at the beginning of the mArgs/mArgsMode arrays,
// Use this only when necessary.
void NUTF::addArgFirst(NUNet *net, CallByMode mode)
{
  NU_ASSERT (mArgs.size () == mArgsMode.size (), this);
  mArgs.insert(mArgs.begin(), net);
  mArgsMode.insert(mArgsMode.begin(), mode);
}


int NUTF::getNumArgs() const
{
  return mArgs.size();
}


NUNet* NUTF::getArg(int idx) const
{
  NU_ASSERT((idx >= 0) && (idx < (int)mArgs.size()),this);
  return mArgs[idx];
}

NUTF::CallByMode NUTF::getArgMode (int idx) const
{
  NU_ASSERT((idx >= 0) && (idx < (int)mArgsMode.size()),this);
  return mArgsMode[idx];
}

void NUTF::putArgMode (int idx, NUTF::CallByMode mode)
{
  NU_ASSERT((idx >= 0) && (idx < (int)mArgsMode.size()),this);
  mArgsMode[idx] = mode;
}


int NUTF::getArgIndex(const NUNet *net) const
{
  NUNetVectorIter iter = std::find(mArgs.begin(), mArgs.end(), net);
  if (iter == mArgs.end()) {
    return -1;
  } else {
    return iter - mArgs.begin();
  }
}


void NUTF::replaceArg(NUNet *cur_arg, NUNet *new_arg)
{
  int arg_idx = getArgIndex(cur_arg);
  NU_ASSERT(arg_idx != -1, cur_arg);
  mArgs[arg_idx] = new_arg;
}


void
NUTF::replaceArg( NUNet* cur_arg, NUNetVector& args )
{
  NUNetVector all_args;
  UtVector<CallByMode> all_arg_modes;
  const SInt32 numArgs = getNumArgs();
  // Create a new NUNetVector and callByMode vector, expanding the replacement
  for ( SInt32 i = 0; i < numArgs; ++i )
  {
    NUNet *arg = mArgs[i];
    CallByMode arg_mode = mArgsMode[i];
    if ( arg == cur_arg )
    {
      all_args.insert( all_args.end(), args.begin(), args.end() );
      const SInt32 vecSize = args.size();
      for ( SInt32 j = 0; j < vecSize; ++j )
      {
        all_arg_modes.push_back( arg_mode );
      }
    }
    else
    {
      all_args.push_back( arg );
      all_arg_modes.push_back( arg_mode );
    }
  }

  mArgs = all_args;
  mArgsMode = all_arg_modes;
}


NUStmtList * NUTF::getStmts() const
{
  return new NUStmtList(mStmts.begin(), mStmts.end());
}

NUStmt* NUTF::getStmt() const
{
    return *(mStmts.begin());
}

void NUTF::replaceStmtList(const NUStmtList& new_stmts)
{
  mStmts.assign(new_stmts.begin(), new_stmts.end());
}


void NUTF::addStmt(NUStmt *stmt)
{
  mStmts.push_back(stmt);
}


void NUTF::addStmts(NUStmtList *stmts)
{
  mStmts.insert(mStmts.end(), stmts->begin(), stmts->end());
}


void NUTF::addStmtStart(NUStmt *stmt)
{
  mStmts.push_front(stmt);
}


void NUTF::removeStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mStmts, stmt, replacement);
}


void NUTF::getAllNonTFNets(NUNetList * /*net_list --unused*/) const
{
}


void NUTF::getAllNets(NUNetList *net_list) const
{
  net_list->insert(net_list->end(), mLocalNetList.begin(), mLocalNetList.end());
  net_list->insert(net_list->end(), mArgs.begin(), mArgs.end());

  for (NUBlockCLoop loop = loopBlocks(); not loop.atEnd(); ++loop) {
    (*loop)->getAllNets(net_list);
  }
  for (NUNamedDeclarationScopeCLoop loop = loopDeclarationScopes(); not loop.atEnd(); ++loop) {
    (*loop)->getAllNets(net_list);
  }
}


void NUTF::printContents(int indent) const
{
  for (NUNetVectorCLoop loop = loopArgs(); not loop.atEnd(); ++loop) {
    (*loop)->print(false, indent);
  }
  for (NUNetCLoop loop = loopLocals(); not loop.atEnd(); ++loop) {
    (*loop)->print(false, indent);
  }
  for (NUNamedDeclarationScopeCLoop loop = loopDeclarationScopes(); not loop.atEnd(); ++loop) {
    (*loop)->print(true, indent);
  }
  for (NUStmtCLoop loop = loopStmts(); not loop.atEnd(); ++loop) {
    (*loop)->print(true, indent);
  }
}


NUTFElab *NUTF::createElab(STBranchNode *hier, STSymbolTable *symtab)
{
  STBranchNode * elab_branch = NUElabHierHelper::findRealHier(mNameBranch, hier, symtab);
  NUBase* base = CbuildSymTabBOM::getNucleusObj(elab_branch);

  NUTFElab* tf_elab = dynamic_cast<NUTFElab*>(base);
  NU_ASSERT(tf_elab, this);

  return tf_elab;
}


NUScopeElab* NUTF::lookupElabScope(const STBranchNode *hier) const
{
  const STBranchNode * parent = mNameBranch->getParent();
  const STBranchNode * relative_hier = NUElabHierHelper::resolveElabHierForTF(parent, hier);
  ST_ASSERT(relative_hier, hier);

  return NUScope::lookupElabScope(relative_hier);
}


void NUTF::clearUseDef()
{
  mBlockNetRefDefMap.clear();
  mBlockingNetRefKillSet.clear();
}


void NUTF::fixupUseDef(NUNetList *remove_list)
{
  helperFixupUseDef(&mBlockNetRefDefMap, remove_list);
  helperFixupUseDef(&mBlockingNetRefKillSet, remove_list);
}
  
void NUTF::addNetHierRefRead( NUNet* net )
{
  mNetHierRefReads.push_back( net );
}

void NUTF::addNetHierRefWrite( NUNet* net )
{
  mNetHierRefWrites.push_back( net );
}

void NUTF::addTaskEnableHierRef( NUTaskEnable* te )
{
  mTaskEnableHierRefs.push_back( te );
}






void NUTask::addNonBlockingUse(NUNet * /* def_net -- unused */, NUNet * /* use_net -- unused */)
{
  // Obsolete
  NU_ASSERT(0=="obsolete", this);
}


void NUTask::addNonBlockingUse(const NUNetRefHdl &def_net_ref,
			       const NUNetRefHdl &use_net_ref)
{
  mNonBlockNetRefDefMap.insert(def_net_ref, use_net_ref);
}


void NUTask::addNonBlockingDef(NUNet * /* net -- unused */)
{
  // Obsolete
  NU_ASSERT(0=="obsolete", this);
}


void NUTask::addNonBlockingDef(const NUNetRefHdl &net_ref)
{
  mNonBlockNetRefDefMap.insert(net_ref);
}


void NUTask::addNonBlockingKill(NUNet * /* net -- unused */)
{
  // Obsolete
  NU_ASSERT(0=="obsolete", this);
}


void NUTask::addNonBlockingKill(const NUNetRefHdl &net_ref)
{
  mNonBlockingNetRefKillSet.insert(net_ref);
}


void NUTask::getNonBlockingKills(NUNetSet *kills) const
{
  for (NUNetRefSet::const_iterator iter = mNonBlockingNetRefKillSet.begin();
       iter != mNonBlockingNetRefKillSet.end();
       ++iter) {
    kills->insert((*iter)->getNet());
  }
}


void NUTask::getNonBlockingKills(NUNetRefSet *kills) const
{
  kills->insert(mNonBlockingNetRefKillSet.begin(), mNonBlockingNetRefKillSet.end());
}


bool NUTask::queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
				   NUNetRefCompareFunction fn,
				   NUNetRefFactory * /* factory -- unused */) const
{
  return (mNonBlockingNetRefKillSet.find(def_net_ref, fn) != mNonBlockingNetRefKillSet.end());
}


void NUTask::getNonBlockingDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getNonBlockingDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin(); iter != ref_defs.end(); ++iter) {
    defs->insert((*iter)->getNet());
  }
}


void NUTask::getNonBlockingDefs(NUNetRefSet *defs) const
{
  mNonBlockNetRefDefMap.populateMappedFrom(defs);
}


bool NUTask::queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory * /* factory -- unused */) const
{
  return mNonBlockNetRefDefMap.queryMappedFrom(def_net_ref, fn);
}


void NUTask::getNonBlockingUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getNonBlockingUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUTask::getNonBlockingUses(NUNetRefSet *uses) const
{
  mNonBlockNetRefDefMap.populateMappedToNonEmpty(uses);
}


bool NUTask::queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory * /* factory -- unused */) const
{
  return mNonBlockNetRefDefMap.queryMappedTo(use_net_ref, fn);
}


void NUTask::getNonBlockingUses(NUNet *net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getNonBlockingUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUTask::getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  mNonBlockNetRefDefMap.populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUTask::queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
				  const NUNetRefHdl &use_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory * /* factory -- unused */) const
{
  return mNonBlockNetRefDefMap.queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}


NUTask::NUTask(NUModule *parent,
	       STBranchNode * name_branch,
	       NUNetRefFactory *netref_factory,
	       const SourceLocator& loc,
               IODBNucleus *iodb) :
  NUTF(parent, name_branch, netref_factory, loc, iodb),
  mTaskEnableVector(0),
  mNonBlockingNetRefKillSet(netref_factory),
  mNonBlockNetRefDefMap(netref_factory)
{
}


NUTask::NUTask(NUModule *parent,
	       StringAtom *name,
	       StringAtom *originalName,
	       NUNetRefFactory *netref_factory,
	       const SourceLocator& loc,
               IODBNucleus *iodb) :
  NUTF(parent, name, originalName, netref_factory, loc, iodb),
  mTaskEnableVector(0),
  mNonBlockingNetRefKillSet(netref_factory),
  mNonBlockNetRefDefMap(netref_factory)
{
}


void NUTask::clearUseDef()
{
  NUTF::clearUseDef();
  mNonBlockNetRefDefMap.clear();
  mNonBlockingNetRefKillSet.clear();
}


void NUTask::fixupUseDef(NUNetList *remove_list)
{
  NUTF::fixupUseDef(remove_list);
  helperFixupUseDef(&mNonBlockNetRefDefMap, remove_list);
  helperFixupUseDef(&mNonBlockingNetRefKillSet, remove_list);
}


bool NUTask::hasHierRef() const
{
  return mTaskEnableVector ? (not mTaskEnableVector->empty()) : false;
}


void NUTask::addHierRef(NUBase *object)
{
  NUTaskEnable* enable = dynamic_cast<NUTaskEnable*>(object);
  NU_ASSERT(enable != NULL, object);
  NU_ASSERT(enable->isHierRef(), enable);

  if (not mTaskEnableVector) {
    mTaskEnableVector = new NUTaskEnableVector;
  }

  // Only add the enable if it isn't already in the vector.
  // If this becomes a performance problem, use a set.
  NUTaskEnableVector::iterator iter = std::find(mTaskEnableVector->begin(),
						mTaskEnableVector->end(),
						enable);
  if (iter == mTaskEnableVector->end()) {
    mTaskEnableVector->push_back(enable);
  }
}

void NUTask::removeHierRef(NUBase *hier_ref)
{
  NU_ASSERT(not isHierRef(), this);
  NUTaskEnable* enable_hier_ref = dynamic_cast<NUTaskEnable*>(hier_ref);
  NU_ASSERT(enable_hier_ref != NULL, this);
  NU_ASSERT(enable_hier_ref->isHierRef(), this);
  
  if (mTaskEnableVector) {
    NUTaskEnableVector::iterator iter = std::find(mTaskEnableVector->begin(),
                                                  mTaskEnableVector->end(),
                                                  enable_hier_ref);
    if (iter != mTaskEnableVector->end()) {
      mTaskEnableVector->erase(iter);
    }
  }
}

NUTask::~NUTask()
{
  if (mTaskEnableVector) {
    for (NUTaskEnableLoop loop = loopHierRefTaskEnables(); not loop.atEnd(); ++loop) {
      (*loop)->getHierRef()->removeResolution(this);
    }
    delete mTaskEnableVector;
  }
}

void NUTask::print(bool verbose_arg, int indent_arg) const
{
  bool verbose = UtIOStreamBase::limitBoolArg(verbose_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  
  UtIO::cout() << "Task(" << this << ") : " << getName()->str() << UtIO::endl;
  if (verbose) {
    printContents(numspaces+2);
  }
  if (verbose and hasHierRef()) {
    indent(numspaces);
    UtIO::cout() << "  Cross-hierarchy referencing enables:" << UtIO::endl;
    for (NUTaskEnableVector::const_iterator iter = mTaskEnableVector->begin();
	 iter != mTaskEnableVector->end();
	 ++iter) {
      (*iter)->print(false, numspaces+4);
    }
  }
}

void NUTask::compose(UtString* pbuf, const STBranchNode* scope, int numSpaces, bool) const {
  pbuf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, pbuf);
    return; 
  }
  *pbuf << "task ";
  *pbuf << getName()->str() << ";\n";
  numSpaces += 2;

  for (NUNetVectorCLoop loop = loopArgs(); not loop.atEnd(); ++loop) {
    pbuf->append(numSpaces, ' ');
    NUNet* port = *loop;
    if (port->isInput())
      *pbuf << "input ";
    else if (port->isOutput())
      *pbuf << "output ";
    else if (port->isBid())
      *pbuf << "inout ";
    port->composeDeclaration(pbuf);
    *pbuf << "\n";
  }
  for (NUNetCLoop loop = loopLocals(); not loop.atEnd(); ++loop) {
    pbuf->append(numSpaces, ' ');
    NUNet* local = *loop;
    // locals must be one of: reg, integer, real, realtime, or time
    // but we do not set the reg flag so specify type here manually.
    *pbuf << "reg ";
    if ( local->isSigned() ){
      *pbuf << "signed ";
    }
    local->composeDeclaration(pbuf);
    *pbuf << "\n";
  }
  for (NUStmtCLoop loop = loopStmts(); not loop.atEnd(); ++loop) {
//    pbuf->append(numSpaces, ' ');
    (*loop)->compose(pbuf, scope, numSpaces, true);
  }
  numSpaces -= 2;
  pbuf->append(numSpaces, ' ');
  *pbuf << "endtask\n";
}

void NUTF::compose(UtString*, const STBranchNode* /* scope */, int, bool) const {}

const char* NUTask::typeStr() const
{
  return "NUTask";
}

bool NUTask::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  for (NUStmtLoop loop = loopStmts(); not loop.atEnd(); ++loop) {
    NUStmt* stmt = (*loop);
    changed |= stmt->replaceLeaves(translator);
  }

  return changed;
}

void NUTask::putRecursive(bool flag) {
  mRecursive = flag;
}

void NUTask::putRequestInline(bool flag) {
  mRequestInline = flag;
}

/*! Only clone locals which have the storage.
 * Nets which do not have the storage have been optimized
 * away in the design, and will be cloned as part of their
 * alias ring.
 */
static bool sNetDoClone(NUNet *net)
{
  return (net->getStorage() == net);
}

void NUTF::cloneScopeLocals(const NUScope * original, 
                            NUScope * target,
                            CopyContext & copy_context)
{
  NU_ASSERT(original->inTFHier(), target->getModule ());

  for ( NUNetCLoop loop = original->loopLocals() ;
	!loop.atEnd() ;
	++loop ) {
    NUNet * net = (*loop);

    if (not sNetDoClone(net)) {
      continue;
    }

    // If we have a temp name (noted by '$', make sure our gensym name
    // database knows about this particular name. Otherwise, we may
    // attempt to generate duplicate temps.

    // An alternative technique would be to copy the gensym name
    // database from original to target task.

    StringAtom * original_name = net->getName();
    const char * original_cname = original_name->str();
    StringAtom * cloned_name = original_name;
    if (original_cname[0] == '$') {
      cloned_name = target->gensym(original_cname);
    }
    NUNet * local = net->clone(target,copy_context.mNetReplacements,
                               cloned_name, net->getFlags());
    target->addLocal ( local );

    copy_context.mNetReplacements[ net ] = local;
  }
} // void NUTF::cloneScopeLocals

void NUTF::cloneDeclarationScope(NUNamedDeclarationScope * original,
                                 NUScope * parent,
                                 CopyContext & copy_context)
{
  NU_ASSERT(original->inTFHier(), original->getModule ());

  NUNamedDeclarationScope * target =
    new NUNamedDeclarationScope(original->getName(), parent,
                                parent->getIODB(),
                                original->getNetRefFactory(),
                                original->getLoc());

  // clone my local nets
  cloneScopeLocals(original, target, copy_context);

  // clone any nets in nested declaration scopes.
  for (NUNamedDeclarationScopeLoop loop = original->loopDeclarationScopes();
       not loop.atEnd();
       ++loop)
  {
    NUNamedDeclarationScope * declaration_scope = (*loop);
    cloneDeclarationScope(declaration_scope, target, copy_context);
  }
}

void NUTF::clone ( NUTF * target,
                   const NUTF& original,
                   CopyContext & copy_context,
                   bool xformNonLocals)
{
  CopyContext local_copy_context(target, copy_context.mStrCache,
				 copy_context.mCopyScopeVariables,
                                 copy_context.mPrefix);

  // During flattening, we do not want to use the existing net replacements
  // when copying the task body.  I don't know why that is.  But in
  // TFPromote, need them.
  if (xformNonLocals) {
    local_copy_context.mNetReplacements.insert(copy_context.mNetReplacements.begin(),
                                               copy_context.mNetReplacements.end());
  }

  // add arguments
  for ( int i = 0 ; i < original.getNumArgs() ; ++i ) {
    NUNet * net = original.getArg(i);
    NUNet * local = net->clone(target,local_copy_context.mNetReplacements,
                               net->getName(), net->getFlags());
    target->addArg ( local, original.getArgMode (i) );

    local_copy_context.mNetReplacements[ net ] = local;
  }

  // add local nets.
  cloneScopeLocals(&original, target, local_copy_context);

  // add any nets declared in verilog named blocks.
  for (NUNamedDeclarationScopeCLoop loop = original.loopDeclarationScopes();
       not loop.atEnd();
       ++loop) {
    NUNamedDeclarationScope * declaration_scope = (*loop);
    cloneDeclarationScope(declaration_scope, target, local_copy_context);
  }

  // add stmts
  for ( NUStmtCLoop loop = original.loopStmts() ;
	!loop.atEnd() ;
	++loop ) {
    NUStmt * stmt = (*loop);
    target->addStmt( stmt->copy(local_copy_context) );
  }

  // save our variable rewrites for our walk
  copy_context.mNetReplacements.insert(local_copy_context.mNetReplacements.begin(),
				       local_copy_context.mNetReplacements.end());

  target->mRequestInline = original.mRequestInline;
  target->mRecursive = original.mRecursive;
} // void NUTF::clone


void
NUTF::setHdlParentScope( NUScope *scope )
{
  mHdlParent = scope;
}






NUTFElab::NUTFElab(NUTF *tf, STBranchNode *hier) :
  NUScopeElab(tf, hier)
{
}

NUTFElab* NUTFElab::lookup(STBranchNode *hier)
{
  NUTFElab *elab = dynamic_cast<NUTFElab*>(lookupScope(hier));
  ST_ASSERT(elab, hier);
  return elab;
}


NUTF* NUTFElab::getTF() const
{
  NUTF* tf = dynamic_cast<NUTF*>(mScope);
  NU_ASSERT(tf, this);
  return tf;
}


//! Realise NUScopeElab::getSourceLocation virtual method
/*virtual*/ const SourceLocator NUTFElab::getSourceLocation () const
{
  return getTF ()->getLoc ();
}


//! Realise NUElabBase::findUnelaborated virtual method
/*virtual*/ bool NUTFElab::findUnelaborated (STSymbolTable *unelabStab, 
  STSymbolTableNode **unelab) const
{
  // find the unelaborated symbol table entry for the enclosing scope
  STBranchNode *elabParent = getHier ()->getParent ();
  STSymbolTableNode *unelabParent;
  INFO_ASSERT (elabParent != NULL, "bogus symbol table entry");
  NUBase *base;
  NUElabBase *elabBase;
  if ((base = CbuildSymTabBOM::getNucleusObj (elabParent)) == NULL) {
    // no object
    NU_ASSERT (base != NULL, this);
    return false;
  } else if ((elabBase = dynamic_cast <NUElabBase *> (base)) == NULL) {
    // not an elaboration object
    NU_ASSERT (elabBase != NULL, this);
    return false;
  } else if (!elabBase->findUnelaborated (unelabStab, &unelabParent)) {
    // Did not find the unelaborated symbol for the parent
    return false;
  } else {
    // unelabParent contains the unelaborated parent for the unelaborated scope
  }
  // now form the unelaborated name from the unelaborated parent symbol and the
  // name of this
  HierStringName tmp (getTF ()->getName ());
  tmp.putParent (getHier ()->getParent ());
  *unelab = unelabStab->safeLookup (&tmp);
  return (*unelab != NULL);
}


void NUTFElab::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);

  UtString buf;
  mSymNode->compose(&buf);
  UtIO::cout() << "TFElab(" << this << ") :  TF(" << mScope
       << ")  BranchNode(" << mSymNode << ")" << UtIO::endl;
  if (recurse) {
    getTF()->print(recurse, numspaces+2);
  }
}


void NUTFElab::printElab(STSymbolTable *symtab, bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  UtIO::cout() << "TFElab(" << this << ") :  TF(" << mScope
       << ")  BranchNode(" << mSymNode << ")" << UtIO::endl;
  if (recurse) {
    for (NUNetLoop iter = getTF()->loopLocals();
         !iter.atEnd();
         ++iter) {
      (*iter)->lookupElab(getHier())->printElab(symtab, true, numspaces+2);
    }
  }
}


NUTFElab::~NUTFElab()
{
}


const char* NUTFElab::typeStr() const
{
  return "TFElab";
}


/*
 * Review comments for recursive tasks:
 *
 *   Cloutier:
 *     9. testing:
 *     do we have tests that show messages for every condition where we should inline
 *     but cannot? the diff file does not contain either of these messages
 *     +          mMsgContext->TaskNonBlockingHierRefs(tf);
 *     +          mMsgContext->TaskNonBlockingRecursive(tf);
 *
 *     10. When I was working on this I found that 
 *     putWalkTasksFromTaskEnables was only needed to be set to true for
 *     MarkSweepUnusedTasks::mark for all of our testcases all other walkers could
 *     avoid walking through task enables.  This does not mean that walking through
 *     task enables is not needed for anything else, only that we do not have
 *     testcases that require it.  Part of this project should be to change the
 *     default action for NUDesignWalker to default to false for
 *     putWalkTasksFromTaskEnables, and only enable it where required.
 *
 *     I assume that you will get back to the UD question after this gets checked in,
 *     it seems like more complex testcases are in order to test this.
 *
 *   Aron:
 *     From: Aron Atkins <aron@carbondesignsystems.com>
 *     To: "access.carbondesignsystems.com" <josh@carbondesignsystems.com>
 *     CC:  cloutier@carbondesignsystems.com, 
 *      'Joe Tatham' <joe@carbondesignsystems.com>
 *     Subject: Re: recursive function support review
 *     Date: Tue, 08 Nov 2005 14:40:12 -0500
 *
 *     access.carbondesignsystems.com wrote:

 *     > I discussed this with Jeff and I think we could do this
 *     > iteratively, or we could build an Graph based on the
 *     > elaborated task information collected by TFPromote (moving
 *     > that capability into a more general purpose file) compute
 *     > SCCs and then propagate the union of all the UD across each
 *     > SCC.
 *
 *     By iteration, I did not mean over the entire design, or even a given 
 *     module. We need to iterate over the cyclic call chain until the use/def 
 *     information settles. This sounds like we're saying the same thing. I'm 
 *     not convinced that two iterations is sufficient.
 *
 *     > I don't have a design note on the walkers, and only know that the walkers
 *     > that I've hit are safe.  But NUDesignWalker and everyone using it (and not
 *     > explicitly recursing on their own) is now safe.
 *
 *     I asked about a design note on recursive task/function support, not 
 *     about the walkers.
 *
 *     By "safe", you mean not crashing, yes? That's nice. ;) I'm worried about 
 *     how we determine that our analyses are correct in the presence of 
 *     recursive calls.
 *
 */
     
