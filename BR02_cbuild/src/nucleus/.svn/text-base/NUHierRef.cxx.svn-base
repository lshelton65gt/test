// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUHierRef.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUAliasDB.h"

#include "util/UtIOStream.h"

#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"
#include "iodb/IODBNucleus.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTable.h"
#include "compiler_driver/CarbonDBWrite.h"


#ifdef CDB
// Debug hooks to facilitate setting breakpoints in constructors & destructors,
// which otherwise is hard in gcc 3.*

void NUHierRef::ctor() {
}
void NUHierRef::dtor() {
}
#endif

NUHierRef::NUHierRef(const AtomArray& path)
  : mPath(path)
{
#ifdef CDB
  ctor();
#endif
}


NUHierRef::~NUHierRef()
{
#ifdef CDB
  dtor();
#endif
}

bool NUHierRef::isLocallyRelative(NUModuleInstanceVector* instPath,
                                  STSymbolTableNode** node) const
{
  const NUModule* mod = getModule();
  bool isLocal = isLocallyRelativeToModule(mod, instPath, node);
  return isLocal;
}


bool NUHierRef::isLocallyRelativeToModule(const NUModule * mod, 
                                          NUModuleInstanceVector * instPath,
                                          STSymbolTableNode ** node) const
{
  NUModuleInstanceVector lclInstPath;
  STSymbolTableNode * resolved_node;
  resolved_node = mod->resolveHierRefRecurse(mPath, 0, &lclInstPath);
  bool isLocal = (resolved_node != 0);

  // If it is local, possibly return the instance information to our caller.
  if (isLocal) {
    // Return information to our caller
    if (instPath != NULL) {
      *instPath = lclInstPath;
    }
    if (node) {
      *node = resolved_node;
    }
  }
  return isLocal;
}


static STSymbolTableNode * lookupNode(STSymbolTable * symtab,
                                      STBranchNode  * lookup_hier,
                                      const AtomArray& path,
                                      UInt32 pathIndex)
{
  if (pathIndex == path.size()) {
    return NULL;
  }

  STSymbolTableNode * node = lookup_hier;
  do {
    // lookup up that part based at our known root and continue search.
    StringAtom * atom = path[pathIndex];
    ++pathIndex;
    STSymbolTableNode * new_node = symtab->find(node, atom);
    // If we were able to find an entry, assert that it is not the
    // same as our current parent.
    ST_ASSERT((new_node==NULL) or (new_node != node), node);
    node = new_node;
  } while ((pathIndex < path.size()) && (node != NULL));

  return node;
} // static STSymbolTableNode * lookupNode

STSymbolTableNode*
NUHierRef::resolveHierRef(const AtomArray& path,
                          UInt32 pathIndex,
                          STSymbolTable* symtab,
                          STBranchNode* startNode,
                          const NUModule *given_module,
                          bool isParseTable)
{
  STSymbolTableNode *resolved_node = 0;

  // If a module was given, then we are doing unelaborated lookups.  Make
  // sure we are at the root of the symtab; the following iterative
  // lookup algorithm assumes it is the root.
  if (given_module) {
    NU_ASSERT(not startNode, given_module);
  }

  // Extract out the first part of the path, for lookup below.
  NU_ASSERT(path.size() > pathIndex, given_module);
  StringAtom* first_part = path[pathIndex];

  // Iterative lookup algorithm is:
  //  1. First try to match to a child
  //  2. If that fails, try to match first part of the path to the current
  //     module's name and try to match the rest of the path to a child
  //  3. If that fails, go up one level and repeat
  for (STBranchNode *lookup_hier = startNode;
       not resolved_node;
       lookup_hier = lookup_hier->getParent()) {

    // 1. Child match
    resolved_node = lookupNode(symtab, lookup_hier, path, pathIndex);

    // 2. Module name match.
    if (not resolved_node) {
      // If a module was given, use that as the module (used for unelaborated
      // lookups, because we do not put the unelaborated module in the symtab).
      const NUModule *cur_module = 0;
      if (given_module) {
        cur_module = given_module;
      } else if (lookup_hier) {
        // Elaborated lookups, get the module from the current symtab node.
        if (isParseTable) {
          NUBase* base = IODBNucleus::getNucleusObject(lookup_hier);
          cur_module = dynamic_cast<NUModule*>(base);
        }
        else {
          cur_module = NUModule::lookup(lookup_hier, true);
        }
        NU_ASSERT(cur_module, given_module);
      }

      // For pre-elaboration lookups, must first check local instantiations.
      bool can_module_match = true;
      if (given_module) {
        // Make sure there are no module instances with the same name.
        // (Children take precedence over parents for lookup).
        for (NUModuleInstanceMultiCLoop loop = cur_module->loopInstances(); not loop.atEnd(); ++loop) {
          NUModuleInstance *inst = *loop;
          if (first_part == inst->getName()) {
            can_module_match = false;
            break;
          }
        }
      }

      // If none of the local instantiations matched, or we are working elaboratedly,
      // then check the module name for a match.
      if (can_module_match and cur_module) {
        if (first_part == cur_module->getName()) {
          resolved_node = lookupNode(symtab, lookup_hier, path, pathIndex + 1);
        }
      }
    }

    // When lookup_hier is 0, then that is the top of the tree.  Stop after
    // trying to resolve from the top.
    if (not lookup_hier) {
      break;
    }
  }

  return resolved_node;
}


NUTaskHierRef::NUTaskHierRef(NUTaskEnable *enable,
                             const AtomArray& path,
                             StringAtom * /*taskName */) : // taskName is in path
  NUHierRef(path),
  mTaskEnable(enable)
{
}


NUTaskHierRef::~NUTaskHierRef()
{
  cleanupResolutions();
}


bool NUTaskHierRef::isLocallyRelative(NUModuleInstanceVector* instPath,
                                      NUTask **resolution) const
{
  STSymbolTableNode *node = 0;
  bool is_local = NUHierRef::isLocallyRelative(instPath, &node);
  if (is_local and (resolution != NULL)) {
    STBranchNode* branch = node->castBranch();
    NU_ASSERT(branch, this);
    NUAliasDataBOM * bom = NUAliasBOM::castBOM(branch->getBOMData());
    *resolution = dynamic_cast<NUTask*>(bom->getScope());
    NU_ASSERT(*resolution, this);
  }
  return is_local;
}


bool 
NUHierRef::fixupResolution(NUBase *old_resolution,
                           NUModuleInstance *old_instance)
{
  bool use_new_resolution = false;

  NUModuleInstanceVector inst_path;
  STSymbolTableNode *resolved_node = 0;
  if (NUHierRef::isLocallyRelative(&inst_path, &resolved_node)) {

    // First check if the old_instance is the root of the resolved node.
    // (This can happen through flattening when the parent instance goes away.)
    bool found = false;
    STSymbolTableNode * resolved_root = resolved_node->getRoot();
    if (resolved_root) {
      found = (strcmp(old_instance->getName()->str(), resolved_root->str()) == 0);
    }

    // If that fails, check that the old_instance appears on the instance path.
    for (NUModuleInstanceVector::iterator iter = inst_path.begin();
         (iter != inst_path.end()) and not found;
         ++iter) {
      if (*iter == old_instance) {
        found = true;
      }
    }
    if (found) {
      // the new resolution will supercede.
      removeResolution(old_resolution);
      old_resolution->removeHierRef(getObject());
      use_new_resolution = true;
    }
  } else {
    use_new_resolution = true;
  }

  return use_new_resolution;
}


void NUHierRef::addResolution(NUBase *obj)
{
  NU_ASSERT(not obj->isHierRef(), this);

  // Just make each possible resolution appear once.
  //
  // The number of hierrefs are usually small, but if this becomes a
  // performance problem (n-squared), use a set.
  NUBaseVector::iterator iter = std::find(mResolutionVector.begin(), mResolutionVector.end(), obj);
  if (iter == mResolutionVector.end()) {
    mResolutionVector.push_back(obj);
  }
}


void NUHierRef::removeResolution(NUBase *obj)
{
  NUBaseVector::iterator iter = std::find(mResolutionVector.begin(), mResolutionVector.end(), obj);
  if (iter != mResolutionVector.end()) {
    mResolutionVector.erase(iter);
  }
}

void NUHierRef::replaceResolutions(const NUBaseVector& newResolutions)
{
  NU_ASSERT(!newResolutions.empty(), this);
  mResolutionVector.assign(newResolutions.begin(), newResolutions.end());
}


void NUHierRef::cleanupResolutions()
{
  NUBase* hier_obj = getObject();
  for (NUBaseVector::iterator iter = mResolutionVector.begin();
       iter != mResolutionVector.end();
       ++iter) {
    NUBase* resolved_obj = *iter;
    resolved_obj->removeHierRef(hier_obj);
  }
  mResolutionVector.clear();
}


NUBaseVectorLoop NUHierRef::loopResolutions()
{
  return NUBaseVectorLoop(mResolutionVector);
}


NUBaseVectorCLoop NUHierRef::loopResolutions() const
{
  return NUBaseVectorCLoop(mResolutionVector);
}

int NUHierRef::resolutionCount() const
{
  return mResolutionVector.size();
}

void NUHierRef::printPossibleResolutions(int indent) const
{
  for (int i = 0; i < indent; i++) {
    UtIO::cout() << " ";
  }

  UtIO::cout() << "  Possible resolutions:" << UtIO::endl;

  for (NUBaseVector::const_iterator iter = mResolutionVector.begin();
       iter != mResolutionVector.end();
       ++iter) {
    (*iter)->print(false, indent+4);
  }
}


NUNet *NUNetHierRef::getLocallyResolvedNet() const
{
  NUNet *resolved_net = 0;
  isLocallyRelative(0, &resolved_net);
  return resolved_net;
}


bool NUHierRef::sameResolutionAsChild(const NUHierRef *child,
                                      NUModuleInstance *inst,
                                      bool do_consistency_check) const
{
  bool same = false;

  // Three cases to consider:
  // 1. The child hierref is locally relative to its parent module.
  // 2. The child hierref is locally relative to this hierref's parent module.
  // 3. Catch-all (same hierref path).

  // NOTE: The matching cases defined here must align with the
  // localization cases defined in NUHierRef::localizeNameForParent.
  // Failure to keep the two consistent will end up in incorrect
  // hierref naming and/or missed resolutions.

  if (child->isLocallyRelative()) {
    // In this case, the first piece of path on this hierref should
    // match the instance name.  And, the rest of this hierref should
    // match the child hierref.
    if (!mPath.empty() && (mPath[0] == inst->getName()) &&
        (mPath.size() == child->mPath.size() + 1))
    {
      same = true;
      for (UInt32 i = 1; same && (i < mPath.size()); ++i) {
        same = (mPath[i] == child->mPath[i - 1]);
      }
    }
  } else {
    const NUModule * parent_module = getModule();
    NUModuleInstanceVector instPath;
    STSymbolTableNode * child_node = NULL;
    
    if (child->isLocallyRelativeToModule(parent_module, &instPath, &child_node)) {
      // In this case, the child hierref is locally relative to the
      // parent module of this hierref. Consider the two hierrefs the
      // same if they have the same locally resolved names.
      NU_ASSERT(child_node, this);

      same = true;
      UInt32 pathIndex = 0;
      for (NUModuleInstanceVector::iterator iter = instPath.begin();
           same && (iter != instPath.end());
           ++iter, ++pathIndex)
      {
        if (pathIndex >= mPath.size() - 1) {
          same = false;
        }
        NUModuleInstance * instance = (*iter);
        same = mPath[pathIndex] == instance->getName();
      }

      if (same) {
	// Make sure the rest of the hierarchical reference matches.
	UtString leaf_name;
	if (child_node->getParent()) {
	  // The child_node contains additional hierarchical information.
	  // Extract it and make sure that it matches before calling
	  // these two hierarchical references equal.
	  UtList<StringAtom*> hierList;
	  STSymbolTableNode *curr_node = child_node->getParent();
	  leaf_name = child_node->str();

	  while (curr_node) {
	    hierList.push_front(curr_node->strObject());
	    curr_node = curr_node->getParent();
	  }

	  for (UtList<StringAtom*>::iterator iter = hierList.begin();
	       same && iter != hierList.end() && pathIndex < mPath.size();
	       ++iter, ++pathIndex) {
	    StringAtom* curr = *iter;
	    same = mPath[pathIndex] == curr;
	  }
	} 
	else {
          child_node->compose(&leaf_name);
	}
      
	if (same) {
	  if (pathIndex != mPath.size() - 1) {
	    // The paths have different sizes. There is no way they can
	    // be referencing the same net.
	    same = false;
	  }
	  else {
	    // Check the leaf. It should match.  
	    same = strcmp(mPath[pathIndex]->str(), leaf_name.c_str()) == 0;
	  }
	}
      }
    } else {
      // In this case, the hierrefs should just be exactly the same.
      same = mPath == child->mPath;
    }
  }

  // Do a sanity check.
  if (same and do_consistency_check) {

    NUBaseSet child_resolutions;
    for (NUBaseVectorCLoop loop = child->loopResolutions(); not loop.atEnd(); ++loop) {
      child_resolutions.insert(*loop);
    }

    bool consistent_resolutions = true;
    if (isLocallyRelative() and not child->isLocallyRelative()) {
      // If this hier-ref is locally resolved, then its resolutions must
      // be a subset of the resolutions for the child.
      for (NUBaseVectorCLoop loop = loopResolutions(); not loop.atEnd(); ++loop) {
        NUBase * resolution = (*loop);
        if (child_resolutions.find(resolution) == child_resolutions.end()) {
          consistent_resolutions = false;
        }
      }
    } else {
      // If this hier-ref is not locally resolved, it must have exactly
      // the same resolutions as the child.
      NUBaseSet my_resolutions;
      for (NUBaseVectorCLoop loop = loopResolutions(); not loop.atEnd(); ++loop) {
        my_resolutions.insert(*loop);
      }
      consistent_resolutions = (my_resolutions == child_resolutions);
    }

    NU_ASSERT2(consistent_resolutions, getObject(), child->getObject());
  }

  return same;
}



NUTask * NUTaskHierRef::CoerceBaseToTask::operator()(NUBase * node) const
{ 
  NUTask * coerced = dynamic_cast<NUTask*>(node); 
  NU_ASSERT(coerced, node);
  return coerced;
}


const NUTask * NUTaskHierRef::CoerceBaseToTask::operator()(const NUBase * node) const
{ 
  const NUTask * coerced = dynamic_cast<const NUTask*>(node); 
  NU_ASSERT(coerced, node);
  return coerced;
}


NUTaskHierRef::TaskVectorLoop NUTaskHierRef::loopResolutions()
{
  return TaskVectorLoop(NUHierRef::loopResolutions());
}


NUTaskHierRef::TaskVectorCLoop NUTaskHierRef::loopResolutions() const
{
  return TaskVectorCLoop(NUHierRef::loopResolutions());
}

const NUModule* NUTaskHierRef::getModule() const
{
  NUTaskEnableHierRef* task_enable;
  task_enable = dynamic_cast<NUTaskEnableHierRef*>(mTaskEnable);
  return task_enable->getModule();
}


NUBase* NUTaskHierRef::getObject() const
{
  return mTaskEnable;
}

void NUHierRef::getName(UtString* name, HdlHierPath& hdl) const {
  UtStringArray tmp;
  for (UInt32 i = 0; i < mPath.size(); ++i) {
    tmp.push_back(mPath[i]->str());
  }
  UtString buf;
  (void) hdl.compPath(tmp, &buf);
  *name << buf;
}

NUTask * NUTaskHierRef::getRepresentativeResolution() const
{
  NUTask * resolution = NULL;
  TaskVectorCLoop loop = loopResolutions();
  NU_ASSERT(not loop.atEnd(), this); // ensure at least one resolution.
  resolution = (*loop);
  NU_ASSERT(resolution, this); // ensure at least one resolution.
  return resolution;
}


NUNetHierRef::~NUNetHierRef()
{
  cleanupResolutions();
}


NUNet * NUNetHierRef::CoerceBaseToNet::operator()(NUBase * node) const
{ 
  NUNet * coerced = dynamic_cast<NUNet*>(node); 
  NU_ASSERT(coerced, node);
  return coerced;
}


const NUNet * NUNetHierRef::CoerceBaseToNet::operator()(const NUBase * node) const
{ 
  const NUNet * coerced = dynamic_cast<const NUNet*>(node); 
  NU_ASSERT(coerced, node);
  return coerced;
}


NUNetHierRef::NetVectorLoop NUNetHierRef::loopResolutions()
{
  return NetVectorLoop(NUHierRef::loopResolutions());
}


NUNetHierRef::NetVectorCLoop NUNetHierRef::loopResolutions() const
{
  return NetVectorCLoop(NUHierRef::loopResolutions());
}


bool NUNetHierRef::isLocallyRelative(NUModuleInstanceVector* instPath,
                                     NUNet **resolution) const
{
  STSymbolTableNode *node = 0;
  bool is_local = NUHierRef::isLocallyRelative(instPath, &node);
  if (is_local and (resolution != NULL)) {
    *resolution = NUNet::findStorageUnelab(node);
    NU_ASSERT(*resolution, this);
  }
  return is_local;
}


void NUHierRef::localizeNameForParent(NUScope* scope,
                                      StringAtom* inst_name,
                                      AtomArray* localized_name) const
{
  // Three cases to consider:
  // 1. The hierref is locally relative to its parent module.
  // 2. The hierref is locally relative to the proposed parent module (post-flattening).
  // 3. Catch-all (non-local hierref path).

  // NOTE: The localization cases defined here must align with the
  // matching cases defined in NUHierRef::sameResolutionAsChild.
  // Failure to keep the two consistent will end up in incorrect
  // hierref naming and/or missed resolutions.

  NUModuleInstanceVector inst_path;
  STSymbolTableNode *resolved_node = 0;

  if (NUHierRef::isLocallyRelative(&inst_path, &resolved_node)) {
    // If this hierref is locally relative, construct the name from the
    // instance path and the resolved node name.
    //
    // NOTE: we do not simply append getName() onto inst_name, because
    // getName() may have module names in it (which are not part of
    // an instance path).  The instance vector takes care of this for us.
    // Also, notice that we use the resolved node, instead of the resolved object.
    // This is because the resolved object may be some weird name due to flattening
    // and aliasing.  The symbol table node name is correct, however.
    if (inst_name) {
      localized_name->push_back(inst_name);
    }
    for (NUModuleInstanceVector::iterator iter = inst_path.begin();
         iter != inst_path.end();
         ++iter) {
      NUModuleInstance * instance = (*iter);
      localized_name->push_back(instance->getName());
    }
    NUHierRef::buildAtomArray(localized_name, resolved_node);
  } else {
    const NUModule * current_parent = getModule();
    const NUModule * proposed_parent = scope->getModule();
    bool localizing_for_different_parent = (current_parent != proposed_parent);
    if (localizing_for_different_parent and 
        NUHierRef::isLocallyRelativeToModule(proposed_parent, &inst_path, &resolved_node)) {

      for (NUModuleInstanceVector::iterator iter = inst_path.begin();
           iter != inst_path.end();
           ++iter) {
        NUModuleInstance * instance = (*iter);
        localized_name->push_back(instance->getName());
      }
      buildAtomArray(localized_name, resolved_node);
    } else {
      // Non-locally-relative paths are already parent localized.
      *localized_name = mPath;
    }
  }
} // void NUHierRef::localizeNameForParent

const NUModule* NUNetHierRef::getModule() const
{
  return mNet->getScope()->getModule();
}

NUBase* NUNetHierRef::getObject() const
{
  return mNet;
}

//! Print info when an NU_ASSERT occurs
void NUNetHierRef::printAssertInfo() const {
  UtIO::cerr() << "NUNetHierRef (0x" << this << "\n";
  printPossibleResolutions(2);
  mNet->print(false, 2);
}

void NUTaskHierRef::printAssertInfo() const {
  HdlVerilogPath path;
  UtString pathbuf;
  getName(&pathbuf, path);
  UtIO::cerr() << "NUTaskHierRef (0x" << this << ") path=" << pathbuf << "\n";
  printPossibleResolutions(2);
  mTaskEnable->print(false, 2);
}

StringAtom* NUHierRef::gensym(NUScope* scope, const AtomArray& path)
{
  // Do not use NUScope::gensym because we don't need or want to
  // uniquify -- the same path should result in the same symbol
  HdlVerilogPath hdl;
  UtString buf, buf2;
  HdlId info;

  for (UInt32 i = 0, n = path.size(); i < n; ++i) {
    StringAtom* name = path[i];
    hdl.compPathAppend(&buf, name->str(), &info);
  }
  buf2 << "$href_" << buf << ";";

  IODBNucleus* iodb = scope->getIODB();
  AtomicCache* cache = iodb->getAtomicCache();
  return cache->intern(buf2);
}  

void NUHierRef::buildAtomArray(AtomArray* atomArray,
                               STSymbolTableNode* node)
{
  if (node != NULL) {
    buildAtomArray(atomArray, node->getParent());
    atomArray->push_back(node->strObject());
  }
}

//! Mutate existing hier-ref to have alternate path
void NUHierRef::putPath(const AtomArray& path) {
  mPath = path;
}

void NUHierRef::print() const {
  HdlVerilogPath hdl;
  UtString pathbuf;
  getName(&pathbuf, hdl);
  UtIO::cout() << typeStr() << ": " << pathbuf << UtIO::endl;
}

const char* NUTaskHierRef::typeStr() const {
  return "NUTaskHierRef";
}

const char* NUNetHierRef::typeStr() const {
  return "NUNetHierRef";
}

void NUHierRef::composeCPath(UtString* buf) const {
  *buf << mPath[0]->str();
  for (UInt32 i = 1, n = mPath.size(); i < n; ++i) {
    *buf << "$" << mPath[i]->str();
  }
}
