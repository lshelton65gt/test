// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/




/*!
  \file
  Implementation of the Nucleus structured procedure (always, initial, ...)
*/


#include "util/StringAtom.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUBlock.h"
#include "util/CarbonAssert.h"
#include "util/CarbonTypes.h"
#include "util/UtIOStream.h"
#include "util/UtIndent.h"

#include "nucleus/NUNetSet.h"
#include "nucleus/NUNetRefMap.h"


NUStructuredProc::NUStructuredProc(StringAtom * name,
                                   NUBlock *block, 
				   NUNetRefFactory *netref_factory,
                                   const SourceLocator& loc) :
  mName(name),
  mBlock(block),
  mLoc(loc),
  mNetRefFactory(netref_factory),
  mNetRefDefMap(new NUNetRefNetRefMultiMap(netref_factory))
{
}


NUStructuredProc::~NUStructuredProc()
{
  clearUseDef();
  delete mBlock;
  delete mNetRefDefMap;
}


StringAtom* NUStructuredProc::getName() const
{
  return mName;
}


const SourceLocator& NUStructuredProc::getLoc() const
{
  return mLoc;
}


void NUStructuredProc::clearUseDef()
{
  mNetRefDefMap->clear();
}


void NUStructuredProc::fixupUseDef(NUNetList *remove_list)
{
  helperFixupUseDef(mNetRefDefMap, remove_list);
}


#ifdef CDB
static NUStructuredProc* sCheckStructuredProc = NULL;
static NUNet* sCheckStructuredProcNet = NULL;

static void sCheck(NUStructuredProc* proc, const NUNetRefHdl& net_ref) {
  if (((proc == sCheckStructuredProc) || (sCheckStructuredProc == NULL)) &&
      (net_ref->getNet() == sCheckStructuredProcNet))
  {
    UtIO::cout() << "stop here\n";
  }
}
#define PROC_CHECK(net_ref) sCheck(this, net_ref)
#else
#define PROC_CHECK(net_ref) 
#endif

void NUStructuredProc::addDef(const NUNetRefHdl &def_net_ref)
{
  PROC_CHECK(def_net_ref);
  mNetRefDefMap->insert(def_net_ref);
}


void NUStructuredProc::addUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref)
{
  PROC_CHECK(def_net_ref);
  mNetRefDefMap->insert(def_net_ref, use_net_ref);
}


void NUStructuredProc::addUses(const NUNetRefHdl &def_net_ref, NUNetRefSet *use_set)
{
  PROC_CHECK(def_net_ref);
  for (NUNetRefSet::iterator iter = use_set->begin(); iter != use_set->end(); ++iter) {
    mNetRefDefMap->insert(def_net_ref, *iter);
  }
}


void NUStructuredProc::getUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUStructuredProc::getUses(&ref_uses); // hack due to UnelabFlow::collect hack
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUStructuredProc::getUses(NUNetRefSet *uses) const
{
  mNetRefDefMap->populateMappedToNonEmpty(uses);
}


bool NUStructuredProc::queryUses(const NUNetRefHdl &use_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory * /* factory -- unused */) const
{
  return mNetRefDefMap->queryMappedTo(use_net_ref, fn);
}


void NUStructuredProc::getUses(NUNet *net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  NUStructuredProc::getUses(net_ref, &ref_uses); // hack due to UnelabFlow::collect hack

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUStructuredProc::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  mNetRefDefMap->populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUStructuredProc::queryUses(const NUNetRefHdl &def_net_ref,
				 const NUNetRefHdl &use_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory * /* factory -- unused */) const
{
  return mNetRefDefMap->queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}

//! Add the set of nets this structured procedure defines to the NUNetSet "defs"
void NUStructuredProc::getDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin(); iter != ref_defs.end(); ++iter) {
    defs->insert((*iter)->getNet());
  }
}


void NUStructuredProc::getDefs(NUNetRefSet *defs) const
{
  mNetRefDefMap->populateMappedFrom(defs);
}


bool NUStructuredProc::queryDefs(const NUNetRefHdl &def_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory * /* factory -- unused */) const
{
  return mNetRefDefMap->queryMappedFrom(def_net_ref, fn);
}


void NUStructuredProc::replaceDef(NUNet* old_net, NUNet* new_net)
{
  NUNetRefSet uses(mNetRefFactory);
  NUNetRefHdl old_net_ref = mNetRefFactory->createNetRef(old_net);
  getUses(old_net_ref, &uses);

  mNetRefDefMap->erase(old_net_ref, &NUNetRef::overlapsSameNet);

  NUNetRefHdl new_net_ref = mNetRefFactory->createNetRef(new_net);
  addUses(new_net_ref, &uses);
}


bool NUStructuredProc::replace(NUNet * old_net, NUNet * new_net)
{
  return mBlock->replace(old_net,new_net);
}

bool NUStructuredProc::replaceLeaves(NuToNuFn & translator)
{
  return mBlock->replaceLeaves(translator);
}

const char* NUStructuredProc::typeStr() const
{
  return "NUStructuredProc";
}


void NUStructuredProc::print(bool recurse, int indent) const
{
  mBlock->print(recurse, indent);
}

void NUStructuredProc::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  *buf << "// " << getName()->str();
  indent.newline();
  mBlock->compose(buf, scope, numSpaces, recurse);
}

const NUModule* NUStructuredProc::findParentModule() const
{
  return mBlock->getParentModule();
}

NUModule* NUStructuredProc::findParentModule()
{
  return mBlock->getParentModule();
}


class CModelDiscoveryCallback : public NUDesignCallback
{
public:
  //! constructor
  CModelDiscoveryCallback() :
    NUDesignCallback() {}

  //! destructor
  ~CModelDiscoveryCallback() {}

  //! Catch all, continue
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Catch the c-model/pli call
  Status operator()(Phase, NUCModelCall*) {
    return eStop;
  }
};

bool NUStructuredProc::isCModelCall() const
{
  CModelDiscoveryCallback callback;
  NUDesignWalker walker(callback, false);
  NUDesignCallback::Status status = walker.structuredProc(const_cast<NUStructuredProc*>(this));
  return (status != NUDesignCallback::eNormal);
}

