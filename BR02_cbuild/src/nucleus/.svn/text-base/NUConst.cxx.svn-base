// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2005-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUExpr.h"
#include "bdd/BDD.h"
#include "shell/carbon_shelltypes.h"
#include "util/UtConv.h"
#include "util/DynBitVector.h"
#include "iodb/IODBNucleus.h"
#include "codegen/carbon_priv.h"

/*! \file
 * Implementation of NUConst and derived classes.
 */  
//! \brief Overall class for constants that contain no x's or z's.

/*! LRM says: Strings used as operands in expressions and assignments shall be
    treated as unsigned integer constants represented by a sequence of
    8-bit ASCII values, with one 8-bit ASCII value representing one
    character. */

//! Utility function called from various NUConstXXX::clearDirtyBits()
static void sDirtyBitClear(DynBitVector* bv, UInt32 bitSize, bool sign) {
  UInt32 dbvSize = bv->size ();
  if (dbvSize > bitSize) {
    bv->resize(bitSize);        // chopping off msbs, just throw away
  } else if (dbvSize < bitSize) { // extending the value
    bv->resize (bitSize);
    if(sign && bv->test (dbvSize-1))    // Was negative
      bv->lpartsel (dbvSize, bitSize-dbvSize).flip ();
  }
}

// extend values to necessary precision - this handles signed constants that
// require extension (consider 3'sd4 + 32'sd4, which should yield 32'd0.
//
static DynBitVector
extendHelper(const DynBitVector& val, UInt32 selfSize, UInt32 bitSize, bool sign)
{
  UInt32 hwSize = val.size ();
  selfSize = std::min (selfSize, hwSize);

  DynBitVector value (bitSize, val);

  if (sign && val.test (selfSize-1)) {
    // want sign extended result and signbit was set
    value.setRange (selfSize, bitSize - selfSize, 1);
  }
  return value;
}


//! convert a value represented in a UtString into a bvalue/drive pair
/*!
  Both bvalue and drive must be of equal and correct size, which
  is as least the optimal size.
  
  \param val value UtString to be represented in bit Reps. Size
  should be the natural size
  \param optimalSize The maximum size needed to represent the value
  (usually less then the natural size)
  \param bvalue Pointer to storage where the nominal of value in
  UtString will be placed, where x's are 1 and z's are 0. A ZERO'D,
  correctly-sized Rep must be entered here. It is also used to
  initialize a mask (an unfortunate side-effect of DynBitVectors).
  \param drive Mask to know whether or not the corresponding bit
  position in bvalue is an x or z. The bits will be 1 for those
  positions which are x or z. This parameter may be NULL. If it is
  NULL it is ignored.
*/
template <class Rep> void setNUValueReps(const UtString* val, 
                                         UInt32 optimalSize,
                                         Rep* bvalue, Rep* drive,
                                         const SourceLocator& loc)
{
  Rep mask(*bvalue); // need to inherit the correct size for bitv's.
  mask += 1;
  for (SInt32 i = ((SInt32) val->size()) - 1, j = 0;
       (i >= 0) && (j < ((SInt32) optimalSize));
       --i, ++j, mask <<= 1)
  {
    switch ((*val)[i]) {
    case '1':
    case 'H': // VHDL weak 1
    case 'h':
      *bvalue |= mask;
      break;
    case '0':
    case 'L': // VHDL weak 0
    case 'l':
      break;
    case 'X':
    case 'x':
    case '?':
    case '-': // VHDL don't care
    case 'W': // VHDL weak unknown
    case 'w':
    case 'U': // VHDL weak uninitialized
    case 'u':
      *bvalue |= mask;
      if (drive)
        *drive |= mask;
    case 'Z':
    case 'z':
      if (drive)
        *drive |= mask;
      break;
    default:
      {
        UtString buf;
        loc.compose(&buf);
        // print something somewhat useful here. Then crash.
        UtIO::cerr() << buf << " Internal compiler error: Unsupported constant value - '" << val->c_str() << "'" <<  UtIO::endl;
        abort();
      }
      break;
    }
  }
}

NUConst::NUConst(UInt32 bitSize, const SourceLocator& loc) :
  NUExpr(loc), mNaturalSize(bitSize)
{
}

NUConst::~NUConst()
{
}


UInt32 NUConst::determineOptimalBitSize(UInt64 val, bool isSigned, UInt32 bitSize)
{
  UInt64 bitpattern = val;
  // the caller specifies the number of bits in \a val that are
  // significant. If that number is larger than 64 (the most that can
  // be represented in \a val) then this routine makes the following
  // assumption: 
  //  All bits beyond the 64 in val are insignificant, this means that
  //  if isSigned is true then they are assumed to be identical to the
  //  most significant bit of val, if isSigned is false then they are
  //  assumed to be all zeros.
  UInt32 searchSize = std::min(bitSize, static_cast<UInt32>(sizeof(UInt64) * 8));
  if (isSigned && (val & (1LL<<(searchSize - 1)))) {
    // val is to be interpreted as a negative number
    if ( searchSize < 64 ) {
      bitpattern = (val | (-1LL << searchSize)); // if necessary sign-extend to 64 bits
    }
    // Looking at 11110xxxxx (or all ones),
    int locMostSignificantZero = carbon_DFLZ(bitpattern);
    if ( locMostSignificantZero == -1 ){
      return 1;      // bitpattern was all 1s, so optimal size is 1 bit
    }
    // The most significant one is one position to the left of the
    // most significant zero, and these positions are zero based.
    UInt32 minimumBitWidth = locMostSignificantZero + 1 + 1;
    return minimumBitWidth;
  } else {
    // Unsigned or positive number
    int locMostSignificantOne = carbon_DFLO(val & MASK(UInt64,searchSize));
    if (locMostSignificantOne == -1) {
      return 1;      // bitpattern was all zeros, so optimal size is 1 bit
    }
    UInt32 minimumBitWidth = locMostSignificantOne+1; // locMostSignificantOne is zero based
    if ( isSigned ) {
      minimumBitWidth++;        // a bit for the sign
    }
    return minimumBitWidth;
  }
}

UInt32 NUConst::determineOptimalBitSize(const UtString* val, bool isSigned)
{
  UtString::size_type pos = val->find_first_of("xXzZ?1");
  UInt32 optimizedSize = val->size();
  if (pos != UtString::npos) {
    optimizedSize -= pos;
    if ( isSigned ){
      optimizedSize++;          // a bit for the sign
    }
  }

  return optimizedSize;
}

void NUConstReal::resizeHelper(UInt32 )
{
  if (!mIsManaged)
  {
    mBitSize = 64;
  }
}
void NUConst::resizeHelper(UInt32 size)
{
  if (!mIsManaged)
  {
    NU_ASSERT (size > 0, this);
    mBitSize = size;
    clearDirtyBits(size);
  }
}

// note that the two makeSizeExplicitHelper implementations are identical
// and can be moved into NUConst to share them.  The value/drive distinction
// is handled in clearDirtyBits, which is virtual.  NUConstReal still needs
// its own version.

NUExpr* NUConstNoXZ::makeSizeExplicitHelper(UInt32 desired_size) {
  clearDirtyBits(desired_size);
  return this;
}

NUExpr* NUConstXZ::makeSizeExplicitHelper(UInt32 desired_size) {
  clearDirtyBits(desired_size);
  return this;
}

NUExpr* NUConstReal::makeSizeExplicitHelper(UInt32 desired_size) {
  // You cannot resize a real.  Real are always 64-bits.  Rely on
  // the backup base-class helper function to concat/partselect the
  // real.
  return NUExpr::makeSizeExplicitHelper(desired_size);
}


NUExpr* NUConstNoXZ::coerceOperandsToReal( ){
  NUExpr* ret_expr = this;
  if ( not ret_expr->isReal() ) {
    ret_expr = new NUUnaryOp (NUOp::eUnItoR, ret_expr, ret_expr->getLoc ());
  }
  return ret_expr->NUExpr::coerceOperandsToReal();
;
}
NUExpr* NUConstXZ::coerceOperandsToReal( ){
  NUExpr* ret_expr = this;
  if ( not ret_expr->isReal() ) {
    // the lrm (3.9.2) says that any x or z bits should be changed to 0 first 
    ret_expr = new NUUnaryOp (NUOp::eUnItoR, ret_expr, ret_expr->getLoc ());
  }
  return ret_expr->NUExpr::coerceOperandsToReal();
}
NUExpr* NUConstReal::coerceOperandsToReal( ){
  return this;                  // no-op
}

// If the constant is 0 -- we need one bit to represent it if it used
// on its own, but no bits to represent it when it's used as the high
// bits of a concat.  We choose to return a size of zero for this
// case.
//
UInt32 NUConstXZ::effectiveBitSizeHelper (ExprIntMap*) const
{
  UInt32 lastVBit = mValue.findLastOne ();
  if (not mValue.test (lastVBit))
    lastVBit = 0;

  UInt32 lastMBit = mDrive.findLastOne ();
  if (not mDrive.test (lastMBit))
    lastMBit = 0;
  return std::max (lastVBit, lastMBit) + 1;
}

UInt32 NUConstXZ::determineBitSizeHelper () const
{
  NU_ASSERT (mValue.size () == mDrive.size (), this);
  return mValue.size ();
}

UInt32 NUConstNoXZ::effectiveBitSizeHelper (ExprIntMap*) const
{
  // DynBitVector::findLastOne() returns msb if all zero
  UInt32 lastBit =  mValue.findLastOne ();
  if (not mValue.test (lastBit))
    return 0;                   // was all zero
  return lastBit+1;
}

void NUConst::getUses(NUNetSet * /* uses -- unused */) const
{
}


void NUConst::getUses(NUNetRefSet * /* uses -- unused */) const
{
}

void NUConst::getUsesElab(STBranchNode*, NUNetElabSet*) const
{
}

void NUConst::getUsesElab(STBranchNode*, NUNetElabRefSet2*) const
{
}


bool NUConst::queryUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			NUNetRefCompareFunction /* fn -- unused */,
			NUNetRefFactory * /* factory -- unused */) const
{
  return false;
}


bool NUConst::replace(NUNet * /* old_net */, NUNet * /* new_net */)
{
  return false;
}

bool NUConst::replaceLeaves(NuToNuFn & /* translator */)
{
  if (mIsManaged)
    return false;
  return false;
}

const char* NUConst::typeStr() const
{
  return "NUConst";
}


void NUConst::print(bool /* recurse_arg -unused */, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << typeStr() << '(' << this << ") ";
  printSize(screen);
  screen << " : ";

  UtString buf;
  const NUConst* string_constant = castConstNoXZ();
  if ( string_constant and string_constant->isDefinedByString() )
  {
    composeNoProtected(&buf, NULL);        // get the string representation
  }
  else
  {
    printVal(&buf, (hasXZ() ? eCarbonBin : isSignedResult () ? eCarbonDec : eCarbonUDec) );
  }
  
  screen << buf << UtIO::endl;

}


bool NUConst::hasBdd(BDDContext *, STBranchNode*) const
{
  if (!hasXZ())
  {
    if (isZero())
      return true;
    else
    {
      DynBitVector val;
      getSignedValue(&val);
      if (val == 1)
        return true;
    }
  }
  return false;
}

BDD NUConst::bdd(BDDContext *ctx, STBranchNode * /* scope -- unused */) const
{
  if (!hasXZ())
  {
    if (isZero())
      return ctx->val0();
    else
    {
      DynBitVector val;
      getSignedValue(&val);
      if (val == 1)
        return ctx->val1();
    }
  }
  return ctx->invalid();
}

NUConst* NUConst::createKnownIntConst(UInt64 val,
                                      UInt32 bit_width,
                                      bool definedByString,
                                      const SourceLocator& loc)
{
  NUConst* new_const = NULL;

    
  // Get the optimal bit width for the value
  DynBitVector v(bit_width);
  v = val;
  new_const = new NUConstNoXZ(v, bit_width, definedByString, loc);
  new_const->resize (bit_width);
  return new_const;
}

NUConst* NUConst::createXZ(const UtString& val, bool /* isSigned */,
                           UInt32 bit_width, const SourceLocator& loc )
{
  NUConst* new_const = NULL;

  UInt32 optWidth = bit_width;
  if (optWidth <= 32) {
    UInt32 wordValue = 0;
    UInt32 wordDrive = 0;
    setNUValueReps(&val, optWidth, &wordValue, &wordDrive, loc);
    new_const = new NUConstXZ(wordValue, wordDrive, bit_width, loc);
  }
  else {
    DynBitVector vecValue(optWidth);
    DynBitVector vecDrive(optWidth);
    setNUValueReps(&val, optWidth, &vecValue, &vecDrive, loc);
    new_const = new NUConstXZ(vecValue, vecDrive, bit_width, loc);
  }

  return new_const;
}

NUConst* NUConst::createKnownConst(const UtString& val,
                                   UInt32 bit_width, bool definedByString,
                                   const SourceLocator& loc)
{
  NUConst* new_const = NULL;

  DynBitVector vecValue(bit_width);
  DynBitVector* dummyVec = NULL;
  setNUValueReps(&val, bit_width, &vecValue, dummyVec, loc);
  new_const = new NUConstNoXZ(vecValue, bit_width, definedByString, loc);

  return new_const;

}

NUConst* NUConst::createKnownConst(const UtString& val,
                                  UInt32 bit_width, const SourceLocator& loc)
{
  // just call createKnownConst saying that source is not a string.
  return createKnownConst(val, bit_width, false, loc);
}

NUConst* NUConst::createConstFromStr(const UtString& val,
                                     UInt32 bit_width, 
                                     const SourceLocator& loc)
{
  UtString::size_type pos = val.find_first_of("xXzZ?");
  NUConst* ret = NULL;
  if (pos != UtString::npos)
    ret = createXZ(val, false, bit_width, loc); // a verilog string converted to a constant is never a signed value
  else
    ret = createKnownConst(val, bit_width, loc);
  return ret;
}

const NUConst* NUConst::castConst() const { return this; }

ptrdiff_t NUConstXZ::compareHelper(const NUExpr* otherExpr, bool,
                                 bool, bool) const
{
  const NUConst * other = otherExpr->castConstXZ();
  NU_ASSERT(other, otherExpr);
  
  // We are certain this pointer is valid to static-cast because
  // compareHelper() only called when getType()'s match
  const NUConstXZ *otherXZ = static_cast<const NUConstXZ*>(other);

  ptrdiff_t cmp = mValue.compare(otherXZ->mValue);
  if (cmp == 0) {
    cmp = mDrive.compare(otherXZ->mDrive);
  }
  return cmp;
}

ptrdiff_t NUConstNoXZ::compareHelper(const NUExpr* otherExpr, bool,
                                 bool, bool) const
{
  const NUConst * other = otherExpr->castConstNoXZ();
  NU_ASSERT(other, otherExpr);
  const NUConstNoXZ* that = static_cast<const NUConstNoXZ*>(other);

  ptrdiff_t cmp = isReal () - that->isReal ();
  if (cmp == 0) {
    cmp = mValue.compare(that->mValue);
  }
  return cmp;
}

ptrdiff_t NUConstReal::compareHelper(const NUExpr* otherExpr, bool,
                                     bool, bool) const
{
  const NUConstReal * other
    = dynamic_cast<const NUConstReal*>(otherExpr->castConstNoXZ());
  if (not other)
    // comparing a real against a non-real
    return NUConstNoXZ::compareHelper (otherExpr, false, false, false);

  double diff = mValue.realvalue () - other->mValue.realvalue ();

  if (diff < 0.0)
    return -1;
  else if (diff == 0.0)
    return 0;
  else return 1;
}

bool NUConstXZ::isDefinedByString() const
{
  return false;
}

const NUConst* NUConstNoXZ::castConstNoXZ() const { return this;}

bool NUConstXZ::hasXZ() const
{
  return mDrive.any();
}

size_t NUConst::hash() const
{
  UInt32 val;
  if (!getUL(&val))
  {
    DynBitVector bv;
    getSignedValue(&bv);
    val = *bv.getUIntArray();
  }
  return size_t(getType()) + (val << 8) + getBitSize();
}


size_t NUConst::shallowHash() const
{
  return hash();
}

NUConst* NUConst::create(bool sign, const DynBitVector& result,
                         UInt32 width, const SourceLocator& loc)
{
  NUConst *c;

  DynBitVector bv(result);
  bv.resize(width);
  c = new NUConstNoXZ(bv, width, false, loc);
  c->resize (width);
  c->setSignedResult (sign);
  return c;
}

NUConst* NUConst::createXZ(bool sign, const DynBitVector& result, const DynBitVector& drive,
                         UInt32 width, const SourceLocator& loc)
{
  NUConst *c;

  c = new NUConstXZ(result, drive, width, loc);
  c->resize (width);
  c->setSignedResult (sign);
  return c;
}

NUConst* NUConst::createXZ(bool sign, UInt64 val, UInt64 drive,
                         UInt32 width, const SourceLocator& loc)
{
  NUConst *c;

  c = new NUConstXZ(val, drive, width, loc);
  c->resize (width);
  c->setSignedResult (sign);
  return c;
}

//! Overloaded
NUConst* NUConst::create (bool sign, UInt64 result, UInt32 width,
                          const SourceLocator& loc)
{
  NUConst* c = createKnownIntConst (result, width, false, loc);
  c->setSignedResult (sign);
  c->resize(width);
  return c;
}

NUConst* NUConst::create (CarbonReal value, const SourceLocator& loc)
{
  NUConst* c = new NUConstReal (value, loc);
  c->resize (64);
  c->setSignedResult (true);
  return c;
}


// Default implementation of getValueDrive -- zeros the drive.  This
// gets overridden by the derived classes that handle X/Z
void NUConst::getValueDrive(DynBitVector* value,
                            DynBitVector* drive,
                            bool /* allowUnsized*/) const
{
  UInt32 sz = getBitSize();
  if (sz == 0)
    sz = determineBitSize();
  drive->resize(sz);
  drive->reset();
  getSignedValue(value);
}

NUConstXZ::NUConstXZ (const DynBitVector& value, const DynBitVector& drive, UInt32 size, const SourceLocator& loc)
  : NUConst (size, loc), mValue (value), mDrive (drive)
{
}

NUConstXZ::NUConstXZ (UInt64 value, UInt64 drive, UInt32 size, const SourceLocator& loc)
  : NUConst (size, loc), mValue (size, value), mDrive (size, drive) {}

NUConstXZ::~NUConstXZ() {}

const NUConst* NUConstXZ::castConstXZ() const { return this; }

void NUConstXZ::getValueDrive(DynBitVector* value, DynBitVector* drive,
                              bool /* allowUnsized */ ) const
{
  UInt32 sz = getBitSize();
  if (sz == 0)
    sz = determineBitSize();
  value->resize (sz);
  *value = mValue;
  drive->resize (sz);
  *drive = mDrive;
  UInt32 size = std::min(determineBitSize(), UInt32(value->size()));
  value->resize(size);
  drive->resize(size);
}


bool NUConstXZ::isolateZ(NUExpr **enable, NUExpr **driver) const {
  if (drivesZ()) {
    *enable = 0;
    *driver = 0;
    return true;
  } else {
    return false;
  }
}

void NUConstXZ::getSignedValue(DynBitVector* value) const {
  value->resize (getBitSize());
  *value = mValue & ~(mDrive);
  UInt32 size = std::min(determineBitSize(), UInt32(value->size()));
  value->resize(size);
}
  
bool
NUConstXZ::drivesZ(DynBitVector* bits) const {
  DynBitVector valcp(mValue);
  valcp.flip ();
  valcp &= mDrive;
  if (bits != NULL) {
    bits->resize(getBitSize());
    *bits |= valcp;
  }
  return (valcp != 0);
}

bool NUConstXZ::drivesOnlyZ() const {
  // Mask must be all 1's (for the bit size), and value must be all 0's
  if (mValue.any()) {
    return false;
  }
  if (mDrive.count() != mDrive.size()) {
    return false;
  }
  return true;
}

bool NUConstXZ::getULL(UInt64* val) const {
  DynBitVector cp(mValue);
  cp &= ~(mDrive);
  *val = cp.llvalue ();
  UInt32 bitSize = getBitSize();
  return (bitSize <= 64) || not cp.partsel (64, bitSize-64).any ();
}
bool NUConstNoXZ::getULL(UInt64* val) const {
  *val = mValue.llvalue ();

  UInt32 bitSize = getBitSize();
  return (bitSize <= 64) || not mValue.partsel (64, bitSize-64).any ();
}

bool NUConstXZ::getLL(SInt64* val) const {
  UInt32 bitSize = getBitSize();
  DynBitVector v (bitSize);
  getSignedValue (&v);
  v &= ~(mDrive);

  if (bitSize < 64)
  {
    // Easy case, can't overflow
    SInt64 tmp = v.partsel (0, bitSize);
    if (mSignedResult && v.test (bitSize-1))
      tmp |= (~UInt64 (0)) << bitSize;
      
    *val = tmp;
    return true;
  }
  else
  {
    // 64 or more bits - could be too large to represent
    SInt64 tmp = v.llvalue ();
    *val = tmp;

    // No overflow if the sign bit matches the other bits..
    if (tmp >= 0)
      return (v.partsel (64, bitSize-64).any () == 0);
    else
      // Negative better be represented by a properly sign-extended value
      return mSignedResult && v.partsel (64, bitSize-64).all ();
  }
}
  
bool NUConstXZ::getUL(UInt32* val) const {
  DynBitVector cp (mValue);
  cp &= ~(mDrive);

  *val = cp.value ();
  UInt32 bitSize = getBitSize();
  return (bitSize <= 32) || not cp.partsel (32, bitSize-32).any ();
}

  //! puts the known part of 32 bit value into (signed)val
bool NUConstXZ::getL(SInt32* val) const {
  UInt32 bitSize = getBitSize();
  DynBitVector v (bitSize);
  getSignedValue (&v);
  v &=  ~(mDrive);

  if (bitSize < 32)
  {
    // Easy case, can't overflow
    SInt32 tmp = v.partsel (0, bitSize);

    // If the expression is signed, then sign-extend it
    // as we widen it to 32 bits.
    if (mSignedResult && (tmp && 1u<<(bitSize-1)))
      tmp |= (~UInt32 (0)) << bitSize;

    *val = tmp;
    return true;
  }
  else
  {
    // 32 or more bits - could be too large to represent
    SInt32 tmp = v.value ();
    *val = tmp;

    if (tmp >= 0)
      // any extended bits must be zero, or we overflowed
      return (v.partsel (32, bitSize-32).any () == 0);
    else
      // all extended bits must be copies of the negative sign
      return v.partsel (32, bitSize-32).all ();
  }
}

NUExpr* NUConstXZ::copyHelper(CopyContext &/*copy_context*/) const {
  UInt32 bitSize = getBitSize();
  NUConstXZ* expr = new NUConstXZ(mValue, mDrive, bitSize, mLoc);
  expr->resize(bitSize);
  if (mSignedResult) {
    expr->setSignedResult(true);
  }
  return expr;
}

NUConst* NUConstXZ::rebuild(bool isSignedResult, UInt32 bitSize) const {
  DynBitVector v(mValue), d(mDrive);
  v.resize(bitSize);
  d.resize(bitSize);
  return NUConst::createXZ(isSignedResult, v, d, bitSize, mLoc);
}


const char* NUConstXZ::typeStr() const {
  return "NUConstXZ";
}

bool NUConstXZ::isZero (void) const
{
  return mValue.none ();
}

//! All ones? 
bool NUConstXZ::isOnes (void) const
{
  DynBitVector temp;
  getSignedValue(&temp);
  temp.flip();
  return temp.none();
}

bool NUConstXZ::isNegative (void) const
{
  return mValue.test (mValue.size ()-1);
}

bool NUConstXZ::isPartiallyDriven() const
{
  bool partiallyDriven = false;
  if (drivesZ())
  {
    DynBitVector tmpDrv;
    DynBitVector tmpVal;
    getValueDrive(&tmpVal, &tmpDrv);


    // If the constant does not model the full set of bits, we may
    // have trimmed the representation to save space (the constant
    // begins with a series of 0s, for example). If the constant
    // does not model all the necessary bits, we can consider it
    // partially driven.
    bool fullyRepresented = ((getBitSize() <= tmpVal.size()) and
                             (getBitSize() <= tmpDrv.size()));

    if (fullyRepresented) {
      // If the bit-vectors fully represent this constant, we don't
      // need to worry about any padding issues. We are partially
      // driven if there are any non-Z bits.

      bool allZs = tmpVal.none() && tmpDrv.all();
      partiallyDriven = not allZs;
    } else {
      // Otherwise, we aren't fully represented. This means that we
      // have at least one non-Z bit (the unrepresented high bits).
      // Since this is an XZ constant, we also know that there is at
      // least one Z bit. Taken together, this means that we are
      // partially driven.

      partiallyDriven = true;
    }
  }
  return partiallyDriven;
}
  
void NUConstXZ::clearDirtyBits(UInt32 size)
{
  sDirtyBitClear(&mValue, size, mSignedResult);
  sDirtyBitClear(&mDrive, size, mSignedResult);
  mBitSize = size;
}

void NUConstXZ::printVal(UtString* buf, CarbonRadix radix) const
{
  DynBitVector overrideMaskBV, value;

  IODBNucleus::sGetTempOverrideMask(this, &overrideMaskBV, &value);
  UInt32 nbits = value.size();

  const UInt32* overrideMask = overrideMaskBV.getUIntArray();

  // now create a buffer to hold the encoded characters
  size_t len = 0;

  switch(radix) {
  case eCarbonBin:
  {
    len = 1 + nbits;
    break;
  }
  case eCarbonHex:
  {
    len = 1 + (nbits+3)/4;
    break;
  }
  case eCarbonOct:
  case eCarbonUDec:
  case eCarbonDec:
  {
    len = 1 + (nbits+2)/3;
    if (radix == eCarbonDec) {
      len++;  // extra char for possible minus sign
    }
    break;
  }
  }

  int bufsize = buf->size();
  buf->reserve(bufsize + len);
  char* valueStr = buf->getBuffer() + bufsize;

  int numCharsConverted = -1;
  bool pulled = false;
  UInt32* val = value.getUIntArray();
  switch(radix) {
  case eCarbonBin:
  {
    numCharsConverted = CarbonValRW::writeBinXZValToStr(valueStr, len, val, NULL, NULL, NULL, overrideMask, pulled, nbits);
    break;
  }
  case eCarbonHex:
  {
    numCharsConverted = CarbonValRW::writeHexXZValToStr(valueStr, len, val, NULL, NULL, NULL, overrideMask, pulled, nbits);
    break;
  }
  case eCarbonOct:
  {
    numCharsConverted = CarbonValRW::writeOctXZValToStr(valueStr, len, val, NULL, NULL, NULL, overrideMask, pulled, nbits);
    break;
  }
  case eCarbonDec:
  {
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, val, NULL, NULL, NULL, overrideMask, pulled, true, nbits);
    break;
  }
  case eCarbonUDec:
  {
    numCharsConverted = CarbonValRW::writeDecXZValToStr(valueStr, len, val, NULL, NULL, NULL, overrideMask, pulled, false, nbits);
    break;
  }
  }
  if ( ( numCharsConverted != -1 ) and ( numCharsConverted < (int) len ) ) {
    buf->resize(bufsize + numCharsConverted);
  }
}

void NUConstXZ::getDrive (DynBitVector* drive) const
{
  drive->resize (getBitSize());
  *drive = mDrive;
}

NUExpr::Type NUConstXZ::getType (void) const
{
  return NUExpr::eNUConstXZ;
}

//! constructor
NUConstNoXZ::NUConstNoXZ(const DynBitVector& val, UInt32 bitSize, bool definedByString, const SourceLocator& loc)
  : NUConst(bitSize, loc), mDefinedByString (definedByString), mValue (val)
{
}

NUConstNoXZ::NUConstNoXZ(UInt32 val, UInt32 bitSize, const SourceLocator& loc)
  : NUConst(bitSize, loc), mDefinedByString (false), mValue (bitSize, &val, 1)
{
}
  
//! Constructor from a real
NUConstNoXZ::NUConstNoXZ (CarbonReal r, const SourceLocator& loc)
  : NUConst (64, loc), mDefinedByString (false), mValue (64) // SIZING!!
{
  mValue.copyreal (r);
}

//! Copy Constructor
NUConstNoXZ::NUConstNoXZ (const NUConstNoXZ& src)
  : NUConst (src.getBitSize(), src.mLoc),
    mDefinedByString (src.mDefinedByString), mValue (src.mValue) {}

//! constructor
NUConstNoXZ::~NUConstNoXZ() {}


// Get the properly signed 2's complement representation.
void NUConstNoXZ::getSignedValue(DynBitVector* value) const
{
  value->resize(getBitSize());
  *value = mValue;
}

//! puts the 64 bit value into val -- Josh sez this routine is busted for signed #s.  Use with care!
bool NUConstNoXZ::getLL(SInt64* val) const {
  UInt32 bitSize = getBitSize();
  DynBitVector v (bitSize);
  getSignedValue (&v);
      
  if (bitSize < 64)
  {
    // Easy case, can't overflow
    SInt64 tmp = v.partsel (0, bitSize);
    if (mSignedResult && v.test (bitSize-1))
      tmp |= (~UInt64 (0)) << bitSize;

    *val = tmp;

    return true;
  }
  else
  {
    // 64 or more bits - could be too large to represent
    SInt64 tmp = v.llvalue ();
    *val = tmp;

    if (tmp >= 0)
      return (v.partsel (64, bitSize - 64).any () == 0);
    else
      return mSignedResult && v.partsel (64, bitSize - 64).all ();
  }
}

//! puts 32-bit unsigned value into val
bool NUConstNoXZ::getUL(UInt32* val) const {
  *val = mValue.value ();
  UInt32 bitSize = getBitSize();
  return (bitSize <= 32) || not mValue.partsel (32, bitSize-32).any ();
}

//! puts 32-bit signed value into val
bool NUConstNoXZ::getL(SInt32* val) const
{
  UInt32 bitSize = getBitSize();
  if (bitSize < 32)
  {
    // Easy case, can't overflow
    SInt32 tmp = mValue.partsel (0, bitSize);

    // If the expression is signed, then sign-extend it
    // as we widen it to 32 bits.
    if (mSignedResult && (tmp & 1u<<(bitSize-1)))
      tmp |= (~UInt32 (0)) << bitSize;

    *val = tmp;
    return true;
  }
  else
  {
    // 32 or more bits - could be too large to represent
    SInt32 tmp = mValue.value ();
    *val = tmp;

    if (tmp >= 0)
      // positive ok if all excess bits are zero
      return (mValue.partsel (32, bitSize-32).any () == 0);
    else
      // neg okay if all excess are one AND number is signed
      return mSignedResult && mValue.partsel (32, bitSize-32).all ();
  }
}

  //! copy
NUExpr* NUConstNoXZ::copyHelper(CopyContext &/*copy_context*/) const {
  NUConstNoXZ* expr = new NUConstNoXZ(*this);
  expr->resize(getBitSize());
  if (mSignedResult) {
    expr->setSignedResult(true);
  }
  return expr;
}
  
//! Create a copy with a different size and sign
NUConst* NUConstNoXZ::rebuild(bool isSignedResult, UInt32 bitSize) const {
  DynBitVector v(mValue);
  v.resize(bitSize);
  return NUConst::create(isSignedResult, v, bitSize, mLoc);
}

bool NUConstNoXZ::isZero (void) const
{
  return mValue == 0;
}

bool NUConstNoXZ::isOnes (void) const
{
  return mValue.count () == getBitSize();
}

bool NUConstNoXZ::isNegative (void) const
{
  return mValue.test (mValue.size ()-1);
}


//! Is this a real number?
bool NUConstNoXZ::isReal( void ) const { return false; }

//! Emit C++ Code
CGContext_t NUConstNoXZ::emitCode (CGContext_t c) const
{
  return NUConst::emitCode( c );
}


//! Returns this class name
const char* NUConstNoXZ::typeStr() const {
  return "NUConstNoXZ";
}

void NUConstNoXZ::clearDirtyBits(UInt32 size) {
  sDirtyBitClear(&mValue, size, mSignedResult);
  mBitSize = size;
}

NUExpr::Type NUConstNoXZ::getType () const
{
  return NUExpr::eNUConstNoXZ;
}

//! value print
void NUConstNoXZ::printVal(UtString* buf, CarbonRadix radix) const {
  printValueByType(buf, mValue, radix);
}

//! Does constant have X or Z?
bool NUConstNoXZ::hasXZ() const { return false; }

//! Partially driven?
bool NUConstNoXZ::isPartiallyDriven() const { return false; }

//! returns true if this constant was defined by a string value.
bool NUConstNoXZ::isDefinedByString() const { return mDefinedByString; }


bool NUConstReal::hasXZ () const
{
  return false;
}
bool NUConstReal::isReal () const
{
  return true;
}

NUConstReal::NUConstReal (CarbonReal value,  const SourceLocator& loc)
  : NUConstNoXZ (value, loc)
{}

bool NUConstReal::getCarbonReal (CarbonReal* value) const
{
  *value = mValue.realvalue ();
  return true;
}

void NUConstReal::printVal (UtString*buf, CarbonRadix radix) const {
  printValueByType (buf, mValue.realvalue (), radix);
}

NUExpr* NUConstReal::copyHelper (CopyContext&) const {
  NUConstReal* expr = new NUConstReal (mValue.realvalue (), mLoc);
  return expr;
}

//! Create a copy with a different size and sign
NUConst* NUConstReal::rebuild(bool isSignedResult, UInt32 bitSize)
  const
{
  NUConstReal* expr = new NUConstReal (mValue.realvalue (), mLoc);
  expr->resize(bitSize);
  expr->setSignedResult(isSignedResult);
  return expr;
}

void NUConst::composeHelper(UtString* buf,
                            const STBranchNode* /* scope */,
                            bool /* includeRoot */,
                            bool /* hierName */,
                            const char* /* separator */) const
{
  bool showSized = true;
  UInt32 bitSize = mBitSize;
  if (bitSize == 0) {
    bitSize = determineBitSize();
  }
  CarbonRadix radix = eCarbonHex;

  if ((bitSize == 1) or hasXZ()) {
    radix = eCarbonBin;
  } else if ( bitSize == 32 ) {
    radix = isSignedResult()? eCarbonDec : eCarbonUDec;
    showSized = false;
  }

  // now ready to display 

  if ( showSized ) {
    composeSized ( buf, NULL, radix );
  } else {
    printVal(buf, radix );
  }
}

void NUConstNoXZ::composeHelper(UtString* buf,
                                const STBranchNode* scope,
                                bool includeRoot,
                                bool hierName,
                                const char* separator) const
{
  if (not isDefinedByString ()) {
    NUConst::composeHelper (buf, scope, includeRoot, hierName, separator);
    return;
  }

  *buf << "\"";
  composeAsciiString(buf);
  *buf << "\"";
}


void NUConstNoXZ::composeAsciiString(UtString* buf) const
{
  printAsciiValueByType (buf,mValue);
}

void NUConst::composeSized(UtString* buf,
                           const STBranchNode * /* scope */,
                           CarbonRadix radix) const
{
  UInt32 sz = getBitSize();
  *buf << sz;

  // In general we can only print accurate X & Z values in binary
  if ((sz == 1) or hasXZ()) {
    radix = eCarbonBin;
  }

  // Want to print as <size>'[s][bhod]<digits>
  const char * s = isSignedResult () ? (char*)"s" : (char *)"";
  switch( radix )
  {
  case eCarbonBin:  { *buf << "'" << s << "b"; break; }
  case eCarbonHex:  { *buf << "'" << s << "h"; break; }
  case eCarbonOct:  { *buf << "'" << s << "o"; break; }
  case eCarbonDec:  { *buf << "'" << s << "d"; break; }
  case eCarbonUDec: { *buf << "'u"; break; }
  }

  printVal(buf, radix);
}

void NUConst::printValueByType(UtString* buf, const DynBitVector& val, CarbonRadix radix) const
{
  UInt32 sz = mBitSize? mBitSize: mNaturalSize;
  UInt32 width = std::min((UInt32) val.size(), sz);

  DynBitVector printable (val.size ());
  printable = extendHelper (val, width, sz, isSignedResult ());
  DynBitVector::format(buf, radix, printable.getUIntArray (), width);
}


void NUConst::printValueByType(UtString* buf, double val, CarbonRadix) const
{
  *buf << val;
}

void NUConst::printAsciiValueByType(UtString* buf, const DynBitVector& val) const
{
  const UInt32 * valArray = val.getUIntArray();
  UInt32 arraySize = val.getUIntArraySize();

  for (SInt32 i = arraySize-1; i >= 0; --i)
  {
    union bigend {
      UInt32 word;
      UInt8 bytes[4];
    } chunk;
    chunk.word = valArray[i];
    for(int j=3; j>=0; --j)
    {
      switch (chunk.bytes[j]) {
      case 0: {
        // null chars are ignored
        break;
      }
      case '\n': {
        (*buf) += "\\n";
        break;
      }
      case '\t': {
        (*buf) += "\\t";
        break;
      }
      case '\"': {
        (*buf) += "\\\"";
        break;
      }
      case '\\': {
        (*buf) += "\\\\";
        break;
      }
      default: {
        (*buf) += chunk.bytes[j];
        break;
      }
      }
    }
  }
}


void
NUConst::printAsciiValueByType(UtString*, const double) const
{
  NU_ASSERT(0, this);
}

#if CG_ENABLE
//! Generate Low-level IR for the constant
/*virtual*/ LIR *NUConstNoXZ::generateLIR () const
{
  const size_t size = mValue.size ();
  if (size == 1 && mValue == 1) {
    return new LIR (getLoc (), LIR::eone, &mValue);
  } else if (size == 1 && mValue == 0) {
    return new LIR (getLoc (), LIR::ezero, &mValue);
  } else if (size <= 8) {
    return new LIR (getLoc (), LIR::econstant8, &mValue);
  } else if (size <= 16) {
    return new LIR (getLoc (), LIR::econstant16, &mValue);
  } else if (size <= 32) {
    return new LIR (getLoc (), LIR::econstant32, &mValue);
  } else if (size <= 64) {
    return new LIR (getLoc (), LIR::econstant64, &mValue);
  } else {
    INFO_ASSERT (false, "unimplemented");
    return NULL;
  }
}

//! Generate Low-level IR for the constant
/*virtual*/ LIR *NUConstXZ::generateLIR () const
{
  NU_ASSERT (false, this);
  return NULL;
}

//! Generate Low-level IR for the constant
/*virtual*/ LIR *NUConstReal::generateLIR () const
{
  NU_ASSERT (false, this);
  return NULL;
}
#endif
