// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUScope.h"
#include "util/CarbonAssert.h"
#include "util/StringAtom.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "util/UtIOStream.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNetRefSet.h"
#include "util/UtIndent.h"

NUModuleInstance::NUModuleInstance(StringAtom* name,
				   NUScope* parent,
				   NUModule* module,
				   NUNetRefFactory *netref_factory,
				   const SourceLocator& loc) :
  mMySymtabIndex(parent->reserveSymtabIndex()),
  mParent(parent),
  mModule(module),
  mName(name),
  mLoc(loc),
  mPortNetRefDefMap(netref_factory),
  mNetRefFactory(netref_factory)
{
}


NUModuleInstance::~NUModuleInstance()
{
  clearUseDef();

  while (not mPortConnections.empty()) {
    delete *(mPortConnections.begin());
    mPortConnections.erase(mPortConnections.begin());
  }
}

SInt32 NUModuleInstance::getSymtabIndex() const 
{ 
  return mMySymtabIndex; 
}


NUPortConnectionMap* NUModuleInstance::getPortMap() const
{
  NUPortConnectionMap* portMap = new NUPortConnectionMap();

  for (NUPortConnectionVectorIter iter = mPortConnections.begin();
       iter != mPortConnections.end();
       iter++) {
    (*portMap)[(*iter)->getFormal()] = *iter;
  }

  return portMap;
}


void NUModuleInstance::getPortConnections(NUPortConnectionList &conn_list) const
{
  conn_list.insert(conn_list.end(),
		   mPortConnections.begin(),
		   mPortConnections.end());
}


NUPortConnection *NUModuleInstance::getPortConnectionByIndex(int idx) const
{
  NU_ASSERT(idx >= 0, this);
  NU_ASSERT(idx < (int)mPortConnections.size(), this);
  return mPortConnections[idx];
}


void NUModuleInstance::getInputPortConnections(NUPortConnectionInputList &conn_list) const
{
  for (NUPortConnectionVectorIter iter = mPortConnections.begin();
       iter != mPortConnections.end();
       ++iter) {
    if ((*iter)->isPortConnInput()) {
      conn_list.push_back(dynamic_cast<NUPortConnectionInput*>(*iter));
    }
  }
}


void NUModuleInstance::getOutputPortConnections(NUPortConnectionOutputList &conn_list) const
{
  for (NUPortConnectionVectorIter iter = mPortConnections.begin();
       iter != mPortConnections.end();
       ++iter) {
    if ((*iter)->isPortConnOutput()) {
      conn_list.push_back(dynamic_cast<NUPortConnectionOutput*>(*iter));
    }
  }
}


void NUModuleInstance::getBidPortConnections(NUPortConnectionBidList &conn_list) const
{
  for (NUPortConnectionVectorIter iter = mPortConnections.begin();
       iter != mPortConnections.end();
       ++iter) {
    if ((*iter)->isPortConnBid()) {
      conn_list.push_back(dynamic_cast<NUPortConnectionBid*>(*iter));
    }
  }
}

NUBase* NUModuleInstance::getParentBase() const
{
  if (mParent->getScopeType() == NUScope::eModule) {
    return dynamic_cast<NUModule*>(mParent);
  } else if (mParent->getScopeType() == NUScope::eNamedDeclarationScope) {
    return dynamic_cast<NUNamedDeclarationScope*>(mParent);
  }
  return NULL;
}

NUModule* NUModuleInstance::getParentModule() const
{
  return mParent->getModule();
}

void NUModuleInstance::addDef(const NUNetRefHdl &def_net_ref)
{
  mPortNetRefDefMap.insert(def_net_ref);
}


void NUModuleInstance::addUse(const NUNetRefHdl &def_net_ref, const NUNetRefHdl &use_net_ref)
{
  mPortNetRefDefMap.insert(def_net_ref, use_net_ref);
}


void NUModuleInstance::addUses(const NUNetRefHdl &def_net_ref, NUNetRefSet *use_set)
{
  for (NUNetRefSet::iterator iter = use_set->begin(); iter != use_set->end(); ++iter) {
    mPortNetRefDefMap.insert(def_net_ref, *iter);
  }
}


void NUModuleInstance::setPortConnections(NUPortConnectionVector& connections)
{
  mPortConnections = connections;
}


void NUModuleInstance::addPortConnection(NUPortConnection* connection)
{
  mPortConnections.push_back(connection);
}


void NUModuleInstance::getUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUModuleInstance::getUses(NUNetRefSet *uses) const
{
  mPortNetRefDefMap.populateMappedToNonEmpty(uses);
}


bool NUModuleInstance::queryUses(const NUNetRefHdl &use_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory * /* factory -- unused */) const
{
  return mPortNetRefDefMap.queryMappedTo(use_net_ref, fn);
}


// TBD this is wrong for wacky ports
void NUModuleInstance::getUses(NUNet* net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


// TBD this is wrong for wacky ports
void NUModuleInstance::getUses(const NUNetRefHdl& net_ref, NUNetRefSet *uses) const
{
  mPortNetRefDefMap.populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUModuleInstance::queryUses(const NUNetRefHdl &def_net_ref,
				 const NUNetRefHdl &use_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory * /* factory -- unused */) const
{
  return mPortNetRefDefMap.queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}


// TBD This is wrong for wacky ports
void NUModuleInstance::getDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin(); iter != ref_defs.end(); ++iter) {
    defs->insert((*iter)->getNet());
  }
}


// TBD This is wrong for wacky ports
void NUModuleInstance::getDefs(NUNetRefSet *defs) const
{
  mPortNetRefDefMap.populateMappedFrom(defs);
}


bool NUModuleInstance::queryDefs(const NUNetRefHdl &def_net_ref,
				 NUNetRefCompareFunction fn,
				 NUNetRefFactory * /* factory -- unused */) const
{
  return mPortNetRefDefMap.queryMappedFrom(def_net_ref, fn);
}


void NUModuleInstance::replaceDef(NUNet *old_net, NUNet* new_net)
{
  NUNetRefSet uses(mNetRefFactory);
  NUNetRefHdl old_net_ref = mNetRefFactory->createNetRef(old_net);
  getUses(old_net_ref, &uses);

  mPortNetRefDefMap.erase(old_net_ref, &NUNetRef::overlapsSameNet);

  NUNetRefHdl new_net_ref = mNetRefFactory->createNetRef(new_net);
  addUses(new_net_ref, &uses);
}


bool NUModuleInstance::replace(NUNet *old_net, NUNet * new_net)
{
  NU_ASSERT2(0=="unimplemented", old_net, new_net);
  return 0;
}

bool NUModuleInstance::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  for (NUPortConnectionVectorIter iter = mPortConnections.begin();
       iter != mPortConnections.end();
       ++iter) {
    NUPortConnection *conn = (*iter);
    changed |= conn->replaceLeaves(translator);
  }
  return changed;
}

void NUModuleInstance::clearUseDef()
{
  mPortNetRefDefMap.clear();
}


void NUModuleInstance::fixupUseDef(NUNetList *remove_list)
{
  helperFixupUseDef(&mPortNetRefDefMap, remove_list);
}


NUModuleElab *NUModuleInstance::lookupElab(STBranchNode *hier) const
{
  // The branch node corresponds to the module parent. If the parent
  // of this instance is a named declaration scope, add it's hierarchy
  // on top prior to lookup.
  if (mParent->getScopeType() != NUScope::eModule) {
    UtList<SInt32> hierSymIndices;
    NUScope* parent = getParent();
    while (parent->getScopeType() != NUScope::eModule) {
      hierSymIndices.push_front(parent->getSymtabIndex());
      parent = parent->getParentScope();
    }
    for (UtList<SInt32>::iterator itr = hierSymIndices.begin();
         itr != hierSymIndices.end(); ++itr) {
      SInt32 symTabIdx = *itr;
      STSymbolTableNode* node = hier->getChild(symTabIdx);
      hier = node->castBranch();
    }
  }
  STSymbolTableNode *node = hier->getChild(mMySymtabIndex);
  NUModuleElab *elab = NUModuleElab::lookup(node->castBranch());
  ST_ASSERT(elab, node);
  return elab;
}


const char* NUModuleInstance::typeStr() const
{
  return "NUModuleInstance";
}


void NUModuleInstance::print(bool verbose_arg, int indent_arg) const
{
  bool verbose = UtIOStreamBase::limitBoolArg(verbose_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "ModuleInstance(" << this << ") : " << mName->str()
       << " of Module(" << mModule << ") in ";
  if (mParent->getScopeType() == NUScope::eModule) {
    UtIO::cout() << "Module(";
  } else if (mParent->getScopeType() == NUScope::eNamedDeclarationScope) {
    UtIO::cout() << "Declaration Scope(";
  }
  UtIO::cout() << mParent << ")" << UtIO::endl;

  if (verbose) {
    for (NUPortConnectionVectorIter iter = mPortConnections.begin();
	 iter != mPortConnections.end();
	 iter++) {
      (*iter)->print(true, numspaces+2);
    }
  }
}

void NUModuleInstance::getScopedName(UtString* buf, bool* previous_was_protected)
{
  UtList<NUScope*> scopes;
  NUScope* scope = getParent();
  while (scope->getScopeType() != NUScope::eModule)
  {
    if ( scope->isNamed() ) {
      scopes.push_front(scope);
    }
    scope = scope->getParentScope();
  }

  for (UtList<NUScope*>::iterator n = scopes.begin(); n != scopes.end(); ++n)
  {
    NUScope* scope = *n;
    if ( scope->getSourceLocation().isTicProtected() ){
      if (not *previous_was_protected ) {
        (*buf) << "<protected>.";
      }
      *previous_was_protected = true;
      continue;
    } else {
      *previous_was_protected = false;
      if ( scope->isNamed() ){
        (*buf) << scope->getName()->str() << ".";
      } else {
        // this should probably be an assert, but I am not yet sure
        // that a customer could not get here, so for now use '<unnamed>'
        (*buf) << "<unnamed>.";
      }
    }
  }
  if ( getLoc().isTicProtected() ) {
    if (not *previous_was_protected ){
      (*buf) << "<protected>";
    }
    *previous_was_protected = true;
  } else {
    (*buf) << getName()->str();
  }
}

void NUModuleInstance::compose(UtString* buf, const STBranchNode* scope, int numSpaces, bool) const
{
  
  // Print "moduleName    // modulefile:line"
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');

  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  *buf << mModule->getName()->str() << " ";
  indent.tabToLocColumn();
  *buf << " // ";
  mModule->getLoc().compose(buf);
  indent.newline();

  // Print "  instanceName    // instancefile:line"
  buf->append(numSpaces + 2, ' ');
  *buf << mName->str() << "( ";

  if (mPortConnections.size()==0) {
    *buf << ");";
  }

  indent.tabToLocColumn();
  *buf << " // ";
  mLoc.compose(buf);
  indent.newline();


  for (size_t i = 0; i < mPortConnections.size(); ++i)
  {
    NUPortConnection* pc = mPortConnections[i];
    NUNet* formal = pc->getFormal();
    buf->append(numSpaces + 4, ' ');
    *buf << "." << formal->getName()->str() << "(";
    NUPortConnectionInput* pci = dynamic_cast<NUPortConnectionInput*>(pc);
    NUPortConnectionOutput* pco = dynamic_cast<NUPortConnectionOutput*>(pc);
    NUPortConnectionBid* pcb = dynamic_cast<NUPortConnectionBid*>(pc);
    const char* dir = NULL;
    if (pci != NULL)
    {
      NUExpr* actual = pci->getActual();
      if (actual != NULL)
        actual->compose(buf, scope);
      dir = "input";
    }
    else if (pco != NULL)
    {
      NULvalue* actual = pco->getActual();
      if (actual != NULL)
        actual->compose(buf, scope, 0, true);
      dir = "output";
    }
    else if (pcb != NULL)
    {
      NULvalue* actual = pcb->getActual();
      if (actual != NULL)
        actual->compose(buf, scope, 0, true);
      dir = "inout";
    }
    NU_ASSERT(dir, pc);
    if (i == mPortConnections.size() - 1)
      *buf << "));";
    else
      *buf << "),";

    // tab over to column 50 for direction
    indent.tab(50);
    *buf << " // " << dir;
    indent.newline();
  } // for
} // void NUModuleInstance::compose

NUPortConnectionInput * NUModuleInstance::CoercePortConnectionToInput::operator()
  (NUPortConnection* conn) const
{
  return dynamic_cast<NUPortConnectionInput*>(conn);
}

const NUPortConnectionInput * NUModuleInstance::CoercePortConnectionToInput::operator()
  (const NUPortConnection* conn) const
{
  return dynamic_cast<const NUPortConnectionInput*>(conn);
}


NUPortConnectionOutput * NUModuleInstance::CoercePortConnectionToOutput::operator()
  (NUPortConnection* conn) const
{
  return dynamic_cast<NUPortConnectionOutput*>(conn);
}

const NUPortConnectionOutput * NUModuleInstance::CoercePortConnectionToOutput::operator()
  (const NUPortConnection* conn) const
{
  return dynamic_cast<const NUPortConnectionOutput*>(conn);
}

NUPortConnectionBid * NUModuleInstance::CoercePortConnectionToBid::operator()
  (NUPortConnection* conn) const
{
  return dynamic_cast<NUPortConnectionBid*>(conn);
}

const NUPortConnectionBid * NUModuleInstance::CoercePortConnectionToBid::operator()
  (const NUPortConnection* conn) const
{
  return dynamic_cast<const NUPortConnectionBid*>(conn);
}

void NUModuleInstance::putModule(NUModule* mod) {
  NU_ASSERT(mod != mModule, this);

  // If we are replacing the module, we need to fix up all the existing
  // port connections, because they refer to nets in the wrong module.

  for (size_t i = 0; i < mPortConnections.size(); ++i) {
    NUPortConnection* pc = mPortConnections[i];
    NUNet* oldFormal = pc->getFormal();
    int portIndex = mModule->getPortIndex(oldFormal);
    NU_ASSERT(portIndex != -1, oldFormal);
    NUNet* newFormal = mod->getPortByIndex(portIndex);
    NU_ASSERT(newFormal, this);
    pc->putFormal(newFormal);
    
  }
  mModule = mod;
}

void NUModuleInstance::addConnection(NUPortConnection* con) {
  NUNet* formal = con->getFormal();
  NUModule* mod = formal->getScope()->getModule();
  NU_ASSERT(mod == mModule, this);
  int portIndex = mod->getPortIndex(formal);
  NU_ASSERT(portIndex != -1, formal);
  NU_ASSERT(portIndex == (int) mPortConnections.size(), formal);
  mPortConnections.push_back(con);
}

int sCompare(const NUScope* s1, const NUScope* s2)
{
  int cmp = 0;
  if (s1 == NULL) {
    cmp = 1;
  } else if (s2 == NULL) {
    cmp = -1;
  } else {
    const char* n1 = s1->getName()->str();
    const char* n2 = s2->getName()->str();
    if (n1 != NULL && n2 != NULL) {
      cmp = strcmp(n1, n2);
      if (cmp == 0) {
        cmp = ((int)s1->getScopeType()) - ((int)s2->getScopeType());
        if (cmp == 0) {
          cmp = sCompare(s1->getParentScope(), s2->getParentScope());
        }
      }
    } else if (n1 == NULL && n2 != NULL) {
      cmp = -1;
    } else if (n2 == NULL) {
      cmp = 1;
    }
  }
  return cmp;
}

int NUModuleInstance::compare(const NUModuleInstance* i1, const NUModuleInstance* i2)
{
  int cmp = 0;
  if (i1 == i2) {
    cmp = 0;
  } else if (i1 == 0) {
    cmp = 1;
  } else if (i2 == 0) {
    cmp = -1;
  } else {
    cmp = strcmp(i1->getName()->str(), i2->getName()->str());
    if(cmp == 0)
      cmp = sCompare(i1->getParent(), i2->getParent());
  }
  return cmp;
}
