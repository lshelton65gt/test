//! -*-C++-*-

/***************************************************************************************
  Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*/

#include "nucleus/NUMappedNetRefList.h"
#include "util/DisjointRange.h"

NUMappedNetRefList::NUMappedNetRefList(const NUMappedNetRefList& src) :
  mNetRefFactory(src.mNetRefFactory),
  mNumBits(0)
{
  insert(end(), src.begin(), src.end());
}

void NUMappedNetRefList::clear() {
  mNetInfoMap.clear();
  mList.clear();
}

NUNetRefList::iterator
NUMappedNetRefList::insert(NUNetRefList::iterator pos, const NUNetRefHdl& hdl) {
  NUNet* net = hdl->getNet();
  INFO_ASSERT(net, "insert of empty netref?");

  mNumBits += hdl->getNumBits();

  pos = mList.insert(pos, hdl);
  ConstantRange range;
  bool contiguous = hdl->getRange(range);
  NU_ASSERT(contiguous, net);

  NetInfo& ni = mNetInfoMap[net];
  DisjointRange& disjoint_range = ni.first;
  RangeIterMap& range_iter_map = ni.second;
#if NU_USE_HASH_TO_SETS 
  ListIterSet& list_iter_set = range_iter_map[range];
  list_iter_set.insert(pos);
#else
  range_iter_map.insert(RangeIterMap::value_type(range, pos));  
#endif
  disjoint_range.insert(range);

  return pos;
}

NUNetRefList::iterator
NUMappedNetRefList::erase(NUNetRefList::iterator pos) {
  NUNetRefHdl ref = *pos;

  NUNet* net = ref->getNet();
  INFO_ASSERT(net, "erasing empty net ref");

  mNumBits -= ref->getNumBits();

  ConstantRange range;
  bool contiguous = ref->getRange(range);
  NU_ASSERT(contiguous, net);

  NetInfo& ni = mNetInfoMap[net];
  DisjointRange& disjoint_range = ni.first;
  RangeIterMap& range_iter_map = ni.second;
#if NU_USE_HASH_TO_SETS 
  ListIterSet& list_iter_set = range_iter_map[range];
  int count = list_iter_set.erase(pos);
  NU_ASSERT(count == 1, net);
  if (list_iter_set.empty()) {
    count = range_iter_map.erase(range);
    NU_ASSERT(count == 1, net);
    if (range_iter_map.empty()) {
      count = mNetInfoMap.erase(net);
      NU_ASSERT(count == 1, net);
    }
    else {
      disjoint_range.erase(range);
    }
  }
#else
  // remove the range from the RangeIterMap first -- that's a multimap
  // so it knows if more than one NUNetRefList::iterator referenced the
  // same range.  If that was the only entry, then we can also remove
  // the range from the disjoint_range.
  RangeIterMap::iterator p = range_iter_map.lower_bound(range);
  RangeIterMap::iterator e = range_iter_map.upper_bound(range);
  UInt32 num_entries_at_this_range = 0;
  bool found_pos = false;
  for (; p != e; ++num_entries_at_this_range) {
    RangeIterMap::iterator cur = p;
    ++p;
    NUNetRefList::iterator this_pos = cur->second;
    if (this_pos == pos) {
      range_iter_map.erase(cur);
      found_pos = true;
      break;
    }
  }
  NU_ASSERT(found_pos, net);

  if (range_iter_map.empty()) {
    mNetInfoMap.erase(net);
  }

  if (num_entries_at_this_range == 1) {
    disjoint_range.erase(range);
  }
#endif

  // remove it from the list itself
  pos = mList.erase(pos);
  return pos;
}

void NUMappedNetRefList::findOverlap(const NUNetRefHdl& ref,
                                     NUNetRefListIterList* iter_list)
{
  NUNet* net = ref->getNet();
  if (net != NULL) {
    NetInfoMap::iterator p = mNetInfoMap.find(net);
    if (p != mNetInfoMap.end()) {
      NetInfo& ni = p->second;
      DisjointRange& disjoint_range = ni.first;
      RangeIterMap& range_iter_map = ni.second;

      // It simplifies the problem if we can assume that the ref is a
      // contiguous range.  Since I think that's the case with
      // port-splitting, I'll assert that here.
      ConstantRange range;
      bool contiguous = ref->getRange(range);
      NU_ASSERT(contiguous, net);

      for (DisjointRange::OverlapLoop r(&disjoint_range, range);
           !r.atEnd(); ++r)
      {
        const ConstantRange& overlap_range = *r;

#       if NU_USE_HASH_TO_SETS 
        ListIterSet& list_iter_set = range_iter_map[overlap_range];
        for (Loop<ListIterSet> q(list_iter_set); !q.atEnd(); ++q) {
          NUNetRefList::iterator iter = *q;
          iter_list->push_back(iter);
        }
#else
        RangeIterMap::iterator q = range_iter_map.lower_bound(overlap_range);
        RangeIterMap::iterator e = range_iter_map.upper_bound(overlap_range);
        for (; q != e; ++q) {
          const ConstantRange& saved_range = q->first;
          NU_ASSERT(saved_range.overlaps(overlap_range), net);
          NUNetRefList::iterator iter = q->second;
          iter_list->push_back(iter);
        }
#endif
      }
    } // if
  } // if
} // void NUMappedNetRefList::findOverlap

void NUMappedNetRefList::print() {
  UtIO::cout() << "List:\n";
  for (Loop<NUNetRefList> p(mList); !p.atEnd(); ++p) {
    NUNetRefHdl ref = *p;
    ref->print(2);
  }

  UtIO::cout() << "Map:\n";
  for (LoopMap<NetInfoMap> p(mNetInfoMap); !p.atEnd(); ++p) {
    NUNet* net = p.getKey();
    const NetInfo& ni = p.getValue();
    const DisjointRange& disjoint_range = ni.first;
    const RangeIterMap& range_iter_map = ni.second;

    net->print(false, 2);
    for (CLoopMap<RangeIterMap> r(range_iter_map); !r.atEnd(); ++r) {
      const ConstantRange& range = r.getKey();
#if NU_USE_HASH_TO_SETS 
      const ListIterSet& list_iter_set = r.getValue();
      for (CLoop<ListIterSet> q(list_iter_set); !q.atEnd(); ++q)
      {
        NUNetRefList::iterator i = *q;
#else
      {
        NUNetRefList::iterator i = r.getValue();
#endif
        NUNetRefHdl ref = *i;
        ref->print(4);
        ConstantRange ref_range;
        bool contiguous = ref->getRange(ref_range);
        NU_ASSERT(contiguous, net);
        NU_ASSERT(ref_range == range, net);
      }
      
    }
    disjoint_range.print();
  }
  UtIO::cout().flush();
} // void NUMappedNetRefList::print
