// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUScope.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"

/*!
  \file
  Implementation of NUEnabledDriver class.
 */

NUEnabledDriver::NUEnabledDriver(NULvalue *lvalue,
                                 NUExpr *enable,
                                 NUExpr *driver,
                                 Strength strength,
                                 bool usesCFNet,
                                 const SourceLocator& loc) :
  NUStmt(usesCFNet, loc),
  mLvalue(lvalue),
  mEnable(enable),
  mDriver(driver),
  mStrength(strength)
{
}

NUEnabledDriver::~NUEnabledDriver()
{
  delete mLvalue;
  delete mEnable;
  delete mDriver;
}


void NUEnabledDriver::getUses(NUNetSet *uses) const
{
  mLvalue->getUses(uses);
  mEnable->getUses(uses);
  mDriver->getUses(uses);
}


void NUEnabledDriver::getUses(NUNetRefSet *uses) const
{
  mLvalue->getUses(uses);
  mEnable->getUses(uses);
  mDriver->getUses(uses);
}


bool NUEnabledDriver::queryUses(const NUNetRefHdl &use_net_ref,
				NUNetRefCompareFunction fn,
				NUNetRefFactory *factory) const
{
  if (mLvalue->queryUses(use_net_ref, fn, factory)) {
    return true;
  }
  if (mEnable->queryUses(use_net_ref, fn, factory)) {
    return true;
  }
  return mDriver->queryUses(use_net_ref, fn, factory);
}


void NUEnabledDriver::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  mLvalue->getUses(net_ref, uses);
  mEnable->getUses(uses);
  mDriver->getUses(uses);
}


bool NUEnabledDriver::queryUses(const NUNetRefHdl &def_net_ref,
				const NUNetRefHdl &use_net_ref,
				NUNetRefCompareFunction fn,
				NUNetRefFactory *factory) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  if (mLvalue->queryUses(def_net_ref, use_net_ref, fn, factory)) {
    return true;
  }
  if (mEnable->queryUses(use_net_ref, fn, factory)) {
    return true;
  }
  return mDriver->queryUses(use_net_ref, fn, factory);
}


void NUEnabledDriver::getDefs(NUNetSet *defs) const
{
  mLvalue->getDefs(defs);
}


void NUEnabledDriver::getDefs(NUNetRefSet *defs) const
{
  mLvalue->getDefs(defs);
}


bool NUEnabledDriver::queryDefs(const NUNetRefHdl &def_net_ref,
				NUNetRefCompareFunction fn,
				NUNetRefFactory *factory) const
{
  return mLvalue->queryDefs(def_net_ref, fn, factory);
}


void NUEnabledDriver::replaceDef(NUNet *old_net, NUNet *new_net)
{
  mLvalue->replaceDef(old_net, new_net);
}

bool NUEnabledDriver::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed |= mLvalue->replace(old_net,new_net);
  changed |= mEnable->replace(old_net,new_net);
  changed |= mDriver->replace(old_net,new_net);
  return changed;
}

bool NUEnabledDriver::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NULvalue * repl = translator(mLvalue, NuToNuFn::ePre);
    if (repl) {
      mLvalue = repl;
      changed = true;
    }
  }
  {
    NUExpr * repl = translator(mEnable, NuToNuFn::ePre);
    if (repl) {
      mEnable = repl;
      changed = true;
    }
    repl = translator(mDriver, NuToNuFn::ePre);
    if (repl) {
      mDriver = repl;
      changed = true;
    }
  }
  changed |= mLvalue->replaceLeaves(translator);
  changed |= mEnable->replaceLeaves(translator);
  changed |= mDriver->replaceLeaves(translator);

  {
    NULvalue * repl = translator(mLvalue, NuToNuFn::ePost);
    if (repl) {
      mLvalue = repl;
      changed = true;
    }
  }
  {
    NUExpr * repl = translator(mEnable, NuToNuFn::ePost);
    if (repl) {
      mEnable = repl;
      changed = true;
    }
    repl = translator(mDriver, NuToNuFn::ePost);
    if (repl) {
      mDriver = repl;
      changed = true;
    }
  }

  return changed;
}

bool NUEnabledDriver::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUEnabledDriver *ed = dynamic_cast<const NUEnabledDriver*>(&stmt);

  return ((*mLvalue == *ed->getLvalue ()) &&
          (*mEnable == *ed->getEnable ()) &&
          (*mDriver == *ed->getDriver ()) &&
          (mStrength == ed->getStrength()));
}

void NUEnabledDriver::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << typeStr() << "(" << this << ") : ";
  printName();

  switch (mStrength) {
  case eStrPull:
    UtIO::cout() << "pull ";
    break;
  case eStrDrive:
    UtIO::cout() << "drive ";
    break;

  case eStrTie:
    UtIO::cout() << "tie ";
    break;
  }
  UtIO::cout() << UtIO::endl;

  if (recurse) {
    mLvalue->print(true, numspaces+2);
    mEnable->print(true, numspaces+2);
    mDriver->print(true, numspaces+2);
  }
}

void
NUEnabledDriver::printName(void) const
{
  return;
}

const char* NUEnabledDriver::typeStr() const
{
  return "EnabledDriver";
}

const NUModule* NUEnabledDriver::findParentModule() const
{
  return mLvalue->findParentModule();
}

NUModule* NUEnabledDriver::findParentModule()
{
  return mLvalue->findParentModule();
}

void NUEnabledDriver::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  buf->append(numSpaces, ' ');
  mLvalue->compose(buf, scope, 0, recurse);
  *buf << " = ";
  getEnable()->compose(buf, scope);
  *buf << " ? ";
  getDriver()->compose (buf, scope);
  *buf << " : ";
  UInt32 bits = mLvalue->getBitSize();
  *buf << bits << "'bz;\n";
}

NUBlockingEnabledDriver::NUBlockingEnabledDriver(NULvalue *lvalue,
                                                 NUExpr *enable,
                                                 NUExpr *driver,
                                                 Strength strength,
                                                 bool usesCFNet,
                                                 const SourceLocator& loc) :
  NUEnabledDriver(lvalue, enable, driver, strength, usesCFNet, loc)
{}

const char* NUBlockingEnabledDriver::typeStr() const
{
  return "BlockingEnabledDriver";
}

NUStmt* NUBlockingEnabledDriver::copy(CopyContext &copy_context) const
{
  return new NUBlockingEnabledDriver(mLvalue->copy(copy_context),
                                     mEnable->copy(copy_context),
                                     mDriver->copy(copy_context),
                                     mStrength, false, mLoc);
}

void NUBlockingEnabledDriver::getBlockingUses(NUNetSet *uses) const
{
  getUses(uses);
}


void NUBlockingEnabledDriver::getBlockingUses(NUNetRefSet *uses) const
{
  getUses(uses);
}


bool NUBlockingEnabledDriver::queryBlockingUses(const NUNetRefHdl &use_net_ref,
					 NUNetRefCompareFunction fn,
					 NUNetRefFactory *factory) const
{
  return queryUses(use_net_ref, fn, factory);
}


void
NUBlockingEnabledDriver::getBlockingUses(const NUNetRefHdl &net_ref,
                                         NUNetRefSet *uses) const
{
  getUses(net_ref, uses);
}


bool NUBlockingEnabledDriver::queryBlockingUses(const NUNetRefHdl &def_net_ref,
                                                const NUNetRefHdl &use_net_ref,
                                                NUNetRefCompareFunction fn,
                                                NUNetRefFactory *factory) const
{
  return queryUses(def_net_ref, use_net_ref, fn, factory);
}


void NUBlockingEnabledDriver::getBlockingDefs(NUNetSet *defs) const
{
  getDefs(defs);
}


void NUBlockingEnabledDriver::getBlockingDefs(NUNetRefSet *defs) const
{
  getDefs(defs);
}


bool NUBlockingEnabledDriver::queryBlockingDefs(const NUNetRefHdl &def_net_ref,
                                                NUNetRefCompareFunction fn,
                                                NUNetRefFactory *factory) const
{
  return queryDefs(def_net_ref, fn, factory);
}


void NUBlockingEnabledDriver::getBlockingKills(NUNetSet *) const
{
  // We can drive z which really means do nothing so it has no kills
  return;
}


void NUBlockingEnabledDriver::getBlockingKills(NUNetRefSet *) const
{
  // We can drive z which really means do nothing so it has no kills
  return;
}


bool
NUBlockingEnabledDriver::queryBlockingKills(const NUNetRefHdl &,
                                            NUNetRefCompareFunction /* fn -- unused */,
                                            NUNetRefFactory *) const
{
  // We can drive z which really means do nothing so it has no kills
  return false;
}


NUContEnabledDriver::NUContEnabledDriver(StringAtom *name,
                                         NULvalue *lvalue,
                                         NUExpr *enable,
                                         NUExpr *driver,
                                         Strength strength,
                                         bool usesCFNet,
                                         const SourceLocator& loc) :
  NUEnabledDriver(lvalue, enable, driver, strength, usesCFNet, loc), mName(name), mScheduled (false)
{}

const char* NUContEnabledDriver::typeStr() const
{
  return "ContEnabledDriver";
}

NUStmt* NUContEnabledDriver::copy(CopyContext& copy_context) const
{
  StringAtom *name = copy_context.mScope->newBlockName(copy_context.mStrCache,
                                                       mLoc);
  return new NUContEnabledDriver(name,
                                 mLvalue->copy(copy_context),
                                 mEnable->copy(copy_context),
                                 mDriver->copy(copy_context),
                                 mStrength,
                                 getUsesCFNet(),
                                 mLoc);
}

void
NUContEnabledDriver::getBlockingUses(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void
NUContEnabledDriver::getBlockingUses(NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool
NUContEnabledDriver::queryBlockingUses(
  const NUNetRefHdl & /* use_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}

void
NUContEnabledDriver::getBlockingUses(
  const NUNetRefHdl & /* net_ref -- unused */,
  NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool 
NUContEnabledDriver::queryBlockingUses(
  const NUNetRefHdl & /* def_net_ref -- unused */,
  const NUNetRefHdl & /* use_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void
NUContEnabledDriver::getNonBlockingUses(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void
NUContEnabledDriver::getNonBlockingUses(NUNetRefSet * /* uses -- unused */)
  const
{
  NU_ASSERT(0, this);
}


bool
NUContEnabledDriver::queryNonBlockingUses(
  const NUNetRefHdl & /* use_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}

void
NUContEnabledDriver::getNonBlockingUses(
  const NUNetRefHdl & /* net_ref -- unused */,
  NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool
NUContEnabledDriver::queryNonBlockingUses(
  const NUNetRefHdl & /* def_net_ref -- unused */,
  const NUNetRefHdl & /* use_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void
NUContEnabledDriver::getBlockingDefs(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void
NUContEnabledDriver::getBlockingDefs(NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool
NUContEnabledDriver::queryBlockingDefs(
  const NUNetRefHdl & /* def_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void
NUContEnabledDriver::getNonBlockingDefs(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void
NUContEnabledDriver::getNonBlockingDefs(NUNetRefSet * /* uses -- unused */)
  const
{
  NU_ASSERT(0, this);
}


bool
NUContEnabledDriver::queryNonBlockingDefs(
  const NUNetRefHdl & /* def_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void
NUContEnabledDriver::getBlockingKills(NUNetSet * /* kills -- unused */) const
{
  NU_ASSERT(0, this);
}


void
NUContEnabledDriver::getBlockingKills(NUNetRefSet * /* kills -- unused */)
  const
{
  NU_ASSERT(0, this);
}


bool
NUContEnabledDriver::queryBlockingKills(
  const NUNetRefHdl & /* def_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContEnabledDriver::getNonBlockingKills(NUNetSet * /* kills -- unused */)
  const
{
  NU_ASSERT(0, this);
}


void
NUContEnabledDriver::getNonBlockingKills(NUNetRefSet * /* kills -- unused */)
  const
{
  NU_ASSERT(0, this);
}


bool
NUContEnabledDriver::queryNonBlockingKills(
  const NUNetRefHdl & /* def_net_ref -- unused */,
  NUNetRefCompareFunction /* fn -- unused */,
  NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}

void
NUContEnabledDriver::printName(void) const
{
  UtIO::cout() << mName->str()  << " : ";
}

NUStmt* NUContEnabledDriver::copyBlocking() const
{
  NU_ASSERT(mStrength == eStrDrive, this);
  CopyContext copy_context(NULL,NULL);
  return new NUBlockingEnabledDriver(mLvalue->copy(copy_context),
                                     mEnable->copy(copy_context),
                                     mDriver->copy(copy_context),
                                     mStrength, getUsesCFNet(), mLoc);
}


//! Use this method to decide whether to call the 'getBlocking*' or 'get*' methods
bool NUContEnabledDriver::useBlockingMethods() const {
  return false;
}

bool NUEnabledDriver::drivesZ(NUNet* net) const {
  NUNetSet defs;
  mLvalue->getDefs(&defs);
  return defs.find(net) != defs.end();
}

bool NUContEnabledDriver::isScheduled (void) const
{
  return mScheduled;
}

void NUContEnabledDriver::putScheduled (bool f)
{
  mScheduled = f;
}
