// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "nucleus/NUFor.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUExpr.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "nucleus/NULvalue.h"

NUFor::NUFor(const NUStmtList & initial_stmts,
	     NUExpr *           condition,
	     const NUStmtList & advance_stmts,
	     const NUStmtList & body_stmts,
	     NUNetRefFactory *netref_factory,
             bool usesCFNet,
	     const SourceLocator & loc) :
  NUBlockStmt(netref_factory, usesCFNet, loc),
  mInitialStmts(initial_stmts),
  mCondition(condition),
  mAdvanceStmts(advance_stmts),
  mBodyStmts(body_stmts)
{
}

NUFor::~NUFor()
{
  clearUseDef();

  while (not mInitialStmts.empty()) {
    delete *(mInitialStmts.begin());
    mInitialStmts.erase(mInitialStmts.begin());
  }

  delete mCondition;

  while (not mAdvanceStmts.empty()) {
    delete *(mAdvanceStmts.begin());
    mAdvanceStmts.erase(mAdvanceStmts.begin());
  }

  while (not mBodyStmts.empty()) {
    delete *(mBodyStmts.begin());
    mBodyStmts.erase(mBodyStmts.begin());
  }
}

bool NUFor::operator==(const NUStmt& stmt) const
{
  if (stmt.getType () != getType ())
    return false;

  const NUFor* forStmt = dynamic_cast<const NUFor*>(&stmt);

  return (ObjListEqual<NUStmtCLoop>(this->loopInitial (), forStmt->loopInitial ())
          && (*mCondition == *(forStmt->getCondition ()))
          && ObjListEqual<NUStmtCLoop>(this->loopAdvance (), forStmt->loopAdvance ())
          && ObjListEqual<NUStmtCLoop>(this->loopBody (), forStmt->loopBody ()) );
}

NUStmtList * NUFor::getInitial(bool copy_statements) const
{
  NUStmtList * result;
  if ( copy_statements ) {
    CopyContext copy_context(NULL,NULL);
    result = new NUStmtList();
    for (NUStmtListIter iter = mInitialStmts.begin(); iter != mInitialStmts.end(); ++iter) {
      result->push_back((*iter)->copy(copy_context));
    }
  } else {
    // just provide a new list that points at the original statements.
    result = new NUStmtList(mInitialStmts.begin(), mInitialStmts.end());
  }
  return result;
}


void NUFor::removeInitialStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mInitialStmts, stmt, replacement);
}


void NUFor::replaceInitial(const NUStmtList& new_stmts)
{
  mInitialStmts.assign(new_stmts.begin(), new_stmts.end());
}

NUStmtList * NUFor::getAdvance(bool copy_statements) const
{
  NUStmtList * result;
  if ( copy_statements ) {
    CopyContext copy_context(NULL,NULL);
    result = new NUStmtList();
    for (NUStmtListIter iter = mAdvanceStmts.begin(); iter != mAdvanceStmts.end(); ++iter) {
      result->push_back((*iter)->copy(copy_context));
    }
  } else {
    // just provide a new list that points at the original statements.
    result = new NUStmtList(mAdvanceStmts.begin(), mAdvanceStmts.end());
  }
  return result;
}


void NUFor::removeAdvanceStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mAdvanceStmts, stmt, replacement);
}


void NUFor::replaceAdvance(const NUStmtList& new_stmts)
{
  mAdvanceStmts.assign(new_stmts.begin(), new_stmts.end());
}


NUStmtList * NUFor::getBody() const
{
  return new NUStmtList(mBodyStmts.begin(), mBodyStmts.end());
}


void NUFor::removeBodyStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mBodyStmts, stmt, replacement);
}


void NUFor::replaceBody(const NUStmtList& new_stmts)
{
  mBodyStmts.assign(new_stmts.begin(), new_stmts.end());
}

void NUFor::addBodyStmt(NUStmt * stmt)
{
  mBodyStmts.push_back(stmt);
}


void NUFor::addBodyStmtStart(NUStmt * stmt)
{
  mBodyStmts.push_front(stmt);
}


void NUFor::replaceDef(NUNet* old_net,
		       NUNet* new_net)
{
  for (NUStmtListIter iter = mInitialStmts.begin(); iter != mInitialStmts.end(); ++iter) {
    (*iter)->replaceDef(old_net,new_net);
  }
  for (NUStmtListIter iter = mAdvanceStmts.begin(); iter != mAdvanceStmts.end(); ++iter) {
    (*iter)->replaceDef(old_net,new_net);
  }
  for (NUStmtListIter iter = mBodyStmts.begin(); iter != mBodyStmts.end(); ++iter) {
    (*iter)->replaceDef(old_net,new_net);
  }
}

bool NUFor::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;

  for (NUStmtListIter iter = mInitialStmts.begin(); iter != mInitialStmts.end(); ++iter) {
    changed |= (*iter)->replace(old_net,new_net);
  }

  changed |= mCondition->replace(old_net,new_net);

  for (NUStmtListIter iter = mAdvanceStmts.begin(); iter != mAdvanceStmts.end(); ++iter) {
    changed |= (*iter)->replace(old_net,new_net);
  }

  for (NUStmtListIter iter = mBodyStmts.begin(); iter != mBodyStmts.end(); ++iter) {
    changed |= (*iter)->replace(old_net,new_net);
  }
  return changed;
}

bool NUFor::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  for (NUStmtListIter iter = mInitialStmts.begin(); iter != mInitialStmts.end(); ++iter) {
    changed |= (*iter)->replaceLeaves(translator);
  }

  {
    NUExpr * repl = translator(mCondition, NuToNuFn::ePre);
    if (repl) {
      mCondition = repl;
      changed = true;
    }
  }
  changed |= mCondition->replaceLeaves(translator);

  for (NUStmtListIter iter = mAdvanceStmts.begin(); iter != mAdvanceStmts.end(); ++iter) {
    changed |= (*iter)->replaceLeaves(translator);
  }

  for (NUStmtListIter iter = mBodyStmts.begin(); iter != mBodyStmts.end(); ++iter) {
    changed |= (*iter)->replaceLeaves(translator);
  }
  {
    NUExpr * repl = translator(mCondition, NuToNuFn::ePost);
    if (repl) {
      mCondition = repl;
      changed = true;
    }
  }

  return changed;
}

void NUFor::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "For(" << this << ")" << UtIO::endl;
  if (recurse) {
    for (NUStmtListIter iter = mInitialStmts.begin(); iter != mInitialStmts.end(); iter++) {
      (*iter)->print(recurse, numspaces+2);
    }

    if(mCondition != NULL)
      mCondition->print(recurse,numspaces+2);

    for (NUStmtListIter iter = mAdvanceStmts.begin(); iter != mAdvanceStmts.end(); iter++) {
      (*iter)->print(recurse, numspaces+2);
    }
    for (NUStmtListIter iter = mBodyStmts.begin(); iter != mBodyStmts.end(); iter++) {
      (*iter)->print(recurse, numspaces+2);
    }
  }
}

const char* NUFor::typeStr() const
{
  return "NUFor";
}

NUStmt* NUFor::copy(CopyContext &copy_context) const
{
  NUStmtList copy_body;
  for (NUStmtListIter iter = mBodyStmts.begin(); iter != mBodyStmts.end(); ++iter) {
    copy_body.push_back((*iter)->copy(copy_context));
  }

  NUStmtList copy_initial;
  for (NUStmtListIter iter = mInitialStmts.begin(); iter != mInitialStmts.end(); ++iter) {
    copy_initial.push_back((*iter)->copy(copy_context));
  }

  NUExpr *           condition = mCondition->copy(copy_context);

  NUStmtList copy_advance;
  for (NUStmtListIter iter = mAdvanceStmts.begin(); iter != mAdvanceStmts.end(); ++iter) {
    copy_advance.push_back((*iter)->copy(copy_context));
  }

  return new NUFor(copy_initial,
		   condition,
		   copy_advance,
		   copy_body,
		   mNetRefFactory,
                   getUsesCFNet(), 
		   mLoc);
}

void MsgContext::composeLocation(const NUFor *stmt, UtString* location)
{
  if (stmt == NULL)
    (*location) << "(null NUFor)";
  else
  {
    const SourceLocator& loc = stmt->getLoc();
    UtString source;
    loc.compose(&source);
    (*location) << source;
  }

}

static bool sGetBinaryOp(const NUExpr* expr, NUOp::OpT* op, const NUNet** var,
                         SInt64* val)
{
  const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(expr);
  if (bop != NULL)
  {
    const NUIdentRvalue* rv =
      dynamic_cast<const NUIdentRvalue*>(bop->getArg(0));
    const NUConst* k = bop->getArg(1)->castConst();
    if ((rv != NULL) && (k != NULL) && k->getLL(val))
    {
      *op = bop->getOp();
      *var = rv->getIdent();
      return true;
    }
  }
  return false;
}

static bool sGetAssignOp(const NUBlockingAssign* assign, NUOp::OpT* op,
                         const NUNet* var,
                         SInt64* val)
{
  const NUNet* assignVar;
  if (assign->getLvalue()->isWholeIdentifier() &&
      (assign->getLvalue()->getWholeIdentifier() == var) &&
      sGetBinaryOp(assign->getRvalue(), op, &assignVar, val) &&
      (assignVar == var))
    return true;
  return false;
}

bool NUFor::estimateLoopCount(SInt32* count)
  const
{
  // Expect condition of the form (var OP const) where OP is <,>,<=,>=, or !=
  // Expect initial of the form (var = const)
  // Expect advance of the form (var = var OP const) where OP = + or -
  NUOp::OpT advanceOp;
  NUOp::OpT condOp;
  SInt64 init, advanceVal, condVal;
  const NUConst* val;
  bool ret = false;

  // Cannot estimate if either the initial or advance is not a single statement
  if ((mInitialStmts.size() != 1) or (mAdvanceStmts.size() != 1)) {
    return false;
  }

  NUStmtListIter iter = mAdvanceStmts.begin();
  NUStmt * stmt = (*iter);
  NUBlockingAssign * advance_stmt = dynamic_cast<NUBlockingAssign *>(stmt);

  iter = mInitialStmts.begin();
  stmt = (*iter);
  NUBlockingAssign * initial_stmt = dynamic_cast<NUBlockingAssign *>(stmt);
  
  if ( ( initial_stmt == NULL ) or ( advance_stmt == NULL ) ){
    // VHDL while loops are converted to a FOR loop (with no initial or
    // advance statements) but if there was a single function call in
    // the while condition, a single task enable will have been inserted
    // into the initial and advance stmt list, make sure that the
    // current loop is not this special case (the stmts must be
    // assigns).  If they are not assignments then we cannot estimate.
    return false;
  }

  const NUNet* var;

  if (sGetBinaryOp(mCondition, &condOp, &var, &condVal) &&
      initial_stmt->getLvalue()->isWholeIdentifier() &&
      (initial_stmt->getLvalue()->getWholeIdentifier() == var) &&
      ((val = initial_stmt->getRvalue()->castConst()) != NULL) &&
      val->getLL(&init) &&
      sGetAssignOp(advance_stmt, &advanceOp, var, &advanceVal) &&
      (advanceVal != 0) &&
      ((advanceOp == NUOp::eBiPlus) || (advanceOp == NUOp::eBiMinus)))
  {
    int sign = (advanceOp == NUOp::eBiPlus)? 1: -1;
    if (advanceVal < 0)
    {
      sign = -sign;
      advanceVal = -advanceVal;
    }
    switch (condOp)
    {
    case NUOp::eBiSLte:
    case NUOp::eBiULte:
      condVal += sign * advanceVal;
      // no break
    case NUOp::eBiSLt:
    case NUOp::eBiULt:
      if (sign == 1)
      {
        *count = (condVal - init) / advanceVal;
        ret = true;
      }
      break;
    case NUOp::eBiSGtre:
    case NUOp::eBiUGtre:
      condVal += sign * advanceVal;
      // no break
    case NUOp::eBiSGtr:
    case NUOp::eBiUGtr:
      if (sign -= 1)
      {
        *count = (init - condVal) / advanceVal;
        ret = true;
      }
      break;
    case NUOp::eBiNeq:
      if ((sign == 1) && (condVal > init))
      {
        *count = (condVal - init) / advanceVal;
        ret = true;
      }
      else if ((sign == -1) && (init > condVal))
      {
        *count = (init - condVal) / advanceVal;
        ret = true;
      }
      break;
    default:
      break;
    } // switch
  }
  return ret;
} // bool NUFor::estimateLoopCount

void NUFor::compose(UtString *buf, const STBranchNode* scope,
                    int numSpaces, bool recurse) const
{
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
     composeProtectedNUObject(mLoc, buf);
    return; 
  }

  bool print_initial_outside = ( mInitialStmts.size() > 1 );
  bool print_advance_in_body = ( mAdvanceStmts.size() > 1 );
  if ( print_initial_outside ){
    for (NUStmtCLoop l=loopInitial (); !l.atEnd (); ++l) {
      (*l)->compose(buf, scope, 0, recurse);
    }
    *buf << "for ( /*initial stmt before for*/ ;\n";
  } else {
    *buf << "for (";
    for (NUStmtCLoop l=loopInitial (); !l.atEnd (); ++l) {
      (*l)->compose(buf, scope, 0, recurse);
    }
  }

  buf->append(numSpaces+5, ' ');
//  *buf << "; ";
  getCondition ()->compose(buf, scope);
  *buf << "; \n";
  buf->append(numSpaces+5, ' ');

  if ( print_advance_in_body ) {
    *buf << "/*advance stmts at end of body*/ ";
  } else {
    // here there can be at most one statement, 
    // we print it without a trailing semi
    for (NUStmtCLoop l=loopAdvance (); !l.atEnd (); ++l) {
      UtString advBuf;
      (*l)->compose(&advBuf, scope, numSpaces + 2, recurse);
      // remove the semicolon
      size_t semiIndex = advBuf.rfind(';');
      if (semiIndex != advBuf.npos) {
        advBuf.erase(semiIndex);
      }
      *buf <<  advBuf;
    }
  }
  buf->append(numSpaces+4, ' ');
  *buf << ") begin\n";
  for (NUStmtCLoop l=loopBody (); !l.atEnd (); ++l) {
    (*l)->compose(buf, scope, numSpaces + 2, recurse);
  }
  buf->append(numSpaces, ' ');
  if ( print_advance_in_body ){
    *buf << "/* Advance statements */\n";
    buf->append(numSpaces, ' ');
    for (NUStmtCLoop l=loopAdvance (); !l.atEnd (); ++l) {
      (*l)->compose(buf, scope, numSpaces + 2, recurse);
    }
  }
  *buf << "end\n";
}

  
