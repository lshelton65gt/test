// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implementation of the Nucleus block class
 */

#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "util/UtIndent.h"
#include "iodb/IODBNucleus.h"
#include "bdd/BDD.h"

NUBlock::NUBlock(const NUStmtList * stmts,
                 NUScope * parent,
                 IODBNucleus * iodb,
                 NUNetRefFactory * netref_factory,
                 bool uses_cf_net,
                 const SourceLocator & loc) :
  NUBlockStmt(netref_factory, uses_cf_net, loc),
  NUBlockScope(iodb, eHTBlock),
  mNetRefFactory(netref_factory),
  mParent(parent),
  mIsBreakTarget(false)
{
  if (stmts != NULL) {
    mStmts.assign(stmts->begin(), stmts->end());
  }

  NU_ASSERT2(dynamic_cast<NUNamedDeclarationScope*>(mParent) == NULL, this, mParent->getModule ());
  mParent->addBlock(this);
}


NUBlock::~NUBlock()
{
  mParent->removeBlock(this);

  clearUseDef();

  for (NUStmtLoop iter = loopStmts(); not iter.atEnd(); ++iter) {
    delete *iter;
  }

  for (NUNetLoop iter = loopLocals(); not iter.atEnd(); ++iter) {
    mNetRefFactory->erase(*iter);
    delete *iter;
  }

  // Note: do not need to call delete on the nested blocks; those are
  // handled when we delete the statements, since they are a statement.
}


//! Realise NUScopeElab::getSourceLocation virtual method
/*virtual*/ const SourceLocator NUBlock::getSourceLocation () const
{
  return getLoc ();                     // the NUStmt method
}


void NUBlock::addBlock(NUBlock * block)
{
  mBlockList.push_back(block);
}


void NUBlock::removeBlock(NUBlock * block)
{
  mBlockList.remove(block);
}


NUBlockCLoop NUBlock::loopBlocks() const 
{ 
  return NUBlockCLoop(mBlockList); 
}


NUBlockLoop NUBlock::loopBlocks() 
{
  return NUBlockLoop(mBlockList); 
}


void NUBlock::addDeclarationScope(NUNamedDeclarationScope *)
{
  NU_ASSERT(0, this); // blocks cannot have declaration scopes.
}


void NUBlock::removeDeclarationScope(NUNamedDeclarationScope *)
{
  NU_ASSERT(0, this); // blocks cannot have declaration scopes.
}


NUNamedDeclarationScopeCLoop NUBlock::loopDeclarationScopes() const 
{
  // construct empty list on the stack so the Loop will be initialized.
  NUNamedDeclarationScopeList dummy;
  return NUNamedDeclarationScopeCLoop(dummy); // empty loop
}


NUNamedDeclarationScopeLoop NUBlock::loopDeclarationScopes() 
{
  // construct empty list on the stack so the Loop will be initialized.
  NUNamedDeclarationScopeList dummy;
  return NUNamedDeclarationScopeLoop(dummy); // empty loop
}


void NUBlock::addLocal(NUNet * net)
{
  // blocks are only allowed to contain non-static temp nets.
  NU_ASSERT((net->isBlockLocal() and net->isNonStatic() and net->isTemp()), net);

  addNetName(net);
  mLocalNetList.push_back(net);
  // When Blocks are created during strip mining (class Stripper)
  // there may not be an iodb visible.  So, we have to be careful.
  if ( mIODB ) {
    mIODB->addTypeIntrinsic( net );
  }
}


void NUBlock::removeLocal(NUNet* net, bool remove_references)
{
  mLocalNetList.remove(net);
  removeNetName(net);
  if (remove_references) {
    mNetRefFactory->erase(net);
  }
}

void NUBlock::removeLocals(const NUNetSet& nets, bool remove_references) {
  NUScope::removeNetsHelper(nets, remove_references, &mLocalNetList);
}

void NUBlock::moveLocals(NUScope* target)
{
  for (NUNetLoop p = loopLocals(); !p.atEnd(); ++p) {
    NUNet* net = *p;
    target->addLocal(net);
    net->replaceScope(this, target);
  }
  mLocalNetList.clear();
}


void NUBlock::getAllNonTFNets(NUNetList *net_list) const
{
  NUNetCLoop net_loop = loopLocals();
  net_list->insert(net_list->end(), net_loop.begin(), net_loop.end());

  getAllSubNets(net_list);
}


void NUBlock::getAllNets(NUNetList *net_list) const
{
  getAllNonTFNets(net_list);
}


void NUBlock::getAllSubNets(NUNetList *net_list) const
{
  for (NUBlockCLoop loop = loopBlocks(); not loop.atEnd(); ++loop) {
    (*loop)->getAllNets(net_list);
  }
}


SInt32 NUBlock::reserveSymtabIndex()
{
  return mParent->reserveSymtabIndex();
}


void NUBlock::removeStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mStmts, stmt, replacement);
}


void NUBlock::replaceStmt(NUStmt *old_stmt, NUStmt *new_stmt)
{
  NUStmtListNonconstIter iter;
  for (iter = mStmts.begin(); iter != mStmts.end(); ++iter) {
    if (*iter == old_stmt) break;
  }
  NU_ASSERT2(iter != mStmts.end(), this, old_stmt);
  mStmts.insert(iter, new_stmt);
  mStmts.erase(iter);
}


void NUBlock::replaceStmt(NUStmt *old_stmt, NUStmtList *new_stmts)
{
  NUStmtListNonconstIter iter;
  for (iter = mStmts.begin(); iter != mStmts.end(); ++iter) {
    if (*iter == old_stmt) break;
  }
  NU_ASSERT2(iter != mStmts.end(), this, old_stmt);
  mStmts.insert(iter, new_stmts->begin(), new_stmts->end());
  mStmts.erase(iter);
}


NUStmtList * NUBlock::getStmts() const
{
  return new NUStmtList(mStmts.begin(), mStmts.end());
}

void NUBlock::replaceStmtList(const NUStmtList& new_stmts)
{
  mStmts.assign(new_stmts.begin(), new_stmts.end());
}


void NUBlock::addStmt(NUStmt *stmt)
{
  mStmts.push_back(stmt);
}


void NUBlock::addStmts(NUStmtList *stmts)
{
  mStmts.insert(mStmts.end(), stmts->begin(), stmts->end());
}


void NUBlock::addStmtStart(NUStmt *stmt)
{
  mStmts.push_front(stmt);
}


void NUBlock::replaceDef(NUNet *old_net, NUNet* new_net)
{
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter) {
    (*iter)->replaceDef(old_net, new_net);
  }
}
  

bool NUBlock::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter)
    changed |= (*iter)->replace(old_net, new_net);
  return changed;
}


bool NUBlock::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter)
    changed |= (*iter)->replaceLeaves(translator);
  return changed;
}


bool NUBlock::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUBlock * nb = dynamic_cast<const NUBlock*>(&stmt);

  return ObjListEqual<NUStmtCLoop> (loopStmts (), nb->loopStmts ());
}


const char* NUBlock::typeStr() const
{
  return "Block";
}


StringAtom* NUBlock::gensym(const char* prefix,
                            const char* name,
                            const NUBase* proxy,
                            bool isAlreadyUnique)
{
  // Blocks are not part of the elaborated symbol table. The parent
  // (usually task or module) generates and enforces symbol
  // uniqueness.
  return getParentScope()->gensym(prefix, name, proxy, isAlreadyUnique);
}


BDD NUBlock::bdd(NUNet* net, BDDContext *ctx, STBranchNode *scope) const
{
  // For now, just deal with single-stmt blocks.
  bool single_stmt = false;
  NUStmtListIter iter = mStmts.begin();
  NUStmt *stmt = 0;
  if (iter != mStmts.end()) {
    stmt = *iter;
    ++iter;
    single_stmt = (iter == mStmts.end());
  }

  if (single_stmt) {
    return stmt->bdd(net, ctx, scope);
  } else {
    return ctx->invalid();
  }
}

void
NUBlock::putIsBreakTarget( bool isBreakTarget )
{
  mIsBreakTarget = isBreakTarget;
}

NUStmt* NUBlock::copy(CopyContext &copy_context) const
{
  NUBlock * block = createNewBlock(copy_context);

  // Map this block in the copy context
  if ( isBreakTarget() ) {
    block->putIsBreakTarget( true );
    copy_context.mBlockReplacements[this] = block;
  }

  CopyContext local_copy_context(block, copy_context.mStrCache, 
                                 copy_context.mCopyScopeVariables,
                                 copy_context.mPrefix);

  // copy in block stuff from our parent scope
  local_copy_context.mBlockReplacements.insert(copy_context.mBlockReplacements.begin(),
                                               copy_context.mBlockReplacements.end());

  if ( copy_context.mCopyScopeVariables ) {
    // copy in net stuff from our parent scope
    local_copy_context.mNetReplacements.insert(copy_context.mNetReplacements.begin(),
					       copy_context.mNetReplacements.end());
    
    // TBD: move this into a method...
    for ( NUNetCLoop loop = loopLocals() ; 
	  !loop.atEnd() ;
	  ++loop ) {
      NUNet * net = (*loop);
      NUNet * local = NULL;
      
      NUMemoryNet * memory_net = dynamic_cast<NUMemoryNet*>(net);
      NUTempMemoryNet * tmp_memory_net = dynamic_cast<NUTempMemoryNet*>(net);

      StringAtom *name;

      // we cannot preserve names of temp nets, as they are unique to
      // a module scope. instead, we uniquify with our parent module's
      // counter (preserving most of the original name).
      if (copy_context.mPrefix) {
        name = block->gensym(copy_context.mPrefix,
                             net->getName()->str());
      } else {
        name = block->gensym(net->getName()->str());
      }
      
      if ( net->isBitNet() ) {
	local = new NUBitNet ( name,
			       net->getFlags(),
			       block,
			       net->getLoc() );
      } else if ( net->isVectorNet() ) {
        NUVectorNet * vector_net = dynamic_cast<NUVectorNet*>(net);
	local = new NUVectorNet ( name,
				  new ConstantRange( *vector_net->getRange() ),
				  vector_net->getFlags(),
                                  vector_net->getVectorNetFlags (),
				  block,
				  vector_net->getLoc() );
      } else if ( memory_net ) {
	NUMemoryNet * local_mem = 
	  new NUMemoryNet ( name,
			    new ConstantRange( *memory_net->getWidthRange() ),
			    new ConstantRange( *memory_net->getDepthRange() ),
			    memory_net->getFlags(),
                            memory_net->getVectorNetFlags (),
			    block,

			    memory_net->getLoc() );
        NU_ASSERT( local_mem->getBitSize() != 0, memory_net );
        local_mem->copyRanges(memory_net);
        local_mem->copyAuxInformation(memory_net);
	local = local_mem;
      } else {
	// fail if we see something unexpected
	NU_ASSERT ( tmp_memory_net, net); // not a bit,vector,memory or temp-memory

	NUMemoryNet * master = tmp_memory_net->getMaster();

	NUNet * net_copy = copy_context.lookup(master);
	NUMemoryNet * master_copy = dynamic_cast<NUMemoryNet*>(net_copy);
	NU_ASSERT (master_copy, master);

	local = new NUTempMemoryNet ( name,
				      master_copy,
				      tmp_memory_net->getFlags(),
				      tmp_memory_net->isDynamic(),
				      block );
      }
      NU_ASSERT(local, net);

      block->addLocal( local );
      local_copy_context.mNetReplacements[ net ] = local;
    }

  }

  NUStmtList copy_stmts;
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter) {
    copy_stmts.push_back((*iter)->copy(local_copy_context));
  }

  block->addStmts(&copy_stmts);

  if ( copy_context.mCopyScopeVariables ) {
    // copy replaced locals back to our parent scope.
    copy_context.mNetReplacements.insert(local_copy_context.mNetReplacements.begin(),
                                         local_copy_context.mNetReplacements.end());
  }
  return block;
}


void NUBlock::moveStmts(NUBlock * dstBlock)
{
  // Move over the statements
  for (NUStmtLoop l = loopStmts(); not l.atEnd(); ++l) {
    NUStmt* stmt = *l;
    dstBlock->addStmt(stmt);
  }
  mStmts.clear();

  // Move over the locals
  NUScope* oldScope = this;
  NUScope* scope = dstBlock;
  for (NUNetLoop l = loopLocals(); not l.atEnd(); ++l) {
    NUNet* net = *l;
    dstBlock->addLocal(net);
    net->replaceScope(oldScope, scope);
  }
  for (NUNetList::iterator p = mLocalNetList.begin();
       p != mLocalNetList.end(); 
       ++p) {
   removeNetName(*p);
  }
  mLocalNetList.clear();

  // Move over the sub-blocks.
  for (NUBlockLoop l = loopBlocks(); not l.atEnd(); ++l) {
    NUBlock* block = *l;
    dstBlock->addBlock(block);
    block->mParent = scope;
  }
  mBlockList.clear();
}


void NUBlock::print(bool recurse_arg, int indent_arg) const
{
  // this strange testing of args is for protection from calls from
  // within gdb  where the default args should have been used. We do
  // not modify the args themselves because that can corrupt the stack
  bool recurse = recurse_arg;
  int  indent = indent_arg;
  if ( ( recurse_arg != true ) && ( recurse_arg != false ) ) { recurse = true; }
  if ( abs(indent_arg) > 150 ) indent = 0;

  for (int i = 0; i < indent; i++) {
    UtIO::cout() << " ";
  }

  mLoc.print();
  UtIO::cout() << "Block(" << this << ") : "
               << "ParentScope(" << mParent << ")";
  if (isSeparated ()) {
    UtIO::cout () << " (separated)";
  }
  UtIO::cout () << UtIO::endl;

  if (recurse) {
    for (NUNetCLoop iter = loopLocals(); not iter.atEnd(); ++iter) {
      (*iter)->print(false, indent+2);
    }
    for (NUStmtCLoop iter = loopStmts(); not iter.atEnd(); ++iter) {
      (*iter)->print(recurse, indent+2);
    }
  }
}


void NUBlock::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( getLoc().isTicProtected() ){
    composeProtectedNUObject(getLoc(), buf);
    return; 
  }

  *buf << "begin ";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);

  if (isSeparated ()) {
    *buf << " (separated)";
  }

  indent.newline();

  numSpaces += 2;

  // print local net declarations
  buf->append(numSpaces, ' ');
  *buf << "// Declarations\n";
  NUNetLoopSorted p(loopLocals());
  for (; !p.atEnd(); ++p)
  {
    NUNet* net = *p;
    buf->append(numSpaces, ' ');
    net->composeType(buf);
    net->composeDeclaration(buf);
    *buf << "\n";
  }

  // nested blocks also appear in the statement list; no special
  // handling of them here.

  // print contained statements.
  buf->append(numSpaces, ' ');
  *buf << "// Statements\n";
  for (NUStmtCLoop p = loopStmts(); !p.atEnd(); ++p)
  {
    NUStmt* stmt = *p;
    stmt->compose(buf, scope, numSpaces, recurse);
  }

  numSpaces -= 2;
  buf->append(numSpaces, ' ');
  *buf << "end\n";
} 

NUBlock* NUBlock::createNewBlock(CopyContext& copy_context) const
{
  return new NUBlock(NULL, copy_context.mScope, mIODB, mNetRefFactory,
                     getUsesCFNet(), mLoc);
}


NUSeparatedBlock::NUSeparatedBlock (StringAtom *name,
  const NUStmtList * stmts,
  NUScope * parent,
  IODBNucleus * iodb,
  NUNetRefFactory * netref_factory,
  bool uses_cf_net,
  const SourceLocator & loc) : 
  NUBlock (stmts, parent, iodb, netref_factory, uses_cf_net, loc),
  mName (name), mClosure (), mHaveClosure (false)
{
#if 0
  UtString b0;
  loc.compose (&b0);
  UtIO::cout () << __PRETTY_FUNCTION__ << ": " << name->str () << ": " << b0 << "\n";
  while (parent != NULL) {
    if (parent->isNamed ()) {
      UtIO::cout () << "  " << parent->getName ()->str () << "\n";
    } else {
      UtIO::cout () << "  anon\n";
    }
    parent = parent->getParentScope ();
  }
#endif
}

NUBlock* NUSeparatedBlock::createNewBlock(CopyContext& copy_context) const
{
  NUSeparatedBlock* blk = new NUSeparatedBlock(mName, NULL, copy_context.mScope, mIODB,
                                               mNetRefFactory, getUsesCFNet(), mLoc);
  blk->uniquifyCopiedName();
  return blk;
}

void NUSeparatedBlock::uniquifyCopiedName()
{
  mName = gensym("copy_", mName->str());
}

// Two mechanisms for enumerating the nets to establish the closure of
// separated blocks are implemented. The first is a design walker that grabs
// the nets from NUIdentRvalue and NUIdentLvalue instances. The other uses
// getBlockingDefs and getBlockingUses. Both should work, however the
// getBlockingXXX method currently fails regression. This requires further
// investigation. However, for now, just set USE_CLOSURE_WALKER and use the
// design walker.

#define USE_CLOSURE_WALKER 1

//! Destructor for an NUSeparatedBlock
/*virtual*/ NUSeparatedBlock::~NUSeparatedBlock() 
{}

// Both the design walker and the getBlocking mechanism test nets with the
// testNetForClosure static function.
static inline bool testNetForClosure (NUNet *net, const NUBlock *block, NUSeparatedBlock::Callback &notify)
{
  return net->isNonStatic ()
    && net->getScope () != block
    && !net->isDead ()
    && notify (net);
}

// Returns true for nets that are not local, not static and are dead.
static inline bool testNetForLocalization (NUNet* net, const NUBlock* block)
{
  return net->isNonStatic()
    && net->getScope() != block
    && net->isDead();
}

#if USE_CLOSURE_WALKER

// Walks the design testing all nets for inclusion in the block closure.
// Also provides a list of nets that need to be replaced by local temporaries.
// These are the ones that are not passed by reference to function, but
// belong to different scope.
class ClosureWalker : public NUDesignCallback {
public:

  ClosureWalker (NUSeparatedBlock::Callback &notify,
                 NUNetSet& netsToLocalize, NUSeparatedBlock *block) : 
    mBlock (block), mNotify (notify), mNetsToLocalize(netsToLocalize) {}

protected:

  virtual Status operator() (Phase, NUBase *) { return eNormal; }

  virtual Status operator() (Phase phase, NUNet *net)
  {
    if (phase == ePre) {
      considerNet (net);
    }
    return eNormal;
  }

  virtual Status operator()(Phase phase, NUIdentRvalue *node)
  {
    if (phase == ePre) {
      considerNet (node->getIdent ());
    }
    return eNormal;
  }


  virtual Status operator()(Phase phase, NUIdentLvalue *node)
  {
    if (phase == ePre) {
      considerNet (node->getIdent ());
    }
    return eNormal;
  }

  virtual Status operator() (Phase phase, NUMemselLvalue *node)
  {
    if (phase == ePre) {
      considerNet (node->getIdent ());
    }
    return eNormal;
  }

  virtual Status operator() (Phase phase, NUMemselRvalue *node)
  {
    if (phase == ePre) {
      considerNet (node->getIdent ());
    }
    return eNormal;
  }

private:

  NUSeparatedBlock *mBlock;
  NUSeparatedBlock::Callback &mNotify;
  NUNetSet& mNetsToLocalize;

  void considerNet (NUNet *net)
  {
    if (testNetForClosure (net, mBlock, mNotify)) {
      mBlock->addNetToClosure (net);
    } else if (testNetForLocalization (net, mBlock)) {
      mNetsToLocalize.insert(net);
    }
  }

};
#endif

void NUSeparatedBlock::computeClosure (Callback &notify,
                                       NUNetSet& netsToLocalize,
                                       bool force /* = false */) const
{
  if (mHaveClosure && !force) {
    return;                             // the closure has already been computed
  }
#if USE_CLOSURE_WALKER
  ClosureWalker callback (notify, netsToLocalize,
                          const_cast <NUSeparatedBlock *> (this));
  NUDesignWalker walk (callback, false);
  walk.blockStmt (const_cast <NUSeparatedBlock *> (this));
#else
  NUNetSet candidates;
  getBlockingUses (&candidates);
  getBlockingDefs (&candidates);
  for (NUNetSet::iterator it = candidates.begin (); it != candidates.end (); ++it) {
    NUNet *net = *it;
    if (testNetForClosure (net, this, notify)) {
      addNetToClosure (net);
    } else if (testNetForLocalization (net, mBlock)) {
      netsToLocalize.insert(net);
    }
  }
#endif
  mHaveClosure = true;
}
