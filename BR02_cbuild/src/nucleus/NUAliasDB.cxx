// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "nucleus/NUAliasDB.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNetRef.h"

#include "exprsynth/SymTabExpr.h"
#include "exprsynth/ExprReduce.h"

#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include "symtab/STBranchNode.h"
#include "iodb/IODBRuntimeAliasBOM.h"
#include "util/UtXmlWriter.h"

NUAliasDataBOM::NUAliasDataBOM() :
  mNet(NULL),
  mMySymtabIndex(-1),
  mNodeIdent(NULL)
{}

NUAliasDataBOM::~NUAliasDataBOM()
{}

void NUAliasDataBOM::print() const 
{
  UtOStream& screen = UtIO::cout();
  if (mNet) {
    UtString b0;
    screen << UtIO::hex << mNet;
  } else {
    screen << "(null)";
  }
  screen << ", ";
  screen << mMySymtabIndex;
  screen << ", ";
  if (mNodeIdent)
    screen << UtIO::hex << mNodeIdent;
  else
    screen << "(null)";
  screen << UtIO::endl;
}

NUNet * NUAliasDataBOM::getNet()
{
  INFO_ASSERT(mNet, "BOM corruption");
  return mNet;
}

const NUNet * NUAliasDataBOM::getNet() const
{
  INFO_ASSERT(mNet, "BOM corruption");
  return mNet;
}

void NUAliasDataBOM::setNet(NUNet * net) 
{
  mNet = net;
}

bool NUAliasDataBOM::hasValidNet()
  const
{
  NUBase* base = mNet;          // could be a scope due to union
  return (base != NULL) && (dynamic_cast<NUNet*>(base) != NULL);
}

NUScope * NUAliasDataBOM::getScope()
{
  // Scope can be NULL (in the case of flattened hierarchy)
  return mScope;
}

const NUScope * NUAliasDataBOM::getScope() const
{
  const NUScope * scope = dynamic_cast<const NUScope*>(mScope);
  return scope;
}

void NUAliasDataBOM::setScope(NUScope * scope) 
{
  mScope = scope;
}

SInt32 NUAliasDataBOM::getIndex() const
{
  return mMySymtabIndex;
}

void NUAliasDataBOM::setIndex(SInt32 index)
{
  mMySymtabIndex = index;
}

void NUAliasDataBOM::putIdent(CarbonIdent* ident)
{
  mNodeIdent = ident;
}

CarbonIdent* NUAliasDataBOM::getIdent()
{
  const NUAliasDataBOM* me = const_cast<const NUAliasDataBOM*>(this);
  return const_cast<CarbonIdent*>(me->getIdent());
}

const CarbonIdent* NUAliasDataBOM::getIdent() const
{
  return mNodeIdent;
}

NUAliasBOM::NUAliasBOM()
{}

NUAliasBOM::~NUAliasBOM()
{}

STFieldBOM::Data NUAliasBOM::allocData()
{
  NUAliasDataBOM * obj = new NUAliasDataBOM;
  return static_cast<Data>(obj);
}

STFieldBOM::Data NUAliasBOM::allocBranchData()
{
  return allocData();
}

STFieldBOM::Data NUAliasBOM::allocLeafData()
{
  return allocData();
}

void NUAliasBOM::freeData(Data* bomdata)
{
  NUAliasDataBOM * obj = castBOM(*bomdata);
  delete obj;
  *bomdata = NULL;
}

void NUAliasBOM::freeBranchData(const STBranchNode*, Data* bomdata)
{
  freeData(bomdata);
}

void NUAliasBOM::freeLeafData(const STAliasedLeafNode*, Data* bomdata)
{
  freeData(bomdata);
}

void NUAliasBOM::printData(const Data bomdata) const
{
  const NUAliasDataBOM * obj = castBOM(bomdata);
  obj->print();
}

void NUAliasBOM::printLeaf(const STAliasedLeafNode* leaf) const
{
  printData(leaf->getBOMData());
}

void NUAliasBOM::printBranch(const STBranchNode* branch) const
{
  printData(branch->getBOMData());
}

NUAliasDataBOM * NUAliasBOM::castBOM(Data bomdata)
{
  return static_cast<NUAliasDataBOM*>(bomdata);
}

/*! \note The unelaborated symbol table written to the GUI DB file is
 *  constructed from the mAliasDB symbol table of each NUModule instance. The
 *  runtime cannot, and should not, just reconstruct the NUAliasDataBOM for
 *  each symbol. Indeed, that would imply constructing nucleus objects at
 *  runtime. This would be absurd. Instead, the NUAliasBOM::writeXXXX methods
 *  write information that is read into an IODBRuntimeAliasDataBOM.
 *
 *  The writeXXX method of NUAliasBOM must match the readXXX methods of
 *  IODBRunRuntimeAliasBOM.
 */

//! Write the branch data
/*! \note If this is changed then you must also change IODBRuntimeAliasBOM::readBranchData.
 */
void NUAliasBOM::writeBranchData(const STBranchNode *branch, ZostreamDB &out,
                                 AtomicCache*) const
{
  NUAliasDataBOM *bom = castBOM (branch->getBOMData ());
  UInt32 hierFlags;
  if (bom != NULL && bom->getScope () != NULL) {
    hierFlags = bom->getScope ()->getHierFlags ();
  } else {
    hierFlags = 0;
  }
  out << hierFlags;
}


//! Write the leaf data
/*! \note If this is changed then you must also change IODBRuntimeAliasBOM::readLeafData
 */
void NUAliasBOM::writeLeafData(const STAliasedLeafNode *leaf, ZostreamDB &out) const
{
  NUAliasDataBOM *bom = castBOM (leaf->getBOMData ());
  NUNet *net = bom->getNet ();
  UInt32 flags = 0;
  if (net->isInput ()) {
    flags |= IODBRuntimeAliasDataBOM::eIsInput;
  }
  if (net->isOutput ()) {
    flags |= IODBRuntimeAliasDataBOM::eIsOutput;
  }
  if (net->isBid ()) {
    flags |= IODBRuntimeAliasDataBOM::eIsBidi;
  }
  out << flags;
}

STFieldBOM::ReadStatus NUAliasBOM::readLeafData(STAliasedLeafNode*, ZistreamDB&, MsgContext*)
{
  INFO_ASSERT (false, "cannot read NUAliasBOM");
  return eReadOK;
}

STFieldBOM::ReadStatus NUAliasBOM::readBranchData(STBranchNode*, ZistreamDB&, MsgContext*)
{
  INFO_ASSERT (false, "cannot read NUAliasBOM");
  return eReadOK;
}

//! Write the signature for this BOM
/*! \note This will be read by IODBRuntimeAliasBOM::readBOMSignature
 */
void NUAliasBOM::writeBOMSignature(ZostreamDB &out) const
{
  // Grab the signature and version from the reader BOM
  out << IODBRuntimeAliasBOM::getSignature () 
      << IODBRuntimeAliasBOM::getVersion ();
}

STFieldBOM::ReadStatus NUAliasBOM::readBOMSignature(ZistreamDB &, UtString *)
{
  INFO_ASSERT (false, "cannot read NUAliasBOM");
  return eReadIncompatible;
}

ESFactory* NUAliasBOM::getExprFactory()
{
  return &mExprFactory;
}

void NUAliasBOM::mapExpr(CarbonIdent* ident, CarbonExpr* expr)
{
  mExprMap.mapExpr(ident, expr);
}


CarbonExpr* NUAliasBOM::getExpr(CarbonIdent* ident)
{
  return mExprMap.getExpr(ident);
}

const CarbonExpr* NUAliasBOM::getExpr(CarbonIdent* ident) const
{
  return mExprMap.getExpr(ident);
}

void NUAliasBOM::preFieldWrite(ZostreamDB&)
{}

STFieldBOM::ReadStatus 
NUAliasBOM::preFieldRead(ZistreamDB&)
{
  return eReadOK;
}


CarbonIdent * NUAliasBOM::storeCarbonIdent(NUNet * net,
                                           CarbonIdent* my_symtab_ident)
{
  bool added = false;
  CarbonIdent * symtab_ident = mExprFactory.createIdent(my_symtab_ident, added);
  if (not added) {
    delete my_symtab_ident;
  }
  
  net->putIdent(symtab_ident);
  return symtab_ident;
}

CarbonIdent * NUAliasBOM::createCarbonIdent(NUNet * net)
{
  CarbonIdent* ret = net->getIdent();
  if (! ret)
  {
    CarbonIdent * my_symtab_ident = NULL;
    my_symtab_ident   = new SymTabIdent(net->getNameLeaf(), 
                                        net->getBitSize());
    ret = storeCarbonIdent(net, my_symtab_ident);
  }
  return ret;
}

CarbonIdent * NUAliasBOM::createCarbonIdentBP(NUNet * net,
                                              const NUNetRefHdl* origNetRefHdl)
{
  CarbonExpr* backPointer = NULL;

  const NUNetRef& orig_net_ref = *(*origNetRefHdl);
  const NUNet* orig_net = orig_net_ref.getNet();
  const NUVectorNet* orig_vec_net = orig_net->castVectorNet();
  NU_ASSERT(orig_vec_net, orig_net);
  
  const ConstantRange* origFullRange = orig_vec_net->getDeclaredRange();
  NU_ASSERT(origFullRange, orig_vec_net);
  
  CarbonIdent* orig_symtab_ident = orig_net->getIdent();
  if (orig_symtab_ident == NULL)
  {
    CarbonIdent * orig_symtab_ident_tmp   = new SymTabIdent(orig_net->getNameLeaf(), 
                                              orig_net->getBitSize());
    
    // This only needs to be done for the original identifiers
    orig_symtab_ident_tmp->putDeclaredRange(origFullRange);
    
    bool added = false;
    orig_symtab_ident = mExprFactory.createIdent(orig_symtab_ident_tmp, added);
    
    if (not added) {
      delete orig_symtab_ident_tmp;
    }
  }

  // this is not a good general solution. Technically, we could be
  // replacing several parts of a net with a single identifier. But,
  // currently, that is not the case.
  ConstantRange origRange;
  bool single_range = orig_net_ref.getRange(origRange);
  NU_ASSERT(single_range, orig_net_ref.getNet());

  if (origRange.getLength() == 1) {
    CarbonConst* bitIndex = mExprFactory.createConst(origRange.getMsb(), 32, true);
    backPointer = mExprFactory.createBinaryOp(CarbonExpr::eBiBitSel, orig_symtab_ident, bitIndex, 1, false, false);
  }
  else {
    backPointer = mExprFactory.createPartsel(orig_symtab_ident, origRange, origRange.getLength(), false);
  }

  NU_ASSERT(backPointer, net);
  
  return createCarbonIdentBP(net, backPointer);
}

CarbonIdent * NUAliasBOM::createCarbonIdentBP(NUNet * net,
                                              CarbonExpr* backPointerOrig)

{
  // Reduce the backpointer to actual design nets in case this is a
  // nested expression
  ExprReduceSymTab exprAnalyze(&mExprMap, &mExprFactory, net->getIdent(), ExprReduceSymTab::eThruBackPointer, NULL);
  CarbonExpr* backPointer = exprAnalyze.reduce(backPointerOrig);
  
  CarbonIdent* my_symtab_ident   = new SymTabIdentBP(net->getNameLeaf(), 
                                                     net->getBitSize(), 
                                                     backPointer);
  NUVectorNet* vec = net->castVectorNet();
  if (vec)
    my_symtab_ident->putDeclaredRange(vec->getDeclaredRange());
  
  return storeCarbonIdent(net, my_symtab_ident);
}

void NUAliasBOM::sanitizeExpressions()
{
  ExprReduceSymTab::IdentSet replacedIdents;
  CarbonIdentExprMap sanitized;
  for (CarbonIdentExprMap::UnsortedLoop p = mExprMap.loopUnsorted(); ! p.atEnd(); ++p)
  {
    CarbonIdent* ident = p.getKey();
    CarbonExpr* replacement = p.getValue();

    ExprReduceSymTab exprAnalyze(&mExprMap, &mExprFactory, ident, ExprReduceSymTab::eStandard, &replacedIdents);
    CarbonExpr* simplified = exprAnalyze.reduce(replacement);
    sanitized.mapExpr(ident, simplified);
  }

  // Run through the sanitized map and find aliasing opportunities
  for (CarbonIdentExprMap::UnsortedLoop p = sanitized.loopUnsorted(); ! p.atEnd(); ++p)
  {
    CarbonIdent* ident = p.getKey();
    CarbonExpr* replacement = p.getValue();
    SymTabIdent* replIdent = replacement->castSymTabIdent();
    if (replIdent)
    {
      // aliasing opportunity
      STAliasedLeafNode* replIdentNode = replIdent->getNode();
      SymTabIdent* origIdent = ident->castSymTabIdent();
      CE_ASSERT(origIdent, ident);
      
      STAliasedLeafNode* identNode = origIdent->getNode();
      
      replIdentNode->linkAlias(identNode);
      
      // now place the ident in the expr removal list
      replacedIdents.insert(ident);
    }
  }

  // now that we have a somewhat sanitized expression map, we need to
  // remove the nested replacement idents. Those are in the
  // replacedIdents map
  for (ExprReduceSymTab::IdentSet::UnsortedLoop p = replacedIdents.loopUnsorted(); ! p.atEnd(); ++p)
  {
    CarbonIdent* doomedIdent = *p;
    sanitized.removeMappedExpr(doomedIdent);
  }
  
  // Now swap our expression map with the sanitized one.
  mExprMap.swap(sanitized);
}



//! Return BOM class name
const char* NUAliasBOM::getClassName() const
{
  return "NUAliasBOM";
}

//! Write the BOMData for a branch node
void NUAliasBOM::xmlWriteBranchData(const STBranchNode* branch,UtXmlWriter* xmlWriter) const
{
  NUAliasDataBOM *dataBom = NUAliasBOM::castBOM (branch->getBOMData ());
  if(dataBom == NULL) {
    return;
  }
  dataBom->writeXml(xmlWriter);

}

//! Write the BOMData for a leaf node
void NUAliasBOM::xmlWriteLeafData(const STAliasedLeafNode* leaf ,UtXmlWriter* xmlWriter) const
{
  NUAliasDataBOM *dataBom = NUAliasBOM::castBOM (leaf->getBOMData ());
  if(dataBom == NULL) {
    return;
  }
  dataBom->writeXml(xmlWriter);
}


void NUAliasDataBOM::writeXml(UtXmlWriter* xmlWriter) const
{
      xmlWriter->WriteAttribute("Type", "NUAliasDataBOM*");

      NUNet* dataNet = NULL;
      NUScope* dataScope = NULL;
      bool hasNet = hasValidNet();
      if(hasNet) {
        dataNet = (NUNet*)getNet();
      }
      else {
        dataScope = (NUScope*)getScope();
      }

      if(dataNet != NULL) {
        xmlWriter->StartElement("Field");
        xmlWriter->WriteAttribute("Name",     "mNet");
        xmlWriter->WriteAttribute("Type",     "NUNet*");
        xmlWriter->WriteAttribute("Pointer",  (const void*) dataNet);
        xmlWriter->EndElement();
      }


      if(dataScope != NULL) {
        xmlWriter->StartElement("Field");
        xmlWriter->WriteAttribute("Name",  "mScope");
        xmlWriter->WriteAttribute("Type",  "NUScope");
        xmlWriter->WriteAttribute("Pointer",  (const void*) dataScope);
        xmlWriter->EndElement();
      }


      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mMySymtabIndex");
      xmlWriter->WriteAttribute("Type",     "SInt32");
      xmlWriter->WriteAttribute("Value",    mMySymtabIndex);
      xmlWriter->EndElement();

      xmlWriter->StartElement("Field");
      xmlWriter->WriteAttribute("Name",     "mNodeIdent");
      xmlWriter->WriteAttribute("Type",     "CarbonIdent*");
      xmlWriter->WriteAttribute("Pointer",  (const void*)mNodeIdent);
      xmlWriter->EndElement();

      return;
} 
