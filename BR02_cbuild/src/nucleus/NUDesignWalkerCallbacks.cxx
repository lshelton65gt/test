// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file

  This file contains some generally useful design walker callbacks so
  that they can be shared across optimizations.
*/

#include "nucleus/NUDesignWalkerCallbacks.h"
#include "nucleus/NULvalue.h"

NUDesignCallback::Status
NUGatherLvaluesCallback::operator()(Phase, NUBase*)
{
  return eNormal;
}

NUDesignCallback::Status
NUGatherLvaluesCallback::operator()(Phase phase, NUMemselLvalue* memsel)
{
  return handleLvalue(phase, memsel);
}

NUDesignCallback::Status
NUGatherLvaluesCallback::operator()(Phase phase, NUVarselLvalue* varsel)
{
  return handleLvalue(phase, varsel);
}

NUDesignCallback::Status
NUGatherLvaluesCallback::operator()(Phase phase, NUIdentLvalue* ident)
{
  return handleLvalue(phase, ident);
}

NUDesignCallback::Status
NUGatherLvaluesCallback::handleLvalue(Phase phase, NULvalue* lvalue)
{
  // If this is the pre phase possibly add it. We don't continue down
  // because we can have an NUVarselValue with an NUIdentLvalue as a
  // sub expression.
  if (phase == ePre) {
    if (lvalue->queryDefs(mNetRef, &NUNetRef::overlapsSameNet, mNetRefFactory)) {
      mLvalues->push_back(lvalue);
    }
    return eSkip;
  } else {
    return eNormal;
  }
}

NUDesignCallback::Status NUFindDynamicLvalues::operator()(Phase, NUBase*)
{
  return eNormal;
}

NUDesignCallback::Status
NUFindDynamicLvalues::operator()(Phase phase, NUVarselLvalue* varselLvalue)
{
  Status status = eNormal;
  if (phase == ePre) {
    if (!varselLvalue->isConstIndex()) {
      status = eStop;
    }
  }
  return status;
}

NUDesignCallback::Status
NUFindDynamicLvalues::operator()(Phase phase, NUMemselLvalue* memselLvalue)
{
  Status status = eNormal;
  if (phase == ePre) {
    if (!memselLvalue->isConstIndex(0)) {
      status = eStop;
    }
  }
  return status;
}

NUDesignCallback::Status NUFindDynamicLvalues::operator()(Phase, NUConcatLvalue*)
{
  return eStop;
}
