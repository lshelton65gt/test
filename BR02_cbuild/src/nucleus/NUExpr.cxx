// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUExpr.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetElabRef.h"
#include "nucleus/NULvalue.h"
#include "util/StringAtom.h"
#include "util/CarbonAssert.h"
#include "util/CbuildMsgContext.h"
#include "util/DynBitVector.h"
#include "util/UtConv.h"
#include "util/UtIOStream.h"
#include "util/CarbonTypeUtil.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUExprFactory.h"
#include "bdd/BDD.h"

#include "util/OSWrapper.h"
#include "codegen/codegen.h"
#include "codegen/CGOFiles.h"
#include "shell/carbon_shelltypes.h"

#include "NUExprI.h"

#undef SHORT_BIT
#undef LONG_BIT
#undef LLONG_BIT

#define SHORT_BIT 16
#define LONG_BIT 32
#define LLONG_BIT 64

/*!
  \file
  Implementation of NUExpr and derived classes.
*/


//! Replace an expression unless the replacement is a constant
// This is used for EdgeExpr, which cannot handle a (posedge 0)
// construct -- it gets handled differently in the scheduler
static bool sReplaceExprUnlessConstant(NUExpr** expr, NuToNuFn::Phase phase, NuToNuFn & translator)
{
  bool changed = false;

  /*
   * Currently, edge expressions cannot handle constants.  We want to
   * perform the fold and then decided based on the result whether to
   * accept the fold or keep the original expression.  The problem is
   * that NuToNuFn's are responsible for deleting the expression
   * passed in, unless it is a managed expression.
   *
   * The solution is to make a backup copy of the expression, then
   * call the NuToNuFn and decide whether to keep the original or the
   * translated version.  If we decide to keep the original, then we
   * must use the backup copy, as the translator will have already
   * deleted the original.
   *
   * We have to do it this way, rather than folding the copy, because
   * NUExprReplace needs to keep track of the expression-pointers it
   * collects, and copying/deleting will get it confused.
   *
   * Bug 2632 requests that the edge expression restrictions be fixed.
   * When that happens, this code can be simplified.
   */
  CopyContext cc(NULL,NULL);
  NUExpr* original = *expr;
  NUExpr* copy = original->copy(cc);
  NUExpr* replacement = translator(original, phase);
  if (replacement && (replacement != original) &&
      (replacement->castConst() == NULL))
  {
    // we have a non-constant replacement, use it
    *expr = replacement;
    changed = true;
    delete copy;
  }
  else
  {
    // we want to keep the original
    if (replacement) {
      delete replacement; // copy was deleted and replacement was suggested (but rejected)
      *expr = copy;

      // note that the pointer was changed, but the semantics of the expression
      // was not, so we will leave 'changed' false.  This may be a little confusing
      // because the pointer changed.  But the alternative is not great either, because
      // an algorithm that says "keep doing replaceLeaves until it settles" will
      // never finish if we return true when we are really just restoring a backup
      // copy.
    }
    else
      delete copy;        // copy was not deleted because no replacement was suggested
  }

  return changed;
} // static bool sReplaceExprUnlessConstant

//! Is this operator a relational operator
/*static*/ bool NUOp::isRelationalOperator (OpT op)
{
  return op >= eFirstRelational && op <= eLastRelational;
}

bool NUOp::isSignedOperator (OpT op)
{
  switch (op) {
  case eBiSMult:
  case eBiSDiv:
  case eBiSMod:
  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
    return true;
  default:
    return false;
  }
}

const char* NUOp::getOpChar(bool cStyle) const
{
  return convertOpToChar(mOp, cStyle);
}

//! this is a static method, it requires an op as an arg since some callers need a string  without having the NUOp
const char* NUOp::convertOpToChar(const OpT op, bool cStyle)
{
  switch (op)
  {
  case NUOp::eStart:       break;
  case NUOp::eUnBuf:       return "buf ";
  case NUOp::eUnPlus:      return "+";
  case NUOp::eUnMinus:     return "-";
  case NUOp::eUnLogNot:    return "!";
  case NUOp::eUnBitNeg:    return "~";
  case NUOp::eUnVhdlNot:   return "~";
  case NUOp::eUnRedAnd:    return "&";
  case NUOp::eUnRedOr:     return "|";
  case NUOp::eUnRedXor:    return "^";
  case NUOp::eUnFFO:	   return "FFO";
  case NUOp::eUnFFZ:	   return "FFZ";
  case NUOp::eUnFLO:       return "FLO";
  case NUOp::eUnFLZ:       return "FLZ";
  case NUOp::eUnCount:	   return "POPCOUNT";
  case NUOp::eUnAbs:       return "abs";
  case NUOp::eUnItoR:      return "$itor";
  case NUOp::eUnRtoI:      return "$rtoi";
  case NUOp::eUnRealtoBits:return "$realtobits";
  case NUOp::eUnBitstoReal:return "$bitstoreal";
  case NUOp::eUnChange:    return "CHANGE";
  case NUOp::eUnRound:     return "ROUND";
  case NUOp::eBiPlus:      return "+";
  case NUOp::eBiMinus:     return "-";
  case NUOp::eBiSMult:     return "* /*signed*/";
  case NUOp::eBiUMult:     return "*";
  case NUOp::eBiSDiv:      return "/ /*signed*/";
  case NUOp::eBiUDiv:      return "/";
  case NUOp::eBiSMod:      return "% /*signed*/";
  case NUOp::eBiUMod:      return "%";
  case NUOp::eBiVhdlMod:   return " mod ";
  case NUOp::eBiEq:        return "==";
  case NUOp::eBiNeq:       return "!=";
  case NUOp::eBiTrieq:     return (cStyle ? "==" : "===");
  case NUOp::eBiTrineq:    return (cStyle ? "!=" : "!==");
  case NUOp::eBiLogAnd:    return "&&";
  case NUOp::eBiLogOr:     return "||";
  case NUOp::eBiSLt:       return "< /*signed*/";
  case NUOp::eBiULt:       return "<";
  case NUOp::eBiSLte:      return "<= /*signed*/";
  case NUOp::eBiULte:      return "<=";
  case NUOp::eBiSGtr:      return "> /*signed*/";
  case NUOp::eBiUGtr:      return ">";
  case NUOp::eBiSGtre:     return ">= /*signed*/";
  case NUOp::eBiUGtre:     return ">=";
  case NUOp::eBiBitAnd:    return "&";
  case NUOp::eBiBitOr:     return "|";
  case NUOp::eBiBitXor:    return "^";
  case NUOp::eBiRoR:       return " ror ";
  case NUOp::eBiRoL:       return " rol ";
  case NUOp::eBiDExp:      return "** /*double*/";
  case NUOp::eBiExp:       return "**";
  case NUOp::eBiRshift:    return ">>";
  case NUOp::eBiLshift:    return "<<";
  case NUOp::eBiDownTo:    return "downto";
  case NUOp::eBiVhZxt:	   return " ZXT ";
  case NUOp::eBiVhExt:     return " EXT ";
  case NUOp::eBiVhLshift:  return " sll ";
  case NUOp::eBiVhRshift:  return " srl ";
  case NUOp::eBiVhLshiftArith: return " sla ";
    
  case NUOp::eBiLshiftArith: return (cStyle ? "<<" : "<<<");
  case NUOp::eBiVhRshiftArith: return " sra ";
  case NUOp::eBiRshiftArith: return (cStyle ? ">>" : ">>>");
  case NUOp::eTeCond:      return "?:";
  case NUOp::eNaConcat:    return "{}";
  case NUOp::eNaFopen:     return "$fopen";
  case NUOp::eNaLut:       return "LUT";
  case NUOp::eZEndFile:    return "ENDFILE";
  case NUOp::eZSysTime:    return "$time";
  case NUOp::eZSysStime:   return "$stime";
  case NUOp::eZSysRealTime: return "$realtime";

  case NUOp::eInvalid:     return "/* INVALID */";
  } // switch

  INFO_ASSERT (0, "operator with no printable name");
  return NULL;
}




NUExpr::NUExpr(const SourceLocator& loc) :
  mLoc(loc),
  mSignedResult(false),
  mIsManaged(false),
  mSizeMadeExplicit(false),
  mBitSize(0)
{
}

NUExpr::~NUExpr()
{
}

bool
NUExpr::sizeVaries () const { return false; }


NUExpr* NUExpr::constCast() const
{
  NU_ASSERT(isManaged(), this);
  return const_cast<NUExpr*>(this);
}

bool NUExpr::shallowEq(const NUExpr& exp) const
{
  if (this == &exp)
    return true;

  if (getType () != exp.getType ()
      || getBitSize() != exp.getBitSize ()
      || mSignedResult != exp.isSignedResult ()

      // Calling determineBitSize here is deadly for expr graphs created by BDDs
      /* || determineBitSize() != exp.determineBitSize() */)
    return false;

  return shallowEqHelper (exp);
}

bool NUExpr::shallowEqHelper (const NUExpr& exp) const
{
  return *this == exp;
}

ptrdiff_t NUExpr::compare(const NUExpr& exp,
                          bool cmpLocator,
                          bool cmpPointer,
                          bool cmpSizeOfConsts)
  const
{
  if (&exp == this) {
    return 0;
  }
  ptrdiff_t cmp = 0;
  if (cmpLocator) {
    cmp = SourceLocator::compare(getLoc(), exp.getLoc());
  }

  if (cmp == 0) {
    cmp = ((ptrdiff_t) getType() - (ptrdiff_t) exp.getType());
    if (cmp == 0) {
      // compareHelper is abstract virtual method overridden by each subclass
      cmp = compareHelper(&exp, cmpLocator, cmpPointer, cmpSizeOfConsts);

      if (cmp == 0) {
        cmp = (ptrdiff_t) mSizeMadeExplicit - 
          ((ptrdiff_t) exp.mSizeMadeExplicit);

        if ((cmp == 0) && (cmpSizeOfConsts || (castConst() == NULL))) {
          cmp = (ptrdiff_t) getBitSize() - (ptrdiff_t) exp.getBitSize();
          if (cmp == 0) {
            cmp = (ptrdiff_t)mSignedResult - (ptrdiff_t)exp.mSignedResult;
            if (cmp == 0) {
              cmp = ((ptrdiff_t) determineBitSize()) -
                ((ptrdiff_t) exp.determineBitSize());
            }
          }
        }
      }

      if ((cmp == 0) && cmpPointer) {
        cmp = carbonPtrCompare(this, &exp);
      }
    }
  }
  return cmp;
} // ptrdiff_t NUExpr::compare

const NUConst* NUExpr::castConstNoXZ() const { return NULL;}

const NUConst* NUExpr::castConstXZ() const { return NULL; }

void NUExpr::manage(NUExprFactory* factory)
{
  mIsManaged = (factory != NULL);
}

// Safe default - it's as big as you need it to be.
UInt32 NUExpr::effectiveBitSize (ExprIntMap* cache) const
{
  if (cache != NULL) {
    ExprIntMap::iterator p = cache->find(this);
    if (p != cache->end()) {
      return p->second;
    }
    UInt32 val = effectiveBitSizeHelper(cache);
    (*cache)[this] = val;
  }
  return effectiveBitSizeHelper(cache);
}

UInt32 NUExpr::effectiveBitSizeHelper (ExprIntMap*) const {
  return getBitSize ();
}

UInt32 NUExpr::getBitSize() const {
  if (mBitSize == 0) {
    (void) determineBitSize();
  }
  return mBitSize;
}

static bool sIsSizedByResize(const NUExpr* expr) {
  bool ret = false;
  if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(expr)) {
    NUOp::OpT op = bop->getOp();
    ret = (op == NUOp::eBiVhExt) || (op == NUOp::eBiVhZxt);
  }
  return ret;
}

void NUExpr::clearBitSizeCache() {
  if (!sIsSizedByResize(this)) {
    mBitSize = 0;
    mSizeMadeExplicit = false;
  }
}

UInt32 NUExpr::determineBitSize() const {
  if (mBitSize == 0) {
    mBitSize = determineBitSizeHelper();
  }
  return mBitSize;
}

void NUExpr::printSize(UtOStream& stream)
  const
{
  stream << "[size=" << determineBitSize() << ", context=" << mBitSize
	 << (mSignedResult ? (const char *)", signed" : (const char*)"")
         << (isManaged() ? ", managed" : (const char*)"")
	 << "] ";
}

NUNet* NUExpr::getWholeIdentifier() const
{
  NU_ASSERT("Not a whole identifier!" == 0, this);
  return 0;
}


NULvalue *NUExpr::Lvalue(const SourceLocator & /* loc -- unused */) const
{
  NU_ASSERT("Unsupported conversion expression to lvalue" == 0, this);
  return 0;
}

bool
NUExpr::isReal() const { return false; }

const NUConst* NUExpr::castConst() const { return NULL; }

NUExpr* NUExpr::makeSizeExplicit(UInt32 desired_size) {
  if (desired_size == 0) {
    desired_size = getBitSize();
  }

  if (mSizeMadeExplicit && (desired_size == mBitSize))
  {
    return this;              // determineBitSize() always == getBitSize()
  }

  NUExpr* ret = this;
  if (mIsManaged) {
    CopyContext cc(NULL, NULL);
    ret = copy(cc);
  }

  if ((mBitSize == 0) && sIsSizedByResize(this)) {
    mBitSize = desired_size;
  }

  ret = ret->makeSizeExplicitHelper(desired_size);

  {
    NU_ASSERT(ret->mBitSize == desired_size, this);

    // recurse into sub-expressions that have not yet been made
    // explicit.
    for (UInt32 i = 0, n = ret->getNumArgs(); i < n; ++i) {
      NUExpr* sub = ret->getArg(i);
      if (! sub->mSizeMadeExplicit) {
        ret->putArg(i, sub->makeSizeExplicit());
      }
    }
  }
  ret->mSizeMadeExplicit = true;
  return ret;
}


//! return an expression where the contex and self-determined sizes match the context size of 'this'
NUExpr* NUExpr::makeSizeExplicitHelper(UInt32 desired_size) {
  UInt32 self_determined_size = (mBitSize != 0)
    ? mBitSize
    : determineBitSize();

  NUExpr* ret_expr = this;
  if ( desired_size < self_determined_size ) {
    // partselect to the desired size
    ret_expr = new NUVarselRvalue (this,
                                   ConstantRange (desired_size-1, 0),
                                   mLoc);
    ret_expr->resize (desired_size);
    // could this ever be signed?
  } else if (desired_size > self_determined_size ) {
    if ( isSignedResult() ){
      // need to sign extend, but first check if the sub expression
      // already is a sign extension, no need to create two in a row
      NUBinaryOp* binOp = dynamic_cast<NUBinaryOp*>(this);
      if ((binOp != NULL) && (binOp->getOp() == NUOp::eBiVhExt)) {
        // Simple, just resize this expression. That way we don't have
        // an EXT of an EXT.
        resize(desired_size);
      } else {
        // Add a sign extension
        NUExpr* size_expr = NUConst::create(false, self_determined_size, 32, mLoc);
        ret_expr = new NUBinaryOp(NUOp::eBiVhExt, this, size_expr, mLoc);
        ret_expr->setSignedResult(true);
        ret_expr->resize(desired_size);
      }
    } else {
      // zero extend the self determined term
      SInt32 req_size = desired_size - self_determined_size;
      NUConst* pad = NUConst::create(false, 0, req_size, mLoc);
      NUExprVector new_vector(2);
      new_vector[0] = pad;
      if (ret_expr->mBitSize != self_determined_size) {
        if (ret_expr->mIsManaged) {
          CopyContext cc(NULL, NULL);
          ret_expr = ret_expr->copy(cc);
        }
        ret_expr->mBitSize = self_determined_size;
      }
      new_vector[1] = ret_expr;
      ret_expr = new NUConcatOp(new_vector, 1, mLoc);
      ret_expr->resize(desired_size);
      ret_expr->setSignedResult(mSignedResult);
    }
  }
  ret_expr->mBitSize = desired_size;
  return ret_expr;
} // NUExpr* NUExpr::makeSizeExplicitHelper

NUExpr* NUExpr::coerceOperandsToReal( ) {
  NUExpr* ret_expr = this;

  if ( not ret_expr->isReal() ) {
    // must get the size correct before we wrap with conversion to real so
    // that no precision is lost.
    ret_expr = ret_expr->makeSizeExplicit(64);
    ret_expr = new NUUnaryOp (NUOp::eUnItoR, ret_expr, ret_expr->getLoc ());
  }
  return ret_expr;
}

NUExpr* NUExpr::limitMemselIndexExpr(bool *was_truncated, MsgContext* msg_context, const SourceLocator &loc)
{
  if ( determineBitSize() > 32 ){
    // we only populate memory addresses of 32 bits or less, tell
    // user we are truncating (as is done by most simulators) then
    // create a partselected version of the address
    NU_ASSERT(msg_context, this);
    msg_context->MemoryIndexTruncation(&loc);
    NUExpr* index_expr = new NUVarselRvalue(this, ConstantRange(31,0), loc);
    index_expr->resize(32);
    *was_truncated = true;
    return index_expr;
  }
  return this;
}

void NUExpr::putSignedResult(bool v) {
  NU_ASSERT(!mIsManaged, this);
  mSignedResult = v;
}

void NUExpr::resize(UInt32 size) {
  if (!sIsSizedByResize(this)) {
    if (size != getBitSize()) {
      UtIO::cout() << "resize should never actually change the size.\n";
      UtIO::cout() << "see the doc for NUExpr::makeSizeExplicit.\n";
      UtIO::cout() << "size=" << size << " getBitSize="
                   << getBitSize() << "\n";
      NU_ASSERT(size == getBitSize(), this);
    }

    // Make the size explicit on all the sub-items
    NUExpr* check = makeSizeExplicit(size);
    NU_ASSERT(check == this, this);
  }
  else {
    MANAGED_SIZE(size);
    resizeHelper(size);
  }
}

NUExpr* NUExpr::copy(CopyContext &copy_context) const {
  NUExpr* expr = copyHelper(copy_context);
  expr->mBitSize = mBitSize;
  expr->setSignedResult(mSignedResult);
  expr->mSizeMadeExplicit = mSizeMadeExplicit;
  return expr;
}

NUExpr* NUExpr::resizeSubHelper(UInt32 size) {
  NUExpr* ret = this;

  if (getBitSize() != size) {
    if (mIsManaged) {
      CopyContext cc(NULL, NULL);
      ret = copy(cc);
    }

    ret = ret->makeSizeExplicit(size);
  }
  return ret;
}

const NUExpr* NUExpr::getArgHelper(UInt32) const {
  NU_ASSERT(0, this);
  return NULL;
}
  
UInt32 NUExpr::getNumArgs() const {
  return 0;
}
  
NUExpr* NUExpr::putArg(UInt32 index, NUExpr * argument) {
  clearBitSizeCache();
  return putArgHelper(index, argument);
}

NUExpr* NUExpr::putArgHelper(UInt32, NUExpr*) {
  NU_ASSERT(0, this);
  return NULL;
}
  

NUEdgeExpr::NUEdgeExpr(ClockEdge edge, NUExpr *expr, const SourceLocator& loc) :
  NUExpr(loc),
  mEdge(edge),
  mExpr(expr),
  mClock(false),
  mReset(false)
{
  NU_ASSERT(dynamic_cast<NUIdentRvalue*>(mExpr) != NULL, this);
  NU_ASSERT(expr->determineBitSize() == 1, expr);

  // edge expressions are always 1 bit.  Try totally flattening test/interp/fpu
  // and you'll find that it will assert without this.
  mBitSize = 1;                 
}

NUEdgeExpr::NUEdgeExpr(ClockEdge edge, const NUExpr *expr, const SourceLocator& loc) :
  NUExpr(loc),
  mEdge(edge),
  mExpr(expr->constCast()),
  mClock(false),
  mReset(false)
{
  NU_ASSERT(dynamic_cast<NUIdentRvalue*>(mExpr) != NULL, this);
  NU_ASSERT(expr->determineBitSize() == 1, expr);

  // edge expressions are always 1 bit.  Try totally flattening test/interp/fpu
  // and you'll find that it will assert without this.
  mBitSize = 1;                 
}


NUEdgeExpr::~NUEdgeExpr()
{
  if (!mIsManaged && mExpr->ownsSubExprs())
    delete mExpr;
}

void NUEdgeExpr::manage(NUExprFactory* factory)
{
  mIsManaged = true;
  mExpr = factory->insert(mExpr)->constCast();
}

NUExpr* NUEdgeExpr::copyHelper(CopyContext &copy_context) const
{
  NUEdgeExpr *expr = new NUEdgeExpr(mEdge, mExpr->copy(copy_context), mLoc);
  if (mClock) {
    expr->setIsClock();
  }
  if (mReset) {
    expr->setIsReset();
  }
  return expr;
}



NUExpr* NUEdgeExpr::copyOpposite() const
{
  ClockEdge opposite_edge = ClockEdgeOppositeEdge( mEdge );
  
  CopyContext copy_context(NULL,NULL);
  NUExpr *expr = new NUEdgeExpr(opposite_edge, mExpr->copy(copy_context), mLoc);
  expr->resize(getBitSize());
  return expr;
}


void NUEdgeExpr::getUses(NUNetSet *uses) const
{
  mExpr->getUses(uses);
}

void NUEdgeExpr::getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const
{
  mExpr->getUsesElab(scope, uses);
}

void NUEdgeExpr::getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const
{
  mExpr->getUsesElab(scope, uses);
}


void NUEdgeExpr::getUses(NUNetRefSet *uses) const
{
  mExpr->getUses(uses);
}


bool NUEdgeExpr::queryUses(const NUNetRefHdl &use_net_ref,
			   NUNetRefCompareFunction fn,
			   NUNetRefFactory *factory) const
{
  return mExpr->queryUses(use_net_ref, fn, factory);
}


bool NUEdgeExpr::replace(NUNet * old_net, NUNet * new_net)
{
  return mExpr->replace(old_net, new_net);
}


bool NUEdgeExpr::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // pre-recursion translation
  changed |= sReplaceExprUnlessConstant(&mExpr, NuToNuFn::ePre, translator);

  // recursion
  changed |= mExpr->replaceLeaves(translator);

  // post-recursion translation
  changed |= sReplaceExprUnlessConstant(&mExpr, NuToNuFn::ePost, translator);

  return changed;
}

UInt32 NUEdgeExpr::determineBitSizeHelper() const
{
  return 1;
}


void NUEdgeExpr::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  // Edge expressions are always sized 1.
  mBitSize = 1;
  mExpr = mExpr->resizeSubHelper(1);
}

NUNet* NUEdgeExpr::getNet() const
{
  if (mExpr->isWholeIdentifier()) {
    return mExpr->getWholeIdentifier();
  }
  NUNetSet net_set;
  mExpr->getUses(&net_set);
  NU_ASSERT(net_set.size() == 1, this);
  NUNet *net = *(net_set.begin());
  return net;
}


void NUEdgeExpr::setIsClock()
{
  if (not isReset()) {
    mClock = true;
  }
}


void NUEdgeExpr::setIsReset()
{
  if (not isClock()) {
    mReset = true;
  }
}


const char* NUEdgeExpr::typeStr() const
{
  return "NUEdgeExpr";
}


void NUEdgeExpr::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; ++i)
    screen << " ";
  mLoc.print();
  screen << "NUEdgeExpr(" << this << ") : ";
  screen << ClockEdgeString( mEdge ) << " ";
  if (mClock) {
    screen << "clock ";
  }
  if (mReset) {
    screen << "reset ";
  }
  screen << UtIO::endl;
  mExpr->print(recurse, numspaces+2);
}

NUExpr* NUEdgeExpr::makeSizeExplicitHelper(UInt32 desired_size) {
  NU_ASSERT(( desired_size == 1 ), this);
  
  return this;
}

NUExpr* NUEdgeExpr::coerceOperandsToReal( ) {
  NU_ASSERT(0, this);  // we should not be converting edge exprs to real
  return this;
}

UInt32 NUEdgeExpr::getNumArgs() const {
  return 1;
}

//! Return the specified argument; 0-based index, 0 is leftmost argument
const NUExpr* NUEdgeExpr::getArgHelper(UInt32 index) const {
  NU_ASSERT(index == 0, this);
  return mExpr;
}

//! Mutate the argument
NUExpr* NUEdgeExpr::putArgHelper(UInt32 index, NUExpr* expr) {
  NU_ASSERT(index == 0, this);
  NUExpr* old = mExpr;
  mExpr = expr;
  return old;
}


NUIdentRvalueElab::NUIdentRvalueElab(NUNetElab* net, const SourceLocator& loc)
  : NUIdentRvalue(net->getNet(), loc)
{
  mNetElab = net;
}


NUIdentRvalueElab::~NUIdentRvalueElab()
{
}

ptrdiff_t NUIdentRvalueElab::compareHelper(const NUExpr* exp, bool,
                                           bool, bool) const
{
  const NUIdentRvalueElab* that = dynamic_cast<const NUIdentRvalueElab*>(exp);
  NU_ASSERT(that, exp);
  ptrdiff_t cmp = NUElabBase::compare(mNetElab, that->mNetElab);
  if (cmp == 0) {
    cmp = NUNet::compare(getIdent(), that->getIdent());
  }
  return cmp;
}

NUExpr* NUIdentRvalueElab::copyHelper(CopyContext &) const
{
  NUIdentRvalueElab *expr = new NUIdentRvalueElab(mNetElab, mLoc);
  return expr;
}


const char* NUIdentRvalueElab::typeStr() const
{
  return "NUIdentRvalueElab";
}


void NUIdentRvalueElab::print(bool /* recurse_arg -unused */, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << "IdentRvalueElab(" << this << ") ";
  printSize(screen);
  screen << UtIO::endl;
  mNetElab->print(false, numspaces+2);
}


NUIdentRvalue::NUIdentRvalue(NUNet *net, const SourceLocator& loc) :
  NUExpr(loc)
{
  mNet = net;

  // VHDL positive subranges of integer are treated as signed values.
  // but they will also be sized as 32 bit objects once fetched.
  mSignedResult = (net->isSigned () || net->isSignedSubrange ())
    and not net->is2DAnything ();
}


NUIdentRvalue::~NUIdentRvalue()
{
}


NUNetElab* NUIdentRvalue::getNetElab(STBranchNode* scope) const
{
  if (scope == NULL)
    return NULL;
  return mNet->lookupElab(scope);
}


NUExpr* NUIdentRvalue::copyHelper(CopyContext & copy_context) const
{
  NUNet * net = copy_context.lookup(mNet);
  NUIdentRvalue *expr = new NUIdentRvalue(net, mLoc);
  return expr;
}


NULvalue *NUIdentRvalue::Lvalue(const SourceLocator &loc) const
{
  return new NUIdentLvalue(mNet, loc);
}

UInt32 NUIdentRvalue::effectiveBitSizeHelper (ExprIntMap*) const
{
  if (mNet->is2DAnything ())
    return mNet->getBitSize ();

  if (mNet->isUnsignedSubrange ())
    return mNet->getBitSize ();

  return isSignedResult () ? getBitSize () : determineBitSize ();
}

UInt32 NUIdentRvalue::determineBitSizeHelper() const
{
  return mNet->getBitSize();
}


//! build a single-bit expression for use as a condition (For, If, Case)
NUExpr* NUExpr::makeConditionExpr() {
  // put in implemenation from Populate*.cxx
  // we can't just set the verilog size of the condition like this:
  //   expr = expr->makeSizeExplicit(1);
  // because if the user has written if(x[3:0]) ... what
  // we really want for the condition is if(x[3:0]!=0)
  UInt32 condWidth = determineBitSize();
  NUExpr* expr = this;
  if (condWidth != 1) {
    NUExpr* zero = NUConst::create(false, 0, condWidth, mLoc);
    expr = new NUBinaryOp(NUOp::eBiNeq, this, zero, mLoc);
  }
  expr->resize(1);
  return expr;
}

void NUIdentRvalue::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  mBitSize = size;
}

void NUIdentRvalue::getUses(NUNetSet *uses) const
{
  uses->insert( getIdent() );
}

void NUIdentRvalue::getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const
{
  // scope is passed as NULL from SCHClkAnal::getProceduralAssignment because
  // the idents should already have been elaborated by that point.
  NU_ASSERT(scope, this);
  uses->insert( getIdent()->lookupElab(scope) );
}

void NUIdentRvalue::getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses)
  const
{
  // scope is passed as NULL from SCHClkAnal::getProceduralAssignment because
  // the idents should already have been elaborated by that point.
  NU_ASSERT(scope, this);
  NUNet* net = getIdent();
  uses->insert(net->lookupElab(scope));
}

void NUIdentRvalueElab::getUsesElab(STBranchNode*, NUNetElabSet *uses) const
{
  uses->insert( mNetElab );
}

void NUIdentRvalueElab::getUsesElab(STBranchNode*, NUNetElabRefSet2 *uses)
  const
{
  uses->insert( mNetElab );
}

void NUIdentRvalue::getUses(NUNetRefSet *uses) const
{
  uses->insert( uses->getFactory()->createNetRef( getIdent() ) );
}


bool NUIdentRvalue::queryUses(const NUNetRefHdl &use_net_ref,
			      NUNetRefCompareFunction fn,
			      NUNetRefFactory *factory) const
{
  NUNetRefHdl net_ref = factory->createNetRef(getIdent());
  return ((*net_ref).*fn)(*use_net_ref);
}


bool NUIdentRvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  if ( mNet == old_net ) {
    mNet = new_net;
    changed = true;
  }
  return changed;
}

bool NUIdentRvalue::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // leaf node; only perform local replacement.
  NUNet * replacement = translator(mNet, NuToNuFn::ePrePost);
  if ( replacement ) {
    mNet = replacement;
    changed = true;
  }
  return changed;
}

// Replace an expression, including one that represents an identifier,
// using the netToExpr callback.
//
// This can only be done if the identifier is an NUIdentRvalue, but it
// might get replaced with something that is not an NUIdentRvalue.
// This is intended to be used with bitsels and partsels.  So it
// returns information about how to normalize the bitsel/partsel
// offset to 0 in &lsb.
bool
NUExpr::replaceExpr(NUExpr** ident, NuToNuFn::Phase phase,
                    NuToNuFn& translator)
{
  bool changed = false;
  NUExpr * repl = translator(*ident, phase);

  if ((repl == NULL) && (*ident)->isWholeIdentifier())
  {
    NUNet* net = (*ident)->getWholeIdentifier();
    NU_ASSERT(net, (*ident));
    NUIdentRvalueElab* ire = dynamic_cast<NUIdentRvalueElab*>(*ident);
    if (ire != NULL) {
      NUNetElab* netElab = ire->getNetElab(NULL);
      repl = translator.netElabToExpr(netElab, phase);
    } else {
      repl = translator.netToExpr(net, phase);
    }
    // the net translators can't delete the ident, so do it now
    if (repl)
      delete (*ident);
  }

  if (repl != NULL) {
    *ident = repl;
    changed = true;
  }

  return changed;
} // bool replaceExpr

NUExpr* NUExpr::translate(NuToNuFn & translator) 
{
  // Call the translator on ourselves.
  NUExpr * replacement = translator(this,NuToNuFn::ePre);
  // If we have a replacement, return it.
  if (replacement) return replacement;

  replacement = this;
  if (replaceExpr(&replacement, NuToNuFn::ePre, translator)) {
    return replacement;
  }

  // Recurse through.
  replaceLeaves(translator);
  // Translate ourselves on the way out.
  replacement = translator(this,NuToNuFn::ePost);
  // Return the replacement if it exists.
  if (replacement) return replacement;

  replacement = this;
  if (replaceExpr(&replacement, NuToNuFn::ePre, translator)) {
    return replacement;
  }

  return this;
}

const char* NUIdentRvalue::typeStr() const
{
  return "NUIdentRvalue";
}


void NUIdentRvalue::print(bool /* recurse_arg -unused */, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << "IdentRvalue(" << this << ") ";
  printSize(screen);
  screen << UtIO::endl;
  mNet->print(false, numspaces+2);
}

// The representation of varsel is not canonical -- this was the way Al wanted
// to do it because, I think, he thought it would make a Strip-mining
// optimization easier.  This routine enforces a canonicalization over the
// redundant representation.
// 
// So v[expr +: width] is represented not as
//   {NUExpr* mIdent; NUExpr* mExpr; UInt32 mWidth;}
// but instead as
//   {NUExpr* mIdent; ConstantRange mRange; NUExpr* mExpr}
// where mRange.getLength()==width.
//
// This means that the representation of, say, v[3] could be
//   {mIdent==v, mRange==[3:3], mExpr==0)
//   {mIdent==v, mRange==[2:2], mExpr==1)
//   {mIdent==v, mRange==[1:1], mExpr==2)
//   {mIdent==v, mRange==[0:0], mExpr==3)'
// To enforce a canonical representation, fixConstExpr must be called
// from every constructor for NUVarselRvalue.  This transforms any of 
// the above four forms to the first one:
//   {mIdent==v, mRange==[3:3], mExpr==0)
// The rule is that    if   mExpr->castConst() != NULL, 
//                     then mExpr->castConst()->isZero() == true
// 
// It should be possible, for anyone so inclined, to change the representation
// of NUVarselRvalue to its canonical form, and provide a method that returns
// a ConstantRange, which would be valid to call if mExpr->isConst()==true, it
// shouldn't be hard to deal with that, although the getRange() method should
// be changed to return a ConstantRange by value, rather than by pointer.  That
// would cause minor edits to large numbers of callers, but it should be pretty
// easy (a little time-consuming) to do that using Emacs next-error.
bool NUVarselRvalue::fixConstExpr() {
  UInt32 exprSize = 0;
  if (isConstIndex ())
  {
    mIndexInBounds = true;
    SInt32 off = getConstIndex ();

    if (off)                    // Non-zero?
    {
      bool wasSigned = mExpr->isSignedResult ();
      mRange.adjust (off);
      if (!mExpr->isManaged()) {
        delete mExpr;
      }
      mExpr = NUConst::create (wasSigned, 0,  LONG_BIT, mLoc);
      return true;
    }
    // Catch silly things like 66'b0 as an index
    exprSize = std::min ((UInt32)LONG_BIT, mExpr->determineBitSize ());
  }
  else {
    exprSize = mExpr->determineBitSize ();
  }
  mExpr = mExpr->resizeSubHelper(exprSize);

  return false;
} // NUVarselRvalue::fixConstExpr

static void sVarselCheck(const NUNet* net)
{
  // Should use memsel's for memories
  NU_ASSERT(!net->is2DAnything(), net);
}

static void sVarselCheck(const NUExpr* netExpr)
{
  // Should use memsel's for memories. Note that we exclude any
  // expressions but the ident lvalue. But this could be a memsel that
  // does not access enough dimensions. Not sure how to check for that
  // yet.
  if ((netExpr->getType() == NUExpr::eNUIdentRvalue) &&
      netExpr->isWholeIdentifier()) {
    NUNet* net = netExpr->getWholeIdentifier();
  NU_ASSERT(!net->is2DAnything(), netExpr);
  }
}

//Bitsel
NUVarselRvalue::NUVarselRvalue(NUNet *net,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(new NUIdentRvalue(net, loc)), mExpr(expr), mRange (ConstantRange (0,0)),
  mIndexInBounds (false)
{
  sVarselCheck(net);
  mSignedResult = false;        // Verilog LRM
  fixConstExpr ();
}

NUVarselRvalue::NUVarselRvalue(NUExpr *net,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(net), mExpr(expr), mRange (ConstantRange (0,0)), mIndexInBounds (false)
{
  sVarselCheck(net);
  mSignedResult = false;        // Verilog LRM
  fixConstExpr ();
}

// Full A[x+:k] constructor
NUVarselRvalue::NUVarselRvalue(const NUExpr *net,
                               const NUExpr *expr,
                               const ConstantRange &range,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(net->constCast()), mExpr(expr->constCast()), mRange (range),
  mIndexInBounds (false)
{
  sVarselCheck(net);
  mSignedResult = false;        // Verilog LRM
  fixConstExpr ();
}

NUVarselRvalue::NUVarselRvalue(NUExpr *net,
                               NUExpr *expr,
                               const ConstantRange &range,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(net), mExpr(expr), mRange (range), mIndexInBounds (false)
{
  sVarselCheck(net);
  mSignedResult = false;        // Verilog LRM
  fixConstExpr ();
}

NUVarselRvalue::NUVarselRvalue(const NUExpr *net,
                               const NUExpr *expr,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(net->constCast()), mExpr(expr->constCast()), mRange (ConstantRange (0,0)),
  mIndexInBounds (false)
{
  sVarselCheck(net);
  mSignedResult = false;        // Verilog LRM
  fixConstExpr ();
}

NUVarselRvalue::NUVarselRvalue(NUNet *net,
                               NUExpr* index,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(new NUIdentRvalue(net, loc)), mExpr (index), mRange(range),
  mIndexInBounds (false)
{
  mSignedResult = false;        // Verilog LRM
  fixConstExpr ();
}


NUVarselRvalue::NUVarselRvalue(NUNet *net,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(new NUIdentRvalue(net, loc)),
  mExpr (NUConst::create (false, 0,  LONG_BIT, loc)), mRange(range), mIndexInBounds (true)
{
  mSignedResult = false;        // Verilog LRM
}


NUVarselRvalue::NUVarselRvalue(NUExpr *expr,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(expr),
  mExpr (NUConst::create (false, 0,  LONG_BIT, loc)), mRange(range), mIndexInBounds (true)
{
  mSignedResult = false;        // Verilog LRM
}

NUVarselRvalue::NUVarselRvalue(const NUExpr *expr,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(expr->constCast()),
  mExpr (NUConst::create (false, 0,  LONG_BIT, loc)),
  mRange(range), mIndexInBounds (true)
{
  sVarselCheck(expr);
  mSignedResult = false;        // Verilog LRM
}


NUNet* NUVarselRvalue::getIdentNet(bool noArray) const
{
  if (mIdent->isWholeIdentifier())
    return mIdent->getWholeIdentifier();

  NUMemselRvalue* memsel = dynamic_cast<NUMemselRvalue*>(mIdent);
  if (memsel)
  {
    NU_ASSERT((not noArray), memsel); // getIdentNet called on arraySelect with noArray==true
    return memsel->getIdent();
  }

  // not a net or memsel -- expression or constant?
  return NULL;
}

bool NUVarselRvalue::isArraySelect() const
{
  NUMemselRvalue* memsel = dynamic_cast<NUMemselRvalue*>(mIdent);
  return (memsel != NULL);
}

void NUVarselRvalue::manage(NUExprFactory* factory)
{
  mIsManaged = true;
  mExpr = factory->insert(mExpr)->constCast();
  mIdent = factory->insert(mIdent)->constCast();
}

NULvalue *NUVarselRvalue::Lvalue(const SourceLocator &loc) const
{
  CopyContext ctx(0,0);
  NUVarselLvalue* v = new NUVarselLvalue(mIdent->Lvalue(loc), mExpr->copy(ctx), mRange, loc);
  v->putIndexInBounds (mIndexInBounds);
  return v;
}


NUNetElab* NUVarselRvalue::getNetElab(STBranchNode* scope) const {
  return mIdent->getNetElab(scope);
}

NUExpr* NUVarselRvalue::copyHelper(CopyContext &copy_context) const
{
  NUExpr* id = mIdent->copy(copy_context);
  NUExpr* bit = mExpr->copy(copy_context);
  NUVarselRvalue* expr = new NUVarselRvalue(id, bit, *getRange (), mLoc);
  expr->putIndexInBounds (mIndexInBounds);
  return expr;
}


bool NUVarselRvalue::isSimpleConstantVarsel() const
{
  return (mIdent->isWholeIdentifier() and mExpr->isConstant());
}


bool NUVarselRvalue::isConstIndex() const
{
  return mExpr->isConstant();
}

SInt32 NUVarselRvalue::getConstIndex() const
{
  int index;
  NUConst *c = mExpr->castConst();
  NU_ASSERT (c, mExpr);
  c->getL( &index );
  return index;
}

void NUVarselRvalue::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  mBitSize = size;
  // Select expression size is self-determined
  mIdent = mIdent->resizeSubHelper(mIdent->determineBitSize());
  mExpr = mExpr->resizeSubHelper(mExpr->determineBitSize());
}

NUExpr* NUVarselRvalue::makeSizeExplicitHelper(UInt32 desired_size) {
  // Michael, comment this code out and run
  //    cbuild -q test/case_inference/lhs-slice-07.v -newSizing
  // to see the bug I'm looking for.  This code avoids a vectorization
  // alert the issue by simplifying in[14:0][3:0] into in[3:0], which
  // is a good thing anyway.

  // I think this should be OK even for a dynamic part-select.  It will
  // just reduce the range we get starting at the dynamic offset.
  if (desired_size < mRange.getLength()) {
    // Just mutate the range to match the desired size
    mRange.setMsb(mRange.getLsb() + desired_size - 1);
    mBitSize = desired_size;
  }

  // Do not expand the mRange to accomodate a larger desired_size.
  // We need to let the default implementation concat with 0s instead,
  // rather than exposing bits that were masked off due to context.

  mIdent = mIdent->makeSizeExplicit();
  mExpr = mExpr->makeSizeExplicit();

  return NUExpr::makeSizeExplicitHelper(desired_size);
}

NUExpr* NUVarselRvalue::coerceOperandsToReal( ) {
  NUExpr* ret_expr = this;
  if ( not ret_expr->isReal() ) {
    ret_expr = new NUUnaryOp (NUOp::eUnItoR, ret_expr, ret_expr->getLoc ());
  }
  return ret_expr->NUExpr::coerceOperandsToReal();
}


bool NUVarselRvalue::isWholeIdentifier() const
{
  if (not mIdent->isWholeIdentifier())
    return false;

  if (not isConstIndex ()
      || getConstIndex () != 0)
    return false;

  if (getRange ()->getLsb () != 0
      || getRange ()->getLength () != getIdent ()->getBitSize ())
    return false;

  return true;
}

NUNet* NUVarselRvalue::getWholeIdentifier() const
{
  NU_ASSERT(isWholeIdentifier(), this);
  return getIdent();
}


void NUVarselRvalue::getUses(NUNetSet *uses) const
{
  mExpr->getUses(uses);
  mIdent->getUses(uses);
}

void NUVarselRvalue::getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const
{
  mExpr->getUsesElab(scope, uses);
  mIdent->getUsesElab(scope, uses);
}

void NUVarselRvalue::getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses)
  const
{
  mExpr->getUsesElab(scope, uses);
  mIdent->getUsesElab(scope, uses);
}

void NUVarselRvalue::getUses(NUNetRefSet *uses) const
{
  NUNet*          scalar = NULL;
  NUVectorNet*    vect   = NULL;
  NUMemselRvalue* memsel = NULL;

  if (isArraySelect())
  {
    memsel = dynamic_cast<NUMemselRvalue*>(mIdent);
    NU_ASSERT(memsel, mIdent);
  }
  else
  {
    vect = dynamic_cast<NUVectorNet*>(getIdentNet());
    if (!vect)
      scalar = getIdentNet();
  }

  if (isConstIndex())
  {
    ConstantRange r(mRange);
    if (vect)
      uses->insert(uses->getFactory()->createVectorNetRef(vect, r));
    else if (scalar)
      uses->insert(uses->getFactory()->createNetRef(scalar));
    else if (memsel)
      memsel->getUses(uses);
    else 
      mIdent->getUses(uses); // uses for an arbitrary identifier expression.
  } else {
    if (vect)
      uses->insert(uses->getFactory()->createVectorNetRef(vect));
    else if (scalar)
      uses->insert(uses->getFactory()->createNetRef(scalar));      
    else if (memsel)
      memsel->getUses(uses);
    else 
      mIdent->getUses(uses); // uses for an arbitrary identifier expression.
    mExpr->getUses(uses); // include the uses of the select expression
  }

}


bool NUVarselRvalue::queryUses(const NUNetRefHdl &use_net_ref,
                               NUNetRefCompareFunction fn,
                               NUNetRefFactory *factory) const
{
  if (mExpr->queryUses(use_net_ref, fn, factory)) {
    return true;
  }

  NUVectorNet* vect = dynamic_cast<NUVectorNet*>(getIdentNet(false));
  if (vect)
  {
    if (isConstIndex ())
    {
      ConstantRange r (mRange);
      
      NUNetRefHdl net_ref = factory->createVectorNetRef(vect, r);
      return ((*net_ref).*fn)(*use_net_ref);
    } else {
      NUNetRefHdl net_ref = factory->createVectorNetRef(vect);
      return ((*net_ref).*fn)(*use_net_ref);
    }
  }

  // Bitselect of a scalar, seems to be allowed
  NUNet* scalar = dynamic_cast<NUBitNet*>(getIdentNet(false));
  if (scalar) {
    NUNetRefHdl net_ref = factory->createNetRef(scalar);
    return ((*net_ref).*fn)(*use_net_ref);
  }

  // Not a vector or scalar net
  return mIdent->queryUses(use_net_ref, fn, factory);
}


bool NUVarselRvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = mIdent->replace(old_net, new_net);
  changed |= mExpr->replace(old_net,new_net);
  return changed;
}

bool NUVarselRvalue::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // replace expr
  changed |= replaceExpr(&mExpr, NuToNuFn::ePre, translator);
  changed |= mExpr->replaceLeaves(translator);
  changed |= replaceExpr(&mExpr, NuToNuFn::ePost, translator);

  // replace ident
  changed |= replaceExpr(&mIdent, NuToNuFn::ePre, translator);
  changed |= mIdent->replaceLeaves(translator);
  changed |= replaceExpr(&mIdent, NuToNuFn::ePost, translator);

  changed |= fixConstExpr ();
  return changed;
} // bool NUVarselRvalue::replaceLeaves

const char* NUVarselRvalue::typeStr() const
{
  return "NUVarselRvalue";
}


void NUVarselRvalue::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << "VarselRvalue(" << this << ") ";
  printSize(screen);
  screen << " : ";
  mRange.print ();
  screen << UtIO::endl;
  mIdent->print(recurse, numspaces+2);

  // Note that this expression is normalized, and will not in general
  // correspond to the original verilog index.  However, the compose
  // method will denormalize.
  mExpr->print(recurse, numspaces+2);
}

NUExpr* NUExpr::catenate(const NUExpr*, CopyContext &) const {
  return NULL;
}

// Create a simplified catenation of these expressions (if possible).
/* Bit/part selects are combined.  a[2:1],a[0] => a[2:0]
 */
NUExpr* NUVarselRvalue::catenate(const NUExpr* rop, CopyContext &copy_context)
  const
{
  if(rop->getType() != eNUVarselRvalue)
    return NULL;

  const NUVarselRvalue *p = dynamic_cast<const NUVarselRvalue *>(rop);
  if (not mExpr->equal (*(p->getIndex ())))
    return NULL;                // Not same bit offsets

  const int l_lsb = getRange()->getLsb();
  const int l_msb = getRange()->getMsb();
  int r_msb;
  int r_lsb;
  const NUExpr *r_ident;

  r_msb = p->getRange()->getMsb();
  r_lsb = p->getRange()->getLsb();
  r_ident = p->getIdentExpr();

  if( *mIdent != *r_ident )
    return NULL;

  if( l_lsb != (r_msb + 1) )
    return NULL;

  // We have adjacent selects.  Create the simplified expression.
  UInt32 size = getBitSize () + rop->getBitSize ();
  ConstantRange new_range(l_msb, r_lsb);
  NUExpr *ni = mIdent->copy( copy_context );
  NUExpr *nx = mExpr->copy( copy_context );
  NUExpr *e = new NUVarselRvalue( ni, nx, new_range, getLoc() );

  e = e->makeSizeExplicit(size);

  return e;
}


NUVarselRvalue::~NUVarselRvalue()
{
  if (!mIsManaged)
  {
    if (mIdent->ownsSubExprs())
      delete mIdent;

    if (mExpr->ownsSubExprs ())
      delete mExpr;
  }
}

UInt32 NUVarselRvalue::effectiveBitSizeHelper (ExprIntMap* cache) const
{
  if (this->isSignedResult ())
    return getBitSize ();

  return std::min (mRange.getLength (), mIdent->effectiveBitSize (cache));
}

UInt32 NUVarselRvalue::determineBitSizeHelper() const
{
  return mRange.getLength();
}

// Can we guarantee that the indexing expression is in-bounds?
bool NUVarselRvalue::isIndexInBounds () const
{
  if (mIndexInBounds)
    return true;                // yup - someone's guaranteed it

  if (isConstIndex ())
    return true;

  // Here's where we should do complex analysis of the size of the
  // ident vs. the number of bits in the index.  [But for now, just
  // say we don't know..]
  
  return false;
}

void NUVarselRvalue::putIndexInBounds (bool b)
{
  mIndexInBounds = b;

  // I think it shouldn't be necessary to invalidate the mBitSize cache
  // when this method is called, but to be on the safe side...
  clearBitSizeCache();
}

UInt32 NUVarselRvalue::getNumArgs() const {
  return 2;
}

//! Return the specified argument; 0-based index, 0 is leftmost argument
const NUExpr* NUVarselRvalue::getArgHelper(UInt32 index) const {
  NU_ASSERT(index <= 1, this);
  return (index == 0) ? mIdent : mExpr;
}

//! Mutate the argument
NUExpr* NUVarselRvalue::putArgHelper(UInt32 index, NUExpr* expr) {
  NU_ASSERT(index <= 1, this);
  if (index == 0) {
    std::swap(expr, mIdent);
  }
  else {
    std::swap(expr, mExpr);
  }
  return expr;
}

NUMemselRvalue::NUMemselRvalue(NUNet *net,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(new NUIdentRvalue(net, loc)), mResynthesized(false)
{
  NU_ASSERT(net->is2DAnything(), net);
  NU_ASSERT((expr->determineBitSize() <= 32), expr); // cbuild only supports index expressions of 32 bits or fewer
  mExprs.push_back( expr );
  mSignedResult = net->isSigned () || net->isSignedSubrange ();
}

NUMemselRvalue::NUMemselRvalue(NUNet *net,
                               NUExprVector *exprs,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(new NUIdentRvalue(net, loc)), mResynthesized(false)
{
  NU_ASSERT(net->is2DAnything(), net);
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
  mSignedResult = net->isSigned () || net->isSignedSubrange ();

}

NUMemselRvalue::NUMemselRvalue(NUExpr *netElab,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(netElab), mResynthesized(false)
{
  NU_ASSERT((expr->determineBitSize() <= 32), expr); // cbuild only supports index expressions of 32 bits or fewer
  mExprs.push_back( expr );
  mSignedResult = netElab->isSignedResult ();
}

NUMemselRvalue::NUMemselRvalue(NUExpr *netElab,
                               NUExprVector *exprs,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(netElab), mResynthesized(false)
{
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
  mSignedResult = netElab->isSignedResult ();
}

NUMemselRvalue::NUMemselRvalue(const NUExpr *netElab,
                               const NUExpr *expr,
                               const SourceLocator& loc) :
  NUExpr(loc), mIdent(netElab->constCast()), mResynthesized(false)
{
  NU_ASSERT((expr->determineBitSize() <= 32), expr); // cbuild only supports index expressions of 32 bits or fewer
  mExprs.push_back( expr->constCast() );
  mSignedResult = netElab->isSignedResult ();
}

NUMemselRvalue::~NUMemselRvalue()
{
  if (!mIsManaged)
  {
    if (mIdent->ownsSubExprs())
      delete mIdent;
    const UInt32 numDims = mExprs.size();
    for ( UInt32 i = 0; i < numDims; ++i )
    {
      if (mExprs[i]->ownsSubExprs())
        delete mExprs[i];
    }
  }
}


void 
NUMemselRvalue::putResynthesized( bool resynth )
{
  mResynthesized = resynth;
  clearBitSizeCache();
}

//! prepend (inject) another index expression to the memsel
void
NUMemselRvalue::prependIndexExpr( NUExpr *expr )
{
  mExprs.insert( mExprs.begin(), expr );
  clearBitSizeCache();// Clear bitsize cache since adding an index might changes it.
}

//! Add another index expression to the memsel
void
NUMemselRvalue::addIndexExpr( NUExpr *expr )
{
  mExprs.push_back( expr );
  // Clear bitsize cache since adding an index changes it.
  clearBitSizeCache();
}


void NUMemselRvalue::manage(NUExprFactory* factory)
{
  mIsManaged = true;
  mIdent = factory->insert(mIdent)->constCast();
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i] = factory->insert(mExprs[i])->constCast();
  }
}


NULvalue *NUMemselRvalue::Lvalue(const SourceLocator &loc) const
{
  if (mIdent->getType() != eNUIdentRvalue) {
    NU_ASSERT("Unsupported conversion expression to lvalue" == 0, this);
    return 0;
  }

  CopyContext ctx(0,0);
  NUExprVector newVec;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    newVec.push_back( mExprs[i]->copy(ctx) );
  }
  NUMemselLvalue *ret = new NUMemselLvalue(mIdent->getWholeIdentifier(), &newVec, loc);
  ret->putResynthesized( mResynthesized );
  return ret;
}


//! Return the identifier
NUNetElab* NUMemselRvalue::getNetElab(STBranchNode* scope) const {
  return mIdent->getNetElab(scope);
}

NUExpr* NUMemselRvalue::copyHelper(CopyContext &copy_context) const
{
  NUExprVector newVec;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    newVec.push_back( mExprs[i]->copy(copy_context) );
  }
  NUMemselRvalue* expr = new NUMemselRvalue(mIdent->copy(copy_context),
                                            &newVec, mLoc);
  expr->putResynthesized( mResynthesized );
  return expr;
}


UInt32 NUMemselRvalue::determineBitSizeHelper() const
{
  if (mIdent->isWholeIdentifier ()) {
    const NUNet *net = mIdent->getWholeIdentifier ();
    const NUMemoryNet* memnet = net->getMemoryNet ();

    NU_ASSERT (memnet, this);

    UInt32 size = 1;
    if (not memnet->isResynthesized()) { // (not mResynthesized)
      // this might be a slice; the size is the product of the indices
      // that aren't referenced in the Memsel

      for(UInt32 numDim = memnet->getNumDims () - mExprs.size ();
          numDim > 0;
          --numDim)
        size *= memnet->getRangeSize (numDim-1);
    } else {
      size = memnet->getRowSize ();
    }

    return size;
  }

  // Unexpected situation here.
  NU_ASSERT (0, this);
  return mIdent->getBitSize();
}

UInt32 NUMemselRvalue::effectiveBitSizeHelper (ExprIntMap*) const
{
  return isSignedResult () ? getBitSize () : determineBitSize ();
}

void NUMemselRvalue::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  mBitSize = size;
  // Select expression size is self-determined
  mIdent = mIdent->resizeSubHelper(mIdent->determineBitSize());
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i] = mExprs[i]->resizeSubHelper(mExprs[i]->determineBitSize());
  }
}

// Temporary memories are special.  Accesses to them are codegen'ed to
// reference the temporary memory and the master memory.
// So in the case of a temporary memory, we need to add a use of the master
// memory.  This is required for dependencies to work right for the backend
// reorder pass.
// Testcase: test/stateupdate
static NUNet *getTempMemMaster( NUNet *n )
{
  NUTempMemoryNet* temp_mem = dynamic_cast<NUTempMemoryNet*>(n);
  if( !temp_mem )
    return NULL;
  return temp_mem->getMaster();
}


void NUMemselRvalue::getUses(NUNetSet *uses) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
  mIdent->getUses(uses);
  
  NUNet* mem = getTempMemMaster( getIdent() );
  if( mem )
    uses->insert( mem );
}


void NUMemselRvalue::getUses(NUNetRefSet *uses) const
{

  NUNet *n = getIdent();
  NUNet* mem = getTempMemMaster( n );

  // NetRefs can only handle one dimension of reference; for a memory
  // this is the outermost dimension, stored in mExprs[0].  This is also
  // the only dimension in a multidim netref that can be guaranteed to
  // exist.
  NUConst *constant = mExprs[0]->castConst();
  if (constant != 0) {
    SInt64 val;
    bool ok = constant->getLL(&val);
    NU_ASSERT(ok, mExprs[0]);

    uses->insert(uses->getFactory()->createMemoryNetRef( n, SInt32 (val) ));

    if( mem )
      uses->insert( uses->getFactory()->createMemoryNetRef( 
                      mem, SInt32(val)));
  }
  else {
    // With a non-constant index, create a netref to the whole memory
    uses->insert( uses->getFactory()->createMemoryNetRef(n) );

    if( mem )
      uses->insert( uses->getFactory()->createMemoryNetRef( mem ) );
  }

  // Gather any uses of nets in the index expressions themselves
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
}

void NUMemselRvalue::getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUsesElab(scope, uses);
  }
  mIdent->getUsesElab(scope, uses);
}

void NUMemselRvalue::getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses)
  const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUsesElab(scope, uses);
  }
  mIdent->getUsesElab(scope, uses);
}


bool NUMemselRvalue::queryUses(const NUNetRefHdl &use_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  NU_ASSERT( isResynthesized(), this );
  if (mExprs[0]->queryUses(use_net_ref, fn, factory)) {
    return true;
  }

  NUConst *constant = mExprs[0]->castConst();
  if (constant != 0) {
    SInt64 val;
    bool ok = constant->getLL(&val);
    NU_ASSERT(ok, this);

    NUNetRefHdl net_ref = factory->createMemoryNetRef(getIdent(), SInt32 (val));
    return ((*net_ref).*fn)(*use_net_ref);
  } else {
    NUNetRefHdl net_ref = factory->createMemoryNetRef(getIdent());
    return ((*net_ref).*fn)(*use_net_ref);
  }
}


bool NUMemselRvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = mIdent->replace(old_net, new_net);
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    changed |= mExprs[i]->replace(old_net,new_net);
  }
  return changed;
}

bool NUMemselRvalue::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    // pre-recursion translation
    changed |= replaceExpr(&mExprs[i], NuToNuFn::ePre, translator);
  }
  // recursion
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    changed |= mExprs[i]->replaceLeaves(translator);
  }
  // post-recursion translation
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    changed |= replaceExpr(&mExprs[i], NuToNuFn::ePost, translator);
  }

  // sanity checking
  NUMemoryNet *mem = getIdent()->getMemoryNet();
  NU_ASSERT( mem, this );
  ConstantRangeVector origRanges;
  const UInt32 origDims = mem->getNumDims();
  for ( UInt32 i = 0; i < origDims; ++i )
    origRanges.push_back( *(mem->getRange(i)) );

  // we treat sub-identifiers differently from other subexprs.
  changed |= replaceExpr(&mIdent, NuToNuFn::ePre, translator);
  changed |= mIdent->replaceLeaves(translator);
  changed |= replaceExpr(&mIdent, NuToNuFn::ePost, translator);

  if ( changed && (getIdentExpr()->getType() == NUExpr::eNUIdentRvalue) )
  {
    // Currently we assume that any memory nets assignment aliased
    // together must have identical types, meaning identical widths and
    // depths.
    mem = getIdent()->getMemoryNet();
    NU_ASSERT( mem, this );
    NU_ASSERT( mem->getNumDims() == origDims, this );
    for ( UInt32 i = 0; i < origDims; ++i )
    {
      NU_ASSERT( mem->getRange(i)->getLength() == origRanges[i].getLength(), this );
    }
  }

  return changed;
}

const char* NUMemselRvalue::typeStr() const
{
  return "NUMemselRvalue";
}


void NUMemselRvalue::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << "MemselRvalue(" << this << ") ";
  printSize(screen);
  screen << UtIO::endl;
  mIdent->print(false, numspaces+2);
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->print(recurse, numspaces+2);
  }
}

bool
NUMemselRvalue::isResynthesized() const
{
  NUMemoryNet *mem = mIdent->getWholeIdentifier()->getMemoryNet();
  NU_ASSERT ( mem != NULL, this );
  return mResynthesized;
}

UInt32 NUMemselRvalue::getNumArgs() const {
  return mExprs.size() + 1;
}

//! Return the specified argument; 0-based index, 0 is leftmost argument
const NUExpr* NUMemselRvalue::getArgHelper(UInt32 index) const {
  NU_ASSERT(index <= mExprs.size(), this);
  return (index == 0) ? mIdent : mExprs[index - 1];
}

//! Mutate the argument
NUExpr* NUMemselRvalue::putArgHelper(UInt32 index, NUExpr* expr) {
  NU_ASSERT(index <= mExprs.size(), this);
  if (index == 0) {
    std::swap(expr, mIdent);
  }
  else {
    std::swap(expr, mExprs[index - 1]);
  }
  return expr;
}



NUOp::NUOp(OpT op, const SourceLocator& loc) :
  NUExpr(loc), mOp(op)
{}


NUOp::~NUOp()
{
}


void NUOp::getUses(NUNetSet *uses) const
{
  for (UInt32 i = 0; i < getNumArgs(); i++) {
    getArg(i)->getUses(uses);
  }
}


void NUOp::getUses(NUNetRefSet *uses) const
{
  for (UInt32 i = 0; i < getNumArgs(); i++) {
    getArg(i)->getUses(uses);
  }
}

void NUOp::getUsesElab(STBranchNode* scope, NUNetElabSet *uses) const
{
  for (UInt32 i = 0; i < getNumArgs(); i++) {
    getArg(i)->getUsesElab(scope, uses);
  }
}

void NUOp::getUsesElab(STBranchNode* scope, NUNetElabRefSet2 *uses) const
{
  for (UInt32 i = 0; i < getNumArgs(); i++) {
    getArg(i)->getUsesElab(scope, uses);
  }
}


bool NUOp::queryUses(const NUNetRefHdl &use_net_ref,
		     NUNetRefCompareFunction fn,
		     NUNetRefFactory *factory) const
{
  for (UInt32 i = 0; i < getNumArgs(); i++) {
    if (getArg(i)->queryUses(use_net_ref, fn, factory)) {
      return true;
    }
  }
  return false;
}


void NUOp::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << typeStr() << "(" << this << ") ";
  printSize(screen);
  screen << " : " << getOpChar(false) << UtIO::endl;
  if (recurse) {
    for (UInt32 i = 0; i < getNumArgs(); i++) {
      getArg(i)->print(recurse, numspaces+2);
    }
  }
}


NUUnaryOp::NUUnaryOp(OpT op,
		     NUExpr *expr,
		     const SourceLocator& loc) :
  NUOp(op, loc), mExpr(expr)
{
  NU_ASSERT((op >= eUnStart) && (op <= eUnEnd), this);
  mSignedResult = expr->isSignedResult ();
}

NUUnaryOp::NUUnaryOp(OpT op,
		     const NUExpr *expr,
		     const SourceLocator& loc) :
  NUOp(op, loc), mExpr(expr->constCast())
{
  NU_ASSERT((op >= eUnStart) && (op <= eUnEnd), this);
  mSignedResult = expr->isSignedResult ();
}


NUUnaryOp::~NUUnaryOp()
{
  if (!mIsManaged && mExpr->ownsSubExprs())
    delete mExpr;
}

void NUUnaryOp::manage(NUExprFactory* factory)
{
  mIsManaged = true;
  mExpr = factory->insert(mExpr)->constCast();
}

NUExpr* NUUnaryOp::copyHelper(CopyContext &copy_context) const
{
  NUUnaryOp *expr = new NUUnaryOp(mOp, mExpr->copy(copy_context), mLoc);
  return expr;
}


bool NUUnaryOp::replace(NUNet * old_net, NUNet * new_net)
{
  return mExpr->replace(old_net,new_net);
}

bool NUUnaryOp::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;
  bool changed = false;

  // pre-recursion translation
  changed |= replaceExpr(&mExpr, NuToNuFn::ePre, translator);

  // recursion
  changed |= mExpr->replaceLeaves(translator);

  // post-recursion translation
  changed |= replaceExpr(&mExpr, NuToNuFn::ePost, translator);

  return changed;
}

UInt32 NUUnaryOp::determineBitSizeHelper() const
{
  switch(mOp) {
  case eUnBuf:
  case eUnPlus:
  case eUnMinus:
  case eUnBitNeg:
  case eUnVhdlNot:
  case eUnAbs:
    return mExpr->determineBitSize();
    break;

  case eUnRound:
    // JDM: What is the self-determined size of round(real_number)?  The way
    // I read the Verilog LRM, I think it's 32, because it returns an
    // 'integer'.  But in src/inc/util/CarbonRuntime.h, I see that
    // carbon_round_real returns SInt64.
    return 64;

  case eUnFFO:
  case eUnFFZ:
  case eUnFLO:
  case eUnFLZ:
    // The assembly instruction on x86, and the runtime bit-hacking we use
    // on other platforms, return a 32-bit -1 if there are no bits set.
    return 32;

  case eUnCount:
    // Size is actual log2(mExpr), since the number of ones
    // can be no more than the number of bits...
    {
      UInt32 log2 = 0;

      for (UInt32 size = mExpr->determineBitSize ();
	   size != 0;
	   ++log2, size >>= 1) ;

      return log2;
    }

  case eUnLogNot:
  case eUnRedAnd:
  case eUnRedOr:
  case eUnRedXor:
  case eUnChange:
    return 1;
    break;

  case eUnRealtoBits:
  case eUnBitstoReal:
  case eUnItoR:
    return 64;
    break;
  case eUnRtoI:
    // this might be confusing: the lrm says that $rtoi returns a
    // value the size of 'integer', and aldec and carbon do so, but
    // the conversion is done in 64 bits and then the converted value
    // is truncated to the low 32 bits.
    // Thus the self determined bit size of $rtoi is 64 and populate
    // must make sure that the low 32 bits are selected as the result
    // of $rtoi.   (test/langcov/Real/rtoi_1.v)
    return 64;
    break;

  default:
    NU_ASSERT (0, this);
  }

  return 0;
}

UInt32 NUUnaryOp::effectiveBitSizeHelper (ExprIntMap* cache) const
{
  switch (mOp) {
  case eUnBuf:
  case eUnPlus:
  case eUnAbs:
    return mExpr->effectiveBitSize (cache);

  case eUnVhdlNot:
    return mExpr->determineBitSize();

  case eUnRound:
  case eUnMinus:
  case eUnBitNeg:
    // All these could have MSBs become negative
    return getBitSize ();

  case eUnFFO:
  case eUnFFZ:
  case eUnFLZ:
  case eUnFLO:
    return 32;            // the result value is in range [-1..(width-1)].
                          // -1 means no bits found with desired value.
                          // So always use 32-bits to accomodate the -1 value.

  case eUnCount:
    // Size is bounded by log2(mExpr->effectiveBitSize(cache)) + 1, since the number of ones
    // can be no more than the number of non-zero bits.
    {
      UInt32 log2 = DynBitVector::sLog2 (mExpr->effectiveBitSize (cache));
      return log2 + 1;
    }

  case eUnLogNot:
  case eUnRedAnd:
  case eUnRedOr:
  case eUnRedXor:
  case eUnChange:
    return 1;
    break;

  case eUnRealtoBits:
  case eUnBitstoReal:
  case eUnItoR:
    return 64;
    break;

  case eUnRtoI:
    return 32;
    break;


  default:
    NU_ASSERT(0, this);
  }

  return 0;
}

void NUUnaryOp::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  switch(mOp) {
  case eUnRound:
  case eUnBuf:
  case eUnPlus:
  case eUnMinus:
  case eUnAbs:
  case eUnBitNeg:
    mExpr = mExpr->resizeSubHelper(size);
    mBitSize = size;
    break;

  case eUnVhdlNot:
  case eUnFFO:
  case eUnFFZ:
  case eUnFLO:
  case eUnFLZ:
  case eUnCount:
    // Non-verilog operators (subexprs are self-sized)
    // result is requested size.
    mExpr = mExpr->resizeSubHelper(mExpr->determineBitSize ());
    mBitSize = size;
    break;

  case eUnLogNot:
  case eUnRedAnd:
  case eUnRedOr:
  case eUnRedXor:
  case eUnChange:
  case eUnRealtoBits:
  case eUnBitstoReal:
  case eUnItoR:
  case eUnRtoI:
    // Subexpressions are self-determined
    mExpr = mExpr->resizeSubHelper(mExpr->determineBitSize());
    // mBitSize = 1; // test/fold
    mBitSize = size;  // bug 5673, 5691
    break;

  default:
    NU_ASSERT(0, this);

  } // switch
} // void NUUnaryOp::resizeHelper

bool NUUnaryOp::isIdentifierBitFlip() const
{
  // This is a size-preserving bit-flip operation if both the ~/! and
  // its argument have the same size.
  const NUExpr * unary_arg = getArg(0);
  bool bit_flip = ((getBitSize()==unary_arg->getBitSize()) and
                   (unary_arg->getType()==NUExpr::eNUIdentRvalue) and
                   ( (getOp()==NUOp::eUnBitNeg) or
                     (getOp()==NUOp::eUnLogNot and getBitSize()==1) ) );
  return bit_flip;
}


//! Return number of arguments to this operator.
UInt32 NUUnaryOp::getNumArgs() const {
  return 1;
}

const NUExpr* NUUnaryOp::getArgHelper(UInt32 index) const
{
  switch (index) {
  case 0:
    return mExpr;
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }
  return 0;
}


NUExpr* NUUnaryOp::putArgHelper(UInt32 index, NUExpr * argument)
{
  NUExpr * old_argument = getArg(index);
  switch (index) {
  case 0:
    mExpr = argument;
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }
  return old_argument;
}


const char* NUUnaryOp::typeStr() const
{
  return "UnaryOp";
}

NUExpr* NUUnaryOp::makeSizeExplicitHelper(UInt32 desired_size) {
  switch(mOp) {
  case eUnBuf:
  case eUnPlus:
  case eUnMinus:
  case eUnAbs:
  case eUnBitNeg:
    mExpr = mExpr->makeSizeExplicit(desired_size);
    mBitSize = desired_size;
    break;

  case eUnVhdlNot:
  case eUnFFO:
  case eUnFFZ:
  case eUnFLO:
  case eUnFLZ:
  case eUnCount:
  case eUnRound:
    // Non-verilog operators (subexprs are self-sized), so no consistency check
    mExpr = mExpr->makeSizeExplicit();
    break;

  case eUnLogNot:
  case eUnRedAnd:
  case eUnRedOr:
  case eUnRedXor:
  case eUnChange:
  case eUnRealtoBits:
  case eUnBitstoReal:
  case eUnItoR:
  case eUnRtoI:
    // Subexpressions are self-determined, so no consistency check
    mExpr = mExpr->makeSizeExplicit();
    break;

  default:
    NU_ASSERT(0, this);

  }
  // we have processed the operands, now pad or partsel the result as needed.
  return  NUExpr::makeSizeExplicitHelper(desired_size);
}

NUExpr* NUUnaryOp::coerceOperandsToReal( ) {
  NUExpr* ret_expr = this;

  if ( not mExpr->isReal() ){
    mExpr = mExpr->coerceOperandsToReal();
  }

  NU_ASSERT( mExpr->isReal(), ret_expr );

  return ret_expr;
}




NUBinaryOp::NUBinaryOp(OpT op,
		       NUExpr *expr1,
		       NUExpr *expr2,
		       const SourceLocator& loc) :
  NUOp(op, loc), mExpr1(expr1), mExpr2(expr2)
{
  NU_ASSERT((op >= eBiStart) && (op <= eBiEnd), this);
  mSignedResult = (mExpr1->isSignedResult () && mExpr2->isSignedResult ())
    && not NUOp::isRelational (op); // LRM 4.5.1

}

NUBinaryOp::NUBinaryOp(OpT op,
		       const NUExpr *expr1,
		       const NUExpr *expr2,
		       const SourceLocator& loc) :
  NUOp(op, loc),
  mExpr1(expr1->constCast()),
  mExpr2(expr2->constCast())
{
  NU_ASSERT((op >= eBiStart) && (op <= eBiEnd), this);
  mSignedResult = mExpr1->isSignedResult () && mExpr2->isSignedResult ()
    && not NUOp::isRelational (op); // LRM 4.5.1
}


NUBinaryOp::~NUBinaryOp()
{
  if (!mIsManaged)
  {
    if (mExpr1->ownsSubExprs())
      delete mExpr1;
    if (mExpr2->ownsSubExprs())
      delete mExpr2;
  }
}

void NUBinaryOp::manage(NUExprFactory* factory)
{
  mIsManaged = true;
  mExpr1 = factory->insert(mExpr1)->constCast();
  mExpr2 = factory->insert(mExpr2)->constCast();
}


NUExpr* NUBinaryOp::copyHelper(CopyContext &copy_context) const
{
  NUBinaryOp *expr = new NUBinaryOp(mOp, mExpr1->copy(copy_context), mExpr2->copy(copy_context), mLoc);
  return expr;
}


bool NUBinaryOp::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed |= mExpr1->replace(old_net,new_net);
  changed |= mExpr2->replace(old_net,new_net);
  return changed;
}

bool NUBinaryOp::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // pre-recursion translation
  changed |= replaceExpr(&mExpr1, NuToNuFn::ePre, translator);
  changed |= replaceExpr(&mExpr2, NuToNuFn::ePre, translator);

  // recursion
  changed |= mExpr1->replaceLeaves(translator);
  changed |= mExpr2->replaceLeaves(translator);

  // post-recursion translation
  changed |= replaceExpr(&mExpr1, NuToNuFn::ePost, translator);
  changed |= replaceExpr(&mExpr2, NuToNuFn::ePost, translator);

  return changed;
}

UInt32 NUBinaryOp::determineBitSizeHelper() const
{
  switch(mOp) {
  case eBiPlus:
  case eBiMinus:
  case eBiSMult:
  case eBiSDiv:
  case eBiSMod:
  case eBiUMult:
  case eBiUDiv:
  case eBiUMod:
  case eBiVhdlMod:
  case eBiBitAnd:
  case eBiBitOr:
  case eBiBitXor:

  case eBiDownTo:
    return std::max(mExpr1->determineBitSize(), mExpr2->determineBitSize());
    break;

/*
    // This hack is not really the Verilog rules.  But it lets us
    // shrink expressions by masking them, without having to resort
    // to Zxt operators, which might not optimize as well.
    return std::min(mExpr1->determineBitSize(), mExpr2->determineBitSize());
    break;
*/

  case eBiEq:
  case eBiNeq:
  case eBiTrieq:
  case eBiTrineq:
  case eBiLogAnd:
  case eBiLogOr:
  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
  case eBiULt:
  case eBiULte:
  case eBiUGtr:
  case eBiUGtre:
    return 1;
    break;

  case eBiRoR:
  case eBiRoL:
  case eBiRshift:
  case eBiLshift:
  case eBiVhLshift:
  case eBiVhRshift:
  case eBiLshiftArith:
  case eBiRshiftArith:
  case eBiVhRshiftArith:
  case eBiVhLshiftArith:
  case eBiExp:
    return mExpr1->determineBitSize();

  case eBiDExp:
    // A real result requires 64 bits
    return 64;

  case eBiVhZxt:
  case eBiVhExt: {
    // The size of eBiVhExt|eBiVhZxt should be determined at population time.
    // I believe in most cases it is.  Hopefully it will have been
    // populated correctly.  If it wasn't populated at all, then we
    // hope we get a constant as the second arg.  In VHDL when
    // populating an eBiVhExt we should be guarding against
    UInt32 val;
    NUConst* k = mExpr2->castConst();
    if ((k == NULL) || !k->getUL(&val)) {
      // If we don't know the size from context, and we can't
      // figure it out statically from the second arg, then we are in trouble.
      // This should not happen because of VhdlPopulate::createSizeExpr,
      // which guards against this with an appropriate error message.
      NU_ASSERT(mBitSize != 0, this);
      val = mBitSize;
    }
    return val;
    break;
  }

  default:
    NU_ASSERT(0, this);
    break;
  }

  return 0;
}

UInt32 NUBinaryOp::effectiveBitSizeHelper(ExprIntMap* cache) const
{
  switch(mOp) {
  case eBiPlus:
    return std::max(mExpr1->effectiveBitSize(cache),
                    mExpr2->effectiveBitSize(cache)) + 1;

  case eBiSMult:
  case eBiUMult:
    return mExpr1->effectiveBitSize (cache) + mExpr2->effectiveBitSize (cache);

  case eBiExp:
  case eBiDExp:
  case eBiSDiv:
    return getBitSize ();	// chicken!

  case eBiUDiv:
    return mExpr1->effectiveBitSize (cache);

  case eBiVhdlMod:
  case eBiSMod:
    if (isSignedResult ())
      return getBitSize ();
    else
      return mExpr2->effectiveBitSize (cache);      

  case eBiUMod:
    return mExpr2->effectiveBitSize (cache);

  case eBiMinus:
  case eBiDownTo:
    return getBitSize ();

  case eBiBitAnd:
    return std::min(mExpr1->effectiveBitSize(cache), mExpr2->effectiveBitSize(cache));
  case eBiBitOr:
  case eBiBitXor:
    return std::max(mExpr1->effectiveBitSize(cache), mExpr2->effectiveBitSize(cache));

  case eBiEq:
  case eBiNeq:
  case eBiTrieq:
  case eBiTrineq:
  case eBiLogAnd:
  case eBiLogOr:
  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
  case eBiULt:
  case eBiULte:
  case eBiUGtr:
  case eBiUGtre:
    return 1;
    break;

  case eBiRshift:
    {
      if (const NUConst* rval = mExpr2->castConst())
      {
        // A constant shift amount reduces the effective bit size
        // of the lhs proportionally.
        UInt32 shiftSize;
        if (rval->getUL (&shiftSize))
          // no loss of significant digits...
          return std::max (0, SInt32(mExpr1->effectiveBitSize (cache) - shiftSize));
      }

      // With variable shift amount, don't know how much smaller
      // this could be.
      return  mExpr1->effectiveBitSize (cache);
    }

  case eBiVhZxt:
    {
      // Consider ZXT (e, const).  The effective bit size is bounded
      UInt32 width;
      NUConst* wp = mExpr2->castConst ();
      if (wp) {
        wp->getUL (&width);
      } else {
        // non-constant field width...
        // worst case is min ((2 ** (mExpr2->getBitSize()) - 1), mExpr1->effectiveBitSize()
        width = mExpr2->effectiveBitSize (cache);
        if (width > 16)
          return getBitSize (); // no useful bounds deducible - worst case assumed
        width = (1<<width) - 1;
      }

      UInt32 m1effsize = mExpr1->effectiveBitSize (cache);

      if (mExpr1->isSignedResult ()) {
        if (m1effsize < mExpr1->getBitSize () && m1effsize < width && wp)
          return m1effsize;
        else if (wp)
          // slightly conservative choice
          return std::min (width, getBitSize ());
        else
          // Assume worst-case
          return getBitSize ();
      } else {
        // zero-extending expr1, so just the smaller of the possibilities
        return std::min (width, m1effsize);
      }
    }

  case eBiVhExt:
    // Could be aggressive about looking at signedResult, constant vs non-constant
    // mExpr2 and actual size of mExpr1.  But worst case is safest.
    {
      UInt32 width;
      if (NUConst* wp = mExpr2->castConst ()) {
        wp->getUL (&width);

      } else {
        // non constant size, but bounded by precision of expr2
        width = mExpr2->effectiveBitSize (cache);
        if (width > 16)
          return getBitSize (); // worst case guess

        width = (1 << width) - 1;
      }

      if (mExpr1->isSignedResult ())
        return getBitSize ();   // Hard to narrow

      UInt32 m1effsize = mExpr1->effectiveBitSize (cache);
      if (width > m1effsize)
        // Sign extending from a zero bit value, so we can lower the bound estimate
        return m1effsize;

      return getBitSize ();
    }


  case eBiRoR:
  case eBiRoL:
  case eBiVhLshift:
  case eBiVhRshift:
  case eBiRshiftArith:
  case eBiLshiftArith:
  case eBiVhRshiftArith:
  case eBiVhLshiftArith:

    return getBitSize ();

  case eBiLshift:
    {
      UInt32 minSize = mExpr1->effectiveBitSize (cache);

      if (minSize == 0)
        return 0;

      UInt32 shiftSize;
      if (const NUConst* rval = mExpr2->castConst())
      {
        if (rval->getUL (&shiftSize))
          return std::min (minSize + shiftSize, getBitSize ());
        else
          return getBitSize ();
      }

      shiftSize = mExpr2->effectiveBitSize (cache);
      if (shiftSize >= 32)
        // pathological - assume it can't get any larger than Verilog wants it to be
        return getBitSize ();

      UInt64 shiftAmount = (1<<shiftSize) - 1;
      return std::min (UInt64 (getBitSize ()),
                       UInt64 (minSize + shiftAmount));
    }

  default:
    NU_ASSERT(0, this);
  }

  return 0;
}


void NUBinaryOp::resizeHelper(UInt32 size)
{
  // Don't do RESIZE_INVARIANT until afterwards.
  UInt32 tmpSize;
  switch(mOp) {
  case eBiPlus:
  case eBiMinus:
  case eBiDownTo:
  case eBiSMult:
  case eBiUMult:
  case eBiBitAnd:
  case eBiBitOr:
  case eBiBitXor:

    mExpr1 = mExpr1->resizeSubHelper(size);
    mExpr2 = mExpr2->resizeSubHelper(size);
    mBitSize = size;
    break;

  case eBiVhZxt:
  case eBiVhExt: {
    // The first operand was properly sized during population AND MUST NOT be altered!
    UInt32 sz = mExpr1->getBitSize ();
    mExpr1 = mExpr1->resizeSubHelper(sz);

    // The second operand is self-determined
    mExpr2 = mExpr2->resizeSubHelper(mExpr2->determineBitSize());
    mBitSize = size;
    break;
  }

  case eBiVhdlMod:
  case eBiSMod:
  case eBiSDiv:
  case eBiUMod:
  case eBiUDiv:
    // Never allow resize to reduce left operand below its determined
    // size.  That would destroy bits that participate in even a smaller
    // result.  In fact the same goes for the right operand.  Consider
    //   (x%2) resized to 1 bit shouldn't turn that "2" into a "0".
    NU_ASSERT((size >= mExpr1->determineBitSize ()), mExpr1);
    NU_ASSERT((size >= mExpr2->determineBitSize ()), mExpr2);
    mExpr1 = mExpr1->resizeSubHelper(size);
    mExpr2 = mExpr2->resizeSubHelper(size);
    mBitSize = size;
    break;

  case eBiLogAnd:
  case eBiLogOr:
    // There's an error in the LRM - && and || are treated as
    // self-determined (see errata dated Nov 26, 2003, or later)
    mExpr1 = mExpr1->resizeSubHelper(mExpr1->determineBitSize ());
    mExpr2 = mExpr2->resizeSubHelper(mExpr2->determineBitSize ());
    mBitSize = size;
    break;

  case eBiEq:
  case eBiNeq:
  case eBiTrieq:
  case eBiTrineq:
  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
  case eBiULt:
  case eBiULte:
  case eBiUGtr:
  case eBiUGtre:
    tmpSize = std::max(mExpr1->determineBitSize(),
                       mExpr2->determineBitSize());
    mExpr1 = mExpr1->resizeSubHelper(tmpSize);
    mExpr2 = mExpr2->resizeSubHelper(tmpSize);
    mBitSize = size;          // allow resizing
    break;

  case eBiRoR:
  case eBiRoL:

    // rotates are funny in that a resize operation
    // that changes the size will cause us to not compute bits that
    // may participate in the result of the expression.
    // Consider:
    //        (U64 rot 8) & 8'hFF
    // naively resizing the and-expression would result in
    // resizing U64 to 8 bits, and after we rotate that right by
    // 8, we've got the wrong bits!
    //
    mExpr1 = mExpr1->resizeSubHelper(mExpr1->determineBitSize ());

    // The shift amount is self-determined
    mExpr2 = mExpr2->resizeSubHelper(mExpr2->determineBitSize());
    mBitSize = size;
    break;

  case eBiVhRshift:
  case eBiVhRshiftArith:
  case eBiRshift:
  case eBiRshiftArith:
    // Don't allow right-shifts to ever produce a SMALLER result - that would
    // result in a loss-of-significance for the MSBs.
    NU_ASSERT((size >=  mExpr1->determineBitSize ()), mExpr1);
    mExpr1 = mExpr1->resizeSubHelper(size);

    // The shift amount is self-determined
    mExpr2 = mExpr2->resizeSubHelper(mExpr2->determineBitSize());
    mBitSize = size;
    break;

  case eBiDExp:
  case eBiExp:
  case eBiLshift:
  case eBiVhLshift:
  case eBiLshiftArith:
  case eBiVhLshiftArith:
    mExpr1 = mExpr1->resizeSubHelper(size);
    // The shift or exponent amount is self-determined
    mExpr2 = mExpr2->resizeSubHelper(mExpr2->determineBitSize());
    mBitSize = size;
    break;

  default:
    NU_ASSERT(0, this);
  }

  // Report if we changed the size of this expression to something SMALLER than
  // its self-determined size.  This is done AFTER we resize() to prevent calling
  // determineBitSize() on a EXT or ZXT that has NEVER been resized.
  RESIZE_INVARIANT (size);
} // void NUBinaryOp::resize


//! Return number of arguments to this operator.
UInt32 NUBinaryOp::getNumArgs() const {
  return 2;
}

const NUExpr* NUBinaryOp::getArgHelper(UInt32 index) const
{
  switch (index) {
  case 0:
    return mExpr1;
    break;

  case 1:
    return mExpr2;
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }
  return 0;
}


NUExpr* NUBinaryOp::putArgHelper(UInt32 index, NUExpr * argument)
{
  NUExpr * old_argument = getArg(index);
  switch (index) {
  case 0:
    mExpr1 = argument;
    break;

  case 1:
    mExpr2 = argument;
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }
  return old_argument;
}


const char* NUBinaryOp::typeStr() const
{
  return "BinaryOp";
}

bool
NUUnaryOp::isReal () const
{
  switch(mOp)
  {
  case eUnPlus:
  case eUnMinus:
    return mExpr->isReal();
    break;

  case eUnItoR:
  case eUnBitstoReal:
    return true;
    break;

  default:
    return false;
  }
}

// JDM: NUBinaryOp::isReal does not appear to properly implement this table from
// the LRM:
/*

    From: Rich Cloutier <cloutier@carbondesignsystems.com>
    To: Josh <Josh@carbondesignsystems.com>
    Subject: verilog reals
    Date: Tue, 03 Oct 2006 11:35:04 -0400

    For non-self-determined operands the following rules apply:
    if any operand is real, the result is real;

    The following operands are valid for real operands:

    The operators shown in Table 10 shall be legal when applied to
    real operands. All other operators shall be considered illegal
    when used with real operands.  The result of using logical or
    relational operators on real numbers is a single-bit scalar value.

    Table 10.  Legal operators for use in real expressions

    unary + unary -    	Unary operators
    + - * / **     	Arithmetic
    > >= < <=     	Relational
    ! && ||     	Logical
    == !=     		Logical equality
    ?:             	Conditional

*/
bool
NUBinaryOp::isReal() const
{
  switch(mOp)
  {
  case eBiPlus:
  case eBiMinus:
  case eBiSMult:
  case eBiSDiv:
    // front-end doesn't like to see mixed mode arithmetic, but we don't care
    // if one of the operands is a real and the other's an integral value...
    return mExpr1->isReal() || mExpr2->isReal();
    break;
  case eBiDExp:
    return true;

  default:
    return false;
  }
}

/*!
  Depending on the operation, explicitly size binary-op operands to the desired_size.
  return expression that has the self-determined size matching the
  context size.  Consider
  o[1:0] = a + b; // a and b are scalars, thus they have a context size
                  // of 2 and a self determined size of 1.  The self
                  // determined size of the RHS is also 1.
  if the sizes of the operands were not made explicit first then we would
  incorrectly return {1'b0,(a+b)} here but this is not the correct sizing
  of the operands. (test/interra/misc2/MISC60 is an example)
 */
NUExpr* NUBinaryOp::makeSizeExplicitHelper(UInt32 desired_size) {
  switch(mOp) {
  case eBiSMult:
  case eBiUMult:
    // Should desired_size by maxed with determineBitSize()?
    // These should not be treated like RShift, because they
    // do require symmetry.  see test/interra/expr1/ARITH12.
    //
    // This is really Josh's note to himself, wondering if there is
    // a situation where significant bits of a multiply may be stripped
    // in a mixed-size scenario, much like right-shift.
  case eBiPlus:
  case eBiMinus:
  case eBiDownTo:             // check this
  case eBiBitAnd:
  case eBiBitOr:
  case eBiBitXor:
    // This group all require that size(lop)==size(rop)==size(this)

    mExpr1 = mExpr1->makeSizeExplicit(desired_size);
    mExpr2 = mExpr2->makeSizeExplicit(desired_size);
    mBitSize = desired_size;
    break;

  case eBiEq:
  case eBiNeq:
  case eBiTrieq:
  case eBiTrineq:
  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
  case eBiULt:
  case eBiULte:
  case eBiUGtr:
  case eBiUGtre: {
    UInt32 tmpSize = std::max(mExpr1->determineBitSize(),
                              mExpr2->determineBitSize());
    mExpr1 = mExpr1->makeSizeExplicit(tmpSize);
    mExpr2 = mExpr2->makeSizeExplicit(tmpSize);
    break;
  }

  case eBiLogAnd:
  case eBiLogOr:
    mExpr1 = mExpr1->makeSizeExplicit();
    mExpr2 = mExpr2->makeSizeExplicit();
    break;

  case eBiVhdlMod:
  case eBiSMod:
  case eBiSDiv:
  case eBiUMod:
  case eBiUDiv:
  case eBiVhRshift:
  case eBiVhRshiftArith:
  case eBiRshift:
  case eBiRshiftArith: {
    // This group all have self-determined rop.  But right-shifts are
    // dangerous.  Say we have a24>>16, and we are looking for 5-bits
    // out of that expression (we are going to & it with 5'h10).  If
    // we recurse into take a24->makeSizeExplicit(5), we are going to
    // get a24[4:0].  That will destroy important bits.  We need to
    // produce (a24>>16)[4:0], not a24[4:0]>>16.
    //
    // On the other hand, consider test/interra/expr2/FUNC13.v, from that
    // this simple testcase is interesting, with 9-bit out2 and 8-bit in4.
    //    assign    out2 = (in4<<2)>>4;
    // Here, the self-determined size of the RHS if 8, and that's too
    // small, as we will lose the MSB when we left-shift in4 by 2.
    UInt32 max_size = std::max(mExpr1->determineBitSize(), desired_size);
    mExpr1 = mExpr1->makeSizeExplicit(max_size);
    mExpr2 = mExpr2->makeSizeExplicit();
    mBitSize = max_size;
    break;
  }    

  case eBiDExp:
    // double-precision Exp should have converted the lop to real and
    // the rop should be self-determined in size.
    mExpr1 = mExpr1->makeSizeExplicit(desired_size);
    mExpr2 = mExpr2->makeSizeExplicit();
    mBitSize = desired_size;
    break;

  case eBiExp:
  case eBiRoR:
  case eBiRoL:
  case eBiLshift:
  case eBiVhLshift:
  case eBiLshiftArith:
  case eBiVhLshiftArith:
    // This group has self-determined rop, but size(lop)==size(this)

    mExpr1 = mExpr1->makeSizeExplicit(desired_size);
    mExpr2 = mExpr2->makeSizeExplicit();
    mBitSize = desired_size;
    break;

  case eBiVhZxt:
  case eBiVhExt:
    break;

  default:
    NU_ASSERT(0, this);
  }
  // we have processed the operands, now pad or partsel the result as needed.
  return NUExpr::makeSizeExplicitHelper(desired_size);
} // NUExpr* NUBinaryOp::makeSizeExplicitHelper


NUExpr* NUBinaryOp::coerceOperandsToReal( ) {
  NUExpr* ret_expr = this;

  switch(mOp) {
  case eBiSMult:
  case eBiUMult:
  case eBiPlus:
  case eBiMinus:
  case eBiDownTo:
  case eBiBitAnd:
  case eBiBitOr:
  case eBiBitXor:
  case eBiEq:
  case eBiNeq:
  case eBiTrieq:
  case eBiTrineq:
  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
  case eBiULt:
  case eBiULte:
  case eBiUGtr:
  case eBiUGtre: 
  case eBiLogAnd:
  case eBiLogOr:
  {
    // This group both ops are to be conveted to real

    if ( not mExpr1->isReal() ){
      mExpr1 = mExpr1->coerceOperandsToReal( );
    }
    if ( not mExpr2->isReal() ){
      mExpr2 = mExpr2->coerceOperandsToReal( );
    }
    break;
  }

  case eBiVhdlMod:
  case eBiSMod:
  case eBiSDiv:
  case eBiUMod:
  case eBiUDiv:
  case eBiVhRshift:
  case eBiVhRshiftArith:
  case eBiRshift:
  case eBiRshiftArith:
  case eBiExp:
  case eBiRoR:
  case eBiRoL:
  case eBiLshift:
  case eBiVhLshift:
  case eBiLshiftArith:
  case eBiVhLshiftArith:
  case eBiDExp:
  {
    // This group only the left op is converted to real, the other is
    // self determined

    if ( not mExpr1->isReal() ){
      mExpr1 = mExpr1->coerceOperandsToReal( );
    }
    break;
  }    

  case eBiVhZxt:
  case eBiVhExt:
    // do not convert either operand
    break;

  default:
    NU_ASSERT(0, this);
  }
  return ret_expr->NUExpr::coerceOperandsToReal();
}


NUTernaryOp::NUTernaryOp(OpT op,
			 NUExpr *expr1,
			 NUExpr *expr2,
			 NUExpr *expr3,
			 const SourceLocator& loc) :
  NUOp(op, loc), mExpr1(expr1), mExpr2(expr2), mExpr3(expr3)
{
  NU_ASSERT((op >= eTeStart) && (op <= eTeEnd), this);
  mSignedResult = expr2->isSignedResult () && expr3->isSignedResult ();
}

NUTernaryOp::NUTernaryOp(OpT op,
			 const NUExpr *expr1,
			 const NUExpr *expr2,
			 const NUExpr *expr3,
			 const SourceLocator& loc) :
  NUOp(op, loc),
  mExpr1(expr1->constCast()),
  mExpr2(expr2->constCast()),
  mExpr3(expr3->constCast())
{
  NU_ASSERT((op >= eTeStart) && (op <= eTeEnd), this);
  mSignedResult = expr2->isSignedResult () && expr3->isSignedResult ();
}


NUTernaryOp::~NUTernaryOp()
{
  if (!mIsManaged)
  {
    if (mExpr1->ownsSubExprs())
      delete mExpr1;
    if (mExpr2->ownsSubExprs())
      delete mExpr2;
    if (mExpr3->ownsSubExprs())
      delete mExpr3;
  }
}

void NUTernaryOp::manage(NUExprFactory* factory)
{
  mIsManaged = true;
  mExpr1 = factory->insert(mExpr1)->constCast();
  mExpr2 = factory->insert(mExpr2)->constCast();
  mExpr3 = factory->insert(mExpr3)->constCast();
}


NUExpr* NUTernaryOp::copyHelper(CopyContext &copy_context) const
{
  NUTernaryOp *expr = new NUTernaryOp(mOp, 
                                      mExpr1->copy(copy_context), 
                                      mExpr2->copy(copy_context), 
                                      mExpr3->copy(copy_context), 
                                      mLoc);
  return expr;
}


bool NUTernaryOp::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed |= mExpr1->replace(old_net,new_net);
  changed |= mExpr2->replace(old_net,new_net);
  changed |= mExpr3->replace(old_net,new_net);
  return changed;
}

bool NUTernaryOp::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // pre-recursion translation
  changed |= replaceExpr(&mExpr1, NuToNuFn::ePre, translator);
  changed |= replaceExpr(&mExpr2, NuToNuFn::ePre, translator);
  changed |= replaceExpr(&mExpr3, NuToNuFn::ePre, translator);

  // recursion
  changed |= mExpr1->replaceLeaves(translator);
  changed |= mExpr2->replaceLeaves(translator);
  changed |= mExpr3->replaceLeaves(translator);

  // post-recursion translation
  changed |= replaceExpr(&mExpr1, NuToNuFn::ePost, translator);
  changed |= replaceExpr(&mExpr2, NuToNuFn::ePost, translator);
  changed |= replaceExpr(&mExpr3, NuToNuFn::ePost, translator);

  return changed;
}

UInt32 NUTernaryOp::determineBitSizeHelper() const
{
  switch(mOp) {
  case eTeCond:
    return std::max(mExpr2->determineBitSize(), mExpr3->determineBitSize());
    break;

  default:
    NU_ASSERT(0, this);
  }

  return 0;
}


void NUTernaryOp::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  switch(mOp) {
  case eTeCond:
    // The select expression is self-determined
    mExpr1 = mExpr1->resizeSubHelper(mExpr1->determineBitSize());
    mExpr2 = mExpr2->resizeSubHelper(size);
    mExpr3 = mExpr3->resizeSubHelper(size);
    mBitSize = size;
    break;

  default:
    NU_ASSERT(0, this);
  }
}

UInt32 NUTernaryOp::effectiveBitSizeHelper (ExprIntMap* cache) const
{
  switch(mOp) {
  case eTeCond:
    return std::max (mExpr2->effectiveBitSize (cache),
                     mExpr3->effectiveBitSize (cache));
    break;
  default:
    NU_ASSERT (0, this);
  }
  return getBitSize ();
}

  //! Return number of arguments to this operator.
UInt32 NUTernaryOp::getNumArgs() const {
  return 3;
}

const NUExpr* NUTernaryOp::getArgHelper(UInt32 index) const
{
  switch (index) {
  case 0:
    return mExpr1;
    break;

  case 1:
    return mExpr2;
    break;

  case 2:
    return mExpr3;
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }
  return 0;
}


NUExpr* NUTernaryOp::putArgHelper(UInt32 index, NUExpr * argument) 
{
  NUExpr * old_argument = getArg(index);
  switch (index) {
  case 0:
    mExpr1 = argument;
    break;

  case 1:
    mExpr2 = argument;
    break;

  case 2:
    mExpr3 = argument;
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }

  return old_argument;
}


const char* NUTernaryOp::typeStr() const
{
  return "TernaryOp";
}

//Only bother checking the first possible result, as they must both be the same type
bool
NUTernaryOp::isReal() const
{
  return mExpr2->isReal();
}

NUExpr* NUTernaryOp::makeSizeExplicitHelper(UInt32 desired_size) {
  switch(mOp) {
  case eTeCond:
    mExpr1 = mExpr1->makeSizeExplicit();
    mExpr2 = mExpr2->makeSizeExplicit(desired_size);
    mExpr3 = mExpr3->makeSizeExplicit(desired_size);
    mBitSize = desired_size;
    break;

  default:
    NU_ASSERT(0, this);
  }
  return  NUExpr::makeSizeExplicitHelper(desired_size);
}


NUExpr* NUTernaryOp::coerceOperandsToReal( ) {
  NUExpr* ret_expr = this;

  switch(mOp) {
  case eTeCond:
    // the condition is self determined
    if ( not mExpr2->isReal() ) {
      mExpr2 = mExpr2->coerceOperandsToReal( );
    }
    if ( not mExpr3->isReal() ) {
      mExpr3 = mExpr3->coerceOperandsToReal( );
    }
    break;
  default:
    NU_ASSERT(0, this);
  }

  return ret_expr->NUExpr::coerceOperandsToReal();
}

BDD NUIdentRvalue::bdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize()!=1) {
    return ctx->invalid();
  }
    
  if (scope == 0) {
    return ctx->bdd(mNet);
  } else {
    return ctx->bdd(mNet->lookupElab(scope));
  }
}

bool NUExpr::hasBdd(BDDContext*, STBranchNode*) const
{
  return false;
}

bool NUIdentRvalue::hasBdd(BDDContext*, STBranchNode*) const
{
  return (getBitSize()==1);
}

BDD NUIdentRvalueElab::bdd(BDDContext *ctx, STBranchNode *) const
{
  if (getBitSize()!=1) {
    return ctx->invalid();
  }

  return ctx->bdd(mNetElab);
}

bool NUIdentRvalueElab::hasBdd(BDDContext *, STBranchNode *) const
{
  return (getBitSize()==1);
}



static const UInt32 cMaxVectorBDD = 32;  // Arbitrary limit to avoid BDD blow-up
static BDD sGenCompareVectorBDD (BDDContext *ctx, const NUExpr *lop,
                                 const NUExpr *rop, STBranchNode * scope);

bool NUUnaryOp::hasBdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize()!=1) {
    return false;
  }

  switch (mOp)
  {
  case eUnBuf:
  case eUnPlus:
  case eUnBitNeg:
  case eUnVhdlNot:
    return mExpr->hasBdd(ctx, scope);

  case eUnLogNot:
  case eUnRedOr:
  case eUnRedAnd:
    // These may have a BDD even though the expression underneath does not.
    // Depending on what it is we can break it down and turn it into
    // logic, and the BDDs will tell us whether we can.  This routine
    // is allowed to be optimistic.  But note that we never do this for
    // excessively wide expressions, to avoid blowing up in the BDD package
    return mExpr->effectiveBitSize() <= cMaxVectorBDD;
    break;

  case eUnFFO:
  case eUnFFZ:
  case eUnFLO:
  case eUnFLZ:
  case eUnCount:
  case eUnAbs:
  case eUnMinus:
  case eUnRedXor:
  case eUnRound:
  case eUnChange:
  case eUnRealtoBits:
  case eUnBitstoReal:
  case eUnItoR:
  case eUnRtoI:
  default:
    break;
  }

  return false;
} // BDD NUUnaryOp::hasBdd

BDD NUUnaryOp::bdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize()!=1) {
    return ctx->invalid();
  }

  switch (mOp)
  {
  case eUnBuf:
  case eUnPlus:
    return ctx->bddRecurse(mExpr, scope);
    break;

  case eUnLogNot:
  case eUnRedOr: {
    UInt32 bitSize = mExpr->getBitSize();
    NUConst* zero = NUConst::create(false, 0, bitSize, mExpr->getLoc());
    BDD eq0 = sGenCompareVectorBDD(ctx, mExpr, zero, scope);
    delete zero;
    if (mOp == eUnRedOr) {
      return ctx->opNot(eq0);
    }
    return eq0;
  }

  case eUnRedAnd: {
    UInt32 bitSize = mExpr->getBitSize();
    DynBitVector onesVec(bitSize);
    onesVec.flip();
    NUConst* ones = NUConst::create(false, onesVec, bitSize, mExpr->getLoc());
    BDD eq1 = sGenCompareVectorBDD(ctx, mExpr, ones, scope);
    delete ones;
    return eq1;
  }

  case eUnBitNeg:
  case eUnVhdlNot:

    return ctx->opNot(ctx->bddRecurse(mExpr, scope));
    break;

  case eUnFFO:
  case eUnFFZ:
  case eUnFLO:
  case eUnFLZ:
  case eUnCount:
  case eUnAbs:
  case eUnMinus:
  case eUnRedXor:
  case eUnRound:
  case eUnChange:
  case eUnRealtoBits:
  case eUnBitstoReal:
  case eUnItoR:
  case eUnRtoI:
    return ctx->invalid();
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }

  return ctx->invalid();
}

// Is this a VectorNet larger than a single bit?
static bool sIsCompareVector (const NUExpr *e)
{
  if(const NUIdentRvalue *net = dynamic_cast<const NUIdentRvalue*>(e))
    return (net->getIdent ()->getBitSize () > 1);
  else if (const NUVarselRvalue* v = dynamic_cast<const NUVarselRvalue*>(e))
    return (v->getIdentExpr ()->getBitSize () > 1);
  else if (const NUConcatOp* cat = dynamic_cast<const NUConcatOp*>(e))
    return (cat->effectiveBitSize() > 1);
  return false;
}

// Build a BDD expression for NET == CONST by exploding it into a bitwise compare
// using a canonically named "NET <n>" for each bit.
//
static BDD sGenCompareVectorBDD (BDDContext *ctx, const NUExpr *lop,
                                 const NUExpr *rop, STBranchNode * scope)
{
  NUNet *net = NULL;
  NUNetElab* netElab = NULL;
  ConstantRange r(lop->getBitSize ()-1,0);
  UInt32 rightSize = rop->effectiveBitSize();

  if (const NUConcatOp* cat = dynamic_cast<const NUConcatOp*>(lop))
  {
    UInt32 effectiveBitSize = cat->effectiveBitSize();
    if (effectiveBitSize >= cMaxVectorBDD)
      return ctx->invalid(); // don't attempt to make a really big BDD
    UInt32 msb = effectiveBitSize - 1;
    BDD result = ctx->invalid();
    if (const NUConst* val = rop->castConstNoXZ ())
    {
      // If any bits in the RHS beyond the size of the lhs are set, then this is
      // guaranteed to compare false.
      if (rightSize > (msb + 1))
        return ctx->val0 ();

      // do a recursive comparison for each concatenated expression
      result = ctx->val1();
      UInt32 value;
      val->getUL(&value);
      UInt32 lsb = 0;
      for (UInt32 count = 0; count < cat->getRepeatCount(); ++count)
      {
        // Go from LSB to MSB
        for (SInt32 i = cat->getNumArgs() - 1; i >= 0; --i) {
          const NUExpr* subExpr = cat->getArg(i);
          UInt32 width = subExpr->getBitSize();
          msb = lsb + width - 1;
          NU_ASSERT(lsb <= msb, lop);
          if (width == 1)
          {
            // we have a single bit, so do the comparison
            BDD bit1 = ctx->bddRecurse(subExpr, scope);
            if ((value >> lsb) & 0x1)
              result = ctx->opAnd(result, bit1);
            else
              result = ctx->opAnd(result, ctx->opNot(bit1));
          }
          else
          {
            // not a single bit, give up
            return ctx->invalid();
          }
          lsb = msb + 1;
        }
      }
    }
    return result;    
  }
  
  if (const NUIdentRvalue* idNet = dynamic_cast<const NUIdentRvalue*> (lop))
  {
    netElab = idNet->getNetElab(scope);
    net = idNet->getIdent ();
    r.setMsb (net->getBitSize ()-1);
  }
  else if (const NUVarselRvalue* psNet = dynamic_cast<const NUVarselRvalue *>(lop))
  {
    const NUIdentRvalue* ident = dynamic_cast<const NUIdentRvalue*>(psNet->getIdentExpr());

    if (ident && psNet->isConstIndex ())
    {
      netElab = ident->getNetElab(scope);
      net = ident->getIdent ();
      r = *psNet->getRange ();
    }
  }
  
  if (net)
  {
    if (const NUConst* val = rop->castConstNoXZ ())
    {
      DynBitVector bitval;
      val->getSignedValue (&bitval);

      // If any bits in the RHS beyond the size of the lhs are set, then this is
      // guaranteed to compare false.
      if (rightSize > r.getLength ())
        if (not (net->isSigned () and not net->isUnsignedSubrange ()) // lhs was zero-extended
            || not (bitval.partsel (r.getMsb (), 1 + rightSize - r.getLength ()).all ()))
          // all sign bits not set, so even if LHS sign-extends, it won't match RHS pattern
          return ctx->val0 ();
      
      BDD result = ctx->val1 ();
      
      bitval.resize (r.getLength ());

      for(SInt32 i = r.getLsb (); i <= r.getMsb (); ++i) {
        BDD indexBdd;
        if (net->is2DAnything()) {
          return ctx->invalid();
        }
        else if (net->isVectorNet()) {
          indexBdd = netElab? ctx->bdd(netElab,i): ctx->bdd(net,i);
        }
        else if (i == 0) {
          indexBdd = netElab? ctx->bdd(netElab): ctx->bdd(net);
        }
        else {
          NU_ASSERT(0,rop);            // currently (04/21/05) we have no testcase that gets here
          indexBdd = ctx->val0();
        }
        BDD valBdd   = bitval.test (i-r.getLsb ()) ? ctx->val1 () : ctx->val0 ();
        BDD equalBdd = ctx->opEqual (indexBdd, valBdd);
        result = ctx->opAnd (result, equalBdd);
      }
      return result;
    }
  }

  // Other expressions besides cat/bitsel/partsel/ident are can be handled,
  // but only if they are 1-bit wide
  else if ((r.getLength() == 1) && (rop->getBitSize() == 1)) {
    // **NOTE** : I wanted to use the new BDDContext::bddRecurse for
    // this bdd's so that we can avoid blowing up bdd's. But it failed
    // because the rop expression is sometimes a temporary expression
    // that then gets immediately deleted. This means that the cache
    // would get populated with a deleted expression that then causes
    // problems downstream.
    //
    // If someone can think of a better way to deal with this, please
    // fix it, because this routine *IS* called with potentially big
    // sub expressions sometimes.
    BDD leftBDD = lop->bdd(ctx, scope);
    BDD rightBDD = rop->bdd(ctx, scope);
    return ctx->opEqual(leftBDD, rightBDD);
  }
  return ctx->invalid();
} // static BDD sGenCompareVectorBDD

bool NUBinaryOp::hasBdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize()!=1) {
    return false;
  }

  bool isSingleBit = ((mExpr1->effectiveBitSize() <= 1) &&
                      (mExpr2->effectiveBitSize() <= 1));
  switch (mOp)
  {
  case eBiPlus:
  case eBiMinus:
  case eBiDownTo:
  case eBiSMult:
  case eBiSDiv:
  case eBiSMod:
  case eBiUMult:
  case eBiUDiv:
  case eBiUMod:
  case eBiVhdlMod:
    // For now, we don't support BDDs on arithmetic operators
    return false;
    break;

  case eBiEq:
  case eBiNeq:
    if (sIsCompareVector (mExpr1))
      return true;
    else if (sIsCompareVector (mExpr2))
      return true;
    return isSingleBit &&
      mExpr1->hasBdd(ctx, scope) && mExpr2->hasBdd(ctx, scope);
    break;

  case eBiLogAnd:
  case eBiLogOr:
  case eBiBitAnd:
  case eBiBitOr:
    // optimistic predict that we can compute BDDs for any single
    // bit operation, because we even if one of the expressions is
    // invalid, if the other one is constant we may be able to get
    // an answer (e.g. 0&invalid==0)
    return isSingleBit;

  case eBiBitXor:
    // We need both operands to be valid
    return isSingleBit && mExpr1->hasBdd(ctx, scope) && mExpr2->hasBdd(ctx, scope);

  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
  case eBiULt:
  case eBiULte:
  case eBiUGtr:
  case eBiUGtre:
  {
    // support computation of relationals of constants directly, without
    // going to the engine.
    const NUConst* k0 = getArg (0)->castConst ();
    const NUConst* k1 = getArg (1)->castConst ();
    if ((k0 != NULL) && (k1 != NULL) && !k0->hasXZ() && !k1->hasXZ())
    {
      return true;
    }
    break;
  }

  default:
    break;
  }

  return false;
} // BDD NUBinaryOp::hasBdd

// We can compute relational expressions of constants, unless they are big
// and signed.
static bool sEvalRelational(NUOp::OpT op, const NUConst* k0, const NUConst* k1)
{
  bool invert = false;
  bool val = false;

  // In order to deal cleanly with signed numbers, our strategy is to
  // compare to 0 rather than comparing to one another, and check the
  // high order bit.  To reduce the cases, just implement lessThan.
  switch (op)
  {
  case NUOp::eBiSLt:
  case NUOp::eBiULt:
    break;
  case NUOp::eBiSGtre:
  case NUOp::eBiUGtre:
    // (a >= b) == !(a < b);
    invert = true;
    break;
  case NUOp::eBiSLte:
  case NUOp::eBiULte:
    // (a <= b) == !(b < a);
    std::swap(k0, k1);
    invert = true;
    break;
  case NUOp::eBiSGtr:
  case NUOp::eBiUGtr:
    // (a < b) == (b < a)
    std::swap(k0, k1);
    break;
  default:
    NU_ASSERT(0, k0);
  }

  DynBitVector v0, v1;
  k0->getSignedValue(&v0);
  k1->getSignedValue(&v1);

  if (NUOp::isSignedOperator (op))
  {
    // (a < b) == ((a - b) < 0); Test for negative by looking at high order bit
    NU_ASSERT(v0.size() == v1.size(), k0);
    v0 -= v1;
    val = v0.test(v0.size() - 1);
  }
  else
    val = v0 < v1;
  return val ^ invert;
} // static bool sEvalRelational

BDD NUBinaryOp::bdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize()!=1) {
    return ctx->invalid();
  }

  bool isSingleBit = ((mExpr1->effectiveBitSize() <= 1) &&
                      (mExpr2->effectiveBitSize() <= 1));
  switch (mOp)
  {
  case eBiPlus:
  case eBiMinus:
  case eBiDownTo:
  case eBiSMult:
  case eBiSDiv:
  case eBiSMod:
  case eBiUMult:
  case eBiUDiv:
  case eBiUMod:
  case eBiExp:
  case eBiDExp:
    // For now, we don't support BDDs on arithmetic operators
    break;

  case eBiEq:
  case eBiTrieq:
    if (sIsCompareVector (mExpr1))
      return sGenCompareVectorBDD (ctx, mExpr1, mExpr2, scope);
    else if (sIsCompareVector (mExpr2))
      return sGenCompareVectorBDD (ctx, mExpr2, mExpr1, scope);
    else if (isSingleBit) {
      BDD bdd1 = ctx->bddRecurse(mExpr1, scope);
      BDD bdd2 = ctx->bddRecurse(mExpr2, scope);
      return ctx->opEqual(bdd1, bdd2);
    }
    break;

  case eBiNeq:
  case eBiTrineq:
    if (sIsCompareVector (mExpr2))
      return ctx->opNot (sGenCompareVectorBDD (ctx, mExpr2, mExpr1, scope));
    else if (sIsCompareVector (mExpr1))
      return ctx->opNot (sGenCompareVectorBDD (ctx, mExpr1, mExpr2, scope));
    else if (isSingleBit) {
      BDD bdd1 = ctx->bddRecurse(mExpr1, scope);
      BDD bdd2 = ctx->bddRecurse(mExpr2, scope);
      return ctx->opNot(ctx->opEqual(bdd1, bdd2));
    }
    break;

  case eBiBitAnd:
  case eBiLogAnd:
  case eBiBitOr:
  case eBiLogOr:
  case eBiBitXor:
    if (isSingleBit) {
      BDD bdd1 = ctx->bddRecurse(mExpr1, scope);
      BDD bdd2 = ctx->bddRecurse(mExpr2, scope);
      switch (mOp) {
      case eBiBitAnd:
      case eBiLogAnd:   return ctx->opAnd(bdd1, bdd2);
      case eBiBitOr:
      case eBiLogOr:    return ctx->opOr(bdd1, bdd2);
      case eBiBitXor:   return ctx->opXor(bdd1, bdd2);
      default:          NU_ASSERT(0, this);
      }
    }
    break;
  case eBiSLt:
  case eBiSLte:
  case eBiSGtr:
  case eBiSGtre:
  case eBiULt:
  case eBiULte:
  case eBiUGtr:
  case eBiUGtre:
  {
    // support computation of relationals of constants directly, without
    // going to the engine.
    const NUConst* k0 = getArg(0)->castConst();
    const NUConst* k1 = getArg(1)->castConst();
    if ((k0 != NULL) && (k1 != NULL) && !k0->hasXZ() && !k1->hasXZ())
    {
      bool one = sEvalRelational(mOp, k0, k1);

      return one? ctx->val1(): ctx->val0();
    }
    break;
  } // case eBiGtre:

  case eBiRoR:
  case eBiRoL:
  case eBiRshift:
  case eBiLshift:
  case eBiVhZxt:
  case eBiVhExt:
  case eBiVhLshift:
  case eBiVhRshift:
  case eBiLshiftArith:
  case eBiRshiftArith:
  case eBiVhRshiftArith:
  case eBiVhLshiftArith:

  case eBiVhdlMod:
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }

  return ctx->invalid();
} // BDD NUBinaryOp::bdd


bool NUTernaryOp::hasBdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize()!=1) {
    return false;
  }

  switch (mOp)
  {
  case eTeCond:
    return (mExpr1->hasBdd(ctx, scope) &&
            mExpr2->hasBdd(ctx, scope) &&
            mExpr3->hasBdd(ctx, scope));
    break;

  default:
    break;
  }

  return false;
}


BDD NUTernaryOp::bdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize()!=1) {
    return ctx->invalid();
  }

  switch (mOp)
  {
  case eTeCond: {
    // introduce the BDD package to 'cond' first.
    // This order is important, as shown by test/constprop/bidi1.v,
    // where a random order may transform expression
    //    (top.S1.a ? (top.S1.b & top.S1.in) : 1)
    // into the BDD
    //    top.S1.in ? (top.S1.b | (!top.S1.a)) : (!top.S1.a)
    // That occurs if the BDD package "learns" about top.S1.a before
    // top.S1.in.  So expression traversal order is desirable.
    BDD condBdd = ctx->bddRecurse(mExpr1, scope); 
    BDD thenBdd = ctx->bddRecurse(mExpr2, scope);
    BDD elseBdd = ctx->bddRecurse(mExpr3, scope);
    return ctx->opCond(condBdd, thenBdd, elseBdd);
    break;
  }

  default:
    NU_ASSERT(0, this);
    break;
  }

  return ctx->invalid();
}

bool NUTernaryOp::isolateZ(NUExpr **enable, NUExpr **driver) const
{
  NU_ASSERT(mOp == eTeCond, this);
  const SourceLocator& enaLoc = mExpr1->getLoc();
  CopyContext copy_context(NULL,NULL);

  // If both exprs can drive Z
  if (mExpr2->drivesZ() and mExpr3->drivesZ()) {
    if (mExpr2->drivesOnlyZ() and mExpr3->drivesOnlyZ()) {
      // The result is unconditionally z.  test/bidi/complex_tri6.v
      *enable = 0;
      *driver = 0;
      return true;
    } else if (mExpr2->drivesOnlyZ() && mExpr3->isolateZ(enable, driver)) {
      // We have
      //   mExpr1 ? z : (enable ? driver : z)
      // which is equivalent to
      //   (!mExpr1 & enable) ? driver : z
      //
      // see  test/bidi/complex_tri5.v
      //
      NUExpr* e1 = mExpr1->copy(copy_context);
      NUExpr* not_e1 = new NUUnaryOp(eUnLogNot, e1, enaLoc);
      *enable = new NUBinaryOp(eBiLogAnd, not_e1, *enable, enaLoc);
    }
    else if (mExpr3->drivesOnlyZ() && mExpr2->isolateZ(enable, driver)) {
      // We have
      //   mExpr1 ? (enable ? driver : z): z
      // which is equivalent to
      //   (mExpr1 & enable) ? driver : z
      //
      // see  test/bidi/complex_tri4.v
      //
      NUExpr* e1 = mExpr1->copy(copy_context);
      *enable = new NUBinaryOp(eBiLogAnd, e1, *enable, enaLoc);
    }
    else {
      NUExpr *ena2, *drv2;
      if (mExpr2->isolateZ(enable, driver) && mExpr3->isolateZ(&ena2, &drv2)) {
        NU_ASSERT(*enable, this);
        NU_ASSERT(*driver, this);
        NU_ASSERT(ena2, this);
        NU_ASSERT(drv2, this);

        // isolated Z on both sides of ":"
        //    (mExpr1 ? (enable ? driver : 1'bz) : (ena2 ? drv2 : 1'bz))
        // which is equivalent to
        //    (mExpr1 ? enable : ena2) ? (mExpr1 ? driver : drv2) : 1'bz
        //
        // see  test/bidi/complex_tri3.v
        //
        NUExpr* e1 = mExpr1->copy(copy_context);
        *enable = new NUTernaryOp(mOp, e1, *enable, ena2, enaLoc);
        e1 = mExpr1->copy(copy_context);
        *driver = new NUTernaryOp(mOp, e1, *driver, drv2, enaLoc);
      }
      else {
        // Cannot isolate.  I don't know how to generate this testcase.
        return false;
      }
    }
  }

  // See if one of the exprs drives only z's.
  else if (mExpr2->drivesOnlyZ()) {
    *driver = mExpr3->copy(copy_context);
    *enable = new NUUnaryOp(eUnLogNot, mExpr1->copy(copy_context), enaLoc);
  } else if (mExpr3->drivesOnlyZ()) {
    *driver = mExpr2->copy(copy_context);
    *enable = mExpr1->copy(copy_context);
  }

  else if (mExpr3->isolateZ(enable, driver)) {
    // We have
    //    (mExpr1 ? mExpr2 : (enable ? driver : 1'bz))
    // which is equivalent to
    //    (mExpr1 || enable) ? (mExpr1 ? mExpr2 : driver) : 1'bz
    //
    // see  test/bidi/complex_tri1.v

    NU_ASSERT(*enable, mExpr3);
    NU_ASSERT(*driver, mExpr3);
    NUExpr* e1 = mExpr1->copy(copy_context);
    *enable = new NUBinaryOp(eBiLogOr, e1, *enable, enaLoc);
    e1 = mExpr1->copy(copy_context);
    NUExpr* a = mExpr2->copy(copy_context);
    *driver = new NUTernaryOp(eTeCond, e1, a, *driver, enaLoc);
  }

  else if (mExpr2->isolateZ(enable, driver)) {
    // We have
    //    (mExpr1 ? (enable ? driver : 1'bz): mExpr3)
    // which is equivalent to
    //    (!mExpr1 || enable) ? (mExpr1 ? driver : mExpr3) : 1'bz
    //
    // see  test/bidi/complex_tri2.v
    
    NU_ASSERT(*enable, mExpr2);
    NU_ASSERT(*driver, mExpr2);
    NUExpr* e1 = mExpr1->copy(copy_context);
    NUExpr* not_e1 = new NUUnaryOp(eUnLogNot, e1, enaLoc);
    *enable = new NUBinaryOp(eBiLogOr, not_e1, *enable, enaLoc);
    e1 = mExpr1->copy(copy_context);
    NUExpr* a = mExpr3->copy(copy_context);
    *driver = new NUTernaryOp(eTeCond, e1, *driver, a, enaLoc);
  }

  else {
    return false;
  }

  (*enable) = (*enable)->resizeSubHelper(mExpr1->getBitSize());
  (*driver) = (*driver)->resizeSubHelper(getBitSize());
  return true;
} // bool NUTernaryOp::isolateZ


bool NUExpr::drivesZ(DynBitVector*) const {
  return false;
}

//! Return true if this expression only drives a Z value, false otherwise.
bool NUExpr::drivesOnlyZ() const {
  return false;
}

bool NUTernaryOp::drivesZ(DynBitVector* bits) const
{
  if (bits == NULL) {
    return (mExpr2->drivesZ() or mExpr3->drivesZ());
  }
  DynBitVector b1, b2;
  bool ret = mExpr2->drivesZ(&b1) | mExpr3->drivesZ(&b2);
  if (ret) {
    bits->resize(getBitSize());
    *bits |= b1;
    *bits |= b2;
  }
  return ret;
}


bool NUTernaryOp::drivesOnlyZ() const
{
  return (mExpr2->drivesOnlyZ() and mExpr3->drivesOnlyZ());
}

bool NUTernaryOp::shouldPreserveZ(NUNet* net) const {
  // Note that Z will not be preserved in the condition (mExpr1), only
  // in the values (mExpr2 and mExpr3)
  return mExpr2->shouldPreserveZ(net) or mExpr3->shouldPreserveZ(net);
}


BDD NUNaryOp::bdd(BDDContext *ctx, STBranchNode * /* scope -- unused */) const
{
  if (getBitSize()!=1) {
    return ctx->invalid();
  }
  switch (mOp)
  {
  case eNaConcat:
    return ctx->invalid();
    break;

  default:
    NU_ASSERT(0, this);
    break;
  }

  return ctx->invalid();
}


BDD NUEdgeExpr::bdd(BDDContext *ctx, STBranchNode * /* scope -- unused */) const
{
  return ctx->invalid();
}


bool NUVarselRvalue::hasBdd(BDDContext*, STBranchNode*) const
{
  // Can only compute BDD for single bit (why?  We can do a vector net.  We
  // should be able to do a constant range select even if it's greater than
  // one bit.
  return ((getBitSize()==1) && mIdent->isWholeIdentifier() && 
          (mRange.getLength () == 1) && isConstIndex());
}

BDD NUVarselRvalue::bdd(BDDContext *ctx, STBranchNode * scope) const
{
  if (getBitSize()!=1) {
    return ctx->invalid();
  }
  if (not isConstIndex ()) {
    // Can't handle non-CTCE bit index, yet.
    return ctx->invalid();
  }

  if (mIdent->isWholeIdentifier() && mRange.getLength () == 1)
  {
    ConstantRange r (mRange);
    SInt32 bitno = r.getLsb ();
    
    NUNetElab* netElab = mIdent->getNetElab(scope);

    if (netElab == NULL) {
      return ctx->bdd(getIdent(),bitno);
    } else {
      return ctx->bdd(netElab,bitno);
    }
  }
  return ctx->invalid();
} // BDD NUVarselRvalue::bdd

bool NUVarselRvalue::sizeVaries () const
{
  if (mExpr->castConst ())
    return false;
  else if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(mExpr))
    return bop->getOp () == NUOp::eBiDownTo;
  return false;
}


BDD NUMemselRvalue::bdd(BDDContext *ctx, STBranchNode * /* scope -- unused */) const
{
  return ctx->invalid();
}

NUNaryOp::NUNaryOp(OpT op,
		   const NUExprVector& exprs,
		   const SourceLocator& loc) :
  NUOp(op, loc),
  mExprs(exprs)
{}

NUNaryOp::NUNaryOp(OpT op,
		   const NUCExprVector& exprs,
		   const SourceLocator& loc) :
  NUOp(op, loc),
  mExprs(exprs.size())
{
  for (size_t i = 0; i < exprs.size(); ++i)
    mExprs[i] = exprs[i]->constCast();
}

NUNaryOp::NUNaryOp(OpT op, const SourceLocator &loc) :
  NUOp(op, loc)
{
}


NUNaryOp::~NUNaryOp()
{
  if (!mIsManaged)
  {
    for (NUExprVectorIter iter = mExprs.begin();
         iter != mExprs.end();
         ++iter)
    {
      NUExpr* e = *iter;
      if (e->ownsSubExprs())
        delete e;
    }
  }
}

void NUNaryOp::manage(NUExprFactory* factory)
{
  mIsManaged = true;
  for (size_t i = 0; i < mExprs.size(); ++i)
    mExprs[i] = factory->insert(mExprs[i])->constCast();
}



UInt32 NUNaryOp::getNumArgs() const
{
  return mExprs.size();
}


const NUExpr *NUNaryOp::getArgHelper(UInt32 index) const
{
  NU_ASSERT(index < mExprs.size(), this);
  return mExprs[index];
}

NUExpr *NUNaryOp::putArgHelper(UInt32 index, NUExpr * argument) 
{
  NU_ASSERT(index < mExprs.size(), this);
  NUExpr * old_argument = getArg(index);
  mExprs[index] = argument;

  return old_argument;
}


NUConcatOp::NUConcatOp(const NUExprVector& exprs,
		       UInt32 repeat_count,
		       const SourceLocator& loc) :
  NUNaryOp(eNaConcat, exprs, loc),
  mRepeatCount(repeat_count)
{
  NU_ASSERT(mRepeatCount >= 1, this);
  mSignedResult = false;        // Verilog LRM 4.5.1
}

NUConcatOp::NUConcatOp(const NUCExprVector& exprs,
		       UInt32 repeat_count,
		       const SourceLocator& loc) :
  NUNaryOp(eNaConcat, exprs, loc),
  mRepeatCount(repeat_count)
{
  NU_ASSERT(mRepeatCount >= 1, this);
  mSignedResult = false;        // Verilog LRM 4.5.1
}

NUConcatOp::NUConcatOp(NUExpr* expr,
		       UInt32 repeat_count,
		       const SourceLocator& loc) :
  NUNaryOp(eNaConcat, NUExprVector(1,expr), loc),
  mRepeatCount(repeat_count)
{
  NU_ASSERT(mRepeatCount >= 1, this);
  mSignedResult = false;        // Verilog LRM 4.5.1
}


NUConcatOp::~NUConcatOp()
{}

void NUConcatOp::putRepeatCount(UInt32 count) {
  mRepeatCount = count;
  clearBitSizeCache();
}

bool
NUConcatOp::sizeVaries () const
{
  for(NUExprCLoop args = loopExprs (); !args.atEnd (); ++args) {
    if ((*args)->sizeVaries ())
      return true;
  }
  return false;
}

ptrdiff_t NUConcatOp::compareHelper(const NUExpr* exp, bool compareLocator,
                                    bool comparePointer, bool) const
{
  const NUConcatOp *cat = dynamic_cast<const NUConcatOp*>(exp);
  NU_ASSERT(cat, exp);

  ptrdiff_t cmp = (ptrdiff_t) mRepeatCount - (ptrdiff_t) cat->mRepeatCount;
  if (cmp == 0) {
    ptrdiff_t numArgs = getNumArgs();
    cmp = numArgs - (ptrdiff_t) cat->getNumArgs();
    for (ptrdiff_t i = 0; (cmp == 0) && i < numArgs; ++i) {
      // Inside concatenations, size of consts is significant
      cmp = getArg(i)->compare(*cat->getArg(i), compareLocator, comparePointer,
                               true); // always compare size inside concats
    }
  }        
  return cmp;
}

bool NUConcatOp::shallowEqHelper(const NUExpr& exp) const
{
  // Already know that the types match
  const NUConcatOp *cat = static_cast<const NUConcatOp*>(&exp);

  if (mRepeatCount != cat->getRepeatCount ())
    return false;

  if (not NUPtrListEqual<NUExprCLoop>(loopExprs (), cat->loopExprs ()))
    return false;

  // But wait, since different size constants will compare equal if they
  // have the same significant bits, we have to check the SIZE of each
  // argument also

  NUExprCLoop other = cat->loopExprs (); 
  for(NUExprCLoop me = loopExprs (); !me.atEnd (); ++me, ++other)
    if ((*me)->getBitSize () != (*other)->getBitSize ())
      return false;
  return true;
}

NUExpr* NUConcatOp::copyHelper(CopyContext &copy_context) const
{
  NUExprVector new_vector(mExprs.size());
  int i = 0;
  for (NUExprVectorIter iter = mExprs.begin();
       iter != mExprs.end();
       ++iter, ++i) {
    new_vector[i] = (*iter)->copy(copy_context);
  }
  NUConcatOp *expr = new NUConcatOp(new_vector, mRepeatCount, mLoc);
  expr->setSignedResult(mSignedResult);
  return expr;
}


bool NUNaryOp::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  for (NUExprVectorIter iter = mExprs.begin();
       iter != mExprs.end();
       ++iter) {
    changed |= (*iter)->replace(old_net,new_net);
  }
  return changed;
}

bool NUNaryOp::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // pre-recursion translation
  UInt32 n = mExprs.size();
  for (UInt32 i = 0; i < n; ++i) {
    changed |= replaceExpr(&mExprs[i], NuToNuFn::ePre, translator);
  }

  // recursion
  for (NUExprVector::iterator iter = mExprs.begin();
       iter != mExprs.end();
       ++iter) {
    changed |= (*iter)->replaceLeaves(translator);
  }

  // post-recursion translation
  for (UInt32 i = 0; i < n; ++i) {
    changed |= replaceExpr(&mExprs[i], NuToNuFn::ePost, translator);
  }
  return changed;
} // bool NUNaryOp::replaceLeaves

UInt32 NUConcatOp::determineBitSizeHelper() const
{
  UInt32 total = 0;
  for (NUExprVectorIter iter = mExprs.begin();
       iter != mExprs.end();
       ++iter) {
    total += (*iter)->determineBitSize();
  }

  return total * mRepeatCount;
}


void NUConcatOp::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  // The concatenation can be resized, but the sub-expressions all have self-determined size.
  mBitSize = size;
}

NUExpr* NUConcatOp::makeSizeExplicitHelper(UInt32 desired_size) {
  // The sub-expressions are all self-determined, but it's not permissible
  // to allow them to shrink to their effective bit size, unlike, say,
  // a reduction operator or !.
  for (size_t i = 1, n = mExprs.size(); i < n; ++i) {
    NUExpr* e = mExprs[i];
    mExprs[i] = e->makeSizeExplicit(e->determineBitSize());
  }
  return NUExpr::makeSizeExplicitHelper(desired_size);
}

NUExpr* NUConcatOp::coerceOperandsToReal( ) {
  return this;                  // all ops are self determined
}


UInt32 NUConcatOp::effectiveBitSizeHelper (ExprIntMap* cache) const
{
  // Remove leading zeros - 
  UInt32 total = determineBitSize (); // Start with worst-case
  for (NUExprVector::const_iterator iter = mExprs.begin ();
       iter != mExprs.end ();
       ++iter)
  {
    NUExpr* expr = *iter;
    UInt32 delta = expr->effectiveBitSize (cache);
    if (delta == 0) {
      // Reduce by width occupied by a leading zero
      total -= expr->determineBitSize ();
    } else {
      // There were some non-zero bits found, so stop here, reduced
      // by the number of leading zeros guaranteed.
      // std::max(0, used here for cases where the effectiveBitSize is
      // larger than determineBitSize such as for:
      //  {1'b0,a}+{1'b0,b}  // (a and b are single bits)
      // (from test/interra/misc2/MISC60)
      total -= std::max(0,(SInt32)(expr->determineBitSize () - delta));
      break;
    }
  }
  return total;
}

bool NUConcatOp::isWholeIdentifier() const
{
  // For now, be conservative, just handle 1 element concats.
  if (mRepeatCount != 1) {
    return false;
  }
  NUExprVectorIter first = mExprs.begin();

  if (first != mExprs.end()) {
    NUExprVectorIter second = first;
    ++second;
    if (second == mExprs.end()) {
      return (*first)->isWholeIdentifier();
    }
  }
  return false;
}


NUNet* NUConcatOp::getWholeIdentifier() const
{
  NU_ASSERT(isWholeIdentifier(), this);
  NUExprVectorIter first = mExprs.begin();
  return (*first)->getWholeIdentifier();
}


NULvalue* NUConcatOp::Lvalue(const SourceLocator &loc) const
{
  if (getRepeatCount() != 1) {
    return 0;
  }

  NULvalueVector lvalues;
  UInt32 nargs = getNumArgs();
  for (UInt32 i = 0; i < nargs; ++i) {
    const NUExpr *expr = getArg(i);
    lvalues.push_back(expr->Lvalue(loc));
  }

  return new NUConcatLvalue(lvalues, loc);
}

NUConcatOp* NUConcatOp::copyConcatOp(CopyContext& cc) const {
  NUExpr* e = copy(cc);
  NUConcatOp* c = dynamic_cast<NUConcatOp*>(e);
  NU_ASSERT(c, e);
  return c;
}
  

const char* NUConcatOp::typeStr() const
{
  return "NUConcatOp";
}


void NUConcatOp::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << typeStr() << "(" << this << ") ";
  printSize(screen);
  screen << " : [RepeatCount=" << mRepeatCount << "]" << UtIO::endl;
  if (recurse) {
    for (UInt32 i = 0; i < getNumArgs(); i++) {
      getArg(i)->print(recurse, numspaces+2);
    }
  }
}


void MsgContext::composeLocation(NUExpr* expr, UtString* location)
{
  if (expr == 0) {
    (*location) << "(null NUExpr)";
  } else {
    const SourceLocator& loc = expr->getLoc();
    UtString source;
    loc.compose(&source);
    (*location) << source;
  }
}

ptrdiff_t NUEdgeExpr::compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const
{
  const NUEdgeExpr * other = dynamic_cast<const NUEdgeExpr*>(otherExpr);
  NU_ASSERT(other, otherExpr);
  ptrdiff_t cmp = ((int) getEdge()) - ((int) other->getEdge());
  if (cmp == 0) {
    cmp = ((int) isClock()) - ((int) other->isClock());
    if (cmp == 0) {
      cmp = ((int) isReset()) - ((int) other->isReset());
      if (cmp == 0) {
        cmp = getExpr()->compare(*other->getExpr(), compareLocator, comparePointer,
                                 compareSizeOfConsts);
      }
    }
  }
  return cmp;
}

bool NUEdgeExpr::shallowEqHelper(const NUExpr & otherExpr) const
{
  const NUEdgeExpr * other = static_cast<const NUEdgeExpr*>(&otherExpr);

  return ((getEdge()==other->getEdge()) and
	  (isClock()==other->isClock()) and
	  (isReset()==other->isReset()) and
	  ((getExpr())==(other->getExpr())));
}

NUEdgeExpr* NUEdgeExpr::copyEdgeExpr(CopyContext& cc) const {
  NUExpr* e = copy(cc);
  NUEdgeExpr* ee = dynamic_cast<NUEdgeExpr*>(e);
  NU_ASSERT(ee, e);
  return ee;
}
  
ptrdiff_t NUIdentRvalue::compareHelper(const NUExpr* otherExpr, bool,
                                       bool, bool) const
{
  const NUIdentRvalue * other = dynamic_cast<const NUIdentRvalue*>(otherExpr);
  NU_ASSERT(other, otherExpr);

  return NUNet::compare(mNet, other->mNet);
}

ptrdiff_t NUVarselRvalue::compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const
{
  const NUVarselRvalue * other = dynamic_cast<const NUVarselRvalue*>(otherExpr);
  NU_ASSERT(other, otherExpr);

  // object comparison for ident equality.
  ptrdiff_t cmp = mIdent->compare(*other->mIdent, compareLocator,
                                  comparePointer, compareSizeOfConsts);
  if (cmp == 0) {
    cmp = mRange.compare(other->mRange);
    if (cmp == 0) {
      cmp = mExpr->compare(*other->mExpr, compareLocator, comparePointer,
                           compareSizeOfConsts);
    }
  }
  return cmp;
}

bool NUVarselRvalue::shallowEqHelper(const NUExpr & otherExpr) const
{
  const NUVarselRvalue * other = static_cast<const NUVarselRvalue*>(&otherExpr);
  // ptr comparison for ident equality.
  return ((mIdent==other->mIdent) and
	  (mExpr==other->mExpr) and
	  (mRange==other->mRange) and          
	  (getBitSize() == other->getBitSize()));
}

ptrdiff_t NUMemselRvalue::compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const
{
  const NUMemselRvalue* other = dynamic_cast<const NUMemselRvalue*>(otherExpr);
  NU_ASSERT(other, otherExpr);

  ptrdiff_t cmp = mIdent->compare(*other->mIdent, compareLocator, comparePointer,
                                  compareSizeOfConsts);
  const UInt32 numDims = mExprs.size();
  UInt32 i = 0;
  // Continue the compare as long as we are still equal and have more
  // dims to examine.
  while (cmp == 0 && i < numDims)
  {
    cmp = mExprs[i]->compare(*other->mExprs[i], compareLocator, comparePointer,
                             compareSizeOfConsts);
    ++i;
    
  }
  return cmp;
}

bool NUMemselRvalue::shallowEqHelper(const NUExpr & otherExpr) const
{
  const NUMemselRvalue * other = static_cast<const NUMemselRvalue*>(&otherExpr);
  // ptr comparison for net equality.
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    // Check all the indicies
    if ( getIndex(i) != other->getIndex(i) )
    {
      return false;
    }
  }
  return ((mIdent==other->mIdent) and
	  (getBitSize() == other->getBitSize()));
}

ptrdiff_t NUOp::compareHelper(const NUExpr* otherExpr, bool compareLocator,
                    bool comparePointer, bool compareSizeOfConsts) const {
  const NUOp * other = dynamic_cast<const NUOp*>(otherExpr);
  NU_ASSERT(other, otherExpr);

  ptrdiff_t cmp = ((int) mOp) - ((int) other->mOp);
  if (cmp == 0) {
    UInt32 n = getNumArgs();
    cmp = ((ptrdiff_t) n) - ((ptrdiff_t) other->getNumArgs());
    for (UInt32 i = 0 ; (cmp == 0) && (i < n) ; ++i ) {
      cmp = getArg(i)->compare(*other->getArg(i), compareLocator, comparePointer,
                               compareSizeOfConsts);
    }
  }
  return cmp;
}

bool NUOp::shallowEqHelper(const NUExpr & otherExpr) const
{
  const NUOp * other = dynamic_cast<const NUOp*>(&otherExpr);

  bool consistent = ((other != NULL) and
                     (getOp()==other->getOp()) and
		     (getNumArgs()==other->getNumArgs()) and
                     (getBitSize() == other->getBitSize()));
  
  for ( UInt32 i = 0, n = getNumArgs() ; consistent && i < n ; ++i ) {
    consistent = ((getArg(i))==(other->getArg(i)));
  }
  return consistent;
}


NUNetElab* NUExpr::getNetElab(STBranchNode*)
  const
{
  NU_ASSERT("not a NUIdentRvalueElab" == 0, this);
  return NULL;
}

size_t NUEdgeExpr::hash() const
{
  return (size_t(getType()) +
          size_t(mEdge) +
          mExpr->hash());
}

size_t NUIdentRvalue::hash() const
{
  return (size_t(getType()) +
          size_t(mNet));
}


size_t NUIdentRvalueElab::hash() const
{
  return (size_t(getType()) +
          size_t(mNetElab));
}

size_t NUVarselRvalue::hash() const
{
  return (size_t(getType()) +
          mIdent->hash() +
          mRange.hash () +
          mExpr->hash());
}

size_t NUMemselRvalue::hash() const
{
  size_t hashval = size_t(getType()) + mIdent->hash();
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    hashval = 17 * hashval + mExprs[i]->hash();
  }
  return hashval;
}

size_t NUUnaryOp::hash() const
{
  return (size_t(getType()) +
          size_t(mOp) +
          mExpr->hash());
}

size_t NUBinaryOp::hash() const
{
  return (size_t(getType()) +
          size_t(mOp) +
          mExpr1->hash() +
          mExpr2->hash());
}

size_t NUTernaryOp::hash() const
{
  return (size_t(getType()) +
          size_t(mOp) +
          mExpr1->hash() +
          mExpr2->hash() +
          mExpr3->hash());
}


size_t NUNaryOp::hash() const
{
  size_t h = size_t(getType()) + size_t(mOp);
  for (NUExprVector::const_iterator p = mExprs.begin(), e = mExprs.end();
       p != e; ++p)
    h = 2*h + (*p)->hash();
  return h;
}
          
size_t NUConcatOp::hash() const
{
  return 11*NUNaryOp::hash() + mRepeatCount;
}

size_t NUEdgeExpr::shallowHash() const
{
  return (size_t(getType()) +
          size_t(mEdge) +
          size_t(mExpr));
}

size_t NUIdentRvalue::shallowHash() const
{
  return (size_t(getType()) +
          size_t(mNet));
}


size_t NUIdentRvalueElab::shallowHash() const
{
  return (size_t(getType()) +
          size_t(mNetElab));
}

size_t NUVarselRvalue::shallowHash() const
{
  return (size_t(getType()) +
          size_t(mIdent) +
          mRange.hash() +
          size_t(mExpr));
}


size_t NUMemselRvalue::shallowHash() const
{
  size_t hashval = size_t(getType()) + size_t(mIdent);
  const UInt32 size = getNumDims();
  for ( UInt32 i = 0; i < size; ++i ) {
    hashval += size_t(mExprs[i]);
  }
  return hashval;
}

size_t NUUnaryOp::shallowHash() const
{
  return (size_t(getType()) +
          size_t(mOp) +
          size_t(mExpr));
}

size_t NUBinaryOp::shallowHash() const
{
  return (size_t(getType()) +
          size_t(mOp) +
          size_t(mExpr1) +
          size_t(mExpr2));
}

size_t NUTernaryOp::shallowHash() const
{
  return (size_t(getType()) +
          size_t(mOp) +
          size_t(mExpr1) +
          size_t(mExpr2) +
          size_t(mExpr3));
}


size_t NUNaryOp::shallowHash() const
{
  size_t h = size_t(getType()) + size_t(mOp);
  for (NUExprVector::const_iterator p = mExprs.begin(), e = mExprs.end();
       p != e; ++p)
    h = 2*h + size_t(*p);
  return h;
}
          
size_t NUConcatOp::shallowHash() const
{
  return 11*NUNaryOp::shallowHash() + mRepeatCount;
}

NUNullExpr::NUNullExpr(const SourceLocator& loc) :
  NUExpr(loc)
{
  mIndex = 0;
}

NUNullExpr::~NUNullExpr()
{
}

NUExpr* NUNullExpr::copyHelper(CopyContext & /* copy_context --unused */) const
{
  NUNullExpr *expr = new NUNullExpr( mLoc );
  return expr;
}

bool NUExpr::isHierRef() const { return false; }


bool NUIdentRvalue::isHierRef() const
{
  return mNet->isHierRef();
}

bool NUIdentRvalue::shouldPreserveZ(NUNet* net) const
{
  return mNet == net;
}

bool NUVarselRvalue::isHierRef() const
{
  return mIdent->isHierRef();
}

bool NUVarselRvalue::shouldPreserveZ(NUNet* net) const
{
  // Probably in synthesized gates, Z would not be preserved on dyn bitsels,
  // but in Verilog HDL semantics they would.
  return mIdent->shouldPreserveZ(net);
}

bool NUMemselRvalue::isHierRef() const
{
  return mIdent->isHierRef();
}

bool NUConcatOp::isHierRef() const
{
  return isWholeIdentifier() && getWholeIdentifier()->isHierRef();
}

bool NUConcatOp::drivesZ(DynBitVector* bits) const
{
  bool ret = false;
  UInt32 offset = 0;
  UInt32 sz = getBitSize();
  if (bits != NULL) {
    bits->resize(sz);
  }

  // If keeping track of which bits are potentially 'z', then we have
  // to loop over every repeat count
  for (SInt32 j = bits? mRepeatCount: 1; j >= 1; --j) {
    for (SInt32 i = getNumArgs() - 1; i >= 0; --i) {
      const NUExpr* elt = getArg(i);
      if (bits != NULL) {
        DynBitVector cbits;
        if (elt->drivesZ(&cbits)) {
          cbits.resize(sz);
          cbits <<= offset;
          *bits |= cbits;
          ret = true;
        }
      }
      else if (elt->drivesZ()) {
        return true;
      }
      offset += elt->getBitSize();
    }
  }
  return ret;
} // bool NUConcatOp::drivesZ

bool NUConcatOp::drivesOnlyZ() const
{
  bool only_z = true;
  if (getNumArgs() == 0) {
    only_z = false;
  }
  for (NUExprCLoop loop = loopExprs(); not loop.atEnd(); ++loop) {
    only_z &= (*loop)->drivesOnlyZ();
  }
  return only_z;
}

bool NUConcatOp::shouldPreserveZ(NUNet* net) const
{
  for (UInt32 i = 0, n = getNumArgs(); i < n; ++i) {
    const NUExpr* expr = getArg(i);
    if (expr->shouldPreserveZ(net)) {
      return true;
    }
  }
  return false;
}


bool
NUIdentRvalue::isReal() const
{
  return mNet->isReal();
}

void NUExpr::printAssertInfoHelper() const {
  printVerilog(true);
}

NUChangeDetect::NUChangeDetect(NUExpr* expr) :
  NUUnaryOp(eUnChange, expr, expr->getLoc()), mTempNet(NULL)
{}

NUChangeDetect::NUChangeDetect(const NUExpr* expr) :
  NUUnaryOp(eUnChange, expr, expr->getLoc()), mTempNet(NULL)
{}

NUChangeDetect::~NUChangeDetect()
{}

NUExpr* NUChangeDetect::copyHelper(CopyContext& copy_context) const
{
  // Should not happen after we have set up the temp net
  NU_ASSERT((mTempNet == NULL), this);

  const NUExpr* origExpr = getArg(0);
  NUChangeDetect *expr = new NUChangeDetect(origExpr->copy(copy_context));
  return expr;
}

void NUChangeDetect::putTempNet(NUNet* tempNet)
{
  // Should not happen more than once
  NU_ASSERT((mTempNet == NULL), this);

  mTempNet = tempNet;
}


void NUChangeDetect::print(bool, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << "changeDetect(" << this << ") ";
  printSize(screen);
  screen << " : " << getOpChar(false) << UtIO::endl;
  if (mTempNet == NULL) {
    screen << " : NULL";
  } else {
    mTempNet->print (true, indent_arg + 2);
  }
}


NULut::NULut(NUExpr *expr,
             const NUConstVector &table,
             const SourceLocator& loc) :
  NUNaryOp(eNaLut, loc),
  mName(0)
{
  mExprs.push_back(expr);
  for (NUConstVector::const_iterator iter = table.begin(); iter != table.end(); ++iter) {
    mExprs.push_back(*iter);
  }
}


NULut::NULut(const NUExpr *expr,
             const NUCConstVector &table,
             const SourceLocator& loc) :
  NUNaryOp(eNaLut, loc),
  mName(0)
{
  mExprs.push_back(expr->constCast());
  for (NUCConstVector::const_iterator iter = table.begin(); iter != table.end(); ++iter) {
    const NUConst* k = *iter;
    NU_ASSERT(k->isManaged(), k);
    mExprs.push_back(const_cast<NUConst*>(k));
  }
}


NULut::~NULut()
{
}


UInt32 NULut::determineBitSizeHelper() const
{
  // Just return the size of the first constant.  All the constants must
  // be the same size.
  return getArg(1)->determineBitSize();
}


UInt32 NULut::effectiveBitSizeHelper(ExprIntMap* cache) const
{
  if (isSignedResult()) {
    return getBitSize();
  } else {
    // For unsigned results, use max effective bit size of the constant values.
    UInt32 result = 0;
    for (UInt32 idx = 0; idx < numTableRows(); ++idx) {
      result = std::max(result, getArg(idx+1)->effectiveBitSize(cache));
    }
    return result;
  }
}


NUExpr* NULut::copyHelper(CopyContext &copy_context) const
{
  const NUExpr *arg0 = getArg(0);

  NUConstVector table_copy(numTableRows());
  for (UInt32 idx = 0; idx < table_copy.size(); ++idx) {
    table_copy[idx] = dynamic_cast<NUConst*>(getArg(idx+1)->copy(copy_context));
  }

  NULut* lut_copy = new NULut(arg0->copy(copy_context), table_copy, mLoc);

  if (mName != NULL) {
    lut_copy->putTableName(mName);
  }

  return lut_copy;
}


UInt32 NULut::numTableRows() const
{
  return (getNumArgs() - 1);
}


void NULut::putTableName(StringAtom *name)
{
  mName = name;
}


StringAtom* NULut::getTableName() const
{
  return mName;
}


bool NULut::hasBdd(BDDContext *, STBranchNode *) const
{
  return (getBitSize() == 1);
}


BDD NULut::bdd(BDDContext *ctx, STBranchNode *scope) const
{
  if (getBitSize() != 1) {
    return ctx->invalid();
  }

  NUExprCLoop exprs = loopExprs();

  const NUExpr *select = *exprs;
  UInt32 select_width = select->getBitSize();
  const SourceLocator &loc = select->getLoc();

  // Create sum-of-products representation.
  //
  // From the code review, interesting idea, but is not implemented here:
  //   The BDD processing may be smaller
  //   & faster if you do a first pass over the rows and see if there are more
  //   1s than 0s.  Then you could do a SOP of the 0-rows and opNot
  //   the result.   Of course you will get the same result but I would
  //   guess it would blow up less along the way. 
  //
  BDD result = ctx->val0();
  UInt32 idx = 0;
  for (++exprs; not exprs.atEnd(); ++exprs, ++idx) {
    const NUConst *row = (*exprs)->castConst ();
    NU_ASSERT(row, this);

    UInt32 val;
    bool ok = row->getUL(&val);
    NU_ASSERT(ok, this);

    // Ignore 0-valued rows, just generate 1 rows.
    if (val) {
      NUConst *const_idx = NUConst::create( false, idx, select_width, loc);
      BDD term = sGenCompareVectorBDD(ctx, select, const_idx, scope);
      result = ctx->opOr(result, term);
      delete const_idx;
    }
  }

  return result;
}


void NULut::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  // Have to resize the sub expressions to the result size so that
  // we will codegen lut of uniform size (bug 3911).
  // But the selector is self-determined, which is the first expression.
  mBitSize = size;
  mExprs[0] = mExprs[0]->resizeSubHelper(mExprs[0]->determineBitSize());
  NU_ASSERT(mExprs.size() > 1, this);
  for (UInt32 i = 1; i < mExprs.size(); ++i) {
    mExprs[i] = mExprs[i]->resizeSubHelper(size);
  }
}


const char* NULut::typeStr() const
{
  return "NULut";
}


NUExpr::Type NULut::getType() const
{
  return eNULut;
}

NUExpr* NULut::makeSizeExplicitHelper(UInt32 desired_size) {
  // Resize the sub expressions to the result size so that
  // we will codegen lut of uniform size (bug 3911).
  // But the selector is self-determined, which is at index 0.
  for (size_t i = 1, n = mExprs.size(); i < n; ++i) {
    mExprs[i] = mExprs[i]->makeSizeExplicit(desired_size);
  }
  return  NUExpr::makeSizeExplicitHelper(desired_size);
}

NUExpr* NULut::coerceOperandsToReal( ) {
  NUExpr* ret_expr = this;

  for (size_t i = 1, n = mExprs.size(); i < n; ++i) {
    if ( not mExprs[i]->isReal() ) {
      mExprs[i] = mExprs[i]->coerceOperandsToReal( );
    }
  }
  return ret_expr->NUExpr::coerceOperandsToReal();
}


BDD NUNullExpr::bdd(BDDContext * ctx, STBranchNode* /* scope -- unused*/) const
{
  return ctx->invalid();
}

//! Function to return the expression type
NUExpr::Type NUNullExpr::getType() const { return NUExpr::eNUNullExpr; }

bool NUExpr::shouldPreserveZ(NUNet*) const {
  return false;
}

//! Return the size this expression "naturally" is.
UInt32 NUNullExpr::determineBitSizeHelper() const {return 0;}

//! Resize this expression to be the given size.  
void NUNullExpr::resizeHelper(UInt32 size) {
  RESIZE_INVARIANT (size);
  mBitSize = size;
}

//! Replace all references to old_net with new_net.
bool NUNullExpr::replace(NUNet * /* old_net -- unused*/ ,
                         NUNet * /* new_net --unused */)
{
  return(false);
}

//! Replace all net references based on a translation functor.
bool NUNullExpr::replaceLeaves(NuToNuFn & /* translator --unused*/)
{
  return(false);
}

// no cost for NUNullExpr
void NUNullExpr::calcCost(NUCost*, NUCostContext*) const {
}

//! Return the set of nets this node reads from, always empty
void NUNullExpr::getUses(NUNetSet* /*uses -- unused */) const {}
void NUNullExpr::getUses(NUNetRefSet * /* uses -- unused */) const {}
void NUNullExpr::getUsesElab(STBranchNode* /* scope -- unused */, NUNetElabSet* /* uses -- unused */) const {}
void NUNullExpr::getUsesElab(STBranchNode* /* scope -- unused */, NUNetElabRefSet2* /* uses -- unused */) const {}
bool NUNullExpr::queryUses(const NUNetRefHdl &/* use_net_ref -- unused */,
                           NUNetRefCompareFunction /* fn -- unused */,
                           NUNetRefFactory * /* factory -- unused */) const {return false;}

ptrdiff_t NUNullExpr::compareHelper(const NUExpr* otherExpr, bool,
                                    bool, bool) const
{
  NU_ASSERT(dynamic_cast<const NUNullExpr*>(otherExpr), otherExpr);
  return 0;  //! return 0 if both are NUNullExpr since any two are equivalent.
}

//! Compose for a NUNullExpr generates a single space
void NUNullExpr::composeHelper(UtString* buf, const STBranchNode* /* scope --unused */,
                               bool /* includeRoot --unused */,
                               bool /* hierName --unused */,
                               const char* /* separator --unused */ ) const
{
  *buf << "/* the null expression ',,' */ ";
}

//! Return class name
const char* NUNullExpr::typeStr() const
{
  return "NUNullExpr";
}


void NUNullExpr::print(bool /* recurse_arg */, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  indent(numspaces);
  mLoc.print();
  UtIO::cout() << typeStr() << "(" << this << ")" << " ,,\n";
}

//! Emit C++ Code 
CGContext_t NUNullExpr::emitCode (CGContext_t) const
{
  // we should never get here, but with incorrect verilog it is
  // possible, and in that case we default to generating a constant 0.
  // You might think that a nullExpr should emit a literal space, but
  // the nullExpr is only a space in the output display when it is
  // unaccompanied by a format specifier, when the nullExpr is used as
  // an argument for a format specifier then nc generates an error,
  // aldec does not.
  UtOStream &out = gCodeGen->CGOUT ();
  out << "UInt32(0x0)";
  return eCGPrecise;
}

//! hash an expression
size_t NUNullExpr::hash() const
{
  return (size_t(getType()));
}

//! shallow-hash an expression -- used for NUExprFactory
size_t NUNullExpr::shallowHash() const
{
  return hash();
}
