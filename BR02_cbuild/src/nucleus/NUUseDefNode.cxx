// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUUseDefNode.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUCycle.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUNetRefMap.h"
#include "bdd/BDD.h"
#include "util/UtOrder.h"
#include "codegen/codegen.h"
#include "codegen/CGProfile.h"

/*!
  \file
  Implementation of NUUseDefNode.
 */


bool NUUseDefNode::isSimpleBufOrAssign() const
{
  return false;
}


bool NUUseDefNode::isPortConnOutput() const
{
  return false;
}


bool NUUseDefNode::isPortConnInput() const
{
  return false;
}


bool NUUseDefNode::isPortConnBid() const
{
  return false;
}

bool NUUseDefNode::isPortConn() const
{
  return false;
}

StringAtom* NUUseDefNode::getName() const
{
  return NULL;
}


void NUUseDefNode::printUDSets(NUNetRefFactory *factory) const
{
  NUNetRefSet defs(factory);
  getDefs(&defs);
  if (not defs.empty()) UtIO::cout() << "Def:" << UtIO::endl;
  for (NUNetRefSetIter defs_iter = defs.begin();
       defs_iter != defs.end();
       ++defs_iter) {
    (*defs_iter)->print(2);

    NUNetRefSet uses(factory);
    getUses(*defs_iter, &uses);
    if (not uses.empty()) UtIO::cout() << "    Uses:" << UtIO::endl;
    uses.print(6);
  }
}


void NUUseDefNode::helperFixupUseDef(NUNetRefNetRefMultiMap *m, NUNetList *remove_list)
{
  for (NUNetList::iterator iter = remove_list->begin();
       iter != remove_list->end();
       ++iter) {
    m->erase(*iter);
  }
}


void NUUseDefNode::helperFixupUseDef(NUNetRefSet *s, NUNetList *remove_list)
{
  NUNetRefFactory *factory = s->getFactory();
  for (NUNetListIter iter = remove_list->begin();
       iter != remove_list->end();
       ++iter) {
    NUNetRefHdl net_ref = factory->createNetRef(*iter);
    s->erase(net_ref, &NUNetRef::overlapsSameNet);
  }
}

void NUUseDefStmtNode::getUses(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}

void NUUseDefStmtNode::getUses(NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUUseDefStmtNode::queryUses(const NUNetRefHdl & /* use_net_ref -- unused */,
                                 NUNetRefCompareFunction /* fn -- unused */,
                                 NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}

void NUUseDefStmtNode::getUses(const NUNetRefHdl & /* net_ref -- unused */,
                               NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUUseDefStmtNode::queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
                                 const NUNetRefHdl & /* use_net_ref -- unused */,
                                 NUNetRefCompareFunction /* fn -- unused */,
                                 NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUUseDefStmtNode::getDefs(NUNetSet * /* defs -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUUseDefStmtNode::getDefs(NUNetRefSet * /* defs -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUUseDefStmtNode::queryDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
                                 NUNetRefCompareFunction /* fn -- unused */,
                                 NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}

void NUUseDefNode::printVerilog(bool addNewline, int numSpaces, bool recurse) const
{
  UtString buf;
  this->compose (&buf, NULL /* scope */,  numSpaces, recurse);
  UtIO::cout () << buf;
  if (addNewline)
    UtIO::cout () << UtIO::endl;
}

const NUModule* NUUseDefStmtNode::findParentModule() const
{
  NUNetSet defs;
  NUNet* net;

  // Try the blocking defs first
  getBlockingDefs(&defs);
  if (defs.empty())
  {
    getNonBlockingDefs(&defs);
    NU_ASSERT(!defs.empty(), this);
  }
  net = *defs.begin();
  return net->getScope()->getModule();
}

NUModule* NUUseDefStmtNode::findParentModule() 
{
  NUNetSet defs;
  NUNet* net;

  // Try the blocking defs first
  getBlockingDefs(&defs);
  if (defs.empty())
  {
    getNonBlockingDefs(&defs);
    if (defs.empty()) {
      return NULL;
    }
  }
  net = *defs.begin();
  return net->getScope()->getModule();
}

void NUUseDefStmtNode::printUDSets(NUNetRefFactory *factory) const
{
  NUNetRefSet defs(factory);
  getBlockingDefs(&defs);
  if (not defs.empty()) UtIO::cout() << "BlockingDef:" << UtIO::endl;
  for (NUNetRefSetIter defs_iter = defs.begin();
       defs_iter != defs.end();
       ++defs_iter) {
    (*defs_iter)->print(2);

    NUNetRefSet uses(factory);
    getBlockingUses(*defs_iter, &uses);
    if (not uses.empty()) UtIO::cout() << "    Uses:" << UtIO::endl;
    uses.print(6);
  }

  defs.clear();
  getNonBlockingDefs(&defs);
  if (not defs.empty()) UtIO::cout() << "NonBlockingDef:" << UtIO::endl;
  for (NUNetRefSetIter defs_iter = defs.begin();
       defs_iter != defs.end();
       ++defs_iter) {
    (*defs_iter)->print(2);

    NUNetRefSet uses(factory);
    getNonBlockingUses(*defs_iter, &uses);
    if (not uses.empty()) UtIO::cout() << "    Uses:" << UtIO::endl;
    uses.print(6);
  }

  NUNetRefSet kills(factory);
  getBlockingKills(&kills);
  if (not kills.empty()) UtIO::cout() << "BlockingKill:" << UtIO::endl;
  kills.print(2);

  kills.clear();
  getNonBlockingKills(&kills);
  if (not kills.empty()) UtIO::cout() << "NonBlockingKill:" << UtIO::endl;
  kills.print(2);
}


bool NUUseDefNode::stopsFlow(NUNet*, bool*)
{
  return false;
}


BDD NUUseDefNode::bdd(NUNet* /*net*/, BDDContext *ctx,
                      STBranchNode* /*scope*/)
  const
{
  return ctx->invalid();
}

int NUUseDefNode::compare(const NUUseDefNode* ud1, const NUUseDefNode* ud2)
{
  int cmp;
  if (ud1 == ud2)
    cmp = 0;

  // Check for NULL use defs (bound nodes)
  else if (ud1 == 0)
    cmp = 1;
  else if (ud2 == 0)
    cmp = -1;

  // Check for cycles
  else if (ud1->isCycle() && ud2->isCycle())
  {
    const NUCycle* cycle1 = dynamic_cast<const NUCycle*>(ud1);
    const NUCycle* cycle2 = dynamic_cast<const NUCycle*>(ud2);
    cmp = cycle1->getID() - cycle2->getID();
    NU_ASSERT2(cmp != 0, ud1, ud2);
  }
  else if (ud1->isCycle())
    cmp = 1;
  else if (ud2->isCycle())
    cmp = -1;

  // Everything else
  else
  {
    // We have valid ud pointers, compare their location
    cmp = SourceLocator::compare(ud1->getLoc(), ud2->getLoc());
    if (cmp == 0)
    {
      // Same file and line, sort by construct name
      const char* s1 = ud1->getName()->str();
      const char* s2 = ud2->getName()->str();

      if (s1 != NULL && s2 != NULL)
      {
        cmp = strcmp(s1, s2);
        if (cmp == 0)
        {
          // Often we compare named blocks to always blocks at same line#
          cmp = ((int) ud1->getType()) - ((int) ud2->getType());          
          if (cmp == 0)
          {
            if (ud1->getType() == eNUModuleInstance)
            {
              // Must be instances in generate blocks since their names and
              // source lines are identical. However their parent names won't be.
              // Their parents would be named declaration scopes with unique names.
              const NUModuleInstance* inst1 = dynamic_cast<const NUModuleInstance*>(ud1);
              const NUModuleInstance* inst2 = dynamic_cast<const NUModuleInstance*>(ud2);
              cmp = NUModuleInstance::compare(inst1, inst2);
            }
            else
            {
              // We must have a parameterized module. So the only way to
              // tell the difference is to compare the parent modules (which
              // must be different).
              const NUModule* m1 = ud1->findParentModule();
              const NUModule* m2 = ud2->findParentModule();
              cmp = NUModule::compare(m1, m2);
            }
            NU_ASSERT2(cmp != 0, ud1, ud2);
          }
        } // if
      }
      else if (s1 == NULL && s2 != NULL)
        cmp = -1;
      else if (s1 != NULL && s2 == NULL)
        cmp = +1;
    }

    if (cmp == 0)
    {
      // If we get here, fall back to using the pointer to determine ordering
      cmp = carbonPtrCompare<NUUseDefNode>(ud1,ud2);
    }
    
  } // else
  return cmp;
} // int NUUseDefNode::compare


void NUUseDefNode::printAssertInfoHelper() const {
  printVerilog(true, 0, true);
}

bool NUUseDefNode::drivesZ(NUNet*) const {
  return false;
}

bool NUUseDefNode::shouldPreserveZ(NUNet*, NUNet*) const {
  return false;
}

void NUUseDefNode::emitStartBucket() const
{
  gCodeGen->getCGProfile()->emitStartBucket(true, getLoc());
}

bool NUUseDefStmtNode::useBlockingMethods() const {
  return true;  // ContAssign and ContEnabledDriver override this
}

