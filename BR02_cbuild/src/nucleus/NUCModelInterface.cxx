// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModelPort.h"
#include "util/StringAtom.h"

NUCModelInterface::NUCModelInterface(const SourceLocator& loc,
				     StringAtom* name,
                                     bool isCallOnPlayback) :
  mLoc(loc), mName(name), mNameIndex(0), mNumVariants(1),
  mIsCallOnPlayback(isCallOnPlayback)
{
  mCModelFnVector = new NUCModelFnVector;
  mContextStrings = new ContextStrings;
  mPorts = new NUCModelPortVector;
  mSortedPorts = new SortedPorts;
}

NUCModelInterface::~NUCModelInterface()
{
  for (NUCModelFnLoop l = loopFunctions(); !l.atEnd(); ++l)
    delete *l;
  delete mCModelFnVector;
  delete mContextStrings;
  for (PortsLoop l = loopPorts(); !l.atEnd(); ++l)
    delete *l;
  delete mPorts;
  delete mSortedPorts;
}

void NUCModelInterface::addFunction(NUCModelFn* fn)
{
  mCModelFnVector->push_back(fn);
  const UtString* context = fn->getContext();
  if (context != NULL)
    mContextStrings->insert(context);
}

NUCModelFnCLoop NUCModelInterface::loopFunctions() const
{
  return NUCModelFnCLoop(*mCModelFnVector);
}

NUCModelFnLoop NUCModelInterface::loopFunctions()
{
  return NUCModelFnLoop(*mCModelFnVector);
}

NUCModelInterface::ContextStringsCLoop
NUCModelInterface::loopContextStrings() const
{
  return ContextStringsCLoop(*mContextStrings);
}

NUCModelInterface::ContextStringsLoop NUCModelInterface::loopContextStrings()
{
  return ContextStringsLoop(*mContextStrings);
}


void NUCModelInterface::addPort(NUCModelPort* port)
{
  mPorts->push_back(port);
  const UtString* name = port->getName();
  mSortedPorts->insert(SortedPorts::value_type(name, port));
}

NUCModelInterface::PortsLoop NUCModelInterface::loopPorts()
{
  return PortsLoop(*mPorts);
}

NUCModelInterface::PortsCLoop NUCModelInterface::loopPorts() const
{
  return PortsCLoop(*mPorts);
}

bool
NUCModelInterface::ComparePorts::operator()(const UtString* s1,
					    const UtString* s2)
  const
{
  return s1->compare(*s2) < 0;
}

NUCModelPort* NUCModelInterface::findPort(const UtString* name) const
{
  SortedPorts::iterator pos = mSortedPorts->find(name);
  if (pos == mSortedPorts->end())
    return NULL;
  else
    return pos->second;
}

UtString* NUCModelInterface::makeName()
{
  UtString* name = new UtString;
  *name << mName->str() << ++mNameIndex;
  return name;
}

bool NUCModelInterface::CompareStrings::operator()(const UtString* s1,
					  const UtString* s2) const
{
  return s1->compare(*s2) < 0;
}

bool NUCModelInterface::hasContextStrings() const
{
  return !mContextStrings->empty();
}

NUCModelInterface::PortsLoopNoNull NUCModelInterface::loopPortsNoNull()
{
  return PortsLoopNoNull(loopPorts(), NullPortFilter());
}

NUCModelInterface::PortsCLoopNoNull NUCModelInterface::loopPortsNoNull() const
{
  return PortsCLoopNoNull(loopPorts(), NullPortFilter());
}

bool NUCModelInterface::NullPortFilter::operator()(NUCModelPort* port) const
{
  return !port->isNullPort();
}

bool NUCModelInterface::hasNullPort() const
{
  return getNullPort() != NULL;
}

NUCModelPort *NUCModelInterface::getNullPort() const
{
  NUCModelPort *nullPort = NULL;
  for (PortsCLoop l = loopPorts(); !l.atEnd() && nullPort == NULL; ++l) {
    NUCModelPort* port = *l;
    if (port->isNullPort())
      nullPort = port;
  }
  return nullPort;
}

UInt32 NUCModelInterface::numPorts() const {
  return mPorts->size();
}
