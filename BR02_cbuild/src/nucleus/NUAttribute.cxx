// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUAttribute.h"
#include "nucleus/NUNetRefSet.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"
#include "NUExprI.h"

/*!
  \file
  Implementation of NUAttribute.
*/

NUAttribute::NUAttribute( NUExpr *expr, const AttribT attrib, const SourceLocator& loc )
  : NUExpr( loc ), mExpr( expr ), mAttrib( attrib ), mLoc( loc ) {}



NUAttribute::~NUAttribute()
{
  if (!mIsManaged && mExpr->ownsSubExprs())
    delete mExpr;
}


const char*
NUAttribute::typeStr() const
{
  return "NUAttribute";
}


const char*
NUAttribute::getAttribChar() const
{
  switch ( mAttrib )
  {
  case NUAttribute::eStart:          break;
  case NUAttribute::eLeft:           return "LEFT";
  case NUAttribute::eRight:          return "RIGHT";
  case NUAttribute::eHigh:           return "HIGH";
  case NUAttribute::eLow:            return "LOW";
  case NUAttribute::eLength:         return "LENGTH";
  case NUAttribute::eStructure:      break;
  case NUAttribute::eBehavior:       break;
  case NUAttribute::eValue:          return "VALUE";
  case NUAttribute::ePos:            return "POS";
  case NUAttribute::eVal:            return "VAL";
  case NUAttribute::eSucc:           return "SUCC";
  case NUAttribute::ePred:           return "PRED";
  case NUAttribute::eLeftof:         return "LEFTOF";
  case NUAttribute::eRightof:        return "RIGHTOF";
  case NUAttribute::eEvent:          return "EVENT";
  case NUAttribute::eActive:         return "ACTIVE";
  case NUAttribute::eLastEvent:      return "LAST_EVENT";
  case NUAttribute::eLastValue:      return "LAST_VALUE";
  case NUAttribute::eLastActive:     return "LAST_ACTIVE";
  case NUAttribute::eDelayed:        return "DELAYED";
  case NUAttribute::eStable:         return "STABLE";
  case NUAttribute::eQuiet:          return "QUIET";
  case NUAttribute::eTransaction:    return "TRANSACTION";
  case NUAttribute::eBase:           return "BASE";
  case NUAttribute::eRange:          return "RANGE";
  case NUAttribute::eReverseRange:   return "REVERSE_RANGE";
  case NUAttribute::eAscending:      return "ASCENDING";
  case NUAttribute::eImage:          return "IMAGE";
  case NUAttribute::eDriving:        return "DRIVING";
  case NUAttribute::eDrivingValue:   return "DRIVING_VALUE";
  case NUAttribute::eSimpleName:     return "SIMPLE_NAME";
  case NUAttribute::eInstanceName:   return "INSTANCE_NAME";
  case NUAttribute::ePathName:       return "PATH_NAME";
  case NUAttribute::eUserDefAttrib:  break;
  case NUAttribute::eInvalid:        break;
  }
  INFO_ASSERT( 0, "Attribute with no printable name" );
  return NULL;
}


void NUAttribute::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; ++i)
    screen << " ";
  mLoc.print();
  screen << "NUAttribute(" << this << ") : " << getAttribChar() << UtIO::endl;
  if ( recurse && mExpr )
  {
    mExpr->print( recurse, numspaces + 2 );
  }
}


bool
NUAttribute::replaceLeaves( NuToNuFn & translator )
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // leaf node; only perform local replacement
  NUExpr *replacement = translator( mExpr, NuToNuFn::ePrePost );
  if ( replacement ) {
    mExpr = replacement;
    changed = true;
  }
  return changed;
}


void
NUAttribute::getUses( NUNetSet *uses ) const
{
  mExpr->getUses( uses );
}


void
NUAttribute::getUses( NUNetRefSet *uses ) const
{
  mExpr->getUses( uses );
}

void NUAttribute::resizeHelper(UInt32 size) {
  mBitSize = size;
  // The attribute expression size is self-determined.
  mExpr->resizeSubHelper(mExpr->determineBitSize());
}

//! Return a copy of this expression tree.
NUExpr* NUAttribute::copyHelper(CopyContext &/*copy_context*/) const {
  NU_ASSERT(0, this);
  return NULL;
}

void NUAttribute::composeHelper(UtString* buf, const STBranchNode* scope,
                                bool includeRoot, bool hierName,
                                const char* separator ) const
{
  mExpr->compose(buf, scope, includeRoot, hierName, separator);
  *buf << "`" << getAttribChar() << " ";
}

UInt32 NUAttribute::getNumArgs() const {
  return 1;
}

//! Return the specified argument; 0-based index, 0 is leftmost argument
const NUExpr* NUAttribute::getArgHelper(UInt32 index) const {
  NU_ASSERT(index == 0, this);
  return mExpr;
}

//! Mutate the argument
NUExpr* NUAttribute::putArgHelper(UInt32 index, NUExpr* expr) {
  NU_ASSERT(index == 0, this);
  NUExpr* old = mExpr;
  mExpr = expr;
  return old;
}

