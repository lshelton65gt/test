// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NULvalue.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUHierRef.h"
#include "nucleus/NUTFWalker.h"
#include "hdl/HdlVerilogPath.h"

/*!
  \file
  Implementation of NUTaskEnable
*/

#ifdef CDB
static NUTaskEnable* sTaskEnableWatch = NULL;
static void sTaskEnableCheck(NUTaskEnable* te) {
  if (sTaskEnableWatch == te) {
    fprintf(stderr, "stop here.\n");
  }
}
#endif

NUTaskEnable::NUTaskEnable(NUTask *task,
                           NUModule* parentModule,
			   NUNetRefFactory *netref_factory,
                           bool usesCFNet,
			   const SourceLocator &loc) :
  NUStmt(usesCFNet, loc),
  mTask(task),
  mNetRefArgDefMap(netref_factory),
  mNetRefFactory(netref_factory),
  mParentModule(parentModule)
{
#ifdef CDB
  sTaskEnableCheck(this);
#endif
}


NUTaskEnable::~NUTaskEnable()
{
  clearUseDef();
  for (NUTFArgConnectionLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    delete *loop;
  }
}


void NUTaskEnable::setArgConnections(const NUTFArgConnectionVector& connections)
{
  mArgs = connections;
}


void NUTaskEnable::addArgConnection(NUTFArgConnection *conn)
{
  mArgs.push_back(conn);
}

void NUTaskEnable::getArgConnections(NUTFArgConnectionVector& connections) const
{
  connections.insert( connections.end(), mArgs.begin(), mArgs.end() );
}


NUTFArgConnection* NUTaskEnable::getArgConnectionByIndex(int idx) const
{
  NU_ASSERT(idx >= 0, this);
  NU_ASSERT(idx < (int)mArgs.size(), this);
  return mArgs[idx];
}


void NUTaskEnable::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  printInfo();

  if (isHierRef()) {
    getHierRef()->printPossibleResolutions(numspaces);
  } else {
    getTask()->print(false, numspaces+2);
  }

  if (recurse) {
    for (NUTFArgConnectionCLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
      (*loop)->print(true, numspaces+2);
    }
  }
}


void NUTaskEnable::printInfo() const
{
  UtIO::cout() << "TaskEnable(" << this << ")" << UtIO::endl;
}


const char* NUTaskEnable::typeStr() const
{
  return "NUTaskEnable";
}


NUStmt* NUTaskEnable::copy(CopyContext & copy_context) const
{
  NUModule* newParent = mParentModule;
  if (copy_context.mScope != NULL) {
    newParent = copy_context.mScope->getModule();
  }
  NUTaskEnable *stmt_copy = new NUTaskEnable(mTask, newParent,
                                             mNetRefFactory, getUsesCFNet(),
                                             mLoc);

  NUTFArgConnectionVector vector_copy;
  for (NUTFArgConnectionCLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    vector_copy.push_back((*loop)->copy(copy_context));
  }

  stmt_copy->setArgConnections(vector_copy);

  return stmt_copy;
}

bool NUTaskEnable::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUTaskEnable *te = dynamic_cast<const NUTaskEnable *>(&stmt);

  // check ptr equality first.
  if (this == te) {
    return true;
  }

  // if either is a hierarchical reference to a task, be pessimistic
  // and consider them different.
  if (te->isHierRef() or isHierRef()) {
    return false;
  }

  // if we have a non-hierref, check that the tasks are equivalent..
  if (te->getTask () != getTask())
    return false;

  // if the tasks are equivalent, compare our connections.
  return ObjListEqual<NUTFArgConnectionCLoop>(loopArgConnections (), te->loopArgConnections ());
}

void NUTaskEnable::addBlockingUse(const NUNetRefHdl &def_net_ref,
				  const NUNetRefHdl &use_net_ref)
{
  mNetRefArgDefMap.insert(def_net_ref, use_net_ref);
}


void NUTaskEnable::addBlockingUses(NUNet * /* def_net */, NUNetSet * /* uses */)
{
  // Obsolete
  NU_ASSERT(0=="obsolete", this);
}


void NUTaskEnable::addBlockingUses(const NUNetRefHdl &def_net_ref, NUNetRefSet *uses)
{
  for (NUNetRefSet::iterator iter = uses->begin(); iter != uses->end(); ++iter) {
    mNetRefArgDefMap.insert(def_net_ref, *iter);
  }
}


void NUTaskEnable::addBlockingDef(const NUNetRefHdl &net_ref)
{
  mNetRefArgDefMap.insert(net_ref);
}


void NUTaskEnable::getBlockingUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getBlockingUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUTaskEnable::getBlockingUses(NUNetRefSet *uses) const
{
  mNetRefArgDefMap.populateMappedToNonEmpty(uses);
}


bool NUTaskEnable::queryBlockingUses(const NUNetRefHdl &use_net_ref,
				     NUNetRefCompareFunction fn,
				     NUNetRefFactory * /* factory -- unused */) const
{
  return mNetRefArgDefMap.queryMappedTo(use_net_ref, fn);
}


void NUTaskEnable::getBlockingUses(NUNet *def_net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(def_net);
  getBlockingUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUTaskEnable::getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  mNetRefArgDefMap.populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUTaskEnable::queryBlockingUses(const NUNetRefHdl &def_net_ref,
				     const NUNetRefHdl &use_net_ref,
				     NUNetRefCompareFunction fn,
				     NUNetRefFactory * /* factory -- unused */) const
{
  return mNetRefArgDefMap.queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}


void NUTaskEnable::getBlockingDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getBlockingDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin(); iter != ref_defs.end(); ++iter) {
    defs->insert((*iter)->getNet());
  }
}


void NUTaskEnable::getBlockingDefs(NUNetRefSet *defs) const
{
  mNetRefArgDefMap.populateMappedFrom(defs);
}


bool NUTaskEnable::queryBlockingDefs(const NUNetRefHdl &def_net_ref,
				     NUNetRefCompareFunction fn,
				     NUNetRefFactory * /* factory -- unused */) const
{
  return mNetRefArgDefMap.queryMappedFrom(def_net_ref, fn);
}


void NUTaskEnable::getBlockingKills(NUNetSet *kills) const
{
  NUNetRefSet kill_set(mNetRefFactory);
  getBlockingKills(&kill_set);
  for (NUNetRefSet::iterator iter = kill_set.begin(); iter != kill_set.end(); ++iter) {
    if ((*iter)->all()) {
      kills->insert((*iter)->getNet());
    }
  }
}


void NUTaskEnable::getBlockingKills(NUNetRefSet *kills) const
{
  for (NUTFArgConnectionCLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    switch ((*loop)->getDir()) {
    case eInput:
      // No-op
      break;

    case eOutput:
      {
	NUTFArgConnectionOutput *oconn = dynamic_cast<NUTFArgConnectionOutput*>(*loop);
	NU_ASSERT(oconn, *loop);
	oconn->getActual()->getCompleteDefs(kills);
      }
      break;

    case eBid:
      {
	NUTFArgConnectionBid *bidconn = dynamic_cast<NUTFArgConnectionBid*>(*loop);
	NU_ASSERT(bidconn, *loop);
	bidconn->getActual()->getCompleteDefs(kills);
      }
      break;

    default:
      NU_ASSERT(0=="unknown task argument type", this);
      break;
    }
  }
}


bool NUTaskEnable::queryBlockingKills(const NUNetRefHdl &def_net_ref,
				      NUNetRefCompareFunction /* fn -- unused */,
				      NUNetRefFactory *factory) const
{
  for (NUTFArgConnectionCLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    switch ((*loop)->getDir()) {
    case eInput:
      // No-op
      break;

    case eOutput:
      {
	NUTFArgConnectionOutput *oconn = dynamic_cast<NUTFArgConnectionOutput*>(*loop);
	NU_ASSERT(oconn, *loop);
	if (oconn->getActual()->queryCompleteDefs(def_net_ref, factory)) {
	  return true;
	}
      }
      break;

    case eBid:
      {
	NUTFArgConnectionBid *bidconn = dynamic_cast<NUTFArgConnectionBid*>(*loop);
	NU_ASSERT(bidconn, *loop);
	if (bidconn->getActual()->queryCompleteDefs(def_net_ref, factory)) {
	  return true;
	}
      }
      break;

    default:
      NU_ASSERT(0=="unknown task argument type", this);
      break;
    }
  }

  return false;
}


void NUTaskEnable::replaceDef(NUNet * old_net, NUNet* new_net)
{
  for (NUTFArgConnectionLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    (*loop)->replaceDef(old_net,new_net);
  }
}

bool NUTaskEnable::replace(NUNet * old_net, NUNet* new_net)
{
  bool changed = false;
  // Don't believe recursing into the actual task is needed.
  // changed |= mTask->replace(old_net,new_net);
  for (NUTFArgConnectionLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    changed |= (*loop)->replace(old_net,new_net);
  }
  return changed;
}

bool NUTaskEnable::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;

  // hierref task enables have NULL mTask.

  if (not isHierRef()) {
    //  recursing into the actual task is not needed, but we do need
    //  to lookup and see if we should replace the referenced task.
    NUTF * repl = translator(mTask, NuToNuFn::ePre);
    if (repl) {
      NUTask * task_repl = dynamic_cast<NUTask*>(repl);
      NU_ASSERT(task_repl, repl);
      mTask = task_repl;
      changed = true;
    }
  }

  for (NUTFArgConnectionLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    changed |= (*loop)->replaceLeaves(translator);
  }

  if (not isHierRef()) {
    NUTF * repl = translator(mTask, NuToNuFn::ePost);
    if (repl) {
      NUTask * task_repl = dynamic_cast<NUTask*>(repl);
      NU_ASSERT(task_repl, repl);
      mTask = task_repl;
      changed = true;
    }
  }

  return changed;
}

void NUTaskEnable::clearUseDef()
{
  mNetRefArgDefMap.clear();
}


void NUTaskEnable::fixupUseDef(NUNetList *remove_list)
{
  helperFixupUseDef(&mNetRefArgDefMap, remove_list);
}

//! create in HDL form
void NUTaskEnable::compose(UtString* pbuf, const STBranchNode* scope, int numSpaces, bool) const
{
  pbuf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, pbuf);
    return; 
  }
  if (isHierRef()) {
    HdlVerilogPath path;
    getHierRef()->getName(pbuf, path);
  } else {
    *pbuf << getTask()->getName()->str();
  }
  *pbuf << "(";

  bool firstArg = true;
  for (NUTFArgConnectionCLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) 
  {
    NUTFArgConnection* connection = *loop;
    if ( firstArg ) { firstArg = false; }
    else            { *pbuf << ", "; }
    connection->compose(pbuf, scope, 0, true);
  }
  *pbuf << ");\n";
}


NUTaskEnableHierRef::NUTaskEnableHierRef(const AtomArray& path,
                                         StringAtom *taskName,
                                         NUScope *scope,
					 NUNetRefFactory *netref_factory,
                                         bool usesCFNet,
					 const SourceLocator &loc) :
  NUTaskEnable(0, scope->getModule(), netref_factory, usesCFNet, loc),
  mTaskHierRef(this, path, taskName),
  mModule(scope->getModule())
{
}


NUTaskEnableHierRef::~NUTaskEnableHierRef()
{
}


NUTask * NUTaskEnableHierRef::getTask() const
{
  NUTask * resolution = NULL;
  for (NUTaskHierRef::TaskVectorCLoop loop = mTaskHierRef.loopResolutions();
       not loop.atEnd();
       ++loop) {
    NU_ASSERT( not resolution, this ); // ensure one resolution
    resolution = (*loop);
  }
  NU_ASSERT( resolution, this ); // ensure one resolution
  return resolution;
}


NUTask * NUTaskEnableHierRef::getRepresentativeResolution() const
{
  return mTaskHierRef.getRepresentativeResolution();
}


void NUTaskEnableHierRef::getNonLocalDefs(NUNetSet *defs)
{
  NUTask * resolution = getRepresentativeResolution();

  NUModule *module = getModule();
  NUTFWalkerCallback callback;
  NUDesignWalker walker(callback, false);
  callback.setWalker(&walker);
  walker.tf(resolution);

  NUNetSet task_defs;
  for (NUNetSet::iterator iter = callback.mNonLocalDefs.begin();
       iter != callback.mNonLocalDefs.end();
       ++iter) {
    NUNet *net = *iter;
    if (not net->isChildOf(resolution)) {
      NUNet* resolvedNet = module->findNetHierRef(net);
      NU_ASSERT(resolvedNet, net);
      defs->insert(resolvedNet);
    }
  }
}


void NUTaskEnableHierRef::getNonLocalUses(NUNetSet *uses)
{
  NUTask * resolution = getRepresentativeResolution();

  NUModule *module = getModule();
  NUTFWalkerCallback callback;
  NUDesignWalker walker(callback, false);
  callback.setWalker(&walker);
  walker.tf(resolution);

  for (NUNetSet::iterator iter = callback.mNonLocalUses.begin();
       iter != callback.mNonLocalUses.end();
       ++iter) {
    NUNet *net = *iter;
    if (not net->isChildOf(resolution)) {
      NUNet *use_net = module->findNetHierRef(net);
      NU_ASSERT(use_net, net);
      uses->insert(use_net);
    }
  }
}


void NUTaskEnableHierRef::printInfo() const
{
  UtIO::cout() << "TaskEnableHierRef(" << this << ")"
	       << UtIO::endl;
}


NUStmt* NUTaskEnableHierRef::copy(CopyContext &copy_context) const
{
  NUTaskEnableHierRef *stmt_copy =
    new NUTaskEnableHierRef(mTaskHierRef.getPath(),
                            mTaskHierRef.getTaskName(), 
                            copy_context.mScope,
                            mNetRefFactory, getUsesCFNet(), mLoc);
  NUTFArgConnectionVector vector_copy;
  for (NUTFArgConnectionCLoop loop = loopArgConnections(); not loop.atEnd(); ++loop) {
    vector_copy.push_back((*loop)->copy(copy_context));
  }
  stmt_copy->setArgConnections(vector_copy);
  // for now, copy over all of the resolutions. fixing up these
  // references happens as post-processing, if necessary.
  NUTaskHierRef * copy_hier_ref = stmt_copy->getHierRef();
  for (NUTaskHierRef::TaskVectorCLoop loop = mTaskHierRef.loopResolutions();
       not loop.atEnd();
       ++loop) {
    NUTask * resolution = (*loop);
    resolution->addHierRef(stmt_copy);
    copy_hier_ref->addResolution(resolution);
  }

  return stmt_copy;
}

//! Rewrite a tf arg connection in terms of a new task.
static NUTFArgConnection * sCloneTFConnection(const NUTFArgConnection * original, NUTF * replacement_tf,
                                              CopyContext& cc)
{
  NUTFArgConnection * replacement = NULL;
  
  NUNet * original_formal = original->getFormal();
  NUTF  * original_tf = original->getTF();

  NUNet * replacement_formal = NULL;
  if (original_formal->getScope()==replacement_tf) {
    // someone has already rewritten the formal; maybe due to a local resolution?
    replacement_formal = original_formal;
  } else {
    // assumption: index in original TF is same as index in new TF.
    int index = original_tf->getArgIndex(original_formal);
    replacement_formal = replacement_tf->getArg(index);
  }

  switch (original->getType()) {
  case eNUTFArgConnectionInput: {
    const NUTFArgConnectionInput * input = dynamic_cast<const NUTFArgConnectionInput*>(original);
    replacement = new NUTFArgConnectionInput(input->getActual()->copy(cc),
                                             replacement_formal,
                                             replacement_tf,
                                             original->getLoc());
    break;
  }
  case eNUTFArgConnectionOutput: {
    const NUTFArgConnectionOutput * output = dynamic_cast<const NUTFArgConnectionOutput*>(original);
    replacement = new NUTFArgConnectionOutput(output->getActual()->copy(cc),
                                              replacement_formal,
                                              replacement_tf,
                                              original->getLoc());
    break;
  }
  case eNUTFArgConnectionBid: {
    const NUTFArgConnectionBid * bid = dynamic_cast<const NUTFArgConnectionBid*>(original);
    replacement = new NUTFArgConnectionBid(bid->getActual()->copy(cc),
                                           replacement_formal,
                                           replacement_tf,
                                           original->getLoc());
    break;
  }
  default:
    NU_ASSERT(0=="unknown task argument type", original);
    break;
  }
  NU_ASSERT(replacement, original);
  return replacement;
}


void NUTaskEnable::replaceTask(NUTask* newTask, CopyContext& cc, NUTaskEnable* connectionSrc) {
  NUTFArgConnectionVector connections;
  bool deleteOldConnections = false;
  if (connectionSrc == NULL) {
    connectionSrc = this;
    deleteOldConnections = true;
  }
  for (NUTFArgConnectionLoop loop = connectionSrc->loopArgConnections(); 
       not loop.atEnd(); 
       ++loop) {
    NUTFArgConnection * old_connection = (*loop);
    NUTFArgConnection * new_connection = sCloneTFConnection(old_connection, newTask, cc);
    connections.push_back(new_connection);
    if (deleteOldConnections) {
      delete old_connection;
    }
  }
  setArgConnections(connections);
  mTask = newTask;
}

const NUModule* NUTaskEnable::findParentModule() const {
  return mParentModule;
}

NUModule* NUTaskEnable::findParentModule() {
  return mParentModule;
}
