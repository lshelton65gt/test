// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/CarbonAssert.h"
#include "nucleus/NUNetRefMap.h"
#include "nucleus/NUNet.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "flow/FLNode.h"
#include "util/DynBitVector.h"
#include "util/UtIOStream.h"
#include "util/ConstantRange.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include <cstddef>
#include "nucleus/NUNetSet.h"

/*!
  \file
  Definition of Nucleus net reference class.
 */

extern void CarbonAbort(const char* file, long line, const char* expStr);

NUNetRef::NUNetRef(NUNet *net, const ConstantRange &r,
                   DynBitVectorFactory* factory)
{
  // Memory nets need to be normalized
  NUMemoryNet* mem = net->getMemoryNet();
  if (mem != NULL) {
    ConstantRange range(r);
    range.normalize(mem->getNetRefRange());
    mBits = DynBitVecDesc(range.getLsb(), range.getLength(), factory);
  }
  else {
    NU_ASSERT(net->containsRange(r), net);
    mBits = DynBitVecDesc(r.getLsb(), r.getLength(), factory);
  }
  initNet(net);
}

NUNetRef::NUNetRef(NUNet* net, SInt32 index,
                   DynBitVectorFactory* factory)
{
  NU_ASSERT(net->containsBit(index), net);

  // Memory nets need to be normalized
  NUMemoryNet* mem = net->getMemoryNet();
  if (mem != NULL) {
    index = mem->getNetRefRange()->offsetBounded(index);
  }
  mBits.populateRange(index, 1, factory);
  initNet(net);
}

NUNetRef::NUNetRef(NUNet* net, const DynBitVector& bv,
                   DynBitVectorFactory* factory)
{
  NU_ASSERT(bv.size() == net->getNetRefWidth(), net);
  mBits = DynBitVecDesc(bv, factory);
  initNet(net);
}

NUNetRef::NUNetRef(NUNet* net,
                   DynBitVectorFactory* factory) :
  mNet(net),
  mBits(0, (net == NULL)? 0: net->getNetRefWidth(), factory)
{
  initNet(net);
}

//! create a NUNetRef for net using the subrange(s) specified in image.
/*! \param net  the new netref will be for this net
 *  \param image the new netref will use a copy of the range defined in
 *  this image.
 * The \arg net and \arg image must be the same type of nucleus
 * objects.
 */
NUNetRef::NUNetRef(NUNet* net, const NUNetRef& image):
  mBits(image.mBits)
{
  // we should check here that net and image are of the same type, for
  // example it does not make sense to create a NUNetRef for a vector
  // if the image is a memory.

  NU_ASSERT2(net->getNetRefWidth() == image.getNet()->getNetRefWidth(),
             net, image.getNet());

  initNet(net);
}

NUNetRef::~NUNetRef() {
  NU_ASSERT(mRefCnt == 0, mNet);
}

//! Test that the two bit vectors are adjacent
/*!
 * Somewhat limited for now, only tests when one bit is set in each vector.
 */
static bool sTestAdjacent(const DynBitVector &vect1,
			  const DynBitVector &vect2)
{
  if ((vect1.count() != 1) or (vect2.count() != 1)) {
      return false;
  } else {
    int idx1 = vect1.findFirstOne ();
    int idx2 = vect2.findFirstOne ();
    return (abs(idx1 - idx2) == 1);
  }
}


//! Return the number of bits in this net ref
UInt32 NUNetRef::getNumBits() const {
  if (empty()) {
    return 0;
  } else {
    UInt32 c = mBits.count();
    // for memories the bit mask is row-level, so multiply by # of bits per row
    NUMemoryNet* memNet = mNet->getMemoryNet();
    if (memNet != NULL) {
      UInt32 depth = memNet->getWidthRange()->getLength();
      c *= depth;
    }
    return c;
  }
}
  
void NUNetRef::getUsageMask(DynBitVector* usageMask) const {
  UInt32 width = mNet->getNetRefWidth();
  usageMask->resize(width);
  DynBitVector bvbuf;
  mBits.getBitVec(usageMask, width);
}

// Force instantiation of templatized maps.
// icc warns that this is already specialized.
#if !pfICC && !defined(__COVERITY__)
template class NUNetRefMultiMap<FLNode*>;
template class NUNetRefNetRefMultiMapB;
#endif


NUNetRef::DriverFilter::DriverFilter(const NUNetRef *net_ref, NUNetRefCompareFunction fn) :
  mNetRef(net_ref), mCompareFunction(fn)
{}


bool NUNetRef::DriverFilter::operator()(FLNode *flow) const
{
  return (mNetRef->*mCompareFunction)(*flow->getDefNetRef());
}


NUNetRef::DriverLoop NUNetRef::loopContinuousDrivers(NUNetRefCompareFunction fn) const {
  return NUNetRef::DriverLoop(mNet->loopContinuousDrivers(), NUNetRef::DriverFilter(this, fn));
}


bool NUNetRef::operator<(const NUNetRef& other) const
{
  return compare(*this, other, true) < 0;
}

int NUNetRef::compare(const NUNetRef& ref1, const NUNetRef& ref2,
                      bool deepNetCompare)
{
  NUNet* net1 = ref1.mNet;
  NUNet* net2 = ref2.mNet;

  int cmp = 0;
  if (net1 != net2)
  {
    if (net1 == 0)
      cmp = -1;
    else if (net2 == 0)
      cmp = 1;
    else if (deepNetCompare)
      cmp = NUNet::compare(net1, net2);
    else
      cmp = carbonPtrCompare(net1, net2);
  }

  if (cmp == 0) {
    cmp = DynBitVecDesc::compare(ref1.mBits, ref1.mNet->getNetRefWidth(),
                                 ref2.mBits, ref2.mNet->getNetRefWidth());
  }
  return cmp;
} // int NUNetRef::compare


//! Print the bit numbers of the bits which are set in vect.
bool NUNetRef::printBits(UtString * buf, int indent, bool denormalizeVects)
  const
{
  const ConstantRange* declaredRange = NULL;
  if (mNet->isMemoryNet()) {
    NUMemoryNet * memNet = mNet->getMemoryNet();
    declaredRange = memNet->getNetRefRange();
  }
  else if (denormalizeVects) {
    if (NUVectorNet* vlocal = dynamic_cast<NUVectorNet*>(mNet)) {
      declaredRange = vlocal->getDeclaredRange();
    }
  }    

  DynBitVector vect;
  mBits.getBitVec(&vect, mNet->getNetRefWidth());

  if (not vect.all()) {
    buf->append(indent, ' ');
    *buf << "[";
    bool first = true;
    for (DynBitVector::RangeLoop p(vect, false); !p.atEnd(); ++p) {
      ConstantRange range = *p;
      if (declaredRange != NULL) {
        range.denormalize(declaredRange, false);
      }
      if (first) {
        first = false;
      }
      else {
        *buf << ",";
      }
      *buf << range.getMsb();
      if (range.getLength() != 1) {
        *buf << ":" << range.getLsb();
      }
    }
    *buf << "]";
    return true;
  } // if
  return false;
} // bool NUNetRef::printBits

void NUNetRef::print(int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  NUBase::indent(numspaces);
  UtIO::cout() << typeStr() << " ";
  if (mNet != 0) {
    mNet->print(0, 0);
  } 
  else {
    UtIO::cout() << UtIO::endl;
  }

  if (not empty()) {
    UtString buf;
    if (printBits(&buf, numspaces, false))
      UtIO::cout() << buf << "\n";
  }
} // void NUNetRef::print

void NUNetRef::compose(UtString* buf, const STBranchNode* scope,
                       bool includeRoot, bool hierName,
                       const char* separator, bool escapeIfMultiLevel,
                       bool denormalize)
  const
{
  if ( mNet->getLoc().isTicProtected() ){
    NUBase::composeProtectedNUObject(mNet->getLoc(), buf);
    return; 
  }

  mNet->compose(buf, scope, includeRoot, hierName, separator,
                escapeIfMultiLevel);
  if (!empty() && !mNet->isBitNet()) {
    printBits(buf, 0, denormalize);
  }
}

void NUNetRef::composeUnelaboratedName(UtString* buf) const
{
  if ( mNet->getLoc().isTicProtected() ){
    NUBase::composeProtectedNUObject(mNet->getLoc(), buf);
    return; 
  }

  mNet->composeUnelaboratedName(buf);
  if (!empty() && !mNet->isBitNet()) {
    printBits(buf, 0, false);
  }
}

const char* NUNetRef::typeStr() const
{
  if (empty()) {
    return "NUEmptyNetRef";
  }
  if (mNet->isBitNet()) {
    return "NUBitNetRef";
  }
  if (dynamic_cast<const NUVectorNet*>(mNet) != NULL) {
    return "NUVectorNetRef";
  }
  NUMemoryNet* memNet = mNet->getMemoryNet();
  if (memNet != NULL) {
    return "NUMemoryNetRef";
  }
  return "???";
}

bool NUNetRef::getRange(ConstantRange& range) const
{
  UInt32 startBit, size;
  bool contiguous = mBits.getContiguousRange(&startBit, &size);
  range.setLsb(startBit);
  range.setMsb(startBit + size - 1);

  NUMemoryNet* memNet = mNet->getMemoryNet();
  if (memNet != NULL) {
    range.denormalize(memNet->getNetRefRange());
  }

  return contiguous;
}

bool NUNetRef::getFirstContiguousRange(ConstantRange& range) const
{
  if (empty())
    return false;
  UInt32 startBit, size;
  (void) mBits.getContiguousRange(&startBit, &size);
  range.setLsb(startBit);
  range.setMsb(startBit + size - 1);

  NUMemoryNet* memNet = mNet->getMemoryNet();
  if (memNet != NULL) {
    range.denormalize(memNet->getNetRefRange());
  }

  return true;
}


bool NUNetRef::overlapsBits(const NUNetRef& other) const
{
  if ((mNet == NULL) || (other.mNet == NULL))
    return false;
  if (other.mBits == mBits)
    return true;
  if (other.empty())
    return false;
  UInt32 width = std::min(mNet->getNetRefWidth(),
                          other.mNet->getNetRefWidth());
  return mBits.anyCommonBitsSet(other.mBits, width);
}

bool NUNetRef::overlapsBit(UInt32 offset) const
{
  if (mNet == NULL) {
    return false;
  }
  return mBits.test(offset);
}

bool NUNetRef::overlapsSameNet(const NUNetRef& other) const
{
  if ((mNet == NULL) || mNet != other.getNet())
    return false;
  if (other.mBits == mBits)
    return true;
  return mBits.anyCommonBitsSet(other.mBits, mNet->getNetRefWidth());
}

bool NUNetRef::operator==(const NUNetRef& other) const
{
  return ((mNet == other.getNet()) && (mBits == other.mBits));
}


bool NUNetRef::covers(const NUNetRef& other) const
{
  if (mNet == other.getNet()) {
    if (mBits == other.mBits) {
      return true;
    } else {
      return mBits.covers(other.mBits, mNet->getNetRefWidth());
    }
  }
  return false;
}

bool NUNetRef::covered(const NUNetRef& other) const
{
  return other.covers(*this);
}


bool NUNetRef::adjacent(const NUNetRef& other) const
{
  if (mNet == other.getNet()) {
    DynBitVector bv1, bv2;
    UInt32 width = mNet->getNetRefWidth();
    mBits.getBitVec(&bv1, width);
    other.mBits.getBitVec(&bv2, other.mNet->getNetRefWidth());
    return sTestAdjacent(bv1, bv2);
  }
  return false;
}


bool NUNetRef::aligned(const NUNetRef& other) const
{
  if (mBits == other.mBits)
    return true;
  else {
    DynBitVector bv1, bv2;
    mBits.getBitVec(&bv1, mNet->getNetRefWidth());
    other.mBits.getBitVec(&bv2, other.mNet->getNetRefWidth());
    return bv1 == bv2;
  }
}


NUNetRef NUNetRef::combine(DynBitVecDesc::CombineFn combine, const NUNetRef& other,
                           DynBitVectorFactory* factory)
  const
{
  // Note that *this should not be empty -- that case should be handled by
  // the caller, since the handling of this->empty() differs based on the three uses
  // for this method.
  NU_ASSERT(!empty(), other.mNet);
  if (other.mNet == NULL)
    return *this;
  NU_ASSERT(mNet == other.mNet, mNet);
  DynBitVector v;
  (*combine)(mBits, other.mBits, &v, mNet->getNetRefWidth());
  return NUNetRef(mNet, v, factory);
}

//! Create a list of Lvalues suitable for assignment to the elements of this net ref
void NUNetRef::createLvalues(NUNetRefFactory* factory, const SourceLocator& loc,
                             UtList<NULvalue*>* lvalueList, bool blastVectors) const
{
  for (NUNetRefRangeLoop r = loopRanges(factory); !r.atEnd(); ++r)
  {
    const ConstantRange& range = *r;
    if (mNet->isBitNet()) {
      NUIdentLvalue* lval = new NUIdentLvalue(mNet,loc);
      lvalueList->push_back(lval);
    } else if (mNet->isVectorNet()) {
      if (blastVectors) {
        // create a separate assignment for each bit
        NUVectorNet* vn = mNet->castVectorNet();
        const ConstantRange* range = vn->getRange();
        for (SInt32 idx = range->leftmost(); idx >= range->rightmost(); --idx)
        {
          ConstantRange this_bit(idx,idx);
          NUVarselLvalue* lval = new NUVarselLvalue(mNet,this_bit,loc);
          lvalueList->push_back(lval);
        }
      } else {
        // assign the entire range as a single group
        NUVarselLvalue* lval = new NUVarselLvalue(mNet,range,loc);
        lvalueList->push_back(lval);
      }
    } else if (mNet->is2DAnything()) {
      if (all()) {
        // this is a whole memory-to-memory assignment
        NUIdentLvalue* lval = new NUIdentLvalue(mNet,loc);
        lvalueList->push_back(lval);
      } else {
        // this is a partial memory assignment --  make a different lvalue for each row
        for (SInt32 idx = range.rightmost(); idx <= range.leftmost(); ++idx)
        {
          UInt32 width = NUConst::determineOptimalBitSize(idx, true, 32); // use same signedness as the next call to NUConst::create
          NUExpr* indexExpr = NUConst::create (true, idx, width, loc);
          NUMemselLvalue* lval = new NUMemselLvalue(mNet,indexExpr, loc);
          lval->putResynthesized(true);
          lvalueList->push_back(lval);
        }
      }
    } else {
      NU_ASSERT("Unexpected net type when creating lvalues"==NULL,mNet);      
    }
  }
}

NUNetRefFactory::NUNetRefFactory()
  : mBVFactory(false)           // do not uniquify by size
{
#ifdef NETREF_DEBUG
  NUNetRef::createTokenContainer();
#endif

  mNetRefMap = new NetRefMap;
}


NUNetRefFactory::~NUNetRefFactory()
{
#ifdef NETREF_DEBUG
  NUNetRef::deleteTokenContainer();
#endif

  delete mNetRefMap;
}


NUNetRefHdl NUNetRefFactory::createNetRef(NUNet *net)
{
  return insert(NUNetRef(net, &mBVFactory));
}


NUNetRefHdl NUNetRefFactory::createBitNetRef(NUNet *net)
{
#if NUBITNETREF_STORED_IN_NUBITNET
  const NUNetRef* netRef = net->getNetRef();
  NU_ASSERT(netRef, net);
  return NUNetRefHdl(netRef);
#else
  return insert(NUNetRef(net, 0, NULL));
#endif
}


NUNetRefHdl NUNetRefFactory::createVectorNetRef(NUNet *net)
{
  NU_ASSERT(net->isVectorNet(), net);
  return insert(NUNetRef(net, &mBVFactory));
}


NUNetRefHdl NUNetRefFactory::createVectorNetRef(NUNet *net, SInt32 idx)
{
  NUVectorNet *vnet = dynamic_cast<NUVectorNet*>(net);
  NU_ASSERT(vnet, net);
  if (vnet->getRange()->contains(idx)) {
    return insert(NUNetRef(net, idx, &mBVFactory));
  } else {
    return createEmptyNetRef();
  }
}


NUNetRefHdl NUNetRefFactory::createVectorNetRef(NUNet *net, const ConstantRange &r)
{
  ConstantRange netRange(0, 0);
  NUMemoryNet* memNet = net->getMemoryNet();
  if (memNet != NULL) {
    netRange = *memNet->getNetRefRange();
  }
  else {
    NUVectorNet *vnet = dynamic_cast<NUVectorNet*>(net);
    if (vnet != NULL)
      netRange = *vnet->getRange();
  }
  if (netRange.contains(r)) {
    return insert(NUNetRef(net, r, &mBVFactory));
  } else if (netRange.overlaps(r)) {
    ConstantRange new_r = netRange.overlap(r);
    return insert(NUNetRef(net, new_r, &mBVFactory));
  } else {
    return createEmptyNetRef();
  }
}


NUNetRefHdl NUNetRefFactory::createMemoryNetRef(NUNet *net,
                                                const ConstantRange &r)
{
  NUMemoryNet *mnet = net->getMemoryNet();
  NU_ASSERT(mnet, net);
  const ConstantRange *mnet_r = mnet->getNetRefRange();
  if (mnet_r->contains(r)) {
    return insert(NUNetRef(net, r, &mBVFactory));
  } else if (mnet_r->overlaps(r)) {
    ConstantRange new_r = mnet_r->overlap(r);
    return insert(NUNetRef(net, new_r, &mBVFactory));
  } else {
    return createEmptyNetRef();
  }
}


NUNetRefHdl NUNetRefFactory::sliceNetRef(const NUNetRefHdl& original,
                                         const ConstantRange& r)
{
  NUNetRefHdl subset_net_ref = createEmptyNetRef();

  NUNet * net = original->getNet();
  if (net->isVectorNet()) {
    subset_net_ref = createVectorNetRef(net, r);
  }
  else if (net->is2DAnything()) {
    subset_net_ref = createMemoryNetRef(net, r);
  } else {
    if (net->isBitNet()) {
      NU_ASSERT(r.getMsb()==0 and r.getLsb()==0, net);
      subset_net_ref = createNetRef(net);
    } else {
      NU_ASSERT(0, net);
    }
  }
  NU_ASSERT(original->covers(*subset_net_ref), net);
  return subset_net_ref;
}


NUNetRefHdl NUNetRefFactory::createMemoryNetRef(NUNet *net)
{
  NU_ASSERT(net->getMemoryNet() != NULL, net);
  return insert(NUNetRef(net, &mBVFactory));
}


NUNetRefHdl NUNetRefFactory::createMemoryNetRef(NUNet *net, SInt32 idx)
{
  NUMemoryNet* memNet = net->getMemoryNet();
  if ((memNet != NULL) && memNet->getNetRefRange()->contains(idx)) {
    return insert(NUNetRef(net, idx, &mBVFactory));
  }
  return createEmptyNetRef();
}

NUNetRefHdl NUNetRefFactory::createNetRefImage(NUNet * net, const NUNetRefHdl & source)
{
  return insert(NUNetRef(net, *source));
}

void NUNetRefFactory::cleanup()
{
  NUNetList clearNets;
  for (NetRefMap::iterator iter = mNetRefMap->begin(), end = mNetRefMap->end();
       iter != end;  ++iter)
  {
    RefSet & rs = iter->second;
    UtVector<NUNetRef*> pre_delete;
    for (RefSet::iterator ref_iter = rs.begin(), ref_end = rs.end(), ref_next;
         ref_iter != ref_end;
         ref_iter = ref_next) {
      ref_next = ref_iter;
      ++ref_next;
      NUNetRef & ref = *ref_iter;
      if (ref.getRefCnt()==0) {
        pre_delete.push_back(&ref);
      }
    }

    for (UtVector<NUNetRef*>::iterator del_iter = pre_delete.begin();
         del_iter != pre_delete.end();
         ++del_iter) {
      NUNetRef * ref = *del_iter;
      rs.erase(*ref);
    }

    if (rs.empty()) {
      // When you delete an element out of a UtHashMap, you invalidate
      // an in-progress iteration, so defer it
      clearNets.push_back(iter->first);
    }
  }

  for (NUNetList::iterator p = clearNets.begin(), e = clearNets.end();
       p != e; ++p)
  {
    NUNet* net = *p;
    mNetRefMap->erase(net);
  }
}


NUNetRefHdl NUNetRefFactory::merge(const NUNetRefHdl &one, const NUNetRefHdl& two)
{
  if (one->empty()) {
    return two;
  }
  if (two->empty()) {
    return one;
  }
  if (one==two) {
    return one;
  }
  return insert(one->combine(DynBitVecDesc::merge, *two, &mBVFactory));
}


NUNetRefHdl NUNetRefFactory::intersect(const NUNetRefHdl &one, const NUNetRefHdl& two)
{
  if (one->empty()) {
    return one;
  }
  if (two->empty()) {
    return two;
  }
  if (one==two) {
    return one;
  }
  return insert(one->combine(DynBitVecDesc::intersect, *two, &mBVFactory));
}


NUNetRefHdl NUNetRefFactory::subtract(const NUNetRefHdl &one, const NUNetRefHdl& two)
{
  if (one->empty())
    return one;
  if (one==two) {
    return createEmptyNetRef();
  }
  return insert(one->combine(DynBitVecDesc::subtract, *two, &mBVFactory));
}


NUNetRefHdl NUNetRefFactory::applyUsageMask(const NUNetRefHdl &src,
                                            const DynBitVector& usageMask)
{
  return insert(NUNetRef(src->getNet(), usageMask, &mBVFactory));
}


NUNetRefHdl NUNetRefFactory::insert(const NUNetRef& ref)
{
  NUNet* net = ref.getNet();
  if (net == NULL)
    return createEmptyNetRef();
  const NUNetRef* netRef = NULL;

#if NUBITNETREF_STORED_IN_NUBITNET
  netRef = net->getNetRef(); // NUBitNet or NUVirtual?
#endif

  if (netRef == NULL) {
    RefSet& rs = (*mNetRefMap)[net]; // inserts or finds
    RefSet::IterBoolPair ibp = rs.insert(ref);
    NUNetRef& insertedRef = *(ibp.first);
    netRef = &insertedRef;
  }
  return NUNetRefHdl(netRef);
}


void NUNetRefFactory::erase(NUNet* net) {
  const NUNetRef* netRef = NULL;
#if NUBITNETREF_STORED_IN_NUBITNET
  netRef = net->getNetRef(); // NUBitNet or NUVirtual?
#endif
  if (netRef == NULL) {         // netrefs managed by factory
    mNetRefMap->erase(net);
  }
}


template <>
FLNode* NUNetRefMultiMap<FLNode*>::nullT() const
{
  return 0;
}

template <>
NUNetRefHdl NUNetRefNetRefMultiMapB::nullT() const
{
  return mFactory->createEmptyNetRef();
}


//! Helper functions to facilitate printing of templatized value in (key,value) map
template <>
void NUNetRefNetRefMultiMapB::helperTPrint(const NUNetRefHdl &v, int indent) const
{
  v->print(indent);
}

template <>
void NUNetRefMultiMap<FLNode*>::helperTPrint(FLNode * const & v, int indent) const
{
  v->print(false, indent);
}

template <>
void NUNetRefMultiMap<FLNode*,HashPointer<FLNode*, 2>, NUNetCmp>::helperTPrint(FLNode * const & v, int indent) const
{
  v->print(false, indent);
}


#if 0
template<class T>
void NUNetRefMultiMap<T>::print(int indent) const
{
  for (typename NetNetRefTMap::const_iterator iter = mMap.begin(); iter != mMap.end(); ++iter) {
    NUNet *net = iter->first;
    const NetRefTMultiMap& m = iter->second;

    if (net) {
      net->print(0, indent+2);
    } else {
      for (int i = 0; i < indent+2; i++) {
	UtIO::cout() << " ";
      }
      UtIO::cout() << "NUNet(0)" << UtIO::endl;
    }

    for (typename NetRefTMultiMap::const_iterator iter2 = m.begin(); iter2 != m.end(); ++iter2) {
      NUNetRefHdl ref = iter2->first;
      const T t = iter2->second;
      ref->print(indent+4);
      helperTPrint(t, indent+6);
      UtIO::cout() << UtIO::endl;
    }
  }
}

template<class T>
void NUNetRefMultiMap<T>::populateMappedFrom(NUNetRefSet *s) const
{
  for (MapLoop l = loop(); not l.atEnd(); ++l) {
    s->insert((*l).first);
  }
}


template<class T>
bool NUNetRefMultiMap<T>::queryMappedFrom(const NUNetRefHdl &net_ref,
                                          NUNetRefCompareFunction fn) const
{
  for (MapLoop l = loop(); not l.atEnd(); ++l) {
    if (((*(*l).first)->*fn)(*net_ref)) {
      return true;
    }
  }
  return false;
}
#endif


void NUNetRefNetRefMultiMap::populateMappedToNonEmpty(NUNetRefSet *s) const
{
  for (MapLoop l = loop(); not l.atEnd(); ++l) {
    NUNetRefHdl ref = (*l).second;
    if (not ref->empty()) {
      s->insert(ref);
    }
  }
}


void NUNetRefNetRefMultiMap::populateMappedToNonEmpty(NUNetRefSet *s,
                                                      const NUNetRefHdl &r,
                                                      NUNetRefCompareFunction fn) const
{
  for (CondLoop l = loop(r, fn); not l.atEnd(); ++l) {
    NUNetRefHdl ref = (*l).second;
    if (not ref->empty()) {
      s->insert(ref);
    }
  }
}


bool NUNetRefNetRefMultiMap::queryMappedTo(const NUNetRefHdl &use_net_ref,
                                           NUNetRefCompareFunction fn) const
{
  for (MapLoop l = loop(); not l.atEnd(); ++l) {
    if (((*(*l).second).*fn)(*use_net_ref)) {
      return true;
    }
  }
  return false;
}


bool NUNetRefNetRefMultiMap::queryMappedTo(const NUNetRefHdl &def_net_ref,
                                           NUNetRefCompareFunction def_fn,
                                           const NUNetRefHdl &use_net_ref,
                                           NUNetRefCompareFunction use_fn) const
{
  for (CondLoop l = loop(def_net_ref, def_fn); not l.atEnd(); ++l) {
    if (((*(*l).second).*use_fn)(*use_net_ref)) {
      return true;
    }
  }
  return false;
}

void NUNetRefSet::copyHelper(const NUNetRefSet& src)
{
  NU_ASSERT(mSet.empty(), (*(mSet.begin ()))->getNet ()); // license to be reckless and copy src blindly
  for (NUNetRefSet::const_iterator i = src.begin(), e = src.end(); i != e; ++i) {
    NUNetRefHdl cur = *i;
    mSet.insert(cur);
  }
}

void NUNetRefSet::insert(const NUNetRefHdl& ref)
{
  iterator iter = mSet.find(ref);

  if (iter == mSet.end()) {
    mSet.insert(ref);
  } else if (not ref->empty()) {
    NUNetRefHdl cur = *iter;

    if (not cur->covers(*ref)) {
      if (ref->covers(*cur)) {
	mSet.erase(iter);
	mSet.insert(ref);
      } else {
	NUNetRefHdl new_ref = mFactory->merge(*iter, ref);
	mSet.erase(iter);
	mSet.insert(new_ref);
      }
    }
  }
}


void NUNetRefSet::insert(NUNetRefSet::const_iterator start, NUNetRefSet::const_iterator end)
{
  for (NUNetRefSet::const_iterator iter = start;
       iter != end;
       ++iter) {
    insert(*iter);
  }
}


NUNetRefSet::const_iterator NUNetRefSet::find(const NUNetRefHdl& ref, NUNetRefCompareFunction fn) const
{
  const_iterator iter = mSet.find(ref);

  if ((iter != mSet.end()) and (((*ref).*fn)(**iter))) {
    return iter;
  } else {
    return mSet.end();
  }
}

NUNetRefSet::iterator NUNetRefSet::find(const NUNetRefHdl& ref, NUNetRefCompareFunction fn)
{
  iterator iter = mSet.find(ref);

  if ((iter != mSet.end()) and (((*ref).*fn)(**iter))) {
    return iter;
  } else {
    return mSet.end();
  }
}

void NUNetRefSet::erase(NUNetRefHdl& ref, NUNetRefCompareFunction fn)
{
  iterator iter = find(ref, fn);
  if (iter != mSet.end()) {
    mSet.erase(iter);
  }
}


void NUNetRefSet::erase(NUNetRefHdl& ref)
{
  iterator iter = find(ref, &NUNetRef::overlapsSameNet);

  if (iter != mSet.end()) {
    NUNetRefHdl cur = *iter;

    if (ref->covers(*cur)) {
      mSet.erase(iter);
    } else {
      NUNetRefHdl new_ref = mFactory->subtract(*iter, ref);
      mSet.erase(iter);
      mSet.insert(new_ref);
    }
  }
}


/*! Choose one element from a net ref set in a way that is stable
 *  across builds.  This routine is O(n) in the size of the set,
 *  and uses a fairly expensive comparison function.
 */
NUNetRefSet::const_iterator NUNetRefSet::stableChoice() const
{
  if (empty())
    return end();

  // start with the first netref as the best
  const_iterator n = begin();
  const_iterator best = n;

  // loop over all subsequent netrefs, promoting any better than the current best
  ++n;
  while (n != end())
  {
    // this comparison is meant to be stable (ie., not pointer-based)
    if (NUNetRef::compare(*(*best), *(*n), true) <= 0)
      best = n;
    ++n;
  }

  // return the iterator to the best entry
  return best;
}

void NUNetRefSet::print(int indent) const
{
  for (const_iterator iter = mSet.begin(); iter != mSet.end(); ++iter) {
    (*iter)->print(indent+2);
  }
}


UInt32 NUNetRefSet::getNumBits() const
{
  UInt32 bit_cnt = 0;
  for (NUNetRefSet::const_iterator iter = begin(); iter != end(); ++iter) {
    bit_cnt += (*iter)->getNumBits();
  }
  return bit_cnt;
}


void NUNetRefSet::set_intersection(const NUNetRefSet &one,
                                   const NUNetRefSet &two,
                                   NUNetRefSet &out)
{
  if (one.size() > two.size()) {
    set_intersection(two, one, out);
    return;
  }

  for (NUNetRefSet::const_iterator iter = one.begin(); iter != one.end(); ++iter) {
    NUNetRefSet::const_iterator find_iter = two.find(*iter, &NUNetRef::overlapsSameNet);
    if (find_iter != two.end()) {
      NUNetRefHdl found = *find_iter;
      NUNetRefHdl i_net_ref = out.getFactory()->intersect(*iter, found);
      NU_ASSERT(not i_net_ref->empty(), found->getNet());
      out.insert(i_net_ref);
    }
  }
}


bool NUNetRefSet::set_has_intersection(const NUNetRefSet & one,
                                       const NUNetRefSet & two)
{
  if (one.size() > two.size()) {
    return set_has_intersection(two, one);
  }

  for (NUNetRefSet::const_iterator iter = one.begin(); iter != one.end(); ++iter) {
    NUNetRefSet::const_iterator find_iter = two.find(*iter, &NUNetRef::overlapsSameNet);
    if (find_iter != two.end()) {
      return true;
    }
  }
  return false;
}


void NUNetRefSet::set_difference(const NUNetRefSet &one,
                                 const NUNetRefSet &two,
                                 NUNetRefSet &out)
{
  for (NUNetRefSet::const_iterator iter = one.begin(); iter != one.end(); ++iter) {
    NUNetRefSet::const_iterator find_iter = two.find(*iter, &NUNetRef::overlapsSameNet);
    if (find_iter != two.end()) {
      NUNetRefHdl d_net_ref = out.getFactory()->subtract(*iter, *find_iter);
      if (not d_net_ref->empty()) {
	out.insert(d_net_ref);
      }
    } else {
      out.insert(*iter);
    }
  }
}


void NUNetRefSet::set_subtract(const NUNetRefSet & one,
                               NUNetRefSet & out)
{
  // If the resultant set is empty there is nothing to do. It is
  // cheaper to test for that than iterate over a non empty subtract
  // set. Testing the subtract set is empty the iteration below will
  // deal with it efficiently.
  if (out.empty()) {
    return;
  }

  // This used to use set_difference but that causes us to use a temp
  // NUNetRefSet that gets allocated and deallocated. This was part of
  // a performance problem. So now we iterate over the subtract set
  // and remove the the output set.
  for (NUNetRefSet::const_iterator i = one.begin(); i != one.end(); ++i) {
    // Find if there is an overlaping net ref in the resulting set
    const NUNetRefHdl sub = *i;
    NUNetRefSet::iterator f = out.find(sub, &NUNetRef::overlapsSameNet);
    if (f != out.end()) {
      // It exists, if they are the same then just erase it. Otherwise
      // we need to do an subtract operation.
      const NUNetRefHdl res = *f;
      if (res == sub) {
        out.erase(f);
      } else {
        NUNetRefHdl diff = out.getFactory()->subtract(res, sub);
        out.erase(f);

        // Should we allow empty netrefs to be inserted into sets?
        // Or should empty sets be empty?  Joe sez NUEmptyNetRefs
        // should be found in empty NUNetRefSets.  Aron disagrees.
        // I think I side with Aron in this case, although it should
        // be noted there are other places in the code where NUEmptyNetRefs
        // can find their way into NUNetRefSets.  But we'll not put one
        // in here (for now).
        if (!diff->empty()) {
          out.insert(diff);
        }
      }
    }
  }
}

bool NUNetRefSet::operator==(const NUNetRefSet& nrs) const {
  // This implementation must be used rather than directly using
  // UtSet::operator==, because NetRefSetCompare will say they are the
  // same if the NUNet* is the same.  We need to make sure the bits
  // are the same as well.
  if (size() != nrs.size()) {
    return false;
  }
  NetRefSet::const_iterator p = mSet.begin();
  NetRefSet::const_iterator q = nrs.mSet.begin();
  for (NetRefSet::const_iterator e = mSet.end(); p != e; ++p, ++q) {
    const NUNetRefHdl& k1 = *p;
    const NUNetRefHdl& k2 = *q;
    if (k1 != k2) {
      return false;
    }
  }
  return true;
}

NUNetRefHdl NUNetRefSet::findNet(NUNet* net) {
  NUNetRefHdl findRef = mFactory->createNetRef(net);
  NetRefSet::iterator p = mSet.find(findRef);
  if (p != mSet.end()) {
    return *p;
  }
  return mFactory->createEmptyNetRef();
}


NUNetRefRangeLoop NUNetRef::loopRanges(NUNetRefFactory* factory)
  const
{
  return NUNetRefRangeLoop(this, factory);
}

NUNetRefRangeLoop::NUNetRefRangeLoop()
{
  mFactory = NULL;
  mValid = false;
}

NUNetRefRangeLoop::NUNetRefRangeLoop(const NUNetRef* netRef, NUNetRefFactory* factory)
  : mNetRef(const_cast<NUNetRef*>(netRef)),
    mFactory(factory)
{
  setup();
}

NUNetRefRangeLoop::NUNetRefRangeLoop(const NUNetRefRangeLoop& src)
  : mNetRef(src.mNetRef),
    mFactory(src.mFactory)
{
  setup();
}

NUNetRefRangeLoop& NUNetRefRangeLoop::operator=(const NUNetRefRangeLoop& src)
{
  if (&src != this)
  {
    mNetRef = src.mNetRef;
    setup();
  }
  return *this;
}

NUNetRefRangeLoop::~NUNetRefRangeLoop()
{
}

void NUNetRefRangeLoop::operator++()
{
  mNetRef = mFactory->subtract(mNetRef, mCurrentNetRef);
  setup();
}

void NUNetRefRangeLoop::setup()
{
  mValid = mNetRef->getFirstContiguousRange(mCurrentRange);
  if (mValid) {
    mCurrentNetRef = mFactory->createVectorNetRef(mNetRef->getNet(),
                                                  mCurrentRange);
  }
} // void NUNetRefRangeLoop::setup

//! get a sense of how many have contiguous ranges
void NUNetRefFactory::countContiguousRanges() {
  printStats();
}

#ifdef NETREF_DEBUG
/*
 * Maintain a set of unique tokens which identify where NUNetRef::incCount/decCount
 * took place.
 * Any tokens currently in the set represent calls to NUNetRef::incCount without
 * a corresponding decCount call.
 */
typedef UtHashMap<int,const NUNetRef*> NetRefTokenMap;
static NetRefTokenMap *sAliveTokens = 0;
static int sCurToken = 0;
static int sWatchToken = -1;
static int sAliveTokensReferers = 0;
void NUNetRef::createTokenContainer()
{
  // Construct a new sAliveTokens map only if this the first referrer.
  if (sAliveTokensReferers == 0) {
    sAliveTokens = new NetRefTokenMap;
  }
  ++sAliveTokensReferers;
}
void NUNetRef::deleteTokenContainer()
{
  // Destruct the sAliveTokens map only if this is the last referrer.
  if (sAliveTokensReferers == 1) {
    delete sAliveTokens;
    sAliveTokens = 0;
  }
  --sAliveTokensReferers;
}
int NUNetRef::obtainToken() const
{
  ++sCurToken;
  if (sWatchToken == sCurToken)
    UtIO::cerr() << "found token" << UtIO::endl;
  (*sAliveTokens)[sCurToken] = this;
  return sCurToken;
}
void NUNetRef::freeToken(int token) const
{
  NetRefTokenMap::iterator iter = sAliveTokens->find(token);
  NU_ASSERT(iter != sAliveTokens->end(), mNet);
  NU_ASSERT(iter->second == this, mNet);
  sAliveTokens->erase(iter);
}
void NUNetRef::printAliveTokens()
{
  for (NetRefTokenMap::iterator iter = sAliveTokens->begin();
       iter != sAliveTokens->end();
       ++iter) {
    if (this==(*iter).second) {
      UtIO::cout() << (*iter).first << "  :  " << (*iter).second << UtIO::endl;
    }
  }
}
void NUNetRef::printAllAliveTokens()
{
  for (NetRefTokenMap::iterator iter = sAliveTokens->begin();
       iter != sAliveTokens->end();
       ++iter) {
    UtIO::cout() << (*iter).first << "  :  " << (*iter).second << UtIO::endl;
  }
}
bool NUNetRef::queryToken(int token)
{
  return (sAliveTokens->find(token) != sAliveTokens->end());
}

void NUNetRef::incCount(int *token)
  const
{
  ++mRefCnt;
  *token = obtainToken();
}


void NUNetRef::decCount(int token)
  const
{
  --mRefCnt;
  NU_ASSERT(mRefCnt >= 0, mNet);
  freeToken(token);
}


#endif

void NUNetRefFactory::printStats(int indent_arg) {
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UInt32 totalSize = 0;

  // Find the total size
  NUNetSet nets;
  for (NetRefMap::iterator p = mNetRefMap->begin(), e = mNetRefMap->end();
       p != e; ++p)
  {
    RefSet& rs = p->second;
    totalSize += rs.size();
  }

  // Print an accounting of what's in the factory
  for (int i = 0; i < numspaces; i++) {UtIO::cout() << " ";}
  UtIO::cout() << "Number of netrefs: " << totalSize << UtIO::endl;
  for (int i = 0; i < numspaces; i++) {UtIO::cout() << " ";}
  UtIO::cout() << "Number of unique nets: " << mNetRefMap->size() << UtIO::endl;
  for (int i = 0; i < numspaces; i++) {UtIO::cout() << " ";}
  UtIO::cout() << "Owned mBVFactory: " << UtIO::endl;
  mBVFactory.printStats(numspaces+2);
}


void NUNetRef::printAssertInfo() const 
{
  this->print(0);
  UtIO::cout().flush();
}

bool NUNetRef::sameBits(const NUNetRef& other) const
{
  if (getNet()->getBitSize() != other.getNet()->getBitSize()) {
    // Different sizes, to hard to tell since we don't know how they
    // would be aliased.
    return false;
  } else {
    return mBits == other.mBits;
  }
}
