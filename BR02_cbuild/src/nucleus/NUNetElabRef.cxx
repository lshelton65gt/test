/***************************************************************************************
  Copyright (c) 2002-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

/*!
  \file 
  Implementation of elaborated net ref representation.
 */

#include "nucleus/NUNetElabRef.h"

#include "symtab/STBranchNode.h"

NUNetElabRef::~NUNetElabRef()
{
  if (mRefCnt != 0) {
    const NUNet* net = getNet ();
    if (net)
      NU_ASSERT(mRefCnt==0, net);
    else
      INFO_ASSERT (mRefCnt==0, "bad reference count");
  }
}


int NUNetElabRef::compare(const NUNetElabRef* ref1, const NUNetElabRef* ref2)
{
  int cmp = HierName::compare(ref1->getScope(), ref2->getScope());
  if (cmp==0) {
    cmp = NUNetRef::compare(*ref1->getNetRef(),*ref2->getNetRef(),true);
  }
  return cmp;
}


void NUNetElabRef::print(int indent) const
{
  for (int i = 0; i < indent; i++) {
    UtIO::cout() << " ";
  }

  UtString scope;
  mScope->compose(&scope);

  UtIO::cout() << typeStr() << "(" << this << ") : "
               << "Count(" << mRefCnt << ") : "
               << "Hierarchy(" << scope.c_str()
               << ")" << UtIO::endl;

  mNetRef->print(indent+2);
}




NUNetElabRefFactory::NUNetElabRefFactory(NUNetRefFactory * netRefFactory) :
  mNetRefFactory(netRefFactory)
{
}


NUNetElabRefFactory::~NUNetElabRefFactory()
{
  UtVector<NUNetElabRef*> pre_delete;
  for (InternalPool::iterator iter = mPool.begin();
       iter != mPool.end();
       ++iter) {
    pre_delete.push_back(*iter);
  }
  mPool.clear();

  for (UtVector<NUNetElabRef*>::iterator iter = pre_delete.begin();
       iter != pre_delete.end();
       ++iter) {
    delete *iter;
  }
}


NUNetElabRefHdl NUNetElabRefFactory::createNetElabRef(STBranchNode * branch, NUNetRefHdl & netRef)
{
  return insert(new NUNetElabRef(branch,netRef));
}


NUNetElabRefHdl NUNetElabRefFactory::merge(const NUNetElabRefHdl& one, const NUNetElabRefHdl& /*two*/)
{
  NU_ASSERT (0, (*one)->getNet ());
  return one;
}


NUNetElabRefHdl NUNetElabRefFactory::intersect(const NUNetElabRefHdl& one, const NUNetElabRefHdl& /*two*/)
{
  NU_ASSERT(0, (*one)->getNet ()); // tbi
  return one;
}


NUNetElabRefHdl NUNetElabRefFactory::subtract(const NUNetElabRefHdl& one, const NUNetElabRefHdl& /*two*/)
{
  NU_ASSERT(0, (*one)->getNet ()); // tbi
  return one;
}



NUNetElabRefHdl NUNetElabRefFactory::insert(NUNetElabRef* ref)
{
  InternalPool::iterator iter = mPool.find(ref);
  if (iter != mPool.end()) {
    delete ref;
    return NUNetElabRefHdl(*iter);
  } else {
    mPool.insert(ref);
    return NUNetElabRefHdl(ref);
  }
}

void NUNetElabRefFactory::printStats(int indent_arg)
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  for (int i = 0; i < numspaces; i++) {UtIO::cout() << " ";}
  UInt32 pool_count = mPool.size();
  UInt32 element_size = sizeof(NUNetElabRefHdl);
  UtIO::cout() << "Number of entries in pool: " << pool_count << " or total: " << pool_count<< " * " << element_size << " = " << pool_count * element_size << UtIO::endl;

  for (int i = 0; i < numspaces; i++) {UtIO::cout() << " ";}
  UtIO::cout() << "Embedded mNetRefFactory: " << UtIO::endl;
  mNetRefFactory->printStats(numspaces+2);
}


NUNetElabRefSet2::NUNetElabRefSet2(NUNetRefFactory* nrf)
  : mNetRefFactory(nrf)
{
}

NUNetElabRefSet2::~NUNetElabRefSet2() {
}

// Insert the elaborated net ref into the set
void NUNetElabRefSet2::insert(NUNetElab* netElab, const NUNetRefHdl& netRef)
{
  NU_ASSERT2((netRef->empty() || netElab->getNet() == netRef->getNet()),
             netElab, netRef);
  ElabRefMap::iterator p = mElabRefMap.find(netElab);
  if (p == mElabRefMap.end())
    mElabRefMap[netElab] = netRef;
  else
    p->second = mNetRefFactory->merge(p->second, netRef);
}

// Insert the elaborated net ref into the set
void NUNetElabRefSet2::insert(NUNetElab* netElab) {
  NUNetRefHdl netRef = mNetRefFactory->createNetRef(netElab->getNet());
  insert(netElab, netRef);
}

// Find the parts of the elaborated net ref that are not covered
// by the set
NUNetRefHdl NUNetElabRefSet2::getUncovered(NUNetElab* netElab,
                                           const NUNetRefHdl& netRef)
  const
{
  NU_ASSERT2(netElab->getNet() == netRef->getNet(), netElab, netRef);
  ElabRefMap::const_iterator p = mElabRefMap.find(netElab);
  if (p == mElabRefMap.end())
    return netRef;
  else
    return mNetRefFactory->subtract(netRef, p->second);
}

NUNetRefHdl NUNetElabRefSet2::getNetRef(NUNetElab* netElab) const {
  ElabRefMap::const_iterator p = mElabRefMap.find(netElab);
  if (p != mElabRefMap.end())
    return p->second;
  return mNetRefFactory->createEmptyNetRef();
}

NUNetElabRefSet2::RefLoop NUNetElabRefSet2::loopRefs() {
  return mElabRefMap.loopSorted();
}
