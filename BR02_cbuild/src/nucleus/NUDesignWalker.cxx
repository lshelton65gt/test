// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUAttribute.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUBreak.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUControlFlowNet.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUStructuredProc.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTriRegInit.h"
#include "util/UtIOStream.h"

/*!\file
 * Implementation of Design Walker and Design Callback classes.
 */

NUDesignWalker::NUDesignWalker(NUDesignCallback &callback, bool verbose,
			       bool rememberVisited) :
  mCallback(callback),
  mScopeSet(*(new NUScopeSet())),
  mWalkReplacements(false),
  mVerbose(verbose),
  mRememberVisited(rememberVisited),
  mWalkTasksFromTaskEnables(true),
  mWalkDeclaredNets(false),
  mCurrentTasks(new NUTaskSet)
{
}


NUDesignWalker::~NUDesignWalker()
{
  delete (&mScopeSet);
  delete mCurrentTasks;
}

void NUDesignWalker::putWalkReplacements(bool walk_replacements) {
  mWalkReplacements = walk_replacements;
}

void NUDesignWalker::putWalkDeclaredNets(bool flag) {
  mWalkDeclaredNets = flag;
}

void NUDesignWalker::putWalkTasksFromTaskEnables(bool flag) {
  mWalkTasksFromTaskEnables = flag;
}

void NUDesignWalker::resetRememberVisited() {
  delete (&mScopeSet);
  mScopeSet= *(new NUScopeSet());
}

NUDesignCallback::Status NUDesignWalker::design(NUDesign *this_design, 
						bool walk_all_modules)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, this_design);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_design); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_design); break;
  }

  NUDesign::ModuleLoop loop = walk_all_modules ? 
    this_design->loopAllModules() :
    this_design->loopTopLevelModules();

  for ( ; not loop.atEnd() ; ++loop ) {
    status = module(*loop);

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, this_design); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, this_design); break;
    default: NU_ASSERT(0, this_design); break;
    }
  }

  status = mCallback(NUDesignCallback::ePost, this_design);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_design); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_design); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::module(NUModule *this_module)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  if (haveVisited(this_module)) {
    return NUDesignCallback::eNormal;
  }

  status = mCallback(NUDesignCallback::ePre, this_module);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<
    NUModule,
    NUModuleInstanceMultiLoop,
    NUModuleInstanceList>
    (this_module,
     &NUModule::loopModuleInstances,
     &NUDesignWalker::moduleInstance,
     &NUModule::replaceModuleInstances
      );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<NUModule,NUTaskLoop,NUTaskVector>
    (this_module,
     &NUModule::loopTasks,
     &NUDesignWalker::task,
     &NUModule::replaceTasks
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<NUModule,NUCModelLoop,NUCModelVector>
    (this_module,
     &NUModule::loopCModels,
     &NUDesignWalker::cmodel,
     &NUModule::replaceCModels
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<NUModule,NUModule::ContAssignLoop,NUContAssignList>
    (this_module,
     &NUModule::loopContAssigns,
     &NUDesignWalker::contAssign,
     &NUModule::replaceContAssigns
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<NUModule,NUModule::ContEnabledDriverLoop,NUEnabledDriverList>
    (this_module,
     &NUModule::loopContEnabledDrivers,
     &NUDesignWalker::enabledDriver,
     &NUModule::replaceContEnabledDrivers
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<NUModule,NUModule::TriRegInitLoop,NUTriRegInitList>
    (this_module,
     &NUModule::loopTriRegInit,
     &NUDesignWalker::triRegInit,
     &NUModule::replaceTriRegInits
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<NUModule,NUModule::InitialBlockLoop,NUInitialBlockList,NUStructuredProc>
    (this_module,
     &NUModule::loopInitialBlocks,
     &NUDesignWalker::structuredProc,
     &NUModule::replaceInitialBlocks
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = nested<NUModule,NUModule::AlwaysBlockLoop,NUAlwaysBlockList,
    NUStructuredProc>
    (this_module,
     &NUModule::loopAlwaysBlocks,
     &NUDesignWalker::structuredProc,
     &NUModule::replaceAlwaysBlocks
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  if (mWalkDeclaredNets) {
    // Walk the ports & module-scoped nets
    status = nested<NUModule,NUModule::NetLoop,NUNetList>
      (this_module,
       &NUModule::loopNets,
       &NUDesignWalker::net
        );

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
    default: NU_ASSERT(0, this_module); break;
    }

    // Walk the special nets
    NUNetVector vnets;
    vnets.push_back(this_module->getExtNet());
    vnets.push_back(this_module->getOutputSysTaskNet());
    vnets.push_back(this_module->getControlFlowNet());
    for (NUNetVectorLoop p(vnets); !p.atEnd(); ++p) {
      NUNet* this_net = *p;
      status = net(this_net);
      switch (status) {
      case NUDesignCallback::eNormal: break;
      case NUDesignCallback::eDelete: NU_ASSERT(0, this_net); break;
      case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
      case NUDesignCallback::eSkip: NU_ASSERT(0, this_net); break;
      default: NU_ASSERT(0, this_module); break;
      }
    }
  } // if

  // Walk the declaration scopes
  status = nested<NUModule,NUNamedDeclarationScopeLoop,NUNamedDeclarationScopeList>
    (this_module,
     &NUModule::loopDeclarationScopes,
     &NUDesignWalker::declarationScope
      );
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module); break;
  default: NU_ASSERT(0, this_module); break;
  }

  status = mCallback(NUDesignCallback::ePost, this_module);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_module); break;
  }

  rememberVisited(this_module);

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::module

template<class ContainerT, class LoopT, class ReplaceT, class TraverseT>
NUDesignCallback::Status
NUDesignWalker::nested(ContainerT *this_container,
                       LoopT (ContainerT::* loop_fn)(),
		       NUDesignCallback::Status (NUDesignWalker::* traverseFn)(TraverseT*),
                       void (ContainerT::* replaceFn)(const ReplaceT&))
{
  typedef typename LoopT::value_type NestedT;
  UtVector<NestedT> removals;

  // Give the user a heads-up that we are heading into a new statement list, so
  // he can distinguish between the branches of an If or Case
  NUDesignCallback::Status status = mCallback.stmtList(NUDesignCallback::ePre, this_container);

  for (LoopT loop = (this_container->*loop_fn)();
       (not loop.atEnd()) and (status != NUDesignCallback::eStop);
       ++loop)
  {
    status = (this->*traverseFn)(*loop);

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: removals.push_back(*loop); status = NUDesignCallback::eNormal; break;
    case NUDesignCallback::eStop: status = NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,*loop); break;
    default: NU_ASSERT(0, *loop); break;
    }
  }

  if (status != NUDesignCallback::eStop) {
    status = mCallback.stmtList(NUDesignCallback::ePost, this_container);
  }

  if (removals.empty()) {
    return status;
  }

  INFO_ASSERT(replaceFn, "No remove function supplied, cannot delete");
  NUDesignCallback::Status remove_status = NUDesignCallback::eNormal;

  // Remove after the container has been traversed, so we don't modify the container
  // while traversing.
  ReplaceT new_list;
  
  UInt32 idx = 0;
  for (LoopT loop = (this_container->*loop_fn)();
       !loop.atEnd(); ++loop)
  {
    NestedT nested_obj = *loop;
    NestedT repl = nested_obj;
    if ((idx < removals.size()) && (removals[idx] == nested_obj)) {
      ++idx;
      if (mVerbose) {
        UtIO::cout() << "Deleting the following Nucleus node:" << UtIO::endl;
        nested_obj->print(0,2);
      }

      repl = mCallback.replacement(nested_obj);

      if (mWalkReplacements and repl and remove_status!=NUDesignCallback::eStop) {
        remove_status = (this->*traverseFn)(repl);
        switch (remove_status) {
        case NUDesignCallback::eNormal: break;
        case NUDesignCallback::eDelete: NU_ASSERT(0, repl); break; // double-deletion.
        case NUDesignCallback::eStop: status = NUDesignCallback::eStop; break;
        case NUDesignCallback::eSkip: NU_ASSERT(0, repl); break;
        default: NU_ASSERT(0, repl); break;
        }
      }
    } // if
    if (repl != NULL) {
      new_list.push_back(repl);
    }
  } // for

  (this_container->*replaceFn)(new_list);

  // delete the objects after having finished all the replacements
  for (idx = 0; idx < removals.size(); ++idx) {
    NestedT doomed = removals[idx];
    delete doomed;
  }

  return status;
}

NUDesignCallback::Status NUDesignWalker::moduleInstance(NUModuleInstance *this_module_inst)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, this_module_inst);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_module_inst); break;
  }

  status = nested<NUModuleInstance, NUModuleInstance::PortConnectionInputLoop,
    NUPortConnectionVector>
    (this_module_inst,
     &NUModuleInstance::loopInputPortConnections,
     &NUDesignWalker::inputPortConnection
     );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module_inst); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module_inst); break;
  default: NU_ASSERT(0, this_module_inst); break;
  }

  status = nested<NUModuleInstance, NUModuleInstance::PortConnectionOutputLoop,
    NUPortConnectionVector>
    (this_module_inst,
     &NUModuleInstance::loopOutputPortConnections,
     &NUDesignWalker::outputPortConnection);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module_inst); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module_inst); break;
  default: NU_ASSERT(0, this_module_inst); break;
  }

  status = nested<NUModuleInstance, NUModuleInstance::PortConnectionBidLoop,
    NUPortConnectionVector>
    (this_module_inst,
     &NUModuleInstance::loopBidPortConnections,
     &NUDesignWalker::bidPortConnection);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module_inst); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module_inst); break;
  default: NU_ASSERT(0, this_module_inst); break;
  }

  status = module(this_module_inst->getModule());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_module_inst); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_module_inst); break;
  default: NU_ASSERT(0, this_module_inst); break;
  }

  status = mCallback(NUDesignCallback::ePost, this_module_inst);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_module_inst); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::inputPortConnection(NUPortConnectionInput *this_conn)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, this_conn);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_conn); break;
  }

  NUExpr *actual = this_conn->getActual();
  if (actual) {
    status = expr(actual);
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, this_conn); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, this_conn); break;
    default: NU_ASSERT(0, this_conn); break;
    }
  }

  status = mCallback(NUDesignCallback::ePost, this_conn);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_conn); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::outputPortConnection(NUPortConnectionOutput *this_conn)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, this_conn);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_conn); break;
  }

  NULvalue *actual = this_conn->getActual();
  if (actual) {
    status = lvalue(actual);
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, this_conn); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, this_conn); break;
    default: NU_ASSERT(0, this_conn); break;
    }
  }

  status = mCallback(NUDesignCallback::ePost, this_conn);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_conn); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::bidPortConnection(NUPortConnectionBid *this_conn)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, this_conn);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_conn); break;
  }

  NULvalue *actual = this_conn->getActual();
  if (actual) {
    status = lvalue(actual);
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, this_conn); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, this_conn); break;
    default: NU_ASSERT(0, this_conn); break;
    }
  }

  status = mCallback(NUDesignCallback::ePost, this_conn);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, this_conn); break;
  }

  return NUDesignCallback::eNormal;
}


class NUDesignRecursionBlocker {
public:
  NUDesignRecursionBlocker(NUTaskSet* taskSet):
    mTaskSet(taskSet),
    mTask(NULL)
  {
  }

  ~NUDesignRecursionBlocker() {
    if (mTask != NULL) {
      bool erased = mTaskSet->erase(mTask) != 0;
      NU_ASSERT(erased, mTask);
    }
  }

  bool recurse(NUTask* task) {
    if (mTaskSet->find(task) != mTaskSet->end()) {
      return true;
    }
    mTaskSet->insert(task);
    mTask = task;
    return false;
  }

private:
  NUTaskSet* mTaskSet;
  NUTask* mTask;
};

NUDesignCallback::Status NUDesignWalker::task(NUTask *this_task)
{
  if (haveVisited(this_task)) {
    return NUDesignCallback::eNormal;
  }
  NUDesignRecursionBlocker recursionBlocker(mCurrentTasks);
  if (recursionBlocker.recurse(this_task)) {
    return NUDesignCallback::eNormal;
  }

  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, this_task);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,this_task); break;
  }

  status = helperTF(this_task);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,this_task); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,this_task); break;
  default: NU_ASSERT(0,this_task); break;
  }

  status = mCallback(NUDesignCallback::ePost, this_task);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,this_task); break;
  }

  rememberVisited(this_task);

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::tf(NUTF *this_tf)
{
  NUTask *this_task = dynamic_cast<NUTask*>(this_tf);
  if (this_task) {
    return NUDesignWalker::task(this_task);
  }

  NU_ASSERT(0, this_tf);        // functions should be gone....

  return NUDesignCallback::eInvalid;
}


NUDesignCallback::Status NUDesignWalker::helperTF(NUTF *this_tf)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  if (mWalkDeclaredNets) {
    // Walk the ports & module-scoped nets
    status = nested<NUTF,NUNetLoop,NUNetVector>
      (this_tf,
       &NUTF::loopLocals,
       &NUDesignWalker::net
        );
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, this_tf); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, this_tf); break;
    default: NU_ASSERT(0, this_tf); break;
    }

    // task formals
    status = nested<NUTF,NUNetVectorLoop,NUNetVector>
      (this_tf, &NUTF::loopArgs, &NUDesignWalker::net);

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, this_tf); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, this_tf); break;
    default: NU_ASSERT(0, this_tf); break;
    }
  } // if

  // Walk the declaration scopes
  status = nested<NUTF,NUNamedDeclarationScopeLoop,NUNamedDeclarationScopeList>
    (this_tf,
     &NUTF::loopDeclarationScopes,
     &NUDesignWalker::declarationScope
      );
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, this_tf); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, this_tf); break;
  default: NU_ASSERT(0, this_tf); break;
  }

  status = nested<NUTF, NUStmtLoop, NUStmtList>
    (this_tf, &NUTF::loopStmts, &NUDesignWalker::stmt, &NUTF::replaceStmtList);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,this_tf); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,this_tf); break;
  default: NU_ASSERT(0,this_tf); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::contAssign(NUContAssign *assign)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, assign);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, assign); break;
  }

  status = lvalue(assign->getLvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, assign); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, assign); break;
  default: NU_ASSERT(0, assign); break;
  }

  status = expr(assign->getRvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, assign); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, assign); break;
  default: NU_ASSERT(0, assign); break;
  }

  status = mCallback(NUDesignCallback::ePost, assign);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, assign); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::enabledDriver(NUEnabledDriver *driver)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, driver);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, driver); break;
  }

  status = lvalue(driver->getLvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, driver); break;
  }

  status = expr(driver->getEnable());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, driver); break;
  }

  status = expr(driver->getDriver());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, driver); break;
  }

  status = mCallback(NUDesignCallback::ePost, driver);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, driver); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::triRegInit(NUTriRegInit *init)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, init);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, init); break;
  }

  status = lvalue(init->getLvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, init); break;
  }

  status = mCallback(NUDesignCallback::ePost, init);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, init); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status
NUDesignWalker::declarationScope(NUNamedDeclarationScope* declScope) {
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, declScope);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, declScope); break;
  }

  status = nested<
    NUNamedDeclarationScope,
    NUModuleInstanceMultiLoop,
    NUModuleInstanceList>
    (declScope,
     &NUNamedDeclarationScope::loopDeclScopeInstances,
     &NUDesignWalker::moduleInstance,
     &NUNamedDeclarationScope::replaceModuleInstances
      );

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, declScope); break;
  }

  if (mWalkDeclaredNets) {
    status = nested<NUNamedDeclarationScope, NUNetLoop, NUNetVector>
      (declScope,
       &NUNamedDeclarationScope::loopLocals,
       &NUDesignWalker::net);
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
    default: NU_ASSERT(0, declScope); break;
    }
  }

  status = nested<NUNamedDeclarationScope,NUNamedDeclarationScopeLoop,NUNamedDeclarationScopeList>
    (declScope,
     &NUNamedDeclarationScope::loopDeclarationScopes,
     &NUDesignWalker::declarationScope
      );
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, declScope); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, declScope); break;
  default: NU_ASSERT(0, declScope); break;
  }

  status = mCallback(NUDesignCallback::ePost, declScope);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, declScope); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, declScope); break;
  }

  return status;
} // NUDesignWalker::declarationScope

NUDesignCallback::Status NUDesignWalker::initialBlock(NUInitialBlock *initial_block)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, initial_block);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,initial_block); break;
  }

  NUBlock *block = initial_block->getBlock();
  status = stmt(block);

  bool must_delete = false;
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: must_delete = true; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,initial_block); break;
  default: NU_ASSERT(0,initial_block); break;
  }

  status = mCallback(NUDesignCallback::ePost, initial_block);

  switch (status) {
  case NUDesignCallback::eNormal: NU_ASSERT(not must_delete, initial_block); break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,initial_block); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::alwaysBlock(NUAlwaysBlock *always_block)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, always_block);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,always_block); break;
  }

  for (NUAlwaysBlock::EdgeExprLoop loop = always_block->loopEdgeExprs();
       not loop.atEnd();
       ++loop) {
    NUDesignCallback::Status status = expr(*loop);
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,always_block); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,always_block); break;
    default: NU_ASSERT(0,always_block); break;
    }
  }

  for (NUAlwaysBlock::ExprLoop loop = always_block->loopLevelExprs();
       not loop.atEnd();
       ++loop) {
    NUDesignCallback::Status status = expr(*loop);
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,always_block); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,always_block); break;
    default: NU_ASSERT(0,always_block); break;
    }
  }

  NUBlock *block = always_block->getBlock();
  status = stmt(block);

  bool must_delete = false;
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: must_delete = true; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,always_block); break;
  default: NU_ASSERT(0,always_block); break;
  }

  status = mCallback(NUDesignCallback::ePost, always_block);

  switch (status) {
  case NUDesignCallback::eNormal: NU_ASSERT(not must_delete, always_block); break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,always_block); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::structuredProc(NUStructuredProc *proc)
{
  switch(proc->getType()) {
    case eNUAlwaysBlock:
      return alwaysBlock((NUAlwaysBlock*)(proc));
      break;
    case eNUInitialBlock:
      return initialBlock((NUInitialBlock*)(proc));
      break;
  default:
    NU_ASSERT(0,proc);
  }
  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::stmt(NUStmt *stmt)
{
  switch (stmt->getType()) {
  case eNUIf:
    return ifStmt((NUIf*) stmt);
  case eNUCase:
    return caseStmt((NUCase*) stmt);
  case eNUBlock:
    return blockStmt((NUBlock*) stmt);
  case eNUFor:
    return forStmt((NUFor*) stmt);
  case eNUBlockingAssign:
    return blockingAssign((NUBlockingAssign*) stmt);
  case eNUNonBlockingAssign:
    return nonBlockingAssign((NUNonBlockingAssign*) stmt);
  case eNUContAssign:
    return contAssign((NUContAssign*) stmt);
  case eNUTaskEnable:
    return taskEnable((NUTaskEnable*) stmt);
  case eNUCModelCall:
    return cmodelCall((NUCModelCall*) stmt);
  case eNUOutputSysTask:
    return sysOutputSys((NUOutputSysTask*) stmt);
  case eNUFOpenSysTask:
    return sysFOpen((NUFOpenSysTask*)stmt);
  case eNUFCloseSysTask:
    return sysFClose((NUFCloseSysTask*)stmt);
  case eNUFFlushSysTask:
    return sysFFlush((NUFFlushSysTask*)stmt);
  case eNUSysRandom:
    return sysRandom((NUSysRandom*)stmt);
  case eNUControlSysTask:
    return sysControlSys((NUControlSysTask*)stmt);
  case eNUReadmemX:
    return sysReadmemX((NUReadmemX*)stmt);
  case eNUInputSysTask:
    return sysInputSys((NUInputSysTask*)stmt);
  case eNUContEnabledDriver:
  case eNUBlockingEnabledDriver:
    return enabledDriver((NUEnabledDriver*) stmt);
  case eNUBreak:
    return breakStmt((NUBreak*) stmt);
  default:
    NU_ASSERT(0, stmt);
    break;
  } // switch

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::assign(NUAssign *this_assign)
{
  switch (this_assign->getType()) {
  case eNUBlockingAssign:
    return blockingAssign((NUBlockingAssign*) this_assign);
  case eNUNonBlockingAssign:
    return nonBlockingAssign((NUNonBlockingAssign*) this_assign);
  case eNUContAssign:
    return contAssign((NUContAssign*) this_assign);
  default:
    NU_ASSERT(0, this_assign);
    break;
  } // switch

  return NUDesignCallback::eInvalid;
}


NUDesignCallback::Status NUDesignWalker::blockingAssign(NUBlockingAssign *blocking_assign)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, blocking_assign);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,blocking_assign); break;
  }

  status = lvalue(blocking_assign->getLvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, blocking_assign); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, blocking_assign); break;
  default: NU_ASSERT(0, blocking_assign); break;
  }

  status = expr(blocking_assign->getRvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, blocking_assign); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, blocking_assign); break;
  default: NU_ASSERT(0, blocking_assign); break;
  }

  status = mCallback(NUDesignCallback::ePost, blocking_assign);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, blocking_assign); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::nonBlockingAssign(NUNonBlockingAssign *non_blocking_assign)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, non_blocking_assign);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, non_blocking_assign); break;
  }

  status = lvalue(non_blocking_assign->getLvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, non_blocking_assign); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, non_blocking_assign); break;
  default: NU_ASSERT(0, non_blocking_assign); break;
  }

  status = expr(non_blocking_assign->getRvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, non_blocking_assign); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, non_blocking_assign); break;
  default: NU_ASSERT(0, non_blocking_assign); break;
  }

  status = mCallback(NUDesignCallback::ePost, non_blocking_assign);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, non_blocking_assign); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::ifStmt(NUIf *if_stmt)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, if_stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, if_stmt); break;
  }

  status = expr(if_stmt->getCond());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, if_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, if_stmt); break;
  default: NU_ASSERT(0, if_stmt); break;
  }

  status = nested<NUIf, NUStmtLoop, NUStmtList>
    (if_stmt, &NUIf::loopThen, &NUDesignWalker::stmt, &NUIf::replaceThen);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, if_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, if_stmt); break;
  default: NU_ASSERT(0, if_stmt); break;
  }

  status = nested<NUIf, NUStmtLoop, NUStmtList>
    (if_stmt, &NUIf::loopElse, &NUDesignWalker::stmt, &NUIf::replaceElse);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, if_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, if_stmt); break;
  default: NU_ASSERT(0, if_stmt); break;
  }

  status = mCallback(NUDesignCallback::ePost, if_stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, if_stmt); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::caseStmt(NUCase *case_stmt)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, case_stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, case_stmt); break;
  }

  status = expr(case_stmt->getSelect());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, case_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, case_stmt); break;
  default: NU_ASSERT(0, case_stmt); break;
  }

  status = nested<NUCase, NUCase::ItemLoop, NUCaseItemList>
    (case_stmt, &NUCase::loopItems, &NUDesignWalker::caseItem, &NUCase::replaceItems);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0, case_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0, case_stmt); break;
  default: NU_ASSERT(0, case_stmt); break;
  }

  status = mCallback(NUDesignCallback::ePost, case_stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, case_stmt); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::caseItem(NUCaseItem *item)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, item);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,item); break;
  }

  status = nested<NUCaseItem, NUCaseItem::ConditionLoop,
    NUCaseItem::ConditionVector>
    (item, &NUCaseItem::loopConditions, &NUDesignWalker::caseCondition);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,item); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,item); break;
  default: NU_ASSERT(0,item); break;
  }

  status = nested<NUCaseItem, NUStmtLoop, NUStmtList>
    (item, &NUCaseItem::loopStmts, &NUDesignWalker::stmt,
     &NUCaseItem::replaceStmts);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,item); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,item); break;
  default: NU_ASSERT(0,item); break;
  }

  status = mCallback(NUDesignCallback::ePost, item);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,item); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::caseCondition(NUCaseCondition *condition)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, condition);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,condition); break;
  }

  status = expr(condition->getExpr());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,condition); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,condition); break;
  default: NU_ASSERT(0,condition); break;
  }

  if (condition->getMask()) {
    status = expr(condition->getMask());

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,condition); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,condition); break;
    default: NU_ASSERT(0,condition); break;
    }
  }

  status = mCallback(NUDesignCallback::ePost, condition);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,condition); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::blockScope(NUBlockScope *block_scope)
{
  switch(block_scope->getScopeType()) {
  case NUScope::eBlock: return blockStmt((NUBlock*)block_scope); break;
  case NUScope::eTask:  return task((NUTask*)block_scope);       break;
  default:         NU_ASSERT(0,((NUBase*)block_scope));
  }
  return NUDesignCallback::eInvalid;
}


NUDesignCallback::Status NUDesignWalker::blockStmt(NUBlock *block)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, block);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,block); break;
  }

  if (mWalkDeclaredNets) {
    // Walk the ports & module-scoped nets
    status = nested<NUBlock,NUNetLoop,NUNetVector>
      (block,
       &NUBlock::loopLocals,
       &NUDesignWalker::net
        );
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, block); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0, block); break;
    default: NU_ASSERT(0, block); break;
    }
  }

  status = nested<NUBlock, NUStmtLoop, NUStmtList>
    (block, &NUBlock::loopStmts, &NUDesignWalker::stmt, &NUBlock::replaceStmtList);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,block); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,block); break;
  default: NU_ASSERT(0,block); break;
  }

  status = mCallback(NUDesignCallback::ePost, block);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,block); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::forStmt(NUFor *for_stmt)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, for_stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,for_stmt); break;
  }

  status = expr(for_stmt->getCondition());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,for_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,for_stmt); break;
  default: NU_ASSERT(0,for_stmt); break;
  }

  status = nested<NUFor, NUStmtLoop, NUStmtList>
    (for_stmt, &NUFor::loopInitial, &NUDesignWalker::stmt, &NUFor::replaceInitial);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,for_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,for_stmt); break;
  default: NU_ASSERT(0,for_stmt); break;
  }


  status = nested<NUFor, NUStmtLoop, NUStmtList>
    (for_stmt, &NUFor::loopAdvance, &NUDesignWalker::stmt, &NUFor::replaceAdvance);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,for_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,for_stmt); break;
  default: NU_ASSERT(0,for_stmt); break;
  }


  status = nested<NUFor, NUStmtLoop, NUStmtList>
    (for_stmt, &NUFor::loopBody, &NUDesignWalker::stmt, &NUFor::replaceBody);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,for_stmt); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,for_stmt); break;
  default: NU_ASSERT(0,for_stmt); break;
  }

  status = mCallback(NUDesignCallback::ePost, for_stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,for_stmt); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::taskEnable(NUTaskEnable *task_enable)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, task_enable);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,task_enable); break;
  }

  status = nested<NUTaskEnable, NUTFArgConnectionLoop, NUTFArgConnectionVector>
    (task_enable, &NUTaskEnable::loopArgConnections,
     &NUDesignWalker::tfArgConnection);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,task_enable); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,task_enable); break;
  default: NU_ASSERT(0,task_enable); break;
  }

  if (mWalkTasksFromTaskEnables) {
    NUTask * the_task = NULL;
    if (task_enable->isHierRef()) {
      NUTaskEnableHierRef * enable_hier_ref = dynamic_cast<NUTaskEnableHierRef*>(task_enable);
      the_task = enable_hier_ref->getRepresentativeResolution();
    } else {
      the_task = task_enable->getTask();
    }
    status = task(the_task);
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,task_enable); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,task_enable); break;
    default: NU_ASSERT(0,task_enable); break;
    }
  }

  status = mCallback(NUDesignCallback::ePost, task_enable);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,task_enable); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::expr(NUExpr* e) {
  NUDesignCallback::Status status = exprDispatch(e, NUDesignCallback::ePre);
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eSkip:   return NUDesignCallback::eNormal;
  case NUDesignCallback::eStop:   return NUDesignCallback::eStop;
  case NUDesignCallback::eDelete: NU_ASSERT(0, e); break;
  default: NU_ASSERT(0, e); break;
  }

  // Not using nested() here as a matter of convenience, but for
  // consistency we may eventually want to use it.
  for (UInt32 arg = 0, n = e->getNumArgs(); arg < n; ++arg) {
    status = expr(e->getArg(arg));

    switch (status) {
    case NUDesignCallback::eSkip: // skipping a sub-expr, but go onto next one
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0, e); break;
    case NUDesignCallback::eStop:   return NUDesignCallback::eStop;
    default: NU_ASSERT(0, e);       break;
    }
  }

  status = exprDispatch(e, NUDesignCallback::ePost);
  if (status == NUDesignCallback::eSkip) {
    status = NUDesignCallback::eNormal;
  }

  return status;
} // NUDesignCallback::Status NUDesignWalker::expr

NUDesignCallback::Status NUDesignWalker::exprDispatch(NUExpr* e,
                                                      NUDesignCallback::Phase phase)
{
  switch (e->getType()) {
  case NUExpr::eNUVarselRvalue:
    return mCallback(phase, (NUVarselRvalue*) e);
    break;
  case NUExpr::eNUConstXZ:
  case NUExpr::eNUConstNoXZ:
    return mCallback(phase, (NUConst*) e);
  case NUExpr::eNUEdgeExpr:
    return mCallback(phase, (NUEdgeExpr*) e);
  case NUExpr::eNUSysFunctionCall:
    return mCallback(phase, (NUSysFunctionCall*) e);
  case NUExpr::eNUIdentRvalue:
  case NUExpr::eNUIdentRvalueElab:
    return mCallback(phase, (NUIdentRvalue*) e);
  case NUExpr::eNUMemselRvalue:
    return mCallback(phase, (NUMemselRvalue*) e);
  case NUExpr::eNUUnaryOp:
    return mCallback(phase, (NUUnaryOp*) e);
  case NUExpr::eNUBinaryOp:
    return mCallback(phase, (NUBinaryOp*) e);
  case NUExpr::eNUTernaryOp:
    return mCallback(phase, (NUTernaryOp*) e);
  case NUExpr::eNULut:
    return mCallback(phase, (NULut*) e);
  case NUExpr::eNUConcatOp:
    return mCallback(phase, (NUConcatOp*) e);
  case NUExpr::eNUAttribute:
    return mCallback(phase, (NUAttribute*) e);
  case NUExpr::eNUNullExpr:
    return mCallback(phase, (NUNullExpr*) e);
  case NUExpr::eNUCompositeExpr:
    return mCallback(phase, ( NUCompositeExpr*) e);
  case NUExpr::eNUCompositeSelExpr:
    // This is more complex than the rest of the exprs; defer to a helper
    if (compositeSelExpr((NUCompositeSelExpr*) e) == NUDesignCallback::eStop) {
      return NUDesignCallback::eStop;
    }
    return NUDesignCallback::eSkip;
  case NUExpr::eNUCompositeFieldExpr:
    if (compositeFieldExpr((NUCompositeFieldExpr*) e) == NUDesignCallback::eStop) {
      return NUDesignCallback::eStop;
    }
    return NUDesignCallback::eSkip;
  case NUExpr::eNUCompositeIdentRvalue:
    return mCallback(phase, ( NUCompositeIdentRvalue*) e);
    // no default
  }
  NU_ASSERT(0, e);
  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::exprDispatch

NUDesignCallback::Status
NUDesignWalker::net(NUNet* net)
{
  NUDesignCallback::Status status = mCallback(NUDesignCallback::ePre, net);
  if (status == NUDesignCallback::eNormal) {
    status = mCallback(NUDesignCallback::ePost, net);
  }
  return status;
}

NUDesignCallback::Status NUDesignWalker::compositeSelExpr(NUCompositeSelExpr *compositesel_expr)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, compositesel_expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositesel_expr); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositesel_expr); break;
  }

  const UInt32 numDims = compositesel_expr->getNumIndices();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    status = expr(compositesel_expr->getIndex(i));

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,compositesel_expr); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,compositesel_expr); break;
    default: NU_ASSERT(0,compositesel_expr); break;
    }
  }

  NUCompositeInterfaceExpr *obj = compositesel_expr->getIdentExpr();
  switch ( obj->getCompositeType( ))
  {
  case NUCompositeInterfaceExpr::eCompositeIdentRvalue:
    status = expr(dynamic_cast<NUCompositeIdentRvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeSelExpr:
    status = compositeSelExpr(dynamic_cast<NUCompositeSelExpr*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeFieldExpr:
    status = compositeFieldExpr(dynamic_cast<NUCompositeFieldExpr*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeIdentLvalue:
  case NUCompositeInterfaceExpr::eCompositeSelLvalue:
  case NUCompositeInterfaceExpr::eCompositeFieldLvalue:
    NU_ASSERT( false, compositesel_expr );
    break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, compositesel_expr); break;
  }

  status = mCallback(NUDesignCallback::ePost, compositesel_expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositesel_expr); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositesel_expr); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::compositeFieldExpr(NUCompositeFieldExpr *compositefield_expr)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, compositefield_expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositefield_expr); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositefield_expr); break;
  }

  NUCompositeInterfaceExpr *obj = compositefield_expr->getIdentExpr();
  switch ( obj->getCompositeType( ))
  {
  case NUCompositeInterfaceExpr::eCompositeIdentRvalue:
    status = expr( dynamic_cast<NUCompositeIdentRvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeSelExpr:
    status = compositeSelExpr( dynamic_cast<NUCompositeSelExpr*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeFieldExpr:
    status = compositeFieldExpr( dynamic_cast<NUCompositeFieldExpr*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeIdentLvalue:
  case NUCompositeInterfaceExpr::eCompositeSelLvalue:
  case NUCompositeInterfaceExpr::eCompositeFieldLvalue:
    NU_ASSERT( false, compositefield_expr );
    break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, compositefield_expr); break;
  }

  status = mCallback(NUDesignCallback::ePost, compositefield_expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositefield_expr); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositefield_expr); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status NUDesignWalker::lvalue(NULvalue *lvalue)
{
  switch (lvalue->getType()) {
  case eNUIdentLvalue:
    return identLvalue((NUIdentLvalue*) lvalue);
  case eNUPartselIndexLvalue:   // subclass of varselLvalue
  case eNUVarselLvalue:
    return varselLvalue((NUVarselLvalue*) lvalue);
  case eNUConcatLvalue:
    return concatLvalue((NUConcatLvalue*) lvalue);
  case eNUMemselLvalue:
    return memselLvalue((NUMemselLvalue*) lvalue);
  case eNUCompositeLvalue:
    return compositeLvalue((NUCompositeLvalue*) lvalue);
  case eNUCompositeSelLvalue:
    return compositeSelLvalue((NUCompositeSelLvalue*) lvalue);
  case eNUCompositeFieldLvalue:
    return compositeFieldLvalue((NUCompositeFieldLvalue*) lvalue);
  case eNUCompositeIdentLvalue:
    return compositeIdentLvalue(( NUCompositeIdentLvalue*) lvalue );
    break;
  default:
    NU_ASSERT(0, lvalue);
  }
  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status
NUDesignWalker::identLvalue(NUIdentLvalue* ident_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, ident_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,ident_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, ident_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,ident_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status
NUDesignWalker::compositeIdentLvalue(NUCompositeIdentLvalue* ident_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, ident_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,ident_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, ident_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,ident_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status NUDesignWalker::concatLvalue(NUConcatLvalue *concat_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, concat_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,concat_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,concat_lvalue); break;
  }

  status = nested<NUConcatLvalue, NULvalueLoop, NULvalueVector>
    (concat_lvalue, &NUConcatLvalue::loopLvalues, &NUDesignWalker::lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,concat_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,concat_lvalue); break;
  default: NU_ASSERT(0,concat_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, concat_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,concat_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,concat_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::compositeLvalue(NUCompositeLvalue *composite_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, composite_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,composite_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,composite_lvalue); break;
  }

  status = nested<NUCompositeLvalue, NULvalueLoop, NULvalueVector>
    (composite_lvalue, &NUCompositeLvalue::loopLvalues, &NUDesignWalker::lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,composite_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,composite_lvalue); break;
  default: NU_ASSERT(0,composite_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, composite_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,composite_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,composite_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::compositeSelLvalue(NUCompositeSelLvalue *compositesel_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, compositesel_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositesel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositesel_lvalue); break;
  }

  const UInt32 numDims = compositesel_lvalue->getNumIndices();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    status = expr(compositesel_lvalue->getIndex(i));

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,compositesel_lvalue); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,compositesel_lvalue); break;
    default: NU_ASSERT(0,compositesel_lvalue); break;
    }
  }

  NUCompositeInterfaceLvalue *obj = compositesel_lvalue->getIdentExpr();
  switch ( obj->getCompositeType( ))
  {
  case NUCompositeInterfaceExpr::eCompositeIdentLvalue:
    status = compositeIdentLvalue(dynamic_cast<NUCompositeIdentLvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeSelLvalue:
    status = compositeSelLvalue(dynamic_cast<NUCompositeSelLvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeFieldLvalue:
    status = compositeFieldLvalue(dynamic_cast<NUCompositeFieldLvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeIdentRvalue:
  case NUCompositeInterfaceExpr::eCompositeSelExpr:
  case NUCompositeInterfaceExpr::eCompositeFieldExpr:
    NU_ASSERT( false, compositesel_lvalue );
    break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, compositesel_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, compositesel_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositesel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositesel_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::compositeFieldLvalue(NUCompositeFieldLvalue *compositefield_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, compositefield_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositefield_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositefield_lvalue); break;
  }

  NUCompositeInterfaceLvalue *obj = compositefield_lvalue->getIdentExpr();
  switch ( obj->getCompositeType( ))
  {
  case NUCompositeInterfaceExpr::eCompositeIdentLvalue:
    status = compositeIdentLvalue( dynamic_cast<NUCompositeIdentLvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeSelLvalue:
    status = compositeSelLvalue( dynamic_cast<NUCompositeSelLvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeFieldLvalue:
    status = compositeFieldLvalue( dynamic_cast<NUCompositeFieldLvalue*>( obj ));
    break;
  case NUCompositeInterfaceExpr::eCompositeIdentRvalue:
  case NUCompositeInterfaceExpr::eCompositeSelExpr:
  case NUCompositeInterfaceExpr::eCompositeFieldExpr:
    NU_ASSERT( false, compositefield_lvalue );
    break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, compositefield_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, compositefield_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,compositefield_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,compositefield_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::varselLvalue(NUVarselLvalue *bitsel_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, bitsel_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,bitsel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,bitsel_lvalue); break;
  }

  status = expr(bitsel_lvalue->getIndex());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,bitsel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,bitsel_lvalue); break;
  default: NU_ASSERT(0,bitsel_lvalue); break;
  }

  status = lvalue (bitsel_lvalue->getLvalue ());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,bitsel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,bitsel_lvalue); break;
  default: NU_ASSERT(0,bitsel_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, bitsel_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,bitsel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,bitsel_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status NUDesignWalker::memselLvalue(NUMemselLvalue *memsel_lvalue)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, memsel_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,memsel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,memsel_lvalue); break;
  }

  const UInt32 numDims = memsel_lvalue->getNumDims();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    status = expr(memsel_lvalue->getIndex(i));

    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,memsel_lvalue); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,memsel_lvalue); break;
    default: NU_ASSERT(0,memsel_lvalue); break;
    }
  }

  status = lvalue(memsel_lvalue->getIdentLvalue());

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0, memsel_lvalue); break;
  }

  status = mCallback(NUDesignCallback::ePost, memsel_lvalue);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,memsel_lvalue); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,memsel_lvalue); break;
  }

  return NUDesignCallback::eNormal;
}


NUDesignCallback::Status NUDesignWalker::tfArgConnection(NUTFArgConnection *conn)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  switch(conn->getDir()) {
  case eInput:  status = mCallback(NUDesignCallback::ePre, dynamic_cast<NUTFArgConnectionInput*>( conn )); break;
  case eOutput: status = mCallback(NUDesignCallback::ePre, dynamic_cast<NUTFArgConnectionOutput*>( conn )); break;
  case eBid:    status = mCallback(NUDesignCallback::ePre, dynamic_cast<NUTFArgConnectionBid*>( conn )); break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,conn); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,conn); break;
  }

  switch (conn->getDir()) {
  case eInput:   status = expr(dynamic_cast<NUTFArgConnectionInput*>(conn)->getActual()); break;
  case eOutput:  status = lvalue(dynamic_cast<NUTFArgConnectionOutput*>(conn)->getActual()); break;
  case eBid:     status = lvalue(dynamic_cast<NUTFArgConnectionBid*>(conn)->getActual()); break;
  default:       NU_ASSERT(0,conn); break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,conn); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,conn); break;
  default: NU_ASSERT(0,conn); break;
  }

  switch(conn->getDir()) {
  case eInput:  status = mCallback(NUDesignCallback::ePost, dynamic_cast<NUTFArgConnectionInput*>( conn )); break;
  case eOutput: status = mCallback(NUDesignCallback::ePost, dynamic_cast<NUTFArgConnectionOutput*>( conn )); break;
  case eBid:    status = mCallback(NUDesignCallback::ePost, dynamic_cast<NUTFArgConnectionBid*>( conn )); break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,conn); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,conn); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status NUDesignWalker::cmodelCall(NUCModelCall* cmodel_call)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, cmodel_call);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_call); break;
  }

  status = nested<NUCModelCall, NUCModelArgConnectionLoop,
    NUCModelArgConnectionVector>
    (cmodel_call, &NUCModelCall::loopArgConnections,
     &NUDesignWalker::cmodelArgConnection);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,cmodel_call); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,cmodel_call); break;
  default: NU_ASSERT(0,cmodel_call); break;
  }

  status = mCallback(NUDesignCallback::ePost, cmodel_call);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_call); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status NUDesignWalker::cmodelFn(NUCModelFn* cmodel_fn) {
  NUDesignCallback::Status status = mCallback(NUDesignCallback::ePre, cmodel_fn);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_fn); break;
  }

  status = mCallback(NUDesignCallback::ePost, cmodel_fn);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_fn); break;
  }

  return status;
}

NUDesignCallback::Status NUDesignWalker::cmodelPort(NUCModelPort* cmodel_port) {
  NUDesignCallback::Status status = mCallback(NUDesignCallback::ePre, cmodel_port);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_port); break;
  }

  status = mCallback(NUDesignCallback::ePost, cmodel_port);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_port); break;
  }

  return status;
}

NUDesignCallback::Status
NUDesignWalker::cmodelInterface(NUCModelInterface* cmodel_interface) {
  NUDesignCallback::Status status;

  status = mCallback(NUDesignCallback::ePre, cmodel_interface);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_interface); break;
  }

  nested<NUCModelInterface, NUCModelInterface::PortsLoop, NUCModelPortVector>
    (cmodel_interface, &NUCModelInterface::loopPorts, &NUDesignWalker::cmodelPort);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_interface); break;
  }

  nested<NUCModelInterface, NUCModelFnLoop, NUCModelFnVector>
    (cmodel_interface, &NUCModelInterface::loopFunctions,
     &NUDesignWalker::cmodelFn);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_interface); break;
  }

  status = mCallback(NUDesignCallback::ePost, cmodel_interface);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel_interface); break;
  }

  return status;
}

NUDesignCallback::Status NUDesignWalker::breakStmt(NUBreak* stmt)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,stmt); break;
  }

  status = mCallback(NUDesignCallback::ePost, stmt);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,stmt); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status NUDesignWalker::sysRandom(NUSysRandom* sysRand)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysRand);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysRand); break;
  }

  status = lvalue(sysRand->getOutLvalue());
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysRand); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysRand); break;
  default: NU_ASSERT(0,sysRand); break;
  }

  if (sysRand->getSeedLvalue() != NULL) {
    status = lvalue(sysRand->getSeedLvalue());
    switch (status) {
      case NUDesignCallback::eNormal: break;
      case NUDesignCallback::eDelete: NU_ASSERT(0,sysRand); break;
      case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
      case NUDesignCallback::eSkip: NU_ASSERT(0,sysRand); break;
      default: NU_ASSERT(0,sysRand); break;
    }
  }

  status = nested<NUSysRandom, NUExprLoop, NUExprVector>
    (sysRand, &NUSysRandom::loopArgs, &NUDesignWalker::expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysRand); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysRand); break;
  default: NU_ASSERT(0,sysRand); break;
  }

  status = mCallback(NUDesignCallback::ePost, sysRand);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysRand); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysRandom


NUDesignCallback::Status NUDesignWalker::sysReadmemX(NUReadmemX* sysMemXTask)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysMemXTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysMemXTask); break;
  }

  status = lvalue(sysMemXTask->getLvalue());
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysMemXTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysMemXTask); break;
  default: NU_ASSERT(0,sysMemXTask); break;
  }

  // at the moment there is nothing in the args loop
  // but there will be in the future
  status = nested<NUReadmemX, NUExprLoop, NUExprVector>
    (sysMemXTask, &NUReadmemX::loopArgs, &NUDesignWalker::expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysMemXTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysMemXTask); break;
  default: NU_ASSERT(0,sysMemXTask); break;
  }

  status = mCallback(NUDesignCallback::ePost, sysMemXTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysMemXTask); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysMemXTask


// shamelessly patterned after sysRandom
// this and sysRandom should be refactored into a single function that
// operates on NUSysTask
NUDesignCallback::Status NUDesignWalker::sysFOpen(NUFOpenSysTask* sysFOpenTask)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysFOpenTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysFOpenTask); break;
  }

  status = lvalue(sysFOpenTask->getLvalue());
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysFOpenTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysFOpenTask); break;
  default: NU_ASSERT(0,sysFOpenTask); break;
  }

  if (sysFOpenTask->getStatusLvalue())
  {
    status = lvalue(sysFOpenTask->getStatusLvalue());
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,sysFOpenTask); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,sysFOpenTask); break;
    default: NU_ASSERT(0,sysFOpenTask); break;
    }
  }

  status = nested<NUFOpenSysTask, NUExprLoop, NUExprVector>
    (sysFOpenTask, &NUFOpenSysTask::loopArgs, &NUDesignWalker::expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysFOpenTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysFOpenTask); break;
  default: NU_ASSERT(0,sysFOpenTask); break;
  }

  status = mCallback(NUDesignCallback::ePost, sysFOpenTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysFOpenTask); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysFOpen


NUDesignCallback::Status NUDesignWalker::sysFClose(NUFCloseSysTask* sysFCloseTask)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysFCloseTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysFCloseTask); break;
  }

  status = nested<NUFCloseSysTask, NUExprLoop, NUExprVector>
    (sysFCloseTask, &NUFCloseSysTask::loopArgs, &NUDesignWalker::expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysFCloseTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysFCloseTask); break;
  default: NU_ASSERT(0,sysFCloseTask); break;
  }

  status = mCallback(NUDesignCallback::ePost, sysFCloseTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysFCloseTask); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysFClose

NUDesignCallback::Status NUDesignWalker::sysFFlush(NUFFlushSysTask* sysFFlushTask)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysFFlushTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysFFlushTask); break;
  }

  status = nested<NUFFlushSysTask, NUExprLoop, NUExprVector>
    (sysFFlushTask, &NUFFlushSysTask::loopArgs, &NUDesignWalker::expr);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysFFlushTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysFFlushTask); break;
  default: NU_ASSERT(0,sysFFlushTask); break;
  }
  
  status = mCallback(NUDesignCallback::ePost, sysFFlushTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysFFlushTask); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysFFlush


NUDesignCallback::Status NUDesignWalker::sysOutputSys(NUOutputSysTask* sysOutputSysTask)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysOutputSysTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysOutputSysTask); break;
  }

  status = nested<NUOutputSysTask, NUExprLoop, NUExprVector>
    (sysOutputSysTask, &NUOutputSysTask::loopArgs, &NUDesignWalker::expr);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysOutputSysTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysOutputSysTask); break;
  default: NU_ASSERT(0,sysOutputSysTask); break;
  }

  status = mCallback(NUDesignCallback::ePost, sysOutputSysTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysOutputSysTask); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysOutputSys

NUDesignCallback::Status NUDesignWalker::sysInputSys(NUInputSysTask* sysInputSysTask)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysInputSysTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysInputSysTask); break;
  }

  if ( sysInputSysTask->getReadLvalue() )
  {
    status = lvalue(sysInputSysTask->getReadLvalue());
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,sysInputSysTask); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,sysInputSysTask); break;
    default: NU_ASSERT(0,sysInputSysTask); break;
    }
  }

  if ( sysInputSysTask->getStatusLvalue() )
  {
    status = lvalue(sysInputSysTask->getStatusLvalue());
    switch (status) {
    case NUDesignCallback::eNormal: break;
    case NUDesignCallback::eDelete: NU_ASSERT(0,sysInputSysTask); break;
    case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
    case NUDesignCallback::eSkip: NU_ASSERT(0,sysInputSysTask); break;
    default: NU_ASSERT(0,sysInputSysTask); break;
    }
  }

  status = nested<NUInputSysTask, NUExprLoop, NUExprVector>
    (sysInputSysTask, &NUInputSysTask::loopArgs, &NUDesignWalker::expr);
  
  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysInputSysTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysInputSysTask); break;
  default: NU_ASSERT(0,sysInputSysTask); break;
  }

  status = mCallback(NUDesignCallback::ePost, sysInputSysTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysInputSysTask); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysInputSys

NUDesignCallback::Status NUDesignWalker::sysControlSys(NUControlSysTask* sysControlSysTask)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, sysControlSysTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysControlSysTask); break;
  }

  status = nested<NUControlSysTask, NUExprLoop, NUExprVector>
    (sysControlSysTask, &NUControlSysTask::loopArgs, &NUDesignWalker::expr);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,sysControlSysTask); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop;
  case NUDesignCallback::eSkip: NU_ASSERT(0,sysControlSysTask); break;
  default: NU_ASSERT(0,sysControlSysTask); break;
  }

  status = mCallback(NUDesignCallback::ePost, sysControlSysTask);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,sysControlSysTask); break;
  }

  return NUDesignCallback::eNormal;
} // NUDesignCallback::Status NUDesignWalker::sysControlSys


NUDesignCallback::Status
NUDesignWalker::cmodelArgConnection(NUCModelArgConnection* conn)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  switch (conn->getDir ()) {
  case eInput: status = mCallback (NUDesignCallback::ePre, dynamic_cast <NUCModelArgConnectionInput*> ( conn )); break;
  case eOutput: status = mCallback (NUDesignCallback::ePre, dynamic_cast <NUCModelArgConnectionOutput*> ( conn )); break;
  case eBid: status = mCallback (NUDesignCallback::ePre, dynamic_cast <NUCModelArgConnectionBid*> ( conn )); break;
  default: NU_ASSERT(0,conn); break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,conn); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,conn); break;
  }

  switch (conn->getDir()) {
  case eInput:   status = expr(dynamic_cast<NUCModelArgConnectionInput*>(conn)->getActual()); break;
  case eOutput:  status = lvalue(dynamic_cast<NUCModelArgConnectionOutput*>(conn)->getActual()); break;
  case eBid:     status = lvalue(dynamic_cast<NUCModelArgConnectionBid*>(conn)->getActual()); break;
  default:       NU_ASSERT(0,conn); break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,conn); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,conn); break;
  default: NU_ASSERT(0,conn); break;
  }

  status = cmodelPort(const_cast<NUCModelPort*>(conn->getFormal()));

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,conn); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: NU_ASSERT(0,conn); break;
  default: NU_ASSERT(0,conn); break;
  }

  switch (conn->getDir ()) {
  case eInput: status = mCallback (NUDesignCallback::ePost, dynamic_cast <NUCModelArgConnectionInput*> ( conn )); break;
  case eOutput: status = mCallback (NUDesignCallback::ePost, dynamic_cast <NUCModelArgConnectionOutput*> ( conn )); break;
  case eBid: status = mCallback (NUDesignCallback::ePost, dynamic_cast <NUCModelArgConnectionBid*> ( conn )); break;
  default: NU_ASSERT(0,conn); break;
  }

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: NU_ASSERT(0,conn); break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,conn); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status
NUDesignWalker::cmodel(NUCModel* cmodel)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, cmodel);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel); break;
  }

  status = mCallback(NUDesignCallback::ePost, cmodel);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,cmodel); break;
  }

  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status
NUDesignWalker::attribute(NUAttribute* attribute)
{
  NUDesignCallback::Status status = NUDesignCallback::eInvalid;

  status = mCallback(NUDesignCallback::ePre, attribute);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,attribute); break;
  }

  status = mCallback(NUDesignCallback::ePost, attribute);

  switch (status) {
  case NUDesignCallback::eNormal: break;
  case NUDesignCallback::eDelete: return NUDesignCallback::eDelete; break;
  case NUDesignCallback::eStop: return NUDesignCallback::eStop; break;
  case NUDesignCallback::eSkip: return NUDesignCallback::eNormal; break;
  default: NU_ASSERT(0,attribute); break;
  }

  return NUDesignCallback::eNormal;
}

bool NUDesignWalker::haveVisited(NUScope *scope) const
{
  return mRememberVisited && (mScopeSet.find(scope) != mScopeSet.end());
}


void NUDesignWalker::rememberVisited(NUScope *scope)
{
  if (mRememberVisited)
    mScopeSet.insert(scope);
}

NUDesignCallback::Status NUDesignWalker::contDriver(NUUseDefNode* useDef)
{
  NU_ASSERT(useDef->isContDriver(), useDef);

  // There are currently 5 continuous driver nodes: cont assigns, cont
  // enabled drivers, module instances, structure procedures, and tri
  // reg inits.
  NUDesignCallback::Status status = NUDesignCallback::eNormal;
  switch(useDef->getType()) {
    case eNUContAssign:
    {
      NUContAssign* assign = dynamic_cast<NUContAssign*>(useDef);
      NU_ASSERT(assign != NULL, useDef);
      status = contAssign(assign);
      break;
    }

    case eNUContEnabledDriver:
    {
      NUContEnabledDriver* enabled = dynamic_cast<NUContEnabledDriver*>(useDef);
      NU_ASSERT(enabled != NULL, useDef);
      status = enabledDriver(enabled);
      break;
    }

    case eNUModuleInstance:
    {
      NUModuleInstance* instance = dynamic_cast<NUModuleInstance*>(useDef);
      NU_ASSERT(instance != NULL, useDef);
      status = moduleInstance(instance);
      break;
    }

    case eNUAlwaysBlock:
    case eNUInitialBlock:
    {
      NUStructuredProc* proc = dynamic_cast<NUStructuredProc*>(useDef);
      NU_ASSERT(proc != NULL, useDef);
      status = structuredProc(proc);
      break;
    }

    case eNUTriRegInit:
    {
      NUTriRegInit* init = dynamic_cast<NUTriRegInit*>(useDef);
      NU_ASSERT(init != NULL, useDef);
      status = triRegInit(init);
      break;
    }

    default:
    {
      bool foundUnexpectedUseDef = true;
      NU_ASSERT(!foundUnexpectedUseDef, useDef);
      status = NUDesignCallback::eInvalid;
      break;
    }
  } // switch

  return status;
} // NUDesignCallback::Status NUDesignWalker::contDriver
