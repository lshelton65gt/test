// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef NUEXPR_I_H_
#define NUEXPR_I_H_

// Helper macros for NUExpr implementation files
/*!
 *! There is no need to put them in inc/nucleus/NUExpr.h, which would
 *! expose all the other users of NUExpr.h to these implementation helper
 *! macros
 */

#if 0
#define MANAGED_SIZE(size) \
  do { \
    if (mIsManaged) { \
      NU_ASSERT(size == mBitSize, this); \
      return; \
    } \
  } while (0)
#else
#define MANAGED_SIZE(size) NU_ASSERT(!mIsManaged, this)
#endif

// Resize is expected to always make an expression larger than its self-determined size.
// Using resize to truncate a value is unlikely to really work, because if the
// resized term appears in a larger context, it will be widened as if NO truncation
// had occurred.
#define RESIZE_INVARIANT(k) NU_ASSERT( ((k) >= mBitSize) || ((k) >= determineBitSize()), this)

#endif // NUEXPR_I_H_
