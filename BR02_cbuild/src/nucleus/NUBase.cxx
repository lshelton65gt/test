// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUBase.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUElabBase.h"
#include "nucleus/NUExpr.h"
#include "util/CarbonAssert.h"
#include "util/UtIndent.h"
#include "util/UtIOStream.h"
#include "util/UtHashSet.h"
#include <algorithm>



// Infrastructure to ensure that design walks hit all allocated nucleus --
// at least all classes derived from NUBase
static NUBaseSet* sBaseSet = NULL;
static bool sInit = false;
static bool sCheckBaseSet = false;


//! Static variable which holds assert status
static bool gAssertOK = true;


// During PV optimization, there are multiple NUDesigns.  So during that
// time, not all the NUBase* allocated in this process will be covered
// by any one walker.  Ideally, the NUBaseSet would reside inside the
// Design.  But because we cannot, in general, get to the owner NUDesign
// from an NUBase, we would not know which set to update when an NUBase
// is constructed or destroyed.  This is not a particularly great situation,
// but for now, while there are multiple designs instantiated, we must
// avoid performing the sanity check.  Note that we should still keep
// the sBaseSet updated with the objects from all the designs, as all
// those objects should be made to disappear once the number of NUDesigns
// instantiated goes to 1.

static SInt32 sNumDesigns = 0;

NUBase::NUBase() {
  baseFlags.mMark = false;
  if (!sInit) {
    sCheckBaseSet = (getenv("CARBON_CHECK_NU_WALKER") != NULL);
    sInit = true;
  }

  if (sCheckBaseSet) {
    if (sBaseSet == NULL) {
      sBaseSet = new NUBaseSet;
    }
    sBaseSet->insert(this);
  }
}

NUBase::~NUBase() {
  if (sBaseSet != NULL) {
    bool found = sBaseSet->erase(this);
    INFO_ASSERT(found, "NUBase::~NUBase called for object not in sBaseSet");
    if (sBaseSet->empty()) {
      delete sBaseSet;
      sBaseSet = NULL;
    }
  }
}

void NUBase::pr() const {
  print(false, 0);
}


void NUBase::printElab(STSymbolTable*, bool /* recurse_arg --unused */, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  UtIO::cout() << typeStr() << " is not an elaborated type" << UtIO::endl;
}


void NUBase::print(bool /* recurse_arg --unused */, int numSpaces_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(numSpaces_arg, 0, 150, 0);
  indent(numspaces);
  UtIO::cout() << typeStr() << "(" << this << ")" " : (default print())" << UtIO::endl;
}

void NUBase::indent(int numSpaces_arg)
{
  int  numSpaces = numSpaces_arg;
  if ( std::abs(numSpaces_arg) > 150 )
    numSpaces = 0;
  for (int i = 0; i < numSpaces; ++i)
    UtIO::cout() << " ";
}

void NUBase::addHierRef(NUBase* hr)
{
  NU_ASSERT2("Illegal on this object type" == NULL, this, hr);
}


void NUBase::removeHierRef(NUBase* hr)
{
  NU_ASSERT2("Illegal on this object type" == NULL, this, hr);
}

void NUBase::enableAsserts()
{
  gAssertOK = true;

}

void NUBase::disableAsserts()
{
  gAssertOK = false;

}

bool NUBase::assertOK()
{
  return gAssertOK;
}

void NUBase::printAssertInfoHeader()
{
  UtIO::cerr() << "\n\n******CARBON INTERNAL ERROR*******\n\n";
}

void NUBase::printAssertInfo() const {
  // Print all the info we need about an object in order for the user to
  // find the problem.  We'd like to give human readable data if possible,
  // which is different depending on the type of object
  if (this == NULL) {
    UtIO::cerr() << "(null object)\n";
  } else {
    print(false, 0);
  }
  UtIO::cout().flush();
}

void NUBase::printAssertInfoHelper() const {
  print(true, 0);
}

void NUBase::printAssertInfoFooter(const char* file, int line, const char* expStr) {
  UtIO::cerr() << file << ':' << line << " NU_ASSERT(" << expStr 
               << ") failed\n";
  // print stack trace ???
#ifndef __COVERITY__
  abort();
#endif
}

//! Callback to mark all reachable nucleus objects
class NUMarkNucleusCallback : public NUDesignCallback {
public:
  NUMarkNucleusCallback() {}

  ~NUMarkNucleusCallback() {}

  //! By default, walk through everything.
  Status operator() (Phase phase, NUBase * object) {
    if (phase == ePre) {
      mark(object);
    }
    return eNormal;
  }

  bool isMarked(NUBase* base) const {
    return (mBaseSet.find(base) != mBaseSet.end());
  }

  void mark(NUBase* base) {
    mBaseSet.insert(base);
  }

  void putWalker(NUDesignWalker* walker) {
    mWalker = walker;
  }

private:
  NUBaseSet mBaseSet;
  NUDesignWalker* mWalker;
};


// Verify that a design walker hits all allocated nucleus objects
void NUBase::checkDesignWalker(NUDesign* design, const char* phase) {
  if ((sBaseSet != NULL) && (sNumDesigns <= 1)) {
    NUMarkNucleusCallback mark;
    NUDesignWalker walker(mark, false, false);
    mark.putWalker(&walker);
    walker.putWalkDeclaredNets(true);
    walker.design(design, true);

    UInt32 numUncovered = 0;
    mark.mark(design);          //  The NUDesign object itself

    // dead cmodel interfaces are not cleaned up, so we have to go find
    // them and mark them
    for (NUCModelInterfaceLoop p = design->loopCModelInterfaces();
         !p.atEnd(); ++p)
    {
      NUCModelInterface* cmi = *p;
      walker.cmodelInterface(cmi);
    }

    // Sweep over the allocated objects and make sure we marked all those
    for (NUBaseSet::iterator p = sBaseSet->begin(), e = sBaseSet->end();
         p != e; ++p)
    {
      NUBase* base = *p;

      // Don't worry about elaborated nucleus -- this walk is just
      // validating unelaborated nucleus for now.
      if ((dynamic_cast<NUElabBase*>(base) == NULL) &&
          (dynamic_cast<NUCycle*>(base) == NULL) &&
          (dynamic_cast<NUIdentRvalueElab*>(base) == NULL) &&
          !mark.isMarked(base))
      {
        UtIO::cout() << "Phase " << phase
                     << ": Nucleus object not covered in design walker: \n";
        base->printAssertInfo();
        ++numUncovered;
      }
    }

    INFO_ASSERT(numUncovered == 0,
                "Allocated Nucleus objects exist that were not covered by design walker. Evidence of a memory leak?");
  }
} // void NUBase::checkDesignWalker

void NUBase::composeProtectedNUObject(SourceLocator const &loc, UtString * buf)
{
  UtIndent indent(buf);
  *buf << "<protected>";
  indent.tabToLocColumn();
  *buf << " // ";
  loc.compose(buf);
  indent.newline();
  return; 
}

void NUBase::incDesignCount() {
  ++sNumDesigns;
}

void NUBase::decDesignCount() {
  --sNumDesigns;
  INFO_ASSERT(sNumDesigns >= 0, "Number of designs fell below zero");
}

#if NEXPR_ENABLE

// These methods are intended only for debugging.
void NUBase::NWr () const
{
  NWrite (UtIO::cout ());
}

void NUBase::NWrite (UtOStream &out) const
{
  NExpr::Writer writer (out);
  UtString errmsg;
  if (!write (writer, true, &errmsg) || errmsg.length () > 0) {
    UtIO::cerr () << "ERROR: " << errmsg << "\n";
  }
}

#endif
