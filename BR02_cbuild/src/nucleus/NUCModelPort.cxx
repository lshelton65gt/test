// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUCModelPort.h"
#include "util/UtString.h"

NUCModelPort::NUCModelPort(const char* name, bool isNullPort, 
                           PortDirectionT dir, UInt32 size, UInt32 index) :
  mName(new UtString(name)),
  mIsNullPort(isNullPort),
  mDirection(dir), 
  mIndex(index)
{
  mSizes = new Sizes;
  mSizes->push_back(size);
}


NUCModelPort::~NUCModelPort()
{
  delete mName;
  delete mSizes;
}


int NUCModelPort::compare(const NUCModelPort* other) const
{
  int cmp = mName->compare(*(other->mName));
  return cmp;
}


UInt32 NUCModelPort::getBitSize(UInt32 variant) const
{
  // Make sure the variant exists
  NU_ASSERT(mSizes->size() > variant, this);

  // Get the size for this variant
  return (*mSizes)[variant];
}


UInt32 NUCModelPort::getSize(UInt32 variant) const
{
  UInt32 bitSize = getBitSize(variant);
  return (bitSize + 31) / 32;
}


void NUCModelPort::addSize(UInt32 variant, UInt32 size)
{
  // Make sure the variant does not exist yet
  NU_ASSERT(mSizes->size() == variant, this);

  // Add this size
  mSizes->resize(variant + 1);
  (*mSizes)[variant] = size;
}


UInt32 NUCModelPort::getMaxBitSize() const
{
  UInt32 maxSize = 0;
  for (Sizes::iterator p = mSizes->begin(); p != mSizes->end(); ++p)
  {
    UInt32 size = *p;
    if (size > maxSize)
      maxSize = size;
  }
  NU_ASSERT(maxSize > 0, this);
  return maxSize;
}


UInt32 NUCModelPort::getMaxSize() const
{
  UInt32 maxBitSize = getMaxBitSize();
  return (maxBitSize + 31) / 32;
}
