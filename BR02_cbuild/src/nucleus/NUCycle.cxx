// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUCycle.h"
#include "nucleus/NUExpr.h"
#include "flow/FLNodeElab.h"
#include "flow/FLNode.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"

// define this as true to enable tracing cyclic flow scheduling calls
#define DEBUG_CYCLE_SCHEDULING 0

//! Add the scheduled flow node to the end of the vector
void NUCycleSimple::fill(FLNodeElabVector* vec) const
{
  vec->push_back(mFlow);
}

//! Compose a cycle schedule for a single flow node
void NUCycleSimple::compose(UtString* buf, UInt32 indent) const
{
  bool needsSpace = false;
  buf->append(indent, ' ');

  NUNetElab* netElab = mFlow->getDefNet();
  if (netElab != NULL)
  {
    netElab->compose(buf, netElab->getHier());
    needsSpace = true;
  }
  NUUseDefNode* useDef = mFlow->getUseDefNode();
  if (useDef != NULL)
  {
    NU_ASSERT(!useDef->isCycle(), useDef);
    if (needsSpace)
      *buf << " ";
    useDef->getLoc().compose(buf);      
  }
  *buf << "\n";
}


//! CodeGen size metric
int NUCycleScheduleBlock::size () const
{
  int sum = 0;
  for(const_iterator p = mSequence.begin (); p != mSequence.end (); ++p)
    sum += (*p)->size ();

  return sum;
}

//! Add the flow nodes in the sequence to the end of the vector, in order
void NUCycleScheduleBlock::fill(FLNodeElabVector* vec) const
{
  for (const_iterator p = mSequence.begin(); p != mSequence.end(); ++p)
    (*p)->fill(vec);
}

//! Compose a cycle schedule for a straight-line sequence of flow nodes
void NUCycleScheduleBlock::compose(UtString* buf, UInt32 indent) const
{
  for (const_iterator p = mSequence.begin(); p != mSequence.end(); ++p)
    (*p)->compose(buf,indent);
}

//! CodeGen size metric
int NUCycleBranch::size () const
{
  return mTrueBlock.size () + mFalseBlock.size () + 1;
}

//! Add the branching flow to the vector, using the 'else' clause order
void NUCycleBranch::fill(FLNodeElabVector* vec) const
{
  mFalseBlock.fill(vec);
}

//! Compose a cycle schedule for a branching sequence
void NUCycleBranch::compose(UtString* buf, UInt32 indent) const
{
  buf->append(indent, ' ');
  *buf << "if (";
  NUNet* net = mNetElab->getNet();
  net->compose(buf, mNetElab->getHier());
  if (mBitIndex != 0 || net->isVectorNet() )
    *buf << "[" << mBitIndex << "]";
  *buf << ")\n";
  buf->append(indent, ' ');
  *buf << "{\n";
  mTrueBlock.compose(buf, indent + 2);
  buf->append(indent, ' ');
  *buf << "} else {\n";
  mFalseBlock.compose(buf, indent + 2);
  buf->append(indent, ' ');
  *buf << "}\n";
}

//! Add the flow nodes in the loop body to the end of the vector
void NUCycleSettleLoop::fill(FLNodeElabVector* vec) const
{
  mBody.fill(vec);
}

//! Compose a cycle schedule for a loop
void NUCycleSettleLoop::compose(UtString* buf, UInt32 indent) const
{
  buf->append(indent, ' ');
  *buf << "loop until settled\n";
  buf->append(indent, ' ');
  *buf << "{\n";
  mBody.compose(buf, indent + 2);
  buf->append(indent, ' ');
  *buf << "}\n";
}

NUCycle::NUCycle(const SourceLocator& loc, UInt32 id)
  : mLoc(loc), mId(id), mScheduled (false), mSchedule()
{
  mInLoop = NULL;
  mBranchCount = 0;
  mFaninCacheIsStale = true;
  mScheduleStack.push(&mSchedule);  
}

//! destructor
NUCycle::~NUCycle()
{
  // nothing to do
}

const char* NUCycle::typeStr() const
{
  return "NUCycle";
}

void NUCycle::dumpCycle(NodeElabSet* covered, Sensitivity sense)
  const
{
  UtIO::cout() << "{" << UtIO::endl;
  for (NUCycle::FlowLoop l = loopFlows(); !l.atEnd(); ++l)
    (*l)->dumpHelper(covered, 0, sense, NULL, false, UtIO::cout());
  UtIO::cout() << "}" << UtIO::endl;
}

//! Schedule a flow-node within the cycle (order is fanin-to-fanout)
void NUCycle::scheduleFlow(FLNodeElab* flow)
{
  FLN_ASSERT(isNodeEncapsulated(flow), flow);
  FLN_ASSERT(!mScheduleStack.empty(), flow);
  NUCycleScheduleBlock* block = mScheduleStack.top();
  block->append(new NUCycleSimple(flow));
#if DEBUG_CYCLE_SCHEDULING
  UtIO::cout() << "  " << flow->getName()->str() << "\n";
#endif
  if (mInLoop)
    mSettledFlowSet.insert(flow);
}

//! Start a branch in the cycle schedule
void NUCycle::scheduleBranch(NUNetElab* netElab, SInt32 bitIndex)
{
  NU_ASSERT(!mScheduleStack.empty(), this);
  NUCycleScheduleBlock* block = mScheduleStack.top();
  NUCycleBranch* branch = new NUCycleBranch(netElab, bitIndex);
  block->append(branch);
  mScheduleStack.push(&(branch->mTrueBlock));
  mScheduleStack.push(&(branch->mFalseBlock));
#if DEBUG_CYCLE_SCHEDULING
  UtIO::cout() << "if (" << mNetElab->getSymNode()->str() << "[" << mBitIndex << "] == 0)\n";
#endif
  ++mBranchCount;

}

//! Start a loop in the cycle schedule
void NUCycle::scheduleLoop()
{
  NU_ASSERT(!mScheduleStack.empty(), this);
  NUCycleScheduleBlock* block = mScheduleStack.top();
  NUCycleSettleLoop* loop = new NUCycleSettleLoop;
  block->append(loop);
  mScheduleStack.push(&(loop->mBody));
  mInLoop = loop;
#if DEBUG_CYCLE_SCHEDULING
  UtIO::cout() << "loop until settled:\n";
#endif
}

//! End a branch or loop segment
void NUCycle::scheduleJoin()
{
  NU_ASSERT(!mScheduleStack.empty(), this);
  mScheduleStack.pop();
#if DEBUG_CYCLE_SCHEDULING
  if (mInLoop)
    UtIO::cout() << "end of loop\n";
  else
    UtIO::cout() << "else\n";
#endif
  mInLoop = NULL;
}

void NUCycle::addCutPoint(FLNodeElab* flow)
{
  FLN_ELAB_ASSERT(mInLoop, flow);
  mInLoop->addCutPoint(flow);  
}

void NUCycle::addFlow(FLNodeElab* flow)
{
  if (mFlowNodeSet.find(flow) != mFlowNodeSet.end())
    return;

  FLN_ASSERT(flow->getCycle() == NULL, flow);
  mFlowNodeSet.insert(flow);
  NUNetElab* defNet = flow->getDefNet();
  mDefNets.insert(defNet);

  // We invalidate the fanin cache instead of updating it, so that
  // we can do a single update after a batch of flow node insertions
  // and delay the computation as long as possible.
  invalidateFaninCache();
} // void NUCycle::addFlow

bool NUCycle::isNodeEncapsulated(FLNodeElab* flow) const
{
  return mFlowNodeSet.find(flow) != mFlowNodeSet.end();
}

/*! Get a vector of FLNodeElabs in the order they would be executed
 *  if no branches are taken.
 */
const FLNodeElabVector* NUCycle::getOrderedFlowVector() const
{
  FLNodeElabVector* vec = new FLNodeElabVector;
  mSchedule.fill(vec);
  return vec;
}

void NUCycle::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtString buf;
  compose(&buf, NULL, numspaces, recurse );
  UtIO::cout() << buf;
}

void NUCycle::compose(UtString* buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  updateFaninCache();
  buf->append(numSpaces, ' ');
  *buf << "{ Cycle #" << mId << ": " << mFlowNodeSet.size()
       << " nodes, " << mFanin.size() << " fanins\n";
  if (recurse)
  {
    buf->append(numSpaces + 2, ' ');
    *buf << "Nodes {\n";
    for (SortedLoop l = loopSorted(); !l.atEnd(); ++l)
    {
      FLNodeElab* flow = *l;
      flow->compose(buf, scope, numSpaces + 4, recurse);
#define SPEW_FANIN 0
#if SPEW_FANIN
      UtIO::cout() << ":";
      for (Fanin f(NULL, flow, eFlowIntoCycles); !f.atEnd(); ++f)
      {
        FLNodeElab* fanin = *f;
        UtIO::cout() << "\n";
        fanin->pname(numSpaces + 8, false);
      }
#endif
      *buf << "\n";
    }
    buf->append(numSpaces + 2, ' ');
    *buf << "}\n";
    buf->append(numSpaces + 2, ' ');
    *buf << "Fanin {\n";
    for (SortedLoop l = loopFaninSorted(); !l.atEnd(); ++l)
    {
      FLNodeElab* flow = *l;
      flow->compose(buf, scope, numSpaces + 4, true);
    }
    buf->append(numSpaces + 2, ' ');
    *buf << "}\n";
    buf->append(numSpaces + 2, ' ');
    *buf << "Schedule {\n";
    mSchedule.compose(buf, numSpaces + 4);
    buf->append(numSpaces + 2, ' ');
    *buf << "}\n";
    if ((mScheduleStack.size() != 1) || (mScheduleStack.top() != &mSchedule))
    {
      buf->append(numSpaces + 2, ' ');
      *buf << "SCHEDULE CONSTRUCTION IS NOT COMPLETE!\n";
    }
  } // if
  buf->append(numSpaces, ' ');
  *buf << "}\n";
} // void NUCycle::compose

bool NUCycle::isFaninNet(NUNetElab* net) const {
  updateFaninCache();
  return mFaninNets.find(net) != mFaninNets.end();
}
  
bool NUCycle::isCycleNet(NUNetElab* net) const {
  return mDefNets.find(net) != mDefNets.end();
}

void NUCycle::updateFaninCache() const
{
  if (!mFaninCacheIsStale)
    return;

  // Clear the existing fanin
  mFaninNets.clear();
  mFanin.clear();

  // Don't add flows that are in the cycle
  FLNodeElabSet cycleFlows;
  for (FlowLoop l = loopFlows(); !l.atEnd(); ++l)
    cycleFlows.insert(*l);

  // Go through the leaf nodes and add the fanin
  for (FlowLoop l = loopFlows(); !l.atEnd(); ++l)
  {
    FLNodeElab* flow = *l;
    addFlowFanin(flow, cycleFlows);
  }

  // The fanin cache is up-to-date again
  mFaninCacheIsStale = false;
}

void NUCycle::addFlowFanin(FLNodeElab* flow, const FLNodeElabSet& cycleFlows) const
{
  for (Fanin f(NULL, flow, eFlowIntoCycles); !f.atEnd(); ++f)
  {
    FLNodeElab* fanin = *f;
    if (cycleFlows.find(fanin) == cycleFlows.end())
    {
      mFaninNets.insert(fanin->getDefNet());
      mFanin.insert(fanin);
    }
  }
}

void NUCycle::putScheduled (bool f)
{
  mScheduled = f;
}

bool NUCycle::isScheduled () const
{
  return mScheduled;
}
