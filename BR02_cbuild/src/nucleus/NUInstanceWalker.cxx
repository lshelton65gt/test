// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "nucleus/NUInstanceWalker.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUTF.h"

#include "symtab/STSymbolTable.h"
/*
NUInstanceCallback::NUInstanceCallback(STSymbolTable* symTab) :
  mSymTab(symTab), mBranchStack(new BranchStack)
{}
*/

NUInstanceCallback::NUInstanceCallback(STSymbolTable* symTab, bool inc_named_decl_scope) :
  mSymTab(symTab), mBranchStack(new BranchStack), 
  mIncludeNamedDeclScope(inc_named_decl_scope)
{}

NUInstanceCallback::~NUInstanceCallback()
{
  ST_ASSERT(mBranchStack->empty(), *(mBranchStack->begin ()));
  delete mBranchStack;
}

NUDesignCallback::Status
NUInstanceCallback::operator()(Phase phase, NUModule * mod)
{
  if (phase == NUDesignCallback::ePre) {
    if (mod->atTopLevel()) {
      // Push this instance on the stack
      NU_ASSERT(mBranchStack->empty(),mod);
      STSymbolTableNode* node = mSymTab->find(NULL, mod->getName());
      NU_ASSERT(node != NULL,mod);
      STBranchNode* branch = node->castBranch();
      NU_ASSERT(branch != NULL,mod);
      mBranchStack->push_back(branch);
    }

    {
      // Call the derived classes function for this module
      NU_ASSERT(not mBranchStack->empty(),mod);
      STBranchNode* branch = mBranchStack->back();
      handleModule(branch, mod);
    }

  } else {
    if (mod->atTopLevel()) {
      mBranchStack->pop_back();
    }
  }
  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status
NUInstanceCallback::operator()(Phase phase, NUNamedDeclarationScope * declScope)
{
  // Later this code should be removed.
  // The handleDeclScope function should be called for any declaration scope.
  // The callbacks should make desision wether to process them.
  bool isNamedBlock = false;
  if(mIncludeNamedDeclScope == true)
  {
    HierFlags hier_flags = declScope->getHierFlags();
    if( (hier_flags & eHierTypeMask) == eHTNamedBlock ||
        (hier_flags & eHierTypeMask) == eHTGenerBlock )
      isNamedBlock = true;
  }

  if (declScope->hasInstances() || isNamedBlock)
  {
    if (phase == NUDesignCallback::ePre) {
      // Push this declaration scope on the stack which may contain instances.
      NU_ASSERT(not mBranchStack->empty(),declScope);
      STBranchNode* parent = mBranchStack->back();

      STSymbolTableNode* node = mSymTab->find(parent, declScope->getName());
      NU_ASSERT(node != NULL, declScope);

      STBranchNode* branch = node->castBranch();
      NU_ASSERT(branch != NULL, declScope);
      mBranchStack->push_back(branch);

      // Call the derived classes function for this declaration scope since it
      // has instances that derived class may want to handle.
      handleDeclScope(branch, declScope);
    } else {
      mBranchStack->pop_back();
    }
  }
  return NUDesignCallback::eNormal;
}

NUDesignCallback::Status
NUInstanceCallback::operator()(Phase phase, NUModuleInstance* inst)
{
  if (phase == NUDesignCallback::ePre) {
    // Push this instance on the stack
    STBranchNode * parentNode = NULL;
    if (not mBranchStack->empty()) {
      parentNode = mBranchStack->back();
    }
    STSymbolTableNode* node = mSymTab->find(parentNode, inst->getName());
    STBranchNode* branch = node->castBranch();
    NU_ASSERT(branch != NULL,inst);
    mBranchStack->push_back(branch);

    // Call the derived classes function for this module instance
    handleInstance(branch, inst);

  } else {
    mBranchStack->pop_back();
  }
  return NUDesignCallback::eNormal;
}

STBranchNode*
NUInstanceCallback::getCurrentNode() const
{
  STBranchNode * branch = NULL;
  if (!mBranchStack->empty())
    branch = mBranchStack->back();
  return branch;
}
