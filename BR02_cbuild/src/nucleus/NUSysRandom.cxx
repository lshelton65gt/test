// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUSysRandom.h"
#include "util/StringAtom.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"

NUSysRandom::NUSysRandom(Function func,
                         StringAtom* name,
                         NULvalue* outLvalue,
                         NULvalue* seedLvalue,
                         NUExprVector& exprs,
                         const NUModule* module,
                         NUNetRefFactory* netRefFactory,
                         bool usesCFNet,
                         const SourceLocator& loc)
  : NUSysTask(name, exprs, module, netRefFactory, usesCFNet, true, loc),
    mFunction(func)
{
  NU_ASSERT (func != eInvalidRandom, this);
  mOutLvalue = outLvalue;

  {
    // what should be done here is that if the size of any argument is
    // not 32 bits then create a new 32 bit net and make the
    // appropriate assignemnt from the original net to the temporary,
    // and use the temp as the arg to the random function.  Also need
    // to create a contassign from the temp for the seed back to the
    // seed variable (arg 0).  For now we will do nothing.
    // Later during population an error will be reported if the arg was not 32 bits.
  }

  // The seed lvalue may be NULL since during construction it should
  // be the same as the seed rvalue.  We split seed from being an
  // inout to an lvalue and rvalue. That way we can change the rvalue
  // to something else without affecting the lvalue (see bug3333 for
  // details).
  mSeedLvalue = seedLvalue;
  if (!mValueExprVector.empty()) {
    // Make sure the first argument is a net. if it isn't the populate
    // code will cause a failure after we create this structure. So we
    // can't assert here.
    NUExpr* arg1 = mValueExprVector[0];
    if (mSeedLvalue == NULL) {
      if (arg1->isWholeIdentifier()) {
        NUNet* net = arg1->getWholeIdentifier();
        mSeedLvalue = new NUIdentLvalue(net, loc);
      }
    }
  }
}

NUSysRandom::~NUSysRandom()
{
  delete mOutLvalue;
  delete mSeedLvalue;
}

NUNet* NUSysRandom::getSeedNet() const
{
  if (mSeedLvalue != NULL) {
    return mSeedLvalue->getWholeIdentifier();
  } else {
    return NULL;
  }
}

//! Function to return the expression type
NUType NUSysRandom::getType() const
{
  return eNUSysRandom;
}

bool NUSysRandom::operator==(const NUStmt & otherStmt) const
{
  // first compare stuff unique to NUSysRandom 
  const NUSysRandom* other = dynamic_cast<const NUSysRandom*>(&otherStmt);
  if (not other)
    return false;
  if (other->mFunction != mFunction)
    return false;

  // now compare the information common to NUSysTask
  if ( not NUSysTask::operator==(otherStmt) )
    return false;

  return true;
}

//! Return class name
const char* NUSysRandom::typeStr() const
{
  return "NUSysRandom";
}

/*
//! hash an expression
size_t NUSysRandom::hash() const
{
  size_t h = (size_t) mFunction + (size_t) getOutLvalue() + (size_t)getSeedNet();
  for (size_t i = 0; i < mArgs.size(); ++i)
  {
    h = 17*h + mArgs[i]->hash();
  }
  return h;
}
*/

//! Return a copy of this expression tree.
NUStmt* NUSysRandom::copy(CopyContext &copy_context) const
{
  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);
  // rjc-todo should the outNet be copied with context?
  NULvalue* lvalue = getOutLvalue()->copy(copy_context);
  NULvalue* seedLvalue = NULL;
  if (getSeedLvalue() != NULL) {
    seedLvalue = getSeedLvalue()->copy(copy_context);
  }
  NUSysRandom *rand = new NUSysRandom(mFunction, getName(), lvalue, seedLvalue,
                                      cpyVector, getModule(), mNetRefFactory,
                                      getUsesCFNet(), getLoc());
  return rand;
}

bool NUSysRandom::lookup(const char* name, Function* func)
{
  bool ret = true;
  if (strcmp(name, "$random") == 0)
    *func = eRandom;
  else if (strcmp(name, "$dist_uniform") == 0)
    *func = eDistUniform;
  else if (strcmp(name, "$dist_normal") == 0)
    *func = eDistNormal;
  else if (strcmp(name, "$dist_exponential") == 0)
    *func = eDistExponential;
  else if (strcmp(name, "$dist_poisson") == 0)
    *func = eDistPoisson;
  else if (strcmp(name, "$dist_chi_square") == 0)
    *func = eDistChiSquare;
  else if (strcmp(name, "$dist_t") == 0)
    *func = eDistT;
  else if (strcmp(name, "$dist_erlang") == 0)
    *func = eDistErlang;
  else
    ret = false;
  return ret;
} // bool NUSysRandom::lookup

//! Validate legal arguments for the system call
bool NUSysRandom::isValid(UtString* msg)
{
  // Validate the right number of arguments
  bool ret = true;
  SInt32 args = -1;
  bool allowNoArgs = false;
  switch (mFunction)
  {
  case eRandom:          args = 1; allowNoArgs = true;   break;
  case eDistUniform:     args = 3; break;
  case eDistNormal:      args = 3; break;
  case eDistExponential: args = 2; break;
  case eDistPoisson:     args = 2; break;
  case eDistChiSquare:   args = 2; break;
  case eDistT:           args = 2; break;
  case eDistErlang:      args = 3; break;
  default: NU_ASSERT(0=="Wrong number of parameters", this);
  }

  if (allowNoArgs && mValueExprVector.empty())
    ;  // no args OK for $random
  else if (((SInt32) mValueExprVector.size()) != args)
  {
    *msg << mValueExprVector.size() << " arguments supplied, " << args << " are required";
    ret = false;
  }

  // If there is a first argument, it must be a whole 32-bit
  // number
  if (!mValueExprVector.empty())
  {
    // The first arg has to be an integer or 32-bit reg because we are
    // going to write the seed back to it, and will do a reinterpret_cast
    // at runtime
    NUExpr* seedVar = mValueExprVector[0];
    if (!seedVar->isWholeIdentifier())
    {
      if (!ret)
        *msg << "\n";
      *msg << "First argument must be a variable to hold the seed, found ";
      seedVar->compose(msg, NULL);
      ret = false;
    }
    else
    {
      NUNet* net = seedVar->getWholeIdentifier();
      if (net->getBitSize() != 32)
      {
        if (!ret)
          *msg << "\n";
        *msg << "Seed variable must have width 32, variable ";
        net->compose(msg, NULL);
        *msg << " has width " << net->getBitSize();
        ret = false;
      }
    }
  } // if
  return ret;
} // bool NUSysRandom::isValid

void NUSysRandom::getBlockingDefs(NUNetSet *defs) const
{
  getOutLvalue()->getDefs(defs);
  if (getSeedLvalue() != NULL) {
    defs->insert(getSeedNet());
  }
}

//! $random and $dist* modify their first arg (seed)
void NUSysRandom::getBlockingDefs(NUNetRefSet *defs) const
{
  getOutLvalue()->getDefs(defs);
  if (getSeedLvalue() != NULL) {
    NUNetRefHdl net_ref = defs->getFactory()->createNetRef(getSeedNet());
    defs->insert(net_ref);
  }
}

//! $random and $dist* modify their first arg (seed)
bool NUSysRandom::queryBlockingDefs(const NUNetRefHdl &def_net_ref,
                                    NUNetRefCompareFunction fn,
                                    NUNetRefFactory *factory) const
{
  bool ret = false;

  if (getSeedLvalue() != NULL) {
    NUNetRefHdl net_ref = factory->createNetRef(getSeedNet());
    ret = ((*net_ref).*fn)(*def_net_ref);
  }
  ret |= getOutLvalue()->queryDefs(def_net_ref, fn, factory);
  return ret;
}

void NUSysRandom::replaceDef(NUNet *old_net, NUNet *new_net)
{
  if (old_net == new_net) {
    // The new net is the same as the old net, nothing to replace
    return;
  }

  if (old_net == getSeedNet())
  {
    NU_ASSERT(new_net->getBitSize() == 32, this);
    delete mSeedLvalue;
    mSeedLvalue = new NUIdentLvalue(new_net, new_net->getLoc());
  }
  getOutLvalue()->replaceDef(old_net, new_net);
} // void NUSysRandom::replaceDef

bool NUSysRandom::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  // first apply replaceLeaves from the baseclass
  changed |= NUSysTask::replaceLeaves(translator);

  // now do replaceLeaves for member variables specific to NUSysRandom
  NULvalue* repl = translator(getOutLvalue(), NuToNuFn::ePre);
  if (repl) {
    mOutLvalue = repl;
    changed = true;
  }
  if (getSeedLvalue() != NULL) {
    repl = translator(getSeedLvalue(), NuToNuFn::ePre);
    if (repl) {
      mSeedLvalue = repl;
      NU_ASSERT(mSeedLvalue, this);
      changed = true;
    }
  }
  changed |= getOutLvalue()->replaceLeaves(translator);
  if (getSeedLvalue() != NULL)
    changed |= getSeedLvalue()->replaceLeaves(translator);
  repl = translator(getOutLvalue(), NuToNuFn::ePost);
  if (repl) {
    mOutLvalue = repl;
    changed = true;
  }
  if (getSeedLvalue() != NULL) {
    repl = translator(getSeedLvalue(), NuToNuFn::ePost);
    if (repl) {
      mSeedLvalue = repl;
      NU_ASSERT(mSeedLvalue, this);
      changed = true;
    }
  }
  
  return changed;
}
bool NUSysRandom::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed = getOutLvalue()->replace(old_net, new_net);
  if (old_net == getSeedNet())
  {
    NU_ASSERT(new_net->getBitSize() == 32, this);
    delete mSeedLvalue;
    mSeedLvalue = new NUIdentLvalue(new_net, new_net->getLoc());
    changed = true;
  }
  for (NUExprVectorIter iter = mValueExprVector.begin();
       iter != mValueExprVector.end();
       ++iter) {
    changed |= (*iter)->replace(old_net,new_net);
  }
  return changed;
}


bool NUSysRandom::isDefNet(NUNet* net)
  const
{
  bool ret = false;
  NUNetSet defs;
  getOutLvalue()->getDefs(&defs);
  if (defs.find(net) != defs.end())
    ret = true;
  else if (net == getSeedNet())
    ret = true;
  return ret;
}

  

void NUSysRandom::getBlockingKills(NUNetSet *kills) const
{
  getBlockingDefs(kills);
}
void NUSysRandom::getBlockingKills(NUNetRefSet *kills) const
{
  getBlockingDefs(kills);
}
bool NUSysRandom::queryBlockingKills(const NUNetRefHdl &def_net_ref,
                                     NUNetRefCompareFunction fn,
                                     NUNetRefFactory *factory) const
{
  return queryBlockingDefs(def_net_ref, fn, factory);
}


static const char* sFuncToStr(NUSysRandom::Function fn)
{
  switch (fn)
  {
  case NUSysRandom::eRandom:          return "$random";
  case NUSysRandom::eDistUniform:     return "$dist_uniform";
  case NUSysRandom::eDistNormal:      return "$dist_normal";
  case NUSysRandom::eDistExponential: return "$dist_exponential";
  case NUSysRandom::eDistPoisson:     return "$dist_poission";
  case NUSysRandom::eDistChiSquare:   return "$dist_chi_square";
  case NUSysRandom::eDistT:           return "$dist_t";
  case NUSysRandom::eDistErlang:      return "$dist_erlang";
  case NUSysRandom::eInvalidRandom:   return "$not_a_random_fctn";
  }
  INFO_ASSERT(0,"Unknown system function");
  return 0;
}

void NUSysRandom::print(bool recurse, int numSpaces) const
{
  indent(numSpaces);
  mLoc.print();
  UtIO::cout() << "NUSysRandom(" << this << ") : " << sFuncToStr(mFunction)
               << UtIO::endl;
  if (recurse)
  {
    getOutLvalue()->print(true, numSpaces + 2);
    if (mSeedLvalue != NULL) {
      mSeedLvalue->print(true, numSpaces + 2);
    }
    for (NUExprVector::const_iterator iter = mValueExprVector.begin();
         iter != mValueExprVector.end();
         ++iter)
    {
      NUExpr* arg = *iter;
      arg->print(true, numSpaces + 2);
    }
  }
}

void NUSysRandom::compose(UtString* buf,
                          const STBranchNode* scope,
                          int indent,
                          bool recurse)
  const
{
  for (int i = 0; i < indent; ++i)
    *buf << ' ';
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  getOutLvalue()->compose(buf, scope, indent, recurse);
  *buf << " = " << sFuncToStr(mFunction);
  if (getSeedLvalue() != NULL) {
    *buf << "(";
    getSeedNet()->compose(buf, NULL);
    for (size_t i = 0; i < mValueExprVector.size(); ++i)
    {
      *buf << ",";
      mValueExprVector[i]->composeSized(buf, scope, eCarbonHex); 
    }
    *buf << ")";
  }
  *buf << ";\n";
} // void NUSysRandom::compose


NULvalue* NUSysRandom::getOutLvalue()
  const
{
  return mOutLvalue;
}

NULvalue* NUSysRandom::getSeedLvalue()
  const
{
  return mSeedLvalue;
}
