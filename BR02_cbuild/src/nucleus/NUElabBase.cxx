// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUElabBase.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "symtab/STSymbolTable.h"
#include "symtab/STSymbolTableNode.h"
#include "util/UtIOStream.h"

NUElabBase::NUElabBase(STSymbolTableNode* symNode): mSymNode(symNode)
{
}

NUElabBase::~NUElabBase()
{
  if (mSymNode != NULL)
  {
    CbuildSymTabBOM::putNucleusObj(mSymNode, NULL);

    // NULL out all the alias pointers to this node.
    STAliasedLeafNode* alias = mSymNode->castLeaf();
    if (alias != NULL) {
      while ((alias = alias->getAlias()) != mSymNode) {
        CbuildSymTabBOM::putNucleusObj(alias, NULL);
      }
    }
  }
}

void NUElabBase::pname() const
{
  UtString buf;
  compose(&buf, NULL);  // we do not need to include root in name
  UtIO::cout() << buf << UtIO::endl;
}


void NUElabBase::compose(UtString* buf,
                         const STBranchNode* /* scope */,
                         const bool includeRoot,
                         const bool hierName,
                         const char* separator) const
{
  mSymNode->compose(buf, includeRoot, hierName, separator );
}


int NUElabBase::compare(const NUElabBase* n1, const NUElabBase* n2)
{
  int cmp = HierName::compare(n1->mSymNode, n2->mSymNode);
  /*
  if (cmp == 0)
    cmp = strcmp(n1->mNet->getName()->str(),
    n2->mNet->getName()->str());
  */
  return cmp;
}

NUElabBase* NUElabBase::find(const STSymbolTableNode* node)
{
  const STSymbolTableNode* master = node;
  const STAliasedLeafNode* aliased = node->castLeaf();
  if (aliased != NULL)
  {
    master = aliased->getMaster();
    ST_ASSERT(master, aliased);
  }
  
  NUBase* base = CbuildSymTabBOM::getNucleusObj(master);
  return dynamic_cast<NUElabBase*>(base);
}


void NUElabBase::deleteObjects(STSymbolTable* symtab)
{
  for (STSymbolTable::NodeLoop i = symtab->getNodeLoop(); !i.atEnd(); ++i)
  {
    STSymbolTableNode* node = *i;
    NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
    if (base != NULL)
    {
      NUElabBase* elabBase = dynamic_cast<NUElabBase*>(base);
      if (elabBase != NULL)
        delete elabBase;
    }
  }
}

void NUElabBase::printAssertInfoHelper() const {
  pname();
  print(true, 0);
}
