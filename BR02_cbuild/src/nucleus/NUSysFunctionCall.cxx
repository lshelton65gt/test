// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUSysFunctionCall.h"
#include "util/StringAtom.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUNetSet.h"
#include "bdd/BDD.h"
#include "NUExprI.h"

NUSysFunctionCall::NUSysFunctionCall(NUOp::OpT op,
                                     SInt8 timeUnit,
                                     SInt8 timePrecision,
                                     const NUExprVector& exprs,
                                     const SourceLocator& loc)
  : NUNaryOp(op, exprs, loc),
    mTimeUnit( timeUnit ),
    mTimePrecision( timePrecision )
{
}

NUSysFunctionCall::~NUSysFunctionCall()
{
}

//! Function to return the expression type
NUExpr::Type NUSysFunctionCall::getType() const
{
  return eNUSysFunctionCall;
}

//! Return the size this expression "naturally" is.
UInt32 NUSysFunctionCall::determineBitSizeHelper() const
{
  switch ( mOp )
  {
  case eUnItoR:
    return 64;
    break;
  case eUnRtoI:
    return 32;
    break;
  case eUnRealtoBits:
  case eUnBitstoReal:
    return 64;
    break;
  case eZSysTime:
  case eZSysRealTime:
    return 64; // $time returns a 64-bit value
    break;
  case eZEndFile:
    return 1;  // returns a boolean
    break;
  default:
    return 32; // Everything else is 32 bits
    break;
  }
}

bool NUSysFunctionCall::isReal() const
{
  return ( mOp == eZSysRealTime );
}

//! return true if both are NUNullExpr since any two are equivalent.
bool NUSysFunctionCall::operator==(const NUExpr & otherExpr) const
{
  const NUSysFunctionCall* other =
    dynamic_cast<const NUSysFunctionCall*>(&otherExpr);
  if (not other)
    return false;

  if ((mOp != other->getOp()) ||
      (mTimeUnit != other->mTimeUnit) ||
      (mTimePrecision != other->mTimePrecision))
  {
    return false;
  }

  if (not ObjListEqual<NUExprCLoop>(loopExprs (), other->loopExprs ()))
    return false;

  return true;
}

//! shallow equality comparison -- used for NUExprFactory
bool NUSysFunctionCall::shallowEqHelper(const NUExpr & otherExpr) const
{
  const NUSysFunctionCall* sfc =
    dynamic_cast<const NUSysFunctionCall*>(&otherExpr);

  if ((sfc == NULL) ||
      (mTimeUnit != sfc->mTimeUnit) ||
      (mTimePrecision != sfc->mTimePrecision))
  {
    return false;
  }
  return NUOp::shallowEqHelper(otherExpr);
}

//! Generate a BDD for this expression (TBD)
/*!
  \param ctx The context which is managing the BDD's
  \param scope If non-0, this expression will be elaborated to the given scope.
  \return BDD representation of this expression
 */
BDD NUSysFunctionCall::bdd(BDDContext * ctx, STBranchNode *) const
{
  return ctx->invalid();
}

//! Return class name
const char* NUSysFunctionCall::typeStr() const
{
  return "NUSysFunctionCall";
}

//! Return a copy of this expression tree.
NUExpr* NUSysFunctionCall::copyHelper(CopyContext &copy_context) const
{
  NUExprVector new_vector(mExprs.size());
  int i = 0;
  for (NUExprVectorIter iter = mExprs.begin();
       iter != mExprs.end();
       ++iter, ++i) {
    new_vector[i] = (*iter)->copy(copy_context);
  }
  NUExpr *expr = new NUSysFunctionCall(mOp, mTimeUnit, mTimePrecision,
                                       new_vector, mLoc);
  return expr;
}

void NUSysFunctionCall::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  mBitSize = size;

  // a zero value for op_size and operandsSelfDetermined==FALSE,
  // indicates that the operation for this function call was not
  // properly considered for setting the operand sizes.
  SInt32 op_size = 0;  
  bool operandsSelfDetermined = false;

  switch ( mOp )
  {
  case eUnItoR:       { op_size = 32; break; }
  case eUnRtoI:       { op_size = 64; break; }
  case eUnRealtoBits: { op_size = 64; break; }
  case eUnBitstoReal: { op_size = 64; break; }

  case eZSysTime:
  case eZSysStime:
  case eZSysRealTime:
  case eZEndFile:     { op_size = 32; break; }

  case eNaFopen: { operandsSelfDetermined = true; break; }

    // using a default here is to silence gcc checking that all enumeration
    // values are considered, the code below will assert if this default
    // is taken
  default:       { break; }
  }

  if ( not operandsSelfDetermined ) {
    NU_ASSERT(op_size, this);
    for (size_t i = 0; i < mExprs.size(); ++i) {
      mExprs[i] = mExprs[i]->resizeSubHelper(op_size);
    }
  }
}

NUOp::OpT NUSysFunctionCall::lookupOp(const char* name, bool * is_converter_function)
{
  *is_converter_function = false;
  // if you add a new function here you should also update the case
  // stmt in NUSysFunctionCall::resizeHelper
  if (strcmp(name, "$rtoi") == 0) {
    *is_converter_function = true;
    return NUOp::eUnRtoI;
  }
  if (strcmp(name, "$itor") == 0) {
    *is_converter_function = true;
    return NUOp::eUnItoR;
  }
  if (strcmp(name, "$realtobits") == 0) {
    *is_converter_function = true;
    return NUOp::eUnRealtoBits;
  }
  if (strcmp(name, "$bitstoreal") == 0) {
    *is_converter_function = true;
    return NUOp::eUnBitstoReal;
  }
  if (strcmp(name, "$time") == 0) {
    return NUOp::eZSysTime;
  }
  if (strcmp(name, "$stime") == 0) {
    return NUOp::eZSysStime;
  }
  if (strcmp(name, "$realtime") == 0) {
    return NUOp::eZSysRealTime;
  }
  if (strcmp(name, "$fopen") == 0) {
    return NUOp::eNaFopen;
  }
  if (strcmp(name, "ENDFILE") == 0) {
    return NUOp::eZEndFile;
  }

  return NUOp::eInvalid;
}

//! Validate legal arguments for the system call
bool NUSysFunctionCall::isValid(UtString* msg) const {
  // check that we have the right number of arguments
  bool ret = true;
  SInt32 minNumArgs =  0;
  SInt32 maxNumArgs = -1;
  switch (mOp)
  {
  case NUOp::eUnItoR:
  case NUOp::eUnRtoI:
  case NUOp::eUnRealtoBits:
  case NUOp::eUnBitstoReal:
  {
    minNumArgs = 1;
    maxNumArgs = 1;
    break;
  }
  case NUOp::eZSysTime:
  case NUOp::eZSysStime:
  case NUOp::eZSysRealTime:
  {
    minNumArgs = 0;
    maxNumArgs = 0;
    break;
  }
  default:
    NU_ASSERT("Wrong number of parameters in syscall" == NULL, this);
  }

  SInt32 availableNumArgs = ((SInt32) getNumArgs());
  if ( ( availableNumArgs < minNumArgs ) or ( maxNumArgs < availableNumArgs ))
  {
    if ( minNumArgs == maxNumArgs )
    {
      *msg << availableNumArgs << " arguments supplied, " << minNumArgs << " are required";
    }
    else
    {
      *msg << availableNumArgs << " arguments supplied, a number between " << minNumArgs << " and " << maxNumArgs << " (inclusive) are required";
    }
    ret = false;
  }

  return ret;
}
