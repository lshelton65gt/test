// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "nucleus/Nucleus.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUCModelArgConnection.h"
#include "util/UtString.h"

class STBranchNode;


NUCModel::NUCModel(NUCModelInterface* cmodelInterface, 
                   NUModule * parent_module,
                   UInt32 variant,
		   UtString* origName) :
  mCModelInterface(cmodelInterface), 
  mModule(parent_module),
  mVariant(variant),
  mName(cmodelInterface->makeName()),
  mParams(new Params)
{
  if (origName != NULL)
    mOrigName = new UtString(*origName);
  else
    mOrigName = NULL;
}

NUCModel::~NUCModel()
{
  delete mName;
  for (Params::iterator p = mParams->begin(); p != mParams->end(); ++p)
    delete *p;
  delete mParams;
  if (mOrigName != NULL)
    delete mOrigName;
}


void NUCModel::addParam(Param& param)
{
  // Copy over the data and insert it on the back. We don't expect
  // repeats so we use a vector.
  Param* newParam = new Param;
  newParam->first = param.first;
  newParam->second = param.second;
  mParams->push_back(newParam);
}

NUCModel::ParamLoop NUCModel::loopParams() const
{
  return ParamLoop(*mParams);
}

UInt32 NUCModel::getNumParams() const
{
  return mParams->size();
}

void NUCModel::compose(UtString* buf, const STBranchNode* /* scope */, int indent, bool /*recurse*/) const
{
  buf->append(indent, ' ');
  *buf << "CModel print routine should go here!!!\n";
}

