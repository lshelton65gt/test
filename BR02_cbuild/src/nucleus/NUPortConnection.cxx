// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUNetRef.h"
#include "nucleus/NUScope.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"
#include "util/UtIndent.h"
#include "util/CbuildMsgContext.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNetRefSet.h"

/*!
  \file
  Implementation of NUPortConnection and derived classes.
*/

NUPortConnection::NUPortConnection(NUNet* formal, const SourceLocator& loc) :
  mLoc(loc), mInst(0), mFormal(formal)
{}

NUPortConnection::~NUPortConnection()
{
}


NUModuleInstance* NUPortConnection::getModuleInstance() const
{
  NU_ASSERT(mInst, this);
  return mInst;
}


void NUPortConnection::setModuleInstance(NUModuleInstance *inst)
{
  NU_ASSERT(mInst == 0, this);
  mInst = inst;
}


NUPortConnectionInput::NUPortConnectionInput(NUExpr *actual,
					     NUNet *formal,
					     const SourceLocator& loc) :
  NUPortConnection(formal, loc),
  mActual(actual)
{}


NUPortConnectionInput::~NUPortConnectionInput()
{
  delete mActual;
}


static bool sSimpleExpression(const NUExpr * expr) 
{
  NUExpr::Type rvalue_type = expr->getType();
  if (rvalue_type==NUExpr::eNUIdentRvalue or 
      rvalue_type==NUExpr::eNUVarselRvalue) {
    return true;
  } else if (rvalue_type==NUExpr::eNUConcatOp) {
    const NUConcatOp * concat = dynamic_cast<const NUConcatOp*>(expr);
    bool simple = concat->getRepeatCount()==1;
    for (NUExprCLoop loop = concat->loopExprs();
         simple and not loop.atEnd();
         ++loop) {
      simple = sSimpleExpression(*loop);
    }
    return simple;
  } else {
    return false;
  }
}


bool NUPortConnectionInput::isSimple() const 
{
  const NUExpr * rvalue = getActual();
  if (rvalue)
    return sSimpleExpression(rvalue);
  else 
    return false;
}

NUNet* NUPortConnectionInput::getActualNet() const 
{
  const NUExpr* rvalue = getActual();
  NU_ASSERT(rvalue, this);
  NU_ASSERT(rvalue->isWholeIdentifier(), this);
  return rvalue->getWholeIdentifier();
}

void NUPortConnectionInput::getActuals(NUNetSet* actuals) const
{
  getUses(actuals);
}

void NUPortConnectionInput::getUses(NUNetSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(uses);
  }
}


void NUPortConnectionInput::getUses(NUNetRefSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(uses);
  }
}


bool NUPortConnectionInput::queryUses(const NUNetRefHdl &use_net_ref,
				      NUNetRefCompareFunction fn,
				      NUNetRefFactory *factory) const
{
  if (mActual != 0) {
    return mActual->queryUses(use_net_ref, fn, factory);
  }
  return false;
}

void NUPortConnectionInput::getUses(const NUNetRefHdl & /* net_ref -- unused */,
				    NUNetRefSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(uses);
  }
}


bool NUPortConnectionInput::queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
				      const NUNetRefHdl &use_net_ref,
				      NUNetRefCompareFunction fn,
				      NUNetRefFactory *factory) const
{
  if (mActual != 0) {
    return mActual->queryUses(use_net_ref, fn, factory);
  }
  return false;
}


void NUPortConnectionInput::getDefs(NUNetSet *defs) const
{
  defs->insert(mFormal);
}


void NUPortConnectionInput::getDefs(NUNetRefSet *defs) const
{
  defs->insert(defs->getFactory()->createNetRef(mFormal));
}


bool NUPortConnectionInput::queryDefs(const NUNetRefHdl &def_net_ref,
				      NUNetRefCompareFunction fn,
				      NUNetRefFactory *factory) const
{
  NUNetRefHdl net_ref = factory->createNetRef(mFormal);
  return ((*net_ref).*fn)(*def_net_ref);
}


void NUPortConnectionInput::replaceDef(NUNet *old_net, NUNet* new_net)
{
  NU_ASSERT(mFormal == old_net, old_net);
  mFormal = new_net;
}

bool NUPortConnectionInput::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  if ( mFormal == old_net ) {
    mFormal = new_net;
    changed = true;
  }
  if (mActual) {
    changed |= mActual->replace(old_net,new_net);
  }
  return changed;
}

bool NUPortConnectionInput::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePre);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if (mActual) {
    NUExpr * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }

  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePost);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if (mActual) {
    NUExpr * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  return changed;
}

void NUPortConnectionInput::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "PortConnectionInput(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal << ")  actual(" << mActual << ")" << UtIO::endl;

  mFormal->print(false, numspaces+2);
  if (mActual != 0) {
    mActual->print(recurse, numspaces+2);
  }
}

bool NUPortConnectionInput::isAliasable() const
{
  NUExpr * actual = getActual();
  NUNet  * formal = getFormal();

  if ( (not actual) or
       (not actual->isWholeIdentifier()) or
       (actual->getBitSize() != formal->getBitSize()) ) {
    return false;
  } else {
    if (actual->isWholeIdentifier() and 
        (actual->getWholeIdentifier()->is2DAnything() xor formal->is2DAnything()))
    {
      // Either both or neither of the formal & actual must be memories.
      // See bug4721.
      return false;
    }
    else
    {
      return true;
    }
  }
}

void NUPortConnection::compose(UtString *buf, const STBranchNode* scope, int indent, bool) const
{
  UtIndent indenter (buf);
  buf->append (indent, ' ');
  if (mLoc.isTicProtected ()) {
    composeProtectedNUObject(mLoc, buf);
    return;
  }
  NUNet *formal = getFormal ();
  switch (getDir ()) {
  case eInput:
    {
      *buf << ".";
      formal->compose (buf, scope);
      *buf << " (";
      NUExpr *actual = dynamic_cast <const NUPortConnectionInput *> (this)->getActual ();
      if (actual != NULL) {
        actual->compose (buf, scope);
      }
      *buf << ")";
      indenter.tabToLocColumn ();
      *buf << " // (input) ";
      getLoc ().compose (buf);
    }
    break;
  case eOutput:
    {
      *buf << ".";
      formal->compose (buf, scope);
      *buf << " (";
      NULvalue *actual = dynamic_cast <const NUPortConnectionOutput *> (this)->getActual ();
      if (actual != NULL) {
        actual->compose (buf, scope, 0, true);
      }
      *buf << ")";
      indenter.tabToLocColumn ();
      *buf << " // (output) ";
      getLoc ().compose (buf);
    }
    break;
  case eBid:
    {
      *buf << ".";
      formal->compose (buf, scope);
      *buf << " (";
      NULvalue *actual = dynamic_cast <const NUPortConnectionBid *> (this)->getActual ();
      if (actual != NULL) {
        actual->compose (buf, scope, 0, true);
      }
      *buf << ")";
      indenter.tabToLocColumn ();
      *buf << " // (bidirectional) ";
      getLoc ().compose (buf);
    }
    break;
  default:
    NU_ASSERT (false, this);
    break;
  }
}

const char* NUPortConnectionInput::typeStr() const
{
  return "NUPortConnectionInput";
}


NUPortConnection* NUPortConnectionInput::copy(CopyContext & copy_context) const
{
  NUNet  * formal = copy_context.lookup(mFormal);
  NUExpr * actual = NULL;
  if (mActual) {
    actual = mActual->copy(copy_context);
  }
  return new NUPortConnectionInput(actual,
				   formal,
				   mLoc);
}

NUPortConnectionOutput::NUPortConnectionOutput(NULvalue *actual,
					       NUNet *formal,
					       const SourceLocator& loc) :
  NUPortConnection(formal, loc),
  mActual(actual)
{}


NUPortConnectionOutput::~NUPortConnectionOutput()
{
  delete mActual;
}

NUNet* NUPortConnectionOutput::getActualNet() const 
{
  const NULvalue* lvalue = getActual();
  NU_ASSERT(lvalue, this);
  NU_ASSERT(lvalue->isWholeIdentifier(), this);
  return lvalue->getWholeIdentifier();
}

void NUPortConnectionOutput::getActuals(NUNetSet* actuals) const
{
  getDefs(actuals);
}


NUNet* NUPortConnectionBid::getActualNet() const 
{
  const NULvalue* lvalue = getActual();
  NU_ASSERT(lvalue, this);
  NU_ASSERT(lvalue->isWholeIdentifier(), this);
  return lvalue->getWholeIdentifier();
}

void NUPortConnectionBid::getActuals(NUNetSet* actuals) const
{
  getDefs(actuals);
}


void NUPortConnectionOutput::getUses(NUNetSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(uses);
  }
  uses->insert(mFormal);
}


void NUPortConnectionOutput::getUses(NUNetRefSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(uses);
  }
  uses->insert(uses->getFactory()->createNetRef(mFormal));
}


bool NUPortConnectionOutput::queryUses(const NUNetRefHdl &use_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory *factory) const
{
  if (mActual != 0) {
    if (mActual->queryUses(use_net_ref, fn, factory)) {
      return true;
    }
  }
  NUNetRefHdl net_ref = factory->createNetRef(mFormal);
  return ((*net_ref).*fn)(*use_net_ref);
}

void NUPortConnectionOutput::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(net_ref, uses);
  }
  uses->insert(uses->getFactory()->createNetRef(mFormal));
}


bool NUPortConnectionOutput::queryUses(const NUNetRefHdl &def_net_ref,
				       const NUNetRefHdl &use_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory *factory) const
{
  if (mActual != 0) {
    if (mActual->queryUses(def_net_ref, use_net_ref, fn, factory)) {
      return true;
    }
  }
  NUNetRefHdl net_ref = factory->createNetRef(mFormal);
  return ((*net_ref).*fn)(*use_net_ref);
}


void NUPortConnectionOutput::getDefs(NUNetSet *defs) const
{
  if (mActual != 0) {
    mActual->getDefs(defs);
  }
}


void NUPortConnectionOutput::getDefs(NUNetRefSet *defs) const
{
  if (mActual != 0) {
    mActual->getDefs(defs);
  }
}


bool NUPortConnectionOutput::queryDefs(const NUNetRefHdl &def_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory *factory) const
{
  if (mActual != 0) {
    return mActual->queryDefs(def_net_ref, fn, factory);
  }
  return false;
}


void NUPortConnectionOutput::replaceDef(NUNet *old_net, NUNet* new_net)
{
  if (mActual != 0) {
    mActual->replaceDef(old_net, new_net);
  }
}

bool NUPortConnectionOutput::replace(NUNet *old_net, NUNet *new_net)
{
  bool changed = false;
  if ( mFormal == old_net ) {
    mFormal = new_net;
    changed = true;
  }
  if (mActual) {
    changed |= mActual->replace(old_net,new_net);
  }
  return changed;
}

bool NUPortConnectionOutput::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePre);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if (mActual) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePost);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if (mActual) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  return changed;
}

void NUPortConnectionOutput::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "PortConnectionOutput(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal << ")  actual(" << mActual << ")" << UtIO::endl;

  mFormal->print(false, numspaces+2);
  if (mActual != 0) {
    mActual->print(recurse, numspaces+2);
  }
}


const char* NUPortConnectionOutput::typeStr() const
{
  return "NUPortConnectionOutput";
}

NUPortConnection* NUPortConnectionOutput::copy(CopyContext & copy_context) const
{
  NUNet    * formal = copy_context.lookup(mFormal);
  NULvalue * actual = NULL;
  if (mActual) {
    actual = mActual->copy(copy_context);
  }
  return new NUPortConnectionOutput(actual,
				    formal,
				    mLoc);
}

bool NUPortConnectionOutput::isAliasable() const
{
  NULvalue * actual = getActual();
  NUNet    * formal = getFormal();

  // cannot alias disconnected ports or in situations where full-net
  // aliasing is impossible.
  if ( (not actual) or
       (not actual->isWholeIdentifier()) or
       (actual->getBitSize() != formal->getBitSize()) ) {
    return false;
  } else {
    if (actual->isWholeIdentifier() and 
        (actual->getWholeIdentifier()->is2DAnything() xor formal->is2DAnything()))
    {
      // Either both or neither of the formal & actual must be memories.
      // See bug4721.
      return false;
    }
    else
    {
      return true;
    }
  }
}


NUPortConnectionBid::NUPortConnectionBid(NULvalue *actual,
					 NUNet *formal,
					 const SourceLocator& loc) :
  NUPortConnection(formal, loc),
  mActual(actual)
{}


NUPortConnectionBid::~NUPortConnectionBid()
{
  delete mActual;
}


void NUPortConnectionBid::getUses(NUNetSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(uses);
  }
  uses->insert(mFormal);
}


void NUPortConnectionBid::getUses(NUNetRefSet *uses) const
{
  if (mActual != 0) {
    mActual->getUses(uses);
  }
  uses->insert(uses->getFactory()->createNetRef(mFormal));
}


bool NUPortConnectionBid::queryUses(const NUNetRefHdl &use_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory *factory) const
{
  if (mActual != 0) {
    if (mActual->queryUses(use_net_ref, fn, factory)) {
      return true;
    }
  }
  NUNetRefHdl net_ref = factory->createNetRef(mFormal);
  return ((*net_ref).*fn)(*use_net_ref);
}

void NUPortConnectionBid::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  if (net_ref->getNet() == mFormal) {
    if (mActual != 0) {
      mActual->getDefs(uses);
      mActual->getUses(uses);
    }
  } else {
    if (mActual != 0) {
      mActual->getUses(net_ref, uses);
    }
    uses->insert(uses->getFactory()->createNetRef(mFormal));
  }
}


bool NUPortConnectionBid::queryUses(const NUNetRefHdl &def_net_ref,
				    const NUNetRefHdl &use_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory *factory) const
{
  if (def_net_ref->getNet() == mFormal) {
    if (mActual != 0) {
      if (mActual->queryDefs(use_net_ref, fn, factory)) {
	return true;
      }
      if (mActual->queryUses(use_net_ref, fn, factory)) {
	return true;
      }
    }
  } else {
    if (mActual != 0) {
      return mActual->queryUses(def_net_ref, use_net_ref, fn, factory);
    }
    NUNetRefHdl net_ref = factory->createNetRef(mFormal);
    return ((*net_ref).*fn)(*use_net_ref);
  }
  return false;
}


void NUPortConnectionBid::getDefs(NUNetSet *defs) const
{
  if (mActual != 0) {
    mActual->getDefs(defs);
  }
  defs->insert(mFormal);
}


void NUPortConnectionBid::getDefs(NUNetRefSet *defs) const
{
  if (mActual != 0) {
    mActual->getDefs(defs);
  }
  defs->insert(defs->getFactory()->createNetRef(mFormal));
}


bool NUPortConnectionBid::queryDefs(const NUNetRefHdl &def_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory *factory) const
{
  if (mActual != 0) {
    if (mActual->queryDefs(def_net_ref, fn, factory)) {
      return true;
    }
  }
  NUNetRefHdl net_ref = factory->createNetRef(mFormal);
  return ((*net_ref).*fn)(*def_net_ref);
}


void NUPortConnectionBid::replaceDef(NUNet *old_net, NUNet *new_net)
{
  if (mFormal == old_net) {
    mFormal = new_net;
  } else {
    mActual->replaceDef(old_net, new_net);
  }
}

bool NUPortConnectionBid::replace(NUNet *old_net, NUNet *new_net)
{
  bool changed = false;
  if ( mFormal == old_net ) {
    mFormal = new_net;
    changed = true;
  }
  if (mActual) {
    changed |= mActual->replace(old_net,new_net);
  }
  return changed;
}

bool NUPortConnectionBid::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePre);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if (mActual) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePost);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if (mActual) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  return changed;
}

void NUPortConnectionBid::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "PortConnectionBid(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal << ")  actual(" << mActual << ")" << UtIO::endl;

  mFormal->print(false, numspaces+2);
  if (mActual != 0) {
    mActual->print(recurse, numspaces+2);
  }
}


const char* NUPortConnectionBid::typeStr() const
{
  return "NUPortConnectionBid";
}

NUPortConnection* NUPortConnectionBid::copy(CopyContext & copy_context) const
{
  NUNet    * formal = copy_context.lookup(mFormal);
  NULvalue * actual = NULL;
  if (mActual) {
    actual = mActual->copy(copy_context);
  }
  return new NUPortConnectionBid(actual,
				 formal,
				 mLoc);
}

bool NUPortConnectionBid::isAliasable() const
{
  NULvalue * actual = getActual();
  NUNet    * formal = getFormal();

  if ( not actual or
       (not actual->isWholeIdentifier()) or
       (actual->getBitSize() != formal->getBitSize()) ) {
    return false;
  } else {
    if (actual->isWholeIdentifier() and 
        (actual->getWholeIdentifier()->is2DAnything() xor formal->is2DAnything()))
    {
      // Either both or neither of the formal & actual must be memories.
      // See bug4721.
      return false;
    }
    else
    {
      return true;
    }
  }
}


const NUModule* NUPortConnection::findParentModule() const
{
  NUNetSet nets;
  getDefs(&nets);
  NUNet* net = *nets.begin();
  return net->getScope()->getModule();
}

NUModule* NUPortConnection::findParentModule()
{
  NUNetSet nets;
  getDefs(&nets);
  NUNet* net = *nets.begin();
  return net->getScope()->getModule();
}

bool NUPortConnection::isPortConn() const
{
  return true;
}

/*static*/ void MsgContext::composeLocation (const NUPortConnection *con, UtString *location)
{
  if (con == NULL) {
    *location << "(null NUPortConnection)";
  } else if (con->getFormal () == NULL) {
    // something is quite wrong here... 
    const SourceLocator &loc = con->getLoc();
    loc.compose (location);
  } else {
    const NUNet *net = con->getFormal ();
    UtString netname;
    net->compose (&netname, NULL);
    UtString source;
    const SourceLocator &loc = con->getLoc();
    loc.compose (&source);
    *location << source << ": " << netname;
  }
}
