// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NULvalue.h"
#include "util/SourceLocator.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"

NUTriRegInit::~NUTriRegInit()
{
  delete mLvalue;
}


void NUTriRegInit::getDefs(NUNetSet *defs) const
{
  mLvalue->getDefs(defs);
}


bool NUTriRegInit::queryDefs(const NUNetRefHdl &def_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const
{
  return mLvalue->queryDefs(def_net_ref, fn, factory);
}


void NUTriRegInit::getDefs(NUNetRefSet *defs) const
{
  mLvalue->getDefs(defs);
}


void NUTriRegInit::replaceDef(NUNet *old_net, NUNet *new_net)
{
  mLvalue->replaceDef(old_net, new_net);
}

bool NUTriRegInit::replace(NUNet * old_net, NUNet * new_net)
{
  return mLvalue->replace(old_net,new_net);
}

bool NUTriRegInit::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  NULvalue * repl = translator(mLvalue, NuToNuFn::ePre);
  if (repl) {
    mLvalue = repl;
    changed = true;
  }
  changed |= mLvalue->replaceLeaves(translator);

  repl = translator(mLvalue, NuToNuFn::ePost);
  if (repl) {
    mLvalue = repl;
    changed = true;
  }

  return changed;
}

void NUTriRegInit::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << typeStr() << "(" << this << ") : " << mName->str() << " : pull" << UtIO::endl;

  if (recurse) {
    mLvalue->print(true, numspaces+2);
  }
}


const char* NUTriRegInit::typeStr() const
{
  return "TriRegInit";
}

const NUModule* NUTriRegInit::findParentModule() const
{
  return mLvalue->findParentModule();
}

NUModule* NUTriRegInit::findParentModule()
{
  return mLvalue->findParentModule();
}

void
NUTriRegInit::compose(UtString*buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  *buf << "TriRegInit ";
  mLvalue->compose (buf, scope, 0, recurse);
  *buf << "\n";
}

bool NUTriRegInit::isScheduled (void) const
{
  return mScheduled;
}

void NUTriRegInit::putScheduled (bool f)
{
  mScheduled = f;
}
