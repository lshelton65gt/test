// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUExpr.h"
#include "util/SourceLocator.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"
#include "util/StringAtom.h"
#include "nucleus/NUNetSet.h"
#include "util/UtIndent.h"

NUAlwaysBlock::NUAlwaysBlock(StringAtom * name,
                             NUBlock *block, 
			     NUNetRefFactory *netref_factory,
                             const SourceLocator& loc,
                             bool is_wild_card,
                             NUAlwaysT always_type) :

  NUStructuredProc(name, block, netref_factory, loc),
  mPrioBlock(NULL),
  mClkBlock(NULL),
  mReverseClockBlock(NULL),
  mOrigLevelExprList(false),
  mWildCardUsed(is_wild_card),
  mAlwaysType(always_type)
{
}


NUAlwaysBlock::~NUAlwaysBlock()
{
  for (NUEdgeExprList::iterator p = mEdgeExprs.begin(); p != mEdgeExprs.end();
       ++p)
  {
    NUEdgeExpr* edgeExpr = *p;
    delete edgeExpr;
  }

  for (NUExprList::iterator p = mLevelExprs.begin(); p != mLevelExprs.end();
       ++p)
  {
    NUExpr* levelExpr = *p;
    delete levelExpr;
  }

  // Do not delete the priority or clk blocks -- they are owned by the module
  if (mReverseClockBlock) {
    // delete the set containing the clock referers.
    delete mReverseClockBlock;
  }
}


NUAlwaysBlock* NUAlwaysBlock::removeFromPriorityChain() {
  NUAlwaysBlock* new_clock_block = mClkBlock;

  // If this is the clock-block, then disconnect all referers, possibly
  // repointing them at a newly anointed clock-block.
  // Do not delete the priority or clk blocks -- they are owned by the module.
  if (mReverseClockBlock != NULL) {
    NU_ASSERT(mClkBlock == NULL, this);

    new_clock_block = mPrioBlock;
    for (NUAlwaysBlockSet::iterator p = mReverseClockBlock->begin(),
           e = mReverseClockBlock->end(); p != e; ++p)
    {
      NUAlwaysBlock* referer = *p;
      NU_ASSERT(referer->mClkBlock == this, this);
      referer->mClkBlock = new_clock_block;
    }

    if (new_clock_block == NULL) {
      // delete the set containing the clock referers.
      delete mReverseClockBlock;
    }
    else {
      UInt32 erased = mReverseClockBlock->erase(new_clock_block);
      NU_ASSERT(erased == 1, this);
      new_clock_block->mReverseClockBlock = mReverseClockBlock;
      new_clock_block->mClkBlock = NULL;
    }
    mReverseClockBlock = NULL;
  }

  else if (mClkBlock != NULL) {
    // If this is in a priority block chain, then fix any blocks
    // that point to this to point to the next higher priority instead
    if (mClkBlock->mPrioBlock == this) {
      mClkBlock->mPrioBlock = mPrioBlock;
    }

    NUAlwaysBlockSet* blks = mClkBlock->mReverseClockBlock;
    UInt32 erased = blks->erase(this);
    NU_ASSERT(erased == 1, this);

    // If all the blocks for the clock-block were deleted before
    // the clock-block itself, then it's not longer a clock-block.
    if (blks->empty()) {
      delete blks;
      mClkBlock->mReverseClockBlock = NULL;
      NU_ASSERT(mClkBlock->mPrioBlock == NULL, mClkBlock);
    }
    else {
      // This loop is potentially n^2.  But usually the number of
      // blocks in a priority chain is fairly small -- I've never seen
      // more than 3.  Latch analysis may make longer chains, but this
      // optimization does not run after latch analysis, so I don't
      // think this will be a problem.  However, it might be better
      // to integrate this code more tightly into FoldI::sequentialGroup,
      // which has enough context to perform this operation without a
      // search.
      for (NUAlwaysBlockSet::iterator p = blks->begin(),
             e = blks->end(); p != e; ++p)
      {
        NUAlwaysBlock* ab = *p;
        if (ab->mPrioBlock == this) {
          ab->mPrioBlock = mPrioBlock;
        }
      }
    }
    mClkBlock = NULL;
  } // if
  mPrioBlock = NULL;

  return new_clock_block;
} // NUAlwaysBlock* NUAlwaysBlock::removeFromPriorityChain

void NUAlwaysBlock::getEdgeUses(NUNetSet *uses) const
{
  // Get the net ref set uses and translate them
  NUNetRefSet ref_uses(mNetRefFactory);
  getEdgeUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end();
       ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUAlwaysBlock::getEdgeUses(NUNetRefSet *uses) const
{
  // Get this always blocks edge uses. if this is a flop with async
  // resets we introduce pessimism and make every edge a uses for all
  // the split always blocks. This makes sure that if any of the
  // original always block is live, all the edges for all the splits
  // are live.
  //
  // It fixes a number of bugs in the scheduler.
  //
  // At some point, we like to redo how we handle async reset blocks
  // to make always blocks more standalone.
  NUAlwaysBlock* clockBlock = getClockBlock();
  if (clockBlock == NULL) {
    clockBlock = const_cast<NUAlwaysBlock*>(this);
  }
  NUAlwaysBlockVector blocks;
  blocks.push_back(clockBlock);
  clockBlock->getClockBlockReferers(&blocks);

  // Walk the blocks getting the edge uses
  for (NUAlwaysBlockVectorLoop l(blocks); !l.atEnd(); ++l) {
    // Get this always blocks edge uses
    NUAlwaysBlock* always = *l;
    for (EdgeExprLoop e = always->loopEdgeExprs(); !e.atEnd(); ++e) {
      NUEdgeExpr* edgeExpr = *e;
      edgeExpr->getUses(uses);
    }
  }
} // void NUAlwaysBlock::getEdgeUses


bool NUAlwaysBlock::queryEdgeUses(const NUNetRefHdl &use_net_ref,
                                  NUNetRefCompareFunction fn,
                                  NUNetRefFactory *factory) const
{
  // Query this always blocks edge uses. If this is a flop with async
  // resets we introduce pessimism and make every edge a uses for all
  // the split always blocks. This makes sure that if any of the
  // original always block is live, all the edges for all the splits
  // are live.
  //
  // It fixes a number of bugs in the scheduler.
  //
  // At some point, we like to redo how we handle async reset blocks
  // to make always blocks more standalone.
  NUAlwaysBlock* clockBlock = getClockBlock();
  if (clockBlock == NULL) {
    clockBlock = const_cast<NUAlwaysBlock*>(this);
  }
  NUAlwaysBlockVector blocks;
  blocks.push_back(clockBlock);
  clockBlock->getClockBlockReferers(&blocks);

  // Walk the blocks getting the edge uses
  for (NUAlwaysBlockVectorLoop l(blocks); !l.atEnd(); ++l) {
    // Get this always blocks edge uses
    NUAlwaysBlock* always = *l;
    for (EdgeExprLoop e = always->loopEdgeExprs(); !e.atEnd(); ++e) {
      NUEdgeExpr* edgeExpr = *e;
      if (edgeExpr->queryUses(use_net_ref, fn, factory)) {
        return true;
      }
    }
  }
  return false;
} // bool NUAlwaysBlock::queryEdgeUses


void NUAlwaysBlock::getUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getUses(&ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUAlwaysBlock::getUses(NUNetRefSet *uses) const
{
  NUStructuredProc::getUses(uses);
  // Add in edge nets
  getEdgeUses(uses);
}


bool NUAlwaysBlock::queryUses(const NUNetRefHdl &use_net_ref,
			      NUNetRefCompareFunction fn,
			      NUNetRefFactory *factory) const
{
  return ( queryEdgeUses(use_net_ref, fn, factory) or
           NUStructuredProc::queryUses(use_net_ref, fn, factory) );
}


void NUAlwaysBlock::getUses(NUNet *net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUAlwaysBlock::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  NUStructuredProc::getUses(net_ref, uses);

  // Add in edge nets
  getEdgeUses(uses);
}


bool NUAlwaysBlock::queryUses(const NUNetRefHdl &def_net_ref,
			      const NUNetRefHdl &use_net_ref,
			      NUNetRefCompareFunction fn,
			      NUNetRefFactory *factory) const
{
  return ( queryEdgeUses(use_net_ref, fn, factory) or
           NUStructuredProc::queryUses(def_net_ref, use_net_ref, fn, factory) );
}


bool NUAlwaysBlock::hasEdgeExprs()
{
  return !( mEdgeExprs.empty());
}

void NUAlwaysBlock::addEdgeExpr(NUEdgeExpr *expr)
{
  mEdgeExprs.push_back(expr);
}

void NUAlwaysBlock::addLevelExpr(NUExpr *expr)
{
  mLevelExprs.push_back(expr);
}

void NUAlwaysBlock::setPriorityBlock(NUAlwaysBlock *block)
{
  if( mPrioBlock ) {
    // Block merging may update the priority block.
    // Adjust the referers.
    NUAlwaysBlock* clockBlock = this;
    if( clockBlock->getClockBlock() )
      clockBlock = clockBlock->getClockBlock();
    clockBlock->removeClockBlockReferer( mPrioBlock );
    if( block )
      clockBlock->addClockBlockReferer( block );
  }
  mPrioBlock = block;
}


NUAlwaysBlock *NUAlwaysBlock::getPriorityBlock() const
{
  return mPrioBlock;
}


void NUAlwaysBlock::setClockBlock(NUAlwaysBlock *block)
{
  if(mClkBlock)
    mClkBlock->removeClockBlockReferer( this );
  mClkBlock = block;
  if( block )
    block->addClockBlockReferer( this );
}


NUAlwaysBlock *NUAlwaysBlock::getClockBlock() const
{
  return mClkBlock;
}


NUEdgeExpr *NUAlwaysBlock::getSyncReset()
{
  NUEdgeExpr *ee = NULL;
  
  // The clock edge is the one and only levelHigh or levelLow expression.
  for( NUEdgeExprListIter i= mEdgeExprs.begin(); i != mEdgeExprs.end(); ++i) {
    ClockEdge ce = (*i)->getEdge();
  
    if( ce == eLevelHigh || ce == eLevelLow ) {
      NU_ASSERT( ee == NULL, this);
      ee = *i;
      // continue to make sure there is only one sync reset
    }
  }
  return ee;
}


void NUAlwaysBlock::addClockBlockReferer(NUAlwaysBlock * referer)
{
  // The fix-up for async block merging (updatePriorityClockBlocks())
  // requires that the referer may be updated before the clock block,
  // so this assert fails.
  // NU_ASSERT(referer->getClockBlock()==this, this);
  if (not mReverseClockBlock) {
    mReverseClockBlock = new NUAlwaysBlockSet;
  }
  mReverseClockBlock->insert(referer);
}


void NUAlwaysBlock::removeClockBlockReferer(NUAlwaysBlock * referer)
{
  // The fix-up for async block merging (updatePriorityClockBlocks())
  // requires that the referer may be updated before the clock block,
  // so this assert fails.
  // NU_ASSERT(referer->getClockBlock()==this, this);
  // NU_ASSERT(mReverseClockBlock, this);
  if( mReverseClockBlock )
    mReverseClockBlock->erase(referer);
}


void NUAlwaysBlock::getClockBlockReferers(NUAlwaysBlockVector * referers) const
{
  if (mReverseClockBlock) {
    referers->insert(referers->end(),
                     mReverseClockBlock->begin(),
		     mReverseClockBlock->end());
  }
}

bool NUAlwaysBlock::isClockBlockReferer(NUAlwaysBlock * referer) const {
  return (mReverseClockBlock != NULL) &&
    (mReverseClockBlock->find(referer) != mReverseClockBlock->end());
}

bool NUAlwaysBlock::hasClockBlockReferer() const {
  return (mReverseClockBlock != NULL) && !mReverseClockBlock->empty();
}

const char* NUAlwaysBlock::typeStr() const
{
  return "NUAlwaysBlock";
}


void NUAlwaysBlock::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);

  mLoc.print();
  UtIO::cout() << "AlwaysBlock(" << this << ") : " << getName()->str() << " : "
	    << "PriorityBlock(" << mPrioBlock << ") "
	    << "ClockBlock(" << mClkBlock << ") ";
  if (isSequential()) {
    UtIO::cout() << "sequential"; // the clock edge is only edge in @()
  } else if (isWildCard()){
    UtIO::cout() << "wildCard"; // saw @(*)
  } else if ( isOrigLevelExprList() ){
    UtIO::cout() << "levelSensitive"; // no edges in @()
  }
  if ( isAlwaysComb() ){
    UtIO::cout() << " always_comb";
  } else if ( isAlwaysFF() ){
    UtIO::cout() << " always_ff";
  } else if ( isAlwaysLatch() ){
    UtIO::cout() << " always_latch";
  }
  UtIO::cout() << UtIO::endl;
  if (recurse) {
    NUEdgeExprListIter iter;
    for (iter = mEdgeExprs.begin(); iter != mEdgeExprs.end(); iter++) {
      (*iter)->print(recurse, numspaces+2);
    }
    for (NUExprList::const_iterator itr = mLevelExprs.begin(); itr != mLevelExprs.end(); itr++) {
      (*itr)->print(recurse, numspaces+2);
    }
    
    NUStructuredProc::print(recurse, numspaces+2);
  }
}


void NUAlwaysBlock::compose(UtString* buf,
                            const STBranchNode* scope,
                            int numSpaces,
                            bool recurse ) const
{
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }


  if ( isAlwaysComb() ){
    *buf << "always_comb ";
  } else if ( isAlwaysFF() ){
    *buf << "always_ff ";
  } else if ( isAlwaysLatch() ){
    *buf << "always_latch ";
  } else {
    *buf << "always ";
  } 

  if (isSequential())
  {
    bool firstEdge = true;
    *buf << "@(";
    for (NUEdgeExprListIter iter=mEdgeExprs.begin (); iter != mEdgeExprs.end (); iter++)
    {
      NUExpr* edge = (*iter); // upcast so that gcc will find the default args (which are
      // statically bound), don't worry, the correct method is found
      // when it is dynamically bound.

      if ( firstEdge ) { firstEdge = false; }
      else             { *buf << " or ";     }
      edge->compose(buf, scope, false); // don't bother with root of name (test/clock-dumper)
    }

    // Show the priority signature, as you have to interpret the signature
    // of the clock block taking into account the priority block.  This is
    // not really verilog, but let's pretend there's an "and" keyword.
    for  (NUAlwaysBlock* prio = mPrioBlock; prio != NULL;
          prio = prio->mPrioBlock)
    {
      NUEdgeExpr* ee = prio->getEdgeExpr();
      *buf << " and not(";
      ee->compose(buf, scope, false);
      *buf << ")";
    }

    *buf << ")";
  }
  else if ( isWildCard() ){
    *buf << "@(*)";
  }
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
  NUStructuredProc::compose(buf, scope, numSpaces + 2, recurse);
} // void NUAlwaysBlock::compose

bool NUAlwaysBlock::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  changed = mBlock->replaceLeaves(translator);
  for (NUEdgeExprListIter iter = mEdgeExprs.begin(); iter != mEdgeExprs.end(); iter++) {
    changed |= (*iter)->replaceLeaves(translator);
  }

  // Now we iterate over level expressions in the always block
  for (NUExprList::iterator iter = mLevelExprs.begin(); iter != mLevelExprs.end(); iter++) 
  {
    NUExpr* repl = (*iter);

    // The translator functor is called for original expression, which
    // activates NUDesignWalker. NUDesignWalker is used in many phases of cbuild,
    // so replaceLeaves is called for every functor, derived from NUDesignWalker.
    // If that functor has NUExpr operator overloaded, the original expr might 
    // be replaced by new one.
    NUExpr* repl_pre = translator(repl, NuToNuFn::ePre);
    if(repl_pre != NULL)
    {
      // If the expression was replaced, store it.
      repl = repl_pre;
      (*iter) = repl_pre;
      changed |= true;
    }

    // Now replace the leaves for the expresiion.
    repl->replaceLeaves(translator);

    // Do the same for the ePost.
    NUExpr* repl_post = translator(repl, NuToNuFn::ePost);
    if(repl_post != NULL)
    {
      (*iter) = repl_post;
      changed |= true;
    }
  }



  if (mPrioBlock) {
    NUAlwaysBlock *new_prio_block = translator(mPrioBlock, NuToNuFn::ePre);
    if (new_prio_block) {
      mPrioBlock = new_prio_block;
      changed = true;
    }
  }

  if (mClkBlock) {
    NUAlwaysBlock *new_clk_block = translator(mClkBlock, NuToNuFn::ePre);
    if (new_clk_block) {
      mClkBlock->removeClockBlockReferer(this);
      mClkBlock = new_clk_block;
      mClkBlock->addClockBlockReferer(this);
      changed = true;
    }
  }

  return changed;
}

bool NUAlwaysBlock::isSequential() const
{
  // The clock edge is the one and only posedge or negedge expression.
  for( NUEdgeExprListIter i= mEdgeExprs.begin(); i != mEdgeExprs.end(); ++i) {
    ClockEdge ce = (*i)->getEdge();
  
    if( ce == eClockPosedge || ce == eClockNegedge )
      return true;
  }
  return false;
}


NUEdgeExpr* NUAlwaysBlock::getEdgeExpr() const
{
  // The clock edge is the one and only posedge or negedge expression.
  for( NUEdgeExprListIter i= mEdgeExprs.begin(); i != mEdgeExprs.end(); ++i) {
    ClockEdge ce = (*i)->getEdge();
  
    if( ce == eClockPosedge || ce == eClockNegedge ) {
      return *i;
    }
  }
  return NULL;
}


void NUAlwaysBlock::getEdgeExprList(NUEdgeExprList *edge_list) const
{
  edge_list->insert(edge_list->end(), mEdgeExprs.begin(), mEdgeExprs.end());
}


void NUAlwaysBlock::removeEdgeExpr(NUEdgeExpr *edge)
{
  NUEdgeExprListNonconstIter iter = std::find(mEdgeExprs.begin(), mEdgeExprs.end(), edge);
  NU_ASSERT2(iter != mEdgeExprs.end(), edge, this);
  mEdgeExprs.erase(iter);
}

void NUAlwaysBlock::replaceEdgeExprs(NUEdgeExprList *edge_list)
{
  mEdgeExprs.assign(edge_list->begin(), edge_list->end());
}


bool NUAlwaysBlock::isSimilar(const NUStructuredProc& /*other*/)
  const
{
  return false;
} // bool NUAlwaysBlock::isSimilar

bool NUAlwaysBlock::stopsFlow(NUNet* /*inputNet*/, bool* /*val*/)
{
/*
  for (NUStmtList::iterator i = mStmts.begin(); i != mStmts.end(); ++i)
  {
    // Verify that this node has no side effects if a value is a constant.
    // For now, this can only happen if the node is an if-statement
    NUIf* ifStmt = dynamic_cast<NUIf*>(*i);
    if (ifStmt == NULL)
      return false;             // unconditional execution -- cannot stop flow
    NUExpr* cond = ifStmt->getCond();
    NU_ASSERT(cond,this);
  }
*/
  return false;
}


void
NUAlwaysBlock::removeConstantSyncReset()
{
  if( !isSequential() )
    return;
    
  bool changed = false;
  NUEdgeExprList replacementEdges;
  getEdgeExprList( &replacementEdges );
    
  for( NUAlwaysBlock::EdgeExprLoop edge = loopEdgeExprs(); not edge.atEnd(); ++edge ) {
    ClockEdge ce = (*edge)->getEdge();
    if( ce != eLevelHigh && ce != eLevelLow )
      continue;

    NUExpr *expr = (*edge)->getExpr();
    if( expr->getType() != NUExpr::eNUConstNoXZ )
      continue;

    // We have a constant level.  Determine if it is active.
    NUConst* constExpr = expr->castConst();
    
    UInt32 val;
    constExpr->getUL(&val);
    // Levels are opposite to match with async reset.
    bool isActive = val == 0;
    if( ce == eLevelLow )
      isActive = !isActive;

    // Remove the constant active/inactive edges from the list.
    replacementEdges.remove( *edge );
    delete *edge;
    changed = true;

    // For inactive levels, disable the always block by removing all statements.
    if( !isActive ) {
      NUStmtLoop loop = getBlock()->loopStmts();
      NUStmtList stmts(loop.begin(), loop.end());
      for (NUStmtList::iterator i = stmts.begin(); i != stmts.end(); ++i)
        delete (*i);
	    
      stmts.clear();
      getBlock()->replaceStmtList( stmts );
    } 
  }
    
  if( changed )
    replaceEdgeExprs( &replacementEdges );
}

void MsgContext::composeLocation(NUAlwaysBlock *block, UtString* location)
{
  if (block == 0) {
    *location << "(null NUAlwaysBlock)";
  } else {
    const SourceLocator& loc = block->getLoc();
    UtString source;
    loc.compose(&source);
    *location << source;
  }
}

NUAlwaysBlock * NUAlwaysBlock::clone ( CopyContext & copy_context,
                                       bool copy_prio_pointers ) const
{
  NUStmt * block_copy = getBlock()->copy(copy_context);
  NUBlock * block = dynamic_cast<NUBlock*>(block_copy);

  StringAtom * name = copy_context.mScope->newBlockName(copy_context.mStrCache,
                                                        getLoc());
  NUAlwaysBlock * always = new NUAlwaysBlock ( name,
                                               block,
					       mNetRefFactory,
					       getLoc(),
                                               mWildCardUsed,
                                               mAlwaysType);
  if (copy_prio_pointers) {
    if (getPriorityBlock()) {
      always->setPriorityBlock(getPriorityBlock());
    }
    if (getClockBlock()) {
      always->setClockBlock(getClockBlock());
    }
  }

  NUEdgeExprList expr_list;
  getEdgeExprList(&expr_list);
  for ( NUEdgeExprListIter expr_iter = expr_list.begin();
	expr_iter != expr_list.end();
	++expr_iter ) {
    NUEdgeExpr * edge_expr = (*expr_iter);
    NUEdgeExpr * new_edge_expr = dynamic_cast<NUEdgeExpr*>(edge_expr->copy(copy_context));
    NU_ASSERT(new_edge_expr, this);
    always->addEdgeExpr(new_edge_expr);
  }

  const NUExprList* level_expr_list = &mLevelExprs;
  for ( NUExprListIter level_expr_iter = level_expr_list->begin();
	level_expr_iter != level_expr_list->end();
	++level_expr_iter ) {
    NUExpr * level_expr = (*level_expr_iter);
    NUExpr * new_level_expr = level_expr->copy(copy_context);
    NU_ASSERT(new_level_expr, this);
    always->addLevelExpr(new_level_expr);
  }

  return always;
}

