// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUTF.h"
#include "util/UtIOStream.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUExprFactory.h"

/*!
  \file
  Implementation of NUTFArgConnection classes.
*/


const NUModule* NUTFArgConnection::findParentModule() const
{
  return getTF()->findParentModule();
}

NUModule* NUTFArgConnection::findParentModule()
{
  return getTF()->findParentModule();
}

UInt32 NUTFArgConnection::getBitSize() const
{ 
  return mFormal->getBitSize(); 
}

void NUTFArgConnection::replaceTF(NUTF* newTF)
{
  // 'this' is the new copy of a NUTFArgConnection that was copied from the old TF

  // Replace the formal
  NU_ASSERT(newTF->getNumArgs() == mTF->getNumArgs(), this);

  int index = mTF->getArgIndex(mFormal); // find index of old formal arg in the connection list for the old TF
  NU_ASSERT(index != -1, this);
  mFormal = newTF->getArg(index); // set the pointer to the formal arg in 'this' which is a new copy of the NUTFArgConnection

  // Now we can update the TF safely
  mTF = newTF;
}

void NUTFArgConnectionInput::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "TFArgConnectionInput(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal << ")  actual(" << mActual << ")" << UtIO::endl;
  mFormal->print(false, numspaces+2);
  mActual->print(recurse, numspaces+2);
}

// void NUTFArgConnection::compose(UtString*, STBranchNode* /* scope */, const bool, const bool, const char*) const {}

void NUTFArgConnectionOutput::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();

  UtIO::cout() << "TFArgConnectionOutput(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal << ")  actual(" << mActual << ")" << UtIO::endl;
  mFormal->print(false, numspaces+2);
  mActual->print(recurse, numspaces+2);
}


void NUTFArgConnectionBid::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "TFArgConnectionBid(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal << ")  actual(" << mActual << ")" << UtIO::endl;
  mFormal->print(false, numspaces+2);
  mActual->print(recurse, numspaces+2);
}


void NUTFArgConnection::manage(NUExprFactory*)
{
}

bool NUTFArgConnection::isSigned() const
{
  return mFormal->isSigned();
}


NUTFArgConnectionInput::NUTFArgConnectionInput(NUExpr *actual, 
                                               NUNet *formal, 
                                               NUTF *tf, 
                                               const SourceLocator& loc) :
  NUTFArgConnection(formal, tf, loc),
  mActual(actual),
  mOwnActual(true)
{
}


NUTFArgConnectionInput::~NUTFArgConnectionInput()
{
  if (mOwnActual)
    delete mActual;
}

void NUTFArgConnectionInput::manage(NUExprFactory* factory)
{
  mActual = const_cast<NUExpr*>(factory->insert(mActual));
  mOwnActual = false;
}

NUTFArgConnectionOutput::NUTFArgConnectionOutput(NULvalue *actual, 
                                                 NUNet *formal, 
                                                 NUTF *tf, 
                                                 const SourceLocator& loc) :
  NUTFArgConnection(formal, tf, loc),
  mActual(actual)
{
}


NUTFArgConnectionOutput::~NUTFArgConnectionOutput()
{
  delete mActual;
}


NUTFArgConnectionBid::~NUTFArgConnectionBid()
{
  delete mActual;
}


void NUTFArgConnectionInput::getUses(NUNetSet *uses) const
{
  mActual->getUses(uses);
}

void NUTFArgConnectionInput::getUsesElab(STBranchNode* scope,
                                         NUNetElabSet *uses)
  const
{
  mActual->getUsesElab(scope, uses);
}


void NUTFArgConnectionInput::getUses(NUNetRefSet *uses) const
{
  mActual->getUses(uses);
}


bool NUTFArgConnectionInput::queryUses(const NUNetRefHdl &use_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory *factory) const
{
  return mActual->queryUses(use_net_ref, fn, factory);
}


void NUTFArgConnectionInput::getUses(NUNet * /* net -- unused */, NUNetSet *uses) const
{
  mActual->getUses(uses);
}


void NUTFArgConnectionInput::getUses(const NUNetRefHdl & /* net_ref -- unused */,
				     NUNetRefSet *uses) const
{
  mActual->getUses(uses);
}


bool NUTFArgConnectionInput::queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
				       const NUNetRefHdl &use_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory *factory) const
{
  return mActual->queryUses(use_net_ref, fn, factory);
}


void NUTFArgConnectionInput::getDefs(NUNetSet *defs) const
{
  defs->insert(mFormal);
}


void NUTFArgConnectionInput::getDefs(NUNetRefSet *defs) const
{
  defs->insert(defs->getFactory()->createNetRef(mFormal));
}


bool NUTFArgConnectionInput::queryDefs(const NUNetRefHdl &def_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory *factory) const
{
  NUNetRefHdl net_ref = factory->createNetRef(mFormal);
  return ((*net_ref).*fn)(*def_net_ref);
}


void NUTFArgConnectionInput::replaceDef(NUNet *old_net, NUNet *new_net)
{
  if (mFormal == old_net) {
    mFormal = new_net;
  }
}


bool NUTFArgConnectionInput::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  if ( mFormal == old_net ) {
    mFormal = new_net;
    changed = true;
  }
  if ( mActual )
    changed |= mActual->replace(old_net,new_net);
  return changed;
}

bool NUTFArgConnectionInput::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePre);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if ( mActual ) {
    NUExpr * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }
  {
    NUTF * repl = translator(mTF, NuToNuFn::ePre);
    if (repl) {
      mTF = repl;
      changed = true;
    }
  }
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePost);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if ( mActual ) {
    NUExpr * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  {
    NUTF * repl = translator(mTF, NuToNuFn::ePost);
    if (repl) {
      mTF = repl;
      changed = true;
    }
  }

  return changed;
}

void NUTFArgConnectionOutput::getUses(NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}

void NUTFArgConnectionOutput::getUsesElab(STBranchNode*, NUNetElabSet*)
  const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


void NUTFArgConnectionOutput::getUses(NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


bool NUTFArgConnectionOutput::queryUses(const NUNetRefHdl & /* use_net_ref */,
					NUNetRefCompareFunction /* fn */,
					NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
  return false;
}


void NUTFArgConnectionOutput::getUses(NUNet * /* net */, NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


void NUTFArgConnectionOutput::getUses(const NUNetRefHdl & /* net_ref */,
				      NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


bool NUTFArgConnectionOutput::queryUses(const NUNetRefHdl & /* def_net_ref */,
					const NUNetRefHdl & /* use_net_ref */,
					NUNetRefCompareFunction /* fn */,
					NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
  return false;
}


void NUTFArgConnectionOutput::getDefs(NUNetSet * /* defs */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


void NUTFArgConnectionOutput::getDefs(NUNetRefSet * /* defs */) const
{
  // TBD
  NU_ASSERT (0=="unimplemented", this);
}


bool NUTFArgConnectionOutput::queryDefs(const NUNetRefHdl & /* def_net_ref */,
					NUNetRefCompareFunction /* fn */,
					NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
  return false;
}


void NUTFArgConnectionOutput::replaceDef(NUNet * old_net, NUNet* new_net)
{
  if (mActual) {
    mActual->replaceDef(old_net,new_net);
  }
}


bool NUTFArgConnectionOutput::replace(NUNet * old_net, NUNet* new_net)
{
  bool changed = false;
  if ( mFormal == old_net ) {
    mFormal = new_net;
    changed = true;
  }
  if ( mActual )
    changed |= mActual->replace(old_net,new_net);
  return changed;
}

bool NUTFArgConnectionOutput::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePre);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }
  {
    NUTF * repl = translator(mTF, NuToNuFn::ePre);
    if (repl) {
      mTF = repl;
      changed = true;
    }
  }

  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePost);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  {
    NUTF * repl = translator(mTF, NuToNuFn::ePost);
    if (repl) {
      mTF = repl;
      changed = true;
    }
  }

  return changed;
}

void NUTFArgConnectionBid::getUses(NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


void NUTFArgConnectionBid::getUsesElab(STBranchNode*, NUNetElabSet*) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


void NUTFArgConnectionBid::getUses(NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


bool NUTFArgConnectionBid::queryUses(const NUNetRefHdl & /* use_net_ref */,
				     NUNetRefCompareFunction /* fn */,
				     NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
  return false;
}


void NUTFArgConnectionBid::getUses(NUNet * /* net */, NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


void NUTFArgConnectionBid::getUses(const NUNetRefHdl & /* net_ref */,
				   NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


bool NUTFArgConnectionBid::queryUses(const NUNetRefHdl & /* def_net_ref */,
				     const NUNetRefHdl & /* use_net_ref */,
				     NUNetRefCompareFunction /* fn */,
				     NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
  return false;
}


void NUTFArgConnectionBid::getDefs(NUNetSet * /* defs */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


void NUTFArgConnectionBid::getDefs(NUNetRefSet * /* defs */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
}


bool NUTFArgConnectionBid::queryDefs(const NUNetRefHdl & /* def_net_ref */,
				     NUNetRefCompareFunction /* fn */,
				     NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0=="unimplemented", this);
  return false;
}


void NUTFArgConnectionBid::replaceDef(NUNet * old_net, NUNet* new_net)
{
  if (mActual) {
    mActual->replaceDef(old_net,new_net);
  }
}

bool NUTFArgConnectionBid::replace(NUNet * old_net, NUNet* new_net)
{
  bool changed = false;
  if ( mFormal == old_net ) {
    mFormal = new_net;
    changed = true;
  }
  if ( mActual )
    changed |= mActual->replace(old_net,new_net);
  return changed;
}

bool NUTFArgConnectionBid::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePre);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }
  {
    NUTF * repl = translator(mTF, NuToNuFn::ePre);
    if (repl) {
      mTF = repl;
      changed = true;
    }
  }
  {
    NUNet * repl = translator(mFormal, NuToNuFn::ePost);
    if (repl) {
      mFormal = repl;
      changed = true;
    }
  }
  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  {
    NUTF * repl = translator(mTF, NuToNuFn::ePost);
    if (repl) {
      mTF = repl;
      changed = true;
    }
  }

  return changed;
}

const char* NUTFArgConnectionInput::typeStr() const
{
  return "NUTFArgConnectionInput";
}


const char* NUTFArgConnectionOutput::typeStr() const
{
  return "NUTFArgConnectionOutput";
}


const char* NUTFArgConnectionBid::typeStr() const
{
  return "NUTFArgConnectionBid";
}


NUTFArgConnection* NUTFArgConnectionInput::copy(CopyContext &copy_context) const
{
  NUNet * formal = copy_context.lookup(mFormal);
  return new NUTFArgConnectionInput(mActual->copy(copy_context), formal, mTF, mLoc);
}


NUTFArgConnection* NUTFArgConnectionOutput::copy(CopyContext &copy_context) const
{
  NUNet * formal = copy_context.lookup(mFormal);
  return new NUTFArgConnectionOutput(mActual->copy(copy_context), formal, mTF, mLoc);
}


NUTFArgConnection* NUTFArgConnectionBid::copy(CopyContext &copy_context) const
{
  NUNet * formal = copy_context.lookup(mFormal);
  return new NUTFArgConnectionBid(mActual->copy(copy_context), formal, mTF, mLoc);
}

bool NUTFArgConnectionInput::operator==(const NUTFArgConnection& arg) const
{
  if  (getType () != arg.getType ())
    return false;

  const NUTFArgConnectionInput* inArg = dynamic_cast<const NUTFArgConnectionInput*>(&arg);
  return (*getActual () == *(inArg->getActual ()));
}

bool NUTFArgConnectionOutput::operator==(const NUTFArgConnection& arg) const
{
  if  (getType () != arg.getType ())
    return false;

  const NUTFArgConnectionOutput* outArg = dynamic_cast<const NUTFArgConnectionOutput*>(&arg);
  return (*getActual () == *(outArg->getActual ()));
}

bool NUTFArgConnectionBid::operator==(const NUTFArgConnection& arg) const
{
  if  (getType () != arg.getType ())
    return false;

  const NUTFArgConnectionBid* bidArg = dynamic_cast<const NUTFArgConnectionBid*>(&arg);
  return (*getActual () == *(bidArg->getActual ()));
}

void
NUTFArgConnectionInput::compose(UtString* pbuf, const STBranchNode* scope, int, bool) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, pbuf);
    return; 
  }
  getActual()->compose(pbuf, scope);
}

void
NUTFArgConnectionBid::compose(UtString* pbuf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, pbuf);
    return; 
  }
  getActual()->compose(pbuf, scope, numSpaces, recurse);
}

void
NUTFArgConnectionOutput::compose(UtString* pbuf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, pbuf);
    return; 
  }
  getActual()->compose(pbuf, scope, numSpaces, recurse);
}

void NUTFArgConnectionInput::getActuals(NUNetSet* actuals) const
{
  getActual()->getUses(actuals);
}

void NUTFArgConnectionOutput::getActuals(NUNetSet* actuals) const
{
  getActual()->getDefs(actuals);
}

void NUTFArgConnectionBid::getActuals(NUNetSet* actuals) const
{
  getActual()->getDefs(actuals);
}
