//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "nucleus/NUNetRefHashSet.h"

void NUNetRefHashSet::copyHelper(const NUNetRefHashSet& src) {
  mMap = src.mMap;
}

bool NUNetRefHashSet::insert(NUNet* net, DynBitVecDesc newBits) {
  if (!newBits.empty()) {
    NetRefMap::MapEntry* p = mMap.findEntry(net);
    if (p != NULL) {
      DynBitVecDesc& oldBits = p->mKeyVal.second;
      DynBitVector merged;
      DynBitVecDesc::merge(oldBits, newBits, &merged, net->getBitSize());
      DynBitVectorFactory* bfactory = mFactory->getDynBitVectorFactory();
      p->mKeyVal.second = DynBitVecDesc(merged, bfactory);
    }
    else {
      mMap[net] = newBits;
    }
  }
  return false;
}

bool NUNetRefHashSet::insert(NUNet* net, const DynBitVector& newBitVec) {
  if (newBitVec.any()) {
    DynBitVectorFactory* bfactory = mFactory->getDynBitVectorFactory();
    DynBitVecDesc newBits(newBitVec, bfactory);
    NetRefMap::MapEntry* p = mMap.findEntry(net);
    if (p != NULL) {
      DynBitVecDesc& oldBits = p->mKeyVal.second;
      DynBitVector merged;
      DynBitVecDesc::merge(oldBits, newBits, &merged, net->getBitSize());
      p->mKeyVal.second = DynBitVecDesc(merged, bfactory);
    }
    else {
      mMap[net] = newBits;
    }
  }
  return false;
}

bool NUNetRefHashSet::insert(const NUNetRefHashSet& src) {
  bool ret = false;
  for (NetRefMap::UnsortedCLoop p(src.mMap); !p.atEnd(); ++p) {
    ret |= insert(p.getKey(), p.getValue());
  }
  return ret;
}

void NUNetRefHashSet::set_intersection(const NUNetRefHashSet &one,
                                   const NUNetRefHashSet &two,
                                   NUNetRefHashSet &out)
{
  if (one.size() > two.size()) {
    set_intersection(two, one, out);
    return;
  }

  DynBitVector intersection;
  for (NetRefMap::UnsortedCLoop p(one.mMap); !p.atEnd(); ++p) {
    NUNet* net = p.getKey();
    const NetRefMap::MapEntry* q = two.mMap.findEntry(net);
    if (q != NULL) {
      DynBitVecDesc bits1 = p.getValue();
      DynBitVecDesc bits2 = q->mKeyVal.second;
      DynBitVecDesc::intersect(bits1, bits2, &intersection, net->getBitSize());
      if (intersection.any()) {
        out.insert(net, intersection);
      }
    }
  }
}


bool NUNetRefHashSet::set_has_intersection(const NUNetRefHashSet & one,
                                       const NUNetRefHashSet & two)
{
  if (one.size() > two.size()) {
    return set_has_intersection(two, one);
  }

  for (NetRefMap::UnsortedCLoop p(one.mMap); !p.atEnd(); ++p) {
    NUNet* net = p.getKey();
    const NetRefMap::MapEntry* q = two.mMap.findEntry(net);
    if (q != NULL) {
      DynBitVecDesc bits1 = p.getValue();
      DynBitVecDesc bits2 = q->mKeyVal.second;
      if (bits1.anyCommonBitsSet(bits2, net->getBitSize())) {
        return true;
      }
    }
  }
  return false;
}

bool NUNetRefHashSet::findNet(NUNet* net, DynBitVecDesc* bits) {
  NetRefMap::MapEntry* p = mMap.findEntry(net);
  if (p != NULL) {
    *bits = p->mKeyVal.second;
    return true;
  }
  return false;
}

// Not Yet Implemented
#if 0
bool NUNetRefHashSet::insert(const NUNetRefHdl& ref) {
  if (!ref->empty()) {
    NUNet* net = ref->getNet();
    DynBitVecDesc newBits;
    ref->getUsageMask(&newBits);
    NetRefMap::MapEntry* p = mMap.findEntry(net);
    if (p != NULL) {
      DynBitVecDesc& oldBits = p->mKeyVal.second;
      DynBitVector merged;
      DynBitVecDesc::merge(oldBits, newBits, &merged, net->getBitSize());
      DynBitVectorFactory* bfactory = mFactory->getDynBitVectorFactory();
      p->mKeyVal.second = DynBitVecDesc(merged, bfactory);
    }
    else {
      mMap[net] = newBits;
    }
  }
  return false;
}

NUNetRefSet::const_iterator NUNetRefSet::find(const NUNetRefHdl& ref, NUNetRefCompareFunction fn) const
{
  const_iterator iter = mSet.find(ref);

  if ((iter != mSet.end()) and (((*ref).*fn)(**iter))) {
    return iter;
  } else {
    return mSet.end();
  }
}

NUNetRefSet::iterator NUNetRefSet::find(const NUNetRefHdl& ref, NUNetRefCompareFunction fn)
{
  iterator iter = mSet.find(ref);

  if ((iter != mSet.end()) and (((*ref).*fn)(**iter))) {
    return iter;
  } else {
    return mSet.end();
  }
}

void NUNetRefSet::erase(NUNetRefHdl& ref, NUNetRefCompareFunction fn)
{
  iterator iter = find(ref, fn);
  if (iter != mSet.end()) {
    mSet.erase(iter);
  }
}


void NUNetRefSet::erase(NUNetRefHdl& ref)
{
  iterator iter = find(ref, &NUNetRef::overlapsSameNet);

  if (iter != mSet.end()) {
    NUNetRefHdl cur = *iter;

    if (ref->covers(*cur)) {
      mSet.erase(iter);
    } else {
      NUNetRefHdl new_ref = mFactory->subtract(*iter, ref);
      mSet.erase(iter);
      mSet.insert(new_ref);
    }
  }
}


/*! Choose one element from a net ref set in a way that is stable
 *  across builds.  This routine is O(n) in the size of the set,
 *  and uses a fairly expensive comparison function.
 */
NUNetRefSet::iterator NUNetRefSet::stableChoice() const
{
  if (empty())
    return end();

  // start with the first netref as the best
  iterator n = begin();
  iterator best = n;

  // loop over all subsequent netrefs, promoting any better than the current best
  ++n;
  while (n != end())
  {
    // this comparison is meant to be stable (ie., not pointer-based)
    if (NUNetRef::compare(*(*best), *(*n), true) <= 0)
      best = n;
    ++n;
  }

  // return the iterator to the best entry
  return best;
}

void NUNetRefSet::print(int indent) const
{
  for (const_iterator iter = mSet.begin(); iter != mSet.end(); ++iter) {
    (*iter)->print(indent+2);
  }
}


UInt32 NUNetRefSet::getNumBits() const
{
  UInt32 bit_cnt = 0;
  for (NUNetRefSet::const_iterator iter = begin(); iter != end(); ++iter) {
    bit_cnt += (*iter)->getNumBits();
  }
  return bit_cnt;
}

void NUNetRefSet::set_difference(const NUNetRefSet &one,
                                 const NUNetRefSet &two,
                                 NUNetRefSet &out)
{
  for (NUNetRefSet::const_iterator iter = one.begin(); iter != one.end(); ++iter) {
    NUNetRefSet::const_iterator find_iter = two.find(*iter, &NUNetRef::overlapsSameNet);
    if (find_iter != two.end()) {
      NUNetRefHdl d_net_ref = out.getFactory()->subtract(*iter, *find_iter);
      if (not d_net_ref->empty()) {
	out.insert(d_net_ref);
      }
    } else {
      out.insert(*iter);
    }
  }
}


void NUNetRefSet::set_subtract(const NUNetRefSet & one,
                               NUNetRefSet & out)
{
  // If the resultant set is empty there is nothing to do. It is
  // cheaper to test for that than iterate over a non empty subtract
  // set. Testing the subtract set is empty the iteration below will
  // deal with it efficiently.
  if (out.empty()) {
    return;
  }

  // This used to use set_difference but that causes us to use a temp
  // NUNetRefSet that gets allocated and deallocated. This was part of
  // a performance problem. So now we iterate over the subtract set
  // and remove the the output set.
  for (NUNetRefSet::const_iterator i = one.begin(); i != one.end(); ++i) {
    // Find if there is an overlaping net ref in the resulting set
    const NUNetRefHdl sub = *i;
    NUNetRefSet::iterator f = out.find(sub, &NUNetRef::overlapsSameNet);
    if (f != out.end()) {
      // It exists, if they are the same then just erase it. Otherwise
      // we need to do an subtract operation.
      const NUNetRefHdl res = *f;
      if (res == sub) {
        out.erase(f);
      } else {
        NUNetRefHdl diff = out.getFactory()->subtract(res, sub);
        out.erase(f);
        out.insert(diff);
      }
    }
  }
}

bool NUNetRefSet::operator==(const NUNetRefSet& nrs) const {
  // This implementation must be used rather than directly using
  // UtSet::operator==, because NetRefSetCompare will say they are the
  // same if the NUNet* is the same.  We need to make sure the bits
  // are the same as well.
  if (size() != nrs.size()) {
    return false;
  }
  NetRefSet::const_iterator p = mSet.begin();
  NetRefSet::const_iterator q = nrs.mSet.begin();
  for (NetRefSet::const_iterator e = mSet.end(); p != e; ++p, ++q) {
    const NUNetRefHdl& k1 = *p;
    const NUNetRefHdl& k2 = *q;
    if (k1 != k2) {
      return false;
    }
  }
  return true;
}

NUNetRefHdl NUNetRefSet::findNet(NUNet* net) {
  NUNetRefHdl findRef = mFactory->createNetRef(net);
  NetRefSet::iterator p = mSet.find(findRef);
  if (p != mSet.end()) {
    return *p;
  }
  return mFactory->createEmptyNetRef();
}
#endif
