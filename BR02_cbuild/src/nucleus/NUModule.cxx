// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/CarbonAssert.h"
#include "util/UtString.h"        // for UtString concat overrides
#include "util/UtVector.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUExtNet.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUTaskLevels.h"
#include "nucleus/NUControlFlowNet.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUCModelInterface.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "util/UtIOStream.h"
#include "util/OSWrapper.h"
#include "util/StringAtom.h"
#include "util/CbuildMsgContext.h"
#include "util/HierStringName.h"
#include "nucleus/NUNetSet.h"
#include "iodb/IODBNucleus.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUCModel.h"
#include "nucleus/NUSysTask.h"
#include "util/UtIndent.h"
#include "hdl/HdlId.h"
#include "hdl/HdlTime.h"
#include "hdl/HdlVerilogPath.h"

NUModule::NUModule(NUDesign *design,
                   bool isTopLevel,
                   StringAtom* name,
                   STSymbolTable * aliasDB,
                   NUNetRefFactory *netref_factory,
                   AtomicCache *string_cache,
                   const SourceLocator& loc,
                   IODBNucleus *iodb,
                   SInt8 timeUnit,
                   SInt8 timePrecision,
                   bool timescaleSpecified,
                   HierFlags sourceLanguage) :
  NUScope( iodb, HierFlags((sourceLanguage & eLanguageMask) | eHTModule) ),
  mName(name),
  mAliasDB(aliasDB),
  mLoc(loc),
  mNetRefFactory(netref_factory),
  mMySymtabIndex(isTopLevel? design->reserveSymtabIndex(): -1),
  mAtTopLevel(isTopLevel),
  mTimeUnit(timeUnit),
  mTimePrecision(timePrecision),
  mDesign(design)
{
  commonInit(string_cache, timescaleSpecified);
}


void NUModule::commonInit(AtomicCache *string_cache, bool timescaleSpecified)
{
  mClassDataSize = 0;
  mClassDataAlignment = 0;
  mClassInstDataSize = 0;
  mCurSymtabIndex = 0;
  mRunsSystemFunctionsOfTimeGroup = false;
  mRunsSystemTasksOfRandomGroup = false;
  mRunsSystemTasksOfOutputGroup = false;
  mRunsVerilogSystemTasksOfOutputGroup = false;
  mRunsSystemTasksOfInputGroup = false;
  mRunsSystemTasksOfControlGroup = false;
  StringAtom* sym = string_cache->intern("$extnet");
  mExtNet = new NUExtNet(sym, NetFlags(eTempNet|eAllocatedNet), this, mLoc);
  StringAtom* ostNetName = string_cache->intern("$ostnet"); // output system task
  mOutputSysTaskNet = new NUSysTaskNet(ostNetName, NetFlags(eTempNet|eAllocatedNet), this, mLoc);
  StringAtom* controlFlowNetName = string_cache->intern("$cfnet");
  mControlFlowNet = new NUControlFlowNet(controlFlowNetName, NetFlags(eTempNet|eAllocatedNet), this, mLoc);
  mPortNetRefDefMap = new NUNetRefNetRefMultiMap(mNetRefFactory);

  mOriginalName = mName;

  mFlags = 0;
  if (timescaleSpecified) {
    mFlags |= eTimescaleSpecified;
  }
  mAsyncResetNets = NULL;
}


NUModule::~NUModule()
{
  removeBlocks();

  for (NUTaskLoop loop = loopTasks(); not loop.atEnd(); ++loop) {
    NUTask* task = *loop;
    delete task;
  }

  while (not mDeclarationScopeList.empty()) {
    // deleting the scope removes it from the list.
    NUNamedDeclarationScope * scope = (*mDeclarationScopeList.begin());
    delete scope;
  }

  while (not mPorts.empty()) {
    mNetRefFactory->erase(mPorts.back());
    delete mPorts.back();
    mPorts.pop_back();
  }

  while (not mLocalNetList.empty()) {
    mNetRefFactory->erase(*(mLocalNetList.begin()));
    delete *(mLocalNetList.begin());
    mLocalNetList.erase(mLocalNetList.begin());
  }

  while (not mNetHierRefList.empty()) {
    mNetRefFactory->erase(*(mNetHierRefList.begin()));
    delete *(mNetHierRefList.begin());
    mNetHierRefList.erase(mNetHierRefList.begin());
  }

  mNetRefFactory->erase(mControlFlowNet);
  delete mControlFlowNet;

  mNetRefFactory->erase(mOutputSysTaskNet);
  delete mOutputSysTaskNet;

  mNetRefFactory->erase(mExtNet);
  delete mExtNet;

  delete mAliasDB->getHdlHier();
  delete mAliasDB;
  delete mPortNetRefDefMap;

  if (mAsyncResetNets != NULL) {
    delete mAsyncResetNets;
  }
}

void NUModule::removeBlocks() {
  clearUseDef();

  while (not mInitialBlockList.empty()) {
    delete *(mInitialBlockList.begin());
    mInitialBlockList.erase(mInitialBlockList.begin());
  }

  while (not mContAssignList.empty()) {
    delete *(mContAssignList.begin());
    mContAssignList.erase(mContAssignList.begin());
  }

  while (not mTriRegInitList.empty()) {
    delete *(mTriRegInitList.begin());
    mTriRegInitList.erase(mTriRegInitList.begin());
  }

  while (not mModuleInstanceList.empty()) {
    delete *(mModuleInstanceList.begin());
    mModuleInstanceList.erase(mModuleInstanceList.begin());
  }

  for (NUNamedDeclarationScopeLoop loop = loopDeclarationScopes();
       !loop.atEnd(); ++loop) {
    // Remove and delete the instances from scopes first. This way if they
    // use nets from other declaration scopes, the order in which declaration
    // scopes are deleted doesn't matter.
    (*loop)->deleteAllInstances();
  }

  // delete always blocks such that priority blocks are deleted before
  // blocks referring to those priority blocks.
  NUAlwaysBlockSet deleted_always_blocks;

  for (AlwaysBlockLoop loop = loopAlwaysBlocks();
       not loop.atEnd();
       ++loop) {
    NUAlwaysBlock * always = (*loop);

    // find the priority chain in order.
    NUAlwaysBlockVector priority_chain;
    do {
      if (deleted_always_blocks.find(always) != deleted_always_blocks.end()) {
        break; // a referrer caused this always block to get deleted.
      }
      priority_chain.push_back(always);
      deleted_always_blocks.insert(always);
    } while((always = always->getPriorityBlock()) != NULL);

    // start with the end of the priority chain and delete in order.
    for (NUAlwaysBlockVector::reverse_iterator riter = priority_chain.rbegin();
         riter != priority_chain.rend();
         ++riter) {
      delete (*riter);
    }
  }
  mAlwaysBlockList.clear();

  while (not mContEnabledDriverList.empty()) {
    delete *(mContEnabledDriverList.begin());
    mContEnabledDriverList.erase(mContEnabledDriverList.begin());
  }

  // Ultimately we should enable the task body removal.  Right now if
  // you enable this code then the usedef info for the taskEnable will
  // be wrong and then there will be failures in
  //    test/hierref
  //    test/langcov/Display
  //    test/hierref_unique
#if 0
  // Don't delete the tasks or their nets, they may still be
  // targets for hierarchical task-enables.  But delete the bodies of
  // the tasks so they no longer make references to anything else.
  for (NUTaskLoop loop = loopTasks(); not loop.atEnd(); ++loop) {
    NUTask* task = *loop;
    task->removeBody();
  }
#endif

  for (NUCModelLoop loop = loopCModels(); 
       not loop.atEnd();
       ++loop)
  {
    NUCModel* cmodel = *loop;
    delete cmodel;
  }
  mCModelVector.clear();
}

SInt32 NUModule::getSymtabIndex() const 
{ 
  return mMySymtabIndex; 
}

SInt32 NUModule::reserveSymtabIndex() 
{
  return mCurSymtabIndex++; 
}

StringAtom* NUModule::getName() const
{
  return mName;
}

StringAtom* NUModule::getOriginalName() const
{
  return mOriginalName;
}


void NUModule::putOriginalName(StringAtom* name)
{
  mOriginalName = name;
}


const SourceLocator& NUModule::getLoc() const
{
  return mLoc;
}


void NUModule::getPorts(NUNetList *net_list) const
{
  net_list->insert(net_list->end(), mPorts.begin(), mPorts.end());
}


int NUModule::getPortIndex(NUNet *net) const
{
  UtHashMap<NUNet*,int>::const_iterator iter = mPortIndices.find(net);
  if (iter == mPortIndices.end()) {
    return -1;
  } else {
    return iter->second;
  }
}


NUNet *NUModule::getPortByIndex(int idx) const
{
  NU_ASSERT((idx >= 0) && (idx < (int)mPorts.size()), this);
  return mPorts[idx];
}


int NUModule::getNumPorts() const
{
  return mPorts.size();
}


void NUModule::getAllTopNets(NUNetList *net_list) const
{
  net_list->insert(net_list->end(), mLocalNetList.begin(), mLocalNetList.end());
  net_list->insert(net_list->end(), mPorts.begin(), mPorts.end());
}

void NUModule::getAllTopNetsPortsFirst(NUNetList *net_list) const
{
  net_list->insert(net_list->end(), mPorts.begin(), mPorts.end());
  net_list->insert(net_list->end(), mLocalNetList.begin(), mLocalNetList.end());
}


void NUModule::getAllNestedNets(NUNetList *net_list) const
{
  for (NUNamedDeclarationScopeCLoop loop = loopDeclarationScopes(); not loop.atEnd(); ++loop) {
    (*loop)->getAllNets(net_list);
  }
  for (NUBlockCLoop loop = loopBlocks(); not loop.atEnd(); ++loop) {
    (*loop)->getAllNets(net_list);
  }
}


void NUModule::getAllHierRefNets(NUNetList *net_list) const
{
  net_list->insert(net_list->end(), mNetHierRefList.begin(), mNetHierRefList.end());
}


void gPrintNetList(NUNetList* net_list) {
  for (Loop<NUNetList> p(*net_list); !p.atEnd(); ++p) {
    NUNet* net = *p;
    UtString path;
    net->getNameLeaf()->verilogCompose(&path);
    UtIO::cerr() << "Net " << path << "\n";
  }
}

void NUModule::getAllNonTFNets(NUNetList * net_list) const
{
  getAllTopNets(net_list);
  getAllNestedNets(net_list);
  getAllHierRefNets(net_list);
 
  net_list->push_back(mExtNet);
  net_list->push_back(mOutputSysTaskNet);
  net_list->push_back(mControlFlowNet);
}


void NUModule::getAllNets(NUNetList *net_list) const
{
  getAllNonTFNets(net_list);

  // consider nets inside of tasks of this module  
  for (NUTaskCLoop loop = loopTasks(); not loop.atEnd(); ++loop) {
    (*loop)->getAllNets(net_list);
  }
}


void NUModule::getFaninStartNets(NUNetList *start_list, IODBNucleus *iodb)
{
  NUNetList net_list;  // list of nets to be considered for addition to start_list

  getAllNets(&net_list);        // consider nets in this module

  // add to start_list any nets that are output/bid/protectedobservable
  for (NUNetListIter iter = net_list.begin();
       iter != net_list.end();
       ++iter) {
    NUNet *net = *iter;
    if (net->isOutput() or
        net->isBid()    or
        net->isProtectedObservable(iodb))
    {
      start_list->push_back(net);
    }
  }
}



void NUModule::getContAssigns(NUContAssignList* assign_list) const
{
  assign_list->insert(assign_list->end(), mContAssignList.begin(), mContAssignList.end());
}

void NUModule::getTriRegInit(NUTriRegInitList* init_list) const
{
  init_list->insert(init_list->end(), mTriRegInitList.begin(), mTriRegInitList.end());
}

void NUModule::getContEnabledDrivers(NUEnabledDriverList* driver_list) const
{
  driver_list->insert(driver_list->end(), mContEnabledDriverList.begin(), mContEnabledDriverList.end());
}

void NUModule::getAlwaysBlocks(NUAlwaysBlockList* always_list) const
{
  always_list->insert(always_list->end(), mAlwaysBlockList.begin(), mAlwaysBlockList.end());
}

void NUModule::getInitialBlocks(NUInitialBlockList* initial_list) const
{
  initial_list->insert(initial_list->end(), mInitialBlockList.begin(), mInitialBlockList.end());
}

void NUModule::getStructuredProcs(NUStructuredProcList * struct_list) const
{
  struct_list->insert(struct_list->end(), mAlwaysBlockList.begin(), mAlwaysBlockList.end());
  struct_list->insert(struct_list->end(), mInitialBlockList.begin(), mInitialBlockList.end());
}

void NUModule::getBlocks(NUBlockList* block_list) const
{
  block_list->insert(block_list->end(), 
                     mBlockList.begin(),
                     mBlockList.end());
}


void NUModule::getDeclarationScopes(NUNamedDeclarationScopeList * scope_list) const
{
  scope_list->insert(scope_list->end(), 
                     mDeclarationScopeList.begin(),
                     mDeclarationScopeList.end());
}


void NUModule::addDef(const NUNetRefHdl &net_ref)
{
  mPortNetRefDefMap->insert(net_ref);
}


void NUModule::addUse(const NUNetRefHdl& def_net_ref, const NUNetRefHdl& use_net_ref)
{
  mPortNetRefDefMap->insert(def_net_ref, use_net_ref);
}


void NUModule::addUses(const NUNetRefHdl& def_net_ref, NUNetRefSet *use_set)
{
  for (NUNetRefSet::iterator iter = use_set->begin(); iter != use_set->end(); ++iter) {
    mPortNetRefDefMap->insert(def_net_ref, *iter);
  }
}


void NUModule::replaceDef(NUNet* old_net,
			  NUNet* /* new_net -- unused  */)
{
  // Invalid to call this
  NU_ASSERT2(0=="unimplemented", old_net, this);
}


bool NUModule::replace(NUNet * old_net,
		       NUNet * /* new_net */)
{
  NU_ASSERT2(0=="unimplemented", old_net, this);
  return false;
}

// Create a callback class to hit the continuous drivers of the module and
// call replaceLeaves() on them.
// We just want to touch each continuous driver, not traverse into them, so Skip
// is returned always.
class ModCallback : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  ModCallback(NuToNuFn &t) : mTranslator(t), mDidFixup(false) {}
  ~ModCallback() {}
  Status operator()(Phase /* phase */, NUBase * /* node */)
  {
    return eSkip;
  }
  Status operator()(Phase /* phase */, NUUseDefNode *node)
  {
    mDidFixup |= node->replaceLeaves(mTranslator);
    return eSkip;
  }
  Status operator()(Phase /* phase */, NUModule * /* node */)
  {
    return eNormal;
  }
  Status operator()(Phase /* phase */, NUNamedDeclarationScope * /* node */)
  {
    return eNormal;
  }
  NuToNuFn &mTranslator;
  bool mDidFixup;
};

bool NUModule::replaceLeaves(NuToNuFn &translator)
{
  ModCallback callback(translator);
  NUDesignWalker walker(callback, false);
  walker.module(this);
  walker.putWalkTasksFromTaskEnables(false);
  return callback.mDidFixup;
}

void NUModule::clearUseDef()
{
  mPortNetRefDefMap->clear();
}


void NUModule::fixupUseDef(NUNetList *remove_list)
{
  helperFixupUseDef(mPortNetRefDefMap, remove_list);
}


void NUModule::addPort(NUNet* net)
{
  mPorts.push_back(net);
  int idx = mPorts.size() - 1;
  addNetName(net);
  UtHashMap<NUNet*,int>::const_iterator map_iter = mPortIndices.find(net);
  NU_ASSERT(map_iter == mPortIndices.end(), net);
  mPortIndices[net] = idx;
  mIODB->addTypeIntrinsic( net );
}

void NUModule::addLocal(NUNet* net)
{
  addNetName(net);
  mLocalNetList.push_back(net);
  mIODB->addTypeIntrinsic( net );
}

void NUModule::removeLocal(NUNet* net, bool remove_references)
{
  removeNetName(net);
  mLocalNetList.remove(net);
  if (remove_references) {
    mNetRefFactory->erase(net);
  }
}

void NUModule::removeLocals(const NUNetSet& nets, bool remove_references)
{
  NUScope::removeNetsHelper(nets, remove_references, &mLocalNetList);
}

void NUModule::replacePorts(const NUNetVector& ports)
{
#ifdef CDB
  for (NUNetVectorLoop l = loopPorts(); !l.atEnd(); ++l)
  {
    NUNet* net = *l;
    removeNetName(net);
  }
#endif

  mPorts = ports;
  mPortIndices.clear();
  int idx = 0;
  for (NUNetVectorLoop l = loopPorts(); !l.atEnd(); ++l, ++idx)
  {
    NUNet* net = *l;
    mPortIndices[net] = idx;
    addNetName(net);
  }
}

void NUModule::replacePort(NUNet* net, NUNetVector& ports)
{
  NUNetVector all_ports;
  for (NUNetVectorLoop loop = loopPorts();
       not loop.atEnd();
       ++loop) {
    NUNet * port = (*loop);
    if (port==net) {
      all_ports.insert(all_ports.end(),ports.begin(),ports.end());
    } else {
      all_ports.push_back(port);
    }
  }
  replacePorts(all_ports);
}

void NUModule::localizePort(NUNet* net)
{
  NetFlags flags = net->getFlags();
  NU_ASSERT(net->isPort(), net);
  flags = NetFlags(flags & ~(eInputNet | eOutputNet | eBidNet));
  net->setFlags(flags);
#ifdef CDB
  NU_ASSERT(not hasNetName(net->getName()),net);
#endif
  addLocal(net);
}

void NUModule::addNetHierRef(NUNet *net)
{
  addNetName(net);
  mNetHierRefList.push_back(net);
}


void NUModule::removeNetHierRef(NUNet* net, bool remove_references)
{
  removeNetName(net);
  mNetHierRefList.remove(net);
  if (remove_references) {
    mNetRefFactory->erase(net);
  }
}


void NUModule::getInstantiatedModules(NUModuleList *module_list) const
{
  UtSet<NUModule*> moduleSet;
  // Get instantiated modules for instances within this module, including
  // those in declaration scopes.
  for (NUModuleInstanceMultiCLoop iter = loopInstances(); !iter.atEnd(); ++iter) {
    NUModule* module = (*iter)->getModule();
    if (moduleSet.find(module) == moduleSet.end()) {
      moduleSet.insert(module);
      module_list->push_back(module);
    }
  }
}

void NUModule::addModuleInstance(NUModuleInstance* instance)
{
  mModuleInstanceList.push_back(instance);
}


void NUModule::addContAssign(NUContAssign* assign)
{
  mContAssignList.push_back(assign);
}


void NUModule::addTriRegInit(NUTriRegInit* init)
{
  mTriRegInitList.push_back(init);
}


void NUModule::addAlwaysBlock(NUAlwaysBlock* block)
{
  mAlwaysBlockList.push_back(block);
}


void NUModule::addInitialBlock(NUInitialBlock* block)
{
  mInitialBlockList.push_back(block);
}


void NUModule::addContEnabledDrivers(NUEnabledDriverList *driver_list)
{
  mContEnabledDriverList.insert(mContEnabledDriverList.begin(),
			    driver_list->begin(),
			    driver_list->end());
}


void NUModule::addBlock(NUBlock * block)
{
  mBlockList.push_back(block);
}


void NUModule::addDeclarationScope(NUNamedDeclarationScope * scope)
{
  mDeclarationScopeList.push_back(scope);
}


void NUModule::addTask(NUTask *task)
{
  mTaskVector.push_back(task);
}


void NUModule::addCModel(NUCModel* cmodel)
{
  mCModelVector.push_back(cmodel);
}

void NUModule::removeCModel(NUCModel* cmodel, NUCModel* rep)
{
  for (UInt32 i = 0, n = mCModelVector.size(); i < n; ++i) {
    if (mCModelVector[i] == cmodel) {
      if (rep != NULL) {
        mCModelVector[i] = rep;
      }
      else {
        // Swap with the last
        UInt32 last = n - 1;
        if (i != last) {
          mCModelVector[i] = mCModelVector[last];
        }
        mCModelVector.resize(last);
        return;
      }
    }
  }
  NU_ASSERT(0, cmodel);
}

bool NUModule::hasInstances() const
{
  bool foundInstances = false;
  if (not mModuleInstanceList.empty()) {
    foundInstances = true;
  }
  for (NUNamedDeclarationScopeCLoop iter = loopDeclarationScopes();
       !foundInstances && !iter.atEnd(); ++iter) {
    foundInstances = (*iter)->hasInstances();
  }
  return foundInstances;
}

NUBlockCLoop NUModule::loopBlocks() const 
{ 
  return NUBlockCLoop(mBlockList); 
}


NUBlockLoop NUModule::loopBlocks() 
{
  return NUBlockLoop(mBlockList); 
}

NUModuleInstanceMultiLoop NUModule::loopInstances()
{
  NUModuleInstanceMultiLoop miLoop = loopModuleInstances();
  for (NUNamedDeclarationScopeLoop iter = loopDeclarationScopes();
       !iter.atEnd(); ++iter) {
    NUNamedDeclarationScope* declScope = *iter;
    miLoop.pushLoopMulti(declScope->loopInstances());
  }
  return miLoop;
}

NUModuleInstanceMultiCLoop NUModule::loopInstances() const
{
  NUModuleInstanceMultiCLoop miLoop = loopModuleInstances();
  for (NUNamedDeclarationScopeCLoop iter = loopDeclarationScopes();
       !iter.atEnd(); ++iter) {
    const NUNamedDeclarationScope* declScope = *iter;
    miLoop.pushLoopMulti(declScope->loopInstances());
  }
  return miLoop;
}

NUModuleInstanceMultiLoop NUModule::loopModuleInstances()
{
  NUModuleInstanceMultiLoop miLoop;
  miLoop.pushLoop(NUModuleInstanceListLoop(mModuleInstanceList));
  return miLoop;
}

NUModuleInstanceMultiCLoop NUModule::loopModuleInstances() const
{
  NUModuleInstanceMultiCLoop miLoop;
  miLoop.pushLoop(NUModuleInstanceListCLoop(mModuleInstanceList));
  return miLoop;
}

NUNamedDeclarationScopeCLoop NUModule::loopDeclarationScopes() const 
{ 
  return NUNamedDeclarationScopeCLoop(mDeclarationScopeList); 
}


NUNamedDeclarationScopeLoop NUModule::loopDeclarationScopes() 
{
  return NUNamedDeclarationScopeLoop(mDeclarationScopeList); 
}


void NUModule::removeModuleInstance(NUModuleInstance* instance, NUModuleInstance* replacement)
{
  mModuleInstanceList.remove(instance);
  if (replacement) {
    mModuleInstanceList.push_back(replacement);
  }
}


void NUModule::removeContAssign(NUContAssign* assign, NUContAssign* replacement)
{
  mContAssignList.remove(assign);
  if (replacement) {
    mContAssignList.push_back(replacement);
  }
}


void NUModule::removeTriRegInit(NUTriRegInit* init, NUTriRegInit *replacement)
{
  mTriRegInitList.remove(init);
  if (replacement) {
    mTriRegInitList.push_back(replacement);
  }
}


void NUModule::removeAlwaysBlock(NUAlwaysBlock * block, NUAlwaysBlock *replacement)
{
  mAlwaysBlockList.remove(block);
  if (replacement) {
    mAlwaysBlockList.push_back(replacement);
  }
}


void NUModule::removeInitialBlock(NUInitialBlock * block, NUInitialBlock *replacement)
{
  mInitialBlockList.remove(block);
  if (replacement) {
    mInitialBlockList.push_back(replacement);
  }
}


void NUModule::removeContEnabledDriver(NUEnabledDriver *driver, NUEnabledDriver *replacement)
{
  mContEnabledDriverList.remove(driver);
  if (replacement) {
    mContEnabledDriverList.push_back(replacement);
  }
}


void NUModule::removeBlock(NUBlock * block)
{
  mBlockList.remove(block);
}


void NUModule::removeDeclarationScope(NUNamedDeclarationScope * scope)
{
  mDeclarationScopeList.remove(scope);
}


void NUModule::removeTask(NUTask* task, NUTask *replacement)
{
  for (NUTaskVector::iterator iter = mTaskVector.begin();
       iter != mTaskVector.end();
       ++iter) {
    if (*iter == task) {
      mTaskVector.erase(iter);
      break;
    }
  }
  if (replacement) {
    mTaskVector.push_back(replacement);
  }
}


void NUModule::replaceInitialBlocks(const NUInitialBlockList& block_list)
{
  mInitialBlockList.assign(block_list.begin(), block_list.end());
}


void NUModule::replaceAlwaysBlocks(const NUAlwaysBlockList& block_list)
{
  mAlwaysBlockList.assign(block_list.begin(), block_list.end());
}


void NUModule::replaceBlocks(const NUBlockList& block_list)
{
  mBlockList.assign(block_list.begin(), block_list.end());
}


void NUModule::replaceDeclarationScopes(const NUNamedDeclarationScopeList& scope_list)
{
  mDeclarationScopeList.assign(scope_list.begin(), scope_list.end());
}


void NUModule::replaceContAssigns(const NUContAssignList& assign_list)
{
  mContAssignList.assign(assign_list.begin(), assign_list.end());
}

void
NUModule::replaceContEnabledDrivers(const NUEnabledDriverList& driver_list)
{
  mContEnabledDriverList.assign(driver_list.begin(), driver_list.end());
}

void NUModule::getUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUModule::getUses(NUNetRefSet *uses) const
{
  mPortNetRefDefMap->populateMappedToNonEmpty(uses);
}


bool NUModule::queryUses(const NUNetRefHdl &use_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory * /* factory -- unused */) const
{
  return mPortNetRefDefMap->queryMappedTo(use_net_ref, fn);
}


void NUModule::getUses(NUNet* net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUModule::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  mPortNetRefDefMap->populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUModule::queryUses(const NUNetRefHdl &def_net_ref,
                         const NUNetRefHdl &use_net_ref,
                         NUNetRefCompareFunction fn,
                         NUNetRefFactory * /* factory -- unused */) const
{
  return mPortNetRefDefMap->queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}


void NUModule::getDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin(); iter != ref_defs.end(); ++iter) {
    defs->insert((*iter)->getNet());
  }
}


void NUModule::getDefs(NUNetRefSet *defs) const
{
  mPortNetRefDefMap->populateMappedFrom(defs);
}


bool NUModule::queryDefs(const NUNetRefHdl &def_net_ref,
			 NUNetRefCompareFunction fn,
			 NUNetRefFactory * /* factory -- unused */) const
{
  return mPortNetRefDefMap->queryMappedFrom(def_net_ref, fn);
}


NUModuleElab::NUModuleElab(NUModule *module, NUModuleInstance *inst,
                           STBranchNode *hier) :
    NUScopeElab(module, hier), mInst(inst)
{
}

NUModuleElab::~NUModuleElab()
{
#if 0
  // Delete all instantiated elaborated modules.
  for (NUModule::InstanceLoop iter = mModule->loopModuleInstances();
       !iter.atEnd();
       ++iter)
  {
    NUModuleInstance *inst = *iter;
    NUModuleElab *module_elab = inst->lookupElab(getHier());
    if (module_elab != 0) {
      delete module_elab;
    }
  }

  // Delete all locally elaborated nets.  Note that block-locals will be handled
  // by their parent named blocks.
  for (NUModule::NetLoop net_loop = mModule->loopLocals();
       not net_loop.atEnd();
       ++net_loop) {
    NUNet *net = *net_loop;
    // Aliased nets do not have a corresponding NUNetElab
    if (not net->isAliased()) {
      delete net->lookupElab(getHier());
    }
  }
  NUNetList ports;
  mModule->getPorts(&ports);
  for (NUNetListIter iter = ports.begin(); iter != ports.end(); ++iter) {
    NUNet *net = *iter;
    // Aliased nets do not have a corresponding NUNetElab
    if (not net->isAliased()) {
      delete net->lookupElab(getHier());
    }
  }
#endif
} // NUModuleElab::~NUModuleElab


NUModuleElab *NUModule::lookupElab(STSymbolTable *symtab) const
{
  NU_ASSERT(mMySymtabIndex != -1, this);


  STSymbolTable::RootIter iter = symtab->getRootIter();
  STSymbolTableNode *root = *iter;
  int index = 0;
  while (not iter.atEnd() && (index++ < mMySymtabIndex)) 
  {
    root = *iter;
    ++iter;
  }

  if (iter.atEnd()) {
    return 0;
  }

  NU_ASSERT(root, this);
  STBranchNode *root_hier = root->castBranch();
  ST_ASSERT(root_hier, root);
  NUModuleElab *elab = NUModuleElab::lookup(root_hier);
  ST_ASSERT(elab, root_hier);
  return elab;
}

NUModule* NUModule::lookup(const STBranchNode *hier, bool lookupParent)
{
  return lookup(const_cast<STBranchNode*>(hier), lookupParent);
}


NUModule* NUModule::lookup(STBranchNode *hier, bool lookupParent)
{
  NUBase* base = CbuildSymTabBOM::getNucleusObj(hier);
  if (not base) {
    return 0;
  }

  // This find works for unelaborated and elaborated symbol tables.
  // So, try to match with both unelaborated and elaborated modules.
  NUModule *module = dynamic_cast<NUModule*>(base);
  if (not module) {
    NUModuleElab *module_elab = dynamic_cast<NUModuleElab*>(base);
    if (module_elab) {
      module = module_elab->getModule();
    }
  }
  // This could be a named declaration scope. Look for parent module if
  // lookupParent is set.
  if ((module == NULL) && lookupParent) {
    NUNamedDeclarationScope* declScope = dynamic_cast<NUNamedDeclarationScope*>(base);
    if (not declScope) {
      NUNamedDeclarationScopeElab* declScopeElab =
        dynamic_cast<NUNamedDeclarationScopeElab*>(base);
      if (declScopeElab) {
        declScope = declScopeElab->getScope();
      }
    }
    if (declScope) {
      module = declScope->getModule();
    }
  }

  return module;
}


//! Return the STBranchNode for the module parent of the supplied node
const STBranchNode *
NUModule::getParentModuleBranch( const STBranchNode *branch )
{
  NUModule *module = NUModule::lookup( branch );
  if ( module == NULL )
  {
    // If branch is not a module, it better have a parent
    const STBranchNode *parent = branch->getParent();
    ST_ASSERT( parent != NULL, branch);
    branch = NUModule::getParentModuleBranch( parent );
  }
  return branch;
}


const char* NUModule::typeStr() const
{
  return "NUModule";
}

void NUModule::printFlagsToBuf(UtString* buf) const
{
  char flag_buf [15];
  sprintf(flag_buf, "(%08x) ", mFlags);
  *buf << flag_buf; // don't use a 0x prefix here even though this is a hex value because cds_diff will ignore values that look like addresses

  if (isEnableOutputSysTasks())  { *buf << " EnableOutputSysTasks";}
  if (isDisableOutputSysTasks()) { *buf << " DisableOutputSysTasks";}
  if (isNoAsyncResets())         { *buf << " NoAsyncResets";}
  if (isHidden())                { *buf << " Hidden";}
  if (isTimescaleSpecified())    { *buf << " TimescaleSpecified";}
  if (isFlattenAllow())          { *buf << " FlattenAllow";}
  if (isFlattenContents())       { *buf << " FlattenContents";}
  if (isFlattenModule())         { *buf << " FlattenModule";}
  if (isFlattenDisallow())       { *buf << " FlattenDisalllow";}
}

void NUModule::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtString flagsBuf;
  printFlagsToBuf(&flagsBuf);
  UtString timescaleBuf;
  
  if (isTimescaleSpecified()){
    timescaleBuf << " ";
    timescaleBuf << HdlTime::VerilogTimeUnits[15 + (getTimeUnit())];
    timescaleBuf << "/";
    timescaleBuf << HdlTime::VerilogTimeUnits[15 + (getTimePrecision())];
    timescaleBuf << " ";
  }

  
  UtIO::cout() << "Module(" << this << ") : " << mName->str() << "  : " << flagsBuf << timescaleBuf << UtIO::endl;
  
  for (NUNetVectorIter iter = mPorts.begin();
       iter != mPorts.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUNetListIter iter = mLocalNetList.begin();
       iter != mLocalNetList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUNetListIter iter = mNetHierRefList.begin();
       iter != mNetHierRefList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUNamedDeclarationScopeCLoop loop = loopDeclarationScopes(); not loop.atEnd(); ++loop) {
    (*loop)->print(true, numspaces+2);
  }
  for (NUTaskCLoop loop = loopTasks(); not loop.atEnd(); ++loop) {
    (*loop)->print(true, numspaces+2);
  }
  for (NUInitialBlockListIter iter = mInitialBlockList.begin();
       iter != mInitialBlockList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUContAssignListIter iter = mContAssignList.begin();
       iter != mContAssignList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUTriRegInitListIter iter = mTriRegInitList.begin();
       iter != mTriRegInitList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUEnabledDriverListIter iter = mContEnabledDriverList.begin();
       iter != mContEnabledDriverList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUAlwaysBlockListIter iter = mAlwaysBlockList.begin();
       iter != mAlwaysBlockList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }
  for (NUModuleInstanceListIter iter = mModuleInstanceList.begin();
       iter != mModuleInstanceList.end();
       iter++) {
    (*iter)->print(true, numspaces+2);
  }

  printFlow(numspaces);

  if (recurse) {
    NUModuleList moduleList;
    getInstantiatedModules(&moduleList);
    for (NUModuleListIter iter = moduleList.begin();
	 iter != moduleList.end();
	 iter++) {
      (*iter)->print(true, numspaces);
    }
  }
}


//! Local helper function to print fanin relationships for the given flow node.
/*!
  \param fl_node The flow node for which to print fanin.
  \param seen_set Set of flow nodes we have already seen on traversal.
  \param indent Number of spaces to indent.
 */
static void helperPrintElabFanin(FLNodeElab *fl_node,
				 FLNodeElabSet *seen_set,
				 int indent)
{
  FLNodeElabIter *fanin_iter =
    fl_node->makeFaninIter(FLIterFlags::eAll,
                           FLIterFlags::eNone,
                           FLIterFlags::eOverAll,
                           true,
                           FLIterFlags::eIterNodeOnce);
  for (; not fanin_iter->atEnd(); fanin_iter->next()) {
    FLNodeElab* fanin = **fanin_iter;
    if (fanin_iter->getCurParent()) {
      for (int i = 0; i < indent+2; i++) {
        UtIO::cout() << " ";
      }
      UtIO::cout() << "(faninelab " << fanin << " " << fanin_iter->getCurParent() << ")" << UtIO::endl;
    }
    if (seen_set->find(fanin) != seen_set->end()) {
      fanin_iter->doNotExpand();
      continue;
    }
    seen_set->insert(fanin);
    fanin->ppPrint(indent+2);
    if (fanin_iter->getCurFlags() & FLIterFlags::eHasNestedAny) {
      for (FLNodeElabLoop loop = fanin->loopNested(); !loop.atEnd(); ++loop) {
        helperPrintElabFanin(*loop, seen_set, indent+2);
      }
    }
  }
  delete fanin_iter;
}


//! Local helper function to print fanin relationships for the list of nets.
/*!
  \param net_list List of nets.
  \param hier Hierarchy in which to lookup net elaborations.
  \param indent Number of spaces to indent.
 */
static void helperPrintElabConnectivity(const NUNetList &net_list,
                                        STBranchNode *hier,
                                        int indent)
{
  FLNodeElabSet seen_set;
  for (NUNetListIter iter = net_list.begin();
       iter != net_list.end();
       iter++)
  {
    for (int i = 0; i < indent; i++) {
      UtIO::cout() << " ";
    }
    NUNetElab *elab = (*iter)->lookupElab(hier, true);
    if (not elab) {
      continue;
    }

    UtIO::cout() << "Iterating over the fanin for:  ";
    elab->print(false, indent+2);

    FLNetElabIter *fanin_iter = elab->makeFaninIter(FLIterFlags::eAll,
						    FLIterFlags::eAll,
						    FLIterFlags::eOverAll,
						    FLIterFlags::eIterNodeOnce);
    for (; not fanin_iter->atEnd(); fanin_iter->next()) {
      helperPrintElabFanin(**fanin_iter, &seen_set, indent);
    }
    delete fanin_iter;
  }
}


NUModule* NUModuleElab::getModule() const
{
  NUModule *module = dynamic_cast<NUModule*>(mScope);
  if (mInst)
    NU_ASSERT(module, mInst);
  else
    INFO_ASSERT (module, "bad module elaboration");
  return module;
}


const char* NUModuleElab::typeStr() const
{
  return "NUModuleElab";
}


void NUModuleElab::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);

  UtIO::cout() << "NUModuleElab ";
  UtString buf;
  mSymNode->compose(&buf);
  UtIO::cout() << buf;
  UtIO::cout() << UtIO::endl;
  if (recurse)
    getModule()->print(recurse, numspaces+2);
}


bool NUModuleElab::atTopLevel() const
{
  return (mSymNode->getParent() == 0);
}


//! Realise NUScopeElab::getSourceLocation virtual method
/*virtual*/ const SourceLocator NUModuleElab::getSourceLocation () const
{
  if (mInst != NULL) {
    return mInst->getLoc ();
  } else {
    return getModule ()->getLoc ();
  }
}


//! Realise NUElabBase::findUnelaborated virtual method
/*virtual*/ bool NUModuleElab::findUnelaborated (STSymbolTable *unelabStab, 
  STSymbolTableNode **unelab) const
{
  // Modules are top-level entities in the unelaborated symbol table so form a
  // name consisting of just the module name and seek that in the stab.
  HierStringName tmp (getModule ()->getName ());
  *unelab = unelabStab->safeLookup (&tmp);
  return (*unelab != NULL);
}


void NUModuleElab::printElab(STSymbolTable *symtab, bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  UtIO::cout() << "ModuleElab(" << this << ") :  Module(" << getModule()
       << ")  BranchNode(" << mSymNode << ")" << UtIO::endl;

  if (atTopLevel()) {
    NUNetList nets;
    getModule()->getAllNets(&nets);
    helperPrintElabConnectivity(nets, getHier(), numspaces);
  }

  if (recurse) {
    for (NUModuleInstanceMultiLoop iter = getModule()->loopModuleInstances();
         !iter.atEnd();
         ++iter)
    {
      (*iter)->lookupElab(getHier())->printElab(symtab, true, numspaces+2);
    }
  }
}


NUModuleElab* NUModuleElab::lookup(const STBranchNode *hier)
{
  return lookup(const_cast<STBranchNode*>(hier));
}


NUModuleElab* NUModuleElab::lookup(STBranchNode *hier)
{
  NUModuleElab *elab = dynamic_cast<NUModuleElab*>(lookupScope(hier));
  return elab;
}

//! Function to compare two modules
int NUModule::compare(const NUModule* m1, const NUModule* m2)
{
  return strcmp(m1->getName()->str(), m2->getName()->str());
}

//! sorting helper function so you can make UtHashSets and UtHashMaps of modules
bool NUModule::operator<(const NUModule& other) const {
  return strcmp(getName()->str(), other.getName()->str()) < 0;
}


STSymbolTableNode* NUModule::resolveHierRefLocally(const AtomArray& path,
                                                   UInt32 pathIndex)
  const
{
  // See if that name is resolvable in this symbol table.  This can
  // occur if the net/task which is being hierarchically-referenced
  // has been flattened into this module.
  return NUHierRef::resolveHierRef(path,
                                   pathIndex,
                                   mAliasDB,
                                   0,    // start at symtab root (this module)
                                   this);
}


STSymbolTableNode*
NUModule::resolveHierRefRecurse(const AtomArray& path,
                                UInt32 pathIndex,
                                NUModuleInstanceVector* instPath)
  const
{
  // First, try to resolve within this module only.
  STSymbolTableNode *node = resolveHierRefLocally(path, pathIndex);
  if (node) {
    return node;
  }

  // Need to try looking at modules which this module instantiates.
  NU_ASSERT(pathIndex < path.size(), this);
  StringAtom* first_part = path[pathIndex];
  ++pathIndex;

  // If no more path left, we have failed.
  if (pathIndex >= path.size()) {
    if (instPath != NULL) instPath->clear();
    return 0;
  }

  // Try to match the first part of the path against an instantiated
  // module.  If that succeeds, recurse into the instantiated module
  // to continue the search.
  for (NUModuleInstanceMultiCLoop loop = loopModuleInstances(); not loop.atEnd(); ++loop) {
    NUModuleInstance *inst = *loop;
    if (first_part == inst->getName()) {
      if (instPath != NULL)
        instPath->push_back(inst);
      return inst->getModule()->resolveHierRefRecurse(path, pathIndex, instPath);
    }
  }

  // If the first part matches the module name, then recurse with
  // the rest of the path.
  // Note that we do not put this module on the instance path in this case.
  if (first_part == getName()) {
    return resolveHierRefRecurse(path, pathIndex, instPath);
  }

  if (instPath != NULL) instPath->clear();
  return 0;
}


NUNet* NUModule::findNetHierRef(const AtomArray& path ) const
{
  for (NUNetCLoop loop = loopNetHierRefs(); not loop.atEnd(); ++loop) {
    NUNet* net = *loop;
    NUHierRef* href = net->getHierRef();
    NU_ASSERT(href, net);
    if (href->getPath() == path) {
      return net;
    }
  }

  return NULL;
}


NUNet *NUModule::findNetHierRef(NUNet *net) const
{
  NUNet *found_net = 0;

  // If the given net is a hierref, look through all the possible resolutions of it
  // to find a hierref which is in this module.
  if (net->isHierRef()) {
    // NOTE: Richard and Joe have reviewed this code, and we are not certain if it
    // is correct or not.  It stops at the first hierref from this module that it
    // matches.  Is that ok?  Or should it look at all, and only allow one to exist?
    // Or, should it do something else entirely?
    for (NUNetHierRef::NetVectorLoop resolutions = net->getHierRef()->loopResolutions();
         not resolutions.atEnd() and not found_net;
         ++resolutions) {
      NUNet* net = dynamic_cast<NUNet*>(*resolutions);
      for (NUNetVectorLoop refs = net->loopHierRefs();
           not refs.atEnd() and not found_net;
           ++refs) {
        if ((*refs)->getScope()->getModule() == this) {
          found_net = *refs;
        }
      }
    }
  } else {
    // Otherwise, the given net is not a heirref, try to find a hierref pointing to it
    // in this module's scope.
    for (NUNetVectorLoop loop = net->loopHierRefs();
         not loop.atEnd() and not found_net;
         ++loop) {
      NUNet *net_hier_ref = *loop;
      if (net_hier_ref->getScope()->getModule() == this) {
        found_net = net_hier_ref;
      }
    }
  }

  return found_net;
}


template<class Iter>
static void sComposeUseDefs(Iter loop, UtString* buf, const STBranchNode* scope,
                            int numSpaces, const char* label)
{
  bool first = true;
  for (typename Iter::value_type node; loop(&node);)
  {
    if (first)
    {
      *buf << "\n";
      buf->append(numSpaces, ' ');
      *buf << "// " << label << "\n";
      first = false;
    }
    node->compose(buf, scope, numSpaces, true);
  }
}


static void sComposeNetDecl(UtString* buf, NUNet* net, UtIndent& indent, int numSpaces, SInt32 prefixCol, SInt32 varCol)
{
  buf->append(numSpaces, ' ');
  if (net->isTemp())
    *buf << "/*temp*/ reg ";
  else
    net->composeType(buf);
  indent.tab(prefixCol);
  net->composeDeclarePrefix(buf);
  indent.tab(varCol);
  *buf << net->getName()->str();
  net->composeDeclareSuffix(buf); // probably no suffix on ports...
  *buf << ";";
  indent.newline();
}

void NUModule::compose(UtString* buf, const STBranchNode* scope, int numSpaces, bool recurse)
  const
{
  UtIndent indent(buf);
  int startCol = buf->size();
  buf->append(numSpaces, ' ');

  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  // print the module name and ports
  *buf << "module " << mName->str() << "(";
  int col = buf->size() - startCol;
  for (size_t i = 0; i < mPorts.size(); ++i)
  {
    const char* portName = mPorts[i]->getName()->str();
    int len = strlen(portName);
    if (len + col > 75)
    {
      indent.newline();
      buf->append(numSpaces + 4, ' ');
      col = numSpaces + 4;
    }
    *buf << portName;
    if (i < (mPorts.size() - 1))
    {
      *buf << ", ";
      col += len + 2;
    }
  }

  // terminate the port list.
  *buf << ");";
  indent.newline();
  col = 0;

  // print the port directions
  numSpaces += 2;
  SInt32 outputRegs = 0;
  SInt32 portWiredNets = 0;
  for (size_t i = 0; i < mPorts.size(); ++i)
  {
    NUNet* port = mPorts[i];
    if (port->isReg())
      ++outputRegs;
    if (port->isWiredNet())
      ++portWiredNets;

    buf->append(numSpaces, ' ');
    if (port->isInput())
      *buf << "input ";
    else if (port->isOutput())
      *buf << "output ";
    else if (port->isBid())
      *buf << "inout ";
    else
    {
      // Input ports that were proved to always be tied to a constant
      // in every instantation can show up here.
      *buf << "/* optimized */ input ";
    }
    if (port->isSigned())
      *buf << "signed ";

    port->composeDeclaration(buf);
    indent.newline();
  }

  // print the local net declarations
  if (recurse)
  {
    const SInt32 prefixCol = 15;
    const SInt32 varCol = 30;

    if (outputRegs != 0)
    {
      indent.newline();
      buf->append(numSpaces, ' ');
      *buf << "// Output regs (" << outputRegs << ")";
      indent.newline();
      for (size_t i = 0; i < mPorts.size(); ++i)
      {
        // Print the port register declarations
        NUNet* port = mPorts[i];
        if (port->isReg())
          sComposeNetDecl(buf, port, indent, numSpaces, prefixCol, varCol);
      }
    }

    if (portWiredNets != 0)
    {
      indent.newline();
      buf->append(numSpaces, ' ');
      *buf << "// wired-net ports (" << portWiredNets << ")";
      indent.newline();
      for (size_t i = 0; i < mPorts.size(); ++i)
      {
        // Print the port register declarations
        NUNet* port = mPorts[i];
        if (port->isWiredNet())
          sComposeNetDecl(buf, port, indent, numSpaces, prefixCol, varCol);
      }
    }
    
    if (!mLocalNetList.empty())
    {
      indent.newline();
      buf->append(numSpaces, ' ');
      *buf << "// Local nets (" << mLocalNetList.size() << ")";
      indent.newline();
      NUNetLoopSorted p (loopLocals ());
      for (; !p.atEnd (); ++p)
      {
        NUNet* net = *p;
        sComposeNetDecl(buf, net, indent, numSpaces, prefixCol, varCol);
      }
    }


    sComposeUseDefs(loopDeclarationScopes(),  buf, scope, numSpaces, "Declarations");

    sComposeUseDefs(loopInitialBlocks(), buf, scope, numSpaces, "Initial Blocks");
    sComposeUseDefs(loopContAssigns(),   buf, scope, numSpaces, "Continuous Assigns");
    sComposeUseDefs(loopContEnabledDrivers(),buf, scope, numSpaces, "Continous Enabled Drivers");
    sComposeUseDefs(loopTriRegInit(),    buf, scope, numSpaces, "Tri-Reg Inits");

    NUAlwaysBlockVector ab (mAlwaysBlockList.begin (), mAlwaysBlockList.end ());
    std::sort (ab.begin (), ab.end (), CmpAlwaysBlocks ());

    sComposeUseDefs(CLoop<NUAlwaysBlockVector> (ab),  buf, scope, numSpaces, "Always Blocks");
    sComposeUseDefs(loopTasks(),         buf, scope, numSpaces, "Tasks");
    sComposeUseDefs(loopModuleInstances(),     buf, scope, numSpaces, "Instances");
    sComposeUseDefs(loopCModels(),       buf, scope, numSpaces, "CModels");
  } // if

  numSpaces -= 2;
  buf->append(numSpaces, ' ');
  *buf << "endmodule // " << mName->str() << "\n";
} // void NUModule::compose

NUModule::NetLoop NUModule::loopNets()
{
  return NetLoop(LclNetLoop(loopPorts(), loopLocals()), loopNetHierRefs());
}

NUModule::NetCLoop NUModule::loopNets() const
{
  return NetCLoop(LclNetCLoop(loopPorts(), loopLocals()), loopNetHierRefs());
}

//! declare that this module runs a system task of the "Time" group
void NUModule::putRunsSystemFunctionsOfTimeGroup(bool flag)
{
  mRunsSystemFunctionsOfTimeGroup = flag;
}
//! declare that this module runs a system task of the "Random" group
void NUModule::putRunsSystemTasksOfRandomGroup(bool flag)
{
  mRunsSystemTasksOfRandomGroup = flag;
}
//! declare that this module runs a system task of the "Output" group, and if verilog specific
void NUModule::putRunsSystemTasksOfOutputGroup(bool flag, bool verilog_flag)
{
  mRunsSystemTasksOfOutputGroup = flag || verilog_flag;
  mRunsVerilogSystemTasksOfOutputGroup |= verilog_flag;
}
//! declare that this module runs a system task of the "Input" group
void NUModule::putRunsSystemTasksOfInputGroup(bool flag)
{
  mRunsSystemTasksOfInputGroup = flag;
}
//! declare that this module runs a system task of the "Control" group
void NUModule::putRunsSystemTasksOfControlGroup(bool flag)
{
  mRunsSystemTasksOfControlGroup = flag;
}

void NUModule::getTaskEnableHierRefs(NUTaskEnableVector* task_enables) const
{
  NUTaskEnableCallback callback(task_enables);
  NUDesignWalker walker(callback, false);
  NUModule* module = const_cast<NUModule*>(this);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(module);
}


bool NUModule::hasHierRefTasks() const 
{
  bool contains_hier_ref_tasks = false;
  for (NUTaskCLoop loop = loopTasks();
       (not contains_hier_ref_tasks) and (not loop.atEnd());
       ++loop) {
    contains_hier_ref_tasks = (*loop)->hasHierRef();
  }
  return contains_hier_ref_tasks;
}

// this is here so we are sure to get a function (not inlined)
NUNet* NUModule::getOutputSysTaskNetAsNUNet() const {
  return mOutputSysTaskNet;
}

// this is here so we are sure to get a function (not inlined)
NUNet* NUModule::getControlFlowNetAsNUNet() const {
  return mControlFlowNet;
}


NUNet *NUModule::getExtNet() const
{
  return mExtNet;
}

class FindChangeDetects : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  FindChangeDetects(NUChangeDetectVector* changeDetects, bool scheduledOnly) :
    mChangeDetects(changeDetects),
    mScheduledOnly(scheduledOnly)
  {}

  ~FindChangeDetects() {}

  //! Don't recurse into sub-modules
  Status operator()(Phase , NUModuleInstance*) { return eSkip; }

  //! Don't recurse into un-scheduled always blocks.
  Status operator()(Phase, NUAlwaysBlock* always) { 
    if (mScheduledOnly) {
      return always->getBlock()->isScheduled() ? eNormal : eSkip;
    } else {
      return eNormal;
    }
  }

  //! Don't recurse into un-scheduled continuous assignments.
  Status operator()(Phase, NUContAssign* assign) { 
    if (mScheduledOnly) {
      return assign->isScheduled() ? eNormal : eSkip;
    } else {
      return eNormal;
    }
  }

  //! Intercept expressions and see if they are change detects
  virtual Status operator()(Phase phase, NUExpr *expr)
  {
    if ((phase == ePre) && (expr->getType() == NUExpr::eNUUnaryOp)) {
      NUChangeDetect* changeDetect = dynamic_cast<NUChangeDetect*>(expr);
      if (changeDetect != NULL) {
        mChangeDetects->push_back(changeDetect);
      }
    }
    return eNormal;
  }

  //! All others just continue
  virtual Status operator()(Phase , NUBase*) { return eNormal; }

private:
  NUChangeDetectVector* mChangeDetects;
  bool mScheduledOnly;          //! Only find scheduled change detects?
}; // class FindChangeDetects : public NUDesignCallback

void NUModule::getChangeDetects(NUChangeDetectVector* changeDetects, bool scheduled_only) const
{
  FindChangeDetects callback(changeDetects, scheduled_only);
  NUDesignWalker walker(callback, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(const_cast<NUModule*>(this));
}

//! Request this module to be flattened or not
void NUModule::setFlattenContents() {
  mFlags = (mFlags & ~eFlattenMask) | eFlattenContents;
}

//! Request this module to be flattened or not
void NUModule::setFlattenModule() {
  mFlags = (mFlags & ~eFlattenMask) | eFlattenModule;
}

//! Request this module to be flattened or not
void NUModule::setFlattenDisallow() {
  mFlags = (mFlags & ~eFlattenMask) | eFlattenDisallow;
}

//! Request this module to not be flattened
void NUModule::setFlattenAllow() {
  mFlags = (mFlags & ~eFlattenMask) | eFlattenAllow;
}

//! Request this module be hidden
void NUModule::setHidden() {
  mFlags |= eHidden;
}

//! Request this module be optimized
void NUModule::setTernaryGateOptimization() {
  mFlags |= eTernaryGateOptimization;
}

//! Request this module have no async resets
void NUModule::setNoAsyncResets() {
  mFlags |= eNoAsyncResets;
}

//! Request this module have no output sys tasks
void NUModule::setDisableOutputSysTasks() {
  mFlags |= eDisableOutputSysTasks;
}
//! Request this module have output sys tasks
void NUModule::setEnableOutputSysTasks() {
  mFlags |= eEnableOutputSysTasks;
}

NUModuleElab* NUModuleElab::find(const STSymbolTableNode* node)
{
  return dynamic_cast<NUModuleElab*>(NUElabBase::find(node));
}

class FindDisplayWithHierName : public NUDesignCallback
{
public:
  //! constructor
  FindDisplayWithHierName() : mFoundDisplay(false) {}

  //! destructor
  ~FindDisplayWithHierName() {}

  //! Catch all
  Status operator()(Phase, NUBase*) { return eNormal; }

  //! Don't walk sub instances
  Status operator()(Phase, NUModuleInstance*) { return eSkip; }
 
  //! Check sys tasks
  Status operator()(Phase, NUOutputSysTask* sysTask)
  {
    if (sysTask->hasHierarchicalName()) {
      mFoundDisplay = true;
      return eStop;
    }
    return eNormal;
  }

  //! See if a display with %m was found
  bool foundDisplayWithHierName() const { return mFoundDisplay; }

private:
  bool mFoundDisplay;
}; // class FindDisplayWithInst : public NUDesignCallback


bool NUModule::hasDisplaysWithHierarchicalName() const
{
  FindDisplayWithHierName findDisplay;
  NUDesignWalker walker(findDisplay, false);
  walker.putWalkTasksFromTaskEnables(false);
  walker.module(const_cast<NUModule*>(this));
  return findDisplay.foundDisplayWithHierName();
}

//! Determine whether this module can be used for Congruence with another
//! by comparing the directive flags.  Any new fields added to NUModule
//! that would affect a modules equivalence with another one should be
//! incorporated into this routine.
bool NUModule::areFlagsCongruent(const NUModule* other) const {
  return ((mFlags == other->mFlags) &&
          (mTimeUnit == other->mTimeUnit) &&
          (mTimePrecision == other->mTimePrecision));
}

NUTaskLoop NUModule::loopTasks() {
  return NUTaskLoop::create(Loop<NUTaskVector>(mTaskVector));
}

NUTaskCLoop NUModule::loopTasks() const {
  return NUTaskLoop::create(CLoop<NUTaskVector>(mTaskVector));
}

class TaskLevelizedLoop : public Iter<NUTask*>::RefCnt {
public:
  //! lexically compare the names of two tasks
  struct CmpTasks {
    bool operator()(const NUTask* t1, const NUTask* t2) const {
      STBranchNode* b1 = t1->getNameBranch();
      STBranchNode* b2 = t2->getNameBranch();
      int cmp = HierName::compare(b1, b2);
      return cmp < 0;
    }
  };

  TaskLevelizedLoop(const NUModule* mod) {
    NUTaskLevels levels;
    levels.module(mod);

    // For each level
    for (UInt32 i = 0; i < levels.numTaskLevels(); ++i) {
      NUTaskVector level;
      for (NUTaskLevels::TaskLevelLoop p = levels.loopLevel(i); !p.atEnd(); ++p)
      {
        NUTask* task = *p;
        level.push_back(task);
      }

      // Sort the level and collect it
      std::sort(level.begin(), level.end(), CmpTasks());
      mTaskVector.insert(mTaskVector.end(), level.begin(), level.end());
    }
    mIndex = 0;
  }

  virtual ~TaskLevelizedLoop() {}
  virtual bool atEnd() const {return mIndex >= mTaskVector.size();}
  virtual void operator++() {++mIndex;}
  virtual NUTask* operator*() const {return mTaskVector[mIndex];}
  virtual bool operator()(NUTask** ptr) {
    if (mIndex >= mTaskVector.size()) {
      return false;
    }
    *ptr = mTaskVector[mIndex];
    ++mIndex;
    return true;
  }

private:
  NUTaskVector mTaskVector;
  UInt32 mIndex;
};

NUTaskLoop NUModule::loopTasksLevelized() const {
  return NUTaskLoop::create(TaskLevelizedLoop(this));
}

void NUModule::addAsyncResetNet(STSymbolTableNode* sym) {
  if (mAsyncResetNets == NULL) {
    mAsyncResetNets = new NodeSet; // STSymbolTableNodeSet;
  }
  mAsyncResetNets->insertWithCheck(sym);
}

NUModule::NodeSet::SortedLoop NUModule::loopAsyncResetNets() const {
  return mAsyncResetNets->loopSorted();
}

/*static*/ void MsgContext::composeLocation (const NUModule *module, UtString *location)
{
  if (module == NULL) {
    *location << "(null NUModule)";
  } else {
    const SourceLocator &loc = module->getLoc();
    loc.compose (location);
    *location << ": " << module->getName ()->str ();
  }
}

void NUModule::updateTempMemoryMasters(NuToNuFn & translator)
{
  NUNetList nets;
  getAllNets(&nets);
  for (NUNetList::iterator iter=nets.begin(); iter!=nets.end(); ++iter) {
    NUNet * net = (*iter);
    NUTempMemoryNet * tmem = dynamic_cast<NUTempMemoryNet*>(net);
    if (tmem) {
      NUMemoryNet * mem = tmem->getMaster();
      NUNet * new_master = translator(mem,NuToNuFn::ePre);
      if (new_master) {
        NUMemoryNet * new_master_mem = dynamic_cast<NUMemoryNet*>(new_master);
        NU_ASSERT(new_master_mem, new_master);
        tmem->putMaster(new_master_mem);
      }
    }
  }
}

void NUModule::sanityCheckAlwaysBlockPrioChain() const {
  NUAlwaysBlockSet ab_set;
  for (AlwaysBlockCLoop p(mAlwaysBlockList); !p.atEnd(); ++p) {
    NUAlwaysBlock* ab = *p;
    ab_set.insert(ab);
  }

  NUAlwaysBlockVector referers;

  for (AlwaysBlockCLoop p(mAlwaysBlockList); !p.atEnd(); ++p) {
    NUAlwaysBlock* ab = *p;
    NUAlwaysBlock* clk = ab->getClockBlock();
    if (clk != NULL) {
      NU_ASSERT2(ab_set.find(clk) != ab_set.end(), ab, clk);
      NU_ASSERT2(clk->isClockBlockReferer(ab), ab, clk);
    }
    NUAlwaysBlock* prio = ab->getPriorityBlock();
    if (prio != NULL) {
      NU_ASSERT2(ab_set.find(prio) != ab_set.end(), ab, prio);
      if (clk == NULL) {
        NU_ASSERT2(prio->getClockBlock() == ab, ab, prio);
      }
      else {
        NU_ASSERT2(prio->getClockBlock() == clk, ab, prio);
      }
    }

    referers.clear();
    ab->getClockBlockReferers(&referers);
    for (UInt32 i = 0; i < referers.size(); ++i) {
      NUAlwaysBlock* referer = referers[i];
      NU_ASSERT2(referer->getClockBlock() == ab, referer, ab);
      NU_ASSERT2(ab_set.find(referer) != ab_set.end(), ab, referer);
    }
  }
} // void NUModule::sanityCheckAlwaysBlockPrioChain



void NUModule::replaceModuleAndDeclScopeInstances(const NUModuleInstanceList& new_mods)
{
  // First clear all the instances in this module and it's declaration scopes.
  mModuleInstanceList.clear();
  for (NUNamedDeclarationScopeLoop loop = loopDeclarationScopes(); !loop.atEnd(); ++loop)
  {
    (*loop)->removeAllInstances();
  }
  // Setup a map of module/declaration scope to list of instances in them. Add to this
  // list the instances we're replacing.
  UtMap<NUScope*,NUModuleInstanceList> instMap;
  for (NUModuleInstanceListIter itr = new_mods.begin(); itr != new_mods.end(); ++itr)
  {
    NUModuleInstance* inst = *itr;
    NUScope* instParent = inst->getParent();
    instMap[instParent].push_back(inst);  
  }
  // Update the instance list of module/declaration scopes.
  for (UtMap<NUScope*,NUModuleInstanceList>::iterator itr = instMap.begin();
       itr != instMap.end(); ++itr)
  {
    NUScope* instParent = itr->first;
    NUModuleInstanceList& instList = itr->second;
    if (instParent->getScopeType() == NUScope::eModule) {
      NUModule* mod = dynamic_cast<NUModule*>(instParent);
      mod->replaceModuleInstances(instList);
    } else if (instParent->getScopeType() == NUScope::eNamedDeclarationScope) {
      NUNamedDeclarationScope* declScope = dynamic_cast<NUNamedDeclarationScope*>(instParent);
      declScope->replaceModuleInstances(instList);
    }
  }
}

void NUModule::getEmptyDeclScopes(NUNamedDeclarationScopeList& emptyDeclScopes)
{
  for (NUNamedDeclarationScopeLoop loop = loopDeclarationScopes(); !loop.atEnd(); ++loop) {
    NUNamedDeclarationScope* declScope = *loop;
    declScope->isEmpty(&emptyDeclScopes);
  }
}

// Experimental code below that's for Programmer's View
#ifdef CARBON_PV

static STBranchNode* sCopyBranch(STSymbolTable* table, STBranchNode* node) {
  STBranchNode* parent = node->getParent();
  SInt32 index = -1;
  if (parent != NULL) {
    parent = sCopyBranch(table, parent);
    index = parent->numChildren();
  }
  else {
    index = table->numRoots();
  }
  STBranchNode* new_node
    = table->createBranch(node->strObject(), parent, index);
  NUAliasDataBOM* bom = (NUAliasDataBOM*) new_node->getBOMData();
  bom->setIndex(index);
  return new_node;
}

static STAliasedLeafNode* sCopyLeaf(STSymbolTable* table,
                                    STAliasedLeafNode* node)
{
  STBranchNode* parent = node->getParent();
  SInt32 index = -1;
  if (parent != NULL) {
    parent = sCopyBranch(table, parent);
    index = parent->numChildren();
  }
  STAliasedLeafNode* new_node
    = table->createLeaf(node->strObject(), parent, index);
  NUAliasDataBOM* bom = (NUAliasDataBOM*) new_node->getBOMData();
  bom->setIndex(index);
  return new_node;
}

static NUNet* sCopyNet(NUNet* src_net,
                       NUScope* dst_scope,
                       NUNetReplacementMap& rep_map)
{
  NUNet* dst_net = src_net->clone(dst_scope, rep_map, src_net->getName(),
                                  src_net->getFlags());
  
  // If this net was flattened, then the "local" name of the net that
  // just got copied will lose the original hierarchial name.
  STAliasedLeafNode* leaf = src_net->getNameLeaf();
  STAliasedLeafNode* master = leaf->getMaster();
  if (leaf != master) {
    STAliasedLeafNode* dst_leaf = dst_net->getNameLeaf();
    NUModule* dst_mod = dst_scope->getModule();
    STAliasedLeafNode* dst_master = sCopyLeaf(dst_mod->getAliasDB(), master);
    NUAliasDataBOM* bom = (NUAliasDataBOM*) dst_master->getBOMData();
    bom->setNet(dst_net);
    NU_ASSERT(dst_master, src_net);
    NU_ASSERT(dst_leaf->getMaster() == dst_leaf, src_net);
    dst_leaf->linkAlias(dst_master);
    dst_master->setThisMaster();
  }
  return dst_net;
}

void sCopyDeclarationScope(NUNamedDeclarationScope* ds,
                           NUScope* parent,
                           IODBNucleus* iodb,
                           NUNetRefFactory* nrf,
                           NUNetReplacementMap* rep_map)
{
  NUNamedDeclarationScope* new_ds =
    new NUNamedDeclarationScope(ds->getName(), parent, iodb, nrf,
                                ds->getLoc());
  for (NUNetLoop p(ds->loopLocals()); !p.atEnd(); ++p) {
    NUNet* net = *p;
    NUNet* new_net = sCopyNet(net, new_ds, *rep_map);
    new_ds->addLocal(new_net);
    (*rep_map)[net] = new_net;
  }

  for (NUNamedDeclarationScopeLoop p(ds->loopDeclarationScopes());
       !p.atEnd(); ++p)
  {
    NUNamedDeclarationScope* sub_ds = *p;
    sCopyDeclarationScope(sub_ds, new_ds, iodb, nrf, rep_map);
  }
}

NUModule* NUModule::copy(NUDesign* new_design, NUModuleMap* mod_map) {
  STSymbolTable* symtab = new_design->getSymbolTable();
  AtomicCache* acache = symtab->getAtomicCache();

  // Create an alias table
  STSymbolTable * aliasDB = new STSymbolTable(new_design->getAliasBOM(),
                                              acache);
  aliasDB->setHdlHier(new HdlVerilogPath);
  NUNetRefFactory* nrf = new_design->getNetRefFactory();
  IODBNucleus* iodb = new_design->getIODB();
  NUModule* new_mod = new NUModule(new_design, atTopLevel(),
                                   mName, aliasDB, nrf, acache, mLoc,
                                   iodb, mTimeUnit, mTimePrecision,
                                   isTimescaleSpecified(),
                                   getSourceLanguage());
  new_mod->putOriginalName(mOriginalName);
  if (atTopLevel()) {
    new_design->addTopLevelModule(new_mod);
  }
  else {
    new_design->addModule(new_mod);
  }
  (*mod_map)[this] = new_mod;

  // copy local nets & ports & declaration scopes
  CopyContext cc(new_mod, acache, true);
  NUNetReplacementMap& rep_map = cc.mNetReplacements;
  for (NUNetList::iterator p = mLocalNetList.begin(), e = mLocalNetList.end();
       p != e; ++p)
  {
    NUNet* net = *p;
    NUNet* new_net = sCopyNet(net, new_mod, rep_map);
    new_mod->addLocal(new_net);
    rep_map[net] = new_net;
  }

  for (UInt32 i = 0; i < mPorts.size(); ++i) {
    NUNet* net = mPorts[i];
    NUNet* new_net = sCopyNet(net, new_mod, rep_map);
    new_mod->addPort(new_net);
    rep_map[net] = new_net;
  }

  for (NUNamedDeclarationScopeList::iterator p = mDeclarationScopeList.begin(),
         e = mDeclarationScopeList.end(); p != e; ++p)
  {
    NUNamedDeclarationScope* ds = *p;
    sCopyDeclarationScope(ds, new_mod, iodb, nrf, &rep_map);
  }

  // map the special nets
  rep_map[mExtNet] = new_mod->mExtNet;
  rep_map[mControlFlowNet] = new_mod->mControlFlowNet;
  rep_map[mOutputSysTaskNet] = new_mod->mOutputSysTaskNet;

  copyModuleInstances(new_mod, cc, mod_map);
  copyAlwaysBlocks(new_mod, cc, NULL);

  // Copy over initial blocks
  for (NUModule::InitialBlockLoop p(loopInitialBlocks()); !p.atEnd(); ++p) {
    NUInitialBlock* ib = *p;
    NUInitialBlock* new_ib = ib->clone(cc);
    new_mod->addInitialBlock(new_ib);
  }

  // Copy over continuous assigns
  for (NUModule::ContAssignLoop p(loopContAssigns()); !p.atEnd(); ++p) {
    NUContAssign* ca = *p;
    NUContAssign* new_ca = dynamic_cast<NUContAssign*>(ca->copy(cc));
    NU_ASSERT(new_ca, ca);
    new_mod->addContAssign(new_ca);
  }

  // Copy mAsyncResetNets
  // Copy mNetHierRefList
  // Copy mTriRegInitList
  // Copy mContEnabledDriverList
  // Copy mBlockList (what is that?)
  // Copy mTaskVector
  // Copy mCModelVector

  new_mod->mRunsSystemFunctionsOfTimeGroup = mRunsSystemFunctionsOfTimeGroup;
  new_mod->mRunsSystemTasksOfRandomGroup = mRunsSystemTasksOfRandomGroup;
  new_mod->mRunsSystemTasksOfOutputGroup = mRunsSystemTasksOfOutputGroup;
  new_mod->mRunsVerilogSystemTasksOfOutputGroup = mRunsVerilogSystemTasksOfOutputGroup;
  new_mod->mRunsSystemTasksOfInputGroup = mRunsSystemTasksOfInputGroup;
  new_mod->mRunsSystemTasksOfControlGroup = mRunsSystemTasksOfControlGroup;
  new_mod->mFlags = mFlags;

  return new_mod;
} // NUModule* NUModule::copy

void NUModule::copyModuleInstances(NUModule* dst_module, CopyContext& cc,
                                   NUModuleMap* mod_map)
{
  // Copy the instances.  Since we are going bottom-up, we know that
  // the instantiated modules are already mapped.
  for (NUModuleInstanceMultiLoop q(loopInstances()); !q.atEnd(); ++q) {
    NUModuleInstance* inst = *q;
    NUModule* inst_mod = inst->getModule();

    if ( mUseLegacyGenerateHierarchyNames ) {
      NUScope* parent = inst->getParent();
      NUModule* parent_module = inst->getParentModule();
      NU_ASSERT((parent == parent_module),inst); // this code does not yet handle instances declared within namedDeclarationScopes
    }

    NUModule* old_inst_mod = inst_mod;
    if (mod_map != NULL) {
      inst_mod = (*mod_map)[inst_mod];
      NU_ASSERT(inst_mod, inst);
    }
    NUModuleInstance* new_inst =
      new NUModuleInstance(inst->getName(), dst_module, inst_mod,
                           dst_module->mNetRefFactory, inst->getLoc());
    NUPortConnectionVector v;
    for (NUPortConnectionLoop p(inst->loopPortConnections()); !p.atEnd(); ++p)
    {
      NUPortConnection* pc = *p;
      NUPortConnection* new_pc = pc->copy(cc);
      v.push_back(new_pc);
      new_pc->setModuleInstance(new_inst);

      // The formal will be for the old module.  Translate it to the
      // new module via the index
      int idx = old_inst_mod->getPortIndex(new_pc->getFormal());
      NU_ASSERT(idx >= 0, new_pc);
      NUNet* new_formal = inst_mod->getPortByIndex(idx);
      NU_ASSERT(new_formal, new_pc);
      new_pc->putFormal(new_formal);
    }
    new_inst->setPortConnections(v);
    dst_module->addModuleInstance(new_inst);
  }
} // void NUModule::copyModuleInstances

void NUModule::copyAlwaysBlocks(NUModule* dst_module, CopyContext& cc,
                                NuToNuFn* xlate)
{
  // Copy over the always blocks.  Operate on prio-block chains together
  NUAlwaysBlockReplacementMap ab_rep_map;
  for (NUModule::AlwaysBlockLoop p(loopAlwaysBlocks()); !p.atEnd(); ++p) {
    NUAlwaysBlock* ab = *p;

    // always start at clock-block
    if (ab->getClockBlock() == NULL) {
      NUAlwaysBlock* prev_blk = NULL;
      NUAlwaysBlock* new_clk_blk = NULL;

      for (NUAlwaysBlock* b = ab; b != NULL; b = b->getPriorityBlock()) {
        NUAlwaysBlock* new_b = b->clone(cc, false);
        if (xlate != NULL) {
          new_b->replaceLeaves(*xlate);
        }

        dst_module->addAlwaysBlock(new_b);
        ab_rep_map[b] = new_b;
        if (new_clk_blk == NULL) {
          new_clk_blk = new_b;
        }
        else {
          prev_blk->setPriorityBlock(new_b);
          new_b->setClockBlock(new_clk_blk);
        }
        prev_blk = new_b;
      }

      // note we can't copy over the referrers yet, because
      // some of those may not be translated yet.
    } // if
  } // for

  // now rebuild the new referrers lists
  for (NUModule::AlwaysBlockLoop p(loopAlwaysBlocks()); !p.atEnd(); ++p) {
    NUAlwaysBlock* ab = *p;

    // always start at clock-block
    if (ab->getClockBlock() == NULL) {
      NUAlwaysBlock* new_clk_blk = ab_rep_map[ab];

      NUAlwaysBlockVector referers;
      ab->getClockBlockReferers(&referers);
      for (UInt32 i = 0; i < referers.size(); ++i) {
        NUAlwaysBlock* referer = referers[i];
        NUAlwaysBlock* new_referer = ab_rep_map[referer];
        NU_ASSERT(new_referer, referer);
        new_clk_blk->addClockBlockReferer(new_referer);
      }
    }
  }
}
#endif // CARBON_PV

