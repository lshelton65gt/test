// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUExpr.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNetSet.h"

#include "nucleus/NUDesignWalker.h"

#include "flow/FLIter.h"
#include "flow/FLNode.h"

#include "compiler_driver/CarbonDBWrite.h"

#include "symtab/STBranchNode.h"
#include "symtab/STSymbolTable.h"
#include "iodb/IODBNucleus.h"

#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"

#include "util/AtomicCache.h"
#include "util/CarbonAssert.h"
#include "util/CarbonTypes.h"
#include "util/ConstantRange.h"
#include "util/OSWrapper.h"
#include "util/UtString.h"        // for UtString concat overrides
#include "util/StringAtom.h"

#include <ctype.h>              // isdigit

/*!
  \file
  Implementation of the NUScope class.
*/


NUScope::~NUScope()
{
  if (mSymNumMap != NULL)
    delete mSymNumMap;
}

const char*
NUScope::getScopeTypeStr() const
{
  const char *retval = NULL;
  ScopeT type = getScopeType();
  switch ( type )
  {
  case eBlock:
    retval = "block";
    break;
  case eModule:
    retval = "module";
    break;
  case eTask:
    retval = "task";
    break;
  case eNamedDeclarationScope:
    retval = "named declaration";
    break;
  case eUserTask:
    retval = "user task";
    break;
  }
  return retval;
}


const char * NUScope::getPrintableName() const
{
  StringAtom * declared_name = getName();
  if (declared_name) {
    return declared_name->str();
  } else {
    return "__anonymous__";
  }
}


NUModule *NUScope::getParentModule() const
{
  NUScope *scope = getParentScope();

  if (scope == 0) {
    return 0;
  }

  while (scope->getScopeType() != NUScope::eModule) {
    scope = scope->getParentScope();
  }

  NUModule *module = dynamic_cast<NUModule*>(scope);
  NU_ASSERT(module, this->getModule ());
  return module;
}


NUModule* NUScope::getModule() const
{
  if (getScopeType() == NUScope::eModule) {
    NUModule *module = dynamic_cast<NUModule*>((NUScope*)this);
    INFO_ASSERT(module, "bad module");
    return module;
  } else {
    return getParentModule();
  }
}


NUTF* NUScope::getTFScope() const
{
  if (getScopeType() == NUScope::eTask ) {
    NUTF *task = dynamic_cast<NUTF*>((NUScope*)this);
    INFO_ASSERT(task, "bad task scope");
    return task;
  }
  else {
    NUScope *parent = getParentScope();
    if ( parent == NULL )
      return NULL;
    else
      return parent->getTFScope();
  }
}


#ifdef CDB
bool NUScope::hasNetName(StringAtom* name) const {
  return mNetHash.find(name) != mNetHash.end();
}
#endif

void NUScope::addNetName(NUNet*
#ifdef CDB
                             net
#endif
  )
{
#ifdef CDB
  StringAtom* name = net->getName();
  NU_ASSERT(mNetHash.find(name) == mNetHash.end(), net);
  mNetHash.insert(name);
#endif
}

void NUScope::removeNetName(NUNet*
#ifdef CDB
                                net
#endif
  )
{
#ifdef CDB
  // net is currently in mNetHash due to being a port, so
  // avoid asserting in addLocal
  size_t numRemoved = mNetHash.erase(net->getName());
  NU_ASSERT(numRemoved == 1, net);
#endif
}

bool NUScope::isParentOf(NUScope *scope) const
{
  NUScope *parent = scope;
  do {
    parent = parent->getParentScope();
  } while (parent and (parent != this));

  return (parent == this);
}


bool NUScope::inTFHier() const
{
  if (getScopeType() == NUScope::eTask) {
    return true;
  } else if (getParentScope() != 0) {
    return getParentScope()->inTFHier();
  } else {
    return false;
  }
}


NUCompositeNet*
NUScope::createTempCompositeNetFromImageForInterraFlowUseOnly( StringAtom *name, NUCompositeNet *master,
                                          NetFlags flags, NUNetRefFactory *netRefFactory )
{
  // NOTE: use createTempNetFromImage do not use this routine.
  // This routine is used only by the Interra flow which is going away, and it is not being fixed.

  NUNamedDeclarationScope *rec_scope = new NUNamedDeclarationScope( name, this, mIODB,
                                                                    netRefFactory,
                                                                    master->getLoc( ));

  NetFlags net_flags = NetFlags(flags | getTempFlags());
  const UInt32 numFields = master->getNumFields();
  NUNetVector fieldVector;
  for ( UInt32 i = 0; i < numFields; ++i )
  {
    NUNet *field = master->getField( i );
    UtString fieldName(field->getName()->str());
    NUNet *temp = rec_scope->createTempNetFromImage( field, fieldName.c_str(), true, true );
    fieldVector.push_back( temp );
  }
  StringAtom *netAtom = gensym( "$recnet", name->str( ));
  NUCompositeNet *net = new NUCompositeNet( master->getTypeMark(),
                                            fieldVector,
                                            *master->getRanges(),
                                            master->isPacked(),
                                            this,
                                            netAtom,
                                            master->getLoc( ),
                                            net_flags);




  addLocal( net );
  return net;
}

NetFlags NUScope::createTempNetFlags(NUNet* original)
{
  NetFlags net_flags = getTempFlags();
  // preserve storage information.
  if ( original->isReg() ) {
    net_flags = NetRedeclare(net_flags,eDMRegNet);
  } else if ( original->isWire() ) {
    net_flags = NetRedeclare(net_flags,eDMWireNet);
  } else {
    // preserve vector characteristics
    if ( original->isInteger() ) {
      net_flags = NetRedeclare(net_flags,eDMIntegerNet); 
    }
    if ( original->isTime() ) {
      net_flags = NetRedeclare(net_flags,eDMTimeNet);
    }
    if ( original->isSigned() )
      net_flags = NetFlags( net_flags | eSigned );
  }

  // Preserve any tri-state characteristics
  if (original->isTri()) {
    net_flags = NetFlags( net_flags | eDMTriNet );
  }
  if (original->isTri1()) {
    net_flags = NetFlags( net_flags | eDMTri1Net );
  }
  if (original->isSupply0()) {
    net_flags = NetFlags( net_flags | eDMSupply0Net);
  }
  if (original->isWand()) {
    net_flags = NetFlags( net_flags | eDMWandNet);
  }
  if (original->isTriand()) {
    net_flags = NetFlags( net_flags | eDMTriandNet);
  }
  if (original->isTri0()) {
    net_flags = NetFlags( net_flags | eDMTri0Net);
  }
  if (original->isSupply1()) {
    net_flags = NetFlags( net_flags | eDMSupply1Net);
  }
  if (original->isWor()) {
    net_flags = NetFlags( net_flags | eDMWorNet);
  }
  if (original->isTrior()) {
    net_flags = NetFlags( net_flags | eDMTriorNet);
  }
  if (original->isTrireg()) {
    net_flags = NetFlags( net_flags | eDMTriregNet);
  }
  
  return net_flags;
}

NUNet* NUScope::createTempNetFromImage(NUNet * original,
                                       const char* prefix,
                                       bool duplicate_memories,
				       bool suppressGenSym)
{
  NUNet * local = NULL;
  
  StringAtom * name;
  if(suppressGenSym)
    name = original->getName();
  else
    name = gensym(prefix, original->getName()->str());

  NUMemoryNet * memory_net = original->getMemoryNet();
  NetFlags net_flags = createTempNetFlags(original);

  if ( original->isBitNet() ) {
    local = new NUBitNet ( name, net_flags,
                           this, original->getLoc() );
  } else if ( original->isVectorNet() ) {
    NUVectorNet * vector_net = original->castVectorNet();
    local = new NUVectorNet ( name,
			      new ConstantRange( *vector_net->getDeclaredRange() ),
			      net_flags,
                              vector_net->getVectorNetFlags (),
			      this, original->getLoc() );
  } else if ( original->isCompositeNet( )) {
    NUCompositeNet *composite_net = original->getCompositeNet();

    // Get a name for the record scope.
    // If we need to, make sure it is unique by calling gensym()
    StringAtom *scopeAtom = composite_net->getOriginalName();
    if (not suppressGenSym) {
      scopeAtom = gensym("record", scopeAtom->str());
    }
    NUNamedDeclarationScope* rec_scope = new NUNamedDeclarationScope(scopeAtom,
                                                                     this,
                                                                     getIODB(),
                                                                     getModule()->getNetRefFactory(),
                                                                     original->getLoc( ));
    

    const UInt32 numFields = composite_net->getNumFields();
    NUNetVector fieldVector;
    for ( UInt32 i = 0; i < numFields; ++i )
    {
      // Want to replicate the original record field names, and since we have just created a new scope
      // for the record, we don't need to worry about name collisions.
      // Therefore, set prefix to NULL and set "suppressGenSym" to true (last argument), which means to not create a temporary name.
      NUNet *temp = rec_scope->createTempNetFromImage( composite_net->getField( i ), NULL, true, true );
      fieldVector.push_back( temp );
    }
    local = new NUCompositeNet( composite_net->getTypeMark(),
                                fieldVector,
                                *composite_net->getRanges(),
                                composite_net->isPacked(),
                                this,
                                name,
                                composite_net->getLoc( ),
                                net_flags);

  } else if ( memory_net ) {
    if (duplicate_memories) {
      net_flags = NetRedeclare(net_flags, original->getDeclarationFlags());

      NUMemoryNet * local_mem = new NUMemoryNet ( name,
                                                  new ConstantRange( ), new ConstantRange( ),// dummy ranges, instead they will be filled in below by copyRanges
                                                  net_flags, 
                                                  original->getVectorNetFlags (),
                                                  this,
                                                  original->getLoc() );
      NU_ASSERT( local_mem->getBitSize() != 0, original );
      local_mem->copyRanges(memory_net);
      local_mem->copyAuxInformation(memory_net);
      local = local_mem;
    } else {
      bool dynamic = (memory_net->getIsDynamic());
      NUTempMemoryNet * local_tmp_mem = 
        new NUTempMemoryNet (name, memory_net, net_flags,
                             dynamic, this);
      if (not dynamic) {
        // this is worst-case. we could do better.
        local_tmp_mem->putNumWritePorts(memory_net->getNumWritePorts());
      }
      local = local_tmp_mem;
    }
  } else {
    // fail if we see something unexpected
    NUTempMemoryNet * tmp_memory_net = dynamic_cast<NUTempMemoryNet*>(original);
    NU_ASSERT ( tmp_memory_net, original);

    NUMemoryNet *master = tmp_memory_net->getMemoryNet();
    // we should always see the master before the temp referencing it.
    NU_ASSERT( master, original );
    local = new NUTempMemoryNet ( name,
                                  master,
                                  net_flags,
                                  tmp_memory_net->isDynamic(),
                                  this );
  }

  NU_ASSERT(local, original);
  addLocal(local);

  return local;
}

NUNet*
NUScope::createTempSubRangeNetFromImage(NUNet* original,  UInt32 bit_size,
                                        const char* prefix)
{
  // This code is not supported on signed nets because it is
  // not obvious whether the resulting net should be signed
  // or not. If this code is needed to do that a thorough
  // analysis will be needed to determine whether the
  // resulting temp net should be an integer/signed/real
  // etc.
  NU_ASSERT(!original->isSigned(), original);

  // Create a name and create the base flags
  StringAtom* name = gensym(prefix, original->getName()->str());
  NetFlags net_flags = createTempNetFlags(original);

  // Currently only supports vectors, other uses should add support as
  // needed.
  NUNet * local = NULL;
  if (original->isVectorNet()) {
    NUVectorNet* vectorNet = original->castVectorNet();
    ConstantRange range(bit_size - 1, 0);
    local = new NUVectorNet(name, range, net_flags, vectorNet->getVectorNetFlags(),
                            this, original->getLoc());
  } else {
    NU_ASSERT("Unexpected net type" == NULL, original);
  }

  // Add the local to the scope
  addLocal(local);
  return local;
}

NUNet* NUScope::createTempNet(StringAtom* name,
			      UInt32 bit_size,
                              bool isSigned,
			      const SourceLocator& loc,
                              StringAtom* orig_name)
{
  LOC_ASSERT(bit_size > 0, loc);
  if (bit_size == 1) {
    NUNet* net = createTempBitNet(name, isSigned, loc, orig_name);
    return net;
  } else {
    return createTempVectorNet(name, ConstantRange(bit_size-1, 0), 
                               isSigned, loc, 
                               eNoneNet, eNoneVectorNet, orig_name);
  }
}


NUNet* NUScope::createTempBitNet(StringAtom* name,
                                 bool is_signed,
				 const SourceLocator& loc,
                                 StringAtom* orig_name)
{
  NetFlags flags = getTempFlags();
  if (is_signed) {
    flags = NetRedeclare(flags, eSigned);
  }
  NUNet *net = new NUBitNet(name, flags, this, loc);
  net->setOriginalName(orig_name);
  addLocal(net);
  return net;
}


NUNet* NUScope::createTempVectorNet(StringAtom* name,
				    const ConstantRange& range,
                                    bool isSigned,
				    const SourceLocator& loc,
				    NetFlags flags,
                                    VectorNetFlags vflags,
                                    StringAtom* orig_name)
{
  NetFlags net_flags = NetFlags(flags | getTempFlags());
  if (isSigned)
    net_flags = NetFlags(net_flags | eSigned);
  NUNet *net = new NUVectorNet(name, range, net_flags, vflags, this, loc);
  net->setOriginalName(orig_name);
  
  addLocal(net);
  return net;
}


NUTempMemoryNet* NUScope::createTempMemoryNet(StringAtom *name,
                                              NUMemoryNet* master,
                                              bool isDynamic,
                                              NetFlags flags,
                                              StringAtom* orig_name)
{
  NetFlags net_flags = NetFlags(flags | getTempFlags());
  NUTempMemoryNet *net = new NUTempMemoryNet(name, master, net_flags,
                                             isDynamic, this);
  net->setOriginalName(orig_name);
  addLocal(net);
  return net;
}


NUMemoryNet* NUScope::createTempMemoryNetFromImage(StringAtom *name,
                                                   NUMemoryNet* master,
                                                   NetFlags flags)
{
  NetFlags net_flags = NetFlags(flags | getTempFlags());
  NU_ASSERT (flags & e2DNetMask, master); // no 2DAnything flag set

  NUMemoryNet *net = new NUMemoryNet(name,
                                     new ConstantRange( ), new ConstantRange( ),// dummy ranges here and they will be filled in below by copyRanges
                                     net_flags, master->getVectorNetFlags (),
                                     this,
                                     master->getLoc());
  net->copyRanges( master );
  net->copyAuxInformation( master );
  NU_ASSERT( net->getBitSize() != 0, master );
  addLocal(net);
  return net;
}


void NUScope::removeNetsHelper(const NUNetSet& nets, bool remove_references,
                               NUNetList* net_list)
{
  NUNetList new_nets;
  UInt32 num_removed = 0;
  for (NUNetList::iterator p = net_list->begin(); p != net_list->end(); ) {
    NUNet* net = *p;
    if (nets.find(net) == nets.end()) {
      ++p;
    }
    else {
      NUNetList::iterator remove_iter = p;
      ++p;
      net_list->erase(remove_iter);
      if (remove_references) {
        NUNetRefFactory* nrf = getModule()->getNetRefFactory();
        nrf->erase(net);
      }
      removeNetName(net);
      ++num_removed;
    }
  }
  INFO_ASSERT(num_removed == nets.size(), "net removal inconsistency");
} // void NUScope::removeNetsHelper


void NUScope::disconnectLocal(NUNet *net)
{
  removeLocal(net, false);
}


/*
StringAtom* NUScope::newBlockName(AtomicCache*,
				  const char* prefix)
{
  return uniquify(NULL, prefix);
}
*/

StringAtom* NUScope::newBlockName(AtomicCache*, const SourceLocator& loc)
{
  UtString path, fileStr;
  OSParseFileName(loc.getFile(), &path, &fileStr);
  size_t dotPlace = fileStr.find(".");
  if (dotPlace != UtString::npos)
    fileStr.replace(dotPlace, 1, "_");
  fileStr << "_L" << loc.getLine();
  return gensym("block", fileStr.c_str());
}


NUScopeElab* NUScope::lookupElabScope(const STBranchNode *hier) const
{
  NU_ASSERT(emitInSymtab(), getModule ());

  // Get the leaf node for this branch node
  const STSymbolTableNode *node = hier->getChild(getSymtabIndex());
  ST_ASSERT (node, hier);
  const STBranchNode* branch = node->castBranch();
  ST_ASSERT(branch, node);

  NUBase* base = CbuildSymTabBOM::getNucleusObj(branch);
  NUScopeElab *elab = dynamic_cast<NUScopeElab*>(base);
  NU_ASSERT(elab, getModule ());
  return elab;
}


//! Local helper function to print fanin relationships for the given flow node.
/*!
  \param fl_node The flow node for which to print fanin.
  \param seen_set Set of flow nodes we have already seen on traversal.
  \param indent Number of spaces to indent.
 */
static void helperPrintFanin(FLNode *fl_node,
                             FLNodeSet *seen_set,
                             int indent)
{
  FLNodeIter *fanin_iter =
    fl_node->makeFaninIter(FLIterFlags::eAll,
                           FLIterFlags::eNone,
                           FLIterFlags::eOverAll,
                           true,
                           FLIterFlags::eIterNodeOnce);
  for (; not fanin_iter->atEnd(); fanin_iter->next()) {
    FLNode* fanin = **fanin_iter;
    if (fanin_iter->getCurParent()) {
      for (int i = 0; i < indent+2; i++) {
        UtIO::cout() << " ";
      }
      UtIO::cout() << "(fanin " << fanin << " " << fanin_iter->getCurParent() << ")" << UtIO::endl;
    }
    if (seen_set->find(fanin) != seen_set->end()) {
      fanin_iter->doNotExpand();
      continue;
    }
    seen_set->insert(fanin);
    fanin->ppPrint(indent+2);
    if (fanin_iter->getCurFlags() & FLIterFlags::eHasNestedBlock) {
      for (Iter<FLNode*> loop = fanin->loopNested(); !loop.atEnd(); ++loop)
      {
        FLNode* nested = *loop;
        helperPrintFanin(nested, seen_set, indent+2);
      }
    }
  }
  delete fanin_iter;
}


//! Local helper function to print fanin relationships for the list of nets.
/*!
  \param net_list List of nets.
  \param indent Number of spaces to indent.
 */
static void helperPrintConnectivity(const NUNetList &net_list, int indent)
{
  FLNodeSet seen_set;
  for (NUNetListIter iter = net_list.begin();
       iter != net_list.end();
       iter++) {
    for (int i = 0; i < indent; i++) {
      UtIO::cout() << " ";
    }
    UtIO::cout() << "Iterating over the fanin for:  ";
    (*iter)->print(false, indent+2);

    FLNetIter *fanin_iter = (*iter)->makeFaninIter(FLIterFlags::eAll,
						   FLIterFlags::eAll,
						   FLIterFlags::eOverAll,
						   FLIterFlags::eIterNodeOnce);
    for (; not fanin_iter->atEnd(); fanin_iter->next()) {
      helperPrintFanin(**fanin_iter, &seen_set, indent);
    }
    delete fanin_iter;
  }
}


void NUScope::printFlow(int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  NUNetList nets;
  getAllNets(&nets);
  helperPrintConnectivity(nets, numspaces);
}


NUScopeElab::NUScopeElab(NUScope *scope, STSymbolTableNode *node) :
  NUElabBase(node),
  mScope(scope)
{
}


STBranchNode* NUScopeElab::getHier() const
{
  STBranchNode *node = mSymNode->castBranch();
  ST_ASSERT(node, mSymNode);
  return node;
}


NUScopeElab*  NUScopeElab::lookupScope(STBranchNode *hier)
{
  NUBase* base = CbuildSymTabBOM::getNucleusObj(hier);
  NUScopeElab *elab = dynamic_cast<NUScopeElab*>(base);
  NU_ASSERT(elab, base);
  return elab;
}

int NUScope::compare(const NUScope* s1, const NUScope* s2)
{
  int cmp = 0;
  if (s1 != s2)
  {
    if (s1 == 0)
      cmp = -1;
    else if (s2 == 0)
      cmp = 1;
    else
    {
      cmp = compare(s1->getParentScope(), s2->getParentScope());
      if (cmp == 0)
      {
        // we must be at the top level
        NUModule* m1 = s1->getModule();
        NUModule* m2 = s2->getModule();
        cmp = NUModule::compare(m1, m2);
      }
    }
  }
  return cmp;
} // int NUScope::compare


void NUScope::filterNonLocals(const NUNetSet * in, NUNetSet * out)
{
  for (NUNetSet::const_iterator iter = in->begin(); iter != in->end(); ++iter) {
    NUNet *net = *iter;
    NUScope *net_scope = net->getScope();
    if (not ((this == net_scope) or (isParentOf(net_scope)))) {
      out->insert(net);
    }
  }
}


//! hash operator -- requires class method hash()
size_t NUNetHashByName::hash(const NUNet* var) const {
  return var->getStorageNameLeaf()->hash();
}

//! equal function
bool NUNetHashByName::equal(const NUNet* v1, const NUNet* v2) const {
  return HierName::compare(v1->getStorageNameLeaf(), v2->getStorageNameLeaf()) == 0;
}

//! shallow less-than function -- for Microsoft hash tables
bool NUNetHashByName::lessThan1(const NUNet* v1, const NUNet* v2) const {
  return v1 < v2;
}

//! lessThan operator -- for sorted iterations
bool NUNetHashByName::lessThan(const NUNet* v1, const NUNet* v2) const {
  return HierName::compare(v1->getStorageNameLeaf(), v2->getStorageNameLeaf()) < 0;
}

// Make a unique "temp" name as close to the provided name as possible.
// prefix it with "$prefix", and suffix it with a ";integer"
// to keep it unique within the scope
StringAtom* NUScope::gensym(const char* prefix, const char* name,
                            const NUBase* proxy, bool isAlreadyUnique)
{
  if (mSymNumMap == NULL)
    mSymNumMap = new SymNumMap;

  // prefix with $ to make it illegal verilog so it does not clash
  UtString buf("$");
  NU_ASSERT(prefix, getModule ());
  if (*prefix == '$')
    ++prefix;                   // do not need $$ at the beginning
  if (isAlreadyUnique)
    buf << prefix;
  else
    stripUniquify(&buf, prefix);

  // If this name was already generated via gensym, so parse
  // out the base name, so we don't wind up appending cascading _N
  // suffixes.
  if (name != NULL)
  {
    buf << "_";
    if (strncmp(name, buf.c_str(), buf.size()) == 0)
      name += buf.size();         // skip common prefix
    if (isAlreadyUnique)
      buf << name;
    else
      stripUniquify(&buf, name);
  }

  if (proxy != NULL)
  {
    // NUBase should get a compose method.  Until then, hack it.
    const NUExpr* e = dynamic_cast<const NUExpr*>(proxy);
    const NUNet* n = dynamic_cast<const NUNet*>(proxy);
    const NUUseDefNode* ud = dynamic_cast<const NUUseDefNode*>(proxy);
    UtString proxyBuf;
    if (e != NULL) {
      if ( e->getLoc().isTicProtected() ){
        isAlreadyUnique=false;
      } 
      e->compose(&proxyBuf, NULL);
    } else if (n != NULL) {
      if ( n->getLoc().isTicProtected() ){
        isAlreadyUnique=false;
      } 
      n->compose(&proxyBuf, NULL);
    } else if (ud != NULL) {
      if ( ud->getLoc().isTicProtected() ){
        isAlreadyUnique=false;
        proxyBuf << "<protected>";
      } else {
        StringAtom* name = ud->getName(); // prefer this for tasks & functions
        if (name == NULL) {
          ud->compose(&proxyBuf, NULL);   // need this for lvalues
        } else {
          proxyBuf << name->str();
        }
      }
    }

    // Remove any embedded spaces that got inserted, which
    // happens in particular for expressions
    UtString tmp;
    for (size_t i = 0; i < proxyBuf.size(); ++i)
    {
      if (!isspace(proxyBuf[i]))
        tmp << proxyBuf[i];
    }
    if (!tmp.empty())
    {
      buf << "_";
      if (isAlreadyUnique)
        buf << tmp;
      else
        stripUniquify(&buf, tmp.c_str());
    }
  } // if

  AtomicCache* cache = mIODB->getAtomicCache();
  StringAtom* sym = cache->intern(buf.c_str(), buf.size());
  SymNumMap::iterator p = mSymNumMap->find(sym);
  SInt32 uniqueIndex = 1;
  if (p == mSymNumMap->end())
  {
    // We have not mapped this name before.  We can use it directly,
    // but don't use it again.
    (*mSymNumMap)[sym] = 2;
  }
  else
  {
    uniqueIndex = p->second;
    p->second = uniqueIndex + 1;
    if (isAlreadyUnique)
    {
      UtIO::cerr() << buf << ": Duplicate name creation attempted." << UtIO::endl;
      NU_ASSERT (0, getModule ());
    }
  }

  if (!isAlreadyUnique)
    buf << ";" << uniqueIndex;
  sym = cache->intern(buf.c_str(), buf.size());
  return sym;
} // StringAtom* NUScope::gensym


StringAtom * NUScope::gensymHierRef(const char * prefix,
                                    const char * leaf_name,
                                    const char * postfix)
{
  // We cannot use NUScope::gensym to construct hierref temporary
  // names. We want to create the name $PREFIX_name_POSTFIX as the
  // final portion of a hierarchical reference, but do not want this
  // name registered in the NUScope's symbol cache. 
  //
  // Generating this temporary name should not cause the
  // "$PREFIX_name_POSTFIX" name to be registered within the
  // mSymNumMap; the "a.b.c.$PREFIX_name_POSTFIX" exists. 
  //
  // Using use NUScope::gensym would register the
  // "$PREFIX_name_POSTFIX" name in the mSymNumMap. This causes
  // artificial name collisions if there is a non-hierref version of
  // the "$PREFIX_name_POSTFIX" name.
  //
  // Bug 4260 discovered this issue.

  NU_ASSERT((prefix!=NULL) and (leaf_name!=NULL) and (postfix!=NULL), getModule ());
  UtString hier_ref_name_string;
  hier_ref_name_string << "$" << prefix << "_"
                       << leaf_name << "_"
                       << postfix;

  AtomicCache* cache = mIODB->getAtomicCache();
  StringAtom * name = cache->intern(hier_ref_name_string);
  return name;
}


void NUScope::stripUniquify(UtString* buf, const char* name)
{
  // Remove $num at the end of the string
  size_t sz = strlen(name);

  // leave a lone ";" alone, so stop if p==name without first hitting a $.
  for (const char* p = name + sz - 1; p > name; --p)
  {
    if (*p == ';')
    {
      sz = p - name;            // "xyz;382" -> "xyz"
      break;
    }
    else if (!isdigit(*p))
      break;                    // "xyz;a" -> "xyz;a"
  }

  buf->append(name, sz);
}
  
// Note that we do not need to look up the specified symbol
// in the module to see if it's a generated symbol, because
// neither VHDL or Verilog names can begin with $.  Verilog
// names can include $, but not as their first letter.  If
// we decide later we need to do a lookup we can make this
// method non-static
bool NUScope::isGensym(StringAtom* sym)
{
  return *sym->str() == '$';
}

//! Find the best symbol in a name-ring
/*! The best symbol is
 *!    1. not a gensym
 *!    2. shallowest in depth
 *!    3. lexically earliest
 *! If all the aliases are genyms, then it returns the one passed in.
 */
STAliasedLeafNode* NUScope::findBestAlias(STAliasedLeafNode* name) {
  STAliasedLeafNode* bestAlias = NULL;
  UInt32 bestDepth = 0;
  for (STAliasedLeafNode::AliasLoop p(name); !p.atEnd(); ++p) {
    STAliasedLeafNode* alias = *p;
    if (! NUScope::isGensym(alias->strObject())) {
      UInt32 depth = alias->findDepth();
      if ((bestAlias == NULL) || (depth < bestDepth) ||
          ((depth == bestDepth) &&
           (HierName::compare(alias, bestAlias) < 0)))
      {
        bestAlias = alias;
        bestDepth = depth;
      }
    }
  }    
  if (bestAlias == NULL) {
    bestAlias = name;
  }
  return bestAlias;
} // STAliasedLeafNode* findBestAlias


SInt32 NUScope::determineModuleDepth(STSymbolTableNode * node)
{
  if (node == NULL) {
    return 0;
  }

  SInt32 depth = 0;
  if (node->castLeaf() != NULL) {
    depth = 1;
    node = node->getParent();
  }

  for ( ; node != NULL; node = node->getParent()) {
    NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
    NUModuleElab *elab = dynamic_cast<NUModuleElab*>(base);
    if (elab != NULL) {
      ++depth;
    }
  }
  return depth;
}


//! Helper callback class to aid in finding nested blocks.
class BlockChildFinder : public NUDesignCallback
{
#if !pfGCC_2
  using NUDesignCallback::operator();
#endif

public:
  BlockChildFinder (NUBlockScope * parent): 
    mNewParent(parent) 
  {}

  ~BlockChildFinder () {}

  //! Reparent this scope, but not anything below this scope.
  Status operator() (Phase, NUBlock * block){
    NUScope *oldParent = block->getParentScope ();

    if (oldParent==mNewParent) {
      // someone called us but no reparenting is necessary.
      return eSkip;
    }

    oldParent->removeBlock(block);
    block->putParentScope(mNewParent);
    mNewParent->addBlock(block);

    // Don't look beneath this point.
    return eSkip;
  }
  Status operator() (Phase, NUCaseItem*) { return eNormal; }
  Status operator() (Phase, NUBlockStmt*) { return eNormal; }
  Status operator() (Phase, NUBase*) { return eSkip; }

private:
  //! Replacement parent scope
  NUBlockScope* mNewParent;
};


void NUBlockScope::adoptChildren (void)
{
  // If we inserted/removed a Block above some other scope construct,
  // we need to fix up the parenting chain so that we are the parent
  // of the top scopes.
  BlockChildFinder callback (this);
  NUDesignWalker walker (callback, false);

  // Look thru this block's statements for any blocks.
  for (NUStmtLoop loop = loopStmts();
       not loop.atEnd();
       ++loop) {
    NUDesignCallback::Status status = walker.stmt (*loop);
    NU_ASSERT (status == NUDesignCallback::eNormal, *loop);
  }
}

void NUBlockScope::adoptChildren(NUStmtList* stmts)
{
  // If a sub-block was deleted and it's statements were moved into
  // this block, or if this block was added and the parent's statements
  // were moved into this block, fix up the parenting chain so that
  // this block is parent of all top level blocks in the given statements.
  BlockChildFinder callback (this);
  NUDesignWalker walker (callback, false);

  // Look through statements for any blocks.
  for (NUStmtListIter itr = stmts->begin(); itr != stmts->end(); ++itr) {
    NUDesignCallback::Status status = walker.stmt (*itr);
    NU_ASSERT (status == NUDesignCallback::eNormal, *itr);
  }
}

void NUScope::putScheduled (bool flag)
{
  mScheduled = flag;
}

bool NUScope::isScheduled (void) const
{
  return mScheduled;
}

StringAtom* NUScope::getOriginalName() const
{
  return getName();
}

//! Generate a unique name based on a list of string
/*! \param prefix specifies are descriptive prefix for the name
 *  \param names list of names that are massaged into a new name
 *  \param name distinguishing string if a name cannot be made from names
 */
StringAtom *NUScope::gensymFromList (const char *prefix,
    UtList <const char *> names,
    const char *name)
{
  StringAtom *atom;
  if (makeCleverName (prefix, names, &atom)
    || makeLongestName (prefix, names, &atom)
    || makeMediumName (prefix, names, &atom)) {
    return atom;
  } else {
    return gensym (prefix, name);
  }
}

#if 0
//! Strip extraneous $bumf from the name
bool NUScope::strip (const char *s, unsigned int *n) const
{
  unsigned int start = 0;
  unsigned int length = 0;
  int state = 0;
  while (state < 100) {
    switch (state) {
    case 0:
      switch (s [*n]) {
      case '$': start = *n + 1; length = 0; state = 10; break;
      case ';': start = *n + 1; length = 0; state = 20; break;
      default:
        state = 100;
        break;
      }
      break;
    case 10:
      if (isalpha (s [start + length])) {
        length++;
      } else if (s [start + length] == '_') {
        state++;                        // matched $ident_
      } else {
        return false;
      }
      break;
    case 11:
      if (!strncmp (s + start, "remodule", length)
        || !strncmp (s + start, "reorder", length)) {
        *n += length + 2;
        return true;
      } else {
        return false;
      }
      break;
    case 20:
      if (isdigit (s [start + length])) {
        length++;
      } else {
        state++;                        // matched ;nnn
      }
      break;
    case 21:
      *n += length + 1;
      return true;
      break;
    default:
      return false;
      break;
    }
  }
  return false;
}
#endif

bool NUScope::makeCleverName (const char *leader, const UtList <const char *> &names, StringAtom **atom)
{
  ASSERT (!names.empty ());
  // grab the first name
  UtList <const char *>::const_iterator it = names.begin ();
  const char *proto = *it;              // prototype name
  // initialise prefix and suffix to entire prototype
  const UInt32 proto_len = strlen (proto);
  UInt32 prefix = proto_len;         // length of common prefix
  UInt32 suffix = proto_len;         // length of common suffix
  it++;
  while ((prefix > 0 || suffix > 0) && it != names.end ()) {
    const char *name = *it;
    // check the prefix match
    UInt32 n = 0;
    while (n < prefix && name [n] != '\0' && name [n] == proto [n]) {
      n++;
    }
    prefix = n;
    // check the suffix match
    const UInt32 length = strlen (name);
    if (prefix == length) {
      // subsumed the entire name... don't make a clever name here
      return false;
    }
    n = 0;
    while (n < suffix && n < length && name [length - n - 1] == proto [proto_len - n - 1]) {
      n++;
    }
    suffix = n;
    it++;                               // advance to next name
  }
  // screw case... if all the nets have the same name...
  if (prefix == proto_len && suffix == proto_len) {
    return false;
  }
  // Poke about the _ characters to see if short prefix or suffix might make a
  // better name
  if (prefix > 2 && proto [prefix-1] != '_' && proto [prefix-2] == '_') {
    // the nets look like p_Xa, p_Xb, p_Xc. p_Xa_Xb_Xc looks better than
    // p_X_a_b_c.
    prefix--;
  }
  if (suffix > 2 && suffix < proto_len
    && proto [proto_len - suffix] != '_' && proto [proto_len - suffix + 1] == '_') {
    // the nets look like aX_s, b_Xs, c_Xs. aX_bX_cX_s looks better than
    // a_b_c_Xs.
    suffix--;
  }
  if (prefix > 0 && suffix > 0 && suffix < 4 && suffix < proto_len
    && proto [prefix - 1] == '_'
    && proto [proto_len - suffix] != '_' && proto [proto_len - suffix - 1] != '_') {
    // A short suffix, and the prefix ends with _. The nets look like p_Xs,
    // p_Ys, p_Zs. p_Xs_Ys_Zs looks better than p_X_Y_Z_s.
    suffix = 0;
  }
  if (suffix == 0 && prefix == 0) {
    return false;                       // no common prefix or suffix
  }
  ASSERT (prefix <= proto_len);
  ASSERT (suffix <= proto_len);
  // OK, we have identified a prefix and a suffix so try to fit the constructed
  // name in the available size
  char s [cMAX_NAME_LENGTH];            // buffer for forming the name
  UInt32 n = 0;                         // index into s
  // copy the prefix
  if (prefix == 0) {
    // no prefix
  } else if (n + prefix >= sizeof (s)) {
    return false;                       // prefix does not fit
  } else {
    ASSERT (n + prefix < sizeof (s));
    memcpy (s + n, proto, prefix);
    n += prefix;
  }
  // copy the varying parts of each name
  for (UtList <const char *>::const_reverse_iterator rit = names.rbegin (); rit != names.rend (); rit++) {
    const char *name = *rit;
    // add a separator
    if (n == 0 || s [n-1] == '_') {
      // no separator
    } else if (n >= sizeof (s) - 1) {
      return false;                     // not enough room
    } else {
      s [n++] = '_';
    }
    // add the varying part of this name 
    const UInt32 length = strlen (name);
    const UInt32 delta = length - prefix - suffix;
    if (prefix + suffix >= length) {
      // This name is just prefix_suffix, so let's arbitrarily just insert an @
      // into the name to indicate this silent contributor.
      s [n++] =  '@';
    } else if (n + delta >= sizeof (s)) {
      return false;                     // does not fit
    } else {
      ASSERT (n + delta < sizeof (s));
      memcpy (s + n, name + prefix, delta);
      n += delta;
    }
  }
  // add the suffix
  if (suffix == 0) {
    // there is no suffix
  } else if (n + suffix + 1 >= sizeof (s)) {
    return false;                       // suffix and separator do not fit
  } else if (s [n - 1] == '_' || proto [proto_len - suffix] == '_') {
    // add the suffix with no separator
    ASSERT (n + suffix < sizeof (s));
    memcpy (s + n, proto + proto_len - suffix, suffix);
    n += suffix;
  } else {
    // add the suffix with a separator
    ASSERT (n + suffix + 1 < sizeof (s));
    s [n++] = '_';
    memcpy (s + n, proto + proto_len - suffix, suffix);
    n += suffix;
  }
  if (n >= sizeof (s)) {
    return false;                       // not enough room for the null
  } else {
    s [n] = '\0';
  }
  *atom = gensym (leader, s, NULL, false);
  return true;
}

//! Try to make a name by concating all the scalars
bool NUScope::makeLongestName (const char *leader, const UtList <const char *> &names, StringAtom **atom)
{
  unsigned int n = 0;
  char s [cMAX_NAME_LENGTH+1];
  for (UtList <const char *>::const_reverse_iterator it = names.rbegin (); it != names.rend (); it++) {
    if (it == names.rbegin ()) {
      // first identifier; no separater
    } else if (n >= sizeof (s) - 2) {
      return false;                     // not enough room
    } else {
      // separate with two underscores
      s [n++] = '_';
      s [n++] = '_';
    }
    const char *netname = *it;
    unsigned int j = 0;
    while (netname [j] != '\0') {
      if (n >= sizeof (s) - 1) {
        // does not fit into the buffer... give up on the longest name
        return false;
      } else if (netname [j] != '$' && netname [j] != ';') {
        // just copy the character from the netname to the merged name
        s [n++] = netname [j++];
#if 0
      } else if (strip (netname, &j)) {
        // strip advanced j
#endif
      } else {
        // the strip routine did not pull out any cbuild junk so just copy the character
        s [n++] = netname [j++];
      }
    }
  }
  s [n++] = '\0';                       // null terminate
  *atom = gensym (leader, s, NULL, false);
  return true;
}

//! Try to make a name from the first and last scalars
bool NUScope::makeMediumName (const char *leader, const UtList <const char *> &names, StringAtom **atom)
{
  unsigned int n = 0;
  // Freeze has checked that we have at least two scalars, so begin and rbegin
  // should give us different names. Try to construct a name of the form firstnet__lastnet.
  char s [cMAX_NAME_LENGTH+1];
  const char *bookend [2] = {names.back (), names.front ()};
  for (UInt32 i = 0; i < 2; i++) {
    if (i == 0) {
      // first identifier; no separater
    } else if (n >= sizeof (s)) {
      return false;                     // not enough room
    } else {
      // separate with two underscores
      s [n++] = '_';
      s [n++] = '_';
    }
    const char *netname = bookend [i];
    unsigned int j = 0;
    while (netname [j] != '\0') {
      if (n >= sizeof (s) - 1) {
        // does not fit into the buffer... give up on the medium name
        return false;
      } else if (netname [j] != '$' && netname [j] != ';') {
        // just copy the character from the netname to the merged name
        s [n++] = netname [j++];
#if 0
      } else if (strip (netname, &j)) {
        // strip advanced j
#endif
      } else {
        // the strip routine did not pull out any cbuild junk so just copy the character
        s [n++] = netname [j++];
      }
    }
  }
  s [n++] = '\0';                       // null terminate
  *atom = gensym (leader, s, NULL, false);
  return true;
}
