// -*-C++-*-
/******************************************************************************
 Copyright (c) 2006-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUScope.h"
#include "util/UtIOStream.h"


NUCompositeNet::NUCompositeNet( const StringAtom*  typeMark,
                                const NUNetVector& fields,
                                const ConstantRangeVector& ranges,
                                bool packed,
                                NUScope *scope,
                                StringAtom *name,
                                const SourceLocator& loc,
                                NetFlags flags):
  NUNet( name, flags, scope, loc ),
  mTypeMark( typeMark ),mFields(fields), mRanges( ranges ), mFirstUnpackedDimensionIndex(0), mPacked( packed ), mResynthesized( false )
{
}

// Steve Lim 2013-11-23: Added a variant of the constructor that takes a ConstantRangeArray
// ranges from which it will construct a ConstantRangeVector. This facilitates the common
// use of VerificVhdlDesignWalker::extractArrayDimensions implemented for processing of
// multi-dimensional arrays targetting a NUMemoryNet which takes a ConstantRangeArray for
// its index ranges.
// Take care to delete the individual ConstantRange objects after we're done populating the
// ConstantRangeVector.
NUCompositeNet::NUCompositeNet( const StringAtom*  typeMark,
                                const NUNetVector& fields,
                                ConstantRangeArray& rangeArray,
                                bool packed,
                                NUScope *scope,
                                StringAtom *name,
                                const SourceLocator& loc,
                                NetFlags flags):
  NUNet( name, flags, scope, loc ),
  mTypeMark( typeMark ),mFields(fields), mFirstUnpackedDimensionIndex(0), mPacked( packed ), mResynthesized( false )
{
  ConstantRangeVector rangeVector;
  for (UInt32 i = 0; i < rangeArray.size(); i++) {
    ConstantRange* range = rangeArray[i];
    rangeVector.push_back(*range);
    rangeArray[i] = NULL;
    delete range;
  }
  mRanges = rangeVector;
}
NUCompositeNet::~NUCompositeNet()
{
}


UInt32
NUCompositeNet::getBitSize() const
{
  UInt64 totalSize = 0;
  const UInt32 numFields = mFields.size();
  for ( UInt32 i = 0; i < numFields; ++i )
  {
    totalSize += mFields[i]->getBitSize();
  }
  const UInt32 numDims = mRanges.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    totalSize *= mRanges[i].getLength();
  }
  if ( totalSize > 0x00000000ffffffffULL )
    return 0;
  return (UInt32)totalSize;
}


const char*
NUCompositeNet::typeStr() const
{
  return "NUCompositeNet";
}

StringAtom* NUCompositeNet::getOriginalName() const
{
  // The declaration scope for composite has the original name of the composite.
  // The composite itself has a generated internal name.
  return getDeclScopeForComposite()->getName();
}

NUScope* NUCompositeNet::getDeclScopeForComposite() const
{
  NUNet* field = getField(0);
  if (field->isCompositeNet()) { // If field 0 is composite, get it's declaration scope.
    NUCompositeNet* compField = field->getCompositeNet();
    NUScope* compFieldScope = compField->getDeclScopeForComposite();
    return compFieldScope->getParentScope();
  }
  return field->getScope();
}

CGContext_t
NUCompositeNet::emitCode (CGContext_t) const
{ 
  NU_ASSERT( false, this );
  return 0;
}

// this method is used to print a verilog description, thus we must follow the dimension order used in the verilog, systemverilog language
void
NUCompositeNet::composeDeclaration( UtString *buf ) const
{
  *buf << getName()->str();
  const UInt32 numDims = mRanges.size();
  *buf << (( numDims != 0 ) ? " " : "");

  composeDeclareSuffix(buf);

  *buf << ";";
}


void
NUCompositeNet::composeDeclarePrefix( UtString *buf ) const
{
  if ( isPacked() )
    *buf << "packed ";
}


void
NUCompositeNet::printNameSize() const
{
  if(isHierRef())
    UtIO::cout() << "CompositeNetHierRef(";
  else
    UtIO::cout() << "CompositeNet(";

  UtIO::cout() << this << ") : " << mTypeMark->str() << " " << getName()->str();

  const UInt32 numDims = mRanges.size();
  if ( numDims > 0 )
    UtIO::cout() << " ";
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mRanges[i].print();       // here we print them in the order the ranges are stored in mRanges, not the order they appear in the HDL
  }
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    if ( ! i ) { UtIO::cout() << " : "; }
    UtIO::cout() << "[size=" << mRanges[i].getLength() << "]";
  }
    UtIO::cout() << " 1stUnpkdDimIdx=" << mFirstUnpackedDimensionIndex;
}


NUNet*
NUCompositeNet::getField( UInt32 fieldIndex ) const
{
  NU_ASSERT( fieldIndex < getNumFields(), this );
  return mFields[fieldIndex];
}


const ConstantRange*
NUCompositeNet::getRange( const UInt32 index ) const
{
  NU_ASSERT( index < getNumDims(), this );
  return &mRanges[index];
}


void
NUCompositeNet::createExprVector( NUExprVector* exprVector ) const
{
  const UInt32 size = getNumFields();
  NU_ASSERT( getNumDims() == 0, this );
  for ( UInt32 i = 0; i < size; ++i )
  {
    NUNet *field = getField(i);
    if ( field->isCompositeNet() )
    {
      field->getCompositeNet()->createExprVector( exprVector );
    }
    else
    {
      NUIdentRvalue *ident = new NUIdentRvalue( field, field->getLoc() );
      exprVector->push_back( ident );
    }
  }
}


void
NUCompositeNet::createLvalueVector( NULvalueVector* lvalueVector ) const
{
  NU_ASSERT( getNumDims() == 0, this );
  const UInt32 size = getNumFields();
  for ( UInt32 i = 0; i < size; ++i )
  {
    NUNet *field = getField(i);
    if ( field->isCompositeNet() )
    {
      field->getCompositeNet()->createLvalueVector( lvalueVector );
    }
    else
    {
      NUIdentLvalue *ident = new NUIdentLvalue( field, field->getLoc() );
      lvalueVector->push_back( ident );
    }
  }
}


void
NUCompositeNet::createNetVector( NUNetVector *netVector ) const
{
  NU_ASSERT( getNumDims() == 0, this );
  const UInt32 size = getNumFields();
  for ( UInt32 i = 0; i < size; ++i )
  {
    NUNet *field = getField(i);
    if ( field->isCompositeNet() )
    {
      field->getCompositeNet()->createNetVector( netVector );
    }
    else
    {
      netVector->push_back( field );
    }
  }
}


void
NUCompositeNet::putField( NUNet *new_net, UInt32 fieldIndex )
{
  NU_ASSERT( fieldIndex < mFields.size(), this );
  mFields[fieldIndex] = new_net;
}


void
NUCompositeNet::composeDeclareSuffix(UtString *buf) const
{
  const UInt32 numDims = mRanges.size();

  // for NUCompositeNets there currently are no packed dimensions, but use the mFirstUnpackedDimensionIndex anyway in case this changes in the future
  // the slowest changing dimension is printed first.
  // this may need to be revisited once systemverilog packed structs are implemented
  for ( SInt32 i = numDims-1; i >= (static_cast<SInt32>(mFirstUnpackedDimensionIndex)); --i ) {
    mRanges[i].format( buf );
  }  

}
