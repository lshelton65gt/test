// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUIf.h"
#include "nucleus/NUExpr.h"

#include "util/CbuildMsgContext.h"
#include "util/UtIndent.h"

NUIf::NUIf(NUExpr *cond,
	   const NUStmtList& then_stmts,
	   const NUStmtList& else_stmts,
	   NUNetRefFactory *netref_factory,
           bool usesCFNet,
	   const SourceLocator& loc) :
  NUBlockStmt(netref_factory, usesCFNet, loc),
  mCond(cond),
  mThenStmts(then_stmts),
  mElseStmts(else_stmts)
{
}


NUIf::NUIf(NUExpr *cond,
	   NUNetRefFactory *netref_factory,
           bool usesCFNet,
	   const SourceLocator& loc) :
  NUBlockStmt(netref_factory, usesCFNet, loc),
  mCond(cond)
{
}


NUIf::~NUIf()
{
  clearUseDef();

  delete mCond;

  while (not mThenStmts.empty()) {
    delete *(mThenStmts.begin());
    mThenStmts.erase(mThenStmts.begin());
  }

  while (not mElseStmts.empty()) {
    delete *(mElseStmts.begin());
    mElseStmts.erase(mElseStmts.begin());
  }
}


NUStmtList *NUIf::getThen() const
{
  return new NUStmtList(mThenStmts.begin(), mThenStmts.end());
}


NUStmtList *NUIf::getElse() const
{
  return new NUStmtList(mElseStmts.begin(), mElseStmts.end());
}

NUStmtList *NUIf::getThenList()
{
  return &mThenStmts;
}

NUStmtList *NUIf::getElseList() 
{
  return &mElseStmts;
}


bool NUIf::emptyThen() const
{
  return mThenStmts.empty();
}

bool NUIf::emptyElse() const
{
  return mElseStmts.empty();
}



bool NUIf::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUIf* ifstmt = dynamic_cast<const NUIf *>(&stmt);

  if ((*mCond) == (*ifstmt->getCond ()))
    // Must be identical - consider: "($signed(a) > 1)"
    // vs. "($unsigned(a) > 1)".  These only differ in mSignedResult
    // flags, but are not equivalent.  So cannot use NUExpr::equal()
    //
    return ObjListEqual<NUStmtCLoop>(loopThen (), ifstmt->loopThen ())
      && ObjListEqual<NUStmtCLoop>(loopElse (), ifstmt->loopElse ());

  return false;
}

//! Add the given statement to the end of the block
void NUIf::addThenStmt(NUStmt *stmt)
{
  mThenStmts.push_back(stmt);
}

//! Add the given statement to the end of the block
void NUIf::addElseStmt(NUStmt *stmt)
{
  mElseStmts.push_back(stmt);
}

//! Add the given statements to the end of the block
void NUIf::addThenStmts(NUStmtList *stmts)
{
  mThenStmts.insert(mThenStmts.end(), stmts->begin(), stmts->end());
}

//! Add the given statements to the end of the block
void NUIf::addElseStmts(NUStmtList *stmts)
{
  mElseStmts.insert(mElseStmts.end(), stmts->begin(), stmts->end());
}

//! Add the given statement to the start of the block
void NUIf::addThenStmtStart(NUStmt *stmt)
{
  mThenStmts.push_front(stmt);
}

//! Add the given statement to the start of the block
void NUIf::addElseStmtStart(NUStmt *stmt)
{
  mElseStmts.push_front(stmt);
}

void NUIf::replaceCond(NUExpr* cond)
{
  mCond = cond;
}


void NUIf::replaceThen(const NUStmtList& new_stmts)
{
  mThenStmts.assign(new_stmts.begin(), new_stmts.end());
}


void NUIf::replaceElse(const NUStmtList& new_stmts)
{
  mElseStmts.assign(new_stmts.begin(), new_stmts.end());
}


void NUIf::removeThenStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mThenStmts, stmt, replacement);
}


void NUIf::removeElseStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mElseStmts, stmt, replacement);
}


void NUIf::replaceDef(NUNet* old_net,
		      NUNet* new_net)
{
  for (NUStmtListIter iter = mThenStmts.begin(); iter != mThenStmts.end(); ++iter)
    (*iter)->replaceDef(old_net,new_net);
  for (NUStmtListIter iter = mElseStmts.begin(); iter != mElseStmts.end(); ++iter)
    (*iter)->replaceDef(old_net,new_net);
}


bool NUIf::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed |= mCond->replace(old_net,new_net);
  for (NUStmtListIter iter = mThenStmts.begin(); iter != mThenStmts.end(); ++iter)
    changed |= (*iter)->replace(old_net,new_net);
  for (NUStmtListIter iter = mElseStmts.begin(); iter != mElseStmts.end(); ++iter)
    changed |= (*iter)->replace(old_net,new_net);
  return changed;
}

bool NUIf::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  NUExpr * repl = translator(mCond, NuToNuFn::ePre);
  if (repl) {
    mCond = repl;
    changed = true;
  }
  changed |= mCond->replaceLeaves(translator);
  for (NUStmtListIter iter = mThenStmts.begin(); iter != mThenStmts.end(); ++iter)
    changed |= (*iter)->replaceLeaves(translator);
  for (NUStmtListIter iter = mElseStmts.begin(); iter != mElseStmts.end(); ++iter)
    changed |= (*iter)->replaceLeaves(translator);

  repl = translator(mCond, NuToNuFn::ePost);
  if (repl) {
    mCond = repl;
    changed = true;
  }

  return changed;
}

void NUIf::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "If(" << this << ")" << UtIO::endl;
  if (recurse) {
    if ( mCond )
      mCond->print(recurse, numspaces+2);
    else
    {
      indent(numspaces + 2);
      UtIO::cout() << "(null condition pointer)" << UtIO::endl;
    }
    for (NUStmtListIter iter = mThenStmts.begin(); iter != mThenStmts.end(); iter++) {
      (*iter)->print(recurse, numspaces+2);
    }
    for (NUStmtListIter iter = mElseStmts.begin(); iter != mElseStmts.end(); iter++) {
      (*iter)->print(recurse, numspaces+2);
    }
  }
}

void NUIf::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  *buf << "if (";
  mCond->compose(buf, scope);
  *buf << ") begin";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
  for (NUStmtListIter iter=mThenStmts.begin (); iter != mThenStmts.end (); ++iter)
  {
    (*iter)->compose(buf, scope, numSpaces + 2, recurse);
  }
  buf->append(numSpaces, ' ');
  *buf << "end";
  if ( not mElseStmts.empty() )
  {
    *buf << "\n";
    buf->append(numSpaces, ' ');
    *buf << "else begin\n";
    for (NUStmtListIter iter=mElseStmts.begin (); iter != mElseStmts.end (); ++iter)
    {
      (*iter)->compose(buf, scope, numSpaces + 2, recurse);
    }
    buf->append(numSpaces, ' ');
    *buf << "end";
  }
  *buf << "\n";
}

void MsgContext::composeLocation(const NUIf *if_stmt, UtString* location)
{
  if (if_stmt == 0) {
    *location << "(null NUIf)";
  } else {
    const SourceLocator& loc = if_stmt->getLoc();
    UtString source;
    loc.compose(&source);
    *location << source;
  }
}


const char* NUIf::typeStr() const
{
  return "NUIf";
}


NUStmt* NUIf::copy(CopyContext &copy_context) const
{
  NUStmtList copy_then;
  for (NUStmtListIter iter = mThenStmts.begin(); iter != mThenStmts.end(); ++iter) {
    copy_then.push_back((*iter)->copy(copy_context));
  }

  NUStmtList copy_else;
  for (NUStmtListIter iter = mElseStmts.begin(); iter != mElseStmts.end(); ++iter) {
    copy_else.push_back((*iter)->copy(copy_context));
  }

  NUExpr *copy_cond = mCond?mCond->copy(copy_context):NULL;
  return new NUIf(copy_cond, copy_then, copy_else, mNetRefFactory, getUsesCFNet(), mLoc);
}
