// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/




/*!
  \file
  TBD
*/

#include "nucleus/NUBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "util/CarbonAssert.h"
#include "util/CarbonTypes.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"
#include "util/UtIndent.h"

// please do not use "using namespace std"

NUInitialBlock::NUInitialBlock(StringAtom * name,
                               NUBlock *block,
			       NUNetRefFactory *netref_factory,
			       const SourceLocator& loc) :
  NUStructuredProc(name, block, netref_factory, loc)
{
}


NUInitialBlock::~NUInitialBlock()
{
}


const char* NUInitialBlock::typeStr() const
{
  return "NUInitialBlock";
}


void NUInitialBlock::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "InitialBlock(" << this << ") : " << getName()->str() << " : ";
  UtIO::cout() << UtIO::endl;
  if (recurse) {
    NUStructuredProc::print(recurse, numspaces+2);
  }
}

void NUInitialBlock::compose(UtString* buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  *buf << "initial ";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
  NUStructuredProc::compose(buf, scope, numSpaces + 2, recurse);
}

bool NUInitialBlock::isSimilar(const NUStructuredProc& /*other*/)
  const
{
  return false;
}

NUInitialBlock* NUInitialBlock::clone(CopyContext& copy_context) const {
  NUStmt * block_copy = getBlock()->copy(copy_context);
  NUBlock * block = dynamic_cast<NUBlock*>(block_copy);
  NU_ASSERT(block, block_copy);

  const SourceLocator& loc = getLoc();
  StringAtom* name = copy_context.mScope->newBlockName(copy_context.mStrCache,
                                                        loc);
  return new NUInitialBlock(name, block, mNetRefFactory, loc);
}
