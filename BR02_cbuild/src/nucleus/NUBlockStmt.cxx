// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUBlockStmt.h"
#include "nucleus/NUNetSet.h"

NUBlockStmt::~NUBlockStmt()
{
  clearUseDef();
}

void NUBlockStmt::clearUseDef()
{
  mBlockNetRefDefMap.clear();
  mNonBlockNetRefDefMap.clear();
  mBlockingNetRefKillSet.clear();
  mNonBlockingNetRefKillSet.clear();
  putUsesCFNet(false);
}


void NUBlockStmt::fixupUseDef(NUNetList *remove_list)
{
  helperFixupUseDef(&mBlockNetRefDefMap, remove_list);
  helperFixupUseDef(&mBlockingNetRefKillSet, remove_list);
  helperFixupUseDef(&mNonBlockNetRefDefMap, remove_list);
  helperFixupUseDef(&mNonBlockingNetRefKillSet, remove_list);
}


#ifdef CDB
static NUBlockStmt* sBlockStmtCheck = NULL;
static NUNet* sBlockStmtCheckNet = NULL;

static void sCheck(NUBlockStmt* stmt, NUNet* net) {
  if (((stmt == sBlockStmtCheck) || (sBlockStmtCheck == NULL)) &&
      (net == sBlockStmtCheckNet))
  {
    UtIO::cout() << "stop here\n";
  }
}
#define BLOCK_CHECK(net_ref) sCheck(this, net_ref->getNet())
#else
#define BLOCK_CHECK(net_ref)
#endif


void NUBlockStmt::addBlockingDef(const NUNetRefHdl &net_ref)
{
  BLOCK_CHECK(net_ref);
  mBlockNetRefDefMap.insert(net_ref);
}


void NUBlockStmt::addNonBlockingDef(NUNet * net)
{
  // Obsolete
  NU_ASSERT(0, net);
}


void NUBlockStmt::addNonBlockingDef(const NUNetRefHdl &net_ref)
{
  mNonBlockNetRefDefMap.insert(net_ref);
}


void NUBlockStmt::addBlockingUse(const NUNetRefHdl &def_net_ref,
				 const NUNetRefHdl &use_net_ref)
{
  BLOCK_CHECK(def_net_ref);
  mBlockNetRefDefMap.insert(def_net_ref, use_net_ref);
}


void NUBlockStmt::addNonBlockingUse(NUNet * def_net, NUNet * use_net)
{
  // Obsolete
  NU_ASSERT2(0, def_net, use_net);
}


void NUBlockStmt::addNonBlockingUse(const NUNetRefHdl &def_net_ref,
				    const NUNetRefHdl &use_net_ref)
{
  mNonBlockNetRefDefMap.insert(def_net_ref, use_net_ref);
}


void NUBlockStmt::getBlockingUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getBlockingUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUBlockStmt::getBlockingUses(NUNetRefSet *uses) const
{
  mBlockNetRefDefMap.populateMappedToNonEmpty(uses);
}


bool NUBlockStmt::queryBlockingUses(const NUNetRefHdl &use_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory * /* factory -- unused */) const
{
  return mBlockNetRefDefMap.queryMappedTo(use_net_ref, fn);
}


void NUBlockStmt::getNonBlockingUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getNonBlockingUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUBlockStmt::getNonBlockingUses(NUNetRefSet *uses) const
{
  mNonBlockNetRefDefMap.populateMappedToNonEmpty(uses);
}


bool NUBlockStmt::queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory * /* factory -- unused */) const
{
  return mNonBlockNetRefDefMap.queryMappedTo(use_net_ref, fn);
}


void NUBlockStmt::getBlockingUses(NUNet *net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getBlockingUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUBlockStmt::getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  mBlockNetRefDefMap.populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUBlockStmt::queryBlockingUses(const NUNetRefHdl &def_net_ref,
                                    const NUNetRefHdl &use_net_ref,
                                    NUNetRefCompareFunction fn,
                                    NUNetRefFactory * /* factory -- unused */) const
{
  return mBlockNetRefDefMap.queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}


void NUBlockStmt::getNonBlockingUses(NUNet *net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getNonBlockingUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUBlockStmt::getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  mNonBlockNetRefDefMap.populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUBlockStmt::queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
				       const NUNetRefHdl &use_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory * /* factory -- unused */) const
{
  return mNonBlockNetRefDefMap.queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}


void NUBlockStmt::getBlockingDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getBlockingDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin(); iter != ref_defs.end(); ++iter) {
    defs->insert((*iter)->getNet());
  }
}


void NUBlockStmt::getBlockingDefs(NUNetRefSet *defs) const
{
  mBlockNetRefDefMap.populateMappedFrom(defs);
}


bool NUBlockStmt::queryBlockingDefs(const NUNetRefHdl &def_net_ref,
				    NUNetRefCompareFunction fn,
				    NUNetRefFactory * /* factory -- unused */) const
{
  return mBlockNetRefDefMap.queryMappedFrom(def_net_ref, fn);
}


void NUBlockStmt::getNonBlockingDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getNonBlockingDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin(); iter != ref_defs.end(); ++iter) {
    defs->insert((*iter)->getNet());
  }
}


void NUBlockStmt::getNonBlockingDefs(NUNetRefSet *defs) const
{
  mNonBlockNetRefDefMap.populateMappedFrom(defs);
}


bool NUBlockStmt::queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
				       NUNetRefCompareFunction fn,
				       NUNetRefFactory * /* factory -- unused */) const
{
  return mNonBlockNetRefDefMap.queryMappedFrom(def_net_ref, fn);
}


void NUBlockStmt::addBlockingKill(const NUNetRefHdl &net_ref)
{
  mBlockingNetRefKillSet.insert(net_ref);
}


void NUBlockStmt::addNonBlockingKill(NUNet * net)
{
  // Obsolete
  NU_ASSERT(0, net);
}


void NUBlockStmt::addNonBlockingKill(const NUNetRefHdl &net_ref)
{
  mNonBlockingNetRefKillSet.insert(net_ref);
}


void NUBlockStmt::getBlockingKills(NUNetSet *kills) const
{
  for (NUNetRefSet::const_iterator iter = mBlockingNetRefKillSet.begin();
       iter != mBlockingNetRefKillSet.end();
       ++iter) {
    if ((*iter)->all()) {
      kills->insert((*iter)->getNet());
    }
  }
}


void NUBlockStmt::getBlockingKills(NUNetRefSet *kills) const
{
  kills->insert(mBlockingNetRefKillSet.begin(), mBlockingNetRefKillSet.end());
}


bool NUBlockStmt::queryBlockingKills(const NUNetRefHdl &def_net_ref,
				     NUNetRefCompareFunction fn,
				     NUNetRefFactory * /* factory -- unused */) const
{
  return (mBlockingNetRefKillSet.find(def_net_ref, fn) != mBlockingNetRefKillSet.end());
}


void NUBlockStmt::getNonBlockingKills(NUNetSet *kills) const
{
  for (NUNetRefSet::const_iterator iter = mNonBlockingNetRefKillSet.begin();
       iter != mNonBlockingNetRefKillSet.end();
       ++iter) {
    if ((*iter)->all()) {
      kills->insert((*iter)->getNet());
    }
  }
}


void NUBlockStmt::getNonBlockingKills(NUNetRefSet *kills) const
{
  kills->insert(mNonBlockingNetRefKillSet.begin(), mNonBlockingNetRefKillSet.end());
}


bool NUBlockStmt::queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
					NUNetRefCompareFunction fn,
					NUNetRefFactory * /* factory -- unused */) const
{
  return (mNonBlockingNetRefKillSet.find(def_net_ref, fn) != mNonBlockingNetRefKillSet.end());
}


void NUBlockStmt::getVisibleBlockingUses(NUStmtLoop loop, NUNetRefSet * visible_uses)
{
  NUNetRefSet kills(visible_uses->getFactory());
  for (/*no initial*/; not loop.atEnd(); ++loop) {
    NUStmt * stmt = (*loop);
    NUNetRefSet uses(visible_uses->getFactory());

    // Visible uses are those which have not been previously covered by a kill.
    stmt->getBlockingUses(&uses);
    NUNetRefSet::set_difference(uses, kills, *visible_uses);
    stmt->getBlockingKills(&kills);
  }
}
