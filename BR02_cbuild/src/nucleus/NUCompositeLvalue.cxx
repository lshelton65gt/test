/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUNetRefSet.h"

/*!
  \file
  Implementation of NUCompositeLvalue and friends.
*/
NUCompositeLvalue::NUCompositeLvalue( const NULvalueVector& lvalues, const SourceLocator& loc )
  : NUConcatLvalue( lvalues, loc ),
    mIsCompositeArray(false)
{}


const char *
NUCompositeLvalue::typeStr() const
{
  return "NUCompositeLvalue";
}

void NUCompositeLvalue::setIsCompositeArray(bool isCompositeArray)
{
  mIsCompositeArray = isCompositeArray;
}

bool NUCompositeLvalue::isCompositeArray() const
{
  return mIsCompositeArray;
}

NULvalue* NUCompositeLvalue::copy(CopyContext &copy_context) const
{
  NULvalueVector mLvalues_copy;
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    NULvalue * lvalue = (*iter);
    mLvalues_copy.push_back( lvalue->copy(copy_context) );
  }
  NUCompositeLvalue* comp_lval = new NUCompositeLvalue(mLvalues_copy, mLoc);
  comp_lval->setIsCompositeArray(mIsCompositeArray);
  return comp_lval;
}


NUCompositeInterfaceLvalue::NUCompositeInterfaceLvalue( const SourceLocator& loc )
  : NULvalue( loc )
{}


NUCompositeInterfaceLvalue::~NUCompositeInterfaceLvalue() {}


void
NUCompositeInterfaceLvalue::getUses(NUNetSet *) const
{
  // Composites don't contribute to uses or defs.
}


void
NUCompositeInterfaceLvalue::getUses(NUNetRefSet *) const
{
  // Composites don't contribute to uses or defs.
}


bool
NUCompositeInterfaceLvalue::queryUses(const NUNetRefHdl &,
                                    NUNetRefCompareFunction ,
                                    NUNetRefFactory *) const
{
  NU_ASSERT( false, this );
  return false;
}


bool
NUCompositeInterfaceLvalue::replace(NUNet *, NUNet *)
{
  NU_ASSERT( false, this );
  return false;
}


bool
NUCompositeInterfaceLvalue::replaceLeaves(NuToNuFn &)
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeInterfaceLvalue::calcCost(NUCost*, NUCostContext*) const
{
  NU_ASSERT( false, this );
}


void
NUCompositeInterfaceLvalue::getUses(const NUNetRefHdl &, NUNetRefSet *) const
{
  NU_ASSERT( false, this );
}


size_t
NUCompositeInterfaceLvalue::hash() const
{
  NU_ASSERT( false, this );
  return (size_t)0;
}


ptrdiff_t
NUCompositeInterfaceLvalue::compareHelper(const NULvalue*, bool, bool) const
{
  NU_ASSERT( false, this );
  return (ptrdiff_t)0;
}


bool
NUCompositeInterfaceLvalue::queryUses(const NUNetRefHdl &,
                                      const NUNetRefHdl &,
                                      NUNetRefCompareFunction ,
                                      NUNetRefFactory *) const
{
  NU_ASSERT( false, this );
  return false;
}


//! Add the nets this node defines to the given set.
void
NUCompositeInterfaceLvalue::getDefs(NUNetSet *) const
{
  // Composites don't contribute to uses or defs.
}


void
NUCompositeInterfaceLvalue::getDefs(NUNetRefSet *) const
{
  // Composites don't contribute to uses or defs.
}


bool
NUCompositeInterfaceLvalue::queryDefs(const NUNetRefHdl &,
                                      NUNetRefCompareFunction ,
                                      NUNetRefFactory *) const
{
  NU_ASSERT( false, this );
  return false;
}


bool
NUCompositeInterfaceLvalue::isHierRef() const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeInterfaceLvalue::getCompleteDefs(NUNetSet *) const
{
  NU_ASSERT( false, this );
}


void
NUCompositeInterfaceLvalue::getCompleteDefs(NUNetRefSet *) const
{
  NU_ASSERT( false, this );
}


bool
NUCompositeInterfaceLvalue::queryCompleteDefs(const NUNetRefHdl &,
                                              NUNetRefFactory *) const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeInterfaceLvalue::replaceDef(NUNet *, NUNet* )
{
  NU_ASSERT( false, this );
}


NUExpr*
NUCompositeInterfaceLvalue::NURvalue() const
{
  NU_ASSERT( false, this );
  return NULL;
}


//! Does the RValue represent the same object
bool
NUCompositeInterfaceLvalue::equalAddr(const NUExpr *) const
{
  NU_ASSERT( false, this );
  return false;
}

bool NUCompositeInterfaceLvalue::hasSelects(NUExprVector* selVector) const
{
  bool hasSel = false;
  switch (getCompositeType())
  {
  case eCompositeIdentLvalue:
  {
    // No indices in a net and no nodes above me
    break;
  }
  case eCompositeSelLvalue:
  {
    // Look for indices in the parents first.
    hasSel = getIdentExpr()->hasSelects(selVector);
    const NUCompositeSelLvalue *csLvalue = dynamic_cast<const NUCompositeSelLvalue*>(this);
    UInt32 numLocalIndices = csLvalue->getNumIndices();
    for (UInt32 i = 0; (selVector != NULL) && (i < numLocalIndices); ++i)  {
      selVector->push_back(csLvalue->getIndex(i));
    }
    hasSel |= (numLocalIndices > 0);
    break;
  }
  case eCompositeFieldLvalue:
  {
    // No indices in a field, but there may be some in parent.
    hasSel = getIdentExpr()->hasSelects(selVector);
    break;
  }
  default:
  {
    NU_ASSERT(false, this);
    break;
  }
  } // switch
  return hasSel;
}

NULvalue*
NUCompositeInterfaceLvalue::copy(CopyContext &) const
{
  NU_ASSERT( false, this );
  return NULL;
}


NUCompositeSelLvalue::NUCompositeSelLvalue( NUCompositeInterfaceLvalue *object,
                                            NUExprVector *exprs,
                                            const SourceLocator& loc) :
  NUCompositeInterfaceLvalue(loc), mComposite(object), mRange(0, 0),
  mIsDummyOffset(false), mIsPartSelect(false)
{
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
}

NUCompositeSelLvalue::NUCompositeSelLvalue( NUCompositeInterfaceLvalue *object,
                                            NUExprVector* exprs,
                                            const ConstantRange range,
                                            bool isDummyOffset,
                                            const SourceLocator& loc ) :
  NUCompositeInterfaceLvalue(loc), mComposite(object), mRange(range),
  mIsDummyOffset(isDummyOffset), mIsPartSelect(true)
{
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
}


NUCompositeSelLvalue::~NUCompositeSelLvalue()
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    delete mExprs[i];
  }
  delete mComposite;
}


const char *
NUCompositeSelLvalue::typeStr() const
{
  return "NUCompositeSelLvalue";
}


NUNet*
NUCompositeSelLvalue::getNet() const
{
  return mComposite->getNet();
}


void
NUCompositeSelLvalue::addIndexExpr( NUExpr *expr )
{
  // Cannot add any more indices to a part select of a composite.
  NU_ASSERT(!mIsPartSelect, this);
  mExprs.push_back( expr );
}


bool
NUCompositeSelLvalue::isHierRef() const
{
//  return mComposite->isHierRef();
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeSelLvalue::getUses(NUNetSet *uses) const
{
  // Composites don't contribute to uses or defs, however their selects do.
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
}


void
NUCompositeSelLvalue::getUses(NUNetRefSet *uses) const
{
  // Composites don't contribute to uses or defs, however their selects do.
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
}


bool
NUCompositeSelLvalue::queryUses(const NUNetRefHdl &use_net_ref,
                                NUNetRefCompareFunction fn,
                                NUNetRefFactory *factory) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    if ( mExprs[i]->queryUses(use_net_ref, fn, factory))
      return true;
  }
  return false;
}


void
NUCompositeSelLvalue::getUses(const NUNetRefHdl & /* net_ref -- unused */, NUNetRefSet *uses) const
{
  // Composites don't contribute to uses or defs, however their selects do.
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
}


void
NUCompositeSelLvalue::compose(UtString* buf, const STBranchNode* scope, int, bool) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  mComposite->compose( buf, scope, 0, false );

  UInt32 numIndices = mExprs.size();
  for( UInt32 i = 0; i < numIndices; ++i )
  {
    *buf << "[";
    bool isConst = false;
    bool printRange = false;
    NUExpr* expr = mExprs[i];

    if ((i == numIndices-1) && mIsPartSelect)
    {
      isConst = expr->isConstant();
      if (isConst) {
        expr = NULL; // If index is constant, print part select instead of index.
      }
      // else print index as well as part select.
      printRange = true; // Print part select range.
    }

    
    if (expr != NULL) {
      mExprs[i]->compose(buf, scope, false);
    }

    if (printRange) {
      NUVarselLvalue::printRange(buf, mRange, isConst);
    }

    *buf << "]";
  }
}


bool
NUCompositeSelLvalue::queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
                                const NUNetRefHdl &use_net_ref,
                                NUNetRefCompareFunction fn,
                                NUNetRefFactory *factory) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    if ( mExprs[i]->queryUses(use_net_ref, fn, factory))
      return true;
  }
  return false;
}


void
NUCompositeSelLvalue::getDefs(NUNetSet *defs) const
{
  defs->insert(getCompositeNet());
}


UInt32
NUCompositeFieldLvalue::getNumDims() const
{
  const NUNet *me = getField( mIndex );
  if ( me->isVectorNet( ))
    return 1;
  else if ( me->isMemoryNet( ))
    return me->getMemoryNet()->getNumDims();
  else if ( me->isCompositeNet( ))
    return me->getCompositeNet()->getNumDims();
  else
    return 0; // bitNet
}



const ConstantRange*
NUCompositeSelLvalue::getRange( const UInt32 index ) const
{
  return mComposite->getRange( index );
}

const ConstantRange NUCompositeSelLvalue::getPartSelRange() const
{
  return mRange;
}

void NUCompositeSelLvalue::setPartSelRange(const ConstantRange& psRange)
{
  mRange = psRange;
}

UInt32
NUCompositeSelLvalue::determineBitSize() const
{
  // The base object is SIZE bits.  For every dimension that is indexed
  // we need to divide the total size by that dimension's length.
  const UInt32 baseSize = mComposite->getCompositeSize();
  UInt32 size = baseSize;
  const UInt32 myDims = mExprs.size();
  // maxDims is used to guard against bad things happening in the middle
  // of resynthesis.  This means the size might not be reported
  // correctly during resynthesis.  Specifically, when a selExpr of a
  // CNet is being referenced and the CNet has been resynthesized but
  // the selExpr has not, we'll not report the correct size.  This is
  // only known to occur during debug printing during resynthesis.
  UInt32 maxDims = mComposite->getNumDims();
  if (mComposite->getType() == eNUCompositeSelLvalue)
  {
    NU_ASSERT(mIsPartSelect, this);
    NUCompositeSelLvalue* ident = dynamic_cast<NUCompositeSelLvalue*>(mComposite);
    maxDims -= ident->getNumIndices();
  }

  for ( UInt32 i = 0; i < myDims && i < maxDims; ++i )
  {
    const UInt32 dimLength = mComposite->getRange( maxDims - i - 1 )->getLength();
    size /= dimLength;
    NU_ASSERT( baseSize % dimLength == 0, this );
  }

  size *= mRange.getLength();
  return size;
}


void
NUCompositeSelLvalue::resize()
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->resize(mExprs[i]->determineBitSize());
  }
}


void
NUCompositeSelLvalue::getCompleteDefs(NUNetSet* /*defs*/) const
{
#if 0
  defs->insert( mComposite );
#endif
}


NUExpr*
NUCompositeSelLvalue::NURvalue () const
{
#if 0
  // NUCompositeSelExpr needs to be implemented first.
  CopyContext copy_context(NULL,NULL);
  NUExprVector newVec;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    newVec.push_back( mExprs[i]->copy( copy_context ));
  }
  NUCompositeSelExpr *ret = new NUCompositeSelExpr( mComposite->NURvalue(),
                                                    &newVec, mLoc );
  return ret;
#else
  NU_ASSERT( false, this );
  return NULL;
#endif
}


NULvalue*
NUCompositeSelLvalue::copy(CopyContext &copy_context) const {
  NUExprVector newVec;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    newVec.push_back( mExprs[i]->copy( copy_context ));
  }
  NULvalue *lvalue = mComposite->copy( copy_context );
  NUCompositeInterfaceLvalue *obj = dynamic_cast<NUCompositeInterfaceLvalue*>( lvalue );
  NU_ASSERT( obj, this );
  NUCompositeSelLvalue *ret = NULL;
  if (mIsPartSelect)
  {
    ret = new NUCompositeSelLvalue(obj, &newVec, mRange, mIsDummyOffset, mLoc);
  }
  else
  {
    ret = new NUCompositeSelLvalue( obj, &newVec, mLoc );
  }
  return ret;
}


ptrdiff_t
NUCompositeSelLvalue::compareHelper( const NULvalue *lval, bool cmpLocator,
                                     bool cmpPointer ) const
{
  const NUCompositeSelLvalue *that = dynamic_cast<const NUCompositeSelLvalue*>( lval );
  NU_ASSERT( that, lval );
  ptrdiff_t cmp =  NUNet::compare( getCompositeNet(), that->getCompositeNet() );
  if ( cmp == 0 )
  {
    const UInt32 size = getNumDims();
    for ( UInt32 i = 0; (cmp == 0) && (i < size); ++i )
    {
      cmp = getIndex(i)->compare( *that->getIndex(i), cmpLocator, cmpPointer, false );
    }
  }
  if (cmp == 0)
  {
    cmp = mRange.compare(*(that->getRange(0)));
  }
  return cmp;
}


bool
NUCompositeSelLvalue::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;

  // Handle the composite
  NULvalue *repl = translator(mComposite, NuToNuFn::ePre);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceLvalue*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }
  changed |= mComposite->replaceLeaves( translator );
  repl = translator(mComposite, NuToNuFn::ePost);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceLvalue*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }

  // Handle index expressions
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    NUExpr *repl = translator(mExprs[i], NuToNuFn::ePre);
    if (repl) {
      mExprs[i] = repl;
      changed = true;
    }
    changed |= mExprs[i]->replaceLeaves(translator);
    repl = translator(mExprs[i], NuToNuFn::ePost);
    if (repl) {
      mExprs[i] = repl;
      changed = true;
    }
  }

  return changed;
}


bool
NUCompositeSelLvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed = mComposite->replace( old_net, new_net );

  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    changed |= mExprs[i]->replace(old_net, new_net);
  }

  return changed;
}


void
NUCompositeSelLvalue::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << typeStr() << "(" << this << ") [size=" << getBitSize() << "]";
  if (mIsPartSelect) {
    UtIO::cout() << " : ";
    mRange.print();
  }
  UtIO::cout() << UtIO::endl;
  mComposite->print(recurse, numspaces+2);
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->print(recurse, numspaces+2);
  }
}


UInt32
NUCompositeSelLvalue::getBitSize() const
{
  NUNet* net = mComposite->getNet();
  NU_ASSERT(net->isCompositeNet(), net);
  NUCompositeNet* cnet = net->getCompositeNet();
  UInt32 totalSize = cnet->getBitSize();
  const UInt32 numDims = getNumDims();
  // This will be true unless we're in the middle of resynthesis...
  if ( cnet->getNumDims() >= numDims )
  {
    for ( UInt32 i = 0; i < numDims; ++i )
    {
      totalSize /= cnet->getRange(i)->getLength();
    }
  }
  return totalSize *= mRange.getLength();
}

void
NUCompositeSelLvalue::getCompleteDefs(NUNetRefSet* /*defs*/) const
{
  NU_ASSERT( false, this );
}


bool
NUCompositeSelLvalue::queryCompleteDefs(const NUNetRefHdl &/*def_net_ref*/,
                                        NUNetRefFactory* /*factory*/) const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeSelLvalue::getDefs(NUNetRefSet *defs) const
{
  defs->insert( defs->getFactory()->createNetRef( const_cast<NUCompositeSelLvalue*>(this)->getCompositeNet( )));
}


bool
NUCompositeSelLvalue::queryDefs(const NUNetRefHdl &/*def_net_ref*/,
                                NUNetRefCompareFunction /*fn*/,
                                NUNetRefFactory* /*factory*/) const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeSelLvalue::replaceDef(NUNet* /*old_net*/, NUNet* /*new_net*/)
{
  NU_ASSERT( false, this );
}


bool
NUCompositeSelLvalue::equalAddr( const NUExpr* /*rhs*/ ) const
{
  NU_ASSERT( false, this );
  return false;
}


size_t
NUCompositeSelLvalue::hash() const
{
  NU_ASSERT( false, this );
  return (size_t)0;
}


bool
NUCompositeSelLvalue::operator==( const NULvalue& /*other*/) const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeSelLvalue::calcCost(NUCost* /*cost*/, NUCostContext* /*context*/) const
{
  NU_ASSERT( false, this );
}


NUCompositeFieldLvalue::NUCompositeFieldLvalue( NUCompositeInterfaceLvalue *object,
                                                UInt32 index,
                                                const SourceLocator& loc) :
  NUCompositeInterfaceLvalue( loc ), mComposite( object ), mIndex( index )
{
}


NUCompositeFieldLvalue::~NUCompositeFieldLvalue()
{
  delete mComposite;
}


const char *
NUCompositeFieldLvalue::typeStr() const
{
  return "NUCompositeFieldLvalue";
}


NUNet*
NUCompositeFieldLvalue::getNet() const
{
  NUNet* net = mComposite->getNet();
  NU_ASSERT(net->isCompositeNet(), net);
  return net->getCompositeNet()->getField( mIndex );
}


bool
NUCompositeFieldLvalue::isHierRef() const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeFieldLvalue::compose(UtString* buf, const STBranchNode* scope, int, bool) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  bool  showRoot = false;  // normally do not include root in name.
  if ( scope == NULL )
  {
    NUScope* actualScope = getCompositeNet()->getScope();
    NUScope::ScopeT typeOfScope = actualScope->getScopeType();
    if ( typeOfScope == NUScope::eNamedDeclarationScope ) {
      showRoot = true;          // show local root
    }
  }
  mComposite->compose( buf, scope, 0, showRoot ); // conditionally include root in name.
  const NUNet *me = getField( mIndex );
  const StringAtom *fieldName = me->getName();
  *buf << "." << *fieldName;
}


UInt32
NUCompositeFieldLvalue::determineBitSize() const
{
  return getNet()->getBitSize();  
}


void
NUCompositeFieldLvalue::resize()
{
  mComposite->resize();
}


void
NUCompositeFieldLvalue::getCompleteDefs(NUNetSet *defs) const
{
  defs->insert( getCompositeNet( ));
}


NUExpr*
NUCompositeFieldLvalue::NURvalue () const
{
  NU_ASSERT( false, this );
  return NULL;
}


NULvalue*
NUCompositeFieldLvalue::copy(CopyContext &copy_context) const {
  NUCompositeInterfaceLvalue *cLval = static_cast<NUCompositeInterfaceLvalue*>( mComposite->copy( copy_context ));
  NUCompositeFieldLvalue *ret = new NUCompositeFieldLvalue( cLval, mIndex, mLoc );
  return ret;
}


ptrdiff_t
NUCompositeFieldLvalue::compareHelper( const NULvalue *lval, bool cmpLocator,
                                       bool cmpPointer ) const
{
  const NUCompositeFieldLvalue *that = dynamic_cast<const NUCompositeFieldLvalue*>( lval );
  NU_ASSERT( that, lval );
  ptrdiff_t cmp = mComposite->getCompositeType() - const_cast<NUCompositeFieldLvalue*>(that)->getIdentExpr()->getCompositeType();
  if ( cmp == 0 )
  {
    cmp = mComposite->compareHelper( that->getIdentExpr(), cmpLocator, cmpPointer );
  }
  if ( cmp == 0 )
  {
    cmp = mIndex - that->getFieldIndex();
  }
  return cmp;
}


bool
NUCompositeFieldLvalue::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;

  NULvalue* repl = translator(mComposite, NuToNuFn::ePre);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceLvalue*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }
  changed |= mComposite->replaceLeaves( translator );
  repl = translator(mComposite, NuToNuFn::ePost);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceLvalue*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }
  
  return changed;  
}


bool
NUCompositeFieldLvalue::replace(NUNet * old_net, NUNet * new_net)
{
  return mComposite->replace( old_net, new_net );
}


void
NUCompositeFieldLvalue::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << typeStr() << "(" << this << ") [size=" << getBitSize() << "]\n";
  mComposite->print(recurse, numspaces+2);
  indent(numspaces);
  mLoc.print();
  const NUNet *me = getNet();
  const StringAtom *fieldName = me->getName();
  UtIO::cout() << "Field " << mIndex << " (" << *fieldName << ")" << UtIO::endl;
}


UInt32
NUCompositeFieldLvalue::getBitSize() const
{
  return mComposite->getField( mIndex )->getBitSize();
}

void
NUCompositeFieldLvalue::getCompleteDefs(NUNetRefSet* /*defs*/) const
{
  NU_ASSERT( false, this );
}


bool
NUCompositeFieldLvalue::queryCompleteDefs(const NUNetRefHdl &/*def_net_ref*/,
                                          NUNetRefFactory* /*factory*/) const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeFieldLvalue::replaceDef(NUNet* /*old_net*/, NUNet* /*new_net*/)
{
  NU_ASSERT( false, this );
}


bool
NUCompositeFieldLvalue::equalAddr( const NUExpr* /*rhs*/ ) const
{
  NU_ASSERT( false, this );
  return false;
}


size_t
NUCompositeFieldLvalue::hash() const
{
  NU_ASSERT( false, this );
  return (size_t)0;
}


bool
NUCompositeFieldLvalue::operator==( const NULvalue& /*other*/) const
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeFieldLvalue::calcCost(NUCost* /*cost*/, NUCostContext* /*context*/) const
{
  NU_ASSERT( false, this );
}


UInt32
NUCompositeFieldLvalue::getNumFields() const
{
  NU_ASSERT( false, this );
  return 1;
}


NUNet*
NUCompositeFieldLvalue::getField(UInt32 index) const
{
  NUNet* net = mComposite->getNet();
  NU_ASSERT(net->isCompositeNet(), net);
  return net->getCompositeNet()->getField(index);
}


const ConstantRange*
NUCompositeFieldLvalue::getRange( const UInt32 index ) const
{
  const ConstantRange *retval = NULL;
  const NUNet *me = getField( mIndex );
  if ( me->isVectorNet( ))
  {
    NU_ASSERT( index == 0, this );
    retval = me->castVectorNet()->getRange();
  }
  else if ( me->isMemoryNet( ))
  {
    retval = me->getMemoryNet()->getRange( index );
  }
  else if ( me->isCompositeNet( ))
  {
    retval = me->getCompositeNet()->getRange( index );
  }
  else
    NU_ASSERT( false, this );
  return retval;
}


UInt32
NUCompositeSelLvalue::getCompositeSize() const
{
  return determineBitSize();
}


UInt32
NUCompositeFieldLvalue::getCompositeSize() const
{
  return determineBitSize();
}


//A field reference doesn't really def the entire net, but we record
//it as such because that's the smallest bit that we can guarantee the
//field ref is actually in.
void
NUCompositeFieldLvalue::getDefs(NUNetSet *defs) const
{
  defs->insert(getCompositeNet());
}


//A field reference doesn't really def the entire net, but we record
//it as such because that's the smallest bit that we can guarantee the
//field ref is actually in.
void
NUCompositeFieldLvalue::getDefs(NUNetRefSet *defs) const
{
  defs->insert(defs->getFactory()->createNetRef(getCompositeNet()));
}


NUCompositeIdentLvalue::NUCompositeIdentLvalue( NUCompositeNet *net, const SourceLocator &loc )
  : NUCompositeInterfaceLvalue( loc ), mNet( net )
{}


NUCompositeIdentLvalue::~NUCompositeIdentLvalue()
{
}


NUNet*
NUCompositeIdentLvalue::getNet() const
{
  return mNet;
}


void
NUCompositeIdentLvalue::compose(UtString *buf, const STBranchNode* scope, int, bool) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  bool  showRoot = false;  // normally do not include root in name (test/schedule/cycle1)
  if ( scope == NULL )
  {
    NUScope* actualScope = getIdent()->getScope();
    NUScope::ScopeT typeOfScope = actualScope->getScopeType();
    if ( typeOfScope == NUScope::eNamedDeclarationScope ) {
      showRoot = true;          // show local root (test/fold/nbtest)
    }
  }
  getIdent()->compose(buf, scope, showRoot);
}


UInt32
NUCompositeIdentLvalue::determineBitSize() const
{
  return mNet->getBitSize();
}


UInt32
NUCompositeIdentLvalue::getNumFields() const
{
  return mNet->getNumFields();
}


NUNet *
NUCompositeIdentLvalue::getField( UInt32 fieldIndex ) const
{
  return mNet->getField( fieldIndex );
}


NUCompositeInterfaceLvalue*
NUCompositeIdentLvalue::getIdentExpr() const
{
  return const_cast<NUCompositeIdentLvalue*>( this );
}


UInt32
NUCompositeIdentLvalue::getNumDims() const
{
  return mNet->getNumDims();
}


const ConstantRange*
NUCompositeIdentLvalue::getRange( const UInt32 index ) const
{
  return mNet->getRange( index );
}


UInt32
NUCompositeIdentLvalue::getCompositeSize() const
{
  return mNet->getBitSize();
}


bool
NUCompositeIdentLvalue::operator==(const NULvalue& lval) const
{
  if (getType () != lval.getType ())
    return false;

  const NUCompositeIdentLvalue *lv = dynamic_cast<const NUCompositeIdentLvalue*>(&lval);
  return (mNet == lv->getIdent ()
	  && getBitSize () == lv->getBitSize ());
}


void
NUCompositeIdentLvalue::print(bool /*recurse_arg*/, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << typeStr() << "(" << this << ") [size=" << getBitSize() << "]"
               << UtIO::endl;
  mNet->print(false, numspaces+2);
}


void
NUCompositeIdentLvalue::getDefs(NUNetSet *defs) const
{
  defs->insert(getCompositeNet());
}


void
NUCompositeIdentLvalue::getDefs(NUNetRefSet *defs) const
{
  defs->insert(defs->getFactory()->createNetRef(getCompositeNet()));
}


NULvalue*
NUCompositeIdentLvalue::copy( CopyContext &copy_context ) const
{
  NUCompositeNet *net = dynamic_cast<NUCompositeNet*>( copy_context.lookup( mNet ));
  NU_ASSERT( net, this );
  return new NUCompositeIdentLvalue( net, mLoc );
}


bool
NUCompositeIdentLvalue::replaceLeaves( NuToNuFn &translator )
{
  bool changed = false;
  NUNet * repl = translator(mNet, NuToNuFn::ePrePost);
  if (repl) {
    mNet = repl->getCompositeNet();
    NU_ASSERT( mNet, this );
    changed = true;
  }
  return changed;
}
