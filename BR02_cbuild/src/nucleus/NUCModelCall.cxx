// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"

NUCModelCall::NUCModelCall(NUCModel* cmodel, 
                           NUCModelFn* cmodelFn,
                           const SourceLocator& loc,
                           NUNetRefFactory* netRefFactory,
                           bool usesCFNet) :
  NUBlockStmt(netRefFactory, usesCFNet, loc),
  mCModel(cmodel), mCModelFn(cmodelFn)
{
  mArgs = new NUCModelArgConnectionVector;
}

NUCModelCall::~NUCModelCall()
{
  for (NUCModelArgConnectionVectorIter i = mArgs->begin();
       i != mArgs->end();
       ++i)
    delete *i;
  delete mArgs;
}

void NUCModelCall::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "CModelCall(" << this << ")" << UtIO::endl;
  if (recurse) {
    mCModelFn->print(false, numspaces+2);
    for (NUCModelArgConnectionVectorIter i = mArgs->begin();
	 i != mArgs->end();
	 ++i) {
      (*i)->print(recurse,numspaces+4);
    }
  }
}

void NUCModelCall::compose(UtString* buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  buf->append(numSpaces, ' ');
  *buf << "CModelCall\n";
  if (recurse)
  {
    mCModelFn->compose(buf, scope, numSpaces + 2, recurse);
  }
}

StringAtom* NUCModelCall::getName() const
{
  return mCModelFn->getName();
}

bool NUCModelCall::operator==(const NUStmt&) const
{
  return false;			// FIXME!
}

const char* NUCModelCall::getContext() const
{
  const UtString* context = mCModelFn->getContext();
  if (context != NULL)
    return context->c_str();
  else
    return NULL;
}

void NUCModelCall::addArgConnection(NUCModelArgConnection* conn)
{
  mArgs->push_back(conn);
}

NUCModelArgConnectionLoop NUCModelCall::loopArgConnections()
{
  return NUCModelArgConnectionLoop(*mArgs);
}

NUCModelArgConnectionCLoop NUCModelCall::loopArgConnections() const
{
  return NUCModelArgConnectionCLoop(*mArgs);
}

NUCModelInterface* NUCModelCall::getCModelInterface() const
{
  return getFunction()->getCModelInterface();
}

NUStmt* NUCModelCall::copy(CopyContext& copy_context) const
{
  // Create a copy of the c-model call
  NUCModelCall* newCall;

  newCall = new NUCModelCall(mCModel, mCModelFn, getLoc(), mNetRefFactory, getUsesCFNet() );

  // Copy the arg connections
  for (NUCModelArgConnectionCLoop l = loopArgConnections(); !l.atEnd(); ++l)
  {
    const NUCModelArgConnection* arg = *l;
    NUCModelArgConnection* newArg = arg->copy(copy_context);
    newCall->addArgConnection(newArg);
    newArg->updateCModelCall(newCall);
  }
  return newCall;
}

bool NUCModelCall::replaceLeaves(NuToNuFn& translator)
{
  bool changed = false;

  // Replace the c-model instance if we have a new one
  NUCModel* repl = translator(mCModel, NuToNuFn::ePre);
  if (repl)
  {
    mCModel = repl;
    changed = true;
  }

  // replace the arg connections
  for (NUCModelArgConnectionLoop l = loopArgConnections(); !l.atEnd(); ++l)
  {
    NUCModelArgConnection* arg = *l;
    changed |= arg->replaceLeaves(translator);
  }
  return changed;
}

bool NUCModelCall::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  // replace the arg connections
  for (NUCModelArgConnectionLoop l = loopArgConnections(); !l.atEnd(); ++l)
  {
    NUCModelArgConnection* arg = *l;
    changed |= arg->replace(old_net,new_net);
  }
  return changed;
}

void NUCModelCall::replaceDef(NUNet * old_net, NUNet * new_net)
{
  // replace the output arg connections
  for (NUCModelArgConnectionLoop l = loopArgConnections(); !l.atEnd(); ++l) {
    NUCModelArgConnection* arg = *l;
    NUCModelArgConnectionOutput* output = arg->castOutput();
    if (output != NULL) {
      arg->replace(old_net, new_net);
    }
  }
}

UInt32 NUCModelCall::inputsWordCount(void) const
{
  UInt32 words = 0;
  for (NUCModelArgConnectionCLoop l = loopArgConnections(); !l.atEnd(); ++l) {
    NUCModelArgConnection* arg = *l;
    NUCModelArgConnectionInput* input = arg->castInput();
    if (input != NULL) {
      UInt32 bitSize = input->getActual()->getBitSize();
      words += (bitSize + 31) / 32;
    }
  }
  return words;
}

UInt32 NUCModelCall::outputsWordCount(void) const
{
  UInt32 words = 0;
  for (NUCModelArgConnectionCLoop l = loopArgConnections(); !l.atEnd(); ++l) {
    NUCModelArgConnection* arg = *l;
    NUCModelArgConnectionOutput* output = arg->castOutput();
    if (output != NULL) {
      UInt32 bitSize = output->getActual()->getBitSize();
      words += (bitSize + 31) / 32;
    }
  }
  return words;
}
