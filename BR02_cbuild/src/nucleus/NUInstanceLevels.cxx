// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUDesign.h"
#include "nucleus/NUInstanceLevels.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"

NUInstanceLevels::NUInstanceLevels() {
  mLevelBuckets = NULL;
  mModuleLevelMap = NULL;
}

NUInstanceLevels::~NUInstanceLevels() {
  delete mModuleLevelMap;
  for (UInt32 i = 0, n = mLevelBuckets->size(); i < n; ++i) {
    NUModuleList* mods = (*mLevelBuckets)[i];
    delete mods;
  }
  delete mLevelBuckets;
}


//! Helper routine to populate the entire instance-depth bucket map
void NUInstanceLevels::design(NUDesign* the_design) {
  mLevelBuckets = new ModuleBuckets;
  mModuleLevelMap = new ModuleLevelMap;

  // The algorithm has to work bottom-up so that we see modules definitions
  // before we see module instantiations.  In particular, it should do all the
  // leaves, then all the modules that have 1 level of children, then 2 levels,
  // etc.  This is not the same as sorting by depth from the root, as a leaf
  // module may appear directly underneath the root (depth=1) and also
  // 5 levels below that.  We want to consider the bottom-up depth.
  NUModuleList mods;
  the_design->getTopLevelModules(&mods);
  for (NUModuleList::iterator p = mods.begin(); p != mods.end(); ++p) {
    NUModule* mod = *p;
    (void) getModuleLevel(mod);
  }
}

//! how many levels of instance hierarchy are there in the design?
/*!
 *! The height of a module, the distance from lowest leaf, differs from its depth,
 *! the distance from the root.  The height of a leaf module is 0, independent of
 *! where it is instantiated relative to the root
 */
UInt32 NUInstanceLevels::numInstanceLevels() const {
  return mLevelBuckets->size();
}

//! Get the height of a module in the instance-hierarchy tree (distinct from depth)
UInt32 NUInstanceLevels::getModuleLevel(NUModule* mod) const {
  ModuleLevelMap::iterator p = mModuleLevelMap->find(mod);
  if (p != mModuleLevelMap->end()) {
    return p->second;
  }

  UInt32 myLevel = 0;

  for (NUModuleInstanceMultiLoop i = mod->loopInstances(); !i.atEnd(); ++i) {
    NUModuleInstance* inst = *i;
    UInt32 childLevel = getModuleLevel(inst->getModule());
    myLevel = std::max(myLevel, childLevel + 1);
  }

  (*mModuleLevelMap)[mod] = myLevel;

  while (mLevelBuckets->size() <= myLevel) {
    mLevelBuckets->push_back(new NUModuleList);
  }

  NUModuleList* bucketList = (*mLevelBuckets)[myLevel];
  bucketList->push_back(mod);

  return myLevel;
} // UInt32 Congruent::getModuleLevel

//! loop over the modules at a particular level (must be < numInstanceLevels())
NUInstanceLevels::ModuleLevelLoop NUInstanceLevels::loopLevel(UInt32 level) const {
  INFO_ASSERT(level < numInstanceLevels(), "level out of bounds");

  NUModuleList* mods = (*mLevelBuckets)[level];

  return ModuleLevelLoop(*mods);
}
