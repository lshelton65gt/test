// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUExprFactory.h"
#include "nucleus/NUExpr.h"
#include "util/UtHashSet.h"

NUExprFactory::NUExprFactory()
{
  mExprSet = new ExprHashSet;
  mNetDeleteCount = NUNet::getNetDeleteCounter();
}

NUExprFactory::~NUExprFactory()
{
  clear();
  delete mExprSet;
}

void NUExprFactory::clear()
{
  // We cannot use hash-table iteration to destruct the factory,
  // otherwise we get problems hashing big expressions
  // when the little expressions inside them are gone.  The
  // easiest way is to copy the expressions in the hash table
  // into a vector, clear the hash table, and then delete the
  // elements from the vector.
  NUExprVector v;
  v.reserve(mExprSet->size());
  for (ExprHashSet::iterator p = mExprSet->begin(); p != mExprSet->end(); ++p)
    v.push_back(*p);
  mExprSet->clear();

  // We don't have to order the deletes bottom-up because each
  // expression knows it's children are managed by the factory,
  // and so it won't try to delete them.
  for (size_t i = 0; i < v.size(); ++i)
    delete v[i];
}

const NUExpr* NUExprFactory::insert(NUExpr* expr, bool copy) {

  // Make sure no one has deleted any nets since the expression cache
  // was initialized, since that keeps pointers to nets
  if (mNetDeleteCount != NUNet::getNetDeleteCounter()) {
    INFO_ASSERT(empty(),
                "Nets have been deleted with a non-empty expression factory");
    mNetDeleteCount = NUNet::getNetDeleteCounter();
  }

  // This code is as complex than it would appear to be necessary
  // because we are using a shallow hash.  Here is the code you might
  // like to write:
  //
  //    ExprHashSet::iterator p = mExprSet->find(expr);
  //    if (p == mExprSet->end())
  //    {
  //      NU_ASSERT(!expr->isManaged(), expr);
  //      expr->manage(this);
  //      mExprSet->insert(expr);
  //    }
  //    else
  //    {
  //      NUExpr* stored = *p;
  //      if (expr != stored)
  //      {
  //        delete expr;
  //        expr = stored;
  //      }
  //    }
  //    return expr;
  //
  // The above code will not work because the first lookup will be
  // too pessimistic.  Let's say I have a binary op (A+1) which
  // has already been entered into the factory.  If I deep-copy it
  // with NUExpr::copy() and then do a lookup, it will not be found.
  // We will look for pointer-equality for the member variables.
  // So we must not attempt to do a factory lookup on an expression
  // whose children are unmanaged.  That's why the code below is as it is:
  if (expr->isManaged())  {
    if (mExprSet->find(expr) != mExprSet->end(), expr) {
      return expr;              // already managed in this factory
    }
    // If we reach here then this expression is managed in *another*
    // factory.  We need to copy it to manage it in this factory.
    copy = true;
  }

  if (copy) {
    CopyContext cc(NULL, NULL);
    expr = expr->copy(cc);
  }
  expr = expr->makeSizeExplicit();
  expr->manage(this);
  ExprHashSet::IterBoolPair p = mExprSet->insert(expr);
  if (!p.second) {
    // expr was already in the set.  Delete the one passed in if
    // that's not the one we stored.  
    if (*(p.first) != expr) {
      delete expr;
      expr = *(p.first);
    }
    else {
      NU_ASSERT(0, expr);
    }
  }
  return expr;
} // const NUExpr* NUExprFactory::insert

//! Is the factory empty?
bool NUExprFactory::empty() const {
  return mExprSet->empty();
}

const NUExpr* NUExprFactory::resize(const NUExpr* e, UInt32 size) {
  if (size == 0) {
    size = e->determineBitSize();
  }
  if (e->getBitSize() != size) {
    CopyContext cc(NULL, NULL);
    NUExpr* expr = e->copy(cc);
    expr = expr->makeSizeExplicit(size);
    e = insert(expr);
  }
  return e;
}

const NUExpr* NUExprFactory::changeSign(const NUExpr* e, bool is_signed) {
  if (e->isSignedResult() != is_signed) {
    CopyContext cc(NULL, NULL);
    NUExpr* expr = e->copy(cc);
    expr->putSignedResult(is_signed);
    e = insert(expr);
  }
  return e;
}

const NUExpr* NUExprFactory::resizeSign(const NUExpr* e, UInt32 size,
                                        bool is_signed)
{
  bool size_changed = e->getBitSize() != size;
  if (size_changed || (e->isSignedResult() != is_signed)) {
    CopyContext cc(NULL, NULL);
    NUExpr* expr = e->copy(cc);
    expr->putSignedResult(is_signed);
    if (size_changed) {
      expr = expr->makeSizeExplicit(size);
    }
    e = insert(expr);
  }
  return e;
}

const NUExpr* NUExprFactory::makeSizeExplicit(const NUExpr* e, UInt32 request_size) {
  UInt32 sz = e->determineBitSize();
  if (request_size == 0) {
    request_size = e->getBitSize();
  }
  if ((e->getBitSize() != sz) || (sz != request_size)) {
    CopyContext cc(NULL, NULL);
    NUExpr* expr = e->copy(cc);
    expr = expr->makeSizeExplicit(request_size);
    e = insert(expr);
  }
  return e;
}
