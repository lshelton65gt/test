// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


//! Namespace for perf estimation functions
namespace NUCostPerformance
{
  //! Number added to combinational blocks for performance estimation
  const double cComboFudge = 11.4;
  //! Performance estimation limit
  /*!
    The performance equation becomes unreliable if the design is small enough to fit into cache. This limit represents the point on the graph at which the cps would represent in-cache performance.
  */
  const UInt32 cPerfLimit = 5076140;
  
  //! Performance estimation equation function
  double calcOverallPerformance(UInt32, UInt32 numCombos) {
    return cPerfLimit/(numCombos + cComboFudge);
  }
}



