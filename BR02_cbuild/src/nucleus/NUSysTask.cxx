// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUControlFlowNet.h"
#include "symtab/STBranchNode.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/UtIndent.h"
#include "util/UtStringUtil.h"


/*!
  \file
  Implementation for the Nucleus system task class
*/

NUSysTask::NUSysTask(StringAtom* name,
                     NUExprVector& exprs,
                     const NUModule* module,
                     NUNetRefFactory* netRefFactory,
                     bool usesCFNet,
                     bool isVerilogTask,
                     const SourceLocator& loc) :
  NUStmt(usesCFNet, loc),
  mNetRefFactory(netRefFactory),
  mName(name),
  mValueExprVector(exprs),
  mModule(module),
  mIsVerilogTask(isVerilogTask)
{
}

NUSysTask::~NUSysTask()
{
  for (NUExprVectorIter iter = mValueExprVector.begin();
       iter != mValueExprVector.end();
       ++iter)
  {
    NUExpr* expr = *iter;
    delete(expr);
  }
}

const char* NUSysTask::typeStr() const
{
  return "NUSysTask";
}


void NUSysTask::compose(UtString * buf, const STBranchNode* /* scope */, int numSpaces, bool /* recurse */) const
{
  // for this baseclass at least print something
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  StringAtom* stmtName = getName();
  
  *buf << "// system task " << (stmtName ? stmtName->str() : "noName") << "\n";
}

void NUSysTask::printFlagsToBuf(UtString* buf) const
{
  if ( isVerilogTask() ) { *buf << " isVerilogTask";}
}

void NUSysTask::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  StringAtom* stmtName = getName();
  UtString buf(": ");
  printFlagsToBuf(&buf);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << typeStr() << "(" << this << ") : " << (stmtName ? stmtName->str() : "noName") << buf << UtIO::endl;

  if (recurse)
  {
    for (NUExprVector::const_iterator iter = mValueExprVector.begin();
         iter != mValueExprVector.end();
         ++iter)
    {
      NUExpr* arg = *iter;
      arg->print(true, numspaces + 2);
    }
  }
}


// this method compares what it knows about, if a derived class has
// additional member variables then they should be checked if this
// method returns true.
bool NUSysTask::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUSysTask* sysStmt = dynamic_cast<const NUSysTask*>(&stmt);
  if ( sysStmt == NULL )
    return false;

  if ( mModule != sysStmt->getModule() )
    return false;

  if ( not ( mValueExprVector == sysStmt->getValueExprs() ) )
    return false;

  // at one time some NUSysTasks had no name so we handle that here
  StringAtom* stmtName = sysStmt->getName();
  
  if ( (mName != NULL ) != (stmtName != NULL ) )
    return false;               // one has a name while the other does not

  if ( (mName != NULL ) and (stmtName != NULL ) ) {
    // they both have names, so compare the strings
    if (std::strcmp (mName->str (), stmtName->str ()) != 0) {
      return false;
    }
  }
  

  return true;                  // as near as we can tell they are the same.
}

CGContext_t NUSysTask::emitCode (CGContext_t) const
{
  return 0;                     // by default nothing is generated
}

void NUSysTask::getBlockingUses(NUNetSet *uses) const
{
  for (NUExprVectorIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); ++iter)
  {
    NUExpr* expr = (*iter);
    expr->getUses(uses);
  }
}


void NUSysTask::getBlockingUses(NUNetRefSet *uses) const
{
  for (NUExprVectorIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); ++iter)
  {
    NUExpr* expr = (*iter);
    expr->getUses(uses);
  }
}

bool NUSysTask::queryBlockingUses(const NUNetRefHdl &use_net_ref,
                                  NUNetRefCompareFunction fn,
                                  NUNetRefFactory *factory) const
{
  for (NUExprVectorIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); ++iter)
  {
    NUExpr* expr = (*iter);
    if (expr->queryUses(use_net_ref, fn, factory))
    {
      return true;
    }
  }
  return false;
}

void NUSysTask::getBlockingUses(NUNet* net, NUNetSet *uses) const
{
  if (isDefNet(net))
    getBlockingUses(uses);
}


void NUSysTask::getBlockingUses(const NUNetRefHdl& net_ref, NUNetRefSet* uses) const
{
  if (isDefNet(net_ref->getNet()))
    getBlockingUses(uses);
}


bool NUSysTask::queryBlockingUses(const NUNetRefHdl & def,
                                  const NUNetRefHdl &use_net_ref,
                                  NUNetRefCompareFunction fn,
                                  NUNetRefFactory *factory) const
{
  return ( isDefNet(def->getNet()) and queryBlockingUses(use_net_ref, fn, factory) );
}


void NUSysTask::getBlockingDefs( NUNetSet* defs ) const
{
  // One net is defined for all system tasks - by default it is 
  // the outputSysTaskNet for the module where "this" (the system
  // task) was specified
  defs->insert(getModule()->getOutputSysTaskNet());
}

void NUSysTask::getBlockingDefs( NUNetRefSet* defs ) const
{
  defs->insert(mNetRefFactory->createNetRef(getModule()->getOutputSysTaskNet()));
}


// check to see if the outputSysTaskNet of this is within def_net_ref
bool NUSysTask::queryBlockingDefs(const NUNetRefHdl& def_net_ref,
                                  NUNetRefCompareFunction fn,
                                  NUNetRefFactory* factory) const
{
  bool ret = false;
  NUNetRefHdl net_ref = factory->createNetRef(getModule()->getOutputSysTaskNet());
  ret = ((*net_ref).*fn)(*def_net_ref);

  return ret;
}

void NUSysTask::getBlockingKills(NUNetSet* /* kills --unused */) const
{
  // nothing to do
}
void NUSysTask::getBlockingKills(NUNetRefSet* /* kills --unused */) const
{
  // nothing to do
}
bool NUSysTask::queryBlockingKills(const NUNetRefHdl & /*def_net_ref --unused */,
                                   NUNetRefCompareFunction /* fn --unused */,
                                   NUNetRefFactory* /* factory --unused */) const
{
  // nothing to do
  return false;
}

bool NUSysTask::replace( NUNet* old_net, NUNet* new_net )
{
  bool return_value = false;

  for (NUExprVectorIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); ++iter)
  {
    return_value |= (*iter)->replace(old_net,new_net);
  }
  return return_value;
}


void NUSysTask::replaceDef(NUNet* /* old_net -- unused */,
			   NUNet* /* new_net -- unused */)
{
  // TBD implement this
  NU_ASSERT(0=="unimplemented", this);
}

bool NUSysTask::replaceLeaves(NuToNuFn & translator)
{
  // Replacement is done in 3 steps, pre-recurse, recurse, post-recurse
  // Here we only do the vector of expressions, if a derived class has
  // other expressions they should be taken care of in a method for
  // that class

  bool changed = false;
  for (NUExprVectorNonconstIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); ++iter)
  {
    NUExpr * repl = translator((*iter), NuToNuFn::ePre);
    if (repl)
    {
      (*iter) = repl;
      changed = true;
    }
    changed |= (*iter)->replaceLeaves(translator);

    repl = translator ((*iter), NuToNuFn::ePost);
    if (repl)
    {
      (*iter) = repl;
      changed = true;
    }
  }
  return changed;
}







void NUSysTask::copyExprVector(NUExprVector& dest, const NUExprVector& source, CopyContext& copy_context) const
{
  int i = 0;
  for (NUExprVectorIter iter = source.begin(); iter != source.end(); ++iter, ++i) 
  {
    const NUExpr* expr = *iter;
    dest[i] = expr->copy(copy_context);
  }
}



NUReadmemX::NUReadmemX(StringAtom* name,
                       NUExprVector& exprs,
                       const NUModule* module,
                       NULvalue *lvalue, StringAtom* file,
		       bool hexFormat, SInt64 startAddress, SInt64 endAddress,
		       bool endSpecified,
                       NUNetRefFactory *netRefFactory,
                       bool usesCFNet,
                       const SourceLocator& loc) :
  NUSysTask(name, exprs, module, netRefFactory, usesCFNet, true, loc)
{
  mLvalue = lvalue;
  mFileName = file;
  mHexFormat = hexFormat;
  mStartAddress = startAddress;
  mEndAddress = endAddress;
  mEndSpecified = endSpecified;
}

NUReadmemX::~NUReadmemX()
{
  delete mLvalue;
}

void NUReadmemX::getBlockingDefs(NUNetSet* defs) const
{
  mLvalue->getDefs(defs);
}

void NUReadmemX::getBlockingDefs(NUNetRefSet* defs) const
{
  mLvalue->getDefs(defs);
}


bool NUReadmemX::queryBlockingDefs(const NUNetRefHdl &def_net_ref,
				   NUNetRefCompareFunction fn,
				   NUNetRefFactory *factory) const
{
  return mLvalue->queryDefs(def_net_ref, fn, factory);
}


void NUReadmemX::getBlockingKills(NUNetSet* defs) const
{
  mLvalue->getCompleteDefs(defs);
}

void NUReadmemX::getBlockingKills(NUNetRefSet* kills) const
{
  NUNetSet net_kills;
  mLvalue->getCompleteDefs(&net_kills);
  for (NUNetSet::iterator iter = net_kills.begin(); iter != net_kills.end(); ++iter) {
    kills->insert(kills->getFactory()->createNetRef(*iter));
  }
}


bool NUReadmemX::queryBlockingKills(const NUNetRefHdl &def_net_ref,
				    NUNetRefCompareFunction /* fn -- unused */,
				    NUNetRefFactory *factory) const
{
  return mLvalue->queryCompleteDefs(def_net_ref, factory);
}


bool NUReadmemX::replace(NUNet * old_net, NUNet * new_net)
{
  return mLvalue->replace(old_net,new_net);
}

bool NUReadmemX::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  NULvalue * repl = translator(mLvalue, NuToNuFn::ePre);
  if (repl) {
    mLvalue = repl;
    changed = true;
  }
  changed |= mLvalue->replaceLeaves(translator);

  repl = translator(mLvalue, NuToNuFn::ePost);
  if (repl) {
    mLvalue = repl;
    changed = true;
  }

  return changed;
}

void NUReadmemX::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  char readmemType;
  if (mHexFormat)
    readmemType = 'h';
  else
    readmemType = 'b';
  UtIO::cout() << " $readmem" << readmemType << "(" << mFileName->str() << "):\n";
  mLvalue->print(recurse, numspaces+2);
}

void NUReadmemX::compose(UtString * , const STBranchNode* /* scope */, int, bool) const {}

bool NUReadmemX::isDefNet(NUNet* /* net */) const
{
  bool ret = false;
  ret = true;                   // the original definition of this method never
                                // checked to see if net was a defnet
  return ret;
}


NUStmt* NUReadmemX::copy(CopyContext &copy_context) const
{
  StringAtom *name = copy_context.mScope->gensym("readmemx");
  NUModule* module = copy_context.mScope->getModule();

  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);

  return new NUReadmemX(name,
                        cpyVector, module,
                        mLvalue->copy(copy_context), mFileName,
                        mHexFormat, mStartAddress, mEndAddress, mEndSpecified,
                        mNetRefFactory, getUsesCFNet(), mLoc);
}


const char* NUReadmemX::typeStr() const
{
  return "NUReadmemX";
}

bool NUReadmemX::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUReadmemX* rmStmt = dynamic_cast<const NUReadmemX*>(&stmt);
  
  if (not (*mLvalue == *(rmStmt->getLvalue ())))
    return false;

  if (mHexFormat != rmStmt->getHexFormat ()
      || mEndSpecified != rmStmt->getEndSpecified ()
      || mStartAddress != rmStmt->getStartAddress ()
      || mEndAddress != rmStmt->getEndAddress ())
      return false;

  return NUSysTask::operator==(stmt);
}

 
NUOutputSysTask::NUOutputSysTask(StringAtom* name,
                                 NUExprVector& exprs,
                                 const NUModule* module,
                                 bool hasFileSpec,
                                 bool addNewline,
                                 SInt8 timeUnit,
                                 SInt8 timePrecision,
                                 NUNetRefFactory* netRefFactory,
                                 bool usesCFNet,
                                 bool isVerilogTask,
                                 bool isWriteLineTask,
                                 bool hasHierarchicalName,
                                 STBranchNode* where,
                                 const SourceLocator& loc) :
  NUSysTask(name, exprs, module, netRefFactory, usesCFNet, isVerilogTask, loc)
{
  mAddNewline = addNewline;
  mHasFileSpec = hasFileSpec;
  mTimeUnit = timeUnit;
  mTimePrecision = timePrecision;
  mWriteLineTask = isWriteLineTask;
  mHasHierarchicalName = hasHierarchicalName;
  mNameBranch = where;
  mPromotePath = NULL;
}

NUOutputSysTask::~NUOutputSysTask()
{
  if (mPromotePath != NULL) {
    StringUtil::free(mPromotePath);
  }
}

const char* NUOutputSysTask::typeStr() const
{
  return "NUOutputSysTask";
}

bool NUOutputSysTask::isDefNet(NUNet* net) const
{
  return ( net == getModule()->getOutputSysTaskNet() );
}

void NUOutputSysTask::printFlagsToBuf(UtString* buf) const
{
  // get flags from class we derived from first
  NUSysTask::printFlagsToBuf(buf);
  
  if ( needsNewline() ) { *buf << " needsNewline";}
  if ( hasFileSpec() ) { *buf << " hasFileSpec";}
  if ( isWriteLineTask() ) { *buf << " isWriteLineTask";}
  if ( hasHierarchicalName() ) { *buf << " hasHierarchicalName";}
}


void NUOutputSysTask::compose(UtString * buf, const STBranchNode* scope, int numspaces, bool /* recurse */) const
{
  UtIndent indent(buf);
  buf->append(numspaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  *buf << "$";
  if ( hasFileSpec() )
  {
    *buf << "f";
  }
  if ( needsNewline() )
  {
    *buf << "display(";
  }
  else
  {
    *buf << "write(";
  }
  const char* separator = "";
  
  for (NUExprVectorIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); iter++)
  {
    *buf << separator;
    separator = ", ";
    NUExpr* expr = *iter;
    expr->compose( buf, scope, 0, false );
  }
  *buf << ");";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
}


bool NUOutputSysTask::operator==(const NUStmt& stmt ) const
{
  // first compare the NUOutputSysTask specific information
  const NUOutputSysTask* outputSysTask = dynamic_cast<const NUOutputSysTask*>(&stmt);
  if ( outputSysTask == NULL )
    return false;

  if ( mAddNewline != outputSysTask->needsNewline() )
    return false;

  if ( mHasFileSpec != outputSysTask->hasFileSpec() )
    return false;

  // now compare the information common to NUSysTasks
  if (not (NUSysTask::operator==(stmt)) )
    return false;
  
  return true;
}


NUType NUOutputSysTask::getType() const
{
  return eNUOutputSysTask;
}


NUStmt* NUOutputSysTask::copy(CopyContext &copy_context) const
{
  StringAtom *name = copy_context.mScope->gensym(mName->str());// copy gets a new name
  NUModule* module = copy_context.mScope->getModule();
  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);

  NUOutputSysTask* sysTask;
  sysTask = new NUOutputSysTask(name, cpyVector, module, mHasFileSpec,
                                mAddNewline, mTimeUnit, mTimePrecision,
                                mNetRefFactory, getUsesCFNet(),
                                isVerilogTask(), isWriteLineTask(),
                                hasHierarchicalName(),
                                getNameBranch(), mLoc);
  if (mPromotePath != NULL) {
    sysTask->mPromotePath = StringUtil::dup(mPromotePath);
  }
  
  return sysTask;
}

NUExpr* NUOutputSysTask::getFileSpec() const
{
  if ( mHasFileSpec )
  {
    NU_ASSERT ( not mValueExprVector.empty(), this);
    //  (if a filespec was defined then it was is stored in element 0 of expression vector)
    return mValueExprVector[0];
  }
  else
  {
    return NULL;
  }
}

void NUOutputSysTask::putPromotePath(const char* path) {
  mPromotePath = StringUtil::dup(path);
}

NUFCloseSysTask::NUFCloseSysTask(StringAtom* name,
                                 NUExprVector& exprs,
                                 const NUModule* module,
                                 NUNetRefFactory* netRefFactory,
                                 bool usesCFNet,
                                 bool isVerilogTask,
                                 bool useInputFileSystem,
                                 const SourceLocator& loc) :
  NUSysTask(name, exprs, module, netRefFactory, usesCFNet, isVerilogTask, loc)
{
  mUseInputFileSystem =  useInputFileSystem;
}

NUFCloseSysTask::~NUFCloseSysTask()
{
}

NUType NUFCloseSysTask::getType() const
{
  return eNUFCloseSysTask;
}

const char* NUFCloseSysTask::typeStr() const
{
  return "NUFCloseSysTask";
}

bool NUFCloseSysTask::isDefNet(NUNet* net) const
{
  if (net == getModule()->getOutputSysTaskNet())
    return true;

  return false;
}


void NUFCloseSysTask::compose(UtString * buf, const STBranchNode* scope, int numspaces, bool /* recurse */) const
{
  UtIndent indent(buf);
  buf->append(numspaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  *buf << "$fclose(";
  getFileSpec()->compose( buf, scope, 0, false );
  *buf << ");";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
}

bool NUFCloseSysTask::operator==(const NUStmt& stmt ) const
{
  const NUFCloseSysTask* closeSysTask = dynamic_cast<const NUFCloseSysTask*>(&stmt);
  if ( closeSysTask == NULL )
    return false;

  return NUSysTask::operator==(stmt);
}

NUStmt* NUFCloseSysTask::copy(CopyContext &copy_context) const
{
  StringAtom *name = copy_context.mScope->gensym(mName->str());// copy gets a new name
  NUModule* module = copy_context.mScope->getModule();

  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);
  
  return new NUFCloseSysTask(name, cpyVector, module, mNetRefFactory, getUsesCFNet(), isVerilogTask(), mUseInputFileSystem, mLoc);
}

NUExpr* NUFCloseSysTask::getFileSpec() const
{
  NU_ASSERT ( mValueExprVector.size() == 1, this ); // NUFCloseSysTask::isValid
                                           // checked this during construction, this
                                           // assert can only happen if memory corrupted

  return mValueExprVector[0];
}

bool NUFCloseSysTask::isValid(UtString* msg)
{
  // check that we have the right number of arguments
  bool ret = true;
  SInt32 availableNumArgs = mValueExprVector.size();
  if ( availableNumArgs != 1 )
  {
    *msg << availableNumArgs << " arguments supplied, exactly 1 is required";
    ret = false;
  }
  return ret;
}







NUFOpenSysTask::NUFOpenSysTask(StringAtom* name,
                               NUNet* outNet,
                               NUIdentLvalue* statusLvalue,
                               NUExprVector& exprs,
                               const NUModule* module,
                               NUNetRefFactory* netRefFactory,
                               bool usesCFNet,
                               bool isVerilogTask,
                               bool useInputFileSystem,
                               const SourceLocator& loc) :
  NUSysTask(name, exprs, module, netRefFactory, usesCFNet, isVerilogTask, loc)
{
  mLvalue = new NUIdentLvalue(outNet, loc);
  mStatusLvalue = statusLvalue;
  mUseInputFileSystem = useInputFileSystem;
}

NUFOpenSysTask::~NUFOpenSysTask()
{
  delete mLvalue;
  if (mStatusLvalue)
    delete mStatusLvalue;
}


const char* NUFOpenSysTask::typeStr() const
{
  return "NUFOpenSysTask";
}

bool NUFOpenSysTask::isDefNet(NUNet* net) const
{
  if (net == getOutNet())
    return true;
  if (net == getModule()->getOutputSysTaskNet())
    return true;
  
  return false;
}

void NUFOpenSysTask::compose(UtString * buf, const STBranchNode* scope, int numspaces, bool /* recurse */) const
{
  UtIndent indent(buf);
  buf->append(numspaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  getOutNet()->compose(buf,NULL);
  *buf << " = $fopen(";
  if (mStatusLvalue)
  {
    mStatusLvalue->compose( buf, scope, 0, false );
    *buf << ", ";
  }
  const char* separator = "";

  for (NUExprVectorIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); ++iter)  
  {
    *buf << separator;
    separator = ", ";
    (*iter)->compose( buf, scope, 0, false );
  }
  *buf << ");";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
}


bool NUFOpenSysTask::operator==(const NUStmt& stmt ) const
{
  const NUFOpenSysTask* openSysTask = dynamic_cast<const NUFOpenSysTask*>(&stmt);
  if ( openSysTask == NULL )
    return false;

  // compare the temp variables defined for this sys task, this is
  // very pessimistic
  if (not ( *(getLvalue()) == *(openSysTask->getLvalue())))
    return false;
  
  if ( not ( *(getStatusLvalue()) == *(openSysTask->getStatusLvalue())))
    return false;

  return NUSysTask::operator==(stmt);
}

void NUFOpenSysTask::getBlockingDefs( NUNetSet* defs ) const
{
  // Two nets are defined in a NUFOpenSysTask. First is the single
  // outputSysTaskNet for the module where "this" (the system
  // task) was specified, Second is the temp net that allows this
  // system function to act like a stytem task.
  //  Here we just insert both in the defs set.
  defs->insert(getModule()->getOutputSysTaskNet());
  defs->insert(getOutNet());
  if (mStatusLvalue)
    mStatusLvalue->getDefs(defs);
}

void NUFOpenSysTask::getBlockingDefs( NUNetRefSet* defs ) const
{
  // question, which of the following is a better way to get the
  // factory(use mNetRefFactory or defs->getFactory()?
  defs->insert(mNetRefFactory->createNetRef(getModule()->getOutputSysTaskNet()));
  defs->insert(defs->getFactory()->createNetRef(getOutNet()));
  if (mStatusLvalue)
    mStatusLvalue->getDefs(defs);
}


// check to see if the outputSysTaskNet of this is within def_net_ref
bool NUFOpenSysTask::queryBlockingDefs(const NUNetRefHdl& def_net_ref,
                                       NUNetRefCompareFunction fn,
                                       NUNetRefFactory* factory) const
{
  bool ret = false;
  NUNetRefHdl net_ref = factory->createNetRef(getModule()->getOutputSysTaskNet());
  ret = ((*net_ref).*fn)(*def_net_ref);

  net_ref = factory->createNetRef(getOutNet());
  ret |= ((*net_ref).*fn)(*def_net_ref);
  if (mStatusLvalue)
    ret |= mStatusLvalue->queryDefs(def_net_ref, fn, factory);

  return ret;
}

void NUFOpenSysTask::getBlockingKills(NUNetSet *kills) const
{
  kills->insert(getOutNet());
  if(mStatusLvalue)
    mStatusLvalue->getCompleteDefs(kills);
}

void NUFOpenSysTask::getBlockingKills(NUNetRefSet *kills) const
{
  kills->insert(kills->getFactory()->createNetRef(getOutNet()));
  if (mStatusLvalue)
  {
    NUNetSet net_kills;
    mStatusLvalue->getCompleteDefs(&net_kills);
    for (NUNetSet::iterator iter = net_kills.begin(); iter != net_kills.end(); ++iter) {
      kills->insert(kills->getFactory()->createNetRef(*iter));
    }
  }
}

bool NUFOpenSysTask::queryBlockingKills(const NUNetRefHdl &def_net_ref,
                                     NUNetRefCompareFunction fn,
                                     NUNetRefFactory *factory) const
{
  bool ret = false;

  NUNetRefHdl net_ref = factory->createNetRef(getOutNet());
  ret |= ((*net_ref).*fn)(*def_net_ref);
  if (mStatusLvalue)
    ret |= mStatusLvalue->queryCompleteDefs(def_net_ref, factory);
  return ret;
}

bool NUFOpenSysTask::replace( NUNet* old_net, NUNet* new_net )
{
  bool return_value = false;
  // first handle nets that are specific to this class
  if (old_net == getOutNet())
  {
    NU_ASSERT(new_net->getBitSize() == 32 || new_net->isVHDLLineNet(), this);
    delete mLvalue;
    mLvalue = new NUIdentLvalue(new_net, new_net->getLoc());
    return_value = true;
  }
  if (mStatusLvalue)
    return_value |= mStatusLvalue->replace(old_net,new_net);
  // now handle nets that are owned by parent class

  return_value |= NUSysTask::replace(old_net, new_net);

  return return_value;
}

void NUFOpenSysTask::replaceDef(NUNet *old_net, NUNet *new_net)
{
  if (old_net == getOutNet())
  {
    NU_ASSERT(new_net->getBitSize() == 32, this);
    delete mLvalue;
    mLvalue = new NUIdentLvalue(new_net, new_net->getLoc());
  }
} 

bool NUFOpenSysTask::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  
  // first replace on the class specific nets
  NUNet *old_net = getOutNet();
  NUNet * repl = translator(old_net, NuToNuFn::ePrePost);
  if (repl)
  {
    changed = replace(old_net, repl); // this properly takes care of updates to mLvalue
  }
  if (mStatusLvalue)
  {
    NUNet *status_net = getStatusNet();
    repl = translator(status_net, NuToNuFn::ePrePost);
    if (repl)
    {
      changed |= replace(status_net, repl); // this properly takes care of updates to mStatusLvalue
    }
  }
  
  // now replace on the vector owned by parent class
  changed |= NUSysTask::replaceLeaves(translator);

  return changed;
}


NUType NUFOpenSysTask::getType() const
{
  return eNUFOpenSysTask;
}

NUStmt* NUFOpenSysTask::copy(CopyContext &copy_context) const
{
  StringAtom *name = copy_context.mScope->gensym(mName->str());// copy gets a new name
  NUModule* module = copy_context.mScope->getModule();
  
  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);
  
  NULvalue* newStatusLvalue = NULL;
  if (mStatusLvalue)
    newStatusLvalue = mStatusLvalue->copy(copy_context);

  NUNet * replacement_out_net = copy_context.lookup(getOutNet());
  if (replacement_out_net == NULL) {
    replacement_out_net = getOutNet();
  }

  return new NUFOpenSysTask(name, replacement_out_net,
                            static_cast<NUIdentLvalue*>(newStatusLvalue),
                            cpyVector,  module, mNetRefFactory, getUsesCFNet(),
                            isVerilogTask(), mUseInputFileSystem, mLoc);
}



NUNet* NUFOpenSysTask::getOutNet() const
{
  return mLvalue->getIdent();
}

NUNet* NUFOpenSysTask::getStatusNet() const
{
  return mStatusLvalue->getIdent();
}

//! Validate legal arguments for this system call
bool NUFOpenSysTask::isValid(UtString* msg)
{
  // check that we have the right number of arguments
  bool ret = true;
  SInt32 minNumArgs =  1;
  SInt32 maxNumArgs =  2;

  SInt32 availableNumArgs = mValueExprVector.size();
  if ( ( availableNumArgs < minNumArgs ) or ( maxNumArgs < availableNumArgs ))
  {
    if ( minNumArgs == maxNumArgs ) {
      *msg << availableNumArgs << " arguments supplied, " << minNumArgs << " are required";
    }
    else {
      *msg << availableNumArgs << " arguments supplied, a number between " << minNumArgs << " and " << maxNumArgs << " (inclusive) are required";
    }
    ret = false;
  }
  if ( availableNumArgs == 2 ) {
    // check to make sure that the length of the mode is 3 or fewer
    // chars
    UInt32 bitsInMode = mValueExprVector[1]->getBitSize();
    if ( bitsInMode > 24 ) {
      UtString buf;
      mValueExprVector[1]->compose(&buf, NULL, 0, false);
      *msg << "The mode argument has too many characters, up to 3 characters are allowed: (" << buf << ")";

      ret = false;
    }
  }

  return ret;
}

// rjc-todo who uses this? NUDesignWalker::sysFOpen, but that should
// be combined with NUDesignWalker::sysRandom into a single function
// that operates on NUSysTask, and then getLvalue must be made to
// return null if there is no Lvalue.
NULvalue* NUFOpenSysTask::getLvalue() const
{
  return mLvalue;
}

NULvalue* NUFOpenSysTask::getStatusLvalue() const
{
  return mStatusLvalue;
}

NUInputSysTask::NUInputSysTask(StringAtom* name,
                               NUExprVector& exprs,
                               NULvalue *lvalue,
                               NULvalue *statusLvalue,
                               const NUModule* module,
                               NUNetRefFactory* netRefFactory,
                               bool usesCFNet,
                               bool isVerilogTask,
                               bool isReadLineTask,
                               const SourceLocator& loc) :
  NUSysTask(name, exprs, module, netRefFactory, usesCFNet, isVerilogTask, loc)
{
  mReadLineTask = isReadLineTask;
  mReadLvalue = lvalue;
  mStatusLvalue = statusLvalue;
}

NUInputSysTask::~NUInputSysTask()
{
  if (mReadLvalue)
    delete mReadLvalue; 
  if (mStatusLvalue)
    delete mStatusLvalue;
}


const char* NUInputSysTask::typeStr() const
{
  return "NUInputSysTask";
}

bool NUInputSysTask::isDefNet(NUNet* net) const
{
  if (net == getModule()->getOutputSysTaskNet())
    return true;

  return false;
}

void NUInputSysTask::compose(UtString * buf, const STBranchNode* scope, int numspaces, bool /* recurse */) const
{
  UtIndent indent(buf);
  buf->append(numspaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  
  if (mIsVerilogTask)
    *buf << " = $fread(";
  else
    *buf << " READ(";
  const char* separator = "";

  for (NUExprVectorIter iter = mValueExprVector.begin(); iter != mValueExprVector.end(); ++iter)  
  {
    *buf << separator;
    separator = ", ";
    (*iter)->compose( buf, scope, 0, false );
  }
  if (mReadLvalue)
  {
    // If it is not a READLINE Task.
    *buf << ", ";
    mReadLvalue->compose( buf, scope, 0, false );
  }
  if (mStatusLvalue)
  {
    // If the READ task has a boolean argument that takes the return status.
    *buf << ", ";
    mStatusLvalue->compose( buf, scope, 0, false );
  }
  *buf << ");";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
}


bool NUInputSysTask::operator==(const NUStmt& stmt ) const
{
  const NUInputSysTask* inputSysTask = dynamic_cast<const NUInputSysTask*>(&stmt);
  if ( inputSysTask == NULL )
    return false;

  if (getReadLvalue())
  {
    // compare the lvalue variables
    if (not ( *(getReadLvalue()) == *(inputSysTask->getReadLvalue())))
      return false;
  }
  if (getStatusLvalue())
  {
    // compare the return_status variable
    if (not ( *(getStatusLvalue()) == *(inputSysTask->getStatusLvalue())))
      return false;
  }
  return NUSysTask::operator==(stmt);
}

void NUInputSysTask::getBlockingDefs( NUNetSet* defs ) const
{
  if (mReadLvalue)
    mReadLvalue->getDefs(defs);
  if (mStatusLvalue)
    mStatusLvalue->getDefs(defs);
  NUSysTask::getBlockingDefs( defs );
}

void NUInputSysTask::getBlockingDefs( NUNetRefSet* defs ) const
{
  if (mReadLvalue)
    mReadLvalue->getDefs(defs);
  if (mStatusLvalue)
    mStatusLvalue->getDefs(defs);
  NUSysTask::getBlockingDefs( defs );
}

bool NUInputSysTask::queryBlockingDefs(const NUNetRefHdl& def_net_ref,
                                       NUNetRefCompareFunction fn,
                                       NUNetRefFactory* factory) const
{
  bool ret = false;
  if (mReadLvalue)
    ret = mReadLvalue->queryDefs(def_net_ref, fn, factory);

  if (mStatusLvalue)
    ret |= mStatusLvalue->queryDefs(def_net_ref, fn, factory);

  ret |= NUSysTask::queryBlockingDefs( def_net_ref , fn, factory );
  return ret;
}

void NUInputSysTask::getBlockingKills(NUNetSet *kills) const
{
  if (mReadLvalue)
      mReadLvalue->getCompleteDefs(kills);
  if (mStatusLvalue)
    mStatusLvalue->getCompleteDefs(kills);
}

void NUInputSysTask::getBlockingKills(NUNetRefSet *kills) const
{
  if (mReadLvalue)
  {
    NUNetSet net_kills;
    mReadLvalue->getCompleteDefs(&net_kills);
    for (NUNetSet::iterator iter = net_kills.begin(); iter != net_kills.end(); ++iter) {
      kills->insert(kills->getFactory()->createNetRef(*iter));
    }
  }
  if (mStatusLvalue)
  {
    NUNetSet net_kills;
    mStatusLvalue->getCompleteDefs(&net_kills);
    for (NUNetSet::iterator iter = net_kills.begin(); iter != net_kills.end(); ++iter) {
      kills->insert(kills->getFactory()->createNetRef(*iter));
    }
  }
}

bool NUInputSysTask::queryBlockingKills(const NUNetRefHdl &def_net_ref,
                                        NUNetRefCompareFunction /*fn*/,
                                        NUNetRefFactory *factory) const
{
  bool return_value = false;
  if (mReadLvalue)
    return_value = mReadLvalue->queryCompleteDefs(def_net_ref, factory);
  if (mStatusLvalue)
    return_value |= mStatusLvalue->queryCompleteDefs(def_net_ref, factory);

  return return_value;
}

bool NUInputSysTask::replace(NUNet * old_net, NUNet * new_net)
{
 bool return_value = false;
 if (mReadLvalue)
   return_value = mReadLvalue->replace(old_net,new_net);

 if (mStatusLvalue)
   return_value |= mStatusLvalue->replace(old_net,new_net);

  // now handle nets that are owned by parent class
 return_value |= NUSysTask::replace(old_net, new_net);

 return return_value;

}

bool NUInputSysTask::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  if (mReadLvalue)
  {
    NULvalue * repl = translator(mReadLvalue, NuToNuFn::ePre);
    if (repl) {
      mReadLvalue = repl;
      changed = true;
    }
    changed |= mReadLvalue->replaceLeaves(translator);

    repl = translator(mReadLvalue, NuToNuFn::ePost);
    if (repl) {
      mReadLvalue = repl;
      changed = true;
    }
  }
  if (mStatusLvalue)
  {
    NULvalue * repl = translator(mStatusLvalue, NuToNuFn::ePre);
    if (repl) {
      mStatusLvalue = repl;
      changed = true;
    }
    changed |= mStatusLvalue->replaceLeaves(translator);

    repl = translator(mStatusLvalue, NuToNuFn::ePost);
    if (repl) {
      mStatusLvalue = repl;
      changed = true;
    }
  }
  // now replace on the vector owned by parent class
  changed |= NUSysTask::replaceLeaves(translator);

  return changed;
}


NUType NUInputSysTask::getType() const
{
  return eNUInputSysTask;
}

NUStmt* NUInputSysTask::copy(CopyContext &copy_context) const
{
  StringAtom *name = copy_context.mScope->gensym(mName->str());// copy gets a new name
  NUModule* module = copy_context.mScope->getModule();
  
  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);
  NULvalue* newReadLvalue = NULL;
  NULvalue* newStatusLvalue = NULL;
  if (mReadLvalue)
    newReadLvalue = mReadLvalue->copy(copy_context);
  if (mStatusLvalue)
    newStatusLvalue = mStatusLvalue->copy(copy_context);

  return new NUInputSysTask(name, cpyVector, 
                            newReadLvalue, newStatusLvalue, module, 
                            mNetRefFactory, getUsesCFNet(), isVerilogTask(),
                            isReadLineTask(), mLoc);
}



NUExpr* NUInputSysTask::getFileSpec() const
{
  NU_ASSERT ( not mValueExprVector.empty(), this);
    // filespec was stored in element 0 of expression vector)
    return mValueExprVector[0];
}

NUControlSysTask::NUControlSysTask(StringAtom* name,
                                   CarbonControlType controlType,
                                   NUExprVector& exprs,
                                   const NUModule* module,
                                   NUNetRefFactory* netRefFactory,
                                   bool usesCFNet,
                                   const SourceLocator& loc) :
  NUSysTask(name, exprs, module, netRefFactory, usesCFNet, true, loc),
  mControlType(controlType),
  mBlockNetRefDefMap(netRefFactory)
{
}

NUControlSysTask::~NUControlSysTask()
{
  clearUseDef();
}

NUType NUControlSysTask::getType() const
{
  return eNUControlSysTask;
}

const char* NUControlSysTask::typeStr() const
{
  return "NUControlSysTask";
}

bool NUControlSysTask::isDefNet(NUNet* net) const
{
  return ( net == getModule()->getControlFlowNet() );
}


void NUControlSysTask::compose(UtString * buf, const STBranchNode* scope, int numspaces, bool /* recurse */) const
{
  UtIndent indent(buf);
  buf->append(numspaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  switch ( getReasonForControlOperation() )
  {
  case eCarbonStop:   { *buf << "$stop(";   break; }
  case eCarbonFinish: { *buf << "$finish("; break; }
  default:            { NU_ASSERT(0=="unknown system task", this); }
  }
  getVerbosity()->compose( buf, scope, 0, false );
  *buf << ");";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
}

bool NUControlSysTask::operator==(const NUStmt& stmt ) const
{
  const NUControlSysTask* controlSysTask = dynamic_cast<const NUControlSysTask*>(&stmt);
  if ( controlSysTask == NULL )
    return false;

  return NUSysTask::operator==(stmt);
}

void NUControlSysTask::getBlockingUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getBlockingUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}

void NUControlSysTask::getBlockingUses(NUNetRefSet *uses) const
{
  mBlockNetRefDefMap.populateMappedToNonEmpty(uses);
}

bool NUControlSysTask::queryBlockingUses(const NUNetRefHdl &use_net_ref,
                                         NUNetRefCompareFunction fn,
                                         NUNetRefFactory * /* factory -- unused */) const
{
  return mBlockNetRefDefMap.queryMappedTo(use_net_ref, fn);
}

void NUControlSysTask::getBlockingUses(NUNet *net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getBlockingUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUControlSysTask::getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  mBlockNetRefDefMap.populateMappedToNonEmpty(uses, net_ref, &NUNetRef::overlapsSameNet);
}


bool NUControlSysTask::queryBlockingUses(const NUNetRefHdl &def_net_ref,
                                         const NUNetRefHdl &use_net_ref,
                                         NUNetRefCompareFunction fn,
                                         NUNetRefFactory * /* factory -- unused */) const
{
  return mBlockNetRefDefMap.queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}

void NUControlSysTask::addBlockingUse(const NUNetRefHdl &def_net_ref,
                                      const NUNetRefHdl &use_net_ref)
{
  mBlockNetRefDefMap.insert(def_net_ref, use_net_ref);
}

void NUControlSysTask::addBlockingDef(const NUNetRefHdl &net_ref)
{
  mBlockNetRefDefMap.insert(net_ref);
}

void NUControlSysTask::clearUseDef()
{
  mBlockNetRefDefMap.clear();
}


void NUControlSysTask::getBlockingDefs( NUNetSet* defs ) const
{
  // A NUControlSysTask only and always defs a control flow 'token'
  // (represented by the ControlFlowNet for this module) instead of the $ostnet
  defs->insert(getModule()->getControlFlowNet());
}

void NUControlSysTask::getBlockingDefs( NUNetRefSet* defs ) const
{
  defs->insert(mNetRefFactory->createNetRef(getModule()->getControlFlowNet()));
}


// check to see if any defs are within def_net_ref
bool NUControlSysTask::queryBlockingDefs(const NUNetRefHdl& def_net_ref,
                                         NUNetRefCompareFunction fn,
                                         NUNetRefFactory* factory) const
{
  bool ret = false;
  // just check the controlFlowNet 
  NUNetRefHdl net_ref = factory->createNetRef(getModule()->getControlFlowNet());
  ret = ((*net_ref).*fn)(*def_net_ref);

  return ret;
}



NUStmt* NUControlSysTask::copy(CopyContext &copy_context) const
{
  StringAtom *name = copy_context.mScope->gensym(mName->str());// copy gets a new name
  NUModule* module = copy_context.mScope->getModule();

  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);
  
  return new NUControlSysTask(name, mControlType, cpyVector, module, mNetRefFactory, getUsesCFNet(), mLoc);
}

NUExpr* NUControlSysTask::getVerbosity() const
{
  NU_ASSERT ( mValueExprVector.size() == 1, this );

  return mValueExprVector[0];
}

CarbonControlType NUControlSysTask::getReasonForControlOperation() const
{
  return mControlType;
}




NUFFlushSysTask::NUFFlushSysTask(StringAtom* name,
                                 NUExprVector& exprs,
                                 const NUModule* module,
                                 NUNetRefFactory* netRefFactory,
                                 bool usesCFNet,
                                 const SourceLocator& loc) :
  NUSysTask(name, exprs, module, netRefFactory, usesCFNet, true, loc)
{
}

NUFFlushSysTask::~NUFFlushSysTask()
{
}

NUType NUFFlushSysTask::getType() const
{
  return eNUFFlushSysTask;
}

const char* NUFFlushSysTask::typeStr() const
{
  return "NUFFlushSysTask";
}

bool NUFFlushSysTask::isDefNet(NUNet* net) const
{
  if (net == getModule()->getOutputSysTaskNet())
    return true;

  return false;
}


void NUFFlushSysTask::compose(UtString * buf, const STBranchNode* scope, int numspaces, bool /* recurse */) const
{
  UtIndent indent(buf);
  buf->append(numspaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  *buf << "$fflush(";

  NUExpr* fileSpec = getFileSpec();
  if ( fileSpec )
    fileSpec->compose( buf, scope, 0, false );
  
  *buf << ");";
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();
}

bool NUFFlushSysTask::operator==(const NUStmt& stmt ) const
{
  const NUFFlushSysTask* flushSysTask = dynamic_cast<const NUFFlushSysTask*>(&stmt);
  if ( flushSysTask == NULL )
    return false;

  return NUSysTask::operator==(stmt);
}

NUStmt* NUFFlushSysTask::copy(CopyContext &copy_context) const
{
  StringAtom *name = copy_context.mScope->gensym(mName->str());// copy gets a new name
  NUModule* module = copy_context.mScope->getModule();

  NUExprVector cpyVector(mValueExprVector.size());
  NUSysTask::copyExprVector(cpyVector, mValueExprVector, copy_context);
  
  return new NUFFlushSysTask(name, cpyVector, module, mNetRefFactory, getUsesCFNet(), mLoc);
}

NUExpr* NUFFlushSysTask::getFileSpec() const
{
  SInt32 numArgs = mValueExprVector.size();
  if ( numArgs == 0 )
  {
    return NULL;
  }
  else {
    NU_ASSERT ( numArgs <= 1, this ); // NUFFlushSysTask::isValid checked this
                             // during creation, this assert can only
                             // happen if memory was corrupted
  }
  return mValueExprVector[0];
}

bool NUFFlushSysTask::isValid(UtString* msg)
{
  // check that we have the right number of arguments
  bool ret = true;
  SInt32 availableNumArgs = mValueExprVector.size();
  if ( availableNumArgs > 1 )
  {
    *msg << availableNumArgs << " arguments supplied, up to 1 is allowed";
    ret = false;
  }
  return ret;
}
