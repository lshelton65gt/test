// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2003-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUCase.h"
#include "nucleus/NUExpr.h"
#include "reduce/Fold.h"
#include "util/UtIndent.h"
#include "util/UtIOStream.h"
#include "util/DynBitVector.h"

NUCaseItem::~NUCaseItem()
{
  for (ConditionLoop conds = loopConditions(); !conds.atEnd(); ++conds) {
    NUCaseCondition* cond = *conds;
    delete cond;
  }
  for (NUStmtListIter p = mStmts.begin(); p != mStmts.end(); ++p) {
    NUStmt* stmt = *p;
    delete stmt;
  }
}

bool NUCaseItem::operator==(const NUCaseItem& caseItem) const
{
  return ObjListEqual<ConditionCLoop>(this->loopConditions (), caseItem.loopConditions ())
    && ObjListEqual<NUStmtCLoop>(this->loopStmts (), caseItem.loopStmts ());
}

void NUCaseItem::removeStmt(NUStmt *stmt, NUStmt *replacement)
{
  RemoveReplaceStmtIfExists(mStmts, stmt, replacement);
}


NUStmtList* NUCaseItem::getStmts() const
{
  return new NUStmtList(mStmts.begin(), mStmts.end());
}

void NUCaseItem::replaceConditions(ConditionVector &new_conds)
{
  mConditions.assign(new_conds.begin(), new_conds.end());
}

void NUCaseItem::replaceStmts(const NUStmtList& new_stmts)
{
  mStmts.assign(new_stmts.begin(), new_stmts.end());
}


NUCaseItem* NUCaseItem::copy(CopyContext &copy_context) const
{
  NUCaseItem* cpy = new NUCaseItem(getLoc());
  for (ConditionCLoop conds = loopConditions(); !conds.atEnd(); ++conds)
  {
    const NUCaseCondition* cond = *conds;
    cpy->addCondition(cond->copy(copy_context));
  }
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter)
  {
    const NUStmt* stmt = *iter;
    cpy->addStmt(stmt->copy(copy_context));
  }
  return cpy;
}


void NUCaseItem::replaceDef(NUNet * old_net, NUNet * new_net)
{
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter)
    (*iter)->replaceDef(old_net,new_net);
}


bool NUCaseItem::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  for (ConditionLoop conds = loopConditions(); !conds.atEnd(); ++conds)
    changed |= (*conds)->replace(old_net,new_net);
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter)
    changed |= (*iter)->replace(old_net,new_net);
  return changed;
}

bool NUCaseItem::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  for (ConditionLoop conds = loopConditions(); !conds.atEnd(); ++conds)
    changed |= (*conds)->replaceLeaves(translator);
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter)
    changed |= (*iter)->replaceLeaves(translator);
  return changed;
}

void NUCaseItem::compose(UtString *buf, const STBranchNode* scope, int indent, bool recurse) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  if ( mConditions.empty() )
  {
    buf->append(indent, ' ');
    *buf << "default:\n";
  }
  else
  {
    bool first = true;
    for (ConditionCLoop conds = loopConditions (); !conds.atEnd (); ++conds)
    {
      const NUCaseCondition* cond = *conds;
      if ( first ) { first = false; }
      else         { *buf << ",\n"; }
      cond->compose(buf, scope, indent, recurse);
    }
    *buf << ":\n";
  }

  // NOTE: empty case items are useful as placeholders in case statements with default clauses
  // and also to keep fully specified case statements fully specified.
  if ( mStmts.empty() )
  {
    // empty stmt list. give a semi.
    buf->append(indent + 2, ' ');
    *buf << "; // empty stmtlist\n";
  }
  else
  {
    bool needBeginEnd = (mStmts.size() > 1);
    if (needBeginEnd)
    {
      buf->append(indent, ' ');
      *buf << "begin\n";
    }
  
    for (NUStmtListIter iter = mStmts.begin (); iter != mStmts.end (); ++iter)
    {
      const NUStmt* stmt = *iter;
      stmt->compose(buf, scope, indent + 2, recurse);
    }
    if (needBeginEnd)
    {
      buf->append(indent, ' ');
      *buf << "end\n";
    }
  }
}

void NUCaseItem::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "CaseItem(" << this << ")";
  if (isDefault()) {
    UtIO::cout() << " default";
  }
  UtIO::cout() << UtIO::endl;

  if (not isDefault()) {
    indent(numspaces);
    UtIO::cout() << "Conditions:" << UtIO::endl;
    for (ConditionCLoop conds = loopConditions(); !conds.atEnd(); ++conds) {
      const NUCaseCondition* cond = *conds;
      cond->print(recurse, numspaces+2);
    }
  }

  indent(numspaces);
  UtIO::cout() << "Statements:" << UtIO::endl;
  for (NUStmtListIter iter = mStmts.begin(); iter != mStmts.end(); ++iter) {
    const NUStmt* stmt = *iter;
    stmt->print(recurse, numspaces+2);
  }
}

NUCase::NUCase(NUExpr* sel,
	       NUNetRefFactory *netref_factory,
               CaseType type,
               bool usesCFNet,
	       const SourceLocator& loc) :
  NUBlockStmt(netref_factory, usesCFNet, loc),
  mSel(sel),
  mUserFullCase(false),
  mFullySpecified(false),
  mHasDefault(false),
  mCaseType(type)
{
}


NUCase::~NUCase()
{
  clearUseDef();
  while (!mCaseItems.empty())
  {
    NUCaseItemList::iterator p = mCaseItems.begin();
    NUCaseItem* item = *p;
    mCaseItems.erase(p);
    delete item;
  }
  delete mSel;
}

bool NUCase::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUCase* caseStmt = dynamic_cast<const NUCase*>(&stmt);

  return (*mSel == *caseStmt->getSelect ())
    && ObjListEqual<ItemCLoop> (this->loopItems (), caseStmt->loopItems ());
}

void NUCase::replaceDef(NUNet* old_net,
			NUNet* new_net)
{
  for (NUCaseItemListIter p = mCaseItems.begin(); p != mCaseItems.end(); ++p)
    (*p)->replaceDef(old_net,new_net);
}

bool NUCase::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed |= mSel->replace(old_net,new_net);
  for (NUCaseItemListIter p = mCaseItems.begin(); p != mCaseItems.end(); ++p)
    changed |= (*p)->replace(old_net,new_net);
  return changed;
}

bool NUCase::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  { 
    NUExpr * repl = translator(mSel, NuToNuFn::ePre);
    if (repl) {
      mSel = repl;
      changed = true;
    }
  }
  changed |= mSel->replaceLeaves(translator);
  for (NUCaseItemListIter p = mCaseItems.begin(); p != mCaseItems.end(); ++p)
    changed |= (*p)->replaceLeaves(translator);

  { 
    NUExpr * repl = translator(mSel, NuToNuFn::ePost);
    if (repl) {
      mSel = repl;
      changed = true;
    }
  }

  return changed;
}

NUStmt* NUCase::copy(CopyContext &copy_context) const
{
  NUCaseItemList copy_items;
  NUCase* caseStmt = new NUCase(mSel->copy(copy_context), mNetRefFactory, getCaseType(), getUsesCFNet(), mLoc);
  for (NUCaseItemListIter p = mCaseItems.begin(); p != mCaseItems.end(); ++p)
    caseStmt->addItem((*p)->copy(copy_context));
  caseStmt->putUserFullCase(isUserFullCase());
  caseStmt->putFullySpecified(isFullySpecified());
  caseStmt->putHasDefault(hasDefault());
  return caseStmt;
}


void NUCase::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "Case(" << this << ")";
  if ( mUserFullCase ) {
    UtIO::cout() << " fullCase";
  }
  if ( mFullySpecified ) {
    UtIO::cout() << " fullySpecified";
  }
  if ( mHasDefault ) {
    UtIO::cout() << " hasDefault";
  }
  UtIO::cout() << UtIO::endl;
  if (recurse)
  {
    mSel->print(recurse,numspaces+2);
    for (NUCaseItemListIter iter = mCaseItems.begin();
	 iter != mCaseItems.end();
	 ++iter)
      (*iter)->print(recurse, numspaces+2);
  }
}

void NUCase::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  switch ( getCaseType() ) {
  case eCtypeCasex: { *buf << "casex("; break; }
  case eCtypeCasez: { *buf << "casez("; break; }
  case eCtypeCase:
  default:          { *buf << "case("; break; }
  }
  mSel->compose (buf, scope);
  *buf << ")";
  if (mUserFullCase)
    *buf << " // carbon full_case";
  
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();

  if (mFullySpecified) {
    buf->append(numSpaces + 2, ' ');
    *buf << "// fully-specified\n";
  }

  if (mHasDefault) {
    buf->append(numSpaces + 2, ' ');
    *buf << "// has-default\n";
  }

  if (recurse)
  {
    for (NUCaseItemListIter iter = mCaseItems.begin ();
         iter != mCaseItems.end ();
         ++iter)
      (*iter)->compose (buf, scope, numSpaces + 2, recurse);
  }

  buf->append(numSpaces, ' ');
  *buf << "endcase\n";
}
    

const char* NUCase::typeStr() const
{
  return "NUCase";
}

NUCase::CaseType NUCase::getCaseType() const
{
  return mCaseType;
}

bool NUCase::isConsistent() const
{
  int numDefaults = 0;
  for (ItemCLoop l = loopItems(); !l.atEnd(); ++l)
  {
    const NUCaseItem* caseItem = *l;
    if (caseItem->isDefault())
      ++numDefaults;
  }
  return (hasDefault() && (numDefaults == 1)) || (!hasDefault() && (numDefaults == 0));
}

NUExpr* NUCaseItem::buildConditional(const NUExpr* caseSel)
  const
{
  NUExpr* cond = NULL;
  for (ConditionCLoop p = loopConditions(); !p.atEnd(); ++p)
  {
    const NUCaseCondition* cc = *p;
    NUExpr* newCond = cc->buildConditional(caseSel);
    if (cond == NULL)
      cond = newCond;
    else
      cond = new NUBinaryOp(NUOp::eBiLogOr, cond, newCond, cc->getLoc());
  }
  if (cond != NULL)
    cond->resize(1);
  return cond;
}
  

NUExpr* NUCaseCondition::buildConditional(const NUExpr* caseSel)
  const
{

  CopyContext copy_context(NULL,NULL);

  if (mIsRange)
    return mExpr->copy(copy_context);// It is already a comparision expr

  NUExpr* expr = mExpr->copy(copy_context);
  SInt32 width = caseSel->getBitSize();
  NUExpr* sel = caseSel->copy(copy_context);

  // Mask off the don't-care bits
  if (mMask != NULL)
  {
    NUExpr* mask = mMask->copy(copy_context);
    sel = new NUBinaryOp(NUOp::eBiBitAnd, sel, mask, mLoc);
    NUConst* k = expr->castConst();
    if (k == NULL) {
      // Variable expression
      mask = mask->copy(copy_context);
      expr = new NUBinaryOp(NUOp::eBiBitAnd, expr, mask, mLoc);
    }
    else {
      // constant expression -- reduce immediately, rather than
      // making gcc do it.
      UInt32 size = expr->getBitSize();
      DynBitVector val(size), drive(size), maskVal(size), maskDrive(size);

      NUConst* maskK = mask->castConst();
      NU_ASSERT(maskK, this);
      k->getValueDrive(&val, &drive);
      maskK->getValueDrive(&maskVal, &maskDrive);
      NU_ASSERT(!maskDrive.any(), this);

      // Zero any bits in the drive that are not in the mask
      drive &= maskVal;
      val &= maskVal;

      // I don't expect any drive-bits after this.  
      if (drive.any()) {
        // warn?
        val &= ~drive;          // treat Z & X as 0
      }
      SourceLocator loc = expr->getLoc();
      delete expr;
      expr = NUConst::create(false, val, size, loc);
    }
  }

  // If Verilog was "case(1) bit[0]: ...; bit[1]: ...; endcase
  // we'd have a SIGNED selector and unsigned operands.  Since
  // we're doing an equality comparison here, force both operands
  // to an unsigned result.

  // binary sub expressions are self-determined except for case
  // statements. 
  sel->setSignedResult (false);
  expr->setSignedResult (false);

  sel = sel->makeSizeExplicit(width);
  expr = expr->makeSizeExplicit(width);

  // Create a comparison operator
  NUExpr* cmp = new NUBinaryOp(NUOp::eBiEq, sel, expr, mLoc);

  cmp->resize(1);
  return cmp;
} // NUExpr* NUCaseCondition::buildConditional

NUCaseCondition::~NUCaseCondition()
{
  // do not delete selectValue -- it's owned by NUCase
  delete mExpr;
  if (mMask != NULL)
    delete mMask;
}

bool NUCaseCondition::operator==(const NUCaseCondition& ccond) const
{
  if (*mExpr == *ccond.getExpr ())
    {
      NUExpr *cmask = ccond.getMask ();
      if (mMask && cmask)	// Both masked
	return *mMask == *cmask; // Equivalence test
      else if (mMask || cmask)	// One masked but not the other
	return false;
      else
	return true;		// neither masked
    }
  return false;			// Selection expressions different
}

bool NUCaseCondition::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed |= mExpr->replace(old_net,new_net);
  if ( mMask ) {
    changed |= mMask->replace(old_net,new_net);
  }
  return changed;
}

bool NUCaseCondition::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NUExpr * repl = translator(mExpr, NuToNuFn::ePre);
    if (repl) {
      mExpr = repl;
      changed = true;
    }
  }
  changed |= mExpr->replaceLeaves(translator);
  if ( mMask ) {
    NUExpr * repl = translator(mMask, NuToNuFn::ePre);
    if (repl) {
      mMask = repl;
      changed = true;
    }
    changed |= mMask->replaceLeaves(translator);
  }
  {
    NUExpr * repl = translator(mExpr, NuToNuFn::ePost);
    if (repl) {
      mExpr = repl;
      changed = true;
    }
  }

  if ( mMask ) {
    NUExpr * repl = translator(mMask, NuToNuFn::ePost);
    if (repl) {
      mMask = repl;
      changed = true;
    }
  }

  return changed;
}

void NUCaseCondition::getUses(NUNetSet* uses) const
{
  mExpr->getUses(uses);
  if (mMask != NULL)
    mMask->getUses(uses);
}

void NUCaseCondition::getUses(NUNetRefSet* uses) const
{
  mExpr->getUses(uses);
  if (mMask != NULL)
    mMask->getUses(uses);
}


bool NUCaseCondition::queryUses(const NUNetRefHdl &use_net_ref,
				NUNetRefCompareFunction fn,
				NUNetRefFactory *factory) const
{
  if (mMask and mMask->queryUses(use_net_ref, fn, factory)) {
    return true;
  }
  return mExpr->queryUses(use_net_ref, fn, factory);
}


NUCaseCondition* NUCaseCondition::copy(CopyContext &copy_context) const
{
  NUExpr* mask = (mMask == NULL)? 0: mMask->copy(copy_context);
  return new NUCaseCondition(mLoc, mExpr->copy(copy_context), mask, mIsRange);
}

void NUCaseCondition::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "CaseCondition(" << this << ")" << UtIO::endl;

  indent(numspaces);
  UtIO::cout() << "CaseCondition Expression:" << UtIO::endl;
  mExpr->print(recurse, numspaces + 2);

  if (mMask != NULL) {
    indent(numspaces);
    UtIO::cout() << "CaseCondition Mask:" << UtIO::endl;
    mMask->print(recurse, numspaces + 2);
  }
}

void NUCaseCondition::compose(UtString* buf, const STBranchNode* scope, int indent, bool) const
{
  buf->append(indent, ' ');
  mExpr->compose (buf, scope);
}

const char* NUCaseCondition::typeStr() const
{
  return "NUCaseCondition";
}


const char* NUCaseItem::typeStr() const
{
  return "NUCaseItem";
}


// Check that case items are suitable for optimization
//
bool
NUCase::canEmitSwitch (bool ignoreMask) const
{
  UInt32 conditionCount = 0;

  for (NUCase::ItemCLoop items = loopItems(); !items.atEnd(); ++items)
  {
    NUCaseItem* caseItem = *items;
    for (NUCaseItem::ConditionLoop conds = caseItem->loopConditions();
         !conds.atEnd(); ++conds)
    {
      const NUCaseCondition* const cond = *conds;
      if ((ignoreMask || cond->isSwitchable()) && cond->getExpr()->castConst ())
        ++conditionCount;
      else
        return false;
    }
  }

  // Emit a C-style switch statement if we've got more than 4 conditions.
  return (conditionCount > 4);

} // canEmitSwitch

void NUCase::replaceSelect(NUExpr* select)
{
  mSel = select;
}

void NUCaseItem::clearConditions(bool deleteConditions) {
  for (ConditionLoop j = loopConditions(); !j.atEnd(); ++j) {
    NUCaseCondition* cond = *j;
    if (deleteConditions) {
      delete cond;
    }
  }
  ConditionVector emptyConditions;
  replaceConditions(emptyConditions);
}

void NUCase::replaceItems(const NUCaseItemList& newList) {
  mCaseItems = newList;
}


void  NUCase::eliminateUnnecessaryCaseBits(Fold* fold)
{
  if ( mCaseType != eCtypeCase ){
    return;    // reduction not allowed for casex or casez
  }

  NUExpr* select_expr = getSelect();

  UInt32 original_sel_width = select_expr->getBitSize();
  // WATCH OUT, a constant zero has an effective bit size of 0,
  UInt32 effective_sel_width = select_expr->effectiveBitSize();
  // impl_sel_width is the width we will use for implementation of
  // a new select, (we cannot use effective_sel_width in case the sel
  // value is zero, and we cannot just change effective_sel_width to
  // be min(1,select_expr->effectiveBitSize()) because 
  // that would cause the following optimization to be missed:
  // case (2'b0)
  //  2'b0: branch1;
  //  2'b1: branch2;    // this branch is unnecessary
  //  default: branch3; // this branch is unnecessary
  //
  UInt32 impl_sel_width = (effective_sel_width == 0) ? 1 : effective_sel_width;

  if ( effective_sel_width >= original_sel_width ) {
    return;          // no reduction/elimination is possible
  }

  NUCase::ItemLoop itemsIter = loopItems();
  if (itemsIter.atEnd()) {
    return;            // no branches
  }

  NUCaseItem* lastItem = NULL;
  
  
  bool found_case_label_to_reduce = false;
  bool found_case_label_to_remove = false;
  // check that all case labels are constants
  for (; !itemsIter.atEnd(); ++itemsIter){
    NUCaseItem* caseItem = *itemsIter;
    lastItem = caseItem;
    
    for (NUCaseItem::ConditionLoop condIter = caseItem->loopConditions();
	 !condIter.atEnd();
	 ++condIter) {
      NUCaseCondition* caseCond = *condIter;
      NUConst* constCaseCondExpr = caseCond->getExpr()->castConst();
      NUExpr* caseCondMaskExpr = caseCond->getMask();
      NUConst* constCaseCondMask = dynamic_cast<NUConst*>(caseCondMaskExpr);
      // Only process constants
      if ( ( constCaseCondExpr == NULL ) || (caseCondMaskExpr && ( constCaseCondMask == NULL )) ) {
        return;  // found a non constant case label, nothing to do here
      }

      if ( caseCondMaskExpr ) {
        DynBitVector mask;
        constCaseCondMask->getSignedValue(&mask);
        if ( mask.any() ){
          return;  // found z or x in case condition, not supported here
        }
      }
      

      UInt32 original_case_item_width = constCaseCondExpr->getBitSize();
      UInt32 effective_case_item_width = constCaseCondExpr->effectiveBitSize();

      found_case_label_to_reduce |= (original_case_item_width > effective_sel_width);
      found_case_label_to_remove |= (effective_case_item_width > effective_sel_width);
    }
  }
  
  if ( not found_case_label_to_reduce ) {
    return;            // no branch found that could be reduced
  }

  NUCaseItem* defaultItem = (hasDefault() ? lastItem : NULL );
 
  // now we know we might reduce the size of the selector and
  // at least one of the case labels will have it's size reduced
  // first do the select_expr (note this will force it to be unsigned)
  const SourceLocator &loc = select_expr->getLoc();
  if ( original_sel_width > impl_sel_width ) {
    NU_ASSERT((impl_sel_width>0), select_expr);
    NUExpr* new_sel_expr = new NUVarselRvalue(select_expr, ConstantRange(impl_sel_width-1,0),loc);
    if ( fold ){
      bool oldVerbose = fold->putVerbose(false);
      new_sel_expr = fold->fold(new_sel_expr,eFoldAggressive);
      fold->putVerbose(oldVerbose);
    }
    replaceSelect(new_sel_expr);
  }
  
  NUCaseItemList unrechableCaseItems; // to be removed at end of processing
  for (NUCase::ItemLoop itemsIter = loopItems(); !itemsIter.atEnd(); ++itemsIter){
    NUCaseItem* caseItem = *itemsIter;
    if ( caseItem == defaultItem ) {
      continue;
    }

    // look at the conditions for this caseItem
    // build a new condition list that only contains the reachable
    // conditions, and each rechable condition has been resized to impl_sel_width
    NUCaseItem::ConditionVector new_conditions;
    NUCaseItem::ConditionVector unneeded_conditions;

    for (NUCaseItem::ConditionLoop condIter = caseItem->loopConditions();
	 !condIter.atEnd();
	 ++condIter) {
      NUCaseCondition* caseCond = *condIter;
      NUConst* constCaseCondExpr = caseCond->getExpr()->castConst();
      UInt32 effective_case_item_width = constCaseCondExpr->effectiveBitSize();
      if ( effective_case_item_width > effective_sel_width ) {
        unneeded_conditions.push_back(caseCond);
        continue;               // this is an unreachable case condition
      }
      // make new caseCondExpr (force to be unsigned)
      NUConst* new_constCaseCondExpr = constCaseCondExpr->rebuild(false, impl_sel_width);
      caseCond->replaceExpr(new_constCaseCondExpr);
      delete constCaseCondExpr;
      constCaseCondExpr = NULL;

      NUExpr* caseCondMaskExpr = caseCond->getMask();
      if ( caseCondMaskExpr ) {
        NUConst* constCaseCondMask = caseCondMaskExpr->castConst();
        NUConst* new_constCaseCondMask = constCaseCondMask->rebuild(false, impl_sel_width);
        caseCond->replaceMask(new_constCaseCondMask);
        delete constCaseCondMask;
        constCaseCondMask = NULL;
      }
      new_conditions.push_back(caseCond);
    }

    if (new_conditions.empty()) {
      // this item has become redundant, because all its conditions
      // could never be selected
      unrechableCaseItems.push_back (caseItem);
    } else {
      caseItem->replaceConditions(new_conditions);
      // and remove any unneeded_conditions
      for (SInt32 index = unneeded_conditions.size()-1; index >= 0; index--){
        delete unneeded_conditions[index];
      }
    }
  }

  for(NUCaseItemList::iterator i = unrechableCaseItems.begin ();
      i != unrechableCaseItems.end ();
      ++i) {
    removeItem(*i);
    delete *i;
  }
}


