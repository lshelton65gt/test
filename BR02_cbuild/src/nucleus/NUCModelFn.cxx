// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "nucleus/NUCModelFn.h"
#include "nucleus/NUCModelInterface.h"
#include "util/StringAtom.h"

NUCModelFn::NUCModelFn(NUCModelInterface* cmodelInterface, UtString* context) :
  mCModelInterface(cmodelInterface)
{
  if (context != NULL)
    mContext = new UtString(*context);
  else
    mContext = NULL;
  mDefUseMap = new DefUseMap;
  mDefs = new Defs;
}

NUCModelFn::~NUCModelFn()
{
  if (mContext != NULL)
    delete mContext;
  delete mDefUseMap;
  delete mDefs;
}

const SourceLocator& NUCModelFn::getLoc() const
{
  return mCModelInterface->getLoc();
}

void NUCModelFn::print(bool /* verbose */, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  getLoc().print();
  UtIO::cout() << "CModelFn(" << this << ") : "
	       << mCModelInterface->getName()->str() << UtIO::endl;
}

void NUCModelFn::compose(UtString* buf, const STBranchNode* /* scope */, int numSpaces, bool /*recurse*/) const
{
  buf->append(numSpaces, ' ');
  *buf << mCModelInterface->getName()->str() << "()\n";
}

StringAtom* NUCModelFn::getName() const
{
  return mCModelInterface->getName();
}

void NUCModelFn::addUse(NUCModelPort* out, NUCModelPort* in)
{
  NU_ASSERT(mDefs->find(out) != mDefs->end(), this);
  mDefUseMap->insert(DefUseMap::value_type(out, in));
}

NUCModelFn::DefUseMapLoop NUCModelFn::loopUD()
{
  return DefUseMapLoop(*mDefUseMap);
}

NUCModelFn::DefUseMapCLoop NUCModelFn::loopUD() const
{
  return DefUseMapCLoop(*mDefUseMap);
}

void NUCModelFn::addDef(NUCModelPort* out)
{
  mDefs->insert(out);
}

NUCModelFn::DefsLoop NUCModelFn::loopDefs()
{
  return DefsLoop(*mDefs);
}

NUCModelFn::DefsCLoop NUCModelFn::loopDefs() const
{
  return DefsCLoop(*mDefs);
}


