// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Implementation of the Nucleus declaration scope and elaborated
  declaration scope classes.
 */

#include "nucleus/NUNamedDeclarationScope.h"

#include "nucleus/NUBitNet.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUAliasDB.h"

#include "util/UtIndent.h"
#include "util/HierStringName.h"
#include "iodb/IODBNucleus.h"
#include "compiler_driver/CarbonDBWrite.h"


//! returns true if the string was escaped and has been removed
static bool sUnEscape ( UtString *pBuf )
{
  bool charRemoved = false;
  if ( (*pBuf)[0] == '\\' )
  {
    pBuf->erase(0,1);
    charRemoved = true;
    int lastCharPos = pBuf->size()-1;
    if ( (*pBuf)[lastCharPos] == ' ' )
    {
      pBuf->erase(lastCharPos,1);
    }
  }
  return charRemoved;
}


//! takes a buffer and two potentially empty other buffers (prepend and append)
//  combines them into pdestBuf with the resulting string properly escaped.
static void sEscNameIfNecessary ( UtString *const pdestBuf,
                                  const UtString *const pprependBuf,
                                  const UtString *const pappendBuf )
{
  bool needsEscape = false;
  bool appendNeedsEscape = false;
  bool prependNeedsEscape = false;
  bool destNeedsEscape = false;
  UtString resultBuf;
  

  if ( pprependBuf && ( not pprependBuf->empty() ))
  {
    UtString tempBuf = *pprependBuf;

    prependNeedsEscape  = sUnEscape ( &tempBuf );
    prependNeedsEscape |= ( (tempBuf)[0] == '$' );
    resultBuf << tempBuf;
  }

  if ( not pdestBuf->empty() )
  {
    UtString tempBuf = *pdestBuf;

    destNeedsEscape  = sUnEscape ( &tempBuf );
    destNeedsEscape |= ((tempBuf)[0] == '$' );
    resultBuf << tempBuf;
  }
  if ( pappendBuf && ( not pappendBuf->empty()))
  {
    UtString tempBuf = *pappendBuf;

    appendNeedsEscape  = sUnEscape ( &tempBuf );
    appendNeedsEscape |= ( (tempBuf)[0]  == '$' );
    resultBuf << tempBuf;
  }

  needsEscape = destNeedsEscape || prependNeedsEscape || appendNeedsEscape;

  if ( needsEscape )
  {
    resultBuf.insert(0, 1, '\\');
    resultBuf.append(1, ' ');
  }
  *pdestBuf = resultBuf;
}


NUNamedDeclarationScope::NUNamedDeclarationScope(StringAtom * name,
                                                 NUScope * parent,
                                                 IODBNucleus * iodb,
                                                 NUNetRefFactory * netref_factory,
                                                 const SourceLocator & loc,
						 HierFlags hierFlag) :
  NUScope(iodb, HierFlags(eHierTypeMask & hierFlag) ),
  mName(name),
  mParent(parent),
  mMySymtabIndex(parent->reserveSymtabIndex()),
  mCurSymtabIndex(0),
  mCurNameIndex(0),
  mNetRefFactory(netref_factory),
  mLoc(loc)
{
  mParent->addDeclarationScope(this);
}


NUNamedDeclarationScope::~NUNamedDeclarationScope()
{
  mParent->removeDeclarationScope(this);

  // Delete local instances and instances in nested child scopes.
  deleteAllInstances();

  // Note: For declaration scopes, we _DO_ need to manually delete the
  // nested scopes because they are not otherwise referenced from the
  // Nucleus tree.
  while (not mDeclarationScopeList.empty()) {
    // deleting the scope removes it from the list.
    NUNamedDeclarationScope * scope = (*mDeclarationScopeList.begin());
    mDeclarationScopeList.erase(mDeclarationScopeList.begin());
    delete scope;
  }

  // Delete locally declared nets.
  for (NUNetLoop iter = loopLocals(); not iter.atEnd(); ++iter) {
    mNetRefFactory->erase(*iter);
    delete *iter;
  }

}

void NUNamedDeclarationScope::deleteAllInstances()
{
  // Delete all instances in nested child scopes.
  for (NUNamedDeclarationScopeLoop loop = loopDeclarationScopes();
       !loop.atEnd(); ++loop) {
    (*loop)->deleteAllInstances();
  }
  // Delete all instances in this scope.
  while (not mInstanceList.empty()) {
    delete *(mInstanceList.begin());
    mInstanceList.erase(mInstanceList.begin());
  }
}

void NUNamedDeclarationScope::removeAllInstances()
{
  // Remove all instances in nested child scopes.
  for (NUNamedDeclarationScopeLoop loop = loopDeclarationScopes();
       !loop.atEnd(); ++loop) {
    NUNamedDeclarationScope * scope = *loop;
    scope->removeAllInstances();
  }
  // Remove all instances in this scope.
  mInstanceList.clear();
}

bool NUNamedDeclarationScope::isEmpty(NUNamedDeclarationScopeList* emptyDeclScopes)
{
  bool empty = true;
  // Are all nested scopes empty?
  for (NUNamedDeclarationScopeLoop loop = loopDeclarationScopes();
       !loop.atEnd(); ++loop) {
    NUNamedDeclarationScope * scope = *loop;
    empty &= scope->isEmpty(emptyDeclScopes);
  }
  // Do we have instances?
  empty &= mInstanceList.empty();
  // Do we have local nets?
  empty &= mLocalNetList.empty();
  // Still empty? Add self to empty declaration scopes list.
  if (empty && emptyDeclScopes) {
    emptyDeclScopes->push_back(this);
  }
  return empty;
}

SInt32 NUNamedDeclarationScope::reserveSymtabIndex()
{
  return mCurSymtabIndex++;
}


SInt32 NUNamedDeclarationScope::getSymtabIndex() const
{
  return mMySymtabIndex;
}


NUNamedDeclarationScopeElab *NUNamedDeclarationScope::lookupElab(STBranchNode *hier) const
{
  NUNamedDeclarationScopeElab * elab = dynamic_cast<NUNamedDeclarationScopeElab*>(lookupElabScope(hier));
  ST_ASSERT(elab, hier);
  return elab;
}


NUNamedDeclarationScopeElab *NUNamedDeclarationScope::createElab(STBranchNode *hier, STSymbolTable *symtab)
{
  STBranchNode* branch_node = symtab->createBranch(getName(), hier, 
                                                  mMySymtabIndex);

  NUNamedDeclarationScopeElab * elab = new NUNamedDeclarationScopeElab(this, branch_node);
  
  CbuildSymTabBOM::putNucleusObj(branch_node, elab);
  return elab;
}


void NUNamedDeclarationScope::addBlock(NUBlock *)
{
  NU_ASSERT(0=="can't add block to declaration scope", this);
}


void NUNamedDeclarationScope::removeBlock(NUBlock *)
{
  NU_ASSERT(0=="can't add block to declaration scope", this);
}


NUBlockCLoop NUNamedDeclarationScope::loopBlocks() const 
{ 
  // construct empty list on the stack so the Loop will be initialized.
  NUBlockList dummy;
  return NUBlockCLoop(dummy); // empty loop
}


NUBlockLoop NUNamedDeclarationScope::loopBlocks() 
{ 
  // construct empty list on the stack so the Loop will be initialized.
  NUBlockList dummy;
  return NUBlockLoop(dummy); // empty loop
}


void NUNamedDeclarationScope::addDeclarationScope(NUNamedDeclarationScope *scope)
{
  mDeclarationScopeList.push_back(scope);
}


void NUNamedDeclarationScope::removeDeclarationScope(NUNamedDeclarationScope *scope)
{
  mDeclarationScopeList.remove(scope);
}


NUNamedDeclarationScopeCLoop NUNamedDeclarationScope::loopDeclarationScopes() const 
{ 
  return NUNamedDeclarationScopeCLoop(mDeclarationScopeList); 
}


NUNamedDeclarationScopeLoop NUNamedDeclarationScope::loopDeclarationScopes() 
{
  return NUNamedDeclarationScopeLoop(mDeclarationScopeList); 
}


void NUNamedDeclarationScope::addLocal(NUNet *net)
{
  if (inTFHier()) {
    // Task-based declaration scopes are only allowed to contain
    // non-static nets (since all task nets are non-static).
    NU_ASSERT((net->isBlockLocal() and net->isNonStatic()), net);
  } else {
    // Module-based declaration scopes are only allowed to contain static nets.
    NU_ASSERT((net->isBlockLocal() and not net->isNonStatic()), net);
  }

  addNetName(net);
  mLocalNetList.push_back(net);
  mIODB->addTypeIntrinsic( net );
}


void NUNamedDeclarationScope::removeLocal(NUNet *net, bool remove_references)
{
  mLocalNetList.remove(net);
  removeNetName(net);
  if (remove_references) {
    mNetRefFactory->erase(net);
  }
}

void NUNamedDeclarationScope::removeLocals(const NUNetSet& nets, bool remove_references) {
  NUScope::removeNetsHelper(nets, remove_references, &mLocalNetList);
}

void NUNamedDeclarationScope::getAllNonTFNets(NUNetList *net_list) const
{
  NUNetCLoop net_loop = loopLocals();
  net_list->insert(net_list->end(), net_loop.begin(), net_loop.end());

  getAllSubNets(net_list);
}


void NUNamedDeclarationScope::getAllNets(NUNetList *net_list) const
{
  getAllNonTFNets(net_list);
}


void NUNamedDeclarationScope::getAllSubNets(NUNetList *net_list) const
{
  for (NUNamedDeclarationScopeCLoop loop = loopDeclarationScopes(); not loop.atEnd(); ++loop) {
    (*loop)->getAllNets(net_list);
  }
}

void NUNamedDeclarationScope::addModuleInstance(NUModuleInstance* instance)
{
  mInstanceList.push_back(instance);
}

void NUNamedDeclarationScope::removeModuleInstance(NUModuleInstance* instance,
                                                   NUModuleInstance* replacement)
{
  mInstanceList.remove(instance);
  if (replacement) {
    mInstanceList.push_back(replacement);
  }
}

NetFlags NUNamedDeclarationScope::getTempFlags() const
{
  // base the flag set on the parent (NUTF or NUModule) so that
  // eNonStatic is determined by it.
  return NetFlags(getParentScope()->getTempFlags() | eBlockLocalNet);
}


const char* NUNamedDeclarationScope::typeStr() const
{
  return "NamedDeclarationScope";
}


// ACA: I don't like having to copy this from NUUseDefNode.
void NUNamedDeclarationScope::printVerilog(bool addNewline, int numSpaces, bool recurse) const
{
  UtString buf;
  this->compose (&buf, NULL /* scope */,  numSpaces, recurse);
  UtIO::cout () << buf;
  if (addNewline)
    UtIO::cout () << UtIO::endl;
}


void NUNamedDeclarationScope::print(bool recurse_arg, int indent_arg) const
{
  // this strange testing of args is for protection from calls from
  // within gdb  where the default args should have been used. We do
  // not modify the args themselves because that can corrupt the stack
  bool recurse = recurse_arg;
  int  indent = indent_arg;
  if ( ( recurse_arg != true ) && ( recurse_arg != false ) ) { recurse = true; }
  if ( abs(indent_arg) > 150 ) indent = 0;

  for (int i = 0; i < indent; i++) {
    UtIO::cout() << " ";
  }
  mLoc.print();
  UtIO::cout() << "NamedDeclarationScope(" << this << ") : " << mName->str() << " : "
	    << "ParentScope(" << mParent->getName()->str()
	    << ")" << UtIO::endl;
  if (recurse) {
    for (NUNetCLoop iter = loopLocals(); not iter.atEnd(); ++iter) {
      (*iter)->print(false, indent+2);
    }
    for (NUNamedDeclarationScopeCLoop iter = loopDeclarationScopes(); not iter.atEnd(); ++iter) {
      (*iter)->print(recurse, indent+2);
    }
    for (NUModuleInstanceMultiCLoop iter = loopDeclScopeInstances(); not iter.atEnd(); ++iter) {
      (*iter)->print(recurse, indent+2);
    }
  }
}


void NUNamedDeclarationScope::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  if ( loopLocals().atEnd() and loopDeclarationScopes().atEnd() and loopInstances().atEnd() ) {
    return; // nothing to include in the block
  }
  
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  UtString blockName = getName()->str();
  if ( scope != NULL )
  {
    UtString name;
    scope->compose(&name, true);   // include root in name 
    sEscNameIfNecessary(&blockName, &name, NULL);
  }
  else
  {
    sEscNameIfNecessary(&blockName, NULL, NULL);
  }
  *buf << "begin : " << blockName;
  indent.tabToLocColumn();
  *buf << " // ";
  getLoc().compose(buf);
  indent.newline();

  numSpaces += 2;

  // print local net declarations
  for (NUNetCLoop p = loopLocals(); !p.atEnd(); ++p) {
    NUNet* net = *p;
    buf->append(numSpaces, ' ');
    net->composeType(buf);
    net->composeDeclaration(buf);
    *buf << "\n";
  }

  // print declarations within nested declaration scopes.
  for (NUNamedDeclarationScopeCLoop p = loopDeclarationScopes(); !p.atEnd(); ++p) {
    (*p)->compose(buf, scope, numSpaces, recurse);
  }

  // print instances within declaration scopes.
  for (NUModuleInstanceMultiCLoop iter = loopDeclScopeInstances(); not iter.atEnd();
       ++iter) {
    (*iter)->compose(buf, scope, numSpaces, recurse);
  }

  numSpaces -= 2;
  buf->append(numSpaces, ' ');
  *buf << "end\n";
}

bool NUNamedDeclarationScope::hasInstances() const
{
  bool foundInstances = false;
  // Does this scope have instances?
  if (not mInstanceList.empty()) {
    foundInstances = true;
  }
  // Do nested declaration scopes have instances?
  for (NUNamedDeclarationScopeCLoop p = loopDeclarationScopes();
       !foundInstances && !p.atEnd(); ++p) {
    foundInstances = (*p)->hasInstances();
  }
  return foundInstances;
}

NUModuleInstanceMultiLoop NUNamedDeclarationScope::loopInstances()
{
  NUModuleInstanceMultiLoop miLoop = loopDeclScopeInstances();
  for (NUNamedDeclarationScopeLoop iter = loopDeclarationScopes();
       !iter.atEnd(); ++iter) {
    NUNamedDeclarationScope* declScope = *iter;
    miLoop.pushLoopMulti(declScope->loopInstances());
  }
  return miLoop;
}

NUModuleInstanceMultiCLoop NUNamedDeclarationScope::loopInstances() const
{
  NUModuleInstanceMultiCLoop miLoop = loopDeclScopeInstances();
  for (NUNamedDeclarationScopeCLoop iter = loopDeclarationScopes();
       !iter.atEnd(); ++iter) {
    const NUNamedDeclarationScope* declScope = *iter;
    miLoop.pushLoopMulti(declScope->loopInstances());
  }
  return miLoop;
}

NUModuleInstanceMultiLoop NUNamedDeclarationScope::loopDeclScopeInstances()
{
  NUModuleInstanceMultiLoop miLoop;
  miLoop.pushLoop(NUModuleInstanceListLoop(mInstanceList));
  return miLoop;
}

NUModuleInstanceMultiCLoop NUNamedDeclarationScope::loopDeclScopeInstances() const
{
  NUModuleInstanceMultiCLoop miLoop;
  miLoop.pushLoop(NUModuleInstanceListCLoop(mInstanceList));
  return miLoop;
}

NUNamedDeclarationScopeElab::NUNamedDeclarationScopeElab(NUNamedDeclarationScope * scope, 
                                                         STBranchNode *hier) :
  NUScopeElab(scope, hier)
{
}


NUNamedDeclarationScopeElab::~NUNamedDeclarationScopeElab()
{
}


const char* NUNamedDeclarationScopeElab::typeStr() const
{
  return "NamedDeclarationScopeElab";
}


NUNamedDeclarationScope* NUNamedDeclarationScopeElab::getScope() const
{
  NUNamedDeclarationScope* scope = dynamic_cast<NUNamedDeclarationScope*>(mScope);
  NU_ASSERT(scope, mScope->getModule ());
  return scope;
}


//! Realise NUScopeElab::getSourceLocation virtual method
/*virtual*/ const SourceLocator NUNamedDeclarationScopeElab::getSourceLocation () const
{
  return getScope ()->getLoc ();
}


//! Realise NUElabBase::findUnelaborated virtual method
/*virtual*/ bool NUNamedDeclarationScopeElab::findUnelaborated (STSymbolTable *unelabStab, 
  STSymbolTableNode **unelab) const
{
  // find the unelaborated symbol table entry for the enclosing scope
  STBranchNode *elabParent = getHier ()->getParent ();
  STSymbolTableNode *unelabParent;
  INFO_ASSERT (elabParent != NULL, "bogus symbol table entry");
  NUBase *base;
  NUElabBase *elabBase;
  if ((base = CbuildSymTabBOM::getNucleusObj (elabParent)) == NULL) {
    // no object
    NU_ASSERT (base != NULL, this);
    return false;
  } else if ((elabBase = dynamic_cast <NUElabBase *> (base)) == NULL) {
    // not an elaboration object
    NU_ASSERT (elabBase != NULL, this);
    return false;
  } else if (!elabBase->findUnelaborated (unelabStab, &unelabParent)) {
    // Did not find the unelaborated symbol for the parent
    return false;
  } else {
    // unelabParent contains the unelaborated parent for the unelaborated scope
  }
  // now form the unelaborated name from the unelaborated parent symbol and the
  // name of this
  HierStringName tmp (getScope ()->getName ());
  tmp.putParent (unelabParent);
  *unelab = unelabStab->safeLookup (&tmp);
  return (*unelab != NULL);
}


NUNamedDeclarationScopeElab* NUNamedDeclarationScopeElab::lookup(STBranchNode *hier)
{
  NUNamedDeclarationScopeElab *elab = dynamic_cast<NUNamedDeclarationScopeElab*>(lookupScope(hier));
  ST_ASSERT(elab, hier);
  return elab;
}

NUNamedDeclarationScopeElab* NUNamedDeclarationScopeElab::find(const STSymbolTableNode* node)
{
  return dynamic_cast<NUNamedDeclarationScopeElab*>(NUElabBase::find(node));
}

void NUNamedDeclarationScopeElab::print(bool recurse_arg, int indent_arg) const
{
  // this strange testing of args is for protection from calls from
  // within gdb  where the default args should have been used. We do
  // not modify the args themselves because that can corrupt the stack
  bool recurse = recurse_arg;
  int  indent = indent_arg;
  if ( ( recurse_arg != true ) && ( recurse_arg != false ) ) { recurse = true; }
  if ( abs(indent_arg) > 150 ) indent = 0;

  UtString buf;
  mSymNode->compose(&buf);
  for (int i = 0; i < indent; ++i) {
    UtIO::cout() << " ";
  }
  UtIO::cout() << "DeclarationScopeElab(" << this << ") :  DeclarationScope(" << mScope
       << ")  BranchNode(" << mSymNode << ")" << UtIO::endl;
  if (recurse) {
    getScope()->print(recurse, indent+2);
  }
}


void NUNamedDeclarationScopeElab::printElab(STSymbolTable *symtab, bool recurse_arg, int indent_arg) const
{
  // this strange testing of args is for protection from calls from
  // within gdb  where the default args should have been used. We do
  // not modify the args themselves because that can corrupt the stack
  bool recurse = recurse_arg;
  int  indent = indent_arg;
  if ( ( recurse_arg != true ) && ( recurse_arg != false ) ) { recurse = true; }
  if ( abs(indent_arg) > 150 ) indent = 0;

  for (int i = 0; i < indent; i++) {
    UtIO::cout() << " ";
  }
  UtIO::cout() << "NamedDeclarationScopeElab(" << this << ") :  Block(" << mScope
       << ")  BranchNode(" << mSymNode << ")" << UtIO::endl;
  if (recurse) {
    for (NUNetLoop iter = getScope()->loopLocals();
         !iter.atEnd();
         ++iter) {
      (*iter)->lookupElab(getHier())->printElab(symtab, true, indent+2);
    }
  }
}

