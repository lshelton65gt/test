// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUExpr.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUCompositeExpr.h"
#include "util/StringAtom.h"
#include "util/CarbonAssert.h"
#include "util/UtIOStream.h"
#include "util/CarbonTypeUtil.h"
#include <cctype>               // for isalpha

void NUExpr::printVerilog(bool addNewline)
  const
{
  UtString buf;
  if ( this == NULL ){
    buf << "!!!expression is NULL!!!";
  } else {
    compose(&buf, NULL);
  }
  if (addNewline) {
    buf << '\n';
  }
  fputs(buf.c_str(), stdout);
  fflush(stdout);
}

// verilog printing functions
void NUEdgeExpr::composeHelper(UtString* buf,
                               const STBranchNode* scope,
                               bool includeRoot,
                               bool hierName,
                               const char* separator )
  const
{
  *buf << ClockEdgeString( mEdge ) << " ";
  mExpr->compose(buf, scope, includeRoot, hierName, separator);
}

void NUIdentRvalue::composeHelper(UtString* buf,
                                  const STBranchNode* scope,
                                  bool includeRoot,
                                  bool hierName,
                                  const char* separator ) const
{
  bool showRoot = includeRoot;
  if ( scope == NULL )
  {
    NUScope* actualScope = mNet->getScope();
    NUScope::ScopeT typeOfScope = actualScope->getScopeType();
    if ( typeOfScope == NUScope::eNamedDeclarationScope ) {
      showRoot = true;
    } else {
      showRoot = false;
    }
  }

  mNet->compose(buf, scope, showRoot, hierName, separator);
}

void NUIdentRvalueElab::composeHelper(UtString* buf,
                                      const STBranchNode* scope,
                                      bool includeRoot,
                                      bool hierName,
                                      const char* separator ) const
{
  mNetElab->compose(buf, scope, includeRoot, hierName, separator);
}

/*!
 * Print a range expression using either the constant partselect, indexed partselect
 * or bitselect syntax.
 *
 * \param buf pointer to UtString to format into
 * \param r range expression
 * \param var TRUE if range is part of an indexed partselect
 */
static void sPrintRange (UtString *buf, const ConstantRange & r, bool var)
{
  if (var)                      // Use variable partsel syntax
  {
    if ( r.getLsb () != 0)
      *buf << "+" << r.getLsb ();
    if (r.getLength () > 1)
      *buf << " +: " << r.getLength ();
  }
  else
  {
    *buf << r.getMsb ();
    if (r.getMsb () != r.getLsb ()) 
      // Treat [k:k] as a bitsel for printing
      *buf << ":" << r.getLsb();
  }
}

void NUVarselRvalue::composeHelper(UtString* buf,
                                   const STBranchNode* scope,
                                   bool includeRoot,
                                   bool hierName,
                                   const char* separator ) const
{
  if (mSignedResult)
    *buf << "$SIGNED(";
  mIdent->compose(buf, scope, includeRoot, hierName, separator);
  *buf << "[";

  ConstantRange r (mRange);
  bool showRangeAsNormalizedSelect = false; // this is set to true if the range is in terms of a resynthesized memory, but this memory is not yet resynthesized
  bool constRange = isConstIndex () && getConstIndex () == 0;

  if (isArraySelect())
  {
    NUMemoryNet* mem = getIdentNet(false)->getMemoryNet();
    NU_ASSERT(mem, this);
    ConstantRange declaredRange = (mem->isResynthesized() ? *(mem->getDeclaredWidthRange()) : *(mem->getRange(0)));
    if (not constRange)
    {
      CopyContext cc(0,0);
      NUExpr* denormalizedSelect = mem->denormalizeSelectExpr(mExpr->copy(cc), 0);
      denormalizedSelect->compose(buf, scope, includeRoot, hierName, separator);
      delete denormalizedSelect;
    }
    else
    {
      r.denormalize(&declaredRange, false);
      //RJC RC#2013/12/18 (cloutier)
      // showRangeAsNormalizedSelect = (! mem->isResynthesized() && (0 != r.compare(declaredRange)));  //RJC (cloutier) this was a bad idea, remove this and related code in the next commit
    }
  }
  else if (mIdent->isWholeIdentifier())
  {
    // Convert range to declared range (which might be little-endian)
    if (NUVectorNet* vn = dynamic_cast<NUVectorNet*>(mIdent->getWholeIdentifier()))
    {
      if (not constRange)
      {
        CopyContext cc(0,0);
        NUExpr* denormalized = vn->denormalizeSelectExpr(mExpr->copy(cc));
        denormalized->compose(buf, scope, includeRoot, hierName, separator);
        delete denormalized;

      }
      else
        // Convert the partselect range if it's a constant partselect
        r.denormalize (vn->getDeclaredRange (), false);

    }
    else if (not constRange) {
      // Not a vector net?
      mExpr->compose(buf, scope, includeRoot, hierName, separator);
    }
  }
  else if (not constRange) {
    mExpr->compose(buf, scope, includeRoot, hierName, separator);
  }

  // Note the code related to showRangeAsNormalizedSelect is needed to display in -dumpVerilog output a construct that would be difficult to
  // display.  It uses a non-valid verilog syntax.  Normally this will only appear in dumpVerilog output files created before
  // memoryResynthesis is complete.  
  // If the range for this varsel is in terms of the resynthesized memory, but this memory is not yet resynthesized then we show it
  // with an extra set of []:
  // reg [3:0] [1:0] mem [0:7][0:5];
  //  mem[7][5] [[6:0]];
  // for this example this is to indicate that we want the low 7 bits of packed dimensions of mem (which has 4*2 or 8 bits).
  // a more correct syntax might be:
  //  {mem[7][5]} [6:0];
  // but this would require that we compose into a temp buf and assemble the result.
  if ( showRangeAsNormalizedSelect ) {*buf << " ["; } // extra [] wrapping this range is not valid verilog, but shows reader that we want a normalized partselect 
  // If we had a non-constant component to the index-expression, we printed it already
  sPrintRange (buf, r, !constRange);
  if ( showRangeAsNormalizedSelect ) {*buf << "] "; }

  *buf << "]";
  if (mSignedResult)
    *buf << ")";

} // void NUVarselRvalue::composeHelper

void NUMemselRvalue::composeHelper(UtString* buf,
                                   const STBranchNode* scope,
                                   bool includeRoot,
                                   bool hierName,
                                   const char* separator ) const
{
  if (mSignedResult)
    *buf << "$SIGNED(";

  mIdent->compose(buf, scope, includeRoot, hierName, separator);
  for( UInt32 i = 0; i < mExprs.size(); ++i )
  {
    *buf << "[";
    mExprs[i]->compose(buf, scope, includeRoot, hierName, separator);
    *buf << "]";
  }

  if (mSignedResult)
    *buf << ")";
}

void NUUnaryOp::composeHelper(UtString* buf,
                              const STBranchNode* scope,
                              bool includeRoot,
                              bool hierName,
                              const char* separator ) const
{
  const char* opStr = getOpChar(false);
  bool operand_needs_parens = (*opStr == '$'); // currently only system functions need parens for operand
  bool operator_needs_space = isalpha(*opStr);
  *buf << "(";
  *buf << opStr;
  if (operator_needs_space) {
    *buf << " ";
  }
  if ( operand_needs_parens ) {
    *buf << "(";
  }
  mExpr->compose(buf, scope, includeRoot, hierName, separator);
  if ( operand_needs_parens ) {
    *buf << ")";
  }
  *buf << ")";
}

void NULut::composeHelper(UtString* buf,
                          const STBranchNode* scope,
                          bool includeRoot,
                          bool hierName,
                          const char* separator ) const
{
  NUExprCLoop exprs = loopExprs();

  // Print out as:  LUT(<select>,{<row1>,<row2>...})
  // Suggestion from the code review:
  //  how about using the repeat count format for the default value?
  // example:
  //  LUT(v[7:0],{1,8,3,7,7,251{8}})

  *buf << "LUT(";
  (*exprs)->compose(buf, scope, includeRoot, hierName, separator);
  *buf << ",{";

  bool first = true;
  for (++exprs; not exprs.atEnd(); ++exprs, first = false) {
    if (not first) {
      *buf << ",";
    }
    (*exprs)->compose(buf, scope, includeRoot, hierName, separator);
  }

  *buf << "})";
}

void NUBinaryOp::composeHelper(UtString* buf,
                               const STBranchNode* scope,
                               bool includeRoot,
                               bool hierName,
                               const char* separator ) const
{
  *buf << "(";
  mExpr1->compose(buf, scope, includeRoot, hierName, separator);
  *buf << " " << getOpChar(false) << " ";
  mExpr2->compose(buf, scope, includeRoot, hierName, separator);
  *buf << ")";
}

void NUTernaryOp::composeHelper(UtString* buf,
                                const STBranchNode* scope,
                                bool includeRoot,
                                bool hierName,
                                const char* separator ) const
{
  if (mSignedResult)
    *buf << "$SIGNED(";

  NU_ASSERT(getOp() == NUOp::eTeCond, this);
  *buf << "(";
  mExpr1->compose(buf, scope, includeRoot, hierName, separator);
  *buf << " ? ";
  mExpr2->compose(buf, scope, includeRoot, hierName, separator);
  *buf << " : ";
  mExpr3->compose(buf, scope, includeRoot, hierName, separator);
  *buf << ")";

  if (mSignedResult)
    *buf << ")";

}

void NUConcatOp::composeHelper(UtString* buf,
                               const STBranchNode* scope,
                               bool /* includeRoot */,
                               bool /* hierName */,
                               const char* /* separator */ ) const
{
  NU_ASSERT(getOp() == NUOp::eNaConcat, this);

  if (mSignedResult)
    *buf << "$SIGNED(";

  if (mRepeatCount != 1)
  {
    *buf << "{";
    *buf << mRepeatCount;
  }
  *buf << "{";
  for (size_t i = 0; i < mExprs.size(); ++i)
  {
    if (i != 0)
      *buf << ",";
    NUConst* as_const = dynamic_cast<NUConst*>(mExprs[i]);
    if ( as_const and as_const->isDefinedByString() ){
      // since the original came from a string this can be printed as a string
      // the size will be correct
      as_const->composeNoProtected(buf, scope);
    } else {
      // use composeSized here since any constant operands of concat must be
      // sized and hex since they may be large 
      mExprs[i]->composeSized(buf, scope, eCarbonHex); 
    }
  }
  *buf << "}";
  if (mRepeatCount != 1)
  {
    *buf << "}";
  }

  if (mSignedResult)
    *buf << ")";
}


void NUSysFunctionCall::composeHelper(UtString* buf,
                                      const STBranchNode* scope,
                                      bool /*includeRoot*/,
                                      bool /*hierName*/,
                                      const char* /*separator*/)
  const
{
  *buf << getOpChar(false);
  if (getNumArgs() != 0)
  {
    *buf << "(";
    for (size_t i = 0; i < mExprs.size(); ++i)
    {
      if (i != 0)
        *buf << ",";
      // use composeSized here since any constant operands of concat must be
      // sized and hex since they may be large 
      mExprs[i]->composeSized(buf, scope, eCarbonHex); 
    }
    *buf << ")";
  }
} // void NUSysFunctionCall::composeHelper

void
NUCompositeSelExpr::composeHelper(UtString* buf,
                                  const STBranchNode* scope,
                                  bool includeRoot,
                                  bool hierName,
                                  const char* separator ) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  if (mSignedResult)
    *buf << "$SIGNED(";

  mComposite->compose(buf, scope, includeRoot);

  UInt32 numIndices = mExprs.size();
  for( UInt32 i = 0; i < numIndices; ++i )
  {
    *buf << "[";
    bool constIndex = false;
    bool printRange = false;
    NUExpr* expr = mExprs[i];

    if ((i == numIndices-1) && mIsPartSelect)
    {
      constIndex = expr->isConstant();
      if (constIndex) {
        expr = NULL; // If index is constant, print part select instead of index.
      }
      // else print index as well as part select.
      printRange = true; // Print part select range.
    }

    
    if (expr != NULL) {
      mExprs[i]->compose(buf, scope, includeRoot, hierName, separator);
    }

    if (printRange) {
      sPrintRange(buf, mRange, !constIndex);
    }

    *buf << "]";
  }

  if (mSignedResult)
    *buf << ")";
}

