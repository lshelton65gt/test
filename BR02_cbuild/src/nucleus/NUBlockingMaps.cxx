// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!  \file

  This file contains common functions for dealing with structures that
  only have blocking defs. Nucleus structures that derived from
  NUUseDefNode can derive from here instead to get the blocking
  def/use/kill functionality.

*/

#include "nucleus/NUBlockingMaps.h"
#include "nucleus/NUNetSet.h"

NUBlockingMaps::NUBlockingMaps(NUNetRefFactory* netRefFactory) :
  NUUseDefNode(), mBlockingNetRefKillSet(netRefFactory),
  mBlockNetRefDefMap(netRefFactory), mNetRefFactory(netRefFactory)
{}

NUBlockingMaps::~NUBlockingMaps()
{}


void NUBlockingMaps::addDef(const NUNetRefHdl &hdl)
{
  // Invalid to call
  NU_ASSERT(0, (*hdl).getNet ());
}


void NUBlockingMaps::addUse(const NUNetRefHdl &d,
			    const NUNetRefHdl &u)
{
  // Invalid to call
  NU_ASSERT2(0, (*d).getNet (), (*u).getNet ());
}


void NUBlockingMaps::addUses(const NUNetRefHdl &d,
			     NUNetRefSet *)
{
  // Invalid to call
  NU_ASSERT(0, (*d).getNet ());
}


void NUBlockingMaps::getUses(NUNetSet *uses) const
{
  getBlockingUses(uses);
  getNonBlockingUses(uses);
}


void NUBlockingMaps::getUses(NUNetRefSet *uses) const
{
  getBlockingUses(uses);
  getNonBlockingUses(uses);
}


bool NUBlockingMaps::queryUses(const NUNetRefHdl &use_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  if (queryBlockingUses(use_net_ref, fn, factory)) {
    return true;
  }
  return queryNonBlockingUses(use_net_ref, fn, factory);
}


void NUBlockingMaps::getUses(NUNet *net, NUNetSet *uses) const
{
  NUNetSet temp;
  getBlockingDefs(&temp);
  if (temp.find(net) != temp.end()) {
    getBlockingUses(net, uses);
  }

  temp.clear();
  getNonBlockingDefs(&temp);
  if (temp.find(net) != temp.end()) {
    getNonBlockingUses(net, uses);
  }
}


void NUBlockingMaps::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses)
  const
{
  NUNetRefSet temp(uses->getFactory());
  getBlockingDefs(&temp);
  if (temp.find(net_ref, &NUNetRef::overlapsSameNet) != temp.end()) {
    getBlockingUses(net_ref, uses);
  }

  temp.clear();
  getNonBlockingDefs(&temp);
  if (temp.find(net_ref, &NUNetRef::overlapsSameNet) != temp.end()) {
    getNonBlockingUses(net_ref, uses);
  }
}


bool NUBlockingMaps::queryUses(const NUNetRefHdl &def_net_ref,
		     const NUNetRefHdl &use_net_ref,
		     NUNetRefCompareFunction fn,
		     NUNetRefFactory *factory) const
{
  if (queryBlockingUses(def_net_ref, use_net_ref, fn, factory)) {
    return true;
  }
  return queryNonBlockingUses(def_net_ref, use_net_ref, fn, factory);
}


void NUBlockingMaps::getDefs(NUNetSet *defs) const
{
  getBlockingDefs(defs);
  getNonBlockingDefs(defs);
}


void NUBlockingMaps::getDefs(NUNetRefSet *defs) const
{
  getBlockingDefs(defs);
  getNonBlockingDefs(defs);
}


bool NUBlockingMaps::queryDefs(const NUNetRefHdl &def_net_ref,
		     NUNetRefCompareFunction fn,
		     NUNetRefFactory *factory) const
{
  if (queryBlockingDefs(def_net_ref, fn, factory)) {
    return true;
  }
  return queryNonBlockingDefs(def_net_ref, fn, factory);
}


void NUBlockingMaps::replaceDef(NUNet *old_net, NUNet *new_net)
{
  // Invalid to call this
  NU_ASSERT2(0, old_net, new_net);
}

bool NUBlockingMaps::replace(NUNet * new_net, NUNet * old_net)
{
  // Invalid to call this?
  NU_ASSERT2(0, old_net, new_net);
  return false;
}

void NUBlockingMaps::getBlockingUses(NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  getBlockingUses(&ref_uses);
  for (NUNetRefSet::iterator iter = ref_uses.begin();
       iter != ref_uses.end();
       ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUBlockingMaps::getBlockingUses(NUNetRefSet *uses) const
{
  mBlockNetRefDefMap.populateMappedToNonEmpty(uses);
}


bool
NUBlockingMaps::queryBlockingUses(const NUNetRefHdl &use_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory * /* factory -- unused */)
  const
{
  return mBlockNetRefDefMap.queryMappedTo(use_net_ref, fn);
}


void NUBlockingMaps::getBlockingUses(NUNet *net, NUNetSet *uses) const
{
  NUNetRefSet ref_uses(mNetRefFactory);
  NUNetRefHdl net_ref = mNetRefFactory->createNetRef(net);
  getBlockingUses(net_ref, &ref_uses);

  for (NUNetRefSet::iterator iter = ref_uses.begin(); iter != ref_uses.end(); ++iter) {
    uses->insert((*iter)->getNet());
  }
}


void NUBlockingMaps::getBlockingUses(const NUNetRefHdl &net_ref,
				     NUNetRefSet *uses) const
{
  mBlockNetRefDefMap.populateMappedToNonEmpty(uses, net_ref,
					      &NUNetRef::overlapsSameNet);
}


bool
NUBlockingMaps::queryBlockingUses(const NUNetRefHdl &def_net_ref,
				  const NUNetRefHdl &use_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory * /* factory -- unused */)
  const
{
  return mBlockNetRefDefMap.queryMappedTo(def_net_ref, &NUNetRef::overlapsSameNet, use_net_ref, fn);
}


void NUBlockingMaps::addBlockingKill(const NUNetRefHdl &net_ref)
{
  mBlockingNetRefKillSet.insert(net_ref);
}


void NUBlockingMaps::addBlockingUse(const NUNetRefHdl &def_net_ref,
				    const NUNetRefHdl &use_net_ref)
{
  mBlockNetRefDefMap.insert(def_net_ref, use_net_ref);
}


void NUBlockingMaps::addBlockingDef(const NUNetRefHdl &net_ref)
{
  mBlockNetRefDefMap.insert(net_ref);
}


void NUBlockingMaps::getBlockingKills(NUNetSet *kills) const
{
  for (NUNetRefSet::const_iterator iter = mBlockingNetRefKillSet.begin();
       iter != mBlockingNetRefKillSet.end();
       ++iter) {
    kills->insert((*iter)->getNet());
  }
}


void NUBlockingMaps::getBlockingKills(NUNetRefSet *kills) const
{
  kills->insert(mBlockingNetRefKillSet.begin(), mBlockingNetRefKillSet.end());
}


bool
NUBlockingMaps::queryBlockingKills(const NUNetRefHdl &def_net_ref,
				   NUNetRefCompareFunction fn,
				   NUNetRefFactory * /* factory -- unused */)
  const
{
  return (mBlockingNetRefKillSet.find(def_net_ref, fn) !=
	  mBlockingNetRefKillSet.end());
}


void NUBlockingMaps::getBlockingDefs(NUNetSet *defs) const
{
  NUNetRefSet ref_defs(mNetRefFactory);
  getBlockingDefs(&ref_defs);
  for (NUNetRefSet::iterator iter = ref_defs.begin();
       iter != ref_defs.end();
       ++iter) {
    defs->insert((*iter)->getNet());
  }
}


void NUBlockingMaps::getBlockingDefs(NUNetRefSet *defs) const
{
  mBlockNetRefDefMap.populateMappedFrom(defs);
}


bool
NUBlockingMaps::queryBlockingDefs(const NUNetRefHdl &def_net_ref,
				  NUNetRefCompareFunction fn,
				  NUNetRefFactory * /* factory -- unused */)
  const
{
  return mBlockNetRefDefMap.queryMappedFrom(def_net_ref, fn);
}


