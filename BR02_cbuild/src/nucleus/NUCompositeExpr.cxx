// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUCompositeExpr.h"
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NUCost.h"
#include "nucleus/NUNetRefSet.h"
#include "nucleus/NUScope.h"
#include "nucleus/NuToNu.h"
#include "bdd/BDD.h"
#include "NUExprI.h"

/*!
  \file
  Implementation of NUCompositeExpr and friends.
*/

NUCompositeExpr::NUCompositeExpr( const NUExprVector& exprs, const SourceLocator& loc )
  : NUNaryOp( eNaConcat, exprs, loc ),
    mIsCompositeArray(false)
{}

const char*
NUCompositeExpr::typeStr() const
{
  return "NUCompositeExpr";
}


// Copied directly from NUConcatOp::resize, modulo the mRepeatCount reference
UInt32
NUCompositeExpr::determineBitSizeHelper() const
{
  UInt32 total = 0;
  for (NUExprVectorIter iter = mExprs.begin();
       iter != mExprs.end();
       ++iter) {
    total += (*iter)->determineBitSize();
  }

  return total;
}

void NUCompositeExpr::setIsCompositeArray(bool isCompositeArray)
{
  mIsCompositeArray = isCompositeArray;
}

bool NUCompositeExpr::isCompositeArray() const
{
  return mIsCompositeArray;
}

// Copied directly from NUConcatOp::resize
void
NUCompositeExpr::resizeHelper(UInt32 size)
{
  // The concatenation can be resized, but the sub-expressions all have self-determined size.
  mBitSize = size;
}

NUExpr*
NUCompositeExpr::copyHelper(CopyContext &copy_context) const
{
  NUExprVector new_vector(mExprs.size());
  int i = 0;
  for (NUExprVectorIter iter = mExprs.begin();
       iter != mExprs.end();
       ++iter, ++i) {
    new_vector[i] = (*iter)->copy(copy_context);
  }
  NUCompositeExpr *expr = new NUCompositeExpr(new_vector, mLoc);
  expr->setIsCompositeArray(mIsCompositeArray);
  return expr;
}


void
NUCompositeExpr::composeHelper(UtString* buf,
                               const STBranchNode* scope,
                               bool /* includeRoot */,
                               bool /* hierName */,
                               const char* /* separator */ ) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  NU_ASSERT(getOp() == NUOp::eNaConcat, this);
  *buf << "{";
  for (size_t i = 0; i < mExprs.size(); ++i)
  {
    if (i != 0)
      *buf << ",";
    NUConst* as_const = dynamic_cast<NUConst*>(mExprs[i]);
    if ( as_const and as_const->isDefinedByString() ){
      // since the original came from a string this can be printed as a string
      // the size will be correct
      as_const->composeNoProtected(buf, scope);
    } else {
      // use composeSized here since any constant operands of concat must be
      // sized and hex since they may be large 
      mExprs[i]->composeSized(buf, scope, eCarbonHex); 
    }
  }
  *buf << "}";
}


NUCompositeInterfaceExpr::NUCompositeInterfaceExpr( const SourceLocator& loc )
  : NUExpr( loc )
{}


NUCompositeInterfaceExpr::~NUCompositeInterfaceExpr() {}


void
NUCompositeInterfaceExpr::getUses(NUNetSet *) const
{
  // Composites don't contribute to uses or defs.
}


void
NUCompositeInterfaceExpr::getUses(NUNetRefSet *) const
{
  // Composites don't contribute to uses or defs.
}


bool
NUCompositeInterfaceExpr::queryUses(const NUNetRefHdl &,
                                    NUNetRefCompareFunction ,
                                    NUNetRefFactory *) const
{
  NU_ASSERT( false, this );
  return false;
}


bool
NUCompositeInterfaceExpr::replace(NUNet *, NUNet *)
{
  NU_ASSERT( false, this );
  return false;
}


bool
NUCompositeInterfaceExpr::replaceLeaves(NuToNuFn &)
{
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeInterfaceExpr::calcCost(NUCost* cost, NUCostContext*)
  const
{
  CompositeType type = getCompositeType();
  switch ( type ){
  case eCompositeIdentLvalue:
  case eCompositeSelLvalue:
  case eCompositeFieldLvalue:{
    NU_ASSERT(0, this);         // not yet implemented
    break;
  }
  case eCompositeIdentRvalue:
  case eCompositeSelExpr:
  case eCompositeFieldExpr:{
    cost->mTemps.addNet(determineBitSizeHelper());
    break;
  }
  }
}


void
NUCompositeInterfaceExpr::getUsesElab(STBranchNode*, NUNetElabSet *) const
{
  NU_ASSERT( false, this );
}


void
NUCompositeInterfaceExpr::getUsesElab(STBranchNode*, NUNetElabRefSet2 *) const
{
  NU_ASSERT( false, this );
}


NUExpr*
NUCompositeInterfaceExpr::copyHelper(CopyContext &) const
{
  NU_ASSERT( false, this );
  return NULL;
}


ptrdiff_t
NUCompositeInterfaceExpr::compareHelper(const NUExpr*, bool,
                                        bool, bool) const
{
  NU_ASSERT( false, this );
  return (ptrdiff_t)0;
}


BDD
NUCompositeInterfaceExpr::bdd(BDDContext* ctx, STBranchNode*) const
{
  return ctx->invalid();
}


size_t
NUCompositeInterfaceExpr::hash() const
{
  NU_ASSERT( false, this );
  return (size_t)0;
}
  

size_t
NUCompositeInterfaceExpr::shallowHash() const
{
  NU_ASSERT( false, this );
  return (size_t)0;
}
  
bool NUCompositeInterfaceExpr::hasSelects(NUExprVector* selVector) const
{
  bool hasSel = false;
  switch (getCompositeType())
  {
  case NUCompositeInterfaceExpr::eCompositeIdentRvalue:
  {
    // No indices in a net and no nodes above me
    break;
  }
  case NUCompositeInterfaceExpr::eCompositeSelExpr:
  {
    // Look for indices in the parents first.
    hasSel = getIdentExpr()->hasSelects(selVector);
    const NUCompositeSelExpr *csExpr = dynamic_cast<const NUCompositeSelExpr*>(this);
    UInt32 numLocalIndices = csExpr->getNumIndices();
    for (UInt32 i = 0; (selVector != NULL) && (i < numLocalIndices); ++i) {
      selVector->push_back(csExpr->getIndex(i));
    }
    hasSel |= (numLocalIndices > 0);
    break;
  }
  case NUCompositeInterfaceExpr::eCompositeFieldExpr:
  {
    // No indices in a field, but there may be some in parent.
    hasSel = getIdentExpr()->hasSelects(selVector);
    break;
  }
  default:
  {
    NU_ASSERT(false, this);
    break;
  }
  } // switch
  return hasSel;
}


NUCompositeSelExpr::NUCompositeSelExpr( NUCompositeInterfaceExpr *object,
                                        NUExprVector *exprs,
                                        const SourceLocator& loc ) :
  NUCompositeInterfaceExpr(loc), mComposite(object), mRange(0, 0),
  mIsDummyOffset(false), mIsPartSelect(false)
{
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
}

NUCompositeSelExpr::NUCompositeSelExpr( NUCompositeInterfaceExpr *object,
                                        NUExprVector* exprs, const ConstantRange range,
                                        bool isDummyOffset, const SourceLocator& loc ) :
  NUCompositeInterfaceExpr(loc), mComposite(object), mRange(range),
  mIsDummyOffset(isDummyOffset), mIsPartSelect(true)
{
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
}

NUCompositeSelExpr::~NUCompositeSelExpr()
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    delete mExprs[i];
  }
  delete mComposite;
}


const char*
NUCompositeSelExpr::typeStr() const
{
  return "NUCompositeSelExpr";
}

NULvalue* NUCompositeSelExpr::Lvalue(const SourceLocator &loc) const
{
  NUExprVector exprs;
  CopyContext cc(0,0);
  for (NUExprVectorIter itr = mExprs.begin(); itr != mExprs.end(); ++itr) {
    NUExpr* cpy = (*itr)->copy(cc);
    exprs.push_back(cpy);
  }
  NUCompositeInterfaceLvalue* compLval = 
    dynamic_cast<NUCompositeInterfaceLvalue*>(mComposite->Lvalue(loc));
  if (mIsPartSelect) {
    return new NUCompositeSelLvalue(compLval, &exprs, mRange, 
                                    mIsDummyOffset, loc);
  }
  return new NUCompositeSelLvalue(compLval, &exprs, loc);
}

size_t
NUCompositeSelExpr::hash() const
{
  size_t hashval = size_t(getType()) + size_t(mComposite);
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    hashval = 17 * hashval + mExprs[i]->hash();
  }
  return hashval;
}


size_t
NUCompositeSelExpr::shallowHash() const
{
  size_t hashval = size_t(getType()) + size_t(mComposite);
  const UInt32 size = getNumDims();
  for ( UInt32 i = 0; i < size; ++i ) {
    hashval += size_t(mExprs[i]);
  }
  return hashval;
}

bool NUCompositeSelExpr::shallowEqHelper (const NUExpr& otherExpr) const
{
  const NUCompositeSelExpr* other = static_cast<const NUCompositeSelExpr*>(&otherExpr);
  const UInt32 size = getNumDims();
  bool isEq = (other->getNumDims() == size);
  for ( UInt32 i = 0; isEq && (i < size); ++i ) {
    isEq = (other->mExprs[i] == mExprs[i]);
  }
  return ((other->mComposite == mComposite) and
          isEq and
          (other->getBitSize() == getBitSize()));
}

const ConstantRange*
NUCompositeSelExpr::getRange( const UInt32 index ) const
{
  return mComposite->getRange( index );
}

const ConstantRange NUCompositeSelExpr::getPartSelRange() const
{
  return mRange;
}

void NUCompositeSelExpr::setPartSelRange(const ConstantRange& psRange)
{
  mRange = psRange;
}

NUNet*
NUCompositeSelExpr::getNet() const
{
  return mComposite->getNet();
}


void
NUCompositeSelExpr::addIndexExpr( NUExpr *expr )
{
  // Cannot add any more indices to a part select of a composite.
  NU_ASSERT(!mIsPartSelect, this);
  mExprs.push_back( expr );
  // Clear cached bit size since adding an index should change it.
  clearBitSizeCache();
}


UInt32
NUCompositeSelExpr::determineBitSizeHelper() const
{
  // The base object is SIZE bits.  For every dimension that is indexed
  // we need to divide the total size by that dimension's length.
  const UInt32 baseSize = mComposite->getCompositeSize();
  UInt32 size = baseSize;
  const UInt32 myDims = mExprs.size();
  // maxDims is used to guard against bad things happening in the middle
  // of resynthesis.  This means the size might not be reported
  // correctly during resynthesis.  Specifically, when a selExpr of a
  // CNet is being referenced and the CNet has been resynthesized but
  // the selExpr has not, we'll not report the correct size.  This is
  // only known to occur during debug printing during resynthesis.
  UInt32 maxDims = mComposite->getNumDims();
  if (mComposite->getType() == NUExpr::eNUCompositeSelExpr)
  {
    NU_ASSERT(mIsPartSelect, this);
    NUCompositeSelExpr* ident = dynamic_cast<NUCompositeSelExpr*>(mComposite);
    maxDims -= ident->getNumIndices();
  }

  for ( UInt32 i = 0; i < myDims && i < maxDims; ++i )
  {
    const UInt32 dimLength = mComposite->getRange( maxDims - i - 1 )->getLength();
    size /= dimLength;
    NU_ASSERT( baseSize % dimLength == 0, this );
  }

  size *= mRange.getLength();
  return size;
}


bool
NUCompositeSelExpr::replace(NUNet* /*old_net*/, NUNet* /*new_net*/)
{
  NU_ASSERT( false, this );
  return false;
#if 0
  bool changed = false;
  if ( mComposite == old_net )
    mComposite = new_net->getCompositeNet();

  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    changed |= mExprs[i]->replace(old_net,new_net);
  }
  return changed;
#endif
}


void
NUCompositeSelExpr::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << typeStr() << "(" << this << ") ";
  printSize(screen);
  if (mIsPartSelect) {
    screen << " : ";
    mRange.print();
  }
  screen << UtIO::endl;
  mComposite->print(recurse, numspaces+2);
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->print(recurse, numspaces+2);
  }
}


bool
NUCompositeSelExpr::replaceLeaves(NuToNuFn & translator)
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // Handle the composite
  NUExpr *repl = translator(mComposite, NuToNuFn::ePre);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceExpr*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }
  changed |= mComposite->replaceLeaves( translator );
  repl = translator(mComposite, NuToNuFn::ePost);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceExpr*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }

  // Handle index expressions
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    // pre-recursion translation
    changed |= NUExpr::replaceExpr(&mExprs[i], NuToNuFn::ePre, translator);
    changed |= mExprs[i]->replaceLeaves(translator);
    changed |= NUExpr::replaceExpr(&mExprs[i], NuToNuFn::ePost, translator);
  }

  // sanity checking
  ConstantRangeVector origRanges;
  const UInt32 origDims = mComposite->getNumDims();
  for ( UInt32 i = 0; i < origDims; ++i )
    origRanges.push_back( *mComposite->getRange(i) );

  if ( changed )
  {
    // Currently we assume that any composite nets assignment aliased
    // together must have identical types, meaning identical widths and
    // depths.
    NU_ASSERT( mComposite->getNumDims() == origDims, this );
    for ( UInt32 i = 0; i < origDims; ++i )
    {
      NU_ASSERT( *mComposite->getRange(i) == origRanges[i], this );
    }
  }

  return changed;
}


NUExpr*
NUCompositeSelExpr::copyHelper( CopyContext &copy_context ) const
{
  NUExprVector newVec;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    newVec.push_back( mExprs[i]->copy( copy_context ));
  }
  NUExpr *expr = mComposite->copy( copy_context );
  NUCompositeInterfaceExpr *cExpr = dynamic_cast<NUCompositeInterfaceExpr*>( expr );
  NU_ASSERT( cExpr, this );

  NUCompositeSelExpr* selExpr = NULL;
  if (mIsPartSelect)
  {
    selExpr = new NUCompositeSelExpr(cExpr, &newVec, mRange, mIsDummyOffset, mLoc);
  }
  else
  {
    selExpr = new NUCompositeSelExpr( cExpr, &newVec, mLoc );
  }
  return selExpr;
}


void
NUCompositeSelExpr::resizeHelper(UInt32 size)
{
  RESIZE_INVARIANT (size);
  mBitSize = size;
  NUExpr* e = mComposite->resizeSubHelper( mComposite->determineBitSize( ));
  mComposite = dynamic_cast<NUCompositeInterfaceExpr*>(e);
  NU_ASSERT(mComposite, e);

  // Select expression's size is self-determined
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i] = mExprs[i]->resizeSubHelper(mExprs[i]->determineBitSize());
  }
}


UInt32
NUCompositeSelExpr::effectiveBitSizeHelper(ExprIntMap*) const
{
  return determineBitSize();
}


UInt32
NUCompositeSelExpr::getCompositeSize() const
{
  return determineBitSize();
}

UInt32 NUCompositeSelExpr::getNumArgs() const {
  return mExprs.size();
}

const NUExpr *NUCompositeSelExpr::getArgHelper(UInt32 index) const
{
  NU_ASSERT(index < mExprs.size(), this);
  return mExprs[index];
}

NUExpr *NUCompositeSelExpr::putArgHelper(UInt32 index, NUExpr * argument) 
{
  NU_ASSERT(index < mExprs.size(), this);
  NUExpr * old_argument = getArg(index);
  mExprs[index] = argument;

  return old_argument;
}

void NUCompositeSelExpr::getUses(NUNetSet *uses) const
{
  // Composites don't contribute to uses or defs, however their selects do.
  SInt32 numIndices = mExprs.size();
  for (SInt32 i = 0; i < numIndices; ++i) {
    if (mExprs[i] != NULL) {
      mExprs[i]->getUses(uses);
    }
  }
}


void NUCompositeSelExpr::getUses(NUNetRefSet *uses) const
{
  // Composites don't contribute to uses or defs, however their selects do.
  SInt32 numIndices = mExprs.size();
  for (SInt32 i = 0; i < numIndices; ++i) {
    if (mExprs[i] != NULL) {
      mExprs[i]->getUses(uses);
    }
  }
}

NUCompositeFieldExpr::NUCompositeFieldExpr( NUCompositeInterfaceExpr *object,
                                            UInt32 index,
                                            const SourceLocator& loc ) :
  NUCompositeInterfaceExpr(loc), mComposite( object ), mIndex( index )
{
}


NUCompositeFieldExpr::~NUCompositeFieldExpr()
{
  delete mComposite;
}


const char*
NUCompositeFieldExpr::typeStr() const
{
  return "NUCompositeFieldExpr";
}


NULvalue* NUCompositeFieldExpr::Lvalue(const SourceLocator &loc) const
{
  NUCompositeInterfaceLvalue* compLval = 
    dynamic_cast<NUCompositeInterfaceLvalue*>(mComposite->Lvalue(loc));
  return new NUCompositeFieldLvalue(compLval, mIndex, loc);
}

BDD
NUCompositeFieldExpr::bdd(BDDContext* ctx, STBranchNode*) const
{
  return ctx->invalid();
}


size_t
NUCompositeFieldExpr::hash() const
{
  size_t hashval = size_t(getType()) + mComposite->hash();
  hashval += 17 * mIndex;
  return hashval;
}


size_t
NUCompositeFieldExpr::shallowHash() const
{
  size_t hashval = size_t(getType()) + size_t(mComposite);
  hashval += 17 * mIndex;
  return hashval;
}

bool NUCompositeFieldExpr::shallowEqHelper (const NUExpr& otherExpr) const
{
  const NUCompositeFieldExpr* other = static_cast<const NUCompositeFieldExpr*>(&otherExpr);
  return ((other->mComposite == mComposite) and
          (other->mIndex == mIndex) and
          (other->getBitSize() == getBitSize()));
}


NUNet*
NUCompositeFieldExpr::getNet() const
{
  NUNet* net = mComposite->getNet();
  NU_ASSERT(net->isCompositeNet(), net);
  return net->getCompositeNet()->getField( mIndex );
}


UInt32
NUCompositeFieldExpr::determineBitSizeHelper() const
{
  return getNet()->getBitSize();
}


bool
NUCompositeFieldExpr::replace(NUNet* /*old_net*/, NUNet* /*new_net*/)
{
#if 0
  bool changed = mComposite->replace( old_net, new_net );
  return changed;
#endif
  NU_ASSERT( false, this );
  return false;
}


void
NUCompositeFieldExpr::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  
  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << typeStr() << "(" << this << ") ";
  printSize(screen);
  screen << UtIO::endl;

  switch( mComposite->getCompositeType())
  {
    case eCompositeSelExpr:
      dynamic_cast<NUCompositeSelExpr*>( mComposite )->print(recurse, numspaces+2);
      break;
    case eCompositeFieldExpr:
      dynamic_cast<NUCompositeFieldExpr*>( mComposite )->print(recurse, numspaces+2);
      break;
    case eCompositeIdentRvalue:
      dynamic_cast<NUCompositeIdentRvalue*>( mComposite )->print(recurse, numspaces+2);
      break;

    case eCompositeIdentLvalue:
    case eCompositeSelLvalue:
    case eCompositeFieldLvalue:
      NU_ASSERT( false, this );
      break;
  }

  for (int i = 0; i < numspaces+2; i++) {
    screen << " ";
  }
  mLoc.print();
  const NUNet *me = getNet();
  const StringAtom *fieldName = me->getName();
  screen << "Field " << mIndex << " (" << *fieldName << ")" << UtIO::endl;
}


bool
NUCompositeFieldExpr::replaceLeaves(NuToNuFn & translator )
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // Handle the composite
  NUExpr *repl = translator(mComposite, NuToNuFn::ePre);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceExpr*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }
  changed |= mComposite->replaceLeaves( translator );
  repl = translator(mComposite, NuToNuFn::ePost);
  {
    if (repl) {
      mComposite = dynamic_cast<NUCompositeInterfaceExpr*>( repl );
      NU_ASSERT( mComposite, this );
      changed = true;
    }
  }

  return changed;
}


NUExpr*
NUCompositeFieldExpr::copyHelper(CopyContext &copy_context) const
{
  NUCompositeSelExpr *cSel = static_cast<NUCompositeSelExpr*>( mComposite->copy( copy_context ));
  NUCompositeFieldExpr* expr = new NUCompositeFieldExpr( cSel, mIndex, mLoc );
  return expr;
}


void
NUCompositeFieldExpr::composeHelper(UtString* buf,
                                    const STBranchNode* scope,
                                    bool includeRoot,
                                    bool /*hierName*/,
                                    const char* /*separator*/ ) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  if (mSignedResult)
    *buf << "$SIGNED(";

  mComposite->compose(buf, scope, includeRoot );
  const NUNet *me = getField( mIndex );
  const StringAtom *fieldName = me->getName();
  *buf << "." << *fieldName;

  if (mSignedResult)
    *buf << ")";
}

void NUCompositeFieldExpr::calcCost(NUCost* cost, NUCostContext*) const
{
  cost->mTemps.addNet(determineBitSizeHelper());
}

void
NUCompositeFieldExpr::resizeHelper(UInt32 size)
{
  mBitSize = size;
}


UInt32
NUCompositeFieldExpr::effectiveBitSizeHelper(ExprIntMap*) const
{
  return determineBitSize();
}


UInt32
NUCompositeFieldExpr::getNumFields() const
{
  NU_ASSERT( false, this );
  return 1;
}


NUNet*
NUCompositeFieldExpr::getField(UInt32 index) const
{
  NUNet* net = mComposite->getNet();
  NU_ASSERT(net->isCompositeNet(), net);
  return net->getCompositeNet()->getField( index );
}


UInt32
NUCompositeFieldExpr::getNumDims() const
{
  const NUNet *me = getField( mIndex );
  if ( me->isVectorNet( ))
    return 1;
  else if ( me->isMemoryNet( ))
    return me->getMemoryNet()->getNumDims();
  else if ( me->isCompositeNet( ))
    return me->getCompositeNet()->getNumDims();
  else
    return 0; // bitNet
}


const ConstantRange *
NUCompositeFieldExpr::getRange( UInt32 index ) const
{
  const ConstantRange *retval = NULL;
  const NUNet *me = getField( mIndex );
  if ( me->isVectorNet( ))
  {
    NU_ASSERT( index == 0, this );
    retval = me->castVectorNet()->getRange();
  }
  else if ( me->isMemoryNet( ))
  {
    retval = me->getMemoryNet()->getRange( index );
  }
  else if ( me->isCompositeNet( ))
  {
    retval = me->getCompositeNet()->getRange( index );
  }
  else
    NU_ASSERT( false, this );
  return retval;
}


UInt32
NUCompositeFieldExpr::getCompositeSize() const
{
  return determineBitSize();
}


void 
NUCompositeFieldExpr::compose( UtString* buf, const STBranchNode* /*scope*/, bool /*showRoot*/ )
{
  *buf << "compose " << typeStr() << " here!\n";
}

NUCompositeIdentRvalue::NUCompositeIdentRvalue( NUCompositeNet *net, const SourceLocator &loc )
  : NUCompositeInterfaceExpr( loc ), mNet( net )
{}


NUCompositeIdentRvalue::~NUCompositeIdentRvalue()
{
}

NULvalue* NUCompositeIdentRvalue::Lvalue(const SourceLocator &loc) const
{
  return new NUCompositeIdentLvalue(mNet, loc);
}

UInt32
NUCompositeIdentRvalue::determineBitSizeHelper() const
{
  return mNet->getBitSize();
}


void
NUCompositeIdentRvalue::resizeHelper( UInt32 size )
{
  NU_ASSERT( (size >= mBitSize) || (size >= determineBitSize()), this);
  mBitSize = size;
}


void
NUCompositeIdentRvalue::composeHelper(UtString* buf, const STBranchNode* scope,
                                      bool includeRoot, bool hierName,
                                      const char* separator ) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  bool showRoot = includeRoot;
  if ( scope == NULL )
  {
    NUScope* actualScope = mNet->getScope();
    NUScope::ScopeT typeOfScope = actualScope->getScopeType();
    if ( typeOfScope == NUScope::eNamedDeclarationScope ) {
      showRoot = true;
    } else {
      showRoot = false;
    }
  }

  mNet->compose(buf, scope, showRoot, hierName, separator);
}


UInt32
NUCompositeIdentRvalue::getNumFields() const
{
  return mNet->getNumFields();
}


NUNet *
NUCompositeIdentRvalue::getField( UInt32 fieldIndex ) const
{
  return mNet->getField( fieldIndex );
}


NUCompositeInterfaceExpr*
NUCompositeIdentRvalue::getIdentExpr() const
{
  return const_cast<NUCompositeIdentRvalue*>( this );
}


UInt32
NUCompositeIdentRvalue::getNumDims() const
{
  return mNet->getNumDims();
}


const ConstantRange*
NUCompositeIdentRvalue::getRange( const UInt32 index ) const
{
  return mNet->getRange( index );
}


NUNet*
NUCompositeIdentRvalue::getNet() const
{
  return mNet;
}


UInt32
NUCompositeIdentRvalue::getCompositeSize() const
{
  return mNet->getBitSize();
}


void
NUCompositeIdentRvalue::print(bool /* recurse_arg -unused */, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtOStream& screen = UtIO::cout();
  for (int i = 0; i < numspaces; i++) {
    screen << " ";
  }
  mLoc.print();
  screen << typeStr() << "(" << this << ") ";
  printSize(screen);
  screen << UtIO::endl;
  mNet->print(false, numspaces+2);
}


NUExpr*
NUCompositeIdentRvalue::copyHelper( CopyContext &copy_context ) const
{
  NUCompositeNet *net = dynamic_cast<NUCompositeNet*>( copy_context.lookup( mNet ));
  NU_ASSERT( net, this );
  NUCompositeIdentRvalue *expr = new NUCompositeIdentRvalue( net, mLoc );
  return expr;

}


bool
NUCompositeIdentRvalue::replaceLeaves( NuToNuFn & translator )
{
  if (mIsManaged)
    return false;

  bool changed = false;

  // leaf node; only perform local replacement.
  NUNet *replacement = translator( mNet, NuToNuFn::ePrePost );
  if ( replacement ) {
    mNet = replacement->getCompositeNet();
    NU_ASSERT( mNet, this );
    changed = true;
  }
  return changed;
}


void
NUCompositeIdentRvalue::getUses(NUNetSet *uses) const
{
  uses->insert( getNet( ));
}


void
NUCompositeIdentRvalue::getUses(NUNetRefSet *uses) const
{
  uses->insert( uses->getFactory()->createNetRef( getNet( )));
}

size_t
NUCompositeIdentRvalue::shallowHash() const
{
  return (size_t(getType()) + size_t(mNet));
}
