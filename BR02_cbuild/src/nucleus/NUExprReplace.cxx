// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "nucleus/NUExprReplace.h"
#include "nucleus/NUExpr.h"

NUExprReplace::NUExprReplace() {
  mTopLevelExprSet = new NUCExprHashSet;
  mSubExprSet = new NUCExprHashSet;
}

NUExprReplace::~NUExprReplace() {
  INFO_ASSERT(mTopLevelExprSet->empty(), "clearExpr sanity 1");
  INFO_ASSERT(mSubExprSet->empty(), "clearExpr sanity 2");
  delete mTopLevelExprSet;
  delete mSubExprSet;
}

class NuRememberSubExprCallback : public NuToNuFn {
public:
  NuRememberSubExprCallback(NUCExprHashSet* subExprSet)
    : mSubExprSet(subExprSet)
  {
  }

  NUExpr* operator()(NUExpr *e, Phase phase) {
    if (isPre(phase)) {
      bool inserted = mSubExprSet->insertWithCheck(e);
      NU_ASSERT(inserted, e);
    }
    return NULL;
  }

private:
  NUCExprHashSet* mSubExprSet;
};

NUExpr* NUExprReplace::operator()(NUExpr* e, Phase phase) {
  NUExpr* repl = NULL;

  if (isPre(phase)) {
    if (mSubExprSet->find(e) == mSubExprSet->end()) {
      mTopLevelExprSet->insert(e);
      NuRememberSubExprCallback rse(mSubExprSet);
      e->replaceLeaves(rse);
    }
  }
  if (isPost(phase)) {
    NUCExprHashSet::iterator p = mTopLevelExprSet->find(e);
    if (p != mTopLevelExprSet->end()) {
      mTopLevelExprSet->erase(p);
      if (mTopLevelExprSet->empty()) {
        NU_ASSERT(mSubExprSet->empty(), e);
      }

      repl = processExpr(e);
    }
    else {
      p = mSubExprSet->find(e);
      NU_ASSERT(p != mSubExprSet->end(), e);
      mSubExprSet->erase(e);
      NU_ASSERT(!mTopLevelExprSet->empty(), e);
    }
  }

  return repl;
} // NUExpr* NUExprReplace::operator
