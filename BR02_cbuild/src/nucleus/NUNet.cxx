// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUNet.h"
#include "nucleus/NUCompositeNet.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUElabHierHelper.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUBitNet.h"
#include "nucleus/NUBitNetHierRef.h"
#include "nucleus/NUMemoryNetHierRef.h"
#include "nucleus/NUVectorNetHierRef.h"
#include "nucleus/NUCompositeNetHierRef.h"
#include "nucleus/NUVirtualNet.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUExtNet.h"
#include "nucleus/NUControlFlowNet.h"

#include "flow/FLNode.h"
#include "flow/FLNodeElab.h"
#include "flow/FLIter.h"
#include "compiler_driver/CarbonDBWrite.h"
#include "symtab/STSymbolTable.h"
#include "iodb/IODBNucleus.h"

#include "util/CbuildMsgContext.h"
#include "util/CarbonAssert.h"
#include "util/StringAtom.h"
#include "util/UtIOStream.h"
#include "util/HierStringName.h"

#include "hdl/HdlVerilogPath.h"

#include <cstdlib>

NUExpr* NUNet::sNormalizeExpr(NUExpr* expr, const ConstantRange& fromRange, const ConstantRange& toRange, SInt32 adjust, bool retNullForOutOfRange)
{
  // If the ranges are the same and there is no adjustment, return the original
  if ((fromRange == toRange) && adjust == 0)
    return expr;

  // for some reason this assert is here, but there are no users of the toRange in this code, in fact it is probably an unused argument
  //   // We assume that the toRange is of the form [width-1:0]
  // NU_ASSERT(toRange.bigEndian() && toRange.getLsb() == 0, expr);

  const SourceLocator loc = expr->getLoc();
  NUConst* k = expr->castConst();
  if (k != NULL)
  {
    // Handle a constant expression by creating a new constant
    SInt32 c_orig;
    SInt32 c;
    bool converted = k->getL(&c_orig);
    if (not converted or not fromRange.contains(c_orig))
    {
      if (retNullForOutOfRange)
        return NULL;

      c = 0;
    }
    else
      c = fromRange.offsetBounded(c_orig);

    c -= adjust;
    if (c != c_orig)
    {
      delete expr;
      expr = NUConst::create(true, c, 32, loc);
    }
  }
  else
  {
    // Dynamic bit-select.  Construct a normalize operator
    // (abs(VhExt(index,index.size) - lsb)) The VhExt operator
    // prevents resizing from altering the HDL semantics.
    //
    bool signedIndexExpr = expr->isSignedResult ();
    if (fromRange.bigEndian()) {
      if (SInt32 lsb_value = fromRange.getLsb() + adjust) {
        NUExpr* lsb = NUConst::create(true, lsb_value, 32, loc);

        if (expr->getBitSize () != 32) {
          NUExpr* msb = NUConst::create (false, 32, 32, loc);
          expr = new NUBinaryOp (signedIndexExpr ? NUOp::eBiVhExt : NUOp::eBiVhZxt, expr, msb, loc);
          expr->setSignedResult (signedIndexExpr);
        }
        expr = new NUBinaryOp(NUOp::eBiMinus, expr, lsb, loc);
        expr->resize (expr->determineBitSize ());
        expr->setSignedResult (signedIndexExpr);
      }
    } else {
      if (SInt32 lsb_value = fromRange.getLsb() - adjust) {
        NUExpr* lsb = NUConst::create(true, lsb_value, 32, loc);

        if (expr->getBitSize () != 32) {
          NUExpr* msb = NUConst::create (false, 32, 32, loc);
          expr = new NUBinaryOp (signedIndexExpr ? NUOp::eBiVhExt
                                                 : NUOp::eBiVhZxt,
                                 expr, msb, loc);
          expr->setSignedResult (signedIndexExpr);
        }
        expr = new NUBinaryOp(NUOp::eBiMinus, lsb, expr, loc);
        expr->setSignedResult (signedIndexExpr);
      } else {
        NUExpr* msb = NUConst::create (false, 32, 32, loc);
        expr = new NUBinaryOp (signedIndexExpr ? NUOp::eBiVhExt
                               : NUOp::eBiVhZxt,
                               expr, msb, loc);
        expr->setSignedResult (signedIndexExpr);
        expr = new NUUnaryOp (NUOp::eUnMinus, expr, loc);
      }
      expr->resize (expr->determineBitSize ());
    }
  }

  return expr;
} // NUExpr* sNormalizeExpr


NUExpr* sDenormalizeExpr(NUExpr* expr, ConstantRange& fromRange, ConstantRange& toRange)
{
  // If the ranges are the same, return the original
  if (fromRange == toRange)
    return expr;

  // We assume that the fromRange is of the form [width-1:0]
  NU_ASSERT(fromRange.bigEndian() && fromRange.getLsb() == 0, expr);

  const SourceLocator loc = expr->getLoc();
  NUConst* k = expr->castConst();
  if (k != NULL)
  {
    SInt32 c;
    bool converted = k->getL(&c);
    NU_ASSERT(converted, expr);
    delete expr;
    expr = NUConst::create(true, toRange.index(c, false), 32, loc);
  }
  else
  {
    expr->setSignedResult (true);

    // Dynamic bit-select.  Construct a denormalize operator (sel-lsb) if
    // little-endian, or (lsb-sel) if big-endian.
    NUExpr* lsb = NUConst::create(true, toRange.getLsb(), 32, loc);
    if (toRange.bigEndian())
      expr = new NUBinaryOp(NUOp::eBiPlus, expr, lsb, loc);
    else
      expr = new NUBinaryOp(NUOp::eBiMinus, lsb, expr, loc);
  }
  expr->resize(expr->determineBitSize ());
  return expr;
} // NUExpr* sDenormalizeExpr

NUNet::NUNet(StringAtom *name,
	     NetFlags flags,
	     NUScope* scope,
	     const SourceLocator& loc) :
  mScope(scope),
  mNameLeaf(0),
  mOrigName(NULL),
  mLoc(loc),
  mFlags(flags),
  mMySymtabIndex(scope->reserveSymtabIndex()),
  mHierRefVector(0)
{
  changeName(name);
}

NUNet::NUNet(STAliasedLeafNode *name_leaf,
	     NetFlags flags,
	     NUScope* scope,
	     const SourceLocator& loc) :
  mScope(scope),
  mNameLeaf(name_leaf),
  mOrigName(NULL),
  mLoc(loc),
  mFlags(flags),
  mMySymtabIndex(scope->reserveSymtabIndex()),
  mHierRefVector(0)
{
}

static UInt32 sNetDeleteCounter = 0;

NUNet::~NUNet()
{
  if (mHierRefVector) {
    for (NUNetVectorLoop loop = loopHierRefs(); not loop.atEnd(); ++loop) {
      (*loop)->getHierRef()->removeResolution(this);
    }
    delete mHierRefVector;
  }
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(mNameLeaf->getBOMData());
  NU_ASSERT(bom, this);
  bom->setNet(NULL);
  ++sNetDeleteCounter;
}

StringAtom* NUNet::getOriginalName() 
{
 return mOrigName;
}

void NUNet::setOriginalName(StringAtom* name) 
{
  mOrigName = name;
}

SInt32 NUNet::getSymtabIndex() const 
{ 
  return mMySymtabIndex; 
}

UInt32 NUNet::getNetDeleteCounter() {
  return sNetDeleteCounter;
}

bool NUNet::isChildOf(const NUScope *scope) const
{
  return (mScope == scope) or scope->isParentOf(mScope);
}


bool NUNet::atTopLevel() const
{
  return mScope->atTopLevel();
}

StringAtom* NUNet::getName() const
{
  return const_cast<StringAtom*>(mNameLeaf->strObject());
}

FLNode* NUNet::getContinuousDriver() const
{
  NU_ASSERT(not isMultiplyDriven(), this);
  if (isContinuouslyDriven()) {
    return *(mContinuousDrivers.begin());
  } else {
    return 0;
  }
}

bool NUNet::hasContinuousDriver(FLNode* driver) const
{
  return mContinuousDrivers.find(driver) != mContinuousDrivers.end();
}


bool NUNet::hasContinuousDriver(FLTypeT flow_type) const
{
  for (ConstDriverLoop loop = loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    const FLNode * flow = *loop;
    if (flow->getType()&flow_type) { 
      return true;
    }
  }
  return false;
}


void NUNet::addContinuousDriver(FLNode *driver)
{
  mContinuousDrivers.insert(driver);
}


void NUNet::replaceContinuousDrivers(FLNode *driver)
{
  mContinuousDrivers.clear();
  mContinuousDrivers.insert(driver);
}


void NUNet::removeContinuousDriver(FLNode *driver) {
  mContinuousDrivers.erase(driver);
}

void NUNet::trimExcessCapacity() {
  // Probably should add this capability to UtHashSet
}

void NUNet::removeContinuousDrivers()
{
  mContinuousDrivers.clear();
}


void NUNet::changeName(StringAtom *name)
{
  NUModule * module = mScope->getModule();
  STBranchNode * parent = NUElabHierHelper::findUnelabBranchForScope(module,mScope);
  STSymbolTable * aliasDB = module->getAliasDB();

  bool name_already_existed = (mNameLeaf != NULL);

  if (name_already_existed) {
    // We should only be doing this early in the compile.
    // Disallow changing names for aliased nets.
    NU_ASSERT(mNameLeaf->getAlias() == mNameLeaf, this);
    mScope->removeNetName(this);
    aliasDB->destroyLeaf(mNameLeaf);
  }

  mNameLeaf = aliasDB->createLeaf(name,parent,mMySymtabIndex);
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(mNameLeaf->getBOMData());
  bom->setNet(this);
  bom->setIndex(mMySymtabIndex);

  if (name_already_existed) {
    mScope->addNetName(this);
  }
}


STAliasedLeafNode * NUNet::getMasterNameLeaf() const
{
  STAliasedLeafNode * name_leaf = getNameLeaf();
  return name_leaf->getMaster();
}

STAliasedLeafNode * NUNet::getStorageNameLeaf() const
{
  STAliasedLeafNode * name_leaf = getNameLeaf();
  return name_leaf->getStorage();
}

NUNet * NUNet::getMaster() const
{
  STAliasedLeafNode * master_leaf = getMasterNameLeaf();
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(master_leaf->getBOMData());
  NUNet * master_net = NULL;
  if (bom->hasValidNet())
    master_net = bom->getNet();
  return master_net;
}

NUNet * NUNet::getStorage() const
{
  STAliasedLeafNode * storage_leaf = getStorageNameLeaf();
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(storage_leaf->getBOMData());
  NUNet * storage_net = NULL;
  if (bom->hasValidNet())
    storage_net = bom->getNet();
  return storage_net;
}


void NUNet::putIdent(CarbonIdent * ident) 
{
  STAliasedLeafNode * name_leaf = getNameLeaf();
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(name_leaf->getBOMData());
  bom->putIdent(ident);
}


CarbonIdent * NUNet::getIdent() const
{
  STAliasedLeafNode * name_leaf = getNameLeaf();
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(name_leaf->getBOMData());
  return bom->getIdent();
}


void NUNet::alias(NUNet * other)
{
  STAliasedLeafNode * my_name_leaf = getNameLeaf();
  STAliasedLeafNode * other_name_leaf = other->getNameLeaf();

  bool useOther = false;
  // do not choose a temp name over a user name.
  if (isTemp() and (not other->isTemp())) {
    // However, there is a special case in which we want to use a
    // temp.  We create temp nets for record ports, so use the other
    // net only if that's not the case.
    if (!isRecordPort()) {
      useOther = true;
    } else {
      // A sanity check for this temp record port.  The master net
      // (the one from which this temp was created) should not be a
      // temp.
      NUNet* master = getMaster();
      NU_ASSERT(!master->isTemp(), this);
      // It should also be a port
      NU_ASSERT(master->isPort(), this);
    }
    
  }

  if (useOther) {
    other_name_leaf->linkAlias(my_name_leaf);
  } else {
    my_name_leaf->linkAlias(other_name_leaf);
  }
  
  // mark the master as the storage element.
  my_name_leaf->setThisStorage();
}


void NUNet::transferAliases(NUNet *other)
{
  STAliasedLeafNode *my_name_leaf = getNameLeaf();
  STAliasedLeafNode *other_name_leaf = other->getNameLeaf();
  my_name_leaf->transferAliases(other_name_leaf);
}


bool NUNet::isProtectedObservable(const IODBNucleus* iodb) const
{
  // Note that we need to treat any net which has hierarchical references to it
  // as observable (meaning we do not want it to go away), hence the "hasHierRef()"
  // check here.  Otherwise, we could delete a net which has hierarchical references.
  if (hasHierRef() or isHierRefRead()) {
    return true;
  } else {
    // Check if this is a task output which is referenced out of
    // scope. If so this sort of behaves like protected observable.
    NUScope* scope = getScope();
    if ((scope->getScopeType() == NUScope::eTask) && (isOutput() || isBid()))
    {
      NUTask* task = dynamic_cast<NUTask*>(scope);
      if (task->hasHierRef())
        return true;
    }

    return isProtectedObservableNonHierref(iodb);
  }
}


bool NUNet::isProtectedObservableNonHierref(const IODBNucleus* iodb) const
{
  STAliasedLeafNode * master = getMasterNameLeaf();
  STAliasedLeafNode * alias  = master;
  do {
    NUAliasDataBOM * alias_bom = NUAliasBOM::castBOM(alias->getBOMData());
    if (alias_bom->hasValidNet())
    {
      NUNet * alias_net = alias_bom->getNet();
      if (IODBNucleus::ProtectedInterface::isProtectedObservable(iodb, alias_net)) {
        return true;
      }
    }
  } while ( (alias=alias->getAlias()) != master );
  return false;
}


bool NUNet::isProtectedMutable(const IODBNucleus* iodb) const
{
  if (isHierRefWritten()) {
    return true;
  } else {
    return isProtectedMutableNonHierref(iodb);
  }
}


bool NUNet::isProtectedMutableNonHierref(const IODBNucleus *iodb,
                                         bool checkPrimary) const
{
  STAliasedLeafNode * master = getMasterNameLeaf();
  STAliasedLeafNode * alias  = master;
  do {
    NUAliasDataBOM * alias_bom = NUAliasBOM::castBOM(alias->getBOMData());
    if (alias_bom->hasValidNet())
    {
      NUNet * alias_net = alias_bom->getNet();

      // Never consider primary inputs or bids protected; they are implicitly so.
      // IODBNucleus::resolveDirectives will issue warnings once the directives
      // are elaborated.
      if (checkPrimary and
          (alias_net->isPrimaryInput() or alias_net->isPrimaryBid())) {
        return false;
      }

      if (IODBNucleus::ProtectedInterface::isProtectedMutable(iodb, alias_net)) {
        return true;
      }
    }
  } while ( (alias=alias->getAlias()) != master );
  return false;
}


void NUNet::putIsProtectedObservableNonHierref(IODBNucleus *iodb)
{
  IODBNucleus::ProtectedInterface::putIsProtectedObservable(iodb,this);
}


void NUNet::removeIsProtectedObservableNonHierref(IODBNucleus *iodb)
{
  IODBNucleus::ProtectedInterface::removeIsProtectedObservable(iodb,this);
}


void NUNet::putIsProtectedMutableNonHierref(IODBNucleus *iodb)
{
  IODBNucleus::ProtectedInterface::putIsProtectedMutable(iodb,this);
}


void NUNet::removeIsProtectedMutableNonHierref(IODBNucleus *iodb)
{
  IODBNucleus::ProtectedInterface::removeIsProtectedMutable(iodb,this);
}


void NUNet::putIsDepositableIODB(IODBNucleus *iodb)
{
  IODBNucleus::ProtectedInterface::putIsDepositable(iodb,this);
}


STAliasedLeafNode * NUNet::lookupElabSymNode(const STBranchNode *hier) const
{
  const STBranchNode *local_hier = NUElabHierHelper::resolveElabHierForNet(this, hier);

  // Get the leaf node for this branch node
  const STSymbolTableNode *node = NULL;
  if (local_hier->hasChild(mMySymtabIndex)) {
    node = local_hier->getChild(mMySymtabIndex);
  }

  // If it exists, convert to a leaf node and return.
  const STAliasedLeafNode* leaf = NULL;
  if (node != NULL) {
    leaf = node->castLeaf();
  }

  return const_cast<STAliasedLeafNode*>(leaf);
}


NUNetElab *NUNet::lookupElab(const STBranchNode *hier, bool allowNULL) const
{
  STAliasedLeafNode * leaf = lookupElabSymNode(hier);

  bool fail = false;

  // Check if it doesn't exist and we are allowing this not to exist.
  if (leaf == NULL) {
    fail = true;
  }

  NUNetElab* net = NULL;
  if (!fail)
  {
    // get the master. It holds the NUNetElab*. The others usually have
    // the NUNet*. Note that before aliasing all leaf nodes point to
    // themselves as the master so this code works before aliasing as
    // well.
    const STAliasedLeafNode* master = leaf->getMaster();
    const STSymbolTableNode* master_node = master;

    // Get the NUNetElab* which is in the NU slot
    NUBase* base = CbuildSymTabBOM::getNucleusObj(master_node);
    net = dynamic_cast<NUNetElab*>(base);
    if (!net)
      fail = true;
  }

  if (fail)
  {
    if (allowNULL) {
      return NULL;
    }

    // We are going to have to assert out, because we don't have
    // the ability to gracefully fail here.  We don't even have
    // a MsgContext.  But at least we could print something useful
    // first.
    UtString hbuf, nbuf;
    hier->compose(&hbuf);
    compose(&nbuf, NULL);
    fprintf(stderr, "Fatal error in NUNet::lookupElab(%s) on net %s\n",
            hbuf.c_str(), nbuf.c_str());
    abort();
    NU_ASSERT(leaf, this);  // caller says that there must be node
  }

  NU_ASSERT(allowNULL || net, this);
  return net;
}


void NUNet::createElab(STBranchNode *hier, STSymbolTable *symtab)
{
  STAliasedLeafNode * net_name_node = getNameLeaf();
  STAliasedLeafNode * master = net_name_node->getMaster();

  STBranchNode * real_hier = NUElabHierHelper::findRealHier(master->getParent(),
                                                            hier,symtab);

  if (symtab->find(real_hier,master->strObject())) {
    // we've encountered another alias for this master.
    return;
  }

  STAliasedLeafNode * storage = master->getInternalStorage();
  STAliasedLeafNode * storage_symtab_node = NULL;

  NUAliasDataBOM * master_bom = NUAliasBOM::castBOM(master->getBOMData());
  NU_ASSERT(master_bom->hasValidNet(), this);
  NUNet * master_net = master_bom->getNet();
  SInt32 symtab_index = master_bom->getIndex();
  NU_ASSERT(symtab_index!=-1, this);

  STAliasedLeafNode* master_symtab_node = symtab->createLeaf(master->strObject(), 
							     real_hier, 
							     symtab_index);
  NUNetElab* master_net_elab = new NUNetElab(master_net, master_symtab_node);
  CbuildSymTabBOM::putNucleusObj(master_symtab_node, master_net_elab);
  
  if (master==storage) {
    storage_symtab_node = master_symtab_node;
    NU_ASSERT(master_net->isAllocated(), this);
  }

  // for each locally known alias, create an elaborated name.
  SInt32 nodes_in_ring = 1;
  for (STAliasedLeafNode * alias = master->getAlias();
       alias != master ;
       alias = alias->getAlias())
  {
    NUAliasDataBOM * alias_bom = NUAliasBOM::castBOM(alias->getBOMData());
    if (alias_bom->hasValidNet())
    {
      ++nodes_in_ring;
      SInt32 alias_symtab_index = alias_bom->getIndex();
      NU_ASSERT(alias_symtab_index!=-1, this);

      if (!alias_bom->hasValidNet())
      {
        NU_ASSERT(alias != storage, this);
        continue;
      }

      NUNet * alias_net = alias_bom->getNet();

      STBranchNode * alias_real_hier = NUElabHierHelper::findRealHier(alias->getParent(),
                                                                      hier,symtab);

      NU_ASSERT(not symtab->find(alias_real_hier,alias->strObject()), this);
      STAliasedLeafNode* alias_symtab_node = symtab->createLeaf(alias->strObject(), 
                                                                alias_real_hier, 
                                                                alias_symtab_index,
                                                                false);
#define STORE_NET_ON_FLATTENED_ALIASES 1
#if STORE_NET_ON_FLATTENED_ALIASES
      CbuildSymTabBOM::putNucleusObj(alias_symtab_node, alias_net);
#endif

      if (alias==storage) {
        storage_symtab_node = alias_symtab_node;
        NU_ASSERT(alias_net->isAllocated(), this);
      }

      master_symtab_node->linkAlias(alias_symtab_node);

      alias_net->putIsAliased(true);
    } // if
  }

  if (not storage_symtab_node) {
    storage_symtab_node = master_symtab_node;
  }

#define BEFORE_SYMTAB_CHANGES 0
#if BEFORE_SYMTAB_CHANGES
  if (nodes_in_ring > 1) {
    storage_symtab_node->setThisStorage();
  }
#else
  storage_symtab_node->setThisStorage();
#endif
}


FLNetIter* NUNet::makeFaninIter(FLIterFlags::TraversalT visit,
				FLIterFlags::TraversalT stop,
				FLIterFlags::NestingT nesting,
				FLIterFlags::IterationT iteration)
{
  return new FLNetIter(this,
		       visit,
		       stop,
		       nesting,
		       iteration);
}

void NUNet::composeType(UtString* buf)
  const
{
//  if (isImplicit()) {
//    *buf << "implicit ";
//  }
  if ( isCompositeNet() )
  {
    const NUCompositeNet *cnet = getCompositeNet();
    const StringAtom *typeMark = cnet->getTypeMark();
    *buf << typeMark->str() << " ";
    return;
  }
  if (isWire()) {
    *buf << "wire ";
  }
  if (isTri()) {
    *buf << "tri ";
  }
  if (isTri1()) {
    *buf << "tri1 ";
  }
  if (isSupply0()) {
    *buf << "supply0 ";
  }
  if (isWand()) {
    *buf << "wand ";
  }
  if (isTriand()) {
    *buf << "triand ";
  }
  if (isTri0()) {
    *buf << "tri0 ";
  }
  if (isSupply1()) {
    *buf << "supply1 ";
  }
  if (isWor()) {
    *buf << "wor ";
  }
  if (isTrior()) {
    *buf << "trior ";
  }
  if (isTrireg()) {
    *buf << "trireg ";
  }
  if (isReg()) {
    *buf << "reg ";
  }
  if ( isReal()) {
    *buf << "real ";
  }
  if ( is2DReg()) {
    *buf << "reg ";
  }
  if ( is2DWire()) {
    *buf << "wire ";
  }
  if ( is2DTime()) {
    *buf << "time ";            // not 2Dtime because this is used in dumpVerilog
  }
  if ( is2DRTime()) {
    *buf << "realtime ";        // not 2Drealtime because this is used in dumpVerilog
  }
  if ( is2DInteger()) {
    *buf << "integer ";         // not 2Dinteger because this is used in dumpVerilog
  }

  if ( isSigned() and showSignedInVerilogDeclaration() ) {
    *buf << "signed ";
  }
} // void NUNet::composeType

const SourceLocator* NUNet::composeUnelaboratedName(UtList<StringAtom*>* names) const
{
  if ( mLoc.isTicProtected() ) {
    names->clear();
    return &mLoc;
  }

  // reconstruct the unelaborated net name by walking back through scopes
  // to the parent module
  names->push_back(getName());
  NUScope* scope = getScope();
  while (scope->getScopeType() != NUScope::eModule)
  {
    names->push_front(scope->getName());
    scope = scope->getParentScope();
  }
  NUModule* module = dynamic_cast<NUModule*>(scope);
  names->push_front(module->getOriginalName());
  if ( module->getLoc().isTicProtected() ){
    names->clear();
    return &(module->getLoc());
  }
  return NULL; // The name is not protected. The unelaborated name is available.
}

void NUNet::composeUnelaboratedName(UtString* buf) const
{
  // reconstruct the unelaborated net name by walking back through scopes
  // to the parent module
  UtList<StringAtom*> names;
  const SourceLocator* loc = composeUnelaboratedName(&names);
  // If a protected location is returned, construct protected name instead of unelab name.
  if (loc != NULL) {
    composeProtectedNUObject(*loc, buf);
    return;
  }
      
  UtString tmp;                 // compPathAppend requires an empty buffer to start.
  HdlVerilogPath path;
  for (UtList<StringAtom*>::iterator n = names.begin(); n != names.end(); ++n)
  {
    StringAtom* name = *n;
    path.compPathAppend(&tmp, name->str());
  }
  (*buf) << tmp;
}

void NUBitNet::composeDeclaration(UtString* buf)
  const
{
  *buf << getName()->str() << ";";
}

void NUVectorNet::composeDeclaration(UtString* buf)
  const
{
  // reals do not have a vector range in their declaration
  if ( not isReal() ) {
    mDeclaredRange.format(buf);
    *buf << " ";
  }
  
  *buf << getName()->str() << ";";
}

void NUMemoryNet::composeDeclaration(UtString* buf) const
{
  // reals do not have a vector range in their declaration
  if ( not isReal() ) {
    composeDeclarePrefix(buf);
    *buf << " ";
  }
  *buf << getName()->str();
  composeDeclareSuffix(buf);
  *buf << ";";
}

void NUTempMemoryNet::composeDeclaration(UtString* buf)
  const
{
  if ( mMaster->isResynthesized() )
  {
    // reals do not have a vector range in their declaration
    if ( not mMaster->isReal() ) {
      mMaster->getDeclaredWidthRange()->format(buf);
      *buf << " ";
    }
    *buf << getName()->str();
    mMaster->getDepthRange()->format(buf);
  }
  else
  {
    // reals do not have a vector range in their declaration
    if ( not mMaster->isReal() ) {
      mMaster->getRange(0)->format(buf);
      *buf << " ";
    }
    *buf << getName()->str();
    const UInt32 numDims = mMaster->getNumDims();
    for ( UInt32 i = 1; i < numDims; ++i )
    {
      mMaster->getRange( i )->format(buf);
    }
  }

  *buf << ";";
}

//! Compose a declaration prefix a vector or memory net.  This is the part of
//! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
void NUNet::composeDeclarePrefix(UtString*) const
{
}

//! Compose a declaration suffix for a memory net.  This is the part of
//! the declaration that comes after the net name -- the depth range.
void NUNet::composeDeclareSuffix(UtString*) const
{
}

//! Compose a declaration prefix a vector or memory net.  This is the part of
//! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
void NUVectorNet::composeDeclarePrefix(UtString* buf) const
{
  mDeclaredRange.format(buf);
}

//! Compose a declaration prefix a vector or memory net.  This is the part of
//! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
void NUMemoryNet::composeDeclarePrefix(UtString* buf) const
{
  if ( mResynthesized ) {
    getDeclaredWidthRange()->format(buf);
  } else {
    // print the dimensions from mFirstUnpackedDimensionIndex downto 0 so that the slowest changing packed dimension is printed first (follows verilog v2k and systemverilog rules)
    for ( SInt32 i = mFirstUnpackedDimensionIndex-1; i >= 0 ; --i ) {
      mRanges[i].format(buf);
    }
  }
}

//! Compose a declaration suffix for a memory net.  This is the part of
//! the declaration that comes after the net name -- the depth range.
void NUMemoryNet::composeDeclareSuffix(UtString* buf) const
{
  if ( mResynthesized ) {
    getDeclaredDepthRange()->format(buf);
  } else {
    const UInt32 numDims = mRanges.size();
    // skip if we don't have unpacked dimensions
    if (mFirstUnpackedDimensionIndex != numDims) {
      // print the dimensions from numDims-1 downto firstUnpackedDimension so that the slowest changing dimension is printed first (follows verilog v2k and systemverilog rules)
      for ( SInt32 i = numDims-1; i >= (static_cast<SInt32>(mFirstUnpackedDimensionIndex)); --i ) {
        mRanges[i].format(buf);
      }
    }
  }
}

//! Compose a declaration prefix a vector or memory net.  This is the part of
//! the declaration that precedes the net name, e.g. [7:0] for a vector net. 
void NUTempMemoryNet::composeDeclarePrefix(UtString* buf) const
{
  mMaster->composeDeclarePrefix(buf);
}

//! Compose a declaration suffix for a memory net.  This is the part of
//! the declaration that comes after the net name -- the depth range.
void NUTempMemoryNet::composeDeclareSuffix(UtString* buf) const
{
  mMaster->composeDeclareSuffix(buf);
}

void NUMemoryNet::composeFlags (UtString* buf) const
{
  // Only print resynthesized flag for 3+dimension memories
  if ( getNumDims() > 2 && isResynthesized())
  {
    *buf << "resynthesized ";
  }

  NUNet::composeFlags (buf);

}

void NUVectorNet::composeFlags (UtString* buf) const
{
  NUNet::composeFlags (buf);

  if (isSignedSubrange ())
    *buf << "CarbonSInt" << mDeclaredRange.getLength () << " ";
  else if (isUnsignedSubrange ())
    *buf << "CarbonUInt" << mDeclaredRange.getLength () << " ";
}

bool NUNet::showSignedInVerilogDeclaration() const
{
  return ( not isReal() and
           not is2DTime() and
           not is2DRTime() and
           not is2DInteger() );
}
void NUNet::composeFlags(UtString* buf) const
{
  char flag_buf [15];
  sprintf(flag_buf, "(%08x) ", getFlags());
  *buf << flag_buf; // don't use a 0x prefix here even though this is a hex value because cds_diff will ignore values that look like addresses
  if (atTopLevel()) {
    *buf << "primary ";
  }
  if (isInput()) {
    *buf << "input ";
  }
  if (isOutput()) {
    *buf << "output ";
  }
  if (isBid()) {
    *buf << "bid ";
  }
  if (isTemp()) {
    *buf << "temp ";
  }
  if (isReset()) {
    *buf << "reset ";
  }
  composeType(buf);
  // special handling for "signed".  Since composeType() is used both for printing flags for print() and to generate verilog (dumpVerilog)
  // there are some cases (showSignedInVerilogDeclaration()) where it is not printed, so in those cases we handle isSigned specially here.
  if ( isSigned() and !showSignedInVerilogDeclaration() ) {
    *buf << "signed ";
  }
  
  // the following items (up to EndOfTypes) seem like they should be defined in composeType.
  // But composeType is used to generate verilog (dumpVerilog) and these extra types would be out of place in generated verilog files.
  if (isFlop()) {
    *buf << "flop ";
  }
  if (isLatch()) {
    *buf << "latch ";
  }
  // EndOfTypes

  
  if (isAliased()) {
    *buf << "aliased ";
  }
  if (isAllocated()) {
    *buf << "allocated ";
  }
  if (isDoubleBuffered()) {
    *buf << "buffered ";
  }
  if (isInteger()) {
    *buf << "integer ";
  }
  if (isTime()) {
    *buf << "time ";
  }
  if (isZ()) {
    *buf << "z ";
  }
  if (isConstZ()) {
    *buf << "const-z ";
  }
  if (isClearAtEnd()) {
    *buf << "clear-at-end ";
  }

//
// Some combination of aassignment aliasing, merge-unelab, and resoping leaves
// behind valid user nets with no drivers.  I think it's merge-unelab.  These
// nets are not referenced and will not be code-generated, but calling them
// "undriven" is confusing.
//
//  if (isUndriven()) {
//    *buf << "undriven ";
//  }
  if (isTriWritten()) {
    *buf << "tri-written ";
  }
  if (isBlockLocal()) {
    *buf << "block-local ";
  }
  if (isNonStatic()) {
    *buf << "non-static ";
  }
  if (isPullUp()) {
    *buf << "pullup ";
  }
  if (isPullDown()) {
    *buf << "pulldown ";
  }
  if (isPrimaryZ()) {
    *buf << "primary-z ";
  }
  if (isInClkPath ()) {
    *buf << "clock-path ";
  }
  if (isInDataPath ()) {
    *buf << "data-path ";
  }
  if (isForcible ()) {
    *buf << "force ";
  }
  if (isDepositable ()) {
    *buf << "deposit ";
  }
  if (isRecordPort ()) {
    *buf << "port-record ";
  }
  if (isRead ()) {
    *buf << "read ";
  }
  if (isWritten ()) {
    *buf << "written ";
  }
  if (isHierRefRead ()) {
    *buf << "hierref-read ";
  }
  if (isHierRefWritten ()) {
    *buf << "hierref-written ";
  }
  if (isDead ()) {
    *buf << "dead ";
  }
  if (isPort()) {
    *buf << "port ";
  }
  
  if (isEdgeTrigger()) {
    *buf << "edge-trigger ";
  }
  if (isInaccurate()) {
    *buf << "inaccurate ";
  }


} // void NUNet::composeFlags

void NUNet::print(bool verbose_arg, int indent_arg) const
{
  bool verbose = UtIOStreamBase::limitBoolArg(verbose_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  printNameSize();
  UtIO::cout() << " : Scope(" 
               << mScope->getPrintableName()
               << ") : ";
  UtString buf;
  composeFlags(&buf);
  UtIO::cout() << buf << UtIO::endl;

  const NUCompositeNet *cNet = getCompositeNet();
  if ( cNet )
  {
    const UInt32 numFields = cNet->getNumFields();
    for ( UInt32 i = 0; i < numFields; ++i )
    {
      cNet->getField(i)->print( verbose_arg, indent_arg+2 );
    }
  }

  if (verbose) {
    if (hasHierRef()) {
      indent(numspaces);
      UtIO::cout() << "  Cross-hierarchy references:" << UtIO::endl;
      for (NUNetVector::const_iterator iter = mHierRefVector->begin();
	   iter != mHierRefVector->end();
	   ++iter) {
	(*iter)->print(false, numspaces+4);
      }
    }

    if (isHierRef()) {
      const NUNetHierRef* href = getHierRef();
      indent(numspaces + 2);
      href->print();
      href->printPossibleResolutions(numspaces);
    }

    for (FLNodeSet::SortedLoop p = mContinuousDrivers.loopSorted();
         !p.atEnd(); ++p)
    {
      FLNode* flow = *p;
      flow->print(true, numspaces+2);
    }
  }
}


bool NUNet::isTFNet() const
{
  NUScope * scope = getScope();
  do {
    switch (scope->getScopeType()) {
    case NUScope::eModule:   return false; break;
    case NUScope::eTask:     return true;  break;
    case NUScope::eBlock: // fall-through
    case NUScope::eNamedDeclarationScope:
      scope = scope->getParentScope();
      NU_ASSERT(scope, this);
      break;
    default:
      NU_ASSERT(0, this);
      break;
    }
  } while(1);
  NU_ASSERT(0, this);
  return false;
}


bool NUNet::hasHierRef() const
{
  return mHierRefVector ? (not mHierRefVector->empty()) : false;
}


void NUNet::getAliases(NUNetList *aliases) const
{
  STAliasedLeafNode * master = getMasterNameLeaf();
  STAliasedLeafNode * alias  = master;
  do {
    NUAliasDataBOM * alias_bom = NUAliasBOM::castBOM(alias->getBOMData());
    if (alias_bom->hasValidNet())
    {
      NUNet *alias_net = alias_bom->getNet();
      aliases->push_back(alias_net);
    }
  } while ((alias = alias->getAlias()) != master);
}


void NUNet::addHierRef(NUBase *hier_ref)
{
  NU_ASSERT(not isHierRef(), this);
  NUNet* net_hier_ref = dynamic_cast<NUNet*>(hier_ref);
  NU_ASSERT(net_hier_ref != NULL, hier_ref);
  NU_ASSERT(net_hier_ref->isHierRef(), net_hier_ref);

  if (not mHierRefVector) {
    mHierRefVector = new NUNetVector;
  }

  // Just make each possible reference appear once.
  //
  // The number of hierrefs are usually small, but if this becomes a
  // performance problem (n-squared), use a set.
  NUNetVector::iterator iter = std::find(mHierRefVector->begin(), mHierRefVector->end(), net_hier_ref);
  if (iter == mHierRefVector->end()) {
    mHierRefVector->push_back(net_hier_ref);
  }
}


void NUNet::removeHierRef(NUBase *hier_ref)
{
  NU_ASSERT(not isHierRef(), this);
  NUNet* net_hier_ref = dynamic_cast<NUNet*>(hier_ref);
  NU_ASSERT(net_hier_ref != NULL, hier_ref);
  NU_ASSERT(net_hier_ref->isHierRef(), net_hier_ref);
  
  if (mHierRefVector) {
    NUNetVector::iterator iter = std::find(mHierRefVector->begin(), mHierRefVector->end(), net_hier_ref);
    if (iter != mHierRefVector->end()) {
      mHierRefVector->erase(iter);
    }
  }
}


NUNetElab::NUNetElab(NUNet *net, STAliasedLeafNode *myName):
  NUElabBase(myName), mNet(net)
{
}


NUNetElab::~NUNetElab()
{
  ++sNetDeleteCounter;
}


const char* NUNetElab::typeStr() const
{
  return "NUNetElab";
}


STAliasedLeafNode* NUNetElab::getSymNode() const
{
  return mSymNode->castLeaf();
}

STBranchNode* NUNetElab::getHier() const 
{ 
  return mSymNode->getParent();
}

STAliasedLeafNode * NUNetElab::getStorageSymNode() const
{
  STAliasedLeafNode * symnode_leaf = mSymNode->castLeaf();
  STAliasedLeafNode * storage_leaf = symnode_leaf->getStorage();
  NU_ASSERT(storage_leaf, this);
  return storage_leaf;
}

void NUNetElab::putIsComboRun(bool isComboRun)
{
  CbuildSymTabBOM::markComboRun(mSymNode->castLeaf(), isComboRun);
}

bool NUNetElab::isComboRun() const
{
  return CbuildSymTabBOM::isComboRun(mSymNode->castLeaf());
}

NUNet * NUNetElab::getStorageNet() const
{
  const STAliasedLeafNode * storage_leaf = getStorageSymNode();
  return NUNet::find(storage_leaf);
}

STBranchNode * NUNetElab::getStorageHier() const
{
  STAliasedLeafNode * storage_leaf = getStorageSymNode();
  STBranchNode* hier = storage_leaf->getParent();
  return const_cast<STBranchNode*>(NUModule::getParentModuleBranch(hier));
}


bool sPrimaryPortHelper(const NUNet * unelab_master, NUNet::QueryFunction fn) 
{
  if (unelab_master->isRecordPort()) {
    // VHDL record ports are represented with two nets.

    // Unelaboratedly, the "master" corresponds to a hierarchical name
    // (the hierarchy is the record path). The "storage" corresponds
    // to a $RECORD_ temp name which exists at the port. These two
    // nets are aliased together.

    // We need to perform this test on the unelaborated representation
    // because we have no guarantee that the elaborated storage
    // element is still at the top level.

    // This is the ONLY situation where the "master" is not synonymous
    // with the primary port in an alias ring.

    NUNet * unelab_master_storage = unelab_master->getStorage();
    return (unelab_master_storage->*fn)();
  } else {
    // Otherwise, assume that any primary port in our ring will be our
    // unelaborated master.
    return (unelab_master->*fn)();
  }
}


//! Realise NUElabBase::getSourceLocation virtual method
/*virtual*/ const SourceLocator NUNetElab::getSourceLocation () const
{
  return mNet->getLoc ();
}


//! Realise NUElabBase::findUnelaborated virtual method
/*virtual*/ bool NUNetElab::findUnelaborated (STSymbolTable *unelabStab, 
  STSymbolTableNode **unelab) const
{
  NUNet *unelabNet = getNet ();
  // find the unelaborated symbol for the enclosing scope
  STBranchNode *elabHier = getHier ();
  NUBase *base;
  NUElabBase *elabBase;
  STSymbolTableNode *unelabParent;
  if ((base = CbuildSymTabBOM::getNucleusObj (elabHier)) == NULL) {
    // no object
    NU_ASSERT (base != NULL, this);
    return false;
  } else if ((elabBase = dynamic_cast <NUElabBase *> (base)) == NULL) {
    // not an elaboration object
    NU_ASSERT (elabBase != NULL, this);
    return false;
  } else if (!elabBase->findUnelaborated (unelabStab, &unelabParent)) {
    // Did not find the unelaborated symbol for the parent
    return false;
  } else {
    // unelabParent contains the unelaborated parent for the unelaborated net
  }
  // construct a hierarchical name for the unelaborated symbol
  HierStringName tmp (unelabNet->getName ());
  tmp.putParent (unelabParent);
  // search for that symbol in the unelaborated stab
  *unelab = unelabStab->safeLookup (&tmp);
  return (*unelab != NULL);
}

bool NUNetElab::isPrimaryPort() const
{
  return sPrimaryPortHelper(getNet(),&NUNet::isPrimaryPort);
}
bool NUNetElab::isPrimaryInput() const
{
  return sPrimaryPortHelper(getNet(),&NUNet::isPrimaryInput);
}
bool NUNetElab::isPrimaryBid() const
{
  return sPrimaryPortHelper(getNet(),&NUNet::isPrimaryBid);
}
bool NUNetElab::isPrimaryOutput() const
{
  return sPrimaryPortHelper(getNet(),&NUNet::isPrimaryOutput);
}


bool NUNetElab::atTopLevel() const
{
  // ************************************************************
  // ************************************************************
  // NOTE: This might not be correct if the net elab happens to exist
  // within an NUNamedDeclarationScope. This can happen for VHDL
  // records and Verilog block-declared nets. What does the caller
  // want if a net elab contains a master at a non-top level of
  // hierarchy but some alias is at the top?
  // ************************************************************
  // ************************************************************

  return (mSymNode->getParent() == NULL);
}


bool NUNet::isOutputConnectionAlias() const
{
  if (not isContinuouslyDriven()) {
    return false;
  }

  if (isMultiplyDriven()) {
    return false;
  }

  FLNode *driver = getContinuousDriver();
  NUUseDefNode *node = driver->getUseDefNode();
  NUModuleInstance *inst = dynamic_cast<NUModuleInstance*>(node);
  if (inst != 0) {
    node = driver->getSingleNested()->getUseDefNode();
    return (dynamic_cast<NUPortConnectionOutput*>(node) != 0);
  } else {
    return false;
  }
}


// This is debug scaffolding written to catch when someone mutates
// flags on a particular net.  All the routines which mutate mFlags
// should call setFlags
#ifdef CDB
static NUNet* sWatchNet = NULL;
#endif

void NUNet::setFlags(NetFlags flags)
{
  if (flags != mFlags)
  {
#ifdef CDB
    if (this == sWatchNet)
      fprintf(stdout, "Found net\n");
#endif
    mFlags = flags;
  }
}
    

void NUNet::putIsBlockLocal(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eBlockLocalNet));
  else
    setFlags(NetFlags(mFlags & ~eBlockLocalNet));
}


void NUNet::putIsNonStatic(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eNonStaticNet));
  else
    setFlags(NetFlags(mFlags & ~eNonStaticNet));
}


void NUNet::putIsFlop(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eFlopNet));
  else
    setFlags(NetFlags(mFlags & ~eFlopNet));
}


void NUNet::putIsAliased(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eAliasedNet));
  else
    setFlags(NetFlags(mFlags & ~eAliasedNet));
}


void NUNet::putIsLatch(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eLatch));
  else
    setFlags(NetFlags(mFlags & ~eLatch));
}


void NUNet::putIsPullUp(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | ePullUp));
  else
    setFlags(NetFlags(mFlags & ~ePullUp));
}


void NUNet::putIsPullDown(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | ePullDown));
  else
    setFlags(NetFlags(mFlags & ~ePullDown));
}


void NUNet::putIsAllocated(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eAllocatedNet));
  else
    setFlags(NetFlags(mFlags & ~eAllocatedNet));
}


void NUNet::putIsInaccurate(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eInaccurateNet));
  else
    setFlags(NetFlags(mFlags & ~eInaccurateNet));
}


void NUNet::putIsDead(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eDeadNet));
  else
    setFlags(NetFlags(mFlags & ~eDeadNet));
}

void NUNet::putIsRecordPort(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eRecordPort));
  else
    setFlags(NetFlags(mFlags & ~eRecordPort));
}

void NUNet::putIsTemp(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eTempNet));
  else
    setFlags(NetFlags(mFlags & ~eTempNet));
}

void NUNet::putIsDoubleBuffered(bool flag)
{
  NU_ASSERT(!flag, this);
}

void NUMemoryNet::putIsDoubleBuffered(bool flag) {
  mIsDoubleBuffered = flag;
}

bool NUNet::isDoubleBuffered() const {
  return false;
}

bool NUMemoryNet::isDoubleBuffered() const {
  return mIsDoubleBuffered;
}

void NUNet::putIsTriWritten(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eTriWritten));
  else
    setFlags(NetFlags(mFlags & ~eTriWritten));
}


void NUNet::putIsConstZ(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eConstZ));
  else
    setFlags(NetFlags(mFlags & ~eConstZ));
}


void NUNet::putIsPrimaryZ(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | ePrimaryZ));
  else
    setFlags(NetFlags(mFlags & ~ePrimaryZ));
}


void NUNet::putIsInput(bool flag)
{
  setFlags(NetFlags(mFlags & ~ePortMask));
  if (flag)
    setFlags(NetFlags(mFlags | eInputNet));
  else
    setFlags(NetFlags(mFlags & ~eInputNet));
}


void NUNet::putIsOutput(bool flag)
{
  setFlags(NetFlags(mFlags & ~ePortMask));
  if (flag)
    setFlags(NetFlags(mFlags | eOutputNet));
  else
    setFlags(NetFlags(mFlags & ~eOutputNet));
}


void NUNet::putIsBid(bool flag)
{
  setFlags(NetFlags(mFlags & ~ePortMask));
  if (flag)
    setFlags(NetFlags(mFlags | eBidNet));
  else
    setFlags(NetFlags(mFlags & ~eBidNet));
}


void NUNet::putIsForcible(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eForcibleNet));
  else
    setFlags(NetFlags(mFlags & ~eForcibleNet));
}


void NUNet::putIsDepositable(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eDepositNet));
  else
    setFlags(NetFlags(mFlags & ~eDepositNet));
}

void NUNet::putIsTrireg(bool flag)
{
  if (flag)
    setFlags(NetRedeclare(mFlags, eDMTriregNet));
  else
    setFlags(NetRedeclare(mFlags, 0));
}


void NUNet::putIsTri1(bool flag)
{
  if (flag)
    setFlags(NetRedeclare(mFlags, eDMTri1Net));
  else
    setFlags(NetRedeclare(mFlags, 0));
}


void NUNet::putIsTri0(bool flag)
{
  if (flag)
    setFlags(NetRedeclare(mFlags, eDMTri0Net));
  else
    setFlags(NetRedeclare(mFlags, 0));
}


void NUNet::putIsWand(bool flag)
{
  if (flag)
    setFlags(NetRedeclare(mFlags, eDMWandNet));
  else
    setFlags(NetRedeclare(mFlags, 0));
}


void NUNet::putIsWor(bool flag)
{
  if (flag)
    setFlags(NetRedeclare(mFlags, eDMWorNet));
  else
    setFlags(NetRedeclare(mFlags, 0));
}


void NUNet::putIsTriand(bool flag)
{
  if (flag)
    setFlags(NetRedeclare(mFlags, eDMTriandNet));
  else
    setFlags(NetRedeclare(mFlags, 0));
}


void NUNet::putIsTrior(bool flag)
{
  if (flag)
    setFlags(NetRedeclare(mFlags, eDMTriorNet));
  else
    setFlags(NetRedeclare(mFlags, 0));
}


void NUNet::putInClkPath(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eInClkPath));
  else
    setFlags(NetFlags(mFlags & ~eInClkPath));
}


void NUNet::putInDataPath(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eInDataPath));
  else
    setFlags(NetFlags(mFlags & ~eInDataPath));
}


void NUNet::putIsEdgeTrigger(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eEdgeTrigger));
  else
    setFlags(NetFlags(mFlags & ~eEdgeTrigger));
}


void NUNet::putIsRead(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eReadNet));
  else
    setFlags(NetFlags(mFlags & ~eReadNet));
}


void NUNet::putIsWritten(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eWrittenNet));
  else
    setFlags(NetFlags(mFlags & ~eWrittenNet));
}


void NUNet::putIsClearAtEnd(bool flag)
{
  if (flag)
    setFlags(NetFlags(mFlags | eClearAtEnd));
  else
    setFlags(NetFlags(mFlags & ~eClearAtEnd));
}


bool NUNet::isHierRefRead() const
{
  if (not hasHierRef()) {
    return false;
  }

  for (NUNetVectorCLoop loop = loopHierRefs(); not loop.atEnd(); ++loop) {
    const NUNet *hier_ref = *loop;
    if (hier_ref->isRead()) {
      return true;
    }
  }

  return false;
}


bool NUNet::isHierRefWritten() const
{
  if (not hasHierRef()) {
    return false;
  }

  for (NUNetVectorCLoop loop = loopHierRefs(); not loop.atEnd(); ++loop) {
    const NUNet *hier_ref = *loop;
    if (hier_ref->isWritten() || hier_ref->isTriWritten()) {
      return true;
    }
  }

  return false;
}

void NUNet::aliasSetFlags(const NUNet *alias)
{
  if (alias->isTriWritten()) {
    putIsTriWritten(true);
  }
  if (alias->isPrimaryZ()) {
    putIsPrimaryZ(true);
  }
  if (alias->isEdgeTrigger()) {
    putIsEdgeTrigger(true);
  }
}


void NUNet::assignAliasSetFlags(const NUNet *alias)
{
  aliasSetFlags(alias);

  if (alias->isUndriven()) {
    // do this for assignment aliasing, but not hierarchial aliasing,
    // which takes care of marking primary-z itself.
    if (isPrimaryBid() or isPrimaryOutput()) {
      putIsPrimaryZ(true);
    }
  }

  if (alias->isTrireg()) {
    putIsTrireg(true);
  }
  if (alias->isTri1()) {
    putIsTri1(true);
  }
  if (alias->isTri0()) {
    putIsTri0(true);
  }
  if (alias->isPullUp()) {
    putIsPullUp(true);
  }
  if (alias->isPullDown()) {
    putIsPullDown(true);
  }
  if (alias->isWand()) {
    putIsWand(true);
  }
  if (alias->isWor()) {
    putIsWor(true);
  }
  if (alias->isTriand()) {
    putIsTriand(true);
  }
  if (alias->isTrior()) {
    putIsTrior(true);
  }
}


FLNodeElab* NUNetElab::getContinuousDriver() const
{
  NU_ASSERT(not isMultiplyDriven(), this);
  if (isContinuouslyDriven()) {
    return *(mContinuousDrivers.begin());
  } else {
    return 0;
  }
}

FLNodeElab* NUNetElab::getFirstContinuousDriverOfType(FLTypeT flow_type) const
{
  for (ConstDriverLoop loop = loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    const FLNodeElab * flow = *loop;
    if (flow->getType()&flow_type) { 
      return const_cast<FLNodeElab*>(flow);
    }
  }
  return NULL;
}

bool NUNetElab::hasContinuousDriver(FLNodeElab* driver) const
{
  return mContinuousDrivers.find(driver) != mContinuousDrivers.end();
}


bool NUNetElab::hasContinuousDriver(FLTypeT flow_type) const
{
  for (ConstDriverLoop loop = loopContinuousDrivers();
       not loop.atEnd();
       ++loop) {
    const FLNodeElab * flow = *loop;
    if (flow->getType()&flow_type) { 
      return true;
    }
  }
  return false;
}


void NUNetElab::addContinuousDriver(FLNodeElab *driver)
{
  mContinuousDrivers.insert(driver);
}


void NUNetElab::replaceContinuousDrivers(FLNodeElab *driver)
{
  mContinuousDrivers.clear();
  mContinuousDrivers.insert(driver);
}


void NUNetElab::removeContinuousDriver(FLNodeElab *driver)
{
  mContinuousDrivers.erase(driver);
}

void NUNetElab::trimExcessCapacity() {
  // Probably should add this capability to UtHashSet
}

struct MatchFlowElab {
  MatchFlowElab(const FLNodeElabSet& flowSet): mFlowSet(flowSet) {}
  bool operator()(const FLNodeElab* flow) const {
    return mFlowSet.find(const_cast<FLNodeElab*>(flow)) != mFlowSet.end();
  }
  const FLNodeElabSet& mFlowSet;
};

UInt32 NUNetElab::removeContinuousDriverSet(const FLNodeElabSet& flowSet) {
  // This routine has a much more straightforward implementation, but it's
  // done this way for performance.  There are typically a large number
  // of flows in flowSet, and a relatively small number of flows in
  // mContinuousDrivers.  So it's better to loop over the small set
  // and create a new small set, than it is to loop over the large set
  // and erase matching entries in the small set.
  //
  // This should arguably be incorporated into the implementation of
  // set_subtract in inc/util/SetOps.h, whenever 's1' is much larger
  // than 's2'.

  UInt32 count = 0;
  FLNodeElabSet keep;
  FLNodeElabSet::const_iterator not_found = flowSet.end();
  for (FLNodeElabSet::iterator p = mContinuousDrivers.begin(),
         e = mContinuousDrivers.end();
       p != e; ++p)
  {
    FLNodeElab* flow = *p;
    if (flowSet.find(flow) == not_found) {
      keep.insert(flow);
    }
    else {
      ++count;
    }
  }
  mContinuousDrivers = keep;
  return count;
}

void NUNetElab::removeContinuousDrivers() {
  mContinuousDrivers.clear();
}

void NUNetElab::setIsMaster()
{
  getSymNode()->setThisMaster();
}


void NUNetElab::setAllocated()
{
  /*
  STAliasedLeafNode *this_node =
    dynamic_cast<STAliasedLeafNode*>(mHier->getChild(mNet->getSymtabIndex()));
  this_node->setThisStorage();
  */
  STAliasedLeafNode* symNode = getSymNode();
  if (symNode->getInternalStorage() != mSymNode)
    symNode->setThisStorage();
}


void NUNetElab::alias(NUNetElab *master, bool fixStorage)
{
  // We now store the NUNet* in the symbol table. This allows us to
  // get all the nets in the alias ring. See getAliasNets
  CbuildSymTabBOM::putNucleusObj(mSymNode, mNet);

  // Let my unelaborated counterpart know it is aliased
  mNet->putIsAliased(true);

  // Propagate flags to master.
  master->getNet()->aliasSetFlags(mNet);

  // Put myself in the alias ring of master.
  STAliasedLeafNode *master_node = master->getSymNode();
  NU_ASSERT(master_node != 0, master);

  NUNet             * storage_net = getStorageNet();
  STAliasedLeafNode * storage     = getStorageSymNode();
    
  master_node->linkAlias(storage);

  // Allocation has already been marked on the unelaborated net.
  if (fixStorage && storage_net->isAllocated()) {
    // avoid n^2 set
    if (storage->getInternalStorage() != storage) {
      storage->setThisStorage();
    }
  }
  mSymNode = NULL;
} // void NUNetElab::alias

void
NUNetElab::getAliasNets(const STAliasedLeafNode* leaf,
                        NUNetVector* netVector,
                        bool includeUnallocated,
                        const STBranchNode* parentFilter)
{
  // Loop through the alias ring adding nets
  const STAliasedLeafNode* next_node = leaf;
  do
  {
    // Add this net to the vector.  The parentFilter, if it exists, is
    // the containing module, not the immediate parent scope.  The
    // immediate parent scope will not be the containing module in the
    // case of VHDL record elements.
    if ((includeUnallocated || next_node->isAllocated()) &&
        ((parentFilter == NULL) || 
         (parentFilter == NUModule::getParentModuleBranch(next_node->getParent())))) {
      NUBase* base = CbuildSymTabBOM::getNucleusObj(next_node);
      NUNet* nextNet;
      if (next_node->getMaster() == next_node)
      {
        NUNetElab* nextNetElab = dynamic_cast<NUNetElab*>(base);
        nextNet = nextNetElab->getNet();
      }
      else
        nextNet = dynamic_cast<NUNet*>(base);
      if (nextNet != NULL)
        netVector->push_back(nextNet);
    }
    
    // Get the next alias
    next_node = next_node->getAlias();
  } while (next_node != leaf);
}

void NUNetElab::getAliasNets(NUNetVector* netVector,
                             bool includeUnallocated,
                             const STBranchNode* parentFilter) const
{
  STAliasedLeafNode* leaf = getSymNode();
  getAliasNets(leaf, netVector, includeUnallocated, parentFilter);
}

FLNetElabIter* NUNetElab::makeFaninIter(FLIterFlags::TraversalT visit,
					FLIterFlags::TraversalT stop,
					FLIterFlags::NestingT nesting,
					FLIterFlags::IterationT iteration)
{
  return new FLNetElabIter(this,
			   visit,
			   stop,
			   nesting,
			   iteration);
}


bool NUNetElab::queryAliases(NUNet::QueryFunction fn) const
{
  NUNetVector alias_vector;
  getAliasNets(&alias_vector, true);
  for (NUNetVectorIter alias_vector_iter = alias_vector.begin();
       alias_vector_iter != alias_vector.end();
       ++alias_vector_iter) {
    NUNet *alias_net = *alias_vector_iter;
    if ((alias_net->*fn)()) {
      return true;
      break;
    }
  }
  return false;
}


void NUNetElab::printNameSize() const
{
  // Should never be called; NUNetElab overrides its print
  NU_ASSERT(0, this);
}


void NUNetElab::print(bool verbose_arg, int indent_arg) const
{
  bool verbose = UtIOStreamBase::limitBoolArg(verbose_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  UtIO::cout() << "NUNetElab(" << this << ") : NUNet(" << mNet
       << ")  BranchNode(" << getHier() << ")";
  if (atTopLevel()) {
    UtIO::cout() << " : primary";
  }
  UtIO::cout() << UtIO::endl;
  mNet->print(false, numspaces+2);
  if (verbose) {
    SInt32 driver_num = 0;
    for (FLNodeElabSet::const_iterator iter = mContinuousDrivers.begin();
         iter != mContinuousDrivers.end();
         ++iter) {
      FLNodeElab* driver = *iter;
      if ( dynamic_cast<FLNodeBoundElab*>(driver) ) {
        UtIO::cout() << "boundary(" << ++driver_num << "):" << UtIO::endl;
      } else {
        UtIO::cout() << "continuous driver(" << ++driver_num << "):" << UtIO::endl;
      }
      driver->print(false, numspaces+2);
    }
  }
}

#ifdef CDB
//  These functions are placed here (instead of CarbonDebug.cxx) so that
//  they can be compiled without causing problems with the inclusion of
//  CarbonDebug in executables that know nothing of NUNetElabs

//! print the items in the NUNetElabSet.
void cdbPrintNUNetElabSet(NUNetElabSet * set){
  SInt32 count = 0;
  for (NUNetElabSet::iterator iter = set->begin(); iter != set->end(); ++iter){
    NUNetElab *net_elab = *iter;
    UtIO::cout() << "Item: " << ++count << UtIO::endl;
    net_elab->print(true, 0);
  }
}
//! print the items in the NUNetSet.
void cdbPrintNUNetSet(NUNetSet * net_set){
  SInt32 count = 0;
  for (NUNetSet::iterator iter = net_set->begin(); iter != net_set->end(); ++iter) {
    NUNet *net = *iter;
    UtIO::cout() << "Item: " << ++count << UtIO::endl;
    net->print(true, 0);
  }
}
#endif

int NUNet::compare(const NUNet* n1, const NUNet* n2)
{
  int cmp = 0;
  if (n1 != n2)
  {
    cmp = strcmp(n1->getName()->str(), n2->getName()->str());

    if (cmp == 0)
    {
      cmp = SourceLocator::compare(n1->getLoc(), n2->getLoc());
      if (cmp == 0) {
        cmp = HierName::compare(n1->getNameLeaf(), n2->getNameLeaf() );
      }
      if ( cmp == 0 ){
        cmp = NUModule::compare(n1->mScope->getModule(),n2->mScope->getModule());
      }
      NU_ASSERT(cmp != 0, n1);
    }
  } // if
  return cmp;
} // int NUNet::compare

void NUNet::compose(UtString* buf,
                    const STBranchNode* scope,
                    bool includeRoot,
                    bool hierName,
                    const char* separator,
                    bool /* escapeIfMultiLevel */ ) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  if ( scope == NULL )
  {
    const NUNetHierRef* href = getHierRef();
    if (href == NULL) {
      mNameLeaf->compose(buf, includeRoot, hierName, separator);
    }
    else {
      HdlVerilogPath vpath;
      href->getName(buf, vpath);
    }
  }
  else
  {
    // user requested a specific scope, find the elaborated net and
    // compse the name for it.
    STBranchNode* scopeToUse = const_cast<STBranchNode*>(scope);
    NUNetElab * netElab = lookupElab(scopeToUse, true );
    if ( netElab == NULL )
    {
      while ( netElab == NULL )
      {
        scopeToUse = scopeToUse->getParent();
        if ( scopeToUse == NULL ) break;
        netElab = lookupElab(scopeToUse, true );
      }
      NU_ASSERT ( netElab != NULL, this );
    }
    netElab->compose(buf, NULL, true, hierName, separator);
  }
}

void MsgContext::composeLocation(const NUNet* net, UtString* location)
{
  if (net == NULL)
    *location << "(null NUNet)";
  else
  {
    const SourceLocator& loc = net->getLoc();
    UtString buf;
    // net scope has no name in test/vhdl/beacon2, tri2.vhdl
    // {/tools/beacon/VHDL-4.1/testcases/misc/tri2/tri2.vhdl:42}BitNet(0x402c36e8) : $rescope_tmp1;1 : Scope(__anonymous__) : temp allocated block-local non-static read written 
    //
    StringAtom* scopeName = net->getScope()->getName();
    if (scopeName != NULL) {
      // this is needed for test/fold/ifswap (only if net is not a netElab)
      buf << scopeName->str() << ".";
    }
    net->compose(&buf, NULL);
    UtString source;
    loc.compose(&source);
    *location << source << ' ' << buf;
  }
}

void MsgContext::composeLocation(STAliasedLeafNode const* node, UtString* location)
{
  NUNet* net = NUNet::find(node);
  if (net != NULL)
  {
    const SourceLocator& loc = net->getLoc();
    UtString source;
    loc.compose(&source);
    *location << source << ' ';
  }
  UtString buf;
  node->compose(&buf);
  *location << buf;
}

void MsgContext::composeLocation(const NUNetElab* net, UtString* location)
{
  if (net == NULL)
    *location << "(null NUNetElab)";
  else
  {
    const SourceLocator& loc = net->getNet()->getLoc();
    UtString buf;
    net->compose(&buf, NULL);
    UtString source;
    loc.compose(&source);
    *location << source << ' ' << buf;
  }
}


void MsgContext::composeLocation(const NUUseDefNode *node, UtString* location)
{
  if (node == NULL)
    *location << "(null NUUseDefNode)";
  else
  {
    const SourceLocator& loc = node->getLoc();
    UtString buf;
    *location << loc.getFile() << ":" << loc.getLine() << ' ' << buf;
  }
}


NUBitNet::NUBitNet(StringAtom *name,
		   NetFlags flags,
		   NUScope *scope,
		   const SourceLocator& loc) :
  NUNet(name, flags, scope, loc)
#if NUBITNETREF_STORED_IN_NUBITNET
  , mNetRef(this, 0, NULL)
#endif
{
}


NUBitNet::~NUBitNet()
{
}


const char* NUBitNet::typeStr() const
{
  return "NUBitNet";
}



void NUBitNet::printNameSize() const
{
  UtIO::cout() << "BitNet(" << this << ") : " << getName()->str();
}


NUVectorNet::NUVectorNet(StringAtom *name,
			 const ConstantRange& range,
			 NetFlags flags,
                         VectorNetFlags vflags,
			 NUScope* scope,
			 const SourceLocator& loc) :
  NUNet(name, flags, scope, loc),
  mDeclaredRange(range),
  mOperationalRange(range.getLength() - 1, 0),
  mVectorNetFlags (vflags)
{
}

NUVectorNet::NUVectorNet(StringAtom *name,
                         ConstantRange* range,
                         NetFlags flags,
                         VectorNetFlags vflags,
                         NUScope* scope,
                         const SourceLocator& loc) :
  NUNet(name, flags, scope, loc),
  mDeclaredRange(*range),
  mOperationalRange(range->getLength() - 1, 0),
  mVectorNetFlags (vflags)
{
  delete range;
}

NUVectorNet::~NUVectorNet()
{
}


UInt32 NUVectorNet::getBitSize() const
{
  return mDeclaredRange.getLength();
}

UInt32 NUVectorNet::getNetRefWidth() const
{
  return mDeclaredRange.getLength();
}

VectorNetFlags NUVectorNet::getVectorNetFlags (void) const
{
  return mVectorNetFlags;
}

void NUVectorNet::putVectorNetFlags (VectorNetFlags f)
{
  mVectorNetFlags = f;
}

bool NUVectorNet::isSignedSubrange () const
{
  bool ssubrange = (mVectorNetFlags & eSignedSubrange) != 0;
  bool usubrange = (mVectorNetFlags & eUnsignedSubrange) != 0;
  NU_ASSERT ( not(ssubrange && usubrange), this );
  return ssubrange;
}

bool NUVectorNet::isUnsignedSubrange () const
{
  bool ssubrange = (mVectorNetFlags & eSignedSubrange) != 0;
  bool usubrange = (mVectorNetFlags & eUnsignedSubrange) != 0;
  NU_ASSERT ( not(ssubrange && usubrange), this );
  return usubrange;
}

bool NUVectorNet::isIntegerSubrange () const
{
  return (mVectorNetFlags & (eSignedSubrange | eUnsignedSubrange)) != 0;
}

VectorNetFlags NUMemoryNet::getVectorNetFlags (void) const
{
  return mVectorNetFlags;
}

void NUMemoryNet::putVectorNetFlags (VectorNetFlags f)
{
  mVectorNetFlags = f;
}

bool NUMemoryNet::isSignedSubrange () const
{
  bool ssubrange = (mVectorNetFlags & eSignedSubrange) != 0;
  bool usubrange = (mVectorNetFlags & eUnsignedSubrange) != 0;
  NU_ASSERT ( not(ssubrange && usubrange), this );
  return ssubrange;
}

bool NUMemoryNet::isUnsignedSubrange () const
{
  bool ssubrange = (mVectorNetFlags & eSignedSubrange) != 0;
  bool usubrange = (mVectorNetFlags & eUnsignedSubrange) != 0;
  NU_ASSERT ( not(ssubrange && usubrange), this );
  return usubrange;
}

bool NUMemoryNet::isIntegerSubrange () const
{
  return (mVectorNetFlags & (eSignedSubrange | eUnsignedSubrange)) != 0;
}

void NUVectorNet::printNameSize() const
{
  UtIO::cout() << "VectorNet(" << this << ") : " << getName()->str() << " ";
  mDeclaredRange.print();
  UtIO::cout() << " : [size=" << mDeclaredRange.getLength() << "]";
  UtIO::cout () << " VFlags (" << mVectorNetFlags << ")";
}


const char* NUVectorNet::typeStr() const
{
  return "NUVectorNet";
}

NUExpr* NUVectorNet::normalizeSelectExpr(NUExpr* sel, SInt32 adjust, bool retNullForOutOfRange)
{
  return sNormalizeExpr(sel, mDeclaredRange, mOperationalRange, adjust, retNullForOutOfRange);
}

NUExpr* NUVectorNet::denormalizeSelectExpr(NUExpr* sel)
{
  return sDenormalizeExpr(sel, mOperationalRange, mDeclaredRange);
}

// constructor for simple memory, that has just 1 packed and 1 unpacked dimension
// note that the two range arguments will be deleted by this constructor
NUMemoryNet::NUMemoryNet(StringAtom *name,
			 ConstantRange *width_range,
			 ConstantRange *depth_range,
			 NetFlags flags,
                         VectorNetFlags vflags,
			 NUScope *scope,
			 const SourceLocator& loc) :
  NUNet(name, flags, scope, loc),
  mFirstUnpackedDimensionIndex(1),
  mMemHadImplicitUnpackedRangeAdded(false),
  mDeclaredWidthRange(*width_range),
  mDeclaredDepthRange(*depth_range), 
  mOperationalWidthRange(width_range->getLength() - 1, 0), // normalized
  mVectorNetFlags (vflags),
  mNumWritePorts(0), mWrittenInForLoop(false), mIsDoubleBuffered(false),
  mResynthesized(true)
{
  mRanges.push_back( *width_range );
  mRanges.push_back( *depth_range );
  mOperationalRanges.push_back( mOperationalWidthRange );
  mOperationalRanges.push_back( ConstantRange(depth_range->getLength()-1, 0) );
  NU_ASSERT( mRanges[0].getLength() == mOperationalRanges[0].getLength(), this );
  NU_ASSERT( mRanges[1].getLength() == mOperationalRanges[1].getLength(), this );
  delete width_range;
  delete depth_range;
}


NUMemoryNet::NUMemoryNet(StringAtom *name,
                         UInt32 firstUnpackedDimension,
                         ConstantRangeArray *range_array,
                         NetFlags flags,
                         VectorNetFlags vflags,
                         NUScope *scope,
                         const SourceLocator& loc) :
  NUNet(name, flags, scope, loc),
  mFirstUnpackedDimensionIndex(firstUnpackedDimension),
  mMemHadImplicitUnpackedRangeAdded(false),
  mVectorNetFlags (vflags),
  mNumWritePorts(0), mWrittenInForLoop(false), mIsDoubleBuffered(false)
{

  const UInt32 numDims = range_array->size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    ConstantRange *range = (*range_array)[i];
    (*range_array)[i] = NULL;
    ConstantRange normalized( range->getLength() - 1, 0);
    mRanges.push_back( *range );
    mOperationalRanges.push_back( normalized );
    NU_ASSERT( mRanges[i].getLength() == mOperationalRanges[i].getLength(), this );
    delete range;
  }
  // a newly created memory is considered 'resynthesized' if it was declared with a single packed dimension and a single unpacked dimension
  // 'resynthesized' here means that the memory is in "normal" form, such as one declared as "reg [7:0] mem [127:0];"
  // here we set this flag only on memories that were declared this way.  memory resynth will take care of any that do not have this flag set.
  mResynthesized = ( ( mRanges.size() == 2 ) && (mFirstUnpackedDimensionIndex ==1) );

  // now fill in the fields that are only valid if mResynthesized is true.
  if ( mResynthesized ){
    mDeclaredWidthRange = mRanges[0];
    mDeclaredDepthRange = mRanges[1];
    mOperationalWidthRange = mOperationalRanges[0];
  } else {
    // use a dummy range of [-1:-1] when mResynthesized is not true so that a user might notice a problem if the range is ever used
    // all methods that return one of these ranges are currently guarded by mResynthesized check
    mDeclaredWidthRange = ConstantRange(-1,-1);
    mDeclaredDepthRange = ConstantRange(-1,-1);
    mOperationalWidthRange = ConstantRange(-1,-1);
  }
}


NUMemoryNet::~NUMemoryNet()
{
}


void 
NUMemoryNet::putResynthesized( bool resynth )
{
  mResynthesized = resynth;
}

void
NUMemoryNet::putMemoryDepth( SInt32 msb, SInt32 lsb )
{
  // This should only be called during memory resynthesis, before
  // setting the resynth flag on a memory
  NU_ASSERT( !mResynthesized, this );
  NU_ASSERT( !lsb, this );      // range is expected to be normalized
  mDeclaredDepthRange.setMsb( msb );
  mDeclaredDepthRange.setLsb( lsb );
}

void
NUMemoryNet::putMemoryWidth( SInt32 msb, SInt32 lsb )
{
  // This should only be called during memory resynthesis, before
  // setting the resynth flag on a memory
  NU_ASSERT( !mResynthesized, this );
  NU_ASSERT( !lsb, this );      // range is expected to be normalized
  NU_ASSERT( ( msb >= lsb ), this); // range is expected to be normalized
  mDeclaredWidthRange.setMsb( msb );
  mDeclaredWidthRange.setLsb( lsb );
  mOperationalWidthRange.setMsb( msb );
  mOperationalWidthRange.setLsb( lsb );
}

//! copy range information from src to this, includes pre/pos resynthesis information as well as firstUnpackedDimension value
// Note does not copy resynthesized flag or other flags, it ONLY handles range information
void
NUMemoryNet::copyRanges( NUMemoryNet *src )
{
  mRanges.clear();
  mOperationalRanges.clear();
  const UInt32 numDims = src->mRanges.size();
  NU_ASSERT(numDims == (src->mOperationalRanges).size(), src);
  // first copy the basic range information 
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mRanges.push_back( src->mRanges[i] );
    mOperationalRanges.push_back( src->mOperationalRanges[i] );
    NU_ASSERT( mRanges[i].getLength() == mOperationalRanges[i].getLength(), src );
  }
  // now copy the resynthesized range information
  mDeclaredWidthRange = src->mDeclaredWidthRange;
  mDeclaredDepthRange = src->mDeclaredDepthRange;
  mOperationalWidthRange = src->mOperationalWidthRange;

  // and information that is specific to the mRanges array
  mFirstUnpackedDimensionIndex = src->getFirstUnpackedDimensionIndex();
}


//! copy auxiliary  information from src to this, eventually all flags and member variables should be handled here
void
NUMemoryNet::copyAuxInformation( NUMemoryNet *src ) {
  mNumWritePorts = src->getNumWritePorts ();
  mWrittenInForLoop = src->getIsWrittenInForLoop();
  mResynthesized = src->isResynthesized();
  mMemHadImplicitUnpackedRangeAdded = src->isMemHaveImplicitUnpackedRange();
}


UInt32 NUMemoryNet::getBitSize () const {
  UInt64 size;
  if ( mResynthesized ) {
    size = (UInt64)mOperationalWidthRange.getLength() * (UInt64)mDeclaredDepthRange.getLength();
  } else {
    const UInt32 numDims = mRanges.size();
    size = mRanges[0].getLength();
    for ( UInt32 i = 1; i < numDims; ++i )
    {
      size *= (UInt64)mRanges[i].getLength();
    }
  }
  if ( size > 0x00000000ffffffffULL )
    return 0;
  return (UInt32)size;
}

UInt32 NUMemoryNet::getRangeSize (UInt32 dimension) const
{
  return mRanges[dimension].getLength();
}

UInt32 NUMemoryNet::getNetRefWidth() const
{
  return getNetRefRange()->getLength();
}


const ConstantRange *NUMemoryNet::getWidthRange() const {
  if ( mResynthesized ){
    return &mOperationalWidthRange;    
  } else if (1 ==  getFirstUnpackedDimensionIndex()) {
    // if there is exactly one packed dimension then we can use that dimension for this range
    return getRange(0);
  }
  NU_ASSERT(mResynthesized, this); // only valid for memories marked as resynthesized
  return NULL;
}

//! returns the depth range. NOTE this not not normalized and is the same as getDeclaredDepthRange() 
const ConstantRange *NUMemoryNet::getDepthRange() const {
  NU_ASSERT(mResynthesized, this); // only valid for memories marked as resynthesized
  return getDeclaredDepthRange();
}




//RJC RC#2013/12/18 (cloutier) does this work with multi dimension arrays, it only returns information about the last dimension
const ConstantRange *NUMemoryNet::getNetRefRange() const
{
  const ConstantRange *retval;
  if ( mResynthesized )
    retval = getDeclaredDepthRange();
  else
    retval = &mRanges[mRanges.size()-1];
  return retval;
}

NUExpr* NUMemoryNet::normalizeSelectExpr(NUExpr* sel, UInt32 dim, SInt32 adjust,
                                         bool retNullForOutOfRange)
{
  return sNormalizeExpr(sel, mRanges[dim], mOperationalRanges[dim], adjust,
                        retNullForOutOfRange);
}

NUExpr* NUMemoryNet::denormalizeSelectExpr(NUExpr* sel, UInt32 dim)
{
  if ( mResynthesized ) {
    NU_ASSERT((dim <= 1), this);
    return sel;                 // for resynthesis the range was declared normalized, so no change
  } else {
    return sDenormalizeExpr(sel, mOperationalRanges[dim], mRanges[dim]);
  }
}

void NUMemoryNet::printNameSize() const
{
  UtIO::cout() << typeStr() << "(" << this << ") : " << getName()->str() << " ";
  if ( mResynthesized ) {
    // print these in the order: (fastest to slowest changing dimensions) (same as mRanges)
    mOperationalWidthRange.print();
    mDeclaredDepthRange.print();
    UtIO::cout() << " : ";
    UtIO::cout() << "[size=" << mOperationalWidthRange.getLength() << "]";
    UtIO::cout() << "[size=" << mDeclaredDepthRange.getLength() << "]";
  }
  else
  {
    const UInt32 numDims = mRanges.size();
    for ( UInt32 i = 0; i < numDims; ++i ) {
      mRanges[i].print();       // here we print them in the order the ranges are stored in mRanges, not the order they appear in the HDL
    }
    for ( UInt32 i = 0; i < numDims; ++i ) {
      if (! i ) { UtIO::cout() << " : "; }
      UtIO::cout() << "[size=" << mOperationalRanges[i].getLength() << "]";
    }
  }
  UtIO::cout() << " 1stUnpkdDimIdx=" << mFirstUnpackedDimensionIndex;
  if ( mMemHadImplicitUnpackedRangeAdded ) {
    UtIO::cout() << " [0:0]added";
  }
  UtIO::cout () << " VFlags (" << mVectorNetFlags << ")";
}


const char* NUMemoryNet::typeStr() const
{
  return "NUMemoryNet";
}

NUTempMemoryNet::NUTempMemoryNet(StringAtom* name, NUMemoryNet* master,
                                 NetFlags flags, bool isDynamic,
                                 NUScope* scope):
  NUNet(name, flags, scope, master->getLoc()), mMaster(master)
{
  // Make the DeclareMask netflags for the temp match those of the master
  mFlags = NetRedeclare (mFlags, (master->getFlags () & (eDeclareMask | eSigned)));
  mIsDynamic = isDynamic;
  mNumWritePorts = 0;
}

UInt32 NUTempMemoryNet::getBitSize() const
{
  return mMaster->getBitSize();
}

UInt32 NUTempMemoryNet::getNetRefWidth() const
{
  return mMaster->getNetRefWidth();
}

NUTempMemoryNet::~NUTempMemoryNet()
{
}

const char* NUTempMemoryNet::typeStr()
  const
{
  return "NUTempMemoryNet";
}

VectorNetFlags NUTempMemoryNet::getVectorNetFlags() const
{
  return mMaster->getVectorNetFlags ();
}

bool NUTempMemoryNet::isSignedSubrange () const
{
  return mMaster->isSignedSubrange ();
}

bool NUTempMemoryNet::isUnsignedSubrange () const
{
  return mMaster->isUnsignedSubrange ();
}

bool NUTempMemoryNet::isIntegerSubrange () const
{
  return mMaster->isIntegerSubrange ();
}

// Don't think we want this one
//void NUTempMemoryNet::putVectorNetFlags (VectorNetFlags f);


void NUTempMemoryNet::printNameSize() const
{
  UtIO::cout() << "TempMemoryNet(" << this << ") : " << getName()->str() << " ";
  getDepthRange()->print();
  getWidthRange()->print();
  UtIO::cout() << " : [size=" << getDepthRange()->getLength() << "]";
  UtIO::cout() << " : [size=" << getWidthRange()->getLength() << "]";
}


NUVirtualNet::NUVirtualNet(StringAtom *name,
                           NetFlags flags,
                           NUScope* scope,
                           const SourceLocator& loc) :
  NUNet(name, flags, scope, loc)
#if NUBITNETREF_STORED_IN_NUBITNET
  , mNetRef(this, 0, NULL)
#endif
{}


NUVirtualNet::~NUVirtualNet()
{
}

const char* NUVirtualNet::typeStr() const
{
  return "NUVirtualNet";
}


bool NUNetFilter::operator()(NUNet* net) {
  NU_ASSERT(mFlags & ePortMask, net);
  return (net->getFlags() & ePortMask) == mFlags;
}

void NUNet::replaceScope(NUScope* oldScope, NUScope* newScope)
{
  NU_ASSERT(mScope == oldScope, this);
  mScope = newScope;
}

//! Return true if this net has multiple continuous drivers
bool NUNet::isMultiplyDriven() const {
  // Calling size() is bad for Lists, but mContinuousDrivers is
  // a UtHashMap, which keeps the size in a member var.
  return mContinuousDrivers.size() > 1;
}

//! Return true if this net is connected to tristate-driver
bool NUNet::isUndriven() const {
  NUNet* master = getMaster();
  if ((master != this) && !master->isUndriven())
    return false;               // If my master is driven then so am I
  bool drivenByInternalBid = !isPrimary() && isBid();
  return
    (!isTriWritten() && !isWritten() && !drivenByInternalBid
     && !isInput() && !isBlockLocal()

     // JDM: workaround what I think is a rescope bug in langcov/alias.v.
     // top.a gets unelab-merged/rescoped with b, c, and o2 assignments,
     // but doesn't get properly aliased.  So it looks undriven.  It
     // really is undriven but it's also unreferenced, so it doesn't
     // really matter.
     // (isRead() || isHierRefRead()) -- alas, this causes massive
     // trauma in test/depobs/test2.v, because if it doesn't look undriven
     // then schedule/MarkDesign.cxx marks top.o2 as a constant 0!

     && !isDepositable()
     && !isHierRefWritten());
}

bool NUNet::isPulled() const {
  return isPullUp() || isPullDown() || isTri0() || isTri1() || isTrireg();
}

bool NUNet::isTristate() const
{
  return isPulled() || isTriWritten() || isPrimaryZ();
}

bool NUNet::isZ() const
{
  // NUNet* master = getMaster();  check master?
  if (isPrimaryZ())
    return true;
  if (isTriWritten() || isUndriven() || isConstZ())
  {
    // This used to check isPulled() for a false value.
    // But, the isPulled() function just really deals with the type the net was declared with,
    // and this check caused inconsistencies between hierarchy, so that has been removed.
    // Additionally, that check could cause the '-tristate z' switch to not work correctly.
    //
    // If no one reads the value then codegen will not
    // emit a net for it, so don't complicate matters
    // by considering it to be Z
    if (isRead() || isBlockLocal() ||
        isConstZ())
    {
      if (!isMemoryNet ())      // Memories are NEVER tristated
        return true;
    }
  }
  return false;
}

NUNet* NUNet::find(const STSymbolTableNode* node)
{
  NUBase* base = CbuildSymTabBOM::getNucleusObj(node);
  NUNet* net = dynamic_cast<NUNet*>(base);
  if (net == NULL)
  {
    NUNetElab* netElab = dynamic_cast<NUNetElab*>(base);
    if (netElab != NULL)
      net = netElab->getNet();
  }
  return net;
}


NUNet* NUNet::findStorage(const STSymbolTableNode* node)
{
  const STAliasedLeafNode *storage_leaf = node->castLeaf()->getStorage();
  NUBase* base = CbuildSymTabBOM::getNucleusObj(storage_leaf);
  NUNet* net = dynamic_cast<NUNet*>(base);
  return net;
}


NUNet* NUNet::findUnelab(const STSymbolTableNode* node)
{
  NUAliasDataBOM * bom = NUAliasBOM::castBOM(node->getBOMData());
  NUNet* net = NULL;
  if (bom->hasValidNet())       // this is a debugging routine, do not assert.
    net = bom->getNet();
  return net;
}


NUNet* NUNet::findStorageUnelab(const STSymbolTableNode* node)
{
  const STAliasedLeafNode *storage_leaf = node->castLeaf()->getStorage();
  return NUNet::findUnelab(storage_leaf);
}


NUBitNetHierRef::NUBitNetHierRef(const AtomArray& path,
                                 NetFlags flags,
				 NUScope *scope,
				 const SourceLocator &loc) :
  NUBitNet(NUHierRef::gensym(scope, path),
           flags, scope, loc),
  mNetHierRef(this, path)
{
}


NUBitNetHierRef::~NUBitNetHierRef()
{
}


void NUBitNetHierRef::printNameSize() const
{
  UtIO::cout() << "BitNetHierRef(" << this << ") : "
	       << getName()->str();
}


NUVectorNetHierRef::NUVectorNetHierRef(const AtomArray& path,
				       ConstantRange *range,
                                       NetFlags flags,
                                       VectorNetFlags vflags,
				       NUScope *scope,
				       const SourceLocator &loc) :
  NUVectorNet(NUHierRef::gensym(scope, path),
              range, flags, vflags, scope, loc),
  mNetHierRef(this, path)
{
}


NUVectorNetHierRef::~NUVectorNetHierRef()
{
}


void NUVectorNetHierRef::printNameSize() const
{
  UtIO::cout() << "VectorNetHierRef(" << this << ") : "
	       << getName()->str() << " ";
  mDeclaredRange.print();
  UtIO::cout() << " : [size=" << mDeclaredRange.getLength() << "]";
}


NUMemoryNetHierRef::NUMemoryNetHierRef(const AtomArray& path,
				       ConstantRange *width_range,
				       ConstantRange *depth_range,
                                       NetFlags flags,
                                       VectorNetFlags vflags,
				       NUScope *scope,
                                       bool resynth,
				       const SourceLocator &loc) :
  NUMemoryNet(NUHierRef::gensym(scope, path),
              width_range, depth_range, flags, vflags, scope, loc),
  mNetHierRef(this, path)
{
  putResynthesized( resynth );
}


NUMemoryNetHierRef::NUMemoryNetHierRef(const AtomArray& path,
				       ConstantRangeArray *rangeArray,
                                       NetFlags flags,
                                       VectorNetFlags vflags,
				       NUScope *scope,
                                       bool resynth,
				       const SourceLocator &loc) :
  //RJC RC#2013/11/20 (cloutier) how can we determine the correct value here for the firstUnpackedDimension? (especially for SV)
  //RJC RC#2013/12/24 (cloutier)  how about mMemHadImplicitUnpackedRangeAdded, mWrittenInForLoop, mNumWritePorts on the base memory?
  NUMemoryNet(NUHierRef::gensym(scope, path), 1 /*firstUnpackedDimension*/,
              rangeArray, flags, vflags, scope, loc),
  mNetHierRef(this, path)
{
  putResynthesized( resynth );
}


NUMemoryNetHierRef::~NUMemoryNetHierRef()
{
}


void NUMemoryNetHierRef::printNameSize() const
{
  UtIO::cout() << "MemoryNetHierRef(" << this << ") : "
	       << getName()->str() << " ";
  if ( isResynthesized() )
  {
    // print these in the order: (fastest to slowest changing dimensions) (same as mRanges)
    mOperationalWidthRange.print();
    mDeclaredDepthRange.print();
    UtIO::cout() << " : ";
    UtIO::cout() << "[size=" << mOperationalWidthRange.getLength() << "]";
    UtIO::cout() << "[size=" << mDeclaredDepthRange.getLength() << "]";
  }
  else
  {
    const UInt32 numDims = mOperationalRanges.size();
    for ( UInt32 i = numDims; i > 0; --i )
    {
      mOperationalRanges[i-1].print();
    }
    for ( UInt32 i = numDims; i > 0; --i )
    {
      if ( !i ) { UtIO::cout() << " : "; }

      UtIO::cout() << "[size=" << mOperationalRanges[i-1].getLength() << "]";
    }
  }
  UtIO::cout() << " 1stUnpkdDimIdx=" << mFirstUnpackedDimensionIndex;
  if ( mMemHadImplicitUnpackedRangeAdded ) {
    UtIO::cout() << " [0:0]added";
  }
}


void NUBitNetHierRef::addHierRef(NUBase *)
{
  NU_ASSERT("Cannot add a hierarchical reference to a hierarchical reference"==0, this);
}


void NUBitNetHierRef::removeHierRef(NUBase *)
{
  NU_ASSERT("Cannot remove a hierarchical reference from a hierarchical reference"==0, this);
}


void NUVectorNetHierRef::addHierRef(NUBase *)
{
  NU_ASSERT("Cannot add a hierarchical reference to a hierarchical reference"==0, this);
}


void NUVectorNetHierRef::removeHierRef(NUBase *)
{
  NU_ASSERT("Cannot remove a hierarchical reference from a hierarchical reference"==0, this);
}


void NUMemoryNetHierRef::addHierRef(NUBase *)
{
  NU_ASSERT("Cannot add a hierarchical reference to a hierarchical reference"==0, this);
}


void NUMemoryNetHierRef::removeHierRef(NUBase *)
{
  NU_ASSERT("Cannot remove a hierarchical reference from a hierarchical reference"==0, this);
}

void NUCompositeNetHierRef::addHierRef(NUBase *)
{
  NU_ASSERT("Cannot remove a hierarchical reference from a hierarchical reference"==0, this);
}

void NUCompositeNetHierRef::removeHierRef(NUBase *)
{
  NU_ASSERT("Cannot remove a hierarchical reference from a hierarchical reference"==0, this);
}


NUCompositeNetHierRef::NUCompositeNetHierRef(const AtomArray& path,
					     const StringAtom *typeMark,
					     NetFlags flags, NUNetVector *fields, 
					     const ConstantRangeVector *ranges, bool packed,
					     NUScope *scope, const SourceLocator &loc) :
  NUCompositeNet(typeMark,
                 *fields,
                 *ranges,
                 packed,
                 scope,
                 NUHierRef::gensym(scope, path),
                 loc,
                 flags),


  mNetHierRef(this, path)
{
  
}


NUCompositeNetHierRef::~NUCompositeNetHierRef()
{
}

void NUCompositeNetHierRef::printNameSize() const
{

  NUCompositeNet::printNameSize();
}


//! Return true if any nets in the loop are hier-ref read or the net itself is read
static bool helperIsHierRefRead(NUNetHierRef::NetVectorCLoop loop)
{
  for (; not loop.atEnd(); ++loop) {
    const NUNet *net = (*loop);
    if (net->isHierRefRead() or net->isRead() or net->isOutput() or net->isBid()) {
      return true;
    }
  }
  return false;
}


//! Return true if any nets in the loop are hier-ref written or the net itself is written
static bool helperIsHierRefWritten(NUNetHierRef::NetVectorCLoop loop)
{
  for (; not loop.atEnd(); ++loop) {
    const NUNet *net = (*loop);
    if (net->isHierRefWritten() or net->isWritten() or net->isInput() or net->isBid()
        or net->isTriWritten()) {
      return true;
    }
  }
  return false;
}


//! Return true if any nets in the loop are protected mutable due to non-hierref reasons
static bool helperIsProtectedMutableNonHierref(NUNetHierRef::NetVectorCLoop loop, const IODBNucleus *iodb, bool checkPrimary)
{
  for (; not loop.atEnd(); ++loop) {
    const NUNet *net = (*loop);
    if (net->isProtectedMutableNonHierref(iodb, checkPrimary)) {
      return true;
    }
  }
  return false;
}


bool NUBitNetHierRef::isHierRefRead() const
{
  return helperIsHierRefRead(mNetHierRef.loopResolutions());
}


bool NUBitNetHierRef::isHierRefWritten() const
{
  return helperIsHierRefWritten(mNetHierRef.loopResolutions());
}


bool NUBitNetHierRef::isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                                   bool checkPrimary) const
{
  return helperIsProtectedMutableNonHierref(mNetHierRef.loopResolutions(),
                                            iodb, checkPrimary);
}


bool NUVectorNetHierRef::isHierRefRead() const
{
  return helperIsHierRefRead(mNetHierRef.loopResolutions());
}


bool NUVectorNetHierRef::isHierRefWritten() const
{
  return helperIsHierRefWritten(mNetHierRef.loopResolutions());
}


bool NUCompositeNetHierRef::isHierRefRead() const
{
  return helperIsHierRefRead(mNetHierRef.loopResolutions());
}


bool NUCompositeNetHierRef::isHierRefWritten() const
{
  return helperIsHierRefWritten(mNetHierRef.loopResolutions());
}



bool NUVectorNetHierRef::isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                                      bool checkPrimary) const
{
  return helperIsProtectedMutableNonHierref(mNetHierRef.loopResolutions(),
                                            iodb, checkPrimary);
}


bool NUMemoryNetHierRef::isHierRefRead() const
{
  return helperIsHierRefRead(mNetHierRef.loopResolutions());
}


bool NUMemoryNetHierRef::isHierRefWritten() const
{
  return helperIsHierRefWritten(mNetHierRef.loopResolutions());
}


bool NUMemoryNetHierRef::isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                                      bool checkPrimary) const
{
  return helperIsProtectedMutableNonHierref(mNetHierRef.loopResolutions(),
                                            iodb, checkPrimary);
}


bool NUCompositeNetHierRef::isProtectedMutableNonHierref(const IODBNucleus* iodb,
                                                      bool checkPrimary) const
{
  return helperIsProtectedMutableNonHierref(mNetHierRef.loopResolutions(),
                                            iodb, checkPrimary);
}


NUExtNet::NUExtNet(StringAtom *name,
		   NetFlags flags,
		   NUScope *scope,
		   const SourceLocator& loc) :
  NUVirtualNet(name, flags, scope, loc)
{
}


NUExtNet::~NUExtNet()
{
}


const char* NUExtNet::typeStr() const
{
  return "NUExtNet";
}


void NUExtNet::composeDeclaration(UtString* buf)
  const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  *buf << getName()->str() << ";";
}


void NUExtNet::printNameSize() const
{
  UtIO::cout() << "ExtNet(" << this << ") : " << getName()->str();
}


NUSysTaskNet::NUSysTaskNet(StringAtom *name,
                           NetFlags flags,
                           NUScope* scope,
                           const SourceLocator& loc) :
  NUVirtualNet(name, flags, scope, loc)
{}


NUSysTaskNet::~NUSysTaskNet()
{
}

const char* NUSysTaskNet::typeStr() const
{
  return "NUSysTaskNet";
}

void NUSysTaskNet::composeDeclaration(UtString* buf)
  const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  *buf << getName()->str() << ";";
}


void NUSysTaskNet::printNameSize() const
{
  UtIO::cout() << "SysTaskNet(" << this << ") : " << getName()->str();
}


NUControlFlowNet::NUControlFlowNet(StringAtom *name,
                                   NetFlags flags,
                                   NUScope* scope,
                                   const SourceLocator& loc) :
  NUVirtualNet(name, flags, scope, loc)
{}


NUControlFlowNet::~NUControlFlowNet()
{
}

const char* NUControlFlowNet::typeStr() const
{
  return "NUControlFlowNet";
}

void NUControlFlowNet::composeDeclaration(UtString* buf)
  const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }

  *buf << getName()->str() << ";";
}


void NUControlFlowNet::printNameSize() const
{
  UtIO::cout() << "ControlFlowNet(" << this << ") : " << getName()->str();
}

bool NUNet::isGensym() const
{
  return mScope->isGensym(getName());
}

NUNet *NUBitNet::createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc)
{
  return new NUBitNetHierRef(name, flags, scope, loc);
}


NUNet *NUVectorNet::createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc)
{
  ConstantRange *range = new ConstantRange(*getRange());
  return new NUVectorNetHierRef(name, range, flags, VectorNetFlags (0), scope, loc);
}


NUNet *NUMemoryNet::createHierRef(const AtomArray& name, NUScope *scope, NetFlags flags, const SourceLocator &loc)
{
  // TJM fix this (RJC fix what?)
  NUMemoryNetHierRef *new_mem = new NUMemoryNetHierRef(name,
                                                       new ConstantRange( ), new ConstantRange( ),// dummy ranges, instead they will be filled in below by copyRanges
                                                       flags,
                                                       VectorNetFlags (0),
                                                       scope, isResynthesized(), loc);
  new_mem->copyRanges(this);
  new_mem->putResynthesized(this->isResynthesized());
  new_mem->setImplicitUnpackedRangeAdded(this->isMemHaveImplicitUnpackedRange());
  return new_mem;
}


NUNet *NUCompositeNet::createHierRef(const AtomArray& name, NUScope* module, NetFlags /*flags*/, const SourceLocator &loc)
{
  // Create hierarchical references for each field of the record
  AtomArray compNetHierName = name;
  compNetHierName.push_back( getName() );

  NUNet *netHR = NULL;

  NUNet *hierRefNet = module->getModule()->findNetHierRef(compNetHierName);
  if(hierRefNet == NULL)
  {
    NUNetVector fields;
    int numFields = getNumFields();
    for(int i = 0; i < numFields; i++)
    {
      NUNet *fieldNet = getField(i);
      if(fieldNet != NULL)
      {
	if(fieldNet->isCompositeNet())
	{
	  NUCompositeNet *compNet = dynamic_cast<NUCompositeNet *> (fieldNet);
	  NUScope *pScope =  compNet->getScope();

	  AtomArray fieldHierName = name;
	  fieldHierName.push_back( pScope->getName() );
	  
	  NUNet *hierRefCompNet = compNet->createHierRef(fieldHierName, module, eNoneNet, fieldNet->getLoc());
	  if(hierRefCompNet != NULL)
	    fields.push_back(hierRefCompNet);
        }
        else
        {
          AtomArray fieldHierName = name;
	  fieldHierName.push_back( fieldNet->getScope()->getName() );
	  fieldHierName.push_back( fieldNet->getName() );

	  NUNet *hierRefFieldNet = fieldNet->buildHierRef(module->getModule(), fieldHierName);
	  if(hierRefFieldNet != NULL)
	    fields.push_back(hierRefFieldNet);
        }
      }
    }

    NUCompositeNetHierRef *com_net = new NUCompositeNetHierRef(compNetHierName,
							       getTypeMark(),
							       getFlags(),
							       &fields,
							       getRanges(), 
							       isPacked(),
							       module, loc);

    module->getModule()->addNetHierRef(com_net);
    NUNetHierRef *new_hier_ref = com_net->getHierRef();
    new_hier_ref->addResolution(this);
    addHierRef(com_net);
    
    netHR = com_net;
  }
  else
    netHR = hierRefNet;

  return netHR;
}

NUExpr* NUCompositeNet::normalizeSelectExpr(NUExpr* sel, UInt32 dim, SInt32 adjust,
                                            bool retNullForOutOfRange)
{
  ConstantRange  norm_range(mRanges[dim]);
  norm_range.normalize(&(mRanges[dim]), false);
  return sNormalizeExpr(sel, mRanges[dim], norm_range, adjust, retNullForOutOfRange);
}


NUNet *NUTempMemoryNet::createHierRef(const AtomArray&, NUScope *, NetFlags , const SourceLocator &)
{
  NU_ASSERT(0, this); // Cannot create hierrefs of temp memories
  return NULL;
}


NUNet *NUVirtualNet::createHierRef(const AtomArray&, NUScope *, NetFlags , const SourceLocator &)
{
  NU_ASSERT(0, this); // Cannot create hierrefs of virtual nets
  return NULL;
}


NUNet *NUBitNetHierRef::createHierRef(const AtomArray&, NUScope *, NetFlags , const SourceLocator &)
{
  NU_ASSERT(0, this); // Cannot create hierrefs of hierrefs
  return NULL;
}


NUNet *NUVectorNetHierRef::createHierRef(const AtomArray&, NUScope *, NetFlags , const SourceLocator &)
{
  NU_ASSERT(0, this); // Cannot create hierrefs of hierrefs
  return NULL;
}


NUNet *NUMemoryNetHierRef::createHierRef(const AtomArray&, NUScope *, NetFlags , const SourceLocator &)
{
  NU_ASSERT(0, this); // Cannot create hierrefs of hierrefs
  return NULL;
}


NUNet *NUCompositeNetHierRef::createHierRef(const AtomArray&, NUScope *, NetFlags , const SourceLocator &)
{
  NU_ASSERT(0, this); // Cannot create hierrefs of hierrefs
  return NULL;
}


void
NUNetElab::getPrimaryNetsAndFlags(const STAliasedLeafNodeVector& leafNodes,
                                  NetFlags* netFlags, NUNetSet* primNets)
{
  NetFlags tmpFlags = eNoneNet;

  STAliasedLeafNodeVector::const_iterator i;
  for (i = leafNodes.begin(); i != leafNodes.end(); ++i)
  {
    // Get the net for this symbol table node
    const STAliasedLeafNode* node = *i;
    NUNet* identNet = NUNet::find(node);
    ST_ASSERT(identNet != NULL, node);

    // If this is a primary port, get the right flags and add this
    // net and all its aliases
    if (identNet->isPrimaryPort())
    {
      // Get all the NUNets in the alias ring
      NUNetVector netVector;
      getAliasNets(node, &netVector, true);
      if (primNets != NULL) {
        primNets->insert(netVector.begin(), netVector.end());
      }

      // Determine the flags
      if (identNet->isBid())
        tmpFlags = eBidNet;
      else if (identNet->isInput())
      {
        if (tmpFlags == eNoneNet)
          tmpFlags = eInputNet;
        else if (tmpFlags == eOutputNet)
          tmpFlags = eBidNet;
      }
      else if (identNet->isOutput())
      {
        if (tmpFlags == eNoneNet)
          tmpFlags = eOutputNet;
        else if (tmpFlags == eInputNet)
          tmpFlags = eBidNet;
      }
      else
        NU_ASSERT(0, identNet);
    } // if
  } // for

  // Update our callers net flags
  *netFlags = tmpFlags;
}

NetFlags
NUNetElab::getPrimaryNetsAndFlags(STSymbolTable* symbolTable, 
                                  NUNetElab* netElab, NUNetSet* primNets)
{
  // Get the BOM which can get the expression leaves.
  STFieldBOM* fieldBOM =  symbolTable->getFieldBOM();
  CbuildSymTabBOM* symTabBOM = dynamic_cast<CbuildSymTabBOM*>(fieldBOM);
  INFO_ASSERT(symTabBOM, "Symbol table has invalid BOM");

 // Grab the leaf nodes for this symbol table node expr
  STAliasedLeafNode* node = netElab->getSymNode();
  STAliasedLeafNodeVector nodes;
  bool isExpr = symTabBOM->getMappedExprLeafNodes(node, &nodes);
  if (!isExpr) {
    nodes.push_back(node);
  }

  // Get the flags for all the nodes
  NetFlags netFlags;
  NUNetElab::getPrimaryNetsAndFlags(nodes, &netFlags, primNets);
  return netFlags;
}

//! Does this net contain the specified range?
bool NUBitNet::containsRange(const ConstantRange& range) const {
  return (range.getLsb() == 0) && (range.getMsb() == 0);
}

//! Does this net contain the specified bit?
bool NUBitNet::containsBit(SInt32 index) const {
  return index == 0;
}

//! Does this net contain the specified range?
bool NUVectorNet::containsRange(const ConstantRange& range) const {
  return mOperationalRange.contains(range);
}

//! Does this net contain the specified bit?
bool NUVectorNet::containsBit(SInt32 index) const {
  return mOperationalRange.contains(index);
}
  

//! Does this net contain the specified range?
bool NUMemoryNet::containsRange(const ConstantRange& range) const {
  ConstantRange denormalized(range);
  denormalized.denormalize(getNetRefRange(), false);
  return getNetRefRange()->contains(denormalized);
}

//! Does this net contain the specified bit?  This implementation is incorrect unless the memory is marked as resynthesized
bool NUMemoryNet::containsBit(SInt32 index) const {
  return getNetRefRange()->contains(index);
}
  
//! Does this net contain the specified range?
bool NUTempMemoryNet::containsRange(const ConstantRange& range) const {
  return mMaster->containsRange(range);
}

//! Does this net contain the specified bit?
bool NUTempMemoryNet::containsBit(SInt32 index) const {
  return mMaster->containsBit(index);
}
  
void NUTempMemoryNet::incrementWritePorts(UInt32 write_ports) {
  mNumWritePorts += write_ports;
}

//! Does this net contain the specified range?
bool NUVirtualNet::containsRange(const ConstantRange& range) const {
  return (range.getLsb() == 0) && (range.getMsb() == 0);
}

//! Does this net contain the specified bit?
bool NUVirtualNet::containsBit(SInt32 index) const {
  return index == 0;
}

#if NUBITNETREF_STORED_IN_NUBITNET
const NUNetRef* NUNet::getNetRef() const {
  return NULL;
}

const NUNetRef* NUBitNet::getNetRef() const {
  return const_cast<NUNetRef*>(&mNetRef);
}

const NUNetRef* NUVirtualNet::getNetRef() const {
  return const_cast<NUNetRef*>(&mNetRef);
}
#endif

bool NUNet::isBitNet() const {
  return false;
}

bool NUBitNet::isBitNet() const {
  return true;
}

bool NUNet::isVectorNet() const {
  return false;
}

bool NUVectorNet::isVectorNet() const {
  return true;
}

bool NUNet::isVHDLLineNet() const {
  return false;
}

bool NUVectorNet::isVHDLLineNet() const {
  return mVectorNetFlags & eVhdlLineNet;
}

bool NUNet::isMemoryNet() const {
  return false;
}

bool NUMemoryNet::isMemoryNet() const {
  return true;
}

bool NUNet::isVirtualNet() const {
  return false;
}

bool NUVirtualNet::isVirtualNet() const {
  return true;
}

bool NUNet::isSysTaskNet() const {
  return false;
}

bool NUSysTaskNet::isSysTaskNet() const {
  return true;
}

bool NUNet::isCompositeNet() const {
  return false;
}

bool NUNet::isArrayNet() const {
  return false;
}

NUMemoryNet* NUNet::getMemoryNet() {
  return NULL;
}

NUMemoryNet* NUMemoryNet::getMemoryNet() {
  return this;
}

NUMemoryNet* NUTempMemoryNet::getMemoryNet() {
  return mMaster;
}

NUCompositeNet* NUNet::getCompositeNet() {
  return NULL;
}

//! Return the declared bit size
UInt32 NUBitNet::getBitSize() const {
  return 1;
}

//! Get the number of bits that a NUBitNet needs in a netref. (1)
UInt32 NUBitNet::getNetRefWidth() const {
  return 1;
}

//! Return the declared bit size
UInt32 NUVirtualNet::getBitSize() const {
  return 1;
}

//! The number of bits required in a NUNetRef.
UInt32 NUVirtualNet::getNetRefWidth() const {
  return 1;
}


NUNetElab* NUNetElab::find(const STSymbolTableNode* node)
{
  return dynamic_cast<NUNetElab*>(NUElabBase::find(node));
}

NUVectorNet* NUNet::castVectorNet() {
  return NULL;
}

NUVectorNet* NUVectorNet::castVectorNet() {
  return this;
}

NUNetElab::SortedDriverLoop NUNetElab::loopSortedContinuousDrivers() const {
  return mContinuousDrivers.loopSorted();
}

bool NUNetElab::NoHierRef::operator() (const STAliasedLeafNode* node) const
{
  const NUNet* net = NUNet::find(node);
  bool ret = false;
  if (net)
    ret = ! net->isHierRef();
  return ret;
}

NUNetElab::NoHierRefAliasLoop 
NUNetElab::loopNoHierRefAliases()
{
  STAliasedLeafNode::AliasLoop aliases(getSymNode());
  NoHierRef func;
  return NoHierRefAliasLoop(aliases, func);
}

NUNet * NUNet::clone ( NUScope * scope,
                       NUNetReplacementMap & net_replacements,
                       StringAtom* name,
                       NetFlags flags)
{
  NUMemoryNet * memory_net = dynamic_cast<NUMemoryNet*>(this);
  NUTempMemoryNet * tmp_memory_net = dynamic_cast<NUTempMemoryNet*>(this);

  if (name == NULL) {
    name = getName();
  }

  // TBD: give good names
  NUNet * local = NULL;
  if ( isBitNet() ) {
    local = new NUBitNet ( name,
                           flags,
                           scope,
                           getLoc() );
  } else if ( isVectorNet() ) {
    NUVectorNet * vector_net = static_cast<NUVectorNet*>(this);
    local = new NUVectorNet ( name,
			      new ConstantRange( *vector_net->getDeclaredRange() ),
			      flags,
                              vector_net->getVectorNetFlags (),
			      scope,
			      vector_net->getLoc() );
  } else if ( memory_net ) {
    NUMemoryNet * local_mem = 
      new NUMemoryNet ( name,
			new ConstantRange( ), new ConstantRange( ),// dummy ranges used here, they will be filled in below by copyRanges
			flags,
                        memory_net->getVectorNetFlags (),
			scope,
			memory_net->getLoc() );

    NU_ASSERT( local_mem->getBitSize() != 0, memory_net );
    local_mem->copyRanges(memory_net); // copy all range arrays and associated member variables
    local_mem->copyAuxInformation( memory_net );
    local = local_mem;

  } else {
    // fail if we see something unexpected
    NU_ASSERT ( tmp_memory_net, this);

    NUMemoryNet * master = tmp_memory_net->getMaster();
    NUMemoryNet * master_copy = master;

    NUNetReplacementMap::iterator location = net_replacements.find(master);
    if ( location != net_replacements.end() ) {
      NUNet * net_copy = location->second;
      master_copy = dynamic_cast<NUMemoryNet*>(net_copy);
      NU_ASSERT(master_copy, this);
    }

    // we should always see the master before the temp referencing it.
    NU_ASSERT(master!=master_copy, this);

    NUTempMemoryNet * local_tmp_mem = 
      new NUTempMemoryNet ( name,
			    master_copy,
			    flags,
			    tmp_memory_net->isDynamic(),
			    scope );
    local = local_tmp_mem;
  }

  return local;
} // NUNet * NUNet::clone

NUNet* NUNet::cloneHierRef(NUModule *module, const AtomArray& path) {
  NU_ASSERT(isHierRef(), this);

  // First, see if one already exists in the module.
  NUNet *new_net = module->findNetHierRef(path);
  if (new_net) {
    return new_net;
  }

  // Doesn't yet exist, so create one.
  NUBitNetHierRef *bit_net_hier_ref = dynamic_cast<NUBitNetHierRef*>(this);
  NUVectorNetHierRef *vector_net_hier_ref = dynamic_cast<NUVectorNetHierRef*>(this);
  NUMemoryNetHierRef *memory_net_hier_ref = dynamic_cast<NUMemoryNetHierRef*>(this);
  if (bit_net_hier_ref) {
    new_net = new NUBitNetHierRef(path,
                                  getFlags(),
                                  module,
                                  getLoc());
  } else if (vector_net_hier_ref) {
    new_net = new NUVectorNetHierRef(path,
                                     new ConstantRange(*vector_net_hier_ref->getDeclaredRange()),
                                     getFlags(),
                                     vector_net_hier_ref->getVectorNetFlags (),
                                     module,
                                     getLoc());
  } else if (memory_net_hier_ref) {
    NUMemoryNetHierRef *mem = new NUMemoryNetHierRef(path,
                                     new ConstantRange(*memory_net_hier_ref->getWidthRange()),
                                     new ConstantRange(*memory_net_hier_ref->getDepthRange()),
                                     getFlags(),
                                     memory_net_hier_ref->getVectorNetFlags (),
                                     module,
                                     memory_net_hier_ref->isResynthesized(),
                                     getLoc());
    mem->copyRanges(memory_net_hier_ref);
    new_net = mem;
  } else {
    NU_ASSERT("Bad hierarchical net reference type" == 0, this);
  }

  // Replicate the resolutions.
  NUNetHierRef *new_hier_ref = new_net->getHierRef();
  for (NUNetHierRef::NetVectorLoop loop = getHierRef()->loopResolutions(); not loop.atEnd(); ++loop) {
    NUNet *resolution_net = (*loop);
    new_hier_ref->addResolution(resolution_net);
    resolution_net->addHierRef(new_net);
  }

  module->addNetHierRef(new_net);

  return new_net;
}

NUNet* NUNet::buildHierRef(NUModule *module, const AtomArray& path) {
  NU_ASSERT(!isHierRef(), this);

  // First, see if one already exists in the module.
  NUNet *new_net = module->findNetHierRef(path);
  if (new_net) {
    return new_net;
  }

  // Doesn't yet exist, so create one.
  NUBitNet *bit_net = dynamic_cast<NUBitNet*>(this);
  NUVectorNet *vector_net = dynamic_cast<NUVectorNet*>(this);
  NUMemoryNet *memory_net = dynamic_cast<NUMemoryNet*>(this);
  NetFlags flags = NetFlags((mFlags & (eDeclareMask | eSigned))
                            | eAllocatedNet);
  if (bit_net) {
    new_net = new NUBitNetHierRef(path,
                                  flags,
                                  module,
                                  getLoc());
  } else if (vector_net) {
    new_net = new NUVectorNetHierRef(path,
                                     new ConstantRange(*vector_net->getDeclaredRange()),
                                     flags,
                                     vector_net->getVectorNetFlags (),
                                     module,
                                     getLoc());
  } else if (memory_net) {
    NUMemoryNetHierRef *mem = new NUMemoryNetHierRef(path,
                                     new ConstantRange( ), new ConstantRange( ),// dummy ranges, instead they will be filled in below by copyRanges
                                     flags,
                                     memory_net->getVectorNetFlags (),
                                     module,
                                     memory_net->isResynthesized(),
                                     getLoc());
    mem->copyRanges(memory_net);
    mem->putResynthesized( memory_net->isResynthesized() );
    mem->setImplicitUnpackedRangeAdded(memory_net->isMemHaveImplicitUnpackedRange());
    new_net = mem;
  } else {
    NU_ASSERT("Bad hierarchical net reference type" == 0, this);
  }

  // Create the resolution
  NUNetHierRef *new_hier_ref = new_net->getHierRef();
  new_hier_ref->addResolution(this);
  module->addNetHierRef(new_net);
  addHierRef(new_net);

  return new_net;
} // NUNet* NUNet::buildHierRef

bool NUNet::is2DAnything() const {
  if ( NetIsDeclaredAs(mFlags, eDMWire2DNet) or
       NetIsDeclaredAs(mFlags, eDMReg2DNet) or
       NetIsDeclaredAs(mFlags, eDMInteger2DNet) or
       NetIsDeclaredAs(mFlags, eDMTime2DNet) or
       NetIsDeclaredAs(mFlags, eDMRTime2DNet)     )
  {
    return true;
  }
  return false;
}

bool NUNet::isRecordField() const
{
  NUScope* scope = getScope();
  if ( (scope->getScopeType() == NUScope::eNamedDeclarationScope) &&
       ((scope->getHierFlags() & eHierTypeMask) == eHTRecord) ) {
    return true;
  }
  return false;
}

//This function should be used during population to get the
//declared number of dimensions which may not be the number of dimensions of
//the net. For both two and three dimensional array nets "is2DAnything" flag is
// set. So, to know whether a net is 3D this function is useful.

UInt32 NUNet::getDeclaredDimensions() const
{
  UInt32 dimension = 1;
  const NUMemoryNet* mn = getMemoryNet();
  if (mn) {
    dimension = mn->getNumDims();
  }
  return dimension;
}

// returns the combined range for the packed dimension(s) of this memory.
// In a simple memory (1packed,1unpacked dims) this is the fastest changing dimension,
// in a resynthesized memory this is the range of the word.
const ConstantRange *NUMemoryNet::getDeclaredWidthRange() const {
  if ( mResynthesized ){
    return &mDeclaredWidthRange;
  } else if (1 ==  getFirstUnpackedDimensionIndex()) {
    // if there is exactly one packed dimension then we can use that dimension for this range (even if memory is not yet resynthesized)
    return getRange(0);
  }
  NU_ASSERT( mResynthesized, this ); // this method only makes sense for resynthesized memories
  return &mDeclaredWidthRange;
}

// returns the combined range for the unpacked dimension(s) of this memory.
// In a simple memory (1packed,1unpacked dims) this is the slowest changing dimension,
// in a resynthesized memory this is the range that covers the words
const ConstantRange *NUMemoryNet::getDeclaredDepthRange() const {
  NU_ASSERT( mResynthesized, this ); // this method only makes sense for resynthesized memories
  return &mDeclaredDepthRange;
}

//! Return the number of words in this memory.
UInt32 NUMemoryNet::getDepth() const {
  NU_ASSERT( mResynthesized  , this ); // this method only makes sense for resynthesized memories
  return mDeclaredDepthRange.getLength();
}

//RJC RC#2013/12/22 (cloutier) this should probably also provide ability to search
// mDeclaredWidthRange & mDeclaredDepthRange if the memory is marked as resynthesized,
// but currently all calls to this method come before MemoryResynth phase.
bool NUMemoryNet::isDeclaredWithNegativeIndex() const
{
  for ( SInt32 idx = getNumDims()-1; idx >= 0; idx-- ){
    const ConstantRange* range = getRange(idx);
    if ( ( range->getLsb() < 0 ) or ( range->getMsb() < 0 ) )
      return true;
  }
  return false;
}

//! construct the range for a net given the normalised offsets
ConstantRange NUNet::makeRange (const ConstantRange &sliced_offsets) const
{
  SInt32 first = sliced_offsets.getMsb ();
  SInt32 last  = sliced_offsets.getLsb ();
  const ConstantRange *range = NULL;
  if (isVectorNet ()) {
    range = castVectorNet ()->getRange();
  } else if (isMemoryNet ()) {
    range = getMemoryNet ()->getWidthRange();
  } else {
    NU_ASSERT (isVectorNet () || isMemoryNet (), this);
  }
  return ConstantRange (range->index (first), range->index (last));
}

HierFlags NUNet::getSourceLanguage() const
{
  // Get the module/architecture for this net and return its source
  // language. Mixed language modules/architectures are not supported.
  NUModule* module = getScope()->getModule();
  return module->getSourceLanguage();
}
