// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUCompositeLvalue.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUScope.h"
#include "util/CbuildMsgContext.h"
#include "util/UtIOStream.h"
#include "util/UtIO.h"
#include "util/UtOrder.h"       // For carbonPtrCompare()
#include "nucleus/NUNetSet.h"
#include "nucleus/NUNetRefSet.h"
#include <cstddef>

NULvalue::~NULvalue()
{
}

bool
NULvalue::sizeVaries () const { return false; }

NUIdentLvalue::NUIdentLvalue(NUNet *net, const SourceLocator& loc) : NULvalue(loc)
{
  mNet = net;
}


NUIdentLvalue::~NUIdentLvalue()
{
}


UInt32 NUIdentLvalue::determineBitSize() const
{
  return mNet->getBitSize();
}

void NUIdentLvalue::getUses(NUNetSet * /* uses -- unused */) const
{
}

void NUIdentLvalue::getUses(NUNetRefSet * /* uses -- unused */) const
{
}


bool NUIdentLvalue::queryUses(const NUNetRefHdl & /* use_net_ref -- unused */,
			      NUNetRefCompareFunction /* fn -- unused */,
			      NUNetRefFactory * /* factory -- unused */) const
{
  return false;
}

void NUIdentLvalue::getUses(const NUNetRefHdl & /* net_ref -- unused */, NUNetRefSet * /* uses -- unused */) const
{
}


bool NUIdentLvalue::queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			      const NUNetRefHdl & /* use_net_ref -- unused */,
			      NUNetRefCompareFunction /* fn -- unused */,
			      NUNetRefFactory * /* factory -- unused */) const
{
  return false;
}


void NUIdentLvalue::getDefs(NUNetSet *defs) const
{
  defs->insert(getIdent());
}


void NUIdentLvalue::getDefs(NUNetRefSet *defs) const
{
  defs->insert(defs->getFactory()->createNetRef(getIdent()));
}


bool NUIdentLvalue::queryDefs(const NUNetRefHdl &def_net_ref,
			      NUNetRefCompareFunction fn,
			      NUNetRefFactory *factory) const
{
  NUNetRefHdl net_ref = factory->createNetRef(getIdent());
  return ((*net_ref).*fn)(*def_net_ref);
}


void NUIdentLvalue::replaceDef(NUNet *old_net, NUNet *new_net)
{
  if (mNet == old_net) {
    mNet = new_net;
  }
}

bool NUIdentLvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  if ( mNet == old_net ) {
    mNet = new_net;
    changed = true;
  }
  return changed;
}

bool NUIdentLvalue::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  NUNet * repl = translator(mNet, NuToNuFn::ePrePost);
  if (repl) {
    mNet = repl;
    changed = true;
  }
  return changed;
}

//! Return true if this lvalue access involves a 2D net
/*virtual*/ bool NUIdentLvalue::is2DAnything () const 
{ 
  return mNet->is2DAnything (); 
}


NUExpr* NUIdentLvalue::NURvalue () const
{
  NUExpr* rvalExpr = new NUIdentRvalue (mNet, mLoc);
  // If this whole net is signed, the expression created out of it will be
  // signed. So, set the sign flag of the created expr.
  if (isWholeIdentifier() and mNet->isSigned())
    rvalExpr->setSignedResult(true);
  return rvalExpr;
}

bool NUIdentLvalue::equalAddr (const NUExpr *rhs) const
{
  const NUIdentRvalue *val = dynamic_cast<const NUIdentRvalue*>(rhs);

  return val && val->getIdent () == getIdent ();
}

void NUIdentLvalue::getCompleteDefs(NUNetSet *defs) const
{
  defs->insert(getIdent());
}


void NUIdentLvalue::getCompleteDefs(NUNetRefSet *defs) const
{
  getDefs(defs);
}


bool NUIdentLvalue::queryCompleteDefs(const NUNetRefHdl &def_net_ref,
				      NUNetRefFactory *factory) const
{
  NUNetRefHdl net_ref = factory->createNetRef(getIdent());
  const NUNetRef& nr = *net_ref;
  return nr.covers(*def_net_ref);
}


const char* NUIdentLvalue::typeStr() const
{
  return "NUIdentLvalue";
}


void NUIdentLvalue::print(bool /* recurse_arg --unused*/, int indent_arg) const
{
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "IdentLvalue(" << this << ") [size=" << getBitSize() << "]" << UtIO::endl;
  mNet->print(false, numspaces+2);
}

bool NUIdentLvalue::operator==(const NULvalue& lval) const
{
  if (getType () != lval.getType ())
    return false;

  const NUIdentLvalue *lv = dynamic_cast<const NUIdentLvalue*>(&lval);
  return (mNet == lv->getIdent ()
	  && getBitSize () == lv->getBitSize ());
}

void NUVarselLvalue::replaceIndex(NUExpr* expr) {
  mExpr = expr;
  fixConstExpr();
}

bool NUVarselLvalue::fixConstExpr (void)
{
  bool ret = false;
  if (isConstIndex ())
  {
    SInt32 off = getConstIndex ();

    if (off)                    // Non-zero?
    {
      mRange.adjust (off);
      bool wasSigned = mExpr->isSignedResult ();
      delete mExpr;
      mExpr = NUConst::create (wasSigned, 0,  32, mLoc);
      ret = true;
    }
    else {
      UInt32 new_size = std::min (32u, mExpr->determineBitSize ());
      mExpr = mExpr->makeSizeExplicit(new_size);
    }
    mIndexInBounds = true;
  }
  else
  {
    mExpr->resize (mExpr->determineBitSize ());
  }
  
  return ret;
}

static void sVarselCheck(const NUNet* net)
{
  // Should use memsel's for memories
  NU_ASSERT(!net->is2DAnything(), net);
}

static void sVarselCheck(const NULvalue* lvalue)
{
  // Should use memsel's for memories. Note that we exclude any
  // expressions but the ident lvalue. But this could be a memsel that
  // does not access enough dimensions. Not sure how to check for that
  // yet.
  if ((lvalue->getType() == eNUIdentLvalue) && lvalue->isWholeIdentifier()) {
    NUNet* net = lvalue->getWholeIdentifier();
  NU_ASSERT(!net->is2DAnything(), lvalue);
  }
}

NUVarselLvalue::NUVarselLvalue(NUNet *net,
                               NUExpr *expr,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(new NUIdentLvalue(net,loc)), mExpr(expr), mRange(range), mIndexInBounds (false)
{
  sVarselCheck(net);
  fixConstExpr ();
  mExpr->resize (mExpr->determineBitSize ());
}

NUVarselLvalue::NUVarselLvalue(NULvalue* lvalue,
                               NUExpr *expr,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(lvalue), mExpr(expr), mRange(range), mIndexInBounds (false)
{
  sVarselCheck(lvalue);
  fixConstExpr ();
}

// Constructor defaulting to [0:0] range
NUVarselLvalue::NUVarselLvalue(NUNet *net,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(new NUIdentLvalue(net,loc)), mExpr(expr), mRange(ConstantRange (0,0)), mIndexInBounds (false)
{
  sVarselCheck(net);
  fixConstExpr ();
  mExpr->resize (mExpr->determineBitSize ());
}

// Constructor defaulting to [0:0] range
NUVarselLvalue::NUVarselLvalue(NULvalue *lvalue,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(lvalue), mExpr(expr), mRange(ConstantRange (0,0)), mIndexInBounds (false)
{
  sVarselCheck(lvalue);
  fixConstExpr ();
}

// Constructor defaulting to zero index
NUVarselLvalue::NUVarselLvalue(NUNet *net,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(new NUIdentLvalue(net,loc)),
  mExpr(NUConst::create (false, 0,  32, loc)),
  mRange(range), mIndexInBounds (true)
{
  sVarselCheck(net);
  mExpr->resize (32);
}

// Constructor defaulting to zero index
NUVarselLvalue::NUVarselLvalue(NULvalue *lvalue,
                               const ConstantRange& range,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(lvalue),
  mExpr(NUConst::create (false, 0,  32, loc)),
  mRange(range), mIndexInBounds (true)
{
  sVarselCheck(lvalue);
  mExpr->resize (32);
}


NUVarselLvalue::~NUVarselLvalue()
{
  delete mIdent;
  delete mExpr;
}


UInt32 NUVarselLvalue::determineBitSize() const
{
  return mRange.getLength ();
}


void NUVarselLvalue::resize()
{
  mExpr->resize(mExpr->determineBitSize());
}

void NUVarselLvalue::putSize(UInt32 bitSize) {
  NU_ASSERT(sizeVaries(), this);
  NU_ASSERT(mRange.getLsb() == 0, this);
  mRange.setMsb(bitSize - 1);
}


bool NUVarselLvalue::isConstIndex() const
{
  int t = mExpr->getType();
  return (t == NUExpr::eNUConstXZ) || (t == NUExpr::eNUConstNoXZ);
}

SInt32 NUVarselLvalue::getConstIndex() const
{
  int index;
  NUConst *c = mExpr->castConst();
  NU_ASSERT(c, this);
  c->getL( &index );
  return index;
}

bool NUVarselLvalue::sizeVaries () const
{
  if (mExpr->castConst ())
    return false;
  else if (const NUBinaryOp* bop = dynamic_cast<const NUBinaryOp*>(mExpr))
    return bop->getOp () == NUOp::eBiDownTo;
  return false;
}


void NUVarselLvalue::getUses(NUNetSet *uses) const
{
  mIdent->getUses(uses);
  mExpr->getUses(uses);
}


void NUVarselLvalue::getUses(NUNetRefSet *uses) const
{
  mIdent->getUses(uses);
  mExpr->getUses(uses);
}


bool NUVarselLvalue::queryUses(const NUNetRefHdl &use_net_ref,
                               NUNetRefCompareFunction fn,
                               NUNetRefFactory *factory) const
{
  return (mExpr->queryUses(use_net_ref, fn, factory) ||
          mIdent->queryUses(use_net_ref, fn, factory));
}

void NUVarselLvalue::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  if (queryDefs(net_ref, &NUNetRef::overlapsSameNet, uses->getFactory()))
  {
    mIdent->getUses(uses);
    mExpr->getUses(uses);
  }
}


bool NUVarselLvalue::queryUses(const NUNetRefHdl &def_net_ref,
                               const NUNetRefHdl &use_net_ref,
                               NUNetRefCompareFunction fn,
                               NUNetRefFactory *factory) const
{
  NUNetRefHdl net_ref;
  if (queryDefs(def_net_ref, fn, factory))
  {
    return (mIdent->queryUses(use_net_ref, fn, factory) ||
            mExpr->queryUses(use_net_ref, fn, factory));
  }
  return false;
}


void NUVarselLvalue::getDefs(NUNetSet *defs) const
{
  mIdent->getDefs(defs);
}


void NUVarselLvalue::getDefs(NUNetRefSet *defs) const
{
  if (isArraySelect()) {
    mIdent->getDefs(defs);
    return;
  }    

  NUNet * net = getIdentNet();

  if (isWholeIdentifier()) {
    defs->insert(defs->getFactory()->createNetRef(net));
    return;
  }

  // Check for non-ident (concat, etc.)
  if (net == NULL) {
    return mIdent->getDefs(defs);
  }
  
  // Bitselect of a scalar, seems to be allowed
  if (not net->isVectorNet()) {
    defs->insert(defs->getFactory()->createNetRef(net));
    return;
  }

  NUVectorNet *vect = dynamic_cast<NUVectorNet*>(net);
  if (isConstIndex ())
  {
    ConstantRange r (mRange);

    // Handle out-of-bounds subscripting (will cause an empty net ref to be created)
    NUNetRefHdl net_ref = defs->getFactory()->createVectorNetRef(vect, r);
    if (not net_ref->empty()) {
      defs->insert(net_ref);
    }
  } else {
    defs->insert(defs->getFactory()->createVectorNetRef(vect));
  }
}


bool NUVarselLvalue::queryDefs(const NUNetRefHdl &def_net_ref,
                               NUNetRefCompareFunction fn,
                               NUNetRefFactory *factory) const
{
  if (isArraySelect()) {
    return mIdent->queryDefs(def_net_ref, fn, factory);
  }

  NUNet * net = getIdentNet();

  // Check for non-ident (concat, etc.)
  if (net == NULL) {
    return mIdent->queryDefs(def_net_ref, fn, factory);
  }
  
  // Bitselect of a scalar, seems to be allowed
  if (not net->isVectorNet()) {
    NUNetRefHdl net_ref = factory->createNetRef(net);
    return ((*net_ref).*fn)(*def_net_ref);
  }

  if (isConstIndex ()) {
    ConstantRange r (mRange);

    NUNetRefHdl net_ref = factory->createVectorNetRef(net, r);
    return ((*net_ref).*fn)(*def_net_ref);
  } else {
    // Mark the whole thing as defined since we can't figure out which bits are defined
    NUNetRefHdl net_ref = factory->createVectorNetRef(net);
    return ((*net_ref).*fn)(*def_net_ref);
  }
}


void NUVarselLvalue::replaceDef(NUNet *old_net, NUNet *new_net)
{
  mIdent->replaceDef(old_net, new_net);
}


bool NUVarselLvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = mIdent->replace(old_net, new_net);
  changed |= mExpr->replace(old_net,new_net);
  return changed;
}

bool NUVarselLvalue::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  
  // Handle ident lvalue
  {
    NULvalue* repl = translator(mIdent, NuToNuFn::ePre);
    if (repl) {
      mIdent = repl;
      changed = true;
    }
  }
  changed |= mIdent->replaceLeaves(translator);
  {
    NULvalue* repl = translator(mIdent, NuToNuFn::ePost);
    if (repl) {
      mIdent = repl;
      changed = true;
    }
  }
  
  // Handle index expression
  {
    NUExpr * repl = translator(mExpr, NuToNuFn::ePre);
    if (repl) {
      mExpr = repl;
      changed = true;
    }
  }
  changed |= mExpr->replaceLeaves(translator);
  {
    NUExpr * repl = translator(mExpr, NuToNuFn::ePost);
    if (repl) {
      mExpr = repl;
      changed = true;
    }
  }
  
  changed |= fixConstExpr ();
  return changed;
}

void NUVarselLvalue::getCompleteDefs(NUNetSet *defs) const
{
  if (isWholeIdentifier()) {
    defs->insert(getIdentNet());
  }
}

void NUVarselLvalue::getCompleteDefs(NUNetRefSet *defs) const
{
  if (isArraySelect()) {
    if (isConstIndex() && (getConstIndex() == 0))
    {
      // if the select covers the entire width range of the ident, we
      // can return a complete def for the row
      NUMemoryNet* mem = getIdentNet(false)->getMemoryNet();
      NU_ASSERT(mem, this);
      if (mRange.getLength() == mem->getBitSize())
        getDefs(defs);
    }
  } else if (isWholeIdentifier() or isConstIndex()) {
    getDefs(defs);
  }
}


bool NUVarselLvalue::queryCompleteDefs(const NUNetRefHdl &def_net_ref,
                                       NUNetRefFactory *factory) const
{
  if (isArraySelect()) {
    if (isConstIndex() && (getConstIndex() == 0))
    {
      // if the select covers the entire width range of the ident, we
      // can return a complete def for the row
      NUMemoryNet* mem = getIdentNet(false)->getMemoryNet();
      NU_ASSERT(mem, this);
      if (mRange.getLength() == mem->getBitSize())
        return queryDefs(def_net_ref, &NUNetRef::covers, factory);    
    }
  } else if (isWholeIdentifier() or isConstIndex ()) {
    return queryDefs(def_net_ref, &NUNetRef::covers, factory);
  }
  return false;
}


NUExpr* NUVarselLvalue::NURvalue () const
{
  CopyContext copy_context(NULL,NULL);
  NUVarselRvalue* v = new NUVarselRvalue (mIdent->NURvalue(),
                                          mExpr->copy (copy_context),
                                          mRange, mLoc);
  v->putIndexInBounds (mIndexInBounds);
  return v;
}

bool NUVarselLvalue::equalAddr (const NUExpr *rhs) const
{
  const NUVarselRvalue *val = dynamic_cast<const NUVarselRvalue*>(rhs);

  // If it isn't the right type or it isn't the same index, they are not equal
  if (!val || !(*(val->getIndex ()) == *(getIndex ())))
    return false;

  // Are the identifiers equal as well?
  if (isArraySelect()) {
    NUMemselLvalue* lmem = dynamic_cast<NUMemselLvalue*>(mIdent);
    const NUMemselRvalue* rmem = dynamic_cast<const NUMemselRvalue*>(val->getIdentExpr());
    if (!lmem || !rmem || !lmem->equalAddr(rmem))
      return false;
  } else if (!val->isWholeIdentifier() || (getIdentNet() != val->getIdent())) {
    return false;
  }

  /*
   * Are we looking at the same bit?  No way to know now, unless it's
   * constant!
   */
  if (not isConstIndex ())
    return false;

  return mRange == *val->getRange ();
}

const char* NUVarselLvalue::typeStr() const
{
  return "NUVarselLvalue";
}


bool NUVarselLvalue::isWholeIdentifier() const
{
  if (not isConstIndex ())
    return false;

  if (isArraySelect())
    return false;

  NUNet* ident = getIdentNet();
  if (!ident)
    return false;

  ConstantRange r (*getRange ());

  if (ident->isVectorNet()) {
    // bit-select of vector-net is whole identifier only when
    // vector-net has one-bit range and the select is the same as
    // that range.
    NUVectorNet * vect = dynamic_cast<NUVectorNet*>(ident);
    return (r == *vect->getRange ());
  } else if (ident->isBitNet()) {
    // bit-select of bit-net is whole identifier only when select
    // is constant 0.
    return (r.getLsb () == 0);
  }

  return false;
}


NUNet *NUVarselLvalue::getWholeIdentifier() const
{
  NU_ASSERT(isWholeIdentifier(), this);
  return getIdentNet();
}

bool NUVarselLvalue::isArraySelect() const
{
  NUMemselLvalue* memsel = dynamic_cast<NUMemselLvalue*>(mIdent);
  return (memsel != NULL);
}

void NUVarselLvalue::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "VarselLvalue(" << this << ") [size=" << getBitSize() << "] : ";
  mRange.print ();
  UtIO::cout () << UtIO::endl;
  mIdent->print(recurse, numspaces+2);
  mExpr->print(recurse, numspaces+2);
}

bool NUVarselLvalue::operator==(const NULvalue& lval) const
{
  if (getType() != lval.getType())
    return false;
 
  const NUVarselLvalue *bs = dynamic_cast<const NUVarselLvalue*>(&lval);
  return ((*mIdent == *(bs->getLvalue()))
          && mRange == *(bs->getRange())
          && (*mExpr == *(bs->getIndex())));
}

NUNet* NUVarselLvalue::getIdentNet(bool noArray) const
{
  NUIdentLvalue* identLvalue = dynamic_cast<NUIdentLvalue*>(mIdent);
  if (identLvalue != NULL)
    return identLvalue->getIdent();

  if (!noArray)
  {
    NUMemselLvalue* memsel = dynamic_cast<NUMemselLvalue*>(mIdent);
    if (memsel != NULL)
      return memsel->getIdent();
  }

  return NULL;
}

NUConcatLvalue::NUConcatLvalue(const NULvalueVector& lvalues,
			       const SourceLocator& loc) :
  NULvalue(loc),
  mLvalues(lvalues)
{}


NUConcatLvalue::~NUConcatLvalue()
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    delete *iter;
  }
}


UInt32 NUConcatLvalue::getNumArgs() const
{
  return mLvalues.size();
}


NULvalue *NUConcatLvalue::getArg(UInt32 index) const
{
  NU_ASSERT(index < mLvalues.size(), this);
  return mLvalues[index];
}


NULvalue *NUConcatLvalue::setArg(UInt32 index, NULvalue * argument) 
{
  NU_ASSERT(index < mLvalues.size(), this);
  NULvalue * old_argument = getArg(index);
  mLvalues[index] = argument;
  return old_argument;
}


UInt32 NUConcatLvalue::determineBitSize() const
{
  UInt32 total = 0;
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    total += (*iter)->determineBitSize();
  }
  return total;
}


void NUConcatLvalue::resize()
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->resize();
  }
}


void NUConcatLvalue::getUses(NUNetSet *uses) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->getUses(uses);
  }
}


void NUConcatLvalue::getUses(NUNetRefSet *uses) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->getUses(uses);
  }
}


bool NUConcatLvalue::queryUses(const NUNetRefHdl &use_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    if ((*iter)->queryUses(use_net_ref, fn, factory)) {
      return true;
    }
  }
  return false;
}

void NUConcatLvalue::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->getUses(net_ref, uses);
  }
}

bool NUConcatLvalue::queryUses(const NUNetRefHdl &def_net_ref,
			       const NUNetRefHdl &use_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    if ((*iter)->queryUses(def_net_ref, use_net_ref, fn, factory)) {
      return true;
    }
  }
  return false;
}


void NUConcatLvalue::getDefs(NUNetSet *defs) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->getDefs(defs);
  }
}


void NUConcatLvalue::getDefs(NUNetRefSet *defs) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->getDefs(defs);
  }
}


bool NUConcatLvalue::queryDefs(const NUNetRefHdl &def_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    if ((*iter)->queryDefs(def_net_ref, fn, factory)) {
      return true;
    }
  }
  return false;
}


void NUConcatLvalue::replaceDef(NUNet *old_net, NUNet *new_net)
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->replaceDef(old_net, new_net);
  }
}


bool NUConcatLvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    changed |= (*iter)->replace(old_net, new_net);
  }
  return changed;
}

bool NUConcatLvalue::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  for (NULvalueVector::iterator iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    NULvalue * repl = translator((*iter), NuToNuFn::ePre);
    if (repl) {
      (*iter) = repl;
      changed = true;
    }
    changed |= (*iter)->replaceLeaves(translator);
  }

  for (NULvalueVector::iterator iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    NULvalue * repl = translator((*iter), NuToNuFn::ePost);
    if (repl) {
      (*iter) = repl;
      changed = true;
    }
  }

  return changed;
}

void NUConcatLvalue::getCompleteDefs(NUNetSet *defs) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->getCompleteDefs(defs);
  }
}


void NUConcatLvalue::getCompleteDefs(NUNetRefSet *defs) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    (*iter)->getCompleteDefs(defs);
  }
}


bool NUConcatLvalue::queryCompleteDefs(const NUNetRefHdl &def_net_ref,
				       NUNetRefFactory *factory) const
{
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    if ((*iter)->queryCompleteDefs(def_net_ref, factory)) {
      return true;
    }
  }
  return false;
}

NUExpr* NUConcatLvalue::NURvalue () const
{
  NUExprVector srcs;
  for (NULvalueVectorIter dests = mLvalues.begin ();
       dests != mLvalues.end ();
       ++dests)
    srcs.push_back ((*dests)->NURvalue ());

  // Construct a concatop
  return new NUConcatOp (srcs, 1, getLoc ());
}

NUExpr* NUConcatLvalue::NURvalue (const SourceLocator &loc) const
{
  NUExprVector srcs;
  for (NULvalueVectorIter dests = mLvalues.begin ();
       dests != mLvalues.end ();
       ++dests)
    srcs.push_back ((*dests)->NURvalue ());

  // Construct a concatop
  return new NUConcatOp (srcs, 1, loc);
}

bool NUConcatLvalue::isWholeIdentifier() const
{
  // For now, be conservative, just handle 1 element concats.
  NULvalueVectorIter first = mLvalues.begin();

  if (first != mLvalues.end()) {
    NULvalueVectorIter second = first;
    ++second;
    if (second == mLvalues.end()) {
      return (*first)->isWholeIdentifier();
    }
  }
  return false;
}

/*virtual*/ bool NUConcatLvalue::is2DAnything() const
{
  // For now, be conservative, just handle 1 element concats.
  for (NULvalueVectorIter it = mLvalues.begin(); it != mLvalues.end (); ++it) {
    if ((*it)->is2DAnything ()) {
      return true;
    }
  }
  return false;
}

NUNet *NUConcatLvalue::getWholeIdentifier() const
{
  NU_ASSERT(isWholeIdentifier(), this);
  NULvalueVectorIter first = mLvalues.begin();
  return (*first)->getWholeIdentifier();
}

void NUConcatLvalue::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "ConcatLvalue(" << this << ") [size=" << getBitSize() << "]" << UtIO::endl;
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {

    (*iter)->print(recurse, numspaces+2);
  }
}

void NUConcatLvalue::compose(UtString* buf, const STBranchNode* scope, int, bool recurse) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  *buf << "{";
  for(NULvalueVectorIter iter = mLvalues.begin ();;)
    {
      (*iter)->compose(buf, scope, 0, recurse);
      ++iter;
      if (iter == mLvalues.end ())
	break;
      *buf << ",";
    }
  *buf << "}";
}

const char* NUConcatLvalue::typeStr() const
{
  return "NUConcatLvalue";
}


NUMemselLvalue::NUMemselLvalue(NUNet *net,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(new NUIdentLvalue(net,loc)), mResynthesized(false)
{
  NU_ASSERT(net->is2DAnything(), this);
  NU_ASSERT((expr->determineBitSize() <= 32), expr); // cbuild only supports index expressions of 32 bits or less
  mExprs.push_back(expr);
}


NUMemselLvalue::NUMemselLvalue(NULvalue *identifier,
                               NUExpr *expr,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(identifier), mResynthesized(false)
{
  NU_ASSERT(identifier->isWholeIdentifier(),this);
  NUNet * ident_net = identifier->getWholeIdentifier();
  NU_ASSERT(ident_net->is2DAnything(),this);
  NU_ASSERT((expr->determineBitSize() <= 32), expr); // cbuild only supports index expressions of 32 bits or fewer
  mExprs.push_back(expr);
}


NUMemselLvalue::NUMemselLvalue(NUNet *net,
                               NUExprVector *exprs,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(new NUIdentLvalue(net,loc)), mResynthesized(false)
{
  NU_ASSERT(net->is2DAnything(), this);
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
}


NUMemselLvalue::NUMemselLvalue(NULvalue *identifier,
                               NUExprVector *exprs,
                               const SourceLocator& loc) :
  NULvalue(loc), mIdent(identifier), mResynthesized(false)
{
  NU_ASSERT(identifier->isWholeIdentifier(),this);
  NUNet * ident_net = identifier->getWholeIdentifier();
  NU_ASSERT(ident_net->is2DAnything(),this);
  for (NUExprVectorIter iter = exprs->begin(); iter != exprs->end(); ++iter) {
    NU_ASSERT(((*iter)->determineBitSize() <= 32), (*iter)); // cbuild only supports index expressions of 32 bits or fewer
  }
  mExprs.insert( mExprs.begin(), exprs->begin(), exprs->end() );
}


NUMemselLvalue::~NUMemselLvalue()
{
  delete mIdent;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    delete mExprs[i];
  }
}


bool NUMemselLvalue::isConstIndex(UInt32 dim) const
{
  NUExpr::Type type = mExprs[dim]->getType();
  return (type == NUExpr::eNUConstXZ) || (type == NUExpr::eNUConstNoXZ);
}

//! prepend (Add) another index expression to the memsel
void
NUMemselLvalue::prependIndexExpr( NUExpr *expr )
{
  mExprs.insert( mExprs.begin(), expr );
}
//! Add another index expression to the memsel
void
NUMemselLvalue::addIndexExpr( NUExpr *expr )
{
  mExprs.push_back( expr );
}

NUExpr* NUMemselLvalue::NURvalue () const
{
  CopyContext copy_context(NULL,NULL);
  NUExprVector newVec;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    newVec.push_back( mExprs[i]->copy( copy_context ));
  }
  NUMemselRvalue *ret = new NUMemselRvalue (mIdent->NURvalue(),
                                            &newVec, mLoc);
  ret->putResynthesized( mResynthesized );
  return ret;
}

bool NUMemselLvalue::equalAddr (const NUExpr *rhs) const
{
  NU_ASSERT( isResynthesized(), this );
  const NUMemselRvalue *val = dynamic_cast<const NUMemselRvalue*>(rhs);

  if (not val
      || (val->getIdent () != getIdent ()))
    return false;

  // Can only compare constant expressions...
  NUConst *constant = mExprs[0]->castConst();
  const NUConst *cval = val->getIndex(0)->castConst();
  SInt64 lval, rval;
  if (not constant
      || not cval
      || not constant->getLL (&lval)
      || not cval->getLL (&rval)
      ||(rval != lval))
    return false;
      
  return true;
}

UInt32 NUMemselLvalue::determineBitSize() const
{
  const NUNet * ident_net = mIdent->getWholeIdentifier();
  NU_ASSERT(ident_net,this);
  const NUMemoryNet* mem = ident_net->getMemoryNet ();
  NU_ASSERT (mem, this);

  if (mem->isResynthesized ())
    // No longer slicing, all memsels are one-dimensional
    return mem->getRowSize ();

  // this might be a slice; the size is the product of the indices
  // that aren't referenced in the Memsel

  UInt32 size = 1;
  for(UInt32 numDim = mem->getNumDims () - mExprs.size ();
      numDim > 0;
      --numDim)
    size *= mem->getRangeSize (numDim-1);

  return size;
}


void NUMemselLvalue::resize()
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->resize(mExprs[i]->determineBitSize());
  }
}


void NUMemselLvalue::getUses(NUNetSet *uses) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
}


void NUMemselLvalue::getUses(NUNetRefSet *uses) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
}


bool NUMemselLvalue::queryUses(const NUNetRefHdl &use_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    if ( mExprs[i]->queryUses(use_net_ref, fn, factory))
      return true;
  }
  return false;
}

void NUMemselLvalue::getUses(const NUNetRefHdl & /* net_ref -- unused */, NUNetRefSet *uses) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->getUses(uses);
  }
}


bool NUMemselLvalue::queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
			       const NUNetRefHdl &use_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    if ( mExprs[i]->queryUses(use_net_ref, fn, factory))
      return true;
  }
  return false;
}


void NUMemselLvalue::getDefs(NUNetSet *defs) const
{
  defs->insert(getIdent());
}


void NUMemselLvalue::getDefs(NUNetRefSet *defs) const
{
  if (isWholeIdentifier()) {
    defs->insert(defs->getFactory()->createNetRef(getIdent()));
    return;
  }

  // NetRefs can only handle one dimension of reference; for a memory
  // this is the outermost dimension, stored in mExprs[0].  This is also
  // the only dimension in a multidim netref that can be guaranteed to
  // exist.
  NUConst *constant = mExprs[0]->castConst();
  if (constant != 0) {
    SInt64 val;
    bool ok = constant->getLL(&val);
    NU_ASSERT(ok, this);

    // Handle out-of-bounds subscripting (will cause an empty net ref to be created)
    NUNetRefHdl net_ref = defs->getFactory()->createMemoryNetRef(getIdent(),
                                                                 (SInt32)val);
    if (not net_ref->empty()) {
      defs->insert(net_ref);
    }
  }
  else {
    // With a non-constant index, create a netref to the whole memory
    defs->insert(defs->getFactory()->createMemoryNetRef(getIdent()));
  }
}


bool NUMemselLvalue::queryDefs(const NUNetRefHdl &def_net_ref,
			       NUNetRefCompareFunction fn,
			       NUNetRefFactory *factory) const
{
  NU_ASSERT( isResynthesized(), this );
  NUConst *constant = mExprs[0]->castConst();
  if (constant != 0) {
    SInt64 val;
    bool ok = constant->getLL(&val);
    NU_ASSERT(ok, this);
    
    NUNetRefHdl net_ref = factory->createMemoryNetRef(getIdent(), (SInt32)val);
    return ((*net_ref).*fn)(*def_net_ref);
  }
  else {
    NUNetRefHdl net_ref = factory->createMemoryNetRef(getIdent());
    return ((*net_ref).*fn)(*def_net_ref);
  }
}


void NUMemselLvalue::replaceDef(NUNet *old_net, NUNet *new_net)
{
  mIdent->replaceDef(old_net,new_net);
}

bool NUMemselLvalue::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = mIdent->replace(old_net, new_net);
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    changed |= mExprs[i]->replace(old_net,new_net);
  }
  return changed;
}

bool NUMemselLvalue::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;

  // Handle ident lvalue
  {
    NULvalue * repl = translator(mIdent, NuToNuFn::ePre);
    if (repl) {
      mIdent = repl;
      changed = true;
    }
  }
  changed |= mIdent->replaceLeaves(translator);
  {
    NULvalue * repl = translator(mIdent, NuToNuFn::ePost);
    if (repl) {
      mIdent = repl;
      changed = true;
    }
  }

  // Handle index expressions
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    NUExpr *repl = translator(mExprs[i], NuToNuFn::ePre);
    if (repl) {
      mExprs[i] = repl;
      changed = true;
    }
    changed |= mExprs[i]->replaceLeaves(translator);
    repl = translator(mExprs[i], NuToNuFn::ePost);
    if (repl) {
      mExprs[i] = repl;
      changed = true;
    }
  }

  return changed;
}

void
NUMemselLvalue::replaceIndex(UInt32 dim, NUExpr* expr)
{
  mExprs[dim] = expr;
}


//! Return true if this lvalue access a single, complete, identifier, false in all other cases
bool NUMemselLvalue::isWholeIdentifier() const
{
  NUNet* ident = getIdent();
  NUMemoryNet *mem = ident->getMemoryNet();
  NU_ASSERT( mem, this );
  const bool memIsResynthesized = mem->isResynthesized();
  const UInt32 numDims = mExprs.size();

  // first some quick checks, if the number of index expressions is greater than the number of dimensions on
  // the memory then something other than the whole identifier is being selected and we can return false immediately
  if ( memIsResynthesized && (numDims > 2 ) ) return false; // resynthized memory with more than 2 select expressions
  if ( !memIsResynthesized && (numDims > mem->getNumDims() )) return false; // non resynthesized memory and more select expressions than dimensions in memory
  // now find any other reason that this is not a whole identifier (and return false)
  for ( UInt32 i = 0; i < numDims; ++i ) {
    NUConst *constant = mExprs[i]->castConst();
    if ( NULL == constant ){
      return false;        // each select expression must be constants in order for this to address whole identifier
    }

    SInt64 val;
    bool ok = constant->getLL(&val);
    NU_ASSERT(ok, this);
    SInt32 sval = static_cast<SInt32>(val);

    const ConstantRange range(sval, sval);
    
    if ( memIsResynthesized ){
      switch ( i ){
      case 0:{
        if (*(mem->getDepthRange()) != range) {
          return false;
        }
      }
      case 1:{
        if (*(mem->getWidthRange()) != range) {
          return false;
        }
      }
      default:{return false;}      // for a resynthesized memory, anything past the second index expression is selecting something other than the whole identifier
      }
    } else {
      // the memory is not marked as being resynthesized yet,
      // so we must check the declared range of the memory for the current index position with the index expression expression (range) of
      // this NUMemselLvalue, (note NOT the normalized dimensions).
      if ( *(mem->getRange(i)) != range ){
        return false;
      }
    }
  }
  return true;
}


NUNet *NUMemselLvalue::getWholeIdentifier() const
{
  bool isWhole = isWholeIdentifier();
  NU_ASSERT(isWhole, this);
  return getIdent();
}


void NUMemselLvalue::getCompleteDefs(NUNetSet *defs) const
{
  if (isWholeIdentifier()) {
    defs->insert(getIdent());
  }
}


void NUMemselLvalue::getCompleteDefs(NUNetRefSet *defs) const
{
  NU_ASSERT( isResynthesized(), this );
  bool is_const_sel = (mExprs[0]->castConst() != 0);
  if (isWholeIdentifier() or is_const_sel) {
    getDefs(defs);
  }
}


bool NUMemselLvalue::queryCompleteDefs(const NUNetRefHdl &def_net_ref,
				       NUNetRefFactory *factory) const
{
  if (isWholeIdentifier()) {
    NUNetRefHdl net_ref = factory->createNetRef(getIdent());
    return net_ref->covers(*def_net_ref);
  }
  return false;
}


const char* NUMemselLvalue::typeStr() const
{
  return "NUMemselLvalue";
}


void NUMemselLvalue::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "MemselLvalue(" << this << ") [size=" << getBitSize() << "]" << UtIO::endl;
  mIdent->print(recurse, numspaces+2);
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs[i]->print(recurse, numspaces+2);
  }
}

void NUMemselLvalue::compose(UtString* buf, const STBranchNode* scope, int, bool) const
{
  if ( mLoc.isTicProtected() ){
     composeProtectedNUObject(mLoc, buf);
    return; 
  }
  bool  showRoot = false;  // normally do not include root in name.
  if ( scope == NULL )
  {
    NUScope* actualScope = getIdent()->getScope();
    NUScope::ScopeT typeOfScope = actualScope->getScopeType();
    if ( typeOfScope == NUScope::eNamedDeclarationScope ) {
      showRoot = true;          // show local root
    }
  }
  getIdent()->compose(buf, scope, showRoot); // conditionally include root in name.
  for ( UInt32 i = 0; i < mExprs.size(); ++i )
  {
    *buf << "[";
    mExprs[i]->compose(buf, scope, false); 
    *buf << "]";
  }
}

// hash function for making canonical sets of these.  This is currently a
// little pessimistic due to the lack of a hash/operator== method for expressions,
// but mostly we expect memory addresses to be expressed as a single net.
size_t NUMemselLvalue::hash() const
{
  NU_ASSERT( isResynthesized(), this );
  NUNet * ident_net = mIdent->getWholeIdentifier();
  NUIdentRvalue* addr = dynamic_cast<NUIdentRvalue*>(mExprs[0]);
  if ((addr == NULL) || !addr->isWholeIdentifier())
    return ((size_t) ident_net) + ((size_t) mExprs[0]);
  // If a simple address, we can hash that more easily
  return ((size_t) ident_net) + ((size_t) addr->getIdent());
}

size_t NUVarselLvalue::hash() const
{
  return mIdent->hash () + mExpr->hash () + mRange.hash ();
}

size_t NUIdentLvalue::hash() const
{
  return ((size_t) mNet);
}

size_t NUConcatLvalue::hash() const
{
  size_t hval = 0;

  for (NULvalueVectorIter iter = mLvalues.begin ();
       iter != mLvalues.end ();
       ++iter)
    hval += (*iter)->hash ();

  return hval;
}


//! comparison function for making canonical sets of these
bool NUMemselLvalue::operator==(const NULvalue& other) const
{
  NU_ASSERT( isResynthesized(), this );
  if (getType () != other.getType ())
    return false;

  const NUMemselLvalue* oth = dynamic_cast<const NUMemselLvalue*>(&other);

  return ((*getIdentLvalue() == *oth->getIdentLvalue()) and
          (*getIndex(0) == *oth->getIndex(0)));
}

//! comparison function for making canonical sets of these
bool NUMemselLvalue::operator<(const NUMemselLvalue& other) const
{
  return compare(this, &other) < 0;
}

//! comparison function for making canonical sets of these
ptrdiff_t NUMemselLvalue::compare(const NUMemselLvalue* m1, const NUMemselLvalue* m2)
{
  NU_ASSERT( m1->isResynthesized(), m1 );
  NU_ASSERT( m2->isResynthesized(), m2 );
  if (m1->getIdent() == m2->getIdent())
  {
    if (*m1->mExprs[0] == *m2->mExprs[0])
      // Completely and trivially identical
      return 0;
    else
      // Can't tell for sure, but give stable sort order on address
      return carbonPtrCompare(m1->mExprs[0], m2->mExprs[0]);
  }
  else
    // Not same memory, sort consistently
    return NUNet::compare (m1->getIdent(), m2->getIdent());

} // int NUMemselLvalue::compare

NUMemselLvalue::NUMemselLvalue(const NUMemselLvalue& src):
  NULvalue(src.mLoc)
{
  CopyContext copy_context(NULL,NULL);
  mIdent = src.mIdent->copy(copy_context);
  const UInt32 numDims = src.mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    mExprs.push_back( src.mExprs[i]->copy(copy_context));
  }
  mResynthesized = src.mResynthesized;
}

NUMemselLvalue& NUMemselLvalue::operator=(const NUMemselLvalue& src)
{
  if (&src != this)
  {
    if (mIdent != NULL)
      delete mIdent;
    if (!mExprs.empty())
    {
      const UInt32 numDims = mExprs.size();
      for ( UInt32 i = 0; i < numDims; ++i )
      {
        delete mExprs[i];
      }
      mExprs.erase( mExprs.begin(), mExprs.end() );
    }
    CopyContext copy_context(NULL,NULL);
    mIdent = src.mIdent->copy(copy_context);
    const UInt32 numDims = src.mExprs.size();
    for ( UInt32 i = 0; i < numDims; ++i )
    {
      mExprs.push_back( src.mExprs[i]->copy(copy_context));
    }
    mLoc = src.mLoc;
  }
  return *this;
}


void MsgContext::composeLocation(NULvalue* lvalue, UtString* location)
{
  if (lvalue == 0) {
    (*location) << "(null NULvalue)";
  } else {
    const SourceLocator& loc = lvalue->getLoc();
    UtString source;
    loc.compose(&source);
    (*location) << source;
  }
}

const NUModule* NULvalue::findParentModule() const
{
  NUNetSet nets;
  getDefs(&nets);
  NUNet* net = *nets.begin();
  return net->getScope()->getModule();
}

NUModule* NULvalue::findParentModule()
{
  NUNetSet nets;
  getDefs(&nets);
  NUNet* net = *nets.begin();
  return net->getScope()->getModule();
}

NULvalue* NUIdentLvalue::copy(CopyContext & copy_context) const {
  NUNet * net = copy_context.lookup(mNet);
  return new NUIdentLvalue(net, mLoc);
}

NULvalue* NUVarselLvalue::catenate (const NUVarselLvalue* v, CopyContext& cc) const
{
  if (not ((*mIdent) == *(v->getLvalue ())))
    return 0;                   // Different objects
  if (not mExpr->equal (*(v->getIndex ())))
    // Could optimize of they are off-by-one...
    return 0;                   // different variable part. 

  ConstantRange vRange = *v->getRange ();
  if (vRange.getMsb () + 1 != mRange.getLsb ())
    return 0;

  vRange.setMsb (mRange.getMsb ());
  return new NUVarselLvalue (mIdent->copy (cc),
                             mExpr->copy (cc),
                             vRange,
                             mLoc);
}

NULvalue* NUVarselLvalue::copy(CopyContext &copy_context) const {
  NUVarselLvalue* v = new NUVarselLvalue(mIdent->copy(copy_context),
                                         mExpr->copy(copy_context),
                                         mRange, mLoc);
  v->mIndexInBounds = mIndexInBounds;
  return v;
}

NULvalue* NUConcatLvalue::copy(CopyContext &copy_context) const {
  NULvalueVector mLvalues_copy;
  for (NULvalueVectorIter iter = mLvalues.begin();
       iter != mLvalues.end();
       ++iter) {
    NULvalue * lvalue = (*iter);
    mLvalues_copy.push_back( lvalue->copy(copy_context) );
  }
  return new NUConcatLvalue(mLvalues_copy, mLoc);
}

bool NUConcatLvalue::operator==(const NULvalue& lvalue) const
{
  if (getType () != lvalue.getType ())
    return false;

  const NUConcatLvalue *concatLvalue = dynamic_cast<const NUConcatLvalue*>(&lvalue);

  // Can't have constants in LHS, so don't need to check sizes here...
  return ObjListEqual<NULvalueCLoop> (loopLvalues (),
				      concatLvalue->loopLvalues () );
}


NULvalue* NUMemselLvalue::copy(CopyContext &copy_context) const {
  NUExprVector newVec;
  const UInt32 numDims = mExprs.size();
  for ( UInt32 i = 0; i < numDims; ++i )
  {
    newVec.push_back( mExprs[i]->copy(copy_context));
  }
  NUMemselLvalue *ret = new NUMemselLvalue(mIdent->copy(copy_context), &newVec, mLoc);
  ret->putResynthesized( mResynthesized );
  return ret;
}

void NUIdentLvalue::compose(UtString *buf, const STBranchNode* scope, int, bool) const
{
  if ( mLoc.isTicProtected() ){
     composeProtectedNUObject(mLoc,buf);
    return; 
  }
  bool  showRoot = false;  // normally do not include root in name (test/schedule/cycle1)
  if ( scope == NULL )
  {
    NUScope* actualScope = getIdent()->getScope();
    NUScope::ScopeT typeOfScope = actualScope->getScopeType();
    if ( typeOfScope == NUScope::eNamedDeclarationScope ) {
      showRoot = true;          // show local root (test/fold/nbtest)
    }
  }
  getIdent()->compose(buf, scope, showRoot);
}

bool NUIdentLvalue::isHierRef() const
{
  return mNet->isHierRef();
}


bool NUIdentLvalue::isReal() const
{
  return mNet->isReal();
}

void NUVarselLvalue::printRange(UtString* buf, ConstantRange r, bool isConst)
{
  if (isConst)
  {
    // Print V[x:y] or V[x]
    *buf << r.getMsb () ;
    if (r.getLength () > 1)
      *buf << ":" << r.getLsb ();
  }
  else
  {
    // Print V[k] or V[k +: w]
    if (r.getLsb () != 0)
      // Need to print some range info, otherwise, it should
      // just look like a bitsel
      *buf << "+" << r.getLsb ();

    if (r.getLength () > 1)
      *buf << " +: " << r.getLength ();
  }
}

void NUVarselLvalue::compose(UtString *buf, const STBranchNode* scope, int, bool) const
{
  if ( mLoc.isTicProtected() ){
    composeProtectedNUObject(mLoc, buf);
    return; 
  }
  bool  showRoot = false;  // normally do not include root in name.
  if ( scope == NULL )
  {
    NUNet* net = getIdentNet(false);
    if (net)
    {
      NUScope* actualScope = net->getScope();
      NUScope::ScopeT typeOfScope = actualScope->getScopeType();
      if ( typeOfScope == NUScope::eNamedDeclarationScope ) {
        showRoot = true;          // show local root
      }
    }
  }
  // conditionally include root in name (test/localcse/cse3,test/cycle/ati_cycle)
  getLvalue()->compose (buf, scope, 0, showRoot); 
  *buf << "[";

  CopyContext cc(0,0);
  ConstantRange declaredRange;
  NUExpr* denormalizedSelect = NULL;
  if (isArraySelect())
  {
    NUMemoryNet* mem = getIdentNet(false)->getMemoryNet();
    NU_ASSERT(mem, this);
    declaredRange = (mem->isResynthesized() ? *(mem->getDeclaredWidthRange()) : *(mem->getRange(0)));
    denormalizedSelect = mem->denormalizeSelectExpr(getIndex()->copy(cc), 0);
  }
  else if (NUVectorNet* vn = dynamic_cast<NUVectorNet*>(getIdentNet()))
  {
    declaredRange = *(vn->getDeclaredRange());
    denormalizedSelect = vn->denormalizeSelectExpr(getIndex()->copy(cc));
  }
  else
  {
    // a select of a something besides a memory or vector
    declaredRange.setLsb(0);
    declaredRange.setMsb(mIdent->determineBitSize() - 1);
    denormalizedSelect = getIndex()->copy(cc);
  }

  ConstantRange r (mRange);
  bool isConst = isConstIndex () && getConstIndex () == 0;
  if (isConst)
  {
    r.denormalize(&declaredRange, false);
  }
  else
  {
    denormalizedSelect->compose(buf, scope, false);
  }

  printRange(buf, r, isConst);

  if (denormalizedSelect)
    delete denormalizedSelect;

  *buf << "]";
}

bool NUVarselLvalue::isHierRef() const
{
  return mIdent->isHierRef();
}

bool NUConcatLvalue::isHierRef() const
{
  return isWholeIdentifier() && getWholeIdentifier()->isHierRef();
}

bool NUMemselLvalue::isHierRef() const
{
  return mIdent->isHierRef();
}


ptrdiff_t
NULvalue::compare( const NULvalue &lval, bool cmpLocator, bool cmpPointer ) const
{
  ptrdiff_t cmp = 0;
  if ( &lval != this )
  {
    if ( cmpLocator )
    {
      cmp = SourceLocator::compare( getLoc(), lval.getLoc() );
    }
    if ( cmp == 0 )
    {
      cmp = ((ptrdiff_t) getType() - (ptrdiff_t) lval.getType());      
    }
    if ( cmp == 0 )
    {
      // compareHelper is abstract virtual method overridden by each subclass
      cmp = compareHelper( &lval, cmpLocator, cmpPointer );      
    }
    if (( cmp == 0 ) && cmpPointer )
    {
      cmp = carbonPtrCompare(this, &lval);
    }
  }
  return cmp;
}


ptrdiff_t
NUIdentLvalue::compareHelper( const NULvalue *lval, bool, bool ) const
{
  const NUIdentLvalue *that = dynamic_cast<const NUIdentLvalue*>( lval );
  NU_ASSERT( that, lval );
  return NUNet::compare( getIdent(), that->getIdent() );
}


ptrdiff_t
NUVarselLvalue::compareHelper( const NULvalue *lval, bool cmpLocator,
                               bool cmpPointer ) const
{
  const NUVarselLvalue *that = dynamic_cast<const NUVarselLvalue*>( lval );
  NU_ASSERT( that, lval );
  ptrdiff_t cmp = getLvalue()->compare( *that->getLvalue(), cmpLocator,
                                       cmpPointer );
  if ( cmp == 0 )
  {
    cmp = getIndex()->compare( *that->getIndex(), cmpLocator, cmpPointer, false );
  }
  if ( cmp == 0 )
  {
    cmp = getRange()->compare( *that->getRange() );
  }
  return cmp;
}

bool NUVarselLvalue::isIndexInBounds () const
{
  if (mIndexInBounds)
    return true;

  if (isConstIndex ())
    return true;

  return false;
}

void NUVarselLvalue::putIndexInBounds (bool b)
{
  mIndexInBounds = b;
}
  

ptrdiff_t
NUConcatLvalue::compareHelper( const NULvalue *lval, bool cmpLocator,
                               bool cmpPointer ) const
{
  const NUConcatLvalue *that = dynamic_cast<const NUConcatLvalue*>( lval );
  NU_ASSERT( that, lval );
  const UInt32 size = getNumArgs();
  ptrdiff_t cmp = 0;
  for ( UInt32 i = 0; (cmp == 0) && (i < size); ++i )
  {
    cmp = getArg(i)->compare( *that->getArg(i), cmpLocator, cmpPointer );
  }
  return cmp;
}


ptrdiff_t
NUMemselLvalue::compareHelper( const NULvalue *lval, bool cmpLocator,
                               bool cmpPointer ) const
{
  const NUMemselLvalue *that = dynamic_cast<const NUMemselLvalue*>( lval );
  NU_ASSERT( that, lval );
  ptrdiff_t cmp =  NUNet::compare( getIdent(), that->getIdent() );
  if ( cmp == 0 )
  {
    const UInt32 size = getNumDims();
    for ( UInt32 i = 0; (cmp == 0) && (i < size); ++i )
    {
      cmp = getIndex(i)->compare( *that->getIndex(i), cmpLocator, cmpPointer, false );
    }
  }
  if ( cmp == 0 )
  {
    cmp = isResynthesized() - that->isResynthesized();
  }
  return cmp;
}

