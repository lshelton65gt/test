// -*-C++-*-
/******************************************************************************
 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  TBD
*/

#include "util/CarbonTypes.h"
#include "util/UtString.h"
#include "nucleus/NUCModelPort.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUScope.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModel.h"
#include "util/UtIOStream.h"
#include "nucleus/NUNetSet.h"


UInt32 NUCModelArgConnection::getBitSize() const 
{
  return mFormal->getBitSize(this->getCModelCall()->getCModel()->getVariant());
}


int NUCModelArgConnection::compare(const NUCModelArgConnection* other) const
{
  // Make sure they are the same direction, this saves on a string compare
  int cmp = (int)getDir() - (int)other->getDir();
  if (cmp == 0)
    // Now compare by names, they should be unique
    cmp = mFormal->compare(other->mFormal);
  return cmp;
}

void NUCModelArgConnectionInput::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "CModelArgConnectionInput(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal->getName()->c_str() << ")"
	       << " actual(" << mActual << ")" << UtIO::endl;
  mActual->print(recurse, numspaces+2);
}


void NUCModelArgConnectionOutput::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "CModelArgConnectionOutput(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal->getName()->c_str() << ")"
	       << " actual(" << mActual << ")" << UtIO::endl;
  mActual->print(recurse, numspaces+2);
}


void NUCModelArgConnectionBid::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << "CModelArgConnectionBid(" << this << ")  ";
  UtIO::cout() << "  formal(" << mFormal->getName()->c_str() << ")"
	       << " actual(" << mActual << ")" << UtIO::endl;
  mActual->print(recurse, numspaces+2);
}

void NUCModelArgConnectionInput::compose(UtString *buf, const STBranchNode *scope, int indent, bool recurse) const
{
  (void) recurse;
  buf->append(indent, ' ');  
  mActual->compose(buf, scope);
}

void NUCModelArgConnectionOutput::compose(UtString *buf, const STBranchNode *scope, int indent, bool recurse) const
{
  mActual->compose(buf, scope, indent, recurse);
}

void NUCModelArgConnectionBid::compose(UtString *buf, const STBranchNode *scope, int indent, bool recurse) const
{
  mActual->compose(buf, scope, indent, recurse);
}

NUCModelArgConnectionInput::~NUCModelArgConnectionInput()
{
  delete mActual;
}


NUCModelArgConnectionOutput::~NUCModelArgConnectionOutput()
{
  delete mActual;
}


NUCModelArgConnectionBid::~NUCModelArgConnectionBid()
{
  delete mActual;
}


void NUCModelArgConnectionInput::getUses(NUNetSet *uses) const
{
  mActual->getUses(uses);
}


void NUCModelArgConnectionInput::getUses(NUNetRefSet *uses) const
{
  mActual->getUses(uses);
}


bool NUCModelArgConnectionInput::queryUses(const NUNetRefHdl &use_net_ref,
					   NUNetRefCompareFunction fn,
					   NUNetRefFactory *factory) const
{
  return mActual->queryUses(use_net_ref, fn, factory);
}


void NUCModelArgConnectionInput::getUses(NUNet * /* net -- unused */, NUNetSet *uses) const
{
  mActual->getUses(uses);
}


void NUCModelArgConnectionInput::getUses(const NUNetRefHdl & /* net_ref -- unused */,
					 NUNetRefSet *uses) const
{
  mActual->getUses(uses);
}


bool NUCModelArgConnectionInput::queryUses(const NUNetRefHdl & /* def_net_ref -- unused */,
					   const NUNetRefHdl &use_net_ref,
					   NUNetRefCompareFunction fn,
					   NUNetRefFactory *factory) const
{
  return mActual->queryUses(use_net_ref, fn, factory);
}


void NUCModelArgConnectionInput::getDefs(NUNetSet* /* defs */) const
{
  // Invalid
  NU_ASSERT(0, this);
}


void NUCModelArgConnectionInput::getDefs(NUNetRefSet* /* defs */) const
{
  // Invalid
  NU_ASSERT(0, this);
}


bool
NUCModelArgConnectionInput::queryDefs(const NUNetRefHdl& /* def_net_ref */,
				      NUNetRefCompareFunction /* fn */,
				      NUNetRefFactory* /* factory */) const
{
  // Invalid
  NU_ASSERT(0, this);
  return false;
}


void NUCModelArgConnectionInput::replaceDef(NUNet* /* old_net */,
					    NUNet* /* new_net */)
{
  // Invalid
  NU_ASSERT(0, this);
}


bool NUCModelArgConnectionInput::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  if ( mActual )
    changed |= mActual->replace(old_net,new_net);
  return changed;
}

bool NUCModelArgConnectionInput::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  if ( mActual ) {
    NUExpr * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }
  if ( mActual ) {
    NUExpr * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }

  return changed;
}

void NUCModelArgConnectionOutput::getUses(NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


void NUCModelArgConnectionOutput::getUses(NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


bool
NUCModelArgConnectionOutput::queryUses(const NUNetRefHdl & /* use_net_ref */,
				       NUNetRefCompareFunction /* fn */,
				       NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0, this);
  return false;
}


void NUCModelArgConnectionOutput::getUses(NUNet * /* net */,
					  NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


void NUCModelArgConnectionOutput::getUses(const NUNetRefHdl & /* net_ref */,
					  NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


bool
NUCModelArgConnectionOutput::queryUses(const NUNetRefHdl & /* def_net_ref */,
				       const NUNetRefHdl & /* use_net_ref */,
				       NUNetRefCompareFunction /* fn */,
				       NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0, this);
  return false;
}


void NUCModelArgConnectionOutput::getDefs(NUNetSet * defs) const
{
  getActual ()->getDefs (defs);
}


void NUCModelArgConnectionOutput::getDefs(NUNetRefSet * /* defs */) const
{
  // TBD
  NU_ASSERT(0, this);
}


bool
NUCModelArgConnectionOutput::queryDefs(const NUNetRefHdl & /* def_net_ref */,
				       NUNetRefCompareFunction /* fn */,
				       NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0, this);
  return false;
}


void NUCModelArgConnectionOutput::replaceDef(NUNet * old_net, NUNet* new_net)
{
  if (mActual) {
    mActual->replaceDef(old_net,new_net);
  }
}


bool NUCModelArgConnectionOutput::replace(NUNet * old_net, NUNet* new_net)
{
  bool changed = false;
  if ( mActual )
    changed |= mActual->replace(old_net,new_net);
  return changed;
}

bool NUCModelArgConnectionOutput::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }

  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  return changed;
}

void NUCModelArgConnectionBid::getUses(NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


void NUCModelArgConnectionBid::getUses(NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


bool
NUCModelArgConnectionBid::queryUses(const NUNetRefHdl & /* use_net_ref */,
				    NUNetRefCompareFunction /* fn */,
				    NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0, this);
  return false;
}


void NUCModelArgConnectionBid::getUses(NUNet * /* net */,
				       NUNetSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


void NUCModelArgConnectionBid::getUses(const NUNetRefHdl & /* net_ref */,
				       NUNetRefSet * /* uses */) const
{
  // TBD
  NU_ASSERT(0, this);
}


bool
NUCModelArgConnectionBid::queryUses(const NUNetRefHdl & /* def_net_ref */,
				    const NUNetRefHdl & /* use_net_ref */,
				    NUNetRefCompareFunction /* fn */,
				    NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0, this);
  return false;
}


void NUCModelArgConnectionBid::getDefs(NUNetSet * defs) const
{
  getActual ()->getDefs (defs);
}


void NUCModelArgConnectionBid::getDefs(NUNetRefSet * /* defs */) const
{
  // TBD
  NU_ASSERT(0, this);
}


bool
NUCModelArgConnectionBid::queryDefs(const NUNetRefHdl & /* def_net_ref */,
				    NUNetRefCompareFunction /* fn */,
				    NUNetRefFactory * /* factory */) const
{
  // TBD
  NU_ASSERT(0, this);
  return false;
}


void NUCModelArgConnectionBid::replaceDef(NUNet * old_net, NUNet* new_net)
{
  if (mActual) {
    mActual->replaceDef(old_net,new_net);
  }
}


bool NUCModelArgConnectionBid::replace(NUNet * old_net, NUNet* new_net)
{
  bool changed = false;
  if ( mActual )
    changed |= mActual->replace(old_net,new_net);
  return changed;
}

bool NUCModelArgConnectionBid::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePre);
    if (repl) {
      mActual = repl;
      changed = true;
    }
    changed |= mActual->replaceLeaves(translator);
  }

  if ( mActual ) {
    NULvalue * repl = translator(mActual, NuToNuFn::ePost);
    if (repl) {
      mActual = repl;
      changed = true;
    }
  }
  return changed;
}

const char* NUCModelArgConnectionInput::typeStr() const
{
  return "NUCModelArgConnectionInput";
}


const char* NUCModelArgConnectionOutput::typeStr() const
{
  return "NUCModelArgConnectionOutput";
}


const char* NUCModelArgConnectionBid::typeStr() const
{
  return "NUCModelArgConnectionBid";
}


NUCModelArgConnection* NUCModelArgConnectionInput::copy(CopyContext &copy_context) const
{
  return new NUCModelArgConnectionInput(mFormal, mActual->copy(copy_context),
					mCModelCall, mLoc);
}


NUCModelArgConnection* NUCModelArgConnectionOutput::copy(CopyContext &copy_context) const
{
  return new NUCModelArgConnectionOutput(mFormal, mActual->copy(copy_context),
					 mCModelCall, mLoc);
}


NUCModelArgConnection* NUCModelArgConnectionBid::copy(CopyContext &copy_context) const
{
  return new NUCModelArgConnectionBid(mFormal, mActual->copy(copy_context),
				      mCModelCall, mLoc);
}

void NUCModelArgConnection::updateCModelCall(NUCModelCall* cmodelCall)
{
  NU_ASSERT2(mCModelCall->getCModel() == cmodelCall->getCModel(), this, cmodelCall);
  mCModelCall = cmodelCall;
}
