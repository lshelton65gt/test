// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/StringAtom.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUEnabledDriver.h"
#include "util/UtIOStream.h"
#include "nucleus/NUNetSet.h"
#include "util/UtIndent.h"
#include "nucleus/NUControlFlowNet.h"
#include "bdd/BDD.h"

NUAssign::NUAssign(NULvalue *lvalue,
                   NUExpr *rvalue,
                   bool usesCFNet,
                   const SourceLocator& loc,
                   bool do_resize) :
  NUStmt(usesCFNet, loc),
  mLvalue(lvalue),
  mRvalue(rvalue)
{
  if (do_resize) {
    resize();
  }
  {
    // if the LHS and RHS are same size, and RHS is signed, LHS is
    // unsigned, LHS is whole identifier, RHS is a constant, then RHS
    // can be coerced into a unsigned value.  This is done to minimize
    // the regolds required, and to avoid potential problems with constant propagation.
    adjustRHSSign();
  }
}

NUAssign::~NUAssign()
{
  if (mLvalue != 0) {
    delete mLvalue;
  }
  if (mRvalue != 0) {
    if (!mRvalue->isManaged())
      delete mRvalue;
  }
}


NUAssign* NUBlockingAssign::create(NULvalue *lvalue, NUExpr *rvalue,
                           NUScope*, const char*)
{
  return new NUBlockingAssign(lvalue, rvalue, getUsesCFNet(), getLoc());
}

NUAssign* NUNonBlockingAssign::create(NULvalue *lvalue, NUExpr *rvalue,
                           NUScope*, const char*)
{
  return new NUNonBlockingAssign(lvalue, rvalue, getUsesCFNet(), getLoc());
}

NUAssign* NUContAssign::create(NULvalue *lvalue, NUExpr *rvalue,
                               NUScope* scope, const char* name)
{
  StringAtom* sym = scope->gensym(name, NULL, this);
  return new NUContAssign(sym, lvalue, rvalue, getLoc(), mStrength);
}

bool NUAssign::operator==(const NUStmt& stmt) const
{
  if (getType () != stmt.getType ())
    return false;

  const NUAssign *asn = dynamic_cast<const NUAssign*>(&stmt);

  return *mLvalue == *asn->getLvalue ()
    && mRvalue->equal (*asn->getRvalue ());
}

const char* NUBlockingAssign::typeStr() const
{
  return "NUBlockingAssign";
}

const char* NUNonBlockingAssign::typeStr() const
{
  return "NUNonBlockingAssign";
}

bool NUNonBlockingAssign::operator==(const NUStmt& stmt) const
{
  return NUAssign::operator==(stmt);
}

const char* NUContAssign::typeStr() const
{
  return "NUContAssign";
}

void NUAssign::replaceDef(NUNet *old_net, NUNet *new_net)
{
  mLvalue->replaceDef(old_net, new_net);
}

bool NUAssign::replace(NUNet * old_net, NUNet * new_net)
{
  bool changed = false;
  changed |= mLvalue->replace(old_net,new_net);
  changed |= mRvalue->replace(old_net,new_net);
  return changed;
}

bool NUAssign::replaceLeaves(NuToNuFn & translator)
{
  bool changed = false;
  {
    NULvalue * repl = translator(mLvalue, NuToNuFn::ePre);
    if (repl) {
      mLvalue = repl;
      changed = true;
    }
  }
  {
    NUExpr * repl = translator(mRvalue, NuToNuFn::ePre);
    if (repl) {
      mRvalue = repl;
      changed = true;
    }
  }
  changed |= mLvalue->replaceLeaves(translator);
  changed |= mRvalue->replaceLeaves(translator);
  {
    NULvalue * repl = translator(mLvalue, NuToNuFn::ePost);
    if (repl) {
      mLvalue = repl;
      changed = true;
    }
  }
  {
    NUExpr * repl = translator(mRvalue, NuToNuFn::ePost);
    if (repl) {
      mRvalue = repl;
      changed = true;
    }
  }

  return changed;
}

void NUAssign::replaceLvalue(NULvalue* lv, bool deleteOld)
{
  if ((mLvalue != NULL) && deleteOld) {
    delete mLvalue;
  }
  mLvalue = lv;
}

bool NUAssign::isSimpleBufOrAssign() const
{
  NUIdentLvalue *ident_lvalue = dynamic_cast<NUIdentLvalue*>(mLvalue);
  NUIdentRvalue *ident_rvalue = dynamic_cast<NUIdentRvalue*>(mRvalue);

  return ((ident_lvalue != 0) and (ident_rvalue != 0));
}

bool NUAssign::isTempMemInit(NUTempMemoryNet** retTempMem) const
{
  NULvalue* lvalue = getLvalue();
  if (lvalue->isWholeIdentifier()) {
    NUNet* net = lvalue->getWholeIdentifier();
    NUTempMemoryNet* tmem = dynamic_cast<NUTempMemoryNet*>(net);
    if ((tmem != NULL)) {
      // Found one, make sure the rhs is the master
      NUMemoryNet* master = tmem->getMaster();
      NUExpr* rvalue = getRvalue();
      if (rvalue->isWholeIdentifier() &&
          (rvalue->getWholeIdentifier()->getMemoryNet() == master)) {
        *retTempMem = tmem;
        return true;
      }
    }
  }
  return false;
}

void NUAssign::resize() {
  bool lhs_real = mLvalue->isReal();
  UInt32 rhs_size = mRvalue->determineBitSize();
  UInt32 size = rhs_size;
  if ( not lhs_real ){
    size = mLvalue->determineBitSize();
    mLvalue->resize();
  }
  mRvalue = mRvalue->makeSizeExplicit(size);
}


//! in some limited cases we can remove the signedness of RHS to simplify later analysis
void NUAssign::adjustRHSSign() {
  if ( mLvalue->isReal() || mRvalue->isReal() ) {
    return;
  }
  if ( not mLvalue->isWholeIdentifier() ){
    return;
  }

  NUIdentLvalue *ident_lvalue = dynamic_cast<NUIdentLvalue*>(mLvalue);
  if ( ident_lvalue == NULL ){
    return;
  }
  
  if ( ident_lvalue->getIdent()->isSigned() ){
    return;
  }

  NUConst* constant_rhs = mRvalue->castConst();
  if ( (constant_rhs == NULL) or not mRvalue->isSignedResult()){
    return;
  }
  UInt32 rhs_size = mRvalue->getBitSize();
  UInt32 lhs_size = ident_lvalue->getBitSize();
  
  if ( rhs_size != lhs_size ){
    return;
  }

  // it is now safe to convert the RHS constant to unsinged.
  NUExpr* old_rvalue = mRvalue;
  mRvalue = constant_rhs->rebuild(false, rhs_size);
  delete old_rvalue;
}

void NUAssign::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  mLoc.print();
  UtIO::cout() << getPrintName() << "(" << this << ")" << UtIO::endl;

  if (recurse) {
    mLvalue->print(true, numspaces+2);
    mRvalue->print(true, numspaces+2);
  }
}

NUBlockingAssign::~NUBlockingAssign()
{
}


NUNonBlockingAssign::~NUNonBlockingAssign()
{
}


void NUBlockingAssign::getBlockingUses(NUNetSet *uses) const
{
  mLvalue->getUses(uses);
  mRvalue->getUses(uses);
  if ( getUsesCFNet() ){
    NUNet *cfNet = mLvalue->findParentModule()->getControlFlowNet();
    uses->insert(cfNet);
  }
}


void NUBlockingAssign::getBlockingUses(NUNetRefSet *uses) const
{
  mLvalue->getUses(uses);
  mRvalue->getUses(uses);
  if ( getUsesCFNet() ){
    NUNet *cfNet = mLvalue->findParentModule()->getControlFlowNet();
    NUNetRefHdl cfNetRef = uses->getFactory()->createNetRef(cfNet);
    uses->insert(cfNetRef);
  }
}


bool NUBlockingAssign::queryBlockingUses(const NUNetRefHdl &use_net_ref,
					 NUNetRefCompareFunction fn,
					 NUNetRefFactory *factory) const
{
  if ( getUsesCFNet() ){
    return true;
  }

  if (mLvalue->queryUses(use_net_ref, fn, factory)) {
    return true;
  }

  return mRvalue->queryUses(use_net_ref, fn, factory);
}

void NUBlockingAssign::getBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  mLvalue->getUses(net_ref, uses);
  mRvalue->getUses(uses);
  if ( getUsesCFNet() ){
    NUNet *cfNet = mLvalue->findParentModule()->getControlFlowNet();
    NUNetRefHdl cfNetRef = uses->getFactory()->createNetRef(cfNet);
    uses->insert(cfNetRef);
  }
}


bool NUBlockingAssign::queryBlockingUses(const NUNetRefHdl &def_net_ref,
					 const NUNetRefHdl &use_net_ref,
					 NUNetRefCompareFunction fn,
					 NUNetRefFactory *factory) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  if ( getUsesCFNet() ){
    return true;
  }

  if (mLvalue->queryUses(def_net_ref, use_net_ref, fn, factory)) {
    return true;
  }

  return mRvalue->queryUses(use_net_ref, fn, factory);
}


void NUBlockingAssign::getBlockingDefs(NUNetSet *defs) const
{
  mLvalue->getDefs(defs);
}


void NUBlockingAssign::getBlockingDefs(NUNetRefSet *defs) const
{
  mLvalue->getDefs(defs);
}


bool NUBlockingAssign::queryBlockingDefs(const NUNetRefHdl &def_net_ref,
					 NUNetRefCompareFunction fn,
					 NUNetRefFactory *factory) const
{
  return mLvalue->queryDefs(def_net_ref, fn, factory);
}


void NUBlockingAssign::getBlockingKills(NUNetSet *kills) const
{
  mLvalue->getCompleteDefs(kills);
}


void NUBlockingAssign::getBlockingKills(NUNetRefSet *kills) const
{
  mLvalue->getCompleteDefs(kills);
}


bool NUBlockingAssign::queryBlockingKills(const NUNetRefHdl &def_net_ref,
					  NUNetRefCompareFunction /* fn -- unused */,
					  NUNetRefFactory *factory) const
{
  return mLvalue->queryCompleteDefs(def_net_ref, factory);
}


BDD NUBlockingAssign::bdd(NUNet* net, BDDContext *ctx, STBranchNode *scope)
  const
{
  // For now, only deal with an lvalue that is exactly the net we are
  // looking for
  if (mLvalue->isWholeIdentifier() && (mLvalue->getWholeIdentifier() == net))
    return mRvalue->bdd(ctx, scope);
  return ctx->invalid();
}

void NUNonBlockingAssign::getNonBlockingUses(NUNetSet *uses) const
{
  mLvalue->getUses(uses);
  mRvalue->getUses(uses);
}


void NUNonBlockingAssign::getNonBlockingUses(NUNetRefSet *uses) const
{
  mLvalue->getUses(uses);
  mRvalue->getUses(uses);
}


bool NUNonBlockingAssign::queryNonBlockingUses(const NUNetRefHdl &use_net_ref,
					       NUNetRefCompareFunction fn,
					       NUNetRefFactory *factory) const
{
  if (mLvalue->queryUses(use_net_ref, fn, factory)) {
    return true;
  }

  return mRvalue->queryUses(use_net_ref, fn, factory);
}

void NUNonBlockingAssign::getNonBlockingUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  mLvalue->getUses(net_ref, uses);
  mRvalue->getUses(uses);
}


bool NUNonBlockingAssign::queryNonBlockingUses(const NUNetRefHdl &def_net_ref,
					       const NUNetRefHdl &use_net_ref,
					       NUNetRefCompareFunction fn,
					       NUNetRefFactory *factory) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  if (mLvalue->queryUses(def_net_ref, use_net_ref, fn, factory)) {
    return true;
  }

  return mRvalue->queryUses(use_net_ref, fn, factory);
}


void NUNonBlockingAssign::getNonBlockingDefs(NUNetSet *defs) const
{
  mLvalue->getDefs(defs);
}


void NUNonBlockingAssign::getNonBlockingDefs(NUNetRefSet *defs) const
{
  mLvalue->getDefs(defs);
}


bool NUNonBlockingAssign::queryNonBlockingDefs(const NUNetRefHdl &def_net_ref,
					       NUNetRefCompareFunction fn,
					       NUNetRefFactory *factory) const
{
  return mLvalue->queryDefs(def_net_ref, fn, factory);
}


void NUNonBlockingAssign::getNonBlockingKills(NUNetSet *kills) const
{
  mLvalue->getCompleteDefs(kills);
}


void NUNonBlockingAssign::getNonBlockingKills(NUNetRefSet *kills) const
{
  NUNetSet net_kills;
  mLvalue->getCompleteDefs(&net_kills);
  for (NUNetSet::iterator iter = net_kills.begin(); iter != net_kills.end(); ++iter) {
    kills->insert(kills->getFactory()->createNetRef(*iter));
  }
}


bool NUNonBlockingAssign::queryNonBlockingKills(const NUNetRefHdl &def_net_ref,
						NUNetRefCompareFunction /* fn -- unused */,
						NUNetRefFactory *factory) const
{
  return mLvalue->queryCompleteDefs(def_net_ref, factory);
}

static void sCompose(NULvalue* lval,
                     NUExpr* rval,
                     const SourceLocator& loc,
                     UtString *buf,
                     int numSpaces, bool recurse,
                     const char* prefix,
                     const char* op,
                     const char* strength,
                     const STBranchNode* scope)
{
  UtIndent indent(buf);
  buf->append(numSpaces, ' ');
  if ( loc.isTicProtected() ){
    NUBase::composeProtectedNUObject(loc, buf);
    return; 
  }

  *buf << prefix;
  lval->compose(buf, scope, 0, recurse);
  *buf << " " << op << " " << strength;
  rval->compose(buf, scope);
  *buf << ";";
  indent.tabToLocColumn();
  *buf << " // ";
  loc.compose(buf);
  indent.newline();
}

void NUBlockingAssign::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  sCompose(mLvalue, mRvalue, getLoc(), buf, numSpaces, recurse, "", "=", "", scope);
}

void NUNonBlockingAssign::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  sCompose(mLvalue, mRvalue, getLoc(), buf, numSpaces, recurse, "", "<=", "", scope);
}

void NUContAssign::compose(UtString *buf, const STBranchNode* scope, int numSpaces, bool recurse) const
{
  const char* strength = ( mStrength == eStrPull )? "(pull1,pull0) ": "";
  sCompose(mLvalue, mRvalue, getLoc(), buf, numSpaces, recurse, "assign ", "=", strength, scope);
}

// Convert a non-blocking assignment into a blocking assignment.
// This is a *lot* easier to work with if it's converted in place
// without a new allocation.  This is relatively simple to do assuming
// the sizeof the two derived classes is the same.  If in the future
// we needed to make a blocking structure bigger, we'd need to
// override NUNonBlockingAssign::operator new to allocate space
// big enough for a non-blocking assign.  Alternatively we could
// hack Reorder.cxx to mutate the statement lists in place while
// removing cycles.
NUBlockingAssign *NUNonBlockingAssign::makeBlocking()
{
  NU_ASSERT(sizeof(NUBlockingAssign) == sizeof(NUNonBlockingAssign), this);
  NULvalue* lv = mLvalue;
  NUExpr* rv = mRvalue;
  SourceLocator loc = mLoc;
  NUBlockingAssign *assign = new (this) NUBlockingAssign(lv, rv, false, loc);
  return assign;
}

bool NUBlockingAssign::operator==(const NUStmt& stmt) const
{
  return NUAssign::operator==(stmt);
}

NUStmt* NUBlockingAssign::copy(CopyContext & copy_context) const
{
  // false is passed as the do_resize argument, because the lhs and rhs have already been sized, so do not want to resize.
  return new NUBlockingAssign(mLvalue->copy(copy_context), mRvalue->copy(copy_context), getUsesCFNet(), mLoc, false);
}

NUStmt* NUNonBlockingAssign::copy(CopyContext & copy_context) const
{
  // false is passed as the do_resize argument, because the lhs and rhs have already been sized, so do not want to resize.
  return new NUNonBlockingAssign(mLvalue->copy(copy_context), mRvalue->copy(copy_context), getUsesCFNet(), mLoc, false);
}


void NUContAssign::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);

  mLoc.print();
  UtIO::cout() << getPrintName() << "(" << this << ") : " << mName->str() << " : ";
  switch (mStrength) {
  case eStrPull:
    UtIO::cout() << "pull ";
    break;

  case eStrDrive:
    UtIO::cout() << "drive ";
    break;

  case eStrTie:
    UtIO::cout() << "tie ";
    break;

  }
  UtIO::cout() << UtIO::endl;

  if (recurse) {
    mLvalue->print(true, numspaces+2);
    mRvalue->print(true, numspaces+2);
  }
}


void NUContAssign::printUDSets(NUNetRefFactory *netref_factory) const
{
  NUUseDefNode::printUDSets(netref_factory);
}


void NUContAssign::getUses(NUNetSet *uses) const
{
  mLvalue->getUses(uses);
  mRvalue->getUses(uses);
}


void NUContAssign::getUses(NUNetRefSet *uses) const
{
  mLvalue->getUses(uses);
  mRvalue->getUses(uses);
}


bool NUContAssign::queryUses(const NUNetRefHdl &use_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const
{
  if (mLvalue->queryUses(use_net_ref, fn, factory)) {
    return true;
  }

  return mRvalue->queryUses(use_net_ref, fn, factory);
}

void NUContAssign::getUses(const NUNetRefHdl &net_ref, NUNetRefSet *uses) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  mLvalue->getUses(net_ref, uses);
  mRvalue->getUses(uses);
}


bool NUContAssign::queryUses(const NUNetRefHdl &def_net_ref,
			     const NUNetRefHdl &use_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const
{
  // TBD: this is pessimistic, in the case of the lvalue being a concatenation
  if (mLvalue->queryUses(def_net_ref, use_net_ref, fn, factory)) {
    return true;
  }

  return mRvalue->queryUses(use_net_ref, fn, factory);
}


void NUContAssign::getDefs(NUNetSet *defs) const
{
  mLvalue->getDefs(defs);
}


void NUContAssign::getDefs(NUNetRefSet *defs) const
{
  mLvalue->getDefs(defs);
}


bool NUContAssign::queryDefs(const NUNetRefHdl &def_net_ref,
			     NUNetRefCompareFunction fn,
			     NUNetRefFactory *factory) const
{
  return mLvalue->queryDefs(def_net_ref, fn, factory);
}

void NUContAssign::getCompleteDefs(NUNetSet* defs) const
{
  mLvalue->getCompleteDefs(defs);
}

void NUContAssign::getCompleteDefs(NUNetRefSet* defs) const
{
  mLvalue->getCompleteDefs(defs);
}

void NUContAssign::getBlockingUses(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getBlockingUses(NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
				     NUNetRefCompareFunction /* fn -- unused */,
				     NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContAssign::getBlockingUses(NUNet * /* net -- unused */,
				   NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getBlockingUses(const NUNetRefHdl & /* net_ref -- unused */,
				   NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
				     const NUNetRefHdl & /* use_net_ref -- unused */,
				     NUNetRefCompareFunction /* fn -- unused */,
				     NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContAssign::getNonBlockingUses(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getNonBlockingUses(NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryNonBlockingUses(const NUNetRefHdl & /* use_net_ref -- unused */,
					NUNetRefCompareFunction /* fn -- unused */,
					NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContAssign::getNonBlockingUses(NUNet * /* net -- unused */,
				      NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getNonBlockingUses(const NUNetRefHdl & /* net_ref -- unused */,
				      NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryNonBlockingUses(const NUNetRefHdl & /* def_net_ref -- unused */,
					const NUNetRefHdl & /* use_net_ref -- unused */,
					NUNetRefCompareFunction /* fn -- unused */,
					NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContAssign::getBlockingDefs(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getBlockingDefs(NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryBlockingDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
				     NUNetRefCompareFunction /* fn -- unused */,
				     NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContAssign::getNonBlockingDefs(NUNetSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getNonBlockingDefs(NUNetRefSet * /* uses -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryNonBlockingDefs(const NUNetRefHdl & /* def_net_ref -- unused */,
					NUNetRefCompareFunction /* fn -- unused */,
					NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContAssign::getBlockingKills(NUNetSet * /* kills -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getBlockingKills(NUNetRefSet * /* kills -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryBlockingKills(const NUNetRefHdl & /* def_net_ref -- unused */,
				      NUNetRefCompareFunction /* fn -- unused */,
				      NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


void NUContAssign::getNonBlockingKills(NUNetSet * /* kills -- unused */) const
{
  NU_ASSERT(0, this);
}


void NUContAssign::getNonBlockingKills(NUNetRefSet * /* kills -- unused */) const
{
  NU_ASSERT(0, this);
}


bool NUContAssign::queryNonBlockingKills(const NUNetRefHdl & /* def_net_ref -- unused */,
					 NUNetRefCompareFunction /* fn -- unused */,
					 NUNetRefFactory * /* factory -- unused */) const
{
  NU_ASSERT(0, this);
  return false;
}


bool NUContAssign::makeEnabledDriver(NUEnabledDriver **new_driver)
{
  if (not mRvalue->drivesZ()) {
    return false;
  }

  NUExpr *enable = 0;
  NUExpr *driver = 0;
  bool success = mRvalue->isolateZ(&enable, &driver);

  if (not success) {
    return 0;
  }

  // A pure Z driver can be discarded entirely
  if ((enable == 0) or (driver == 0)) {
    NU_ASSERT(enable == 0, this);
    NU_ASSERT(driver == 0, this);
    *new_driver = 0;
    return true;
  }

  CopyContext copy_context(NULL,NULL);
  *new_driver =
    new NUContEnabledDriver(mName,
                            mLvalue->copy(copy_context),
                            enable,
                            driver,
                            getStrength(),
                            false,
                            mLoc);
  return true;
}

const NUModule* NUContAssign::findParentModule() const
{
  return mLvalue->findParentModule();
}

NUModule* NUContAssign::findParentModule()
{
  return mLvalue->findParentModule();
}

NUStmt* NUContAssign::copy(CopyContext & copy_context) const
{
  StringAtom *name = copy_context.mScope->newBlockName(copy_context.mStrCache, mLoc);

  // false is passed as the do_resize argument, because the lhs and rhs have already been sized, so do not want to resize.
  return new NUContAssign(name, mLvalue->copy(copy_context), mRvalue->copy(copy_context), mLoc, mStrength, false);
}

bool NUContAssign::operator==(const NUStmt& stmt) const
{
  const NUContAssign *asn = dynamic_cast<const NUContAssign*>(&stmt);
  if (!asn || (asn->mStrength != mStrength))
    return false;

  return NUAssign::operator==(stmt);
}

NUStmt* NUContAssign::copyBlocking() const
{
  NU_ASSERT(mStrength == eStrDrive, this);
  CopyContext copy_context(NULL,NULL);
  NUAssign* ba = new NUBlockingAssign(mLvalue->copy(copy_context),
                                      mRvalue->copy(copy_context), getUsesCFNet(), mLoc);
  ba->getRvalue()->resize(mRvalue->getBitSize());
  return ba;
}  

bool NUContAssign::isConstantAssign(const IODBNucleus *iodb) const
{
  if (mLvalue->isWholeIdentifier())
  {
    NUNet* net = mLvalue->getWholeIdentifier();
    return (!net->isPort() && !net->isNonStatic() && !net->isMultiplyDriven() 
            && net->isContinuouslyDriven() && !net->isBlockLocal()
            && !net->isProtectedMutable (iodb)
	    && (mRvalue->castConst() != NULL));
  }
  return false;
}

//! Use this method to decide whether to call the 'getBlocking*' or 'get*' methods
bool NUContAssign::useBlockingMethods() const {
  return false;
}

//! Can this node drive a Z on the specified net?
bool NUContAssign::drivesZ(NUNet* dest) const {
  if (mStrength == eStrPull) {
    return true;
  }
  if (mRvalue->drivesZ()) {
    NUNetSet defs;
    getDefs(&defs);
    if (defs.find(dest) != defs.end()) {
      return true;
    }
  }
  return false;
}

bool NUContAssign::isScheduled (void) const {
  return mScheduled;
}

void NUContAssign::putScheduled (bool f) {
  mScheduled = f;
}

//! In Verilog semantics, does this node preserve a Z on src
//! when it drives dest?
/*!
 *! Note that this does not imply (at this time) that
 *! Carbon will preserve the Z properly -- only that Verilog
 *! semantics require it.  This routine is for use in Nucleus
 *! transformation code, or in code that warns about potential
 *! mismatches with simulation
 */
bool NUAssign::shouldPreserveZ(NUNet* src, NUNet* dest) const {
  if (mRvalue->shouldPreserveZ(src)) {
    NUNetSet defs;
    mLvalue->getDefs(&defs);
    if (defs.find(dest) != defs.end()) {
      return true;
    }
  }
  return false;
}

// See if the \a defs have overlapping bit references with the \a uses. \a noCover
// false allows exact uses to be ignored
static bool overlapHelper (NUNetRefSet& defs, NUNetRefSet& uses, bool noCover)
{
  for(NUNetRefSet::iterator p = defs.begin (); p != defs.end (); ++p)
    {
      NUNetRefSet::const_iterator ause = uses.find (*p, &NUNetRef::overlapsSameNet);

      if (ause != uses.end ()) {
	if (noCover)
	  return true;		// No overlap is permitted
	else if (*ause != *p)
	  // There's an overlap and it's not 1:1
	  return true;
      }

      // At this point, we either have no overlaps, or they're 1:1 and
      // we've decided that's okay
    }

  // no dangerous overlap detected
  return false;
}

//! Does the destination overlap with the source expression values?
// noCover=> true says 1:1 overlap not allowed
bool NUBlockingAssign::overlaps (NUNetRefFactory* factory, bool noCover) const
{
  NUNetRefSet defs (factory);
  NUNetRefSet uses (factory);

  getBlockingDefs (&defs);
  getBlockingUses (&uses);

  return overlapHelper (defs, uses, noCover);
}

bool NUContAssign::overlaps (NUNetRefFactory* factory, bool noCover) const
{
  NUNetRefSet defs (factory);
  NUNetRefSet uses (factory);
  getDefs (&defs);
  getUses (&uses);

  return overlapHelper (defs, uses, noCover);
}

bool NUNonBlockingAssign::overlaps (NUNetRefFactory* factory, bool noCover) const
{
  NUNetRefSet defs (factory);
  NUNetRefSet uses (factory);
  getNonBlockingDefs (&defs);
  getNonBlockingUses (&uses);

  return overlapHelper (defs, uses, noCover);
}
