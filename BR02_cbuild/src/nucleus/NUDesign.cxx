// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "hdl/HdlId.h"
#include "hdl/HdlVerilogPath.h"
#include "iodb/ScheduleFactory.h"
#include "nucleus/NUAliasDB.h"
#include "nucleus/NUCModelInterface.h"
#include "nucleus/NUControlFlowNet.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUDesignWalker.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUNetSet.h"
#include "nucleus/NUSysTaskNet.h"
#include "nucleus/NUTaskEnable.h"
#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/CbuildMsgContext.h"
#include "util/OSWrapper.h"
#include "util/UtIOStream.h"
#include "util/UtUniquify.h"

NUDesign::NUDesign(STSymbolTable* pSTSymbolTable,
                   IODBNucleus*   pIODBNucleus,
                   SourceLocatorFactory* pSourceLocatorFactory,
                   SCHScheduleFactory*    pSCHScheduleFactory,
                   MsgContext* pMessageContext,
                   ArgProc* pArgProc,
                   NUNetRefFactory* nrf) :
  mCurSymtabIndex(0),
  mGlobalTimePrecision(0),
  mNetRefFactory(nrf)
{
  mSymtab = pSTSymbolTable;
  mIODB = pIODBNucleus;
  mSourceLocatorFactory = pSourceLocatorFactory;
  mScheduleFactory = pSCHScheduleFactory;
  mMsgContext = pMessageContext;
  mArgProc = pArgProc;

  mAliasBOM = new NUAliasBOM;
  mTieNets = new NUNetSet;
  mOnDemandNets = new NUConstNetSet;
  mBufferedNetMap = new NUNetElabMap;
  NUBase::incDesignCount();
}


class DesignTFCallback : public NUDesignCallback {
public:
  //! By default, walk through everything.
  Status operator() (Phase , NUBase * )  { return eNormal; }

  //! clear out resolutions from hier task enables
  Status operator() (Phase phase, NUTaskEnable* taskEna) {
    if (phase == ePre) {
      NUTaskHierRef* href = taskEna->getHierRef();
      if (href != NULL) {
        href->cleanupResolutions();
      }
    }
    return eNormal;
  }
};


NUDesign::~NUDesign()
{
  // Need to delete modules bottom-up in order to avoid netref issues
  NUModuleList mods;
  getModulesBottomUp(&mods);

  // Clear the hierarchial task-enable resolutions before deleting
  // modules, since hier-refs can be top down (and often are).
  DesignTFCallback tfClear;
  NUDesignWalker tfWalker(tfClear, false, false);
  tfWalker.putWalkTasksFromTaskEnables(false);
  for (NUModuleList::iterator p = mods.begin(), e = mods.end(); p != e; ++p) {
    NUModule *module = *p;
    tfWalker.module(module);
  }

  for (NUModuleList::iterator p = mods.begin(), e = mods.end(); p != e; ++p) {
    NUModule *module = *p;
    mAllModules.erase(module);
    delete module;
  }

  // Flattening leaves NUModules allocated but unused, and therefore unreached
  // by getModulesBottomUp.  Those are the ones that remain in mAllModules.
  // Clean 'em up.
  while (not mAllModules.empty()) {
    NUModuleSet::iterator p = mAllModules.begin();
    NUModule *module = *p;
    delete module;
    mAllModules.erase(p);
  }

  while (not mAllCModelInterfaces.empty()) {
    CModelInterfaceMapIter i = mAllCModelInterfaces.begin();
    NUCModelInterface* cmodelInterface = i->second;
    delete cmodelInterface;
    mAllCModelInterfaces.erase(i);
  }

  delete mAliasBOM;
  delete mTieNets;
  delete mOnDemandNets;
  delete mBufferedNetMap;
  //delete mIODB;             // will be delete by CarbonContext
  //delete mSymtab;           // will be delete by CarbonContext
  //delete mNetRefFactory;    // will be delete by CarbonContext
  //delete mScheduleFactory;  // will be delete by CarbonContext

  NUBase::decDesignCount();
}

SInt32 NUDesign::reserveSymtabIndex() 
{
  return mCurSymtabIndex++; 
}

/*
void NUDesign::cleanupElab()
{
  for (NUModuleSetIter iter = mTopLevelModules.begin();
       iter != mTopLevelModules.end();
       iter++) {
    NUModuleElab *module_elab = (*iter)->lookupElab(mSymtab);
    if (module_elab != 0) {
      delete module_elab;
    }
  }
}
*/

void NUDesign::addTopLevelModule(NUModule* module)
{
  mTopLevelModules.insert(module);
  mAllModules.insert(module);
}


void NUDesign::addModule(NUModule* module)
{
  mAllModules.insert(module);
}

void NUDesign::getTopLevelModules(NUModuleList *module_list) const
{
  module_list->insert(module_list->end(), mTopLevelModules.begin(), mTopLevelModules.end());
}


void NUDesign::getAllModules(NUModuleList *module_list) const
{
  module_list->insert(module_list->end(), mAllModules.begin(), mAllModules.end());
}

const char* NUDesign::typeStr() const
{
  return "NUDesign";
}


void NUDesign::print(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);

  UtIO::cout() << "Design(" << this << ")" << UtIO::endl;

  if (recurse) {
    for (NUModuleSetIter iter = mTopLevelModules.begin();
	 iter != mTopLevelModules.end();
	 ++iter) {
      (*iter)->print(true, numspaces + 2);
    }
  }
}


void NUDesign::printElab(bool recurse_arg, int indent_arg) const
{
  bool recurse = UtIOStreamBase::limitBoolArg(recurse_arg, true);
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  indent(numspaces);
  UtIO::cout() << "Design(" << this << ")" << UtIO::endl;

  if (recurse) {
    for (NUModuleSetIter iter = mTopLevelModules.begin();
	 iter != mTopLevelModules.end();
	 ++iter) {
      (*iter)->lookupElab(mSymtab)->printElab(mSymtab, true, 2);
    }
  }
}

void NUDesign::observeNet(NUNetElab* net)
{
  mObservable.insert(net->getSymNode());
}

void NUDesign::handleElabInternalNetsHelper(NUNet* net, StringAtom* modName)
{
  STSymbolTableNode* node = mSymtab->find(NULL, modName);
  if (not node) {
    UtString internalError ("module name ");
    internalError << modName->str () << " not found in design symbol table";
    INFO_ASSERT(0, internalError.c_str ());
  }
  STBranchNode* branch = node->castBranch();
  ST_ASSERT(branch, node);
  NUNetElab* netElab = net->lookupElab(branch);
  NU_ASSERT(netElab, net);      // there must always be a netElab for
                               // this internal net since this
                               // method should have been called
                               // after elaboration.
  observeNet(netElab);         // add the net to the mOutputPorts
}

void NUDesign::handleElabInternalNets()
{
 for (NUDesign::ModuleLoop l = loopTopLevelModules(); !l.atEnd(); ++l)
 {
   NUModule* module = *l;
   StringAtom* modName = module->getName();

   NUNet* net = module->getOutputSysTaskNet();
   handleElabInternalNetsHelper(net, modName);

   net = module->getControlFlowNet();
   handleElabInternalNetsHelper(net, modName);
 }
}


void NUDesign::addClockMaster(NUNetElab* net)
{
  mClockMasters.insert(net->getSymNode());
}

NUNetElab* NUDesign::findNetElab(const char* name)
{
  HdlHierPath::Status status;
  HdlId info;
  STSymbolTableNode* node = mSymtab->getNode(name, &status, &info);
  if (node == NULL)
    return NULL;
  return NUNetElab::find(node);
}

NUNet* NUDesign::findNet(const char* name)
{
  HdlHierPath::Status status;
  HdlId info;

  if (mSymtab != NULL) {
    STSymbolTableNode* node = mSymtab->getNode(name, &status, &info);
    if (node != NULL)
    {
      NUNetElab* netElab = NUNetElab::find(node);
      if (netElab != NULL)
        return netElab->getNet();
    }
  }

  // Try to find it as modulename.netname
  HdlVerilogPath hdl;
  UtStringArray names;
  status = hdl.parseName(&name, &names, &info);
  if (((status != HdlHierPath::eLegal) && (status != HdlHierPath::eEndPath)) || (names.size() <= 1))
    UtIO::cout() << "Illegal path/modulename.netname specified\n";
  else
  {
    // Find the module
    NUModule* module = NULL;
    const char* modname = names[0];
    for (NUModuleSet::iterator p = mAllModules.begin();
         p != mAllModules.end(); ++p)
    {
      NUModule* mod = *p;
      if (strcmp(mod->getName()->str(), modname) == 0) {
        module = mod;
        break;
      }
    }

    if (module == NULL)
      UtIO::cout() << "Module " << names[0] << " does not exist\n";
    else
    {
      STSymbolTable* modTab = module->getAliasDB();
      STSymbolTableNode* node = NULL;
      AtomicCache* cache = modTab->getAtomicCache();

      for (UInt32 i = 1; i < names.size(); ++i)
      {
        StringAtom* name = cache->getIntern(names[i]);
        if (name == NULL) {
          node = NULL;
          break;
        }
        node = modTab->find(node, name);
      }
      if (node == NULL)
        UtIO::cout() << "Path does not exist\n";
      else
        return NUNet::find(node);
    }
  } // else

  return NULL;
} // NUNet* NUDesign::findNet

NUModuleElab* NUDesign::findMod(const char* name)
{
  HdlHierPath::Status status;
  HdlId info;
  STSymbolTableNode* node = mSymtab->getNode(name, &status, &info);
  if (node == NULL)
    return NULL;
  return NUModuleElab::find(node);
}

void NUDesign::addCModelInterface(UtString& name,
				  NUCModelInterface* cmodelInterface)
{
  NU_ASSERT(mAllCModelInterfaces.find(name) == mAllCModelInterfaces.end(), cmodelInterface);
  mAllCModelInterfaces.insert(CModelInterfaceMapValue(name, cmodelInterface));
  mCModelInterfaceVector.push_back(cmodelInterface);
}

NUCModelInterface* NUDesign::findCModelInterface(UtString& name) const
{
  CModelInterfaceMapConstIter i = mAllCModelInterfaces.find(name);
  if (i == mAllCModelInterfaces.end())
    return NULL;
  else
    return i->second;
}

void NUDesign::printVerilog(const char* filename)
{
  UtString errmsg;
  FILE* f = OSFOpen(filename, "w", &errmsg);
  if (f == NULL)
    UtIO::cout() << errmsg << UtIO::endl;
  else
  {
    UtString buf;
    for (NUModuleSet::iterator p = mAllModules.begin();
         p != mAllModules.end(); ++p)
    {
      NUModule* mod = *p;
      buf.clear();
      mod->compose(&buf, NULL,  0, true);
      fwrite(buf.c_str(), 1, buf.size(), f);
      fprintf(f, "\n\n");
    }
    fclose(f);
  }
} // void NUDesign::printVerilog

void NUDesign::findModuleInstances(ModuleInstances* moduleInstances)
{
  // Walk all the top modules and add them to the set. They will have
  // empty module instances.
  NUModuleList mods;
  getModulesBottomUp(&mods);
  for (NUModuleList::iterator iter = mods.begin();
       iter != mods.end();
       ++iter) {
    // Allocate the instances set if it doesn't exist
    NUModule* module = *iter;
    ModuleInstances::iterator pos = moduleInstances->find(module);
    Instances* instances;
    if (pos == moduleInstances->end())
    {
      instances = new Instances;
      moduleInstances->insert(ModuleInstancesValue(module, instances));
    }
  }

  // Walk all the modules in the design
  for (NUModuleList::iterator iter = mods.begin();
       iter != mods.end();
       ++iter) {
    // Walk over all sub instances
    NUModule* parent = *iter;
    for (NUModuleInstanceMultiLoop i = parent->loopInstances(); !i.atEnd(); ++i)
    {
      NUModuleInstance* moduleInstance = *i;
      NUModule* module = moduleInstance->getModule();
    
      // Allocate the instances set if it doesn't exist
      ModuleInstances::iterator pos = moduleInstances->find(module);
      Instances* instances;
      if (pos == moduleInstances->end())
      {
        instances = new Instances;
        moduleInstances->insert(ModuleInstancesValue(module, instances));
      }
      else
        instances = pos->second;
      instances->insert(moduleInstance);
    }
  } // for
} // NUDesign::findModuleInstances

static void sRecurseMods(NUModule* mod,
                         NUModuleSet* covered,
                         NUModuleList* mods)
{
  if (covered->find(mod) == covered->end())
  {
    covered->insert(mod);

    // Do all the submodules first for bottom-up accuracy on ports
    for (NUModuleInstanceMultiLoop p = mod->loopInstances(); !p.atEnd(); ++p)
    {
      NUModuleInstance* i = *p;
      sRecurseMods(i->getModule(), covered, mods);
    }

    mods->push_back(mod);
  }
}

void NUDesign::getModulesBottomUp(NUModuleList* mods)
  const
{
  NUModuleSet covered;
  for (ModuleLoop p = loopTopLevelModules(); !p.atEnd(); ++p)
    sRecurseMods(*p, &covered, mods);
}

//! class to find all modules that are not tic-protected
class UnprotectedModulesCallback : public NUDesignCallback {
public:
  UnprotectedModulesCallback(NUModuleList* unprotected_modules) : mUnprotected(unprotected_modules) {}
  Status operator() (Phase, NUBase*) { return eNormal; }

  Status operator() (Phase phase, NUModule* module) {
    if (phase == ePre) {
      if ( not module->getLoc().isTicProtected() ) {
        mUnprotected->push_back(module);
      }
    }
    // need to look at instances and decide if we want to recurse
    return eNormal;
  }
  Status operator() (Phase phase, NUModuleInstance* instance) {
    if (phase == ePre) {
      if ( instance->getLoc().isTicProtected() ) {
        return eSkip;           // no need to recurse into this instance
      } else {
        return eNormal;
      }
    }
    return eNormal;
  }
private:
  NUModuleList* mUnprotected;
};

void NUDesign::getUnprotectedModules(NUModuleList* unprotected_modules)
{
  UnprotectedModulesCallback cb(unprotected_modules);
  NUDesignWalker walker(cb, false, true); // visit each module only once
  walker.putWalkTasksFromTaskEnables(false);
  walker.putWalkDeclaredNets(false);
  walker.design(this);
}



void
NUDesign::setGlobalTimePrecision( SInt8 value )
{
  if ( value < mGlobalTimePrecision )
    mGlobalTimePrecision = value;
}


SInt8
NUDesign::getGlobalTimePrecision() const
{
  return mGlobalTimePrecision;
}

NUDesign::NetLoop NUDesign::loopLocals() const {
  ModuleLoop modules = mTopLevelModules.loopSorted();
  return NetLoop(modules);
}

NUDesign::ModuleLoop NUDesign::loopAllModules() const {
  return mAllModules.loopSorted();
}

NUDesign::ModuleLoop NUDesign::loopTopLevelModules() const {
  return mTopLevelModules.loopSorted();
}

STSymbolTable* NUDesign::putSymtab(STSymbolTable* newSymTab)
{
  STSymbolTable* oldSymbolTable = mSymtab;
  mSymtab = newSymTab;
  return oldSymbolTable;
}

void NUDesign::clearDesignOutputs()
{
  mObservable.clear();
  resetClockMasters();
}

void NUDesign::dumpVerilog(const char* fileRoot, const char* dirName,
                           MsgContext* msgContext)
{
  UtString dir, errmsg, cmd;
  dir << fileRoot << "." << dirName;
  (void) OSDeleteRecursive(dir.c_str(), &errmsg);
  errmsg.clear();
  if (OSMkdir(dir.c_str(), 0777, &errmsg) != 0) {
    msgContext->VlogCannotOpenFile(dir.c_str(), errmsg.c_str());
    return;
  }
  
  UtString buf;
  UtUniquify uniqueFiles;
  NUModuleList unprotected_modules;

  getUnprotectedModules(&unprotected_modules);
  
  for (Loop<NUModuleList> p = Loop<NUModuleList>(unprotected_modules); !p.atEnd(); ++p) {
    NUModule* mod = *p;
    const char* modName = mod->getName()->str();
    UtString vfile, buf;
    const char* fname = OSLegalizeFilename(modName, &buf);
    fname = uniqueFiles.insert(fname);
    buf.clear();
    buf << fname << ".v";
    OSConstructFilePath(&vfile, dir.c_str(), buf.c_str());
    buf.clear();
    FILE* f = OSFOpen(vfile.c_str(), "w", &errmsg);
    if (f == NULL) {
      // See if this filename was too heinous
      msgContext->VlogCannotOpenFile(vfile.c_str(), errmsg.c_str());
      errmsg.clear();
      continue;
    }
    mod->compose(&buf, NULL, 0, true);
    fwrite(buf.c_str(), 1, buf.size(), f);
    fclose(f);
  }
} // void NUDesign::dumpVerilog

// Experimental code for Programmer's View
#ifdef CARBON_PV
NUDesign* NUDesign::copy() {
  AtomicCache* acache = mSymtab->getAtomicCache();
  NUDesign* new_design = new NUDesign(mSourceLocatorFactory,
                                      mMsgContext, mArgProc,
                                      mSymtab->getFieldBOM(), acache,
                                      mNetRefFactory);
  IODBNucleus* new_iodb = new_design->getIODB();
  new_iodb->shareBVFactory(getIODB()->getBVFactory());
  UtHashMap<NUModule*,NUModule*> mod_map;

  // add all the modules to the design
  NUModuleList mods;
  getModulesBottomUp(&mods);
  for (NUModuleList::iterator p = mods.begin(), e = mods.end(); p != e; ++p) {
    NUModule* mod = *p;
    (void) mod->copy(new_design, &mod_map);
  }

  return new_design;
} // NUDesign* NUDesign::copy
#endif

AtomicCache* NUDesign::getAtomicCache() const {
  return mSymtab->getAtomicCache();
}
