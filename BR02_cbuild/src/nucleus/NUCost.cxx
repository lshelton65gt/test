// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUCost.h"
#include "nucleus/NUDesign.h"
#include "nucleus/NUModule.h"
#include "nucleus/NUNet.h"
#include "nucleus/NUModuleInstance.h"
#include "nucleus/NUBlock.h"
#include "nucleus/NUNamedDeclarationScope.h"
#include "nucleus/NUUseNode.h"
#include "nucleus/NUAssign.h"
#include "nucleus/NUTF.h"
#include "nucleus/NUTFArgConnection.h"
#include "nucleus/NUAlwaysBlock.h"
#include "nucleus/NUInitialBlock.h"
#include "nucleus/NUUseDefNode.h"
#include "flow/FLNode.h"
#include "nucleus/NUExpr.h"
#include "nucleus/NUIf.h"
#include "nucleus/NUCase.h"
#include "nucleus/NUEnabledDriver.h"
#include "nucleus/NULvalue.h"
#include "nucleus/NUTriRegInit.h"
#include "nucleus/NUSysTask.h"
#include "nucleus/NUSysFunctionCall.h"
#include "nucleus/NUSysRandom.h"
#include "nucleus/NUTaskEnable.h"
#include "nucleus/NUPortConnection.h"
#include "nucleus/NUCycle.h"
#include "nucleus/NUFor.h"
#include "nucleus/NUCModelCall.h"
#include "nucleus/NUCModelArgConnection.h"
#include "nucleus/NUCModelPort.h"
#include "util/StringAtom.h"
#include "util/OSWrapper.h"
#include "util/UtStringUtil.h"
#include "NUCostPerformance.h"
#include "symtab/STBranchNode.h"

static const SInt32 cFlopBitCost     = 6;
static const SInt32 cLatchBitCost    = 3;
static const SInt32 cTriRegBitCost   = 3;
static const SInt32 cAddBitCost      = 6;
static const SInt32 cIncBitCost      = 3;
static const SInt32 cCompareBitCost  = 3;
static const SInt32 cEqBitCost       = 2;
static const SInt32 cMux1BitCost     = 2;
static const SInt32 cBranchInstructionOverhead = 8;

static UInt32 sBitInstructions(UInt32 bits)
{
  return ((bits + 31) / 32);
}

/*!
 * Function supporting saturated positive arithmetic over type SInt32.
 * Note the use of a reference parameter, so that we can do things like
 *      MAXIFY (somecounter += 1);
 * But \e beware of doing
 *      MAXIFY (++othercounter);
 * because the lack of a sequence point means that we might not update
 * othercounter before calling the function.
 */
void MAXIFY (SInt32& field)
{
  const SInt32 largestPositive = 0x7fffffff;

  if (field < 0)
    field = largestPositive;
}

NUCostContext::NUCostContext() :
  mUpdateInstances(true),
  mTaskEnablesHaveTaskCost(false)
{
}

NUCostContext::~NUCostContext()
{
  clear();
}

NUCost* NUCostContext::allocCost(const NUBase* obj, CostMap* costMap)
{
  INFO_ASSERT(obj != (NUBase*)0xdeadbeef, "costing a deleted nucleus object");
  NUCost* cost = (*costMap)[obj];
  if (cost == NULL)
    (*costMap)[obj] = cost = new NUCost;
  return cost;
}


void NUCostContext::calcDesign(const NUDesign* design, NUCost* passedInCost)
{
  mDesignCost.clear();
  for (NUDesign::ModuleLoop m = design->loopTopLevelModules(); !m.atEnd(); ++m)
  {
    const NUModule* mod = *m;
    calcModule(mod, &mDesignCost, true);
  }

  // Calculate elaborated instance counts.
  for (NUDesign::ModuleLoop m = design->loopTopLevelModules(); !m.atEnd(); ++m)
  {
    const NUModule* mod = *m;
    countInstances(mod);
  }

  // Calculate unelaborated instance counts.
  // Iterate through all modules, counting their children.
  NUModuleList mods;
  design->getModulesBottomUp(&mods);
  for (NUModuleList::iterator iter = mods.begin();
       iter != mods.end();
       ++iter) {
    const NUModule* mod = *iter;
    countUnelabSubinstances(mod);
  }
  // Make sure to count the top-level modules as being instantiated.
  for (NUDesign::ModuleLoop m = design->loopTopLevelModules(); !m.atEnd(); ++m)
  {
    const NUModule* mod = *m;
    incrementUnelabInstances(mod);
  }

  if (passedInCost != NULL)
    *passedInCost = mDesignCost;
}

void NUCostContext::recalcModule(const NUModule* mod)
{
  CostMap::iterator p = mDeepCostMap.find(mod);
  if (p != mDeepCostMap.end())
  {
    delete p->second;
    mDeepCostMap.erase(p);
  }
  p = mSelfCostMap.find(mod);
  if (p != mSelfCostMap.end())
  {
    delete p->second;
    mSelfCostMap.erase(p);
  }
  calcModule(mod, NULL, false);
}

void NUCostContext::recalcTF(const NUTF* tf)
{
  CostMap::iterator p = mSelfCostMap.find(tf);
  if (p != mSelfCostMap.end())
  {
    delete p->second;
    mSelfCostMap.erase(p);
  }
  calcTF(tf);
}

void NUCostContext::calcTF(const NUTF* tf)
{
  if(mSelfCostMap[tf] == NULL) {
    NUCost* cost = allocCost(tf, &mSelfCostMap);
    tf->calcCost(cost, this);
  }
}

void NUCostContext::calcStmtList(const NUStmtList & stmts, NUCost * cost)
{
  for (NUStmtList::const_iterator iter = stmts.begin();
       iter != stmts.end();
       ++iter) {
    (*iter)->calcCost(cost,this);
  }
}

NUCost* NUCostContext::findCost(const NUBase* obj, bool deep)
{
  CostMap& costMap = deep? mDeepCostMap: mSelfCostMap;
  CostMap::iterator p = costMap.find(obj);
  if (p == costMap.end())
    return NULL;
  return p->second;
}

const NUCost* NUCostContext::findCost(const NUBase* obj, bool deep)
  const 
{
  const CostMap& costMap = deep? mDeepCostMap: mSelfCostMap;
  CostMap::const_iterator p = costMap.find(obj);
  if (p == costMap.end())
    return NULL;
  return p->second;
}

NUCost* NUCostContext::getCycleCost(const NUCycle* cycle)
{
  NUCost* selfCost = mSelfCostMap[cycle];
  return selfCost;
}

void NUCostContext::putCycleCost(const NUCycle* cycle, const NUCost& cost)
{
  NUCost* selfCost = allocCost(cycle, &mSelfCostMap);
  selfCost->addCost(cost, false);
}


//! Callback to find nets referenced in modules
class NUCostNetFinder : public NUDesignCallback {
public:
  NUCostNetFinder(NUModule* mod): mModule(mod)
  {
  }

  void addNet(NUNet* net) {
    if (net->getScope()->getModule() == mModule) {
      mNets.insert(net);
    }
  }

  Status operator()(Phase phase, NUIdentRvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUIdentLvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUMemselLvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase phase, NUMemselRvalue* id) {
    if (phase == ePre) {
      addNet(id->getIdent());
    }
    return eNormal;
  }
  Status operator()(Phase, NUBase*) {
    return eNormal;
  }

  // Avoid recursing into modules (except the one we are called with)
  Status operator() (Phase, NUModule* mod) {
    if (mod != mModule) {
      return eSkip;
    }
    return eNormal;
  }

  NUNetSet mNets;
  NUModule* mModule;
}; // class NUCostNetFinder : public NUDesignCallback

#define COUNT_NETS_FROM_DECLARATIONS 0

void NUCostContext::calcModule(const NUModule* mod, NUCost* cost, bool addIOs)
{
  NUCost* deepCost = mDeepCostMap[mod];
  NUCost* selfCost = mSelfCostMap[mod];
  if (deepCost == NULL)
  {
    deepCost = allocCost(mod, &mDeepCostMap);
    selfCost = allocCost(mod, &mSelfCostMap);

    NUCostNetFinder cnf(const_cast<NUModule*>(mod));
    NUDesignWalker walker(cnf, false, false);
    walker.module(const_cast<NUModule*>(mod));

    for (NUNetSet::iterator p = cnf.mNets.begin(), e = cnf.mNets.end();
         p != e; ++p)
    {
      NUNet* net = *p;

      // Don't count nets referenced from hierarchical tasks (for now)
      if (net->getScope()->getModule() == mod) {
        net->calcCost(selfCost);
      }
    }

    for (NUTaskLoop p = mod->loopTasksLevelized(); !p.atEnd(); ++p) {
      NUTask* task = *p;
      calcTF(task);
      NUCost* taskCost = findCost(task);
      NU_ASSERT(taskCost, task);  // What task didn't we find cost info for?
      selfCost->addCost(*taskCost, false, true, 0);  // include syntactic elements of task cost
    }        

#if COUNT_NETS_FROM_DECLARATIONS
    for (NUModule::NetCLoop net = mod->loopNets(); !net.atEnd(); ++net)
      (*net)->calcCost(selfCost);
#endif

    for (NUNamedDeclarationScopeCLoop scope = mod->loopDeclarationScopes(); !scope.atEnd(); ++scope)
      (*scope)->calcCost(selfCost, this);
    for (NUModule::ContAssignCLoop ca = mod->loopContAssigns(); !ca.atEnd(); ++ca)
      (*ca)->calcCost(selfCost, this);
    for (NUModule::AlwaysBlockCLoop ab = mod->loopAlwaysBlocks(); !ab.atEnd();
         ++ab)
      (*ab)->calcCost(selfCost, this);
    for (NUModule::InitialBlockCLoop ab = mod->loopInitialBlocks(); !ab.atEnd();
         ++ab)
      (*ab)->calcCost(selfCost, this);
    // Now copy all that data to deep-cost and dive down
    *deepCost = *selfCost;

    for (NUModuleInstanceMultiCLoop i = mod->loopModuleInstances(); !i.atEnd(); ++i)
    {
      NUModuleInstance* instance = *i;
      calcModule(instance->getModule(), deepCost, false /* no IOs */);
      ++selfCost->mSubmodules;
      ++deepCost->mSubmodules;
    }

    for (NUCModelCLoop l = mod->loopCModels(); !l.atEnd(); ++l)
    {
      ++selfCost->mCModels;
      ++deepCost->mCModels;
    }
  }
  NU_ASSERT(selfCost, mod);
  if (cost != NULL)
    cost->addCost(*deepCost, addIOs);
} // void NUCostContext::calcModule

void NUCostContext::countInstances(const NUModule* mod)
{
  NUCost* deepCost = mDeepCostMap[mod];
  NUCost* selfCost = mSelfCostMap[mod];

  // Run through the instance loop again, multiplying the instance counts,
  // where the multiplying takes place via increment-by-one!  2B optimized later!
  for (NUModuleInstanceMultiCLoop i = mod->loopModuleInstances(); !i.atEnd(); ++i)
  {
    NUModuleInstance* instance = *i;
    countInstances(instance->getModule());
  }
  for (NUNamedDeclarationScopeCLoop scope = mod->loopDeclarationScopes();
       !scope.atEnd(); ++scope)
  {
    NUNamedDeclarationScope* declScope = *scope;
    countInstances(declScope);
  }
  ++selfCost->mInstances;
  ++deepCost->mInstances;
}

void NUCostContext::countInstances(const NUNamedDeclarationScope* declScope)
{
  for (NUModuleInstanceMultiCLoop i = declScope->loopDeclScopeInstances();
       !i.atEnd(); ++i)
  {
    NUModuleInstance* instance = *i;
    countInstances(instance->getModule());
  }
  for (NUNamedDeclarationScopeCLoop scope = declScope->loopDeclarationScopes();
       !scope.atEnd(); ++scope)
  {
    NUNamedDeclarationScope* declScope = *scope;
    countInstances(declScope);
  }
}

void NUCostContext::countUnelabSubinstances(const NUModule* mod)
{
  // Since this is an unelaborated count, do not recurse.  Just count
  // immediate instances.
  for (NUModuleInstanceMultiCLoop i = mod->loopModuleInstances(); !i.atEnd(); ++i)
  {
    NUModuleInstance* instance = *i;
    incrementUnelabInstances(instance->getModule());
  }
  for (NUNamedDeclarationScopeCLoop scope = mod->loopDeclarationScopes();
       !scope.atEnd(); ++scope)
  {
    NUNamedDeclarationScope* declScope = *scope;
    countUnelabSubinstances(declScope);
  }
}

void NUCostContext::countUnelabSubinstances(const NUNamedDeclarationScope* declScope)
{
  for (NUModuleInstanceMultiCLoop i = declScope->loopDeclScopeInstances();
       !i.atEnd(); ++i)
  {
    NUModuleInstance* instance = *i;
    incrementUnelabInstances(instance->getModule());
  }
  for (NUNamedDeclarationScopeCLoop scope = declScope->loopDeclarationScopes();
       !scope.atEnd(); ++scope)
  {
    NUNamedDeclarationScope* declScope = *scope;
    countUnelabSubinstances(declScope);
  }
}

void NUCostContext::incrementUnelabInstances(const NUModule* mod)
{
  NUCost* deepCost = mDeepCostMap[mod];
  NUCost* selfCost = mSelfCostMap[mod];
  if (not deepCost) {
    deepCost = allocCost(mod, &mDeepCostMap);
    selfCost = allocCost(mod, &mSelfCostMap);
  }
  ++selfCost->mUnelabInstances;
  ++deepCost->mUnelabInstances;
}

static bool getFullNameIfUnprotectedObj(const NUBase* obj, UtString* buf)
{
  buf->clear();
  const NUModule* mod = dynamic_cast<const NUModule*>(obj);
  if (mod != NULL ) {
    if ( mod->getLoc().isTicProtected() ) {
      return false;
    } else {
      (*buf) << mod->getName()->str();
      return true;
    }
  }
  const NUTF* tf = dynamic_cast<const NUTF*>(obj);
  if ( tf != NULL ) {
    if ( tf->getLoc().isTicProtected() ) {
      return false;
    } else {
      (*buf) << tf->getParentScope()->getName()->str() << '.';
      tf->getNameBranch()->compose(buf);
      return true;
    }
  }
  (*buf) << "<Unknown>";
  return true;
}

bool NUCostContext::printCostTable(const char* filename, bool deep, bool csv,
                                   bool estimateCPS)
  const
{
  UtString reason;
  FILE* f = (filename == NULL)? stdout: OSFOpen(filename, "w", &reason);
  if (f == NULL)
  {
    fprintf(stderr, "%s\n", reason.c_str());
    return false;
  }

  const CostMap& costMap = deep? mDeepCostMap: mSelfCostMap;

  // Find the maximum length of all the module names
  size_t maxModNameLength = strlen("DESIGN");
  bool has_protected_objects = false;
  UtString buf;
  for (CostMap::UnsortedCLoop p = costMap.loopCUnsorted(); !p.atEnd(); ++p)
  {
    if ( getFullNameIfUnprotectedObj(p.getKey(), &buf) ) {
      maxModNameLength = std::max(maxModNameLength, buf.size());
    } else {
      has_protected_objects = true;
    }
  }

  // Print a sorted table of the costs of every module
  char format[30];
  if (csv)
    strcpy(format, "%s,");
  else
    sprintf(format, "%%-%ds ", (int) maxModNameLength);
  fprintf(f, format, "");
  NUCost::printHeader(f, csv);
  fprintf(f, format, "DESIGN");
  mDesignCost.print(f, csv);
  for (CostMap::SortedLoop p = costMap.loopSorted(); !p.atEnd(); ++p)
  {
    if ( getFullNameIfUnprotectedObj(p.getKey(), &buf) ) {
      const NUCost* cost = p.getValue();
      fprintf(f, format, buf.c_str());
      cost->print(f, csv);
    }
  }

  if ( has_protected_objects ) {
    fprintf(f, "NOTE: This design contains some `protected regions.\n");
  }

  if (estimateCPS)
  {
    UInt32 numTotalFlops = mDesignCost.mFlops.mVars;
    UInt32 numTotalCombos = mDesignCost.mCombinationalBlocks;  
    fprintf(f, "Estimated performance: %f cps\n", NUCostPerformance::calcOverallPerformance(numTotalFlops, numTotalCombos));
  }
  
  
  if (filename == NULL)
    fflush(stdout);
  else
    fclose(f);
  return true;
} // bool NUCostContext::printCostTable

void NUCost::printHeader(FILE* f, bool csv)
{
  if (csv)
  {
    fprintf(f, "Insts,UnelabInsts,Submods,Ops,Instructs,");
    fprintf(f, "RunInstructions,Gates,If,For,Case,ContAssigns,");
    fprintf(f, "BlckAssigns,NBlckAssigns,Flops,Flop bits,Latches,Latch bits,");
    
    fprintf(f, "IOs,IO bits,Locals,Local bits,HierNets,Tristates,Tristate bits,");
    fprintf(f, "DoubleBuffers,DoubleBuffer bits,Memories,Mem Bits,");
    fprintf(f, "Mem Addrs,ComboBlocks,Seq Blocks,Reset Blocks,");
    fprintf(f, "Initial Blocks,Tasks,HierTasks,CModels,Cmodel Calls,"); // rjc Function cost is obsolete
    fprintf(f, "Cmodel Args,SysTasks,SysFuncs\n");
  }
  else
  fprintf(f,
          "insts unelabinsts submd oprs/instrs/rinstr/gates   if/ for/case casn/basn/nasn  flops/bits latchs/bits  IOs/bits locals/bits hiernets trists/bits dblbuf/bits mems/bits/adrs comb/seqs/rsts/init tasks/hiertasks CMods/Calls/ Args SysTasks SysFuncs\n");
}

template<typename T>
static void sFormatInt(T num, UtString* buf, int cols,
                       bool csv, char delim)
{
  if (csv)
  {
    *buf << num << ((delim == '\n')? delim: ',');
  }
  else
  {
    StringUtil::formatInt(num, buf, cols);
    *buf << delim;
  }
}

void NUCost::NetInfo::format(UtString* buf, bool csv, int cols, char delim)
  const
{
  sFormatInt(mVars, buf, cols, csv, '/');
  sFormatInt(mBits, buf, cols, csv, delim);
}

void NUCost::MemoryInfo::format(UtString* buf, bool csv, int cols, char delim)
  const
{
  sFormatInt(mVars, buf, cols, csv, '/');
  sFormatInt(mBits, buf, cols, csv, delim);
}

void NUCost::print(FILE* f, bool csv)
  const
{
  UtString buf;
  sFormatInt(mInstances, &buf, 5, csv, ' ');
  sFormatInt(mUnelabInstances, &buf, 5, csv, ' ');
  sFormatInt(mSubmodules, &buf, 5, csv, ' ');
  sFormatInt(mOps, &buf, 5, csv, '/');
  sFormatInt(mInstructions, &buf, 5, csv, '/');
  sFormatInt(mInstructionsRun, &buf, 5, csv, '/');
  sFormatInt(mAsicGates, &buf, 5, csv, ' ');
  sFormatInt(mIfs, &buf, 4, csv, '/');
  sFormatInt(mFors, &buf, 4, csv, '/');
  sFormatInt(mCases, &buf, 4, csv, ' ');
  sFormatInt(mContAssigns, &buf, 4, csv, '/');
  sFormatInt(mBlockingAssigns, &buf, 4, csv, '/');
  sFormatInt(mNonBlockingAssigns, &buf, 4, csv, ' ');
  mFlops.format(&buf, csv);
  mLatches.format(&buf, csv);
  NetInfo ios;
  ios += mInputs;
  ios += mOutputs;
  ios += mBidirects;
  ios.format(&buf, csv, 4);
  mLocalNets.format(&buf, csv);
  sFormatInt(mHierNets, &buf, 5, csv, ' ');
  mTristates.format(&buf, csv);
  mDoubleBuffered.format(&buf, csv);
  //mClkPath.format(&buf);
  mMemories.format(&buf, csv, 4, '/');
  sFormatInt(mMemories.mAddrs, &buf, 4, csv, ' ');
  sFormatInt(mCombinationalBlocks, &buf, 4, csv, '/');
  sFormatInt(mSequentialBlocks, &buf, 4, csv, '/');
  sFormatInt(mResetBlocks, &buf, 4, csv, '/');
  sFormatInt(mInitialBlocks, &buf, 4, csv, ' ');
  sFormatInt(mTasks, &buf, 5, csv, '/');
  sFormatInt(mHierTasks, &buf, 5, csv, '/');
  sFormatInt(mCModels, &buf, 5, csv, '/');
  sFormatInt(mCModelCalls, &buf, 5, csv, '/');
  sFormatInt(mCModelArgs, &buf, 5, csv, '/');
  sFormatInt(mSysTasks, &buf, 5, csv, '/');
  sFormatInt(mSysFunctions, &buf, 5, csv, '\n');
  fputs(buf.c_str(), f);
} // void NUCost::print

NUCost::NUCost()
{
  clear();
}

NUCost::~NUCost()
{
}

void NUCost::clear()
{
  memset(this, 0, sizeof(NUCost));
}

SInt32 NUCost::totalAssigns() const
{
  return mContAssigns + mBlockingAssigns + mNonBlockingAssigns;
}

double NUCost::complexity() const
{
  SInt32 assigns = totalAssigns();
  return assigns ? (1.0*mInstructions)/assigns : (1.0*mInstructions);
}

void NUCost::addCost(const NUCost& other, bool addIOs, bool addSyntactic, UInt32 executionCount)
{
  // incorporate costs for all syntactic measurements
  if (addSyntactic)
  {
    mPrimGates           += other.mPrimGates;
    mFlops               += other.mFlops;
    mLatches             += other.mLatches;
    mTristates           += other.mTristates;
    mLocalNets           += other.mLocalNets;
    mTemps               += other.mTemps;
    mMemories            += other.mMemories;
    mDoubleBuffered      += other.mDoubleBuffered;
    mClkPath             += other.mClkPath;
    mIntegers            += other.mIntegers;
    mBufAssigns          += other.mBufAssigns;
    mInstances           += other.mInstances;
    mUnelabInstances     += other.mUnelabInstances;
    mSubmodules          += other.mSubmodules;
    mUDPs                += other.mUDPs;
    mSequentialBlocks    += other.mSequentialBlocks;
    mCombinationalBlocks += other.mCombinationalBlocks;
    mResetBlocks         += other.mResetBlocks;
    mInitialBlocks       += other.mInitialBlocks;
    mTasks               += other.mTasks;
    mSysFunctions        += other.mSysFunctions;
    mFors                += other.mFors;
    mIfs                 += other.mIfs;
    mCases               += other.mCases;
    mContAssigns         += other.mContAssigns;
    mBlockingAssigns     += other.mBlockingAssigns;
    mNonBlockingAssigns  += other.mNonBlockingAssigns;
    mInstructions        += other.mInstructions;
    mOps                 += other.mOps;
    mCModels             += other.mCModels;
    mCModelCalls         += other.mCModelCalls;
    mCModelArgs          += other.mCModelArgs;
    mSysTasks            += other.mSysTasks;
    mHierNets            += other.mHierNets;
    mHierTasks           += other.mHierTasks;
  }

  // incorporate counts of I/Os
  if (addIOs)
  {
    mInputs    += other.mInputs;
    mOutputs   += other.mOutputs;
    mBidirects += other.mBidirects;
  }
  
  // incorporate estimates of execution resources
  if (executionCount > 0)
  {
    mInstructionsRun += executionCount * other.mInstructionsRun;
    mAsicGates       += executionCount * other.mAsicGates;
  }

  // In test/constprop/crc.v, these four fields get so big that
  // they exceed 31 bits and go negative.  Test for this.
  MAXIFY(mInstructions);
  MAXIFY(mInstructionsRun);
  MAXIFY(mAsicGates);
  MAXIFY(mOps);

} // void NUCost::addCost

void NUEnabledDriver::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  mEnable->calcCost(cost, context);
  if (mDriver != NULL) {
    mDriver->calcCost(cost, context);
  }
  mLvalue->calcCost(cost, context);

  UInt32 instructions = cBranchInstructionOverhead;

  // Add in some arbitrary cost to represent the procedure-call overhead
  // for the separately scheduled enabled driver, plus the initialization
  // at the beginning of the schedule.  This is enough to kick the cost
  // of two enabled drivers:
  //    assign out = en1? a : 1'bz;
  //    assign out = en2? b : 1'bz;
  // over the cost of a single cont assign:
  //    assign out = (en1 | en2) ? (en1 ? a : b) : 1'bz;
  instructions += cBranchInstructionOverhead;

  MAXIFY (cost->mInstructions += instructions);
  MAXIFY (cost->mInstructionsRun += instructions);
}

void NUNamedDeclarationScope::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  for (NUModuleInstanceMultiCLoop i = loopDeclScopeInstances(); !i.atEnd(); ++i) {
    NUModuleInstance* instance = *i;
    context->calcModule(instance->getModule(), cost, false /* no IOs */);
    ++cost->mSubmodules;
  }

  for (NUNamedDeclarationScopeCLoop scope = loopDeclarationScopes(); !scope.atEnd(); ++scope) {
    (*scope)->calcCost(cost, context);
  }
#if COUNT_NETS_FROM_DECLARATIONS
  for (NUNetCLoop n = loopLocals(); !n.atEnd(); ++n) {
    (*n)->calcCost(cost, context);
  }
#endif
}

void NUBlock::calcCost(NUCost* cost, NUCostContext* context)
  const
{
#if COUNT_NETS_FROM_DECLARATIONS
  for (NUNetCLoop n = loopLocals(); !n.atEnd(); ++n)
    (*n)->calcCost(cost, context);
#endif

  // don't loop over sub-blocks -- they're already counted in the stmtlist.
  for (NUStmtCLoop s = loopStmts(); !s.atEnd(); ++s)
    (*s)->calcCost(cost, context);
}


void NUEdgeExpr::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  context->calcExpr(mExpr, cost);
}

void NUIdentRvalue::calcCost(NUCost* cost, NUCostContext*)
  const
{
  SInt32 bitIns = sBitInstructions(getBitSize()); // Use access size, not object size
  MAXIFY (cost->mInstructions += bitIns);
  MAXIFY (cost->mInstructionsRun += bitIns);
}

void NUVarselRvalue::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  SInt32 wordWidth = sBitInstructions(mRange.getLength ());

  // *2: 1 for the shift, 1 for the mask
  MAXIFY (cost->mInstructions += 2*wordWidth);
  MAXIFY (cost->mInstructionsRun += 2*wordWidth);
  MAXIFY (cost->mOps+=1);

  if ( not mIdent->isWholeIdentifier ()) {
    // Complex partselects need their base object costed as well
    context->calcExpr (mIdent, cost);
  } else {
    // For whole idents, account for the fetch.
    MAXIFY (cost->mInstructions += wordWidth);
    MAXIFY (cost->mInstructionsRun += wordWidth);
  }

  context->calcExpr(mExpr, cost);
}


void NUMemselRvalue::calcCost(NUCost* cost, NUCostContext*context)
  const
{
  MAXIFY (cost->mInstructions += 5);
  MAXIFY (cost->mInstructionsRun += 5);
  MAXIFY (cost->mOps+=1);
  if (not mIdent->isWholeIdentifier ())
    context->calcExpr (mIdent, cost);

  const UInt32 numDims = mExprs.size();
  for( UInt32 i = 0; i < numDims; ++i )
    context->calcExpr (mExprs[i], cost);
}

void NUConst::calcCost(NUCost*, NUCostContext*)
  const
{
}

static void sCombineBranchCosts(NUCost* cost,
                                const NUCost& c1, const NUCost& c2)
{
  SInt32 run = std::max(c1.mInstructionsRun, c2.mInstructionsRun)
    + cost->mInstructionsRun;
  cost->addCost(c1, true);
  cost->addCost(c2, true);
  MAXIFY (run);
  cost->mInstructionsRun = run;
}

void NUOp::calcCost(NUCost* cost, NUCostContext* costContext)
  const
{
  bool doSubExprs = true;
  SInt32 nargs = getNumArgs();
  SInt32 ncost = std::max(1, ((SInt32) nargs) - 1);
  UInt32 size = getBitSize();

  // For LUTs, the constant table requires a fetch.
  if (mOp == eNaLut) {
    ncost = 1;
  }

  switch (mOp)
  {
  case eBiVhExt:
  case eBiVhZxt:
    // With a constant width, there is zero extra cost for forcing the width
    // (and maybe a trivially small cost to do a sign-extend - but that's mostly
    // accounted for in the width of the resulting expression.)
    if (getArg (1)->castConst ()) {
      return costContext->calcExpr (getArg (0), cost);
    }
    // fall-thru

  default:
    for (SInt32 i = 0; i < nargs; ++i) {
      const NUExpr* expr = getArg(i);
      UInt32 asize = expr->getBitSize();
      if (asize > size) {
        size = asize;
      }
    }
    break;
  }

  MAXIFY (cost->mOps += ncost);
  UInt32 rawInstructions = ncost * sBitInstructions(size);
  MAXIFY (cost->mInstructions += rawInstructions);
  MAXIFY (cost->mInstructionsRun += rawInstructions);
  UInt32 gates = 0;
  switch (mOp)
  {
  case eUnBuf:        gates = 1;                                 break;
  case eUnPlus:       gates = 1;                                 break;
  case eUnMinus:      gates = cAddBitCost;                       break;
  case eUnLogNot:     gates = 1;                                 break;
  case eUnBitNeg:     gates = 1;                                 break;
  case eUnVhdlNot:    gates = 1;                                 break;
  case eUnRedAnd:     gates = 1;                                 break;
  case eUnRedOr:      gates = 1;                                 break;
  case eUnRedXor:     gates = 1;                                 break;
  case eUnCount:      gates = 1;				 break;
  case eUnAbs:        gates = 1;				 break;
  case eUnFLO:
  case eUnFFO:	      gates = 1;		 		 break;
  case eUnFLZ:
  case eUnFFZ:	      gates = 1;				 break;
  case eUnRound:      gates = 1;                                 break;
  case NUOp::eUnItoR: gates = 0;                                 break;
  case NUOp::eUnRtoI: gates = 0;                                 break;
  case NUOp::eUnRealtoBits: gates = 0;                           break;
  case NUOp::eUnBitstoReal: gates = 0;                           break;
  case eUnChange:     gates = 0;                                 break;
  case eBiPlus:       gates = cAddBitCost;                       break;
  case eBiMinus:      gates = cAddBitCost;                       break;
  case eBiSMult:
  case eBiUMult:      gates = 1;  /* assume power of 2 */        break;
  case eBiSDiv:
  case eBiUDiv:       gates = 1;                                 break;
  case eBiSMod:
  case eBiUMod:       gates = 1;                                 break;
  case eBiVhdlMod:    gates = 1;                                 break;
  case eBiDExp:
  case eBiExp:        gates = 1;                                 break;
  case eBiEq:         gates = cEqBitCost;                        break;
  case eBiNeq:        gates = cEqBitCost;                        break;
  case eBiTrieq:      gates = cEqBitCost;                        break;
  case eBiTrineq:     gates = cEqBitCost;                        break;
  case eBiLogAnd:     gates = 1;                                 break;
  case eBiLogOr:      gates = 1;                                 break;
  case eBiSLt:
  case eBiULt:        gates = cCompareBitCost;                   break;
  case eBiSLte:
  case eBiULte:       gates = cCompareBitCost;                   break;
  case eBiSGtr:
  case eBiUGtr:       gates = cCompareBitCost;                   break;
  case eBiSGtre:
  case eBiUGtre:      gates = cCompareBitCost;                   break;
  case eBiBitAnd:     gates = 1;                                 break;
  case eBiBitOr:      gates = 1;                                 break;
  case eBiBitXor:     gates = cEqBitCost;                        break;
  case eBiRoR:        gates = 1;                                 break;
  case eBiRoL:        gates = 1;                                 break;
  case eBiRshift:     gates = 1;                                 break;
  case eBiLshift:     gates = 1;                                 break;
  case eBiVhZxt:
  case eBiVhExt:      gates = 0;                                 break;
  case eBiVhLshift:   gates = 1;                                 break;
  case eBiVhRshift:   gates = 1;                                 break;
  case NUOp::eBiVhLshiftArith:
  case eBiRshiftArith:gates = 1;                                 break;
  case NUOp::eBiVhRshiftArith:
  case eBiLshiftArith:gates = 1;                                 break;

  case eTeCond: {
    gates = cMux1BitCost;
    // Branches cost.  Account for it.
    MAXIFY (cost->mInstructions += cBranchInstructionOverhead);
    MAXIFY (cost->mInstructionsRun += cBranchInstructionOverhead);

    doSubExprs = false;

    // We always pay for the conditional
    costContext->calcExpr(getArg(0), cost);

    // We only pay the max of the instruction costs for the two
    // branches
    NUCost thenCost, elseCost;
    costContext->calcExpr(getArg(1), &thenCost);
    costContext->calcExpr(getArg(2), &elseCost);
    sCombineBranchCosts(cost, thenCost, elseCost);
    break;
  }
  case eNaConcat:
    gates = 0;
    // In addition to moving all the data, concats require shifting
    // and masking for every entry after the first one.  So although
    // they don't create gates, concats are about 3x more expensive
    // than other operations in terms of instructions
    cost->mInstructions += 2*(rawInstructions - 1);
    MAXIFY (cost->mInstructions);

    cost->mInstructionsRun += 2*(rawInstructions - 1);
    MAXIFY (cost->mInstructionsRun);
    break;
  case eNaFopen:      gates = 0;                                 break;
  case eNaLut:        gates = cEqBitCost;                        break;
  case eZEndFile:     gates = 0;                                 break;
  case eZSysTime:     gates = 0;                                 break;
  case eZSysStime:    gates = 0;                                 break;
  case eZSysRealTime: gates = 0;                                 break;
  case eBiDownTo:     gates = cAddBitCost;			 break;
    
  case eStart:       break;
  case eInvalid:     break;
  } // switch
  cost->mAsicGates += ncost * gates * size;
  MAXIFY (cost->mAsicGates);

  if (doSubExprs) {
    for (SInt32 i = 0; i < nargs; ++i)
      costContext->calcExpr(getArg(i), cost);
  }
}

void NUIdentLvalue::calcCost(NUCost* cost, NUCostContext*)
  const
{
  cost->mInstructions += sBitInstructions(getBitSize()); // use access size, not object size!
  MAXIFY (cost->mInstructions);

  cost->mInstructionsRun += sBitInstructions(getBitSize());
  MAXIFY (cost->mInstructionsRun);
}

void NUVarselLvalue::calcCost(NUCost* cost, NUCostContext*context)
  const
{
  SInt32 wordWidth = sBitInstructions (mRange.getLength ());

  // Partial stores are expensive; you have to fetch the current value,
  // shift the new partial value into place, mask, and or the values together.
  //
  // This multiplier accounts for the shift, mask, or:
  MAXIFY (cost->mInstructions += 3*wordWidth);

  MAXIFY (cost->mInstructionsRun += 3*wordWidth);
  MAXIFY (cost->mOps+=1);

  if (not mIdent->isWholeIdentifier ()) {
    mIdent->calcCost (cost, context);
  } else {
    // For whole idents, account for the fetch & the store.
    MAXIFY (cost->mInstructions += 2*wordWidth);
    MAXIFY (cost->mInstructionsRun += 2*wordWidth);
  }

  context->calcExpr (mExpr, cost);
}

void NUConcatLvalue::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  MAXIFY (cost->mInstructions += sBitInstructions(determineBitSize()));
  MAXIFY (cost->mInstructionsRun += sBitInstructions(determineBitSize()));
  MAXIFY (cost->mOps += getNumArgs());

  for(NULvalueVector::const_iterator i = mLvalues.begin (); i != mLvalues.end (); ++i)
    (*i)->calcCost (cost, context);
}

void NUMemselLvalue::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  MAXIFY (cost->mInstructions += 5);
  MAXIFY (cost->mInstructionsRun += 5);
  MAXIFY (cost->mOps += 1);
  
  // Note that MemselLvalue's don't have a NULvalue underneath - just a Memory
  // so there's nothing extra to cost here...

  const UInt32 numDims = mExprs.size();
  for( UInt32 i = 0; i < numDims; ++i )
    context->calcExpr (mExprs[i], cost);
}

void NUAlwaysBlock::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  if (isSequential())
  {
    if (getClockBlock() == NULL)
      ++cost->mSequentialBlocks;
    else
      ++cost->mResetBlocks;
  }
  else
    ++cost->mCombinationalBlocks;
  getBlock()->calcCost(cost, context);
}

void NUIf::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  MAXIFY (cost->mInstructions += 2);        // for if-statement itself (test & branch)
  MAXIFY (cost->mInstructionsRun += 2);     // for if-statement itself (test & branch)
  ++cost->mIfs;
  mCond->calcCost(cost, context);
  SInt32 thenStmts = 0;
  SInt32 elseStmts = 0;
  NUCost thenCost, elseCost;
  for (NUStmtCLoop s = loopThen(); !s.atEnd(); ++s)
  {
    (*s)->calcCost(&thenCost, context);
    ++thenStmts;
  }
  for (NUStmtCLoop s = loopElse(); !s.atEnd(); ++s)
  {
    (*s)->calcCost(&elseCost, context);
    ++elseStmts;
  }

  sCombineBranchCosts(cost, thenCost, elseCost);

  // For gate counts, assume that this if-statement forms N 2-input muxes,
  // where N is max(thenStmts,elseStmts)
  MAXIFY (cost->mAsicGates += std::max(thenStmts,elseStmts) * 2 * cMux1BitCost);
} // void NUIf::calcCost

void NUCase::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  MAXIFY (cost->mInstructions += 1);        // for case-statement itself
  MAXIFY (cost->mInstructionsRun += 1);     // for case-statement itself
  ++cost->mCases;
  getSelect()->calcCost(cost, context);
  SInt32 maxNumStmts = 0;
  SInt32 numItems = 0;
  SInt32 run = cost->mInstructionsRun;
  SInt32 maxRun = 0;
  for (NUCase::ItemCLoop i = loopItems(); !i.atEnd(); ++i)
  {
    NUCaseItem* ci = *i;
    for (NUCaseItem::ConditionLoop c = ci->loopConditions(); !c.atEnd(); ++c)
    {
      const NUCaseCondition* cond = *c;
      cond->getExpr()->calcCost(cost, context);
      const NUExpr* mask = cond->getMask();
      if (mask != NULL)
        mask->calcCost(cost, context);
    }
    SInt32 numStmts = 0;
    NUCost itemCost;
    for (NUStmtLoop s = ci->loopStmts(); !s.atEnd(); ++s)
    {
      (*s)->calcCost(&itemCost, context);
      ++numStmts;
    }
    if (itemCost.mInstructionsRun > maxRun)
      maxRun = itemCost.mInstructionsRun;
    cost->addCost(itemCost, true);    // will fix run below
    maxNumStmts = std::max(maxNumStmts, numStmts);
    ++numItems;
  }
  MAXIFY (cost->mInstructionsRun = run + maxRun);
  MAXIFY (cost->mAsicGates += cMux1BitCost * numItems * maxNumStmts);
} // void NUCase::calcCost

void NUFor::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  SInt32 estimated_loop_count = 0;
  bool estimate_valid = estimateLoopCount(&estimated_loop_count);
  if ( not estimate_valid ){
    estimated_loop_count = 0;
  }

  ++cost->mFors;

  NUCost initialCost;
  for ( NUStmtCLoop s = loopInitial(); !s.atEnd(); ++s) {
    (*s)->calcCost(&initialCost, context);
  }

  // As far as gate level costs are concerned, for-loops should be
  // considered completely unrolled, and the initial, condition and
  // advance should not impact gate count.  They do impact instruction
  // count, though.
  initialCost.mAsicGates = 0;
  cost->addCost(initialCost, false, true, 1);

  NUCost conditionCost;
  getCondition()->calcCost(&conditionCost, context);
  conditionCost.mAsicGates = 0;
  // the condition is evaluated one extra time
  cost->addCost(conditionCost, false, true, estimated_loop_count + 1);


  NUCost advanceCost;
  for ( NUStmtCLoop s = loopAdvance(); !s.atEnd(); ++s) {
    (*s)->calcCost(&advanceCost, context);
  }
  advanceCost.mAsicGates = 0;
  cost->addCost(advanceCost, false, true, estimated_loop_count);

  NUCost bodyCost;
  for (NUStmtCLoop s = loopBody(); !s.atEnd(); ++s)
    (*s)->calcCost(&bodyCost, context);
  cost->addCost(bodyCost, false, true, estimated_loop_count);
}

void NUTF::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  for (NUNamedDeclarationScopeCLoop scope = loopDeclarationScopes(); !scope.atEnd(); ++scope)
    (*scope)->calcCost(cost, context);

#if COUNT_NETS_FROM_DECLARATIONS
  // don't loop over blocks -- they're already counted in the stmtlist.
  for (NUNetCLoop n = loopLocals(); !n.atEnd(); ++n)
    (*n)->calcCost(cost, context);
#endif

  for (NUStmtCLoop s = loopStmts(); !s.atEnd(); ++s)
    (*s)->calcCost(cost, context);
}

void NUTriRegInit::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  mLvalue->calcCost(cost, context);
  MAXIFY (cost->mAsicGates += cTriRegBitCost * mLvalue->getBitSize());
}

void NUReadmemX::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  mLvalue->calcCost(cost, context);
  MAXIFY (cost->mInstructions += 10);
}

void NUOutputSysTask::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  ++cost->mSysTasks;
  // first add in the cost of the individual arguments
  for (NUExprCLoop e = loopCArgs(); !e.atEnd(); ++e){
    NUExpr* expr = *e;
    expr->calcCost(cost, context);
  }
  // and now the overhead cost.
  // the execution of an output sys task requires approximately 
  // 20 instructions for the simplest version of $display. Currently 20
  // is the threshold for in-lining functions, so use 21 to force the
  // code for the the $display to be placed outside of the header file.
  MAXIFY (cost->mInstructions += 21);
  MAXIFY (cost->mInstructionsRun += 21);
}

void NUFCloseSysTask::calcCost(NUCost* cost, NUCostContext*)
  const
{
  ++cost->mSysTasks;
  // rjc-todo we should also add the proper number of instructions and run instructions based on the size of the strings and the number of arguments
  MAXIFY (cost->mInstructions += 1);
  MAXIFY (cost->mInstructionsRun += 1);
}
void NUFOpenSysTask::calcCost(NUCost* cost, NUCostContext*)
  const
{
  ++cost->mSysTasks;
  // rjc-todo we should also add the proper number of instructions and run instructions based on the size of the strings and the number of arguments
  MAXIFY (cost->mInstructions += 1);
  MAXIFY (cost->mInstructionsRun += 1);
}
void NUInputSysTask::calcCost(NUCost* cost, NUCostContext*)
  const
{
  ++cost->mSysTasks;
  // rjc-todo we should also add the proper number of instructions and run instructions based on the size of the strings and the number of arguments
  MAXIFY (cost->mInstructions += 1);
  MAXIFY (cost->mInstructionsRun += 1);
}

void NUControlSysTask::calcCost(NUCost* cost, NUCostContext*)
  const
{
  ++cost->mSysTasks;
  // rjc-todo we should also add the proper number of instructions and
  // run instructions based on the estimiated size of the registered callbacks.
  MAXIFY (cost->mInstructions += 1);
  MAXIFY (cost->mInstructionsRun += 1);
}

void NUFFlushSysTask::calcCost(NUCost* cost, NUCostContext*)
  const
{
  ++cost->mSysTasks;
  // rjc-todo we should also add the proper number of instructions and run instructions based on the size of the strings and the number of arguments
  MAXIFY (cost->mInstructions += 1);
  MAXIFY (cost->mInstructionsRun += 1);
}

void NUSysFunctionCall::calcCost(NUCost* cost, NUCostContext* ctx) const
{
  ++cost->mSysFunctions;
  NUOp::calcCost(cost, ctx);
  MAXIFY (cost->mInstructions += 1);
  MAXIFY (cost->mInstructionsRun += 1);
}

void NUSysRandom::calcCost(NUCost* cost, NUCostContext* ctx) const
{
  ++cost->mSysFunctions;

  SInt32 n = ((SInt32) mValueExprVector.size()) - 1;
  UInt32 size = 32;
  MAXIFY (cost->mOps += n);
  MAXIFY (cost->mInstructions += n * sBitInstructions(size));
  MAXIFY (cost->mInstructionsRun += n * sBitInstructions(size));
  
  for (SInt32 i = 0; i <= n; ++i)
    ctx->calcExpr(mValueExprVector[i], cost);
}

void NUTFArgConnectionInput::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  getActual()->calcCost(cost, context);
}

void NUTFArgConnectionOutput::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  getActual()->calcCost(cost, context);
}

void NUTFArgConnectionBid::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  getActual()->calcCost(cost, context);
}

void NUPortConnectionInput::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  getActual()->calcCost(cost, context);
}

void NUPortConnectionOutput::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  getActual()->calcCost(cost, context);
}

void NUPortConnectionBid::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  getActual()->calcCost(cost, context);
}

void NUTaskEnable::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  if (isHierRef()) {
    ++cost->mHierTasks;
  }
  else {
    ++cost->mTasks;
  }

  // The cost of the connections.
  for (NUTFArgConnectionCLoop a = loopArgConnections(); !a.atEnd(); ++a)
    (*a)->calcCost(cost, context);

  // The cost of the called task.
  if (isHierRef()) {
    // Hierarchical task enable. getTask is not legal if there is more
    // than one resolution.
    const NUTaskHierRef * hier_ref = getHierRef();
    NUCost resolutionCost;
    bool gotResolutionCost = false;
    for (NUTaskHierRef::TaskVectorCLoop loop = hier_ref->loopResolutions();
         not loop.atEnd();
         ++loop) {
      const NUTask * resolution = (*loop);
      NUCost* taskCost = context->findCost(resolution);
      if (taskCost == NULL) {
        // Hier ref to a task without pre-existing cost information.
        context->calcTF(resolution);
        taskCost = context->findCost(resolution);
        NU_ASSERT(taskCost, resolution);
      }
      // This is pessimistic -- increment the instance count for each
      // potential resolution. In truth, we are instantiating only one
      // of these, but cannot determine which (unelaboratedly).
      if (context->getUpdateInstances()) {
        ++taskCost->mInstances;
      }

      // Give the task enable the cost of the first resolution. We may
      // eventually want to generate the maximum cost of all possible
      // resolutions and use that instead.
      if (not gotResolutionCost) {
        gotResolutionCost = true;
        resolutionCost = (*taskCost);
      }
    }
    // we add the execution cost but not the syntactic cost
    cost->addCost(resolutionCost, false, context->getTaskEnablesHaveTaskCost(), 1);
  } else {
    // Non hierarchical task enable. Calling getTask() is legal.
    NUTask * task = getTask();
    NUCost* taskCost = context->findCost(task);
    if (taskCost == NULL) {
      // Reference to a task without pre-existing cost information.
      // With flattening and hier-refs, a task may end up defined
      // after it is used.
      context->calcTF(task);
      taskCost = context->findCost(task);
      NU_ASSERT(taskCost, task);
    }
    if (context->getUpdateInstances()) {
      ++taskCost->mInstances;
    }
    // we add the execution cost but not the syntactic cost
    cost->addCost(*taskCost, false, context->getTaskEnablesHaveTaskCost(), 1);
  }
}

void NUInitialBlock::calcCost(NUCost* cost, NUCostContext* context )
  const
{
  ++cost->mInitialBlocks;
  SInt32 start_gate_count = cost->mAsicGates;
  getBlock()->calcCost(cost, context);
  cost->mAsicGates = start_gate_count; // initial blocks do not add to the gate count
}

void NUModule::calcCost(NUCost*, NUCostContext*)
  const
{
  NU_ASSERT(0, this);
}


void NUModuleInstance::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  for (NUPortConnectionCLoop p = loopPortConnections(); !p.atEnd(); ++p)
    (*p)->calcCost(cost, context);
}

void NUCycle::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  NUCost* myCost = context->getCycleCost(this);
  if (myCost == NULL)
  {
    NUCost cycleCost;
    for (FlowLoop p = loopFlows(); !p.atEnd(); ++p)
    {
      FLNodeElab* flow = *p;
      NUUseDefNode* node = flow->getUseDefNode();
      if (node != NULL)
        node->calcCost(&cycleCost, context);
    }
    context->putCycleCost(this, cycleCost);
    myCost = context->getCycleCost(this);
  }
  // assume the cycle will execute twice -- just a guess
  cost->addCost(*myCost, false, true, 2);
}

static void sAssignCost(const NUAssign* assign, NUCost* cost,
                        NUCostContext* context)
{
  NULvalue* lv = assign->getLvalue();
  NUExpr* rv = assign->getRvalue();
  lv->calcCost(cost, context);
  rv->calcCost(cost, context);
//  if (lv->isWholeIdentifier() && rv->isWholeIdentifier())
//    cost->mBufAssigns.addNet(lv->determineBitSize());
}

void NUBlockingAssign::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  ++cost->mBlockingAssigns;
  sAssignCost(this, cost, context);
}

void NUNonBlockingAssign::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  ++cost->mNonBlockingAssigns;
  sAssignCost(this, cost, context);
}

void NUContAssign::calcCost(NUCost* cost, NUCostContext* context)
  const
{
  ++cost->mContAssigns;
  sAssignCost(this, cost, context);
}

// Compute costs for the net as a function of a specific width
static void sNetCostHelper (const NUNet*n, UInt32 width, NUCost* cost)
{
  if (n->isHierRef()) {
    ++cost->mHierNets;
  }
  else {
    const NUMemoryNet* mem = n->getMemoryNet();
    if (mem != NULL) {
      width = mem->getWidthRange()->getLength();
    }

    if (n->isBid())               cost->mBidirects.addNet(width);
    else if (n->isInput())        cost->mInputs.addNet(width);
    else if (n->isOutput())       cost->mOutputs.addNet(width);
    else if (n->isTemp())         cost->mTemps.addNet(width);
    else                          cost->mLocalNets.addNet(width);
    if (n->isDoubleBuffered())    cost->mDoubleBuffered.addNet(width);
    if (n->isInClkPath())         cost->mClkPath.addNet(width);
    if (n->isInteger())           cost->mIntegers.addNet(width);
    if (n->isTriWritten())        cost->mTristates.addNet(width);
    if ((mem == NULL) && !n->isNonStatic() && !n->isBlockLocal()) {
      if (n->isLatch()) {
        cost->mLatches.addNet(width);
        MAXIFY (cost->mAsicGates += cLatchBitCost*width);
      }
      else if (n->isFlop()) {
        cost->mFlops.addNet(width);
        MAXIFY (cost->mAsicGates += cFlopBitCost*width);
      }
    } // else
  }
} // static void sNetCostHelper

void NUNet::calcCost(NUCost* cost, NUCostContext*)
  const
{
  sNetCostHelper (this, 1, cost);
}

void NUVectorNet::calcCost(NUCost* cost, NUCostContext*)
  const
{
  sNetCostHelper (this, getBitSize (), cost);
}

void NUMemoryNet::calcCost(NUCost* cost, NUCostContext*)
  const
{
  sNetCostHelper(this, getBitSize (), cost);

  ++cost->mMemories.mVars;
  UInt64 depth = getDepth();
  UInt32 width = getWidthRange()->getLength();
  cost->mMemories.mBits += depth * width;
  cost->mMemories.mAddrs += depth;
  cost->mMemories.mWritePorts += getNumWritePorts();
}

void NUTempMemoryNet::calcCost(NUCost* cost, NUCostContext*)
  const
{
  // dynamic temps have a worst case address-write cost of the whole depth
  const NUMemoryNet* mem = getMaster();
  SInt32 numWrites = isDynamic()? mem->getDepth(): getNumWritePorts();
  SInt32 wordWidth = sBitInstructions(mem->getRowSize());
  MAXIFY (cost->mInstructions +=  numWrites * wordWidth);
}

void NUCModelArgConnectionInput::calcCost(NUCost* cost, NUCostContext* cntxt)
  const
{
  ++cost->mCModelArgs;

  // Calculate the cost of the actual
  NUExpr* actual = getActual();
  actual->calcCost(cost, cntxt);
  UInt32 bitSize = actual->getBitSize();

  // The number of assigns is dependent on bit size.  Here is some
  // code generated for a simple 32-bit port, which requires no data
  // translation or mutation at all:
  //
  // CarbonUInt32* tmp_1_in = 0;  (the =0 will probably be optimized away)
  // CarbonUInt32 tmp2_1_in;
  // tmp_1_in = &tmp2_1_in;
  // tmp2_1_in = m_in;
  // ...run...
  // If the bit size is between 33 and 64, then there will be some extra
  // instructions for decomposing the internal UInt64 into a UInt32 array
  // stored locally
  UInt32 extraAssigns = 2;
  if ((bitSize <= 64) && (bitSize > 32)) {
    ++extraAssigns;
  }
  cost->mBlockingAssigns += extraAssigns;
  MAXIFY (cost->mInstructions += 3*extraAssigns); // load+modify+store = 3
  MAXIFY (cost->mInstructionsRun += 3*extraAssigns);
} // void NUCModelArgConnectionInput::calcCost

void NUCModelArgConnectionOutput::calcCost(NUCost* cost, NUCostContext* cntxt)
  const
{
  ++cost->mCModelArgs;
  // Calculate the cost of the actual
  NULvalue* actual = getActual();
  actual->calcCost(cost, cntxt);

  // There is a temp assignment for any bit-size, even 32-bit signals.
  // Here is a very simple 32-bit output-arg:
  /*
   *  // Convert the arguments to UInt32* as necessary
   * CarbonUInt32* tmp_2_out = 0;  (assign to 0 probably optimized away)
   * tmp_2_out = (&get_out());
   * ...run ....
   * // Convert any data back to Carbon format as necessary
   * get_out()&= carbon_mask<CarbonUInt32>(32);
   */
  // I see two assignments here, plus for non-32-bit values
  UInt32 num_assigns = 2;
  cost->mBlockingAssigns += num_assigns;
  MAXIFY (cost->mInstructions += 3*num_assigns);
  MAXIFY (cost->mInstructionsRun += 3*num_assigns);
  
  // In addition, I'm throwing in two instructions for the mask.
  // With these values, we contrive to get the cost for a simple
  // 1-input 1-output C-model call to 21, which forces it out of
  // line with the default inline thresholds.
  MAXIFY (cost->mInstructions += 2);
  MAXIFY (cost->mInstructionsRun += 2);
} // void NUCModelArgConnectionOutput::calcCost

void NUCModelArgConnectionBid::calcCost(NUCost* cost, NUCostContext* cntxt)
  const
{
  ++cost->mCModelArgs;

  // Calculate the cost of the actual
  NULvalue* actual = getActual();
  actual->calcCost(cost, cntxt);

  // The number of assigns is based on the bit size. See
  // NUCModelArgConnection::emitCode for details.
  UInt32 bitSize = actual->getBitSize();
  if ((bitSize <= 16) || ((bitSize > 32) && (bitSize <= 64))) {
    ++cost->mBlockingAssigns;
    MAXIFY (cost->mInstructions += 3); // load+modify+store = 3
    MAXIFY (cost->mInstructionsRun += 3);
  }
}

void NUCModelCall::calcCost(NUCost* cost, NUCostContext* context) const
{
  ++cost->mCModelCalls;

  // Add an instruction for each arg that we have to push onto the stack,
  // plus an instruction for the call itself, and also two for the
  // handle and timing args
  MAXIFY (cost->mInstructions += 3 + mArgs->size());
  MAXIFY (cost->mInstructionsRun += 3 + mArgs->size());

  for (NUCModelArgConnectionCLoop l = loopArgConnections(); !l.atEnd(); ++l) {
    (*l)->calcCost(cost, context);
  }
}

static const char* getModName(const NUBase* obj)
{
  const NUModule* mod = dynamic_cast<const NUModule*>(obj);
  if (mod != NULL)
    return mod->getName()->str();
  const NUTF* tf = dynamic_cast<const NUTF*>(obj);
  NU_ASSERT(tf, obj);           // not a module or task
  return tf->getParentScope()->getName()->str();
}

bool NUCostContext::HashModFunc::lessThan(const NUBase* v1, const NUBase* v2)
  const
{
/*
  mBuf1.clear();
  getFullNname(v1, &mBuf1);
  mBuf2.clear();
  getFullNname(v2, &mBuf2);
  return strcmp(mBuf1.c_str(), mBuf2.c_str()) < 0;
*/

  // Note that a t
  if (v1 == v2)
    return false;
  int cmp = strcmp(getModName(v1), getModName(v2));
  if (cmp == 0)
  {
    // I must be comparing a TF to its parent module, or a TF
    // to another one in the same module
    const NUTF* tf1 = dynamic_cast<const NUTF*>(v1);
    const NUTF* tf2 = dynamic_cast<const NUTF*>(v2);
    NU_ASSERT2((tf1 != NULL) || (tf2 != NULL), v1, v2);
    if (tf1 == NULL)
      cmp = -1;                 // modules are "less than" their child TFs
    else if (tf2 == NULL)
      cmp = 1;
    else
    {
      // They are both tasks.  They should be of the same module
      NU_ASSERT2(tf1->getParentScope() == tf2->getParentScope(), tf1, tf2);
      cmp = HierName::compare(tf1->getNameBranch(), tf2->getNameBranch());
      NU_ASSERT2(cmp != 0, tf1, tf2);
    }
  }
  return cmp < 0;
} // bool NUCostContext::HashModFunc::lessThan


// Calculate the cost of an expression, memoizing costs for expressions
// that are managed in a factory.  This is necessary if a DFS of an
// expression graph would be very costly, which can only happen in
// a factory-managed expression.
void NUCostContext::calcExpr(const NUExpr* expr, NUCost* cost)
{
  if (expr->isManaged())
  {
    NUCost* selfCost = mSelfCostMap[expr];
    if (selfCost == NULL)
    {
      selfCost = allocCost(expr, &mSelfCostMap);
      expr->calcCost(selfCost, this);
    }
    cost->addCost(*selfCost, false);
  }
  else
    expr->calcCost(cost, this);
}

void NUCostContext::clear() {
  for (CostMap::iterator p = mSelfCostMap.begin(); p != mSelfCostMap.end(); ++p)
    delete p->second;
  for (CostMap::iterator p = mDeepCostMap.begin(); p != mDeepCostMap.end(); ++p)
    delete p->second;
  mSelfCostMap.clear();
  mDeepCostMap.clear();
  mDesignCost.clear();
}
