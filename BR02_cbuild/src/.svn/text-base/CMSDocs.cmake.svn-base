project(CMSDocs)

# CMake has a doxygen package, but it doesn't do anything other than
# find the executables, so it's not really worth it.  Plus, we have a
# wrapper script to set LD_LIBRARY_PATH correctly.
set(DOXYGEN ${CARBON_HOME}/scripts/rundoxygen ${TOOLS_DIR}/doxygen-1.4.5/bin/doxygen)

# The old makefile for the dox target used a bunch of phony targets,
# so they were always built.  We'll do the same thing here, i.e. use
# cmake custom targets.

set(HtmlDoxyfile ${CARBON_SANDBOX}/src/doc/doxy)
add_custom_target(Html ALL ${DOXYGEN} ${HtmlDoxyfile}
                  DEPENDS ${HtmlDoxyfile}
                  WORKING_DIRECTORY ${CARBON_OBJ}
                  COMMENT Running doxygen for ${HtmlDoxyfile}
                  )

# The remaining rules are mostly similar, so create macros to add them.
macro(add_doc_target targetName doxyFile docDir)
  add_custom_target(${targetName} ALL ${DOXYGEN} ${doxyFile}
                    COMMAND ${CARBON_SANDBOX}/src/doc/fixfooter_latex.pl -m doc/${docDir}
                    COMMAND cd doc/${docDir}_latex && $(MAKE)
                    DEPENDS ${doxyFile}
                    WORKING_DIRECTORY ${CARBON_OBJ}
                    COMMENT Running doxygen for ${doxyFile}
                    )
endmacro()

macro(add_doc_target_fix_index targetName doxyFile docDir)
  add_custom_target(${targetName} ALL ${DOXYGEN} ${doxyFile}
                    COMMAND ${CARBON_SANDBOX}/src/doc/fixfooter_latex.pl -m doc/${docDir}
                    COMMAND ${CARBON_SANDBOX}/src/doc/fixindex.pl -m doc/${docDir}
                    COMMAND cd doc/${docDir}_latex && $(MAKE)
                    DEPENDS ${doxyFile}
                    WORKING_DIRECTORY ${CARBON_OBJ}
                    COMMENT Running doxygen for ${doxyFile}
                    )
endmacro()


add_doc_target_fix_index(Api ${CARBON_SANDBOX}/src/doc/shelldoxybrief vhm_api)
add_doc_target(Carbonsim ${CARBON_SANDBOX}/src/doc/carbonsimapi vm_api)
add_doc_target_fix_index(PliWrapper ${CARBON_SANDBOX}/src/doc/pliwrapper vhm_pli)
add_doc_target_fix_index(VhmDbApi ${CARBON_SANDBOX}/src/doc/vhmdbapi vhm_dbapi)
add_doc_target_fix_index(VhmDbCApi ${CARBON_SANDBOX}/src/doc/vhmdbcapi vhm_dbcapi)
add_doc_target(SystemCWrapper ${CARBON_SANDBOX}/src/doc/systemcwrapper vhm_scw)
add_doc_target_fix_index(SystemCPcie ${CARBON_SANDBOX}/src/doc/systemcpcie pcie_sysc)
add_doc_target_fix_index(SystemApi ${CARBON_SANDBOX}/src/doc/systemapi system_api)
