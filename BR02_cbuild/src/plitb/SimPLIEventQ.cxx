// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implementation of the SimPLIEventQ and auxiliary classes
*/

#include "SimPLISignal.h"
#include "SimPLIEventQ.h"
#include "SimPLI.h"
#include "waveform/WfAbsSignalVCIter.h"
#include "util/UtConv.h"

/*!
  \param sig_p Signal that this entry tracks
 */
SimPLIEventQEntry::SimPLIEventQEntry(SimPLISignal * const sig_p)
    : mSig(sig_p),
      mcType(sig_p->getType())
{
    mTime = sig_p->getIter()->getTime();
}

SimPLIEventQEntry::~SimPLIEventQEntry()
{
    // When the entry in the event queue is no longer needed,
    // our signal has seen the end of its life
    delete mSig;
}

/*!
  \return Pointer to the signal controlled by this entry
 */
SimPLISignal * SimPLIEventQEntry::getSignal() const
{
    return mSig;
}

/*!
  \return Simulation time of the signal's next value change in the waveform file
 */
UInt64 SimPLIEventQEntry::getTime() const
{
    return mTime;
}

/*!
  Attempts to find a future value change in the waveform file for this signal,
  and updates the event time if successful

  \retval true Another event was found
  \retval false No more events for this signal were found
 */
bool SimPLIEventQEntry::nextEvent()
{
    // try to move to the next event, and return whether
    // we were successful
    mSig->getIter()->gotoNextTime();

    // this failed if we're at max time
    if (mSig->getIter()->atEnd())
	return false;

    mTime = mSig->getIter()->getTime();
    return true;
}

/*!
  Resets the iterator for this signal to the beginning of the waveform,
  and updates the time of this entry accordingly.
*/
void SimPLIEventQEntry::Reset()
{
    mSig->getIter()->gotoMinTime();
    mTime = mSig->getIter()->getTime();
}

SimPLIEventQ::SimPLIEventQ()
{
    mCurrTime = 0;
}

SimPLIEventQ::~SimPLIEventQ()
{
    SimPLIEventQEntry *ptr;

    while (!mQ.empty()) {
	ptr = mQ.top();
	mQ.pop();
	delete ptr;
    }

    while (!mFinished.empty()) {
	ptr = mFinished.front();
	mFinished.pop_front();
	delete ptr;
    }
}

/*!
  Adds a signal to the event queue and schedules it among the other entries

  \param sig_p Pointer to the signal to schedule
 */
void SimPLIEventQ::Schedule(SimPLISignal * const sig_p)
{
    SimPLIEventQEntry * const entry_p = new SimPLIEventQEntry(sig_p);
    mQ.push(entry_p);

    // keep this accurate
    mCurrTime = getTime();
}

/*!
  \return Time of the next event
 */
UInt64 SimPLIEventQ::getTime() const
{
    return mQ.top()->getTime();
}

/*!
  \retval true The event queue is empty
  \retval false The event queue is not empty
 */
bool SimPLIEventQ::Empty() const
{
    return mQ.empty();
}

/*!
  As events from the current timestamp are processed and removed from
  the event queue, they are stored in a list.  If the signals in that
  list have more events remaining, this function will update them
  and reschedule them in the queue.  If there are no events remaining,
  the event object is put away in case of multiple iterations of the
  simulation.
 */
void SimPLIEventQ::Reschedule()
{
    SimPLIEventQEntry *entry_p;

    // Go through all signals we removed from the queue in this
    // timestamp, and reschedule them if they have any events
    // remaining.  If the signal has no more events, save it in
    // a separate list in case we're doing multiple iterations of
    // the simulation.

    while (!mSched.empty()) {
	entry_p = mSched.front();
	mSched.pop_front();
	if (entry_p->nextEvent())
	    mQ.push(entry_p);	// re-insert it - order will reflect the new timestamp
	else
	    mFinished.push_back(entry_p);
    }

    // Update time if there are events remaining
    if (!Empty())
	mCurrTime = getTime();
}

/*!
  Attempts to get a signal and its value for the current timestamp.
  This succeeds only if the time of the event at the head of the queue
  matches the current simulation time.

  \param sig_p Pointer to the SimPLISignal object for the signal that changed (returned by pointer)
  \param value Value of the signal (returned by reference)
  \retval true A signal was found for this timestamp
  \retval false No more signals found for this timestamp

  \sa NextSignal
 */
bool SimPLIEventQ::getSignalValue(SimPLISignal ** const sig_p,
				  UtString * const value) const
{
    // We can't return another signal for this timestamp if the
    // queue is empty, or if the event's timestamp is greater
    // than the current time.
    if (Empty() ||
	(getTime() != mCurrTime))
	return false;

    // Else, just copy the info
    *sig_p = mQ.top()->getSignal();
    (*sig_p)->getIter()->getValueStr(value);
    return true;
}


/*!
  Moves to the next signal in the current timestamp.
  This must be called after a successful GetSignalValue to
  advance to the next signal.

  \sa GetSignalValue
 */
void SimPLIEventQ::nextSignal()
{
    SimPLIEventQEntry *entry_p;
    // We need to save the signals we've popped off so
    // we can reschedule them

    entry_p = mQ.top();
    mSched.push_back(entry_p);
    mQ.pop();
}

void SimPLIEventQ::Restart()
{
    SimPLIEventQEntry *entry_p;

    while (!mFinished.empty()) {
	entry_p = mFinished.front();
	mFinished.pop_front();
	entry_p->Reset();
	mQ.push(entry_p);	// re-insert it - order will reflect the new timestamp
    }

    mCurrTime = getTime();
}
