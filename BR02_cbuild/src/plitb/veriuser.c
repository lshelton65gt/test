/*
 * |-----------------------------------------------------------------------|
 * |                                                                       |
 * |   Copyright Cadence Design Systems, Inc. 1985, 1988.                  |
 * |     All Rights Reserved.       Licensed Software.                     |
 * |                                                                       |
 * |                                                                       |
 * | THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF CADENCE DESIGN SYSTEMS |
 * | The copyright notice above does not evidence any actual or intended   |
 * | publication of such source code.                                      |
 * |                                                                       |
 * |-----------------------------------------------------------------------|
 */

/*
 * |-------------------------------------------------------------|
 * |                                                             |
 * | PROPRIETARY INFORMATION, PROPERTY OF CADENCE DESIGN SYSTEMS |
 * |                                                             |
 * |-------------------------------------------------------------|
 */

/*****************************************************************************
*   This is the `veriuser.c' file.  For more information about the contents
*   of this file, please see `veriuser.doc'.
*****************************************************************************/

#include "veriuser.h"
#include "vxl_veriuser.h"

char *veriuser_version_str = "";

int (*endofcompile_routines[TF_MAXARRAY])() = 
{
    /*** my_eoc_routine, ***/
    0 /*** final entry must be 0 ***/
};

bool err_intercept(level,facility,code)
int level; char *facility; char *code;
{
  (void) level; (void)facility; (void)code;

  return(true);
}

extern int init_vcd_call();
extern int init_vcd_misc(int data, int reason);
extern int add_clock_check();
extern int add_clock_call();
extern int add_input_check();
extern int add_input_call();
extern int add_output_check();
extern int add_output_call();
extern int add_bidir_check();
extern int add_bidir_call();
extern int add_enable_check();
extern int add_enable_call();
extern int drive_clocks_call();
extern int drive_clocks_misc(int data, int reason);
extern int drive_signals_call();
extern int drive_signals_misc(int data, int reason);
extern int incr_err_count_call();
extern int set_options_call();

#define Z8 0,0,0,0,0,0,0,0
s_tfcell veriusertfs[TF_MAXARRAY] =
{
    /*** Template for an entry:
    { usertask|userfunction, data,
      checktf(), sizetf(), calltf(), misctf(),
      "$tfname", forwref?, Vtool?, ErrMsg? },
    Example:
    { usertask, 0, my_check, 0, my_func, my_misctf, "$my_task" },
    ***/

    /*** add user entries here ***/

    { usertask, 0, 0, 0, init_vcd_call, init_vcd_misc, "$init_vcd", Z8 },
    { usertask, 0, add_clock_check, 0, add_clock_call, 0, "$add_clock", Z8 },
    { usertask, 0, add_input_check, 0, add_input_call, 0, "$add_input", Z8 },
    { usertask, 0, add_output_check, 0, add_output_call, 0, "$add_output", Z8 },
    { usertask, 0, add_bidir_check, 0, add_bidir_call, 0, "$add_bidir", Z8 },
    { usertask, 0, add_enable_check, 0, add_enable_call, 0, "$add_enable", Z8 },
    { usertask, 0, 0, 0, drive_clocks_call, drive_clocks_misc, "$drive_clocks", Z8 },
    { usertask, 0, 0, 0, drive_signals_call, drive_signals_misc, "$drive_signals", Z8 },
    { usertask, 0, 0, 0, incr_err_count_call, 0, "$incr_err_count", Z8 },
    { usertask, 0, 0, 0, set_options_call, 0, "$set_options", Z8 },

    { 0, 0, 0, 0, 0, 0, 0, Z8 } /*** final entry must be 0 ***/
#undef Z8
};


