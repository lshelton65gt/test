// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implementation of the IODBIntf class
*/

#include "IODBIntf.h"
#include "util/UtIOStream.h"
#include "iodb/IODBTypes.h"
#include "exprsynth/ExprFactory.h"
#include "util/UtList.h"

/*
  \param filename IODB file to be opened
*/
IODBIntf::IODBIntf(const UtString &filename)
    : mcFilename(filename)
{
    m_errStream = new MsgStreamIO(stderr, true);
    m_msgContext.addReportStream(m_errStream);
    m_bom.putScheduleFactory(&m_scheduleFactory);
    m_designSymTab = new STSymbolTable(&m_bom, &m_atomicCache);
    //				       new HdlVerilogPath ());
    mExprFactory = new ESFactory;
    // PLITB doesn't need a source locator factory
    m_iodb = new IODBRuntime(&m_atomicCache, m_designSymTab, &m_msgContext,
                             &m_scheduleFactory, mExprFactory, NULL);

    INFO_ASSERT(m_iodb->readDB(mcFilename.c_str(), eCarbonIODB), "Problem reading iodb file" );
}

IODBIntf::~IODBIntf()
{
    delete m_iodb;
    delete m_designSymTab;
    delete m_errStream;
    delete mExprFactory;
}

/*
  Extracts signal information from the IODB file, and returns lists of
  four categories of signals.  The lists are mutually exclusive - clock
  signals appear only in the clock list even though they are technically
  inputs as well.

  \param clocks Pointer to the list of clock signals to be returned
  \param clocks Pointer to the list of input signals to be returned
  \param clocks Pointer to the list of output signals to be returned
  \param clocks Pointer to the list of bidirectional signals to be returned
*/
void IODBIntf::getSignalLists(UtList<SignalInfo *> * const clocks,
			      UtList<SignalInfo *> * const inputs,
			      UtList<SignalInfo *> * const outputs,
			      UtList<SignalInfo *> * const bidirs)
{
    UtList<SignalInfo *>::iterator iter;
    
    clocks->clear();
    getSignals(clocks, m_iodb->loopClocks());
    inputs->clear();
    getSignalsExclude(inputs, clocks, m_iodb->loopInputs());
    outputs->clear();
    getSignals(outputs, m_iodb->loopOutputs());
    bidirs->clear();
    getSignals(bidirs, m_iodb->loopBidis());

    // The list of clocks includes all clocks at the top level, even if
    // they're not primary inputs.  We need to strip those out now.
    // Luckily, this is easy because every input clock is an input as
    // well, so in the call to getSignalsExclude we've marked these
    // entries with a flag that they matched one of the input signals.
    // We can just go through the list and remove all entries without
    // this flag set.
    iter = clocks->begin();
    while (iter != clocks->end()) {
	if ((*iter)->excluded)
	    ++iter;
	else
	    iter = clocks->erase(iter);
    }
}

/*
  \param list_ptr Pointer to the list to fill with signals
  \param loop Looping function specifying the type of signals to return
 */
void IODBIntf::getSignals(UtList<SignalInfo *> * const list_ptr, IODB::NameSetLoop loop)
{
    STSymbolTableNode* node;
    UtString buf;
    UtString::size_type match_pos;
    SignalInfo *sinfo;
    int width;
    while (loop(&node)) {
	buf.clear();
	m_iodb->getIOSymbolTable()->getHdlHier()->compPathHier(node, &buf);
        const IODBIntrinsic *intrinsic = m_iodb->getLeafIntrinsic(node->castLeaf());
        FUNC_ASSERT( intrinsic, node->print() );
	width = intrinsic->getWidth();
	// strip off the top module
	match_pos = buf.find(".");
	if (match_pos != UtString::npos)
	    buf = buf.substr(match_pos + 1);

	// Don't insert any signal that's not at the top level
	if (buf.find(".") == UtString::npos) {
	    sinfo = new SignalInfo(buf, width);
	    list_ptr->push_back(sinfo);
	}
    }
}

/*
  \param list_ptr Pointer to the list to fill with signals
  \param excl_ptr List of signals to exclude from this new list
  \param loop Looping function specifying the type of signals to return
 */
void IODBIntf::getSignalsExclude(UtList<SignalInfo *> * const list_ptr, const UtList<SignalInfo *> * const excl_ptr, IODB::NameSetLoop loop)
{
    STSymbolTableNode* node;
    UtString buf;
    UtString::size_type match_pos;
    SignalInfo *sinfo;
    int width;
    bool found;
    UtList<SignalInfo *>::const_iterator iter;
    while (loop(&node)) {
	buf.clear();
	m_iodb->getIOSymbolTable()->getHdlHier()->compPathHier(node, &buf);
        const IODBIntrinsic *intrinsic = m_iodb->getLeafIntrinsic( node->castLeaf() );
        FUNC_ASSERT( intrinsic, node->print() );
	width = intrinsic->getWidth();
	// strip off the top module
	match_pos = buf.find(".");
	if (match_pos != UtString::npos)
	    buf = buf.substr(match_pos + 1);

	// Don't insert any signal that's not at the top level
	if (buf.find(".") == UtString::npos) {
	    found = false;
	    // Don't add the signal if it's already in the exclude list.
	    // But, if it's there, tag the entry in the exclude list
	    for (iter = excl_ptr->begin(); !found && iter != excl_ptr->end(); ++iter)
		if (buf == (*iter)->name) {
		    found = true;
		    break;
		}

	    if (!found) {
		sinfo = new SignalInfo(buf, width);
		list_ptr->push_back(sinfo);
	    } else
		(*iter)->excluded = true;
	}
    }
}
