// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Header for the IODBIntf class
*/

#ifndef __IODBINTF_H__
#define __IODBINTF_H__

#include "iodb/IODBRuntime.h"
#include "hdl/HdlVerilogPath.h"
#include "util/AtomicCache.h"
#include "symtab/STSymbolTableNode.h"
#include "util/ShellMsgContext.h"
#include "iodb/ScheduleFactory.h"
#include "shell/CarbonDBRead.h"
#include "shell/CarbonNet.h"
#include "util/UtString.h"

class ESFactory;

/*!
  \brief A struct to store attributes of signals being traced
*/
struct SignalInfo
{
    UtString name;			//!< The name of the signal, including any hierarchy below the module being tested
    UtString mang_name;			//!< A mangled version of the name, used in register declarations
    int width;				//!< Bit width of the signal
    UtString enable;			//!< Verilog expression of the enable pin, if the signal is bidirectional
    bool ignore;			//!< Flag to ignore this signal when comparing values
    UtString forced_value;		//!< Value to force this signal to during simulation
    int delay;				//!< Simulation time to delay before comparing this signal
    bool excluded;			//!< Indicates if this signal was excluded in a call to getSignalsExclude

    //! Constructor
    SignalInfo(UtString &name_in, int width_in = 1)
	: name(name_in),
	 mang_name(name_in),
	 width(width_in),
	 enable(""),
	 ignore(false),
	 forced_value(""),
	 delay(1),
	 excluded(false)
    {
	// Replace any "." with "__", so that the resulting
	// string will be a valid verilog register name
	UtString::size_type pos = mang_name.find(".");
	while (pos != UtString::npos) {
	    mang_name.replace(pos, 1, "__");
	    pos = mang_name.find(".");
	}
    }
};

/*!
  \brief Interface class to read signal information from the IODB file

  This class reads an IODB file, and extracts signal information from it.
  This information is returned in four lists:  clocks, inputs, outputs,
  and bidirs.  The contents of the lists are mutually exclusive.

  Much of this class was borrowed from carbondb.cxx
*/
class IODBIntf
{
 public:
    IODBIntf(const UtString &filename);		//!< Constructor
    ~IODBIntf();					//!< Destructor

    void getSignalLists(UtList<SignalInfo *> * const clocks,
			UtList<SignalInfo *> * const inputs,
			UtList<SignalInfo *> * const outputs,
			UtList<SignalInfo *> * const bidirs);	//!< Gets lists of signals from the database

 protected:
    const UtString mcFilename;			//!< database filename
    AtomicCache m_atomicCache;

    MsgStreamIO *m_errStream;
    MsgContext m_msgContext;
    SCHScheduleFactory m_scheduleFactory;
    ShellSymTabBOM m_bom;
  
    STSymbolTable *m_designSymTab;
    IODBRuntime *m_iodb;
    ESFactory* mExprFactory;

    //! Gets signals of a specific type
    void getSignals(UtList<SignalInfo *> * const list_ptr, IODB::NameSetLoop loop);
    //! Gets signals of a specific type, excluding those already contained in another list
    void getSignalsExclude(UtList<SignalInfo *> * const list_ptr, const UtList<SignalInfo *> * const excl_ptr, IODB::NameSetLoop loop);

};

#endif
