// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Header for the SimPLISignal class
*/

#ifndef __SIMPLISIGNAL_H__
#define __SIMPLISIGNAL_H__

#include "util/UtString.h"

class WfAbsSignal;
class WfAbsSignalVCIter;
class WfAbsFileReader;

/*!
  \brief Class to track a signal being used in the simulation

  This class encapsulates various information about signals being
  used in the simulation.  Specifically, it stores the instance
  of the signal as a parameter in the PLI call, to allow writing
  its value.  It also controls the interface to the signal in
  the waveform file.
 */
class SimPLISignal
{
public:
    SimPLISignal(char * const instance,
		 WfAbsSignal * const wf_sig,
		 int bits,
		 int type);				//!< Constructor
    ~SimPLISignal();					//!< Destructor
    char * getInstance() const;                         //!< Gets the instance pointer for the signal
    void setPartner(SimPLISignal *partner);		//!< Stores a partner signal
    const SimPLISignal * getPartner() const;		//!< Returns a signal's partner
    bool initIter(WfAbsFileReader* reader);					//!< Initializes the signal's iterator in the waveform file
    void getName(UtString * const name) const;		//!< Returns the name of the signal in the waveform file
    int getNumBits() const;				//!< Returns the bit width of the signal
    int getType() const;				//!< Returns the type of the signal
    WfAbsSignalVCIter * getIter() const;		//!< Returns the waveform iterator for the signal
  
protected:
    char * const mInstance;				//!< Pointer to the instance of the PLI call where the signal was referenced
    const int mcBits;					//!< Number of bits in the signal
    const int mcType;					//!< Type of signal (see SimPLI::SignalTypes)
    SimPLISignal * mPartner;				//!< SimPLISignal object for the other half of the signal/enable pair, if applicable
    WfAbsSignal * const mWfSignal;			//!< Object connecting this signal to the waveform file
    WfAbsSignalVCIter *mWfSignalIter;			//!< Iterator for the signal in the waveform file
};

#endif
