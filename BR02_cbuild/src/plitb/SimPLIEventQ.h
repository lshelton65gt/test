// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Header for the SimPLIEventQ and auxiliary classes
*/

#ifndef __SIMPLIEVENTQ_H__
#define __SIMPLIEVENTQ_H__

#include <queue>
#include "SimPLI.h"	// for SimPLI::eCLOCK
#include "util/UtList.h"
#include "util/UtVector.h"

class SimPLISignal;
class SimPLIEventQEntry;



/*!
  \brief Entry for a signal value change event

  This class stores information relating to a signal and its
  next scheduled value change in the waveform file.
 */
class SimPLIEventQEntry {
 public:
    SimPLIEventQEntry(SimPLISignal * const sig_p);	//!< Constructor
    ~SimPLIEventQEntry();				//!< Destructor
    SimPLISignal * getSignal() const;		//!< Returns a pointer to the signal
    UInt64 getTime() const;				//!< Returns the time of the event
    bool nextEvent();					//!< Advances to the next event for this signal
    void Reset();					//!< Resets internal iterator to the beginning of the waveform

    /*!
      \brief Functor comparitor class for ordering events

      This functor implements a greater-than comparison on objects
      with member times and types, specifically those of type
      SimPLIEventQEntry.  It is used as the ordering predicate for
      the priority queue that stores events. The comparison is done
      first on the time of the signal's event, and (if the times
      match) on the signal type.  Non-clocks are considered greater
      than clock signals so that clock events are given priority
      within a timestamp.
    */
    template<class T>
    class Greater
    {
    public:

	/*!
	  \brief The comparison function

	  \param lhs Pointer to the left-hand side of the comparison
	  \param rhs Pointer to the right-hand side of the comparison
	  \retval true lhs is greater than rhs
	  \retval false lhs is not greater than rhs
	*/
	bool operator()(const T * const lhs,
			const T * const rhs) const
	{
	    // This should return true if the LHS has a higher (i.e. later)
	    // timestamp than the RHS.  If two signals have the same
	    // timestamp, we should return false, *UNLESS* the LHS is
	    // an ordinary signal and the RHS is a clock.  This ensures
	    // that clocks will always precede signals at the same
	    // timestamp, which is necessary because clocks are driven
	    // first through the PLI.

	    return ((lhs->mTime > rhs->mTime) ||
		    ((lhs->mTime == rhs->mTime) &&
		     (lhs->mcType != SimPLI::eCLOCK) &&
		     (rhs->mcType == SimPLI::eCLOCK)));
	}
    };

    SimPLISignal * const mSig;				//!< Pointer to the signal for this event
    const int mcType;					//!< Type of signal this is
    UInt64 mTime;					//!< Time of the next event for this signal
};

/*!
  \brief Priority queue for scheduling and retrieving signal events

  This class tracks all signal value changes in the simulation and 
  schedules them to be processed.  For each signal, the time when
  its next value change occurs is determined.  Using this information,
  the signal events are stored in a priority queue, so that the value
  changes can be processed in the correct order.  When a current event
  is removed from the queue, and there are future value changes
  remaining for that signal, the event is updated and rescheduled in
  the queue.

  \sa SimPLIEventQEntry
 */
class SimPLIEventQ
{
    template<class T> friend class EventQGreater;

 public:
    SimPLIEventQ();					//!< Constructor
    ~SimPLIEventQ();					//!< Destructor
    
    void Schedule(SimPLISignal * const sig_p);		//!< Adds a signal and schedules its first value change
    UInt64 getTime() const;				//!< Returns the time of the first event in the queue
    bool Empty() const;					//!< Indicates if the queue is empty
    void Reschedule();					//!< Reschedules all the signals in the current timestamp
    bool getSignalValue(SimPLISignal ** const sig_p,
			UtString * const value) const;	//!< Attempts to return the current signal and value at the head of the queue
    void nextSignal();					//!< Moves to the next signal in this timestamp
    void Restart();					//!< Re-initialized all signals to the beginning of the waveform

 protected:
    std::priority_queue<SimPLIEventQEntry *,
	UtVector<SimPLIEventQEntry *>,
	SimPLIEventQEntry::Greater<SimPLIEventQEntry> > mQ;	//!< The priority queue to store the events
    UtList<SimPLIEventQEntry *> mSched;		//!< List of processed events for the current timestamp that need to be rescheduled
    UtList<SimPLIEventQEntry *> mFinished;		//!< List of signals that have no more events in the current iteration
    UInt64 mCurrTime;					//!< Current simulation time


};

#endif

