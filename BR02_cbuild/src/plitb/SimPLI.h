// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Header for the SimPLI class
*/

#ifndef __SIMPLI_H__
#define __SIMPLI_H__

#include "util/UtString.h"
#include "util/UtList.h"
#include "util/AtomicCache.h"
#include "util/DynBitVector.h"
#include "util/UtUInt64Factory.h"

class SimPLISignal;
class SimPLIEventQ;
class WfAbsFileReader;
class Stats;

/*!
  \brief Main control class for the PLI testbench simulation

  This class is the direct interface between the PLI routines
  and the Verilog simulation.  It is responsible for initializing
  the waveform tool, tracking signals in the waveform, and
  driving the values back into the simulation at the appropriate
  times.  It also handles some additional tasks, such as error
  tracking.

  The waveform file is manipulated through an instance of the
  WfAbsFileReader class, an abstract interface that can be derived
  to support various file formats.  Currently, only VCD format is
  supported, but as new readers become available, they can be
  easily added to openFile().
*/
class SimPLI
{
public:
    SimPLI(const char * const prefix);	//!< Constructor
    ~SimPLI();				//!< Destructor

    int openFile(int format, const UtString &filename);		//!< Opens the trace file
    bool addSignal(char * const instance,
		   const UtString &name,
		   int bits,
		   int type);		//!< Adds a signal to be read from the file
    void initDone();			//!< Indicates the end of the signal setup
    UInt64 getNextDelay();		//!< Returns the time until the next waveform file event
    bool noMoreEvents() const;		//!< Indicates if the schedule of signal events is empty
    void nextTimestamp();		//!< Removes signals from the current timeslot and reschedules them for their next change
    bool getSignalValue(SimPLISignal ** const sig_p,
			UtString * const value) const;	//!< Attempts to return the next available signal and value for the current time
    void nextSignal();			//!< Moves pointer to the next signal in the timestamp
    void setMaxErrors(int num);		//!< Sets the maximum number of errors allowed before failing the simulation
    bool incrErrorCount();		//!< Records an error, and returns whether the max error count has been exceeded
    void enableStats();			//!< Enables statistics reporting
    void setStartTime(UInt64 time);	//!< Sets the time to begin logging errors
    void setEndTime(UInt64 time);	//!< Sets the time to end the simulation
    void setNumIters(int num);		//!< Sets the number of iterations to run

    //! Enumeration of waveform file types
    enum WaveformTypes {
	eVCD,				//!< VCD format
	eFSDB,				//!< FSDB format
	eUNKNOWN			//!< unknown format
    };

    //! Enumeration of signal types
    enum SignalTypes {
	eCLOCK,				//!< Signal is a clock
	eINPUT,				//!< Signal is an input pin
	eOUTPUT,			//!< Signal is an output pin
	eBIDIR,				//!< Signal is a bidirectional (inout) pin
	eENABLE				//!< Signal is an enable controlling a bidirectional pin
    };

protected:
    AtomicCache mStringCache; //!< String cache
  DynBitVectorFactory mDynFactory; //! Waveform value factorys
  UtUInt64Factory mTimeFactory; //! Time factory

    WfAbsFileReader *mWfFile;	//!< Waveform file reader
    const UtString mPrefix;		//!< Hierarchy prefix for signals in VCD file
    unsigned int mErrorCount;		//!< Number of compare errors seen
    unsigned int mMaxErrors;		//!< Maximum number of errors allowed before failing the simulation
    UInt64 mCurrTime;			//!< Current simulation time as read from the waveform file
    SimPLIEventQ * const mEventQ;	//!< Queue of upcoming signal change events - used for scheduling tasks
    UtList<SimPLISignal *> mSigList;	//!< Temporary list to store signals until we have a chance to schedule them
    Stats *mStats;			//!< Statistics object
    UInt64 mStartTime;			//!< Time at which to start logging errors
    UInt64 mEndTime;			//!< Time at which to end the simulation
    bool mStartTimePassed;		//!< Flag set when we pass the time to start logging errors
    bool mEndTimePassed;		//!< Flag set when we pass the time to stop simulating
    unsigned int mNumIters;		//!< Number of iterations of the simulation to run
    unsigned int mCurrIter;		//!< Iteration count we're on now
};

#endif
