// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implementation of the SimPLI class
*/

#include "waveform/WfVCDFileReader.h"
#include "waveform/WfAbsSignal.h"
#include "util/Stats.h"
#include "SimPLI.h"
#include "SimPLISignal.h"
#include "SimPLIEventQ.h"
#include <stdio.h>
/*!
  The constructor initializes several members, but does not touch
  the waveform reader.  Use OpenFile() for that.
 */
SimPLI::SimPLI(const char * const prefix)
    :mDynFactory(true),
     mPrefix(prefix),
     mMaxErrors(1),
     mEventQ(new SimPLIEventQ),
     mStats(0),
     mStartTime(0),
     mEndTime(UtUINT64_MAX),
     mStartTimePassed(true),
     mEndTimePassed(false)
{
    mWfFile = 0;
    mErrorCount = 0;
    mCurrTime = 0;
    mNumIters = 1;
    mCurrIter = 1;
    mSigList.clear();
}

SimPLI::~SimPLI()
{
    if (mWfFile)
	delete mWfFile;
    if (mEventQ)
 	delete mEventQ;
    if (mStats) {
	mStats->printIntervalStatistics("Simulation");
	mStats->printTotalStatistics();
	delete mStats;
    }
}

/*!
  Opens the waveform file, which must be in the format specified.
  Currently the only supported format is VCD, but
  other formats can be easily added.  Any file format reader
  that uses the WfAbsFileReader interface are compatible.

  \param format Indicates the type of file that will be opened \sa WaveformTypes
  \param filename Name of the waveform file to open
  \retval 0 The file was opened successfully
  \retval 1 Unsupported file format
  \retval 2 Another error occurred

 */
int SimPLI::openFile(int format, const UtString &filename)
{
    switch (format) {
    case eVCD:
	mWfFile = new WfVCDFileReader(filename.c_str(), &mStringCache, &mDynFactory, &mTimeFactory);
	break;
    case eFSDB:
	// Just uncomment when we support it, add the #include, and it *should* work...
// 	wf_file_p_ = new WfFSDBFileReader(filename);
// 	break;
    case eUNKNOWN:
    default:
	return 1;
    };

    UtString reason;
    if (mWfFile->loadSignals(&reason) != WfAbsFileReader::eOK)
    {
      fprintf(stderr, "%s\n", reason.c_str());
      return 2;
    }
    
    
    if (mStats)
      mStats->printIntervalStatistics("Load Signals");

    return 0;
}

/*!
  Adds a signal to the list of those that are extracted from the waveform file.

  \param instance Pointer to the instance of the PLI call with this signal as a parameter
  \param name Hierarchical name of the signal as found in the waveform file
  \param bits Bit width of the signal
  \param type Type of signal being added
  \retval true Signal was added successfully
  \retval false Signal could not be found in the waveform file

  \sa SignalTypes
 */
bool SimPLI::addSignal(char * const instance,
		       const UtString &name,
		       int bits,
		       int type)
{
    SimPLISignal *sig_p;	// *en_p;
    WfAbsSignal *wf_sig_p;

    // Build the actual signal name
    UtString full_name = mPrefix + "." + name;

    // Tell the waveform object that we need to watch this signal
    wf_sig_p = mWfFile->watchSignal(full_name);
    if (!wf_sig_p)		// check if signal found
	return false;
    sig_p = new SimPLISignal(instance, wf_sig_p, bits, type);

    mSigList.push_back(sig_p);		// store the signal until we can schedule it

    return true;
}

/*!
  Indicates that all signals have been added, and prepares the
  waveform reader to begin tracking signal values.
 */
void SimPLI::initDone()
{
    SimPLISignal *sig_p;
    // Tell the waveform reader to get rid of any signals we're
    // not using, and to load the values for the signals we need
    mWfFile->unloadSignals();
    UtString msg;
    WfAbsFileReader::Status loadStat = mWfFile->loadValues(&msg);
    if (loadStat != WfAbsFileReader::eOK)
    {
      fprintf(stderr, "%s\n", msg.c_str());
      if (loadStat == WfAbsFileReader::eError)
        exit(1);
    }
    
    if (mStats)
	mStats->printIntervalStatistics("Load Values");

    // Now that the values are loaded, we can empty our temporary
    // list of signals, initialize the iterator for each, and
    // schedule them in the event queue.
    while (!(mSigList.empty())) {
	sig_p = mSigList.front();
	mSigList.pop_front();
	sig_p->initIter(mWfFile);
	// There's a possibility that the signal exists in the VCD file, but has no
	// value change events (not even an initial value).  Don't schedule the
	// signal in this case - it's no use to us, and it'll break because the iterator
	// has no values to point to.
	if (!(sig_p->getIter()->atEnd()))
	    mEventQ->Schedule(sig_p);
    }
}

/*!
  Determines the delay (in current timescale units) until
  the next change of any signal being tracked.

  \return Delay time until next signal value change
 */
UInt64 SimPLI::getNextDelay()
{
    // A bit of a misnomer.  This returns the current time,
    // i.e. the time of the event at the head of the queue.
    // But from the point of view of the PLI task, which
    // calls this function, this time is in the future, so
    // the name's gonna stay.
    UInt64 delay = mEventQ->getTime() - mCurrTime;

    // Set these flags before updating the time to the next event
    mStartTimePassed = (mCurrTime > mStartTime);
    mEndTimePassed = (mCurrTime > mEndTime);
    mCurrTime = mEventQ->getTime();

    return delay;
}

/*!
  Checks if there are any more signal change events scheduled.
  In other words, is the simulation over yet?

  \retval true There are no event remaining
  \retval false There are events remaining
 */
bool SimPLI::noMoreEvents() const
{
    return (mEventQ->Empty() || mEndTimePassed);
}

/*!
  Tells the SimPLI object that all signals that changed at this
  timestamp have been processed.  The signals are rescheduled
  for their next value change time.
 */
void SimPLI::nextTimestamp()
{
    // tell the event queue to reschedule all signals in the
    // current timestamp
    mEventQ->Reschedule();

    // If the event queue is empty (meaning there are no
    // changes left in the waveform file, we'd normally be
    // done, and the PLI routine would determine this when it
    // calls noMoreEvents.
    // However, if we're doing multiple iterations and this
    // isn't the last one, intercept it and reset the queue
    // so it's transparent.
    if (noMoreEvents() && (++mCurrIter <= mNumIters)) {
	mEventQ->Restart();
	mCurrTime = 0;
    }
}

/*!
  Attempts to get a signal and its value for the current timestamp,
  if the value changed.

  \param sig_p Pointer to the SimPLISignal object pointer for the signal that changed (returned by reference)
  \param value Pointer to the value of the signal (returned by reference)
  \retval true A signal was found for this timestamp
  \retval false No more signals found for this timestamp

  \sa NextSignal
 */
bool SimPLI::getSignalValue(SimPLISignal ** const sig_p,
			    UtString * const value) const
{
    return mEventQ->getSignalValue(sig_p, value);
}

/*!
  Moves to the next signal in the current timestamp.
  This must be called after a successful GetSignalValue to
  advance to the next signal.

  \sa GetSignalValue
 */
void SimPLI::nextSignal()
{
    mEventQ->nextSignal();
}

/*!
  \param num Number of errors to allow
*/
void SimPLI::setMaxErrors(int num)
{
    mMaxErrors = num;
}

/*!
  Increments the number of errors seen in simulation, and
  returns whether the maximum number of errors has been exceeded.

  \retval true The maximum number of errors has been exceeded
  \retval false The maximum number of errors has not been exceeded
 */
bool SimPLI::incrErrorCount()
{
    if (!mStartTimePassed)
	return false;

    return (++mErrorCount >= mMaxErrors);
}

void SimPLI::enableStats()
{
    mStats = new Stats();
}

/*
  \param time Time to start error logging, in simulation timescale units
*/
void SimPLI::setStartTime(UInt64 time)
{
    mStartTime = time;
    mStartTimePassed = false;
}

/*
  \param time Time to end the simulation, in simulation timescale units
*/
void SimPLI::setEndTime(UInt64 time)
{
    mEndTime = time;
}

/*
  \param num Number of iterations to run
*/
void SimPLI::setNumIters(int num)
{
    mNumIters = num;
}
