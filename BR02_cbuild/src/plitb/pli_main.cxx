// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  PLI functions implementing Verilog user tasks
*/

#include "SimPLI.h"
#include "SimPLISignal.h"


#define PROTO_PARAMS(params) params
#define EXTERN extern "C"
#include <stdio.h>
#include "veriuser.h"
#undef PROTO_PARAMS
#undef EXTERN

SimPLI *g_pli_p;	//!< Global pointer to the SimPLI object

//! A simple struct used to keep track of PLI sync references
struct SimPLISync
{
    char *clock_drive;		//!< Instance pointer for the clock driving call
    char *signal_drive;		//!< Instance pointer for the signal driving call
    char *set_fail;		//!< Instance pointer for the failure call
    int scale_factor;		//!< Factor by which to scale the delay to match the desired timescale
} g_sync;

//! Utility routine for PLI functions to call on simulation failure
static void sim_fail(bool immediate)
{
    if (immediate) {
	io_printf(reinterpret_cast<const PLI_BYTE8*>("Aborting...\n"));
	tf_dofinish();
    } else
	tf_iputp(1, 1, g_sync.set_fail);
}

//! PLI call to open the waveform file and init the SimPLI object
extern "C" int init_vcd_call()
{
    const char *filename;
    const char *format;
    const char *basepath;
    int retval, f_num;
    UtString f_str;
    const char *option;
    int tscale, tprec;

    // Determine the base path for the signal locations in the source VCD file
    basepath = mc_scan_plusargs(reinterpret_cast<const PLI_BYTE8*>("basePath="));
    if (basepath)
	g_pli_p = new SimPLI(basepath);
    else {
	// basePath needs to be specified!
	g_pli_p = 0;
	io_printf("No +basePath specified on command line!\n");
	sim_fail(true);
    }

    // determine if we have to convert the time precision of the VCD file
    // (which must be the same as the time precision of the simulation)
    // to the timescale of the simulation
    tscale = tf_gettimeunit();
    tprec = tf_gettimeprecision();
    g_sync.scale_factor = 1;
    // All delays need to be scaled by 10 raised to the power of the difference.
    // pow() is for doubles, not ints, and this is easy enough to do.
    while (tscale > tprec) {
	g_sync.scale_factor *= 10;
	--tscale;
    }

    if (mc_scan_plusargs("stats"))
        g_pli_p->enableStats();

    // make sure arguments are passed correctly
    // should be +waveFile=<file> and either +vcd or +fsdb
    filename = mc_scan_plusargs("waveFile=");
    if (filename) {
	// check for a format
	f_num = SimPLI::eUNKNOWN;
	format = mc_scan_plusargs("vcd");
	if (format)
	    f_num = SimPLI::eVCD;
	format = mc_scan_plusargs("fsdb");
	if (format) {
	    // check for both +vcd and +fsdb
	    if (f_num == SimPLI::eVCD) {
		io_printf("Error - options +vcd and +fsdb are mutually exclusive\n");
		sim_fail(true);
	    }
	    f_num = SimPLI::eFSDB;
	}
	// check for neither +vcd nor +fsdb
	if (f_num == SimPLI::eUNKNOWN) {
	    io_printf("Error - must specify either +vcd or +fsdb for waveform format\n");
	    sim_fail(true);
	}

	retval = g_pli_p->openFile(f_num, filename);
	switch (retval) {
	case 1:		// Unsupported file format
	    io_printf("Unsupported file format %s\n", format);
	    sim_fail(true);
	case 2:		// Other error
	    io_printf("Error opening input file %s\n", filename);
	    sim_fail(true);
	}
    } else {
	io_printf("Couldn't find option +waveFile on command line\n");
	io_printf("Please specify the source waveform file with +waveFile=<file>\n");
	sim_fail(true);
    }

    // Check for other options
    option = mc_scan_plusargs("maxerrors=");
    if (option)
	g_pli_p->setMaxErrors(atoi(option));

    option = mc_scan_plusargs("starttime=");
    if (option)
	g_pli_p->setStartTime(atoll(option));

    option = mc_scan_plusargs("endtime=");
    if (option)
	g_pli_p->setEndTime(atoll(option));

    option = mc_scan_plusargs("iter=");
    if (option)
	g_pli_p->setNumIters(atoll(option));

    return 0;
}

//! Misc PLI call, used to clean up at the end of the simulation
extern "C" int init_vcd_misc(int, int reason)
{
    // this is only used to call destructors when we're done
    if (reason != reason_finish)
	return 0;

    delete g_pli_p;

    return 0;
}

//! Parameter checking routine for add_clock_call()
extern "C" int add_clock_check()
{
    // must take 2 parameters
    if (tf_nump() != 2) {
	io_printf("$add_clock must take 2 parameters\n");
	sim_fail(true);
    }

    return 0;
}

//! PLI call to map a clock signal from the waveform to the simulation
extern "C" int add_clock_call()
{
    if (!(g_pli_p->addSignal(tf_getinstance(), tf_getcstringp(2), tf_sizep(1), SimPLI::eCLOCK))) {
	io_printf("Error adding clock %s\n", tf_getcstringp(2));
	sim_fail(true);
    }

    return 0;
}

//! Parameter checking routine for add_input_call()
extern "C" int add_input_check()
{
    // must take 2 parameters
    if (tf_nump() != 2) {
	io_printf("$add_input must take 2 parameters\n");
	sim_fail(true);
    }

    return 0;
}

//! PLI call to map an input signal from the waveform to the simulation
extern "C" int add_input_call()
{
    if (!(g_pli_p->addSignal(tf_getinstance(), tf_getcstringp(2), tf_sizep(1), SimPLI::eINPUT))) {
	io_printf("Error adding input %s\n", tf_getcstringp(2));
	sim_fail(true);
    }

    return 0;
}

//! Parameter checking routine for add_output_call()
extern "C" int add_output_check()
{
    // must take 2 parameters
    if (tf_nump() != 2) {
	io_printf("$add_output must take 2 parameters\n");
	sim_fail(true);
    }

    return 0;
}

//! PLI call to map an output signal from the waveform to the simulation
extern "C" int add_output_call()
{
    if (!(g_pli_p->addSignal(tf_getinstance(), tf_getcstringp(2), tf_sizep(1), SimPLI::eOUTPUT))) {
	io_printf("Error adding output %s\n", tf_getcstringp(2));
	sim_fail(true);
    }

    return 0;
}

//! Parameter checking routine for add_bidir_call()
extern "C" int add_bidir_check()
{
    // must take 2 parameters
    if (tf_nump() != 2) {
	io_printf("$add_bidir must take 2 parameters\n");
	sim_fail(true);
    }

    return 0;
}

//! PLI call to map a bidirection signal from the waveform to the simulation
extern "C" int add_bidir_call()
{
    if (!(g_pli_p->addSignal(tf_getinstance(), tf_getcstringp(2), tf_sizep(1), SimPLI::eBIDIR))) {
	io_printf("Error adding bidirect %s\n", tf_getcstringp(2));
	sim_fail(true);
    }

    return 0;
}

//! Parameter checking routine for add_enable_call()
extern "C" int add_enable_check()
{
    // must take 2 parameters
    if (tf_nump() != 2) {
	io_printf("$add_enable must take 2 parameters\n");
	sim_fail(true);
    }

    return 0;
}

//! PLI call to map an enable signal from the waveform to the simulation
extern "C" int add_enable_call()
{
    if (!(g_pli_p->addSignal(tf_getinstance(), tf_getcstringp(2), tf_sizep(1), SimPLI::eENABLE))) {
	io_printf("Error adding enable %s\n", tf_getcstringp(2));
	sim_fail(true);
    }

    return 0;
}

//! PLI setup call to drive clock signals from the waveform to the simulation
extern "C" int drive_clocks_call()
{
    // We're done with signal setup now - time to start driving signals
    g_pli_p->initDone();

    g_sync.clock_drive = tf_getinstance();

    return 0;
}

//! Callback routine to drive clock signals at the correct time
extern "C" int drive_clocks_misc(int, int reason)
{
    SimPLISignal *sig_p;
    UtString value;
    bool is_clock = true;

    // Make sure we only respond to events we expect to be called for
    if (!((reason == reason_synch) ||
	  (reason == reason_reactivate)))
	return 0;

    // Go through the list of signals and values for this
    // timestamp.  Stop when we hit the end of the list or
    // something that's not a clock.  GetSignalValue()
    // returns all clocks for a timestamp before any of the
    // regular signals.
    while (is_clock &&
	   g_pli_p->getSignalValue(&sig_p, &value)) {
	if (sig_p->getType() != SimPLI::eCLOCK)
	    is_clock = false;
	else {
	    // Pass the new value to the verilog world through
	    // the saved instance of this signal parameter, and
	    // move to the next signal in the list.
	    tf_istrdelputp(1, sig_p->getNumBits(), 'b', const_cast<char*>(value.c_str()), 0, 0,
			   sig_p->getInstance());
	    g_pli_p->nextSignal();
	}
    }

    // schedule the regular signals to drive as well
    tf_isynchronize(g_sync.signal_drive);

    return 0;
}

//! PLI setup call to drive non-clock signals from the waveform to the simulation
extern "C" int drive_signals_call()
{
    g_sync.signal_drive = tf_getinstance();

    // We have a decision to make.
    // If the first signal information from the file is at time 0
    // (and there's no good reason why it wouldn't be, but you
    // never know...), we can sync on the clock routine to drive
    // the first clock signals.
    // Otherwise, because the drive_*_misc routines assume they're
    // being called at the correct time, we have to manually
    // insert the first delay here.
    UInt64 delay = g_pli_p->getNextDelay();
    if (delay == 0)
	tf_isynchronize(g_sync.clock_drive);
    else {
	if (g_sync.scale_factor == 1) {
	    // stupid PLI handles 64-bit numbers in 32-bit chunks
	    unsigned int high, low;
	    high = static_cast<unsigned int>(delay >> 32);
	    low = static_cast<unsigned int>(delay);
	    tf_isetlongdelay(low, high, g_sync.clock_drive);
	} else {
	    // scale appropriately
	    double scaled_delay = static_cast<double>(delay);
	    scaled_delay /= g_sync.scale_factor;
	    tf_isetrealdelay(scaled_delay, g_sync.clock_drive);
	}
    }

    return 0;
}

//! Callback routine to drive non-clock signals at the correct time
extern "C" int drive_signals_misc(int, int reason)
{
    SimPLISignal *sig_p;
    UtString value;

    // Make sure we only respond to events we expect to be called for
    if (!((reason == reason_synch) ||
	  (reason == reason_reactivate)))
	return 0;

    // The idea here is the same as in drive_clocks_misc(),
    while (g_pli_p->getSignalValue(&sig_p, &value)) {
	tf_istrdelputp(1, sig_p->getNumBits(), 'b', const_cast<char*>(value.c_str()), 0, 0,
		       sig_p->getInstance());
	g_pli_p->nextSignal();
    }

    // We've handled all the signals for this timestamp, so
    // it's time to move to the next one.
    g_pli_p->nextTimestamp();
    if (g_pli_p->noMoreEvents()) {
	// Probably want to set a flag for the testbench to
	// exit gracefully, but this will work for now.
	io_printf("Simulation passed!\n");
	tf_dofinish();
    } else {
	// Find the next delay and schedule the clock routine
	// to run then.
	UInt64 delay = g_pli_p->getNextDelay();
	if (g_sync.scale_factor == 1) {
	    unsigned int high, low;
	    high = static_cast<int>(delay >> 32);
	    low = static_cast<int>(delay);
	    tf_isetlongdelay(low, high, g_sync.clock_drive);
	} else {
	    // scale appropriately
	    double scaled_delay = static_cast<double>(delay);
	    scaled_delay /= g_sync.scale_factor;
	    tf_isetrealdelay(scaled_delay, g_sync.clock_drive);
	}
    }

    return 0;
}

//! PLI call to increment the number of errors seen in the simulation
extern "C" int incr_err_count_call()
{
    if (g_pli_p->incrErrorCount()) {
	io_printf("Simulation failed!\n");
	sim_fail(false);
    }

    return 0;
}


/*!
  \brief PLI call to set some options

  Designates a register to set when the simulation fails
  This function also sets the error_delay parameter if
  a plusarg is found for it.
*/
extern "C" int set_options_call()
{
    char *cmdline_option;
    char *filename;
    unsigned int filename_length, i;
    unsigned char temp;

    // Save the instance so we can set the fail register later
    g_sync.set_fail = tf_getinstance();

    // Check for error delay override on command line
    cmdline_option = mc_scan_plusargs("error_delay=");
    if (cmdline_option)
	tf_putp(2, atoi(cmdline_option));

    // See if we're dumping VCD
    cmdline_option = mc_scan_plusargs("dumpFile=");
    if (cmdline_option) {
	tf_putp(3, 1);		// set dump_vcd flag
	// Need to convert filename to a hexidecimal string of
	// the ASCII values of each of the characters, so it
	// can be passed through the PLI via tf_strdelputp()
	filename_length = strlen(cmdline_option);
	// Each true char in the filename needs 2 chars in the
	// hex string to represent it (e.g. "A" becomes "41")
	filename = new char[filename_length * 2 + 1];
	for (i = 0; i < filename_length * 2; i += 2) {
	    temp = static_cast<unsigned char>(cmdline_option[i/2]);
	    sprintf(&filename[i], "%x%x", (temp >> 4) & 0xf, temp & 0xf);
	}
	// Number of bits is length of original filename, times 8
	// (bytes -> bits)
	if (filename_length * 8 > static_cast<unsigned int>(tf_sizep(4))) {
	    io_printf("Filename %s exceeds hard-coded verilog register size... aborting\n", cmdline_option);
	    sim_fail(true);
	}
 	tf_strdelputp(4, filename_length * 8, 'h', filename, 0, 0);

	delete [] filename;
    }

    return 0;
}
