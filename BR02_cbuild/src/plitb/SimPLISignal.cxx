// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Implementation of the SimPLISignal class
*/

#include "SimPLISignal.h"
#include "waveform/WfAbsSignal.h"
#include "waveform/WfAbsSignalVCIter.h"
#include "waveform/WfAbsFileReader.h"
#include <stdio.h>

/*!
  \param instance Pointer to the instance of the PLI call where the signal was used as a parameter
  \param wf_sig Pointer to the WfAbsSignal tracking this signal in the waveform file
  \param bits Bit width of this signal
  \param type Signal type

  \sa SimPLI::SignalTypes
 */
SimPLISignal::SimPLISignal(char * const instance,
			   WfAbsSignal * const wf_sig,
			   int bits,
			   int type)
    : mInstance(instance),
      mcBits(bits),
      mcType(type),
      mWfSignal(wf_sig)
{
    // We can't initialize this yet, because signals are still
    // being added and the value portion of the wavefile hasn't
    // been loaded yet.
    mWfSignalIter = 0;
    mPartner = 0;
}

SimPLISignal::~SimPLISignal()
{
    // WfAbsSignal and WfAbsSignalVCIter objects are allocated
    // when they're returned from a create function, and this
    // class is the only one that uses them, so we might as well
    // deallocate them here.
  // delete mWfSignal;
    if (mWfSignalIter)
	delete mWfSignalIter;
}

/*!
  \return Pointer to the instance of the PLI call where the signal was used as a parameter
 */
char * SimPLISignal::getInstance() const
{
    return mInstance;
}

/*!
  \param partner Pointer to the signal that should be set as this signal's partner
 */
void SimPLISignal::setPartner(SimPLISignal *partner)
{
    mPartner = partner;
}

/*!
  \return Pointer to this signal's partner
 */
const SimPLISignal * SimPLISignal::getPartner() const
{
    return mPartner;
}

/*!
  Initializes the iterator into the waveform file, which is used to reference value
  changes on the signal.

  \retval true The iterator was initialized successfully
  \retval false An error occurred
 */
bool SimPLISignal::initIter(WfAbsFileReader* reader)
{
    // The value information has been loaded into the waveform
    // interface, so it's safe to init the iterator
  UtString buf;
    mWfSignalIter = reader->createSignalVCIter(mWfSignal, &buf);
    if (! buf.empty())
      fprintf(stderr, "%s\n", buf.c_str());
    
    return (mWfSignalIter != 0);
}

/*!
  \param name Name of the signal (returned by reference)
 */
void SimPLISignal::getName(UtString * const name) const
{
    mWfSignal->getName(name);
}

/*!
  \return Bit width of the signal
*/
int SimPLISignal::getNumBits() const
{
    return mcBits;
}

/*!
  \return Signal type

  \sa SimPLI::SignalTypes
 */
int SimPLISignal::getType() const
{
    return mcType;
}

/*!
  \return Pointer to the waveform file iterator
 */
WfAbsSignalVCIter * SimPLISignal::getIter() const
{
    return mWfSignalIter;
}
