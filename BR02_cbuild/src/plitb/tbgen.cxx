// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*
  \file
  Main routine for PLI testbench generation
*/

#include "util/UtIOStream.h"
#include "IODBIntf.h"
#include "util/UtIOStream.h"
#include "util/UtList.h"
#include "util/UtIStream.h"

/*!
  \brief Looks for a signal in a list of signals
  \param name Name of the signal to search for
  \param list_ref List to search
  \return Iterator to signal (list_ref.end() if not found)
*/
UtList<SignalInfo *>::iterator find_signal(const UtString &name, UtList<SignalInfo *> &list_ref)
{
    UtList<SignalInfo *>::iterator iter;
    for (iter = list_ref.begin(); iter != list_ref.end(); ++iter)
	if ((*iter)->name == name)
	    break;
    return iter;
}

/*!
  \brief Parses an enable string from a string stream into a list of tokens
  \param en_list Reference to the list where the tokens will be returned
  \param str String stream containing the enable string
*/
void parse_enables(UtList<UtString> &en_list, UtIStringStream &str)
{
    UtString::iterator start_iter, end_iter;
    bool first_time, last_was_signal, curr_is_signal_first, curr_is_signal_other;
    UtString temp, token;

    en_list.clear();

    // Got through all the whitespace-delimited chunks of the string
    while (str >> temp) {
	first_time = true;
	last_was_signal = false;
	start_iter = temp.begin();
	end_iter = temp.begin();
	// Step through the current string, breaking it up when we see a new token.
	// The parser isn't that smart, because it doesn't have to be.  The only
	// kinds of tokens we care about are signal names, and everything else.
	// Anything that's not a signal name (e.g. a sequence of operators, parens,
	// etc.) gets lumped together.
	//
	// Since we're lumping all non-signals together, and there can't be two
	// signals back-to-back without an operator between them, all we have to
	// do is look for transitions between signals and non-signals.
	while (end_iter != temp.end()) {
	    // valid characters for the first character of a signal name
	    curr_is_signal_first = (((*end_iter >= 'A') && (*end_iter <= 'Z')) ||
				    ((*end_iter >= 'a') && (*end_iter <= 'z')));
	    // valid characters for subsequent characters of a signal name
	    curr_is_signal_other = (((*end_iter >= '0') && (*end_iter <= '9')) ||
				    (*end_iter == '_') || (*end_iter == '.'));
	    if (!last_was_signal && curr_is_signal_first) {
		// If the last character we saw was not part of a signal name, but
		// the current character is a valid first character of a signal name,
		// save the previous token and start a new signal name token.
		last_was_signal = true;
		// The first time through, if the string starts with a signal, we'll
		// enter here with an empty initial token, so catch this case and
		// skip it.
		if (!first_time) {
		    token.assign(start_iter, end_iter);	// grab the previous token
		    en_list.push_back(token);		// add it
		    start_iter = end_iter;		// move to the new token
		}
	    } else if (last_was_signal && !curr_is_signal_first && !curr_is_signal_other) {
		// If the last character we saw was part of a signal name, but
		// the current character isn't, save the previous token and start
		// a new "other" token.
		last_was_signal = false;
		token.assign(start_iter, end_iter);
		en_list.push_back(token);
		start_iter = end_iter;
	    }
	    // move to the next character
	    ++end_iter;
	    first_time = false;
	}
	// When we're done with the string, we still have the last token to add
	token.assign(start_iter, end_iter);
	en_list.push_back(token);
    }
}

int main(int argc, char **argv)
{
    UtList<SignalInfo *> clocks, inputs, outputs, bidirs, enables, forced;	// lists of signals of various types
    UtList<SignalInfo *>::iterator iter, iter2;
    SignalInfo *sinfo;

    UtList<UtString> enable_list;			// list of enables for a bidirectional signal
    UtList<UtString>::iterator enable_iter;

    const UtString ref_suf("__REF");			// suffix added to top-level signal declarations for waveform file reference values
    const UtString ts_suf("__TS");			// suffix added to top-level signal declarations for tri-state wires
    const UtString outfile("top.v");			// output testbench filename
    const unsigned int FILENAME_LENGTH = 100;		// length of the output vcd filename

    IODBIntf *intf;

    UtString mod_name;					// name of the module to simulate
    UtString timescale;					// string to use in constructing the `timescale directive
    UtString dbfile("libdesign.io.db");			// name of the IODB file
    UtString cfgfile;					// name of the input configuration file
    UtString includefile;				// extra verilog file to include
    bool use_db = true;					// are we using the IODB file for finding ports?
    bool firstparam = true;
    bool ignoreall = false;
    int delay;

    UtString fileline;
    UtString temp, bits, enb;
    UtString::size_type bitpos;
    unsigned int config_errors = 0;
    
    UtOStream& cerr = UtIO::cerr();

    // Check arguments
    if ((argc < 2) || (argc > 3)) {
	cerr << "usage: " << argv[0] << " [-nodb] <config file> [db file]" << UtIO::endl;
	exit(1);
    }
     // Get IODB file if specified
    for (int i = 1; i < argc; ++i) {
 	temp = argv[i];
 	// check for -nodb
 	if (temp[0] == '-') {
 	    if (temp == "-nodb")
 		use_db = false;
 	    else {
 		cerr << "Illegal option " << temp << UtIO::endl;
 		exit(1);
 	    }
 	} else {
 	    // Must be a file name.  If we haven't seen a config file yet,
 	    // this must be it.  Otherwise, it's the IODB file.
 	    if (cfgfile == "")
 		cfgfile = temp;
 	    else
 		dbfile = temp;
 	}
    }

    // Open the input and output files
    UtOBStream outf(outfile.c_str());
    if (!outf) {
	cerr << "Error opening output file " << outfile << UtIO::endl;
	exit(1);
    }

    UtIBStream inf(cfgfile.c_str());
    if (!inf) {
	cerr << "Error opening config file " << cfgfile << UtIO::endl;
	exit(1);
    }

    // Open the IODB file, and extract the signals
    if (use_db) {
	intf = new IODBIntf(dbfile);
	intf->getSignalLists(&clocks, &inputs, &outputs, &bidirs);
	delete intf;
    }

    // Parse the config file, a line at a time
    while (inf.getline(&fileline)) {
	UtIStringStream file_str(fileline.c_str());
	while (file_str >> temp) {
	    if (temp[0] == '#')		// ignore comments
		break;
	    else if (temp == "module") {
		file_str >> mod_name;
		break;
	    } else if (temp == "basePath") {
		cerr << "-->" << fileline << UtIO::endl;
		cerr << "Directive basePath is no longer supported" << UtIO::endl;
		cerr << "Specify with runtime option +basePath=<hierarchy>" << UtIO::endl;
		exit(1);
	    } else if (temp == "nodump") {
		cerr << "-->" << fileline << UtIO::endl;
		cerr << "Directive nodump is no longer supported" << UtIO::endl;
		cerr << "Enable VCD dumping with runtime option +dumpFile=<filename>" << UtIO::endl;
		exit(1);
	    } else if (temp == "width") {
		file_str >> temp;
		// Attempt to find the specified signal, and set its width
		iter = find_signal(temp, clocks);
		if (iter != clocks.end()) {
		    file_str >> (*iter)->width;
		    break;
		}
		iter = find_signal(temp, inputs);
		if (iter != inputs.end()) {
		    file_str >> (*iter)->width;
		    break;
		}
		iter = find_signal(temp, outputs);
		if (iter != outputs.end()) {
		    file_str >> (*iter)->width;
		    break;
		}
		iter = find_signal(temp, bidirs);
		if (iter != bidirs.end()) {
		    file_str >> (*iter)->width;
		    break;
		}
		iter = find_signal(temp, enables);
		if (iter != enables.end()) {
		    file_str >> (*iter)->width;
		    break;
		}
		cerr << "-->" << fileline << UtIO::endl;
		cerr << "Couldn't find signal " << temp << " in design" << UtIO::endl;
		++config_errors;
	    } else if (temp == "enable") {
		file_str >> temp;
		// Enables might be only for specific bits of a bidir, so strip the
		// bit info off, if it exists.
		bitpos = temp.find("[");
		if (bitpos == UtString::npos)
		    bits = "";		// no [...] in bidir signal
		else {
		    // break the expression into signal name and bit range
		    bits.assign(temp, bitpos, UtString::npos);
		    temp.erase(bitpos);
		}
		    

		// Enables are only valid on bidirectionals
		iter = find_signal(temp, bidirs);
		if (iter == bidirs.end()) {
		    cerr << "-->" << fileline << UtIO::endl;
		    cerr << "Couldn't find bidirectional signal " << temp << " in design" << UtIO::endl;
		    ++config_errors;
		    //file_str.ignore(FILELINE_SIZE);
		} else {
		    // We need to do two things here:
		    // 1. Extract the names of the enable signals from the expression in the file, and
		    //    add them to the list of enable signals.
		    // 2. Substitute the mangled top-level enable signals in the expression, and store
		    //    it with the bidirectional signal.
		    parse_enables(enable_list, file_str);
		    // The enable string for a bidir is a comma separated list of bit ranges and expressions.
		    // If we already have an enable for a previous bit range of this enable, we need to add a comma
		    // before we can continue.
		    if ((*iter)->enable != "")
			(*iter)->enable += ",";
		    // Add the enable bits
		    (*iter)->enable += bits;
		    for (enable_iter = enable_list.begin(); enable_iter != enable_list.end(); ++enable_iter) {
			// If the first character of the token is a letter, it's a signal name
			if ((((*enable_iter)[0] >= 'A') && ((*enable_iter)[0] <= 'Z')) || (((*enable_iter)[0] >= 'a') && ((*enable_iter)[0] <= 'z'))) {
			    // Add an enable signal to the list.
			    // Since the same enable might control more than one output,
			    // or different bits of the same enable might control different
			    // outputs, don't create a new signal if it already exists.
			    iter2 = find_signal(*enable_iter, enables);
			    if (iter2 == enables.end()) {
				sinfo = new SignalInfo(*enable_iter);
				enables.push_back(sinfo);
			    } else
				sinfo = *iter2;
			    // Append the mangled enable name to the bidir's enable string
			    (*iter)->enable += sinfo->mang_name + ref_suf;
			} else
			    (*iter)->enable += *enable_iter;	// not a signal name - just append the expression
		    }
		}
	    } else if (temp == "ignore") {
		file_str >> temp;
		// Ignore is only valid for output, bidirs, and enables
		iter = find_signal(temp, outputs);
		if (iter != outputs.end()) {
		    (*iter)->ignore = true;
		    break;
		}
		iter = find_signal(temp, bidirs);
		if (iter != bidirs.end()) {
		    (*iter)->ignore = true;
		    break;
		}
		iter = find_signal(temp, enables);
		if (iter != enables.end()) {
		    (*iter)->ignore = true;
		    break;
		}
		cerr << "-->" << fileline << UtIO::endl;
		cerr << "Couldn't find signal " << temp << " in design" << UtIO::endl;
		++config_errors;
	    } else if (temp == "compare") {
		file_str >> temp;
		sinfo = new SignalInfo(temp);
		// just treat the internal compare signal as an enable
		enables.push_back(sinfo);
	    } else if (temp == "tie") {
		file_str >> temp;
		// Ignore is only valid for clocks and inputs
		iter = find_signal(temp, clocks);
		if (iter != clocks.end()) {
		    file_str >> (*iter)->forced_value;
		    forced.push_back(*iter);
		    break;
		}
		iter = find_signal(temp, inputs);
		if (iter != inputs.end()) {
		    file_str >> (*iter)->forced_value;
		    forced.push_back(*iter);
		    break;
		}
		cerr << "-->" << fileline << UtIO::endl;
		cerr << "Couldn't find input signal " << temp << " in design" << UtIO::endl;
		++config_errors;
	    } else if (temp == "timeunit") {
		file_str >> temp;
		timescale = temp + "/" + temp;
		break;
	    } else if (temp == "timescale") {
		file_str >> timescale;
		break;
	    } else if (temp == "clock") {
		if (use_db) {
		    cerr << "-->" << fileline << UtIO::endl;
		    cerr << "Must use -nodb switch to manually specify clock signals" << UtIO::endl;
		    exit(1);
		}
		file_str >> temp;
		sinfo = new SignalInfo(temp);
		clocks.push_back(sinfo);
	    } else if (temp == "input") {
		if (use_db) {
		    cerr << "-->" << fileline << UtIO::endl;
		    cerr << "Must use -nodb switch to manually specify input signals" << UtIO::endl;
		    exit(1);
		}
		file_str >> temp;
		sinfo = new SignalInfo(temp);
		inputs.push_back(sinfo);
	    } else if (temp == "output") {
		if (use_db) {
		    cerr << "-->" << fileline << UtIO::endl;
		    cerr << "Must use -nodb switch to manually specify output signals" << UtIO::endl;
		    exit(1);
		}
		file_str >> temp;
		sinfo = new SignalInfo(temp);
		outputs.push_back(sinfo);
	    } else if (temp == "bidir") {
		if (use_db) {
		    cerr << "-->" << fileline << UtIO::endl;
		    cerr << "Must use -nodb switch to manually specify bidir signals" << UtIO::endl;
		    exit(1);
		}
		file_str >> temp;
		sinfo = new SignalInfo(temp);
		bidirs.push_back(sinfo);
	    } else if (temp == "ignoreall") {
		ignoreall = true;
	    } else if (temp == "delay") {
		file_str >> temp;
		// Delay is only valid for output, bidirs, and enables
		iter = find_signal(temp, outputs);
		if (iter != outputs.end()) {
		    file_str >> (*iter)->delay;
		    break;
		}
		iter = find_signal(temp, bidirs);
		if (iter != bidirs.end()) {
		    file_str >> (*iter)->delay;
		    break;
		}
		iter = find_signal(temp, enables);
		if (iter != enables.end()) {
		    file_str >> (*iter)->delay;
		    break;
		}
		cerr << "-->" << fileline << UtIO::endl;
		cerr << "Couldn't find signal " << temp << " in design" << UtIO::endl;
		++config_errors;
	    } else if (temp == "delayall") {
		// This should set the compare delay for all outputs, bidirs, and enables.
		// However, you should still be able to override specific signals to
		// have a different delay, provided their "delay" directive is after the
		// "delayall" directive.  We'll just set all the delays now, so that they
		// can change as we continue parsing.
		file_str >> delay;
		for (iter = outputs.begin(); iter != outputs.end(); ++iter)
		    (*iter)->delay = delay;
		for (iter = bidirs.begin(); iter != bidirs.end(); ++iter)
		    (*iter)->delay = delay;
		for (iter = enables.begin(); iter != enables.end(); ++iter)
		    (*iter)->delay = delay;
	    } else if (temp == "include") {
		file_str >> includefile;
	    } else {
		cerr << "-->" << fileline << UtIO::endl;
		cerr << "Unknown directive " << temp << UtIO::endl;
		exit(1);
	    }
	}
    }
    if (config_errors) {
	cerr << config_errors << " error(s) while processing config file" << UtIO::endl;
	exit(1);
    }

    inf.close();

    // Make sure a module was specified
    if (mod_name == "") {
	cerr << "No module specified in configuration file" << UtIO::endl;
	exit(1);
    }

    // Start printing to the file

    if (timescale != "") {
	outf << "`timescale " << timescale << UtIO::endl;
	outf << UtIO::endl;
    }
    outf << "module top;" << UtIO::endl;
    outf << UtIO::endl;

    // Declare registers for all known signals.  These are where
    // the values from the waveform file will be driven.
    // Add widths for vectors where necessary
    for (iter = clocks.begin(); iter != clocks.end(); ++iter) {
	outf << "   reg ";
	if ((*iter)->width != 1)
	    outf << "[" << ((*iter)->width - 1) << ":0] ";
	outf << (*iter)->mang_name << ref_suf << ";" << UtIO::endl;
    }
    for (iter = inputs.begin(); iter != inputs.end(); ++iter) {
	outf << "   reg ";
	if ((*iter)->width != 1)
	    outf << "[" << ((*iter)->width - 1) << ":0] ";
	outf << (*iter)->mang_name << ref_suf << ";" << UtIO::endl;
    }
    for (iter = outputs.begin(); iter != outputs.end(); ++iter) {
	outf << "   reg ";
	if ((*iter)->width != 1)
	    outf << "[" << ((*iter)->width - 1) << ":0] ";
	outf << (*iter)->mang_name << ref_suf << ";" << UtIO::endl;
    }
    for (iter = bidirs.begin(); iter != bidirs.end(); ++iter) {
	outf << "   reg ";
	if ((*iter)->width != 1)
	    outf << "[" << ((*iter)->width - 1) << ":0] ";
	outf << (*iter)->mang_name << ref_suf << ";" << UtIO::endl;
	// Bidirectionals need a wire definition as well for the tri-state pin
	outf << "   wire ";
	if ((*iter)->width != 1)
	    outf << "[" << ((*iter)->width - 1) << ":0] ";
	outf << (*iter)->mang_name << ts_suf << ";" << UtIO::endl;
    }
    for (iter = enables.begin(); iter != enables.end(); ++iter) {
	outf << "   reg ";
	if ((*iter)->width != 1)
	    outf << "[" << ((*iter)->width - 1) << ":0] ";
	outf << (*iter)->mang_name << ref_suf << ";" << UtIO::endl;
    }
    outf << UtIO::endl;

    // Instantiate the module.
    // Connect the reference registers to the ports for clocks and inputs,
    // and the tri-state wires for bidirectionals.  Let outputs unconnected.
    outf << "   " << mod_name << " " << mod_name << "(";
    for (iter = clocks.begin(); iter != clocks.end(); ++iter) {
	if (!firstparam) {
	    // Need to add a comma and newline except on the first parameter
	    outf << "," << UtIO::endl;
	    outf << "        ";
	}
	outf << "." << (*iter)->name << "( " << (*iter)->name << ref_suf << " )";
	firstparam = false;
    }
    for (iter = inputs.begin(); iter != inputs.end(); ++iter) {
	if (!firstparam) {
	    outf << "," << UtIO::endl;
	    outf << "        ";
	}
	outf << "." << (*iter)->name << "( " << (*iter)->name << ref_suf << " )";
	firstparam = false;
    }
    for (iter = outputs.begin(); iter != outputs.end(); ++iter) {
	if (!firstparam) {
	    outf << "," << UtIO::endl;
	    outf << "        ";
	}
	outf << "." << (*iter)->name << "()";
	firstparam = false;
    }
    for (iter = bidirs.begin(); iter != bidirs.end(); ++iter) {
	if (!firstparam) {
	    outf << "," << UtIO::endl;
	    outf << "        ";
	}
	outf << "." << (*iter)->name << "( " << (*iter)->name << ts_suf << " )";
	firstparam = false;
    }
    outf << " );" << UtIO::endl;
    outf << UtIO::endl;

    // Common setup commands
    outf << "   reg fail;" << UtIO::endl;
    outf << "   integer error_delay;" << UtIO::endl;
    outf << "   reg dump_vcd;" << UtIO::endl;
    outf << "   reg [" << (FILENAME_LENGTH * 8 - 1) << ":0] dump_vcd_filename;" << UtIO::endl;
    outf << UtIO::endl;

    // Unfortunately, the tf_ routine to pass large values (the filename, in our case)
    // back to a register doesn't make its results visible immediately.  Even setting
    // the delay to 0 causes the update to occur at the end of the current simulation
    // cycle.  This means we can't call $dumpfile in the same initial block as the
    // call to $set_options, because the filename isn't set yet.  So, we'll set up
    // an always block to be triggered by the change, and start VCD dumping then.  It's
    // still at time 0, just at the end of the simulation step, which shouldn't matter.
    outf << "   always @(dump_vcd_filename)" << UtIO::endl;
    outf << "      if (dump_vcd) begin" << UtIO::endl;
    outf << "         $dumpfile(dump_vcd_filename);" << UtIO::endl;
    outf << "         $dumpvars;" << UtIO::endl;
    outf << "      end" << UtIO::endl;
    outf << UtIO::endl;
    outf << "   initial begin" << UtIO::endl;
    outf << "      fail = 0;" << UtIO::endl;
    outf << "      error_delay = 100;" << UtIO::endl;
    outf << "      dump_vcd = 0;" << UtIO::endl;
    outf << "      $set_options(fail, error_delay, dump_vcd, dump_vcd_filename);" << UtIO::endl;
    outf << "      $display(\"Starting simulation\");" << UtIO::endl;
    outf << "      $init_vcd;" << UtIO::endl;

    // Create a pli call for all signals.  Don't bother for any inputs
    // that we're forcing.
    for (iter = clocks.begin(); iter != clocks.end(); ++iter)
	if ((*iter)->forced_value == "")
	    outf << "      $add_clock(" << (*iter)->mang_name << ref_suf << ", \"" << (*iter)->name << "\");" << UtIO::endl;
    for (iter = inputs.begin(); iter != inputs.end(); ++iter)
	if ((*iter)->forced_value == "")
	    outf << "      $add_input(" << (*iter)->mang_name << ref_suf << ", \"" << (*iter)->name << "\");" << UtIO::endl;
    for (iter = outputs.begin(); iter != outputs.end(); ++iter)
	outf << "      $add_output(" << (*iter)->mang_name << ref_suf << ", \"" << (*iter)->name << "\");" << UtIO::endl;
    for (iter = bidirs.begin(); iter != bidirs.end(); ++iter)
	outf << "      $add_bidir(" << (*iter)->mang_name << ref_suf << ", \"" << (*iter)->name << "\");" << UtIO::endl;
    for (iter = enables.begin(); iter != enables.end(); ++iter)
	outf << "      $add_enable(" << (*iter)->mang_name << ref_suf << ", \"" << (*iter)->name << "\");" << UtIO::endl;
    outf << UtIO::endl;

    outf << "      $drive_clocks;" << UtIO::endl;
    outf << "      $drive_signals;" << UtIO::endl;
    outf << "   end" << UtIO::endl;
    outf << UtIO::endl;

    // Routine for delayed exit on failure
    outf << "   always @(posedge fail) begin" << UtIO::endl;
    outf << "      $display(\"Stopping simulation...\");" << UtIO::endl;
    outf << "      #error_delay;" << UtIO::endl;
    outf << "      $finish;" << UtIO::endl;
    outf << "   end" << UtIO::endl;
    outf << UtIO::endl;

    // Assign the tri-state bidir wires based on the enable value.
    // If the enable is true, float the signal so the device can drive it.
    // If the enable is false, drive the signal with the value from the waveform file
    for (iter = bidirs.begin(); iter != bidirs.end(); ++iter) {
	// Need to strip out all the enable information from the string
	// All the enables for the bidir are stored in this string, in the format:
	// [bits1]expr1,[bits2]expr2
	// If an expression controls the entire bit range, there is no [bits] field
	temp = (*iter)->enable;
	if (temp == "")
	    cerr << "Warning! No enable specified for bidirectional signal " << (*iter)->name << UtIO::endl;
	while (!temp.empty()) {
	    if (temp[0] == '[') {	// This enable controls a bit range
		// Split up the bit range and enable expression
		bitpos = temp.find("]");
		bits.assign(temp, 0, bitpos + 1);
		temp.erase(0, bitpos + 1);
	    } else
		bits = "";
	    // See if there are any more expressions in the string,
	    // and assign the expression appropriately
	    bitpos = temp.find(",");
	    if (bitpos == UtString::npos) {
		// End of string.  Assign the whole thing to the enable and clear the string
		enb = temp;
		temp.clear();
	    } else {
		// Assign up to the comma to the enable string, and set the temp string
		// to the beginning of the next field
		enb.assign(temp, 0, bitpos);
		temp.erase(0, bitpos + 1);
	    }
	    outf << "   assign " << (*iter)->mang_name << ts_suf << bits << " = (" << enb << ") ? 'bz : " << (*iter)->mang_name << ref_suf << bits << ";" << UtIO::endl;
	}
    }
    outf << UtIO::endl;

    // Forced input values
    if (!forced.empty()) {
	outf << "   initial begin" << UtIO::endl;
	for (iter = forced.begin(); iter != forced.end(); ++iter)
	    outf << "      force " << (*iter)->mang_name << ref_suf << " = " << (*iter)->forced_value << ";" << UtIO::endl;
	outf << "   end" << UtIO::endl;
	outf << UtIO::endl;
    }

    // Compares for outputs, bidirs, and enables, provided they're not ignored
    if (!ignoreall) {
	for (iter = outputs.begin(); iter != outputs.end(); ++iter)
	    if (!(*iter)->ignore) {
		temp.assign((*iter)->width, 'x');
		outf << "   always @(" << mod_name << "." << (*iter)->name << " or " << (*iter)->mang_name << ref_suf << ")" << UtIO::endl;
		outf << "      #" << (*iter)->delay << " if ((" << (*iter)->mang_name << ref_suf << " !== " << (*iter)->width << "'b" << temp
		     << ") && (" << mod_name << "." << (*iter)->name << " !== " << (*iter)->mang_name << ref_suf << ")) begin" << UtIO::endl;
		outf << "         $display(\"" << mod_name << "." << (*iter)->name << "(%x) !== " << (*iter)->mang_name << ref_suf << "(%x) at time %t\", "
		     << mod_name << "." << (*iter)->name << ", " << (*iter)->mang_name << ref_suf << ", $time);" << UtIO::endl;
		outf << "         $incr_err_count;" << UtIO::endl;
		outf << "      end" << UtIO::endl;
		outf << UtIO::endl;
	    }
	for (iter = bidirs.begin(); iter != bidirs.end(); ++iter)
	    if (!(*iter)->ignore) {
		temp.assign((*iter)->width, 'x');
		outf << "   always @(" << mod_name << "." << (*iter)->name << " or " << (*iter)->mang_name << ref_suf << ")" << UtIO::endl;
		outf << "      #" << (*iter)->delay << " if ((" << (*iter)->mang_name << ref_suf << " !== " << (*iter)->width << "'b" << temp
		     << ") && (" << mod_name << "." << (*iter)->name << " !== " << (*iter)->mang_name << ref_suf << ")) begin" << UtIO::endl;
		outf << "         $display(\"" << mod_name << "." << (*iter)->name << "(%x) !== " << (*iter)->mang_name << ref_suf << "(%x) at time %t\", "
		     << mod_name << "." << (*iter)->name << ", " << (*iter)->mang_name << ref_suf << ", $time);" << UtIO::endl;
		outf << "         $incr_err_count;" << UtIO::endl;
		outf << "      end" << UtIO::endl;
		outf << UtIO::endl;
	    }
	for (iter = enables.begin(); iter != enables.end(); ++iter)
	    if (!(*iter)->ignore) {
		temp.assign((*iter)->width, 'x');
		outf << "   always @(" << mod_name << "." << (*iter)->name << " or " << (*iter)->mang_name << ref_suf << ")" << UtIO::endl;
		outf << "      #" << (*iter)->delay << " if ((" << (*iter)->mang_name << ref_suf << " !== " << (*iter)->width << "'b" << temp
		     << ") && (" << mod_name << "." << (*iter)->name << " !== " << (*iter)->mang_name << ref_suf << ")) begin" << UtIO::endl;
		outf << "         $display(\"" << mod_name << "." << (*iter)->name << "(%x) !== " << (*iter)->mang_name << ref_suf << "(%x) at time %t\", "
		     << mod_name << "." << (*iter)->name << ", " << (*iter)->mang_name << ref_suf << ", $time);" << UtIO::endl;
		outf << "         $incr_err_count;" << UtIO::endl;
		outf << "      end" << UtIO::endl;
		outf << UtIO::endl;
	    }
    }

    // Include an additional verilog file (if specified) for additional forces, etc.
    if (includefile != "")
	outf << "`include \"" << includefile << "\"" << UtIO::endl;
	
    outf << UtIO::endl;
    outf << "endmodule" << UtIO::endl;

    UtIO::cout() << "File top.v successfully created for module " << mod_name << UtIO::endl;

    for (iter = clocks.begin(); iter != clocks.end(); ++iter)
	delete *iter;
    for (iter = inputs.begin(); iter != inputs.end(); ++iter)
	delete *iter;
    for (iter = outputs.begin(); iter != outputs.end(); ++iter)
	delete *iter;
    for (iter = bidirs.begin(); iter != bidirs.end(); ++iter)
	delete *iter;
    for (iter = enables.begin(); iter != enables.end(); ++iter)
	delete *iter;

    return 0;
}
