// -*-C++-*-
/******************************************************************************
 Copyright (c) 2013-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "svinspector/SvinspectorCB.h"
#include "util/UtString.h"

//Verilog headers
#include "VeriTreeNode.h"
#include "VeriModuleItem.h"
#include "VeriModule.h"
#include "VeriMisc.h"
#include "VeriStatement.h"
#include "VeriExpression.h"
#include "VeriConstVal.h"
#include "VeriId.h"
#include "VeriLibrary.h"
#include "VeriScope.h"
#include "LineFile.h"
#include "veri_tokens.h"

#include <iostream>

// #define TRACE_SVINSPECTOR

#ifdef TRACE_SVINSPECTOR


#define TRACE_SVINSPECTOR_WALK(METHODNAME,MSG) \
    switch (phase){ \
        case Verific::VerilogDesignWalkerCB::VISITPRE:  std::cout << "SVInsp PHASE:  PRE " << METHODNAME << " " << MSG << std::endl; break;\
        case Verific::VerilogDesignWalkerCB::VISITPOST: std::cout << "SVInsp PHASE: POST " << METHODNAME << " " << MSG << std::endl; break;\
    }
#else
// empty macro, actually just ignores phase argument
#define TRACE_SVINSPECTOR_WALK(METHODNAME,MSG) \
  (void)phase;
#endif

// similar to LineFile::Compare but only check the file and line numbers (thus ignore the column information)
unsigned CrbnLanguageUnit::isSameLineFile(const Verific::linefile_type left, const Verific::linefile_type right)
{
    if (left == right ) return 1; // Return 1 if left and right points to same location, 0 otherwise
    if (!left || !right) return 0 ;

    if (Verific::LineFile::GetFileId(left) != Verific::LineFile::GetFileId(right)) return 0 ; // Different file
    if (Verific::LineFile::GetLineNo(left) != Verific::LineFile::GetLineNo(right)) return 0 ;

    return 1 ; // Equal
}


void CrbnLanguageUnit::printItems(int currentLimit){
  unsigned i;
  int numUniqueLF = 0;
  Verific::linefile_type last_lf_val = static_cast<Verific::linefile_type>(0);

  UInt32 numInThisGroup = mSourceLocators.size();
  for (i = 0; i < numInThisGroup; ++i)
  {
    Verific::linefile_type lf = mSourceLocators[i];

    if ( isSameLineFile(lf, last_lf_val ) ){
      std::cout << ".";
      continue;
    }
    last_lf_val = lf;
    // if lf == 0 report from generic interface
    if (i)     std::cout << std::endl;
    if (lf != 0) {
      std::cout << "   " << Verific::LineFile::GetFileName(lf) << ":" << Verific::LineFile::GetLineNo(lf) << " .";
    } else {
      std::cout << "   generic interface?:0 .";
    }
    numUniqueLF++;
    if ( (currentLimit > 0) &&  numUniqueLF >= currentLimit ){
      std::cout << "  -- Truncated after " << numUniqueLF << " unique lines (total of " << numInThisGroup << ")";
      break;
    }
  }
  std::cout << std::endl;
}




// static
const char* SVInspector::getPrintToken(unsigned veri_token)
{
    // Get a string for a token
    switch (veri_token) {
    case 0 : return "" ; // No token

    case VERI_OPAREN :              return "(" ;
    case VERI_CPAREN :              return ")" ;
    case VERI_MODULUS :             return "%" ;
    case VERI_PLUS :                return "+" ;
    case VERI_MIN :                 return "-" ;
    case VERI_MUL :                 return "*" ;
    case VERI_DIV :                 return "/" ;
    case VERI_GT :                  return ">" ;
    case VERI_LT :                  return "<" ;
    case VERI_GEQ :                 return ">=" ;
    case VERI_LEQ :                 return "<=" ;
    case VERI_CASEEQ :              return "===" ;
    case VERI_CASENEQ :             return "!==" ;
    case VERI_LOGEQ :               return "==" ;
    case VERI_LOGNEQ :              return "!=" ;
    case VERI_LOGNOT :              return "!" ;
    case VERI_LOGAND :              return "&&" ;
    case VERI_LOGOR :               return "||" ;
    case VERI_REDNOT :              return "~" ;
    case VERI_REDAND :              return "&" ;
    case VERI_REDOR :               return "|" ;
    case VERI_REDXOR :              return "^" ;
    case VERI_REDNAND :             return "~&" ;
    case VERI_REDNOR :              return "~|" ;
    case VERI_REDXNOR :             return "~^" ;
    case VERI_LSHIFT :              return "<<" ;
    case VERI_RSHIFT :              return ">>" ;
    case VERI_QUESTION :            return "?" ;
    case VERI_COLON :               return ":" ;
    case VERI_ALLPATH :             return "*>" ;
    case VERI_LEADTO :              return "=>" ;
    case VERI_RIGHTARROW :          return "->" ;

    case VERI_ALWAYS :              return "always" ;
    case VERI_AND :                 return "and" ;
    case VERI_ASSIGN :              return "assign" ;
    case VERI_BEGIN :               return "begin" ;
    case VERI_BUF :                 return "buf" ;
    case VERI_BUFIF0 :              return "bufif0" ;
    case VERI_BUFIF1 :              return "bufif1" ;
    case VERI_CASE :                return "case" ;
    case VERI_CASEX :               return "casex" ;
    case VERI_CASEZ :               return "casez" ;
    case VERI_CMOS :                return "cmos" ;
    case VERI_DEASSIGN :            return "deassign" ;
    case VERI_DEFAULT :             return "default" ;
    case VERI_DEFPARAM :            return "defparam" ;
    case VERI_DISABLE :             return "disable" ;
    case VERI_EDGE :                return "edge" ;
    case VERI_ELSE :                return "else" ;
    case VERI_END :                 return "end" ;
    case VERI_ENDCASE :             return "endcase" ;
    case VERI_ENDFUNCTION :         return "endfunction" ;
    case VERI_ENDMODULE :           return "endmodule" ;
    case VERI_ENDPRIMITIVE :        return "endprimitive" ;
    case VERI_ENDSPECIFY :          return "endspecify" ;
    case VERI_ENDTABLE :            return "endtable" ;
    case VERI_ENDTASK :             return "endtask" ;
    case VERI_EVENT :               return "event" ;
    case VERI_FOR :                 return "for" ;
    case VERI_FORCE :               return "force" ;
    case VERI_FOREVER :             return "forever" ;
    case VERI_FORK :                return "fork" ;
    case VERI_FUNCTION :            return "function" ;
    case VERI_HIGHZ0 :              return "highz0" ;
    case VERI_HIGHZ1 :              return "highz1" ;
    case VERI_IF :                  return "if" ;
    case VERI_IFNONE :              return "ifnone" ;
    case VERI_INITIAL :             return "initial" ;
    case VERI_INOUT :               return "inout" ;
    case VERI_INPUT :               return "input" ;
    case VERI_INTEGER :             return "integer" ;
    case VERI_JOIN :                return "join" ;
    case VERI_LARGE :               return "large" ;
    case VERI_MACROMODULE :         return "macromodule" ;
    case VERI_MEDIUM :              return "medium" ;
    case VERI_MODULE :              return "module" ;
    case VERI_NAND :                return "nand" ;
    case VERI_NEGEDGE :             return "negedge" ;
    case VERI_NMOS :                return "nmos" ;
    case VERI_NOR :                 return "nor" ;
    case VERI_NOT :                 return "not" ;
    case VERI_NOTIF0 :              return "notif0" ;
    case VERI_NOTIF1 :              return "notif1" ;
    case VERI_OR :                  return "or" ;
    case VERI_OUTPUT :              return "output" ;
    case VERI_PARAMETER :           return "parameter" ;
    case VERI_PMOS :                return "pmos" ;
    case VERI_POSEDGE :             return "posedge" ;
    case VERI_PRIMITIVE :           return "primitive" ;
    case VERI_PULL0 :               return "pull0" ;
    case VERI_PULL1 :               return "pull1" ;
    case VERI_PULLDOWN :            return "pulldown" ;
    case VERI_PULLUP :              return "pullup" ;
    case VERI_RCMOS :               return "rcmos" ;
    case VERI_REAL :                return "real" ;
    case VERI_REALTIME :            return "realtime" ;
    case VERI_REG :                 return "reg" ;
    case VERI_RELEASE :             return "release" ;
    case VERI_REPEAT :              return "repeat" ;
    case VERI_RNMOS :               return "rnmos" ;
    case VERI_RPMOS :               return "rpmos" ;
    case VERI_RTRAN :               return "rtran" ;
    case VERI_RTRANIF0 :            return "rtranif0" ;
    case VERI_RTRANIF1 :            return "rtranif1" ;
    case VERI_SCALARED :            return "scalared" ;
    case VERI_SMALL :               return "small" ;
    case VERI_SPECIFY :             return "specify" ;
    case VERI_SPECPARAM :           return "specparam" ;
    case VERI_STRONG0 :             return "strong0" ;
    case VERI_STRONG1 :             return "strong1" ;
    case VERI_SUPPLY0 :             return "supply0" ;
    case VERI_SUPPLY1 :             return "supply1" ;
    case VERI_TABLE :               return "table" ;
    case VERI_TASK :                return "task" ;
    case VERI_TIME :                return "time" ;
    case VERI_TRAN :                return "tran" ;
    case VERI_TRANIF0 :             return "tranif0" ;
    case VERI_TRANIF1 :             return "tranif1" ;
    case VERI_TRI :                 return "tri" ;
    case VERI_TRI0 :                return "tri0" ;
    case VERI_TRI1 :                return "tri1" ;
    case VERI_TRIAND :              return "triand" ;
    case VERI_TRIOR :               return "trior" ;
    case VERI_TRIREG :              return "trireg" ;
    case VERI_VECTORED :            return "vectored" ;
    case VERI_WAIT :                return "wait" ;
    case VERI_WAND :                return "wand" ;
    case VERI_WEAK0 :               return "weak0" ;
    case VERI_WEAK1 :               return "weak1" ;
    case VERI_WHILE :               return "while" ;
    case VERI_WIRE :                return "wire" ;
    case VERI_WOR :                 return "wor" ;
    case VERI_XNOR :                return "xnor" ;
    case VERI_XOR :                 return "xor" ;

    case VERI_OCUR :                return "{" ;
    case VERI_CCUR :                return "}" ;
    case VERI_OBRACK :              return "[" ;
    case VERI_CBRACK :              return "]" ;
    case VERI_SEMI :                return ";" ;
    case VERI_DOT :                 return "." ;
    case VERI_COMMA :               return "," ;
    case VERI_AT :                  return "@" ;
    case VERI_EQUAL :               return "=" ;
    case VERI_EQUAL_ASSIGN :        return "=" ;
    case VERI_POUND :               return "#" ;

    case VERI_EDGE_AMPERSAND :      return "&&&" ;

        // Verilog 2000 added tokens :
    case VERI_PARTSELECT_UP :       return "+:" ;
    case VERI_PARTSELECT_DOWN :     return "-:" ;
    case VERI_ARITLSHIFT :          return "<<<" ;
    case VERI_ARITRSHIFT :          return ">>>" ;
    case VERI_POWER :               return "**" ;
    //case VERI_OP_STAR_CP :          return "(*)" ;
    case VERI_OATTR :               return "(*" ;
    case VERI_CATTR :               return "*)" ;

        // Verilog 2000 added keywords :
    case VERI_CONFIG :              return "config" ;
    case VERI_ENDCONFIG :           return "endconfig" ;
    case VERI_DESIGN :              return "design" ;
    case VERI_INSTANCE :            return "instance" ;
    case VERI_INCDIR :              return "incdir" ;
    case VERI_INCLUDE :             return "include" ;
    case VERI_CELL :                return "cell" ;
    case VERI_USE :                 return "use" ;
    case VERI_LIBRARY :             return "library" ;
    case VERI_LIBLIST :             return "liblist" ;
    case VERI_GENERATE :            return "generate" ;
    case VERI_ENDGENERATE :         return "endgenerate" ;
    case VERI_GENVAR :              return "genvar" ;
    case VERI_LOCALPARAM :          return "localparam" ;
    case VERI_SIGNED :              return "signed" ;
    case VERI_UNSIGNED :            return "unsigned" ;
    case VERI_AUTOMATIC :           return "automatic" ;
    case VERI_PULSESTYLE_ONEVENT :  return "pulsestyle_onevent" ;
    case VERI_PULSESTYLE_ONDETECT : return "pulsestyle_ondetect" ;
    case VERI_SHOWCANCELLED :       return "showcancelled" ;
    case VERI_NOSHOWCANCELLED :     return "noshowcancelled" ;
    case VERI_TICK_PROTECTED :      return "";
    case VERI_STRINGTYPE :          return "";
    case VERI_CROSS :               return "";
    case VERI_CAST_OP :             return "'";
    case VERI_TYPE :                return "";
    case VERI_UWIRE :               return "";
    case VERI_ALWAYS_COMB :         return "always_comb";
    case VERI_ALWAYS_FF :           return "always_ff";
    case VERI_ALWAYS_LATCH :        return "always_latch";
    case VERI_ASSERT :              return "";
    case VERI_ASSERT_STROBE :       return "";
    case VERI_BIT :                 return "bit";
    case VERI_BREAK :               return "";
    case VERI_BYTE :                return "";
    case VERI_CHANGED :             return "";
    case VERI_CHAR :                return "";
    case VERI_CONST :               return "";
    case VERI_CONTINUE :            return "";
    case VERI_DO :                  return "";
    case VERI_ENDINTERFACE :        return "";
    case VERI_ENUM :                return "enum";
    case VERI_EXPORT :              return "";
    case VERI_EXTERN :              return "";
    case VERI_FORKJOIN :            return "";
    case VERI_RETURN :              return "";
    case VERI_IFF :                 return "";
    case VERI_IMPORT :              return "";
    case VERI_INT :                 return "";
    case VERI_INTERFACE :           return "";
    case VERI_LOGIC :               return "";
    case VERI_LONGINT :             return "";
    case VERI_LONGREAL :            return "";
    case VERI_MODPORT :             return "";
    case VERI_PACKED :              return "";
    case VERI_PRIORITY :            return "priority";
    case VERI_SHORTINT :            return "";
    case VERI_SHORTREAL :           return "";
    case VERI_STATIC :              return "";
    case VERI_STRUCT :              return "";
    case VERI_TIMEPRECISION :       return "";
    case VERI_TIMEUNIT :            return "";
    case VERI_TYPEDEF :             return "";
    case VERI_UNION :               return "";
    case VERI_UNIQUE :              return "unique";
    case VERI_VOID :                return "";
    case VERI_PROCESS :             return "";
    case VERI_DOTSTAR :             return "";
    case VERI_DOLLAR_ROOT :         return "";
    case VERI_UNSIZED_LITERAL :     return "";
    case VERI_INC_OP :              return "++";
    case VERI_DEC_OP :              return "--";
    case VERI_PLUS_ASSIGN :         return "+=";
    case VERI_MIN_ASSIGN :          return "-=";
    case VERI_MUL_ASSIGN :          return "*=";
    case VERI_DIV_ASSIGN :          return "/=";
    case VERI_MOD_ASSIGN :          return "%=";
    case VERI_AND_ASSIGN :          return "&=";
    case VERI_OR_ASSIGN :           return "|=";
    case VERI_XOR_ASSIGN :          return "^=";
    case VERI_LSHIFT_ASSIGN :       return "";
    case VERI_RSHIFT_ASSIGN :       return "";
    case VERI_ALSHIFT_ASSIGN :      return "";
    case VERI_ARSHIFT_ASSIGN :      return "";
    case VERI_ALIAS :               return "";
    case VERI_BEFORE :              return "";
    case VERI_BIND :                return "";
    case VERI_CHANDLE :             return "";
    case VERI_CLASS :               return "";
    case VERI_CLOCKING :            return "";
    case VERI_COLON_COLON :         return "::";
    case VERI_COLON_EQUAL :         return ":=";
    case VERI_COLON_NEQUAL :        return ":/";
    case VERI_CONSTRAINT :          return "";
    case VERI_CONTEXT :             return "";
    case VERI_DIST :                return "";
    case VERI_ENDCLASS :            return "";
    case VERI_ENDCLOCKING :         return "";
    case VERI_ENDPROGRAM :          return "";
    case VERI_EXTENDS :             return "";
    case VERI_FINAL :               return "final";
    case VERI_INSIDE :              return "";
    case VERI_JOIN_ANY :            return "";
    case VERI_JOIN_NONE :           return "";
    case VERI_LOCAL :               return "";
    case VERI_NEW :                 return "";
    case VERI_NULL :                return "";
    case VERI_PROGRAM :             return "";
    case VERI_PROTECTED :           return "";
    case VERI_PUBLIC :              return "public";
    case VERI_PURE :                return "";
    case VERI_RAND :                return "";
    case VERI_RANDC :               return "";
    case VERI_RIGHTARRAW_ARROW :    return "->";
    case VERI_REF :                 return "";
    case VERI_SOLVE :               return "";
    case VERI_STEPCONTROL_OP :      return "@@";
    case VERI_SUPER :               return "";
    case VERI_THIS :                return "";
    case VERI_VAR :                 return "";
    case VERI_VIRTUAL :             return "";
    case VERI_WAIT_ORDER :          return "";
    case VERI_WITH :                return "";
    case VERI_WILDEQUALITY :        return "";
    case VERI_WILDINEQUALITY :      return "";
    case VERI_PROPERTY :            return "";
    case VERI_ENDPROPERTY :         return "";
    case VERI_COVER :               return "";
    case VERI_OVERLAPPED_IMPLICATION :return "";
    case VERI_NON_OVERLAPPED_IMPLICATION :return "";
    case VERI_OVERLAPPED_FOLLOWED_BY :return "";
    case VERI_NON_OVERLAPPED_FOLLOWED_BY :return "";
    case VERI_CYCLE_DELAY_OP :      return "##";
    case VERI_DOLLAR :              return "$";
    case VERI_CONSECUTIVE_REPEAT :  return "[* ";
    case VERI_CONSECUTIVE_REPEAT_MUL :return "[*]";
    case VERI_CONSECUTIVE_REPEAT_PLUS :return "[+]";
    case VERI_NON_CONSECUTIVE_REPEAT :return "[*=";
    case VERI_GOTO_REPEAT :         return "[->";
    case VERI_INTERSECT :           return "";
    case VERI_FIRST_MATCH :         return "";
    case VERI_THROUGHOUT :          return "";
    case VERI_WITHIN :              return "";
    case VERI_ENDSEQUENCE :         return "";
    case VERI_SEQUENCE :            return "";
    case VERI_LOG_EQUIVALENCE :     return "<->";
    case VERI_ASSUME :              return "";
    case VERI_BINS :                return "";
    case VERI_BINSOF :              return "";
    case VERI_COVERGROUP :          return "";
    case VERI_COVERPOINT :          return "";
    case VERI_ENDGROUP :            return "";
    case VERI_ENDPACKAGE :          return "";
    case VERI_EXPECT :              return "";
    case VERI_FOREACH :             return "foreach";
    case VERI_IGNOREBINS :          return "";
    case VERI_ILLEGAL_BINS :        return "";
    case VERI_MATCHES :             return "";
    case VERI_PACKAGE :             return "";
    case VERI_RANDCASE :            return "";
    case VERI_RANDSEQUENCE :        return "";
    case VERI_TAGGED :              return "";
    case VERI_TIMELITERAL :         return "";
    case VERI_WILDCARD :            return "";
    case VERI_DOLLAR_UNIT :         return "";
    case VERI_WITH_DEFAULT :        return "";
    case VERI_LET :                 return "";
    case VERI_GLOBAL :              return "";
    case VERI_CHECKER :             return "";
    case VERI_ENDCHECKER :          return "";
    case VERI_ACCEPT_ON :           return "";
    case VERI_REJECT_ON :           return "";
    case VERI_SYNC_ACCEPT_ON :      return "";
    case VERI_SYNC_REJECT_ON :      return "";
    case VERI_NEXTTIME :            return "";
    case VERI_S_NEXTTIME :          return "";
    case VERI_IMPLIES :             return "";
    case VERI_UNTIL :               return "";
    case VERI_S_UNTIL :             return "";
    case VERI_UNTIL_WITH :          return "";
    case VERI_S_UNTIL_WITH :        return "";
    case VERI_EVENTUALLY :          return "";
    case VERI_RESTRICT :            return "";
    case VERI_S_ALWAYS :            return "";
    case VERI_S_EVENTUALLY :        return "";
    case VERI_STRONG :              return "";
    case VERI_UNIQUE0 :             return "unique0";
    case VERI_UNTYPED :             return "";
    case VERI_WEAK :                return "";
    case VERI_OPT_PRAGMA :          return "";
    case VERI_OPT_COMMENT :         return "";
    case VERI_FORCE_READ :          return "";
    case VERI_TICK_TIMESCALE :      return "";
    case VERI_FATAL_ERROR :         return "";
    case veri_no_ocur :             return "";
    case veri_no_else :             return "";
    case veri_no_comment :          return "";
    case veri_force_token_read :    return "";
    case veri_no_colon :            return "";
    case veri_no_cparen :           return "";
    case VERI_CLOCKING_EVENT :      return "";
    case veri_sv_implication :      return "";
    case VERI_UNARY_OPER :          return "";

    // Added for default nettype :
    default : return "" ;
    }
}



void SVInspector::logMsg(Verific::VerilogDesignWalkerCB::VisitPhase phase, const UtString & msg, Verific::linefile_type lf){
  switch (phase){ 
  case Verific::VerilogDesignWalkerCB::VISITPRE: {
    sawLU(msg,lf);
    break;
  }
  case Verific::VerilogDesignWalkerCB::VISITPOST: {
    break;
  }
  }

}

void SVInspector::sawLU(const UtString & name, Verific::linefile_type lf){
  sawLU(name.c_str(), lf);
}
void SVInspector::sawLU(const char * char_name, Verific::linefile_type lf){

  UtString name(char_name);
  CrbnLanguageUnit* clu;
  
  LanguageUnitMapIter itr = mLUMap.find(name);
  if (itr == mLUMap.end()) {
    clu = new CrbnLanguageUnit();
    clu->addItem(lf);
    mLUMap[name] = clu;
  } else {
    clu = itr->second;
    clu->addItem(lf);
  }
}

void SVInspector::printAllLU(int limit){
  LanguageUnitMapIter itr;
  UtString luName;
  CrbnLanguageUnit* clu;
  
  for (itr = mLUMap.begin(); itr != mLUMap.end(); ++itr) {
    luName = itr->first;
    clu = itr->second;
    std::cout << " found " << clu->numItems() << " LU for " << luName.c_str();
    if (( luName == "Unknown" ) || ( luName == "VeriTreeNode" )) { std::cout << "******************"; }
    std::cout << std::endl;
    clu->printItems(limit);
    std::cout << std::endl;
  }
}

bool SVInspector::isFromWhat(Verific::VeriIdDef* start_id, bool lookForPackage, bool lookForInterface)
{
  bool retval = false;
  if (start_id == 0) return retval;

  Verific::VeriIdDef* id = start_id;


  // see if this is from any of the requested types, work backward until there is no remaining ID
  while ( id && ! retval) {
    retval |= ( lookForPackage && id->IsPackage() );
    retval |= ( lookForInterface && id->IsInterface() );
    
    Verific::VeriScope* scope = id->GetOwningScope();
    if ( ! scope ) break;

    Verific::VeriIdDef* scopeId = scope->GetOwner();
    if ( ! scopeId ) break;

    retval |= ( lookForPackage && scopeId->IsPackage() );
    retval |= ( lookForInterface && scopeId->IsInterface()); 

    id = scopeId;
  }
  return retval;


}

Verific::VeriIdDef* SVInspector::getNearestPackage(Verific::VeriIdDef* start_id)
{
  Verific::VeriIdDef* retId = NULL;

  Verific::VeriIdDef* id = start_id;


  // see if this is from any of the requested types, work backward until there is no remaining ID
  while ( id ) {
    if ( id->IsPackage() ){
      retId = id;
      break;
    }

    Verific::VeriScope* scope = id->GetOwningScope();
    if ( ! scope ) break;

    Verific::VeriIdDef* scopeId = scope->GetOwner();
    if ( ! scopeId ) break;

    if ( scopeId->IsPackage() ) {
      retId = scopeId;
      break;
    }

    id = scopeId;
  }
  return retId;

}


void SVInspector::getFlagsForDataType(Verific::VeriDataType* dataType, bool *isVoidType, bool *isIntType)
{
  *isIntType |= ( dataType->IsIntegerType() ); // why is this not sufficient?
  *isIntType |= ( VERI_INT == dataType->GetType());
  *isIntType |= ( VERI_SHORTINT == dataType->GetType());
  *isIntType |= ( VERI_LONGINT == dataType->GetType());
  *isIntType |= ( VERI_INTEGER == dataType->GetType());
  *isVoidType |= ( VERI_VOID == dataType->GetType());
}

// returns true if the idDef is a queue (specific limits defined by 2 bool arguments)
bool SVInspector::isQueue(Verific::VeriIdDef* start_id, bool lookForUnbounded, bool lookForBounded)
{
  bool retval = false;
  if (start_id == 0) return retval;

  Verific::VeriIdDef* id = start_id;

  unsigned numDimensions = id->UnPackedDimension();
  unsigned i;
  for ( i = 0; (!retval && (i < numDimensions)); i++){
    Verific::VeriRange *ve_range = id->GetDimensionAt(i);
    Verific::VeriExpression* ve_lexpr = ve_range->GetLeft();
    Verific::VeriExpression* ve_rexpr = ve_range->GetRight();
    bool leftIsDollar = (ve_lexpr && ve_lexpr->GetClassId() == Verific::ID_VERIDOLLAR );
    if ( leftIsDollar ) {
      retval = ((lookForBounded && (ve_rexpr != NULL )) || (lookForUnbounded && (ve_rexpr == NULL )));
    }
  }

  return retval;


}







//***************************************************************************************************

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriTreeNode, node, phase)
{
  if (&node == NULL ) {       std::cout << "Warning:: SvinspectorCB::VeriWalkCBVeriTreeNode called with NULL arg."<< std::endl; return NORMAL;}
  (void)phase;
  UtString msg;
  msg <<  "node  (" << node.GetClassId() << ")  ";
  if (Verific::LineFile::GetFileName(node.Linefile())) {
    //  for some reason interface definitions do not have a file name test7_20.2.2_genericinterface.sv
    msg << Verific::LineFile::GetFileName(node.Linefile()) << ":" << Verific::LineFile::GetLineNo(node.Linefile());
  }
  TRACE_SVINSPECTOR_WALK("VeriTreeNode", msg.c_str());
  mSVInspector.sawLU("VeriTreeNode",node.Linefile());
  return NORMAL;
}



Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriModule, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}
  UtString msg("VeriModule");
  TRACE_SVINSPECTOR_WALK(msg.c_str(), node.Name());

  UtString modifier;
  // see if it has any nested modules

  bool hasNestedModule = false;
  unsigned i ;
  Verific::VeriModuleItem *moduleItem ;
  Verific::Array* moduleItems = node.GetModuleItems();
  FOREACH_ARRAY_ITEM(moduleItems, i, moduleItem) {
    if ( moduleItem == NULL ) continue;
    // {/*debug*/ moduleItem->PrettyPrint(std::cout,0); std::cout<<std::endl;}
    hasNestedModule =  ( moduleItem->IsModule() );
    if ( hasNestedModule ) break;
  }

  if ( hasNestedModule ) { modifier << ":hasNestedMod";}
  
  if ( node.HasGenericInterfacePorts() ) {modifier << ":hasGenericInterfacePorts";}
  else if ( node.HasInterfacePorts() ) {modifier << ":hasInterfacePorts";}

  if ( node.HasTypeParameters() ) { modifier << ":hasTypeParameters";}
  if ( node.IsRootModule() ) { modifier << ":isRoot";}
  msg << modifier;
  
  switch (phase){ 
  case VISITPRE: {
    mSVInspector.addModuleItem(&node);
    mSVInspector.sawLU(msg,node.Linefile());
    break;
  }
  case VISITPOST: {
    mSVInspector.popModuleItem();
    if ( mSVInspector.isTopModule() ) {
      std::cout << "---  SystemVerilog Language Unit Report For: " << node.GetName() << "  ---" << std::endl;
      mSVInspector.printAllLU(mSVInspectorCount);
      std::cout << "---  SystemVerilog Language Unit Report End For: " << node.GetName() << "  ---" << std::endl;
    }
    break;
  }
  }
  return NORMAL;
}
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriExpression, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriExpression");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;
  
  modifier << ( ( node.GetClassId() == Verific::ID_VERINEW ) ? ":isNEW" : "");

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriIdRef, node, phase)
{
  UtString name("VeriIdRef");
  UtString modifier;
  TRACE_SVINSPECTOR_WALK(name.c_str(), (node.GetName()?node.GetName() : node.FullId()->GetName()));
  Verific::VeriIdDef* idDef = node.GetId();

  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}
  // if ( node.GetBaseDataType() == VERI_DOLLAR_ROOT ) modifier << ":$rootType"; // for some reason the parser is not allowing '$root' in source files
  if ( mSVInspector.isFromPackage(idDef) ) { modifier << ":fromPackage"; }
  if ( mSVInspector.isFromInterface(idDef) ) { modifier << ":fromInterface"; }
  UtString msg;

  msg << name << modifier;
  
  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriIndexedId, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriIndexedId");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;
  // nodeDims are the number of dimensions remaining within the node after the single index express is removed
  unsigned nodeDims = node.PackedDimension() + node.UnPackedDimension();

  bool isASlice = (nodeDims > 1 );

  if ( isASlice ) modifier << ":HasSlice";
  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriIndexedMemoryId, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriIndexedMemoryId");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;
  // nodeDims are the number of dimensions remaining within the node after the indxExpressions are removed
  unsigned nodeDims = node.PackedDimension() + node.UnPackedDimension(); 

  bool isASlice = (nodeDims > 1 );

  if ( isASlice ) modifier << ":HasSlice";


  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}






Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriFunctionCall, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriFunctionCall");
  TRACE_SVINSPECTOR_WALK(name.c_str(), node.GetName());

  UtString modifier;
  if ( mSVInspector.isFromPackage(node.GetId()) ) { modifier << ":fromPackage"; }
  if ( mSVInspector.isFromInterface(node.GetId()) ) { modifier << ":fromInterface"; }

  UtString msg;
  msg << name << ":" << node.GetName() << modifier ; // tack on name of function

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriSystemFunctionCall, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriSystemFunctionCall");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());
  unsigned fctType = node.GetFunctionType();
  (void) fctType;               // from veri_tokens.h (like VERI_SYS_CALL_LEFT)

  UtString msg;
  UtString modifier;

  modifier << ":" << node.GetName();

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}



Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriUnaryOperator, node, phase)
{
  UtString msg("VeriUnaryOperator");
  msg << mSVInspector.getPrintToken(node.OperType());

  TRACE_SVINSPECTOR_WALK(msg.c_str(),msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriAnsiPortDecl, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriAnsiPortDecl");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  Verific::Array* defs = node.GetIds();
  Verific::VeriIdDef *idDef;
  unsigned i ;
  FOREACH_ARRAY_ITEM(defs, i, idDef) {
    UtString msg;
    UtString modifier;
    if ( idDef->Type() == VERI_PACKAGE ) { modifier << ":fromPackage"; }
    if ( idDef->Type() == VERI_INTERFACE ) { modifier << ":fromInterface"; }

    if ( Verific::ID_VERIVARIABLE == idDef->GetClassId() ) {
      Verific::VeriVariable* myV = (static_cast<Verific::VeriVariable*>(idDef));
      fillVeriVariableModifier(*myV, &modifier);
    }

    msg <<name << modifier;

    mSVInspector.logMsg(phase, msg, node.Linefile());
  }


  return NORMAL;
}


Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriBinaryOperator, node, phase)
{
  UtString msg("VeriBinaryOperator");
  msg << mSVInspector.getPrintToken(node.OperType());

  TRACE_SVINSPECTOR_WALK(msg.c_str(),msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriIdDef, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriIdDef");
  TRACE_SVINSPECTOR_WALK(name.c_str(), node.GetName());

  UtString modifier;

  if ( mSVInspector.isFromPackage(&node) ) { modifier << ":fromPackage"; }
  if ( mSVInspector.isFromInterface(&node) ) { modifier << ":fromInterface"; }
  if ( mSVInspector.isBoundedQueue(&node) ) { modifier << ":BoundedQueue"; }
  if ( mSVInspector.isUnBoundedQueue(&node) ) { modifier << ":UnBoundedQueue"; }


  if ( node.IsPort() && ! node.IsAnsiPort() ) {
    // only handle non-ansi ports here, any ansi ports are handled at visitor for VeriAnsiPortDecl
    modifier << ":nonAnsiPort";
    if ( Verific::ID_VERIVARIABLE == node.GetClassId() ) {
      Verific::VeriVariable* myV = (static_cast<Verific::VeriVariable*>(&node));
      fillVeriVariableModifier(*myV, &modifier);
    }
  }

  UtString msg;
  msg << name << modifier;


  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

void SvinspectorCB::fillVeriVariableModifier(const Verific::VeriVariable& node, UtString* modifier)
{
  unsigned numDimensions = (node.UnPackedDimension()+node.PackedDimension());
  bool hasInit = (node.GetInitialValue());
  bool isAutomatic = node.IsAutomatic();

  if ( numDimensions == 0 ){
    (*modifier) << ":noDimensions";
  } else {
    if (node.UnPackedDimension() > 0){
      (*modifier) << ":has" << node.UnPackedDimension() << "unpackedDimensions";
    }
    if (node.PackedDimension() > 0){
      (*modifier) << ":has" << node.PackedDimension() << "packedDimensions";
    }
  }
  (*modifier) << ( hasInit ? ":initialAssign" : "");
  (*modifier) << ( isAutomatic ? ":automatic" : "");
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriVariable, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString modifier;
  fillVeriVariableModifier(node, &modifier);

  UtString msg("VeriVariable");
  msg << modifier;

  TRACE_SVINSPECTOR_WALK(msg.c_str(),msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriInstId, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriInstId");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;

  if ( node.HasOrderedPortAssoc() ) modifier << ":hasOrderedPortAssoc";
  if ( node.HasNamedPortAssoc() ) modifier << ":hasNamedPortAssoc";
  if ( node.IsInterfaceInst() ) modifier << ":fromInterface";
  if ( node.UnPackedDimension() > 0 ) modifier << ":has" << node.UnPackedDimension() << "UnpackedDimensions";
  

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}


Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriModuleId, node, phase)
{
  UtString name("VeriModuleId");
  TRACE_SVINSPECTOR_WALK(name.c_str(), node.Name());

  UtString msg;
  UtString modifier;

  msg <<name << modifier;
  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriParamId, node, phase)
{
  UtString name("VeriParamId");

  TRACE_SVINSPECTOR_WALK(name.c_str(), node.Name());

  UtString msg;
  UtString modifier;

  msg <<name << modifier;
  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriRange, node, phase)
{
  bool isDynamic = node.IsDynamicRange();
  UtString modifier;
  modifier << (isDynamic ? ".hasDynamicRange" : ".staticRange");

  UtString msg("VeriRange");
  msg << modifier;


  TRACE_SVINSPECTOR_WALK(msg.c_str(), msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriDataDecl, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}
  UtString name("VeriDataDecl");
  UtString trace_msg;
  trace_msg <<  "node  (" << node.GetClassId() << ")  " << Verific::LineFile::GetFileName(node.Linefile()) << ":" << Verific::LineFile::GetLineNo(node.Linefile());
  TRACE_SVINSPECTOR_WALK(name.c_str(), trace_msg.c_str());

  UtString modifier;

  // there may be a list of ids but we just use the first as a representative for package/interface location
  Verific::Array* defs = node.GetIds();
  Verific::VeriIdDef *idDef;
  unsigned i ;
  FOREACH_ARRAY_ITEM(defs, i, idDef) {
    if ( mSVInspector.isFromPackage(idDef) ) { modifier << ":fromPackage"; }
    if ( mSVInspector.isFromInterface(idDef) ) { modifier << ":fromInterface"; }
    break;                      // the first is enough
  }

  modifier << ( node.GetDir() ? ":Dir" : "");
  unsigned declTypeToken = node.GetDeclType();

  modifier << (( VERI_PARAMETER == declTypeToken ) ? ":parameter" : "");
  modifier << (( VERI_LOCALPARAM == declTypeToken ) ? ":localParam" : "");
  modifier << (( VERI_SPECPARAM == declTypeToken ) ? ":specParam" : "");
  modifier << (( VERI_GENVAR == declTypeToken ) ? ":genvar" : "");

  UtString msg;
  msg << name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriFunctionDecl, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriFunctionDecl");
  name << ":" << node.GetSubprogramName()->GetName();
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;

  Verific::VeriDataType* dataType = node.GetDataType();
  bool isVoidType = false;
  bool isIntType = false;
  mSVInspector.getFlagsForDataType(dataType, &isVoidType, &isIntType);

  if ( node.IsCalledAsConstFunction() ) modifier << ":isCalledAsConstant";
  if ( node.IsVerilogMethod() ) modifier << ":isVerilogMethod";
  if ( isVoidType ) modifier << ":isVoid";
  if ( isIntType ) modifier << ":isInt";

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}


Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriTaskDecl, node, phase)
{
  TRACE_SVINSPECTOR_WALK("VeriTaskDecl", node.GetSubprogramName());

  UtString name("VeriTaskDecl");
  UtString msg;
  UtString modifier;

  msg <<name << modifier;
  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}


Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriModuleInstantiation, node, phase)
{
  TRACE_SVINSPECTOR_WALK("VeriModuleInstantiation", node.GetModuleName());  // want the instance name not the module name here
  switch (phase){ 
  case VISITPRE: {
    mSVInspector.sawLU("VeriModuleInstantiation",node.Linefile());
    (void) doProcessModuleContents(true);
    mSVInspector.addModuleInstance(static_cast<Verific::VeriModuleItem*>(&node));
    break;
  }
  case VISITPOST: {
    mSVInspector.popModuleInstance();
    break;
  }
  }
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriBlockingAssign, node, phase)
{
  UtString name("VeriBlockingAssign");
  TRACE_SVINSPECTOR_WALK(name.c_str(), mSVInspector.getPrintToken(node.OperType()));
  UtString modifier;
  
  modifier << ":" << mSVInspector.getPrintToken(node.OperType());
  
  UtString msg;
  msg << name << modifier;


  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriNonBlockingAssign, node, phase)
{
  TRACE_SVINSPECTOR_WALK("VeriNonBlockingAssign", mSVInspector.getPrintToken(node.OperType()));
  UtString name("VeriNonBlockingAssign");
  UtString msg;
  UtString modifier;

  msg <<name << modifier;
  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriGenVarAssign, node, phase)
{
  TRACE_SVINSPECTOR_WALK("VeriGenVarAssign", ((node.GetName()) ? node.GetName() : node.GetId()->GetName()));

  UtString name("VeriGenVarAssign");
  UtString msg;
  UtString modifier;
  modifier << ((node.GetName()) ? node.GetName() : node.GetId()->GetName());

  msg <<name << modifier;
  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriAssign, node, phase)
{
  UtString msg("0x");
  msg << (void*)(&node);

  TRACE_SVINSPECTOR_WALK("VeriAssign", msg.c_str());

  switch (phase){ 
  case VISITPRE: {
    mSVInspector.sawLU("VeriAssign",node.Linefile());
    break;
  }
  case VISITPOST: {
    break;
  }
  }
  return NORMAL;
}
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriDeAssign, node, phase)
{
  UtString msg("0x");;
  msg << (void*)(&node);
  TRACE_SVINSPECTOR_WALK("VeriDeAssign", msg.c_str());

  switch (phase){ 
  case VISITPRE: {
    mSVInspector.sawLU("VeriDeAssign",node.Linefile());
    break;
  }
  case VISITPOST: {
    break;
  }
  }
  return NORMAL;
}
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriTaskEnable, node, phase)
{
  UtString name("VeriTaskEnable");
  TRACE_SVINSPECTOR_WALK(name.c_str(), node.GetTaskName()->GetName());
  UtString modifier;
  if ( mSVInspector.isFromPackage(node.GetTaskName()->GetId()) ) { modifier << ":fromPackage"; }
  if ( mSVInspector.isFromInterface(node.GetTaskName()->GetId()) ) { modifier << ":fromInterface"; }

  UtString msg;
  msg << name << modifier << ":" << node.GetTaskName()->GetName();

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriSystemTaskEnable, node, phase)
{
  UtString msg("VeriSystemTaskEnable");
  msg << ":" << node.GetName();
  TRACE_SVINSPECTOR_WALK(msg.c_str(),msg.c_str());
  UtString modifier;

  bool hasPercentP = false;
  bool hasPercentV = false;

  unsigned fctType = node.GetFunctionType();
  switch ( fctType ){
  case VERI_SYS_CALL_DISPLAY:
  case VERI_SYS_CALL_DISPLAYB:
  case VERI_SYS_CALL_DISPLAYH:
  case VERI_SYS_CALL_DISPLAYO:
  case VERI_SYS_CALL_FDISPLAY:
  case VERI_SYS_CALL_FDISPLAYB:
  case VERI_SYS_CALL_FDISPLAYH:
  case VERI_SYS_CALL_FDISPLAYO:
  {
    unsigned i ;
    Verific::VeriExpression *ve_expr ;
    Verific::Array* ve_experssions = node.GetArgs();
    FOREACH_ARRAY_ITEM(ve_experssions, i, ve_expr) {
      if ( ve_expr && ve_expr->IsConst() ){
        UtString format_string(ve_expr->Image()); // FYI image is surrounded by quotes 
        format_string.lowercase(); // fold to lower so search below is simipler
        if (( format_string.find("%p") != UtString::npos) ||
            ( format_string.find("%0p") != UtString::npos)  ){
          hasPercentP = true;
        }
        if (( format_string.find("%v") != UtString::npos) ) {
          hasPercentV = true;
        }
        if ( hasPercentV && hasPercentP ) {
          break;
        }
      }
    }
    break;
  }
  }
  modifier << ( (hasPercentP) ? ":has%P": "");
  modifier << ( (hasPercentV) ? ":has%V": "");

  msg << modifier;
  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriConditionalStatement, start_node, phase)
{
  Verific::VeriConditionalStatement &node = static_cast<Verific::VeriConditionalStatement&>(start_node);

  bool isUnique = node.GetQualifier(VERI_UNIQUE) ;
  bool isUnique0 = node.GetQualifier(VERI_UNIQUE0) ;
  bool isPriority = node.GetQualifier(VERI_PRIORITY) ;

  UtString condStmtModifier;
  if ( isUnique ) condStmtModifier = ":withUnique";
  if ( isUnique0 ) condStmtModifier = ":withUnique0";
  if ( isPriority) condStmtModifier = ":withPriority";

  UtString msg("VeriConditionalStatement");
  msg << condStmtModifier;
  
  TRACE_SVINSPECTOR_WALK("VeriConditionalStatement", msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriCaseStatement, node, phase)
{
  bool isUnique = node.GetQualifier(VERI_UNIQUE) ;
  bool isUnique0 = node.GetQualifier(VERI_UNIQUE0) ;
  bool isPriority = node.GetQualifier(VERI_PRIORITY) ;

  UtString condStmtModifier;
  if ( isUnique ) condStmtModifier = ":withUnique";
  if ( isUnique0 ) condStmtModifier = ":withUnique0";
  if ( isPriority) condStmtModifier = ":withPriority";

  UtString msg("VeriCaseStatement");
  msg << condStmtModifier;
  
  TRACE_SVINSPECTOR_WALK("VeriCaseStatement", msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}






Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriAlwaysConstruct, node, phase)
{
  TRACE_SVINSPECTOR_WALK("VeriAlwaysConstruct", mSVInspector.getPrintToken(node.OperType()));

  switch (phase){ 
  case VISITPRE: {
    if ( node.GetQualifier(VERI_ALWAYS_COMB) ) {
      mSVInspector.sawLU("VeriAlwaysConstruct:COMB",node.Linefile());
    } else if ( node.GetQualifier(VERI_ALWAYS_LATCH) ){
      mSVInspector.sawLU("VeriAlwaysConstruct:LATCH",node.Linefile());
    } else if ( node.GetQualifier(VERI_ALWAYS_FF) ){
      mSVInspector.sawLU("VeriAlwaysConstruct:FF",node.Linefile());
    } else if ( node.GetQualifier(VERI_FINAL) ){
      mSVInspector.sawLU("VeriAlwaysConstruct:FINAL",node.Linefile());
    } else if ( node.GetQualifier(VERI_ALWAYS) ){
      mSVInspector.sawLU("VeriAlwaysConstruct",node.Linefile());
    }
    break;
  }
  case VISITPOST: {
    break;
  }
  }
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriIntVal, node, phase)
{
  CarbonUInt32 num = node.GetNum();
  UtString msg;
  msg << num;
  TRACE_SVINSPECTOR_WALK("VeriIntVal", msg.c_str() );

  switch (phase){ 
  case VISITPRE: {
    mSVInspector.sawLU("VeriIntVal",node.Linefile());
    break;
  }
  case VISITPOST: {
    break;
  }
  }
  return NORMAL;
}


Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriRealVal, node, phase)
{
  float num = node.GetNum();
  UtString msg;
  msg << num;
  TRACE_SVINSPECTOR_WALK("VeriRealVal", msg.c_str() );

  switch (phase){ 
  case VISITPRE: {
    mSVInspector.sawLU("VeriRealVal",node.Linefile());
    break;
  }
  case VISITPOST: {
    break;
  }
  }
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriInterface, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriInterface");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriClass, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriClass");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;
  if ( mSVInspector.isFromPackage(node.GetId()) ) { modifier << ":fromPackage"; }
  if ( mSVInspector.isFromInterface(node.GetId()) ) { modifier << ":fromInterface"; }

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}





// does this ever get visited after elaboration?
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriPackage, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriPackage");
  TRACE_SVINSPECTOR_WALK(name.c_str(), node.Name());

  UtString msg;
  UtString modifier;

  
  if (node.GetId()->GetOwningScope()->GetOwner()->IsPackage()){
    modifier << ":fromPackage" << node.GetId()->GetOwningScope()->GetOwner()->GetName();
  } else {
    modifier <<  ":notFrompackage";
  }
  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

// here we assume the usage of a VeriTypeRef means that there was a typedef involved.
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriTypeRef, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriTypeRef");
  TRACE_SVINSPECTOR_WALK(name.c_str(), node.GetName());

  UtString modifier;

  if ( mSVInspector.isFromPackage(node.GetId()) ) { modifier << ":fromPackage"; }
  if ( mSVInspector.isFromInterface(node.GetId()) ) { modifier << ":fromInterface"; }

  modifier <<  ":" << node.GetName();

  if ( node.GetId()->GetOwningScope() && node.GetId()->GetOwningScope()->GetOwner() )
  {
    // check for nested packages
    UtString curPkgSuffix ("Package:");
  
    Verific::VeriIdDef *temp_idDef = node.GetId()->GetOwningScope()->GetOwner();

    // check if there are nested packages
    while ( NULL != (temp_idDef = mSVInspector.getNearestPackage(temp_idDef)) ) {
      UtString tempSuffix("Within");
      tempSuffix << curPkgSuffix;
      curPkgSuffix = tempSuffix;
      modifier<< ":nested" << curPkgSuffix << temp_idDef->GetName(); // inserts names like ":nestedWithinWithinPackage:FOO"
      Verific::VeriScope* scope = temp_idDef->GetOwningScope();
      temp_idDef = scope ? scope->GetOwner() : NULL;
    }
  }

  UtString msg;
  msg << name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}


Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriStructUnion, node, phase)
{
  UtString name;
  if ( node.IsStructType() )     name << "VeriStruct";
  else if ( node.IsUnionType() ) name << "VeriUnion";
  else                           name << "VeriStructUnion:undefined";

  UtString modifier;
  if ( node.IsPacked() ) { modifier << ":Packed"; }
  if ( node.IsUnpacked() ) { modifier << ":UnPacked"; }
  if ( node.IsTagged() ) { modifier << ":Tagged"; }
  modifier << ( ( node.Sign() ) ? ":Signed" : ":Unsigned");

  // name of struct/union is outside of this context
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  bool thisIntType = false;
  bool hasVoidType = false;
  unsigned i ;
  Verific::VeriDataDecl *decl ;
  Verific::Array* decls = node.GetDecls();
  FOREACH_ARRAY_ITEM(decls, i, decl) {
    if (!decl) continue ;
    if (!(decl->IsDataDecl())) continue;
    thisIntType = false;
    Verific::VeriDataType * dtype = decl->GetDataType();
    mSVInspector.getFlagsForDataType(dtype, &hasVoidType, &thisIntType);
    if ( hasVoidType && ! thisIntType ) break;  // no need to look further
  }
  modifier << ( thisIntType ? ":AllIntType" : ":notAllIntType");
  if ( hasVoidType ) { modifier << ":hasVoidType"; }


  UtString msg;
  msg <<name << modifier;

  TRACE_SVINSPECTOR_WALK(name.c_str(), msg.c_str());
  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}


Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriEnum, node, phase)
{
  bool hasXZ = false;
  bool hasDeclared = false;
  unsigned i ;
  Verific::VeriIdDef *idDef ;
  FOREACH_ARRAY_ITEM(node.GetIds(), i, idDef) {
    if ( idDef->GetClassId() == Verific::ID_VERIPARAMID ){
      Verific::VeriParamId *param_node = static_cast<Verific::VeriParamId*>(idDef);
      if ( param_node->GetInitialValue() ){
        {
          // FIXME GetInitialValue is not sufficient to detect if the HDL specifies an initial value, it will return a value even for the defalut values
          //debugging
          //param_node->GetInitialValue()->PrettyPrint(std::cout, 0);
          //std::cout<<endl;
        }
        hasDeclared = true;
        Verific::VeriExpression* iv = param_node->GetInitialValue();
        if ( iv->HasXZ() ){
          hasXZ = true;
          break;
        }
      }
    }
  }
  UtString modifier;
  if ( hasXZ ) {
    modifier << ":WithXZ";
  } else if ( hasDeclared ){
    modifier << ":WithInitVal";
  }

  UtString msg("VeriEnum");
  msg << modifier;


  TRACE_SVINSPECTOR_WALK(msg.c_str(), msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriCast, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriCast");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;
  Verific::VeriExpression* ve_targetType =  node.GetTargetType();
  if ( ve_targetType && ve_targetType->IsConst() ) {
    modifier << ":toNumBits";
  } else {
    modifier << ":toType";
    if ( ve_targetType->IsDataType() ){
      Verific::VeriDataType* ve_datatype =  ve_targetType->GetDataType();
      Verific::VeriName* ve_typename =  ve_datatype->GetTypeName();
      if ( ve_typename ) {
        modifier << ":" << ve_typename->GetName();
      }
    }
  }

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriJumpStatement, node, phase)
{
  UtString name("VeriJumpStatement");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;

  modifier << ":" << Verific::VeriNode::PrintToken(node.GetJumpToken());

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
  
}

// is this a declaration of a typedef?
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriTypeId, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriTypeId");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriStreamingConcat, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriStreamingConcat");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;
  modifier << mSVInspector.getPrintToken(node.OperType());

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriForeach, node, phase)
{
  bool hasIndexVariable = false;
  unsigned i ;
  Verific::VeriIdDef *idDef ;
  FOREACH_ARRAY_ITEM(node.GetLoopIndexes(), i, idDef) {
    if ( idDef->GetClassId() == Verific::ID_VERIVARIABLE ){
      hasIndexVariable = true;
    }
  }

  UtString modifier;
  modifier << (hasIndexVariable ? ":WithIndexVar" : "");

  UtString msg("VeriForeach");
  msg << modifier;


  TRACE_SVINSPECTOR_WALK(msg.c_str(), msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());

  return NORMAL;
}

// this is just the &&& operator in a VeriQuestionColon
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriCondPredicate, node, phase)
{
  //{/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriCondPredicate");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier(":&&&");

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}



// this is an VeriExpression
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriTaggedUnion, node, phase)
{
  //  {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriTaggedUnion");

  UtString msg;
  UtString modifier(":expression");

  msg <<name << modifier;

  TRACE_SVINSPECTOR_WALK(msg.c_str(), msg.c_str());

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriArrayMethodCall, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriArrayMethodCall");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier;

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}





// this is the 'MATCHES' operator
Verific::VerilogDesignWalkerCB::Status SvinspectorCB::VERI_WALK(VeriPatternMatch, node, phase)
{
  // {/*debug*/ node.PrettyPrint(std::cout,0); std::cout<<std::endl;}

  UtString name("VeriPatternMatch");
  TRACE_SVINSPECTOR_WALK(name.c_str(), name.c_str());

  UtString msg;
  UtString modifier(":matches");

  msg <<name << modifier;

  mSVInspector.logMsg(phase, msg, node.Linefile());
  return NORMAL;
}

