/* -*- C++ -*- */
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/c_util.h"
#include <string.h>
#include <errno.h>

#if pfWINDOWS
#include <windows.h>
#endif

char* carbon_lasterror(char* buf, size_t len)
{
#if pfWINDOWS
  void* errBuf;
  DWORD error = GetLastError();
  
  // Allocate a buffer in buff and fill it with the message
  // corresponding to the error.
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                FORMAT_MESSAGE_FROM_SYSTEM,
                NULL,
                error,
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                (LPTSTR) &errBuf,
                0, NULL);
  strncpy(buf, (char*)errBuf, len);
  buf[len - 1] = '\0';
  LocalFree(errBuf);
  return buf;

#elif pfLINUX && pfGCC
  return strerror_r(errno, buf, len);

#else
  char* str = strerror(errno);
  strncpy(buf, str, len);
  
  // paranoia
  // terminate the buffer properly.
  buf[len - 1] = '\0';
  
  return buf;
#endif
}
