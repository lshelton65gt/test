/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CARBON_HASHTABLE_PRIVATE_H_
#define __CARBON_HASHTABLE_PRIVATE_H_

#define HASH_EXPAND_ENTRY(smallptr) \
  SMALLPTR_EXPAND(carbon_hashEntry*, smallptr)

#define HASH_GET_TABLE(hashTable) \
  ((hashTable->sizes.primeIndex == 0) \
   ? &hashTable->data.singlebucket \
   : hashTable->data.table)

#define HASH_FIND_BUCKET(bucketVarDecl, hashTable, hashVal, bucketIndex) \
  UInt32 __HB_idx = hashTable->sizes.primeIndex; \
  UInt32 bucketIndex = (hashVal) % carbonPrimes[__HB_idx].numBuckets; \
  bucketVarDecl = HASH_EXPAND_ENTRY((__HB_idx == 0)? \
                                    hashTable->data.singlebucket: \
                                    hashTable->data.table[bucketIndex])

  
#define HASH_GET_BUCKET_PTR(bucketVarDecl, hashTable, bucketIndex) \
  UInt32 __HB_idx = hashTable->sizes.primeIndex; \
  bucketVarDecl = (__HB_idx == 0)? &hashTable->data.singlebucket: \
    &hashTable->data.table[bucketIndex]

#define HASH_FIND_BUCKET_PTR(bucketVarDecl, hashTable, hashVal, bucketIndex) \
  UInt32 __HB_idx = hashTable->sizes.primeIndex; \
  UInt32 bucketIndex = (hashVal) % carbonPrimes[__HB_idx].numBuckets; \
  bucketVarDecl = (__HB_idx == 0)? &hashTable->data.singlebucket: \
    &hashTable->data.table[bucketIndex]


#endif
