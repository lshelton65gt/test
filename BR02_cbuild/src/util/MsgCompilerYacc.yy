/* -*-C++-*- */
%{
#include <stdlib.h>
#include "MsgCompiler.h"
#include <string>
#define YYERROR_VERBOSE 1
using namespace std;          // can't seem to make std::string work in %union

class Msg;
static Msg* sCurrentMsg;
static int sLineNumber;
static const char* sFileName;
MsgCompiler* sMsgCompiler = NULL;
%}

%union {
  int ival;
  std::string* text;
}

%token <ival> NUM
%token <text> ID
%token <text> STRING
%token <ival> WARNING
%token <ival> ERROR
%token <ival> FATAL
%token <ival> ALERT
%token <ival> CONTINUE
%token <ival> SUPPRESS
%token <ival> NOTE
%token <ival> INCLUDE
%start msg_file

%%

msg_file :  msg_decl msg_file
         | /* empty */
;

severity : WARNING {Msg::putNextSeverity(eWarning);}
         | FATAL   {Msg::putNextSeverity(eFatal);}
         | ALERT   {Msg::putNextSeverity(eAlert);}
         | ERROR   {Msg::putNextSeverity(eError);}
         | NOTE    {Msg::putNextSeverity(eNote);}
         | CONTINUE{Msg::putNextSeverity(eContinue);}
         | SUPPRESS{Msg::putNextSeverity(eSuppress);}
;
         
location : '@' msg_arg              {sCurrentMsg->putHasLocation(true);}
         | /* empty */              {sCurrentMsg->putHasLocation(false);}
;

msg_decl : severity NUM ':' ID     {sCurrentMsg = new Msg($2, $4->c_str());
                                    sMsgCompiler->append(sCurrentMsg);
                                    delete $4;}
           location ':'
           msg_text ':'
           help_text ';'
         | INCLUDE STRING          {sMsgCompiler->addInclude($2->c_str());
                                    delete $2;}
         | '#'                     {}
;

type_list: ID type_list         {sCurrentMsg->getArg()->addType(*$1);
                                 delete $1;}
         | /* empty */
;

msg_comp : STRING               {sCurrentMsg->addString($1->c_str());
                                 delete $1;}
         | msg_arg
;

msg_arg :  '(' type_list ':' ID ')' {sCurrentMsg->getArg()->putVariableName($4->c_str());
                                 sCurrentMsg->finalizeArg();
                                 delete $4;}
         | '(' type_list '*' ':' ID ')' {sCurrentMsg->getArg()->putVariableName($5->c_str());
                                 sCurrentMsg->getArg()->putIsPointer();
                                 sCurrentMsg->finalizeArg();
                                 delete $5;}
         | '(' type_list '&' ':' ID ')' {sCurrentMsg->getArg()->putVariableName($5->c_str());
                                 sCurrentMsg->getArg()->putIsReference();
                                 sCurrentMsg->finalizeArg();
                                 delete $5;}
;

msg_text : msg_comp
         | msg_comp '+' msg_text
;

help_text : STRING                {sCurrentMsg->addHelp($1->c_str());
                                   delete $1;}
          | STRING '+' help_text  {sCurrentMsg->addHelp($1->c_str());
                                   delete $1;}
;
%%

int yyerror(const char* text) {
  fprintf(stderr, "%s:%d: parser error: %s\n", sFileName, sLineNumber, text);
  sMsgCompiler->bumpErrorCount();
  return 0;
}

void gYaccNewline() {
  ++sLineNumber;
}

std::string* gYaccMkstr(const char* buf, int len, int trim) {
  std::string* str = new std::string();
  // Walk over the string and accomodate slashified characters
  // according to usual C rules
  len -= trim;
  for (int i = trim; i < len; ++i) {
    char c = buf[i];
    if (c == '\\') {
      ++i;
      c = buf[i];
      if (i == len) {
        yyerror("Backslash at end of string");
      }
      else {
        switch (c) {
        case '\\': str->append("\\"); break;
        case 'n':  str->append("\n"); break;
        case 't':  str->append("\t"); break;
        case '"':  str->append("\""); break;
        default: {
          char msg[100];
          sprintf(msg, "Unexpected backslash sequence: %c", c);
          yyerror(msg);
          break;
        }
        }
      }
    }
    else {
      if (c == '\n') {
        gYaccNewline();
      }
      str->append(&c, 1);
    }
  } // for
  return str;
}

extern "C" int MsgCompiler_wrap() {
  return 1;                     // No more input files
}

extern FILE* MsgCompiler_in;

void MsgCompiler::parse(const char* file)
{
  FILE* f = fopen(file, "r");
  if (f == NULL) {
    perror(file);
    ++mErrorCount;
  }
  MsgCompiler_in = f;
  sCurrentMsg = NULL;
  sLineNumber = 1;
  sFileName = file;
  MsgCompiler_parse();
  fclose(f);
}

MsgCompiler::MsgCompiler()
{
  mCurrentMsg = NULL;

  // For now there can be at most 1 MsgCompiler resident at any one
  // time, which makes it convenient for the yacc rules
  // to know where to put stuff.
  assert(sMsgCompiler == NULL); // this_assert_OK
  sMsgCompiler = this;
  mErrorCount = 0;
}

MsgCompiler::~MsgCompiler()
{
  assert(sMsgCompiler == this); // this_assert_OK
  sMsgCompiler = NULL;
}
