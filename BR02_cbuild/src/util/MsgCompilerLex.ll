%{ /* -*-C++-*- */
#include <stdlib.h>
#include <cstring>
#include <string>
#include "util/OSWrapper.h"

#include "MsgCompilerYacc.hxx"

extern std::string* gYaccMkstr(const char*, int, int);
extern void gYaccNewline();
#define YY_NO_UNPUT
%}

/* To avoid isatty problems on Windows. */
%option never-interactive

letter                          [a-zA-Z]
DIGIT                           [0-9]
ident                           {letter}({letter}|{DIGIT}|_)*
%x COMMENT

%%
\"(\n|\\.|[^\\"])*\"            {MsgCompiler_lval.text = gYaccMkstr(yytext, yyleng, 1);
                                 return STRING;}

"/*"	 			{ BEGIN COMMENT; /* Enter comment eating state */}
<COMMENT>"*/"	 		{ BEGIN 0;}
<COMMENT>\n 			{gYaccNewline();}
<COMMENT>.			;


"//"[^\n]*\n			{gYaccNewline();} /* Ignore C++ comments to end of line. */

{DIGIT}+                        {MsgCompiler_lval.ival = atoi(yytext);
                                 //printf("int %d\n", MsgCompiler_lval.ival);
                                 return NUM;}
"continue"                      {return CONTINUE;}
"suppress"                      {return SUPPRESS;}
"warning"                       {return WARNING;}
"error"                         {return ERROR;}
"alert"                         {return ALERT;}
"fatal"                         {return FATAL;}
"note"                          {return NOTE;}
"include"                       {return INCLUDE;}

{ident}                         {MsgCompiler_lval.text = gYaccMkstr(yytext, yyleng, 0); return ID;}
":"                             {return ':';}
";"                             {return ';';}
"+"                             {return '+';}
"*"                             {return '*';}
"&"                             {return '&';}
"@"                             {return '@';}
"("                             {return '(';}
")"                             {return ')';}
"<"                             {return '<';}
">"                             {return '>';}
"#[^\n]*"                       {return '#';}
"\n"                            {gYaccNewline();}
[ \t]+				{}
%%
