// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/StringAtom.h"
#include "util/UtHashMap.h"
#include "util/LoopMap.h"
#include "util/UtString.h" // for UtString::hash

/*!
  \file
  Implementation of AtomicCache
*/

//! Class to hide STL string mapping
class AtomicCache::StringCache
{
public: CARBONMEM_OVERRIDES

  struct CharStarHash {
    size_t hash(const char* str) const {return UtString::hash(str);}
    bool equal(const char* s1, const char* s2) const {return strcmp(s1, s2) == 0;}
    bool lessThan1(const char* s1, const char* s2) const {return s1 < s2;}
    bool lessThan(const char* s1, const char* s2) const {return strcmp(s1, s2) < 0;}
  };

  //! String to StringAtom map type
  typedef UtHashMap<const char*, StringAtom*, CharStarHash> StringMap;
  //! Loop iteration. Used by AtomicCache to pass to IterMap
  typedef LoopMap<StringMap > StrIter;

  //! constructor
  StringCache() 
  {}

  //! Destructor - destroys strings
  ~StringCache() {
    for (StringMap::iterator p = mStrs.begin(), e = mStrs.end(); p != e; ) 
    {
      const char* v = p->first;
      ++p;
      CarbonMem::free((void*) v);
    }
    mStrs.clear();

    // Can't call this because char* require delete []
    // mStrs.clearPointerKeys();
  }

  //! Loop the map
  StrIter loopCache() 
  {
    return StrIter(mStrs);
  }
  
  //! Num elements in the map
  UInt32 size() const
  {
    return static_cast<UInt32>(mStrs.size());
  }

  char* makeStr(const char* str, size_t len) {
    char* gen = (char*) CarbonMem::malloc(len + 1); // allow room for \0
    MEMCPY(gen, str, len);
    gen[len] = '\0';
    return gen;
  }

  //! Get the associated StringAtom or create one
  StringAtom* maybeCreate(const char* str, size_t len)
  {
    StringAtom* ret = NULL;

    // if the user didn't null-terminate the string passed in,
    // then we'll have to make a temp copy, which we can then
    // use for the permanent storage if it was not already in
    // the table.
    char* gen = NULL;
    if (str[len] != '\0')
      str = gen = makeStr(str, len);

    StringMap::iterator p = mStrs.find(str);
    if (p != mStrs.end())
    {
      StringMap::NameValue mapPair = *p;
      ret = mapPair.second;
      if (gen != NULL)
        CarbonMem::free(gen);
    }
    else
    {
      if (gen == NULL)
        gen = makeStr(str, len);
      ret = reinterpret_cast<StringAtom*>(gen);
      mStrs[gen] = ret;
    }
    
    return ret;
  }

  //! Find a StringAtom. Return NULL if it doesn't exist
  StringAtom* find(const char* str)
  {
    StringAtom* ret = NULL;
    StringMap::const_iterator p = mStrs.find(str);
    
    if (p != mStrs.end())
    {
      StringMap::NameValue mapPair = *p;
      ret = mapPair.second;
    }
    return ret;
  }
  
private:
  StringMap mStrs;
};

AtomicCache::AtomicCache() 
{
  mStringCache = new StringCache;
}

AtomicCache::~AtomicCache()
{
  delete mStringCache;
}

UInt32 AtomicCache::size() const
{
  return mStringCache->size();
}

AtomicCache::CacheIter
AtomicCache::getCacheIter() 
{
  //return CacheIter(mStringCache);
  return CacheIter::create(mStringCache->loopCache());
}

StringAtom* AtomicCache::intern(const char* str)
{
  return intern(str, strlen(str));
}

StringAtom* AtomicCache::intern(const char* str, size_t len)
{
  return mStringCache->maybeCreate(str, len);
}

StringAtom* AtomicCache::intern(const UtString& str)
{
  return mStringCache->maybeCreate(str.c_str(), str.size());
}

StringAtom* AtomicCache::getIntern(const char* str) const
{
  return mStringCache->find(str);
}

