// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// This file contains overrides for operator new and delete.
// The memory manager does not depend on these, and so if you
// want to use Carbon's memory manager, to pull it in from the
// library you must call CarbonMem::overrideNewDelete() from main()

#include "util/MemManager.h"
#if pfGCC_3 | pfGCC_4
#include <exception_defines.h>
#endif
#include "new"

void
operator delete (void *ptr) throw()
{
  if (ptr != NULL)
    carbonmem_free(ptr);
}

void
operator delete(void *ptr, const std::nothrow_t&) throw()
{
  if (ptr != NULL)
    carbonmem_free (ptr);
}

void
operator delete[](void *ptr) throw()
{
  if (ptr != NULL)
    carbonmem_free(ptr);
}

void
operator delete[](void *ptr, const std::nothrow_t&) throw()
{
  if (ptr != NULL)
    carbonmem_free(ptr);
}

void *
operator new (std::size_t sz, const std::nothrow_t & /* nothrow -- unused */)
  throw()
{
  return carbonmem_malloc(sz);
}

void *
operator new (std::size_t sz) throw(std::bad_alloc)
{
  return carbonmem_malloc(sz);
}

void*
operator new[](std::size_t sz, const std::nothrow_t & /* nothrow -- unused */)
  throw()
{
  return carbonmem_malloc(sz);
}

void*
operator new[](std::size_t sz) throw(std::bad_alloc)
{
  return carbonmem_malloc(sz);
}


// This function doesn't actually do anything but force linking in this
// module
void CarbonMem::overrideNewDelete()
{
}
