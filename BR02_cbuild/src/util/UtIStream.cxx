// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// Open issues:
// 1) Lack of comments. Some more inline comments for peakBuffer, and other 
//    larger methods would be nice. But, you can do that when you clean this 
//    stuff up more.
// 2) clean up doxygen comments for UtIStream & new functions in UtFileBuf
// 3) Consider Creating UtOStream.h and move relevant declarations there,
//    changing all the references that are needed everywhere

#include "util/MutexWrapper.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtIZStream.h"
#include "util/UtStringUtil.h"
#include "util/UtShellTok.h"
#include "util/OSWrapper.h"
#include <new>
#include <cerrno>
#include <fcntl.h>              // O_WRONLY, etc
#include <sys/types.h>
#include <sys/stat.h>
#if pfMSVC
#include <io.h>
#else
#include <unistd.h>             // write()
#endif
#include "util/UtCachedFileSystem.h"
#include "util/UtConv.h"
#include "util/BitVector.h"
#include "util/DynBitVector.h"
#include "util/UtCheckpointStream.h"
#include <ctype.h>              // isalpha, topupper
#include <algorithm>            // std::min

#define MIN_BUFSIZ 200          // must be larger than LARGE_TOKEN_SIZE to allow
                                // nul termination of any buffer
#define LARGE_TOKEN_SIZE 100    // must fit a binary UInt64 or a double
#define SMALL_TOKEN_SIZE 40     // must fit a binary UInt32

// Fast maps indicating whether a character is part of various
// legal sets.  This is used for tokenizing words separated by
// whitespace, numbers delimited by any non-numeric character, etc.
static bool sCharsInitialized = false;
static bool sLegalOctChars[256];
static bool sLegalHexChars[256];
static bool sLegalDecChars[256];
static bool sLegalBinChars[256];
static bool sNonSpaceChars[256];

//! Mutex for cin() stream creation/deletion
MUTEX_WRAPPER_DECLARE(sStreamMutex);

static void sInitCharSet() {
  for (const char* s = "0123456789abcdefABCDEF"; *s != '\0'; ++s) {
    sLegalHexChars[(unsigned int) *s] = true;
  }
  for (const char* s = "0123456789"; *s != '\0'; ++s) {
    sLegalDecChars[(unsigned int) *s] = true;
  }
  for (const char* s = "01234567"; *s != '\0'; ++s) {
    sLegalOctChars[(unsigned int) *s] = true;
  }
  for (const char* s = "01"; *s != '\0'; ++s) {
    sLegalBinChars[(unsigned int) *s] = true;
  }
  for (unsigned int i = 0; i < 256; ++i) {
    sNonSpaceChars[i] = !isspace(i);
  }
  sCharsInitialized = true;
}

// Shared constructor -- ensures that the buffer is sized
// large enough for contiguous buffers for parsing numbers
// and other tokens
UtIStream::UtIStream(UInt32 bufsiz) :
  mErrmsg(NULL), 
  mPrevErrmsg(NULL)
{
  // Set up our static tables if they ar not already set up
  if (!sCharsInitialized)
    sInitCharSet();
  mStripCR = false;
  if (bufsiz < MIN_BUFSIZ)
    bufsiz = MIN_BUFSIZ;
  mFileBuf = new UtFileBuf(bufsiz);
  reset();
}

void UtIStream::reset()
{
  if (mErrmsg != NULL)
    delete mErrmsg;
  if (mPrevErrmsg != NULL)
    delete mPrevErrmsg;
  mErrmsg = NULL;
  mPrevErrmsg = NULL;
  mFailure = false;
  mEOF = false;
  mFileBuf->reset();
  mPos = 0;
  mNumReads = 0;
  mUngetBuffers = NULL;
}

UtIStream::~UtIStream()
{
  if (mErrmsg != NULL) {
    delete mErrmsg;
  }
  if (mPrevErrmsg != NULL) {
    delete mPrevErrmsg;
  }
  if (mFileBuf != NULL) {
    delete mFileBuf;
  }
  while (mUngetBuffers != NULL) {
    StringCell* next = mUngetBuffers->next;
    delete mUngetBuffers;
    mUngetBuffers = next;
  }
}

// Pushes arbitrary bytes onto the input stream
void UtIStream::unget(const char* str, UInt32 len) {
  (void) getErrmsg();           // clear out any prior EOF errors, etc
  mFailure = false;             // clear out any parse errors
  mPos -= len;

  // Try to put the string in the existing buffer, if it will fit.
  // The buffer looks like this:
  //     0                                                                bufsiz
  //     CCCCCCCCCCRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRWWWWWWWWWWWWWWWWW
  //               |                                     |
  //               ReadIndex                             WriteIndex
  //
  // In this digagram, C represents characters in the buffer that have already
  // been Consumed.  R represents characters that are Ready to be read.
  // W represents empty buffer space that, should peakBuffer require a 
  // contiguous buffer for parsing that exceeds the number of R characters, 
  // it can read more bytes from the physical file.
  //
  // 1. The best situation for unget is that we put the characters directly into
  //    zone C and move the ReadIndex back.  This is requires no excess memcpys
  //    beyond what is needed to capture str, and no memory allocations.
  //
  // 2. The next alternative is to take the entire zone R and save it in 
  //    auxiliary storage.  We then set the read&write indices in the
  //    middle of the buffer, opening up half the buffer for further calls
  //    to unget, and half the buffer for getting data from the file.
  //    Note that Verilog has ungetc, so if the user wants to
  //    unget multiple characters he will have to call it multiple times,
  //    so it's good to open up a lot of space.
  //
  // 3. If the user wants to unget more bytes than we have total buffer space
  //    for in UtFileBuf then we have no choice but to use auxiliary storage
  //
  // These are coded in order 2,3,4,1 so that we can combine the methods to
  // minimize the total extra work and memory allocation.

  UInt32 readIndex = mFileBuf->getReadIndex(); // readIndex == zone C size
  UInt32 writeIndex = mFileBuf->getWriteIndex();
  UInt32 zone_R_size = writeIndex - readIndex;
  UInt32 writeBufSize;

  if (readIndex < len) {
    // 2. Insufficient space in zone C.  Maximize the space
    //    by moving all of zone R to auxiliary storage
    char* readBuf = mFileBuf->getBufferRemaining(&writeBufSize);
    INFO_ASSERT(writeBufSize == zone_R_size, "Consistency check failed.");
    ungetSaveBuffer(readBuf, zone_R_size);

    // To avoid bad peakBuffer performance, we must leave at more than
    // LARGE_TOKEN_SIZE bytes at the end of the buffer.  So split the
    // difference, giving reasonable available space for subsequent
    // ungets and subsequent peakBuffers.
    UInt32 bufsiz = mFileBuf->size();
    INFO_ASSERT(bufsiz > LARGE_TOKEN_SIZE, "File buffer not large enough.");
    writeIndex = (bufsiz - LARGE_TOKEN_SIZE) / 2;
    mFileBuf->putWriteIndex(writeIndex);

    if (writeIndex < len) {
      // 4. Now, we have cleared out the buffer as much as we care to.  If it
      //    is still not enough it's because the user has tried to unget more 
      //    bytes than we have buffer space for, and we will need to use 
      //    auxiliary storage
      UInt32 saveSize = len - writeIndex;
      ungetSaveBuffer(str + len - saveSize, saveSize);
      len -= saveSize;
      INFO_ASSERT(writeIndex >= len, "Buffer underflow.");
    } // if

    readIndex = writeIndex - len;
    mFileBuf->putReadIndex(readIndex);
    readBuf = mFileBuf->getBufferRemaining(&writeBufSize);
    MEMCPY(readBuf, str, len);
  } // if
  else {
    // guaranteed that zone C is big enough, jam it in & adjust pointers
    mFileBuf->putReadIndex(readIndex - len);
    char* readBuf = mFileBuf->getBufferRemaining(&writeBufSize);
    MEMCPY(readBuf, str, len);
  }
} // void UtIStream::unget

// Internal routine used to save "unget" buffers using additional heap
// space.  unget() tries to avoid calling this, but this is needed to
// handle overflows
void UtIStream::ungetSaveBuffer(const char* str, UInt32 len) {
  // Push the specified text onto a stack of strings to use
  // before going back to the physical file.
  mUngetBuffers = new StringCell(str, len, mUngetBuffers);
}

// String-cell constructor used to to store unget buffers on the heap
UtIStream::StringCell::StringCell(const char* str, int size, StringCell* next):
  str(str, size),
  index(0),
  next(next)
{
}
  

// Read bytes from the input stream.  First checks unget buffers, then
// the physical input file.  This function is shared between all UtIStream
// derivations.  It only goes to the subclass via virtual function readFromFile
// when it's out of unget buffer space
UInt32 UtIStream::readBytes(char* buf, UInt32 len) {
  UInt32 total = 0;
  StringCell* ug = mUngetBuffers;
  while (ug != NULL) {
    UInt32 strLen = ug->str.size() - ug->index;
    UInt32 consumeBytes = std::min(strLen, len);
    memcpy(buf, ug->str.c_str() + ug->index, consumeBytes);
    ug->index += consumeBytes;
    if (ug->index == ug->str.size()) {
      mUngetBuffers = ug->next;
      delete ug;
      ug = mUngetBuffers;
    }
    len -= consumeBytes;
    total += consumeBytes;
    if (len == 0) {
      return total;
    }
    buf += consumeBytes;
  }
  total += readFromFile(buf, len);
  return total;
} // UInt32 UtIStream::readBytes

// Provide fast access to a continuous buffer containing the next 'size'
// bytes of the file.  The caller can mess with the buffer provided,
// including nul-terminating it by writing one character beyond
// *actualSize, as long as he restores that character before reading
// any more data.  Data that is peaked at using this routine is not
// consumed from the input buffer until consumBuffer(n) is called.
//
// Note that any skipped whitespace is consumed by this routine.
bool UtIStream::peakBuffer(UInt32 size,    // bytes requested. must be < bufsiz
                           char** buf,     // returned buffer for data
                           UInt32* actualSize, // returned actual size
                           bool skipWhitespace) // skip whitespace before finding size bytes
{
  INFO_ASSERT(size < mFileBuf->size(), "Requested number of bytes to peak greater than buffersize.");
  while (true) {
    // First try to satisfy the request from the buffer
    UInt32 rdSize;
    char* rdbuf = mFileBuf->getBufferRemaining(&rdSize);
    if (rdSize != 0) {
      char* str = rdbuf;

      // If the user wants to skip whitespace, then move past it
      // and actually consume it.
      if (skipWhitespace) {
        str = const_cast<char*>(StringUtil::skip(rdbuf, rdSize));
        UInt32 numSpaces = str - rdbuf;
        if (numSpaces > rdSize) {
          numSpaces = rdSize;
        }
        rdSize -= numSpaces;
        mFileBuf->consume(numSpaces);
        mPos += numSpaces;
      }

      // The buffer probably contains more than the user wants, but
      // just give him what he wants
      UInt32 numRead = std::min(size, rdSize);

      // We are done if we got what we wanted, or if we've
      // come to the end of the file.
      if ((numRead == size) || (mEOF && (mUngetBuffers == NULL))) {
        // If we got exactly the number of characters we wanted, and that
        // got us to the end of our buffer, then move the bytes to the
        // beginning of the buffer.  We want to return a buffer that the
        // caller can nul-terminate, so it can't be flush to the end of
        // the buffer.  This is why MIN_BUFSIZ must be more than
        // LARGE_TOKEN_SIZE.
        if (mFileBuf->getReadIndex() + numRead == mFileBuf->size()) {
          mFileBuf->moveRemainingBytesToStart();
          str = mFileBuf->getBufferRemaining(&rdSize);
          INFO_ASSERT(rdSize == numRead, "Consistency check failed.");
        }

        // There was enough in the buffer, or we aren't going to get any more
        *buf = str;
        *actualSize = numRead;

        if (numRead == 0) {
          mOSErrmsg = "No more tokens";
          return false;
        }
        return true;
      }

      // If we need to read more data to satisfy the user's request then make
      // contiguous space in the buffer by moving the bytes we've already read
      // to the beginning of the buffer.
      mFileBuf->moveRemainingBytesToStart();
    }

    UInt32 writeBufSize;
    char* writeBuf = mFileBuf->getWriteBuf(&writeBufSize);
    UInt32 sysRead = readBytes(writeBuf, writeBufSize);
    if (sysRead > 0) {
      mFileBuf->putWriteIndex(mFileBuf->getWriteIndex() + sysRead);
    }
    else if (rdSize == 0) {
      mOSErrmsg = "No more tokens";
      return false;
    }
  } // while
  // NOT REACHED
} // bool UtIStream::peakBuffer

// Derived-class implementation to read bytes from a file
UInt32 UtIBStream::readFromFile(char* buf, UInt32 size) {
  int sysRead = 0;
  if (! mEOF) {
    if (!getFD()) {                      // UtCachedFile overrides this
      reportError("File is not open");
      return 0;
    }

    mOSErrmsg.clear();
    sysRead = OSSysRead(mFD, buf, size, &mOSErrmsg);
    if (sysRead <= 0) {
      mEOF = true;
      if (sysRead < 0) {
        reportError(mOSErrmsg.c_str());
        mOSErrmsg.clear();
        sysRead = 0;
      }
    }
    else if (sysRead < (int) size) {
      // If we got less than we expected, it must be because
      // we naturally hit the end of the file.  Don't bother
      // doing another read, stop looking now.
      if (errno == 0) {
        mEOF = true;
      }
    }
  }
  mPhysicalPos += sysRead;
  ++mNumReads;
  return (UInt32) sysRead;
}

// Derived-class implementation to read bytes from a file
UInt32 UtIFileStream::readFromFile(char* buf, UInt32 size) {
  int numRead = 0;
  UtString str;
  if (! mEOF) {
    if (fgets(buf, size, mFile) != NULL)
    {
      //fgets returns a buf of length size if '\n' or EOF is not encountered.
      //fgets is used so that when reading from stdin we are sensitive to '\n'.
      INFO_ASSERT(*buf != '\0', "Buffer overrun.");
      str.append(buf);

      numRead = str.length();
      if (numRead <= 0) {
        if (numRead < 0) {
          UtString errBuf;
          reportError(OSGetLastErrmsg(&errBuf));
          numRead = 0;
        }
        mEOF = true;
      }
    }
    else
    {
      mEOF = true;
    }
  }
  return (UInt32) numRead;
}

//! consume some or all of the bytes that you have peaked at
void UtIStream::consumeBuffer(UInt32 numBytes) {
  mFileBuf->consume(numBytes);
  mPos += numBytes;
}

// Utility class to temporarily nul-terminate a string, restoring
// the nulled out byte to its prior value on destruction
struct NulTerminator {
  NulTerminator(char* str, UInt32 len) {
    mSavePtr = str + len;
    mSaveVal = *mSavePtr;
    *mSavePtr = '\0';
  }
  ~NulTerminator() {
    *mSavePtr = mSaveVal;
  }

  char mSaveVal;
  char* mSavePtr;
};

// Get a line of text from the file, including the delimiter,
// if there is one.  Note that we look at LARGE_TOKEN_SIZE buffers,
// but that is not a limitation on the line-length we can handle.
// We just break it up like that to fit nicely into our input
// buffer strategy
bool UtIStream::getline(UtString* str, char delim) {
  str->clear();

  UInt32 numChars;
  char* buf;
  bool foundNewline = false;
  bool moreBytes = true;

  while (!foundNewline && moreBytes) {
    moreBytes = peakBuffer(LARGE_TOKEN_SIZE, &buf, &numChars, false);
    if (moreBytes) {
      NulTerminator nulTerm(buf, numChars); // do not look past numChars...
      char* dp = strchr(buf, delim);
      if (dp == NULL) {
        // Maybe the line was more than LARGE_TOKEN_SIZE chars.
      }
      else {
        // We found the newline
        numChars = dp - buf + 1;
        foundNewline = true;

        // If we are stripping CRs from the end of line, and we found
        // one, get rid of it -- let the file-reading code think there
        // is only a \n
        if ((delim == '\n') && (numChars > 1) && (buf[numChars - 2] == '\r')) {
          buf[numChars - 2] = '\n';
          --numChars;
        }
      }
      str->append(buf, numChars);
      consumeBuffer(numChars);
    }
  }        
  return (!str->empty());
}

bool UtIFileStream::getline(UtString* str, char delim) {
  str->clear();
  INFO_ASSERT(delim == '\n', "Only newline supported for delimiter specification..");        // since I use fgets I can't do anything else
  const size_t cBufsize = BUFSIZ;
  char buf[cBufsize];

  while(fgets(buf, cBufsize, mFile) != NULL) 
  {
    INFO_ASSERT(*buf != '\0', "Buffer overrun.");
    str->append(buf);
    if (*str->rbegin() == delim)
      return true;
  }

  if (str->empty()) {
    mEOF = true;
    return false;                 // EOF
  }

  return true;
}


void UtIStream::reportError(const char* errmsg)
{
  if (mErrmsg == NULL)
    mErrmsg = new UtString(errmsg);
  else
    *mErrmsg << "\n" << errmsg;
  mFailure = true;
}

bool UtIStream::bad() const {
  return !is_open() || (mErrmsg != NULL);
}

bool UtIStream::eof() const {
  return mEOF && (mUngetBuffers == NULL) &&
    (mFileBuf->getReadIndex() == mFileBuf->getWriteIndex());
}

bool UtIStream::fail() const {
  return mFailure;
}

const char* UtIStream::getFilename() const
{
  return NULL;
}

const char* UtIStream::getErrmsg()
{
  if (mErrmsg == NULL)
    return NULL;
  if (mPrevErrmsg != NULL)
    delete mPrevErrmsg;
  mPrevErrmsg = mErrmsg;
  mErrmsg = NULL;
  mFailure = false;
  return mPrevErrmsg->c_str();
}



UtIStream& UtIStream::operator>>(DynBitVector& bv) {
  (void) readDynBitVec(&bv, false);
  return *this;
}

// Parse a dynamic bitvector using the current base (mRadix) and field-width
// (mWidth).  Note that respecting the field-width is an aspect where the
// Carbon implementation of istreams differs from the standard.  If sizeToToken
// is false, the current width of the DynBitVector is respected.  Otherwise
// it is resized based on the parsed field-width and current radix.  E.g.
// in Octal, 217 requires 3*8=24 bits.
bool UtIStream::readDynBitVec(DynBitVector* bv, bool sizeToToken) {
  bv->reset();

  bool* legalChars = NULL;
  UInt32 numBits = 0;
  bool neg = false;
  switch (mRadix)
  {
  case UtIO::dec:  
  case UtIO::sdec: {
    legalChars = sLegalDecChars;
    char* sign;
    UInt32 numChars;
    if (peakBuffer(1, &sign, &numChars)) {
      if (*sign == '-') {
        neg = true;
        consumeBuffer(1);
      }
      else if (*sign == '+') {
        consumeBuffer(1);
      }
    }
    break;
  }
  case UtIO::HEX:
  case UtIO::hex:  legalChars = sLegalHexChars; numBits = 4; break;
  case UtIO::oct:  legalChars = sLegalOctChars; numBits = 3; break;
  case UtIO::bin:  legalChars = sLegalBinChars; numBits = 1; break;
  }
  INFO_ASSERT(legalChars, "Unhandled radix.");

  UtString digits;
  if (readDelimBuffer(&digits, legalChars)) {
    if (sizeToToken) {
      if (numBits == 0) {
        // For decimal, assume 4 bits per char
        bv->resize(digits.size() * 4);
      }
      else {
        bv->resize(digits.size() * numBits);
      }
    }

    if (numBits == 0) {       // decimal or signed-decimal
      // For decimal, it's easier to walk forward throught the string,
      // seeing the most significant bits first and multiplying
      // by 10.
      for (const char* c = digits.c_str(); *c != '\0'; ++c) {
        bv->multBy10();
        int digit = *c - '0';
        (*bv) += digit;
      }
      if (neg) {
        bv->negate();
      }
    }
    else {
      UInt32 bit = 0;
      for (int i = digits.size() - 1; (i >= 0) && bit < bv->size(); --i) {
        char c = digits[i];
        int digit = c - '0';

        if (isalpha(c)) {
          digit = tolower(c) - 'a' + 10;
        }
        for (UInt32 b = 0; (b < numBits) && bit < bv->size(); ++b, ++bit) {
          if ((digit & (1 << b)) != 0) {
            bv->set(bit);
          }
        }
      }
    }
  } // if

  return !mFailure;
} // bool UtIStream::readDynBitVec

bool UtIStream::readBitVec(UInt32* val, UInt32 numBits) {
  DynBitVector bv(numBits);
  bool ret = readDynBitVec(&bv, false);
  MEMCPY(val, bv.getUIntArray(), 4*bv.getUIntArraySize());
  return ret;
}

// Read a delimited buffer, based on the passed-in delimSet (see
// sInitCharSet above) and the current field with (mWidth).
bool UtIStream::readDelimBuffer(UtString* s, const bool* delimSet) {
  s->clear();

  UInt32 numChars, peakWidth = LARGE_TOKEN_SIZE;
  if ((mWidth != 0) && (mWidth < LARGE_TOKEN_SIZE)) {
    peakWidth = mWidth;
  }
  char* buf;
  bool done = false;
  bool skipWhitespace = true;

  // Even if the user specifies a very large width, don't read
  // it all in at once -- limit ourselves to LARGE_TOKEN_SIZE
  // because we can't exceed the buffer size
  while (!done && peakBuffer(peakWidth, &buf, &numChars, skipWhitespace)) {
    skipWhitespace = false;
    char* p = buf;
    for (char* e = buf + numChars; !done && (p != e); ++p) {
      done = !delimSet[static_cast<int>(*p)];
    }
    UInt32 consumed = p - buf;
    if (done) {
      --consumed;               // do not include the trailing space
    }
    s->append(buf, consumed);
    consumeBuffer(consumed);
    if (mWidth != 0) {
      INFO_ASSERT(consumed <= mWidth, "Buffer overrun.");
      mWidth -= consumed;
      if (mWidth < LARGE_TOKEN_SIZE) {
        peakWidth = mWidth;
      }
      if (mWidth == 0) {
        done = true;
      }
    }
  }
  if (s->empty()) {
    reportError("No more tokens");
    mFailure = true;
  }
  mWidth = 0;
  return !mFailure;
} // bool UtIStream::readDelimBuffer

UtIStream& UtIStream::operator>>(UtString& s) {
  if (mSlashify) {
    char* buf;
    UInt32 numChars;
    bool ok = false;
    if (peakBuffer(LARGE_TOKEN_SIZE, &buf, &numChars)) {
      char save = buf[numChars];
      buf[numChars] = '\0';
      UtShellTok tok(buf, false); // no env vars

      // If the end of this buffer split a token, then we need to grab
      // more characters and feed them to the tokenizer until the
      // tokenizer finds a delimiter, or we run out of chars for real.
      bool cont = true;
      ok = true;
      while (tok.eof() && cont) {
        buf[numChars] = save;      
        consumeBuffer(tok.numConsumed());
        cont = peakBuffer(LARGE_TOKEN_SIZE, &buf, &numChars);
        if (cont) {
          save = buf[numChars];
          buf[numChars] = '\0';
          tok.parseString(buf);
        }
        else {
          // If the end of the stream occurs without a formal termination
          // (e.g. closing quote or whitespace), consider that a 'pass'
          ok = **tok != '\0';
        }
      }
      s = *tok;
      consumeBuffer(tok.numConsumed());
      buf[numChars] = save;
    } // if

    // Note that when tokenizing, it is necessary to have a trailing
    // delimeter.  It is legal to tokenize an empty string, in which
    // case the returned string will not grow, but the tokenizer will
    // report success.  However, if you pass a buffer of "abc", the end
    // of token is not seen and we will report a failure.  A trailing
    // newline or space would do it.  Also, "\"abc\"" works fine.
    if (!ok) {
      reportError("No more tokens");
      mFailure = true;
    }
  } // if
  else {
    readDelimBuffer(&s, sNonSpaceChars);
  }
  return *this;
}

// Skip whitespace and read a whitespace-delimited token, compare
// it to expectedToken.  If it's different, set the current error
// message and indicate failure status.
UtIStream& UtIStream::operator>>(const char* expectedToken) {
  UtString buf;
  *this >> buf;
  if (!mFailure) {
    if (strcmp(buf.c_str(), expectedToken) != 0) {
      UtString errmsg;
      errmsg << "Expected token `" << expectedToken << "', received `"
             << buf << "'";
      reportError(errmsg.c_str());
      mFailure = true;
    }
  }
  return *this;
}


//! convert a string into an integer with the specified radix
/*!
  \param T                  the C++ type of the number
  \param str                the input string
  \param numChars           number of characters in the input string
  \param radix              input radix
  \param reqdWidth          the number of characters required.Ignore if 0
  \param numCharsConsumed   writes the number of characters that were parsed
  \param errmsg             string to hold an error message
  \param isDouble           is T a double?
 */
template <typename T>
static bool convStrToNum(char* str, UInt32 numChars, UtIO::Radix radix,
                         UInt32 fieldWidth, UInt32 reqdWidth,
                         UInt32* numCharsConsumed,
                         T* num, UtString* errmsg,
                         bool isDouble = false)
{
  if (fieldWidth == 0) {
    fieldWidth = reqdWidth ? ((numChars < reqdWidth) ? numChars : reqdWidth)
                             :numChars;
  }
  NulTerminator nulTerm(str, fieldWidth);
  char* endptr = NULL;

  if (isDouble) {
    // Here we cast the result to a double to avoid a compiler
    // warning, but if 'T' is not 'double', then isDouble will
    // be false and this call will be optimized away.
    *num = (T) OSStrToD(str, &endptr, errmsg);
  }
  else if (radix == UtIO::sdec) {
    if (sizeof(T) < 8)
      *num = OSStrToS32(str, &endptr, 10, errmsg);
    else
      *num = (T) OSStrToS64(str, &endptr, 10, errmsg);
  }

  else {
    int base = 0;
    switch (radix) {
    case UtIO::hex:
    case UtIO::HEX:
      base = 16;
      break;
    case UtIO::oct:
      base = 8;
      break;
    case UtIO::bin:
      base = 2;
      break;
    case UtIO::sdec:
    case UtIO::dec:
      base = 10;
      break;
    }
      
    if (sizeof(T) < 8)
      *num = OSStrToU32(str, &endptr, base, errmsg);
    else
      *num = (T) OSStrToU64(str, &endptr, base, errmsg);
  } // else

  if ((endptr != NULL) && (endptr != str)) {
    *numCharsConsumed = endptr - str;
    if (reqdWidth && (reqdWidth != *numCharsConsumed)) {
      UtIO::cout() << "Error: Wrong number of characters in input vector. Expected " << reqdWidth << ", got " << *numCharsConsumed << UtIO::endl; 
      return false;
    }
    return true;
  }
  return false;
} // static bool convStrToNum

UtIStream& UtIStream::operator>>(UInt64& num) {
  UInt32 numConsumed = 0;
  UInt32 numChars;
  UInt32 peakWidth = (mWidth == 0)? LARGE_TOKEN_SIZE: mWidth;
  char* buf;
  if (peakBuffer(peakWidth, &buf, &numChars) &&
      convStrToNum<UInt64>(buf, numChars, mRadix, mWidth, mMaxWidth,
                           &numConsumed, &num, &mOSErrmsg))
  {
    consumeBuffer(numConsumed);
  }
  else {
    mFailure = true;
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  mWidth = 0;
  return *this;
}

UtIStream& UtIStream::operator>>(SInt64& num) {
  UInt32 numConsumed = 0;
  UInt32 numChars;
  UInt32 peakWidth = (mWidth == 0)? LARGE_TOKEN_SIZE: mWidth;
  char* buf;
  if (peakBuffer(peakWidth, &buf, &numChars) &&
      convStrToNum<SInt64>(buf, numChars, mSignedRadix, mWidth, mMaxWidth,
                           &numConsumed, &num, &mOSErrmsg))
  {
    consumeBuffer(numConsumed);
  }
  else {
    mFailure = true;
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  mWidth = 0;
  return *this;
}

UtIStream& UtIStream::operator>>(SInt32& num) {
  UInt32 numConsumed = 0;
  UInt32 numChars;
  UInt32 peakWidth = (mWidth == 0)? SMALL_TOKEN_SIZE: mWidth;
  char* buf;
  if (peakBuffer(peakWidth, &buf, &numChars) &&
      convStrToNum<SInt32>(buf, numChars, mSignedRadix, mWidth, mMaxWidth,
                           &numConsumed, &num, &mOSErrmsg))
  {
    consumeBuffer(numConsumed);
  }
  else {
    mFailure = true;
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  mWidth = 0;
  return *this;
}

UtIStream& UtIStream::operator>>(UInt32& num) {
  UInt32 numConsumed = 0, numChars;
  UInt32 peakWidth = (mWidth == 0)? SMALL_TOKEN_SIZE: mWidth;
  char* buf;
  if (peakBuffer(peakWidth, &buf, &numChars) &&
      convStrToNum<UInt32>(buf, numChars, mRadix, mWidth, mMaxWidth,
                           &numConsumed, &num, &mOSErrmsg))
  {
    consumeBuffer(numConsumed);
  }
  else {
    mFailure = true;
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  mWidth = 0;
  return *this;
} // UtIStream& UtIStream::operator>>


template<typename T>
void sRangeErr(UtString* errMsg, T val, T minVal, T maxVal) {
  *errMsg << "Converted value `" << val 
          << "' is out of range [" << minVal << "," << maxVal << "]";
}

UtIStream& UtIStream::operator>>(UInt16& num) {
  UInt32 tmp;
  *this >> tmp;
  if (tmp > 0xffff) {
    sRangeErr(&mOSErrmsg, tmp, (UInt32) 0, (UInt32) 0xffff);
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  num = tmp;
  return *this;
}

UtIStream& UtIStream::operator>>(SInt16& num) {
  SInt32 tmp;
  *this >> tmp;
  if ((-32768 > tmp) || (tmp > 32767)) {
    sRangeErr(&mOSErrmsg, tmp, -32768, 32767);
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  num = tmp;
  return *this;
}

UtIStream& UtIStream::operator>>(UInt8& num) {
  UInt32 tmp;
  *this >> tmp;
  if (tmp > 0xff) {
    sRangeErr(&mOSErrmsg, tmp, (UInt32) 0, (UInt32) 0xff);
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  num = tmp;
  return *this;
}


UtIStream& UtIStream::operator>>(double& num) {
  UInt32 numConsumed = 0;
  UInt32 numChars = 0;
  UInt32 peakWidth = (mWidth == 0)? LARGE_TOKEN_SIZE: mWidth;
  char* buf;
  if (peakBuffer(peakWidth, &buf, &numChars) &&
      convStrToNum<double>(buf, numChars, mRadix, mWidth, mMaxWidth,
                           &numConsumed, &num, &mOSErrmsg, true))
  {
    consumeBuffer(numConsumed);
  }
  else {
    mFailure = true;
    reportError(mOSErrmsg.c_str());
    mOSErrmsg.clear();
  }
  mWidth = 0;
  return *this;
}
UtIStream& UtIStream::operator>>(char& ch) {
  if (read(&ch, 1) < 1) {
    reportError("EOF reading char");
    mFailure = true;
  }
  return *this;
}
UtIStream& UtIStream::operator>>(void*& ptr) {
  UIntPtr num;
  *this >> num;
  ptr = (void*) num;
  return *this;
}

#undef LONG_BIT
#define LONG_BIT  (UInt32)(CHAR_BIT * sizeof (UInt32))


UtIStream& UtIStream::operator>>(const BVref<false>& v)
{
  UInt32 size = v._M_bsiz;
  UInt32 offset = v._M_bpos;

  // Read the stream into a DynBitVector
  DynBitVector bv(size);
  readDynBitVec(&bv, false);

  // Now copy the DynBitVector bits into the BVref. 
  // Resize the DynBitVector to replicate the number of words in BVref.
  bv.resize(size+offset);
  // Shift in the offset to replicate the bit-pattern of BVref.
  bv <<= offset;
  // Copy the IntArray into Bvref.
  MEMCPY(v._M_wp, bv.getUIntArray(), 4*bv.getUIntArraySize());
  return *this;
}
#undef LONG_BIT

UtIStream& UtIStream::operator>>(UtIO::Radix radix) {
  mRadix = radix;
  mSignedRadix = (radix == UtIO::dec)? UtIO::sdec: radix;
  return *this;
}
UtIStream& UtIStream::operator>>(UtIO::FloatMode mode) {
  mFloatMode = mode;
  return *this;
}
UtIStream& UtIStream::operator>>(UtIO::Width w) {
  mWidth = w.mWidth;
  return *this;
}
UtIStream& UtIStream::operator>>(UtIO::MaxWidth w) {
  mMaxWidth = w.mMaxWidth;
  return *this;
}
UtIStream& UtIStream::operator>>(UtIO::Precision p) {
  mPrecision = p.mPrecision;
  return *this;
}
UtIStream& UtIStream::operator>>(UtIO::Fill f) {
  INFO_ASSERT(f.mFillChar != 0, "Uninitialized Fill.");
  mFillChar = f.mFillChar;
  return *this;
}
UtIStream& UtIStream::operator>>(UtIO::FieldMode mode) {
  mField = mode;
  return *this;
}
UtIStream& UtIStream::operator>>(UtIO::Slashification slash) {
  mQuotify = (slash == UtIO::quotify);
  mSlashify = (slash == UtIO::slashify) || mQuotify;
  return *this;
}

FILE* UtIFileStream::getFile() {return mFile;}
bool UtIFileStream::is_open() const {
  return (mFile != NULL);
}
bool UtIFileStream::close() {
  INFO_ASSERT(mFile != NULL, "File already closed.");
  bool ret = true;
  if (fclose(mFile) != 0)
  {
    UtString errBuf;
    reportError(OSGetLastErrmsg(&errBuf));
    ret = false;
  }
  mFile = NULL;
  return ret;
}

UtIFileStream::UtIFileStream(FILE* f): mFile(f)
{
}

UtIFileStream::UtIFileStream() : mFile(NULL)
{
}

UtIFileStream::~UtIFileStream() {
}

UtIFStream::UtIFStream(const char* file) : UtIFileStream()
{
  UtString reason;
  mFile = OSFOpen(file, "r", &reason);
  if (mFile != NULL) {
    // the file was opened successfully
  } else if (reason.size () == 0) {
    reportError ("file open failed");
  } else {
    reportError (reason.c_str ());
  }
}

UtIFStream::~UtIFStream()
{
  if (is_open())
    close();
}

UtIZStream::UtIZStream(const char* fileName, void* key)
  : mZFile(fileName, key)
{}

UtIZStream::~UtIZStream()
{
  if (is_open())
    close();
}

bool UtIZStream::is_open() const
{
  return mZFile.is_open();
}

bool UtIZStream::close()
{
  return mZFile.close();
}

bool UtIZStream::bad() const
{
  return mZFile.fail();
}

UInt32 UtIZStream::readFromFile(char* buf, UInt32 size)
{
  UInt32 ret = 0;
  if (! mEOF) {
    ret = mZFile.read(buf, size);
    if (ret < size) {
      if (ret == 0) {
        reportError(mZFile.getError());
      }
      mEOF = true;
    }
  }
  return ret;
}

UtIStringStream::UtIStringStream(const char* str) {
  init(str, strlen(str));
}

UtIStringStream::UtIStringStream(const UtString& str) {
  init(str.c_str(), str.size());
}

UtIStringStream::UtIStringStream(const char* str, size_t len) {
  init(str, len);
}

UtIStringStream::UtIStringStream() {
}

void UtIStringStream::str(const char* str) {
  init(str, strlen(str));
}

void UtIStringStream::init(const char* str, size_t len) {
  reset();
  mInputBuffer = str;
  mInputBufferSize = len;
  mStringPos = 0;
}

UtIStringStream::~UtIStringStream() {
}

UInt32 UtIStringStream::readFromFile(char* buf, UInt32 size) {
  UInt32 num = 0;
  if (mStringPos < mInputBufferSize) {
    num = mInputBufferSize - mStringPos;
    if (size < num) {
      num = size;
    }
    memcpy(buf, mInputBuffer + mStringPos, num);
    mStringPos += num;
    mEOF = (mStringPos == mInputBufferSize);
  }
  return num;
}

//! True if the file/buffer is open
bool UtIStringStream::is_open() const
{
  return true;
}

//! Close the file/buffer
bool UtIStringStream::close()
{
  return true;
}

static UtIStream* cinStream = NULL;

static inline UtIStream* getStream(UtIStream** streamptr, FILE* f)
{
  MutexWrapper mutex(&sStreamMutex);
  if (*streamptr == NULL)
  {
    // this will be deleted by UtIO::memCleanup
    *streamptr = new UtIFileStream(f);
  }
  return *streamptr;
}

UtIStream& UtIO::cin()
{
  return *getStream(&cinStream, stdin);
}

void  UtIO::memCleanupIStream(){
  MutexWrapper mutex(&sStreamMutex);
  if ( cinStream != NULL ){
    delete cinStream;
    cinStream = NULL;
  }
}


// Buffered stream
UtIBStream::UtIBStream(const char* filename, UInt32 bufSize)
  : UtIStream(bufSize),
    mFilename(filename)
{
  INFO_ASSERT(bufSize > 0, filename);
  INFO_ASSERT(bufSize < 100*1024*1024, filename); // 100Meg is too big - sanity check

  mFD = -1;
  (void) open(true);
  mPhysicalPos = 0;
}

bool UtIBStream::save(UtOCheckpointStream &out)
{
  out << mFilename.c_str();
  out << mPos;  // NOTE: This is the inherited position from UtIStream.
                //       mPhysicalPos cannot be used because it includes
                //       buffer chars not yet consumed.

  return !out.fail();
}

UtIBStream::UtIBStream(UtICheckpointStream &in)
{
  restore(in);
}

bool UtIBStream::restore(UtICheckpointStream &in)
{
  UtString errorMessage;

  mFD = -1;

  in >> mFilename;
  in >> mPos;  // NOTE: This is the inherited position from UtIStream.
               //       mPhysicalPos cannot be used because it includes
               //       buffer chars not yet consumed.

  // open the file
  int flags = O_RDONLY | O_BINARY;
  mFD = OSSysOpen(mFilename.c_str(), flags, 
                  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
                  &errorMessage);
  if (!is_open()) {
    reportError(errorMessage.c_str());
    mPos = 0;
  }
  else {
    // try to seek to the previous offset
    if (OSSysSeek(mFD, mPos, SEEK_SET, &errorMessage) == -1) {
      // if we can't go to where we were, just position at EOF.
      OSSysSeek(mFD, 0, SEEK_END, &errorMessage);
    }
  
    // set offset to where we actually are in the file
    SInt64 offset = OSSysTell(mFD, &errorMessage);
    if (offset == -1) {
      reportError(errorMessage.c_str());
    }
    else {
      mPos = offset;
    }
  }
  mPhysicalPos = mPos;
  return (mFD != -1);
}

bool UtIBStream::open(bool /*firstTime*/) {
  INFO_ASSERT(mFD == -1, mFilename.c_str());
  int flags = O_RDONLY | O_BINARY;
  UtString errmsg;
  mFD = OSSysOpen(mFilename.c_str(), flags, 
                  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
                  &errmsg);
  if (! is_open())
    reportError(errmsg.c_str());
  return UtIBStream::is_open();
}

UtIBStream::~UtIBStream()
{
  if (is_open())
  {
    (void) close();
  }
}

bool UtIBStream::is_open() const
{
  return mFD != -1;             // note: UtOCStream overrides
}

bool UtIBStream::close()
{
  INFO_ASSERT(is_open(), mFilename.c_str());
  UtString errmsg;
  bool ret = true;
  if (OSSysClose(mFD, &errmsg) != 0)
  {
    reportError(errmsg.c_str());
    ret = false;
  }
  mFD = -1;
  return ret;
}

bool UtIBStream::getFD()
{
  return (mFD != -1);
}

UInt32 UtIStream::read(char* buf, UInt32 size)
{
  UInt32 totalNumRead = 0;
  while (true) {
    // First try to satisfy the request from the buffer
    UInt32 rdSize;
    char* rdbuf = mFileBuf->getBufferRemaining(&rdSize);
    UInt32 numRead = std::min(size, rdSize);
    if (numRead != 0) {
      MEMCPY(buf, rdbuf, numRead);
      mFileBuf->consume(numRead);
      size -= numRead;
      buf += numRead;
      totalNumRead += numRead;
    }

    if (size == 0) {
      break;
    }

    // If we need to get more from the physical file, then clear
    // out the buffer (cause it must be fully consumed) and read
    // in another chunk
    mFileBuf->reset();
    rdbuf = mFileBuf->getBufferRemaining(&rdSize);
    UInt32 sysRead = readBytes(rdbuf, mFileBuf->size());
    if (sysRead != 0) {
      mFileBuf->putWriteIndex(sysRead);
    }
    else {
      break;
    }
  } // while
  mPos += totalNumRead;
  return totalNumRead;
} // UInt32 UtIStream::read

UInt32 UtIStream::readChar(char* buf) {
  ++mPos;

  // First try to satisfy the request from the buffer
  if (mFileBuf->readBuffer(buf, 1) == 1) {
    return 1;
  }
  
  UInt32 rdSize;
  mFileBuf->reset();
  char* rdbuf = mFileBuf->getBufferRemaining(&rdSize);
  UInt32 sysRead = readBytes(rdbuf, mFileBuf->size());
  mFileBuf->putWriteIndex(sysRead);
  if (sysRead == 0) {
    --mPos;
    return 0;
  }
  mFileBuf->readBuffer(buf, 1);
  return 1;
}

UInt32 UtIStream::readUInt32(UInt32* buf) {
  mPos += 4;

  // First try to satisfy the request from the buffer
  UInt32 bytesSoFar = mFileBuf->readBuffer((char*) buf, 4);
  UInt32 needBytes = 4 - bytesSoFar;
  if (needBytes == 0) {
    return 4;
  }

  UInt32 rdSize;
  mFileBuf->reset();
  char* rdbuf = mFileBuf->getBufferRemaining(&rdSize);
  UInt32 sysRead = readBytes(rdbuf, mFileBuf->size());
  mFileBuf->putWriteIndex(sysRead);
  if (sysRead < needBytes) {
    mPos -= needBytes;
    return 4 - needBytes;
  }

  char* cbuf = ((char*) buf) + bytesSoFar;
  mFileBuf->readBuffer(cbuf, needBytes);
  return 4;
}

UtIStream::operator void*() const
{
  return bad() ? (void*) 0 : (void*)(-1);
}

const char * UtIBStream::getFilename() const
{
  return mFilename.c_str();
}

// common code used by constructors for initialization
void UtICStream::initialize(UtCachedFileSystem* sys)
{
  if (UtIBStream::is_open())
  {
    mFileCache = sys;
    mFileCache->maybeRelease();   // make way for a new file descriptor
    mFileCache->registerFile(this);
    mFileCache->activate(this);
    mTimestamp = mFileCache->bumpTimestamp();
  }
  else
    mFileCache = NULL;
}

// Cached file stream -- shares file descriptors within a system so
// that there are no OS-imposed limits on effective number of open streams.
UtICStream::UtICStream(const char* filename, UtCachedFileSystem* sys,
                       UInt32 bufsiz)
  : UtIBStream(filename, bufsiz)
{
  initialize(sys);
}

UtICStream::UtICStream(UtICheckpointStream &in, UtCachedFileSystem* sys)
  : UtIBStream(in)
{
  initialize(sys);
}

UtICStream::~UtICStream()
{
  if (is_open())
  {
    (void) close();
    // note -- cannot rely on UtIBStream's destructor because the
    // UtOCstream parts have already been destroyed by the time
    // the base-class destructor is called
  }
}

bool UtICStream::close()
{
  bool ret = true;
  mFileCache->unregisterFile(this);
  if (UtIBStream::is_open())
    ret &= UtIBStream::close();
  mFileCache = NULL;
  return ret;
}

bool UtICStream::is_open() const
{
  return (mFileCache != NULL);
}

bool UtICStream::getFD()
{
  bool ret = true;
  
  if (mFD == -1)
  {
    INFO_ASSERT(mFileCache, "File cache not initialized.");
    mFileCache->maybeRelease();        // make way for a new file descriptor
    if (open(false)) {
      if (lseek(mFD, mPhysicalPos, SEEK_SET) < 0) { // handle EINTR?
        UtString errBuf;
        reportError(OSGetLastErrmsg(&errBuf));
        ret = false;
      }
      mFileCache->activate(this);      // declare we have grabbed an FD
    }
    else
      ret = false;
  }
  mTimestamp = mFileCache->bumpTimestamp();
  return ret;
} // bool UtICStream::getFD

void UtICStream::releaseFD()
{
  INFO_ASSERT(mFileCache, "File cache not initialized");
  INFO_ASSERT(mFD != -1, "File not open.");
  UtString errmsg;
  if (OSSysClose(mFD, &errmsg) != 0)
    reportError(errmsg.c_str());
  mFD = -1;                     // Note we do not need to flush!
}

bool UtICStream::hasActiveFD() const
{
  return UtIBStream::is_open();
}

int UtIO::seekModeToInt(SeekMode mode) {
  switch (mode) {
  case UtIO::seek_cur:    return SEEK_CUR;
  case UtIO::seek_set:    return SEEK_SET;
  case UtIO::seek_end:    return SEEK_END;
  }
  INFO_ASSERT(0, "invalid seek mode");
  return SEEK_CUR;
}

SInt64 UtIFileStream::seek(SInt64 pos, UtIO::SeekMode flag) {
  reset();
  mPos = (SInt64) fseek(mFile, pos, UtIO::seekModeToInt(flag));
  return mPos;
}

SInt64 UtIStringStream::seek(SInt64 pos, UtIO::SeekMode flag) {
  reset();
  switch (flag) {
  case UtIO::seek_cur:
    mPos += pos;
    break;
  case UtIO::seek_set:
    mPos = pos;
    break;
  case UtIO::seek_end:
    mPos = mInputBufferSize + pos;
    break;
  }

  mEOF = false;
  if (mPos < 0) {
    mPos = 0;
  }
  else if (mPos >= (SInt64) mInputBufferSize) {
    mPos = mInputBufferSize;
    mEOF = true;
  }
  mStringPos = (size_t) mPos;
  return mPos;
} // virtual SInt64 UtIStringStream::seek

SInt64 UtIBStream::seek(SInt64 pos, UtIO::SeekMode flag) {
  reset();
  mPos = (SInt64) lseek(mFD, pos, UtIO::seekModeToInt(flag));
  return mPos;
}

SInt64 UtIZStream::seek(SInt64, UtIO::SeekMode) {
  INFO_ASSERT(0, "Cannot seek in a zstream");
  return -1;
}
