// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include "util/OSWrapper.h"
#include "util/c_memmanager.h"
#include "util/CarbonTypes.h"
#include "util/ArgProc.h"
#include "util/CodeStream.h"
#include "util/MemManager.h"
#include "util/OSWrapper.h"
#include "util/OSWrapper.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtIZStream.h"
#include "util/UtOZStream.h"
#include "util/UtVector.h"
#include "util/DynBitVector.h"

/******************
 * CodeAnnotation *
 ******************/

//! Write the annotation as a comment to a stream
void CodeAnnotation::write (UtOStream &s) const
{
  if (mManager == NULL) {
    return;                             // do nothing...
  } 
  if (test (cVISIBLE|cLEADSLINE) && !s.isStartOfLine ()) {
    // the annotation must start the line
    s << "\n";
  }
  if (!(mFlags & cVISIBLE)) {
    // output nothing
  } else {
    // output the annotation flavour prefix and the domain
    s << "/*" << mManager->getPrefix () << mManager->stripName (mDomain.mFile)
      << ":" << mDomain.mLine << "*/";
  }
  if (test (cVISIBLE|cNEWLINE)) {
    // a newline is required after the annotation
    s << "\n";
  }
}

/**************
 * CodeStream *
 **************/

//! ctor for CodeStream
/*! \param filename name of the output file - this must be the name of sink
 *  \param sink the wrapped UtOStream - must be opened by the instantiator of this
 *  \param implementation_manager manager for implementation annotations
 *  \param hdl_manager manager for HDL source annotations
 */
CodeStream::CodeStream (
  const char *filename,
  CodeAnnotationManager &implementation_manager,
  CodeAnnotationManager &hdl_manager, 
  UtOStream *sink,
  InducedFaultMap *faults,
  const UInt32 flags) : 
  UtOStream (), 
  mImplAnnotations (implementation_manager), 
  mHDLAnnotations (hdl_manager), mInducedFaults (faults),
  mSink (sink), mFlags (flags), mFilename (filename), mLine (1), mColumn (1), 
  mIndent (0), mNeedHashLine (mFlags & cHASH_LINES)
{
  if (mInducedFaults != NULL && mSink != NULL) {
    // write the prepended lines from the induced fault map to the stream
    UtList <UtString>::const_iterator it;
    for (it = mInducedFaults->begin_prepended (); it != mInducedFaults->end_prepended (); ++it) {
      *mSink << it->c_str () << "\n";
      mLine++;
    }
  }
}

/*virtual*/ CodeStream::~CodeStream ()
{
  if (mSink != NULL) {
    close ();
  }
}

/*virtual*/ SimpleCodeStream::~SimpleCodeStream () 
{ 
  if (mFlags & cDO_NOT_CLOSE) {
    // The creator specified that the stream is not to be closed.
    mSink->flush ();
  } else {
    delete mSink;
  }
}

//! Implement superclass is_open predicate
/*virtual*/ bool CodeStream::is_open () const 
{ 
  return mSink->is_open (); 
}

//! Implement superclass close method
/*virtual*/ bool CodeStream::close ()
{
  if (mInducedFaults != NULL && mSink != NULL) {
    UtList <UtString>::const_iterator it;
    for (it = mInducedFaults->begin_appended (); it != mInducedFaults->end_appended (); ++it) {
      *mSink << it->c_str () << "\n";
    }
  }
  if (mFlags & cDO_NOT_CLOSE) {
    // this does not own the stream so must neither delete nor close...
    return true;
  } else if (mSink != NULL) {
    flush ();
    bool ret = mSink->close ();
    delete mSink;
    return ret;
  } else {
    return false;
  }
}

//! \return iff a fault had been specified for the given line of the file
/*! \note If a fault is specified then it is return in faul
 */
bool CodeStream::InducedFaultMap::isFaulted (const UInt32 line, UtString *fault) const
{
  const_iterator found;
  if ((found = find (line)) == end ()) {
    return false;
  } else {
    *fault = found->second;
    return true;
  }

}
void CodeStream::InducedFaultMap::print (UtOStream &s) const
{
  for (const_iterator it = begin (); it != end (); ++it) {
    UInt32 lineno = it->first;
    UtString fault = it->second;
    s << lineno << ": " << fault << "\n";
  }
}

void CodeStream::InducedFaultMap::pr () const
{
  print (UtIO::cout ());
}

//! Write an annotation to the output stream
void CodeStream::write (const CodeAnnotation &x)
{
  // set the range of the annotation
  x.set (CodeAnnotation::Range (mFilename.c_str (), mLine, getColumn ()));
  // bind the annotation to the appropriate manager
  if (x.test (CodeAnnotation::cHDL_SOURCE)) {
    mHDLAnnotations.bind (x);
  } else if (x.test (CodeAnnotation::cIMPL)) {
    mImplAnnotations.bind (x);
  }
  // now write it out
  x.write (*this);
}

//! Implement superclass flush method
/*virtual*/ bool CodeStream::flush()
{
  mSink->flush ();
  return true;
}

//! output an explicit #line directive
void CodeStream::line (const SourceLocator loc)
{
  if (mFlags & cNO_USER_HASH_LINES) {
    // #line directives are suppressed
  } else {
    UtString directive;
    if (!isStartOfLine ()) {
      write ("\n", 1);
    }
    directive << "#line " << loc.getLine () << " \"" << loc.getFile () << "\"\n";
    mSink->write (directive.c_str (), directive.length ());
    mLine++;
  }
}

//! output a line directive that points back to the current location in the output file
void CodeStream::line ()
{
  mNeedHashLine = true;
}

//! Tab to a given column number
void CodeStream::tab (const UInt32 column)
{
  while (mColumn < column) {
    char spaces [21] = "                    ";
    UInt32 delta = column - mColumn;
    if (delta < 20) {
      write (spaces, delta);
    } else {
      write (spaces, 20);
    }
  }
}

//! Write a buffer to the stream, returning false if an error is discovered
bool CodeStream::write (const char *buffer, UInt32 len)
{
  UInt32 m = 0;                         // number of characters from buffer that have been written
  UInt32 n = 0;                         // index into buffer
  UtString induced_fault;
  while (n < len) {
    if (mColumn == 1 && mInducedFaults != NULL && mInducedFaults->isFaulted (mLine, &induced_fault)) {
      // An induced fault has been specified for this line, so write it out and
      // increment the line counter.
      mSink->write (induced_fault.c_str (), induced_fault.length ());
      mSink->write ("\n", 1);
      mLine++;
    } else if (mColumn == 1 && mNeedHashLine) {
      // At the start of a line and we need output a #line directive.  First
      // output whatever is in the buffer before the need for the #line was
      // discovered.
      if (!mSink->write (buffer + m, n - m)) {
        // something bad happened in the underlying stream
        return false;
      }
      m = n;                            // written the first m characters now
      // now the #line
      mLine++;                          // #line denotes the number of the next line (see CPP docs)
      UtString directive;
      directive << "#line " << mLine << " \"" << mFilename << "\"\n";
      mSink->write (directive.c_str (), directive.length ());
      mNeedHashLine = false;            // it's written now
    } else if ((mFlags & cHASH_LINES)
      && mColumn == 1                   // start of a line
      && buffer [n] == '#'              // CPP directive
      && ((len - n > 7 && !strncmp (buffer + n, "#include", 7))
        || (len - n > 3 && !strncmp (buffer + n, "#if", 3))
        || (len - n > 5 && !strncmp (buffer + n, "#else", 5))
        || (len - n > 6 && !strncmp (buffer + n, "#endif", 6)))) {
      // This is the start of a line and it is a #include, or a conditional
      // compilation directive. Note that we need a #line directive on the next
      // line of output.
      mNeedHashLine = true;
      mColumn++;
      n++;
    } else if ((mFlags & cDO_INDENT) && mColumn == 1 && mIndent > 0 && buffer [n] != '\n') {
      // Output what is in the buffer up to the start of this line
      if (!mSink->write (buffer + m, n - m)) {
        return false;
      }
      m = n;
      // Output the indenting spaces
      char spaces [33] = "                                ";        
      UInt32 residue = mIndent;
      while (residue > 0) {
        if (residue <= 32 && mSink->write (spaces, residue)) {
          residue = 0;                  // wrote the last few characters
        } else if (residue > 32 && mSink->write (spaces, 32)) {
          residue -= 32;                // wrote another 32 spaces
        } else {
          return false;                 // the write failed
        }
      }          
      mColumn = mIndent + 1;
    } else if (buffer [n] == '\n') {
      // end of a line
      mLine++;                          // increment the line number counter
      mColumn = 1;                      // reset the column number
      n++;                              // consume the input character
    } else {
      // not the end of a line
      mColumn++;                        // increment the column counter
      n++;                              // consume the input character
    }
  }
  // After scanning the input buffer, we have written out m characters. So,
  // write out all the rest.
  return mSink->write (buffer + m, n - m);
}

//! Output statistics on the stream
/*virtual*/ void CodeStream::print (UtOStream *s) const
{
  *s << mFilename << ": " << mLine << ": " << mColumn << "\n";
}

// WARNING - This indent engine blows out CodeGen performance.

//! ctor for IndentingCodeStream
/*! \param filename name of the output file - this must be the name of sink
 *  \param sink the wrapped UtOStream - must be opened by the instantiator of this
 *  \param implementation_manager manager for implementation annotations
 *  \param hdl_manager manager for HDL source annotations
 */
IndentingCodeStream::IndentingCodeStream (
  const char *filename,
  CodeAnnotationManager &impl_manager,
  CodeAnnotationManager &hdl_manager, 
  UtOStream *sink,
  InducedFaultMap *faults) :
  CodeStream (filename, impl_manager, hdl_manager, sink, faults),
  mIndent (0), mIsFirst (true), mInComment (false), mInQuotes (false), 
  mDoNotIndentNextBrace (false), mNeedHashLine (true) , mBufferLength (0)
{}

//! Write a buffer to the stream, returning false if an error is discovered
/*! This write implementation tracks line and column information for
  annotations and will perform a limited amout of C++ prettification */
bool IndentingCodeStream::write (const char *buffer, UInt32 len)
{
  // A state machine walks through buffer looking for braces, keywords and
  // newlines
  int state = 0;
  UInt32 n = 0;
  int is_directive = 0;
  UtString induced_fault;
  while (state < 999) {
    // use 999 as the final state
    switch (state) {
    case 0:
      // initital state
      if (mIsFirst && mInducedFaults != NULL && mInducedFaults->isFaulted (mLine, &induced_fault)) {
        state = 20;
      } else if (mIsFirst && mNeedHashLine) {
        state = 10;
      } else if (n >= len) { 
        state = 999;                    // end of the string
      } else if (mIsFirst) {
        // we are writing at the start of a line
        state = 50;
      } else {
        // we are inside a line
        state = 100;
      }
      break;
    case 10:
      // Write out a #line directive
      {
        UtString directive;
        directive << "#line " << mLine << "\"" << mFilename << "\"\n";
        output (directive.c_str (), directive.length ());
      }
      mLine++;
      mIsFirst = 1;                   // at beginning of line next time
      mInComment = false;
      mNeedHashLine = false;
      state = 0;
      break;
    case 20:
      indent (mIndent);
      output (induced_fault.c_str (), induced_fault.length ());
      output ('\n');
      mLine++;
      state = 0;
      break;
    case 50:
      // at the start of the line so look for the next non-space character in
      // the input string.
      if (n >= len) {
        state = 0;                      // end of the string
      } else {
        switch (buffer [n]) {
        case ' ': case '\t':
          n++;                            // leading space
          break;
        default:
          state++;                        // non-space
          break;
        }
      }
      break;
    case 51:
      // at the start of an output line and just found the leading non-space
      // character in the input string
      is_directive = 0;
      switch (buffer [n]) {
      case '#':                         // CPP directive
        is_directive = 1;
        state = 100;                    // no indent
        if (!strncmp (buffer + n, "#include", 7)) {
          // It is an include directive so we need to output a new #line
          // directive before writing the next line
          mNeedHashLine = true;
        }
        break;
      case '%':
        state = 100;                    // used for %%, %{, %}, leave alone
        break;
      case '\n':
        mInComment = false;
        state = 100;                    // don't bother with the indent
        break;
      case '}':
        // leading with a close brace so indent by two less
        indent (mIndent - 2);
        state = 100;
        break;
      case 'n':
        if (!strncmp (buffer + n, "namespace", 8)) {
          mDoNotIndentNextBrace = true;
        }
        state = 100;
        break;
      case 'c':
      case 'd':
      case 'p':
        if (!strncmp (buffer + n, "public:", 7) 
          || !strncmp (buffer + n, "case", 4) 
          || !strncmp (buffer + n, "default:", 8)
          || !strncmp (buffer + n, "private:", 8)
          || !strncmp (buffer + n, "protected:", 10)) {
          indent (mIndent - 2);         // ditto
        } else  {
          indent (mIndent);             // indent by the normal amount
        }
        state = 100;
        break;        
      default:
        indent (mIndent);
        state = 100;
        break;
      }
      break;
    case 100:
      // start of the part of a line that is copied directly to output
      mIsFirst = 0;                    // done the begining of line stuff
      state++;
      break;
    case 101:
      // inside a line so copy all characters up the CR and track indent
      if (n >= len) {
        state = 0;
      } else {
        switch (buffer [n]) {
        case '"':
          mInQuotes = !mInQuotes;
          output (buffer [n++]);
          break;
        case '{':
          if (!mInQuotes && !mDoNotIndentNextBrace) {
            mIndent += 2;                 // increase the indent amount
          }
          mDoNotIndentNextBrace = false;
          output (buffer [n++]);          // output and consume
          break;
        case '}':
          if (!mInQuotes && mIndent > 1) {
            mIndent -= 2;                 // decrease the indent amount
          }
          output (buffer [n++]);          // output and consume
          break;
        case ' ': case '\t':
          // special white space processing
          if (mInComment || mInQuotes) {
            output (buffer [n++]);
          } else {
            state++;
          }
          break;
        case '\n':
          output (buffer [n++]);          // output and consume the new line
          mIsFirst = 1;                   // at beginning of line next time
          mInComment = false;
          mLine++;
          state = 0;
          break;
        case '/':
          output (buffer [n++]);
          state = 103;
          break;
        default:
          output (buffer [n++]);               // output and consume the character
          break;
        }
      }
      break;
    case 102:
      switch (buffer [n]) {
      case ' ': case '\t':
        n++;                            // fold into a single space
        break;
      case ')': case ']': case ';': case ':':
        state--;                        // no space before these
        break;
      default:
        output (' ');                   // single space
        state--;
        break;
      }
      break;
    case 103:
      if (buffer [n] == '/') {
        mInComment = true;
        output ('/');
        n++;
      }
      state = 101;
      break;
    default:
      ASSERT (0);
      return false;
      break;
    }
  }
  return flush ();
}

//! Write a string to the output buffer
void IndentingCodeStream::output (const char *s, const UInt32 length)
{
  UInt32 residue = length;
  UInt32 n = 0;
  while (residue > 0) {
    if (mBufferLength >= sizeof (mBuffer)) {
      flush ();
    }
    UInt32 delta;
    if (mBufferLength + residue > sizeof (mBuffer)) {
      // cannot fit the rest of the string into the buffer, so use all the remaining buffer
      delta = sizeof (mBuffer) - mBufferLength;
    } else {
      // just push everything into the buffer
      delta = residue;
    }
    memcpy (mBuffer + mBufferLength, s + n, delta);
    residue -= delta;
    n += delta;
    mBufferLength += delta;
  }
}

//! Write a character to the output buffer
void IndentingCodeStream::output (char c)
{
  if (c == '\n') {
    mColumn = 0;
  }
  if (mBufferLength >= sizeof (mBuffer)) {
    flush ();
  }
  mBuffer [mBufferLength++] = c;
  mColumn++;
}

//! Indent by n in the output buffer
void IndentingCodeStream::indent (const int n)
{
  int m = 0;
  while (m < n) {
    if (mBufferLength >= sizeof (mBuffer)) {
      flush ();
    }
    mBuffer [mBufferLength++] = ' ';
    m++;
    mColumn++;
  }
}

//! Implement superclass flush method
/*virtual*/ bool IndentingCodeStream::flush()
{
  if (mBufferLength == 0) {
    // Indentation engine buffer is empty so just flush the wrapped stream
    return mSink->flush ();
  } else if (!mSink->write (mBuffer, mBufferLength)) {
    // Something screwed up...
    return false;
  } else {
    // Flushed the indentation buffer so...
    mBufferLength = 0;                  // reset the length
    return mSink->flush ();             // and flush the wrapped stream
  }
}

//! Write a DynBitVector to the stream
CodeStream &operator << (CodeStream &out, const DynBitVector &value)
{
  UtString buffer;
  value.format (&buffer, eCarbonHex);
  out << "0x" << buffer;
  return out;
}

/*************************
 * CodeAnnotationManager *
 *************************/

//! ctor for CodeAnnotationManager
/*! \param flags option mask formed from enum CodeAnnotation::Flags bits
 *  \param carbon_context compiler context
 *  \param msg_context whining stream
 */
CodeAnnotationManager::CodeAnnotationManager (const UInt32 flags) : 
  mFlags (flags), mStore (CodeAnnotationStore::cWRITING, getName ())
{}

/*virtual*/ CodeAnnotationManager::~CodeAnnotationManager ()
{
}

//! Bind a manager and a range to an annotation.
void CodeAnnotationManager::bind (const CodeAnnotation &x)
{ 
  x.mFlags |= mFlags; 
  x.mManager = this;
  if (!(mFlags & CodeAnnotation::cNO_STORE)) {
    mStore.write (x);
  }
}

//! \return the prefix for visible comments in the generated code
const char *CodeAnnotationManager::getPrefix () const
{
  if (mFlags & CodeAnnotation::cHDL_SOURCE) {
    return "%H ";
  } else if (mFlags & CodeAnnotation::cIMPL) {
    return "%I ";
  } else {
    return "%? ";
  }
}

//! \return the full name for the store
const char *CodeAnnotationManager::getName () const
{
  if (mFlags & CodeAnnotation::cHDL_SOURCE) {
    return "HDL";
  } else if (mFlags & CodeAnnotation::cIMPL) {
    return "implementation";
  } else {
    return "<error>";
  }
}

//! \return the file name prefix for the store
const char *CodeAnnotationManager::getStoreNamePrefix () const
{
  if (mFlags & CodeAnnotation::cHDL_SOURCE) {
    return "hdl";
  } else if (mFlags & CodeAnnotation::cIMPL) {
    return "impl";
  } else {
    return "<error>";
  }
}

//! \return the filename part of a path
const char *CodeAnnotationManager::stripName (const char *name) const
{
  if (mFlags & CodeAnnotation::cIMPL) {
    // for implementation annotation strip out the directory part... we will
    // always be able to suss out which file in src/codegen from the filename
    // part
    unsigned int slash = 0;
    unsigned int n = 0;
    for (n = 0; name [n] != '\0'; n++) {
      if (name [n] == '/') {
        slash = n;
      }
    }
    return (slash > 0 && slash < n - 1) ? name + slash + 1 : name;
  } else {
    return name;
  }
}

//! Open the annotation store for reading or writing
/*! \return iff the store was open successfully
 *  \param file_root The root of the directory name created (usually libdesign)
 *  \param errmesg If opening the store fails errmsg is set to an error message
 */
bool CodeAnnotationManager::openStore (const char *file_root, const UInt32 file_map_options, 
  UtString *errmsg)
{
  ASSERT (!(mFlags & CodeAnnotation::cNO_STORE));
  if (mFlags & CodeAnnotation::cCOMPRESS_STORE) {
    mStore.set (CodeAnnotationStore::cCOMPRESSED);
  }
  if (!mStore.open (file_root, getStoreNamePrefix (), file_map_options, errmsg)) {
    return false;
  }
  return false;
}

//! Dump the manager configuration to stdout
void CodeAnnotationManager::pr () const
{
  print (UtIO::cout ());
}

//! Dump the manager configuration to a stream
void CodeAnnotationManager::print (UtOStream &s) const
{
  int n = 0;
  s << getStoreNamePrefix () << "-CodeAnnotationManager: ";
#define SHOW(_x_) if ((mFlags & CodeAnnotation::c##_x_) == 0) { \
  } else if (n > 0) { \
    s << "|" #_x_; \
  } else { \
    s << #_x_; n++; \
  }
  SHOW (HDL_SOURCE);
  SHOW (IMPL);
  SHOW (VISIBLE);
  SHOW (NEWLINE);
  SHOW (LEADSLINE);
  SHOW (COMPRESS_STORE);
#undef SHOW
  s << "\n";
  mStore.print (s);
}


/***********************
 * CodeAnnotationStore *
 ***********************/

CodeAnnotationStore::CodeAnnotationStore (const UInt32 flags, ErrorSink *error_sink) : 
  mName ("<unknown>"), mErrorSink (error_sink), mFlags (flags), 
  mDbStream (NULL), mCfgStream (NULL), mDbInput (NULL), mCfgInput (NULL)
{}

CodeAnnotationStore::CodeAnnotationStore (const UInt32 flags, const char *name) : 
  mName (name), mErrorSink (NULL), mFlags (flags), 
  mDbStream (NULL), mCfgStream (NULL), mDbInput (NULL), mCfgInput (NULL)
{}

/*virtual*/ CodeAnnotationStore::~CodeAnnotationStore ()
{
  close ();
}

//! Look up a file in the file map
bool CodeAnnotationStore::findFileIndex (const UtString &filename, UInt32 *index) const
{
  FileMap::const_iterator found;
  if ((found = mFileMap.find (filename.c_str ())) == mFileMap.end ()) {
    return false;
  } else {
    *index = found->second;
    return true;
  }
}

//! Dump the CodeAnnotationStore configuration to stdout
void CodeAnnotationStore::pr () const
{
  print (UtIO::cout ());
}

//! Dump the CodeAnnotationStore configuration to a file
void CodeAnnotationStore::print (UtOStream &s) const
{
  int n = 0;
  s << "CodeAnnotationStore: ";
#define SHOW(_x_) if ((mFlags & CodeAnnotationStore::c##_x_) == 0) { \
  } else if (n > 0) { \
    s << "|" #_x_; \
  } else { \
    s << #_x_; n++; \
  }
  SHOW (READING);
  SHOW (WRITING);
  SHOW (COMPRESSED);
#undef SHOW
  s << "\n";
  mFileMap.print (s);
}

//! Open the persistent store
bool CodeAnnotationStore::open (const char *file_root, const char *name, const UInt32 file_map_options,
  UtString *errmsg)
{
  mFileMap.set (file_map_options);
  ASSERT ((mFlags & (cREADING|cWRITING)) != 0); // it must be one or 'tother
  UtString stat_errormsg;
  UtString mkdir_errormsg;
  OSStatEntry stat_entry;
  UtString filename;
  UtString dirname;
  dirname << file_root << "." << name << "-annotations";
  OSStatFileEntry (dirname.c_str (), &stat_entry, &stat_errormsg);
  if (stat_entry.exists () && !stat_entry.isDirectory ()) {
    *errmsg << dirname << ": exists but is not a directory";
    return false;
  } else if ((mFlags & cREADING) && !stat_entry.exists ()) {
    *errmsg << "unable to locate annotation store directory " << dirname;
    return false;
  } else if ((mFlags & cWRITING) && stat_entry.exists ()) {
    // directory already exists
  } else if ((mFlags & cWRITING) && OSMkdir (dirname.c_str (), 0755, &mkdir_errormsg) < 0) {
    *errmsg << "unable to create annotation store directory: " << dirname << mkdir_errormsg;
    return false;
  }
  // open for writing if cWRITING
  if (!(mFlags & cWRITING)) {
    // not writting
  } else if (!openOutputFile (dirname, "annotations.cfg", &mCfgStream, false, errmsg)) {
    return false;                       // failed to create the configuration file
  } else if (!openOutputFile (dirname, "annotations.db", &mDbStream, true, errmsg)) {
    return false;                       // failed to create the database file
  }
  // open for reading if cREADING
  if (!(mFlags & cREADING)) {
    // not reading
  } else if (!openInputFile (dirname, "annotations.cfg", &mCfgInput, false, 0, errmsg)) {
    return false;
  } else if (!readCfg (errmsg)) {
    // need to read the configuration before opening annotations.db because the
    // configuration tells whether the database is compressed
    return false;
  } else if (!openInputFile (dirname, "annotations.db", &mDbInput, (mFlags & cCOMPRESSED), 
      Lexer::cNEWLINE_TOKEN, errmsg)) {
    return false;
  }
  mFlags |= cOPEN;                      // mark the store as open
  return true;
}

void CodeAnnotationStore::close ()
{
  if (mCfgStream != NULL) {
    writeCfg ();
    delete mCfgStream;
    mCfgStream = NULL;
  }
  if (mDbStream != NULL) {
    mDbStream->flush ();
    delete mDbStream;
    mDbStream = NULL;
  }
  if (mCfgInput != NULL) {
    delete mCfgInput;
    mCfgInput = NULL;
  }
  if (mDbInput != NULL) {
    delete mDbInput;
    mDbInput = NULL;
  }
  mFlags &= ~cOPEN;                     // mark the store as closed
}

//! Write the configuration files to mCfgStream
bool CodeAnnotationStore::writeCfg () const
{
  if (!writeFlags (*mCfgStream, mFlags) || !mFileMap.write (*mCfgStream)) {
    return false;
  }
  *mCfgStream << "%store_name " << mName << "\n";
  if (mCfgStream->bad ()) {
    return false;
  }
  mCfgStream->flush ();
  return true;
}

//! Read the configuration files from a mCfgInput
bool CodeAnnotationStore::readCfg (UtString *errmsg)
{
  while (!mCfgInput->lookingAt (Lexer::cEOF)) {
    switch (mCfgInput->currentToken ()) {
    case Lexer::cOPTIONS:
      if (!readFlags (*mCfgInput, &mFlags, errmsg)) {
        return false;
      }
      break;
    case Lexer::cSTORE_NAME:
      if (!mCfgInput->expected (Lexer::cSTORE_NAME)) {
        *errmsg << "configuration file is corruped";
        return false;
      } else if (!mCfgInput->expected (Lexer::cSTRING)) {
        mCfgInput->error ("expected annotation store name");
        *errmsg << "configuration file is corruped";
        return false;
      } else {
        mName = mCfgInput->getString ();
      }
      break;
    case Lexer::cFILES:
      if (!mFileMap.read (*mCfgInput, errmsg)) {
        return false;
      }
      break;
    default:
      mCfgInput->error ("unexpected token in store");
      *errmsg << "configuration file is corrupted";
      return false;
      break;
    }
  }
  return true;
}

bool CodeAnnotationStore::writeFlags (UtOStream &s, const UInt32 flags) const
{
  s << "%options\n";
#define CHECKFLAG(_flag_, _keyword_) if (flags & (_flag_)) { s << _keyword_ << "\n"; }
  CHECKFLAG (cCOMPRESSED, "%compressed");
#undef CHECKFLAG
  s << "%end\n";
  return !s.bad ();
}

bool CodeAnnotationStore::readFlags (Lexer &lexer, UInt32 *flags, UtString *errmsg) const
{
  // look for %options keyword
  if (!lexer.expected (Lexer::cOPTIONS)) {
    *errmsg << "annotations configuration file is corrupted";
    return false;
  }
  // read the flag setting keyword
  *flags &= ~(cCOMPRESSED);             // clear bits set from the cfg file
  while (!lexer.lookingAt (Lexer::cEOF) && !lexer.lookingAt (Lexer::cEND)) {
    switch (lexer.currentToken ()) {
    case Lexer::cCOMPRESSED:
      *flags |= cCOMPRESSED;
      break;
    default:
      lexer.error ("expected a configuration option; found %s", lexer.currentTokenName ());
      break;
    }
    lexer.nextToken ();
  }
  // look for %end keyword
  if (!lexer.expected (Lexer::cEND)) {
    *errmsg << "annotations configuration file is corrupted";
    return false;
  }
  return true;
}

//! Open an output file in the annotation store directory
bool CodeAnnotationStore::openOutputFile (UtString &dirname, const char *filename, 
  UtOStream **stream, bool compressible, UtString *errmsg)
{
  ASSERT (mFlags & cWRITING);
  // form the complete path name
  UtString path;
  path << dirname << "/" << filename;
  // If the cCOMPRESSED flag is set, then some of the files in the annotation
  // store are written as compressed files. This method created compressed
  // files if both the compressed parameter is set and the cCOMPRESSED option
  // bit is set.
  if (compressible && ((mFlags & cCOMPRESSED) != 0)) {
    // create a compressed stream
    UtOZStream *zstream;
    UTOZSTREAM_ALLOC (zstream, path.c_str ());
    *stream = zstream;
  } else {
    *stream = new UtOFStream (path.c_str ());    // uncompressed stream
  }
  // check that stream creation was successfull
  if (*stream == NULL) {
    // this is not good...
    *errmsg << filename << ": stream allocation failed";
    return false;
  } else if (!(*stream)->is_open ()) {
    // An OS error occurred creating the file.
    UtString oserror;
    OSGetLastErrmsg (&oserror);
    *errmsg << filename << ":" << oserror;
    return false;
  }
  return true;                          // it's all good...
}

//! Open an input file in the annotation store directory
bool CodeAnnotationStore::openInputFile (UtString &dirname, const char *filename, 
  Lexer **lexer, bool compressed, const UInt32 lexer_flags, UtString *errmsg)
{
  ASSERT (mFlags & cREADING);
  ASSERT (mErrorSink != NULL);
  UtIStream *stream;
  // form the complete path name
  UtString path;
  path << dirname << "/" << filename;
  if (compressed) {
    UtIZStream *zstream;
    UTIZSTREAM_ALLOC (zstream, path.c_str ());
    stream = zstream;
  } else {
    stream = new UtIFStream (path.c_str ());
  }
  // create the stream
  if (stream == NULL) {
    *errmsg << filename << ": stream allocation failed";
    return false;
  } else if (!stream->is_open ()) {
    UtString oserror;
    OSGetLastErrmsg (&oserror);
    *errmsg << filename << ":" << oserror;
    return false;
  }
  // create the Lexer
  *lexer = new Lexer (filename, stream, lexer_flags, mErrorSink);
  return true;
}

//! Write an annotation to the store
void CodeAnnotationStore::write (const CodeAnnotation &annotation)
{
  if (mFlags & cOPEN) {
    ASSERT (mDbStream != NULL);
    CodeAnnotation::Domain domain = annotation.getDomain ();
    CodeAnnotation::Range range = annotation.getRange ();
    *mDbStream << intern (domain.mFile) << " " << domain.mLine << " "
      << intern (range.mFile) << " " << range.mLine << " " << range.mColumn << "\n";
  }
}

//! Write a database entry to a file
void CodeAnnotationStore::DatabaseEntry::print (UtOStream &s) const
{
  s << mDomainFileIndex << " " << mDomainLineno << " "
    << mRangeFileIndex << " " << mRangeLineno << " " << mRangeColumn << "\n";
}

//! Write a database entry to stdout
void CodeAnnotationStore::DatabaseEntry::pr () const
{
  print (UtIO::cout ());
}

//! Read the next mapping from the database
bool CodeAnnotationStore::read (DatabaseEntry *entry)
{
  if (mDbInput->lookingAt (Lexer::cEOF)) {
    return false;                       // end of file
  } else if (!mDbInput->expected (&entry->mDomainFileIndex) 
    || !mDbInput->expected (&entry->mDomainLineno)
    || !mDbInput->expected (&entry->mRangeFileIndex) 
    || !mDbInput->expected (&entry->mRangeLineno) 
    || !mDbInput->expected (&entry->mRangeColumn)
    || !mDbInput->expected (Lexer::cNEWLINE)) {
    mDbInput->error ("database file is corrupted");
    return false;
  } else {
    return true;
  }
}

//! Read and dump the DB file contents
void CodeAnnotationStore::dumpDB (UtOStream &s)
{
  // construct a reverse file map that maps the integers to filenames
  UtHashMap <UInt32, UtString> reverse_file_map;
  mFileMap.reverse (&reverse_file_map);
  // now read the entries from the database
  while (!mDbInput->lookingAt (Lexer::cEOF)) {
    DatabaseEntry entry;
    if (!read (&entry)) {
      return;
    } 
    UtHashMap <UInt32, UtString>::const_iterator domain_file_found;
    UtHashMap <UInt32, UtString>::const_iterator range_file_found;
    if ((domain_file_found = reverse_file_map.find (entry.mDomainFileIndex)) == reverse_file_map.end ()) {
      mDbInput->error ("no entry found for file index %d", entry.mDomainFileIndex);
      return;
    }
    if ((range_file_found = reverse_file_map.find (entry.mRangeFileIndex)) == reverse_file_map.end ()) {
      mDbInput->error ("no entry found for file index %d", entry.mRangeFileIndex);
      return;
    }
    s << domain_file_found->second << ": " << entry.mDomainLineno << " -> "
      << range_file_found->second << ": " << entry.mRangeLineno << ": " << entry.mRangeColumn
      << "\n";
  }
}

/******************************
 * CodeAnnotationStore::Lexer *
 ******************************/

CodeAnnotationStore::Lexer::Lexer (const char *filename, UtIStream *input, const UInt32 flags,
  ErrorSink *error_sink) : 
  mFlags (flags), mErrorSink (error_sink), mFilename (filename), mInput (input), 
  mLineno (1), mBufferLength (0), mBufferIndex (0), mCurrent ('\0'), mTextIndex (0)
{
  nextChar ();                          // get the first character from the buffer
  nextToken ();                         // get the first token
}

/*virtual*/ CodeAnnotationStore::Lexer::~Lexer ()
{
  if (mInput != NULL) {
    delete mInput;
  }
}

//! Fill the buffer from the input stream
bool CodeAnnotationStore::Lexer::fillBuffer ()
{
  UInt32 nr = mInput->read (mBuffer, sizeof (mBuffer));
  if (nr > 0) {
    // read some characters from the stream
    mBufferIndex = 0;
    mBufferLength = nr;
    return true;
  } else if (nr == 0) {
    // read returns 0 at end of file
    return false;                       // end of file
  } else {
    // read failed with some error
    error ("%s", mInput->getErrmsg ());
    return false;
  }
}

//! Get the next character from the buffer
void CodeAnnotationStore::Lexer::nextChar ()
{
  if (mBufferIndex < mBufferLength) {
    mCurrent = mBuffer [mBufferIndex++];
  } else if (fillBuffer ()) {
    mCurrent = mBuffer [mBufferIndex++];
  } else {
    mCurrent = '\0';
  }
}

//! Report an error to the error sink
void CodeAnnotationStore::Lexer::error (const char *fmt, ...)
{
  char buffer [256];
  va_list ap;
  va_start (ap, fmt);
#if defined _MSC_VER && _MSC_VER < 0x0800
  unsigned int n = vsprintf (buffer, fmt, ap);
  ASSERT (n < sizeof (buffer));
#else
  vsnprintf (buffer, sizeof (buffer), fmt, ap);
#endif
  va_end (ap);
  (*mErrorSink) (mFilename, mLineno, buffer);
}

/*! \return iff the last token scanned matches the argument 
 *  \note If the token matches then nextToken is called to scan the next
 *  token in the stream. If the token does not match then an error message
 *  is written to the error sink.
 */
bool CodeAnnotationStore::Lexer::expected (const Tokens token)
{
  if (mLastToken == token) {
    nextToken ();
    return true;
  } else {
    error ("expected %s; found %s", image (token), image (mLastToken));
    return false;
  }
}

/*! \return iff the last token scanned matches was cINTEGER
 *
 *  \note If the token was cINTEGER then nextToken is called to scan the
 *  next token, and value is set to the integer value.  If the token does
 *  not match then an error message is written to the error sink.
 */
bool CodeAnnotationStore::Lexer::expected (UInt32 *value)
{
  if (mLastToken == cINTEGER) {
    *value = mLval.mInteger;
    nextToken ();
    return true;
  } else {
    error ("expected an integer; found %s", image (mLastToken));
    return false;
  }
}

//! \return a human readable name for the token
/*static*/ const char *CodeAnnotationStore::Lexer::image (const Tokens token)
{
  switch (token) {
#define CASE(_x_) case c##_x_: return #_x_
  CASE (COMPRESSED);
  CASE (END);
  CASE (EOF);
  CASE (FILES);
  CASE (INTEGER);
  CASE (NEWLINE);
  CASE (OPTIONS);
  CASE (STRING);
#undef CASE
  default: return "%%error%%";
  }
}

//! Scan the next token in the input stream
void CodeAnnotationStore::Lexer::nextToken ()
{
  int state = 0;                        // state of the machine
  // The state machine...
  while (state < 100) {
    switch (state) {
    case 0:
      // initial state - looking for a token
      switch (mCurrent) {
      case '\0':
        mLastToken = cEOF;
        state = 100;                    // final state
        break;
      case ' ': case '\t':
        nextChar ();                    // consume whitespace
        break;
      case '\n':
        mLineno++;
        nextChar ();
        if (mFlags & cNEWLINE_TOKEN) {
          // lexer is configured to return newline as a token
          mLastToken = cNEWLINE;
          state = 100;                  // final state
        } else {
          // just consume the newline and keep on scanning... it's whitespace
        }
        break;
      case '%':
        state = 10;                     // scanning a %keyword
        mTextIndex = 0;
        nextChar ();                    // consume the %
        break;
      case '"':
        state = 20;                     // scanning a string
        mTextIndex = 0;
        nextChar ();                    // consume the '"'
        break;
      default:
        if (isalpha (mCurrent) || ispunct (mCurrent)) {
          state = 30;                   // scanning an identifier or filename
          mTextIndex = 0;
        } else if (isdigit (mCurrent)) {
          mLval.mInteger = 0;
          state = 40;
        } else {
          error ("unexpected character: '%c'", mCurrent);
          nextChar ();
        }
        break;
      }
      break;
    // States for scanning %keyword
    case 10:
      // fill the mText buffer
      if (!isalpha (mCurrent) && mCurrent != '_') {
        state = 12;                     // end of the keyword
      } else if (mTextIndex >= sizeof (mText) - 1) {
        state = 11;                     // filled the buffer but not at the end of the keyword
      } else {
        mText [mTextIndex++] = mCurrent;
        nextChar ();
      }
      break;
    case 11:
      // overflowed the mText buffer... it will be a bogus keyword
      if (isalpha (mCurrent)) {
        nextChar ();                    // just through away the overflow
      } else {
        state = 12;
      }
      break;
    case 12:
      mText [mTextIndex++] = '\0';      // null terminate the keyword
      // identify the keyword
      if (!strcmp (mText, "compressed")) {
        mLastToken = cCOMPRESSED;
        state = 100;                    // final state
      } else if (!strcmp (mText, "end")) {
        mLastToken = cEND;
        state = 100;                    // final state
      } else if (!strcmp (mText, "options")) {
        mLastToken = cOPTIONS;
        state = 100;                    // final state
      } else if (!strcmp (mText, "files")) {
        mLastToken = cFILES;
        state = 100;                    // final state
      } else if (!strcmp (mText, "store_name")) {
        mLastToken = cSTORE_NAME;
        state = 100;                    // final state
      } else {
        error ("unrecognised keyword: %%%s", mText);
        state = 0;
      }
      break;
    // States for scanning strings
    case 20:
      if (mCurrent == '\0') {
        error ("unterminated string at end of file");
        state = 23;                     // end the string
      } else if (mCurrent == '\n') {
        error ("unterminated string at end of line");
        state = 23;                     // end the string
      } else if (mTextIndex >= sizeof (mText) - 1) {
        error ("string has overflowed the text buffer; maximum size of %d", sizeof (mText) - 1);
        state = 22;                     // look for an end to the string
      } else if (mCurrent == '"') {
        state = 23;                     // end of the string
        nextChar ();                    // consume the quote
      } else {
        // acrete the character into the string
        mText [mTextIndex++] = mCurrent;
        nextChar ();
      }
      break;
    case 21:
      // backquoted character in the string
      if (mCurrent == '\0' || mCurrent == '\n') {
        error ("backquote at end of line");
        state = 23;                     // end the string
      } else {
        mText [mTextIndex++] = mCurrent;
        state = 20;
      }
      break;
    case 22:
      // consume until end of line, end of file or "
      if (mCurrent == '\0' || mCurrent == '\n') {
        state = 20;                     // it is an error, state 20 will report
      } else if (mCurrent == '"') {
        state = 23;
        nextChar ();
      } else {
        nextChar ();
      }
      break;
    case 23:
      mText [mTextIndex] = '\0';
      mLval.mString = mText;
      mLastToken = cSTRING;
      state = 100;                    // final state
      break;
    // scan an identifier or filename
    case 30:
      if (mCurrent == '"') {
        state = 32;                     // hit the start of a string
      } else if (!isalnum (mCurrent) && !ispunct (mCurrent)) {
        state = 32;
      } else if (mTextIndex >= sizeof (mText) - 1) {
        error ("identifier or filename has overflowed the text buffer; maximum size of %d", sizeof (mText) - 1);
        state = 31;
      } else {
        mText [mTextIndex++] = mCurrent; // accumulate the character into the
        // identifier
        nextChar ();
      }
      break;
    case 31:
      // consume characters until the end of the token
      if (mCurrent == '"') {
        state = 32;                   // start of a quoted string
      } else if (!isalnum (mCurrent) && !ispunct (mCurrent)) {
        state = 32;
      } else {
        nextChar ();                    // consume the character
      }
      break;
    case 32:
      // return the identifier or filename
      mLastToken = cSTRING;
      mText [mTextIndex] = '\0';
      mLval.mString = mText;
      state = 100;
      break;
    // form an integer
    case 40:
      if (isdigit (mCurrent)) {
        mLval.mInteger = 10 * mLval.mInteger + (UInt32) mCurrent - (UInt32) '0';
        nextChar ();
      } else {
        mLastToken = cINTEGER;
        state = 100;                    // final state
      }
      break;
    default:
      error ("scanner jammed in state %d on '%c'", state, mCurrent);
      state = 0;
      break;
    }
  }
  // Noisy mode is for debugging the annotation store reader. It echoes each
  // token to stdout before returning. Note that there is no way to set noisy
  // from the command line, you will need to tweak the instantiation of the
  // lexer and recompile. Probably a bad idea to ship the reader with a noisy
  // scanner!
  if (mFlags & cNOISY) {
    switch (mLastToken) {
    case cSTRING:
      UtIO::cout () << "scanner returns " << image (mLastToken) << ": " << mLval.mString << "\n";
      break;
    case cINTEGER:
      UtIO::cout () << "scanner returns " << image (mLastToken) << ": " << mLval.mInteger << "\n";
      break;
    default:
      UtIO::cout () << "scanner returns " << image (mLastToken) << "\n";
      break;
    }
  }
}

/********************************
 * CodeAnnotationStore::FileMap *
 ********************************/

CodeAnnotationStore::FileMap::~FileMap ()
{
}

//! \return a unique integer for a filename
UInt32 CodeAnnotationStore::FileMap::intern (const char *filename)
{
  iterator found;
  if ((found = find (filename)) != end ()) {
    // found an extant entry
    return found->second;
  }
  UInt32 value = mNextValue++;
  insert (value_type (filename, value));
  if (filename [0] == '.' && filename [1] == '/') {
    // The filename is of the form ./foo.bar. Also create an entry for foo.bar;
    // they are the same file.
    insert (value_type (filename + 2, value));
  }
#if !pfWINDOWS
  if (mFlags & cNOISY) {
    char buffer [256];
    snprintf (buffer, sizeof (buffer), "intern (%s at 0x%08lx) new string returns %d",
      filename, (unsigned long) filename, value);
    UtIO::cout () << buffer << "\n";
  }
#endif
  return value;
}

//! Print the file map to a stream
void CodeAnnotationStore::FileMap::print (UtOStream &s) const
{
  for (const_iterator it = begin (); it != end (); ++it) {
    s << it->first << ": " << it->second << "\n";
  }
}

//! Print the file map to stdout
void CodeAnnotationStore::FileMap::pr () const
{
  print (UtIO::cout ());
}

//! Write the mapping to a stream
bool CodeAnnotationStore::FileMap::write (UtOStream &s) const
{
  s << "%files\n";
  for (const_iterator it = begin (); it != end (); ++it) {
    s << it->first << " " << it->second << "\n";
  }
  s << "%end\n";
  return !s.bad ();
}

//! Read the mapping from a stream
bool CodeAnnotationStore::FileMap::read (Lexer &lexer, UtString *errmsg)
{
  // look for %files keyword
  if (!lexer.expected (Lexer::cFILES)) {
    *errmsg << "annotations configuration file is corrupted";
    return false;
  }
  // Use cNEWLINE to terminate file table entries so set the scanner to return
  // newlines as tokens.
  UInt32 saved_lexer_flags;
  lexer.pushFlags (Lexer::cNEWLINE_TOKEN, &saved_lexer_flags);
  // read the file table entries
  while (!lexer.lookingAt (Lexer::cEOF) && !lexer.lookingAt (Lexer::cEND)) {
    if (!lexer.lookingAt (Lexer::cSTRING)) {
      *errmsg << "annotations configuration file is corrupted";
      return false;
    }
    UtString filename (lexer.getString ());
    lexer.nextToken ();
    if (!lexer.lookingAt (Lexer::cINTEGER)) {
      *errmsg << "annotations configuration file is corrupted";
      return false;
    }
    UInt32 index = lexer.getInteger ();
    lexer.nextToken ();
    if (!lexer.lookingAt (Lexer::cNEWLINE)) {
      *errmsg << "annotations configuration file is corrupted";
      return false;
    }  
    lexer.nextToken ();
    insert (value_type (filename, index));
  }
  // Set the scanner back to treating newlines as whitespace
  lexer.popFlags (saved_lexer_flags);
  // look for %end keyword
  if (!lexer.expected (Lexer::cEND)) {
    *errmsg << "annotations configuration file is corrupted";
    return false;
  }
  return true;
}

//! Reverse the mapping
void CodeAnnotationStore::FileMap::reverse (UtHashMap <UInt32, UtString> *reversed) const
{
  for (const_iterator it = begin (); it != end (); ++it) {
    UtString filename = it->first;
    reversed->insert (UtHashMap <UInt32, UtString>::value_type (it->second, it->first));
  }
}
