/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtDLList.h"

UtDLListNodeBase::UtDLListNodeBase() {
      mPrev = NULL;
      mNext = NULL;
}

void UtDLListNodeBase::swap(UtDLListNodeBase *x, UtDLListNodeBase *y) {
  UtDLListNodeBase *tmp_x_prev = x->mPrev;
  UtDLListNodeBase *tmp_x_next = x->mNext;

  x->mPrev = y->mPrev;
  x->mNext = y->mNext;

  y->mPrev = tmp_x_prev;
  y->mNext = tmp_x_next;
}

void UtDLListNodeBase::transfer(UtDLListNodeBase *first, UtDLListNodeBase *last) {

  // First Remove From their old list
  first->mPrev->mNext = last->mNext;
  last->mNext->mPrev = first->mPrev;
  
  // Then Hook to this list
  first->mPrev = mPrev;
  last->mNext  = this;
  mPrev->mNext = first;
  
  // Now Updated myself
  mPrev = last;

}

// Put "this" node before the specified node in the list
void UtDLListNodeBase::hook(UtDLListNodeBase * const pos) {
  mPrev        = pos->mPrev;
  mNext        = pos;
  mPrev->mNext = this;
  mNext->mPrev = this;
}

// Remove "this" node from the list
void UtDLListNodeBase::unhook() {
  mPrev->mNext = mNext;
  mNext->mPrev = mPrev;
}

