// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
//
// Carbon's Linux environment is RedHat 7.3 (RH73).  That is where we will
// build our software for distribution to customers, and many of our
// compute-farm machines are RH73.  But the new development machines are
// RH8.  So if a developer links an exectuable in RH8 he cannot easily
// run it on RH73.
//
// In order to run on RH73, we create an area under $CARBON_HOME from
// carbon_config:  $CARBON_HOME/Linux/lib/rh8, and put two libraries
// there: libpthreads and libc.  To run an RH8-linked executable, we
// must include $CARBON_HOME/Linux/lib/rh8 in LD_LIBRARY_PATH.  This
// is done in $CARBON_EXEC, if that directory exists.
//
// That directory will not exist when running a configuration build
// on RH73.
//
// So far so good, but many of our regression tests link programs, and
// so when we run regression on RH73 from an RH80-linked executable,
// we must *not* have that variable set.  So any program that spawns
// other programs, such as cbuild and speedcc, must strip the lib/rh8
// from LD_LIBRARY_PATH.  This file provides a function to do that.
//
// Note that 'putenv' requires that we never modify the string we give
// it.  So to avoid looking like a memory leak to the carbon memory
// system, we just use strdup.

#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/OSWrapper.h"
#include <stdlib.h>
#include <string.h>

// intended usage: 
//    UtStripEnv("LD_LIBRARY_PATH", ":$CARBON_HOME/Linux/lib/rh8")
void UtStripEnv(const char* envVar, const char* stringToRemove)
{
  char* val = getenv(envVar);
  if (val != NULL)
  {
    char* substr = strstr(val, stringToRemove);
    if (substr != NULL)
    {
      UtString tmp(envVar);
      tmp << "=";
      tmp.append(val, substr - val);
      tmp.append(substr + strlen(stringToRemove));
      char* newval = strdup(tmp.c_str());
      int status = putenv(newval);
      if (status != 0)
      {
        perror(newval);
        exit(1);
      }
    }
  }
}

void UtStripRH8lib()
{
  const char* carbonHome = getenv("CARBON_HOME");
  INFO_ASSERT(carbonHome, "$CARBON_HOME is not set.");
  UtString strip(":");
  strip << carbonHome << "/Linux/lib/rh8";
  UtStripEnv("LD_LIBRARY_PATH", strip.c_str());
}
