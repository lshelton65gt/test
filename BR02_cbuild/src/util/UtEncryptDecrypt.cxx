//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"

#include "util/UtEncryptDecrypt.h"

UtString UtEncryptDecrypt::encryptBuffer(const char* inputBuffer)
{
  UtString str(inputBuffer);
  
  UtString encrypted;
  ZENCRYPT_STRING(str, &encrypted);

  UtString encodedStr;
  StringUtil::asciiEncode(encrypted.data(), encrypted.length(), &encodedStr);
 
  return encodedStr;
}

UtString UtEncryptDecrypt::decryptBuffer(const char* inputBuffer, bool* status)
{
  UtString encodedStr(inputBuffer);

  UtString decodedStr;
  UtString decrypted;
  if (StringUtil::asciiDecode(encodedStr.c_str(), encodedStr.length(), &decodedStr))
  {
    *status = true;  
    ZDECRYPT_STRING(decodedStr, &decrypted);
    return decrypted;
  }
  
  *status = false;
  return "";
}


UtString UtEncryptDecrypt::decryptFile(const char* fileName, UtString* errMsg, bool* status)
{
  SInt64 size = 0;
  *status = true;
  *errMsg = "";

  if (OSGetFileSize(fileName, &size, errMsg) == false) 
    *status = false;

  FILE* inFile = 0;
  if ((inFile = OSFOpen(fileName, "r", errMsg)) == NULL)
    *status = false;

  if (*status)
  {
    UtString buffer(size+1, '\0');
    fread(buffer.getBuffer(), size, 1, inFile);
    fclose(inFile);
    return decryptBuffer(buffer.c_str(), status);
  }

  return "";
}

UtString UtEncryptDecrypt::encryptFile(const char* fileName, UtString* errMsg, bool* status)
{
  SInt64 size = 0;
  *status = true;
  *errMsg = "";

  if (OSGetFileSize(fileName, &size, errMsg) == false) 
    *status = false;

  FILE* inFile = 0;
  if ((inFile = OSFOpen(fileName, "r", errMsg)) == NULL)
    *status = false;

  if (*status)
  {
    UtString buffer(size+1, '\0');
    fread(buffer.getBuffer(), size, 1, inFile);
    fclose(inFile);
    return encryptBuffer(buffer.c_str());
  }

  return "";
}

bool UtEncryptDecrypt::protectEFFile(const char* fileName, UtString* errMsg)
{
  bool status = false;

  UtString fpath;
  UtString fname;
  UtString baseName;

  OSParseFileName(fileName, &fpath, &fname);

  baseName = fname;
  size_t dot = baseName.find_last_of('.');
  if (dot != UtString::npos)
    baseName.erase(dot);

  UtString oname;
  oname << baseName << ".ef";

  if (fpath.length() == 0)
    fpath = ".";

  UtString outputName;
  OSConstructFilePath(&outputName, fpath.c_str(), oname.c_str());

  UtString fileContents = encryptFile(fileName, errMsg, &status);

  if (status)
  {
    FILE* f = OSFOpen(outputName.c_str(), "w", errMsg);
    if (f)
    {
      fwrite(fileContents.c_str(), 1, fileContents.size(), f);
      fclose(f);
    }
    else
      status = false;
  }
 
  return status;
}

bool UtEncryptDecrypt::unprotectEFFile(const char* fileName, UtString* errMsg)
{
  bool status = false;

  UtString fpath;
  UtString fname;
  UtString baseName;

  OSParseFileName(fileName, &fpath, &fname);

  baseName = fname;
  size_t dot = baseName.find_last_of('.');
  if (dot != UtString::npos)
    baseName.erase(dot);

  UtString oname;
  oname << baseName << ".ori";

  if (fpath.length() == 0)
    fpath = ".";

  UtString outputName;
  OSConstructFilePath(&outputName, fpath.c_str(), oname.c_str());

  UtString fileContents = decryptFile(fileName, errMsg, &status);
  
  if (status)
  {
    FILE* f = OSFOpen(outputName.c_str(), "w", errMsg);
    if (f)
    {
      fwrite(fileContents.c_str(), 1, fileContents.size(), f);
      fclose(f);
    }
    else
      status = false;
  }
  return status;
}

