// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#define DynBitVector_KEEP_DEFS 1
#include "util/DynBitVector.h"
#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/Zstream.h"
#include "util/UtIOStream.h"
#include "util/UtConv.h"
#include "util/MemManager.h"
#include "util/CarbonRunTime.h"
#include "codegen/carbon_priv.h"

#undef LONG_BIT
#define LONG_BIT 32

static const char* cDynBitVectorSig = "DynBitVector";
static const UInt32 cDynBitVectorVersion = 0;

static inline size_t _S_whichword( size_t __pos ) {
  return __pos / __BITS_PER_WORDT;
}
static inline size_t _S_whichbit( size_t __pos ) {
  return __pos % __BITS_PER_WORDT;
}
static inline UInt32 _S_maskbit( size_t __pos ) {
  return static_cast<UInt32>(1) << _S_whichbit(__pos);
}
static inline UInt32 _S_maskbit (size_t __pos, size_t __siz) {
  INFO_ASSERT((__pos + __siz) <= LONG_BIT, "Index out-of-bounds."); // Can't cross word boundary
  return (__siz == LONG_BIT) ? 0xffffffff
    : ((1u<<__siz)-1) << __pos;
}

static inline UInt32 _S_firstfrag (size_t p, size_t s)
{
  if (((UInt32)p + (UInt32)s) > LONG_BIT)
    return LONG_BIT - p;
  else
    return s;
}


//! Compute log2(x)
size_t DynBitVector::sExactLog2 (size_t x){
  int log = 0;
  if (x== 0 || x != (x & -((long)x)))
    INFO_ASSERT(0, "Parameter not a power of two.");
  while((x >>= 1) != 0)
    log++;
  return log;
}

size_t DynBitVector::sLog2 (size_t x) {
  if (x == 0)
    return 0;

  if ((x & -((long)x)) == x)
    return sExactLog2 (x);

  DynBitVector v;
  v = x;
  return v.findLastOne () + 1;
}


UInt32& DynBitVector::_M_getword(size_t __pos)
{
  return getUIntArray()[_S_whichword(__pos)];
}
UInt32& DynBitVector::_M_hiword() 
{
  return getUIntArray()[numWords() - 1];
}
UInt32 DynBitVector::_M_hiword() const
{
  return getUIntArray()[numWords() - 1];
}

UInt32 DynBitVector::_M_getword(size_t __pos) const
{
  return getUIntArray ()[_S_whichword(__pos)];
}
#if JDM_NOTCOVERED
UInt32 DynBitVector::_M_hiword() const
{
  return getUIntArray ()[numWords() - 1];
}
#endif

//! iterate over a pair of BitVectors which are not necessarily the same size
class BVPairIter
{
public: CARBONMEM_OVERRIDES
  BVPairIter(DynBitVector* bv1, const DynBitVector* bv2):
    mV1(bv1->getUIntArray ()),
    mV2(bv2->getUIntArray ()),
    mSize1 (bv1->numWords ()),
    mSize2 (bv2->numWords ()),
    mLimit (std::min (mSize1, mSize2)),
    mIndex(0)
  {
  }

  void operator++() {++mIndex;}
  bool atEnd() {return mIndex == mLimit; }
  UInt32& w1() {return mV1[mIndex];}
  UInt32 w2(UInt32 fillval = 0) {
    return (mIndex < mSize2) ? mV2[mIndex]: fillval;
  }

private:
  UInt32 *mV1;
  const UInt32 *mV2;

  size_t mSize1,mSize2;
  size_t mLimit;
  size_t mIndex;
};

class ConstBVPairIter
{
public: CARBONMEM_OVERRIDES
  ConstBVPairIter(const DynBitVector* a, const DynBitVector* b):
    mA (a->getUIntArray ()),
    mB (b->getUIntArray ()),
    mSizeA (a->numWords ()),
    mSizeB (b->numWords ()),
    mIndex (std::max (mSizeA, mSizeB))
  {
  }

  void operator--() {--mIndex;}
  bool atEnd() {return mIndex == 0;}
  size_t off() { return mIndex-1; }
  UInt32 w1(UInt32 fillval = 0) {
    return (off() < mSizeA) ? mA[off()]: fillval;
  }
  UInt32 w2(UInt32 fillval = 0) {
    return (off() < mSizeB) ? mB[off()]: fillval;
  }
private:
  const UInt32* mA, *mB;
  size_t mSizeA, mSizeB;
  size_t mIndex;
};


bool DynBitVector::_M_is_greater(const DynBitVector& __x) const {
  return (compare(__x) > 0);
}

bool DynBitVector::isLessThan(const DynBitVector& __x) const {
  return _M_is_less(__x);
}
bool DynBitVector::isGreaterThan(const DynBitVector& __x) const {
  return _M_is_greater(__x);
}

bool DynBitVector::_M_is_less(const DynBitVector& __x) const {
  return (compare(__x) < 0);
}

bool DynBitVector::_M_is_equal(const DynBitVector& __x) const {
  return (compare(__x) == 0);
}

bool DynBitVector::_M_is_any() const {
  const UInt32* ptr = getUIntArray ();
  for ( const UInt32* end = ptr + numWords(); ptr != end; ++ptr ) {
    if ( *ptr != 0u )
      return true;
  }
  return false;
}

size_t DynBitVector::hash() const
{
  size_t hashVal = 0;

  // Count down so high order 0s don't affect the hash value,
  // so that we can make a hash-table for the factory where 0001 == 01
  const UInt32* lsw = getUIntArray ();
  for (const UInt32* ptr = lsw + numWords() - 1; ptr >= lsw; --ptr)
    hashVal = (17*hashVal) + *ptr;
  return hashVal;
}


//! Return the number of UInt32s used by the value array
UInt32 DynBitVector::getUIntArraySize() const {
  return (mNumBits + 31)/ 32;
}

void DynBitVector::_M_do_sanitize() {
  size_t extraBits = mNumBits%__BITS_PER_WORDT;
  if (extraBits != 0)
    _M_hiword() &= ~((~static_cast<UInt32>(0)) << extraBits);
}

// Count the ones in a 32-bit word.
size_t DynBitVector::popcount(unsigned int x)
{
  x -= ((x >> 1) & 0x55555555);
  x = (((x >> 2) & 0x33333333) + (x & 0x33333333));
  x = (((x >> 4) + x) & 0x0f0f0f0f);
  x += (x >> 8);
  x += (x >> 16);
  return (x & 0x000003F);
}

// Return parity of a 32-bit word.
size_t DynBitVector::redxor_word(unsigned int x)
{
    x ^= (x >> 16);
    x ^= (x >> 8);
    x ^= (x >> 4);
    x &= 0xf;
    return ((0x6996 >> x) & 0x1);
}


// Special checked reference construction
DynBitVector::reference DynBitVector::lpartsel(size_t pos, size_t size) {
  /*
   * It's necessary to have a location that any out-of-range bitfield
   * refers to (so that writes don't have to have special code.
   *
   */
  static UInt32 theNaughtyBits=0;

  if (pos >= mNumBits)
    // evaluate to zero-length reference
    {
      return reference(&theNaughtyBits, 0, 32, 0);
    }
  else if (pos+size > mNumBits)
    size = mNumBits - pos;

  return reference(*this, pos, size);
}

const DynBitVector::reference DynBitVector::partsel(size_t pos, size_t size) const {
  /*
   * It's necessary to have a location that any out-of-range bitfield
   * refers to (so that writes don't have to have special code.
   *
   */
  static UInt32 theNaughtyBits=0;

  if (pos >= mNumBits)
    // evaluate to zero-length reference
    {
      return reference(&theNaughtyBits, 0, 32, 0);
    }
  else if (pos+size > mNumBits)
    size = mNumBits - pos;

  return reference(*this, pos, size);
}

UInt64 DynBitVector::llvalue() const {
  if (numWords() < 2)
    return UInt64(value());
  else {
    const UInt32* ptr = getUIntArray ();
    return UInt64 (ptr[0]) | (UInt64)(ptr[1]) << __BITS_PER_WORDT;
  }
}

UInt32 DynBitVector::value() const {
  return *getUIntArray ();
}

CarbonReal DynBitVector::realvalue () const {
  // The 64 bits was stored as an image via memcpy
  INFO_ASSERT ( mNumBits == 64, "64 bits not allocated for real value." );
  CarbonReal r;
  memcpy (&r, getUIntArray (), sizeof(CarbonReal));
  return r;
}

DynBitVector& DynBitVector::operator=(UInt64 rhs) {
  UInt32* ptr = getUIntArray ();
  ptr[0] = (UInt32)rhs;

  if (__BITVECTOR_WORDS (mNumBits) > 1)
    ptr[1] = UInt32(rhs >> __BITS_PER_WORDT);

  if (__BITVECTOR_WORDS (mNumBits) <= 2)
    _M_do_sanitize ();
  else
    memset (static_cast<void*>(&ptr[2]), 0, 
            sizeof(UInt32) * (__BITVECTOR_WORDS (mNumBits) - 2));

  return *this;
}

DynBitVector& DynBitVector::operator=(const reference& rhs) {
  reference dest(this->getUIntArray (), 0, mNumBits, mNumBits);
  dest.anytoany (&rhs);
  return *this;
}

DynBitVector& DynBitVector::operator=(const DynBitVector& rhs)
{
  if (&rhs != this)
  {
    UInt32 nw = numWords ();
    UInt32 rhsNw = rhs.numWords ();
    if (nw != rhsNw)
    {
      if (nw > 1)
        CARBON_FREE_VEC(getUIntArray (), UInt32, nw);
      if (rhsNw > 1)
        mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, rhsNw);
    }
    mNumBits = rhs.mNumBits;
    memcpy (getUIntArray (), rhs.getUIntArray (), sizeof(UInt32) * rhsNw);
  }
  return *this;
}

void DynBitVector::copy(const DynBitVector& rhs)
{
  if (rhs.mNumBits == mNumBits)         // equal size -- assume rhs is sane
    memcpy(getUIntArray (), rhs.getUIntArray (), sizeof(UInt32) * __BITVECTOR_WORDS (mNumBits));
  else if  (rhs.mNumBits > mNumBits)   // src larger -- copy to our size & sanitize
  {
    memcpy(getUIntArray (), rhs.getUIntArray (), sizeof(UInt32) * __BITVECTOR_WORDS (mNumBits));
    _M_do_sanitize ();
  }
  else                        // dst larger -- copy & pad
  {
    size_t rhsBytes = sizeof(UInt32) * __BITVECTOR_WORDS (rhs.mNumBits);
    size_t lhsBytes = sizeof(UInt32) * __BITVECTOR_WORDS (mNumBits);
    UInt32* dptr = getUIntArray ();
    memcpy(dptr, rhs.getUIntArray (), rhsBytes);
    memset(((char*) dptr) + rhsBytes, 0, lhsBytes - rhsBytes);
  }
}

void DynBitVector::copy(UInt64 rhs)
{
  UInt32* ptr = getUIntArray ();
  ptr[0] = (UInt32)rhs;

  if (__BITVECTOR_WORDS (mNumBits) > 1)
    ptr[1] = UInt32(rhs >> __BITS_PER_WORDT);

  if (__BITVECTOR_WORDS (mNumBits) <= 2)
    _M_do_sanitize ();
  else
    memset (static_cast<void*>(&ptr[2]), 0, 
            sizeof(UInt32) * (__BITVECTOR_WORDS (mNumBits) - 2));
}


void
DynBitVector::copyreal(const CarbonReal rhs)
{
  INFO_ASSERT ( mNumBits == 64, "64 bits not allocated for real value." );
  memcpy( getUIntArray (), &rhs, 8 );
}


// 23.3.5.2 BitVector operations:

DynBitVector& DynBitVector::operator+=(const DynBitVector& __rhs) {
  this->_M_do_add(__rhs);
  this->_M_do_sanitize ();
  return *this;
}

DynBitVector& DynBitVector::operator+=(unsigned int rvalue) {
  this->_M_do_add (rvalue);
  this->_M_do_sanitize ();
  return *this;
}

DynBitVector& DynBitVector::operator-=(const DynBitVector& __rhs) {
  this->_M_do_sub(__rhs);
  this->_M_do_sanitize ();
  return *this;
}

DynBitVector& DynBitVector::operator-=(unsigned int rvalue) {
  this->_M_do_sub (rvalue);
  this->_M_do_sanitize ();
  return *this;
}

DynBitVector& DynBitVector::operator/=(size_t __rhs) {
  this->_M_do_div (__rhs);
  return *this;
}

DynBitVector& DynBitVector::operator*=(size_t __rhs) {
  this->_M_do_mul (__rhs);
  this->_M_do_sanitize ();
  return *this;
}

void DynBitVector::signExtend(UInt32 sign_bit_index) {
  UInt32 new_sign_index = sign_bit_index + 1;
  if (new_sign_index < mNumBits) {
    UInt32 num_extra_sign_bits = mNumBits - new_sign_index;
    setRange(new_sign_index, num_extra_sign_bits, test(sign_bit_index));
  }
}

void DynBitVector::signedMultiply(const DynBitVector& lv,
                                  const DynBitVector& rv)
{
  UInt32 l_words = lv.getUIntArraySize();
  UInt32 l_last = lv.size() - 1;
  UInt32 r_words = rv.getUIntArraySize();
  UInt32 r_last = rv.size() - 1;
  UInt32 words = getUIntArraySize();

  // if both inputs are aligned or unsigned to begin with, then
  // there is no need to copy and sign-extend the data
  UInt32 num_sign_bits = lv.test(l_last) + rv.test(r_last);
  if (num_sign_bits == 0) {
    carbon_multiply (lv.getUIntArray (),
                     rv.getUIntArray (),
                     getUIntArray (),
                     l_words, r_words, words);
  }
  else {
    UInt32 l_aligned = l_words * 32;
    UInt32 r_aligned = r_words * 32;
    DynBitVector left(l_aligned), right(r_aligned);
    left.copy(lv);
    right.copy(rv);
    left.signExtend(l_last);
    right.signExtend(r_last);

    carbon_signed_multiply (left.getUIntArray (),
                            right.getUIntArray (),
                            getUIntArray (),
                            l_words, r_words, words);
  }

  _M_do_sanitize ();
}

SInt32 DynBitVector::getSignExtendedHighWord() const {
  SInt32 result = (SInt32) _M_hiword();
  UInt32 extend_bits = 32 - (size() % 32);
  if (extend_bits < 32) {
    result = (result << extend_bits) >> extend_bits;
  }
  return result;
}

// Compare two negative bitvectors that must be the same size.  This is
// essentially the same code as for templatized SBitVector.
static SInt32 sCompareNeg(const DynBitVector& a, const DynBitVector& b) {
  SInt32 a_hi_word = a.getSignExtendedHighWord();
  SInt32 b_hi_word = b.getSignExtendedHighWord();
  if (a_hi_word < b_hi_word) {
    return -1;
  }
  else if (a_hi_word > b_hi_word) {
    return 1;
  }
  // if here then both are negative, and same size, and first word is
  // identical so now we only need to compare the remaining words
  ConstBVPairIter i (&a, &b);
  for (--i; not i.atEnd(); --i) {
    if (i.w1() > i.w2())
      return 1;
    else if (i.w1() < i.w2())
      return -1;
  }
  return 0;		// must have been ==
}

SInt32 DynBitVector::signedCompare(const DynBitVector& that) const {
  // First compare the sign
  UInt32 this_size = size();
  bool this_is_negative = test(this_size - 1);
  UInt32 that_size = that.size();
  bool that_is_negative = that.test(that_size - 1);
  
  if ( this_is_negative and not that_is_negative ){
    // this < that
    return -1;
  } else if ( not this_is_negative and that_is_negative ){
    // this > that
    return 1;
  } else if ( this_is_negative and that_is_negative ){
    // before we do the compare we must have the same size vectors.
    if ( this_size > that_size ){
      DynBitVector cpy(this_size);
      cpy.copy(that);
      cpy.signExtend(that_size - 1);
      return sCompareNeg(*this, cpy);
    }
    if ( this_size < that_size ){
      DynBitVector cpy(that_size);
      cpy.copy(*this);
      cpy.signExtend(this_size - 1);
      return sCompareNeg(cpy, that);
    }
    return sCompareNeg(*this, that);
  }

  // if here then both numbers are non-negative, so we can use an
  // unsigned compare.
  return compare(that);
} // SInt32 DynBitVector::signedCompare

void DynBitVector::multiply(const DynBitVector& lv,
                                  const DynBitVector& rv)
{
  carbon_multiply (lv.getUIntArray (),
                   rv.getUIntArray (),
                   getUIntArray (),
                   lv.numWords (),
                   rv.numWords (),
                   numWords ());
  _M_do_sanitize ();
}

DynBitVector& DynBitVector::operator&=(const DynBitVector& __rhs) {
  this->_M_do_and(__rhs);
  return *this;
}

DynBitVector& DynBitVector::operator|=(const DynBitVector& __rhs) {
  this->_M_do_or(__rhs);
  this->_M_do_sanitize();
  return *this;
}

DynBitVector& DynBitVector::operator^=(const DynBitVector& __rhs) {
  this->_M_do_xor(__rhs);
  this->_M_do_sanitize();
  return *this;
}


#if JDM_NOTYET
DynBitVector& DynBitVector::operator<<=(const DynBitVector&__rhs) {
  UInt32 __pos = __rhs[0][std::min (__rhs.mNumBits, __BITS_PER_WORDT)];

  this->_M_do_left_shift(__pos);
  this->_M_do_sanitize();
  return *this;
}

DynBitVector& DynBitVector::operator>>=(const DynBitVector&__rhs) {
  UInt32 __pos = __rhs[0][std::min (__rhs.mNumBits, __BITS_PER_WORDT)];

  this->_M_do_right_shift(__pos);
  this->_M_do_sanitize();	// ??? is this necessary?
  return *this;
}
#endif

// With a UInt32
DynBitVector& DynBitVector::operator&=(UInt32 __rhs) {
  UInt32* ptr = getUIntArray ();
  *ptr &= __rhs;

  for(UInt32* end=ptr + numWords (); ptr != end; ++ptr)
    *ptr = 0;

  return *this;
}

DynBitVector& DynBitVector::operator|=(UInt32 __rhs) {
  *getUIntArray () |= __rhs;
  this->_M_do_sanitize ();
  return *this;
}

DynBitVector& DynBitVector::operator^=(UInt32 __rhs) {
  *getUIntArray () ^= __rhs;
  this->_M_do_sanitize();
  return *this;
}

// With a UInt64
DynBitVector& DynBitVector::operator&=(UInt64 __rhs) {
  // Assume remaining bits are 1's
  UInt32* p = getUIntArray();
  p[0] &= __rhs & 0xffffffff;
  p[1] &= __rhs >> 32;
  return *this;
}

DynBitVector& DynBitVector::operator|=(UInt64 __rhs) {
  // Assume remaining bits are 0's
  UInt32* p = getUIntArray();
  p[0] |= __rhs & 0xffffffff;
  p[1] |= __rhs >> 32;
  this->_M_do_sanitize ();
  return *this;
}

DynBitVector& DynBitVector::operator^=(UInt64 __rhs) {
  // Assume remaining bits are 0's
  UInt32* p = getUIntArray();
  p[0] ^= __rhs & 0xffffffff;
  p[1] ^= __rhs >> 32;
  this->_M_do_sanitize();
  return *this;
}

DynBitVector& DynBitVector::operator<<=(size_t __pos) {
  this->_M_do_left_shift(__pos);
  this->_M_do_sanitize();
  return *this;
}

DynBitVector& DynBitVector::operator>>=(size_t __pos) {
  this->_M_do_right_shift(__pos);
  this->_M_do_sanitize();
  return *this;
}



// Conversions
#if JDM_NOTYET
// Convert an object of size mNumBits to size K
//
template <size_t K> operator BitVector<K>()
  {
    if (__BITVECTOR_WORDS (K) == __BITVECTOR_WORDS (mNumBits))
      // Can just copy by value
      return *(BitVector<K> *)this;
    else
      // use existing bv to initialize new one via copy construction
      return BitVector<K>(*this);
  }
#endif

//
// Extension:
// Versions of single-bit set, reset, flip, test
//
DynBitVector& DynBitVector::set(size_t __pos) {
  INFO_ASSERT(__pos < mNumBits, "Index out-of-bounds.");
  this->_M_getword(__pos) |= _S_maskbit(__pos);
  return *this;
}

DynBitVector& DynBitVector::set(size_t __pos, int __val) {
  INFO_ASSERT(__pos < mNumBits, "Index out-of-bounds.");
  if (__val)
    this->_M_getword(__pos) |= _S_maskbit(__pos);
  else
    this->_M_getword(__pos) &= ~ _S_maskbit(__pos);

  return *this;
}

DynBitVector& DynBitVector::reset(size_t __pos) {
  INFO_ASSERT(__pos < mNumBits, "Index out-of-bounds.");
  this->_M_getword(__pos) &= ~ _S_maskbit(__pos);
  return *this;
}

DynBitVector& DynBitVector::flip(size_t __pos) {
  INFO_ASSERT(__pos < mNumBits, "Index out-of-bounds.");
  this->_M_getword(__pos) ^= _S_maskbit(__pos);
  return *this;
}

bool DynBitVector::test(size_t __pos) const {
  INFO_ASSERT(__pos < mNumBits, "Index out-of-bounds.");
  return (this->_M_getword(__pos) & _S_maskbit(__pos)) != static_cast<UInt32>(0);
}

// Set, reset, and flip.

DynBitVector& DynBitVector::set() {
  this->_M_do_set();
  this->_M_do_sanitize();
  return *this;
}

DynBitVector& DynBitVector::reset() {
  this->_M_do_reset();
  return *this;
}

DynBitVector& DynBitVector::flip() {
  this->_M_do_flip();
  this->_M_do_sanitize();
  return *this;
}

DynBitVector DynBitVector::operator~() const { 
  return DynBitVector(*this).flip();
}

DynBitVector DynBitVector::operator-() const {
  DynBitVector result(*this);
  result.negate();
  return result;
}

void DynBitVector::setRange(size_t pos, size_t num, int fillVal) {
  // clear/set as many bits as we can 32-at-a-time
  UInt32 firstBitPos = pos % 32;
  SInt32 firstWord = pos / 32;
  bool firstWordIsFull = (firstBitPos == 0) && (num >= 32);

  if (!firstWordIsFull) {
    ++firstWord;
  }

  SInt32 lastBit = pos + num - 1;
  UInt32 numBitsInLastWord = (lastBit % 32) + 1;

  SInt32 lastWord = lastBit / 32;
  bool lastWordIsFull = (numBitsInLastWord == 32) && (num >= 32);

  if (!lastWordIsFull) {
    --lastWord;
  }

  UInt32* words = getUIntArray();
  if (firstWord <= lastWord) {
    UInt32 fullVal = fillVal ? 0xffffffff : 0x0;
    // Is it faster to loop because it's always 32-bit aligned?
    memset(words + firstWord, fullVal, 4*(lastWord - firstWord + 1));
  }

  // deal with the slop at the beginning of the range
  if (!firstWordIsFull) {
    --firstWord;
    UInt32 numBitsInFirstWord = std::min((UInt32) num, 32 - firstBitPos);
    UInt32 mask = ((1 << numBitsInFirstWord) - 1) << firstBitPos;
    if (fillVal) {
      words[firstWord] |= mask;
    }
    else {
      words[firstWord] &= ~mask;
    }
  }    

  if (!lastWordIsFull) {
    ++lastWord;
    if (lastWord > firstWord) {
      INFO_ASSERT(numBitsInLastWord < 32, "lastWord must be full");
      UInt32 mask = (1 << numBitsInLastWord) - 1;
      if (fillVal) {
        words[lastWord] |= mask;
      }
      else {
        words[lastWord] &= ~mask;
      }
    }
  }
} // void DynBitVector::setRange

//! element access:
//for b[i];
DynBitVector::reference DynBitVector::operator[](size_t __pos) {
  INFO_ASSERT(__pos < mNumBits, "Index out-of-bounds.");
  return reference(*this,__pos);
}

//! element access:
//for b[i];
const DynBitVector::reference DynBitVector::operator[](size_t __pos) const {
  INFO_ASSERT(__pos < mNumBits, "Index out-of-bounds.");
  return reference(*this,__pos);
}

//  bool test(size_t __pos) const { return DynBitVector::test(__pos); }

UInt32 DynBitVector::to_ulong() const { return this->_M_do_to_ulong(); }

size_t DynBitVector::count() const { return this->_M_do_count(); }

size_t DynBitVector::redxor () const {
  size_t result = 0;
  const UInt32* ptr = getUIntArray ();
  for (const UInt32* end= ptr + numWords (); ptr != end; ++ptr)
    result ^= *ptr;

  return redxor_word (result) ;
}

size_t DynBitVector::size() const { return mNumBits; }

static inline void copy_words(UInt32* dst, UInt32 dstSize,
                              const UInt32* src, UInt32 srcSize)
{
  if (dstSize <= srcSize)
    memcpy(static_cast<void *>(dst), static_cast<const void *>(src),
           dstSize*sizeof(UInt32));
  else
  {
    memcpy(static_cast<void *>(dst), static_cast<const void *>(src),
           srcSize*sizeof(UInt32));
    memset(static_cast<void *>(&dst[srcSize]),
           0,
           (dstSize - srcSize)*sizeof(UInt32));
  }
}

void DynBitVector::resize(size_t size) {
  size_t oldNumWords = numWords();
  UInt32* old = getUIntArray ();
  mNumBits = size;
  size_t newNumWords = numWords();
  if (oldNumWords != newNumWords)
  {
    UInt32 oldCopy = *old;        // Only need if resizing from one-word
    if (newNumWords != 1)
      mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, newNumWords);
    if (oldNumWords != 1) {
      copy_words(getUIntArray (), newNumWords, old, oldNumWords);
      CARBON_FREE_VEC(old, UInt32, oldNumWords);
    } else {
      // We had one word before
      copy_words (getUIntArray (), newNumWords, &oldCopy, 1);
    }
  }
  _M_do_sanitize ();
}

bool DynBitVector::operator==(const DynBitVector& __rhs) const {
  return this->_M_is_equal(__rhs);
}
bool DynBitVector::operator<(const DynBitVector& __rhs) const {
  return this->_M_is_less(__rhs);
}
#if pfGCC_2
bool DynBitVector::operator>(const DynBitVector& __rhs) const {
  return this->_M_is_greater(__rhs);
}
#endif

// relational operators between BitVector and UInt32
bool DynBitVector::operator==(UInt32 rhs) const {
  const UInt32* ptr = getUIntArray ();
  if (ptr[0] == rhs)
    {
      for (size_t i=1; i<__BITVECTOR_WORDS (mNumBits); ++i)
        if (ptr[i] != 0)
          return false;
      return true;
    }
  else
    return false;
}

#if pfGCC_2
bool DynBitVector::operator!=(const DynBitVector& r) const
{
  return !(*this == r);
}
#endif

bool DynBitVector::operator<(UInt32 rhs) const {
  const UInt32* ptr = getUIntArray ();
  const UInt32* end = ptr + numWords ();
  if (*ptr++ >= rhs)
    return false;

  for (; ptr != end; ++ptr)
    if (*ptr != 0)
      return false;

  return true;
}

bool DynBitVector::operator>(UInt32 rhs) const {
  const UInt32* ptr = getUIntArray ();
  const UInt32* end = ptr + numWords ();
  if (*ptr++ > rhs)
    return true;

  for (; ptr != end; ++ptr)
    if (*ptr != 0)
      return true;

  return false;
}

bool DynBitVector::any() const { return this->_M_is_any(); }
bool DynBitVector::none() const { return !this->_M_is_any(); }

bool DynBitVector::isRangeOnes (UInt32 pos, UInt32 size) const
{
  return partsel (pos, size).count () == size;
}

bool DynBitVector::isRangeZero (UInt32 pos, UInt32 size) const
{
  return not partsel (pos, size).any ();
}

bool DynBitVector::anyCommonBitsSet(const DynBitVector &v) const
{
  const UInt32* ptr = getUIntArray (),
        * vptr = v.getUIntArray ();
  for (const UInt32* end = ptr + std::min(numWords(), v.numWords());
       ptr != end;
       ++ptr,++vptr)
    if (*ptr & *vptr)
      return true;

  return false;
}


void DynBitVector::printHex() const
{
  const UInt32 * uv = getUIntArray ();

  // generate format UtString for high order UtIO::hex digit
  int highHexDigits = size() % 8;
  if (highHexDigits == 0)
    highHexDigits = 8;
  char format[10];
  sprintf(format, "%%0%dx", highHexDigits);

  int i=__BITVECTOR_WORDS (size()) - 1;
  fprintf(stdout, format, uv[i]);
  for (--i; i >= 0; --i)
    fprintf(stdout, "%08x", uv[i]);
  fputc('\n', stdout);
  fflush(stdout);
}

void DynBitVector::printBin() const
{
  for (int i = size() - 1; i >= 0; --i)
  {
    bool val = (*this)[i] != 0;
    fputc(val? '1': '0', stdout);
  }
  fputc('\n', stdout);
  fflush(stdout);
}

// Handy place to insert a breakpoint.  Function should be inlined
// in production compiler.
//
void DynBitVector::initcheck (UInt32 size)
{
  INFO_ASSERT (size <= DYNBITVECTOR_MAX_NUM_BITS, "Requested size too large.");
}
  
DynBitVector::DynBitVector(const DynBitVector& dbv)
{
  initcheck (dbv.mNumBits);
  mNumBits = dbv.mNumBits;
  UInt32 nw = numWords ();
  if (nw > 1)
    mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, nw);

  memcpy (getUIntArray (), dbv.getUIntArray (), sizeof(UInt32)*nw);
}

//! General left-shift
void DynBitVector::_M_do_left_shift(size_t __shift) 
{
  if (__shift == 0)
    return;
  else if (__shift >= mNumBits)
    this->_M_do_reset ();
  else if (__shift % CHAR_BIT)  
    {
      const size_t __wshift = __shift / __BITS_PER_WORDT;
      const size_t __offset = __shift % __BITS_PER_WORDT;
      const size_t __sub_offset = __BITS_PER_WORDT - __offset;
      size_t __n = numWords() - 1;
      UInt32* ptr = getUIntArray ();
      for ( ; __n > __wshift; --__n)
	ptr[__n] = (ptr[__n - __wshift] << __offset) | 
	  (ptr[__n - __wshift - 1] >> __sub_offset);
      if (__n == __wshift)
	ptr[__n] = ptr[0] << __offset;
      for (size_t __n1 = 0; __n1 < __n; ++__n1)
	ptr[__n1] = static_cast<UInt32>(0);
    }
  else
    {
      const size_t byteoff = __shift / CHAR_BIT;
      char *cp = reinterpret_cast<char *>(getUIntArray ());
      memmove (&cp[byteoff], &cp[0], numWords()*sizeof(UInt32) - byteoff);
      memset (&cp[0], 0, byteoff);
    }
}

//! General right-shift
void DynBitVector::_M_do_right_shift(size_t __shift) 
{
  if (__shift == 0) 
    return;
  else if (__shift >= mNumBits)
    this->_M_do_reset ();       // Zero it...
  else if (__shift % CHAR_BIT)  {
    const size_t __wshift = __shift / __BITS_PER_WORDT;
    const size_t __offset = __shift % __BITS_PER_WORDT;
    const size_t __sub_offset = __BITS_PER_WORDT - __offset;
    const size_t __limit = numWords() - __wshift - 1;
    size_t __n = 0;
    UInt32 cur;
    UInt32 nex;
    UInt32* ptr = getUIntArray ();
    for ( ; __n < __limit; ++__n) {
      cur = (ptr[__n + __wshift] >> __offset);
      nex = (ptr[__n + __wshift + 1] << __sub_offset);
      ptr[__n] = (cur | nex);
    }
    ptr[__limit] = ptr[numWords()-1] >> __offset;
    for (size_t __n1 = __limit + 1; __n1 < numWords(); ++__n1)
      ptr[__n1] = static_cast<UInt32>(0);
  }
  else {
    const size_t byteoff = __shift / CHAR_BIT;
    const size_t numBytes = numWords()*sizeof(UInt32) - byteoff;
    char *cp = reinterpret_cast<char *>(getUIntArray ());
    memmove(&cp[0], &cp[byteoff], numBytes);
    // We just moved numBytes from byteoff to 0. Therefore, the
    // remaining bytes to zero out start at numBytes offset and we
    // need to zero mNumBits - (mNumBits - byteoff) = byteoff
    memset (&cp[numBytes], 0, byteoff);
  }
}

DynBitVector::DynBitVector(size_t size,
                           const UInt32* initArray,
                           size_t initArrayLen)
{
  initcheck (size);
  mNumBits = size;
  size_t nwords = numWords();
  if (nwords > 1)
    mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, nwords);
  copy_words(getUIntArray (), nwords, initArray, initArrayLen);
  _M_do_sanitize ();
}


// ------------------------------------------------------------
// Lookup tables for find and count operations.
//! structure to aid in counting bits (Useful for reduction operators)
static unsigned char _S_bit_count[256] = {
  0, /*   0 */ 1, /*   1 */ 1, /*   2 */ 2, /*   3 */ 1, /*   4 */
  2, /*   5 */ 2, /*   6 */ 3, /*   7 */ 1, /*   8 */ 2, /*   9 */
  2, /*  10 */ 3, /*  11 */ 2, /*  12 */ 3, /*  13 */ 3, /*  14 */
  4, /*  15 */ 1, /*  16 */ 2, /*  17 */ 2, /*  18 */ 3, /*  19 */
  2, /*  20 */ 3, /*  21 */ 3, /*  22 */ 4, /*  23 */ 2, /*  24 */
  3, /*  25 */ 3, /*  26 */ 4, /*  27 */ 3, /*  28 */ 4, /*  29 */
  4, /*  30 */ 5, /*  31 */ 1, /*  32 */ 2, /*  33 */ 2, /*  34 */
  3, /*  35 */ 2, /*  36 */ 3, /*  37 */ 3, /*  38 */ 4, /*  39 */
  2, /*  40 */ 3, /*  41 */ 3, /*  42 */ 4, /*  43 */ 3, /*  44 */
  4, /*  45 */ 4, /*  46 */ 5, /*  47 */ 2, /*  48 */ 3, /*  49 */
  3, /*  50 */ 4, /*  51 */ 3, /*  52 */ 4, /*  53 */ 4, /*  54 */
  5, /*  55 */ 3, /*  56 */ 4, /*  57 */ 4, /*  58 */ 5, /*  59 */
  4, /*  60 */ 5, /*  61 */ 5, /*  62 */ 6, /*  63 */ 1, /*  64 */
  2, /*  65 */ 2, /*  66 */ 3, /*  67 */ 2, /*  68 */ 3, /*  69 */
  3, /*  70 */ 4, /*  71 */ 2, /*  72 */ 3, /*  73 */ 3, /*  74 */
  4, /*  75 */ 3, /*  76 */ 4, /*  77 */ 4, /*  78 */ 5, /*  79 */
  2, /*  80 */ 3, /*  81 */ 3, /*  82 */ 4, /*  83 */ 3, /*  84 */
  4, /*  85 */ 4, /*  86 */ 5, /*  87 */ 3, /*  88 */ 4, /*  89 */
  4, /*  90 */ 5, /*  91 */ 4, /*  92 */ 5, /*  93 */ 5, /*  94 */
  6, /*  95 */ 2, /*  96 */ 3, /*  97 */ 3, /*  98 */ 4, /*  99 */
  3, /* 100 */ 4, /* 101 */ 4, /* 102 */ 5, /* 103 */ 3, /* 104 */
  4, /* 105 */ 4, /* 106 */ 5, /* 107 */ 4, /* 108 */ 5, /* 109 */
  5, /* 110 */ 6, /* 111 */ 3, /* 112 */ 4, /* 113 */ 4, /* 114 */
  5, /* 115 */ 4, /* 116 */ 5, /* 117 */ 5, /* 118 */ 6, /* 119 */
  4, /* 120 */ 5, /* 121 */ 5, /* 122 */ 6, /* 123 */ 5, /* 124 */
  6, /* 125 */ 6, /* 126 */ 7, /* 127 */ 1, /* 128 */ 2, /* 129 */
  2, /* 130 */ 3, /* 131 */ 2, /* 132 */ 3, /* 133 */ 3, /* 134 */
  4, /* 135 */ 2, /* 136 */ 3, /* 137 */ 3, /* 138 */ 4, /* 139 */
  3, /* 140 */ 4, /* 141 */ 4, /* 142 */ 5, /* 143 */ 2, /* 144 */
  3, /* 145 */ 3, /* 146 */ 4, /* 147 */ 3, /* 148 */ 4, /* 149 */
  4, /* 150 */ 5, /* 151 */ 3, /* 152 */ 4, /* 153 */ 4, /* 154 */
  5, /* 155 */ 4, /* 156 */ 5, /* 157 */ 5, /* 158 */ 6, /* 159 */
  2, /* 160 */ 3, /* 161 */ 3, /* 162 */ 4, /* 163 */ 3, /* 164 */
  4, /* 165 */ 4, /* 166 */ 5, /* 167 */ 3, /* 168 */ 4, /* 169 */
  4, /* 170 */ 5, /* 171 */ 4, /* 172 */ 5, /* 173 */ 5, /* 174 */
  6, /* 175 */ 3, /* 176 */ 4, /* 177 */ 4, /* 178 */ 5, /* 179 */
  4, /* 180 */ 5, /* 181 */ 5, /* 182 */ 6, /* 183 */ 4, /* 184 */
  5, /* 185 */ 5, /* 186 */ 6, /* 187 */ 5, /* 188 */ 6, /* 189 */
  6, /* 190 */ 7, /* 191 */ 2, /* 192 */ 3, /* 193 */ 3, /* 194 */
  4, /* 195 */ 3, /* 196 */ 4, /* 197 */ 4, /* 198 */ 5, /* 199 */
  3, /* 200 */ 4, /* 201 */ 4, /* 202 */ 5, /* 203 */ 4, /* 204 */
  5, /* 205 */ 5, /* 206 */ 6, /* 207 */ 3, /* 208 */ 4, /* 209 */
  4, /* 210 */ 5, /* 211 */ 4, /* 212 */ 5, /* 213 */ 5, /* 214 */
  6, /* 215 */ 4, /* 216 */ 5, /* 217 */ 5, /* 218 */ 6, /* 219 */
  5, /* 220 */ 6, /* 221 */ 6, /* 222 */ 7, /* 223 */ 3, /* 224 */
  4, /* 225 */ 4, /* 226 */ 5, /* 227 */ 4, /* 228 */ 5, /* 229 */
  5, /* 230 */ 6, /* 231 */ 4, /* 232 */ 5, /* 233 */ 5, /* 234 */
  6, /* 235 */ 5, /* 236 */ 6, /* 237 */ 6, /* 238 */ 7, /* 239 */
  4, /* 240 */ 5, /* 241 */ 5, /* 242 */ 6, /* 243 */ 5, /* 244 */
  6, /* 245 */ 6, /* 246 */ 7, /* 247 */ 5, /* 248 */ 6, /* 249 */
  6, /* 250 */ 7, /* 251 */ 6, /* 252 */ 7, /* 253 */ 7, /* 254 */
  8  /* 255 */
}; // end _Bit_count

size_t DynBitVector::_M_do_count() const {
  size_t __result = 0;
  const unsigned char* __byte_ptr = (const unsigned char*)getUIntArray ();
  const unsigned char* __end_ptr = (const unsigned char*)(getUIntArray () + numWords());

  while ( __byte_ptr < __end_ptr ) {
    __result += _S_bit_count[*__byte_ptr];
    __byte_ptr++;
  }
  return __result;
}

#if 0
std::ostream& operator<< (std::ostream &f,
                          const DynBitVector::reference& v)
{
  DynBitVector::reference w(v.mWords, v._M_bpos, v._M_bsiz);
  size_t fw;
  for (fw = w._M_bsiz; fw > __BITS_PER_WORDT; fw -= __BITS_PER_WORDT)
    {
      int width=__BITS_PER_WORDT - w._M_bpos;

      UInt32 mask = 1ULL << width - (1 << w._M_bpos);
      f << *w.mWords++ & mask;
      w._M_bpos = (w._M_bpos + width) % __BITS_PER_WORDT;
    }

  // Remaining bits.

  f << *w.mWords & ((1ULL << fw) - 1);

  return f;
}
#endif

DynBitVector::DynBitVector()
{
  initcheck (32);
  mNumBits = 32;                // Assume smaller vectors are more common
  _M_do_reset();
}

// 23.3.5.1 constructors:
DynBitVector::DynBitVector(size_t size)
{
  initcheck (size);
  mNumBits = size;
  if (numWords () > 1)
    mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, numWords());
  _M_do_reset();
}

DynBitVector::DynBitVector(size_t size, UInt64 val)
{
  initcheck (size);
  mNumBits = size;
  if (numWords () > 1)
    mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, numWords());
  *this = val;
  _M_do_sanitize();
}

DynBitVector::DynBitVector (size_t size, const DynBitVector& v)
{
  initcheck (size);
  mNumBits = size;
  size_t nwords = numWords();
  if (nwords > 1)
    mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, nwords);

  copy_words(getUIntArray (), nwords, v.getUIntArray (), v.numWords());
  if (mNumBits < v.mNumBits)
    // sanitize any extra bits copied in high word
    _M_do_sanitize ();
}


DynBitVector::~DynBitVector()
{
  if (numWords () > 1)
    CARBON_FREE_VEC(getUIntArray (), UInt32, numWords());
}

//! Move bitfields.  Must handle unaligned and overlapping
// fields.
void DynBitVector::reference::anytoany(const DynBitVector::reference *src)
{
  // check for differing sizes!
  UInt32 cpySize = std::min (src->_M_bsiz, _M_bsiz);

  if (cpySize == 0)
    // No source bits need moving - fall thru to end of function
    // where we check for excess destination bits needing to be zeroed
    ;
  else if (_M_bpos == src->_M_bpos)
    // Totally aligned
    {
      // Move initial fragment, then move aligned words, and finally
      // move any trailing fragment.
      //
      UInt32 fraglen = _S_firstfrag (_M_bpos, cpySize);

      UInt32 idx=0;		// Track how much we've advanced thru

      if (fraglen < LONG_BIT)
        {
          UInt32 mask = _S_maskbit (_M_bpos, fraglen);
          mWords[0] &= ~mask;
          mWords[0] |= (src->mWords[0] & mask);
          idx++;
        }
      else
        fraglen = 0;	// No fractional part moved yet.

      // Now move remaining words.
      for(fraglen = cpySize - fraglen;
          fraglen >= LONG_BIT; fraglen -= LONG_BIT)
        {
          mWords[idx] = src->mWords[idx];
          idx++;
        }

      // Lastly move fragment
      if (fraglen > 0)
        {
          UInt32 mask = _S_maskbit (0, fraglen);
          mWords[idx] &= ~mask;
          mWords[idx] |= (src->mWords[idx] & mask);
        }
    }
  else
    // worst case access.
    // copy 32 bits at a time...
    {
      // Move a little piece to the destination to align it.
      UInt32 len = std::min (cpySize, (UInt32)(LONG_BIT - _M_bpos));
      reference dstTemp (*this, len); // Just enough to finish this word
      reference srcTemp (*src, len);

      UInt32 pieces = cpySize;
      if (_M_bpos)		// Dest not aligned
        {
          dstTemp = srcTemp.value ();
          pieces -= len;

          len = std::min (pieces, (UInt32)LONG_BIT);
          dstTemp.lpartsel (len);
          srcTemp.partsel (len);
        }

      // Destination is now ALIGNED.  So move words to destination.

      while (pieces >= LONG_BIT) {
        *dstTemp.mWords++ = srcTemp.value ();
        pieces -= LONG_BIT;
        srcTemp.partsel (LONG_BIT);
      }

      // There's possibly one fractional piece left
      if (pieces)
        {
          dstTemp._M_bsiz = pieces; // amount to deposit
          srcTemp._M_bsiz = pieces;
          dstTemp = srcTemp.value ();
        }
    }

  // Now we've moved all the src bits that would fit.  If the dest
  // size is still larger, we should zero-fill those bits.
  if (_M_bsiz > cpySize)
    {
      reference filler (&mWords[_S_whichword (_M_bpos + cpySize)],
                        _S_whichbit (_M_bpos + cpySize),
                        mNumBits - cpySize,
                        _M_bsiz - cpySize);
      filler = 0; // zero fill to end
    }
} // void DynBitVector::reference::anytoany

//! formatted output for DynBitVectors
void DynBitVector::format(UtString* buf, CarbonRadix radix)
  const
{
  format(buf, radix, getUIntArray (), size());
}

// formatted output for UInt32*,numBits
void DynBitVector::format(UtString* strBuf, CarbonRadix radix,
                          const UInt32* words, UInt32 numBits)
{
  int bufsiz = 0;
  char* buf = NULL;
  int success = 0;
  switch ( radix )
  {
  case eCarbonDec:
    bufsiz = (numBits+2)/3 + 1 + 1; // extra char for minus sign if negative
    buf = CARBON_ALLOC_VEC(char, bufsiz);
    success = CarbonValRW::writeDecValToStr(buf, bufsiz, words, true,
                                            numBits);
    break;
  case eCarbonUDec:
    bufsiz = (numBits+2)/3 + 1;
    buf = CARBON_ALLOC_VEC(char, bufsiz);
    success = CarbonValRW::writeDecValToStr(buf, bufsiz, words, false,
                                            numBits);
    break;
  case eCarbonHex:
    bufsiz = (numBits+3)/4 + 1;
    buf = CARBON_ALLOC_VEC(char, bufsiz);
    success = CarbonValRW::writeHexValToStr(buf, bufsiz, words, numBits);
    break;
  case eCarbonOct:
    bufsiz = (numBits+2)/3 + 1;
    buf = CARBON_ALLOC_VEC(char, bufsiz);
    success = CarbonValRW::writeOctValToStr(buf, bufsiz, words, numBits);
    break;
  case eCarbonBin:
    bufsiz = numBits + 1;
    buf = CARBON_ALLOC_VEC(char, bufsiz);
    success = CarbonValRW::writeBinValToStr(buf, bufsiz, words, numBits);
    break;
  };
  INFO_ASSERT(success != -1, "Unhandled radix.");
  *strBuf << buf;
  CARBON_FREE_VEC(buf, char, bufsiz);
} // void DynBitVector::format

bool DynBitVector::dbWrite(ZostreamDB& out) const
{
  out << cDynBitVectorSig;
  out << cDynBitVectorVersion;

  out << UInt32(mNumBits);
  const UInt32* arr = getUIntArray();
  for (size_t i = 0, nw = numWords(); i < nw; ++i)
    out << arr[i];
  return !out.fail();
}

bool DynBitVector::dbRead(ZistreamDB& in)
{
  UtString signature;
  if (! (in >> signature))
    return false;

  if (signature.compare(cDynBitVectorSig) != 0)
  {
    UtString buf;
    buf << "Invalid DynBitVector signature: " << signature;
    in.setError(buf.c_str());
    return false;
  }

  UInt32 version;
  if (! (in >> version))
    return false;
  
  if (version > cDynBitVectorVersion)
  {
    UtString buf;
    buf << "Unsupported DynBitVector version: " << version;
    in.setError(buf.c_str());
    return false;
  }
  
  size_t oldNumWords = numWords();
  UInt32 nbits;
  in >> nbits;
  UInt32 *optr = getUIntArray ();

  mNumBits = nbits;

  size_t newNumWords = numWords();
  if (oldNumWords != newNumWords)
  {
    if (oldNumWords > 1)
      CARBON_FREE_VEC(optr, UInt32, oldNumWords);
    if (newNumWords > 1)
      mWordUnion.mWords = CARBON_ALLOC_VEC(UInt32, newNumWords);
  }
  UInt32* ptr = getUIntArray ();
  for (UInt32 i = 0; i < newNumWords; ++i)
    in >> ptr[i];
  return !in.fail();
}


// Constructor from a bitvector
DynBitVector::reference::reference( const DynBitVector& __B, size_t __pos, size_t __siz ) {
  DynBitVector& __b = const_cast<DynBitVector&>(__B);
  mNumBits = __b.mNumBits;
  if (__pos >= mNumBits)
    {
      mWords = __b.getUIntArray ();
      _M_bpos = 0;
      _M_bsiz = 0;
      mNumBits = 0;
    }
  else if ((__pos + __siz) > mNumBits)
    {
      mWords = __b.getUIntArray ();
      _M_bpos = __pos;
      _M_bsiz = mNumBits - __pos;
    }
  else
    {
      mWords = &__b._M_getword(__pos);
      _M_bpos = _S_whichbit(__pos);
      _M_bsiz = __siz;
      mNumBits -= (__pos / LONG_BIT);
    }
}

//! Constructor changing size
DynBitVector::reference::reference (const reference& r, size_t size):
  mWords (r.mWords), _M_bpos (r._M_bpos), mNumBits (r.mNumBits), _M_bsiz (size) {}

//! Constructor from simple array of UInt32
/*
 * In this case we assume that the pos/size are within the actual
 * memory, as we have no other guarantees.
 */
DynBitVector::reference::reference (UInt32 *wp, size_t pos, size_t nb, size_t siz):
  mWords(wp), _M_bpos(pos), mNumBits(nb), _M_bsiz(siz) {}

// Copy Constructor
DynBitVector::reference::reference (const reference& r):
  mWords (r.mWords), _M_bpos (r._M_bpos), mNumBits(r.mNumBits), _M_bsiz (r._M_bsiz) {}

// Default destructor
DynBitVector::reference::~reference() {}


//! Generalized extended precision addition.
void DynBitVector::_M_do_add(const DynBitVector& __x) {
  UInt64 sum = 0;
  for (BVPairIter i(this, &__x); !i.atEnd(); ++i)
    {
      sum += (UInt64) i.w1() + (UInt64) i.w2();
      i.w1() = UInt32(sum);
      sum >>= __BITS_PER_WORDT;
    }
}

//! \overload
void DynBitVector::_M_do_add (UInt32 rval) {
  UInt32* p = getUIntArray ();
  UInt64 sum = rval;
  for ( size_t __i = 0; __i < numWords(); __i++ )
    {
      sum += (UInt64) p[__i];
      p[__i] = UInt32(sum);
      sum >>= __BITS_PER_WORDT;
    }
}

//! Generalized extended precision subtraction
void DynBitVector::_M_do_sub (const DynBitVector& __x) {
  SInt64 diff = 0;
  for (BVPairIter i(this, &__x); !i.atEnd(); ++i)
    {
      diff += (UInt64) i.w1() - (UInt64) i.w2();
      i.w1() = UInt32(diff);
      diff = (diff<0) ? UtUINT64_MAX : UtUINT64_MIN;
    }
}

//! \overload
void DynBitVector::_M_do_sub (UInt32 rval) {
  SInt64 diff = rval;

  diff = -diff;
  UInt32 *p = getUIntArray ();
  for ( size_t __i = 0; __i < numWords(); __i++ )
    {
      diff += (UInt64) p[__i];
      p[__i] = UInt32(diff);
      diff = (diff<0) ? UtUINT64_MAX : UtUINT64_MIN;
    }
}


//! Unary negation
void DynBitVector::_M_do_neg (void) {
  UInt32* p = getUIntArray ();
  SInt64 temp = 0;
  for (size_t __i = 0; __i < numWords(); ++__i)
    {
      temp -= (UInt64) p[__i];
      p[__i] = UInt32(temp);
      temp = (temp<0) ? UtUINT64_MAX : UtUINT64_MIN;
    }
}

//! Verilog Std. says only multiply by power of two is legal
void DynBitVector::_M_do_mul (const size_t n) {
  if (n == 0)
    return _M_do_reset ();

  size_t shifts = sExactLog2 (n);

  return _M_do_left_shift (shifts);
}

//! divide operator
void DynBitVector::_M_do_div (const size_t n) {
  INFO_ASSERT(n!=0, "Divide by zero.");

  size_t shifts = sExactLog2 (n);
  return _M_do_right_shift (shifts);
}

// integer divide 
void DynBitVector::integerDiv(const DynBitVector& divider, DynBitVector* remainder)
{

  if (divider == 1)
  {
    *remainder = 0;
    return;
  }
  INFO_ASSERT(divider != 0, "Divide by zero.");

  if (*this < divider)
  {
    *remainder = *this;
    reset();
  }
  else
  {
    DynBitVector result(size());
    while (*this >= divider)
    {
      *this -= divider;
      result += 1;
    }
    
    *remainder = *this;
    *this = result;
  }
}

// integer divide 
void DynBitVector::integerMult(const DynBitVector& multiplier)
{
  carbon_multiply (this->getUIntArray (), multiplier.getUIntArray (), this->getUIntArray (),
                   this->numWords (), multiplier.numWords (), this->numWords ());
}

// fast multiply-by-10.  We should make integerMult in general
// be about this fast.
void DynBitVector::multBy10() {
  // the *2 is easy
  *this <<= 1;
  // The *5 is slightly harder: 5*x = 4*x + x
  DynBitVector tmp(*this);
  *this <<= 2;
  *this += tmp;
}

//! logical and
void DynBitVector::_M_do_and(const DynBitVector& __x) {
  for (BVPairIter i(this, &__x); !i.atEnd(); ++i)
    i.w1() &= i.w2(0xFFFFFFFF);
}

//! logical or
void DynBitVector::_M_do_or(const DynBitVector& __x) {
  for (BVPairIter i(this, &__x); !i.atEnd(); ++i)
    i.w1() |= i.w2();
}

//! logical xor
void DynBitVector::_M_do_xor(const DynBitVector& __x) {
  for (BVPairIter i(this, &__x); !i.atEnd(); ++i)
    i.w1() ^= i.w2();
}

// Optimize special case X[i] = ~X[i]
void DynBitVector::_M_do_flip() {
  UInt32* p = getUIntArray ();
  for ( size_t __i = 0; __i < numWords(); __i++ ) {
    p[__i] = ~p[__i];
  }
}

//! Unsigned compare \returns -1,0,1 (LTU,EQ,GTU)
int DynBitVector::compare(const DynBitVector& x) const {
  for (ConstBVPairIter i (this, &x); not i.atEnd(); --i) {
    if (i.w1() > i.w2())
      return 1;
    else if (i.w1() < i.w2())
      return -1;
  }
  return 0;		// must have been ==
}

// Fast set/reset operations
void DynBitVector::_M_do_set() {
  memset (getUIntArray (), -1, sizeof (UInt32)*numWords());
}

void DynBitVector::_M_do_reset() {
  memset (getUIntArray (), 0, sizeof (UInt32)* numWords());
}

inline UInt32 DynBitVector::_M_do_to_ulong() const {
  return *getUIntArray ();
}

void DynBitVector::negate()
{
  _M_do_neg ();
  _M_do_sanitize ();
}

//! b[pos][size]
DynBitVector::reference& DynBitVector::reference::operator[](size_t __siz){
  _M_bsiz = __siz;
  return *this;
}

//! b[pos][size]
const DynBitVector::reference&
DynBitVector::reference::operator[](size_t __siz) const {
  _M_bsiz = __siz;
  return *this;
}

//! functional converter from reference to representable int
DynBitVector::reference::operator UInt64 () const
{
  if (_M_bsiz == 1)
    return ((_S_maskbit(_M_bpos)) & *mWords) != 0;

  // Lazy conversion - do anytoany move
  size_t bitsize = std::min (__BITS_PER_WORDT*2, _M_bsiz);
  UInt32 temp[2]={0,0};
  reference dst (&temp[0], 0, mNumBits, bitsize);
  reference src (mWords, _M_bpos, mNumBits, bitsize);

  dst.anytoany (&src);

  // And convert to host-formatted 64 bits
  return ((UInt64) temp[0])
    | ((UInt64)temp[1]<< __BITS_PER_WORDT);
}

//! functional converter from reference to representable int
UInt32 DynBitVector::reference::value() const
{
  if (_M_bsiz == 1)
    return (*(mWords) & _S_maskbit(_M_bpos)) != 0;

  INFO_ASSERT(_M_bsiz <= __BITS_PER_WORDT, "Reference size out-of-bounds.");

  int fraglen = std::min (__BITS_PER_WORDT - _M_bpos, _M_bsiz);

  UInt32 value = UInt32((*mWords >> _M_bpos) & ((KUInt64(1)<<fraglen)-1));
  
  fraglen = _M_bsiz - fraglen;
  if (fraglen > 0)
    value |= (mWords[1] & ((1<<fraglen)-1)) << (__BITS_PER_WORDT - _M_bpos);

  return value;
}

UInt64 DynBitVector::reference::llvalue() const
{
  if (_M_bsiz == 1)
    return (*(mWords) & _S_maskbit(_M_bpos)) != 0;

  INFO_ASSERT(_M_bsiz <= (__BITS_PER_WORDT*2), "Reference size out-of-bounds");

  return (UInt64) mWords[0] | ((UInt64) mWords[1]<< __BITS_PER_WORDT);
}

// Invert a range of bits!
DynBitVector::reference& DynBitVector::reference::flip() {
  int fraglen = std::min (_M_bsiz, __BITS_PER_WORDT - _M_bpos);

  UInt32 fragMask = _S_maskbit (_M_bpos, fraglen);
  *mWords ^= fragMask;

  fraglen = _M_bsiz - fraglen;
  int i;
  for(i=1; fraglen >= (int)__BITS_PER_WORDT; ++i,fraglen -= __BITS_PER_WORDT)
    {
      mWords[i] ^= ~0;
    }

  if (fraglen) {
    fragMask = _S_maskbit (0, fraglen);
    mWords[i] ^= fragMask;
  }
  return *this;
}

//! reference equalities...
bool DynBitVector::reference::operator==(const reference& r) const
{
  // Check for easy case...
  if (this->_M_bpos == r._M_bpos) // Aligned?
    {
      int fraglen = std::min (__BITS_PER_WORDT - _M_bpos, _M_bsiz);
      UInt32 mask = _S_maskbit (_M_bpos, fraglen);
      if (((*mWords)&mask) != ((*r.mWords)&mask))
        return false;

      // No differences found
      return true;
    }

  // Need any-to-any bit comparisons. Copy and compare them.
  DynBitVector lop (mNumBits, mWords, __BITVECTOR_WORDS (_M_bpos+_M_bsiz));
  DynBitVector rop (mNumBits, r.mWords, __BITVECTOR_WORDS (r._M_bpos + r._M_bsiz));
  lop >>= _M_bpos;
  rop >>= r._M_bpos;
  return lop[0][_M_bsiz] == rop[0][r._M_bsiz];
}

//! relative advance of reference to new [p][s] access
DynBitVector::reference& DynBitVector::reference::lpartsel (size_t size) {
  // since we are advancing in contigous pieces, we should always
  // see the new pos as equal to the old size.
  mWords += _S_whichword (_M_bpos + _M_bsiz);
  _M_bpos = _S_whichbit (_M_bpos + _M_bsiz);
  mNumBits -= _M_bsiz;
  _M_bsiz = size;

  return *this;
}

const DynBitVector::reference& DynBitVector::reference::partsel (size_t size) {
  // since we are advancing in contigous pieces, we should always
  // see the new pos as equal to the old size.
  mWords += _S_whichword (_M_bpos + _M_bsiz);
  _M_bpos = _S_whichbit (_M_bpos + _M_bsiz);
  mNumBits -= _M_bsiz;
  _M_bsiz = size;

  return *this;
}

//! Deposit masked
void DynBitVector::reference::deposit (UInt32 value, size_t pos, size_t siz)
{
  UInt32 mask = _S_maskbit (pos, siz);
  *mWords &= ~mask;		// Clear destination
  *mWords |= (value & mask);
}

//! Any bits in the range non-zero?
bool DynBitVector::reference::any (void) const
{
  int fraglen = std::min (_M_bsiz, __BITS_PER_WORDT - _M_bpos);

  if (*mWords & _S_maskbit (_M_bpos, fraglen))
    return true;

  fraglen=_M_bsiz - fraglen;

  int i;
  for(i=1; fraglen >= (int)__BITS_PER_WORDT; ++i,fraglen -= __BITS_PER_WORDT)
    {
      if (mWords[i])
        return true;
    }

  if (fraglen)
    return (mWords[i] & _S_maskbit (0, fraglen)) != 0;

  return false;
}


//! All bits in the range are set?
bool DynBitVector::reference::all (void) const
{
  int fraglen = std::min (_M_bsiz, __BITS_PER_WORDT - _M_bpos);

  UInt32 fragMask = _S_maskbit (_M_bpos, fraglen);
  if ((*mWords & fragMask) != fragMask)
    return false;               // some bit was zero

  fraglen=_M_bsiz - fraglen;
  int i;
  for(i=1; fraglen >= (int)__BITS_PER_WORDT; ++i,fraglen -= __BITS_PER_WORDT)
    {
      if (mWords[i] != UInt32 (-1))
        return false;           // whole word not -1
    }

  if (fraglen) {
    fragMask = _S_maskbit (0, fraglen);
    return (mWords[i] & fragMask) != fragMask;  // A

  }
  return true;
}


//! count bits in the range
size_t DynBitVector::reference::count (void) const
{
  int fraglen = std::min (_M_bsiz, __BITS_PER_WORDT - _M_bpos);
  size_t cnt =  popcount(*mWords & _S_maskbit (_M_bpos, fraglen));

  fraglen=_M_bsiz - fraglen;

  for(int i=1; fraglen > 0; ++i,fraglen -= __BITS_PER_WORDT)
    {
      if (fraglen >= (int)__BITS_PER_WORDT)
        cnt += popcount (mWords[i]);
      else
        cnt += popcount (mWords[i] & _S_maskbit (0, fraglen));
    }

  return cnt;
}

//! reduction xor bits in the range
size_t DynBitVector::reference::redxor (void) const
{
  int fraglen = std::min (_M_bsiz, __BITS_PER_WORDT - _M_bpos);
  size_t xorval =  mWords[0] & _S_maskbit (_M_bpos, fraglen);

  fraglen=_M_bsiz - fraglen;

  for(int i=1; fraglen > 0; ++i,fraglen -= __BITS_PER_WORDT)
    {
      if (fraglen >= (int)__BITS_PER_WORDT)
        xorval ^= (mWords[i]);
      else
        xorval ^= (mWords[i] & _S_maskbit (0, fraglen));
    }

  return redxor_word (xorval);
}

//! for:	 b[i] = __x;
//!  or:	 b[i][siz] = __x;
/*!
 * \em Warning: assignment doesn't change the reference members
 */
DynBitVector::reference& DynBitVector::reference::operator=(UInt32 __x) {
  if (_M_bsiz > 1)
    {
      int fraglen = std::min (_M_bsiz, __BITS_PER_WORDT - _M_bpos);
      if (size_t (fraglen) < __BITS_PER_WORDT)
        {
          UInt32 mask = _S_maskbit (_M_bpos, fraglen);
          *mWords &= ~mask;
          *mWords |=  (__x<<_M_bpos) & mask;
          __x >>= fraglen;
        }
      else { // full word move
        *mWords = __x;
        __x = 0;
      }

      fraglen = _M_bsiz - fraglen;

      int i=1;
      for(; fraglen >= (int) __BITS_PER_WORDT; ++i, fraglen -= __BITS_PER_WORDT)
        // Do all fullword moves
        {
          mWords[i] |= __x;
          __x = 0;
        }

      // One last fragment?
      if (fraglen != 0)
      {
        UInt32 mask = _S_maskbit (0, fraglen);
        mWords[i] &= ~mask;
        mWords[i] |= __x & mask;
      }

    }
  else if (_M_bsiz == 1)
    // special case for single bit
    if ( __x )
      *mWords |= _S_maskbit(_M_bpos);
    else
      *mWords &= ~_S_maskbit(_M_bpos);
  else if (_M_bsiz == 0)
    // Nothing to do if destination is zero length.
    (void)0;
  return *this;
}


//!  b[i][size] = b[__j][size];
DynBitVector::reference& DynBitVector::reference::operator=(const reference& __j) {
  if (_M_bsiz == 1)
    if ( (*(__j.mWords) & _S_maskbit(__j._M_bpos)) )
      *mWords |= _S_maskbit(_M_bpos);
    else
      *mWords &= ~_S_maskbit(_M_bpos);
  else if (_M_bsiz > 1)
    // store field, really b[i][siz] = b[j][siz];
    //
    // (do we need to worry about overlaps? - ANS: YES!)
    anytoany(&__j);

  return *this;
}

//! b[i][j] = bitUtVector<K>;
//
// Note that K should be equal to the field size
// 
DynBitVector::reference& DynBitVector::reference::operator=(const DynBitVector& __j) {
  const reference src(const_cast<UInt32*>(__j.getUIntArray ()), 0, __j.mNumBits,
                      std::min (__j.mNumBits, _M_bsiz)); // Don't read more than dst can hold

  anytoany(&src);
  return *this;
}

// Need a whole bunch of other operators to do anything
// complex with references beyond just copying them around.
//
// This is going to be a LOT of work to support, as we need
//   reference <OP> (reference|BitVector|int)
//
// Probably the right thing is to generate BitVectors  for the inputs
// and do a BitVector<K>::operator+= with a scratch temporary.
// This is far from optimal for something like:
//		reg[31:16] += 1;
// where we could translate to
//		BitVector<reg.size()> temp(1<<16);
//		reg += temp;


DynBitVector::reference& DynBitVector::reference::operator+=(UInt32 rval){
  UInt64 sum = (UInt64)rval << _M_bpos;
  size_t fraglen = (_M_bpos + _M_bsiz);

  for(int i = fraglen, indx=0; sum != 0 && i>0; i-=__BITS_PER_WORDT, indx++)
    {
      sum += (UInt64)mWords[indx];
      if ((UInt32)i < __BITS_PER_WORDT)
        {
          UInt32 mask = _S_maskbit (0,i);
          mWords[indx] &= ~mask;
          mWords[indx] |= UInt32(sum & mask);
        }
      else
          mWords[indx] = (UInt32)sum;

      sum >>= __BITS_PER_WORDT;
    }

  return *this;
}

DynBitVector::reference& DynBitVector::reference::operator-=(UInt32 rval){
  SInt64 diff = (UInt64)rval << _M_bpos;
  size_t fraglen = (_M_bpos + _M_bsiz);

  for(int i = fraglen, indx=0; diff != 0 && i > 0; i-=__BITS_PER_WORDT, indx++)
    {
      diff = (UInt64)mWords[indx] - diff;

      if (i < (int)__BITS_PER_WORDT)
        // last chunk smaller than word
        {
          UInt32 mask = _S_maskbit (0,i);
          mWords[indx] &= ~mask;
          mWords[indx] |= UInt32(diff & mask);
        }
      else
          mWords[indx] = (UInt32)diff;

      diff >>= __BITS_PER_WORDT;
      // Must propagate borrow correctly!
      if (diff & _S_maskbit (__BITS_PER_WORDT-1))
        diff |= KUInt64(0xFFFFFFFF00000000);
      diff = -diff;
    }

  return *this;
}

// bitwise operators are easier - their results don't propagate.
DynBitVector::reference& DynBitVector::reference::operator|=(UInt32 rval){
  size_t masklen = std::min (_M_bsiz, __BITS_PER_WORDT);

  UInt64 dval = rval & _S_maskbit (0, masklen);

  mWords[0] |= UInt32(dval << _M_bpos);
  if ((_M_bpos + _M_bsiz) >= __BITS_PER_WORDT)
    mWords[1] |= UInt32(dval >> (__BITS_PER_WORDT-_M_bpos));
  
  return *this;
}

DynBitVector::reference& DynBitVector::reference::operator^=(UInt32 rval){
  size_t masklen = std::min (_M_bsiz, __BITS_PER_WORDT);

  UInt64 dval = rval & _S_maskbit (0, masklen);

  mWords[0] ^= UInt32(dval << _M_bpos);
  if ((_M_bpos + _M_bsiz) >= __BITS_PER_WORDT)
    mWords[1] ^= UInt32(dval >> (__BITS_PER_WORDT-_M_bpos));

  return *this;
}

DynBitVector::reference& DynBitVector::reference::operator&=(UInt32 rval){
  size_t masklen = std::min (_M_bsiz, __BITS_PER_WORDT);

  // Create mask with hole where value goes
  UInt64 mask = ~(((static_cast<UInt64>(1) << masklen) - 1) << _M_bpos);

  // or in bits from rvalue to make composite mask
  mask |= ((UInt64)(rval) << _M_bpos);

  mWords[0] &= UInt32(mask);

  if ((_M_bpos + _M_bsiz) >= __BITS_PER_WORDT)
    mWords[1] &= UInt32(mask >> __BITS_PER_WORDT);
  
  return *this;
}

bool DynBitVector::reference::operator!() const{
  if (_M_bsiz == 1)
    return (mWords[0] & _S_maskbit (_M_bpos)) == 0;
  else
    return !this->any ();
}

//! flips the bit
bool DynBitVector::reference::operator~() const {
  if (_M_bsiz == 1)
    return (*(mWords) & _S_maskbit(_M_bpos)) == 0;
  else
    return (this->count () != _M_bsiz);
}

UtOStream& operator<<(UtOStream& f, const DynBitVector& bv)
{
  f.formatBitvec(bv);
  return f;
}

bool DynBitVector::getContiguousRange(UInt32* startBit, UInt32* sz) const
{
  bool contiguous = true;
  *startBit = 0;
  *sz = 0;
  enum SearchState {
    eStart,
    eFirstBitFound,
    eLastBitFound
  };
  SearchState searchState = eStart;

  const UInt32* p = getUIntArray ();
  SInt32 n = mNumBits / 32; // first, operate on all whole words
  for (SInt32 i = 0; (i < n) && contiguous; ++i) {
    if (p[i] == 0xFFFFFFFF) {
      // whole word is set.
      switch(searchState){ 
      case eStart:
        (*startBit) = i * 32;   // the first bit for the i-th word
        (*sz)      += 32;       // number of bits per word.
        searchState = eFirstBitFound;
        break;
      case eFirstBitFound:
        (*sz) += 32;            // number of bits per word.
        break;
      case eLastBitFound:
        contiguous = false;
        break;
      }
    } else if(p[i] == 0) {
      // whole word is unset.
      switch(searchState) {
      case eStart:
        // nothing.
        break;
      case eFirstBitFound:
        searchState = eLastBitFound;
        break;
      case eLastBitFound:
        // nothing.
        break;
      }
    } else {
      SInt32 first_bit = i*32;
      SInt32 last_bit  = first_bit + 32;
      for (SInt32 j = first_bit; (j < last_bit) && contiguous; ++j) {
        if (test(j)) {
          // bit is set.
          switch (searchState) {
          case eStart:
            *startBit = j;
            searchState = eFirstBitFound;
            *sz = 1;
            break;
          case eFirstBitFound:
            ++(*sz);
            break;
          case eLastBitFound:
            contiguous = false;
            break;
          }
        } else {
          // bit is not set.
          switch (searchState) {
          case eStart:
            // nothing.
            break;
          case eFirstBitFound:
            searchState = eLastBitFound;
            break;
          case eLastBitFound:
            // nothing.
            break;
          }
        }
      }
    }
  }

  // if the last word is only partially used, handle it on a per-bit basis
  SInt32 first_bit = n*32;
  SInt32 last_bit  = mNumBits;
  for (SInt32 j = first_bit; (j < last_bit) && contiguous; ++j) {
    if (test(j)) {
      // bit is set.
      switch (searchState) {
      case eStart:
        *startBit = j;
        searchState = eFirstBitFound;
        *sz = 1;
        break;
      case eFirstBitFound:
        ++(*sz);
        break;
      case eLastBitFound:
        contiguous = false;
        break;
      }
    } else {
      // bit is not set.
      switch (searchState) {
      case eStart:
        // nothing.
        break;
      case eFirstBitFound:
        searchState = eLastBitFound;
        break;
      case eLastBitFound:
        // nothing.
        break;
      }
    }
  }

  return contiguous;
} // bool DynBitVector::getContiguousRange


bool DynBitVector::all() const {
  SInt32 n = numWords() - 1;
  const UInt32* p=getUIntArray ();
  for (SInt32 i = 0; i < n; ++i)
    if (p[i] != 0xFFFFFFFF)
      return false;

  UInt32 lastWord = 0xFFFFFFFF;
  size_t extraBits = mNumBits % __BITS_PER_WORDT;
  if (extraBits != 0)
    lastWord &= ~((~static_cast<UInt32>(0)) << extraBits);

  return (p[n] == lastWord);
}

void DynBitVector::composeBits(UtString* buf) const {
  bool first = true;
  for (UInt32 i = 0, s = size(); i < s; ++i) {
    if (test(i)) {
      if (not first) {
        *buf << ", ";
      }
      *buf << i;
      first = false;
    }
  }
}


// Returns first non-zero bit, or zero if none found...
UInt32 DynBitVector::findFirstOne() const
{
  const UInt32 *p = getUIntArray ();
  UInt32 nw = numWords ();
  for (UInt32 i=0; i<nw; ++i)
  {
    SInt32 pos = carbon_FFO (*p++);
    if (pos >=0)
    {
      pos += i*LONG_BIT;
      return pos;
    }
  }
  return 0;
}

// Returns last non-zero bit found or last bitpos if none found
UInt32 DynBitVector::findLastOne() const
{
  const UInt32 *p = getUIntArray ();
  for (SInt32 i = numWords ()-1; i >= 0; --i)
  {
    SInt32 pos = carbon_FLO (p[i]);
    if (pos >=0)
    {
      pos += (i)*LONG_BIT;
      return pos;
    }
  }
  return mNumBits-1;
}


DynBitVector::RangeLoop::RangeLoop(const DynBitVector& bv, bool onesAndZeros)
  : mBitVector(bv),
    mNextBit(0),
    mNextSize(0),
    mOnesAndZeros(onesAndZeros)
{
  mCurrentState = true;         // kick off setup() in by getting a new range
  next();
}

void DynBitVector::RangeLoop::next() {
  UInt32 rangeStart;
  if (mCurrentState) {
    (void) mBitVector.getContiguousRange(&rangeStart, &mNextSize);
    if (mNextSize != 0) {
      // clear out the contiguous range we just found so that the next
      // call to getContiguous gets the *next* range
      mBitVector.setRange(rangeStart, mNextSize, 0);
    }
  }

  mCurrentRange.setLsb(mNextBit);

  if (mNextSize == 0) {        // all 0s remaining
    if (mCurrentState) {
      // report the 0s, if there are any
      mCurrentState = false;
      mNextBit = mBitVector.size();
    }
    else {
      // We are all out of bits.  Make mCurrentRange invalid

      mNextBit = mBitVector.size();
      mCurrentRange.setLsb(mNextBit);
    }
  }
  else if (mCurrentState) {
    if (mNextBit == rangeStart) {
      // Only happens when bv[0] is 1
      INFO_ASSERT(mNextBit == 0, "RangeLoop logic error");
      mNextBit = rangeStart + mNextSize;
    }
    else {
      // Reporting the 0s leading up to this contiguous range
      mCurrentState = false;
      mNextBit = rangeStart;
    }
  }
  else {
    // Report the 1s previously found
    mCurrentState = true;
    mNextBit += mNextSize;
  }
  
  mCurrentRange.setMsb(mNextBit - 1);

  // If the user is only interested in ranges of 1s, and we are
  // about to report a range of 0s, then do it again
  if (!mCurrentState && !mOnesAndZeros && !atEnd()) {
    next();
  }
}

DynBitVector::RangeLoop::RangeLoop(const RangeLoop& src)
  : mBitVector(src.mBitVector),
    mCurrentRange(src.mCurrentRange),
    mNextBit(src.mNextBit),
    mNextSize(src.mNextSize),
    mCurrentState(src.mCurrentState),
    mOnesAndZeros(src.mOnesAndZeros)
{
}

DynBitVector::RangeLoop&
DynBitVector::RangeLoop::operator=(const RangeLoop& src)
{
  if (&src != this) {
    mBitVector = src.mBitVector;
    mCurrentRange = src.mCurrentRange;
    mNextBit = src.mNextBit;
    mCurrentState = src.mCurrentState;
    mOnesAndZeros = src.mOnesAndZeros;
  }
  return *this;
}

DynBitVector::RangeLoop::~RangeLoop()
{
}
