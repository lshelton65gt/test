// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtShellTok.h"
#include "util/UtStringUtil.h"
#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"

static const char sDelims[] = " \t\r\n";

UtShellTok::UtShellTok(const char* str, bool envVars, const char* delims, bool specialChars)
  : mEOF(false),
    mEnvVars(envVars),
    mSpecialChars(specialChars),
    mStatus(eOk)
{
  if (delims)
    mDelims = delims;
  else
    mDelims = sDelims;
  resetStr(str);
  if (str != NULL)
    ++(*this);
}

// have we come to the end of the string?
bool UtShellTok::atEnd() const {return mNextTok == NULL;}

// get the current token
const char* UtShellTok::operator*() const {
  return mToken.c_str();
}

// parse the current token as a based number, returning true for success
bool UtShellTok::parseNumber(UInt32* number, int base)
  const
{
  return StringUtil::parseNumber(**this, number, base);
}


// advance to the next token
void UtShellTok::operator++()
{
  mProtectedTok = false;
  // skip leading whitespace
  mNextTok = StringUtil::skip(mNextTok, mDelims.c_str());

  // Check for end of string
  if (*mNextTok == '\0')
    mNextTok = NULL;
  else
  {
    // find end ptr
    mStr = mNextTok;

    mInSQuote = false;
    mInDQuote = false;
    mToken.clear();
    scanInput();
  }
}

void UtShellTok::scanInput() {
  while ((*mNextTok != '\0') &&
         (mInSQuote || mInDQuote || 
          (std::strchr(mDelims.c_str(), *mNextTok) == NULL)))
  {
    // Check if we should do something with special characters like quotes, and backslashes
    bool insert = true;
    if (mSpecialChars) {
      if (*mNextTok == '\\') {
        ++mNextTok;             // insert next next character unconditionally
        char c = *mNextTok ;
        switch (c) {
          case 't': mToken << "\t";  break;
          case 'n': mToken << "\n";  break;
          case 'r': mToken << "\r";  break;
          case '\0': --mNextTok;     break;
          default:  mToken << c;     break;
        }
        insert = false;

      } else if (mInSQuote) {
        if (*mNextTok == '\'') {
          mInSQuote = false;
          insert = false;
        }

      } else if (mInDQuote) {
        if (*mNextTok == '"') {
          mInDQuote = false;
          insert = false;
        }

      } else if (*mNextTok == '\'') {
        mInSQuote = true;
        insert = false;
        mProtectedTok = true;

      } else if (*mNextTok == '"') {
        mInDQuote = true;
        insert = false;
        mProtectedTok = true;
      }
    } // if

    // Transfer the character to the token if requested and go to the next character
    if (insert) {
      mToken << *mNextTok;
    }
    ++mNextTok;
  }
  mEOF = (*mNextTok == '\0');
} // void UtShellTok::scanInput

bool UtShellTok::isProtected() const {
  return mProtectedTok && !mInDQuote && !mInSQuote;
}

void UtShellTok::parseString(const char* ptr) {
  INFO_ASSERT(mEOF, "Do not call parseString until you have run out of chars");
  resetStr(ptr);
  scanInput();
}

// check for the end, get the current token, advance to the next one
bool UtShellTok::operator()(const char** ptr) {
  if (atEnd())
    return false;
  *ptr = **this;
  ++(*this);
  return true;
}

//! get the current position in the string
const char* UtShellTok::curPos() {return mStr;}

// check if a comment is contained in the string
void getComments(const char* str, UtHashMap<const void*, bool>& inComment)
{
  const char* pos = str;
  bool commentOn = false;
  bool multilineCommentOn = false;

  while (*pos != '\0')
  {
    if (*pos == '#' && ! multilineCommentOn)
    {
      commentOn = true;
    }
    else if (*pos == '/')
    {
      if (*(pos+1) == '/' && ! multilineCommentOn)
      {
	commentOn = true;
      }
      else if (*(pos+1) == '*' && ! commentOn)
      {
	multilineCommentOn = true;
      }
    }
    else if (multilineCommentOn && *pos == '*' && *(pos+1) == '/')
    {
      multilineCommentOn = false;
    }

    inComment[pos] = commentOn || multilineCommentOn;

    pos++;
  }
}

void UtShellTok::resetStr(const char* str)
{
  mStr = NULL;

  // Translate environment variables, leaving any that are illegally
  // specified intact including the '$', which I think
  // will make it easier to debug if something goes wrong.  If it
  // is legally specified, but undefined, an error will be issued
  UtHashMap<const void*, bool> inComment;
  getComments(str, inComment);
  const char* dollar = NULL;
  mBuffer.clear();
  mErrorBuf.clear();
  while ((dollar = std::strchr(str, '$')) != NULL) {
    // Insert all the text up to the dollar, but not including it
    mBuffer.append(str, dollar - str);
    str = dollar + 1;

    // If there is a comment, make sure the envVar is before the comment
    if (inComment[dollar]) 
    {
      mBuffer << '$';
      continue;
    }

    UtString envVar;
    const char* envVarPtr = str;
    if (*envVarPtr == '{') {
      const char* envVarEnd = std::strchr(envVarPtr, '}');
      if (envVarEnd != NULL) {
        str = envVarEnd + 1;
        envVar.append(envVarPtr + 1, str - envVarPtr - 2);
      }
      else {
	mErrorBuf << "Missing '}' in environment variable specification.";
	mStatus = eError;
	mBuffer << '$';
      }
    }
    else if (*envVarPtr == '(') {
      const char* envVarEnd = std::strchr(envVarPtr, ')');
      if (envVarEnd != NULL) {
	str = envVarEnd + 1;
	UtString var;
	var.append(envVarPtr + 1, str - envVarPtr -2);
	if (mEnvVars) {
	  mErrorBuf << "If $("<< var << ") was supposed to be an environment variable,";
	  mErrorBuf << "it must be delimited with curly-braces: ${" << var << "}.";
	  mStatus = eNote;
	}
	mBuffer << "$(" << var << ")";
      }
      else {
	mBuffer << '$';
      }
    }
    else {
      // It was specified like $FOO/x.  Look for first non-alpha
      // after $ to terminate
      for (const char* p = envVarPtr; *p != '\0'; ++p) {
        if (!isalnum(*p) && (*p != '_')) {
          envVar.append(envVarPtr, p - envVarPtr);
          str = p;
          break; 
        }
      }
    }

    if (! envVar.empty()) {
      const char* envVal = ::getenv(envVar.c_str());
      if (envVal != NULL) {
	if (mEnvVars) {
	  mBuffer << envVal;
	}
	else {
	  mBuffer << "${" << envVar << "}";
	}
      }
      else {
	mErrorBuf.clear();
	mStatus = eError;
	mErrorBuf << "Environment variable '" << envVar << "' is not defined.";
      }
    }
  }

  mBuffer << str;

  mNextTok = mBuffer.c_str();
} // void UtShellTok::resetStr
  


//! given a string that needs to be interpreted as a token, quote it for
//! csh if it needs embedded whitespace
const char* UtShellTok::quote(const char* str, UtString* buf, bool always_quote, const char* delims)
{
  bool needQuote = always_quote || (*str == '\0');
  bool hasDQuote = false;
  bool hasSQuote = false;
  char quoteChar = '\0';
  if (delims == NULL)
    delims = sDelims;

  buf->clear();

  for (const char* s = str; !needQuote && (*s != '\0'); ++s) {
    needQuote = std::strchr(delims, *s) != NULL;
    hasDQuote |= (*s == '"');
    hasSQuote |= (*s == '\'');
  }

  // The entire string will need to be quoted if
  // there are any delims in the string.
  // If the string only has a single quote, use
  // the double quote for quoting. 
  // Otherwise, quote with the single quote.
  if (needQuote) {
    if (hasSQuote && !hasDQuote) {
      quoteChar = '"';
    }
    else {
      quoteChar = '\'';
    }
    *buf << quoteChar;
  }

  for (const char* s = str; *s != '\0'; ++s) {
    char c = *s;
    switch (*s) {
    case '\t':   *buf << "\\t";  break;
    case '\n':   *buf << "\\n";  break;
    case '\r':   *buf << "\\r";  break;
    case ',':
      // Commas need to be escaped too, specifically
      // for the Verific ProcessFFile method, which
      // does not allow unescaped commas.
    case '"':
    case '\'':
      // Escape single and double quotes. Previously,
      // we only escaped quotes if it was not quoteChar.
      // But this counfounded the Verific ProcessFFile
      // method, so now we escape both kinds of quotes.
      *buf << "\\";  
      *buf << c;
      break;
    case '\\':                     *buf << '\\';   // no break
    default:                       *buf << c;      break;
    }
  }

  // empty strings require a quote
  if ((quoteChar != '\0') || buf->empty()) {
    *buf << quoteChar;
  }
  return buf->c_str();
} // const char* UtShellTok::quote

UInt32 UtShellTok::numConsumed() const {
  INFO_ASSERT(!mEnvVars,
              "Cannot determine consumption if env vars are allowed");
  INFO_ASSERT(mNextTok > mBuffer.c_str(), "numConsumed is undefined");
  return mNextTok - mBuffer.c_str();
}  // UInt32 UtShellTok::numConsumed

UtShellTok::Status UtShellTok::getResetStrStatus() const {
  return mStatus;
} // UtShellTok::Status UtShellTok::getResetStrStatus

void UtShellTok::clearResetStrStatus() {
  mStatus = eOk;
} // UtShellTok::Status UtShellTok::clearResetStrStatus

const char* UtShellTok::getErrorBuffer() const {
  return mErrorBuf.c_str();
} // const char* errorBuffer
