// -*- C++ -*-
/*****************************************************************************


 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/UtLicenseWrapper.h"
#include "util/UtCLicense.h"

// C interface to license checkout
extern "C" CarbonLicenseData * UtLicenseWrapperCheckout(const char *featureName, const char *featureDescription)
{
  return (CarbonLicenseData *) new UtLicenseWrapper(featureName, featureDescription);
}

// C interface to license heartbeat
extern "C" void UtLicenseWrapperHeartbeat(CarbonLicenseData *pLicense)
{
  UtLicenseWrapper * wrapper = (UtLicenseWrapper *) pLicense;
  wrapper->heartbeat();
}

// C interface to license release
extern "C" void UtLicenseWrapperRelease(CarbonLicenseData **ppLicense)
{
  UtLicenseWrapper * wrapper = (UtLicenseWrapper *) *ppLicense;
  delete wrapper;
  *ppLicense = NULL;
}

