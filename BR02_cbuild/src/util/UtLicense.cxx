// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2013 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/CarbonPlatform.h"
#include "util/UtLicense.h"
#include "util/UtLicenseMsg.h"
#include "util/UtString.h"
#include "util/UtShellTok.h"
#include "util/OSWrapper.h"
#include "util/UtHashMap.h"
#include "util/UtSimpleCrypt.h"
#include "util/RandomValGen.h"
#include "util/UtStringArray.h"
#include "util/UtHashSet.h"
#include "util/UtArray.h"

#define CARBON_TOKENS_FEATURE_NAME "crbn_vsp_tokens"
#define CARBON_TOKENS_FEATURE_PREFIX "crbn_vsp_tokens:"

#if defined(CDB) || defined(LICENSE_BYPASS)
#define OLD_LICENSE
#endif

#if defined(CHECKIN)
#ifdef OLD_LICENSE
#undef OLD_LICENSE
#endif
#endif

/*
  Set to 1 if you want licensing to tell you when a license is checked
  out, how many, and which feature.
*/
#define VERBOSE 0
// needed for the exit callback catch
#include <stdio.h>

// Just define this for a time-bomb
#ifdef OLD_LICENSE
#include <time.h>

// In the assignments for scYear,scMonth,scDay we use the CURTIME*
// macros that are defined in the makefile for this directory and
// passed to gcc on the command line.  To make sure that this makefile
// will work on all platforms only the simplest date formatting
// capabilities of /bin/date are used. This can result in a
// value for CURTIMEMONTH or CURTIMEDAY of 09 (which has a leading
// zero but is not a valid octal value).  To force these values to
// always be interpreted as decimal values (instead of as octal because
// of the leading zero), a "1" is prepended to each.  Thus the range
// of months is 101-112 and the range of days is 101-131.

// CURTIMEYEAR is 100 for the year 2000
// bump the year up by 1 if the license period causes us to move into the next year
static const int scYear = CURTIMEYEAR +              (1 * ((int)(CURTIMEMONTH-100)/(int)12));
// CURTIMEMONTH has a range of 101 to 112
// in the following we add 1 to the month to give us a 1 month license
static const int scMonth = (CURTIMEMONTH-100) + 1 - (12 * ((int)(CURTIMEMONTH-100)/(int)12));
// CURTIMEDAY has a range of 101 to 131
// In the following we limit the day of month to 28 days, so it will be in range for any month
static const int scDay = (CURTIMEDAY-100) - (3 * ((int)(CURTIMEDAY-100)/(int)29));
static const int scHour = 0; // midnight
static const int scMin = 0; // midnight
static const int scSec = 0; // midnight

#else

#include "lmclient.h"
#include "lm_attr.h"

#endif

class UtLicense::Counter
{
public:
  CARBONMEM_OVERRIDES

  Counter() : mCount(0)
  {}
  
  void add(int num) { mCount += num; }
  void subtract(int num) { mCount -= num; }

  int num() const { return mCount; }

private:
  int mCount;
};

struct UtLicense::LicenseData
{
  CARBONMEM_OVERRIDES

  LicenseData(Type licType) : mType(licType) 
  {}

  ~LicenseData()
  {}

  int numOut() const { return mLicenseTickets.size(); }

  int popLicense() 
  { 
    int ret = -1;
    {
      if (! mLicenseTickets.empty()) {
        ret = mLicenseTickets.back();
        mLicenseTickets.pop_back();
      }
    }
    return ret;
  }
  
  void pushLicense(int groupNumber)
  {
    mLicenseTickets.push_back(groupNumber);
  }

  void assignLicenseTickets(const LicenseData* srcLD)
  {
    mLicenseTickets = srcLD->mLicenseTickets;
  }
  
  typedef UtArray<int> IntArray;
  IntArray mLicenseTickets;
  
  UtLicense::Type mType;
  
  UtLicense::Job* mJob;
};

// MAX_FEATURE_LEN is currently 30 bytes.
#ifndef MAX_FEATURE_LEN
#define MAX_FEATURE_LEN 30
#endif
static const int cMaxNameSize = MAX_FEATURE_LEN+1;

// These are versions of the product pertaining to the license file.
#ifndef OLD_LICENSE
static const char* cVersionString = "1.0";
#endif

struct UtLicense::Job
{
  struct StringGather
  {
    CARBONMEM_OVERRIDES
    
    StringGather(UtStringArray* arr) : mStrArray(arr)
    {}

    bool exists(const char* candidate) const
    {
      UtString tmpCpy(candidate);
      bool ret = (mStrSet.find(tmpCpy) != mStrSet.end());
      return ret;
    }
    
    void addUnique(const char* candidate)
    {
      mStrArray->push_back(candidate);
      addLast();
    }
    
    void addLast()
    {
      UtString tmpCpy(mStrArray->back());
      mStrSet.insert(tmpCpy);
    }

    typedef UtHashSet<UtString> StrSet;
    
    UtStringArray* mStrArray;
    StrSet mStrSet;
  };
  
  CARBONMEM_OVERRIDES

  Job(bool wait, bool manualHeartbeats, UtLicense::MsgCB* msgCB,
      UtLicense::Counter* counter) :
    mMsgCB(msgCB),
    mLicenseCounter(counter),
    mNumActive(0),
    mWait(wait),
    mManualHeartbeats(manualHeartbeats)
#ifndef OLD_LICENSE
    , mId(NULL)
#endif
  {}

  void putBlocking(bool block) {
    mWait = block;
  }

  UtLicense::MsgCB* mMsgCB;
  UtLicense::Counter* mLicenseCounter;
  UtString mLicenseGroup; 
  int mNumActive;
  bool mWait;
  bool mManualHeartbeats;

#ifndef OLD_LICENSE
  LM_HANDLE* mId;
  VENDORCODE mCode;

  static void sWrapExitCall(LM_HANDLE*, char* feature, void* data)
  {
    UtString err;
    err << "Lost license, " << feature << ". Unable to reconnect to server.";
    Job* me = static_cast<Job*>(data);
    me->doExitCB(&err);
  }

#endif

#ifndef OLD_LICENSE
  bool wrap_new_job(LM_HANDLE** id, VENDORCODE* code, const char* defaultDir, int serverTimeout, UtString* reason)
  {
    bool stat = true;
    if (lc_new_job(0, lc_new_job_arg2, code, id))
    {
      *reason << "lc_new_job failed: " << lc_errstring(*id);
      stat = false;
    }
    else
    {
      lc_set_attr(*id, LM_A_CHECK_BADDATE, (LM_A_VAL_TYPE) 1);
      if (defaultDir)
        lc_set_attr(*id, LM_A_LICENSE_DEFAULT, (LM_A_VAL_TYPE) defaultDir);

      if (mManualHeartbeats)
      {
        lc_set_attr(*id, LM_A_CHECK_INTERVAL, (LM_A_VAL_TYPE) -1);
        lc_set_attr(*id, LM_A_RETRY_INTERVAL, (LM_A_VAL_TYPE) -1);
      }

      // turns of .flexlmrc update
      lc_set_attr(*id, LM_A_CKOUT_INSTALL_LIC, (LM_A_VAL_TYPE) 0);

      // allow for unresponsive servers - to a point. This equals
      // about 40 minutes of downtime for the server. A retry happens
      // every 30 seconds.
      lc_set_attr(*id, LM_A_RETRY_COUNT, (LM_A_VAL_TYPE) 80);

      // Allow for unresponsive clients up to 4 hours and 15 minutes
      // (the maximum by flexlm = 15300 seconds). This is settable by
      // serverTimeout. We use flexlm's default of 7200 seconds otherwise.
      // if serverTimeout is <= 0, we just use the default; otherwise,
      // we set the timeout to serverTimeout (up to the max).
      if (serverTimeout > 0)
      {
        if (serverTimeout > cServerTimeoutMax)
          serverTimeout = cServerTimeoutMax;
        // needed for Linux64 compile
        SIntPtr serverTimeoutCast = serverTimeout;
        lc_set_attr(*id, LM_A_TCP_TIMEOUT, (LM_A_VAL_TYPE) serverTimeoutCast);
      }
      
      // all applications now have exit callback mechanism via the
      // UtLicense::MsgCB structure
      lc_set_attr(*id, LM_A_VENDOR_CALLBACK_DATA, (LM_A_VAL_TYPE) this);
      lc_set_attr(*id, LM_A_USER_EXITCALL_EX, (LM_A_VAL_TYPE) sWrapExitCall);
    }
    return stat;
  }
#endif

  bool newId(const UtString* 
#ifndef OLD_LICENSE
             defaultDir
#endif
             , 
             int
#ifndef OLD_LICENSE
             serverTimeout
#endif
             ,
             UtString* 
#ifndef OLD_LICENSE
             reason
#endif
             )
  {
    bool stat = true;
#ifndef OLD_LICENSE
    stat = wrap_new_job(&mId, &mCode, defaultDir->c_str(), serverTimeout, reason);
#endif
    return stat;
  }
  
  void doExitCB(UtString* errMsg)
  {
    mMsgCB->exitNow(errMsg->c_str());
    // should never get here.
    // In case we do, be helpful and print out the error message in
    // case the exitNow call didn't. I'd rather have two of the same
    // message than no message.
    fprintf(stderr, "%s\n", errMsg->c_str());
    exit(1); 
  }
  
  void setCheckoutData(int groupNumber)
  {
    mLicenseGroup.clear();
    mLicenseGroup << groupNumber;
    
#ifndef OLD_LICENSE
    lc_set_attr(mId, LM_A_CHECKOUT_DATA, (LM_A_VAL_TYPE) mLicenseGroup.c_str());
#endif
  }
  
  void checkin(const UtString& 
#ifndef OLD_LICENSE
               feature
#endif
               ,
               int groupNumber
               )
  {
    --mNumActive;
    mLicenseCounter->subtract(1);
    setCheckoutData(groupNumber);
#ifndef OLD_LICENSE
    lc_checkin(mId, const_cast<char*>(feature.c_str()), mNumActive);
#endif
  }

#ifndef OLD_LICENSE
  void wrap_free_job(LM_HANDLE* id)
  {
    lc_free_job(id);
  }
#endif
  
  void checkinAllOnly()
  {
#ifndef OLD_LICENSE
    if (mNumActive > 0) {
      mLicenseCounter->subtract(mNumActive);
      lc_checkin(mId, (char*) LM_CI_ALL_FEATURES, 0);
    }
#endif
    mNumActive = 0;
  }

  void checkinAll()
  {
    checkinAllOnly();
#ifndef OLD_LICENSE
    wrap_free_job(mId);
    mId = NULL;
#endif
  }

  void heartbeat()
  {
#ifndef OLD_LICENSE
    int num_reconnects;
    static const int cNumMinutes = 2;
    static const int cMaxReconnects = 20;
    if (lc_heartbeat(mId, &num_reconnects, cNumMinutes) != 0)
    {
      // server is down
      if (num_reconnects >= cMaxReconnects)
      {
        UtString licErr;
        licErr << "FlexLM license server is down. Cannot reconnect.";
        doExitCB(&licErr);
      }
    }
#endif
  }

  bool checkoutTest(const char*
                    transFeature,
                    UtString* reason)
  {
    bool exists = true;
#ifndef OLD_LICENSE 

#if pfWINDOWS
    lc_set_attr(mId, LM_A_PERROR_MSGBOX, static_cast<LM_A_VAL_TYPE>(false));
    // This checkout function is called for features that might not
    // exist.  For example, if a diagnostic license is available, it
    // is checked out, and no other licenses are needed.  If the
    // diagnostic license is not available, it's not a big deal.  It
    // just causes individual license features to be checked out
    // later.
    //
    // The diagnostic check is done for all models, even those that
    // are time-bombed and would otherwise not need a license server.
    // This has the side effect of popping up a dialog box asking for
    // license server information in the case that the server can't be
    // contacted.
    //
    // If the user has a time-bombed model, it's likely that he
    // doesn't even have a Carbon license.  The fact that no
    // diagnostic license is available doesn't matter, so don't pop up
    // dialog box if license can't be found.
    lc_set_attr(mId, LM_A_PROMPT_FOR_FILE, static_cast<LM_A_VAL_TYPE>(false));
#endif
    // create a separate job for this and free it. We need to do this
    // because the LOCALTEST flag causes a persistent connection to
    // the server
    if (lc_checkout(mId, const_cast<char*>(transFeature), 
                    const_cast<char*>(cVersionString), 
                    1,
                    LM_CO_LOCALTEST, &mCode, LM_DUP_NONE))
    {
      *reason << "License feature check failed: " << lc_errstring(mId);
      exists = false;
    }
#else
    exists = checkout(transFeature, 1, reason) == eSuccess;
#endif
    return exists;
  }
  
  bool isErrorCausedByFilter()
  {
    bool ret = false;
#ifndef OLD_LICENSE
    LM_ERR_INFO* errInfo = lc_err_info(mId);
    if (errInfo)
      ret = (errInfo->maj_errno == LM_LOCALFILTER);
#endif
    return ret;
  }

#ifndef OLD_LICENSE
  static bool sIsQueueStatus(int lcStat)
  {
    return (lcStat == LM_MAXUSERS) || (lcStat == LM_USERSQUEUED) ||
      (lcStat == LM_FEATQUEUE);
  }
#endif

#define MAX_QUEUE_TIME 60
#define QUEUE_INTERVAL 5

  UtLicense::CheckoutStatus checkout(const char* 
#ifndef OLD_LICENSE 
                featureName
#endif
                , 
                int 
#ifndef OLD_LICENSE 
                numLics
#endif
                ,
                UtString* reason
                )
  {
    UtLicense::CheckoutStatus stat = eSuccess;

#ifndef OLD_LICENSE
    if (getenv("CARBON_LICENSE_DIAG"))
      fprintf(stderr, "CRBN-I-LICCHECKOUT: %s\n", featureName );
#endif

#ifndef OLD_LICENSE
    int coFlags = LM_CO_NOWAIT;
    if (mWait)
      coFlags = LM_CO_QUEUE;
    int lcStat = lc_checkout(mId, const_cast<char*>(featureName), 
                             const_cast<char*>(cVersionString), 
                             numLics,
                             coFlags,
                             &mCode, LM_DUP_NONE);
    
    if (lcStat != 0)
    {
      if (mWait)
      {
        if (sIsQueueStatus(lcStat))
        {
          mMsgCB->waitingForLicense(featureName);
          
          int i = 0;
          while ((lcStat = lc_status(mId, const_cast<char*>(featureName))))
          {
            if (! sIsQueueStatus(lcStat))
            {
              int numTries = (i / QUEUE_INTERVAL) + 1;
              *reason << "checkout failed after " << numTries << " attempt";
              if (numTries > 1)
                *reason << "s";
              *reason << ": " << lc_errstring(mId);

#ifndef OLD_LICENSE
              if (getenv("CARBON_LICENSE_DIAG"))
              {
                fprintf(stderr, "CRBN-I-LICCHECKOUT: %s FAILED TO CHECKOUT, Reason: %s\n", featureName, (*reason).c_str()); 
              }
#endif
              return eFail;
            }
            
            OSSleep(QUEUE_INTERVAL);
            i += QUEUE_INTERVAL;

            /* we could be waiting a very long time. If we have
               licenses, we could be causing a dead-lock. Returning
               eRestart will cause us to relinquish
               the licenses, wait for some random time, and then check
               out the relinquished licenses as well as this one.

               If mNumActive is 0 we haven't checked out any other
               licenses so don't return eRestart in this case. Just
               keep looping until we acquire the license.
            */
            if ((mLicenseCounter->num() > 0) && (i >= MAX_QUEUE_TIME))
            {
#ifndef OLD_LICENSE
              if (getenv("CARBON_LICENSE_DIAG"))
              {
                fprintf(stderr, "CRBN-I-LICCHECKOUT: %s FAILED TO CHECKOUT, eRestart\n", featureName); 
              }
#endif
              return eRestart;
            }
            
            
#if VERBOSE
            if ((i % 10) == 0)
              fprintf(stderr, "%d seconds\n", i);
#endif
          }
          mMsgCB->queuedLicenseObtained(featureName);
          ++mNumActive;
          mLicenseCounter->add(1);
        }
        else
        {
          *reason << "checkout failed (notQueued): " << lc_errstring(mId);
          stat = eFail;
        }
      }
      else
      {
        *reason << "checkout failed (nowait): " << lc_errstring(mId);
        stat = eFail;
      }
    }
    else 
    {
      ++mNumActive;
      mLicenseCounter->add(1);
    }
#else

    struct tm timeBomb;
    timeBomb.tm_sec = scSec;
    timeBomb.tm_min = scMin;
    timeBomb.tm_hour = scHour;
    timeBomb.tm_mday = scDay;
    timeBomb.tm_mon = scMonth-1; // scMonth defines Jan == 1, mktime expects Jan == 0
    timeBomb.tm_year = scYear;
    timeBomb.tm_isdst = -1; // don't deal with daylight savings
    
    time_t targetTime = mktime(&timeBomb);
    time_t curTime;
    time(&curTime);

    if ((targetTime < 0) || (curTime < 0))
    {
      reason->assign("System error when checking out license");
      stat = eFail;
    }
    else if (curTime > targetTime)
    {
      reason->assign("License has expired");
      stat = eFail;
    }
    else
    {
      ++mNumActive;
      mLicenseCounter->add(1);
    }
#endif

#ifndef OLD_LICENSE
    if (getenv("CARBON_LICENSE_DIAG"))
    {
      if (stat != eFail)
        fprintf(stderr, "CRBN-I-LICCHECKOUT: %s Success\n", featureName );
      else
        fprintf(stderr, "CRBN-I-LICCHECKOUT: %s FAILED TO CHECKOUT, Reason: %s\n", featureName, (*reason).c_str()); 
    }
#endif
    return stat;
  }
  
};

class UtLicense::FeatureHelper
{
public:
  CARBONMEM_OVERRIDES

  typedef UtHashMap<UtString, LicenseData*> FeatureMap;
  typedef UtHashMap<UtString, int> TokensMap;

  FeatureHelper() {}
  
  ~FeatureHelper()
  {
    FeatureMap::iterator e = mUserReqs.end();
    for (FeatureMap::iterator p = mUserReqs.begin(); p != e; ++p)
    {
      LicenseData* fData = p->second;
      if (fData)
        delete fData;
    }
  }

  LicenseData* getLicenseEnumData(Type licType, UtString* featureStr)
  {
    translateFeature(licType, featureStr);
    return getLicenseFeatureData(featureStr->c_str(), licType);
  }

  // Get the license data for a feature, and cache the fact that we already requested it.
  LicenseData* getLicenseFeatureData(const char* featureName, UtLicense::Type licType)
  {
    UtString featureStr(featureName);

    // All crbn_vsp_tokens_ features always checkout the base feature name crbn_vsp_tokens
    if (strncmp(featureName, CARBON_TOKENS_FEATURE_PREFIX, strlen(CARBON_TOKENS_FEATURE_PREFIX)) == 0)
        featureStr = CARBON_TOKENS_FEATURE_NAME;

    LicenseData* featureData = NULL;    
    FeatureMap::iterator p = mUserReqs.find(featureStr);
    if (p == mUserReqs.end())
    {
      featureData = new LicenseData(licType);
      mUserReqs[featureStr] = featureData;     
    }
    else
      featureData = p->second;

    return featureData;
  }

  void setTokenFeatureConsumed(const char* featureName)
  {
    mTokenReqs[featureName] = 1;
  }

  // Special handling for "crbn_vsp_tokens_xx"
  bool isFeatureNameConsumed(const char* featureName) const
  {
    bool isConsumed = false;
   
    // Special handling for crbn_vsp_tokens_, we look into a different
    // cache
    if (strncmp(featureName, CARBON_TOKENS_FEATURE_PREFIX, strlen(CARBON_TOKENS_FEATURE_PREFIX)) == 0)
    {
      TokensMap::const_iterator p = mTokenReqs.find(featureName);
      if (p != mTokenReqs.end())
        isConsumed = true;
    }
    else // Do what we did before
    {
      FeatureMap::const_iterator p = mUserReqs.find(featureName);
    
      if (p != mUserReqs.end())
      {
        const LicenseData* lcData = p->second;
        if (lcData && (lcData->numOut() > 0) && (lcData->mType != eInvalid))
          isConsumed = true;
      }
    }
    return isConsumed;
    
  }

  bool isLicenseConsumed(Type licType) const
  {
    UtString feature;
    translateFeature(licType, &feature);
    return isFeatureNameConsumed(feature.c_str());
  }


  // This leaks! Only call if you have transferred the pointers using
  // a copy constructor see relinquishAndRestart
  void clearPointers()
  {
    mUserReqs.clear();
    mTokenReqs.clear();
  }

  typedef FeatureMap::UnsortedLoop UnsortedLoop;
  typedef FeatureMap::SortedLoop SortedLoop;

  UnsortedLoop loopUnsorted() { return mUserReqs.loopUnsorted(); }
  SortedLoop loopSorted() const { return mUserReqs.loopSorted(); }
  
private:
  // Map of feature name to FeatureData.
  FeatureMap mUserReqs;
  // Map of tokens name to feature count, i.e. crbn_vsp_tokens_a15 -> (num tokens)
  TokensMap mTokenReqs; 

  void translateFeatureScrambled(Type type, char* featureBuf) const
  {
    switch (type)
    {
    case eSpeedCompiler:
      SCRAMBLECODE21("cds_speedcompiler_dev", featureBuf);
      break;
    case eDesignPlayer:
      SCRAMBLECODE20("cds_designplayer_dev", featureBuf);
      break;
    case eMemory:
      SCRAMBLECODE16("cds_memories_dev", featureBuf);
      break;
    case eTransactor:
      SCRAMBLECODE19("cds_transactors_dev", featureBuf);
      break;
    case eDIAGNOSTICS:
      SCRAMBLECODE12("cds_internal", featureBuf);
      break;
    case eSpeedCompilerVlog:
      SCRAMBLECODE25("cds_speedcompiler_verilog", featureBuf);
      break;
    case eSpeedCompilerVhdl:
      SCRAMBLECODE22("cds_speedcompiler_vhdl", featureBuf);
      break;
    case eSpeedCompilerMixed:
      SCRAMBLECODE30("cds_speedcompiler_vhdl_verilog", featureBuf);
      break;
    case eCwaveTestbench:
      SCRAMBLECODE11("cds_cwavetb", featureBuf);
      break;
    case eVSPCompiler:
      SCRAMBLECODE13("crbn_vsp_comp", featureBuf);
      break;
    case eVSPRuntime:
      SCRAMBLECODE8("crbn_vsp", featureBuf);
      break;
    case eVSPCompOpt:
      SCRAMBLECODE17("crbn_vsp_comp_opt", featureBuf);
      break;
    case eVSPCompVerilog:
      SCRAMBLECODE21("crbn_vsp_comp_verilog", featureBuf);
      break;
    case eVSPCompVhdl:
      SCRAMBLECODE18("crbn_vsp_comp_vhdl", featureBuf);
      break;
    case eVSPExecVHM:
      SCRAMBLECODE17("crbn_vsp_exec_vhm", featureBuf)
        break;
    case eLMToolSP:
      SCRAMBLECODE14("crbn_lmtool_sp", featureBuf);
      break;
    case eLMToolSPTimed:
      SCRAMBLECODE20("crbn_lmtool_sp_timed", featureBuf);
      break;
    case eVSPExecPlatArch:
      SCRAMBLECODE27("crbn_vsp_exec_intf_platarch", featureBuf);
      break;
    case eVSPReplay:
      // Alpha version for now. This will be changed when replay is
      // released.
      SCRAMBLECODE15("crbn_vsp_replay", featureBuf);
      break;
    case eIntfRealViewSOC:
      SCRAMBLECODE25("crbn_vsp_exec_intf_rvsocd", featureBuf);
      break;
    case eCleartext:
      SCRAMBLECODE27("crbn_vsp_gencleartextsource", featureBuf);
      break;          
    case eDumpVerilog:
      SCRAMBLECODE20("crbn_vsp_dumpverilog", featureBuf);
      break;
    case eOnDemand:
      SCRAMBLECODE17("crbn_vsp_ondemand", featureBuf);
      break;
    case eModelValidation:
      SCRAMBLECODE28("crbn_vsp_exec_modelvalidator", featureBuf);
      break;          
    case eSystemCComp:
      SCRAMBLECODE26("crbn_vsp_systemc_component", featureBuf);
      break;
    case eIPCreator:
      SCRAMBLECODE15("crbn_ip_creator", featureBuf);
      break;          
    case eTimebombBase:
      SCRAMBLECODE13("crbn_timebomb", featureBuf);
      break;          
    case eSoCDSystemCExport:
      SCRAMBLECODE25("crbn_socd_sysc_export_gen", featureBuf);
      break;
    case eVSPCompilerSV:
      SCRAMBLECODE16("crbn_vsp_comp_sv", featureBuf);
      break;
    case eUnknown:
      INFO_ASSERT(type != eUnknown, "Invalid type.");
      break;
    case eInvalid:
      INFO_ASSERT(type != eInvalid, "Bad type.");
      break;
    }
  }

  void stringifyEncrypted(Type type, 
                          const char* featureBuf,
                          UtString* str) const

  {
    size_t numElems = 0;
    int* arr = NULL;
    switch (type)
    {
    case eSpeedCompiler:
      numElems = 21;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE21(featureBuf, arr);
      break;
    case eDesignPlayer:
      numElems = 20;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE20(featureBuf, arr);
      break;
    case eMemory:
      numElems = 16;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE16(featureBuf, arr);
      break;
    case eTransactor:
      numElems = 19;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE19(featureBuf, arr);
      break;
    case eDIAGNOSTICS:
      numElems = 12;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE12(featureBuf, arr);
      break;
    case eSpeedCompilerVlog:
      numElems = 25;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE25(featureBuf, arr);
      break;
    case eSpeedCompilerVhdl:
      numElems = 22;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE22(featureBuf, arr);
      break;
    case eVSPExecPlatArch:
      numElems = 27;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE27(featureBuf, arr);
      break;
    case eSpeedCompilerMixed:
      numElems = 30;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE30(featureBuf, arr);
      break;
    case eCwaveTestbench:
      numElems = 11;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE11(featureBuf, arr);
      break;
    case eVSPCompiler:
      numElems = 13;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE13(featureBuf, arr);
      break;
    case eVSPRuntime:
      numElems = 8;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE8(featureBuf, arr);
      break;
    case eVSPCompOpt:
      numElems = 17;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE17(featureBuf, arr);
      break;
    case eVSPCompVerilog:
      numElems = 21;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE21(featureBuf, arr);
      break;
    case eVSPCompVhdl:
      numElems = 18;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE18(featureBuf, arr);
      break;
    case eVSPExecVHM:
      numElems = 17;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE17(featureBuf, arr);
      break;
    case eLMToolSP:
      numElems = 14;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE14(featureBuf, arr);
      break;
    case eLMToolSPTimed:
      numElems = 20;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE20(featureBuf, arr);
      break;
    case eVSPReplay:
      numElems = 15;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE15(featureBuf, arr);
      break;
    case eIntfRealViewSOC:
      numElems = 25;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE25(featureBuf, arr);
      break;
    case eCleartext:
      numElems = 27;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE27(featureBuf, arr);
      break;
    case eDumpVerilog:
      numElems = 20;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE20(featureBuf, arr);
      break;
    case eOnDemand:
      numElems = 17;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE17(featureBuf, arr);
      break;
    case eModelValidation:
      numElems = 28;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE28(featureBuf, arr);
      break;
    case eSystemCComp:
      numElems = 26;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE26(featureBuf, arr);
      break;
    case eIPCreator:
      numElems = 15;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE15(featureBuf, arr);
      break;          
    case eTimebombBase:
      numElems = 13;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE13(featureBuf, arr);
      break;          
    case eSoCDSystemCExport:
      numElems = 25;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE25(featureBuf, arr);
      break;          
    case eVSPCompilerSV:
      numElems = 16;
      arr = CARBON_ALLOC_VEC(int, numElems);
      SCRAMBLECODE16(featureBuf, arr);
      break;          
    case eUnknown:
      INFO_ASSERT(type != eUnknown, "Invalid type.");      
      break;
    case eInvalid:
      INFO_ASSERT(type != eInvalid, "Bad type.");
      break;
    }
    INFO_ASSERT(arr != NULL, "Bad type.");

    for (size_t i = 0; i < numElems; ++i)
      *str << char(arr[i]);
  
    CARBON_FREE_VEC(arr, int, numElems);
  }

  void translateFeature(Type licType, UtString* featureStr) const
  {
    char buf[cMaxNameSize];
    translateFeatureScrambled(licType, buf);
    stringifyEncrypted(licType, buf, featureStr);
  }
};

/*!
  Now that vendor strings are removed, this class serves little
  purpose, since there's no need to manage multiple job IDs with
  different filter characteristics.  However, when we decide to
  support both activation and floating licenses, it might become
  useful again.
 */
class UtLicense::JobManager
{
public:
  CARBONMEM_OVERRIDES

  JobManager(MsgCB* msgCB, bool manualHeartbeats) 
    : mLicenseTicker(0),
      mServerTimeout(-1),
      mMsgCB(msgCB),
      mDefaultDir("/license"),
      mBlocking(false),
      mManualHeartbeats(manualHeartbeats)
  {}

  ~JobManager() 
  {
    releaseAll();
  }

  const UtString* getDefaultDirPtr() const { return &mDefaultDir; }

  bool isManualHeartbeat() const { return mManualHeartbeats; }

  void putServerTimeout(int numSeconds) {
    mServerTimeout = numSeconds;
  }

  void putInstallDir(const UtString* installDir)
  {
    mDefaultDir.erase();
    OSConstructFilePath(&mDefaultDir, installDir->c_str(), "license");
  }

  Job* createJob(UtString* reason)
  {
    Job* job = new Job(mBlocking, mManualHeartbeats, mMsgCB, &mCounter); 
    if (! job->newId(&mDefaultDir, mServerTimeout, reason))
    {
      // If this failed, it means that lc_new_job() failed, which
      // indicates a horrible problem with the license server, such as
      // a bad encryption key.  Delete the job wrapper.
      job = NULL;
    } else {
      mJobs.push_back(job);
    }
    return job;
  }

  int setGroup(Job* job)
  {
    ++mLicenseTicker;
    job->setCheckoutData(mLicenseTicker);
    return mLicenseTicker;
  }

  void putBlocking(bool block)
  {
    mBlocking = block;

    // Set all the existing jobs to be blocking, as well.
    //
    // Our previous use of vendor strings masked a bug here.  cbuild
    // tries to check out an internal license before it enables
    // license queueing.  It used to be that we would create a new job
    // for each vendor string.  Internal licenses didn't have vendor
    // strings, but normal licenses did.  When the normal license
    // checkout occurred, a new job was created, picking up the new
    // blocking flag.
    //
    // With the removal of vendor strings, the original job from the
    // internal license checkout attempt is reused, so the blocking
    // flag needs to be propagated.
    for (JobVecLoop p(mJobs); ! p.atEnd(); ++p)
    {
      Job* j = *p;
      j->putBlocking(block);
    }
  };

  void deleteJob(Job* job)
  {
    for (JobVecLoop p(mJobs); ! p.atEnd(); ++p)
    {
      Job* curJob = *p;
      if (curJob == job)
      {
        // begin() is a misnomer for current.
        mJobs.erase(p.begin());
        curJob->checkinAll();
        delete curJob;
        break;
      }
    }
  }
  
  void releaseAll()
  {
    for (JobVecLoop p(mJobs); ! p.atEnd(); ++p)
    {
      Job* curJob = *p;
      curJob->checkinAll();
      delete curJob;
    }
    mJobs.clear();
  }

  inline void performHeartbeat()
  {
    if (mManualHeartbeats && ! mJobs.empty())
    {
      for (JobVecLoop p(mJobs); ! p.atEnd(); ++p)
      {
        Job* job = *p;
        job->heartbeat();
      }
    }
  }

  void checkinAllOnly()
  {
    for (JobVecLoop p(mJobs); ! p.atEnd(); ++p)
    {
      Job* curJob = *p;
      curJob->checkinAllOnly();
    }
  }
  
  typedef UtArray<Job*> JobVec;
  typedef Loop<JobVec> JobVecLoop;

  JobVecLoop loopJobs()
  {
    return JobVecLoop(mJobs);
  }

private:
  JobVec mJobs;
  int mLicenseTicker;
  int mServerTimeout;
  MsgCB* mMsgCB;
  UtString mDefaultDir;
  Counter mCounter;
  bool mBlocking;
  bool mManualHeartbeats;
};

UtLicense::UtLicense(MsgCB* msgCB, bool manualHeartbeats)
{
  mJobManager = new JobManager(msgCB, manualHeartbeats);
  mFeatureHelper = new FeatureHelper;
  mMode = eRefCount;
  mMsgCB = msgCB;
  INFO_ASSERT(mMsgCB, "Callback mechanism is NULL.");
  
  // initialize a random value generator. This is needed for deadlock
  // avoidance.
  // It can't be predictable; otherwise, we may stay in deadlock
  // longer with another carbon process. Base it on the pid. It isn't
  // absolutely perfect, but even if we do deadlock with another
  // carbon process with the same pid,  we will eventually come out of
  // deadlock due to slight timing differences in hardware.
  UInt32 pid = OSGetPid();
  mRandom = new RandomValGen((SInt32) pid);
}

UtLicense::~UtLicense()
{
  releaseAll();
  delete mFeatureHelper;
  delete mRandom;
  delete mJobManager;
}

void UtLicense::setMode(Mode mode)
{
  mMode = mode;
}

void UtLicense::releaseAll()
{
  delete mFeatureHelper;
  mFeatureHelper = new FeatureHelper;
  
  mJobManager->releaseAll();
}

void UtLicense::deleteJob(Job* job)
{
  mJobManager->deleteJob(job);
}

void UtLicense::putInstallDir(const UtString* installDir)
{
  mJobManager->putInstallDir(installDir);
}

/*
  Notice that every checkout calls setGroup() in the job
  manager. Setting each checkout to a different group makes each
  license checkout unique. Therefore, each license checkin is
  unique. Otherwise, the default grouping creates one pool of licenses
  that you can only checkin all at once. 
  In addition, we have to do our own group setting in order to
  differentiate between license features of the same name but with
  different vendor strings.

  For instance, let's say we have an app that checks out a feature
  with a vendor string of Nike and then the same feature license with
  a vendor string of Addidas. If we used the default grouping, and we
  only wanted to checkin 1 of those licenses we would be
  unsuccessful, we could only checkin both of those licenses.
*/
#define CARBON_USE_DEFAULT_FLEXLM_GROUPING 0
int UtLicense::getNumLicensesToCheckout(LicenseData* 
#if CARBON_USE_DEFAULT_FLEXLM_GROUPING
                                        lcData
#endif
)
{
  int ret = -1;
  switch(mMode)
  {
  case eRefCount:
    ret = 1;
    break;
  case eMultiple:
#if CARBON_USE_DEFAULT_FLEXLM_GROUPING
    ret = lcData->numOut();
#else
    ret = 1;
#endif
    break;
  }
  return ret;
}

void UtLicense::heartbeat()
{
  mJobManager->performHeartbeat();
}

void UtLicense::putBlocking(bool block)
{
  mJobManager->putBlocking(block);
}

UtLicense::LicenseData* 
UtLicense::getLicenseData(Type feature, 
                          UtString* featureStr)
{
  INFO_ASSERT(feature != eInvalid, "Bad feature.");
  return mFeatureHelper->getLicenseEnumData(feature, featureStr);
}


bool
UtLicense::isCompilerLicensed( UtString *reason )
{
  bool exists = doesFeatureExist(eVSPCompiler, reason );
  if ( !exists ) exists = doesFeatureExist( eSpeedCompilerVlog, reason );
  if ( !exists ) exists = doesFeatureExist( eSpeedCompilerVhdl, reason );
  if ( !exists ) exists = doesFeatureExist( eSpeedCompilerMixed, reason );
  // The old cds_speedcompiler_dev license is deprecated
  if ( !exists ) exists = doesFeatureExist( eSpeedCompiler, reason );
  return exists;
}


bool UtLicense::doesFeatureExist(Type feature, UtString* reason)
{
  if (isDeprecatedFeature(feature))
    return true;

  reason->clear();
  FeatureHelper tmpFeatureHelper;
  UtString transFeature;
  

  (void) tmpFeatureHelper.getLicenseEnumData(feature, &transFeature);
  
  return doExistCheck(transFeature.c_str(), reason);
}

bool UtLicense::doesFeatureNameExist(const char* featureName,
                                     UtString* reason)
{
  reason->clear();

  FeatureHelper tmpFeatureHelper;

  // The actual feature name is always crbn_vsp_tokens for the token based features
  if (strncmp(featureName, CARBON_TOKENS_FEATURE_PREFIX, strlen(CARBON_TOKENS_FEATURE_PREFIX)) == 0)
    featureName = CARBON_TOKENS_FEATURE_NAME;

  (void) tmpFeatureHelper.getLicenseFeatureData(featureName, eUnknown);

  return doExistCheck(featureName, reason);
}

void UtLicense::getFeaturesMatchingPrefix(const char* prefix, UtStringArray* matching)
{
#ifdef OLD_LICENSE
  // Can't do this if there's no FlexLM.
  (void) prefix;
  (void) matching;
#else
  // Get all the features from the license file.  We're not going to
  // keep this Job around, so this is very similar to doExistCheck().
  Counter tmpCounter;
  Job tempJob(false, mJobManager->isManualHeartbeat(), mMsgCB, &tmpCounter);
  UtString reason;
  if (tempJob.newId(mJobManager->getDefaultDirPtr(), -1, &reason)) {
    const char** featurePtr = const_cast<const char**>(lc_feat_list(tempJob.mId, LM_FLIST_ALL_FILES, NULL));
    // If there are valid licenses, we'll get an array of features,
    // the last of which will be an empty string (not a NULL pointer).
    // However, if there are no licenses, we get back a NULL pointer,
    // not a single element that's an empty string.
    if (featurePtr != NULL) {
      const char* feature;
      while ((feature = *featurePtr) != NULL) {
        if (strstr(feature, prefix) == feature) {
          matching->push_back(feature);
        }
        ++featurePtr;
      }
    }
  }
#endif
}

bool UtLicense::doExistCheck(const char* featureName, 
                             UtString* reason)
{
  bool exists = false;
  Counter tmpCounter;
  Job tempJob(false, mJobManager->isManualHeartbeat(), mMsgCB, &tmpCounter);
  if (tempJob.newId(mJobManager->getDefaultDirPtr(), -1, reason))
  {
    exists = tempJob.checkoutTest(featureName, reason);
    // frees the lm_job
    tempJob.checkinAll();
  }
  return exists;
}

bool UtLicense::checkout(Type versionFeature,
                         UtString* reason)
{
  if (isDeprecatedFeature(versionFeature))
    return true;

  if (versionFeature == eDIAGNOSTICS)
  {
    // don't use DIAGS to test licenseing
    if (getenv("CARBON_NO_DIAGS") != NULL)
      return false;

    if (! doesFeatureExist(versionFeature, reason))
      return false;
  }

  UtString transFeature;
  LicenseData* lcData = getLicenseData(versionFeature, &transFeature);
  bool stat = checkout(transFeature.c_str(), lcData, reason);
  return stat;
}

bool UtLicense::checkout(const char* featureName, LicenseData* lcData, UtString* reason)
{
  // Each FlexLM job can only connect to a single license server.  If
  // license features are spread across multiple servers, we need
  // multiple jobs.  While we could create a new job for each
  // checkout, that's inefficient.  Instead, we'll try each existing
  // job.  If that fails, we'll create a new one.

  bool success = false;
  bool lastIter = false;
  JobManager::JobVecLoop l = mJobManager->loopJobs();
  while (!success && !lastIter) {
    Job* job;
    if (l.atEnd()) {
      // If we've gone through all the existing jobs, try creating a
      // new one.  This will be the last iteration through the loop,
      // regardless of success.
      job = mJobManager->createJob(reason);
      lastIter = true;
    } else {
      job = *l;
    }

    if (job != NULL) {
      success = prepDataAndCheckout(featureName, lcData, job, reason);

      // If we created a new job and that still didn't succeed, we
      // don't need that job anymore.
      if (!success && lastIter) {
        mJobManager->deleteJob(job);
      }
    }

    ++l;
  }

  return success;
}

bool UtLicense::prepDataAndCheckout(const char* featureName, 
                                    LicenseData* lcData, 
                                    Job* job, 
                                    UtString* reason)
{
  lcData->mJob = job;
  bool stat = true;
  if ((mMode != eRefCount) || (lcData->numOut() == 0))
  {
    // up the ref count
    lcData->pushLicense(mJobManager->setGroup(job));

    CheckoutStatus coStat = doCheckout(featureName, lcData, reason);
    switch (coStat)
    {
    case eSuccess:
      break;
    case eFail:
      stat = false;
      break;
    case eRestart:
      stat = relinquishAndRestart(lcData, reason);
      break;
    }
    
  }
  return stat;
}

bool UtLicense::checkoutFeatureName(const char* featureName,
                                    UtString* reason)
{
  LicenseData* lcData = mFeatureHelper->getLicenseFeatureData(featureName, eUnknown);

  bool stat = checkout(featureName, lcData, reason);

  // Remember upon success that we got a token based license
  if (stat && 0 == strncmp(featureName, CARBON_TOKENS_FEATURE_PREFIX, strlen(CARBON_TOKENS_FEATURE_PREFIX)))
    mFeatureHelper->setTokenFeatureConsumed(featureName);
 
  return stat;
}

bool UtLicense::checkoutFeatureNameFromList(const UtStringArray& featureList,
                                            UtString* reason)
{
  bool success = false;

  for (UInt32 i = 0; !success && (i < featureList.size()); ++i) 
  {
    const char* feature = featureList[i];
    success = checkoutFeatureName(feature, reason);
  }

  return success;
}

bool UtLicense::isCheckedOut(Type feature) const
{
  if (isDeprecatedFeature(feature))
    return true;

  INFO_ASSERT(feature != eInvalid, "Bad feature.");
  bool isOut = false;
  
  if (mFeatureHelper)
    isOut = mFeatureHelper->isLicenseConsumed(feature);
  return isOut;
}

bool UtLicense::isFeatureNameCheckedOut(const char* featureName) const
{
  bool isOut = false;
  
  if (mFeatureHelper)
    isOut = mFeatureHelper->isFeatureNameConsumed(featureName);
  return isOut;
}

//
// feature name may be of the form:
// crbn_vsp_tokens:<model>:<count> which indicates that we need <count> tokens for 
// a model named <model>, so the actual feature name is crbn_vsp_tokens using <count>
// but the cached name is crbn_vsp_tokens:<model>
//
bool UtLicense::isFeatureNameFromListCheckedOut(const UtStringArray& featureList) const
{
  bool isOut = false;
  
  if (mFeatureHelper) {
    for (UInt32 i = 0; !isOut && (i < featureList.size()); ++i) {
      const char* feature = featureList[i];
      isOut = mFeatureHelper->isFeatureNameConsumed(feature);
    }
  }

  return isOut;
}

void UtLicense::release(Type feature)
{
  INFO_ASSERT(feature != eInvalid, "Bad feature.");
  if (isDeprecatedFeature(feature))
    return;

  UtString featureStr;
  LicenseData* lcData = getLicenseData(feature, &featureStr);
  
  if ((lcData->numOut() > 0) && (lcData->mType != eInvalid))
  {
    int groupNumber = lcData->popLicense();
    Job* job = lcData->mJob;
    if ((mMode != eRefCount) || (lcData->numOut() != 0))
      job->checkin(featureStr, groupNumber);
  }
}

void UtLicense::releaseFeatureName(const char* featureName)
{
  LicenseData* lcData = mFeatureHelper->getLicenseFeatureData(featureName, eUnknown);
  
  if ((lcData->numOut() > 0) && (lcData->mType != eInvalid))
  {
    int groupNumber = lcData->popLicense();
    Job* job = lcData->mJob;
    if ((mMode != eRefCount) || (lcData->numOut() != 0))
      job->checkin(featureName, groupNumber);
  }
}

//
// WARNING: As of the Fall 2007 Release, the following features
// have been deprecated according to Tom and Matt
//
bool UtLicense::isDeprecatedFeature(Type feature)
{
  bool retValue = false;
  
  if (feature == eVSPCompOpt ||       // crbn_vsp_comp_opt
      feature == eVSPCompVerilog ||   // crbn_vsp_comp_verilog
      feature == eVSPCompVhdl ||      // crbn_vsp_comp_vhdl
      feature == eVSPExecVHM          // crbn_vsp_exec_vhm
      )
  {
      retValue = true;
  }

  return retValue;
}
void UtLicense::getFeatureName(UtString* buf, Type feature)
{
  if (feature != eInvalid)
  {
    FeatureHelper tmpHelper;
    (void) tmpHelper.getLicenseEnumData(feature, buf);
  }
}

UtLicense::CheckoutStatus 
UtLicense::doCheckout(const char* transFeature, 
                      LicenseData* lcData, 
                      UtString* reason)
{
  int numLics = 1; //getNumLicensesToCheckout(lcData);
 
  if (0 == strncmp(transFeature, CARBON_TOKENS_FEATURE_PREFIX, strlen(CARBON_TOKENS_FEATURE_PREFIX)))
  {
    UtShellTok tok(transFeature, false, ":");
    ++tok; // skip crbn_vsp_tokens
    // skip model Name
    ++tok;
    const char* featureCount = *tok;

    numLics = atoi(featureCount);
    // shorten the actual name
    transFeature = CARBON_TOKENS_FEATURE_NAME;
  }

  Job* job = lcData->mJob;

  if (getenv("CARBON_LICENSE_DIAG"))
    fprintf(stderr, "CRBN-I-LICCHECKOUT: Checking out %d licenses of %s\n", numLics, transFeature);


  CheckoutStatus coStat = job->checkout(transFeature, numLics, reason);
  
  if (coStat == eFail)
  {
    lcData->popLicense();
    if (job->mNumActive == 0)
    {
      deleteJob(job);
      lcData->mJob = NULL;
    }
  }
  return coStat;
}

bool UtLicense::relinquishAndRestart(LicenseData* lcDataCause, UtString* reason)
{
  //relinquish all active licenses, and check them back out.

  // This transfers the pointers to oldFeatureHelper
  FeatureHelper oldFeatureHelper(*mFeatureHelper);
  // This nulls out the pointers so they don't get deleted
  mFeatureHelper->clearPointers();
  
  /*
    As we checkout licenses mFeatureHelper will be updated with
    the licenses that have been successfully checked
    out. oldFeatureHelper will always have the original checked out
    licenses (as well as the license request that didn't get checked
    out).
    When we finally have a fully successful checkout,
    oldFeatureHelper's destructor deletes the old data, but
    mFeatureHelper has exactly the same data with a successful
    checkout.
  */
  bool beginAgain = true;

  FeatureHelper* featureReporter = &oldFeatureHelper;
  // lcDataCause is the license we attempted to checkout but timed out
  // in the queue.
  LicenseData* lcCauseReporter = lcDataCause;
  
  while (beginAgain)
  {
    beginAgain = false;
    
    if (! relinquish(lcCauseReporter, featureReporter))
      return false;
    
    /*
      After we relinquish the old licenses, we don't want to report
      relinquishing them again if we have to restart. We want to
      report that we are relinquishing any newly checked out
      licenses. So, null out the lcCauseReporter and set the
      featureReporter to mFeatureHelper, since that will have the new
      stuff.
    */
    lcCauseReporter = NULL;
    featureReporter = mFeatureHelper;

    // Now, attempt to checkout all the required licenses.
    for (FeatureHelper::UnsortedLoop p = oldFeatureHelper.loopUnsorted(); ! beginAgain && ! p.atEnd(); ++p)
    {
      const UtString& transFeature = p.getKey();
      LicenseData* oldLcData = p.getValue();
      if (oldLcData->numOut() > 0)
      {
        // this updates mFeatureReporter
        LicenseData* lcData = mFeatureHelper->getLicenseFeatureData(transFeature.c_str(), oldLcData->mType);
        lcData->assignLicenseTickets(oldLcData);
        mMsgCB->requeueLicense(transFeature.c_str());

        // We need to attempt the checkout with all existing jobs, and
        // potentially create a new one.  This is because each job can
        // only connect to a single license server.  This is similar
        // to UtLicense::checkout(const char*, const UtString&,
        // LicenseData*, UtString*) - see that for details.
        bool success = false;
        bool lastIter = false;
        JobManager::JobVecLoop l = mJobManager->loopJobs();
        while (!success && !lastIter && !beginAgain) {
          if (l.atEnd()) {
            // If we've gone through all the existing jobs, try creating a
            // new one.  This will be the last iteration through the loop,
            // regardless of success.
            lcData->mJob = mJobManager->createJob(reason);
            lastIter = true;
          } else {
            lcData->mJob = *l;
          }

          if (lcData->mJob != NULL) {
            CheckoutStatus coStat = doCheckout(transFeature.c_str(), lcData, reason);
            switch(coStat)
            {
            case eSuccess:
              // Checkout of this feature succeeded, so exit the inner
              // loop and move to the next feature.
              success = true;
              break;
            case eFail:
              // If we created a new job and that still didn't
              // succeed, we don't need that job anymore.
              if (lastIter) {
                mJobManager->deleteJob(lcData->mJob);
                lcData->mJob = NULL;
              }
              break;
            case eRestart:
              // Need to restart, so exit the middle loop, which
              // causes us to check out all features again.
              beginAgain = true;
              break;
            }
          }

          ++l;
        }
      }
    }
  }
  return true;
}

bool UtLicense::relinquish(const LicenseData* lcDataCause, const FeatureHelper* filteredFeatures)
{
  // first print a message for each license that will be relinquished.
  for (FeatureHelper::SortedLoop p = filteredFeatures->loopSorted(); ! p.atEnd(); ++p)
  {
    const UtString& featureName = p.getKey();
    const LicenseData* lcData = p.getValue();
    
    if (lcData && (lcData != lcDataCause) && (lcData->numOut() > 0))
    {
      INFO_ASSERT(lcData->mType != eInvalid, "Inconsistent license data");
      mMsgCB->relinquishLicense(featureName.c_str());
    }
  }
  
  releaseAll();

  // wait a random amount of time 1-10 seconds
  UInt32 numSeconds = mRandom->URRandom(1, 10);
  OSSleep(numSeconds);
  return true;
}

void UtLicense::putServerTimeout(int numSeconds)
{
  mJobManager->putServerTimeout(numSeconds);
}

UtLicense::MsgCB* UtLicense::getMsgCB() { return mMsgCB; }

void UtLicense::emergencyCheckin()
{
  mJobManager->checkinAllOnly();
}

UtLicense::MsgCB::~MsgCB()
{}
