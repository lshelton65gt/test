// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/StringAtom.h"

/*
//! Allow use of << operator
std::ostream &operator<<(std::ostream &out, StringAtom& s)
{
  return out << s.str();
}

std::ostream &operator<<(std::ostream &out, StringAtom * s)
{
  return out << s->str();
}
*/
