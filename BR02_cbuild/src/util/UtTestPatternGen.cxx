// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/UtTestPatternGen.h"
#include "util/UtConv.h"
#include "util/DynBitVector.h"

/**
 **
 **
 **
 */
UtTestPatternGen::UtTestPatternGen(UInt32 width) : mWidth(width)
{
}


UtTestPatternGen::~UtTestPatternGen(void)
{
}


void UtTestPatternGen::generatePattern(UtString &/* pattern */)
{
}


/*
std::ostream &operator<<(std::ostream &out, UtTestPatternGen& t)
{
  UtString pattern;
  t.getPattern(pattern);
  return out << pattern;
}


std::ostream &operator<<(std::ostream &out, UtTestPatternGen * t)
{
  UtString pattern;
  t->getPattern(pattern);
  return out << pattern;
}
*/

UtTestPatternGen &operator++(UtTestPatternGen& t)
{
  t.nextPattern();
  return t;
}



/**
 **
 **
 **
 */
UtConstantTestPatternGen::UtConstantTestPatternGen(UInt32 width, char pattern_constant) : UtTestPatternGen(width), mPattern("")
{
  for (UInt32 i = 0; i < width; i++)
    {
      mPattern += pattern_constant;
    }
}


UtConstantTestPatternGen::~UtConstantTestPatternGen(void)
{
}


void UtConstantTestPatternGen::appendPattern(UtString* pattern)
{
  *pattern << mPattern;
}


void UtConstantTestPatternGen::nextPattern(void)
{
  ;
}

UtToggledTestPatternGen::UtToggledTestPatternGen(UInt32 width, 
                                                 RandomValGen& valueGen) : 
  UtRandomTestPatternGen(width, valueGen), 
  mCount(0) {
  generatePattern(mPatternA);
  for(UtString::iterator i = mPatternA.begin(); i != mPatternA.end(); i++) {
    mPatternB += ((*i == '0') ? '1' : '0');
  }
}


UtToggledTestPatternGen::~UtToggledTestPatternGen(void)
{
}


void UtToggledTestPatternGen::appendPattern(UtString* pattern)
{
  *pattern << ((mCount % 2) ? mPatternA : mPatternB);
}


void UtToggledTestPatternGen::nextPattern(void)
{
  mCount++;
}

UtRandomTestPatternGen::UtRandomTestPatternGen(UInt32        width,
                                               RandomValGen& valueGen,
                                               bool          initPattern) : 
  UtTestPatternGen(width),
  mValueGen(valueGen) {
  if (initPattern)
    nextPattern();
}

UtRandomTestPatternGen::~UtRandomTestPatternGen(void) {
}

void UtRandomTestPatternGen::generatePattern(UtString &pattern) {
  UInt32 bit_count = 0;

  pattern.clear();
  for(UInt32 i = 0; bit_count < mWidth; i++) {
    UInt32 bits = mValueGen.URandom();

    for(UInt32 j = 0; (j < 32) && (bit_count < mWidth); j++, bit_count++) {
      pattern += ((bits & 0x00000001) ? '1' : '0');
      bits = bits >> 1;
    }
  }
}

void UtRandomTestPatternGen::appendPattern(UtString* pattern) {
  *pattern << mPattern;
}

void UtRandomTestPatternGen::nextPattern(void) {
  generatePattern(mPattern);
}

UtConstrainedRandomIntGen::UtConstrainedRandomIntGen(UInt32        width, 
                                                     SInt32        left, 
                                                     SInt32        right, 
                                                     RandomValGen& valueGen) :
  UtRandomTestPatternGen(width, valueGen, false) {
  mMin = left;
  mMax = right;
  if(left > right) {
    mMin = right;
    mMax = left;
  }
  nextPattern();
}

void UtConstrainedRandomIntGen::generatePattern(UtString &pattern) {
  
  UInt32 bits = static_cast<UInt32>(mValueGen.SRRandom(mMin, mMax));

  if(mWidth == 32) {
    // This is the most common width for an integer, so optimize for it.
    char buf[33];

    CarbonValRW::writeBinValToStr(buf, 33, &bits, 32, false);
    pattern.assign(buf);
  }
  else {
    char* buf = CARBON_ALLOC_VEC(char, mWidth + 1);

    DynBitVector value(mWidth);
    // if we can have a negative value and the bits msb is 1, the
    // dynbitvector should be initialized to all ones.
    if((mMin < 0) && ((bits & 0x80000000) != 0))
      value.flip();
    UInt32* arr = value.getUIntArray();
    arr[0] = bits;
    CarbonValRW::writeBinValToStr(buf, mWidth + 1, arr, mWidth, false);
    pattern.assign(buf);
    CARBON_FREE_VEC(buf, char, mWidth + 1);
  }
}

/**
 **
 **
 **
 */
UtStepTestPatternGen::UtStepTestPatternGen(UInt32 width, bool step_up, UInt32 wait_count) : UtTestPatternGen(width), mWaitCount(wait_count), mCurrentCount(0)
{
  mPatternA = "";
  mPatternB = "";

  for (UInt32 i = 0; i < width; i++)
  {
    mPatternA += ((step_up) ? '0' : '1');
    mPatternB += ((step_up) ? '1' : '0');
  }
}


UtStepTestPatternGen::~UtStepTestPatternGen(void)
{
}


void UtStepTestPatternGen::appendPattern(UtString* pattern)
{
  *pattern << ((mCurrentCount < mWaitCount) ? mPatternA : mPatternB);
}


void UtStepTestPatternGen::nextPattern(void)
{
  mCurrentCount++;
}

