// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
  \file
  Contains the graph walker implementation for the abstract Graph class.
*/

#include "util/Graph.h"
#include "util/CarbonTypes.h"
#include "util/Iter.h"
#include <algorithm>

/*! 
 * Walk the given graph, calling the GraphWalker's virtual visiting
 * functions for each node entry/exit and edge traversed.  Edges are
 * classified according to their role in a depth-first traversal during
 * the walk, as either tree edges, cross edges or back edges.
 *
 * The visiting functions can control the walk by returning appropriate
 * GraphWalker::Command values.  GW_SUCCESS and GW_FAIL immediately
 * stop the walk and return success or failure, respectively.
 * GW_CONTINUE continues the normal depth-first traversal.  GW_SKIP
 * returned from an edge visitor returns to the parent node and
 * traverses the next outbound edge.  GW_BACKTRACK skips all remaining
 * outbound edges from a node and continues traversing from its parent.
 */
bool GraphWalker::walk(Graph* g)
{
  reset(g); // make sure flags are clear

  // visit all nodes in the graph
  Nodes nodeVector;
  nodes(&nodeVector, g);
  for (Nodes::iterator p = nodeVector.begin(); p != nodeVector.end(); ++p)
  {
    GraphNode* node = *p;
    if (mFinished.find(node) != mFinished.end())
      continue;
    if (not isValidStartNode(g, node))
      continue;
    Command cmd = visit(node);
    if (cmd == GW_FAIL || cmd == GW_SUCCEED)
      return (cmd == GW_SUCCEED);
  }

  return true;
}

/*!
 * Begin a graph walk from a specified node.
 */
bool GraphWalker::walkFrom(Graph* g, GraphNode* node)
{
  mGraph = g;
  if (mFinished.find(node) != mFinished.end())
    return true;
  return (visit(node) == GW_SUCCEED);
}

/*!
 * Clear the finished set. This allows the graph to be walked again.
 */
void GraphWalker::reset(Graph* graph)
{
  mFinished.clear();
  mGraph = graph;
}

//! function called to fill a vector with the graph's nodes
void GraphWalker::nodes(Nodes* nodeVector, Graph* g)
{
  for (Iter<GraphNode*> iter = g->nodes(); !iter.atEnd(); ++iter)
    nodeVector->push_back(*iter);
}

//! function called to fill a vector with a node's edges
Iter<GraphEdge*> GraphWalker::edges(GraphNode* node) {
  return mGraph->edges(node);
}

bool GraphWalker::returnVisitNode(Command* cmd)
{
  Command cmdIn = *cmd;
  if (cmdIn == GW_FAIL || cmdIn == GW_SUCCEED || cmdIn == GW_BACKTRACK) {
    return true;
  } else if (cmdIn == GW_SKIP) {
    *cmd = GW_CONTINUE;
    return true;
  }
  return false;
}
  
bool GraphWalker::returnVisitEdge(Command* cmd)
{
  Command cmdIn = *cmd;
  if (cmdIn == GW_FAIL || cmdIn == GW_SUCCEED || cmdIn == GW_BACKTRACK) {
    return true;
  } else if (cmdIn == GW_SKIP) {
    *cmd = GW_CONTINUE;
  }
  return false;
}

/*!
 * This is an internal traversal function that implements the standard
 * depth-first graph walk, accouting for GraphWalker::Command requests.
 */
GraphWalker::Command GraphWalker::visit(GraphNode* node)
{
  // visit node before visiting any children
  Command cmd = visitNodeBefore(mGraph, node);
  if (returnVisitNode(&cmd)) {
    return cmd;
  }

  // Mark this node pending
  mPending.insert(node);

  // visit children.  Note that we call edges() to get an iterator,
  // which is a virtual method that is overridden in GraphSortedWalker.
  bool between = false;
  for (Iter<GraphEdge*> p(edges(node)); !p.atEnd(); ++p) {
    // If we are in between edges, visit node between
    if (between)  {
      // visit node between each child
      cmd = visitNodeBetween(mGraph, node);
      if (returnVisitNode(&cmd)) {
        mPending.erase(node);
        return cmd;
      }
    } else
      between = true;

    // visit the next edge
    GraphEdge* edge = *p;
    bool retry = true;
    while (retry) {
      retry = false;
      cmd = visitEdge(node, edge);
      if (cmd == GW_BACKTRACK) {
        if (!continueBacktrack(mGraph, node)) {
          cmd = GW_CONTINUE;
          retry = true;
        }
      }
    }
    if (returnVisitEdge(&cmd)) {
      mPending.erase(node);
      return cmd;
    }
  }

  // visit node after visiting all children
  cmd = visitNodeAfter(mGraph, node);

  // update the status sets
  mPending.erase(node);
  mFinished.insert(node);

  return cmd;
}

GraphWalker::Command GraphWalker::visitEdge(GraphNode* node, GraphEdge* edge)
{
  Command cmd = GW_CONTINUE;
  GraphNode* to = mGraph->endPointOf(edge);
  if (mFinished.find(to) != mFinished.end())
  {
    // this is a cross edge
    cmd = visitCrossEdge(mGraph, node, edge);
  }
  else if (mPending.find(to) != mPending.end())
  {
    // this is a back edge
    cmd = visitBackEdge(mGraph, node, edge);
  }
  else
  {
    // this is a tree edge
    cmd = visitTreeEdge(mGraph, node, edge);
    if (cmd == GW_CONTINUE) {
      cmd = visit(to); // recurse to visit child node
      if (returnVisitNode(&cmd)) {
        return cmd;
      }
    }
  }
  return cmd;
} // GraphWalker::Command GraphWalker::visitEdge


GraphSortedWalker::GraphSortedWalker() : 
  mNodeCmpWrapper(NULL), mEdgeCmpWrapper(NULL)
{}

GraphSortedWalker::~GraphSortedWalker() {}

bool GraphSortedWalker::walk(Graph* g, NodeCmp* nodeCmp, EdgeCmp* edgeCmp)
{
  mNodeCmpWrapper = NodeCmpWrapper(nodeCmp);
  mEdgeCmpWrapper = EdgeCmpWrapper(edgeCmp);
  return GraphWalker::walk(g);
}

bool 
GraphSortedWalker::walkFrom(Graph* g, GraphNode* node,
                            NodeCmp* nodeCmp, EdgeCmp* edgeCmp)
{
  mNodeCmpWrapper = NodeCmpWrapper(nodeCmp);
  mEdgeCmpWrapper = EdgeCmpWrapper(edgeCmp);
  return GraphWalker::walkFrom(g, node);
}

void GraphSortedWalker::nodes(Nodes* nodeVector, Graph* graph)
{
  // Use the base class to gather the nodes and then sort them
  GraphWalker::nodes(nodeVector, graph);
  std::sort(nodeVector->begin(), nodeVector->end(), mNodeCmpWrapper);
}

//! Loop subclass for sorted walks over edges
/*!
 *! this loop class must 'own' the vector used to hold the sorted
 *! order, so that the vector's lifetime matches that of the iterator.
 *! It is declared here in the .cxx file because it will be "abstracted"
 *! by wrapping it in an Iter<GraphEdge> to be returned to the user
 */
class GraphSortedWalkerEdgeIter : public Loop<GraphSortedWalker::Edges> {
public:
  GraphSortedWalkerEdgeIter() {}

  template<class Sorter>
  void init(Sorter& sorter) {
    std::sort(mEdgeVector.begin(), mEdgeVector.end(), sorter);
    mPtr = mEdgeVector.begin();
    mEnd = mEdgeVector.end();
  }
  
  GraphSortedWalkerEdgeIter(const GraphSortedWalkerEdgeIter& src) :
    Loop<GraphSortedWalker::Edges>(src),
    mEdgeVector(src.mEdgeVector)
  {
    reset(src);
  }
    
  GraphSortedWalkerEdgeIter& operator=(const GraphSortedWalkerEdgeIter& src) {
    if (&src != this) {
      mEdgeVector = src.mEdgeVector;
      reset(src);
    }
    return *this;
  }

  void reset(const GraphSortedWalkerEdgeIter& src) {
    GraphSortedWalker::Edges::const_iterator p = src.mPtr;
    GraphSortedWalker::Edges::const_iterator start = src.mEdgeVector.begin();
    UInt32 offset = p - start;
    mPtr = mEdgeVector.begin() + offset;
    mEnd = mEdgeVector.end();
  }

  GraphWalker::Edges mEdgeVector;
};

Iter<GraphEdge*> GraphSortedWalker::edges(GraphNode* node) {
  // use the base class to gather the edges and then sort them
  GraphSortedWalkerEdgeIter edges;
  for (Iter<GraphEdge*> iter(GraphWalker::edges(node)); !iter.atEnd(); ++iter) {
    edges.mEdgeVector.push_back(*iter);
  }
  edges.init(mEdgeCmpWrapper);
  return Iter<GraphEdge*>::create(edges);
}


/*!
 * Populate a set of roots with all nodes in the graph then prune out
 * nodes with inbound edges. Any nodes remaining in the set define a
 * set of roots.
 *
 * A cyclic graph may not have roots.
 */
void GraphRootFinder::findRoots(Graph * g, GraphNodeSet* rootSet)
{
  rootSet->clear();
  // Start by inserting all nodes into the set of roots.
  for (Iter<GraphNode*> n = g->nodes(); not n.atEnd(); ++n) 
  {
    GraphNode* node = *n;
    rootSet->insert(node);
  }
  // Remove all nodes from the root set which have allowed, inbound edges.
  for (Iter<GraphNode*> n = g->nodes(); not n.atEnd(); ++n) 
  {
    GraphNode* node = *n;
    for (Iter<GraphEdge*> iter = g->edges(node); !iter.atEnd(); ++iter)
    {
      GraphEdge* edge = *iter;
      if (processEdge(g,edge)) {
        GraphNode * fanout = g->endPointOf(edge);
        rootSet->erase(fanout);
      }
    }
  }
  // Nodes remaining in the set are the roots.
}
