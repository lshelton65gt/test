// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtStackTrace.h"
#include "util/UtArray.h"
#include "util/Zstream.h"
#include <stdio.h>
#if __linux
#include <execinfo.h>		// For backtrace()
#endif

struct StackFrame {
  StackFrame* next;
  void* PC;
};


// Define static members
void* UtStackTrace::mMainFrame = NULL;
SInt32 UtStackTrace::smMaxStackDepth = 0; // The largest stacktrace we've seen


#define USE_BACKTRACE pfLINUX || pfLINUX64

void UtStackTrace::grab(SInt32 
#if USE_BACKTRACE
 ignoreFrames
#endif
)
{
#if USE_BACKTRACE

  // Use glibc's backtrace() function.
  mNum = backtrace(mFrames, MAX_STACK_DEPTH);
  // Scootch each entry up ignoreFrames + 1 frames.
  for (int i = 0; i < mNum and i + ignoreFrames + 1 < MAX_STACK_DEPTH; i++)
    mFrames[i] = mFrames[i + ignoreFrames + 1];
  mNum -= (ignoreFrames + 1);
  if (mNum < 0)
    mNum = 0;			// Having fewer than 0 frames is bad.

#elif pfGCC

  void* bfa;			// Holds __builtin_frame_address() result
  int i = 0;

  #define GET_FRAME(f)			\
    bfa = __builtin_return_address(f);	\
    if (bfa != NULL and			\
	bfa != mMainFrame)		\
    {					\
      mFrames[i++] = bfa;
  #define DONE_GET_FRAME } 

  // MAX_STACK_TRACE=15, so we can record frames 1 to 15 inclusive

  GET_FRAME(1)
    GET_FRAME(2)
      GET_FRAME(3)
        GET_FRAME(4)
          GET_FRAME(5)
            GET_FRAME(6)
              GET_FRAME(7)
                GET_FRAME(8)
                  GET_FRAME(9)
                    GET_FRAME(10)
                      GET_FRAME(11)
                        GET_FRAME(12)
                          GET_FRAME(13)
                            GET_FRAME(14)
                              GET_FRAME(15)
                              DONE_GET_FRAME
                            DONE_GET_FRAME
                          DONE_GET_FRAME
                        DONE_GET_FRAME
                      DONE_GET_FRAME
                    DONE_GET_FRAME
                  DONE_GET_FRAME
                DONE_GET_FRAME
              DONE_GET_FRAME
            DONE_GET_FRAME
          DONE_GET_FRAME
        DONE_GET_FRAME
      DONE_GET_FRAME
    DONE_GET_FRAME
  DONE_GET_FRAME

  mNum = i;
#endif

  if (mNum > smMaxStackDepth)
    smMaxStackDepth = mNum;
} // void UtStackTrace::grab

// On Linux64, __builtin_frame_address(), __builtin_return_address(), and
// framePtr->next are all non-zero even when asking for the frame below
// main()'s.  A few frames past that we segfault.  To avoid that, we allow
// main() to tell us it's frame address so we can avoid traversing beyond that.
void UtStackTrace::putMainFrame(void* frame)
{
  mMainFrame = frame;
}

UtExeSymbolTable::UtExeSymbolTable()
{
}

UtExeSymbolTable::~UtExeSymbolTable()
{
  clear();
}

void UtExeSymbolTable::clear() {
  for (UInt32 i = 0; i < mEntries.size(); ++i)
  {
    Entry* e = mEntries[i];
    delete e;
  }
  mEntries.clear();
}

// For low detail histogramming we will select the most "interesting"
// entry on the stack frame and print that.  Uninteresting stack frames
// include OS/lib artifacts like __end, std::__simple_alloc, and the
// like.  This function
bool UtExeSymbolTable::isInteresting(const char* proc)
{
  // hash_set is unfortunately part of the __gnu_cxx namespace, and we want it
  if (strncmp(proc, "__gnu_cxx::hash_", 16) == 0)
    return true;

  // Other classes prefixed with _ are probably not interesting
  if ((strncmp(proc, "std::_", 6) == 0) ||
      (strncmp(proc, "_", 1) == 0) ||
      (strncmp(proc, ".", 1) == 0) ||
      (strncasecmp(proc, "carbonmem", 9) == 0) ||
      (strncmp(proc, "CarbonSTLAlloc", 14) == 0))
    return false;
  return true;
}

// class for the current template level as we are parsing a complex
// nested template.  Only the first template argument is interesting
// in the context of memory management, unless it's a map of some
// sort, and then the first 2 arguments are interesting.
class TemplateState
{
public: CARBONMEM_OVERRIDES
  TemplateState(const UtString& name, bool isInteresting):
    mName(name),
    mIsInteresting(isInteresting),
    mArgCount(1) {}
  ~TemplateState() {}
  bool isMap() const {
    return ((strstr(mName.c_str(), "map") != NULL) ||
            (strstr(mName.c_str(), "pair") != NULL));
  }
  bool isInteresting() const {return mIsInteresting;}
  void bumpArgCount() {
    ++mArgCount;
    if (mIsInteresting && ((mArgCount > 2) || !isMap()))
      mIsInteresting = false;
  }

private:
  UtString mName;
  bool mIsInteresting;
  int mArgCount;
};
    

// For detail=0 and detail=1, simplify a procedure name by removing 
// uninteresting template parameters.  We assume only the first
// template parameters are interesting, except for maps, where the
// first two are interesting.
const char* UtExeSymbolTable::simplify(const char* proc, UtString* buf)
{
  // (Default template parameter doesn't appear to work on VC++ .NET 2003)
  UtArray<TemplateState*> templateStack;
  TemplateState* templateState = NULL;
  buf->clear();
  for (StrToken tok(proc, "<,>"); !tok.atEnd(); ++tok)
  {
    bool isInteresting = ((templateState == NULL) ||
                          templateState->isInteresting());
    if (isInteresting)
      *buf << *tok;

    if (strstr(*tok, "operator") != NULL)
      *buf << tok.curDelim();   // do not do template processing...
    else
    {
      switch (tok.curDelim())
      {
      case '<':
        tok.putDelim("<,>");      // can't start with , because I don't want
                                  // to bother counting proc args, only template
                                  // args.
        templateState = new TemplateState(*tok, isInteresting);
        templateStack.push_back(templateState);
        *buf << "<";
        break;
      case '>':
        *buf << ">";
        if (templateState != NULL)
        {
          delete templateState;
          templateStack.pop_back();
          if (templateStack.empty())
            templateState = NULL;
          else
            templateState = templateStack.back();
        }
        break;
      case ',':
        *buf << ",";
        if (templateState != NULL)
          templateState->bumpArgCount();
        break;
      };
    } // else

  } // for

  // our imperfect parser might get confused by operator<< or something....
  while (!templateStack.empty())
  {
    delete templateStack.back();
    templateStack.pop_back();
  }

  return buf->c_str();
}


bool UtExeSymbolTable::load(const char* exeName)
{
#if pfLINUX || pfLINUX64
  bool ret = true;
  // Grab the symbol table for the executable via nm
  if (mEntries.empty())
  {
    fprintf(stdout, "reading symbol table ...\n");
    fflush(stdout);
    UtString cmd = "nm";
    UtString nmPath, dummy;
    if (OSExpandFilename(&nmPath, "$CARBON_HOME/$CARBON_HOST_ARCH/bin/nm", &dummy))
    {
      // If there's a file there and it's executable, use it.
      if (OSStatFile(nmPath.c_str(), "x", &dummy) == 1)
        cmd = nmPath;
    }
    cmd << " -n " << exeName << " | c++filt";
    FILE* f = popen(cmd.c_str(), "r");
    if (f == NULL)
    {
      perror(cmd.c_str());
      ret = false;
    }
    else
    {
      char buf[5000];
      while (fgets(buf, 5000, f) != NULL)
      {
        // Looking for line like "080506fc T _init"
        StrToken tok(buf);
        UInt32 addr;
        if (!tok.atEnd() && tok.parseNumber(&addr, 16))
        {
          ++tok;
          if (!tok.atEnd())     // mode -- ignore it
          {
            tok.putDelim("\n"); // grab rest of line
            ++tok;
            if (!tok.atEnd()) {
              mEntries.push_back(new Entry(addr, StringUtil::skip(*tok)));
            }
          }
        }
      }
      fclose(f);
      UtExeSymbolTable::Cmp cmp;
      sort(mEntries.begin(), mEntries.end(), cmp);
    }
  } // if

  if (mEntries.empty())
  {
    fprintf(stdout, "%s symbol table empty\n", exeName);
    ret = false;
  }
  return ret;
#else
  fprintf(stdout, "%s Symbol table translation not supported on Windows\n", exeName);
  fflush(stdout);
  return false;
#endif
} // bool UtExeSymbolTable::load

bool UtExeSymbolTable::save(ZostreamDB* db)
{
  if (! (*db << ((UInt32) mEntries.size())))
    return false;
  for (UInt32 i = 0; i < mEntries.size(); ++i)
  {
    Entry* e = mEntries[i];
    UInt32 address = e->first;
    UtString& function = e->second;
    if (! (*db << address) ||
        ! (*db << function))
      return false;
  }
  return true;
}

bool UtExeSymbolTable::restore(ZistreamDB* db)
{
  UInt32 numEntries;
  if (! (*db >> numEntries))
    return false;
  clear();
  mEntries.resize(numEntries);
  for (UInt32 i = 0; i < numEntries; ++i)
  {
    Entry* e = CARBON_NEW(Entry);
    mEntries[i] = e;
    if (! (*db >> e->first) ||
        ! (*db >> e->second))
      return false;
  }
  return true;
}
