/*****************************************************************************

 Copyright (c) 2003-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/carbon_hashtable.h"
#include "util/carbon_hashtable_itr.h"
#include "carbon_hashtable_priv.h"

#include "carbon/c_memmanager.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define LOADFACTOR 0.74

/* 
   from good hash primes at 
   http://planetmath.org/encyclopedia/GoodHashtablePrimes.html
*/
/*
 * Store the number of buckets and the load limit.  Note
 * here how many bytes the array takes as well.  We should
 * try to keep the carbon memory manager optimized for those
 * sizes.  It turns out the these sizes match those used by
 * SGI hash tables so this optimization has already been done.
 *
 * Note that there are 28 primes in this table so we need
 * 5 bits for the primeIndex, leaving us 32-5=27 bits for the
 * maximum table size, which is 128M.  Is that enough?  A 128M
 * table would take 128M*8bytes=1Gbytes just for the linked
 * list of entries.  So you could really just have 3 of those,
 * assuming they were just sets of integers.  So on 32-bits
 * it seems reasonble to allow just 27 bits for the entry count.
 * On 64 bit machines we should not enforce that limitation, because
 * it would be truly limiting.
 */
#define SIZES_ENTRY(n) { n, (unsigned int) (n * LOADFACTOR + 1) }
const struct carbonHashTableSizes carbonPrimes[] = 
{                               /* array bytes */
    {1, 5},                     /* Stay with 1 bucket till there are 5 elements */
    SIZES_ENTRY(23),            /* 92 */
    SIZES_ENTRY(53),            /* 212 */
    SIZES_ENTRY(97),            /* 388 */
    SIZES_ENTRY(193),           /* 722 */
    SIZES_ENTRY(389),           /* 1556 */
    SIZES_ENTRY(769),           /* 3076 */
    SIZES_ENTRY(1543),          /* 6172 */
    SIZES_ENTRY(3079),          /* 12316 */
    SIZES_ENTRY(6151),          /* 24604 */
    SIZES_ENTRY(12289),         /* 49156 */
    SIZES_ENTRY(24593),         /* ... exceeds CarbonMem's 64k limit */
    SIZES_ENTRY(49157),
    SIZES_ENTRY(98317),
    SIZES_ENTRY(196613),
    SIZES_ENTRY(393241),
    SIZES_ENTRY(786433),
    SIZES_ENTRY(1572869),
    SIZES_ENTRY(3145739),
    SIZES_ENTRY(6291469),
    SIZES_ENTRY(12582917),
    SIZES_ENTRY(25165843),
    SIZES_ENTRY(50331653),
    SIZES_ENTRY(100663319),
    SIZES_ENTRY(201326611),
    SIZES_ENTRY(402653189),
    SIZES_ENTRY(805306457),
    SIZES_ENTRY(1610612741)
  };

static const UInt32 scNumPrimes = sizeof(carbonPrimes)/sizeof(*carbonPrimes);

/*
 * To compute the byte-size of the bucket-array, we use the size of the
 * small-pointer
 */
#define HASHBUCKET_SIZE(nbuckets) \
  (sizeof(carbon_hashEntryPtr) * (nbuckets))

#ifndef __STRING
#define __STRING(exp) #exp
#endif

#define CHASH_ASSERT(exp, msg) \
  do { \
    if (! (exp)) { \
      fprintf(stderr,"\n\n******CARBON INTERNAL ERROR*******\n\n"); \
      fprintf(stderr, "%s\n", (msg)); \
      fprintf(stderr, "%s:%d CHASH_ASSERT(%s) failed\n", __FILE__, __LINE__,  __STRING(exp)); \
      abort(); \
    } \
  } while (0) /* no trailing semicolon */

/*****************************************************************************/
struct carbon_hashtable *
create_carbon_hashtable(int size_index)
{
  struct carbon_hashtable* h =
    carbonmem_alloc(sizeof(struct carbon_hashtable));
  carbon_hashtable_init(h, size_index);
  return h;
}

void carbon_hashtable_init(struct carbon_hashtable * h, int size_index)
{
  UInt32 i;
  UInt32 size = carbonPrimes[size_index].numBuckets;

  // Clear the entire structure containing size and prime Index in
  // one shot, which makes it look cleaner to valgrind.
  memset(h, 0, sizeof(*h));
  
  if (size_index != 0) {
    h->data.table = carbonmem_alloc(HASHBUCKET_SIZE(size));
    for (i=0; i<size; ++i)
      h->data.table[i] = 0/*NULL*/;    /* No need to shrink NULL */
  }
  h->sizes.primeIndex   = size_index;
  h->sizes.entrycount   = 0;
}

/*****************************************************************************/
/* The key passed is already expanded if necessary. */
#define BUCKET_INDEX(primeIndex, entry, hashFn, userData) \
  (hashFn(userData, entry) % carbonPrimes[primeIndex].numBuckets)

/*****************************************************************************/
static int
carbon_hashtable_expand(struct carbon_hashtable *h, carbonHashFn hashFn,
                        void* userData)
{
  /* Double the size of the table to accomodate more entries */
  carbon_hashEntryPtr *newtable;
  UInt32 newsize, i, index, oldsize, bytes, idx;
  carbon_hashEntryPtr * table;
  
  /* Check we're not hitting max capacity */
  if (h->sizes.primeIndex >= scNumPrimes ) return 0;
  
  idx = h->sizes.primeIndex;
  oldsize = carbonPrimes[idx].numBuckets;
  ++idx;
  newsize = carbonPrimes[idx].numBuckets;
  
  bytes = HASHBUCKET_SIZE(newsize);
  newtable = carbonmem_alloc(bytes);
  memset(newtable, 0, bytes);
  /* This algorithm is not 'stable'. ie. it reverses the list
   * when it transfers entries between the tables */
  table = HASH_GET_TABLE(h);
  for (i = 0; i < oldsize; ++i, ++table) {
    carbon_hashEntry* e = HASH_EXPAND_ENTRY(*table);
    carbon_hashEntry* next;

    for (; e != NULL; e = next) {
      next = HASH_EXPAND_ENTRY(e->next);
      index = BUCKET_INDEX(idx, e, hashFn, userData);
#ifdef CDB
      CHASH_ASSERT(index < carbonPrimes[idx].numBuckets,
                   "Hash bucket index overflow.");
#endif
      e->next = newtable[index];
      newtable[index] = SMALLPTR_SHRINK(e);
    }
  }
  h->sizes.primeIndex = idx;

  if (oldsize != 1) {
    carbonmem_dealloc(h->data.table, HASHBUCKET_SIZE(oldsize));
  }
  h->data.table = newtable;

  return -1;
}

// Print the number of elements in each bucket
void carbon_hashtable_print(struct carbon_hashtable *h) {
  int numBuckets = carbonPrimes[h->sizes.primeIndex].numBuckets;
  int i, j;
  int total = 0;
  carbon_hashEntryPtr *table = HASH_GET_TABLE(h);
  for (i = 0; i < numBuckets; ++i, ++table) {
    carbon_hashEntry* e = HASH_EXPAND_ENTRY(*table);
    j = 0;
    for (; e != NULL; e = HASH_EXPAND_ENTRY(e->next)) {
      ++j;
    }
    total += j;
    fprintf(stdout, "Bucket %d:\t%d entries\n", i, j);
  }
  fprintf(stdout, "Total %d entries\n", total);
  if (total != (int)h->sizes.entrycount) {
    fprintf(stdout, "Sizes mismatch, entrycount=%d\n", h->sizes.entrycount);
  }
}


// Print a summary of the provided hashtable, for large hashtables this is better than carbon_hashtable_print
void carbon_hashtable_histogram(struct carbon_hashtable *h) {
  int numBuckets = carbonPrimes[h->sizes.primeIndex].numBuckets;
  int i, j;
  int largest_idx = 0;
  int largest_val = 0;
  int maximum_size= 0;
  int total = 0;
#define PRINT_MAX 200
  int histogram[PRINT_MAX] = {0}; // a histogram for the first PRINT_MAX sizes
  int print_last = PRINT_MAX-1;

  carbon_hashEntryPtr *table = HASH_GET_TABLE(h);
  for (i = 0; i < numBuckets; ++i, ++table) {
    carbon_hashEntry* e = HASH_EXPAND_ENTRY(*table);
    j = 0;
    for (; e != NULL; e = HASH_EXPAND_ENTRY(e->next)) {
      ++j;
    }
    if ( j > largest_val ){
      largest_idx = i;
      largest_val = j;
    }
    total += j;
    if ( j > maximum_size ){
	maximum_size = j;
    }
    
    if ( j < 200 ) histogram[j]++;
  }
  fprintf(stdout, "Total of %d entries found\n", total);
  if (total != (int)h->sizes.entrycount) {
    fprintf(stdout, "Error: Size mismatch, actual count: %d does not match entrycount: %d\n", total, h->sizes.entrycount);
  }

  if ( print_last > maximum_size ) {
    print_last = maximum_size;
  }
  fprintf(stdout, "Histogram of hash entry sizes:\nSize        Count\n");
  for (i = 0; i <= print_last; ++i) {
    fprintf(stdout, "%4d %12d\n", i, histogram[i]);
  }
  fprintf(stdout, "All other entry sizes have a count of zero.\n");
  fprintf(stdout, "The hashEntry table at index %d is the largest with %d entries\n", largest_idx, largest_val);
}

/*****************************************************************************/
UInt32
carbon_hashtable_count(const struct carbon_hashtable *h)
{
  return h->sizes.entrycount;
}

/*****************************************************************************/
/* Convenience function for use by carbon_hashtable_map_insert() and
   carbon_hashtable_set_insert(). */
void
carbon_hashtable_insert(struct carbon_hashtable *h,
			carbonHashFn hashFn,
                        void* userData,
			carbon_hashEntry** entry,
                        unsigned int entrySize,
                        UInt32 hashVal)
{
  carbon_hashEntryPtr * pE;

  if (++(h->sizes.entrycount) > carbonPrimes[h->sizes.primeIndex].loadLimit) {
    carbon_hashtable_expand(h, hashFn, userData);
  }

  *entry = carbonmem_alloc(entrySize);
  {
    HASH_FIND_BUCKET_PTR(pE, h, hashVal, bucketIndex);
  }
  (*entry)->next = *pE;
  *pE = SMALLPTR_SHRINK(*entry);
}

/*****************************************************************************/
int /* returns 1 if found, 0 if not */
carbon_hashtable_search(const struct carbon_hashtable *h,
                        const void* k,
			carbonHashFn hashFn,
			carbonHashEqFn keyEqFn,
                        void* userData,
			struct carbon_hashtable_itr* hIter,
                        UInt32 hashVal)
{
//  carbon_hashtable_iterator_nullinit(hIter, (struct carbon_hashtable*) h);
  HASH_FIND_BUCKET(carbon_hashEntry *e, h, hashVal, bucketIndex);

  hIter->prev = NULL;
  while (NULL != e) {
    // Call the hash function on the stored data to guard against the
    // user specifying a pickier hash function than their eq func.
    if (keyEqFn(userData, k, e)) {
#ifdef CDB  // This costs 2% in cbuild, and probably about 1% in runtime
      if (hashFn(userData, e) != hashVal) {
        void** p1 = (void**) k;
        void** p2 = (void**) (((char*) e) + sizeof(carbon_hashEntry));
        char buf[200];
        sprintf(buf, "Hash-table inconsistency: %p vs %p", p1, p2);
        CHASH_ASSERT(0, buf);
      }
#else
      (void) hashFn;
#endif
      hIter->e = e;
      hIter->index = bucketIndex;
      hIter->h = (struct carbon_hashtable*) h;
      return 1;
    }
    hIter->prev = e;
    e = HASH_EXPAND_ENTRY(e->next);
  }
  return 0;
}

// Like search(), but doesn't fill an iterator.  Returns the hashEntry or
// NULL
carbon_hashEntry*
carbon_hashtable_findEntry(const struct carbon_hashtable *h,
                           const void* k,
                           carbonHashFn hashFn,
                           carbonHashEqFn keyEqFn,
                           void* userData,
                           UInt32 hashVal)
{
//  carbon_hashtable_iterator_nullinit(hIter, (struct carbon_hashtable*) h);
  HASH_FIND_BUCKET(carbon_hashEntry *e, h, hashVal, bucketIndex);

  while (NULL != e) {
    // Call the hash function on the stored data to guard against the
    // user specifying a pickier hash function than their eq func.
    if (keyEqFn(userData, k, e)) {
#ifdef CDB  // This costs 2% in cbuild, and probably about 1% in runtime
      if (hashFn(userData, e) != hashVal) {
        void** p1 = (void**) k;
        void** p2 = (void**) (((char*) e) + sizeof(carbon_hashEntry));
        char buf[200];
        sprintf(buf, "Hash-table inconsistency: %p vs %p", p1, p2);
        CHASH_ASSERT(0, buf);
      }
#else
      (void) hashFn;
#endif
      return e;
    }
    e = HASH_EXPAND_ENTRY(e->next);
  }
  return 0;
}

/*****************************************************************************/
/* returns 1 if an insertion was needed, 0 if the item was already
   in the set
 */
int carbon_hashtable_maybe_insert(struct carbon_hashtable *h,
                                  const void* k,
                                  carbonHashFn hashFn,
                                  carbonHashEqFn keyEqFn,
                                  void* userData,
                                  UInt32 hashVal,
                                  UInt32 entrySize,
                                  carbon_hashEntry** pEntry)
{
  carbon_hashEntry* e;
  HASH_FIND_BUCKET_PTR(carbon_hashEntryPtr *pE, h, hashVal, bucketIndex);

  for (e = HASH_EXPAND_ENTRY(*pE); e != NULL; e = HASH_EXPAND_ENTRY(e->next)) {
    // Call the hash function on the stored data to guard against the
    // user specifying a pickier hash function than their eq func.
    if (keyEqFn(userData, k, e)) {
#ifdef CDB  // This costs 2% in cbuild, and probably about 1% in runtime
      if (hashFn(userData, e) != hashVal) {
        void** p1 = (void**) k;
        void** p2 = (void**) (((char*) e) + sizeof(carbon_hashEntry));
        char buf[200];
        sprintf(buf, "Hash-table inconsistency: %p vs %p", p1, p2);
        CHASH_ASSERT(0, buf);
      }
#endif
      *pEntry = e;
      return 0;              /* item already in set */
    }
  }

  /* If an expansion is required, we will need to re-find the bucket */
  if (++(h->sizes.entrycount) > carbonPrimes[h->sizes.primeIndex].loadLimit) {
    carbon_hashtable_expand(h, hashFn, userData);
    {
      HASH_FIND_BUCKET_PTR(pE, h, hashVal, bucketIndex);
    }
  }

  e = carbonmem_alloc(entrySize);
  e->next = *pE;
  *pE = SMALLPTR_SHRINK(e);
  *pEntry = e;

  return 1;
}

void
carbon_hashtable_clear(struct carbon_hashtable* h, unsigned int entrySize)
{
  if (h->sizes.entrycount > 0)
  {
    struct carbon_hashtable_itr itr;
    carbon_hashtable_iterator_init(&itr, h);
    while (h->sizes.entrycount > 0)
    {
      carbon_hashtable_iterator_remove_current(&itr, entrySize);
      carbon_hashtable_iterator_advance(&itr);
    }
  }
}

/*!
 This is a highly specialized routine used by UtHashMap2 to clear all
 the bucket pointers following UtHashMap2::RemoveLoop.  Do not use this
 for any other purpose.  Note: this is a procedure rather than a macro
 because it needs access to the primes table which is private to
 carbon_hashtable.c.
*/
void
carbon_hashtable_clear_internal(struct carbon_hashtable* h)
{
  UInt32 numEntries = carbonPrimes[h->sizes.primeIndex].numBuckets;
  memset(HASH_GET_TABLE(h), 0, numEntries * sizeof(carbon_hashEntryPtr));
  h->sizes.entrycount = 0;
}

/* Clear and initialize the table to be efficiently filled with the same number
   of elements again.
   Used by SparseDynTempMemory to improve performance when a the temp memory is
   instantiated and repeatedly filled and emptied (often with similar #'s of values).
 */
void
carbon_hashtable_clear_and_resize(struct carbon_hashtable* h, unsigned int entrySize)
{
  UInt32 newIndex, index;
  UInt32 old_entries = h->sizes.entrycount;
  carbon_hashtable_clear( h, entrySize);
  
  index = h->sizes.primeIndex;
  newIndex = index;
  
  // See if this table to too big for the existing entries.  If it is downsize it.
  if( newIndex > 0 && (carbonPrimes[--newIndex].loadLimit >= old_entries) ) {
    carbon_hashEntryPtr *newtable;
    UInt32 bytes;
    UInt32 oldsize = carbonPrimes[index].numBuckets;
    if(oldsize != 1)
      carbonmem_dealloc(h->data.table, HASHBUCKET_SIZE(oldsize));
    
    // Count down the table sizes until we find the one that fits.
    while( newIndex > 0 &&
           (carbonPrimes[--newIndex].loadLimit >= old_entries) )
    {
    }
    h->sizes.primeIndex = newIndex;
    bytes = HASHBUCKET_SIZE(carbonPrimes[newIndex].numBuckets);
    if (newIndex != 0) {
      newtable = carbonmem_alloc( bytes );
      memset(newtable, 0, bytes);
      h->data.table = newtable;
    }
    else {
      h->data.table = NULL;     /* A real pointer could be larger in the union, zero it */
    }
  }
}

/*****************************************************************************/
int /* returns 1 if found, 0 if not */
carbon_hashtable_remove(struct carbon_hashtable *h, const void* k,
			carbonHashFn hashFn, carbonHashEqFn keyEqFn,
                        void* userData,
                        unsigned int entrySize)
{
  /* TODO: consider compacting the table when the load factor drops enough,
   *       or provide a 'compact' method. */
  
  HASH_FIND_BUCKET_PTR(carbon_hashEntryPtr* pE, h, hashFn(userData, k),
                       bucketIndex);
  carbon_hashEntry *e = HASH_EXPAND_ENTRY(*pE);

  while (NULL != e)
  {
    if (keyEqFn(userData, k, e)) {
      *pE = e->next;
      h->sizes.entrycount--;
      carbonmem_dealloc(e, entrySize);
      return 1;			/* found */
    }
    pE = &(e->next);
    e = HASH_EXPAND_ENTRY(e->next);
  }
  return 0;			/* didn't find */
}

/***********************************************************************/
/* destroy */
void
carbon_hashtable_destroy(struct carbon_hashtable *h, unsigned int entrySize)
{
  carbon_hashtable_clean(h, entrySize);
  carbonmem_dealloc(h, sizeof(struct carbon_hashtable));
}
void
carbon_hashtable_clean(struct carbon_hashtable *h, unsigned int entrySize)
{
  UInt32 idx = h->sizes.primeIndex;
  UInt32 i;
  carbon_hashEntryPtr *table = HASH_GET_TABLE(h);
  UInt32 size = carbonPrimes[idx].numBuckets;
  carbon_hashtable_clear(h, entrySize);

  for (i = 0; i < size; ++i, ++table) {
    carbon_hashEntry *e = HASH_EXPAND_ENTRY(*table);
    while (NULL != e) {
      carbon_hashEntry *next = HASH_EXPAND_ENTRY(e->next);
      carbonmem_dealloc(e, entrySize);
      e = next;
    }
  }
  
  if (idx != 0) {
    carbonmem_dealloc(h->data.table, HASHBUCKET_SIZE(size));
    h->data.table = NULL;
    h->sizes.primeIndex = 0;
    h->sizes.entrycount = 0;
  }
}

#if 0
int
carbon_hashtable_change(struct carbon_hashtable *h, const void* k, void *v,
			carbonHashFn hashFn, carbonHashEqFn keyEqFn,
                        void* userData,
                        enum KeyValKind keyKind,
                        enum KeyValKind valKind)
{
  carbon_hashEntry *e;
  UInt32 index;
  index = bucketIndex(h->sizes.primeIndex, k, hashFn, userData);
  e = HASH_EXPAND_ENTRY(h->table[index]);
  while (NULL != e)
    {
      /* Check hash value to short circuit heavier comparison */
      if (keyEqFn(userData, k, EXPAND_KEY_POINTER(e->k, keyKind))) {
	e->v = (valKind == ePointer) ? shrinkPtr(v) : (SmallPtr)(UIntPtr)v;
        return -1;
      }
      e = HASH_EXPAND_ENTRY(e->next);
    }
  return 0;
}
#endif


/*
 * Copyright (C) 2002 Christopher Clark <firstname.lastname@cl.cam.ac.uk>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */
