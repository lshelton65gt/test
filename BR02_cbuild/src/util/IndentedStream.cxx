// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonAssert.h"
#include "util/IndentedStream.h"
#include "util/UtIOStream.h"
#include "util/CodeStream.h"

template <typename _SinkType>
IndentedStream <_SinkType>::IndentedStream (_SinkType &sink, const UInt32 options) :
  mSink (sink), mOptions (options), mNeedsSpace (false), mNeedsNewline (false), 
  mMaxWidth (cWIDTH), mWidth (0), mDepth (options & cINITIAL_INDENT_MASK),
  mNoAuto (options & cNOAUTO)
{
  if (options & cMAX_WIDTH_MASK) {
    mMaxWidth = (options & cMAX_WIDTH_MASK) >> 8;
  }
}

template <typename _SinkType>
/*virtual*/ IndentedStream <_SinkType>::~IndentedStream ()
{
  if (mNeedsNewline || mWidth > 0) {
    mSink.write ("\n", 1);
  }
}

template <typename _SinkType>
void IndentedStream <_SinkType>::doGlue (const char next_char)
{
  if (next_char == ')') {
    // do not put spaces or newlines before a closing parenthesis
    mNeedsSpace = false;
  } else if (mNeedsNewline) {
    // output a new line and indent one space for each nesting level
    mSink.write ("\n", 1);
    mWidth = 0;
  } else if (mNeedsSpace && mWidth > 0) {
    // output a single white space
    mSink.write (" ", 1);
    mNeedsSpace = false;
    mWidth++;
  }
  if (mWidth == 0) {
    static char spaces [33] = "                                ";
    while (mWidth < mDepth) {
      UInt32 residue = mDepth - mWidth;
      if (residue <= 32) {
        mSink.write (spaces, residue);
        mWidth += residue;
      } else {
        mSink.write (spaces, 32);
        mWidth += 32;
      }
    }
    mNeedsNewline = false;
    mNeedsSpace = false;
  }
}

template <typename _SinkType>
void IndentedStream <_SinkType>::output (const char *s)
{
  int n = 0;
  while (s [n] != '\0') {
    switch (s [n]) {
    case '\0':
      INFO_ASSERT (s [n] != '\0', "run off the end");
      break;
    case '\t':
      mNeedsSpace = true;
      mNeedsNewline |= mWidth >= mMaxWidth;
      n++;
      break;
    case '\n':
      mNeedsNewline = true;
      n++;
      break;
    default:
      {
        bool end_of_text = false;
        UInt32 new_depth = mDepth;
        int m = n;
        do {
          switch (s [n]) {
          case '\0': case '\t': case '\n':
            end_of_text = true;
            break;
          case '(': case '[': case '{':
            n++;
            if (!mNoAuto) {
              new_depth++;
            }
            break;
          case ')': case ']': case '}':
            n++;
            if (!mNoAuto) {
              new_depth--;
            }
            break;
          case '\\':
            INFO_ASSERT (s [n+1] != '\0', "trailing \\");
            n++;
            break;
          default:
            n++;
            break;
          }
        } while (!end_of_text);
        INFO_ASSERT (n > m, "bogus segment");
        doGlue (s [m]);
        mDepth = new_depth;
        mSink.write (s + m, n - m);
        mWidth += n - m;
      }
    }
  }
}

template <typename _SinkType>
void IndentedStream <_SinkType>::nl (bool force)
{
  if (mWidth != 0 || force) {
    mSink.write ("\n", 1);
    mWidth = 0;
    mNeedsNewline = false;
    mNeedsSpace = false;
  }
}

template <typename _Sink>
IndentedStream<_Sink> &operator << (IndentedStream <_Sink> &sink, const char *s)
{
  sink (s);
  return sink;
}

template <typename _Sink>
IndentedStream<_Sink> &operator << (IndentedStream <_Sink> &sink, const UInt32 n)
{
  char buffer [64];
  snprintf (buffer, sizeof (buffer), "%d", n);
  sink (buffer);
  return sink;
}

// explicit instantiations
template class IndentedStream <UtOStream>;
template class IndentedStream <CodeStream>;
template IndentedStream <UtOStream> &operator << (IndentedStream <UtOStream> &, const char *);
template IndentedStream <UtOStream> &operator << (IndentedStream <UtOStream> &, const UInt32);
template IndentedStream <CodeStream> &operator << (IndentedStream <CodeStream> &, const char *);
template IndentedStream <CodeStream> &operator << (IndentedStream <CodeStream> &, const UInt32);
