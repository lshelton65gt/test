// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtProcess.h"
#include <QProcess>
#include <QCoreApplication>

UtProcessContext::UtProcessContext(int* argc, char** argv) {
  mApplication = new QCoreApplication(*argc, argv);
}

UtProcessContext::~UtProcessContext() {
  delete mApplication;
}

UtProcess::UtProcess(UtProcessContext*, char** args) {
  mErrCode = -1;
  mProcess = new QProcess;
  mProcess->setReadChannelMode(QProcess::MergedChannels);

  QStringList qargs;
  QString prog = *args;
  for (++args; *args != NULL; ++args) {
    QString arg = *args;
    qargs << arg;
  }
  mProcess->start(prog, qargs);
  if (!mProcess->waitForStarted()) {
    mErrCode = 1;
    const char* emsg = "unknown error";
    switch (mProcess->error()) {
    case QProcess::FailedToStart: emsg = "FailedToStart"; break;
    case QProcess::Crashed: emsg = "Crashed"; break;
    case QProcess::Timedout: emsg = "Timedout"; break;
    case QProcess::WriteError: emsg = "WriteError"; break;
    case QProcess::ReadError: emsg = "ReadError"; break;
    case QProcess::UnknownError: emsg = "UnknownError"; break;
    }
    mOutputBuf << emsg << "\n";
    delete mProcess;
    mProcess = NULL;
  }
} // UtProcess::UtProcess

UtProcess::~UtProcess() {
  if (mProcess != NULL) {
    wait();
  }
}

int UtProcess::wait() {
  if (mProcess != NULL) {
    if (! mProcess->waitForFinished(-1)) {
      mOutputBuf << "QProcess::waitForFinished(-1) returned false\n";
      mErrCode = 1;
    }
    else {
      QByteArray bytes = mProcess->readAll();
      const char* buf = bytes;
      mOutputBuf << buf;
      mErrCode = mProcess->exitCode();
    }
    delete mProcess;
    mProcess = NULL;
  }
  return mErrCode;
}
