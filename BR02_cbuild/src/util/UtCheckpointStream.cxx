// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtCheckpointStream.h"
#include "util/UtString.h"

static const UInt32 sMaxNormalStringLen = 128;

UtICheckpointStream& UtICheckpointStream::operator >> (UtString &str)
{
  UInt32 len;

  str.clear();  // empty the string
  
  // get the length of the string.
  read(&len, sizeof(len));

  // If the length is bigger than 'normal', the next thing we should
  // see is the max 'normal' length, as a sanity test.  The idea is to
  // prevent us from reading a garbage length and then using that to
  // read, say, 4 gigabyes of string data.
  if (len > sMaxNormalStringLen) {
    UInt32 sanity;
    read(&sanity, sizeof(sanity));
    if (sanity != sMaxNormalStringLen) {
      mFailed = true;
      
      UtString message;
      message << "String length sanity check failed reading " 
              << mName << ". Requested string length: " << len;
      (*mErrorFunc)(mUserContext, message);
    }
  }

  if (!mFailed) {
    // make string big enough to hold the text
    str.resize(len);
  
    // read the characters
    read(str.getBuffer(), len);
  }

  return *this;
}

UInt32 UtICheckpointStream::read(void *buffer, UInt32 numBytes)
{
  UInt32 bytesRead = (*mReadFunc)(mUserContext, buffer, numBytes);
  if (bytesRead != numBytes) {
    mFailed = true;
    UtString message;
    message << "Expected " << numBytes << " bytes from " 
            << mName << ", but " << bytesRead << " bytes were read.";
    (*mErrorFunc)(mUserContext, message);
  }
  return bytesRead;
}

bool UtICheckpointStream::checkToken(const char *token)
{
  UtString t;

  operator >>(t);
  if (t != token) {
    UtString message;
    message << "Expected token \"" << token << "\" not found reading " << mName;
    (*mErrorFunc)(mUserContext, message);
    return false;
  }
  return true;
}

///////////////////////////////////////////////////////////////////////////////

UtOCheckpointStream& UtOCheckpointStream::operator << (const char *str)
{
  UInt32 len = std::strlen(str);

  // write length of string
  write(&len, sizeof(len));

  // If the length is unusually long, write the max expected length
  // out as a sanity check.
  if (len > sMaxNormalStringLen) {
    write(&sMaxNormalStringLen, sizeof(sMaxNormalStringLen));
  }

  // write the actual text
  write(str, len);

  return *this;
}

UInt32 UtOCheckpointStream::write(const void *buffer, const UInt32 numBytes)
{
  UInt32 bytesWritten = (*mWriteFunc)(mUserContext, buffer, numBytes);
  if (bytesWritten != numBytes) {
    mFailed = true;
    UtString message;
    message << "Attempted to write " << numBytes << " bytes to " 
            << mName << ", but " << bytesWritten << " bytes were written.";
    (*mErrorFunc)(mUserContext, message);
  }
  return bytesWritten;
}

void UtOCheckpointStream::writeToken(const char *token)
{
  operator <<(token);
}
