//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtParamFile.h"

UtParamFile::UtParamFile(const char* filename)
  : UtIBStream(filename),
    mLineNumber(0)
{
}

UtParamFile::~UtParamFile() {
}

bool UtParamFile::getline(UtIStringStream* line) {
  bool ret = true;
  bool cont = true;
  while (cont && ret) {
    mBuf.clear();
    ret = UtIStream::getline(&mBuf) && !bad();
    if (ret) {
      ++mLineNumber;
      cont = (mBuf.empty() || (mBuf[0] == '#') || (mBuf == "\n"));
    }
  }
  if (ret) {
    line->str(mBuf.c_str());
    *line >> UtIO::slashify;
  }
  return ret;
} // bool UtParamFile::getline

  
