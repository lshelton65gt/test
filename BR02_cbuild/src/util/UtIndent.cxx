// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtString.h"
#include "util/UtIndent.h"

//! constructor
UtIndent::UtIndent(UtString* buf)
  : mBuf(buf), mPosAtStartOfLastLine(1 + buf->find_last_of("\n",buf->size()))
{
  if ( mPosAtStartOfLastLine == UtString::npos )
  {
    mPosAtStartOfLastLine = buf->size();
  }
}

void UtIndent::newline()
{
  *mBuf << "\n";
  mPosAtStartOfLastLine = mBuf->size();
}

bool UtIndent::tab(const SInt32 targetCol)
{
  SInt32 col = ((SInt32) mBuf->size()) - mPosAtStartOfLastLine;
  if (col < targetCol) {
    mBuf->append(targetCol - col, ' ');
    return true;
  }
  return false;
}

void UtIndent::clear() {
  mBuf->clear();
  mPosAtStartOfLastLine = 0;
}

size_t UtIndent::getCurLineLength() const
{
  return mBuf->size() - mPosAtStartOfLastLine;
}
