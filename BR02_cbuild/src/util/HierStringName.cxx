// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file
  Implementation of HierStringName class object.
*/

#include "util/HierStringName.h"
#include "util/StringAtom.h"
#include "util/CarbonAssert.h"
#include "util/UtArray.h"
#include "util/UtString.h"

HierStringName::HierStringName(StringAtom* id)
{
  mAtom = id;
  putParent(NULL);
}

HierStringName::~HierStringName()
{
}

HierStringName::HierStringName(const HierStringName& src) : HierName()
{
  mAtom = src.mAtom;
  mParent = src.mParent;
}

const char* HierStringName::str() const
{
  return mAtom->str();
}

StringAtom* HierStringName::strObject() const
{
  return mAtom;
}

const HierName* HierStringName::getParentName() const
{
  return mParent;
}

void HierStringName::composeHelper(UtString *buf, const bool includeRoot, 
  const bool hierName, const char *separator, bool escapeIfMultiLevel) const
{
  bool needsEsc = false;

  if (!hierName) {
    // we might also be able to go here if !getParent ()
    *buf += str ();    // so we only add the name
    return;
  }
  UtArray<const HierName *> hierStack;
  const HierName *current = this;
  const HierName *parent;

  // the first time through the following loop the node for 'this' is pushed on
  // so that it is available when we unload hierStack
  while (current != NULL) {
    needsEsc |= (current->str ()[0] == '\\');
    needsEsc |= (current->str ()[0] == '$');

    parent = current->getParentName ();
    if ((this != current) && (parent == NULL) && (!includeRoot)) {
      break;
    }
    hierStack.push_back (current);
    current = parent;
  }

  int index = hierStack.size () - 1;   // index is 0 based
  needsEsc |= (index > 0); 

  // check to see if caller asked for escaped multi-level names and there is a reason to escape the name 
  bool writeEscapedName = (needsEsc && (escapeIfMultiLevel));

  if (writeEscapedName) {
    *buf += '\\';
  }

  while (index >= 0) {
    UtString tbuf = hierStack[index]->str ();
    if (writeEscapedName && (tbuf[0] == '\\')) {
      tbuf.erase (0,1);      // if creating an escaped verilog name do not embed a backslash
    } 
    *buf += tbuf;
    if (index > 0) { *buf += separator; } // add separator (if not lowest level)
    --index;
  }
  if (writeEscapedName) {
    *buf += " ";           // escaped names need a space at end
  }

}

void HierStringName::print() const
{
  UtArray<const HierName*> tmpVec(20);
  
  const HierName* name;
  int index = -1;
  const HierName* current = this;
  while((name = current->getParentName()))
  {
    ++index;
    tmpVec[index] = name;
    current = name;
  }
  
  while (index >= 0)
  {
    fprintf(stdout, "%s.", tmpVec[index]->str());
    --index;
  }
  
  fprintf(stdout, "%s\n", str());
  fflush(stdout);
}

void HierStringName::printInfo() const
{
  fprintf(stdout, "HierStringName node: ");
  print();
}

UtString& operator<<(UtString& s, const HierName& name) {
  if (&name == NULL)
    s << "(null)";
  else
    name.compose(&s, true);      // include root in name  test/schedule/derivedclocks4, bidi etal are sensitive to this line
  return s;
}

UInt32 HierName::hash()
  const
{
  UInt32 h = 0;
  for (const HierName* p = this; p != NULL; p = p->getParentName())
    h = 10*h + reinterpret_cast<UIntPtr>(p->strObject());
  return h;
}

//! Print assertion information for this symbol table object
void HierName::printAssertInfo(const char* file, int line, const char* exprStr) const
{
  // Print all the info we need about an object in order for the user to
  // find the problem.  We'd like to give human readable data if possible,
  // which is different depending on the type of object
  fprintf(stderr,"\n\n******CARBON INTERNAL ERROR*******\n\n");
  if (this == NULL) {
    fprintf(stderr,"(null object)\n");
  } else {
    printInfo();
  }

  // print assert location and expression
  fprintf(stderr, "%s:%d ST_ASSERT(%s) failed\n", file, line, exprStr);
  fflush(stderr);
  // print stack trace ???
  abort();
}
