// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/ZstreamZip.h"
#include "util/OSWrapper.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"

// This is just the header for the file entries. So, this does not
// need a numeric id, just a version
static const char cEntryHeader[] = "CarbonZip_H";
static const UInt32 cEntryHeaderSize = sizeof(cEntryHeader);
static const UInt32 cEntryHeaderVersion = 2;
static const UInt32 cEntryHeaderReserved = 0;

// Numeric id. Hopefully, a set of 12 unique bytes to id a zip
// archive. The fail safe is the string id.
static const UInt32 cFileId[] = {0xaf0180cc, 0xe4e261bb, 0xc1ff3efa };
static const UInt32 cFileIdNum = sizeof(cFileId)/sizeof(UInt32);

// The string id for the carbon zip archive
static const char cFileHeader[] = "CarbonZipArchive";
static const UInt32 cFileHeaderSize = sizeof(cFileHeader);

// Simply extra bytes in case we want to make the above string longer,
// or want to add more fields.
static const UInt32 cFileHeaderReserved[] = { 0, 0, 0, 0 };
static const UInt32 cFileHeaderReservedSize = sizeof(cFileHeaderReserved)/sizeof(UInt32);


// Numeric file version
static const UInt32 cFileVersion = 1;
// Reserved bytes (in case we want to do a minor/major versioning)
static const UInt32 cFileVersionReserved = 0;


//! An entry that is a normal Zistream
class ZistreamZip::ZistreamEntryN : 
  public ZistreamEntry, public Zistream
{
public: 
  CARBONMEM_OVERRIDES
  //! constructor
  /*!
    \param beginOffset The current position of the file descriptor
    \param hardStop Byte in file at which to consider this stream the eof.
    \param name Name of the file or buffer that this entry represents
    \param srcStrm The archive file stream
  */
  ZistreamEntryN(SInt64 beginOffset, SInt64 hardStop, const char* name, Zistream* srcStrm);
  
  //! destructor
  virtual ~ZistreamEntryN();
  
  //! Dummy close. This does not close the file
  /*!
    This does not close the file because we do not own the file
    descriptor, ZistreamZip does.
    \returns true, but invalidates this stream
  */
  virtual bool close();

  //! ZistreamEntry::castZistream()
  virtual Zistream* castZistream();

  //! ZistreamEntry::castZistreamDB()
  virtual ZistreamDB* castZistreamDB();
  
private:
  // forbid
  ZistreamEntryN();
  ZistreamEntryN(const ZistreamEntryN&);
  ZistreamEntryN& operator=(const ZistreamEntryN&);
};

//! An entry that is a ZistreamDB
class ZistreamZip::ZistreamDBEntryN : 
  public ZistreamEntry, public ZistreamDB
{
public: 
  CARBONMEM_OVERRIDES
  //! constructor
  /*!
    \param beginOffset The current position of the file descriptor
    \param hardStop Byte in file at which to consider this stream the eof.
    \param name Name of the file or buffer that this entry represents
    \param srcStrm The archive file stream
  */
  ZistreamDBEntryN(SInt64 beginOffset, SInt64 hardStop, const char* name, Zistream* srcStrm);
  
  //! destructor
  virtual ~ZistreamDBEntryN();
  
  //! Dummy close. This does not close the file
  /*!
    This does not close the file because we do not own the file
    descriptor, ZistreamZip does.
    \returns true, but invalidates this stream
  */
  virtual bool close();

  //! ZistreamEntry::castZistream()
  virtual Zistream* castZistream();

  //! ZistreamEntry::castZistreamDB()
  virtual ZistreamDB* castZistreamDB();
  
private:
  // forbid
  ZistreamDBEntryN();
  ZistreamDBEntryN(const ZistreamDBEntryN&);
  ZistreamDBEntryN& operator=(const ZistreamDBEntryN&);
};

class ZostreamZip::DBEntry : public ZostreamDB
{
public:
  CARBONMEM_OVERRIDES

  DBEntry(const char* name, Zostream* srcStrm);
  
  //! destructor
  virtual ~DBEntry();
  
  //! Dummy close. This does not close the file
  /*!
    This does not close the file because we do not own the file
    descriptor, ZistreamZip does.
    \returns true, but invalidates this stream
  */
  virtual bool close();

private:
  // forbid
  DBEntry();
  DBEntry(const DBEntry&);
  DBEntry& operator=(const DBEntry&);
};

ZstreamZip::ZstreamZip() : mStream(NULL), 
                           mFirstEntry(NULL),
                           mLastEntry(NULL)
{}

ZstreamZip::~ZstreamZip()
{
  for (StringEntryMap::UnsortedLoop i = mEntryIds.loopUnsorted(); !i.atEnd(); ++i)
    // Linux64 needs the cast.
    delete static_cast<ZipEntry*>((*i).second);
  mEntryIds.clear();
}

ZstreamZip::operator void*() const
{
  return fail()  ? (void*) 0 : (void*)(-1);
}

int ZstreamZip::operator!() const
{
  return fail() ? 1 : 0;
}

bool ZstreamZip::fail() const
{
  return mStream->fail() || ! mErrMsg.empty();
}

void ZstreamZip::checkActiveEntryError()
{}

const char* ZstreamZip::getFileError()
{
  const char* msg = NULL;
  checkActiveEntryError();
  if (! mErrMsg.empty())
    msg = mErrMsg.c_str();
  else
    msg = mStream->getError();
  return msg;
}

void ZstreamZip::getFilename(UtString* filename) const
{
  INFO_ASSERT(mStream, filename->c_str());
  mStream->getFilename(filename);
}

bool ZstreamZip::checkUnique(const char* id)
{
  UtString idStr(id);
  return mEntryIds.find(idStr) == mEntryIds.end();
}

void ZstreamZip::addUnique(const char* id, ZipEntry* entry)
{
  UtString idStr(id);
  INFO_ASSERT(mEntryIds.find(idStr) == mEntryIds.end(), id);
  mEntryIds[idStr] = entry;
  
  if (mLastEntry)
    mLastEntry->putNext(entry);

  mLastEntry = entry;
  
  if (! mFirstEntry)
    mFirstEntry = entry;
}


ZostreamZip::ZostreamZip(const char* outputFile, void* key) : 
  mBeginPos(0), mCurDBEntry(NULL)
{
  mOutFile = new ZostreamDB(outputFile, key);
}

ZostreamZip::~ZostreamZip()
{
  finalize();
  delete mOutFile;
}

bool ZostreamZip::open()
{
  bool isGood = true;
  if (! mOutFile->fail())
  {
    // not needed, but safe in case Zstream changes its MO
    mOutFile->flush();

    // write the numeric id for a zip archive
    UInt32 i;
    for (i = 0; i < cFileIdNum; ++i)
      *mOutFile << cFileId[i];
    
    // write out the zstreamzip file string header
    mOutFile->write(cFileHeader, cFileHeaderSize);

    // write reserved bytes
    for (i = 0; i < cFileHeaderReservedSize; ++i)
      *mOutFile << cFileHeaderReserved[i];
    
    // write the file version
    *mOutFile << cFileVersion;
    
    // write the reserved version
    *mOutFile << cFileVersionReserved;
    
    mOutFile->flush();

    mBeginPos = mOutFile->tellfd();
    // Can't compress the header position
    SInt64 num = 0;
    // Don't need to byte flip a 0
    mOutFile->safeWrite(reinterpret_cast<char*>(&num), sizeof(SInt64));
  }
  
  if (mOutFile->fail())
  {
    isGood = false;
    mErrMsg << mOutFile->getError();
  }
  return isGood;
}


void ZostreamZip::addEntryDescriptor(const char* name)
{
  *mOutFile << name;
  mOutFile->flush();
  mOutFile->markRecordEnd();
}

void ZostreamZip::createZipEntry(const char* id, FileType fileType)
{
  ZipEntry* entry = new ZipEntry(mOutFile->tellfd(), id, fileType);  
  addEntryDescriptor(id);
  addUnique(id, entry);
}

void ZostreamZip::beginFileEntry()
{
  // Everything written is compressed.
  mOutFile->ZOSTREAM_WRITE_MAGIC_HEADER();  
}

void ZostreamZip::endFileEntry()
{
  mOutFile->flush();
  mOutFile->markRecordEnd();
}


ZostreamDB* ZostreamZip::addDatabaseEntry(const char* id)
{
  INFO_ASSERT(id, "Invalid entry id.");
  INFO_ASSERT(! mCurDBEntry, id);

  if (fail())
    return NULL;
  
  mErrMsg.clear();

  createZipEntry(id, eFileDB);
  
  mCurDBEntry = new DBEntry(id, mOutFile);
  return mCurDBEntry;
}

bool ZostreamZip::endDatabaseEntry()
{
  INFO_ASSERT(mCurDBEntry, "Overlapped entry endpoints.");
  mCurDBEntry->flush();
  endFileEntry();
  delete mCurDBEntry;
  mCurDBEntry = NULL;
  return ! fail();
}

bool ZostreamZip::addFile(const char* file, FileType fileType)
{
  INFO_ASSERT(! mCurDBEntry, file);

  if (fail())
    return false;
  
  INFO_ASSERT(mOutFile->is_open(), file);

  bool isGood = true;

  mErrMsg.clear();

  bool rawWrite = false;

  UtIBStream f(file);
  if (! f.fail())
  {
    createZipEntry(file, fileType);
    // DO NOT CALL beginFileEntry() if the file is already a
    // zstream. The magic header which is already written. 
    if (fileType == eFileTxt)
      beginFileEntry();
    else 
      rawWrite = true;

    // Now write out the file;      
#define BUFSIZE 65536
    char buf[BUFSIZE];
    UInt32 numRead;
    
    while(! f.eof() && ! f.fail())
    {
      numRead = f.read(buf, BUFSIZE);
      if (! rawWrite)
        mOutFile->write(buf, numRead);
      else
        mOutFile->safeWrite(buf, numRead);
    }
    if (f.fail())
    {
      mErrMsg << f.getErrmsg();
      isGood = false;
    }
    f.close();
    if (mOutFile->fail())
    {
      mErrMsg << mOutFile->getError();
      isGood = false;
    }
    endFileEntry();
  }
  else
  {
    isGood = false;
    mErrMsg << f.getErrmsg();
  }
  
#undef BUFSIZE

  return isGood;
}

bool ZostreamZip::addBuffer(const char* id, const UtString& buffer)
{
  INFO_ASSERT(! mCurDBEntry, id);

  if (fail())
    return false;

  bool isGood = true;
  
  createZipEntry(id, eFileTxt);
  // write the magic header
  beginFileEntry();

  mOutFile->write(buffer.c_str(), buffer.size());

  if (mOutFile->fail())
  {
    mErrMsg.clear();
    mErrMsg << mOutFile->getError();
    isGood = false;
  }
  endFileEntry();

  return isGood;
}

bool ZostreamZip::addEntry(ZistreamEntry* readEntry, FileType filetype)
{
  UtString idName;
  Zistream* src = readEntry->castZistream();
  src->getFilename(&idName);
  
  INFO_ASSERT(! mCurDBEntry, idName.c_str());
  createZipEntry(idName.c_str(), filetype);
  beginFileEntry();

#define ZIP_BUFSIZE 65536
  char buf[ZIP_BUFSIZE];
  UInt32 numRead;
  while(! src->eof() && ! src->fail())
  {
    numRead = src->read(buf, ZIP_BUFSIZE);
    mOutFile->write(buf, numRead);
  }
#undef ZIP_BUFSIZE

  bool isGood = true;
  if (mOutFile->fail())
  {
    isGood = false;
    mErrMsg << mOutFile->getError();
  }
  else if (src->fail()) {
    isGood = false;
    mErrMsg << src->getError();
  }

  endFileEntry();
  return isGood;
}

bool ZostreamZip::finalize()
{
  bool isGood = ! mOutFile->fail();
  if (isGood && mOutFile->is_open())
  {
    SInt64 headPos = mOutFile->tellfd();
    mOutFile->write(cEntryHeader, cEntryHeaderSize);
    *mOutFile << cEntryHeaderVersion;
    *mOutFile << cEntryHeaderReserved;
    mOutFile->flush();

    const ZipEntry* entry = mFirstEntry;
    while (entry)
    {
      *mOutFile << entry->getFilePos();
      
      const UtString& name = entry->getName();
      *mOutFile << name;
      *mOutFile << (SInt32) entry->getType();
      entry = entry->getNext();
    }

    mOutFile->flush();

    mOutFile->seek(mBeginPos);
    // get the fd, byte flip the header pos, and write

    mOutFile->byteSwapOnLittleEndian(&headPos);
    mOutFile->safeWrite(reinterpret_cast<char*>(&headPos), sizeof(SInt64));

    // should seek to end of file before closing
    if (! mOutFile->fail())
      mOutFile->seekEnd();
    
    // at this point we may have had an error
    if (mOutFile->fail() || ! mOutFile->close())
    {
      mErrMsg << mOutFile->getError();
      isGood = false;
    }
  }
  else if (! isGood)
    mErrMsg << mOutFile->getError();
  
  return isGood;
}

ZistreamZip::ZistreamZip(const char* inFile, void* key) : 
  mActiveEntry(NULL),
  mHeaderPos(0),
  mInFile(NULL)
{
  mBufferedFileSize = 0;
  mInFile = new ZistreamDB(inFile, key);
}

ZistreamZip::ZistreamZip(size_t size, const char* buf, void* key) : 
  mActiveEntry(NULL),
  mHeaderPos(0),
  mInFile(NULL)
{
  mBufferedFileSize = size;
  mInFile = new ZistreamDB(size, buf, key);
}

ZistreamZip::~ZistreamZip()
{
  mZistreams.clearPointers();
  delete mInFile;
}

void ZistreamZip::checkActiveEntryError()
{
  // only write an error message if we don't already have one.
  if (mActiveEntry && mActiveEntry->isActive() && mErrMsg.empty())
  {
    Zistream* activeFd = mActiveEntry->castZistream();
    if (activeFd->fail())
    {
      getFilename(&mErrMsg);
      mErrMsg << ": " << activeFd->getError();
    }
  }
}

bool ZistreamZip::open()
{
  bool isGood = true;
  if (! mInFile->fail())
  {
    // First check if this is a proper ZistreamDB
    if (! mInFile->isCompressed())
    {
      UtString fileName;
      mInFile->getFilename(&fileName);
      mErrMsg << fileName << " is not a carbon database.";
      return false;
    }
    
    // Ok we have a ZistreamDB

    // Read the numeric id
    UInt32 i;
    UInt32 numericId = 0;
    bool isMatched = true;
    for (i = 0; (i < cFileIdNum) && isMatched; ++i)
    {
      *mInFile >> numericId;
      isMatched = numericId == cFileId[i];
    }
    
    if (mInFile->fail())
    {
      isGood = false;
      mErrMsg = mInFile->getError();
    }
    else if (! isMatched)
    {
      // We know we have a ZistreamDB. Let's assume that we are trying
      // to read in a deprecated format of a particular file. For
      // example, libdesign.io.db used to be a ZstreamDB, now it is a
      // ZstreamZip.
      UtString fileName;
      mInFile->getFilename(&fileName);
      mErrMsg << fileName << " is not a carbon database. This format is no longer supported.";
      isGood = false;
    }

    if (isGood)
    {
      // read in the zstreamzip file header string and check it
      char headerBuf[cFileHeaderSize + 1];
      mInFile->read(headerBuf, cFileHeaderSize);
      if (! mInFile->fail())
      {
        headerBuf[cFileHeaderSize] = '\0';
        if (strcmp(headerBuf, cFileHeader) != 0)
        {
          UtString fileName;
          mInFile->getFilename(&fileName);
          mErrMsg << fileName << ": Corrupt Carbon database or file is not a database.";
          isGood = false;
        }
      }
      else
      {
        mErrMsg << mInFile->getError();
        isGood = false;
      }
    }

    if (isGood)
    {
      // now read the reserved words and ignore them
      for (i = 0; i < cFileHeaderReservedSize; ++i)
        *mInFile >> numericId;
      
      if (mInFile->fail())
      {
        isGood = false;
        mErrMsg << mInFile->getError();
      }
    }

    // Now, the version and the reserved
    if (isGood)
    {
      UInt32 fileVersion = 0;
      *mInFile >> fileVersion;
      if (! mInFile->fail() && (fileVersion != cFileVersion))
      {
        isGood = false;
        mErrMsg << "Incompatible file carbon database version: " 
                << fileVersion << ", expected " << cFileVersion;
      }
      // ignore the next 4 bytes
      // Note, this won't actually happen if there was a file problem
      *mInFile >> fileVersion;
      if (mInFile->fail())
      {
        isGood = false;
        mErrMsg << mInFile->getError();
      }
    }

    if (isGood)
    {
      int headPosRead;
      mInFile->safeRead(reinterpret_cast<char*>(&mHeaderPos), sizeof(SInt64), 
                       &headPosRead);
      INFO_ASSERT(headPosRead == sizeof(SInt64), "Database consistency check failed.");
      mInFile->byteSwapOnLittleEndian(&mHeaderPos);
      
      if (! mInFile->fail())
      {
        SInt64 fileSize;
        UtString fileName;
        mInFile->getFilename(&fileName);
        if (mBufferedFileSize != 0) {
          fileSize = mBufferedFileSize;
        }
        else if (!OSGetFileSize(fileName.c_str(), &fileSize, &mErrMsg)) {
          mErrMsg << fileName << ": Cannot get file size";
          isGood = false;
        }
        if (isGood && (fileSize < mHeaderPos)) {
          mErrMsg << fileName << ": Bad offset (" << mHeaderPos << ")";
          isGood = false;
        }
      }
    }

  }
  
  if (isGood && mInFile->fail())
  {
    isGood = false;
    mErrMsg << mInFile->getError();
  }
  
  return isGood;
}

bool ZistreamZip::checkEntries()
{
  mErrMsg.clear();

  bool isGood = true;
  if (mEntryIds.empty())
  {
    // probably didn't fill it up
    mInFile->seek(mHeaderPos);
    char buf[cEntryHeaderSize+1];
    mInFile->read(buf, cEntryHeaderSize);
    if (! mInFile->fail())
    {
      buf[cEntryHeaderSize] = '\0';
      if (strcmp(buf, cEntryHeader) != 0)
      {
        isGood = false;
        mInFile->getFilename(&mErrMsg);
        mErrMsg << ": " << "Corrupt header";
      }
    }
    
    if (isGood)
    {
      // the next 8 bytes are versioning info.
      UInt32 entryVersion = 0;
      *mInFile >> entryVersion;
      if (! mInFile->fail() && (entryVersion != cEntryHeaderVersion))
      {
        mErrMsg << "File entry header version (" << entryVersion << ") != " 
                << cEntryHeaderVersion;
        isGood = false;
      }
      // read the next 4 bytes (won't actually happen if the stream is
      // in a bad state)
      *mInFile >> entryVersion;

      if (mInFile->fail())
      {
        isGood  = false;
        mErrMsg = mInFile->getError();
      }
    }
    
    while (isGood && ! mInFile->eof() && ! mInFile->fail())
    {
      SInt64 filePos = 0;
      UtString name;
      FileType fileType;
      
      // eof may happen on next write
      *mInFile >> filePos;
      if (! mInFile->eof())
      {
        isGood = ! mInFile->fail() && readEntry(&name, &fileType);
        if (isGood)
        {
          const char* idName = name.c_str();
          ZipEntry* entry = new ZipEntry(filePos, idName, fileType);
          addUnique(idName, entry);
        }
      }
    } // while
    
    if (isGood && mInFile->fail())
    {
      isGood = false;
      mErrMsg << mInFile->getError();
    }
  } // if mEntryIds.empty()
  return isGood;
}

bool ZistreamZip::getAllEntryIds(UtStringArray* ids)
{
  bool isGood = checkEntries();
  if (isGood)
  {
    for (const ZipEntry* cur = mFirstEntry; cur != NULL; cur = cur->getNext())
      ids->push_back(cur->getName());
  }
  return isGood;
}

bool ZistreamZip::rewrite(EntryRewriter* userRewriter)
{
  // set up the various filename strings
  UtString origFilename;
  mInFile->getFilename(&origFilename);
  // Filename for new file
  UtString tmpFilename(origFilename);
  tmpFilename << "." << OSGetPid();
  // Filename for backup
  UtString fileNameBk(origFilename);
  fileNameBk << ".bk";

  // Load the entries, if needed.
  bool isGood = checkEntries();
  if (isGood)
  {
    userRewriter->msgBackingupFile(origFilename.c_str(), fileNameBk.c_str());

    // backup the current file
    isGood = OSCopyFile(origFilename.c_str(), fileNameBk.c_str(), &mErrMsg);
    
    userRewriter->msgWritingTmpFile(tmpFilename.c_str());
    
    // open the new file
    ZOSTREAMZIP(rewriteZip, tmpFilename.c_str());
    if (! rewriteZip)
    {
      mErrMsg << rewriteZip.getFileError();
      isGood = false;
    }
    
    for (const ZipEntry* cur = mFirstEntry; 
         isGood && (cur != NULL); cur = cur->getNext())
    {
      ZistreamEntry* readEntry = NULL;
      ZostreamDB* entryAdd = NULL;
      
      switch(userRewriter->whichAction(cur->getName()))
      {
      case eEntryUnmodified:
        // Simply create the read entry and write it to the new
        // file.
        readEntry = getEntry(cur->getName().c_str());
        if (readEntry)
        {
          isGood = rewriteZip.addEntry(readEntry, cur->getType());
          if (! isGood)
            mErrMsg << rewriteZip.getFileError();
          finishEntry(&readEntry);
        }
        else
          isGood = false;
        break;
      case eEntryModified:
        // assert that the entry is a db entry
        INFO_ASSERT(cur->getType() == eFileDB, "Only db entry types are supported for modification.");
        readEntry = getEntry(cur->getName().c_str());
        entryAdd = rewriteZip.addDatabaseEntry(cur->getName().c_str());
        if (entryAdd && readEntry)
        {
          isGood = userRewriter->modifyDBEntry(readEntry, cur->getName(), entryAdd);
          finishEntry(&readEntry);
          rewriteZip.endDatabaseEntry();
        }
        else
        {
          isGood = false;
        
          if (entryAdd == NULL)
            mErrMsg << rewriteZip.getFileError();
        }
        break;
      case eEntryDelete:
        // skip this entry
        break;
      }
    }

    if (isGood)
    {
      if (! rewriteZip.finalize())
      {
        isGood = false;
        mErrMsg << rewriteZip.getFileError();
      }
      else
      {
        // CREATE A OSRename FUNCTION. For now, just copy and delete
        userRewriter->msgMovingTmpToOrig(tmpFilename.c_str(), origFilename.c_str());

        UtString errMsg;
        isGood = OSCopyFile(tmpFilename.c_str(), origFilename.c_str(), &errMsg);
        isGood = isGood && (OSUnlink(tmpFilename.c_str(), &errMsg) == 0);
        if (! isGood)
          mErrMsg << errMsg;
      }
    }
  }
  
  if (! mInFile->fail())
  {
    // Put into the error state, so this object can no longer be used
    mInFile->setError("Operations after rewrite are not permitted.");
  }
  return isGood;
}

ZistreamEntry* ZistreamZip::getEntry(const char* entryName)
{
  ZistreamEntry* ret = NULL;
  UtString entryStr(entryName);

  NameZEntryMap::iterator zp = mEntryZistreams.find(entryStr);
  if (zp != mEntryZistreams.end())
  {
    ret = zp->second;
    // We already have it. If it isn't the active entry, finish active
    // entry and set it to this one.
    if (ret != mActiveEntry)
      switchEntry(ret);
    return ret;
  }

  // ok this entry has not been requested before now.
  bool isGood = checkEntries();
  
  if (isGood)
  {
    StringEntryMap::const_iterator p = mEntryIds.find(entryStr);
    if (p != mEntryIds.end())
    {
      const ZipEntry* entry = p->second;
      deactivateCurEntry();
      SInt64 entryFilePos = entry->getFilePos();
      mInFile->seek(entryFilePos);
      // At the file description. Check the filename
      UtString fileEntryName;
      *mInFile >> fileEntryName;
      isGood = ! mInFile->fail() && ! mInFile->eof();
      if (isGood)
      {
        // calculate the next stop (compressed bytes)
        SInt64 nextOffset = mHeaderPos;
        const ZipEntry* next = entry->getNext();
        if (next)
          nextOffset = next->getFilePos();
        INFO_ASSERT(nextOffset >= entryFilePos, entryName);
        
        if (entry->getType() != eFileDB)
          ret = new ZistreamEntryN(entryFilePos, nextOffset, 
                                   entryName, mInFile);
        else
          ret = new ZistreamDBEntryN(entryFilePos, nextOffset, 
                                     entryName, mInFile);

        mZistreams.insert(ret);
        mEntryZistreams[entryStr] = ret;

        // post-init, file pos could change
        ret->markPosition();
        ret->putIsActive(true);
        mActiveEntry = ret;
      }
      else
      {
        mErrMsg << "Corrupt Entry for " << entryStr << ": ";
        if (mInFile->fail())
          mErrMsg << mInFile->getError();
        else
          mErrMsg << "Unexpected end of input";
      }
    }
  }
  
  return ret;
}

void ZistreamZip::finishEntry(ZistreamEntry** entry)
{
  if (entry && *entry)
  {
    ZistreamEntry* entryRef = *entry;
    if (entryRef == mActiveEntry)
      mActiveEntry = NULL;
    ZistreamSet::iterator p = mZistreams.find(entryRef);
    INFO_ASSERT(p != mZistreams.end(), "Database consistency check failed.");
    mZistreams.erase(p);

    // Remove the entry from name->Zistream map
    UtString name;
    Zistream* strm = entryRef->castZistream();
    strm->getFilename(&name);
    mEntryZistreams.erase(name);
    
    // delete the Zistream
    delete entryRef;
    *entry = NULL;
  }
}

void ZistreamZip::deactivateCurEntry()
{
  if (mActiveEntry)
  {
    mActiveEntry->markPosition();
    mActiveEntry->putIsActive(false);
  }
}

void ZistreamZip::switchEntry(ZistreamEntry* entry)
{
  deactivateCurEntry();
  mInFile->seek(entry->getCurrentPosition());
  entry->putIsActive(true);
  mActiveEntry = entry;
}


bool ZistreamZip::readEntry(UtString* name, FileType* fileType)
{
  bool isGood = true;
  *fileType = eFileTxt;

  *mInFile >> *name;
  SInt32 tmpType;
  *mInFile >> tmpType;
  if (mInFile->fail())
  {
    isGood = false;
    mErrMsg << "Corrupt entry - " << mInFile->getError();
  }
  else
    *fileType = (FileType) tmpType;
  
  return isGood;
}


ZistreamEntry::ZistreamEntry(SInt64 beginOffset)
  : mCurPosition(beginOffset), mIsActive(false)
{}

ZistreamEntry::~ZistreamEntry()
{}

bool ZistreamEntry::isActive() const {
  return mIsActive;
}

void ZistreamEntry::putIsActive(bool isActive)
{
  mIsActive = isActive;
}

void ZistreamEntry::markPosition()
{
  Zistream* me = castZistream();
  mCurPosition = me->tellfd();
}

SInt64 ZistreamEntry::getCurrentPosition() const
{
  return mCurPosition;
}

ZistreamZip::ZistreamEntryN::ZistreamEntryN(SInt64 beginOffset, 
                               SInt64 hardStop, 
                               const char* name, Zistream* srcStrm) :
  ZistreamEntry(beginOffset), Zistream(name, hardStop, srcStrm)
{
}

ZistreamZip::ZistreamEntryN::~ZistreamEntryN()
{
  /* 
     Since this destructor gets called before Zistream::close, the
     ZistreamEntryN::close() method is no
     longer valid in this context when Zistream::close is
     called. Therefore, in order to stop the file descriptor from
     being closed we need to trick the base class into thinking that
     it already is.
  */
  // mFile = -1;
  //
  // JDM: we could set mFile=NULL, but I this is already taken care of
  // because Zstream has an mOwnsFile member variable which is false
  // when constructed with the Zistream* srcStream arg.
}

Zistream* ZistreamZip::ZistreamEntryN::castZistream()
{
  return this;
}

ZistreamDB* ZistreamZip::ZistreamEntryN::castZistreamDB()
{
  return NULL;
}

bool ZistreamZip::ZistreamEntryN::close()
{
  //mFile = -1;  see comment in destructor
  return true;
}

ZistreamZip::ZistreamDBEntryN::ZistreamDBEntryN(SInt64 beginOffset, 
                                                SInt64 hardStop, 
                                                const char* name, Zistream* srcStrm) :
  ZistreamEntry(beginOffset), ZistreamDB(name, hardStop, srcStrm)
{
}

ZistreamZip::ZistreamDBEntryN::~ZistreamDBEntryN()
{
  /* 
     Since this destructor gets called before Zistream::close, the
     ZistreamEntryN::close() method is no
     longer valid in this context when ZistreamDB::close is
     called. Therefore, in order to stop the file descriptor from
     being closed we need to trick the base class into thinking that
     it already is.
  */
  //mFile = -1;  see mOwnsFile comment above
}

Zistream* ZistreamZip::ZistreamDBEntryN::castZistream()
{
  return this;
}

ZistreamDB* ZistreamZip::ZistreamDBEntryN::castZistreamDB()
{
  return this;
}

bool ZistreamZip::ZistreamDBEntryN::close()
{
  // mFile = -1;  see mOwnsFile comment above
  return true;
}

ZostreamZip::DBEntry::DBEntry(const char* name, Zostream* srcStream) :
  ZostreamDB(name, srcStream)
{}

ZostreamZip::DBEntry::~DBEntry()
{
  mFile = -1;
}

bool ZostreamZip::DBEntry::close()
{
  mFile = -1;
  return true;
}

ZistreamZip::EntryRewriter::~EntryRewriter()
{}
