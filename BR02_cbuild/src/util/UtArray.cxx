// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/UtArray.h"
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "util/MemManager.h"
#include "util/OSWrapper.h"

UtPtrArray::UtPtrArray() {
  alloc(0, 0);
}

//! construct an array with specified size and capacity
UtPtrArray::UtPtrArray(SIntPtr sz, SIntPtr capacity, bool clearData) {
  INFO_ASSERT(sz >= 0, "Negative array size.");
  if (capacity < sz)
    capacity = sz;
  alloc(sz, capacity);
  if (clearData)
    clearRange(0, sz);
}

//! construct an array with specified size and capacity, and data
UtPtrArray::UtPtrArray(ConstPtrArray data, SIntPtr sz, SIntPtr capacity) {
  init(data, sz, capacity);
}

//! copy ctor
UtPtrArray::UtPtrArray(const UtPtrArray& src) {
  init(src.mData, src.size(), src.capacity());
}

//! assign op
UtPtrArray& UtPtrArray::operator=(const UtPtrArray& src) {
  if (&src != this)
  {
    SIntPtr sz = src.size();
    resize(sz);
    memcpy(mData, src.mData, sz*sizeof(void*));
  }
  return *this;
}

bool UtPtrArray::operator==(const UtPtrArray& other) const
{
  UInt32 mySize = size();
  bool isEq = mySize == other.size();
  if (isEq)
    isEq = memcmp(mData, other.mData, mySize*sizeof(void*)) == 0;

  return isEq;
}

//! dtor
UtPtrArray::~UtPtrArray() {
  clear();
}

//! append one element, resizing if necessary
void UtPtrArray::push_back(void* data) {
  SInt32 idx = size();
  resize(idx + 1);
  mData[idx] = data;
}

# define cUtArrayBigArrayFlag   0x80000000
# define cUtArrayMaxSmallCap    0x00007FFF

// Maximum desired size such that when bumped to the next power of 2
// does not exceed 15 bits
# define cUtArrayMaxSmallCap2   0x00004000

void UtPtrArray::resize(SIntPtr sz, bool clearExtra) {
  SInt32 oldSize = size();
  if (sz >= capacity()) {
    UInt32 nextCap = 0;
    if (sz > cUtArrayMaxSmallCap2)
    {
      nextCap = nextPowerOfTwo(sz + 1);  // leave room for size in array
      INFO_ASSERT(nextCap > cUtArrayMaxSmallCap, "Consistency check failed.");
    }
    else
    {
      nextCap = nextPowerOfTwo(sz);
      INFO_ASSERT(nextCap <= cUtArrayMaxSmallCap, "Consistency check failed.");
    }
    UtPtrArray tmp(sz, nextPowerOfTwo(sz), false);
    memcpy(tmp.mData, mData, oldSize*sizeof(void*));
    swap(tmp);
  } else {
    if (isBig()) {
      mControl.mSize = sz | cUtArrayBigArrayFlag;
    } else {
      mControl.mSmall.mSize = (UInt16) sz;
    }
  }
  if (clearExtra && (sz > oldSize))
    clearRange(oldSize, sz - oldSize);
}

void UtPtrArray::reserve(SIntPtr cap) {
  SIntPtr sz = size();
  INFO_ASSERT(cap >= sz, "Invalid reserve size (cap).");
  if (cap != capacity()) {
    UtPtrArray tmp(sz, cap, false);
    tmp = *this;
    swap(tmp);
  }
}

//! Clear contents, freeing memory
void UtPtrArray::clear()
{
  if (mData != NULL) {
    SIntPtr cap = capacity();
    if (isBig()) {
      void** allocedData = mData - 1;
      CARBON_FREE_VEC(allocedData, void*, cap + 1);
    }
    else {
      CARBON_FREE_VEC(mData, void*, cap);
    }
    mData = NULL;
    mControl.mSize = 0;
  }
}

//! swap contents of two vectors
void UtPtrArray::swap(UtPtrArray& other) {
  std::swap (other.mData, mData);
  std::swap (other.mControl.mSize, mControl.mSize);
}

//! Find the smallest power-of-two that's >= x.
/*! returns 0 if x > 2^31 */
UInt32 UtPtrArray::nextPowerOfTwo(UInt32 x) {
  // binary-search to find the next power of 2 >= x
  UInt32 pwr2 = 1;

  // loop should be unrolled
  for (; (pwr2 != 0) && (pwr2 < x); pwr2 += pwr2)
    ;
  return pwr2;
}        

UInt32 UtPtrArray::size() const {
  if (isBig())
    return mControl.mSize & ~cUtArrayBigArrayFlag;
  return mControl.mSmall.mSize;
}

void UtPtrArray::printPtrs()
{
  for (UInt32 i = 0; i < size(); ++i)
  {
    fprintf(stdout, "%d: " FormatPtr "\n", (int) i, (void*) (*this)[i]);
  }
}

void UtPtrArray::printStrings()
{
  for (UInt32 i = 0; i < size(); ++i)
  {
    fprintf(stdout, "%d: %s\n", (int) i, (const char*) (*this)[i]);
  }
}

// private methods:

//! initialize all member vars, copy data
void UtPtrArray::init(ConstPtrArray data, SIntPtr sz, SIntPtr cap) {
  alloc(sz, cap);
  memcpy(mData, data, sz * sizeof(void*));
}

//! is this array in "big" mode (capacity >= 32k?)
bool UtPtrArray::isBig() const {
  return (mControl.mSize & cUtArrayBigArrayFlag) != 0;
}

SIntPtr UtPtrArray::capacity() const {
  if (isBig())
    return (SIntPtr) mData[-1];
  return mControl.mSmall.mCapacity;
}    

//! Set up data struct to have size & capacity as specified, unitialized
void UtPtrArray::alloc(SIntPtr sz, SIntPtr cap) {
  if (cap <= cUtArrayMaxSmallCap) {
    // For small vectors, pack the size & capacity into one control word
    mControl.mSmall.mSize = (UInt16) sz;
    mControl.mSmall.mCapacity = (UInt16) cap;
    mData = CARBON_ALLOC_VEC(void*, cap);
  } else {
    // For large vectors, use the first word of the allocated data, which
    // is actually the -1 index from mData
    mControl.mSize = sz | cUtArrayBigArrayFlag;
    void** data = CARBON_ALLOC_VEC(void*, cap + 1);
    mData = data + 1;
    *data = (void*) cap;
  }
}

//! Clear the sz words starting at word start
void UtPtrArray::clearRange(SIntPtr start, SIntPtr sz) {
  if (sz != 0)
    memset(mData + start, '\0', sz * sizeof(void*));
}

void UtPtrArray::erase(iterator& iter)
{
  SIntPtr sz = size();
  void*& val = *iter;
  void** ptr = &val;
  SInt32 remainingCount = mData + sz - ptr - 1;
  INFO_ASSERT(remainingCount >= 0, "Consistency check failed.");
  memmove(ptr, ptr + 1, remainingCount * sizeof(void*));
  resize(sz - 1);
}

void UtPtrArray::erase(iterator& beg, iterator& end)
{
  SInt32 sz = size();
  void*& bval = *beg;
  void** bptr = &bval;
  void*& eval = *end;
  void** eptr = &eval;
  SInt32 num_erased = eptr - bptr;
  SInt32 remainingCount = mData + sz - eptr;
  INFO_ASSERT(remainingCount >= 0, "Consistency check failed.");
  if (remainingCount > 0)
    memmove(bptr, eptr, remainingCount * sizeof(void*));
  resize(sz - num_erased);
}

size_t UtPtrArray::remove(const void* elt) {
  void** a = mData;
  size_t removed = 0;
  for (size_t i = 0, j = 0, n = size(); j < n; ++j) {
    if (a[j] != elt) {
      if (i != j) {
        a[i] = a[j];
      }
      ++i;
    }
    else
      ++removed;
  }
  if (removed != 0) {
    // removed is a size_t, i.e., unsigned
    INFO_ASSERT(removed <= size(), "Consistency check failed.");
    resize(size() - removed, true);
  }
  return removed;
}

size_t UtPtrArray::removeMatching(const Predicate& pred) {
  void** a = mData;
  size_t removed = 0;
  for (size_t i = 0, j = 0, n = size(); j < n; ++j) {
    if (!pred(a[j])) {
      if (i != j) {
        a[i] = a[j];
      }
      ++i;
    }
    else
      ++removed;
  }
  if (removed != 0) {
    // removed is a size_t, i.e., unsigned
    INFO_ASSERT(removed <= size(), "Consistency check failed.");
    resize(size() - removed, true);
  }
  return removed;
}

UtPtrArray::Predicate::~Predicate() {
}
