// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Contains an algorithm for computing strongly-connected components of a graph.
*/

#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphSCC.h"
#include "util/UtStack.h"
#include "util/UtList.h"
#include "util/UtMap.h"
#include "util/UtSet.h"
#include <utility> // for stl_pair

GraphSCC::GraphSCC()
  : mCount(0), mStack(), mOriginalGraph(NULL), mComponents(), mCyclicComponents(), mCG(NULL)
{
}

/*!
 * Compute the strongly connected components of a directed graph.
 * The algorithm is based on the observation that a strongly-connected
 * component encountered during a depth-first traversal will have a structure
 * in which all paths from the root of the component through tree edges to
 * other parts of the component (ending with a back edge or cross edge to
 * another part of the same component) will terminate at a node with
 * a depth-first discovery time greater or equal to the root's.
 *
 * So the root of a component can be determined by collecting the minimum
 * discovery time reachable from each node through tree paths terminated
 * by back edges or cross edges to nodes not in a different component.
 */
void GraphSCC::compute(Graph* g)
{
  // clear the SCCRecord fields
  for (Iter<GraphNode*> n = g->nodes(); !n.atEnd(); ++n)
    g->setScratch(*n, (UInt32)NULL);

  // reset the result state
  cleanup();

  // traverse the graph
  mOriginalGraph = g;
  walk(g);
}

/*! 
 * Assign a discovery time for this node (if we haven't already) and
 * push a record for it onto the stack.
 */
GraphWalker::Command GraphSCC::visitNodeBefore(Graph* g, GraphNode* node)
{
  // create an SCCRecord for this node, if it doesn't already have one
  SCCRecord* record = (SCCRecord*)(g->getScratch(node));
  if (record == NULL)
  {
    record = new SCCRecord(node, ++mCount);
    g->setScratch(node, (UIntPtr)record);
  }

  // push this node's record on the stack
  mStack.push_back(record);

  return GraphWalker::GW_CONTINUE;
}

/*!
 * Check if this node is a root (ie. it has a minimum link value equal to its own discovery time).
 * If so, enumerate the nodes in its component by popping all records off the stack until we
 * reach our own record.
 *
 * Also, if this node was reached from a parent node, update its minimum link value to reflect
 * the minimum link we have finished computing for ourselves.
 */
GraphWalker::Command GraphSCC::visitNodeAfter(Graph* g, GraphNode* node)
{
  // Keep track of record parent and minLink because if this is the
  // root of a strongly connected component, we are going to delete
  // the record for this node
  SCCRecord* record = (SCCRecord*)(g->getScratch(node));
  GraphNode* parent = record->parent();
  UInt32 minLink = record->minLink();

  // if this node is a root, enumerate the nodes in its strongly connected component
  if (record->isRoot())
  {
    SCCRecord* r;
    GraphNode* n;
    Component* component = new Component(record->isInCycle());
    mComponents.push_front(component);
    if (component->isCyclic())
      mCyclicComponents.push_front(component);
    do
    {
      r = mStack.back();
      mStack.pop_back();
      n = r->node();
      g->setScratch(n,(UInt32)NULL);
      component->addNode(n);
      delete r;
    } while (n != node);
  }

  // if we have a parent, update its minLink with our data
  if (parent != NULL)
  {
    SCCRecord* rp = (SCCRecord*)(g->getScratch(parent));
    rp->updateMinLink(minLink);
  }

  return GraphWalker::GW_CONTINUE;
}

/*!
 * We are traversing from a node to an unvisited child.  Prepare the child by assigning it a
 * discovery time and creating an SCCRecord for it.
 */
GraphWalker::Command GraphSCC::visitTreeEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  // create an SCCRecord for the child, recording the parent pointer
  GraphNode* child = g->endPointOf(edge);
  SCCRecord* record = new SCCRecord(child, ++mCount, from);
  g->setScratch(child,(UIntPtr)record);

  return GraphWalker::GW_CONTINUE;
}

/*!
 * We have found a cycle, so retrieve the minimum link value for the node and update our parent.
 */
GraphWalker::Command GraphSCC::visitBackEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  SCCRecord* rp = (SCCRecord*)(g->getScratch(from));
  SCCRecord* rc = (SCCRecord*)(g->getScratch(g->endPointOf(edge)));

  // update the parent's minLink based on the child's value
  rp->updateMinLink(rc->minLink());

  // record in the child that it is part of a cycle
  rc->setInCycle();

  return GraphWalker::GW_CONTINUE;
}

/*!
 * We have encountered an edge to a node that has already been visited.  If it is part of a different
 * component, ignore it.  If it is part of our own component (it will still be on the stack), then use
 * its minimum link value to update our parent.
 */
GraphWalker::Command GraphSCC::visitCrossEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  // if the child node is on the stack, it is part of this connected component, and we should use its minLink
  GraphNode* child = g->endPointOf(edge);
  SCCRecord* rc = (SCCRecord*)(g->getScratch(child));
  if (rc != NULL)
  {
    SCCRecord* rp = (SCCRecord*)(g->getScratch(from));
    rp->updateMinLink(rc->minLink());
  }
  
  return GraphWalker::GW_CONTINUE;
}

/*!
 * Loop over all components of the graph.
 */
GraphSCC::ComponentLoop GraphSCC::loopComponents() const
{
  return ComponentLoop(mComponents);
}

/*!
 * Loop over all cyclic components of the graph.
 */
GraphSCC::ComponentLoop GraphSCC::loopCyclicComponents() const
{
  return ComponentLoop(mCyclicComponents);
}

//! Extract the subgraph of nodes in the component
Graph* GraphSCC::extractComponent(Component* component, bool copyScratch) const
{
  return mOriginalGraph->subgraph(component->mNodes, copyScratch);
}

/*!
 * Build the component graph induced by a given graph.
 */
GraphSCC::CompGraph* GraphSCC::buildComponentGraph(Graph* g)
{
  // Create the component graph for population
  INFO_ASSERT(mCG == NULL, "Function called more than once.");
  mCG = new CompGraph();

  // Create the components
  compute(g);

  // build a map from graph nodes to components
  UtMap<GraphNode*,CNode*> nodeMap;
  for (ComponentLoop loop = loopComponents(); !loop.atEnd(); ++loop)
  {
    Component* comp = *loop;
    CNode* node = new CNode(comp);
    mCG->addNode(node);
    for (Component::NodeLoop n = comp->loopNodes(); !n.atEnd(); ++n)
    {
      GraphNode* graphNode = *n;
      nodeMap[graphNode] = node;
    }
  }

  // build edges between components where needed
  UtSet<std::pair<CNode*,CNode*> > done;
  for (Iter<GraphNode*> n = g->nodes(); !n.atEnd(); ++n)
  {
    GraphNode* from = *n;
    CNode* f = nodeMap[from];
    for (Iter<GraphEdge*>e = g->edges(from); !e.atEnd(); ++e)
    {
      GraphEdge* edge = *e;
      GraphNode* to = g->endPointOf(edge);
      CNode* t = nodeMap[to];
      if (f == t)
        continue;
      if (done.find(std::make_pair(f,t)) == done.end())
      {
        // new edge
        f->addEdge(new CEdge(t, NULL));
        done.insert(std::make_pair(f,t));
      }
    }
  }

  return mCG;
}

//! Get the component from a node in the component graph
GraphSCC::Component* GraphSCC::getComponent(const GraphNode* node) const
{
  const CNode* compNode = mCG->castNode(node);
  INFO_ASSERT(compNode != NULL, "Invalid GraphNode.");
  return compNode->getData();
}

//! release all of the resources being used by a GraphSCC instance
void GraphSCC::cleanup()
{
  for (ComponentLoop loop = loopComponents(); !loop.atEnd(); ++loop)
  {
    Component* comp = *loop;
    delete comp;
  }
  mOriginalGraph = NULL;
  mComponents.clear();
  mCyclicComponents.clear();
}

GraphSCC::~GraphSCC()
{
  INFO_ASSERT(mStack.empty(), "Record stack not empty.");
  if (mCG != NULL)
    delete mCG;
  cleanup();
}
