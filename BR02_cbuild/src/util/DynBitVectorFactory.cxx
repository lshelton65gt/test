// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/DynBitVector.h"
#include "util/UtHashSet.h"
#include "util/Zstream.h"
#include "util/UtIOStream.h"

static const char* cFactorySig = "DynBitVectorFactory";
static const UInt32 cFactoryVersion = 0;

class DynBitVectorFactory::HashBV: public HashPointerValue<DynBitVector*> {
public:
  HashBV(bool sizeUniquifies): mSizeUniquifies(sizeUniquifies) {}

  // Override the hash/equal functions to take into account the width
  size_t hash(const DynBitVector* var) const {
    size_t h = mSizeUniquifies? var->size(): 0;
    return h + var->hash();
  }

  bool equal(const DynBitVector* v1, const DynBitVector* v2) const {
    return (v1 == v2) ||
      ((!mSizeUniquifies || (v1->size() == v2->size())) && (*v1 == *v2));
  }

  bool mSizeUniquifies;
};

DynBitVectorFactory::DynBitVectorFactory(bool sizeUniquifies) {
  mBVs = new BVHash(sizeUniquifies);
}

DynBitVectorFactory::~DynBitVectorFactory() {
  for (BVHash::UnsortedLoop p = mBVs->loopUnsorted(); ! p.atEnd(); ++p)
    delete *p;
  delete mBVs;
}

const DynBitVector* DynBitVectorFactory::find(const DynBitVector& value) 
{
  DynBitVector* ret = NULL;
  DynBitVector* valRef = const_cast<DynBitVector*>(&value);
  BVHash::iterator p = mBVs->find(valRef);
  if (p != mBVs->end())
    ret = *p;
  return ret;
}

void DynBitVectorFactory::insert(const DynBitVector& value)
{
  (void) alloc(value);
}

const DynBitVector* DynBitVectorFactory::alloc(const DynBitVector& value) 
{
  const DynBitVector* ret = find(value);
  if (ret == NULL)
  {
    DynBitVector* tmp = new DynBitVector(value);
    ret = tmp;
    mBVs->insert(tmp);
  }
  return ret;
}

bool DynBitVectorFactory::dbWrite(ZostreamDB& out) const
{
  out << cFactorySig;
  out << cFactoryVersion;
  return out.writePointerValueContainer(*mBVs);
}

bool DynBitVectorFactory::dbRead(ZistreamDB& in)
{
  UtString signature;
  if (! (in >> signature))
    return false;
  
  if (signature.compare(cFactorySig) != 0)
  {
    UtString buf;
    buf << "Invalid DynBitVectorFactory signature: " << signature;
    in.setError(buf.c_str());
    return false;
  }
  
  UInt32 version;
  in >> version;
  if (in.fail())
    return false;
  
  if (version > cFactoryVersion)
  {
    UtString buf;
    buf << "Unsupported DynBitVectorFactory version: " << version;
    in.setError(buf.c_str());
    return false;
  }
  
  DynBitVector dummy;
  return in.readPointerValueContainer(mBVs, dummy);
}

void DynBitVectorFactory::printStats(int indent_arg) {
  int numspaces = UtIOStreamBase::limitIntArg(indent_arg, 0, 150, 0);
  for (int i = 0; i < numspaces; i++) {UtIO::cout() << " ";}
  UtIO::cout() << "Number of dynamic bit vectors: " << mBVs->size() << UtIO::endl;
}

bool DynBitVectorFactory::empty() const {
  return mBVs->empty();
}
