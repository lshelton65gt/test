// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/UtStringArray.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"

#include <cstring>

UtStringArray::UtStringArray()
{}

UtStringArray::UtStringArray(UInt32 reserveSize)
  : mStrs(reserveSize)
{}

UtStringArray::UtStringArray(const UtStringArray& src)
{
  copy(src);
}

UtStringArray& UtStringArray::operator=(const UtStringArray& src)
{
  if (this != &src)
  {
    clear();
    copy(src);
  }
  return *this;
}

UtStringArray::~UtStringArray()
{
  clear();
}

const char* UtStringArray::back() const
{
  return mStrs.back();
}

void UtStringArray::push_back(const char* entry)
{
  if (entry)
    add(entry, strlen(entry));
  else
    mStrs.push_back(NULL);
}

void UtStringArray::push_back(const UtString& str)
{
  add(str.c_str(), str.size());
}

void UtStringArray::add(const char* entry, UInt32 len)
{
  len += 1; // for '\0'
  char* s = (char*) carbonmem_malloc(len);
  strncpy(s, entry, len);
  mStrs.push_back(s);
}

void UtStringArray::copy(const UtStringArray& src)
{
  UInt32 numSrcElems = src.size();
  for (UInt32 i = 0; i < numSrcElems; ++i)
    push_back(src[i]);
}

UInt32 UtStringArray::size() const
{
  return mStrs.size();
}

const char* UtStringArray::operator[] (UInt32 index) const
{
  return mStrs[index];
}

char* UtStringArray::operator[] (UInt32 index)
{
  return mStrs[index];
}

#if 0
char*& UtStringArray::operator[] (UInt32 index)
{
  return mStrs[index];
}
#endif

void UtStringArray::resize(UInt32 capacity)
{
  mStrs.resize(capacity);
}

UtStringArray::UnsortedLoop 
UtStringArray::loopUnsorted()
{
  return UnsortedLoop(mStrs);
}

UtStringArray::UnsortedCLoop 
UtStringArray::loopCUnsorted() const
{
  return UnsortedCLoop(mStrs);
}

UtStringArray::iterator UtStringArray::begin() 
{
  return mStrs.begin();
}
  
UtStringArray::const_iterator UtStringArray::begin() const
{
  return mStrs.begin();
}
  
UtStringArray::iterator UtStringArray::end() 
{
  return mStrs.end();
}

UtStringArray::const_iterator UtStringArray::end() const
{
  return mStrs.end();
}

bool UtStringArray::empty() const
{
  return mStrs.empty();
}

void UtStringArray::clear()
{
  CharArray::iterator e = mStrs.end();
  for (CharArray::iterator p = mStrs.begin(); p != e; ++p)
  {
    char* str = *p;
    if (str) 
      carbonmem_free(str);
  }

  mStrs.clear();
}

void UtStringArray::pop_back()
{
  if (! empty())
  {
    char* s = mStrs.back();
    if (s)
      carbonmem_free(s);
  }
  
  mStrs.pop_back();
}

UtStringArray::reverse_iterator UtStringArray::rbegin()
{
  return mStrs.rbegin();
}

UtStringArray::const_reverse_iterator UtStringArray::rbegin() const
{
  return mStrs.rbegin();
}

UtStringArray::reverse_iterator UtStringArray::rend()
{
  return mStrs.rend();
}

UtStringArray::const_reverse_iterator UtStringArray::rend() const
{
  return mStrs.rend();
}

size_t UtStringArray::remove(const char* elt)
{ 
  size_t removed = 0;
  for (size_t i = 0, j = 0, n = size(); j < n; ++j) {
    char* cur = mStrs[j];
    if (strcmp(cur, elt) != 0) {
      if (i != j) {
        mStrs[i] = cur;
      }
      ++i;
    }
    else
    {
      ++removed;
      carbonmem_free(cur);
    }
  }
  if (removed != 0) {
    INFO_ASSERT(removed > 0, elt);
    INFO_ASSERT(removed <= size(), elt);
    mStrs.resize(size() - removed, true);
  }
  return removed;
}

void UtStringArray::erase(iterator iter)
{
  if (iter != end())
  {
    char* p = *iter;
    mStrs.erase(iter);
    carbonmem_free(p);
  }
}

UtStringArray::const_iterator
UtStringArray::find(const char* elt) const
{
  bool found = false;
  const_iterator ret = end();
  for (UnsortedCLoop p = loopCUnsorted(); ! found && ! p.atEnd(); ++p)
  {
    if (strcmp(*p, elt) == 0)
    {   
      found = true;
      ret = p.begin(); // misnomer in Loop. points to current
    }
  }
  return ret;
}
