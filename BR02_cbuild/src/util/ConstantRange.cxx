// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/ConstantRange.h"
#include <algorithm>
#include <stdio.h>
#include "util/UtString.h"
#include "util/Zstream.h"
#include "util/UtConv.h"
#include "util/UtHashSet.h"
#include "util/UtIOStream.h"
#include "util/UtVector.h"

static const char* cConstantRangeSig = "ConstantRange";
static const UInt32 cConstantRangeVersion = 0;
static const char* cConstantRangeFactorySig = "ConstantRangeFactory";
static const UInt32 cConstantRangeFactoryVersion = 0;

static void sFailMsg1(const char* msg, const ConstantRange& one)
{
  UtIO::cout() << msg << UtIO::endl;
  one.print();
  UtIO::cout() << UtIO::endl;
  UtIO::cout().flush();
}

static void sFailMsg2(const char* msg, const ConstantRange& one, const ConstantRange& two)
{
  UtIO::cout() << msg << UtIO::endl;
  UtIO::cout() << "1: ";
  one.print();
  UtIO::cout() << UtIO::endl << "2: " ;
  two.print();
  UtIO::cout() << UtIO::endl;
  UtIO::cout().flush();
}

//! Wrapper for hashset
class ConstantRangeFactory::Aux
{
  typedef UtHashSet<ConstantRange*, HashPointerValue<ConstantRange*> > RangeSet;
  typedef RangeSet::const_iterator CRangeSetIter;
  
public:
  CARBONMEM_OVERRIDES

  Aux() {}

  ~Aux() {
    mRanges.clearPointers();
  }

  const ConstantRange* find(SInt32 msb, SInt32 lsb)
  {
    ConstantRange tmp(msb, lsb);
    return findRange(&tmp);
  }

  
  bool dbWrite(ZostreamDB& out) const
  {
    out << cConstantRangeFactorySig;
    out << cConstantRangeFactoryVersion;
    return out.writePointerValueContainer(mRanges);
  }
  
  bool dbRead(ZistreamDB& in)
  {
    UtString signature;
    if (! (in >> signature))
      return false;

    if (signature.compare(cConstantRangeFactorySig) != 0)
    {
      UtString buf;
      buf << "Invalid ConstantRangeFactory signature: " << signature;
      in.setError(buf.c_str());
      return false;
    }
    
    UInt32 version;
    in >> version;
    
    if (in.fail())
      return false;
    
    if (version > cConstantRangeFactoryVersion)
    {
      UtString buf;
      buf << "Unsupported ConstantRangeFactory version: " << version;
      in.setError(buf.c_str());
      return false;
    }
    
    ConstantRange dummyRange;
    return in.readPointerValueContainer(&mRanges, dummyRange);
  }

  void add(const Aux* otherFactory)
  {
    for (RangeSet::UnsortedCLoop p = otherFactory->mRanges.loopCUnsorted();
         ! p.atEnd(); ++p)
    {
      const ConstantRange* tmp = *p;
      (void) findRange(tmp);
    }
  }

private:
  RangeSet mRanges;
  
  const ConstantRange* findRange(const ConstantRange* key)
  {
    const ConstantRange* ret = NULL;
    CRangeSetIter p = mRanges.find(const_cast<ConstantRange*>(key));
    if (p == mRanges.end())
    {
      ConstantRange* range = new ConstantRange(*key);
      mRanges.insert(range);
      ret = range;
    }
    else
      ret = *p;
    
    return ret;
  }

};


void ConstantRange::print() const
{
  fprintf(stdout, "[%ld:%ld]", (long) mMsb, (long) mLsb);
  fflush(stdout);
}

const char* ConstantRange::format(UtString* str) const
{
  *str << "[" << mMsb << ":" << mLsb << "]";
  return str->c_str();
}

UInt32 ConstantRange::getLength() const
{ 
  return std::abs(mMsb - mLsb) + 1; 
}

SInt32 ConstantRange::offsetUnbounded(SInt32 idx) const
{
  if (mMsb > mLsb)
    return idx - mLsb;
  return mLsb - idx;
}

UInt32 ConstantRange::offsetBounded(SInt32 idx) const
{
  FUNC_ASSERT(contains(idx), sFailMsg1("Index not within range.", *this));
  return offsetUnbounded(idx);
}

SInt32 ConstantRange::index(UInt32 offset, bool check) const
{
  if (check)
    FUNC_ASSERT(offset < getLength(), sFailMsg1("Offset out-of-bounds", *this));
  if (mMsb > mLsb)
    return mLsb + offset;
  else
    return mLsb - offset;
}

ConstantRange* ConstantRange::getMax(ConstantRange* first, ConstantRange* second)
{
  ConstantRange* max = first;
  if (first->getLength() < second->getLength())
    max = second;
  return max;
}

bool ConstantRange::adjacent(const ConstantRange &r) const
{
  SInt32 this_rightmost = rightmost();
  SInt32 r_rightmost = r.rightmost();
  SInt32 this_leftmost = leftmost();
  SInt32 r_leftmost = r.leftmost();

  SInt32 this_direction = ((this_leftmost == this_rightmost ) ? 0 : ( ( (this_leftmost - this_rightmost) > 0 ) ? -1 : 1));
  SInt32 r_direction =    ((r_leftmost    == r_rightmost    ) ? 0 : ( ( (r_leftmost    - r_rightmost   ) > 0 ) ? -1 : 1));
  FUNC_ASSERT( ( (this_direction * r_direction) >= 0), sFailMsg2("Constant ranges with different directions", *this,r));
  
  return ( (not overlaps(r)) and
           (( std::abs(this_rightmost - r_leftmost) == 1 ) or ( std::abs(r_rightmost - this_leftmost) == 1 ))   );
}

ConstantRange ConstantRange::combine(const ConstantRange &r) const
{
  FUNC_ASSERT(adjacent(r), sFailMsg2("Cannot combine two ranges that are not adjacent.", *this, r));

  SInt32 this_rightmost = rightmost();
  SInt32 r_leftmost = r.leftmost();

  
  if ( std::abs( this_rightmost - r_leftmost ) == 1 ){
    return ConstantRange(leftmost(), r.rightmost());
  } else {
    return ConstantRange(r_leftmost, this_rightmost);
  }
}


bool ConstantRange::overlaps(const ConstantRange &r) const
{
  SInt32 this_rightmost = rightmost();
  SInt32 r_rightmost = r.rightmost();

  if (this_rightmost < r_rightmost) {
    return (r_rightmost <= leftmost());
  } else {
    return (this_rightmost <= r.leftmost());
  }
}

ConstantRange ConstantRange::overlap(const ConstantRange &r) const
{
  FUNC_ASSERT(overlaps(r), sFailMsg2("This range and specified range do not intersect.", *this, r));
  
  SInt32 left = std::min(leftmost(), r.leftmost());
  SInt32 right= std::max(rightmost(), r.rightmost());

  if (mMsb < mLsb)
    std::swap (left, right);

  return ConstantRange(left, right);
}

SInt32 ConstantRange::rightmost() const
{
  return std::min(mMsb, mLsb);
}

SInt32 ConstantRange::leftmost() const
{
  return std::max(mMsb, mLsb);
}

bool ConstantRange::dbWrite(ZostreamDB& db) const
{
  db << cConstantRangeSig;
  db << cConstantRangeVersion;
  db << mMsb;
  db << mLsb;
  return ! db.fail();
}

bool ConstantRange::dbRead(ZistreamDB& db)
{
  UtString signature;
  if (! (db >> signature))
    return false;
  
  if (signature.compare(cConstantRangeSig) != 0)
  {
    UtString buf;
    buf << "Invalid ConstantRange signature: " << signature;
    db.setError(buf.c_str());
    return false;
  }
  
  UInt32 version;
  if (! (db >> version))
    return false;
  
  if (version > cConstantRangeVersion)
  {
    UtString buf;
    buf << "Unsupported ConstantRange version: " << version;
    db.setError(buf.c_str());
    return false;
  }
  
  db >> mMsb;
  db >> mLsb;

  return !db.fail();
}

size_t ConstantRange::hash() const
{
  return 100*mMsb + mLsb;
}

void ConstantRange::normalize( const ConstantRange *r, bool check )
{
  if (check)
    FUNC_ASSERT( r->contains(mMsb) && r->contains(mLsb), sFailMsg2("Ranges do not overlap", *this, *r));
  
  SInt32 lsb = r->getLsb();
  if (r->getMsb() >= lsb ) {
    // downto
    mMsb = mMsb - lsb;
    mLsb = mLsb - lsb;
  } else {
    // to
    mMsb = lsb - mMsb;
    mLsb = lsb - mLsb;
  }
  FUNC_ASSERT(mMsb >= mLsb, sFailMsg2("Consistency check failed.", *this, *r));
}

void ConstantRange::denormalize( const ConstantRange *r, bool check )
{
  mMsb = r->index( mMsb, check );
  mLsb = r->index( mLsb, check );

  if (check)
    FUNC_ASSERT( r->contains(mMsb) && r->contains(mLsb), sFailMsg2("Ranges do not overlap", *this, *r));
}

UInt32 ConstantRange::numWordsNeeded() const
{
  return CarbonValRW::getRangeNumUInt32s(mMsb, mLsb);
}

UInt32 ConstantRange::numBytesNeeded() const
{
  UInt32 length = getLength();
  const UInt32 byteWidth = sizeof(UInt8) * 8;
  return (length + byteWidth - 1)/byteWidth;
}

bool ConstantRange::safeIndex(UInt32 num_bits) const
{
  if ((num_bits < 1) or (num_bits > 31)) return false;
  UInt32 max_num = 1 << num_bits;
  return ((max_num <= getLength()) and ((mMsb == 0) or (mLsb == 0)));
}

ptrdiff_t ConstantRange::compare(const ConstantRange& b) const
{
  ptrdiff_t diff = mLsb - b.mLsb;
  if (diff == 0) {
    // Starting offset is the same, sort by size
    diff = mMsb - b.mMsb;
  }

  return diff;
}

bool ConstantRange::isContainedFlip(const ConstantRange& b) const
{
  bool isContainedFlip = false;
  if (contains(b))
    // this range contains b, now we just need to see if b is flipped
    isContainedFlip = isFlipped(b);
  
  return isContainedFlip;
}

bool ConstantRange::isFlipped(const ConstantRange& b) const
{
  bool isFlipped = false;
  if (mMsb < mLsb)
    // if this msb < lsb then b is flipped if lsb < msb
    isFlipped = b.mLsb < b.mMsb;
  else
    isFlipped = b.mMsb < b.mLsb;
  return isFlipped;
}

void ConstantRange::switchSBs()
{
  SInt32 tmp = mMsb;
  mMsb = mLsb;
  mLsb = tmp;
}

void ConstantRange::truncate(UInt32 newLength)
{
  FUNC_ASSERT(newLength < getLength(), sFailMsg1("Truncation length > width", *this));
  UInt32 diff = getLength() - newLength;
  if (getLsb() > getMsb())
    // Little endian
    setLsb(getLsb() - diff);
  else
    setLsb(getLsb() + diff);
  FUNC_ASSERT(newLength == getLength(), sFailMsg1("Truncation miscalculation", *this));
}

// ConstantRangeVectorCompare class implementation.

bool ConstantRangeVectorCompare::operator()(const ConstantRangeVector& vec1,
                                            const ConstantRangeVector& vec2) const
{
  return (compare(vec1, vec2) < 0);
}

SInt32 ConstantRangeVectorCompare::compare(const ConstantRangeVector& vec1,
                                           const ConstantRangeVector& vec2) const
{
  // Compare vector sizes. Vectors with less elements are smaller than
  // vectors with more elements.
  SInt32 size1 = vec1.size();
  SInt32 size2 = vec2.size();

  if (size1 != size2)
    return (size1 - size2);

  // The vectors have identical number of elements. Since ranges for higher
  // dimensions appear at lower indices, the first smaller higher dimension
  // decides which vector is smaller.
  ConstantRangeVector::const_iterator first1 = vec1.begin();
  ConstantRangeVector::const_iterator last1 = vec1.end();
  ConstantRangeVector::const_iterator first2 = vec2.begin();

  SInt32 compVal = 0;
  while ((compVal == 0) && (first1 != last1))
  {
    compVal = (*first1).compare(*first2);
    ++first1;
    ++first2;
  }
  return compVal;
}

// ConstantRangeFactory implementation

ConstantRangeFactory::ConstantRangeFactory()
{
  mAux = new Aux;
}

ConstantRangeFactory::~ConstantRangeFactory()
{
  delete mAux;
}

const ConstantRange* ConstantRangeFactory::find(SInt32 msb, SInt32 lsb)
{
  return mAux->find(msb, lsb);
}

bool ConstantRangeFactory::dbWrite(ZostreamDB& out) const
{
  return mAux->dbWrite(out);
}

bool ConstantRangeFactory::dbRead(ZistreamDB& in)
{
  return mAux->dbRead(in);
}

void ConstantRangeFactory::add(const ConstantRangeFactory& otherFactory)
{
  mAux->add(otherFactory.mAux);
}
