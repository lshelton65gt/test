// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/




/*!
  \file
  Implements a class to keep track of and print resource usage statistics
*/

#include "util/Stats.h"
#include "util/MemManager.h"
#if !pfMSVC
#include <unistd.h>
#endif
#include <stdio.h>
#include "util/UtIOStream.h"
#include "util/UtStack.h"
#include "util/UtVector.h"
#include "util/OSWrapper.h"
#include "util/UtIStream.h"
#include "util/UtHashMap.h"

// please do not use "using namespace std"

//! Gets the current time of day (real, user, and system) and mem usage
static inline void
getData(RealtimeType* realTime, CpuStruct* procTime)
{
  // Get the information first so that it is as close to one spot in
  // time as possibe (matters more for time).
#if pfHAS_TMS
  int status1 = gettimeofday(realTime, NULL);
  clock_t curTime = times(procTime);
  INFO_ASSERT((status1 == 0) && (curTime != ((clock_t)-1)), "syscall gettimeofday or times failed.");
#else
  int status1 = QueryPerformanceCounter(realTime);
  INFO_ASSERT (status1 != 0, "syscall QueryPerformanceCounter failed.");
  FILETIME dummyFILETIME;
  int status2 = GetProcessTimes(GetCurrentProcess(),
                                &dummyFILETIME, // creation time
                                &dummyFILETIME, // exit time
                                &procTime->system,
                                &procTime->user);
  INFO_ASSERT ((status2 != 0), "syscall GetProcessTimes failed.");
#endif
}

#if pfWINDOWS
//! Computes the elapsed real time in seconds given end and start
//  QueryPerformanceCounter() values.
double
computeRealTime(LARGE_INTEGER endTime, LARGE_INTEGER startTime)
{
  static LARGE_INTEGER freq = {{0, 0}};
  if (freq.QuadPart == 0) {
    VERIFY(QueryPerformanceFrequency(&freq) != 0);
#if 0
    printf("Initialized freq.LowPart: %ld freq.HighPart: %ld\n",
           (long)freq.LowPart, (long)freq.HighPart);
#endif
  }

  INFO_ASSERT(freq.QuadPart != 0, "syscall QueryPerformanceFrequency failed.");
  return (endTime.QuadPart - startTime.QuadPart)/(double)freq.QuadPart;
}
#else
//! Computes the elapsed real time in seconds
double
computeRealTime(struct timeval endTime, struct timeval startTime)
{
  double start = (double)startTime.tv_sec + startTime.tv_usec / 1e6;
  double end = (double)endTime.tv_sec + endTime.tv_usec / 1e6;
  return (end - start);
}
#endif // pfWINDOWS

#if pfWINDOWS

//! Computes the elapsed user time in seconds
double
computeUserTime(CpuStruct endTime, CpuStruct startTime)
{
  ULARGE_INTEGER
    end = {{endTime.user.dwLowDateTime, endTime.user.dwHighDateTime}};
  ULARGE_INTEGER
    start = {{startTime.user.dwLowDateTime, startTime.user.dwHighDateTime}};

#if 0
  static int count=20;
  if (count > 0) {
    printf("end: %Ld start: %Ld ", end.QuadPart, start.QuadPart);
    LONGLONG diff = end.QuadPart - start.QuadPart;
    printf("diff: %Lu = %fs\n", diff, (double)diff / 1e7);
    count--;
  }
#endif
  // end and start are in 100-nanosecond intervals.
  return (end.QuadPart - start.QuadPart) * 1e-7;
}

//! Computes the elapsed system time in seconds
double
computeSystemTime(CpuStruct endTime, CpuStruct startTime)
{
  ULARGE_INTEGER
    end = {{endTime.system.dwLowDateTime, endTime.system.dwHighDateTime}};
  ULARGE_INTEGER
    start = {{startTime.system.dwLowDateTime, startTime.system.dwHighDateTime}};

  // end and start are in 100-nanosecond intervals.
  return (end.QuadPart - start.QuadPart) / 1e7;
}

#else  // pfWINDOWS

//! Computes the elapsed user time in seconds
double
computeUserTime(struct tms endTime, struct tms startTime)
{
  static double clocksPerSecond = 0;
  if (clocksPerSecond == 0)
    clocksPerSecond = (double)sysconf(_SC_CLK_TCK);
  return ((((double)endTime.tms_utime + (double)endTime.tms_cutime) -
	   ((double)startTime.tms_utime + (double)startTime.tms_cutime)) /
	  clocksPerSecond);
}

//! Computes the elapsed system time in seconds
double
computeSystemTime(struct tms endTime, struct tms startTime)
{
  static double clocksPerSecond = 0;
  if (clocksPerSecond == 0)
    clocksPerSecond = (double)sysconf(_SC_CLK_TCK);
  return ((((double)endTime.tms_stime + (double)endTime.tms_cstime) -
	   ((double)startTime.tms_stime + (double)startTime.tms_cstime)) /
	  clocksPerSecond);
}
#endif // pfWINDOWS

//! Compute the virtual memory used
static inline double
computeVirtualMemory(void* startSbrk)
{
#if pfLINUX || pfLINUX64
  // Declare this here so that unsupported platforms fail to compile
  double size;

  // Open the file with the data
  FILE *statfile = OSFOpen ("/proc/self/statm", "r", NULL);
  if (!statfile)
    INFO_ASSERT(0, "Failed to open /proc/self/statm");

  // Get the bytes per page once
  static double pages = 0;
  if (pages == 0)
    pages = (double)sysconf(_SC_PAGESIZE);

  // At some point we may want to get more than just the size
  int lsize, resident, shared, trs, drs, lrs, dt;
  fscanf(statfile, "%d %d %d %d %d %d %d", &lsize, &resident,
	 &shared, &trs, &drs, &lrs, &dt);
  fclose(statfile);

  // Compute the size in bytes
  size = (double)lsize * pages;

  // Linux isn't accurate with sbrk, assert so we don't have an unused
  // variable.
  INFO_ASSERT(startSbrk, "NULL sbrk var.");
#endif
#if pfWINDOWS && !pfLINUX
  (void) startSbrk;		// prevent complaints
  double size = 0;
#endif
  return (size / (1024*1024));
}

bool Stats::findEstimatedTimeRemaining(const char* phaseName) {
  // Use the smaller of 'user' and 'real' time.  This is not
  // an exact science, but 'user' will be smaller if the system
  // is loaded, and 'real' will be smaller if the system is
  // unloaded but we are in the back-end process with parallel
  // make.
  if (mPhaseMap != NULL) {
    double elapsedTime = std::min(mAbsData.mReal, mAbsData.mUser);

    PhaseMap::iterator p = mPhaseMap->find(phaseName);
    if (p != mPhaseMap->end()) {
      const DurationIndex& di = p->second;
      double percent = di.first;
      mPhaseIndex = di.second;

      UInt32 percentComplete = UInt32(100*percent + .5);

      // Never go backwards!
      if (percentComplete > mPercentComplete) {
        mPercentComplete = percentComplete;
      }

  //    mOut << UtIO::setw(2) << UInt32(100*percent + .5) << "% complete";

      // If we are over 5%, then go ahead and extrapolate how much total
      // time is expected.
      if ((percent >= .05) && (elapsedTime > 10)) {
        mEstimatedTotalTime = elapsedTime / percent;
  /*
        UInt32 hours = timeRemaining / 60;
        UInt32 minutes = timeRemaining % 60;
        mOut << " " << UtIO::setw(2) << UtIO::setfill('0') << hours
             << ":" << UtIO::setw(2) << UtIO::setfill('0') << minutes
             << " remaining";
        //<<  totalTime << " total, " << elapsedTime << " elapsed";
  */
      }
  //    mOut << UtIO::endl;
    }
    return true;
  } // if
  return false;
} // void Stats::findEstimatedTimeRemaining

static void sPrintTime(UtOStream& out, double numSeconds) {
  UInt32 secs = (UInt32) (numSeconds + .5);
  UInt32 hours = secs / 3600;
  secs -= hours*3600;
  UInt32 minutes = secs / 60;
  secs -= minutes*60;
  out << UtIO::setw(1) << hours << ":"
      << UtIO::setw(2) << UtIO::setfill('0') << minutes << ":"
      << UtIO::setw(2) << UtIO::setfill('0') << secs
      << UtIO::setfill(' ');
}

void Stats::printStats(UtOStream& out,
		       const char* interval,
		       int intervalIndex,
		       const StatsData &data)
{
  bool progress = findEstimatedTimeRemaining(interval);

  if (mEchoStatistics) {
    // Make sure the interval is at exactly 9 characters so that the
    // data is lined up.  Using << UtIO::setMaxw(9) << UtIO::setw(9)
    // gets the right width, but only shows the rightmost characters
    // in 'interval', rather than the leftmost characters.
    char str[10];
    int len = strlen(interval);
    if (len > 9)
    {
      strncpy(str, interval, 9);
      str[9] = '\0';
    }
    else
    {
      for (int i = 0; i < 9 - len; i++)
        str[i] = ' ';
      strcpy(&str[9 - len], interval);
    }
    out << "SL" << UtIO::setw(1) << intervalIndex << ": " << str << " "
        << UtIO::fixed << UtIO::setprecision(1)
        << "Real:" << UtIO::setw(8) << data.mReal << "s"
        << " User:" << UtIO::setw(8) << data.mUser << "s"
        << " Sys:" << UtIO::setw(8) << data.mSys << "s";
    if (mEchoProgress) {
      out << " Elap:";
      sPrintTime(out, mAbsData.mReal);
      out << " Comp:" << UtIO::setw(2) << mPercentComplete << "%";
    }

    double mb = ((double)data.mAllocSize / (1024*1024));
    double hiMb = ((double)data.mHiWater / (1024*1024));

    out << " Mem:" << UtIO::setw(6) << mb << "MB"
        << " Max:" << UtIO::setw(6) << hiMb << "MB"
        << " VM:" << UtIO::setw(6) << data.mSize << "MB"
        << UtIO::endl;
    out.flush();
  } // if
  else if (mEchoProgress && progress) {
    out << "#" << UtIO::setw(3) << mPhaseIndex << " "
        << UtIO::fixed << UtIO::setprecision(1) << " Elapsed: ";
    sPrintTime(out, mAbsData.mReal);
    out << " Complete: " << UtIO::setw(2) << mPercentComplete << "%"
        << UtIO::endl;
    out.flush();
  }
} // void Stats::printStats

void Stats::constructor (void)
{
  // Get the start times for the whole process (should be done first)
  getData(&mStartRealTime, &mStartTime);

  // Get the start Sbrk point
#if pfWINDOWS
  mStartSbrk = 0;
#else
  mStartSbrk = sbrk(0);
#endif

  // The interval start times are the same as the total start times at
  // the beginning. Create the new interval timer
  mIntervalStack->push(new Resources (mStartRealTime, mStartTime));
  mIntervalIndex = 1;

  mEchoStatistics = true;
  mEchoProgress = false;        // turn on without -q

  mGatherStatsFile = NULL;
  mPhaseMap = NULL;
  mEstimatedTotalTime = 0;
  mPhaseIndex = 0;
  mPrevPhaseIndex = 0;
  mPercentComplete = 0;
  mOwnOut = false;
}

Stats::Stats(FILE* f) : mIntervalStack (new ResourceStack), mOut(*new UtOFileStream(f))
{
  constructor ();
  mOwnOut = true;
}

//! Avoid passing objects between different runtime libraries...
Stats::Stats (): mIntervalStack (new ResourceStack), mOut (UtIO::cout())
{
  constructor ();
}

Stats::~Stats()
{
  // Empty the interval timer stack
  INFO_ASSERT(mIntervalStack->size() == 1, "Stack consistency check failed.");
  delete mIntervalStack->top();

  delete(mIntervalStack);
  if (mOwnOut) {
    delete &mOut;
  }

  if (mGatherStatsFile != NULL) {
    if (!mGatherStatsFile->close()) {
      UtIO::cerr() << mGatherStatsFile->getErrmsg() << "\n";
    }
    delete mGatherStatsFile;
  }
  if (mPhaseMap != NULL) {
    delete mPhaseMap;
  }
}

// Opens up a statistics fiole for writing.  We write in here the
// raw #minutes/phaseName pairs, one per line.  They must be
// post-processed into percentages and then averaged with other
// runs to get reasonable statistics.
bool Stats::gatherStatistics(const char* filename, UtString* errmsg) {
  mGatherStatsFile = new UtOBStream(filename);
  if (!mGatherStatsFile->is_open()) {
    *errmsg = mGatherStatsFile->getErrmsg();
    delete mGatherStatsFile;
    mGatherStatsFile = NULL;
    return false;
  }
  return true;
}

// Load up the post-processed pairs of "percentage phase-name",
// so that we can use them to estimate progress.
bool Stats::loadStatistics(StatsProgressRecord* spr) {
  if (mPhaseMap == NULL) {
    mPhaseMap = new PhaseMap;
  }
  else {
    mPhaseMap->clear();
  }
  
  // phase indices start at 0, but we never show progress on the first one,
  // since we have not even parsed the command line yet, so we allow the
  // indices to be 0-based
  for (UInt32 i = 0; spr->phaseName != NULL; ++spr, ++i) {
    (*mPhaseMap)[spr->phaseName] = DurationIndex(spr->percent, i);
  }
  return true;
}

void
Stats::printTotalStatistics()
{
  StatsData data;
  computeIntervalStatistics(&data, mStartRealTime, mStartTime);
  printStats(mOut, "total", 0, data);
}

double Stats::getMemAlloced()
{
  double size = computeVirtualMemory(mStartSbrk);
  return size;
}

void Stats::computeIntervalStatistics(StatsData* data,
				      const RealtimeType &start_real_time,
				      const CpuStruct &start_time,
                                      bool update)
{
  // Get the end time
  RealtimeType endRealTime;
  CpuStruct endTime;
  getData(&endRealTime, &endTime);

  // Get the bytes allocated
  data->mAllocSize = CarbonMem::getBytesAllocated();

  // Get the timer information
  Resources* resources = mIntervalStack->top();
  RealtimeType intervalStartRealTime = start_real_time;
  CpuStruct intervalStartTime = start_time;

  // Compute the total times
  data->mReal = computeRealTime(endRealTime, intervalStartRealTime);
  data->mUser = computeUserTime(endTime, intervalStartTime);
  data->mSys = computeSystemTime(endTime, intervalStartTime);

  // Compute the virtual memory
  data->mSize = computeVirtualMemory(mStartSbrk);
  data->mHiWater = CarbonMem::getHighWater();

  // Compute the absolute data (since the start of the run)
  // Most of the fields accumulate, but the interval times don't.
  mAbsData = *data;
  mAbsData.mReal = computeRealTime(endRealTime, mStartRealTime);
  mAbsData.mUser = computeUserTime(endTime, mStartTime);
  mAbsData.mSys = computeSystemTime(endTime, mStartTime);

  // Update the interval start time
  if (update) {
    resources->mStartRealTime = endRealTime;
    resources->mStartTime = endTime;
  }
}

void
Stats::getIntervalStatistics(StatsData* data) {
  Resources* resources = mIntervalStack->top();
  computeIntervalStatistics(data,
			    resources->mStartRealTime,
			    resources->mStartTime);
}

void
Stats::peekIntervalStatistics(StatsData* data) {
  Resources* resources = mIntervalStack->top();
  computeIntervalStatistics(data,
			    resources->mStartRealTime,
			    resources->mStartTime,
                            false);
}


void
Stats::printIntervalStatistics(const char* intervalName, StatsData* intervalStats)
{
  StatsData data;
  getIntervalStatistics(&data);

  // Print the information
  printStats(mOut, intervalName, mIntervalIndex, data);

  if (intervalStats)
    *intervalStats = data;

  if (mGatherStatsFile != NULL) {
    // Use the smaller of 'user' and 'real' time.  This is not
    // an exact science, but 'user' will be smaller if the system
    // is loaded, and 'real' will be smaller if the system is
    // unloaded but we are in the back-end process with parallel
    // make.
    double elapsedTime = std::min(mAbsData.mReal, mAbsData.mUser);
    *mGatherStatsFile << UtIO::setw(20)
                      << elapsedTime << ' ' << intervalName << '\n';
  }
} // Stats::printIntervalStatistics

void
Stats::pushIntervalTimer()
{
  Resources* resources = new Resources;
  getData(&resources->mStartRealTime, &resources->mStartTime);
  mIntervalStack->push(resources);
  ++mIntervalIndex;
}

void
Stats::popIntervalTimer()
{
  INFO_ASSERT(mIntervalStack->size() > 1, "Stack underflow.");
  delete mIntervalStack->top();
  mIntervalStack->pop();
  --mIntervalIndex;
}


AccumStats::AccumStats(Stats *s) :
  Stats(),
  mNextIdent(0)
{
  if (s) {
    mIntervalIndex = s->mIntervalIndex;
  }
  mIntervalIdentMap = new StringIntMap;
  mAccumStats = new BucketVector;
}


AccumStats::AccumStats(FILE *f, Stats *s) :
  Stats(f),
  mNextIdent(0)
{
  if (s) {
    mIntervalIndex = s->mIntervalIndex;
  }
  mIntervalIdentMap = new StringIntMap;
  mAccumStats = new BucketVector;
}


AccumStats::~AccumStats()
{
  delete mIntervalIdentMap;
  for (BucketVector::iterator iter = mAccumStats->begin();
       iter != mAccumStats->end();
       ++iter)
  {
    BucketData* bd = *iter;
    delete bd;
  }

  delete mAccumStats;
}


void AccumStats::printIntervalStatistics(const char* interval_name, StatsData* intervalStats)
{
  // If the given name doesn't have bucket created for it yet, create one.
  UtString name(interval_name);
  UInt32 interval_ident = 0;
  StringIntMap::iterator iter = mIntervalIdentMap->find(name);
  if (iter == mIntervalIdentMap->end()) {
    interval_ident = mNextIdent;
    (*mIntervalIdentMap)[name] = mNextIdent;
    ++mNextIdent;
    BucketData* new_data = new BucketData(name, mIntervalIndex);
    mAccumStats->push_back(new_data);
  } else {
    interval_ident = (*iter).second;
  }

  BucketData* bucket_data = (*mAccumStats)[interval_ident];

  StatsData cur_data;
  Resources* resources = mIntervalStack->top();
  computeIntervalStatistics(&cur_data,
			    resources->mStartRealTime,
			    resources->mStartTime);

  if (intervalStats)
    *intervalStats = cur_data;
  
  bucket_data->mData += cur_data;
}


void AccumStats::printAllAccumulated()
{
  for (BucketVector::iterator iter = mAccumStats->begin();
       iter != mAccumStats->end();
       ++iter) {
    BucketData* bucket_data = *iter;
    printStats(mOut,
	       bucket_data->mName.c_str(),
	       bucket_data->mLevel,
	       bucket_data->mData);
  }
}
