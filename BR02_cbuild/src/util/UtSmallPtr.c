#include "shell/carbon_shelltypes.h"
#include "util/UtSmallPtr.h"
#include <assert.h>

#if pfLP64
#define SMALLPTR_SHIFT 3
#define SMALLPTR_HIGHVAL 0x800000000L     /* 2**(32 + SMALLPTR_SHIFT) */
#define SMALLPTR_LOWMASK 7                /* 2**SMALLPTR_SHIFT - 1 */

void* SmallPtrExpand(CarbonUInt32 shrunken)
{
  if (shrunken == 0)
    return 0;
  else
    return (void*) (SMALLPTR_HIGHVAL | ((CarbonUIntPtr)shrunken << SMALLPTR_SHIFT));
}

CarbonUInt32 SmallPtrShrink(const void* realPtr)
{
  /* Ensure allocated from our segment, and least significants bits are 0. */
  assert(((CarbonUInt64)realPtr & (SMALLPTR_HIGHVAL | SMALLPTR_LOWMASK)) == SMALLPTR_HIGHVAL || /* this_assert_OK */
         realPtr == 0);
  return (CarbonUInt32)((CarbonUInt64)realPtr >> SMALLPTR_SHIFT);
}

#endif
