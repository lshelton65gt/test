// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/MutexWrapper.h"
#include "util/UtIOStream.h"
#include "util/UtOZStream.h"
#include "util/OSWrapper.h"
#include <new>
#include <cerrno>
#include <fcntl.h>              // O_WRONLY, etc
#include <sys/types.h>
#include <sys/stat.h>
#if pfMSVC
#include <io.h>
#else
#include <unistd.h>             // write()
#endif
#include "util/UtCachedFileSystem.h"
#include "util/UtConv.h"
#include "util/UtStringUtil.h"
#include "util/BitVector.h"
#include "util/DynBitVector.h"
#include "util/UtCheckpointStream.h"
#include "util/UtShellTok.h"
#include "util/CodeStream.h"
#include <ctype.h>              // isalpha, toupper

#define BUFSIZE 400

//! Mutex for cout()/cerr() stream creation/deletion
MUTEX_WRAPPER_DECLARE(sStreamMutex);

//! convert an integer into a string with the specified radix
/*!
  \param T the type of integer, possibly signed
  \param UT the unsigned version of the template variable \a T
  \param num the integer to be converted (must be of type T)
  \param radix output radix
  \param buf buffer where result is built
  \param buf_size  max size of result buffer
  \param parse ISO C parse format indicator
  \returns the number of characters in the result buffer or -1 if failed
 */
template <typename T,
          typename UT
          >
int convNumToStr(T num, UtIO::Radix radix, char * buf, size_t buf_size, UtIO::ParsibleFormat parse)
{
  int numChars = -1;
  switch (radix)
  {
  case UtIO::dec:               // unsigned 
  {
    UT uNum = num;
    numChars = CarbonValRW::writeDecValToStr(buf, buf_size, &uNum, (radix==UtIO::sdec), 8*sizeof(T));
    INFO_ASSERT(numChars != -1, "Decimal conversion failed.");
    break;
  }
  case UtIO::sdec:              // signed
  {
    UT uNum = num;
    numChars = CarbonValRW::writeDecValToStr(buf, buf_size, &uNum, (radix==UtIO::sdec), 8*sizeof(T));
    INFO_ASSERT(numChars != -1, "Decimal conversion failed.");
    if (parse == UtIO::iso_c && uNum == (UT (1)<<((8*sizeof(UT)) - 1))) {
      // Can't print this as a signed number
      INFO_ASSERT (numChars < (int)buf_size, "Insufficient buffer for numeric conversion");
      buf[numChars++] = 'u';
    }
    break;
  }
  case UtIO::hex:
  case UtIO::HEX:
  {
    UT uNum = num;
    numChars = CarbonValRW::writeHexValToStr(buf, buf_size, &uNum, 8*sizeof(T), (radix==UtIO::HEX), true);
    INFO_ASSERT(numChars != -1, "Hex conversion failed.");
    break;
  }
  case UtIO::oct:
  {
    UT uNum = num;
    numChars = CarbonValRW::writeOctValToStr(buf, buf_size, &uNum, 8*sizeof(T), true);
    INFO_ASSERT(numChars != -1, "Octal conversion failed.");
    break;
  }
  case UtIO::bin:
  {
    UT uNum = num;
    numChars = CarbonValRW::writeBinValToStr(buf, buf_size, &uNum, 8*sizeof(T), false);
    INFO_ASSERT(numChars != -1, "Binary conversion failed.");
  }
  } // switch

  return numChars;
}

//! Class to output to stdout and stderr
/*!
  Since a testbench might be linked with one C RTL (e.g. Microsoft's)
  and libcarbon.dll might be linked with another RTL (e.g., MinGW),
  buffered output will come out in the wrong order if we don't do
  something to synchronize it.  We solve that by passing the stdout
  and stderr objects and function pointers from the Carbon Model at
  initialization time, and call through them.
*/
class UtOStdStream: public UtOStream
{
  // Forbid
  UtOStdStream (const UtOStdStream&);
  UtOStdStream& operator=(const UtOStdStream&);

public: CARBONMEM_OVERRIDES
  //! Constructor for FILE*.  To be used only by UtIO::cout() and cerr().
  UtOStdStream(FILE* f);

  //! UtOStream::is_open()
  virtual bool is_open() const;
  //! UtOStream::close()
  virtual bool close();
  //! UtOStream::flush()
  virtual bool flush();

  //! UtOStream::write()
  virtual bool write(const char* s, UInt32 size);

  FILE* getFile() { return mFile; }

  //! Class to encapsulate chains of UtOStdStream objects
  /*!
    Over the course of a simulation, multiple instances of
    UtOStdStream may be allocated for use in cout()/cerr().  They're
    tracked in a simple linked list so they can all be freed on exit.
   */
  class ListNode
  {
  public:
    CARBONMEM_OVERRIDES

    ListNode(UtOStdStream* strm, ListNode* next)
      : mStream(strm), mNext(next) {}
    ~ListNode() {}

    UtOStdStream* getStream() { return mStream; }
    ListNode* getNext() { return mNext; }

  private:
    UtOStdStream* mStream;
    ListNode* mNext;
  };

protected:
  //! The FILE*
  FILE* mFile;
};

bool UtIOStreamBase::limitBoolArg(const bool& source, bool limited_value)
{
  if ( ( source != true ) && ( source != false ) ) {
    return limited_value;
  }
  else{
    return source;
  }
}
int UtIOStreamBase::limitIntArg(const int& source, const int min_in_range_value, const int max_in_range_value, const int limited_value)
{
  if ( ( source < min_in_range_value ) or ( max_in_range_value < source ) ) {
    return limited_value;
  }
  else{
    return source;
  }
}



UtIOStreamBase::UtIOStreamBase()
{
  mWidth = 0;
  mMaxWidth = 0;
  mPrecision = 0;
  mShowPositive = false;
  mFormat[0] = '%';
  mRadix = UtIO::dec;
  mSignedRadix = UtIO::sdec;
  mFloatMode = UtIO::general;
  mFillChar = ' '; // std says default is ' '
  mField = UtIO::right;
  mSlashify = false;
  mQuotify = false;
  mParseFormat = UtIO::undecorated; // no need to format for C/C++ parsers
}

UtIOStreamBase::~UtIOStreamBase() {
}

void UtIOStreamBase::reset()
{
  resetWidth();
  resetMaxWidth();
  resetFill();
}

void UtIOStreamBase::resetWidth()
{
  mWidth = 0;
}

void UtIOStreamBase::resetMaxWidth()
{
  mMaxWidth = 0;
}

void UtIOStreamBase::resetFill()
{
  mFillChar = ' ';
}

void UtIOStreamBase::setwidth(int w) {
  mWidth = w;
}

char* UtIOStreamBase::afmt() {
  INFO_ASSERT(0, "Not used.");  // no one uses afmt, and I don't think it works
              // correctly anyway by trying to use the fill char as
              // part of a printf format string
  if (mWidth == 0)
    strcpy(&mFormat[1], "s");
  else
  {
    if (mFillChar != 0)
      sprintf(&mFormat[1], "%c%us", mFillChar, (UInt32) mWidth);
    else
      sprintf(&mFormat[1], "%us", (UInt32) mWidth);
  }
  reset();
  return mFormat;
}

char* UtIOStreamBase::cfmt() {
  INFO_ASSERT(0, "Not used.");  // no one uses cfmt, and I don't think it works
              // correctly anyway by trying to use the fill char as
              // part of a printf format string
  if (mWidth == 0)
    strcpy(&mFormat[1], "c");
  else
  {
    if ( mFillChar != 0 )
      sprintf(&mFormat[1], "%c%uc", mFillChar, (UInt32) mWidth);
    else
      sprintf(&mFormat[1], "%uc", (UInt32) mWidth);
  }
  reset();
  return mFormat;
}

char* UtIOStreamBase::sfmt(const char* type) {
  INFO_ASSERT(0, "Not used.");  // no one uses sfmt, and I don't think it works
              // correctly anyway by trying to use the fill char as
              // part of a printf format string
  if (mWidth == 0)
  {
    sprintf(&mFormat[1], "%s%c", type, (char) mSignedRadix);
  }
  else
  {
    if ( mFillChar != 0 )
      sprintf(&mFormat[1], "%c%u%s%c", mFillChar, (UInt32) mWidth, type, (char) mSignedRadix);
    else
      sprintf(&mFormat[1], "%u%s%c", (UInt32) mWidth, type, (char) mSignedRadix);
  }
  reset();
  return mFormat;
}

char* UtIOStreamBase::ufmt(const char* type) {
  INFO_ASSERT(0, "Not used.");  // no one uses ufmt, and I don't think it works
              // correctly anyway by trying to use the fill char as
              // part of a printf format string

  if (mWidth == 0)
  {
    sprintf(&mFormat[1], "%s%c", type, (char) mRadix);
  }   
  else
  {
    if ( mFillChar != 0 )
      sprintf(&mFormat[1], "%c%u%s%c", mFillChar, (UInt32) mWidth, type, (char) mRadix);
    else
      sprintf(&mFormat[1], "%u%s%c", (UInt32) mWidth, type, (char) mRadix);
  }
  reset();
  return mFormat;
}

//! double format
char* UtIOStreamBase::dfmt() {
  SInt32 pos = 1;
  
  if (mField == UtIO::left){
    mFormat[pos] = '-';           // make left aligned
    pos++;
  }
  if ( mShowPositive ){
    mFormat[pos] = '+';           // show the sign symbol
    pos++;
  }
  if (mWidth == 0)
  {
    if (mPrecision == 0)
      sprintf(&mFormat[pos], "%c", (char) mFloatMode);
    else
      sprintf(&mFormat[pos], ".%u%c", (UInt32) mPrecision, (char) mFloatMode);
  }
  else if (mPrecision == 0)
    sprintf(&mFormat[pos], "%u%c", (UInt32) mWidth, (char) mFloatMode);
  else
    sprintf(&mFormat[pos], "%u.%u%c", (UInt32) mWidth, (UInt32) mPrecision, (char) mFloatMode);
  reset();
  return mFormat;
}

bool UtIOStreamBase::hasActiveFD() const {
  INFO_ASSERT(0, "Not implemented.");
  return false;
}

void UtIOStreamBase::releaseFD() {
  INFO_ASSERT(0, "Not implemented.");
}

UInt32 UtIOStreamBase::getTimestamp() const
{
  INFO_ASSERT(0, "Not implemented.");
  return 0;
}

bool UtIOStreamBase::save(UtOCheckpointStream &)
{
  INFO_ASSERT(0, "Not implemented.");
  return false;
}


UtOStream::UtOStream() 
{
  mErrmsg = NULL;
  mPrevErrmsg = NULL;
  mfillPatternOther = NULL;
}

UtOStream::~UtOStream()
{
  if (mErrmsg != NULL)
    delete mErrmsg;
  if (mPrevErrmsg != NULL)
    delete mPrevErrmsg;
  if ( mfillPatternOther != NULL ){
    delete mfillPatternOther;
  }
}

void UtOStream::reportError(const char* errmsg)
{
  if (mErrmsg == NULL)
    mErrmsg = new UtString(errmsg);
  else
    *mErrmsg << "\n" << errmsg;
}

bool UtOStream::bad() const {
  return !is_open() || (mErrmsg != NULL);
}

const char * UtOStream::getFilename() const
{
  return NULL;
}

//! Output statistics on the stream
/*virtual*/ void UtOStream::print (UtOStream *) const {}
/*virtual*/ void UtOStream::pr () const { print (&UtIO::cout ()); }

const char* UtOStream::getErrmsg()
{
  if (mErrmsg == NULL)
    return NULL;
  if (mPrevErrmsg != NULL)
    delete mPrevErrmsg;
  mPrevErrmsg = mErrmsg;
  mErrmsg = NULL;
  return mPrevErrmsg->c_str();
}

//! size of space allocated for pattern
static const size_t FILL_PATTERN_LENGTH = 100;
//! a common pattern of characters used for filling
static const char fill_pattern_spaces[FILL_PATTERN_LENGTH+1] = "                                                                                                    ";
//! a common pattern of characters used for filling
static const char fill_pattern_zeros[FILL_PATTERN_LENGTH+1] = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

//! determine any prefix needed and width of value 
/*!
 * determine how much of the data needs to be displayed, and how much
 * padding is required.  mMaxWidth ==0 means no limit on size
 * The possible situations are:
 *
 *  mWidth wider than data
 *    |---------mWidth-----------|  worklength
 *    |----fill----|----data-----|
 * |----------mMaxWidth----------| //a
 *          |---mMaxWidth--------| //b
 *                   |-mMaxWidth-| //c
 *  
 *  mWidth narrower than data (or same length as)
 *                  |---mWidth---| 
 *                |-----data-----| worklength
 *          |---mMaxWidth--------| //d
 *                   |-mMaxWidth-| //e
 */
// UtIO::FieldMode mField;
void UtOStream::calculatePaddingAndDataSizes(const char * buf, const char ** prefix_ptr, size_t * prefix_len, const char ** val_ptr, size_t * val_len, const char ** suffix_ptr, size_t* suffix_len)
{
  (*val_ptr) = buf;             // start at beginning of data, if truncation needed it is done below
  size_t dataLen = *val_len;    // caller tells us the number of chars in val_ptr
  UInt32 pad_length = 0;
  const char* pad_ptr = NULL;
  (*prefix_len) = 0;
  (*prefix_ptr) = NULL;
  (*suffix_len) = 0;
  (*suffix_ptr) = NULL;

  if ( (mWidth > 0) || ( mMaxWidth > 0 ) )
  {
    size_t work_length = (dataLen > (size_t)mWidth) ? dataLen : mWidth;

    if ( (mMaxWidth > 0 ) and ( work_length >= mMaxWidth ) ) {  // b, c, e
      // desired width is mMaxWidth (must truncate something)
      if ( dataLen >= mMaxWidth ){ // c, e, truncate data
        pad_length = 0;
        (*val_len) = mMaxWidth;
        (*val_ptr) += (dataLen - mMaxWidth);
      } else { // b  // truncate part of fill
        pad_length = mMaxWidth - dataLen; 
        (*val_len) = dataLen;
      }
    } else{  // a, d
      // desired width is work_length
      if ( dataLen <= work_length ){  // a
        pad_length = (work_length - dataLen);
        (*val_len) = dataLen;
      } else { // d
        pad_length = 0;
        (*val_len) = dataLen;
      }
    }
    resetWidth();    // std says that width IS reset after a single use by <<
    resetMaxWidth(); // like width the maxWidth is reset after a single use by <<
    
    if ( pad_length > 0 ){
      // first check to see if the fill pattern is one of the 'standard' ones
      if ( pad_length <= FILL_PATTERN_LENGTH ) {
        if ( mFillChar == '0' ){
          pad_ptr = fill_pattern_zeros;
        } else if ( mFillChar == ' ' ) {
          pad_ptr = fill_pattern_spaces;
        }
      }
      if ( pad_ptr == NULL ) {
        // we need a different fill char, or the required prefix is
        // longer than the currently defined length, use the 'other' buffer.
        if ( mfillPatternOther == NULL ){
          // first time we have used this other buffer, put what we need in it
          mfillPatternOther = new UtString(pad_length, mFillChar);
          pad_ptr = (mfillPatternOther->c_str());
        } else if ( mfillPatternOther->c_str()[0] == mFillChar ) {
          if ( mfillPatternOther->size() >= pad_length ){
            // the 'other' buffer has the right fill character and is long enough
            pad_ptr = (mfillPatternOther->c_str());
          } else {
            // the 'other' buffer has the right fill character but is not yet long enough
            mfillPatternOther->append((pad_length - mfillPatternOther->size()), mFillChar );
            pad_ptr = (mfillPatternOther->c_str());
          }
        } else {
          // the 'other' buffer has wrong fill char, so empty then refill with correct char(s)
          mfillPatternOther->clear();
          mfillPatternOther->append(pad_length, mFillChar);
          pad_ptr = (mfillPatternOther->c_str());
        }
      }
    }
    if ( mField == UtIO::left) {
      (*suffix_ptr) = pad_ptr;
      (*suffix_len) = pad_length;
    } else {
      (*prefix_ptr) = pad_ptr;
      (*prefix_len) = pad_length;
    }
  }
}

//! internal method used in place of operator<<, the arguments are not modified by this method
void UtOStream::inserterHelper(const char * buf, size_t num_chars) {
  const char * prefix_ptr;
  size_t prefix_len;
  const char * suffix_ptr;
  size_t suffix_len;
  const char * val_ptr;
  size_t val_len = num_chars;

  calculatePaddingAndDataSizes(buf, &prefix_ptr, &prefix_len, &val_ptr, &val_len, &suffix_ptr, &suffix_len);
  if ( prefix_len > 0 ){
    write ( prefix_ptr, prefix_len);    // write prefix
  }
  if ( val_len > 0){
    write(val_ptr, val_len);            // write data value
  }
  if ( suffix_len > 0 ){
    write ( suffix_ptr, suffix_len);    // write suffix
  }
  if ( mSlashify ) {
    write ( " ", 1);
  }
}


UtOStream& UtOStream::operator<<(const UtString& s) {
  if (mSlashify) {
    UtString buf;
    UtShellTok::quote(s.c_str(), &buf, mQuotify);
    inserterHelper(buf.c_str(), buf.size());
  }
  else {
    inserterHelper(s.c_str(), s.size());
  }
  return *this;
}

UtOStream& UtOStream::operator<<(const char* s) {
  if (mSlashify) {
    UtString buf;
    UtShellTok::quote(s, &buf, mQuotify);
    inserterHelper(buf.c_str(), buf.size());
  }
  else {
    inserterHelper(s, strlen(s));
  }
  return *this;
}

bool UtOStream::formatBignum(const UInt32* words, UInt32 numBits)
{
  UtString buf;

  CarbonRadix radix = eCarbonBin;
  switch (mRadix)
  {
  case UtIO::dec:  radix = eCarbonUDec;  break;
  case UtIO::sdec: radix = eCarbonDec;   break;
  case UtIO::HEX:
  case UtIO::hex:  radix = eCarbonHex;   break;
  case UtIO::oct:  radix = eCarbonOct;   break;
  case UtIO::bin:  radix = eCarbonBin;   break;
  }

  DynBitVector::format(&buf, radix, words, numBits);
  if (mRadix == UtIO::HEX)
  {
    for (UtString::iterator p = buf.begin(); p != buf.end(); ++p) {
      if (isalpha(*p)) {
        *p = toupper(*p);
      }
    }
  }

  const char * prefix_ptr;
  size_t prefix_len;
  const char * suffix_ptr;
  size_t suffix_len;
  const char * val_ptr;
  size_t val_len = buf.size();
  bool status = true;

  calculatePaddingAndDataSizes(buf.c_str(), &prefix_ptr, &prefix_len, &val_ptr, &val_len, &suffix_ptr, &suffix_len);
  if ( prefix_len > 0 ){
    status &= write ( prefix_ptr, prefix_len);
  }
  if ( val_len > 0){
    status &= write(val_ptr, val_len);
  }
  if ( suffix_len > 0 ){
    write ( suffix_ptr, suffix_len);
  }
  return status;
} // bool UtOStream::formatBignum

UtOStream& UtOStream::operator<<(UInt64 num) {
  char buf[100];
  int length = convNumToStr<UInt64,UInt64>(num, mRadix, buf, 100, mParseFormat);
  INFO_ASSERT (length != -1, "Radix conversion failed.");
  inserterHelper(buf, length);
  return *this;
}
UtOStream& UtOStream::operator<<(SInt64 num) {
  char buf[100];
  int length = convNumToStr<SInt64,UInt64>(num, mSignedRadix, buf, 100, mParseFormat);
  INFO_ASSERT (length != -1, "Radix conversion failed.");
  inserterHelper(buf, length);
  return *this;
}

UtOStream& UtOStream::operator<<(UInt32 num) {
  char buf[100];
  int length = convNumToStr<UInt32,UInt32>(num, mRadix, buf, 100, mParseFormat);
  INFO_ASSERT (length != -1, "Radix conversion failed.");
  inserterHelper(buf, length);
  return *this;
}
UtOStream& UtOStream::operator<<(UInt16 num) {
  return *this << UInt32(num);
}

UtOStream& UtOStream::operator<<(SInt16 num) {
  return *this << SInt32(num);
}

UtOStream& UtOStream::operator<<(SInt32 num) {
  char buf[100];
  int length = convNumToStr<SInt32,UInt32>(num, mSignedRadix, buf, 100, mParseFormat);
  INFO_ASSERT (length != -1, "Radix conversion failed.");
  inserterHelper(buf, length);
  return *this;
}

UtOStream& UtOStream::operator<<(double num) {
  char buf[BUFSIZE];
#if pfWINDOWS
  sprintf(&buf[0], dfmt(), num);
  int width = strlen(buf);
  // Microsoft's CRT prints scientific notation with three digit
  // exponents (unless you first call _set_output_format(_TWO_DIGIT_EXPONENT)
  // in VC++ 2005 or newer).  So here we massage the result to match
  // standard IOStreams behavior.
  if (mFloatMode == UtIO::exponent) {
    // Remove leading '0'.
#ifdef CDB
    INFO_ASSERT(width >= 3, "Formatted real number impossibly less than 3 characters");
#endif
    if (width >= 3 && buf[width - 3] == '0') {
      buf[width - 3] = buf[width - 2];
      buf[width - 2] = buf[width - 1];
      buf[width - 1] = '\0';
      width--;
    }
  }
#else
  int width = snprintf(&buf[0], sizeof(buf), dfmt(), num);
#endif
  if ( (width < 0 ) || ( BUFSIZE <= width )){
    // problem with formatting, try again, using general format
#if pfWINDOWS    
    sprintf(&buf[0], dfmt(), num);
    width = strlen(buf);
#else    
    width = snprintf(&buf[0], sizeof(buf), "%g", num);
#endif
    INFO_ASSERT(( (0 < width)&&(width <= BUFSIZE)), "Printing of a real number failed.");
  }
  inserterHelper(buf, width);
  return *this;
}

UtOStream& UtOStream::operator<<(const char ch) {
  if ( ( mWidth > 0 ) || ( mMaxWidth > 0 ) ) {
    inserterHelper(&ch, 1);
  } else {
    write(&ch, 1);
  }
  return *this;
}

UtOStream& UtOStream::operator<<(void* ptr) {
  INFO_ASSERT(sizeof(UIntPtr) == sizeof(void *), "Consistency check failed.");
  //mRadix = UtIO::hex;
  char buf[BUFSIZE];
  int width;
#if pfWINDOWS
  {
    sprintf(&buf[0], FormatPtr, ptr);
    width = strlen(buf);
  }
#else
  {
    width = snprintf(&buf[0], sizeof(buf), FormatPtr, ptr);
  }
#endif  
  INFO_ASSERT(( (0 < width)&&(width <= BUFSIZE)), "Printing of an address failed.");

  inserterHelper(buf, width);
  return *this;
}

UtOStream& UtOStream::operator<<(const void* ptr){
  INFO_ASSERT(sizeof(UIntPtr) == sizeof(void *), "Consistency check failed.");
  char buf[BUFSIZE];
  int width;
#if pfWINDOWS
  {
    sprintf(&buf[0], FormatPtr, ptr);
    width = strlen(buf);
  }
#else
  {
    width = snprintf(&buf[0], sizeof(buf), FormatPtr, ptr);
  }
#endif  
  INFO_ASSERT(( (0 < width)&&(width <= BUFSIZE)), "Printing of an address failed.");
  inserterHelper(buf, width);
  return *this;
}

UtOStream& UtOStream::operator<<(const CodeAnnotation &x)
{
  write (x);
  return *this;
}

UtOStream& UtOStream::operator<<(UtIO::Radix radix) {
  mRadix = radix;
  mSignedRadix = (radix == UtIO::dec)? UtIO::sdec: radix;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::FloatMode mode) {
  mFloatMode = mode;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::Width w) {
  mWidth = w.mWidth;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::MaxWidth w) {
  mMaxWidth = w.mMaxWidth;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::Precision p) {
  mPrecision = p.mPrecision;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::ShowPositve val) {
  mShowPositive = val == UtIO::showpositive;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::Fill f) {
  INFO_ASSERT(f.mFillChar != 0, "Uninitialized Fill");
  mFillChar = f.mFillChar;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::Flush) {
  flush();
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::Endl) {
  *this << '\n';
  // iostreams defines endl to output a newline and flush, but we
  // don't flush because we worry that it could cause a performance
  // penalty.
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::FieldMode mode) {
  mField = mode;
  return *this;
}
UtOStream& UtOStream::operator<<(UtIO::Slashification slash) {
  mQuotify = (slash == UtIO::quotify);
  mSlashify = (slash == UtIO::slashify) || mQuotify;
  return *this;
}

UtOStream& UtOStream::operator<<(UtIO::ParsibleFormat fmt) {
  mParseFormat = fmt;
  return *this;
}

FILE* UtOFileStream::getFile() {return mFile;}
bool UtOFileStream::is_open() const {
  return (mFile != NULL);
}
bool UtOStdStream::is_open() const {
  return (mFile != NULL);
}
bool UtOFileStream::close() {
  INFO_ASSERT(mFile != NULL, "File already closed.");
  bool ret = true;
  if (fclose(mFile) != 0)
  {
    UtString errMsg;
    reportError(OSGetLastErrmsg(&errMsg));
    ret = false;
  }
  mFile = NULL;
  return ret;
}
bool UtOStdStream::close()
{
  // Don't close stdout or stderr.
  return true;
}

bool UtOFileStream::flush()
{
  return flushHelper(fflush, mFile);
}
bool UtOStdStream::flush()
{
  // Use the Carbon Model's fflush() function.
  // We use OSStdio::mstdout or mstderr rather than mFile because they
  // may have changed since this object was instantiated.  That
  // happens if this object is instantiated before the Carbon Model is
  // initialized.
  return flushHelper(OSStdio::pfflush,
                     mFile==stdout ? OSStdio::mstdout :
                     (mFile==stderr ? OSStdio::mstderr : mFile));
}

bool UtOStream::flushHelper(OSStdio::fflush_typedef pflushfunc, FILE* file) {
  bool ret = true;
  if ((pflushfunc)(file) != 0)
  {
    UtString errMsg;
    reportError(OSGetLastErrmsg(&errMsg));
    ret = false;
  }
  return ret;
}

UtOFileStream::UtOFileStream(FILE* f): mFile(f)
{
}

UtOStdStream::UtOStdStream(FILE* f): mFile(f)
{
}

UtOFileStream::UtOFileStream() : mFile(NULL)
{}

bool UtOFileStream::write(const char* buf, UInt32 len)
{
  return writeHelper(buf, len, fwrite, mFile);
}

bool UtOStdStream::write(const char* buf, UInt32 len)
{
  // Use the Carbon Model's fwrite() function.
  // We use OSStdio::mstdout or mstderr rather than mFile because they
  // may have changed since this object was instantiated.  That
  // happens if this object is instantiated before the Carbon Model is
  // initialized.
  return writeHelper(buf, len, OSStdio::pfwrite,
                     mFile==stdout ? OSStdio::mstdout :
                     (mFile==stderr ? OSStdio::mstderr : mFile));
}

bool UtOStream::writeHelper(const char* buf, UInt32 len, OSStdio::fwrite_typedef pwritefunc, FILE* file)
{
  INFO_ASSERT(file, "File not open.");
  int numWritten = (*pwritefunc)(buf, 1, len, file);
  bool ret = (numWritten == (int) len); // cannot fix with EINTR!
  if (!ret) {
    UtString errMsg;
    reportError(OSGetLastErrmsg(&errMsg));
  }
  return ret;
}

UtOFStream::UtOFStream(const char* file) : UtOFileStream()
{
  mFile = OSFOpen(file, "w", NULL);
}

UtOFStream::~UtOFStream()
{
  if (is_open())
    close();
}

UtOZStream::UtOZStream(const char* fileName, void* key)
{
  mZFile = new Zostream(fileName, key);
}

UtOZStream::~UtOZStream()
{
  if (is_open())
    close();
  delete mZFile;
}

bool UtOZStream::is_open() const
{
  return mZFile->is_open();
}

bool UtOZStream::close()
{
  return mZFile->close();
}

bool UtOZStream::flush()
{
  return mZFile->flush();
}

bool UtOZStream::bad() const
{
  return mZFile->fail();
}

bool UtOZStream::write(const char* buf, UInt32 size)
{
  bool ret = mZFile->write(buf, size);
  if (!ret)
    reportError(mZFile->getError());
  return ret;
}

UtOStringStream::UtOStringStream(UtString* str)
  : mStr(str)
{
}

bool UtOStringStream::write(const char* buf, UInt32 size)
{
  mStr->append(buf, size);
  return true;
}

bool UtOStringStream::flush()
{
  return true;
}

//! True if the file/buffer is open
bool UtOStringStream::is_open() const
{
  return true;
}

//! Close the file/buffer
bool UtOStringStream::close()
{
  INFO_ASSERT(0, "OStringStream close not allowed.");
  return false;
}

static UtOStdStream::ListNode* sCoutStreamHead = NULL;
static UtOStdStream::ListNode* sCerrStreamHead = NULL;

static inline UtOStream* getStream(UtOStdStream::ListNode** nodePtr, FILE* f)
{
  MutexWrapper mutex(&sStreamMutex);
  UtOStdStream* streamPtr;
  if (*nodePtr == NULL)
  {
    // this will be deleted by UtIO::memCleanup
    streamPtr = new UtOStdStream(f);
    *nodePtr = new UtOStdStream::ListNode(streamPtr, NULL);
  }
  else if (f != (*nodePtr)->getStream()->getFile())
  {
    // FILE* has changed, so save the old UtOStream and recreate
    // with new file.  This can happen when a Carbon Model is initialized -- we
    // want to use the Carbon Model's values rather than libcarbon's.
    // This will be deleted by UtIO::memCleanup.
    streamPtr = new UtOStdStream(f);
    *nodePtr = new UtOStdStream::ListNode(streamPtr, *nodePtr);
  } else {
    streamPtr = (*nodePtr)->getStream();
  }
      
  return streamPtr;
}

UtOStream& UtIO::cout()
{
  return *getStream(&sCoutStreamHead, OSStdio::mstdout);
}

UtOStream& UtIO::cerr()
{
  return *getStream(&sCerrStreamHead, OSStdio::mstderr);
}

static void sFreeList(UtOStdStream::ListNode** nodePtr)
{
  while (*nodePtr != NULL ){
    UtOStdStream* strm = (*nodePtr)->getStream();
    UtOStdStream::ListNode* next = (*nodePtr)->getNext();
    delete strm;
    delete (*nodePtr);
    (*nodePtr) = next;
  }
}

void UtIO::memCleanup()
{
  // first cleanup the istream
  memCleanupIStream();

  // now output streams
  MutexWrapper mutex(&sStreamMutex);
  sFreeList(&sCoutStreamHead);
  sFreeList(&sCerrStreamHead);
}

// Buffered stream
UtOBStream::UtOBStream(const char* filename, const char* mode, UInt32 bufSize)
  : mFilename(filename)
{
  INFO_ASSERT(bufSize > 0, filename);
  INFO_ASSERT(bufSize < 100*1024*1024, filename); // 100Meg is too big - sanity check
  UtString errmsg;
  mFD = -1;
  mOpenMode = eOBStreamWriteMode;
  if (0 == strcmp(mode, "a"))
    mOpenMode = eOBStreamAppendMode;

  if (open(true))
    mBuffer = new UtFileBuf(bufSize);
  else
    mBuffer = NULL;
  mNumWrites = 0;
}

UtOBStream::UtOBStream(UtICheckpointStream &in)
{
  restore(in);
}

bool UtOBStream::save(UtOCheckpointStream &out)
{
  UtString errorMessage;

  flush();

  SInt64 size = 0;
  if (!OSGetFileSize(mFilename.c_str(), &size, &errorMessage)) {
    reportError(errorMessage.c_str());
  }

  SInt64 offset;
  if (mFD == -1) {
    // if file is not currently open, assume offset is EOF
    offset = size;
  }
  else {
    offset = OSSysTell(mFD, &errorMessage);
    if (offset == -1) {
      reportError(errorMessage.c_str());
    }
  }

  out << mFilename.c_str();
  out.write(&mOpenMode, sizeof(mOpenMode));
  out << size;
  out << offset;
  
  return !out.fail();
}

bool UtOBStream::restore(UtICheckpointStream &in)
{
  SInt64 prevSize, prevOffset;
  SInt64 size;
  bool fileExists;
  UtString errorMessage;

  mFD = -1;

  in >> mFilename;
  in.read(&mOpenMode, sizeof(mOpenMode));
  in >> prevSize;
  in >> prevOffset;

  // get info about file
  OSStatEntry stats;
  if (OSStatFileEntry(mFilename.c_str(), &stats, &errorMessage) == 0) {
    fileExists = stats.exists();
    size = stats.getFileSize();
  }
  else {
    fileExists = false;
    size = 0;
  }

  // open the file
  int flags = O_WRONLY | O_BINARY;
  if (fileExists) {
    // file exists, open for append
    flags |= O_APPEND;
  }
  else {
    // missing output file, create it
    flags |= O_CREAT;
  }
  mFD = OSSysOpen(mFilename.c_str(), flags, 
                  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
                  &errorMessage);
  if (! is_open()) {
    reportError(errorMessage.c_str());
  }
  else {
    // if the file is bigger than it was at the time of the save, 
    // resize it back to where it was.
    if (size > prevSize) {
      if (OSSysFTruncate(mFD, prevSize, &errorMessage) == -1) {
        reportError(errorMessage.c_str());
      }
    }

    // seek to as close to the previous offset as possible.
    SInt64 targetOffset;
    if (size >= prevSize) {
      // file at least as big as is used to be; should be safe to seek to 
      // the previous offset.
      targetOffset = prevOffset;
    }
    else {
      // file smaller than it was; just seek to the end.
      targetOffset = size;
    }
    if (OSSysSeek(mFD, targetOffset, SEEK_SET, &errorMessage) == -1) {
      reportError(errorMessage.c_str());
    }
  }

  // allocate file buffer (and any other constructor setup)
  if (mFD != -1) {
    mBuffer = new UtFileBuf(cDefaultBufferSize);
  }
  else {
    mBuffer = NULL;
  }
  mNumWrites = 0;

  return (mFD != -1);
}

bool UtOBStream::open(bool firstTime)
{
  INFO_ASSERT(mFD == -1, mFilename.c_str());
  int flags = O_WRONLY | O_BINARY;
  if (firstTime)
  {
    flags |= O_CREAT;
    if (mOpenMode == eOBStreamWriteMode)
      flags |= O_TRUNC;
    else if (mOpenMode == eOBStreamAppendMode)
      flags |= O_APPEND;
  }
  else
  {
    flags |= O_APPEND;
  }
  UtString errmsg;
  mFD = OSSysOpen(mFilename.c_str(), flags, 
                  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
                  &errmsg);
  if (! is_open())
    reportError(errmsg.c_str());
  return UtOBStream::is_open();
}

UtOBStream::~UtOBStream()
{
  if (is_open())
  {
    (void) flush();
    (void) close();
  }
  if (mBuffer != NULL)
    delete mBuffer;
}

bool UtOBStream::is_open() const
{
  return mFD != -1;             // note: UtOCStream overrides
}

bool UtOBStream::close()
{
  INFO_ASSERT(is_open(), mFilename.c_str());
  bool ret = flush();
  UtString errmsg;
  if (OSSysClose(mFD, &errmsg) != 0)
  {
    reportError(errmsg.c_str());
    ret = false;
  }
  mFD = -1;
  return ret;
}

bool UtOBStream::getFD()
{
  return (mFD != -1);
}

bool UtOBStream::flush()
{
  if (!getFD()) {                      // UtCachedFile overrides this
    reportError("File is not open");
    return false;
  }

  UInt32 size;
  char* buf = mBuffer->getBufferAll(&size);

  if (size != 0)
    ++mNumWrites;               // don't count rewrites due to EINTR
  while (size != 0)
  {
    int written = ::write(mFD, buf, size);
    if (written > 0) {
      buf += written;
      INFO_ASSERT(((UInt32) written) <= size, "Consistency check failed.");
      size -= written;
    }
    else if (written < 0)
    {
      // check for signal interruption
      if (errno != EINTR)
      {
        UtString errMsg;
        reportError(OSGetLastErrmsg(&errMsg));
        return false;
      }
    }
  }
  mBuffer->reset();

  return true;
} // bool UtOBStream::flush

bool UtOBStream::writeStr(const char* buf, UInt32 size)
{
  INFO_ASSERT(mBuffer, mFilename.c_str());
  bool ret = true;
  UInt32 written;
  while (ret && ((written = mBuffer->write(buf, size)) < size))
  {
    ret = flush();
    buf += written;
    size -= written;
  }
  return ret;
}

bool UtOBStream::write(const char* s, UInt32 size)
{
  return writeStr(s, size);
}

const char * UtOBStream::getFilename() const
{
  return mFilename.c_str();
}


// common code shared by constructors
void UtOCStream::initialize(UtCachedFileSystem* sys)
{
  if (UtOBStream::is_open())
  {
    mFileCache = sys;
    mFileCache->maybeRelease();   // make way for a new file descriptor
    mFileCache->registerFile(this);
    mFileCache->activate(this);
    mTimestamp = mFileCache->bumpTimestamp();
  }
  else
    mFileCache = NULL;
}

// Cached file stream -- shares file descriptors within a system so
// that there are no OS-imposed limits on effective number of open streams.
UtOCStream::UtOCStream(const char* filename, UtCachedFileSystem* sys,
                       const char* mode, UInt32 bufsiz)
  : UtOBStream(filename, mode, bufsiz)
{
  initialize(sys);
}

UtOCStream::UtOCStream(UtICheckpointStream &in, UtCachedFileSystem* sys)
  : UtOBStream(in)
{
  initialize(sys);
}

UtOCStream::~UtOCStream()
{
  if (is_open())
  {
    (void) close();
    // note -- cannot rely on UtOBStream's destructor because the
    // UtOCstream parts have already been destroyed by the time
    // the base-class destructor is called
  }
}

bool UtOCStream::close()
{
  bool ret = flush();
  mFileCache->unregisterFile(this);
  if (UtOBStream::is_open())
    ret &= UtOBStream::close();
  mFileCache = NULL;
  return ret;
}

bool UtOCStream::is_open() const
{
  return (mFileCache != NULL);
}

bool UtOCStream::getFD()
{
  bool ret = true;
  
  if (mFD == -1)
  {
    INFO_ASSERT(mFileCache, "File cache not initialized.");
    mFileCache->maybeRelease();        // make way for a new file descriptor
    if (open(false))
      mFileCache->activate(this);      // declare we have grabbed an FD
    else
      ret = false;
  }
  mTimestamp = mFileCache->bumpTimestamp();
  return ret;
}

void UtOCStream::releaseFD()
{
  INFO_ASSERT(mFileCache, "File cache not initialized");
  INFO_ASSERT(mFD != -1, "File not open.");
  UtString errmsg;
  if (OSSysClose(mFD, &errmsg) != 0)
    reportError(errmsg.c_str());
  mFD = -1;                     // Note we do not need to flush!
}

bool UtOCStream::hasActiveFD() const
{
  return UtOBStream::is_open();
}

UInt32 UtOCStream::getTimestamp() const
{
  return mTimestamp;
}
