// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtFileEntries.h"

UtFileEntries::Entry::Entry(const char* filePath)
  : mFilename(filePath)
{}

size_t  UtFileEntries::Entry::hash() const
{
  UInt64 device, inode;
  mStatEntry.getDevInodePair(&device, &inode);
  return (size_t) (device + 7*inode);
}
    
//! Equal if device and inode are equal
bool  UtFileEntries::Entry::operator==(const Entry& other) const
{
#if !pfWINDOWS
  UInt64 mydevice, myinode;
  mStatEntry.getDevInodePair(&mydevice, &myinode);
  
  UInt64 odevice, oinode;
  other.mStatEntry.getDevInodePair(&odevice, &oinode);

  bool isEq = mydevice == odevice;
  isEq = isEq && (myinode == oinode);
  return isEq;
#else
  return mFilename == other.mFilename;
#endif
}

//! Sort based on filename
bool  UtFileEntries::Entry::operator<(const Entry& other) const
{
  return mFilename < other.mFilename;
}


UtFileEntries::UtFileEntries()
{
}

UtFileEntries::~UtFileEntries()
{
  for(DevInodeSet::UnsortedLoop p = mDevInodeSet.loopUnsorted(); 
      ! p.atEnd(); ++p)
  {
    Entry* doomed = *p;
    delete doomed;
  }
}


UtFileEntries::Status
UtFileEntries::addFile(const char* filePath, UtString* errMsg)
{
  Status stat = eOK;

  Entry* fileEntry = new Entry(filePath);

  OSStatEntry* statEntry = &(fileEntry->mStatEntry);
  
  if (OSStatFileEntry(filePath, 
                      statEntry, errMsg) != -1)
  {
    std::pair<DevInodeSet::iterator, bool> insertStat = mDevInodeSet.insert(fileEntry);
    if (! insertStat.second)
    {
      delete fileEntry;
      stat = eDup;

      const Entry* foundPair = *(insertStat.first);
      const UtString* name = foundPair->getFilename();
      *errMsg << filePath << " and " << *name << " are the same file.";
    }
  }
  else
  {
    delete fileEntry;
    stat = eStatProb;
    // errMsg already has the error
  }

  return stat;
}

const UtFileEntries::Entry* 
UtFileEntries::getFileEntry(const char* filePath, Status* stat) const
{
  const Entry* ret = NULL;

  *stat = eOK;

  Entry fileEntry(filePath);
  
  OSStatEntry* statEntry = &(fileEntry.mStatEntry);

  UtString errMsg;
  if (OSStatFileEntry(filePath, 
                      statEntry, &errMsg) != -1)
    ret = internalGetEntry(&fileEntry);
  else
    *stat = eStatProb;
  return ret;
}

bool UtFileEntries::removeFile(const char* filePath, Status* stat)
{
  Entry* entry = NULL;
  
  *stat = eOK;

  Entry fileEntry(filePath);
  
  bool ret = false;

  OSStatEntry* statEntry = &(fileEntry.mStatEntry);
  
  UtString errMsg;
  if (OSStatFileEntry(filePath, 
                      statEntry, &errMsg) != -1)
  {
    entry = internalGetEntry(&fileEntry);
    if (entry)
    {
      mDevInodeSet.erase(entry);
      ret = true;
      delete entry;
    }
  }
  else
    *stat = eStatProb;
  return ret;
  
}

const UtFileEntries::Entry* 
UtFileEntries::internalGetEntry(Entry* entry) const
{
  const Entry* ret = NULL;

  DevInodeSet::const_iterator p = mDevInodeSet.find(entry);
  if (p != mDevInodeSet.end())
    ret = *p;
  return ret;
}

UtFileEntries::Entry* 
UtFileEntries::internalGetEntry(Entry* entry)
{
  Entry* ret = NULL;
  
  DevInodeSet::iterator p = mDevInodeSet.find(entry);
  if (p != mDevInodeSet.end())
    ret = *p;
  return ret;
}
