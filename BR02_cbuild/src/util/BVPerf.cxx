// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/BVPerf.h"
#if !pfMSVC
#include <unistd.h>
#endif
#include <cstdio>
#include <cstring>

// Allocate the global variable
BVPerf *BVPC;

// Make sure we've allocated a performance counter for SIZE
// and return it
BVPerfData* BVPerf::check (UInt32 size)
{
  BVStatsMapT::iterator i = mPerfData.find (size);
  BVPerfData * val;

  if (i == mPerfData.end ()) {
    val = new BVPerfData ();
    mPerfData[size] = val;
  } else
    val = (*i).second;

  return val;
}

#define GENCOUNTER(N) \
void BVPerf::count##N (UInt32 size) \
{				    \
  check (size)->m##N++;             \
}

GENCOUNTER (Copy)
GENCOUNTER (Default)
GENCOUNTER (Array)
GENCOUNTER (BVEqRef)
GENCOUNTER (BVEqBVK)
GENCOUNTER (BVRefs)
GENCOUNTER (AnyTo)
GENCOUNTER (AnyFrom)
GENCOUNTER (Int)
GENCOUNTER (RefEqI64)
GENCOUNTER (RefEqI32)
GENCOUNTER (RefEqBV)
GENCOUNTER (RefPlusEq32)
GENCOUNTER (RefMinusEq32)
GENCOUNTER (RefOrEq32)
GENCOUNTER (RefXOrEq32)
GENCOUNTER (RefAndEq32)
GENCOUNTER (RefL)
GENCOUNTER (RefLL)
GENCOUNTER (Ref2EqRefUA)
GENCOUNTER (RefPlusEqRef)
GENCOUNTER (RefMinusEqRef)
GENCOUNTER (RefOrEqRef)
GENCOUNTER (RefXOrEqRef)
GENCOUNTER (RefAndEqRef)


void BVPerf::resetAll (void)
{
  for (BVStatsMapT::iterator i = mPerfData.begin ();
       i != mPerfData.end ();
       ++i)
    std::memset (i->second, 0, sizeof(BVPerfData));
}

void BVPerf::print (void) const
{
  std::fflush (stdout);
  fprintf (stdout, "BVPERF:\nBVPERF:\nBVPERF:\t	BitVector<N> Profiling Statistics\n");
  //                           12      20       29         40           53       62     69          81       90        110    117      126      135      144      153       163     171     179
  fprintf (stdout, "BVPERF:\tSize       BV()    BV(BV&)  BV(int[])  BV(Int64)    BVref    AnyTo  AnyFrom     =I64     =I32      =BV    +=I32    -=I32    |=I32    ^=I32    &=I32     value   llvalue ==unalign    +=Ref    -=Ref    |=Ref    ^=Ref    &=Ref   BV=Ref   BV=BVK\n");
  // 192      201      210      219      228     236      245
  
  for (BVStatsMapT::SortedLoop i = mPerfData.loopSorted ();
       ! i.atEnd ();
       ++i)
    {
      BVPerfData *p = i.getValue();

      std::fprintf (stdout, "BVPERF:\t%4u%11u%11u%11u%11u%9u%9u%9u%9u%9u%9u%9u%9u%9u%9u%9u%9u%9u%10u%9u%9u%9u%9u%9u%9u%9u\n", i.getKey(),
		    p->mDefault,
		    p->mCopy,
		    p->mArray,
                    p->mInt,
		    p->mBVRefs,
		    p->mAnyTo,
                    p->mAnyFrom,
                    p->mRefEqI64,
                    p->mRefEqI32,
                    p->mRefEqBV,
                    p->mRefPlusEq32,
                    p->mRefMinusEq32,
                    p->mRefOrEq32,
                    p->mRefXOrEq32,
                    p->mRefAndEq32,
                    p->mRefL,
                    p->mRefLL,
                    p->mRef2EqRefUA,
                    p->mRefPlusEqRef,
                    p->mRefMinusEqRef,
                    p->mRefOrEqRef,
                    p->mRefXOrEqRef,
                    p->mRefAndEqRef,
                    p->mBVEqRef,
                    p->mBVEqBVK
                    );
      
    }
  
  std::fprintf (stdout, "BVPERF:\nBVPERF:\n");
  std::fflush (stdout);
}

BVPerf::~BVPerf ()
{
  // Dump the statistics
  print ();

  // Now delete all the entries
  for (BVStatsMapT::iterator i = mPerfData.begin ();
       i != mPerfData.end ();
       ++i)
    delete i->second;
}

