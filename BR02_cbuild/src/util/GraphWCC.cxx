// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.
 
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Contains an algorithm for computing weakly-connected components of a graph.
  The algorithm uses a Union-Find data structure to partition the graph nodes
  into equivalence classes, where two nodes are in the same class whenever
  there is an edge between them in the graph.
*/

#include "util/Graph.h"
#include "util/GenericDigraph.h"
#include "util/GraphWCC.h"
#include "util/UtList.h"
#include "util/UtMap.h"
#include "util/UtSet.h"
#include "util/UnionFind.h"

GraphWCC::GraphWCC()
  : mOriginalGraph(NULL), mComponents()
{
}

/*!
 * Compute the weakly connected components of a directed graph.
 */
void GraphWCC::compute(Graph* g)
{
  // reset the result state
  cleanup();

  // traverse the graph
  mOriginalGraph = g;
  walk(g);

  // build the components
  UnionFind<GraphNode*>::EquivalencePartition* partition = mUnionFind.getPartition();
  for (UnionFind<GraphNode*>::EquivalencePartition::iterator pos = partition->begin();
       pos != partition->end();
       ++pos)
  {
    UtSet<GraphNode*> & equivSet = pos->second;
    Component* component = new Component;
    mComponents.push_back(component);
    for (UtSet<GraphNode*>::iterator n = equivSet.begin(); n != equivSet.end(); ++n)
    {
      GraphNode* node = *n;
      component->addNode(node);
    }
  }
  delete partition;
}

//! Every node must be added -- it is its own partition until an incident edge is found
GraphWalker::Command GraphWCC::visitNodeBefore(Graph*, GraphNode* node)
{
  mUnionFind.add(node);

  return GraphWalker::GW_CONTINUE;
}

//! Record the equivalance relation implied by this edge
GraphWalker::Command GraphWCC::visitTreeEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  mUnionFind.equate(from, g->endPointOf(edge));

  return GraphWalker::GW_CONTINUE;
}

//! Record the equivalance relation implied by this edge
GraphWalker::Command GraphWCC::visitBackEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  mUnionFind.equate(from, g->endPointOf(edge));

  return GraphWalker::GW_CONTINUE;
}

//! Record the equivalance relation implied by this edge
GraphWalker::Command GraphWCC::visitCrossEdge(Graph* g, GraphNode* from, GraphEdge* edge)
{
  mUnionFind.equate(from, g->endPointOf(edge));

  return GraphWalker::GW_CONTINUE;
}

/*!
 * Loop over all components of the graph.
 */
GraphWCC::ComponentLoop GraphWCC::loopComponents() const
{
  return ComponentLoop(mComponents);
}

//! Extract the subgraph of nodes in the component
Graph* GraphWCC::extractComponent(Component* component, bool copyScratch) const
{
  return mOriginalGraph->subgraph(component->mNodes, copyScratch);
}

//! release all of the resources being used by a GraphWCC instance
void GraphWCC::cleanup()
{
  for (ComponentLoop loop = loopComponents(); !loop.atEnd(); ++loop)
  {
    Component* comp = *loop;
    delete comp;
  }
  mOriginalGraph = NULL;
  mComponents.clear();
  mUnionFind.clear();
}

GraphWCC::~GraphWCC()
{
  cleanup();
}
