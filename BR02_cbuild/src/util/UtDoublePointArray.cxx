//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtDoublePointArray.h"
#include "util/CarbonAssert.h"
#include <cstring>              // for memcpy

#define INIT_ARRAY_CAPACITY 100

// This fairly specialized array implementation is intended for driving plots
// using the Qwt widget set.  It could also be done easily with 2 UtVector<double>.
// Unfortunately we can't currently link that into the GUI due to exception handling
// issues.

UtDoublePointArray::UtDoublePointArray() {
  mCapacity = 0;
  mXData = NULL;
  mYData = NULL;
  mNumPoints = 0;
}

UtDoublePointArray::~UtDoublePointArray() {
  if (mXData != NULL) {
    CARBON_FREE_VEC(mXData, double, mCapacity);
    CARBON_FREE_VEC(mYData, double, mCapacity);
  }
}

void UtDoublePointArray::push_back(double x, double y) {
  if (mNumPoints == mCapacity) {
    if (mXData != NULL) {
      UInt32 new_capacity = 2*mNumPoints;
      double* px = CARBON_ALLOC_VEC(double, new_capacity);
      double* py = CARBON_ALLOC_VEC(double, new_capacity);
      memcpy(px, mXData, mNumPoints * sizeof(double));
      memcpy(py, mYData, mNumPoints * sizeof(double));
      CARBON_FREE_VEC(mXData, double, mCapacity);
      CARBON_FREE_VEC(mYData, double, mCapacity);
      mXData = px;
      mYData = py;
      mCapacity = new_capacity;
    }
    else {
      mCapacity = INIT_ARRAY_CAPACITY;
      mXData = CARBON_ALLOC_VEC(double, mCapacity);
      mYData = CARBON_ALLOC_VEC(double, mCapacity);
    }
  }

  mXData[mNumPoints] = x;
  mYData[mNumPoints] = y;
  ++mNumPoints;
}

void UtDoublePointArray::clear() {
  mNumPoints = 0;
}

void UtDoublePointArray::unitTest() {
  for (UInt32 i = 0; i < 2*INIT_ARRAY_CAPACITY; ++i) {
    push_back(i, 2*i);
  }
  for (UInt32 i = 0; i < 2*INIT_ARRAY_CAPACITY; ++i) {
    INFO_ASSERT(mXData[i] == i, "x array contents mangled");
    INFO_ASSERT(mYData[i] == 2*i, "y array contents mangled");
  }
}

void UtDoublePointArray::pop_back() {
  INFO_ASSERT(mNumPoints > 0, "pop_back on empty vector");
  --mNumPoints;
}
