// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtUInt64Factory.h"
#include "util/Zstream.h"

static const char* cFactorySig = 0;
static const UInt32 cFactoryVersion = 0;

UtUInt64Factory::UtUInt64Factory()
{
}

UtUInt64Factory::~UtUInt64Factory()
{
  for (NumPtrSet::UnsortedLoop p = mNums.loopUnsorted();
       ! p.atEnd(); ++p)
  {
    UInt64* num = *p;
    CARBON_FREE_VEC(num, UInt64, 1);
  }
}

static UInt64* sCreateVal(UInt64 val)
{
  UInt64* ret = CARBON_ALLOC_VEC(UInt64, 1);
  *ret = val;
  return ret;
}

const UInt64* UtUInt64Factory::getNumPtr(UInt64 val)
{
  const UInt64* ret = find(&val);
  return ret;
}

const CarbonReal* UtUInt64Factory::getRealPtr(CarbonReal val)
{
  union {
    CarbonReal d;
    UInt64 i; } pun;
  pun.d = val;
  UInt64* val64Ptr = &pun.i;
  const UInt64* factoriedValue = find(val64Ptr);
  return (const CarbonReal*) factoriedValue;
}

void UtUInt64Factory::insert(UInt64 val)
{
  (void) getNumPtr(val);
}

const UInt64* UtUInt64Factory::find(UInt64* val)
{
  const UInt64* ret = NULL;
  NumPtrSet::const_iterator p = mNums.find(val);
  if (p != mNums.end())
    ret = *p;
  else
  {
    UInt64* tmp = sCreateVal(*val);
    ret = tmp;
    mNums.insert(tmp);
  }
  return ret;
}

bool UtUInt64Factory::dbWrite(ZostreamDB& out) const
{
  out << cFactorySig;
  out << cFactoryVersion;
  out << size();
  for (UnsortedCLoop p = loopCUnsorted(); ! p.atEnd(); ++p)
  {
    const UInt64* num = *p;
    out << *num;
  }
  return ! out.fail();
}

bool UtUInt64Factory::dbRead(ZistreamDB& in)
{
  UtString signature;
  if (! (in >> signature))
    return false;
  
  if (signature.compare(cFactorySig) != 0)
  {
    UtString buf;
    buf << "Invalid UInt64Factory signature: " << signature;
    in.setError(buf.c_str());
    return false;
  }

  UInt32 version;
  in >> version;
  if (in.fail())
    return false;

  if (version > cFactoryVersion)
  {
    UtString buf;
    buf << "Unsupported UInt64Factory version: " << version;
    in.setError(buf.c_str());
    return false;
  }
  
  UInt32 numElems;
  if (! (in >> numElems))
    return false;

  for (UInt32 i = 0; i < numElems; ++i)
  {
    UInt64 num;
    if (! (in >> num))
      return false;
    
    UInt64* keep = sCreateVal(num);
    mNums.insert(keep);
    INFO_ASSERT(mNums.size() == i + 1, "Consistency check failed.");
  }

  INFO_ASSERT(mNums.size() == numElems, "Consistency check failed.");
  return ! in.fail();
}
