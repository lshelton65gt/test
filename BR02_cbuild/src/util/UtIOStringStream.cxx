// -*-C++-*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtIOStringStream.h"
#include "util/UtCheckpointStream.h"
#include <algorithm>            // std::min


UInt32 UtIOStringStream::readFromFile(char* buf, UInt32 bytes) {
  UInt32 numRead = 0;
  if (!mEOF)
  {
    numRead = std::min(bytes, (UInt32) mOutBuf.size());
    if (numRead <= bytes )
      mEOF = true; // mEOF means that we have extracted all chars from the file
                   // , but some may remain in the buffer.
    memcpy(buf, mOutBuf.c_str(), numRead);
    mOutBuf.erase(0, numRead); 
  }
  return numRead;
}

void UtIOStringStream::insert(const char* buf, UInt32 len)
{
  mEOF = false;
  mOutBuf.append(buf,len);
}

void UtIOStringStream::reset()
{
  UtIStringStream::reset();
  mOutBuf.clear();
}


bool UtIOStringStream::save(UtOCheckpointStream& out)
{
  out << mEOF;
  out << mOutBuf.c_str();

  return !out.fail();
}

UtIOStringStream::UtIOStringStream(UtICheckpointStream& in)
  : mOutput(&mOutBuf)
{
  in >> mEOF;
  in >> mOutBuf;
}
