// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonAssert.h"
#include "util/OSWrapper.h"

// OSWrapper calls that require the socket library on Windows must be in this
// file to avoid having to change every testsuite build to include the windows
// socket library.  

#if pfUNIX
#include <sys/utsname.h>        // uname

void OSGetHostname(UtString* name) {
  struct utsname unamebuf;
  int unameStatus = uname(&unamebuf);
  // on Linux uname returns 0 for success.
  // on Solaris uname returns a non-negative number
  INFO_ASSERT(unameStatus > -1, name->c_str());
  *name = unamebuf.nodename;
}

#elif pfWINDOWS
#include <winsock2.h>

void OSGetHostname(UtString* name) {
  char buf[1000];
  if (gethostname(buf, 1000) == 0) {
    *name = buf;
  }
  else {
    OSGetLastErrmsg(name);
  }
}
#endif

void OSGetTempFileName(UtString* out)
{
  static UInt32 count = 0;
  const int maxTries = 20;
  UtString statResult;

  bool done = false;
  int numTries = 0;
  UInt32 pid = OSGetPid();
  UtString host;
  OSGetHostname(&host);
  do
  {
    out->clear();
    *out << ".carbon.tmp." << host << "." << pid << "." << count++;
    done = (OSStatFile(out->c_str(), "e", &statResult) == 0);
  } while (!done && numTries++ < maxTries);

}
