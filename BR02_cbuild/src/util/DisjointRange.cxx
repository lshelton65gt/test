//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonAssert.h"
#include "util/DisjointRange.h"
#include <algorithm>
#include "util/UtVector.h"
#include "util/UtIOStream.h"

// Note: this is compiled into libgraph.a rather than libutil.a
// because it employs UtSet which means it's hard to link at runtime.
// This should be re-visited as the current implementation is not
// great -- it's slow.  It could be sped up and the UtSet usage
// eliminated by employing compression and projections.  This is
// really just a footnote to myself.


// Identify & Sort all the parts that can be assigned.  We
// can simplify sort/uniq all the numbers in the ConstantRanges
// Generate the minimal set of minimal disjoint constant ranges that
// can be combined to cover any of the ConstantRanges in assignRangeVec.
//
// Let's say start with this (not exactly the example above):
//
//   [2:0]    
//   [9:0]
//   [3:1]
//   [8:3]
//   [9:5]
//   [12:11]
//   
//
// We should generate
//   [12:11]
//   [9:9]
//   [8:5]
//   [4:4]
//   [3:3]
//   [2:1]
//   [0:0]
//
// This is we can construct any member of the first group with
// combinations of the second group:
//
//   [2:0]    == {[2:1],[0:0]}
//   [9:0]    == {[9:9],[8:5],[4:3],[2:1],[1:0]}
//   [3:1]    == {[3:3],[2:1]}
//   [8:3]    == {[8:5],[4,4],[3:3]}
//   [9:5]    == {[9:9],[8:5]}
//   [12:11]  == {[12:11]}
//
// We arrive at this disjoint set incrementally.
//
//  [2:0] --> [2:0]
//
// when we see [9:0] we recognize an overlap with an existing members
// and we break up the existing members acccrding to the overlap.
//

DisjointRange::DisjointRange() :
  mSlotsChanged(true)
{
}

DisjointRange::~DisjointRange() {
  mRequestMap.clearPointerValues();

  // Clearing the ranges should remove all the slots directly
  INFO_ASSERT(mSlotMap.empty(), "slots not cleaned");
}

void DisjointRange::insert(SInt32 rmin, SInt32 rmax) {
  ConstantRange range(rmax, rmin);
  INFO_ASSERT(rmin <= rmax, "empty range");

  Request* request = mRequestMap[range];
  if (request != NULL) {
    // already know about this request
    return;                     
  }
  request = new Request(range);
  mRequestMap[range] = request;

  mSlotsChanged = true;

  insertHelper(rmin, rmax, request, NULL);
}

void DisjointRange::insertHelper(SInt32 rmin, SInt32 rmax, Request* request, Slot* orig_slot) {
  // Prune out empty ranges
  if (rmin > rmax) {
    return;
  }

  ConstantRange range(rmax, rmin);

  UInt32 idx = 0;
  SlotMap::iterator p = mSlotMap.lower_bound(rmin);
  if (! mSlotMap.empty() && (p != mSlotMap.begin()) &&
      ((p == mSlotMap.end()) || (p->first != rmin)))
  {
    --p;
  }
  SlotMap::iterator e = mSlotMap.upper_bound(rmax);

  for (; p != e; ++p) {
    Slot* existing_slot = p->second;
    const ConstantRange& disjoint = existing_slot->getRange();
    if (disjoint == range) {
      ++p;
      INFO_ASSERT(p == e,
                  "if complete overlap, there should be no more overlaps");
      INFO_ASSERT(idx == 0, "complete overlap");
      INFO_ASSERT(orig_slot != existing_slot, "identical slot?");
      if (orig_slot != NULL) {
        existing_slot->copyRequests(orig_slot);
      }
      existing_slot->maybeInsertRequest(request);
      return;
    }
    else if (disjoint.overlaps(range)) {
      mSlotMap.erase(p);
      SInt32 dmin = disjoint.getLsb();
      SInt32 dmax = disjoint.getMsb();

      // Classify what type of overlap this is and assume the
      // maximum breakup.  Note that some of the ranges we are
      // inserting may be empty, but insertRequest will filter those out
      if (disjoint.contains(range)) {
        insertHelper(dmin, rmin - 1, request, existing_slot);
        insertHelper(rmin, rmax, request, existing_slot);
        insertHelper(rmax + 1, dmax, request, existing_slot);
      }
      else if (range.contains(disjoint)) {
        insertHelper(rmin, dmin - 1, request, existing_slot);
        insertHelper(dmin, dmax, request, existing_slot);
        insertHelper(dmax + 1, rmax, request, existing_slot);
      }
      else {
        INFO_ASSERT(dmin != rmin, "expected containment");
        INFO_ASSERT(dmax != rmax, "expected containment");

        if (rmin < dmin) {
          insertHelper(rmin, dmin - 1, request, existing_slot);
          insertHelper(dmin, rmax, request, existing_slot);
          insertHelper(rmax + 1, dmax, request, existing_slot);
        }
        else {
          insertHelper(dmin, rmin - 1, request, existing_slot);
          insertHelper(rmin, dmax, request, existing_slot);
          insertHelper(dmax + 1, rmax, request, existing_slot);
        }
      }
      delete existing_slot;
      return;
    } // if
  } // for

  
  Slot* new_slot = new Slot(range, this);
  INFO_ASSERT(mSlotMap.find(rmin) == mSlotMap.end(), "do not expect overwrite");
  mSlotMap[rmin] = new_slot;
  if (orig_slot != NULL) {
    new_slot->copyRequests(orig_slot);
  }
  new_slot->maybeInsertRequest(request);
} // void DisjointRange::insertHelper

void DisjointRange::Slot::maybeInsertRequest(Request* request) {
  if (request->getRange().overlaps(mRange)) {
    // we expect this range to be contained.  Slots always wholly contain
    // ranges, if they overlap them at all.
    INFO_ASSERT(request->getRange().contains(mRange), "expect containment");
    mRequests.insert(request);
    request->insertSlot(this);
  }
}

void DisjointRange::Slot::eraseRequest(Request* request) {
  mRequests.erase(request);
  if (mRequests.empty()) {
    mDisjointRange->removeSlot(this);
  }
}

void DisjointRange::removeSlot(Slot* slot) {
  mSlotMap.erase(slot->getRange().getLsb());
  delete slot;
}

void DisjointRange::Slot::copyRequests(const Slot* old_slot) {
  for (CLoop<RequestPtrSet> p(old_slot->mRequests); !p.atEnd(); ++p) {
    Request* request = *p;
    maybeInsertRequest(request);
  }
}

void DisjointRange::findSlotIndices() const {
  UInt32 i = 0;
  for (SlotCLoop r = loopSlots(); !r.atEnd(); ++r, ++i) {
    const Slot* slot = r.getValue();
    slot->putIndex(i);
  }
  mSlotsChanged = false;
}

bool DisjointRange::construct(const ConstantRange& range,
                              ConstantRange* indexRequest)
  const
{
  if (mSlotsChanged) {
    findSlotIndices();
  }
  
  RequestMap::const_iterator p = mRequestMap.find(range);
  if (p == mRequestMap.end()) {
    return false;
  }
  Request* request = p->second;

  // Find the min & max indices for all the slots known to this request
  bool first = true;
  for (SlotLoop s(request->loopSlots()); !s.atEnd(); ++s) {
    Slot* slot = *s;
    if (first) {
      first = false;
      indexRequest->setLsb(slot->getIndex());
      indexRequest->setMsb(slot->getIndex());
    }
    else {
      indexRequest->setLsb(std::min(indexRequest->getLsb(), slot->getIndex()));
      indexRequest->setMsb(std::max(indexRequest->getMsb(), slot->getIndex()));
    }
  }
  INFO_ASSERT(!first, "no slots for matching request");
  return true;
} // bool DisjointRange::construct

void DisjointRange::Request::check() {
  for (SlotLoop p(mSlots); !p.atEnd(); ++p) {
    Slot* slot = *p;
    INFO_ASSERT(slot->hasRequest(this), "request->slot does not have request");
    mRange.contains(slot->getRange());
  }
}

void DisjointRange::Slot::check() {
  for (RequestLoop p(mRequests); !p.atEnd(); ++p) {
    Request* request = *p;
    INFO_ASSERT(request->hasSlot(this), "slot->request does not have slot");
    request->getRange().contains(mRange);
    request->check();
  }
}

void DisjointRange::check() {
  UInt32 i = 0;
  Slot* prev = NULL;
  for (SlotMapLoop r = loopSlots(); !r.atEnd(); ++r, ++i) {
    Slot* slot = r.getValue();
    INFO_ASSERT(slot->getRange().getLsb() == r.getKey(), "slot key insanity");
    if (prev != NULL) {
      INFO_ASSERT(slot->getRange().getLsb() > prev->getRange().getLsb(),
                  "slot order insanity");
    }
    slot->check();
    prev = slot;
  }
}

void DisjointRange::print() const {
  UInt32 i = 0;
  for (SlotCLoop r = loopSlots(); !r.atEnd(); ++r, ++i) {
    const Slot* slot = r.getValue();
    const ConstantRange& range = slot->getRange();
    SInt32 key = r.getKey();;
    UtIO::cout() << "Slot " << i << " (" << key << ") ";
    range.print();
    UtIO::cout() << "\n";
  }
  UtIO::cout().flush();
}

void DisjointRange::erase(const ConstantRange& range) {
  RequestMap::iterator p = mRequestMap.find(range);
  if (p != mRequestMap.end()) {
    Request* request = p->second;
    delete request;
    mRequestMap.erase(p);
    // Merge identical adjacent slots?
  }
}

bool DisjointRange::hasRequest(const ConstantRange& range) const {
  RequestMap::const_iterator p = mRequestMap.find(range);
  return p != mRequestMap.end();
}

DisjointRange::OverlapLoop::OverlapLoop(DisjointRange* dr,
                                        const ConstantRange& overlap_range)
{
  SInt32 rmin = overlap_range.getLsb();

  // Requests can occur in multiple slots.  Iterate over them only once though.
  RequestPtrSet covered;

  SlotMap::iterator p = dr->mSlotMap.lower_bound(rmin);
  if (! dr->mSlotMap.empty() && (p != dr->mSlotMap.begin()) &&
      ((p == dr->mSlotMap.end()) || (p->first != rmin)))
  {
    --p;
  }
  SlotMap::iterator e = dr->mSlotMap.upper_bound(overlap_range.getMsb());
  for (; p != e; ++p) {
    Slot* slot = p->second;
    const ConstantRange& disjoint = slot->getRange();
    if (disjoint.overlaps(overlap_range)) {
      for (RequestLoop q(slot->loopRequests()); !q.atEnd(); ++q) {
        Request* request = *q;
        if (covered.insertWithCheck(request)) {
          mPending.push_back(request);
        }
      }
    }
  }
  mRequestIter = mPending.begin();
} // DisjointRange::OverlapLoop::OverlapLoop

DisjointRange::Request* DisjointRange::findRequest(const ConstantRange& range)
  const
{
  RequestMap::const_iterator p = mRequestMap.find(range);
  if (p == mRequestMap.end()) {
    return NULL;
  }
  return p->second;
}
