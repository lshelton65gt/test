/*****************************************************************************

 Copyright (c) 2003-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/carbon_hashtable_itr.h"
#include "carbon_hashtable_priv.h"
#include "carbon/c_memmanager.h"
#include <stdlib.h> /* defines NULL */

extern const struct carbonHashTableSizes carbonPrimes[];

struct carbon_hashtable_itr *
carbon_hashtable_iterator(struct carbon_hashtable *h)
{
  struct carbon_hashtable_itr *itr = (struct carbon_hashtable_itr *)
    carbonmem_alloc(sizeof(struct carbon_hashtable_itr));
  if (NULL == itr) return NULL;
  carbon_hashtable_iterator_init(itr, h);
  return itr;
}

void carbon_hashtable_iterator_nullinit(struct carbon_hashtable_itr* i) {
  i->e = 0;
  i->prev = 0;
  i->mNoAdvance = 0;
  
  i->h = 0;
  i->index = 0;
}

void carbon_hashtable_iterator_init(struct carbon_hashtable_itr* itr, 
				    struct carbon_hashtable* h)
{
  UInt32 i, tablelength;
  UInt32 idx = h->sizes.primeIndex;
  carbon_hashEntryPtr* table;

  itr->h = h;
  itr->e = NULL;
  itr->prev = NULL;
  itr->mNoAdvance = 0;
  tablelength = carbonPrimes[idx].numBuckets;
  itr->index = tablelength;
  if (0 == h->sizes.entrycount) return;
  
  table = HASH_GET_TABLE(h);
  for (i = 0; i < tablelength; ++i, ++table)
  {
    carbon_hashEntry* e = HASH_EXPAND_ENTRY(*table);
    if (0/*NULL*/ != e)
    {
      itr->e = e;
      itr->index = i;
      break;
    }
  }
}

void carbon_hashtable_iterator_destroy(struct carbon_hashtable_itr *i)
{
  if (i)
    carbonmem_dealloc(i, sizeof(struct carbon_hashtable_itr));
}

void carbon_hashtable_iterator_remove_current(struct carbon_hashtable_itr *i,
                                              unsigned int entrySize)
{
  carbon_hashEntry* tmp;
  carbon_hashEntry* doomed;

  if (i->e != NULL)
  {
    if (i->prev == NULL)
    {
      carbon_hashEntryPtr* pE = HASH_GET_TABLE(i->h) + i->index;
      *pE = i->e->next;
    }
    else
      i->prev->next = i->e->next;
    
    tmp = HASH_EXPAND_ENTRY(i->e->next);
    doomed = i->e;

    /*
     * What state should we leave the iterator in after a 'remove'?  99% of
     * the time no one cares -- an iterator is created via find() in order
     * to remove an element, and then it is discarded.  And if the hash-table
     * is sparsely populated, calling carbon_hashtable_iterator_advance from
     * here, as we used to do, is very slow.
     *
     * Instead, we just need to leave the iterator in a state so that the
     * next call to advance() will do the right thing.  This is dependent
     * on both i->mNoAdvance and i->e.  If i->e is null then advance()
     * will search further through the buckets to find a non-empty bucket.
     */

    i->e = tmp;
    i->mNoAdvance = 1;

    carbonmem_dealloc(doomed, entrySize);
    i->h->sizes.entrycount--;
  }
}

/*****************************************************************************/
/* advance - advance the iterator to the next element
 *           returns zero if advanced to end of table */

int
carbon_hashtable_iterator_advance(struct carbon_hashtable_itr *itr)
{
  UInt32 j,tablelength;
  SMALLPTR(carbon_hashEntry*) *table;
  SMALLPTR(carbon_hashEntry*)/*struct carbon_mapEntry **/next = 0;

  if (!itr->mNoAdvance) {
    if (NULL == itr->e) 
      return 0; /* stupidity check */
    next = itr->e->next;
    itr->prev = itr->e;
    if (0/*NULL*/ != next)
    {
      itr->e = HASH_EXPAND_ENTRY(next);
      return -1;
    }
  }
  else {
    itr->mNoAdvance = 0;
    if (itr->e != NULL) {
      return -1;
    }
  }

  tablelength = carbonPrimes[itr->h->sizes.primeIndex].numBuckets;
  j = ++itr->index;
  if ((j >= tablelength) || (itr->h->sizes.primeIndex == 0)) {
    itr->e = NULL;
    return 0;
  }

  itr->prev = NULL;
  table = itr->h->data.table;
  while (0/*NULL*/ == (next = table[j]))
  {
    if (++j >= tablelength)
    {
      itr->index = tablelength;
      itr->e = NULL;
      return 0;
    }
  }
  itr->index = j;
  itr->e = HASH_EXPAND_ENTRY(next);
  return -1;
}

void
carbon_hashtable_iterator_simple_advance(struct carbon_hashtable_itr *itr)
{
  SMALLPTR(carbon_hashEntry*)/*struct carbon_mapEntry **/next = itr->e->next;
  if (0/*NULL*/ != next)
  {
    itr->e = HASH_EXPAND_ENTRY(next);
  }
  else {
    UInt32 tablelength = carbonPrimes[itr->h->sizes.primeIndex].numBuckets;
    UInt32 j = ++itr->index;

    if ((j >= tablelength) || (itr->h->sizes.primeIndex == 0)) {
      itr->e = NULL;
    }
    else {
      SMALLPTR(carbon_hashEntry*) *table = itr->h->data.table;

      while (0/*NULL*/ == (next = table[j]))
      {
        if (++j >= tablelength)
        {
          itr->index = tablelength;
          itr->e = NULL;
          return;
        }
      }
      itr->index = j;
      itr->e = HASH_EXPAND_ENTRY(next);
    }
  }
}

/*
 * Copyright (C) 2002 Christopher Clark <firstname.lastname@cl.cam.ac.uk>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * */
