// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include <stdio.h>
#include "util/UtApplication.h"
#include "util/ShellMsgContext.h"
#include "util/Stats.h"


UtApplication::UtApplication(int *argc, char** argv) : mArgc(argc), mArgv(argv)
{
  mErrorStream = new MsgStreamIO(stderr, true);
  mMsgContext = new MsgContext;
  mMsgContext->addReportStream(mErrorStream);

  // fatally exit if not set
  if (getenv("CARBON_HOME") == NULL)
    mMsgContext->SHLCarbonHomeUnset();

  setInstallDir(); 
}


UtApplication::~UtApplication()
{
  delete mMsgContext;
  delete mErrorStream;
}


void UtApplication::setInstallDir(void)
{
  const char* carbon_home = getenv("CARBON_HOME");
  INFO_ASSERT(carbon_home, "CARBON_HOME must be set.");
  mInstallDir = carbon_home;
}


const UtString & UtApplication::getInstallDir(void) const
{
  return mInstallDir;
}

ArgProc* UtApplication::getArgProc()
{
  return &mArgs;
}
