// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Class to compute node depth using DFS walk
*/

#include "util/Graph.h"
#include "util/GraphDepthWalker.h"

GraphWalker::Command
UtGraphDepthWalker::visitNodeBefore(Graph* graph, GraphNode* node)
{
  putDepth(graph, node, 0);
  updateWalker(0);
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDepthWalker::visitNodeAfter(Graph* graph, GraphNode* node)
{
  // Update our depth from the last child
  SIntPtr depth = updateDepth(graph, node, mDepth);
  updateWalker(depth);
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDepthWalker::visitNodeBetween(Graph* graph, GraphNode* node)
{
  // Update our depth from the last child and reset the walker
  updateDepth(graph, node, mDepth);
  updateWalker(0);
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDepthWalker::visitCrossEdge(Graph* graph, GraphNode*, GraphEdge* edge)
{
  // Update the walker from this child that was previously visited
  GraphNode* to = graph->endPointOf(edge);
  updateWalker(getDepth(graph, to));
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDepthWalker::visitBackEdge(Graph*, GraphNode*, GraphEdge*)
{
  INFO_ASSERT(0, "Found a cycle while computing depth!");
  return GW_SKIP;
}

void UtGraphDepthWalker::putDepth(Graph* graph, GraphNode* node, SIntPtr depth)
{
  graph->setScratch(node, depth);
}

SIntPtr UtGraphDepthWalker::getDepth(Graph* graph, GraphNode* node) const
{
  return graph->getScratch(node);
}

SIntPtr
UtGraphDepthWalker::updateDepth(Graph* graph, GraphNode* node, SIntPtr faninDepth)
{
  // Compute the fanin depth
  SIntPtr newDepth = faninDepth + 1;

  // If the depth increased, update it. Return the max of the depths
  SIntPtr depth = getDepth(graph, node);
  if (newDepth > depth) {
    putDepth(graph, node, newDepth);
  } else {
    newDepth = depth;
  }
  return newDepth;
}
