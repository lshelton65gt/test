// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include <cctype>

#include "util/UtConv.h"
#include "util/DynBitVector.h"
#include "util/UtStack.h"
#include "util/UtVector.h"
#include "util/UtIOStream.h"
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/OSWrapper.h"

/*!
  \file
  Implementations of general conversion routines
*/

struct UtConvRangeInfo;
static void sGetRangeInfo(UtConvRangeInfo* info, size_t dstindex, size_t length);

static inline int sCalcNumHexCharsNeeded(size_t numBits)
{
  return (numBits + 3)/4;
}

static inline int sCalcNumOctCharsNeeded(size_t numBits)
{
  return (numBits + 2)/3;
}

static const UInt64 scParityMasks[] = {0x00000005, 0x00000050, 0x00000500, 0x00005000, 
                                       0x00050000, 0x00500000, 0x05000000, 0x50000000,
                                       KUInt64(0x0000000500000000),
                                       KUInt64(0x0000005000000000),
                                       KUInt64(0x0000050000000000),
                                       KUInt64(0x0000500000000000), 
                                       KUInt64(0x0005000000000000),
                                       KUInt64(0x0050000000000000),
                                       KUInt64(0x0500000000000000),
                                       KUInt64(0x5000000000000000)};
static const UInt64 scBCDAddMasks[] = {0x00000003, 0x00000030, 0x00000300, 0x00003000, 
                                       0x00030000, 0x00300000, 0x03000000, 0x30000000,
                                       KUInt64(0x0000000300000000),
                                       KUInt64(0x0000003000000000),
                                       KUInt64(0x0000030000000000),
                                       KUInt64(0x0000300000000000), 
                                       KUInt64(0x0003000000000000),
                                       KUInt64(0x0030000000000000),
                                       KUInt64(0x0300000000000000),
                                       KUInt64(0x3000000000000000)};

static const UInt64 scOneMasks[]    = {0x0000000f, 0x000000f0, 0x00000f00, 0x0000f000, 
                                       0x000f0000, 0x00f00000, 0x0f000000, 0xf0000000,
                                       KUInt64(0x0000000f00000000),
                                       KUInt64(0x000000f000000000),
                                       KUInt64(0x00000f0000000000),
                                       KUInt64(0x0000f00000000000), 
                                       KUInt64(0x000f000000000000),
                                       KUInt64(0x00f0000000000000),
                                       KUInt64(0x0f00000000000000),
                                       KUInt64(0xf000000000000000)};
static const UInt64 scSubOrMasks[]  = {0x00000008, 0x00000080, 0x00000800, 0x00008000, 
                                       0x00080000, 0x00800000, 0x08000000, 0x80000000,
                                       KUInt64(0x0000000800000000),
                                       KUInt64(0x0000008000000000),
                                       KUInt64(0x0000080000000000),
                                       KUInt64(0x0000800000000000), 
                                       KUInt64(0x0008000000000000),
                                       KUInt64(0x0080000000000000),
                                       KUInt64(0x0800000000000000),
                                       KUInt64(0x8000000000000000)};
static const size_t scNumNibbles = sizeof(scParityMasks)/sizeof(UInt64);
static const size_t scSizeRep = sizeof(UInt64);

static const size_t scUInt32Size = sizeof(UInt32);
static const UInt32 scWordBitSize = scUInt32Size * 8;


struct UtConvRangeInfo
{
  //! Number of words in the range
  size_t mNumRangeWords;
  //! First word index
  size_t mStartWordIndex;
  //! Last word index
  size_t mLastWordIndex;
  //! Number of indices
  size_t mNumIndices;
  //! Current word offset shift
  size_t mFrontOffset;
  //! Previous word offset shift
  size_t mBackOffset;
  //! End of value mask
  UInt32 mEValMask;
  //! Beginning of value mask
  UInt32 mBValMask;

  bool mIsInclusiveWord;
};
  
static void sGetRangeInfo(UtConvRangeInfo* info, size_t dstindex, size_t length)
{
  info->mNumRangeWords = (length + 31)/32;

  size_t vectorWordIndex = dstindex/scWordBitSize;
  info->mStartWordIndex = vectorWordIndex;
  
  info->mLastWordIndex = (dstindex + length - 1)/scWordBitSize;
  info->mNumIndices = info->mLastWordIndex - vectorWordIndex;
  info->mFrontOffset = dstindex % scWordBitSize;
  info->mBackOffset = scWordBitSize - info->mFrontOffset;

  // Is the range within 1 word of the value?
  info->mIsInclusiveWord = false;
  size_t tmpChk = (info->mFrontOffset + length) % scWordBitSize;
  if (tmpChk == 0)
    tmpChk = scWordBitSize;
  info->mIsInclusiveWord = (tmpChk == (length + info->mFrontOffset));

  // calculate the first value word mask
  UInt32 bValMask = 0;
  if (info->mFrontOffset != 0)
  {
    bValMask = ~bValMask;
    bValMask <<= info->mFrontOffset;
    bValMask = ~bValMask;
  }
  info->mBValMask = bValMask;

  // calculate the last value word mask
  const size_t lastShiftOffset = 
    (dstindex + length) % scWordBitSize;
  
  UInt32 eValMask = 0;
  if (lastShiftOffset != 0)
  {
    eValMask = ~eValMask;
    eValMask <<= lastShiftOffset;
  }
  info->mEValMask = eValMask;
}


static size_t sGetNumWords(size_t width)
{
  return (width + (scWordBitSize - 1))/scWordBitSize;
}

template <typename Prim> Prim getPrimVal(Prim unmasked, size_t bitwidth) {
  Prim mask = 0;
  mask = ~mask;
  for (size_t i = 0; i < bitwidth; ++i)
    mask <<= 1;
  mask = ~mask;
  return (unmasked & mask);
}

//! returns the position of the most significant one (the lsb has a position of 1)
/*! this can handle UInt8/16/32/64 and UInt32*
 * if all bits within the \a bitwidth range are zero then zero is returned.
 *
 * \param val pointer to value array
 * \param bitwidth number of bits in \a val that are to be examined.
 *
 * This function is undefined if \a val was not defined to have at
 * least \a bitwidth bits.
 */
template <typename T> size_t findMostSignificantBitPosition(const T* val, size_t bitwidth)
{
  size_t bitPosition = bitwidth;

  const size_t sizeofTInBits = (sizeof(T) * 8);

  const T fullBitMask = (T)((T)1 << (sizeofTInBits - 1));

  // create the necessary masks to handle the array element that contains
  // the MSB
  T msbElementMask = T(~0);
  T msbElementBitMask = 1;
  {
    UInt32 rem = bitwidth % sizeofTInBits;
    if (rem != 0) {
      msbElementMask <<= rem;
      msbElementMask = ~msbElementMask;
      msbElementBitMask <<= (rem - 1);
    }
    else {
      msbElementBitMask = fullBitMask;
    }
  }
    
  T cur;
  T bitMask;

  // work from the last array element (msb) to the first array element (lsb)
  bool msbElement = true;
  int currentElemIndex = ((bitwidth + sizeofTInBits - 1)/sizeofTInBits) -1;

  while (currentElemIndex >= 0) 
  {
    if ( msbElement ){
      // first array element may be partial
      cur = val[currentElemIndex] & msbElementMask;
      bitMask = msbElementBitMask;
      msbElement = false;
    } else {
      // in all other array elements process the whole T
      cur = val[currentElemIndex];
      bitMask = fullBitMask;
    }
    
    if (cur == 0) {
      // keep looking
      --currentElemIndex;
      bitPosition -= sizeofTInBits;
    }
    else {
      // there is at least one bit set in this array element, 
      // identify its position and we are done
      while ( (cur & bitMask) == 0 ) {
        bitMask >>= 1;
        --bitPosition;
      }
      break;
    }
  }

  return bitPosition;
}


class BCDElem
{
public: CARBONMEM_OVERRIDES
  BCDElem() : mPackedBCD(0), mNext(NULL) {}
  ~BCDElem() {}
  
  inline void bcdcheck(size_t i) {
    if ((mPackedBCD & scOneMasks[i]) >= scParityMasks[i]) 
      mPackedBCD += scBCDAddMasks[i];
  }
  
  inline void correct()
  {
    // Much better speed if we unroll this loop
    /*
    for (size_t i = 0; i < scNumNibbles; ++i) 
      bcdcheck(i);
    */
    // Unfortunately, no checking allowed. So, if we change the
    // representation to be UInt32s instead of UInt64s we need to
    // change this function. BUT IT IS WORTH IT. This is twice as
    // fast.
    bcdcheck(0);
    bcdcheck(1);
    bcdcheck(2);
    bcdcheck(3);
    bcdcheck(4);
    bcdcheck(5);
    bcdcheck(6);
    bcdcheck(7);
    bcdcheck(8);
    bcdcheck(9);
    bcdcheck(10);
    bcdcheck(11);
    bcdcheck(12);
    bcdcheck(13);
    bcdcheck(14);
    bcdcheck(15);
  }

  inline UInt32 shift(UInt32 in) {
    correct();
    register UInt32 out = 0x0;
    if ((mPackedBCD & scSubOrMasks[scNumNibbles - 1]) != 0)
      out = 0x1;
    mPackedBCD <<= 1;
    mPackedBCD |= in;
    if (mNext)
      out = mNext->shift(out);
    return out;
  }
  
  inline void putNext(BCDElem* next) {
    mNext = next;
  }

  inline void setFirst() {
    mPackedBCD |= 0x1;
  }
  
  UInt64 getVal() const {
    return mPackedBCD;
  }

  bool getStr(bool pad, char** result, size_t* len)
    const
  {
    bool isGood = true;
    UInt64 valCp = mPackedBCD;
    const UInt64 mask = 0xf;
    if (pad)
    {
      if (*len < 17) // +1 for terminator
        return false;
      for (int i = 0; i < 16; ++i, valCp >>= 4)
      {
        UInt8 digit = (UInt8)(valCp & mask);
        (*result)[15 - i] = (char) ('0' + digit);
      }
      *result += 16;
      *len -= 16;
    }
    else
    {
      UInt32 shift = 64 - 4;
      bool foundFirstOne = false;
      for (int i = 15; (i >= 0) && isGood; --i, shift -= 4)
      {
        UInt8 digit = (UInt8)((valCp >> shift) & mask);
        if (foundFirstOne || (digit > 0))
        {
          foundFirstOne = true;
          if (*len > 1) // +1 for terminator
          {
            --(*len);
            (*result)[0] = (char) ('0' + digit);
            ++(*result);
          }
          else
            isGood = false;

        }
      }
    }
    return isGood;
  }
  
  static inline size_t getNumBytesOfRep() {
    return sizeof(UInt64);
  }
  
private:
  UInt64 mPackedBCD;
  BCDElem* mNext;
};

class BCDConv
{
public: CARBONMEM_OVERRIDES
  BCDConv(size_t numBitsBinary, const UInt32* binaryArray) {
    mBinArray = binaryArray;
    mCurIndex = 0;
    mNumBits = numBitsBinary;
    mNumWords = (numBitsBinary + 31)/32;
    SInt32 tmpNum = mNumWords;
    for (SInt32 max = mNumWords; (max > 0) && (binaryArray[max - 1] == 0); --max)
      --tmpNum;
    
    if (tmpNum != mNumWords) {
      mNumWords = tmpNum;
      mNumBits = mNumWords * (sizeof(UInt32) * 8);
    }

    // The absolute worst case is that we insert a bit into a nibble
    // every 3 full shifts. Therefore, the max number of nibbles we
    // would need is (n + n/3)/4 or n/3, where n = mNumBits, rounded
    // up of course. Therefore, the max number of BCD Elems we will
    // need is the max number of bits divided by the size of the BCD
    // Elem packed representation, rounded up.
    size_t numBcdElems = (mNumBits + scSizeRep - 1)/scSizeRep;
    mBCDElems.resize(numBcdElems);
  }
  
  ~BCDConv() {}

  void encode()
  {
    register UInt32 readmask = 1;
    int i;
    size_t intSize = sizeof(UInt32) * 8;
    int firstSize = mNumBits % intSize;
    if (firstSize == 0)
      firstSize = intSize;
    for (i = 1; i < firstSize; ++i)
      readmask <<= 1;
    
    register UInt32 inMask = 0;
    register UInt32 outMask = 0;
    for (i = mNumWords - 1; i >= 0; --i) {
      for (; readmask != 0; readmask >>= 1) {
        inMask = ((mBinArray[i] & readmask) != 0) ? 0x1 : 0x0;
        outMask = mBCDElems[0].shift(inMask);
        if (outMask != 0) {
          size_t curIndex = mCurIndex;
          ++mCurIndex;
          BCDElem& elem = mBCDElems[mCurIndex];
          elem.setFirst();
          mBCDElems[curIndex].putNext(&elem);
        }
      }
      readmask = UInt32(scSubOrMasks[7]);
    }
  }
  
  void print() const
  {
    size_t len = (mNumBits + 2)/3;
    char* str = CARBON_ALLOC_VEC(char, len);
    asciify(str, len);
    fwrite(str, strlen(str), 1, stdout);
    fputc('\n', stdout);
    fflush(stdout);
    CARBON_FREE_VEC(str, char, len);
  }
  
  bool asciify(char* result, size_t len) const
  {
    bool isGood = true;
    if (mBCDElems.empty())
    {
      if (len > 1)
      {
        result[0] = '0';
        result[1] = '\0';
      }
      else
        isGood = false;
    }
    else {
      isGood = mBCDElems[mCurIndex].getStr(false, &result, &len);

      for (int i = mCurIndex - 1; (i >= 0) && isGood; --i)
        isGood = mBCDElems[i].getStr(true, &result, &len);
      if (isGood)
        *result = '\0';
    }
    return isGood;
  }
  
private:
  size_t mCurIndex;
  size_t mNumBits;
  SInt32 mNumWords;
  const UInt32* mBinArray;
  UtVector<BCDElem> mBCDElems;
};

static const char scNumberToHex[] = "0123456789abcdef";
static const char scNumberToHEX[] = "0123456789ABCDEF";

//! Template class for formatting values
/*!
  This handles all the different primitive types. This will not
  handle bitvectors, but there is a way to get the array of longs
  from the bitvectors in order to format.
  This class does NOT handle x's or z's.
*/
template<typename Prim> 
class PrimConvBin
{
private:
  const Prim* mVal;
    
public: CARBONMEM_OVERRIDES
  //! constructor
  PrimConvBin(const Prim* val) : mVal(val) {}
    
  //! write bin value into string
  /*!
    \param buffer pointer to buffer that will hold the result
    \param bufferSize      length of \a buffer (in chars)
    \param numBitsToEncode  number of bits to be converted.
     If the value in mVal contains more than \a numBitsToEncode
     then only the least significant numBitsToEncode are encoded.
     If mVal was defined to have fewer than numBitsToEncode then
     this function is undefined.

   \return -1 if encoding could not be completed, otherwise the number
    of characters placed in \a buffer (not including trailing null) are
    returned. 
  */
  int writeBin(char* buffer, size_t bufferSize, size_t numBitsToEncode) {
    size_t numElems = getNumElems(numBitsToEncode);
    int primSizeBits = sizeof(Prim) * 8;
    if (bufferSize < numBitsToEncode + 1)
      return -1;
    int remainder = (numBitsToEncode % primSizeBits);
    size_t elemOffset = numElems;
    if ((numBitsToEncode > 0) && (remainder == 0))
      // we are aligned
      ++elemOffset;
      
    int index = ((elemOffset - 1) * primSizeBits) + remainder - 1;
    buffer[index + 1] = '\0';
    for (size_t j = 0; j < numElems; ++j)
    {
      Prim mask = 1;
      for (int i = 0; (i < primSizeBits) && (index >= 0); ++i)
      {
        buffer[index] = ((mVal[j] & mask) != 0) ? '1' : '0';
        --index;
        mask <<= 1;
      }
    }

    return (int) numBitsToEncode;
  } // void writeBin
    
  //! write hex value into string
  /*!
    \param buffer pointer to buffer that will hold the result
    \param bufferSize      length of \a buffer (in chars)
    \param numBitsToEncode  number of bits to be converted.
     If the value in mVal contains more than \a numBitsToEncode
     then only the least significant numBitsToEncode are encoded.
     If mVal was defined to have fewer than numBitsToEncode then
     this function is undefined.
    \param upperCase if true then use 'ABCDEF' instead of 'abcdef'

    \return -1 if encoding could not be completed, otherwise the number
     of characters placed in \a buffer (not including trailing null) are
     returned. 
  */
  int writeHex(char* buffer, size_t bufferSize, size_t numBitsToEncode, bool upperCase) {
    size_t numElems = getNumElems(numBitsToEncode);
    int retVal = sCalcNumHexCharsNeeded(numBitsToEncode);
    int insertIndex = retVal;
 
    const char* alphaSet = scNumberToHex;
    if (upperCase)
      alphaSet = scNumberToHEX;
    
    if (bufferSize < (unsigned) insertIndex + 1)
      return -1;
    if (numElems != 0) {
      buffer[insertIndex] = '\0';
      --insertIndex;

      Prim lastMask = Prim(~0);
      {
        UInt32 numlastBits = numBitsToEncode % (sizeof(Prim) * 8);
        if (numlastBits != 0)
        {
          lastMask <<= numlastBits;
          lastMask = ~lastMask;
        }
      }

      const Prim hexbitMask = 0xf;
      for (size_t i = 0; i < numElems; ++i)
      {
        Prim cur = mVal[i];
        
        if (i == numElems - 1)
          cur &= lastMask;

        if (cur == 0)
        {
          for (size_t j = 0; (j < sizeof(Prim) * 2) && (insertIndex >= 0); ++j, --insertIndex)
            buffer[insertIndex] = '0';
        }
        else if (cur == Prim(~0))
        {
          for (size_t j = 0; (j < sizeof(Prim) * 2)  && (insertIndex >= 0); ++j, --insertIndex)
            buffer[insertIndex] = alphaSet[15];
        }
        else
        {
          for (size_t j = 0; (j < sizeof(Prim) * 2) && (insertIndex >= 0); ++j, cur >>= 4, --insertIndex)
          {
            UInt32 hexdigit = (UInt32)(cur & hexbitMask);
            buffer[insertIndex] = alphaSet[hexdigit];
          }
        }
      }
    } // if
    
    return retVal;
  } // bool writeHex
  
  //! write oct value into string
  /*!
    \param buffer pointer to buffer that will hold the result
    \param bufferSize      length of \a buffer (in chars)
    \param numBitsToEncode  number of bits to be converted.
     If the value in mVal contains more than \a numBitsToEncode
     then only the least significant numBitsToEncode are encoded.
     If mVal was defined to have fewer than numBitsToEncode then
     this function is undefined.

     \return -1 if encoding could not be completed, otherwise the number
     of characters placed in \a buffer (not including trailing null) are
     returned. 
  */
  int writeOct(char* buffer, size_t bufferSize, size_t numBitsToEncode) {
    size_t numElems = getNumElems(numBitsToEncode);
    int retVal = sCalcNumOctCharsNeeded(numBitsToEncode);
    int insertIndex = retVal;
    
    if (bufferSize < (unsigned) insertIndex + 1)
      return -1;
    if (numElems != 0) {
      buffer[insertIndex] = '\0';
      --insertIndex;
      
      const Prim octbitMask = 0x7;

      // for a byte this will be 2, short 1, etc. on the first time
      // through the for loop. After that, it changes as the shift by
      // 3 morphs the value.
      UInt32 headShift = 0;

      Prim lastMask = Prim(~0);
      {
        UInt32 numlastBits = numBitsToEncode % (sizeof(Prim) * 8);
        if (numlastBits != 0)
        {
          lastMask <<= numlastBits;
          lastMask = ~lastMask;
        }
      }
      
      // We start with no leftover
      Prim cur = mVal[0];
      
      // mask off unused bits
      if (numElems == 1)
        cur &= lastMask;

      for (size_t i = 0; i < numElems; ++i)
      {
        // recalculate the head shift
        headShift = ((sizeof(Prim) * 8) + headShift) % 3;
        
        if (cur == 0)
        {
          // special case all 0's
          for (size_t j = 0; j < (sizeof(Prim) * 8)/3 && (insertIndex >= 0); ++j, --insertIndex)
            buffer[insertIndex] = '0';
        }

        // g++ didn't like ~Prim(0), which seems correct
        else if (cur == Prim(~0))
        {
          // special case all 1's
          for (size_t j = 0; j < (sizeof(Prim) * 8)/3  && (insertIndex >= 0); ++j, --insertIndex)
            buffer[insertIndex] = '7';
        }
         
       else
        {
          for (size_t j = 0; j < (sizeof(Prim) * 8)/3 && (insertIndex >= 0); ++j, cur >>= 3, --insertIndex)
          {
            UInt32 octdigit = (UInt32) (cur & octbitMask);
            buffer[insertIndex] = (char) ('0' + octdigit);
          }
        }

        // get the next value and combine it with the leftover and
        // calculate. Be careful to mask off unused bits.
        if (insertIndex >= 0)
        {
          cur = mVal[i];
          Prim next = 0;
          if (i < numElems - 1)
          {
            next = mVal[i + 1];
            if (i == numElems - 2)
              next &= lastMask;
          }
          else
            cur &= lastMask;

          if (headShift != 0)
          {
            // base 8 correction.

            cur >>= (sizeof(Prim) * 8 - headShift);
            cur |= (next << headShift);
          
            UInt32 octdigit = (UInt32)(cur & octbitMask);
            buffer[insertIndex--] = (char) ('0' + octdigit);
            cur = next >> (3 - headShift);
          }
          else 
            // no correction needed. Ended conversion at the end of
            // the primitive element.
            cur = next;
        }
      }

      // don't have to worry about insertIndex exceeding the number
      // of words available. At this point we either need to add one
      // digit or return
      if (insertIndex == 0)
      {
        UInt32 octdigit = (UInt32) (cur & octbitMask);
        buffer[insertIndex--] = (char) ('0' + octdigit);
      }
    }
    return retVal;
  } // bool writeOct
  
    
private:
  size_t getNumElems(size_t numBits)
  {
    size_t primSize = sizeof(Prim) * 8;
    return (numBits + primSize - 1)/primSize;
  }
};

//! Convert an array of primitives to a null terminated binary/octal/hex encoded string
/*!
  \param val pointer to the value to be encoded
  \param numBits number of bits in val to be encoded
  \param result pointer to buffer where result is placed
  \param len is the size of \a result buffer
         (be sure to leave space for trailing null)
  \param radix selects encoding format (only Bin/Oct/Hex are supported)
  \param upperCase if true then the characters 'ABCDEF' are used
  instead of 'abcdef' when encoding hex
  \param radix selects the type of encoding.
*/
template<typename Prim> 
int PrimArrToRawBinStr(const Prim* val, size_t numBits, 
                       char* result, size_t len, CarbonRadix radix, bool upperCase = false) {
  PrimConvBin<Prim> writer(val);
  int ret = 0;
  switch (radix)
  {
  case eCarbonBin:
    ret = writer.writeBin(result, len, numBits);
    break;
  case eCarbonHex:
    ret = writer.writeHex(result, len, numBits, upperCase);
    break;
  case eCarbonOct:
    ret = writer.writeOct(result, len, numBits);
    break;
  case eCarbonDec:
  case eCarbonUDec:
    INFO_ASSERT(0, "Invalid radix.");
    break;
  }

  return ret;
}

template <typename T> 
void sFixBinaryXZValueUtil(char* valueStr, 
                           const T* xdrive, 
                           const T* idrive,
                           const T* forceMask,
                           const T* controlMask, 
                           const T* valueMask, 
                           const T* xzMask,
                           bool isPulled, int* numXs, int* numZs, 
                           int numElems, int bitwidth)
{
  *numXs = 0;
  *numZs = 0;
  
  for (int i = 0; i < numElems; ++i)
  {
    T controlVal = 0;
    if (controlMask)
      controlVal = controlMask[i];

    const int cNumBitsPerT = (sizeof(T) * 8);

    T widthMask = T(~0);
    if (i == numElems - 1)
    {
      UInt32 widthMod = bitwidth % cNumBitsPerT;
      if (widthMod != 0)
      {
        widthMask <<= widthMod;
        widthMask = ~widthMask;
      }
      controlVal &= widthMask;
    }
    

    int numBitsToProcess = (cNumBitsPerT < bitwidth) ? cNumBitsPerT : bitwidth;
    
    T drvVal = 0;
    T forceVal = 0;
    // xdrive can only exist with idrive
    if (xdrive && idrive)
      drvVal = xdrive[i] & ~idrive[i];
    else if (idrive)
      drvVal = ~idrive[i];
    else if (xdrive) // needed for expression nets
      drvVal = xdrive[i];
    
    if (forceMask)
      forceVal = forceMask[i];
    
    T bitmask = 0x1;
    for (int j = 0; j < numBitsToProcess; ++j)
    {      
      /*
        Priority control setting. Force overrides
        everything. ControlVal overrides drive val and pull. 
        Pull overrides driveval.
      */
      T bitControl = 0;
      T bitXZ = 0;
      T bitValue = 0;
      
      // forceVal trumps everyone
      if ((forceVal & bitmask) == 0)
      {
        T tmpControlBit = controlVal & bitmask;
        bitControl |= tmpControlBit;
        
        // We need to reset the bitXZ and bitValue to the overridden
        // values if tmpControlBit is positive
        if (tmpControlBit != 0)
        {
          T valueMaskCp = 0;
          if (valueMask)
            valueMaskCp = valueMask[i];
          T xzMaskCp = 0;
          if (xzMask)
            xzMaskCp = xzMask[i];

          bitXZ = xzMaskCp & bitmask;
          bitValue = valueMaskCp & bitmask;
        }
        else if (! isPulled)
        {
          // if the value is pulled, the drive value doesn't matter
          bitControl = drvVal & bitmask;
          // This allows for constant ShellNets to have x's
          if (valueStr[bitwidth - j - 1] == '1')
            bitValue = 1;
          if (bitControl != 0)
            bitXZ = 1;
        }
      }
      
      if ((bitControl != 0) && (bitXZ != 0))
      {
        // ok we are definitely overriding either by the override
        // mask or the drive mask.
        if (bitValue == 0)
        {
          valueStr[bitwidth - j - 1] = 'z';
          ++(*numZs);
        }
        else
        {
          valueStr[bitwidth - j - 1] = 'x';
          ++(*numXs);
        }
      }
      bitmask <<= 1;
    }
    bitwidth -= numBitsToProcess;
  }
}

//! Calculates x/z, constant 1/0 into precalculated string
/*!
  \param valueStr string buffer for binary value.
  \param xdrive For a bidirect, the external drive value
  NULL, if not a tristate
  \param idrive For a tristate, the internal drive value.
  NULL, if not a tristate
  \param forceMask Force mask. This actually trumps any bit
  overriding. For each bit that is 1 in this mask, the bit is forced
  and the value should not be touched. Null if not a force net.
  \param controlMask Override Control mask. The statically known bits
  to override.Can be NULL.
  \param valueMask The statically known values for each bit. Combines
  with xzmask to determine whether or not we have an x/z/0/1. Can be NULL.
  \param xzMask Force mask. The statically known x/z'ness for each
  bit. Can be NULL.
  \param isPulled Whether or not the net is pulled
  \param numXs The number of X's that were inserted into the binary
  string.
  \param numZs The number of Z's that were inserted into the binary
  string.
  \param bitwidth Number of significant bits
*/
template <typename T> void sFixBinaryXZValue(char* valueStr, 
                                             const T* xdrive, 
                                             const T* idrive, 
                                             const T* forceMask,
                                             const T* controlMask, 
                                             const T* valueMask,
                                             const T* xzMask,
                                             bool isPulled, 
                                             int bitwidth)
{
  int numElems = (bitwidth + sizeof(T) * 8 - 1)/(sizeof(T) * 8);
  int numXs, numZs;
  sFixBinaryXZValueUtil(valueStr, xdrive, idrive, forceMask, controlMask, valueMask, xzMask, 
                        isPulled, &numXs, &numZs, numElems, bitwidth);
}

template <typename T>
void sFixHexXZValue(char* valueStr,
                    const T* xdrive,
                    const T* idrive,
                    const T* forceMask,
                    const T* controlMask,
                    const T* valueMask,
                    const T* xzMask,
                    bool isPulled, 
                    int bitwidth)
{
  int numElems = (bitwidth + sizeof(T) * 8 - 1)/(sizeof(T) * 8);

  UInt32 insertIndex = (bitwidth + 3)/4;
  
  for (int j = 0; j < numElems; ++j)
  {
    // initialize so that the external world is not driving. This
    // works well if we are dealing with a non-bidirect
    // tristate. Allows xdrive & ~idrive to work.
    T xdriveCp = T(~0);
    if (xdrive)
      xdriveCp = xdrive[j];

    // initialize so that the internal world is always driving. In
    // case this is a non-bidirect tristate
    T idriveCp = T(~0);
    if (idrive)
      idriveCp = idrive[j];
    
    T forceCp = 0;
    if (forceMask)
      forceCp = forceMask[j];
    
    T controlMaskCp = 0;
    T valueMaskCp = 0;
    T xzMaskCp = 0;
    
    if (controlMask)
    {
      controlMaskCp = controlMask[j];
      valueMaskCp = valueMask[j];
      xzMaskCp = xzMask[j];
    }

    int numNibbles = 8;
    if (j == numElems - 1)
    {
      // bitwidth has been modified
      numNibbles = (bitwidth + 3)/4;
    }
    
    for (int i = 0; i < numNibbles; ++i)
    {
      int nibbleSize = (4 < bitwidth) ? 4 : bitwidth;
      int numXs, numZs;
      {
        char tmpChar[5] = {'0', '0', '0', '0', '\0'};
        sFixBinaryXZValueUtil(tmpChar, &xdriveCp, &idriveCp, &forceCp, &controlMaskCp, &valueMaskCp, &xzMaskCp, isPulled, &numXs, &numZs, 1, nibbleSize);
      }
      
      --insertIndex;
      if (numXs == nibbleSize)
        valueStr[insertIndex] = 'x';
      else if (numZs == nibbleSize)
        valueStr[insertIndex] = 'z';
      else if (numXs > 0)
        valueStr[insertIndex] = 'X';
      else if (numZs > 0)
        valueStr[insertIndex] = 'Z';
      
      controlMaskCp >>= nibbleSize;
      valueMaskCp >>= nibbleSize;
      xzMaskCp >>= nibbleSize;
      forceCp >>= nibbleSize;
      xdriveCp >>= nibbleSize;
      idriveCp >>= nibbleSize;
      bitwidth -= nibbleSize;
    }
  }
}

template <typename T>
void sFixOctXZValue(char* valueStr, 
                    const T* xdrive, 
                    const T* idrive, 
                    const T* forceMask,
                    const T* controlMask,
                    const T* valueMask,
                    const T* xzMask,
                    bool isPulled, 
                    int bitwidth)
{
  int numElems = (bitwidth + sizeof(T) * 8 - 1)/(sizeof(T) * 8);

  UInt32 headShift = 0;

  // see comments in sFixHexXZValue
  T xdriveCp = T(~0);
  T idriveCp = T(~0);
  T forceCp = 0;
  T controlMaskCp = 0;
  T valueMaskCp = 0;
  T xzMaskCp = 0;
  
  if (xdrive)
    xdriveCp = xdrive[0];
  if (idrive)
    idriveCp = idrive[0];
  if (forceMask)
    forceCp = forceMask[0];

  if (controlMask)
  {
    controlMaskCp = controlMask[0];
    valueMaskCp = valueMask[0];
    xzMaskCp = xzMask[0];
  }

  UInt32 insertIndex = (bitwidth + 2)/3;

  const int cNumBitsPerT = (sizeof(T) * 8);

  for (int i = 0; i < numElems; ++i)
  {
    // recalculate the head shift
    headShift = (cNumBitsPerT + headShift) % 3;

    int numOctets = cNumBitsPerT/3; // 10 octets
    if (i == numElems - 1)
    {
      // bitwidth has been modified
      numOctets = (bitwidth + 2)/3;
    }
    
    for (int j = 0; j < numOctets; ++j)
    {
      int octetSize = (3 < bitwidth) ? 3 : bitwidth;
      
      int numXs, numZs;
      {
        char tmpChar[4] = {'0', '0', '0', '\0'};
        sFixBinaryXZValueUtil(tmpChar, &xdriveCp, &idriveCp, &forceCp, &controlMaskCp, &valueMaskCp, &xzMaskCp, isPulled, &numXs, &numZs, 1, octetSize);
      }
      
      --insertIndex;
      if (numXs == octetSize)
        valueStr[insertIndex] = 'x';
      else if (numZs == octetSize)
        valueStr[insertIndex] = 'z';
      else if (numXs > 0)
        valueStr[insertIndex] = 'X';
      else if (numZs > 0)
        valueStr[insertIndex] = 'Z';
      
      controlMaskCp >>= octetSize;
      valueMaskCp >>= octetSize;
      xzMaskCp >>= octetSize;
      forceCp >>= octetSize;
      xdriveCp >>= octetSize;
      idriveCp >>= octetSize;
      bitwidth -= octetSize;
    } // for j
    
    // get the next value and combine it with the leftover and
    // calculate. 
    xdriveCp = T(~0);
    idriveCp = T(~0);
    forceCp = 0;
    controlMaskCp = 0;
    valueMaskCp = 0;
    xzMaskCp = 0;
    
    if (xdrive)
      xdriveCp = xdrive[i];
    if (xdrive)
      idriveCp = idrive[i];
    if (forceMask)
      forceCp = forceMask[i];
    if (controlMask)
    {
      controlMaskCp = controlMask[i];
      valueMaskCp = valueMask[i];
      xzMaskCp = xzMask[i];
    }

    T nextXdrive = T(~0);
    T nextIdrive = T(~0);
    T nextForce = 0;
    T nextControl = 0;
    T nextValue = 0;
    T nextXZ = 0;
    
    if (i < numElems - 1)
    {
      if (xdrive)
        nextXdrive= xdrive[i + 1];
      if (idrive)
        nextIdrive= idrive[i + 1];

      if (forceMask)
        nextForce = forceMask[i + 1];
      if (controlMask)
      {
        nextControl = controlMask[i + 1];
        nextValue = valueMask[i + 1];
        nextXZ = xzMask[i + 1];
      }
    }

    if (bitwidth > 0)
    {
      if (headShift != 0)
      {
        // base 8 correction.
        
        xdriveCp >>= (cNumBitsPerT - headShift);
        xdriveCp |= (nextXdrive << headShift);
        idriveCp >>= (cNumBitsPerT - headShift);
        idriveCp |= (nextIdrive << headShift);
        forceCp >>= (cNumBitsPerT - headShift);
        forceCp |= (nextForce << headShift);
        controlMaskCp >>= (cNumBitsPerT - headShift);
        controlMaskCp |= (nextControl << headShift);
        valueMaskCp >>= (cNumBitsPerT - headShift);
        valueMaskCp |= (nextValue << headShift);
        xzMaskCp >>= (cNumBitsPerT - headShift);
        xzMaskCp |= (nextXZ << headShift);
        
        int octetSize = (3 < bitwidth) ? 3 : bitwidth;
        
        char tmpChar[4] = {'0', '0', '0', '\0'};
        int numXs, numZs;
        sFixBinaryXZValueUtil(tmpChar, &xdriveCp, &idriveCp, &forceCp, &controlMaskCp, &valueMaskCp, &xzMaskCp, isPulled, &numXs, &numZs, 1, octetSize);
        
        if (numXs == octetSize)
          valueStr[insertIndex] = 'x';
        else if (numZs == octetSize)
          valueStr[insertIndex] = 'z';
        else if (numXs > 0)
          valueStr[insertIndex] = 'X';
        else if (numZs > 0)
          valueStr[insertIndex] = 'Z';
        
        --insertIndex;
        xdriveCp = nextXdrive >> (3 - headShift);
        idriveCp = nextIdrive >> (3 - headShift);
        forceCp = nextForce >> (3 - headShift);
        controlMaskCp = nextControl >> (3 - headShift);
        valueMaskCp = nextValue >> (3 - headShift);
        xzMaskCp = nextXZ >> (3 - headShift);
        bitwidth -= octetSize;
      }
      else 
      {
        // no correction needed. Ended conversion at the end of
        // the word
        xdriveCp = nextXdrive;
        idriveCp = nextIdrive;
        forceCp = nextForce;
        controlMaskCp = nextControl;
        valueMaskCp = nextValue;
        xzMaskCp = nextXZ;
      }
    }
  }
}

// This function is only used indirectly by carbon shellnets. No need
// to handle vhdl values here (yet?)
static char sAccumulateXZValCheck(char cumulative, char wordResult)
{
  switch (cumulative)
  {
  case '0':
    if (wordResult == 'X')
      return 'X';
    cumulative = wordResult;
    break;
  case 'x':
    if (wordResult != 'x')
      // done, partially x
      return 'X';
    break;
  case 'z':
    if ((wordResult == 'x') || (wordResult == 'X'))
      // done, partially x
      return 'X';
    else if ((wordResult == '0') || (wordResult == 'Z'))
      cumulative = 'Z'; // partially z so far
    break;
  case 'Z':
    if ((wordResult == 'x') || (wordResult == 'X'))
      // done, partially x
      return 'X';
    break;
  case 'X':
    // can't get here, but just in case
    INFO_ASSERT(cumulative != 'X', "String not properly initialized.");
    break;
  default:
    {
      UtString buf;
      buf << "Invalid state value: " << cumulative;
      INFO_ASSERT(0, buf.c_str());
    }
    break;
  }
  return cumulative;
}

template <typename T>
char sIsDecValXZ(const T* src, 
                 const T* xdrive, const T* idrive, 
                 const T* forceMask,
                 const T* controlMask, 
                 const T* valueMask, 
                 const T* xzMask, 
                 bool isPulled, int bitwidth)
{
  char cumulative = '0';
  const size_t cNumBitsPerT = (sizeof(T) * 8);
  int numElems = (bitwidth + cNumBitsPerT - 1)/cNumBitsPerT;
  
  T lastMask = T(~0);
  {
    UInt32 numlastBits = bitwidth % cNumBitsPerT;
    if (numlastBits != 0)
    {
      lastMask <<= numlastBits;
      lastMask = ~lastMask;
    }
  }
  
  for (int j = 0; j < numElems; ++j)
  {
    T mask = T(~0);
    if (j == numElems - 1)
      mask = lastMask;
    
    T controlMaskCp = 0;
    T valueMaskCp = 0;
    T xzMaskCp = 0;
    
    if (controlMask)
    {
      controlMaskCp = controlMask[j] & mask;
      valueMaskCp = valueMask[j] & mask;
      xzMaskCp = xzMask[j] & mask;
    }

    T drvCp = 0;
    T forceCp = 0;
    T srcCp = src[j] & mask;

    if (xdrive && idrive)
      drvCp = xdrive[j] & ~idrive[j];
    else if (idrive)
      drvCp = ~idrive[j];
    else if (xdrive)
      drvCp = xdrive[j]; // needed for expression nets
    
    drvCp &= mask;
    
    if (forceMask)
      forceCp = forceMask[j];
    

    // override xz and control masks with force
    // cannot force x or z, so it removes an unknown.
    controlMaskCp &= ~forceCp;
    xzMaskCp &= ~forceCp;
    
    // override drvCp where xzMask is set
    drvCp &= ~xzMaskCp;
    
    char wordResult = '0';
    
    if (! isPulled)
    {
      // all z's/x's?
      if (drvCp == mask) 
      {
        if (srcCp == 0)
          wordResult = 'z';
        else if (srcCp == mask)
          wordResult = 'x';
        else
          wordResult = 'X';
      }
      else if (drvCp != 0) // partially z/x?
      {
        if ((srcCp & drvCp) != 0)
          wordResult = 'X';
        else
          wordResult = 'Z';
      }
    }

    // assuming xzMaskCp would only have 1's in places that
    // controlMaskCp would have 1's (no need to mask the two)
    if (xzMaskCp != 0)
    {
      T xzValCombine = xzMaskCp & valueMaskCp;
      
      if (xzValCombine == xzMaskCp) // all x's?
        wordResult = 'x';
      else if (xzValCombine != 0) // partially x?
        wordResult = 'X';
      else if (xzMaskCp == mask) // all z's?
        wordResult = 'z';
      else
        wordResult = 'Z'; // must be partially z
    }
    
    cumulative = sAccumulateXZValCheck(cumulative, wordResult);
  } // for
  
  return cumulative;
}

template <typename T> 
void sDoSetConstantBits(T* val, T* idrive, const T* controlMask, const T* valueMask, const T* xzMask, size_t bitwidth)
{
  const size_t cNumBitsPerT = (sizeof(T) * 8);
  int numElems = (bitwidth + cNumBitsPerT - 1)/cNumBitsPerT;
  
  for (int i = 0; i < numElems; ++i)
  {
    T controlCp = controlMask[i];
    T valMCp = valueMask[i];
    T xzCp = xzMask[i];
    
    int numBitsToProcess = (cNumBitsPerT < bitwidth) ? cNumBitsPerT : bitwidth;
    T valCp = val[i];
    
    T idriveCp = 0;
    if (idrive)
      idriveCp = idrive[i];
    
    T mask = 0x1;
    while(controlCp != 0)
    {
      if (((controlCp & 0x1) != 0) && ((xzCp & 0x1) == 0))
      {
        // always driven
        idriveCp |= mask;
        if ((valMCp & mask) != 0)
          valCp |= mask;
        else 
          valCp &= ~mask;
      }
      controlCp >>= 1;
      xzCp >>= 1;
      mask <<= 1;
    }
    
    val[i] = valCp;
    if (idrive)
      idrive[i] = idriveCp;
    bitwidth -= numBitsToProcess;
  }
}

int CarbonValRW::writeBinXZValToStr(char* valueStr, size_t len, 
                                    const UInt8* src, 
                                    const UInt8* xdrive, 
                                    const UInt8* idrive, 
                                    const UInt8* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt8* controlMask = NULL;
    const UInt8* valueMask = NULL;
    const UInt8* xzMask = NULL;
    
    UInt8 control, value, xz;

    if (overrideMask)
    {
      control = *overrideMask;
      value = *(overrideMask + 1);
      xz = *(overrideMask + 2);

      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }
    
    sFixBinaryXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                      valueMask, xzMask, 
                      isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeBinXZValToStr(char* valueStr, size_t len, 
                                    const UInt16* src, 
                                    const UInt16* xdrive, 
                                    const UInt16* idrive, 
                                    const UInt16* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt16* controlMask = NULL;
    const UInt16* valueMask = NULL;
    const UInt16* xzMask = NULL;
    
    UInt16 control, value, xz;

    if (overrideMask)
    {
      control = *overrideMask;
      value = *(overrideMask + 1);
      xz = *(overrideMask + 2);

      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixBinaryXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                      valueMask, xzMask, 
                      isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeBinXZValToStr(char* valueStr, size_t len, 
                                    const UInt32* src, 
                                    const UInt32* xdrive, 
                                    const UInt32* idrive, 
                                    const UInt32* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt32* controlMask = NULL;
    const UInt32* valueMask = NULL;
    const UInt32* xzMask = NULL;

    if (overrideMask)
    {
      size_t numControlWords = sGetNumWords(bitwidth);
      controlMask = overrideMask;
      valueMask = overrideMask + numControlWords;
      xzMask = overrideMask + 2*numControlWords;
    }
    sFixBinaryXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                      valueMask, xzMask, 
                      isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeBinXZValToStr(char* valueStr, size_t len, 
                                    const UInt64* src, 
                                    const UInt64* xdrive,
                                    const UInt64* idrive,
                                    const UInt64* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt64* controlMask = NULL;
    const UInt64* valueMask = NULL;
    const UInt64* xzMask = NULL;

    UInt64 control, value, xz;
    if (overrideMask)
    {
      cpSrcToDest(&control, overrideMask, 2);
      cpSrcToDest(&value, overrideMask + 2, 2);
      cpSrcToDest(&xz, overrideMask + 4, 2);
      
      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixBinaryXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                      valueMask, xzMask, 
                      isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeHexXZValToStr(char* valueStr, size_t len, 
                                    const UInt8* src, 
                                    const UInt8* xdrive, 
                                    const UInt8* idrive, 
                                    const UInt8* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt8* controlMask = NULL;
    const UInt8* valueMask = NULL;
    const UInt8* xzMask = NULL;
    
    UInt8 control, value, xz;
    
    if (overrideMask)
    {
      control = *overrideMask;
      value = *(overrideMask + 1);
      xz = *(overrideMask + 2);

      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixHexXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                   valueMask, xzMask, 
                   isPulled, bitwidth);
  }
  
  return stat;
}

int CarbonValRW::writeHexXZValToStr(char* valueStr, size_t len, 
                                    const UInt16* src, 
                                    const UInt16* xdrive, 
                                    const UInt16* idrive, 
                                    const UInt16* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt16* controlMask = NULL;
    const UInt16* valueMask = NULL;
    const UInt16* xzMask = NULL;
    
    UInt16 control, value, xz;
    
    if (overrideMask)
    {
      control = *overrideMask;
      value = *(overrideMask + 1);
      xz = *(overrideMask + 2);

      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixHexXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                   valueMask, xzMask, 
                   isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeHexXZValToStr(char* valueStr, size_t len, 
                                    const UInt32* src, 
                                    const UInt32* xdrive, 
                                    const UInt32* idrive, 
                                    const UInt32* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt32* controlMask = NULL;
    const UInt32* valueMask = NULL;
    const UInt32* xzMask = NULL;

    if (overrideMask)
    {
      size_t numControlWords = sGetNumWords(bitwidth);
      controlMask = overrideMask;
      valueMask = overrideMask + numControlWords;
      xzMask = overrideMask + 2*numControlWords;
    }
    sFixHexXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                   valueMask, xzMask, 
                   isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeHexXZValToStr(char* valueStr, size_t len, 
                                    const UInt64* src, 
                                    const UInt64* xdrive,
                                    const UInt64* idrive,
                                    const UInt64* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt64* controlMask = NULL;
    const UInt64* valueMask = NULL;
    const UInt64* xzMask = NULL;

    UInt64 control, value, xz;
    if (overrideMask)
    {
      cpSrcToDest(&control, overrideMask, 2);
      cpSrcToDest(&value, overrideMask + 2, 2);
      cpSrcToDest(&xz, overrideMask + 4, 2);
      
      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixHexXZValue(valueStr, xdrive, idrive, forceMask, 
                   controlMask, valueMask, xzMask,
                   isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeOctXZValToStr(char* valueStr, size_t len, 
                                    const UInt8* src, 
                                    const UInt8* xdrive, 
                                    const UInt8* idrive, 
                                    const UInt8* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt8* controlMask = NULL;
    const UInt8* valueMask = NULL;
    const UInt8* xzMask = NULL;
    
    UInt8 control, value, xz;
    
    if (overrideMask)
    {
      control = *overrideMask;
      value = *(overrideMask + 1);
      xz = *(overrideMask + 2);

      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixOctXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                   valueMask, xzMask, 
                   isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeOctXZValToStr(char* valueStr, size_t len, 
                                    const UInt16* src, 
                                    const UInt16* xdrive, 
                                    const UInt16* idrive, 
                                    const UInt16* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt16* controlMask = NULL;
    const UInt16* valueMask = NULL;
    const UInt16* xzMask = NULL;
    
    UInt16 control, value, xz;
    
    if (overrideMask)
    {
      control = *overrideMask;
      value = *(overrideMask + 1);
      xz = *(overrideMask + 2);

      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixOctXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                   valueMask, xzMask, 
                   isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeOctXZValToStr(char* valueStr, size_t len, 
                                    const UInt32* src, 
                                    const UInt32* xdrive, 
                                    const UInt32* idrive, 
                                    const UInt32* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt32* controlMask = NULL;
    const UInt32* valueMask = NULL;
    const UInt32* xzMask = NULL;

    if (overrideMask)
    {
      size_t numControlWords = sGetNumWords(bitwidth);
      controlMask = overrideMask;
      valueMask = overrideMask + numControlWords;
      xzMask = overrideMask + 2*numControlWords;
    }
    sFixOctXZValue(valueStr, xdrive, idrive, forceMask, controlMask, 
                   valueMask, xzMask, 
                   isPulled, bitwidth);
  }
  return stat;
}

int CarbonValRW::writeOctXZValToStr(char* valueStr, size_t len, 
                                    const UInt64* src, 
                                    const UInt64* xdrive,
                                    const UInt64* idrive,
                                    const UInt64* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    size_t bitwidth)
{
  int stat = PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
  // -1 is an error, and 0 means no work to do
  if (stat > 0)
  {
    const UInt64* controlMask = NULL;
    const UInt64* valueMask = NULL;
    const UInt64* xzMask = NULL;

    UInt64 control, value, xz;
    if (overrideMask)
    {
      cpSrcToDest(&control, overrideMask, 2);
      cpSrcToDest(&value, overrideMask + 2, 2);
      cpSrcToDest(&xz, overrideMask + 4, 2);
      
      controlMask = &control;
      valueMask = &value;
      xzMask = &xz;
    }

    sFixOctXZValue(valueStr, xdrive, idrive, forceMask, 
                   controlMask, valueMask, xzMask,
                   isPulled, bitwidth);
  }
  return stat;
}


int CarbonValRW::writeDecXZValToStr(char* valueStr, size_t len, 
                                    const UInt8* src, 
                                    const UInt8* xdrive, 
                                    const UInt8* idrive, 
                                    const UInt8* forceMask, 
                                    const UInt32* overrideMask, 
                                    bool isPulled,
                                    bool doSigned,
                                    size_t bitwidth)
{
  int stat = -1;

  const UInt8* controlMask = NULL;
  const UInt8* valueMask = NULL;
  const UInt8* xzMask = NULL;
    
  UInt8 control, value, xz;
    
  if (overrideMask)
  {
    control = *overrideMask;
    value = *(overrideMask + 1);
    xz = *(overrideMask + 2);

    controlMask = &control;
    valueMask = &value;
    xzMask = &xz;
  }

  char isXZ = sIsDecValXZ(src, xdrive, idrive, forceMask,
                          controlMask, valueMask, xzMask, 
                          isPulled, bitwidth);

  if (isXZ != '0')
  {
    if (len > 1)
    {
      valueStr[0] = isXZ;
      valueStr[1] = '\0';
      stat = 1;
    }
  }
  else
    stat = writeDecValToStr(valueStr, len, src, doSigned, bitwidth);

  return stat;
}

int CarbonValRW::writeDecXZValToStr(char* valueStr, size_t len, 
                                    const UInt16* src, 
                                    const UInt16* xdrive, 
                                    const UInt16* idrive, 
                                    const UInt16* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    bool doSigned,
                                    size_t bitwidth)
{
  int stat = -1;

  const UInt16* controlMask = NULL;
  const UInt16* valueMask = NULL;
  const UInt16* xzMask = NULL;
    
  UInt16 control, value, xz;
    
  if (overrideMask)
  {
    control = *overrideMask;
    value = *(overrideMask + 1);
    xz = *(overrideMask + 2);

    controlMask = &control;
    valueMask = &value;
    xzMask = &xz;
  }

  char isXZ = sIsDecValXZ(src, xdrive, idrive, forceMask,
                          controlMask, valueMask, xzMask, 
                          isPulled, bitwidth);

  if (isXZ != '0')
  {
    if (len > 1)
    {
      valueStr[0] = isXZ;
      valueStr[1] = '\0';
      stat = 1;
    }
  }
  else
    stat = writeDecValToStr(valueStr, len, src, doSigned, bitwidth);

  return stat;
}

int CarbonValRW::writeDecXZValToStr(char* valueStr, size_t len, 
                                    const UInt32* src, 
                                    const UInt32* xdrive, 
                                    const UInt32* idrive, 
                                    const UInt32* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    bool doSigned,
                                    size_t bitwidth)
{
  int stat = -1;

  const UInt32* controlMask = NULL;
  const UInt32* valueMask = NULL;
  const UInt32* xzMask = NULL;
  
  if (overrideMask)
  {
    size_t numControlWords = sGetNumWords(bitwidth);
    controlMask = overrideMask;
    valueMask = overrideMask + numControlWords;
    xzMask = overrideMask + 2*numControlWords;
  }

  char isXZ = sIsDecValXZ(src, xdrive, idrive, forceMask,
                          controlMask, valueMask, xzMask, 
                          isPulled, bitwidth);
  
  if (isXZ != '0')
  {
    if (len > 1)
    {
      valueStr[0] = isXZ;
      valueStr[1] = '\0';
      stat = 1;
    }
  }
  else
    stat = writeDecValToStr(valueStr, len, src, doSigned, bitwidth);

  return stat;
}

int CarbonValRW::writeDecXZValToStr(char* valueStr, size_t len, 
                                    const UInt64* src, 
                                    const UInt64* xdrive, 
                                    const UInt64* idrive, 
                                    const UInt64* forceMask, 
                                    const UInt32* overrideMask, bool isPulled,
                                    bool doSigned,
                                    size_t bitwidth)
{
  int stat = -1;

  const UInt64* controlMask = NULL;
  const UInt64* valueMask = NULL;
  const UInt64* xzMask = NULL;
  
  UInt64 control, value, xz;
  if (overrideMask)
  {
    cpSrcToDest(&control, overrideMask, 2);
    cpSrcToDest(&value, overrideMask + 2, 2);
    cpSrcToDest(&xz, overrideMask + 4, 2);
    
    controlMask = &control;
    valueMask = &value;
    xzMask = &xz;
  }
  
  char isXZ = sIsDecValXZ(src, xdrive, idrive, forceMask,
                          controlMask, valueMask, xzMask, 
                          isPulled, bitwidth);

  if (isXZ != '0')
  {
    if (len > 1)
    {
      valueStr[0] = isXZ;
      valueStr[1] = '\0';
      stat = 1;
    }
  }
  else
    stat = writeDecValToStr(valueStr, len, src, doSigned, bitwidth);
  
  return stat;
}

int CarbonValRW::writeBinValToStr(char* valueStr, size_t len, 
                                  const UInt64* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
}
  
int CarbonValRW::writeBinValToStr(char* valueStr, size_t len,
                                  const UInt32* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
}

int CarbonValRW::writeBinValToStr(char* valueStr, size_t len,
                                  const UInt16* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
}
   
int CarbonValRW::writeBinValToStr(char* valueStr, size_t len,
                                  const UInt8* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
}

int CarbonValRW::writeBinValToStr(char* valueStr, size_t len,
                                  const char* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonBin);
}

int CarbonValRW::writeHexValToStr(char* valueStr, size_t len,
                                  const UInt64* src, size_t bitwidth,
                                  bool upperCase, bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex, upperCase);
}
  
int CarbonValRW::writeHexValToStr(char* valueStr, size_t len,
                                  const UInt32* src, size_t bitwidth,
                                  bool upperCase, bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex, upperCase);
}


int CarbonValRW::writeHexValToStr(char* valueStr, size_t len,
                                  const UInt16* src, size_t bitwidth,
                                  bool upperCase, bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex, upperCase);
}
  
int CarbonValRW::writeHexValToStr(char* valueStr, size_t len,
                                  const UInt8* src, size_t bitwidth,
                                  bool upperCase, bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonHex, upperCase);
}


//! encode in decimal format anything that will fit into a single element of type T
/*!
 *  \param valueStr pointer to buffer where result is placed
 *  \param len is the capacity of the \a valueStr buffer
 *         (be sure to leave space for the trailing null)
 *  \param src pointer to the value to be encoded
 *  \param bitwidth number of bits in \a src to be encoded,
 *  \param doSigned if true then \a src is interpreted as a signed
 *          value and if negative then \a valueStr will include a
 *          minus sign '-' as the first character.
 *          If false then src is interpreted as an unsigned value.
 *
 * \return the number of characters placed in valueStr (not including
 * the trailing null).
 */
template <typename T> int smallDecToStrConvert(char* valueStr, size_t len,
                                               const T* src, bool doSigned,
                                               size_t bitwidth)
{

  // note: the arguments to this method are modified within this
  // method

  if ( bitwidth > (size_t) sizeof(T)*8 ) {
    return -1;                  // bitwidth is too large
  }

  int retVal = 0;
  // optimize for scalar. Scalars can never be signed
  if (bitwidth == 1)
  {
    if (len > 1)
    {
      valueStr[0] = ((*src & 0x1) != 0) ? '1' : '0';
      valueStr[1] = '\0';
      retVal = 1;
    }
    else
      retVal = -1;
  }
  else if (bitwidth > 0)
  {
    T val = getPrimVal(*src, bitwidth);
    
    if ( doSigned ){
      bool isNeg = ( (val & ((T) 1 << (bitwidth-1))) != 0 );
    
      if (isNeg)
      {
        // we will need at least 3 bytes, one for sign, one for a
        // digit, and one for trailing null
        if (len > 2)
        {
          // print sign
          valueStr[0] = '-';
          // and generate 2's complement, to handle as unsigned
          val = ~val;
          ++val;
          val = getPrimVal(val, bitwidth); // remask 

          // modify args so that the minus sign is preserved at beginning of buffer
          --len;
          ++valueStr;
          retVal += 1;
        }
        else {
          return -1;
        }
      }
    }

    // from here on we handle the value as an unsigned
    if (val < 10)
    {
      // special case of single digit
      if (len > 1)
      {
        valueStr[0] = (char) ('0' + val);
        valueStr[1] = '\0';
        retVal += 1;
        return retVal;
      } else {
        return -1;
      }
    }
    else
    {
      UInt32 worstCaseBufsize = sCalcNumOctCharsNeeded(bitwidth);

      // this buffer size test is slightly pessimistic,
      // consider the 2 cases:
      // val   bitwidth   worstCaseBufsize
      // 8190  12         4
      // 8191  13         5
      // both could fit into 4+1 bytes, but to be safe we must protect
      // the worst case, so we use <=
       if (len <= worstCaseBufsize) {
        return -1;
      }

      // build decimal number from units end, inserting it into buffer
      // at end, once conversion is complete then it will be moved to front
      
      valueStr[len - 1] = '\0';

      // curChar points to where we are inserting digits
      char* curChar = valueStr + len - 1;
      while (val != 0)
      {        
        UInt32 rem = val % 10;
        --curChar;
        *curChar = (char) ('0' + rem);
        val /= 10;
      }

      // check to see if number needs to be shifted into place 
      if (curChar != valueStr) {
        int numChars = valueStr + len - curChar; // includes nul
        retVal += (numChars - 1);
        memmove(valueStr, curChar, numChars);
      }
      else {
        // the buffer was just large enough to hold the encoded value
        retVal += (len - 1);
      }
    }
  }
  return retVal;
}

int CarbonValRW::writeDecValToStr(char* valueStr, size_t len, 
                                  const UInt64* src,
                                  bool doSigned, size_t bitwidth)
{
#if pfLP64
  // very slow on a 32 bit machine
  return smallDecToStrConvert(valueStr, len, src, doSigned, bitwidth);
#else
  // much faster on a 32 bit machine
  UInt64 maskedVal = getPrimVal(*src, bitwidth);
#if 1
  // copy value into a value array
  UInt32 val[2];
  cpSrcToDest(val, &maskedVal, 2);
  if ( doSigned ) {
    return UtConv::BinaryToSignedDec(val, bitwidth, false, valueStr, len);
  } else {
    return UtConv::BinaryToUnsignedDec(val, bitwidth, valueStr, len);
  }
#else
  // This ended up being slower than above
  // bitrot warning: the doSigned arg is not supported here
  return UtConv::BinaryToUnsignedDec((UInt32*) &maskedVal, bitwidth, valueStr, len);
#endif
#endif
}

int CarbonValRW::writeDecValToStr(char* valueStr, size_t len, 
                                  const UInt32* src, 
                                  bool doSigned, size_t bitwidth)
{
  int ret = 0;
  if (bitwidth <= 32) 
  {
    ret = smallDecToStrConvert(valueStr, len, src, doSigned, bitwidth);
  }
  else {
    // call in the big gun
    if ( doSigned ) {
      ret = UtConv::BinaryToSignedDec(src, bitwidth, false, valueStr, len);
    } else {
      ret = UtConv::BinaryToUnsignedDec(src, bitwidth, valueStr, len);
    }
  }
  return ret;
}

int CarbonValRW::writeDecValToStr(char* valueStr, size_t len, 
                                  const UInt16* src,
                                  bool doSigned, size_t bitwidth) 
{
  return smallDecToStrConvert(valueStr, len, src, doSigned, bitwidth);
}
   
int CarbonValRW::writeDecValToStr(char* valueStr, size_t len, 
                                  const UInt8* src,
                                  bool doSigned, size_t bitwidth)
{
  return smallDecToStrConvert(valueStr, len, src, doSigned, bitwidth);
}

int CarbonValRW::writeOctValToStr(char* valueStr, size_t len, 
                                  const UInt64* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
}

int CarbonValRW::writeOctValToStr(char* valueStr, size_t len, 
                                  const UInt32* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
}


int CarbonValRW::writeOctValToStr(char* valueStr, size_t len,
                                  const UInt16* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
}
   
int CarbonValRW::writeOctValToStr(char* valueStr, size_t len, 
                                  const UInt8* src, size_t bitwidth,
                                  bool useMinWidth)
{
  if ( useMinWidth and ( bitwidth != 0 )){
    bitwidth = findMostSignificantBitPosition(src, bitwidth);
    if ( bitwidth == 0 ) {
      if ( len > 1 ) {
        valueStr[0] = '0';      // src is zero
        valueStr[1] = '\0';
        return 1;
      } else {
        return -1;              // no space
      }
    }
  }
  return PrimArrToRawBinStr(src, bitwidth, valueStr, len, eCarbonOct);
}

void CarbonValRW::setConstantBits(UInt8* val, UInt8* idrive, const UInt32* overrideMask, size_t bitwidth)
{
  UInt8 controlMask, valueMask, xzMask;
  
  // There are only 3 words in overrideMask, since the size of the
  // vector < 32 bits
  controlMask = *overrideMask;
  valueMask = *(overrideMask + 1);
  xzMask = *(overrideMask + 2);
  
  sDoSetConstantBits(val, idrive, &controlMask, &valueMask, &xzMask, bitwidth);
}

void CarbonValRW::setConstantBits(UInt16* val, UInt16* idrive, const UInt32* overrideMask, size_t bitwidth)
{
  UInt16 controlMask, valueMask, xzMask;

  // There are only 3 words in overrideMask, since the size of the
  // vector < 32 bits  
  controlMask = *overrideMask;
  valueMask = *(overrideMask + 1);
  xzMask = *(overrideMask + 2);
  
  sDoSetConstantBits(val, idrive, &controlMask, &valueMask, &xzMask, bitwidth);
}

void CarbonValRW::setConstantBits(UInt32* val, UInt32* idrive, const UInt32* overrideMask, size_t bitwidth)
{
  // Here, we may have more than 1 word representing the vector. The
  // overrideMask is 3x the number of words.
  size_t numControlWords = sGetNumWords(bitwidth);
  const UInt32* controlMask = overrideMask;
  const UInt32* valueMask = (overrideMask + numControlWords);
  const UInt32* xzMask = (overrideMask + 2*numControlWords);
  
  sDoSetConstantBits(val, idrive, controlMask, valueMask, xzMask, bitwidth);
}

void CarbonValRW::setConstantBits(UInt64* val, UInt64* idrive, const UInt32* overrideMask, size_t bitwidth)
{
  UInt64 controlMask, valueMask, xzMask;

  // Here, we wave exactly 2*3=6 words in overrideMask. The first 2
  // words are the controlMask, the next 2 are the valuemask and the
  // final 2 are the xz mask.
  cpSrcToDest(&controlMask, overrideMask, 2);
  cpSrcToDest(&valueMask, overrideMask + 2, 2);
  cpSrcToDest(&xzMask, overrideMask + 4, 2);
  
  sDoSetConstantBits(val, idrive, &controlMask, &valueMask, &xzMask, bitwidth);
}

void CarbonValRW::setControlBits(UInt32* valBuf, const UInt32* overrideMask, 
                                 size_t bitwidth)
{
  size_t numControlWords = sGetNumWords(bitwidth);
  cpSrcToDest(valBuf, overrideMask, numControlWords);
}

template <typename T> T getPrimBitMaskUnused(T width)
{
  T mask = ~T(0);
  int bitShift = (int)(width % (sizeof(T) * 8));
  if (bitShift > 0)
  {
    mask <<= bitShift;
    mask = ~mask;
  }
  return mask;
}

static bool sIsXValue(const char bit)
{
  const char upBit = toupper(bit);
  // handle vhdl values as well. All vhdl undefines become x at carbon
  return ((upBit == 'X') || 
          (upBit == 'U') || (upBit == 'W') || (upBit == '-'));
}

static bool sIsXZValue(const char bit)
{
  bool ret = sIsXValue(bit);
  if (! ret)
    ret = (bit == 'Z') || (bit == 'z');
  return ret;
}

bool UtConv::strToLongModify(const char** strPtr, SInt32* value, CarbonRadix radix, UtString* errMsg)
{
  int base = 0;
  switch(radix)
  {
  case eCarbonBin:
    INFO_ASSERT(radix != eCarbonBin, *strPtr);
    break;
  case eCarbonOct:
    base = 8;
    break;
  case eCarbonHex:
    base = 16;
    break;
  case eCarbonUDec:
  case eCarbonDec:
    base = 10;
    break;
  }
  const char* p = *strPtr;
  char* endPtr;

  *value = OSStrToS32(p, &endPtr, base, errMsg);
  *strPtr = endPtr;
  bool converted = true;
  if (endPtr == p)
    converted = false;
  return converted;
}

static void sFitStrValue(UtString* valPtr, int* truncExtStat, UInt32 bit_width)
{
  StringUtil::strip(valPtr);
  UtString& val = *valPtr;

  *truncExtStat = 0;
  {
    UtString::size_type pos = 0;
    while ((pos = val.find_first_of('_', pos)) != UtString::npos)
      val.replace(pos, 1, "");
  }

  UtString::size_type bitSize = val.size();
  if (bitSize > bit_width)
  {
    val.erase(0, bitSize - bit_width);
    *truncExtStat = -1;
  }
  else if (bitSize < bit_width)
  {
    char extender = '0';
    if (sIsXZValue(val[0]))
      extender = val[0];
    
    val.insert(UtString::size_type (0), bit_width - bitSize, extender);
    *truncExtStat = 1;
  }

}

bool UtConv::BinStrToValDrvFit(const char* s, DynBitVector *value, DynBitVector *drive, UInt32 bit_width, int* truncExtStat, bool do4State)
{
  UtString str(s);
  sFitStrValue(&str, truncExtStat, bit_width);

  value->resize(bit_width);
  if (drive)
    drive->resize(bit_width);
  
  bool result = true;
  *truncExtStat = 0;
  if (drive)
  {
    DynBitVector drv(bit_width);
    result = BinaryStringToUInt32(str.c_str(), value->getUIntArray(), drive->getUIntArray(), bit_width, do4State);
    drive->flip();
  }
  else
    result = BinaryStringToUInt32(str.c_str(), value->getUIntArray(), NULL, bit_width, do4State);
  
  return result;
}

bool UtConv::BinStrToUInt32Fit(const char* s, UInt32 *data, UInt32 *mask, UInt32 bit_width, int* truncExtStat)
{
  UtString val(s);
  sFitStrValue(&val, truncExtStat, bit_width);
  return BinaryStringToUInt32(val.c_str(), data, mask, bit_width);
}

bool UtConv::BinaryStringToUInt32(const char* s, UInt32 *data, UInt32 *mask, UInt32 bit_width, bool do4value)
{
  SInt32 wordIndex = -1;
  SInt32 shift = 32;
  char digit = '\0';
  const char* pos;
  bool result = true;


  for (pos = s + strlen(s) - 1; (pos != s - 1) and result; --pos)
  {
    digit = toupper(*pos);

    // Check if we should move to the next character (true on first iteration)
    if (shift == 32)
    {
      shift = 0;
      wordIndex++;
      data[wordIndex] = 0;
      if (mask)
      {
	mask[wordIndex] = 0;
      }
    }

    // Convert the character.  By default, X or Z get made into
    // 0.
    if (bit_width > 0)
    {
      if (digit == '1')
      {
	data[wordIndex] |= 1 << shift;
	if (mask)
	{
	  mask[wordIndex] |= 1 << shift;
	}
      }
      else if (digit == '0')
      {
	if (mask)
	{
	  mask[wordIndex] |= 1 << shift;
	}
      }
      else if (digit == '_')
      {
        // bit_width is going to be subtracted below. This is just a
        // separator. So, we need to add the bit back on.
        ++bit_width;
      }
      else if (do4value && sIsXValue(digit))
      {
	data[wordIndex] |= 1 << shift;
      }
      else if (do4value && (digit == 'Z'))
        ; // fine as is
      else if (! sIsXZValue(digit))
      {
	result = false;
      }
      
      // We moved 1 bit
      bit_width -= 1;
      shift++;
    }
    else if (digit != '_')
    {
      result = false;
    }
  }

  // Check for size mismatch
  if (result and ((bit_width != 0) or (pos != s - 1)))
  {
    result = false;
  }

  return result;
}



UtString * UtConv::ZeroExtendValue(const char* s, UInt32 bit_width, CarbonRadix radix)
{
  UtString::size_type target_size = 0;

  INFO_ASSERT(bit_width > 0, s);

  switch (radix)
  {
  case eCarbonBin:
    target_size = bit_width;
    break;
  case eCarbonOct:
    target_size = ((bit_width - 1)/ 3) + 1;
    break;
  case eCarbonDec:
    INFO_ASSERT(0, "Not yet implemented."); // !! TBD
    break;
  case eCarbonHex:
    target_size = ((bit_width - 1) / 4) + 1;
    break;
  default:
    INFO_ASSERT(0, "Invalid radix.");
    break;
  }

  UtString * b = new UtString(target_size, '0');
  
  size_t source_size = strlen(s);
  b->replace(target_size - source_size, source_size, s);

  return b;
}

bool UtConv::HexStrToUInt32Fit(const char* s, UInt32 *data, UInt32 *mask, UInt32 bit_width, int* truncExtStat)
{
  UtString val(s);
  StringUtil::strip(&val);
  *truncExtStat = 0;
  {
    UtString::size_type pos = 0;
    while ((pos = val.find_first_of('_', pos)) != UtString::npos)
      val.replace(pos, 1, "");
  }
  UtString::size_type bitSize = val.size();

  UInt32 numHexNeeded = (bit_width + 3)/4;
  UInt32 numCurrent = bitSize;
  
  if (numCurrent > numHexNeeded)
  {
    val.erase(0, numCurrent - numHexNeeded);
    *truncExtStat = -1;
  }
  else if (numCurrent < numHexNeeded)
  {
    char extender = '0';
    if (sIsXZValue(val[0]))
      extender = val[0];
    
    val.insert(UtString::size_type (0), numHexNeeded - numCurrent, extender);
    *truncExtStat = 1;
  }
  return HexStringToUInt32(val.c_str(), data, mask, bit_width);
}

bool UtConv::HexStringToUInt32(const char* s, UInt32 *data, UInt32* mask,
                               UInt32 bit_width)
{
  SInt32 wordIndex = -1;
  SInt32 shift = 32;
  char digit = '\0';
  const char* pos;
  bool result = true;

  for (pos = s + strlen(s) - 1; pos >= s and result; --pos)
  {
    digit = toupper(*pos);
    // Check if we should move to the next character (true on first iteration)
    if (shift == 32)
    {
      shift = 0;
      wordIndex++;
      data[wordIndex] = 0;
      if (mask != NULL)
        mask[wordIndex] = 0;
    }

    bool isZorX = sIsXZValue(digit);
    if ((bit_width > 0) && (isxdigit(digit) || isZorX))
    {
      UInt32 subWord;
      UInt32 subWordMask = 0;

      // Convert it to binary
      if (isdigit(digit))
      {
	subWord = (UInt32)digit - (UInt32)'0';
        subWordMask = 0xF;
      }
      else if (isZorX)
      {
	subWord = 0;
      }
      else
      {
	subWord = (UInt32)digit - (UInt32)'A' + 10;
        subWordMask = 0xF;
      }

      // Check if we have less than 4 bits to insert
      SInt32 dataWidth = 4;
      if (bit_width < 4)
      {
	// We have less so mask our data.
	subWord = subWord & ((1 << bit_width) - 1);
	subWordMask = subWordMask & ((1 << bit_width) - 1);
	dataWidth = bit_width;
      }

      // Put it in the right place
      data[wordIndex] |= (subWord << shift);
      if (mask != NULL)
        mask[wordIndex] |= (subWordMask << shift);

      // We moved dataWidth bits
      bit_width -= dataWidth;
      shift += dataWidth;
    }
    else if (digit != '_')
    {
      result = false;
    }
  }

  // Check for size mismatch
  if (result and ((bit_width != 0) or (pos != s - 1)))
  {
    result = false;
  }

  return result;
}

#define BCD_DEC_PRINT 0

#if BCD_DEC_PRINT 
static bool sBinaryToUDecimal(const UInt32* inputData, size_t width, 
                              char* result, size_t len)
{
  BCDConv bcd(width, inputData);
  bcd.encode();
  return bcd.asciify(result, len);
}

#else

// Divide the bigNumber by 10, returning the remainder, and modifying
// numWords to indicate the highest order non-zero word
#define ALL_ZEROS (-1)

static UInt32 s_divide_by_10(UInt32* bigNumber, SInt32* numWords) {
  UInt32 remainder = 0;
  bool foundNonZero = false;
  for (int i = *numWords; i >= 0; --i) {
    UInt32 word = bigNumber[i];
    UInt32 upper = (word >> 16) + (remainder << 16);
    UInt32 lower = word & 0xffff;

    // Do two divisions in base 65536, so we don't overflow
    lower += (upper % 10) << 16;        // carry remainder
    upper /= 10;

    remainder = lower % 10;
    lower /= 10;
    INFO_ASSERT(lower < 65536, "Divide by 10 overflow.");
    INFO_ASSERT(upper < 65536, "Divide by 10 overflow.");
    word = (upper << 16) + lower;
    bigNumber[i] = word;

    if (!foundNonZero && (word != 0)) {
      *numWords = i;
      foundNonZero = true;
    }
  }

  if (!foundNonZero) {
    *numWords = ALL_ZEROS;                    // all zeros left
  }
  
  return remainder;
} // static UInt32 s_divide_by_10

static inline int s_decimal_print(DynBitVector* inputData,
                                   char* result, int len)
{
  // Figure out the index with the most significant one
  // This will print the digits backward

  SInt32 n = inputData->numWords();
  UInt32* bigNumber = inputData->getUIntArray();

  int retVal = 0;

  char* lsb = result + len - 1; // leave room for NUL terminator
  *lsb = '\0';
  --n;
  while (n != ALL_ZEROS) {
    --lsb;
    UInt32 remainder = s_divide_by_10(bigNumber, &n);
    *lsb = (remainder + '0');
  }

  // You can't accurately predict the exact buffer size needed for
  // decimal printing, so we may have wound up a little off.
  if (lsb != result) {
    INFO_ASSERT(lsb > result, result);
    int numChars = result + len - lsb; // includes nul
    retVal += (numChars - 1);
    memmove(result, lsb, numChars);
  }
  else
    retVal = len - 1;
  return retVal;
} // static void s_decimal_print

#endif

int UtConv::BinaryToUnsignedDec(const UInt32* inputData, size_t width, 
                                 char* result, size_t len)
{
#if BCD_DEC_PRINT 
  return sBinaryToUDecimal(inputData, width, result, len);
#else
  int retVal = width;
  if (width > 0)
  {
    UInt32 worstCaseBufsize = sCalcNumOctCharsNeeded(width);
    if (len <= worstCaseBufsize) {
      return -1;                // buffer must be large enough to hold
                                // all possible digits plus trailing null char
    }
    DynBitVector tmp(width, inputData, sGetNumWords(width));
    retVal = s_decimal_print(&tmp, result, len);
  }
  return retVal;
#endif
}


int UtConv::BinaryToSignedDec(const UInt32* inputData, size_t width, 
                              bool showPositive, char* result, size_t len)
{
  int retVal = width;
  if (width > 0)
  {
    bool isNeg = false;
    DynBitVector tmp(width, inputData, sGetNumWords(width));
    if (tmp.test(width - 1)) {
      isNeg = true;
      // 2's complement
      tmp.flip();
      tmp += 1;
    }

    int diff = 1;
    if (isNeg)
      *result = '-';
    else if (showPositive)
      *result = '+';
    else 
      diff = 0;
    
    UInt32 worstCaseBufsize = sCalcNumOctCharsNeeded(width);
    if ((len-diff) <= worstCaseBufsize) {
      // buffer must be large enough to hold all digits, optional sign char, and trailing null char
      return -1;
    }
    
#if BCD_DEC_PRINT  
    retVal = diff + BinaryToUnsignedDec(tmp.getUIntArray(), width, result + diff, len - diff);   
#else
    retVal = diff + s_decimal_print(&tmp, result + diff, len - diff);
#endif
  }
  return retVal;
}

bool UtConv::BinaryStrToHex(UtString* value)
{
  int valSize = int(value->size());
  if (value->find_first_of("xzXZ?") != UtString::npos)
    return false;
  
  // now for the conversion
  int hexIndex = valSize;
  UInt16 nibble = 0;
  int nibblePow = 1;
  for (int i = valSize - 1; i >= 0; --i)
  {
    char p = (*value)[i];   
    INFO_ASSERT((p == '0') || (p == '1'), value->c_str());
    if (p == '1')
      nibble += nibblePow;
    nibblePow <<= 1;
    if (nibblePow == 0x10)
    {
      --hexIndex;
      if (nibble < 10)
        (*value)[hexIndex] = '0' + nibble;
      else
        (*value)[hexIndex] = 'a' + nibble - 10;

      nibble = 0;
      nibblePow = 1;
    }
  }
  if (nibblePow != 1)
  {
    // In the middle of a nibble calculation when we ran out of
    // characters
    --hexIndex;
    (*value)[hexIndex] = '0' + nibble;
  }

  // Remove the binary values.
  value->erase(0, hexIndex);
  return true;
}

bool UtConv::StrToSignedDec(const char* valStr, char** rem, SInt32* value, UtString* errMsg)
{
  INFO_ASSERT(rem, valStr);
  *value = 0;
  SInt32 val = OSStrToS32(valStr, rem, 10, errMsg);
  bool isValid = (*valStr != '\0') && ((**rem == '\0') || (*rem != valStr));
  if (isValid)
    *value = SInt32(val);
  return isValid;
}

void CarbonValRW::cpSrcToDest(UInt32* dest, const UInt8* src, size_t)
{
  dest[0] = *src;
}

void CarbonValRW::cpSrcToDest(UInt32* dest, const UInt16* src, size_t) 
{
  dest[0] = *src;
}

void CarbonValRW::cpSrcToDest(UInt32* dest, const UInt32* src, size_t numWords) 
{
  memcpy (dest, src, numWords * sizeof(UInt32));
}


void CarbonValRW::cpSrcToDest(UInt8* dest, const UInt32* src, size_t)
{
  *dest = (src[0] & 0x000000ff);
}
  
void CarbonValRW::cpSrcToDest(UInt16* dest, const UInt32* src, size_t)
{
  *dest = (src[0] & 0x0000ffff);
}
  

void CarbonValRW::cpSrcToDest(UInt64* dest, const UInt32* src, size_t)
{
  *dest = src[1];
  *dest <<= 32;
  *dest += src[0];
}

void CarbonValRW::cpSrcToDest(UInt64* dest, const UInt64* src, size_t)
{
  *dest = *src;
}
void CarbonValRW::cpSrcToDest(UInt16* dest, const UInt16* src, size_t)
{
  *dest = *src;
}
void CarbonValRW::cpSrcToDest(UInt8* dest, const UInt8* src, size_t)
{
  *dest = *src;
}

void
CarbonValRW::cpSrcToDest(CarbonReal* dest, const UInt64* src, size_t) 
{
  memcpy ( dest, src, sizeof( CarbonReal ));
}

void CarbonValRW::cpSrcToDest(UInt64* dest, const CarbonReal* src, size_t)
{
  memcpy ( dest, src, sizeof( UInt64 ));
}

bool CarbonValRW::typedIsEqual(const UInt8* val1, const UInt8* val2, size_t)
{
  return (*val1 == *val2);
}

bool CarbonValRW::typedIsEqual(const UInt16* val1, const UInt16* val2, size_t)
{
  return (*val1 == *val2);
}

bool CarbonValRW::typedIsEqual(const UInt32* val1, const UInt32* val2, size_t words)
{
  return (CarbonValRW::memCompare(val1, val2, words) == 0);
}

bool CarbonValRW::typedIsEqual(const UInt64* val1, const UInt64* val2, size_t)
{
  return (*val1 == *val2);
}

int CarbonValRW::memCompare(const void* shadow, const UInt8* val, size_t)
{
  UInt32 tmpVal[1];
  tmpVal[0] = *val;
  return memCompare(shadow, tmpVal, 1);
}

int CarbonValRW::memCompare(const void* shadow, const UInt16* val, size_t)
{
  UInt32 tmpVal[1];
  tmpVal[0] = *val;
  return memCompare(shadow, tmpVal, 1);
}

int CarbonValRW::memCompare(const void* shadow, const UInt32* val, size_t numWords)
{
  int ret = memcmp(shadow, static_cast<const void*>(val), numWords * sizeof(UInt32));
  if (ret < 0)
    ret = -1;
  if (ret > 0)
    ret = 1;
  return ret;
}

int CarbonValRW::memCompare(const void* shadow, const UInt64* val, size_t)
{
  UInt32 tmp[2];
  tmp[0] = UInt32(*val & 0x00000000ffffffff);
  tmp[1] = UInt32((*val >> 32) & 0x00000000ffffffff);
  return memCompare(shadow, tmp, 2);
}

void CarbonValRW::cpSrcWordToDest(UInt64* dest, const UInt32 src, size_t wordIndex)
{
  INFO_ASSERT((wordIndex == 0) || (wordIndex == 1), "Invalid wordIndex for 64 bit number.");
  UInt64 srcCp = src;
  UInt64 mask = 0x00000000ffffffff;
  mask = mask << UInt64(32 * (1 - wordIndex));
  *dest &= mask;
  srcCp = srcCp << UInt64(32 * wordIndex);
  *dest += srcCp;
}

void CarbonValRW::cpSrcWordToDest(UInt32* dest, const UInt32 src, size_t wordIndex)
{
  dest[wordIndex] = src;
}

void CarbonValRW::cpSrcWordToDest(UInt16* dest, const UInt32 src, size_t wordIndex)
{
  INFO_ASSERT(wordIndex == 0, "Invalid word index for < 32 bit number.");
  *dest = (src & 0x0000ffff);
}

void CarbonValRW::cpSrcWordToDest(UInt8* dest, const UInt32 src, size_t wordIndex)
{
  INFO_ASSERT(wordIndex == 0, "Invalid word index for < 32 bit number.");
  *dest = (src & 0x000000ff);
}

void CarbonValRW::cpSrcToDestWord(UInt32* dest, const UInt64* src, size_t wordIndex)
{
  INFO_ASSERT((wordIndex == 0) || (wordIndex == 1), "Invalid wordIndex for 64 bit number.");
  if (wordIndex == 0)
    *dest = UInt32(*src & 0x00000000ffffffff);
  else
  {
    UInt64 tmp = UInt64(*src >> 32);
    tmp &= 0x00000000ffffffff;
    *dest = UInt32(tmp);
  }
}

void CarbonValRW::cpSrcToDestWord(UInt32* dest, const UInt32* src, size_t wordIndex)
{
  dest[0] = src[wordIndex];
}

void CarbonValRW::cpSrcToDestWord(UInt32* dest, const UInt16* src, size_t wordIndex)
{
  INFO_ASSERT(wordIndex == 0, "Invalid word index for < 32 bit number.");
  cpSrcToDest(dest, src, 1);
}

void CarbonValRW::cpSrcToDestWord(UInt32* dest, const UInt8* src, size_t wordIndex)
{
  INFO_ASSERT(wordIndex == 0, "Invalid word index for < 32 bit number.");
  cpSrcToDest(dest, src, 1);
}

void CarbonValRW::cpSrcToDestRange(UInt64* dest, const UInt32 *src, 
                                   size_t dstindex, size_t length)
{
  UInt32 temp_dest[2];
  
  temp_dest[0] = UInt32(0x00000000ffffffff & *dest);
  temp_dest[1] = UInt32(0x00000000ffffffff & (*dest >> 32));
  
  cpSrcToDestRange(temp_dest, src, dstindex, length);

  *dest = temp_dest[1];
  *dest = *dest << 32;
  *dest = (*dest & KUInt64(0xffffffff00000000)) | temp_dest[0];
}

void CarbonValRW::cpSrcToDestRange(UInt16* dest, const UInt32 *src, 
                                   size_t dstindex, size_t length)
{
  UInt32 temp_dest = *dest;
  cpSrcToDestRange(&temp_dest, src, dstindex, length);
  *dest = temp_dest & 0x0000ffff;
}

void CarbonValRW::cpSrcToDestRange(UInt8* dest, const UInt32 *src, 
                                   size_t dstindex, size_t length)
{
  UInt32 temp_dest = *dest;
  cpSrcToDestRange(&temp_dest, src, dstindex, length);
  *dest = temp_dest & 0x000000ff;
}

void CarbonValRW::cpSrcToDestRange(UInt32* dest, const UInt32 *src, 
                                   size_t dstindex, size_t length)
{
  if (src == NULL)
    return;

  UtConvRangeInfo info;
  sGetRangeInfo(&info, dstindex, length);
  
  register UInt32 depWordVal = 0;

  { // begin scope for first word set

    // get the value for the first word
    UInt32 exWordVal = dest[info.mStartWordIndex]; 
    depWordVal = exWordVal & info.mBValMask;
    
    {
      UInt32 wordBuf = src[0];
      wordBuf <<= info.mFrontOffset;
      depWordVal |= wordBuf;
    }
    
    // get the last word that will be deposited to.
    if (info.mIsInclusiveWord)
    {
      // Fill back in the original value in the head of the word
      depWordVal &= ~info.mEValMask; // clean dirty bits
      depWordVal |= (exWordVal & info.mEValMask);
    }
    
    dest[info.mStartWordIndex] = depWordVal;
  } // end scope for first word set

  // The destination buffer is now aligned.

  ++info.mStartWordIndex;
  
  register UInt32 carryOverMaskVal;
  register bool calcCarryOver = (info.mBackOffset < scWordBitSize);

  register size_t i = 0; 
  for (;
       (info.mStartWordIndex < info.mLastWordIndex); 
       ++info.mStartWordIndex)
  {
    ++i;
    // avoid portability issue with shift >= 32 bits | 64 bits
    carryOverMaskVal = 0;
    if (calcCarryOver)
      carryOverMaskVal = src[i - 1] >> info.mBackOffset;
    
    depWordVal = (src[i] << info.mFrontOffset) | carryOverMaskVal;
    
    dest[info.mStartWordIndex] = depWordVal;
  }
  
  if (! info.mIsInclusiveWord)
  {
    carryOverMaskVal = 0;
    if (calcCarryOver)
    {
      carryOverMaskVal = src[i] >> info.mBackOffset;
      /* To avoid reading uninitialized memory, we need to check if
         the current value in the destination needs to be updated with
         part of the value of the next word from the source. We are
         now in the LastWordIndex, so we must not include that when
         calculating the left over length (hence, minus 1).
      */
      size_t leftOverLength = length - info.mBackOffset - scWordBitSize * (info.mNumIndices - 1);
      if (leftOverLength > info.mFrontOffset)
        carryOverMaskVal |= (src[i + 1] << info.mFrontOffset);
    }
    else
      carryOverMaskVal = src[i + 1];
    
    dest[info.mLastWordIndex] &= info.mEValMask; // clean bits to be assigned.
    dest[info.mLastWordIndex] |= (carryOverMaskVal & ~info.mEValMask);
  }
}

void CarbonValRW::cpSrcWordToDest(UInt32* dest, const UInt64 src, size_t wordIndex)
{
  INFO_ASSERT((wordIndex == 0) || (wordIndex == 1), "Invalid wordIndex for 64 bit number.");
  UInt64 srcCp = src;
  srcCp >>= (32 * wordIndex);
  *dest = UInt32(0x00000000ffffffff & srcCp);
}

void CarbonValRW::cpSrcWordToDest(UInt32* dest, const UInt16 src, size_t wordIndex)
{
  INFO_ASSERT(wordIndex == 0, "Invalid word index for < 32 bit number.");
  *dest = src;
  *dest &= 0x0000ffff;
}

void CarbonValRW::cpSrcWordToDest(UInt32* dest, const UInt8 src, size_t wordIndex)
{
  INFO_ASSERT(wordIndex == 0, "Invalid word index for < 32 bit number.");
  *dest = src;
  *dest &= 0x000000ff;
}

void CarbonValRW::cpSrcRangeToDest(UInt32* dest, DynBitVector* src_bv_ptr,
                                   size_t srcindex,  size_t length)
{
  DynBitVector& src_bv = *src_bv_ptr;
  src_bv >>= srcindex;
  
  UInt32* srcCp = src_bv.getUIntArray();
  
  int numFullWords = length / 32;
  memcpy(dest, srcCp, numFullWords * sizeof(UInt32));
  
  size_t rem = (length % 32);
  if (rem != 0) {
    UInt32 mask = 0;
    mask = ~mask;
    mask <<= rem;
    
    UInt32 lword = (srcCp[numFullWords] & ~mask);
    
    dest[numFullWords] &= mask;
    dest[numFullWords] |= lword;
  }
}

void CarbonValRW::cpSrcRangeToDest(UInt32* dest, const UInt32* src, 
                                   size_t srcindex, size_t length)
{
  if (length > 0)
    cpSrcRangeToDestRange(dest, 0, src, srcindex, length);
}

void CarbonValRW::cpSrcRangeToDest(UInt32* dest, const UInt64* src, 
                                   size_t srcindex, size_t length)
{
  UInt32 temp_src[2];
  temp_src[0] = UInt32(0x00000000ffffffff & *src);
  temp_src[1] = UInt32(0x00000000ffffffff & (*src >> 32));
  cpSrcRangeToDest(dest, temp_src, srcindex, length);
}
  
void CarbonValRW::cpSrcRangeToDest(UInt32* dest, const UInt16* src, 
                                   size_t srcindex, size_t length)
{
  UInt32 temp_src = *src;
  cpSrcRangeToDest(dest, &temp_src, srcindex, length);
}

void CarbonValRW::cpSrcRangeToDest(UInt32* dest, const UInt8* src, 
                                   size_t srcindex, size_t length)
{
  UInt32 temp_src = *src;
  cpSrcRangeToDest(dest, &temp_src, srcindex, length);
}

void CarbonValRW::cpSrcRangeToDestRange(UInt32* dst, size_t dstindex, 
                                        const UInt32* src, size_t srcindex,
                                        size_t length)
{
  size_t srcWordIndex = srcindex/scWordBitSize;
  size_t srcBitIndex = srcindex % scWordBitSize;  
  
  if (srcBitIndex == 0)
  {
    // pass it to cpSrcToDestRange, which is a little more optimal to
    // do it now
    cpSrcToDestRange(dst, &(src[srcWordIndex]), dstindex, length);
    return;
  }
  
  size_t dstWordIndex = dstindex/scWordBitSize;
  size_t dstLastWordIndex = (dstindex + length - 1)/scWordBitSize;
  size_t dstBitIndex = dstindex % scWordBitSize;
  size_t lastShiftOffset = (dstindex + length) % scWordBitSize;  

  if (srcBitIndex < dstBitIndex)
  {
    int shiftAmt = dstBitIndex - srcBitIndex;
    size_t dstBackOffset = (scWordBitSize - dstBitIndex);

    // Figure out the masks for the source and destination initially
    UInt32 dstMask;
    UInt32 tmpSrcMask;
    if (length >= scWordBitSize) {
      // At least the size of a word so make a mask with all FFs
      dstMask = ~0u;
      tmpSrcMask = ~0u;
    } else {
      // Less than the size of word we can take the maxValue-1 to make a mask
      dstMask = (0x1 << length) - 1;
      tmpSrcMask = (0x1 << length) - 1;
    }
    dstMask <<= dstBitIndex;
    dstMask = ~dstMask;
    tmpSrcMask <<= srcBitIndex;

    // Grab the source data masked out
    register UInt32 srcWord = src[srcWordIndex] & tmpSrcMask;
    
    // Apply the initial copy
    if (length < dstBackOffset) {
      length = 0;
    } else {
      length -= dstBackOffset;
    }
    dst[dstWordIndex] &= dstMask;
    dst[dstWordIndex] |= (srcWord << shiftAmt);
    
    // Loop over the remaining portions if there are any
    if (length > 0)
    {
      size_t srcShift = srcBitIndex + dstBackOffset;
      size_t indexLimit = dstLastWordIndex;
      if (lastShiftOffset != 0)
        --indexLimit;
      
      for (++dstWordIndex, ++srcWordIndex; dstWordIndex <= indexLimit; ++dstWordIndex, ++srcWordIndex)
      {
        srcWord >>= srcShift;
        dst[dstWordIndex] = srcWord | (src[srcWordIndex] << shiftAmt);
        srcWord = src[srcWordIndex];
      }

      // The last remaining portion
      if (lastShiftOffset != 0)
      {
        UInt32 eValMask = ~0u;
        eValMask <<= lastShiftOffset;
        dst[dstWordIndex] &= eValMask;
        srcWord >>= srcShift;
        UInt32 lastWord = srcWord | (src[srcWordIndex] << shiftAmt);
        lastWord &= ~eValMask;
        dst[dstWordIndex] |= lastWord;
      }
      
    }
  }
  else if (srcBitIndex > dstBitIndex)
  {
    UInt32 srcMask = ~0u;
    srcMask <<= srcBitIndex;
    
    size_t srcBackOffset = (scWordBitSize - srcBitIndex);
    
    if (length < srcBackOffset)
    {
      // small copy size. Need to fix the source mask to keep
      // extra bits.
      UInt32 tmpMask = ~0u;
      tmpMask >>= (srcBackOffset - length);
      srcMask &= tmpMask;
      length = 0;
    }
    else
      length -= srcBackOffset;
    
    UInt32 srcWord = src[srcWordIndex] & srcMask;
    
    size_t shiftAmt = srcBitIndex - dstBitIndex;
    srcWord >>= shiftAmt;
    srcMask >>= shiftAmt;
    
    srcMask = ~srcMask;
    // zero bits that will be overwritten
    dst[dstWordIndex] &= srcMask;
    dst[dstWordIndex] |= srcWord;
    
    if (length > 0)
    {
      ++srcWordIndex;
      CarbonValRW::cpSrcToDestRange(dst, &(src[srcWordIndex]), 
                                    dstindex + srcBackOffset, 
                                    length);
    }
  }
  else
    CarbonValRW::copyRange(&(dst[dstWordIndex]), &(src[srcWordIndex]), 
                           dstBitIndex, length);
}


size_t CarbonValRW::calcRangeIndex(int range_msb, int range_lsb, 
                                   int msb, int lsb)
{
  size_t index;
  if (msb >= lsb)
  {
    INFO_ASSERT(range_msb >= range_lsb, "Incorrect order for specified msb/lsb");
    INFO_ASSERT(range_msb <= msb, "Specified msb is out-of-bounds.");
    INFO_ASSERT(range_lsb >= lsb, "Specified lsb is out-of-bounds.");
    index = range_lsb - lsb;
  }
  else
  {
    INFO_ASSERT(range_msb <= range_lsb, "Incorrect order for specified msb/lsb");
    INFO_ASSERT(range_msb >= msb, "Specified msb is out-of-bounds.");
    INFO_ASSERT(range_lsb <= lsb, "Specified lsb is out-of-bounds.");

    index = lsb - range_lsb;
  }
  
  return index;
}

int CarbonValRW::memCompareRange(const UInt32* src, const UInt8* val, 
                                 size_t bitindex, size_t bitlength)
{
  UInt32 tmpVal = *val;
  return memCompareRange(src, &tmpVal, bitindex, bitlength);
}

int CarbonValRW::memCompareRange(const UInt32* src, const UInt16* val, 
                                 size_t bitindex,  size_t bitlength)
{
  UInt32 tmpVal = *val;
  return memCompareRange(src, &tmpVal, bitindex, bitlength);
}

int CarbonValRW::memCompareRange(const UInt32* src, const UInt32* val, 
                                 size_t bitindex, size_t bitlength)
{
  int ret = 0;
  if (bitlength > 0)
  {
    if ((bitlength + bitindex) <= 32)
    {
      UInt32 tmpVal = *val;
      tmpVal >>= bitindex;
      
      UInt32 highmask = 0;
      highmask = ~highmask;

      // Shift by 32 or greater does not necessarily do the right
      // thing (implementation defined), so force it to do so.
      // The bidi tests with bit length of 31 found this bug.
      if ((bitindex + bitlength) >= 32) {
	highmask = 0;
      } else {
	highmask <<= bitlength;
      }

      highmask = ~highmask;
      
      ret = ((*src & highmask) - (tmpVal & highmask));
    }
    else
    {

      DynBitVector val_bv(bitindex + bitlength, val, 
                          (bitindex + bitlength + 31)/32);
      
      val_bv >>= bitindex;
      val_bv.resize(bitlength);
      
      const UInt32* valArr = val_bv.getUIntArray();

      size_t numWordsCmp = (bitlength + 31)/32 - 1;
      for (size_t i = 0; (i < numWordsCmp) && (ret == 0); ++i)
        ret = valArr[i] - src[i];

      if (ret == 0)
      {
        size_t remainder = bitlength % 32;
        UInt32 mask = 0;
        mask = ~mask;
        if (remainder > 0)
        {
          mask <<= remainder;
          mask = ~mask;
        }
        ret = valArr[numWordsCmp] - (src[numWordsCmp] & mask);
      }
    }
  }
  
  if (ret < 0)
    ret = -1;
  else if (ret > 0)
    ret = 1;
  return ret;
}

int CarbonValRW::memCompareRange(const UInt32* src, const UInt64* val, 
                                 size_t bitindex, size_t bitlength)
{
  UInt32 tmp[2];
  tmp[0] = UInt32(*val & 0x00000000ffffffff);
  tmp[1] = UInt32((*val >> 32) & 0x00000000ffffffff);
  return memCompareRange(src, tmp, bitindex, bitlength);
}

void CarbonValRW::writeBin4ToStr(UtString* valBuf, const DynBitVector* value, 
                                 const DynBitVector* mask)
{
  if (mask)
    INFO_ASSERT(value->size() == mask->size(), "Value and drive have different widths.");

  UInt32 bitwidth = value->size();
  UInt32 bufsiz = bitwidth + 1;
  char* tmpBuf = CARBON_ALLOC_VEC(char, bufsiz);
  int convToStrOK = writeBinValToStr(tmpBuf, bufsiz, value->getUIntArray(), bitwidth);
  INFO_ASSERT(convToStrOK != -1, "Failed to convert to binary.");
  *valBuf = tmpBuf;
  CARBON_FREE_VEC(tmpBuf, char, bufsiz);

  // now using the mask replace the 0's or 1's with z's or x's, respectively.
  if (mask && mask->any())
  {
    for(UInt32  pos = bitwidth-1, index = 0 ; index < bitwidth; --pos, ++index)
    {
      if (mask->test (index))
      {
        (*valBuf)[pos] =  value->test (index) ? 'x' : 'z';
      }
    }
  }
}

bool CarbonValRW::setRangeToZero(UInt32* val, size_t bitIndex, size_t rangeLength)
{
  bool changed = false;
  UtConvRangeInfo info;
  sGetRangeInfo(&info, bitIndex, rangeLength);
  
  // Clear out the appropriate bits of the first word
  UInt32 exWordVal = val[info.mStartWordIndex];
  
  UInt32 depWordVal = exWordVal & info.mBValMask;

  if (info.mIsInclusiveWord)
  {
    INFO_ASSERT(info.mNumRangeWords == 1, "Expected 1 word.");
    // Fill back in the original value in the head of the word
    depWordVal &= ~info.mEValMask; // clean dirty bits
    depWordVal |= (exWordVal & info.mEValMask);
  }
  
  changed = val[info.mStartWordIndex] != depWordVal;
  val[info.mStartWordIndex] = depWordVal;
  ++info.mStartWordIndex;
  
  if (! info.mIsInclusiveWord)
  {
    // clear out the appropriate part of the last word
    depWordVal = val[info.mLastWordIndex];
    depWordVal &= info.mEValMask;
    changed = changed || (val[info.mLastWordIndex] != depWordVal);
    val[info.mLastWordIndex] = depWordVal;

    // Clear everything in between
    for (size_t i = 1; 
         i < info.mNumIndices;
         ++i, ++info.mStartWordIndex)
    {
      changed = changed || (val[i] != 0);
      val[i] = 0;
    }
  }
  return changed;
}

bool CarbonValRW::setRangeToOnes(UInt32* val, size_t bitIndex, size_t rangeLength)
{
  bool changed = false;
  UtConvRangeInfo info;
  sGetRangeInfo(&info, bitIndex, rangeLength);
  
  // Set the appropriate bits of the first word
  UInt32 exWordVal = val[info.mStartWordIndex];
  UInt32 depWordVal = 0;
  if (info.mIsInclusiveWord)
  {
    INFO_ASSERT(info.mNumRangeWords == 1, "Expected 1 word.");
    // Combine the two masks for the 1 word
    UInt32 combinedMask = info.mEValMask | info.mBValMask;
    // Now, invert it and or it to the original word.
    depWordVal |= (exWordVal | ~combinedMask);
  }
  else
    depWordVal = exWordVal | ~info.mBValMask;
  
  val[info.mStartWordIndex] = depWordVal;
  changed = val[info.mStartWordIndex] != exWordVal;

  ++info.mStartWordIndex;
  
  if (! info.mIsInclusiveWord)
  {
    // set out the appropriate part of the last word
    exWordVal = val[info.mLastWordIndex];
    val[info.mLastWordIndex] |= ~info.mEValMask;

    changed = changed || (val[info.mLastWordIndex] == exWordVal);
    // set everything in between
    for (size_t i = 1; 
         i < info.mNumIndices;
         ++i, ++info.mStartWordIndex)
    {
      val[i] = UtUINT32_MAX;
      changed = changed || (val[i] != UtUINT32_MAX);
    }
  }
  return changed;
}

void CarbonValRW::setToZero(UInt32* val, size_t numWords)
{
  memset(val, 0, numWords * sizeof(UInt32));
}

void CarbonValRW::setToOnes(UInt32* val, size_t numWords)
{
  memset(val, 0xffffffff, numWords * sizeof(UInt32));
}

bool CarbonValRW::isZero(const UInt32* val, size_t numWords)
{
  for (size_t i = 0; i < numWords; ++i)
  {
    if (val[i] != 0)
      return false;
  }
  return true;
}

bool CarbonValRW::isAllOnes(const UInt32* val, UInt32 width)
{
  UInt32 divWords = (width - 1)/(sizeof(UInt32) * 8);
  // does index 0 to numWords - 2
  for (UInt32 i = 0; i < divWords; ++i)
  {
    if (val[i] != 0xffffffff)
      return false;
  }
  // does numWords - 1
  if (val[divWords] != getWordMask(width))
    return false;
  return true;
}

UInt32 CarbonValRW::getWordMask(UInt32 numBits)
{
  return getPrimBitMaskUnused(numBits);
}

UInt64 CarbonValRW::getWordMaskLL(UInt64 numBits)
{
  return getPrimBitMaskUnused(numBits);
}

UInt32 CarbonValRW::getRangeBitWidth(SInt32 range_msb, SInt32 range_lsb)
{
  return std::abs(range_msb - range_lsb) + 1;
}

UInt32 CarbonValRW::getRangeNumUInt32s(SInt32 range_msb, SInt32 range_lsb)
{
  size_t bitWidth = getRangeBitWidth(range_msb, range_lsb);
  static const size_t wordSize = sizeof(UInt32) * 8;
  return (bitWidth + (wordSize - 1)) / wordSize;
}

void CarbonValRW::copyRange(UInt32* dest, const UInt32 *src, 
                            size_t dstindex, size_t length)
{
  UtConvRangeInfo info;
  sGetRangeInfo(&info, dstindex, length);
  
  // get the value for the first word
  UInt32 exWordVal = dest[info.mStartWordIndex]; 
  
  UInt32 wordBuf = 0;
  UInt32 depWordVal = 0;
  
  depWordVal = exWordVal & info.mBValMask;
  wordBuf = src[info.mStartWordIndex];
  wordBuf &= ~info.mBValMask;
  depWordVal |= wordBuf;
  
  if (info.mIsInclusiveWord)
  {
    // Fill back in the original value in the head of the word
    depWordVal &= ~info.mEValMask; // clean dirty bits
    depWordVal |= (exWordVal & info.mEValMask);
  }
  
  dest[info.mStartWordIndex] = depWordVal;
  
  // get the last word that will be deposited to.
  if (! info.mIsInclusiveWord)
  {
    ++info.mStartWordIndex;

    // get the last word that will be deposited to.
    UInt32 lastWordVal = dest[info.mLastWordIndex];
    
    
    for (; 
         (info.mStartWordIndex < info.mLastWordIndex); 
         ++info.mStartWordIndex)
      dest[info.mStartWordIndex] = src[info.mStartWordIndex];
    
    
    depWordVal = src[info.mLastWordIndex];
    depWordVal &= ~info.mEValMask; // clean dirty bits
    depWordVal |= (lastWordVal & info.mEValMask); 
    dest[info.mLastWordIndex] = depWordVal;
  }
}
