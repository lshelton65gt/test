%{
/******************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include <stdio.h>
#include "CExprPriv.h"
#include "CExprBison.hxx"
void count();

#define DEBUGGING 0
#if DEBUGGING
#else
#define debug(x)
#endif

#if pfWINDOWS
#define YY_NO_UNISTD_H
#define isatty _isatty
extern "C" int isatty(int);
#endif

//#define YY_INPUT(buf, result, max_size) carbonCExpr_YYInput(buf, &result, max_size)
//#define YY_NO_UNPUT

%}


D			[0-9]
L			[a-zA-Z_]
H			[a-fA-F0-9]
E			[Ee][+-]?{D}+
FS			(f|F|l|L)
IS			(u|U|l|L)*

%%
"/*"			{ carbonCExprComment(); }

"auto"			{ carbonCExprCount(); return(AUTO); }
"break"			{ carbonCExprCount(); return(BREAK); }
"case"			{ carbonCExprCount(); return(CASE); }
"char"			{ carbonCExprCount(); return(CHAR); }
"const"			{ carbonCExprCount(); return(CONST); }
"continue"		{ carbonCExprCount(); return(CONTINUE); }
"default"		{ carbonCExprCount(); return(DEFAULT); }
"do"			{ carbonCExprCount(); return(DO); }
"double"		{ carbonCExprCount(); return(DOUBLE); }
"else"			{ carbonCExprCount(); return(ELSE); }
"enum"			{ carbonCExprCount(); return(ENUM); }
"extern"		{ carbonCExprCount(); return(EXTERN); }
"float"			{ carbonCExprCount(); return(FLOAT); }
"for"			{ carbonCExprCount(); return(FOR); }
"goto"			{ carbonCExprCount(); return(GOTO); }
"if"			{ carbonCExprCount(); return(IF); }
"int"			{ carbonCExprCount(); return(INT); }
"long"			{ carbonCExprCount(); return(LONG); }
"register"		{ carbonCExprCount(); return(REGISTER); }
"return"		{ carbonCExprCount(); return(RETURN); }
"short"			{ carbonCExprCount(); return(SHORT); }
"signed"		{ carbonCExprCount(); return(SIGNED); }
"sizeof"		{ carbonCExprCount(); return(SIZEOF); }
"static"		{ carbonCExprCount(); return(STATIC); }
"struct"		{ carbonCExprCount(); return(STRUCT); }
"switch"		{ carbonCExprCount(); return(SWITCH); }
"typedef"		{ carbonCExprCount(); return(TYPEDEF); }
"union"			{ carbonCExprCount(); return(UNION); }
"unsigned"		{ carbonCExprCount(); return(UNSIGNED); }
"void"			{ carbonCExprCount(); return(VOID); }
"volatile"		{ carbonCExprCount(); return(VOLATILE); }
"while"			{ carbonCExprCount(); return(WHILE); }

{L}({L}|{D})*		{ carbonCExprCount(); return(carbonCExprCheckType()); }

0[xX]{H}+{IS}?		{ carbonCExprCount(); return(CONSTANT); }
0{D}+{IS}?		{ carbonCExprCount(); return(CONSTANT); }
{D}+{IS}?		{ carbonCExprCount(); return(CONSTANT); }
'(\\.|[^\\'])+'		{ carbonCExprCount(); return(CONSTANT); }

{D}+{E}{FS}?		{ carbonCExprCount(); return(CONSTANT); }
{D}*"."{D}+({E})?{FS}?	{ carbonCExprCount(); return(CONSTANT); }
{D}+"."{D}*({E})?{FS}?	{ carbonCExprCount(); return(CONSTANT); }

\"(\\.|[^\\"])*\"	{ carbonCExprCount(); return(STRING_LITERAL); }

">>="			{ carbonCExprCount(); return(RIGHT_ASSIGN); }
"<<="			{ carbonCExprCount(); return(LEFT_ASSIGN); }
"+="			{ carbonCExprCount(); return(ADD_ASSIGN); }
"-="			{ carbonCExprCount(); return(SUB_ASSIGN); }
"*="			{ carbonCExprCount(); return(MUL_ASSIGN); }
"/="			{ carbonCExprCount(); return(DIV_ASSIGN); }
"%="			{ carbonCExprCount(); return(MOD_ASSIGN); }
"&="			{ carbonCExprCount(); return(AND_ASSIGN); }
"^="			{ carbonCExprCount(); return(XOR_ASSIGN); }
"|="			{ carbonCExprCount(); return(OR_ASSIGN); }
">>"			{ carbonCExprCount(); return(RIGHT_OP); }
"<<"			{ carbonCExprCount(); return(LEFT_OP); }
"++"			{ carbonCExprCount(); return(INC_OP); }
"--"			{ carbonCExprCount(); return(DEC_OP); }
"->"			{ carbonCExprCount(); return(PTR_OP); }
"&&"			{ carbonCExprCount(); return(AND_OP); }
"||"			{ carbonCExprCount(); return(OR_OP); }
"<="			{ carbonCExprCount(); return(LE_OP); }
">="			{ carbonCExprCount(); return(GE_OP); }
"=="			{ carbonCExprCount(); return(EQ_OP); }
"!="			{ carbonCExprCount(); return(NE_OP); }
";"			{ carbonCExprCount(); return(';'); }
"{"			{ carbonCExprCount(); return('{'); }
"}"			{ carbonCExprCount(); return('}'); }
","			{ carbonCExprCount(); return(','); }
":"			{ carbonCExprCount(); return(':'); }
"="			{ carbonCExprCount(); return('='); }
"("			{ carbonCExprCount(); return('('); }
")"			{ carbonCExprCount(); return(')'); }
"["			{ carbonCExprCount(); return('['); }
"]"			{ carbonCExprCount(); return(']'); }
"."			{ carbonCExprCount(); return('.'); }
"&"			{ carbonCExprCount(); return('&'); }
"!"			{ carbonCExprCount(); return('!'); }
"~"			{ carbonCExprCount(); return('~'); }
"-"			{ carbonCExprCount(); return('-'); }
"+"			{ carbonCExprCount(); return('+'); }
"*"			{ carbonCExprCount(); return('*'); }
"/"			{ carbonCExprCount(); return('/'); }
"%"			{ carbonCExprCount(); return('%'); }
"<"			{ carbonCExprCount(); return('<'); }
">"			{ carbonCExprCount(); return('>'); }
"^"			{ carbonCExprCount(); return('^'); }
"|"			{ carbonCExprCount(); return('|'); }
"?"			{ carbonCExprCount(); return('?'); }

[ \t\v\n\f]		{ carbonCExprCount(); }
.			{ carbonCExpr_error("bad character"); }

%%

int yywrap()
{
	return(1);
}

SInt32 carbonCExprCheckType()
{
/*
* pseudo code --- this is what it should check
*
*	if (yytext == type_name)
*		return(TYPE_NAME);
*
*	return(IDENTIFIER);
*/

/*
*	it actually will only return IDENTIFIER
*/

  carbonCExprDeclareIdent(yytext);
	return(IDENTIFIER);
}


void carbonCExprComment() {
  char c, c1;

loop:
  while ((c = yyinput()) != '*' && c != 0)
    ;

  if ((c1 = yyinput()) != '/' && c != 0) {
    unput(c1);
    goto loop;
  }
}
