// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/UtLibXmlWriter.h"

#define DEFAULT_XML_ENCODING "ISO-8859-1"

/*!
  \file 
  Implementation of UtLibXmlWriter class
*/ 

UtLibXmlWriter::UtLibXmlWriter():
  mXmlWriter(NULL),
  mWritePointers(false)
{
  mXmlEncoding = DEFAULT_XML_ENCODING;
}

UtLibXmlWriter::UtLibXmlWriter(const char* xmlEncoding):
  mXmlWriter(NULL),
  mWritePointers(false)
{
  if(xmlEncoding && (strlen(xmlEncoding) != 0)) {
    mXmlEncoding =   xmlEncoding;
  }
  else {
    // Start the document with the ISO-8859-1 encoding
    mXmlEncoding =   DEFAULT_XML_ENCODING;
  }
}

UtLibXmlWriter::~UtLibXmlWriter()
{
  if(mXmlWriter != NULL) {
    xmlFreeTextWriter(mXmlWriter);
    mXmlWriter = NULL;
  }
}

void 
UtLibXmlWriter::setWritePointers(bool state)
{
  mWritePointers = state;
}


bool
UtLibXmlWriter::getWritePointers()
{
  return mWritePointers;
}


void
UtLibXmlWriter::StartElement( UtString& name)
{
  int rc = xmlTextWriterStartElement(mXmlWriter, BAD_CAST name.c_str());

  INFO_ASSERT(rc >= 0, "Could not start element");
}

void
UtLibXmlWriter::StartElement( const char*  name)
{
  int rc = xmlTextWriterStartElement(mXmlWriter, BAD_CAST name);

  INFO_ASSERT(rc >= 0, "Could not start element");
}


void
UtLibXmlWriter::EndElement()
{
  int rc = xmlTextWriterEndElement(mXmlWriter);

  INFO_ASSERT(rc >= 0, "Could not end element");
}

void
UtLibXmlWriter::WriteAttribute(UtString& name, UtString& value)
{
  WriteAttribute(name.c_str(), value.c_str());
}


void
UtLibXmlWriter::WriteAttribute(const char* name, UtString& value)
{
  WriteAttribute(name, value.c_str());
}


void
UtLibXmlWriter::WriteAttribute(UtString& name, const char* value)
{
  WriteAttribute(name.c_str(), value);
}


void
UtLibXmlWriter::WriteAttribute(const char*  name, const char* value)
{
  int rc = xmlTextWriterWriteAttribute(mXmlWriter, BAD_CAST name, BAD_CAST value);

  INFO_ASSERT(rc >= 0, "Could not write attribute");
}


void
UtLibXmlWriter::WriteAttribute(const char*  name, SInt32 value)
{
  char xmlValue[64];
  for(unsigned i = 0 ; i < sizeof(xmlValue); i++)
  {
    xmlValue[i] = 0;
  }
  sprintf(xmlValue,"%d", value);

  int rc = xmlTextWriterWriteAttribute(mXmlWriter, BAD_CAST name, BAD_CAST xmlValue);

  INFO_ASSERT(rc >= 0, "Could not write attribute");
}


void
UtLibXmlWriter::WriteAttribute(const char*  name, const void* value)
{
  char xmlValue[64];
  if(value == NULL) {
    sprintf(xmlValue,"NULL");
  }
  else {
    if(mWritePointers) {
      sprintf(xmlValue,"%p", value);
    }
    else {
      sprintf(xmlValue,"VALID");
    }
  }

  int rc = xmlTextWriterWriteAttribute(mXmlWriter, BAD_CAST name, BAD_CAST xmlValue);

  INFO_ASSERT(rc >= 0, "Could not write attribute");
}



void
UtLibXmlWriter::WriteAttribute(const char*  name, bool value)
{
  char xmlValue[32];
  if(value) {
    sprintf(xmlValue,"true");
  }
  else {
    sprintf (xmlValue,"false");
  }

  int rc = xmlTextWriterWriteAttribute(mXmlWriter, BAD_CAST name, BAD_CAST xmlValue);

  INFO_ASSERT(rc >= 0, "Could not write attribute");
}


void
UtLibXmlWriter::WriteElement(UtString& name, UtString& value)
{
  WriteElement(name.c_str(), value.c_str());
}

void
UtLibXmlWriter::WriteElement(const char*  name, UtString& value)
{
  WriteElement(name, value.c_str());
}


void
UtLibXmlWriter::WriteElement(UtString& name, const char* value)
{
  WriteElement(name.c_str(), value);
}

void
UtLibXmlWriter::WriteElement(const char* name, const char* value)
{
  int rc = xmlTextWriterWriteElement(mXmlWriter, BAD_CAST name, BAD_CAST value);

  INFO_ASSERT(rc >= 0, "Could not write attribute");
}


void
UtLibXmlWriter::StartWriter(UtString& targetFileName)
{
  StartWriter(targetFileName.c_str());
}


void
UtLibXmlWriter::StartWriter(const char* targetFileName)
{
  if(mXmlWriter != NULL) {
    xmlFreeTextWriter(mXmlWriter);
    mXmlWriter = NULL;
  }

  /* Create a new XmlWriter for targetFileName with no compression. */
  mXmlWriter  = xmlNewTextWriterFilename(targetFileName, 0);
  INFO_ASSERT(mXmlWriter, "Could not create XML writer.");
  int rc = xmlTextWriterStartDocument(mXmlWriter, NULL, mXmlEncoding.c_str(), NULL);
  INFO_ASSERT(rc >= 0, "Could not start XML document");
  xmlTextWriterSetIndent(mXmlWriter, 2);

}



void
UtLibXmlWriter::EndWriter()
{

  INFO_ASSERT(mXmlWriter, "Could not end XML writer.");
  int rc = xmlTextWriterEndDocument(mXmlWriter);
  
  INFO_ASSERT(rc >= 0, "Could not end XML document");
  rc =  xmlTextWriterFlush(mXmlWriter);
  INFO_ASSERT(rc >= 0, "Could not flush XML writer");  
  xmlFreeTextWriter(mXmlWriter);
  mXmlWriter = NULL;

}



