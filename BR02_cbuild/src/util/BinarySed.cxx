//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// Simple C++ program to read a binary file, do a string
// substitution, and write it out.  This does not use Carbon
// util libraries because it is needed in order to build
// those libraries!

#include <stdio.h>
#include <string>
#include <unistd.h>

int main(int argc, char** argv) {
  if (argc != 3) {
    return 1;
  }
  std::string from(argv[1]);
  const char* to = argv[2];

  // Read the entire file, to be sure we don't split the search-string
  // across buffer boundaries.  Use unbuffered input because we are just
  // going to make a big buffer for the entire file anyway, so there is
  // not much performance benefit in letting the system buffer for us.
  std::string filebuf;
  char buf[100000];
  int nread;
  int in_fd = fileno(stdin);
  while ((nread = read(in_fd, buf, 100000)) > 0) {
    filebuf.append(buf, nread);
  }

  // Walk through the file, writing out substitutions.  Use
  // buffered output because we may be writing relatively small
  // chunks
  size_t prev = 0;
  size_t pos = 0;
  size_t to_len = strlen(to);
  while ((pos = filebuf.find(from, prev)) != std::string::npos) {
    // Write out the contents of the file before the substution
    fwrite(filebuf.c_str() + prev, 1, pos - prev, stdout);
    fwrite(to, 1, to_len, stdout);
    prev = pos + from.size();
  }
  fwrite(filebuf.c_str() + prev, 1, filebuf.size() - prev, stdout);
  return 0;
} // int main
