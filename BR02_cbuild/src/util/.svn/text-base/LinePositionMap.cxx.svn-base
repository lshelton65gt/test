// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2003-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.
*/

#include "util/LinePositionMap.h"
#include "util/UtHashMap.h"
#include "util/UtIStream.h"

LinePositionMap::LinePositionMap() {
  mFilePosMap = new FilePosMap;
}

//! dtor
LinePositionMap::~LinePositionMap() {
  delete mFilePosMap;
}

//! find a position for seeking, given a filename and line number
bool LinePositionMap::getPosition(const char* filename, UInt32 line_number,
                                  UInt64* position, UtString* errmsg,
                                  UtIStream* file)
{
  PosMap& pos_map = (*mFilePosMap)[filename];
  if (pos_map.empty()) {
    bool close_file = true;
    if (file == NULL) {
      file = new UtIBStream(filename);
    }
    else {
      close_file = false;
    }
    if (! file->is_open()) {
      *errmsg = file->getErrmsg();
      if (close_file) {
        delete file;
      }
      return false;
    }

    // Rewind the file and map out all the positions
    UtString line_buf;
    file->seek(0, UtIO::seek_set);
    UInt64 pos = 0;
    for (UInt32 line = 1; file->getline(&line_buf); ++line) {
      pos_map[line] = pos;
      pos = file->tell();
    }
  }

  PosMap::iterator p = pos_map.find(line_number);
  if (p == pos_map.end()) {
    *errmsg << "File " << filename << ": Line " << line_number
            << " out of range";
    return false;
  }
  *position = p->second;
  return true;
} // bool LinePositionMap::getPosition
