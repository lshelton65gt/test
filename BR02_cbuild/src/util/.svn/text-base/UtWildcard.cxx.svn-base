// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtWildcard.h"
#include "util/UtString.h"

static bool sIsWild(char ch)
{
  return (ch == '*') || (ch == '?');
}

UtWildcard::UtWildcard(const char* wildcardExpr, const bool honour_escaped_identifiers,
                       bool wildcardMatchesDot)
{
  putExpr(wildcardExpr, honour_escaped_identifiers, wildcardMatchesDot);
}

void UtWildcard::putExpr(const char* wildcardExpr, const bool honour_escaped_identifiers,
                         bool wildcardMatchesDot)
{
  mWildcardMatchesDot = wildcardMatchesDot;
  mTokens.clear();
  UtString buf;
  if (honour_escaped_identifiers && *wildcardExpr == '\\') {
    // It is an escaped identifier, and we are honouring escaped identifiers,
    // so just push the string onto the token list. It does not start with * or
    // ? so the isMatch will always match the single token with strncmp.
    mTokens.push_back (wildcardExpr);
  } else {
    for (const char* s = wildcardExpr; *s != '\0'; ++s)
      {
        if (sIsWild(*s))
          {
            if (!buf.empty())
              {
                mTokens.push_back(buf.c_str());
                buf.clear();
              }

            if (*s == '*')
              {
                // Do not put two * in a row.  Just ignore the second *.
                if (mTokens.empty() || (mTokens[mTokens.size() - 1][0] != '*'))
                  mTokens.push_back("*");
              }
            else
              mTokens.push_back("?");
          }
        else
          buf << *s;
      } // for
    if (!buf.empty())
      mTokens.push_back(buf.c_str());
  }
} // UtWildcard::UtWildcard

UtWildcard::~UtWildcard() {
}

bool UtWildcard::matchHelper(size_t i, const char* str) const {
  for (; i < mTokens.size(); ++i)
  {
    const char* token = mTokens[i];
    size_t tokenLen = strlen(token);
    switch (*token)
    {
    case '*':
      // look ahead to match the next literal

      // Matching "*?*??**xyz" means any sequence of any length at
      // least 3 long, followed by "xyz".  Ignore any extra * in
      // the sequence.
      for (++i; i < mTokens.size(); ++i)
      {
        token = mTokens[i];
        char c = token[0];
        if (!sIsWild(c))
          break;
        if (c == '?')
        {
          // '?' doesn't match '.' if mWildcardMatchesDot is false
          if ((*str == '\0') || (!mWildcardMatchesDot && (*str == '.')))
            return false;
          ++str;
        }
      }
      tokenLen = strlen(token);

      // If there is another token left, look for it in str, so we have
      // to explore all the possibilties.  This is n^2.  We should really
      // build an FSM matcher, if we want this to be fast.
      if (i < mTokens.size())
      {
        const char* lastStr = str;
        while ((str = strstr(str, token)) != NULL) {
          // if mWildcardMatchesDot is false, there can't have been a
          // '.' in the characters we skipped over.
          const char* dot = strchr(lastStr, '.');
          if (!mWildcardMatchesDot && (dot != NULL) && (dot < str)) {
            return false;
          }

          str += tokenLen;
          if (matchHelper(i + 1, str)) {
            return true;
          }

          // if that recursive match didn't work then loop around
          // and find another occurrence of token in str.  This
          // happens with pattern="abc*?def" and str="abcXdefXdef".
          // We have to ignore failure following the first match of 'def' and
          // try the second one.
        }
        // none of the occurrences of token in str led to a match.
        return false;
      }
      else {
        // Last token was a '*'.  That's a match unless
        // mWildcardMatchesDot is false and there's a '.' in the
        // remaining string.
        if (!mWildcardMatchesDot && (strchr(str, '.') != NULL)) {
          return false;
        }
        return true;
      }
      break;
    case '?':
      // '?' doesn't match '.' if mWildcardMatchesDot is false
      if ((*str == '\0') || (!mWildcardMatchesDot && (*str == '.')))
        return false;
      ++str;
      break;
    default:
      if (strncmp(str, mTokens[i], tokenLen) != 0)
        return false;
      str += tokenLen;
      break;
    } // switch
  }

  // We have one if we've exhausted the string and the tokens at
  // the same time, or if the last token was a *.
  return (*str == '\0');
}

bool UtWildcard::isMatch(const char* str) const
{
  return matchHelper(0, str);
} // bool UtWildcard::isMatch

//! assign a wildcard
UtWildcard& UtWildcard::operator=(const UtWildcard& src) {
  if (&src != this) {
    mTokens = src.mTokens;
  }
  return *this;
}

//! copy-construct a wildcard
UtWildcard::UtWildcard(const UtWildcard& src):
  mTokens(src.mTokens)
{
}

UtWildcard::UtWildcard() {
}
