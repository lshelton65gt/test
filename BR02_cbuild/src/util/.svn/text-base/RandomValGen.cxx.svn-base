// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "util/CarbonTypes.h"
#include "util/CarbonPlatform.h"
#include "util/RandomValGen.h"
#include "util/CarbonAssert.h"

RandomValGen::RandomValGen()
{
  Reseed(1);
}

RandomValGen::RandomValGen(SInt32 seed)
{
  Reseed(seed);
}

void RandomValGen::Reseed(SInt32 seed)
{
  UInt32 s = (UInt32)seed;
  for (mStateIndex = 0; mStateIndex < (SInt32) eN; mStateIndex++)
  {
    s = s * 29943829 - 1;
    mState[mStateIndex] = s;
  }
}



UInt32 RandomValGen::URandom()
{
  UInt32 val;

  if (mStateIndex >= (SInt32) eN)
  {
    // generate eN words at one time
    const UInt32 LOWER_MASK = (1u << eR) - 1;
    const UInt32 UPPER_MASK = 0xffffffffu << eR;
    SInt32 kk, km;

    for (kk = 0, km = (SInt32) eM; kk < ((SInt32) eN) - 1; kk++)
    {
      val = (mState[kk] & UPPER_MASK) | (mState[kk + 1] & LOWER_MASK);
      mState[kk] = mState[km] ^ (val >> 1) ^ (-(SInt32)(val & 1) & eMATRIX_A);
      if (++km >= (SInt32) eN)
      {
	km = 0;
      }
    }

    val = (mState[eN - 1] & UPPER_MASK) | (mState[0] & LOWER_MASK);
    mState[eN - 1] = mState[eM - 1] ^ (val >> 1) ^ (-(SInt32)(val & 1) & eMATRIX_A);
    mStateIndex = 0;
  }

  val = mState[mStateIndex++];

  val ^=  val >> eTEMU;
  val ^= (val << eTEMS) & eTEMB;
  val ^= (val << eTEMT) & eTEMC;
  val ^=  val >> eTEML;
  return val;
}



UInt32 RandomValGen::URRandom(UInt32 min, UInt32 max)
{
  UInt32 value = URandom();
  UInt32 size = max - min + 1;
  if (size != 0)                // note possibility of min=0, max=0xffffffff
  {
    value = (value % size) + min;
  }
  return value;
}


SInt32 RandomValGen::SRRandom(SInt32 min, SInt32 max)
{
  SInt32 value = min;
  if (max != min)
  {
    INFO_ASSERT(max > min, "Function parameters are switched?");
    value = (SInt32) URRandom((UInt32) min, (UInt32) max);
    INFO_ASSERT(min <= value, "Invalid random number generated.");
    INFO_ASSERT(value <= max, "Invalid random number generated.");
  }
  return value;
}


double RandomValGen::DRandom()
{
  // output random float number in the interval 0 <= x < 1
  union {
    double f;
    UInt32 i[2];
  }
  convert;
  // get 32 random bits and convert to float
  UInt32 r = URandom();

  convert.i[0] =  r << 20;
  convert.i[1] = (r >> 12) | 0x3FF00000;

  return convert.f - 1.0;
}



double RandomValGen::DRRandom(double min, double max)
{
  INFO_ASSERT(max >= min, "Function parameters are switched?");
  return (max - min) * DRandom() + min;
}



SInt32 RandomValGen::SRandom()
{
  return (SInt32) URandom();
}
