// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtString.h"
#include "util/UtStringUtil.h"
#include "util/UtIOStream.h"
#include "util/MsgContext.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"
#include "shell/carbon_shelltypes.h"
#if pfWINDOWS
#define _WIN32_WINNT 0x0500     // Windows 2000 or better, needed to
                                // declare GetConsoleWindow()
#include <windows.h>
#endif

typedef UtHashMap<SInt32, MsgContextBase::Severity> SeverityMap;

class MsgContextPrivate
{
public: CARBONMEM_OVERRIDES
  MsgContextPrivate() {}
  ~MsgContextPrivate() {}

  // User functions registered for callback when a message is reported.
  typedef UtArray<MsgCallback*> CallbackArray;
  CallbackArray mCallbacks;

  UtArray<MsgStream*> mReporters;

  // Severity map for out-of-range suppressions.  This is so we can suppress
  // Cheetah or Jaguar licenses with the same mechanism
  SeverityMap mSeverityMap;
};

extern MsgContextBase::Severities gDefaultSeverities[];
extern size_t gNumDefaultSeverities;

MsgContextBase::MsgContextBase() :
  mDefaultSeverities (gDefaultSeverities),
  mPrivate(new MsgContextPrivate)
{
  mSeverityCount[eNote] = 0;
  mSeverityCount[eWarning] = 0;
  mSeverityCount[eAlert] = 0;
  mSeverityCount[eError] = 0;
  mSeverityCount[eStatus] = 0;
  mSeverityCount[eFatal] = 0;
  mSeverityCount[eContinue] = 0;
  mSeverityCount[eSuppress] = 0;
  mAllNotesSuppressed = false;
  mAllWarningsSuppressed = false;
  mSuppressed = false;
}

MsgContextBase::~MsgContextBase()
{
  delete mPrivate;
}

// Sparcworks requires extern "C" call backs.
extern "C" int cmp_severities(const void* p1, const void* p2)
{
  MsgContextBase::Severities *s1 = (MsgContextBase::Severities *) p1;
  MsgContextBase::Severities *s2 = (MsgContextBase::Severities *) p2;
  return s1->code - s2->code;
}

static MsgContextBase::Severities* sFindDefaultSeverity(int errorNumber)
{
  MsgContextBase::Severities sev, *sp;
  sev.code = errorNumber;

  sp = (MsgContextBase::Severities*) std::bsearch(&sev, gDefaultSeverities,
                                                  gNumDefaultSeverities,
                                                  sizeof(sev),
                                                  cmp_severities);
  return sp;
}

bool MsgContextBase::putMsgNumSeverity(SInt32 msgNo, Severity sev) {
  Severities *sp = sFindDefaultSeverity(msgNo);
  Severity oldSeverity;
  bool status = false;
  if (!getMsgSeverity(msgNo, &oldSeverity))
    oldSeverity = eSuppress;    // eg interra error number with unknown default

  if (((int) sev > (int) oldSeverity) ||   // It's OK to increase severity
      ((int) oldSeverity <= (int) eAlert)) // It's OK to demote alerts and below
  {
    status = true;
    if (sp != NULL)
      sp->severity = sev;
    else
      mPrivate->mSeverityMap[msgNo] = sev;
  }
  return status;
}

void MsgContextBase::getUnknownSeverityRange(SInt32 minSeverity, SInt32 maxSeverity,
                                             SeverityArray* sev, IntArray* msgNumbers)
{
  for (SeverityMap::SortedLoop p = mPrivate->mSeverityMap.loopSorted();
       !p.atEnd(); ++p)
  {
    SInt32 msgNumber = p.getKey();
    if ((minSeverity <= msgNumber) && (msgNumber <= maxSeverity)) {
      sev->push_back(p.getValue());
      msgNumbers->push_back(msgNumber);
    }
  }
}

void MsgContextBase::putMsgSeverity(const char* msg, Severity sev, bool *msgIdValid, bool *msgChangeValid)
{
  // We don't have an associative structure based on 'name' at the
  // moment, so just search.
  SInt32 msgNo = -1;
  *msgIdValid = false;
  *msgChangeValid = false;
  if (StringUtil::parseNumber(msg, &msgNo) && (msgNo >= 0)) {
    *msgIdValid = true;
    *msgChangeValid = putMsgNumSeverity(msgNo, sev);
  } else {
    for (size_t i = 0; i < gNumDefaultSeverities; ++i) {
      if (strcmp(msg, mDefaultSeverities[i].name) == 0) {
        *msgIdValid = true;
        *msgChangeValid = putMsgNumSeverity(mDefaultSeverities[i].code, sev);
        break;
      }
    }
  }
}

bool MsgContextBase::getMsgSeverity(const char* msg, Severity *sev)
  const
{
  // We don't have an associative structure based on 'name' at the
  // moment, so just search.
  for (size_t i = 0; i < gNumDefaultSeverities; ++i)
  {
    if (strcmp(msg, mDefaultSeverities[i].name) == 0)
    {
      *sev = mDefaultSeverities[i].severity;
      if ((mAllNotesSuppressed && (*sev == eNote)) ||
	  (mAllWarningsSuppressed && (*sev == eWarning))) {
        *sev = eSuppress;
      }
      return true;
    }
  }
  return false;
}

bool MsgContextBase::getMsgSeverity(SInt32 msgNo, Severity *sev)
  const
{
  bool ret = false;

  // We don't have an associative structure based on 'name' at the
  // moment, so just search.
  Severities* sp = NULL;
  if ((msgNo >= 0) &&
      //      (msgNo < ((SInt32) gNumDefaultSeverities)) &&
      ((sp = sFindDefaultSeverity(msgNo)) != NULL))
  {
    *sev = sp->severity;
    ret = true;
  }
  else
  {
    SeverityMap::iterator p = mPrivate->mSeverityMap.find(msgNo);
    if (p != mPrivate->mSeverityMap.end())
    {
      *sev = p->second;
      ret = true;
    }
  }
  if (ret && ((mAllNotesSuppressed && (*sev == eNote)) ||
	      (mAllWarningsSuppressed && (*sev == eWarning)))) {
    *sev = eSuppress;
  }
  return ret;
} // bool MsgContextBase::getMsgSeverity

bool MsgContextBase::getMsgCount(const char* msg, int* msgCount)
  const
{
  // We don't have an associative structure based on 'name' at the
  // moment, so just search.
  for (size_t i = 0; i < gNumDefaultSeverities; ++i)
  {
    if (strcmp(msg, mDefaultSeverities[i].name) == 0)
    {
      *msgCount = mDefaultSeverities[i].msgCount;
      return true;
    }
  }
  return false;
}

MsgContextBase::MsgObject::MsgObject(int id,
                                     const char* mneumonic,
                                     Severity severity,
                                     const char* location,
                                     const char* formattedText)
{
  mMsgNumber = id;
  mMneumonic = mneumonic;
  mSeverity = severity;
  mPreviousSeverity = eContinue;
  mLocation = (location ? new UtString (location) : 0);
  mText = new UtString (formattedText);
}

const char* MsgContextBase::severityToString(Severity sev) {
  switch (sev)
  {
  case eNote: return "Note";
  case eWarning: return "Warning";
  case eAlert: return "Alert";
  case eError: return "Error";
  case eFatal: return "Fatal";
  case eStatus: return "Status";
  case eContinue: return "Continue";
  case eSuppress: return "Suppress";
  };
  return NULL;
}

void MsgContextBase::MsgObject::format(UtString* str, bool includeSuppress)
  const
{
  UtString& of = *str;
  if (includeSuppress || (mSeverity != eSuppress))
  {
    if (mLocation && not mLocation->empty() )
      of << *mLocation << ": ";
    switch (mSeverity)
    {
    case eNote: of << "Note"; break;
    case eWarning: of << "Warning"; break;
    case eAlert: of << "Alert"; break;
    case eError: of << "Error"; break;
    case eFatal: of << "Fatal"; break;
    case eStatus: of << "Status"; break;
    case eContinue: break;
    case eSuppress: break;
    };

    if (mSeverity != eContinue)
      of << " " << mMsgNumber << ": " << *mText;
    else
      of << *mText;
    if (mSeverity != eStatus)
    {
      if ((*mText)[mText->size() -1] != '\n')
        of << '\n';
    }
    else
      of << '\r';
  } // if
} // void MsgContextBase::MsgObject::format

void MsgContextBase::MsgObject::report(FILE* f, bool includeSuppress)
  const
{
  UtString buf;
  format(&buf, includeSuppress);
  (*OSStdio::pfwrite)(buf.c_str(), buf.size(), 1, f);
  (*OSStdio::pfflush)(f);
  // If we just attempted to write to standard error or standard output
  // and the message shouldn't be suppressed...
  if ((f == stderr || f == OSStdio::mstderr
       || f == stdout || f == OSStdio::mstdout)
      && (includeSuppress || mSeverity != eSuppress))
    reportMessageBox(buf.c_str());
}

// If a Windows application, display the message in a message box.
void
MsgContextBase::MsgObject::reportMessageBox(const char* message) const
{
#if pfWINDOWS
  // If we're running on Windows and we're linked into a Windows
  // (rather than a console) application, the beautiful message we
  // just wrote to standard error or standard output probably just got
  // thrown on the floor.  So, we'll also display it in a message
  // box.

  // We assume that if GetConsoleWindow() returns non-NULL the user
  // can see the message that we wrote to standard error/output, so
  // we're done.
  // GetConsoleWindow() is declared in wincon.h (included from
  // windows.h) only when the macro _WIN32_WINNT is defined to be
  // 0x0500 or greater, and only works with Windows 2000 and newer.
  if (GetConsoleWindow() != NULL)
    return;
  // Currently GetConsoleWindow() on Wine is a stub that returns
  // NULL.  When running regression tests on Wine we don't want
  // message boxes popping up.  And if somebody's running on Wine,
  // they're likely to be able to see standard output and standard
  // error.  So on Wine, don't display message boxes.
  else if (OSIsWine())
    return;

  // If not a continued message, remember the severity in case the
  // next is a continue.  If continued, reuse the previous severity.
  Severity severity;
  if (mSeverity != eContinue)
    mPreviousSeverity = severity = mSeverity;
  else
    severity = mPreviousSeverity;

  UINT type = MB_OK;
  switch (severity) {
  case eStatus:
  case eSuppress:
  case eNote:		type |= MB_ICONINFORMATION; break;
  case eWarning:	type |= MB_ICONWARNING; break;
  case eAlert:
  case eError:
  case eFatal:		type |= MB_ICONERROR; break;
  case eContinue:	INFO_ASSERT(1, "Internal logic error"); break;
  }

  UtString caption = UtString("Carbon ")
    + MsgContextBase::severityToString(severity);
  MessageBox(NULL, message, caption.c_str(), type);
#else
  (void) message;               // Unused parameter
#endif // pfWINDOWS
}

const char* MsgContextBase::MsgObject::getLocation() {
  return mLocation->c_str();
}

const char* MsgContextBase::MsgObject::getText() {
  return mText->c_str();
}

void MsgContextBase::incrSeverityCount(Severity severity)
{
  ++mSeverityCount[severity];
}

eCarbonMsgCBStatus MsgContextBase::processCallbacks(MsgObject* mo)
{
  MsgContextPrivate::CallbackArray::reverse_iterator cb, end;
  end = mPrivate->mCallbacks.rend();
  eCarbonMsgCBStatus status = eCarbonMsgContinue;
  for (cb = mPrivate->mCallbacks.rbegin();
       status == eCarbonMsgContinue && cb != end;
       ++cb)
  {
    status = (*cb)->callHandler(mo);
  }
  return status;
}

MsgContextBase::Severity
MsgContextBase::addMessage(int errorNumber, // to lookup severity
                           const char* mneumonic,
                           const char* location,
                           const char* formattedText)
{
  Severities *sp = sFindDefaultSeverity(errorNumber);
  INFO_ASSERT(sp, "Invalid error number.");
  Severity severity = sp->severity;
  if ((mAllNotesSuppressed && (severity == eNote)) ||
      (mAllWarningsSuppressed && (severity == eWarning)) ||
      (mSuppressed && (severity == eContinue))) {
    severity = eSuppress;
  }
  mSuppressed = (severity == eSuppress);

  MsgObject mo(errorNumber, mneumonic, severity, location, formattedText);
  incrSeverityCount(severity);
  // Call any handlers registered by the C API.
  eCarbonMsgCBStatus status = processCallbacks(&mo);

  if ( status == eCarbonMsgContinue )
  {
    UtArray<MsgStream*>::iterator p;
    for (p = mPrivate->mReporters.begin(); p != mPrivate->mReporters.end(); ++p)
    {
      (*p)->report(&mo);
      if ( severity == eFatal )
      {
        // If we exit on a fatal error, let's summarize ourself to the
        // user.  This string exactly recreates message 110,
        // MessageSummary; unfortunately we can't call it directly from
        // here.
        UtString msg;
        msg << getSeverityCount( eWarning ) << " warnings, " 
            << getSeverityCount( eError ) + 1 << " errors, and "
            << getSeverityCount( eAlert ) << " alerts detected.";
        MsgContextBase::MsgObject sm( 110, "MessageSummary", eContinue,
                                      NULL, msg.c_str( ));
        (*p)->report(&sm);

      }
    }
    if ( severity == eFatal ){
      exit(1);      // defer the exit until all streams have been written
    }
  }
  return severity;
}

MsgStream::MsgStream()
{
}
  
MsgStream::~MsgStream()
{
}

MsgStreamIO::MsgStreamIO(FILE* stream, bool includeStatusMsgs):
  mStream(stream),
  mIncludeStatusMsgs(includeStatusMsgs)
{
}

void MsgStreamIO::report(MsgContextBase::MsgObject* mo)
{
  if (mo->getSeverity() == MsgContextBase::eStatus)
  {
    if (mIncludeStatusMsgs)
      mo->report(mStream, false);
  }
  else
    mo->report(mStream, false);
}

void MsgContextBase::addReportStream(MsgStream* ms)
{
  mPrivate->mReporters.push_back(ms);
}

void MsgContextBase::removeReportStream(MsgStream* ms)
{
  UtArray<MsgStream*>::iterator p;
  for (p = mPrivate->mReporters.end() - 1;
       p >= mPrivate->mReporters.begin(); --p)
  {
    if (*p == ms)
      mPrivate->mReporters.erase(p, p + 1);
  }    
}

void MsgContextBase::addMessageCallback(MsgCallback *msgCallback)
{
  mPrivate->mCallbacks.push_back(msgCallback);
}

void MsgContextBase::removeMessageCallback(MsgCallback *msgCallback)
{
  MsgContextPrivate::CallbackArray::iterator p;
  for (p = mPrivate->mCallbacks.end() - 1;
       p >= mPrivate->mCallbacks.begin(); --p)
  {
    if (*p == msgCallback)
    {
      mPrivate->mCallbacks.erase(p, p + 1);
    }
  }    
}

MsgContextBase::MsgObject::MsgObject(const MsgObject& src)
{
  *this = src;

  // Make copies of the strings so that we can delete them safely
  if (src.mLocation)
    mLocation = new UtString (*src.mLocation);

  mText = new UtString (*src.mText);
}

MsgContextBase::MsgObject::~MsgObject ()
{
  delete mLocation;
  delete mText;
}

MsgContextBase::MsgObject& MsgContextBase::MsgObject::operator=(const MsgObject& src)
{
  if (&src != this)
  {
    mMsgNumber = src.mMsgNumber;
    mMneumonic = src.mMneumonic;
    mSeverity = src.mSeverity;
    if (mLocation != NULL)
      delete mLocation;
    if (mText != NULL)
      delete mText;
    mLocation = (src.mLocation ? new UtString (*src.mLocation): 0);
    mText = new UtString (*src.mText);
  }
  return *this;
}

void MsgContextBase::suppressAllNotes(bool suppress)
{
  mAllNotesSuppressed = suppress;
}

void MsgContextBase::suppressAllWarnings(bool suppress)
{
  mAllWarningsSuppressed = suppress;
}

int MsgContextBase::getExitStatus() {
  UInt32 errCount = (getSeverityCount(eError) +
                     getSeverityCount(eAlert) +
                     getSeverityCount(eFatal));
  return (errCount == 0)? 0: 1;
}

MsgCallback::MsgCallback(CarbonMsgCB userFunc, CarbonClientData userData):
  mUserFunc(userFunc), mUserData(userData)
{
}

MsgCallback::~MsgCallback()
{
}

static CarbonMsgSeverity sTranslateSeverity(MsgContextBase::Severity internalSev)
{
  CarbonMsgSeverity msgSev;

  switch(internalSev)
  {
    case MsgContextBase::eStatus:   msgSev = eCarbonMsgStatus; break;
    case MsgContextBase::eNote:     msgSev = eCarbonMsgNote; break;
    case MsgContextBase::eWarning:  msgSev = eCarbonMsgWarning; break;
    case MsgContextBase::eAlert:    msgSev = eCarbonMsgAlert; break;
    case MsgContextBase::eError:    msgSev = eCarbonMsgError; break;
    case MsgContextBase::eFatal:    msgSev = eCarbonMsgFatal; break;
    case MsgContextBase::eSuppress: msgSev = eCarbonMsgSuppress; break;
    case MsgContextBase::eContinue: msgSev = eCarbonMsgSuppress; break;
    default: msgSev = eCarbonMsgFatal; break;
  }

  return msgSev;
}

eCarbonMsgCBStatus MsgCallback::callHandler(MsgContextBase::MsgObject *mo)
{
  eCarbonMsgCBStatus status;
  const char *text = mo->getText();

  // call the user-provided function
  status = mUserFunc(mUserData,
                     sTranslateSeverity(mo->getSeverity()),
                     mo->getMsgNumber(),
                     text,
                     strlen(text));

  return status;
}
