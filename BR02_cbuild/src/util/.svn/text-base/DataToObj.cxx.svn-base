//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// Convert an arbitrary data file to a C routine that will build
// that file.

#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtStringUtil.h"
#include "util/UtString.h"

bool UtConvertFileToCFunction(const char* inputDataFile,
                              const char* cFileName,
                              const char* cFunctionName,
                              UtString* errmsg)
{
  UtIBStream in(inputDataFile);
  if (! in.is_open()) {
    *errmsg = in.getErrmsg();
    return false;
  }
  UtOBStream out(cFileName);
  if (! out.is_open()) {
    *errmsg = out.getErrmsg();
    return false;
  }

  out << "static const char sStringData_" << cFunctionName << "[] = \n  \"";

# define BUFSIZE 1000
  char buf[BUFSIZE];
  UInt32 totalSize = 0;
  UInt32 numRead = 0;
  UtString outbuf;
  UInt32 column = 3;
  while ((numRead = in.read(buf, BUFSIZE)) != 0) {
    totalSize += numRead;
    for (UInt32 i = 0; i < numRead; ++i) {
      if (column >= 75) {
        out << "\"\n  \"";
        column = 3;
      }

      unsigned char c = buf[i];
      switch (c) {
      case '"':
      case '\\':
        out << '\\' << (char) c;
        column += 2;
        break;
      default:
        if (!isprint(c) || (c == '?')) { // make ? in Octal to avoid trigraphs
          out << '\\';
          char buf[10];
          sprintf(buf, "%o", (unsigned int) c);
          for (UInt32 i = strlen(buf); i < 3; ++i) {
            out << '0';
          }
          out << buf;
          column += 4;
        }
        else {
          out << (char) c;
          column += 1;
        }
        break;
      } // switch
    }
  }
  out << "\";\n\nstatic int sSizeData_" << cFunctionName << " = " << totalSize
      << ";\n\n";
  out << "void " << cFunctionName << "(const char** buf, int* size) {\n";
  out << "  *buf = sStringData_" << cFunctionName << ";\n";
  out << "  *size = sSizeData_" << cFunctionName << ";\n";
  out << "}\n";

  if (!out.close()) {
    *errmsg = out.getErrmsg();
    return false;
  }
  return true;
} // bool UtConvertFileToCFunction
