// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtCustomerDB.h"
#include "util/UtString.h"
#include "util/Loop.h"
#include "util/Zstream.h"
#include "util/UtConv.h"
#include "util/UtArray.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"
#include "util/UtHashSet.h"

static const char scCommentDelim = '#';
static const char scModelDelim = '-';

class UtCustomerDB::CustDBHelper
{
public: 
  CARBONMEM_OVERRIDES

  CustDBHelper() : mCustomerSignature(NULL),
                   mAllSigStrings(NULL)
  {
  }
  
  ~CustDBHelper() 
  {
    delete mAllSigStrings;
    
    delete mCustomerSignature;
  }
  
  Signature* setCustomerSignature(UtCustomerDB::SettingHelper pHelper)
  {
    if (mCustomerSignature)
    {
      if (mAllSigStrings)
        mAllSigStrings->erase(mCustomerSignature->getStr());

      mCustomerSignature->putName(pHelper.mCurrent);
    }
    else
    {
      mCustomerSignature = createSignature(pHelper.mCurrent);
      if (mAllSigStrings)
        mAllSigStrings->insert(mCustomerSignature->getStr());
    }

    return mCustomerSignature;

  } 

  Signature* getCustomerSignature() const
  {
    return mCustomerSignature;
  }
  
  bool dbWrite(ZostreamDB& outDB) const
  {
    outDB << scDBSig;
    outDB << scVersion;

    INFO_ASSERT(mCustomerSignature != NULL, "Invalid customer db. Customer signature is not set.");

    outDB.writeObject(*mCustomerSignature);
    
    return ! outDB.fail();
  }
  
  bool dbRead(ZistreamDB& inDB)
  {
    // Note the mAllSigStrings set is NOT used in the read. The
    // runtime has no real use for it. 
    if (! inDB.expect(scDBSig))
    {
      inDB.setError("Invalid UtCustomerDB signature");
      return false;
    }

    UInt32 version;
    if (! (inDB >> version))
      return false;

    if (version > scVersion)
    {
      UtString buf;
      buf << "Unsupported UtCustomerDB version: " << version;
      inDB.setError(buf.c_str());
      return false;
    }

    bool ret = true;
    // There is only 1 customer db and no subdivisions of it, so we
    // should only read it and not append to it.
    INFO_ASSERT(mCustomerSignature == NULL, "The customer db can only be read once.");
    
    mCustomerSignature = new Signature;
    ret = inDB.readObject(mCustomerSignature);

    return ret;
  }
  
private:
  static const char* scDBSig;
  // Version 1 - removed license type and CEM (and generally broke compatibility)
  static const UInt32 scVersion = 1;
  
  typedef UtArray<UtCustomerDB::Signature*> SigVec;
  typedef UtHashSet<const UtString*, HashPointerValue<const UtString*> > StringHash;
  
  Signature* mCustomerSignature; // this is used for both model kits and IP
  StringHash* mAllSigStrings;
  
  bool validateName(const char* name, UtString* reason)
  {
    bool isValid = (name && (*name != '\0'));
    if (! isValid)
      *reason << "Name is empty";
    else
    {
      isValid = StringUtil::findFirstNonAlphanumeric(name) == NULL;
      if (! isValid)
        *reason << "Non-alphanumeric character found in name, '" << name << "'";
    }
    return isValid;
  }
  
  //! Is this signature already added?
  bool isAlreadyAdded(const char* signature) const
  {
    UtString sigStr(signature);
    return (mAllSigStrings->find(&sigStr) != mAllSigStrings->end());
  }
  
  UtCustomerDB::Signature* createSignature(const char* sig)
  {
    Signature* ret = new Signature(sig);
    if (mAllSigStrings)
      mAllSigStrings->insert(ret->getStr());
    
    return ret;
  }
};

const char* UtCustomerDB::CustDBHelper::scDBSig = "_Cust_DB_3";

UtCustomerDB::UtCustomerDB()
{
  mDBHelper = new CustDBHelper;
}

UtCustomerDB::~UtCustomerDB()
{
  delete mDBHelper;
}

bool UtCustomerDB::dbWrite(ZostreamDB& outDB) const
{
  return mDBHelper->dbWrite(outDB);
}

bool UtCustomerDB::dbRead(ZistreamDB& inDB)
{
  return mDBHelper->dbRead(inDB);
}


// Signature implementation
static const char* scSignature_Sig = "CustDBSignature";
// Version 2 - removed license type and CEM (and generally broke compatibility)
static const UInt32 scSignatureVersion = 2;
// version when we started supporting time-bombs
static const UInt32 scTimeBombVersion = 1; 

class UtCustomerDB::Signature::Helper
{
public:
  CARBONMEM_OVERRIDES

  Helper(UInt32 version) : mTimeBomb(0), mVersion(version) 
  {}

  ~Helper() {}

  
  void addIPFeature(const char* featureName)
  {
    mIPFeatures.push_back(featureName);
  }

  UtCustomerDB::StrIter loopIPFeatures() const
  {
    return UtCustomerDB::StrIter::create(mIPFeatures.loopCUnsorted());
  }

  bool dbWrite(ZostreamDB& outDB) const
  {
    UInt32 num = mIPFeatures.size();
    outDB << num;
    for (UInt32 i = 0; i < num; ++i)
      outDB << mIPFeatures[i];
    
    outDB << mTimeBomb;
    return ! outDB.fail();
  }

  bool dbRead(ZistreamDB& inDB)
  {
    UInt32 num;
    if (! (inDB >> num))
      return false;
    
    for (UInt32 i = 0; i < num; ++i)
    {
      UtString tmpStr;
      if (! (inDB >> tmpStr))
        return false;
      mIPFeatures.push_back(tmpStr);
    }

    if (mVersion >= scTimeBombVersion)
      inDB >> mTimeBomb;
    
    return ! inDB.fail();
  }

  SInt64 getTimeBomb() const { return mTimeBomb; }
  void putTimeBomb(SInt64 tb) 
  {
    mTimeBomb = tb;
  }
  
private:
  typedef UtArray<UtLicense::Type> LicTypeVec;
  LicTypeVec mLicTypes;
  UtStringArray mIPFeatures;
  SInt64 mTimeBomb;
  UInt32 mVersion;

  Helper(const Helper&);
  Helper& operator=(const Helper&);
};

UtCustomerDB::Signature::Signature() :
  mHelper(NULL), mSignature(NULL)
{
}

UtCustomerDB::Signature::Signature(const char* sig)
{
  mSignature = new UtString(sig);
  mHelper = new Helper(scSignatureVersion);
}

UtCustomerDB::Signature::~Signature()
{
  delete mHelper;
  delete mSignature;
}

void UtCustomerDB::Signature::putName(const char* name)
{
  mSignature->assign(name);
}

const char* UtCustomerDB::Signature::c_str() const
{
  return mSignature->c_str();
}

void UtCustomerDB::Signature::addIPFeature(const char* featureName)
{
  mHelper->addIPFeature(featureName);
}

UtCustomerDB::StrIter
UtCustomerDB::Signature::loopIPFeatures() const
{
  return mHelper->loopIPFeatures();
}

UtCustomerDB::Signature*
UtCustomerDB::setCustomerSignature(const char* cust_key, 
                                    UtString* reason)
{
  SettingHelper pHelper;
  pHelper.mCurrent = cust_key;
  pHelper.mReason = reason;
  return mDBHelper->setCustomerSignature(pHelper);
}

const UtCustomerDB::Signature* 
UtCustomerDB::getCustomerSignature() const
{
  return mDBHelper->getCustomerSignature();
}

UtCustomerDB::Signature* 
UtCustomerDB::getCustomerSignature() 
{
  return mDBHelper->getCustomerSignature();
}

SInt64 UtCustomerDB::Signature::getTimeBomb() const
{ 
  return mHelper->getTimeBomb();
}

void UtCustomerDB::Signature::putTimeBomb(SInt64 timeBomb)
{
  mHelper->putTimeBomb(timeBomb);
}

bool UtCustomerDB::Signature::dbWrite(ZostreamDB& outDB) const
{
  outDB << scSignature_Sig;
  outDB << scSignatureVersion;
  mSignature->dbWrite(outDB);
  mHelper->dbWrite(outDB);
  return ! outDB.fail();
}

bool UtCustomerDB::Signature::dbRead(ZistreamDB& inDB)
{
  UtString sig;
  if (! (inDB >> sig))
    return false;

  if (sig.compare(scSignature_Sig) != 0)
  {
    inDB.setError("Invalid CustDBSignature signature.");
    return false;
  }
  
  UInt32 version;
  if (! (inDB >> version))
    return false;
  
  if (version > scSignatureVersion)
  {
    UtString buf;
    buf << "Unsupported CustDBSignature version: " << version;
    inDB.setError(buf.c_str());
    return false;
  }

  mSignature = new UtString;
  mSignature->dbRead(inDB);

  mHelper = new Helper(version);
  mHelper->dbRead(inDB);

  return ! inDB.fail();
}
