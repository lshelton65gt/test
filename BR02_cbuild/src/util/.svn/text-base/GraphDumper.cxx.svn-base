// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  Class to do a sorted depth first discovery order dump of a graph
*/

#include "util/GraphDumper.h"
#include "util/UtIOStream.h"

UtGraphDumper::UtGraphDumper(UtOStream& out) :
  mOut(out), mIndent(0), mNodeCount(0), mEdgeStr(""), mParentNode(0)
{}

void UtGraphDumper::dump(Graph* graph, NodeCmp* nodeCmp, EdgeCmp* edgeCmp,
                         const UtString& edgeStr)
{
  mEdgeStr = edgeStr;
  walk(graph, nodeCmp, edgeCmp);
}

GraphWalker::Command
UtGraphDumper::visitNodeBefore(Graph* graph, GraphNode* node)
{
  // Bump the counts and set this graph node's number
  UInt32 num = ++mNodeCount;
  UInt32 spaces = mIndent++;
  graph->setScratch(node, num);

  // Print the prefix for the node
  mOut << "[" << num << "]";
  if (num < 1000)
    spaces++;
  if (num < 100)
    spaces++;
  if (num < 10)
    spaces++;
  indent(spaces);
  mOut << mEdgeStr;
  if (mParentNode)
    mOut << mParentNode;
  mOut << ":";

  // Print the node
  printNode(graph, node);
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDumper::visitNodeAfter(Graph*, GraphNode*)
{
  // Pop the indentation
  --mIndent;
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDumper::visitCrossEdge(Graph* graph, GraphNode* node, GraphEdge* edge)
{
  // Print the cross edge
  printCrossOrBackEdgeNode(graph, node, edge, false);
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDumper::visitBackEdge(Graph* graph, GraphNode* node, GraphEdge* edge)
{
  // Print the back edge
  printCrossOrBackEdgeNode(graph, node, edge, true);
  return GW_CONTINUE;
}

GraphWalker::Command
UtGraphDumper::visitTreeEdge(Graph* graph, GraphNode* node, GraphEdge* edge)
{
  // Set the mParentNode index
  recordParent(graph, node);

  // Update the string for this edge
  edgeString(graph, node, edge, &mEdgeStr);
  return GW_CONTINUE;
}

void
UtGraphDumper::printNode(Graph* graph, GraphNode* node)
{
  // Ask the derived class for the string of the node and print it
  UtString str;
  nodeString(graph, node, &str);
  mOut << str << "\n";
}

void
UtGraphDumper::printCrossOrBackEdgeNode(Graph* graph, GraphNode* node,
                                        GraphEdge* edge, bool isBackEdge)
{
  // Set the mParentNode index
  recordParent(graph, node);

  // Update the string for this edge
  edgeString(graph, node, edge, &mEdgeStr);

  // If this is a back edge start with a *
  if (isBackEdge) {
    mOut << "[*]";
  } else {
    mOut << "   ";
  }

  // Print the info for the to node
  GraphNode* to = graph->endPointOf(edge);
  indent(mIndent+3);
  UInt32 num = graph->getScratch(to);
  mOut << mEdgeStr << mParentNode << ":[" << num << "]:";
  printNode(graph, to);
}

void UtGraphDumper::indent(UInt32 spaces)
{
  UtString str;
  str.append(spaces, ' ');
  mOut << str;
}

void UtGraphDumper::recordParent(Graph* graph, GraphNode* node)
{
  mParentNode = graph->getScratch(node);
}
