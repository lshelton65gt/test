// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/StringAtom.h"
#include "util/HierName.h"

/*!
  \file
  Implementation of HierName methods
*/

SInt32 HierName::findDepth()
  const
{
  SInt32 i = 0;
  for (const HierName* node = this; node != NULL; node = node->getParentName())
    ++i;
  return i;
}

int HierName::compare(const HierName* n1, const HierName* n2) {
  // shallower node wins, but don't call findDepth because there is no
  // reason to find the total depth of the deeper node.  In fact we
  // can stop comparing depths once if we hit the same node while we
  // walk up the parent chain.  E.g.  we know that a.b.c.d and a.b.c.e
  // have the same depth as soon as we hit 'c'.  The i1!=i2 termination
  // condition takes care of that.
  for (const HierName* i1 = n1, *i2 = n2; i1 != i2;
       i1 = i1->getParentName(), i2 = i2->getParentName())
  {
    if (i1 == NULL)
      return -1;
    if (i2 == NULL)
      return 1;
  }

  // If we get this far then we know the names are at the same depth,
  // so we don't have to check that either n1 or n2 is NULL.  The
  // n1!=n2 check keeps us out of trouble if we compare two identical
  // names from different symbol tables, and they both hit NULL terminators
  for (; n1 != n2; n1 = n1->getParentName(), n2 = n2->getParentName()) {
    int cmp = strcmp(n1->str(), n2->str());
    if (cmp < 0)
      return -1;
    else if (cmp > 0)
      return 1;
  }
  return 0;                 // names are equivalent
}

bool HierName::operator<(const HierName& other) const
{
  return compare(this, &other) < 0;
}

