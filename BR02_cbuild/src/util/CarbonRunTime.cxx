// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


/*!
  \file 
  Implements minimal runtime functions
*/

#include "util/CarbonPlatform.h"
#include "shell/carbon_shelltypes.h"
#include "util/MemManager.h"
#include "util/CarbonRunTime.h"
#include <cstring>
#include <algorithm>
#include <cmath>

// Given a[0:m-1], b[0:n-1], compute product into c[0:k-1]
// Derived from "Mathematics and Algorithms for Computer Algebra", by Dr Francis J. Wright
void
carbon_multiply (const UInt32 *a, const UInt32 *b, UInt32 *c, int m, int n, int k)
{
  int mult_size = m + n;

#if !pfGCC
  // VC++ demands constant bounds on auto array, so use dynamic allocation if
  // we don't have GCC extension available.
  UInt32* prod = new UInt32[mult_size];   // Full sized local result
#else
  UInt32 prod[mult_size];             // Full sized local result
#endif

  ::memset (prod, 0, sizeof(UInt32) * (mult_size));

  if (k > mult_size) {
    // Extra product bits need to be zeroed, cuz we won't touch 'em otherwise.
    ::memset (&c[mult_size], 0, (k-mult_size) * sizeof(c[0]));
  }
  else {
    mult_size = k;
  }

  for (int j = 0; j <= n-1; ++j)
  {
    UInt32 r = 0;                 // tracks the overflow
  
    for(int i = 0; i <= m-1; ++i)
    {
      UInt64 s = UInt64 (a[i]) * UInt64 (b[j]) + prod[i+j] + r;
      prod[i+j] = (UInt32) s;
      r = (UInt32) (s >> 32);
    }

    prod[j+m] = r;
  }

  ::memcpy (c, prod, mult_size*sizeof(UInt32));
#if !pfGCC
  delete []prod;
#endif
}

// compute OUT = - IN for I-word vector
static void negate(UInt32* out, const UInt32* in, UInt32 num_words) {
  UInt64 sum = 1;
  for (UInt32 i = 0; i < num_words; ++i) {
    sum += ~in[i];
    out[i] = (UInt32) sum;
    sum >>= 32;
  }
}

// Signed multiplication
void
carbon_signed_multiply (const UInt32 *a, const UInt32 *b, UInt32 *c,
                        int m, int n, int k)
{
  int signcase= ((SInt32 (a[m-1]) < 0) << 1) + (SInt32 (b[n-1]) < 0);
  switch (signcase) {
  case 0:                       // both operands positive
    carbon_multiply (a, b, c, m, n, k);
    break;

  case 2:                       // a is negative
    std::swap (a,b);
    std::swap (m,n);
    // fall-thru

  case 1:                       // b is negative
  {
#if !pfGCC
    UInt32 *newb = new UInt32[n];
#else
    UInt32 newb[n];
#endif
    negate (newb, b, n);
    carbon_multiply (a, newb, c, m, n, k);
#if !pfGCC
    delete newb;
#endif
    negate (c,c,k);
    break;
  }

  case 3:                       // both a, b negative
  {
#if !pfGCC
    UInt32 *newa = new UInt32[m],
      *newb = new UInt32[n];
#else
    UInt32 newa[m], newb[n];
#endif

    negate (newa, a, m);
    negate (newb, b, n);
    carbon_multiply (newa, newb, c, m, n, k);
#if !pfGCC
    delete newa;
    delete newb;
#endif
    break;
  }
  }
} // carbon_signed_multiply

SInt64
carbon_round_real (CarbonReal r)
{
  CarbonReal rounded;

  // Convert the real r into an integral value and a fraction, such that
  // -1.0 < frac < 1.0
  CarbonReal frac = std::modf (r, &rounded);
  SInt64 intPart;
  if (rounded >= 0)
    intPart = (UInt64) rounded;
  else
    intPart = (SInt64) rounded;

  if (frac >= 0.5) {
    ++intPart;
  } else if (frac <= -0.5) {
    --intPart;
  }


  return intPart;
}

UInt64
carbon_real_to_bits(CarbonReal r)
{
  CarbonRealToBitsUnion converter;
  converter.r = r;
  return converter.i64;
}

CarbonReal
carbon_bits_to_real(UInt64 i64)
{
  CarbonRealToBitsUnion converter;
  converter.i64 = i64;
  // note that any reference to converter.r will fail for some values
  // of i64 on the windows platform.  I think we need a better way to
  // test for valid real numbers but _isnan(x) is not available in wine.
  if ( converter.r != converter.r ){
    // when a real number is not equal to itself then it must be NaN
    // (not a number), we substitute zero instead, which is not
    // strictly correct but aldec and other simulators do so most of
    // the time.
    converter.r = 0.0;
  }
  return converter.r;
}

