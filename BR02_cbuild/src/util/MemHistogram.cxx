// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include <stdio.h>
#include "MemHistogram.h"
#include "util/UtIOStream.h"
#include "util/UtStack.h"
#include "util/CarbonPlatform.h"
#include "util/UtStackTrace.h"
#include "util/Zstream.h"
#include "util/OSWrapper.h"
#include "util/UtStringUtil.h"

// undefine MEM_ALLOC_CACHE_SIZE to disable the alloc cache
#define MEM_ALLOC_CACHE_SIZE 50000000

struct MemStackTrace: public UtStackTrace {
  UInt32 mTotalBytes;
  SInt32 mNumAllocs;
  SInt32 mID;
  UtArray<void*>* mAllocs;      // used only temporarily for printing

  MemStackTrace()
  {
    mTotalBytes = 0;
    mNumAllocs = 0;
    mAllocs = NULL;
    mID = -1;
  }

  MemStackTrace(const MemStackTrace& src): UtStackTrace(src)
  {
    mTotalBytes = src.mTotalBytes;
    mNumAllocs = src.mNumAllocs;
    if (src.mAllocs == NULL)
      mAllocs = NULL;
    else
      mAllocs = new UtArray<void*>(*src.mAllocs);
  }

  ~MemStackTrace()
  {
    clear();
  }


  static int compare(const MemStackTrace& st1, const MemStackTrace& st2)
  {
    int cmp = 0;
    cmp = (st1.mTotalBytes - st2.mTotalBytes);
    if (cmp == 0) {
      cmp = st1.mNumAllocs - st2.mNumAllocs;
      if (cmp == 0) {
        cmp = st1.mNum - st2.mNum;
        if (cmp == 0) {
          cmp = memcmp(st1.mFrames, st2.mFrames,
                       st2.mNum * sizeof(st1.mFrames[0]));
        }
      }
    }
    return cmp;
  }

  // Sorting routine to sort by # by of bytes allocated.
  bool operator<(const MemStackTrace& st) const
  {
    return compare(*this, st) < 0;
  }

  static void printHeader(UtOStream& file)
  {
    file << "  #Bytes %Tot #Allocs StackTrace\n";
  }

  bool isInteresting() { return mTotalBytes != 0; }

  void print(UInt32 totalBytes, UtOStream& file)
    const
  {
    UtString lineBuf;
    StringUtil::formatInt(mTotalBytes, &lineBuf, 7);
    double percent = (100.0 * mTotalBytes) / totalBytes;
    StringUtil::formatInt(static_cast<SInt32>(percent + .5), &lineBuf, 4);
    lineBuf << "% ";
    StringUtil::formatInt(mNumAllocs, &lineBuf, 7);
    file << " " << lineBuf << " ";
  }

  void printMore(UtOStream& file)
  {
    // for extreme detail, print all the pointers for this trace
    if (mAllocs != NULL)
    {
      std::sort(mAllocs->begin(), mAllocs->end());
      int col = 0;
      for (size_t i = 0; i < mAllocs->size(); ++i)
      {
        if (col == 0)
        {
          file << "            ";
          col = 12;
        }
        file << (*mAllocs)[i] << "   ";
        col += 13; // space, '0', 'x', 8 chars, 3 spaces
        if (col > 65)
        {
          file << "\n";
          col = 0;
        }
      }
      if (col != 0)
        file << "\n";
    } // if
  } // void printMore

  void operator+=(const MemStackTrace& st)
  {
    mTotalBytes += st.mTotalBytes;
    mNumAllocs += st.mNumAllocs;
    if (st.mAllocs != NULL) {
      for (UtArray<void*>::iterator p = st.mAllocs->begin(), e = st.mAllocs->end();
           p != e; ++p)
      {
        push_back(*p);
      }
    }
  }
    
  void push_back(void* ptr)
  {
    if (mAllocs == NULL)
      mAllocs = new UtArray<void*>;
    mAllocs->push_back(ptr);
  }

  void clear()
  {
    if (mAllocs != NULL)
      delete mAllocs;
    mAllocs = NULL;
  }

  void dump(ZostreamDB* f)
  {
    *f << mID << mNum;
    for (SInt32 i = 0; i < mNum; ++i)
      *f << (UIntPtr) mFrames[i];
  }

  static MemStackTrace* read(ZistreamDB& file)
  {
    UIntPtr frame;
    MemStackTrace* trace = new MemStackTrace;
    if (!(file >> trace->mID) || !(file >> trace->mNum))
    {
      delete trace;
      return NULL;
    }
    for (SInt32 i = 0; i < trace->mNum; ++i)
    {
      if (!(file >> frame))
      {
        delete trace;
        return NULL;
      }
      trace->mFrames[i] = (void*) frame;
    }
    return trace;
  }

  void putID(SInt32 id) {mID = id;}
  SInt32 getID() {return mID;}
};

MemHistogram::MemHistogram(const char* dumpfile, const char* executable,
                           bool capture)
{
  mTotalBytes = 0;
  mBusyDepth = 0;
  push();
  mTraces = new TraceSet;
  mTokenCount = 0;
  mDetail = 0;
  mListPointers = false;
  mMemExhausted = false;
  mMemReserve = NULL;
  mTraceVec = NULL;
  if (capture)
    initCapture(dumpfile, executable);
  else
    initReplay(dumpfile);
  pop();
}

#define MEM_RESERVE_SIZE 10*1000*1000 // block-size + 2M
void MemHistogram::initCapture(const char* dumpfile, const char* executable)
{
  ZOSTREAMDB_ALLOC(mMemDump, dumpfile);
  mAllocs = NULL;
  mDumpFilename = NULL;
  mSymbolTable = NULL;
#ifdef MEM_ALLOC_CACHE_SIZE
  mPtrCache = new PtrCache(this);
#else
  mPtrCache = NULL;
#endif
  mMemReserve = ::malloc(MEM_RESERVE_SIZE);

  if (!mMemDump->is_open())
  {
    fprintf(stderr, "%s\n", mMemDump->getErrmsg());
    exit(1);
  }

  // Find the absolute path to the executable
  UtString buf;
  const char* exe = OSFindExecutableInPath(executable, &buf);
  if (exe == NULL)
  {
    fprintf(stderr, "Error setting up memory debugging for %s:\n%s\n",
            executable, buf.c_str());
    exit(1);
  }
  else
  {
    // Create a symbol table, load it, and save it.
    UtExeSymbolTable symtab;
    if (!symtab.load(exe))
    {
      fprintf(stderr, "Error loading symbol table for %s:\n", exe);
      exit(1);
    }
    if (!symtab.save(mMemDump))
    {
      fprintf(stderr, "Error saving symbol table for %s:\n%s\n", exe,
              mMemDump->getErrmsg());
      exit(1);
    }
  }
} // void MemHistogram::initCapture

void MemHistogram::initReplay(const char* dumpfile)
{
  mMemDump = NULL;
  mSymbolTable = new UtExeSymbolTable;
  mAllocs = new TraceMap;     // replay
  mDumpFilename = new UtString(dumpfile);
  mPtrCache = NULL;
}

MemHistogram::~MemHistogram()
{
  flushCache();
  push();

  if (mMemDump != NULL)
  {
    if (!mMemDump->close())
    {
      fprintf(stderr, "%s\n", mMemDump->getErrmsg());
      exit(1);
    }
    delete mPtrCache;
    delete mMemDump;
    mMemDump = NULL;
  }
  else
  {
    delete mAllocs;
    delete mSymbolTable;
    delete mDumpFilename;
  }

  if (!mTraces->empty() && MemStackTrace::smMaxStackDepth == 0)
  {
    fprintf(stderr, "MemHistogram: All stacks traced were empty.\n");
    fprintf(stderr, "MemHistogram: May need to compile with gcc's -fno-omit-frame-pointer.\n\n");
  }

  mTraces->clearPointers();
  delete mTraces;
} // MemHistogram::~MemHistogram

static SInt32 sMemDebugAllocToken = -1;
void MemHistogram::bumpToken()
{
  ++mTokenCount;
  if (mTokenCount == sMemDebugAllocToken)
    CarbonMem::stopHere();
}

bool MemHistogram::replay(int detail, bool listPointers)
{
  mDetail = detail;
  mListPointers = listPointers;

  ZISTREAMDB(file, mDumpFilename->c_str());
  if (!file.is_open())
  {
    fprintf(stderr, "%s\n", file.getError());
    return false;
  }

  if (!mSymbolTable->restore(&file))
  {
    fprintf(stderr, "%s\n", file.getError());
    return false;
  }

  UInt32 id = 0;                // silence MSVC
  size_t size;
  UIntPtr ptr;

  mTraceVec = new TraceVector;
  char token;
  MemStackTrace* trace = NULL;
  bool success = true;
  UtString buf;

  while (success && (file >> token) && !file.eof())
  {
    switch (token)
    {
    case 't':
      trace = MemStackTrace::read(file);
      if (trace == NULL)
        success = false;
      else {
        INFO_ASSERT(trace->getID() == (SInt32) mTraceVec->size(), "Consistency check failed.");
        mTraceVec->push_back(trace);
        mTraces->insert(trace);
        INFO_ASSERT(mTraceVec->size() == mTraces->size(), "Consistench check failed.");
      }
      break;
    case 'a':
      if ((file >> ptr) && (file >> id) && (file >> size))
      {
        INFO_ASSERT((size_t) id < mTraceVec->size(), "Invalid alloc record.");
        replayAlloc((void*) ptr, (*mTraceVec)[id], size);
      }
      else
        success = false;
      break;
    case 'f':
      if (!(file >> ptr) || !replayFree((void*) ptr)) {
        success = false;
      }
      break;
    case 'p':
      if (file >> buf)
        print(buf.c_str());
      else
        success = false;
      break;
    default:
      UtIO::cout() << *mDumpFilename << ": Unexpected token: "
                   << token << UtIO::endl;
      return false;
    } // switch
    bumpToken();
    buf.clear();
  } // while
  if (!success)
    UtIO::cout() << *mDumpFilename << ": " << file.getError() << UtIO::endl;
  return success;
} // bool MemHistogram::replay

void MemHistogram::replayAlloc(void* ptr, MemStackTrace* trace, size_t size)
{
  mTotalBytes += size;
  trace->mTotalBytes += size;
  ++trace->mNumAllocs;
  INFO_ASSERT(mAllocs, "Mem Replay not initialized.");
  INFO_ASSERT(mAllocs->find(ptr) == mAllocs->end(), "Mem Replay memory corruption."); // memory corruption
  (*mAllocs)[ptr] = TraceEntry(trace, size);
}

bool MemHistogram::replayFree(void* ptr)
{
  INFO_ASSERT(mAllocs, "Mem Replay not initialized.");
  TraceMap::iterator p = mAllocs->find(ptr);
  if (p != mAllocs->end())
  {
    INFO_ASSERT(p != mAllocs->end(), "Pointer not found in allocation table.");
    TraceEntry& te = p->second;
    MemStackTrace* stackTrace = te.first;
    UInt32 size = te.second;
    mTotalBytes -= size;
    stackTrace->mTotalBytes -= size;
    --stackTrace->mNumAllocs;
    mAllocs->erase(p);
    return true;
  }
  fprintf(stderr, "MemHistogram::replayFree(%p): alloc not found.\n", ptr);
  return false;
}

void MemHistogram::PtrCache::insert(void* ptr, size_t size,
                                    MemStackTrace* stackTrace)
{
  if (mMap.size() == MEM_ALLOC_CACHE_SIZE) {
    // The map is full.  Remove and delete the oldest item (front of list).
    AllocRecord* ar = mFifo;
    mMemHistogram->writeAlloc(ar->mPtr, ar->mSize, ar->mStackTrace);
    mMap.erase(ar->mPtr);
    delete ar;
  }

  mMap[ptr] = new AllocRecord(ptr, size, stackTrace, *this);
}

bool MemHistogram::PtrCache::erase(void* ptr) {
  // We no longer need this allocation recorded.  Remove it from the map
  // and delete the allocation record.
  Map::iterator p = mMap.find(ptr);
  if (p == mMap.end())
    return false;
  AllocRecord* ar = p->second;
  INFO_ASSERT(ar->mPtr == ptr, "Consistency check failed.");
  mMap.erase(p);
  delete ar;
  return true;
}

void MemHistogram::PtrCache::clear() {
  mMap.clear();
  // ~AllocRecord maintains mFifo.
  while (mFifo != NULL)
    delete mFifo;
}

void MemHistogram::record(void* allocedPtr, size_t size)
{
  if (!isActive())
  {
    push();

    MemStackTrace trace;
    // the first 2 or more frames are usualy uninteresting because they
    // are something like this:
    //             0  UtStackTrace::grab(int)
    //             1  MemHistogram::record(void*, unsigned)
    //             2  CarbonMemAllocate(unsigned, bool)
    //             3  operator new(unsigned)
    //                or
    //             3  operator new[](unsigned)
    //                or
    //             3  carbonmem_alloc
    //                or
    //             3  CarbonMem::allocate(unsigned)
    // But we have found some variability in the number of uninteresting
    // frames due to optimizations and platform-differences between
    // Linux, Linux64, and Solaris.  The best approach I've found is to
    // ignore the first two frames, during the grab, and then filter out
    // the uninteresting ones with UtExeSymbolTable::isInteresting. 
    trace.grab(1);

    MemStackTrace* stackTrace = NULL;
    TraceSet::iterator p = mTraces->find(&trace);
    if (p == mTraces->end())
    {
      stackTrace = new MemStackTrace(trace);
      SInt32 id = mTraces->size();
      stackTrace->putID(id);
      INFO_ASSERT(mMemDump, "Mem Capture not initialized.");
      bumpToken();
      *mMemDump << 't';
      stackTrace->dump(mMemDump);
      mTraces->insert(stackTrace);
      INFO_ASSERT((SInt32) mTraces->size() == id + 1, "Consistency check failed.");
    }
    else
      stackTrace = *p;

    if (mPtrCache == NULL)
      writeAlloc(allocedPtr, size, stackTrace);
    else {
      mPtrCache->insert(allocedPtr, size, stackTrace);
    }

    pop();
  } // if
} // void MemHistogram::record

void MemHistogram::writeAlloc(void* ptr, size_t size, MemStackTrace* trace)
{
  push();
  INFO_ASSERT(mMemDump, "Mem Capture not initialized.");
  bumpToken();
  *mMemDump << 'a' << ptr << trace->getID() << size;
  pop();
}

void MemHistogram::erase(void* ptr)
{
  if (isActive())
    return;
  push();
  
  // If we are freeing a cached allocation, just forget we ever
  // allocated it, rather than writing the matched alloc/free calls
  if (mPtrCache && !mPtrCache->erase(ptr)) {
    // Otherwise we have to write out the 'free' record
    
    INFO_ASSERT(mMemDump, "Mem Capture not initialized.");
    bumpToken();
    *mMemDump << 'f' << (UIntPtr)ptr;
  }
  pop();
} // void MemHistogram::erase

void MemHistogram::flushCache()
{
  push();
  if (mPtrCache != NULL)
  {
    for (PtrCache::iterator p = mPtrCache->begin(); p != mPtrCache->end(); ++p)
      writeAlloc(p->second->mPtr, p->second->mSize, p->second->mStackTrace);
    mPtrCache->clear();
  }
  pop();
}

bool MemHistogram::flush()
{
  if (mMemDump == NULL)
  {
    UtIO::cout() << "Memory dump file is not open\n";
    return false;
  }

  push();
  flushCache();
  bool ret = mMemDump->flush();
  if (!ret)
    //UtIO::cout() << "Memory dump file error: " << mMemDump->getError() << "\n";
    UtIO::cout() << "Memory dump file error: " << mMemDump->getErrmsg() << "\n";
  pop();
  return ret;
}

void MemHistogram::checkpoint(const char* filename)
{
  INFO_ASSERT(mMemDump != NULL, "checkpoint only can be called during capture.");
  push();
  flushCache();
  bumpToken();
  *mMemDump << 'p' << filename;
  flush();
  pop();
}
  
struct MemTraceCmp {
  bool operator()(const MemStackTrace* t1, const MemStackTrace* t2)
    const
  {
    return *t1 < *t2;
  }
};

#if pfMSVC
bool MemHistogram::print(const char*)
{
  return true;
}
#else
bool MemHistogram::print(const char* filename)
{
  bool file_found = false;
  UtOStream* file = NULL;
  UtString printfilename(filename);
  if (filename == NULL) {
    file = &UtIO::cout();
    file_found = true;
  } else {
    file = new UtOBStream(filename);
    if (file->is_open()) {
      file_found = true;
    } else {
      UtIO::cout() << file->getErrmsg() << "\n";
      UtIO::cout().flush();
      // there was a problem with the specified filename, if it is an
      // absolute path then see if we can put it in the current directory.
      // This can be useful if the data has been copied from a
      // customer site/directory, and thus it points to a directory
      // that is not locally available
      if ( filename[0] == '/' ){
        delete file;            // we are going to reuse file
        file = NULL;
        UtString fullname(filename);
        UtString shortname(".");
        size_t pos_last_slash = fullname.rfind('/');
        shortname << fullname.substr(pos_last_slash);
        file = new UtOBStream(shortname.c_str());
        if (file->is_open()) {
          file_found = true;
          printfilename = shortname;
        } else {
          UtIO::cout() << file->getErrmsg() << "\n";
          UtIO::cout().flush();
          return false;
        }
      }
      if ( ! file_found ) {  
        return false;
      }
    }
  }    

  push();
  {
    if (mListPointers)
    {
      for (TraceMap::iterator p = mAllocs->begin(); p != mAllocs->end(); ++p)
      {
        void* ptr = p->first;
        TraceEntry& te = p->second;
        MemStackTrace* trace = te.first;
        trace->push_back(ptr);
      }
    }

    std::sort(mTraceVec->begin(), mTraceVec->end(), MemTraceCmp());
    typedef Loop<TraceVector> TraceLoop;
    TraceLoop loop(*mTraceVec);
    mSymbolTable->print<MemStackTrace, TraceLoop, UInt32>
      (NULL, mDetail, loop, mTotalBytes, *file);
    // loop must go out of scope before we re-enable

    if (mListPointers)
    {
      for (TraceSet::iterator p = mTraces->begin(); p != mTraces->end(); ++p)
      {
        MemStackTrace* trace = *p;
        trace->clear();
      }
    }
  }
  pop();

  UtIO::cout() << "Printed memory dump " << mDetail
               << " to file " << printfilename << "\n";
  UtIO::cout().flush();

  return true;
} // bool MemHistogram::replayPrint
#endif

void MemReportOutOfMemory();    // defined in MemSystem.cxx

UInt32 MemHistogram::outOfMemory()
{
  if (mMemExhausted)
    return 0;     // avoid infinite recursion if reserve insufficient
  mMemExhausted = true;

  // When we run out of memory, we want to leave a record of
  // what was allocated in the memory database.  To do that,
  // need a little memory in reserve.
  UInt32 freedCount = 0;
  if (mMemReserve != NULL)
  {
    ::free(mMemReserve);
    mMemReserve = NULL;
    freedCount = MEM_RESERVE_SIZE;
  }

  // If MemHistogram was busy recording a memory operation when
  // we ran out of memory, we are going to need to complete that,
  // but then immediately checkpoint and exit.
  if (!isActive())
  {
    checkpoint("carbon_mem_abort");
    flush();
    mMemDump->close();
  }
  return freedCount;
} // UInt32 MemHistogram::outOfMemory

void MemHistogram::push()
{
  ++mBusyDepth;
}

void MemHistogram::pop()
{
  INFO_ASSERT(mBusyDepth > 0, "BusyDepth underflow.");
  --mBusyDepth;
  if ((mBusyDepth == 0) && mMemExhausted && mMemDump->is_open())
  {
    // We must have run out of memory while recording a memory
    // operation.  That operation is now over, so checkpoint and exit.
    push();
    checkpoint("carbon_mem_abort");
    flush();
    mMemDump->close();
    MemReportOutOfMemory();     // does not return
  }
}
