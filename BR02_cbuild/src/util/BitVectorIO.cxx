// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// This file implements the OStream operators for BVRefs, using DynamicBitVectors
// as temps.  Doing this here rather than in UtIOStream limits confusing error
// message about BVRefs when a compilation problem has to do with some other
// aspect of IOStreams.

#include "util/DynBitVector.h"
#include "util/BitVector.h"
#include "util/UtIOStream.h"

#undef LONG_BIT
#define LONG_BIT  (UInt32)(CHAR_BIT * sizeof (UInt32))
template<class BVR> 
static bool sFormatBitvecRef(UtOStream& os, const BVR& v)
{
  // Turn the BVref into a local DynBitVector, then send it to the stream.
  //  This allows the stream to be configured for any radix/format and
  //  we let formatBignum do the right thing.
  UInt32 size = v._M_bsiz;
  UInt32 offset = v._M_bpos;
  UInt32 numwords = (size+offset + LONG_BIT - 1 )/ LONG_BIT;
  // first make a copy of all the necessary bits (plus the offset area)
  DynBitVector tempCpy((size+offset), v._M_wp, numwords);
  // then shift out the offset area and resize the result so that it
  // only contains the bits defined by the BVref
  tempCpy >>= offset;
  tempCpy.resize(size);
  return os.formatBignum( tempCpy.getUIntArray(), size);
}

UtOStream& operator<<(UtOStream& os, const BVref<false>& v)
{
  sFormatBitvecRef(os, v);
  return os;
}

UtOStream& operator<<(UtOStream& os, const BVref<true>& v)
{
  sFormatBitvecRef(os, v);
  return os;
}
#undef LONG_BIT
