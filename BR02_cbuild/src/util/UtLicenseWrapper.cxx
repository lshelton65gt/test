// -*- C++ -*-
/*****************************************************************************


 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/MutexWrapper.h"

#if pfWINDOWS
#include <process.h>  // for _exit()
#else
#include <unistd.h>  // for _exit()
#endif

#include "util/UtLicenseWrapper.h"
#include "util/UtIOStream.h"
#include "util/CarbonAssert.h"

class UtLicenseWrapperMsgCB : public UtLicense::MsgCB
{
public:
  CARBONMEM_OVERRIDES

  virtual ~UtLicenseWrapperMsgCB() {}

  //! Exit CB. This must not return!
  /*!
    If we lose the connection to the license server (over a long
    period of time) we must exit. This has a catch all mechanism
    underneath it. If this does not return, the error is printed to
    stderr and then a forced exit happens. 

    \warning All applications that use licensing must define this
    and exit appropriately.

    \param reason The reason why we are exiting. Print this to the
    message context of the application.
  */
  virtual void exitNow(const char* reason)
  {
    UtIO::cout() << reason << UtIO::endl;
    exit(1);
  }


  // These 4 functions only need to do something if license queuing is
  // turned on. See compiler_driver/CarbonContextLicMsg.cxx for an
  // example.

  virtual void waitingForLicense(const char* /*feature*/)
  {}

  virtual void queuedLicenseObtained(const char* /*feature*/)
  {}

  virtual void requeueLicense(const char* /*featureName*/)
  {}

  virtual void relinquishLicense(const char* /*featureName*/)
  {}
};


UtLicense * UtLicenseWrapper::mLicense = NULL;
UtLicense::MsgCB * UtLicenseWrapper::mMsgCB = NULL;
UInt32 UtLicenseWrapper::mLicenseCount = 0;
UInt32 UtLicenseWrapper::mHeartbeatRequestCount = 0;
UInt32 UtLicenseWrapper::mRequestsPerHeartbeat = 0;

// We need a mutex to ensure thread-safe access to the static data
MUTEX_WRAPPER_DECLARE(sMutex);

UtLicenseWrapper::UtLicenseWrapper(const char *featureName, const char *featureDescription)
  :mFeatureName(featureName), mFeatureDescription(featureDescription)
{
  // We may allocate memory below, so install a custom memory pool
  // along with the mutex.
  MutexWrapperMemPool mutex(&sMutex);

  // if this is the first license checkout, allocate a UtLicense to
  // share across all licenses.
  if (mLicense == NULL) {
    mMsgCB = new UtLicenseWrapperMsgCB();
    mLicense = new UtLicense(mMsgCB, true);
    mLicense->setMode(UtLicense::eRefCount);
    mLicense->putServerTimeout(UtLicense::cServerTimeoutMax);
  }

  UtString reason;
  UtString internalLicReason;
  // If we fail to checkout an actual license try checking out the
  // internal license. 
  if (! mLicense->checkoutFeatureName(mFeatureName.c_str(), &reason) &&
      ! mLicense->checkout(UtLicense::eDIAGNOSTICS, &internalLicReason)) {
    UtIO::cout() << reason << UtIO::endl;
    UtIO::cout().flush();
    
    UtIO::cout() << "Failed to obtain license for " << mFeatureDescription << "." << UtIO::endl;
	 
    // If for some reason MX_MSG_FATAL didn't exit
    _exit(1);
  }

  // increment count of licenses in use
  ++mLicenseCount;

  // update number of heartbeat requests we should expect to receive before
  // we need to call UtLicense::heartbeat
  mRequestsPerHeartbeat = computeRequestsPerHeartbeat();
}

UtLicenseWrapper::~UtLicenseWrapper()
{
  // We may allocate memory below, so install a custom memory pool
  // along with the mutex.
  MutexWrapperMemPool mutex(&sMutex);

  // return the license to the server
  mLicense->releaseFeatureName(mFeatureName.c_str());

  // one fewer license in use
  --mLicenseCount;

  // update number of heartbeat requests we should expect to receive before
  // we need to call UtLicense::heartbeat
  mRequestsPerHeartbeat = computeRequestsPerHeartbeat();

  // free the UtLicense context if there are no more licenses checked out.
  if (mLicenseCount == 0) {
    delete mLicense;
    delete mMsgCB;
    mLicense = NULL; // static - make sure it is zero for next instance
    mMsgCB = NULL;   // static - make sure it is zero for next instance
  }
}

/*
 * For now (14-Sep-2006), after discussion w/ Matt Grasse it is not
 * important that we heartbeat the transactor adapter licenses.
 *
 * If we need to re-enable this, we should consider an algorithm that
 * checks the time before calling the heartbeat function, dynamically
 * adjusting mRequestsPerHeartbeat to avoid making too many 
 * UtLicense::heartbeat calls.
 *
 * Note that UtLicense::heartbeat does not necessarily call the
 * server; it calls lm_heartbeat which may actualy do its own time
 * check.  Since checking the time is expensive, we want to minimize
 * those calls anyway.
 */

/*
void UtLicenseWrapper::heartbeat()
{
  // Count this request for a heartbeat
  ++mHeartbeatRequestCount;

  // if we have been called enough times to update the server
  if (mHeartbeatRequestCount > mRequestsPerHeartbeat) {

    // let the server know we still need the licenses we have checked out
    mLicense->heartbeat();

    // start counting again
    mHeartbeatRequestCount = 0;
  }
}
*/

UInt32 UtLicenseWrapper::computeRequestsPerHeartbeat()
{
  // Assume for now that the slowest we can go is 1Hz, and that a heartbeat is
  // requested once per cycle, per license wrapper instance.

  // If the user is sitting at a breakpoint or something, they will probably 
  // lose their license. But hopefully it will be available when they continue.

  // There is a real danger here that we will be requesting too many heartbeats.
  // Without checking the time, there is no easy way.
  //
  // Perhaps we should consider checking the time only when we
  // actually request a heartbeat.  Breakpoints would still screw us
  // up, but we should in theory correct quickly.
  return UtLicense::cServerTimeoutMax * mLicenseCount;
}

