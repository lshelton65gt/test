// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2007-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtIOStream.h"
#include "util/ArgProc.h"
#include "util/FileCollector.h"
#include "util/ShellMsgContext.h"
#include "util/UtList.h"
#include "util/RandomValGen.h"
#include "util/AtomicCache.h"
#include "util/ConstantRange.h"
#include "exprsynth/ExprFactory.h"
#include "iodb/IODBRuntime.h"
#include "iodb/ScheduleFactory.h"
#include "iodb/IODBRuntimeAliasBOM.h"
#include "iodb/IODBUserTypes.h"
#include "shell/ShellGlobal.h"
#include "shell/CarbonDBRead.h"
#include "shell/carbon_dbapi.h"
#include "shell/carbon_misc.h"

/* \file IODB reader
 */

// command line option strings
static const char *scOutput       = "-o";
static const char *scHier         = "-hier";
static const char *scPorts        = "-ports";
static const char *scNets         = "-nets";
static const char *scUnelaborated = "-unelab";
static const char *scDirectives   = "-dirs";
static const char *scType         = "-type";
static const char *scNoLoc        = "-noLoc";
static const char *scNoUnelab     = "-noUnelab";
static const char *scAll          = "-all";

class Context;       // forward declaration

class Separator {
public:

  Separator (const char *prefix = ": ", const char *separator = ", ") : 
    mCount (0), mPrefix (prefix), mSeparator (separator) {}

  void reset () { mCount = 0; }

  UInt32 mCount;
  const char *mPrefix;
  const char *mSeparator;
};

UtOStream &operator << (UtOStream &s, Separator &separator)
{
  if (separator.mCount++ > 0) {
    s << separator.mSeparator;
  } else {
    s << separator.mPrefix;
  }
  return s;
}

//! \class IODBInspector

/*! Opens up an IODB file and dumps a variety of information about the symbol
 *  tables read from the database.
 */

class IODBInspector {
public:

  enum Options {
    cHIER         = 1<<0,               //!< traverse the symbol tables hierarchically
    cPORTS        = 1<<1,               //!< list ports
    cNETS         = 1<<2,               //!< list all nets in modules
    cUNELABORATED = 1<<3,               //!< show the unelaborated symbol table
    cDIRECTIVES   = 1<<4,               //!< show the directives
    cTYPE         = 1<<5,               //!< list types for all ports and nets in modules
    cNOLOC        = 1<<6,               //!< do not output source locations
    cNOUNELAB     = 1<<7,               //!< do not output corresponding unelaborated symbols
    cALL = cPORTS|cNETS|cTYPE           //!< output all details for a net
  };

  //! ctor
  /*! The constructor opens up the database file:
   *  \arg msg_context  error stream
   *  \arg filename     name of the database file
   *  \arg typ          type of database to open (io.db, symtab.db or gui.db)
   *  \arg
   */

  IODBInspector (MsgContext &msg_context, SourceLocatorFactory &sources,
    UtString filename, CarbonDBType type) : 
    mMsgContext (msg_context), mFilename (filename)
  {
    // build all that necessary junk....
    mAtomicCache = new AtomicCache;
    mShellSymTabBOM = new ShellSymTabBOM;
    mScheduleFactory = new SCHScheduleFactory;
    mExprFactory = new ESFactory;
    mShellSymTabBOM->putScheduleFactory (mScheduleFactory);
    mSymTab = new STSymbolTable (mShellSymTabBOM, mAtomicCache);
    // construct the IODBRuntime object
    mDB = new IODBRuntime (mAtomicCache, mSymTab, ShellGlobal::getProgErrMsgr(), mScheduleFactory, mExprFactory, &sources);
    if (mDB != NULL && mDB->readDB (filename.c_str (), type)) {
      // the database was read successfully
    } else {
      mMsgContext.UnableToOpenDB (filename.c_str ());
      mDB = NULL;
    }
  }

  virtual ~IODBInspector ()
  {
    // take out the trash
    delete mExprFactory;
    delete mSymTab;
    delete mShellSymTabBOM;
    delete mScheduleFactory;
    delete mAtomicCache;
    if (mDB != NULL) {
      // if the read failed, then the ctor will have deleted and nulled mDB
      delete mDB;
    }
  }

  //! Write the contents of the database file to an output stream
  bool operator () (UtOStream &out, const UInt32 options)
  {
    if (mDB == NULL) {
      return false;                     // something broke opening the database
    }
    out << "Symbol table nodes: " << mSymTab->numNodes () << "\n";
    const STSymbolTable *symtab;
    if (options & cUNELABORATED) {
      symtab = mDB->getUnelaboratedSymtab ();
    } else {
      symtab = mSymTab;
    }
    if (options & cHIER) {
      // Write the symbol table hierarchically
      STSymbolTable::SortedRootIter loop = const_cast <STSymbolTable *> (symtab)->getSortedRootIter ();
      while (!loop.atEnd ()) {
        STSymbolTableNode *node = *loop;
        STBranchNode *branch;
        if ((branch = node->castBranch ()) == NULL) {
          UtString name;
          node->verilogCompose (&name);
          mMsgContext.ExpectedABranchNode (name.c_str ());
        } else {
          Separator separator;
          describe (out, branch, separator, options);
        }
        ++loop;
      }
    } else {
      STSymbolTable::NodeLoopSorted loop = symtab->getNodeLoopSorted ();
      while (!loop.atEnd ()) {
        STSymbolTableNode *node = *loop;
        STAliasedLeafNode *leaf;
        STBranchNode *branch;
        UtString name;
        node->verilogCompose (&name);
        if ((leaf = node->castLeaf ()) != NULL) {
          // it is a leaf node
          Separator separator;
          describe (out, leaf, separator, options);
        } else if ((branch = node->castBranch ()) != NULL) {
          // it is a branch node
          Separator separator;
          describe (out, branch, separator, options);
        } else {
          // don't know what the !%@^! it is
          mMsgContext.UnableToClassifyNode (name.c_str ());
        }
        ++loop;
      }
    }
    if (options & cDIRECTIVES) {
      list_directives (out);
    }
    return true;
  }
 
private:

  MsgContext &mMsgContext;              //!< all of the
  AtomicCache* mAtomicCache;            //!< ... usual crap
  ShellSymTabBOM* mShellSymTabBOM;      //!< ... that we need to
  SCHScheduleFactory* mScheduleFactory; //!< ... do anything with
  STSymbolTable* mSymTab;               //!< ... IODB classes
  ESFactory* mExprFactory;              //!< ... of any flavour
  UtString mFilename;                   //!< filename of the database
  IODBRuntime* mDB;                     //!< database under examination

  //! List the directives
  void list_directives (UtOStream &out) const
  {
    // design directives
    IODBDesignDirective::List &design_dirs = mDB->getDesignDirectives ();
    for (IODBDesignDirective::List::iterator it = design_dirs.begin (); it != design_dirs.end (); it++) {
      (*it)->print (out);
    }
    // module directives
    IODBModuleDirective::List &module_dirs = mDB->getModuleDirectives ();
    for (IODBModuleDirective::List::iterator it = module_dirs.begin (); it != module_dirs.end (); it++) {
      (*it)->print (out);
    }
  }

  //! Describe a branch in the symbol table
  /*! \arg out     stream for writing the description
   *  \arg branch  the symbol table branch node under examination
   *  \arg options bitmask over enum Options describing what to write
   *  \arg indent  indentation depth
   */
  void describe (UtOStream &out, STBranchNode *branch, Separator &separator, 
    const UInt32 options, const UInt32 indent = 0)
  {
    UtString name;
    branch->verilogCompose (&name);
    UtString leaders (indent, ' ');
    if (options & cHIER) {
      // We are doing a hierarchical traversal
      out << leaders << "enter scope " << name.c_str ();
      contents (out, branch, separator, options, indent);
      out << "\n";
      if (options & cPORTS) {
        // list the ports
        list_ports (out, branch, eInputPorts,  options, indent + 1);
        list_ports (out, branch, eOutputPorts, options, indent + 1);
        list_ports (out, branch, eBidiPorts,   options, indent + 1);
      }
      // Tranverse the children of this node
      for (int i = 0; i < branch->numChildren (); i++) {
        STSymbolTableNode *node = branch->getChild (i);
        if (node->castBranch () != NULL) {
          Separator separator;
          describe (out, node->castBranch (), separator, options, indent + 1);
        } else if (!(options & cNETS)) {
          // not writing out nets
        } else if (node->castLeaf () != NULL) {
          Separator separator;
          describe (out, node->castLeaf (), separator, options, indent + 1);
        } else {
          // don't know what the !%@^! it is
          mMsgContext.UnableToClassifyNode (name.c_str ());
        }
      }
      out << leaders << "exit scope " << name.c_str () << "\n";
    } else {
      out << leaders << "branch " << name.c_str () << " ";
      contents (out, branch, separator, options, indent);
      out << "\n";
    }
  }

  void show_location (UtOStream &out, STSymbolTableNode *node, Separator &separator, 
    const UInt32 options)
  {
    SourceLocator loc;
    if (options & cNOLOC) {
      // suppress source location
    } else if (!mDB->findLoc (node, &loc)) {
      separator.reset ();
      out << ": no location";
    } else {
      UtString b;
      separator.reset ();
      loc.compose (&b);
      out << ": " << b;
    }
  }

  void show_unelaborated (UtOStream &out, STSymbolTableNode *node, Separator &separator,
    const UInt32 options)
  {
    STSymbolTableNode *unelaborated;
    if (options & cNOUNELAB) {
      // suppress unelaborated symbol listing
    } else if (options & cUNELABORATED) {
      // this is an unelaborated symbol
    } else if (!mDB->getUnelaborated (node, &unelaborated)) {
      separator.reset ();
      out << ": no unelaborated";
    } else {
      UtString b;
      separator.reset ();
      unelaborated->compose (&b);
      out << ": unelab=" << b;
    }
  }

  //! Describe a leaf in the symbol table
  /*! \arg out     stream for writing the description
   *  \arg leaf    the symbol table leaf node under examination
   *  \arg options bitmask over enum Options describing what to write
   *  \arg indent  indentation depth
   */
  void describe (UtOStream &out, STAliasedLeafNode *leaf, Separator &separator,
    const UInt32 options, const UInt32 indent = 0)
  {
    UtString leaders (indent, ' ');
    UtString name;
    leaf->verilogCompose (&name);
    out << leaders;
    if ((options & cHIER) == 0) {
      out << "leaf: ";
    }
    out << name.c_str ();
    contents (out, leaf, separator, options, indent);
    out << "\n";
  }

  //! List some the contents of a node
  void contents (UtOStream &out, STSymbolTableNode *node, Separator separator,
    const UInt32 options, const UInt32 indent)
  {
    // write out letters indicating what data in embedded in symbol table entry
    bool is_elaborated = (options & cUNELABORATED) == 0;
    if (mDB->isInput (node, is_elaborated ? IODBRuntime::eElaborated : IODBRuntime::eUnelaborated)) {
      out << separator << "in";
    }
    if (mDB->isOutput (node, is_elaborated ? IODBRuntime::eElaborated : IODBRuntime::eUnelaborated)) {
      out << separator << "out";
    }
    if (mDB->isBidirect (node, is_elaborated ? IODBRuntime::eElaborated : IODBRuntime::eUnelaborated)) {
      out << separator << "bi";
    }
    // Output information embedded in IODBGenTypeEntry data
    // Output leaf-specific information embedded in the IODBInstrinsic data
    STAliasedLeafNode *leaf = node->castLeaf ();
    const IODBIntrinsic *intrinsic;
    if (options & cUNELABORATED) {
      // there is no type information with the symbol
    } else if (leaf == NULL) {
      // it's a leaf
    } else if ((intrinsic = mDB->getLeafIntrinsic (leaf)) == NULL) {
      // no intrinsics
      out << separator << "*** no intrinsics ***";
    } else {
      switch (intrinsic->getType ()) {
      case IODBIntrinsic::eScalar:
        out << separator << "scalar";
        break;
      case IODBIntrinsic::eVector:
        out << separator << "[" << intrinsic->getMsb () << ":" << intrinsic->getLsb () << "]";
        break;
      case IODBIntrinsic::eMemory:
        {
          const ConstantRange *range = intrinsic->getVecRange ();
          out << separator << "[" << intrinsic->getMsb () << ":" << intrinsic->getLsb () << "]";
          out << "[" << range->getMsb () << ":" << range->getLsb () << "]";
        }
        break;
      default:
        out << "*error:" << __LINE__ << "*";
        break;
      }
    }
    // Output the source location associated with the symbol
    show_location (out, node, separator, options);
    // Output the unelaborated symbol
    show_unelaborated (out, node, separator, options);
    // Output the user type information
    if ((options & cTYPE) && !(options & cUNELABORATED)) {
      const UserType* ut = mDB->getUserType(node);
      if (ut != NULL) {
        UtString leaders (indent + 2, ' ');
        out << separator << "user type:\n" << leaders;
        ut->print(&out);
      }
    }
  }

  //! List the ports of a module
  /*! \arg out       stream for writing the port list
   *  \arg branch    symbol that denotes a module or module instance
   *  \arg direction which flavour of port to output
   *  \arg indent  indentation depth
   */

  enum PortDirection {eInputPorts, eOutputPorts, eBidiPorts};

  void list_ports (UtOStream &out, STBranchNode *branch, const PortDirection direction,
    const UInt32 options, const UInt32 indent)
  {
    UtString leaders (indent, ' ');    // indentation prefix
    int n = 0;
    IODBRuntime::ElaborationState is_elaborated = 
      (options & cUNELABORATED) == 0 ? IODBRuntime::eElaborated : IODBRuntime::eUnelaborated;
    for (int i = 0; i < branch->numChildren (); i++) {
      STAliasedLeafNode *leaf = branch->getChild (i)->castLeaf ();
      if (leaf == NULL 
        || (direction == eBidiPorts && !mDB->isBidirect (leaf, is_elaborated))
        || (direction == eInputPorts && !mDB->isInput (leaf, is_elaborated))
        || (direction == eOutputPorts && !mDB->isOutput (leaf, is_elaborated))) {
        // not interested
      } else if (n++ == 0) {
        // first intersting leaf, so indent and write the prefix
        out << leaders;
        switch (direction) {
        case eInputPorts: out << "input"; break;
        case eOutputPorts: out << "output"; break;
        case eBidiPorts: out << "bidi"; break;
        }
        // now write the leaf
        UtString b0;
        leaf->compose (&b0);
        out << " " << b0;
      } else {
        // write a separator and then the leaf
        UtString b0;
        leaf->compose (&b0);
        out << ", " << b0;
      }
    }
    if (n > 0) {
      // wrote something so terminate the line
      out << "\n";
    }
  }

};

//! \class Context
/*! Context wraps all the processing for the reader. */

class Context {
public:

  //! ctor for class Context
  Context (MsgContext &msg_context, SourceLocatorFactory &sources) : 
    mMsgContext (msg_context), mSources (sources), 
    mOutput (&UtIO::cout ()), mOwnOutput (false), 
    mFiles (msg_context)
  { 
    configureArgs (); 
  }

  virtual ~Context ()
  {
    if (mOwnOutput) {
      delete mOutput;
    }
  }

  bool operator () (int *argc, char **argv)
  {
    if (!processArgs (argc, argv)) {
      return false;                     // failure
    } else if (getFilename ().size () == 0) {
      mMsgContext.ReadIODBError ("no files specified on the command line.");
      return false;
    }        
    IODBInspector inspector (mMsgContext, mSources, getFilename (), getDBType ());
    return inspector (*mOutput, getInspectorArgs ());
  }

  //! Access the filenames specified on the command line
  UtString getFilename () const { return mFiles.getFilename (); }

  //! \return the database type
  CarbonDBType getDBType () const { return mFiles.getDBType (); }

  //! \return options specified for the inspector
  UInt32 getInspectorArgs () const
  {
    UInt32 mask = 0;
    if (mArgs.getBoolValue (scHier)) {
      mask |= IODBInspector::cHIER;
    }
    if (mArgs.getBoolValue (scPorts)) {
      mask |= IODBInspector::cPORTS;
    }
    if (mArgs.getBoolValue (scNets)) {
      mask |= IODBInspector::cNETS;
    }
    if (mArgs.getBoolValue (scUnelaborated)) {
      mask |= IODBInspector::cUNELABORATED;
    }
    if (mArgs.getBoolValue (scDirectives)) {
      mask |= IODBInspector::cDIRECTIVES;
    }
    if (mArgs.getBoolValue (scType)) {
      mask |= IODBInspector::cTYPE;
    }
    if (mArgs.getBoolValue (scAll)) {
      mask |= IODBInspector::cALL;
    }
    if (mArgs.getBoolValue (scNoLoc)) {
      mask |= IODBInspector::cNOLOC;
    }
    if (mArgs.getBoolValue (scNoUnelab)) {
      mask |= IODBInspector::cNOUNELAB;
    }
    return mask;
  }

private:

  MsgContext &mMsgContext;              //!< whining stream
  SourceLocatorFactory &mSources;       //!< source location database
  ArgProc mArgs;                        //!< argument vector handler
  UtOStream *mOutput;                   //!< the output stream (default to UtIO::cout)
  bool mOwnOutput;                      //!< iff this instance owns mOutput

  //! Configure the argument processor
  void configureArgs ()
  {
    mArgs.setDescription ("Annotation Store Utility", "readannotationstore",
      "Utility to read annotation store information.");
    mArgs.addString (scOutput, "Send the output to a file instead of stdout.",
      NULL, true, false, 1);
    mArgs.addBool (scHier, "Enumerate the symbol tables hierarchically.", 
      false, 1);
    mArgs.addBool (scPorts, "List the ports with each hierarchically enumerated module.", 
      false, 1);
    mArgs.addBool (scNets, "List all the nets with each hierarchically enumerated module..", 
      false, 1);
    mArgs.addBool (scType, "List the user type information for ports and nets for each hierarchically enumerated module.",
      false, 1);
    mArgs.addBool (scAll, "List all the details for ports and nets",
      false, 1);
    mArgs.addBool (scNoLoc, "Do not output source locations.",
      false, 1);
    mArgs.addBool (scNoUnelab, "Do not output mapping to unelaborated symbols.",
      false, 1);
    mArgs.addBool ("-h", "Print the documentation for annotation store reader options and exit.", 
      false, 1);
    mArgs.addBool (scUnelaborated, "Show the unelaborated symbol table.", 
      false, 1);
    mArgs.addBool (scDirectives, "List the directives.", 
      false, 1);
    mArgs.addSynonym ("-h", "-help", false);
  }

  //! Process the command line arguments
  bool processArgs (int *argc, char **argv)
  {
    mArgs.accessHiddenOptions ();
    mArgs.allowUsageAll ();
    // run the parser
    UtString errMsg;
    int numOptions;
    if (mArgs.parseCommandLine (argc, argv, &numOptions, &errMsg, &mFiles) != ArgProc::eParsed) {
      mMsgContext.ReadIODBError (errMsg.c_str ());
      return false;
    } else if (!mFiles.isOK ()) {
      // command line errors were reported
      return false;
    } else if (mArgs.getBoolValue ("-h")) {
      // just print the help strinf and then go away
      printUsage ();
      exit(0);
    }
    const char *filename;
    UtOStream *output;
    if (mArgs.getStrValue (scOutput, &filename) != ArgProc::eKnown) {
      // no -o options specified, mOutput will stay pointing at UtIO::cout
    } else if ((output = new UtOFStream (filename)) == NULL) {
      // allocation of the object failed... this is bad
      mMsgContext.CannotOpenOutputFile (filename, "allocation failed");
    } else if (!output->is_open ()) {
      // unable to open the output file
      UtString oserror;
      OSGetLastErrmsg (&oserror);
      mMsgContext.CannotOpenOutputFile (filename, oserror.c_str ());
      delete output;
    } else {
      // opened an output file successfully
      mOutput = output;
      mOwnOutput = true;
    }
    return true;
  }

  //! Print annotation store reader usage instructions
  void printUsage ()
  {
    UtString usage;
    mArgs.getUsageVerbose(&usage, false);
    UtIO::cout() << usage << UtIO::endl;
  }

  //! \class Context::Collector
  class Collector : public FileCollector {
  public:

    Collector (MsgContext &msg_context) : 
      FileCollector (), mMsgContext (msg_context), mNErrors (0)
    {}

    virtual bool scanArgument (const char *, const char *)
    { 
      return false;
    }

    UtString getFilename () const { return mFilename; }
    CarbonDBType getDBType () const { return mDBType; }

    // \return iff no error were found on the command line
    bool isOK () const { return mNErrors == 0; }

    virtual void addArgument (const char *arg)
    {
      // only dumping a single database file
      if (mFilename.length () > 0) {
        mMsgContext.ReadIODBError2 (arg, "multiple input files listed on the command line");
        mNErrors++;
        return;
      }
      // cache away the filename
      mFilename = arg;
      // Identify the database file type by scanning the filename. Database
      // file are call libfoo.type.db where type is one of io, symtab or gui.
      UInt32 i = 0;
      // look for the first '.'
      while (arg [i] != '\0' && arg [i] != '.') {
        i++;
      }
      // look for the second '.'
      UInt32 j = i + 1;
      while (arg [j] != '\0' && arg [j] != '.') {
        j++;
      }
      // classify the database from the .type. part of the filename
      if (arg [i] != '.' || arg [j] != '.') {
        mMsgContext.ReadIODBError2 (arg, "filename is not of the form libfoo.type.db");
      } else if (!strncmp (arg + i, ".gui.", 5)) {
        mDBType = eCarbonGuiDB;
      } else if (!strncmp (arg + i, ".io.", 4)) {
        mDBType = eCarbonIODB;
      } else if (!strncmp (arg + i, ".symtab.", 8)) {
        mDBType = eCarbonFullDB;
      } else {
        mMsgContext.ReadIODBError2 (arg, "cannot infer database type from the filename");
        mNErrors++;
        return;
      }
    }

  private:

    MsgContext &mMsgContext;            //!< for whining
    UtString mFilename;                 //!< the single expected filename
    UInt32 mNErrors;                    //!< number of command line errors seen
    CarbonDBType mDBType;               //!< type of the database inferred from the filename

  };

  Collector mFiles;                     //!< for gathering arguments not associated with options

};

/***************
 * entry point *
 ***************/

int main (int argc, char **argv)
{
  SourceLocatorFactory sources;
  MsgStreamIO msg_stream (stdout, true);
  MsgContext msg_context;
  msg_context.addReportStream (&msg_stream);
  Context context (msg_context, sources);
  return context (&argc, argv) ? 0 : 1;
}

