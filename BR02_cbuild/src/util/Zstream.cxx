// -*- C  -*-
/******************************************************************************
 Copyright(c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/OSWrapper.h"
#include "util/UtStreamSupport.h"
#include "util/Zstream.h"
#include "util/UtIOStream.h"
#include "util/UtArray.h"
#include "util/RandomValGen.h"
#include "util/UtIStream.h"

#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#if pfMSVC
#include <io.h>
#else
#include <unistd.h>
#endif

#include <cctype>
#include <cmath>
#include <cstdlib>

#if pfMSVC
#include "zlib.h"
#else
#include <zlib.h>
#endif

/*!
  \file
  Implementation of classes described in Zstream.h
*/

// 64k lower bound
#define BUF_LOWER_BOUND 65536

// 8M upper bound
#define BUF_UPPER_BOUND (1 << 23)

const unsigned char Zstream::cCarbonMagic[cMagicFlagSize] = { 0xa5, 0xd2, 0xf3 };
const unsigned char Zstream::cMagicName[7] = "carbon";
const unsigned char Zstream::cCarbonTail[cMagicFlagSize] = {0,0,0};

extern "C" 
{
  //! C function to implement a mem-managed calloc for zlib
  static voidpf sCarbonCalloc(voidpf, uInt items, uInt size)
  {
    size_t actualSize = items * size;
    void* ret = CarbonMem::malloc(actualSize);
    ::memset(ret, 0, actualSize);
    return static_cast<voidpf>(ret);
  }
  
  //! C function to implement a mem-managed free for zlib
  static void sCarbonFree(voidpf, voidpf address)
  {
    CarbonMem::free(static_cast<void*>(address));
  }
}

const char Zendl = '\n';

//! Class to encrypt buffers
/*!
  Formerly known as Cipher, but I need to outline the mask function
  because it is too slow inlined. The name has to be believable but
  not related to encryption, because mask will show up in an nm dump.
*/
class Zstream::CIPHER
{
public: 
  CARBONMEM_OVERRIDES
  //! constructor
  CIPHER() : mDoCipher(false), mReadMode(false), mSequenceBegin(30)
  {
  }

  //! destructor
  ~CIPHER() {}

  //! Set the sequence begin index
  void putSequenceBegin(UInt32 seqBegin)
  {
    mSequenceBegin = seqBegin;
    reset();
  }
  
  //! Set the cipher to read mode. Influences lastchar selection
  void setReadMode() {
    mReadMode = true;
  }
  
  //! If doCipher is false, turns off default ciphering
  void putActivate(bool doCipher) 
  {
    mDoCipher = doCipher;
  }

  //! Returns whether or not encryption is on.
  bool isCipherActivated() const {return mDoCipher;}

  void mask(char* s, int size);
  
  void reset()
  {
    mByteMask = cSequence[mSequenceBegin];
    mSequenceNumber = mSequenceBegin;
    mChunkBytesLeft = cChunkSize;
    mLastChar = 0x0;
  }
  
private:
  static const UInt32 cSequenceSize = 37;
  static const UInt32 cChunkSize = 32768;
  static const unsigned char cSequence[cSequenceSize];

  bool mDoCipher;
  bool mReadMode;
  unsigned char mByteMask;
  unsigned char mLastChar;
  UInt32 mSequenceNumber;
  UInt32 mChunkBytesLeft;
  UInt32 mSequenceBegin;

  void calcNextKey(char dataByte)
  {
    mLastChar = static_cast<unsigned char>(dataByte);
    ++mSequenceNumber;
    mSequenceNumber %= cSequenceSize;
    if (mSequenceNumber == 0)
      mSequenceNumber += mSequenceBegin;
    mByteMask += cSequence[mSequenceNumber] + mLastChar;
    mChunkBytesLeft = cChunkSize;
  }
  
  //forbid
  CIPHER(const CIPHER&);
  CIPHER& operator=(const CIPHER&);
};

void Zstream::CIPHER::mask(char* s, int size)
{
  if (mDoCipher)
  {
    int limitedSize = std::min(size, int(mChunkBytesLeft));
    char lastChar = s[limitedSize - 1];
    for (int i = 0; i < limitedSize; ++i)
      s[i] = s[i] ^ mByteMask;
    if (mReadMode)
      lastChar = s[limitedSize - 1];
    mChunkBytesLeft -= limitedSize;
    if (mChunkBytesLeft == 0)
      calcNextKey(lastChar);
    if (limitedSize < size)
      mask(s + limitedSize, size - limitedSize);
  }
}

const unsigned char Zstream::CIPHER::cSequence[] = {
  // Current version begins at index 0
  0x90, 0xab, 0xee, 0x14, 0xc1, 0x1,
  // Version 1 begins at index 6
  0x88, 0xf, 0x5e, 0x2e, 0x8f, 
  0x1, 0x5, 0x62, 0x39, 0xa2, 
  0x92, 0x2, 0x2, 0x7f, 0xa1, 
  0xa1, 0xa1, 0xb, 0xe4, 0x3c, 
  0x44, 0x67, 0x55, 0xff, 0x1, 
  0x1, 0x11, 0x80, 0xf0, 0xf2,
  0x20
};

static void sCheckKey(void* key)
{
  if (key != ZSTREAM_KEY)
  {
    // just to confuse hackers
    RandomValGen exitStatGen(OSGetPid());
    // run it once before using it
    (void) exitStatGen.URandom();
    UInt32 status = exitStatGen.URRandom(3, 255);
    exit(status);
  }
}

#define NASTY_COMPRESSBUF_INIT \
  /* another defense. If mCompressBuf is not properly initialized */ \
  /* to 0 later an invalid free will happen with a very large size. */ \
  mCompressBuf = (char*)0x1; \
  mCompressBufSize = 1 << 12;
  
Zstream::Zstream(const char* fname, void* key) :
  mDataBuf(64 * 1024),
  mFileBuf(64 * 1024)
{
  mFilename = fname;
  mFileGood = false;
  sCheckKey(key);

  NASTY_COMPRESSBUF_INIT;
}

Zstream::~Zstream()
{
  delete mCipher;
  delete mFileCipher;
  CARBON_FREE_VEC(mCompressBuf, char, mCompressBufSize);
}

void Zstream::ZSTREAM_CIPHER_INIT()
{
  mCipher = new CIPHER;
  mFileCipher = new CIPHER;
  // The first write does not write the size of the file buffer
  mFileCipher->putActivate(false);
  resizeCompressBuf();
}

void Zstream::setError(const char* msg, bool includeFilename)
{
  if (includeFilename)
    mErrMsg << mFilename << "': ";
  mErrMsg << msg;
  mFileGood = false;
}

void Zstream::setErrorInternal(const char* msg, z_stream* zlibStruct)
{
  mErrMsg = msg;
  mErrMsg << " '" << mFilename << "': ";

  if (zlibStruct && zlibStruct->msg)
    mErrMsg << zlibStruct->msg;
  else {
    UtString errBuf;
    mErrMsg << OSGetLastErrmsg(&errBuf);
  }
  
  mFileGood = false;
}

void Zstream::markRecordEnd()
{
  mFileCipher->reset();
  mCipher->reset();
}

bool Zostream::is_open() const
{
  return (mFile != -1);
}

bool Zistream::is_open() const
{
  return (mFile != NULL) && mFile->is_open();
}

Zstream::operator void*() const
{
  return fail() ? (void*) 0 : (void*)(-1);
}

int Zstream::operator!() const
{
  return fail() ? 1 : 0;
}

bool Zstream::fail() const
{
  return (! mFileGood);
}

const char* Zstream::getError() const
{
  return mErrMsg.c_str();
}

bool Zstream::checkBufResize(UInt32* size)
{
  bool doResize = false;
  UInt32 curSize = mFileBuf.size();
  if (*size <= BUF_LOWER_BOUND)
  {
    *size = BUF_LOWER_BOUND;
    if (curSize > BUF_LOWER_BOUND)
      doResize = true;
  }
  else if (*size >= BUF_UPPER_BOUND)
  {
    *size = BUF_UPPER_BOUND;
    if (curSize < BUF_UPPER_BOUND)
      doResize = true;
  }
  else
    doResize = true;
  return doResize;
}

void Zstream::resizeFileBuffer(UInt32 size)
{
  if (checkBufResize(&size))
    mFileBuf.resize(size);
}

void Zstream::resizeDataBuffer(UInt32 size)
{
  if (checkBufResize(&size))
  {
    mDataBuf.resize(size);
    resizeCompressBuf();
  }
}


void Zstream::resizeCompressBuf()
{
  // THIS IS OVERKILL
  /*
    The zlib algorithm only needs a buffer 0.1% + 12 bytes more than
    the input buffer. But, it is simpler to just double the size of
    the input buffer. If this becomes an issue, then we will do the
    more appropriate calculation.
  */
  if (mCompressBuf)
    CARBON_FREE_VEC(mCompressBuf, char, mCompressBufSize);
  mCompressBufSize = mDataBuf.size() * 2;
  mCompressBuf = CARBON_ALLOC_VEC(char, mCompressBufSize);
}

bool Zostream::close()
{
  if (is_open())
  {
    flush();
    UtString reason;
    int result = OSSysClose(mFile, &reason);
    if (result != 0)
      setError(reason.c_str());
    mFile = -1;
  }
  return mFileGood;
}

SInt64 Zistream::tellfd() {
  return mFile->tell();
}

SInt64 Zostream::tellfd() 
{
  SInt64 curPos = lseek(mFile, 0, SEEK_CUR);
  if (curPos < 0)
    setErrorInternal("lseek,SEEK_CUR failed.");
  return curPos;
}

void Zostream::seekHelper(SInt64 pos)
{
  if (lseek(mFile, (off_t) pos, SEEK_SET) < 0) {
    UtString errMsg;
    UtString errBuf;
    errMsg << mFilename << ": " << OSGetLastErrmsg(&errBuf);
    setError(errMsg.c_str());
  }
}

void Zostream::seekEnd()
{
  mDataBuf.reset();
  mFileBuf.reset();
  SInt64 curPos = lseek(mFile, 0, SEEK_END);
  if (curPos < 0)
    setErrorInternal("lseek,SEEK_END failed.");
  mFileCipher->reset();
  mCipher->reset();
}


void Zistream::seekEnd()
{
  mDataBuf.reset();
  mFileBuf.reset();
  SInt64 curPos = mFile->seek(0, UtIO::seek_end);
  if (curPos < 0)
  {
    UtString errMsg;
    UtString errBuf;
    errMsg << mFilename << ": " << OSGetLastErrmsg(&errBuf);
    setError(errMsg.c_str());
  }
  mFileCipher->reset();
  mCipher->reset();
  mFileEof = true;
  mEof = true;
}


void Zstream::getFilename(UtString* filename) const
{
  *filename = mFilename;
}

void Zstream::byteSwapOnLittleEndian(SInt64* num)
{
  if (num)
  {
    BYTESWAPONLITTLEENDIAN8(num);
  }
}

// Zostream implementation

Zostream::Zostream(const char* name, void* key) :
  Zstream(name, key),
  mFile(-1)
{
}

Zostream::Zostream(const char* name, Zostream* srcStream) :
  Zstream(name, ZSTREAM_KEY),
  mFile(srcStream->getfd())
{
  ZSTREAM_INIT();
  mFilename.assign(name);
  ZOSTREAM_SETUP_VARS();
  ZOSTREAM_WRITE_MAGIC_HEADER();
}

void Zostream::ZOSTREAM_SETUP_VARS()
{
  mFileCipher->putSequenceBegin(0);
  mCipher->putSequenceBegin(0);
  mWritePos = 0;
}

Zostream::~Zostream()
{
  close();
}


void Zostream::ZOSTREAM_WRITE_MAGIC_HEADER()
{
  /* 
     if we are writing we need to write the magic header. If we
     are reading we need to determine if this is a compressed
     stream or not.
  */
  char magic_buf[cMagicBufSize];
  if (! fail())
  {
    // The file buffer better be empty
    INFO_ASSERT(mFileBuf.getWriteIndex() == 0, "File buffer is not empty.");
    
    MEMCPY(magic_buf, cCarbonMagic, cMagicFlagSize);
    MEMCPY(magic_buf + cMagicFlagSize, cMagicName, cMagicNameSize);
    MEMCPY(magic_buf + cMagicFlagSize + cMagicNameSize, cCarbonTail, cMagicFlagSize);
    rawWriteToFileBuf(magic_buf, cMagicBufSize);
    // Need to write the header untouched
    mCipher->putActivate(false);
    mFileCipher->putActivate(false);
    // flush the header
    flush();
    // write the version and flush again.
    writeRawUInt32(cFileVersion);
    writeRawUInt32(cFileVersionReserved);
    flush();
    // Re-enable encryption
    mCipher->putActivate(true);
    mFileCipher->putActivate(true);
  }
}

void Zostream::openForWrite()
{
  if (mFile == -1)
  {
    mFileGood = true;
    UtString reason;
    int flags = O_WRONLY | O_TRUNC | O_CREAT | O_BINARY;
    mFile = OSSysOpen(mFilename.c_str(), flags, 
                      S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH, &reason);
    if (! is_open())
      setError(reason.c_str());

  }
}

void Zostream::rawWriteToFileBuf(const char* s, UInt32 n)
{
  if (mFileGood)
  {
    UInt32 written = mFileBuf.write(s, n);
    UInt32 diff = n - written;
    if (diff > 0)
    {
      flush();
      rawWriteToFileBuf(s + written, diff);
    }
  }
}

void Zostream::writeRawUInt32(UInt32 n)
{
  BYTESWAPONLITTLEENDIAN4(&n);
  rawWriteToFileBuf(reinterpret_cast<char*>(&n), sizeof(UInt32));
}

void Zostream::writeCompressBufferToFile(UInt32 numUncompressedBytes,
                                         UInt32 n)
{
  // First save the size of the amount to be written
  writeRawUInt32(numUncompressedBytes);
  writeRawUInt32(n);
  rawWriteToFileBuf(mCompressBuf, n);
}

bool Zostream::write(const char* s, UInt32 n)
{
  if (mFileGood)
  {
    UInt32 written = mDataBuf.write(s, n);
    mWritePos += written;
    UInt32 diff = n - written;
    if (diff > 0)
    {
      flushDataBuffer();
      write(s + written, diff);
    }
  }
  return mFileGood;
}

bool Zostream::write(const void* s, UInt32 n)
{
  return write(reinterpret_cast<const char*>(s), n);
}

void Zostream::flushDataBuffer()
{
  UInt32 size;
  char* s = mDataBuf.getBufferAll(&size);
  if (size > 0)
  {
    // compress the buffer
    z_stream stream;
    stream.next_in = reinterpret_cast<Bytef*>(s);
    stream.avail_in = uInt(size);
    stream.next_out = reinterpret_cast<Bytef*>(mCompressBuf);
    stream.avail_out = uInt(mCompressBufSize);
    
    // should check for > 64K on a 16-bit machine, but
    // no one uses those anymore, and wouldn't work with our stuff
    // anyway.
    
    stream.zalloc = sCarbonCalloc;
    stream.zfree = sCarbonFree;
    stream.opaque = Z_NULL;
    
    
    int zerr = deflateInit(&stream, Z_DEFAULT_COMPRESSION);
    if (zerr != Z_OK)
    {
      setErrorInternal("Buffer write initialization error", &stream);
      return;
    }
    
    zerr = deflate(&stream, Z_FINISH);
    /*
      Technically, if zerr returns Z_OK, deflate is supposed to be
      called again after freeing up some output space, but we
      should have allocated more than enough space to begin
      with. So, taking a cue right from compress.c in the zlib
      source this is going to fail if this does not return
      Z_STREAM_END.
    */
    if (zerr != Z_STREAM_END)
    {
      deflateEnd(&stream);
      if (zerr == Z_OK)
        setError("Internal write error: Not enough buffer space", true);
      else
        setErrorInternal("Internal write error", &stream);
      return;
    }
    
    int numBytesW = stream.total_out;
    zerr = deflateEnd(&stream);
    if (zerr != Z_OK)
    {
      setErrorInternal("Internal state write error", &stream);
      return;
    }
    
    // must be called before writeCompressBufferToFile; otherwise, potential
    // infinite loop
    mDataBuf.reset();
    
    writeCompressBufferToFile(size, numBytesW);
  }
}

void Zostream::safeWrite(char* s, int numBytesW)
{
  if (is_open())
  {
    int result;
    do
    {
      result = ::write(mFile, static_cast<void*>(s), 
                       numBytesW);
      if (result > 0) {
        s += result;
        numBytesW -= result;
      }
      else if (result < 0) {
        // check for signal interruption
        if (errno != EINTR)
          numBytesW = 0;
      }
    } while (numBytesW > 0);
  
    if (result == -1)
      setErrorInternal("Unable to write");
  }
}

bool Zostream::flush()
{
  // have to call this first.
  flushDataBuffer();
  
  if (mFileGood)
  {

    UInt32 size;
    char* s = mFileBuf.getBufferAll(&size);
    if (size > 0)
    {
      int numBytesW = size;
      
      // The cipher will be deactivated when writing the header. In
      // which case we do not want to write the number of bytes.
      if (mFileCipher->isCipherActivated())
      {
        // Allocate a little more just in case size changes later to an
        // UInt64 and we forget to do this.
        UtFileBuf tmpBuf(8);
        // byte swap the size
        BYTESWAPONLITTLEENDIAN4(&size);
        tmpBuf.write(reinterpret_cast<char*>(&size), sizeof(UInt32));
        
        // now cipher the size
        UInt32 tmpSize;
        char* sizeBuf = tmpBuf.getBufferAll(&tmpSize);
        INFO_ASSERT(tmpSize == sizeof(UInt32), "Invalid buffer size.");
        mFileCipher->mask(sizeBuf, tmpSize);
        safeWrite(sizeBuf, tmpSize);
      }

      // Now write the file buffer
      mCipher->mask(s, numBytesW);

      safeWrite(s, numBytesW);
      
      mFileBuf.reset();
    }
  }
  return mFileGood;
}

Zostream& Zostream::operator<< (const UtString& str)
{
  write(str.c_str(), str.size());
  return *this;
}

Zostream& Zostream::operator<< (const char* str)
{
  write(str, strlen(str));
  return *this;
}

Zostream& Zostream::operator<< (const void* p)
{
  return (*this) << reinterpret_cast<UIntPtr>(p);
}

Zostream& Zostream::operator<< (const char c)
{
  write(&c, 1);
  return *this;
}

Zostream& Zostream::operator<< (const unsigned char c)
{
  return (*this) << static_cast<char>(c);
}

Zostream& Zostream::operator<< (const signed char c)
{
  return (*this) << static_cast<char>(c);
}

Zostream& Zostream::operator<< (double num)
{
  UtString buf;
  buf << num;
  return (*this) << buf;
}

Zostream& Zostream::operator<< (SInt32 num)
{
  UtString buf;
  buf << num;
  return (*this) << buf;
}

Zostream& Zostream::operator<< (UInt32 num)
{
  UtString buf;
  buf << num;
  return (*this) << buf;
}

Zostream& Zostream::operator<< (SInt16 num)
{
  UtString buf;
  buf << num;
  return (*this) << buf;
}

Zostream& Zostream::operator<< (UInt16 num)
{
  UtString buf;
  buf << num;
  return (*this) << buf;
}

Zostream& Zostream::operator<< (UInt64 num)
{
  UtString buf;
  buf << num;
  return (*this) << buf;
}

Zostream& Zostream::operator<< (SInt64 num)
{
  UtString buf;
  buf << num;
  return (*this) << buf;
}

Zostream& Zostream::operator<< (const UtStringArray &strings)
{
  *this << strings.size ();
  for (UtStringArray::const_iterator it = strings.begin (); it != strings.end (); ++it) {
    *this << *it;
  }
  return *this;
}

// ZoStringStream implementation
ZoStringStream::ZoStringStream(UtString* f, void* key) :
  Zostream("", key), mStringDescriptor(f)
{}

ZoStringStream::~ZoStringStream()
{}

void ZoStringStream::openForWrite()
{
  mFileGood = true;
}

void ZoStringStream::safeWrite(char* s, int numBytesW)
{
  mStringDescriptor->append(s, numBytesW);
}

// Zistream implementation
Zistream::Zistream(const char* fname, void* key) :
  Zstream(fname, key)
{
  mOwnFile = true;
}

Zistream::Zistream(UtIStream* istream, void* key) :
  Zstream("", key)
{
  mOwnFile = false;
  mFile = istream;
}

Zistream::Zistream(const char* name, SInt64 hardStop, Zistream* srcStream)
  : Zstream(name, ZSTREAM_KEY)
{
  mFile = srcStream->mFile;
  mOwnFile = false;
  ZSTREAM_INIT();
  ZISTREAM_INIT_VARS(); // init()
  mHardStop = hardStop;
  ZISTREAM_READ_MAGIC_HEADER();
}

Zistream::~Zistream()
{
  if (mOwnFile) {
    Zistream::close();
  }
}

bool Zistream::close()
{
  if (is_open() && !mFile->close()) {
    setError(mFile->getErrmsg());
  }
  if (mOwnFile) {
    delete mFile;
    mFile = NULL;
  }
  return mFileGood;
}

void Zistream::ZISTREAM_READ_MAGIC_HEADER()
{
  char magic_buf[cMagicBufSize]; 
  /* read in *JUST* the magic header. */ 
  UInt32 curFileBufSize = mFileBuf.size(); 
  mFileBuf.resize(cMagicBufSize); 
  ZISTREAM_CIPHER_ACTIVATE(mCipher, 0); 
  UInt32 numRead = rawReadFileBuf(magic_buf, cMagicBufSize); 
  if (! fail()) 
  { 
    if (numRead != cMagicBufSize) 
      mCompressed = false; 
    else 
    { 
      mCompressed = true; 
      mCompressed = (memcmp(magic_buf, cCarbonMagic, cMagicFlagSize) == 0); 
      mCompressed =  mCompressed && (memcmp(magic_buf + cMagicFlagSize, cMagicName, cMagicNameSize) == 0); 
      mCompressed = mCompressed && (memcmp(magic_buf + cMagicFlagSize + cMagicNameSize, cCarbonTail, cMagicFlagSize) == 0); 
    } 
    if (! mCompressed) 
    { 
      /* must resize the file buffer before placing the bytes back */ 
      /* into it! */ 
      mFileBuf.resize(curFileBufSize); 
      /* return the uncompressed data to the buffer (gone when we */ 
      /* resized above) */ 
      mFileBuf.write(magic_buf, numRead); 
      /* turn off decryption for good */ 
      ZISTREAM_CIPHER_ACTIVATE(mCipher, 0); 
      ZISTREAM_CIPHER_ACTIVATE(mFileCipher, 0); 
    } 
    else 
    { 
      /* check the Zstream version. */ 
      UInt32 version = 0; 
      mFileBuf.resize(sizeof(UInt32)); 
      readRawUInt32(&version); 
      if (version > cFileVersion) 
      { 
        UtString errMsg; 
        errMsg << "Incompatible low-level file version: " 
               << version << " > " << cFileVersion; 
        setError(errMsg.c_str(), true); 
      } 
      else {
        /* Setup ciphers */ 
        ZISTREAM_CIPHER_VERSION_SETUP(mCipher, version); 
        ZISTREAM_CIPHER_VERSION_SETUP(mFileCipher, version); 
        /* read reserved word */ 
        readRawUInt32(&version); 
      } 
        mFileBuf.resize(curFileBufSize); 
        ZISTREAM_CIPHER_ACTIVATE(mCipher, 494); 
        ZISTREAM_CIPHER_ACTIVATE(mFileCipher, 432); 
    } /* else if not compressed */ 
  } /* if ! fail */
}

void Zistream::ZISTREAM_CIPHER_ACTIVATE(void* cipherOpaque, UInt32 activate)
{
  CIPHER* cipher = (CIPHER*) cipherOpaque;
  if (activate != 0)
    cipher->putActivate(true);
  else
    cipher->putActivate(false);
}

void Zistream::ZISTREAM_CIPHER_VERSION_SETUP(void* cipherOpaque, UInt32 version)
{
  CIPHER* cipher = (CIPHER*) cipherOpaque;
  if (version == 1)
    cipher->putSequenceBegin(6);
  else
    cipher->putSequenceBegin(0);
}

void Zistream::ZISTREAM_INIT_VARS()
{
  mEof = false;
  mFileEof = false;
  mCompressed = true;
  mReadPos = 0;
  mCipher->setReadMode();
  mFileCipher->setReadMode();
  mHardStop = SInt64(-1);
  mBufferStop = SInt64(-1);
}

void Zistream::openForRead()
{
  if (! mFilename.empty()) {
    mFile = new UtIBStream(mFilename.c_str());
    if (! mFile->is_open()) {
      setError(mFile->getErrmsg());
    }
  }
}

void Zstream::seek(SInt64 pos) {
  if (tellfd() != pos)
  {
    mDataBuf.reset();
    mFileBuf.reset();
    seekHelper(pos);
    mFileCipher->reset();
    mCipher->reset();
  }
}

void Zistream::seekHelper(SInt64 pos) {
  mFileEof = false;
  mEof = false;
  if (mFile->seek((off_t) pos, UtIO::seek_set) < 0) {
    setError(mFile->getErrmsg());
  }
}

bool Zistream::eof() const
{
  return mEof;
}

bool Zistream::fileEof() const
{
  return mFileEof;
}

UInt32 Zistream::readline(UtString* buffer)
{
  buffer->clear();
  char buf = '\0';
  UInt32 totRead = 0;
  while ((buf != Zendl) &&
         (Zistream::read(&buf, 1) > 0))
  {
    ++totRead;
    buffer->append(&buf, 1);
  }
  return totRead;
}

void Zistream::putHardStop(SInt64 fdPos)
{
  mHardStop = fdPos;
}

void Zistream::putBufferStop(SInt64 totalBytes)
{
  mBufferStop = totalBytes;
}

bool Zistream::isCompressed() const
{
  return mCompressed;
}

int Zistream::safeRead(char* fileBuf, UInt32 size, int* numRead)
{
  *numRead = mFile->read(fileBuf, size);
  if (*numRead == 0) {
    mFileEof = true;
    if (! *mFile) {
      setErrorInternal(mFile->getErrmsg());
    }
  }
  return *numRead;
}

void Zistream::fillFileBuf()
{
  if (mFileGood)
  {
    UInt32 size;
    mFileBuf.reset();
    char* fileBuf = mFileBuf.getBufferAll(&size);
    // keep the initial position of fileBuf for masking
    char* origFileBuf = fileBuf;

    // Throw away bogus size and get the real size
    size = mFileBuf.size();

    if (mHardStop != SInt64(-1))
    {
      // we have to see how much space we still have before the
      // hardstop
      SInt64 curPos = tellfd();
      INFO_ASSERT(curPos <= mHardStop, "Buffer overrun.");
      SInt64 spaceLeft = mHardStop - curPos;
      if (spaceLeft < SInt64(size))
        size = UInt32(spaceLeft);
    }

    // If there was a hardstop this definitely could be true
    if (size == 0)
      mFileEof = true;
    
    int numRead;
    int result = 0;
    
    // if the file is not compressed, there is no chunksize!
    if (!mFileEof && mCompressed && mFileCipher->isCipherActivated())
    {
      UInt32 fileChunkSize = size;
      // First we need to find out how much to read    
      result = safeRead(reinterpret_cast<char*>(&fileChunkSize), sizeof(UInt32), &numRead);
      if (result > 0)
      {
        INFO_ASSERT(numRead == sizeof(UInt32), "Consistency check failed.");
        // first cipher it, then swap.
        mFileCipher->mask(reinterpret_cast<char*>(&fileChunkSize), sizeof(UInt32));
        BYTESWAPONLITTLEENDIAN4(&fileChunkSize);
        
        if (fileChunkSize > BUF_UPPER_BOUND)
          setError("Corrupted file input. Invalid file chunk size", true);
        else
        {
          // is the file buffer big enough?
          if (mFileBuf.size() < fileChunkSize)
          {
            resizeFileBuffer(fileChunkSize);
            fileBuf = mFileBuf.getBufferAll(&size);
            origFileBuf = fileBuf;
          }
          
          size = fileChunkSize;
          if (mHardStop != -1)
          {
            // we have to see how much space we still have before the
            // hardstop
            SInt64 curPos = tellfd();
            SInt64 spaceLeft = mHardStop - curPos;
            // user should have marked a record end here
            INFO_ASSERT(spaceLeft >= fileChunkSize, "Corrupted chunk size.");
          }
        }
      }
    }
    
    if (mFileGood && (size > 0))
    {
      result = safeRead(fileBuf, size, &numRead);
      if (result > 0)
      {
        mCipher->mask(origFileBuf, numRead);
        mFileBuf.putWriteIndex(static_cast<UInt32>(numRead));
      }
    }
  } // if mFileGood
}

UInt32 Zistream::rawReadFileBuf(char* buf, UInt32 n)
{
  UInt32 numRead = 0;
  if (! mEof && ! fail())
  {
    numRead = mFileBuf.readBuffer(buf, n);
    if ((numRead < n) && ! mFileEof)
    {
      fillFileBuf();
     
      if (! mFileEof)
        numRead += rawReadFileBuf(buf + numRead, n - numRead);
    }
  }
  return numRead;
}

void Zistream::readRawUInt32(UInt32* n)
{
  rawReadFileBuf(reinterpret_cast<char*>(n), sizeof(UInt32));
  BYTESWAPONLITTLEENDIAN4(n);
}

UtFileBuf* Zistream::getFileBuf()
{
  INFO_ASSERT(!mCompressed, "Invalid call.");
  return &mFileBuf;
}

UtFileBuf* Zistream::getDataBuf()
{
  return &mDataBuf;
}

void Zistream::fillReadBuf()
{
  if (fail())
    return;
  
  int numBytesR = 0;
  UInt32 bufCapacity;
  mDataBuf.reset();
  char* dataBuf = mDataBuf.getBufferAll(&bufCapacity);
  // Throw away bogus size and get the real size
  bufCapacity = mDataBuf.size();
  
  if (mCompressed)
  {
    UInt32 nextChunkSize = 0;
    UInt32 nextUncompressSize = 0;
    readRawUInt32(&nextUncompressSize);
    readRawUInt32(&nextChunkSize);
    
    if ((nextChunkSize != 0) && (nextUncompressSize != 0))
    {
      // It is possible that the compressed data is actually larger than
      // the uncompressed data, so we need to resize to the larger of
      // the two.
      UInt32 needBufSize = std::max(nextChunkSize, nextUncompressSize);
      if (mDataBuf.size() < needBufSize)
      {
        // tricky. The uncompressed buf size cannot be greater than the
        // upper bound, but the compressed buf size can be but not by
        // more than twice the uncompressed size
        if ((nextUncompressSize > BUF_UPPER_BOUND) || 
            (nextChunkSize > BUF_UPPER_BOUND * 2))
          setError("Corrupted file input. Invalid chunk size", true);
        else
        {
          // need to make mDataBuf and mCompressBuf bigger.
          resizeDataBuffer(needBufSize);
          dataBuf = mDataBuf.getBufferAll(&bufCapacity);
          bufCapacity = mDataBuf.size();
        }
      }

      if (! fail())
      {
        if (rawReadFileBuf(mCompressBuf, nextChunkSize) != nextChunkSize)
          setError("Unexpected end of input", true);
      }

      if (fail())
        return;
    
      // now inflate the buffer
      z_stream stream;
      stream.next_in = reinterpret_cast<Bytef*>(mCompressBuf);
      stream.avail_in = uInt(nextChunkSize);
      stream.next_out = reinterpret_cast<Bytef*>(dataBuf);
      stream.avail_out = uInt(bufCapacity);
    
      // should check for > 64K on a 16-bit machine, but
      // no one uses those anymore, and wouldn't work with our stuff
      // anyway.
    
      stream.zalloc = sCarbonCalloc;
      stream.zfree = sCarbonFree;
      stream.opaque = Z_NULL;
    
      int zerr = inflateInit(&stream);
      if (zerr != Z_OK)
      {
        setErrorInternal("Buffer read initialization error", &stream);
        return;
      }
    
      zerr = inflate(&stream, Z_FINISH);
      /*
        Technically, if zerr returns Z_OK, inflate is supposed to be
        called again after freeing up some output space, but we
        should have allocated more than enough space to begin
        with. So, taking a cue right from compress.c in the zlib
        source this is going to fail if this does not return
        Z_STREAM_END.
      */
      if (zerr != Z_STREAM_END)
      {
        inflateEnd(&stream);
        if ((zerr == Z_OK) || (zerr == Z_BUF_ERROR))
          setError("Internal read error: Not enough buffer space", true);
        else
          setErrorInternal("Internal read error", &stream);
        return;
      }
    
      numBytesR = stream.total_out;
      zerr = inflateEnd(&stream);
      if (zerr != Z_OK)
      {
        setErrorInternal("Internal read state error", &stream);
        return;
      }
    } // else if chunksize....
  } // if compressed
  else // if not compressed
    numBytesR = rawReadFileBuf(dataBuf, bufCapacity);

  mDataBuf.putWriteIndex(static_cast<UInt32>(numBytesR));
}

UInt32 Zistream::read(char* buf, UInt32 n)
{
  UInt32 numRead = 0;
  if (! mEof && ! fail())
  {
    if (mBufferStop != -1)
    {
      // we have to see how much space we still have before the
      // stop
      INFO_ASSERT(mReadPos <= mBufferStop, "Consistency check failed.");
      SInt64 spaceLeft = mBufferStop - mReadPos;
      if (spaceLeft == 0)
      {
        mFileEof = true;
        mEof = true;
        mDataBuf.reset();
        return 0;
      }
      else if (spaceLeft < SInt64(n))
        n = UInt32(spaceLeft);
    }

    numRead = mDataBuf.readBuffer(buf, n);
    mReadPos += numRead;
    if (numRead < n)
    {
      fillReadBuf();
      
      if (mFileEof && (numRead == 0) && (mDataBuf.getWriteIndex() == 0))
        mEof = true;
      else
        numRead += Zistream::read(buf + numRead, n - numRead);
    }
  }
  else {
    mFileGood = false;
  }
  return numRead;
}

void Zistream::skipSpaces(char* byte)
{
  // skip initial spaces
  while((Zistream::read(byte, 1) > 0) &&
        isspace(byte[0]))
    ;
}

void Zistream::skipSpacesPeek()
{
  bool space = true;
  while (! fail() && !mFileEof && space)
  {
    if (mDataBuf.isEndBufRead())
      fillReadBuf();
    else if ((space = isspace(mDataBuf.readPeek())))
    {
      char buf[1];
      Zistream::read(buf, 1);
    }
  }
}

char Zistream::peek()
{
  char ret = '\0'; // return something
  if (! fail() && !mFileEof)
  {
    if (mDataBuf.isEndBufRead())
      fillReadBuf();
    else 
      ret = mDataBuf.readPeek();
  }
  return ret;
}

UInt32 Zistream::skip(UInt32 numChars)
{
  UInt32 numRead = 0;
  char buf[256];
  UInt32 minRead = std::min(static_cast<UInt32>(256), numChars);
  while (!eof() && (numRead < numChars))
    numRead += Zistream::read(buf, minRead);

  return numRead;
}

Zistream& Zistream::operator>> (UtString& str)
{
  str.clear();
  char buf[1];
  skipSpaces(buf);
  
  if (! fail())
  {
    str.append(buf, 1);
    
    // append until we find a space
    while((Zistream::read(buf, 1) > 0) &&
          ! isspace(buf[0]))
      str.append(buf, 1);
  }
  return *this;
}

Zistream& Zistream::operator>> (char& c)
{
  char buf[1];
  skipSpaces(buf);
  
  if (! fail())
    c = buf[0];
  return *this;
}

Zistream& Zistream::operator>> (unsigned char& c)
{
  return operator>>((char&) c);
}

Zistream& Zistream::operator>> (signed char& c)
{
  return operator>>((char&) c);
}


//! UtStringArray
Zistream& Zistream::operator>> (UtStringArray &val)
{
  UInt32 size;
  *this >> size;
  for (UInt32 n = 0; n < size; n++) {
    UtString str;
    *this >> str;
    val.push_back (str);
  }
  return *this;
}

// virtual ones

Zistream& Zistream::operator>> (double& num)
{
  UtString buffer;
  fillForNumConversion(&buffer, true);
  if (! (buffer >> num))
    mFileGood = false;

  return *this;
}

Zistream& Zistream::operator>> (SInt32& num)
{
  UtString buffer;
  fillForNumConversion(&buffer);
  if (! (buffer >> num))
    mFileGood = false;

  return *this;
}

Zistream& Zistream::operator>> (UInt32& num)
{
  UtString buffer;
  fillForNumConversion(&buffer);
  if (! (buffer >> num))
    mFileGood = false;

  return *this;
}

Zistream& Zistream::operator>> (SInt16& num)
{
  UtString buffer;
  fillForNumConversion(&buffer);
  if (! (buffer >> num))
    mFileGood = false;

  return *this;
}

Zistream& Zistream::operator>> (UInt16& num)
{
  UtString buffer;
  fillForNumConversion(&buffer);
  if (! (buffer >> num))
    mFileGood = false;

  return *this;
}

Zistream& Zistream::operator>> (UInt64& num)
{
  UtString buffer;
  fillForNumConversion(&buffer);
  if (! (buffer >> num))
    mFileGood = false;

  return *this;
}

Zistream& Zistream::operator>> (SInt64& num)
{
  UtString buffer;
  fillForNumConversion(&buffer);
  if (! (buffer >> num))
    mFileGood = false;

  return *this;
}

void Zistream::fillForNumConversion(UtString* buffer, bool nonInteger)
{
  skipSpacesPeek();

  char buf[1];
  bool foundDot = false;
  // need to peek ahead for sign
  char sign = peek();
  bool readFirst = false;
  if (sign == '-')
  {
    readFirst = true;
    *buffer << sign;
  }
  else if (sign == '+')
    readFirst = true;
  
  if (readFirst)
    Zistream::read(buf, 1);
  
  while ((Zistream::read(buf, 1) > 0) &&
         (isdigit(int(buf[0])) || (nonInteger && !foundDot && 
                              (foundDot = (buf[0] == '.')))))
    *buffer << buf[0];
}

// ZostreamDB implementation
ZostreamDB::ZostreamDB(const char* name, void* key) :
  Zostream(name, key)
{
  initDB();
}

ZostreamDB::ZostreamDB(const char* name, Zostream* srcStream) :
  Zostream(name, srcStream)
{
  initDB();
}

void ZostreamDB::initDB()
{
  mapPtr(NULL);
}  

ZostreamDB::~ZostreamDB()
{
}

Zostream& ZostreamDB::operator<< (const char c) {
  write(&c, 1);
  return *this;
}

Zostream& ZostreamDB::operator<< (const signed char c) {
  write(&c, 1);
  return *this;
}

Zostream& ZostreamDB::operator<< (const unsigned char c) {
  write(&c, 1);
  return *this;
}

Zostream& ZostreamDB::operator<< (double num)
{
  BYTESWAPONLITTLEENDIAN8(&num);
  write(reinterpret_cast<char*>(&num), sizeof(double));
  return *this;
}

Zostream& ZostreamDB::operator<< (UInt32 num)
{
  BYTESWAPONLITTLEENDIAN4(&num);
  write(reinterpret_cast<char*>(&num), sizeof(UInt32));
  return *this;
}

Zostream& ZostreamDB::operator<< (SInt32 num)
{
  BYTESWAPONLITTLEENDIAN4(&num);
  write(reinterpret_cast<char*>(&num), sizeof(SInt32));
  return *this;
}

Zostream& ZostreamDB::operator<< (UInt16 num)
{
  BYTESWAPONLITTLEENDIAN2(&num);
  write(reinterpret_cast<char*>(&num), sizeof(UInt16));
  return *this;
}

Zostream& ZostreamDB::operator<< (SInt16 num)
{
  BYTESWAPONLITTLEENDIAN2(&num);
  write(reinterpret_cast<char*>(&num), sizeof(SInt16));
  return *this;
}

Zostream& ZostreamDB::operator<< (UInt64 num)
{
  BYTESWAPONLITTLEENDIAN8(&num);
  write(reinterpret_cast<char*>(&num), sizeof(UInt64));
  return *this;
}

Zostream& ZostreamDB::operator<< (SInt64 num)
{
  BYTESWAPONLITTLEENDIAN8(&num);
  write(reinterpret_cast<char*>(&num), sizeof(SInt64));
  return *this;
}

Zostream& ZostreamDB::operator<< (const char* val)
{
  SInt32 len = strlen(val);
  (*this) << len;
  write(val, len);
  return *this;
}

Zostream& ZostreamDB::operator<< (const UtString& val)
{
  SInt32 len = val.size();
  (*this) << len;
  write(val.c_str(), len);
  return *this;
}

Zostream& ZostreamDB::operator<< (const UtStringArray &strings)
{
  *this << strings.size ();
  for (UtStringArray::const_iterator it = strings.begin (); it != strings.end (); ++it) {
    *this << *it;
  }
  return *this;
}

void ZostreamDB::mapPtr(const void* ptr) {
  UInt32 index = mPtrToIndex.size();
  mPtrToIndex[ptr] = index;
}

UInt32 ZostreamDB::getMapSize() const {
  return mPtrToIndex.size();
}

bool ZostreamDB::isMapped(const void* ptr) const
{
  return (mPtrToIndex.find(ptr) != mPtrToIndex.end());
}

ZistreamDB::ZistreamDB(const char* filename, void* key) :
  Zistream(filename, key)
{
  ZISTREAMDB_INIT(); // initDB()
}

ZistreamDB::ZistreamDB(size_t len, const char* buf, void* key) :
  Zistream(new UtIStringStream(buf, len), key)
{
  ZISTREAMDB_INIT(); // initDB()
  mOwnFile = true;
  mFileGood = true;
}

ZistreamDB::ZistreamDB(const char* name, SInt64 hardStop, Zistream* srcStream)
  : Zistream(name, hardStop, srcStream)
{
  ZISTREAMDB_INIT(); // initDB()
}

// ZistreamDB implementation
void ZistreamDB::ZISTREAMDB_INIT()
{
  mIndexToPtr = new VoidVec;
  mapPtr(NULL);                    // NULL maps to index 0
}

ZistreamDB::~ZistreamDB() {
  delete mIndexToPtr;
}


Zistream& ZistreamDB::operator>> (signed char& c)
{
  Zistream::read((char *)&c, sizeof(char));
  return *this;
}


Zistream& ZistreamDB::operator>> (unsigned char& c)
{
  Zistream::read((char*)&c, sizeof(char));
  return *this;
}

Zistream& ZistreamDB::operator>> (char& c)
{
  Zistream::read(&c, sizeof(char));
  return *this;
}

Zistream& ZistreamDB::operator>> (double& num)
{
  Zistream::read(reinterpret_cast<char*>(&num), sizeof(double));
  BYTESWAPONLITTLEENDIAN8(&num);
  return *this;
}

Zistream& ZistreamDB::operator>> (SInt32& num)
{
  Zistream::read(reinterpret_cast<char*>(&num), sizeof(SInt32));
  BYTESWAPONLITTLEENDIAN4(&num);
  return *this;
}

Zistream& ZistreamDB::operator>> (UInt32& num)
{
  Zistream::read(reinterpret_cast<char*>(&num), sizeof(UInt32));
  BYTESWAPONLITTLEENDIAN4(&num);
  return *this;
}

Zistream& ZistreamDB::operator>> (SInt16& num)
{
  Zistream::read(reinterpret_cast<char*>(&num), sizeof(SInt16));
  BYTESWAPONLITTLEENDIAN2(&num);
  return *this;
}

Zistream& ZistreamDB::operator>> (UInt16& num)
{
  //  skipSpacesPeek();
  Zistream::read(reinterpret_cast<char*>(&num), sizeof(UInt16));
  BYTESWAPONLITTLEENDIAN2(&num);
  return *this;
}

Zistream& ZistreamDB::operator>> (UInt64& num)
{
  Zistream::read(reinterpret_cast<char*>(&num), sizeof(UInt64));
  BYTESWAPONLITTLEENDIAN8(&num);
  return *this;
}

Zistream& ZistreamDB::operator>> (SInt64& num)
{
  Zistream::read(reinterpret_cast<char*>(&num), sizeof(SInt64));
  BYTESWAPONLITTLEENDIAN8(&num);
  return *this;
}
Zistream& ZistreamDB::operator>> (UtString& val)
{
  SInt32 len;
  if ((*this) >> len)
  {
    size_t oldsize = val.size();
    val.insert(oldsize, len, '\0'); // make UtString be the right size
    Zistream::read(&val[oldsize], len);
  }
  return *this;
}

//! UtStringArray
Zistream& ZistreamDB::operator>> (UtStringArray &val)
{
  UInt32 size;
  *this >> size;
  for (UInt32 n = 0; n < size; n++) {
    UtString str;
    *this >> str;
    val.push_back (str);
  }
  return *this;
}

bool ZistreamDB::expect(const char* token)
{
  size_t len = strlen(token);
  UInt32 actual;
  if (!(*this >> actual) || (actual != len))
    return false;

  // We have verified that we are going to get a UtString of the expected
  // length.  We expect these tokens are small, so we can allocate a
  // fixed size buffer
  char buf[100];
  if ((read(buf, len) != len) || (strncmp(buf, token, len) != 0))
    return false;
  return true;
}

void ZistreamDB::mapPtr(const void* ptr)
{
  mIndexToPtr->push_back(const_cast<void*>(ptr));
}

UInt32 ZistreamDB::getMapSize() const {
  return mIndexToPtr->size();
}

bool ZistreamDB::readRawPointer(void** ptr)
{
  UInt32 index;
  if (*this >> index)
  {
    if (index >= mIndexToPtr->size()) {
      setError("Object pointer consistency check failed.", true);
      return false;
    }
    *ptr = (*mIndexToPtr)[index];
    return true;
  }
  return false;
}
