// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/DynBitVecDesc.h"
#include "util/DynBitVector.h"
#include "util/UtIOStream.h"

void DynBitVecDesc::clear() {
  mRep.startAndSize = DynBitVecDesc_SMALL_CONTIGUOUS;
}

DynBitVecDesc::DynBitVecDesc(SInt32 bit, DynBitVectorFactory* factory) {
  populateRange(bit, 1, factory);
}

DynBitVecDesc::DynBitVecDesc(SInt32 start, SInt32 size,
                             DynBitVectorFactory* factory)
{
  populateRange(start, size, factory);
}

DynBitVecDesc::DynBitVecDesc(const DynBitVector& bitvec,
                             DynBitVectorFactory* factory)
{
  UInt32 start, size;
  if (bitvec.getContiguousRange(&start, &size))
    populateRange(start, size, factory);
  else
    mRep.bitvec = factory->alloc(bitvec);
}

void DynBitVecDesc::populateRange(SInt32 start, SInt32 size,
                                  DynBitVectorFactory* factory)
{
  if ((size == 1) && (start < (1 << 30))) {
    INFO_ASSERT(start < (1 << 30), "Consistency check failed.");
    mRep.startAndSize = (start << 2) | DynBitVecDesc_SINGLE_BIT;
  }
  else if ((start < (1 << 15)) && (size < (1 << 15))) {
    mRep.startAndSize = (start << 2) | (size << 17)
      | DynBitVecDesc_SMALL_CONTIGUOUS;
  }
  else if (start == 0) {
    mRep.startAndSize = (size << 2) | DynBitVecDesc_ALL_ONES;
  }
  else {
    // bug 4280 - we need to represent the low unset bits in the
    // bitvector, so add start to size.
    DynBitVector bitvec(start + size);
    bitvec.setRange(start, size, 1);
    mRep.bitvec = factory->alloc(bitvec);
  }
}

void DynBitVecDesc::getBitVec(DynBitVector* bv, UInt32 numBits) const {
  if (isBitVec()) {
    *bv = *mRep.bitvec;
    bv->resize(numBits);
  }
  else {
    bv->resize(numBits);
    bv->reset();
    if (isSmallRange()) {
      bv->setRange(getRangeStart(), getRangeSize(), 1);
    }
    else if (isAllOnes()) {
      size_t size = getAllOnesSize();
      bv->setRange(0, size, 1);
    }
    else {
      size_t bit = getBit();
      bv->set(bit);
    }
  }
} // void DynBitVecDesc::getBitVec

bool DynBitVecDesc::all(UInt32 size) const {
  if (isSmallRange()) {
    return (size == getRangeSize()) && (getRangeStart() == 0);
  }
  if (isSingleBit()) {
    return (size == 1) && (getBit() == 0);
  }
  if (isAllOnes()) {
    return size == getAllOnesSize();
  }
  return (size == mRep.bitvec->size()) && mRep.bitvec->all();
}

UInt32 DynBitVecDesc::count() const {
  if (isSmallRange()) {
    return getRangeSize();
  }
  if (isSingleBit()) {
    return 1;
  }
  if (isAllOnes()) {
    return getAllOnesSize();
  }
  return mRep.bitvec->count();
}

bool DynBitVecDesc::getContiguousRange(UInt32* startBit, UInt32* size)
  const
{
  if (isSmallRange()) {
    *startBit = getRangeStart();
    *size = getRangeSize();
    return true;
  }
  else if (isSingleBit()) {
    *startBit = getBit();
    *size = 1;
    return true;
  }
  else if (isAllOnes()) {
    *startBit = 0;
    *size = getAllOnesSize();
    return true;
  }
  return mRep.bitvec->getContiguousRange(startBit, size);
}

static inline int sCmpBV(const DynBitVecDesc& dbv1, UInt32 sz1,
                         const DynBitVecDesc& dbv2, UInt32 sz2)
{
  DynBitVector bv1, bv2;
  dbv1.getBitVec(&bv1, sz1);
  dbv2.getBitVec(&bv2, sz2);
  return bv1.compare(bv2);
}

// Turn this #define on if you want to double-check that the
// DynBitVecDesc compare routine is returning the same result
// as the corresponding DynBitVector.
#define DOUBLE_CHECK_CMP 0
#if DOUBLE_CHECK_CMP
static int sSign(int x) {
  if (x < 0)
    x = -1;
  else if (x > 0)
    x = 1;
  return x;
}
#endif

int DynBitVecDesc::compare(const DynBitVecDesc& dbv1, UInt32 sz1,
                           const DynBitVecDesc& dbv2, UInt32 sz2)
{
  // The simplest possible code is as follows
  //    DynBitVector bv1, bv2;
  //    dbv1.getBitVec(&bv1, sz1);
  //    dbv2.getBitVec(&bv2, sz2);
  //    return bv1.compare(bv2);
  // this is slow because most DynBitVecDesc will not need DynBitVector
  // representation -- they will be either single-bit or continguous range.
  // so expanding out the DynBitVector on comparison is going to be slow.
  // We should only do that if one or the other side requires it.  Otherwise
  // we should special case

  int cmp = 0;
  if (dbv1.isBitVec() || dbv2.isBitVec()) {
    cmp = sCmpBV(dbv1, sz1, dbv2, sz2);
  }

  else {
    UInt32 start1, len1, start2, len2;
    dbv1.getContiguousRange(&start1, &len1);
    dbv2.getContiguousRange(&start2, &len2);
    UInt32 msb1 = start1 + len1; // - 1, but it doesn't matter..
    UInt32 msb2 = start2 + len2; // - 1, cause it cancels out with this
    cmp = msb1 - msb2;
    if (cmp == 0) {
      // The subtraction is actually correct.  That's
      // what those DOUBLE_CHECK_CMPs prove.  Consider these
      // two cases, both of which dbv1 is greater
      //
      // 1. dbv1   4'b1100       msb1=4 start1=2 
      //    dbv2   4'b0110       msb2=3 start2=1
      //                         cmp = msb1 - msb2 = 4 - 3 = 1
      // 2. dbv1   4'b1110       msb1=4 start1=1
      //    dbv2   4'b1100       msb2=4 start2=2
      //                         cmp = msb1 - msb2 = 4 - 4 = 0
      //                         cmp = start2 - start1 = 2 - 1 = 1
      cmp = start2 - start1;
    }
#if DOUBLE_CHECK_CMP
    INFO_ASSERT(sSign(cmp) == sSign(sCmpBV(dbv1, sz1, dbv2, sz2)), "Sign consistency check failed.");
#endif
  }
  return cmp;
} // int DynBitVecDesc::compare

bool DynBitVecDesc::anyCommonBitsSet(const DynBitVecDesc& other, UInt32 size)
  const
{
  bool overlap = false;
  if (isBitVec() && other.isBitVec()) {
    // both are bitvectors.
    DynBitVector bv1, bv2;
    getBitVec(&bv1, size);
    other.getBitVec(&bv2, size);
    overlap = bv1.anyCommonBitsSet(bv2);
  } else if (isBitVec()) {
    // this is bitvector.
    DynBitVector bv1;
    getBitVec(&bv1, size);

    UInt32 start2, len2;
    other.getContiguousRange(&start2, &len2);
    UInt32 end2 = start2 + len2 - 1;

    for (UInt32 i = start2; (i <= end2) && (!overlap); ++i) {
      overlap = bv1.test(i);
    }
  } else if (other.isBitVec()) {
    // other is bitvector.
    UInt32 start1, len1;
    getContiguousRange(&start1, &len1);
    UInt32 end1 = start1 + len1 - 1;

    DynBitVector bv2;
    other.getBitVec(&bv2, size);

    for (UInt32 i = start1; (i <= end1) && (!overlap); ++i) {
      overlap = bv2.test(i);
    }
  } else {
    // neither is bitvector.
    UInt32 start1, len1, start2, len2;
    getContiguousRange(&start1, &len1);
    other.getContiguousRange(&start2, &len2);
    UInt32 end1 = start1 + len1 - 1;
    UInt32 end2 = start2 + len2 - 1;

    // It is easier to enumerate the two ways in which the bitvectors
    // will *not* overlap.

    bool notOverlap = (start1 > end2) || (start2 > end1);
    overlap = ! notOverlap;
  }
  return overlap;
} // bool DynBitVectorFactory::anyCommonBitsSet


//! Test the first bit vector covers the other
static bool sTestCover(const DynBitVector &vect1,
		       const DynBitVector &vect2,
		       UInt32 size)
{
  DynBitVector v(size, vect1);
  v &= vect2;
  return (v == vect2);
}

bool DynBitVecDesc::covers(const DynBitVecDesc & other, UInt32 size) const
{
  if (isBitVec() && other.isBitVec()) {
    DynBitVector bv1, bv2;
    getBitVec(&bv1, size);
    other.getBitVec(&bv2, size);
    return sTestCover(bv1, bv2, size);
  } else if (isBitVec()) {
    DynBitVector bv1;
    getBitVec(&bv1, size);
    UInt32 lsb2, len2;
    other.getContiguousRange(&lsb2, &len2);
    UInt32 msb2 = lsb2 + len2 - 1;

    // Test for coverage by checking that we contain all bits in
    // 'other'.
    for (UInt32 i = lsb2; i <= msb2; ++i) {
      if (not bv1.test(i)) {
        return false;
      }
    }
    return true;
  } else if (other.isBitVec()) {
    UInt32 lsb1, len1;
    getContiguousRange(&lsb1, &len1);
    DynBitVector bv2;
    other.getBitVec(&bv2, size);

    // If the other is a bit vector, clear all of our bits; we have
    // coverage if the vector is now empty.
    bv2.setRange(lsb1, len1, 0);
    return not bv2.any();
  } else {
    UInt32 lsb1, len1, lsb2, len2;
    getContiguousRange(&lsb1, &len1);
    other.getContiguousRange(&lsb2, &len2);
    UInt32 msb1 = lsb1 + len1 - 1;
    UInt32 msb2 = lsb2 + len2 - 1;
    // this vector covers the other if our contiguous range completely
    // contains the other one.
    return ( (lsb1 <= lsb2) && (msb1 >= msb2) );
  }
}

bool DynBitVecDesc::test(UInt32 offset) const
{
  if (isBitVec()) {
    return (mRep.bitvec->size() > offset) && mRep.bitvec->test(offset);
  }

  UInt32 lsb1, len1;
  getContiguousRange(&lsb1, &len1);
  UInt32 msb1 = lsb1 + len1 - 1;
  return ( (lsb1 <= offset) && (msb1 >= offset) );
}


void DynBitVecDesc::merge(const DynBitVecDesc & one, 
                          const DynBitVecDesc & two,
                          DynBitVector * result,
                          UInt32 size)
{
  result->resize(size);
  if (one.isBitVec() && two.isBitVec()) {
    DynBitVector vect1, vect2;
    one.getBitVec(&vect1, size);
    two.getBitVec(&vect2, size);

    (*result) = vect1 | vect2;
  } else if (one.isBitVec()) {
    one.getBitVec(result, size);
    UInt32 lsb, len;
    two.getContiguousRange(&lsb, &len);
    result->setRange(lsb,len,1);
  } else if (two.isBitVec()) {
    two.getBitVec(result, size);
    UInt32 lsb, len;
    one.getContiguousRange(&lsb, &len);
    result->setRange(lsb,len,1);
  } else {
    UInt32 lsb, len;
    one.getContiguousRange(&lsb, &len);
    result->setRange(lsb,len,1);
    two.getContiguousRange(&lsb, &len);
    result->setRange(lsb,len,1);
  }
}


void DynBitVecDesc::intersect(const DynBitVecDesc & one, 
                              const DynBitVecDesc & two,
                              DynBitVector * result,
                              UInt32 size)
{
  DynBitVector vect2;
  one.getBitVec(result, size);
  two.getBitVec(&vect2, size);

  (*result) &= vect2;
}


void DynBitVecDesc::subtract(const DynBitVecDesc & one, 
                             const DynBitVecDesc & two,
                             DynBitVector * result,
                             UInt32 size)
{
  result->resize(size);

  if (one.isBitVec() and not two.isBitVec()) {
    one.getBitVec(result, size);
    UInt32 lsb, len;
    two.getContiguousRange(&lsb, &len);
    result->setRange(lsb,len,0);
  } else if (two.isBitVec()) {
    DynBitVector vect2;
    one.getBitVec(result, size);
    two.getBitVec(&vect2, size);

    vect2.flip();
    (*result) &= vect2;
  } else {
    UInt32 lsb1, len1, lsb2, len2;
    one.getContiguousRange(&lsb1, &len1);
    two.getContiguousRange(&lsb2, &len2);
    result->setRange(lsb1,len1,1);
    result->setRange(lsb2,len2,0);
  }
}

