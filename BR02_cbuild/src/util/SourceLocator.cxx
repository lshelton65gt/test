// -*-C++-*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/SourceLocator.h"
#include "util/ShellMsgContext.h"
#include "util/UtIOStream.h"
#include "util/StringAtom.h"
#include "util/AtomicCache.h"
#include "util/UtArray.h"
#include "util/UtHashMap.h"
#include "util/Zstream.h"

// There can only be one SourceLocatorFactory, because SourceLocators depend
// on the SourceLocatorFactory to decode filenames and line numbers, and we
// don't want to pay for the pointer in the gazillion SourceLocators that
// are created.  If thread-safety is an issue when creating source locators,
// then this will have to be changed.
SourceLocatorFactory* SourceLocator::smTheFactory = NULL;

/*!
  \file
  Definition of the classes to manage source locators.
*/


//! set this to false if you want to see encrypted names/source
#ifndef HIDE_TIC_PROTECTED_REGIONS
#define HIDE_TIC_PROTECTED_REGIONS true
#endif

void SourceLocator::compose(UtString* str) const
{
  const char* file;
  UInt32 line;
  SourceLocator::smTheFactory->decode(mBits, &file, &line);

  (*str) << file;
  // the following inclusion of the line (if not zero) is a trick to
  // handle source locators for protected regions.  If the source
  // locator is from a protected region then the line number we want
  // the user to see has already been included in the file name.
  // (only when HIDE_TIC_PROTECTED_REGIONS is false will we want to show
  // the value of line, and in that case it will be non zero)
  if ( line ) {
    (*str) << ":" << line;
  }
}

bool SourceLocator::isTicProtected() const
{
  if ( HIDE_TIC_PROTECTED_REGIONS ) {
    return (SourceLocator::smTheFactory->isTicProtected(mBits));
  } else {
    return false;
  }
}

void SourceLocator::print() const
{
  UtOStream& cout = UtIO::cout();

  const char* file;
  UInt32 line;
  SourceLocator::smTheFactory->decode(mBits, &file, &line);

  cout << "{" << file;

  // source locators with a value of 0 for line usually mean that
  // the source is from a protected region, see comment in
  // SourceLocator::compose for more info
  if ( line ) {
    cout << ":" << line;
  }
  cout << "}";
  cout.flush();
}


const char* SourceLocator::getFile() const {
  const char* file;
  UInt32 line;
  SourceLocator::smTheFactory->decode(mBits, &file, &line);
  return file;
}

UInt32 SourceLocator::getLine() const {
  const char* file;
  UInt32 line;
  SourceLocator::smTheFactory->decode(mBits, &file, &line);
  return line;
}

SourceLocator SourceLocatorFactory::create(const char* file, UInt32 line)
{
  if (this != SourceLocator::smTheFactory) {
    return SourceLocator::smTheFactory->create(file, line);
  }

  StringAtom* fileSym = mFilenames->intern(file);

  // Find the index for this file
  FilenameToIndexMap::iterator nameIter = mFilenameToIndexMap->find(fileSym);
  UInt32 fileIndex = 0;
  if (nameIter == mFilenameToIndexMap->end()) {
    fileIndex = mFilenameVector->size();
    // Reached the max file
    if (fileIndex != 0xffff) {
      fileIndex = mFilenameVector->size();
      (*mFilenameToIndexMap)[fileSym] = fileIndex;
      mFilenameVector->push_back(fileSym);
    }
  }
  else {
    fileIndex = nameIter->second;
  }

  // Construct as FileBlockDesc based on the high order bits of
  // the line number.  Look that up to find the index of this
  // FileBlockDesc
  FileBlockDesc fbd = (line & 0xffff0000) | fileIndex;
  FileBlockDescToIndexMap::iterator fbdIter = mFileBlockDescToIndexMap->find(fbd);
  UInt32 fbdIndex = 0;
  if (fileIndex == 0xffff) {
    fbdIndex = 0xffff;          // overflow in filename --> overflow in fbd
  }
  else if (fbdIter == mFileBlockDescToIndexMap->end()) {
    fbdIndex = mFileBlockDescVector->size();
    // avoid overflow
    if (fbdIndex != 0xffff) {
      (*mFileBlockDescToIndexMap)[fbd] = fbdIndex;
      mFileBlockDescVector->push_back(fbd);
    }
  }
  else {
    fbdIndex = fbdIter->second;
  }

  // Now the source locator can be created by oring the low-order bits
  // of the line, with the shifted FileBlockDesc 
  return SourceLocator((fbdIndex << 16) | (line & 0xffff));
} // SourceLocator SourceLocatorFactory::create

bool SourceLocatorFactory::isTicProtected(const UInt32 bits)
{
  bool isTicProtected = false;
  const char* unused_filename;
  UInt32 unused_lineNumber;
  decodeHelper(bits, &isTicProtected, &unused_filename, &unused_lineNumber);
  return isTicProtected;
}


void SourceLocatorFactory::decodeHelper(UInt32 bits, bool* isTicProtected,
                                        const char** filename, UInt32* lineNumber)
{
  UInt32 fbdIndex = bits >> 16;
  *lineNumber = bits & 0xffff;

  if (fbdIndex == 0xffff) {
    *filename = "<FILENAME_OVERFLOW>";
    *isTicProtected = true;     // failsafe
  } else {
    INFO_ASSERT(mFileBlockDescVector->size() > fbdIndex, "Invalid locator decode index.");
    FileBlockDesc fbd = (*mFileBlockDescVector)[fbdIndex];
    *lineNumber |= (fbd & 0xffff0000); // recover high-order bits of line #

    UInt32 fileIndex = fbd & 0xffff;
    INFO_ASSERT(mFilenameVector->size() > fileIndex, "Invalid locator decode index.");
    StringAtom* fileSym = (*mFilenameVector)[fileIndex];
    *filename = fileSym->str();
    *isTicProtected = ( (**filename) == '-' );
  }
}

void SourceLocatorFactory::decode(UInt32 bits, 
                                  const char** filename, UInt32* lineNumber)
{
  bool unused = false;
  decodeHelper(bits, &unused, filename, lineNumber);

  if ( HIDE_TIC_PROTECTED_REGIONS ) {
    // sanitize the filename/line if this locator comes from an protected region
    if ( (**filename) == '-' ) {
      (*filename)++;    // skip the protected indicator
      *lineNumber = 0;  // the linenumber of protected region is included in filename
    }
  }
}

SourceLocatorFactory::SourceLocatorFactory() {
  if (SourceLocator::smTheFactory == NULL) {
    mFilenames = new AtomicCache;
    mFilenameToIndexMap = new FilenameToIndexMap;
    mFilenameVector = new FilenameVector;
    mFileBlockDescToIndexMap = new FileBlockDescToIndexMap;
    mFileBlockDescVector = new FileBlockDescVector;
    SourceLocator::smTheFactory = this;
  }
  else {
    mFilenames = NULL;
    mFilenameToIndexMap = NULL;
    mFilenameVector = NULL;
    mFileBlockDescToIndexMap = NULL;
    mFileBlockDescVector = NULL;
  }
}


SourceLocatorFactory::~SourceLocatorFactory() {
  if (mFilenameVector != NULL) {
    delete mFilenameVector;
  }
  if (mFilenameToIndexMap != NULL) {
    delete mFilenameToIndexMap;
  }
  if (mFileBlockDescVector != NULL) {
    delete mFileBlockDescToIndexMap;
  }
  if (mFileBlockDescVector != NULL) {
    delete mFileBlockDescVector;
  }
  if (mFilenames != NULL) {
    delete mFilenames;
  }
  if (SourceLocator::smTheFactory == this) {
    SourceLocator::smTheFactory = NULL;
  }
}

void MsgContext::composeLocation(const SourceLocator *loc, UtString* location)
{
  if (loc == NULL)
    *location << "(null SourceLocator)";
  else
  {
    loc->compose(location);
  }
}

SourceLocator::ID SourceLocator::NULL_ID = 0;

bool SourceLocatorFactory::writeDB (ZostreamDB &out)
{
  bool ret = true;
  // Write the filenames
  out << "filenames";
  out << mFilenameVector->size ();
  UInt32 n_filenames = 0;
  for (FilenameVector::iterator it = mFilenameVector->begin (); it != mFilenameVector->end (); it++) {
    StringAtom *filename = *it;
    FilenameToIndexMap::iterator found;
    if ((found = mFilenameToIndexMap->find (filename)) == mFilenameToIndexMap->end ()) {
      // This is weird, there is a filename in the factory with no
      // corresponding index. This really should not happen.
      return false;
    }
    // Write the filename and index
    out << found->first->str ();
    INFO_ASSERT (n_filenames++ == found->second, "Inconsistent SourceLocatorFactory");
  }
  out << "end filenames";
  // Write the file block descriptor
  out << "fileblockdescriptors";
  out << mFileBlockDescVector->size ();
  UInt32 n_descriptors = 0;
  for (FileBlockDescVector::iterator it = mFileBlockDescVector->begin (); 
       it != mFileBlockDescVector->end (); it++) {
    FileBlockDesc fbd = *it;
    out << fbd;
    FileBlockDescToIndexMap::iterator found;
    if ((found = mFileBlockDescToIndexMap->find (fbd)) == mFileBlockDescToIndexMap->end ()) {
      // This is weird, a file block descriptor without an index. This cannot happen.
      return false;
    }
    INFO_ASSERT (n_descriptors++ == found->second, "Inconsistent SourceLocatorFactory");
  }
  out << "end fileblockdescriptors";
  return ret;
}

bool SourceLocatorFactory::readDB (ZistreamDB &in)
{
  bool ret = true;
  INFO_ASSERT (this != SourceLocator::smTheFactory, "Cannot read into extant SourceLocatorFactory");
  INFO_ASSERT (mFilenames == NULL, "Reading into non-empty SourceLocatorFactory");
  INFO_ASSERT (mFilenameToIndexMap == NULL, "Reading into non-empty SourceLocatorFactory");
  INFO_ASSERT (mFilenameVector == NULL, "Reading into non-empty SourceLocatorFactory");
  INFO_ASSERT (mFileBlockDescToIndexMap == NULL, "Reading into non-empty SourceLocatorFactory");
  INFO_ASSERT (mFileBlockDescVector == NULL, "Reading into non-empty SourceLocatorFactory");
  mFilenames = new AtomicCache;
  mFilenameToIndexMap = new FilenameToIndexMap;
  mFilenameVector = new FilenameVector;
  mFileBlockDescToIndexMap = new FileBlockDescToIndexMap;
  mFileBlockDescVector = new FileBlockDescVector;
  // Read the filenames
  INFO_ASSERT (in.expect ("filenames"), "Corrupt DB");
  UInt32 expected_filename_vector_size;
  in >> expected_filename_vector_size;
  for (UInt32 n = 0; n < expected_filename_vector_size; n++) {
    UtString filename;
    in >> filename;
    StringAtom *symbol = mFilenames->intern (filename);
    mFilenameVector->push_back (symbol);
    mFilenameToIndexMap->insert (FilenameToIndexMap::value_type (symbol, n));
  }
  INFO_ASSERT (in.expect ("end filenames"), "Corrupt DB");
  // Read the file block descriptors
  INFO_ASSERT (mFileBlockDescVector->size () == 0, "Reading into non-empty SourceLocatorFactory");
  INFO_ASSERT (in.expect ("fileblockdescriptors"), "Corrupt DB");
  UInt32 expected_fbd_vector_size;
  in >> expected_fbd_vector_size;
  for (UInt32 n = 0; n < expected_fbd_vector_size;n++) {
    FileBlockDesc fbd;
    in >> fbd;
    mFileBlockDescVector->push_back (fbd);
    mFileBlockDescToIndexMap->insert (FileBlockDescToIndexMap::value_type (fbd, n));
  }
  INFO_ASSERT (in.expect ("end fileblockdescriptors"), "Corrupt DB");
  return ret;
}

/*static*/ bool SourceLocatorFactory::ignoreFactoryInDB (ZistreamDB &in)
{
  bool ret = true;
  // Read the filenames
  INFO_ASSERT (in.expect ("filenames"), "Corrupt DB");
  UInt32 expected_filename_vector_size;
  in >> expected_filename_vector_size;
  for (UInt32 n = 0; n < expected_filename_vector_size; n++) {
    UtString filename;
    in >> filename;
  }
  INFO_ASSERT (in.expect ("end filenames"), "Corrupt DB");
  // Read the file block descriptors
  INFO_ASSERT (in.expect ("fileblockdescriptors"), "Corrupt DB");
  UInt32 expected_fbd_vector_size;
  in >> expected_fbd_vector_size;
  for (UInt32 n = 0; n < expected_fbd_vector_size;n++) {
    FileBlockDesc fbd;
    in >> fbd;
  }
  INFO_ASSERT (in.expect ("end fileblockdescriptors"), "Corrupt DB");
  return ret;
}

bool SourceLocatorFactory::writeDB (ZostreamDB &out, const SourceLocator loc) const
{
  out << loc.mBits;
  return true;
}

bool SourceLocatorFactory::readDB (ZistreamDB &in, SourceLocator *loc, 
  SourceLocatorFactory *destination)
{
  UInt32 bits;
  in >> bits;
  if (this == destination) {
    // Reading into the global factory so we can just copy the bits
    loc->mBits = bits;
  } else {
    // Need to translate from the factory that was written into the database
    // file to the factory that we need the source location to belong in.
    const char *filename;
    UInt32 line;
    decode (bits, &filename, &line);
    *loc = destination->create (filename, line);
  }
  return true;
}
bool SourceLocatorFactory::ignoreEntryInDB (ZistreamDB &in)
{
  UInt32 bits;
  in >> bits;
  return true;
}
