// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/UtFileBuf.h"
#include "util/OSWrapper.h"     // for MEMCPY
#include "util/CarbonAssert.h"
#include "util/MemManager.h"
#include <algorithm>            // for std::min

UtFileBuf::UtFileBuf(UInt32 size)
{
  mSize = size;
  mCapacity = mSize;
  mBuf = CARBON_ALLOC_VEC(char, mSize);
  reset();
}

UtFileBuf::~UtFileBuf()
{
  CARBON_FREE_VEC(mBuf, char, mSize);
}


void UtFileBuf::resize(UInt32 size)
{
  UInt32 oldSize = mSize;
  mSize = size;
  if (mSize > mCapacity)
  {
    mCapacity = mSize;
    CARBON_FREE_VEC(mBuf, char, oldSize);
    mBuf = CARBON_ALLOC_VEC(char, mSize);
  }
  mBufIndexW = 0;
  mBufIndexR = 0;
}

UInt32 UtFileBuf::write(const char* s, UInt32 size)
{
  UInt32 written = std::min(mSize - mBufIndexW, size);
  if (written > 0)
  {
    MEMCPY(&mBuf[mBufIndexW], s, written);
    mBufIndexW += written;
  }
 
  return written;
}

void UtFileBuf::reset()
{
  mBufIndexW = 0;
  mBufIndexR = 0;
}

char* UtFileBuf::getBufferAll(UInt32* size)
{
  *size = mBufIndexW;
  return mBuf;
}

char* UtFileBuf::getBufferRemaining(UInt32* size)
{
  *size = mBufIndexW - mBufIndexR;
  return mBuf + mBufIndexR;
}

char* UtFileBuf::getCurrentPos()
{
  INFO_ASSERT(mBufIndexR <= mBufIndexW, "Invalid read index.");
  return &mBuf[mBufIndexR];
}

UInt32 UtFileBuf::getWriteIndex() const
{
  return mBufIndexW;
}

// Used when reading a file
void UtFileBuf::putWriteIndex(UInt32 numBytes)
{
  INFO_ASSERT(numBytes <= mSize, "Write index out-of-bounds.");
  mBufIndexW = numBytes;
}

void UtFileBuf::resetReadIndex()
{
  mBufIndexR = 0;
}

UInt32 UtFileBuf::incrReadIndex()
{
  ++mBufIndexR;
  INFO_ASSERT(mBufIndexR <= mBufIndexW, "Invalid read index increment");
  return mBufIndexR;
}

void UtFileBuf::putReadIndex(UInt32 index)
{
  INFO_ASSERT(index <= mBufIndexW, "Invalid read index.");
  mBufIndexR = index;
}

char UtFileBuf::readPeek() const
{
  char ret = '\0'; // init to *something*
  UInt32 numAbleToRead = mBufIndexW - mBufIndexR;
  if (numAbleToRead > 0)
    ret = mBuf[mBufIndexR];
  return ret;
}

UInt32 UtFileBuf::capacity() const
{
  return mCapacity;
}

UInt32 UtFileBuf::size() const
{
  return mSize;
}

void UtFileBuf::moveRemainingBytesToStart() {
  INFO_ASSERT(mBufIndexW >= mBufIndexR, "Invalid read index.");
  UInt32 remaining = mBufIndexW - mBufIndexR;
  if (remaining != 0) {
    memmove(mBuf, mBuf + mBufIndexR, remaining);
    mBufIndexR = 0;
    mBufIndexW = remaining;
  }
}

char* UtFileBuf::getWriteBuf(UInt32* numBytes) {
  *numBytes = mSize - mBufIndexW;
  INFO_ASSERT(*numBytes != 0, "Write index is out-of-bounds.");
  return mBuf + mBufIndexW;
}
