// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

// We only build a thread-safe libcarbon now
#define PTHREADS

#ifdef PTHREADS
#include "util/CarbonPthread.h"
#endif
#include "util/MemManager.h"
#include "util/UtIOStream.h"
#include "util/CarbonPlatform.h"
#include "util/Stats.h"

#include "MemSystem.h"
#include "MemHistogram.h"

#include <cstdlib>
#include <algorithm>

#if pfLINUX && !pfICC
#include "memcheck.h"
#endif

// please do not use "using namespace std"
using std::new_handler;
using std::bad_alloc;
using std::malloc;


#define CARBON_MEMORY_SYSTEM
#ifdef CARBON_MEMORY_SYSTEM
# include "MemSystem.h"
# define CARBON_MEM_SIZE_OVERHEAD 0
extern "C" void* CarbonMalloc(size_t size, int needOverhead);
extern "C" void CarbonFree(void* ptr, size_t sz);
extern "C" void* CarbonRealloc(void* ptr, size_t old_size, size_t new_size,
                               bool hasOverhead);
#else
# define CarbonMalloc malloc
# define CarbonFree free
# define CarbonGetSize sGetSize
  std::size_t sGetSize(void* ptr, void** fixedPtr, bool);
# ifdef MEM_DEBUG_ENABLE // See inc/util/MemHistogram.h
#  define CARBON_MEM_SIZE_OVERHEAD 8
# else
#  define CARBON_MEM_SIZE_OVERHEAD 0
# endif
#endif

#include "carbon/c_memmanager.h"

extern new_handler __new_handler;

#define NUM_DEBUG_POINTERS 10
static void* sDebugPointers[NUM_DEBUG_POINTERS + 1];
static int sNumDebugPointers = 0;

static bool sSuppressScribble = false;
static bool sIsInitialized = false;
bool gMemSystemUseMalloc = false;
bool gDisableCarbonMemSystem = false;
MemHistogram* gMemHistogram = NULL;

#define purify_is_running() (false)

#if !pfLINUX
#undef RUNNING_ON_VALGRIND
#define RUNNING_ON_VALGRIND (false)
#endif

#if pfICC
#undef RUNNING_ON_VALGRIND
#define RUNNING_ON_VALGRIND (false)
#endif

bool CarbonMem::isPurifyRunning() {return purify_is_running();}
bool CarbonMem::isValgrindRunning() {return RUNNING_ON_VALGRIND;}

Stats* gStats = NULL;
UInt32 gStatsOverhead = 0;

extern void MemSystemInit();
extern void MemSystemClearByteCount();

#ifdef PTHREADS
static pthread_mutex_t sLock = PTHREAD_MUTEX_INITIALIZER;
static void sInitMutex()
{
  pthread_mutex_init(&sLock, NULL);
}
#endif

static const char* sMemDumpName = NULL;
static const char* sMemDumpExe = NULL;


bool gCarbonMemSystemIsInitialized() { return sIsInitialized; }

static void sInit()
{
#ifdef PTHREADS
  // When threading is on, we can't set sIsInitialized until the end of sInit()
  static pthread_t sLockOwner = 0;


  // We can check the lock-owner prior to acquiring the lock because
  // we will not asynchronously grab memory from the same thread --
  // we will set sLockOwner before recursively asking for memory.
  // When it's a different thread, sLockOwner will transition
  // from NULL to the first-locker.  Assuming the write to sLockOwner
  // is atomic then either way pthread_equal will return false, and
  // then we fall through to the lock.
  if (pthread_equal(sLockOwner, pthread_self()))
    return;                     // recursive allocation.  do not acquire lock

  static pthread_once_t once = PTHREAD_ONCE_INIT;
  pthread_once(&once, sInitMutex);

  pthread_mutex_lock(&sLock);
  if (sIsInitialized)
  {
    // Another thread must have started sInit first, and is now finished.
    // Note below that we set sIsInitialized to true before unlocking,
    // so there is no race condition here.
    pthread_mutex_unlock(&sLock);
    return;
  }
  INFO_ASSERT(sLockOwner == 0, "PTHREAD_ERROR: Lock is still owned by another thread.");
  sLockOwner = pthread_self();

#else
  // avoids recursing into sInit when threading is off
  sIsInitialized = true;
#endif

  if (gDisableCarbonMemSystem || 
      ((getenv("CARBON_MEM_FORCEUSE") == NULL) && (RUNNING_ON_VALGRIND || purify_is_running() ||
                                                   (getenv("CARBON_MEM_DISABLE") != NULL))))
  {
    gMemSystemUseMalloc = true;
    sSuppressScribble = true;
    if (gDisableCarbonMemSystem) {
      // Do not output a status message. This is most likely because we are
      // running in the GUI.
    } else if (getenv ("CARBON_MEM_DISABLE") != NULL) {
      // Do not output a message if the memory system is explicitly disabled
      // through the environment.
    } else {
      // Automagically disabled by purify, so output a status message.
      fprintf(stderr, "Running with purify/valgrind -- turning memory scribbling off...\n");
    }
    //cout << "Press return to continue ...." << UtIO::endl;
    //string x;
    //cin >> x;
  }
  else
    MemSystemInit();

  //UInt32 beforeStats = CarbonMem::getBytesAllocated();
  bool saveUseSys = gMemSystemUseMalloc;
  gMemSystemUseMalloc = true;
  gStats = new Stats(stdout);
  gMemSystemUseMalloc = saveUseSys;

  if (!gMemSystemUseMalloc)
  {
    if (sMemDumpName == NULL)
      // memory debugging may be useful in a context where we don't
      // have access to main().  We have to initialize the memory histogram
      // structure before any other allocations take place, which is why I
      // don't use the normal argument processing mechanism or even CRYPT.
      sMemDumpName = getenv("CARBON_MEM_DUMP");
    if (sMemDumpExe == NULL)
      sMemDumpExe = getenv("CARBON_MEM_EXE");
    if (sMemDumpName != NULL)
    {
      gMemHistogram = new MemHistogram(sMemDumpName, sMemDumpExe, true);
       // to allow flush file to work in Zstream
      //gMemHistogramReserve = (char*) malloc(gMemHistogramReserveSize);
    }
  }
  MemSystemClearByteCount();

#ifdef PTHREADS
  sIsInitialized = true;
  sLockOwner = 0;
  pthread_mutex_unlock(&sLock);
#endif
} // static void sInit

CarbonMem::CarbonMem(int* argc, char** argv, const char* dumpFileName)
{
  INFO_ASSERT(*argc >= 1, "Argv is empty.");		// Need argv[0] to set sMemDumpExe
  sMemDumpExe = argv[0];
  sMemDumpName = dumpFileName;	// Might be NULL.
				// Can be overridden by -dumpMemory in argv.

  // Scan for -dumpMemory <memdb> switch, removing those args from argv
  int skip = 0;

  // I don't want to use CRYPT here because this is a util.  So
  // do something basic to avoid failing test/string_hiding.  This
  // ugly business creates dumpMemory on the stack
  char dumpMemory[100];
  static const char f[] = "\111\001\023\012\030\044\017\006\003\037\027";
  int i;
  for (i = 0; f[i] != '\0'; ++i)
    dumpMemory[i] = f[i] ^ (i + 100);
  dumpMemory[i] = '\0';

  for (int i = 0; i < *argc - 1; ++i)
  {
    if (skip != 0)
      argv[i] = argv[i + skip];
    else if (strcmp(argv[i], dumpMemory) == 0)
    {
      sMemDumpName = argv[i + 1];
      argv[i] = argv[i + 2];
      if (i + 3 < *argc)
        argv[i + 1] = argv[i + 3];
      ++i;                      // skip over filename
      skip = 2;
    }
  }
  *argc -= skip;
  argv[*argc] = NULL;

  // If the user has turned on memory debugging, then read the symbol
  // table immediately
  if (sMemDumpName != NULL)
  {
    if (sIsInitialized)
    {
      fprintf(stderr, "CarbonMem::init has been called after memory allocation.\n");
      fprintf(stderr, "This may be caused by static constructors, which we recommend\n");
      fprintf(stderr, "avoiding.  In the meantime, to turn on memory debugging, use\n");
      fprintf(stderr, "   setenv CARBON_MEM_DUMP <dumpfilename>\n");
      fprintf(stderr, "   setenv CARBON_MEM_EXE <exefilename>\n");
      exit(1);
    }
    sInit();
  }

#if pfGCC_3_3 || pfGCC_3_4
  // gcc 3.3/3.4 has a leaky library
#if pfLP64
  const size_t staticallyConstructedAllocs = 48; // from C++ library
#else
  const size_t staticallyConstructedAllocs = 24; // from C++ library
#endif
#elif pfICC_7
  const size_t staticallyConstructedAllocs = 160; // A little worse...
#else
  const size_t staticallyConstructedAllocs = 16; // from C++ library (?)
#endif

#ifdef CDB
  mMemLeakCheck = true;
#else
  mMemLeakCheck = (getenv("CARBON_MEM_CHECK_LEAKS") != NULL);
#endif
  if (getenv("CARBON_MEM_NO_CHECK_LEAKS") != NULL) {
    mMemLeakCheck = false;      // overrides CDB or the enabling env v
  }
  mAllocCount = getBytesAllocated();

  if ((mAllocCount > staticallyConstructedAllocs) && mMemLeakCheck)
  {
    UtIO::cerr() << "Please eliminate any new static constructors!!!\n";
    UtIO::cerr() << "Based on Cheetah/Jaguar, & std::ostreams we expected "
         << staticallyConstructedAllocs << " bytes allocated\n";
    UtIO::cerr() << "at the start of main(), but we got "
                 <<  mAllocCount << UtIO::endl;
    exit(1);
  }
//  size_t trapCount = CarbonMem::getBytesTrapped();
} // CarbonMem::CarbonMem

CarbonMem::~CarbonMem() {
  if (mMemLeakCheck) {
    UtIO::memCleanup();
    size_t finalAllocCount = CarbonMem::getBytesAllocated();
    //size_t finalTrapCount = CarbonMem::getBytesTrapped();
    if (finalAllocCount != mAllocCount) {
      if (gMemHistogram != NULL)
        CarbonMem::checkpoint("carbon_mem_leak");

      CarbonMem::stopHere();
      UtIO::cout() << "after all deletions, there are still "
           << (finalAllocCount - mAllocCount) << " bytes allocated.\n";
      UtIO::cout() << "You may also want to try re-running the program\n";
      UtIO::cout() << "with switch:\n";
      UtIO::cout() << "   -dumpMemory mem.dump\n";
      UtIO::cout() << "(this switch must be on the command line)\n";
      UtIO::cout() << "and env var CARBON_MEM_CHECK_LEAKS=1\n";
      UtIO::cout() << "\n";
      UtIO::cout() << "You can than decode the generated dump, using carbondb:\n";
      UtIO::cout() << "\n";
      UtIO::cout() << "  carbondb -memDebug mem.dump -memDetail 1 -memPointers\n\n";
      UtIO::cout() << "You can re-run carbondb with varying levels of detail.\n";
      exit(15);
    } // if
  /*
    else if (finalTrapCount != trapCount)
    {
      UtIO::cout() << "An additional " << (finalTrapCount - trapCount) << " bytes\n";
      UtIO::cout() << "is trapped in the freelist system, despite the fact that\n";
      UtIO::cout() << "the allocation size has not grown.\n";
    }
  */
  } // if

  cleanup();
} // CarbonMem::~CarbonMem

void CarbonMem::disableLeakDetection()
{
  mMemLeakCheck = false;
}

bool CarbonMem::isCheckingLeaks() const
{
  return mMemLeakCheck;
}

bool CarbonMem::isMemDebugOn()
{
  return sMemDumpName != NULL;
}


// Break on memStopHere if you want to know when a watched
// pointers is deleted
void CarbonMem::stopHere()
{
}

static MemHistogram* sReplayHistogram = NULL;

bool CarbonMem::replay(const char* dumpFile, const char* exeName,
                       int detail, bool listPointers)
{
  if (sReplayHistogram != NULL)
    delete sReplayHistogram;
  sReplayHistogram = new MemHistogram(dumpFile, exeName, false);
  return sReplayHistogram->replay(detail, listPointers);
}

void CarbonMem::watch(void* ptr)
{
  if (sNumDebugPointers == NUM_DEBUG_POINTERS)
    //cout << "Too many watched pointers (" << sNumDebugPointers << ")" << UtIO::endl;
    ;
  else
  {
    sDebugPointers[sNumDebugPointers++] = ptr;
  }
}

void CarbonMem::unwatch(void* ptr)
{
  for (int i = 0; i < sNumDebugPointers; ++i)
  {
    if (sDebugPointers[i] == ptr)
    {
      sDebugPointers[i] = sDebugPointers[sNumDebugPointers];
      --sNumDebugPointers;
      break;
    }
  }

}

static inline void sCheckPointer(void* ptr)
{
  if (sNumDebugPointers != 0)
  {
    for (int i = 0; i < sNumDebugPointers; ++i)
      if (sDebugPointers[i] == ptr)
        CarbonMem::stopHere();
  }
}

// Round to 8 bytes on LP64, so three LSBs of every address we allocate will
// remain 0 to allow the small pointer hack.
#if pfLINUX64
const std::size_t cMinAlloc = 8;
#else
const std::size_t cMinAlloc = sizeof(unsigned int);
#endif

static inline std::size_t sRoundup(std::size_t sz)
{
  // Round up to multiple of 8 bytes
  /* malloc (0) is unpredictable; avoid it.  */
  if (sz == 0)
    sz = 1;
  sz += ((cMinAlloc - sz)&(cMinAlloc-1)) & (cMinAlloc-1);

  // When we improve the memory allocator, we will be able to detect
  // what size we have allocated by looking at the pointer (e.g. different
  // sizes will be in different pools).  In the meantime, for debug scribbling,
  // we need to keep the value in the pointer itself.
  sz += CARBON_MEM_SIZE_OVERHEAD;

  // Return at least enough bytes to hold a pointer, so the memory can
  // be placed on the free list.
  return std::max(sz, sizeof(void*));
}

std::size_t CarbonMem::roundup(std::size_t sz) {
  return sRoundup(sz);
}

static void sScribbleMemory(void *ptr, std::size_t sz)
{
  // Blast ugly values over all over memory to help catch use-after-free
  UInt32* lp = (UInt32*) ptr;
  sz /= sizeof(UInt32);
  while (sz != 0)
  {
    *lp = 0xdeadbeef;
    ++lp;
    --sz;
  }
}

void CarbonMem::check(void* ptr)
{
  sCheckPointer(ptr);
}

void CarbonMem::scribble(void* ptr, size_t sz)
{
  sScribbleMemory(ptr, sz);
}

std::size_t sGetSize(void* ptr, void** fixedPtr, bool)
{
  void* sizePtr = (void*) (((char*) ptr) - CARBON_MEM_SIZE_OVERHEAD);
  unsigned long* lp = (unsigned long*) sizePtr;
  *fixedPtr = sizePtr;
  return *lp;
}

void CarbonMem::free(void* ptr)
{
  if (gMemSystemUseMalloc)
  {
    ::free(ptr);
    return;
  }

  if (ptr)
  {
    sCheckPointer(ptr);
    if (!sSuppressScribble)
    {
#ifdef CDB
      void* realPtr;
      std::size_t sz = CarbonGetSize(ptr, &realPtr, true);
      sScribbleMemory(ptr, sz);
#endif

#ifdef MEM_DEBUG_ENABLE
      if (gMemHistogram != NULL)
        gMemHistogram->erase(ptr);
#endif
    }

    // UtIO::cout() << "**** free " << ptr << UtIO::endl;
    CarbonFree(ptr, 0);
  }
} // void CarbonMem::free

void CarbonMem::deallocate(void* ptr, size_t sz)
{
  carbonmem_dealloc(ptr, sz);
}

//! new function which is non-throwing, used to implement
//! both throwing and non-throwing new.
void* CarbonMemAllocate(std::size_t sz, bool needOverhead)
{
  if (!sIsInitialized)
  {
//#ifdef PTHREADS
    //static pthread_once_t once = PTHREAD_ONCE_INIT;
//    pthread_once(&once, sInit);
//#else
    sInit();
//#endif
  }
  if (gMemSystemUseMalloc)
    return malloc(sz);

  void *p;

  sz = sRoundup(sz);
  p = CarbonMalloc (sz, needOverhead);
  // UtIO::cout() << "**** malloc " << p << UtIO::endl;

  while (p == 0)
  {
    fprintf(stderr, "\n\nMemory exhausted on request for %lu bytes, with %lu thus far\n",
            (unsigned long) sz,
            (unsigned long) CarbonMem::getBytesAllocated());

    //if (! handler)
      std::abort();
    //p = (void *) malloc (sz);
  }

  // Keep the allocated size in the data for now (see sRoundup)
  if (!sSuppressScribble)
  {
#ifndef CARBON_MEMORY_SYSTEM
    unsigned long* pl = (unsigned long*) p;
    sz -= CARBON_MEM_SIZE_OVERHEAD;
    *pl = sz;
    p = (void*) (((char*) p) + CARBON_MEM_SIZE_OVERHEAD);
#endif
#ifdef MEM_DEBUG_ENABLE
    void* realPtr;
    std::size_t realSize = CarbonGetSize(p, &realPtr, false);
    if (realSize == 0)
      realSize = sz;
    if (gMemHistogram != NULL)
      gMemHistogram->record(p, realSize);
#endif
#ifdef CDB
    sScribbleMemory(p, realSize);
#endif
  }
  sCheckPointer(p);
  return p;
} // static void* CarbonMemAllocate

void* CarbonMem::allocate(std::size_t sz)
{
  return CarbonMemAllocate(sz, false);
}

void* CarbonMem::malloc(std::size_t sz)
{
  return CarbonMemAllocate(sz, true);
}

// Reallocation of memory, where the memory came from carbonmem_alloc,
// or a previous call to carbonmem_reallocate.  It will be freed
// by carbonmem_dealloc.  The burden is on the user to provide the
// previous size of the object.
void* carbonmem_reallocate(void* ptr, size_t old_size, size_t new_size) {
  if (gMemSystemUseMalloc) {
    return ::realloc(ptr, new_size);
  }
  else if (ptr == NULL) {
    ptr = CarbonMem::allocate(new_size);
  }
  else if (new_size == 0) {
    CarbonMem::deallocate(ptr, old_size);
    ptr = NULL;
  }
#ifdef MEM_DEBUG_ENABLE
  else if (gMemHistogram != NULL) {
    gMemHistogram->erase(ptr);
    old_size = sRoundup(old_size);
    ptr = CarbonRealloc(ptr, old_size, sRoundup(new_size), false);
    void* realPtr;
    std::size_t realSize = CarbonGetSize(ptr, &realPtr, false);
    if (realSize == 0) {
      realSize = new_size;
    }
    gMemHistogram->record(ptr, realSize);
  }
#endif
  else {
    ptr = CarbonRealloc(ptr, sRoundup(old_size), sRoundup(new_size), false);
  }
  return ptr;
} // void* carbonmem_realloc

// Reallocation of memory, where the memory came from carbonmem_malloc,
// or a previous call to carbonmem_realloc.  It will be freed
// by carbonmem_free.  The user does not have to keep track of the
// previous size.  But we will have to allocate overhead bytes whenever
// the memory block exceeds our 64k max chunk size.
void* carbonmem_realloc(void* ptr, size_t new_size) {
  // If we are using the system malloc, just call realloc and
  // skip the Carbon malloc and histogram operations.
  if (gMemSystemUseMalloc) {
    return ::realloc(ptr, new_size);
  }

  if (ptr == NULL) {
    ptr = CarbonMem::malloc(new_size);
  }
  else if (new_size == 0) {
    CarbonMem::free(ptr);
    ptr = NULL;
  }
#ifdef MEM_DEBUG_ENABLE
  if (gMemHistogram != NULL) {
    gMemHistogram->erase(ptr);
    ptr = CarbonRealloc(ptr, 0, sRoundup(new_size), true);
    void* realPtr;
    std::size_t realSize = CarbonGetSize(ptr, &realPtr, true);
    if (realSize == 0) {
      realSize = new_size;
    }
    gMemHistogram->record(ptr, realSize);
  }
#endif
  else {
    ptr = CarbonRealloc(ptr, 0, sRoundup(new_size), true);
  }
  return ptr;
} // void* carbonmem_realloc


/*
bool CarbonMem::printAllocs(int detail, bool listPointers)
{
  return printAllocsToFile(detail, listPointers, NULL);
}
*/

void CarbonMem::checkpoint(const char* filename)
{
/*
  if (sReplayHistogram != NULL)
    return sReplayHistogram->print(detail, listPointers, filename);
*/

  if (gMemHistogram == NULL)
  {
    fprintf(stdout, "CarbonMem::printAllocs requires running with environment variable\n");
    fprintf(stdout, "CARBON_MEM_DUMP set, or cooperatation between main() and CarbonMem's constructor\n\n");
    fflush(stdout);
  }
  else
    gMemHistogram->checkpoint(filename);
}

void CarbonMem::cleanup()
{
  if (gMemHistogram != NULL)
  {
    //(void) gMemHistogram->flush();
    MemHistogram* hist = gMemHistogram;
    gMemHistogram = NULL;
    delete hist;
  }
}

void CarbonMem::flush()
{
  if (gMemHistogram != NULL)
    (void) gMemHistogram->flush();
}

// do not remove or crypt the sBuiltInCopyright*, it is included so that
// executing the strings command on an executable or library will
// display a copyright notice.  
// This is included in MemManager because this file is expected to be
// included in every carbon product, and thus the copyright will be
// built in.
const char* CarbonMem::sBuiltInCopyrightDate = "\
Copyright (c) 2002-2014 by Carbon Design Systems, Inc., All Rights Reserved.\n\
";

const char* CarbonMem::sBuiltInCopyrightNotice = "\
 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION\n\
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR\n\
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT\n\
 OF CARBON DESIGN SYSTEMS, INC.\n\
";

const char* CarbonMem::sBuiltInTechSupportNotice ="\
 For technical support send email to support@carbondesignsystems.com\n\
                       or call +1 978.264.7399\n\
";

const char* CarbonMem::sBuiltInRightsReservedNotice ="\
     All Rights Reserved. Use, disclosure or duplication without\n\
   prior written permission of Carbon Design Systems is prohibited.\n\
";



// extern C function definitions

void* carbonmem_malloc(size_t size)
{
  return CarbonMem::malloc(size);
}

void* CarbonMem::reallocate(void* p, size_t old_size, size_t new_size) {
  return carbonmem_reallocate(p, old_size, new_size);
}

void carbonmem_free(void* ptr)
{
  CarbonMem::free(ptr);
}

void* carbonmem_alloc(size_t size)
{
  return CarbonMemAllocate(size, false);
}

void carbonmem_dealloc(void* ptr, size_t sz)
{
  if (ptr == NULL)        // 'delete NULL' is just too damn common to debug now
    return;

  if (gMemSystemUseMalloc)
  {
    free(ptr);
    return;
  }

  if (!sSuppressScribble)
  {
    // To properly account for all the bytes allocated and freed, we
    // must, as a rule, use the number of bytes that we actually
    // allocated, not the number of bytes requested.  If the user
    // requests  57 bytes, we probably allocate 64.  We need to
    // record 64, not 57.  Even if the user is using this deallocate
    // interface where he is promising to tell us what size he
    // requested, we must, for consistency, send the 64
    // size to CarbonFree, not 57.
    void* realPtr;
    std::size_t realSize = CarbonGetSize(ptr, &realPtr, false);
    if (realSize == 0) {
      sz = sRoundup(sz);
    }
    else {
      sz = realSize;
    }
#ifdef CDB
    sScribbleMemory(ptr, realSize);
#endif
#ifdef MEM_DEBUG_ENABLE
    //INFO_ASSERT((realSize == sRoundup(sz),"size inconsistency"));
    if (gMemHistogram != NULL)
      gMemHistogram->erase(ptr);
#endif
  }

  if (ptr)
  {
    sCheckPointer(ptr);
    // UtIO::cout() << "**** free " << ptr << UtIO::endl;
    CarbonFree(ptr, sz);
  }
}

// Here is a function to substitute for the automatic call generated to
// the C++ library function __cxa_pure_virtual, for every pure virtual
// class.                   Carbon_PureVirtual   it has to have the
// exact same number of characters, and as you can see, it does.
extern "C" void Carbon_PureVirtual() {
  INFO_ASSERT(0, "pure virtual function called");
}

#if pfGCC_4_2
// replacing static constructor guard to serialize initializations.
// gcc-4.2 adds calls to __cxa_guard_acquire() and __cxa_guard_release() to
// serialize initialization of static constructors.  Carbon renames these entries
// in the carbon libraries and doesn't have any static constructors, so we can
// just have these dummy routines.
extern "C" void Carbon_guard_release (void *)
{
  INFO_ASSERT (0, "lock release called");
}

extern "C" void Carbon_guard_acquire (void *)
{
  INFO_ASSERT (0, "lock acquire called");
}
#endif
  
