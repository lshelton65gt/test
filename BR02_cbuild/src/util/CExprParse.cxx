//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/UtString.h"
#include "CExprPriv.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

static UtStringSet* sIdentifiers = NULL;

int carbonCExpr_lex(int*) {
  int lex_ret = carbonCExpr_lex();
  return lex_ret;
}

// This is a hook that I put into the Flex code to keep track of
// how many charactesr have been parsed so far.  This was an attempt
// to get a column-count so we could report the column for a syntax
// error.  I've since given up on trying to do this.
void carbonCExprCount() {
}

// It appears that Flex has no way of communicating an error to Bison,
// so if we get a bogus character (e.g. @) then in the Flex code, we
// call this function.  We can then make sure there were no flex errors
// before we return to the user.
static bool sFlexError = false;
void carbonCExpr_error(const char*) {
  sFlexError = true;
} // void carbonCExpr_error

bool carbonCExprValidate(const char* str, UtStringSet* identifiers) {
  sIdentifiers = identifiers;
  sFlexError = false;
  carbonCExpr__scan_string(str);
  int ret = carbonCExpr_parse() || sFlexError;
  sIdentifiers = NULL;
  return ret == 0;
}

void carbonCExprDeclareIdent(const char* ident) {
  if (sIdentifiers != NULL) {
    sIdentifiers->insert(ident);
  }
}
