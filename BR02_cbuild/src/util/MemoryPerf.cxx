// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/MemoryPerf.h"
#if !pfMSVC
#include <unistd.h>
#endif
#include <cstdio>
#include <cstring>

// Allocate the pointer to the global memory counter.
MemoryPerf * MemPC;

MemoryPerfData::MemoryPerfData()
{
  reset();
}


void MemoryPerfData::reset()
{
  mReads  = 0;
  mWrites = 0;
}


MemoryPerf::MemoryPerf()
{
}


MemoryPerf::~MemoryPerf()
{
  // Dump the statistics.
  print();

  // Cleanup the statistics.
  for (MemoryStatsMap::iterator iter = mPerfData.begin();
       iter != mPerfData.end();
       ++iter) {
    delete static_cast<MemoryStatsMap::mapped_type>(iter->second);
  }
}


void MemoryPerf::check(UInt32 width, UInt32 depth)
{
  MemoryWidthDepth dimension(width,depth);
  if (mPerfData.find(dimension) == mPerfData.end()) {
    mPerfData[dimension] = new MemoryPerfData();
  }
}


#define GENCOUNTER(N) \
void MemoryPerf::count##N(UInt32 width, UInt32 depth) \
{                                                     \
  check(width,depth);                                 \
  MemoryWidthDepth dimension(width,depth);            \
  mPerfData[dimension]->m##N++;                       \
}


GENCOUNTER(Reads)
GENCOUNTER(Writes)
GENCOUNTER(Constructed)


void MemoryPerf::reset()
{
  for (MemoryStatsMap::iterator iter = mPerfData.begin();
       iter != mPerfData.end();
       ++iter) {
    iter->second->reset();
  }
}


void MemoryPerf::print() const
{
  std::fflush (stdout);
  std::fprintf(stdout, "\n\n");
  std::fprintf(stdout, "Memory Profiling Statistics\n");
  //                             1         2         3         4         5         6         7
  //                    1234567890123456789012345678901234567890123456789012345678901234567890
  std::fprintf(stdout, "    Dimension Constructed      Reads     Writes\n");

  for (MemoryStatsMap::SortedLoop iter = mPerfData.loopSorted ();
       ! iter.atEnd ();
       ++iter) {
    MemoryWidthDepth dimension = iter.getKey();
    MemoryPerfData * data      = iter.getValue();

    std::fprintf(stdout,
                 "<%4u,%6u> %11u%11u%11u\n",
                 dimension.first,
                 dimension.second,
                 data->mConstructed,
                 data->mReads,
                 data->mWrites);
  }

  std::fprintf(stdout, "\n\n");
  std::fflush (stdout);
}
