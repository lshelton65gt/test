// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2005-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "util/UtVector.h"
#include "util/Graph.h"
#include "util/GraphSCC.h"
#include "util/GraphDepthWalker.h"
#include "util/GraphMerge.h"
#include "util/Loop.h"
#include "util/LoopMap.h"
#include "util/UtIOStream.h"


// Public Functions

bool UtGraphMerge::init(BiGraph* graph)
{
  // Store the graph
  mGraph = graph;

  // Run strongly connected components to make sure the graph is
  // acylic. If it isn't, merge the nodes in the cycle and mark them
  // unmergable.
  GraphSCC graphSCC;
  graphSCC.compute(mGraph);
  if (graphSCC.hasCycle()) {
    GraphSCC::ComponentLoop l;
    for (l = graphSCC.loopCyclicComponents(); !l.atEnd(); ++l) {
      // Encapsulate this component into one node (it must have more
      // than one node)
      GraphSCC::Component* comp = *l;
      INFO_ASSERT(comp->size() > 1, "Inconsistency found in block merge graph");
      encapsulateComponent(comp);
    }
  } // if

  // Init is complete
  mInitSuccess = true;
  return true;
}

void UtGraphMerge::mergeByDepth(NodeCmp* nodeBuckets, NodeCmp* nodeOrder)
{
  // Make sure init was called
  INFO_ASSERT(mInitSuccess, "Merge by depth operation called without valid init");

  // Compute the depth of all the nodes
  computeDepth();

  // Walk the groups of mergable blocks and merge the ones with the
  // same depth
  mergeNodesByDepth(nodeBuckets, nodeOrder);

  // Remove nodes from the graph that have been merged
  cleanupMergedNodes();
}

void UtGraphMerge::mergeFanin(NodeCmp* nodeOrder, EdgeCmp* edgeOrder)
{
  // Make sure init was called
  INFO_ASSERT(mInitSuccess, "Merge by depth operation called without valid init");

  // Gather all the nodes that can be merged. They should be sorted by
  // the most fanin to improve merging performance.
  NodeVector nodes;
  gatherStartNodes(&nodes, nodeOrder);

  // Walk the start nodes and merge fanin
  mergeNodesWithFanin(nodes, nodeOrder, edgeOrder);

  // Remove nodes from the graph that have been merged
  cleanupMergedNodes();
}

void UtGraphMerge::mergeByDepthOrder(NodeCmp* nodeBuckets, NodeCmp* nodeOrder)
{
  // Make sure init was called
  INFO_ASSERT(mInitSuccess, "Merge by depth operation called without valid init");

  // Compute the depth of all the nodes
  computeDepth();

  // Walk the groups of mergable blocks and merge the ones with the
  // same depth
  mergeNodesByDepthOrder(nodeBuckets, nodeOrder);

  // Remove nodes from the graph that have been merged
  cleanupMergedNodes();
  mInitSuccess = false;
}


// Private Classes and Functions

// Definitions for flags on nodes and edges
#define GM_MERGED        (1 << 16) //!< This node has been merged into another
#define GM_UNMERGE_PATH  (1 << 17) //!< Reachable through an unmergable node

//! Class to do a DFS walk of the graph and compute its depth
class GMDepthWalker : public UtGraphDepthWalker
{
public:
  //! constructor
  GMDepthWalker(UtGraphMerge* graphMerge) : mGraphMerge(graphMerge) {}

  // Override the visitBackEdge to make sure there are no cycles
  Command visitBackEdge(Graph*, GraphNode*, GraphEdge*)
  {
    INFO_ASSERT(0, "Found a cycle in the block merging graph!");
    return GW_SKIP;
  }

protected:
  //! Override function to update the depth of a node
  SIntPtr updateDepth(Graph* graph, GraphNode* node, SIntPtr faninDepth)
  {
    // Compute the new depth, only mergable nodes increment the depth
    SIntPtr newDepth;
    if (mGraphMerge->nodeMergable(node)) {
      newDepth = faninDepth + 1;
    } else {
      if (faninDepth == 0) {
        newDepth = 1;
      } else {
        newDepth = faninDepth;
      }
    }

    // If the depth increased, update it. Return the max of the depths
    SIntPtr depth = getDepth(graph, node);
    if (newDepth > depth) {
      putDepth(graph, node, newDepth);
    } else {
      newDepth = depth;
    }
    return newDepth;
  }

  // The graph merge class to call nodeMergable()
  UtGraphMerge* mGraphMerge;
}; // class GMDepthWalker : public UtGraphDepthWalker

void UtGraphMerge::computeDepth()
{
  // Compute the depth through a DFS walk
  GMDepthWalker depthWalker(this);
  depthWalker.walk(mGraph);
}

//! A class to sort nodes by depth and a provided sub sorting class
class GMNodeCmpDepth : public UtGraphMerge::NodeCmp
{
public:
  //! constructor
  GMNodeCmpDepth(Graph* graph, UtGraphMerge::NodeCmp* subCmp) :
    mGraph(graph), mSubCmp(subCmp)
  {}

  //! Compare operator
  bool operator()(const GraphNode* n1, const GraphNode* n2) const
  {
    // Sort by depth first since it is known to be fast. The depth is
    // stored in the scratch.
    SIntPtr depth1 = mGraph->getScratch(n1);
    SIntPtr depth2 = mGraph->getScratch(n2);
    if (depth1 == depth2) {
      // Same depth, ask the sub compare function to sort further
      if (mSubCmp != NULL) {
        return (*mSubCmp)(n1, n2);
      } else {
        // No sub compare, just return they are the same depth
        return false;
      }
    } else if (depth1 < depth2) {
      return true;
    } else {
      return false;
    }
  }

private:
  Graph* mGraph;
  UtGraphMerge::NodeCmp* mSubCmp;
}; // class GMNodeCmpDepth : public UtGraphMerge::NodeCmp

//! A class to sort by a provided order
/*! A wrapper is needed so that it calls the correct compare function
 *  and so that it is defined. The std::sort routine needs the class
 *  passed by value.
 */
class GMNodeOrder : public UtGraphMerge::NodeCmp
{
public:
  //! constructor
  GMNodeOrder(UtGraphMerge::NodeCmp* subCmp) : mSubCmp(subCmp)
  {}

  //! Compare operator
  bool operator()(const GraphNode* n1, const GraphNode* n2) const
  {
    return (*mSubCmp)(n1, n2);
  }

private:
  UtGraphMerge::NodeCmp* mSubCmp;
};

void UtGraphMerge::mergeNodesByDepth(NodeCmp* nodeBuckets, NodeCmp* nodeCmp)
{
  // Insert the nodes into a sorted map. Each item in the map is the
  // set of nodes that can be merged.
  GMNodeCmpDepth cmpDepth(mGraph, nodeBuckets);
  typedef UtMap<GraphNode*, NodeVector, GMNodeCmpDepth> MergeGroups;
  MergeGroups mergeGroups(cmpDepth);
  for (Iter<GraphNode*> i = mGraph->nodes(); !i.atEnd(); ++i) {
    // Make sure the node is mergable
    GraphNode* node = *i;
    if (nodeMergable(node)) {
      // Insert it into the right vector
      NodeVector& nodes = mergeGroups[node];
      nodes.push_back(node);
    }
  }

  // Walk the sorted map and merge any nodes that can be merged
  // (vector size > 1)
  typedef MergeGroups::iterator MergeGroupsIter;
  for (MergeGroupsIter i = mergeGroups.begin(); i != mergeGroups.end(); ++i) {
    NodeVector& nodes = i->second;
    if (nodes.size() > 1) {
      // Sort the nodes so that we get consistent output. Since we are
      // merging by depth there is no order.
      GMNodeOrder nodeOrder(nodeCmp);
      std::sort(nodes.begin(), nodes.end(), nodeOrder);

      // Do the merge
      mergeGraphNodes(nodes);

      // The graph is all done, merge the users data as well
      mergeNodes(nodes);
    }
  }
} // void UtGraphMerge::mergeNodesByDepth

void UtGraphMerge::mergeNodesByDepthOrder(NodeCmp* nodeBuckets, NodeCmp* nodeCmp)
{
  // Insert the nodes into a sorted map. Each item in the map is the
  // set of nodes that can be merged.
  GMNodeOrder bucketOrder(nodeBuckets);
  typedef UtMap<GraphNode*, NodeVector, GMNodeOrder> MergeGroups;
  MergeGroups mergeGroups(bucketOrder);
  for (Iter<GraphNode*> i = mGraph->nodes(); !i.atEnd(); ++i) {
    // Make sure the node is mergable
    GraphNode* node = *i;
    if (nodeMergable(node)) {
      // Insert it into the right vector
      NodeVector& nodes = mergeGroups[node];
      nodes.push_back(node);
    }
  }

  // Walk the sorted map and merge any nodes that can be merged
  // (vector size > 1)
  typedef MergeGroups::iterator MergeGroupsIter;
  for (MergeGroupsIter i = mergeGroups.begin(); i != mergeGroups.end(); ++i) {
    NodeVector& nodes = i->second;
    if (nodes.size() > 1) {
      // Sort the nodes so that we get consistent output. Since we are
      // merging by depth there is no order. Also sort by depth
      GMNodeCmpDepth nodeOrder(mGraph, nodeCmp);
      std::sort(nodes.begin(), nodes.end(), nodeOrder);

      // Do the merge
      mergeGraphNodes(nodes);

      // The graph is all done, merge the users data as well
      mergeNodes(nodes);
    }
  }
} // void UtGraphMerge::mergeNodesByDepthOrder

void UtGraphMerge::gatherStartNodes(NodeVector* nodes, NodeCmp* nodeOrder)
{
  // Compute the depth, we sort so that we get nodes with the most fanin first
  computeDepth();

  // Gather all the nodes for merging
  for (Iter<GraphNode*> i = mGraph->nodes(); !i.atEnd(); ++i) {
    GraphNode* node = *i;
    if (validStartNode(node) && nodeMergable(node)) {
      nodes->push_back(node);
    }
  }

  // Sort them by depth and the users compare function
  GMNodeCmpDepth cmpDepth(mGraph, nodeOrder);
  std::sort(nodes->begin(), nodes->end(), cmpDepth);
}

void 
UtGraphMerge::mergeNodesWithFanin(const NodeVector& nodes, NodeCmp* nodeOrder,
                                  EdgeCmp* edgeOrder)
{
  // start at every node that hasn't been merged yet and merge it with
  // its fanin
  for (NodeVector::const_reverse_iterator i = nodes.rbegin();
       i != nodes.rend(); ++i) {
    GraphNode* node = *i;
    if (!mGraph->anyFlagSet(node, GM_MERGED) && nodeMergable(node)) {
      mergeNodeWithFanin(node, nodeOrder, edgeOrder);
    }
  }
}

//! Class to walk a graph from a starting point and find unmergable nodes
/*! There are two types of unmergable nodes, one is nodes that are
 *  unmergable on their own. The other is nodes that are the reachable
 *  from an unmergable edge.
 */
class UnmergableNodesWalker : public GraphSortedWalker
{
public:
  //! constructor
  UnmergableNodesWalker(GraphNodeSet* nodes, UtGraphMerge* graphMerge) :
    mNodes(nodes), mGraphMerge(graphMerge) {}

  //! Override visitNodeBefore to check if this node is unmergable on its own
  Command visitNodeBefore(Graph* graph, GraphNode* node)
  {
    // Clear any flags for the later pass
    graph->clearFlags(node, GM_UNMERGE_PATH);

    // Check if this node is mergable on its own
    if (!mGraphMerge->nodeMergable(node)) {
      mNodes->insert(node);
      return GW_SKIP;
    }
    return GW_CONTINUE;
  }

  //! Override visitTreeEdge to test if two nodes are mergable
  Command visitTreeEdge(Graph* graph, GraphNode*, GraphEdge* edge)
  {
    return edgeMergable(graph, edge);
  }

  //! Override visitCrossEdge to test if two nodes are mergable
  Command visitCrossEdge(Graph* graph, GraphNode*, GraphEdge* edge)
  {
    return edgeMergable(graph, edge);
  }

  //! Override visitBackEdge to make sure the graph is acylic
  Command visitBackEdge(Graph*, GraphNode*, GraphEdge*)
  {
    INFO_ASSERT(0, "Found a cycle during a graph merging operation");
    return GW_CONTINUE;
  }

private:
  //! Function to test an edge
  Command edgeMergable(Graph* graph, GraphEdge* edge)
  {
    if (!mGraphMerge->edgeMergable(edge)) {
      GraphNode* to = graph->endPointOf(edge);
      mNodes->insert(to);
      return GW_SKIP;
    }
    return GW_CONTINUE;
  }

  //! The unmergable nodes in the graph
  GraphNodeSet* mNodes;

  //! The GraphMerge class to test if a node or edge is mergable
  UtGraphMerge* mGraphMerge;
}; // class UnmergableNodesWalker : public GraphWalker

//! Class to mark all the unmergable paths from unmergable nodes
class UnmergablePathsWalker : public GraphWalker
{
public:
  //! constructor
  UnmergablePathsWalker() {}

  //! Override the visitNodeAfter to mark the node unmergable
  Command visitNodeAfter(Graph* graph, GraphNode* node)
  {
    graph->setFlags(node, GM_UNMERGE_PATH);
    return GW_CONTINUE;
  }
};

//! Class to walk a graph in order, gathering the mergable nodes
class GatherMergableFanin : public GraphSortedWalker
{
public:
  //! Abstraction for a vector of nodes
  typedef UtVector<GraphNode*> NodeVector;

  //! constructor
  GatherMergableFanin(NodeVector* nodes) : mNodes(nodes) {}

  //! Override the visitNodeBefore to stop iteration over unmergable paths
  Command visitNodeBefore(Graph* graph, GraphNode* node)
  {
    if (graph->anyFlagSet(node, GM_UNMERGE_PATH)) {
      return GW_SKIP;
    } else {
      return GW_CONTINUE;
    }
  }

  //! Override the visitNodeAfter to gather mergable nodes
  Command visitNodeAfter(Graph* graph, GraphNode* node)
  {
    if (!graph->anyFlagSet(node, GM_UNMERGE_PATH)) {
      mNodes->push_back(node);
    }
    return GW_CONTINUE;
  }
private:
  //! Storage for mergable nodes
  NodeVector* mNodes;
}; // class GatherMergableFanin : public GraphWalker

void 
UtGraphMerge::mergeNodeWithFanin(GraphNode* node, NodeCmp* nodeOrder,
                                 EdgeCmp* edgeOrder)
{
  // Walk the graph from this point and gather unmergable nodes. We
  // also clear flags during this pass.
  GraphNodeSet nodeSet;
  UnmergableNodesWalker unmergableNodes(&nodeSet, this);
  unmergableNodes.walkFrom(mGraph, node, nodeOrder, edgeOrder);

  // Walk the graph from all unmergable nodes and mark the nodes that
  // are reachable from unmergable nodes as unmergable. Note that the
  // class is defined outside the loop so that we mark nodes done
  // across iterations of the loop.
  typedef Loop<GraphNodeSet> GraphNodeSetLoop;
  UnmergablePathsWalker unmergablePaths;
  for (GraphNodeSetLoop l(nodeSet); !l.atEnd(); ++l) {
    GraphNode* unmergableNode = *l;
    unmergablePaths.walkFrom(mGraph, unmergableNode);
  }
  nodeSet.clear();

  // Gather all the mergable nodes in the right order. If there are
  // enough, merge them.
  NodeVector nodes;
  GatherMergableFanin gatherMergableFanin(&nodes);
  gatherMergableFanin.walkFrom(mGraph, node, nodeOrder, edgeOrder);
  if (nodes.size() > 1) {
    mergeGraphNodes(nodes);
    mergeNodes(nodes);
  }
}

void UtGraphMerge::mergeGraphNodes(const NodeVector& nodes)
{
  // Grab the last node, all nodes will be merged into it
  NodeVector::const_reverse_iterator n = nodes.rbegin();
  GraphNode* collectNode = *n++;

  // Types for gathering edges
  typedef UtVector<GraphEdge*> EdgeVector;
  typedef Loop<EdgeVector> EdgeVectorLoop;

  // Move the edges to/from all other nodes to the last node
  for (; n != nodes.rend(); ++n) {
    // Gather all the fanin edges for removal
    GraphNode* node = *n;
    EdgeVector faninEdges;
    for (Iter<GraphEdge*> e = mGraph->edges(node); !e.atEnd(); ++e) {
      GraphEdge* edge = *e;
      faninEdges.push_back(edge);
    }

    // Move the edges to the collection node
    for (EdgeVectorLoop l(faninEdges); !l.atEnd(); ++l) {
      // Modify the edge into this node. If the edge already exists,
      // use that one but ask the derived class to merge the two
      // edges.
      GraphEdge* edge = *l;
      GraphEdge* existingEdge = mGraph->modifyInEdge(edge, collectNode);
      if (existingEdge != NULL) {
        // Already there, don't add it again. But also ask the derived
        // class to merge any edge data -- the first argument is the
        // kept edge while the second is the deleted edge.
        mergeEdges(existingEdge, edge);
        delete edge;
      }
    }

    // Gather all the fanout edges for this node
    EdgeVector fanoutEdges;
    for (Iter<GraphEdge*> e = mGraph->reverseEdges(node); !e.atEnd(); ++e) {
      GraphEdge* edge = *e;
      fanoutEdges.push_back(edge);
    }

    // Move the edges to the collection node
    for (EdgeVectorLoop l(fanoutEdges); !l.atEnd(); ++l) {
      // If we aren't going to create a self reference move the edge
      GraphEdge* edge = *l;
      if (mGraph->startPointOf(edge) != collectNode) {
        // Modify the edge into this node. If the edge already exists,
        // use that one but ask the derived class to merge the two
        // edges.
        GraphEdge* existingEdge = mGraph->modifyOutEdge(edge, collectNode);
        if (existingEdge != NULL) {
          // Already there, don't add it again and ask the derived
          // class to merge the edge data.
          mergeEdges(existingEdge, edge);
          delete edge;
        }
      } else {
        // Don't add a self cycle
        mGraph->removeEdge(edge);
        delete edge;
      }
    }

    // Mark this node as merged into another so we don't process it
    // further. We don't delete it till the end.
    mGraph->setFlags(node, GM_MERGED);
  } // for
} // void UtGraphMerge::mergeGraphNodes

void UtGraphMerge::cleanupMergedNodes()
{
  // Gather all nodes to be removed, tell the caller about it
  GraphNodeSet mergedNodes;
  for (Iter<GraphNode*> i = mGraph->nodes(); !i.atEnd(); ++i) {
    GraphNode* node = *i;
    if (mGraph->anyFlagSet(node, GM_MERGED)) {
      mergedNodes.insert(node);
      removeMergedNode(node);
    }
  }
  
  // Clean up the graph
  mGraph->removeNodes(mergedNodes);
}

void UtGraphMerge::encapsulateComponent(GraphSCC::Component* comp)
{
  // gather the nodes in the cycle. Remember the last one, it is the
  // collection node
  NodeVector nodes;
  GraphNode* lastNode = NULL;
  for (GraphSCC::Component::NodeLoop n = comp->loopNodes(); !n.atEnd(); ++n) {
    lastNode = *n;
    nodes.push_back(lastNode);
  }

  // Merge the graph nodes but don't ask the derived class to
  // merge them. This is because it isn't a real merge. We are
  // just merging them to make the graph acyclic.
  mergeGraphNodes(nodes);

  // Remove any self edges from the last node
  UtVector<GraphEdge*> selfEdges;
  for (Iter<GraphEdge*> i = mGraph->edges(lastNode); !i.atEnd(); ++i) {
    GraphEdge* edge = *i;
    GraphNode* end = mGraph->endPointOf(edge);
    if (lastNode == end) {
      selfEdges.push_back(edge);
    }
  }
  for (UtVector<GraphEdge*>::iterator i = selfEdges.begin();
       i != selfEdges.end(); ++i) {
    GraphEdge* edge = *i;
    mGraph->removeEdge(edge);
    delete edge;
  }

  // Now tell the derived class that we merged a set of nodes to
  // make the graph acyclic.
  encapsulateCycle(nodes);
}

void UtGraphMerge::print(const GraphNode* node) const
{
  UtIO::cout() << node << "\n";
}
