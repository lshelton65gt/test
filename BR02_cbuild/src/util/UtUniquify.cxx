// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtUniquify.h"
#include "util/UtString.h"


UtUniquify::UtUniquify(): mNames (new StringSet), mIndex (0)
{
}

UtUniquify::~UtUniquify()
{
  mNames->clearPointers();
  delete mNames;
}

//! make a name unique within the set
const char* UtUniquify::insert(const char* name)
{
  UtString* buf = new UtString(name);

  size_t sz = buf->size();
  while (mNames->find(buf) != mNames->end())
  {
    buf->resize(sz);
    *buf << "_" << mIndex++;
  }
  mNames->insert(buf);
  return buf->c_str();
}
