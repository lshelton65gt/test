// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/MutexWrapper.h"
#include "util/UtIOStream.h"
#include "util/CarbonAssert.h"
#include "util/CarbonAbort.h"
#if pfWINDOWS
#include <windows.h>
#endif

// We need a mutex to ensure thread-safe access to the static data.
// We don't need the common one, because there's no memory allocation.
MUTEX_WRAPPER_DECLARE(sMutex);

CarbonAbortOverride* CarbonAbortOverride::sOverride = NULL;

CarbonAbortOverride::CarbonAbortOverride() {
  MutexWrapper mutex(&sMutex);
  mSaveOverride = sOverride;
  sOverride = this;
}

void CarbonAbortOverride::print(const char* str) {
  MutexWrapper mutex(&sMutex);
  if (sOverride != NULL) {
    sOverride->printHandler(str);
  }
  else {
    UtIO::cerr() << str;
#if pfWINDOWS
    // On Windows, standard error often is redirected to the bit bucket,
    // or if a console application, it often disappears before the developer
    // gets to see what got written to it.  So, also display the message in
    // the debugger, if there is one.
    OutputDebugString(str);
#endif
  }
}

void CarbonAbortOverride::abort() {
  MutexWrapper mutex(&sMutex);
  if (sOverride != NULL) {
    sOverride->abortHandler();
  }
  else {
#ifndef __COVERITY__     // coverity views abort as too natural a death
    std::abort();
#endif
  }
}


void CarbonAbort(const char* file, long lineNumber, const char* expStr, const char* caller)
{
  UtString buf;
  buf << file << ':' << (UInt32)lineNumber << " " << caller
      << "(" << expStr  << ") failed\n";
  // print stack trace ???
  CarbonAbortOverride::print(buf.c_str());
  CarbonAbortOverride::abort();
}

void CarbonPrintAssertBanner() {
  CarbonAbortOverride::print("\n\n******CARBON INTERNAL ERROR*******\n\n");
}

void CarbonHelpfulAssert(const char* file, long lineNumber, 
                         const char* expStr, const char* msg)
{
  UtString buf;
  buf << "\n\n******CARBON INTERNAL ERROR*******\n\n";
  buf << msg << "\n";
  buf << file << ':' << (UInt32)lineNumber;
  if (expStr) {
    buf << " INFO_ASSERT(" << expStr << ") failed\n";
  } else {
    buf << "\n";
  }
  CarbonAbortOverride::print(buf.c_str());
  // print stack trace ???
  CarbonAbortOverride::abort();
}
