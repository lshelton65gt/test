// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// Memory management subsystem
// Joshua Marantz, December 2002
// 30000 feet above North Dakota
//

/*

Goals:

 - 0 overhead for each allocation
 - constant time alloc/free
 - be able reclaim freed blocks for general purpose reuse
 - peaceful coexistence with subsystems that use malloc/free directly
 - debug infrastructure for keeping track of memory usage histogram

Strategy:

 - Allocate very large memory blocks from malloc in a non-wasteful fashion,
   e.g. 8 meg blocks.
 - From those large blocks, allocate smaller chunks, e.g. 64k, which 
   are then used for uniformly allocated units.  We will typically 
   not be able to use 1 of the 64k chunks due to malloc overhead,
   but we can use some of that memory for our own bookkeeping.
-  The overhead is 64/8000, or 0.8%.  We can control that overhead
   by making the large block size bigger.  That's a simple tradeoff
   that can be controlled by adjusting the block and chunk size.
   Larger block sizes give smaller overhead, but will be less
   friendly when used in conjunction with subsystems that use malloc/free.
 - We never return the 8 meg pools for general system use (though we
   could in principle)
 - Large memory allocations will be handled by malloc/free.  We should
   use a blocked array to limit the number of large memory allocations.
   The STL rope class may also be useful for dealing with large strings.
 - We should be able to rapidly tell, with a mask & a lookup, what
   64k chunk a piece of memory came from, and hence what size it is.  We
   are willing to spend some static data storage to do that.

The mechanism for mapping an address to a chunk or block is different
between 64-bit and 32-bit modes, basically because we can index the
entire 32-bit address space but not the 64-bit one.  Details of the
implemenation differences are below.

*/

#include "util/CarbonPthread.h"
#include "util/MemManager.h"
#include "MemHistogram.h"
#include "util/CarbonTypes.h"
#include "util/CarbonAssert.h"
#include <malloc.h>
#include <sys/types.h>
#include <time.h>
#include "util/Stats.h"
#include "util/UtIOStream.h"
#include "util/MutexWrapper.h"

// These are needed for enforcing the datasize rlimit, since Linux doesn't do it.
#if pfLINUX || pfLINUX64
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#endif

extern bool gMemSystemUseMalloc;

#define K 1024
#define M (K*K)
#define G (M*K)
#define BLOCK_SIZE (8*M)
#define CHUNK_SIZE (64*K)
#define CHUNK_SHIFT 16

// We use the 64-bit memory manager on 64-bit Linux, and the 32-bit
// one everywhere else.
#if pfLINUX64
#define MEM_MANAGER_64 1
#else
#define MEM_MANAGER_64 0
#endif

#if MEM_MANAGER_64

// On 64-bit systems we will access 4Gig*8 = 32Gig.
// The address space is divided into 512K 64K chunks.
//
// To map blocks and chunks, we'll group blocks into 4G megablocks
// based on the high 32-bits of the block's starting address.  We'll
// track all the megablocks being used, and assign each an ID.
//
// To map a chunk or block to an index, we'll use the same basic
// strategy as the 32-bit manager, except that we start with a
// sub-index within the megablock.  We then add an offset based on the
// megablock's ID to get the actual index.
# define NUM_CHUNKS 8*(64*K)
# define MIN_SIZE_FOR_MAGIC_PATTERN 12
# define MALLOC_OVERHEAD 16
typedef UInt32 ChunkID;


#else

// Our 4 gigs of address space is divided into 64k 64k chunks.  Our
// minimum allocation size is 4 bytes, so in each chunk, there can be
// up to 64k/4 = 16k allocations.  We can represent that with 2 bytes.
// So for a fixed overhead of 64k*2 = 128k bytes, we can keep track of
// the number of allocated objects in each chunk.  Obviously this changes
// in 64-bit compiles.   
# define NUM_CHUNKS (64*K)
# define MIN_SIZE_FOR_MAGIC_PATTERN 8
# define MALLOC_OVERHEAD 8
typedef UInt16 ChunkID;

#endif

extern MemHistogram* gMemHistogram;
#define RECORD_BYTES() ((gMemHistogram == NULL) || !gMemHistogram->isActive())


// Keep track of all the blocks we can allocate in 32-(or 35-)bits.  There
// are 4G/8M = 512 (or 8 times that = 4K) of them.  Do the arithmetic
// scaled down by a meg.  Each block has a unique small integer index which
// is obtained by dividing the beginning of the block memory by 8M.  We
// use an array of those indices to store the pointers to the beginnings
// of the blocks
#if MEM_MANAGER_64
#define MAX_NUM_BLOCKS ((8*4*K)/(BLOCK_SIZE/M))
#else
#define MAX_NUM_BLOCKS ((4*K)/(BLOCK_SIZE/M))
#endif
typedef UInt16 BlockID;

#define CHUNK_PTR_MASK_32 ((unsigned long) 0xFFFF0000)
#define CHUNK_PTR_MASK_64  0xFFFFFFFFFFFF0000
#define MEGA_BLOCK_MASK 0xFFFFFFFF00000000
#define MEGA_BLOCK_SHIFT 32
#define BLOCK_MASK 0x00000000FFFFFFFF
// These are also done scaled down by a K.  We can't do a meg because
// the chunk size is smaller than that.
#define BLOCKS_PER_MEGABLOCK ((4*M)/(BLOCK_SIZE/K))
#define CHUNKS_PER_MEGABLOCK ((4*M)/(CHUNK_SIZE/K))
typedef UInt16 MegaBlockID;

// There are 8M/64k = 128 chunks in each block.  One is always wasted
// for alignment and malloc overhead, so we can actually use a maximum
// of 127 chunks in the block.
//
// In 32-bit mode, we should always get the full 127, but that's not
// always true in 64-bit mode.  When a block would span multiple
// megablocks, only chunks that would fit in the first one are used.
#define MAX_CHUNKS_PER_BLOCK ((BLOCK_SIZE / CHUNK_SIZE) - 1)

void MemReportOutOfMemory();

// Forward declarations of block and chunk management functions.  The
// implementations are different for 32-bit vs. 64-bit.
struct ChunkCell;
struct MemPool;

static ChunkID sChunkToIndex(void* ptr);
static ChunkCell* sIndexToChunk(ChunkID index);
static ChunkCell* sPtrToChunk(void* ptr);
static BlockID sBlockId(char* block);
static char* sLastChunk(char* block, size_t size);

#ifndef __STRING
//! Stringify expression
#define __STRING(exp) #exp
#endif

#define MEM_ASSERT(exp) \
  if (!(exp)) { \
    sPrintMemAssertHeader(); \
    fprintf(stderr, "%s:%d MEM_ASSERT(%s) failed\n", \
            __FILE__, __LINE__, __STRING(exp)); \
    abort(); \
  }

static void sPrintMemAssertHeader() {
  // When printing a memory assertion, be sure not to use
  // any memory, or you will die printing!
  fprintf(stdout, "****INTERNAL ERROR*****\n");
  fprintf(stdout, "Memory Subsystem corrupt.  Please check for:\n");
  fprintf(stdout, "\n");
  fprintf(stdout, " 1. memory that is used after being freed\n");
  fprintf(stdout, " 2. memory that is freed twice\n");
  fprintf(stdout, " 3. memory that was never allocated being freed\n");
  fprintf(stdout, " 4. memory allocated with malloc/calloc/realloc and freed with delete\n");
  fprintf(stdout, " 5. memory allocated with new and freed with free\n");
  fprintf(stdout, " 6. general memory stomps\n");
  fprintf(stdout, "\n");
  fprintf(stdout, " Tools such as purify (an IBM product), or valgrind (an open-source memory\n");
  fprintf(stdout, "debugging tool) can help find these issues quickly.\n");
}

// When all the cells in a chunk are freed, the chunk becomes available
// for re-use.  But to actually free the chunk, we must remove all the
// chunk's cells from the free-list.  That can be time-consuming because
// the freelist may otherwise be quite large.  So 

// We will keep free-lists for selected object sizes from 4:65536.
// As the sizes go up, we increase the courseness, until we get
// to 1k, where we only allocate in powers of 2.  We could maybe
// do that even earlier.  We also accommodate sizes 772, 1556,
// 3076, 6172, and 12316, because those are used by the SGI hash
// tables.  After 12316 SGI-hash alloocates 24604, but there is
// no benefit to alllocating that size rather than 32768, because
// either way we can only fit 2 in a 64k chunk.
//
// To accommodate 772 we really may as well handle 776 because
// either way we fit 84 of them in 64k
//
// Likeways, we accommodate 1556 with 1560, 3076 with 3120,
// 6172  with 6550, and 12316 with 13104
//
// Note that this routine is slow, but we'll make a table during initialization
// so we don't have to call it much
static size_t sFreeListIndex(size_t sz)
{
  MEM_ASSERT((sz & 3) == 0);
  if (sz <= 32)
    return (sz >> 2);                     // 32 --> index 8
  if (sz <= 64)
    return 8 + ((sz  +  7 - 32) >> 3);    // 64 --> index 13
  if (sz <= 128)
    return 12 + ((sz + 15 - 64) >> 4);    // 128 --> index 16
  if (sz <= 256)
    return 16 + ((sz + 31 - 128) >> 5);   // 256 --> index 20
  if (sz <= 512)
    return 20 + ((sz + 63 - 256) >> 6);   // 512 --> index 24

  // above 1k we just allocate powers of 2, plus the SGI hash sizes
  if (sz <= 776)                // SGI hash allocates 772
    return 25;
  if (sz <= 1024)
    return 26;
  if (sz <= 1560)               // SGI hash allocates 1556
    return 27;
  if (sz <= 2048)
    return 28;
  if (sz <= 3120)               // SGI hash allocates 3076
    return 29;
  if (sz <= 4096)
    return 30;
  if (sz <= 6552)               // SGI hash allocates 6172
    return 31;
  if (sz <= 8192)
    return 32;
  if (sz <= 13104)              // SGI hash allocates 12316
    return 33;
  if (sz <= 16384)
    return 34;
  if (sz <= 32768)
    return 35;
  if (sz <= 65536)
    return 36;
  
  return 0;
} // static size_t sFreeListIndex}

static size_t sFreeListIndexTable[1 + (CHUNK_SIZE >> 2)];
static void sInitFreelistIndexTable() {
  for (SInt32 i = 0; i <= CHUNK_SIZE; i += 4)
    sFreeListIndexTable[i >> 2] = sFreeListIndex(i);
}

static size_t sFastFreeListIndex(size_t size) {
  if (size > CHUNK_SIZE)
    return 0;
  return sFreeListIndexTable[(size) >> 2];
}

#define MAX_INDEX 36

static const size_t sIndexToSize[MAX_INDEX + 1] =
{
  0,
  4, 8, 12, 16,                 // skip by 4
  20, 24, 28, 32,               //     "
  40, 48, 56, 64,               // skip by 8
  80, 96, 112, 128,             // skip by 16
  160, 192, 224, 256,           // skip by 32
  320, 384, 448, 512,           // skip by 64
  776, 1024, 1560, 2048,        // powers of 2 + SGI sizes
  3120, 4096, 6552, 8192,
  13104, 16384, 32768, 65536
};

// Keep track of available free chunks, overlaying the chunk memory
struct ChunkCell {
  ChunkCell* next;
};

struct AllocCell {
  AllocCell* next;
#ifdef CDB
  UInt32 magic;       // only used for sizes >= 8 (32-bit) or 12 (64-bit)
#endif
};

// When we first assign a 64k chunk to a freelist index,
// the simple thing is to chop up the chunk into cells
// of the rounded size, and put them all into the freelist.
// When all the cells in a chunk are freed, then we'd have
// to remove them from the freelist before returning the
// chunk into the free chunks list.
//
// That can result in an unfortunate thrashing effect if
// have a program that just does this:
//   for (int i = 0; i < 10000; ++i) {
//     char* p = new char[4];
//     delete [] p;
//   }
// Sitting on the threshold of having a chunk allocated
// for 4-byte items, this results in 64k/4 = 16k enqueue &
// dequeue operations for each iteartion.  Pretty nasty.
// 
// Instead, what we'll do when we allocate a chunk in the
// first place, is to keep a pointer into the chunk, rather
// than putting it into the next free list.  We need to
// keep one of these chunk contexts for each of the 35
// free-lists, so the cost is not very large.

struct MemChunkContext
{
  AllocCell* mNextCell;
  AllocCell* mLastCell;
  size_t     mCellsAlloced;
  size_t     mNumReclaimableChunks; // zeroed by memset in MemPool::init()
  ChunkCell* mChunk;
  ChunkID    mChunkIndex;

  void setNewChunk(ChunkCell* chunk, size_t size, ChunkID chunk_index)
  {
    mChunk = chunk;
    mNextCell = (AllocCell*) chunk;
    mChunkIndex = chunk_index;
    size_t numCells = CHUNK_SIZE / size; // some waste at end
    mLastCell = (AllocCell*) (((char*) mNextCell) + (numCells*size));
    mCellsAlloced = 0;
  }
  AllocCell* getCell(size_t roundedSize)
  {
    AllocCell* ret = mNextCell;
    mNextCell = (AllocCell*) (((char*) mNextCell) + roundedSize);
    ++mCellsAlloced;
    return ret;
  }
  bool atEnd() {
    return mNextCell == mLastCell;
  }
  size_t numCellsAlloced() {
    return mCellsAlloced;
  }
  void clear()
  {
    mNextCell = 0;
    mLastCell = 0;
    mCellsAlloced = 0;
  }

  bool ownsPointer(void* ptr)
  {
    return ((mNextCell <= ptr) && (ptr < mLastCell));
  }

  size_t numBytesTrapped()
  {
    return ((char*) mLastCell) - ((char*) mNextCell);
  }

  size_t getChunkIndex() { return mChunk == NULL ? 0 : mChunkIndex; }
}; // struct MemChunkContext

//! Context information for a pool of memory.  There is one pool per thread
struct MemPool
{
  // Note that per-size information is based by size index
  
  //! sub-structure tracking state of the current chunk for a size 
  MemChunkContext mChunkContexts[MAX_INDEX + 1];

  //! Free list of cells for a size
  AllocCell* mFreeList[MAX_INDEX + 1];

  //! Holding area for cells moved off the free list because they belong
  //! to a reclaimable chunk
  AllocCell* mReclaimableCells[MAX_INDEX + 1];

  //! Keep track of which blocks are used in this pool
  char* mAllBlocks[MAX_NUM_BLOCKS];

#if MEM_MANAGER_64
  //! Number of chunks in each block
  /*!
    Because all the chunks in a block need to be in the same
    megablock, we won't have a constant number of chunks when the
    block spans a 4G alignment boundary.
  */
  UInt8 mNumChunksInBlock[MAX_NUM_BLOCKS];
#endif

  //! How many allocations have been done in this chunk?  
  static UInt16 smChunkAllocs[NUM_CHUNKS];

  //! What is the size of this chunk?  Note -- we could use a UInt8 array
  //! instead for this if we stored the index rather than the size.  Note
  //! that the size requires 17 bits (65536 is a legal size) but we right
  //! shift by 2.  The high order bit is used to indicate whether this
  //! chunk is active, or free/reclaimable.  We don't have a way to
  //! distinguish between free vs reclaimable based on this information,
  //! but we don't need it
  static UInt16 smChunkSizes[NUM_CHUNKS];

  //! How many bytes have been allocated in this pool?
  size_t mBytesAllocated;

  //! What is the maximum number of bytes ever allocated in this pool?
  size_t mMaxBytesAllocated;

  void addAllocation(size_t size) {
    mBytesAllocated += size;
    if (mBytesAllocated > mMaxBytesAllocated)
      mMaxBytesAllocated = mBytesAllocated;
  }

  //! How many chunks controlled by this pool are reclaimable?  A chunk
  //! is reclaimable when its allocation count goes to 0.
  SInt32 mTotalReclaimableChunks;

  //! Next pool in the linked list of pools.
  MemPool* mNextPool;

  //! Guard variable for fast assertion for thread safety
  SInt32 mActive;

  //! Explicit init method, no static constructors!
  void init()
  {
    memset(this, 0, sizeof(MemPool));
  }

  //! Free list of chunks
  ChunkCell* mFreeChunks;

  // We also need to understand the object size assigned to
  // each chunk.  This also maxes out at 64k and therefore is represented
  // in 2 bytes, costing us another fixed 128k.  To allow us to store
  // 64k we right-shift by 2.  This leaves the high order bit unused,
  // but we're going to use it to mark a chunk that's reclaimable or free,
  // without corrupting its size.
  void setChunkSize(ChunkID chunk_id, size_t size)
  {
    smChunkSizes[chunk_id] = ((size) >> 2);
  }

  //! Get the chunk size of a chunk given the chunk's ID
  static size_t getChunkSize(ChunkID chunk_id) {
    return ((smChunkSizes[chunk_id] << 2) & 0x1FFFF);
  }

  //! Set a chunk's 'free/reclaimed' bit
  void setChunkFree(ChunkID chunk_id) {
    smChunkSizes[chunk_id] |= 0x8000;
  }

  //! Test a chunk's 'free/reclaimed' bit
  bool isChunkFree(ChunkID chunk_id) {
    return ((smChunkSizes[chunk_id] & 0x8000) != 0);
  }

  //! insert a cell in the free list
  inline void freeListInsert(size_t index, AllocCell* cell) {
    AllocCell** freeHead = &mFreeList[index];
    cell->next = *freeHead;
    *freeHead = cell;

#ifdef CDB
    ChunkID chunkIndex = sChunkToIndex(cell);
    size_t chunkSize = getChunkSize(chunkIndex);
    size_t rounded_size = sIndexToSize[index];
    MEM_ASSERT(rounded_size == chunkSize);
#endif
  }

  //! insert a cell in the reclaim list
  inline void reclaimListInsert(size_t index, AllocCell* cell) {
    AllocCell** freeHead = &mReclaimableCells[index];
    cell->next = *freeHead;
    *freeHead = cell;
  }

  void freeReclaimableChunks(size_t sizeIndex);
  void reclaimCellsFromFreeList(size_t index);
  void collectReclaimedCells(size_t index);

  bool freeNextReclaimableChunk();
  bool freeAllChunks();
  BlockID findBlock(ChunkCell* chunk);
  void newBlock();
  void releaseBlocks();
  ChunkCell* newChunk();
  size_t getBytesTrapped();
};

UInt16 MemPool::smChunkAllocs[NUM_CHUNKS];
UInt16 MemPool::smChunkSizes[NUM_CHUNKS];

// Linux doesn't appear to enforce memory limit restrictions,
// so do it ourselves.  This makes it easier to write tests
// that catch infinite-recursion-with-alloc issues, such as
// bug1848, by setting a resource limit.  Linux should really
// do this for us.
static UInt32 sMemLimit = 0;
static void sFindMemLimit()
{
#if pfLINUX || pfLINUX64
  struct rlimit rlim;
  getrlimit(RLIMIT_DATA, &rlim);
  if ((rlim.rlim_cur != rlim.rlim_max) && (rlim.rlim_cur != RLIM_INFINITY))
    sMemLimit = rlim.rlim_cur;
#endif
}

// Structure allocated on the stack during memory allocation and 
// freeing to ensure that no one frees memory from a thread's pool
// while that thread is mutating the pool.
class MemSysActive
{
public: // no CARBONMEM_OVERRIDES
  MemSysActive(MemPool* memPool)
  {
    MEM_ASSERT(memPool->mActive == 0);
    ++memPool->mActive;
    mMemPool = memPool;
  }
  ~MemSysActive()
  {
    --mMemPool->mActive;
    MEM_ASSERT(mMemPool->mActive == 0);
  }
private:
  MemPool* mMemPool;
};

static MemPool* sChunkPools[NUM_CHUNKS];

static pthread_key_t sThreadKey = 0;
static pthread_mutex_t sThreadMutex = PTHREAD_MUTEX_INITIALIZER;

class PthreadLock
{
public: // no CARBONMEM_OVERRIDES
  PthreadLock()
  {
    pthread_mutex_lock(&sThreadMutex);
  }
  ~PthreadLock()
  {
    pthread_mutex_unlock(&sThreadMutex);
  }
};

static MemPool* sFirstPool = NULL;

class PthreadIter
{
public: // no CARBONMEM_OVERRIDES
  PthreadIter()
  {
    mPool = sFirstPool;
  }
  ~PthreadIter()
  {
  }

  MemPool* operator*() {return mPool;}
  bool atEnd() const {return mPool == NULL;}
  void operator++() {mPool = mPool->mNextPool;}

private:
  MemPool* mPool;
  PthreadLock mLock;
};

static void sInitKey()
{
  if (pthread_key_create(&sThreadKey, 0))
  {
    perror("pthread_key");
    exit(1);
  }
  if (pthread_mutex_init(&sThreadMutex, NULL))
  {
    perror("pthread_mutex_init");
    exit(1);
  }
}


static MemPool* sGetPool()
{
  if (sThreadKey == 0)
  {
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    pthread_once(&once, sInitKey);
  }

  MemPool* memPool = static_cast<MemPool*>(pthread_getspecific(sThreadKey));
  if (memPool == NULL)
  {
    PthreadLock lock;
    memPool = (MemPool*) malloc(sizeof(MemPool));
    MEM_ASSERT(memPool);
    memPool->init();
    memPool->mNextPool = sFirstPool;
    sFirstPool = memPool;
    pthread_setspecific(sThreadKey, memPool);
  }
  return memPool;
}

void MemSystemInit()
{
  sInitFreelistIndexTable();
  sFindMemLimit();
}

// #define LOG_MEM_OPS
#ifdef LOG_MEM_OPS
#include <stdio.h>
#define LOG_MALLOC(size, ptr) fprintf(stderr, "CarbonMalloc %d " FormatPtr "\n", size, ptr)
#define LOG_FREE(ptr, size) fprintf(stderr, "CarbonFree " FormatPtr " %d\n", ptr, size)
#define LOG_RELEASE fprintf(stderr, "CarbonMem::releaseBlocks\n")
#else
#define LOG_MALLOC(size, ptr)
#define LOG_FREE(ptr, size) 
#define LOG_RELEASE
#endif

#ifdef CDB
# define OVERHEAD_FOR_SIZE 8
#else
# define OVERHEAD_FOR_SIZE 0
#endif

// When debugging, it is useful to be able to inquire how a pointer
// was allocated.  For memory allocations <= 64k, our system supports
// this at no cost.  But for large allocations we need to bloat them
// to support this debugging feature.   We pump them up by 8 bytes,
// and use ptr[-2] to store the size, and ptr[-1] to store a magic
// pattern 0xfeedface to use for redundancy checking
#define MAGIC_PATTERN 0xfeedface
static void* sBuiltinAlloc(size_t numBytes, int needOverhead)
{
  PthreadLock lock;
  MEM_ASSERT((numBytes & 3) == 0);

  void* ptr = ::malloc(numBytes + (needOverhead? OVERHEAD_FOR_SIZE: 0));
  if (ptr == 0)
    return 0;

#ifdef CDB
  if (needOverhead)
  {
    UInt32* pl = (UInt32*) ptr;
    pl[0] = numBytes;
    pl[1] = MAGIC_PATTERN;
    ptr = (void*) (((char*) ptr) + OVERHEAD_FOR_SIZE);
  }
#endif
  return ptr;
}

static size_t sBuiltinGetSize(void* ptr, void** realPtr)
{
#ifdef CDB
  *realPtr = (void*) (((char*) ptr) - OVERHEAD_FOR_SIZE);
  UInt32* pl = (UInt32*) *realPtr;
  MEM_ASSERT(pl[1] == MAGIC_PATTERN);
  return pl[0];
#else
  *realPtr = ptr;
  return 0;
#endif
}

static void sBuiltinFree(void* ptr)
{
  if (ptr != NULL)
  {
    PthreadLock lock;
#ifdef CDB
    size_t size = sBuiltinGetSize(ptr, &ptr);
    LOG_FREE(ptr, size);
    if (RECORD_BYTES())
    {
      MemPool* memPool = sGetPool();
      memPool->mBytesAllocated -= size;
    }
#endif
    free(ptr);
  }
}

void MemReportOutOfMemory()
{
  size_t size = BLOCK_SIZE - MALLOC_OVERHEAD;
  CarbonMem::printStats();
  fprintf(stderr, "\n\nMemory exhausted on request for %lu bytes\n",
          (unsigned long) size);
  if (gMemHistogram != NULL)
    sMemLimit += gMemHistogram->outOfMemory();
  exit(1);
}

// Get a new 8-megabyte block via malloc, and slice it into 64k chunks
void MemPool::newBlock() {
  PthreadLock lock;
  size_t size = BLOCK_SIZE - MALLOC_OVERHEAD;
  char* block = NULL;

  if ((sMemLimit == 0) || (mBytesAllocated < sMemLimit))
  {
    block = (char*) ::malloc(size);
  }

  // It is very inconvenient to run out of memory in the course
  // of flushing the memory histogram database, because memory
  // histogramming is our mechanism to debug the cause of
  // out-of-memory conditions.  So if block is NULL and we
  // were in the middle of a histogram flush, then tell the
  // histogram to release its reserve buffer so we can unwind.
  if ((block == NULL) && (gMemHistogram != NULL) && gMemHistogram->isActive())
  {
    sMemLimit += gMemHistogram->outOfMemory();
    block = (char*) ::malloc(size);
  }

  if (block == NULL)
    MemReportOutOfMemory();

  BlockID blockId = sBlockId(block);
  mAllBlocks[blockId] = block;

  // Divvy up the 8 meg into 64k chunks.  Go from high to low because
  // we will be pulling the data out LIFO, so we get increasing
  // addresses, making debugging more predictable.  Our chunks must
  // be aligned on 64k byte boundaries so we can look at just the
  // high order bits.  
  char *bptr = sLastChunk(block, size);
  UInt8 numChunks = 0;
  for (; bptr >= block; bptr -= CHUNK_SIZE)
  {
    ChunkCell* cell = (ChunkCell*) bptr;
    cell->next = mFreeChunks;
    ChunkID chunkIndex = sChunkToIndex(cell);
    smChunkAllocs[chunkIndex] = 0;
    smChunkSizes[chunkIndex] = 0;
    mFreeChunks = cell;
    MEM_ASSERT(sChunkPools[chunkIndex] == NULL);
    sChunkPools[chunkIndex] = this;
    ++numChunks;
  }

#if MEM_MANAGER_64
  // Save the number of chunks
  MEM_ASSERT(numChunks <= MAX_CHUNKS_PER_BLOCK);
  mNumChunksInBlock[blockId] = numChunks;
#else
  // We should always get the maximum in 32-bit mode
  MEM_ASSERT(numChunks == MAX_CHUNKS_PER_BLOCK);
#endif
}

// Free a reclaimable chunk, and all the other chunk that need to be
// reclaimed that are associated with the size of the most
// recently reclaimable block.
void MemPool::freeReclaimableChunks(size_t index) {
  reclaimCellsFromFreeList(index);
  collectReclaimedCells(index);
}

void MemPool::reclaimCellsFromFreeList(size_t index) {
  size_t rounded_size = sIndexToSize[index];

  // Walk through the free-list for this size, and move all the
  // reclaimable cells on the freeList to the reclaimableCells list
  AllocCell** freeHead = &mFreeList[index];
  AllocCell* prev = NULL;
  for (AllocCell* p = *freeHead, *next; p != NULL; p = next)
  {
    ChunkID ci = sChunkToIndex(p);
    size_t csize = getChunkSize(ci);
    MEM_ASSERT(rounded_size == csize);

    // Copy out the 'next' pointer because if we move the cell onto
    // the reclaim list we will be overwriting it.
    next = p->next;

    if (isChunkFree(ci)) {
      reclaimListInsert(index, p);

      // splice out of free list
      if (prev == NULL)
        *freeHead = next;
      else
        prev->next = next;
    }
    else
      prev = p;
  }
} // void MemPool::reclaimCellsFromFreeList

void MemPool::collectReclaimedCells(size_t index) {
  MemChunkContext& mcc = mChunkContexts[index];
  size_t rounded_size = sIndexToSize[index];
  size_t reclaimedCells = 0;
  
  // Now we can reclaim all the reclaimableCells.  Note that we will
  // hit cells for the same chunk numerous times, but we only insert
  // the cell's chunk in the free chunk-list for the first cell in the
  // chunk.  That one is guaranteed to be on the reclaimCells list,
  // not left in the unallocated pool accounted for in
  // MemChunkContext.  That decision is not arbitrary.  When we put
  // the chunk in the free-chunk list, we must mutate its first word
  // to point to the next free chunk.  That word also serves as the
  // cell's next-pointer for the free-list, all hell breaks loose if
  // we fix a chunk's next-pointer when that chunk's first cell is
  // still downstream in the free-list.
  MEM_ASSERT(mReclaimableCells[index] != NULL);
  UInt32 numReclaimedChunks = 0;
  // This isn't exactly correct, due to alignment and malloc overhead,
  // but it doesn't matter.  It's just a theoretical maximum.  It's
  // used to ensure that number of cells actually reclaimed plus the
  // theoretical number of unallocated cells equals the theoretical
  // number of cells in the chunk.
  UInt32 cellsPerChunk = CHUNK_SIZE / rounded_size;
  for (AllocCell* p = mReclaimableCells[index]; p != NULL; ) {
    UInt32 chunkIndex = sChunkToIndex(p);
    size_t csize = getChunkSize(chunkIndex);
    MEM_ASSERT(rounded_size == csize);
    ChunkCell* chunk = sPtrToChunk(p);
    bool firstCellInChunk = (((AllocCell*) chunk) == p);
    p = p->next;

    MEM_ASSERT(isChunkFree(chunkIndex));
    if (firstCellInChunk) {
      chunk->next = mFreeChunks;
      mFreeChunks = chunk;
      ++numReclaimedChunks;
      if (mcc.getChunkIndex() == chunkIndex) {
        MEM_ASSERT(cellsPerChunk >= mcc.mCellsAlloced);
        size_t cellsInChunkNeverAlloced = cellsPerChunk - mcc.mCellsAlloced;
        reclaimedCells += cellsInChunkNeverAlloced;
        mcc.clear();
      }
    }
    ++reclaimedCells;
  } // for
  mReclaimableCells[index] = NULL; // clear out the list
    
  MEM_ASSERT((cellsPerChunk * numReclaimedChunks) == reclaimedCells);
  MEM_ASSERT(numReclaimedChunks == mcc.mNumReclaimableChunks);
  mcc.mNumReclaimableChunks = 0;
  mTotalReclaimableChunks -= numReclaimedChunks;
  MEM_ASSERT(mTotalReclaimableChunks >= 0);
} // void MemPool::freeReclaimableChunks

// Find some chunk to reclaim for re-use with another size.  After
// we find one size-index that's got reclaimable chunks, we can
// reclaim all the ones at that size and quit.
bool MemPool::freeNextReclaimableChunk() {
  if (mTotalReclaimableChunks != 0) {
    for (size_t index = 0; index <= MAX_INDEX; ++index) {
      MemChunkContext& mcc = mChunkContexts[index];
      if (mcc.mNumReclaimableChunks != 0) {
        freeReclaimableChunks(index);
        return true;
      }
    }
    MEM_ASSERT("freed something" == NULL);
  }
  return false;
}

// Reclaim some or all reclaimable blocks
bool MemPool::freeAllChunks() {
  if (mTotalReclaimableChunks != 0) {
    for (size_t index = 0; index <= MAX_INDEX; ++index) {
      MemChunkContext& mcc = mChunkContexts[index];
      if (mcc.mNumReclaimableChunks != 0) {
        freeReclaimableChunks(index);
      }
    }
    MEM_ASSERT(mTotalReclaimableChunks == 0);
    return true;
  }
  return false;
}
  
BlockID MemPool::findBlock(ChunkCell* chunk)
{
  char* chunkPtr = (char*) chunk;

  // Blocks are not aligned on 8*M multiples so we have to look at
  // the block addresses to decide whether the cell is in this one
  // or the previous one.
  BlockID blockId = sBlockId(chunkPtr);
  char* block = mAllBlocks[blockId];
  char* block2 = (blockId >= 1)? mAllBlocks[blockId - 1]: 0;
  if ((block == NULL) || ((block2 != NULL) && (chunkPtr < block)))
  {
    MEM_ASSERT(block2);
    --blockId;
    block = block2;
  }
  MEM_ASSERT(chunkPtr >= block);
  MEM_ASSERT(chunkPtr < block + BLOCK_SIZE);
  return blockId;
}

void CarbonMem::releaseBlocks()
{
  LOG_RELEASE;
  sGetPool()->releaseBlocks();
/*
  for (PthreadIter p; !p.atEnd(); ++p)
    (*p)->releaseBlocks();
*/
}

void MemPool::releaseBlocks()
{
  if (gMemSystemUseMalloc)
    return;
  freeAllChunks();

  // Walk through all the free chunks and figure out which blocks can
  // be reclaimed.  We can free a block if all possible chunks in the
  // block are found in the free chunks list.

  // Now walk through all the free chunks and find which blocks
  // they correspond to, incrementing the freeChunksInBlock count
  int freeChunksInBlock[MAX_NUM_BLOCKS];
  memset(freeChunksInBlock, 0, sizeof(freeChunksInBlock));
  for (ChunkCell* c = mFreeChunks; c != NULL; c = c->next)
  {
    BlockID blockId = findBlock(c);
    ++freeChunksInBlock[blockId];
  }

  // Walk through all the blocks and zero out the freeChunks count
  // if it's not completely free.
  int numReleasableBlocks = 0;
  int chunksThisBlock = MAX_CHUNKS_PER_BLOCK;
  for (int i = 0; i < MAX_NUM_BLOCKS; ++i)
  {
#if MEM_MANAGER_64
    // Get the actual number of chunks in this block.
    chunksThisBlock = mNumChunksInBlock[i];
#endif

    // If no chunks have been allocated from this block, or not all
    // the allocated blocks are free, we can't release the block.
    if ((chunksThisBlock == 0) || (freeChunksInBlock[i] != chunksThisBlock)) // 127
      freeChunksInBlock[i] = 0;
    else
    {
      ++freeChunksInBlock[i];   // bump this to 128
      ++numReleasableBlocks;
    }
  }

  // walk through the free chunks one more time, and if it belongs
  // to a block that's got freeable chunks, then remove the chunk
  // from the chunk-free-list -- we are going to free the whole
  // block.
  if (numReleasableBlocks != 0)
  {
    ChunkCell* prev = NULL;
    for (ChunkCell* c = mFreeChunks; c != NULL; c = c->next)
    {
      BlockID blockId = findBlock(c);
      if (freeChunksInBlock[blockId] != 0)
      {
        if (prev == NULL)
          mFreeChunks = c->next;
        else
          prev->next = c->next;
        --freeChunksInBlock[blockId];
        MEM_ASSERT(freeChunksInBlock[blockId] != 0); // should be 1 left at en)d
        ChunkID chunkIndex = sChunkToIndex(c);
        smChunkAllocs[chunkIndex] = 0;
        smChunkSizes[chunkIndex] = 0;
      }
      else
        prev = c;
    }

    // Finally, rip through the block-array and return the block
    // memory to the system.
    for (int i = 0; i < MAX_NUM_BLOCKS; ++i)
    {
      if (freeChunksInBlock[i] == 1) // because we bumped to 128 earlier
      {
        char* block = mAllBlocks[i];

        // clear the MemPool pointer for all the chunks in the block
        size_t size = BLOCK_SIZE - MALLOC_OVERHEAD;
        char* bptr = sLastChunk(block, size);
        for (; bptr >= block; bptr -= CHUNK_SIZE)
        {
          ChunkCell* cell = (ChunkCell*) bptr;
          ChunkID chunkIndex = sChunkToIndex(cell);
          sChunkPools[chunkIndex] = NULL;
        }

        MEM_ASSERT(block);
        ::free(block);
        mAllBlocks[i] = NULL;
        --numReleasableBlocks;
      }
      else
        MEM_ASSERT(freeChunksInBlock[i] == 0);
    }
    MEM_ASSERT(numReleasableBlocks == 0);
  } // if
} // void MemPool::releaseBlocks

// Find a new 64k chunk, allocating a new block if needed
ChunkCell* MemPool::newChunk() {
  if ((mFreeChunks == NULL) && !freeNextReclaimableChunk())
    newBlock();
  ChunkCell* chunk = mFreeChunks;
  mFreeChunks = chunk->next;
  return chunk;
}

// Allocate memory, in order of preference, from:
//   1. the free list for the desired rounded size
//   2. grab a free 64k chunk from our list of free chunks,
//      and assign it to our desired size, go to step 1
//   3. grab a new 8M block and divide it into 64k chunks,
//      go to step 2
//   4. directly using malloc/free -- only needed for allocs >64k
extern "C" void* CarbonMalloc(size_t size, int needOverhead)
{
#ifdef __COVERITY__
  return malloc(size);
#else
  MemPool* memPool = sGetPool();
  int index = sFastFreeListIndex(size);

  MemSysActive msa(memPool);

  if (index != 0)		// size <= 64K (CHUNK_SIZE)
  {
    size_t rounded_size = sIndexToSize[index];
    if (RECORD_BYTES())
    {
      memPool->addAllocation(rounded_size);
    }
    AllocCell* ptr = NULL;
    ChunkID chunkIndex = 0;
    while ((ptr == NULL) &&
           ((ptr = (AllocCell*) memPool->mFreeList[index]) != NULL))
    {
#ifdef CDB
      if (size >= MIN_SIZE_FOR_MAGIC_PATTERN)
        MEM_ASSERT(ptr->magic == MAGIC_PATTERN);
#endif
      memPool->mFreeList[index] = ptr->next;
      chunkIndex = sChunkToIndex(ptr);
      size_t chunkSize = memPool->getChunkSize(chunkIndex);
      MEM_ASSERT(chunkSize == rounded_size);
      if (memPool->isChunkFree(chunkIndex))
      {
        // this is part of a reclaimable chunk.  move it
        // out of the free list and into the reclaimable list
        memPool->reclaimListInsert(index, ptr);
        ptr = NULL;
      }
    } 
    if (ptr == NULL)		// Nothing usable from free list
    {
      MemChunkContext& mcc = memPool->mChunkContexts[index];
      chunkIndex = mcc.getChunkIndex();

      // When we allocate a chunk, we don't immediately carve it up into cells
      // and put them all on the free list, because doing so would cause slow
      // behavior with the pattern of repeated free(malloc(k)).  So in mcc
      // we keep track of how many cells have been allocated out of the current
      // chunk.  If it gets to 0 that means we know this chunk is reclaimable,
      // so it's moderately faster to reclaim it immediately than to search
      // for reclaimable chunks across every sizeIndex.  This is especially true
      // since we know from getting here that the free-list is empty (ptr==NULL).
      // All its cells were moved to the reclaimable cells list, and so the reclaim
      // process will be very fast.
      if (!mcc.atEnd() && memPool->isChunkFree(chunkIndex)) {
        memPool->freeReclaimableChunks(index);
      }

      if (mcc.atEnd())
      {
        ChunkCell* chunk = memPool->newChunk();
        // Carbon will not handle out-of-memory, but
        // this policy will be enforced in MemManager.cxx.a
        if (chunk == NULL)
          return NULL;
        chunkIndex = sChunkToIndex(chunk);
        memPool->setChunkSize(chunkIndex, rounded_size);
        mcc.setNewChunk(chunk, rounded_size, chunkIndex);
      }
      ptr = mcc.getCell(rounded_size);
      MEM_ASSERT(chunkIndex == sChunkToIndex(ptr));
    }
    ++memPool->smChunkAllocs[chunkIndex];
    LOG_MALLOC(size, ptr);
    return (void*) ptr;
  } // if

  // Otherwise the size is too big.  Use builtin malloc.
  if (RECORD_BYTES())
  {
    // If we are compiled for product mode, then we will not
    // be able to decrement the memory allocated when we delete
    // a pointer if we are relying on the overhead figure.
#ifndef CDB
    if (!needOverhead)
#endif
    {
      memPool->addAllocation(size);
    }
  }
  void* ptr = sBuiltinAlloc(size, needOverhead);
  LOG_MALLOC(size, ptr);
  return ptr;
#endif
} // extern "C" void* CarbonMalloc

void MemCheckFreeList(int index)
{
  //for (PthreadIter pools; !pools.atEnd(); ++pools)
  {
    MemPool* pool = sGetPool(); //*pools;
    for (AllocCell* p = pool->mFreeList[index]; p != NULL; p = p->next)
    {
      ChunkID chunkIndex = sChunkToIndex(p);
      size_t chunkSize = pool->getChunkSize(chunkIndex);
      size_t rounded_size = sIndexToSize[index];
      MEM_ASSERT(rounded_size == chunkSize);
    }
  }
}

static inline MemPool* sAnalyzeBlock(void* ptr, size_t* size, ChunkID* cidx) {
  // Figure out the size based on what chunk this object is in
  ChunkID chunkIndex = sChunkToIndex(ptr);

  MemPool* memPool = sChunkPools[chunkIndex];
  if (memPool == NULL)
    memPool = sGetPool();       // must be a direct malloc/free
  MemSysActive msa(memPool);

  *size = memPool->getChunkSize(chunkIndex);
  *cidx = chunkIndex;
  return memPool;
} // static inline MemPool* analyzeBlock


extern "C" void CarbonFree(void* ptr, size_t sz)
{
#ifdef __COVERITY__
  free(ptr);
#else
  size_t size;
  ChunkID chunkIndex;
  MemPool* memPool = sAnalyzeBlock(ptr, &size, &chunkIndex);

  if (size == 0)
  {
    if (sz == 0)
      sBuiltinFree(ptr);        // figure out size based on 8 overhead bytes
    else
    {
      LOG_FREE(ptr, sz);
      PthreadLock lock;
      free(ptr);                // the size was known, so we did not need
                                // the overhead

      if (RECORD_BYTES())
        memPool->mBytesAllocated -= sz;
    }
  }
  else
  {
    LOG_FREE(ptr, size);
    if (RECORD_BYTES())
      memPool->mBytesAllocated -= size;
    AllocCell* cell = (AllocCell*) ptr;
    size_t index = sFastFreeListIndex(size);
#ifdef CDB
    if (size >= MIN_SIZE_FOR_MAGIC_PATTERN)
    {
      if (cell->magic == MAGIC_PATTERN)
      {
        // if the second word of our memory is set to our magic
        // pattern, then it's possible we are freeing already freed
        // memory.  To find out we have to walk the free list.  But
        // this is debug mode so go do it.
        for (AllocCell* p = memPool->mFreeList[index]; p != NULL; p = p->next)
        {
          bool doubleDestruction = (p == cell);
          MEM_ASSERT(!doubleDestruction);
        }
      }
      cell->magic = MAGIC_PATTERN;
    }
#endif

    memPool->freeListInsert(index, cell);
    if (--memPool->smChunkAllocs[chunkIndex] == 0)
    {
      MEM_ASSERT(!memPool->isChunkFree(chunkIndex));
      memPool->setChunkFree(chunkIndex);
      MemChunkContext& mcc = memPool->mChunkContexts[index];
      ++mcc.mNumReclaimableChunks;
      ++memPool->mTotalReclaimableChunks;
    }
  } // else
#endif
} // extern "C" void CarbonFree

extern "C" size_t CarbonGetSize(void* ptr, void** realPtr, bool hasOverhead);

extern "C" void* CarbonRealloc(void* ptr, size_t old_size, size_t new_size,
                               bool hasOverhead)
{
#ifdef __COVERITY__
  return realloc(ptr, new_size);
#else
  size_t bytes_to_cpy = std::min(old_size, new_size);
  size_t new_index = sFastFreeListIndex(new_size);
  size_t real_old_size;
  ChunkID chunkIndex;
#ifndef CDB
  MemPool* memPool = 
#endif
    sAnalyzeBlock(ptr, &real_old_size, &chunkIndex);

  // If the old size is managed by MemSystem.cxx's chunks (<=64k) then we can 
  // short-circuit the realloc.
  if (real_old_size != 0) {
    // Vrishal made a review suggestion that we should do a consistency
    // check between 'old_size' and 'new_size'.  This is possible, but
    // because we round up to the next chunk size, it has to be a <=
    // check.  Also, note that an error on the user's part of what size
    // memory he thinks he has can only affect our byte-count accounting,
    // and will not cause us to miscategorize memory.
    old_size = real_old_size;
    bytes_to_cpy = std::min(old_size, new_size);

    // If the user used to have 65000 bytes, and now wants to bump it
    // up to 65008 bytes, then we are in the same size-bucket and we
    // can simply return the passed-in ptr.
    size_t old_index = sFastFreeListIndex(old_size);
    if (new_index == old_index) {
      INFO_ASSERT(new_index != 0, "realloc index insanity");
      return ptr;
    }
  }

#ifdef CDB
  else if (hasOverhead) {
    // Get the old size from the system.
    void* realPtr;
    old_size = CarbonGetSize(ptr, &realPtr, true);
    bytes_to_cpy = std::min(old_size, new_size);
  }
#else
  // If the old block was too large for us, and the new block is
  // also too large for us, then we can just let ::realloc handle
  // this, to take advantage of its ability to extend existing blocks
  // Don't do this in CDB-mode, though, because we always add overhead to
  // malloc-blocks in CDB.  Just let CarbonMalloc handle that.

  else if (new_index == 0) {
    ptr = realloc(ptr, new_size);
    if (RECORD_BYTES()) {
      memPool->mBytesAllocated -= old_size;
      if (! hasOverhead) {
        memPool->addAllocation(new_size);
      }
    }
    return ptr;
  }
  else if (hasOverhead) {
    // We don't know how big the old block was, but it was bigger than
    // 64k, because we could not look up its size in the chunk map.  And
    // the new size is <64k, because new_index!=0.  That means we are
    // shrinking the block, so we can memcpy to the new smaller size.
    bytes_to_cpy = new_size;
  }
#endif

  // Otherwise, we will handle the realloc ourselves, possibly moving
  // a previously malloced block back into a <= 64k block
  void* newPtr = CarbonMalloc(new_size, hasOverhead);
  if (newPtr == NULL) {
    fprintf(stderr, "\n\nMemory exhausted on request for %lu bytes, with %lu thus far\n",
            (unsigned long) new_size,
            (unsigned long) CarbonMem::getBytesAllocated());
    std::abort();
  }
  memcpy(newPtr, ptr, bytes_to_cpy);
  CarbonFree(ptr, old_size);
  // CarbonMalloc & CarbonFree will update sizes

  return newPtr;
#endif
} // extern "C" void* CarbonRealloc

// Find the size of an allocated object
extern "C" size_t CarbonGetSize(void* ptr, void** realPtr, bool hasOverhead)
{
  size_t size = 0;

  // Figure out the size based on what chunk this object is in
  ChunkID chunkIndex = sChunkToIndex(ptr);
  size = sGetPool()->getChunkSize(chunkIndex);

  if ((size == 0) && hasOverhead)
    size = sBuiltinGetSize(ptr, realPtr);
  return size;
}

//! Print memory-system information about a pointer.  Don't crash.
void CarbonMem::printPtr(void* ptr)
{
  ChunkID chunkIndex = sChunkToIndex(ptr);
  MemPool* memPool = sGetPool();
  size_t size = memPool->getChunkSize(chunkIndex);
  fprintf(stdout, "Pointer %p", ptr);
  if (size == 0)
  {
#ifdef CDB
    UInt32* pl = (UInt32*) (((char*) ptr) - OVERHEAD_FOR_SIZE);
    if (pl[1] == MAGIC_PATTERN)
      fprintf(stdout, " is allocated via malloc(%ld)\n", (long) *pl);
    else
    {
      fprintf(stdout, " has unknown size.  It may have been allocated\n");
      fprintf(stdout, " behind the back of CarbonMem, or it may be \n");
      fprintf(stdout, " a large chunk allocated by an STL container\n");
    }
#else
    fprintf(stdout, " is either a large chunk or was allocated directly via\n");
    fprintf(stdout, " malloc, but because this is not a debug build, we can't\n");
    fprintf(stdout, " tell.\n");
#endif
  }
  else
  {
    int index = sFastFreeListIndex(size);
    bool isFree = memPool->isChunkFree(chunkIndex);
    bool sizeKnown = false;

    // The chunk may be alive, but this pointer is not yet allocated from it
    if (!isFree)
    {
      MemChunkContext& mcc = memPool->mChunkContexts[index];
      isFree = mcc.ownsPointer(ptr);

      // Also this pointer may be in the freelist
      for (AllocCell* p = memPool->mFreeList[index];
           !isFree && (p != NULL); p = p->next)
      {
        isFree = (p == ptr);
        sizeKnown = true;
      }
    }

    if (isFree)
      fprintf(stdout, " has been freed, and %s %ld, from chunk %ld\n",
              (sizeKnown? "was size ": "may have been size "),
              (long) size, (long) chunkIndex);
    else
      fprintf(stdout, " is allocated at size %ld, from chunk %ld\n",
              (long) size, (long) chunkIndex);
  } // else
} // void CarbonMem::printPtr

//! Print memory-system information about a pointer.  Don't crash.
bool CarbonMem::getPtrInfo(void* ptr, bool* isAllocated, size_t* size)
{
  MemPool* memPool = sGetPool();
  ChunkID chunkIndex = sChunkToIndex(ptr);
  *size = memPool->getChunkSize(chunkIndex);
  bool success = false;
  if (*size == 0)
  {
    UInt32* pl = (UInt32*) (((char*) ptr) - OVERHEAD_FOR_SIZE);
    if (pl[1] == MAGIC_PATTERN)
    {
      *isAllocated = true;
      *size = *pl;
      success = true;
    }
  }
  else
  {
    int index = sFastFreeListIndex(*size);
    bool isFree = memPool->isChunkFree(chunkIndex);
    bool sizeKnown = false;

    // The chunk may be alive, but this pointer is not yet allocated from it
    if (!isFree)
    {
      MemChunkContext& mcc = memPool->mChunkContexts[index];
      isFree = mcc.ownsPointer(ptr);

      // Also this pointer may be in the freelist
      for (AllocCell* p = memPool->mFreeList[index];
           !isFree && (p != NULL); p = p->next)
      {
        isFree = (p == ptr);
        sizeKnown = true;
      }
    }

    *isAllocated = !isFree;
    success = true;
  } // else
  return success;
} // bool CarbonMem::getPtrInfo

extern Stats* gStats;

void CarbonMem::printStats()
{
  fprintf(stdout, "%lu allocated, %lu trapped, %lu highwater, %gM system\n",
          (unsigned long) getBytesAllocated(),
          (unsigned long) getBytesTrapped(),
          (unsigned long) getHighWater(),
          gStats? gStats->getMemAlloced(): 0.0);
}

// Undocumented routine to print memory for every thread.  This routine
// must be called when every thread is quiescent, or VERY BAD THINGS WILL
// HAPPEN!!!!  This is why it is not documented.  The main reason that
// VERY BAD THINGS will happen is that getBytesTrapped() does a GC on that
// thread's memory pool.  We can probably have some gentler summary routines
// that just give the allocated & hiwater.
void MemPrintStatsAllThreads()
{
  int i = 0;
  unsigned long totalAlloced = 0;
  unsigned long totalTrapped = 0;
  unsigned long maxAlloced = 0;

  for (PthreadIter pools; !pools.atEnd(); ++pools, ++i)
  {
    MemPool* pool = *pools;
    totalAlloced += pool->mBytesAllocated;
    totalTrapped += pool->getBytesTrapped();
    maxAlloced += pool->mMaxBytesAllocated;
    fprintf(stdout,
            "Thread %d: %lu allocated, %lu trapped, %lu highwater\n",
            i,
            (unsigned long) pool->mBytesAllocated,
            (unsigned long) pool->getBytesTrapped(),
            (unsigned long) pool->mMaxBytesAllocated);
  }
  fprintf(stdout,
          "TOTAL: %lu allocated, %lu trapped, %lu highwater, %gM system\n",
          totalAlloced,
          totalTrapped,
          maxAlloced,
          gStats? gStats->getMemAlloced(): 0.0);
} // void MemPrintStatsAllThreads

void CarbonMem::printDetail()
{
  // Print out information about all the chunks & all the freelists
  MemPool* memPool = sGetPool();
  for (int i = 0; i < NUM_CHUNKS; ++i)
  {
    size_t size = memPool->getChunkSize(i);
    if (size != 0)
    {
      fprintf(stdout, "Chunk %ld: size %ld, %ld allocs, %s\n",
              (long) i, (long) size, (long) memPool->smChunkAllocs[i],
              (memPool->isChunkFree(i)? " (free)": " (inuse)"));
    }
  }

  for (int i = 1; i <= MAX_INDEX; ++i)
  {
    int count = 0;
    for (AllocCell* p = memPool->mFreeList[i]; p != NULL; p = p->next)
      ++count;
    if (count != 0)
    {
      size_t size = sIndexToSize[i];
      UtOStream& cout = UtIO::cout();
      cout << "Freelist " << i << ": size " << size
           << ", " << count << " cells, "
           << count*size << " bytes"
           << UtIO::endl;
    }
  }
}

#ifdef MEM_DEBUG_ENABLE
void CarbonMem::printAllocations(size_t size)
{
  MemPool* memPool = sGetPool();
  int index = 0;
  if (size != 0)
  {    
    int index = sFastFreeListIndex(size);
    if (index == 0)
    {
      fprintf(stdout,
              "that size is not managed directly by the Carbon allocator\n");
      return;
    }
    size = sIndexToSize[index];
  }

  // Walk through all the chunks, looking for chunks of the given size
  // The index is explicitly a UInt32, because in 32-bit mode ChunkID
  // is a UInt16 and there are 64K chunks, so the comparison would
  // always be true.
  for (UInt32 i = 0; i < NUM_CHUNKS; ++i)
  {
    if (!memPool->isChunkFree(i)
        && (((size == 0U) && (memPool->getChunkSize(i) != 0)) 
            || ((size != 0) && ((size_t) memPool->getChunkSize(i)) == size)))
    {
      size_t sz = memPool->getChunkSize(i);
      if (size == 0)
        index = sFastFreeListIndex(sz);
      fprintf(stdout, "Chunk %ld (size %ld):", (long) i, (long) sz);
      // n^2 find all the pointers in the chunk that are not in the free list
      MemChunkContext mcc;
      ChunkCell* chunk = sIndexToChunk(i);
      mcc.setNewChunk(chunk, sz, i);
      if (memPool->mChunkContexts[index].getChunkIndex() == i)
        mcc.mLastCell = memPool->mChunkContexts[index].mNextCell;

      while (!mcc.atEnd())
      {
        AllocCell* cell = mcc.getCell(sz);

        // We have a cell that's been pulled out of the chunk.  But
        // we need to check if it's in the freelist.  That's potentially
        // n^2.
#ifdef CDB
        if ((sz < MIN_SIZE_FOR_MAGIC_PATTERN)
            || (cell->magic == MAGIC_PATTERN))
#endif
        {
          for (AllocCell* p = memPool->mFreeList[index];
               (p != NULL) && (cell != NULL);
               p = p->next)
          {
            if (p == cell)
              cell = NULL;
          }
        }
        if (cell != NULL)
          fprintf(stdout, " %p", cell);
      }
      
      fprintf(stdout, "\n");
    } // if
  } // for
} // void CarbonMem::printAllocationsOfSize
#endif

size_t CarbonMem::getBytesTrapped()
{
  size_t numTrapped = sGetPool()->getBytesTrapped();
/*
  unsigned long numTrapped = 0;
  for (PthreadIter pools; !pools.atEnd(); ++pools)
  {
    numTrapped += (*pools)->getBytesTrapped();
  }
*/
  return numTrapped;
}

size_t MemPool::getBytesTrapped()
{
  freeAllChunks();

  // All the memory in free lists is trapped.
  size_t numTrapped = 0;
  for (int i = 1; i <= MAX_INDEX; ++i)
  {
    size_t size = sIndexToSize[i];
    for (AllocCell* p = mFreeList[i]; p != NULL; p = p->next)
      numTrapped += size;

    // All the memory pending in the most recent chunk is trapped
    MemChunkContext& mcc = mChunkContexts[i];
    numTrapped += mcc.numBytesTrapped();
  }
  return numTrapped;
}


void MemSystemClearByteCount()
{
  sGetPool()->mBytesAllocated = 0; // ignore overhead for Stats
}

//extern UInt32 gStatsOverhead;
size_t CarbonMem::getBytesAllocated()
{
  size_t bytesAlloced = sGetPool()->mBytesAllocated;
/*
  unsigned long bytesAlloced = 0;
  for (PthreadIter pools; !pools.atEnd(); ++pools)
  {
    bytesAlloced += (*pools)->mBytesAllocated;
  }
*/

  return bytesAlloced;
}

size_t CarbonMem::getHighWater()
{
  size_t maxBytesAlloced = sGetPool()->mMaxBytesAllocated;

/*
  unsigned long maxBytesAlloced = 0;
  for (PthreadIter pools; !pools.atEnd(); ++pools)
  {
    maxBytesAlloced += (*pools)->mMaxBytesAllocated;
  }
*/

  return maxBytesAlloced;
}

// Arrays for tracking temporary memory pools for reuse.  These must
// be maintained without any memory allocation.
static const UInt32 scMaxTempPools = 10;
static MemPool* sTempPools[scMaxTempPools] = { NULL };
static void* sTempPoolIDs[scMaxTempPools] = { NULL };
static UInt32 sNumTempPools = 0;

static MemPool* sFindTempPool(void* id)
{
  MemPool* pool = NULL;
  for (UInt32 i = 0; (pool == NULL) && (i < sNumTempPools); ++i) {
    if (sTempPoolIDs[i] == id) {
      pool = sTempPools[i];
    }
  }
  return pool;
}

MemPool* CarbonMem::createMemPool(void* id)
{
  // Get the current pool for this thread
  MemPool* oldPool = sGetPool();

  // Have we already allocated a temp pool?
  MemPool* newPool = sFindTempPool(id);
  if (newPool == NULL) {
    // Allocate/initialize a new pool.  Use system malloc(), not the
    // memory manager's.
    PthreadLock lock;
    newPool = (MemPool*) ::malloc(sizeof(MemPool));
    MEM_ASSERT(newPool);
    newPool->init();

    // Save the void*->MemPool mapping.  The order here is important
    // because sFindTempPool doesn't have the lock when it runs, and
    // it needs a consistent view of the pools.
    MEM_ASSERT(sNumTempPools < scMaxTempPools);
    sTempPools[sNumTempPools] = newPool;
    sTempPoolIDs[sNumTempPools] = id;
    ++sNumTempPools;
  }
    
  // Use the new pool for this thread
  pthread_setspecific(sThreadKey, newPool);

  return oldPool;
}

void CarbonMem::restoreMemPool(MemPool* pool)
{
  pthread_setspecific(sThreadKey, pool);
}


// Below are the 32-bit and 64-bit implementations of block and chunk
// management functions.

#if MEM_MANAGER_64
// The 64-bit memory manager needs some additional bookkeeping to map
// blocks and chunks to indices.  Each megablock in use is assigned an
// index, which is used in the generation of a chunk's ID.
//
// We'll track the megablocks in static arrays.  Chunk/index lookups
// will require a linear walk of this array, but that shouldn't be
// that bad.  In practice, only a few megablocks will be used.  In
// fact, for processes that use a small (<< 4G) amount of memory, it's
// likely that everything will be in the same megablock.
//
// Making these fixed arrays (instead of allocating/reallocating them
// as megablocks are used) allows readers of the arrays to access them
// without having to acquire a mutex first, which is important for
// performance.  A mutex will only be needed when adding a new
// megablock to the array, which will be infrequent.
//
// Because the megablock index is basically (with a multiplier) an
// additional offset into the block and chunk tables, all the possible
// chunks and blocks in each megablock are implicitly indexed, even if
// they're not actually in use.  That means we'll probably never
// really use the maximum number of blocks and chunks we can track.
// That's not really a big deal, because successive mallocs should
// generally be from the same megablock.
//
// The block and chunk tables allow for 32 GB of memory and each
// megablock is 4GB, so we can handle 8 unique megablocks.  I did an
// experiment in which I tried to allocate 32GB in 8M blocks (which is
// what the memory manager does), and I got to 29GB before I would
// have needed a ninth megablock.  90% utilization sounds pretty good
// to me, especially because it's unlikely that anyone will really
// use that much memory.

//! Maximum number of megablocks
static const MegaBlockID scMaxMegaBlocks = MAX_NUM_BLOCKS / BLOCKS_PER_MEGABLOCK;
typedef UInt32 MegaBlockTag;
//! High 32-bits of the megablocks in use
static MegaBlockTag sMegaBlocks[scMaxMegaBlocks] = { 0 };
//! Number of megablocks in use
static MegaBlockID sNumMegaBlocks = 0;
//! Mutex for adding megablocks
MUTEX_WRAPPER_DECLARE(sMegaBlockMutex);

//! Returns the megablock index for an address, creating a new one if necessary
static MegaBlockID sGetMegaBlockIndex(char* ptr)
{
  // The high 32 bits are used as a tag to identify the megablock.
  // Check if we have this one indexed already.
  MegaBlockTag tag = ((UIntPtr) ptr) >> MEGA_BLOCK_SHIFT;
  for (MegaBlockID index = 0; index < sNumMegaBlocks; ++index) {
    if (sMegaBlocks[index] == tag) {
      return index;
    }
  }

  // Didn't find a match, so add a new entry.  We need a lock for this.
  MutexWrapper mutex(&sMegaBlockMutex);

  // Do another pass.  It's possible that another thread was trying to
  // add the same megablock tag at the same time and beat us to it.
  for (MegaBlockID index = 0; index < sNumMegaBlocks; ++index) {
    if (sMegaBlocks[index] == tag) {
      return index;
    }
  }

  // Add the new entry, provided we have room
  MEM_ASSERT(sNumMegaBlocks < scMaxMegaBlocks);
  MegaBlockID index = sNumMegaBlocks;
  sMegaBlocks[index] = tag;
  // This must be done last so non-mutexed readers of the megablock
  // see a valid state.
  ++sNumMegaBlocks;

  return index;
}

//! Returns the overall block index for an address, when the megablock index is already known.
static BlockID sGetBlockIndexFromMegaBlock(char* ptr, MegaBlockID megablockIndex)
{
  // Calculate the offset within the megablock just as is done for the
  // entire 4G address space in 32-bit mode.
  BlockID offset = ((UIntPtr)ptr & BLOCK_MASK) / BLOCK_SIZE;
  // Add the starting index of the megablock
  return (BlockID)megablockIndex * BLOCKS_PER_MEGABLOCK + offset;
}

//! Returns the overall chunk index for an address, when the megablock index is already known.
static ChunkID sGetChunkIndexFromMegaBlock(char* ptr, MegaBlockID megablockIndex)
{
  // Calculate the offset within the megablock just as is done for the
  // entire 4G address space in 32-bit mode.
  ChunkID offset = ((UIntPtr)ptr & BLOCK_MASK) >> CHUNK_SHIFT;
  // Add the starting chunk index of the megablock
  return (ChunkID)megablockIndex * CHUNKS_PER_MEGABLOCK + offset;
}

#endif

static ChunkID sChunkToIndex(void* ptr)
{
#if MEM_MANAGER_64
  MegaBlockID megablockIndex = sGetMegaBlockIndex((char*)ptr);
  ChunkID chunkIndex = sGetChunkIndexFromMegaBlock((char*)ptr, megablockIndex);
  return chunkIndex;
#else
  return ((UIntPtr) ptr) >> CHUNK_SHIFT;
#endif
}

static ChunkCell* sIndexToChunk(ChunkID index)
{
#if MEM_MANAGER_64
  // Determine the corresponding megablock
  MegaBlockID megablockIndex = index / BLOCKS_PER_MEGABLOCK;
  MEM_ASSERT(megablockIndex < sNumMegaBlocks);
  // The high 32 bits of the start of the chunk come from the
  // megablock tag.  The low 32-bits are calculated just as in 32-bit
  // mode
  UIntPtr highBits = (UIntPtr)sMegaBlocks[megablockIndex] << MEGA_BLOCK_SHIFT;
  UIntPtr lowBits = ((UIntPtr)index << CHUNK_SHIFT) & BLOCK_MASK;
  return (ChunkCell*)(highBits | lowBits);
#else
  return ((ChunkCell*) ((UIntPtr)(index) << CHUNK_SHIFT));
#endif
}

static ChunkCell* sPtrToChunk(void* ptr)
{
#if MEM_MANAGER_64
  return (ChunkCell *) ((unsigned long) ptr & CHUNK_PTR_MASK_64);
#else
  return (ChunkCell *) ((unsigned long) ptr & CHUNK_PTR_MASK_32);
#endif
}

static BlockID sBlockId(char* block)
{
#if MEM_MANAGER_64
  MegaBlockID megablockIndex = sGetMegaBlockIndex(block);
  BlockID blockIndex = sGetBlockIndexFromMegaBlock(block, megablockIndex);
  return blockIndex;
#else
  return (((UIntPtr)block) / BLOCK_SIZE);
#endif
}

// Determine the address of the last chunk in a block.  Chunks are
// always aligned to 64K boundaries.
static char* sLastChunk(char* block, size_t size)
{
  char* lastChunk;
#if MEM_MANAGER_64
  // We have to consider the case that an 8 MB block spans two
  // megablocks.  That shouldn't happen too frequently, but when it
  // does, we have to allocate chunks only in the first megablock.
  // Otherwise the fast chunk->index mapping won't work.
  char* endOfBlock = block + size;
  if (((UIntPtr)(endOfBlock - 1) & MEGA_BLOCK_MASK) != ((UIntPtr)block & MEGA_BLOCK_MASK)) {
    lastChunk = (char *) (((UIntPtr)endOfBlock & MEGA_BLOCK_MASK) - CHUNK_SIZE);
  } else {
    lastChunk = (char *) ((UIntPtr)(endOfBlock - CHUNK_SIZE) & CHUNK_PTR_MASK_64);
  }
#else
  lastChunk = (char *) ((((UIntPtr) block) + size - CHUNK_SIZE) & CHUNK_PTR_MASK_32);
#endif
  return lastChunk;
}
