// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "util/CarbonTypes.h"
#include "util/CarbonTypeUtil.h"
#include "util/CarbonAssert.h"
#include <string.h> // For NULL
#include "util/UtString.h"
#include "util/UtIOStream.h"

ClockEdge ClockEdgeOppositeEdge( ClockEdge ce )
{
  ClockEdge opposite_edge = eClockNegedge; // silence gcc
  switch (ce) {
  case eClockPosedge:
    opposite_edge = eClockNegedge;
    break;
  case eClockNegedge:
    opposite_edge = eClockPosedge;
    break;
  case eLevelHigh:
    opposite_edge = eLevelLow;
    break;
  case eLevelLow:
    opposite_edge = eLevelHigh;
    break;
  default:
    INFO_ASSERT(0, "Invalid ClockEdge.");
    break;
  }
  return opposite_edge;
}

const char * ClockEdgeString( ClockEdge ce )
{
  switch (ce) {
  case eClockPosedge:
    return "posedge";
  case eClockNegedge:
    return "negedge";
  case eLevelHigh:
    return "levelHigh";
  case eLevelLow:
    return "levelLow";
  default:
    INFO_ASSERT(0, "Invalid ClockEdge.");
    break;
  }
  return NULL;
}


UInt32 Log2(UInt32 width)
{
  UInt32 log = 0;

  // If the size is not an exact power-of two, we need to round up
  if ((width & -((SInt32)width)) != width) {
    log++;                      // round up power
  }

  while (width>>=1) log++;

  return log;
}

static void sListAppendToString(UtString* str, const char* item)
{
  if (! str->empty())
    (*str) << ", ";
  (*str) << item;
}

void carbonNetPrintFlags(NetFlags flags)
{
  UtString text;
  if (NetIsPortType(flags, eInputNet))
    sListAppendToString(&text, "input");
  if (NetIsPortType(flags, eOutputNet))
    sListAppendToString(&text, "output");
  if (NetIsPortType(flags, eBidNet))
    sListAppendToString(&text, "bid");

  // The declarationType code in IODBRead should probably be
  // refactored so that it can be reused here. For now, the
  // declaration type will be skipped here (but not in 
  // shell/carbondb.cxx).

  if(NetIsFlop(flags))
    sListAppendToString(&text, "flop");
  if(NetIsLatch(flags))
    sListAppendToString(&text, "latch");
  if(NetIsAliased(flags))
    sListAppendToString(&text, "aliased");
  if(NetIsAllocated(flags))
    sListAppendToString(&text, "allocated");
  if(NetIsInaccurate(flags))
    sListAppendToString(&text, "inaccurate");
  if(NetIsTriWritten(flags))
    sListAppendToString(&text, "tri-written");
  if(NetIsBlockLocal(flags))
    sListAppendToString(&text, "block-local");
  if(NetIsNonStatic(flags))
    sListAppendToString(&text, "non-static");
  if(NetIsPullUp(flags))
    sListAppendToString(&text, "pullup");
  if(NetIsPullDown(flags))
    sListAppendToString(&text, "pulldown");
  if(NetIsPrimaryZ(flags))
    sListAppendToString(&text, "primary-z");
  if(NetIsInClkPath(flags))
    sListAppendToString(&text, "clk-path");
  if(NetIsInDataPath(flags))
    sListAppendToString(&text, "data-path");
  if(NetIsEdgeTrigger(flags))
    sListAppendToString(&text, "edge-trigger");
  if(NetIsTemp(flags))
    sListAppendToString(&text, "temp");
  if(NetIsForcible(flags))
    sListAppendToString(&text, "force");
  if(NetIsDeposit(flags))
    sListAppendToString(&text, "deposit");
  if(NetIsRecordPort(flags))
    sListAppendToString(&text, "record-port");
  if(NetIsRead(flags))
    sListAppendToString(&text, "read");
  if(NetIsWritten(flags))
    sListAppendToString(&text, "written");
  if(NetIsConstZ(flags))
    sListAppendToString(&text, "const-z");
  if(NetIsDead(flags))
    sListAppendToString(&text, "dead");
  if(NetIsReset(flags))
    sListAppendToString(&text, "reset");
  if(NetIsSigned(flags))
    sListAppendToString(&text, "signed");
  if(NetIsClearAtEnd(flags))
    sListAppendToString(&text, "clear-at-end");

  UtIO::cout() << text << UtIO::endl;
}
