#undef STRUCT                   /* defined by carbon_shelltypes.h */
#include "util/CarbonTypes.h"
#include "util/UtString.h"

#ifndef YYSTYPE
#define YYSTYPE int
#endif

SInt32 carbonCExpr_lex(YYSTYPE *ignore);
SInt32 carbonCExpr_lex();
SInt32 carbonCExpr_parse();
void carbonCExpr_YYInput(char* buf, int* result, int max_size);
extern "C" void carbonCExpr_error(const char*);
extern "C" void carbonCExprComment();
extern "C" void carbonCExprCount();
extern "C" SInt32 carbonCExprCheckType();
extern "C" SInt32 carbonCExprInput();
extern "C" void carbonCExprUnput(char);
extern "C" void carbonCExprDeclareIdent(const char*);
//extern "C" void carbonCExprPutchar(char);
extern "C" int carbonCExpr_input();
extern "C" void carbonCExpr_unput(char);

struct yy_buffer_state* carbonCExpr__scan_string(const char*);
