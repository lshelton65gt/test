/******************************************************************************
 Copyright (c) 2004-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*! 
  \file 
  Verilog file and VHDL File->library association class implementation
*/

#include "util/HdlFileCollector.h"
#include "util/MemManager.h"
#include "util/UtStringUtil.h"  // for strcasecmp
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtHashMap.h"
#include "util/UtPair.h"
#include "util/OSWrapper.h"

#include <cstring>

const char* HdlFileCollector::scLegacyHdlLibOption[2] = {"-vlogLib",  "-vhdlLib"};

bool sIsOption(const char* arg)
{
  return ((*arg == '-') || (*arg == '+'));
}

HdlFileCollector::HdlFileCollector()
{
   for (int i=0; i < LangVer::LangVer_MAX; i++) {
      mHasLangLib[i] = false;
   }

   mVhdlVer = LangVer::VHDL93;
   mVerilogVersion = LangVer::Verilog;   // verilog 95 is the default


   // The following definition of values for mHdlExtensionVector implies that a
   // particular file extension indicates a particular language version (and that
   // may have been the original intent of the mHdlExtensionVector, but that is
   // not how this code works).
   // It is only important that an extension actually appers in one of the VHDL or Verilog entries.
   // The specific language version that will be used to parse a file is not tied to the
   // extension (or to which mHdlExtensionVector) but instead is determined by command line options like -93 or -v2k.
   // A particular extension string may appear in different vectors for the same language group
   // (for example '.sv' can appear in both SystemVerilog2005 and SystemVerilog2009), 
   // but it is wrong to put the same extension in both a VHDL and a Verilog vector.
   mHdlExtensionVector[LangVer::VHDL87].push_back(".vhd");
   mHdlExtensionVector[LangVer::VHDL93].push_back(".vhdl");

   mHdlExtensionVector[LangVer::Verilog].push_back(".v");

   mHdlExtensionVector[LangVer::SystemVerilog2005].push_back(".sv");
   mHdlExtensionVector[LangVer::SystemVerilog2005].push_back(".sysv");
   
   // .cem extension
   mHdlExtensionVector[LangVer::Verilog].push_back(".cem");

   mLibSpecifiedOnCmdLine = false;

}

void HdlFileCollector::addHdlExt(LangVer langVer, const char* ext)
{
    UtString extStr;
    if (*ext != '.')
	extStr << '.';
    extStr << ext;

    mHdlExtensionVector[langVer].push_back(extStr.c_str());
}

HdlFileCollector::~HdlFileCollector()
{
   for( hdlLibList::iterator iter = mLibs.begin();
	iter != mLibs.end();
	++iter )
   {
      delete *iter;
   }
   mLibs.clear();
   mCurrentLib = NULL;
}

//! Provide a callback from arg parsing
/*! This method currently does the parsing of the -vhdlLib option and for vhdl files.
 *  It also processes the language variant options like -v2k or -93.
 *  It also processes the Verilog source files.
 *  
 *  \returns true if the callback has consumed the arg and param
 */
bool
HdlFileCollector::scanArgument( const char *arg, const char *param )
{
  // Parse off the library information
  // This string _must_ always match the definition in JaguarContext.cxx!

  bool consumed = false;
  LangVer langVer;

  if ( !strncmp( arg, HdlFileCollector::scLegacyHdlLibOption[0], strlen(HdlFileCollector::scLegacyHdlLibOption[0]) ) )
  {
     UtString libName, libPath;
     scanHdlLib(param, libName, libPath);

     addLibrary( libName, libPath,  LangVer::Verilog ); // do we need Verilog2001 here?

     // Make this library the default
     setDefaultLib( libName.c_str(), libPath.c_str(), LangVer::Verilog ); // do we need Verilog2001 here?

     mLibSpecifiedOnCmdLine = true;

     mHasLangLib[LangVer::Verilog] = true;
  }
  else if ( !strncmp( arg, HdlFileCollector::scLegacyHdlLibOption[1], strlen(HdlFileCollector::scLegacyHdlLibOption[1]) ) )
  {
     UtString libName, libPath;
     scanHdlLib(param, libName, libPath);

     addLibrary( libName, libPath,  mVhdlVer );

     // Make this library the default
     setDefaultLib( libName.c_str(), libPath.c_str(), mVhdlVer );

     mLibSpecifiedOnCmdLine = true;

     mHasLangLib[LangVer::VHDL87] = true;
     mHasLangLib[LangVer::VHDL93] = true;
  } 
  else if ( isHdlFile(arg, langVer) )
  {
    // this is a file with a known extension, so save the file and language pair
     consumed = true;
    
    // Make a list of all the HDL files
    addFile( arg, langVer );
  }
  // Parse off the language version information
  // These strings _must_ always match the definition in JaguarContext.cxx!  RJC which file? FIX THIS to use constant strings
  else if ( !strncmp( arg, "-87", 3 ) )
  {
    consumed = true;
    mVhdlVer = LangVer::VHDL87;
  }
  else if ( !strncmp( arg, "-93", 3 ) )
  {
    consumed = true;
    mVhdlVer = LangVer::VHDL93;
  }
  else if ( ( !strncmp( arg, "-v2k", 4 ) ) ||
            ( !strncmp( arg, "-v2001", 6 ) ) ||
            ( !strncmp( arg, "-v2000", 6 ) )   ) {
    consumed = true;
    mVerilogVersion = LangVer::Verilog2001;
  }
  else if ( !strncmp( arg, "-sv2005", 7 ) ) {
    consumed = true;
    mVerilogVersion = LangVer::SystemVerilog2005;
  }
  else if ( ( !strncmp( arg, "-sv2009", 7 ) ) ||
            ( !strncmp( arg, "-sverilog", 9 ) ) ){ // currently -sverilog is the same as -sv2009
    consumed = true;
    mVerilogVersion = LangVer::SystemVerilog2009;
  }
  else if ( !strncmp( arg, "-sv2012", 7 ) ) {
    consumed = true;
    mVerilogVersion = LangVer::SystemVerilog2012;
  }
  else {
    // here we assume that all other arguments are simply a filename and we will use the current Verilog variant to parse them
    consumed = true;
    addFile( arg, mVerilogVersion );
  }
  return consumed;
}

void
HdlFileCollector::setDefaultLib(const char *hdlLibName, const char *hdlLibPath, LangVer langVer )
{
  if (hdlLibName != NULL) {
    mDefaultLibName[langVer] = hdlLibName;
  }

  if (hdlLibPath != NULL) {
    mDefaultLibPath[langVer] = hdlLibPath;
  }

}

const char*
HdlFileCollector::getDefaultLibName(LangVer langVer) const
{
  return mDefaultLibName[langVer].c_str();
}

const char*
HdlFileCollector::getDefaultLibPath(LangVer langVer) const
{
  return mDefaultLibPath[langVer].c_str();
}

void HdlFileCollector::setFileRoot(const char* fileRoot)
{
  mFileRoot = fileRoot;

  for( LangVer langVer=LangVer::LangVer_MIN; langVer < LangVer::LangVer_MAX; langVer++) {
    if (mDefaultLibPath[langVer].empty()) {
      mDefaultLibPath[langVer] << mFileRoot << "." << mDefaultLibName[langVer];
    }
  }

  for ( const_iterator lib_iter = this->begin(); lib_iter != this->end(); ++lib_iter ) {
      (*lib_iter)->setFileRoot(fileRoot);
  }
}

HdlFileCollector::const_hdlLibListIterator
HdlFileCollector::begin() const
{
  return mLibs.begin();
}


HdlFileCollector::const_hdlLibListIterator
HdlFileCollector::end() const
{
  return mLibs.end();
}


void
HdlFileCollector::addArgument( const char *arg )
{
   UtIO::cout() << "HdlFileCollector::addArgument(): why are you using this function. " << arg << UtIO::endl;
}

const bool HdlFileCollector::hasVerilogFiles() const {
  bool haveVlogFiles = false;
  for (LangVer langVer=LangVer::LangVerVerilog_MIN; (!haveVlogFiles && (langVer <= LangVer::LangVerVerilog_MAX)); langVer++) {
    haveVlogFiles |= (hasHdlFiles(langVer));
  }
  return haveVlogFiles;
}

const bool HdlFileCollector::hasVHDLFiles() const {
  bool haveVHDLFiles = hasHdlFiles(LangVer::VHDL87) || hasHdlFiles(LangVer::VHDL93);
  return haveVHDLFiles;
}



// returns true if the file extension of arg is known to be used for one (or more) language variants, also returns the associated language variant in langVer
bool HdlFileCollector::isHdlFile(const char* arg, LangVer& langVer) const
{
   bool ret = false;
   const char* atExt = std::strrchr(arg, '.');
   if (atExt) {
     for( langVer=LangVer::LangVer_MIN; langVer < LangVer::LangVer_MAX; langVer++) {
       for (UtStringArray::UnsortedCLoop p = mHdlExtensionVector[langVer].loopCUnsorted(); !p.atEnd(); ++p) {
	 ret = (strcasecmp(atExt, *p) == 0);
         // a recognized extension has been detected
         // (note: the same extension might be listed under multiple mHdlExtensionVector[langVer],
         //  but that is not important here since langVer will be forced to either the default VHDL
         //  or verilog langVer below)
	 if(ret == true) {
           switch ( langVer ){
           case LangVer::VHDL87:
           case LangVer::VHDL93:{
             langVer = mVhdlVer; // if -93 or -87 specified on cmd line, then force this VHDL file to be parsed with that variant
             break;
           }
           case LangVer::Verilog:
           case LangVer::Verilog2001:
           case LangVer::SystemVerilog2005:
           case LangVer::SystemVerilog2009:
           case LangVer::SystemVerilog2012: {
	     langVer = mVerilogVersion;  // use the language variant defined by the -v2k -v95 -sv2012 type switch
             break;
           }
           case LangVer::LangVer_MAX:{
             INFO_ASSERT(0, "Incorrect value for langVer");
           }
           }
	   return true;
	 }
       }
     }
   }
   return ret;
}

void HdlFileCollector::scanHdlLib(const char *param,
                                  UtString& libName, 
				  UtString& libPath)
{
  char *paramcopy = (char*) CarbonMem::malloc(strlen( param ) + 1);
  strncpy( paramcopy, param, strlen( param ) + 1 );
  char *colon = strchr( paramcopy, ':' );

  if ( colon != NULL ) {
    *colon = '\0';
    libPath = ++colon;
  }

  libName = paramcopy;
  CarbonMem::free(paramcopy);
}

void HdlFileCollector::printOrderedFileList() const
{
  UtString fileName(mFileRoot);

  fileName << ".ordered";

  UtOFStream f(fileName.c_str());

  if (!f)
  {
    UtIO::cerr() << "Could not write ordered file list to " << fileName << UtIO::endl;
    return;
  }

  for (HdlFileCollector::const_hdlLibListIterator hdlLibIter = begin();
       hdlLibIter != end(); 
       ++hdlLibIter)
  {
    HdlLib* hdlLib = (*hdlLibIter);

    UtString libName(hdlLib->getLogicalLibrary());
    UtString libPath(hdlLib->getLibPath());
    
    f << "-lib " << libName << ":" << libPath << UtIO::endl;

    for ( UInt32 ver = 0; ver < LangVer::LangVer_MAX; ++ver) {
      LangVer langVer(ver);

      if ( hdlLib->getNumberOfFiles( langVer ) ) {
	if ( LangVer::VHDL87 == langVer ) {
	  f << "-87" << UtIO::endl;
	}
	else if ( LangVer::VHDL93 == langVer ) {
	  f << "-93" << UtIO::endl;
	}
      }	

      for (HdlLib::const_hdlFileIterator fileIter = hdlLib->begin(langVer); 
	   fileIter != hdlLib->end(langVer); 
	   ++fileIter) {
	f << *fileIter << UtIO::endl;
      }
    }
  }
}

bool HdlFileCollector::parseLibMapFile(const char* libMapFile)
{
  UtIFStream infile(libMapFile);

  if (!infile.is_open()) {
    // If the file is not there, it will created afterwards.
    return true;
  }

  UtString line;

  while (infile.getline(&line)) {
    // Skip comments (lines that start with '#', '//' or '--')
    if ((line[0] == '#') ||
	(line[0] == '/' && line[1] == '/') ||
	(line[0] == '-' && line[1] == '-')) {
      continue;
    }
    
    StrToken tok(line.c_str());

    if (*tok == NULL)
      continue;

    UtString logicalLibName(*tok);
    
    // Skip the empty line
    if (logicalLibName.empty()) {
      continue;
    }

    ++tok;

    if (*tok == NULL) {
      continue;
    }

    UtString separator(*tok);

    // Make sure the separator is there
    if (separator != "=>") {
      UtIO::cerr() << "Illegal separator used in library mapping file. "
	"Valid separator is \"=>\". Ignoring line." << UtIO::endl;
      continue;
    }

    ++tok;

    if (*tok == NULL)
      continue;

    UtString physicalLibLoc( *tok );

    mLibSpecifiedOnCmdLine = true;

    // Since we don't know if the library specified in the libmap file is Verilog,
    // VHDL 87, or VHDL 93 or all three, it needs to be added to all the library types 
    for (LangVer langVer=LangVer::LangVer_MIN; langVer<LangVer::LangVer_MAX; langVer++) {

      // Add the library
      addLibrary( logicalLibName, physicalLibLoc, langVer );

      // The last library read will be the default library
      setDefaultLib( logicalLibName.c_str(), physicalLibLoc.c_str(), langVer );
    }
  }

  if (!infile.close()) {
    UtIO::cerr() << "Could not close the file specified in the -libMap option." << UtIO::endl;
    return false;
  }

  return true;
}

bool HdlFileCollector::saveLibMapFile(const char* libMapFile)
{
  UtOFStream f(libMapFile);

  if (!f) {
    UtIO::cerr() << "Could not write to file specified in the -libMap option." << UtIO::endl;
    return false;
  }

  // Create a common repository for all VLOG and VHDL library mappings, to avoid duplication
  typedef UtPair<UtString,UtString> LibNameAndPath;
  typedef UtArray<LibNameAndPath*> LibraryList;
  typedef UtHashMap<UtString, UtString> DuplicateMap;

  LibraryList libNameAndPathList;
  DuplicateMap libNameAndPathMap;

  for (const_iterator libIter = mLibs.begin(); libIter != mLibs.end(); ++libIter) {
     UtString libName = (*libIter)->getLogicalLibrary();
     UtString libPath = (*libIter)->getLibPath();
     DuplicateMap::iterator addLibrary = libNameAndPathMap.find(libName);
     if (addLibrary == libNameAndPathMap.end()) {
	libNameAndPathList.push_back(new LibNameAndPath(libName, libPath));
	libNameAndPathMap[libName] = libPath;
     }
  }

  // Output the list
  for (LibraryList::iterator iter = libNameAndPathList.begin(); iter != libNameAndPathList.end(); ++iter) {
    f << (*iter)->first << " => " << (*iter)->second << "\n";
  }

  // Delete the list elements
  for (LibraryList::iterator iter = libNameAndPathList.begin(); iter != libNameAndPathList.end(); ++iter) {
    delete *iter;
  }

  if (!f.close()) {
    UtIO::cerr() << "Could not close the file specified in the -libMap option." << UtIO::endl;
    return false;
  }

  return true;
}

//! Add a new library to the list of libraries
void
HdlFileCollector::addLibrary( const UtString &library, const UtString &path, LangVer langVer )
{
    // Don't allow duplicates
    HdlLib *newLib = NULL;

    for (const_iterator libIter = mLibs.begin(); libIter != mLibs.end(); ++libIter) {
      if ((strcmp((*libIter)->getLogicalLibrary().c_str(), library.c_str()) == 0) &&
	  (langVer == (*libIter)->libraryLanguage())){
	newLib = *libIter;
	break;
      }
    }

    if ( newLib == NULL ) {
      newLib = new HdlLib( library, path );
      newLib->setLibraryLanguage(langVer);
      mLibs.push_back( newLib );
    }
    else {
      if ((0 != strlen(newLib->getLibPath().c_str())) && (0 != strlen(path.c_str()))) {
	
	// We may need more elaborate path comparison. TODO
	UtString newPath;
	if(path.empty()) {
	  newPath << mFileRoot << "." << mDefaultLibName[langVer];
	}
	else if('/' != path.c_str()[0]) {
	  newPath = mFileRoot + newPath;
	}
	
	UtString oldPath;
	if(path.empty()) {
	  oldPath << mFileRoot << "." << mDefaultLibName[langVer];
	}
	else if('/' != path.c_str()[0]) {
	  oldPath = mFileRoot + oldPath;
	}
	
	bool pathNotSame =  strcmp(oldPath.c_str(), newPath.c_str());
	
	UtString buf;
	buf << "Old library physical path \"" << newLib->getLibPath()
	    << "\" is not same as current library physical path\"" << path << "\""
	    << " for logical library \"" << newLib->getLogicalLibrary() << "\"";
	INFO_ASSERT(pathNotSame == false, buf.c_str());
      }
      
      newLib->setLibPath( path );
    }

    mCurrentLib = newLib;
}


//! Add a new HDL file to the currently active library
void
HdlFileCollector::addFile( const UtString &hdlFile, LangVer langVer )
{
  if ( mLibs.empty() ) {
    // The user did not specify a HDL library, so we need
    // to create an entry for the default WORK lib.
    addLibrary(mDefaultLibName[langVer], mDefaultLibPath[langVer], langVer);
  }
  
  INFO_ASSERT(mCurrentLib!=NULL, "No current HDL library exists");
  mCurrentLib->addFile( hdlFile, langVer );
}

UInt32 HdlFileCollector::getNumberOfLibs() const
{
    return mLibs.size();
}

bool HdlFileCollector::hasHdlFiles(LangVer ver) const
{
  const_iterator lib_iter;
  for ( lib_iter = this->begin(); lib_iter != this->end(); ++lib_iter ) {
    if ( ( LangVer::LangVerVerilog_MIN <= ver ) && ( ver <= LangVer::LangVerVerilog_MAX ) ) {
      // this is a request for verilog or SystemVerilog language
      HdlLib::const_hdlFileIterator file_iter =  (*lib_iter)->begin(ver);
      for (; file_iter != (*lib_iter)->end(ver); ++file_iter ) {
	if(!sIsOption(*file_iter)) return true;
      }
    } 
    else {
      if((*lib_iter)->getNumberOfFiles(ver) > 0) {
	return true;
      }
    }
  }
  return false;
}

bool HdlFileCollector::hasHdlOptions(LangVer ver) const
{
  const_iterator lib_iter;
  for ( lib_iter = this->begin(); lib_iter != this->end(); ++lib_iter ) {
    HdlLib::const_hdlFileIterator file_iter =  (*lib_iter)->begin(ver);
    for (; file_iter != (*lib_iter)->end(ver); ++file_iter ) {
      if(sIsOption(*file_iter)) return true;
    }
  }
  return false;
}


//////////////////////////////////////////////////////////////////////
// class HdlLib definitions

HdlLib::HdlLib( const UtString &library, const UtString &path ) :
    mLogicalLibrary( library ),
    mPhysicalLibPath( path ),
    mLibLang( LangVer::LangVer_MIN )
{ }

HdlLib::~HdlLib(){}

void
HdlLib::addFile( const UtString &hdlFile, LangVer ver )
{
  mFilesAndArgs[ver].push_back( hdlFile );

  if(ver != this->libraryLanguage()) {
      this->setLibraryLanguage(LangVer::LangVer_MAX);
  }
}

UInt32
HdlLib::getNumberOfFiles( LangVer ver )
{
  return mFilesAndArgs[ver].size();
}

HdlLib::const_iterator
HdlLib::begin( LangVer ver) const
{
  return mFilesAndArgs[ver].begin();
}

HdlLib::const_iterator
HdlLib::end( LangVer ver) const
{
  return mFilesAndArgs[ver].end();
}

HdlLib::hdlFileIterator
HdlLib::find( const char* file, LangVer ver )
{
  HdlLib::hdlFileIterator iter = mFilesAndArgs[ver].begin();
  for (; iter != mFilesAndArgs[ver].end(); ++iter) {
    if (0 == strcmp(*iter, file)) {
      return iter;
    }
  }

  return mFilesAndArgs[ver].end();
}

HdlLib::const_hdlFileIterator
HdlLib::placeFileAtBottom( LangVer ver, const char* file )
{
  // Need to get a non-const iterator pointing to the file
  hdlFileIterator iter = find( file, ver );

  // If this file is already at the end of the list, return
  if ( (iter + 1) == mFilesAndArgs[ver].end() ) {
    return end( ver );
  }

  // The currFile is the file we are moving to the bottom of the list
  UtString currFile( *iter );

  // Get the name of the next file in the list
  UtString nextFile( *(iter + 1) );

  // Erase the file. The iterator is now invalid.
  mFilesAndArgs[ver].erase( iter );

  // Add the file at the bottom of the list.
  addFile( currFile, ver );

  // Return the const_iterator for the next file in the list.
  return mFilesAndArgs[ver].find( nextFile.c_str() );
}


const UtString& HdlLib::getLogicalLibrary() const
{
  return mLogicalLibrary;
}

const UtString& HdlLib::getLibPath() const
{
  return mPhysicalLibPath;
}

void HdlLib::setLogicalLibrary( const UtString& library )
{
  mLogicalLibrary.assign( library, 0, library.length() );
}

void HdlLib::setLibPath( const UtString& path )
{
  mPhysicalLibPath.assign( path, 0, path.length() );
}

void HdlLib::setFileRoot(const char* fileRoot)
{
  if (mPhysicalLibPath.empty()) {
    mPhysicalLibPath << fileRoot << "." << mLogicalLibrary;
  }
}

