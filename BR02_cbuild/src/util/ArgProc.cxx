// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2014 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/ArgProc.h"
#include "util/CarbonAssert.h"
#include "util/OSWrapper.h"
#include "util/FileCollector.h"
#include "util/UtStringArray.h"
#include "util/UtWildcard.h"
#include "util/UtShellTok.h"
#include "util/UtIStream.h"
#include <ctype.h>
#ifndef _C_MEMMANAGER_H_
#include "util/c_memmanager.h"
#endif

#if pfUNIX
#include <errno.h>  // for EOVERFLOW
#endif

/*!
  \file
  Implementation of ArgProc class.
*/

//! verbatim begin string
static const char scVerbatimBegin[] = "<verbatim>";
//! Length of verbatim begin string
static const UInt32 scVerbBeginLen = sizeof(scVerbatimBegin) - 1;

//! verbatim end string
static const char scVerbatimEnd[] = "</verbatim>";
//! Length of verbatim end string
static const UInt32 scVerbEndLen = sizeof(scVerbatimEnd) - 1;

//! Formatting object
/*!
  This class handles print formatting for usage information
*/
class ArgProc::LineWrapBuf
{
public: CARBONMEM_OVERRIDES

  //! Indent UtString constant
  const UtString cIndent;
  //! Subsection indent UtString constant
  const UtString cSubIndent;

  //! Constructor
  LineWrapBuf(SInt32 margin) :
    cIndent("    "),
    cSubIndent("  "),
    mMargin(margin)
  {
    mCharCount = 0;
    mVerbatim = false;
  }
  
  //! Add an indent
  void indent()
  {
    mIndent += cIndent;
    mBuf += cIndent;
  }
  
  //! Subtract an indent
  void unindent()
  {
    UtString::size_type indSize = mIndent.size();
    mIndent.erase(indSize - cIndent.size(), indSize);
  }
  
  //! Begin a subsection, adds a sub indent
  void beginSubSection()
  {
    mIndent += cSubIndent;
    addNewline();
  }

  //! End a subsection, subtracts a sub indent
  void endSubSection()
  {
    UtString::size_type indSize = mIndent.size();
    mIndent.erase(indSize - cSubIndent.size(), UtString::npos);
    addNewline();
  }

  //! Add a new line 
  void addNewline()
  {
    addNewlineVerbatim();
    mBuf += mIndent;
    mCharCount += mIndent.size();
  }

  //! Add a newline only
  void addNewlineVerbatim()
  {
    mBuf += '\n';
    mCharCount = 0;
  }
  
  //! Get the buffer for printing
  const UtString& getBuffer() const
  {
    return mBuf;
  }

  //! Add a whitespace
  void addSpace() 
  {
    if (! maybeAddLine())
    {
      mBuf += ' ';
      ++mCharCount;
    }
  }

  //! Add a UtString (series of words)
  void addStr(const UtString& str)
  {
    UtString::size_type strSize = str.size();
    
    if (strSize > 0)
    {
      maybeAddLine();

      INFO_ASSERT(mMargin >= mCharCount, str.c_str());

      // check for verbatim
      UtString::size_type beginVerbatim;
      if ((beginVerbatim = str.find(scVerbatimBegin)) != UtString::npos)
      {
        // we have a verbatim block
        // chop off any preceding or postceding text, and do those
        // separately.
        
        if (beginVerbatim > 0)
          // Prefix processing
          addStr(str.substr(0, beginVerbatim - 1));
        

        // Must start on a fresh line
        if (mCharCount != 0)
          addNewlineVerbatim();
        
        // Find the end of the block
        UtString::size_type endVerbatim = 
          str.find(scVerbatimEnd, beginVerbatim + scVerbBeginLen);
        
        /* 
           if endVerbatim is UtString::npos then that is ok. We just add
           the rest of the string
        */
        
        const UtString& tmp = str.substr(beginVerbatim + scVerbBeginLen, endVerbatim - (beginVerbatim + scVerbBeginLen));
        mBuf += tmp;
        addNewline();  // Indent here
        
        if ((endVerbatim != UtString::npos) && ((endVerbatim + scVerbEndLen) < strSize))
          // we have postfix to process
          addStr(str.substr(endVerbatim + scVerbEndLen, strSize - (endVerbatim + scVerbEndLen)));
      }
      else if (strSize <= static_cast<UtString::size_type>(mMargin - mCharCount)) 
      {
        // just add the string
        mBuf += str;
        mCharCount = (mCharCount + strSize) % mMargin;
      }
      else
      {
        // str is too big to fit on this line
        // I need to modify the UtString, so make a copy.
        UtString workStr = str;

        // step 1: fit as much as possible
        UtString tmpStr;
        tmpStr.assign(workStr.data(), (mMargin - mCharCount));
        
        // step 2: Find last space in sub string
        UtString::size_type lastSpace = tmpStr.rfind(' ');
        if (lastSpace != UtString::npos)
        {
          mBuf.append(tmpStr, 0, lastSpace);
          mCharCount += lastSpace;
          workStr.erase(0, lastSpace + 1);

          // step 3: new line and finish the string
          addNewline();
          addStr(workStr);
        }
        else
        {
          // Cannot break this humongous 'word' up.
          mBuf += tmpStr;
          workStr.erase(0, tmpStr.size());
          
          // Does the rest of the UtString have any spaces? Would like to
          // break it at a word boundary
          UtString::size_type endSpace = workStr.find(' ');
          if (endSpace != UtString::npos)
          {
            mBuf.append(workStr, 0, endSpace);
            workStr.erase(endSpace + 1);

            mCharCount = mMargin;
            addNewline();
            addStr(workStr);
          }
          else
          {
            // No space on the tail. Just append the rest of the
            // string.
            mBuf += workStr;
            mCharCount = mMargin; // newline on next addition
          } // else
        } // else
      } // else
    } // if strsize > 0
    INFO_ASSERT(mCharCount <= mMargin, str.c_str());
  }
  
  //! Add a title and center it
  void centerTitle(const UtString& title)
  {
    if (mCharCount != 0)
      addNewline();

    if (title.size() <= static_cast<UtString::size_type>(mMargin))
    {
      UtString::size_type offset = (mMargin - title.size())/2;
      UtString tmp (offset, ' ');
      tmp += title;
      addStr(tmp);
      addNewline();
    }
    else
    {
      // need to chop it up, as in addStr
      UtString workStr = title;
      UtString tmpStr;
      tmpStr.assign(workStr.data(), mMargin);
      
      UtString::size_type lastSpace = tmpStr.rfind(' ');
      if (lastSpace != UtString::npos)
      {
        mBuf.append(tmpStr, 0, lastSpace);
        mCharCount += lastSpace;
        workStr.erase(0, lastSpace + 1);
        
        // step 3: new line and finish the string
        addNewline();
        centerTitle(workStr);
      }
      else
      {
        // Cannot break this humongous 'word' up.
        mBuf += tmpStr;
        workStr.erase(0, tmpStr.size());
          
        // Does the rest of the UtString have any spaces? Would like to
        // break it at a word boundary
        UtString::size_type endSpace = workStr.find(' ');
        if (endSpace != UtString::npos)
          {
            mBuf.append(workStr, 0, endSpace);
            workStr.erase(endSpace + 1);
            
            mCharCount = mMargin;
            addNewline();
            centerTitle(workStr);
          }
          else
          {
            // No space on the tail. Just append the rest of the
            // string.
            mBuf += workStr;
            mCharCount = mMargin; // newline on next addition
          } // else
      } // else
    } // else
  }

private:
  SInt32 mMargin;
  UtString mBuf;
  UtString mIndent;
  SInt32 mCharCount;
  bool mVerbatim;

  bool maybeAddLine()
  {
    bool added = false;
    if (mCharCount == mMargin)
    {
      addNewline();
      added = true;
    }
    return added;
  }

  // forbid
  LineWrapBuf();
  CARBON_FORBID_DEFAULT_CTORS(LineWrapBuf);
};

//! Base class of all option types.
/*!
  This is a rather messy class since we have to handle different types
  of options, i.e., strings, doubles, ints, bools. This is very
  inelegant, but it is only meant for private use within
  ArgProc, so all is fine.
*/
class ArgProc::CmdLineArg
{
  friend class ArgProc::Section;
  friend class ArgProc::OptionCmp;

public: CARBONMEM_OVERRIDES

  //! constructor
  CmdLineArg(const UtString& optionName, const char* doc, 
             bool allowMultiple, SInt32 pass)
    : mSection(NULL), mUnprocessedGroup(NULL), mName(optionName), mOptionDoc(doc),
      mAllowMultiple(allowMultiple)
  {
    mHasDefault = false;
    mIsInputArgFile = false;
    mIsParsed = eNotParsed;
    mAllowUnsectionedUsage = false;
    mPass = pass;
    mIsDeprecated = false;
    mIsAllowedOnlyInEFFile = false;
  }
  
  //! virtual destructor
  virtual ~CmdLineArg() {}
  
  //! Get the option name
  const UtString& getName() const { return mName; }

  //! Get the option documentation
  const UtString& getDoc() const { return mOptionDoc; }

  //! Get the pass number for this option
  SInt32 getPassNum() const { return mPass; }

  //! Add valid Synonym
  void addValidSynonym(const char* synonym) {
    mSynonyms.push_back(synonym);
  }

  void addDeprecatedSynonym(const char* synonym) {
    mDeprecatedSynonyms.push_back(synonym);
  }

  bool isDeprecatedSynonym(const UtString& synonym)
  {
    bool ret = false;
    if (! mDeprecatedSynonyms.empty() &&
        mName.compare(synonym) != 0)
      ret = mDeprecatedSynonyms.find(synonym.c_str()) != mDeprecatedSynonyms.end();
    return ret;
  }

  // has synonyms that aren't deprecated?
  bool hasValidSynonyms() const {
    return ! mSynonyms.empty();
  }

  bool hasSynonyms() const {
    return hasValidSynonyms() || ! mDeprecatedSynonyms.empty();
  }
  
  void addSynonymsToBuf(ArgProc::LineWrapBuf* lwBuf) const
  {
    for (UtStringArray::UnsortedCLoop p = mSynonyms.loopCUnsorted();
         !p.atEnd(); ++p)
    {
      UtString tmp("|");
      tmp << *p;
      lwBuf->addStr(tmp);
    }
  }

  //! Return the option type
  virtual ArgProc::OptionTypeT getType() const = 0;

  virtual UInt32 numValues() const = 0;

  //! Add an integer value
  virtual void addValue(SInt32, UtString*) {}
  //! Return the first value encountered (or default) - integer
  virtual SInt32 getValueInt() const { return -1; }

  //! Add a string
  virtual void addValue(const UtString&, UtString*) {}

  //! Overloaded
  virtual void addValue (const char *, UtString*) {}

  //! Return the first value encountered (or default) - string
  virtual const char* getValueStr() const { return ""; }
  
  //! Change the boolean default
  virtual void addValue(bool, UtString*) {}

  //! Add a double value
  virtual void addValue(double, UtString*) {}
  //! Return the first value encountered (or default) - double
  virtual double getValueDouble() const { return 0.0; }

  //! Returns true if multiple instances of option are allowed
  bool allowsMultiple() const { return mAllowMultiple;}
  //! Set the multiple allowance
  void setAllowMultiple(bool allowMultiple)
  {
    mAllowMultiple = allowMultiple;
  }
  
  //! Returns true if a default value is stored
  virtual bool hasDefault() const { return mHasDefault; }
  
  //! Returns a description in words of the type
  /*!
    This includes an article (a, an), so an error or general message
    can seamlessly integrate.

    \code
    // arg is a CmdLineArg*
    fprintf(stderr, "Parse error! Expected %s\n", arg->getTypeDescription());
    \endcode
  */
  virtual const char* getTypeDescription() const = 0;

  //! Resets the option as if it wasn't parsed
  /*!
    This can be used to reset an option in a given ArgProc to a
    pre-parse state.
  */
  void reInit()
  {
    mIsParsed = eNotParsed;
    resetToDefault();
  }

  //! Resets the option to its default value.
  /*!
    Clears the value list and resets the value to the default value.
    If the option does not have a default, the values are only
    cleared.
  */
  virtual void resetToDefault() = 0;

  //! Set the parse status for this option
  void setIsParsed(ArgProc::ParseStatusT parseStatus)
  {
    // never supercede an error
    if (mIsParsed != ArgProc::eParseError)
      mIsParsed = parseStatus;
  }
  
  //! Returns parse status for this option
  ArgProc::ParseStatusT isParsed() const { return mIsParsed; }

  //! Sets the section to which the option is assigned
  void putSection(ArgProc::Section* section)
  {
    mSection = section;
  }

  //! Returns the section to which the option is assigned
  ArgProc::Section* getSection() 
  {
    const CmdLineArg* me = const_cast<const CmdLineArg*>(this);
    return const_cast<ArgProc::Section*>(me->getSection());
  }

  const ArgProc::Section* getSection() const
  {
    return mSection;
  }

  void setIsAllowed() {
    mAllowUnsectionedUsage = true;
  }

  void setDisallowed() {
    mAllowUnsectionedUsage = false;
  }

  bool isAllowed() const 
  {
    return (mSection != NULL) || mAllowUnsectionedUsage;
  }
  
  //! Sets the unprocessed argument group to which the option is assigned
  void putUnprocessedGroup(ArgProc::UnprocessedGroup* group)
  {
    mUnprocessedGroup = group;
  }

  //! Returns the unprocessed argument group to which the option is assigned
  ArgProc::UnprocessedGroup* getUnprocessedGroup() 
  {
    const CmdLineArg* me = const_cast<const CmdLineArg*>(this);
    return const_cast<ArgProc::UnprocessedGroup*>(me->getUnprocessedGroup());
  }

  const ArgProc::UnprocessedGroup* getUnprocessedGroup() const
  {
    return mUnprocessedGroup;
  }

  // Methods for avoiding dynamic_cast
  virtual const ArgProc::IntCmdLineArg*     castIntCmdLineArg()     const { return NULL; }

  virtual const ArgProc::StrCmdLineArg*     castStrCmdLineArg()     const { return NULL; }
  virtual const ArgProc::BoolOverrideArg*   castBoolOverrideArg()   const { return NULL; }
  virtual const ArgProc::BoolCmdLineArg*    castBoolCmdLineArg()    const { return NULL; }
  virtual const ArgProc::BoolWildcardArg*   castBoolWildcardArg()   const { return NULL; }
  virtual const ArgProc::DoubleCmdLineArg*  castDoubleCmdLineArg()  const { return NULL; }
  virtual const ArgProc::InFileCmdLineArg*  castInFileCmdLineArg()  const { return NULL; }
  virtual const ArgProc::OutFileCmdLineArg* castOutFileCmdLineArg() const { return NULL; }


  ArgProc::IntCmdLineArg* castIntCmdLineArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::IntCmdLineArg*>(me->castIntCmdLineArg());
  }

  ArgProc::StrCmdLineArg* castStrCmdLineArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::StrCmdLineArg*>(me->castStrCmdLineArg());
  }

  ArgProc::BoolCmdLineArg* castBoolCmdLineArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::BoolCmdLineArg*>(me->castBoolCmdLineArg());
  }

  ArgProc::BoolOverrideArg* castBoolOverrideArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::BoolOverrideArg*>(me->castBoolOverrideArg());
  }

  ArgProc::BoolWildcardArg* castBoolWildcardArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::BoolWildcardArg*>(me->castBoolWildcardArg());
  }

  ArgProc::DoubleCmdLineArg* castDoubleCmdLineArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::DoubleCmdLineArg*>(me->castDoubleCmdLineArg());
  }

  ArgProc::InFileCmdLineArg* castInFileCmdLineArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::InFileCmdLineArg*>(me->castInFileCmdLineArg());
  }

  ArgProc::OutFileCmdLineArg* castOutFileCmdLineArg() { 
    const CmdLineArg* me = const_cast<const CmdLineArg*> (this);
    return const_cast<ArgProc::OutFileCmdLineArg*>(me->castOutFileCmdLineArg());
  }

  void putIsInputArgFile(bool isInputArgFile) {
    mIsInputArgFile = isInputArgFile;
  }
  
  bool isInputArgFile() const { return mIsInputArgFile; }
  
  void putSideEffect(const char* sideEffectString){
    mSideEffectString = sideEffectString;
  }

  const char* getSideEffect() const { return mSideEffectString.c_str(); }

  void putIsDeprecated(bool isDeprecated) 
  {
    mIsDeprecated = isDeprecated;
  }

  bool isDeprecated() const {
    return mIsDeprecated;
  }
  void setIsAllowedOnlyInEFFile()
  {
    mIsAllowedOnlyInEFFile = true;
  }

  bool isAllowedOnlyInEFFile() const {
    return mIsAllowedOnlyInEFFile;
  }

protected:
  //! default existence boolean
  bool mHasDefault;

  //! Returns true if this allows multiple or is not being overridden
  bool checkAllowMultiple(UtString* str)
  {
    bool allowsMult = allowsMultiple();
    bool ret = allowsMult || (str == NULL) || (isParsed() != ArgProc::eParsed);
    
    if (! allowsMult)
      // set the parse status to not parsed so it will replace any
      // value already added
      setIsParsed(ArgProc::eNotParsed);
    
    return ret;
  }
  
private:
  ArgProc::Section* mSection;
  ArgProc::UnprocessedGroup* mUnprocessedGroup;
  //! name of option
  UtString mName;
  //! documentation of option
  UtString mOptionDoc;
  //! parse status
  ArgProc::ParseStatusT mIsParsed;
  //! pass number
  SInt32 mPass;
  //! Synonym list
  UtStringArray mSynonyms;
  //! Deprecated synonym list
  UtStringArray mDeprecatedSynonyms;

  //! allow multiple boolean
  bool mAllowMultiple;
  //! Allow usage of this arg in the case that it is not sectioned
  bool mAllowUnsectionedUsage;
  //! is input arg file
  bool mIsInputArgFile;
  //! switch(s) that are added when the current one is specified
  UtString mSideEffectString;
  //! Is this a deprecated option?
  bool mIsDeprecated;
  //! allowed only in .ef file
  bool mIsAllowedOnlyInEFFile;

  // forbid
  CmdLineArg();
  CARBON_FORBID_DEFAULT_CTORS(CmdLineArg);
};

//! Integer command line argument object
class ArgProc::IntCmdLineArg : public ArgProc::CmdLineArg
{
public: CARBONMEM_OVERRIDES

  //! constructor
  IntCmdLineArg(const char* optionName, const char* doc, 
                bool allowMultiple, SInt32 pass) 
    : CmdLineArg(optionName, doc, allowMultiple, pass),
      mDefaultValue(0)
  {
  }
  
  //! virtual destructor
  virtual ~IntCmdLineArg() {}
  
  /* implementation */
  //! See CmdLineArg::getType()
  virtual ArgProc::OptionTypeT getType() const 
  { 
    return ArgProc::eInt;
  }

  //! See CmdLineArg::getTypeDescription()
  virtual const char* getTypeDescription() const
  {
    return "an integer";
  }

  virtual void resetToDefault()
  {
    mValues.clear();
    if (mHasDefault)
      setDefault(mDefaultValue);
  }
  
  /* virtual override */
  //! Add an integer value
  /*!
    This overrides CmdLineArg::addValue(SInt32, UtString*)
  */
  virtual void addValue(SInt32 value, UtString* overrideMsg) 
  {
    // checkAllowMultiple() must be called!
    if (! checkAllowMultiple(overrideMsg))
    {
      if (! mValues.empty() && mValues[0] != value)
        *overrideMsg << "Overriding (" << getName() << " " << mValues[0] 
                     << ") with (" << getName() << " " << value << ")\n";
    }

    if ((isParsed() == ArgProc::eNotParsed) &&
        (mValues.size() == 1))
      mValues[0] = value;
    else
      mValues.push_back(value);
  }

  virtual void addValue (const UtString&, UtString*){INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (const char *, UtString*) {INFO_ASSERT (0, "Cannot happen."); }
  virtual void addValue (bool, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (double, UtString*) {INFO_ASSERT (0, "Cannot happen.");}

  //! Returns first value in the integer list, or default
  /*!
    Overrides CmdLineArg::getValueInt()
  */
  virtual SInt32 getValueInt() const { return mValues[0]; }

  virtual const ArgProc::IntCmdLineArg* castIntCmdLineArg() const { return this; }

  /* specialization */
  //! Set the default value
  void setDefault(SInt32 defaultVal)
  {
    mHasDefault = true;
    mDefaultValue = defaultVal;
    addValue(defaultVal, NULL);
  }

  //! Get the integer list iterator
  IntIter getIntIter() const
  {
    return IntIter(mValues.begin(), mValues.end());
  }

  virtual UInt32 numValues() const
  {
    return mValues.size();
  }

private:
  UtArray<SInt32> mValues;
  SInt32 mDefaultValue;

  // forbid
  IntCmdLineArg();
  CARBON_FORBID_DEFAULT_CTORS(IntCmdLineArg);
};

//! String command line argument object
class ArgProc::StrCmdLineArg : public ArgProc::CmdLineArg
{
public: CARBONMEM_OVERRIDES

  //! Constructor
  StrCmdLineArg(const char* optionName, const char* doc, 
                bool allowMultiple, SInt32 pass) 
    : CmdLineArg(optionName, doc, allowMultiple, pass)
  {
  }
  
  //! virtual destructor
  virtual ~StrCmdLineArg() {}
  
  /* implementation */
  //! See CmdLineArg::getType()
  virtual ArgProc::OptionTypeT getType() const 
  { 
    return ArgProc::eString;
  }

  //! See CmdLineArg::getTypeDescription()
  virtual const char* getTypeDescription() const
  {
    return "a string";
  }

  virtual void resetToDefault()
  {
    mValues.clear();
    if (mHasDefault)
      setDefault(mDefaultValue.c_str());
  }

  /* virtual override */
  //! Add UtString value to list
  /*!
    Overrides CmdLineArg::addValue(const char*, UtString*)
  */
  virtual void addValue(const char* value, UtString* overrideMsg) 
  {
    if (! checkAllowMultiple(overrideMsg))
    {
      if (! mValues.empty() && strcmp(mValues[0], value) != 0)
        *overrideMsg << "Overriding (" << getName() << " " << mValues[0] 
                     << ") with (" << getName() << " " << value << ")\n";
    }
    
    if ((isParsed() == ArgProc::eNotParsed) &&
        (mValues.size() == 1))
    {
      mValues.clear();
    }
    mValues.push_back(value);
  }

  //! Add the unexpanded version (no environment variables expanded) of the string value
  void addUnexpandedValue(const char* unexpandedValue) {
    if ((isParsed() == ArgProc::eNotParsed) &&
        (mUnexpandedValues.size() == 1))
    {
      mUnexpandedValues.clear();
    }
    mUnexpandedValues.push_back(unexpandedValue);
  }

  virtual void addValue (bool, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (SInt32, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (double, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (const UtString&, UtString*){INFO_ASSERT (0, "Cannot happen.");}
  //! Get first UtString value in list (or default)
  /*!
    Overrides CmdLineArg::getValueStr()
  */
  virtual const char* getValueStr() const 
  { 
    return mValues[0]; 
  }

  //! Returns first UtString value of the list (or default)
  const char* getUnexpandedValueStr() const
  {
    return mUnexpandedValues[0];
  }

  virtual const ArgProc::StrCmdLineArg* castStrCmdLineArg() const { return this; }

  /* specialization */
  //! Set the default UtString value
  void setDefault(const char* defaultVal)
  {
    mHasDefault = true;
    mDefaultValue = defaultVal;
    addValue(defaultVal, NULL);
  }

  //! Sets the equivalent unexpanded default value
  void setUnexpandedDefault(const char* unexpandedDefaultVal) 
  {
    addUnexpandedValue(unexpandedDefaultVal);
  }

  //! Return UtString list iterator
  StrIter getStrIter() const
  {
    return StrIter(mValues.begin(), mValues.end());
  }

  //! Return UtString list iterator of unexpanded values
  StrIter getUnexpandedStrIter() const
  {
    return StrIter(mUnexpandedValues.begin(), mUnexpandedValues.end());
  }

  virtual UInt32 numValues() const
  {
    return mValues.size();
  }

private:
  UtStringArray mValues;
  UtStringArray mUnexpandedValues;
  UtString mDefaultValue;

  // forbid
  StrCmdLineArg();
  CARBON_FORBID_DEFAULT_CTORS(StrCmdLineArg);
};


//! Input File command line argument object
class ArgProc::InFileCmdLineArg : public ArgProc::StrCmdLineArg
{
  //friend class ArgProc::OutFileCmdLineArg;
public: CARBONMEM_OVERRIDES

  //! Constructor
  InFileCmdLineArg(const char* optionName, const char* doc, 
                   bool allowMultiple, SInt32 pass) 
    : StrCmdLineArg(optionName, doc, allowMultiple, pass)
  {
  }
  
  //! virtual destructor
  virtual ~InFileCmdLineArg() {}
  
  /* implementation */
  //! See CmdLineArg::getType()
  virtual ArgProc::OptionTypeT getType() const 
  { 
    return ArgProc::eInFile;
  }

  //! See CmdLineArg::getTypeDescription()
  virtual const char* getTypeDescription() const
  {
    return "an input file";
  }

  virtual const ArgProc::InFileCmdLineArg* castInFileCmdLineArg() const { return this; }

private:

  // forbid
  InFileCmdLineArg();
  CARBON_FORBID_DEFAULT_CTORS(InFileCmdLineArg);
};

//! Output File command line argument object
class ArgProc::OutFileCmdLineArg : public ArgProc::StrCmdLineArg
{
  //friend class ArgProc::InFileCmdLineArg;
public: CARBONMEM_OVERRIDES

  //! Constructor
  OutFileCmdLineArg(const char* optionName, const char* doc, 
                   bool allowMultiple, SInt32 pass) 
    : StrCmdLineArg(optionName, doc, allowMultiple, pass)
  {
    mFileType = eRegFile;
  }
  
  //! virtual destructor
  virtual ~OutFileCmdLineArg() {}
  
  /* implementation */
  //! See CmdLineArg::getType()
  virtual ArgProc::OptionTypeT getType() const 
  { 
    return ArgProc::eOutFile;
  }

  //! See CmdLineArg::getTypeDescription()
  virtual const char* getTypeDescription() const
  {
    return "an output file";
  }

  virtual const ArgProc::OutFileCmdLineArg* castOutFileCmdLineArg() const { return this; }

  void setFileType(FileTypeT fileType) {
    mFileType = fileType;
  }

  FileTypeT getFileType() const {
    return mFileType;
  }
  
private:
  FileTypeT mFileType;

  // forbid
  OutFileCmdLineArg();
  CARBON_FORBID_DEFAULT_CTORS(OutFileCmdLineArg);
};

//! CmdLineArg for type bool
class ArgProc::BoolCmdLineArg : public ArgProc::CmdLineArg
{
public: CARBONMEM_OVERRIDES

  //! constructor
  BoolCmdLineArg(const char* optionName, const char* doc, bool allowMultiple, SInt32 pass) 
    : CmdLineArg(optionName, doc, allowMultiple, pass)
  {
    mValue = false;
    mHasOverride = false;
    mHasDefault = true;
    mDefault = false;
  }
  
  //! virtual destructor
  virtual ~BoolCmdLineArg() {}
  
  /* implementation */
  //! See CmdLineArg::getType()
  virtual ArgProc::OptionTypeT getType() const 
  { 
    return ArgProc::eBool;
  }
  
  //! See CmdLineArg::getTypeDescription()
  virtual const char* getTypeDescription() const
  {
    return "a bool";
  }

  virtual void resetToDefault()
  {
    mValue = mDefault;
  }

  /* virtual override */
  //! Replaces boolean value
  /*!
    The function name is a misnomer, but it follows the conventions of
    its sibling functions.
  */
  virtual void addValue(bool value, UtString*) 
  {
    mValue = value;
  }

  virtual void addValue (const UtString&, UtString*){INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (const char *, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (SInt32, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (double, UtString*) {INFO_ASSERT (0, "Cannot happen.");}

  virtual const ArgProc::BoolCmdLineArg* castBoolCmdLineArg() const { return this; }

  /* specialization */
  //! Set the default boolean value
  void setDefault(bool defaultVal)
  {
    mHasDefault = true;
    mDefault = defaultVal;
    mValue = defaultVal;
  }

  //! Get the boolean value
  bool getValue() const
  {
    return mValue;
  }

  //! Get the default boolean value
  bool getDefault() const
  {
    // All boolean options have defaults.
    return mDefault;
  }
  
  void setHasOverride()
  {
    mHasOverride = true;
  }

  bool hasOverride() const
  {
    return mHasOverride;
  }

  virtual UInt32 numValues() const
  {
    return 0;
  }

private:
  bool mDefault;
  bool mValue;
  bool mHasOverride;

  // forbid
  BoolCmdLineArg();
  CARBON_FORBID_DEFAULT_CTORS(BoolCmdLineArg);
};

class ArgProc::BoolWildcardArg : public ArgProc::BoolCmdLineArg
{
public:
  BoolWildcardArg(const char* optionName, const char* visibleOptionName, 
                         const char* doc, SInt32 pass) :
    BoolCmdLineArg(visibleOptionName, doc, false, pass), mWildcardExpr(optionName)
  {}

  const char* getWildcardExpr() const { return mWildcardExpr.c_str(); }

  void addMatch(const char* occurrence) 
  {
    mOccurrences.push_back(occurrence);
    addValue(true, NULL);
  }
  
  StrIter getMatches() const
  {
    return StrIter(mOccurrences.begin(), mOccurrences.end());
  }

  const BoolWildcardArg* castBoolWildcardArg() const { return this; }

private:
  CARBON_FORBID_DEFAULT_CTORS(BoolWildcardArg);

  UtString mWildcardExpr;
  UtStringArray mOccurrences;
};

//! CmdLineArg for a boolean override
class ArgProc::BoolOverrideArg: public ArgProc::CmdLineArg
{
  
public: CARBONMEM_OVERRIDES

  BoolOverrideArg(const char* optionName, const char* doc, SInt32 pass) 
    : CmdLineArg(optionName, doc, false, pass), mOption(NULL)
  {}

  virtual ~BoolOverrideArg() {}

  void setOption(BoolCmdLineArg* option)
  {
    mOption = option;
  }

  BoolCmdLineArg* getOption() 
  {
    return mOption;
  }

  //! Return the option type
  virtual ArgProc::OptionTypeT getType() const 
  {
    return ArgProc::eBoolOverride;
  }

  virtual const char* getTypeDescription() const
  {
    return "a bool";
  }

  virtual void resetToDefault()
  {
    // Reset the option to its default. This class's getValue handles
    // the override.
    mOption->resetToDefault();
  }
  
  virtual const ArgProc::BoolOverrideArg* castBoolOverrideArg() const { return this; }

  //! Get the boolean value
  /*!
    This is only here for completeness. A user should never get the
    value of an override.
  */
  bool getValue() const
  {
    return ! mOption->getValue();
  }
  
  //! The override option was found on the command line
  /*!
    This inverts the value of the option this is overriding.
  */
  void setOverrideOn(UtString* overrideMsg)
  {
    if (mOption->getValue() == mOption->getDefault())
    {
      if (mOption->isParsed() == ArgProc::eParsed)
      {
        // If the user explicitly put the overridden option on the
        // command line, then warn that it is being overridden.
        *overrideMsg << "Overriding (" << mOption->getName() << ") with (" << getName() << ")\n";
      }
      mOption->addValue(! mOption->getDefault(), NULL);
    }
  }
  
  //! This reverses the override and warns appropriately
  void setOverrideOff(UtString* overrideMsg)
  {
    if (isParsed() == ArgProc::eParsed)
    {
      // we parsed the boolean we are overriding after we have parsed
      // this option
      if (mOption->getValue() != mOption->getDefault())
        *overrideMsg << "Overriding (" << getName() << ") with (" << mOption->getName() << ")\n";
    }
    
    mOption->addValue(mOption->getDefault(), NULL);
  }

  virtual UInt32 numValues() const
  {
    return 0;
  }

private:
  CARBON_FORBID_DEFAULT_CTORS(BoolOverrideArg);

  BoolCmdLineArg* mOption;
};

//! CmdLineArg for type double
class ArgProc::DoubleCmdLineArg : 
  public ArgProc::CmdLineArg
{
public: CARBONMEM_OVERRIDES

  //! constructor
  DoubleCmdLineArg(const char* optionName, const char* doc, 
                   bool allowMultiple, SInt32 pass) 
    : CmdLineArg(optionName, doc, allowMultiple, pass),
      mDefaultValue(0.0)
  {
  }
  
  //! virtual destructor
  virtual ~DoubleCmdLineArg() {
    clearValues();
  }
  
  /* implementation */
  //! See CmdLineArg::getType()
  virtual ArgProc::OptionTypeT getType() const 
  { 
    return ArgProc::eDouble;
  }

  //! See CmdLineArg::getTypeDescription()
  virtual const char* getTypeDescription() const
  {
    return "a double";
  }

  virtual void resetToDefault()
  {
    clearValues();
    if (mHasDefault)
      setDefault(mDefaultValue);
  }

  /* virtual override */
  //! Add a double value to list
  /*!
    Overrides CmdLineArg::addValue(double, UtString*)
  */
  virtual void addValue(double value, UtString* overrideMsg) 
  {
    if (! checkAllowMultiple(overrideMsg))
    {
      if (! mValues.empty() && *(mValues[0]) != value)
        *overrideMsg << "Overriding (" << getName() << " " << *mValues[0] 
                     << ") with (" << getName() << " " << value << ")\n";
    }
    
    if ((isParsed() == ArgProc::eNotParsed) &&
        (mValues.size() == 1))
    {
      *mValues[0] = value;
    }
    else {
      double* vp = CARBON_ALLOC_VEC(double, 1);
      *vp = value;
      mValues.push_back(vp);
    }
  }

  virtual void addValue (const UtString&, UtString*){INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (const char *, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (bool, UtString*) {INFO_ASSERT (0, "Cannot happen.");}
  virtual void addValue (SInt32, UtString*) {INFO_ASSERT (0, "Cannot happen.");}

  //! Returns first value in list, or default
  /*!
    Overrides CmdLineArg::getValueDouble()
  */
  virtual double getValueDouble() const { return *mValues[0]; }

  virtual const ArgProc::DoubleCmdLineArg* castDoubleCmdLineArg() const { return this; }

  /* specialization */
  //! Set the default value 
  void setDefault(double defaultVal)
  {
    mHasDefault = true;
    mDefaultValue = defaultVal;
    addValue(defaultVal, NULL);
  }

  //! Get the list iterator
  DblIter getDblIter() const
  {
    return DblIter(DoubleStarLoop(mValues.begin(), mValues.end()));
  }

  virtual UInt32 numValues() const {
    return mValues.size();
  }

private:
  UtArray<double*> mValues;
  double mDefaultValue;

  void clearValues()
  {
    for (UInt32 i = 0; i < mValues.size(); ++i) {
      double* vp = mValues[i];
      CARBON_FREE_VEC(vp, double, 1);
    }
    mValues.clear();
  }


  // forbid
  DoubleCmdLineArg();
  CARBON_FORBID_DEFAULT_CTORS(DoubleCmdLineArg);
};

class ArgProc::OptionCmp
{
  friend class ArgProc::CmdLineArg;

public: CARBONMEM_OVERRIDES

  bool operator() (const ArgProc::CmdLineArg* arg1, const ArgProc::CmdLineArg* arg2) const
  {
    const UtString& arg1Name = arg1->getName();
    const UtString& arg2Name = arg2->getName();
    return (arg1Name.compare(arg2Name) < 0);
  }
  
};

//! Section class
class ArgProc::Section
{
  friend class ArgProc::CmdLineArg;
  
  typedef UtArray<ArgProc::CmdLineArg*> OptionVector;
  
public: CARBONMEM_OVERRIDES

  typedef Loop<OptionVector> OptionIter;
  typedef CLoop<OptionVector> OptionCIter;
  
  //! constructor
  Section(const UtString& name) : mName(name)
  {}
  
  //! Destructor
  ~Section() {}

  //! loop over options
  OptionIter loopOptions() { return OptionIter(mOptions); }
  //! loop over constant options
  OptionCIter cLoopOptions() const { return OptionCIter(mOptions); }
  
  //! sort the options
  void sortOptions() const {
    std::sort(mOptions.begin(), mOptions.end(), ArgProc::OptionCmp());
  }

  //! Get the section name
  const UtString& getName() const { return mName; }

  //! Add an option to this section
  void addOption(ArgProc::CmdLineArg* option) {
    mOptions.push_back(option);
  }
  
  //! True if any options exist in this section
  bool hasOptions() const { return ! mOptions.empty(); }
  
private:
  CARBON_FORBID_DEFAULT_CTORS(Section);

  UtString mName;
  
  mutable OptionVector mOptions;

};

class ArgProc::UnprocessedGroup : public ArgProc::Section
{
public: CARBONMEM_OVERRIDES
  UnprocessedGroup( const UtString& name ) : Section( name ) {}
  ~UnprocessedGroup() {}

  void addOccurrence( const char* occurrence, ArgProc::OptionTypeT type,
                      ArgProc::CmdLineArg* option)
  {
    mOptionOccurrences.push_back( occurrence );
    if (option)
    {
      if (type == eBool)
      {
        BoolWildcardArg* wcCmdLineArg = option->castBoolWildcardArg(); 
        if (wcCmdLineArg)
          wcCmdLineArg->addMatch(occurrence);
      }
      
      option->setIsParsed(ArgProc::eParsed);
    }
  }

  UtStringArgv &getOccurrenceArgv() { return mOptionOccurrences; }

private:
  CARBON_FORBID_DEFAULT_CTORS(UnprocessedGroup);

  UtStringArgv mOptionOccurrences;
};

void ArgProc::writeUsageToBuf(const CmdLineArg* option, 
                              LineWrapBuf* lwBuf) const
{
  lwBuf->addStr(option->getName());
  option->addSynonymsToBuf(lwBuf);
  OptionTypeT type = option->getType();
  if (type != eBool)
    lwBuf->addSpace();
  
  // Need a stream to handle the type formatting
  UtString tmp; 
  switch(type)
  {
  case eInt:
    tmp <<  "<integer>";
    if (option->hasDefault())
      tmp << ", DEFAULT: " << option->getValueInt();
    break;
  case eString:
  case eInFile:
  case eOutFile:
    tmp << "<string>";
    if (option->hasDefault())
      tmp << ", DEFAULT: " << option->getValueStr();
    break;
  case eDouble:
    tmp << "<real>";
    if (option->hasDefault())
      tmp << ", DEFAULT: " << option->getValueDouble();
    break;
  case eBool:
    {
      const BoolCmdLineArg* boolArg = option->castBoolCmdLineArg();
      if (boolArg->getDefault())
        tmp << ", DEFAULT: true";
    }
    break;
  case eBoolOverride:
    break;
  }

  if (option->allowsMultiple())
    tmp << ", multiple instances allowed.";

  if (option->isDeprecated())
    tmp << " (DEPRECATED)";

  lwBuf->addStr(tmp);
}

ArgProc::ArgProc(): mPass(0),
                    mAccessHiddenOptions(false),
                    mWarnOnDuplicates(true)
{
}

ArgProc::~ArgProc()
{
  mSectionTable.clear();
  
  for (SectionVector::iterator s = mSections.begin();
       s != mSections.end(); ++s)
    delete *s;
  
  for (UnprocessedGroupVector::iterator u = mUnprocessedGroups.begin();
       u != mUnprocessedGroups.end(); ++u)
    delete *u;
  
  mOptionTable.clear();
  for (OptionVector::iterator o = mOptions.begin();
       o != mOptions.end();
       ++o) {
    delete (*o);
  }

  mUnprocessed.clear();
  for (OptionVector::iterator o = mUnprocessedOptions.begin();
       o != mUnprocessedOptions.end();
       ++o) {
    delete (*o);
  }

}

void ArgProc::createSection(const char* sectionName)
{
  UtString section(sectionName);
  INFO_ASSERT(lookupSection(section) == NULL, sectionName);
  Section* sectionStruct = new Section(sectionName);
  mSectionTable[section] = sectionStruct;
  mSections.push_back(sectionStruct);
}

void ArgProc::addToSection(const char* sectionName, const char* optionName)
{
  UtString optionStr(optionName);
  CmdLineArg* option = lookupOption(optionStr, NULL);
  if (! option)
    option = lookupUnprocessed(optionStr);
  INFO_ASSERT(option, optionName);
  UtString sectionStr(sectionName);
  Section* section = lookupSection(sectionStr);
  INFO_ASSERT(section, sectionName);
  INFO_ASSERT(option->getSection() == NULL, optionName);
  option->putSection(section);
  section->addOption(option);
}

void ArgProc::accessHiddenOptions()
{
  mAccessHiddenOptions = true;
}

void
ArgProc::createUnprocessedGroup( const char* groupName )
{
  UtString group( groupName );
  INFO_ASSERT( lookupUnprocessedGroup( group ) == NULL, groupName );
  UnprocessedGroup* unprocessedGroupStruct = new UnprocessedGroup( groupName );
  mUnprocessedGroupTable[group] = unprocessedGroupStruct;
  mUnprocessedGroups.push_back(unprocessedGroupStruct);
}

void
ArgProc::addToUnprocessedGroup(const char* groupName, const char* optionName)
{
  UtString optionStr( optionName );
  CmdLineArg* option = lookupOption( optionStr, NULL );
  if ( !option )
    option = lookupUnprocessed( optionStr );
  INFO_ASSERT(option, optionName);
  UtString groupStr( groupName );
  UnprocessedGroup* group = lookupUnprocessedGroup( groupStr );
  INFO_ASSERT( group, groupName );
  INFO_ASSERT( option->getUnprocessedGroup() == NULL, optionName );
  option->putUnprocessedGroup( group );
  group->addOption( option );
}

ArgProc::UnprocessedGroup*
ArgProc::lookupUnprocessedGroup(const UtString& groupName)
{
  UnprocessedGroup* group = NULL;
  UnprocessedGroupTable::iterator p = mUnprocessedGroupTable.find(groupName);
  if (p != mUnprocessedGroupTable.end())
    group = p->second;
  return group;
}

UtStringArgv &
ArgProc::lookupUnprocessedGroupOccurrences( const char* groupName )
{
  ArgProc::UnprocessedGroup *group = lookupUnprocessedGroup( groupName );
  return group->getOccurrenceArgv();
}

void ArgProc::setDescription(const char* title, const char* name, 
                             const char* description)
{
  if (title)
    mTitle = title;

  if (name)
    mName = name;

  if (description)
    mDescription = description;
}


void ArgProc::addSynopsis(const UtString& synopsis)
{
  mSynopses.push_back(synopsis);
}

ArgProc::CmdLineArg* 
ArgProc::maybeAddOption(const char* optionName,
                        const char* doc, 
                        bool allowMultiple,
                        OptionTypeT type,
                        SInt32 pass)
{
  CmdLineArg* arg = NULL;
  UtString optionNameStr(optionName);
  OptionTable::const_iterator p =
    mOptionTable.find(optionNameStr);
  if (p == mOptionTable.end())
  {
    switch(type)
    {
    case eInt:
      arg = new IntCmdLineArg(optionName, doc, allowMultiple, pass);
      break;
    case eString:
      arg = new StrCmdLineArg(optionName, doc, allowMultiple, pass);
      break;
    case eBoolOverride:
      arg = new BoolOverrideArg(optionName, doc, pass);
      break;
    case eBool:
      arg = new BoolCmdLineArg(optionName, doc, allowMultiple, pass);
      break;
    case eDouble:
      arg = new DoubleCmdLineArg(optionName, doc, allowMultiple, pass);
      break;
    case eInFile:
      arg = new InFileCmdLineArg(optionName, doc, allowMultiple, pass);
      break;
    case eOutFile:
      arg = new OutFileCmdLineArg(optionName, doc, allowMultiple, pass);
      break;
    }
    mOptionTable[optionNameStr] = arg;
    mOptions.push_back(arg);
  }
  return arg;
}

void ArgProc::addInt(const char* optionName, const char* doc, 
                     SInt32 defaultVal, bool noDefault,
                     bool allowMultiple, SInt32 pass)
{
  CmdLineArg* arg = maybeAddOption(optionName, doc, allowMultiple, eInt, pass);
  if (VERIFY(arg))
  {
    IntCmdLineArg* intArg = arg->castIntCmdLineArg();
    INFO_ASSERT(intArg, optionName);

    bool hasDefault = ! noDefault;
    if (hasDefault)
      intArg->setDefault(defaultVal);
  }
  
}

void ArgProc::addString(const char* optionName, const char* doc, 
                        const char* defaultVal, 
                        bool noDefault,
                        bool allowMultiple, SInt32 pass)
{
  CmdLineArg* arg = maybeAddOption(optionName, doc, allowMultiple,
                                   eString, pass);
  
  if (VERIFY(arg))
  {
    StrCmdLineArg* strArg = arg->castStrCmdLineArg();
    INFO_ASSERT(strArg, optionName);

    bool hasDefault = ! noDefault;
    if (hasDefault && VERIFY(defaultVal))
      strArg->setDefault(defaultVal);
  }
}

void ArgProc::addUnprocessedString(const char *group, const char* optionName,
                                   const char* doc, const char *defaultVal,
                                   bool noDefault, bool allowMultiple, 
                                   SInt32 pass)
{
  UtString optionNameStr(optionName);

  if (mUnprocessed.find(optionNameStr) == mUnprocessed.end())
  {
    StrCmdLineArg* arg = new StrCmdLineArg(optionName, doc, allowMultiple, pass);
    mUnprocessed[optionNameStr] = arg;
    mUnprocessedOptions.push_back(arg);
    if ( !noDefault && VERIFY(defaultVal))
      arg->setDefault(defaultVal);
  }
  
  addToUnprocessedGroup( group, optionName );
}

void ArgProc::addUnprocessedBoolWildcard(const char* group, const char* optionName, 
                                         const char* visibleOptionName, 
                                         const char* doc,
                                         SInt32 pass)
{
  UtString optionNameStr(optionName);

  if (mUnprocessed.find(optionNameStr) == mUnprocessed.end())
  {
    CmdLineArg* arg = new BoolWildcardArg(optionName, visibleOptionName, doc, pass);
    arg->setAllowMultiple(false);
    
    mUnprocessed[optionNameStr] = arg;
    mUnprocessedOptions.push_back(arg);
    mUnprocessedWildcardOptions.push_back(arg);
  }
  addToUnprocessedGroup( group, optionName );
}

void ArgProc::addUnprocessedBool(const char *group, const char* optionName,
                                 const char* doc,
                                 SInt32 pass)
{
  UtString optionNameStr(optionName);

  if (mUnprocessed.find(optionNameStr) == mUnprocessed.end())
  {
    CmdLineArg* arg = new BoolCmdLineArg(optionName, doc, false, pass);
    arg->setAllowMultiple(true);
    
    mUnprocessed[optionNameStr] = arg;
    mUnprocessedOptions.push_back(arg);
  }
  addToUnprocessedGroup( group, optionName );
}

void ArgProc::addBool(const char* optionName, const char* doc, 
                      bool defaultVal,
                      SInt32 pass)
{
  CmdLineArg* arg = maybeAddOption(optionName, doc, false, eBool, pass);
  
  if (VERIFY(arg))
  {
    BoolCmdLineArg* boolArg = arg->castBoolCmdLineArg();
    INFO_ASSERT(boolArg, optionName);
    boolArg->setDefault(defaultVal);
  }
}

void ArgProc::addBoolOverride(const char* overrideOptName, 
                              const char* optionNameToOverride,
                              const char* doc)
{
  UtString docStr;
  docStr << "Overrides " << optionNameToOverride;
  if (doc)
    docStr.assign(doc);
  
  UtString optionNameStr(optionNameToOverride);
  CmdLineArg* barg = lookupOption(optionNameStr, NULL);
  INFO_ASSERT(barg, optionNameToOverride);
  BoolCmdLineArg* boolarg = barg->castBoolCmdLineArg();
  INFO_ASSERT(boolarg, optionNameToOverride);

  SInt32 pass = boolarg->getPassNum();

  CmdLineArg* arg = maybeAddOption(overrideOptName, docStr.c_str(),
                                   false, eBoolOverride, pass);
  
  if (VERIFY(arg))
  {
    BoolOverrideArg* overrideArg = arg->castBoolOverrideArg();
    INFO_ASSERT(overrideArg, overrideOptName);
    overrideArg->setOption(boolarg);
    boolarg->setHasOverride();
  }
}

void ArgProc::addDouble(const char* optionName, const char* doc,
                        double defaultVal, bool noDefault,
                        bool allowMultiple, SInt32 pass)
{
  CmdLineArg* arg = maybeAddOption(optionName, doc, allowMultiple,
                                   eDouble, pass);
  if (VERIFY(arg))
  {
    DoubleCmdLineArg* doubleArg = arg->castDoubleCmdLineArg();
    INFO_ASSERT(doubleArg, optionName);

    bool hasDefault = ! noDefault;
    if (hasDefault)
      doubleArg->setDefault(defaultVal);
  }
}

void ArgProc::addInputFile(const char* optionName, const char* doc,
                           const char* defaultVal, bool noDefault,
                           bool allowMultiple, SInt32 pass)
{
  CmdLineArg* arg = maybeAddOption(optionName, doc, allowMultiple, eInFile, pass);
  
  if (VERIFY(arg))
  {
    InFileCmdLineArg* strArg = arg->castInFileCmdLineArg();
    INFO_ASSERT(strArg, optionName);

    bool hasDefault = ! noDefault;
    if (hasDefault && VERIFY(defaultVal))
      strArg->setDefault(defaultVal);
  }

}

void ArgProc::addInputArgFile(const char* optionName, const char* doc)
{
  CmdLineArg* arg = maybeAddOption(optionName, doc, true, eInFile, 0);
  
  if (VERIFY(arg))
  {
    InFileCmdLineArg* strArg = arg->castInFileCmdLineArg();
    INFO_ASSERT(strArg, optionName);
    strArg->putIsInputArgFile(true);
  }
}

void ArgProc::addOutputFile(const char* optionName, const char* doc,
                            const char* defaultVal, 
                            bool noDefault,
                            FileTypeT fileType,
                            bool allowMultiple, SInt32 pass)
{
  CmdLineArg* arg = maybeAddOption(optionName, doc, allowMultiple, eOutFile, pass);
  
  if (VERIFY(arg))
  {
    OutFileCmdLineArg* strArg = arg->castOutFileCmdLineArg();
    INFO_ASSERT(strArg, optionName);

    strArg->setFileType(fileType);

    bool hasDefault = ! noDefault;
    if (hasDefault && VERIFY(defaultVal))
      strArg->setDefault(defaultVal);
  }
}


void ArgProc::addSynonym(const char * masterName,
                         const char * subordinateName,
                         bool subordIsDeprecated)
{
  UtString subordinateStr(subordinateName);
  UtString masterStr(masterName);

  // first look in the processed options
  CmdLineArg * masterArg = lookupOption(masterStr, NULL);
  if ( masterArg ){
    OptionTable::const_iterator s = mOptionTable.find(subordinateStr);
    INFO_ASSERT(s == mOptionTable.end(), subordinateName);
    mOptionTable[subordinateStr] = masterArg;
    if (! subordIsDeprecated) {
      masterArg->addValidSynonym(subordinateName);
    } else {
      masterArg->addDeprecatedSynonym(subordinateName);
    }
    return;
  }
  // now check the unprocessed options
  masterArg = lookupUnprocessed(masterStr);

  if ( masterArg ) {
    // First check to see if this matched a wildcard option,
    // we do not allow synonyms to wildcard options because there is
    // no simple way to extract the master option string from the
    // wildcard string, and so we will not be able to determine what
    // option string should be passed to the sub process.
    bool matched_wildcard_option = (NULL != masterArg->castBoolWildcardArg());
    if ( matched_wildcard_option ){
      masterArg = NULL;
    }
  }

  if ( masterArg ) {
    OptionTable::const_iterator s = mUnprocessed.find(subordinateStr);
    INFO_ASSERT(s == mUnprocessed.end(), subordinateName);
    mUnprocessed[subordinateStr] = masterArg;
    if (! subordIsDeprecated) {
      masterArg->addValidSynonym(subordinateName);
    } else {
      masterArg->addDeprecatedSynonym(subordinateName);
    }
    return;
  }
  // no support for synonym for unprocessed bool wildcards

  // if we get here then the master was not found in processed or unprocessed options
  INFO_ASSERT(false, subordinateName);
}

void ArgProc::addSideEffect(const char* masterName, const char* sideEffectString)
{
  UtString masterStr(masterName);
  CmdLineArg * masterArg = lookupOption(masterStr, NULL);
  if (! masterArg) {
    masterArg = lookupUnprocessed(masterStr);     // not a known option. Maybe an unprocessed one?
  }
  INFO_ASSERT(masterArg, masterName);
  masterArg->putSideEffect(sideEffectString);
}


void ArgProc::putIsDeprecated(const char* option, bool deprecated)
{
  UtString masterStr(option);
  CmdLineArg * masterArg = lookupOption(masterStr, NULL);
  if (! masterArg)
    // not a known option. Maybe an unprocessed one?
    masterArg = lookupUnprocessed(masterStr);
  INFO_ASSERT(masterArg, option);
  
  masterArg->putIsDeprecated(deprecated);
}

void ArgProc::setIsAllowedOnlyInEFFile(const char* option)
{
  UtString masterStr(option);
  CmdLineArg * masterArg = lookupOption(masterStr, NULL);
  if (! masterArg) {
    masterArg = lookupUnprocessed(masterStr);    // unknown option. Perhaps unprocessed?
  }
  INFO_ASSERT(masterArg, option);
  
  masterArg->setIsAllowedOnlyInEFFile(); // mark for .ef only
  allowUsage(option);                    // and allow even if not in a section
}

ArgProc::CmdLineArg* ArgProc::lookupUnprocessed(const UtString& optionName)
{
  const ArgProc* me = const_cast<const ArgProc*>(this);
  return const_cast<CmdLineArg*>(me->lookupUnprocessed(optionName));
}

const ArgProc::CmdLineArg* 
ArgProc::lookupUnprocessed(const UtString& optionName) const
{
  const CmdLineArg* arg = NULL;
  OptionTable::const_iterator p =
    mUnprocessed.find(optionName);
  
  if (p != mUnprocessed.end())
    arg = p->second;
  else 
  {
    // check the list of wildcarded unprocessed options.
    for (OptionVectorCLoop p(mUnprocessedWildcardOptions); (arg == NULL) && ! p.atEnd(); ++p)
    {
      CmdLineArg* tmpArg = *p;
      // only deal with unprocessed bools currently
      BoolWildcardArg* wcCmdLineArg = tmpArg->castBoolWildcardArg();
      INFO_ASSERT(wcCmdLineArg, optionName.c_str());
      
      UtWildcard wc(wcCmdLineArg->getWildcardExpr());
      if (wc.isMatch(optionName.c_str()))
        arg = tmpArg;
    }
  }
  return arg;
}

ArgProc::CmdLineArg* 
ArgProc::lookupOption(const UtString& optionName, const char** scrunchedOption)
{
  const ArgProc* me = const_cast<const ArgProc*>(this);
  const CmdLineArg* option = me->lookupOption(optionName, scrunchedOption);
  return const_cast<CmdLineArg*>(option);
}

const ArgProc::CmdLineArg* 
ArgProc::lookupOption(const UtString& optionName, const char** scrunchedOption) const
{
  const CmdLineArg* arg = NULL;
  OptionTable::const_iterator p =
    mOptionTable.find(optionName);

  if (scrunchedOption)
    *scrunchedOption = NULL;
  
  if (p != mOptionTable.end())
  {
    arg = p->second;
  }
  else if (scrunchedOption && ! optionName.empty())
  {
    // we may have a single char option with an argument
    UtString::const_iterator s = optionName.begin();
    bool go = true;
    bool gotOpt = false;

    while (go && (s != optionName.end()))
    {
      size_t dist = std::distance(optionName.begin(), s);
      if (gotOpt)
      {
        go = false;
        if (isdigit(*s) || (*s == 's')) // special case -Os
        {
          // worth looking up.
          const UtString tmp = *scrunchedOption;
          p = mOptionTable.find(tmp);
          if (p != mOptionTable.end())
          {
            arg = p->second;
            mSubstrBuf = optionName.substr(dist, optionName.size() - dist);
            *scrunchedOption = mSubstrBuf.c_str();
          }
          else
            *scrunchedOption = NULL;
        }
      }
      else if ((dist == 0) && (*s == '-'))
        ;
      else if ((dist < 2) && (isalpha(*s)))
      {
        mSubstrBuf = optionName.substr(0, dist + 1);
        *scrunchedOption = mSubstrBuf.c_str();
        gotOpt = true;
      }
      else
        go = false;
      
      ++s;
    }
  }
  return arg;
}

ArgProc::Section* ArgProc::lookupSection(const UtString& sectionName)
{
  Section* section = NULL;
  SectionTable::iterator p = mSectionTable.find(sectionName);
  if (p != mSectionTable.end())
    section = p->second;
  return section;
}

ArgProc::ParseStatusT 
ArgProc::isParsed(const char* optionName) const
{
  ParseStatusT status = eParseError;
  UtString optStr(optionName);
  const CmdLineArg* arg = lookupOption(optStr, NULL);
  if (! arg)
  {
    // check the unprocessed option table
    arg = lookupUnprocessed(optStr);
  }

  if (arg)
    status = arg->isParsed();

  return status;
}

ArgProc::OptionStateT 
ArgProc::getUsage(const char* optName, 
                  UtString* usage, 
                  SInt32 margin) const
{
  OptionStateT state = eUnknown;
  LineWrapBuf lwBuf(margin);
  const CmdLineArg* optionArg = lookupOption(optName, NULL);
  if (optionArg && ((! optionArg->isDeprecated() && optionArg->getSection() != NULL) || mAccessHiddenOptions))
  {
    state = eKnown;
    
    lwBuf.addNewline();
    writeUsageToBuf(optionArg, &lwBuf);
    lwBuf.beginSubSection();
    lwBuf.addStr(optionArg->getDoc());
    lwBuf.endSubSection();
    lwBuf.addNewline();

    usage->erase();
    usage->append(lwBuf.getBuffer());
  }
  return state;
}


void ArgProc::getUsageVerbose(UtString* usage, bool noInternalOptions, SInt32 margin)
  const
{
  LineWrapBuf lwBuf(margin);

  if (! mTitle.empty())
    lwBuf.centerTitle(mTitle);
  lwBuf.addNewline();

  if (! mName.empty())
  {
    lwBuf.addStr("NAME");
    lwBuf.indent();
    lwBuf.addNewline();
    lwBuf.addStr(mName);
    lwBuf.unindent();
    lwBuf.addNewline();
    lwBuf.addNewline();    
  }

  addSynopsisToBuf(&lwBuf);

  if (! mDescription.empty())
  {
    lwBuf.addStr("DESCRIPTION");
    lwBuf.indent();
    lwBuf.addNewline();
    lwBuf.addStr(mDescription);
    lwBuf.unindent();
    lwBuf.addNewline();
    lwBuf.addNewline();
  }

  lwBuf.addStr("OPTIONS");
  lwBuf.addNewline();
  lwBuf.indent();
  lwBuf.addNewline();


  for (SectionVector::const_iterator p = mSections.begin(); 
       p != mSections.end(); ++p)
  {
    Section* sect = *p;
    if (sect->hasOptions())
    {
      sect->sortOptions();
      lwBuf.addStr(sect->getName());
      lwBuf.addNewline();
      
      lwBuf.beginSubSection();

      CmdLineArg* arg = NULL;    
      Section::OptionCIter loop = sect->cLoopOptions();
      while(loop(&arg))
      {
        if (! arg->isDeprecated() || mAccessHiddenOptions)
        {
          writeUsageToBuf(arg, &lwBuf);
          lwBuf.beginSubSection();
          lwBuf.addStr(arg->getDoc());
          lwBuf.endSubSection();
          lwBuf.addNewline();
        }
      }
      
      lwBuf.endSubSection();
      lwBuf.addNewline();
    }
  }

  if (mAccessHiddenOptions && ! noInternalOptions)
  {

    typedef UtArray<const CmdLineArg*> OptionList;
    OptionList internalOptions;
    for (OptionVector::const_iterator ip = mOptions.begin();
         ip != mOptions.end(); ++ip)
    {
      const CmdLineArg* privOpt = (*ip);
      if (privOpt->getSection() == NULL)
        internalOptions.push_back(privOpt);
    }
    
    if (! internalOptions.empty())
    {
      std::sort(internalOptions.begin(), internalOptions.end(), ArgProc::OptionCmp());
      
      lwBuf.addStr("INTERNAL");
      lwBuf.addNewline();
      lwBuf.beginSubSection();
      
      const CmdLineArg* arg = NULL;    
      for(OptionList::const_iterator optIter = internalOptions.begin();
          optIter != internalOptions.end(); ++optIter)
      {
        arg = *optIter;
        writeUsageToBuf(arg, &lwBuf);
        lwBuf.beginSubSection();
        lwBuf.addStr(arg->getDoc());
        lwBuf.endSubSection();
        lwBuf.addNewline();
      }
      
      lwBuf.endSubSection();
      lwBuf.addNewline();
    }
  }
  
  usage->erase();
  usage->append(lwBuf.getBuffer());
}

void ArgProc::addSynopsisToBuf(LineWrapBuf* lwBuf) const
{
  if (! mSynopses.empty())
  {
    lwBuf->addStr("SYNOPSIS");
    lwBuf->indent();
    lwBuf->addNewline();
    
    UtStringArray::const_iterator p = mSynopses.begin();
    while(p != mSynopses.end())
    {
      lwBuf->addStr(*p);
      ++p;
      lwBuf->addNewline();
      lwBuf->addNewline();
    }

    lwBuf->unindent();
    lwBuf->addNewline();
  }

}

void ArgProc::getSynopsis(UtString* usage, SInt32 margin) const
{
  LineWrapBuf lwBuf(margin);
  addSynopsisToBuf(&lwBuf);
  usage->erase();
  usage->append(lwBuf.getBuffer());
}

void ArgProc::setParseErrMsg(UtString* str,
                             CmdLineArg* option, 
                             const UtString& optionName)
{
  *str << "Command parse error: expected "
       << option->getTypeDescription()
       << " after option "
       << optionName << '\n';
  option->setIsParsed(eParseError);
}

//! Remove the specified number of args from the supplied array
static void
removeArgs( int *argc, char **argv, int arg_start_index, int n_to_remove )
{
  int j;
  int numArgs = *argc - n_to_remove;
  
  for (j = arg_start_index; j < numArgs; ++j)
    argv[j] = argv[j + n_to_remove];

  *argc -= n_to_remove;
  argv[*argc] = NULL;
}



// This function processes the -ef switch.
// If there is an -ef switch no -f switches are allowed.
ArgProc::ParseStatusT
ArgProc::preParseCommandLineEF(int argc, 
			       char* const* argv,
                               UInt32 num_allowed_ef,                               
                               unsigned int* index,  // index of the -ef switch
                               UtString* errMsg)
{
  unsigned int numberOther = 0;  // number of other switches
  unsigned int numberEF = 0;     // number of -ef switches
  unsigned int index_ef = 0;     // -ef switch index

  INFO_ASSERT((num_allowed_ef<=1), "A maximum of 1 -ef option is supported."); // support for multiple -ef would require extensive changes

  // count the nuber of -ef options used
  ParseStatusT status = eNotParsed;
  for (int i = 0; i < argc; ++i)
  {
    if ( strcmp(argv[i], "-ef") == 0 )
    {
      numberEF++;
      index_ef = i;
    }
  }

  if ( num_allowed_ef < numberEF ) {
    status = eParseError;
    (*errMsg) << "Only "  << num_allowed_ef << " -ef option" << ((num_allowed_ef != 1) ? "s":"") << " allowed. (Do you have nested -ef?)";
  } else if ( numberEF > 0 ) {
    // count the number of other options
    for (int i = 0; i < argc; ++i) {
      if(argv[i][0] ==  '+') {
        numberOther++;
      } else if(argv[i][0] ==  '-') {
        // the options -vcp, -novspcc and -vspCompileFileFlag
        // are added by cbuild script, exclude them.
        // the option backendCompileOnly is added, when -q is added,
        // exclude it as well
        if((strcmp(argv[i], "-q") != 0) &&
           (strcmp(argv[i], "-backendCompileOnly") != 0) &&
           (strcmp(argv[i], "-profileGenerate") != 0) &&
           (strcmp(argv[i], "-profileUse") != 0) &&
           (strcmp(argv[i], "-vsp") != 0) &&
           (strcmp(argv[i], "-novspcc") != 0) &&
           (strcmp(argv[i], "-vspCompileFileFlag") != 0) &&
           (strcmp(argv[i], "-ef") != 0)         // skip over the allowed -ef  
          ) {
          numberOther++;
        }
      }
    }

    if ( numberOther != 0 ) {
      status = eParseError;
      errMsg->append("no other option, is allowed with the -ef option."); // this is not quite true, but the message will only
                                                                          // be printed when an option that is not allowed was
                                                                          // included with -ef
    } else if(numberEF == 1) {
      *index = index_ef;
      status = eParsed;
    }
  }
  return status;
}

UInt32 ArgProc::countStringMatches(int argc, char** const argv, const char* str, bool expandDashFArgs, ParseStatusT *status )
{
  // this routine has no concept of not expanding through -f args, so make sure caller is aware that it
  // acts like it always expands through -f args.
  INFO_ASSERT(expandDashFArgs, "This routine only supports the automatic expansion of -f options.");

  *status = eParsed;
  UInt32 count = 0;
  
  for (int i = 0; i < argc; ++i)
  {
    if ( strcmp(argv[i], str) == 0 ) {
      count++;
    }
  }

  return count;
}

UInt32 ArgProc::countStringMatches(const UtString& buffer, const char* str, bool expandDashFArgs, ParseStatusT *status)
{
  *status = eParsed;
  UInt32 count = 0;

  UtIStringStream iss(buffer);
  UtString line;
  while(iss.getline(&line) && *status == eParsed)
  {
    UtStringArgv token_argv;
    UtString errMsg;
    *status = tokenizeArgString(line.getBuffer(), &token_argv, &errMsg, expandDashFArgs);
    if(*status == eParsed)
    {
      char* const* argv_buf =  token_argv.getArgv();
      for(int i = 0; i < token_argv.getArgc(); i++)
      {
        if ( strcmp(argv_buf[i], str) == 0) {
          count++;
        }
      }
    }
  }

  return count;
}

ArgProc::ParseStatusT
ArgProc::replaceArgvForEF(int argc, char* const* argv, 
                          unsigned int index_ef,
                          UtString* ef_context,
                          UtStringArgv* resolvedCmdLine,
                          UtString* errMsg)
{
  ParseStatusT status = eParsed;

  // First, remove the -ef switch and ef file from argv
  int index_temp = index_ef;
  for (int i = 0; i < argc; ++i)
  {
    if((i != index_temp) && (i != index_temp+1))
    {
      resolvedCmdLine->push_back(argv[i]);
    }
  }

  UtIStringStream iss(*ef_context);
  UtString line;
  while(iss.getline(&line) && status == eParsed)
  {
    UtStringArgv token_argv;
    status = tokenizeArgString(line.getBuffer(), &token_argv, errMsg);
    char* const* argv_ef =  token_argv.getArgv();
    for(int i = 0; i < token_argv.getArgc(); i++)
    {
      resolvedCmdLine->push_back(argv_ef[i]);
    }
  }

  return status;
}

ArgProc::ParseStatusT
ArgProc::preParseCommandLine(int argc, 
			     char* const* argv,
                             UtStringArgv* resolvedCmdLine,
                             UtString* errMsg,
			     bool expandEnvVars)
{
  ParseStatusT status = eParsed;

  for (int i = 0; (i < argc) && (status == eParsed); ++i)
  {
    int curArgvIndex = i;       // keep track of the index used to get switch from argv

    if (isInputArgFile(argv[i]))
    {
      if (i < argc - 1)
      {
        bool nextArgIsFile = false;
        status = includeArgFile(argv[i + 1], resolvedCmdLine, errMsg, &nextArgIsFile, expandEnvVars);
        ++i;
      }
    }
    else {
      resolvedCmdLine->push_back(argv[i]);
    }

    // now handle any side effect additions
    const char* sideEffect = getSideEffect(argv[curArgvIndex]);
    if ( sideEffect ){
      resolvedCmdLine->push_back(sideEffect);
      // NOTE: currently this only supports the case where sideEffect is a single switch
      // to support chained side effect switches,
      // (where -a causes -b to be added and then -b being used causes -c to be added)
      // or to support multiple side effect switches (where -a causes "-b -c -d" to be added)
      // we probably want to replace the resolvedCmdLine->push-back(sideEffect) above with
      // the recursive call:
      // preParseCommandLine(count_of_switches_in_sideEffect, sideEffect, resolvedCmdLine, errMsg, expandEnvVars);
    }
  }
  return status;
}

bool ArgProc::passMatches(CmdLineArg* option) const {
  return (option->getPassNum() == mPass) ||
    (option->getPassNum() == cAllPasses);
}

ArgProc::ParseStatusT
ArgProc::parseCommandLine(int* argc, char** argv, int* numOptions,
                          UtString* errMsg, FileCollector *callback,
			  int* unexpandedArgc, char** unexpandedArgv)
{
  INFO_ASSERT(argc, "NULL argc.");
  INFO_ASSERT(argv, "NULL argv.");
  INFO_ASSERT(numOptions, "NULL option counter.");
  INFO_ASSERT(errMsg, "NULL error string.");

  *numOptions = 0;
  ParseStatusT status = eNotParsed;
  CmdLineArg* option = NULL;

  errMsg->clear();
  
  UtString msgTmp;

  ++mPass;

  // Table to keep track of bool override options we come across
  // We don't store a pointer in the bool option that is overridden.
  // That would make boolean options effectively 4 bytes larger 
  OptionTable boolOverrides;
  
  for (int i = 1; i < *argc; ++i)
  {
    bool rmArgs = true;
    const char* scrArg = NULL;
    UtString tmp(argv[i]);
    option = lookupOption(tmp, &scrArg);
    bool isRestrictedOption = option && ! option->isAllowed();
    bool isAllowedOption = option && (option->isAllowed() || (isRestrictedOption && mAccessHiddenOptions));
    int n_to_remove, arg_start_index = i;
    bool isBoolType = option && ((option->getType() == eBool) ||
                                 (option->getType() == eBoolOverride));

    if (option)
    {
      INFO_ASSERT(! option->isInputArgFile(), tmp.c_str());
      
      if (! passMatches(option)) {
        if (! isBoolType && scrArg == NULL) {
          ++i;
        }
        continue;
      }
    
      bool isDeprecatedValidOption = option->isDeprecated();
      bool isDeprecatedSynonym = false;
      if (isDeprecatedValidOption || 
          (isDeprecatedSynonym = option->isDeprecatedSynonym(tmp)))
      {
        msgTmp << "Option (" << tmp << ") is deprecated.";

        
        if (isDeprecatedSynonym && isAllowedOption)
          msgTmp <<  " Use (" << option->getName() << ") instead.";
        msgTmp << "\n";
        
        if (status != eParseError)
          status = eParseWarning;
      }
    }
    
    if (isAllowedOption)
    {
      // At this point we do NOT have an unprocessable option.

      if (option->allowsMultiple() || (option->isParsed() != eParsed))
        (*numOptions)++;
      
      OptionTypeT optType = option->getType();
      UtString overrideMsg;

      // boolean? If not, better be another arg
      if (! isBoolType && (i + 1 >= *argc) && ( NULL == scrArg ) )
      {
        setParseErrMsg(&msgTmp, option, argv[i]);
        status = eParseError;
      }
      else
      {
        SInt32 intNum;
        double dblNum;
        bool unconvertible = false;
        UtString tmpStream;
        char* filePath;
	char* unexpandedFilePath;
        OutFileCmdLineArg* outFileArg;
        StrCmdLineArg* strArg;

        switch (optType) 
        {
        case eBoolOverride: {
          BoolOverrideArg* bo = option->castBoolOverrideArg();
          INFO_ASSERT(bo, tmp.c_str());
          bo->setOverrideOn(&overrideMsg);
          BoolCmdLineArg* b = bo->getOption();
          boolOverrides[b->getName()] = bo;
          break;
        }
        case eBool: {
          BoolCmdLineArg* b = option->castBoolCmdLineArg();
          INFO_ASSERT(b, tmp.c_str());

          if (! b->hasOverride())
            b->addValue(! b->getDefault(), NULL);
          else
          {
            OptionTable::iterator p = boolOverrides.find(b->getName());
            if (p != boolOverrides.end())
            {
              if (b->getValue() != b->getDefault())
              {
                // The value has been overridden and now we are
                // unoverriding it.
                CmdLineArg* tmpOverride = p->second;
                BoolOverrideArg* bo = tmpOverride->castBoolOverrideArg();
                INFO_ASSERT(bo, tmp.c_str());
                bo->setOverrideOff(&overrideMsg);
              }
            }
            else
            {
              // If it is an overridden option that has not yet been
              // overridden, the value better be the default.
              INFO_ASSERT(b->getValue() == b->getDefault(), tmp.c_str());
            }
          }
          break;
        }
        case eString: 
          // Unfortunately, char* can be interpreted as bools
          strArg = option->castStrCmdLineArg();
          INFO_ASSERT(strArg, tmp.c_str());
          if ( NULL == scrArg ) {
            scrArg = argv[++i];
	  }
          strArg->addValue(scrArg, &overrideMsg);

          break;
        case eInt: {
          if ( NULL == scrArg ) {
            scrArg =  argv[++i];
          }
          int base = 10;
          if ( strncasecmp(scrArg, "0x", 2) == 0 ) {
            scrArg +=2;
            base = 16;
          }
          if (StringUtil::parseNumber(scrArg, &intNum, base)) {
            option->addValue(intNum, &overrideMsg);
          } else {
            unconvertible = true;
          }
          break;
        }
        case eDouble:
          if ( NULL == scrArg )
            scrArg =  argv[++i];
          if (StringUtil::parseNumber(scrArg, &dblNum))
            option->addValue(dblNum, &overrideMsg);
          else
            unconvertible = true;
          break;
        case eInFile: 
          // Unfortunately, char* can be interpreted as bools
          filePath = argv[++i];
          option->castInFileCmdLineArg()->addValue(filePath, &overrideMsg);
	  if (unexpandedArgv != NULL) {
	    unexpandedFilePath = unexpandedArgv[i];
	    option->castInFileCmdLineArg()->addUnexpandedValue(unexpandedFilePath);
	  }
          if (! checkInputPath(filePath, &msgTmp))
            status = eParseError;
          break;
        case eOutFile: 
          // Unfortunately, char* can be interpreted as bools
          filePath = argv[++i];
          outFileArg = option->castOutFileCmdLineArg();
          outFileArg->addValue(filePath, &overrideMsg); 
	  if (unexpandedArgv != NULL) {
	    unexpandedFilePath = unexpandedArgv[i];
	    outFileArg->addUnexpandedValue(unexpandedFilePath);
	  }
          if (! checkOutputPath(filePath, outFileArg, &msgTmp)) 
            status = eParseError;
          break;
        } // switch
        
        if (unconvertible)
        {
          setParseErrMsg(&msgTmp, option, option->getName());
          status = eParseError;
        }
      }
      
      if (mWarnOnDuplicates && ! overrideMsg.empty()) {
        msgTmp << overrideMsg;
        
        // never supercede an error
        if (status != eParseError) 
          status = eParseWarning;
      }
      
      // callback with the parsed arg and its option (if it exists)
      if ( callback != NULL ) {
        if ( option->getType() == eBool || option->getType() == eBoolOverride )
        {
          callback->scanArgument( argv[i], NULL );
        }
        else
        {
          callback->scanArgument( argv[i - 1], argv[i] );
        }
      }
      
      if (option)
        option->setIsParsed(eParsed);
      
    } // if
    else if (isRestrictedOption)
    {
      // we do not have an unprocessable option, but the option we
      // have is not allowed to be used.
      
      status = eParseError;
      option->setIsParsed(eParseError);
      msgTmp << "Option (" << option->getName() << ") is not allowed.\n";
      (*numOptions)++;
      
      // bypass the argument, if any
      if (! isBoolType && ( NULL == scrArg ) )
        ++i;
    }
    else
    {
      //See if it's an unprocessed argument
      option = lookupUnprocessed( tmp );
      // Unprocessed options are never hidden
      if ( option )
      { 
        if (passMatches(option)) {
          UnprocessedGroup *group = option->getUnprocessedGroup();
          OptionTypeT unprocessedOptType = option->getType();
          switch (unprocessedOptType)
          {
          case eBool: {
            bool matched_wildcard_option = (NULL != option->castBoolWildcardArg());
            // in the following we get/use the name from the option class
            // (instead of using argv[i]) in case the user specified a
            // synonym. (unless this is a wildcard option where we
            // just use the string provided -- there is no support for
            // synonyms for wildcard options)
            const char* option_name = matched_wildcard_option ? tmp.c_str() : option->getName().c_str();
            group->addOccurrence( option_name, unprocessedOptType, option );
            break;
          }
          case eString:
            group->addOccurrence( option->getName().c_str(), unprocessedOptType, option );
            // if this is not true, it's an error that should be caught
            // later on down the road.  We can't throw an error here
            // because we don't have a message context available.
            if ( i + 1 < *argc )
            {
              group->addOccurrence( argv[++i], unprocessedOptType, NULL );
            }
            break;
          default:
            msgTmp << "Unknown message type (" << option->getType()
                   << ") for option" << argv[i] << ".  Ignoring option.\n";
            // never supercede an error
            if (status != eParseError) 
              status = eParseWarning;
            break;
          }
        }
        else 
          rmArgs = false;
      }
      else if ( *argv[i] == '+' ) 
      {
        // Remove these in any pass. There is no add method for
        // plusargs. Those are just handled special here.

        // All arguments that start with '+', even ones that are not
        // explicitly recognized, are passed to Cheetah
        UnprocessedGroup *group = lookupUnprocessedGroup( "Verilog" );
        if (group)
          group->addOccurrence( argv[i], eBool, NULL );
      }
      else
      {
        // callback with the unrecognized and ignored argument
        if ( callback != NULL )
        {
          bool consumed = callback->scanArgument( argv[i], NULL );
          // consumed is set if the callback handles the argument completely
          if ( ! consumed )
          {
            // Add this argument to the callback object
            callback->addArgument( argv[i] );
          }
        }
        else
          rmArgs = false;
      }
    }
    
    if (rmArgs)
    {
      n_to_remove = i - arg_start_index + 1;
      removeArgs( argc, argv, arg_start_index, n_to_remove );
      if (unexpandedArgc != NULL && unexpandedArgv != NULL) {
	removeArgs( unexpandedArgc, unexpandedArgv, arg_start_index, n_to_remove );
      }
      i -= n_to_remove;
    }
  } // for
    
  // status begins as eNotParsed
  if ((*numOptions > 0) && (status != eParseError) && (status != eParseWarning))
    status = eParsed;
  else if (*numOptions == 0)
    status = eParsed; // no options specified
  if ((status == eParseError) || (status == eParseWarning))
    *errMsg << msgTmp;
  return status;
}

//! Does this option valid, & does it require a value?
bool ArgProc::isValid(const char* optionName, bool* requiresValue,
                      bool *isInputFile,
                      bool *isOutputFile) const
{
  const CmdLineArg* option = lookupOption(optionName, NULL);
  if (option == NULL)
  {
    *requiresValue = false;
    *isInputFile = false;
    *isOutputFile = false;
    return false;
  }
  OptionTypeT type = option->getType();
  *requiresValue = (type != eBool) && (type != eBoolOverride);
  *isInputFile = (type == eInFile);
  *isOutputFile = (type == eOutFile);
  return true;
}

ArgProc::OptionStateT 
ArgProc::getIntIter(const char* optionName, 
                    IntIter* iter) const
{
  OptionStateT state = eUnknown;
  const CmdLineArg* option;
  if ((option = lookupOption(optionName, NULL)))
  {
    state = eKnown;
    if (option->getType() == eInt)
    {
      const IntCmdLineArg* intOption = option->castIntCmdLineArg();
      INFO_ASSERT(intOption, optionName);
      *iter = intOption->getIntIter();
    }
    else
      state = eTypeMismatch;
  }
  return state;
}

ArgProc::OptionStateT 
ArgProc::getIntFirst(const char* optionName, SInt32 *value) const
{
  OptionStateT state = eUnknown;
  ArgProc::IntIter i;
  getIntIter (optionName, &i);

  if (!i.atEnd()) {
    state = eKnown;
    *value = *i;
  }

  return state;
}

ArgProc::OptionStateT 
ArgProc::getIntLast(const char* optionName, SInt32 *value) const
{
  OptionStateT state = eUnknown;
  ArgProc::IntIter i;

  for (getIntIter (optionName, &i); !i.atEnd(); ++i)
  {
    state = eKnown;
    *value = *i;
  }

  return state;
}


ArgProc::OptionStateT 
ArgProc::setIntValue(const char* optionName, SInt32 value)
{
  OptionStateT state = eUnknown;
  CmdLineArg* option;
  if ((option = lookupOption(optionName, NULL)))
  {
    state = eKnown;
    if (option->getType() == eInt)
    {
      IntCmdLineArg* intOption = option->castIntCmdLineArg();
      INFO_ASSERT(intOption, optionName);
      intOption->addValue(value, NULL);
    }
    else
    {
      state = eTypeMismatch;
    }
  }
  return state;
}


ArgProc::OptionStateT 
ArgProc::getDoubleIter(const char* optionName, 
                       DblIter* iter) const
{
  OptionStateT state = eUnknown;
  const CmdLineArg* option;
  if ((option = lookupOption(optionName, NULL)))
  {
    state = eKnown;
    if (option->getType() == eDouble)
    {
      const DoubleCmdLineArg* dblOption = option->castDoubleCmdLineArg();
      INFO_ASSERT(dblOption, optionName);
      *iter = dblOption->getDblIter();
    }
    else
      state = eTypeMismatch;
  }
  return state;
}

ArgProc::OptionStateT
ArgProc::getDoubleFirst(const char* optionName, 
                        double* val) const
{
  OptionStateT state = eUnknown;
  ArgProc::DblIter i;

  for (getDoubleIter (optionName, &i); !i.atEnd(); ++i)
  {
    state = eKnown;
    *val = *i;
  }

  return state;
}

ArgProc::OptionStateT 
ArgProc::getStrIter(const char* optionName, 
                    StrIter* iter) const
{
  OptionStateT state = eUnknown;
  const CmdLineArg* option;
  if ((option = lookupOption(optionName, NULL)))
  {
    state = eKnown;
    if ((option->getType() == eString) || 
        (option->getType() == eInFile) ||
        (option->getType() == eOutFile))
    {
      const StrCmdLineArg* strOption = option->castStrCmdLineArg();
      INFO_ASSERT(strOption, optionName);
      *iter = strOption->getStrIter();
    }
    else
      state = eTypeMismatch;
  }
  return state;
}

ArgProc::OptionStateT 
ArgProc::getUnexpandedStrIter(const char* optionName, 
			      StrIter* iter) const
{
  OptionStateT state = eUnknown;
  const CmdLineArg* option;
  if ((option = lookupOption(optionName, NULL)))
  {
    state = eKnown;
    if ((option->getType() == eString) || 
        (option->getType() == eInFile) ||
        (option->getType() == eOutFile))
    {
      const StrCmdLineArg* strOption = option->castStrCmdLineArg();
      INFO_ASSERT(strOption, optionName);
      *iter = strOption->getUnexpandedStrIter();
    }
    else
      state = eTypeMismatch;
  }
  return state;
}

ArgProc::OptionStateT 
ArgProc::getBoolValue(const char* optionName, 
                      bool* value) const
{
  OptionStateT state = eUnknown;
  const CmdLineArg* option;
  if ((option = lookupOption(optionName, NULL)))
  {
    state = eKnown;
    if (option->getType() == eBool)
    {
      const BoolCmdLineArg* boolOption = option->castBoolCmdLineArg();
      INFO_ASSERT(boolOption, optionName);
      *value = boolOption->getValue();
    }
    else if (option->getType() == eBoolOverride)
    {
      const BoolOverrideArg* overrideOption = option->castBoolOverrideArg();
      INFO_ASSERT(overrideOption, optionName);
      *value = overrideOption->getValue();
    }
    else
      state = eTypeMismatch;
  }
  return state;
}

ArgProc::OptionStateT 
ArgProc::setBoolValue(const char* optionName, bool value)
{
  OptionStateT state = eUnknown;
  CmdLineArg* option;
  if ((option = lookupOption(optionName, NULL)))
  {
    state = eKnown;
    if (option->getType() == eBool)
    {
      BoolCmdLineArg* boolOption = option->castBoolCmdLineArg();
      INFO_ASSERT(boolOption, optionName);
      boolOption->addValue(value, NULL);
    }
    else if (option->getType() == eBoolOverride)
    {
      BoolOverrideArg* overrideOption = option->castBoolOverrideArg();
      INFO_ASSERT(overrideOption, optionName);
      overrideOption->addValue(not value, NULL);
    }
    else
      state = eTypeMismatch;
  }
  return state;
}

bool ArgProc::checkInputPath(const char* inPath, UtString* str)
{
  UtString result;

  int ret = OSStatFile(inPath, "ef", &result);
#if pfUNIX
  if ((ret < 0) && (errno == EOVERFLOW)) {
    // If the file is >2gig in length then stat fails.  open
    // the file and see if that works.  I suppose this might
    // yield a false positive for a directory of size > 2gig.
    UtIBStream ib(inPath);
    ret = ib.is_open();
    if (!ret) {
      *str << ib.getErrmsg();
    }
  }
  else
#endif
  if (ret < 0)
    *str << result;
  else if (ret == 0)
  {
    INFO_ASSERT(result.size() == 2, inPath);
    if (result[0] == '0')
      *str << "'" << inPath << "' No such file or directory\n";
    else {
      if (result[1] == '0')
        *str << "'" << inPath << "' is not a regular file\n";
    }
  }
  else
  {
    // open the file to check readability.
    FILE* f = OSFOpen(inPath, "r", NULL);
    if (f)
      fclose(f);
    else
    {
      ret = -1;
      *str << "'" << inPath << "' does not have read permission\n" ;
    }
  }

  return (ret == 1);
}

bool ArgProc::checkOutputPath(const char* outPath, 
                              OutFileCmdLineArg* outFileArg,
                              UtString* str)
{
  bool isGood = true;
  UtString result;
  UtString file;
  UtString dir;
  FileTypeT fileType = outFileArg->getFileType();

  OSParseFileName(outPath, &dir, &file);

  if (dir.empty())
    dir = ".";

  // If we are trying to write a regular file, then the directory
  // should already exist and be writable
  if (fileType == eRegFile) {
    int dirStat = OSStatFile(dir.c_str(), "edw", &result);
    if (dirStat != 1) {
      *str << "Directory `" << dir << "' is not writable\n";
      return false;
    }
  }

  // Make sure we stat the file with its directory.
  OSConstructFilePath(&file, dir.c_str(), file.c_str());
  
  int ret = OSStatFile(file.c_str(), "efdw", &result);
  INFO_ASSERT(ret != 1, file.c_str()); // impossible 

  if (ret < 0)
  {
    isGood = false;
    *str << result;
  }
  else 
  {
    INFO_ASSERT(result.size() == 4, file.c_str());
    // if it doesn't exist that is ok, for a file
    if ((result[0] == '0') && (fileType == eDirExist)) {
      UtString& tmp = dir;
      if (dir.empty())
        tmp = file;
      *str << tmp << "' does not exist.\n";
      isGood = false;
    }
    
    if ((result[0] == '1') && (result[3] == '0'))
    {
      isGood = false;
      if (result[2] == '1')
        *str << "Directory ";
      
      *str << "'" << file << "' does not have write permission.\n";
    }
      
    if ((fileType == eRegFile) && (result[0] == '1') && (result[1] == '0') && (result[2] == '0'))
    {
      isGood = false;
      *str << "'" << file << "' is not a regular file.\n";
    }
    
    if (isGood)
    {
      if ((fileType == eRegFile) && (result[2] == '1'))
      {
        isGood = false;
        *str << "'" << file << "' is a directory, expected a file name.\n";
      }
      if ((fileType == eDirExist) && (result[2] == '0'))
      {
        isGood = false;
        *str << "'" << file <<"' is not a directory.\n";
      }
    }
  }
  return isGood;
}

ArgProc::OptionStateT ArgProc::getStrValue(const char* optionName,
                                           const char** val)
  const
{
  StrIter iter;
  OptionStateT state = getStrIter(optionName, &iter);
  if (state == eKnown)
  {
    if (iter.atEnd())
      state = eTypeMismatch;
    else
    {
      *val = (*iter);
      ++iter;
      if (! iter.atEnd())
        state = eTypeMismatch;  // must have exactly 1 value
    }
  }
  return state;
}

const char* ArgProc::getStrValue(const char* optionName)
  const
{
  const char* val = NULL;
  if (getStrValue(optionName, &val) == eKnown)
    return val;
  return NULL;
}

const char* ArgProc::getStrLast(const char* optionName) const
{
  const char* val = NULL;
  StrIter iter;
  OptionStateT state = getStrIter(optionName, &iter);
  if (state == eKnown)
  {
    if (iter.atEnd())
      state = eTypeMismatch;
    else
    {
      while (! iter.atEnd())
      {
        val = *iter;
        ++iter;
      }
    }
  }
  return val;
}


void ArgProc::allowUsageAll()
{
  OptionTable::iterator e = mOptionTable.end();
  for (OptionTable::iterator p = mOptionTable.begin();
       p != e; ++p)
  {
    CmdLineArg* option = p->second;
    option->setIsAllowed();
  }
}

ArgProc::OptionStateT 
ArgProc::disallowUsage(const char* unsectionedOption)
{
  UtString optionStr(unsectionedOption);
  OptionStateT state = eUnknown;
  CmdLineArg* option = lookupOption(unsectionedOption, NULL);
  if (option)
  {
    state = eKnown;
    option->setDisallowed();
  }
  return state;
}

ArgProc::OptionStateT 
ArgProc::allowUsage(const char* unsectionedOption)
{
  UtString optionStr(unsectionedOption);
  OptionStateT state = eUnknown;
  CmdLineArg* option = lookupOption(unsectionedOption, NULL);
  if (option)
  {
    state = eKnown;
    option->setIsAllowed();
  }
  return state;
}


void ArgProc::putWarnOnDuplicates(bool warnOnDuplicates)
{
  mWarnOnDuplicates = warnOnDuplicates;
}

ArgProc::StrIter 
ArgProc::getMatchedArgs(const char* optionName)
{
  const CmdLineArg* option;
  UtString optStr(optionName);
  option = lookupUnprocessed(optStr);
  INFO_ASSERT(option, optionName);
  const BoolWildcardArg* wcArg = option->castBoolWildcardArg();
  INFO_ASSERT(wcArg, optionName);
  return wcArg->getMatches();
}

ArgProc::ParseStatusT 
ArgProc::includeArgFile(const char* file, 
			UtStringArgv* resolveCmdLine, 
                        UtString* errMsg, 
			bool* nextArgIsFile,
			bool expandEnvVars,
                        bool expandDashFArgs)
{
  UtIBStream f(file);
  if (!f.is_open()) {
    *errMsg = f.getErrmsg();
    return eParseError;
  }

  UtString buf;

  ParseStatusT status = eParsed;
  *nextArgIsFile = false;
  int inComment = 0;
  int lineNum = 1;
  UtString errBuf;
  while (f.getline(&buf))
  {
    errBuf.clear();
    errBuf << file << ": " << lineNum << ": ";
    ParseStatusT localStatus = parseString(buf.c_str(), resolveCmdLine, &errBuf, nextArgIsFile, &inComment, expandDashFArgs, expandEnvVars);
    if (localStatus > status)
    {
      status = localStatus;
      errMsg->clear();
      *errMsg << errBuf;
    }
    else if (localStatus != eParsed && localStatus == status)
    {
      *errMsg << errBuf << " ";
    }
    lineNum++;
  }

  if (status == eParsed)
  {
    if (inComment != 0)
    {
      *errMsg << file << ": Unterminated /*..*/ comment";
      return eParseError;
    }

    if (*nextArgIsFile)
    {
      *errMsg << file << ": -f without filename";
      return eParseError;
    }
  }


  return status;
} // bool CarbonContext::includeArgFile


ArgProc::ParseStatusT
ArgProc::parseString(const char* str, UtStringArgv* resolveCmdLine,
                     UtString* errMsg, bool* nextArgIsFile, int* inComment,
                     bool expandDashFArgs, bool expandEnvVars)
{
  ParseStatusT status = eParsed;
  for (UtShellTok tok(str, expandEnvVars); !tok.atEnd(); ++tok)
  {
    const char* arg = *tok;
    const char* s = tok.curPos();

    if (! tok.isProtected() && arg)
    {
      int numChars = 0;
      bool isNewline = false;
      const char* argTmp = arg;
      while ((*argTmp != '\0') && (numChars < 2))
      {
        ++numChars;
        if (*argTmp == '\n')
          isNewline = true;
        ++argTmp;
      }
      if (isNewline && (numChars == 1))
        continue;
      
      /* Don't care if we are in a comment or not. We are just
         ignoring backslash-newline occurrences
      */
    }
    
    if (((*inComment > 0) || ! tok.isProtected()) && 
        (strncmp(arg, "/*", 2) == 0))
    {
      ++(*inComment);
      s += 2;
    }

    UtShellTok::Status tokStatus = tok.getResetStrStatus();

    // determine if an error was encountered expanding environment variables
    if (tokStatus != UtShellTok::eOk)
    {
      // Ignore any error if we are in a comment
      if (! *inComment)
      {
	*errMsg << tok.getErrorBuffer();
	if (tokStatus == UtShellTok::eNote)
	{
	  status = eParseNote;
	}
	else if (tokStatus == UtShellTok::eWarning)
	{
	  status = eParseWarning;
	}
	else if (tokStatus == UtShellTok::eError) 
	{
	  return eParseError;
	}
      }

      tok.clearResetStrStatus();
    }

    if (*inComment != 0)
    {
      // The end of the comment might be on the same line. Also, look
      // out for the stupid /*...*/ trick. And always be on the
      // lookout for more /* on the line
      
      while ((*inComment != 0) && (*s != '\0'))
      {
        bool foundForward = false;
        while ((*s != '\0') && (*s != '*'))
        {
          foundForward = (*s == '/');
          ++s;
        }

        bool foundAsterick = false;        
        while ((*s != '\0') && (*s == '*'))
        {
          ++s;
          foundAsterick = true;
          if (foundForward)
          {
            ++(*inComment);
            foundForward = false;
            foundAsterick = false;
          }
        }
        
        if (foundAsterick && *s == '/')
        {
          --(*inComment);
          ++s; // pass the '/'
        }
      } 
      
      if (*inComment != 0)
        return eParsed; // didn't find end of comment
      else
      {
        // found end of comment. reset token can continue
        tok.resetStr(s);
        // ++ will be called next.
      }
        
    }
    else if (! tok.isProtected() && ((strncmp(arg, "//", 2) == 0) || (strncmp(arg, "#", 1) == 0))) {
      return eParsed;                   // ignore rest of line
    }
    else if (*nextArgIsFile) {
      if ( expandDashFArgs ) {
        // Make sure that any environment variables in the file name are expanded
        UtShellTok tok2(arg);
        arg = *tok2;
        ParseStatusT localStatus = includeArgFile(arg, resolveCmdLine, errMsg, nextArgIsFile, expandEnvVars);
        if (localStatus > status)
          status = localStatus;
      } else {
        // skip the processing of the -f file name
        *nextArgIsFile = false;
      }
    }
    else if (isInputArgFile(arg)) {
      *nextArgIsFile = true;
    }
    else {
      resolveCmdLine->push_back(arg);
      if ( expandDashFArgs ){
        // only apply side effect switches if we are expanding -f, otherwise we assume that this side effect has already been applied, or
        // will be handled by the caller
        const char* sideEffect = getSideEffect(arg);
        if ( sideEffect ){
          resolveCmdLine->push_back(sideEffect);
        }
      }
    }
  }

  return status;
}

ArgProc::ParseStatusT
ArgProc::tokenizeArgString(const char* str, UtStringArgv* resolveCmdLine, UtString* errMsg, bool expandDashFArgs)
{
  bool nextArgIsFile = false;
  int inComment = 0;
  ParseStatusT stat = parseString(str, resolveCmdLine, errMsg, &nextArgIsFile, &inComment, expandDashFArgs);
  if (inComment != 0)
  {
    *errMsg << "Unterminated /*..*/ comment";
    stat = eParseError;
  }
  
  if ((stat == eParsed) && nextArgIsFile)
  {
    *errMsg << "-f found without a filename";
    stat = eParseError;
  }
  return stat;
}

bool ArgProc::isInputArgFile(const char* arg) const 
{
  bool ret = false;
  const CmdLineArg* option = lookupOption(arg, NULL);
  if (option)
    ret = option->isInputArgFile();
  return ret;
}

// returns null if there is no side effect string registered, otherwise returns the side effect string,
// caller may need to break it apart into individual strings
const char * ArgProc::getSideEffect(const char* arg) const
{
  const CmdLineArg* option = lookupOption(arg, NULL);
  if (option) {
    const char* ret = option->getSideEffect();
    UtString sideEffectStr(ret);
    if ( ! sideEffectStr.empty() ){
      return ret;               // more than 0 characters long, return the string
    }
  }
  return NULL;
}


UInt32 ArgProc::getNumArgs(const char* optionName) const
{
  const CmdLineArg* option = lookupOption(optionName, NULL);
  INFO_ASSERT(option, optionName);
  return option->numValues();
}

void ArgProc::reInit(const char* optionName)
{
  CmdLineArg* option = lookupOption(optionName, NULL);
  INFO_ASSERT(option, optionName);
  return option->reInit();
}

void ArgProc::reInitSection(const char* sectionName)
{
  UtString sectionStr(sectionName);
  Section* section = lookupSection(sectionStr);
  INFO_ASSERT(section, sectionName);

  for (Section::OptionIter p = section->loopOptions(); ! p.atEnd(); ++p)
  {
    CmdLineArg* option = *p;
    option->reInit();
  }
}

SInt32 ArgProc::cAllPasses = 0;


bool ArgProc::testForMisuseOfEFOptions(const UtString& ef_buffer, int argc, char** argv, UtString*errmsg )
{
  // look through all defined options, for options restricted to the .ef file
  // then make sure that if they appeared on command line, they only appeared in the .ef file
  for (OptionVectorCLoop p(mOptions); ! p.atEnd(); ++p)
  {
    CmdLineArg* tmpArg = *p;
    if ( tmpArg->isAllowedOnlyInEFFile() ){
      UtString option_name = tmpArg->getName();
      ParseStatusT status = eParsed;
      // note that here we use two countStringMatches (with different signatures)
      UInt32 seenInEFfile = countStringMatches(ef_buffer, option_name.c_str(), false, &status); // the options from .ef file (without looking into any nested -f files)
      INFO_ASSERT(status == eParsed, "Option check inconsistency.");
      UInt32 seenInCommandLine = countStringMatches(argc, argv, option_name.c_str(), true, &status); // all options (including those from the .ef file)
      INFO_ASSERT(status == eParsed, "Option check inconsistency.");
      INFO_ASSERT(seenInEFfile <= seenInCommandLine, "Option count inconsistency.");

      // the following if condition  works because the -ef option can only
      // appear on the command line alone.  So if the restricted
      // option (option_name) appears in the command line and did not
      // appear the same number of times in the .ef file then there is
      // a problem
      if(seenInEFfile < seenInCommandLine) {
        (*errmsg) << "The option " << option_name << " is not allowed on the command line or in any -f file.";
        return true;
      }
    }
  }
  return false; // all ok
}
