// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004, 2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
// Debug functions
#include <stdio.h>
#include "util/UtList.h"
#include "util/UtArray.h"
#include "util/UtMap.h"
#include "util/UtSet.h"
#include "util/UtHashSet.h"

// please do not use "using namespace std"
#if pfGCC_3 | pfGCC_4
using namespace __gnu_cxx;
#endif

#ifdef CDB

int cdbListSize(void* p)
{
  UtList<int>* l = (UtList<int>*) p;
  return l->size();
}

int cdbArraySize(void* p)
{
  UtArray<int>* l = (UtArray<int>*) p;
  return l->size();
}

int cdbSetSize(void *p)
{
  UtSet<int>* l = (UtSet<int>*) p;
  return l->size();
}

int cdbHashSetSize(void* p)
{
  UtHashSet<int>* l = (UtHashSet<int>*) p;
  return l->size();
}  

int cdbMapSize(void* p)
{
  UtMap<void*,UInt32>* l = (UtMap<void*,UInt32>*) p;
  return l->size();
}


void cdbPrintBV(void* bv, size_t numBits)
{
  UInt32* p = (UInt32 *) bv;
  int numWords = (numBits + 31) / 32;
  for (int i = numWords - 1; i >= 0; --i)
  {
    UInt32  word = p[i];
    int bitsInWord = numBits % 32;
    if (bitsInWord == 0)
      bitsInWord = 32;
    for (int j = bitsInWord - 1; j >= 0; --j)
      fputc(((word & (1 << j)))? '1': '0', stdout);
    numBits -= bitsInWord;
  }
  fputc('\n', stdout);
  fflush(stdout);
}

// see also the definition of cdbPrintNUNetElabSet (defined in NUNet.cxx)

#endif // CDB

void cdbInit()
{
  // Causes these routines to be linked in from the library
}

