// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include "util/Zstream.h"
#include <algorithm> // for replace
#include "util/UtArray.h"
#include "util/UtConv.h"
#include "util/OSWrapper.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"

#include "time.h"

#ifndef __CarbonAssert_H_
#include "util/CarbonAssert.h"
#endif

#ifndef __MemManager_h_
#include "util/MemManager.h"
#endif

#define CARBON_DECODE_ERROR 64

UtString::UtString() {
  init_helper("", 0);
}

UtString::UtString(const char* str) {
  init_helper(str, str? strlen(str): 0);
}

UtString::UtString(const UtString& str, size_t pos, size_t n) {
  if (n == npos) {
    n = str.mSize - pos;
  }
  init_helper(str.c_str() + pos, n);
}

UtString::UtString(const char* str, size_t size) {
  init_helper(str, size);
}

UtString::UtString(size_t n, char ch) {
  init_helper(NULL, n);
  for (size_t i = 0; i < n; ++i) {
    mBuffer[i] = ch;
  }
}

UtString& UtString::operator=(const UtString& src) {
  if (&src != this) {

    // It is not possible for two distinct strings to have
    // overlapping buffers, so resize/memcpy is OK
    resize(src.mSize);
    memcpy(mBuffer, src.mBuffer, src.mSize);
  }
  return *this;
}

UtString::~UtString() {
  CarbonMem::deallocate(mBuffer, mCapacity + 1);
}

void UtString::init_helper(const char* str, size_t len) {
  mCapacity = len;
  mBuffer = (char*) CarbonMem::allocate(len + 1);
  if (str != NULL) {
    memcpy(mBuffer, str, len);  // str might not be nul-terminated
  }
  mBuffer[len] = '\0';        // always nul-terminate mBuffer
  mSize = len;
}

void UtString::clear() {
  resize(0);
}

char* UtString::insert_helper(size_t pos, size_t n) {
  INFO_ASSERT(pos <= mSize, "Index out-of-bounds.");
  size_t nshift = mSize - pos;
  resize(mSize + n);
  char* p = mBuffer + pos;
  memmove(p + n, p, nshift);
  return p;
}

void UtString::reserve(size_t newCap) {
  INFO_ASSERT(newCap >= mSize, "Invalid reserve size.");
  if (mCapacity < newCap) {
    char* newbuf = (char*) CarbonMem::allocate(newCap + 1);
    memcpy(newbuf, mBuffer, mSize + 1); // include NUL
    CarbonMem::deallocate(mBuffer, mCapacity + 1);
    mBuffer = newbuf;
    mCapacity = newCap;
  }
}

void UtString::resize(size_t n) {
  if (mCapacity < n) {
    reserve(2*n);
  }
  mSize = n;
  mBuffer[mSize] = '\0';
}

void UtString::resize(size_t n, char c) {
  size_t i = mSize;
  resize(n);
  for (; i < mSize; ++i) {
    mBuffer[i] = c;
  }
}


char* UtString::getTempOverlapBuf(const char* str, size_t sz, bool* needFree) {
  if ((str >= mBuffer) && ((str + sz) <= mBuffer + mSize)) {
    str = StringUtil::dup(str, sz);
    *needFree = true;
  }
  else {
    *needFree = false;
  }
  return const_cast<char*>(str);
}

UtString& UtString::insert(size_type pos, const char* s, size_t sz) {
  bool needFree;
  char* str = getTempOverlapBuf(s, sz, &needFree);
  char* dest = insert_helper(pos, sz);
  memcpy(dest, str, sz);

  if (needFree) {
    StringUtil::free(str, sz);
  }

  return *this;
}
  
UtString& UtString::insert(size_type pos, size_type n, char c) {
  char* p = insert_helper(pos, n);
  for (char* e = p + n; p != e; ++p)
    *p = c;
  return *this;
}

UtString& UtString::append(size_t n, char c) {
  size_t oldSize = mSize;
  resize(n + oldSize);
  char* p = mBuffer + oldSize;
  for (char* e = p + n; p != e; ++p) {
    *p = c;
  }
  return *this;
}

UtString& UtString::append(const UtString& s, size_t pos, size_t n) {
  INFO_ASSERT(pos <= s.size(), "Index out-of-bounds.");
  const_iterator first = s.begin() + pos;
  const_iterator last = s.begin() + pos + std::min(n, s.size() - pos);
  return insert(end(), first, last);
}

UtString& UtString::insert(iterator pos,
                           const_iterator first, const_iterator last)
{
  INFO_ASSERT(pos <= end(), "Index out-of-bounds.");
  INFO_ASSERT(first <= last, "Iterator parameters are in the wrong order?");
  return insert(pos - mBuffer, first, last - first);
}

UtString& UtString::append(const char* s) {
  INFO_ASSERT(s, "Attempting to append NULL to string.");
  return append(s, strlen(s));
}

void UtString::appendHelper(const char* s, size_t sz, size_t oldSize) {
  INFO_ASSERT(s, "Attempting to append NULL to string.");
  bool needFree;
  char* str = getTempOverlapBuf(s, sz, &needFree);
  resize(sz + oldSize);
  char* dest = mBuffer + oldSize;
  memcpy(dest, str, sz);
  dest[sz] = '\0';
  if (needFree) {
    StringUtil::free(str, sz);
  }    
}

int UtString::compare(const UtString& s) const {
  int cmp = 0;
  size_t sz = mSize;
  if (sz != s.mSize) {
    sz = std::min(sz, s.mSize);
    cmp = memcmp(mBuffer, s.mBuffer, sz);
    if (cmp == 0) {
      cmp = (mSize < s.mSize)? -1: 1;
    }
  } else {
    cmp = memcmp(mBuffer, s.mBuffer, sz);
  }
  return cmp;
}

void UtString::erase(iterator first, iterator last) {
  INFO_ASSERT(last >= first, "Iterator parameters in wrong order?");
  erase(first - mBuffer, last - first);
}
void UtString::erase(size_t pos, size_t n) {
  INFO_ASSERT(pos <= mSize, "Index out-of-bounds.");
  if (n == npos) {
    n = mSize - pos;
  }
  else {
    INFO_ASSERT(pos + n <= mSize, "Consistency check failed.");
  }
  size_t nshift = mSize - pos - n;
  char* p = mBuffer + pos;
  memmove(p, p + n, nshift);
  resize(mSize - n);
}

size_t UtString::find_first_of(const char* s, size_t pos, size_t n) const {
  if (mSize != 0) {
    INFO_ASSERT(pos <= mSize, "Index out-of-bounds.");
    const char* last_s = s + n;
    for (const char* p = mBuffer + pos, *e = end(); p != e; ++p) {
      for (const char* q = s; q != last_s; ++q) {
        if (*p == *q) {
          return p - mBuffer;
        }
      }
    }
  }
  return npos;
}

size_t UtString::find_first_not_of(const char* s, size_t pos, size_t n) const {
  if (mSize != 0) {
    const char* last_s = s + n;
    for (const char* p = mBuffer + pos, *e = end(); p != e; ++p) {
      bool found = false;
      for (const char* q = s; q != last_s; ++q) {
        if (*p == *q) {
          found = true;
          break;
        }
      }
      if (!found) {
        return p - mBuffer;
      }
    }
  }
  return npos;
}

size_t UtString::find_last_of(const char* s, size_t pos, size_t n) const {
  if (mSize != 0) {
    const char* last_s = s + n;
    for (const char* p = mBuffer + std::min(pos, mSize - 1); p >= mBuffer; --p) {
      for (const char* q = s; q != last_s; ++q) {
        if (*p == *q) {
          return p - mBuffer;
        }
      }
    }
  }    
  return npos;
}

size_t UtString::find_last_not_of(const char* s, size_t pos, size_t n) const {
  INFO_ASSERT(pos <= mSize, "Index out-of-bounds.");
  if (mSize != 0) {
    const char* last_s = s + n;
    for (const char* p = mBuffer + std::min(pos, mSize - 1); p >= mBuffer; --p) {
      bool found = false;
      for (const char* q = s; q != last_s; ++q) {
        if (*p == *q) {
          found = true;
          break;
        }
      }
      if (!found) {
        return p - mBuffer;
      }
    }
  }
  return npos;
}

size_t UtString::find(char c, size_t n) const {
  char* p = strchr(mBuffer + n, c);
  if (p == NULL)
    return npos;
  return p - mBuffer;
}

size_t UtString::rfind(char c, size_t pos) const {
  for (const char* p = mBuffer + std::min(pos, mSize - 1); p >= mBuffer; --p) {
    if (*p == c) {
      return p - mBuffer;
    }
  }
  return npos;
}

size_t UtString::find(const char* s, size_t pos, size_t n) const {
  if (n <= mSize) {
    INFO_ASSERT(pos <= mSize, "Index out-of-bounds.");
    for (const char* p = mBuffer + pos, *e = end() - n; p <= e; ++p) {
      if (memcmp(p, s, n) == 0) {
        return p - mBuffer;
      }
    }
  }
  return npos;
}

size_t UtString::rfind(const char* s, size_t pos, size_t n) const {
  if (n <= mSize) {
    for (char* p = mBuffer + std::min(pos, mSize - n); p >= mBuffer; --p) {
      if (memcmp(p, s, n) == 0) {
        return p - mBuffer;
      }
    }
  }
  return npos;
}

void UtString::assign(iterator p, iterator q)
{
  INFO_ASSERT(p <= q, "Iterator parameters in wrong order?");
  assign(p, q - p);
}
void UtString::assign(int repeat, char c)
{
  clear();
  append(repeat, c);
}

void UtString::replace(size_t pos, size_t sz, const char* str)
{
  // this could be more efficient -- it could do 1 memmove rather than 2
  erase(pos, sz);
  insert(pos, str);
}
void UtString::replace(size_t pos, size_t sz, const UtString& str)
{
  // this could be more efficient -- it could do 1 memmove rather than 2
  erase(pos, sz);
  insert(pos, str);
}

void UtString::replace(iterator p, iterator finish, char oldCh, char newCh)
{
  INFO_ASSERT(p <= finish, "Iterator parameters in wrong order?");
  INFO_ASSERT(finish <= end(), "Second parameter not a valid iterator.");
  for (; p != finish; ++p) {
    if (*p == oldCh) {
      *p = newCh;
    }
  }
}

UtString UtString::operator+(const UtString& s) const {
  UtString buf(*this);
  buf += s;
  return buf;
}
UtString UtString::operator+(const char* s) const {
  UtString buf(*this);
  buf += s;
  return buf;
}
UtString UtString::operator+(char c) const {
  UtString buf(*this);
  buf += c;
  return buf;
}

UtString UtString::substr(size_t pos, size_t n)
  const
{
  return UtString(*this, pos, n);
}

bool UtString::dbWrite(ZostreamDB& outDB) const
{
  outDB << *this;
  return ! outDB.fail();
}

bool UtString::dbRead(ZistreamDB& inDB)
{
  inDB >> *this;
  return ! inDB.fail();
}


void UtString::lowercase()
{
  size_t sz = size();
  for ( size_t i = 0; i < sz; ++i )
  {
    char *c = &mBuffer[i];
    *c = tolower(*c);
  }
}

void UtString::uppercase()
{
  size_t sz = size();
  for ( size_t i = 0; i < sz; ++i )
  {
    char *c = &mBuffer[i];
    *c = toupper(*c);
  }
}

/*
std::istream& operator>>(std::istream& f, UtString& str)
{
  return UtStringI::shift(f, str);
}

std::ifstream& operator>>(std::ifstream& f, UtString& str)
{
  return UtStringI::shift(f, str);
}

std::ostream& operator<<(std::ostream& f, const UtString& str)
{
  return UtStringI::shift(f, str);
}

namespace std {
  bool getline(std::istream& f, UtString& str)
  {
    return UtStringI::getline(f, str);
  }
};
*/

/*
std::ofstream& operator<<(std::ofstream& f, const UtString& str)
{
  return UtStringI::shift(f, str);
}
*/


/* this does not work yet...
bool fgetline(ifstream& file, UtString* str, char delim)
{
  const size_t bufsize = 5;
  static char buf[bufsize];

  while (!file.eof())
  {
    file.getline(buf, bufsize, delim);
    int len = strlen(buf);
    str->append(buf, len);
    if (len == 0)
      perror("0 length read");
    if (len != bufsize - 1)
      return true;
  }
  perror("foo");


  return false;                 // never saw newline
}
*/

/*
UtString& operator<<(UtString& str, UtOStringStream& ostr)
{
  std::basic_string<char, std::char_traits<char>, CarbonSTLAlloc<char> > tmp
//  UtString tmp
    = ostr.str();
  str.append(tmp.c_str(), tmp.size());
  return str;
}
*/

/*
std::ostream& operator<<(std::ostream& f, UtString& str)
{
  f << str.c_str();
  return f;
}
*/

//! Allocate (using Carbon allocator) a string of numBytes bytes
char* StringUtil::alloc(size_t numBytes)
{
  return CARBON_ALLOC_VEC(char, numBytes);
}

//! Allocate (using Carbon allocator) and duplicate a zero-terminated string
//! of strlen(s)+1 bytes
char* StringUtil::dup(const char* s)
{
  size_t numChars = strlen(s);
  void* buffer = CARBON_ALLOC_VEC(char, numChars+1);
  return static_cast<char*>(memcpy(buffer, static_cast<const void*>(s), numChars+1));
}

//! Allocate (using Carbon allocator) and duplicate an array of N characters.
//! Zeros aren't special.
char* StringUtil::dup(const char* s, size_t numChars)
{
  void* buffer = CARBON_ALLOC_VEC(char, numChars);
  return static_cast<char*>(memcpy(buffer, static_cast<const void*>(s), numChars));
}

//! Free a string allocated with alloc() or dup().  Assumes the array is one
//! greater than strlen(s).
void StringUtil::free(char* s)
{
  CARBON_FREE_VEC(s, char, strlen(s)+1);
}

//! Free a string allocated with alloc() or dup().  Don't forget to include
//! any terminating null character in the count.
void StringUtil::free(char* s, size_t numBytes)
{
  CARBON_FREE_VEC(s, char, numBytes);
}


//! Walk a character pointer past any delimiters at the beginning
const char* StringUtil::skip(const char* s, const char* delims)
{
  while ((*s != '\0') && (strchr(delims, *s) != NULL))
    ++s;
  return s;
}

//! Walk a character pointer past any delimiters at the beginning
const char* StringUtil::skip(const char* s, size_t sz,
                             const char* delims)
{
  for (size_t i = 0; (i != sz) && (strchr(delims, *s) != NULL); ++i)
    ++s;
  return s;
}

//! parse the current token as a based number, returning true for success
bool StringUtil::parseNumber(const char* str, UInt32* number, int base)
{
  char* endptr;
  *number = OSStrToU32(str, &endptr, base, NULL);
  return (*endptr == '\0');
}

//! parse the current token as a based number, returning true for success
bool StringUtil::parseNumber(const char* str, SInt32* number, int base)
{
  char* endptr;
  *number = OSStrToS32(str, &endptr, base, NULL);
  bool success = (*endptr == '\0');
  if ( ( not success ) && ( base == 16 ) ){
    UInt32 unsigned_val = OSStrToU32(str, &endptr, base, NULL);
    success = (*endptr == '\0');
    if ( success ){
      *number = static_cast<SInt32>(unsigned_val);
    }
  }
  return success;
}

//! parse the current token as a based number, returning true for success
bool StringUtil::parseNumber(const char* str, double* number)
{
  char* endptr;
  *number = OSStrToD(str, &endptr, NULL);
  return (*endptr == '\0');
}

//! constructor
StrToken::StrToken(const char* str, const char* delims)
{
  mStr = NULL;
  mNextTok = str;
  mDelims = delims;
  if (str != NULL)
    ++(*this);
}

//! get the current token
const char* StrToken::operator*() const {
  // setup mBuf
  mBuf.erase();
  mBuf.insert(0, mStr, mNextTok - mStr);
  return mBuf.c_str();
}

//! advance to the next token
StrToken& StrToken::operator++()
{
  // skip leading whitespace
  const char* delims = mDelims.c_str();
  mNextTok = StringUtil::skip(mNextTok, delims);

  // Check for end of string
  mStr = mNextTok;
  if (*mNextTok == '\0')
    mNextTok = NULL;
  else
  {
    // find end ptr
    ++mNextTok;
    while ((*mNextTok != '\0') && (strchr(delims, *mNextTok) == NULL))
      ++mNextTok;
  }
  return *this;
}

// check for the end, get the current token, advance to the next one
bool StrToken::operator()(const char** ptr) {
  if (atEnd())
    return false;
  *ptr = **this;
  ++(*this);
  return true;
}

UtString& UtString::operator<<(const void * ptr)
{
  INFO_ASSERT(sizeof(UIntPtr) == sizeof(void *), "Incorrect type specification for UIntPtr.");
  char s[32];
  int width;
  width = sprintf(s, FormatPtr, ptr);

  append(s, width);
  return *this;
}

UtString& UtString::operator<<(UInt64 num)
{
  char s[32]; // more than enough characters
  sprintf(s, Format64 (u), num);
  append(s);
  return *this;
}

UtString& UtString::operator<<(UInt32 num)
{
  char s[32];
  sprintf(s, "%u", num);
  append(s);
  return *this;
}

UtString& UtString::operator<<(UInt16 num)
{
  char s[32];
  sprintf(s, "%u", num);
  append(s);
  return *this;
}

UtString& UtString::operator<<(UInt8 num)
{
  char s[32];
  sprintf(s, "%u", num);
  append(s);
  return *this;
}

UtString& UtString::operator<<(SInt64 num)
{
  char s[32];
  sprintf(s, Format64 (d), num);
  append(s);
  return *this;
}

UtString& UtString::operator<<(SInt32 num)
{
  char s[32];
  sprintf(s, "%d", num);
  append(s);
  return *this;
}

UtString& UtString::operator<<(SInt16 num)
{
  char s[32];
  sprintf(s, "%d", num);
  append(s);
  return *this;
}

UtString& UtString::operator<<(double num)
{
  char s[100];
  sprintf(s, "%f", num);
  append(s);
  return *this;
}

bool operator>>(UtString& buf, char& c) {
  if (not buf.empty())
  {
    UtString::iterator p = buf.begin();
    c = *p;
    buf.erase(p);
    return true;
  }
  return false;
}

bool operator>>(UtString& str, UInt64& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToU64(s, &e, 0, NULL);
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}

bool operator>>(UtString& str, UInt32& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToU32(s, &e, 0, NULL);
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}

bool operator>>(UtString& str, UInt16& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToU32(s, &e, 0, NULL); // possible loss of data
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}

bool operator>>(UtString& str, UInt8& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToU32(s, &e, 0, NULL); // possible loss of data
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}


bool operator>>(UtString& str, SInt64& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToS64(s, &e, 0, NULL);
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}

bool operator>>(UtString& str, SInt32& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToS32(s, &e, 0, NULL);
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}

bool operator>>(UtString& str, SInt16& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToS32(s, &e, 0, NULL); // possible loss of data
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}

bool operator>>(UtString& str, double& num)
{
  const char* s = str.c_str();
  char *e;
  num = OSStrToD(s, &e, NULL);
  if (e == s)
    return false;
  str.erase(0, e - s);
  return true;
}

bool fgetline(FILE* file, UtString* str, char delim)
{
  str->clear();
  INFO_ASSERT(delim == '\n', "Delimiter must be newline.");        // since I use fgets I can't do anything else
  const size_t bufsize = BUFSIZ;
  char buf[bufsize];
  while ((fgets(buf, bufsize, file) != NULL) && (*buf != '\0')) {
    str->append(buf);
    if (*str->rbegin() == delim)
      return true;
  }
  
  if (str->empty())
    return false;                 // EOF

  return true;
}

#define ABS(a) (((a) < 0)? (-(a)): (a))

template<class IntType>
void sFormatInt(IntType num, UtString* buf, int numCols)
{
  char tmp[100], format[10], suffix[2];
  suffix[0] = suffix[1] = '\0';
  long lnum = (long) num;
  if (ABS(lnum) < 1000)
  {
    sprintf(format, "%%%dd", numCols);
    sprintf(tmp, format, (long) num);
  }
  else
  {
    sprintf(format, "%%%df", numCols - 1);
    if (num < 1000000)
    {
      sprintf(tmp, format, ((double) num) / 1000.0);
      suffix[0] = 'K';
    }
    else if (num < 1000000000)
    {
      sprintf(tmp, format, ((double) num) / 1000000.0);
      suffix[0] = 'M';
    }
    else
    {
      sprintf(tmp, format, ((double) num) / 1000000000.0);
      suffix[0] = 'G';
    }
    tmp[numCols - 1] = '\0';
  }
  *buf << tmp << suffix;
} // static void sFormatInt

void StringUtil::formatInt(SInt32 num, UtString* buf, int numCols)
{
  sFormatInt(num, buf, numCols);
}

void StringUtil::formatInt(SInt64 num, UtString* buf, int numCols)
{
  sFormatInt(num, buf, numCols);
}

void StringUtil::formatInt(UInt32 num, UtString* buf, int numCols)
{
  sFormatInt(num, buf, numCols);
}

void StringUtil::formatInt(UInt64 num, UtString* buf, int numCols)
{
  sFormatInt(num, buf, numCols);
}

void StringUtil::deSlashify(UtString* buf)
{
  UtString newBuf;
  const char* bufP;

  bool inSlash = false;
  static const char* scCharsBSMap = "t\tn\nr\r";
  
  for (bufP = buf->c_str(); *bufP != '\0'; ++bufP)
  {
    if (*bufP == '\\')
    {
      if (inSlash)
      {
        newBuf << *bufP;        
        inSlash = false;
      }
      else
        inSlash = true;
    }
    else
    {
      char p = *bufP;
      const char* s;
      if (inSlash && ((s = strchr(scCharsBSMap, *bufP)) != NULL))
      {
        // make sure that the map of chars to their backslashed
        // counterparts is valid
        INFO_ASSERT (*(s + 1) != '\0', "Buffer overrun");
        // The current s should be pointing to a printable character
        INFO_ASSERT ((*s > '!') && (*s < '~'), "Buffer overrun.");
        // Grab the intended character
        p = *(s + 1);
      }
      newBuf << p;
      inSlash = false;
    }
  }
  *buf = newBuf;
}

const char* StringUtil::findFirstNonAlphanumeric(const char* name)
{
  const char* ret = NULL;
  bool alphaNumeric = true;;
  while (name && (*name != '\0') &&
         (alphaNumeric = isalnum(*name)))
    ++name;
  
  if (! alphaNumeric)
    ret = name;
  return ret;
}

#if 0
void StringUtil::slashifyToken(const char *in, UtString* out) {
  // We will tokenize on whitespace, so if there is any embedded
  // whitespace then we will need to surround the text with ".
  bool hasWhitespace = false;
  for (const char *p = in; !hasWhitespace && (*p != '\0'); ++p) {
    hasWhitespace = isspace(*p);
  }

  if (hasWhitespace) {
    *out << '"';
  }
  escapeCString(in, out);
  if (hasWhitespace) {
    *out << '"';
  }
  *out << ' ';
}

StringUtil::SlashifiedTokenizer::SlashifiedTokenizer(const char* ptr):
  mIn(NULL),
  mOrigIn(NULL),
  mHasQuote(false),
  mStatus(eTokenComplete)
{
  parseInput(ptr);
}

void StringUtil::SlashifiedTokenizer::parseInput(const char* in) {
  mIn = skip(in);
  mOrigIn = in;

  if (mStatus == eTokenComplete) {
    mHasQuote = *in == '"';
    mStatus = eEndOfData;
  }

  for (char c = *mIn; (hasQuote ? (c != '"') : !isspace(c)); c = *++mIn) {
    if (c == '\0') {
      mStatus = eEndOfData;     // quote not yet closed
      return eEndOfData;
    }

    if (c == '\\') {
      c = *++in;
      switch (c) {
      case 'n':  *mOutputBuf += '\n'; break;
      case 't':  *mOutputBuf += '\n'; break;
      case 'r':  *mOutputBuf += '\r'; break;
      case '"':  *mOutputBuf += '"'; break;
      case '\\': *mOutputBuf += '\\'; break;
      case '\'': *mOutputBuf += '\''; break;
      default:
        mStatus = eMalformedSlash;
        return mStatus;
      }
    }
    else {
      *mOutputBuf += c;
    }
  } // for

  mStatus = eTokenComplete;
  return mStatus;
} // void StringUtil::SlashifiedTokenizer::addInput

SInt32 StringUtil::readSlashifiedToken(const char *in, UtString* out) {
  SlashifiedTokenizer tok(out);
  if (tok.addInput(in) == StringUtil::SlashifiedTokenizer::eTokenComplete) {
    return tok.numConsumed();
  }
  return -1;
}
#endif

void StringUtil::escapeCString(const char *in, UtString* out, char newlineCode)
{
  for (const char *p = in; *p != '\0'; p++)
  {
    if (*p == '\n') {
      *out += '\\';
      *out += newlineCode;
    } else {
      if (*p == '\\' || *p == '"')
        *out += '\\';
      *out += *p;
    }
  }
}

// This is basically a base64 encoding using this map:
//   0..25 -> A-Z
//  26..51 -> a-z
//  52..61 -> 0-9
//      62 -> +
//      63 -> /
//
static unsigned char sEncodeChar(unsigned char in)
{
  unsigned char out;
  if (in < 26)
    out = 'A' + in;
  else if (in < 52)
    out = 'a' - 26 + in;
  else if (in < 62)
    out = '0' - 52 + in;
  else if (in == 62)
    out = '+';
  else if (in == 63)
    out = '/';
  else
    out = 0; // error

  INFO_ASSERT(out != 0, "Invalid character.");
  return out;
}

// Decode the chars mapped by sEncodeChar
// If speed becomes an issue, consider a sparse array
// (256-byte table with 64 values indexed by in char).
static unsigned char sDecodeChar(unsigned char in)
{
  unsigned char out;
  if (in >= 'A' && in <= 'Z')
    out = in - 'A';
  else if (in >= 'a' && in <= 'z')
    out = in - 'a' + 26;
  else if (in >= '0' && in <= '9')
    out = in - '0' + 52;
  else if (in == '+')
    out = 62;
  else if (in == '/')
    out = 63;
  else
    out = CARBON_DECODE_ERROR;
  return out;
}

// we use chars 58 to 64 as pads:  :;<=>?@
static unsigned char sGetPad(unsigned char seed)
{
  return 58 + (seed % 7);
}

// return true if the given char is one returned by sGetPad()
static bool sIsPad(unsigned char c)
{
  return c >= 58 && c <= 64;
}

// Perform the actual encoding of the binary data.
// We read 3 input bytes, padding with 0 if necessary, to
// to get 24 bits.  We then break these into 4 6-bit numbers
// and encode the result using sEncodeChar().
void StringUtil::asciiEncode(const char *bytes, size_t len, UtString* encoded)
{
  unsigned char in[3], out[4];
  size_t index = 0;

  encoded->clear();
  while (index < len)
  {
    // fetch three input bytes, padding with zero if we run out
    int numZeroBytesAdded = 0;
    for (int i = 0; i < 3; i++)
    {
      if (index < len)
        in[i] = bytes[index];
      else
      {
        in[i] = 0;
        ++numZeroBytesAdded;
      }
      ++index;
    }

    // split the 24 bits we have collected from the input into 4 6-bit
    // numbers, encoding each one.
    out[0] = sEncodeChar((in[0] & 0xfc) >> 2);
    out[1] = sEncodeChar(((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4));
    out[2] = sEncodeChar(((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6));
    out[3] = sEncodeChar(in[2] & 0x3f);

    // if we padded the input with zeros, then some of the output bytes
    // need to be pad markers instead of 'real' data values.
    switch (numZeroBytesAdded)
    {
      case 2: out[2] = sGetPad(out[0]);
      case 1: out[3] = sGetPad(out[1]);
    }

    // append the encoded bytes to the output
    encoded->append((const char *) out, 4);

    // break lines
    if ((index  % 19) == 0)
      encoded->append("\n");
  }
}

// Decode the given data.
// We read 4 input bytes at a time, treating pads as 0, and map them 
// into 6-bit values using sDecodeChar().
// The 4 6-bit values are then packed into 3 8-bit bytes, which 
// represent the data that was originally encoded using asciiEncode().
bool StringUtil::asciiDecode(const char *bytes, size_t len, UtString* decoded)
{
  unsigned char in[4], out[3];
  unsigned char c;
  size_t index = 0;

  decoded->clear();

  // calculate end of data within buffer (up to trailing whitespace)
  size_t dataLen = len;
  while (dataLen > 0 && isspace(bytes[dataLen -1]))
    --dataLen;

  while (index < dataLen)
  {
    // fetch 4 bytes of input
    int numPads = 0;
    for (int i = 0; i < 4; i++)
    {
      // get next input byte, skipping spaces
      c = 115;
      while (index < dataLen && isspace(c = bytes[index++]))
        ;

      // decode the byte, check for pads.
      if (sIsPad(c))
      {
        ++numPads;
        in[i] = 0;
      }
      else
      {
        in[i] = sDecodeChar(c);

        // check for decoding error
        if (in[i] == CARBON_DECODE_ERROR)
        {
          return false;          
        }
      }
    }

    // munge the 4 6-bit values into 3 8-bit values.
    out[0] = (in[0] << 2) | ((in[1] & 0x30) >> 4);
    out[1] = ((in[1] & 0x0f) << 4) | ((in[2] & 0x3c) >> 2);
    out[2] = ((in[2] & 0x03) << 6) | (in[3] & 0x3f);

    // if the input contained pad bytes, the number of bytes to output
    // may be less than 3. We only output bytes that have non-pad
    // bits in them.  
    if (! (numPads < 3 && numPads >=0))
      return false;

    int numBytes = 3 - numPads;

    // append bytes to output
    decoded->append((const char *) out, numBytes);
  }
  return true;
}

void StringUtil::strip(UtString* str, const char* delims)
{
  size_t remove = 0;
  for (const char* p = str->c_str();
       (*p != '\0') && (strchr(delims, *p) != NULL);
       ++p, ++remove)
    ;
  str->erase(0, remove);
  
  remove = 0;
  for (int i = ((int) str->size()) - 1;
       (i >= 0) && (strchr(delims, (*str)[i]) != NULL);
       --i, ++remove)
    ;
  str->resize(str->size() - remove);
}

char *StringUtil::index(const char *str, char c)
{
  // discarding 'const' here makes the func more usabale, 
  // and conforms to the definition for 'index' in strings.h
  char *result = (char *) str;

  while (*result != '\0' && *result != c) {
    ++result;
  }
  return (*result == c) ? result : NULL;
}


size_t UtString::count(const char* str, char c) {
  size_t cnt = 0;
  for (; *str != '\0'; ++str) {
    if (*str == c) {
      ++cnt;
    }
  }
  return cnt;
}

void StringUtil::makeIdent(UtString* buf)
{
  for (size_t i = 0; i < buf->size(); ++i)
    if (!isalnum((*buf)[i]) && ((*buf)[i] != '_'))
      (*buf)[i] = '$';
}


// Helper class for null-terminated argv arrays
UtStringArgv::UtStringArgv()
{
  mArgv = new CharArray;
  mArgv->push_back(NULL);
  mStorage = new UtStringArray;
}


UtStringArgv::UtStringArgv(int argc, char** argv)
{
  mArgv = new CharArray(argc + 1);
  mStorage = new UtStringArray(argc);
  for (int i = 0; i < argc; ++i)
  {
    const char* str = argv[i];
    mStorage->push_back(str);
    char* s = (*mStorage)[i];
    (*mArgv)[i] = s;
  }
  (*mArgv)[argc] = NULL;
}

UtStringArgv::UtStringArgv(const UtStringArgv& src)
{
  mArgv = new CharArray;
  mArgv->push_back(NULL);
  mStorage = new UtStringArray;
  for (int i = 0; i < src.getArgc(); ++i)
    push_back((*src.mArgv)[i]);
}

UtStringArgv::~UtStringArgv()
{
  // delete the references first
  delete mArgv;
  // Now delete the storage
  delete mStorage;

}

UtStringArgv& UtStringArgv::operator=(const UtStringArgv& src)
{
  if (&src != this) {
    mArgv->clear();
    mArgv->push_back(NULL);
    mStorage->clear();
    for (int i = 0; i < src.getArgc(); ++i)
      push_back((*src.mArgv)[i]);
  }
  return *this;
}

UtStringArgv& UtStringArgv::operator+(const UtStringArgv& src)
{
    for (int i = 0; i < src.getArgc(); ++i)
	push_back((*src.mArgv)[i]);
    return *this;
}

void UtStringArgv::push_back(const char* str)
{
  mStorage->push_back(str);
  (*mArgv)[getArgc()] = mStorage->back();
  mArgv->push_back(NULL);       // maintain null termination
}

void UtStringArgv::replace(size_t index, const char* newVal)
{
  mStorage->push_back(newVal);
  INFO_ASSERT(((int) index) < getArgc(), "Invalid array index.");
  (*mArgv)[index] = mStorage->back();
}

void UtStringArgv::push_back(const UtString& str)
{
  push_back(str.c_str());
}

char** UtStringArgv::getArgv()
{
  return &((*mArgv)[0]);
}

int UtStringArgv::getArgc()
  const
{
  return mArgv->size() - 1;
}

void UtStringArgv::putArgc(int newArgc)
{
  INFO_ASSERT(newArgc <= getArgc(), "Invalid array index.");
  if (newArgc < getArgc())
  {
    mArgv->resize(newArgc + 1);
    (*mArgv)[newArgc] = NULL;
  }
}

