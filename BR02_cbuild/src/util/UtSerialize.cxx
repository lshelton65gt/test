//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// -*-C++-*-

#include "util/UtSerialize.h"

#if 0
UtSerialObject::UtSerialObject() {
}

UtSerialObject::~UtSerialObject() {
}
#endif

UtSerialStruct::UtSerialStruct() {
}

UtSerialStruct::~UtSerialStruct() {
  mNameMap.clearPointerValues();
}


bool UtSerialStruct::read(UtIStream* is) {
  *is >> UtIO::quotify;

  UtString tok;
  if (! (*is >> tok)) {
    return false;
  }
  if (tok != "{") {
    is->reportError("Expected { to open structure");
  }
  char c;
  while (*is >> c) {
    if (!isspace(c)) {
      if (c == '}') {
        return true;            // end of structure without errors
      }
      is->unget(&c, 1);
      if (! (*is >> tok)) {
        return false;           // could not find keyword
      }

      // Expect the token to be one of the names in our map
      NameMap::iterator p = mNameMap.find(tok);
      if (p == mNameMap.end()) {
        UtString buf("Unexpected token: ");
        buf << tok;
        is->reportError(buf.c_str());
      }
      else {
        UtSerialObject* obj = p->second;
        if (!obj->read(is)) {
          return false;         // failed to read object
        }
      }
    }
  }
  is->reportError("Expected } to close structure");
  return false; 
} // virtual bool read

void UtSerialStruct::write(UtOStream* os, UInt32 indent) {
  *os << UtIO::quotify;

  *os << '{' << '\n';
  indent += 2;
  for (NameMap::SortedLoop p = mNameMap.loopSorted(); !p.atEnd(); ++p) {
    for (UInt32 i = 0; i < indent; ++i) {
      *os << ' ';
    }
    *os << p.getKey() << ' ';
    UtSerialObject* obj = p.getValue();
    obj->write(os, indent);
    *os << '\n';
  }
  indent -= 2;
  for (UInt32 i = 0; i < indent; ++i) {
    *os << ' ';
  }
  *os << '}';
}
